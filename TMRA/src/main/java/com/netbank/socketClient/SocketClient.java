package com.netbank.socketClient;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.binary.Hex;

import com.google.common.primitives.Bytes;
import com.netbank.telcomm.FundResult;
import com.netbank.util.HexDumpUtil;

import bank.comm.CommunicationException;
import lombok.extern.slf4j.Slf4j;

/**
 * 基金電文的socket client
 */
@Slf4j
public class SocketClient {
	
	/**
	 * socket連線
	 * @throws CommunicationException 
	 */
	public SocketChannel connect(String ip,int port) throws CommunicationException{
		SocketChannel socketChannel = null;
		try{
			socketChannel = SocketChannel.open();
			socketChannel.configureBlocking(false);
//			socketChannel.configureBlocking(true);
			socketChannel.socket().setSoTimeout(5*1000);
			socketChannel.connect(new InetSocketAddress(ip, port));
			socketChannel.socket().setKeepAlive(true);
			//現在時間
			long currentTime = System.currentTimeMillis();
			
			log.debug("block line...");
			//設定timeout
			while(!socketChannel.finishConnect() && ((System.currentTimeMillis() - currentTime) <= 5000)){
				log.debug("連線中...");
				Thread.sleep(1*50);
			}
			
			if(!socketChannel.finishConnect()){
				socketChannel = null;
//				return null;
				throw new Exception("Connect fail");
			}
		}
		catch(Exception e){
			log.error("",e);
			throw new CommunicationException("M1", "Z020", "");
//			return null;
		}
		return socketChannel;
	}
	
	public SocketChannel waitTimeOut(SocketChannel socketChannel){
		//現在時間
		long currentTime = System.currentTimeMillis();
		try {
			while(!socketChannel.finishConnect() && ((System.currentTimeMillis() - currentTime) <= 5000)){
				log.debug("連線中...");
				Thread.sleep(1*1000);
			}
			
			if(!socketChannel.finishConnect()){
				return null;
			}
		} catch (Exception e) {
			log.error("",e);
			return null;
		}
		return socketChannel;
	}
	/**
	 * 傳送資料
	 * @throws CommunicationException 
	 */
	public SocketChannel send(SocketChannel socketChannel,byte[] finalByte) throws CommunicationException{
		try{
			//編碼改成BIG5
			//log.debug("finalByteString=" + new String(finalByte,"UTF-8"));
//			log.debug("finalByteString=" + new String(finalByte,"BIG5"));
//			log.debug("finalByteHEX=" + StringUtils.toHex(finalByte));
//			socketChannel.socket().setSoTimeout(6*1000);
//			socketChannel.configureBlocking(true);
			log.trace("finalByte>>{}",finalByte);
			log.trace("finalByte.length>>{}",finalByte.length);
//			ByteBuffer byteBuffer = ByteBuffer.allocate(finalByte.length);
			ByteBuffer byteBuffer = ByteBuffer.allocate(4096);
			
			byteBuffer.put(finalByte);
			byteBuffer.flip();
			
			while(byteBuffer.hasRemaining()){
				socketChannel.write(byteBuffer);
			}
			
			byteBuffer.clear();
		}
		catch(Exception e){
			log.error("",e);
			throw new CommunicationException("M1", "Z027", "");
//			return null;
		}
		return socketChannel;
	}
	/**
	 * 傳送資料
	 * 臨時測試外匯400 login 用
	 */
	public SocketChannel sendII(SocketChannel socketChannel,byte[] finalByte){
		try{
			//編碼改成BIG5
			//log.debug("finalByteString=" + new String(finalByte,"UTF-8"));
//			log.debug("finalByteString=" + new String(finalByte,"BIG5"));
//			log.debug("finalByteHEX=" + StringUtils.toHex(finalByte));
//			socketChannel.socket().setSoTimeout(6*1000);
//			socketChannel.configureBlocking(true);
			ByteBuffer byteBuffer = ByteBuffer.allocate(8192);
			
			byteBuffer.put(finalByte);
			byteBuffer.flip();
			
			while(byteBuffer.hasRemaining()){
				socketChannel.write(byteBuffer);
			}
//			TODO 臨時測試用
			ByteBuffer byteBuffer2 = ByteBuffer.allocate(8192);
			log.debug("byteBuffer2=" + byteBuffer2);
			int totalBytes = socketChannel.read(byteBuffer2);
			log.debug("totalBytes=" + totalBytes);
			
			int watitimes = 1;
			while(totalBytes==0 && watitimes <10){
				totalBytes = socketChannel.read(byteBuffer);
				Thread.sleep(1*1000);
				log.trace("totalBytes_z>>{}",totalBytes);
				watitimes++;
			}
			log.debug("watitimes>>"+watitimes);
			if(watitimes ==10){
				log.debug("response timeout>>"+watitimes);
			}
			
			byteBuffer2.flip();
			
			if(byteBuffer2.remaining() > 0){
				byte[] data = byteBuffer2.array();
				log.trace("receive data>>",data);
				log.trace("string  data>>",new String(data));
			}
			
			
			
			
			
			
			byteBuffer.clear();
		}
		catch(Exception e){
			log.error("",e);
			return null;
		}
		return socketChannel;
	}
	/**
	 * 接收資料
	 * @throws CommunicationException 
	 */
	public byte[] receive(SocketChannel socketChannel) throws CommunicationException{
		byte[] data = null;
		ByteBuffer byteBuffer =null;
		try{
			log.debug("socketChannel port=" + socketChannel.socket().getPort());
			log.debug("socketChannel local port=" + socketChannel.socket().getLocalPort());
			log.debug("socketChannel getReceiveBufferSize=" + socketChannel.socket().getReceiveBufferSize());
			log.debug("socketChannel SoTimeout=" + socketChannel.socket().getSoTimeout());
			log.debug("isBlocking =" + socketChannel.isBlocking());
//			測試400 回應封包堆疊情況， 測試完記得註解掉
//			try {  
//				Thread.sleep(2000);   
//			} catch (InterruptedException e) {  
//				e.printStackTrace();  
//			}
			byteBuffer = ByteBuffer.allocate(8192);
			log.debug("byteBuffer=" + byteBuffer);
			int totalBytes = socketChannel.read(byteBuffer);
			log.debug("totalBytes=" + totalBytes);
//			20161216 add by hugo 
//			注意!此timeout機制必須在非阻塞模式下才有作用  isBlocking = false;
//			建立receive timeout 機制 totalBytes=0表示無資料回傳或以讀取完畢 ，timeout 設定60秒
//			https://sw1982.iteye.com/blog/870612
			int watitimes = 1;
//			while(totalBytes==0 && watitimes <60){
//				totalBytes = socketChannel.read(byteBuffer);
//				Thread.sleep(1*1000);
//				watitimes++;
//			}
			while(totalBytes==0 && watitimes <6000){
				totalBytes = socketChannel.read(byteBuffer);
//				if(totalBytes !=0) {
//					log.debug("totalBytes_zz>>{}",totalBytes);
//					log.debug("watitimes_zz>>{}",watitimes);
//				}
				Thread.sleep(10);
				watitimes++;
			}
			log.debug("watitimes>>{}",watitimes);
			if(watitimes ==6000){
				log.debug("response timeout>>{}",watitimes);
			}
			
			byteBuffer.flip();
			log.trace("byteBuffer.remaining()>>{}",byteBuffer.remaining());
			int length = 0;
			length = byteBuffer.remaining();
//			byteBuffer.clear();
			if(length > 0){
//				data = byteBuffer.array();
//				log.trace("receive data>>{}",data);
//				log.trace("string  data>>{}",new String(data));
				data = new byte[length];
				System.arraycopy(byteBuffer.array(),0,data,0,length);
				if(log.isTraceEnabled()) {
					log.trace("EBCDIC  data>>\n{}",HexDumpUtil.format(data,72, HexDumpUtil.HD_EBCDIC));
				}
			}else {
				throw  new Exception("recv Timeout ");
			}
		}
		catch(Exception e){
			log.error("",e);
			throw new CommunicationException("M1", "Z032", "");
		}finally {
			if(byteBuffer !=null) {
				byteBuffer.clear();
			}
		}
		return data;
	}
	
	/**
	 * 外匯專用
	 * @param socketChannel
	 * @return
	 * @throws CommunicationException
	 */
	public byte[] receiveForFx(SocketChannel socketChannel) throws CommunicationException{
		byte[] data = null;
		ByteBuffer byteBuffer =null;
		try{
			log.debug("socketChannel port=" + socketChannel.socket().getPort());
			log.debug("socketChannel local port=" + socketChannel.socket().getLocalPort());
			log.debug("socketChannel getReceiveBufferSize=" + socketChannel.socket().getReceiveBufferSize());
			log.debug("socketChannel SoTimeout=" + socketChannel.socket().getSoTimeout());
			log.debug("isBlocking =" + socketChannel.isBlocking());
//			測試400 回應封包堆疊情況， 測試完記得註解掉
//			try {  
//				Thread.sleep(2000);   
//			} catch (InterruptedException e) {  
//				e.printStackTrace();  
//			}
			byteBuffer = ByteBuffer.allocate(8192);
			log.debug("byteBuffer=" + byteBuffer);
			int totalBytes = socketChannel.read(byteBuffer);
			log.debug("totalBytes=" + totalBytes);
//			20161216 add by hugo 
//			注意!此timeout機制必須在非阻塞模式下才有作用  isBlocking = false;
//			建立receive timeout 機制 totalBytes=0表示無資料回傳或以讀取完畢 ，timeout 設定60秒
//			https://sw1982.iteye.com/blog/870612
			int watitimes = 1;
//			while(totalBytes==0 && watitimes <60){
//				totalBytes = socketChannel.read(byteBuffer);
//				Thread.sleep(1*1000);
//				watitimes++;
//			}
			int maxBytes = 0;
//			60*1000 /500 = 1200
			while( maxBytes < 4096 && watitimes <(6*1000)){
				totalBytes = socketChannel.read(byteBuffer);
//				log.debug("totalBytesXX>>{}",totalBytes);
				maxBytes += totalBytes;
//				log.debug("maxBytes>>{}",maxBytes);
				if(maxBytes >= 4096 ) {
					log.debug("maxBytes>>{}",maxBytes);
					log.debug("watitimes_zz>>{}",watitimes);
				}
				Thread.sleep(10);
				watitimes++;
			}
//			60*1000 /500 = 1200
//			while(totalBytes==0 && watitimes <120){
//				totalBytes = socketChannel.read(byteBuffer);
//				if(totalBytes !=0) {
//					log.debug("totalBytes_zz>>{}",totalBytes);
//					log.debug("watitimes_zz>>{}",watitimes);
//				}
//				Thread.sleep(200);
//				watitimes++;
//			}
			log.debug("watitimes>>{}",watitimes);
			if(watitimes ==(6*1000)){
				log.debug("response timeout>>{}",watitimes );
			}
			
			byteBuffer.flip();
			log.trace("byteBuffer.remaining()>>{}",byteBuffer.remaining());
			int length = 0;
			length = byteBuffer.remaining();
//			byteBuffer.clear();
			if(length > 0){
//				data = byteBuffer.array();
//				log.trace("receive data>>{}",data);
//				log.trace("string  data>>{}",new String(data));
				data = new byte[length];
				System.arraycopy(byteBuffer.array(),0,data,0,length);
				if(log.isTraceEnabled()) {
					log.trace("EBCDIC  data>>\n{}",HexDumpUtil.format(data,72, HexDumpUtil.HD_EBCDIC));
				}
			}else {
				throw  new Exception("recv Timeout ");
			}
		}
		catch(Exception e){
			log.error("",e);
			throw new CommunicationException("M1", "Z032", "");
		}finally {
			if(byteBuffer !=null) {
				byteBuffer.clear();
			}
		}
		return data;
	}
	
	public byte[] receiveII(SocketChannel socketChannel){
		Map<String,Object> receiveMap = null;
		byte[] data = null;
		try{
			log.debug("socketChannel port=" + socketChannel.socket().getPort());
			log.debug("socketChannel local port=" + socketChannel.socket().getLocalPort());
			log.debug("socketChannel getReceiveBufferSize=" + socketChannel.socket().getReceiveBufferSize());
			log.debug("socketChannel SoTimeout=" + socketChannel.socket().getSoTimeout());
			log.debug("isBlocking =" + socketChannel.isBlocking());
//			測試400 回應封包堆疊情況， 測試完記得註解掉
//			try {  
//				Thread.sleep(5000);   
//			} catch (InterruptedException e) {  
//				e.printStackTrace();  
//			}
			ByteBuffer byteBuffer = ByteBuffer.allocate(8192);
			log.debug("byteBuffer=" + byteBuffer);
			int totalBytes = socketChannel.read(byteBuffer);
			log.debug("totalBytes=" + totalBytes);
			byte[] tmp = byteBuffer.array();
			log.debug(" Hex.encodeHexString.byteBuffer={}",Hex.encodeHexString(byteBuffer.array()));
			log.debug(" Hex.encodeHexString={}", Hex.encodeHexString(tmp, true));
			log.debug("byteBuffer.array={}" , byteBuffer.array());
			log.debug("byteBuffer.string={}" ,new String(byteBuffer.array()));
//			20161216 add by hugo 
//			注意!此timeout機制必須在非阻塞模式下才有作用  isBlocking = false;
//			建立receive timeout 機制 totalBytes=0表示無資料回傳或以讀取完畢 ，timeout 設定10秒
			int watitimes = 1;
			while(totalBytes==0 && watitimes <10){
				totalBytes = socketChannel.read(byteBuffer);
				Thread.sleep(1*1000);
				log.trace("ss>>",new String(byteBuffer.array()));
				watitimes++;
			}
			log.debug("watitimes>>"+watitimes);
			if(watitimes ==10){
				log.debug("response timeout>>"+watitimes);
			}
			
			byteBuffer.flip();
			if(byteBuffer.remaining() > 0){
				data = byteBuffer.array();
				int a = Bytes.indexOf(data, "007*OK".getBytes());
				log.trace("a>>{}",a);
				log.trace("receive data>>{}",data);
				log.trace("string  data>>{}",new String(data));
			}
			byteBuffer.clear();
		}
		catch(Exception e){
			log.error("",e);
		}
		return data;
	}
	
	
	public FundResult FundRS_Check(byte[] rs) {
		FundResult fr = new FundResult();
		
		
		return null;
		
	}
	
	
	
	
	
	
	/**
	/**
	 * socket斷線
	 */
	public void close(SocketChannel socketChannel){
		try {
			socketChannel.close();
		}
		catch(Exception e) {
			log.error("",e);
		}
	}
	
	
	
	
	
	
}