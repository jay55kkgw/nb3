package com.netbank.telcomObject;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 電文LIST物件
 */
@XmlRootElement(name = "BLUESTAR")
@XmlType(propOrder = {"TITA","TOTA"})
public class TitaTotaList{
	private List<Tita> TITA = new ArrayList<Tita>();
	private List<Tota> TOTA = new ArrayList<Tota>();
	
	public List<Tita> getTITA(){
		return TITA;
	}
	public void setTITA(List<Tita> tITA){
		TITA = tITA;
	}
	public List<Tota> getTOTA(){
		return TOTA;
	}
	public void setTOTA(List<Tota> tOTA){
		TOTA = tOTA;
	}
}