package com.netbank.telcomObject;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 電文TOTA XML物件
 */
@XmlRootElement(name = "TOTA")
@XmlType(propOrder = {"TAGNAME","LENGTH"})
public class Tota{
	private String TAGNAME;
	private int LENGTH;
	private boolean NOPUT;
	private boolean DOFIRST;
	private boolean DOLAST;
	private boolean LOOP;
	private int STARTLOOPPOSITION;
	
	public String getTAGNAME(){
		return TAGNAME;
	}
	public void setTAGNAME(String tAGNAME){
		TAGNAME = tAGNAME;
	}
	public int getLENGTH(){
		return LENGTH;
	}
	public void setLENGTH(int lENGTH){
		LENGTH = lENGTH;
	}
	@XmlAttribute(name="NoPut")
	public boolean isNOPUT(){
		return NOPUT;
	}
	public void setNOPUT(boolean nOPUT){
		NOPUT = nOPUT;
	}
	@XmlAttribute(name="DoFirst")
	public boolean isDOFIRST(){
		return DOFIRST;
	}
	public void setDOFIRST(boolean dOFIRST){
		DOFIRST = dOFIRST;
	}
	@XmlAttribute(name="DoLast")
	public boolean isDOLAST(){
		return DOLAST;
	}
	public void setDOLAST(boolean dOLAST){
		DOLAST = dOLAST;
	}
	@XmlAttribute(name="Loop")
	public boolean isLOOP(){
		return LOOP;
	}
	public void setLOOP(boolean lOOP){
		LOOP = lOOP;
	}
	@XmlAttribute(name="StartLoopPosition")
	public int getSTARTLOOPPOSITION(){
		return STARTLOOPPOSITION;
	}
	public void setSTARTLOOPPOSITION(int sTARTLOOPPOSITION){
		STARTLOOPPOSITION = sTARTLOOPPOSITION;
	}
}