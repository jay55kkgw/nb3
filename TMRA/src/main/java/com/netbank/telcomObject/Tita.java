package com.netbank.telcomObject;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 電文TITA XML物件
 */
@XmlRootElement(name = "TITA")
@XmlType(propOrder = {"TAGNAME","LENGTH","TYPE"})
public class Tita{
	private String TAGNAME;
	private int LENGTH;
	private String TYPE;
	
	public String getTAGNAME(){
		return TAGNAME;
	}
	public void setTAGNAME(String tAGNAME){
		TAGNAME = tAGNAME;
	}
	public int getLENGTH(){
		return LENGTH;
	}
	public void setLENGTH(int lENGTH){
		LENGTH = lENGTH;
	}
	public String getTYPE(){
		return TYPE;
	}
	public void setTYPE(String tYPE){
		TYPE = tYPE;
	}
}