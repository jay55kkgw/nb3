package com.netbank.tmraObject;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import javax.resource.cci.IndexedRecord;
import javax.resource.cci.Streamable;

/**
 * 實作IndexedRecord和Streamable
 */
public class IndexedRecordStreamableImpl implements IndexedRecord,Streamable ,Serializable{
	private static final long serialVersionUID = -8175602348278848553L;
	private ArrayList<Object> list = new ArrayList<Object>();
	private String name;
	private String description;
	
	public void read(InputStream inputStream){
		
	}
	public void write(OutputStream outputStream){
		
	}
	
	public String getRecordName(){
		return name;
	}
	public void setRecordName(String name){
		this.name = name;
	}
	public void setRecordShortDescription(String description){
		this.description = description;
	}
	public String getRecordShortDescription(){
		return description;
	}
	public int size(){
		return list.size();
	}
	public boolean isEmpty(){
		return list.isEmpty();
	}
	public boolean contains(Object o){
		return list.contains(o);
	}
	public Iterator<Object> iterator(){
		return list.iterator();
	}
	public Object[] toArray(){
		return list.toArray();
	}
	public Object[] toArray(Object[] a){
		return list.toArray(a);
	}
	public boolean add(Object o){
		return list.add(o);
	}
	public boolean remove(Object o){
		return list.remove(o);
	}
	@SuppressWarnings("rawtypes")
	public boolean containsAll(Collection c){
		return list.containsAll(c);
	}
	@SuppressWarnings({"rawtypes","unchecked"})
	public boolean addAll(Collection c){
		return list.addAll(c);
	}
	@SuppressWarnings({"unchecked","rawtypes"})
	public boolean addAll(int index,Collection c){
		return list.addAll(index,c);
	}
	@SuppressWarnings("rawtypes")
	public boolean removeAll(Collection c){
		return list.removeAll(c);
	}
	@SuppressWarnings("rawtypes")
	public boolean retainAll(Collection c){
		return list.retainAll(c);
	}
	public void clear(){
		list.clear();
	}
	public Object get(int index){
		return list.get(index);
	}
	public Object set(int index,Object o){
		return list.set(index,o);
	}
	public void add(int index,Object o){
		list.add(index,o);
	}
	public Object remove(int index){
		return list.remove(index);
	}
	public int indexOf(Object o){
		return list.indexOf(o);
	}
	public int lastIndexOf(Object o){
		return list.lastIndexOf(o);
	}
	public ListIterator<Object> listIterator(){
		return list.listIterator();
	}
	public ListIterator<Object> listIterator(int index){
		return list.listIterator(index);
	}
	public List<Object> subList(int fromIndex,int toIndex){
		return list.subList(fromIndex,toIndex);
	}
	public Object clone() throws CloneNotSupportedException{
		throw new CloneNotSupportedException();
	}
}