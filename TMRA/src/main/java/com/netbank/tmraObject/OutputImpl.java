package com.netbank.tmraObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;

import org.apache.commons.io.IOUtils;

import lombok.extern.slf4j.Slf4j;
import com.netbank.util.CodeUtil;
import com.netbank.util.TelcomUtil;
/**
 * TMRA OUTPUT實作
 */
@Slf4j
public class OutputImpl extends IndexedRecordStreamableImpl{
	private static final long serialVersionUID = -8065384733307294906L;
	private byte[] outputBytes;
	
	/**
	 * READ實作
	 */
	public void read(InputStream inputStream){
		try{
			log.debug("read dateTime={}",new Date());
			
			InputStreamReader inputStreamReader = new InputStreamReader(inputStream,"CP937");
			
			outputBytes = IOUtils.toByteArray(inputStreamReader,"CP937");
			log.debug("outputBytes={}",outputBytes);
			String hexData = CodeUtil.ByteArrayToHexString(outputBytes);
			log.debug("hexData={}",hexData);
			
			outputBytes = TelcomUtil.padChineseCode(outputBytes);
			log.debug("outputBytes={}",outputBytes);
			hexData = CodeUtil.ByteArrayToHexString(outputBytes);
			log.debug("hexData={}",hexData);
			
			outputBytes = TelcomUtil.CP937ToBIG5(outputBytes);
			log.debug("outputBytes={}",outputBytes);
			hexData = CodeUtil.ByteArrayToHexString(outputBytes);
			log.debug("hexData={}",hexData);
		}
		catch(Exception e){
			log.error(String.valueOf(e));
		}
	}
	/**
	 * 取outputBytes值
	 */
	public byte[] getOutputBytes(){
		return outputBytes;
	}
}