package com.netbank.tmraObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.resource.ResourceException;
import javax.resource.cci.Connection;
import javax.resource.cci.ConnectionFactory;
import javax.resource.cci.Interaction;
import javax.resource.cci.InteractionSpec;

import org.joda.time.DateTime;

import com.ibm.connector2.ims.ico.IMSConnectionMetaData;
import com.ibm.connector2.ims.ico.IMSDFSMessageException;
import com.ibm.connector2.ims.ico.IMSInteractionSpec;
import com.ibm.connector2.ims.ico.IMSManagedConnectionFactory;
import com.netbank.rest.util.RESTUtil;
import com.netbank.util.DateUtil;
import com.netbank.util.HexDumpUtil;

import bank.comm.CommunicationException;
import fstop.util.MailUtil;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 核心程式
 */
@Slf4j
public class Core{
//	private static final Interaction interaction = null;
	private String imsDataStoreName;
	private String imsHostName;
	private Integer imsPortNumber;
	
	public static Integer socketTimeOut= 126;
	public static Integer executionTimeout=30;
	public static Integer executionTimeout2=125;
	public static Boolean isPreventQueue=Boolean.TRUE;
	
	public static String tmraEnv="P";
	public static String mails="sox@tbb.mail.com.tw";
	public static String ms_psChannel="http://msps:9080/ms_ps/com/";
	
	
	/**
	 * 建構子
	 */
	public Core(String imsDataStoreName,String imsHostName,Integer imsPortNumber){
		this.imsDataStoreName = imsDataStoreName;
		this.imsHostName = imsHostName;
		this.imsPortNumber = imsPortNumber;
	}
	
	
	
	
	/**
	 * N071等 走財經電文用
	 * @param inputImpl
	 * @param outputImpl
	 * @throws CommunicationException 
	 */
	public void execute(InputImpl inputImpl,OutputImpl2 outputImpl , Boolean isManaged ) throws CommunicationException{
		log.info("execute...");
		log.debug("inputImpl={}",inputImpl);
		log.debug("outputImpl={}",outputImpl);
//		boolean isManaged = Boolean.TRUE;
		InitialContext initialContext = null;
		ConnectionFactory connectionFactory = null;
		Connection connection = null;
		Interaction interaction = null;
//		IMSInteraction interaction = null;
		Boolean result = Boolean.FALSE;
		DateTime d1 = null;
		DateTime d2 = null;
		IMSConnectionMetaData  imm = null;
		String clientID ="";
		try{
			if(isManaged){
//				https://www.ibm.com/support/knowledgecenter/en/SSEPH2_13.1.0/com.ibm.ims13.doc.tmra/topics/rimssamplecci.htm
//				https://www.ibm.com/support/knowledgecenter/en/SSEPH2_13.1.0/com.ibm.ims13.doc.tmra/topics/timsconfigconnfactorywaslp.htm
				initialContext = new InitialContext();
				log.debug("initialContext={}",initialContext);
				
				connectionFactory = (ConnectionFactory)initialContext.lookup("ims/tmra");
				log.debug("connectionFactory={}",connectionFactory);
				
			}
			else{
				IMSManagedConnectionFactory imsManagedConnectionFactory = new IMSManagedConnectionFactory();
				log.debug("imsManagedConnectionFactory={}",imsManagedConnectionFactory);
				
				imsManagedConnectionFactory.setDataStoreName(imsDataStoreName);
				imsManagedConnectionFactory.setHostName(imsHostName);
				imsManagedConnectionFactory.setPortNumber(imsPortNumber);
				
				connectionFactory = (ConnectionFactory)imsManagedConnectionFactory.createConnectionFactory();
				log.debug("connectionFactory={}",connectionFactory);
			}
			
			connection = connectionFactory.getConnection();
			log.debug("connection={}",connection);
			interaction = connection.createInteraction();
//			IMSInteraction interaction = (IMSInteraction) connection.createInteraction();
			log.debug("interaction={}",interaction);
			IMSInteractionSpec imsInteractionSpec = new IMSInteractionSpec();
			log.debug("imsInteractionSpec={}",imsInteractionSpec);
//			20190425 add by hugo 根據IBM Carson 建議 有走財經的電文setCommitMode(0)
//			當Commit Mode 為0 時, IMS將會以 Client ID為 TPIPE name , IMS的跨行程式將會把這 TPIPE name及交易序號寫入資料庫,
//			待(電文)回來我們台企後, 再從資料庫內找出此交易序號及 TPIPE. 
//			imsInteractionSpec.setCommitMode(InteractionSpec.SYNC_SEND);
			
			imsInteractionSpec.setImsRequestType(IMSInteractionSpec.IMS_REQUEST_TYPE_IMS_TRANSACTION);
			imsInteractionSpec.setCommitMode(IMSInteractionSpec.COMMIT_THEN_SEND);
			imsInteractionSpec.setInteractionVerb(IMSInteractionSpec.SYNC_SEND_RECEIVE);
			
//			log.debug("InteractionSpec.SYNC_SEND>>{}",InteractionSpec.SYNC_SEND);
//			log.debug("imsInteractionSpec.COMMIT_THEN_SEND>>{}",imsInteractionSpec.COMMIT_THEN_SEND);
//			imsInteractionSpec.setInteractionVerb(InteractionSpec.SYNC_SEND_RECEIVE);
//			readTimeout 30秒
//			因跨行的setSocketTimeout 無效 所以統一在這邊設定為2分鐘
			imsInteractionSpec.setSocketTimeout(socketTimeOut*1000);
//			imsInteractionSpec.setSocketTimeout(((2*60)+6)*1000);
//			imsInteractionSpec.setSocketTimeout(35*1000);
			
//			參考連結https://www.ibm.com/support/knowledgecenter/SSEPH2_12.1.0/com.ibm.etools.ims.tmra.doc/topics/timssocketval.htm
//			ExecutionTimeout 才是redTimeOut
			log.debug("executionTimeout>>{}",executionTimeout);
			if(1 > executionTimeout) {
				
				imsInteractionSpec.setExecutionTimeout(1*10);
			}else {
				
				imsInteractionSpec.setExecutionTimeout(executionTimeout*1000);
			}
//			imsInteractionSpec.setExecutionTimeout(30*1000);
			log.debug("execute...");
			d1 = new DateTime();
			result = interaction.execute(imsInteractionSpec,inputImpl,outputImpl);
			
			log.debug("execute finsh");
			
	}
	catch(IMSDFSMessageException e){
		log.error("IMSDFSMessageException>>{}",String.valueOf(e));
		log.error("getDFSMessage>>{}",e.getDFSMessage());
		log.error("getErrorCode>>{}",e.getErrorCode());
		log.error("getLocalizedMessage>>{}",e.getLocalizedMessage());
		log.error("getDFSMessageSegments>>{}",e.getDFSMessageSegments());
		throw new CommunicationException("M1", "Z032","");
	} catch (ResourceException e) {
		log.error("ResourceException>>{}",e);
		log.error("getMessage>>{}",e.getMessage());
		log.error("getErrorCode>>{}",e.getErrorCode());
		log.error("getLocalizedMessage>>{}",e.getLocalizedMessage());
//20191209 by hugo  From TBB sox  針對Timeout 
		if(StrUtils.isNotEmpty(e.getMessage()) && e.getMessage().indexOf("ICO0113E") >=0) {
			throw new CommunicationException("M1", "Z032","");
		}else {
			throw new CommunicationException("M1", "Z020","");
		}
	} catch (NamingException e) {
		log.error("NamingException>>{}",e);
		throw new CommunicationException("M1", "Z020","");
	}
	catch(Exception e){
		log.error("Exception>>{}",String.valueOf(e));
		throw new CommunicationException("M1", "Z006","");
	}
	finally {
			if(d1 !=null) {
				log.warn("execute diffTime>>{}", DateUtil.getDiffTimeMills(d1, new DateTime()));
			}
			log.debug("TMRA.inputImpl>>{} , outputImpl>>{}", inputImpl, outputImpl);
			telLog(inputImpl, outputImpl);

			
			
			
			if(interaction != null){
				try{
					interaction.close();
				}
				catch(Exception e){
					log.error(String.valueOf(e));
				}
			}
			
			if (connection != null) {
//				紀錄ClientID
				try {
					imm = (IMSConnectionMetaData)connection.getMetaData();
					clientID = imm.getClientID();
					log.debug("clientID a>>{}",clientID);
				} catch (ResourceException e1) {
					log.warn("getClientID.ERROR>>{}",e1.toString());
				}
				
				try {
					connection.close();
				} catch (Exception e) {
					log.error(String.valueOf(e));
				}
			}
			if (initialContext != null) {
				try {
					initialContext.close();
				} catch (Exception e) {
					log.error(String.valueOf(e));
				}
			}
		}
		
	}
	
	
	/**
	 * 紀錄上下行電文內容
	 * @param inputImpl
	 * @param outputImpl
	 */
	public void telLog(InputImpl inputImpl,OutputImpl2 outputImpl) {
		
//		加入log.isXXXEnabled 原因 ，避免log層級改變時，HexDumpUtil.format還是有在work 消耗資源
		if(log.isInfoEnabled()) {
			if(inputImpl !=null && inputImpl.getInputBytes() !=null && inputImpl.getInputBytes().length>0) {
				log.info("inputImpl.Hex>>\n{}",HexDumpUtil.format(inputImpl.getInputBytes() ,72,HexDumpUtil.HD_EBCDIC) );
			}else {
				log.info("inputImpl is null");
			}
		}
		if(log.isDebugEnabled()) {
			if(outputImpl !=null && outputImpl.getOutputBytes() !=null && outputImpl.getOutputBytes().length >0) {
				log.debug("outputImpl.Hex>>\n{}",HexDumpUtil.format(outputImpl.getOutputBytes() ,72,HexDumpUtil.HD_EBCDIC) );
			}else {
				log.debug("outputImpl is null");
			}
		}
		if(log.isTraceEnabled()) {
			if(outputImpl!=null && outputImpl.getOutputBytes() !=null && outputImpl.getOutputBytes().length >0) {
				
				log.trace("outputImpl.Hex-4>>\n{}",HexDumpUtil.format(Arrays.copyOfRange(outputImpl.getOutputBytes(), 4, outputImpl.getOutputBytes().length) ,72,HexDumpUtil.HD_EBCDIC) );
			}else {
				log.trace("outputImpl is null");
			}
		}
	}
	
	
	
	public void execute2(InputImpl inputImpl,OutputImpl2 outputImpl , Boolean isManaged ) throws CommunicationException{
		log.info("execute2...");
		log.debug("inputImpl={}",inputImpl);
		log.debug("outputImpl={}",outputImpl);
//		boolean isManaged = false;
		InitialContext initialContext = null;
		ConnectionFactory connectionFactory = null;
		Connection connection = null;
		Interaction interaction = null;
		Boolean result = Boolean.FALSE;
		DateTime d1 = null;
		DateTime d2 = null;
		IMSConnectionMetaData  imm = null;
		String clientID ="";
		try{
			if(isManaged){
//				https://www.ibm.com/support/knowledgecenter/en/SSEPH2_13.1.0/com.ibm.ims13.doc.tmra/topics/rimssamplecci.htm
//				https://www.ibm.com/support/knowledgecenter/en/SSEPH2_13.1.0/com.ibm.ims13.doc.tmra/topics/timsconfigconnfactorywaslp.htm
				initialContext = new InitialContext();
				log.debug("initialContext={}",initialContext);
				connectionFactory = (ConnectionFactory)initialContext.lookup("ims/tmra");
				log.debug("connectionFactory={}",connectionFactory);
			}
			else{
				IMSManagedConnectionFactory imsManagedConnectionFactory = new IMSManagedConnectionFactory();
				log.debug("imsManagedConnectionFactory={}",imsManagedConnectionFactory);
				
				imsManagedConnectionFactory.setDataStoreName(imsDataStoreName);
				imsManagedConnectionFactory.setHostName(imsHostName);
				imsManagedConnectionFactory.setPortNumber(imsPortNumber);
				
				connectionFactory = (ConnectionFactory)imsManagedConnectionFactory.createConnectionFactory();
				log.debug("connectionFactory={}",connectionFactory);
			}
			
			connection = connectionFactory.getConnection();
			log.debug("connection={}",connection);
			
			interaction = connection.createInteraction();
			log.debug("interaction={}",interaction);
			IMSInteractionSpec imsInteractionSpec = new IMSInteractionSpec();
			log.debug("imsInteractionSpec={}",imsInteractionSpec);
//			20190425 add by hugo 根據IBM Carson 建議 有走財經的電文setCommitMode(0)
//			當Commit Mode 為0 時, IMS將會以 Client ID為 TPIPE name , IMS的跨行程式將會把這 TPIPE name及交易序號寫入資料庫,
//			待(電文)回來我們台企後, 再從資料庫內找出此交易序號及 TPIPE. 
//			20190516 hugo 原本的模式不適用財經通路(去跟回不同通路)
//			imsInteractionSpec.setCommitMode(InteractionSpec.SYNC_SEND);
//			imsInteractionSpec.setInteractionVerb(InteractionSpec.SYNC_SEND_RECEIVE);
//			readTimeout 30秒
//			result = interaction.execute(imsInteractionSpec,inputImpl,outputImpl);
			
			
			
			
			
//			防止Q
			if(isPreventQueue) {
				preventQueue(imsInteractionSpec, interaction, outputImpl );
			}
			
			
//			try {
//				Boolean ret = Boolean.FALSE;
//				int cnt = 0;
//				do {
//					cnt++;
//					ret = cnt>10 ? Boolean.FALSE:Boolean.TRUE;
//					log.trace("ret>>{}",ret);
//					imsInteractionSpec.setCommitMode(InteractionSpec.SYNC_SEND);
//					imsInteractionSpec.setInteractionVerb(IMSInteractionSpec.SYNC_SEND);
//					imsInteractionSpec.setInteractionVerb(IMSInteractionSpec.SYNC_RECEIVE_ASYNCOUTPUT_SINGLE_WAIT);
//					imsInteractionSpec.setSocketTimeout(1*1000);
//					imsInteractionSpec.setExecutionTimeout(1*800);
//					interaction.execute(imsInteractionSpec,null,outputImpl);
//				}while (!ret);
//			} catch (Exception e) {
//				log.warn("先行>>{}",e);
//			}
			
			imsInteractionSpec.setCommitMode(InteractionSpec.SYNC_SEND);
			imsInteractionSpec.setInteractionVerb(IMSInteractionSpec.SYNC_SEND);
			imsInteractionSpec.setSocketTimeout(socketTimeOut*1000);
			d1 = new DateTime();
			
			interaction.execute(imsInteractionSpec,inputImpl,null);
			imm = (IMSConnectionMetaData)connection.getMetaData();
			clientID = imm.getClientID();
			log.debug("clientID 0>>{}",clientID);
			
			imsInteractionSpec.setInteractionVerb(IMSInteractionSpec.SYNC_RECEIVE_ASYNCOUTPUT_SINGLE_WAIT);
//			此time out 是配合SYNC_RECEIVE_ASYNCOUTPUT_SINGLE_WAIT 財經timeout 是2分鐘
			imsInteractionSpec.setSocketTimeout(socketTimeOut*1000);
			imsInteractionSpec.setExecutionTimeout(executionTimeout2*1000);
			interaction.execute(imsInteractionSpec,null,outputImpl);
			
		}
		catch(IMSDFSMessageException e){
			log.error("IMSDFSMessageException>>{}",String.valueOf(e));
			log.error("getDFSMessage>>{}",e.getDFSMessage());
			log.error("getErrorCode>>{}",e.getErrorCode());
			log.error("getLocalizedMessage>>{}",e.getLocalizedMessage());
			log.error("getDFSMessageSegments>>{}",e.getDFSMessageSegments());
			throw new CommunicationException("M1", "Z032","");
		} catch (ResourceException e) {
			log.error("ResourceException>>{}",e);
			log.error("getMessage>>{}",e.getMessage());
			log.error("getErrorCode>>{}",e.getErrorCode());
			log.error("getLocalizedMessage>>{}",e.getLocalizedMessage());
			//20191209 by hugo  From TBB sox  針對Timeout 
			if(StrUtils.isNotEmpty(e.getMessage()) && e.getMessage().indexOf("ICO0113E") >=0) {
				throw new CommunicationException("M1", "Z032","");
			}else {
				throw new CommunicationException("M1", "Z020","");
			}
		} catch (NamingException e) {
			log.error("NamingException>>",e);
			throw new CommunicationException("M1", "Z020","");
		}
		catch(Exception e){
			log.error("Exception>>{}",String.valueOf(e));
			throw new CommunicationException("M1", "Z006","");
		}
		finally {
			if(d1 != null) {
				log.info("execute2 diffTime>>{}",DateUtil.getDiffTimeMills(d1, new DateTime()));
			}
			log.debug("TMRA.inputImpl>>{} , outputImpl>>{}",inputImpl,outputImpl);
			telLog(inputImpl, outputImpl);
			if(interaction != null){
				try{
					interaction.close();
				}
				catch(Exception e){
					log.error(String.valueOf(e));
				}
			}
			if(connection != null){
//				紀錄ClientID
				try {
					imm = (IMSConnectionMetaData)connection.getMetaData();
					clientID = imm.getClientID();
					log.debug("clientID 1>>{}",clientID);
				} catch (ResourceException e1) {
					log.warn("getClientID.ERROR>>{}",e1.toString());
				}
				try{
					connection.close();
				}
				catch(Exception e){
					log.error(String.valueOf(e));
				}
			}
			if(initialContext != null){
				try{
					initialContext.close();
				}
				catch(Exception e){
					log.error(String.valueOf(e));
				}
			}
		}
		
	}
	
	
	
	
	
	private void preventQueue(IMSInteractionSpec imsInteractionSpec ,Interaction interaction ,OutputImpl2 outputImpl ) {
		Boolean ret = Boolean.FALSE;
		
		int cnt = 0;
		List<OutputImpl2> list = null;
		try {
			list =  new LinkedList<OutputImpl2>();
			do {
				cnt++;
				ret = cnt > 10 ? Boolean.FALSE:Boolean.TRUE;
				log.trace("ret>>{}",ret);
				imsInteractionSpec.setCommitMode(InteractionSpec.SYNC_SEND);
				imsInteractionSpec.setInteractionVerb(IMSInteractionSpec.SYNC_RECEIVE_ASYNCOUTPUT_SINGLE_WAIT);
				imsInteractionSpec.setSocketTimeout(1*1000);
				imsInteractionSpec.setExecutionTimeout(1*800);
				interaction.execute(imsInteractionSpec,null,outputImpl);
				list.add(outputImpl);
//				list.add(String.valueOf(cnt));
			}while (ret);
			
		} catch (Exception e) {
			log.warn("preventQueue.error>>{}",e.toString());
		}finally {
			try {
				if(null != list && !list.isEmpty()) {
					for(OutputImpl2 out:list) {
						telLog(null, out);
					}
					sendMail(String.valueOf(list.size()), "TMRA");
				}
			} catch (Exception e) {
				log.warn("log Queue log error>>{}",e.toString());
			}
		}
	}
	
	
	public void sendMail(String cnt , String channel ) {
		HashMap<String, Object> map;
		String subject = "";
		String content = "";
		RESTUtil  restutil = null;
		MailUtil mailutil = null;
		List<String> mailList = new ArrayList<String>();
		try {
			mailutil = new MailUtil();
			log.info("mails>>{}",mails);
			mailList.add(mails);
			content = mailutil.getMailContentForQueue(cnt, channel, tmraEnv);
			switch (tmraEnv) {
			case "D":
				subject = "開發套新個網NB3GW異常通知";
				break;
			case "T":
				subject = "測試套新個網NB3GW異常通知";
				break;
			default:
				subject = "新個網NB3GW異常通知";
				break;
			}
			restutil = new RESTUtil();
			restutil.SendAdMail_REST(subject, content, mailList,"1" ,ms_psChannel);
				
		} catch (Exception e) {
			log.error("{}", e.toString());
		}
	}
}