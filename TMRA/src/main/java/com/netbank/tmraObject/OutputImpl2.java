package com.netbank.tmraObject;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.IOUtils;

import lombok.extern.slf4j.Slf4j;
import com.netbank.util.CodeUtil;
import com.netbank.util.TelcomUtil;
/**
 * TMRA OUTPUT實作
 */
@Slf4j
public class OutputImpl2 extends IndexedRecordStreamableImpl{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7579307274418774208L;
	private byte[] outputBytes;
	
	
	
	
	/**
	 * READ實作
	 */
	public void read(InputStream inputStream){
		try{
			log.debug("read dateTime={}",new Date());
			outputBytes = IOUtils.toByteArray(inputStream);
//			log.debug("Hex.encodeHexStringxx>>{}",Hex.encodeHexString(outputBytes));
//			
			log.debug("outputBytes={}",outputBytes);
			if(outputBytes !=null) {
				log.debug("outputBytes.length={}",outputBytes.length);
			}
			String hexData = CodeUtil.ByteArrayToHexString(outputBytes);
//			log.debug("hexData={}",hexData);
//			log.debug("outputBytes.string={}",CodeUtil.EBCDICtoBig5(outputBytes));
			
		}
		catch(Exception e){
			log.error(String.valueOf(e));
		}
	}
	
//	public void read(InputStream inputStream){
//		try{
//			log.debug("read dateTime={}",new Date());
//			InputStreamReader inputStreamReader = new InputStreamReader(inputStream,"CP937");
//			log.debug("getEncoding={}",inputStreamReader.getEncoding());
//			outputBytes = IOUtils.toByteArray(inputStreamReader,"CP937");
//			log.debug("outputBytes={}",outputBytes);
//			log.debug("outputBytes.length={}",outputBytes.length);
//			String hexData = CodeUtil.ByteArrayToHexString(outputBytes);
//			log.debug("hexData={}",hexData);
//			log.debug("outputBytes.string={}",CodeUtil.EBCDICtoBig5(outputBytes));
//			
////			outputBytes = TelcomUtil.padChineseCode(outputBytes);
////			log.debug("outputBytes={}",outputBytes);
////			hexData = CodeUtil.ByteArrayToHexString(outputBytes);
////			log.debug("hexData={}",hexData);
////			
////			outputBytes = TelcomUtil.CP937ToBIG5(outputBytes);
////			log.debug("outputBytes={}",outputBytes);
////			hexData = CodeUtil.ByteArrayToHexString(outputBytes);
////			log.debug("hexData={}",hexData);
//		}
//		catch(Exception e){
//			log.error(String.valueOf(e));
//		}
//	}
	/**
	 * 取outputBytes值
	 */
	public byte[] getOutputBytes(){
		return outputBytes;
	}
}