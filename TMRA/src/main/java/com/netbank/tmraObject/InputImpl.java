package com.netbank.tmraObject;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * TMRA INPUT實作
 */
@Slf4j
@Data
public class InputImpl extends IndexedRecordStreamableImpl{
	private static final long serialVersionUID = -3437554112055087444L;
	private byte[] inputBytes;
	
	/**
	 * READ實作
	 */
	public void read(InputStream inputStream){
		try{
			log.debug("inputBytes={}",inputBytes);
			log.debug("read dateTime={}",new Date());
			
			inputStream.read(inputBytes);
		}
		catch(Exception e){
			log.error(String.valueOf(e));
		}
	}

	/**
	 * WRITE實作
	 */
	public void write(OutputStream outputStream){
		try{
			log.debug("inputBytes={}",inputBytes);
			log.debug("write dateTime={}",new Date());
			
			outputStream.write(inputBytes);
		}
		catch(Exception e){
			log.error(String.valueOf(e));
		}
	}
	/**
	 * inputBytes給值
	 */
//	public void setInputBytes(byte[] inputBytes){
//		this.inputBytes = inputBytes;
//	}
//	public void setInputBytes(byte[] inputBytes){
//		this.inputBytes = inputBytes;
//	}
}