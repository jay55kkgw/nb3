package com.netbank.rest.bean;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Value;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class BaseRestBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7558266676532828276L;
	
	private String LOGINTYPE ="NB";
	private String TRXCOD;
	private String FILL01;
	private String TXID;
	private String VERSION;
	private String msgCode;
	private String msgName;
	private String TOPMSG;
//	private static String ms_Channel;
//	@Value("$ms_psChannel")
//	static String ms_Channel;
	

	
	
	
	
//	public static String getAPI_Name(Class<?> retClass) {
//		  if(ms_Channel.endsWith("/") == false) {
//		    	ms_Channel = ms_Channel + "/";
//		    }
//		return ms_Channel + retClass.getSimpleName().replace("_REST_RQ", "");
//	}
//	
	
	

}
