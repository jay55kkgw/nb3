package com.netbank.rest.bean;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import bank.comm.StringUtil;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
@Data
@Slf4j
public class BaseRestBean_PS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2121405776126678393L;

	
	public  String ms_Channel  ;

	public  String getAPI_Name(Class<?> retClass) {
		if (ms_Channel.endsWith("/") == false) {
			ms_Channel = ms_Channel + "/";
		}
		return ms_Channel + retClass.getSimpleName().replace("_REST_RQ", "");
	}

	public  String getAPI_Name(Class<?> retClass, String _ms_Channel) {

		if (StringUtil.isNotEmpty(_ms_Channel)) {
			if (_ms_Channel.endsWith("/") == false) {
				_ms_Channel = _ms_Channel + "/";
			}
			return _ms_Channel + retClass.getSimpleName().replace("_REST_RQ", "");
		}
		return getAPI_Name(retClass);
	}
}
