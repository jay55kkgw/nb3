package com.netbank.rest.web.context;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

public class UserContext {

    public static ThreadLocal<HttpServletRequest> requestT = ThreadLocal.withInitial(() -> null);
    public static ThreadLocal<HttpServletResponse> responseT = ThreadLocal.withInitial(() -> null);
    public static ThreadLocal<String> browserIp = ThreadLocal.withInitial(() -> "");

    public static ThreadLocal<LoginBean> loginBeanThreadLocal = ThreadLocal.withInitial(() -> null);

    public static Optional<HttpServletRequest> getRequest() {
        return Optional.ofNullable(requestT.get());
    }

    public static void setRequest(HttpServletRequest requestT) {
        UserContext.requestT.set(requestT);
    }

    public static Optional<HttpServletResponse> getResponse() {
        return Optional.ofNullable(responseT.get());
    }

    public static void setResponse(HttpServletResponse responseT) {
        UserContext.responseT.set(responseT);
    }

    public static Optional<LoginBean> getLoginBean() {
        return Optional.ofNullable(loginBeanThreadLocal.get());
    }

    public static void setLoginBean(LoginBean loginBean) {
        UserContext.loginBeanThreadLocal.set(loginBean);
    }

    public static boolean hasLoginBean() {
        return UserContext.getLoginBean() != null;
    }

    public static String browserIp() {
        return UserContext.browserIp.get();
    }



}
