package com.netbank.rest.web.support.xml;

import com.thoughtworks.xstream.XStream;

public class XmlMarshaller<T> {
    private XStream xstream = new ScsbXStream();

    public XmlMarshaller(Class<T> type) {
        this.xstream.processAnnotations(type);
        this.xstream.alias("ibank", type);
    }

    public T parseXML(String xml) {
        return (T)this.xstream.fromXML(xml);
    }
}
