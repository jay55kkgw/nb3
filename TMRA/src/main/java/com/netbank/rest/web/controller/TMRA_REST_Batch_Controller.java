package com.netbank.rest.web.controller;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.netbank.rest.comm.batch.BatchResult;
import com.netbank.rest.comm.batch.FindFileChange;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.util.JSONUtils;
import fstop.util.SpringBeanFactory;
import lombok.extern.slf4j.Slf4j;

/**
 *
 */
@RestController
@RequestMapping(value = "/batch/com")
@Slf4j
public class TMRA_REST_Batch_Controller {

	@Autowired
	ServletContext servletContext;

	@Autowired
	FindFileChange findfilechange;

	@PostConstruct
	public void init() {

		// 註冊到 SpringBeanFactory
		SpringBeanFactory.setApplicationContext(WebApplicationContextUtils.getWebApplicationContext(servletContext));
	}

	@RequestMapping(value = "/findfilechange", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult doAction(@RequestBody List<String> args)
			throws JsonProcessingException {
		Boolean result = Boolean.FALSE;
		log.info("args: {}",ESAPIUtil.vaildLog(CodeUtil.toJson(args)) );
		BatchResult br = new BatchResult();
		br.setSuccess(Boolean.FALSE);
		br.setErrorCode("FE0009");
		br = findfilechange.findChange(servletContext.getContextPath());
		
		return br;

	}
	
	@RequestMapping(value = "/echo", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
//	@Scope("request")
	public HashMap<String, Object> echo(@RequestBody List<String> args) throws JsonProcessingException {
		
		log.info(" args: {}", ESAPIUtil.vaildLog(JSONUtils.toJson(args) ));
		HashMap<String, Object> retMap = new HashMap<String, Object>();
//		TODO 測試用
//		try {
//			Thread.sleep(1*1000);
//		} catch (InterruptedException e) {
//			log.error("{}",e.toString());
//		}
		retMap.put("isSuccess", Boolean.TRUE);
		retMap.put("batchName", "echo");
		
		return retMap;
		
	}
	
	
	private List getD4ERR() {
		List list = new LinkedList<>();
		Map<String,String> map = new HashMap<String,String>();
		map.put("TOMMSG", "FE0003");
		list.add(map);
		return list;
	}

}
