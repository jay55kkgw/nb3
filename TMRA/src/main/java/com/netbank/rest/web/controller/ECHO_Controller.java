package com.netbank.rest.web.controller;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;

import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.netbank.rest.service.REST_Service;
import com.netbank.tmraObject.Core;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.orm.dao.AdmNbStatusDao;
import fstop.util.JSONUtils;
import fstop.util.SpringBeanFactory;
import lombok.extern.slf4j.Slf4j;

/**
 *
 */
@RestController
@RequestMapping(value = "/hi")
@Slf4j
public class ECHO_Controller {

    @Autowired
    ServletContext servletContext;

    @Autowired
    REST_Service rest_service;
    
    @Autowired
    AdmNbStatusDao admNbStatusDao;

//  https://www.jianshu.com/p/1aa662a5f170
//  這方式雖可行 但遇到難字放在環境變數 會出現亂碼 導致parse異常
  @Value("#{${hardWordMap}}")
  Map<String,String> hardWordMap;
//  @Value("{${hardWordMap}")
//  String hardWordMap;
    
    
    @PostConstruct
    public void init() {

        //註冊到 SpringBeanFactory
        SpringBeanFactory.setApplicationContext(WebApplicationContextUtils.getWebApplicationContext(servletContext));
        log.info("ContextPath >> {}",servletContext.getContextPath());
        
        try {
        	log.trace("hardWordMap>>{}",hardWordMap);
			CodeUtil.hardWordMap =hardWordMap ;
//			CodeUtil.hardWordMap =CodeUtil.fromJson(hardWordMap, Map.class) ;
			log.trace("CodeUtil.hardWordMap>>{}",CodeUtil.hardWordMap);
		} catch (Throwable e) {
			log.error("get hardWordMap.error>>{}",e);
		}
        
        admNbStatusDao.setServerStatus(servletContext.getContextPath().replace("/", "").toUpperCase());
    }


//    @RequestMapping(value = "/{serviceID}")
//    @ResponseBody
//    public Map<String,Object> doAction(@PathVariable String serviceID, @RequestParam HashMap request) throws JsonProcessingException {
//
//        log.info("service id: {} - request: {}", ESAPIUtil.vaildLog(serviceID) , ESAPIUtil.vaildLog(JSONUtils.toJson(request)));
//
////        TODO 測試用
//        Map map = new HashMap<String,Object>();
//
//        map.put("hi", "okok");        
//        
//        return map;
//        
//
//    }
//    
    
    
    @RequestMapping(value = "/env/chg", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
    public Map<String,Object> changeEnv(@RequestBody HashMap request) throws JsonProcessingException {
    	
    	log.info(" env/chg/rq  {}" , ESAPIUtil.vaildLog(JSONUtils.toJson(request)));
    	Core.socketTimeOut = (Integer) request.get("socketTimeOut");
    	Core.executionTimeout = (Integer) request.get("executionTimeout");
    	Core.executionTimeout2 = (Integer) request.get("executionTimeout2");
    	Core.isPreventQueue = (Boolean) request.get("isPreventQueue");
    	request.clear();
//    	Reflected XSS All Clients
//    	request.put("socketTimeOut", StringEscapeUtils.escapeHtml(String.valueOf(Core.socketTimeOut)));
//    	request.put("executionTimeout",StringEscapeUtils.escapeHtml(String.valueOf(Core.executionTimeout)));
//    	request.put("executionTimeout2",StringEscapeUtils.escapeHtml(String.valueOf(Core.executionTimeout2)));
//    	request.put("isPreventQueue", StringEscapeUtils.escapeHtml(String.valueOf(Core.isPreventQueue)));
//    	Reflected XSS All Clients
    	HashMap<String,Object> retMap = new HashMap<String,Object>();
    	retMap.put("socketTimeOut", StringEscapeUtils.escapeHtml(String.valueOf(Core.socketTimeOut)));
    	retMap.put("executionTimeout",StringEscapeUtils.escapeHtml(String.valueOf(Core.executionTimeout)));
    	retMap.put("executionTimeout2",StringEscapeUtils.escapeHtml(String.valueOf(Core.executionTimeout2)));
    	retMap.put("isPreventQueue", StringEscapeUtils.escapeHtml(String.valueOf(Core.isPreventQueue)));
    	
    	return retMap;
    	
    	
    }

}
