package com.netbank.rest.web.controller;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;

import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.netbank.rest.service.REST_Service;
import com.netbank.rest.service.Interface.TelcomServiceInterface;
import com.netbank.util.ESAPIUtil;

import fstop.util.JSONUtils;
import fstop.util.SpringBeanFactory;
import lombok.extern.slf4j.Slf4j;

/**
 *
 */
@RestController
@RequestMapping(value = "/com")
@Slf4j
public class TMRA_REST_Controller {

	@Autowired
	ServletContext servletContext;

	@Autowired
	@Qualifier("telcomModule_Service")
	TelcomServiceInterface service;

	@PostConstruct
	public void init() {

		// 註冊到 SpringBeanFactory
		SpringBeanFactory.setApplicationContext(WebApplicationContextUtils.getWebApplicationContext(servletContext));
	}

	@RequestMapping(value = "/{serviceID}", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public List doAction(@PathVariable String serviceID, @RequestBody HashMap<String, String> request)
			throws JsonProcessingException {
		Boolean result = Boolean.FALSE;
		List list = null;
		String msg = "";
		log.info("service id: {} - request: {}", ESAPIUtil.vaildLog(serviceID), ESAPIUtil.vaildLog(JSONUtils.toJson(request)));
		try {
			serviceID = serviceID.toUpperCase();
			serviceID = ESAPIUtil.validInput(serviceID, "GeneralString", true);
			log.info("service id.toUpperCase: {}",ESAPIUtil.vaildLog(serviceID) );
//			log.info("service id.escapeHtml: {}", StringEscapeUtils.escapeHtml(serviceID) );
			msg = (String) ESAPIUtil.escapeHTML(serviceID);
			list = service.doTelcomModule(msg, request);
		} catch (Exception e) {
			log.error("doAction.Exception>>{}",e);
			list = getD4ERR();
		}
//		if (result) {
//			list = service.doTelcomModule(serviceID, request);
//		} else {
//			list = getD4ERR();
//		}
		log.trace("list>>{}", list);
		return list;

	}
	
	private List getD4ERR() {
		List list = new LinkedList<>();
		Map<String,String> map = new HashMap<String,String>();
		map.put("TOMMSG", "FE0003");
		list.add(map);
		return list;
	}

}
