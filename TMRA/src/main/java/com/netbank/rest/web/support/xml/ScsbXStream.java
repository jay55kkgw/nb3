package com.netbank.rest.web.support.xml;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.XStreamException;
import com.thoughtworks.xstream.io.xml.Xpp3Driver;
import com.thoughtworks.xstream.mapper.MapperWrapper;
import java.lang.reflect.Field;
import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ScsbXStream extends XStream {
    private static final Logger LOG = LoggerFactory.getLogger(ScsbXStream.class);
    private boolean strictMode;
    private boolean includeFailingXml;

    public ScsbXStream() {
        super(new Xpp3Driver(new LowerCaseXmlNameCoder()));
        this.setStrictMode(false);
        this.setIncludeFailingXml(true);
    }

    public boolean isStrictMode() {
        return this.strictMode;
    }

    public void setStrictMode(boolean strictMode) {
        this.strictMode = strictMode;
    }

    public boolean isIncludingFailingXml() {
        return this.includeFailingXml;
    }

    public void setIncludeFailingXml(boolean includeFailingXml) {
        this.includeFailingXml = includeFailingXml;
    }

    protected MapperWrapper wrapMapper(MapperWrapper next) {
        return new MapperWrapper(next) {
            public boolean shouldSerializeMember(Class definedIn, String fieldName) {
                if (ScsbXStream.this.strictMode) {
                    return super.shouldSerializeMember(definedIn, fieldName);
                } else if (definedIn == Object.class) {
                    return false;
                } else {
                    Field[] fields = definedIn.getDeclaredFields();
                    Field[] var4 = fields;
                    int var5 = fields.length;

                    for (int var6 = 0; var6 < var5; ++var6) {
                        Field field = var4[var6];
                        if (field.getName().equalsIgnoreCase(fieldName)) {
                            return true;
                        }
                    }

                    return this.shouldSerializeMember(definedIn.getSuperclass(), fieldName);
                }
            }

            public String attributeForAlias(String alias) {
                return alias.toLowerCase();
            }
        };
    }

    public Object fromXML(String xml) {
        LOG.trace("Receiving XML: " + xml);

        try {
            return super.fromXML(xml);
        } catch (XStreamException var3) {
            throw new XStreamException(StringEscapeUtils.escapeHtml(xml), var3);
        }
    }
}