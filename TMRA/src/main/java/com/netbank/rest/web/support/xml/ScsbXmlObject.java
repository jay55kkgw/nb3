package com.netbank.rest.web.support.xml;

import com.thoughtworks.xstream.annotations.XStreamAlias;

public class ScsbXmlObject {

    @XStreamAlias("txretmsg")
    private ScsbXmlObject.TxRetMsg error;
    @XStreamAlias("txretflg")
    private String returnFlag;
    @XStreamAlias("etxretflg")
    private String returnFlag2;

    public ScsbXmlObject() {
    }

    public String getReturnMessage() {
        return this.error != null && this.error.errorMessage != null ? this.error.errorMessage : null;
    }

    public String getReturnCode() {
        if (this.error != null && this.error.errorCode != null) {
            String[] split = this.error.errorCode.split(":");
            return split.length > 1 ? split[1] : this.error.errorCode;
        } else {
            return null;
        }
    }

    public String getReturnFlag() {
        return this.returnFlag != null ? this.returnFlag : this.returnFlag2;
    }

    public String getReturnCodeRaw() {
        return this.error != null && this.error.errorCode != null ? this.error.errorCode : null;
    }

    public String getErrorParam() {
        if (this.error != null) {
            if (this.error.errorParam0 != null) {
                return this.error.errorParam0;
            }

            if (this.error.errorParam1 != null) {
                return this.error.errorParam1;
            }
        }

        return null;
    }

    public boolean hasErrors() {
        return this.returnFlag != null && "1".equals(this.returnFlag.trim()) || this.returnFlag2 != null && "1".equals(this.returnFlag2.trim());
    }

    public class TxRetMsg {
        @XStreamAlias("errormsg")
        private String errorMessage;
        @XStreamAlias("errorcode")
        private String errorCode;
        @XStreamAlias("errorparam0")
        private String errorParam0;
        @XStreamAlias("errorparam1")
        private String errorParam1;

        public TxRetMsg() {
        }
    }
}
