package com.netbank.rest.web.support.xml;

import com.thoughtworks.xstream.io.xml.XmlFriendlyNameCoder;

public class LowerCaseXmlNameCoder extends XmlFriendlyNameCoder {

    public LowerCaseXmlNameCoder() {}

    public String decodeNode(String elementName) {
        return super.decodeNode(elementName).toLowerCase();
    }
}
