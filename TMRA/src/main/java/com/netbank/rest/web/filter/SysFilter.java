package com.netbank.rest.web.filter;

import com.netbank.rest.web.context.UserContext;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Slf4j
public class SysFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        log.info("into to sysfilter ...");
        try {
            UserContext.setRequest((HttpServletRequest)servletRequest);
            UserContext.setResponse((HttpServletResponse)servletResponse);

            HttpSession session = UserContext.getRequest().get().getSession();

            HttpServletRequest request = (HttpServletRequest)servletRequest;
            HttpServletResponse response = (HttpServletResponse)servletResponse;
//            String version = request.getHeader("app-version");
//            log.info("app-version: " + version);

//            if(StrUtils.isEmpty(version)) {
//                // 回傳錯誤
//                // 回傳 EmptyAppVersion
//                MessageResponse msgResponse = new MessageResponse();
//                msgResponse.setMsgCode("EmptyAppVersion");
//                msgResponse.setMessage("");
//                returnErrorToCLient(response, msgResponse);
//
//                return;
//            }

            try {
//                Version currentVersion = new Version("3.1.1");
//                Version appVersion = new Version(version);

//                if (currentVersion.compareTo(appVersion) > 0) {
                    // 回傳 ErrorVersion
//                    MessageResponse msgResponse = new MessageResponse();
//                    msgResponse.setMsgCode("ErrorVersion");
//                    msgResponse.setMessage("APP版本錯誤");
//
//                    returnErrorToCLient(response, msgResponse);

//                    return;
//                }
            } catch (Exception e) {
                log.error("版本比對錯誤.", e);
            }
//
//            String account = (String)session.getAttribute(Constraint.LOGIN_SESSION_KEY);
//            String opAccount = (String)session.getAttribute(Constraint.OPLOGIN_SESSION_KEY);
//            UserContext.browserIp.set(servletRequest.getRemoteAddr());
//
//            log.info("browser ip: " + servletRequest.getRemoteAddr());
//            if(StrUtils.isNotEmpty(account) || StrUtils.isNotEmpty(opAccount)) {
//                UserContext.setLoginBean(LoginBean.builder()
//                        .account(account)
//                        .opAccount(opAccount)
//                        .build());
//            }


            filterChain.doFilter(servletRequest, servletResponse);
        }
        finally {
            UserContext.setRequest(null);
            UserContext.setResponse(null);
            UserContext.setLoginBean(null);
        }
    }

    @Override
    public void destroy() {

    }
//
//    private void returnErrorToCLient(HttpServletResponse res, MessageResponse msgResponse) {
//        res.setContentType("application/json; charset=utf-8");
//
//        OutputStream o = null;
//        try {
//            o = res.getOutputStream();
//
//            Gson gson = new GsonBuilder().setPrettyPrinting().create();
//            String json = gson.toJson(msgResponse);
//            o.write(json.getBytes("UTF-8"));
//            o.flush();
//        }
//        catch(Exception e) {
//            e.printStackTrace();
//        }
//        finally {
//            try {
//                if(o != null)
//                    o.close();
//            }
//            catch(Exception e) {}
//        }
//    }

}