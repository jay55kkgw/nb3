package com.netbank.rest.comm.batch;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.netbank.rest.bean.Sendadmail_REST_RQ;
import com.netbank.rest.bean.Sendadmail_REST_RS;
import com.netbank.rest.util.RESTUtil;
import com.netbank.util.BaseResult;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;
import com.netbank.util.ResultCode;

import fstop.orm.dao.AdmMsgCodeDao;
import fstop.orm.dao.SysParamDataDao;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class FindFileChange {

	@Autowired
	SysParamDataDao sysParamDataDao;
	@Autowired
    private AdmMsgCodeDao admMsgCodeDao;
	
	@Autowired
	RESTUtil restutil;
	
	DateTime initdate ;
	
	@Value("${scanPath:/opt/ibm/wlp/usr/servers/defaultServer/apps/expanded/}")
	String scanPath ="";
	@Value("${ms_psChannel}")
	String ms_Channel ="";
//	@Autowired
//	private Notifier notifier;
	
	
	/**
	 * 初始化基本日期時間，來當作基準
	 */
	@PostConstruct
	public void init() {
		initdate = new DateTime();
		log.info("initdate>>{}",initdate.toString("yyyy-MM-dd HH:mm:ss"));
	}
	  
	public BatchResult findChange(String contextPath) {
//		String nb3path = "D:\\PROJ\\ox-wk-dev\\ms_batch_main\\src\\main\\java\\fstop\\batch\\main\\FindFileChange.java";
//		DateTime date = new DateTime();
//		DateTime before1H = date.minusHours(1);
//		File file = new File(nb3path);
//		DateTime filedate = new DateTime(file.lastModified());
//		log.info("filedate>>{}", filedate.toString("yyyy-MM-dd HH:mm:ss"));
//		isRange(filedate, 1);
		BatchResult br = new  BatchResult();
		br.setSuccess(Boolean.FALSE);
		br.setErrorCode("FE0009");
		contextPath = contextPath.replace("/", "").trim().toUpperCase();
//		was 路徑
//		/opt/IBM/WebSphere/AppServer/profiles/AppSrv01/installedApps/DefaultCell01/nnb-ear.ear
//		liberty 路徑
//		/opt/ibm/wlp/usr/servers/defaultServer/apps/expanded/
//		scanPath = "D:\\PROJ\\ox-wk-dev\\ms_batch_main\\src\\";
		List<String> list = new LinkedList<String>();
		listDir(new File(scanPath), list);
		
		if(list!=null && !list.isEmpty()) {
			sendMail(contextPath, list);
		}
		br.setSuccess(Boolean.TRUE);
		br.setErrorCode("0");
//		HashMap<String,Object> map = new HashMap<String,Object>();
//		map.put("result", Boolean.TRUE);
		return br;
	}
	   
	
	public void sendMail(String contextPath ,List<String> list) {
//		NotifyAngent.
		HashMap<String, Object> map;
		String subject = contextPath + "檔案異動";
		String content = "";
		String mails = "";
		
		List<String> mailList = new ArrayList<String>();
		try {
			if (list != null && !list.isEmpty()) {
//				mails = getAPmail();
				log.info("mails>>{}",mails);
				mailList.add(mails);
				content = getMailContent(list, contextPath);
				SendAdMail_REST(subject, content, mailList,"1");
				
				log.info("mails>>{}",mails);
				for(String mail :mailList) {
//					notifier.sendmsg(mail, subject, content, null);
				}
			}
		} catch (Exception e) {
			log.error("{}", e.toString());
		}
	}
	
	
	public String getMailContent(List<String> list,String contextPath) {
		StringBuffer sb = new StringBuffer();
		sb.append("<html>\n<head>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF8\" />\n");
		sb.append("<title>新個網檔案異動通知</title>\n");
		sb.append("</head>\n");
		sb.append("<body>\n");
		sb.append("主機名稱:"+ contextPath+"<BR>");
		sb.append("檔案異動清單:"+ CodeUtil.toJson(list));
		sb.append("</body>\n");
		sb.append("</html>\n");
		log.info("sb>>{}",sb.toString());
		
		return sb.toString();
	}
	
	
	public Boolean isRange(DateTime filedate, Integer range) {
		Boolean result = Boolean.FALSE;
//		DateTime date = new DateTime();
//		date = date.minusHours(range);
		log.debug("base date time>>{}",initdate.toString("yyyy-MM-dd HH:mm:ss"));
		DateTime date = initdate.minusMinutes(range);
		log.info("filedate>>{}", filedate.toString("yyyy-MM-dd HH:mm:ss"));
		result = filedate.isAfter(date);
		log.info("isRange>>{}", result);
		return result;

	}
	
	
	/**
	 * https://www.itread01.com/content/1547205846.html
	 * @param file
	 */
	public void listDir(File file , List<String> list ) {
//		List<String> list = new LinkedList<String>();
		
		log.trace("file.getName()>>{}",file.getName());
		if (file.isDirectory()) { // 是一個目錄
			// 列出目錄中的全部內容
			File results[] = file.listFiles();
			if (results != null) {
				for (int i = 0; i < results.length; i++) {
					listDir(results[i] ,list); // 繼續一次判斷
				}
			}
		} else { // 是檔案
			
			DateTime filedate = new DateTime(file.lastModified());
			if(isRange(filedate, 1)) {
				list.add(file.getPath());
			}
		}
		// file.delete(); //刪除!!!!! 根目錄,慎操作
		// 獲取完整路徑
		log.info("list>>{}",list);
	}
	
//	public String getAPmail() {
//		SYSPARAMDATA po = sysParamDataDao.findById("NBSYS");
//		return  po.getADAPMAIL();
//	}
	
	
	public BaseResult SendAdMail_REST(String Subject,String Content,List<String> Receivers,String contentType) {
        log.trace("SendAdMail_REST");
        BaseResult bs = null;
        Sendadmail_REST_RQ rq = null;
        Sendadmail_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new Sendadmail_REST_RQ();
            rq.setSubject(Subject);
            rq.setContent(Content);
            rq.setReceivers(Receivers);
            rq.setContentType(contentType);
            rq.setMs_Channel(ms_Channel);
            rs = restutil.send(rq, Sendadmail_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace("SendAdMail_RS>>{}", rs);
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("SendAdMail_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }
	
	/**
     * 根據訊息代碼填入訊息
     * 
     * @param bs
     */
    public void getMessageByMsgCode(BaseResult bs) {
        log.trace("bs>>{}", bs);
        String msg = "";
        String msgCode = "";
        if (bs != null) {
            msg = bs.getMessage();
            msgCode = bs.getMsgCode();
            log.trace("r>>{}", msg);
            log.trace("MsgCode>>{}", msgCode);
            if ("0".equals(bs.getMsgCode())) {
                log.trace("MsgCode is 0  return..");
                return;
            }
            
            msg = getMessageByMsgCode(bs.getMsgCode());
            bs.setMessage(msgCode, msg);
            log.trace("msg>>{}", ESAPIUtil.vaildLog(bs.getMessage()) );
           
        }
    }
	
    public String getMessageByMsgCode(String msgCode) {
        String message = "";
        try {
        	
            log.trace("admMsgCodeDao>>{}", admMsgCodeDao);
            if (admMsgCodeDao.hasError(msgCode)) {
            	message = admMsgCodeDao.getErrorCodeMsg( msgCode);
            }
        } catch (Exception e) {
            log.error( e.toString());
        }
        log.trace("message>>{}", ESAPIUtil.vaildLog(message));
        return message;
    }
}
