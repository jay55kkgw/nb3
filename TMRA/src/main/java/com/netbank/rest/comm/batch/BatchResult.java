package com.netbank.rest.comm.batch;

import lombok.Data;

import java.util.Map;

@Data
public class BatchResult {
	public String batchName;

	public boolean isSuccess;

	public String errorCode;

	public Map<String, Object> data;
	
}
