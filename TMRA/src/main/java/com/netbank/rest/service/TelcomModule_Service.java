/**
 * 
 */
package com.netbank.rest.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.netbank.rest.service.Interface.TelcomServiceInterface;
import com.netbank.telcomm.FXTransactionImpl;
import com.netbank.telcomm.FundTransactionImpl;
import com.netbank.telcomm.PARKTransactionImpl;
import com.netbank.telcomm.TMRATransactionImpl;
import com.netbank.util.DateUtil;
import com.netbank.util.XmlUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 功能說明 :
 * 電文模組服務
 */
@Service
@Slf4j
public class TelcomModule_Service implements TelcomServiceInterface {
	@Autowired
	TMRATransactionImpl tmra ;
	@Autowired
	FundTransactionImpl fund ;
	@Autowired
	FXTransactionImpl fx ;
	@Autowired
	PARKTransactionImpl park ;
	
	
	@Value("${imsDataStoreName}")
	private String imsDataStoreName;
	@Value("${imsHostName}")
	private String imsHostName;
	@Value("${imsPortNumber}")
	private Integer imsPortNumber;
	@Value("${xmlFilePath}")
	private String xmlFilePath;
	
	
	@Override
	public Map<String, Object> doTelcom(Map<String, String> parameterMap) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List doTelcomModule(String txid ,Map<String, String> params) {
		List list =  null;
		
		String key = getChanel(txid);
		try {
			log.trace("key>>{}",key);
			params.put(fx.FXTCPIP_KEY, key);
			params.put(fx.FXMTCPIP_KEY, key);
			switch (key) {
			case "FUND":
				list = fund.processTransaction(txid, params);
				break;
			case "PARKING":
				list = park.processTransaction(txid, params);
				break;
			case "FXQTCPIP":
				list = fx.processTransaction(txid, params);
				break;
			case "FXUTCPIP":
				list = fx.processTransaction(txid, params);
				break;
			case "FXMTCPIP":
				list = fx.processTransaction(txid, params);
				break;

			default:
				list = tmra.processTransaction(txid, params);
				break;
			}
		} catch (Exception e) {
			log.error("",e);
		}
		
		return list;
	}
	
	
	public String getChanel(String txid) {
		String chanel = "";
		DateTime d1 = new DateTime();
		chanel = XmlUtil.getChanel(xmlFilePath, txid);
		log.info("getChanel diffTime>>{}",DateUtil.getDiffTimeMills(d1, new DateTime()));
		return chanel;
	}

}
