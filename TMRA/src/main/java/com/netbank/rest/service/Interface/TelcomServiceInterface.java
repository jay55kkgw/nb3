package com.netbank.rest.service.Interface;

import java.util.List;
import java.util.Map;

public interface TelcomServiceInterface {

	/**
	 * 走TMRA 回覆一般電文資料格式
	 * @param parameterMap
	 * @return
	 */
	public Map<String,Object> doTelcom(Map<String,String> parameterMap);
	
	/**
	 * 走電文模組 回覆舊bean可以接收的資料格式
	 * @param param`eterMap
	 * @return
	 */
	public List doTelcomModule(String txid ,Map<String,String> parameterMap);
	
	
}
