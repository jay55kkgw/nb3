package com.netbank.rest.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;

import com.ctc.wstx.io.EBCDICCodec;
import com.netbank.tmraObject.Core;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.netbank.tmraObject.InputImpl;
import com.netbank.tmraObject.OutputImpl;
import com.netbank.util.CodeUtil;
import com.netbank.util.TelcomUtil;

/**
 * 處理電文的服務
 */
@Service
@Slf4j
public class TelcomService{
	@Value("${imsDataStoreName}")
	private String imsDataStoreName;
	@Value("${imsHostName}")
	private String imsHostName;
	@Value("${imsPortNumber}")
	private Integer imsPortNumber;
	
	/**
	 * 電文實作
	 */
	public Map<String,Object> doTelcom(Map<String,String> parameterMap){
		log.debug("IN doTelcom");
		
		Map<String,Object> resultMap = new HashMap<String,Object>();
		resultMap.put("msgCode","ERROR");
		resultMap.put("queryTime",new Date());
		
		try{
			//智彬
			TelcomUtil.preProgress(parameterMap);
			
			byte[] headerBasicDataByte = TelcomUtil.getHeaderAndBasicData(parameterMap);
			log.debug("headerBasicDataByte={}",headerBasicDataByte);
			
			//組HEADER和BASICDATA成功
			if(headerBasicDataByte != null){
				//組RQ的欄位並回傳組好的BYTE[]
				byte[] dataByte = TelcomUtil.getRQData(parameterMap);
				log.debug("dataByte={}",dataByte);
				log.debug("dataByte.first={}",CodeUtil.EBCDICtoBig5( dataByte));
				//組RQ的欄位成功
				if(dataByte != null){
					//組起來
					byte[] finalByte = CodeUtil.combineByte(headerBasicDataByte,dataByte);
					log.debug("finalByte={}",finalByte);
					log.debug("finalByte22={}",CodeUtil.EBCDICtoBig5(finalByte));
					
					//組成功
					if(finalByte != null){
						InputImpl inputImpl = new InputImpl();
						inputImpl.setInputBytes(finalByte);
						
						OutputImpl outputImpl = new OutputImpl();
						
						Core core = new Core(imsDataStoreName,imsHostName,imsPortNumber);
//						core.execute(inputImpl,outputImpl);
						
						byte[] outputBytes = outputImpl.getOutputBytes();
						log.debug("outputBytes={}",outputBytes);
						log.debug("outputBytes.first={}",CodeUtil.EBCDICtoBig5( outputBytes));
						//下行電文後製成功
						if(outputBytes != null){
							String hexData = CodeUtil.ByteArrayToHexString(outputBytes);
							log.debug("hexData={}",hexData);
							
							String outString = new String(CodeUtil.hexStringToByteArray(hexData),"BIG5");
							log.debug("outString={}",outString);
							
							//將前面4個字元截掉
							hexData = hexData.substring(8);
							log.debug("hexData={}",hexData);
							
							outString = new String(CodeUtil.hexStringToByteArray(hexData),"BIG5");
							log.debug("outString={}",outString);
							
							List<Map<String,String>> dataListMap = new ArrayList<Map<String,String>>();
							
							Map<String,String> returnMap = TelcomUtil.getRSData(dataListMap,hexData,parameterMap);
							log.debug("returnMap={}",returnMap);
							
							String getRSDataResult = returnMap.get("result");
							log.debug("getRSDataResult={}",getRSDataResult);
							
							//取得電文下行資料成功
							if("TRUE".equals(getRSDataResult)){
								boolean whileFinish = true;
								
								boolean doAgain = TelcomUtil.doTelcomAgain(parameterMap,returnMap);
								
								//主機還有資料，要再去要
								while(doAgain == true){
									whileFinish = false;
									
									//再打電文去要資料，主機用來識別的資料
									TelcomUtil.getIdentifyData(parameterMap,returnMap);
									
									dataByte = TelcomUtil.getRQData(parameterMap);
									log.debug("dataByte={}",dataByte);
									log.debug("dataByte2={}",CodeUtil.EBCDICtoBig5( dataByte));
									
									//組RQ的欄位成功
									if(dataByte != null){
										//組起來
										finalByte = CodeUtil.combineByte(headerBasicDataByte,dataByte);
										log.debug("finalByte={}",finalByte);
										log.debug("finalByte2={}",CodeUtil.EBCDICtoBig5( finalByte));
										
										//組成功
										if(finalByte != null){
											inputImpl.setInputBytes(finalByte);
											
//											core.execute(inputImpl,outputImpl);
											
											outputBytes = outputImpl.getOutputBytes();
											log.debug("outputBytes={}",outputBytes);
											
											hexData = CodeUtil.ByteArrayToHexString(outputBytes);
											log.debug("hexData={}",hexData);
											
											outString = new String(CodeUtil.hexStringToByteArray(hexData),"BIG5");
											log.debug("outString={}",outString);
											
											//將前面4個字元截掉
											hexData = hexData.substring(8);
											log.debug("hexData={}",hexData);
											
											outString = new String(CodeUtil.hexStringToByteArray(hexData),"BIG5");
											log.debug("outString={}",outString);
											
											returnMap = TelcomUtil.getRSData(dataListMap,hexData,parameterMap);
											log.debug("returnMap={}",returnMap);
											
											getRSDataResult = returnMap.get("result");
											log.debug("getRSDataResult={}",getRSDataResult);
											
											//再次取得電文下行資料成功
											if("TRUE".equals(getRSDataResult)){
												whileFinish = true;
												
												doAgain = TelcomUtil.doTelcomAgain(parameterMap,returnMap);
											}
											//再次取得電文下行資料失敗
											else{
												resultMap.put("msgName","getRSData出現問題");
												break;
											}
										}
										//組失敗
										else{
											resultMap.put("msgName","combineByte出現問題");
											break;
										}
									}
									//組RQ的欄位失敗
									else{
										resultMap.put("msgName","getRQData出現問題");
										break;
									}
								}
								if(whileFinish == true){
									int REC_NO = dataListMap.size();
									log.debug("REC_NO={}",REC_NO);
									resultMap.put("REC_NO",REC_NO);
									
									resultMap.put("REC",dataListMap);
									
									resultMap.put("msgCode","0");
								}
							}
							//取得電文下行資料失敗
							else{
								resultMap.put("msgName","getRSData出現問題");
							}
						}
						//下行電文後製失敗
						else{
							resultMap.put("msgName","getOutputBytes出現問題");
						}
					}
					//組失敗
					else{
						resultMap.put("msgName","combineByte出現問題");
					}
				}
				//組RQ的欄位失敗
				else{
					resultMap.put("msgName","getRQData出現問題");
				}
			}
			//組HEADER和BASICDATA失敗
			else{
				resultMap.put("msgName","getHeaderAndBasicData出現問題");
			}
		}
		catch(Exception e){
			log.error(String.valueOf(e));
			resultMap.put("msgName",String.valueOf(e));
		}
		return resultMap;
	}
}