package com.netbank.rest.og.aop;

import java.util.Base64;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.joda.time.DateTime;
import org.joda.time.DateTimeUtils;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.netbank.util.DateUtil;
import com.netbank.util.ESAPIUtil;

import brave.Tracer;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Aspect
@Component
public class SysLoggerAop {
	
	@Autowired HttpServletRequest req; 
	
	@Autowired Tracer tracer;
	
	
//	@Pointcut(
//			"within(com.netbank.rest.web.controller..*) &&" +
//			"@annotation(requestMapping) &&" +
//			"@annotation(pathVariable) &&" +
//			"execution(* *(..))"
//			 )
//	public void controller(RequestMapping requestMapping ,PathVariable pathVariable) {}
//	
////	public Object doBefore() {
//	@Before("within(com.netbank.rest.web.controller..*) && controller(requestMapping , pathVariable)")
//		public Object doBefore(JoinPoint joinPoint ,RequestMapping requestMapping,PathVariable pathVariable) {
//		
//		log.debug("SysLoggerAop.doBefore...");
//		String cusidn = "";
//		try {
////			requestMapping.
//			log.trace("requestMapping>>{}",requestMapping.headers());
//			log.trace("pathVariable>>{}", pathVariable.value());
//			cusidn = req.getHeader("trace_cusidn");
//			cusidn = new String (Base64.getDecoder().decode(cusidn) ,"utf-8");
//			log.info("trace_cusidn>>{}",cusidn);
//			log.trace("traceId>>{}",tracer.currentSpan().context().traceId());
//			if(StrUtils.isNotEmpty(cusidn)) {
//				MDC.put("loginid",cusidn );
//			}
//		} catch (Exception e) {
//			log.error(e.toString());
//		}
//		return null;
//	}
	
	
	
	
//	@Before("within(com.netbank.rest.web.controller..*)")
//	public Object doBefore(JoinPoint joinPoint ) {
//		log.debug("SysLoggerAop.doBefore...");
//		String cusidn = "";
//		String userid = "";
//		String adguid = "";
//		String apiName = "";
//		
//		try {
//			log.debug("getArgs 0 >>{}",joinPoint.getArgs()[0]);
//			cusidn = req.getHeader("trace_cusidn");
//			userid = req.getHeader("trace_userid");
//			adguid = req.getHeader("trace_adguid");
//			log.info("trace_cusidn>>{}",ESAPIUtil.vaildLog(cusidn));
//			log.info("trace_userid>>{}",ESAPIUtil.vaildLog(userid));
//			log.info("trace_adguid>>{}",ESAPIUtil.vaildLog(adguid));
//			if(StrUtils.isNotEmpty(cusidn)) {
//				cusidn = new String (Base64.getDecoder().decode(cusidn) ,"utf-8");
//				log.info("cusidn>>{}", ESAPIUtil.vaildLog(cusidn));
//				MDC.put("loginid",cusidn );
//			}
//			if(StrUtils.isNotEmpty(userid)) {
//				userid = new String (Base64.getDecoder().decode(userid) ,"utf-8");
//				log.info("userid>>{}",ESAPIUtil.vaildLog(userid));
//				MDC.put("userid",userid );
//			}
//			apiName = (String) joinPoint.getArgs()[0];
//			log.info("apiName>>{}",apiName);
//			
//			if(StrUtils.isNotEmpty(apiName)) {
//				MDC.put("apiName",apiName );
//			}
//			if(StrUtils.isNotEmpty(adguid)) {
//				MDC.put("adguid",adguid );
//			}
//			log.trace("traceId>>{}",tracer.currentSpan().context().traceId());
//			log.trace("traceIdString...>>{}",tracer.currentSpan().context().traceIdString());
//			
//		} catch (Exception e) {
//			log.error(e.toString());
//		}
//		return null;
//	}
	
	
	@Around("within(com.netbank.rest.web.controller..*)")
	public Object doAround(ProceedingJoinPoint pjp ) {
		log.info("SysLoggerAop.doBefore...");
		String cusidn = "";
		String userid = "";
		String adguid = "";
		String apiName = "";
		Object obj = null;
		Object[] args =null;
		try {
			log.debug("getArgs 0 >>{}",pjp.getArgs()[0]);
			cusidn = req.getHeader("trace_cusidn");
			userid = req.getHeader("trace_userid");
			adguid = req.getHeader("trace_adguid");
			DateTime dd1 = new DateTime();
			log.info("trace_cusidn>>{}",ESAPIUtil.vaildLog(cusidn));
			log.info("trace_userid>>{}",ESAPIUtil.vaildLog(userid));
			log.info("trace_adguid>>{}",ESAPIUtil.vaildLog(adguid));
			log.info("after ESAPIUtil.vaildLog>>{}",DateUtil.getDiffTimeMills(dd1, new DateTime()));
			
//			壓力測試用
			if(StrUtils.isNotEmpty(req.getParameter("PUUID"))) {
				adguid = req.getParameter("PUUID");
			}
			
			if(StrUtils.isNotEmpty(cusidn)) {
				cusidn = new String (Base64.getDecoder().decode(cusidn) ,"utf-8");
				log.info("cusidn>>{}", ESAPIUtil.vaildLog(cusidn));
				MDC.put("loginid",cusidn );
			}
			if(StrUtils.isNotEmpty(userid)) {
				userid = new String (Base64.getDecoder().decode(userid) ,"utf-8");
				log.info("userid>>{}",ESAPIUtil.vaildLog(userid));
				MDC.put("userid",userid );
			}
//			apiName = (String) pjp.getArgs()[0];
			try {
				args = pjp.getArgs();
				if(args !=null) {
					for(int i = 0 ; i < args.length ; i++) {
						if( args[i]  instanceof String) {
							apiName = (String) args[i];
						}
					}
				}
			} catch (Exception e) {
				log.error("{}",e);
			}
			log.info("apiName>>{}",apiName);
			
			if(StrUtils.isNotEmpty(apiName)) {
				MDC.put("apiName",apiName );
			}
			if(StrUtils.isNotEmpty(adguid)) {
				MDC.put("adguid",adguid );
			}
			log.trace("traceId>>{}",tracer.currentSpan().context().traceId());
			log.trace("traceIdString...>>{}",tracer.currentSpan().context().traceIdString());
			
		} catch (Exception e) {
			log.error(e.toString());
		}
		DateTime d1 = new DateTime();
		try {
			obj = pjp.proceed();
		} catch (Throwable e) {
			log.error("{}",e.toString());
		}finally {
			log.warn("TMRA CTRL timeDiff>>{}", DateUtil.getDiffTimeMills(d1, new DateTime()));
		}
		
		return obj;
	}

}
