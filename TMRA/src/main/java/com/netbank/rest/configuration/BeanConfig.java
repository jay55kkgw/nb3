package com.netbank.rest.configuration;

import com.netbank.rest.web.filter.SysFilter;
import lombok.extern.slf4j.Slf4j;
import org.apache.cxf.spring.boot.autoconfigure.CxfAutoConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorViewResolver;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.servlet.ModelAndView;
import org.yaml.snakeyaml.Yaml;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;

@Configuration
@EnableAutoConfiguration(exclude = {
       DataSourceAutoConfiguration.class, CxfAutoConfiguration.class,
        LiquibaseAutoConfiguration.class})
@Slf4j
public class BeanConfig  {

    @Bean
    protected Yaml yaml() throws Exception {
        return new Yaml();
    }

    @Bean
    public FilterRegistrationBean sysFilterRegistration() {
        log.info("sysfilter config ..");
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new SysFilter());
        registration.addUrlPatterns("/*");
        //registration.addInitParameter("paramName", "paramValue");
        registration.setName("SysFilter");
        registration.setOrder(1);
        return registration;
    }



    @Bean
    ErrorViewResolver supportPathBasedLocationStrategyWithoutHashes() {
        return new ErrorViewResolver() {
            @Override
            public ModelAndView resolveErrorView(HttpServletRequest request, HttpStatus status, Map<String, Object> model) {
                return status == HttpStatus.NOT_FOUND
                        ? new ModelAndView("index.html", Collections.<String, Object>emptyMap(), HttpStatus.OK)
                        : null;
            }
        };
    }

    @Bean
    public JavaMailSender getJavaMailSender(
            @Value("${spring.mail.properties.mail.smtp.auth:}") String smtpAuth,
            @Value("${spring.mail.host:}") String host,
            @Value("${spring.mail.port:}") String port) {


        log.info("MAIL Config >> smtpAuth:" + smtpAuth + ", host: " + host + ", port: " + port);

        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(host);
        try {
            mailSender.setPort(Integer.valueOf(port));
        } catch (Exception e) {}

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", smtpAuth);
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.connectiontimeout", "3000");
        props.put("mail.smtp.timeout", "3000");
        props.put("mail.smtp.writetimeout", "3000");
        props.put("mail.debug", "true");

        return mailSender;
    }
    @Bean
    public Jackson2ObjectMapperBuilderCustomizer jacksonObjectMapperCustomization() {
        return builder -> {
            builder.timeZone(TimeZone.getDefault());
//            builder.dateFormat(new ISO8601DateFormat());
        };
    }

}
