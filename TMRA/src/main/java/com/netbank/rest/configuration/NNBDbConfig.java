package com.netbank.rest.configuration;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jndi.JndiObjectFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.naming.NamingException;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

/**
 * 連舊網銀DB，因暫時沒用到 且防火牆未開通 故註解
 * 功能說明 :
 *
 */



//@Configuration
//@EnableTransactionManagement
//@EnableJpaRepositories(
//        entityManagerFactoryRef = "nnbentityManagerFactory",
//        basePackages = {"fstop.orm.dao"},
//        transactionManagerRef = "nb3transactionManager"
//)
public class NNBDbConfig {

//    
//    @Bean(name="nnbDataSource")
//    public DataSource nnbDataSource() throws IllegalArgumentException, NamingException {
//    	JndiObjectFactoryBean bean = new JndiObjectFactoryBean();
//    	bean.setJndiName("jdbc/nnb");
//    	bean.setProxyInterface(DataSource.class);
//    	bean.setLookupOnStartup(false);
//    	bean.afterPropertiesSet();
//    	return (DataSource)bean.getObject();
//    }
//    
//    
//    @Bean(name = "nnbentityManagerFactory")
//    public LocalContainerEntityManagerFactoryBean nnbentityManagerFactory( EntityManagerFactoryBuilder builder,
//    		@Qualifier("nnbDataSource") DataSource dataSource) {
//    	return builder
//    			.dataSource(dataSource)
//    			.packages("fstop.orm.po")
//    			.persistenceUnit("nnb")
//    			.build();
//    }
//
//    
//    
//    @Bean(name = "nnbtransactionManager")
//    public PlatformTransactionManager nnbtransactionManager(
//    		@Qualifier("nnbentityManagerFactory") EntityManagerFactory entityManagerFactory) {
//    	return new JpaTransactionManager(entityManagerFactory);
//    }
}
