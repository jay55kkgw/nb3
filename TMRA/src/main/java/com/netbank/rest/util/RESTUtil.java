package com.netbank.rest.util;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
//import org.apache.http.ssl.TrustStrategy;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.netbank.rest.bean.Sendadmail_REST_RQ;
import com.netbank.rest.bean.Sendadmail_REST_RS;
import com.netbank.util.BaseResult;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;
import com.netbank.util.ResultCode;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 功能說明 :電文傳送Util
 *
 */
@Component
@Slf4j
public class RESTUtil {
//	@Value("${isAgent}")
//	String isAgent = "N";
	
//	@Value("${cert_path}")
//	String cert_path = "C:\\FSTOP\\tool\\cert\\DataPower\\focastestcer";
//	@Value("${soci}")
//	String soci = "changeit";
	

	public BaseResult SendAdMail_REST(String Subject,String Content,List<String> Receivers,String contentType ,String ms_Channel) {
        log.info("SendAdMail_REST");
        BaseResult bs = null;
        Sendadmail_REST_RQ rq = null;
        Sendadmail_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new Sendadmail_REST_RQ();
            rq.setSubject(Subject);
            rq.setContent(Content);
            rq.setReceivers(Receivers);
            rq.setContentType(contentType);
            rq.setMs_Channel(ms_Channel);
            rs = this.send(rq, Sendadmail_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.debug("SendAdMail_RS>>{}", rs);
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("SendAdMail_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        return bs;
    }
	
	
	/**
	 * 電文傳送 RESTful
	 * @param params
	 * @param url
	 * @return
	 * @throws Exception 
	 */
	@SuppressWarnings("unchecked")
	public  String send(Map params ,String url, Integer toTmraTimeout ) {
		String result = null;
		RestTemplate restTemplate = null;
		ClientHttpRequestFactory rf = null;
		try {
			log.trace("url>>{}",url);
			log.trace("params>>{}",params);
			if(!params.isEmpty()) {
				log.trace("RESTfulClient.params.toJson>>{}", CodeUtil.toJson(params));
			}
			restTemplate = setHttpConfigII(toTmraTimeout);
			

			HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
			log.debug("request>>{}",request);
			String trace_cusidn =  request.getHeader("trace_cusidn");
			log.debug("trace_cusidn>>{}",ESAPIUtil.vaildLog(trace_cusidn));
			String trace_userid = request.getHeader("trace_userid"); 
			log.debug("trace_userid>>{}",ESAPIUtil.vaildLog(trace_userid));
			
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.set("trace_cusidn", trace_cusidn);
			headers.set("trace_userid", trace_userid);
			
			HttpEntity<Map<String, String>> entity = new HttpEntity<>(params, headers);
			ResponseEntity<String> re  = restTemplate.postForEntity(url, entity, String.class );
			
			result = re.getBody();
			
			log.trace("result>>{}",ESAPIUtil.vaildLog(result) );
			
			if(result == null) {
				throw new Exception("result == null ,電文接收異常");
			}
			
		} catch (ResourceAccessException e) {
			log.error("{}",e);
			throw e;
			
		} catch (Exception e) {
			log.error("{}",e);
		}
		
		
		return result;
		
	}
	public  <T> T send(Object sendClass , Class<?> retClass ,String api  ) {
		Map result = null;
		RestTemplate restTemplate = null;
		ClientHttpRequestFactory rf = null;
		Object retobj = null;
		Integer toMsTimeOut = 1;
		try {
			log.trace("url>>{}",api);
			restTemplate = setHttpConfigII(toMsTimeOut);
			HashMap<String,String> params = CodeUtil.objectCovert(HashMap.class, sendClass);
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			log.debug(ESAPIUtil.vaildLog("sendJSON >> " + CodeUtil.toJson(params))); 
			HttpEntity<Map<String, String>> entity = new HttpEntity<>(params, headers);
//			ResponseEntity<String> re  = restTemplate.postForEntity(api, entity, String.class );
			ResponseEntity<HashMap> re  = restTemplate.postForEntity(api, entity, HashMap.class );
			
			result = re.getBody();
			
			log.trace("result>>{}",result);
			
			if(result == null) {
				throw new Exception("result == null ,電文接收異常");
			}
			retobj = CodeUtil.objectCovert(retClass   ,result);
		} catch (ResourceAccessException e) {
			log.error("{}",e.toString());
			throw e;
			
		} catch (Exception e) {
			log.error("{}",e.toString());
		}
		
		
		return (T) retobj;
		
	}
	
	// DataPower.query
	public String send2DataPower(Map<String, String> params ,String url, Integer toDataPowerTimeout ) {
		String result = null;
//		HashMap<String, Object> result = null;
		RestTemplate restTemplate = null;
		try {
			log.trace("send2DataPower.url: {}", url);
			log.trace("send2DataPower.params: {}", params);
			
			if(!params.isEmpty()) {
				log.trace("RESTfulClient.params.toJson: {}", CodeUtil.toJson(params));
			}
			
			restTemplate = setHttpConfigII(toDataPowerTimeout);
			
			result = restTemplate.postForObject(url, params, String.class);
			
			log.trace("send2DataPower.result: {}", result);
			
			if(result == null) {
				throw new Exception("result == null ,電文接收異常");
			}
			
		} catch (ResourceAccessException e) {
			log.error("{}",e);
			throw e;
			
		} catch (Exception e) {
			log.error("{}",e);
		}
		
		return result;
	}
	
	
    

	public  RestTemplate setHttpConfig() {
		Integer connectTimeout = 5*1000;
		Integer socketTimeout = 55*1000;
		HttpComponentsClientHttpRequestFactory rf = new HttpComponentsClientHttpRequestFactory();
		rf.setConnectTimeout(connectTimeout);
		rf.setReadTimeout(socketTimeout);
		return new RestTemplate(rf);
		
	}
	/**
	 * 設定Http 參數
	 * https://codertw.com/%E7%A8%8B%E5%BC%8F%E8%AA%9E%E8%A8%80/12005/
	 * @param toTmraTimeout
	 * @return
	 * @throws KeyManagementException
	 * @throws NoSuchAlgorithmException
	 * @throws KeyStoreException
	 */
	public  RestTemplate setHttpConfigII(Integer toTmraTimeout) throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
		Integer connectTimeout = 1*1000;
		Integer socketTimeout = toTmraTimeout*1000;
		HttpComponentsClientHttpRequestFactory rf =
				new HttpComponentsClientHttpRequestFactory();
		rf.setConnectTimeout(connectTimeout);
		rf.setReadTimeout(socketTimeout);
		log.trace("setHttpConfigI end..");
		return new RestTemplate(rf);
		
	}
	
	
	
	
}
