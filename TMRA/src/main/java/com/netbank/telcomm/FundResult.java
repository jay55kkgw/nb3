package com.netbank.telcomm;

import java.util.List;

import lombok.Data;
@Data
public class FundResult {
	private String okmsg="0007*OK"; 
	private Boolean result = Boolean.FALSE;
	private Boolean isLoop = Boolean.FALSE;
	private Boolean isTOTA = Boolean.FALSE;
	private Boolean keepRecive = Boolean.FALSE;
	private Boolean isSticky = Boolean.FALSE;
	private List    data;
	
	
	public void reset() {
		result = Boolean.FALSE;
		isLoop = Boolean.FALSE;
		isTOTA = Boolean.FALSE;
		isSticky = Boolean.FALSE;
	}
}
