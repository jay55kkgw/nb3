package com.netbank.telcomm;

import java.io.UnsupportedEncodingException;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Vector;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.common.primitives.Bytes;
import com.netbank.rest.util.RESTUtil;
import com.netbank.socketClient.SocketClient;
import com.netbank.util.CodeUtil;
import com.netbank.util.DateUtil;
import com.netbank.util.ESAPIUtil;
import com.netbank.util.HexDumpUtil;
import com.netbank.util.StrUtils;

import bank.comm.CommunicationException;
import bank.comm.ConvCode;
import bank.comm.DataTemplateX;
import bank.comm.FlowObject;
import bank.comm.MsgMap;
import bank.comm.TelegramDataList;
import fstop.util.MailUtil;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 功能說明 : 整合收發電文的功能,根據收發的邏輯<br>
 * 完成一筆電文交易,該類別實作之交易邏輯具有下列特徵 : <br>
 * (1)以EBCDIC編碼方式發送電文及接收電文<br>
 * (2)必須根據中心回傳的訊息編組電文上傳中心以繼續接收資料或結束交易
 */
@Component
@Data
@Slf4j
public class FXTransactionImpl {
	public static final String FXTCPIP_KEY = "FXTCPIP_KEY";
	public static final String FXMTCPIP_KEY = "FXMTCPIP_KEY";
	@Value("${fxHostName}")
	private String fxHostName;
	@Value("${fxPortNumber}")
	private Integer fxPortNumber;
	@Value("${fxuPortNumber}")
	private Integer fxuPortNumber;
	@Value("${fxmPortNumber}")
	private Integer fxmPortNumber;
	@Value("${fxuPortNumberA}")
	private Integer fxuPortNumberA;
	@Value("${xmlFilePath}")
	private String xmlFilePath;
	@Value("${FxStime}")
	private String FxStime;
	@Value("${FxEtime}")
	private String FxEtime;
	@Value("${isChgFxPort}")
	private String isChgFxPort;
	@Value("${FxloginUser}")
	private String FxloginUser;
	@Value("${FxloginPin}")
	private String FxloginPin;
	
	@Value("${mails:sox@tbb.mail.com.tw,H15D302B@mail.tbb.com.tw}")
	private String mails = "";
	@Value("${tmraEnv:P}")
	private String tmraEnv = "";
	@Value("${ms_psChannel:http://msps:9080/ms_ps/com/}")
	private String ms_psChannel = "";
	// private String fxHostName;
	// private Integer fxPortNumber;
	// private String xmlFilePath;

	/**
	 * 首次發送至中心之上行電文的byte array中第5個byte至第16個byte之資料
	 */
	private byte[] transferHeader = new byte[12];
	
	@Autowired
	RESTUtil restutil;

	@PostConstruct
	public void test() {
		// DataTemplateX.XML_FILE_PATH = xmlFilePath;
		// log.trace("XML_FILE_PATH>>{}",DataTemplateX.XML_FILE_PATH);
		// log.trace("xmlFilePath>>{}",xmlFilePath);
	}

	/**
	 * 取得上行電文
	 * 
	 * @param
	 * @return
	 * @throws CommunicationException
	 */
	public byte[] getTITABytes(String txid, Hashtable message, Integer id,Hashtable TOTAByteRecord) throws CommunicationException {
		byte[] TITAArray = new byte[4096];
		byte[] buf = null;
		StringBuffer TITABuffer = new StringBuffer();
		FlowObject titaFlow = DataTemplateX.TITAFlow[id];
		TelegramDataList tita = DataTemplateX.TITA[id];
		Hashtable positionMapping = new Hashtable();
		Vector input = new Vector();
		try {
			input.add(message);
			log.trace("titaFlow>>{}", titaFlow);
			log.trace("titaFlow,tita>>{}", tita);
			int titaLength = titaFlow.getTITABytes(tita, input, TITAArray, 0, TOTAByteRecord, positionMapping);
			positionMapping.put("TITAByteStreamEndingPosition", new Integer(titaLength));
			log.trace("TITAArray.length>>{}", TITAArray.length);
			log.trace("titaLength>>{}", titaLength);
			Arrays.fill(TITAArray, titaLength, TITAArray.length, (byte) 64);
			TITAArray = Arrays.copyOfRange(TITAArray, 8, TITAArray.length);
			buf = new byte[8];
			Arrays.fill(buf, (byte) 64);
			buf = CodeUtil.combineByte(TITAArray, buf);
			log.trace("buf.length>>{}", buf.length);
			if(log.isTraceEnabled()) {
				log.trace("HexDumpUtil.buf>>\n{}", HexDumpUtil.format(buf, 72, HexDumpUtil.HD_EBCDIC));
			}
		} catch (Exception e) {
			log.error("{}", e);
			throw new CommunicationException("M1", "Z011", "");
		}

		return buf;
	}


	public String HashToStr(Hashtable Tita, Hashtable ReTota) {
		String RetStr = new String();
		String txid = (String) Tita.get("HOSTMSGID");
		int userdataoffset = 0;
		// String DateStr=new String();
		// String TimeStr=new String();
		Hashtable sumary = new Hashtable();
		/*
		 * if(txid.equals("N130")) { userdataoffset=74; } else if(txid.equals("N133")) {
		 * userdataoffset=85; }
		 */
		for (Object key : Tita.keySet()) {
			// RetStr=RetStr+"["+key+"="+Tita.get(key)+"]";
			sumary.put(key, Tita.get(key));
		}

		for (Object key : ReTota.keySet()) {
			log.trace("key>>{}", key);
			// 20190429 add by hugo 這邊跟舊網銀不同 針對抓出來是Vector 的物件做個過濾
			if (ReTota.get(key) != null && ReTota.get(key) instanceof Vector) {
				log.trace("ReTota.Vector>>{}", ReTota.get(key));
				continue;
			}

			byte[] tmpobj = (byte[]) ReTota.get(key);
			String valueStr;
			try {
				valueStr = new String(ConvCode.host2pc(tmpobj), "BIG5");
			} catch (UnsupportedEncodingException e) {
				// TODO 自動產生 catch 區塊
				valueStr = new String(ConvCode.host2pc(tmpobj));
			}
			/*
			 * if(key.equals("USERDATA") && (txid.equals("N130") || txid.equals("N133"))) {
			 * String tmpvalue=valueStr.substring(0, userdataoffset) +"000"+
			 * valueStr.substring(userdataoffset+3); valueStr=tmpvalue; // new
			 * HexDump(HexDump.HD_ASCII,"USERDATA",valueStr.getBytes(),valueStr.length()); }
			 * else
			 */
			if (valueStr.equals("OKOV")) {
				valueStr = "";
			} else {
				// String titavalue=(String)Tita.get(key);
				// if(titavalue!=null)
				// Tita.put(key,valueStr);
			}
			sumary.put(key, valueStr);
			// RetStr=RetStr+"["+key+"="+ valueStr +"]";
			// System.out.println(key +"=" + valueStr);
		}
		for (Object key : sumary.keySet()) {
			RetStr = RetStr + "[" + key + "=" + sumary.get(key) + "]";
		}
		return (RetStr);
	}
	
	
	
	
	
	/**
	 * 處理下行電文
	 * 
	 * @param ID
	 * @param answerVector
	 * @param message
	 * @param result
	 * @return
	 * @throws CommunicationException
	 */
	public Boolean processTOTA(int ID, Vector answerVector, Hashtable message, Vector result,Hashtable TOTAByteRecord)
			throws CommunicationException {
		byte[] answer = (byte[]) answerVector.get(0);
		boolean resultType = false;
		FlowObject totaFlow = DataTemplateX.TOTAFlow[ID];
		TelegramDataList tota = DataTemplateX.TOTA[ID];
		String userdata_x50 = "";
		int cnt = 0;
		totaFlow.SetMaxRecodMsg("");
		int status = totaFlow.getTOTATable(tota, answer, result, 0, message, TOTAByteRecord);
		log.trace("status>>{}", status);
		log.trace("result>>{}", result);
		log.trace("TOTAByteRecord>>{}", TOTAByteRecord);
		
		
		String retmaxmsg = totaFlow.GetMaxRecodMsg();

		log.trace("retmaxmsg >>{} }", retmaxmsg);

		if (retmaxmsg.equals(totaFlow.contmaxrecodemsg) && totaFlow.contmaxrecodemsg.length() > 0 && status == 0) {
			String nextquery = HashToStr(message, TOTAByteRecord);
			log.trace("String nextquery = {}", ESAPIUtil.vaildLog(nextquery) );
			Hashtable h = new Hashtable();
			h.put("QUERYNEXT", nextquery);
			result.add(h);
			// tempVector.add(h);
			// result.setElement(tempVector);
			// return (FSTopResultSet)result;
		}
		
		
		if (status == 1)
			return false;
		else if (status == 0)
			return true;

		return false;

	}

	/**
	 * 處理電文交易
	 * 
	 * @param txid
	 * @param params
	 * @return
	 */
	public List processTransaction(String txid, Map params) {
		byte[] tita = null;
		byte[] rs = null;
		Vector totaVector = null;
		Vector answerVector = null;
		Boolean result = Boolean.FALSE;
		Boolean isLogin = Boolean.FALSE;
		List dataList = null;
		Hashtable message = null;
		SocketClient sc = null;
		SocketChannel socketChannel = null;
		Integer portNumber = null;
		List tempList = null;
		try {
			int id = -1;
			tempList = new LinkedList();

			id = DataTemplateX.getID(txid);
			if (id <= 0) {
				throw new CommunicationException("M1", "Z007", "TXID can't be found in the hashtable");
			}
			Hashtable TOTAByteRecord = new Hashtable();
			// 過濾掉 value 是null 避免Hashtable error
			params.values().removeIf(Objects::isNull);
			message = new Hashtable();
			message.putAll(params);
			addD4Params(message, txid, id);
			answerVector = new Vector();
			totaVector = new Vector();
			sc = new SocketClient();
			portNumber = getPortNumber(params);
			log.debug("portNumber>>{}", portNumber);
			socketChannel = sc.connect(getFxHostName(), portNumber);

			log.debug("Login..");
			isLogin = login(sc, socketChannel);
			log.debug("loop..");
			int cnt = 1;
			do {

				if (isLogin) {
					answerVector.clear();
					log.trace("message>>{}",ESAPIUtil.vaildLog( CodeUtil.toJson(message)) );
					tita = getTITABytes(txid, message, id,TOTAByteRecord);
					socketChannel = sc.send(socketChannel, tita);
					rs = sc.receiveForFx(socketChannel);
//					rs = sc.receive(socketChannel);
					log.trace("rs>>{}", rs);
					answerVector.add(rs);
					result = processTOTA(id, answerVector, message, totaVector,TOTAByteRecord);
					log.trace("totaVector>>", totaVector);
					cnt++;
				}
			} while (!result);

			result = Boolean.TRUE;
		} catch (CommunicationException e) {
			log.error("CommunicationException>>", e);
			setMsgCode(e.getErrorCode(), tempList);
			sendMail(e.getErrorCode(), "FX", txid);
		} catch (Exception e) {
			log.error("", e);
			setMsgCode("Z018", tempList);
			sendMail("Z018", "FX", txid);
		} finally {
			dataList = new LinkedList();
			if (result) {
				dataList.addAll(totaVector);
			} else {
				dataList.addAll(tempList);
			}
			if (sc != null && socketChannel != null) {
				sc.close(socketChannel);
			}
		}
		log.trace("dataList>>{}", dataList);
		return dataList;

	}
	

	public void sendMail(String errorCode , String channel ,String txid) {
		HashMap<String, Object> map;
		String subject = "";
		String content = "";
		
		MailUtil mailutil = null;
		List<String> mailList = new ArrayList<String>();
		try {
			mailutil = new MailUtil();
			log.info("mails>>{}",mails);
			mailList.add(mails);
			content = mailutil.getMailContentForGW(errorCode, channel ,txid ,tmraEnv);
			switch (tmraEnv) {
			case "D":
				subject = "開發套新個網NB3GW異常通知";
				break;
			case "T":
				subject = "測試套新個網NB3GW異常通知";
				break;
			default:
				subject = "新個網NB3GW異常通知";
				break;
			}
			restutil.SendAdMail_REST(subject, content, mailList,"1" ,ms_psChannel);
				
		} catch (Exception e) {
			log.error("{}", e.toString());
		}
	}

	public void addD4Params(Hashtable message, String txid, Integer id) throws CommunicationException {
		ArrayList temp = null;
		temp = DataTemplateX.nameMaps[id];

		if (temp != null) {
			log.trace("temp.get(2)>>{},temp.get(3)>>{},temp.get(4)>>{}", temp.get(2), temp.get(3), temp.get(4));
			message.put("CLIENTENCODING", temp.get(2) + "");
			message.put("HOSTENCODING", temp.get(3) + "");
			message.put("TOPMSG", temp.get(4) + "");
			if (null != temp.get(5)) {
				message.put("TOPMAXRECOD", (String) temp.get(5));
			}
		} else {
			throw new CommunicationException("M1", "Z002", "There is something wrong in Telegram.xml" + txid);
		}
	}
	
	
	/**
	 * 取得外幣port
	 * 
	 * @param params
	 * @return
	 */
	public Integer getPortNumber(Map<String, String> params) {
		log.debug("params.get(FXTCPIP_KEY)>>{}", ESAPIUtil.vaildLog(params.get(FXTCPIP_KEY)) );
		log.debug("params.get(FXMTCPIP_KEY)>>{}", ESAPIUtil.vaildLog(params.get(FXMTCPIP_KEY)));
		if (StrUtils.isNotEmpty(params.get(FXTCPIP_KEY)) && "FXUTCPIP".equals(params.get(FXTCPIP_KEY))) {

			if ("Y".equalsIgnoreCase(isChgFxPort)) {

				if (DateUtil.belongCalendar(DateUtil.getTimeRetDate(""), DateUtil.getTimeRetDate(FxStime),
						DateUtil.getTimeRetDate(FxEtime))) {

					return fxuPortNumberA;
				}

			}

			return fxuPortNumber;
		}
		if (StrUtils.isNotEmpty(params.get(FXMTCPIP_KEY)) && "FXMTCPIP".equals(params.get(FXMTCPIP_KEY))) {
			
			
			return fxmPortNumber;
		}

		return fxPortNumber;

	}

	/**
	 * 設定回覆訊息
	 * 
	 * @param msgCode
	 * @param tempVector
	 */
	public void setMsgCode(String msgCode, List tempVector) {
		tempVector.clear();
		MsgMap.geterrmsg(msgCode, tempVector);
	}

	/**
	 * 外幣HandShake
	 * 
	 * @param sc
	 * @param socketChannel
	 * @return
	 * @throws CommunicationException
	 */
	public Boolean login(SocketClient sc, SocketChannel socketChannel) throws CommunicationException {

		Boolean result = Boolean.FALSE;
//		String s1 = "AS400                         SOCKETUSERSAM1289   ";
		String s1 = "AS400                         ";
		String tmp ="";
		// 長度應該要16
		byte[] tita = null;
		try {
			log.trace("FxloginPin>>{}",FxloginPin);
			tmp = new String(Base64.getDecoder().decode(FxloginPin),"utf-8");
			log.trace("FxloginPin1>>{}",tmp);
//			U0FNMTI4OQ==
			s1 = s1+FxloginUser+tmp;
			s1 = String.format("%-50s", s1);
			log.trace("s1.length()>>{}", s1.length());
			tita = s1.getBytes("CP937");
			log.trace("SocketChannel>>{}", socketChannel);
			if(log.isTraceEnabled()) {
				log.trace("HexDumpUtil.buf>>\n{}", HexDumpUtil.format(tita, 72, HexDumpUtil.HD_EBCDIC));
			}
			sc.send(socketChannel, tita);
			byte[] rs = sc.receive(socketChannel);
			log.trace("isOK>>{}", Bytes.indexOf(rs, "0000000".getBytes("CP937")));

			if (rs != null && Bytes.indexOf(rs, "0000000".getBytes("CP937")) != -1) {
				result = Boolean.TRUE;
			} else {
				log.error("login fail...");
				throw new CommunicationException("M1", "Z035", "");
			}
		} catch (Exception e) {
			log.error("login Exception>>{}", e);
			throw new CommunicationException("M1", "Z035", "");
		}

		return result;

	}



}
