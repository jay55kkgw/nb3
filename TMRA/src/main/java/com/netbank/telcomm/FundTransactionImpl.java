package com.netbank.telcomm;

import java.io.UnsupportedEncodingException;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Vector;

import javax.annotation.PostConstruct;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.common.primitives.Bytes;
import com.netbank.rest.util.RESTUtil;
import com.netbank.socketClient.SocketClient;
import com.netbank.util.CodeUtil;
import com.netbank.util.DateUtil;
import com.netbank.util.ESAPIUtil;
import com.netbank.util.HexDumpUtil;

import bank.comm.CommunicationException;
import bank.comm.DataTemplateX;
import bank.comm.FlowObject;
import bank.comm.MsgMap;
import bank.comm.TelegramDataList;
import fstop.util.MailUtil;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 功能說明 : 整合收發電文的功能,根據收發的邏輯<br>
 * 完成一筆電文交易,該類別實作之交易邏輯具有下列特徵 : <br>
 * (1)以EBCDIC編碼方式發送電文及接收電文<br>
 * (2)必須根據中心回傳的訊息編組電文上傳中心以繼續接收資料或結束交易
 */
@Component
@Data
@Slf4j
public class FundTransactionImpl {
	@Value("${fundHostName}")
	private String fundHostName;
	@Value("${fundPortNumber}")
	private Integer fundPortNumber;
	@Value("${xmlFilePath}")
	private String xmlFilePath;

	@Value("${mails:sox@tbb.mail.com.tw,H15D302B@mail.tbb.com.tw}")
	private String mails = "";
	@Value("${tmraEnv:P}")
	private String tmraEnv = "";
	@Value("${ms_psChannel:http://msps:9080/ms_ps/com/}")
	private String ms_psChannel = "";
	// private String fundHostName;
	// private Integer fundPortNumber;
	// private String xmlFilePath;
	public static final String EBDIC = "CP937";
	public static final String BIG5 = "BIG5";

	/**
	 * 首次發送至中心之上行電文的byte array中第5個byte至第16個byte之資料
	 */
	private byte[] transferHeader = new byte[12];

	@Autowired
	RESTUtil restutil;
	
	
	@PostConstruct
	public void test() {
		DataTemplateX.XML_FILE_PATH = xmlFilePath;
		// log.trace("XML_FILE_PATH>>{}",DataTemplateX.XML_FILE_PATH);
		// log.trace("xmlFilePath>>{}",xmlFilePath);
	}

	/**
	 * 取得上行電文
	 * 
	 * @param
	 * @return
	 * @throws CommunicationException
	 */
	public byte[] getTITABytes(String txid, Hashtable message, Integer id) throws CommunicationException {
		byte[] TITAArray = new byte[4096];
		byte[] buf = null;
		StringBuffer TITABuffer = new StringBuffer();
		FlowObject titaFlow = DataTemplateX.TITAFlow[id];
		TelegramDataList tita = DataTemplateX.TITA[id];
		Hashtable TOTAByteRecord = new Hashtable();
		Hashtable positionMapping = new Hashtable();
		Vector input = new Vector();
		try {
			input.add(message);
			log.trace("titaFlow>>{}", titaFlow);
			log.trace("titaFlow,tita>>{}", tita);
			int titaLength = titaFlow.getTITABytes(tita, input, TITAArray, 0, TOTAByteRecord, positionMapping);
			positionMapping.put("TITAByteStreamEndingPosition", new Integer(titaLength));
			log.trace("TITAArray.length>>{}", TITAArray.length);
			log.trace("titaLength>>{}", titaLength);
			// 把byte[]多餘的長度移除 ps:不是把空白拿掉
			buf = new byte[titaLength];
			System.arraycopy(TITAArray, 0, buf, 0, titaLength);
			if(log.isTraceEnabled()) {
				log.trace("HexDumpUtil.buf0>>\n{}", HexDumpUtil.format(buf, 72, HexDumpUtil.HD_ASCII));
			}
			// 移除FUND和後面5個空白 總共9
			buf = Arrays.copyOfRange(buf, 9, buf.length);
			if(log.isTraceEnabled()) {
				log.trace("HexDumpUtil.buf1>>\n{}", HexDumpUtil.format(buf, 72, HexDumpUtil.HD_ASCII));
			}
			byte[] tmp = CodeUtil.getFundTelHeader(buf.length);
			buf = CodeUtil.combineByte(tmp, buf);
			log.trace("buf.length>>{}", buf.length);
			if(log.isTraceEnabled()) {
				log.trace("HexDumpUtil.buf>>\n{}", HexDumpUtil.format(buf, 72, HexDumpUtil.HD_ASCII));
			}
		} catch (Exception e) {
			log.error("{}", e);
		}

		return buf;
	}

	public Boolean processTOTA(int ID, Vector answerVector, Hashtable message, Vector result)
			throws CommunicationException {
		byte[] answer = (byte[]) answerVector.get(0);
		boolean resultType = false;
		boolean isNoData = Boolean.TRUE;
		FlowObject totaFlow = DataTemplateX.TOTAFlow[ID];
		TelegramDataList tota = DataTemplateX.TOTA[ID];
		String userdata_x50 = "";
		Hashtable TOTAByteRecord = new Hashtable(); // 拆解TOTA stream成(name,byte[]) pair 6.6.23
		int status = totaFlow.getTOTATable(tota, answer, result, 0, message, TOTAByteRecord);
		log.trace("status>>{}", status);
		log.trace("result>>{}", result);
		log.trace("TOTAByteRecord>>{}", TOTAByteRecord);
		log.trace("processTOTA.message>>{}",ESAPIUtil.vaildLog(CodeUtil.toJson(message)) );
		log.trace("status2>>{}", status);
		switch (status) {
		case 0:
			// resultType = Boolean.TRUE;
			resultType = Boolean.FALSE;
			break;
		case 2:
			resultType = Boolean.TRUE;
			break;

		default:
			break;
		}

		return resultType;

	}

	public String Big5toEBCDIC(byte[] req) {
		String s = "";
		return null;

	}

	public static String EBCDICtoBig5(byte[] ebcdic) {
		String s = "";
		char[] ebcdic_char = null;
		byte[] big5 = null;
		try {
			ebcdic_char = new String(ebcdic, "CP937").toCharArray();
			big5 = new String(ebcdic_char).getBytes("Big5");
			s = new String(big5);
		} catch (UnsupportedEncodingException e) {
			log.error("",e);
		}

		return s;

	}

	/**
	 * 處理電文交易
	 * 
	 * @param txid
	 * @param params
	 * @return
	 */
	public List processTransaction(String txid, Map params) {
		byte[] tita = null;
		byte[] rs = null;
		Vector totaVector = null;
		Vector answerVector = null;
		Vector<byte[]> stickyVector = null;
		Boolean result = Boolean.FALSE;
		List dataList = null;
		Hashtable message = null;
		SocketClient sc = null;
		SocketChannel socketChannel = null;
		List tempList = null;
		DateTime d1 = null;
		DateTime d2 = null;
		DateTime d3 = null;
		DateTime d4 = null;
		String pstimediff ="";
		Map<String,String> loopTime = new HashMap<String, String>(); 
		try {
			int id = -1;
			tempList = new LinkedList();
			id = DataTemplateX.getID(txid);
			if (id <= 0) {
				throw new CommunicationException("M1", "ZE007", "TXID can't be found in the hashtable");
			}
			d1 = new DateTime();
			// 過濾掉 value 是null 避免Hashtable error
			params.values().removeIf(Objects::isNull);
			message = new Hashtable();
			message.putAll(params);
			addD4Params(message, txid, id);
			tita = getTITABytes(txid, message, id);
			answerVector = new Vector();
			totaVector = new Vector();
			stickyVector = new Vector();
			sc = new SocketClient();
			socketChannel = sc.connect(getFundHostName(), getFundPortNumber());
			log.trace("SocketChannel>>{}", socketChannel);
			log.trace("loop..");
			int cnt = 1;
			DateTime start_s = null;
			DateTime start = null;
			FundResult fr = new FundResult();
			do {
				start_s = new DateTime();
				if (fr.getIsLoop()) {
					tita = fr.getOkmsg().getBytes(BIG5);
				}
				if(log.isTraceEnabled()) {
					log.trace("before tita>>\n{}", HexDumpUtil.format(tita, 72, HexDumpUtil.HD_ASCII));
				}
				log.trace("fr.getKeepRecive()>>{}", fr.getKeepRecive());
				start = new DateTime();
				d3 = new DateTime();
				if (!fr.getKeepRecive()) {
					socketChannel = sc.send(socketChannel, tita);
					rs = sc.receive(socketChannel);
				} else {
					rs = sc.receive(socketChannel);
				}
//				log.info("@@>第{}次折返，電文收送花費時間>>{}毫秒",cnt,new Duration(start, new DateTime()).getMillis());
				d4 = new DateTime();		
				loopTime.put( cnt+"" , DateUtil.getDiffTimeMills(d3, d4));
				fr.reset();
				rs = fundStatusCheck(fr, rs,stickyVector);
				log.debug("fr.getIsTOTA>>{}",fr.getIsTOTA());
				log.debug("fr.getIsSticky>>{}",fr.getIsSticky());
				if (fr.getIsTOTA()) {
//					沒黏包使用以下邏輯，有黏包會在fundStatusCheck 處理好 放在stickyVector
					if(!fr.getIsSticky()) {
						log.trace("after rs  >>\n{}", HexDumpUtil.format(rs, 72, HexDumpUtil.HD_ASCII));
						rs = Arrays.copyOfRange(rs, 4, rs.length);
						log.trace("after rs1 >>\n{}", HexDumpUtil.format(rs, 72, HexDumpUtil.HD_ASCII));
						answerVector.add(rs);
						log.debug("answerVector.size>>{}",answerVector.size());
						Boolean isloop = processTOTA(id, answerVector, message, totaVector);
						log.debug("totaVector.size>>{}",totaVector.size());
						
						log.trace("isloop>>{}", isloop);
						answerVector.clear();
						fr.setIsLoop(isloop);
						if(!isloop) {
							fr.setKeepRecive(Boolean.FALSE);
						}
					}else {
						
						answerVector.clear();
						for(byte[] rss:stickyVector) {
							answerVector.add(rss);
							Boolean isloop = processTOTA(id, answerVector, message, totaVector);
							log.debug("sticky.totaVector.size>>{}",totaVector.size());
							log.trace("sticky.isloop>>{}", isloop);
							answerVector.clear();
							fr.setIsLoop(isloop);
							if(!isloop) {
								fr.setKeepRecive(Boolean.FALSE);
							}
						}//for end
					}//if end
				}
				log.trace("getIsLoop>>{}", fr.getIsLoop());
				log.info("##>第{}次折返，電文處理花費時間>>{}毫秒",cnt,new Duration(start_s, new DateTime()).getMillis());
				cnt++;
			} while (fr.getIsLoop());

			result = Boolean.TRUE;

		} catch (CommunicationException e) {
			log.error("CommunicationException>>", e);
			setMsgCode(e.getErrorCode(), tempList);
			sendMail(e.getErrorCode(), "FUND", txid);
		} catch (Exception e) {
			log.error("", e);
			setMsgCode("Z018", tempList);
			sendMail("Z018", "FUND", txid);
		} finally {
			dataList = new LinkedList();
			if (result) {
				dataList.addAll(totaVector);
			} else {
				dataList.addAll(tempList);

			}

			if (sc != null && socketChannel !=null) {
				sc.close(socketChannel);
			}

			d2 = new DateTime();
			pstimediff = DateUtil.getDiffTimeMills(d1, d2);
			log.info("api>>{} ,pstimediff>>{}",ESAPIUtil.vaildLog(txid),pstimediff);
			log.info("loopTime>>{}",CodeUtil.toJson(loopTime) );
			
		}
		log.trace("dataList>>{}", dataList);
		return dataList;

	}
	
	
	public void sendMail(String errorCode , String channel ,String txid) {
		HashMap<String, Object> map;
		String subject = "";
		String content = "";
		
		MailUtil mailutil = null;
		List<String> mailList = new ArrayList<String>();
		try {
			mailutil = new MailUtil();
			log.info("mails>>{}",mails);
			mailList.add(mails);
			content = mailutil.getMailContentForGW(errorCode, channel ,txid ,tmraEnv);
			switch (tmraEnv) {
			case "D":
				subject = "開發套新個網NB3GW異常通知";
				break;
			case "T":
				subject = "測試套新個網NB3GW異常通知";
				break;
			default:
				subject = "新個網NB3GW異常通知";
				break;
			}
			restutil.SendAdMail_REST(subject, content, mailList,"1" ,ms_psChannel);
				
		} catch (Exception e) {
			log.error("{}", e.toString());
		}
	}
	

	public void addD4Params(Hashtable message, String txid, Integer id) throws CommunicationException {
		ArrayList temp = null;
		temp = DataTemplateX.nameMaps[id];

		if (temp != null) {
			log.trace("temp.get(2)>>{},temp.get(3)>>{},temp.get(4)>>{}", temp.get(2), temp.get(3), temp.get(4));
			message.put("CLIENTENCODING", temp.get(2) + "");
			message.put("HOSTENCODING", temp.get(3) + "");
			message.put("TOPMSG", temp.get(4) + "");
			if (null != temp.get(5)) {
				message.put("TOPMAXRECOD", (String) temp.get(5));
			}
		} else {
			throw new CommunicationException("M1", "Z002", "There is something wrong in Telegram.xml" + txid);
		}
	}
	
	
	
	/**
	 * 設定回覆訊息
	 * 
	 * @param msgCode
	 * @param tempVector
	 */
	public void setMsgCode(String msgCode, List tempVector) {
		tempVector.clear();
		MsgMap.geterrmsg(msgCode, tempVector);
	}

	// public String getFundHostName() {
	// return fundHostName;
	// }
	//
	// public void setFundHostName(String fundHostName) {
	// this.fundHostName = fundHostName;
	// }
	//
	// public Integer getFundPortNumber() {
	// return fundPortNumber;
	// }
	//
	// public void setFundPortNumber(Integer fundPortNumber) {
	// this.fundPortNumber = fundPortNumber;
	// }
	//
	// public String getXmlFilePath() {
	// return xmlFilePath;
	// }
	//
	// public void setXmlFilePath(String xmlFilePath) {
	// this.xmlFilePath = xmlFilePath;
	// }

	public byte[] fundStatusCheck(FundResult fr, byte[] rs , Vector totaList) {
		Boolean isSticky = Boolean.FALSE;
		try {
			log.trace("rs.length>>{}", rs.length);
			log.trace("rs.indexOf>>{}", Bytes.indexOf(rs, fr.getOkmsg().getBytes(BIG5)));
			// 表示 後面還有資料
			if (rs.length == 7 && Bytes.indexOf(rs, fr.getOkmsg().getBytes(BIG5)) != -1) {
				fr.setIsLoop(Boolean.TRUE);
//				fr.setKeepRecive(Boolean.TRUE);
				fr.setKeepRecive(Boolean.FALSE);
				isSticky = stickyCheck(rs);
			}

			// 黏包表示0007*OK 後面有電文資料要擷取給TOTA
			if (rs.length > 7 && Bytes.indexOf(rs, fr.getOkmsg().getBytes(BIG5)) != -1) {
				rs = Arrays.copyOfRange(rs, 7, rs.length);
				fr.setIsTOTA(Boolean.TRUE);
				isSticky = stickyCheck(rs);
			}
			// 正常電文且開頭無0007*OK
			if (rs.length > 11 && (Bytes.indexOf(rs, "0210".getBytes(BIG5)) != -1 || Bytes.indexOf(rs, "0211".getBytes(BIG5)) != -1)) {
//				if (rs.length > 11 && Bytes.indexOf(rs, "0210".getBytes(BIG5)) != -1) {
				fr.setIsTOTA(Boolean.TRUE);
				isSticky = stickyCheck(rs);
			}
			log.trace("isloop>>{},IsTOTA>>{} ,KeepRecive>>{}", fr.getIsLoop(), fr.getIsTOTA(), fr.getKeepRecive());
			log.debug("isSticky>>{}",isSticky);
			if(isSticky) {
				fr.setIsTOTA(Boolean.TRUE);
				fr.setIsSticky(Boolean.TRUE);
				log.debug("yes..Sticky..");
				spiltByteArray(rs, totaList);
			}
			
		} catch (Exception e) {
			log.error("", e);
		}
		return rs;

	}
	
	
	public Boolean stickyCheck(byte[] rs) {
		log.debug("rs.length>>{}",rs.length);
		log.debug("rs.length>>{}",new String(rs));
		
		try {
			byte[] lengthByte = Arrays.copyOfRange(rs, 0, 4);
			Integer length =  Integer.valueOf(new String(lengthByte));
			log.debug("stickyCheck.length>>{}",length);
			log.debug("stickyCheck.rs.length>>{}",rs.length);
			if(rs.length > length) {
				log.warn("警告...黏包");
				return Boolean.TRUE;
			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			log.warn("stickyCheck parseInt 警告... 判斷可能已無資料，無黏包");
			log.warn("stickyCheck>>",e);
			return Boolean.FALSE;
		}
		
		return Boolean.FALSE;
		
	}


	public void spiltByteArray(byte[] rs ,Vector<byte[]> totaList ) {
		 byte[] tmp = null;
		 Boolean hasNext = Boolean.FALSE;
		 Integer tel_len = null;
		 Integer spilt_point = null;
		 Integer spilt_point_s = null;
		 Integer max_Loop = 3;
		 Integer cnt = 0;
		 
		 try {
			do {
//				 tel_len =  Integer.valueOf(new String(Arrays.copyOfRange(rs, 0, 4))) ;
				 spilt_point = Bytes.indexOf(rs, "0210".getBytes());
				 log.debug("spilt_point>>{}",spilt_point);
//			 攔截點前7是電文長度開頭
				 spilt_point_s = spilt_point-7;
				 log.debug("spilt_point_s>>{}",spilt_point_s);
				 tel_len = Integer.valueOf(new String( Arrays.copyOfRange(rs, spilt_point_s, spilt_point_s+4)));
				 log.debug("tel_len>>{}",tel_len);
//				 根據電文長度切割出正確電文
				 tmp = Arrays.copyOfRange(rs, spilt_point_s, tel_len);
//				 放進去前去掉前面電文長度4碼
				 tmp = Arrays.copyOfRange(tmp,4, tmp.length);
				 totaList.add(tmp);
				 if(log.isTraceEnabled()) {
					 log.trace("rs tmp  >>\n{}", HexDumpUtil.format(tmp, 72, HexDumpUtil.HD_ASCII));
					 log.trace("rs rs 0  >>\n{}", HexDumpUtil.format(rs, 72, HexDumpUtil.HD_ASCII));
				 }
//				 切割電文後剩下的電文
				 rs =  Arrays.copyOfRange(rs, tel_len, rs.length);
				 if(log.isTraceEnabled()) {
					 log.trace("rs rs  >>\n{}", HexDumpUtil.format(rs, 72, HexDumpUtil.HD_ASCII));
				 }
//				 判斷電文是否黏包
				 if(stickyCheck(rs)) {
					 hasNext = Boolean.TRUE;
//					 totaList.add(tmp);
				 }else {
					 hasNext = Boolean.FALSE;
					 tmp = null;
					 tmp = Arrays.copyOfRange(rs,4, rs.length);
					 totaList.add(tmp);
				 }
				 cnt++;
				 if(cnt==max_Loop) {
					 log.warn("警告 電文迴圈已達>>{},強制脫離..",cnt);
					 hasNext = Boolean.FALSE;
				 }
			} while (hasNext);
			 
			 
			 for(byte[] vb : totaList) {
				 if(log.isTraceEnabled()) {
					 log.trace("totaList rs  >>\n{}", HexDumpUtil.format(vb, 72, HexDumpUtil.HD_ASCII));
				 }
			 }
		} catch (NumberFormatException e) {
			log.error("NumberFormatException>>{}",e);
		}
		 
		 
	}
	
	
	
	public Map test1(String txid) {

		String s1 = "0094   ";
		// 長度應該要16
		log.trace("s1.length()>>{}", s1.length());
		SocketClient sc = null;
		byte[] tita = null;
		setFundHostName("127.0.0.1");
		setFundPortNumber(4501);
		setXmlFilePath("D:/mock_data/xml/");
		Vector totaVector = null;
		Vector answerVector = null;
		try {
			DataTemplateX.XML_FILE_PATH = getXmlFilePath();
			int id = -1;

			id = DataTemplateX.getID(txid);
			if (id < 0) {
				throw new CommunicationException("M1", "ZE007", "TXID can't be found in the hashtable");
			}
			log.trace("id>>{}", id);
			Hashtable message = new Hashtable();
			message.put("CUSIDN", "A123456814");

			sc = new SocketClient();
			SocketChannel socketChannel = sc.connect(getFundHostName(), getFundPortNumber());
			log.trace("SocketChannel>>{}", socketChannel);

			answerVector = new Vector();
			totaVector = new Vector();
			Boolean isloop = Boolean.FALSE;

			tita = getTITABytes(txid, message, id);

			// socketChannel = sc.send(socketChannel, tita);
			// byte[] rs =sc.receiveII(socketChannel);
			// String s = new String(rs);
			// socketChannel = sc.send(socketChannel, s.getBytes("big5"));
			// rs =sc.receive(socketChannel);
			//
			// answerVector.add(rs);
			// processTOTA(id, answerVector, message, totaVector);

			// socketChannel = sc.send(socketChannel, s.getBytes("big5"));
			// rs =sc.receiveII(socketChannel);
			FundResult fr = new FundResult();
			byte[] rs = null;
			do {
				if (fr.getIsLoop()) {
					tita = fr.getOkmsg().getBytes(BIG5);
				}
				log.trace("before tita>>\n{}", HexDumpUtil.format(tita, 72, HexDumpUtil.HD_ASCII));
				if (!fr.getKeepRecive()) {
					socketChannel = sc.send(socketChannel, tita);
					rs = sc.receive(socketChannel);
				} else {
					rs = sc.receive(socketChannel);
				}
				fr.reset();
				rs = fundStatusCheck(fr, rs,answerVector);
				if (fr.getIsTOTA()) {
					log.trace("after rs  >>\n{}", HexDumpUtil.format(rs, 72, HexDumpUtil.HD_ASCII));
					rs = Arrays.copyOfRange(rs, 4, rs.length);
					log.trace("after rs1 >>\n{}", HexDumpUtil.format(rs, 72, HexDumpUtil.HD_ASCII));
					answerVector.add(rs);
					isloop = processTOTA(id, answerVector, message, totaVector);
					log.trace("isloop>>{}", isloop);
					answerVector.clear();
					fr.setIsLoop(isloop);
				}
				log.trace("getIsLoop>>{}", fr.getIsLoop());
			} while (fr.getIsLoop());

		} catch (Exception e) {
			log.error("", e);
		}

		return null;

	}


	
	
//	public static void main(String[] args) {
//		// TODO Auto-generated method stub
//		FundTransactionImpl fund = new FundTransactionImpl();
//		try {
//			fund.test1("C021");
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}

}
