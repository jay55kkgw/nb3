package com.netbank.telcomm;

import java.io.UnsupportedEncodingException;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Vector;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.netbank.rest.util.RESTUtil;
import com.netbank.socketClient.SocketClient;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;
import com.netbank.util.HexDumpUtil;

import bank.comm.CommunicationException;
import bank.comm.ConvCode;
import bank.comm.DataTemplateX;
import bank.comm.FlowObject;
import bank.comm.HexDump;
import bank.comm.MsgMap;
import bank.comm.TelegramDataList;
import fstop.util.MailUtil;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 功能說明 : 整合收發電文的功能,根據收發的邏輯<br>
 * 完成一筆電文交易,該類別實作之交易邏輯具有下列特徵 : <br>
 * (1)以EBCDIC編碼方式發送電文及接收電文<br>
 * (2)必須根據中心回傳的訊息編組電文上傳中心以繼續接收資料或結束交易
 */
@Component
@Slf4j
@Data
public class PARKTransactionImpl {
	@Value("${parkHostName}")
	private String parkHostName;
	@Value("${parkPortNumber}")
	private Integer parkPortNumber;
	@Value("${xmlFilePath}")
	private String xmlFilePath;


	@Value("${mails:sox@tbb.mail.com.tw,H15D302B@mail.tbb.com.tw}")
	private String mails = "";
	@Value("${tmraEnv:P}")
	private String tmraEnv = "";
	@Value("${ms_psChannel:http://msps:9080/ms_ps/com/}")
	private String ms_psChannel = "";
	
	@Autowired
	RESTUtil restutil;

	@PostConstruct
	public void init() {
		DataTemplateX.XML_FILE_PATH = xmlFilePath;
		log.debug("XML_FILE_PATH>>{}", DataTemplateX.XML_FILE_PATH);
		log.debug("xmlFilePath>>{}", xmlFilePath);
	}

	/**
	 * 取得上行電文
	 * 
	 * @param
	 * @return
	 * @throws CommunicationException
	 */
	public byte[] getTITABytes(String txid, Hashtable message, Integer id, Hashtable TOTAByteRecord)
			throws CommunicationException {
		byte[] TITAArray = new byte[4096];
		byte[] buf = null;
		FlowObject titaFlow = DataTemplateX.TITAFlow[id];
		TelegramDataList tita = DataTemplateX.TITA[id];
		Hashtable positionMapping = new Hashtable();
		Vector input = new Vector();
		try {
			input.add(message);
			log.trace("titaFlow>>{}", titaFlow);
			log.trace("titaFlow,tita>>{}", tita);
			int titaLength = titaFlow.getTITABytes(tita, input, TITAArray, 0, TOTAByteRecord, positionMapping);
			positionMapping.put("TITAByteStreamEndingPosition", new Integer(titaLength));
			log.debug("titaLength>>{}", titaLength);
			// 把byte[]多餘的長度移除 ps:不是把空白拿掉
			buf = new byte[titaLength];
			System.arraycopy(TITAArray, 0, buf, 0, titaLength);
			if(log.isDebugEnabled()) {
				log.debug("HexDumpUtil.buf000>>\n{}", HexDumpUtil.format(buf, 72, HexDumpUtil.HD_EBCDIC));
			}
//			字首PARKING 8碼不需要
			buf = Arrays.copyOfRange(buf, 8, buf.length);
			if(log.isDebugEnabled()) {
				log.debug("HexDumpUtil.buf001>>\n{}", HexDumpUtil.format(buf, 72, HexDumpUtil.HD_EBCDIC));
			}
			byte[] tmp = CodeUtil.getParkTelHeader(buf.length);
			buf = CodeUtil.combineByte(tmp, buf);
			log.trace("buf.length>>{}", buf.length);
			if(log.isDebugEnabled()) {
				log.debug("HexDumpUtil.buf>>\n{}", HexDumpUtil.format(buf, 72, HexDumpUtil.HD_EBCDIC));
			}
		} catch (Exception e) {
			log.error("{}", e);
		}

		return buf;
	}

	/**
	 * 處理下行電文
	 * 
	 * @param ID
	 * @param answerVector
	 * @param message
	 * @param result
	 * @param TOTAByteRecord
	 * @return
	 * @throws CommunicationException
	 */
	public Boolean processTOTA(int ID, Vector answerVector, Hashtable message, Vector result, Hashtable TOTAByteRecord)
			throws CommunicationException {
		byte[] answer = (byte[]) answerVector.get(0);
		boolean resultType = false;
		FlowObject totaFlow = DataTemplateX.TOTAFlow[ID];
		TelegramDataList tota = DataTemplateX.TOTA[ID];
		String userdata_x50 = "";
		totaFlow.SetMaxRecodMsg("");
		int cnt = 0;
		// Hashtable TOTAByteRecord = new Hashtable(); //拆解TOTA stream成(name,byte[])
		// pair 6.6.23
		int status = totaFlow.getTOTATable(tota, answer, result, 0, message, TOTAByteRecord);
		log.trace("status>>{}", status);
		log.trace("result>>{}", result);
		log.trace("TOTAByteRecord>>{}", TOTAByteRecord);
		log.trace("totaFlow.contmaxrecodemsg >>{} }", totaFlow.contmaxrecodemsg);

		String retmaxmsg = totaFlow.GetMaxRecodMsg();

		log.trace("retmaxmsg >>{} }", retmaxmsg);

		if (retmaxmsg.equals(totaFlow.contmaxrecodemsg) && totaFlow.contmaxrecodemsg.length() > 0 && status == 0) {
			String nextquery = HashToStr(message, TOTAByteRecord);
			log.trace("String nextquery = {}",ESAPIUtil.vaildLog(nextquery) );
			Hashtable h = new Hashtable();
			h.put("QUERYNEXT", nextquery);
			result.add(h);
			// tempVector.add(h);
			// result.setElement(tempVector);
			// return (FSTopResultSet)result;
		}

		if (status == 1)
			return false;
		else if (status == 0)
			return true;

		return false;

	}

	/**
	 * 處理電文交易
	 * 
	 * @param txid
	 * @param params
	 * @return
	 */
	public List processTransaction(String txid, Map params) {
		int id = -1;
		byte[] rs = null;
		byte[] tita = null;
		List tempList = null;
		List dataList = null;
		Vector totaVector = null;
		Vector answerVector = null;
		Hashtable message = null;
		Boolean result = Boolean.FALSE;
		SocketClient sc = null;
		SocketChannel socketChannel = null;
		try {

			tempList = new LinkedList();
			dataList = new LinkedList();
			// 過濾掉 value 是null 避免Hashtable error
			params.values().removeIf(Objects::isNull);
			message = new Hashtable();
			message.putAll(params);


			id = DataTemplateX.getID(txid);
			log.trace("id>>{}", id);
			if (id <= 0) {
				throw new CommunicationException("M1", "Z007", "TXID can't be found in the hashtable");
			}

			addD4Params(message, txid, id);
			answerVector = new Vector();
			totaVector = new Vector();
			Hashtable TOTAByteRecord = new Hashtable();
			sc = new SocketClient();
			socketChannel = sc.connect(getParkHostName(), getParkPortNumber());
			log.trace("loop..");
			int cnt = 1;
			do {
				answerVector.clear();
				log.trace("message>>{}", ESAPIUtil.vaildLog(CodeUtil.toJson(message) ));
				tita = getTITABytes(txid, message, id, TOTAByteRecord);
				
				writeSNA_Stylelog(tita, "tita");
				
				socketChannel = sc.send(socketChannel,tita );
				rs = sc.receive(socketChannel);
				rs = Arrays.copyOfRange(rs, 2, rs.length);
				answerVector.add(rs);
				result = processTOTA(id, answerVector, message, totaVector, TOTAByteRecord);
				log.trace("totaVector>>", totaVector);
				cnt++;
				// result = cnt >3? Boolean.TRUE:Boolean.FALSE;
			} while (!result);
			result = Boolean.TRUE;
		} catch (CommunicationException e) {
			log.error("CommunicationException>>", e);
			setMsgCode(e.getErrorCode(), tempList);
			sendMail(e.getErrorCode(), "PARK", txid);
		} catch (Exception e) {
			log.error("", e);
			setMsgCode("Z018", tempList);
			sendMail("Z018", "PARK", txid);
		} finally {
			if (result) {

				dataList.addAll(totaVector);
			} else {
				dataList.addAll(tempList);
			}
		}
		log.trace("dataList>>{}", dataList);
		return dataList;

	}

	public void writeSNA_Stylelog(byte[] b ,String keyword) {
		HexDump hd = new HexDump();
		log.trace(keyword+">>{}\n",
				hd.toHexString2(HexDump.HD_EBCDIC, b,b.length));
	}
	
	
	
	public void sendMail(String errorCode , String channel ,String txid) {
		HashMap<String, Object> map;
		String subject = "";
		String content = "";
		
		MailUtil mailutil = null;
		List<String> mailList = new ArrayList<String>();
		try {
			mailutil = new MailUtil();
			log.info("mails>>{}",mails);
			mailList.add(mails);
			content = mailutil.getMailContentForGW(errorCode, channel ,txid ,tmraEnv);
			switch (tmraEnv) {
			case "D":
				subject = "開發套新個網NB3GW異常通知";
				break;
			case "T":
				subject = "測試套新個網NB3GW異常通知";
				break;
			default:
				subject = "新個網NB3GW異常通知";
				break;
			}
			restutil.SendAdMail_REST(subject, content, mailList,"1" ,ms_psChannel);
				
		} catch (Exception e) {
			log.error("{}", e.toString());
		}
	}
	
	
	/**
	 * 加入預設參數，來源是電文xml
	 * 
	 * @param message
	 * @param txid
	 * @param id
	 * @throws CommunicationException
	 */
	public void addD4Params(Hashtable message, String txid, Integer id) throws CommunicationException {
		ArrayList temp = null;
		temp = DataTemplateX.nameMaps[id];

		if (temp != null) {
			log.trace("temp.get(2)>>{},temp.get(3)>>{},temp.get(4)>>{}", temp.get(2), temp.get(3), temp.get(4));
			message.put("CLIENTENCODING", temp.get(2) + "");
			message.put("HOSTENCODING", temp.get(3) + "");
			message.put("TOPMSG", temp.get(4) + "");
			if (null != temp.get(5)) {
				message.put("TOPMAXRECOD", (String) temp.get(5));
			}
		} else {
			throw new CommunicationException("M1", "Z002", "There is something wrong in Telegram.xml" + txid);
		}
	}

	public void setMsgCode(String msgCode, List tempVector) {
		// public void setMsgCode(String msgCode , Hashtable herrmsg , Hashtable
		// htl_Data , Vector tempVector) {
		// herrmsg.clear();
		// htl_Data.clear();
		tempVector.clear();
		MsgMap.geterrmsg(msgCode, tempVector);
		// herrmsg.put("TOPMSG",htl_Data.get("clierrcod"));
		// herrmsg.put("TOPMSGTEXT",htl_Data.get("clierrtext"));
		// tempVector.add(herrmsg);
	}


	public Hashtable StrToHash(String NextQuery) {
		Hashtable RetHas = new Hashtable();
		String Key = "";
		String Value = "";
		byte[] tmp = null;// =new byte[NextQuery.length()];
		byte[] tmp1 = new byte[301];
		int i = 0, j = 0;
		try {
			tmp = NextQuery.getBytes("BIG5");
		} catch (UnsupportedEncodingException e) {
			// TODO 自動產生 catch 區塊
			tmp = NextQuery.getBytes();
		}
		while (i < NextQuery.length()) {
			if (tmp[i] == '[') {
				i++;
				j = 0;
				tmp1 = new byte[301];
				while (true) {

					tmp1[j] = tmp[i];
					i++;
					j++;
					if (tmp[i] == '=')
						break;
				}
				Key = new String(tmp1).trim();
				System.out.println("Key=" + Key);
				i++;
				j = 0;
				tmp1 = new byte[301];
				while (true) {
					// if(tmp[i]=='[')
					// i++;
					if (tmp[i] == ']') {
						i++;
						break;
					}
					tmp1[j] = tmp[i];
					i++;
					j++;
				}
				// Value=new String(tmp1).trim();
				byte[] valuearray = new byte[j];
				System.arraycopy(tmp1, 0, valuearray, 0, j);
				try {
					Value = new String(valuearray, "BIG5");
				} catch (UnsupportedEncodingException e) {
					// TODO 自動產生 catch 區塊
					Value = new String(valuearray);
				}
				System.out.println("Value=" + Value);
				RetHas.put(Key, Value);
			}
		}
		return (RetHas);
	}

	public String HashToStr(Hashtable Tita, Hashtable ReTota) {
		String RetStr = new String();
		String txid = (String) Tita.get("HOSTMSGID");
		int userdataoffset = 0;
		// String DateStr=new String();
		// String TimeStr=new String();
		Hashtable sumary = new Hashtable();
		/*
		 * if(txid.equals("N130")) { userdataoffset=74; } else if(txid.equals("N133")) {
		 * userdataoffset=85; }
		 */
		for (Object key : Tita.keySet()) {
			// RetStr=RetStr+"["+key+"="+Tita.get(key)+"]";
			sumary.put(key, Tita.get(key));
		}

		for (Object key : ReTota.keySet()) {
			log.trace("key>>{}", key);
			// 20190429 add by hugo 這邊跟舊網銀不同 針對抓出來是Vector 的物件做個過濾
			if (ReTota.get(key) != null && ReTota.get(key) instanceof Vector) {
				log.trace("ReTota.Vector>>{}", ReTota.get(key));
				continue;
			}

			byte[] tmpobj = (byte[]) ReTota.get(key);
			String valueStr;
			try {
				valueStr = new String(ConvCode.host2pc(tmpobj), "BIG5");
			} catch (UnsupportedEncodingException e) {
				// TODO 自動產生 catch 區塊
				valueStr = new String(ConvCode.host2pc(tmpobj));
			}
			/*
			 * if(key.equals("USERDATA") && (txid.equals("N130") || txid.equals("N133"))) {
			 * String tmpvalue=valueStr.substring(0, userdataoffset) +"000"+
			 * valueStr.substring(userdataoffset+3); valueStr=tmpvalue; // new
			 * HexDump(HexDump.HD_ASCII,"USERDATA",valueStr.getBytes(),valueStr.length()); }
			 * else
			 */
			if (valueStr.equals("OKOV")) {
				valueStr = "";
			} else {
				// String titavalue=(String)Tita.get(key);
				// if(titavalue!=null)
				// Tita.put(key,valueStr);
			}
			sumary.put(key, valueStr);
			// RetStr=RetStr+"["+key+"="+ valueStr +"]";
			// System.out.println(key +"=" + valueStr);
		}
		for (Object key : sumary.keySet()) {
			RetStr = RetStr + "[" + key + "=" + sumary.get(key) + "]";
		}
		return (RetStr);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PARKTransactionImpl tmra = new PARKTransactionImpl();
		Hashtable map = new Hashtable();
		// map.put("CUSIDN", "Y191673184");
		map.put("CUSIDN", "W100089655");
		// map.put("CUSIDN", "A123456814");
		// map.put("ACN", "00123456789");
		byte[] tita = null;
		try {
			// tita = tmra.getTITABytes("N110", map);
			// tmra.processTransaction("N110", map);
			tmra.processTransaction("N810", map);
			// tmra.sendtest();
			// tmra.sendtest2();
			// System.out.println("tita>>"+tmra.EBCDICtoBig5(tita));
		} catch (Exception e) {
			log.error("",e);
		}
	}

}
