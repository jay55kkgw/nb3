package com.netbank.domain.orm.core;


import java.io.Serializable;
import java.util.List;

public interface CoreRepository<T, ID extends Serializable> {
    long count();

    T findById(ID var1);

    List<T> findAll();

    void save(T var1);

    T update(T var1);

    void delete(T var1);
}
