package com.netbank.domain.orm.core;

import javax.persistence.*;

import com.netbank.util.ESAPIUtil;

import java.util.*;

public class QueryAdapter implements Query {
    private Query query;
    public QueryAdapter(Query query) {
        this.query = query;
    }


    @Override
    public List getResultList() {
        return  ESAPIUtil.validList(query.getResultList());
    }

    @Override
    public Object getSingleResult() {
        return ESAPIUtil.escapeHTML(query.getSingleResult());
    }

    @Override
    public int executeUpdate() {
        return query.executeUpdate();
    }

    @Override
    public Query setMaxResults(int i) {
        return query.setMaxResults(i);
    }

    @Override
    public int getMaxResults() {
        return query.getMaxResults();
    }

    @Override
    public Query setFirstResult(int i) {
        return query.setFirstResult(i);
    }

    @Override
    public int getFirstResult() {
        return query.getFirstResult();
    }

    @Override
    public Query setHint(String s, Object o) {
        return query.setHint(s, o);
    }

    @Override
    public Map<String, Object> getHints() {
        return query.getHints();
    }

    @Override
    public <T> Query setParameter(Parameter<T> parameter, T t) {
        return query.setParameter(parameter, t);
    }

    @Override
    public Query setParameter(Parameter<Calendar> parameter, Calendar calendar, TemporalType temporalType) {
        return query.setParameter(parameter, calendar, temporalType);
    }

    @Override
    public Query setParameter(Parameter<Date> parameter, Date date, TemporalType temporalType) {
        return query.setParameter(parameter, date, temporalType);
    }

    @Override
    public Query setParameter(String s, Object o) {
        return query.setParameter(s, o);
    }

    @Override
    public Query setParameter(String s, Calendar calendar, TemporalType temporalType) {
        return query.setParameter(s, calendar, temporalType);
    }

    @Override
    public Query setParameter(String s, Date date, TemporalType temporalType) {
        return query.setParameter(s, date, temporalType);
    }

    @Override
    public Query setParameter(int i, Object o) {
        return query.setParameter(i+1, o);
    }

    @Override
    public Query setParameter(int i, Calendar calendar, TemporalType temporalType) {
        return query.setParameter(i+1, calendar, temporalType);
    }

    @Override
    public Query setParameter(int i, Date date, TemporalType temporalType) {
        return query.setParameter(i+1, date, temporalType);
    }

    @Override
    public Set<Parameter<?>> getParameters() {
        return query.getParameters();
    }

    @Override
    public Parameter<?> getParameter(String s) {
        return query.getParameter(s);
    }

    @Override
    public <T> Parameter<T> getParameter(String s, Class<T> aClass) {
        return null;
    }

    @Override
    public Parameter<?> getParameter(int i) {
        return query.getParameter(i + 1);
    }

    @Override
    public <T> Parameter<T> getParameter(int i, Class<T> aClass) {
        return query.getParameter(i + 1, aClass);
    }

    @Override
    public boolean isBound(Parameter<?> parameter) {
        return query.isBound(parameter);
    }

    @Override
    public <T> T getParameterValue(Parameter<T> parameter) {
        return query.getParameterValue(parameter);
    }

    @Override
    public Object getParameterValue(String s) {
        return query.getParameter(s);
    }

    @Override
    public Object getParameterValue(int i) {
        return query.getParameter(i + 1);
    }

    @Override
    public Query setFlushMode(FlushModeType flushModeType) {
        return query.setFlushMode(flushModeType);
    }

    @Override
    public FlushModeType getFlushMode() {
        return query.getFlushMode();
    }

    @Override
    public Query setLockMode(LockModeType lockModeType) {
        return query.setLockMode(lockModeType);
    }

    @Override
    public LockModeType getLockMode() {
        return query.getLockMode();
    }

    @Override
    public <T> T unwrap(Class<T> aClass) {
        return query.unwrap(aClass);
    }
}
