package com.netbank.domain.orm.core;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.poi.ss.formula.functions.T;

public class SessionAdapter {
    private EntityManager entityManager;
    public SessionAdapter(EntityManager manager) {
        this.entityManager = manager;
    }

    public Query createQuery(String sql) {

        return new QueryAdapter(entityManager.createNativeQuery(sql));
    }

    public Query createSQLQuery(String sql) {
        sql = JPAUtils.replaceToJpaStyle(sql);
        return new QueryAdapter(entityManager.createNativeQuery(sql));
    }
//    20190924 add  hugo
    public Query createNativeQuery(String sql,Class c) {
    	sql = JPAUtils.replaceToJpaStyle(sql);
    	return new QueryAdapter(entityManager.createNativeQuery(sql,c));
    }
}
