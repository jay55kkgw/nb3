package com.netbank.domain.orm.core;

public class JPAUtils {
    public static String replaceToJpaStyle(String hql) {

        char[] chars = hql.toCharArray();
        StringBuilder sb = new StringBuilder();

        int index = 0;
        for(char c : chars) {
            if(c == '?') {
                sb.append(c).append(index + 1);
                index++;
            } else {
                sb.append(c);
            }
        }

        return sb.toString();

    }
}
