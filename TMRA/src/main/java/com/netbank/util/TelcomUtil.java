package com.netbank.util;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import com.netbank.telcomObject.Tita;
import com.netbank.telcomObject.TitaTotaList;
import com.netbank.telcomObject.Tota;
import lombok.extern.slf4j.Slf4j;

/**
 * 電文工具程式
 */
@Slf4j
public class TelcomUtil{
	/**
	 * 事先放參數
	 */
	public static void preProgress(Map<String,String> parameterMap){
		if("N015".equals(parameterMap.get("transactionCode"))){
			parameterMap.put("SEQ","001");
		}
		else if("N510".equals(parameterMap.get("transactionCode"))){
			parameterMap.put("TRNSRC","NB");
		}
		else if("N523".equals(parameterMap.get("transactionCode"))){
			parameterMap.put("TRNSRC","NB");
		}
		else if("N524".equals(parameterMap.get("transactionCode"))){
			parameterMap.put("TRNSRC","NB");
		}
		else if("N530".equals(parameterMap.get("transactionCode"))){
			parameterMap.put("TRNSRC","NB");
		}
		else if("N550".equals(parameterMap.get("transactionCode"))){
			parameterMap.put("TRNSRC","NB");
		}
		else if("N554".equals(parameterMap.get("transactionCode"))){
			parameterMap.put("TRNSRC","NB");
		}
		else if("N810".equals(parameterMap.get("transactionCode"))){
			parameterMap.put("SEQ","01");
		}
		else if("N920".equals(parameterMap.get("transactionCode"))){
			parameterMap.put("SEQ","01");
		}
	}
	/**
	 * 組HEADER和BASICDATA
	 */
	public static byte[] getHeaderAndBasicData(Map<String,String> parameterMap){
		byte[] result = null;
		byte[] byteOne = null;
		byte[] byteTwo = null;
		
		try{
			String transactionCode = parameterMap.get("transactionCode");
			log.debug("transactionCode={}",transactionCode);
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
			
			//現在時間
			String nowDateTime = sdf.format(new Date());
			log.debug("nowDateTime={}",nowDateTime);
			
			String ROCYear = String.valueOf(Integer.valueOf(nowDateTime.substring(0,4)) - 1911);
			log.debug("ROCYear={}",ROCYear);
			String month = nowDateTime.substring(4,6);
			log.debug("month={}",month);
			String day = nowDateTime.substring(6,8);
			log.debug("day={}",day);
			String time = nowDateTime.substring(8,14);
			log.debug("time={}",time);
			
			//判斷電文代號
			if("N014".equals(transactionCode)){
				byteOne = new byte[4];
				byteOne[0] = 0x00;
				//上行電文的長度，需包含此四個BYTE
				byteOne[1] = (byte)181;
				byteOne[2] = 0x00;
				byteOne[3] = 0x00;
				log.debug("byteOne={}",byteOne);
				
				String text = "TNB8MBBT " + transactionCode + "                                                                                NB08 ";
				log.debug("text={}",text);
				
				//轉成CP937
				byteTwo = text.getBytes("CP937");
				log.debug("byteTwo={}",byteTwo);
			}
			else if("N015".equals(transactionCode)){
				byteOne = new byte[4];
				byteOne[0] = 0x00;
				//上行電文的長度，需包含此四個BYTE
				byteOne[1] = (byte)184;
				byteOne[2] = 0x00;
				byteOne[3] = 0x00;
				log.debug("byteOne={}",byteOne);
				
				String text = "TNB8MBBT " + transactionCode + "   NB08 " + ROCYear + month + day + time + "                                                                        ";
				log.debug("text={}",text);
				
				//轉成CP937
				byteTwo = text.getBytes("CP937");
				log.debug("byteTwo={}",byteTwo);
			}
			else if("N016".equals(transactionCode)){
				byteOne = new byte[4];
				byteOne[0] = 0x00;
				//上行電文的長度，需包含此四個BYTE
				byteOne[1] = (byte)182;
				byteOne[2] = 0x00;
				byteOne[3] = 0x00;
				log.debug("byteOne={}",byteOne);
				
				String text = "TNB8MBBT " + transactionCode + "                                                                                NB08 ";
				log.debug("text={}",text);
				
				//轉成CP937
				byteTwo = text.getBytes("CP937");
				log.debug("byteTwo={}",byteTwo);
			}
			else if("N110".equals(transactionCode)){
				byteOne = new byte[4];
				byteOne[0] = 0x00;
				//上行電文的長度，需包含此四個BYTE
				byteOne[1] = (byte)173;
				byteOne[2] = 0x00;
				byteOne[3] = 0x00;
				log.debug("byteOne={}",byteOne);
				
				String text = "TNB8MBBT " + transactionCode + "                                                                                NB08 ";
				log.debug("text={}",text);
				
				//轉成CP937
				byteTwo = text.getBytes("CP937");
				log.debug("byteTwo={}",byteTwo);
			}
			else if("N130".equals(transactionCode)){
				byteOne = new byte[4];
				byteOne[0] = 0x00;
				//上行電文的長度，需包含此四個BYTE
				byteOne[1] = (byte)247;
				byteOne[2] = 0x00;
				byteOne[3] = 0x00;
				log.debug("byteOne={}",byteOne);
				
				String text = "TNB8MBBT " + transactionCode + "                                                                                NB08 ";
				log.debug("text={}",text);
				
				//轉成CP937
				byteTwo = text.getBytes("CP937");
				log.debug("byteTwo={}",byteTwo);
			}
			else if("N133".equals(transactionCode)){
				byteOne = new byte[4];
				byteOne[0] = 0x00;
				//上行電文的長度，需包含此四個BYTE
				byteOne[1] = (byte)247;
				byteOne[2] = 0x00;
				byteOne[3] = 0x00;
				log.debug("byteOne={}",byteOne);
				
				String text = "TNB8MBBT " + transactionCode + "                                                                                NB08 ";
				log.debug("text={}",text);
				
				//轉成CP937
				byteTwo = text.getBytes("CP937");
				log.debug("byteTwo={}",byteTwo);
			}
			else if("N320".equals(transactionCode)){
				byteOne = new byte[4];
				byteOne[0] = 0x00;
				//上行電文的長度，需包含此四個BYTE
				byteOne[1] = (byte)181;
				byteOne[2] = 0x00;
				byteOne[3] = 0x00;
				log.debug("byteOne={}",byteOne);
				
				String text = "TNB8MBBT " + transactionCode + "                                                                                NB08 ";
				log.debug("text={}",text);
				
				//轉成CP937
				byteTwo = text.getBytes("CP937");
				log.debug("byteTwo={}",byteTwo);
			}
			else if("N420".equals(transactionCode)){
				byteOne = new byte[4];
				byteOne[0] = 0x00;
				//上行電文的長度，需包含此四個BYTE
				byteOne[1] = (byte)173;
				byteOne[2] = 0x00;
				byteOne[3] = 0x00;
				log.debug("byteOne={}",byteOne);
				
				String text = "TNB8MBBT " + transactionCode + "                                                                                NB08 ";
				log.debug("text={}",text);
				
				//轉成CP937
				byteTwo = text.getBytes("CP937");
				log.debug("byteTwo={}",byteTwo);
			}
			else if("N510".equals(transactionCode)){
				byteOne = new byte[4];
				byteOne[0] = 0x00;
				//上行電文的長度，需包含此四個BYTE
				byteOne[1] = (byte)185;
				byteOne[2] = 0x00;
				byteOne[3] = 0x00;
				log.debug("byteOne={}",byteOne);
				
				String text = "TNBAMBBT " + transactionCode + "                                                                                NB08 ";
				log.debug("text={}",text);
				
				//轉成CP937
				byteTwo = text.getBytes("CP937");
				log.debug("byteTwo={}",byteTwo);
			}
			else if("N523".equals(transactionCode)){
				byteOne = new byte[4];
				byteOne[0] = 0x00;
				//上行電文的長度，需包含此四個BYTE
				byteOne[1] = (byte)196;
				byteOne[2] = 0x00;
				byteOne[3] = 0x00;
				log.debug("byteOne={}",byteOne);
				
				String text = "TNBAMBBT " + transactionCode + "                                                                                NB08 ";
				log.debug("text={}",text);
				
				//轉成CP937
				byteTwo = text.getBytes("CP937");
				log.debug("byteTwo={}",byteTwo);
			}
			else if("N524".equals(transactionCode)){
				byteOne = new byte[4];
				byteOne[0] = 0x00;
				//上行電文的長度，需包含此四個BYTE
				byteOne[1] = (byte)196;
				byteOne[2] = 0x00;
				byteOne[3] = 0x00;
				log.debug("byteOne={}",byteOne);
				
				String text = "TNBAMBBT " + transactionCode + "                                                                                NB08 ";
				log.debug("text={}",text);
				
				//轉成CP937
				byteTwo = text.getBytes("CP937");
				log.debug("byteTwo={}",byteTwo);
			}
			else if("N530".equals(transactionCode)){
				byteOne = new byte[4];
				byteOne[0] = 0x00;
				//上行電文的長度，需包含此四個BYTE
				byteOne[1] = (byte)179;
				byteOne[2] = 0x00;
				byteOne[3] = 0x00;
				log.debug("byteOne={}",byteOne);
				
				String text = "TNBAMBBT " + transactionCode + "                                                                                NB08 ";
				log.debug("text={}",text);
				
				//轉成CP937
				byteTwo = text.getBytes("CP937");
				log.debug("byteTwo={}",byteTwo);
			}
			else if("N550".equals(transactionCode)){
				byteOne = new byte[4];
				byteOne[0] = 0x00;
				//上行電文的長度，需包含此四個BYTE
				byteOne[1] = (byte)211;
				byteOne[2] = 0x00;
				byteOne[3] = 0x00;
				log.debug("byteOne={}",byteOne);
				
				String text = "FXQTCPIP 0000TNBAMBBT " + transactionCode + "                                                NB08        ";
				log.debug("text={}",text);
				
				//轉成CP937
				byteTwo = text.getBytes("CP937");
				log.debug("byteTwo={}",byteTwo);
			}
			else if("N552".equals(transactionCode)){
				byteOne = new byte[4];
				byteOne[0] = 0x00;
				//上行電文的長度，需包含此四個BYTE
				byteOne[1] = (byte)181;
				byteOne[2] = 0x00;
				byteOne[3] = 0x00;
				log.debug("byteOne={}",byteOne);
				
				String text = "TNBAMBBT " + transactionCode + "                                                                                NB08 ";
				log.debug("text={}",text);
				
				//轉成CP937
				byteTwo = text.getBytes("CP937");
				log.debug("byteTwo={}",byteTwo);
			}
			else if("N554".equals(transactionCode)){
				byteOne = new byte[4];
				byteOne[0] = 0x00;
				//上行電文的長度，需包含此四個BYTE
				byteOne[1] = (byte)185;
				byteOne[2] = 0x00;
				byteOne[3] = 0x00;
				log.debug("byteOne={}",byteOne);
				
				String text = "TNBAMBBT " + transactionCode + "                                                                                NB08 ";
				log.debug("text={}",text);
				
				//轉成CP937
				byteTwo = text.getBytes("CP937");
				log.debug("byteTwo={}",byteTwo);
			}
			else if("N625".equals(transactionCode)){
				byteOne = new byte[4];
				byteOne[0] = 0x00;
				//上行電文的長度，需包含此四個BYTE
				byteOne[1] = (byte)336;
				byteOne[2] = 0x00;
				byteOne[3] = 0x00;
				log.debug("byteOne={}",byteOne);
				
				String text = "TNB8MBBT " + transactionCode + "                                                                                NB08 ";
				log.debug("text={}",text);
				
				//轉成CP937
				byteTwo = text.getBytes("CP937");
				log.debug("byteTwo={}",byteTwo);
			}
			else if("N810".equals(transactionCode)){
				byteOne = new byte[4];
				byteOne[0] = 0x00;
				//上行電文的長度，需包含此四個BYTE
				byteOne[1] = (byte)50;
				byteOne[2] = 0x00;
				byteOne[3] = 0x00;
				log.debug("byteOne={}",byteOne);
				
				String text = "TVO8MBBT " + transactionCode + "   NB08 ";
				log.debug("text={}",text);
				
				//轉成CP937
				byteTwo = text.getBytes("CP937");
				log.debug("byteTwo={}",byteTwo);
			}
			else if("N813".equals(transactionCode)){
				byteOne = new byte[4];
				byteOne[0] = 0x00;
				//上行電文的長度，需包含此四個BYTE
				byteOne[1] = (byte)48;
				byteOne[2] = 0x00;
				byteOne[3] = 0x00;
				log.debug("byteOne={}",byteOne);
				
				String text = "TVO8MBBT " + transactionCode + "   NB08 ";
				log.debug("text={}",text);
				
				//轉成CP937
				byteTwo = text.getBytes("CP937");
				log.debug("byteTwo={}",byteTwo);
			}
			else if("N920".equals(transactionCode)){
				byteOne = new byte[4];
				byteOne[0] = 0x00;
				//上行電文的長度，需包含此四個BYTE
				byteOne[1] = (byte)51;
				byteOne[2] = 0x00;
				byteOne[3] = 0x00;
				log.debug("byteOne={}",byteOne);
				
				String text = "TVO9MBBT " + transactionCode + "   NB08 ";
				log.debug("text={}",text);
				
				//轉成CP937
				byteTwo = text.getBytes("CP937");
				log.debug("byteTwo={}",byteTwo);
			}
			else if("N924".equals(transactionCode)){
				byteOne = new byte[4];
				byteOne[0] = 0x00;
				//上行電文的長度，需包含此四個BYTE
				byteOne[1] = (byte)48;
				byteOne[2] = 0x00;
				byteOne[3] = 0x00;
				log.debug("byteOne={}",byteOne);
				
				String text = "TVO9MBBT " + transactionCode + "   NB08 ";
				log.debug("text={}",text);
				
				//轉成CP937
				byteTwo = text.getBytes("CP937");
				log.debug("byteTwo={}",byteTwo);
			}
			else if("TD01".equals(transactionCode)){
				byteOne = new byte[4];
				byteOne[0] = 0x00;
				//上行電文的長度，需包含此四個BYTE
				byteOne[1] = (byte)109;
				byteOne[2] = 0x00;
				byteOne[3] = 0x00;
				log.debug("byteOne={}",byteOne);

				String text = "TPNB01   NB01     " + transactionCode + "   ";
				log.debug("text={}",text);
				
				//轉成CP937
				byteTwo = text.getBytes("CP937");
				log.debug("byteTwo={}",byteTwo);
			}
			else{
				log.debug("查無此電文代號：{}",transactionCode);
			}
			//有值
			if(byteOne != null && byteTwo != null){
				//將兩個byte[]合併
				result = CodeUtil.combineByte(byteOne,byteTwo);
				log.debug("result={}",result);
			}
		}
		catch(Exception e){
			log.error(String.valueOf(e));
		}
		return result;
	}
	/**
	 * 組RQ的欄位並回傳組好的BYTE[]
	 */
	public static byte[] getRQData(Map<String,String> parameterMap){
		byte[] result = null;
		
		try{
			TitaTotaList titaTotaList = getTitaTotaList(parameterMap);
			
			if(titaTotaList == null){
				log.debug("getTitaTotaList出現問題");
				
				return result;
			}
			
			List<Tita> list = titaTotaList.getTITA();
			log.debug("list={}",list);
			
			String data = "";
			for(Tita tita : list){
				log.debug("tita={}",tita);
				
				String TAGNAME = tita.getTAGNAME();
				log.debug("TAGNAME={}",TAGNAME);
				
				String value = parameterMap.get(TAGNAME);
				log.debug("value={}",value);
				
				if(value == null){
					value = "";
				}
				
				int LENGTH = tita.getLENGTH();
				log.debug("LENGTH={}",LENGTH);
				
				String TYPE = tita.getTYPE();
				log.debug("TYPE={}",TYPE);
				
				//驗證長度及補位
				if(!"Z".equals(TYPE)){
					//長度不足或剛好
					if(value.getBytes("BIG5").length <= LENGTH){
						//補位
						//左補空白
						if("A".equals(TYPE)){
							while(value.getBytes("BIG5").length < LENGTH){
								value = " " + value;
							}
						}
						//右補空白
						else if("B".equals(TYPE)){
							while(value.getBytes("BIG5").length < LENGTH){
								value = value + " ";
							}
						}
						//左補0
						else if("C".equals(TYPE)){
							while(value.getBytes("BIG5").length < LENGTH){
								value = "0" + value;
							}
						}
						//右補0
						else if("D".equals(TYPE)){
							while(value.getBytes("BIG5").length < LENGTH){
								value = value + "0";
							}
						}
					}
					//長度超過
					else{
						log.debug("{}的值長度不正確",TAGNAME);
						
						return result;
					}
				}
				//驗證長度但不補位
				else{
					//長度不足或超過
					if(value.getBytes("BIG5").length != LENGTH){
						log.debug("{}的值長度不正確",TAGNAME);
						
						return result;
					}
				}
				data += value;
			}
			//編碼改成CP937
			result = data.getBytes("CP937");
		}
		catch(Exception e){
			log.error(String.valueOf(e));
		}
		return result;
	}
	/**
	 * 將下行電文依照定義的XML截取並放進LIST
	 */
	public static Map<String,String> getRSData(List<Map<String,String>> dataListMap,String hexData,Map<String,String> parameterMap){
		Map<String,String> returnMap = new HashMap<String,String>();
		returnMap.put("result","FALSE");
		
		try{
			TitaTotaList titaTotaList = getTitaTotaList(parameterMap);
			
			if(titaTotaList == null){
				log.debug("getTitaTotaList出現問題");
				
				return returnMap;
			}

			List<Tota> list = titaTotaList.getTOTA();
			log.debug("list={}",list);
			
			Map<String,String> totaMap;
			//跑FOR迴圈的次數
			int loopTime = 0;
			//LOOP BYTE的起始位置
			int loopPosition = 0;
			//截BYTE的起始位置
			int position = 0;
			//截取的HEX(用HEX判斷比用BYTE和STRING準確得多)
			String sliceHex;
			
			//先整個掃一次
			//將ABEND和USERDATA和SEQ和TXNCNT和MSGCOD和RSPCOD和ENDCOD放進returnMap，之後才能再向主機要資料
			//如果有REC_NO或COUNT或RECNUM或CNT欄位的話，表示有多筆資料，記錄截LOOP BYTE的起始位置
			for(Tota tota : list){
				if("ABEND".equals(tota.getTAGNAME())){
					sliceHex = hexData.substring(position * 2,position * 2 + tota.getLENGTH() * 2);
					log.debug("position * 2={}，tota.getLENGTH() * 2={}",position * 2,tota.getLENGTH() * 2);
					log.debug("ABEND=「{}」",new String(CodeUtil.hexStringToByteArray(sliceHex),"BIG5"));
					
					returnMap.put("ABEND",new String(CodeUtil.hexStringToByteArray(sliceHex),"BIG5").trim());
				}
				if("RSPCOD".equals(tota.getTAGNAME())){
					sliceHex = hexData.substring(position * 2,position * 2 + tota.getLENGTH() * 2);
					log.debug("position * 2={}，tota.getLENGTH() * 2={}",position * 2,tota.getLENGTH() * 2);
					log.debug("RSPCOD=「{}」",new String(CodeUtil.hexStringToByteArray(sliceHex),"BIG5"));
					
					returnMap.put("RSPCOD",new String(CodeUtil.hexStringToByteArray(sliceHex),"BIG5").trim());
				}
				if("TXNCNT".equals(tota.getTAGNAME())){
					sliceHex = hexData.substring(position * 2,position * 2 + tota.getLENGTH() * 2);
					log.debug("position * 2={}，tota.getLENGTH() * 2={}",position * 2,tota.getLENGTH() * 2);
					log.debug("TXNCNT=「{}」",new String(CodeUtil.hexStringToByteArray(sliceHex),"BIG5"));
					
					returnMap.put("TXNCNT",new String(CodeUtil.hexStringToByteArray(sliceHex),"BIG5"));
				}
				if("MSGCOD".equals(tota.getTAGNAME())){
					sliceHex = hexData.substring(position * 2,position * 2 + tota.getLENGTH() * 2);
					log.debug("position * 2={}，tota.getLENGTH() * 2={}",position * 2,tota.getLENGTH() * 2);
					log.debug("MSGCOD=「{}」",new String(CodeUtil.hexStringToByteArray(sliceHex),"BIG5"));
					
					returnMap.put("MSGCOD",new String(CodeUtil.hexStringToByteArray(sliceHex),"BIG5"));
				}
				if("USERDATA".equals(tota.getTAGNAME())){
					sliceHex = hexData.substring(position * 2,position * 2 + tota.getLENGTH() * 2);
					log.debug("position * 2={}，tota.getLENGTH() * 2={}",position * 2,tota.getLENGTH() * 2);
					log.debug("USERDATA=「{}」",new String(CodeUtil.hexStringToByteArray(sliceHex),"BIG5"));
					
					returnMap.put("USERDATA",new String(CodeUtil.hexStringToByteArray(sliceHex),"BIG5"));
				}
				if("SEQ".equals(tota.getTAGNAME())){
					sliceHex = hexData.substring(position * 2,position * 2 + tota.getLENGTH() * 2);
					log.debug("position * 2={}，tota.getLENGTH() * 2={}",position * 2,tota.getLENGTH() * 2);
					log.debug("SEQ=「{}」",new String(CodeUtil.hexStringToByteArray(sliceHex),"BIG5"));
					
					returnMap.put("SEQ",new String(CodeUtil.hexStringToByteArray(sliceHex),"BIG5"));
				}
				if("TXID".equals(tota.getTAGNAME())){
					sliceHex = hexData.substring(position * 2,position * 2 + tota.getLENGTH() * 2);
					log.debug("position * 2={}，tota.getLENGTH() * 2={}",position * 2,tota.getLENGTH() * 2);
					log.debug("TXID=「{}」",new String(CodeUtil.hexStringToByteArray(sliceHex),"BIG5"));
					
					returnMap.put("TXID",new String(CodeUtil.hexStringToByteArray(sliceHex),"BIG5").trim());
				}
				if("ENDCOD".equals(tota.getTAGNAME())){
					sliceHex = hexData.substring(position * 2,position * 2 + tota.getLENGTH() * 2);
					log.debug("position * 2={}，tota.getLENGTH() * 2={}",position * 2,tota.getLENGTH() * 2);
					log.debug("ENDCOD=「{}」",new String(CodeUtil.hexStringToByteArray(sliceHex),"BIG5"));
					
					returnMap.put("ENDCOD",new String(CodeUtil.hexStringToByteArray(sliceHex),"BIG5"));
				}
				if("REC_NO".equals(tota.getTAGNAME()) || "COUNT".equals(tota.getTAGNAME()) || "RECNUM".equals(tota.getTAGNAME()) || "CNT".equals(tota.getTAGNAME())){
					sliceHex = hexData.substring(position * 2,position * 2 + tota.getLENGTH() * 2);
					//轉成INT
					loopTime = Integer.valueOf(new String(CodeUtil.hexStringToByteArray(sliceHex),"BIG5").trim());
					log.debug("loopTime={}",loopTime);
					
					loopPosition = tota.getSTARTLOOPPOSITION();
					log.debug("loopPosition={}",loopPosition);
				}
				position += tota.getLENGTH();
			}
			position = 0;
			
			int tempLoopPosition = loopPosition;
			//先整個掃一次
			//將CHGCOD放進returnMap，之後才能再向主機要資料
			for(int x=0;x<loopTime;x++){
				for(Tota tota : list){
					//第一次
					if(x == 0){
						//塞資料
						if(tota.isDOFIRST()){
							tempLoopPosition += tota.getLENGTH();
							log.debug("tempLoopPosition={}",tempLoopPosition);
						}
						//是第一次也是最後一次
						if(x == loopTime - 1){
							//DOLAST為TRUE才做
							if(tota.isDOLAST()){
								if("CHGCOD".equals(tota.getTAGNAME())){
									sliceHex = hexData.substring(tempLoopPosition * 2,tempLoopPosition * 2 + tota.getLENGTH() * 2);
									log.debug("tempLoopPosition * 2={}，tota.getLENGTH() * 2={}",tempLoopPosition * 2,tota.getLENGTH() * 2);
									log.debug("{}=「{}」",tota.getTAGNAME(),new String(CodeUtil.hexStringToByteArray(sliceHex),"BIG5"));
									
									returnMap.put(tota.getTAGNAME(),new String(CodeUtil.hexStringToByteArray(sliceHex),"BIG5").trim());
								}
								tempLoopPosition += tota.getLENGTH();
								log.debug("tempLoopPosition={}",tempLoopPosition);
							}
						}
					}
					//第二次以後
					else{
						//LOOP
						if(tota.isLOOP()){
							tempLoopPosition += tota.getLENGTH();
							log.debug("tempLoopPosition={}",tempLoopPosition);
						}
						//DOLAST
						else if(tota.isDOLAST()){
							//最後一次
							if(x == loopTime - 1){
								if("CHGCOD".equals(tota.getTAGNAME())){
									sliceHex = hexData.substring(tempLoopPosition * 2,tempLoopPosition * 2 + tota.getLENGTH() * 2);
									log.debug("tempLoopPosition * 2={}，tota.getLENGTH() * 2={}",tempLoopPosition * 2,tota.getLENGTH() * 2);
									log.debug("{}=「{}」",tota.getTAGNAME(),new String(CodeUtil.hexStringToByteArray(sliceHex),"BIG5"));
									
									returnMap.put(tota.getTAGNAME(),new String(CodeUtil.hexStringToByteArray(sliceHex),"BIG5").trim());
								}
								tempLoopPosition += tota.getLENGTH();
								log.debug("tempLoopPosition={}",tempLoopPosition);
							}
						}
					}
				}
			}
			
			boolean doContinueBoolean = doContinue(parameterMap,returnMap);
			log.debug("doContinueBoolean={}",doContinueBoolean);
			
			//沒有資料可以裝
			if(doContinueBoolean == false){
				returnMap.put("result","TRUE");
				
				return returnMap;
			}
			
			//開始裝資料
			//有多筆資料，只裝多筆資料
			if(loopTime > 0){
				for(int x=0;x<loopTime;x++){
					totaMap = new HashMap<String,String>();
					
					for(Tota tota : list){
						//第一次
						if(x == 0){
							//塞資料
							if(tota.isDOFIRST()){
								sliceHex = hexData.substring(loopPosition * 2,loopPosition * 2 + tota.getLENGTH() * 2);
								log.debug("loopPosition * 2={}，tota.getLENGTH() * 2={}",loopPosition * 2,tota.getLENGTH() * 2);
								log.debug("{}=「{}」",tota.getTAGNAME(),new String(CodeUtil.hexStringToByteArray(sliceHex),"BIG5"));
								
								//有些值要塞
								if(tota.isNOPUT() == false){
									totaMap.put(tota.getTAGNAME(),new String(CodeUtil.hexStringToByteArray(sliceHex),"BIG5").trim());
								}
								
								loopPosition += tota.getLENGTH();
								log.debug("loopPosition={}",loopPosition);
							}
							//是第一次也是最後一次
							else if(x == loopTime - 1){
								//DOLAST為TRUE才做
								if(tota.isDOLAST()){
									sliceHex = hexData.substring(loopPosition * 2,loopPosition * 2 + tota.getLENGTH() * 2);
									log.debug("loopPosition * 2={}，tota.getLENGTH() * 2={}",loopPosition * 2,tota.getLENGTH() * 2);
									log.debug("{}=「{}」",tota.getTAGNAME(),new String(CodeUtil.hexStringToByteArray(sliceHex),"BIG5"));
									
									//有些值要塞
									if(tota.isNOPUT() == false){
										totaMap.put(tota.getTAGNAME(),new String(CodeUtil.hexStringToByteArray(sliceHex),"BIG5").trim());
									}
									
									loopPosition += tota.getLENGTH();
									log.debug("loopPosition={}",loopPosition);
								}
							}
						}
						//第二次以後
						else{
							//最後一次
							if(x == loopTime - 1){
								//DOLAST為TRUE才做
								if(tota.isDOLAST()){
									sliceHex = hexData.substring(loopPosition * 2,loopPosition * 2 + tota.getLENGTH() * 2);
									log.debug("loopPosition * 2={}，tota.getLENGTH() * 2={}",loopPosition * 2,tota.getLENGTH() * 2);
									log.debug("{}=「{}」",tota.getTAGNAME(),new String(CodeUtil.hexStringToByteArray(sliceHex),"BIG5"));
									
									//有些值要塞
									if(tota.isNOPUT() == false){
										totaMap.put(tota.getTAGNAME(),new String(CodeUtil.hexStringToByteArray(sliceHex),"BIG5").trim());
									}
									
									loopPosition += tota.getLENGTH();
									log.debug("loopPosition={}",loopPosition);
								}
							}
							//不是最後一次
							else{
								//塞資料
								if(tota.isLOOP()){
									sliceHex = hexData.substring(loopPosition * 2,loopPosition * 2 + tota.getLENGTH() * 2);
									log.debug("loopPosition * 2={}，tota.getLENGTH() * 2={}",loopPosition * 2,tota.getLENGTH() * 2);
									log.debug("{}=「{}」",tota.getTAGNAME(),new String(CodeUtil.hexStringToByteArray(sliceHex),"BIG5"));
									
									totaMap.put(tota.getTAGNAME(),new String(CodeUtil.hexStringToByteArray(sliceHex),"BIG5").trim());
									
									loopPosition += tota.getLENGTH();
									log.debug("loopPosition={}",loopPosition);
								}
							}
						}
					}
					//TOTAMAP有資料才放
					if(!totaMap.isEmpty()){
						dataListMap.add(totaMap);
					}
				}
				log.debug("dataListMap={}",dataListMap);
			}
			//每個都裝
			else{
				totaMap = new HashMap<String,String>();
				
				for(Tota tota : list){
					//塞資料
					sliceHex = hexData.substring(position * 2,position * 2 + tota.getLENGTH() * 2);
					log.debug("position * 2={}，tota.getLENGTH() * 2={}",position * 2,tota.getLENGTH() * 2);
					log.debug("{}=「{}」",tota.getTAGNAME(),new String(CodeUtil.hexStringToByteArray(sliceHex),"BIG5"));
					
					totaMap.put(tota.getTAGNAME(),new String(CodeUtil.hexStringToByteArray(sliceHex),"BIG5").trim());
					
					position += tota.getLENGTH();
					log.debug("position={}",position);
				}
				dataListMap.add(totaMap);
				log.debug("dataListMap={}",dataListMap);
			}
			
			returnMap.put("result","TRUE");
		}
		catch(Exception e){
			log.error(String.valueOf(e));
		}
		return returnMap;
	}
	/**
	 * 判斷是否要再往下做
	 */
	public static boolean doContinue(Map<String,String> parameterMap,Map<String,String> returnMap){
		boolean doContinueBoolean = false;
		
		String transactionCode = parameterMap.get("transactionCode");
		log.debug("transactionCode={}",transactionCode);
		
		String ABEND = returnMap.get("ABEND");
		log.debug("ABEND={}",ABEND);
		
		//判斷電文代號
		if("N014".equals(transactionCode)){
			if("OKOK".equals(ABEND) || "OKLR".equals(ABEND) || "OKOV".equals(ABEND)){
				doContinueBoolean = true;
			}
		}
		else if("N015".equals(transactionCode)){
			doContinueBoolean = true;
		}
		else if("N016".equals(transactionCode)){
			if("OKOK".equals(ABEND) || "OKLR".equals(ABEND) || "OKOV".equals(ABEND)){
				doContinueBoolean = true;
			}
		}
		else if("N110".equals(transactionCode)){
			if("OKOK".equals(ABEND) || "OKLR".equals(ABEND) || "OKOV".equals(ABEND)){
				doContinueBoolean = true;
			}
		}
		else if("N130".equals(transactionCode)){
			if("OKOK".equals(ABEND) || "OKLR".equals(ABEND) || "OKOV".equals(ABEND)){
				doContinueBoolean = true;
			}
		}
		else if("N133".equals(transactionCode)){
			if("OKOK".equals(ABEND) || "OKLR".equals(ABEND) || "OKOV".equals(ABEND)){
				doContinueBoolean = true;
			}
		}
		else if("N320".equals(transactionCode)){
			if("OKOK".equals(ABEND) || "OKLR".equals(ABEND) || "OKOV".equals(ABEND)){
				doContinueBoolean = true;
			}
		}
		else if("N420".equals(transactionCode)){
			if("OKOK".equals(ABEND) || "OKLR".equals(ABEND) || "OKOV".equals(ABEND)){
				doContinueBoolean = true;
			}
		}
		else if("N510".equals(transactionCode)){
			if("OKOK".equals(ABEND) || "OKLR".equals(ABEND) || "OKOV".equals(ABEND)){
				doContinueBoolean = true;
			}
		}
		else if("N523".equals(transactionCode)){
			if("OKOK".equals(ABEND) || "OKLR".equals(ABEND) || "OKOV".equals(ABEND)){
				doContinueBoolean = true;
			}
		}
		else if("N524".equals(transactionCode)){
			if("OKOK".equals(ABEND) || "OKLR".equals(ABEND) || "OKOV".equals(ABEND)){
				doContinueBoolean = true;
			}
		}
		else if("N530".equals(transactionCode)){
			if("OKOK".equals(ABEND) || "OKLR".equals(ABEND) || "OKOV".equals(ABEND)){
				doContinueBoolean = true;
			}
		}
		else if("N550".equals(transactionCode)){
			if("OKOK".equals(ABEND) || "OKLR".equals(ABEND) || "OKOV".equals(ABEND)){
				doContinueBoolean = true;
			}
		}
		else if("N552".equals(transactionCode)){
			if("OKOK".equals(ABEND) || "OKLR".equals(ABEND) || "OKOV".equals(ABEND)){
				doContinueBoolean = true;
			}
		}
		else if("N554".equals(transactionCode)){
			if("OKOK".equals(ABEND) || "OKLR".equals(ABEND) || "OKOV".equals(ABEND)){
				doContinueBoolean = true;
			}
		}
		else if("N625".equals(transactionCode)){
			if("OKOK".equals(ABEND) || "OKLR".equals(ABEND) || "OKOV".equals(ABEND)){
				doContinueBoolean = true;
			}
		}
		else if("N810".equals(transactionCode)){
			doContinueBoolean = true;
		}
		else if("N813".equals(transactionCode)){
			doContinueBoolean = true;
		}
		else if("N920".equals(transactionCode)){
			doContinueBoolean = true;
		}
		else if("N924".equals(transactionCode)){
			doContinueBoolean = true;
		}
		else if("TD01".equals(transactionCode)){
			doContinueBoolean = true;
		}
		
		return doContinueBoolean;
	}
	/**
	 * 判斷是否要再打一次電文
	 */
	public static boolean doTelcomAgain(Map<String,String> parameterMap,Map<String,String> returnMap){
		boolean doAgain = false;
		
		String transactionCode = parameterMap.get("transactionCode");
		log.debug("transactionCode={}",transactionCode);
		
		String ABEND = returnMap.get("ABEND");
		log.debug("ABEND={}",ABEND);
		
		String MSGCOD = returnMap.get("MSGCOD");
		log.debug("MSGCOD={}",MSGCOD);
		
		String SEQ = returnMap.get("SEQ");
		log.debug("SEQ={}",SEQ);
		
		String CHGCOD = returnMap.get("CHGCOD");
		log.debug("CHGCOD={}",CHGCOD);
		
		String TXNCNT = returnMap.get("TXNCNT");
		log.debug("TXNCNT={}",TXNCNT);
		
		String RSPCOD = returnMap.get("RSPCOD");
		log.debug("RSPCOD={}",RSPCOD);
		
		String ENDCOD = returnMap.get("ENDCOD");
		log.debug("ENDCOD={}",ENDCOD);
		
		//判斷電文代號
		if("N014".equals(transactionCode)){
			if("OKOK".equals(ABEND)){
				doAgain = true;
			}
		}
		else if("N015".equals(transactionCode)){
			if("    ".equals(MSGCOD) || "0000".equals(MSGCOD)){
				if(!"999".equals(SEQ)){
					doAgain = true;
				}
			}
		}
		else if("N016".equals(transactionCode)){
			if("OKOK".equals(ABEND)){
				doAgain = true;
			}
		}
		else if("N110".equals(transactionCode)){
			if("OKOK".equals(ABEND)){
				doAgain = true;
			}
		}
		else if("N130".equals(transactionCode)){
			if("OKOK".equals(ABEND)){
				doAgain = true;
			}
		}
		else if("N133".equals(transactionCode)){
			if("OKOK".equals(ABEND)){
				doAgain = true;
			}
		}
		else if("N320".equals(transactionCode)){
			if("OKOK".equals(ABEND)){
				doAgain = true;
			}
		}
		else if("N420".equals(transactionCode)){
			if("OKOK".equals(ABEND)){
				doAgain = true;
			}
		}
		else if("N510".equals(transactionCode)){
			if("OKOK".equals(ABEND)){
				if("N".equals(CHGCOD)){
					doAgain = true;
				}
			}
		}
		else if("N523".equals(transactionCode)){
			if("OKOK".equals(ABEND)){
				if("N".equals(CHGCOD)){
					doAgain = true;
				}
			}
		}
		else if("N524".equals(transactionCode)){
			if("OKOK".equals(ABEND)){
				if("N".equals(CHGCOD)){
					doAgain = true;
				}
			}
		}
		else if("N530".equals(transactionCode)){
			if("OKOK".equals(ABEND)){
				doAgain = true;
			}
		}
		else if("N550".equals(transactionCode)){
			if("OKOK".equals(ABEND)){
				doAgain = true;
			}
		}
		else if("N552".equals(transactionCode)){
			if("OKOK".equals(ABEND)){
				doAgain = true;
			}
		}
		else if("N554".equals(transactionCode)){
			if("OKOK".equals(ABEND)){
				doAgain = true;
			}
		}
		else if("N625".equals(transactionCode)){
			if("OKOK".equals(ABEND)){
				doAgain = true;
			}
		}
		else if("N810".equals(transactionCode)){
			if("    ".equals(RSPCOD) || "0000".equals(RSPCOD)){
				if(!"99".equals(SEQ)){
					doAgain = true;
				}
			}
		}
		else if("N920".equals(transactionCode)){
			if("    ".equals(MSGCOD) || "0000".equals(MSGCOD)){
				if(!"99".equals(SEQ)){
					doAgain = true;
				}
			}
		}
		else if("TD01".equals(transactionCode)){
			if("    ".equals(RSPCOD) || "0000".equals(RSPCOD)){
				if(!"9999".equals(TXNCNT)){
					doAgain = true;
				}
			}
		}
		log.debug("doAgain={}",doAgain);
		
		return doAgain;
	}
	/**
	 * 再打一次電文，先取得識別資料
	 */
	public static void getIdentifyData(Map<String,String> parameterMap,Map<String,String> returnMap){
		String transactionCode = parameterMap.get("transactionCode");
		log.debug("transactionCode={}",transactionCode);
		
		String USERDATA;
		String SEQ;
		String CHGCOD;
		String TXNCNT;
		String ENDCOD;
		
		//判斷電文代號
		if("N014".equals(transactionCode)){
			USERDATA = returnMap.get("USERDATA");
			log.debug("returnMap的USERDATA={}",USERDATA);
			
			parameterMap.put("USERDATA",USERDATA);
			
			ENDCOD = returnMap.get("ENDCOD");
			log.debug("returnMap的ENDCOD={}",ENDCOD);
			
			parameterMap.put("ENDCOD",ENDCOD);
		}
		else if("N015".equals(transactionCode)){
			USERDATA = returnMap.get("USERDATA");
			log.debug("returnMap的USERDATA={}",USERDATA);
			
			parameterMap.put("USERDATA",USERDATA);
			
			SEQ = parameterMap.get("SEQ");
			log.debug("parameterMap的SEQ={}",SEQ);
			
			int newSEQInt = Integer.valueOf(SEQ) + 1;
			log.debug("newSEQInt={}",newSEQInt);
			
			String newSEQ = String.valueOf(newSEQInt);
			
			while(newSEQ.length() < 3){
				newSEQ = "0" + newSEQ;
			}
			log.debug("newSEQ={}",newSEQ);
			
			parameterMap.put("SEQ",newSEQ);
		}
		else if("N016".equals(transactionCode)){
			USERDATA = returnMap.get("USERDATA");
			log.debug("returnMap的USERDATA={}",USERDATA);
			
			parameterMap.put("USERDATA",USERDATA);
		}
		else if("N110".equals(transactionCode)){
			USERDATA = returnMap.get("USERDATA");
			log.debug("returnMap的USERDATA={}",USERDATA);
			
			parameterMap.put("USERDATA",USERDATA);
		}
		else if("N130".equals(transactionCode)){
			USERDATA = returnMap.get("USERDATA");
			log.debug("returnMap的USERDATA={}",USERDATA);
			
			parameterMap.put("USERDATA",USERDATA);
		}
		else if("N133".equals(transactionCode)){
			USERDATA = returnMap.get("USERDATA");
			log.debug("returnMap的USERDATA={}",USERDATA);
			
			parameterMap.put("USERDATA",USERDATA);
		}
		else if("N320".equals(transactionCode)){
			USERDATA = returnMap.get("USERDATA");
			log.debug("returnMap的USERDATA={}",USERDATA);
			
			parameterMap.put("USERDATA",USERDATA);
		}
		else if("N420".equals(transactionCode)){
			USERDATA = returnMap.get("USERDATA");
			log.debug("returnMap的USERDATA={}",USERDATA);
			
			parameterMap.put("USERDATA",USERDATA);
		}
		else if("N510".equals(transactionCode)){
			USERDATA = returnMap.get("USERDATA");
			log.debug("returnMap的USERDATA={}",USERDATA);
			
			parameterMap.put("USERDATA",USERDATA);
			
			CHGCOD = returnMap.get("CHGCOD");
			log.debug("returnMap的CHGCOD={}",CHGCOD);
			
			parameterMap.put("CHGCOD",CHGCOD);
		}
		else if("N523".equals(transactionCode)){
			USERDATA = returnMap.get("USERDATA");
			log.debug("returnMap的USERDATA={}",USERDATA);
			
			parameterMap.put("USERDATA",USERDATA);
			
			CHGCOD = returnMap.get("CHGCOD");
			log.debug("returnMap的CHGCOD={}",CHGCOD);
			
			parameterMap.put("CHGCOD",CHGCOD);
		}
		else if("N524".equals(transactionCode)){
			USERDATA = returnMap.get("USERDATA");
			log.debug("returnMap的USERDATA={}",USERDATA);
			
			parameterMap.put("USERDATA",USERDATA);
			
			CHGCOD = returnMap.get("CHGCOD");
			log.debug("returnMap的CHGCOD={}",CHGCOD);
			
			parameterMap.put("CHGCOD",CHGCOD);
		}
		else if("N530".equals(transactionCode)){
			USERDATA = returnMap.get("USERDATA");
			log.debug("returnMap的USERDATA={}",USERDATA);
			
			parameterMap.put("USERDATA",USERDATA);
		}
		else if("N550".equals(transactionCode)){
			USERDATA = returnMap.get("USERDATA");
			log.debug("returnMap的USERDATA={}",USERDATA);
			
			parameterMap.put("USERDATA",USERDATA);
		}
		else if("N552".equals(transactionCode)){
			USERDATA = returnMap.get("USERDATA");
			log.debug("returnMap的USERDATA={}",USERDATA);
			
			parameterMap.put("USERDATA",USERDATA);
		}
		else if("N554".equals(transactionCode)){
			USERDATA = returnMap.get("USERDATA");
			log.debug("returnMap的USERDATA={}",USERDATA);
			
			parameterMap.put("USERDATA",USERDATA);
		}
		else if("N625".equals(transactionCode)){
			USERDATA = returnMap.get("USERDATA");
			log.debug("returnMap的USERDATA={}",USERDATA);
			
			parameterMap.put("USERDATA",USERDATA);
		}
		else if("N810".equals(transactionCode)){
			SEQ = parameterMap.get("SEQ");
			log.debug("parameterMap的SEQ={}",SEQ);
			
			int newSEQInt = Integer.valueOf(SEQ) + 1;
			log.debug("newSEQInt={}",newSEQInt);
			
			String newSEQ;
			
			if(newSEQInt < 10){
				newSEQ = "0" + newSEQInt;
			}
			else{
				newSEQ = String.valueOf(newSEQInt);
			}
			log.debug("newSEQ={}",newSEQ);
			
			parameterMap.put("SEQ",newSEQ);
		}
		else if("N920".equals(transactionCode)){
			SEQ = parameterMap.get("SEQ");
			log.debug("parameterMap的SEQ={}",SEQ);
			
			int newSEQInt = Integer.valueOf(SEQ) + 1;
			log.debug("newSEQInt={}",newSEQInt);
			
			String newSEQ;
			
			if(newSEQInt < 10){
				newSEQ = "0" + newSEQInt;
			}
			else{
				newSEQ = String.valueOf(newSEQInt);
			}
			log.debug("newSEQ={}",newSEQ);
			
			parameterMap.put("SEQ",newSEQ);
		}
		else if("TD01".equals(transactionCode)){
			TXNCNT = parameterMap.get("TXNCNT");
			log.debug("parameterMap的TXNCNT={}",TXNCNT);
			
			int newTXNCNTInt = Integer.valueOf(TXNCNT) + 1;
			log.debug("newTXNCNTInt={}",newTXNCNTInt);
			
			String newTXNCNT;
			
			if(newTXNCNTInt < 10){
				newTXNCNT = "0" + newTXNCNTInt;
			}
			else{
				newTXNCNT = String.valueOf(newTXNCNTInt);
			}
			log.debug("newTXNCNT={}",newTXNCNT);
			
			parameterMap.put("TXNCNT",newTXNCNT);
		}
	}
	/**
	 * 取得電文定義的LIST
	 */
	public static TitaTotaList getTitaTotaList(Map<String,String> parameterMap){
		try{
			String transactionCode = parameterMap.get("transactionCode");
			log.debug("transactionCode={}",transactionCode);
			
			ClassLoader classLoader = TelcomUtil.class.getClassLoader();
			log.debug("classLoader={}",classLoader);
			
			InputStream inputStream = classLoader.getResourceAsStream("com/netbank/telcomObject/" + transactionCode + "Rule.xml");
			log.debug("inputStream={}",inputStream);
			
			JAXBContext jaxbContext = JAXBContext.newInstance(TitaTotaList.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			
			TitaTotaList titaTotaList = (TitaTotaList)unmarshaller.unmarshal(inputStream);
			log.debug("titaTotaList={}",titaTotaList);
			
			return titaTotaList;
		}
		catch(Exception e){
			log.error(String.valueOf(e));
			
			return null;
		}
	}
	/**
	 * 處理中心回傳EBCDIC下行電文中文資料
	 */
	public static byte[] processBytes(byte[] inBytes){
		byte[] outBytes = null;
		
		try{
			outBytes = CP937ToBIG5(inBytes);
			
			List<ChineseObject> chineseObjectList = cutChineseCode(inBytes);
			log.debug("chineseObjectList={}",chineseObjectList);
			
			//難字計數器
			int difficultCount = 0;
			String chineseString = "";
			
			for(int i=0;i<chineseObjectList.size();i++){
				ChineseObject chineseObject = chineseObjectList.get(i);
				log.debug("chineseObject={}",chineseObject);
				
				chineseString += ebcdicToBIG5Chinese(chineseObject);
				log.debug("chineseString={}",chineseString);
				
				if(chineseObject.isDifficultWord()){
					difficultCount++;
				}
			}
			log.debug("difficultCount={}",difficultCount);
			
			if(difficultCount > 0){
				chineseString += "__UC";
				
				outBytes = chineseString.getBytes("UnicodeBig");
			}
		}
		catch(Exception e){
			log.error(String.valueOf(e));
		}
		return outBytes;
	}
	/**
	 * CP937轉BIG5
	 */
	public static byte[] CP937ToBIG5(byte[] CP937){
		byte[] BIG5 = null;
		
		try{
			String CP937String = new String(CP937,"CP937");
			log.debug("CP937String={}",CP937String);
			
			BIG5 = CP937String.getBytes("BIG5");
		}
		catch(Exception e){
			log.error(String.valueOf(e));
		}
        return BIG5;
	}
	/**
	 * 判斷0E及0F的位置，如果有成對，代表資料內容有中文，中文要從0E的位置+1開始擷取到0F位置之前結束，並在右邊補兩個空白
	 */
	public static byte[] padChineseCode(byte[] inBytes){
		byte[] resultBytes = null;
		
		int index = 0;
		int indexS = -1;
		int indexE = -1;
		byte[] temp = null;
		
		while(index < inBytes.length){
			indexS = find0E(indexS + 1,inBytes);
			log.debug("indexS={}",indexS);
			
			indexE = find0F(indexE + 1,inBytes);
			log.debug("indexE={}",indexE);
			
			//這一段有中文
			if(indexS > -1 && indexE > indexS){
				//這一段開頭不是中文
				if(indexS > index){
					//第一段沒有中文
					if(resultBytes == null){
						resultBytes = new byte[indexS];
						System.arraycopy(inBytes,0,resultBytes,0,indexS);
					}
					//前面有中文
					else{
						temp = new byte[indexS - index - 1];
						System.arraycopy(inBytes,index + 1,temp,0,indexS - index - 1);
						
						resultBytes = CodeUtil.combineByte(resultBytes,temp);
					}
					temp = new byte[indexE + 1 - indexS + 2];
					System.arraycopy(inBytes,indexS,temp,0,indexE + 1 - indexS);
					temp[indexE + 1 - indexS] = 0x40;
					temp[indexE + 1 - indexS + 1] = 0x40;
					resultBytes = CodeUtil.combineByte(resultBytes,temp);
				}
				//這一段開頭是中文
				else{
					if(resultBytes == null){
						resultBytes = new byte[indexS];
						System.arraycopy(inBytes,0,resultBytes,0,indexS);
					}
					temp = new byte[indexE + 1 - indexS + 2];
					System.arraycopy(inBytes,indexS,temp,0,indexE + 1 - indexS);
					temp[indexE + 1 - indexS] = 0x40;
					temp[indexE + 1 - indexS + 1] = 0x40;
					resultBytes = CodeUtil.combineByte(resultBytes,temp);
				}
				index = indexE;
			}
			//這一段沒有中文
			else{
				//整串沒有中文
				if(resultBytes == null){
					resultBytes = inBytes;
					
					break;
				}
				//前面有中文，這是最後一段
				else{
					temp = new byte[inBytes.length - index - 1];
					System.arraycopy(inBytes,index + 1,temp,0,inBytes.length - index - 1);
					resultBytes = CodeUtil.combineByte(resultBytes,temp);
					
					break;
				}
			}
			
			log.debug("index={}",index);
		}
		return resultBytes;
	}
	/**
	 * 判斷0E及0F的位置，如果有成對，代表資料內容有中文，中文要從0E的位置+1開始擷取到0F位置之前結束，中文特別擷取出來並在塞回去FORMAT成原本電文欄位長度，左靠右補空白補滿長度
	 */
	public static List<ChineseObject> cutChineseCode(byte[] inBytes){
		List<ChineseObject> chineseObjectList = new ArrayList<ChineseObject>();
		
		int index = 0;
		int indexS = -1;
		int indexE = -1;
		byte[] temp = null;
		//-1表示init，0表示英文，1表示中文
		int ischinese=-1;
		
		while(index < inBytes.length){
			indexS = find0E(indexS + 1,inBytes);
			log.debug("indexS={}",indexS);
			
			indexE = find0F(indexE + 1,inBytes);
			log.debug("indexE={}",indexE);
			
			if(indexS > 0 && indexE == -1){
				ChineseObject chineseObject = new ChineseObject();
				temp = new byte[indexS];
				System.arraycopy(inBytes,index,temp,0,indexS);
				temp[indexS] = (byte)0x40;
				chineseObject.setEbcdicCode(temp);
				chineseObjectList.add(chineseObject);
				index += temp.length;
			}
			else if(indexS == -1 && indexE > 0){
				ChineseObject chineseObject = new ChineseObject();
				temp = new byte[indexS];
				System.arraycopy(inBytes,index,temp,0,indexE);
				temp[indexE] = (byte)0x40;
				chineseObject.setEbcdicCode(temp);
				chineseObjectList.add(chineseObject);
				index += temp.length;
			}
			else if(((index != indexS && ischinese == -1) || ischinese == 1) && indexS > 0 && indexE > 0){
				ChineseObject chineseObject = new ChineseObject();
				temp = new byte[indexS - index];
				System.arraycopy(inBytes,index,temp,0,temp.length);
				chineseObject.setEbcdicCode(temp);
				chineseObjectList.add(chineseObject);
				index += temp.length;

				chineseObject = new ChineseObject();
				temp = new byte[indexE - indexS + 1];
				System.arraycopy(inBytes,indexS,temp,0,temp.length);
				chineseObject.setEbcdicCode(temp);
				chineseObject.setDifficultWord(findDifficultWord(temp));
				chineseObject.setDifficultIndex(finddiffwordIndex(temp));
				chineseObject.setChinese(true);
				chineseObjectList.add(chineseObject);
				index += temp.length;
				ischinese = 1;
			}
			else if(indexS == -1 && indexE == -1){
				ChineseObject chineseObject = new ChineseObject();
				temp = new byte[inBytes.length - index];
				System.arraycopy(inBytes,index,temp,0,temp.length);
				chineseObject.setEbcdicCode(temp);
				chineseObjectList.add(chineseObject);
				index += temp.length;
				ischinese=0;
			}
			else{
				ChineseObject chineseObject = new ChineseObject();
				temp = new byte[indexE - indexS + 1];
				System.arraycopy(inBytes,indexS,temp,0,temp.length);
				chineseObject.setEbcdicCode(temp);
				chineseObject.setDifficultWord(findDifficultWord(temp));
				chineseObject.setDifficultIndex(finddiffwordIndex(temp));
				chineseObject.setChinese(true);
				chineseObjectList.add(chineseObject);
				index += temp.length;
			}
			log.debug("index={}",index);
		}
		return chineseObjectList;
	}
	/**
	 * 判斷0E位置
	 */
	public static int find0E(int chineseIndex,byte[] chineseCode){
		for(int i=chineseIndex;i<chineseCode.length;i++){
			if(chineseCode[i] == 0x0E){
				return i;
			}
		}
		return -1;
	}
	/**
	 * 判斷0F位置
	 */
	public static int find0F(int chineseIndex,byte[] chineseCode){
		for(int i=chineseIndex;i<chineseCode.length;i++){
			if(chineseCode[i]==0x0F){
				return i;
			}
		}
		return -1;
	}
	/**
	 * 尋找有無難字
	 */ 
	public static boolean findDifficultWord(byte[] inBytes){
		boolean result = false;
		
		try{
			byte[] BIG5 = CP937ToBIG5(inBytes);
			
			if(BIG5 != null){
				String BIG5String = new String(BIG5,"BIG5");
				log.debug("BIG5String={}",BIG5String);
				
				int BIG5Index = BIG5String.lastIndexOf("?");
				log.debug("BIG5Index={}",BIG5Index);
				
				if(BIG5Index != -1){
					result = true;
				}
			}
		}
		catch(Exception e){
			log.error(String.valueOf(e));
		}
		return result;
	}
	/**
	 * 尋找有無難字，回傳BIG5難字INDEX
	 */
	public static int[] finddiffwordIndex(byte[] inBytes){
		int[] index = null;
		int tempIndex = 0;

		try{
			byte[] BIG5 = CP937ToBIG5(inBytes);
			
			if(BIG5 != null){
				String BIG5String = new String(BIG5,"BIG5");
				log.debug("BIG5String={}",BIG5String);
				
				index = new int[BIG5String.length()];
				
				for(int i=0;i<index.length;i++){
					index[i] = -1;
				}
				
				while(tempIndex < BIG5String.length()){
					tempIndex = BIG5String.indexOf("?",tempIndex);
					if(tempIndex == -1){
						break;
					}
					index[tempIndex] = 1;
					tempIndex++;
				}
			}
		}
		catch(Exception e){
			log.error(String.valueOf(e));
		}
		log.debug("index={}",index);
		
		return index;
	}
	/**
	 * 從EBCDIC碼轉成中文碼
	 */
	public static String ebcdicToBIG5Chinese(ChineseObject chineseObject){
		Map<String,String> map = new HashMap<String,String>();
		map.put("E2D9","6052");
		map.put("E2D5","7881");
		map.put("D2F4","706F");
		map.put("D2DB","5EFC");
		map.put("D687","0020");
		
		String result = "";
		
		try{
			byte[] ebcdicCode = chineseObject.getEbcdicCode();
			int[] difficultIndex = chineseObject.getDifficultIndex();
			
			String tempWord = null;
			byte[] BIG5 = CP937ToBIG5(ebcdicCode);
			
			if(BIG5 != null){
				if(chineseObject.isDifficultWord() == false && chineseObject.isChinese() == false){
					tempWord = new String(BIG5);
				}
				else{
					tempWord = new String(BIG5,"BIG5");
				}
				log.debug("tempWord={}",tempWord);
				
				int i = 0;
				
				while(i < tempWord.length()){
					if(difficultIndex == null || difficultIndex[i] != 1){
						result += tempWord.substring(i,i+1);
					}
					else if(difficultIndex[i] == 1){
						byte[] h = new byte[2]; 
						String hexString = CodeUtil.ByteArrayToHexString(ebcdicCode);
						
						String hs = hexString.substring((i * 4) + 0 + 2,(i * 4) + 4 + 2);
						
						if(map.get(hs) != null){
							String unicodeBig = (String)map.get(hs);
							log.debug("unicodeBig={}",unicodeBig);
							
							h[0] = (byte)Integer.parseInt(unicodeBig.substring(0,2),16);
							h[1] = (byte)Integer.parseInt(unicodeBig.substring(2,4),16);
							result += new String(h,"UnicodeBig");
						}
						chineseObject.setDifficultWord(true);
					}
					log.debug("result={}",result);
					
					i++;
				}
			}
		}
		catch(Exception e){
			log.error(String.valueOf(e));
		}
		return result;
	}
}