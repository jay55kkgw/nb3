package com.netbank.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class StrUtils {
	public static boolean isEmpty(String s) {
		return s == null || s.length() == 0;
	}
	
	public static boolean isNotEmpty(String s) {
		return !(s == null || s.length() == 0);
	}
	
	public static String trim(String s) {
		if(isEmpty(s))
			return "";
		return s.trim();
	}
	
	public static String right(String str, int len) {
		if(str.length() <= len)
			return str;
		return str.substring(str.length() - len);
	}
	
	public static String left(String str, int len) {
		if(str.length() <= len)
			return str;
		return str.substring(0, len);
	}
	
	public static String repeat(String str, int len) {
		StringBuffer sb = new StringBuffer();
		for(int i =0; i < len ; i++) {
			sb.append(str);
		}
		return sb.toString();
	}
	
	/**
	 * 若傳入的 v 為空字串或者為null, 則回傳 default value
	 * @param v
	 * @param defaultValue
	 * @return
	 */
	public static String nvl(String v, String defaultValue) {
		return (isEmpty(v) ? defaultValue : v);
	}

	
	public static Set<String> splitToSet(String delim, String value) {
		return new HashSet(Arrays.asList(value.split(delim)));
	}
	
	/**
	 * 將 arys 加上 delim 變成 String
	 * @param delim
	 * @param arys
	 * @return
	 */
	public static String implode(String delim, String[] arys) {
		if(arys == null || arys.length == 0)
			return "";
		
		StringBuffer result = new StringBuffer();
		
		for(String x : arys) {
			if(result.length() != 0)
				result.append(delim);
			result.append(x);
		}
		
		return result.toString();
	}
	
	public static String implode(String delim, Set<String> arys) {
		if(arys == null || arys.size() == 0)
			return "";
		
		StringBuffer result = new StringBuffer();
		
		for(String x : arys) {
			if(result.length() != 0)
				result.append(delim);
			result.append(x);
		}
		
		return result.toString();
	}
	
	public static String implode(String delim, List<String> arys) {
		if(arys == null || arys.size() == 0)
			return "";
		
		StringBuffer result = new StringBuffer();
		
		for(String x : arys) {
			if(result.length() != 0)
				result.append(delim);
			result.append(x);
		}
		
		return result.toString();
	}
	
	
//	
//    /**
//     * 字串複製, 最長 4096 bytes
//     */
//    public static void copyStream( InputStream in, OutputStream out ) throws IOException {
//        byte[] buf = new byte[4096];
//        int len;
//        while ( ( len = in.read( buf ) ) != -1 )
//            out.write( buf, 0, len );
//    }
//
//    /**
//     * 字串複製, 最長 4096 bytes
//     */
//    public static void copyStream( Reader in, Writer out ) throws IOException {
//        char[] buf = new char[4096];
//        int len;
//        while ( ( len = in.read( buf ) ) != -1 )
//            out.write( buf, 0, len );
//    }
//
//    /**
//     * 字串複製, 最長 4096 bytes
//     */
//    public static void copyStream( InputStream in, Writer out ) throws IOException {
//
//        byte[] buf1 = new byte[4096];
//        char[] buf2 = new char[4096];
//        int len, i;
//        while ( ( len = in.read( buf1 ) ) != -1 ) {
//            for ( i = 0; i < len; ++i )
//                buf2[i] = (char) buf1[i];
//            out.write( buf2, 0, len );
//        }
//    }
//
//    /**
//     * 字串複製, 最長 4096 bytes
//     */
//    public static void copyStream( Reader in, OutputStream out ) throws IOException
//    {
//        char[] buf1 = new char[4096];
//        byte[] buf2 = new byte[4096];
//        int len, i;
//        while ( ( len = in.read( buf1 ) ) != -1 ) {
//            for ( i = 0; i < len; ++i )
//                buf2[i] = (byte) buf1[i];
//            out.write( buf2, 0, len );
//        }
//    }
	
	
    public static String toHex(byte[] b){
        StringBuffer hex = new StringBuffer();
        for (int i=0; i<b.length; i++) {
        hex.append(""+"0123456789ABCDEF".charAt(0xf&b[i]>>4)+"0123456789ABCDEF".charAt(b[i]&0xf));
        }
        return hex.toString(); 
    }

    public static byte[] Hex2Bin(byte[] hex) {
        byte[] bin = new byte[hex.length / 2];
        for (int i = 0, j = 0; i < bin.length; i++, j += 2) {
            int iL = hex[j] - '0';
            if (iL > 9) {
                iL -= 7;
            }
            iL <<= 4;
            //
            int iR = hex[j + 1] - '0';
            if (iR > 9) {
                iR -= 7;
            }
            bin[i] = (byte) (iL | iR);
        }
        return bin;
    }
    
    /***
     * 將字串按指定長度切割成子字串
     * @param src 被切割的字串
     * @param length 子字串指定長度
     * @return 子字串組
     */
    public static String[] splitStringByLength(String src, int length) {
        // 檢查參數是否合法
        if (null == src||src.equals("")) {
        	log.debug("the string is null");
            return null;
        }
        
        if (length <= 0) {
            log.debug("the length < 0");
            return null;
        }
        
        
        String[] split = null;
		try {
			int n = (src.length() + length - 1) / length; // 字串可以被切割成子字串的個數
			
			split = new String[n];
			
			for (int i = 0; i < n; i++) {
			    if (i < (n -1)) {
			        split[i] = src.substring(i * length, (i + 1) * length);
			    } else {
			        split[i] = src.substring(i * length);
			    }
			}
		} catch (Exception e) {
			log.error("{}",e);
		}
        
        
        return split;
    }
    
    /**
     * 去掉前後全形空白
     * @param s
     * @return
     * https://www.baeldung.com/java-trim-alternatives
     * https://www.itread01.com/content/1549130583.html
     */
    public static String trimBlank(String s){
        String result = "";
        if(null!=s && !"".equals(s)){
            result = s.replaceAll("^[　| ]*", "").replaceAll("[　| ]*$", "");
//            result = s.replaceAll("^[　*| *|//s*]*", "").replaceAll("[　*| *|//s*]*$", "");
        }
        return result;
    }
    
    /**
     * 去掉右邊全形空白
     * @param s
     * @return
     */
    public static String trimRBlank(String s){
    	String result = "";
    	if(null!=s && !"".equals(s)){
    		result = s.replaceAll("[　| ]*$", "");
//    		result = s.replaceAll("[　*| *|//s*]*$", "");
    	}
    	return result;
    }
    /**
     * 去掉左邊全形空白
     * @param s
     * @return
     */
    public static String trimLBlank(String s){
    	String result = "";
    	if(null!=s && !"".equals(s)){
            result = s.replaceAll("^[　| ]*", "");
//            result = s.replaceAll("^[　*| *|//s*]*", "");
//            result = s.replaceAll("^[　*| *| *|//s*]*", "");
    	}
    	return result;
    }

}
