package com.netbank.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStream;
import java.util.Iterator;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import lombok.extern.slf4j.Slf4j;
@Slf4j
public class XmlUtil {

//	public static String getChanel(String filePath, String fileName) {
//		File f = null;
//		SAXReader reader = null;
//		Document doc = null;
//		Element foo = null;
//		String chanel = "";
//		Boolean isLegalPath =Boolean.FALSE;
//		try {
//			f = new File(filePath + fileName+".xml");
//			log.trace("f.getAbsolutePath>>{}",ESAPIUtil.vaildLog(f.getAbsolutePath()));
//			log.trace("f.path>>{}",ESAPIUtil.vaildLog(f.getPath()));
//			log.trace("f.getParent>>{}",ESAPIUtil.vaildLog(f.getParent()));
//			log.trace("filePath>>{}",ESAPIUtil.vaildLog(filePath));
////			isok = ESAPIUtil.getValidDirectoryPath(f.getParent(),new File(filePath) , "ValidDirectoryPath : ");
//			ESAPIUtil.isLegalPath(f.getParent(), filePath);
//			log.trace("isLegalPath>>{}",isLegalPath);
//			if(!isLegalPath) {
//				log.error("Illegal path>>{}",f.getParent());
//				return "";
//			}
//			reader = new SAXReader();
//			doc = reader.read(f);
//			Element root = doc.getRootElement();
//			Iterator tita_iterator = root.elementIterator("TITA");
//			while (tita_iterator.hasNext()) {
//				foo = (Element) tita_iterator.next();
//				Iterator item_iterator = foo.elementIterator("ITEM");
//				while (item_iterator.hasNext()) {
//					foo = (Element) item_iterator.next();
//					chanel = foo.getText();
//					log.trace("text>>{}" , ESAPIUtil.vaildLog(foo.getText()));
//					// 只抓第一筆
//					break;
//				}
//			}
//
//		} catch (Exception e) {
//			log.error(e.toString());
//		}
//		return chanel;
//	}
	
	
	public static String getChanel(String filePath, String fileName) {
//		File f = null;
		SAXReader reader = null;
		Document doc = null;
		Element foo = null;
		String chanel = "";
		Boolean isLegalPath =Boolean.FALSE;
		String legal = "";
		try {
			legal = filePath + fileName+".xml";
			chanel = parseXML(legal);
			log.info("chanel>>{}", ESAPIUtil.vaildLog(chanel) );
		} catch (Exception e) {
			log.error(e.toString());
		}
		return chanel;
	}

	public static String parseXML(String legal) {
//		public static String parseXML(File f) {
		String retStr = "";
		StringBuilder content = new StringBuilder();
		XMLInputFactory factory = XMLInputFactory.newInstance();
		factory.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, false);
		factory.setProperty(XMLInputFactory.SUPPORT_DTD, false);
		try {
			XMLStreamReader reader = factory.createXMLStreamReader(new FileReader(legal));
//			XMLStreamReader reader = factory.createXMLStreamReader(new FileInputStream(url));
//			log.trace("reader.getElementText()>>{}",reader.getElementText());
			while (reader.hasNext()) {
				if(reader.hasName()) {
					log.trace("reader.getName()>>{}",ESAPIUtil.vaildLog(reader.getName().toString()));
					if("ITEM".equals(reader.getName().toString()) ) {
						while(reader.hasNext()) {
							if (reader.hasText() ) {
								log.trace("reader.reader.getText()>>{}",ESAPIUtil.vaildLog(reader.getText()) );
								retStr = reader.getText();
								return  retStr;
							}
							reader.next();
						}
					}
				}
				reader.next();
			}
		} catch (XMLStreamException e) {
			log.error("{}", e);
		} catch (FileNotFoundException e) {
			log.error("{}", e);
		}
		return retStr;
	}
	
	
	
//	public static void main(String[] args) {
//		// TODO Auto-generated method stub
////		getChanel("D:\\mock_data\\xml\\", "C021");
//		parseXML(new File("D:\\mock_data\\xml\\N110.xml"));
//	}

}
