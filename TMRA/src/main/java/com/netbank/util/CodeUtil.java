package com.netbank.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.joda.time.DateTime;
import org.springframework.lang.NonNull;

import com.google.gson.Gson;

import lombok.extern.slf4j.Slf4j;

/**
 * 工具程式
 */
@Slf4j
public class CodeUtil {

	public static Map<String,String> hardWordMap = null;
	
	static Gson gs = new Gson();

	public static String toJson(Object obj) {
		return gs.toJson(obj);
	}

	public static <T> T fromJson(String json, Class<T> to) {
		return gs.fromJson(json, to);
	}

	public static <T> T objectCovert(Class<T> to, Object from) {
		String json = null;
		if (from == null) {
			log.error("  from Object is null");
			return null;
		}
		json = gs.toJson(from);
		T t = gs.fromJson(json, to);

		return t;
	}

	public static void convert2BaseResult(BaseResult bs, Object obj) {
		convert2BaseResult(bs, obj, null, null);
	}

	public static void convert2BaseResult(BaseResult bs, Object obj, Boolean showLog) {
		if (showLog = true)
			convert2BaseResultNoLog(bs, obj, null, null);
		else
			convert2BaseResult(bs, obj, null, null);
	}

	public static void convert2BaseResult(BaseResult bs, Object obj, @NonNull String msgCode, @NonNull String message) {
		// Gson gs = new Gson();
		String json = null;
		try {
			if (bs == null) {
				log.warn("警告 BaseResult is null...");
			}
			if (StrUtils.isNotEmpty(msgCode)) {
				bs.setMsgCode(msgCode);
			}
			if (StrUtils.isNotEmpty(message)) {
				bs.setMessage(message);
			}

			json = gs.toJson(obj);
			log.trace("convert2Baseresult.json>> {}", json);
			log.trace("convert2Baseresult.jsontoMap>> {}", gs.fromJson(json, Map.class));
			bs.setTBB_WS_Result(gs.fromJson(json, Map.class));
		} catch (Exception e) {
			log.error("convert2BaseResult.error>>{}", e);
		}
	}

	public static void convert2BaseResultNoLog(BaseResult bs, Object obj, @NonNull String msgCode,
			@NonNull String message) {
		// Gson gs = new Gson();
		String json = null;
		try {
			log.trace("convert2Baseresult...");
			if (bs == null) {
				log.warn("警告 BaseResult is null...");
			}
			if (StrUtils.isNotEmpty(msgCode)) {
				bs.setMsgCode(msgCode);
			}
			if (StrUtils.isNotEmpty(message)) {
				bs.setMessage(message);
			}

			json = gs.toJson(obj);
			// log.trace("convert2Baseresult.json>> {}", json);
			// log.trace("convert2Baseresult.jsontoMap>> {}", gs.fromJson(json, Map.class));
			bs.setTBB_WS_Result(gs.fromJson(json, Map.class));
		} catch (Exception e) {
			log.error("convert2BaseResult.error>>{}", e);
		}
	}

	/**
	 * OBJECT轉成BYTE[]
	 */
	public static byte[] objectToByte(Object object) {
		byte[] result = null;
		ObjectOutputStream objectOutputStream = null;

		try {
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
			objectOutputStream.writeObject(object);
			result = byteArrayOutputStream.toByteArray();
		} catch (Exception e) {
			log.error(String.valueOf(e));
		}
		if (objectOutputStream != null) {
			try {
				objectOutputStream.close();
			} catch (Exception e) {
				log.error(String.valueOf(e));
			}
		}
		return result;
	}

	/**
	 * BYTE[]轉成OBJECT
	 */
	public static Object byteToObject(byte[] byteArray) {
		Object result = null;
		ByteArrayInputStream byteArrayInputStream = null;
		ObjectInputStream objectInputStream = null;

		try {
			byteArrayInputStream = new ByteArrayInputStream(byteArray);
			objectInputStream = new ObjectInputStream(byteArrayInputStream);
			result = objectInputStream.readObject();
		} catch (Exception e) {
			log.error(String.valueOf(e));
		}
		if (objectInputStream != null) {
			try {
				objectInputStream.close();
			} catch (Exception e) {
				log.error(String.valueOf(e));
			}
		}
		if (byteArrayInputStream != null) {
			try {
				byteArrayInputStream.close();
			} catch (Exception e) {
				log.error(String.valueOf(e));
			}
		}
		return result;
	}

	/**
	 * BYTE[]轉成HEX
	 */
	public static String ByteArrayToHexString(byte[] byteArray) {
		StringBuilder stringBuilder = new StringBuilder();
		for (byte b : byteArray) {
			stringBuilder.append(String.format("%02x", b & 0xff));
		}
		return stringBuilder.toString();
	}

	/**
	 * HEX轉成BYTE[]
	 */
	public static byte[] hexStringToByteArray(String string) {
		int length = string.length();
		byte[] data = new byte[length / 2];
		for (int i = 0; i < length; i += 2) {
			data[i / 2] = (byte) ((Character.digit(string.charAt(i), 16) << 4)
					+ Character.digit(string.charAt(i + 1), 16));
		}
		return data;
	}

	/**
	 * 將兩個byte[]合併
	 */
	public static byte[] combineByte(byte[] byteOne, byte[] byteTwo) {
		try {
			byte[] result = new byte[byteOne.length + byteTwo.length];

			System.arraycopy(byteOne, 0, result, 0, byteOne.length);
			System.arraycopy(byteTwo, 0, result, byteOne.length, byteTwo.length);

			return result;
		} catch (Exception e) {
			log.error(String.valueOf(e));

			return null;
		}
	}

	/**
	 * 將XML轉成字串
	 */
	public static String marshalXML(Object source) {
		StringWriter stringWriter = new StringWriter();

		try {
			JAXBContext context = JAXBContext.newInstance(source.getClass());
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			marshaller.marshal(source, stringWriter);
		} catch (Exception e) {
			log.error("", e);
		}
		return stringWriter.toString();
	}

	/**
	 * 將字串轉成XML
	 */
	public static <T> T unmarshalXML(Object target, String xml) {
		try {
			JAXBContext context = JAXBContext.newInstance(target.getClass());
			Unmarshaller unmarshaller = context.createUnmarshaller();
			StringReader stringReader = new StringReader(xml);
			return (T) unmarshaller.unmarshal(stringReader);
		} catch (Exception e) {
			log.error("", e);
			return null;
		}
	}

	/**
	 * 組出上行電文的header
	 * 
	 * @param len
	 *            原始電文長度
	 * @return
	 */
	public static byte[] getTelHeader(Integer len) {
		byte[] header = new byte[4];
		log.trace("header.length>>{}", header.length);
		log.trace("(len+header.length)>>{}", (len + header.length));
		// header[0] = 0x00;
		// header[1] = (byte)(len+header.length);
		// header[2] = 0x00;
		// header[3] = 0x00;
		// short a1 = 0;
		short a1 = (short) (len + header.length);
		// log.trace("a1>>{}",(a1 >> 8));
		log.trace("a2>>{}", (a1 >> 8));
		log.trace("a2_1>>{}", (a1 % 256));

		header[0] = (byte) (a1 >> 8);
		header[1] = (byte) (a1 % 256);
		header[2] = 0;
		header[3] = 0;
		return header;

	}

	/**
	 * 根據停車費電文長度組Header
	 * 
	 * @param len
	 * @return
	 */
	public static byte[] getParkTelHeader(Integer len) {
		byte[] header = new byte[2];
		log.trace("header.length>>{}", header.length);
		log.trace("(len+header.length)>>{}", (header.length));
		// short a1 = (short)(header.length);
		Short a1 = (short) (len.intValue());
		log.trace("a1>>{}", a1);
		log.trace("a2>>{}", (a1 >> 8));
		log.trace("a2_1>>{}", (a1 % 256));
		header[0] = (byte) (a1 >> 8);
		header[1] = (byte) (a1 % 256);
		return header;

	}

	// public static byte[] getParkTelHeader(Integer len) {
	// String s ="";
	// s = String.format("%02d", len);
	// log.trace("s>>{}",s);
	// byte[] header = new byte[s.getBytes().length];
	// System.arraycopy(s.getBytes(),0,header,0,s.getBytes().length);
	// return header;
	//
	// }

	/**
	 * 根據基金電文長度組Header
	 * 
	 * @return
	 */
	public static byte[] getFundTelHeader(Integer len) {
		String s = "";
		// +4表示包含長度字串本身
		s = String.format("%04d", len + 4);
		log.trace("s>>{}", s);
		byte[] header = new byte[s.getBytes().length];
		System.arraycopy(s.getBytes(), 0, header, 0, s.getBytes().length);
		// header = s.getBytes();
		// Arrays.fill(header, (byte)0x00);
		return header;

	}

	/**
	 * 根據外匯AS400電文長度組Header
	 * 
	 * @param len
	 * @return
	 */
	public static byte[] getFXTelHeader(Integer len) {
		String s = "";
		// +4表示包含長度字串本身
		s = String.format("%04d", len);
		log.trace("s>>{}", s);
		byte[] header = null;
		try {
			header = new byte[s.getBytes("CP937").length];
			System.arraycopy(s.getBytes("CP937"), 0, header, 0, s.getBytes().length);
		} catch (Exception e) {
			log.error("{}", e);
		}
		return header;

	}

	public static String EBCDICtoBig5(byte[] ebcdic) {
		String s = "";
		char[] ebcdic_char = null;
		byte[] big5 = null;
		try {
			ebcdic_char = new String(ebcdic, "CP937").toCharArray();
			big5 = new String(ebcdic_char).getBytes("Big5");
			s = new String(big5);
		} catch (UnsupportedEncodingException e) {
			log.error("{}", e);
		}

		return s;

	}

	/**
	 * 將上行電文byte array中的各byte值輸出於螢幕上
	 * 
	 * @param TITA
	 *            上行電文byte array
	 */
	public static void printOutTITA(String txid, byte[] TITA) {
		StringBuffer buf = new StringBuffer("");
		try {
			log.trace("-->[{}]TITA Data in ASCII.....", txid);
			// log.trace("-->"+TITAForDB);
			log.trace("-->TITA Byte Data sent to TMRA.....");

			int iValue;
			String hexValue;
			for (int i = 0; i < TITA.length; i++) {
				iValue = TITA[i];
				hexValue = Integer.toHexString(iValue).toUpperCase();
				if (hexValue.length() == 8) {
					hexValue = hexValue.substring(6);
				}
				buf.append(hexValue).append(" ");
			}

			log.trace("-->" + buf.toString());
		} catch (Exception e) {
			// logger.debug(e.toString());
			log.error("", e);
		}
	}

	/**
	 * 將所有收到的下行電文byte array輸出於螢幕上 TOTAs 所有收到的下行電文byte array
	 */
	public void printOutTOTA(String txid, byte[] TOTA) {
		log.trace("printOutTOTA-->[{}]TOTA Data in ASCII.....", txid);
		log.trace(" 0 1 2 3  4 5 6 7  8 9 A B  C D E F");
		log.trace("--------|--------|--------|--------|");
		log.trace(toHexString(TOTA));

	}

	/**
	 * 將傳入的byte值轉為十六進位並將其轉成字元存於一StringBuffer物件中
	 * 
	 * @param b
	 *            被轉換的byte
	 * @param buf
	 *            儲存轉換結果之StringBuffer物件
	 */
	public static void byte2hex(byte b, StringBuffer buf) {
		char[] hexChars = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
		int high = ((b & 0xf0) >> 4);
		int low = (b & 0x0f);
		buf.append(hexChars[high]);
		buf.append(hexChars[low]);
	}

	public static String toHexString(byte[] block) {
		StringBuffer buf = new StringBuffer("");
		int len = block.length;
		for (int i = 0; i < len; i++) {
			byte2hex(block[i], buf);
			if ((i + 1) % 16 == 0)
				buf.append("\n");
			else if ((i + 1) % 4 == 0) {
				buf.append(" ");
			}
		}
		return buf.toString();
	}
	public static String toHexStringII(byte[] block) {
		StringBuffer buf = new StringBuffer("");
		int len = block.length;
		for (int i = 0; i < len-1; i++) {
			if(i==0) {
				continue;
			}
			byte2hex(block[i], buf);
			if ((i ) % 2 == 0) {
				buf.append(" ");
			}
		}
		return buf.toString();
	}
	public static String toHexStringIII(byte[] block) {
		StringBuffer buf = new StringBuffer("");
		int len = block.length;
		for (int i = 0; i < len-1; i++) {
			if(i==0) {
				continue;
			}
			byte2hex(block[i], buf);
			if ((i ) % 6 == 0) {
				buf.append(" ");
			}
		}
		return buf.toString();
	}
	
	
	
	public static byte[] hardWordProcess(byte[] ebcdic_byteAry) {
		String ebc_hexstr = null;
		String utf8_hexstr = null;
		byte[] utf8_ByteArray =  null;
		try {
			utf8_ByteArray = new String(ebcdic_byteAry, "CP937").getBytes("UTF-8");
			
//			System.out.println("碁UTF-8-H>>"+ByteArrayToHexString("碁".getBytes("UTF-8")).toUpperCase());
//			System.out.println("銹UTF-8-H>>"+ByteArrayToHexString("銹".getBytes("UTF-8")).toUpperCase());
//			System.out.println("裏UTF-8-H>>"+ByteArrayToHexString("裏".getBytes("UTF-8")).toUpperCase());
//			System.out.println("墻UTF-8-H>>"+ByteArrayToHexString("墻".getBytes("UTF-8")).toUpperCase());
//			System.out.println("恒UTF-8-H>>"+ByteArrayToHexString("恒".getBytes("UTF-8")).toUpperCase());
//			System.out.println("粧UTF-8-H>>"+ByteArrayToHexString("粧".getBytes("UTF-8")).toUpperCase());
//			System.out.println("嫺UTF-8-H>>"+ByteArrayToHexString("嫺".getBytes("UTF-8")).toUpperCase());
			
			if(hardWordMap == null || hardWordMap.isEmpty()) {
				
				hardWordMap = new LinkedHashMap<String, String>();
				hardWordMap.put("EFA093", "E7A281");
				hardWordMap.put("EFA094", "E98AB9");
				hardWordMap.put("EFA095", "E8A38F");
				hardWordMap.put("EFA096", "E5A2BB");
				hardWordMap.put("EFA097", "E68192");
				hardWordMap.put("EFA098", "E7B2A7");
				hardWordMap.put("EFA099", "E5ABBA");
				
//				hardWordMap.put("EFA093", "碁");
//				hardWordMap.put("EFA094", "銹");
//				hardWordMap.put("EFA095", "裏");
//				hardWordMap.put("EFA096", "墻");
//				hardWordMap.put("EFA097", "恒");
//				hardWordMap.put("EFA098", "粧");
//				hardWordMap.put("EFA099", "嫺");
			}
			ebc_hexstr =  ByteArrayToHexString(ebcdic_byteAry).toUpperCase() ;
			log.trace("ebc_hexstr>>{}",ebc_hexstr);
//			中文才有難字問題
			if(!(ebc_hexstr.startsWith("0E") && ebc_hexstr.lastIndexOf("0F") != -1)) {
				return utf8_ByteArray;
			}
			
			utf8_hexstr = ByteArrayToHexString(utf8_ByteArray).toUpperCase();
			
//			System.out.println("utf8_hexstr0>>"+utf8_hexstr);
			log.trace("check hard word...");
			for(String k:hardWordMap.keySet()) {
				if(utf8_hexstr.indexOf(k) != -1) {
					utf8_hexstr = utf8_hexstr.replace(k,  hardWordMap.get(k) );
//					utf8_hexstr = utf8_hexstr.replace(k, ByteArrayToHexString( hardWordMap.get(k).getBytes("UTF-8") ));
				}
			}
//			System.out.println("utf8_hexstr>>"+utf8_hexstr);
//			System.out.println("utf8_hexstr>>"+ new String( hexStringToByteArray(utf8_hexstr)  , "UTF-8")  );
			
			utf8_ByteArray = hexStringToByteArray(utf8_hexstr);
		} catch (Exception e) {
			log.error("hardWordProcess.error>>{}",e);
		}
		return utf8_ByteArray;
	}
	
	
	
	
	
//	public static byte[] hardWordProcess(byte[] ebcdic_byteAry) {
//		String ebc_hexstr = null;
//		String utf8str = null;
//		byte[] big5_ByteArray = null;
//		byte[] utf8_ByteArray =  null;
//		LinkedList<String> wordMappingList = new LinkedList<String>();
//		try {
//			utf8_ByteArray = new String(ebcdic_byteAry, "CP937").getBytes("UTF-8");
//			
//			System.out.println("UTF-8>>"+new String(utf8_ByteArray, "UTF-8"));
//			System.out.println("UTF-8-H>>"+ByteArrayToHexString(utf8_ByteArray));
//			byte[] utf16_ByteArray = new String(ebcdic_byteAry, "CP937").getBytes("UTF-16");
//			System.out.println("UTF-16>>"+new String(utf16_ByteArray, "UTF-16"));
//			
//			big5_ByteArray = new String(ebcdic_byteAry, "CP937").getBytes("BIG5");
//			System.out.println("BIG5>>"+new String(big5_ByteArray, "BIG5"));
//			
//			utf8_ByteArray = new String(big5_ByteArray, "BIG5").getBytes("UTF-8");
//			if(hardWordMap == null || hardWordMap.isEmpty()) {
//				
//				hardWordMap = new LinkedHashMap<String, String>();
//				hardWordMap.put("E2D5", "碁");
//				hardWordMap.put("E2D6", "銹");
//				hardWordMap.put("E2D7", "裏");
//				hardWordMap.put("E2D8", "墻");
//				hardWordMap.put("E2D9", "恒");
//				hardWordMap.put("E2DA", "粧");
//				hardWordMap.put("E2DB", "嫺");
//			}
//			ebc_hexstr =  ByteArrayToHexString(ebcdic_byteAry).toUpperCase() ;
//			log.trace("ebc_hexstr>>{}",ebc_hexstr);
////			System.out.println("toHexStringII>>"+toHexStringII(ebcdic_byteAry)); 
//			log.trace("ebc_hexstr>>{}",ebc_hexstr);
////			中文才有難字問題
//			if(!(ebc_hexstr.startsWith("0E") && ebc_hexstr.lastIndexOf("0F") != -1)) {
//				return utf8_ByteArray;
//			}
//			
//			log.trace("check hard word...");
////			移除0E0F 4字元個為一組(EBCDIC HEX 4個字元為一個字)
//			String tmp = toHexStringII(ebcdic_byteAry);
//			Boolean isHardWord = Boolean.FALSE;
//			
//			for(String k:hardWordMap.keySet()) {
//				if(tmp.indexOf(k) != -1) {
//					isHardWord = Boolean.TRUE;
//					break;
//				}
//			}
//			
//			if(!isHardWord) {
//				return utf8_ByteArray;
//			}
//			log.trace("hard word pre process...");
//			String[] sAry = tmp.split(" ");
//			
//			for(int i=0 ;i<sAry.length;i++) {
//				if(hardWordMap.containsKey(sAry[i]) ) {
//					wordMappingList.add(hardWordMap.get(sAry[i]));
//				}
//			}
//			
//			utf8str = new String(utf8_ByteArray,"UTF-8");
////			System.out.println("wordMappingList>>"+wordMappingList);
////			表示有難字 難字在轉譯過程中會變?  例外:如果出現未在難字表裡面的難字 會有bug 
//			if(wordMappingList !=null && !wordMappingList.isEmpty() ) {
//				log.trace("find ? 有難字");
//				for(String s :wordMappingList) {
////					System.out.println("utf8str>>"+utf8str);
//					utf8str = utf8str.trim().replaceFirst("\\?", s);
//				}
//			}
//			utf8_ByteArray = utf8str.getBytes("UTF-8");
//		} catch (Exception e) {
//			log.error("{}",e);
//		}
//		
//		
//		return utf8_ByteArray;
//	}

	
//	
//	
//	public static void main(String[] args) {
//		// testBig5andutf8();
//		DateTime d1 = new DateTime();
//		System.out.println("d1>>"+d1);
//		byte[] b = hardWordProcess(hexStringToByteArray("0EE2DBE2D6E2D7E2D8E2D9E2DAE2DB404040400F".replaceAll(" ","")) );
////		byte[] b = hardWordProcess(hexStringToByteArray("0E 58 F2 E2 D7 60 E7 E2 D5 75 CF 40 40 40 40 40 40 40 40 0F".replaceAll(" ","")) );
//		System.out.println("diff>>"+DateUtil.getDiffTimeMills(d1, new DateTime()));
//		try {
//			System.out.println("b>>"+new String(b,"UTF-8"));
//		} catch (UnsupportedEncodingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}

}