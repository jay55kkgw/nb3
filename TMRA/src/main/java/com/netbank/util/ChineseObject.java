package com.netbank.util;

/**
 * 儲存中文物件
 */
public class ChineseObject{
	//EBCDIC內碼
	private byte[] ebcdicCode;
	//中文內碼
	private byte[] chineseCode;
	//是否為難字
	private boolean isDifficultWord = false;
	//難字索引字
	private int[] difficultIndex;
	private boolean isChinese = false;
	
	public byte[] getEbcdicCode(){
		return ebcdicCode;
	}
	public void setEbcdicCode(byte[] ebcdicCode){
		this.ebcdicCode = ebcdicCode;
	}
	public byte[] getChineseCode(){
		return chineseCode;
	}
	public void setChineseCode(byte[] chineseCode){
		this.chineseCode = chineseCode;
	}
	public boolean isDifficultWord(){
		return isDifficultWord;
	}
	public void setDifficultWord(boolean isDifficultWord){
		this.isDifficultWord = isDifficultWord;
	}
	public int[] getDifficultIndex(){
		return difficultIndex;
	}
	public void setDifficultIndex(int[] difficultIndex){
		this.difficultIndex = difficultIndex;
	}
	public boolean isChinese(){
		return isChinese;
	}
	public void setChinese(boolean isChinese){
		this.isChinese = isChinese;
	}
}