package com.netbank.util;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class HexDumpUtil {
	static public int HD_HEX = 0, HD_ASCII = 1, HD_EBCDIC = 2;
	public static final char[] hexchars = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c',
			'd', 'e', 'f' };
	static byte[] ebc2asc = { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, /* 00-0f */
			0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,

			0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, /* 10-1f */
			0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F,

			0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, /* 20-2f */
			0x28, 0x29, 0x2A, 0x2B, 0x2C, 0x2D, 0x2E, 0x2F,

			0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, /* 30-3f */
			0x38, 0x39, 0x3A, 0x3B, 0x3C, 0x3D, 0x3E, 0x3F,

			0x20, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, /* 40-4f */
			0x48, 0x49, 0x5E, 0x2E, 0x3C, 0x28, 0x2B, 0x4F,

			0x26, 0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57, /* 50-5f */
			0x58, 0x59, 0x21, 0x24, 0x2A, 0x29, 0x3B, 0x5F,

			0x2D, 0x2F, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, /* 60-6f */
			0x68, 0x69, 0x7C, 0x2C, 0x25, 0x5F, 0x3E, 0x3F,

			0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77, /* 70-7f */
			0x78, 0x60, 0x3A, 0x23, 0x40, 0x27, 0x3D, 0x22,

			(byte) 0x80, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, /* 80-8f */
			0x68, 0x69, (byte) 0x8A, (byte) 0x8B, (byte) 0x8C, (byte) 0x8D, (byte) 0x8E, (byte) 0x8F,

			(byte) 0x90, 0x6A, 0x6B, 0x6C, 0x6D, 0x6E, 0x6F, 0x70, /* 90-9f */
			0x71, 0x72, (byte) 0x9A, (byte) 0x9B, (byte) 0x9C, (byte) 0x9D, (byte) 0x9E, (byte) 0x9F,

			(byte) 0xA0, 0x7E, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78, /* a0-af */
			0x79, 0x7A, (byte) 0xAA, (byte) 0xAB, (byte) 0xAC, (byte) 0xAD, (byte) 0xAE, (byte) 0xAF,

			(byte) 0xB0, (byte) 0xB1, (byte) 0xB2, (byte) 0xB3, (byte) 0xB4, (byte) 0xB5, (byte) 0xB6, (byte) 0xB7, /*
																													 * b0
																													 * -
																													 * bf
																													 */
			(byte) 0xB8, (byte) 0xB9, (byte) 0xBA, (byte) 0xBB, (byte) 0xBC, (byte) 0xBD, (byte) 0xBE, (byte) 0xBF,

			0x7B, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, /* c0-cf */
			0x48, 0x49, (byte) 0xCA, (byte) 0xCB, (byte) 0xCC, (byte) 0xCD, (byte) 0xCE, (byte) 0xCF,

			0x7D, 0x4A, 0x4B, 0x4C, 0x4D, 0x4E, 0x4F, 0x50, /* d0-df */
			0x51, 0x52, (byte) 0xDA, (byte) 0xDB, (byte) 0xDC, (byte) 0xDD, (byte) 0xDE, (byte) 0xDF,

			0x5C, (byte) 0xE1, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, /* e0-ef */
			0x59, 0x5A, (byte) 0xEA, (byte) 0xEB, (byte) 0xEC, (byte) 0xED, (byte) 0xEE, (byte) 0xEF,

			0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, /* f0-ff */
			0x38, 0x39, (byte) 0xFA, (byte) 0xFB, (byte) 0xFC, (byte) 0xFD, (byte) 0xFE, (byte) 0xFF, };

	/* ********************************************************************** */
	/* ASCII to EBCDIC Translate Table */
	/* ********************************************************************** */

	static byte[] asc2ebc = { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, /* 00-0f */
			0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,

			0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, /* 10-0f */
			0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F,

			0x40, 0x5A, 0x7F, 0x7B, 0x5B, 0x6C, 0x50, 0x7D, /* 20-2f */
			0x4D, 0x5D, 0x5C, 0x4E, 0x6B, 0x60, 0x4B, 0x61,

			(byte) 0xF0, (byte) 0xF1, (byte) 0xF2, (byte) 0xF3, (byte) 0xF4, (byte) 0xF5, (byte) 0xF6, (byte) 0xF7, /*
																													 * 30
																													 * -
																													 * 3f
																													 */
			(byte) 0xF8, (byte) 0xF9, 0x7A, 0x5E, 0x4C, 0x7E, 0x6E, 0x6F,

			0x7C, (byte) 0xC1, (byte) 0xC2, (byte) 0xC3, (byte) 0xC4, (byte) 0xC5, (byte) 0xC6, (byte) 0xC7, /* 40-4f */
			(byte) 0xC8, (byte) 0xC9, (byte) 0xD1, (byte) 0xD2, (byte) 0xD3, (byte) 0xD4, (byte) 0xD5, (byte) 0xD6,

			(byte) 0xD7, (byte) 0xD8, (byte) 0xD9, (byte) 0xE2, (byte) 0xE3, (byte) 0xE4, (byte) 0xE5, (byte) 0xE6, /*
																													 * 50
																													 * -
																													 * 5f
																													 */
			(byte) 0xE7, (byte) 0xE8, (byte) 0xE9, (byte) 0xFF, (byte) 0xE0, (byte) 0xFF, (byte) 0x5F, (byte) 0x6D,

			0x79, (byte) 0x81, (byte) 0x82, (byte) 0x83, (byte) 0x84, (byte) 0x85, (byte) 0x86, (byte) 0x87, /* 60-6f */
			(byte) 0x88, (byte) 0x89, (byte) 0x91, (byte) 0x92, (byte) 0x93, (byte) 0x94, (byte) 0x95, (byte) 0x96,

			(byte) 0x97, (byte) 0x98, (byte) 0x99, (byte) 0xA2, (byte) 0xA3, (byte) 0xA4, (byte) 0xA5, (byte) 0xA6, /*
																													 * 70
																													 * -
																													 * 7f
																													 */
			(byte) 0xA7, (byte) 0xA8, (byte) 0xA9, (byte) 0xC0, 0x6A, (byte) 0xD0, (byte) 0xA1, 0x7F,

			(byte) 0x80, (byte) 0x81, (byte) 0x82, (byte) 0x83, (byte) 0x84, (byte) 0x85, (byte) 0x86, (byte) 0x87, /*
																													 * 80
																													 * -
																													 * 8f
																													 */
			(byte) 0x88, (byte) 0x89, (byte) 0x8A, (byte) 0x8B, (byte) 0x8C, (byte) 0x8D, (byte) 0x8E, (byte) 0x8F,

			(byte) 0x90, (byte) 0x91, (byte) 0x92, (byte) 0x93, (byte) 0x94, (byte) 0x95, (byte) 0x96, (byte) 0x97, /*
																													 * 90
																													 * -
																													 * 9f
																													 */
			(byte) 0x98, (byte) 0x99, (byte) 0x9A, (byte) 0x9B, (byte) 0x9C, (byte) 0x9D, (byte) 0x9E, (byte) 0x9F,

			(byte) 0xA0, (byte) 0xA1, (byte) 0xA2, (byte) 0xA3, (byte) 0xA4, (byte) 0xA5, (byte) 0xA6, (byte) 0xA7, /*
																													 * a0
																													 * -
																													 * af
																													 */
			(byte) 0xA8, (byte) 0xA9, (byte) 0xAA, (byte) 0xAB, (byte) 0xAC, (byte) 0xAD, (byte) 0xAE, (byte) 0xAF,

			(byte) 0xB0, (byte) 0xB1, (byte) 0xB2, (byte) 0xB3, (byte) 0xB4, (byte) 0xB5, (byte) 0xB6, (byte) 0xB7, /*
																													 * b0
																													 * -
																													 * bf
																													 */
			(byte) 0xB8, (byte) 0xB9, (byte) 0xBA, (byte) 0xBB, (byte) 0xBC, (byte) 0xBD, (byte) 0xBE, (byte) 0xBF,

			(byte) 0xC0, (byte) 0xC1, (byte) 0xC2, (byte) 0xC3, (byte) 0xC4, (byte) 0xC5, (byte) 0xC6, (byte) 0xC7, /*
																													 * c0
																													 * -
																													 * cf
																													 */
			(byte) 0xC8, (byte) 0xC9, (byte) 0xCA, (byte) 0xCB, (byte) 0xCC, (byte) 0xCD, (byte) 0xCE, (byte) 0xCF,

			(byte) 0xD0, (byte) 0xD1, (byte) 0xD2, (byte) 0xD3, (byte) 0xD4, (byte) 0xD5, (byte) 0xD6, (byte) 0xD7, /*
																													 * d0
																													 * -
																													 * df
																													 */
			(byte) 0xD8, (byte) 0xD9, (byte) 0xDA, (byte) 0xDB, (byte) 0xDC, (byte) 0xDD, (byte) 0xDE, (byte) 0xDF,

			(byte) 0xE0, (byte) 0xE1, (byte) 0xE2, (byte) 0xE3, (byte) 0xE4, (byte) 0xE5, (byte) 0xE6, (byte) 0xE7, /*
																													 * e0
																													 * -
																													 * ef
																													 */
			(byte) 0xE8, (byte) 0xE9, (byte) 0xEA, (byte) 0xEB, (byte) 0xEC, (byte) 0xED, (byte) 0xEE, (byte) 0xEF,

			(byte) 0xF0, (byte) 0xF1, (byte) 0xF2, (byte) 0xF3, (byte) 0xF4, (byte) 0xF5, (byte) 0xF6, (byte) 0xF7, /*
																													 * f0
																													 * -
																													 * ff
																													 */
			(byte) 0xF8, (byte) 0xF9, (byte) 0xFA, (byte) 0xFB, (byte) 0xFC, (byte) 0xFD, (byte) 0xFE, (byte) 0xFF, };

	public HexDumpUtil() {

	}

	public HexDumpUtil(int Option, String Title, byte[] buffer, int len) {
	}

	public static String format(byte[] buf) {
		return format(buf, 80);
	}

	public static String format(byte[] buf, int width) {
		int bs = (width - 8) / 4;
		int i = 0;
		StringBuffer sb = new StringBuffer();
		do {
			for (int j = 0; j < 6; j++) {
				sb.append(hexchars[(i << (j * 4) & 0xF00000) >> 20]);
			}
			sb.append('\t');
			sb.append(toHex(buf, i, bs));
			sb.append(' ');
			sb.append(toAscii(buf, i, bs));
			sb.append('\n');
			i += bs;
		} while (i < buf.length);
		return sb.toString();
	}
	public static String format(byte[] buf, int width , Integer option) {
		int bs = (width - 8) / 4;
		int i = 0;
		StringBuffer sb = new StringBuffer();
//		log.trace("option>>{}",option);
		do {
			for (int j = 0; j < 6; j++) {
				sb.append(hexchars[(i << (j * 4) & 0xF00000) >> 20]);
			}
			sb.append('\t');
			sb.append(toHex(buf, i, bs));
			sb.append(' ');
//			sb.append(toAscii(buf, i, bs));
			switch (option) {
			case 2:
				sb.append(ebctoAscii(buf, i, bs));
				break;

			default:
				sb.append(toAscii(buf, i, bs));
				break;
			}
			sb.append('\n');
			i += bs;
		} while (i < buf.length);
		return sb.toString();
	}

	public static String toAscii(byte[] buf, int ofs, int len) {
		StringBuffer sb = new StringBuffer();
		int j = ofs + len;
		for (int i = ofs; i < j; i++) {
			if (i < buf.length) {
				if ((20 <= buf[i]) && (126 >= buf[i])) {
					sb.append((char) buf[i]);
				} else {
					sb.append('.');
				}
			} else {
				sb.append(' ');
			}
		}
		return sb.toString();
	}
	
	public static String ebctoAscii(byte[] buf, int ofs, int len) {
		
		StringBuffer sb = new StringBuffer();
		int j = ofs + len;
		for (int i = ofs; i < j; i++) {
			if (i < buf.length) {
				int aa=buf[i]<0?(256+(buf[i])):buf[i];
				if ((ebc2asc[aa])>= 0x20 && (ebc2asc[aa])<= 0x7e) {
//				if ((20 <= ebc2asc[i]) && (126 >= ebc2asc[i])) {
					sb.append((char) ebc2asc[aa]);
				} else {
					sb.append('.');
				}
			} else {
				sb.append(' ');
			}
		}
//		log.trace("ebctoAscii.str>>{}",sb.toString());
		return sb.toString();
	}

	public static String toHex(byte[] buf, int ofs, int len) {
		StringBuffer sb = new StringBuffer();
		int j = ofs + len;
		for (int i = ofs; i < j; i++) {
			if (i < buf.length) {
				sb.append(hexchars[(buf[i] & 0xF0) >> 4]);
				sb.append(hexchars[buf[i] & 0x0F]);
				sb.append(' ');
			} else {
				sb.append(' ');
				sb.append(' ');
				sb.append(' ');
			}
		}
		return sb.toString().toUpperCase();
	}

	private static void byte2hex(byte b, StringBuffer buf) {
		char[] hexChars = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
		int high = ((b & 0xf0) >> 4);
		int low = (b & 0x0f);
		buf.append(hexChars[high]);
		buf.append(hexChars[low]);
	}
}
