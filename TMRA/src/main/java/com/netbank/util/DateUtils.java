package com.netbank.util;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateUtils {
    public static Date beEnd(Date d) {
        DateTime dt = new DateTime(d);
        dt = dt.plusMinutes(1);
        return dt.toDate();
    }

    public static Date beStart(Date d) {
        DateTime dt = new DateTime(d);
        return dt.toDate();
    }

    public static String formatDate(Date date, String pattern) {
        return formatDate(date, pattern, (TimeZone)null);
    }

    public static String formatDate(Date date, String pattern, TimeZone timeZone) {
        return formatDate(date, pattern, timeZone, (Locale)null);
    }

    public static String formatDate(Date date, String pattern, TimeZone timeZone, Locale locale) {
        if (date == null) {
            return null;
        } else {
            SimpleDateFormat sdf;
            if (locale == null) {
                sdf = new SimpleDateFormat(pattern);
            } else {
                sdf = new SimpleDateFormat(pattern, locale);
            }

            if (timeZone != null) {
                sdf.setTimeZone(timeZone);
            }

            return sdf.format(date);
        }
    }

    public static Date parseDate(String date, String pattern) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            return sdf.parse(date);
        } catch(Exception e) {
            throw new RuntimeException("parseDate Error. (date: " + date + ", pattern: " + pattern, e);
        }
    }
}
