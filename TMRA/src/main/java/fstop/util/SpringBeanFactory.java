package fstop.util;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * 2019 jimmy 重寫
 */
@Slf4j
@Component
@Data
public class SpringBeanFactory {

	private static ApplicationContext context;

	public static void setApplicationContext(ApplicationContext _context) {
		context = _context;
	}


	public static Object getBean(String beanid) {


		return context.getBean(beanid);
	}
}
