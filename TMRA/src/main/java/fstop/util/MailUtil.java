package fstop.util;

import java.io.*;
import java.util.*;

import com.netbank.util.CodeUtil;

/**
 * 
 * 功能說明 :組mail內容用
 *
 */
public class MailUtil {
	

	/**
	 * GW異常電文內容
	 * @param errorCode
	 * @param channel
	 * @param txid
	 * @param env
	 * @return
	 */
	public String getMailContentForGW(String errorCode,String channel ,String txid , String env) {
		StringBuffer sb = new StringBuffer();
		sb.append("<html>\n<head>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF8\" />\n");
		switch (env) {
		case "D":
			sb.append("<title>開發套新個網NB3GW異常通知</title>\n");
			break;
		case "T":
			sb.append("<title>測試套新個網NB3GW異常通知</title>\n");
			break;
		default:
			sb.append("<title>新個網NB3GW異常通知</title>\n");
			break;
		}
		sb.append("</head>\n");
		sb.append("<body>\n");
		sb.append("channel:"+ channel+"<BR>");
		sb.append("錯誤代碼:"+ errorCode+",電文代號:"+txid);
		sb.append("</body>\n");
		sb.append("</html>\n");
		
		return sb.toString();
	}
	
	/**
	 * tmra 發現有Queue住的訊息
	 * @param errorCode
	 * @param channel
	 * @param txid
	 * @param env
	 * @return
	 */
	public String getMailContentForQueue(String cnt,String channel , String env) {
		StringBuffer sb = new StringBuffer();
		sb.append("<html>\n<head>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF8\" />\n");
		switch (env) {
		case "D":
			sb.append("<title>開發套新個網NB3GW_Queue通知</title>\n");
			break;
		case "T":
			sb.append("<title>測試套新個網NB3GW_Queue通知</title>\n");
			break;
		default:
			sb.append("<title>新個網NB3GW_Queue通知</title>\n");
			break;
		}
		sb.append("</head>\n");
		sb.append("<body>\n");
		sb.append("channel:"+ channel+"<BR>");
		sb.append("發現Queue筆數，共:"+ cnt+"筆<BR>");
		sb.append("詳細資料請參考tmra.log");
		sb.append("</body>\n");
		sb.append("</html>\n");
		
		return sb.toString();
	}
	
	
}
