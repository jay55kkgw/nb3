package fstop.util;

import fstop.core.BeanUtils;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class JSONUtils {
	public static String map2json(Map m) {
		return JSONObject.fromObject(m).toString();
	}
	
	public static Map json2map(String json) {
		return JSONObject.fromObject(json);
	}
	
	public static String toJson(Object o) {
		if(o instanceof List)
			return JSONArray.fromObject(o).toString(); 
		else if(o instanceof Map)
			return JSONObject.fromObject(o).toString();
		else {
			try {
				Map m = BeanUtils.describe(o);
				return JSONObject.fromObject(m).toString();
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		return "";
	}
	
	/**
	 * 產生JSON字串(以Html替代符號替換JSON字串內的單引號，以防網頁上以單引號括住的JSON字串被誤判)
	 */
	public static String toHtmlJson(Object o) {
		if(o instanceof List)
			return JSONArray.fromObject(o).toString().replaceAll("'", "&#039"); 
		else if(o instanceof Map)
			return JSONObject.fromObject(o).toString().replaceAll("'", "&#039");
		else {
			try {
				Map m = BeanUtils.describe(o);
				return JSONObject.fromObject(m).toString().replaceAll("'", "&#039");
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		return "";
	}

	public static List toList(String json) {
		return new ArrayList(Arrays.asList(JSONArray.fromObject(json).toArray()));
		
	}
	public static boolean checkjsonstring(String json) {
		try{
		    JSONObject jsonObject = JSONObject.fromObject( json ); 
		    return true;
		}catch(Exception e){
		    return false;
		}		
	}
}
