package fstop.util;

import java.io.*;
import java.util.*;


public class StrUtils {
	public static boolean isEmpty(String s) {
		return s == null || s.length() == 0;
	}
	
	public static boolean isNotEmpty(String s) {
		return !(s == null || s.length() == 0);
	}
	
	public static String trim(String s) {
		if(isEmpty(s))
			return "";
		return s.trim();
	}
	
	public static String right(String str, int len) {
		if(str.length() <= len)
			return str;
		return str.substring(str.length() - len);
	}
	
	public static String left(String str, int len) {
		if(str.length() <= len)
			return str;
		return str.substring(0, len);
	}
	
	public static String repeat(String str, int len) {
		StringBuffer sb = new StringBuffer();
		for(int i =0; i < len ; i++) {
			sb.append(str);
		}
		return sb.toString();
	}
	
	/**
	 * 若傳入的 v 為空字串或者為null, 則回傳 default value
	 * @param v
	 * @param defaultValue
	 * @return
	 */
	public static String nvl(String v, String defaultValue) {
		return (isEmpty(v) ? defaultValue : v);
	}


	
	/**
	 * 將 arys 加上 delim 變成 String
	 * @param delim
	 * @param arys
	 * @return
	 */
	public static String implode(String delim, String[] arys) {
		if(arys == null || arys.length == 0)
			return "";
		
		StringBuffer result = new StringBuffer();
		
		for(String x : arys) {
			if(result.length() != 0)
				result.append(delim);
			result.append(x);
		}
		
		return result.toString();
	}
	
	public static String implode(String delim, Set<String> arys) {
		if(arys == null || arys.size() == 0)
			return "";
		
		StringBuffer result = new StringBuffer();
		
		for(String x : arys) {
			if(result.length() != 0)
				result.append(delim);
			result.append(x);
		}
		
		return result.toString();
	}
	
	public static String implode(String delim, List<String> arys) {
		if(arys == null || arys.size() == 0)
			return "";
		
		StringBuffer result = new StringBuffer();
		
		for(String x : arys) {
			if(result.length() != 0)
				result.append(delim);
			result.append(x);
		}
		
		return result.toString();
	}
    public static String toHex(byte[] b){
        StringBuffer hex = new StringBuffer();
        for (int i=0; i<b.length; i++) {
        hex.append(""+"0123456789ABCDEF".charAt(0xf&b[i]>>4)+"0123456789ABCDEF".charAt(b[i]&0xf));
        }
        return hex.toString(); 
    }

    public static byte[] Hex2Bin(byte[] hex) {
        byte[] bin = new byte[hex.length / 2];
        for (int i = 0, j = 0; i < bin.length; i++, j += 2) {
            int iL = hex[j] - '0';
            if (iL > 9) {
                iL -= 7;
            }
            iL <<= 4;
            //
            int iR = hex[j + 1] - '0';
            if (iR > 9) {
                iR -= 7;
            }
            bin[i] = (byte) (iL | iR);
        }
        return bin;
    }
	public static byte[] hexToByte(String hexStr) {
		byte[] bts = new byte[hexStr.length() / 2];
		for (int i = 0; i < bts.length; i++) {
			bts[i] = (byte) Integer.parseInt(
					hexStr.substring(2 * i, 2 * i + 2), 16);
		}
		return bts;
	}

	/**
	 * 左補零
	 * @param org
	 * @param newLength
	 * @return
	 */
	public static String padZeroLeft(String org, int newLength)
	{
		return padOnLeft(org, (byte) 0x30, newLength);
	}

	/**
	 * 左補滿
	 * 當 newLength 的值小於 輸入字串長度時，回傳原有字串
	 * @param org  原有的字串
	 * @param pad  要補滿的字元(byte)
	 * @param newLength 長度
	 * @return 補滿的字串
	 */
	public static String padOnLeft(String org, byte pad, int newLength)
	{
		if (org.length() > newLength)
		{
			return org;
		}

		byte[] newArr = new byte[newLength];

		Arrays.fill( newArr, pad );

		byte[] orgByteArr = org.getBytes();
		System.arraycopy(orgByteArr, 0, newArr, newArr.length-orgByteArr.length, orgByteArr.length);

		return new String(newArr);
	}


	public static Set<String> splitToSet(String delim, String value) {
		return new HashSet(Arrays.asList(value.split(delim)));
	}

	public static List<String> splitToList(String delim, String value) {
		List<String> result = new ArrayList();
		String[] ary = StrUtils.trim(value).split(delim);
		if(ary == null || ary.length == 0)
			return new ArrayList();
		for(String s : ary) {
			result.add(s);
		}
		return result;
	}

}
