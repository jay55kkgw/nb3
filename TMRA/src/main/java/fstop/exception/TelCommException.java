package fstop.exception;


public class TelCommException extends UncheckedException { 
	public TelCommException(String msg) {
		super(msg);
	}
	public TelCommException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
