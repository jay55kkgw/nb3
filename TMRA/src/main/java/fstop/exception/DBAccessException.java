package fstop.exception;


public class DBAccessException extends UncheckedException { 

	private static final long serialVersionUID = 7034496227113613816L;
	
	public DBAccessException(String msg) {
		super(msg);
	}
	public DBAccessException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
