package fstop.exception;

import java.io.PrintStream;
import java.io.PrintWriter;

public abstract class UncheckedException extends RuntimeException {


	/** Root cause of this nested exception */
	private Throwable cause;
	
	/**
	 * Construct a <code>NestedRuntimeException</code> with the specified detail message.
	 * @param msg the detail message
	 */
	public UncheckedException(String msg) {
		super(msg);
	}

	/**
	 * Construct a <code>NestedRuntimeException</code> with the specified detail message
	 * and nested exception.
	 * @param msg the detail message
	 * @param cause the nested exception
	 */
	public UncheckedException(String msg, Throwable cause) {
		super(msg);
		this.cause = cause;
	}


	/**
	 * Return the nested cause, or <code>null</code> if none.
	 * <p>Note that this will only check one level of nesting.
	 * Use {@link #getRootCause()} to retrieve the innermost cause.
	 */
	public Throwable getCause() {
		// Even if you cannot set the cause of this exception other than through
		// the constructor, we check for the cause being "this" here, as the cause
		// could still be set to "this" via reflection: for example, by a remoting
		// deserializer like Hessian's.
		return (this.cause == this ? null : this.cause);
	}

	/**
	 * Return the detail message, including the message from the nested exception
	 * if there is one.
	 */
	public String getMessage() {
		return UncheckExceptionUtils.buildMessage(super.getMessage(), getCause());
	}

	/**
	 * Print the composite message and the embedded stack trace to the specified stream.
	 * @param ps the print stream
	 */
	public void printStackTrace(PrintStream ps) {
		if (getCause() == null) {
			super.printStackTrace(ps);
		}
		else {
//			ps.println(this);
			//ps.print("Caused by: ");
//			ps.print("發生在: ");
//			Information Exposure Through an Error Message
//			getCause().printStackTrace(ps);
		}
	}

	/**
	 * Print the composite message and the embedded stack trace to the specified writer.
	 * @param pw the print writer
	 */
	public void printStackTrace(PrintWriter pw) {
		if (getCause() == null) {
//			Incorrect Permission Assignment For Critical Resources
//			super.printStackTrace(pw);
		}
		else {
//			pw.println(this);
			//pw.print("Caused by: ");
//			pw.print("發生在: ");
//			getCause().printStackTrace(pw);
		}
	}


	/**
	 * Retrieve the innermost cause of this exception, if any.
	 * <p>Currently just traverses <code>NestedRuntimeException</code> causes.
	 * Will use the JDK 1.4 exception cause mechanism once Spring requires JDK 1.4.
	 * @return the innermost exception, or <code>null</code> if none
	 * @since 2.0
	 */
	public Throwable getRootCause() {
		Throwable cause = getCause();
		if (cause instanceof UncheckedException) {
			Throwable rootCause = ((UncheckedException) cause).getRootCause();
			return (rootCause != null ? rootCause : cause);
		}
		else {
			return cause;
		}
	}

	/**
	 * Retrieve the most specific cause of this exception, that is,
	 * either the innermost cause (root cause) or this exception itself.
	 * <p>Differs from {@link #getRootCause()} in that it falls back
	 * to the present exception if there is no root cause.
	 * @return the most specific cause (never <code>null</code>)
	 * @since 2.0.3
	 */
	public Throwable getMostSpecificCause() {
		Throwable rootCause = getRootCause();
		return (rootCause != null ? rootCause : this);
	}

	/**
	 * Check whether this exception contains an exception of the given type:
	 * either it is of the given class itself or it contains a nested cause
	 * of the given type.
	 * <p>Currently just traverses <code>NestedRuntimeException</code> causes.
	 * Will use the JDK 1.4 exception cause mechanism once Spring requires JDK 1.4.
	 * @param exType the exception type to look for
	 * @return whether there is a nested exception of the specified type
	 */
	public boolean contains(Class exType) {
		if (exType == null) {
			return false;
		}
		if (exType.isInstance(this)) {
			return true;
		}
		Throwable cause = getCause();
		if (cause instanceof UncheckedException) {
			return ((UncheckedException) cause).contains(exType);
		}
		else {
			return (cause != null && exType.isInstance(cause));
		}
	}

}
