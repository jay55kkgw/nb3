package fstop.exception;


public class SendMailException extends UncheckedException { 
	public SendMailException(String msg) {
		super(msg);
	}
	public SendMailException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
