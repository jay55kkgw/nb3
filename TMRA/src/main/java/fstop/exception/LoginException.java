package fstop.exception;

public class LoginException extends UncheckedException { 
	public LoginException(String msg) {
		super(msg);
	}
	public LoginException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
