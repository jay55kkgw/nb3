package fstop.exception;


public class RecordNotExistException extends DBAccessException { 

	private static final long serialVersionUID = 7034496227113613816L;
	
	public RecordNotExistException(String msg) {
		super(msg);
	}
	public RecordNotExistException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
