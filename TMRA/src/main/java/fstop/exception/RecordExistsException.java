package fstop.exception;


public class RecordExistsException extends DBAccessException { 

	private static final long serialVersionUID = 7034496227113613816L;
	
	public RecordExistsException(String msg) {
		super(msg);
	}
	public RecordExistsException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
