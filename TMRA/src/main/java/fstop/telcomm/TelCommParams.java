package fstop.telcomm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class TelCommParams extends TelcommObject {
	private String desc;
	
	// default 的設定
	private Map<String, String> defaultFieldFormatDefine = new HashMap();

	//by 不同電文的設定
	private Map<String, String> fieldFormat = new HashMap();
	
	private List<String> beforeCommand = new ArrayList<String>();
	private List<String> afterCommand = new ArrayList<String>();

	public Map<String, String> getDefaultFieldFormatDefine() {
		return defaultFieldFormatDefine;
	}

	public Map<String, String> getFieldFormat() {
		return fieldFormat;
	}

	public void setFieldFormat(Map<String, String> fieldFormat) {
		this.fieldFormat = fieldFormat;
	}

	public void setDefaultFieldFormatDefine(
			Map<String, String> defaultFieldFormatDefine) {
		this.defaultFieldFormatDefine = defaultFieldFormatDefine;
	}

	public List<String> getAfterCommand() {
		return afterCommand;
	}

	public void setAfterCommand(List<String> afterCommand) {
		this.afterCommand = afterCommand;
	}

	public List<String> getBeforeCommand() {
		return beforeCommand;
	}

	public void setBeforeCommand(List<String> beforeCommand) {
		this.beforeCommand = beforeCommand;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}	
}
