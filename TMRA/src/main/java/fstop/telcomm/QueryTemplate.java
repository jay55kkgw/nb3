package fstop.telcomm;

import java.util.Hashtable;

public interface QueryTemplate {
	public void query(final TelCommParams telcomm, final Hashtable params, final TelCommReceiver receiver);
}
