package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "ADMMSGCODE")
@Data
public class ADMMSGCODE implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3288901554939791522L;

	@Id
	private String ADMCODE;

	private String ADMRESEND = "";//可否提供台幣類交易人工重送 Y:可人工重送N:否

	private String ADMEXCE = "";   //需列入異常事件通知否

	private String ADMSGIN = ""; //訊息說明

	private String ADMSGOUT = "";//客戶訊息內容

	private String LASTUSER = "";//最後修改者

	private String LASTDATE = "";//最後修改日

	private String LASTTIME = "";//最後修改時間

	private String ADMRESENDFX = "";//可否提供外幣類交易人工重送  Y:可人工重送N:否

	private String ADMAUTOSEND = "";//可否提供台幣類交易自動重送  Y:可自動重送N:否
	
	private String ADMAUTOSENDFX = "";//可否提供外幣類交易自動重送	Y:可自動重送N:否

	@Override
	public String toString() {
		return "ADMMSGCODE [ADMCODE=" + ADMCODE + ", ADMRESEND=" + ADMRESEND + ", ADMEXCE=" + ADMEXCE + ", ADMSGIN="
				+ ADMSGIN + ", ADMSGOUT=" + ADMSGOUT + ", LASTUSER=" + LASTUSER + ", LASTDATE=" + LASTDATE
				+ ", LASTTIME=" + LASTTIME + ", ADMRESENDFX=" + ADMRESENDFX + ", ADMAUTOSEND=" + ADMAUTOSEND
				+ ", ADMAUTOSENDFX=" + ADMAUTOSENDFX + "]";
	}
	
	
}
