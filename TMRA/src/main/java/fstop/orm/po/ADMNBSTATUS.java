package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "ADMNBSTATUS")
@Data
public class ADMNBSTATUS implements Serializable {

	@Id
	private String ADNBSTATUSID;

	private String ADNBSTATUS;

	private String LASTUSER;

	private String LASTDATE;

	private String LASTTIME;

}
