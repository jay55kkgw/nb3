package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

import lombok.Data;

@Entity
@Table(name = "SYSPARAMDATA")
@Data
public class SYSPARAMDATA implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5105431519789711697L;

	@Id
	private String ADPK;

	private String ADNAME = "";

	@Column(name="ADPWD")
	private String ADPW = "";

	private String ADAPNAME = "";

	private String ADFXSHH = "";

	private String ADFXSMM = "";

	private String ADFXSSS = "";

	private String ADFXEHH = "";

	private String ADFXEMM = "";

	private String ADFXESS = "";

	private String ADFDSHH = "";

	private String ADFDSMM = "";

	private String ADFDSSS = "";

	private String ADFDEHH = "";

	private String ADFDEMM = "";

	private String ADFDESS = "";

	private String ADPORTALJNDI = "";

	private String ADNBJNDI = "";

	private String ADSESSIONTO = "";

	private String ADOPMAIL = "";

	private String ADAPMAIL = "";

	private String ADSECMAIL = "";

	private String ADSPMAIL = "";

	private String ADFDSVRIP = "";

	private String ADMAILSVRIP = "";

	private String ADPARKSVRIP = "";

	private String ADARSVRIP = "";

	private String ADFDRETRYTIMES = "";

	private String ADPKRETRYTIMES = "";

	private String ADRESENDTIMES = "";

	private String ADBHMAILACNORULE = "";

	private String LASTUSER = "";

	private String LASTDATE = "";

	private String LASTTIME = "";

	private String ADPORTALURL = "";

	private String ADPORTALURL_ADM = "";
	
	private String ADGDMAIL = "";
	
	private String ADGDTXNMAIL = "";

	private String ADGDSHH = "";

	private String ADGDSMM = "";

	private String ADGDSSS = "";

	private String ADGDEHH = "";

	private String ADGDEMM = "";

	private String ADGDESS = "";
	
	private String KYCDATE = "";
	private String SCN070 = "";
	private String SCF002 = "";
	
	private String ADFX1SHH = "";

	private String ADFX1SMM = "";

	private String ADFX1SSS = "";

	private String ADFXE1HH = "";

	private String ADFXE1MM = "";

	private String ADFXE1SS = "";

	@Override
	public String toString() {
		return "SYSPARAMDATA [ADPK=" + ADPK + ", ADNAME=" + ADNAME + ", ADPW=" + ADPW + ", ADAPNAME=" + ADAPNAME
				+ ", ADFXSHH=" + ADFXSHH + ", ADFXSMM=" + ADFXSMM + ", ADFXSSS=" + ADFXSSS + ", ADFXEHH=" + ADFXEHH
				+ ", ADFXEMM=" + ADFXEMM + ", ADFXESS=" + ADFXESS + ", ADFDSHH=" + ADFDSHH + ", ADFDSMM=" + ADFDSMM
				+ ", ADFDSSS=" + ADFDSSS + ", ADFDEHH=" + ADFDEHH + ", ADFDEMM=" + ADFDEMM + ", ADFDESS=" + ADFDESS
				+ ", ADPORTALJNDI=" + ADPORTALJNDI + ", ADNBJNDI=" + ADNBJNDI + ", ADSESSIONTO=" + ADSESSIONTO
				+ ", ADOPMAIL=" + ADOPMAIL + ", ADAPMAIL=" + ADAPMAIL + ", ADSECMAIL=" + ADSECMAIL + ", ADSPMAIL="
				+ ADSPMAIL + ", ADFDSVRIP=" + ADFDSVRIP + ", ADMAILSVRIP=" + ADMAILSVRIP + ", ADPARKSVRIP="
				+ ADPARKSVRIP + ", ADARSVRIP=" + ADARSVRIP + ", ADFDRETRYTIMES=" + ADFDRETRYTIMES + ", ADPKRETRYTIMES="
				+ ADPKRETRYTIMES + ", ADRESENDTIMES=" + ADRESENDTIMES + ", ADBHMAILACNORULE=" + ADBHMAILACNORULE
				+ ", LASTUSER=" + LASTUSER + ", LASTDATE=" + LASTDATE + ", LASTTIME=" + LASTTIME + ", ADPORTALURL="
				+ ADPORTALURL + ", ADPORTALURL_ADM=" + ADPORTALURL_ADM + ", ADGDMAIL=" + ADGDMAIL + ", ADGDTXNMAIL="
				+ ADGDTXNMAIL + ", ADGDSHH=" + ADGDSHH + ", ADGDSMM=" + ADGDSMM + ", ADGDSSS=" + ADGDSSS + ", ADGDEHH="
				+ ADGDEHH + ", ADGDEMM=" + ADGDEMM + ", ADGDESS=" + ADGDESS + ", KYCDATE=" + KYCDATE + ", SCN070="
				+ SCN070 + ", SCF002=" + SCF002 + ", ADFX1SHH=" + ADFX1SHH + ", ADFX1SMM=" + ADFX1SMM + ", ADFX1SSS="
				+ ADFX1SSS + ", ADFXE1HH=" + ADFXE1HH + ", ADFXE1MM=" + ADFXE1MM + ", ADFXE1SS=" + ADFXE1SS + "]";
	}

	

}
