package fstop.orm.dao;

import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.LegacyJpaRepository;

import fstop.orm.po.ADMMSGCODE;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
public class AdmMsgCodeDao extends LegacyJpaRepository<ADMMSGCODE, String> {

	@PersistenceContext(unitName="default")
	private EntityManager em;

    @Transactional(readOnly = true)
    public String getErrorCodeMsg(String err_code) {
        List qresult = find("SELECT ADMSGOUT FROM ADMMSGCODE WHERE ADMCODE=?", err_code);

        String result = "";
        if(qresult.size() > 0) {
            result = (String)qresult.iterator().next();
        }
        return result;
    }

    @Transactional(readOnly = true)
    public List<ADMMSGCODE> findMsgCodeLike(String msgCode) {
        List<ADMMSGCODE> qresult = find("FROM ADMMSGCODE WHERE ADMCODE like ? ", "%" + msgCode+ "%");

        return qresult;
    }

    @Transactional
    public ADMMSGCODE isError(String topmsg) {
        try {
        	// 交易類錯誤代碼欄位亦可為帳號，若資料庫找不到錯誤代碼原邏輯會視為成功，但不一定是成功，也有可能單純只是資料庫沒有對應的錯誤代碼，故另外加上長度及不為成功代碼的判斷
        	String[] successCodes = {"R000","OKLR","OKOK","","0000000","0000","OKOV"};
        	
            Query q =  getQuery("FROM ADMMSGCODE WHERE ADMCODE = '" +topmsg+ "'");

            List qresult = q.getResultList();
            ADMMSGCODE result = null;
            if(qresult.size() > 0) {
                result = (ADMMSGCODE)qresult.get(0);
            } else if (topmsg != null && topmsg.length()<=6 && !Arrays.stream(successCodes).anyMatch(topmsg::equals)) {
            	result = new ADMMSGCODE();
            	result.setADMCODE(topmsg);
            	result.setADMSGIN("");
            	result.setADMSGOUT("");
            }
            
            return result;
        }
        catch(Exception e){
            log.error("取得 ADMMSGCODE 有誤 .", e);
        }
        return null;
    }
    
    
    @Transactional
    public Boolean hasError(String topmsg) {
    	Boolean ret =Boolean.FALSE;
    	try {
    		// 交易類錯誤代碼欄位亦可為帳號，若資料庫找不到錯誤代碼原邏輯會視為成功，但不一定是成功，也有可能單純只是資料庫沒有對應的錯誤代碼，故另外加上長度及不為成功代碼的判斷
    		String[] successCodes = {"R000","OKLR","OKOK","","0000000","0000","OKOV"};
    		
    		Query q =  getQuery("FROM ADMMSGCODE WHERE ADMCODE = '" +topmsg+ "'");
    		
    		List qresult = q.getResultList();
    		ADMMSGCODE result = null;
    		if(qresult.size() > 0) {
    			result = (ADMMSGCODE)qresult.get(0);
    		} else if (topmsg != null && topmsg.length()<=6 && !Arrays.stream(successCodes).anyMatch(topmsg::equals)) {
    			result = new ADMMSGCODE();
    			result.setADMCODE(topmsg);
    			result.setADMSGIN("");
    			result.setADMSGOUT("");
    		}
    		
    		if(result!=null) {
    			ret = Boolean.TRUE;
    		}
    		
    		return ret;
    	}
    	catch(Exception e){
    		log.error("取得 ADMMSGCODE 有誤 .", e);
    	}
    	return null;
    }

    @Transactional(readOnly = true)
    public List<ADMMSGCODE> findAll() {
        List<ADMMSGCODE> qresult = find("FROM ADMMSGCODE ORDER BY ADMCODE ");

        return qresult;
    }

    @Transactional(readOnly = true)
    public String findRESEND(String admcode) {
        try {
            Query query = getSession().createQuery("SELECT ADMRESEND FROM ADMMSGCODE WHERE ADMCODE = ? ");
            query.setParameter(0, admcode);
            List result = query.getResultList();
            if(result.size() == 0)
                return "";

            return result.get(0).toString();
        }
        catch(Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    @Transactional(readOnly = true)
    public String findAUTORESEND(String admcode) {
        try {
            Query query = getSession().createQuery("SELECT ADMAUTOSEND FROM ADMMSGCODE WHERE ADMCODE = ? ");
            query.setParameter(0, admcode);
            List result = query.getResultList();
            if(result.size() == 0)
                return "";

            return result.get(0).toString();
        }
        catch(Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    @Transactional(readOnly = true)
    public String findFXRESEND(String admcode) {
        try {
            Query query = getSession().createQuery("SELECT ADMRESENDFX FROM ADMMSGCODE WHERE ADMCODE = ? ");
            query.setParameter(0, admcode);
            List result = query.getResultList();
            if(result.size() == 0)
                return "";

            return result.get(0).toString();
        }
        catch(Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    @Transactional(readOnly = true)
    public String findFXAUTORESEND(String admcode) {
        try {
            Query query = getSession().createQuery("SELECT ADMAUTOSENDFX FROM ADMMSGCODE WHERE ADMCODE = ? ");
            query.setParameter(0, admcode);
            List result = query.getResultList();
            if(result.size() == 0)
                return "";

            return result.get(0).toString();
        }
        catch(Exception e) {
            e.printStackTrace();
        }

        return "";
    }
}
