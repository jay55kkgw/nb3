package fstop.orm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.util.DateUtil;

import fstop.orm.po.ADMNBSTATUS;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
public class AdmNbStatusDao extends LegacyJpaRepository<ADMNBSTATUS, String> {

    @Transactional(readOnly = true)
    public void setServerStatus(String severName) {
    	try {
    		if(this.findById(severName) != null) {
    			ADMNBSTATUS data = findById(severName);
    			data.setLASTDATE(DateUtil.getDate(""));
    			data.setLASTTIME(DateUtil.getTheTime(""));
    			this.save(data);
    		}else {
    			ADMNBSTATUS data = new ADMNBSTATUS();
    			data.setADNBSTATUSID(severName);
    			data.setADNBSTATUS("Y");
    			data.setLASTUSER("SYS");
    			data.setLASTDATE(DateUtil.getDate(""));
    			data.setLASTTIME(DateUtil.getTheTime(""));
    			this.save(data);
    		}
    	}catch(Exception e) {
            log.error("取得 ADMNBSTATUS 有誤 ." + e.getMessage());
    	}
    }
}
