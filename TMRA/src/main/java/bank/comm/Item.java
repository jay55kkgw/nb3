package bank.comm;

import java.util.*;
import java.net.*;

/**
  */

public interface Item
{
	// @return必須為String才有彈性
	public String getValue(TelegramDataList template, String messageType,Hashtable result, Hashtable preMessage);
	public boolean hasValue();
    public String getMessageType();
}