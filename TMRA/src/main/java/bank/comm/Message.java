package bank.comm;
import lombok.extern.slf4j.Slf4j;
/**
 *Message類別會組成一與SNA server溝通的byte array,包括長度,指令與訊息<br>
 *如同一header,該byte array的內容如下 : <br>
 *第1~4 byte -- 紀錄自第5個byte開始起算之byte array的長度
 *第5~8 byte -- 通知SNA server目前這個byte array要進行的動作,包括 1.conn : 連接SNA server  2.send : 傳送交易資料上行電文<br>
 *3.recv : 接收中心回傳電文 4.disc : 結束與SNA server的連線
*第9 byte -- SNA server執行結果
* @author 林季穎
 * @version 1.0
 */
@Slf4j
public class Message {
    /**
    *預留的byte array總長度
     */
    int length;
    /**
     *預留的上行電文(下行電文)byte array總長度
     */
    int dataLength;
    /**
    *指令,包括1.conn,2.send,3.recv,4.disc
     */
    String command=null;
    /**
     *SNA server執行結果
     */
    String code=null;
    /**
     *上行電文(下行電文)的byte array
     */
    byte[] data;
    /**
     *EAI平台程式紀錄器
     */
//    static Logger logger = Logger.getLogger(Message.class.getName());
    /** 
        *建立一Message物件
        */
    public Message() {
        length=dataLength=16100;
        command="    ";
        code=" ";
        data=new byte[16000];
    }
    /**
     *根據一已具有header的byte array建立一Message物件
     *@param myData 已具有header的byte array
     */
    public Message(byte[]myData)
    {
        try{
//        	MyDebug.debprt("myData length=" + myData.length);
//        	MyDebug.debprt("Message new String( = " + new String(myData,0,4) + "0,4)");
        	String SLen=new String(myData,0,4);
//        	MyDebug.debprt("Message SLen = " + SLen);
            length=Integer.parseInt(SLen); //0~3 bytes:長度
//            MyDebug.debprt("tota.length="+length);
            command=new String(myData,4,4); //4~8 bytes:指令
//            MyDebug.debprt("tota.command="+command);
            code=new String(myData,8,1);    //9 bytes:結果 
//            MyDebug.debprt("tota.code="+code);
            dataLength=length-5;
//            MyDebug.debprt("Message dataLength="+dataLength);
            data=new byte[dataLength];
            System.arraycopy(myData,9,data,0,dataLength);
//			System.out.println("Message data="+new String(data));
        }catch(NumberFormatException e)
        {
//        	MyDebug.errprt("Message error NumberFormatException --> " + e.getStackTrace());
        	code="X";
        }
    }
    /**
     *根據輸入之長度,指令,訊息及一不具有header的byte array建立一Message物件
     *@param length 長度
     *@param command 指令
     *@param code 訊息
     *@param input 不具有header的byte array
     */
    public Message(int length,String command,String code,byte[] input){
        this.length=length;
        this.command=command;
        this.code=code;
        this.data=input;
        if (input==null)
            dataLength=0;
        else 
            dataLength=length-5;
/*
        if (input==null)
            dataLength=0;
        else 
            dataLength=input.length;
*/
    }
    /**
     *回傳上行電文(下行電文)之byte array
     *@return 上行電文(下行電文)之byte array
     */
    public byte[] getData(){
        return data;
    }
    /**
     *回傳帶有header之byte array的總長度
     *@return 帶有header之byte array的總長度
     */
    public int getLength(){
        return length;
    }
    /**
     *回傳SNA server執行結果
     *@return SNA server執行結果
     */
    public String getCode(){
        return code;
    }
    /**
     *回傳上行電文(下行電文)之byte array的長度
     *@return 上行電文(下行電文)之byte array的長度
     */
    public int getDataLength(){
        return dataLength;
    }
    /**
     *以字串形式回傳上行電文(下行電文)之byte array的長度
     *@return 上行電文(下行電文)之byte array的長度(字串形式)
     */
    public String getDataLengthString(){
        return makeSNASize(dataLength);
    }
    /**
     *回傳送至SNA server的指令
     *@return 送至SNA server的指令
     */
    public String getCommand(){
        return command;
    }
    /**
     *將header及上行電文組合之後轉成一byte array後回傳
     *@return 帶有header之byte array
     */
    public byte[] getBytes()
    {
        byte[] result=new byte[length+4];
        try{
            byte[] temp= (makeSNASize(length)+command+code).getBytes("ISO8859-1");
//            MyDebug.debprt("temp.length="+temp.length+",temp="+new String(temp));
//            MyDebug.debprt("data.length="+(length-4-1));
            if (dataLength==0) 
                result=temp;
            else
            {
//				System.out.println("result.length="+result.length+",length="+length+",temp.length="+temp.length+",data.length="+(length));
				System.arraycopy(temp,0,result,0,temp.length);
//				System.out.println("result.length=" + result.length + "result=" + new String(result));
				System.arraycopy(data,0,result,temp.length,(length-4-1));
//				System.out.println("result.length=" + result.length + "result=" + new String(result));
                //System.arraycopy(data,0,result,temp.length,data.length);
            }
        }
        catch(Exception e)
        {
//        	MyDebug.errprt("Message class getBytes err:"+e.getStackTrace());
        	log.error("Message class getBytes err:{}",e.toString());
        }
        return result;
    }
     /**
     *將header及上行電文組合之後轉成一字串後回傳
     *@return 帶有header之字串
     */
    public String toString(){
        String result=null;
        String temp= (makeSNASize(length)+command+code);
        if (dataLength==0)
        	result=temp;
        else
        {
        	try
        	{
        		result=temp+new String(data);
        	}
        	catch(Exception e)
        	{
//        		MyDebug.errprt("Message class toString error :" + e.getStackTrace());
        		log.error("Message class toString error :{}",e.toString());
        	}
        }
        return result.trim();
    }
    /**
     *根據輸入的數字右靠左補0至header中長度欄位之要求
     *@param size header中長度欄位需填入之值
     *@return 符合header中長度欄位要求之字串
     */
    private String makeSNASize(int size){
        String sizeInString=Integer.toString(size);
        size=sizeInString.length();
        switch(size){
            case 0: sizeInString="0000";
            break;
            case 1: sizeInString="000"+sizeInString;
            break;
            case 2: sizeInString="00"+sizeInString;
            break;
            case 3: sizeInString="0"+sizeInString;
            break;
            case 4: sizeInString=sizeInString;
            break;
            default:
//            	MyDebug.errprt("Message class in makeSNASize error ");
        }
        return sizeInString;
    }
    
}
