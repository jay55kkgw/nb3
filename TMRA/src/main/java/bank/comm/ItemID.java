package bank.comm;

import java.util.*;
import java.net.*;
import org.apache.log4j.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
/**
  */
public class ItemID implements Item
{
	String name;
	String messageType=null;
	
	//public ItemID( Node flow, String type )
	public ItemID( Node node )	//設此node為<ITEMID>
	{
		getNodeValue(node);
	}
	
	public String getValue(TelegramDataList template, String messageType , 
							Hashtable result, Hashtable preMessage)
	{
		return name;
	}
	
	public String getName()
	{
		return name;
	}

    public boolean hasValue()
    {
        if(name==null)
            return false;
        return true;
    }
    
	public String toString()
	{
		return name;
	}
		
    public String getMessageType()
    {
        return messageType;
    }

	private void getNodeValue(Node node)
	{
	    Node n = node.getFirstChild();
        if(n!=null)
            name = n.getNodeValue();
		else
            name = null;
	}
}