package bank.comm;
/**
 * 每個TelegramData物件將Telegram.xml中一個'ITEM' tag的屬性及預設值轉化為類別成員,
 * TelemMessageMapper物件會根據這些資料,配合TelegramDataList物件及畫面傳入之資料,
 * 組成上行電文或處理下行電文
 * @author 林季穎
 * @version 1.0
 * @version 1.5 李國樞 2006.10.10 
 */
public class TelegramData		// implements TelegramItem
{
    /**
        * 每個TelegramData物件所代表的欄位在上(下)行電文中的起始位置
        */
    public int start=0;
    /**
        * 每個TelegramData物件所代表的欄位在上(下)行電文中的長度
        */
    public int length=0;
    /**
        * 每個TelegramData物件所代表的欄位之中文說明
        */
    public String desc=null;
    /**
        * 每個TelegramData物件所代表的欄位在上(下)行電文中的型態
        */
    public String type=null;
    /**
        * 每個TelegramData物件所代表的欄位名稱
        */
    public String name=null;
    /**
        * 每個TelegramData物件所代表的欄位在上(下)行電文中所需之值
        */
    public String value=null;
     /**
        * 每個TelegramData物件所代表的欄位在上(下)行電文中是否為必要欄位
        * 或 此欄位是否存在是看其他欄位的值來決定的.optional="該欄位名稱"  (06.10.10 for 一銀TM)
        */
    public String optional=null;
    /**
        * 每個TelegramData物件所代表的欄位在上(下)行電文中是否需要參照其他欄位
        * 或 上述optional欄位的值==refval時,該欄位存在  (06.10.10 for 一銀TM)
        */
    public String refval=null;
     /**
        * 每個TelegramData物件所代表的欄位在上(下)行電文中所要遵照的格式
        */
    public String format=null;
    /**
        *將此欄位的值視為一資料區段,由specific procedure處理.而該視此欄位為何種資料區段,會由電文的某個欄位值來決定.
        *datasemantic屬性定義此欄位.   (06.10.10 for 一銀TM)
        */
    public String datasemantic=null;
    /**
        * 資料區段種類for一銀端末: [純資料] | [row,column,data], 由此屬性來定義   (06.10.10 for 一銀TM)
        */
    public String datasemanticval=null;
    /**
        * 每個TelegramData物件所代表的欄位在上(下)行電文中的編碼方式
        */
    public String encoding=null;
    /**
        * 每個TelegramData物件所代表的欄位在上(下)行電文中是否需要轉換編碼方式
        */
    public String transfer=null;
    /**
        * 建立一TelegramData物件並指定其起始位置與長度
        * @param start 起始位置
        * @param length 長度
        */
    public TelegramData(int start, int length) {
        this.start=start;
        this.length=length;
    }
    /**
        * 建立一TelegramData物件並指定其所有屬性
        * @param name 欄位名稱
        * @param type 型態
        * @param value 欄位值
        * @param start 起始位置
        * @param length 長度
        * @param optional 必要欄位
        * @param refval 參照欄位
        * @param format 格式
        * @param encoding 編碼方式
         * @param transfer 轉換編碼方式
        */
    public TelegramData( String name,String type, String value,int start, int length, String optional, String refval, 
            String format, String encoding, String transfer , String desc, String datasemantic, String datasemanticval) 
    {
    	this.desc=desc;
        this.name=name;
        this.value=value;
        this.type=type;
        this.start=start;
        this.length=length;
        this.optional=optional;
        this.refval=refval;
        this.format=format;
        this.encoding=encoding;
        this.transfer=transfer;
        this.datasemantic = datasemantic;
        this.datasemanticval = datasemanticval;
    }
    /**
        * 傳回TelegramData物件所代表的欄位在上(下)行電文中的起始位置
        * @return 起始位置
        */
    public int getPosition(){ return start; }
    /**
        * 傳回TelegramData物件所代表的欄位在上(下)行電文中的起始位置
        * @return 起始位置
        */
    public int getStart(){ return start;}
    /**
        * 傳回TelegramData物件所代表的欄位在上(下)行電文中的長度
        * @return 欄位長度
        */
    public int getLength(){ return length; }
    /**
        * 傳回TelegramData物件所代表的欄位在上(下)行電文中的型態
        * @return 型態(9;右靠左補零,X:左靠右補空白,H:hex value)
        */
    public String getType(){ return type; }
    /**
        * 傳回TelegramData物件所代表的欄位在上(下)行電文中的欄位名稱
        * @return 欄位名稱
        */
    public String getName(){ return name; }
    /**
        * 傳回TelegramData物件所代表的欄位在上(下)行電文中的欄位值
        * @return 欄位值
        */
    public String getValue(){ return value; }
    /**
        * 傳回TelegramData物件所代表的欄位在上(下)行電文中的是否必要欄位之值
        * @return YES表示該欄位為非必要欄位,NO或空字串表示該欄位為必要欄位
        */
    public String getOptional(){ return optional; }
    /**
        * 傳回TelegramData物件所代表的欄位在上(下)行電文中的參照欄位名稱
        * @return 參照欄位名稱
        */
    public String getRefVal(){ return refval; }
    /**
        * 傳回TelegramData物件所代表的欄位在上(下)行電文中的格式
        * @return 格式
        */
    public String getFormat(){ return format; }
    /**
        * 傳回TelegramData物件所代表的欄位在上(下)行電文中的編碼方式
        * @return 編碼方式
        */
    public String getEncoding(){ return encoding; }
    /**
        * 傳回TelegramData物件所代表的欄位在上(下)行電文中是否轉換編碼方式之值
        * @return YES表示需將該欄位轉為指定之編碼方式,NO表示不需轉換
        */
    public String getTransfer(){ return transfer; }
    
    public String getDataSemantic(){ return datasemantic;  }
    
    public String getDataSemanticVal(){ return datasemanticval;  }
    
    /**
        * 將TelegramData物件所代表的欄位的名稱,型態,欄位值,起始位置與長度以java.lang.String物件傳回
        * @return TelegramData物件所代表的欄位的名稱,型態,欄位值,起始位置與長度
        */
    public String toString() { return "Name: "+name+" Type: "+type+" Value: "+value+" Start: "+start+" Length: "+length;}
    
}
