package bank.comm;

import java.net.InetAddress;
import java.util.*;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Required;

import com.netbank.util.ESAPIUtil;

import lombok.extern.slf4j.Slf4j;

//import fstop.notifier.NotifyAngent;
//import fstop.orm.dao.SysLogDao;
//import fstop.orm.dao.SysParamDataDao;
//import fstop.orm.po.SYSPARAMDATA;
//import fstop.util.DateTimeUtils;
//import fstop.util.SpringBeanFactory;
@Slf4j
public class MsgMap
{
//	private SysParamDataDao sysParamDataDao;
//
//	@Required
//	public void setSysParamDataDao(SysParamDataDao sysParamDataDao) {
//		this.sysParamDataDao = sysParamDataDao;
//	}

	static private String[][] ErrMap={	
			{"0","OKOK","OK"},
			{".","Z001","Load setup.properties fail"},
			{".","Z002","從快取記憶體載入電文失敗"},
//			{".","Z003","發送電文失敗"},
			{".","Z004","接收電文失敗"},
			{".","Z005","折返時發送電文失敗"},
			{".","Z006","CommunicationException fail"},
			{".","Z007","載入電文XML檔失敗"},
			{".","Z008","CommunicationException other fail"},
			{".","Z009","DP/PARK/FUND Transaction Exception"},
			{".","Z010","欄位FORMAT錯誤"},
			{".","Z011","組上行電文發生例外請查看tmar.log"},
			{".","Z012","輸入資料最大長度大餘欄位最大長度"},
			{".","Z013","欄位名稱未定義"},
			{".","Z020","getConnection Exception"},
			
			{"1","Z021","與主機連線中斷失敗"},
			{"2","Z014","RET_RECEIVE_TIMEOUT for host"},
			{"3","Z022","cmd 無效(除CONN,DISC,SEND,RECV,其餘無效)"},
			{"4","Z023","取得LU超過等候時間"},
			{"5","Z024","找不到應用程式"},
			{"6","Z025","所有LU已離線"},
			{"7","Z026","找不到LU可使用"},
			{"8","Z003","發送電文失敗"},
			{"9","Z027","MSG SEND 失敗"},
			{"A","Z028","MSG RECV 失敗"},
			{"B","Z029","APPC_TP_STARTED 失敗"},
			{"C","Z030","APPC_ALLOCATE 失敗"},
			{"D","Z031","APPC_SEND_DATA 失敗"},
			{"E","Z032","APPC_RECEIVE_AND_WAIT 失敗"},
			{"F","Z035","Login 中心失敗"},
			{"G","Z035","Login 中心失敗"},
			{"X","Z033","Web 至 APSERVICE 溝通異常"},
			{"*","Z015","RET_RECEIVE_TIMEOUT for local"},
			{"+","Z016","電文長度異常，請查看tmra.log"},
			{"-","Z017","InterruptedIOException，請查看tmra.log"},
			{"@","Z018","TMRA Exception，請查看tmra.log"},
			{"/","Z019","應用程式未執行(apservice)"},
			{"~","Z034","ABNORMAL TERMINATED"},
			{"","",""}};
	
    static public int geterrmsg(String jsnaerrcod, Hashtable htl_Data)
    {

    	for(int i=0;i<ErrMap.length;i++)
    	{
    		if(jsnaerrcod.equals(ErrMap[i][0]))
    		{
    			htl_Data.put("clierrcod", new String(ErrMap[i][1]));
    			htl_Data.put("clierrtext", new String(ErrMap[i][2]));

    			if(ErrMap[i][1].equals("Z021") || ErrMap[i][1].equals("Z023") || ErrMap[i][1].equals("Z025") ||
        				ErrMap[i][1].equals("Z032") || ErrMap[i][1].equals("Z034"))
//    				TODO 先註解 SendMail DAO 還沒好
//        				SendMail(new String(ErrMap[i][1]) + " " + new String(ErrMap[i][2]));
    			break;
    		}
    	}
    	return(0);
    }
    
    static public int geterrmsg(Hashtable htl_Data,String errcod)
    {

    	for(int i=0;i<ErrMap.length;i++)
    	{
    		if(errcod.equals(ErrMap[i][1]))
    		{
    			htl_Data.put("clierrcod", new String(ErrMap[i][1]));
    			htl_Data.put("clierrtext", new String(ErrMap[i][2]));

    			if(ErrMap[i][1].equals("Z021") || ErrMap[i][1].equals("Z023") || ErrMap[i][1].equals("Z025") ||
    				ErrMap[i][1].equals("Z032") || ErrMap[i][1].equals("Z034"))
//    				TODO 先註解 SendMail 的DAO 還沒好
//    				SendMail(new String(ErrMap[i][1]) + " " + new String(ErrMap[i][2]));
    			break;
    		}
    	}
    	return(0);
    }
    
    
    static public int geterrmsg(String errcod ,List list)
    {
    	Map htl_Data = new HashMap();
    	for(int i=0;i<ErrMap.length;i++)
    	{
    		if(errcod.equals(ErrMap[i][1]))
    		{
    			htl_Data.put("TOPMSG", new String(ErrMap[i][1]));
    			htl_Data.put("TOPMSGTEXT", new String(ErrMap[i][2]));

    			if(ErrMap[i][1].equals("Z021") || ErrMap[i][1].equals("Z023") || ErrMap[i][1].equals("Z025") ||
    				ErrMap[i][1].equals("Z032") || ErrMap[i][1].equals("Z034"))
//    				TODO 先註解 SendMail 的DAO 還沒好
//    				SendMail(new String(ErrMap[i][1]) + " " + new String(ErrMap[i][2]));
    			break;
    		}
    	}
    	list.add(htl_Data);
    	return(0);
    }
    
    
//    
//    static int SendMail(String errorcode)
//	{
//		SYSPARAMDATA po = null;
//		String emailap=new String();
//		String emailsp=new String();
//		Date d = new Date();
//		String today = DateTimeUtils.getDateShort(d);
//		String time = DateTimeUtils.getTimeShort(d);
//
//		SysParamDataDao sysParamDataDao = (SysParamDataDao)SpringBeanFactory.getBean("sysParamDataDao");
//		po = sysParamDataDao.get("NBSYS");
//		if(po==null)
//		{
//			System.out.println("查無AP , SP 人員");
//			return(1);
//		}
//
//		emailap=po.getADAPMAIL().replace(";", ",") + ",";
//		emailsp=po.getADSPMAIL().replace(";", ",");
//		emailap += emailsp;
//		System.out.println("emailap = " + emailap);
//		Hashtable<String, String> varmail = new Hashtable();
//		varmail.put("MACHNAME", GetHostName());
//		varmail.put("SYSDATE", today.substring(0, 4) + "/" + today.substring(4, 6) + "/" + today.substring(6, 8));
//		varmail.put("SYSTIME", time.substring(0, 2) + ":" + time.substring(2, 4) + ":" + time.substring(4, 6));
//		varmail.put("SYSSTATUS", errorcode);
//
//		boolean isSendSuccess = NotifyAngent.sendNotice("COMM", varmail, emailap,"batch");
//		if(!isSendSuccess) {
//			System.out.println("發送 Email 失敗.(" + emailap + ")");
//			return(2);
//		}
//		System.out.println("發送 Email 成功.(" + emailap + ")");
//		return(0);
//	}

	static String GetHostName()
	{
		String hostname=null;
		try
		{
			InetAddress addr = InetAddress.getLocalHost();
			hostname= addr.getHostName();
		}
		catch(Exception e)
		{
			log.error("GetHostName error : {}",e.toString());
			hostname="";
		}
		log.debug("HostName = {}" , ESAPIUtil.vaildLog(hostname));
		return(hostname);
	}

}
