package bank.comm;

import org.apache.log4j.Logger;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import java.util.*;

public class XMLDataTITA
{
//	static Logger logger = Logger.getLogger(XMLDataTITA.class.getName());
    Document xmldoc = null;
    Hashtable master = null;
    Vector detail = null;
    Node rootnode = null;

    public XMLDataTITA()
    {
        
    }
    
    public void setDocument(Document doc)
    {
        xmldoc = doc;
    }
    
    public Document getDocument()
    {
        return xmldoc;
    }
    
    public void setRootNode(Node node)
    {
        rootnode = node;
    }
    
    public Node getRootNode()
    {
        return rootnode;
    }

    public void setCurrentMasterDetail(Hashtable h, Vector v)
    {
        master = h;
        detail = v;
    }
    
    public Hashtable getCurrentMaster()
    {
        if(master != null)
            return master;
        else
            return (Hashtable)detail.get(0);
    }
    
    public void setCurrentMaster(Hashtable h)
    {
        if(detail!=null)
        {
            master = h;
            //detail.add(h);
        }
        else
        	MyDebug.errprt("必須先setCurrentDetail");
    }
    
    public void setCurrentDetail(Vector v, String fieldname)
    {
        if(fieldname==null)
        	MyDebug.errprt("setCurrentDetail的fieldname不得為null");
        else if(master!=null)   //detail vector
        {
            master.put(fieldname, v);
            detail = v;
        }
        else
        	MyDebug.errprt("必須先setCurrentMaster");
    }
    
    public void setCurrentDetail(Vector v)
    {
        detail = v;
    }
    
    public Vector getCurrentDetail()
    {
        return detail;
    }
    
    public void parseMessage(TelegramDataList template, Node node, Vector msgField, int xmlTreeLevel)
    {
        String nodename = node.getNodeName();   
        
        for(int i=0; i<msgField.size(); i++)
        {
            String field = (String)msgField.get(i);
            
            if(master==null)
            {
            	MyDebug.errprt("Master Hashtable has not been created,nodename="+nodename);
                break;
            }
            else if(nodename.equals(field))    
            {
                //System.out.print(field+",");
                fetchMasterResult(node,field);
            }
        }//for
        //System.out.println("parseMessage");
    }//parseMessage
    
    //we use 'sheet' to represent one master-key of a master-detail query result
    private Hashtable createNewSheet(Vector result)
    {
        Hashtable h = new Hashtable();
        result.add(h);      //第result.size()張master-detail sheet
        return h;
    }
    
    private void fetchMasterResult(Node node, String field)
    {
        String fieldvalue=getXMLNodeValue(node,field);
        master.put(field, fieldvalue); 
    }
    
    private String getXMLNodeValue(Node node, String field)
    {
        String fieldvalue ="";
        Node firstchild = node.getFirstChild();
        if(firstchild!=null)
            fieldvalue = firstchild.getNodeValue();
        //if(fieldvalue.equals(""))
            //System.out.println("field("+field+") is null in input XML stream");
        return fieldvalue;
    }
}
