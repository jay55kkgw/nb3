package bank.comm;


public class StringUtil {
	public static boolean isEmpty(String s) {
		return s == null || s.length() == 0;
	}
	
	public static boolean isNotEmpty(String s) {
		return !(s == null || s.length() == 0);
	}
	
	public static String trim(String s) {
		if(isEmpty(s))
			return "";
		return s.trim();
	}
	
	public static String right(String str, int len) {
		if(str.length() <= len)
			return str;
		return str.substring(str.length() - len);
	}
	
	public static String left(String str, int len) {
		if(str.length() <= len)
			return str;
		return str.substring(0, len);
	}
	
	public static String repeat(String r, int length) {
		StringBuffer result = new StringBuffer(length);
		for(int i = 0; i < length; i++) {
			result.append(r);
		}
		return result.toString();
	}
}
