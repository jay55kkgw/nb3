package bank.comm;

import java.util.*;
import java.net.*;
import org.apache.log4j.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
/**
 * XML CONDITION tag
 * @author 李國樞
 * @create 2005/07/19
 * @version 2.0
 */
public class Condition 
{
	private Item LValue = null,RValue=null;
	private String op = null, unary_op = null;

	public Condition(Item l , String s , Item r, String unary)
	{
		LValue = l;
		op = s;
		RValue = r;
		unary_op = unary;
	}
	
	public String toString()
	{
		return "\n{Condition:[ " +LValue +" ] "+op+ " [ "+RValue+" ]}\n";
	}

	public void addLValue(Item i)
	{
		LValue = i;
	}

	public void addRValue(Item i)
	{
		RValue = i;
	}
	
	public void addComparator(String s)
	{
		op = s;
	}
	
	public void getConditionValue(Hashtable conditionTable)
	{
	    if(op.equalsIgnoreCase("Equal"))
	    {
	        String lvalue=null;
	        if(LValue.getMessageType().equals("TITA"))
	            lvalue = "TITA."+LValue.toString();
	        else
	            lvalue = LValue.toString();
	        conditionTable.put(lvalue , RValue.toString());
	    }   
	}
	
	//還沒開始寫  8/1
	public boolean calculate(TelegramDataList template, String messageType, 
										Hashtable result, Hashtable preMessage)
	{	//如果不是integer呢?如果是9(12)V2或9(11)V3或帶小數點...各種不同format?
/*		if(op.equalsIgnoreCase("Equal"))
			return Integer.parseInt(LValue.getValue(template,messageType,result,preMessage) ) 
					== Integer.parseInt(RValue.getValue(template,messageType,result,preMessage));
		else if(op.equalsIgnoreCase("GraterEqual"))
			return Integer.parseInt(LValue.getValue(template,messageType,result,preMessage) ) 
					>= Integer.parseInt(RValue.getValue(template,messageType,result,preMessage));
		else if(op.equalsIgnoreCase("LessEqual"))
			return Integer.parseInt(LValue.getValue(template,messageType,result,preMessage) ) 
					<= Integer.parseInt(RValue.getValue(template,messageType,result,preMessage));
		else if(op.equalsIgnoreCase("Greater"))
			return Integer.parseInt(LValue.getValue(template,messageType,result,preMessage) ) 
					> Integer.parseInt(RValue.getValue(template,messageType,result,preMessage));
		else if(op.equalsIgnoreCase("Less"))
			return Integer.parseInt(LValue.getValue(template,messageType,result,preMessage) ) 
					< Integer.parseInt(RValue.getValue(template,messageType,result,preMessage));
		else
			return false;*/
			
		//現在的做法是,如果判斷大於小於的,一定是數字.故是否相等的判斷,用字串比對即可  8/8
		boolean r =false;
		if(op.equalsIgnoreCase("Equal"))
			r = LValue.getValue(template,messageType,result,preMessage).equals(
				   RValue.getValue(template,messageType,result,preMessage));
		else if(op.equalsIgnoreCase("GraterEqual"))
			r = Integer.parseInt(LValue.getValue(template,messageType,result,preMessage) ) 
					>= Integer.parseInt(RValue.getValue(template,messageType,result,preMessage));
		else if(op.equalsIgnoreCase("LessEqual"))
			r = Integer.parseInt(LValue.getValue(template,messageType,result,preMessage) ) 
					<= Integer.parseInt(RValue.getValue(template,messageType,result,preMessage));
		else if(op.equalsIgnoreCase("Greater"))
			r = Integer.parseInt(LValue.getValue(template,messageType,result,preMessage) ) 
					> Integer.parseInt(RValue.getValue(template,messageType,result,preMessage));
		else if(op.equalsIgnoreCase("Less"))
			r = Integer.parseInt(LValue.getValue(template,messageType,result,preMessage) ) 
					< Integer.parseInt(RValue.getValue(template,messageType,result,preMessage));
		else if(op.equalsIgnoreCase("NotEqual"))
			r = !LValue.getValue(template,messageType,result,preMessage).equals(
				   RValue.getValue(template,messageType,result,preMessage));
		
		if(unary_op!=null)
		{
			if(unary_op.equals("NOT"))
				r = !r;
		}
		return r;
	}//calculate
}