package bank.comm;

import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Stack;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.w3c.dom.Comment;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.InputSource;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;
import com.netbank.util.StrUtils;

import bank.classes.ArrayTool;
import lombok.extern.slf4j.Slf4j;

/**
  *
  * FlowObject有三個主要功能
  *		一、解析XML範本取得TITA,TOTA的flow定義
  *			所謂flow,包括TITA,TOTA電文的格式變化,及資料通訊的行為方式(TOTA一送多收,一送一收折返)
  *		二、依flow定義、client pass之Hashtable、DataTemplateX編製TITA byte stream
  *		三、依flow定義、中心系統傳來之TOTA bytes、DataTemplateX解讀TOTA byte stream
  *
  * @create 2005/07/13
  * @author 李國樞  
  *			(based on 林季穎 DataTemplateX TeleMessageMapper @version 1.0 work)
  * @version 2.0
  * 
 **/
 /*
 methods:
 	public:
	    public FlowObject(Node flowNode, String type, String tita, String streamType)		//starter
	    public FlowObject(Hashtable flowObjects, String id, String type, String tita)
 		public int getTOTATable(TelegramDataList template, byte []msg, Hashtable result, 
                    int pos, Hashtable preTITA,Hashtable TOTAByteRecord)
 		public int getTITABytes(TelegramDataList template, Hashtable TITATable, byte []msg, 
				int pos, Hashtable TOTAByteRecord,Hashtable positionMapping, int LoopTimes)
 		public int makeTOTA(TelegramDataList template, Hashtable TOTATable, byte []msg, 
				int pos, byte []TOTAString,Hashtable positionMapping, int LoopTimes) throws CommunicationException

	private:
		private void   createFlowStructure(Node node, Hashtable flowobjects)
		private void   getItemList(Node node)
		private void   getTemplateItems(Node node)
 		private boolean calculateCondition(TelegramDataList template, 
						String messageType, Hashtable result, Hashtable preTITA)
 		private int    calculateSwitch(TelegramDataList template, 
						String messageType, Hashtable result, Hashtable preTITA)
        private int    getTITAFields(TelegramDataList template, Hashtable TITATable, byte []msg, 
            int pos, Hashtable TOTAByteRecord,Hashtable positionMapping, int LoopTimes)
 		private int getTOTATable(TelegramDataList template, byte []msg, Hashtable result, 
            int pos, Hashtable preTITA,Hashtable TOTAByteRecord,int LoopTimes, String length)
 		private int getTOTATable(TelegramDataList template, byte []msg, Hashtable result,
                    int pos, Hashtable preTITA,Hashtable TOTAByteRecord,int LoopTimes)
        private int getTOTATable(TelegramDataList template,Node xmlmsg,Vector result,XMLDataTOTA format)     //解析XML data stream專用
	    private int[] packTOTAElement(TelegramDataList template, Hashtable TOTATable, int pos,
		      byte[] TOTAReport,String clientEncoding,String hostEncoding,
		      String occursblocklength, int loopTimes,int occursflag : 1.陣列 0.非陣列 ---970626)
		private void packTOTAXMLElement(TelegramDataList template, int layer,Hashtable result, 
		              Node xmlnode , XMLDataTOTA format)        //XML data stream
		private void   packTITAElement(String userValue, TelegramData telegramData, int pos, 
    				byte []msg, String clientEncoding, boolean transfertype)		
		private void   packTOTAByteElement(String userValue, TelegramData telegramData, 
					int pos, byte []msg, String clientEncoding, boolean transfertype)
        private String getMsgFieldName(int i)
        private String getMsgFieldName(String field)
        private boolean isNodeNameDefined(String name)
        private String getComplexTypeTagName()
 		private void   getIfConditionS(Node node,Stack operand,Stack operator)
 		private void   getIfCondition(Node node,Stack operand,Stack operator)
 		private boolean needStackPop(String oldOP, String newOP)
 		private void   getQuoteOperator(Node node, Stack operator)
		private void   getLoopContent(Node node)
		private void   getSwitchValue(Node node)
 		private void   getSubFlowObectID(NamedNodeMap attrs)
 		private Hashtable getFlowObjects(Node node)
 		package byte[] fitInTITAFormat(int length, String type, String source)
 		private String fitInTOTAFormat(String type, byte[] element)
 		private byte[] fitInTOTAByteFormat(int length, String type, String source)
 		private Vector CalculateTOTAField(byte []element)
 		private int    calculate2BytesHex(byte hi, byte lo)
 		private int    calculateHexValue(byte h)
 		private void   gatherData(int cellsize, int row, int column,byte []celldata, Vector result)
 		private String formatResponse(String target, byte[] source)
 		private byte[] reverseConvertedCode(byte[] input)
 		private byte[] pc2host(byte[] in)
 		private byte[] host2pc(byte[] in)
 		private byte[] addMarker(byte[] source)
 		private byte[] purgeOutMarker(byte[] source)
 		private byte[] parseAllBytes(byte[] input,Vector posData)
		private String getUnaryOperator(NamedNodeMap attrs)
		private void   checkUnaryOperator(Stack operand, Stack operator)
		private void   getReturnBytes()
 */
@Slf4j
public class FlowObject
{
	private static Logger logger = Logger.getLogger(FlowObject.class.getName());
	private String messageType=null , STREAM_TYPE=null , COMPONENT=null;
	private String flowtype=null,flowid=null,trueid=null,falseid=null;
	private Vector branch=new Vector();    //07.03.28 Albert add, FLOWOBJECT BRANCH#
	private String commandtype=null;   // 07.01.29 add by Albert, to represent the type of Command (Ex.printer,monitor,message...)
	//String refval = null, optional=null;		//for TBB occurs,暫時不做
	private Vector switchID=new Vector();
	private Vector msgField = new Vector();				//此flowObject所指定的電文欄位名稱,如果type!=電文欄位,則size=0
	private SyntaxTree conditionTree = null;
	private ItemValue _switch=null;		
	private Item occursLengthItemValue =null;
	private Vector _caseItemValue=new Vector();			//case value (也是ItemValue物件增加彈性)
	private String OccursLength=null, OccursOffset=null, OccursTimes=null, OccursName=null;

	private FlowObject trueObject=null,falseObject=null;
	private FlowObject []branchObject;
	private FlowObject []switchObject;
	
	private Hashtable ExternVAr=new Hashtable();

	private String clientEncoding=null,hostEncoding=null,titaID=null;
	private Vector nodeVector=null;

	//private boolean isTitaTransfer = true;   // false : 強制送 TITA 時不轉碼
	private boolean isTitaTransfer = true;
	public String contmaxrecodemsg=new String();
	private String maxrecodemsg=new String();
	private String ErrCod=new String();
	private String ErrField=new String();
	private String ErrText=new String();
	
	public String toString()
	{
		return "TYPE="+flowtype+" FLOWID="+flowid+" TRUE="+trueid+" FALSE="+falseid+"\nTrue node="+trueObject+" False node="+falseObject+"\nSyntaxTree="+conditionTree+"\nbranch="+branch;
	}
	
	public FlowObject( Node flowNode, String type, String tita, String streamType, String component,String maxrecode,Hashtable ExVar)
	{   //root FlowObject constructor

		titaID = tita;
		this.STREAM_TYPE = streamType;            // add by Albert for XML TITA,TOTA , 07.3.26
		this.COMPONENT = component;
		messageType = type;
		Hashtable flowobjects = getFlowObjects(flowNode);	
		Node startNode;
		this.contmaxrecodemsg=maxrecode;
		this.ExternVAr=ExVar;
		if(type.equals("TITA"))	
			startNode = (Node)flowobjects.get("開始TITA");
		else
			startNode = (Node)flowobjects.get("開始TOTA");
		createFlowStructure(startNode,flowobjects,maxrecode,ExVar);
//		System.out.println("flowobject="+this);
	}
	
	public FlowObject(Hashtable flowObjects, String id, String type, String tita, String streamType,String maxrecode,Hashtable ExVar)
	{
	    titaID = tita;
		messageType = type;
		Node flowNode = (Node)flowObjects.get(id);
		this.STREAM_TYPE = streamType;
		this.contmaxrecodemsg=maxrecode;
		this.ExternVAr=ExVar;
		createFlowStructure(flowNode,flowObjects,maxrecode,ExVar);
	}
	
	/*
		@param template	: 此電文的所有欄位定義
		@param msg 		: TOTA電文byte stream
		@param result 	: 解譯完成的TOTA資料結構
		@param pos		:
		@param preTITA	: TOTA相應的上行電文
		@param LoopTimes: occur times
		@param length	: occurs total length
			refval,optional attributes for TBB,暫時不做
		@return 0:電文結束 , 1:折返 , 2:繼續接收
	 */
	//是原先的getTelegramDataList和packTOTAElement的合體
	private int getTOTATable(TelegramDataList template, byte []msg, Hashtable result, 
			int pos, Hashtable preTITA,Hashtable TOTAByteRecord,int LoopTimes, String length) throws CommunicationException
	{
		int end = 2;
		int retcod=0;
//		System.out.println("getTOTATable 8 ");
//		System.out.println("flowtype = " + flowtype);
		log.trace("getTOTATable.flowtype>>{}",flowtype);
		//現在採取的替代做法是把pos一直傳遞下去
//		System.out.println("TOTA type="+flowtype+",falseid="+falseid+",trueid="+trueid);
		log.trace("TOTA type>>{},falseid>>{},trueid>>{}",flowtype,falseid,trueid);
		if( (flowtype.equals("開始TOTA") || flowtype.equals("開始TITA")) )
		{
			trueObject.SetAllVarTypeValue(preTITA);
			retcod=trueObject.getTOTATable(template,msg,result,pos,preTITA,TOTAByteRecord,1);
            if(retcod==4)
            {
            	this.ErrCod=trueObject.GetErrCod();
            	this.ErrField=trueObject.GetErrField();
            	this.ErrText=trueObject.GetErrText();
//            	System.out.println(this.ErrCod + ":" + this.ErrField + ":" + this.ErrText);
            }
			if(this.maxrecodemsg.length()==0)
				this.maxrecodemsg=trueObject.GetMaxRecodMsg();
			return(retcod);
		}
		else if(flowtype.equals("條件判斷"))
		{
			boolean b = calculateCondition(template,"TOTA",result,preTITA);	//利用condition定義和電文資料取得條件值,未寫
			log.trace("calculateCondition.b>>{}",b);
			if(b && trueObject!=null)
			{
				trueObject.SetAllVarTypeValue(preTITA);
				retcod=trueObject.getTOTATable(template,msg,result,pos,preTITA,TOTAByteRecord,1);
				log.trace("calculateCondition.retcod>>{}",retcod);
                if(retcod==4)
                {
                	this.ErrCod=trueObject.GetErrCod();
                	this.ErrField=trueObject.GetErrField();
                	this.ErrText=trueObject.GetErrText();
//                	System.out.println(this.ErrCod + ":" + this.ErrField + ":" + this.ErrText);
                }
				if(this.maxrecodemsg.length()==0)
					this.maxrecodemsg=trueObject.GetMaxRecodMsg();
				return(retcod);
			}
			else if(!b && falseObject!=null)
			{
				falseObject.SetAllVarTypeValue(preTITA);
				retcod=falseObject.getTOTATable(template,msg,result,pos,preTITA,TOTAByteRecord,1);
                if(retcod==4)
                {
                	this.ErrCod=falseObject.GetErrCod();
                	this.ErrField=falseObject.GetErrField();
                	this.ErrText=falseObject.GetErrText();
//                	System.out.println(this.ErrCod + ":" + this.ErrField + ":" + this.ErrText);
                }
				if(this.maxrecodemsg.length()==0)
					this.maxrecodemsg=trueObject.GetMaxRecodMsg();
				return(retcod);
			}
			else
				MyDebug.errprt("error; b="+b+",trueObject=【"+trueObject+"】,falseObject=【"+falseObject+"】");
        }
        else if(flowtype.equals("SWITCH"))
        {
            int index = calculateSwitch(template,"TOTA",result,preTITA);	//利用switch定義和電文資料取得條件值,未寫
            //此index為switchObject的array index
            if(switchObject[index]!=null)
                return switchObject[index].getTOTATable(template,msg,result,pos,preTITA,TOTAByteRecord,1);
        }
        else if(flowtype.equals("電文欄位"))
        {
            //children = new String[msgField.size()];   //here!here!here!
            //int size = msgField.size()+sub.length;
            int []r;
            //從byte[]中取得欄位值
//            System.out.println("getTOTATable 電文欄位 STREAM_TYPE = " + STREAM_TYPE);
            if(STREAM_TYPE==null || !STREAM_TYPE.equals("XML") )
            {
//    			this.ErrCod="";
//    			this.ErrField="";
//    			this.ErrText="";
                r = packTOTAElement(template,result, pos, msg, "", "", null, 1 ,TOTAByteRecord,false,preTITA);
                if(r[0]==0)
                {
                	MyDebug.errprt(this.ErrCod + ":" + this.ErrField + ":" + this.ErrText);
                	return(4);
                }
                if(trueObject!=null)        //for byte stream (EBCDIC or ASCII)
                {
                    int blockLength = r[0];
                    pos = pos + blockLength;
                    trueObject.SetAllVarTypeValue(preTITA);
                    end = trueObject.getTOTATable(template,msg,result,pos,preTITA,TOTAByteRecord,1);
                    if(end==4)
                    {
                    	this.ErrCod=trueObject.GetErrCod();
                    	this.ErrField=trueObject.GetErrField();
                    	this.ErrText=trueObject.GetErrText();
//                    	System.out.println(this.ErrCod + ":" + this.ErrField + ":" + this.ErrText);
                    }
                    if(this.maxrecodemsg.length()==0)
                    	this.maxrecodemsg=trueObject.GetMaxRecodMsg();
//            System.out.println("getTOTATable 電文欄位 result = " + result);
                }
            }
            else if(STREAM_TYPE.equals("XML"))        // for XML data 07.03.30
            {
            	MyDebug.errprt("不該發生的條件;XML data stream不該走此method,請檢查錯誤何在");
            }
            return end;
        }
        else if(flowtype.equals("電文陣列"))
        {
            int looptime ;
//            System.out.println("getTOTATable 電文陣列 OccursTimes = " + OccursTimes);
//            System.out.println("getTOTATable 電文陣列 OccursTimes = " + (String)result.get(OccursTimes));
            try
            {
            	if(isNumberOrString(OccursTimes))     //true:number
            		looptime = Integer.parseInt(OccursTimes);
            	else                                  //false:field name
            		looptime = Integer.parseInt( (String)result.get(OccursTimes) );
            	MyDebug.debprt("getTOTATable 電文陣列 looptime = " + looptime);
            }
            catch(Exception e)
            {
            	// System.out.println("OccursTimes execption : " + e);
            	looptime=0;
            }
//			this.recodecnt+=looptime;
//			System.out.println("this.recodecnt = " + this.recodecnt);
            String occurLengthDef=null;
            if(occursLengthItemValue!=null)
            {
                OccursLength = occursLengthItemValue.getValue(template, messageType, result,preTITA);
//				System.out.println("OccursLength="+OccursLength+",occursLengthItemValue="+occursLengthItemValue);
                int len = Integer.parseInt(OccursLength)-Integer.parseInt(OccursOffset);
                occurLengthDef = Integer.toString(len);
            }
            int []r = packTOTAElement(template,result, pos, msg, "", "", occurLengthDef, looptime,TOTAByteRecord,true,null);
            int blockLength = r[0];
            pos = pos + blockLength;
            if(trueObject!=null)
            {
            	trueObject.SetAllVarTypeValue(preTITA);
                end = trueObject.getTOTATable(template,msg,result,pos,preTITA,TOTAByteRecord,1);
                if(end==4)
                {
                	this.ErrCod=trueObject.GetErrCod();
                	this.ErrField=trueObject.GetErrField();
                	this.ErrText=trueObject.GetErrText();
                	MyDebug.errprt(this.ErrCod + ":" + this.ErrField + ":" + this.ErrText);
                }
                if(this.maxrecodemsg.length()==0)
                	this.maxrecodemsg=trueObject.GetMaxRecodMsg();
            }
            return end;
        }
        else if(flowtype.equals("結束TOTA") || flowtype.equals("結束TITA"))
        {
//			System.out.println("結束了");
//            if(this.maxrecodemsg.equals(this.contmaxrecodemsg))
//            {
//            	return(3);
//            }
            return 0;
        }
        else if(flowtype.equals("延續"))
        {
        	MyDebug.debprt("延續");
            return 2;
        }
        else if(flowtype.equals("折返"))
        {
//System.out.println("TOTAByteRecord="+TOTAByteRecord);
//			System.out.println("折返");
            getReturnBytes(TOTAByteRecord);
            MyDebug.debprt("折返 this.maxrecodemsg=" + this.maxrecodemsg + "|");
            MyDebug.debprt("折返 this.contmaxrecodemsg=" + this.contmaxrecodemsg + "|");
/*
            if(this.maxrecode<=this.recodecnt)
            	return 3;// maxrecode > maxrecode return(3);
*/
            return 1;
        }
		return end;
	}//getTOTATable

    //解析XML data stream專用
    private int getTOTATable(TelegramDataList template, Node xmlmsg, Vector result,
                                    int xmlTreeLevel, XMLDataTOTA format)
    {
        Node subnode=null;
        xmlTreeLevel++;

        if( flowtype.equals("開始TOTA") )
        {
            return trueObject.getTOTATable(template,xmlmsg,result,xmlTreeLevel,format);
        }
        else if(flowtype.equals("電文欄位"))
        {
            int []r;
            String fname=null;
            
            if(format.getTotalDataContainer()==null)
            {
//                System.out.println("xmlmsg.getNodeName()="+xmlmsg.getNodeName());
                format.setTotalDataContainer(result);
                if(branchObject[0].getFlowObjectType().equals("電文陣列"))
                {
                    Vector v = getAllSubComplexNodeForField( xmlmsg, branchObject[0].getMsgFields() );
                    branchObject[0].addXMLNodes(v);
                    branchObject[0].getTOTATable(template,null,result,xmlTreeLevel,format);    //電文陣列
                }
                else    //電文欄位
                    return branchObject[0].getTOTATable(template,xmlmsg,result,xmlTreeLevel,format);
            }
            else
            {
            if(format.getCurrentMaster()==null)
                format.setCurrentMaster( new Hashtable() );
            Hashtable master = format.getCurrentMaster();
            Vector    detail = format.getCurrentDetail();
            if(branch.size()>0 && STREAM_TYPE.equals("XML"))        // for XML data 07.03.30
            {
                if(msgField.size()>1)
                {
                	MyDebug.errprt("XML格式定義語法錯誤:定義complex type的電文欄位不可以有兩個以上的tag name");
                    return 0;
                }
                //System.out.println("電文欄位 processing:"+xmlmsg.getNodeName());

                for(int ii=0 ; ii<branchObject.length ; ii++)
                {
                    format.setCurrentMasterDetail( master,detail );
                    NodeList nodes = xmlmsg.getChildNodes();
                    for(int jj=0 ; jj<nodes.getLength(); jj++)
                    {
                        subnode = nodes.item(jj);
                        if( branchObject[ii].getMsgFieldName(subnode) != null)  
                        {
                            //System.out.println("Process:("+subnode.getNodeName()+")");  
                            if(branchObject[ii].getFlowObjectType().equals("電文陣列"))
                            {
                                Vector v = getAllSubComplexNodeForField( subnode, branchObject[ii].getMsgFields() );
                                //addXMLNodes & new hashtable?
                                branchObject[ii].addXMLNodes(v);
                                branchObject[ii].getTOTATable(template, null, result, xmlTreeLevel, format);
                            }
                            else if(branchObject[ii].getFlowObjectType().equals("電文欄位"))
                            {   //處理電文欄位的FlowObject要根據自己的msgField在node1中找符合的sub node
                                branchObject[ii].getTOTATable(template, subnode, result, xmlTreeLevel, format);
                            }
                        }
                    }
                }
            }
            else if(branch.size()==0 && STREAM_TYPE.equals("XML") )     // for XML data 07.03.30
            {   //當branch.size()==0時表示此XML node已不是complex node,可取其node value
                NodeList nodes = xmlmsg.getChildNodes();
                for(int i=0; i<nodes.getLength(); i++)
            	{
                    subnode = (Node)nodes.item(i);
                    if( isNodeNameDefined(subnode.getNodeName()) ) 
                    {
                        packTOTAXMLElement(template,subnode,xmlTreeLevel,format);
                    }
                }
            }
            }
        }
        else if(flowtype.equals("電文陣列"))
        {   //做法如下:
            //1.前一個FlowObject會依此電文陣列FlowObject定義的tag name,在XML data stream中找出所有的node
            //  例如電文陣列FlowObject定義的是OCCURS1,而XML data stream中是
            //  <FIELD1>xxx</FIELD1><FIELD2>yyy</FIELD2><OCCURS1></OCCURS1><OCCURS1></OCCURS1><OCCURS2></OCCURS2>
            //  前一層FlowObject就會將兩個OCCURS1 node放入nodeVector中
            //2.nodeVector中的每一個node就相對於一份Detail table 
            //3.找到定義nodeVector裡的這些nodes的FlowObject XXX
            //4.
            if(result.size()>0)
            {
                result = new Vector();
                format.setCurrentDetail(result, (String)msgField.get(0));
            }
            //System.out.println("電文陣列 processing:"+(String)msgField.get(0));  //▽debug訊息解開可用
            for(int i=0; i<nodeVector.size(); i++)
            {
                Node node1 = (Node)nodeVector.get(i);
                Hashtable master = new Hashtable();
                format.setCurrentDetail( result );
                format.setCurrentMaster( master );
                for(int ii=0 ; ii<branchObject.length ; ii++)
                {
                    if( branchObject[ii].getMsgFieldName(node1) != null)  
                    {
                        //System.out.println("Process:("+node1.getNodeName()+")");   //△debug訊息解開可用
                        if(branchObject[ii].getFlowObjectType().equals("電文陣列"))
                        {
                            Vector v = getAllSubComplexNodeForField( node1, branchObject[ii].getMsgFields() );
                            //System.out.println("getAllSubComplexNodeForField="+v);
                            branchObject[ii].addXMLNodes(v);
                            branchObject[ii].getTOTATable(template, null, result, xmlTreeLevel, format);
                        }
                        else if(branchObject[ii].getFlowObjectType().equals("電文欄位"))
                        {   //處理電文欄位的FlowObject要根據自己的msgField在node1中找符合的sub node
                            branchObject[ii].getTOTATable(template, node1, result, xmlTreeLevel, format);
                        }
                    }
                    format.setCurrentMasterDetail( master,result );
                }
            }
        }
        else if(flowtype.equals("結束TOTA") || flowtype.equals("結束TITA"))
        {
        	
        	MyDebug.errprt("XML結束了,但不應該出現在這裡,XML data stream沒有結束TAG");
        }
        else
        	MyDebug.errprt("電文定義XML格式錯誤:XML data stream只有電文欄位");

        return 0;
    }//getTOTATable for XML stream
    
	//沒有occurs(電文陣列),所以length給null
	private int getTOTATable(TelegramDataList template, byte []msg, Hashtable result,
                        int pos, Hashtable preTITA,Hashtable TOTAByteRecord,int LoopTimes) throws CommunicationException
	{
//		System.out.println("getTOTATable 7 ");
		return getTOTATable(template,msg,result,pos,preTITA,TOTAByteRecord,LoopTimes,null);
	}
	
	public void addXMLNodes( Vector nodes )
	{
        nodeVector = nodes;
    }
    private Vector getAllSubComplexNodeForField( Node node, Vector fields )
    {
        NodeList nodes = node.getChildNodes();
        Vector objectNodes = new Vector();
        for(int i=0; i<nodes.getLength(); i++)
        {
            Node subnode = nodes.item(i);
            if(subnode.getNodeType() == Node.ELEMENT_NODE)
            {
                for(int j=0; j<fields.size(); j++)
                {
                    String f = (String)fields.get(j);
                    if(f.equals(subnode.getNodeName()) )
                    {
                        objectNodes.add(subnode);
                        break;
                    }
                }
            }
        }
//		System.out.println("getAllSubComplexNodeForField="+objectNodes);
        if(objectNodes.size()==0)
        	MyDebug.errprt("匹配錯誤: nodename = "+node.getNodeName()+",fields="+fields);
        else
            return objectNodes;
        return null;
    }//getAllSubComplexNodeForField

	//不需參考preTITA的情形,preTITA給null
	public int getTOTATable(TelegramDataList template, byte []msg, Vector result, 
                            int pos, Hashtable preTITA, Hashtable TOTAByteRecord) throws CommunicationException
	{
	    int end=0;

	    //此method的result改成Vector(原先為Hashtable),主要是為了方便xml data stream的解析.
	    //在原先的電文收送架構下,vector放在TransactionImpl去new,因為是否有下一個電文需折返是在那控制的.
	    //而XML stream很可能是一次收足.所以在不調整原先架構的原則下,我將Vector傳進來,在這裡new Hastable.
	    //這樣剛好可滿足兩邊的需求         2007.04.04
	    MyDebug.debprt("getTOTATable 6 parm STREAM_TYPE = " + STREAM_TYPE);
        if(STREAM_TYPE==null || !STREAM_TYPE.equals("XML"))  //中心電文
        {
            Hashtable h = new Hashtable();
            end = getTOTATable(template, msg, h, pos, preTITA,TOTAByteRecord,1,null);
            result.add(h);
            return end;
        }
        else if(STREAM_TYPE.equals("XML"))   //XML電文
        {
            XMLDataTOTA format =null;
            Element root=null;
            int xmlTreeLevel=1;

            format = new XMLDataTOTA();

            try
            {
                ByteArrayInputStream xmlmsg = new ByteArrayInputStream(msg);
                
                int avail = xmlmsg.available() ;
                byte []b = new byte[avail];
                xmlmsg.read(b,0,avail);
//				System.out.println("\n\n\nmsg.length="+msg.length+",avail="+avail+","+new String(b)+".");  
                xmlmsg.reset();
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = factory.newDocumentBuilder();
                InputSource source = new InputSource(xmlmsg);
                
                source.setEncoding("MS950");
                Document document = builder.parse(source);
                //Document document = builder.parse("C:\\JAVA\\TEST\\bank.new\\R140.xml");
                root = document.getDocumentElement();
                end = getTOTATable(template,root,result,xmlTreeLevel,format);
            }
            catch(Exception ee)
            {
            	MyDebug.errprt("getTOTATable error-->" + ee.getStackTrace());
            }
            MyDebug.debprt("OK,XML結束了");
            return end;
        }
        return end;
	}//public getTOTATable 

    // @LoopTimes: total loop times
    // @[]fields : 帶編號的field name
    // @date : 2005/10/03 Albert
    // @deprecated 這method已不需要,因現在對occurs不需要取得有_的field name(改為放vector) 6/6/23
    private int[] getLoopedFields(TelegramDataList template,int LoopTimes, 
                                    String []fields, String length)
    {
		int index=0,blockLength=0,c;
		int occurLength = length==null ? 2147483647 : Integer.parseInt(length);
		//occurs次數由occurLength控制,occurLength定義occurs的總長度
		//blockLength累計目前occurs已讀取的長度
		for(c=0; c<LoopTimes && blockLength<occurLength; c++)
		{
    	   for(index=0; index<msgField.size(); index++)
	   	   {
                String fieldName = (String)msgField.get(index);
    			TelegramData t = (TelegramData)template.get(fieldName);
    	   		String f_optional = t.optional, f_refval = t.refval;
	       	  	// 當optional所指定的欄位其TOTA中的值等於refval,	(用於欄位可有可無)
                // 表示目前電文有此欄位,否則表示目前電文沒有此欄位	albert 6/14
                /*if(f_optional!=null && f_refval!=null)
			     {
				    if( !((String)result.get(f_optional)).equals(f_refval) )
					   continue;
			     }*/
			    blockLength += t.length;
                fields[index+msgField.size()*c] = LoopTimes==1 ? t.name : t.name+"_"+c; 
            }//for end
        }//for end

        int []result = new int[2];
        result[0] = blockLength;
        result[1] = c;
        return result;
    } //getLoopedFields
    
	//  @param TOTATable	: 從TOTAReport bytes中取得的result (name/value pair)
	// 把getLoopedFields中處理loop times與occurs length的搬來這裡
	private int[] packTOTAElement(TelegramDataList template, Hashtable TOTATable, int pos,
		byte[] TOTAReport,String clientEncoding,String hostEncoding, 
		String occursblocklength, int loopTimes, Hashtable TOTAByteRecord, boolean occursflag,Hashtable preTITA) throws CommunicationException
    {
		int x,y;
		Vector[] elementVector = new Vector[msgField.size()];
		int index=0,blockLength=0 , i , j, timesCount;
		int occurLength = occursblocklength==null ? 2147483647 : Integer.parseInt(occursblocklength);
		Hashtable occursBlock = new Hashtable();  //occurs內的欄位集合,用在master-detail table 6.7.17

		for(i=0; i<msgField.size(); i++)  
			elementVector[i] = new Vector();

		MyDebug.debprt("====1===== blockLength = " + blockLength);
		MyDebug.debprt("====1===== occurLength = " + occurLength);
		MyDebug.debprt("====1===== loopTimes = " + loopTimes);
		for(j=0; j<loopTimes && blockLength<occurLength; j++)
		{
//     	System.out.println("====2===== loopTimes = " + loopTimes);
			MyDebug.debprt("====2===== msgField.size = " + msgField.size());
			for(i=0,timesCount=1; i<msgField.size(); i++)
			{
				MyDebug.debprt("loop start msgField.get(" + i +")=" + msgField.get(i) + "|");
				TelegramData tdata = template.get((String)msgField.get(i) );
				if(tdata==null)
				{
//					int []err_result = new int[2];
//					err_result[0] = 0;  //若occursblocklength!=null則blockLength就是occursblocklength
//					err_result[1] = 0;    // real occurs times    6.6.23
//					this.ErrCod="Z013";
//					this.ErrField=(String)msgField.get(i);
//					this.ErrText="欄位名稱未定義";
//					return err_result;
					throw new CommunicationException("M1", "Z013","");
					
				}
				String name = tdata.name;
				MyDebug.debprt("name = " + tdata.name);
				String value = tdata.value;
				MyDebug.debprt("value = " + tdata.value);
				String type = tdata.type;
				MyDebug.debprt("type = " + tdata.type);
				String optional = tdata.optional;
            String refval = tdata.refval;
            String format = tdata.format;
            String encoding = tdata.encoding;
            String transfer = tdata.transfer;
            String datasemantic = tdata.datasemantic;       //06.10.11
            String datasemanticval = tdata.datasemanticval; //06.10.11
            int length = tdata.length;
            String elementValue = "";

            if(length==0 && datasemantic.length()>1)
            {   //此欄位是否存在由optional指向的欄位值與refval定義的參考值來決定 (@optional==refval =>存在)
                //基本上,此欄位代表整個變動區,長度直接讀至array.length  (目前for FCB TM電文   06.10.11)
                String optValue = (String)TOTATable.get(optional);
                //if(optValue.equals(refval))   只要datasemantic有定義,變動區就一定存在,將來取消
                //datasemantic後會改成判斷FlowObject的type     06.10.23:15P(與之前版本差異)
                {   //變動區存在
                    String strSemantic = (String)TOTATable.get(datasemantic);
                    if(strSemantic==null || !strSemantic.equals(datasemanticval))
                    {   //此變動區為(row,column,length,data) format
                        elementVector   = new Vector[3];
                        for(int ii=0 ; ii<elementVector.length; elementVector[ii++]=new Vector())  ;
                        while(pos<TOTAReport.length)
                        {
                            byte []row      = ArrayTool.subarray(TOTAReport, pos, 2);
                            byte []column   = ArrayTool.subarray(TOTAReport, pos+2, 1);
                            byte []flength  = ArrayTool.subarray(TOTAReport, pos+2+1, 1);
                            int  datalen    = getValueFromByteArray(flength);
//							System.out.println("datalen="+datalen+",TOTAReport.length="+TOTAReport.length+",pos="+pos);
                            byte []data     = ArrayTool.subarray(TOTAReport, pos+2+1+1, datalen);
                            pos+= 2+1+1+datalen;
                            elementVector[0].add( Integer.toString(getValueFromByteArray(row)) );
                            elementVector[1].add( Integer.toString(getValueFromByteArray(column)) );
                            elementVector[2].add( fitInTOTAFormat("x", data) );
                        }
                        TOTATable.put("Row",elementVector[0]);
                        TOTATable.put("Column",elementVector[1]);
                        TOTATable.put("Data",elementVector[2]);
                        occursBlock.put("Row",elementVector[0]);
                        occursBlock.put("Column",elementVector[1]);
                        occursBlock.put("Data",elementVector[2]);
                        TOTATable.put("OCCURS_"+name,occursBlock);
                        blockLength = pos;
                    }
                    else
                    {   //此變動區為一個字串
                        length = TOTAReport.length-pos;
                        elementVector[0] = new Vector();
                        if(length>0)
                        {
                            byte []element = ArrayTool.subarray(TOTAReport, pos, length);
                            elementValue = fitInTOTAFormat(type, element);
                            elementVector[0].add(elementValue);
                        }
                        TOTATable.put("Data",elementVector[0]);
                        length=0;
//						System.out.println("TOTAReport.length="+TOTAReport.length+",length="+length+",pos="+pos);
                    }
                }
            }
            else if(length==0)		//這一段暫時保留,現在不測		albert 5/8/1
            {	//如果length==0,則此欄位的長度是由前面的欄位所決定的	albert 6/16
            	//以TBB來說,若TEXTFIELD.LENGTH=="0",且
            	//TEXTFIELD.OPTIONAL所指向的欄位(APCNTLINFOBYTE)==TEXTFIELD.REFVAL,
            	//則length=TEXTLNG(TEXTFIELD.FORMAT)-APCNTLINFO.LENGTH(TEXTFIELD.ENCODING)
            	//若TEXTFIELD.OPTIONAL所指向的欄位(APCNTLINFOBYTE)!=TEXTFIELD.REFVAL,
            	//則length=TEXTLNG(TEXTFIELD.FORMAT)
            	format=format+timesCount;
            	encoding=encoding+timesCount;
//				System.out.println("format="+format+",encoding="+encoding+",optional="+optional);
            	String refFieldValue = (String)TOTATable.get(optional);
           		int textLength = Integer.parseInt((String)TOTATable.get(format));
            	if(refFieldValue.equals(refval))
            	{
            		String enc=encoding;	//在telegramDataList中的欄位名沒有序號,
            		String num = Integer.toString(timesCount);
            		//if(occursStart)
            			enc = encoding.substring(0,encoding.length()-num.length());
            		TelegramData t = template.get(enc);
            		int APCntlInfoLength = t.length;
            		length = textLength - APCntlInfoLength ;
            	}
            	else
            		length = textLength ;
            }

            if(length>0)
            {
            	MyDebug.debprt("TOTAReport.length="+TOTAReport.length+",pos="+pos+",length="+length+",msgField.get("+i+")="+(String)msgField.get(i)+",j="+j);
                byte[] element=null;
                if(TOTAReport.length>=(pos+length))
                {
                	element = ArrayTool.subarray(TOTAReport, pos, length);
                }
                else
                {
                	element=new byte[length];
                	if(TOTAReport.length>pos)
                	{
                		for(x=0;x<(TOTAReport.length-pos);x++)
                		{
                			element[x] = TOTAReport[pos+x];
                		}
                    	for(y=TOTAReport.length-pos;y<length;y++)
                    	{
                    		if(!transfer.equalsIgnoreCase("NO"))
                    			element[y] = (byte)0xF0;
                    		else
                    			element[y] = (byte)0x30;
                    	}
                	}
                	else
                	{
                		for(y=0;y<length;y++)
                    	{
                    		if(!transfer.equalsIgnoreCase("NO"))
                    			element[y] = (byte)0xF0;
                    		else
                    			element[y] = (byte)0x30;
                    	}
                	}
                }
                pos += length;
                blockLength += length;
                if(loopTimes==1)
                	TOTAByteRecord.put(msgField.get(i), element);
    
                if((!encoding.equals(clientEncoding))&&(!encoding.equals("")))
                {	// encoding作用未被啟動
                    //userValue = convert(userValue,encoding);	
                }
                if(!transfer.equalsIgnoreCase("NO"))    //要轉碼(ex.EBCDIC)
                {	//for TBB,加兩個type
                	//	XC type : 不用addMarker  				albert 6/15
            	   //	CL type : recursive(row column data)計算
                    if(type.equalsIgnoreCase("X+"))
                        elementValue = fitInTOTAFormat(type, reverseConvertedCode(ConvCode.host2pc(addMarker(element))));
                    else if(type.equalsIgnoreCase("XC"))  //albert 6/15
                    	elementValue = fitInTOTAFormat(type, reverseConvertedCode(ConvCode.host2pc(element)));
                    else
                    {
                        elementValue = fitInTOTAFormat(type, ConvCode.host2pc(element));
                        if(type.equals("9"))
                        {
                        	int spcnt=0;
                        	byte[] comp=elementValue.getBytes();
                        	for(int zzz=0;zzz<comp.length;zzz++)
                        	{
                        		if(comp[zzz]==0x20)
                        			spcnt++;
                        		else
                        			break;
                        	}
                        	if(spcnt==comp.length)
                        	{
                        		elementValue=value;
                        	}
                            try {
                                // System.out.println("type = 9 elementValue Hex=="+ConvCode.toHexString(elementValue.getBytes("UnicodeBig")));
                                }catch(Exception e) {
                                	
                                }                        
                        }
                    }
                    try {
                        // System.out.println("type = X elementValue Hex=="+ConvCode.toHexString(elementValue.getBytes("UnicodeBig")));
                        }catch(Exception e) {                        	
                        }
                    MyDebug.debprt("packTOTAElement add transfer=YES elementValue = " + elementValue);
                                        
                    if (elementValue.indexOf("__UC") != -1)
                    {
                    	elementValue = elementValue.substring(0, elementValue.indexOf("__UC"));
                    
                    	try
                    	{
                    		elementValue = new String(elementValue.getBytes("UnicodeBig"), "UnicodeBig");
                    	}
                    	catch (Exception e)
                    	{}
                    }
                    
                    elementVector[i].add(elementValue);
                    
                    MyDebug.debprt("name="+name+",pos="+pos+",length="+length+",elementVector["+i+"]="+elementVector[i]);
                    MyDebug.debprt("packTOTAElement transfer=YES elementValue = " + elementValue);
                } 
                else    //不要轉碼
                {
//                	20200924 add by hugo 不轉碼表示從AS400過來的電文是big5 不是EBCDIC 因後面直接都轉字串UTF-8故在此做big5-->utf8 
//                	TODO 未來可能要處理BIG5的難字
                	try {
						element = new String(element,"MS950").getBytes("UTF-8");
					} catch (UnsupportedEncodingException e) {
						log.error("BIG5-->UTF-8.ERROR>>{}",e);
					}
                	
                	
                    if(occursflag)	    //albert 06/6/22
                    {
                    	//elementVector = CalculateTOTAField(element);
                    	elementValue = fitInTOTAFormat(type, element);
                    	elementVector[i].add(elementValue);
                    	MyDebug.debprt("name="+name+",pos="+pos+",length="+length+",elementVector["+i+"]="+elementVector[i]);
                    }
                    else 
                    {   //沒有loop
                    	MyDebug.debprt("type="+type+",elementValue="+elementValue);
                    	elementValue = fitInTOTAFormat(type, element);
                        if(type.equals("9"))
                        {
                        	int spcnt=0;
                        	byte[] comp=elementValue.getBytes();
                        	for(int zzz=0;zzz<comp.length;zzz++)
                        	{
                        		if(comp[zzz]==0x20)
                        			spcnt++;
                        		else
                        			break;
                        	}
                        	if(spcnt==comp.length)
                        	{
                        		elementValue=value;
                        	}
                        }
                    }
                    MyDebug.debprt("packTOTAElement transfer=NO elementValue = " + elementValue);
                }  
                if(!format.equals("")){
                    //elementValue = reformat(elementValue,format);                    
                }
            }
            
//            if(loopTimes>1)		// for occurs    albert 06/6/22 。0970626陣列會有問題須改如下
            if(occursflag)
            {
                Vector v = (Vector)TOTATable.get(msgField.get(i));
//				System.out.println("packTOTAElement Vector v = " + v);
                MyDebug.debprt("packTOTAElement msgField.get(" + i +")=" + msgField.get(i));
                MyDebug.debprt("packTOTAElement elementVector(" + i +")=" + elementVector[i]);
                if(v==null)
                {   //occurs欄位除了放在TOTATable,也要另外放在occursBlock,這是為了多occurs的電文,
                    //我們會對每個occurs命名,以方便前端存取不同occurs內的欄位(相當於master-detail中的detail table)
            	    TOTATable.put(msgField.get(i),elementVector[i]);
                    occursBlock.put(msgField.get(i),elementVector[i]);  //6.7.17
            	}
            }
            else
            {
            	TOTATable.put(msgField.get(i),elementValue);
            	MyDebug.debprt("msgField.get(" + i + ")=" + msgField.get(i) + ",preTITA.get(TOPMSG)=" + preTITA.get("TOPMSG"));
            	if(msgField.get(i).equals(preTITA.get("TOPMSG")))
            	{
            		TOTATable.put("TOPMSG",elementValue);
            		MyDebug.debprt("TOTATable.get(TOPMSG)=" + TOTATable.get("TOPMSG"));
            		maxrecodemsg=elementValue;
            		MyDebug.debprt("packTOTAElement this.maxrecodemsg=" + this.maxrecodemsg + "|");
            	}
            	MyDebug.debprt("packTOTAElement msgField.get(" + i +")=" + msgField.get(i) + "=" + elementValue);
            }
    	}//end for i
      }//end for j
//      if(loopTimes>1)   0970626陣列會有問題須改如下
      if(occursflag)
      {
//      System.out.println("packTOTAElement OCCURS_"+OccursName);
//		System.out.println("packTOTAElement occursBlock" + occursBlock );
        TOTATable.put("OCCURS_"+OccursName,occursBlock);    //7.17
        Vector v=null;
        if( (v=(Vector)TOTATable.get("OCCURS_VECTOR_FOR_MULTIPLE_INDEX")) ==null)
        {
            v = new Vector();
            TOTATable.put("OCCURS_VECTOR_FOR_MULTIPLE_INDEX",v);
        }
        v.add(occursBlock);
      }  
      int []result = new int[2];
      result[0] = blockLength;  //若occursblocklength!=null則blockLength就是occursblocklength
      result[1] = j;    // real occurs times    6.6.23
      return result;
    }//packTOTAElement end

    //當TOTA需折返時,此method根據折返tag中所定義的欄位取出tota bytes需折返的一段
    //並將折返欄位與折返bytes放在hashtable中        06.06.21    albert
    private void getReturnBytes(Hashtable TOTAByteRecord)
    {
        Hashtable h = new Hashtable();
        for(int i=0; i<msgField.size(); i++)
        {
            String s = (String)msgField.get(i);
            byte []b = (byte [])TOTAByteRecord.get(s);
            if(b!=null)
                h.put(s, b);
            else
            {
            	MyDebug.errprt("FlowObject fatal error at getReturnBytes!");
                System.exit(1);
            }
        }
        TOTAByteRecord.clear();
        for(Enumeration e = h.keys(); e.hasMoreElements(); )
        {
            String keyname =(String)e.nextElement();
            TOTAByteRecord.put(keyname, h.get(keyname));
        }
        TOTAByteRecord.put("TOTARETURNFIELDS", msgField);
    }//getReturnBytes

    //@date : 2006.10.11    Albert
    private int getValueFromByteArray(byte []flength)
    {
        int r=0;
        for(int i=flength.length-1,mul=1; i>=0 ; i--,mul*=256)
            r+= (int)flength[i] * mul;
        return r;
    }

    //@date : 2006.06.23    Albert
    private boolean isNumberOrString(String value)     //true:number
    {
        boolean isNumber = true;
        try
        {
            Integer.parseInt(value);
        }
        catch(NumberFormatException e)
        {   isNumber = false;   }
        return isNumber;
    }//isNumberOrString

    // layer:目前處裡的xml node是第幾層的tag,例如root tag是0,其下第一層的sub nodes都是1,...餘類推
    private void packTOTAXMLElement(TelegramDataList template, Node xmlnode,int xmlTreeLevel,XMLDataTOTA format)
    {
        if(format != null)
        {
            //System.out.println("packTOTAXMLElement,nodename="+xmlnode.getNodeName());
            format.parseMessage(template,xmlnode,msgField,xmlTreeLevel);
        }
        else
        	MyDebug.errprt("********XMLDataTOTA COMPONENT definition error***********");
    }//packTOTAXMLElement
    
    public Vector getMsgFields()
    {
        return msgField;
    }
    public String getMsgFieldName(int i)
    {
        return (String)msgField.get(i);
    }
    public String getMsgFieldName(String field)
    {
        for(int i=0; i<msgField.size(); i++)
        {
            String s = (String)msgField.get(i);
            if(s.equals(field))
                return s;
        }
        return null;
    }
    public String getMsgFieldName(Node node)
    {
        NodeList nodes = node.getChildNodes();
        for(int i=0; i<nodes.getLength(); i++)
        {
            Node node1 = nodes.item(i);
	        if(node1.getNodeType() == Node.ELEMENT_NODE)
            {   
                for(int j=0; j<msgField.size(); j++)
                {
                    String s1 = (String)msgField.get(j);
                    if(s1.equals(node1.getNodeName()))
                        return s1;
                }
            }
        }
        return null;
    }
    public String getFlowObjectType()
    {
        return flowtype;
    }
    
    //2007.4.10 Albert
    private boolean isNodeNameDefined(String name)
    {
        for(int i=0; i<msgField.size(); i++)
            if(name.equals((String)msgField.get(i)))
                return true;
        return false;
    }
    
    //★★★ 當STREAM_TYPE=="XML"時,若FlowObject被定義成電文陣列,則表示該XML stream相對應的tag會出現多次
    //特別注意:這裡的做法會沿著FLOWOBJECT路徑遞迴下去直到找到電文陣列,並把此level以上的電文欄位type都打平(與電文陣列Vector同階)
    //在拿到新的structure of XML stream時這種處理XML結構的方式要特別加以審視,確定是否符合該structure的語義   2007.4.13 Albert
    // @return : 路徑中有電文陣列return tag name,沒有則return null
    private String getComplexTypeTagName()
    {
        String ret=null;
        if(flowtype.equals("電文陣列"))
            return (String)msgField.get(0);
        else if(trueObject!=null)
            return trueObject.getComplexTypeTagName();
        else if(branchObject.length>0)
        {
            for(int i=0 ; i<branchObject.length; i++)
                if( (ret=branchObject[i].getComplexTypeTagName()) != null )
                    break;
        }
        return ret;
    }//getComplexTypeTagName
    
	private boolean calculateCondition(TelegramDataList template, 
						String messageType, Hashtable result, Hashtable preTITA)
	{  	//利用conditionTree(SyntaxTree)與傳入之電文值(byte []msg)計算boolean值
		//利用result取得condition所需的欄位position
//		System.out.println("calculateCondition");
		return conditionTree.calculate(template, messageType, result,preTITA);
	}
	
	private int calculateSwitch(TelegramDataList template, 
						String messageType, Hashtable result, Hashtable preTITA)
	{	//利用_switch(ItemValue)、_caseItemValue與傳入之電文值(byte []msg)計算boolean值

		String s = _switch.getValue(template, messageType, result,preTITA);  // _switch是<SWITCH>值
		for(int i=0; i<_caseItemValue.size(); i++)	
		{	//_caseItemValue(Vector)是switch的case value
			ItemValue item = (ItemValue)_caseItemValue.get(i);
			if(s.equals(item.getValue(template, messageType, result, preTITA)))	
				return i;		//找出第幾個case符合switch的值
		}
		return 999999;	//應改成throws exception
	}
	
	/*
	  @param template   : 所有的TITA欄位
	  @param TITATable	: 由user client送過來的TITA data structure
	  @param byte msg[]	: 編好的上行電文byte stream
	  @param pos		: msg的current position
	  @param TOTAByteRecord	: 前一個TOTA的內容(已解譯成hashtable)
	  @return 			: TITA byte stream length
	 */
	private int getTITABytes(TelegramDataList template, Hashtable TITATable, byte []msg, 
				int pos, Hashtable TOTAByteRecord,Hashtable positionMapping, int LoopTimes) throws CommunicationException
	{	//pos一代一代傳遞,就能提供packTITAElement處理msg[]時正確的position 		8/3
		//這裡最大的問題是,某個欄位的值可能不在client傳來的TITATable中,而是		8/1
		//從一些欄位值透過計算而得. 此時,我是否應把計算公式定義在XML中?
		//例如occurs的長度就是如此.一定是到了編制電文時才能計算再回填
		log.trace("flowtype>>{}",flowtype);
		int ret;
		
		if(flowtype.equals("開始TITA"))
		{
			trueObject.SetAllVarTypeValue(TITATable);
			ret=trueObject.getTITABytes(template,TITATable,msg,pos,TOTAByteRecord,positionMapping,1);
			this.ErrCod=trueObject.GetErrCod();
			this.ErrField=trueObject.GetErrField();
			this.ErrText=trueObject.GetErrText();
			return(ret);
		}
		else if(flowtype.equals("條件判斷"))
		{
			boolean b = calculateCondition(template,"TITA",TITATable,TOTAByteRecord);	//利用condition定義和電文資料取得條件值,未寫
//			System.out.println(flowid+".pos="+pos);
			if(b && trueObject!=null)
			{
				ret=trueObject.getTITABytes(template,TITATable,msg,pos,TOTAByteRecord,positionMapping,1);
				this.ErrCod=trueObject.GetErrCod();
				this.ErrField=trueObject.GetErrField();
				this.ErrText=trueObject.GetErrText();
				return(ret);
			}
			else if(b && falseObject!=null)
			{
				ret=falseObject.getTITABytes(template,TITATable,msg,pos,TOTAByteRecord,positionMapping,1);
				this.ErrCod=trueObject.GetErrCod();
				this.ErrField=trueObject.GetErrField();
				this.ErrText=trueObject.GetErrText();
				return(ret);
			}
		}
		else if(flowtype.equals("SWITCH"))
		{
			int index = calculateSwitch(template,"TITA",TITATable,TOTAByteRecord);	//利用switch定義和電文資料取得條件值,未寫
			//此index為switchObject的array index
			if(switchObject[index]!=null)
				return switchObject[index].getTITABytes(template,TITATable,msg,pos,TOTAByteRecord,positionMapping,1);
		}
		else if(flowtype.equals("電文欄位"))
		{
//			System.out.println("TITATable="+TITATable);
            pos = getTITAFields(template,TITATable,msg,pos,TOTAByteRecord,positionMapping,1);
            if(pos==0)
            	return(pos);
//			System.out.println(flowid+".pos="+pos);
            if(trueObject!=null)
            {
            	trueObject.SetAllVarTypeValue(TITATable);
            	ret=trueObject.getTITABytes(template,TITATable,msg,pos,TOTAByteRecord,positionMapping,1);
				this.ErrCod=trueObject.GetErrCod();
				this.ErrField=trueObject.GetErrField();
				this.ErrText=trueObject.GetErrText();
				return(ret);
            }
		}
		else if(flowtype.equals("電文陣列"))
		{
			int looptime = Integer.parseInt(OccursTimes);
			pos = pos + getTITAFields(template,TITATable,msg,pos,TOTAByteRecord,positionMapping,1);
            if(pos==0)
            	return(pos);
			if(trueObject!=null)
			{
				trueObject.SetAllVarTypeValue(TITATable);
				ret=trueObject.getTITABytes(template,TITATable,msg,pos,TOTAByteRecord,positionMapping,1);
				this.ErrCod=trueObject.GetErrCod();
				this.ErrField=trueObject.GetErrField();
				this.ErrText=trueObject.GetErrText();
                return(ret);
			}
		}
		
		return pos;
	}//getTITABytes

    private int getTITAFields(TelegramDataList template, Hashtable TITATable, byte []msg, 
				int pos, Hashtable TOTAByteRecord,Hashtable positionMapping, int LoopTimes)throws CommunicationException
    {
		//children = new String[msgField.size()];   //here!here!here!
		//int size = msgField.size()+sub.length;
		int index=0;
		String field = null;
		//occurs次數由TITATable是否抓得到occur欄位來控制
		boolean occurEnd = false;
		for(int c=0; c<LoopTimes&&!occurEnd ; c++)
		{
			for(index=0; index<msgField.size(); index++)
			{
//				System.out.println("index = " + index);
				boolean transfertype ;
				String fieldName = (String)msgField.get(index);
//              System.out.println("fieldName = " + fieldName);
				byte []totaElement=null;
				Object tempObj=null;
				if(TOTAByteRecord==null || (tempObj=TOTAByteRecord.get(fieldName)) ==null)
				{
					field = LoopTimes==1 ? fieldName : fieldName+"_"+c; 
					TelegramData t = (TelegramData)template.get(field);
					String userValue = (String)TITATable.get(field);
//				System.out.println("@@getTITAFields field=" + field + ",userValue="+userValue+",t.name="+t.name);
					if( userValue==null || userValue.equals(""))
					{
						userValue = t.value;
						transfertype = true;
//					System.out.println("t.value="+t.value+",transfertype="+transfertype+",t.name="+t.name);
					}
					else
					{
						transfertype = false;
//					System.out.println("userValue="+userValue+",transfertype="+transfertype+",t.name="+t.name);
					}
					if(t==null)	//找不到fieldname_c表示client沒有此筆資料傳入
					{
						occurEnd = true;
						break;
					}
				
				//從Hashtable中取得欄位值填入byte[]中
				//現在因為pos已做為參數傳入,而不再是instance variable,所以pos在這邊也得累加
				//這樣下面call trueObject.getTITABytes()時才有正確的pos    05.8.2
				//如果還要用instance variable的話,就要用synchronized(currentPos)了
					if(!fieldName.equals("FILLER4BYTES"))
					{
						if(packTITAElement(userValue, t , pos, msg, clientEncoding, transfertype)!=0)
							return(0);
					}
					else if(titaID.equals("1030") || titaID.equals("1700"))
					{
//					System.out.println("1030 transaction .....................");
						byte []b1030 = { 0x42,0 ,0x10,0};   //因為還不知道這4bytes的值是怎麼來的,所以只好先塞值
						System.arraycopy(b1030,0,msg,pos,t.length);
					}
//				System.out.println("Post packTITAElement, userValue="+userValue+",msg="+(new String(msg)).trim());
					positionMapping.put(t.name, new Integer(pos));
					if(t.type.equals("nospace") || t.type.equals("NOSPACE"))
						pos = pos+userValue.trim().length();
					else
						pos = pos+t.length;
				}
				else      //折返    06.06.24
				{
					totaElement = (byte [])tempObj;
					TelegramData t = (TelegramData)template.get(fieldName);
					String type = t.type;
					if (type.equals("LSEQNO") || type.equals("lseqno"))
					{
						String SSeqno="";
						byte []tmpbuf=ConvCode.host2pc(totaElement);
						String aaa=new String(tmpbuf);
						MyDebug.debprt("ReSEND aaa=" + aaa);
						if(aaa.length()==0)
							SSeqno="0";
						else
							SSeqno=aaa; 
						int a=new Integer(SSeqno)+1;
						SSeqno=StringUtil.repeat("0", t.length) + a;
						totaElement=SSeqno.substring(SSeqno.length()-t.length).getBytes();
						log.trace("ReSEND LSEQNO>>{}" , new String(totaElement));
						System.arraycopy(ConvCode.pc2host(totaElement),0,msg,pos,t.length);
					}
					else
					{
						System.arraycopy(totaElement,0,msg,pos,t.length);
					}
					pos = pos+t.length;
				}
//				System.out.println("getTITAFields.pos="+pos+",fieldName="+fieldName);
			}//for(index=0; index<msgField.size(); index++)
		}//for(int c=0; c<LoopTimes&&!occurEnd ; c++)
		return pos;
    } //getTITAFields end
    
	public int getTITABytes(TelegramDataList template, Vector input, byte []msg, 
								int pos, Hashtable TOTAByteRecord, Hashtable positionMapping) throws CommunicationException
	{
	    Hashtable TITATable = (Hashtable)input.get(0);
		log.trace("input.get(0)>>{}",ESAPIUtil.vaildLog(CodeUtil.toJson(TITATable))  );
		return getTITABytes(template, TITATable, msg, pos, TOTAByteRecord, positionMapping, 1);
	}
    
	
	
//	Incorrect Permission Assignment For Critical Resources
//    public int getTITAXML_test(TelegramDataList template, Vector input)
//    {
//        
//        try
//        {
//            XMLDataTITA format = new XMLDataTITA();
//            format.setCurrentDetail(input);
//            getTITAXML(template,input,null,null,format);
//            Document rootdoc = format.getDocument();
//            
//            TransformerFactory xformFactory = TransformerFactory.newInstance();  
//            Transformer idTransform = xformFactory.newTransformer();
//            Source inputSource = new DOMSource(rootdoc);
//            FileOutputStream f = new FileOutputStream("140.xml");
//            Result output = new StreamResult(f);
//            idTransform.transform(inputSource, output);
//
//        }
//        catch(Exception e)
//        {
//        	log.error(e.toString());
//        }
//        return 0;
//    }
    
    private int getTITAXML(TelegramDataList template, Vector input, Node current, 
                                            Document tita,XMLDataTITA format) throws Exception
    {
        int end=0;
     
        if( flowtype.equals("開始TOTA")||flowtype.equals("開始TITA") )
        {
            return trueObject.getTITAXML(template,input,current,tita,format);
        }
        else if(flowtype.equals("電文欄位"))
        {
            int []r;
            String fname=null;
            
            if(format.getRootNode()==null)  //ROOT電文欄位,要create XML document
            {
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                //factory.setNamespaceAware(true);
                DocumentBuilder builder = factory.newDocumentBuilder();
                DOMImplementation impl = builder.getDOMImplementation();
                DocumentType doctype = impl.createDocumentType("REQUESTDATA", "", "");
                Document doc = impl.createDocument("", (String)msgField.get(0) , doctype);
                format.setDocument(doc);
                Node rootElement = doc.getDocumentElement();
                Comment comment = doc.createComment("XML TITA");
                doc.insertBefore(comment, rootElement);
                format.setRootNode(rootElement);
                format.setCurrentDetail(input);
                return branchObject[0].getTITAXML(template,input,rootElement,doc,format);
            }
            else
            {
                if(branch.size()>0 && STREAM_TYPE.equals("XML"))
                {   //有sub flowobject,then do nothing, bypass
                    if(msgField.size()>1)
                    {
                    	MyDebug.errprt("XML格式定義語法錯誤:定義complex type的電文欄位不可以有兩個以上的tag name");
                        return 0;
                    }
//					MyDebug.debprt("電文欄位 node:"+msgField);
                    String field = (String)msgField.get(0);
                    Hashtable master = format.getCurrentMaster();
                    Node node = tita.createElementNS("", field);
                    current.appendChild(node);

                    for(int i=0; i<branchObject.length; i++)
                    {   //如果該branchObject[j]是電文陣列的話,可以先取出其Vector放入format中
                        if(branchObject[i].getFlowObjectType().equals("電文陣列"))
                        {
                            Vector v = branchObject[i].getMsgFields();
                            if(v.size()>1)
                            	MyDebug.errprt("電文陣列flowObject不該有兩個以上的field name:"+v);
                            String subDetailName = (String)v.get(0);
                            Vector subDetail = (Vector)master.get(subDetailName);
                            format.setCurrentDetail(subDetail);
                            format.setCurrentMaster(null);
                            branchObject[i].getTITAXML(template,input,node,tita,format);
                        }
                        else    //branchObject[j] is 電文欄位
                        {
                            format.setCurrentMaster(master);
                            branchObject[i].getTITAXML(template,input,node,tita,format);
                        }
                    }
                }
                else if(branch.size()==0 && STREAM_TYPE.equals("XML") )     // for XML data 07.03.30
                {   //當branch.size()==0時表示此XML node已不是complex node,可取其value
                    Hashtable master = format.getCurrentMaster(); 
                    String str = (String)msgField.get(0);
                    for(int i=0; i<msgField.size(); i++)
                    {
                        String field = (String)msgField.get(i);
                        String value = (String)master.get(field);
                        if(value==null) value = "";

                        Node node = tita.createElementNS("", field);
                        Text nodeText = tita.createTextNode(value);
                        node.appendChild(nodeText);
                        current.appendChild(node);
                    }
//					System.out.println("電文欄位 leaf node:"+msgField);
                }
            }
        }
        else if(flowtype.equals("電文陣列"))
        {   //如果是upperDetail,則不用從master中get,反之若不是upperDetail,detail=master.get(fieldname)
            Vector upperDetail = format.getCurrentDetail(),detail = null;
            if(upperDetail.size()>0)
            {
//				System.out.println("電文陣列:"+msgField+",upperDetail.size()="+upperDetail.size()+",branchObject[]="+branchObject.length);
                for(int i=0; i<upperDetail.size(); i++)
                {
                    Hashtable master = (Hashtable)upperDetail.get(i);
                    format.setCurrentMaster(master);
                    String field = (String)msgField.get(0);
//					System.out.println("upperDetail("+i+"),size="+upperDetail.size()+",fieldname="+field);
                    Object o = master.get(field);
                    if(o!=null) //o==null=>detail已是最外層的Vector,沒有master
                    {
//						System.out.println("we got detail:"+field+",detail="+(Vector)o);
                        detail = (Vector)o;
                    }
                    else
                        detail = upperDetail;
                    format.setCurrentDetail(detail);

                    Node node = tita.createElementNS("", field);
                    current.appendChild(node);
//					System.out.println("detail["+i+"]"+",detail.size()="+detail.size());
                    for(int j=0; j<branchObject.length; j++)
                    {   //如果該branchObject[j]是電文陣列的話,可以先取出其Vector放入format中
                        if(branchObject[j].getFlowObjectType().equals("電文陣列"))
                        {
                            Vector v = branchObject[j].getMsgFields();
                            if(v.size()>1)
                            	;
                            	// System.out.println("電文陣列flowObject不該有兩個以上的field name:"+v);
                            String subDetailName = (String)v.get(0);
                            Vector subDetail = (Vector)master.get(subDetailName);
                            format.setCurrentDetail(subDetail);
                            format.setCurrentMaster(null);
                            branchObject[j].getTITAXML(template,input,node,tita,format);
                        }
                        else    //branchObject[j] is 電文欄位
                        {
                            format.setCurrentMaster(master);
                            branchObject[j].getTITAXML(template,input,node,tita,format);
                        }
                    }
                }
            }
            else    //雖然沒有資料,但還是可以create node
            {
//				System.out.println("電文陣列:"+msgField+"(no data)");
                String field = (String)msgField.get(0);
                Node node = tita.createElementNS("", field);
                current.appendChild(node);
            }
        }
        else if(flowtype.equals("結束TOTA") || flowtype.equals("結束TITA"))
        {
        	MyDebug.errprt("XML結束了,但不應該出現在這裡,XML data stream沒有結束TAG");
        }
        else
        	MyDebug.errprt("電文定義XML格式錯誤:找不到正確的FlowObject");

     
        
        return end;
    }
	private int getTITABytes(TelegramDataList template,Hashtable TITATable,byte []msg,int pos) throws CommunicationException
	{
		return getTITABytes(template, TITATable, msg, pos, null, null, 1);
	}
    /**
     *根據欄位的屬性及要求,將欄位輸入值轉為byte array
     *@param userValue 代表上行電文中某欄位的欄位值
     *@param telegramData 對應該欄位之bank.comm.TelegramData物件
     *@param clientEncoding EAI平台編碼方式
     *@param ASCIIBuffer 存放上行電文之字串(ASCII編碼)
     *@param transfertype 欄位值是否使用telegramData中的預設值(若client端沒有值,則使用default)
          				若使用user登打的字串,則須多經過fitInTITAFormat處理,這是唯一不同
      *@return 欄位輸入值之byte array
     *@throws CommunicationException 當轉換失敗時,丟出CommunicationException
     */
    private int packTITAElement(String userValue, TelegramData telegramData, int pos, 
    				byte []msg, String clientEncoding, boolean transfertype) throws CommunicationException
    {
    	int retcode=0;
    	String name = telegramData.name;
		String type = telegramData.type;
		String optional = telegramData.optional;
       	String refval = telegramData.refval;
        String format = telegramData.format;
   	    String encoding = telegramData.encoding;
       	String transfer = telegramData.transfer;
        int length = telegramData.length;
        byte[] out=null;
        
        try
        {
        	log.trace("TelegramData.refval>>{} , userValue >>{},optional>>{}",ESAPIUtil.vaildLog( telegramData.refval), ESAPIUtil.vaildLog(  userValue),ESAPIUtil.vaildLog( telegramData.optional));
            
        	log.trace("big5>>{} , utf-8>>{} , bbig5>>{}" , ESAPIUtil.vaildLog(new String(userValue.getBytes("Big5"))) , ESAPIUtil.vaildLog(new String(userValue.getBytes("UTF-8"))) ,ESAPIUtil.vaildLog( new String(userValue.getBytes("Big5"),"Big5")));
        	
        	byte []a = new String(userValue.getBytes("Big5"),"Big5").getBytes("Big5");
        	log.trace("vvv>>{}",ESAPIUtil.vaildLog(new String(a)));
        	byte []b = userValue.getBytes("Big5");
        	log.trace("bbv>>{}",ESAPIUtil.vaildLog(new String(b,"Big5")));
        	if(b.length<telegramData.length)
        		userValue = fillStringLength(b,telegramData.length);
        	log.trace("bbv2>>{}" ,ESAPIUtil.vaildLog(new String(b,"Big5")) );
        	if(!optional.equals(""))
        	{
   	    		userValue = userValue.equals(optional)?refval:userValue;
        	}
        	if((!format.equals(""))&&(!userValue.equals("")))
        	{
   		        //userValue = deformat(userValue,format);                    
        	}
        	if((!encoding.equals(clientEncoding))&&(!encoding.equals(""))&&(!userValue.equals("")))
        	{
       	    //userValue = convert(userValue,encoding);	
        	}
        	log.trace("TelegramData.name>>{} , userValue >>{},transfertype>>{}",ESAPIUtil.vaildLog(telegramData.name) ,ESAPIUtil.vaildLog(userValue),transfertype);

        	if(type.equals("X") || type.equals("x") || type.equals("XR") || type.equals("xr") ||
        			type.equalsIgnoreCase("NTRIM") || type.equalsIgnoreCase("ntrim"))
        	{
        		log.trace("name >>{} ,field length>>{} ,b ={} ,b length>>{}",ESAPIUtil.vaildLog(name)  ,length,ESAPIUtil.vaildLog(new String(b)), b.length); 
        		int chklen=chklength(length,b,b.length);
        		if(chklen==0)
        		{
//        			MyDebug.errprt("Z012:" + name.trim() + ":call chklength func: 輸入資料最大長度大餘欄位最大長度");
//        			this.ErrCod="Z012";
//        			this.ErrField=name.trim();
//        			this.ErrText="call chklength func: 輸入資料最大長度大餘欄位最大長度";
//        			return(1);
        			log.error("name >>{} ,field length>>{} ,b ={} ,b length>>{}",ESAPIUtil.vaildLog(name)  ,length,ESAPIUtil.vaildLog(new String(b)), b.length);
        			throw new CommunicationException("M1", "Z012","");
        		}
        	}
        	byte[] tempbyte = fitInTITAFormat(length, type, userValue);
        	log.trace("tempbyte>>{} , tempbyte.string>>{}",tempbyte , new String(tempbyte,"big5"));
        	if((new String(tempbyte)).equals("ERROR"))
        	{
//        		MyDebug.errprt("fitInTITAFormat 欄位FORMAT錯誤");
//            	this.ErrCod="Z010";
//            	this.ErrField=name.trim();
//            	this.ErrText="call fitInTITAFormat func: 欄位FORMAT錯誤";
//        		return(1);
        		throw new CommunicationException("M1", "Z010","");
        	}
//        	byte[] tmpaddlead=new byte[length];
        	if(transfertype==true)
        	{	//transfertype==true,byte array需要經過fitInTITAFormat處理
        		if(isTitaTransfer && transfer.equalsIgnoreCase("YES"))
        		{
				/*
				byte[] tempbyte = new byte[length];
				byte[] tempbyte1 = fitInTITAFormat(length, type, userValue); */
        			if(type.equalsIgnoreCase("X+"))	// X+ type是中文字
        			{	
        				
        				System.arraycopy(purgeOutMarker(ConvCode.pc2host(getConvertedCode(tempbyte))),0,msg,pos,length); 
        			}
        			else if(type.equalsIgnoreCase("NOSPACE") || type.equalsIgnoreCase("nospace"))
        			{
        				out=ConvCode.pc2host(tempbyte);
        				for(int i=0;i<out.length;i++)
        				{
        					if(out[i]==(byte)0x15)
        					{
        						out[i]=(byte)0x25;
        					}
        				}

        				System.arraycopy(out,0,msg,pos,out.length);
        			}
        			else
        			{
        				if(type.equalsIgnoreCase("XR"))
        				{
        					byte[] tmpcnv=ConvCode.pc2host(tempbyte);
        	                int diff = length-tmpcnv.length;
        	                byte[] offset = new byte[diff];
        	                for (int i=0; i<diff; i++)
        	                	offset[i]=' ';
        					System.arraycopy(tmpcnv,0,msg,pos,tmpcnv.length);
        					System.arraycopy(offset,0,msg,pos + tmpcnv.length ,diff);
        				}
        				else
        				{
        					System.arraycopy(ConvCode.pc2host(tempbyte),0,msg,pos,length);
        				}
        			}
        		}
        		else 	//transfer.equals("NO")
        		{	
/* 	            byte []tempbyte = fitInTITAFormat(length, type, userValue); */
//				System.out.println("no problem telegramData="+telegramData+",userValue="+userValue+",length="+length+",pos="+pos+",tempbyte.length="+tempbyte.length);
        			if(type.equalsIgnoreCase("NOSPACE") || type.equalsIgnoreCase("nospace"))
        			{
        				System.arraycopy(tempbyte,0,msg,pos,tempbyte.length);
        			}
        			else
        			{
        				System.arraycopy(tempbyte,0,msg,pos,telegramData.length);
        			}
        		}
        	}//transfertype==true end
        	else	
        	{	//transfertype==false
        		if(isTitaTransfer && transfer.equalsIgnoreCase("YES"))
        		{
        			if(type.equalsIgnoreCase("X+"))
        			{
/*					System.arraycopy(purgeOutMarker(pc2host(getConvertedCode(userValue.getBytes()))),0,msg,pos,telegramData.length);     */
        				System.arraycopy(purgeOutMarker(ConvCode.pc2host(getConvertedCode(tempbyte))),0,msg,pos,telegramData.length);    
        			}
        			else if(type.equalsIgnoreCase("NOSPACE") || type.equalsIgnoreCase("nospace"))
        			{
        				out=ConvCode.pc2host(tempbyte);
        				for(int i=0;i<out.length;i++)
        				{
        					if(out[i]==(byte)0x15)
        					{
        						out[i]=(byte)0x25;
        					}
        				}

        				System.arraycopy(out,0,msg,pos,out.length);
        			}
        			else
        			{
        				if(type.equalsIgnoreCase("XR"))
        				{
        					byte[] tmpcnv=ConvCode.pc2host(tempbyte);
        	                int diff = length-tmpcnv.length;
        	                byte[] offset = new byte[diff];
        	                for (int i=0; i<diff; i++)
        	                	offset[i]=' ';
        					System.arraycopy(tmpcnv,0,msg,pos,tmpcnv.length);
        					System.arraycopy(offset,0,msg,pos + tmpcnv.length ,diff);
        				}
        				else
        				{
        					tempbyte = ConvCode.pc2host(tempbyte);
        					log.trace("tempbyte.length>>{},msg.length>>{},pos>>{},length>>{}",tempbyte.length,msg.length,pos,length);
        					System.arraycopy(tempbyte,0,msg,pos,length);
//        					System.arraycopy(ConvCode.pc2host(tempbyte),0,msg,pos,length);
        				}
        			}
        		} 
        		else 	//transfer.equals("NO")
        		{	
/*   	            byte []tempbyte = fitInTITAFormat(length, type, userValue); */
//				System.out.println("no problem telegramData="+telegramData+",userValue="+userValue+",length="+length+",pos="+pos+",tempbyte.length="+tempbyte.length);
        			if(type.equalsIgnoreCase("NOSPACE") || type.equalsIgnoreCase("nospace"))
        			{
        				System.arraycopy(tempbyte,0,msg,pos,tempbyte.length);
        			}
        			else
        			{
        				System.arraycopy(tempbyte,0,msg,pos,telegramData.length);
        			}
        		}//transfer.equals("NO")
        	}//transfertype==false  end
        	return(0);
        }
        catch(CommunicationException ce)
        {
        	log.error("",ce);
        	throw ce;
        }
        catch(Exception e)
        {
//        	MyDebug.errprt("Z010 : " + name.trim() + " : call packTITAElement func : " + e.toString());
//        	this.ErrCod="Z010";
//        	this.ErrField=name.trim();
//        	this.ErrText="call packTITAElement func : " + e.toString();
//        	return(1);
        	throw new CommunicationException("M1", "Z010","");
        }
   	}//packTITAElement

	private void packTOTAByteElement(String userValue, TelegramData telegramData, int pos, 
    				byte []msg, String clientEncoding, boolean transfertype) throws CommunicationException
    {
		String name = telegramData.name;
		String type = telegramData.type;
		String optional = telegramData.optional;
       	String refval = telegramData.refval;
        String format = telegramData.format;
   	    String encoding = telegramData.encoding;
       	String transfer = telegramData.transfer;
        int length = telegramData.length;
        
        try
        {
        	if(!optional.equals(""))
        	{
        		userValue = userValue.equals(optional)?refval:userValue;
        	}
        	if((!format.equals(""))&&(!userValue.equals("")))
        	{
   		        //userValue = deformat(userValue,format);                    
        	}
        	if((!encoding.equals(clientEncoding))&&(!encoding.equals(""))&&(!userValue.equals("")))
        	{
       	    //userValue = convert(userValue,encoding);	
        	}
//		System.out.println("TelegramData.name="+telegramData.name+",userValue="+userValue+",transfertype="+transfertype);
        	if(transfertype==true)
        	{	//transfertype==true,byte array需要經過fitInTITAFormat處理
        		if(transfer.equalsIgnoreCase("YES"))
        		{	
        			byte[] tempbyte = new byte[length];
        			byte[] tempbyte1 = fitInTITAFormat(length, type, userValue);
        			if(type.equalsIgnoreCase("X+"))	// X+ type是中文字
        			{	
        				log.trace("tempbyte1>>{}",tempbyte1);
        				System.arraycopy(purgeOutMarker(ConvCode.pc2host(getConvertedCode(tempbyte1))),0,msg,pos,length); 
        			}
        			else
        			{	
        				log.trace("tempbyte1-1>>{}",tempbyte1);
        				System.arraycopy(ConvCode.pc2host(tempbyte1),0,msg,pos,length);
        			}
        		}
        		else 	//transfer.equals("NO")
        		{	
        			byte []rrr = fitInTOTAByteFormat(length, type, userValue);
//				System.out.println("rrr[].length="+rrr.length);
        			System.arraycopy(rrr,0,msg,pos,telegramData.length);
        		}
        	}//transfertype==true end
        	else	
        	{	//transfertype==false
        		if(transfer.equalsIgnoreCase("YES"))
        		{
        			if(type.equalsIgnoreCase("X+"))
        			{
        				System.arraycopy(purgeOutMarker(ConvCode.pc2host(getConvertedCode(userValue.getBytes()))),0,msg,pos,telegramData.length);    
        			}
        			else
        			{
        				System.arraycopy(ConvCode.pc2host(userValue.getBytes()),0,msg,pos,telegramData.length);
        			}
        		} 
        		else 	//transfer.equals("NO")
        		{	
        			if(!telegramData.type.equalsIgnoreCase("H") )
        				System.arraycopy(userValue.getBytes(),0,msg,pos,telegramData.length);
        			else
        			{
        				byte []rrr = fitInTOTAByteFormat(length, type, userValue);
//					System.out.println("rrr[].length="+rrr.length+",telegramData.length="+telegramData.length);
        				System.arraycopy(rrr,0,msg,pos,rrr.length);
        			}
//				System.out.println("tempbyte.length="+tempbyte.length+"tempbyte="+new String(tempbyte));
        		}//transfer.equals("NO")
        	}//transfertype==false  end
        }
        catch(Exception e)
        {
//        	MyDebug.errprt("Z010 : " + name.trim() + " : call packTOTAByteElement func : " + e.toString());
//        	this.ErrCod="Z010";
//        	this.ErrField=name.trim();
//        	this.ErrText="call packTOTAByteElement func error : " + e.toString();
        	throw new CommunicationException("M1", "Z010","");
        }
    }//packTOTAByteElement

    private String fillStringLength(byte []b,int length)
    {
    	String s = "";
        byte []result = new byte[length];
        System.arraycopy(b,0,result,0,b.length);
        try {
			s= new String(result,"BIG5");
		} catch (UnsupportedEncodingException e) {
			log.error(e.toString());
		}
        return s ;
    }
	
	//以下的methods是解析XML定義用的,上面的methods是電文bytes array進來後,計算有那些欄位
	//並讀取(TOTA)、組合(TITA,outbound TOTA)
	
	
	
	private void createFlowStructure(Node node, Hashtable flowobjects,String maxrecode,Hashtable ExVar)
	{
		if(node==null) return ;		// should throw Exception
		
		NamedNodeMap attrs = node.getAttributes();
		getSubFlowObectID(attrs);		//setup  FLOWOBJECT   'attributes'
//		System.out.println("type="+flowtype+ " id="+flowid+",trueid="+trueid+",falseid="+falseid);
		if(flowtype.equals("開始TOTA") || flowtype.equals("開始TITA"))
			trueObject = new FlowObject(flowobjects,trueid,messageType,titaID,STREAM_TYPE,maxrecode,ExVar);			//recursive
		else if(flowtype.equals("電文欄位"))
		{
            getItemList(node);
            if(trueid!=null)
            	trueObject = new FlowObject(flowobjects,trueid,messageType,titaID,STREAM_TYPE,maxrecode,ExVar);			//recursive
            else if(branch.size()>0)      //07.03.28 for XML TITA,TOTA stream
            {
                branchObject = new FlowObject[branch.size()];
                for(int i=0; i<branch.size(); i++)
                    branchObject[i] = new FlowObject(flowobjects,(String)branch.get(i),messageType,titaID,STREAM_TYPE,maxrecode,ExVar);    //recursive
            }
        }
        else if(flowtype.equals("條件判斷"))
        {
            Stack operand =new Stack(),operator=new Stack();
        	getIfConditionS(node,operand,operator);			// setup conditionTree(SyntaxTree)
   			conditionTree = new SyntaxTree();
//			System.out.println("operator= "+operator+"\noperand= "+operand);
   			if(operand.size()==2)
        		conditionTree.addRootNode(new TreeNode((String)operator.pop(),operand.pop(),operand.pop()));
        	else if(operand.size()==1&&operator.size()==0)
        		conditionTree.addRootNode(operand.pop());
        	else if(operand.size()==1&&operator.size()==1)
        		conditionTree.addRootNode(new TreeNode((String)operator.pop(),operand.pop()));
        	else
        		MyDebug.errprt("createFlowStructure:Fatal syntax error!");
        	trueObject = new FlowObject(flowobjects,trueid,messageType,titaID,STREAM_TYPE,maxrecode,ExVar);			//recursive
        	if(falseid!=null)	//電文解譯last bug free 7/27 17:20
        		falseObject = new FlowObject(flowobjects,falseid,messageType,titaID,STREAM_TYPE,maxrecode,ExVar);		//recursive
        }
        else if(flowtype.equals("SWITCH"))
        {
        	getSwitchValue(node);			// setup _switch(ItemValue)
        	switchObject = new FlowObject[switchID.size()];
        	for(int i =0; i<switchID.size(); i++)
        		switchObject[i] = new FlowObject(flowobjects,(String)switchID.get(i),messageType,titaID,STREAM_TYPE,maxrecode,ExVar);			//recursive
        }
        else if(flowtype.equals("電文陣列"))
        {
        	getLoopContent(node);
        	getItemList(node);
        	if(trueid!=null)
        	   trueObject = new FlowObject(flowobjects,trueid,messageType,titaID,STREAM_TYPE,maxrecode,ExVar);  //recursive
            else if(branch.size()>0)      //07.03.28 for XML TITA,TOTA stream
            {
                branchObject = new FlowObject[branch.size()];
                for(int i=0; i<branch.size(); i++)
                    branchObject[i] = new FlowObject(flowobjects,(String)branch.get(i),messageType,titaID,STREAM_TYPE,maxrecode,ExVar);    //recursive
            }
        }
        else if(flowtype.equals("折返"))
        {
/*
        	getItemList(node);
			trueObject = null;
*/
        	try {
        		getItemList(node);
        	}
        	catch(Exception e)
        	{
        	}
        	trueObject = null;
        }
        else if(flowtype.equals("結束TOTA") || flowtype.equals("結束TITA"))
        	trueObject = null;
	}// end createFlowStructure
	
	private void getItemList(Node node)
	{
		NodeList nodes = node.getChildNodes();
        for(int i=0; i<nodes.getLength(); i++)
        {
	    	Node node0 = nodes.item(i);
	        if(node0.getNodeType() == Node.ELEMENT_NODE)
    	    {
        		if(node0.getNodeName().equals("ITEMLIST"))
        			getTemplateItems(node0);
        	}
        }
    } //getItemList 
	
	private void getTemplateItems(Node node)
	{
    	String items = node.getFirstChild().getNodeValue();
      	StringTokenizer st = new StringTokenizer(items,",");
        while(st.hasMoreTokens())
        	msgField.add(st.nextToken().trim());
    } //getTemplateItems

	private void getIfConditionS(Node node,Stack operand,Stack operator)
	{	//碰到operator就到operand Stack pop兩個element出來construct一個TreeNode,
		//並把此node push回stack. 如果stack中只剩一個element,則要將此operator先push
		//進operator Stack,等下一次碰到operator或right paraenthsis時再pop出來建TreeNode
		NodeList nodes = node.getChildNodes();
        for(int i=0; i<nodes.getLength(); i++)
        {
	    	Node node0 = nodes.item(i);
	        if(node0.getNodeType() == Node.ELEMENT_NODE)
    	    {
        		if(node0.getNodeName().equals("CONDITIONS"))
        			getIfCondition(node0,operand,operator);
        	}
        }
    } // getIfConditions

	//由XML node取得Condition下的定義 (將condition statement轉成syntax tree便於運算)
	private void getIfCondition(Node node0,Stack operand,Stack operator)
	{	//碰到operator就到operand Stack pop兩個element出來construct一個TreeNode,
		//並把此node push回stack. 如果stack中只剩一個element,則要將此operator先push
		//進operator Stack,等下一次碰到operator或right paraenthsis時再pop出來建TreeNode
        NodeList nodes0 = node0.getChildNodes();
		for(int k=0; k<nodes0.getLength(); k++)
        {
			Node node1 = nodes0.item(k);
	    	if(node1.getNodeType() == Node.ELEMENT_NODE)
		    {
	        	if(node1.getNodeName().equals("CONDITION"))
		  	    {
					NamedNodeMap attrs = node1.getAttributes();
					String unary = getUnaryOperator(attrs);
			       	Item left=null,right=null;
    	   			String comp=null,logic=null;
        			NodeList nodes1 = node1.getChildNodes();
		    	    for(int i1=0; i1<nodes1.getLength(); i1++)
        			{
        				Node node2 = nodes1.item(i1);
        				if(node2.getNodeType() == Node.ELEMENT_NODE)
		        		{
        					if(node2.getNodeName().equals("LEFTVALUE"))
        						left = getValue(node2,messageType);
	        				else if(node2.getNodeName().equals("RIGHTVALUE"))
    	    					right = getValue(node2,messageType);
			        		else if(node2.getNodeName().equals("COMPARE"))
        						comp = node2.getFirstChild().getNodeValue();
        					else if(node2.getNodeName().equals("LOGIC"))
        						logic = node2.getFirstChild().getNodeValue();
		        		} // end <LEFTVALUE>,<RIGHTVALUE>,<COMPARE>,<LOGIC>
        			} // end for each CONDITION children
        			
			        operand.push(new Condition(left,comp,right,unary));
			        String op =null;
        			//再考慮一下:下面應該用if還是while??
			        if(operator.size()>0 && logic!=null && 
        					needStackPop(op=(String)operator.pop(),logic))	//如果op和logic之priority相同
        				operand.push(new TreeNode(op,operand.pop(),operand.pop()));
			        else if(op!=null)
        				operator.push(op);
			        if(logic!=null)	operator.push(logic);
         		} //end <CONDITION>
		       	else if(node1.getNodeName().equals("QUOTES"))
			    {
					String op=null;
					NamedNodeMap attrs = node1.getAttributes();
					String unary = getUnaryOperator(attrs);
					if(unary!=null)	operator.push(unary);
						operator.push("(");
					getIfCondition(node1,operand,operator);  // recursive	<QUOTES>下又是<CONDITION>
					while(!(op=(String)operator.pop()).equals("("))
						operand.push(new TreeNode(op,operand.pop(),operand.pop()));
					checkUnaryOperator(operand, operator);
					getQuoteOperator(node1,operator);
				} //end <QUOTES>
			} //end if node1 == Node.ELEMENT_NODE
		} //end foreach nodes0
	}//end getIfCondition()
	
	private void checkUnaryOperator(Stack operand, Stack operator)
	{
		String op = null;
		if(operator.size()>0)
		{
			if((op=(String)operator.pop())!=null &&!(op.equals("NOT")) )
				operator.push(op);
			else
				operand.push(new TreeNode(op,operand.pop()));
		}
	}
	
	//取得<CONDITION>或<QUOTES>的屬性定義 (NOT會被寫在這裡)
	private String getUnaryOperator(NamedNodeMap attrs)
	{
		for(int i=0; i<attrs.getLength(); i++)
		{
			Node attrNode = attrs.item(i);
			if(attrNode.getNodeName().equals("UNARYOPERATOR"))
        		return attrNode.getNodeValue();
       	}
       	return null;
	
	}//getUnaryOperator
	//unary operator完成 8/11 14:50
	
	private Item getValue(Node node,String messageType)
	{
		NodeList nodes = node.getChildNodes();
		if(nodes.getLength()>1)
		{
			for(int i=0; i<nodes.getLength(); i++)
			{
				Node node1 = nodes.item(i);
				if( node1.getNodeName().equals("ITEMVALUE") )
					return new ItemValue(node1,messageType) ;
				else if(node1.getNodeName().equals("STRING") )
				{
					return new ImmediateValue(node1);
				}
			}
		}
		else
			return new ImmediateValue(node);
	
		return null;
	}

	private boolean needStackPop(String oldOP, String newOP)
	{	// 如果newOP.priority>oldOP.priority,則不需pop operand stack
		// )						高	(遇右括弧要一直pop碰到左括弧)
		// and , or, not			低
		// (						(不需比較)
		int oldp=0,newp=0;
		if(oldOP.equals("AND") || oldOP.equals("OR") ||oldOP.equals("NOT") )
			oldp = 1;
		else if(oldOP.equals(")"))
			oldp = 5;
		if(newOP.equals("AND") || newOP.equals("OR") ||newOP.equals("NOT") )
			newp = 1;
		else if(newOP.equals(")"))
			newp = 5;
		
		if(newp > oldp)	return false;
		else			return true;
	}
	
	//這是為了取得右括號後面的邏輯運算子,如果有就將其push入operator Stack
	private void getQuoteOperator(Node node, Stack operator)
	{
		NodeList nodes = node.getChildNodes();
        for(int i=0; i<nodes.getLength(); i++)
        {
	    	Node node1 = nodes.item(i);
	        if(node1.getNodeType() == Node.ELEMENT_NODE)
    	    {
        		if(node1.getNodeName().equals("LOGIC"))
        		{
        			String op = node1.getFirstChild().getNodeValue();
        			operator.push(op);
        		}
        	}
        }
    }
	
	private void getLoopContent(Node node)
	{
		NodeList nodes = node.getChildNodes();
		OccursName = flowid;
        for(int i=0; i<nodes.getLength(); i++)
        {
	    	Node node1 = nodes.item(i);
	        if(node1.getNodeType() == Node.ELEMENT_NODE)
    	    {
        		if(node1.getNodeName().equals("LENGTH"))
        		{
        			occursLengthItemValue = new ItemValue(node1,"parent",messageType);
        			if(!occursLengthItemValue.hasValue())
                        occursLengthItemValue=null;
        		}
        		else if(node1.getNodeName().equals("OFFSET"))
        		{	//如果不是立即值,只要把這裡改成和上面LENGTH一樣的寫法就行了
        			OccursOffset = getValue(node1,messageType).toString();  //= node1.getFirstChild().getNodeValue();
        		}
        		else if(node1.getNodeName().equals("TIMES"))
        		{	//如果不是立即值,只要把這裡改成和上面LENGTH一樣的寫法就行了
        			OccursTimes = getValue(node1,messageType).toString();   //node1.getFirstChild().getNodeValue();
        		}
        		/*else if(node1.getNodeName().equals("NAME"))
        		{
                    OccursName = getValue(node1,messageType).toString();
                }*/
			}//end if ELEMENT
		}//end for
	}//getLoopContent

	//由XML node取得switch下的定義 (將SWITCHVALUE,CASEVALUE的ITEMVALUE定義轉成ItemValue物件)
	private void getSwitchValue(Node node)
	{
		NodeList nodes = node.getChildNodes();
        for(int i=0; i<nodes.getLength(); i++)
        {
	    	Node node1 = nodes.item(i);
	        if(node1.getNodeType() == Node.ELEMENT_NODE)
    	    {
        		if(node1.getNodeName().equals("SWITCHVALUE"))
        		{
        			NodeList nodes1 = node1.getChildNodes();
        			for(int i1=0; i<nodes1.getLength(); i1++)
        			{
        				Node node2 = nodes1.item(i1);
        				if(node2.getNodeType() == Node.ELEMENT_NODE)
        				{
        					if(node2.getNodeName().equals("ITEMVALUE"))
        						_switch = new ItemValue(node2,messageType);		// ***主要就是new此物件
        				}
        			}// end for each nodes1
        		}// end if "SWITCHVALUE"
        		else if(node1.getNodeName().equals("CASEVALUE") ||
        					node1.getNodeName().equals("DEFAULT"))
        		{	//取得CASEVALUE的attributes
        			NamedNodeMap attrs = node1.getAttributes();
					for(int i1=0; i1<attrs.getLength(); i1++)
					{
						Node attrNode = attrs.item(i1);
			            if(attrNode.getNodeName().equals("TRUEID"))
        					switchID.add( attrNode.getNodeValue() );
        			}
        			//取得CASEVALUE的value (child node)
        			NodeList nodes1 = node1.getChildNodes();
        			for(int i1=0; i1<nodes1.getLength(); i1++)	//(如果是default,此loop應不會跑
        			{
        				Node node2 = nodes1.item(i1);
        				if(node2.getNodeType() == Node.ELEMENT_NODE)
        				{	//已假設ItemValue class能處理immediate value
        					if(node2.getNodeName().equals("ITEMVALUE"))
        						_caseItemValue.add(new ItemValue(node2,messageType));
        					else
        						_caseItemValue.add(node2.getFirstChild().getNodeValue());
        				}
        			}
        		}//end if "CASEVALUE"
        	}//end if node1.getNodeType
        }//end for each nodes
	}//getSwitchValue
	
	// 取得此node的trueid,falseid,switchID String
	private void getSubFlowObectID(NamedNodeMap attrs)
	{
		for(int i=0; i<attrs.getLength(); i++)
		{
			Node attrNode = attrs.item(i);
            if(attrNode.getNodeName().equals("ID"))
            	flowid = attrNode.getNodeValue();
            else if(attrNode.getNodeName().equals("TRUEID"))
            	trueid=attrNode.getNodeValue();
            else if(attrNode.getNodeName().equals("FALSEID"))
            	falseid=attrNode.getNodeValue();
            else if(attrNode.getNodeName().equals("TYPE"))
            	flowtype = attrNode.getNodeValue();
            else if(attrNode.getNodeName().equals("COMMAND"))
                commandtype = attrNode.getNodeValue();
            else if(attrNode.getNodeName().substring(0,6).equals("BRANCH"))
                branch.add(attrNode.getNodeValue());
		}
//		System.out.println("getSubFlowObectID(),id="+flowid+",trueid="+trueid);
	}
	
	//這個method只有在建FlowObject root時會用到,先將所有XML中定義的FlowObject node
	//讀入Hashtable中  (找出FLOWSTART,FLOWSTART是createFlowStructure的start node)
	private Hashtable getFlowObjects(Node node)
	{
		Hashtable flowObjects = new Hashtable();
		NodeList nodes = node.getChildNodes();
        for(int i=0; i<nodes.getLength(); i++)
        {
	    	Node node1 = nodes.item(i);
	        if(node1.getNodeType() == Node.ELEMENT_NODE)
    	    {
        		if(node1.getNodeName().equals("FLOWOBJECT"))
        		{
        			String type=null, id=null , atr=null;
					NamedNodeMap attrs = node1.getAttributes();
                    for(int jj=0; jj<attrs.getLength(); jj++)
                    {	//這個迴圈就是為了取得FLOWOBJECT type是否為FLOWSTART
                    	Node attrNode = attrs.item(jj);
                        if(attrNode.getNodeName().equals("TYPE"))
                        {
                        	atr = attrNode.getNodeValue();
							if(atr.equals("開始TOTA")||atr.equals("開始TITA"))
							{
								type = atr;
								break;
							}
						}
						else if(attrNode.getNodeName().equals("ID"))
							id = attrNode.getNodeValue();
					}
					if(type==null)	 //如果不是FLOWSTART,就用id#代表此FLOWOBJECT
						flowObjects.put(id,node1);
					else		//如果是FLOWOSTART
						flowObjects.put(atr,node1);
				}//end if
			}// end if 
		}// end for
		return flowObjects;
	}// end getFlowObjects
	
	
	
	//以下移植原先DataTemplateX中的methods	
		
	
	
    /**
        * 將傳入之字串轉為上行電文中對應欄位之byte array,並根據資料型態補足至欄位指定長度<br>
        *若資料型態為X -> 左靠右補空白<br>
        *若資料型態為9 -> 右靠左補0<br>
        *若資料型態為H -> 16進位值<br>
        *@param length 欄位指定長度
        *@param type 資料型態
        *@param source 上行電文某欄位之內容,為尚未轉換之字串
        *@return 上行電文中對應欄位之byte array
        */
	static Pattern v99 = Pattern.compile("9V(9+)", Pattern.CASE_INSENSITIVE);
	static Pattern s9 = Pattern.compile("9", Pattern.CASE_INSENSITIVE);
	
    static byte[] fitInTITAFormat(int length, String type, String source)
    {
        byte[] result = "ERROR".getBytes();
        byte[] sourceBytes =null;
        
        
        log.trace("source.getBytes().String>>{}",new String(source.getBytes()));
        
//			if(type.equalsIgnoreCase("NTRIM") || type.equalsIgnoreCase("ntrim"))
//				sourceBytes = source.getBytes();
//			else
//				sourceBytes = source.trim().getBytes();
			
        try {
			if(type.equalsIgnoreCase("NTRIM") || type.equalsIgnoreCase("ntrim"))
				sourceBytes = source.getBytes("big5");
			else
				sourceBytes = source.trim().getBytes("big5");
		} catch (UnsupportedEncodingException e1) {
			log.error("{}",e1);
		}
        
        log.trace("type>>{} , sourceBytes.String>>{}",type,new String(sourceBytes));
        
        
        
        try {
			if (type.equals("9"))
			{ //9 type
				if(chknumber(sourceBytes,sourceBytes.length)==false)
					return(result);
				MyDebug.debprt("9 type sourceBytes="+new String(sourceBytes)+",sourceBytes.length="+ sourceBytes.length + ",length=" + length);
			    if (sourceBytes.length==length) {
			        result = sourceBytes;
			    } else if (sourceBytes.length > length) {
			        result = ArrayTool.subarray(sourceBytes, sourceBytes.length-length, length);
			    } else {
			        int diff = length-sourceBytes.length;
			        char[] offset = new char [diff];
			        for (int i=0; i<diff; i++) offset[i]='0';
			        result = (new String(offset)+new String(sourceBytes)).getBytes();
			    }
			    // System.out.println("9 type result.length="+result.length+",result="+ new String(result) +"|");
			}
			else if(type.equals("S9") || type.equals("9S") || type.equals("+9") || type.equals("+S")) {
				String ret = "";
				String target="";
				Matcher m;
				m = s9.matcher(type);
				String xx=new String(sourceBytes);
				xx=xx.replaceAll("\\$", "");
				String yy=xx.toString();
				yy=yy.replaceAll("\\,", "");
				String zz = yy.trim();
				if(zz.length()==0)
					zz="0";
				if(StringUtil.right(zz,1).equals("+") || 
						StringUtil.right(zz,1).equals("-"))
				{
					target=StringUtil.right(zz,1) + StringUtil.left(zz,zz.length()-1);
				}
				else
				{
					target=zz;
				}
				BigDecimal num = new BigDecimal(target.trim());
				String flag = (num.doubleValue() >= 0 ? " " : "-");
				String flag_plus = (num.doubleValue() >= 0 ? "+" : "-");
				num = num.abs();
				NumberFormat fmt = new DecimalFormat("0");
				String x = StringUtil.repeat("0", length) + fmt.format(num);
				x = x.replaceAll("\\.", "");
				String after_src = StringUtil.right(x, length);
				if(type.startsWith("S"))
					after_src = flag + after_src.substring(1);
				if(type.endsWith("S"))
					after_src = after_src.substring(1) + flag;
				if(type.startsWith("+"))
					after_src =  flag_plus + after_src.substring(1);
				if(type.endsWith("+"))
					after_src = after_src.substring(1) + flag_plus;
				ret = after_src;
				log.trace("Type>>{} ,ret.length>>{} , ret>>{}",type,ret.length(),ret);
				result = ret.getBytes();
			}
			else if(type.startsWith("9") || type.startsWith("S") || type.endsWith("S")
					|| type.startsWith("+") || type.endsWith("+")) {
				//9V99 or S9V99 or 9V99S or 9V99+ or +9V99
				String ret = "";
				String target="";
				Matcher m;
				m = v99.matcher(type);
				if(m.find()) {
					String dot = m.group(1);
					if(dot == null )
						dot = "";
					String xx=new String(sourceBytes);
					xx=xx.replaceAll("\\$", "");
					String yy=xx.toString();
					yy=yy.replaceAll("\\,", "");
					String zz = yy.trim();
					if(zz.length()==0)
						zz="0";
					if(StringUtil.right(zz,1).equals("+") || 
							StringUtil.right(zz,1).equals("-"))
					{
						target=StringUtil.right(zz,1) + StringUtil.left(zz,zz.length()-1);
					}
					else
					{
						target=zz;
					}
					BigDecimal num = new BigDecimal(target.trim());
					String flag = (num.doubleValue() >= 0 ? " " : "-");
					String flag_plus = (num.doubleValue() >= 0 ? "+" : "-");
					num = num.abs();
					NumberFormat fmt = new DecimalFormat("0." + StringUtil.repeat("0", dot.length()));
					String x = StringUtil.repeat("0", length) + fmt.format(num);
					x = x.replaceAll("\\.", "");
					
					String after_src = StringUtil.right(x, length);
					if(type.startsWith("S"))
						after_src = flag + after_src.substring(1);
					if(type.endsWith("S"))
						after_src = after_src.substring(1) + flag;
					if(type.startsWith("+"))
						after_src =  flag_plus + after_src.substring(1);
					if(type.endsWith("+"))
						after_src = after_src.substring(1) + flag_plus;
					ret = after_src;
				}
				
				log.trace("Type>>{} ,ret.length>>{} , ret>>{}",type,ret.length(),ret);
				result = ret.getBytes();
			}
			else if (type.equalsIgnoreCase("X") || type.equalsIgnoreCase("X+") ||
				type.equalsIgnoreCase("x") || type.equalsIgnoreCase("x+") ||
				type.equalsIgnoreCase("NTRIM") || type.equalsIgnoreCase("ntrim"))
			{
				log.trace("x type sourceBytes="+new String(sourceBytes)+",sourceBytes.length="+ sourceBytes.length + ",length=" + length);
			    if (sourceBytes.length==length) {
			        if(type.equalsIgnoreCase("X") ||
			        	type.equalsIgnoreCase("x") ||
			        	type.equalsIgnoreCase("NTRIM") || type.equalsIgnoreCase("ntrim") ){
			            result = sourceBytes;
			            try {
							log.trace("result.str0>>", new String(result,"big5"));
						} catch (UnsupportedEncodingException e) {
							log.error(e.toString());
						}
			        }else if(type.equalsIgnoreCase("X+") ||
			        	type.equalsIgnoreCase("x+")){
			            result = ArrayTool.subarray(sourceBytes, 0, length-2);
			        }
			    } else if (sourceBytes.length > length) {
			        if(type.equalsIgnoreCase("X") ||
			        	type.equalsIgnoreCase("x") ||
			        	type.equalsIgnoreCase("NTRIM") || type.equalsIgnoreCase("ntrim")){
			            result = ArrayTool.subarray(sourceBytes, 0, length);
			            try {
							log.trace("result.str>>", new String(result,"big5"));
						} catch (UnsupportedEncodingException e) {
							log.error(e.toString());
						}
			        }else if(type.equalsIgnoreCase("X+") ||
			        	type.equalsIgnoreCase("x+")){
			            result = ArrayTool.subarray(sourceBytes, 0, length-2);
			        }
			    } else {
//			    	這邊確定是sourceBytes.length < length
//			    	20190415 add by hugo，中文UTF-8 轉 BIG5 會有長度問題 ，捨棄原有作法，直接用byte[] merge
			    	int diff = length-sourceBytes.length;
			    	result = new byte[diff];
			    	log.trace("length>>{},sourceBytes.length>>{}",length,sourceBytes.length);
//			    	補滿空白
			    	Arrays.fill(result,0,result.length,(byte) 32);
			    	result =CodeUtil.combineByte(sourceBytes, result);
			    	log.trace("sourceBytes2.length>>{}",sourceBytes.length);
			    	log.trace("sourceBytes.str>>{}",new String(sourceBytes));
			    	log.trace("result.len>>{}",result.length);
			    	log.trace("result.str>>{}",new String(result));
			    	
//			    	
//			    	log.trace("length>>{},sourceBytes.length>>{}",length,sourceBytes.length);
//			        int diff = length-sourceBytes.length;
//			        log.trace("diff>>{}",diff);
//			        byte[] offset = new byte[diff];
//					for (int i = 0; i < diff; i++) {
//						offset[i] = ' ';
//					}
//					log.trace("offset.length>>{}",offset.length);
//					try {
////						20190411 edit by hugo 處理中文亂碼問題offset getBytes要特別指定big5
//						
//						log.trace("a>>{}",new String(sourceBytes).length());
//						log.trace("bbig5>>{}",new String(sourceBytes,"big5").length());
//						result = (new String(sourceBytes) + new String(offset)).getBytes("big5");
//						log.trace("result.length>>{}",result.length);
//					} catch (UnsupportedEncodingException e) {
//					}
//			        log.trace("x. type result.length="+result.length+",result="+ new String(result) +"|");
//			        
//			        
			    }
			}//X || X+
			else if (type.equalsIgnoreCase("XR") || type.equalsIgnoreCase("Xr"))
			    {
			    	MyDebug.debprt("x type sourceBytes="+new String(sourceBytes)+",sourceBytes.length="+ sourceBytes.length + ",length=" + length);
			        if (sourceBytes.length==length) {
			                result = sourceBytes;
			        } else if (sourceBytes.length > length) {
			                result = ArrayTool.subarray(sourceBytes, 0, length);
			        } else {
			        	result = ArrayTool.subarray(sourceBytes, 0, sourceBytes.length);
			            MyDebug.debprt("x type result.length="+result.length+",result="+ new String(result) +"|");
			        }
			    }//X || X+
/*
			else if (type.equalsIgnoreCase("H") || type.equalsIgnoreCase("h"))
			{
			    int count = length;
			    result = new byte[length];
//			System.out.println("source="+source);
			    int value = Integer.parseInt(source);
			    for (int i=0; i<length; i++)
			    {
			      int radix = 1<<8*(count-1);
			      result[i] = (byte)(value/radix);
			      value = value%radix;
			      count--;
			    }
			} //H
*/
			else if (type.equalsIgnoreCase("H") || type.equalsIgnoreCase("h"))
			{
			    int count = length,retunpackbytes=0;
			    result = new byte[length];
			    byte[] achunpackbytes=new byte[length*2];
			    String Strachunpackbytes=null;
			    log.trace("source>>{} ,length>>{}",source,length);
//            int value = Integer.parseInt(source);
			    int k=0;
			    byte[] ch1=new byte[1];
			    byte[] ch2=new byte[1];
			    for (int i=0; i<length; i++)
			    {
			    	log.trace("sourceBytes[{}]>>{}",k,sourceBytes[k]);
			    	if(sourceBytes[k]< 'A')
			    	{
			    		ch1[0]=(byte)(sourceBytes[k]-0x30);
			    	}
			    	else
			    	{
			    		ch1[0]=(byte)(sourceBytes[k]-0x07);
			    	}
			    	k++;
			    	if(sourceBytes[k]< 'A')
			    	{
			    		ch2[0]=(byte)(sourceBytes[k]-0x30);
			    	}
			    	else
			    	{
			    		ch2[0]=(byte)(sourceBytes[k]-0x07);
			    	}
			    	k++;
					result[i]=(byte)((byte)((ch1[0] & 0x0f) << 4) | (byte)(ch2[0] & 0x0f));
					Strachunpackbytes=UnPackBytes(result,result.length,achunpackbytes,retunpackbytes);
					log.trace("Strachunpackbytes>>",Strachunpackbytes);
			    }
			} //H
			else if (type.equalsIgnoreCase("H+") || type.equalsIgnoreCase("h+"))
			{
			    result = new byte[length];
			    int sourceLength = source.length();
			    int pos = 0;
			    do{
			        Integer value = Integer.decode("0x"+source.substring(pos,pos+2));
			        result[pos/2] = value.byteValue();
			        pos += 2;
			    }while(pos!=sourceLength);
			}//H+
			else if (type.equals("TIME") || type.equals("time"))
			{
				SimpleDateFormat sdftime = new SimpleDateFormat("HHmmss");
				String nowtime=sdftime.format(new Date());
				nowtime = StringUtil.repeat("0", 6) + sdftime.format(new Date());
				result = nowtime.substring(nowtime.length()-6).getBytes();
			}
			else if (type.equals("YYYYMMDD") || type.equals("yyyymmdd"))
			{
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
				result=sdf.format(new Date()).getBytes();
				log.trace("today(YYYYMMDD)>>",new String(result));
			}
			else if (type.equals("CCYYMMDD") || type.equals("ccyymmdd"))
			{
				String today="";
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
				String date=sdf.format(new Date());
				String t1=date.substring(4);
				t1=new Integer(date.substring(0,4))-1911+t1;
				today=StringUtil.repeat("0", 8)+t1;
				result=today.substring(today.length()-8).getBytes();
				log.trace("today(CCYYMMDD)>>",new String(result));
			}
			else if (type.equals("CYYMMDD") || type.equals("cyymmdd"))
			{
				String today="";
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
				String date=sdf.format(new Date());
				String t1=date.substring(4);
				t1=new Integer(date.substring(0,4))-1911+t1;
				today=StringUtil.repeat("0", 7)+t1;
				result=today.substring(today.length()-7).getBytes();
				log.trace("today(CYYMMDD)>>",new String(result));
			}
			else if (type.equals("CCMMDD") || type.equals("ccmmdd"))
			{
				String today="";
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
				String date=sdf.format(new Date());
				String t1=date.substring(4);
				t1=new Integer(date.substring(0,4))-1911+t1;
				today=t1.substring(t1.length()-6);
				result=today.getBytes();
				log.trace("today(CCMMDD)>>",new String(result));
			}
			else if (type.equals("YYMMDD") || type.equals("yymmdd"))
			{
				String today="";
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
				String date=sdf.format(new Date());
				today=StringUtil.repeat("0", 7) + date;
				result=today.substring(today.length()-6).getBytes();
				log.trace("today(YYMMDD)>>",new String(result));
			}
			else if (type.equals("MMDD") || type.equals("mmdd"))
			{
				String today="";
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
				String date=sdf.format(new Date());
				today=StringUtil.repeat("0", 7) + date;
				result=today.substring(today.length()-4).getBytes();
				log.trace("today(MMDD)>>",new String(result));
			}
			else if (type.equals("LSEQNO") || type.equals("lseqno"))
			{
				String SSeqno="";
				String aaa=new String(sourceBytes);
				if(aaa.length()==0)
					SSeqno="1";
				else
					SSeqno=aaa; 
				int a=new Integer(SSeqno);
				SSeqno=StringUtil.repeat("0", length) + a;
				result=SSeqno.substring(SSeqno.length()-length).getBytes();
				log.trace("LSEQNO>>",new String(result));
			}
			else if (type.equals("DBSEQNO"))
			{
				
			}
			else if (type.equals("NOSPACE") || type.equals("nospace"))
			{
				result = new String(sourceBytes).getBytes();
			}
		} catch (Exception e) {
			log.error(e.toString());
		}
        return result;
    }//fitInTITAFormat end 
    
    private byte[] fitInTOTAByteFormat(int length, String type, String source)
    {
        byte[] result = "ERROR".getBytes();
        byte[] sourceBytes = source.getBytes();
        if (type.equals("9"))
        {
            if (sourceBytes.length==length) {
                result = sourceBytes;
                
            } else if (sourceBytes.length > length) {
                result = ArrayTool.subarray(sourceBytes, sourceBytes.length-length, length);
            } else {
                int diff = length-sourceBytes.length;
                char[] offset = new char [diff];
                for (int i=0; i<diff; i++) offset[i]='0';
                result = (new String(offset)+new String(sourceBytes)).getBytes();
            }
        }//9 type
        else if (type.equalsIgnoreCase("X") || type.equalsIgnoreCase("X+") ||
        	type.equalsIgnoreCase("x") || type.equalsIgnoreCase("x+"))
        {
            if (sourceBytes.length==length) {
                if(type.equalsIgnoreCase("X") || type.equalsIgnoreCase("x")){
                    result = sourceBytes;
                }else if(type.equalsIgnoreCase("X+") || type.equalsIgnoreCase("x+")){
                    result = ArrayTool.subarray(sourceBytes, 0, length-2);
                }
            } else if (sourceBytes.length > length) {
                if(type.equalsIgnoreCase("X") || type.equalsIgnoreCase("x")){
                    result = ArrayTool.subarray(sourceBytes, 0, length);
                }else if(type.equalsIgnoreCase("X+") || type.equalsIgnoreCase("x+")){
                    result = ArrayTool.subarray(sourceBytes, 0, length-2);
                }
            } else {
                int diff = length-sourceBytes.length;
                char[] offset = new char [diff];
                for (int i=0; i<diff; i++) offset[i]=' ';
                result = (new String(sourceBytes)+new String(offset)).getBytes();
            }
        }//X || X+
        else if (type.equalsIgnoreCase("H") || type.equalsIgnoreCase("h"))
        {
            result = new byte[length];
            int sourceLength = source.length();
            int pos = 0;
            do{
            	String subs = source.substring(pos,pos+2);
//				System.out.println("subs="+subs);
                Integer value = new Integer(Integer.parseInt(subs,16));
               	result[pos/2] = (byte)value.intValue();
                pos += 2;
            }while(pos!=sourceLength);
        } //H
        else if (type.equalsIgnoreCase("H+") || type.equalsIgnoreCase("h+"))
        {
            result = new byte[length];
            int sourceLength = source.length();
            int pos = 0;
            do{
                Integer value = Integer.decode("0x"+source.substring(pos,pos+2));
                result[pos/2] = value.byteValue();
                pos += 2;
            }while(pos!=sourceLength);
        }//H+
        return result;
    }//fitInTOTAByteFormat
    
    /**
        * 將下行電文中對應欄位之byte array轉為字串,並根據資料型態做適當處理<br>
        *若資料型態為X或X+ -> 去除下行電文資料多餘的空白<br>
        *若資料型態為H -> 將下行電文資料轉為十進位值<br>
        *若資料型態為H+ -> 將下行電文資料的每個byte值轉為十六進位表示<br>
        *@param type 資料型態
        *@param element 下行電文中對應欄位之byte array
        * @return 將轉換(處理)過後之資料轉為字串輸出
        */
    private String fitInTOTAFormat(String type, byte[] element)
    {
        String result = "";        
        String encoding = "";
        //String source = new String(element);
        String source = "";        
        String typecode = "";
        try {
        	/*
        	typecode=new String(element,"UnicodeBig");
        	if(typecode.indexOf("__UC")!=-1)
        	{
            	// System.out.println("ConvCode element Hex:"+ConvCode.toHexString(element));
        		source=typecode;
        		// System.out.println("source Hex:"+ConvCode.toHexString(source.getBytes("UnicodeBig")));
        		encoding="UnicodeBig";
        	}
        	else
        		{*/
        	source = new String(element);encoding="UTF-8";//}
        }catch(Exception e) {
        	
        }
    	// System.out.println("@@@ FlowObject.fitInTOTAFormat() source == "+source);

        //判斷是否包含: Unicode指示記號 => "UnicodeBig"
        /*
        if (encoding.equals("UnicodeBig"))
        {	
        	// System.out.println("@@@ FlowObject.fitInTOTAFormat() source.length() == "+source.length());
        	        	
        	try 
        	{
        		source = source.substring(0,source.indexOf("__UC"));
        		element = source.getBytes(encoding);
        		// System.out.println("@@@ FlowObject.fitInTOTAFormat() UnicodeBig == "+new String(element, encoding));        		
        	}
        	catch (Exception e)
        	{
        		// System.out.println("@@@ Exception from FlowObject.fitInTOTAFormat() __UC == "+e.getMessage());
        	}
        }*/
//        if(encoding.equals("BIG5"))
        	// System.out.println("@@@ FlowObject.fitInTOTAFormat() BIG5 == "+source);       
        if(type.equals("9")) {
        	try{
        		result = new String(element,encoding);
        	}catch(Exception e){
        		
        	}
        }else if(type.equalsIgnoreCase("XNTRIM"))
        {
        	int position = element.length;
        	try
        	{
        		/*
        		if (encoding.equals("UnicodeBig"))
        			result = new String(element, encoding).trim();	
        		else*/
        			result = new String(ArrayTool.subarray(element, 0, position), "UTF-8");
//        			result = new String(ArrayTool.subarray(element, 0, position), "MS950");
        		
        		MyDebug.debprt("type = |" + type + "| result = " + result);
        	}
            catch(Exception e)
            {
            	MyDebug.errprt("fitInTOTAFormat Type=" + type +",Conv String error--->" + e.getStackTrace());
            	return(null);
            }
        }
        else if(type.equalsIgnoreCase("X") || type.equalsIgnoreCase("X+") ||
        	type.equalsIgnoreCase("x") || type.equalsIgnoreCase("x+"))
        {
            int position = element.length-1;
            for(int j=element.length-1;j>=0;j--)
            {
                if(element[j]==" ".getBytes()[0])
                {	// 略過空格 albert
                    position = j-1;
                    continue;
                // 20150907 因為N130下行電文"補充資料欄(DATA16)"只有一個"@"，所以會導致程式異常，故需在此加入j-1 >= 0判斷且要放在最前面先判斷。
                }else if(j-1 >= 0 && element[j]==64 && element[j-1]==-95)	// why?? albert
                {	// 略過空格for EBCDIC? albert 8/1
                    position = j-2;
                    j = j-1;
                    continue;
                }else
                {	//一般文字, OK
                    break;
                }
            }
            try
            {
        		/*
            	if (encoding.equals("UnicodeBig"))
        			result = new String(element, encoding).trim();	
        		else*/            	
        			result = new String(ArrayTool.subarray(element, 0, position+1), "UTF-8");
//        			針對難字處理轉UTF-8時 HEX40會變全形空白 統一在此處理 只處理右邊空白和全形空白 
        			result = StrUtils.trimRBlank(result);
//        			result = new String(ArrayTool.subarray(element, 0, position+1), "MS950");            	
            }
            catch(Exception e)
            {
            	MyDebug.errprt("fitInTOTAFormat Type=" + type +",Conv String error--->" + e.getStackTrace());
            	return(null);
            }

            MyDebug.debprt("type = |" + type + "| result = " + result);
        }// X || X+
        else if (type.equalsIgnoreCase("H") || type.equalsIgnoreCase("h"))
        {
            int retunpackbytes=0;
            result = new String(element);
            byte[] achunpackbytes=new byte[element.length*2];
            String Strachunpackbytes=null;
			
			Strachunpackbytes=UnPackBytes(element,element.length,achunpackbytes,retunpackbytes);
			MyDebug.debprt("fitInTOTAFormat Strachunpackbytes = " + Strachunpackbytes);
			result = new String(achunpackbytes);
        }
/*
        if (type.equalsIgnoreCase("H") || type.equalsIgnoreCase("h"))
        {	//將整個hex value bytes array表示為一個integer number		albert
            int totalValue = 0;
            int length = element.length;
            for (int i=0; i<element.length; i++){
                int byteValue = 0;
                byte temp = element[i];
//				System.out.println("element[i]="+Integer.toHexString((int)temp));
                if ( temp >=0 && temp<128){
                    byteValue = temp;
                } else {
                    byteValue = temp+256;
                }
                totalValue += byteValue*(1<<(--length)*8);
            }
            result = new Integer(totalValue).toString();
        }//H
*/
        else if (type.equalsIgnoreCase("H+") || type.equalsIgnoreCase("h+"))
        {	//若是H+,則仍要保留每個byte的hex string format
            int length = element.length; 
            for (int i=0; i<element.length; i++){
                int value = 0; 
	        if(element[i]<0){
	            value = new Byte(element[i]).intValue()+256;
	        }else{
	            value = new Byte(element[i]).intValue();
	        }
	        result += Integer.toHexString(value);
            }   
        }//H+
        return result;
    }//fitInTOTAFormat end
    
    /**
        * 將下行電文中對應欄位之byte array轉為Vector, for TBB專用  albert add, 6/16
        * 讀取byte array中repeated (row,column,data)依序放入vector
        * @param element 下行電文中對應欄位之byte array
        * @return 將轉換(處理)過後之資料轉為一個vector
        */
	private Vector CalculateTOTAField(byte []element)
	{
		int size = element.length;
		int cellsize = 0;
		byte []celldata = new byte[size];
		int row=0,column=0;
		Vector result = new Vector();
		// dataSection=true:新欄位開始,將上一個欄位的row,column,data放入resutl Vector   6/16
		// 規則:(row,column,data) row:0x34,0x04  column:0x34,0x00  data:直到下一個row|column control code
		// 但一組(row,column,data)可能沒有row,只有column (row相同就不用給)
		boolean dataSection = false;		
		
		for(int i = 0; i<size; i++)
		{
			if(element[i]==0x34&&element[i+1]==0x04)
			{
				if(dataSection)
					gatherData(cellsize,row,column,celldata,result);
				dataSection=false;	
				
				row = calculateHexValue(element[i+2]);
				i+=2;
			}
			else if(element[i]==0x34&&element[i+1]==0)
			{
				if(dataSection)
					gatherData(cellsize,row,column,celldata,result);
				cellsize=0;
				column = calculateHexValue(element[i+2]);
				i+=2;
				dataSection=true;
			}
			else
				celldata[cellsize++] = element[i];
		}
		gatherData(cellsize,row,column,celldata,result);
		return result;
	}//CalculateTOTAField

	private int calculate2BytesHex(byte hi, byte lo)
	{
		int high = (int)hi;//calculateHexValue(hi);
		int low = (int)lo;//calculateHexValue(lo);
		return high*256+low;
	}		

	private int calculateHexValue(byte h)
	{
		int hex = (int)h;
		int hi = hex>>4;
		int lo = hex&0x0F;
		int result = hi*16+lo;
		return result;
	}

	private void gatherData(int cellsize, int row, int column, 
						byte []celldata, Vector result)
	{
		byte []data = new byte[cellsize];
		for(int k =0;k<cellsize;k++)
			data[k] = celldata[k];
		String[] code={new String()};
	    byte[] out=ConvCode.HOST2PC(data,code);

		result.add(Integer.toString(row));
		result.add(Integer.toString(column));
		result.add(new String(out));
	}
    /**
     * 將下行電文中對應欄位之byte array轉為字串,並根據資料型態做適當處理<br>
     * 若資料型態為H -> 將下行電文資料轉為十進位值
     * @deprecated . 
     * @param target 資料型態
     * @param source 下行電文中對應欄位之byte array
     * @return 將轉換(處理)過後之資料轉為字串輸出
     */
    private String formatResponse(String target, byte[] source)
    {
        String result = null;
        int length = source.length;
        int totalValue = 0;
        if (target.charAt(0)=='H') {
            for (int i=0; i<source.length; i++){
                int byteValue = 0;
                byte temp = source[i];
                if ( temp >=0 && temp<128){
                    byteValue = temp;
                } else {
                    byteValue = temp+256;
                }
               //.debug("Hex["+i+"]"+byteValue+" ");
                totalValue += byteValue*(1<<(--length)*8);
            }
            result = new Integer(totalValue).toString();
        } else {
            result = new String(source);
        }
        //center.debug(result);
        return result;
    }//end formatResponse



	//以下移植TeleMessageMapper
	//MessageMappingUtility



    /** 
          *將被轉為全形的資料轉為半形
          *@param input 被轉為全形的ASCII編碼方式之byte array
          *@return 轉為半形後的ASCII編碼方式之byte array
      */
    private byte[] reverseConvertedCode(byte[] input)
    {
        byte[] result = null;
        int[] ascii = {0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2a,0x2b,0x2c,0x2d,0x2e,0x2f,0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x3a,0x3b,0x3c,0x3d,0x3e,0x3f,0x40,0x41,0x42,0x43,0x44,0x45,0x46,0x47,0x48,0x49,0x4a,0x4b,0x4c,0x4d,0x4e,0x4f,0x50,0x51,0x52,0x53,0x54,0x55,0x56,0x57,0x58,0x59,0x5a,0x5b,0x5c,0x5d,0x5e,0x5f,0x60,0x61,0x62,0x63,0x64,0x65,0x66,0x67,0x68,0x69,0x6a,0x6b,0x6c,0x6d,0x6e,0x6f,0x70,0x71,0x72,0x73,0x74,0x75,0x76,0x77,0x78,0x79,0x7a,0x7b,0x7c,0x7d,0x7e};
        int[][] ascii1 = {{0xA1,0xA1,0xA1,0xA1,0xA2,0xA2,0xA1,0xA1,0xA1,0xA1,0xA1,0xA1,0xA1,0xA1,0xA1,0xA1,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA1,0xA1,0xA1,0xA1,0xA1,0xA1,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA1,0xA2,0xA1,0xA1,0xA1,0xA1,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA3,0xA3,0xA3,0xA3,0xA1,0xA1,0xA1,0xA1},
        		  {0x40,0x49,0xA8,0xAD,0x43,0x48,0xAE,0xA6,0x5D,0x5E,0xAF,0xCF,0x41,0xD0,0x44,0xFE,0xAF,0xB0,0xB1,0xB2,0xB3,0xB4,0xB5,0xB6,0xB7,0xB8,0x47,0x46,0xD5,0xD7,0xD6,0x48,0x49,0xCF,0xD0,0xD1,0xD2,0xD3,0xD4,0xD5,0xD6,0xD7,0xD8,0xD9,0xDA,0xDB,0xDC,0xDD,0xDE,0xDF,0xE0,0xE1,0xE2,0xE3,0xE4,0xE5,0xE6,0xE7,0xE8,0x65,0x40,0x66,0x73,0xC4,0xAB,0xE9,0xEA,0xEB,0xEC,0xED,0xEE,0xEF,0xF0,0xF1,0xF2,0xF3,0xF4,0xF5,0xF6,0xF7,0xF8,0xF9,0xFA,0xFB,0xFC,0xFD,0xFE,0x40,0x41,0x42,0x43,0x61,0x55,0x62,0xE3}};
        Vector temp = new Vector();
        for(int i=1;i<input.length;i++)
        {
            boolean converted = false;
            for(int j=0;j<ascii1[1].length;j++)
            {
                if(input[i]==new Integer(ascii1[1][j]).byteValue() && input[i-1]==new Integer(ascii1[0][j]).byteValue())
                {
                    temp.add(new Byte(new Integer(ascii[j]).byteValue()));
                    converted = true;
                    break;
                }
            }
            if(!converted)
            {
                temp.add(new Byte(input[i-1]));
                temp.add(new Byte(input[i]));
                i++;
            }else{
                i++;
            }
        }
        result = new byte[temp.size()];
        for(int k=0;k<temp.size();k++) result[k] = ((Byte)temp.get(k)).byteValue();
        return result;
    }//reverseConvertedCode

     /** 
          *由於某些下行電文之欄位值為中文時,其EBCDIC編碼中不包括0x0E及0x0F<br>
          *故addMarker方法在中文轉換為ASCII編碼之前,加上前面的0x0E及後面的0x0F<br>
          *以便轉換
          *@param source EBCDIC編碼方式之byte array
          *@return 加上0x0E及0x0F的EBCDIC編碼方式之byte array
       */
    private byte[] addMarker(byte[] source)
    {
        Vector temp = new Vector();
        temp.add(new Byte("14"));
        int position = 0;
        for(int i=source.length-1;i>=0;i--)
        {
            if(source[i]!=64)
            {
                position = i;
                break;
            }
        }
        for(int j=0;j<=position;j++)
        {
            temp.add(new Byte(source[j]));
        }
        temp.add(new Byte("15"));
        byte[] result = new byte[temp.size()];
        for(int k=0;k<temp.size();k++){
            result[k] = ((Byte)temp.get(k)).byteValue();
        }
        return result;
    }//addMarker
    /** 
          *由於某些上行電文之欄位值為中文時,轉換為EBCDIC編碼後不可以在前後加上0x0E及0x0F
          *故purgeOutMarker方法在中文轉換為EBCDIC編碼後,去掉前面的0x0E及後面的0x0F
          *@param source EBCDIC編碼方式之byte array
          *@return 去掉0x0E及0x0F的EBCDIC編碼方式之byte array
          */
    private byte[] purgeOutMarker(byte[] source)
    {
        for(int i=0;i<source.length;i++)
        {
            if(source[i]==14)
            {
                do{
                    if(source[i+1]==15)
                    {
                        source[i] = 64;
                        i++;
                    }else{
                        source[i] = source[i+1];
                        i++;
                    }
                }while(source[i]!=15);
                source[i] = 64;
            }
        }
        return source;
    }//purgeOutMarker
    /** 
          *將輸入資料轉為全形
          *@param input ASCII編碼方式之byte array
          *@return 轉為全形後的ASCII編碼方式之byte array
          */
    private byte[] getConvertedCode(byte[] input)
    {
        byte[] result = null;
        int[] ascii = {0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2a,0x2b,0x2c,0x2d,0x2e,0x2f,0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x3a,0x3b,0x3c,0x3d,0x3e,0x3f,0x40,0x41,0x42,0x43,0x44,0x45,0x46,0x47,0x48,0x49,0x4a,0x4b,0x4c,0x4d,0x4e,0x4f,0x50,0x51,0x52,0x53,0x54,0x55,0x56,0x57,0x58,0x59,0x5a,0x5b,0x5c,0x5d,0x5e,0x5f,0x60,0x61,0x62,0x63,0x64,0x65,0x66,0x67,0x68,0x69,0x6a,0x6b,0x6c,0x6d,0x6e,0x6f,0x70,0x71,0x72,0x73,0x74,0x75,0x76,0x77,0x78,0x79,0x7a,0x7b,0x7c,0x7d,0x7e};
        int[][] ascii1 = {{0xA1,0xA1,0xA1,0xA1,0xA2,0xA2,0xA1,0xA1,0xA1,0xA1,0xA1,0xA1,0xA1,0xA1,0xA1,0xA1,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA1,0xA1,0xA1,0xA1,0xA1,0xA1,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA1,0xA2,0xA1,0xA1,0xA1,0xA1,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA2,0xA3,0xA3,0xA3,0xA3,0xA1,0xA1,0xA1,0xA1},
        		  {0x40,0x49,0xA8,0xAD,0x43,0x48,0xAE,0xA6,0x5D,0x5E,0xAF,0xCF,0x41,0xD0,0x44,0xFE,0xAF,0xB0,0xB1,0xB2,0xB3,0xB4,0xB5,0xB6,0xB7,0xB8,0x47,0x46,0xD5,0xD7,0xD6,0x48,0x49,0xCF,0xD0,0xD1,0xD2,0xD3,0xD4,0xD5,0xD6,0xD7,0xD8,0xD9,0xDA,0xDB,0xDC,0xDD,0xDE,0xDF,0xE0,0xE1,0xE2,0xE3,0xE4,0xE5,0xE6,0xE7,0xE8,0x65,0x40,0x66,0x73,0xC4,0xAB,0xE9,0xEA,0xEB,0xEC,0xED,0xEE,0xEF,0xF0,0xF1,0xF2,0xF3,0xF4,0xF5,0xF6,0xF7,0xF8,0xF9,0xFA,0xFB,0xFC,0xFD,0xFE,0x40,0x41,0x42,0x43,0x61,0x55,0x62,0xE3}};
        Vector temp = new Vector();
        for(int i=0;i<input.length;i++)
        {
            if(input[i]<0)
            {
                temp.add(new Byte(input[i]));
                temp.add(new Byte(input[i+1]));
                i++;
                continue;
            }else
            {
                for(int j=0;j<ascii.length;j++)
                {
                    if(input[i]==new Integer(ascii[j]).byteValue())
                    {
                        temp.add(new Byte(new Integer(ascii1[0][j]).byteValue()));
                        temp.add(new Byte(new Integer(ascii1[1][j]).byteValue()));
                        break;
                    }
                }
            }//else
        }//for
        result = new byte[temp.size()];
        for(int k=0;k<temp.size();k++) result[k] = ((Byte)temp.get(k)).byteValue();
        return result;
    }//end getConvertedCode

	int power(int base,int pow)
	{
		if(pow==0)
			return 1;
		else
		{
			for(int i=1; i<pow; i++)
				base*=base;
			return base;
		}
	}// power
	//為了VirtualHost的template string轉碼(某些需轉EBCDIC碼的欄位)
	//@Create date		: 2005/8/8
	//@Author			: 李國樞
	//@param TOTATable	: 從TOTAString中取出的TOTA field value (initial empty)
	//@param msg		: result TOTA byte array
	//@param pos		: 目前處理的msg byte array之array index
	//@param stringPos	: 目前處理的TOTAString position		(5/8/15)
	//@param TOTAString : template string
	//@param positionMapping
	//@param LoopTimes
	//@param endingControl:0:結束,1:折返,2:延續        06.06.25  Albert
 	public int makeTOTA(TelegramDataList template, Hashtable TOTATable, byte []msg, 
 	    int pos, int []stringPos, byte []TOTAString,Hashtable positionMapping, int LoopTimes, 
		String length , int []endingControl) throws CommunicationException
	{
		//這裡的pos要有兩種,一種是byte stream position pointer,另一種是answer string
		//position pointer		8/15
//		System.out.println("makeTOTA");
//		System.out.println("makeTOTA flowtype = " + flowtype);
		if(flowtype.equals("開始TOTA"))
			return trueObject.makeTOTA(template,TOTATable,msg,pos,stringPos,TOTAString,positionMapping,1,null,endingControl);
		else if(flowtype.equals("條件判斷"))
		{
			boolean b = calculateCondition(template,"TOTA",TOTATable,null);	//利用condition定義和電文資料取得條件值,未寫
			if(b && trueObject!=null)
				return trueObject.makeTOTA(template,TOTATable,msg,pos,stringPos,TOTAString,positionMapping,1,null,endingControl);
			else if(b && falseObject!=null)
				return falseObject.makeTOTA(template,TOTATable,msg,pos,stringPos,TOTAString,positionMapping,1,null,endingControl);
		}
		else if(flowtype.equals("SWITCH"))
		{
			int index = calculateSwitch(template,"TOTA",TOTATable,null);	//利用switch定義和電文資料取得條件值,未寫
			//此index為switchObject的array index
			if(switchObject[index]!=null)
				return switchObject[index].makeTOTA(template,TOTATable,msg,pos,stringPos,TOTAString,positionMapping,1,null,endingControl);
		}
		else if(flowtype.equals("電文欄位"))
		{
            pos = pos + makeLoopedTOTABytes(template,TOTATable,msg,pos,stringPos,
				                                TOTAString,positionMapping,1,null);
			if(trueObject!=null)
				return trueObject.makeTOTA(template,TOTATable,msg,pos,stringPos,TOTAString,positionMapping,1,null,endingControl);
		}
		else if(flowtype.equals("電文陣列"))
		{
			int looptime = Integer.parseInt(OccursTimes);
			if(occursLengthItemValue!=null&& occursLengthItemValue.hasValue())
			{    
                OccursLength = occursLengthItemValue.getValue(template, messageType, TOTATable,null);
				int len = Integer.parseInt(OccursLength)-Integer.parseInt(OccursOffset);
				String occurLengthDef = Integer.toString(len);

				pos = pos + makeLoopedTOTABytes(template,TOTATable,msg,pos,stringPos,
				                TOTAString,positionMapping,looptime,occurLengthDef);
				//return trueObject.makeTOTA(template,TOTATable,msg,pos,stringPos,TOTAString,positionMapping,looptime,occurLengthDef);
			}
			else
			{
//				System.out.println("occursLengthItemValue==null,looptime="+looptime);
                pos = pos + makeLoopedTOTABytes(template,TOTATable,msg,pos,stringPos,
				                TOTAString,positionMapping,looptime,null);
				//return trueObject.makeTOTA(template,TOTATable,msg,pos,stringPos,TOTAString,positionMapping,looptime,null);
			}
			if(trueObject!=null)
				return trueObject.makeTOTA(template,TOTATable,msg,pos,stringPos,
				                                TOTAString,positionMapping,1,null,endingControl);
		}
		else if(flowtype.equals("結束TOTA"))
		{
            endingControl[0] = 0;
        }
		else if(flowtype.equals("折返"))
		{
//			System.out.println("makeTOTA.msg="+ new String(msg));
            endingControl[0] = 1;
        }
        endingControl[0] = 2;
		return pos;
	}//makeTOTA  (for VirtualHost.java)
	
    //VirtualHost讀取testing data stream時用        
    private int makeLoopedTOTABytes(TelegramDataList template, Hashtable TOTATable, byte []msg, int pos,
		int []stringPos,  byte []TOTAString,Hashtable positionMapping, int LoopTimes, String length) throws CommunicationException
    {
		int c;
		int index=0,blockLength=0;
		int occurLength = length==null ? 2147483647 : Integer.parseInt(length);
		String field = null, userValue=null, ascUserValue=null;
		//occurs次數由 ......... 控制  8.8
		boolean occurEnd = false;
		for(c=0; c<LoopTimes && blockLength<occurLength; c++)
		{
			for(index=0; index<msgField.size(); index++)
			{
				boolean transfertype ;
				String fieldName = (String)msgField.get(index);
				field = LoopTimes==1 ? fieldName : fieldName+"_"+c; 
				TelegramData t = (TelegramData)template.get(fieldName);
//				System.out.println("fieldName="+fieldName+",TOTAString.length()="+TOTAString.length);
                byte []temp;
                if(t.type.equalsIgnoreCase("H"))
                {
                    temp = new byte[2*t.length];
//					System.out.println("times="+c+",fieldName="+fieldName+",stringPos[0]="+stringPos[0]+",t.length="+t.length+",TOTAString.length()="+TOTAString.length);
                    System.arraycopy(TOTAString,stringPos[0],temp,0,2*t.length);
                }
                else
                {
                    temp = new byte[t.length];
//					System.out.println("times="+c+",fieldName="+fieldName+",stringPos[0]="+stringPos[0]+",t.length="+t.length+",TOTAString.length()="+TOTAString.length);
                    System.arraycopy(TOTAString,stringPos[0],temp,0,t.length);
                }                
				userValue = new String(temp);
				byte[] tempbyte1;
				if(t.type.equalsIgnoreCase("H"))
				{
					tempbyte1 = fitInTOTAByteFormat(t.length, t.type, userValue);
					if(tempbyte1.toString().equals("ERROR"))
					{
//						MyDebug.errprt("Z010 : " + fieldName +" : call fitInTOTAByteFormat error");
//						this.ErrCod="Z010";
//						this.ErrField=fieldName;
//						this.ErrText="call fitInTOTAByteFormat error";
//						return(-1);
						throw new CommunicationException("M1", "Z010","");
					}
					int uvalue=0,yy,zz;
					for(yy=tempbyte1.length-1,zz=0; yy>=0; yy--,zz++)
					{
						Byte bv = new Byte(tempbyte1[yy]);
						uvalue = uvalue + bv.intValue()*power(256,zz);
//						System.out.println("uvalue="+uvalue+",tempbyte1.length="+tempbyte1.length+",zz="+zz+",yy="+yy+",bv="+bv.intValue());
					}
					ascUserValue = Integer.toString(uvalue);
					TOTATable.put(field, ascUserValue);
				}
				else	
//					TOTATable.put(field, StrUtils.trimBlank(userValue));
					TOTATable.put(field, userValue.trim());

				if( userValue==null )
				{
					userValue = t.value;
					transfertype = true;
				}
				else
				 	transfertype = false;
				if(t==null)	//找不到fieldname_c表示client沒有此筆資料傳入
				{
					occurEnd = true;
				    break;
				}
				//從Hashtable中取得欄位值填入byte[]中
				//現在因為pos已做為參數傳入,而不再是instance variable,所以pos在這邊也得累加
				//這樣之後call trueObject.getTITABytes()時才有正確的pos    05.8.2
				//如果還要用instance variable的話,就要用synchronized(currentPos)了
				packTOTAByteElement(userValue, t , pos, msg, "", transfertype);
//				System.out.println("Post packTOTAByteElement, userValue="+userValue+",msg="+(new String(msg)).trim());
				positionMapping.put(t.name, new Integer(pos));
				pos = pos+t.length;
				stringPos[0] = t.type.equalsIgnoreCase("H") ? stringPos[0]+t.length*2 : stringPos[0]+t.length;
				blockLength += t.length;	//blockLength應該還是byte array index
				//fields[index+c*msgField.size()] = field; 
			}//for
		}//for
		return pos;
    } //makeLoopedTOTABytes end
    
    //這裡就不能再用calculateCondition,calculateSwitch..這些methods      12/29
    //最重要的是: 1.記錄TITA_ID , 2.條件值(column,value)記錄在hashtable中
    //3.
    //@template : XML tags          @conditionTable : (column,value)
    //@pos :        @stringPos :            @TOTAString : output string
 	public void makeTOTATestData(TelegramDataList template, Hashtable conditionTable, 
 	      int pos, int []stringPos, StringBuffer TOTAString, Hashtable positionMapping,
 	      int LoopTimes, String length) throws CommunicationException
	{
		if(flowtype.equals("開始TOTA"))
        {   
            deleteDBRecord();
			trueObject.makeTOTATestData(template,conditionTable,pos,stringPos,TOTAString,positionMapping,1,null);
        }
        else if(flowtype.equals("條件判斷"))
        {
            StringBuffer sb = new StringBuffer(TOTAString.toString());
            Hashtable pm = (Hashtable)positionMapping.clone();
            Hashtable ct = (Hashtable)conditionTable.clone();
            if(trueObject!=null)
            {   //在true時填入欄位之條件值
                recordCondition(conditionTable);  // 將condition中所reference的欄位名記錄在conditionTable中    1/2 10:20
                //System.out.println("條件判斷==true");
				trueObject.makeTOTATestData(template,conditionTable,pos,stringPos,TOTAString,positionMapping,1,null);
            }
			if(falseObject!=null)
                falseObject.makeTOTATestData(template,ct,pos,stringPos,sb,pm,1,null);
		}
		else if(flowtype.equals("SWITCH"))
		{
            for(int index=0; index<switchObject.length; index++)
            {
                if(switchObject[index]!=null)
                {
                    StringBuffer sb = new StringBuffer(TOTAString.toString());
                    Hashtable pm = (Hashtable)positionMapping.clone();
                    Hashtable ct = (Hashtable)conditionTable.clone();
                    recordCondition(ct,index);
				    switchObject[index].makeTOTATestData(template,ct,pos,stringPos,sb,pm,1,null);
				}
            }
        }
        else if(flowtype.equals("電文欄位"))
        {
            pos = pos + makeLoopedTOTAStream(template,conditionTable,pos,stringPos,
				            TOTAString,positionMapping,1,null);
            if(trueObject!=null)
                trueObject.makeTOTATestData(template,conditionTable,pos,stringPos,TOTAString,positionMapping,1,null);
            else
                writeTOTAString2DB(template,conditionTable,positionMapping,TOTAString,1);
		}
		else if(flowtype.equals("電文陣列"))
		{
			int looptime = Integer.parseInt(OccursTimes);
            OccursLength = occursLengthItemValue.toString();
			pos = pos + makeLoopedTOTAStream(template,conditionTable,pos,stringPos,
			            TOTAString,positionMapping,looptime,null);
//			System.out.println("OccursLength="+OccursLength+",OccursOffset="+OccursOffset+",LoopTimes="+LoopTimes+",flowtype="+flowtype);
	
			if(trueObject!=null)
				trueObject.makeTOTATestData(template,conditionTable,pos,stringPos,
				                                TOTAString,positionMapping,1,null);
            else
                writeTOTAString2DB(template,conditionTable,positionMapping,TOTAString,1);
		}
		else if(flowtype.equals("結束TOTA"))
            writeTOTAString2DB(template,conditionTable,positionMapping,TOTAString,0);
	}// TeleMessageMapper.makeTOTATestData  (for VirtualHost.java)

    //2005/12/29    albert
    //totaType:0    end
    //totaType:1    repeat
    //totaType:2    return
    private void writeTOTAString2DB(TelegramDataList template, Hashtable conditionTable, 
                    Hashtable positionMapping, StringBuffer TOTAString, int totaType)
    {   
        Hashtable titaCondition = new Hashtable();
        fillTestDataFieldValue(template,conditionTable,titaCondition,positionMapping,TOTAString);
        writeDB(TOTAString,titaCondition,totaType);
//		System.out.println("TOTA String:"+TOTAString.toString());
    }
    
    private void writeDB(StringBuffer TOTAString, Hashtable titaCondition, int totaType)
    {
        Connection con=null;
        Statement statement=null;
        int num = titaCondition.size();
        String key[] = new String[num];
        String value[] = new String[num];
        try
        {
            
            Class.forName("com.microsoft.jdbc.sqlserver.SQLServerDriver");
            con = DriverManager.getConnection("jdbc:microsoft:sqlserver://localhost:1433;User=sa;Password=1234");
            statement = con.createStatement();
            String sql = "use TopTestMessages";
            statement.execute(sql);
            
            Enumeration enum1 = titaCondition.keys();
            for(int i=0;enum1.hasMoreElements();i++)
            {
                key[i] = enum1.nextElement().toString();
                value[i] = (String)titaCondition.get(key);
            }
            
            sql = "insert TestMessages (TITA_ID ,";
            for(int i=1; i<=num; i++)
                sql = sql+ "column"+i+" , value"+i+" , ";
            sql=sql+"messageContent , returnFlag ) values ('"+titaID+"' , ";
            for(int i=0; i<num; i++)
                sql = sql+"'"+key[i]+"'" + " , '"+value[i]+"' , ";
            sql = sql+"'"+TOTAString.toString()+"' , '"+totaType+"' )";
//			System.out.println("sql command INSERT="+sql);
            statement.execute(sql);
            statement.close();
            con.close();
        }catch(Exception e){
        	log.error(e.toString());
    	}
    }//writeDB
    
    private void deleteDBRecord()
    {
        try
        {
            Connection con=null;
            Statement statement=null;
            Class.forName("com.microsoft.jdbc.sqlserver.SQLServerDriver");
            con = DriverManager.getConnection("jdbc:microsoft:sqlserver://localhost:1433;User=sa;Password=1234");
            statement = con.createStatement();
            String sql = "use TopTestMessages";
            statement.execute(sql);

            sql = "delete from TestMessages where TITA_ID='"+titaID+"'";
            statement.execute(sql);
            statement.close();
            con.close();
        }catch(Exception e)
        {
        	// System.out.println(e);
        }
    }

    //當有condition branch時,就以condition field value填入測試資料中
    private void fillTestDataFieldValue(TelegramDataList template, Hashtable conditionTable, 
               Hashtable titaCondition, Hashtable positionMapping, StringBuffer TOTAString)
    {
        Enumeration enum1 = conditionTable.keys();
        for(String key;enum1.hasMoreElements();)
        {
            key = enum1.nextElement().toString();
            String value = (String)conditionTable.get(key);
            Integer pos = (Integer)positionMapping.get(key);
            if(pos!=null)
            {
                TelegramData t = (TelegramData)template.get(key);
                TOTAString.delete(pos.intValue(), t.length);
                TOTAString.insert(pos.intValue(), value);
            }
            else if(key.startsWith("TITA."))
            {
                //String s = key.substring(key.indexOf(".")+1, key.length());
                titaCondition.put(key,value);
            }
        }   //成功! 6/1/10 16:29
    }//fillTestDataFieldValue
    
    private void recordCondition(Hashtable conditionTable) 
    {   
        conditionTree.getConditionValue(conditionTable);
    }

    private void recordCondition(Hashtable conditionTable,int i) 
    {   
        conditionTable.put(_switch, _caseItemValue.get(i).toString());
    }

    //VirtualHost製造testing data stream時用            2005/12/27
    private int makeLoopedTOTAStream(TelegramDataList template, Hashtable TOTATable, 
            int pos, int []stringPos, StringBuffer TOTAString,
            Hashtable positionMapping, int LoopTimes, String length) throws CommunicationException
    {   //這裡還是需要positionMapping的,當需要事後填值時就能容易找到位置  1/2 11:48
		int c;
		int index=0,blockLength=0;
		int occurLength = length==null ? 2147483647 : Integer.parseInt(length);
		String field = null, ascUserValue=null;
		//occurs次數由 ......... 控制  8.8
		boolean occurEnd = false;
		for(c=0; c<LoopTimes && blockLength<occurLength; c++)
		{
			for(index=0; index<msgField.size(); index++)
			{
				boolean transfertype ;
				String fieldName = (String)msgField.get(index);
				field = LoopTimes==1 ? fieldName : fieldName+"_"+c; 
				TelegramData t = (TelegramData)template.get(fieldName);
				if(t.type.equalsIgnoreCase("H"))
				    TOTAString.append(t.value+t.value);
				else
				    TOTAString.append(t.value);

//				System.out.println("userValue="+userValue);
                positionMapping.put(fieldName, new Integer(pos));
				pos = pos+t.length;
				stringPos[0] = t.type.equalsIgnoreCase("H") ? stringPos[0]+t.length*2 : stringPos[0]+t.length;
				blockLength += t.length;	//blockLength應該還是byte array index
				//fields[index+c*msgField.size()] = field; 
			}//for
		}//for
		
//		System.out.println("OccursLength="+OccursLength+",OccursOffset="+OccursOffset+",LoopTimes="+LoopTimes+",flowtype="+flowtype);
		//回填occurs length     1/2 17:13
		if(OccursLength!=null)
            updateFieldLength(template,positionMapping,TOTAString,blockLength);

		return pos;
    } //makeLoopedTOTAStream end

    //value:待轉為Hex string的數字            1/2 16:42
    //fieldLength:轉成的String之長度(左補0)
    private String getHexValue(int value , int fieldLength)
    {
        String hexValue = Integer.toHexString(value);
        for(int i=hexValue.length(); i<fieldLength; i++)
            hexValue = "0"+hexValue;
        return hexValue;
    }//getHexValue
    
    private void updateFieldLength(TelegramDataList template, Hashtable positionMapping, 
                    StringBuffer TOTAString, int blockLength)
    {   //回填tota string中標示occurs長度欄位之值
		int fieldLength = 0;  //occurs長度欄位的長度
		Integer occursLengthPos = (Integer)positionMapping.get(OccursLength);
		TelegramData t = (TelegramData)template.get(OccursLength);
		if(t.type.equalsIgnoreCase("H"))    
            fieldLength = t.length*2;
        else
            fieldLength = t.length;
        TOTAString.delete(occursLengthPos.intValue(),occursLengthPos.intValue()+t.length*2);
        TOTAString.insert(occursLengthPos.intValue(),getHexValue(blockLength , fieldLength));
    }//updateFieldLength
    
    
	/*
		@param template	: 此電文的所有欄位定義
		@param msg 		: TOTA電文String value(for Web Services tmp use)   6/3/3
		@param result 	: 解譯完成的TOTA資料結構
		@param pos		:
		@param preTITA	: TOTA相應的上行電文
		@param LoopTimes: occur times
		@param length	: occurs total length
			refval,optional attributes for TBB,暫時不做
		@return 0:電文結束 , 1:繼續接收 , 2:折返
	 */
	//是原先的getTelegramDataList和packTOTAElement的合體
	public int getStringTOTATable(TelegramDataList template, String msg, Hashtable result, 
			int pos, Hashtable preTITA,int LoopTimes, String length)
	{
		int end = 2;
		//現在採取的替代做法是把pos一直傳遞下去
//		System.out.println("TOTA type="+flowtype+",falseid="+falseid+",trueid="+trueid);
		if(flowtype.equals("開始TOTA") || flowtype.equals("開始TITA"))
			return trueObject.getStringTOTATable(template,msg,result,pos,preTITA,1,null);
		else if(flowtype.equals("條件判斷"))
		{
			boolean b = calculateCondition(template,"TOTA",result,preTITA);	//利用condition定義和電文資料取得條件值,未寫
			if(b && trueObject!=null)
				return trueObject.getStringTOTATable(template,msg,result,pos,preTITA,1,null);
			else if(!b && falseObject!=null)
				return falseObject.getStringTOTATable(template,msg,result,pos,preTITA,1,null);
			else
				MyDebug.errprt("error; b="+b+",trueObject=【"+trueObject+"】,falseObject=【"+falseObject+"】");
		}
		else if(flowtype.equals("SWITCH"))
		{
			int index = calculateSwitch(template,"TOTA",result,preTITA);	//利用switch定義和電文資料取得條件值,未寫
			//此index為switchObject的array index
			if(switchObject[index]!=null)
				return switchObject[index].getStringTOTATable(template,msg,result,pos,preTITA,1,null);
		}
		else if(flowtype.equals("電文欄位"))
		{
			//children = new String[msgField.size()];   //here!here!here!
			//int size = msgField.size()+sub.length;
			String []fields = new String[msgField.size()*LoopTimes];
			int []r = getLoopedFields(template, 1, fields, length);
			int blockLength = r[0];
			
			//從byte[]中取得欄位值
//			System.out.println("pre packTOTAElement msg="+(new String(msg)).trim()+",LoopTimes="+LoopTimes+",blockLength="+blockLength+",length="+length+",flowtype="+flowtype);
			packStringTOTAElement(template,result, pos, msg, fields, "", "", 1);
//			System.out.println("post packTOTAElement result="+result);
			pos = pos + blockLength;
			if(trueObject!=null)
				end = trueObject.getStringTOTATable(template,msg,result,pos,preTITA,1,null);
			return end;
		}
		else if(flowtype.equals("電文陣列"))
		{
			int looptime = Integer.parseInt(OccursTimes);
			if(occursLengthItemValue!=null)
			{
				OccursLength = occursLengthItemValue.getValue(template, messageType, result,preTITA);
//				System.out.println("OccursLength="+OccursLength+",occursLengthItemValue="+occursLengthItemValue);
				int len = Integer.parseInt(OccursLength)-Integer.parseInt(OccursOffset);
				String occurLengthDef = Integer.toString(len);
                return makeLoopedStringTOTAItems(template,msg,result,pos,preTITA,
                                                    trueObject,looptime,occurLengthDef);
			}
			else
			{
			    return makeLoopedStringTOTAItems(template,msg,result,pos,preTITA,
			                                                 trueObject,looptime,null);
		    }
		}
		else if(flowtype.equals("結束TOTA") || flowtype.equals("結束TITA"))
			return 0;
		else if(flowtype.equals("FLOWRETURN"))
			return 2;
		return 2;
	}//getStringTOTATable

    //  for getStringTOTATable(), Web Services connection專用     06/3/3
	//  @param TOTATable	: 從TOTAReport bytes中取得的result (name/value pair)
	//	@param fields		: Hashtable result中的key name,本來應該就是msgFields中的field name,
	//					  但在occurs時會有_1,_2,_3....等,這是唯一的不同情形
	private void packStringTOTAElement(TelegramDataList template, Hashtable TOTATable, int pos,
		String TOTAReport,String []fields,String clientEncoding,String hostEncoding,int loopTimes) 
    {
      for(int j=0; j<loopTimes; j++)
      {
        for(int i=0,timesCount=1; i<msgField.size(); i++)
        {
			TelegramData tdata = template.get((String)msgField.get(i) );
            String name = tdata.name;
            String value = tdata.value;
            String type = tdata.type;
            String optional = tdata.optional;
            String refval = tdata.refval;
            String format = tdata.format;
            String encoding = tdata.encoding;
            String transfer = tdata.transfer;
            int length = type.equalsIgnoreCase("H") ? tdata.length*2 : tdata.length;
            
            String elementValue = "";
            Vector elementVector = null;
            
//			System.out.println("TOTAReport.length="+TOTAReport.length+",pos="+pos+",length="+length+",msgField.get("+i+")="+(String)msgField.get(i)+",j="+j);
            String element = TOTAReport.substring(pos, pos+length);
            if(type.equalsIgnoreCase("H"))
            {
                element = Integer.toString( Integer.parseInt(element,16) );
            }
            pos += length;
            TOTATable.put(fields[i+(j*msgField.size())],element);
    	}//end for i
      }//end for j
    }//packStringTOTAElement end

    //for Web Services connection only
    private int makeLoopedStringTOTAItems(TelegramDataList template, String msg, Hashtable result, 
    	 int pos, Hashtable preTITA,FlowObject flowObject, int looptime, String occurLengthDef)
    {
        String []fields = new String[msgField.size()*looptime];
        int []r = getLoopedFields(template, looptime, fields, occurLengthDef);
		int blockLength = r[0] , end=2;
		int c = r[1];
        packStringTOTAElement(template,result, pos, msg, fields, "", "", c);
        pos = pos + blockLength;
    	if(trueObject!=null)
            end = trueObject.getStringTOTATable(template,msg,result,pos,preTITA,1,null);
		return end;
    }//makeLoopedStringTOTAItems            (for Web Services connection only)
    
	static String UnPackBytes(byte[] achpackbyte,int packbytes,byte[] achunpackbytes,int unpackbytes)
	{
		String result;
		byte[] tmpbuf=new byte[packbytes*2];
		int i=0,k=0;
		
		for(i=0,k=0;i<packbytes;i++)
		{
			tmpbuf[k++]=(byte)((achpackbyte[i] & 0xf0) >> 4);
			tmpbuf[k++]=(byte)(achpackbyte[i] & 0x0f);
		}
		for(i=0,k=0;i<tmpbuf.length;i++)
		{
			if(tmpbuf[i]<=0x09)
				tmpbuf[i]|=0x30;
			else
				tmpbuf[i]=(byte)(0x41+(tmpbuf[i]-0x0A));
		}
		MyDebug.debprt("UnPackBytes=" + new String(tmpbuf));
//		achunpackbytes=tmpbuf;
		System.arraycopy(tmpbuf,0,achunpackbytes,0,tmpbuf.length);
		unpackbytes=tmpbuf.length;
		return(new String(tmpbuf));
	}

	int chklength(int fieldlen,byte[]src,int srclen)
	{
		int j,i;
		byte[] tmpsrc=new byte[srclen];
		boolean ischiness=false;
		
		j=0; // src index
		i=0; // des index
		System.arraycopy(src,0,tmpsrc,0,srclen);
		while(i<tmpsrc.length)
		{
			if((tmpsrc[i] & (byte)0x80)==(byte)0x80)
			{
				if(ischiness==false)
				{
					ischiness=true;
					j+=2;
				}
				j+=2;
				i+=2;
				if(j>fieldlen)
				{
					return(0);
				}
			}
			else
			{
				if(ischiness==true)
				{
					ischiness=false;
				}
				j++;
				i++;
				if(j>fieldlen)
				{
					return(0);
				}
			}
		}
		if(ischiness==true)
		{
			ischiness=false;
		}
		MyDebug.debprt("chklength field length = " + fieldlen);
		MyDebug.debprt("chklength sum length = " + j);
		if(j>fieldlen)
			return(0);
		else
			return(j);
	}
	
	boolean IsChiness(byte[] chinessbuf,int chinesslen)
	{
		int i;
		
		for(i=0;i<chinesslen;i++)
		{
			if((chinessbuf[i] & (byte)0x80)==(byte)0x80)
			{
				return(true);
			}
		}
		return(false);
	}
	
	public String GetMaxRecodMsg()
	{
		String retmsg=this.maxrecodemsg;
		return(retmsg);
	}

	String GetErrCod()
	{
		String retmsg=this.ErrCod;
		return(retmsg);
	}
	String GetErrField()
	{
		String retmsg=this.ErrField;
		return(retmsg);
	}
	String GetErrText()
	{
		String retmsg=this.ErrText;
		return(retmsg);
	}

	public int SetMaxRecodMsg(String retmsg)
	{
		this.maxrecodemsg=retmsg;
		return(0);
	}
	/*
	 * 檢查字串內容是否為數字
	 * return true  : 為數字無誤
	 *        false : 除數字尚有其它資料
	 */
	static boolean chknumber(byte[] src,int len)
	{
		int i;
		
		for(i=0;i<len;i++)
		{
			if(src[i]<(byte)0x30 || src[i]>(byte)0x39)
			{
				return(false);
			}
		}
		return(true);
	}
	boolean SetAllVarTypeValue(Hashtable InputData)
	{
		VarType tmpvartype;
		
		if(ExternVAr==null)
			return(false);
		for(Object key : ExternVAr.keySet())
		{
			if(InputData.get(key)!=null)
			{
				tmpvartype=(VarType)ExternVAr.get(key);
				if(tmpvartype!=null)
				{
					tmpvartype.VarVAlue=(String)InputData.get(key);
					ExternVAr.put(key, tmpvartype);
					return(true);
				}
			}
			else // InputData.get(key)==null
			{
				tmpvartype=(VarType)ExternVAr.get(key);
				if(tmpvartype!=null)
				{
					InputData.put(key, tmpvartype.VarVAlue);
					return(true);
				}
			}
		}
		return(false);
	}

	public static void main(String[] argv) {
		fitInTITAFormat(12, "S9", "0");
		fitInTITAFormat(12, "S9", "100");
		fitInTITAFormat(12, "S9", "+300");
		fitInTITAFormat(12, "S9", "-500");
		fitInTITAFormat(12, "S9", "-40,000");
		fitInTITAFormat(12, "S9", "-20,000.01");
		fitInTITAFormat(12, "+9", "21,000.21");
		fitInTITAFormat(12, "+9", "3,300");
		fitInTITAFormat(12, "+9", "-4,300");
		fitInTITAFormat(12, "+9", "-4,400.1");
		fitInTITAFormat(12, "+9", "4,500.1");
		fitInTITAFormat(13, "S9V99", "1.00");
//		fitInTITAFormat(13, "S9V99", "0");
//		fitInTITAFormat(13, "S9V99", "-1");
	}

}// end FlowObject



//如果能改寫此class,把一些功能分散到一些輔助object中去做會更好
//此工作留待以後  8/2