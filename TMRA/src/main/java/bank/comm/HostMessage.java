package bank.comm;

import java.net.*;
import java.io.*;
import java.util.*;

import org.apache.log4j.Logger;

public class HostMessage
{
//	private static Logger logger = Logger.getLogger(HostMessage.class.getName());
	MyProperties myProperties=null;
	String txid = null, msgPattern=null;
	int patternCount=1;
	
	public HostMessage()
	{
	    try 
   		{
			myProperties = new MyProperties("TBB");
    		myProperties.load(new FileInputStream("VirtualHostMsg.properties"));
		} 
	    catch(Exception e)
	    {
	    	MyDebug.errprt("HostMessage() erro --> " + e.getStackTrace());
	    }
	}

	public HostMessage(String filename)
	{
	    try 
   		{
			myProperties = new MyProperties("TBB");
    		myProperties.load(new FileInputStream(filename));
		}
	    catch(Exception e)
	    {
	    	MyDebug.errprt("HostMessage("+filename+") erro --> " + e.getStackTrace());
	    }
	}
	
	public void setTxID(String txid)
	{
		this.txid = txid;
	}
	
	public boolean next()
	{
//		System.out.println("txid=" + txid + "-" + patternCount);
		if(txid==null)	return false;
		msgPattern = myProperties.getProperty(txid+"-"+patternCount);
		if(msgPattern==null)
			return(false);
		byte []tmp=msgPattern.getBytes();
//		this.printOut("HostMessage.next",tmp);
		patternCount++;
//			System.out.println("msgPattern="+msgPattern);
		if(msgPattern!=null)
			return true;
		else
			return false;
	}
	
	public byte []getMessage()	throws UnsupportedEncodingException
	{
		String msg = "RECV0"+msgPattern;
		String s = (getMessageLength(msg)+msg);
//		System.out.println("msgPattern="+s);
		return s.getBytes("BIG5");
	}
	
	public String getMessagePatternString() 
	{
		return msgPattern;
	}
		
	public String getMessageLength(String lenStr, int len)
	{
		switch(len)
		{
			case 1:
				return "000"+lenStr;
				
			case 2:
				return "00"+lenStr;
				
			case 3:
				return "0"+lenStr;
				
			default:
				return lenStr;
		}
	}
	
	public String getMessageLength(String msg)
	{
		String len = Integer.toString(msg.getBytes().length);
//		System.out.println("getMessageLength len="+len);
		return getMessageLength(len, len.length());
	}
	
	public String toHexString(byte[] block) 
	{
		StringBuffer buf = new StringBuffer("");
		int len = block.length;
		for (int i = 0; i < len; i++)
		{
			byte2hex(block[i], buf);
			if ((i+1)%16==0)
				buf.append("\n");
			else if ( (i+1)%4==0 )
			{
				buf.append(" ");
			}
		}
		return buf.toString();
	}
     /**
      * 將傳入的byte值轉為十六進位並將其轉成字元存於一StringBuffer物件中
      *@param b 被轉換的byte
      *@param buf 儲存轉換結果之StringBuffer物件
      */
     private void byte2hex(byte b, StringBuffer buf) 
     {
          char[] hexChars = { '0', '1', '2', '3', '4', '5', '6', '7', '8',
          '9', 'A', 'B', 'C', 'D', 'E', 'F' };
          int high = ((b & 0xf0) >> 4);
          int low = (b & 0x0f);
          buf.append(hexChars[high]);
          buf.append(hexChars[low]);
     }
     
	public void printOut(String title,byte[] TOTA)
	{
        System.out.println("-->" + title + " Data in ASCII.....");
        System.out.println(" 0 1 2 3  4 5 6 7  8 9 A B  C D E F");
        System.out.println("--------|--------|--------|--------|");
        System.out.println(this.toHexString(TOTA));
    }

}// end class HostMessage