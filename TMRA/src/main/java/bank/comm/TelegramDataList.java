package bank.comm;
import java.util.*;
/**
 * 儲存電文範本的物件,該物件會儲存TelegramData物件。<br>
 * 由於一個TelegramData物件代表上行電文或下行電文某一欄位的屬性。<br>
 * TelegramDataList會依欄位順序將TelegramData物件儲存起來,形成一筆上行電文或下行電文的範本<br>
 * @author 林季穎
 * @version 1.0
 * @see bank.comm.TelegramData
 */
public class TelegramDataList 
{
    /**
        *java.util.ArrayList物件,用以存放TelegramData物件
        */
    private ArrayList telegramDataList=new ArrayList();
    /**
        *java.util.HashMap物件,和telegramDataList存放的內容是一樣的,
        *為public TelegramData get(String name) 專用		albert 050614
        */
    private HashMap telegramDataHash = new HashMap();
    /**
        *將一TelegramData物件加入
        *@param o TelegramData物件
        */
    public void add(Object o)
    { 
    	telegramDataList.add(o);
    	telegramDataHash.put( ((TelegramData)o).getName(), o);
    }
    /**
        *將指定位置的TelegramData物件移除
        *@param i 指定位置
        */
    public void remove(int i)
    { 
    	TelegramData t = (TelegramData)telegramDataList.get(i);
    	telegramDataList.remove(i);
    	telegramDataHash.remove(t.getName());
    }
    /**
        *取出指定位置的TelegramData物件
        *@param i 指定位置
        * @return Object物件
        */
    public TelegramData get(int i)
    { 						
    	return (TelegramData)telegramDataList.get(i); 
	}
	
	//欄位在byte stream中的起始位置必須透過計算獲得 
	//(因為我們不知此次電文有那些欄位,或者有沒有optional field)
	public int getStartPosition(String item,byte []msg)
	{
		int pos=0;
		
		for(int i=0; i<telegramDataList.size(); i++)
		{
			TelegramData t = (TelegramData)telegramDataList.get(i);
			if(t.name.equals(item))		//已算出item欄位在msg中的position
				break;
			if(!t.optional.equals(""))
			{	//如果有optional欄位,要從電文內容來判斷此欄位是否存在
				TelegramData d = (TelegramData)telegramDataHash.get(t.optional);  //取得optional所指向的欄位
				if(d!=null)
				{
					int p = getStartPosition(d.name,msg);			//recursive
					byte []buf = new byte[d.length];
					for(int ii=p,j=0; ii<p+d.length; ii++,j++)
						buf[j] = msg[ii];
					String[] return_code = {new String()};
					String r = new String(ConvCode.HOST2PC(buf, return_code));
					if(r.equals(d.refval))
						continue;	// 若optional所指到的欄位其值等於refval定義的值,表示電文中沒有此欄位,不計長度
				}
			}
			pos += t.length;
		}
		return 	pos;
	}
    /**
        *取出指定名稱的TelegramData物件
        * @param name 指定名稱
        * @return TelegramData物件
        */
    public TelegramData get(String name) 
    {
        return (TelegramData)telegramDataHash.get(name);    
    }
    /**
        *傳回指定位置的TelegramData物件所代表的欄位在電文中的起始位置
        *@param ID 指定位置
        * @return TelegramData物件所代表的欄位在電文中的起始位置
        */
    public int getPosition(int ID){ return (((TelegramData)telegramDataList.get(ID)).start); }
     /**
        *傳回指定位置的TelegramData物件所代表的欄位在電文中的長度
        *@param ID 指定位置
        * @return TelegramData物件所代表的欄位在電文中的長度
        */
    public int getLength(int ID){ return (((TelegramData)telegramDataList.get(ID)).length); }
     /**
        *傳回目前存放的TelegramData物件的數目
        * @return 目前存放的TelegramData物件的數目
        */
    public int getSize(){ return telegramDataList.size(); }
     /**
        *傳回目前存放的所有TelegramData物件
        * @return 目前存放的所有TelegramData物件
        */
    TelegramData[] toArray(){ return (TelegramData[])telegramDataList.toArray(); }
    //public void printOut(){ for (int i=0;i<telegramDataList.size();i++){}}
    /*public boolean isHexData(int position){
        for (int i=0;i<telegramDataList.size();i++){
            if (position>=((TelegramData)telegramDataList.get(i)).getPosition()&&
            position<((TelegramData)telegramDataList.get(i)).getLength()) return true;
        }
        return false;
    }*/
    public String toString() 
    {
    	String result="{ ";
    	
		for(int i = 0 ; i<telegramDataList.size(); i++)
		{
			Object data = telegramDataList.get(i);
			/*TelegramData telegramData =null;
			TelegramDataOccurs telegramDataOccurs =null;
			if(o instanceof TelegramData)
				telegramData = (TelegramData)o;
			else if(o instanceof TelegramDataOccurs)
				telegramDataOccurs = (TelegramDataOccurs)o;*/
			result = result+" ["+data+"], \n";
		}
		result = result+" }";
		
		return result;
	}
}

