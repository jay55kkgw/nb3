package bank.comm;

import java.util.*;
import java.net.*;
import org.apache.log4j.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
/**
	取得<ITEMID></ITEMID>定義的欄位之值

	@Author 李國樞
	@Create 2005/7/11
	@Version 2.0
  */
public class ItemValue implements Item
{
	/*
		itemID為item name. start,length定義從itemID所指向的欄位之start位置取length bytes
	*/
	private String itemID=null, messageType=null;
	private int start=0,length=0;		//如果length==0表示此ItemValue並沒有定義START,LENGTH
	
	public ItemValue( Node node, String type)
	{
		messageType = type;
		getItemValue(node);
	}

	public String toString()
	{
		return itemID;
	}
	
	public String getMessageType()
	{
	    return messageType;
	}
		
	public ItemValue( Node node, String t, String type)
	{
		messageType = type;
		if(t.equals("parent") )
		{
			NodeList nodes = node.getChildNodes();
    	    for(int i=0; i<nodes.getLength(); i++)
        	{
	    		Node node1 = nodes.item(i);
				if(nodes.getLength()==1)	
					itemID = node1.getNodeValue();
		        if(node1.getNodeType() == Node.ELEMENT_NODE)
    		    {
        			if(node1.getNodeName().equals("ITEMVALUE"))
						getItemValue(node1);
				}
			}
		}
		//else throw new Exception("Method argument error");
	}

	//這才是真正要用byte stream與TelegramDataList計算欄位position的地方
	//取得ITEMVALUE	
	//從result取得itemID所指向的欄位之值
						//template, messageType, result,preTITA
	public String getValue(TelegramDataList template, String messageType , 
							Hashtable result, Hashtable preMessage)
	{
		String value=null;
		TelegramData t=null;
		
		//如果相等,表示ITEMVALUE要索引的值就是目前正在處理的電文
		//如果不相等,表示參考前一個電文(目前是TOTA的話,前一個電文就是TITA)
		System.out.println("itemID="+itemID);
		System.out.println("messageType="+messageType);
		System.out.println("this.messageType="+this.messageType);
		if(this.messageType.equals(messageType))
		{
			value = (String)result.get(itemID);
			t = template.get(itemID);
			if(length==0)	//如果length==0表示此ItemValue並沒有定義START,LENGTH
				length = t.length;		//length就是XML中<ITEM>定義的長度
			if(t.type.equals("H"))
			{
				int i = Integer.parseInt(value);
				if(i<0) 
				{
					value = Integer.toString(i+256);
					MyDebug.debprt("value="+value);
				}
				return value;
			}
		}
		else
		{
			value = (String)preMessage.get(itemID);
			if(value.length()>0)
				length=value.length();
		}
		
	
		try{
			return value.substring(start,start+length);
		}
		catch(StringIndexOutOfBoundsException e)
		{
//			MyDebug.errprt("StringIndexOutOfBoundsException : " + e.toString() + "value="+value+",start="+start+",length="+length);
			byte[] tmpbyte=new byte[length];
			String tmp="";
			if(t.type.equalsIgnoreCase("X"))
			{
				for (int i=value.length(); i<length; i++)
					tmpbyte[i]=' ';
				tmp = value + new String(tmpbyte);
			}
			if(t.type.equalsIgnoreCase("9"))
			{
				for (int i=value.length(); i<length; i++)
					tmpbyte[i]='0';
				tmp = new String(tmpbyte) + value;
			}
			MyDebug.errprt("tmp = |" + tmp + "|");
			return tmp.substring(start,start+length);
//			throw e; 
		}
	}//getValue

    public boolean hasValue()
    {
        if(itemID==null)
            return false;
        return true;
    }
    
	private void getItemValue(Node node)
	{
		NodeList nodes = node.getChildNodes();
    	NamedNodeMap attrs = node.getAttributes();
    	String temp = getItemAttributes(attrs);
    	if(temp!=null)
    		messageType = temp;
    	
        for(int i=0; i<nodes.getLength(); i++)
        {
	    	Node node1 = nodes.item(i);
	    	
	    	if(nodes.getLength()==1)	
	    		itemID = node1.getNodeValue();
	        else if(node1.getNodeType() == Node.ELEMENT_NODE)
    	    {
        		if(node1.getNodeName().equals("ITEMID"))
        		{
					ItemID id = new ItemID(node1);
					itemID = id.getName();
				}
				else if(node1.getNodeName().equals("START"))
					start=Integer.parseInt(node1.getFirstChild().getNodeValue());
				else if(node1.getNodeName().equals("LENGTH"))
					length=Integer.parseInt(node1.getFirstChild().getNodeValue());
			}
		}
//System.out.println("itemID="+itemID+",nodes.getLength()="+nodes.getLength());
	}//getItemValue
	
	private String getItemAttributes(NamedNodeMap attrs)
	{
/*		System.out.println("attrs.getLength()="+attrs.getLength()); */
		for(int jj=0; jj<attrs.getLength(); jj++)
        {	//這個迴圈是為了取得ItemValue是否有定義MESSAGETYPE (TITA|TOTA)  9/27改
        	Node attrNode = attrs.item(jj);
            if(attrNode.getNodeName().equals("TYPE"))
            {	    	
            	return attrNode.getNodeValue();
            }
            else if(attrNode.getNodeName().equals("MESSAGETYPE"))
            {	    	
            	return attrNode.getNodeValue();
            }
        }
        return null;		//error
	}//getItemAttributes
}