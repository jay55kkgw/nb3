package bank.comm;

import java.util.Vector;

import com.netbank.util.CodeUtil;
import com.netbank.util.HexDumpUtil;

import lombok.extern.slf4j.Slf4j;

//import org.apache.log4j.Logger;


//import sun.io.ByteToCharConverter;
//import sun.io.CharToByteConverter;
import java.io.*;

@Slf4j
public class ConvCode
{
//	http://www.docjar.com/html/api/sun/io/ByteToCharConverter.java.html
//	private static Logger logger = Logger.getLogger(ConvCode.class);
	
    /** 
     *將Big5(ASCII)的編碼方式轉為EBCDIC的編碼方式
     *@param in Big5(ASCII)編碼方式之byte array
     *@return 轉為EBCDIC編碼後之byte array
     *@throws CommunicationException 當輸入之byte array無法轉換為EBCDIC的編碼方式時,丟出CommunicationException
     */	
	static public byte[] pc2host(byte[] in) throws CommunicationException
	{
		int i;
		String[] size={new String()};
		String[] code={new String()};
		byte[] out=PC2HOST(in,code);
		if(!code[0].equals("0"))
		{ 
			MyDebug.errprt("PC2HOST error code is --->" + code[0]);
			return(null);
//   	throw new CommunicationException("M1","ZE009","JSNA PC2HOST failed! return code:"+code[0]);  
		}
/*
		for(i=0;i<out.length;i++)
		{
			if(out[i]==(byte)0x15)
			{
				out[i]=(byte)0x25;
			}
		}
*/
		return out;
	}
	static public byte[] host2pc(byte[] in)
    {
        int length = in.length;
        
        String[] size={new String()};
        String[] code={new String()};
        byte[] out=HOST2PC(in,code);
        if(code[0].equals("0"))
            return out;        
        else
        {
        	MyDebug.errprt("host2pc error code is --->" + code[0]);
            Vector posData = checkShiftPair(in);
            String checkResult = (String)posData.get(0);
            if(checkResult.equals("true"))
            {
            	byte[] result = parseAllBytes(in,posData);
//            	System.out.println("host2pc error return result byte value--->" + ConvCode.toHexString(result));
            	return result;
            }
            else
            {
                byte[] error = new byte[length];
                for(int i=0;i<length;i++) error[i] = 0x3F;
//                System.out.println("host2pc error return error value--->" + ConvCode.toHexString(error));
                return error;
            }
        }
    }//host2pc
    // This method is a little bit different from C version HOST2PC
    // In this version, after conversion, the two shift bits will be replaced
    // by 2 spaces, so return a new byte array instead of passing by reference
    // Therefore, the JSNA_Bridge will need modify a little
	
	
	
    /**
        *  將EBCDIC的編碼方式轉為Big5(ASCII)的編碼方式
        *  @param cvt_buffer EBCDIC編碼方式之byte array
        *  @param return_code 轉換結果,傳回0表示轉換成功,傳回1表示轉換失敗
        *  @return 轉為Big5(ASCII)編碼後之byte array
    */
//	static public byte[] HOST2PC(byte[] cvt_buffer, String[] return_code)
//	{
//		try
//        {
//            boolean chinese=false;
//            new HexDump(HexDump.HD_EBCDIC | HexDump.HD_ASCII,"HOST2PC EBCDIC",cvt_buffer,cvt_buffer.length);
//            ByteToCharConverter toUnicode = ByteToCharConverter.getConverter("CP937");
//            CharToByteConverter fromUnicode = CharToByteConverter.getConverter("MS950");
//            char[] input = toUnicode.convertAll(cvt_buffer);            
//            //解決難字Begin
//            CharToByteConverter TOCP937 = CharToByteConverter.getConverter("CP937");            
//            byte [] unichar=TOCP937.convertAll(input); 
////            System.out.println("@@@ HOST2PC() unichar CP937 Hex value:"+ConvCode.toHexString(unichar));
//            //new HexDump(HexDump.HD_EBCDIC | HexDump.HD_ASCII,"HOST2PC CP937",unichar,unichar.length);
//            //解決難字End
//                       
//            byte[] ascii = fromUnicode.convertAll( input );
////            System.out.println("@@@ HOST2PC() ascii Hex value:"+ConvCode.toHexString(ascii));
////            System.out.println("@@@ HOST2PC() find ascii invalid word for loop");            
//            for(int i=0;i<ascii.length;i++)
//            {            	
//            	if(ascii[i]=='?')
//            	{
//            		//ascii[i]=' ';
//            		            		
///*
//  byte[] b = new byte[2];
//  b[1] = 0x60;
//  b[0] = 0x52;
//  String tmp = new String(b, "UnicodeLittle");
//  String name = new String("李永")  + tmp; 
// */            		            	         
//            		//若遇到中文難字,直接以Unicode之byte[]回傳
////            		System.out.println("@@@ HOST2PC() ascii[i]=='?' index == "+i);            		
////            		System.out.println("@@@ HOST2PC() CP937 Hex=="+toHexString(unichar));
//            		//String str = CP937toUnicodeBig(unichar) + "__UC"; 
////            		System.out.println("@@@ HOST2PC() UnicodeBig str=="+toHexString(str.getBytes("UnicodeBig")));
//            		//return_code[0]="0";
//            		//return str.getBytes("UnicodeBig"); 
//            	}
//            }
//            
//            //byte[] answer=packChinese(ascii,cvt_buffer.length);
//            return_code[0]="0";
//            //return answer;
//            return ascii;
//        }
//		catch(Exception e)
//        {
////            e.printStackTrace();
//			MyDebug.errprt("ConvCode class HOST2PC error --->"+e.getStackTrace());
//            
//            //轉碼失敗, 回傳空白
//            return_code[0]="0";
//            byte[] ret = new byte[cvt_buffer.length];
//            for(int i=0; i < ret.length; i++) 
//            	ret[i] = 0x20;
//            return ret;
//        }
//	}
	

    /**
        *  將EBCDIC的編碼方式轉為Big5(ASCII)的編碼方式
        *  @param cvt_buffer EBCDIC編碼方式之byte array
        *  @param return_code 轉換結果,傳回0表示轉換成功,傳回1表示轉換失敗
        *  @return 轉為Big5(ASCII)編碼後之byte array
    */
	static public byte[] HOST2PC(byte[] cvt_buffer, String[] return_code)
	{
		byte[] utf8Ary = null;
		try
        {
			utf8Ary = CodeUtil.hardWordProcess(cvt_buffer);
            return_code[0]="0";
            log.trace("utf8Ary to String>>{}",new String (utf8Ary,"UTF-8"));
            //return answer;
            return utf8Ary;
        }
		catch(Exception e)
        {
            log.error("ConvCode class HOST2PC error --->",e);
            //轉碼失敗, 回傳空白
            return_code[0]="0";
            byte[] ret = new byte[cvt_buffer.length];
            for(int i=0; i < ret.length; i++) 
            	ret[i] = 0x20;
            return ret;
        }
	}
	
	
	
	
     // This method is a little bit different from C version pc2host
     // In this version, the chinese character segments
     // would be added 2 shift bits after conversion,
     // so return a new byte array instead of passing by reference
     // Therefore, the JSNA_Bridge will need modify a little
     /**
      *  將Big5(ASCII)的編碼方式轉為EBCDIC的編碼方式
      *  @param cvt_buffer Big5(ASCII)編碼方式之byte array
      *  @param return_code 轉換結果,傳回0表示轉換成功,傳回1表示轉換失敗
      *  @return 轉為EBCDIC編碼後之byte array
      */
	static public byte[] PC2HOST(byte[] cvt_buffer, String[] return_code)
	{
		try
		{
			
//			ByteToCharConverter toUnicode= ByteToCharConverter.getConverter("Big5");
//			CharToByteConverter fromUnicode= CharToByteConverter.getConverter("CP937");
//			char[] input=toUnicode.convertAll(cvt_buffer);
//			byte[] ebcdic = fromUnicode.convertAll( input );
			log.trace("cvt_buffer.length>>"+cvt_buffer.length);
//			System.out.println("cvt_buffer>>"+new String(cvt_buffer,"BIG5"));
//			char[] input= new String(cvt_buffer,"Big5").toCharArray();
//			byte[] ebcdic = new String(input).getBytes("CP937");
			byte[] ebcdic = new String(cvt_buffer,"BIG5").getBytes("CP937");
//			System.out.println("ebcdic>>"+HexDumpUtil.format(ebcdic, 72));
//			new HexDump(HexDump.HD_EBCDIC | HexDump.HD_ASCII,"PC2HOST",ebcdic,ebcdic.length);
			return_code[0]="0";
			
			
//			System.out.println("ebcdic>>"+new String(ebcdic));
//			System.out.println("ebcdic.big5>>"+new String(ebcdic,"Big5"));
//			
			char[] tmpebcdic = new String(ebcdic,"CP937").toCharArray();
			byte[] tmpinput = new String(tmpebcdic).getBytes("Big5");
			
//			System.out.println("tmpinput>>"+new String(tmpinput));
			
			return ebcdic;
		}
		catch(Exception e)
		{
               //System.out.println(e.toString(),e);
			MyDebug.errprt("JSNA CLASS PC2HOST error --->"+e.getStackTrace());
			return_code[0]="1";
			return null;
		}
	}
//	/**
//	 *  將Big5(ASCII)的編碼方式轉為EBCDIC的編碼方式
//	 *  @param cvt_buffer Big5(ASCII)編碼方式之byte array
//	 *  @param return_code 轉換結果,傳回0表示轉換成功,傳回1表示轉換失敗
//	 *  @return 轉為EBCDIC編碼後之byte array
//	 */
//	static public byte[] PC2HOST(byte[] cvt_buffer, String[] return_code)
//	{
//		try
//		{
//			
//			ByteToCharConverter toUnicode= ByteToCharConverter.getConverter("Big5");
//			CharToByteConverter fromUnicode= CharToByteConverter.getConverter("CP937");
//			char[] input=toUnicode.convertAll(cvt_buffer);
//			byte[] ebcdic = fromUnicode.convertAll( input );
////			new HexDump(HexDump.HD_EBCDIC | HexDump.HD_ASCII,"PC2HOST",ebcdic,ebcdic.length);
//			return_code[0]="0";
//			return ebcdic;
//		}
//		catch(Exception e)
//		{
//			//System.out.println(e.toString(),e);
//			MyDebug.errprt("JSNA CLASS PC2HOST error --->"+e.getStackTrace());
//			return_code[0]="1";
//			return null;
//		}
//	}
     /**
      *若傳入的EBCDIC byte array帶有中文字,檢查其中0x0E與0x0F是否成對,若成對,結果為true,
      *若不成對,結果為false.若成對,紀錄0x0E及0x0F所在之位置並存於java.util.Vector物件中回傳
      *@param EBCDICArray EBCDIC編碼之byte array
      *@return java.util.Vector物件,包含檢查的結果及0x0E及0x0F在陣列中的位置
      */
	static private Vector checkShiftPair(byte[] EBCDICArray)
	{
		Vector result = new Vector();
		Vector v1 = new Vector();
		Vector v2 = new Vector();
		int shiftInCount = 0;
		int shiftOutCount = 0;
		for(int i=0;i<EBCDICArray.length;i++)
		{
			if(EBCDICArray[i]==0x0E)
			{
				shiftInCount++;
				v1.add(new Integer(i));
			}
			if(EBCDICArray[i]==0x0F)
			{
				shiftOutCount++;
				v2.add(new Integer(i));
			}
		}
		if((shiftInCount==shiftOutCount) && shiftInCount!=0)
		{
			result.add("true");
			int[] v1Array = new int[v1.size()];
			int[] v2Array = new int[v2.size()];
			for(int j=0;j<v1.size();j++)
			{
				v1Array[j] = ((Integer)v1.get(j)).intValue(); 
                v2Array[j] = ((Integer)v2.get(j)).intValue();
			}
			result.add(v1Array);
			result.add(v2Array);
		}
		else
		{
			result.add("false");
		}
		return result;
	}
	static private byte[] parseAllBytes(byte[] input,Vector posData)
	{
		int[] shiftInPos = (int[])posData.get(1);
		int[] shiftOutPos = (int[])posData.get(2);
		String[] code = {new String()};
		Vector tempVector = new Vector();
		int totalLength = 0;
		
		for(int i=0;i<shiftInPos.length;i++)
		{
			int start = shiftInPos[i];
			int end = shiftOutPos[i];
			if(i==0 && shiftInPos[0]!=0)
			{
				int length = shiftInPos[0];
				byte[] temp = new byte[length];
				System.arraycopy(input,0,temp,0,temp.length);
				byte[] temp1 = HOST2PC(temp,code);
				if(!code[0].equals("0"))
				{
					MyDebug.errprt("parseAllBytes HOST2PC error code is --->" + code[0]);
					byte[] error = new byte[length];
					for(int ii=0;ii<length;ii++)
						error[ii] = 0x3F;
					tempVector.add(error);
					totalLength += error.length;
				}
				else
				{
					tempVector.add(temp1);
					totalLength += temp1.length;
				}
			}
			for(int j=start+1;j<end;j++)
			{
				byte[] temp = new byte[4];
				temp[0] = 0x0E;
				temp[1] = input[j];
				temp[2] = input[++j];
				temp[3] = 0x0F;
				byte[] temp1 = HOST2PC(temp,code);
				if(!code[0].equals("0"))
				{
					MyDebug.errprt("parseAllBytes HOST2PC for loop error code is --->" + code[0]);
					byte[] error = new byte[2];
					error[0] = new Integer(0xA1).byteValue();
					error[1] = 0x48;
					tempVector.add(error);
					totalLength += error.length;
				}
				else
				{
					tempVector.add(temp1);
					totalLength += temp1.length;
				}
			}
			if(i!=shiftInPos.length-1)
			{
				int nextStart = shiftInPos[i+1];
				if((nextStart-end)!=1)
				{
					int length = nextStart-end-1;
					byte[] temp = new byte[length];
					System.arraycopy(input,end+1,temp,0,temp.length);
					byte[] temp1 = HOST2PC(temp,code);
					if(!code[0].equals("0"))
					{
						MyDebug.errprt("parseAllBytes shiftInPos HOST2PC error code is --->" + code[0]);
						byte[] error = new byte[length];
						for(int ii=0;ii<length;ii++) error[ii] = 0x3F;
						tempVector.add(error);
						totalLength += error.length;
					}
					else
					{
						tempVector.add(temp1);
						totalLength += temp1.length;
					}
				}
			}
			else if(i==shiftInPos.length-1 && end<input.length-1)
			{
				int length = input.length-end-1;
				byte[] temp = new byte[length];
				System.arraycopy(input,end+1,temp,0,temp.length);
				byte[] temp1 = HOST2PC(temp,code);
				if(!code[0].equals("0"))
				{
					MyDebug.errprt("parseAllBytes shiftInPos end HOST2PC error code is --->" + code[0]);
					byte[] error = new byte[length];
					for(int ii=0;ii<length;ii++) error[ii] = 0x3F;
					tempVector.add(error);
					totalLength += error.length;
				}
				else
				{
					tempVector.add(temp1);
					totalLength += temp1.length;
				}
			}
		}
		byte[] result = new byte[totalLength];
		int pos = 0;
		for(int k=0;k<tempVector.size();k++)
		{
			byte[] answer = (byte[])tempVector.get(k);
			for(int kk=0;kk<answer.length;kk++) result[pos+kk] = answer[kk];
			pos += answer.length;
		}
		return result;
	}//parseAllBytes end
    /** 
     *  將Byte Array轉為Hex顯示
     *  @param Byte Array
     *  @return 轉為Hex值
     */	  	
	  public static String toHexString( byte[] bytes )
	  {
	      StringBuffer sb = new StringBuffer( bytes.length*2 );
	      for( int i = 0; i < bytes.length; i++ )
	      {
	          sb.append( toHex(bytes[i] >> 4) );
	          sb.append( toHex(bytes[i]) );
	      }

	      return sb.toString();
	  }
	  private static char toHex(int nibble)
	  {
	      final char[] hexDigit =
	      {
	          '0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'
	      };
	      return hexDigit[nibble & 0xF];
	  }	
	     /** @author Sox
	      *  @category 僅提供至多二個難字作轉換 CP937 -> unicodebig
	      *  將CP937的編碼方式轉為UnicodeBig的編碼方式
	      *  @param CP937編碼方式之byte array
	      *  @return 轉為UnicodeBig編碼後之字串
	      */	  
	public static String CP937toUnicodeBig(byte[] bytes )
	{
	String data;
	int iADD = 0;
	int record = 0;
	String bigcounter="";
	byte[] b = new byte[2];
	byte[] b1 = new byte[2];
	String name="";
	String tmp="";
	String tmp1="";
	int counter=0;
	String header="";
	String middle="";
	String tailer="";
//	讀字碼對應表
	try {
	//FileReader fileStream = new FileReader("C:\\Tomcat4.1\\webapps\\ROOT\\mapper.txt");
	FileReader fileStream = new FileReader("/TBB/nnb/cfg/mapper.txt");
	BufferedReader bufferedStream = new BufferedReader(fileStream);
	do {
		data = bufferedStream.readLine();
		if (data == null) {
			break;
		}
		iADD++;
	}
	while (true);
	fileStream.close();
	bufferedStream.close();
	}catch(Exception e) {
	}
//	字碼對應表存入陣列
	String[][] result = new String[iADD+1][2];
	try {
	//FileReader fileStream1 = new FileReader("C:\\Tomcat4.1\\webapps\\ROOT\\mapper.txt");
	FileReader fileStream1 = new FileReader("/TBB/nnb/cfg/mapper.txt");	
	BufferedReader bufferedStream1 = new BufferedReader(fileStream1);
	do {
		data = bufferedStream1.readLine();
		if (data == null) {
			break;
		}
		int token = -1;
		token = data.indexOf("=");
			if (token >= 0) {
				result[record][0] = data.substring(0, token);					
				data = data.substring(token + 1);
				result[record][1] = data.substring(0); 
			}
			else {
				break;
			}
		record++;
	}
	while (true);
	fileStream1.close();
	bufferedStream1.close();	
	}catch(Exception e) {
	}		
	try {
	String cp937tobig5=new String(bytes,"CP937");
	byte [] big5byte=cp937tobig5.getBytes("Big5");
//	System.out.println("Origin Big5 Byte value： ");
	for(int j=0;j<big5byte.length;j++)
	{
//			System.out.println(big5byte[j]);  
	}
//	System.out.println("Big5 Hex value："+toHexString(big5byte));	

//	找難字字數
	for(int i=0;i<big5byte.length;i++)
	{
		if(big5byte[i]==63)
		{
			counter++;
			if(counter>1)
				bigcounter=bigcounter+","+String.valueOf(i);
			else
				bigcounter=String.valueOf(i);	
	    }		
	}
//	存入難字索引位置
	int [] indexpositon=new int[counter];
	int tempi=0;
	for(int i=0;i<big5byte.length;i++)
	{
	   if(big5byte[i]==63)
	   {
	     indexpositon[tempi++]=i;   
	   }
	}
//	System.out.println("invalid word count:"+counter+" position index："+bigcounter);
	String big5string1=new String(big5byte,"Big5");
//	System.out.println("Origin big5 string："+big5string1);
	byte [] cp937byte=bytes;
//	System.out.println("Big5 to CP937："+ new String(cp937byte,"CP937"));
		
	if(counter==1)  //1 個難字的處理方法
	{
//		System.out.println("CP937 Byte value：");
		for(int i=0;i<record;i++)
		{
			for(int j=0;j<cp937byte.length;j++)
			{
//				System.out.println(cp937byte[j]);  
			}
			if(toHexString(cp937byte).indexOf(result[i][0])!=-1)
			{	
//				System.out.println("Mapping CP937 old="+result[i][0]+"--> UnicodeBig new="+result[i][1]);
				b[0] = (byte)Integer.parseInt(result[i][1].substring(0,2),16);//0x60;
				b[1] = (byte)Integer.parseInt(result[i][1].substring(2),16);//0x52;
			}
		}		
	    if(indexpositon[0]+1<big5byte.length)
		{
			if(indexpositon[0]==0)
			{
				tailer = new String(big5byte,"Big5").substring(1,(big5byte.length+1)/2);
				tmp = new String(b, "UnicodeBig");
				name = tmp + tailer;
	        }else {
				header = new String(big5byte,"Big5").substring(0,indexpositon[0]/2);
				tmp = new String(b, "UnicodeBig");
				tailer = new String(big5byte,"Big5").substring((indexpositon[0]/2)+1,(big5byte.length+1)/2);
				name = header + tmp + tailer;
			}		
		}else {
				header = new String(big5byte,"Big5").substring(0,indexpositon[0]/2);
				tmp = new String(b, "UnicodeBig");
				name = header + tmp;
		}
	}
	if(counter==2)  //2個難字的處理方法
	{
		int mappingcount=0;
		int position=-1;
//		System.out.println("CP937 Byte value：");
		String tempword=toHexString(cp937byte);
		for(int i=0;i<record;i++)
		{
			for(int j=0;j<cp937byte.length;j++)
			{
//				System.out.println(cp937byte[j]);  
			}
			position=tempword.indexOf(result[i][0]);
			if(tempword.indexOf(result[i][0])!=-1 && mappingcount==0)
			{	
//				System.out.println("Mapping CP937 old="+result[i][0]+"--> UnicodeBig new="+result[i][1]);
				b[0] = (byte)Integer.parseInt(result[i][1].substring(0,2),16);//0x60;
				b[1] = (byte)Integer.parseInt(result[i][1].substring(2),16);//0x52;
				mappingcount++;
				tempword=tempword.substring(position+result[i][0].length());
			}
			//tempword=tempword.substring(position+result[i][0].length());
			if(tempword.indexOf(result[i][0])!=-1 && mappingcount==1)
			{	
//				System.out.println("Mapping CP937 old="+result[i][0]+"--> UnicodeBig new="+result[i][1]);
				b1[0] = (byte)Integer.parseInt(result[i][1].substring(0,2),16);//0x60;
				b1[1] = (byte)Integer.parseInt(result[i][1].substring(2),16);//0x52;
				mappingcount++;
			}				
		}
//		System.out.println(" b:"+ new String(b, "UnicodeBig"));
//		System.out.println(" b1:"+ new String(b1, "UnicodeBig"));
	    if(indexpositon[0]+1<big5byte.length-1 && indexpositon[1]+1<big5byte.length)
		{
			if(indexpositon[0]==0 && indexpositon[1]==1)
			{
				tmp = new String(b, "UnicodeBig");
				tmp1 = new String(b1, "UnicodeBig");
				tailer = new String(big5byte,"Big5").substring(2);			
				name = tmp + tmp1 + tailer;
	        }else if(indexpositon[1]==indexpositon[0]+1){
				header = new String(big5byte,"Big5").substring(0,indexpositon[0]/2);
				tmp = new String(b, "UnicodeBig");
				tmp1 = new String(b1, "UnicodeBig");
				tailer = new String(big5byte,"Big5").substring((indexpositon[0]/2)+2,(big5byte.length+2)/2);
				name = header + tmp + tmp1 + tailer;
			}else {
				header = new String(big5byte,"Big5").substring(0,indexpositon[0]/2);
				tmp = new String(b, "UnicodeBig");
				middle = new String(big5byte,"Big5").substring((indexpositon[0]/2)+1,(indexpositon[1]/2)+1);
				tmp1 = new String(b1, "UnicodeBig");
				tailer = new String(big5byte,"Big5").substring(indexpositon[1]/2+2,(big5byte.length+2)/2);
				name = header + tmp + middle + tmp1 + tailer;		    
			}
		}else {
				if(indexpositon[0]==0 && indexpositon[1]+1==big5byte.length)
				{
					tmp = new String(b, "UnicodeBig");
					header = new String(big5byte,"Big5").substring(1,(indexpositon[1]/2)+1);
					tmp1 = new String(b1, "UnicodeBig");
					name = tmp + header + tmp1;				  
				}	
				if(indexpositon[0]==2 && indexpositon[1]+1==big5byte.length)
				{
					header = new String(big5byte,"Big5").substring(0,indexpositon[0]/2);
					tmp = new String(b, "UnicodeBig");
					middle = new String(big5byte,"Big5").substring((indexpositon[0]/2)+1,(indexpositon[1]/2)+1);
					tmp1 = new String(b1, "UnicodeBig");
					name = header + tmp + middle + tmp1;				  
				}
				if(indexpositon[0]==big5byte.length-2 && indexpositon[1]==big5byte.length-1)
				{
					header = new String(big5byte,"Big5").substring(0,indexpositon[0]/2);
					tmp = new String(b, "UnicodeBig");
					tmp1 = new String(b1, "UnicodeBig");
					name = header + tmp + tmp1;
				}
		}	
	}
//		System.out.println("CP937 Hex value："+toHexString(cp937byte));	
//		System.out.println("UnicodeBig Hex value："+toHexString(name.getBytes("UnicodeBig")));
//		System.out.println("CP937 to UnicodeBig--> "+name+"<BR>");
	}catch(Exception e) {
	}
	return name;
	}	
/*
	static public byte[] host2pcUnicode(byte[] in)
    {
        int length = in.length;
        
        String[] size={new String()};
        String[] code={new String()};
        byte[] out=HOST2PCUNICODE(in,code);
        if(code[0].equals("0"))
            return out;
        else
        {
        	MyDebug.errprt("host2pc error code is --->" + code[0]);
            Vector posData = checkShiftPair(in);
            String checkResult = (String)posData.get(0);
            if(checkResult.equals("true"))
            {
            	byte[] result = parseAllBytes(in,posData);
            	return result;
            }
            else
            {
                byte[] error = new byte[length];
                for(int i=0;i<length;i++) error[i] = 0x3F;
                return error;
            }
        }
    }//host2pc

	static public byte[] HOST2PCUNICODE(byte[] cvt_buffer, String[] return_code)
	{
		try
        {
            boolean chinese=false;
            byte[] unichar=new byte[cvt_buffer.length*2];
            ByteToCharConverter toUnicode= ByteToCharConverter.getConverter("CP937");
            CharToByteConverter fromUnicode= CharToByteConverter.getConverter("Big5");
            char[] input=toUnicode.convertAll(cvt_buffer);
            for(int i=0,j=0;i<input.length;i++,j+=2)
            {
            	unichar[j]=(byte)((input[i] & 0xff00) >> 8);
            	unichar[j+1]=(byte)(input[i] & 0x00ff);
            }
            new HexDump(HexDump.HD_EBCDIC | HexDump.HD_ASCII,"PC2HOST unicode",unichar,unichar.length);
            byte[] ascii = fromUnicode.convertAll( input );
            new HexDump(HexDump.HD_EBCDIC | HexDump.HD_ASCII,"PC2HOST big5",ascii,ascii.length);
            return_code[0]="0";
            //return answer;
            return unichar;
        }
		catch(Exception e)
        {
//            e.printStackTrace();
			MyDebug.errprt("ConvCode class HOST2PC error --->"+e.getStackTrace());
            
            //轉碼失敗, 回傳空白
            return_code[0]="0";
            byte[] ret = new byte[cvt_buffer.length];
            for(int i=0; i < ret.length; i++) 
            	ret[i] = 0x20;
            return ret;
        }
	}
	
	public static void main(String[] argv)
	{
		String ascii="黃綉鈺";
		byte[] ebcdic=new byte[ascii.length()];
		try
		{
			ebcdic=pc2host(ascii.getBytes());
		}
		catch(CommunicationException e)
		{
			
		}
		byte[] asciibyte=new byte[ascii.length()];
		byte[] ebcdic1=new byte[8];
		ebcdic1[0]=0x0e;
		ebcdic1[1]=(byte)0x5b;
		ebcdic1[2]=(byte)0xa6;
		ebcdic1[3]=(byte)0xd0;
		ebcdic1[4]=0x4b;
		ebcdic1[5]=(byte)0x7c;
		ebcdic1[6]=(byte)0xb8;
		ebcdic1[7]=0x0f;
		asciibyte=host2pcUnicode(ebcdic1);
	}
*/
}
