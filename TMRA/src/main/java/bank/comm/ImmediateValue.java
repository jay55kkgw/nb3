package bank.comm;

import java.util.*;
import java.net.*;
import org.apache.log4j.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
/**
  */
public class ImmediateValue implements Item
{
	String name;
	String messageType=null;
	
	public ImmediateValue( Node node )	//設此node為<STRING>
	{
		getNodeValue(node);
	}
	
	public String getValue(TelegramDataList template, String messageType , 
							Hashtable result, Hashtable preMessage)
	{
		return name;
	}
	
	public String getName()
	{
		return name;
	}

    public boolean hasValue()
    {
        if(name==null)
            return false;
        return true;
    }

    public String getMessageType()
    {
        return messageType;
    }
    
	public String toString()
	{
		return name;
	}
		
	private void getNodeValue(Node node)
	{
		name = node.getFirstChild().getNodeValue();
	}
}