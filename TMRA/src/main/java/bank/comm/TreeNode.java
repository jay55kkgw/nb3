package bank.comm;

import java.util.*;
import java.net.*;
import org.apache.log4j.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
/**
 * Syntax tree node
 * 				operator (and,or,not)
 *			   /		\
 *		   Left node   Right node
 * Left or Right maybe a TreeNode(if has sub-node) or Condition(if no sub-node)
 * @create 2005/07/26
 * @author 李國樞
 * @version 2.0
 */
public class TreeNode
{
	Object Left=null,Right=null;	// Left和Right可能是TreeNode|Condition
	String operator=null;			// operator必然是String(logical)
	//LogicOperand operand=null;		
	
	public String toString()
	{
		return "\n{TreeNode: Left=["+Left+"],operator=["+operator+ "],Right=["+Right+"]}\n";
	}

	public TreeNode(String op,Object up, Object down)
	{							// up,down是stack中的位置
		operator = op;
		Left = down;
		Right = up;
	}

	//for unary operator(ex. NOT), so Right=null
	public TreeNode(String op,Object down)
	{							// up,down是stack中的位置
		operator = op;
		Left = down;
		Right = null;	
	}

	public void addLeft(Object left)
	{
		Left = left;
	}
	
	public void addRight(Object right)
	{
		Right = right;
	}
	
	public void addOperator(String o)
	{
		operator=o;
	}
	
	public Object getLeft()
	{
		return Left;
	}
		
	public Object getRight()
	{
		return Right;
	}

	public String getOperator()
	{
		return operator;
	}
	
	public void getConditionValue(Hashtable conditionTable)
	{
		if(Left instanceof Condition)
			((Condition)Left).getConditionValue(conditionTable);
		else if(Left instanceof TreeNode)
			((TreeNode)Left).getConditionValue(conditionTable);

		if(Right instanceof Condition)
			((Condition)Right).getConditionValue(conditionTable);
		else if(Right instanceof TreeNode)
			((TreeNode)Right).getConditionValue(conditionTable);
	}
	
	boolean calculate(TelegramDataList template, String messageType, 
									Hashtable result, Hashtable preTITA)
	{
		boolean leftval,rightval;
		
		if(Left instanceof Condition)
			leftval = ((Condition)Left).calculate(template, messageType, result, preTITA);
		else if(Left instanceof TreeNode)
			leftval = ((TreeNode)Left).calculate(template, messageType, result, preTITA);
		else
			leftval = false;	//should throw exception
			
		if(Right instanceof Condition)
			rightval = ((Condition)Right).calculate(template, messageType, result, preTITA);
		else if(Right instanceof TreeNode)
			rightval = ((TreeNode)Right).calculate(template, messageType, result, preTITA);
		else
			rightval = false;	//should throw exception		
			
		if( ((String)operator).equalsIgnoreCase("AND") )
			return leftval && rightval;
		else if( ((String)operator).equalsIgnoreCase("OR") )
			return leftval || rightval;
		else if( ((String)operator).equalsIgnoreCase("NOT") )
			return !(leftval);
		else
			return false;		//should throw exception		
		/*else if( (String)operator.equalsIgnoreCase("NOT") )
			return !(Left.calculate(msg,list));*/
	}//calculate
}
