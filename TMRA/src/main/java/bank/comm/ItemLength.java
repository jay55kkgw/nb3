package bank.comm;

import java.util.*;
import java.net.*;
import org.apache.log4j.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
/**
	取得<ITEMID></ITEMID>定義的欄位之長度

	@Author 李國樞
	@Create 2005/8/10
	@Version 2.0
  */
public class ItemLength implements Item
{
	/*
		
	*/
	private String itemID=null, messageType=null;
	
	public ItemLength( Node node, String type)
	{
		messageType = type;
		getItemValue(node);
	}

	public String toString()
	{
		return itemID;
	}
		
	public ItemLength( Node node, String t, String type)
	{
		if(t.equals("parent") )
		{
			NodeList nodes = node.getChildNodes();
    	    for(int i=0; i<nodes.getLength(); i++)
        	{
	    		Node node1 = nodes.item(i);
				if(nodes.getLength()==1)	
					itemID = node1.getNodeValue();
		        if(node1.getNodeType() == Node.ELEMENT_NODE)
    		    {
        			if(node1.getNodeName().equals("ITEMLENGTH"))
						getItemValue(node1);
				}
			}
		}
		//else throw new Exception("Method argument error");
	}
	
	public String getMessageType()
	{
		return messageType;
	}

	public boolean hasValue()
	{
		return true;
	}
	public String getValue(TelegramDataList template, String messageType,Hashtable result, Hashtable preMessage)
	{
		TelegramData t = template.get(itemID);
		return Integer.toString(t.length);
	}//getValue

	private void getItemValue(Node node)
	{
		NodeList nodes = node.getChildNodes();
    	
        for(int i=0; i<nodes.getLength(); i++)
        {
	    	Node node1 = nodes.item(i);
	    	
	    	if(nodes.getLength()==1)	
	    		itemID = node1.getNodeValue();
	        else if(node1.getNodeType() == Node.ELEMENT_NODE)
    	    {
        		if(node1.getNodeName().equals("ITEMID"))
        		{
					Item id = new ItemID(node1);
					itemID = id.getValue(null,null,null,null);
				}
			}
		}
	}//getItemValue
	
	private String getItemAttributes(NamedNodeMap attrs)
	{
//System.out.println("attrs.getLength()="+attrs.getLength());
		for(int jj=0; jj<attrs.getLength(); jj++)
        {	//這個迴圈是為了取得ItemValue是否有定義MESSAGETYPE (TITA|TOTA)
        	Node attrNode = attrs.item(jj);
            if(attrNode.getNodeName().equals("MESSAGETYPE"))
            {	    	
            	return attrNode.getNodeValue();
            }
        }
        return null;		//error
	}//getItemAttributes
}