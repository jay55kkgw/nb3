package bank.comm;

import java.util.*;
import java.net.*;
import org.apache.log4j.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
/**
  */
public class SyntaxTree 
{
	Object root=null;
	
	public SyntaxTree()
	{
		
	}
	
	public void addRootNode(Object r)		//Operand : Condition | TreeNode
	{
		root = r;
		//System.out.println("Root="+root);
	}
	
	public boolean calculate(TelegramDataList template,	String messageType, 
											Hashtable result, Hashtable preTITA)
	{	// tree traversal
		if(root instanceof TreeNode)
			return ((TreeNode)root).calculate(template, messageType, result, preTITA);
		else 
			return ((Condition)root).calculate(template, messageType, result, preTITA);
	}
	
	public void getConditionValue(Hashtable conditionTable)
	{
	    if(root instanceof TreeNode)
	        ((TreeNode)root).getConditionValue(conditionTable);
	    else
	        ((Condition)root).getConditionValue(conditionTable);
	}
}//SyntaxTree
