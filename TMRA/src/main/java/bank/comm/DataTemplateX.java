package bank.comm;

import java.util.*;
import java.io.*;

import org.w3c.dom.*;

import com.netbank.util.ESAPIUtil;

import javax.xml.parsers.*;
import bank.classes.ArrayTool;
import lombok.extern.slf4j.Slf4j;

import org.apache.log4j.*;

/**
* DataTemplateX會將Telegram.xml讀入並解析以形成組成上行電文或處理下行電文的範本
* @author 林季穎  @version 1.0
* @modifier 李國樞  @version 2.0
*/

/*		幾個主要的資料結構
	**	(ArrayList)nameMaps[]	:只有TRANSACTION的TITA,TOTA,CLIENT_ENCODING,HOST_ENCODING attributes
	**	(TelegramDataList)TITA[]:
			此array每一個element存放TelegramData,而每個TelegramData裡有其name, type, keyValue, start, length, optional, refval, format, encoding, transfer  等attributes
	**	(TelegramDataList)TOTA[]		:
	**	(FlowObject)TITAFlow[]		
	**	(FlowObject)TOTAFlow[]	
*/
@Slf4j
public class DataTemplateX 
{
	public static String XML_FILE_PATH = "";
	public static String root = "./";
    static final String msgPath = "xmlConfig" + File.separator + "web" + File.separator + "HostMessages";
     /** 電文範本數量
     */    
    static final int SIZE = 1024;
    /** EAI平台程式紀錄器
     */    
    /**
        * 儲存電文交易代號
        */
    
    private static Map<String, Integer> pools = new HashMap();
    
    public static ArrayList[] nameMaps = new ArrayList[SIZE];
    /**
        * 儲存上行電文範本
        */
    public static TelegramDataList[] TITA = new TelegramDataList[SIZE];
    /**
        * 儲存下行電文範本
        */
    public static TelegramDataList[] TOTA = new TelegramDataList[SIZE];
    /**
     * 儲存外部變數
     */
	public static Hashtable ExternVAr = null;
    /**
        * 儲存上行電文處理流程
        */
    public static FlowObject[] TITAFlow = new FlowObject[SIZE];
    /**
        * 儲存下行電文處理流程
        */
    public static FlowObject[] TOTAFlow = new FlowObject[SIZE];
    /**
        * XML文件物件
        */
    private static Document document;
    /**
        * 同一交易名稱之下行電文中具有相同訊息代號卻有不同內容之範本數目
        */
    /**
        * 紀錄正在解析之交易的編號(由0開始)
        */
    static int nameMapIndex = 0;
    /**
        * 建立一DataTemplateX物件,當DataTemplateX物件建立時,會使用DOM parser解析Telegram.xml
        */
	
    
    private static Integer getMaxIndexFromPool() {
    	Integer result = 0;
    	for(Integer i : pools.values()) {
    		result = Math.max(i, result);
    	}
    	return result + 1;
    }
    
   	/**
     * 根據交易電文名稱,找出電文範本所在位置並回傳
     * @param txnName 交易電文名稱
     * @return 電文範本所在位置
     */
    public static int getID(String txnName)
    {	// 注意,此txnName必須含版本代號,電文長度(optional)
    	
   	
        int j=0;
        
        String txDefineFile = (txnName+".xml");
        
        Integer indexInPool = pools.get(txDefineFile);
        log.trace("indexInPool = {}",indexInPool);
        if(indexInPool == null) {
        	synchronized (DataTemplateX.class) {
        		MyDebug.debprt("pools.get(" + txDefineFile +")=" + pools.get(txDefineFile));
        		if(pools.get(txDefineFile) == null) {
	        		j = getMaxIndexFromPool();
	        		MyDebug.debprt("j = " + j);
	            	pools.put(txDefineFile, new Integer(j));
        		}
        		else {
        			j = pools.get(txDefineFile);
        		}
        	}
            try {
            	parseByDOM(txDefineFile , j);
            }
            catch(Exception e) {
//            	MyDebug.errprt("getID 1 :parseByDOM error!" + e.getMessage());
            	log.error("getID 1 :parseByDOM error!>>{}",e.toString());
            	return(0);
            }
            
            indexInPool = j;
        }
        /* 980518 每次load xml 電文檔        */
        else {
            try {
            	parseByDOM(txnName+".xml" , indexInPool);
            }
            catch(Exception e) {
            	log.error(e.toString());
            	return(0);
            }
        }
        log.trace("return indexInPool = {}",indexInPool);
        return indexInPool;

    }//getID
     /**
        * 利用Telegram.xml中的預設值建立上行電文中對應欄位之byte array,並根據資料型態補足至欄位指定長度<br>
        *若資料型態為X -> 左靠右補空白<br>
        *若資料型態為9 -> 右靠左補0<br>
        *若資料型態為H -> 16進位值<br>
        * @param length 欄位指定長度
        *@param type 資料型態
        *@param key Node物件,上行電文欄位在Telegram.xml中所對應的ITEM
        * @return 上行電文中對應欄位之byte array
        */
    private static byte[] initValue(int length, String type, Node key)
    {
        byte[] temp = new byte[length];
        if (type.equalsIgnoreCase("X") || type.equalsIgnoreCase("NTRIM"))
        {
            if (key.hasChildNodes())
            {
                temp = FlowObject.fitInTITAFormat(length, type, key.getFirstChild().getNodeValue());
            } 
            else 
            {
                for(int z=0; z<length; z++)
                    temp[z] = ' ';
            }
        } 
        else if (type.equalsIgnoreCase("9"))
        {
            if (key.hasChildNodes()){
                temp = FlowObject.fitInTITAFormat(length, type, key.getFirstChild().getNodeValue());
            } 
            else 
            {
                for(int z=0; z<length; z++)
                    temp[z] = '0';
            }
        } 
        else if (type.equalsIgnoreCase("H"))
        {
            if (key.hasChildNodes()){
                temp = key.getFirstChild().getNodeValue().getBytes();
                //temp = FlowObject.fitInTITAFormat(length, type, key.getFirstChild().getNodeValue());
            } 
            else 
            {
                for(int z=0; z<length; z++)
                    temp[z] = 0;
            }
        }
        return temp;
    }//initValue
    /**
     * 將下行電文中對應欄位之byte array轉為字串,並根據資料型態做適當處理<br>
     * 若資料型態為H -> 將下行電文資料轉為十進位值
     * @deprecated . 
     * @param target 資料型態
     * @param source 下行電文中對應欄位之byte array
     * @return 將轉換(處理)過後之資料轉為字串輸出
     */
    public static String formatResponse(String target, byte[] source)
    {
        String result = null;
        int length = source.length;
        int totalValue = 0;
        if (target.charAt(0)=='H') 
        {
            for (int i=0; i<source.length; i++)
            {
                int byteValue = 0;
                byte temp = source[i];
                if ( temp >=0 && temp<128)
                    byteValue = temp;
                else 
                    byteValue = temp+256;
               //.debug("Hex["+i+"]"+byteValue+" ");
                totalValue += byteValue*(1<<(--length)*8);
            }
            result = new Integer(totalValue).toString();
        } 
        else 
            result = new String(source);
        //center.debug(result);
        return result;
    }//formatResponse
    /**
     * 回傳DOM parser解析Telegram.xml後之Document物件
     * @return DOM parser解析Telegram.xml後之Document物件
     */
    public static Document getDOMDocument(){
    	return document;
    }
    
    /*public int getTOTAType(){
    	return TOTAType;
    }*/
   /**
     * 回傳指定交易電文之電文範本
     * @param id 電文範本所在位置
     * @param telegramType 指定範本型態,TITA表示回傳上行電文範本,TOTA表示回傳上行電文範本
     * @param mark 處理上,下行電文時所須之資料
     * @return 電文範本
     * @see bank.comm.TelegramDataList
     */
   /**
     * 使用DOM parser 解析指定的XML檔
     * @param xmlFileName 指定的XML檔
     * @throws Exception 當DOM parser解析失敗時,丟出Exception
     */
    private static void parseByDOM(String xmlFileName, int j) throws Exception
    {
        //DOMParser parser = new DOMParser();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
//        20190715 hugo XML External Entity Reference (XXE)
        factory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        factory.setFeature("http://xml.org/sax/features/external-general-entities", false);
        factory.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
        
        DocumentBuilder builder = factory.newDocumentBuilder();

//        String s = root  + "/" + msgPath + File.separator + xmlFileName;
//        TODO 先寫死測試
//        String s ="file:///G:/aFstop-SVN/臺企銀_新世代網路銀行應用系統建置案/工具/電文定義from舊網銀code/";
//        String s ="file:///D:/";
//        String s ="D:/"+xmlFileName;
        try {
			log.trace("XML_FILE_PATH>>{}",XML_FILE_PATH);
			String s =XML_FILE_PATH+xmlFileName;
			log.trace("String s>>{}",ESAPIUtil.vaildLog(s) );
//			MyDebug.debprt("xmlFileName = "+s);
			document = builder.parse(s);
			Element root = document.getDocumentElement();
			parseTelegram(root, j);
		} catch (Exception e) {
			log.error(e.toString());
		}
    }//parseByDOM
    /**
     * 將各交易之上行電文範本及下行電文範本分別儲存於兩個陣列之中
     * @param parentNode 父節點
     */
    private static void parseTelegram(Node node, int index)
    {
        ArrayList nameMap = new ArrayList();
        NamedNodeMap attrs = node.getAttributes();	
        Hashtable hash = new Hashtable();
        for (int j=0; j<attrs.getLength(); j++)
        {	// 讀出所有的attributes,要包括VERSION_NAME & VERSION (第4動,未進行) albert
        	Node attrNode = attrs.item(j);
        	if ( attrNode.getNodeName().equals("TITA")){
            	hash.put("TITA",attrNode.getNodeValue());
            	MyDebug.debprt("TITA = "+attrNode.getNodeValue());
            } else if (attrNode.getNodeName().equals("TOTA")){
            	hash.put("TOTA",attrNode.getNodeValue());
            } else if (attrNode.getNodeName().equals("CLIENT_ENCODING")){
            	hash.put("CLIENT_ENCODING",attrNode.getNodeValue());
            } else if (attrNode.getNodeName().equals("HOST_ENCODING")){
            	hash.put("HOST_ENCODING",attrNode.getNodeValue());
            } else if (attrNode.getNodeName().equals("VERSION")){
            	hash.put("VERSION",attrNode.getNodeValue());
            } else if (attrNode.getNodeName().equals("VERSION_NAME")){
            	hash.put("VERSION_NAME",attrNode.getNodeValue());
	        } else if (attrNode.getNodeName().equals("LENGTH")){
    			hash.put("LENGTH",attrNode.getNodeValue());
	        } else if (attrNode.getNodeName().equals("STREAM_TYPE")){
	           hash.put("STREAM_TYPE",attrNode.getNodeValue());
	        } else if (attrNode.getNodeName().equals("COMPONENT")){
	           hash.put("COMPONENT",attrNode.getNodeValue());
	        } else if (attrNode.getNodeName().equals("TOPMSG")){
		       hash.put("TOPMSG",attrNode.getNodeValue());
		       MyDebug.debprt("TOPMSG = "+attrNode.getNodeValue());
	        } else if (attrNode.getNodeName().equals("TOPMAXRECOD")){
			   hash.put("TOPMAXRECOD",attrNode.getNodeValue());
			   MyDebug.debprt("TOPMAXRECOD = "+attrNode.getNodeValue());
		    }
    	}
        String tita = (String)hash.get("TITA");
        String version = (String)hash.get("VERSION");
        String versionName = (String)hash.get("VERSION_NAME");
        String length = (String)hash.get("LENGTH");
        String STREAM_TYPE = (String)hash.get("STREAM_TYPE");
        String COMPONENT = (String)hash.get("COMPONENT");
        String ERRMSG = (String)hash.get("TOPMSG");
        String topmaxrecode = (String)hash.get("TOPMAXRECOD");
        tita = (version!=null) ? tita+version : tita;
        tita = (length!=null) ? tita+length : tita;

        nameMap.add(0,tita.trim());	//此時TITA,TOTA裡存放的是交易代號
        nameMap.add(1,hash.get("TOTA"));
        nameMap.add(2,hash.get("CLIENT_ENCODING"));
        nameMap.add(3,hash.get("HOST_ENCODING"));
        nameMap.add(4,hash.get("TOPMSG"));
		if(topmaxrecode!=null)
		{
        	nameMap.add(5,topmaxrecode);
		}
		else
		{
			topmaxrecode="";
			nameMap.add(5,"");
		}
        if(version!=null)
        	nameMap.add(6,version);		// 版本號碼
        if(versionName!=null)
        	nameMap.add(7,versionName);	// 版本號碼欄位名稱 :土銀PTYPE
		if(length!=null)
        	nameMap.add(8,length);		// TITA電文長度,用來區分相同電文代號、相同版號,但不同長度電文內容
		
        nameMaps[index] = nameMap;
        
        FlowObject[] temp = makeFlowData(node,tita,STREAM_TYPE,COMPONENT,topmaxrecode);
        TITAFlow[index] = temp[0];
        TOTAFlow[index] = temp[1];
        makeTwoArrays(node, index);
		//nameMapIndex ++;
    } //parseTelegram
   /**
     * 為parseTelegram(Node parentNode)所呼叫,解析處理上下行電文時所須之流程及條件
     * @param parentNode 父節點
     * @return 同一交易名稱之下行電文中具有相同訊息代號卻有不同內容之範本數目
     */
    private static void makeTwoArrays(Node parentNode, int index)
    {	
        NodeList nodes = parentNode.getChildNodes();
        TelegramDataList TITADataList = new TelegramDataList(),TOTADataList =new TelegramDataList();
        
        for(int i=0; i<nodes.getLength(); i++)
        {
            Node node = nodes.item(i);
            if(node.getNodeType() == Node.ELEMENT_NODE)
            {
                if(node.getNodeName().equals("TITA"))
                {	
                    NodeList keys = node.getChildNodes();
                    int start =0;
                    for (int j=0; j<keys.getLength(); j++)
                    {
                        Node key = keys.item(j);
                        if(key.getNodeName().equals("ITEM") && key.getNodeType() == Node.ELEMENT_NODE )
                        {	// 一般ITEM
							NamedNodeMap keyAttrs = key.getAttributes();
							Node keyDesc = keyAttrs.getNamedItem("DESC");
							Node keyName = keyAttrs.getNamedItem("NAME");
                            Node keyType = keyAttrs.getNamedItem("TYPE");
                            Node keyLength = keyAttrs.getNamedItem("LENGTH");
                            Node keyOptional = keyAttrs.getNamedItem("OPTIONAL");
                            Node keyRefVal = keyAttrs.getNamedItem("REFVAL");
                            Node keyFormat = keyAttrs.getNamedItem("FORMAT");
                            Node keyEncoding = keyAttrs.getNamedItem("ENCODING");
                            Node keyTransfer = keyAttrs.getNamedItem("TRANSFER");
                            Node keyDatasemantic = keyAttrs.getNamedItem("DATASEMANTIC");
                            Node keyDatasemanticval = keyAttrs.getNamedItem("DATASEMANTICVAL");
                            String desc = keyDesc.getNodeValue();
//                            System.out.println("TITA DESC = "+desc);
                            String type = keyType.getNodeValue();
                            int length = Integer.parseInt(keyLength.getNodeValue());
                            String name = keyName.getNodeValue();
                            byte[] temp = initValue(length, type, key);
                            String keyValue = new String(temp);
                            String optional = keyOptional.getNodeValue();
                            String refval = keyRefVal.getNodeValue();
                            String format = keyFormat.getNodeValue();
                            String encoding = keyEncoding.getNodeValue();
                            String transfer = keyTransfer.getNodeValue();
                            String datasemantic = keyDatasemantic==null ? null : keyDatasemantic.getNodeValue();
                            String datasemanticval = keyDatasemanticval==null ? null : keyDatasemanticval.getNodeValue();
//                            System.out.println("name = "+name+",type = "+type+",start = "+start+",length = "+length);
//                            System.out.println("real length = "+temp.length);
                            TelegramData telegramData = new TelegramData(name, type, keyValue, start, length, 
                                optional, refval, format, encoding, transfer, desc, datasemantic, datasemanticval);
                            TITADataList.add(telegramData);	//放到TelegramDataList
                            start += length;
                        }//if ITEM
                    }//for each Element
                }// if TITA
                else if(node.getNodeName().equals("TOTA"))
                {	//處理TOTA電文
                    NodeList keys = node.getChildNodes();
                    int start =0;
					for(int j=0; j<keys.getLength(); j++)
                    {	//取得該TOTA下所有的elements (ITEM tag)
                        Node key = keys.item(j);
                        if(key.getNodeName().equals("ITEM") && key.getNodeType() == Node.ELEMENT_NODE )
                        {	// 一般ITEM
							NamedNodeMap keyAttrs = key.getAttributes();
							Node keyDesc = keyAttrs.getNamedItem("DESC");
							Node keyName = keyAttrs.getNamedItem("NAME");
                            Node keyType = keyAttrs.getNamedItem("TYPE");
                            Node keyLength = keyAttrs.getNamedItem("LENGTH");
                            Node keyOptional = keyAttrs.getNamedItem("OPTIONAL");
                            Node keyRefVal = keyAttrs.getNamedItem("REFVAL");
                            Node keyFormat = keyAttrs.getNamedItem("FORMAT");
                            Node keyEncoding = keyAttrs.getNamedItem("ENCODING");
                            Node keyTransfer = keyAttrs.getNamedItem("TRANSFER");
                            Node keyDatasemantic = keyAttrs.getNamedItem("DATASEMANTIC");
                            Node keyDatasemanticval = keyAttrs.getNamedItem("DATASEMANTICVAL");
                            String desc = keyDesc.getNodeValue();
//                            System.out.println("TOTA DESC = "+desc);
                            String type = keyType.getNodeValue();
                            int length = Integer.parseInt(keyLength.getNodeValue());
                            String name = keyName.getNodeValue();
                            byte[] temp = initValue(length, type, key);
                            String keyValue = new String(temp);
                            String optional = keyOptional.getNodeValue();
                            String refval = keyRefVal.getNodeValue();
                            String format = keyFormat.getNodeValue();
                            String encoding = keyEncoding.getNodeValue();
                            String transfer = keyTransfer.getNodeValue();
                            String datasemantic = keyDatasemantic==null ? null : keyDatasemantic.getNodeValue();
                            String datasemanticval = keyDatasemanticval==null ? null : keyDatasemanticval.getNodeValue();
                            //System.out.println("name = "+name+",type = "+type+",start = "+start+",length = "+length);
                            //System.out.println("real length = "+temp.length);
                            TelegramData telegramData = new TelegramData(name, type, keyValue, start, length, 
                                optional, refval, format, encoding, transfer, desc, datasemantic, datasemanticval);
                            TOTADataList.add(telegramData);	//放到TelegramDataList
                            start += length;
                        }//if ITEM
                    }//for each Element
                }//if TOTA
            }//if
        }//for
        TOTA[index] = TOTADataList;
		TITA[index] = TITADataList;     //TITA[]是一Hashtable ARRAY             
    }//makeTwoArrays
    /**
     * 為parseTelegram(Node parentNode)所呼叫,解析處理上下行電文時所須之流程及條件
     * @param node 父節點
     *@return 上行電文處理流程及下行電文處理流程
     */
    private static FlowObject[] makeFlowData(Node node, String tita, String STREAM_TYPE,String COMPONENT,String topmaxrecode)
    {
    	FlowObject []result=new FlowObject[2];
//		System.out.println("STREAM_TYPE="+STREAM_TYPE+",COMPONENT="+COMPONENT);
      	try
      	{
        	NodeList nodes = node.getChildNodes();
        	for(int i=0; i<nodes.getLength(); i++)
        	{
	        	Node node1 = nodes.item(i);
	            if (node1.getNodeType() == Node.ELEMENT_NODE)
    	        {
        	    	if(node1.getNodeName().equals("FLOW"))
            	    {	//確定是FLOW
                		NodeList nodes1 = node1.getChildNodes();
                    	for(int j=0;j<nodes1.getLength();j++)
	                    {
    	                	Node node2 = nodes1.item(j);
        	                if (node2.getNodeType() == Node.ELEMENT_NODE)
            	            {	
                	        	if(node2.getNodeName().equals("TITAFLOW"))
                    	        {	//確定是TITAFLOW
    								result[0] = new FlowObject(node2,"TITA",tita,STREAM_TYPE,COMPONENT,topmaxrecode,ExternVAr);	// 這是start constructor
//    								System.out.println("result[0]=" + result[0]);
    							}
	    						else if(node2.getNodeName().equals("TOTAFLOW"))
    	                        {	//確定是TOTAFLOW
    								result[1] = new FlowObject(node2,"TOTA",tita,STREAM_TYPE,COMPONENT,topmaxrecode,ExternVAr);
//    								System.out.println("result[1]=" + result[1]);
    							}
    						}
    					}//for each child of <FLOW>
	    			}//if <FLOW> end
        	    	else if(node1.getNodeName().equals("EXVAR"))
        	    	{
        	    		NodeList nodes1 = node1.getChildNodes();
        	    		if(nodes1.getLength()>0)
        	    			ExternVAr=new Hashtable();
        	    		for(int j=0;j<nodes1.getLength();j++)
        	    		{
        	    			Node key = nodes1.item(j);
        	    			if (key.getNodeName().equals("VAR") && key.getNodeType() == Node.ELEMENT_NODE)
        	    			{
        	    				NamedNodeMap keyAttrs = key.getAttributes();
        	    				Node keyName = keyAttrs.getNamedItem("NAME");
                                Node keyType = keyAttrs.getNamedItem("TYPE");
                                Node keyValue = keyAttrs.getNamedItem("VALUE");
                                VarType vartype=new VarType();
                                vartype.VarNAme=keyName.getNodeValue();
                                vartype.VarType=keyType.getNodeValue();
                                vartype.VarVAlue=keyValue.getNodeValue();
                                ExternVAr.put(keyName.getNodeValue(), vartype);
        	    			}
        	    		}
        	    	}
    			}//if
    		}//for
    	}catch(Exception e)
	  	{
//	  		MyDebug.errprt("makeFlowData error" + e.toString());
	  		log.error("makeFlowData error>>{}", e.toString());
	  	}
 		return result;
    }//makeFlowData
}//DataTemplateX