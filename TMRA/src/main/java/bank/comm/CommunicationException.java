package bank.comm;


/**
* 當與中心溝通或收發(處理)電文出現錯誤時,提供錯誤訊息
 * @author 林季穎
 * @version 1.0
 */

public class CommunicationException extends Exception{
    /**
     *中心下傳之錯誤代碼
     */
    String errorCode=null;
    /**
     *例外類型,對應DB COMM_CODE_TBL中訊息類別(CODE_TYPE欄位)
     */
    String codeType = null;
    /**
     *自中心接收之錯誤原因描述
     */
    String hostErrorMessage="";
    /**
        *上行電文(ASCII)
     */
    String TITAString = "";
    /**
        * 建立一CommunicationException物件,例外原因描述預設為null
        */
    public CommunicationException(){
        super();
    }
    /**
        * 建立一帶有例外原因描述的CommunicationException物件
        * @param reason 關於例外原因的描述
        */
    public CommunicationException(String reason) {
        super(reason);
    }
    /**
        * 建立一帶有例外原因描述及上行電文(ASCII)的CommunicationException物件
        * @param reason 關於例外原因的描述
        *@param TITAString 上傳中心之上行電文
        */
    public CommunicationException(String reason, String TITAString) {
        super(reason);
        this.TITAString = TITAString;
    }
    /**
        * 建立一帶有例外類型,中心下傳錯誤代碼及中心下傳錯誤原因描述的CommunicationException物件
        * @param codetype 例外類型,對應DB COMM_CODE_TBL中訊息類別(CODE_TYPE欄位)
        *@param code 自中心接收之錯誤代碼
        *@param hostErrorMessage 自中心接收之錯誤原因描述
        */
    public CommunicationException(String codetype, String code, String hostErrorMessage){
        codeType = codetype;
        errorCode=code;
        this.hostErrorMessage = hostErrorMessage;
    }
    /**
        * 建立一帶有例外類型,中心下傳錯誤代碼,中心下傳錯誤原因描述及上行電文(ASCII)的CommunicationException物件
        * @param codetype 例外類型,對應DB COMM_CODE_TBL中訊息類別(CODE_TYPE欄位)
        *@param code 自中心接收之錯誤代碼
        *@param hostErrorMessage 自中心接收之錯誤原因描述
        *@param TITAString 上傳中心之上行電文
        */
    public CommunicationException(String codetype, String code, String hostErrorMessage, String TITAString){
        codeType = codetype;
        errorCode=code;
        this.hostErrorMessage = hostErrorMessage;
        this.TITAString = TITAString;
    }
    /**
        * 檢視該CommunicationException物件是否帶有中心下傳之錯誤代碼
        * @return 當該CommunicationException物件帶有中心下傳之錯誤代碼傳回true,反之則傳回false
        */
    public boolean isErrorCode(){
        if (errorCode!=null) return true;
        else return false;
    }
    /**
        * 取得該CommunicationException物件之中心下傳錯誤代碼
        * @return 中心下傳之錯誤代碼
        */  
    public String getErrorCode(){
        return errorCode;
    }
    /**
        * 取得該CommunicationException物件之例外類型
        * @return 例外類型,對應DB COMM_CODE_TBL中訊息類別(CODE_TYPE欄位)
        */  
    public String getCodeType(){
        return codeType;
    }
    /**
        * 將中心下傳錯誤原因描述指派該CommunicationException物件
        * @param error 中心下傳錯誤原因描述
        */    
    public void setHostErrorMessage(String error){
        hostErrorMessage=error;
    }
    /**
        * 取得該CommunicationException物件之中心下傳錯誤原因描述
        * @return 中心下傳錯誤原因描述
        */    
    public String getHostErrorMessage(){
        return hostErrorMessage;
    }
    /**
        * 取得該CommunicationException物件之上行電文(ASCII)
        * @return 上行電文(ASCII)
        */  
    public String getTITA(){
        return TITAString;
    }
    
    @Override
	public String getMessage() {
    	StringBuffer result = new StringBuffer();
    	result.append("codetype: " + codeType).append(",");
    	result.append("errorCode: " + errorCode).append(",");
    	result.append("hostErrorMessage: " + hostErrorMessage).append("");
    	
		return result.toString();
	}
}
