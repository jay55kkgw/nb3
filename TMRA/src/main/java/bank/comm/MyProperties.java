package bank.comm;

import java.net.*;
import java.io.*;
import java.util.*;

public class MyProperties extends Properties
{
	BufferedReader buffer=null;
	static HashMap propItem = null;
	String bank;
		
	public MyProperties(String bank)
	{
		this.bank = bank;
	}
	public void load(FileInputStream f) throws UnsupportedEncodingException,IOException
	{
        if(propItem!=null)  return;
        else                propItem=new HashMap();
		String line=null;
		buffer = new BufferedReader( new InputStreamReader(f,"MS950") );
//		buffer = new BufferedReader( new InputStreamReader(f,"CP937") );
		while( (line=buffer.readLine())!=null)
		{
//			System.out.println("line="+line.substring(0,3));
			if(line.substring(0,3).equals("001"))
			{
				byte []tmp=line.getBytes();
				this.printOut("001-1 myproperties load",tmp);
			}
			int offset = line.indexOf("=");
			String propName = line.substring(0,offset);
			String propData = line.substring(offset+1);
//			this.printOut("propData myproperties load",propData.getBytes());
			propItem.put(propName,propData);
		}
	}	
		
	public String getProperty(String name)
	{
		String r= (String)propItem.get(name);
		return r;
	}
	
	public String toString()
	{
		String r="";
		Set s = propItem.entrySet();
		Object []a = s.toArray();
		for(int i=0; i<a.length; i++)
			r = r+"\n\n"+a[i].toString();
		return r;
	}
	
	public String toHexString(byte[] block) 
	{
		StringBuffer buf = new StringBuffer("");
		int len = block.length;
		for (int i = 0; i < len; i++)
		{
			byte2hex(block[i], buf);
			if ((i+1)%16==0)
				buf.append("\n");
			else if ( (i+1)%4==0 )
			{
				buf.append(" ");
			}
		}
		return buf.toString();
	}
     /**
      * 將傳入的byte值轉為十六進位並將其轉成字元存於一StringBuffer物件中
      *@param b 被轉換的byte
      *@param buf 儲存轉換結果之StringBuffer物件
      */
     private void byte2hex(byte b, StringBuffer buf) 
     {
          char[] hexChars = { '0', '1', '2', '3', '4', '5', '6', '7', '8',
          '9', 'A', 'B', 'C', 'D', 'E', 'F' };
          int high = ((b & 0xf0) >> 4);
          int low = (b & 0x0f);
          buf.append(hexChars[high]);
          buf.append(hexChars[low]);
     }
     
	public void printOut(String title,byte[] TOTA)
	{
        System.out.println("-->" + title + " Data in ASCII.....");
        System.out.println(" 0 1 2 3  4 5 6 7  8 9 A B  C D E F");
        System.out.println("--------|--------|--------|--------|");
        System.out.println(this.toHexString(TOTA));
    }


}// end MyProperties

