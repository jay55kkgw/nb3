package bank.classes;
/*
 * ArrayTool.java
 *
 * Created on 2002年3月4日, 上午 9:26
 */

/** 此為一Utility Class其主要功能為提供額外的Java array功能供使用者使用。
 *
 * @author Richard
 * @version 1.0
 */
public class ArrayTool {
     /** 此為ByteArray的指位器，所指的位置是最後處理的位置。
      */     
    private int counter;
    
    /** 建立一ArrayTool物件。 
      */
    public ArrayTool() {
    }
    /** 此方法主要是依傳入之啟始位置和長度，截取傳入之來源ByteArray再將其傳回。
     * @return 截取後所得的ByteArray。
     * @param source 欲截取之來源ByteArray。
     * @param start 欲截取之啟始位置。
     * @param length 欲截取之長度。
     */    
    public static byte[] subarray(byte[] source, int start, int length){
        byte[] result=new byte[length];
        for (int i=0;i<length;i++) result[i]=source[start+i];
        return result;
    }
    /** 此方法主要是依傳入之啟始位置，截取傳入之來源ByteArray再將其傳回。
     * @return 截取後所得的ByteArray。
     * @param source 欲截取之來源ByteArray。
     * @param start 欲截取之啟始位置。
     */    
     public static byte[] subarray(byte[] source, int start){
        return subarray(source,start,source.length-start+1);
    }
    // This method is not ThreadSafe
    /** 此方法主要是將傳入之來源ByteArray加到目地ByteArray。(此方法非ThreadSafe方法)
      * @param target 目地ByteArray。
     * @param source 來源ByteArray。
     */    
     public void addOn(byte[] target,byte[] source){
        for (int i=0;i<source.length;i++) target[i+counter]=source[i];
        counter+=source.length;
    }
}
