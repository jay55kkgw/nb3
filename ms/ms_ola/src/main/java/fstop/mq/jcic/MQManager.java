package fstop.mq.jcic;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.model.Rows;
import lombok.extern.slf4j.Slf4j;

import com.ibm.mq.*;
import com.ibm.mq.constants.MQConstants;
import com.ibm.mq.constants.CMQC;

@Slf4j
public class MQManager {

	public MQQueueManager getQueueManagerConnection(String IP) throws MQException {
		MQQueueManager Qmgr=null;
		try{		
			int 
			connectionoptions = MQConstants.MQCNO_NONE;
			String QmgrNM="J050";

			MQEnvironment.hostname = IP;//"10.16.22.49";
			//MQEnvironment.hostname = "127.0.0.1";
			MQEnvironment.CCSID = 950;
			MQEnvironment.port = 1414;
			MQEnvironment.channel = "SYSTEM.DEF.SVRCONN";
			
			Qmgr=new MQQueueManager(QmgrNM,connectionoptions);
		}catch(Exception e){
			log.error(e.getMessage());
		}
		return Qmgr;

	} // End getQueueManagerConnection

	
	public boolean putQueue(MQQueueManager Qmgr,String QName,String QMsg) throws Exception{
		MQQueue putQueue =null;
		boolean flag = false;
		try{
			putQueue =Qmgr.accessQueue(QName,
					MQConstants.MQOO_OUTPUT
					| MQConstants.MQOO_INQUIRE
					| MQConstants.MQOO_FAIL_IF_QUIESCING);
	
			
			MQPutMessageOptions pmo = new MQPutMessageOptions();
	
			pmo.options = 
					MQConstants.MQPMO_FAIL_IF_QUIESCING | 
					MQConstants.MQPMO_NEW_MSG_ID |
					MQConstants.MQPMO_NO_SYNCPOINT;
			
			MQMessage putMessage = new MQMessage();
			putMessage.characterSet = 950;
			putMessage.format = MQConstants.MQFMT_STRING;
			putMessage.writeString(QMsg);
			
			putQueue.put(putMessage,pmo);
			flag=true;
		}catch (MQException e) {
			log.error("putQueue error >> {}",e);
		}catch(Exception e){
			log.error(e.getMessage());
		}finally{
			if(putQueue!=null)	putQueue.close();
		}
		return flag;
	}
	

	//e.reasonCode == MQException.MQRC_NO_MSG_AVAILABLE
	public MVH getQueue(MQQueueManager Qmgr,String QName,String TRANSTYPE) throws Exception{
		MQQueue getQueue =null;
		Map rm = new HashMap();
		Rows result = new Rows();
		MVHImpl result1 = null;
		int cnt=0;
		int maxCNT=100;
		try{
			getQueue =Qmgr.accessQueue(QName,
					MQConstants.MQOO_INPUT_SHARED | 
					MQConstants.MQOO_FAIL_IF_QUIESCING | 
					MQConstants.MQOO_BROWSE | 
					MQConstants.MQOO_INQUIRE);	
			
			MQGetMessageOptions gmo = new MQGetMessageOptions();

			gmo.waitInterval =2000;
			gmo.matchOptions =MQConstants.MQMO_NONE;
			gmo.options = getopt("MQGMO_BROWSE_FIRST");
			
			MQMessage getMessage = new MQMessage();
			getQueue.get(getMessage,gmo);
			int queuecnt=getQueue.getCurrentDepth();
			log.info("Queue DEPTH>>>"+queuecnt);
			for(int i=0;i<queuecnt&&getQueue.getCurrentDepth()>0;i++)
			{
				getQueue.get(getMessage,gmo);
				byte[] byt = new byte[getMessage.getMessageLength()];
				getMessage.readFully(byt);
				String msgt = new String(byt);
				//log.info(getMessage.messageId);
				log.info(i+">>>"+msgt);
				if(TRANSTYPE.equals("H")) {
					if(msgt.indexOf("NNB")>-1 && msgt.indexOf("HTML")>-1)
					{	 
						rm.put("JCICSTRING", msgt);
						//log.info("HZ21 Hashmap JCICSTRING:"+msgt);
						Row r = new Row(rm);
						result.addRow(r);
					}
					else
					{
						if(msgt.indexOf("ETABS")>-1)
							putQueue(Qmgr,"ETABS.OUT",msgt);
						else
							putQueue(Qmgr,"Q050IF01",msgt);
					
					}
				}
				if(TRANSTYPE.equals("Q")) {
					if(msgt.indexOf("NNB")>-1 && msgt.indexOf("RZ21")>-1)
					{	 
						rm.put("JCICSTRING", msgt);
						//log.info("QZ21 Hashmap JCICSTRING:"+msgt);
						Row r = new Row(rm);
						result.addRow(r);
					}
					else
					{
						if(msgt.indexOf("ETABS")>-1)
							putQueue(Qmgr,"ETABS.OUT",msgt);
						else
							putQueue(Qmgr,"Q050IF01",msgt);
					
					}
				}				
				gmo.options = getopt("MQGMO_MSG_UNDER_CURSOR");
				getQueue.get(getMessage,gmo);				
				getMessage.clearMessage();
				gmo.options = getopt("MQGMO_BROWSE_NEXT");				
			}
			/*
			while(getQueue.getCurrentDepth()>0 && cnt<maxCNT){
				getQueue.get(getMessage,gmo);
				byte[] byt = new byte[getMessage.getMessageLength()];
				getMessage.readFully(byt);
				String msgt = new String(byt);
				//log.info(getMessage.messageId);
				log.info(cnt+">>>"+msgt);
				if(msgt.indexOf("NNB")>-1 && msgt.indexOf("HTML")>-1)
				{ 
					rm.put("JCICSTRING", msgt);
					log.info("Hashmap JCICSTRING:"+msgt);
					Row r = new Row(rm);
					result.addRow(r);
					cnt++;
				}
				else
				{
					putQueue(Qmgr,"TBBJCIC.OUT",msgt);
					putQueue(Qmgr,"ETABS.OUT",msgt);
				}
				gmo.options = getopt("MQGMO_MSG_UNDER_CURSOR");
				getQueue.get(getMessage,gmo);				
				getMessage.clearMessage();
				gmo.options = getopt("MQGMO_BROWSE_NEXT");
			}
			*/
			result1 = new MVHImpl(result);
		}catch (MQException e) {
			if(e.reasonCode==2033) {
				log.info("===================="+QName+":佇列是空的");
			}else {
				log.error("getQueue error >> {}",e);
			}
		}catch(Exception e){
			log.error(e.getMessage());
		}finally{
			if(getQueue!=null)	getQueue.close();
		}
		return result1;
	}
	

	public  String getQueue2(MQQueueManager Qmgr,String QName) throws Exception{
		MQQueue getQueue =null;
		int cnt=0;
		int maxCNT=1;
		String msgt="";
		try{
			getQueue =Qmgr.accessQueue(QName,
					MQConstants.MQOO_INPUT_SHARED | 
					MQConstants.MQOO_FAIL_IF_QUIESCING | 
					MQConstants.MQOO_BROWSE | 
					MQConstants.MQOO_INQUIRE);	
			
			MQGetMessageOptions gmo = new MQGetMessageOptions();

			gmo.waitInterval =2000;
			gmo.matchOptions =MQConstants.MQMO_NONE;
			gmo.options = getopt("MQGMO_BROWSE_FIRST");
			
			MQMessage getMessage = new MQMessage();
			getQueue.get(getMessage,gmo);
			log.info("Queue DEPTH>>>"+getQueue.getCurrentDepth());
			
			if(getQueue.getCurrentDepth()>0 && cnt<maxCNT){
				getQueue.get(getMessage,gmo);
				byte[] byt = new byte[getMessage.getMessageLength()];
				getMessage.readFully(byt);
				msgt = new String(byt);
				//log.info(getMessage.messageId);
				//log.info(cnt+">>>"+msgt);
				cnt++;
				gmo.options = getopt("MQGMO_MSG_UNDER_CURSOR");
				getQueue.get(getMessage,gmo);				
				getMessage.clearMessage();
				gmo.options = getopt("MQGMO_BROWSE_NEXT");
			}
		}catch (MQException e) {
			if(e.reasonCode==2033) {
				log.info("===================="+QName+":佇列是空的");
			}else {
				log.error("getQueue2 error >> {}",e);
			}
		}catch(Exception e){
			log.error(e.getMessage());
		}finally{
			if(getQueue!=null)	getQueue.close();
			}
		return msgt;
	}
	
	private int getopt(String strmqg) {
		int gopt = 0;
		if(strmqg.equals("MQGMO_BROWSE_FIRST")){ //讀取第一筆
			gopt = MQConstants.MQGMO_WAIT | MQConstants.MQGMO_BROWSE_FIRST;
		}else if(strmqg.equals("MQGMO_BROWSE_NEXT")){ //讀取下一筆
			gopt = MQConstants.MQGMO_WAIT | MQConstants.MQGMO_BROWSE_NEXT;
		}else if(strmqg.equals("MQGMO_MSG_UNDER_CURSOR")){ //讀取後即刪
			gopt = MQConstants.MQGMO_WAIT | MQConstants.MQGMO_FAIL_IF_QUIESCING | MQConstants.MQGMO_MSG_UNDER_CURSOR;
		}
		gopt =MQConstants.MQGMO_NO_SYNCPOINT |gopt;
//		: MQC.MQGMO_NO_SYNCPOINT;
		return gopt;
	}
	public MVH getQueue1(MQQueueManager Qmgr,String QName) throws Exception{
		MQQueue getQueue =null;
		Map rm = new HashMap();
		Rows result = new Rows();
		int maxCNT = 100;
		int cnt=0;
		try{
			getQueue =Qmgr.accessQueue(QName,
					MQConstants.MQOO_INPUT_SHARED | 
					MQConstants.MQOO_FAIL_IF_QUIESCING | 
					MQConstants.MQOO_BROWSE | 
					MQConstants.MQOO_INQUIRE);	
			
			MQGetMessageOptions gmo = new MQGetMessageOptions();

			gmo.waitInterval =2000;
			gmo.matchOptions =MQConstants.MQMO_NONE;
			gmo.options = getopt("MQGMO_BROWSE_FIRST");
			
			MQMessage getMessage = new MQMessage();
			getQueue.get(getMessage,gmo);
			log.info("Queue1 DEPTH>>>"+getQueue.getCurrentDepth());
			
			while(getQueue.getCurrentDepth()>0 && cnt<maxCNT){
				getQueue.get(getMessage,gmo);
				byte[] byt = new byte[getMessage.getMessageLength()];
				getMessage.readFully(byt);
				String msgt = new String(byt);
				//log.info(getMessage.messageId);
				log.info(cnt+">>>"+msgt);
				rm.put("JCICSTRING", msgt);			
				Row r = new Row(rm);
				result.addRow(r);
				cnt++;
				gmo.options = getopt("MQGMO_MSG_UNDER_CURSOR");
				getQueue.get(getMessage,gmo);				
				getMessage.clearMessage();
				gmo.options = getopt("MQGMO_BROWSE_NEXT");
			}
			MVHImpl result1 = new MVHImpl(result);
			return result1;
		}catch (MQException e) {
			if(e.reasonCode==2033) {
				log.info("===================="+QName+":佇列是空的");
			}else {
				log.error("getQueue1 error >> {}",e);
			}
		}catch(Exception e){
			log.error(e.getMessage());
		}finally{
			if(getQueue!=null)	getQueue.close();
			}
		return null;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public MVH getQueueNew(MQQueueManager Qmgr,String fromQueueName,String cusidn,String TRANSTYPE) throws Exception{
		MQQueue getQueue =null,putQueue = null;
		Map rm = new HashMap();
		Rows result = new Rows();
		MVHImpl result1 = null;
		int cnt = 0,maxCNT =  0,totalmaxCNT = 0;
		try{
			getQueue = Qmgr.accessQueue(fromQueueName,CMQC.MQOO_INPUT_SHARED | CMQC.MQOO_FAIL_IF_QUIESCING | CMQC.MQOO_BROWSE | CMQC.MQOO_INQUIRE);	
			
			MQGetMessageOptions gmo = new MQGetMessageOptions();

			gmo.waitInterval =2000;
			gmo.matchOptions =CMQC.MQMO_NONE;
			gmo.options = getoptNew("MQGMO_BROWSE_FIRST");//預設為只看不取走
			
			MQMessage getMessage = new MQMessage();
			getQueue.get(getMessage,gmo);
			totalmaxCNT = getQueue.getCurrentDepth();
			cusidn = cusidn.trim();
			log.info(fromQueueName+"   fromQueueName totalmaxCNT>>>"+totalmaxCNT);
			log.info("keyword>>>["+cusidn+"]");
			
			for(int i=0;i<totalmaxCNT&&getQueue.getCurrentDepth()>0;i++)
			{
				getQueue.get(getMessage,gmo);
				byte[] byt = new byte[getMessage.getMessageLength()];
				getMessage.readFully(byt);
				String msgt = new String(byt,"BIG5");
				String source = new String(Arrays.copyOfRange(byt,25,33)).trim();
				String id = "";
				log.info(i+">>>["+source+"]");
				
				if("NNB".equals(source)) {
					if(TRANSTYPE.equals("H")) {
						id = new String(Arrays.copyOfRange(byt,90,100)).trim();
						log.info("id>>>["+id+"]");
						if(cusidn.equals(id))
						{
							//取出
							gmo.options = getoptNew("MQGMO_MSG_UNDER_CURSOR");//讀取後即刪
							getQueue.get(getMessage,gmo);
							rm.put("JCICSTRING", msgt);
							Row r = new Row(rm);
							result.addRow(r);
							log.info("===match id take out===");
							break;
						}else {
							log.info("===abandon===");
						}
					}else if(TRANSTYPE.equals("Q")) {
						id = new String(Arrays.copyOfRange(byt,101,111)).trim();
						log.info("id>>>["+id+"]");
						if(cusidn.equals(id))
						{
							//取出
							gmo.options = getoptNew("MQGMO_MSG_UNDER_CURSOR");//讀取後即刪
							getQueue.get(getMessage,gmo);
							rm.put("JCICSTRING", msgt);
							Row r = new Row(rm);
							result.addRow(r);	
							log.info("===match id take out===");
							break;
						}else {
							log.info("===abandon===");
						}
					}else {
						log.info("===abandon===");
					}
				}else {
					log.info("===abandon===");
				}
					
				gmo.options = getoptNew("MQGMO_BROWSE_NEXT");		//預設為只看不取走
			}

			getMessage.clearMessage();
			maxCNT = getQueue.getCurrentDepth();
			log.info(fromQueueName+"   fromQueueName DEPTH>>>"+maxCNT);
			
			result1 = new MVHImpl(result);
		}catch (MQException e) {
			if(e.reasonCode==2033) {
				log.info("===================="+fromQueueName+":佇列是空的");
				log.info("===================="+fromQueueName+":佇列是空的");
			}else {
				log.error("getQueue error >> {}",e);
			}
		}catch(Exception e){
			log.error(e.getMessage());
		}finally{
			if(getQueue!=null)
				getQueue.close();
			if(Qmgr != null)
			{
//				Qmgr.disconnect();
				Qmgr.close();
			}
		}
		return result1;
	}	
	
	
	

	

	
	/**
	 * 	讀取的參數設定
	 * @param strmqg
	 * @return
	 */
	private static int getoptNew(String strmqg)
	{
		int gopt = 0;
		
		//讀取第一筆
		if("MQGMO_BROWSE_FIRST".equals(strmqg))
			gopt = CMQC.MQGMO_WAIT | CMQC.MQGMO_BROWSE_FIRST;
		
		//讀取下一筆
		else if("MQGMO_BROWSE_NEXT".equals(strmqg))
			gopt = CMQC.MQGMO_WAIT | CMQC.MQGMO_BROWSE_NEXT;
		
		//讀取後即刪
		else if("MQGMO_MSG_UNDER_CURSOR".equals(strmqg))
			gopt = CMQC.MQGMO_WAIT | CMQC.MQGMO_FAIL_IF_QUIESCING | CMQC.MQGMO_MSG_UNDER_CURSOR;
		
		gopt = CMQC.MQGMO_NO_SYNCPOINT | gopt;
		
		return gopt;
	}	
	
	
	
	
	
	
	
	
	
	/**
	 * 	將msgObj置放到Queue裡
	 * @param queueName
	 * @param msgObj
	 * @return
	 * @throws MQException
	 * @throws Exception
	 */
	public static byte[] putMessage(MQQueue putQueue, byte[] msgObj) throws MQException, Exception
	{
		if(null == putQueue)
			return null;
		
		MQMessage msg = null;
		MQPutMessageOptions pmo = null;
		byte[] msgID = null;
		try
		{
			msg = new MQMessage();
			msg.write(msgObj);
			msg.messageId = CMQC.MQMI_NONE;
			msg.correlationId = CMQC.MQCI_NONE;

			pmo = new MQPutMessageOptions();
			pmo.options = CMQC.MQPMO_NEW_MSG_ID + CMQC.MQPMO_NEW_CORREL_ID;

			putQueue.put(msg, pmo);
			msgID = msg.messageId;

		}
		catch (MQException e)
		{
			throw new MQException(e.completionCode, e.reasonCode,e.exceptionSource);
		}
		catch (Exception e)
		{
			throw e;
		}
		return msgID;
	}	
	
	
	
	
	
	
} // End of file