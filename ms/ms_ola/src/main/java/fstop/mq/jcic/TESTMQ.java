package fstop.mq.jcic;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.ibm.mq.*;
import com.ibm.mq.constants.CMQC;
import com.netbank.util.ESAPIUtil;

import fstop.model.IMasterValuesHelper;
import fstop.model.MVHImpl;
import fstop.orm.dao.SysDailySeqDao;
import fstop.orm.po.SYSOP;
import fstop.services.impl.N203;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TESTMQ {
	
	@Value("${jcic.mq.uri}")
	private String mq_url;
	@Autowired
	SysDailySeqDao sysDailySeqDao;
	
//	public void setSysDailySeqDao(SysDailySeqDao sysDailySeqDao) {
//		this.sysDailySeqDao = sysDailySeqDao;
//	}
//	public void setSetting(Map setting) {
//		this.setting = setting;
//	}
	
	
	
	
/*
	public static void main(String[] args) throws MQException {
		
		
////////////////////////////////////////////////////		
		MQManager MQ = new MQManager();
		MQQueueManager Qmgr=null;
		try {
			Qmgr=MQ.getQueueManagerConnection("1212");
			if(Qmgr==null){
				throw new Exception("QUEUE CONNECTION FAIL");
			}
			
			//MQ.putQueue(Qmgr,"QJMET","050IF0120090724092308HZ21ETABS   C92308890   000004D00000108iamcv01   B5A       X123400338                                                  090070410671008110001000                                         ");
			//MQ.putQueue(Qmgr,"QJMET","050IF0120090724093922HZ21NNB     C93922687   000004D00000500winifred01AXA       56780210                                                                   ?Mode=Inquired_Record.Y                           ");
			//MQ.putQueue(Qmgr,"QJMET","050IF0120090724092036HZ22NNB     C92036906   000001D00000108iamcv01   AXA       B120398969                                                                   ?Mode=Inquired_Record.Y                           ");
//			MQ.putQueue(Qmgr,"QJMET","050IF0120090724093922HZ22ETABS   C93922687   000004D00000500winifred01AXA       X123400749                                                                 ?Mode=Inquired_Record.Y                           ");
			
			MQ.putQueue(Qmgr,"QJMET","050IF0120210706173349QZ21NNB     C93922687   000051D00001507USER001   AXA       B120398969110010130700101063000000                                         ");
			//MQ.putQueue(Qmgr,"QJMET","050IF0120210706173355HZ22NNB     C93922687   000052D00001507USER001   AXA       B120398969                                                  110010130700101?Mode=Inquired_Record.Y                           ");
			
			
			Thread.sleep(2400);
			
			String TT = "Q";
			String MSG="";
			int MQCNT = 0;
			
			IMasterValuesHelper MQJCIC1 = MQ.getQueueNew(Qmgr, "Q050IF01", "B120398969", TT);
			if(MQJCIC1!=null)
			{
				MQCNT = MQJCIC1.getValueOccurs("JCICSTRING");
				if(MQCNT>0){
					for(int i=0;i<MQCNT;i++){	
						MSG=MQJCIC1.getValueByFieldName("JCICSTRING",i+1);
						if(MSG.length()>0)
						{
							log.info("H MSG.substring(90,150)==>"+MSG.substring(90,150).trim());
							log.info("Q MSG.substring(85,95)==>"+MSG.substring(85,95));
							if(TT.equals("H"))
							{	
								if(MSG.indexOf("HTML")>-1)
								{ 
									if(MSG.substring(90,150).trim().equals("B120398969"))
									{	
										MSG=MSG.substring(165);
										log.info("MSG==>"+MSG);
										break;
									}
								}
							}
							if(TT.equals("Q"))
							{	
								if(MSG.indexOf("RZ21")>-1)
								{ 
									if(MSG.substring(85,95).equals("B120398969"))
									{	
										MSG=MSG.substring(176);
										log.info("MSG==>"+MSG);
										break;
									}
								}							
							}
						}else {
							MSG="無資料";
						}	
					}
				}else {
					Thread.sleep(2400);
					IMasterValuesHelper MQJCIC2 = MQ.getQueueNew(Qmgr, "NNB.OUT", "B120398969", TT);
					if(MQJCIC2!=null)
					{
						MQCNT = MQJCIC2.getValueOccurs("JCICSTRING");
						if(MQCNT>0)
						{	
							for(int i=0;i<MQCNT;i++){	
								MSG=MQJCIC2.getValueByFieldName("JCICSTRING",i+1);
								if(MSG.length()>0)
								{
									log.info("H MSG.substring(90,150)==>"+MSG.substring(90,150).trim());
									log.info("Q MSG.substring(85,95)==>"+MSG.substring(85,95));
									if(TT.equals("H"))
									{	
										if(MSG.indexOf("HTML")>-1)
										{ 
											if(MSG.substring(90,150).trim().equals("B120398969"))
											{	
												MSG=MSG.substring(165);
												log.info("MSG==>"+MSG);
												break;
											}
										}
									}
									if(TT.equals("Q"))
									{	
										if(MSG.indexOf("RZ21")>-1)
										{ 
											if(MSG.substring(85,95).equals("B120398969"))
											{	
												MSG=MSG.substring(176);
												log.info("MSG==>"+MSG);
												break;
											}
										}							
									}
								}
							}
						}else {
							MSG="無資料";
						}	
					}else {
						MSG="無資料";
					}	
				}
			}else {
				Thread.sleep(2400);
				IMasterValuesHelper MQJCIC3 = MQ.getQueueNew(Qmgr, "NNB.OUT", "B120398969", TT);
				if(MQJCIC3!=null)
				{
					MQCNT = MQJCIC3.getValueOccurs("JCICSTRING");
					if(MQCNT>0)
					{	
						for(int i=0;i<MQCNT;i++){	
							MSG=MQJCIC3.getValueByFieldName("JCICSTRING",i+1);
							if(MSG.length()>0)
							{
								log.info("H MSG.substring(90,150)==>"+MSG.substring(90,150).trim());
								log.info("Q MSG.substring(85,95)==>"+MSG.substring(85,95));
								if(TT.equals("H"))
								{	
									if(MSG.indexOf("HTML")>-1)
									{ 
										if(MSG.substring(90,150).trim().equals("B120398969"))
										{	
											MSG=MSG.substring(165);
											log.info("MSG==>"+MSG);
											break;
										}
									}
								}
								if(TT.equals("Q"))
								{	
									if(MSG.indexOf("RZ21")>-1)
									{ 
										if(MSG.substring(85,95).equals("B120398969"))
										{	
											MSG=MSG.substring(176);
											log.info("MSG==>"+MSG);
											break;
										}
									}							
								}
							}
						}
					}else {
						MSG="無資料";
					}	
				}else {
					MSG="無資料";
				}	
			}
			
		}catch (Exception e){

		}finally{
			if(Qmgr != null)
			{
				Qmgr.disconnect();
			}
		}
////////////////////////////////////////////////////		
		
		
		
		
	}

*/	
	
	
	
	public String process(Map params) throws Exception{
		String MSG="";
		try {
	    	String ip = StrUtils.trim(mq_url);
			String MQRequestStr = "";
			String TSEQ=""; 
			String Padding="";
			Integer SEQNO=sysDailySeqDao.dailySeq("N203");
			log.info("MQ SEQNO:"+SEQNO);
			if(SEQNO.toString().length()>=6)
				TSEQ=SEQNO.toString().substring(0,6);
			if(SEQNO.toString().length()<6)
			{
				for(int i=0;i<6-SEQNO.toString().length();i++)
				{
					Padding+="0";
				}
				TSEQ=Padding+SEQNO.toString();
			}
			String CUSIDN = params.get("CUSIDN").toString();
			String space = "";
			String TT = params.get("TID").toString().substring(0,1);
			if(TT.equals("H")) {
				for(int i=0;i<60-CUSIDN.length();i++)
				{
					space+=" ";
				}
			}
			CUSIDN+=space;
			MQRequestStr+=params.get("BANKID");
			MQRequestStr+=params.get("BRHID");
			MQRequestStr+=params.get("DATE");
			MQRequestStr+=params.get("TIME");
			MQRequestStr+=params.get("TID");
			MQRequestStr+=params.get("QID");
			MQRequestStr+=params.get("MEMREV");
			MQRequestStr+=TSEQ;
			MQRequestStr+=params.get("ERRCODE");
			MQRequestStr+=params.get("PAYID");
			MQRequestStr+=params.get("USERID");
			MQRequestStr+=params.get("REASONCODE");
			MQRequestStr+=params.get("FILLER01");
			MQRequestStr+=params.get("FILLER02");
			MQRequestStr+=CUSIDN;
			MQRequestStr+=params.get("FILLER03");
			MQRequestStr+=params.get("QUERYPRINT");
			

				
////////////////////////////////////////////////////				
			MQManager MQ = new MQManager();
			MQQueueManager Qmgr=null;
			try {
				Qmgr=MQ.getQueueManagerConnection(ip);
				if(Qmgr==null){
					throw new Exception("QUEUE CONNECTION FAIL");
				}
				
//				MQ.putQueue(Qmgr,"QJMET","050IF0120090724092308HZ21ETABS   C92308890   000004D00000108iamcv01   B5A       X123400338                                                  090070410671008110001000                                         ");
//				MQ.putQueue(Qmgr,"QJMET","050IF0120090724093922HZ21NNB     C93922687   000004D00000500winifred01AXA       56780210                                                                   ?Mode=Inquired_Record.Y                           ");
//				MQ.putQueue(Qmgr,"QJMET","050IF0120090724092036HZ22NNB     C92036906   000001D00000108iamcv01   AXA       56780175                                                                   ?Mode=Inquired_Record.Y                           ");
//				MQ.putQueue(Qmgr,"QJMET","050IF0120090724093922HZ22ETABS   C93922687   000004D00000500winifred01AXA       X123400749                                                                 ?Mode=Inquired_Record.Y                           ");
				log.info("TESTMQ.java MQRequestStr:"+MQRequestStr);
				MQ.putQueue(Qmgr, "QJMET", MQRequestStr);
				
				Thread.sleep(2400);
				
				int MQCNT = 0;
				
				IMasterValuesHelper MQJCIC1 = MQ.getQueueNew(Qmgr, "Q050IF01", CUSIDN, TT);
				if(MQJCIC1!=null)
				{
					MQCNT = MQJCIC1.getValueOccurs("JCICSTRING");
					if(MQCNT>0){
						for(int i=0;i<MQCNT;i++){	
							MSG=MQJCIC1.getValueByFieldName("JCICSTRING",i+1);
							if(MSG.length()>0)
							{
								log.info("H MSG.substring(90,150)==>"+MSG.substring(90,150).trim());
								log.info("Q MSG.substring(85,95)==>"+MSG.substring(85,95));
								if(TT.equals("H"))
								{	
									if(MSG.indexOf("HTML")>-1)
									{ 
										if(MSG.substring(90,150).trim().equals(CUSIDN.trim()))
										{	
											MSG=MSG.substring(165);
											log.info(ESAPIUtil.vaildLog("Find Q050IF01 MQ MSG HTML:"+MSG));
											break;
										}
									}
								}
								if(TT.equals("Q"))
								{	
									if(MSG.indexOf("RZ21")>-1)
									{ 
										if(MSG.substring(85,95).equals(CUSIDN.trim()))
										{	
											MSG=MSG.substring(176);
											log.info(ESAPIUtil.vaildLog("Find Q050IF01 MQ MSG QZ21:"+MSG));
											break;
										}
									}							
								}
							}else {
								MSG="無資料";
							}	
						}
					}else {
						Thread.sleep(2400);
						IMasterValuesHelper MQJCIC2 = MQ.getQueueNew(Qmgr, "NNB.OUT", CUSIDN, TT);
						if(MQJCIC2!=null)
						{
							MQCNT = MQJCIC2.getValueOccurs("JCICSTRING");
							if(MQCNT>0)
							{	
								for(int i=0;i<MQCNT;i++){	
									MSG=MQJCIC2.getValueByFieldName("JCICSTRING",i+1);
									if(MSG.length()>0)
									{
										log.info("H MSG.substring(90,150)==>"+MSG.substring(90,150).trim());
										log.info("Q MSG.substring(85,95)==>"+MSG.substring(85,95));
										if(TT.equals("H"))
										{	
											if(MSG.indexOf("HTML")>-1)
											{ 
												if(MSG.substring(90,150).trim().equals(CUSIDN.trim()))
												{	
													MSG=MSG.substring(165);
													log.info(ESAPIUtil.vaildLog("Find  NNB.OUT MQ MSG HTML:"+MSG));
													break;
												}
											}
										}
										if(TT.equals("Q"))
										{	
											if(MSG.indexOf("RZ21")>-1)
											{ 
												if(MSG.substring(85,95).equals(CUSIDN.trim()))
												{	
													MSG=MSG.substring(176);
													log.info(ESAPIUtil.vaildLog("Find  NNB.OUT MQ MSG QZ21:"+MSG));
													break;
												}
											}							
										}
									}
								}
							}else {
								MSG="無資料";
							}	
						}else {
							MSG="無資料";
						}	
					}
				}else {
					Thread.sleep(2400);
					IMasterValuesHelper MQJCIC3 = MQ.getQueueNew(Qmgr, "NNB.OUT", CUSIDN, TT);
					if(MQJCIC3!=null)
					{
						MQCNT = MQJCIC3.getValueOccurs("JCICSTRING");
						if(MQCNT>0)
						{	
							for(int i=0;i<MQCNT;i++){	
								MSG=MQJCIC3.getValueByFieldName("JCICSTRING",i+1);
								if(MSG.length()>0)
								{
									log.info("H MSG.substring(90,150)==>"+MSG.substring(90,150).trim());
									log.info("Q MSG.substring(85,95)==>"+MSG.substring(85,95));
									if(TT.equals("H"))
									{	
										if(MSG.indexOf("HTML")>-1)
										{ 
											if(MSG.substring(90,150).trim().equals(CUSIDN.trim()))
											{	
												MSG=MSG.substring(165);
												log.info(ESAPIUtil.vaildLog("Find  NNB.OUT MQ MSG HTML:"+MSG));
												break;
											}
										}
									}
									if(TT.equals("Q"))
									{	
										if(MSG.indexOf("RZ21")>-1)
										{ 
											if(MSG.substring(85,95).equals(CUSIDN.trim()))
											{	
												MSG=MSG.substring(176);
												log.info(ESAPIUtil.vaildLog("Find  NNB.OUT MQ MSG QZ21:"+MSG));
												break;
											}
										}							
									}
								}
							}
						}else {
							MSG="無資料";
						}	
					}else {
						MSG="無資料";
					}	
				}
				
			}catch (Exception e){

			}finally{
				if(Qmgr != null)
				{
					Qmgr.disconnect();
				}
			}				
////////////////////////////////////////////////////					
		
			
			
			
///////////////////old/////////////////////////////////			
//			MQManager MQ = new MQManager();
//			MQQueueManager Qmgr=null;
//			try {
//				Qmgr=MQ.getQueueManagerConnection(ip);
//				if(Qmgr==null){
//					throw new Exception("QUEUE CONNECTION FAIL");
//				}else{
//	//				Qmgr.disconnect();
//	
//					//put
//					//MQ.putQueue(Qmgr,"Q050IF01","050IF01L001QJCICTBBrrrrrrrrrrrr000001000000500XXXXXXXXxxxxxxxxxxxx050ET00112345678");
//					//MQ.putQueue(Qmgr,"QJMET","050IF0120101207160720L001QJCICTBBrrrrrrrrrrrr000001000000500XXXXXXXXxxxxxxxxxxxx050ET00112345678");
//					//MQ.putQueue(Qmgr,"QJMET","050IF0120090724092308HZ21ETABS   C92308890   000004D00000108iamcv01   B5A       X123400338                                                  090070410671008110001000                                         ");
//					//MQ.putQueue(Qmgr,"QJMET","050IF0120090724093922HZ22ETABS   C93922687   000004D00000500winifred01AXA       X123400749                                                                 ?Mode=Inquired_Record.Y                           ");
//					//MQ.putQueue(Qmgr,"ETABS.IN",MQRequestStr);
//					log.info("TESTMQ.java MQRequestStr:"+MQRequestStr);
//					MQ.putQueue(Qmgr, "QJMET", MQRequestStr);
//					//MQ.putQueue(Qmgr,"QJMET","050IF0120090724093922HZ21NNB     C93922687   000004D00000500winifred01AXA       56780210                                                                   ?Mode=Inquired_Record.Y                           ");
//					//MQ.putQueue(Qmgr,"QJMET","050IF0120090724092036HZ22NNB     C92036906   000001D00000108iamcv01   AXA       56780175                                                                   ?Mode=Inquired_Record.Y                           ");
//					//MQ.putQueue(Qmgr,"ETABS.IN","050IF0120090724092308Z22ETABS   C92308890   000004D00000108iamcv01   B5A       56780921  ");
//					//MQ.putQueue(Qmgr,"ETABS.IN","050IF0120090724092308QZ22ETABS   C92308890   000004D00000108iamcv01   B5A       X123400338");				
//					
//					//MQ.putQueue(Qmgr,"TBBJCIC.IN","050IF0120090724092308HZ21        C92308890   000004D00000108iamcv01   B5A       X123400338                                                  090070410671008110001000                                         ");
//					//MQ.putQueue(Qmgr,"TBBJCIC.IN","050IF0120090724093922HZ22        C93922687   000004D00000500winifred01AXA       56780210                                                                   ?Mode=Inquired_Record.Y                           ");
//	
//					
//					//if(!MQ.putQueue(Qmgr,"ETABS.IN","新增MESSAGE~~"+getNowDate(1)+"      "+getNowDate(2)))
//					//	System.err.println("putQueue ERROR!!!");
//					
//					//get
//					//log.info("GET QUEUE="+MQ.getQueue(Qmgr,"ETABS.OUT"));
//					Thread.sleep(2400);
//					//int jcic_cnt=MQ.getQueue(Qmgr,"Q050IF01");
//					//log.info("GET QUEUE="+jcic_cnt);
//					
//					
//					IMasterValuesHelper MQJCIC = MQ.getQueue(Qmgr, "Q050IF01",TT);
//					//IMasterValuesHelper MQJCIC = MQ.getQueue(Qmgr, "NNB.OUT");
//					if(MQJCIC!=null)
//					{
//						int MQCNT = MQJCIC.getValueOccurs("JCICSTRING");
//						log.info("GET Q050IF01 QUEUE="+MQCNT);
//						//log.info("GET NNB.OUT QUEUE="+MQCNT);
//						if(MQCNT>0)
//						{	
//							for(int i=0;i<MQCNT;i++)
//							{	
//								MSG=MQJCIC.getValueByFieldName("JCICSTRING",i+1);
//								log.info(ESAPIUtil.vaildLog("Q050IF01 Hashmap MQ MSG:"+MSG));
//								//log.info("NNB.OUT Hashmap MQ MSG:"+MSG);
//								if(MSG.length()>0)
//								{
//								//String type= MSG.substring(MSG.indexOf("HZ21")-1,MSG.indexOf("HZ21")+3);
//								//log.info("GET TYPE="+type);
//									if(TT.equals("H"))
//									{	
//										if(MSG.indexOf("NNB")>-1 && MSG.indexOf("HTML")>-1)
//										{ 
//											//log.info(i+" Q050IF01 MQ MSG:"+MSG);
//											//log.info(i+" NNB.OUT MQ MSG:"+MSG);
//											if(MSG.substring(80,140).trim().equals(params.get("CUSIDN").toString().trim()))
//											{	
//												MSG=MSG.substring(165);
//												log.info(ESAPIUtil.vaildLog("Find Q050IF01 MQ MSG HTML:"+MSG));
//												//log.info("Find NNB.OUT MQ MSG HTML:"+MSG);
//												break;
//											}
//										}
//									}
//									if(TT.equals("Q"))
//									{	
//										if(MSG.indexOf("NNB")>-1 && MSG.indexOf("RZ21")>-1)
//										{ 
//											//log.info(i+" Q050IF01 MQ MSG:"+MSG);
//											//log.info(i+" NNB.OUT MQ MSG:"+MSG);
//											if(MSG.substring(85,95).equals(params.get("CUSIDN").toString().trim()))
//											{	
//												MSG=MSG.substring(176);
//												log.info(ESAPIUtil.vaildLog("Find Q050IF01 MQ MSG QZ21:"+MSG));
//												//log.info("Find NNB.OUT MQ MSG HTML:"+MSG);
//												break;
//											}
//										}							
//									}								
//								}
//							}
//						}
//						else
//						{
//							MQJCIC = MQ.getQueue(Qmgr, "NNB.OUT",TT);
//							//MQJCIC = MQ.getQueue(Qmgr, "Q050IF01");
//							if(MQJCIC!=null)
//							{
//								MQCNT = MQJCIC.getValueOccurs("JCICSTRING");
//								log.info("GET NNB.OUT QUEUE="+MQCNT);
//								//log.info("GET Q050IF01 QUEUE="+MQCNT);
//								if(MQCNT>0)
//								{	
//									for(int i=0;i<MQCNT;i++)
//									{	
//										MSG=MQJCIC.getValueByFieldName("JCICSTRING",i+1);
//										log.info(ESAPIUtil.vaildLog(" NNB.OUT Hashmap MQ MSG:"+MSG));
//										//log.info(" Q050IF01 Hashmap MQ MSG:"+MSG);
//										if(MSG.length()>0)
//										{
//										//String type= MSG.substring(MSG.indexOf("HZ21")-1,MSG.indexOf("HZ21")+3);
//										//log.info("GET TYPE="+type);
//											if(TT.equals("H"))
//											{	
//												if(MSG.indexOf("NNB")>-1 && MSG.indexOf("HTML")>-1)
//												{ 
//													//log.info(i+"  NNB.OUT MQ MSG:"+MSG);
//													//log.info(i+"  Q050IF01 MQ MSG:"+MSG);
//													if(MSG.substring(80,140).trim().equals(params.get("CUSIDN").toString().trim()))
//													{	
//														MSG=MSG.substring(165);
//														log.info(ESAPIUtil.vaildLog("Find  NNB.OUT MQ MSG HTML:"+MSG));
//														//log.info("Find  Q050IF01 MQ MSG HTML:"+MSG);
//														break;
//													}
//												}
//											}
//											if(TT.equals("Q"))
//											{	
//												if(MSG.indexOf("NNB")>-1 && MSG.indexOf("RZ21")>-1)
//												{ 
//													//log.info(i+"  NNB.OUT MQ MSG:"+MSG);
//													//log.info(i+"  Q050IF01 MQ MSG:"+MSG);
//													if(MSG.substring(85,95).equals(params.get("CUSIDN").toString().trim()))
//													{	
//														MSG=MSG.substring(176);
//														log.info(ESAPIUtil.vaildLog("Find  NNB.OUT MQ MSG QZ21:"+MSG));
//														//log.info("Find  Q050IF01 MQ MSG HTML:"+MSG);
//														break;
//													}
//												}
//											}
//										}											
//									}
//								}
//							}							
//							else
//								MSG="無資料";
//						}	
//					}
//					else
//					{
//						MQJCIC = MQ.getQueue(Qmgr, "NNB.OUT",TT);
//						//MQJCIC = MQ.getQueue(Qmgr, "Q050IF01");
//						if(MQJCIC!=null)
//						{
//							int MQCNT = MQJCIC.getValueOccurs("JCICSTRING");
//							log.info("GET NNB.OUT QUEUE="+MQCNT);
//							//log.info("GET Q050IF01 QUEUE="+MQCNT);
//							if(MQCNT>0)
//							{	
//								for(int i=0;i<MQCNT;i++)
//								{	
//									MSG=MQJCIC.getValueByFieldName("JCICSTRING",i+1);
//									log.info(ESAPIUtil.vaildLog(" NNB.OUT Hashmap MQ MSG:"+MSG));
//									//log.info(" Q050IF01 Hashmap MQ MSG:"+MSG);
//									if(MSG.length()>0)
//									{
//									//String type= MSG.substring(MSG.indexOf("HZ21")-1,MSG.indexOf("HZ21")+3);
//									//log.info("GET TYPE="+type);
//										if(TT.equals("H"))
//										{	
//											if(MSG.indexOf("NNB")>-1 && MSG.indexOf("HTML")>-1)
//											{ 
//												//log.info(i+"  NNB.OUT MQ MSG:"+MSG);
//												//log.info(i+"  Q050IF01 MQ MSG:"+MSG);
//												if(MSG.substring(80,140).trim().equals(params.get("CUSIDN").toString().trim()))
//												{	
//													MSG=MSG.substring(165);
//													log.info(ESAPIUtil.vaildLog("Find  NNB.OUT MQ MSG HTML:"+MSG));
//													//log.info("Find  Q050IF01 MQ MSG HTML:"+MSG);
//													break;
//												}
//											}
//										}
//										if(TT.equals("Q"))
//										{	
//											if(MSG.indexOf("NNB")>-1 && MSG.indexOf("RZ21")>-1)
//											{ 
//												//log.info(i+"  NNB.OUT MQ MSG:"+MSG);
//												//log.info(i+"  Q050IF01 MQ MSG:"+MSG);
//												if(MSG.substring(85,95).equals(params.get("CUSIDN").toString().trim()))
//												{	
//													MSG=MSG.substring(176);
//													log.info(ESAPIUtil.vaildLog("Find  NNB.OUT MQ MSG QZ21:"+MSG));
//													//log.info("Find  Q050IF01 MQ MSG HTML:"+MSG);
//													break;
//												}
//											}
//										}
//									}
//								}
//							}
//							else
//							{
//								MQJCIC = MQ.getQueue(Qmgr, "Q050IF01",TT);
//								//MQJCIC = MQ.getQueue(Qmgr, "NNB.OUT");
//								if(MQJCIC!=null)
//								{
//									MQCNT = MQJCIC.getValueOccurs("JCICSTRING");
//									log.info("GET Q050IF01 QUEUE="+MQCNT);
//									//log.info("GET NNB.OUT QUEUE="+MQCNT);
//									if(MQCNT>0)
//									{	
//										for(int i=0;i<MQCNT;i++)
//										{	
//											MSG=MQJCIC.getValueByFieldName("JCICSTRING",i+1);
//											log.info(ESAPIUtil.vaildLog(" Q050IF01 Hashmap MQ MSG:"+MSG));
//											//log.info(" NNB.OUT Hashmap MQ MSG:"+MSG);
//											if(MSG.length()>0)
//											{
//											//String type= MSG.substring(MSG.indexOf("HZ21")-1,MSG.indexOf("HZ21")+3);
//											//log.info("GET TYPE="+type);
//												if(TT.equals("H"))
//												{	
//													if(MSG.indexOf("NNB")>-1 && MSG.indexOf("HTML")>-1)
//													{ 
//														//log.info(i+"  Q050IF01 MQ MSG:"+MSG);
//														//log.info(i+"  NNB.OUT MQ MSG:"+MSG);
//														if(MSG.substring(80,140).trim().equals(params.get("CUSIDN").toString().trim()))
//														{	
//															MSG=MSG.substring(165);
//															log.info(ESAPIUtil.vaildLog("Find  Q050IF01 MQ MSG HTML:"+MSG));
//															//log.info("Find  NNB.OUT MQ MSG HTML:"+MSG);
//															break;
//														}
//													}
//												}
//												if(TT.equals("Q"))
//												{	
//													if(MSG.indexOf("NNB")>-1 && MSG.indexOf("RZ21")>-1)
//													{ 
//														//log.info(i+"  Q050IF01 MQ MSG:"+MSG);
//														//log.info(i+"  NNB.OUT MQ MSG:"+MSG);
//														if(MSG.substring(85,95).equals(params.get("CUSIDN").toString().trim()))
//														{	
//															MSG=MSG.substring(176);
//															log.info(ESAPIUtil.vaildLog("Find  Q050IF01 MQ MSG QZ21:"+MSG));
//															//log.info("Find  NNB.OUT MQ MSG HTML:"+MSG);
//															break;
//														}
//													}
//												}
//											}
//										}
//									}	
//								}	
//								
//							}
//						}
//						else
//							MSG="無資料";
//					}
//					
//				}
//			}catch (MQException e) {
//				log.info("N203.java Z21 QUERY ERROR:"+e.getMessage());
//			}finally{
//				if(Qmgr!=null){
//					Qmgr.disconnect();
//					log.info("CLOSE QUEUE");		
//				}
//			}
//////////////////////////////////////////////////////			
			
			
			
		}catch (Exception e) {
			log.info(e.getMessage());
		}
		return MSG;
	}


	
 	//取得日期時間　1.西元年 2.時間 3.民國年 4.西元年月
 	public String getNowDate(int type) {
		Calendar cd = Calendar.getInstance();
		String day =Integer.toString(cd.get(Calendar.DAY_OF_MONTH));
		String hours =Integer.toString(cd.get(Calendar.HOUR_OF_DAY));
		String sDT ="";
		SimpleDateFormat formatter =null;
		if(type==1){
			formatter = new SimpleDateFormat ("yyyy-MM-dd");
			cd.add(Calendar.DAY_OF_YEAR,0);
			sDT =  formatter.format(cd.getTime());
		}else if(type==2){
			formatter = new SimpleDateFormat ("HH:mm:ss");
			sDT =  formatter.format(cd.getTime());
		}else if(type==3){
			formatter = new SimpleDateFormat ("yyyy/MM/dd");
			cd.add(Calendar.DAY_OF_YEAR,0);
			sDT =  formatter.format(cd.getTime());
			sDT =((Integer.parseInt(sDT.substring(0,4))-911)+"").substring(1)+
			sDT.substring(4);
		}if(type==4){
			formatter = new SimpleDateFormat ("yyyyMM");
			cd.add(Calendar.DAY_OF_YEAR,0);
			sDT =  formatter.format(cd.getTime());
		}
		return sDT;
	} //getNowDate	
	
} // End of file