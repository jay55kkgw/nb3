package fstop.services.mgn;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fstop.core.BeanUtils;
import fstop.exception.ToRuntimeException;
import fstop.model.DBResult;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.orm.dao.SysParamDao;
import fstop.orm.dao.SysParamDataDao;
import fstop.orm.po.SYSPARAM;
import fstop.orm.po.SYSPARAMDATA;
import fstop.services.Auditable;
import fstop.services.CommonService;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class B106 extends CommonService implements Auditable {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	private SysParamDao sysParamDao;
	@Autowired
	private SysParamDataDao sysParamDataDao;
	
//	@Required
//	public void setSysParamDao(SysParamDao sysParamDao) {
//		this.sysParamDao = sysParamDao;
//	}
//	
//	@Required
//	public void setSysParamDataDao(SysParamDataDao sysParamDataDao) {
//		this.sysParamDataDao = sysParamDataDao;
//	}
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params) { 
		Map<String, String> params = _params;		
		boolean isExists = false;
		SYSPARAMDATA po = null; 
		try {
			po = sysParamDataDao.findById("NBSYS");
			isExists = true;
		}
		catch(ObjectNotFoundException e) {
			isExists = false;
		}
		
		if(!isExists) {
			po = new SYSPARAMDATA();
			po.setADPK("NBSYS");
		}
		
		String[] propertyName = {
				"ADSESSIONTO",
				"ADOPMAIL",
				"ADAPMAIL",
				"ADSECMAIL",
				"ADSPMAIL",
				"ADFDSVRIP",
				"ADMAILSVRIP",
				"ADPARKSVRIP",
				"ADARSVRIP",
				"ADFDRETRYTIMES",
				"ADPKRETRYTIMES",
				"ADRESENDTIMES",				
				"ADGDMAIL",		
				"ADGDTXNMAIL",
				"KYCDATE",
				"SCN070",
				"SCF002"
		};
		 
		Date d = new Date();
		params.put("LASTDATE", DateTimeUtils.format("yyyyMMdd", d));
		params.put("LASTTIME", DateTimeUtils.format("HHmmss", d));

		MVHImpl helper = new MVHImpl();
		if("UPDATE".equalsIgnoreCase(params.get("EXECUTEFUNCTION"))) {									
			for(String p : propertyName) {																									
				try {					
					BeanUtils.setProperty(po, p, StrUtils.trim(params.get(p)));					
				} catch (IllegalAccessException e) {
					throw new ToRuntimeException("BeanUtils.setProperty (" + p + ") Error.", e);
				} catch (InvocationTargetException e) {
					throw new ToRuntimeException("BeanUtils.setProperty (" + p + ") Error.", e);
				}
			}
			
			po.setADBHMAILACNORULE(StrUtils.trim(params.get("ADLEADBYTE")) + "@"+ StrUtils.trim(params.get("ADMAILDOMAIN")));
			
			po.setLASTDATE(params.get("LASTDATE"));
			po.setLASTTIME(params.get("LASTTIME"));

			helper.getFlatValues().putAll(_params);
			
			sysParamDataDao.save(po);
			
			// 儲存 SysParam 資料
			String ADBANK3MAIL = params.get("ADBANK3MAIL");
			sysParamDao.update("ADBANK3MAIL", ADBANK3MAIL);

			String TXNTWAMTMAIL = params.get("TXNTWAMTMAIL");
			sysParamDao.update("TXNTWAMTMAIL", TXNTWAMTMAIL);			
			
			// 讀取 SysParam 資料
			List<SYSPARAM> qresult = sysParamDao.getAll();
			for(SYSPARAM sysParam : qresult) {
				helper.getFlatValues().put(sysParam.getADPARAMNAME(), sysParam.getADPARAMVALUE());
			}
			
		}
		else {
			List<String> ary = new ArrayList(Arrays.asList(propertyName));
			ary.add("ADBHMAILACNORULE");
			for(String p : ary) {
				String v = "";
				try {
					v = (String)BeanUtils.getProperty(po, p);					
				} catch (IllegalAccessException e) {
					throw new ToRuntimeException("BeanUtils.getProperty (" + p + ") Error.", e);
				} catch (InvocationTargetException e) {
					throw new ToRuntimeException("BeanUtils.getProperty (" + p + ") Error.", e);
				} catch (NoSuchMethodException e) {
					throw new ToRuntimeException("BeanUtils.getProperty (" + p + ") Error.", e);
				}
				
				
				
				helper.getFlatValues().put(p, v);
			}
			
			String ADBHMAILACNORULE = helper.getFlatValues().get("ADBHMAILACNORULE");
			log.debug("ADBHMAILACNORULE == "+ADBHMAILACNORULE);
			try {
				String[] s = ADBHMAILACNORULE.split("@");
				helper.getFlatValues().put("ADLEADBYTE", s[0]);
				helper.getFlatValues().put("ADMAILDOMAIN", s[1]);
			}
			catch(Exception e){}
			
			
			// 讀取 SysParam 資料
			List<SYSPARAM> qresult = sysParamDao.getAll();
			for(SYSPARAM sysParam : qresult) {
				helper.getFlatValues().put(sysParam.getADPARAMNAME(), sysParam.getADPARAMVALUE());
			}
		}
		
		 
		helper.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		
		
		return helper;
	}



}
