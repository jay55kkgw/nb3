package fstop.services.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.netbank.util.ESAPIUtil;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.AdmEmpInfoDao;
import fstop.services.CommonService;
import fstop.services.batch.CheckServerHealth;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import fstop.va.VaCheck;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class N203_VA extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	AdmEmpInfoDao admEmpInfoDao;

	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 

		MVHImpl telcommResult = new MVHImpl();
		//TelcommResult telcommResult = new TelcommResult(new Vector());
		String MSGCODE = "";
		log.warn(ESAPIUtil.vaildLog("N203_VA.java FGTXWAY:"+params.get("FGTXWAY").toString()));
		//晶片金融卡
		/*
		if("2".equals(params.get("FGTXWAY").toString()))
		{
			Date d = new Date();
			String trin_transeq = "1234";
			String trin_issuer = "05000000";
			String trin_acnno = params.get("ACNNO").toString();
			String trin_icdttm = DateTimeUtils.format("yyyyMMddHHmmss", d);
			logger.warn("N203_VA.java ICDTTM ----------> " + trin_icdttm);
			String trin_iseqno = params.get("iSeqNo").toString();
			String trin_icmemo = "";
			String trin_tac_length = "000A";		
			String trin_tac = params.get("TAC").toString();
			logger.warn("N203_VA.java TAC ----------> " + trin_tac_length + trin_tac + "(" + (trin_tac_length.length() + trin_tac.length()) + ")");
			String trin_trmid = params.get("TRMID").toString();
		
			params.put("TRANSEQ", trin_transeq);
			params.put("ISSUER", trin_issuer);
			params.put("ACNNO", trin_acnno);
			params.put("ICDTTM", trin_icdttm);
			params.put("ICSEQ", trin_iseqno);
			params.put("ICMEMO", trin_icmemo);
			params.put("TAC_Length", trin_tac_length);
			params.put("TAC", trin_tac);
			params.put("TRMID", trin_trmid);
		}
		*/		
		if(params.containsKey("pkcs7Sign") && "4".equals(params.get("FGTXWAY"))) {
			//壓簽章字串比對
			String jsondc = StrUtils.trim(params.get("jsondc").toString());
			String __mac = StrUtils.trim(params.get("pkcs7Sign").toString());
			params.put("__SIGN", __mac);			
			log.warn(ESAPIUtil.vaildLog("N203_VA.java JSONDC:"+jsondc));
			log.warn(ESAPIUtil.vaildLog("N203_VA.java pkcs7Sign:"+__mac));
			try {
				String CertInfo = VaCheck.doVAVerify(__mac, jsondc, "VARule1");
				int VerfyICSrtn = -1;
				if(CertInfo.length()==0) {
					MSGCODE="Z089";
					CheckServerHealth checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
					checkserverhealth.sendZ300Notice("Z089");
					telcommResult.getFlatValues().put("MSGCODE", MSGCODE);
					log.warn("N203_VA.java doVAVerify fail: cert information error");
				}
				else
				{
					try {
						VerfyICSrtn = VaCheck.doVerfyICS(CertInfo, params.get("CUSIDN").toString(), true);
						if(VerfyICSrtn!=0)
						{
							MSGCODE="Z089";
							CheckServerHealth checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
							checkserverhealth.sendZ300Notice("Z089");
							telcommResult.getFlatValues().put("MSGCODE", MSGCODE);
							log.warn("N203_VA.java doVerfyICS fail");
						}
					}
					catch(Exception e){
						log.warn("N203_VA.java doVerfyICS fail Exception:"+e.getMessage());
					}
				}
			}
			catch(Exception e) {
				log.warn("N203_VA.java doVAVerify fail Exception:"+e.getMessage());
			}
			
		}				
		telcommResult.getFlatValues().put("MSGCOD", MSGCODE);
		telcommResult.getFlatValues().put("CUSIDN",params.get("CUSIDN").toString());
		telcommResult.getFlatValues().put("UID",params.get("UID").toString());
		telcommResult.getFlatValues().put("FGTXWAY", params.get("FGTXWAY").toString());
		telcommResult.getFlatValues().put("EMPINFO", admEmpInfoDao.isEmpCUSID(params.get("CUSIDN").toString()) ? "Y" : "N");
		
		return telcommResult;
	}


}
