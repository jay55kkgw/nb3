package fstop.services.impl;

import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;
import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.services.CommonService;
import com.netbank.rest.model.CommonPools;
import com.netbank.util.ESAPIUtil;

import fstop.orm.po.TXNACNAPPLY;
import fstop.telcomm.TelCommExec;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * @author Owner
 * 
 */
@Slf4j
public class NB31 extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("nb32Telcomm")
	private TelCommExec nb32Telcomm;
	@Autowired
	private CommonPools commonPools;

//	@Required
//	public void setNb32Telcomm(TelCommExec telcomm) {
//		nb32Telcomm = telcomm;
//	}

	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) {

		// 查詢

		log.warn(ESAPIUtil.vaildLog("NB31.java CUSIDN:" + params.get("CUSIDN")));
		MVHImpl telcommResult = new MVHImpl();
		MVHImpl telcommResult1 = new MVHImpl();
		try {
			params.put("TYPE", "01");
			telcommResult1 = nb32Telcomm.query(params);
			//log.trace("telcommResult1"+telcommResult1);
		} catch (TopMessageException e) {
			log.warn("NB31.java query acn fail Exception:" + e.getMsgcode());
			throw TopMessageException.create(e.getMsgcode());
		}
		
		if (telcommResult1.getFlatValues().get("ACN").toString().trim().equals(
				"")) {
			try {
				TXNACNAPPLY txnAcnApply = commonPools.applyUtils
						.getApplyDataByCUSIDN(params.get("CUSIDN").toString());
				//log.trace("txnAcnApply"+txnAcnApply.getACN());
				//20190704原先判斷是否為Null，但Dao會建空物件，因而無法導向錯誤
				if (txnAcnApply.getACN().equals("")) {
					throw TopMessageException.create("Z617");
				}
				telcommResult.getFlatValues().put("MSGCOD",telcommResult1.getFlatValues().get("MSGCOD"));
				telcommResult.getFlatValues().put("ACNTYPE",txnAcnApply.getACNTYPE());
				telcommResult.getFlatValues().put("ACN", txnAcnApply.getACN());
				telcommResult.getFlatValues().put("CUSIDN",params.get("CUSIDN").toString());
				telcommResult.getFlatValues().put("UID",params.get("CUSIDN").toString());
				telcommResult.getFlatValues().put("POSTCOD",txnAcnApply.getPOSTCOD2());        //取得通訊地址郵遞區號
				telcommResult.getFlatValues().put("MILADR",txnAcnApply.getCTTADR());           //取得通訊地址
				
			} catch (TopMessageException ex) {
				log.warn("NB31.java get acntype Exception:"+ ex.getMessage());
				throw TopMessageException.create("Z617");
				
			} catch (Exception e) {
				log.warn("NB31.java get acntype Exception:"+ e.getMessage());
				throw TopMessageException.create("Z617");
			}
		} else {
			throw TopMessageException.create("Z623");
		}
		return telcommResult;
	}

}
