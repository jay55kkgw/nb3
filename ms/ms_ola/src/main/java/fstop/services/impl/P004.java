package fstop.services.impl;

import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class P004 extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("p004Telcomm")
	private TelCommExec p004Telcomm;
	
//	@Required
//	public void setP004Telcomm(TelCommExec telCommExec) {
//		this.p004Telcomm = telCommExec;
//	}
	
	@Override
	public MVH doAction(Map params) {
		
		String periodStr = "";
		String stadate = (String) params.get("STARTSERACHDATE");
		String enddate = (String) params.get("ENDSERACHDATE");
		stadate = StrUtils.trim(stadate).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
		enddate = StrUtils.trim(enddate).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
		
		if (stadate != null && stadate.length() == 8) {
			Date sd = DateTimeUtils.parse("yyyyMMdd", stadate);
			periodStr = DateTimeUtils.format("yyyy/MM/dd", sd);
		}
		if (enddate != null && enddate.length() == 8) {
			Date sd = DateTimeUtils.parse("yyyyMMdd", enddate);
			periodStr += "~" + DateTimeUtils.format("yyyy/MM/dd", sd);
		} else { // 若未指定結束時間, 則使用今天日期
			enddate = DateTimeUtils.getCDateShort(new Date());
			periodStr += "~" + DateTimeUtils.format("yyyy/MM/dd", new Date());
		}
		/*************************新增多筆查詢功能****************************************/
		String ZoneCode = (String)params.get("ZoneCode");
		String[] arrayParams = {"TPC","TPY","KHC","TYC","TCC","TNC","HCC"};//TPC-台北市;TPY-台北縣;KHC-高雄市;TYC-桃園市;TCC-台中市;TNC-台南市;HCC-新竹市
		MVHImpl helper = null;
		MVHImpl tmpResult = null;
		boolean isFirst = true;
		if(ZoneCode.equals("   "))//表示查全部
		{
			int totalcount = 0;        //本次查詢條件在資料庫查詢所得的總筆數
			int waitpayrecord = 0;     //本次查詢條件下的待扣款筆數
			int paysuccessrecord = 0;  //本次查詢條件下的扣款成功筆數
			int payfailrecord = 0;     //本次查詢條件下的扣款失敗筆數
			
			for(String p : arrayParams) {
				log.debug("查全部一次一次送電文 do : " + p); //{}
				//TODO: Map無法直接clone
//				Hashtable cloneParams = (Hashtable)params.clone();
				Map cloneParams = params;
				cloneParams.put("ZoneCode", p);
				
				try {
					if(tmpResult != null)  isFirst = false;
					tmpResult = p004Telcomm.query(cloneParams);
					totalcount += Integer.parseInt(tmpResult.getValueByFieldName("TotalRecord"));
					waitpayrecord += Integer.parseInt(tmpResult.getValueByFieldName("WaitPayRecord"));
					paysuccessrecord += Integer.parseInt(tmpResult.getValueByFieldName("PaySuccessRecord"));
					payfailrecord += Integer.parseInt(tmpResult.getValueByFieldName("PayFailRecord"));
					
					if(!isFirst)
					{
						helper.getOccurs().addRows(tmpResult.getOccurs());
					}else
					{
						helper = tmpResult;
					}
				}
				catch(TopMessageException e) {
					//tr = new MVHImpl();
				}
				
			}
			if(null == helper) throw TopMessageException.create("R806", "N614");
			helper.getFlatValues().put("TotalRecord",totalcount+ "");
			helper.getFlatValues().put("WaitPayRecord",waitpayrecord+ "");
			helper.getFlatValues().put("PaySuccessRecord",paysuccessrecord+ "");
			helper.getFlatValues().put("PayFailRecord",payfailrecord+ "");
			helper.getFlatValues().put("Count",totalcount+ "");
			helper.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
			helper.getFlatValues().put("CMSEACHPERDATE", periodStr);
		}
		else
		{		
			helper = p004Telcomm.query(params);
			helper.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
			helper.getFlatValues().put("CMSEACHPERDATE", periodStr);
		}
		/*******************************************************************************/
		return helper;
	}
}
