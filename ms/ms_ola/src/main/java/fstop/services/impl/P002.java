package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class P002 extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());
	
	@Autowired
    @Qualifier("p002Telcomm")
	private TelCommExec p002Telcomm;
	
//	@Required
//	public void setP002Telcomm(TelCommExec telCommExec) {
//		this.p002Telcomm = telCommExec;
//	}
	
	@Override
	public MVH doAction(Map params) {		
		TelcommResult helper = p002Telcomm.query(params);
		helper.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));

		return helper;
	}
}
