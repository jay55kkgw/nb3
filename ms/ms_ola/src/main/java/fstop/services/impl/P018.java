package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;

public class P018 extends CommonService {
	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("p018Telcomm")
	private TelCommExec p018Telcomm;
	
//	@Required
//	public void setP018Telcomm(TelCommExec telcomm) {
//		this.p018Telcomm = telcomm;
//	}
	
	@Override
	public MVH doAction(Map params) {		
		TelcommResult helper = p018Telcomm.query(params);
		helper.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));

		return helper;
	}
}
