package fstop.services.impl;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
//import java.util.HashMap;
//import java.util.Date;
import java.util.Hashtable;
//import java.util.Iterator;
import java.util.List;
//import java.util.List;
//import java.util.Map;
import java.util.Map;

//import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.core.BeanUtils;
import fstop.exception.TopMessageException;
//import fstop.model.DBResult;
import fstop.model.MVH;
import fstop.model.MVHImpl;
//import fstop.model.Row;
//import fstop.model.Rows;
import fstop.orm.dao.TxnLoanApplyDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.TXNLOANAPPLY;
import fstop.orm.po.TXNUSER;
import fstop.services.CommonService;
import fstop.telcomm.cmd.CommandUtils;
import fstop.util.DateTimeUtils;
import fstop.util.IPUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import fstop.util.thread.WebServletUtils;
import lombok.extern.slf4j.Slf4j;
//import fstop.util.JSONUtils;
//import fstop.util.StrUtils;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import java.security.SecureRandom;


/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class NA04 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	private TxnLoanApplyDao txnLoanApplyDao;
	
//	@Required
//	public void setTxnLoanApplyDao(TxnLoanApplyDao txnLoanApplyDao) {
//		this.txnLoanApplyDao = txnLoanApplyDao;
//	}
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 
		log.info("==========NA04.java IN==========");
		Map<String, String> _params = params;
		
		log.info(ESAPIUtil.vaildLog("_params:"+CodeUtil.toJson(_params)));
		
		MVHImpl telcommResult = new MVHImpl();
//		MVHImpl cardata = new MVHImpl();
		List<String> commands = new ArrayList();
		String fgtxway = _params.get("FGTXWAY").toString();
		try {
			if(_params.containsKey("pkcs7Sign") && "1".equals(fgtxway)) {
				commands.add("XMLCA()");
				commands.add("XMLCN()");
				
				CommandUtils.doBefore(commands, _params);

			}
//			Gson gson = new Gson();
			sun.misc.BASE64Encoder encoder= new sun.misc.BASE64Encoder();//base64加密
//			sun.misc.BASE64Decoder decoder= new sun.misc.BASE64Decoder();//base64解密			
//			NA04Request na04 = new NA04Request();
			
			SecureRandom secrandom = new SecureRandom(); //取六位亂數
			String randomval="";
			for(int i=0;i<6;i++)  		   
			{  		    		   
				randomval +=secrandom.nextInt(10);
			}
			log.info("NA04.java randomval---->"+randomval);
			String rcvno = (String)_params.get("RCVNO");//取小額信貸申請資料查詢的案件編號
			String act =encoder.encode("query".getBytes());//分為query:查詢 unlock:離開頁面時通知eloan解lock check:完成對保
			String key =encoder.encode(rcvno.getBytes());//由前一頁案件編號帶進來
			log.info("NA04.java act before:"+act);
			log.info(ESAPIUtil.vaildLog("NA04.java key before:"+key));
						
			String localIP = IPUtils.getLocalIp();//取得IP位址
	        String defaultBaseUrl ="";	        	        	        
	        
	        if(localIP.equals("10.16.22.17")){//測試環境
	        	defaultBaseUrl = "http://10.16.22.155/TBBeLoan/scrt/outter/Sc2EBankWeb.jsp?act="+act+"&key="+key+"&ss="+randomval;
	        }else{							  //營運環境	
	        	defaultBaseUrl = "https://10.16.21.68:11122/ZoneParkFees/ParkfeesServlet";
	        }
	        log.info(ESAPIUtil.vaildLog("NA04.java defaultBaseUrl:"+defaultBaseUrl));
			URL obj = new URL(defaultBaseUrl);
	        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
	        con.setRequestMethod("POST");
	        con.setRequestProperty("Content-Type", "application/xml");
	        con.setDoOutput(true);
	        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
//	        logger.info("NA04.java JSON After:"+JSONUtils.toJson(na04).getBytes("UTF8"));
//	        wr.write(JSONUtils.toJson(na04).getBytes("UTF8"));	        
	        wr.flush();
	        wr.close();
	        
	        int responseCode = con.getResponseCode();
	        log.info("NA04.java responseCode:" + responseCode);
	        
	        StringBuffer response = new StringBuffer();
	        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(),"UTF-8"));
	        String inputLine;
	        while ((inputLine = in.readLine()) != null) {
	            response.append(inputLine);
	        }
	        in.close();
	        
	        log.info(ESAPIUtil.vaildLog("NA04.java response Json:" + response.toString()));
//	        NA04Response r = (NA04Response)gson.fromJson(response.toString(), NA04Response.class);	
	        String json = response.toString();
	        log.info(ESAPIUtil.vaildLog("NA04.java json:" + json));
	        //Map<String, String> jm = JSONUtils.json2map(json);
//	        JSONUtils.toList(json);
//	        String newjson = json.replaceAll(",", ":").replace("[", "{").replace("]", "}");
//	        logger.info("newjson:" + newjson);
//	        Map<String, String> jm = JSONUtils.json2map(newjson);
//	        logger.info("NA04.java jm OTHKEY:"+jm.get("OTHKEY"));
	        JSONArray jsonArray = JSONArray.fromObject(json);
//	        JSONObject myjObject = null;
//	        for(int i =0;i <jsonArray.size() ;i++) {
//	        	myjObject = jsonArray.getJSONObject(i);
//	        }
//	        for(int i=0; i < jsonArray.size();i++) {
//	        	logger.info("getString("+i+"):" + jsonArray.getString(i));
//		        //logger.info("getString 1:" + jsonArray.getString(1));//法院
//	        }
//	        NA04Response r = (NA04Response)gson.fromJson(loandata, NA04Response.class);	        
//	        describePoToParams(telcommResult,myjObject);
//	        logger.info("REPAY:" + jm.get("REPAY"));//還款方式
	        //0:尚無資料 1:經辦編制中 2:待主管放行 3:待對保(這個才做事) -1:不知道怎麼錯的 -7:尚未核定 -6:非核准 -8:無法鎖定文件 -9:超過30天不給對保(或核定日期有誤)
	        //9:線上對保完成
	        String docflow = (String)jsonArray.getString(5);
	        log.info(ESAPIUtil.vaildLog("docflow:" + docflow));//案件狀態
	        if(docflow.equals("0") ||
	           docflow.equals("1") ||
	           docflow.equals("2")   ) {//審核中
	        	throw TopMessageException.create("Z899");
	        }
	        if(docflow.equals("9")) {//對保已完成
	        	throw TopMessageException.create("Z898");
	        }
	        if(docflow.equals("-1") ||
 	           docflow.equals("-6") ||
 	           docflow.equals("-7") ||  
 	           docflow.equals("-8") ||
 	           docflow.equals("-9")   ) {//系統異常
 	        	throw TopMessageException.create("ZX99");
 	        }
	        //String fgtxway =(String)_params.get("FGTXWAY");//認證方式
	        //String rcvno ="";
	        String appidno = "";
	        String appname ="";
	        String city2 ="";
	        String zone2 ="";
	        String addr2 ="";
	        String phone02 ="";//通訊電話
	        String phone03 ="";//通訊電話(分機)

	        List<TXNLOANAPPLY> result = txnLoanApplyDao.findByRCVNO(jsonArray.getString(1));//查詢某一筆案件編號資料	        
	        for(TXNLOANAPPLY ad : result) {
	        	//rcvno = ad.getRCVNO();//案件編號
	        	appidno = ad.getAPPIDNO();//統編
	        	appname = ad.getAPPNAME();//姓名
	        	city2 = ad.getCITY2();//住宅縣市
	        	zone2 = ad.getZONE2();//住宅區域
	        	addr2 = ad.getADDR2();//住宅地址
	        	phone02 = ad.getPHONE_02();//通訊電話
	        	phone03 = ad.getPHONE_03();//通訊電話(分機)
			}
	        telcommResult.getFlatValues().put("OTHKEY", jsonArray.getString(1));   //OTHKEY	      網銀自用案件編號                        
	        telcommResult.getFlatValues().put("RCVNO", jsonArray.getString(3));    //RCVNO 	   eloan用的案件編號(不存進網銀DB)
	        telcommResult.getFlatValues().put("DOCFLOW", jsonArray.getString(5));  //DOCFLOW   案件狀態                       
	        telcommResult.getFlatValues().put("LOANAMT", jsonArray.getString(7));  //LOANAMT   借款金額                       
	        telcommResult.getFlatValues().put("REPAY", jsonArray.getString(9));    //REPAY     還款方式                       
	        telcommResult.getFlatValues().put("LOANYYS",jsonArray.getString(11));  //LOANYYS   借款期間年起                   
	        telcommResult.getFlatValues().put("LOANMMS",jsonArray.getString(13));  //LOANMMS   借款期間月起                   
	        telcommResult.getFlatValues().put("LOANDDS",jsonArray.getString(15));  //LOANDDS   借款期間日起                   
	        telcommResult.getFlatValues().put("LOANYYE",jsonArray.getString(17));  //LOANYYE   借款期年止                     
	        telcommResult.getFlatValues().put("LOANMME",jsonArray.getString(19));  //LOANMME   借款期月止                     
	        telcommResult.getFlatValues().put("LOANDDE",jsonArray.getString(21));  //LOANDDE   借款期日止                     
	        telcommResult.getFlatValues().put("SYSTEMFEE",jsonArray.getString(23));//SYSTEMFEE 系統作業費                     
	        telcommResult.getFlatValues().put("CREDITFEE",jsonArray.getString(25));//CREDITFEE 信用查詢費                     
	        telcommResult.getFlatValues().put("PAYDAY",jsonArray.getString(27));   //PAYDAY    撥款日                         
	        telcommResult.getFlatValues().put("REPAYDAY",jsonArray.getString(29)); //REPAYDAY  每月繳款日                     
	        telcommResult.getFlatValues().put("IPO1CHK",jsonArray.getString(31));  //IPO1CHK   貸款費用1                      
	        telcommResult.getFlatValues().put("IPO1RATEM",jsonArray.getString(33));//IPO1RATEM 基準利率月調整％計算           
	        telcommResult.getFlatValues().put("IPO1RATEY",jsonArray.getString(35));//IPO1RATEY 加年利率 ％                    
	        telcommResult.getFlatValues().put("IPO1RATES",jsonArray.getString(37));//IPO1RATES 合計年利率％                   
	        telcommResult.getFlatValues().put("IPO2CHK",jsonArray.getString(39));  //IPO2CHK   貸款費用2                      	        
	        telcommResult.getFlatValues().put("FSTYYS",jsonArray.getString(41));   //FSTYYS   第一次繳款日(年)
	        telcommResult.getFlatValues().put("FSTMMS",jsonArray.getString(43));   //FSTMMS   第一次繳款日(月)
	        telcommResult.getFlatValues().put("FSTDDS",jsonArray.getString(45));   //FSTDDS   第一次繳款日(日)	        
	        telcommResult.getFlatValues().put("IPO2YYS",jsonArray.getString(47));  //IPO2YYS   自民國 年起                    
	        telcommResult.getFlatValues().put("IPO2MMS",jsonArray.getString(49));  //IPO2MMS   自民國 月起                    
	        telcommResult.getFlatValues().put("IPO2DDS",jsonArray.getString(51));  //IPO2DDS   自民國 日起                    
	        telcommResult.getFlatValues().put("IPO2YYE",jsonArray.getString(53));  //IPO2YYE   至民國 年止                    
	        telcommResult.getFlatValues().put("IPO2MME",jsonArray.getString(55));  //IPO2MME   至民國 月止                    
	        telcommResult.getFlatValues().put("IPO2DDE",jsonArray.getString(57));  //IPO2DDE   至民國 日止                    
	        telcommResult.getFlatValues().put("IPO2RATEM",jsonArray.getString(59));//IPO2RATEM 款機動利率 ％                  
	        telcommResult.getFlatValues().put("IPO2RATEY",jsonArray.getString(61));//IPO2RATEY 加年利率 ％                    
	        telcommResult.getFlatValues().put("IPO2RATES",jsonArray.getString(63));//IPO2RATES 目前合計年利率 ％              
	        telcommResult.getFlatValues().put("COURT",jsonArray.getString(65));    //COURT     法院
	        telcommResult.getFlatValues().put("LIMITM",jsonArray.getString(75));   //LIMITM    借款總期數;
	        //telcommResult.getFlatValues().put("RCVNO",rcvno);    //RCVNO    DB案件編號
	        telcommResult.getFlatValues().put("APPIDNO",appidno);//APPIDNO  DB統編   
	        telcommResult.getFlatValues().put("APPNAME",appname);//APPNAME  DB姓名 
	        
	        telcommResult.getFlatValues().put("CITY2",city2);//CITY2  DB住宅縣市
	        telcommResult.getFlatValues().put("ZONE2",zone2);//ZONE2  DB住宅區域
	        telcommResult.getFlatValues().put("ADDR2",addr2);//ADDR2  DB住宅地址
	        telcommResult.getFlatValues().put("PHONE_02",phone02);//PHONE_02  DB通訊電話
	        telcommResult.getFlatValues().put("PHONE_03",phone03);//PHONE_03  DB通訊電話(分機)
	        telcommResult.getFlatValues().put("FGTXWAY",fgtxway);//交易機制
	        	        
	        log.info("====NA04.java try end====");
		}
		catch (TopMessageException e) {
			log.info("TopMessageException error:"+e.getMsgcode());
			telcommResult.getFlatValues().put("ERRCODE", e.getMsgcode());			
		}catch(Exception e){
			log.info("ERROR:"+e.getMessage());
			telcommResult.getFlatValues().put("ERRCODE", "ZX99");
		}
			
		log.info("====NA04.java end====");
		
		return telcommResult;
	}
	
}
