package fstop.services.impl;

import java.util.Date;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Map;
import java.util.StringTokenizer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

import com.netbank.rest.model.CommonPools;
import com.netbank.rest.model.FieldTranslator;
import com.netbank.rest.model.JSPUtils;

import fstop.exception.TopMessageException;
import fstop.model.IMasterValuesHelper;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.notifier.NotifyAngent;
import fstop.orm.dao.AdmBranchDao;
import fstop.orm.dao.AdmEmpInfoDao;
import fstop.orm.dao.TxnAcnApplyDao;
import fstop.orm.dao.TxnCardLogDao;
import fstop.orm.dao.TxnRejDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.dao.VaSignDataDao;
import fstop.orm.po.TXNACNAPPLY;
import fstop.orm.po.TXNCARDLOG;
import fstop.orm.po.TXNREJ;
import fstop.orm.po.TXNUSER;
import fstop.orm.po.VASIGNDATA;
import fstop.services.CommonService;
import fstop.services.batch.CheckServerHealth;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import fstop.va.VaCheck;
import fstop.ws.AMLWebServiceTemplate;
import fstop.ws.aml.bean.AML001RQ;
import fstop.ws.aml.bean.AML001RQ.SearchNamesSoap;
import fstop.ws.aml.bean.AML001RQ.SearchNamesSoap.Branch;
import fstop.ws.aml.bean.AML001RQ.SearchNamesSoap.DateOfBirth;
import fstop.ws.aml.bean.AML001RQ.SearchNamesSoap.Names;
import fstop.ws.aml.client.SearchResponseSoap;
import lombok.extern.slf4j.Slf4j;
import fstop.mq.jcic.*;


/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class N203 extends CommonService  {

	@Autowired
	@Qualifier("na30Telcomm")
	private TelCommExec na30Telcomm;
	@Autowired
	@Qualifier("nb30Telcomm")
	private TelCommExec nb30Telcomm;
	@Autowired
	@Qualifier("nb31Telcomm")
	private TelCommExec nb31Telcomm;
	@Autowired
	@Qualifier("n203Telcomm")
	private TelCommExec n203Telcomm;
	@Autowired
	@Qualifier("n931Telcomm")
	private TelCommExec n931Telcomm;	
//	@Autowired
//	@Qualifier("n930Telcomm")
//	private TelCommExec n930Telcomm; //用戶電子郵箱位址變更	
	@Autowired
	@Qualifier("n913Telcomm")
	private TelCommExec n913Telcomm;	
	
	@Autowired
	@Qualifier("a105Telcomm")
    private TelCommExec a105Telcomm; //20181207 Add by 邦文 H150704661增加任職機構欄位電文	
	
	@Autowired
	@Qualifier("n007Telcomm")
    private TelCommExec n007Telcomm; //20190730 Add by 邦文 數位帳戶拒絕同步中心資料庫
    
//	private Logger logger = Logger.getLogger(N203.class);
	private Map ordersetting;
	@Autowired
	private TxnUserDao txnUserDao;
	@Autowired
	private TxnAcnApplyDao txnAcnApplyDao;
	@Autowired
	private VaSignDataDao vaSignDataDao;
	@Autowired
	private TxnCardLogDao txnCardLogDao;
	@Autowired
	private AdmBranchDao admBranchDao;
	@Autowired
	private AdmEmpInfoDao admEmpInfoDao;
	@Autowired
	private TxnRejDao txnRejDao;
	@Autowired
	private CommonPools commonPools;
	
	@Autowired
	private AMLWebServiceTemplate amlWebServiceTemplate;

	@Autowired
	private TESTMQ testmq;

	@Value("${jcic.mq.use}")
	private String mq_use;
//	@Required
//	public void setNa30Telcomm(TelCommExec na30Telcomm) {
//		this.na30Telcomm = na30Telcomm;
//	}
//	@Required
//	public void setNb30Telcomm(TelCommExec nb30Telcomm) {
//		this.nb30Telcomm = nb30Telcomm;
//	}
//	@Required
//	public void setNb31Telcomm(TelCommExec nb31Telcomm) {
//		this.nb31Telcomm = nb31Telcomm;
//	}	
//	@Required
//	public void setN203Telcomm(TelCommExec telcomm) {
//		n203Telcomm = telcomm;
//	}
//	public void setOrdersetting(Map ordersetting) {
//		this.ordersetting = ordersetting;
//	}
//	@Required
//	public void setN931Telcomm(TelCommExec telcomm) {
//		n931Telcomm = telcomm;
//	}	
//	public void setN930Telcomm(TelCommExec telCommExec) {
//		this.n930Telcomm = telCommExec;
//	}	
//	@Required
//	public void setTxnUserDao(TxnUserDao txnUserDao) {
//		this.txnUserDao = txnUserDao;
//	}
//	@Required
//	public void setTxnAcnApplyDao(TxnAcnApplyDao txnAcnApplyDao) {
//		this.txnAcnApplyDao = txnAcnApplyDao;
//	}
//	@Required
//	public void setVaSignDataDao(VaSignDataDao vaSignDataDao) {
//		this.vaSignDataDao = vaSignDataDao;
//	}
//	@Required
//	public void setTxnCardLogDao(TxnCardLogDao txnCardLogDao) {
//		this.txnCardLogDao = txnCardLogDao;
//	}
//	@Required
//	public void setN913Telcomm(TelCommExec telcomm) {
//		n913Telcomm = telcomm;
//	}
//	
//	@Required
//	public void setAdmBranchDao(AdmBranchDao admBranchDao) {
//		this.admBranchDao = admBranchDao;
//	}
//	
//	@Required
//	public void setAdmEmpInfoDao(AdmEmpInfoDao admEmpInfoDao) {
//		this.admEmpInfoDao = admEmpInfoDao;
//	}
//	
//	@Required
//	public void setTxnRejDao(TxnRejDao txnRejDao) {
//		this.txnRejDao = txnRejDao;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params) {
		Map<String, String> params = _params;		
		log.trace("N203 param: {}", params);
		MVHImpl telcommResult = new MVHImpl();
		MVHImpl telcommResult1 = new MVHImpl();
		String MSGCODE = "";
		String FGTXWAY = params.get("FGTXWAY").toString();  //交易機制 2:晶片金融卡 4:自然人憑證
		String CHECKSIGN = "";
		String PLAINTEXT = "";
		String SIGNEDTEXT = "";
		String EMAIL = params.get("MAILADDR");
		//String N913MSGCODE = "";
		String HASRISK   = (String) params.get("HASRISK")==null ? "" : (String) params.get("HASRISK");
		try {
			_params.put("ADOPID", "N203");
			HASRISK = amlWebServiceTemplate.callAML(_params) ? "Y": "N";
			params.put("HASRISK", HASRISK);
		}catch(Exception e){
			log.error(e.toString());
		}
		log.warn("N203.java FGTXWAY:"+FGTXWAY);
		log.warn("N203.java MAILADDR:"+params.get("MAILADDR"));
		log.warn("N203.java threecode:"+params.get("MAILADDR").substring(1, 4));
		EMAIL = EMAIL.substring(0,EMAIL.indexOf("@")+1)+EMAIL.substring(EMAIL.indexOf("@")+1).toLowerCase(); //將@後轉換為小寫
		log.warn("N203.java EMAIL:"+EMAIL);
		if (!"10.".equals(params.get("IP").toString().substring(0, 3))) { //是否使用行內IP
			if(admEmpInfoDao.isEmpCUSID((String) params.get("CUSIDN"))){  //是否為行員ID
				if(EMAIL.indexOf("@")==4){                                    //判斷email是否為四位數
					if("b".equals(EMAIL.substring(0,1))&&                     //判斷email第一個字是否為b
							admBranchDao.isBranchMail(EMAIL.substring(1, 4))){ //判斷是否為分行代碼
						throw TopMessageException.create("Z625");
					}
				}
			}else{
				if(EMAIL.indexOf("@")==4){                                    //判斷email是否為四位數
					if(("b".equals(EMAIL.substring(0,1))&&                     //判斷email第一個字是否為b
							admBranchDao.isBranchMail(EMAIL.substring(1, 4)))||//判斷是否為分行代碼
							"mail.tbb.com.tw".equals(EMAIL.substring(EMAIL.indexOf("@")+1))){//判斷@後是否為台企 
						throw TopMessageException.create("Z625");
					}
				}			
			}
			
		} else {
			if(EMAIL.indexOf("@")==4){ //判斷email是否為四位數
				if("b".equals(EMAIL.substring(0,1))&& //判斷email第一個字是否為b
						admBranchDao.isBranchMail(EMAIL.substring(1, 4))){ //判斷是否為分行代碼
					throw TopMessageException.create("Z625");
				}
			}
		}
				
		//晶片金融卡
		if("2".equals(FGTXWAY)) {
			Date d = new Date();
			String trin_transeq = "1234";
			String trin_issuer = "05000000";
			String trin_acnno = params.get("ACNNO").toString();
			String trin_icdttm = DateTimeUtils.format("yyyyMMddHHmmss", d);
			log.warn("ICDTTM ----------> " + trin_icdttm);
			String trin_iseqno = params.get("iSeqNo").toString();
			String trin_icmemo = "";
			String trin_tac_length = "000A";		
			String trin_tac = params.get("TAC").toString();
			log.warn("TAC ----------> " + trin_tac_length + trin_tac + "(" + (trin_tac_length.length() + trin_tac.length()) + ")");
			String trin_trmid = params.get("TRMID").toString();
		
			params.put("TRANSEQ", trin_transeq);
			params.put("ISSUER", trin_issuer);
			params.put("ACNNO", trin_acnno);
			params.put("ICDTTM", trin_icdttm);
			params.put("ICSEQ", trin_iseqno);
			params.put("ICMEMO", trin_icmemo);
			params.put("TAC_Length", trin_tac_length);
			params.put("TAC", trin_tac);
			params.put("TRMID", trin_trmid);
			params.put("ACNTYPE", "A2");
		}		
		 //自然人憑證
		if(params.containsKey("pkcs7Sign") && "4".equals(FGTXWAY)) {
			//壓簽章字串比對
			String jsondc = StrUtils.trim(params.get("jsondc").toString());			
			String __mac = StrUtils.trim(params.get("pkcs7Sign").toString());
			params.put("__SIGN", __mac);	
			params.put("ACNTYPE", "A1");
			log.warn("JSONDC:"+jsondc);
			log.warn("pkcs7Sign:"+__mac);
			PLAINTEXT = jsondc;
			SIGNEDTEXT = __mac;
			String CertInfo = VaCheck.doVAVerify(__mac, jsondc, "VARule1");  //_mac:元件簽章後字串 jsondc:簽章前字串以utf-8 編碼過
			int VerfyICSrtn = -1;
			if(CertInfo.length()==0) {
				MSGCODE="Z089";
				CheckServerHealth checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
				checkserverhealth.sendZ300Notice("Z089");
				log.warn("N203.java doVAVerify fail: cert length zero");
				CHECKSIGN="1";
			} else {
				VerfyICSrtn = VaCheck.doVerfyICS(CertInfo, params.get("CUSIDN").toString(), true);
				if(VerfyICSrtn!=0)
				{
					MSGCODE="Z089";
					CheckServerHealth checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
					checkserverhealth.sendZ300Notice("Z089");
					log.warn("N203.java doVerfyICS fail");
					CHECKSIGN="1";
				}
				else
					CHECKSIGN="0";
			}			
		}			
		//2021/04/22 Ziv, 跨行金融帳戶驗證
		if("5".equals(FGTXWAY)) {
			params.put("ACNTYPE", "A3");
		}
		//===================================================================================
		//聯徵查詢Z21 start
		String msg = "";
		String occurMsg="";
		String NA30MSG="";
		boolean NBOPENSTATUS=false;
		String Z21MSGCODE="";
		String Z22MSGCODE="";
		//String N913MSGCODE="";
		boolean MBOPENSTATUS=false;
		Date d = new Date();
		String df = DateTimeUtils.format("yyyyMMdd", d);
		String tf = DateTimeUtils.format("HHmmss", d);
		String RTYPE1="";
		int JCICCNT = 0;
		
		//檢查是否開啟 聯徵查詢
		if(mq_use.equals("Y")) {
			if(!MSGCODE.equals("Z089")) {
				params.put("BANKID","050");
				params.put("BRHID","IF01");
				params.put("DATE", df);
				params.put("TIME", tf);
				params.put("TID", "QZ21");  //從HZ21改成QZ21來查純文字資料
				params.put("QID", "NNB     ");
				params.put("MEMREV","C93922687   ");
				params.put("ERRCODE","D0000");
				log.warn("N203.java BANKCODE:"+params.get("BANKCODE").toString());
				if(params.get("BANKCODE").toString().length()>3)
					params.put("PAYID",params.get("BANKCODE").toString().substring(3));//由開戶行通匯代號代入
				else
					params.put("PAYID", "0500");
				params.put("USERID","USER001   ");
				params.put("REASONCODE","AXA");
				params.put("FILLER01","      ");
				params.put("FILLER02"," ");
				params.put("CUSIDN",(String)params.get("UID"));
				log.warn("N203.java CHGE_DT:"+params.get("CHGE_DT").toString());
				log.warn("N203.java ID_CHGE:"+params.get("ID_CHGE").toString());
				log.warn("N203.java BIRTHDAY:"+params.get("BIRTHDAY").toString());		
				String FILLER03=params.get("CHGE_DT").toString()+params.get("ID_CHGE").toString()+params.get("BIRTHDAY").toString();
				log.warn("N203.java FILLER03 LENGTH:"+FILLER03.length());
				params.put("FILLER03",FILLER03);
				String HAS_PIC=params.get("HAS_PIC").toString().equals("Y") ? "0" : "1";
				log.warn("N203.java HAS_PIC:"+HAS_PIC);
				String QUERYPRINT=HAS_PIC+FieldTranslator.transfer("N203", "CHGE_CY", params.get("CHGE_CY").toString());
				String space="";
				for(int i=0;i<50-QUERYPRINT.length();i++)
				{
					space+=" ";	
				}
				QUERYPRINT=QUERYPRINT+space;
				log.warn("N203.java QUERYPRINT:"+QUERYPRINT +"LENGTH:"+QUERYPRINT.length());
				params.put("QUERYPRINT",QUERYPRINT);
				try {
					msg = testmq.process(params);
					log.warn("N203.java QZ21 msg:"+msg);
			    	int token = -1;
			    	token = msg.indexOf("STM011");
			    	String Z21MSG = "";
			    	if(token>-1)
			    		Z21MSG = msg.substring(0,token);
			    	else if(msg.indexOf("STS012")>-1)
			    		Z21MSG = msg.substring(0,msg.indexOf("STS012"));
			    	else
			    		Z21MSG = msg;
			    	log.warn("N203.java Z21MSG:"+Z21MSG);
			    	//查詢Z21狀態，不符合Y代表異常顯示Z618訊息
			    	StringTokenizer sz21 = new StringTokenizer(Z21MSG,"\r\n");
			    	while(sz21.hasMoreTokens()) {
			    		String linestr = sz21.nextToken();
			    		log.warn("N203.java linestr:"+linestr);
			    		if(linestr.indexOf("VAS004")>-1)
			    		{
			    			if(!linestr.substring(40,41).equals("Y"))
			    			{
								log.warn("N203.java Z21 STATUS:"+linestr.substring(40,41)+" Z21MSGCODE:Z618");  
								Z21MSGCODE ="Z618";
			    			}
			    		}
			    	}	
			    	if(token>-1)
			    		msg = msg.substring(token);
			    	else if(msg.indexOf("STS012")>-1)
			    		msg = msg.substring(msg.indexOf("STS012"));
			    	
			    	String data = msg ;
			    	StringTokenizer st  = new StringTokenizer(data,"\r\n");
			    	int STM011CNT =0;  //當日Z21新存款開戶查詢 次數統計
			    	int STS012CNT =0;  //最近三個月(不含當日)Z21 新存款開戶查詢 次數統計
			    	while(st.hasMoreTokens()) {
			    		String tempstr = st.nextToken();
			    		if(tempstr.indexOf("STM011")>-1)
			    		{
			    			log.warn(tempstr+":"+tempstr.replaceAll("[^\\x00-\\xff]", "**").substring(53,54));
			    			if(tempstr.replaceAll("[^\\x00-\\xff]", "**").substring(53,54).equals("3") || tempstr.replaceAll("[^\\x00-\\xff]", "**").substring(53,54).equals("5")) //把中文字取代成二個*號，java計算長度截取字串才不會截取錯誤
			    				STM011CNT++;						
			    		}
			    		if(tempstr.indexOf("STS012")>-1)
			    		{
			    			log.warn(tempstr+":"+tempstr.substring(16,21));
			    			STS012CNT = Integer.parseInt(tempstr.substring(16,21));					
			    		}
			    	}
			    	log.warn("N203.java STM011CNT:"+STM011CNT);
			    	log.warn("N203.java STS012CNT:"+STS012CNT);
					if((STM011CNT+STS012CNT)>=5)
					{
	                    JCICCNT=STM011CNT+STS012CNT;//20200605 增加聯徵開戶查詢次數記錄
						log.warn("N203.java JCIC COUNTER >=5 Z21MSGCODE:Z626");			
//						Z21MSGCODE = "Z626";  //聯徵查詢新開戶次數超過(含)五次，不得執行開戶
					}					
				}
				catch(Exception e)
				{			
					log.warn("N203.java Z21 QUERY ERROR:"+e.getMessage());
				}
			
				//聯徵查詢Z21 END
				//聯徵查詢Z22 start
				if(!Z21MSGCODE.equals("Z626")) {   //Z21 查詢新存款開戶次數>=5次  不用在查Z22
					d = new Date();
					df = DateTimeUtils.format("yyyyMMdd", d);
					tf = DateTimeUtils.format("HHmmss", d);		
					params.put("DATE", df);
					params.put("TIME", tf);
					params.put("TID", "HZ22");
					params.put("QUERYPRINT","?Mode=Inquired_Record.Y                           ");
					try {
						msg = testmq.process(params); 
						log.warn("HZ22 MSG:"+msg);
						if(msg.indexOf("HZ22NNB")>=0)
						{
								String Z22MSG1 = "";
								String Z22MSG2 = "";
								int Z22CNT = 0;
								if(msg.indexOf("通報案件紀錄資訊")>-1)
								{
									Z22MSG1 = msg.substring(msg.indexOf("通報案件紀錄資訊")+11,msg.indexOf("【其他補充註記】")-3);
									log.warn("N203.java Z22MSG1 coming str:"+Z22MSG1);
									if(Z22MSG1.indexOf("查揭露資料庫中無該項資訊")==-1)
									{	
										Z22MSGCODE="Z619";
										Z22CNT=Z22CNT+1;
										log.warn("N203.java Z22MSG1 CNT+1");
									}
									log.warn("N203.java Z22MSG1 result:"+Z22MSGCODE);	
								}
								if(msg.indexOf("其他補充註記")>-1)
								{
									Z22MSG2 = msg.substring(msg.indexOf("其他補充註記")+8);
									log.warn("N203.java Z22MSG2 coming str:"+Z22MSG2);
									if(Z22MSG2.indexOf("查揭露資料庫中無該項資訊")==-1)
									{	
										Z22MSGCODE="Z619";
										Z22CNT=Z22CNT+1;
										log.warn("Z22MSG2 CNT+1");
									}
									log.warn("N203.java Z22MSG2 result:"+Z22MSGCODE);	
								}				
								log.warn("N203.java Z22 find count:"+Z22CNT);
						}					
					}
					catch(Exception e)
					{
						log.warn("N203.java Z22 QUERY ERROR:"+e.getMessage());
					}
				}
				//聯徵查詢Z22 END
				//==============================================================================================
			}
		}
		
		TXNREJ po = new TXNREJ();
		po.setNAME(params.get("CUSNAME")==null ?"":params.get("CUSNAME").toString());
		po.setCUSIDN(params.get("CUSIDN")==null ?"":params.get("CUSIDN").toString());
		po.setAPPLYDAY(DateTimeUtils.format("yyyyMMdd", d));
		po.setBIRTHDAY(params.get("BIRTHDAY")==null ?"":params.get("BIRTHDAY").toString());
		po.setBLACKLIST(HASRISK);
		po.setCITYCHA(params.get("CITYCHA")==null ?"":params.get("CITYCHA").toString());
		po.setCMDATE(params.get("CMDATE2")==null ?"":String.valueOf((Integer.parseInt(params.get("CHGE_DT").toString().substring(0,3))+1911))+params.get("CHGE_DT").toString().substring(3,7)); //補換發時間
		po.setPURPOSE(params.get("PURPOSE")==null ?"":params.get("PURPOSE").toString());
		log.warn("N203.java po.setPURPOSE:"+params.get("PURPOSE"));
		po.setDIGVERSION(params.get("DIGVERSION")==null ?"":params.get("DIGVERSION").toString());
		po.setACNVERSION(params.get("ACNVERSION")==null ?"":params.get("ACNVERSION").toString());
		
		if(Z21MSGCODE.equals("Z626")){  //記錄聯徵查詢Z21、Z22結果
			po.setJCICFAILED("Z626");
		}else if(!Z21MSGCODE.equals("Z626") && Z21MSGCODE.equals("Z618") && Z22MSGCODE.equals("Z619")){
			po.setJCICFAILED("Z620");
		}else if(!Z21MSGCODE.equals("Z626") && Z21MSGCODE.equals("Z618") && !Z22MSGCODE.equals("Z619")){
			po.setJCICFAILED("Z618");
		}else if(!Z21MSGCODE.equals("Z626") && !Z21MSGCODE.equals("Z618") && Z22MSGCODE.equals("Z619")){
			po.setJCICFAILED("Z619");
		}else{
			po.setJCICFAILED(" ");
		}
		txnRejDao.save(po);


        
        //加發聯徵結果之電文 
        if(!RTYPE1.equals("")) {
            try {
                d = new Date();
                df = DateTimeUtils.getCDateShort(d);
                tf = DateTimeUtils.format("HHmmss", d);
                //輸入日期(電文)
                params.put("DATE", df);
                log.warn("N203.java N007 DATE:"+ params.get("DATE")==null ?"":params.get("DATE").toString());
                //輸入時間(電文)
                params.put("TIME", tf);
                log.warn("N203.java N007 TIME:" + params.get("TIME")==null ?"":params.get("TIME").toString());
                //輸入身分證統編(電文)
                log.warn("N203.java N007 CUSIDN:"+ params.get("CUSIDN")==null ?"":params.get("CUSIDN").toString());
                //輸入戶名(電文)
                log.warn("N203.java N007 NAME:"+params.get("NAME")==null ?"":params.get("NAME").toString());
                //輸入拒絕開戶主要原因(電文)
                params.put("RTYPE1", RTYPE1);
                log.warn("N203.java N007 RTYPE1:"+RTYPE1);
                //輸入補充說明(電文)
                params.put("MEMO", "");
                log.warn("N203.java N007 MEMO:"+ "");
                //輸入服務分行(電文)
                params.put("BRHCOD", params.get("BRHCOD")==null ? (String)params.get("BHID") : (String)params.get("BRHCOD"));
                params.put("BRHNO", params.get("BRHCOD")==null ? (String)params.get("BHID") : (String)params.get("BRHCOD"));
                log.warn("N203.java N007 BRHNO:"+params.get("BRHCOD")==null ? (String)params.get("BHID") : (String)params.get("BRHCOD"));
                //打N007電文
                telcommResult1 = n007Telcomm.query(params);
            }catch (TopMessageException e){
                log.warn("N203.java n007 fail MSGCOD:"+ e.getMessage());
            }
        }
        //-----------------------------------------------------------------------------------------------------------
        
		
		//查詢		
		d = new Date();
		df = DateTimeUtils.getCDateShort(d);
		tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);
		String NAME=JSPUtils.convertFullorHalf((String)params.get("NAME"), 1);
		String PMTADR=JSPUtils.convertFullorHalf((String)params.get("PMTADR"), 1);
		String CTTADR=JSPUtils.convertFullorHalf((String)params.get("CTTADR"), 1);
		params.put("CUSIDN", (String)params.get("UID"));
		params.put("NAME", NAME);
		params.put("PMTADR", PMTADR); 
		params.put("CTTADR", CTTADR);
		params.put("BRHCOD", params.get("BRHCOD")==null ? (String)params.get("BHID") : (String)params.get("BRHCOD"));
    	params.put("DEGREE",((String) params.get("DEGREE")).equals("#") ? "" : ((String) params.get("DEGREE")));
    	params.put("MARRY", ((String) params.get("MARRY")).equals("#") ? "" : ((String) params.get("MARRY")));
    	params.put("CHIPACN", params.get("CHIP_ACN")==null ? "" : (String)params.get("CHIP_ACN"));
		String ACN ="";
		String BRHCOD=params.get("BRHCOD")==null ? (String)params.get("BHID") : (String)params.get("BRHCOD");
//		String pathname = (String)ordersetting.get("pathname");				
//		String uploadpath = (String) params.get("UPLOADPATH");
//		if(!StrUtils.trim(pathname).endsWith("/")) 
//			pathname = StrUtils.trim(pathname) + "/";
//		if(!StrUtils.trim(uploadpath).endsWith("/"))
//			uploadpath = StrUtils.trim(uploadpath) + "/";
//		String FILE1 = (String) params.get("FILE1")==null ? "" : (String) params.get("FILE1");
//		String FILE2 = (String) params.get("FILE2")==null ? "" : (String) params.get("FILE2");
//		String FILE3 = (String) params.get("FILE3")==null ? "" : (String) params.get("FILE3");
//		String FILE4 = (String) params.get("FILE4")==null ? "" : (String) params.get("FILE4");
		String rFILE1 =(String) params.get("FILE1")==null ? "" : (String) params.get("FILE1");
		String rFILE2 =(String) params.get("FILE2")==null ? "" : (String) params.get("FILE2");
		String rFILE3 =(String) params.get("FILE3")==null ? "" : (String) params.get("FILE3");
		String rFILE4 =(String) params.get("FILE4")==null ? "" : (String) params.get("FILE4");
		String CHECKNB = (String) params.get("CHECKNB")==null ? "" : (String) params.get("CHECKNB");
		String EBILLFLAG = (String) params.get("EBILLFLAG")==null ? "" : (String) params.get("EBILLFLAG");
		log.warn("N203.java CHECKNB:"+CHECKNB+" EBILLFLAG:"+EBILLFLAG);
		
		boolean chkway = false;
		boolean chkna30 = false;
		try {
			if(!Z21MSGCODE.equals("Z626") && !Z21MSGCODE.equals("Z618") && !Z22MSGCODE.equals("Z619") && !MSGCODE.equals("Z089"))
			{		
				log.warn("N203.java N203 MAINFUND:"+params.get("MAINFUND").toString());
				log.warn("N203.java N203 MAPPING MAINFUND:"+FieldTranslator.transfer("N203","MAINFUND1", params.get("MAINFUND").toString()));
				params.put("PURPOSE1", FieldTranslator.transfer("N203","PURPOSE1", params.get("PURPOSE").toString()));  //開戶目的
				params.put("RENAME", params.get("RENAME").toString());     //是否更換過姓名
				params.put("HAS_RISK", params.get("HASRISK").toString());  //AML黑名單註記
				params.put("R_CUSIDN", txnAcnApplyDao.findCUSIDNByMAILADDRCELPHONE(params.get("MAILADDR").toString(),params.get("CELPHONE").toString()).toString());  //關係人統編
				params.put("MAINFUND", FieldTranslator.transfer("N203","MAINFUND1", params.get("MAINFUND").toString())); //主要資金/財產來源
                String nJCICCNT=JCICCNT<10 ? "0"+String.valueOf(JCICCNT) : String.valueOf(JCICCNT);
                params.put("JCICCNT", nJCICCNT);//20200605 增加聯徵開戶查詢次數記錄
                params.put("LINKFLG",  params.get("outside_source")==null ? "" : (String)params.get("outside_source"));//外部連結
				telcommResult = n203Telcomm.query(params);
				occurMsg=telcommResult.getValueByFieldName("MSGCOD");  //取得電文代碼							

				params.put("N203MSGCOD", occurMsg);
				telcommResult.getFlatValues().put("N203MSGCOD", occurMsg);
				log.warn("N203.java N203 MSGCOD:"+occurMsg);
				telcommResult.getFlatValues().put("occurMsg", occurMsg);
				telcommResult.getFlatValues().put("CHECKNB", CHECKNB);
				if(occurMsg.equals("0000") || occurMsg.trim().equals(""))  //線上開戶作業成功
				{
					ACN = telcommResult.getValueByFieldName("ACN"); //數位存款帳戶
					params.put("ACN", ACN);
					params.put("TROUFG", params.get("NAATAPPLY").toString());
					params.put("TROSUM", params.get("CChargeApply").toString());
					params.put("CROSS", params.get("CROSSAPPLY").toString());
					String CARDAPPLY = params.get("CARDAPPLY")==null ? "" : params.get("CARDAPPLY").toString();
					try {
						if(CARDAPPLY.equals("Y")) {
							telcommResult = nb31Telcomm.query(params);  //數位帳戶申請晶片金融卡
							occurMsg=telcommResult.getValueByFieldName("MSGCOD");
							//telcommResult.getFlatValues().put("occurMsg", occurMsg);
							if(occurMsg.equals("0000") || occurMsg.trim().equals(""))  //申請晶片金融卡作業成功
							{
								log.warn("N203.java NB31 success ACN:"+ACN);
							}
						}
					} catch(TopMessageException e) {
						occurMsg = e.getMsgcode();
						//telcommResult.getFlatValues().put("occurMsg", occurMsg);
						log.warn("N203.java NB31 fail MSGCOD:"+occurMsg+" ACN:"+ACN);
					}
					//FTP作業
//					if(FILE1.length()>0)
//						ftpFileToETABS1(pathname + ((String)params.get("UID")).toUpperCase()+"_01"+(FILE1.substring(FILE1.lastIndexOf(".jpg"), FILE1.length())).toLowerCase(),uploadpath + FILE1);
//					if(FILE2.length()>0)
//						ftpFileToETABS1(pathname + ((String)params.get("UID")).toUpperCase()+"_02"+(FILE2.substring(FILE2.lastIndexOf(".jpg"), FILE2.length())).toLowerCase(),uploadpath + FILE2);							
//					if(FILE3.length()>0)
//						ftpFileToETABS1(pathname + ((String)params.get("UID")).toUpperCase()+"_03"+(FILE3.substring(FILE3.lastIndexOf(".jpg"), FILE3.length())).toLowerCase(),uploadpath + FILE3);							
//					if(FILE4.length()>0)
//						ftpFileToETABS1(pathname + ((String)params.get("UID")).toUpperCase()+"_04"+(FILE4.substring(FILE4.lastIndexOf(".jpg"), FILE4.length())).toLowerCase(),uploadpath + FILE4);		    		
					log.warn("N203.java get ACN:"+ACN);
					log.warn("N203.java get BRHCOD:"+BRHCOD);	
//					if(FILE1.length()>0)
//						rFILE1 =((String)params.get("UID")).toUpperCase()+"_01"+(FILE1.substring(FILE1.lastIndexOf(".jpg"), FILE1.length())).toLowerCase();
//					if(FILE2.length()>0)
//						rFILE2 = ((String)params.get("UID")).toUpperCase()+"_02"+(FILE2.substring(FILE2.lastIndexOf(".jpg"), FILE2.length())).toLowerCase();
//					if(FILE3.length()>0)
//						rFILE3 = ((String)params.get("UID")).toUpperCase()+"_03"+(FILE3.substring(FILE3.lastIndexOf(".jpg"), FILE3.length())).toLowerCase();
//					if(FILE4.length()>0)
//						rFILE4 = ((String)params.get("UID")).toUpperCase()+"_04"+(FILE4.substring(FILE4.lastIndexOf(".jpg"), FILE4.length())).toLowerCase();

					
					if(CHECKNB.equals("N") && ("2".equals(FGTXWAY) || "4".equals(FGTXWAY) || "5".equals(FGTXWAY) || "6".equals(FGTXWAY))) {
						chkway = true;
						//NA30 晶片金融卡線上申請網銀交易  第二類舊戶使用
						if("2".equals(FGTXWAY)) {						
							params.put("TYPE", "02");
							params.put("CARDNUM", (String)params.get("ACNNO"));
							params.put("CUSIDN", (String)params.get("CUSIDN"));
							params.put("MOBILE", params.get("CELPHONE")==null ? "" : (String)params.get("CELPHONE"));
							params.put("MAIL", (String)params.get("MAILADDR"));
							params.put("ICSEQ", (String)params.get("ICSEQ"));
							params.put("ATMTRAN", (String)params.get("TRFLAG"));
							params.put("FGTXWAY", "");
						}
						//NB30 第一類、第三類新戶使用...2021/04/22 Ziv修改
						if("4".equals(FGTXWAY) || "5".equals(FGTXWAY)) {	
							String type = FGTXWAY.equals("4") ? "01" : "03"; 
							params.put("TYPE", type);
							params.put("FGTXWAY", "");
						}
						if("6".equals(FGTXWAY)) {
							params.put("TYPE", "04");
							params.put("FGTXWAY", "");
						}
						//logger.warn("N203.java old HLOGINPIN:"+(String)params.get("HLOGINPIN"));
						//logger.warn("N203.java old HTRANSPIN:"+(String)params.get("HTRANSPIN"));
						
						try{/*關閉第二類舊戶申請需打NA30電文申請網銀，改成不論新舊客戶都打NB30申請網銀
							if("2".equals(FGTXWAY))
							{	
								telcommResult = na30Telcomm.query(params);
								NA30MSG=telcommResult.getValueByFieldName("MSGCOD");
								logger.warn("N203.java NA30 MSGCOD:"+NA30MSG);
							}
							else {*/
							telcommResult = nb30Telcomm.query(params);
							NA30MSG=telcommResult.getValueByFieldName("MSGCOD");
							log.warn("N203.java NB30 MSGCOD:"+NA30MSG);
							//}
							//logger.warn("N203.java new HLOGINPIN:"+(String)params.get("HLOGINPIN"));
							//logger.warn("N203.java new HTRANSPIN:"+(String)params.get("HTRANSPIN"));
							if(NA30MSG.equals("0000"))  //晶片金融卡線上申請網銀成功
							{ 				    								
								chkna30 = true;
								NBOPENSTATUS=true;
								if("2".equals(FGTXWAY))
								{
									TXNCARDLOG cardlog = new TXNCARDLOG();		
									cardlog.setLOGDATE(DateTimeUtils.format("yyyyMMdd", d));
									cardlog.setLOGTIME(DateTimeUtils.format("HHmmss", d));
									cardlog.setDPSUERID((String)params.get("CUSIDN"));
									cardlog.setCARDTYPE("1");
									cardlog.setCRADNO((String)params.get("CARDNUM"));
									cardlog.setOPERATETYPE("02");
									cardlog.setREMOTEIP((String)params.get("IP"));
									cardlog.setSTATUS("Y");
									cardlog.setADMCODE("");
									txnCardLogDao.save(cardlog);
								}
								//N930更新中心EMAIL位址
//								params.put("E_MAIL_ADR", (String)params.get("MAILADDR"));
//								params.put("FLAG", "02");  //更新需用02打中心更心
//								try {
//									telcommResult = n930Telcomm.query(params);
//									log.warn("N203.java n930 MSGCOD:"+telcommResult.getValueByFieldName("MSGCOD"));
//								}catch(TopMessageException e) {
//									log.warn("N203.java n930 fail MSGCOD:"+e.getMessage());
//								}
								//N931開通電子帳單註記
								params.put("FGTXWAY", FGTXWAY);
								params.put("PPSYNC", "");
								params.put("PINKEY", "");
								params.put("PPSYNCN", "");
								params.put("PINNEW", "");
								params.put("TYPE", "01");
								params.put("ITEM", "10");		
								params.put("CARDAP", "Y");
								try {
									telcommResult = n931Telcomm.query(params);
									log.warn("N203.java n931 MSGCOD:"+telcommResult.getValueByFieldName("MSGCOD"));
								}catch(TopMessageException e) {
									log.warn("N203.java n931 fail MSGCOD:"+e.getMessage());
								}
								
								//申請成功發送Email通知申請者_使用晶片金融卡線上申請網銀
								/*關閉第二類舊戶申請需發送MAIL通知網銀開戶結果及啟用行動銀行電文 2018.01.26 By Sox 
								  if("2".equals(FGTXWAY)) {
									Hashtable<String, String> varmail1 = new Hashtable();
									varmail1.put("SUB","晶片金融卡申請網路銀行成功通知");
									varmail1.put("MAILTEXT", getContent1());
									if(!NotifyAngent.sendNotice("NA30",varmail1,(String)params.get("MAILADDR"))){
										logger.warn("晶片金融卡申請網路銀行發送Email失敗.(mail:"+(String)params.get("MAILADDR")+")");
									}
									params.put("PPSYNC", "");
									params.put("PINKEY", "");
									params.put("OPTION", "1");   //啟用行動銀行服務								
									try{
										telcommResult = n913Telcomm.query(params);
										N913MSGCODE=telcommResult.getValueByFieldName("MSGCOD");
										if(N913MSGCODE.equals("0000") || N913MSGCODE.trim().equals(""))
											MBOPENSTATUS=true;
				
										telcommResult.getFlatValues().put("N913MSGCODE", N913MSGCODE);
										logger.warn("N203.java N913 MSGCODE:"+N913MSGCODE);
									}
									catch(TopMessageException e) {
										telcommResult.getFlatValues().put("N913MSGCODE", e.getMessage());
										logger.warn("N203.java N913 MSGCODE:"+e.getMessage());								
									}
								}*/
							}
						}catch (TopMessageException e) {
							NA30MSG = e.getMsgcode();
							telcommResult.getFlatValues().put("NA30MSGCOD", NA30MSG);
							telcommResult.getFlatValues().put("CHECKNB", CHECKNB);
							telcommResult.getFlatValues().put("ACNTYPE", params.get("ACNTYPE").toString());
							log.warn("NB30 MSGCOD:"+NA30MSG);
						}
					}
					else
					{
		                if("Y".equals(params.get("HASRISK").toString())){
						   txnRejDao.save(po);	
		                }
						//N930更新中心EMAIL位址
//						params.put("E_MAIL_ADR", (String)params.get("MAILADDR"));
//						params.put("FLAG", "02");  //更新需用02打中心更心
//						try {
//							telcommResult = n930Telcomm.query(params);
//							log.warn("N203.java n930 MSGCOD:"+telcommResult.getValueByFieldName("MSGCOD"));
//						}catch(TopMessageException e) {
//							log.warn("N203.java n930 fail MSGCOD:"+e.getMessage());
//						}
						//N931開通電子帳單註記
						params.put("FGTXWAY", FGTXWAY);
						params.put("PPSYNC", "");
						params.put("PINKEY", "");
						params.put("PPSYNCN", "");
						params.put("PINNEW", "");
						params.put("TYPE", "01");
						params.put("ITEM", "10");		
						params.put("CARDAP", "Y");
						try {
							telcommResult = n931Telcomm.query(params);
							log.warn("N203.java n931 MSGCOD:"+telcommResult.getValueByFieldName("MSGCOD"));
						}catch(TopMessageException e) {
							log.warn("N203.java n931 fail MSGCOD:"+e.getMessage());
						}						
					}
					
					//發送Email通知_線上開戶
					String mail  = (String)params.get("MAILADDR");
					Hashtable<String, String> varmail = new Hashtable();
					varmail.put("SUB","完成線上開戶通知");
					varmail.put("MAILTEXT", getContent(ACN,BRHCOD,NBOPENSTATUS,CHECKNB,MBOPENSTATUS));
					if(!NotifyAngent.sendNotice("N203",varmail,mail)){
						log.warn("線上開戶通知發送Email失敗.(mail:"+mail+")");
					}

                    String employer = convertToFullWidth(params.get("EMPLOYER").toString());
                    try{
                        log.warn("N104_2.java A105 CUSIDN:"+params.get("CUSIDN").toString());
                        //輸入類別(電文)
                        params.put("TYPE", "U");
                        log.warn("N203.java a105 TYPE:" + params.get("TYPE").toString());
                        //輸入金額種類(電文)
                        params.put("CODE", "A");
                        log.warn("N203.java a105 CODE:"+ params.get("CODE").toString());
                        //輸入預期往來金額(電文)
                        params.put("SAMT", params.get("S_MONEY").toString());
                        log.warn("N203.java a105 SAMT:"+ params.get("SAMT").toString());
                        
                        log.warn("N203.java A105 RENAME:"+params.get("RENAME").toString());
                        
                        //輸入黃金存摺帳號(電文)
                        params.put("GDACN", "");
                        log.warn("N203.java a105 GDAN:"+ params.get("GDACN").toString());
                        
                        if (params.get("PURPOSE").equals("A"))
                        {   params.put("PURPOSE","1");}
                        if (params.get("PURPOSE").equals("B"))
                        {   params.put("PURPOSE","2");}
                        if (params.get("PURPOSE").equals("C"))
                        {   params.put("PURPOSE","4");}
                        if (params.get("PURPOSE").equals("D"))
                        {   params.put("PURPOSE","5");}
                        if (params.get("PURPOSE").equals("E"))
                        {   params.put("PURPOSE","6");}
                        log.warn("N203.java A105 PURPOSE:"+params.get("PURPOSE").toString());
                        
                        //輸入任職機構(電文)
                        params.put("CONPNAM", employer);
                        log.warn("N203.java a105 CONPNAM:"+ params.get("CONPNAM").toString());
                        //打A105電文
                        telcommResult1 = a105Telcomm.query(params);
                    }catch(TopMessageException e){
                        log.warn("N203.java a105 fail MSGCOD:"+ e.getMessage());
                    }
					//記錄線上開戶資料
					TXNACNAPPLY acnapply = new TXNACNAPPLY();
					log.warn("N203.java CUSIDN:"+(String)params.get("UID") + " LENGTH:"+((String)params.get("UID")).length());
					acnapply.setCUSIDN((String)params.get("UID"));
					log.warn("N203.java BRHCOD:"+BRHCOD + " LENGTH:"+BRHCOD.length());
					acnapply.setBRHCOD(BRHCOD);
					log.warn("N203.java NAME:"+NAME + " LENGTH:"+NAME.length());
					acnapply.setNAME(NAME);
					log.warn("N203.java BIRTHDAY:"+(String)params.get("BIRTHDAY") + " LENGTH:"+((String)params.get("BIRTHDAY")).length());
					acnapply.setBIRTHDAY((String)params.get("BIRTHDAY"));
					log.warn("N203.java POSTCOD1:"+(String)params.get("POSTCOD1") + " LENGTH:"+((String)params.get("POSTCOD1")).length());
					acnapply.setPOSTCOD1((String)params.get("POSTCOD1"));
					log.warn("N203.java PMTADR:"+(String)params.get("PMTADR") + " LENGTH:"+((String)params.get("PMTADR")).length());
					acnapply.setPMTADR((String)params.get("PMTADR"));
					log.warn("N203.java POSTCOD2:"+(String)params.get("POSTCOD2") + " LENGTH:"+((String)params.get("POSTCOD2")).length());
					acnapply.setPOSTCOD2((String)params.get("POSTCOD2"));
					log.warn("N203.java CTTADR:"+(String)params.get("CTTADR") + " LENGTH:"+((String)params.get("CTTADR")).length());
					acnapply.setCTTADR((String)params.get("CTTADR"));
					log.warn("N203.java HOMEZIP:"+(String)params.get("HOMEZIP") + " LENGTH:"+((String)params.get("HOMEZIP")).length());
					acnapply.setHOMEZIP(params.get("HOMEZIP")==null ? "" : params.get("HOMEZIP").toString());
					log.warn("N203.java HOMETEL:"+(String)params.get("HOMETEL") + " LENGTH:"+((String)params.get("HOMETEL")).length());
					acnapply.setHOMETEL(params.get("HOMETEL")==null ? "" : params.get("HOMETEL").toString());
					log.warn("N203.java BUSZIP:"+(String)params.get("BUSZIP") + " LENGTH:"+((String)params.get("BUSZIP")).length());
					acnapply.setBUSZIP(params.get("BUSZIP")==null ? "" : params.get("BUSZIP").toString());
					log.warn("N203.java BUSTEL:"+(String)params.get("BUSTEL") + " LENGTH:"+((String)params.get("BUSTEL")).length());
					acnapply.setBUSTEL(params.get("BUSTEL")==null ? "" : params.get("BUSTEL").toString());
					log.warn("N203.java CELPHONE:"+(String)params.get("CELPHONE") + " LENGTH:"+((String)params.get("CELPHONE")).length());
					acnapply.setCELPHONE(params.get("CELPHONE")==null ? "" : params.get("CELPHONE").toString());
					log.warn("N203.java ARACOD3:"+(String)params.get("ARACOD3") + " LENGTH:"+((String)params.get("ARACOD3")).length());
					acnapply.setARACOD3(params.get("ARACOD3")==null ? "" : params.get("ARACOD3").toString());
					log.warn("N203.java FAX:"+(String)params.get("FAX") + " LENGTH:"+((String)params.get("FAX")).length());
					acnapply.setFAX(params.get("FAX")==null ? "" : params.get("FAX").toString());
					log.warn("N203.java MAILADDR:"+(String)params.get("MAILADDR") + " LENGTH:"+((String)params.get("MAILADDR")).length());
					acnapply.setMAILADDR(params.get("MAILADDR").toString());
					log.warn("N203.java MARRY:"+(String)params.get("MARRY") + " LENGTH:"+((String)params.get("MARRY")).length());
					acnapply.setMARRY(params.get("MARRY")==null ? "" : params.get("MARRY").toString());
					log.warn("N203.java CHILD:"+(String)params.get("CHILD") + " LENGTH:"+((String)params.get("CHILD")).length());
					acnapply.setCHILD(params.get("CHILD")==null ? "" : params.get("CHILD").toString());
					log.warn("N203.java DEGREE:"+(String)params.get("DEGREE") + " LENGTH:"+((String)params.get("DEGREE")).length());
					acnapply.setDEGREE(params.get("DEGREE")==null ? "" : params.get("DEGREE").toString());
					log.warn("N203.java CAREER1:"+(String)params.get("CAREER1") + " LENGTH:"+((String)params.get("CAREER1")).length());
					acnapply.setCAREER1(params.get("CAREER1").toString());
					log.warn("N203.java CAREER2:"+(String)params.get("CAREER2") + " LENGTH:"+((String)params.get("CAREER2")).length());
					acnapply.setCAREER2(params.get("CAREER2").toString());
					log.warn("N203.java EMPLOYER:"+(String)params.get("EMPLOYER") + " LENGTH:"+((String)params.get("EMPLOYER")).length());
					acnapply.setEMPLOYER(params.get("EMPLOYER").toString());
					log.warn("N203.java SALARY:"+(String)params.get("SALARY") + " LENGTH:"+((String)params.get("SALARY")).length());
					acnapply.setSALARY(params.get("SALARY")==null ? "" : params.get("SALARY").toString());
					log.warn("N203.java TRFLAG:"+(String)params.get("TRFLAG") + " LENGTH:"+((String)params.get("TRFLAG")).length());
					acnapply.setTRFLAG(params.get("TRFLAG").toString());
					log.warn("N203.java EBILLFLAG:"+(String)params.get("EBILLFLAG") + " LENGTH:"+((String)params.get("EBILLFLAG")).length());
					acnapply.setEBILLFLAG(params.get("EBILLFLAG").toString());
					if (params.get("PURPOSE").equals("1"))
					{	params.put("PURPOSE","A");}
					if (params.get("PURPOSE").equals("2"))
					{	params.put("PURPOSE","B");}
					if (params.get("PURPOSE").equals("4"))
					{	params.put("PURPOSE","C");}
					if (params.get("PURPOSE").equals("5"))
					{	params.put("PURPOSE","D");}
					if (params.get("PURPOSE").equals("6"))
					{	params.put("PURPOSE","E");}
					log.warn("N203.java PURPOSE:"+(String)params.get("PURPOSE") + " LENGTH:"+((String)params.get("PURPOSE")).length());
					acnapply.setPURPOSE(params.get("PURPOSE").toString());
					log.warn("N203.java PREASON:"+(String)params.get("PREASON") + " LENGTH:"+((String)params.get("PREASON")).length());
					acnapply.setPREASON(params.get("PREASON")==null ? "" : params.get("PREASON").toString());
					log.warn("N203.java BDEALING:"+(String)params.get("BDEALING") + " LENGTH:"+((String)params.get("BDEALING")).length());
					acnapply.setBDEALING(params.get("BDEALING").toString());
					log.warn("N203.java BREASON:"+(String)params.get("BREASON") + " LENGTH:"+((String)params.get("BREASON")).length());
					acnapply.setBREASON(params.get("BREASON")==null ? "" : params.get("BREASON").toString());
					log.warn("N203.java ACN:"+ACN + " LENGTH:"+ACN.length());
					acnapply.setACN(ACN);
					log.warn("N203.java N203MSGCOD:"+(String)params.get("N203MSGCOD") + " LENGTH:"+((String)params.get("N203MSGCOD")).length());
					acnapply.setN203MSGCOD(params.get("N203MSGCOD")==null ? "" : params.get("N203MSGCOD").toString());
					log.warn("N203.java NA30MSGCOD:"+NA30MSG + " LENGTH:"+NA30MSG.length());
					acnapply.setNA30MSGCOD(NA30MSG);
					log.warn("N203.java FILE1:"+rFILE1 + " LENGTH:"+rFILE1.length());
					acnapply.setFILE1(rFILE1);
					log.warn("N203.java FILE2:"+rFILE2 + " LENGTH:"+rFILE2.length());
					acnapply.setFILE2(rFILE2);
					log.warn("N203.java FILE3:"+rFILE3 + " LENGTH:"+rFILE3.length());
					acnapply.setFILE3(rFILE3);
					log.warn("N203.java FILE4:"+rFILE4 + " LENGTH:"+rFILE4.length());
					acnapply.setFILE4(rFILE4);
					log.warn("N203.java IP:"+(String)params.get("IP") + " LENGTH:"+((String)params.get("IP")).length());
					acnapply.setIP(params.get("IP").toString());
					acnapply.setLASTDATE(DateTimeUtils.format("yyyyMMdd", d));
					acnapply.setLASTTIME(DateTimeUtils.format("HHmmss", d));
					log.warn("N203.java CARDAPPLY:"+(String)params.get("CARDAPPLY") + " LENGTH:"+((String)params.get("CARDAPPLY")).length());
					acnapply.setCARDAPPLY(params.get("CARDAPPLY").toString());
					log.warn("N203.java NAATAPPLY:"+(String)params.get("NAATAPPLY") + " LENGTH:"+((String)params.get("NAATAPPLY")).length());
					acnapply.setNAATAPPLY(params.get("NAATAPPLY").toString());
					log.warn("N203.java CChargeApply:"+(String)params.get("CChargeApply") + " LENGTH:"+((String)params.get("CChargeApply")).length());
					acnapply.setCChargeApply(params.get("CChargeApply").toString());
					log.warn("N203.java CROSSAPPLY:"+(String)params.get("CROSSAPPLY") + " LENGTH:"+((String)params.get("CROSSAPPLY")).length());
					acnapply.setCROSSAPPLY(params.get("CROSSAPPLY").toString());
					log.warn("N203.java ACNTYPE:"+(String)params.get("ACNTYPE") + " LENGTH:"+((String)params.get("ACNTYPE")).length());
					acnapply.setACNTYPE(params.get("ACNTYPE").toString());					
					log.warn("N203.java CRDTYP:"+(String)params.get("CRDTYP") + " LENGTH:"+((String)params.get("CRDTYP")).length());
					acnapply.setCRDTYP(params.get("CRDTYP").toString());					
					log.warn("N203.java LMT:"+(String)params.get("LMT") + " LENGTH:"+((String)params.get("LMT")).length());
					acnapply.setLMT(params.get("LMT").toString());					
					log.warn("N203.java LINKFLG:"+(String)params.get("outside_source") + " LENGTH:"+((String)params.get("outside_source")).length());
					acnapply.setLINKFLG(params.get("outside_source")==null ? "" : (String)params.get("outside_source"));//外部連結
					txnAcnApplyDao.save(acnapply);	
					if("4".equals(FGTXWAY)) {
					//記錄自然人憑證簽章資訊						
						try {
							VASIGNDATA vasigndata = new VASIGNDATA();	
							vasigndata.setCUSIDN((String)params.get("UID"));
							vasigndata.setPLAINTEXT(PLAINTEXT.getBytes());
							vasigndata.setSIGNEDTEXT(SIGNEDTEXT.getBytes());
							vasigndata.setCHECKSIGN(CHECKSIGN);
							vasigndata.setIP(params.get("IP").toString());
							vasigndata.setLASTDATE(DateTimeUtils.format("yyyyMMdd", d));
							vasigndata.setLASTTIME(DateTimeUtils.format("HHmmss", d));
							vaSignDataDao.save(vasigndata);
						}
						catch(Exception e) {
							//telcommResult.getFlatValues().put("occurMsg", "ZX99");
							log.warn("vaSignDataDao save Exception:"+e.getMessage());
						}
					}
					String EMPLOYER;
					EMPLOYER= commonPools.applyUtils.getEMPLOYERByCusidn(params.get("CUSIDN").toString());
					telcommResult.getFlatValues().put("ACN", ACN);
					telcommResult.getFlatValues().put("BRHCOD", BRHCOD);
					telcommResult.getFlatValues().put("CHECKNB", CHECKNB);
					telcommResult.getFlatValues().put("NA30MSGCOD", NA30MSG);
					telcommResult.getFlatValues().put("EMPLOYER", EMPLOYER);
					telcommResult.getFlatValues().put("ACNTYPE", params.get("ACNTYPE").toString());
				}
			}
			else 
			{
				telcommResult.getFlatValues().put("CHECKNB", CHECKNB);
				telcommResult.getFlatValues().put("ACNTYPE", params.get("ACNTYPE").toString());
//				if(removefile(uploadpath + FILE1)) log.debug("remove pic1 successful");
//				else log.warn("remove pic1 fail");
//				if(removefile(uploadpath + FILE2)) log.debug("remove pic2 successful");
//				else log.warn("remove pic2 fail");
//				if(removefile(uploadpath + FILE3)) log.debug("remove pic3 successful");					
//				else log.warn("remove pic3 fail");
//				if(removefile(uploadpath + FILE4)) log.debug("remove pic4 successful");					
//				else log.warn("remove pic4 fail");
				
				if(!MSGCODE.equals("Z089")) {
					if(Z21MSGCODE.equals("Z626") && Z21MSGCODE.length()>0)					
					{
						telcommResult.getFlatValues().put("occurMsg", Z21MSGCODE);	
					}					
					if((Z21MSGCODE.equals("Z618") && !Z22MSGCODE.equals("Z619")) && (Z21MSGCODE.length()>0))					
					{
						telcommResult.getFlatValues().put("occurMsg", Z21MSGCODE);	
					}
					if((!Z21MSGCODE.equals("Z626") && !Z21MSGCODE.equals("Z618") && Z22MSGCODE.equals("Z619")) && (Z22MSGCODE.length()>0))
					{
						telcommResult.getFlatValues().put("occurMsg", Z22MSGCODE);	
					}
					if((Z21MSGCODE.equals("Z618") && Z22MSGCODE.equals("Z619")) && (Z21MSGCODE.length()>0 && Z22MSGCODE.length()>0))
					{
						telcommResult.getFlatValues().put("occurMsg", "Z620");	
					}
				}
				else
				{
					telcommResult.getFlatValues().put("occurMsg", MSGCODE);
				}
					
				log.warn("N203.java goto else occurMsg:"+occurMsg);
			}
		} catch (TopMessageException e) {
			occurMsg = e.getMsgcode();
			telcommResult.getFlatValues().put("N203MSGCOD", occurMsg);
			telcommResult.getFlatValues().put("occurMsg", occurMsg);
			telcommResult.getFlatValues().put("CHECKNB", CHECKNB);
			telcommResult.getFlatValues().put("ACNTYPE", params.get("ACNTYPE").toString());
//			if(removefile(uploadpath + FILE1)) log.debug("remove pic1 successful");
//			else log.warn("remove pic1 fail");
//			if(removefile(uploadpath + FILE2)) log.debug("remove pic2 successful");
//			else log.warn("remove pic2 fail");
//			if(removefile(uploadpath + FILE3)) log.debug("remove pic3 successful");					
//			else log.warn("remove pic3 fail");
//			if(removefile(uploadpath + FILE4)) log.debug("remove pic4 successful");					
//			else log.warn("remove pic4 fail");
		}
		finally {
			//更新TXNUSER
			try {
				TXNUSER user = null;
				if(chkna30) {
					user = txnUserDao.chkRecordNotify((String)params.get("CUSIDN"), (String)params.get("MAILADDR"), true, true);
				}
				
				if(!chkway && !chkna30) {
					user = txnUserDao.chkRecordNotify((String)params.get("CUSIDN"), (String)params.get("MAILADDR"), false, false);
				}
				
				if(user == null) {
					log.warn("txnUserDao chkRecordNotify fail");
				}
			}
			catch (Exception e) {
				// TODO: handle exception
				log.error("TXNUSER ERROR>>>{}",e);
			}
			
		}
		params.remove("CUSIDN");
		params.remove("UID");				
	    return telcommResult;
	}

	@SuppressWarnings("unused")
	private String getContent(String ACN,String BRHCOD,boolean NBOPENSTATUS,String CHECKNB,boolean MBOPENSTATUS){

    	if(ACN.length()==11)
		{	
			ACN = ACN.substring(0,3)+"-"+ACN.substring(3,5)+"-"+ACN.substring(5);
    	}
		IMasterValuesHelper bhContact = commonPools.fundUtils.getAdmBhContact(BRHCOD);
		String BRHNAME = bhContact.getValueByFieldName("ADBRANCHNAME",1);		
		String BRHTEL = bhContact.getValueByFieldName("TELNUM",1);	
		StringBuilder sb = new StringBuilder ();
		sb.append("<table cellSpacing=0 cellPadding=0 width=600 border=0 frame=border >").append("\n");
		sb.append("<tbody>").append("\n");
		sb.append("<tr><td align=left>親愛的客戶您好：</td></tr></br>").append("\n");
		sb.append("<tr><td align=left>謝謝您!完成本行『數位存款帳戶帳戶服務』申請作業").append("，本行將儘速辦理您的開戶申請，並以e-mail方式通知您辦理結果。</td></tr>").append("\n");
		//if(CHECKNB.equals("N"))
		//	sb.append("<b><font color=red>(").append((NBOPENSTATUS==true) ? "一般網路銀行開戶已成功" : "一般網路銀行開戶失敗，請自行執行線上申請網路銀行服務").append((MBOPENSTATUS==true) ? "，行動銀行已啟用成功" : "，行動銀行啟用失敗，請自行啟用行動銀行服務").append(")</b></font></td></tr>").append("\n");
		sb.append("<tr><td align=left>若有任何問題，您可撥電話至您所指定服務分行：").append(BRHNAME).append("(聯絡電話：").append(BRHTEL).append(")詢問，我們將竭誠為您服務。</td></tr>").append("\n");
		sb.append("<tr><td align=left>祝您事業順心，萬事如意!</td></tr>").append("\n");
		sb.append("<tr><td align=left><BR><BR>提醒您!本行將審核您所上傳之資料，如有模糊不清或資料缺漏，將有專人將與您聯繫，如您未於申請日起20日內(含)補正，您的數位存款帳戶申請將失效。</td></tr>").append("\n");
		sb.append("<tr><td align=right><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;臺灣企銀&nbsp;&nbsp;&nbsp;敬上</td></tr>").append("\n");
		sb.append("</tbody>").append("\n");
		sb.append("</table>").append("\n");
		return sb.toString();
	}
	@SuppressWarnings("unused")
	private String getContent1(){
		StringBuilder sb = new StringBuilder ();
		sb.append("<table cellSpacing=0 cellPadding=0 width=600 border=0 frame=border >").append("\n");
		sb.append("<tbody>").append("\n");
		sb.append("<tr><td align=left>").append("親愛的客戶，您好：</td></tr></br>").append("\n");
		sb.append("<tr><td align=left>感謝您申請臺灣企銀網路銀行服務！即日起，您將可以更快速、更安全的透過本行網路銀行查詢您相關帳務資訊，歡迎立即登入網路銀行使用。</td></tr>").append("\n");
		sb.append("<tr><td align=rigth>臺灣企銀   敬啟</td></tr>").append("\n");
		sb.append("</tbody>").append("\n");
		sb.append("</table>").append("\n");
		return sb.toString();
	}	
//	public boolean removefile(String localPath) {
//		try
//		{
//			File DelFile = new File(localPath);
//			DelFile.delete();
//			log.warn("刪除圖檔成功:"+localPath);
//			return(true);
//		}
//		catch(Exception RemoveFileEx)
//		{
//			log.warn("刪除圖檔失敗"+localPath);
//			return(false);
//		}		
//	}
//	public boolean ftpFileToETABS1(String eloanPath, String localPath) {
		//TODO:FtpBase64有問題，先註解
//		logger.info("@FTP@ " + " " + localPath + " " + eloanPath);
//    	String localIP = IPUtils.getLocalIp();
//		String ip = (String)ordersetting.get("ftp.ip");
//		String tmp = "/TBB/nnb/applyfile";
//		FtpBase64 ftpBase64 = new FtpBase64(ip);
//		int rc=ftpBase64.upload(eloanPath, localPath);
//		if(rc!=0)
//		{
//			logger.warn("FTP 失敗(ETABS 主機一次) FILE："+eloanPath);
//			CheckServerHealth checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
//			checkserverhealth.sendUnknowNotice("FTP 失敗(ETABS 主機一次) FILE："+eloanPath);
//			rc=ftpBase64.upload(eloanPath, localPath);
//			if(rc!=0)
//			{	
//				logger.warn("FTP 失敗(ETABS 主機二次) FILE："+eloanPath);
//				checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
//				checkserverhealth.sendUnknowNotice("FTP 失敗(ETABS 主機二次) FILE："+eloanPath);
//				return(false);
//			}
//			else
//			{	
//				try
//				{
//					File DelFile = new File(localPath);
//					FileUtils.copyFileTo(DelFile, tmp);
//				    RuntimeExec runtimeExec=null;
//				    if(localIP.equals("10.16.22.17"))
//				    	runtimeExec=new RuntimeExec(tmp,"chown -R apt003:apgrp "+tmp+"/");
//				    else
//				    	runtimeExec=new RuntimeExec(tmp,"chown -R dcp001:dcpgrp "+tmp+"/");				    		
//				    if(runtimeExec.getcommstat()!=0)
//					{
//						logger.warn("chown  exec command error");
//					}																			
//					DelFile.delete();
//					logger.warn("刪除圖檔成功:"+localPath);
//				}
//				catch(Exception e)
//				{
//					logger.warn("複製或刪除圖檔Path:"+localPath);
//					logger.warn("複製或刪除圖檔Fail:"+e.toString());
//				}								
//				return(true);				
//			}
//		}
//		else
//		{
//			try
//			{
//				File DelFile = new File(localPath);
//				FileUtils.copyFileTo(DelFile, tmp);
//			    RuntimeExec runtimeExec=null;
//			    if(localIP.equals("10.16.22.17"))
//			    	runtimeExec=new RuntimeExec(tmp,"chown -R apt003:apgrp "+tmp+"/");
//			    else
//			    	runtimeExec=new RuntimeExec(tmp,"chown -R dcp001:dcpgrp "+tmp+"/");				    		
//			    if(runtimeExec.getcommstat()!=0)
//				{
//					logger.warn("chown  exec command error");
//				}												
//				DelFile.delete();
//				logger.warn("刪除圖檔成功:"+localPath);
//			}
//			catch(Exception e)
//			{
//				logger.warn("複製或刪除圖檔Path:"+localPath);
//				logger.warn("複製或刪除圖檔Fail:"+e.toString());
//			}					
//		}
//		return true;
//	}	
	
	public boolean getAmlmsg(Map reqParam) {
		try{
			String name = (String)reqParam.get("NAME");
			String BRTHDY = (String)reqParam.get("BIRTHDAY");
			int yearnum = Integer.parseInt(BRTHDY.substring(0,3))+1911;
			String year = String.valueOf(yearnum);
			String month = BRTHDY.substring(3,5);
			String day = BRTHDY.substring(5,7);
			
			
			AML001RQ rq  = new AML001RQ();
			SearchNamesSoap  searchNamesSoap = new SearchNamesSoap();
			Names names = new Names();
			LinkedList list = new LinkedList<String>();
			list.add(name);
			names.setName(list);
			Branch branch = new Branch();
			branch.setBranchId((String)reqParam.get("BRHCOD"));
			branch.setBusinessUnit("H57");
			branch.setSourceSystem("NNB-W100089655");
			DateOfBirth dateOfBirth = new DateOfBirth();
			dateOfBirth.setYear(year);
			dateOfBirth.setMonth(month);
			dateOfBirth.setDay(day);
			searchNamesSoap.setNames(names);
			searchNamesSoap.setBranch(branch);
			searchNamesSoap.setDateOfBirth(dateOfBirth);
			rq.setMSGNAME("AML001");
			rq.setAPP("XML");
			rq.setCLIENTIP("::1");
			rq.setTYPE("01");
			rq.setSearchNamesSoap(searchNamesSoap);
			
			SearchResponseSoap rs = amlWebServiceTemplate.sendAndReceive (rq);
			return rs.isHit();
		}catch(Exception e) {
			log.debug(e.toString());
		}
		return true;
		
	} 
    public static String convertToFullWidth(String input) { 
        if (input==null)
        {
            return null;
        }
        
        char[] charArray = input.toCharArray(); 
        for ( int i = 0 ; i < charArray.length; i ++ ) { 
            int ic =(int) charArray[i];
            if (ic>= 33 && ic<=126) { 
                charArray[i] =(char)(ic+65248); 
            } else if (ic == 32) { 
                charArray[i] = ( char )12288; 
 
            }
        } 
        return new String(charArray); 
    }
}
