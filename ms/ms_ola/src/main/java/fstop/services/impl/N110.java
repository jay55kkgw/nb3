package fstop.services.impl;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.RowsSelecter;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N110 extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("n110Telcomm")
	private TelCommExec n110Telcomm;

//	@Required
//	public void setN110Telcomm(TelCommExec n110Telcomm) {
//		this.n110Telcomm = n110Telcomm;
//	}

	@Override
	public MVH doAction(Map params) {
		MVHImpl result = n110Telcomm.query(params);
		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		result.getFlatValues().put("CMRECNUM", result.getValueOccurs("ACN") + "");
		
		RowsSelecter rowSelecter = new RowsSelecter(result.getOccurs());
		DecimalFormat fmt = new DecimalFormat("0.00");
		Double adpibal = (double)rowSelecter.sum("ADPIBAL");
		result.getFlatValues().put("TOTAL_ADPIBAL", fmt.format(adpibal));
		Double bdpibal = (double)rowSelecter.sum("BDPIBAL");
		result.getFlatValues().put("TOTAL_BDPIBAL", fmt.format(bdpibal));
		Double clr = (double)rowSelecter.sum("CLR");
		result.getFlatValues().put("TOTAL_CLR", fmt.format(clr));
		return result;
	}
}
