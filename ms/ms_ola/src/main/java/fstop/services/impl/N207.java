package fstop.services.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N207 extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("n207Telcomm")
	private TelCommExec n207Telcomm;

//	@Required
//	public void setN110Telcomm(TelCommExec n110Telcomm) {
//		this.n110Telcomm = n110Telcomm;
//	}

	@Override
	public MVH doAction(Map params) {
		
		MVHImpl result = n207Telcomm.query(params);
		return result;
	
	}
}
