package fstop.services.impl;

import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.rest.model.CommonPools;
import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.services.CommonService;
import lombok.extern.slf4j.Slf4j;
/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class N104 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());
	

	@Autowired
	private CommonPools commonPools;
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 

		//查詢

		log.warn(ESAPIUtil.vaildLog("N104.java CUSIDN:"+params.get("CUSIDN")));
		MVHImpl telcommResult = new MVHImpl();
		try {
			String ACNTYPE = commonPools.applyUtils.getACNTYPEByCusidn(params.get("CUSIDN").toString());
			telcommResult.getFlatValues().put("ACNTYPE", ACNTYPE);
			telcommResult.getFlatValues().put("CUSIDN", params.get("CUSIDN").toString());
			telcommResult.getFlatValues().put("UID", params.get("CUSIDN").toString());
			if(ACNTYPE.length()!=2/* || ACNTYPE.equals("A2")*/)  //限定只有第1類自然人憑證可以修改資料-->開放第二類也可以修改基本資料
				throw TopMessageException.create("Z617");
		}
		catch(Exception e){
			log.warn("N104.java get acntype Exception:"+e.getMessage());
			throw TopMessageException.create("Z617");
		}
		return telcommResult;
	}


}
