package fstop.services.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.rest.model.CommonPools;
import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.IMasterValuesHelper;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.services.CommonService;
import fstop.services.batch.CheckServerHealth;
import fstop.telcomm.TelCommExec;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import fstop.va.VaCheck;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class NB32_1 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("nb32Telcomm")
	private TelCommExec nb32Telcomm;
	@Autowired
	private CommonPools commonPools;
	
//	@Required
//	public void setNb32Telcomm(TelCommExec telcomm) {
//		nb32Telcomm = telcomm;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 

		//查詢
		log.warn(ESAPIUtil.vaildLog("NB32_1.java FGTXWAY:"+params.get("FGTXWAY")));
		log.warn(ESAPIUtil.vaildLog("NB32_1.java CUSIDN:"+params.get("CUSIDN")));
		MVHImpl telcommResult = new MVHImpl();
		String MSGCODE = "";
		String ACN = params.get("ACN").toString();
		String BRHCOD = params.get("BRHCOD").toString();
		//晶片金融卡
		if("2".equals(params.get("FGTXWAY").toString()))
		{
			/*
			Date d = new Date();
			String trin_transeq = "1234";
			String trin_issuer = "05000000";
			String trin_acnno = params.get("ACNNO").toString();
			String trin_icdttm = DateTimeUtils.format("yyyyMMddHHmmss", d);
			logger.warn("ICDTTM ----------> " + trin_icdttm);
			String trin_iseqno = params.get("iSeqNo").toString();
			String trin_icmemo = "";
			String trin_tac_length = "000A";		
			String trin_tac = params.get("TAC").toString();
			logger.warn("TAC ----------> " + trin_tac_length + trin_tac + "(" + (trin_tac_length.length() + trin_tac.length()) + ")");
			String trin_trmid = params.get("TRMID").toString();
		
			params.put("TRANSEQ", trin_transeq);
			params.put("ISSUER", trin_issuer);
			params.put("ACNNO", trin_acnno);
			params.put("ICDTTM", trin_icdttm);
			params.put("ICSEQ", trin_iseqno);
			params.put("ICMEMO", trin_icmemo);
			params.put("TAC_Length", trin_tac_length);
			params.put("TAC", trin_tac);
			params.put("TRMID", trin_trmid);
			*/
		}		
		if(params.containsKey("pkcs7Sign") && "4".equals(params.get("FGTXWAY"))) {
			//壓簽章字串比對
			String jsondc = StrUtils.trim(params.get("JSON:DC").toString());			
			String __mac = StrUtils.trim(params.get("pkcs7Sign").toString());
			params.put("__SIGN", __mac);			
			log.warn(ESAPIUtil.vaildLog("JSONDC:"+jsondc));
			log.warn(ESAPIUtil.vaildLog("pkcs7Sign:"+__mac));
			
			String CertInfo = VaCheck.doVAVerify(__mac, jsondc, "VARule1");
			int VerfyICSrtn = -1;
			if(CertInfo.length()==0) {
				MSGCODE="Z089";
				CheckServerHealth checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
				checkserverhealth.sendZ300Notice("Z089");
				log.warn("NB32_1.java doVAVerify fail: cert length zero");
				throw TopMessageException.create(MSGCODE);
			}
			else
			{
				VerfyICSrtn = VaCheck.doVerfyICS(CertInfo, params.get("CUSIDN").toString(), true);
				if(VerfyICSrtn!=0)
				{
					MSGCODE="Z089";
					CheckServerHealth checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
					checkserverhealth.sendZ300Notice("Z089");
					log.warn("NB32_1.java doVerfyICS fail");
					params.remove("CUSIDN");
					params.remove("UID");											
					throw TopMessageException.create(MSGCODE);					
				}
			}			
		}						
		try {
			params.put("TYPE", "01"); //nb32電文查詢
			telcommResult = nb32Telcomm.query(params);
			log.warn(ESAPIUtil.vaildLog("NB32_1.java nb32 MSGCOD:"+telcommResult.getValueByFieldName("MSGCOD")));
			if(telcommResult.getValueByFieldName("MSGCOD").equals("0000"))
				ACN=telcommResult.getValueByFieldName("ACN");
			if(telcommResult.getValueByFieldName("ACN").length()==11)
			{	
				IMasterValuesHelper bhContact = commonPools.fundUtils.getAdmBhContact(BRHCOD);
				String BRHNAME = bhContact.getValueByFieldName("ADBRANCHNAME",1);
				telcommResult.getFlatValues().put("BRHNAME", BRHNAME);
			}
			else
			{	
				params.remove("CUSIDN");
				params.remove("UID");										
				throw TopMessageException.create("Z617");
			}	
		}
		catch(TopMessageException e) {
			log.warn("NB32_1.java query acn fail Exception:"+e.getMsgcode());
			params.remove("CUSIDN");
			params.remove("UID");									
			throw TopMessageException.create(e.getMsgcode());
		}
		telcommResult.getFlatValues().put("FGTXWAY", params.get("FGTXWAY").toString());
		telcommResult.getFlatValues().put("CUSIDN", params.get("CUSIDN").toString());
		telcommResult.getFlatValues().put("UID", params.get("UID").toString());
		telcommResult.getFlatValues().put("ACN", ACN);
		telcommResult.getFlatValues().put("BRHCOD", BRHCOD);
		return telcommResult;
	}
}
