package fstop.services.impl;

import java.util.Map;

import com.netbank.util.ESAPIUtil;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.services.CommonService;
import fstop.services.batch.CheckServerHealth;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import fstop.va.VaCheck;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class N2042 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());
	

	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 

		//查詢
		log.warn(ESAPIUtil.vaildLog("N2042.java FGTXWAY:"+params.get("FGTXWAY")));
		log.warn(ESAPIUtil.vaildLog("N2042.java CUSIDN:"+params.get("CUSIDN")));
		MVHImpl telcommResult = new MVHImpl();
		String MSGCODE = "";
		//晶片金融卡
		if("2".equals(params.get("FGTXWAY").toString()))
		{
			/*
			Date d = new Date();
			String trin_transeq = "1234";
			String trin_issuer = "05000000";
			String trin_acnno = params.get("ACNNO").toString();
			String trin_icdttm = DateTimeUtils.format("yyyyMMddHHmmss", d);
			logger.warn("ICDTTM ----------> " + trin_icdttm);
			String trin_iseqno = params.get("iSeqNo").toString();
			String trin_icmemo = "";
			String trin_tac_length = "000A";		
			String trin_tac = params.get("TAC").toString();
			logger.warn("TAC ----------> " + trin_tac_length + trin_tac + "(" + (trin_tac_length.length() + trin_tac.length()) + ")");
			String trin_trmid = params.get("TRMID").toString();
		
			params.put("TRANSEQ", trin_transeq);
			params.put("ISSUER", trin_issuer);
			params.put("ACNNO", trin_acnno);
			params.put("ICDTTM", trin_icdttm);
			params.put("ICSEQ", trin_iseqno);
			params.put("ICMEMO", trin_icmemo);
			params.put("TAC_Length", trin_tac_length);
			params.put("TAC", trin_tac);
			params.put("TRMID", trin_trmid);
			*/
			if( params.get("ACNNO").toString().length()==0) {
				MSGCODE = "Z500";
			}
		}		
		if(params.containsKey("pkcs7Sign") && "4".equals(params.get("FGTXWAY"))) {
			//壓簽章字串比對
			String jsondc = StrUtils.trim(params.get("jsondc").toString());			
			String __mac = StrUtils.trim(params.get("pkcs7Sign").toString());
			params.put("__SIGN", __mac);			
			log.warn(ESAPIUtil.vaildLog("JSONDC:"+jsondc));
			log.warn(ESAPIUtil.vaildLog("pkcs7Sign:"+__mac));
			
			String CertInfo = VaCheck.doVAVerify(__mac, jsondc, "VARule1");
			int VerfyICSrtn = -1;
			if(CertInfo.length()==0) {
				MSGCODE="Z089";
				CheckServerHealth checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
				checkserverhealth.sendZ300Notice("Z089");
				telcommResult.getFlatValues().put("MSGCODE", MSGCODE);
				log.warn("N2042.java doVAVerify fail: cert length zero");
				//throw TopMessageException.create(MSGCODE);
			}
			else
			{
				try {
					VerfyICSrtn = VaCheck.doVerfyICS(CertInfo, params.get("CUSIDN").toString(), true);
					if(VerfyICSrtn!=0)
					{
						MSGCODE="Z089";
						CheckServerHealth checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
						checkserverhealth.sendZ300Notice("Z089");
						telcommResult.getFlatValues().put("MSGCODE", MSGCODE);
						log.warn("N2042.java doVerfyICS fail");
						//throw TopMessageException.create(MSGCODE);					
					}
				}
				catch(Exception e){
					log.warn("N203_VA.java doVerfyICS fail Exception:"+e.getMessage());
				}
			}			
		}						
		telcommResult.getFlatValues().put("ERRCODE", MSGCODE);	
		telcommResult.getFlatValues().put("FGTXWAY", params.get("FGTXWAY").toString());
		telcommResult.getFlatValues().put("CUSIDN", params.get("CUSIDN").toString());
		telcommResult.getFlatValues().put("UID", params.get("UID").toString());	
		telcommResult.getFlatValues().put("NAME", params.get("NAME").toString());
		telcommResult.getFlatValues().put("BRHCOD", params.get("BRHCOD").toString());
		telcommResult.getFlatValues().put("BRHNAME", params.get("BRHNAME").toString());
		return telcommResult;
	}
}
