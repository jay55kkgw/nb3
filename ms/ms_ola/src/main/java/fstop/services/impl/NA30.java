package fstop.services.impl;

import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class NA30 extends CommonService {

//	private Logger logger = Logger.getLogger(NA30.class);

	@Autowired
    @Qualifier("na30VerifyTelcomm")
	private TelCommExec na30VerifyTelcomm;

//	@Required
//	public void setNa30VerifyTelcomm(TelCommExec na30VerifyTelcomm) {
//		this.na30VerifyTelcomm = na30VerifyTelcomm;
//	}

	@Override
	public MVH doAction(Map params) {

		MVHImpl result = new MVHImpl();

		String occurMsg = "0000";

		log.warn("Put CCBIRTHDATEYY ===================================================");
		String inputbirthyy  = (String)params.get("CCBIRTHDATEYY");
		log.warn("Put CCBIRTHDATEMM ===================================================");
		String inputbirthmm  = (String)params.get("CCBIRTHDATEMM");
		log.warn("Put CCBIRTHDATEDD ===================================================");
		String inputbirthdd  = (String)params.get("CCBIRTHDATEDD");

		String inputbirthday = "";

		if (inputbirthyy != null && inputbirthmm != null && inputbirthdd != null){
			inputbirthday = inputbirthyy.trim() + inputbirthmm.trim() + inputbirthdd.trim();
			Date sd = DateTimeUtils.parse("yyyyMMdd", inputbirthday);
			inputbirthday = DateTimeUtils.getCDateShort(sd);
		}

		//身份驗證
		if("0000".equals(occurMsg)){
			String seq = "00000000" + String.valueOf(Integer.parseInt((String)params.get("icSeq")) - 1);
			seq = seq.substring(seq.length() - 8);

			Hashtable<String, String> _params = new Hashtable<String, String>();
			log.warn("Put CUSIDN ===================================================");
			_params.put("CUSIDN", (String)params.get("CUSIDN"));//20190621 _CUSIDN修改成CUSIDN
			_params.put("TYPE", "01");
			log.warn("Put CARDNUM ===================================================");
			_params.put("CARDNUM", (String)params.get("CARDNUM"));
			log.warn("Put BIRTHDAY ===================================================");
			_params.put("BIRTHDAY", inputbirthday);
			log.warn("Put MOBILE ===================================================");
			_params.put("MOBILE", (String)params.get("MOBILE"));
			log.warn("Put MAIL ===================================================");
			_params.put("MAIL", (String)params.get("MAIL"));
			log.warn("NA30.java Put ICSEQ ==================================================="+seq);
			_params.put("ICSEQ", seq);

			try {
				result = na30VerifyTelcomm.query(_params);
			} catch (TopMessageException e){
				occurMsg = e.getMsgcode();
			}
		}
		
		//錯誤訊息往後帶
		result.getFlatValues().put("occurMsg", occurMsg);
		
		//將頁面訊息帶回result
		Iterator it = params.entrySet().iterator();
		while (it.hasNext()){
			Map.Entry entry = (Map.Entry) it.next();
			result.getFlatValues().put((String)entry.getKey(), (String)entry.getValue());
		}

		return result;
	}
}
