package fstop.services.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class NB32_2 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());
	
	@Autowired
	@Qualifier("nb32Telcomm")
	private TelCommExec nb32Telcomm;

//	@Required
//	public void setNb32Telcomm(TelCommExec telcomm) {
//		nb32Telcomm = telcomm;
//	}
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 

		//查詢
		log.warn(ESAPIUtil.vaildLog("NB32_2.java ACN:"+params.get("ACN")));
		log.warn(ESAPIUtil.vaildLog("NB32_2.java CUSIDN:"+params.get("CUSIDN")));
		MVHImpl telcommResult = new MVHImpl();
		String MSGCODE = "";
		String ACN = params.get("ACN").toString();
			
		try {
			params.put("TYPE", "02"); //nb32電文_啟用晶片金融卡交易
			telcommResult = nb32Telcomm.query(params);
			log.warn(ESAPIUtil.vaildLog("NB32_2.java nb32 MSGCOD:"+telcommResult.getValueByFieldName("MSGCOD")));		

		}
		catch(TopMessageException e) {
			log.warn("NB32_2.java query acn fail Exception:"+e.getMsgcode());
			throw TopMessageException.create(e.getMsgcode());
		}
		return telcommResult;
	}
}
