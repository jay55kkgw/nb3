package fstop.services.impl;

import java.util.Date;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.notifier.NotifyAngent;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.TXNUSER;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Owner
 */
@Slf4j
public class NA03 extends CommonService  {
	@Autowired
	private TxnUserDao txnUserDao;
	@Autowired
	@Qualifier("n930Telcomm")
	private TelCommExec n930Telcomm; //用戶電子郵箱位址變更
	@Autowired
	@Qualifier("n931Telcomm")
	private TelCommExec n931Telcomm; //用戶電子對帳單變更

	/**
	 * 將上傳及INSERT TXNCARDAPPLY的部份拿到NB3
	 */
	@Override
	public MVH doAction(Map _params) { 
		MVHImpl result = new MVHImpl();
		Map<String, String> params = _params;

		Date d = new Date();
		
		params.put("VERSION", "NB08");//因前面VERSION已被覆寫版號，打電文前需把VERSION還原成NB08
		params.put("DATE", DateTimeUtils.getCDateShort(new Date()));
		params.put("TIME", DateTimeUtils.format("HHmmss", new Date()));
		params.put("CUSIDN", (String)params.get("CPRIMID"));
		params.put("E_MAIL_ADR", (String)params.get("CPRIMEMAIL"));
		params.put("FLAG", "02");  //更新需用02打中心更心
		String dateTime = DateTimeUtils.getCDateShort(d) + DateTimeUtils.getTimeShort(d);
		String occurMsg="";
		MVHImpl result1 = new MVHImpl();
		try{
			result1 = n930Telcomm.query(params);
			occurMsg=result1.getValueByFieldName("MSGCOD").trim();
			log.warn(ESAPIUtil.vaildLog("NA03.java n930telcomm msgcod:"+occurMsg));
			if(occurMsg.equals("0000") || occurMsg.equals("")){
				TXNUSER txnUser = txnUserDao.findById((String)params.get("CPRIMID"));
				if(txnUser != null){
					txnUser.setDPMYEMAIL((String)params.get("CPRIMEMAIL"));
					txnUser.setEMAILDATE(dateTime);
				}
				if(((String)params.get("VARSTR3")).equals("11")){	
					params.put("TYPE", "01");
					params.put("ITEM", "01");
					params.put("CARDAP", "Y");
					try{
						result1 = n931Telcomm.query(params);
						occurMsg=result1.getValueByFieldName("MSGCOD").trim();
						log.warn("NA03.java n931telcomm msgcod:"+ESAPIUtil.vaildLog(occurMsg));
						if(occurMsg.equals("0000") || occurMsg.equals("")){
							if(txnUser != null){
								txnUser.setDPCARDBILL("Y");
								txnUser.setCARDBILLDATE(dateTime);
							}				
						}
					}catch (TopMessageException e) {
						occurMsg = e.getMsgcode();
						log.warn("NA03.java n931telcomm exception msgcod:"+occurMsg);
					}
				}
				if(txnUser != null){
					txnUserDao.save(txnUser);
				}
				//處理完成
				result.getFlatValues().put("MSGCOD","0000");
			}
		}catch (TopMessageException e) {
			occurMsg = e.getMsgcode();
			log.warn("NA03.java n930telcomm exception msgcod:"+occurMsg);
		}
		try{
			//發送email
			String dateday = DateTimeUtils.format("yyyyMMdd", d);
			String year = dateday.substring(0, 4);
			String month = dateday.substring(4, 6);
			String day= dateday.substring(6, 8);
			//email內容要顯示用
			params.put("YEAR", year);					//年
			params.put("MONTH", month);					//月
			params.put("DAY", day);						//日
			params.put("CN1DESC", params.get("CNDESC"));				//信用卡名稱
			params.put("RCVNO", params.get("RCVNO"));		//案件編號
			if(StrUtils.isNotEmpty(params.get("CPRIMEMAIL"))) {//電子信箱
				boolean isSendSuccess = NotifyAngent.sendNotice("NA03", params,params.get("CPRIMEMAIL"));
				if(!isSendSuccess) {
					result.getFlatValues().put("__MAILERR", "發送 Email 失敗.");
				}
			}
		}
		catch(Exception e){
			log.error("doAction error >> {}",e);
		}
		return result;		
	}
}