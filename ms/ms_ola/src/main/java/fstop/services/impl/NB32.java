package fstop.services.impl;

import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.rest.model.CommonPools;
import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.services.CommonService;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class NB32 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());
	

	@Autowired
	private CommonPools commonPools;
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 

		//查詢

		log.warn(ESAPIUtil.vaildLog("NB32.java CUSIDN:"+params.get("CUSIDN").toString()));
		MVHImpl telcommResult = new MVHImpl();
		try {
			String ACNTYPE = commonPools.applyUtils.getACNTYPEByCusidn(params.get("CUSIDN").toString());
			String ACN = commonPools.applyUtils.getACNByCusidn(params.get("CUSIDN").toString());
			String BRHCOD = commonPools.applyUtils.getBRHCODByCusidn(params.get("CUSIDN").toString());
			log.warn(ESAPIUtil.vaildLog("NB32.java BRHCOD:"+BRHCOD));
			telcommResult.getFlatValues().put("ACNTYPE", ACNTYPE);
			telcommResult.getFlatValues().put("CUSIDN", params.get("CUSIDN").toString());
			telcommResult.getFlatValues().put("UID", params.get("CUSIDN").toString());
			telcommResult.getFlatValues().put("ACN", ACN);
			telcommResult.getFlatValues().put("BRHCOD", BRHCOD);
			if(ACNTYPE.length()!=2)
			{	
				params.remove("CUSIDN");
				params.remove("UID");						
				throw TopMessageException.create("Z617");
			}
			log.warn(ESAPIUtil.vaildLog("NB32.java ACNTYPE:"+ACNTYPE));
		}
		catch(Exception e){
			log.warn("NB32.java get acntype Exception:"+e.getMessage());
			params.remove("CUSIDN");
			params.remove("UID");					
			throw TopMessageException.create("Z617");
		}
		return telcommResult;
	}


}
