package fstop.services.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.rest.model.CommonPools;
import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.TxnAcnApplyDao;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Owner
 * 
 */
@Slf4j
public class NB31_1 extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("nb31Telcomm")
	private TelCommExec nb31Telcomm;

	@Autowired
	private TxnAcnApplyDao txnAcnApplyDao;
	
	@Autowired
	private CommonPools commonPools;

//	@Required
//	public void setNb31Telcomm(TelCommExec nb31Telcomm) {
//		this.nb31Telcomm = nb31Telcomm;
//	}

	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) {
		MVHImpl telcommResult = new MVHImpl();
		String MSGCODE = "";
		String ERRCODE = "";
		String ACN = params.get("ACN").toString() == null ? "" : params
				.get("ACN").toString();
		String CUSIDN = params.get("CUSIDN").toString() == null ? ""
				: params.get("CUSIDN").toString();
		String NAATAPPLY = params.get("NAATAPPLY").toString() == null ? ""
				: params.get("NAATAPPLY").toString();
		String CChargeApply = params.get("CChargeApply").toString() == null ? ""
				: params.get("CChargeApply").toString();
		String CROSSAPPLY = params.get("CROSSAPPLY").toString() == null ? ""
				: params.get("CROSSAPPLY").toString();
		String SEND = params.get("SEND").toString() == null ? "" : params
				.get("SEND").toString();
		String CRDTYP = params.get("CRDTYP").toString() == null ? "" : params
				.get("CRDTYP").toString();
		String LMT = params.get("LMT").toString() == null ? "" : params
				.get("LMT").toString();
		//XML已拿掉
//		String POSTCOD = params.get("POSTCOD").toString() == null ? "" : params
//				.get("POSTCOD").toString();
//		String MILADR = params.get("MILADR").toString() == null ? "" : params
//				.get("MILADR").toString();
		try {		
			if (ACN.length() == 0 || CUSIDN.length() == 0
					|| NAATAPPLY.length() == 0 || CChargeApply.length() == 0
					|| CROSSAPPLY.length() == 0 || SEND.length() == 0) {
				throw TopMessageException.create("ZX99");
			}

			params.put("TROUFG", params.get("NAATAPPLY").toString());
			params.put("TROSUM", params.get("CChargeApply").toString());
			params.put("CROSS", params.get("CROSSAPPLY").toString());
			params.put("SNDFLG", params.get("SEND").toString());
//			params.put("POSTCOD", params.get("POSTCOD").toString());
//			params.put("CTTADR", params.get("MILADR").toString());
            params.put("TRNTYP", "02");
			telcommResult = nb31Telcomm.query(params);
		} catch (TopMessageException e) {
			ERRCODE = e.getMsgcode();
			log.warn("NB31_1.java NB31 ERRCODE:" + ERRCODE);
			telcommResult.getFlatValues().put("errMsg", ERRCODE);
			throw TopMessageException.create(e.getMsgcode());
		}
		try {
			int rtn = txnAcnApplyDao.updateApplyData("Y", NAATAPPLY, CChargeApply, CROSSAPPLY, CRDTYP, LMT,CUSIDN);
		} catch (Exception e) {
			log.warn(ESAPIUtil.vaildLog("NB31_1.java update txnacnapply fail exception:"
					+ e.getStackTrace()+","+"Y,"+ NAATAPPLY+","+CChargeApply+","+CROSSAPPLY+","+CRDTYP+","+LMT+","+
					CUSIDN));
			throw TopMessageException.create("ZX99");
		}
		String BRHCOD = commonPools.applyUtils.getBRHCODByCusidn(params.get(
				"CUSIDN").toString());
		MSGCODE = telcommResult.getValueByFieldName("MSGCOD");
		telcommResult.getFlatValues().put("BRHCOD", BRHCOD);
		telcommResult.getFlatValues()
				.put("SEND", params.get("SEND").toString());
		telcommResult.getFlatValues().put("occurMsg", MSGCODE);
		return telcommResult;
	}
}
