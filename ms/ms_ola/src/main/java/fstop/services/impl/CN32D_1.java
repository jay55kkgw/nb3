package fstop.services.impl;

import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CN32D_1 extends CommonService {

//	private Logger logger = Logger.getLogger(CN32D_1.class);

	@Autowired
	@Qualifier("cn32Telcomm")
	private TelCommExec cn32Telcomm;

//	@Required
//	public void setCn32Telcomm(TelCommExec cn32Telcomm) {
//		this.cn32Telcomm = cn32Telcomm;
//	}

	@Override
	public MVH doAction(Map params1) {
		Map<String, String> params = params1;

		MVHImpl result = new MVHImpl();

		String occurMsg = "0000";
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);	
		params.put("ACN", StringUtils.leftPad(params.get("ACN"), 16, '0'));
		params.put("BNKCOD", (params.get("ISSUER").toString()).substring(0,3));
		params.put("CMPASSWORD", params.get("CMPW"));
		try {
			result = cn32Telcomm.query(params);
		} catch (TopMessageException e){
			occurMsg = e.getMsgcode();
		}
		
		//錯誤訊息往後帶
		result.getFlatValues().put("occurMsg", occurMsg);
		//帳號往後帶
		result.getFlatValues().put("CUSIDN", params.get("CUSIDN").toString());
		result.getFlatValues().put("ACN", params.get("ACN").toString());
		result.getFlatValues().put("CMPW", result.getValueByFieldName("CMPASSWORD"));
		//將頁面訊息帶回result
		Iterator it = params.entrySet().iterator();
		while (it.hasNext()){
			Map.Entry entry = (Map.Entry) it.next();
			result.getFlatValues().put((String)entry.getKey(), (String)entry.getValue());
		}

		return result;
	}
}
