package fstop.services.impl;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.IPUtils;
import fstop.util.JSONUtils;
import fstop.ws.HostPARK003JsonRequest;
import fstop.ws.HostPARK003JsonResponse;
import lombok.extern.slf4j.Slf4j;
import fstop.ws.HostPARK003JsonRecord;


@Slf4j
@Transactional
public class P003 extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("p003Telcomm")
	private TelCommExec p003Telcomm;
	
    @Value("${P003BaseUrl:http://10.16.22.231:8084/ZoneParkFees/ParkfeesServlet}")
    private String restBaseUrl;
    
//	@Required
//	public void setP003Telcomm(TelCommExec telCommExec) {
//		this.p003Telcomm = telCommExec;
//	}
	
	@Override
	public MVH doAction(Map params) {	
		
		Map<String, String> _params = params;
		
		log.info(ESAPIUtil.vaildLog("_params:"+_params));
		log.info(ESAPIUtil.vaildLog("TxnCode:"+params.get("TxnCode")));
		log.info(ESAPIUtil.vaildLog("CustomerId:"+params.get("CustomerId")));
		log.info(ESAPIUtil.vaildLog("ZoneCode:"+params.get("ZoneCode")));
		log.info(ESAPIUtil.vaildLog("cardid:"+((String)params.get("cardid")).trim()));
		log.info(ESAPIUtil.vaildLog("params:"+params));
		MVHImpl telcommResult = new MVHImpl();
		MVHImpl cardata = new MVHImpl();
//		TelcommResult helper = p003Telcomm.query(params);
//		helper.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));

		try {
			Gson gson = new Gson();
			//測試 暫時註解
			
			HostPARK003JsonRequest p003 = new HostPARK003JsonRequest();
			p003.setTxncode((String)params.get("TxnCode"));
			p003.setCustomerid((String)params.get("CustomerId"));
			p003.setZonecode((String)params.get("ZoneCode"));
			p003.setCarid(((String)params.get("cardid")).trim());
			p003.setAccount("");
			
			log.info(ESAPIUtil.vaildLog("P003.java gson JSON before:"+gson.toJson(p003)));
			log.info(ESAPIUtil.vaildLog("P003.java JSON before:"+JSONUtils.toJson(p003)));
			String localIP = IPUtils.getLocalIp();//取得IP位址
	        String defaultBaseUrl ="";	        
	        //defaultBaseUrl 改為使用 環境變數
//	        if(localIP.equals("10.16.22.17")){//測試環境
//	        	defaultBaseUrl = "http://10.16.22.231:8084/ZoneParkFees/ParkfeesServlet";
//	        }else{							  //營運環境	
//	        	defaultBaseUrl = "https://10.16.21.68:11122/ZoneParkFees/ParkfeesServlet";
//	        }
	        defaultBaseUrl = restBaseUrl;
	        log.info("P003.java defaultBaseUrl:"+defaultBaseUrl);
			URL obj = new URL(defaultBaseUrl);
	        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
	        con.setRequestMethod("POST");
	        con.setRequestProperty("Content-Type", "application/xml");
	        con.setDoOutput(true);
	        con.setConnectTimeout(5 * 1000);
	        con.setReadTimeout(60 * 1000);
	        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
	        log.info(ESAPIUtil.vaildLog("P003.java JSON After:"+JSONUtils.toJson(p003).getBytes("UTF8")));
	        wr.write(JSONUtils.toJson(p003).getBytes("UTF8"));	        
	        wr.flush();
	        wr.close();
	        
	        int responseCode = con.getResponseCode();
	        log.info("responseCode:" + responseCode);
	        
	        StringBuffer response = new StringBuffer();
	        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
	        String inputLine;
	        while ((inputLine = in.readLine()) != null) {
	            response.append(inputLine);
	        }
	        in.close();
	        
	        log.info(ESAPIUtil.vaildLog("response Json:" + response.toString()));
//			String response = "{\"respcode\":\"R000\",\"respstring\":\"\",\"customerid\":\"H120036595\",\"email\":\"139188@mail.tbb.com.tw\",\"phone\":\"0912345678\",\"name\":\"\",\"count\":\"1\",\"cars\":[{\"zonecode\":\"TPY\",\"carid\":\"1234-5678\",\"carkind\":\"C\",\"startdate\":\"20190530\",\"account\":\"\",\"source\":\"\",\"status\":\"CA\"}]}";
	        HostPARK003JsonResponse r = (HostPARK003JsonResponse)gson.fromJson(response.toString(), HostPARK003JsonResponse.class);
	        log.info(ESAPIUtil.vaildLog("r.getRespcode():" + r.getRespcode()));
	        log.info(ESAPIUtil.vaildLog("r.getRespstring():" + r.getRespstring()));
	        log.info(ESAPIUtil.vaildLog("r.getCustomerid():" + r.getCustomerid()));
	        log.info(ESAPIUtil.vaildLog("r.getEmail():" + r.getEmail()));
	        log.info(ESAPIUtil.vaildLog("r.getPhone():" + r.getPhone()));
	        log.info(ESAPIUtil.vaildLog("r.getName():" + r.getName()));
	        log.info(ESAPIUtil.vaildLog("r.getCount():" + r.getCount()));	        
	        log.info(ESAPIUtil.vaildLog("size:" + r.getCars().size()));
	        
	        if(r.getCars().size()==0)//無停車費資料
	        {	
	        	log.warn("query not car data");					
	        	telcommResult.getFlatValues().put("Count","0");
	        	throw TopMessageException.create("ENRD");
	        }
	        telcommResult.getFlatValues().put("Respcode", r.getRespcode());
	        telcommResult.getFlatValues().put("Customerid", r.getCustomerid());
	        telcommResult.getFlatValues().put("Email", r.getEmail());
	        telcommResult.getFlatValues().put("Phone", r.getPhone());
	        telcommResult.getFlatValues().put("Name", r.getName());
	        telcommResult.getFlatValues().put("Count",r.getCount());
	        
	        //修改資料放入方式
	        List<HostPARK003JsonRecord> cars = r.getCars();
	        for (HostPARK003JsonRecord car: cars) {
	        	log.info("Zonecode:" + car.getZonecode());
	        	log.info("Carid:" + car.getCarid());
	        	log.info("Carkind:" + car.getCarkind());	        	
	        	log.info("Startdate:" + car.getStartdate());
	        	log.info("Account:" + car.getAccount());
	        	log.info("source:" + car.getSource());
	        	log.info("Status:" + car.getStatus());
	        	
	        	Row carRow = new Row(new HashMap<String, String>());
	        	carRow.getValues().put("ZoneCode", car.getZonecode());
	        	carRow.getValues().put("CarId", car.getCarid());
	        	carRow.getValues().put("CarKind", car.getCarkind());
	        	carRow.getValues().put("RegistDate", car.getStartdate());
	        	carRow.getValues().put("Account", car.getAccount());
	        	carRow.getValues().put("source", car.getSource());
	        	carRow.getValues().put("Status", car.getStatus());
	        	telcommResult.getOccurs().addRow(carRow);
	        	cardata.getOccurs().addRow(carRow);
	        }
	        telcommResult.addTable(cardata,"停車費資料");
	        telcommResult.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		}
		catch (TopMessageException e) {
			log.info("TopMessageException error:"+e.getMsgcode());
			telcommResult.getFlatValues().put("ERRCODE", e.getMsgcode());			
		}catch(Exception e){
			log.info("ERROR:"+e.getMessage());
			telcommResult.getFlatValues().put("ERRCODE", "ZX99");
		}
        
		
		
		return telcommResult;
	}
}
