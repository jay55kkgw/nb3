package fstop.services.impl;

import java.util.Date;
import java.util.LinkedList;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.rest.model.CommonPools;
import com.netbank.rest.model.FieldTranslator;
import com.netbank.rest.model.JSPUtils;
import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.TxnAcnApplyDao;
import fstop.services.CommonService;
import fstop.services.batch.CheckServerHealth;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import fstop.va.VaCheck;
import fstop.ws.AMLWebServiceTemplate;
import fstop.ws.aml.bean.AML001RQ;
import fstop.ws.aml.bean.AML001RQ.SearchNamesSoap;
import fstop.ws.aml.bean.AML001RQ.SearchNamesSoap.Branch;
import fstop.ws.aml.bean.AML001RQ.SearchNamesSoap.DateOfBirth;
import fstop.ws.aml.bean.AML001RQ.SearchNamesSoap.Names;
import fstop.ws.aml.client.SearchResponseSoap;
import lombok.extern.slf4j.Slf4j;
/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class N104_2 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("n104Telcomm")
	private TelCommExec n104Telcomm;
	@Autowired
	@Qualifier("a105Telcomm")
	private TelCommExec a105Telcomm;
	@Autowired
	private TxnAcnApplyDao txnAcnApplyDao;
	@Autowired
	private CommonPools commonPools;

	@Autowired
	private AMLWebServiceTemplate amlWebServiceTemplate;
	
//	@Required
//	public void setN104Telcomm(TelCommExec telcomm) {
//		n104Telcomm = telcomm;
//	}
//	
//	@Required
//	public void setA105Telcomm(TelCommExec telcomm) {
//		a105Telcomm = telcomm;
//	}
	
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 

		//查詢
		log.warn(ESAPIUtil.vaildLog("N104_2.java FGTXWAY:"+params.get("FGTXWAY")));
		log.warn(ESAPIUtil.vaildLog("N104_2.java CUSIDN:"+params.get("CUSIDN")));
		MVHImpl telcommResult = new MVHImpl();
		MVHImpl telcommResult1 = new MVHImpl();
		String MSGCODE = "";
		String ERRCODE = "";
		//String jsondata = JSONUtils.map2json(params).replace("\"", "'");
		params.put("ADOPID", "N104");
		if(amlWebServiceTemplate.callAML(params)) {
			telcommResult.getFlatValues().put("occurMsg", "Z616");
		    return telcommResult;
		}
		//晶片金融卡
		if("2".equals(params.get("FGTXWAY").toString()))
		{
			Date d = new Date();
			String trin_transeq = "1234";
			String trin_issuer = "05000000";
			String trin_acnno = params.get("ACNNO").toString();
			String trin_icdttm = DateTimeUtils.format("yyyyMMddHHmmss", d);
			log.warn("ICDTTM ----------> " + trin_icdttm);
			String trin_iseqno = params.get("iSeqNo").toString();
			String trin_icmemo = "";
			String trin_tac_length = "000A";		
			String trin_tac = params.get("TAC").toString();
			log.warn(ESAPIUtil.vaildLog("TAC ----------> " + trin_tac_length + trin_tac + "(" + (trin_tac_length.length() + trin_tac.length()) + ")"));
			String trin_trmid = params.get("TRMID").toString();
			params.put("TRANSEQ", trin_transeq);
			params.put("ISSUER", trin_issuer);
			params.put("ACNNO", trin_acnno);
			params.put("ICDTTM", trin_icdttm);
			params.put("ICSEQ", trin_iseqno);
			params.put("ICMEMO", trin_icmemo);
			params.put("TAC_Length", trin_tac_length);
			params.put("TAC", trin_tac);
			params.put("TRMID", trin_trmid);
		}		
		if(params.containsKey("pkcs7Sign") && "4".equals(params.get("FGTXWAY"))) {
			//壓簽章字串比對
			String jsondc = StrUtils.trim(params.get("jsondc").toString());			
			String __mac = StrUtils.trim(params.get("pkcs7Sign").toString());
			params.put("__SIGN", __mac);			
			log.warn(ESAPIUtil.vaildLog("JSONDC:"+jsondc));
			log.warn(ESAPIUtil.vaildLog("pkcs7Sign:"+__mac));
			
			String CertInfo = VaCheck.doVAVerify(__mac, jsondc, "VARule1");
			int VerfyICSrtn = -1;
			if(CertInfo.length()==0) {
				MSGCODE="Z089";
				CheckServerHealth checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
				checkserverhealth.sendZ300Notice("Z089");
				log.warn("N104_2.java doVAVerify fail: cert length zero");
			}
			else
			{
				VerfyICSrtn = VaCheck.doVerfyICS(CertInfo, params.get("CUSIDN").toString(), true);
				if(VerfyICSrtn!=0)
				{
					MSGCODE="Z089";
					CheckServerHealth checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
					checkserverhealth.sendZ300Notice("Z089");
					log.warn("N104_2.java doVerfyICS fail");					
				}
			}
			
		}						
		try {
			if(!MSGCODE.equals("Z089"))
			{	
				String NAME=JSPUtils.convertFullorHalf((String)params.get("NAME"), 1);
				String PMTADR=JSPUtils.convertFullorHalf((String)params.get("PMTADR"), 1);
				String CTTADR=JSPUtils.convertFullorHalf((String)params.get("CTTADR"), 1);
				params.put("NAME", NAME);
				params.put("PMTADR", PMTADR); 
				params.put("CTTADR", CTTADR);
				params.put("MAINFUND", FieldTranslator.transfer("N203","MAINFUND1", params.get("MAINFUND").toString()));
				params.put("TYPE", "U"); //更新
				
				telcommResult = n104Telcomm.query(params);
				MSGCODE = telcommResult.getValueByFieldName("ABEND");
				
				if(MSGCODE.equals("0000") || MSGCODE.trim().equals(""))
				{
					 try {
						 int rtn=commonPools.applyUtils.updaterec(params.get("CUSIDN").toString(), params.get("BRHCOD").toString(), params.get("PURPOSE").toString(),params.get("PREASON").toString(),params.get("BDEALING").toString(),params.get("BREASON").toString(),params.get("IP").toString(),params.get("EMPLOYER").toString());
						 log.warn(ESAPIUtil.vaildLog("N104_2.java update txnacnapply record:CUSIDN/BRHCOD-->"+params.get("CUSIDN").toString()+"/"+params.get("BRHCOD").toString() +" rtn:"+rtn));
					 }
					 catch(Exception e) {
						 log.warn("N104_2.java update txnacnapply fail exception:"+e.getMessage());
					 }
					 try {
		                   params.put("TYPE", "U");
		                   params.put("CUSIDN", params.get("CUSIDN").toString());
		                   params.put("SAMT", params.get("SMONEY").toString());
		                   params.put("PURPOSE", FieldTranslator.transfer("N203","PURPOSE1", params.get("PURPOSE").toString()));
		                   telcommResult1 = a105Telcomm.query(params);
		             } catch(TopMessageException e) {
			             ERRCODE=e.getMsgcode();
			             log.warn("N104_2.java A105 ERRCODE:"+ERRCODE);
						 telcommResult.getFlatValues().put("errMsg", ERRCODE);
		             }
				}
				
			}
		}
		catch(TopMessageException e) {
			MSGCODE=e.getMsgcode();
			log.warn("N104_2.java N104 MSGCODE:"+MSGCODE);
		}
		telcommResult.getFlatValues().put("BRHCOD", params.get("BRHCOD").toString());
		telcommResult.getFlatValues().put("occurMsg", MSGCODE);
		params.remove("CUSIDN");
		params.remove("UID");		
		return telcommResult;
	}
	
	public boolean getAmlmsg(Map reqParam) {
		try{
			String name = (String)reqParam.get("NAME");
			String BRTHDY = (String)reqParam.get("BIRTHDAY");
			int yearnum = Integer.parseInt(BRTHDY.substring(0,3))+1911;
			String year = String.valueOf(yearnum);
			String month = BRTHDY.substring(3,5);
			String day = BRTHDY.substring(5,7);
			
			
			AML001RQ rq  = new AML001RQ();
			SearchNamesSoap  searchNamesSoap = new SearchNamesSoap();
			Names names = new Names();
			LinkedList list = new LinkedList<String>();
			list.add(name);
			names.setName(list);
			Branch branch = new Branch();
			branch.setBranchId((String)reqParam.get("BRHCOD"));
			branch.setBusinessUnit("H57");
			branch.setSourceSystem("NNB-W100089655");
			DateOfBirth dateOfBirth = new DateOfBirth();
			dateOfBirth.setYear(year);
			dateOfBirth.setMonth(month);
			dateOfBirth.setDay(day);
			searchNamesSoap.setNames(names);
			searchNamesSoap.setBranch(branch);
			searchNamesSoap.setDateOfBirth(dateOfBirth);
			rq.setMSGNAME("AML001");
			rq.setAPP("XML");
			rq.setCLIENTIP("::1");
			rq.setTYPE("01");
			rq.setSearchNamesSoap(searchNamesSoap);
			
			SearchResponseSoap rs = amlWebServiceTemplate.sendAndReceive (rq);
			return rs.isHit();
		}catch(Exception e) {
			log.debug(e.toString());
		}
		return true;
		
	} 
}
