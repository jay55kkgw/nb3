package fstop.services.impl;

import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;

import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.notifier.NotifyAngent;
import fstop.orm.dao.TxnCardLogDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.TXNCARDLOG;
import fstop.orm.po.TXNUSER;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class NA40_1 extends CommonService {

//	private Logger logger = Logger.getLogger(NA40_1.class);

	@Autowired
    @Qualifier("na40Telcomm")
	private TelCommExec na40Telcomm;

	@Autowired
	private TxnUserDao txnUserDao;

	@Autowired
	private TxnCardLogDao txnCardLogDao;

//	@Required
//	public void setTxnCardLogDao(TxnCardLogDao txnCardLogDao) {
//		this.txnCardLogDao = txnCardLogDao;
//	}
//
//	@Required
//	public void setTxnUserDao(TxnUserDao txnUserDao) {
//		this.txnUserDao = txnUserDao;
//	}
//
//	@Required
//	public void setNa40Telcomm(TelCommExec na40Telcomm) {
//		this.na40Telcomm = na40Telcomm;
//	}

	@Override
	public MVH doAction(Map params) {

		MVHImpl result = new MVHImpl();
		Date d = new Date();

		String occurMsg = "0000";

		//先檢查使用者存不存在
		TXNUSER user = txnUserDao.findById((String)params.get("_CUSIDN"));

		//前一次申請之Email
		String pre_mail = user.getDPMYEMAIL();
		//這一次申請之Email
		String mail  = (String)params.get("MAIL");

		//錯誤次數達3次時，踢回不能申請。
		if("0000".equals(occurMsg) && user.getASKERRTIMES() >= 3) {
			occurMsg = "Z603";
		}
		
		//確認申請
		if("0000".equals(occurMsg)){
			params.put("TYPE", "02");
			params.put("CUSIDN", (String)params.get("_CUSIDN"));
			try {
				result = na40Telcomm.query(params);
			} catch (TopMessageException e) {
				occurMsg = e.getMsgcode();
			}
		}		

		//記錄log
		TXNCARDLOG cardlog = new TXNCARDLOG();
		cardlog.setLOGDATE(DateTimeUtils.format("yyyyMMdd", d));
		cardlog.setLOGTIME(DateTimeUtils.format("HHmmss", d));
		cardlog.setDPSUERID((String)params.get("_CUSIDN"));
		cardlog.setCARDTYPE("2");
		cardlog.setCRADNO((String)params.get("CARDNUM"));			
		cardlog.setOPERATETYPE("02");			
		cardlog.setREMOTEIP((String)params.get("USERIP"));
		cardlog.setSTATUS("0000".equals(occurMsg)? "Y" : "N");
		cardlog.setADMCODE(occurMsg);
		txnCardLogDao.save(cardlog);

		//更新TXNUSER
		//user.setASKERRTIMES("0000".equals(occurMsg)? 0 : user.getASKERRTIMES() + 1);
		user.setASKERRTIMES("0000".equals(occurMsg)? 0 : user.getASKERRTIMES()); //02 申請電文發生錯誤時，不列入錯誤次數。
		user.setCCARDFLAG("0000".equals(occurMsg)? "1" : user.getCCARDFLAG());
		user.setDPMYEMAIL("0000".equals(occurMsg)? mail: user.getDPMYEMAIL());
		user.setEMAILDATE("0000".equals(occurMsg)? DateTimeUtils.getCDateShort(d) + DateTimeUtils.getTimeShort(d) : user.getEMAILDATE());
		user.setDPCARDBILL(("0000".equals(occurMsg) && "Y".equals(params.get("NOTIFY_CCARDBILL")))?"Y":"N");

		String notify = "";
		if("Y".equals(params.get("NOTIFY_CCARDPAY"))){
			notify = notify + "11";
		}
		if("Y".equals(params.get("NOTIFY_ACTIVE"))){
			if("".equals(StrUtils.trim(notify))){
				notify = notify + "15";
			}else{
				notify = notify + ",15";
			}
		}
		user.setDPNOTIFY("0000".equals(occurMsg) ? notify : user.getDPNOTIFY());
		txnUserDao.save(user);

		//發送Email通知
		if("0000".equals(occurMsg)) {
			Hashtable<String, String> varmail = new Hashtable();
			//首次申請成功發送Email通知申請者
			if("".equals(pre_mail)){
				varmail.put("SUB","信用卡申請網銀成功通知");
				varmail.put("MAILTEXT", getFirstContent(""));
				if(!NotifyAngent.sendNotice("NA40",varmail,mail)){
					log.debug(ESAPIUtil.vaildLog("信用卡申請網銀發送Email失敗.(mail:"+mail+")"));
				}
			}

			//重新申請後,額外發送Email通知原來的申請者
			if(!"".equals(pre_mail)){
				String recivers = "";
				if(pre_mail.equals(mail)){
					recivers = mail;
				} else {
					recivers = pre_mail + "," + mail;
				}
				varmail.put("SUB","信用卡重新申請網銀成功通知");
				varmail.put("MAILTEXT",getContent(""));
				if(!NotifyAngent.sendNotice("NA40",varmail,recivers)){
					log.debug(ESAPIUtil.vaildLog("信用卡申請網銀發送Email失敗.(mail:"+recivers+")"));
				}
			}
		}

		//錯誤訊息往後帶
//		result.getFlatValues().put("occurMsg", occurMsg);
		//為符合目前架構將occurMsg改為msgCode
		result.getFlatValues().put("msgCode", occurMsg);

		//交易時間
		result.getFlatValues().put("CCTXTIME", DateTimeUtils.getDatetime(new Date()));

		//將頁面訊息帶回result
		Iterator it = params.entrySet().iterator();
		while (it.hasNext()){
			Map.Entry entry = (Map.Entry) it.next();
			result.getFlatValues().put((String)entry.getKey(), (String)entry.getValue());
		}

		return result;
	}

	@SuppressWarnings("unused")
	private String getFirstContent(String name){
		StringBuilder sb = new StringBuilder ();
		sb.append("<table cellSpacing=0 cellPadding=0 width=600 border=0 frame=border >").append("\n");
		sb.append("<tbody>").append("\n");
		sb.append("<tr><td align=left>").append(name).append("親愛的客戶，您好：</td></tr></br>").append("\n");
		sb.append("<tr><td align=left>感謝您申請臺灣企銀網路銀行服務！即日起，您將可以更快速、更安全的透過本行網路銀行查詢您信用卡相關資訊，歡迎立即登入網路銀行使用。</td></tr>").append("\n");
		sb.append("<tr><td align=left>如有任何疑問，歡迎致電本行客服專線0800017171，將有專人為您服務。</td></tr></br>").append("\n");
		sb.append("<tr><td align=rigth>臺灣企銀   敬啟</td></tr>").append("\n");
		sb.append("</tbody>").append("\n");
		sb.append("</table>").append("\n");
		return sb.toString();
	}

	@SuppressWarnings("unused")
	private String getContent(String name){

		String dt = DateTimeUtils.getCDateShort(new Date());

		StringBuilder sb = new StringBuilder ();
		sb.append("<table cellSpacing=0 cellPadding=0 width=600 border=0 frame=border >").append("\n");
		sb.append("<tbody>").append("\n");
		sb.append("<tr><td align=left>").append(name).append("親愛的客戶，您好：</td></tr></br>").append("\n");
		sb.append("<tr><td align=left>貴戶已於").append(dt.substring(0, 3)).append("年").append(dt.substring(3, 5)).append("月").append(dt.substring(5,7)).append("日");
		sb.append("重新申請臺灣企銀網路銀行查詢信用卡相關資訊服務，歡迎立即登入網路銀行使用。</td></tr>").append("\n");
		sb.append("<tr><td align=left>如有任何疑問，歡迎致電本行客服專線0800017171，將有專人為您服務。</td></tr>").append("\n");
		sb.append("<tr><td align=rigth>臺灣企銀   敬啟</td></tr>").append("\n");
		sb.append("</tbody>").append("\n");
		sb.append("</table>").append("\n");
		return sb.toString();
	}

}
