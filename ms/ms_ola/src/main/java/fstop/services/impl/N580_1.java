package fstop.services.impl;


import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;

import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j; 

@Slf4j
public class N580_1 extends CommonService{
	private TelCommExec n580Telcomm;

	@Autowired
	@Qualifier("n580Telcomm")
	public void setN580Telcomm(TelCommExec n580Telcomm) {
		this.n580Telcomm = n580Telcomm;
	}
	
	@Override
	public MVH doAction(Map params1) {
		Map<String, String> params = params1;

		MVHImpl result = new MVHImpl();

		String occurMsg = "0000";
		String type = "";
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);	
//		20201008 edit by hugo 根據行動需求調整
		type = params1.get("TYPE") !=null && params1.get("TYPE").toString().length() != 0 ? params1.get("TYPE").toString():"D2";
		params.put("TYPE",type);
		try {
			result = n580Telcomm.query(params);
		} catch (TopMessageException e){
			occurMsg = e.getMsgcode();
		}
		
		//錯誤訊息往後帶
		result.getFlatValues().put("occurMsg", occurMsg);
		//帳號往後帶
		result.getFlatValues().put("CUSIDN", params.get("CUSIDN").toString());
		result.getFlatValues().put("ACN", params.get("ACN").toString());
		//將頁面訊息帶回result
		Iterator it = params.entrySet().iterator();
		while (it.hasNext()){
			Map.Entry entry = (Map.Entry) it.next();
			result.getFlatValues().put((String)entry.getKey(), (String)entry.getValue());
		}

		return result;
	}
}
