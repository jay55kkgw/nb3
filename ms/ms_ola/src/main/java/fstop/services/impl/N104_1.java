package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.rest.model.CommonPools;
import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.services.CommonService;
import fstop.services.batch.CheckServerHealth;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import fstop.va.VaCheck;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class N104_1 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("n104Telcomm")
	private TelCommExec n104Telcomm;

	@Autowired
	@Qualifier("a105Telcomm")
	private TelCommExec a105Telcomm;
	@Autowired
	private CommonPools commonPools;
	
//	@Required
//	public void setN104Telcomm(TelCommExec telcomm) {
//		n104Telcomm = telcomm;
//	}
//	
//	@Required
//	public void setA105Telcomm(TelCommExec telcomm) {
//		a105Telcomm = telcomm;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 

		//查詢
		log.warn(ESAPIUtil.vaildLog("N104_1.java FGTXWAY:"+params.get("FGTXWAY")));
		log.warn(ESAPIUtil.vaildLog("N104_1.java CUSIDN:"+params.get("CUSIDN")));
		MVHImpl telcommResult = new MVHImpl();
		MVHImpl telcommResult1 = new MVHImpl();
		String MSGCODE = "";
		//晶片金融卡
		if("2".equals(params.get("FGTXWAY").toString()))
		{
			
			Date d = new Date();
			String trin_transeq = "1234";
			String trin_issuer = "05000000";
			String trin_acnno = params.get("ACNNO").toString();
			String trin_icdttm = DateTimeUtils.format("yyyyMMddHHmmss", d);
			log.warn("ICDTTM ----------> " + trin_icdttm);
			String trin_iseqno = params.get("iSeqNo").toString();
			String trin_icmemo = "";
			String trin_tac_length = "000A";		
			String trin_tac = params.get("TAC").toString();
			log.warn(ESAPIUtil.vaildLog("TAC ----------> " + trin_tac_length + trin_tac + "(" + (trin_tac_length.length() + trin_tac.length()) + ")"));
			String trin_trmid = params.get("TRMID").toString();
		
			params.put("TRANSEQ", trin_transeq);
			params.put("ISSUER", trin_issuer);
			params.put("ACNNO", trin_acnno);
			params.put("ICDTTM", trin_icdttm);
			params.put("ICSEQ", trin_iseqno);
			params.put("ICMEMO", trin_icmemo);
			params.put("TAC_Length", trin_tac_length);
			params.put("TAC", trin_tac);
			params.put("TRMID", trin_trmid);
			
		}		
		if(params.containsKey("pkcs7Sign") && "4".equals(params.get("FGTXWAY"))) {
			//壓簽章字串比對
			String jsondc = StrUtils.trim(params.get("jsondc").toString());			
			String __mac = StrUtils.trim(params.get("pkcs7Sign").toString());
			params.put("__SIGN", __mac);			
			log.warn(ESAPIUtil.vaildLog("JSONDC:"+jsondc));
			log.warn(ESAPIUtil.vaildLog("pkcs7Sign:"+__mac));
			
			String CertInfo = VaCheck.doVAVerify(__mac, jsondc, "VARule1");
			int VerfyICSrtn = -1;
			if(CertInfo.length()==0) {
				MSGCODE="Z089";
				CheckServerHealth checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
				checkserverhealth.sendZ300Notice("Z089");
				log.warn("N104_1.java doVAVerify fail: cert length zero");
				throw TopMessageException.create(MSGCODE);
			}
			else
			{
				VerfyICSrtn = VaCheck.doVerfyICS(CertInfo, params.get("CUSIDN").toString(), true);
				if(VerfyICSrtn!=0)
				{
					MSGCODE="Z089";
					CheckServerHealth checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
					checkserverhealth.sendZ300Notice("Z089");
					log.warn("N104_1.java doVerfyICS fail");
					throw TopMessageException.create(MSGCODE);					
				}
			}			
		}						
		try {
			if(!MSGCODE.equals("Z089"))
			{	
				params.put("TYPE", "R"); //查詢
				params.put("NAME", "");
				params.put("BIRTHDAY", "");
				params.put("POSTCOD1", "");
				params.put("PMTADR", "");
				params.put("POSTCOD2", "");
				params.put("CTTADR", "");
				params.put("HOMEZIP", "");
				params.put("HOMETEL", "");
				params.put("BUSZIP", "");
				params.put("BUSTEL", "");
				params.put("CELPHONE", "");
				params.put("ARACOD3", "");
				params.put("FAX", "");
				params.put("MAILADDR", "");
				params.put("DEGREE", "");
				params.put("SALARY", "");
				params.put("MAINFUND", "");
				params.put("MARRY", "");
				params.put("CHILD", "");
				params.put("CAREER1", "");
				params.put("CAREER2", "");
				
				telcommResult = n104Telcomm.query(params);
			}
			else
			{	
				log.warn("N104_1.java VA fail:"+MSGCODE);
				throw TopMessageException.create(MSGCODE);
			}	
		}
		catch(TopMessageException e) {
			params.remove("CUSIDN");
			params.remove("UID");	
			throw TopMessageException.create(e.getMsgcode());
		}
		String PURPOSE = commonPools.applyUtils.getPURPOSEByCusidn(params.get("CUSIDN").toString());
		String PREASON = commonPools.applyUtils.getPREASONByCusidn(params.get("CUSIDN").toString());
		String BDEALING = commonPools.applyUtils.getBDEALINGByCusidn(params.get("CUSIDN").toString());
		String BREASON = commonPools.applyUtils.getBREASONByCusidn(params.get("CUSIDN").toString());
		String BRHCOD = commonPools.applyUtils.getBRHCODByCusidn(params.get("CUSIDN").toString());
		String EMPLOYER = commonPools.applyUtils.getEMPLOYERByCusidn(params.get("CUSIDN").toString());
		telcommResult.getFlatValues().put("FGTXWAY", params.get("FGTXWAY").toString());
		telcommResult.getFlatValues().put("CUSIDN", params.get("CUSIDN").toString());
		telcommResult.getFlatValues().put("UID", params.get("UID").toString());
		telcommResult.getFlatValues().put("PURPOSE", PURPOSE);
		telcommResult.getFlatValues().put("PREASON", PREASON);
		telcommResult.getFlatValues().put("BDEALING", BDEALING);
		telcommResult.getFlatValues().put("BREASON", BREASON);
		telcommResult.getFlatValues().put("BRHCOD", BRHCOD);
		telcommResult.getFlatValues().put("EMPLOYER", EMPLOYER);
		//a105 type R 已不對外開放
		/*try{
			String PURPOSEA105 = "";
            if (PURPOSE.equals("A"))
            {   PURPOSEA105 = "1";}
            if (PURPOSE.equals("B"))
            {   PURPOSEA105 = "2";}
            if (PURPOSE.equals("C"))
            {  PURPOSEA105 = "4";}
            if (PURPOSE.equals("D"))
            {   PURPOSEA105 = "5";}
            if (PURPOSE.equals("E"))
            {   PURPOSEA105 = "6";}
			params.put("TYPE", "R");
			params.put("CODE", "A");
			params.put("PURPOSE", PURPOSEA105);
			params.put("CUSIDN", params.get("CUSIDN").toString());
			telcommResult1 = a105Telcomm.query(params);
			telcommResult.getFlatValues().put("SMONEY",telcommResult1.getValueByFieldName("SAMT"));
			telcommResult.getFlatValues().put("RENAME",telcommResult1.getValueByFieldName("RENAME"));
		} catch(TopMessageException e) {
		   telcommResult.getFlatValues().put("SMONEY", "");
		   telcommResult.getFlatValues().put("RENAME","");
		   //throw TopMessageException.create(e.getMsgcode());
		}*/
		return telcommResult;
	}
}
