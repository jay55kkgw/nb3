package fstop.services.impl;

import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CN32D extends CommonService {

//	private Logger logger = Logger.getLogger(CN32D.class);

	@Autowired
	@Qualifier("cn29Telcomm")
	private TelCommExec cn29Telcomm;

//	@Required
//	public void setCn29Telcomm(TelCommExec cn29Telcomm) {
//		this.cn29Telcomm = cn29Telcomm;
//	}

	@Override
	public MVH doAction(Map params1) {
		Map<String, String> params = params1;

		MVHImpl result = new MVHImpl();

		String occurMsg = "0000";
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);			
		params.put("ACN", params.get("ACNNO").toString());
		String trin_transeq = "1234";
		String trin_issuer = "05000000";
		params.put("ACNNO", StringUtils.leftPad(params.get("ACNNO"), 16, '0'));
		String trin_acnno = params.get("ACNNO");
		String trin_icdttm = DateTimeUtils.format("yyyyMMddHHmmss", d);
		log.debug("ICDTTM ----------> " + trin_icdttm);
		String trin_iseqno = params.get("iSeqNo");
		String trin_icmemo = "";
		String trin_tac_length = "000A";		
		String trin_tac = params.get("TAC");
		log.debug(ESAPIUtil.vaildLog("TAC ----------> " + trin_tac_length + trin_tac + "(" + (trin_tac_length.length() + trin_tac.length()) + ")"));
		String trin_trmid = params.get("TRMID");
	
		params.put("TRANSEQ", trin_transeq);
		params.put("ISSUER", trin_issuer);
		params.put("ACNNO", trin_acnno);
		params.put("ICDTTM", trin_icdttm);
		params.put("ICSEQ", trin_iseqno);
		params.put("ICMEMO", trin_icmemo);
		params.put("TAC_Length", trin_tac_length);
		params.put("TAC", trin_tac);
		params.put("TRMID", trin_trmid);		
		params.put("FGTXWAY", "");
			try {
				result = cn29Telcomm.query(params);
			} catch (TopMessageException e){
				occurMsg = e.getMsgcode();
			}
		
		//錯誤訊息往後帶
		result.getFlatValues().put("occurMsg", occurMsg);
		//帳號往後帶
		
		result.getFlatValues().put("CUSIDN", params.get("CUSIDN").toString());
		result.getFlatValues().put("ACN", params.get("ACNNO").toString());
		//將頁面訊息帶回result
		Iterator it = params.entrySet().iterator();
		while (it.hasNext()){
			Map.Entry entry = (Map.Entry) it.next();
			result.getFlatValues().put((String)entry.getKey(), (String)entry.getValue());
		}

		return result;
	}
}
