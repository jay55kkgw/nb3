package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.TxnSmsLogDao;
import fstop.orm.po.TXNSMSLOG;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.SMSOTPProperties;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import ni.otp.Proc00F8;
import ni.otp.Proc00FA;
import ni.otp.Proc00FB;
import tw.com.mitake.sms.MitakeSms;
import tw.com.mitake.sms.result.MitakeSmsSendResult;
import tw.com.mitake.sms.result.MitakeSmsSendResult.SendResult;
import tw.com.tbb.utils.GetConfigProperty;

/**
 * OTP驗證
 */
@Slf4j
public class SmsOtp extends CommonService {

	private final String OTP_TYPE = "NNB";
	private final String OTP_SECOND = "120";
	private final String PHONE_REGEX = "^09\\d{8}$";

	@Autowired
	@Qualifier("n960Telcomm")
	private TelCommExec n960Telcomm;

	@Autowired
	private SMSOTPProperties smsotp;
	
	@Value("${sms_url}")
	private String smsurl;
	
	@Autowired
	private TxnSmsLogDao txnSmsLogDao;
	
	public MVH doAction(Map params) {

		log.warn("===SmsOtp SoftAction service request data action method start ===");
		log.warn(ESAPIUtil.vaildLog("SmsOtp getFuncType  = " + params.get("FuncType").toString()));

		MVHImpl result = new MVHImpl();

		// 缺欄位合理性檢核
		String _FuncType =  StrUtils.isEmpty((String)params.get("FuncType")) ? "" : params.get("FuncType").toString(); // 0:發送 1:驗證
		String _OTP =  StrUtils.isEmpty((String)params.get("OTP")) ? "" : params.get("OTP").toString(); // SMSOTP VALUE
		String msgType = StrUtils.isEmpty((String)params.get("MSGTYPE")) ? "" : params.get("MSGTYPE").toString();
		String CUSIDN = "";
		String ADOPID = "";
		String MOBTEL = "";
		int findcount = 0;
		try {
			CUSIDN = (String) params.get("UID");
			//取得簡訊密碼時，不寫TXNLOG所以改SMSADOPID
			ADOPID = StrUtils.isEmpty((String)params.get("ADOPID"))? (String) params.get("SMSADOPID") : (String) params.get("ADOPID"); // 依交易代號計算次數限制
			log.warn(ESAPIUtil.vaildLog("CUSIDN = " + CUSIDN));
			
			if("0".equals(_FuncType)) {			
				MOBTEL = StrUtils.isEmpty((String)params.get("MOBTEL"))? "" : params.get("MOBTEL").toString();				
				if (!MOBTEL.matches(PHONE_REGEX)) {
					log.error("SmsOtp error =>> 手機號碼錯誤!!");
					throw new Exception("手機號碼錯誤!!");
				}
			}
			// 目前簡訊發送成功次數
			Date d = new Date();
			// 使用者名稱及簽入密碼重設-->簡訊發送次數計算
			log.warn(ESAPIUtil.vaildLog("ADOPID==>" + ADOPID));
			log.warn(ESAPIUtil.vaildLog("MOBTEL==>" + MOBTEL));
//			20200720 by hugo 次數查詢改用此API
			if (StrUtils.isNotEmpty(ADOPID)) {
				findcount = txnSmsLogDao.findTotalCountByInput(CUSIDN, DateTimeUtils.format("yyyyMMdd", d),ADOPID);
				log.warn("{} findcount = {}" ,ESAPIUtil.vaildLog(ADOPID) ,findcount);
			}
			log.warn(ESAPIUtil.vaildLog("ADOPID:" + ADOPID + " COUNTER:" + findcount));

			// 0:發送+發簡訊
			if ("0".equals(_FuncType)) {
				if (findcount >= 3) {
					log.warn("txnsmslog count>=3 throw XQ03");
					throw TopMessageException.create("XQ03");
				} else {
					log.warn("getSMSOTP touch");
					result = (MVHImpl) getSMSOTP(CUSIDN, MOBTEL, ADOPID , msgType);
				}
			} else if ("1".equals(_FuncType)) {
				log.warn("verifySMSOTP touch");
				result = (MVHImpl) verifySMSOTP(CUSIDN, _OTP);
			} 

		} catch (TopMessageException top) {
			result.getFlatValues().put("MSGCOD", top.getMsgcode());
			result.getFlatValues().put("MSGSTR", top.getMsgout());

			log.warn("SmsOtp SoftAction TopMessageException==" + top);
			log.warn("SmsOtp SoftAction MsgCode==" + result.getValueByFieldName("MSGCOD") + " MsgName=="
					+ result.getValueByFieldName("MSGSTR"));
		} catch (Exception e) {
			log.error("SmsOtp SoftAction Exception== {}", e);
			result.getFlatValues().put("MSGCOD", "ZX99");
			result.getFlatValues().put("MSGSTR", "系統異常");
		}

		log.warn("===SmsOtp SoftAction method web service handle end ===");

		return result;
	}

	// 申請軟體式動態密碼
	public MVH registerSMSOTP(String userID) {
		log.warn("===SmsOtp registerSMSOTP method start ===");

		MVHImpl result = new MVHImpl();
		int retCode = 0;
		String otpIp = null;
		String otpPort = null;
		try {
			log.warn("=============上行電文============");
			otpIp = smsotp.getOTPIP();
			otpPort = smsotp.getOTPPORT();
			// 軟體式動態密碼申請
			retCode = new Proc00FA().response2(OTP_TYPE + "-" + userID, otpIp, otpPort, "RSPWD", "NNB");

			if (retCode != 1) {
				result.getFlatValues().put("MSGCOD", Integer.toString(retCode));
				return result;
			}
			// 產生回應訊息
			result.getFlatValues().put("MSGCOD", "0000");
			result.getFlatValues().put("MSGSTR", "OTP密碼註冊成功");
			log.warn("===SmsOtp registerSMSOTP success===");
		} catch (TopMessageException top) {
			TopMessageException top1 = TopMessageException.create(top.getMsgcode());
			log.warn(ESAPIUtil.vaildLog("SmsOtp.action() Msgcode==" + top1.getMsgcode()));
			log.warn(ESAPIUtil.vaildLog("SmsOtp.action() Msgout==" + top1.getMsgout()));
			log.warn("SmsOtp.registerSMSOTP() TopMessageException==" + top);
			log.warn(ESAPIUtil.vaildLog("SmsOtp.registerSMSOTP() Msgcode==" + top1.getMsgcode()));
			log.warn(ESAPIUtil.vaildLog("SmsOtp.registerSMSOTP() Msgout==" + top1.getMsgout()));
		} catch (Exception e) {
			log.warn("SmsOtp.registerSMSOTP() Exception==" + e);
			log.warn("SmsOtp MsgCode == ZX99 ");
			result.getFlatValues().put("MSGCOD", "ZX99");
			result.getFlatValues().put("MSGSTR", "系統異常!");
		}
		log.warn("===SmsOtp registerSMSOTP method end===");

		return result;
	}

	// 取得軟體式動態密碼
	public MVH getSMSOTP(String userID, String mobtel, String adopid ,String msgType) {
		log.warn("===SmsOtp getSMSOTP method start ===");

		MVHImpl result = new MVHImpl();
		String otpIp = null;
		String otpPort = null;
		try {
			log.warn("=============上行電文============");
			log.debug("smsotp>>>{}", smsotp);
			otpIp = smsotp.getOTPIP();
			otpPort = smsotp.getOTPPORT();
			log.debug("otpIp>>>" + otpIp + " otpPort>>>" + otpPort);
			// 軟體式動態密碼申請 (有效期120秒)
			String OTP = "";

			log.warn(ESAPIUtil.vaildLog("申請  registerSMSOTP=====>>>> Proc00FB()==> C1=" + OTP_TYPE + "-" + userID + " C2=" + OTP_SECOND + "C3=" + otpIp + "C4=" + otpPort));
			OTP = new Proc00FB().response2(OTP_TYPE + "-" + userID, OTP_SECOND, otpIp, otpPort, "RSPWD", "NNB");

			if (OTP.equals("E6102")) { // 使用者帳號錯誤(尚未註冊)
				log.warn(ESAPIUtil.vaildLog("Aotu registerSMSOTP=====>>>>" + userID));
				MVH cmMsg = registerSMSOTP(userID);
				if (cmMsg.getValueByFieldName("MSGCOD").equals("0000")) {
					log.warn(ESAPIUtil.vaildLog("註冊  registerSMSOTP=====>>>> Proc00FB()==> C1=" + OTP_TYPE + "-"
							+ userID + " C2=" + OTP_SECOND + "C3=" + otpIp + "C4=" + otpPort));
					OTP = new Proc00FB().response2(OTP_TYPE + "-" + userID, OTP_SECOND, otpIp, otpPort, "RSPWD", "NNB");
				} else {
					result.getFlatValues().put("MSGCOD", cmMsg.getValueByFieldName("MSGCOD"));
					return result;
				}
			}

			if (OTP.length() != 8) { // 有錯誤訊息
				TopMessageException top1 = TopMessageException.create(OTP);

				result.getFlatValues().put("MSGCOD", top1.getMsgcode());
				result.getFlatValues().put("MSGSTR", top1.getMsgout());
				log.warn(ESAPIUtil.vaildLog("SmsOtp.getSMSOTP() TopMessageException==" + OTP));
				log.warn(ESAPIUtil.vaildLog("SmsOtp.getSMSOTP() Msgcode==" + top1.getMsgcode()));
				log.warn(ESAPIUtil.vaildLog("SmsOtp.getSMSOTP() Msgout==" + top1.getMsgout()));

				return result;
			}

			// 發送簡訊
			String message = "";
			String MOBTEL = mobtel;
			
			if (!MOBTEL.matches(PHONE_REGEX)) {
				log.error("SmsOtp error =>> 手機號碼錯誤!!");
				throw new Exception("手機號碼錯誤!!");
			}

			log.warn(ESAPIUtil.vaildLog("SmsOtp.getSMSOTP() phone==" + MOBTEL));

			switch (msgType) {
			case "FISCAT":
				message = "【臺灣企銀】您的他行存款帳戶驗證身分確認程序之驗證碼為" + OTP + "，請於2分鐘內完成驗證，切勿將驗證碼洩露予他人。";
				break;
			case "N203A4":
				message = "【臺灣企銀】您的本行存款帳戶驗證身分確認程序之驗證碼為" + OTP + "，請於2分鐘內完成驗證，切勿將驗證碼洩露予他人。";
				break;
			default:
				message = "【臺灣企銀】您的重設密碼作業之驗證碼為" + OTP + "，請於2分鐘內完成驗證，切勿將驗證碼洩露予他人。";
				break;
			}
			try {
				log.info(ESAPIUtil.vaildLog("SmsOtp.sendSMS message = " + message));
				sendSMS(OTP, userID, MOBTEL, message, adopid); // 發送OTP驗證碼至簡訊平台
				log.warn(ESAPIUtil.vaildLog("SmsOtp.getSMSOTP() userID =" + userID + "  OTP == " + OTP));
				// 產生回應訊息
				result.getFlatValues().put("phone", MOBTEL);
				result.getFlatValues().put("MSGCOD", "0000");
				result.getFlatValues().put("MSGSTR", "OTP密碼取得成功");
				log.warn("===SmsOtp getSMSOTP success===");
			} catch (Exception e) {
				log.warn("SmsOtp.sendSMS Exception==" + e);
				result.getFlatValues().put("MSGCOD", "ZX99");
				result.getFlatValues().put("MSGSTR", "傳送簡訊失敗");
				log.warn("===SmsOtp getSMSOTP fail===");
			}

		} catch (TopMessageException top) {
			TopMessageException top1 = TopMessageException.create(top.getMsgcode());
			result.getFlatValues().put("MSGCOD", top1.getMsgcode());
			result.getFlatValues().put("MSGSTR", top1.getMsgout());
			log.warn("SmsOtp.getSMSOTP() TopMessageException==" + top);
			log.warn(ESAPIUtil.vaildLog("SmsOtp.getSMSOTP() Msgcode==" + top1.getMsgcode()));
			log.warn(ESAPIUtil.vaildLog("SmsOtp.getSMSOTP() Msgout==" + top1.getMsgout()));
		} catch (Exception e) {
			log.error("SmsOtp.getSMSOTP() Exception==" + e);
			log.warn("SmsOtp MsgCode == ZX99 ");
			result.getFlatValues().put("MSGCOD", "ZX99");
			result.getFlatValues().put("MSGSTR", "系統異常!");
		}
		log.warn("===SmsOtp getSMSOTP method end===");
		return result;
	}

	// 驗證軟體式動態密碼
	public MVH verifySMSOTP(String userID, String otp) {
		log.warn("===SmsOtp verifySMSOTP method start ===");

		MVHImpl result = new MVHImpl();
		int retCode = 0;
		String otpIp = null;
		String otpPort = null;
		try {
			log.warn("=============上行電文============");
			otpIp = smsotp.getOTPIP();
			otpPort = smsotp.getOTPPORT();
			// 驗證軟體式動態密碼
			log.warn(ESAPIUtil.vaildLog("驗證  verifySMSOTP=====>>>> Proc00F8()==> C1=" + OTP_TYPE + "-" + userID + " C2=" + otp + "C3=" + otpIp + "C4=" + otpPort));

			retCode = new Proc00F8().response2(OTP_TYPE + "-" + userID, otp, otpIp, otpPort, "RSPWD", "NNB");
			log.warn("SmsOtp verifySMSOTP retCode=========>" + retCode);
			if (retCode != 1) {
				result.getFlatValues().put("MSGCOD", Integer.toString(retCode));
				result.getFlatValues().put("MSGSTR", "OTP密碼驗證失敗");
				return result;
			}
			// 產生回應訊息
			result.getFlatValues().put("MSGCOD", "0000");
			result.getFlatValues().put("MSGSTR", "OTP密碼驗證成功");
			log.warn("===SmsOtp verifySMSOTP success===");

		} catch (TopMessageException top) {
			TopMessageException top1 = TopMessageException.create(top.getMsgcode());
			result.getFlatValues().put("MSGCOD", top1.getMsgcode());
			result.getFlatValues().put("MSGSTR", top1.getMsgout());
			log.warn("SmsOtp() TopMessageException==" + top);
			log.warn(ESAPIUtil.vaildLog("SmsOtp.verifySMSOTP() Msgcode==" + top1.getMsgcode()));
			log.warn(ESAPIUtil.vaildLog("SmsOtp.verifySMSOTP() Msgout==" + top1.getMsgout()));
		} catch (Exception e) {
			log.warn("SmsOtp.verifySMSOTP() Exception==" + e);
			log.warn("SmsOtp MsgCode == ZX99 ");
			result.getFlatValues().put("MSGCOD", "ZX99");
			result.getFlatValues().put("MSGSTR", "系統異常!");
		}
		log.warn("===SmsOtp verifySMSOTP method end===");

		return result;
	}

	// 傳送簡訊
	public void sendSMS(String _otp, String _id, String _phone, String _message, String adopid) {
		log.warn("===SmsOtp sendSMS method start ===");
		log.debug("Sms Url: " + GetConfigProperty.getValue("url"));
//		log.debug("Sms username: " + GetConfigProperty.getValue("username"));
//		log.debug("Sms userpw: " + GetConfigProperty.getValue("userpw"));
		try {
			MitakeSmsSendResult result = MitakeSms.send(_phone, _message);
			log.warn(ESAPIUtil.vaildLog("Send SMS OTP to " + _phone + ", result: " + result.toString()));
			log.warn(ESAPIUtil.vaildLog("getAccountPoint===>" + result.getAccountPoint()));
			Date d = new Date();
			SendResult sendResult = result.getSendResult();
			if (sendResult != null) {
				String phoneNumber = sendResult.phoneNumber;
				String messageId = sendResult.messageId;
				String statusCode = sendResult.statusCode.getCode();
				if (messageId != null && (statusCode.equals("1") || statusCode.equals("2") || statusCode.equals("3"))) {
					// SMS發送成功，存Log
					// 缺存簡訊
					log.warn(ESAPIUtil.vaildLog("Send SMS OTP to " + _phone + " successfully"));
				} else {
					// SMS發送失敗，存Log
					log.warn(ESAPIUtil.vaildLog( "Send SMS OTP to " + _phone + ", statusCode: " + sendResult.statusCode.getCode()));
					// 缺存簡訊
				}
				TXNSMSLOG tt = new TXNSMSLOG();
				tt.setADUSERID(_id);
				tt.setPHONE(_phone);
				tt.setSTATUSCODE(statusCode);
				tt.setMSGID(messageId);
				tt.setLASTDATE(DateTimeUtils.format("yyyyMMdd", d));
				tt.setLASTTIME(DateTimeUtils.format("HHmmss", d));
				tt.setADOPID(adopid);
				txnSmsLogDao.insert(tt);
			}
		} catch (Throwable e) {
			log.error("sendSMS Exception:" + e.getMessage());
			log.error("===SmsOtp sendSMS method fail ===");
		}
	}

}
