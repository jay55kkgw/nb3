package fstop.services.impl;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
//import java.util.HashMap;
//import java.util.Date;
import java.util.Hashtable;
//import java.util.Iterator;
import java.util.List;
//import java.util.List;
//import java.util.Map;
import java.util.Map;

//import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.core.BeanUtils;
import fstop.exception.TopMessageException;
//import fstop.model.DBResult;
import fstop.model.MVH;
import fstop.model.MVHImpl;
//import fstop.model.Row;
//import fstop.model.Rows;
import fstop.orm.dao.TxnLoanApplyDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.TXNLOANAPPLY;
import fstop.orm.po.TXNUSER;
import fstop.services.CommonService;
import fstop.util.DateTimeUtils;
import fstop.util.IPUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import fstop.util.thread.WebServletUtils;
import lombok.extern.slf4j.Slf4j;
//import fstop.util.JSONUtils;
//import fstop.util.StrUtils;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import java.security.SecureRandom;


/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class NA04_1 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	private TxnLoanApplyDao txnLoanApplyDao;
	
//	@Required
//	public void setTxnLoanApplyDao(TxnLoanApplyDao txnLoanApplyDao) {
//		this.txnLoanApplyDao = txnLoanApplyDao;
//	}
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 
		log.info("==========NA04_1.java IN==========");
		Map<String, String> _params = params;
		
		log.info(ESAPIUtil.vaildLog("NA04_1.java _params:"+CodeUtil.toJson(_params)));
		
		MVHImpl telcommResult = new MVHImpl();
//		MVHImpl cardata = new MVHImpl();
/*
		try {
			Gson gson = new Gson();
			sun.misc.BASE64Encoder encoder= new sun.misc.BASE64Encoder();//base64加密
//			sun.misc.BASE64Decoder decoder= new sun.misc.BASE64Decoder();//base64解密			
//			NA04Request na04 = new NA04Request();
			
			SecureRandom secrandom = new SecureRandom(); //取六位亂數
			String randomval="";
			for(int i=0;i<6;i++)  		   
			{  		    		   
				randomval +=secrandom.nextInt(10);
			}
			logger.info("NA04_1.java randomval---->"+randomval);
			String rcvno = (String)_params.get("RCVNO");//取小額信貸申請資料查詢的案件編號
			String act =encoder.encode("check".getBytes());//分為query:查詢 unlock:離開頁面時通知eloan解lock check:完成對保
			//String key =encoder.encode("LOAN2018032801".getBytes());//要記得改回由前一頁案件編號帶進來
			String key =encoder.encode(rcvno.getBytes());//由前一頁案件編號帶進來			
			logger.info("NA04_1.java act before:"+act);
			logger.info("NA04_1.java key before:"+key);
						
			String localIP = IPUtils.getLocalIp();//取得IP位址
	        String defaultBaseUrl ="";	        	        	        	        
	        if(localIP.equals("10.16.22.17")){//測試環境
	        	defaultBaseUrl = "http://10.16.22.155/TBBeLoan/scrt/outter/Sc2EBankWeb.jsp?act="+act+"&key="+key+"&ss="+randomval;
	        }else{							  //營運環境	
	        	defaultBaseUrl = "https://10.16.21.68:11122/ZoneParkFees/ParkfeesServlet";
	        }
	        logger.info("NA04_1.java defaultBaseUrl:"+defaultBaseUrl);
			URL obj = new URL(defaultBaseUrl);
	        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
	        con.setRequestMethod("POST");
	        con.setRequestProperty("Content-Type", "application/xml");
	        con.setDoOutput(true);
	        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
//	        logger.info("NA04.java JSON After:"+JSONUtils.toJson(na04).getBytes("UTF8"));
//	        wr.write(JSONUtils.toJson(na04).getBytes("UTF8"));	        
	        wr.flush();
	        wr.close();
	        
	        int responseCode = con.getResponseCode();
	        logger.info("NA04_1.java responseCode:" + responseCode);
	        
	        StringBuffer response = new StringBuffer();
	        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(),"UTF-8"));
	        String inputLine;
	        while ((inputLine = in.readLine()) != null) {
	            response.append(inputLine);
	        }
	        in.close();
	        
	        logger.info("NA04_1.java response Json:" + response.toString());
//	        NA04Response r = (NA04Response)gson.fromJson(response.toString(), NA04Response.class);	
	        String json = response.toString();
	        logger.info("NA04_1.java json:" + json);
	        	        	        
	        logger.info("====NA04_1.java try end====");
		}
		catch (TopMessageException e) {
			logger.info("TopMessageException error:"+e.getMsgcode());
			telcommResult.getFlatValues().put("ERRCODE", e.getMsgcode());			
		}catch(Exception e){
			logger.info("ERROR:"+e.getMessage());
			telcommResult.getFlatValues().put("ERRCODE", "ZX99");
		}
*/			
		log.info("====NA04_1.java end====");
		
		return telcommResult;
	}
	
}
