package fstop.services.impl;

import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.rest.model.CommonPools;
import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.IMasterValuesHelper;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.services.CommonService;
import fstop.orm.dao.TxnAcnApplyDao;
import fstop.orm.po.TXNACNAPPLY;
import fstop.util.SpringBeanFactory;
import lombok.extern.slf4j.Slf4j;
/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class N2041 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());
	

	@Autowired
	private CommonPools commonPools;
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 

		//查詢

		log.warn(ESAPIUtil.vaildLog("N2041.java CUSIDN:"+params.get("CUSIDN")));
		MVHImpl telcommResult = new MVHImpl();
		try {
			TxnAcnApplyDao txnAcnApplyDao = (TxnAcnApplyDao)SpringBeanFactory.getBean("txnAcnApplyDao");
			boolean user = txnAcnApplyDao.getacnapplycnt(params.get("CUSIDN").toString());
			String ACNTYPE = commonPools.applyUtils.getACNTYPEByCusidn(params.get("CUSIDN").toString());
			String NAME = commonPools.applyUtils.getNAMEByCusidn(params.get("CUSIDN").toString());
			String BRHCOD = commonPools.applyUtils.getBRHCODByCusidn(params.get("CUSIDN").toString());
			IMasterValuesHelper bhContact = commonPools.fundUtils.getAdmBhContact(BRHCOD);
			String BRHNAME = bhContact.getValueByFieldName("ADBRANCHNAME",1);				
			telcommResult.getFlatValues().put("ACNTYPE", ACNTYPE);
			telcommResult.getFlatValues().put("NAME", NAME);
			telcommResult.getFlatValues().put("CUSIDN", params.get("CUSIDN").toString());
			telcommResult.getFlatValues().put("UID", params.get("CUSIDN").toString());
			telcommResult.getFlatValues().put("BRHCOD", BRHCOD);
			telcommResult.getFlatValues().put("BRHNAME", BRHNAME);
			if(!user)
			{	
				log.warn("N2041.java can not get user data");
				throw TopMessageException.create("Z617");
			}
		}
		catch(Exception e){
			log.warn("N2041.java get user data Exception:"+e.getMessage());
			throw TopMessageException.create("Z617");
		}
		return telcommResult;
	}


}
