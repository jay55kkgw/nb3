package fstop.services.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class N103 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("n103Telcomm")
	private TelCommExec n103Telcomm;
	
//	@Required
//	public void setN103Telcomm(TelCommExec telcomm) {
//		n103Telcomm = telcomm;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 
		//查詢
		log.warn(ESAPIUtil.vaildLog("N103.java CUSIDN:"+params.get("CUSIDN").toString()+" FGTXWAY:"+params.get("FGTXWAY").toString()));
		MVHImpl telcommResult = new MVHImpl();
		//String ACNTYPE = params.get("FGTXWAY").toString().equals("4") ? "A1" : "A2";
		boolean newCust = false;
		String fgtxway = params.get("FGTXWAY").toString();
		switch (fgtxway) {
		case "4":
			params.put("ACNTYPE", "A1");
			newCust = true;
			break;
		case "5":
			params.put("ACNTYPE", "A3");
			newCust = true;
			break;
		case "6":
			params.put("ACNTYPE", "A4");
			break;
		default:
			params.put("ACNTYPE", "A2");
			break;
		}
		
		//TelcommResult telcommResult = new TelcommResult(new Vector());
		try {
			telcommResult = n103Telcomm.query(params);
		} catch(TopMessageException e) {
			if(e.getMsgcode().equals("E292") && newCust) {
				//第一類要pass無資料之訊息
				telcommResult.getFlatValues().put("TOPMSG", "0000");
			} else {
				throw TopMessageException.create(e.getMsgcode());
			}
		}
		telcommResult.getFlatValues().put("FGTXWAY", params.get("FGTXWAY").toString());
		telcommResult.getFlatValues().put("CUSIDN",params.get("CUSIDN").toString());
		telcommResult.getFlatValues().put("UID",params.get("CUSIDN").toString());
		return telcommResult;
	}


}
