package fstop.services.impl;

import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.TxnCardLogDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.TXNCARDLOG;
import fstop.orm.po.TXNUSER;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 錯誤定義： 
 * 	1.達錯誤次數時，踢回不能申請	。  -->	TopMessageException.create("Z603")
 *  2.已申請網銀不可重新申請		。  -->	TopMessageException.create("Z604")
 * 	3.電文交易錯誤				。  -->	TopMessageException e
 * 
 * @author Albert
 *
 */

@Slf4j
public class NA40 extends CommonService {

//	private Logger logger = Logger.getLogger(NA40.class);

	@Autowired
    @Qualifier("na40VerifyTelcomm")
	private TelCommExec na40VerifyTelcomm;

	@Autowired
	private TxnUserDao txnUserDao;

	@Autowired
	private TxnCardLogDao txnCardLogDao;

//	@Required
//	public void setTxnUserDao(TxnUserDao txnUserDao) {
//		this.txnUserDao = txnUserDao;
//	}
//
//	@Required
//	public void setNa40VerifyTelcomm(TelCommExec na40VerifyTelcomm) {
//		this.na40VerifyTelcomm = na40VerifyTelcomm;
//	}
//
//	@Required
//	public void setTxnCardLogDao(TxnCardLogDao txnCardLogDao) {
//		this.txnCardLogDao = txnCardLogDao;
//	}

	@Override
	public MVH doAction(Map params) {

		MVHImpl result = new MVHImpl();
		Date d = new Date();
		
		String occurMsg = "0000";
		String inputbirthyy  = (String)params.get("CCBIRTHDATEYY");
		String inputbirthmm  = (String)params.get("CCBIRTHDATEMM");
		String inputbirthdd  = (String)params.get("CCBIRTHDATEDD");

		String inputbirthday = "";
		String outputbirthday = "";

		if (inputbirthyy != null && inputbirthmm != null && inputbirthdd != null){
			inputbirthday = inputbirthyy.trim() + inputbirthmm.trim() + inputbirthdd.trim();
			Date sd = DateTimeUtils.parse("yyyyMMdd", inputbirthday);
			inputbirthday = DateTimeUtils.getCDateShort(sd);
			outputbirthday = inputbirthyy+"/"+inputbirthmm+"/"+inputbirthdd;
		}

		//先檢查使用者存不存在
		TXNUSER user = txnUserDao.chkRecord((String)params.get("_CUSIDN"));

		//錯誤次數達3次時，踢回不能申請。
		if("0000".equals(occurMsg) && user.getASKERRTIMES() >= 3) {
			occurMsg = "Z603";
		}

		//已申請網銀不可重新申請
		/*
		if("0000".equals(occurMsg) && "1".equals(user.getCCARDFLAG())){
			occurMsg = "Z604";
		}
        */
		//重新申請網銀需隔日生效
		if("0000".equals(occurMsg) && !"".equals(user.getRESETTIME().trim())){
			if(DateTimeUtils.parse("yyyyMMdd", user.getRESETTIME().trim()).compareTo(
					DateTimeUtils.parse("yyyyMMdd", DateTimeUtils.format("yyyyMMdd", new Date())) ) >= 0){
				occurMsg = "Z606";
			}			
		}
		
		//身份驗證
		if("0000".equals(occurMsg)){
			params.put("TYPE", "01");
			params.put("BIRTHDAY", inputbirthday);
			params.put("CUSIDN", (String)params.get("_CUSIDN"));			
			try {
				result = na40VerifyTelcomm.query(params);
			} catch (TopMessageException e){
				occurMsg = e.getMsgcode();
			}
		}
		log.trace("occurMsg>>>"+occurMsg);
		
		//增加錯誤次數
		if(!"0000".equals(occurMsg)){
			user.setASKERRTIMES(user.getASKERRTIMES()+1);
			txnUserDao.save(user);
		}
		
		//記錄log
		TXNCARDLOG cardlog = new TXNCARDLOG();
		cardlog.setLOGDATE(DateTimeUtils.format("yyyyMMdd", d));
		cardlog.setLOGTIME(DateTimeUtils.format("HHmmss", d));
		cardlog.setDPSUERID((String)params.get("_CUSIDN"));
		cardlog.setCARDTYPE("2");
		cardlog.setCRADNO((String)params.get("CARDNUM"));			
		cardlog.setOPERATETYPE("01");			
		cardlog.setREMOTEIP((String)params.get("USERIP"));
		cardlog.setSTATUS("0000".equals(occurMsg)? "Y" : "N");
		cardlog.setADMCODE(occurMsg);
		txnCardLogDao.save(cardlog);

		//錯誤訊息往後帶
//		result.getFlatValues().put("occurMsg", occurMsg);
		//為符合目前架構將occurMsg改為msgCode
		result.getFlatValues().put("TOPMSG", occurMsg);
		
		//將頁面訊息帶回result
		Iterator it = params.entrySet().iterator();
		while (it.hasNext()){
			Map.Entry entry = (Map.Entry) it.next();
			result.getFlatValues().put((String)entry.getKey(), (String)entry.getValue());
		}

		return result;
	}
	
}
