package fstop.services.impl;

import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.rest.model.CommonPools;
import com.netbank.rest.model.JSPUtils;
import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.IMasterValuesHelper;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.notifier.NotifyAngent;
import fstop.services.CommonService;
import fstop.services.batch.CheckServerHealth;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import fstop.va.VaCheck;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class N204 extends CommonService {

	@Autowired
	@Qualifier("n204Telcomm")
	private TelCommExec n204Telcomm;
	// private Logger logger = Logger.getLogger(N204.class);
	private Map ordersetting;
	@Autowired
	private CommonPools commonPools;

	// @Required
	// public void setN204Telcomm(TelCommExec telcomm) {
	// n204Telcomm = telcomm;
	// }
	//
	// public void setOrdersetting(Map ordersetting) {
	// this.ordersetting = ordersetting;
	// }

	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params) {
		Map<String, String> params = _params;
		// 查詢
		String MSGCODE = "";
		log.warn(ESAPIUtil.vaildLog("N204.java FGTXWAY:" + params.get("FGTXWAY").toString()));
		// 晶片金融卡
		if ("2".equals(params.get("FGTXWAY").toString())) {
			/*
			 * Date d = new Date(); String trin_transeq = "1234"; String trin_issuer =
			 * "05000000"; String trin_acnno = params.get("ACNNO").toString(); String
			 * trin_icdttm = DateTimeUtils.format("yyyyMMddHHmmss", d);
			 * logger.warn("ICDTTM ----------> " + trin_icdttm); String trin_iseqno =
			 * params.get("iSeqNo").toString(); String trin_icmemo = ""; String
			 * trin_tac_length = "000A"; String trin_tac = params.get("TAC").toString();
			 * logger.warn("TAC ----------> " + trin_tac_length + trin_tac + "(" +
			 * (trin_tac_length.length() + trin_tac.length()) + ")"); String trin_trmid =
			 * params.get("TRMID").toString();
			 * 
			 * params.put("TRANSEQ", trin_transeq); params.put("ISSUER", trin_issuer);
			 * params.put("ACNNO", trin_acnno); params.put("ICDTTM", trin_icdttm);
			 * params.put("ICSEQ", trin_iseqno); params.put("ICMEMO", trin_icmemo);
			 * params.put("TAC_Length", trin_tac_length); params.put("TAC", trin_tac);
			 * params.put("TRMID", trin_trmid);
			 */
		}
		if (params.containsKey("pkcs7Sign") && "4".equals(params.get("FGTXWAY"))) {
			// 壓簽章字串比對
			String jsondc = StrUtils.trim(params.get("jsondc").toString());
			String __mac = StrUtils.trim(params.get("pkcs7Sign").toString());
			params.put("__SIGN", __mac);
			log.warn(ESAPIUtil.vaildLog("JSONDC:" + jsondc));
			log.warn(ESAPIUtil.vaildLog("pkcs7Sign:" + __mac));

			String CertInfo = VaCheck.doVAVerify(__mac, jsondc, "VARule1");
			int VerfyICSrtn = -1;
			if (CertInfo.length() == 0) {
				MSGCODE = "Z089";
				CheckServerHealth checkserverhealth = (CheckServerHealth) SpringBeanFactory
						.getBean("checkserverhealth");
				checkserverhealth.sendZ300Notice("Z089");
				log.warn("N204.java doVAVerify fail: cert length zero");
				// throw TopMessageException.create(MSGCODE);
			} else {
				VerfyICSrtn = VaCheck.doVerfyICS(CertInfo, params.get("CUSIDN").toString(), true);
				if (VerfyICSrtn != 0) {
					MSGCODE = "Z089";
					CheckServerHealth checkserverhealth = (CheckServerHealth) SpringBeanFactory
							.getBean("checkserverhealth");
					checkserverhealth.sendZ300Notice("Z089");
					log.warn("N204.java doVerfyICS fail");
					// throw TopMessageException.create(MSGCODE);
				}
			}
		}
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);
		String NAME = JSPUtils.convertFullorHalf((String) params.get("NAME"), 1);
		params.put("CUSIDN", (String) params.get("UID"));
		params.put("NAME", NAME);
		params.put("BRHCOD",
				params.get("BRHCOD") == null ? (String) params.get("BHID") : (String) params.get("BRHCOD"));
		String occurMsg = "";
		MVHImpl telcommResult = new MVHImpl();
		String BRHCOD = params.get("BRHCOD") == null ? (String) params.get("BHID") : (String) params.get("BRHCOD");
//		String pathname = (String) ordersetting.get("pathname");
//		String uploadpath = (String) params.get("UPLOADPATH");
//		if (!StrUtils.trim(pathname).endsWith("/"))
//			pathname = StrUtils.trim(pathname) + "/";
//		if (!StrUtils.trim(uploadpath).endsWith("/"))
//			uploadpath = StrUtils.trim(uploadpath) + "/";
//		String FILE1 = (String) params.get("FILE1") == null ? "" : (String) params.get("FILE1");
//		String FILE2 = (String) params.get("FILE2") == null ? "" : (String) params.get("FILE2");
//		String FILE3 = (String) params.get("FILE3") == null ? "" : (String) params.get("FILE3");
//		String FILE4 = (String) params.get("FILE4") == null ? "" : (String) params.get("FILE4");
		try {
			if (!MSGCODE.equals("Z089")) {
				telcommResult = n204Telcomm.query(params);
				occurMsg = telcommResult.getValueByFieldName("MSGCOD");
				params.put("occurMsg", occurMsg);
				telcommResult.getFlatValues().put("occurMsg", occurMsg);
				if (occurMsg.equals("0000") || occurMsg.equals("    ")) // 線上補傳身分證件交易成功
				{
					// FTP作業
//					if (FILE1.length() > 0)
//						ftpFileToETABS1(
//								pathname + ((String) params.get("UID")).toUpperCase() + "_01"
//										+ (FILE1.substring(FILE1.lastIndexOf(".jpg"), FILE1.length())).toLowerCase(),
//								uploadpath + FILE1);
//					if (FILE2.length() > 0)
//						ftpFileToETABS1(
//								pathname + ((String) params.get("UID")).toUpperCase() + "_02"
//										+ (FILE2.substring(FILE2.lastIndexOf(".jpg"), FILE2.length())).toLowerCase(),
//								uploadpath + FILE2);
//					if (FILE3.length() > 0)
//						ftpFileToETABS1(
//								pathname + ((String) params.get("UID")).toUpperCase() + "_03"
//										+ (FILE3.substring(FILE3.lastIndexOf(".jpg"), FILE3.length())).toLowerCase(),
//								uploadpath + FILE3);
//					if (FILE4.length() > 0)
//						ftpFileToETABS1(
//								pathname + ((String) params.get("UID")).toUpperCase() + "_04"
//										+ (FILE4.substring(FILE4.lastIndexOf(".jpg"), FILE4.length())).toLowerCase(),
//								uploadpath + FILE4);

					// 發送Email通知_線上傳補資料成功
					String mail = (String) params.get("MAILADDR");
					Hashtable<String, String> varmail = new Hashtable();
					varmail.put("SUB", "完成線上開戶資料補正交易通知");
					varmail.put("MAILTEXT", getContent(BRHCOD));
					if (!NotifyAngent.sendNotice("N203", varmail, mail)) {
						log.warn(ESAPIUtil.vaildLog("線上開戶資料補正交易Email失敗.(mail:" + mail + ")"));
					}
					telcommResult.getFlatValues().put("BRHCOD", BRHCOD);
				}
			} else
				telcommResult.getFlatValues().put("occurMsg", MSGCODE);
		} catch (TopMessageException e) {
			occurMsg = e.getMsgcode();
			telcommResult.getFlatValues().put("occurMsg", occurMsg);
//			if (removefile(uploadpath + FILE1))
//				log.debug("remove pic1 successful");
//			else
//				log.warn("remove pic1 fail");
//			if (removefile(uploadpath + FILE2))
//				log.debug("remove pic2 successful");
//			else
//				log.warn("remove pic2 fail");
//			if (removefile(uploadpath + FILE3))
//				log.debug("remove pic3 successful");
//			else
//				log.warn("remove pic3 fail");
//			if (removefile(uploadpath + FILE4))
//				log.debug("remove pic4 successful");
//			else
//				log.warn("remove pic4 fail");
		}
		return telcommResult;
	}

	@SuppressWarnings("unused")
	private String getContent(String BRHCOD) {

		IMasterValuesHelper bhContact = commonPools.fundUtils.getAdmBhContact(BRHCOD);
		String BRHNAME = bhContact.getValueByFieldName("ADBRANCHNAME", 1);
		String BRHTEL = bhContact.getValueByFieldName("TELNUM", 1);
		StringBuilder sb = new StringBuilder();
		sb.append("<table cellSpacing=0 cellPadding=0 width=600 border=0 frame=border >").append("\n");
		sb.append("<tbody>").append("\n");
		sb.append("<tr><td align=left>親愛的客戶您好：</td></tr></br>").append("\n");
		sb.append("<tr><td align=left>謝謝您完成本行數位存款帳戶補上傳身分證件交易，</td></tr>").append("\n");
		sb.append("<tr><td align=left>若有任何問題，您可撥電話至您所指定服務分行：").append(BRHNAME).append("(聯絡電話：").append(BRHTEL)
				.append(")詢問</td></tr>").append("\n");
		sb.append("<tr><td align=left>我們將竭誠為您服務</td></tr>").append("\n");
		sb.append("<tr><td align=left><BR><BR>祝您事業順心，萬事如意!</td></tr>").append("\n");
		sb.append(
				"<tr><td align=left><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;臺灣企銀&nbsp;&nbsp;&nbsp;敬上</td></tr>")
				.append("\n");
		sb.append("</tbody>").append("\n");
		sb.append("</table>").append("\n");
		return sb.toString();
	}
// TODO:將上傳檔案移至NB3
//	public boolean removefile(String localPath) {
//		try {
//			File DelFile = new File(localPath);
//			DelFile.delete();
//			log.warn("刪除圖檔成功:" + localPath);
//			return (true);
//		} catch (Exception RemoveFileEx) {
//			log.warn("刪除圖檔失敗" + localPath);
//			return (false);
//		}
//	}
//
//	public boolean ftpFileToETABS1(String eloanPath, String localPath) {
//		log.info("@FTP@ " + " " + localPath + " " + eloanPath);
//		// TODO:FtpBase64有問題，先註解
//		// String localIP = IPUtils.getLocalIp();
//		// String ip = (String)ordersetting.get("ftp.ip");
//		// String tmp = "/TBB/nnb/applyfile";
//		// FtpBase64 ftpBase64 = new FtpBase64(ip);
//		// int rc=ftpBase64.upload(eloanPath, localPath);
//		// if(rc!=0)
//		// {
//		// log.warn("FTP 失敗(ETABS 主機一次) FILE："+eloanPath);
//		// CheckServerHealth checkserverhealth =
//		// (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
//		// checkserverhealth.sendUnknowNotice("FTP 失敗(ETABS 主機一次) FILE："+eloanPath);
//		// rc=ftpBase64.upload(eloanPath, localPath);
//		// if(rc!=0)
//		// {
//		// log.warn("FTP 失敗(ETABS 主機二次) FILE："+eloanPath);
//		// checkserverhealth =
//		// (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
//		// checkserverhealth.sendUnknowNotice("FTP 失敗(ETABS 主機二次) FILE："+eloanPath);
//		// return(false);
//		// }
//		// else
//		// {
//		// try
//		// {
//		// File DelFile = new File(localPath);
//		// FileUtils.copyFileTo(DelFile, tmp);
//		// RuntimeExec runtimeExec=null;
//		// if(localIP.equals("10.16.22.17"))
//		// runtimeExec=new RuntimeExec(tmp,"chown -R apt003:apgrp "+tmp+"/");
//		// else
//		// runtimeExec=new RuntimeExec(tmp,"chown -R dcp001:dcpgrp "+tmp+"/");
//		// if(runtimeExec.getcommstat()!=0)
//		// {
//		// log.warn("chown exec command error");
//		// }
//		// DelFile.delete();
//		// log.warn("刪除圖檔成功:"+localPath);
//		// }
//		// catch(Exception e)
//		// {
//		// log.warn("複製或刪除圖檔Path:"+localPath);
//		// log.warn("複製或刪除圖檔Fail:"+e.toString());
//		// }
//		// return(true);
//		// }
//		// }
//		// else
//		// {
//		// try
//		// {
//		// File DelFile = new File(localPath);
//		// FileUtils.copyFileTo(DelFile, tmp);
//		// RuntimeExec runtimeExec=null;
//		// if(localIP.equals("10.16.22.17"))
//		// runtimeExec=new RuntimeExec(tmp,"chown -R apt003:apgrp "+tmp+"/");
//		// else
//		// runtimeExec=new RuntimeExec(tmp,"chown -R dcp001:dcpgrp "+tmp+"/");
//		// if(runtimeExec.getcommstat()!=0)
//		// {
//		// log.warn("chown exec command error");
//		// }
//		// DelFile.delete();
//		// log.warn("刪除圖檔成功:"+localPath);
//		// }
//		// catch(Exception e)
//		// {
//		// log.warn("複製或刪除圖檔Path:"+localPath);
//		// log.warn("複製或刪除圖檔Fail:"+e.toString());
//		// }
//		// }
//		return true;
//	}
}
