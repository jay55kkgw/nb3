package fstop.services.impl;

import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import org.apache.commons.lang.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N580 extends CommonService {

	
	@Autowired
	@Qualifier("cn28Telcomm")
	private TelCommExec cn28Telcomm;
	
	@Autowired
	@Qualifier("n581Telcomm")
	private TelCommExec n581Telcomm;

	@Override
	public MVH doAction(Map params1) {
		Map<String, String> params = params1;

		MVHImpl result = new MVHImpl();
		MVHImpl result1 = new MVHImpl();
		String occurMsg = "0000";
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);			
		params.put("ACN", params.get("ACNNO").toString());
		String trin_transeq = "1234";
		String trin_issuer = "05000000";
		params.put("ACNNO", StringUtils.leftPad(params.get("ACNNO"), 16, '0'));
		String trin_acnno = params.get("ACNNO");
		String trin_icdttm = DateTimeUtils.format("yyyyMMddHHmmss", d);
		log.warn("ICDTTM ----------> " + trin_icdttm);
		String trin_iseqno = params.get("iSeqNo");
		String trin_icmemo = "";
		String trin_tac_length = "000A";		
		String trin_tac = params.get("TAC");
		log.warn(ESAPIUtil.vaildLog("TAC ----------> " + trin_tac_length + trin_tac + "(" + (trin_tac_length.length() + trin_tac.length()) + ")"));
		String trin_trmid = params.get("TRMID");
	
		params.put("TRANSEQ", trin_transeq);
		params.put("ISSUER", trin_issuer);
		params.put("ACNNO", trin_acnno);
		params.put("ICDTTM", trin_icdttm);
		params.put("ICSEQ", trin_iseqno);
		params.put("ICMEMO", trin_icmemo);
		params.put("TAC_Length", trin_tac_length);
		params.put("TAC", trin_tac);
		params.put("TRMID", trin_trmid);		
		params.put("FGTXWAY", "");
		boolean checkapply = false;
		String VERNUM = "";
		String ACNSTR= "";
		try {
			result = n581Telcomm.query(params);
			int i_Count = result.getValueOccurs("TYPE");
			log.warn("N580.java i_Count:"+i_Count);
			if(i_Count>0)
			{	
				for(int i=1;i<=i_Count;i++)   
				{
				    if(result.getValueByFieldName("TYPE", i).equals("A2"))
				    {
				    	checkapply = true;
				    	VERNUM = result.getValueByFieldName("VERNUM", i);
				    	log.warn("N580.java checkapply:"+checkapply+" VERNUM:"+VERNUM);
				    	break;
				    }
				}
				if(checkapply)   //有申請掃描提款服務才需發送cn28查詢有效的晶片金融卡帳號
				{
					try {
						result1=cn28Telcomm.query(params);
						int i_Count1 = result1.getValueOccurs("ACN");
						log.warn("N580.java i_Count1:"+i_Count1);
						for(int j=0;j<i_Count1;j++) {
							ACNSTR+=result1.getValueByFieldName("ACN",j+1).substring(5,16);
						}
						log.warn("N580.java ACN:"+ACNSTR);
					}catch(TopMessageException e) {
						occurMsg = e.getAppCode();
						log.warn("N580.java CN28 fail occurMsg:"+occurMsg);
					}
				}
			}
			else
			{
				checkapply=false;
			}
		} catch (TopMessageException e){
			occurMsg = e.getMsgcode();	
			log.warn("N580.java N581 fail occurMsg:"+occurMsg);
		}
		log.warn("N580.java ENABLE:"+(checkapply==false ? "N" : "Y"));
		params.put("ENABLE", checkapply==false ? "N" : "Y");	//註記是否申請掃描提款服務
    	params.put("VERNUM",VERNUM);   //約定書版號存入hashtable
		params.put("ACN",ACNSTR);      //有晶片金融卡帳號列出存入hashtable 		
		log.warn(ESAPIUtil.vaildLog("N580.java hashtable CUSIDN:"+params.get("CUSIDN")==null ? "" : params.get("CUSIDN").toString()));
		log.warn(ESAPIUtil.vaildLog("N580.java hashtable VERNUM:"+params.get("VERNUM")==null ? "" : params.get("VERNUM").toString()));
		log.warn(ESAPIUtil.vaildLog("N580.java hashtable ENABLE:"+params.get("ENABLE")==null ? "" : params.get("ENABLE").toString()));
		log.warn(ESAPIUtil.vaildLog("N580.java hashtable ACN:"+params.get("ACN")==null ? "" : params.get("ENABLE").toString()));
		//錯誤訊息往後帶
		result.getFlatValues().put("occurMsg", occurMsg);
		//ID、同意書版號、啟用掃描提款註記、帳號往後帶		
		result.getFlatValues().put("CUSIDN", params.get("CUSIDN")==null ? "" : params.get("CUSIDN").toString());
		result.getFlatValues().put("VERNUM", params.get("VERNUM")==null ? "" : params.get("VERNUM").toString());
		result.getFlatValues().put("ENABLE", params.get("ENABLE")==null ? "" : params.get("ENABLE").toString());
		result.getFlatValues().put("ACN", params.get("ACN")==null ? "" : params.get("ENABLE").toString());
		//將頁面訊息帶回result
		Iterator it = params.entrySet().iterator();
		while (it.hasNext()){
			Map.Entry entry = (Map.Entry) it.next();
			result.getFlatValues().put((String)entry.getKey(), (String)entry.getValue());
		}
		return result;
	}
}
