package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.TxnCardLogDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.TXNCARDLOG;
import fstop.orm.po.TXNUSER;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class NB30 extends CommonService  {

	@Autowired
	@Qualifier("nb30Telcomm")
	private TelCommExec nb30Telcomm;
//	@Autowired
//	@Qualifier("n930Telcomm")
//	private TelCommExec n930Telcomm; //用戶電子郵箱位址變更	
	@Autowired
	@Qualifier("n931Telcomm")
	private TelCommExec n931Telcomm;	
	@Autowired
	private TxnCardLogDao txnCardLogDao;
	@Autowired
	private TxnUserDao txnUserDao;

	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params) {
		Map<String, String> params = _params;		
		log.debug("N203 param: {}", params);
		MVHImpl telcommResult = new MVHImpl();
		boolean chkway = false;
		boolean chkna30 = false;
		try {
			String FGTXWAY = params.get("FGTXWAY").toString();  //交易機制 2:晶片金融卡 4:自然人憑證
			String NA30MSG="";
			Date d = new Date();
			String df = DateTimeUtils.format("yyyyMMdd", d);
			String tf = DateTimeUtils.format("HHmmss", d);
	
	    	params.put("CHIPACN", params.get("CHIP_ACN")==null ? "" : (String)params.get("CHIP_ACN"));
	    	
			chkway = true;
			//NA30 晶片金融卡線上申請網銀交易  第二類舊戶使用
			if("2".equals(FGTXWAY)) {						
				params.put("TYPE", "02");
				params.put("CARDNUM", (String)params.get("ACNNO"));
				params.put("CUSIDN", (String)params.get("CUSIDN"));
				params.put("MAIL", (String)params.get("MAILADDR"));
				params.put("ICSEQ", (String)params.get("ICSEQ"));
				params.put("ATMTRAN", (String)params.get("TRFLAG"));
				params.put("FGTXWAY", "");
			}
			//NB30 第一類、第三類新戶使用...2021/04/22 Ziv修改
			if("4".equals(FGTXWAY) || "5".equals(FGTXWAY)) {	
				String type = FGTXWAY.equals("4") ? "01" : "03"; 
				params.put("TYPE", type);
				params.put("FGTXWAY", "");
			}
			if("6".equals(FGTXWAY)) {
				params.put("TYPE", "04");
				params.put("FGTXWAY", "");
			}
			
			try{
				telcommResult = nb30Telcomm.query(params);
				NA30MSG=telcommResult.getValueByFieldName("MSGCOD");
				log.warn("N203.java NB30 MSGCOD:"+NA30MSG);
				if(NA30MSG.equals("0000"))  //晶片金融卡線上申請網銀成功
				{ 				    								
					chkna30 = true;
					if("2".equals(FGTXWAY))
					{
						TXNCARDLOG cardlog = new TXNCARDLOG();		
						cardlog.setLOGDATE(DateTimeUtils.format("yyyyMMdd", d));
						cardlog.setLOGTIME(DateTimeUtils.format("HHmmss", d));
						cardlog.setDPSUERID((String)params.get("CUSIDN"));
						cardlog.setCARDTYPE("1");
						cardlog.setCRADNO((String)params.get("CARDNUM"));
						cardlog.setOPERATETYPE("02");
						cardlog.setREMOTEIP((String)params.get("IP"));
						cardlog.setSTATUS("Y");
						cardlog.setADMCODE("");
						txnCardLogDao.save(cardlog);
					}
					//N930更新中心EMAIL位址
//					params.put("E_MAIL_ADR", (String)params.get("MAILADDR"));
//					params.put("FLAG", "02");  //更新需用02打中心更心
//					try {
//						telcommResult = n930Telcomm.query(params);
//						log.warn("N203.java n930 MSGCOD:"+telcommResult.getValueByFieldName("MSGCOD"));
//					}catch(TopMessageException e) {
//						log.warn("N203.java n930 fail MSGCOD:"+e.getMessage());
//					}
					//N931開通電子帳單註記
					params.put("FGTXWAY", FGTXWAY);
					params.put("PPSYNC", "");
					params.put("PINKEY", "");
					params.put("PPSYNCN", "");
					params.put("PINNEW", "");
					params.put("TYPE", "01");
					params.put("ITEM", "10");		
					params.put("CARDAP", "Y");
					try {
						telcommResult = n931Telcomm.query(params);
						log.warn("N203.java n931 MSGCOD:"+telcommResult.getValueByFieldName("MSGCOD"));
					}catch(TopMessageException e) {
						log.warn("N203.java n931 fail MSGCOD:"+e.getMessage());
					}
				}
			}catch (TopMessageException e) {
				NA30MSG = e.getMsgcode();
				telcommResult.getFlatValues().put("TOPMSG", NA30MSG);
				log.warn("NB30 MSGCOD:"+NA30MSG);
			}
		} catch (Exception e) {
			log.error("NB30 error >> {}", e);
			telcommResult.getFlatValues().put("TOPMSG", "FE0003");
		}
		finally {
			//更新TXNUSER
			try {
				TXNUSER user = null;
				if(chkna30) {
					user = txnUserDao.chkRecordNotify((String)params.get("CUSIDN"), (String)params.get("MAILADDR"), true, true);
				}
				
				if(!chkway && !chkna30) {
					user = txnUserDao.chkRecordNotify((String)params.get("CUSIDN"), (String)params.get("MAILADDR"), false, false);
				}
				
				if(user == null) {
					log.warn("txnUserDao chkRecordNotify fail");
				}
			}
			catch (Exception e) {
				// TODO: handle exception
				log.error("TXNUSER ERROR>>>{}",e);
			}
			
		}
		return telcommResult;
	}
}
