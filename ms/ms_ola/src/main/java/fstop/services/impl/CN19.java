package fstop.services.impl;

import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.TxnVAApplyDao;
import fstop.orm.po.TXNVAAPPLY;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CN19 extends CommonService {

//	private Logger logger = Logger.getLogger(CN19.class);

	@Autowired
	@Qualifier("cn19Telcomm")
	private TelCommExec cn19Telcomm;
	@Autowired
	private TxnVAApplyDao txnVAApplyDao;
	@Value("${MSIP}")
	private String msIp;
//	@Required
//	public void setCn19Telcomm(TelCommExec cn19Telcomm) {
//		this.cn19Telcomm = cn19Telcomm;
//	}
//	@Required
//	public void setTxnVAApplyDao(TxnVAApplyDao txnVAApplyDao) {
//		this.txnVAApplyDao = txnVAApplyDao;
//	}
	@Override
	public MVH doAction(Map params1) {
		Map<String, String> params = params1;

		MVHImpl result = new MVHImpl();

		String occurMsg = "0000";
		Date d = new Date();
		String trin_transeq = "1234";
		String trin_issuer = "05000000";
		params.put("ACNNO", StringUtils.leftPad(params.get("ACNNO"), 16, '0'));
		String trin_acnno = params.get("ACNNO");
		String trin_icdttm = DateTimeUtils.format("yyyyMMddHHmmss", d);
		log.debug("ICDTTM ----------> " + trin_icdttm);
		String trin_iseqno = params.get("iSeqNo");
		String trin_icmemo = "";
		String trin_tac_length = "000A";		
		String trin_tac = params.get("TAC");
		log.debug(ESAPIUtil.vaildLog("TAC ----------> " + trin_tac_length + trin_tac + "(" + (trin_tac_length.length() + trin_tac.length()) + ")"));
		String trin_trmid = params.get("TRMID");
	
		params.put("TRANSEQ", trin_transeq);
		params.put("ISSUER", trin_issuer);
		params.put("ACNNO", trin_acnno);
		params.put("ICDTTM", trin_icdttm);
		params.put("ICSEQ", trin_iseqno);
		params.put("ICMEMO", trin_icmemo);
		params.put("TAC_Length", trin_tac_length);
		params.put("TAC", trin_tac);
		params.put("TRMID", trin_trmid);
		params.put("FGTXWAY", "");
		String STATUS="";
			try {
				result = cn19Telcomm.query(params);
				STATUS="Y";
			} catch (TopMessageException e){
				result.getFlatValues().put("TOPMSG", e.getMsgcode());
				occurMsg = e.getMsgcode();
				STATUS="N";
			}
			//記錄軟憑申請服務LOG
			try {
				TXNVAAPPLY vaapply = new TXNVAAPPLY();	
				vaapply.setUSERID((String)params.get("UID"));
				vaapply.setSTATUS(STATUS);
				vaapply.setVERSION(params.get("CN19VER").toString());
				vaapply.setMSGCODE(occurMsg);
				vaapply.setIP(msIp);
				vaapply.setLASTDATE(DateTimeUtils.format("yyyyMMdd", d));
				vaapply.setLASTTIME(DateTimeUtils.format("HHmmss", d));
				txnVAApplyDao.save(vaapply);
			}
			catch(Exception e) {
				log.warn("txnVAApplyDao save Exception:"+e.getMessage());
			}		
		
		//將頁面訊息帶回result
		Iterator it = params.entrySet().iterator();
		while (it.hasNext()){
			Map.Entry entry = (Map.Entry) it.next();
			result.getFlatValues().put((String)entry.getKey(), (String)entry.getValue());
		}

		return result;
	}
}
