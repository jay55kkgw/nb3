package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class P005 extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("p005Telcomm")
	private TelCommExec p005Telcomm;
	
//	@Required
//	public void setP005Telcomm(TelCommExec telcomm) {
//		this.p005Telcomm = telcomm;
//	}
	
	@Override
	public MVH doAction(Map params) {		
		TelcommResult helper = p005Telcomm.query(params);
		helper.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		
		return helper;
	}
}
