package com.netbank.rest.configuration;

import com.netbank.rest.web.filter.SysFilter;
import lombok.extern.slf4j.Slf4j;
import org.apache.cxf.spring.boot.autoconfigure.CxfAutoConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorViewResolver;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.servlet.ModelAndView;
import org.yaml.snakeyaml.Yaml;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Configuration
@Slf4j
public class TelcommFormatConfig {


    @Bean
    public Map<String, String> defaultFieldFormatDefine() {
        Map<String, String> fmt = new HashMap<>();

        fmt.put("ADPIBAL", "N:#0.00");
        fmt.put("AMT", "N:#0.00");
        fmt.put("AMT5", "N:#0.00");
        fmt.put("AMOUNT", "N:#0");
        fmt.put("AMTFDP", "N:#0.00");
        fmt.put("AMTTRN", "N:#0.00");
        fmt.put("AVAILABLE", "N:#0.00");
        fmt.put("BAL", "N:#0.00");
        fmt.put("BALANCE", "N:#0.00");
        fmt.put("NTDBAL", "N:#0.00");
        fmt.put("BDPIBAL", "N:#0.00");
        fmt.put("CLR", "N:#0.00");
        fmt.put("FEXRATE", "N:#0.0000000");
        fmt.put("LNFIXR", "N:#0.000000");
        fmt.put("LNCOS", "N:#0.00");
        fmt.put("ITR", "N:#0.000");
        fmt.put("RLCTXAMT", "N:#0.00");
        fmt.put("RLCOSBAL", "N:#0.00");
        fmt.put("RLGAMT", "N:#0.00");
        fmt.put("RLGOS", "N:#0.00");
        fmt.put("RADVFIXR", "N:#0.000000");
        fmt.put("RBILLAMT", "N:#0.00");
        fmt.put("CAPCOS", "N:#0.00");
        fmt.put("INTCOS", "N:#0.00");
        fmt.put("RORIGAMT", "N:#0.00");
        fmt.put("RLCOS", "N:#0.00");
        fmt.put("PRNDAMT", "N:#0.00");
        fmt.put("ACUCOUNT", "N:#0.0000");
        fmt.put("REFUNDAMT", "N:#0.0000");
        fmt.put("FXRATE", "N:#0.0000");
        fmt.put("REFVALUE", "N:#0.00");
        fmt.put("RTNRATE", "N:#0.00");
        fmt.put("TOTAMT", "N:#0.00");
        fmt.put("PAYAMT", "N:#0.00");
        fmt.put("FUNDAMT", "N:#0.00");
        fmt.put("RATE", "N:#0.00000000");
        fmt.put("O_TOTBAL", "N:#0.00");
        fmt.put("O_AVLBAL", "N:#0.00");
        fmt.put("SNTCNT", "N:#0");
        fmt.put("LCYRAT", "N:#0.00");
        fmt.put("FCYRAT", "N:#0.00");
        fmt.put("SRCAMNT", "N:#0.00");
        fmt.put("CURAMNT", "N:#0.00");
        fmt.put("PAYCLAS", "N:#0");
        fmt.put("PAY", "N:#0");
        fmt.put("PAYADD", "N:#0");
        fmt.put("PAYCNT1", "N:#0");
        fmt.put("PAYCNT2", "N:#0");
        fmt.put("PAYCNT3", "N:#0");
        fmt.put("PAYCNT4", "N:#0");
        fmt.put("MONPAY", "N:#0");
        fmt.put("TAX", "N:#0");
        fmt.put("E_PAY", "N:#0");
        fmt.put("H_PAY", "N:#0");
        fmt.put("FUND", "N:#0");
        fmt.put("FUNDPAY", "N:#0");
        fmt.put("COSTCNT1", "N:#0");
        fmt.put("AVECNT", "N:#0");
        fmt.put("L_PAY", "N:#0");
        fmt.put("HOUSEPY", "N:#0");
        fmt.put("CARPAY", "N:#0");
        fmt.put("COSTCNT2", "N:#0");
        fmt.put("COSTCNT3", "N:#0");
        fmt.put("COSTCNT4", "N:#0");
        fmt.put("COSTAMT", "N:#0");
        fmt.put("YEARAMT", "N:#0");
        fmt.put("TAXAMT", "N:#0");
        fmt.put("FOOD", "N:#0");
        fmt.put("MONEPAY", "N:#0");
        fmt.put("RETPAYA", "N:#0");
        fmt.put("RETTOTA", "N:#0");
        fmt.put("RETPAYB", "N:#0");
        fmt.put("RETTOTB", "N:#0");
        fmt.put("U_price", "N:#0");
        fmt.put("Sale_amt", "N:#0");
        fmt.put("Tax_amt", "N:#0");
        fmt.put("Disc_amt", "N:#0");
        fmt.put("Sale_q", "N:#0.00");
        fmt.put("Take_q", "N:#0");
        fmt.put("U_price_g", "N:#0");
        fmt.put("Dpdiff", "N:#0");
        fmt.put("TSFBAL", "N:#0.00");
        fmt.put("GDBAL", "N:#0.00");
        fmt.put("ODFBAL", "N:#0.00");
        fmt.put("MKBAL", "N:#0");
        fmt.put("RATIO", "N:#0.00");
        fmt.put("TRNGD", "N:#0.00");
        fmt.put("PRICE", "N:#0");
        fmt.put("TRNAMT", "N:#0");
        fmt.put("AMT_FEE", "N:#0");

        return fmt;
    }


    @Bean
    public Map<String, String> fxFieldFormatDefine() {
        Map<String, String> fmt = new HashMap<>();
        fmt.put("ADPIBAL", "N:#0.00");
        fmt.put("AMT", "N:#0.00");
        fmt.put("AMOUNT", "N:#0.00");
        fmt.put("AMTFDP", "N:#0.00");
        fmt.put("AMTTRN", "N:#0.00");
        fmt.put("AVAILABLE", "N:#0.00");
        fmt.put("BAL", "N:#0.00");
        fmt.put("BALANCE", "N:#0.00");
        fmt.put("NTDBAL", "N:#0.00");
        fmt.put("BDPIBAL", "N:#0.00");
        fmt.put("CLR", "N:#0.00");
        fmt.put("FEXRATE", "N:#0.0000000");
        fmt.put("LNFIXR", "N:#0.000000");
        fmt.put("LNCOS", "N:#0.00");
        fmt.put("ITR", "N:#0.00");
        fmt.put("RLCTXAMT", "N:#0.000");
        fmt.put("RLCOSBAL", "N:#0.000");
        fmt.put("RLGAMT", "N:#0.00");
        fmt.put("RLGOS", "N:#0.00");
        fmt.put("RADVFIXR", "N:#0.000000");
        fmt.put("RBILLAMT", "N:#0.00");
        fmt.put("CAPCOS", "N:#0.00");
        fmt.put("INTCOS", "N:#0.00");
        fmt.put("RORIGAMT", "N:#0.00");
        fmt.put("RLCOS", "N:#0.00");
        fmt.put("PRNDAMT", "N:#0.00");
        fmt.put("ACUCOUNT", "N:#0.0000");
        fmt.put("REFUNDAMT", "N:#0.0000");
        fmt.put("FXRATE", "N:#0.0000");
        fmt.put("REFVALUE", "N:#0.00");
        fmt.put("RTNRATE", "N:#0.00");
        fmt.put("TOTAMT", "N:#0.00");
        fmt.put("PAYAMT", "N:#0.00");
        fmt.put("FUNDAMT", "N:#0.00");
        fmt.put("RATE", "N:#0.00000000");
        fmt.put("O_TOTBAL", "N:#0.00");
        fmt.put("O_AVLBAL", "N:#0.00");
        fmt.put("RLCAMT", "N:#0.00");
        fmt.put("RREMITAM", "N:#0.00");
        return fmt;
    }
}
