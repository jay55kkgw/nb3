package com.netbank.rest.model.pool;

import java.lang.reflect.InvocationTargetException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.rest.model.NewOneCallback;
import com.netbank.rest.model.Pool;
import com.netbank.util.ESAPIUtil;

import fstop.core.BeanUtils;
import fstop.model.DBResult;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.model.RowFilterCallback;
import fstop.model.Rows;
import fstop.model.RowsSelecter;
import fstop.orm.dao.AdmBankDao;
import fstop.orm.dao.AdmBranchDao;
import fstop.orm.dao.AdmCurrencyDao;
import fstop.orm.dao.AdmKeyValueDao;
import fstop.orm.dao.AdmRoleAuthDao;
import fstop.orm.dao.SysOpGroupDao;
import fstop.orm.dao.TxnTrAccSetDao;
import fstop.orm.po.ADMCURRENCY;
import fstop.orm.po.TXNTRACCSET;
import fstop.util.JSONUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class UISelectListPool extends Pool {
	private Logger logger = Logger.getLogger(getClass());
	
	@Autowired
	private AdmBankDao admBankDao;
	
	@Autowired
	private TxnTrAccSetDao txnTrAccSetDao;
	
	@Autowired
	private AdmCurrencyDao admCurrencyDao;
	
	@Autowired
	private SysOpGroupDao sysOpGroupDao;
	
	@Autowired
	@Qualifier("n921Pool")
	private N921Pool n921Pool;
	
	@Autowired
	private AdmBranchDao admBranchDao;
	
	@Autowired
	private AdmRoleAuthDao admRoleAuthDao;
	
	@Autowired
	private AdmKeyValueDao admKeyValueDao;
	
	
	/*
	 * 
	 */
	public MVH getDayListByMonth(final int month) {
		
		NewOneCallback callback = new NewOneCallback() {

			public Object getNewOne() {

				Calendar cal = Calendar.getInstance();
				
				cal.set(Calendar.DATE, 1);
				cal.set(Calendar.MONTH, month -1);
				cal.add(Calendar.MONTH, 1);
				cal.add(Calendar.DATE, -1);
				
				cal.getMaximum(Calendar.MONTH);
				
				HashMap<String, String> m = new HashMap();
				int endd = cal.get(Calendar.DATE);
				for(int i=1; i < endd; i++) {
					m.put("DAY_" + i, i+ "");
				}
				
				return new MVHImpl(m);
			}
			
		};

		return (MVH)getPooledObject("getDayListByMonth(" + month + ")", callback);
	}

	/**
	 * 取得所有銀行列表
	 * @return
	 */
	public MVH getBankList() {
		
		NewOneCallback callback = new NewOneCallback() {

			public Object getNewOne() {
				return new DBResult(admBankDao.getAll());
			}
			
		};
		return (MVH)getPooledObject("getBankList()", callback);
	}
	
	/**
	 * 取得指定分類的鍵與值列表
	 * @return
	 */
	public MVH getKeyValueList(final String category) {
		
		NewOneCallback callback = new NewOneCallback() {

			public Object getNewOne() {
				return new DBResult(admKeyValueDao.find("CATEGORY", category));
			}
			
		};
		return (MVH)getPooledObject("getKeyValueList(" + category + ")", callback);
	}
	
	/**
	 * 取得所有分行列表
	 * @return
	 */
	public MVH getBranchList() {
		
		NewOneCallback callback = new NewOneCallback() {

			public Object getNewOne() {
				return new DBResult(admBranchDao.getAll());
			}
			
		};
		return (MVH)getPooledObject("getBranchkList()", callback);
	}
	
	/**
	 * 取得 幣別
	 * @return
	 */
	public MVH getCRYList() { 
		
		NewOneCallback callback = new NewOneCallback() {

			public Object getNewOne() {
				return new DBResult(admCurrencyDao.getAll());
			}
			
		};
		return (MVH)getPooledObject("getCRYList()", callback);
	}
	
	public MVH getCRY(String cry) { 
		
		if ("".equals(cry))
			return new MVHImpl();
		else if ("NTD".equals(cry))
			cry = "TWD";
				
		try {
			ADMCURRENCY cur = admCurrencyDao.findById(cry);
			
			Map bean = BeanUtils.describe(cur);
			return new MVHImpl(bean);
		}
		catch(Exception e){}
		
		return new MVHImpl();
	}
	
	/**
	 * 常用帳號
	 * @param uid
	 * @return
	 */
	public MVH getCustomAcnoList(final String uid) {

		NewOneCallback callback = new NewOneCallback() {

			public Object getNewOne() {
				List<TXNTRACCSET> dqr = txnTrAccSetDao.getByUid(uid);
				log.trace(ESAPIUtil.vaildLog("dqr >>{}"+dqr.toArray().toString()));
				
				Rows result = new Rows();
				
				for(TXNTRACCSET row : dqr) {
					try {
						Map<String,String> bean = BeanUtils.describe(row);
						String bankCode = row.getDPTRIBANK();
						log.trace(ESAPIUtil.vaildLog("bankCode >>"+bankCode));
						String bankName = getBankName(bankCode);
						log.trace(ESAPIUtil.vaildLog("bankName >>"+bankName));
						//row.setValue("ADBANKNAME", admbank.getADBANKNAME());
						
						bean.put("DPTRIBANK", bankCode);
						bean.put("DPTRDACNO", row.getDPTRDACNO());
						
						//統一使用 BNKCOD, 及 ACN
						Map<String, String> mr = new LinkedHashMap();
						mr.put("BNKCOD", (String)bean.get("DPTRIBANK"));
						log.trace(ESAPIUtil.vaildLog("bean.DPTRIBANK >> "+(String)bean.get("DPTRIBANK")));
						log.trace(ESAPIUtil.vaildLog("bean.DPTRDACNO >> "+(String)bean.get("DPTRDACNO")));
						mr.put("ACN", (String)bean.get("DPTRDACNO"));
						String agree = row.getDPTRACNO().equals("1") ? "1" : "0"; //資料庫中常用帳號  1:約定 2:非約定
						mr.put("AGREE", agree); //0: 非約定, 1: 約定
						log.trace("Agree >> {}",agree);
						bean.put("VALUE", JSONUtils.map2json(mr));
						log.trace(ESAPIUtil.vaildLog("VALUE>>" +JSONUtils.map2json(mr)));
						String txt = bean.get("DPTRIBANK") + "-" + bankName + " " + 
							bean.get("DPTRDACNO") + " " + 
								StrUtils.trim((String)bean.get("DPGONAME"));
						bean.put("TEXT", txt);
						log.trace(ESAPIUtil.vaildLog("TEXT >> " + txt));
						result.addRow(new Row(bean));
					} catch (IllegalAccessException e) {
						logger.error("無法將 PO 轉換成 Map.", e);
					} catch (InvocationTargetException e) {
						logger.error("無法將 PO 轉換成 Map.", e);
					} catch (NoSuchMethodException e) {
						logger.error("無法將 PO 轉換成 Map.", e);
					}
				}
				
				return result;
			}
			
		};
		
		//Rows poolobj = (Rows)getPooledObject("getCustomAcnoList_" + uid, callback);
		
		return new MVHImpl((Rows)callback.getNewOne());
	}

	/**
	 * 合併N921
	 * 約定加常用帳號, 限定 約定或非約定
	 * @param uid
	 * @param DPTRACNO
	 * @return
	 */
	public MVH getAgreeAcnoList(final String uid, final String[] agree) {
		return getAgreeAcnoList(uid, agree, false);
	}

	/**
	 * 合併N921
	 * 約定加常用帳號, 限定 約定或非約定
	 * @param uid
	 * @param DPTRACNO
	 * @param agreeOnly
	 * @return
	 */
	public MVH getAgreeAcnoList(final String uid, final String[] agree, boolean agreeOnly) {

		Rows resultRows = new Rows();
		
		final Map<String, Row> inDbAcc = new HashMap();
		final Map<String, Row> notAgreeAcc = new HashMap();
		final String tbbBankCode = "050";
		MVHImpl mvh = (MVHImpl)getCustomAcnoList(uid);
			
		RowsSelecter rowselecter = new RowsSelecter(mvh.getOccurs());
		rowselecter.setFilter(new RowFilterCallback(){

			public boolean accept(Row row) {
				//資料庫中常用帳號  1:約定 2:非約定
				if("1".equals(row.getValue("DPTRACNO"))) {  //只要約定帳號
					inDbAcc.put(row.getValue("DPTRIBANK") + "_" + row.getValue("DPTRDACNO"), row);
				}else if ("2".equals(row.getValue("DPTRACNO"))) {
					notAgreeAcc.put(row.getValue("DPTRIBANK") + "_" + row.getValue("DPTRDACNO"), row);
				}
				
				return true;
			}}
		);
		
		MVHImpl mvhN921 = (MVHImpl)n921Pool.getAcnoList(uid, agree);
		
		Rows rows = mvhN921.getOccurs();
		
		//先加入N921約定帳號
		//VALUE=0:非約定 VALUE=1:約定
		if (rows.getSize() > 0) {
			Row firstRow = new Row(new HashMap());
			firstRow.setValue("VALUE", "#");
			firstRow.setValue("TEXT", "<B>--約定帳號--</B>");
			firstRow.setValue("BNKCOD", "");
			resultRows.addRow(firstRow);
			
			int addCount = 0;
			for(Row r : rows.getRows()) {
				String acno = StrUtils.trim(r.getValue("ACN"));
				Row rx = new Row(new HashMap());
				String key = r.getValue("BNKCOD") + "_" + acno;
				
				if(inDbAcc.containsKey(key)) 
				{//N921與DB皆有設定此轉入帳號
					Row dbRow = inDbAcc.get(key);
					rx.setValue("VALUE", r.getValue("VALUE"));
					rx.setValue("TEXT", dbRow.getValue("TEXT"));	//使用DB的TEXT
					rx.setValue("BNKCOD", r.getValue("BNKCOD"));
					resultRows.addRow(rx);
				}
				else 
				{
					//DB裡的常用帳號可能是16位的(舊網銀轉入)
					String tempAcno = StrUtils.repeat("0", 16) + acno;
					String key16 = r.getValue("BNKCOD") + "_" + tempAcno.substring(tempAcno.length() - 16);
					
					if(inDbAcc.containsKey(key16)) 
					{//N921與DB皆有設定此轉入帳號
						Row dbRow = inDbAcc.get(key16);
						rx.setValue("VALUE", r.getValue("VALUE"));
						rx.setValue("TEXT", r.getValue("TEXT") + " " + dbRow.getValue("DPGONAME"));	//使用N921的TEXT + DB的好記名稱
						rx.setValue("BNKCOD", r.getValue("BNKCOD"));
						resultRows.addRow(rx);
					}
					else
					{//只有N921有此轉入帳號，DB沒有
						rx.setValue("VALUE", r.getValue("VALUE"));
						rx.setValue("TEXT", r.getValue("TEXT"));
						rx.setValue("BNKCOD", r.getValue("BNKCOD"));
						resultRows.addRow(rx);
					}
				}
				addCount ++;
			}
			
			if (addCount == 0)
				resultRows.removeRow(firstRow);
		}
			
		
		//再加入DB 裡設定的常用非約定帳號
		if (!agreeOnly && notAgreeAcc.size() > 0) {
			Row firstRow = new Row(new HashMap());
			firstRow.setValue("VALUE", "#");
			firstRow.setValue("TEXT", "<B>--常用非約定帳號--</B>");
			firstRow.setValue("BNKCOD", "");
			resultRows.addRow(firstRow);
			
			int addCount = 0;
			Iterator pairs = notAgreeAcc.entrySet().iterator();
			while (pairs.hasNext())
			{
				Row rx = new Row(new HashMap());
				Map.Entry<String, Row> entry = (Map.Entry<String, Row>)pairs.next();
				Row dbRow = notAgreeAcc.get(entry.getKey());
				rx.setValue("VALUE", dbRow.getValue("VALUE"));
				rx.setValue("TEXT", dbRow.getValue("TEXT"));
				rx.setValue("BNKCOD", dbRow.getValue("TEXT").substring(0,3));
				resultRows.addRow(rx);
				addCount ++;
			}
			
			if (addCount == 0)
				resultRows.removeRow(firstRow);
		}
		
		MVHImpl result = new MVHImpl(resultRows);

		return result;
	}

	/**
	 * 取得銀行的中文名稱
	 * @param bankcod
	 * @return
	 */
	public String getBankName(final String bankcod) {
		String result = "";
		
		NewOneCallback callback = new NewOneCallback() {

			public Object getNewOne() {
				return admBankDao.findById(bankcod).getADBANKNAME();
			}
			
		};
		
		try {
			result = (String)getPooledObject("getBankName(" + bankcod + ")", callback);
		}
		catch(Exception e) {
			logger.error(ESAPIUtil.vaildLog("找不到分行的中文名稱(" + bankcod + ")."));
		}
		return result;
	}
	
	/**
	 * 取得銀行的中文名稱
	 * @param bankcod
	 * @return
	 */
	public MVH getGroupChilds(final String gpauth) {
		
		MVHImpl result = new MVHImpl(sysOpGroupDao.getGroupChilds(gpauth));
		return result;
	}
	
	public MVH getGroup(final String gpauth, String[] adopids) {
		
		return sysOpGroupDao.getGroup(gpauth, adopids);
	}
	
	/**
	 * 取得權限
	 * @param roleno
	 * @param staffno
	 * @return
	 */
	public Vector getRoleAuth(final String roleno, final String staffno) {
		
		Vector result = new Vector();
		
		//先找staff的權限，如沒有設定，則使用role的權限
		String auth = "";
		auth = admRoleAuthDao.findRolenoAuth(roleno, staffno);
		if (auth.length() == 0)
		{
			auth = admRoleAuthDao.findRolenoAuth(roleno);			
		}
		
		if (auth.length() == 0)
			return result;
		
		String[] authList = StrUtils.trim(auth).split(",");
		for (int i = 0; i < authList.length; i++)
		{
			result.add(authList[i]);
		}
		
		return result;
	}
	
	/**
	 * 合併N921
	 * 約定加常用帳號, 限定 約定或非約定
	 * @param uid
	 * @param DPTRACNO
	 * @return
	 */
	public MVH getn921AcnoAndBankName(final String uid, final String[] agree) {

		Rows resultRows = new Rows();
		
		final Map<String, Row> inDbAcc = new HashMap();
		MVHImpl mvh = (MVHImpl)getCustomAcnoList(uid);
		RowsSelecter rowselecter = new RowsSelecter(mvh.getOccurs());
		rowselecter.setFilter(new RowFilterCallback(){

			public boolean accept(Row row) {
				if("1".equals(row.getValue("DPTRACNO"))) {  //只要約定帳號
					inDbAcc.put(row.getValue("DPTRIBANK") + "_" + row.getValue("DPTRDACNO"), row);
				}
				return true;  //只要約定帳號
			}}
		);
		
		MVHImpl mvhN921 = (MVHImpl)n921Pool.getAcnoList(uid, agree);
		Rows rows = mvhN921.getOccurs();
		
		if (rows.getSize() > 0) {
			for(Row r : rows.getRows()) {
				Row rx = new Row(new HashMap());
				rx.setValue("VALUE", r.getValue("VALUE"));
				log.trace("VALUE >>{}", r.getValue("VALUE"));
				rx.setValue("TEXT", r.getValue("TEXT"));
				log.trace("TEXT >>{}", r.getValue("TEXT"));
				rx.setValue("BNKCOD", r.getValue("BNKCOD"));
				log.trace("BNKCOD >>{}", r.getValue("BNKCOD"));
				resultRows.addRow(rx);
				/*String key = r.getValue("BNKCOD") + "_" + r.getValue("ACN");
				if(inDbAcc.containsKey(key)) {
					Row dbRow = inDbAcc.get(key);
					//設定的常用帳號裡有設定
					rx.setValue("VALUE", r.getValue("VALUE"));
					rx.setValue("TEXT", dbRow.getValue("TEXT"));
					rx.setValue("BNKCOD", r.getValue("BNKCOD"));
					resultRows.addRow(rx);
				}
				else {
						rx.setValue("VALUE", r.getValue("VALUE"));
						rx.setValue("TEXT", r.getValue("TEXT"));
						rx.setValue("BNKCOD", r.getValue("BNKCOD"));
						resultRows.addRow(rx);
				}*/
			}
		}		
		MVHImpl result = new MVHImpl(resultRows);
		return result;
	}

}
