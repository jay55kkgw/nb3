package com.netbank.rest.configuration;

import com.netbank.rest.spring.AutowiringSpringBeanJobFactory;
import org.quartz.JobDetail;
import org.quartz.SimpleTrigger;
import org.quartz.spi.JobFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.scheduling.quartz.SimpleTriggerFactoryBean;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.Properties;

//@Configuration
//@ConditionalOnProperty(name = "quartz.enabled")
public class SchedulerConfig {

    private static final Logger log = LoggerFactory.getLogger(SchedulerConfig.class);

    @Bean
    public JobFactory jobFactory(ApplicationContext applicationContext
            //,
                                 // injecting SpringLiquibase to ensure liquibase is already initialized and created the quartz tables:
                                 //SpringLiquibase springLiquibase
    )
    {

        //log.info("Spring Liquibase start ? " + (springLiquibase != null));

        AutowiringSpringBeanJobFactory jobFactory = new AutowiringSpringBeanJobFactory();
        jobFactory.setApplicationContext(applicationContext);
        return jobFactory;
    }

    @Bean
    public SchedulerFactoryBean schedulerFactoryBean(@Qualifier("dataSource") DataSource dataSource, JobFactory jobFactory
//            ,
//                                                     @Qualifier("calculateFilterConditionJobTrigger") Trigger calculateFilterConditionJobTrigger,
//                                                     @Qualifier("amSyncJobTrigger") Trigger amSyncJobTrigger,
//                                                     @Qualifier("notifyFilterConditionJobTrigger") Trigger notifyFilterConditionJobTrigger


                                                     ) throws IOException {
        log.info("schedulerFactoryBean start ? " + (dataSource != null));
        SchedulerFactoryBean factory = new SchedulerFactoryBean();
        // this allows to update triggers in DB when updating settings in config file:
        factory.setOverwriteExistingJobs(true);
        factory.setDataSource(dataSource);
        factory.setJobFactory(jobFactory);

        factory.setQuartzProperties(quartzProperties());


//        factory.setTriggers(calculateFilterConditionJobTrigger, amSyncJobTrigger, notifyFilterConditionJobTrigger);  //啟動 自動 Sync AppManager 的資料

        // factory.setTriggers(notifyFilterConditionJobTrigger);  //過濾條件到期通知

        return factory;
    }

    @Bean
    public Properties quartzProperties() throws IOException {
        log.info("quartzProperties start true ");
        PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
        propertiesFactoryBean.setLocation(new ClassPathResource("/quartz.properties"));
        propertiesFactoryBean.afterPropertiesSet();
        return propertiesFactoryBean.getObject();
    }


    // ----
//    @Bean
//    public JobDetailFactoryBean amSyncJobDetail() {
//        log.info("amSyncJobDetail start true ");
//        return createJobDetail(AmSyncJob.class);
//    }
//
//    @Bean(name = "amSyncJobTrigger")
//    public SimpleTriggerFactoryBean amSyncJobTrigger(@Qualifier("amSyncJobDetail") JobDetail jobDetail,
//                                                     @Value("${amSyncJob.frequency}") long frequency) {
//        log.info("amSyncJobTrigger start true, frequency: " + frequency);
//        return createTrigger(jobDetail, frequency);
//    }


    // ----
    private static JobDetailFactoryBean createJobDetail(Class jobClass) {
        JobDetailFactoryBean factoryBean = new JobDetailFactoryBean();
        factoryBean.setJobClass(jobClass);
        // job has to be durable to be stored in DB:
        factoryBean.setDurability(true);
        factoryBean.setRequestsRecovery(true);

        return factoryBean;
    }

    private static SimpleTriggerFactoryBean createTrigger(JobDetail jobDetail, long pollFrequencyMs) {
        SimpleTriggerFactoryBean factoryBean = new SimpleTriggerFactoryBean();
        factoryBean.setJobDetail(jobDetail);
        factoryBean.setStartDelay(0L);
        factoryBean.setRepeatInterval(pollFrequencyMs);
        factoryBean.setRepeatCount(SimpleTrigger.REPEAT_INDEFINITELY);

        // in case of misfire, ignore all missed triggers and continue :
        factoryBean.setMisfireInstruction(SimpleTrigger.MISFIRE_INSTRUCTION_RESCHEDULE_NEXT_WITH_REMAINING_COUNT);
        return factoryBean;
    }

    // Use this method for creating cron triggers instead of simple triggers:
    private static CronTriggerFactoryBean createCronTrigger(JobDetail jobDetail, String cronExpression) {
        CronTriggerFactoryBean factoryBean = new CronTriggerFactoryBean();
        factoryBean.setJobDetail(jobDetail);
        factoryBean.setCronExpression(cronExpression);
        factoryBean.setMisfireInstruction(SimpleTrigger.MISFIRE_INSTRUCTION_FIRE_NOW);
        return factoryBean;
    }
}
