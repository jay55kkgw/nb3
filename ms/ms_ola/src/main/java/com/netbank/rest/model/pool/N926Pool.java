package com.netbank.rest.model.pool;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.netbank.rest.model.NewOneCallback;
import com.netbank.rest.model.UserPool;

import fstop.model.IMasterValuesHelper;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.model.RowFilterCallback;
import fstop.model.Rows;
import fstop.model.RowsSelecter;
import fstop.model.TelcommResult;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import fstop.util.StrUtils;

@Component
public class N926Pool extends UserPool {
	
	@Autowired
    @Qualifier("n926Telcomm")
	private TelCommExec n926Telcomm;
	
//	@Required
//	public void setN926Telcomm(TelCommExec telcomm) {
//		n926Telcomm = telcomm;
//	}	
//	
	/**
	 * 
	 * @param uid  使用者的身份證號
	 * @param acnotype  帳戶屬性
	 * 
	 * ‘10’ 黃金存摺帳號 
	 * 
	 * @return
	 */
	public MVH getAcnoList(final String idn, final String[] acnotype) {
		Assert.notEmpty(acnotype, "輸入的 type 不可為 空"); 
		final String uid = StrUtils.trim(idn).toUpperCase();
		if(uid.length() == 0)
			return new MVHImpl();

		final Set<String> accounts = new HashSet();
		Rows rows = new Rows();
		if(acnotype != null) {
			for(String act : acnotype) {
				if(StrUtils.isNotEmpty(act)) {
					String type = StrUtils.right("00" + StrUtils.trim(act), 2);

					Rows r  = (Rows)getPooledObject(uid ,"N926_" + type, getNewOneCallback(uid, type));
					for(int i=0; i < r.getSize(); i++) {
						
						//不重複加入相同的銀行別加帳號
						String acount = r.getRow(i).getValue("BNKCOD") + "_" + r.getRow(i).getValue("ACN");
						if(accounts.contains(acount))
							continue;
						accounts.add(acount);
						
						rows.addRow(r.getRow(i));
					}
				}
			}
		}

		return new MVHImpl(rows);
	}

	/**
	 * 將 身份證字號 為KEY 放置在POOL 的 N926 資料刪除
	 * @param idn
	 */
	public void removeFromPool(String idn) {
		super.removePoolObject(idn);
	}
	
	private NewOneCallback getNewOneCallback(final String uid, final String type) {
		
		NewOneCallback callback = new NewOneCallback() {
	
			public Object getNewOne() {
	
				Hashtable params = new Hashtable();
				params.put("CUSIDN", uid);
				Date d = new Date();
				String df = DateTimeUtils.getCDateShort(d);
				String tf = DateTimeUtils.format("HHmmss", d);
				params.put("DATE", df);
				params.put("TIME", tf);
				params.put("TYPE", type);
				
				TelcommResult mvh = n926Telcomm.query(params);
				
			    //VALUE為行庫別及帳號的組合，如[050,12345678901]
			    //TEXT為行庫別、行庫名稱及帳號的組合，如[050-臺灣企銀 12345678901]
				Rows result = new Rows();
				result.addRows(mvh.getOccurs());		
				
				return result;								
			}
			
		};
		
		return callback;
	}
	
}
