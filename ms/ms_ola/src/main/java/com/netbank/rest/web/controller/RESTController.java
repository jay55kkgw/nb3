package com.netbank.rest.web.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.netbank.rest.service.N110_Tel_Service;
import com.netbank.rest.service.REST_Service;
import com.netbank.util.ESAPIUtil;
import com.netbank.util.StrUtils;
import fstop.model.MVH;
import fstop.services.CommonService;
import fstop.util.JSONUtils;
import fstop.util.SpringBeanFactory;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang.StringEscapeUtils;
import org.owasp.esapi.SafeFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Optional;
import java.util.Properties;

/**
 *
 */
@RestController
@RequestMapping(value = "/com")
@Slf4j
public class RESTController {

    @Autowired
    ServletContext servletContext;

    @Autowired
    REST_Service rest_service;
    
	@Value("${sms_url}")
	private String smsurl;
	@Value("${sms_username}")
	private String smsusername;
	@Value("${sms_userpw}")
	private String smsuserpw;
    
    @PostConstruct
    public void init() {

        //註冊到 SpringBeanFactory
        SpringBeanFactory.setApplicationContext(WebApplicationContextUtils.getWebApplicationContext(servletContext));
        try {
	        Properties prop = new Properties();
	        Resource resource = new ClassPathResource("smsSetting.properties");
//	        InputStream in = RESTController.class.getClassLoader().getResourceAsStream("smsSetting.properties");
	        InputStream in = resource.getInputStream();
	        prop.load(in);
	        log.trace("rewrite url >> {}, username >> {}, userpw >> {}",smsurl,smsusername,smsuserpw);
	        prop.setProperty("url", smsurl);
	        prop.setProperty("username", smsusername);
	        prop.setProperty("userpw", smsuserpw);
	        
	        File pf = new SafeFile(resource.getURI().getPath());
			pf.setWritable(true, true);
			pf.setReadable(true, true);
			pf.setExecutable(true, true);
	        
	        BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(pf));
	        
	        prop.store(outputStream, null);
	        outputStream.close();
        }catch(Exception e) {
        	log.error("{}",e);
        }
    }


    @RequestMapping(value = "/{serviceID}", method = {RequestMethod.POST},
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public HashMap<String,Object> doAction(@PathVariable String serviceID, @RequestBody HashMap request) throws JsonProcessingException {

        log.info(ESAPIUtil.vaildLog("service id: {} - request: {}"+ serviceID+" - "+ JSONUtils.toJson(request)));
        
        // Reflected XSS All Clients
        String serviceId = StringEscapeUtils.escapeHtml(serviceID);
        
        return rest_service.process(serviceId, request);
        

    }

}
