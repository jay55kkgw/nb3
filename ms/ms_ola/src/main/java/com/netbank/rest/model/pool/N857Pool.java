package com.netbank.rest.model.pool;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.netbank.rest.model.UserPool;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class N857Pool extends UserPool {

	@Autowired
	@Qualifier("n857Telcomm")
	private TelCommExec n857Telcomm;

	/**
	 * @param
	 * @return
	 */
	public MVH doAction(Map _params) {

		Map<String, String> params = _params;

		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);

		params.put("DATE", df);
		params.put("TIME", tf);

		log.debug("N857 doAction params = " + params);

		TelcommResult mvh = n857Telcomm.query(params);

		return mvh;
	}
}
