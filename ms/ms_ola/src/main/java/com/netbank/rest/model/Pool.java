package com.netbank.rest.model;

import fstop.util.SpringBeanFactory;
import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;

public class Pool {
	
	/*
	 * 要等到 Spring 的環境都起來後, 才有辦法使用 SpringBeanFactory
	 */
	private Cache getCache() {
		return (Cache)SpringBeanFactory.getBean("sysObjectCache");
	}
	
	public Object getPooledObject(String poolkey, NewOneCallback newone) {
		Cache cache = getCache();
		Element elm = cache.get(poolkey);
		if(elm != null) {
			
			return elm.getValue();
		}
		else {
			Object o = newone.getNewOne();
			cache.put(new Element(poolkey, o));
			return o;
		}
	}

	public void removePoolObject(String poolkey) {
		Cache cache = getCache();
		cache.remove(poolkey);
	}
	
	
}
