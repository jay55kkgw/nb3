/**
 * 
 */
package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Map;

import fstop.model.MVH;

/**
 * 
 * 功能說明 :Tel_Service介面
 *
 */
public interface Tel_Service {

	HashMap pre_Processing(Map request);
	HashMap process(String serviceID,Map request);
	HashMap data_Retouch(MVH mvh);
	void setMsgCode(Map dataMap,Boolean data_Retouch_result);
}
