package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Optional;

import com.netbank.util.StrUtils;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.services.CommonService;
import fstop.util.SpringBeanFactory;
import lombok.extern.slf4j.Slf4j;
@Slf4j
public abstract class Common_REST_Service implements Tel_Service{

	@Override
	public HashMap pre_Processing(Map request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HashMap process(String serviceID ,Map request) {
		MVH mvh = null;
		try {
			CommonService service = (CommonService)SpringBeanFactory.getBean(
			        String.format("%sService",
			                Optional.ofNullable(serviceID)
			                    .filter(s -> StrUtils.isNotEmpty(s)).map(s -> s.toLowerCase())
			                    .orElse(null)));
			 mvh = service.doAction(request);
		} catch (Exception e) {
			log.error("process.error>>{}",e);
		}finally {
//			TODO true 做data_Retouch false 做msgCode 回覆處理
			return data_Retouch(mvh);
		}
		
	}

	@Override
	public HashMap data_Retouch(MVH mvh) {
		MVHImpl mvhimpl = null;
		LinkedList<Row> tmplist = null;
		Boolean result = Boolean.FALSE;
		HashMap<String,Object> dataMap = new HashMap<String,Object>();
		HashMap<String,String> rowMap = new HashMap<String,String>();
		LinkedList<HashMap<String,String>> rowlist = null;
    	try {
    		mvhimpl = (MVHImpl) mvh;
    	log.info("getFlatValues>>"+mvhimpl.getFlatValues());
    	log.info("result.getInputQueryData()>>"+mvhimpl.getInputQueryData());
    	log.info("result.getInputQueryData()>>"+mvhimpl.getOccurs().getRows());
			Iterator<Row> tmp = mvhimpl.getOccurs().getRows().iterator();
			tmplist = new LinkedList<Row>();
			rowlist = new LinkedList<HashMap<String,String>>();
			tmp.forEachRemaining(tmplist::add);
			
			log.info("datalist()>>"+tmplist);
			for(Row  row:tmplist) {
				log.info("getValues>>"+row.getValues());
				rowMap = new HashMap<String,String>();
				rowMap.putAll(row.getValues());
				rowlist.add(rowMap);
			}
			dataMap.putAll(mvhimpl.getFlatValues());
			dataMap.put("REC", rowlist);
			result = Boolean.TRUE;
			
		} catch (Exception e) {
			log.error("data_Retouch error >> {}",e);
		}finally {
			if(result) {
				dataMap.put("msgCode", "0");
			}else {
				dataMap.put("msgCode", "FE0002");
			}
			
			
		}
		return dataMap;
	}

	
	
	
}
