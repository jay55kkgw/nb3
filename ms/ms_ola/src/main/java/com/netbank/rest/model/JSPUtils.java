package com.netbank.rest.model;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
//import org.w3c.tidy.*;
//import webBranch.utility.AsianFontProvider;
//import com.itextpdf.text.PageSize;
//import com.itextpdf.text.Document;
//import com.itextpdf.text.DocumentException;
//import com.itextpdf.text.Font;
//import com.itextpdf.text.pdf.BaseFont;
//import com.itextpdf.text.pdf.PdfWriter;
//import com.itextpdf.tool.xml.XMLWorkerHelper;

import fstop.model.IMasterValuesHelper;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class JSPUtils {

//	public static String toString2(String includeJsp, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
//       
//		BufferedHttpResponseWrapper wrapper = new BufferedHttpResponseWrapper(response);
//		ServletContext context = request.getSession().getServletContext();
//		String url = response.encodeRedirectURL("/" + includeJsp);
//		RequestDispatcher dispatcher = context.getRequestDispatcher(url);
//		//response.setCharacterEncoding("UTF-8");
//		dispatcher.include(request, wrapper);
//		String result = new String(wrapper.getOutput());
//
//        return result;
//	}
	
	public static void clearUserSession(HttpSession session) throws IOException, ServletException {
	       	
        Enumeration sessionNames = session.getAttributeNames();
        while (sessionNames.hasMoreElements()) {
             String sessionName = (String)sessionNames.nextElement();
             Object sessionValue = session.getAttribute(sessionName);
             
             if (sessionValue instanceof Hashtable)                  
            	 session.removeAttribute(sessionName);             
             else if (sessionValue instanceof IMasterValuesHelper)
            	 session.removeAttribute(sessionName);              	
             
        }//end while		
        
        session.removeAttribute("LASTTRANCODE");   
	}
	
	 //轉換全型半型 option 0: toHalf, 1:toFull
 public static String convertFullorHalf(String originalStr, int option)
 {
   String[] asciiTable = {" ", "!", "\"", "#", "$", "%", "&", "'", "(", ")", "*", "+", ",", "-", ".", "/",
       "0", "1", "2", "3", "4", "5","6", "7", "8", "9",  ":", ";", "<", "=", ">", "?", "@",
       "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
       "[", "\\", "]", "^", "_", "`",
       "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
       "{", "|", "}", "~"};
   String[] big5Table = {"　", "！", "”", "＃", "＄", "％", "＆", "’", "（", "）", "＊", "＋", "，", "－", "\u2027", "／",
       "０", "１", "２", "３", "４", "５","６", "７", "８", "９",  "：", "；", "＜", "＝", "＞", "？", "＠",
       "Ａ", "Ｂ", "Ｃ", "Ｄ", "Ｅ", "Ｆ", "Ｇ", "Ｈ", "Ｉ", "Ｊ", "Ｋ", "Ｌ", "Ｍ", "Ｎ", "Ｏ", "Ｐ", "Ｑ", "Ｒ", "Ｓ", "Ｔ", "Ｕ", "Ｖ", "Ｗ", "Ｘ", "Ｙ", "Ｚ",
       "〔", "\uFE68", "〕", "︿", "＿", "\uFF40",
       "ａ", "ｂ", "ｃ", "ｄ", "ｅ", "ｆ", "ｇ", "ｈ", "ｉ", "ｊ", "ｋ", "ｌ", "ｍ", "ｎ", "ｏ", "ｐ", "ｑ", "ｒ", "ｓ", "ｔ", "ｕ", "ｖ", "ｗ", "ｘ", "ｙ", "ｚ",
       "｛", "｜", "｝", "\uFF5E"};

   if(asciiTable.length == big5Table.length)
   {
     //log.debug("對照表長度正確！ 長度為： " + asciiTable.length);
     //開始轉換
     if(originalStr == null || "".equalsIgnoreCase(originalStr))
     {
       return "";
     }
     for(int i = 0 ; i < asciiTable.length; i++)
     {
       //印出對照表
       //log.debug((i + 1) + ":  " + asciiTable[i] + "\t" + big5Table[i]);
       //開始轉換
       if(option == 0)//to Half
       {
	 originalStr = originalStr.replace(big5Table[i].charAt(0), asciiTable[i].charAt(0));//不可以用replaceAll，會因regular expression出錯
       }
       if(option == 1)//to Full
       {
	 originalStr = originalStr.replace(asciiTable[i].charAt(0), big5Table[i].charAt(0));//不可以用replaceAll，會因regular expression出錯
       }
     }
     return originalStr;
   }
   else
   {
     //log.debug("對照表長度不正確！ asciiTable長度為： " + asciiTable.length);
     //log.debug("對照表長度不正確！ big5Table長度為： " + big5Table.length);
     return originalStr;
   }
 }
 /**
  * 遮蔽姓名：二個字遮第二個字，超過二個字保留頭尾，中間遮掉
  * @author Owner
  *  
  */
 public static String hideusername(String originalStr)  
 {
  String header="";
  String middle="";
  String tailer="";
  String newstr="";
  String originalStr_1=convertFullorHalf(originalStr.replace((char) 12288, ' ').trim(),1);
  if(originalStr_1.length()>0)
  {	  
	  if(originalStr_1.length()==2)
	  {	  
		  header=originalStr_1.substring(0,1);
		  middle="０";
		  newstr="";
	  }
	  else if(originalStr_1.length()==3)
	  {
		  header=originalStr_1.substring(0,1);
		  middle="０";
		  tailer=originalStr_1.substring(2,3);	  
	  }
	  else
	  {
		  header=originalStr_1.substring(0,1);
		  for(int i=0;i<originalStr_1.length()-2;i++)
		  {
			  middle=middle+"０";
		  }	  
		  tailer=originalStr_1.substring(originalStr_1.length());	 	  
	  }
  }
  newstr=header+middle+tailer;
  return newstr;
 }
 /**
  * 遮蔽帳號：遮7、8、9碼(含信託帳號)
  * @author Owner
  *  
  */ 
 public static String hideaccount(String originalStr)
 {
  String header="";
  String middle="";
  String tailer="";
  String newstr="";
  if(originalStr.length()>0)
  {
	  if(originalStr.length()==9)
	  {	  
		  header=originalStr.substring(0,6);
		  middle="***";
		  tailer="";	 	  
	  }else if(originalStr.length()>9)
	  {
		  header=originalStr.substring(0,6);
		  middle="***";
		  tailer=originalStr.substring(9);	  
	  }	  
  }
  newstr=header+middle+tailer;  
  return newstr;
 } 
 /**
  * 遮蔽身分證號/統編：遮末三碼
  * @author Owner
  *  
  */ 
 public static String hideid(String originalStr)
 {
  String header="";
  String middle="";
  String tailer="";
  String newstr="";
  if(originalStr.length()>0)
  {
	  if(originalStr.length()>3)
	  {	  
		  header=originalStr.substring(0,originalStr.length()-3);
		  tailer="***";	 	  
	  }
  }
  newstr=header+tailer;  
  return newstr;
 }  
 /**
  * 遮蔽信用卡號：遮第14及第15碼
  * @author Owner
  *  
  */ 
 public static String hidecardnum(String originalStr)
 {
  String header="";
  String middle="";
  String tailer="";
  String newstr="";
  if(originalStr.length()>0)
  {
	  if(originalStr.length()>=16)
	  {	  
		  header=originalStr.substring(0,originalStr.length()-3);
		  middle="**";
		  tailer=originalStr.substring(originalStr.length()-1);
	  }
  }
  newstr=header+middle+tailer;  
  return newstr;
 }
 /**
  * 配合字霸作特別處理
  * 遮蔽姓名：二個字遮第二個字，超過二個字保留頭尾，中間遮掉
  * @author Owner
  *  
  */
 public static String hideusername1(String originalStr)  
 {
  String header="";
  String middle="";
  String tailer="";
  String newstr="";
  int index1 = originalStr.indexOf("<img") , index2 = originalStr.lastIndexOf("<img"), index3 = originalStr.indexOf("</img>") , index4 = originalStr.lastIndexOf("</img"), count = 0;
  int len= ((index4+6)-index2);
  String indexstr="";
  if(index1 >= 0){
	  count++;
	  indexstr=String.valueOf(index1);	  
	  while(index1 <= index2){
		  //log.debug(index1);
		  index1 += len;
		  index1 = originalStr.indexOf("<img",index1);
		  if(index1!=-1) {
			  count++;
			  indexstr+=","+index1;
		  }
		  else {
			  break;
		  }

	  }
	  log.debug("Find <img" + " : " + count + " times!");
  }
  else{
	  log.debug("<img 找不到!");
  }
  int chinesenum=(originalStr.length()-(len*count))+count;
  index1 = originalStr.indexOf("<img");
  index2 = originalStr.lastIndexOf("<img");
  index3 = originalStr.indexOf("</img>");
  index4 = originalStr.lastIndexOf("</img");
  int endlen=index1+len;
  if(index1 == 0){
	  if(chinesenum==2)
	  {	  
		  header=originalStr.substring(0,endlen);
		  middle="０";
		  newstr="";
	  }
	  else if(chinesenum==3)
	  {
		  header=originalStr.substring(0,endlen);
		  middle="０";
		  tailer=originalStr.substring(endlen+1,endlen+2);	  
	  }
	  else
	  {
		  header=originalStr.substring(0,endlen);
		  for(int i=0;i<chinesenum-2;i++)
		  {
			  middle=middle+"０";
			  endlen+=1;
		  }	  
		  tailer=originalStr.substring(endlen);	 	  
	  }
	  newstr=header+middle+tailer;
  }
  else if(index1>0) {
	  if(chinesenum==2)
	  {	  
		  header=originalStr.substring(0,1);
		  middle="０";
		  newstr="";
	  }
	  else if(chinesenum==3)
	  {
		  header=originalStr.substring(0,1);
		  if(index1==1) {
			  middle="０";
			  tailer=originalStr.substring(endlen);
		  }else {
			  middle="０";
			  tailer=originalStr.substring(index1);
		  }
	  }
	  else
	  {
		  header=originalStr.substring(0,1);
		  if(index2==originalStr.length()-len) {
			  for(int i=0;i<chinesenum-2;i++)
			  {
				  middle=middle+"０";
			  }			  
			  tailer=originalStr.substring(index2);
		  }else {
			  for(int i=0;i<chinesenum-2;i++)
			  {
				  middle=middle+"０";
			  }			  
			  tailer=originalStr.substring(originalStr.length()-1);			  
		  }
	  }
	  newstr=header+middle+tailer;
  }
  else{
	  String originalStr_1=convertFullorHalf(originalStr.replace((char) 12288, ' ').trim(),1);
	  if(originalStr_1.length()>0)
	  {	  
		  if(originalStr_1.length()==2)
		  {	  
			  header=originalStr_1.substring(0,1);
			  middle="０";
			  newstr="";
		  }
		  else if(originalStr_1.length()==3)
		  {
			  header=originalStr_1.substring(0,1);
			  middle="０";
			  tailer=originalStr_1.substring(2,3);	  
		  }
		  else
		  {
			  header=originalStr_1.substring(0,1);
			  for(int i=0;i<originalStr_1.length()-2;i++)
			  {
				  middle=middle+"０";
			  }	  
			  tailer=originalStr_1.substring(originalStr_1.length());	 	  
		  }
	  }
	  newstr=header+middle+tailer;	  
  }    

  return newstr;
 } 
 public static String parseXhtml(String f_in){

     ByteArrayInputStream stream = new ByteArrayInputStream(f_in.getBytes());
     ByteArrayOutputStream  tidyOutStream = new ByteArrayOutputStream();
//     Tidy tidy = new Tidy();
//     tidy.setInputEncoding("BIG5");
//     tidy.setQuiet(true);
//     tidy.setOutputEncoding("CP950");
//     tidy.setShowWarnings(false);
//     tidy.setIndentContent(true);
//     tidy.setSmartIndent(true);
//     tidy.setIndentAttributes(false);
//     tidy.setPrintBodyOnly(true);
//     tidy.setWraplen(1024);
//     tidy.setXHTML(true);
//     tidy.setMakeClean(true);
//     tidy.setErrout(new PrintWriter(System.out));
//     tidy.parse(stream, tidyOutStream);
     return tidyOutStream.toString();
}
 public static boolean CreatePdf(String inputhtmlstr,String OutputPathFilename){
      boolean status=false;
      //TODO:JSPUtils使用有待商確，故先將有問題部分註解
//	  try {
//		  // step 1
//		  Document document = new Document(PageSize.A4);
//		  // step 2
//		  PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(OutputPathFilename));
//		  // step 3
//		  document.open();
//		  // step 4
//		  String xhtmlstr=parseXhtml(inputhtmlstr);
//		  log.debug("xhtmlstr:"+xhtmlstr);
//		  XMLWorkerHelper.getInstance().parseXHtml(writer, document, new ByteArrayInputStream(xhtmlstr.getBytes()), null,new AsianFontProvider());
//		  // step 5
//	      document.close();
//	      status=true;
//	  }catch(Exception e)
//	  {
//		  log.debug(e.getMessage());
//		  status=false;
//	  } 
	  return status;
} 
 /**
  * 遮蔽密碼：遮第1碼及最末碼
  * @author Owner
  *  
  */ 
 public static String hidepasswd(String originalStr)
 {
  String header="";
  String middle="";
  String tailer="";
  String newstr="";
  if(originalStr.length()>=3)
  {
		  header="*";
		  middle=originalStr.substring(1,originalStr.length()-1);
		  tailer="*";
  }
  newstr=header+middle+tailer;  
  return newstr;
 } 
}
