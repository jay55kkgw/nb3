package com.netbank.rest.configuration;

import com.netbank.telcomm.CommonTelcomm;
import com.netbank.telcomm.MockFileDataCommonTelcomm;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.io.IOException;

@Configuration
@Slf4j
@ConditionalOnProperty(name = "mock.telcomm.datasource", havingValue = "FILEDATA")
public class TelcommFileDataConfig extends AbstractTelcommConfig {
    @Value("${mock.telcomm.data.dir}")
    private String mockTelcommDataDir;

    protected CommonTelcomm createTelcomm(String mofileName, String txid) throws IOException {
        MockFileDataCommonTelcomm ret = new MockFileDataCommonTelcomm();

        String data = "";
        try {
            if(new File(mockTelcommDataDir, mofileName).exists())
                data = FileUtils.readFileToString(new File(mockTelcommDataDir, mofileName), "UTF-8");
        } catch(Exception e) {
            log.error("無法讀取 MOCK 資料(file: " + new File(mockTelcommDataDir, mofileName).getAbsolutePath() + ")", e);
        }
        ret.setTxid(txid.toUpperCase());
        ret.setMockResult(data);
        return ret;
    }


}
