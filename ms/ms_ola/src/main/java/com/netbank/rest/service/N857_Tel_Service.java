package com.netbank.rest.service;

import java.util.HashMap;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netbank.rest.model.CommonPools;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import lombok.extern.slf4j.Slf4j;

/**
 * N997POOL
 */
@Service
@Slf4j
public class N857_Tel_Service extends Common_Tel_Service {

	@Autowired
	private CommonPools commonPools;

	@Override
	public HashMap pre_Processing(Map request) {
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) {
		MVH mvh = new MVHImpl();
		mvh = commonPools.n857.doAction(request);
		
		return data_Retouch(mvh);
	}

	@Override
	public HashMap data_Retouch(MVH mvh) {
		HashMap<String,Object> dataMap = new HashMap<String,Object>();
		dataMap.put("msgCode","0000");

		return dataMap;
	}

	@Override
	public void setMsgCode(Map dataMap, Boolean data_Retouch_result) {
		super.setMsgCode(dataMap, data_Retouch_result);
	}
}
