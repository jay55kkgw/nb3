package fstop.services.mgn;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.Auditable;
import fstop.services.DispatchService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 分行外匯連絡人檔維護
 * @author Owner
 *
 */
@Slf4j
public class NA20 extends DispatchService implements Auditable {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("na20Telcomm")
	private TelCommExec na20Telcomm;
	
//	@Required
//	public void setNa20Telcomm(TelCommExec telcomm) {
//		this.na20Telcomm = telcomm;
//	}
	
	@Override
	public String getActionMethod(Map<String, String> params) {
		Date d = new Date();
		
		params.put("LASTDATE", DateTimeUtils.format("yyyyMMdd", d));
		params.put("LASTTIME", DateTimeUtils.format("HHmmss", d));
		
		return "query";
	}
	
	@SuppressWarnings("unchecked")
	public MVH query(Map<String, String> params) {
		
		//身份證或統編轉大寫
		params.put("CUSIDN", params.get("CUSIDN").toUpperCase());
		
		TelcommResult qresult = na20Telcomm.query(params);
				
		qresult.getFlatValues().put("CMQTIME", DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", new Date()));
		
		qresult.dump();
		
		return qresult;
	}

}
