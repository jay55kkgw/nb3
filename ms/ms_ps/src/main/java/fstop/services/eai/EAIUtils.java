package fstop.services.eai;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.util.HtmlUtils;

import com.google.gson.Gson;
import com.netbank.rest.model.JSPUtils;
import com.netbank.rest.service.Verify_mail_cancel_Tel_Service;
import com.netbank.rest.service.Verify_mail_send_Tel_Service;
import com.netbank.rest.util.RESTUtil;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.notification.Notification;
import fstop.notification.PushData;
import fstop.notification.TopicLabel;
import fstop.notifier.PushAgent;
import fstop.orm.dao.TxnPhoneTokenDao;
import fstop.orm.po.TXNPHONETOKEN;
import fstop.util.JSONUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.thread.ThreadSemaphore;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class EAIUtils {

	@Autowired
	private TxnPhoneTokenDao txnPhoneTokenDao;
	
	@Autowired
	Verify_mail_send_Tel_Service verify_mail_send_Tel_Service;
	
	@Autowired
	Verify_mail_cancel_Tel_Service verify_mail_cancel_Tel_Service;
	
	@Autowired
	private RESTUtil restUtil;

	@Value("${firebase_useflag}")
	private String firebase_useflag;
	
	@Value("${mstw_url}")
	private String mstw_url;

	public String executeN186(String data) {
		log.info("==EAI BlueStar TITA XML ===============\r\n" + data + "\r\n===============");
		String retxml = send_N186(data);
		return retxml;
	}
	
	
	public String executeN221(String data) {
		log.info("==EAI_N221 TITA XML ===============\r\n" + data + "\r\n===============");
		data = HtmlUtils.htmlUnescape(data);
		String retxml = do_N221(data);
		return retxml;
	}

	private String send_N186(String sendData) {

		N186in n186in = new N186in();
		String ACN = "";
		String AMOUNT = "";
		String CUSIDN = "";
		String DATTRN = "";
		String ECMARK = "";
		String INPCST = "";
		String ITEM = "";
		String TIMTRN = "";

		Pattern pattern = Pattern.compile("\\s*>([^>]+?)</ACN>");
		Matcher matcher = pattern.matcher(sendData);
		if (matcher.find()) {
			ACN = matcher.group(1);
		}
		pattern = Pattern.compile("\\s*>([^>]+?)</AMOUNT>");
		matcher = pattern.matcher(sendData);
		if (matcher.find()) {
			AMOUNT = matcher.group(1);
		}
		pattern = Pattern.compile("\\s*>([^>]+?)</CUSIDN>");
		matcher = pattern.matcher(sendData);
		if (matcher.find()) {
			CUSIDN = matcher.group(1);
		}
		pattern = Pattern.compile("\\s*>([^>]+?)</DATTRN>");
		matcher = pattern.matcher(sendData);
		if (matcher.find()) {
			DATTRN = matcher.group(1);
		}
		pattern = Pattern.compile("\\s*>([^>]+?)</ECMARK>");
		matcher = pattern.matcher(sendData);
		if (matcher.find()) {
			ECMARK = matcher.group(1);
		}
		pattern = Pattern.compile("\\s*>([^>]+?)</INPCST>");
		matcher = pattern.matcher(sendData);
		if (matcher.find()) {
			INPCST = matcher.group(1);
		}
		pattern = Pattern.compile("\\s*>([^>]+?)</ITEM>");
		matcher = pattern.matcher(sendData);
		if (matcher.find()) {
			ITEM = matcher.group(1);
		}
		pattern = Pattern.compile("\\s*>([^>]+?)</TIMTRN>");
		matcher = pattern.matcher(sendData);
		if (matcher.find()) {
			TIMTRN = matcher.group(1);
		}

		n186in.setACN(ACN);
		n186in.setAMOUNT(AMOUNT);
		n186in.setCUSIDN(CUSIDN);
		n186in.setDATTRN(DATTRN);
		n186in.setECMARK(ECMARK);
		n186in.setINPCST(INPCST);
		n186in.setITEM(ITEM);
		n186in.setTIMTRN(TIMTRN);

		return send_msg(n186in);
	}

	private String send_msg(N186in params) {
		log.info("==N186.send() start===");
		N186out result = new N186out();

		String token = null;
		String pushtext = null;
		List<PushData> listPushData = new ArrayList<PushData>();
		pushtext = "親愛的客戶：提醒您有一筆款項入帳（帳號：" + JSPUtils.hideaccount(params.getACN())
		+ "）。本通知僅供參考，不作為交易憑證，實際入帳情形請查詢存款帳戶。";
		Map<String, String> reqMap = new HashMap<String, String>();
		reqMap.put("CUSIDN" , params.getCUSIDN());
		reqMap.put("pushtext" , pushtext);
		log.info("reqMap={}",CodeUtil.toJson(reqMap));
		Map<String, Object> res = new HashMap<String, Object>();
		Gson gson = new Gson();
		String rs = null;
		String url = mstw_url+"/NotifyApi";
		try {
			TXNPHONETOKEN txnphonetoken = txnPhoneTokenDao.getPhoneToken(params.getCUSIDN());
			if (txnphonetoken != null) {
				rs = restUtil.send(reqMap, url, 90);
				log.info("rs>>>{}",rs);
				res = gson.fromJson(rs, Map.class);
				result.setMsgCode((String) res.get("msgCode"));
			} else {
				result.setMsgCode("-2");
				result.setMsgName("此帳號尚未註冊推播服務");
				log.debug("WsN186Impl CUSIDN = " + params.getCUSIDN() + " not register to notification service");
			}
		} catch (Exception e) {
			result.setMsgCode("ZX99");
			result.setMsgName("");
			log.error("WsN186Impl Exception:" + e.getMessage());
		}

		String returnString = result.getMsgCode();

		return returnString;
	}
	
	
	private String do_N221(String inputData) {

		String BRHCOD = "";
		String TLRNUM = "";
		String TRNNUM = "";
		String CUSIDN = "";
		String TYPE = "";
		String O_MAILADDR = "";
		String N_MAILADDR = "";
		String RDATE = "";
		
		Map<String,String> apiParam = new HashMap<String,String>();

		Pattern pattern = Pattern.compile("\\s*>([^>]+?)</BRHCOD>");
		Matcher matcher = pattern.matcher(inputData);
		if (matcher.find()) {
			BRHCOD = matcher.group(1);
		}
		pattern = Pattern.compile("\\s*>([^>]+?)</TLRNUM>");
		matcher = pattern.matcher(inputData);
		if (matcher.find()) {
			TLRNUM = matcher.group(1);
		}
		pattern = Pattern.compile("\\s*>([^>]+?)</TRNNUM>");
		matcher = pattern.matcher(inputData);
		if (matcher.find()) {
			TRNNUM = matcher.group(1);
		}
		pattern = Pattern.compile("\\s*>([^>]+?)</CUSIDN>");
		matcher = pattern.matcher(inputData);
		if (matcher.find()) {
			CUSIDN = matcher.group(1);
		}
		pattern = Pattern.compile("\\s*>([^>]+?)</TYPE>");
		matcher = pattern.matcher(inputData);
		if (matcher.find()) {
			TYPE = matcher.group(1);
		}
		pattern = Pattern.compile("\\s*>([^>]+?)</O_MAILADDR>");
		matcher = pattern.matcher(inputData);
		if (matcher.find()) {
			O_MAILADDR = matcher.group(1);
		}
		
		pattern = Pattern.compile("\\s*>([^>]+?)</N_MAILADDR>");
		matcher = pattern.matcher(inputData);
		if (matcher.find()) {
			N_MAILADDR = matcher.group(1);
		}
		
		pattern = Pattern.compile("\\s*>([^>]+?)</RDATE>");
		matcher = pattern.matcher(inputData);
		if (matcher.find()) {
			RDATE = matcher.group(1);
		}
		
		apiParam.put("BRHCOD",BRHCOD);
		apiParam.put("TLRNUM",TLRNUM);
		apiParam.put("TRNNUM",TRNNUM);
		apiParam.put("CUSIDN",CUSIDN);
		apiParam.put("TYPE",TYPE);
		apiParam.put("O_MAILADDR",O_MAILADDR);
		apiParam.put("N_MAILADDR",N_MAILADDR);
		apiParam.put("RDATE",RDATE);
		
		apiParam.put("CHANNEL","CT");
		
		log.debug("GET PARAM : BRHCOD:"+BRHCOD+", TLRNUM:"+TLRNUM+", TRNNUM:"+TRNNUM+", CUSIDN:"+CUSIDN+", TYPE:"+TYPE+", O_MAILADDR:"+O_MAILADDR+", N_MAILADDR:"+N_MAILADDR+", RDATE:"+RDATE);

		//
		switch (TYPE) {
		case "A":
			//應該沒有此狀態 新增 >> 如果臨櫃新增成功  不用告知網銀發信
			break;
		case "U":
			//應該沒有此狀態 修改 >> 對網銀而言 , 如果他又修改了重複的mail , 只要給我O的狀態即可
			break;
		case "D":
			//註銷 這是要給網銀的 , 要更新最新那封驗證信的狀態 , 讓前臺首頁不顯示提示 , 讓批次不跑寄送過期 
			verify_mail_cancel_Tel_Service.process("verify_mail_cancel", apiParam);
			break;
		case "O":
			//啟用 這要給我 , 要去用身分證字號取主表資料 發EMAIL
			verify_mail_send_Tel_Service.process("verify_mail_send",apiParam);
			break;
		}

		return "0";
	}
	
	
}
