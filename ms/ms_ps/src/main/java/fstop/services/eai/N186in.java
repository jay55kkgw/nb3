package fstop.services.eai;

public class N186in {
	
	
	private String CUSIDN;	// 身分證統一編號
	private String ITEM;	// 交易項目
	private String DATTRN;	// 交易日期
	private String TIMTRN;	// 交易時間
	private String ACN;		// 入帳帳號
	private String AMOUNT;	// 轉帳金額
	private String INPCST;	// 資料內容
	private String ECMARK;	// 更正註記
	
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getITEM() {
		return ITEM;
	}
	public void setITEM(String iTEM) {
		ITEM = iTEM;
	}
	public String getDATTRN() {
		return DATTRN;
	}
	public void setDATTRN(String dATTRN) {
		DATTRN = dATTRN;
	}
	public String getTIMTRN() {
		return TIMTRN;
	}
	public void setTIMTRN(String tIMTRN) {
		TIMTRN = tIMTRN;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getAMOUNT() {
		return AMOUNT;
	}
	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}
	public String getINPCST() {
		return INPCST;
	}
	public void setINPCST(String iNPCST) {
		INPCST = iNPCST;
	}
	public String getECMARK() {
		return ECMARK;
	}
	public void setECMARK(String eCMARK) {
		ECMARK = eCMARK;
	}
	
}
