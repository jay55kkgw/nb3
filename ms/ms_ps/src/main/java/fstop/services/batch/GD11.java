package fstop.services.batch;

import fstop.exception.TopMessageException;
import fstop.exception.UncheckedException;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.orm.dao.AdmHolidayDao;
import fstop.orm.dao.GoldPriceDao;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.po.ADMHOLIDAY;
import fstop.orm.po.GOLDPRICE;
import fstop.orm.po.SYSBATCH;
import fstop.services.AfterSuccessQuery;
import fstop.services.batch.annotation.Batch;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.ESAPIUtil;

import java.util.*;

/**
 * 黃金存摺牌告資料查詢
 * 
 * @author Owner
 * 
 */
@Slf4j
@RestController
@Batch(id = "batch.gd11", name = "黃金存摺牌告資料查詢", description = "黃金存摺牌告資料查詢")
public class GD11 extends DispatchBatchExecute implements BatchExecute {
	// private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("gd11Telcomm")
	private TelCommExec gd11Telcomm;

	@Autowired
	private GoldPriceDao goldPriceDao;

	@Autowired
	private AdmHolidayDao admholidayDao;

	@Autowired
	private SysBatchDao sysBatchDao;

	/**
	 * args[0] method name
	 */
	@RequestMapping(value = "/batch/gd11", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {
		List<String> safeArgs = ESAPIUtil.validStrList(args);
		log.info("batch GD11...");
		log.info(ESAPIUtil.vaildLog("safeArgs: {}" + JSONUtils.toJson(safeArgs)));
		BatchResult batchresult = new BatchResult();
		Map<String, Object> data = new HashMap();
		batchresult.setBatchName(getClass().getSimpleName());
		batchresult.setSuccess(true);

		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}

		Map params = new Hashtable();
		Date d = new Date();
		String today = DateTimeUtils.getCDateShort(d);
		log.debug("DATE_S >> {} ", today);

		ADMHOLIDAY holiday = null;
		try {
			String holidaytoday = DateTimeUtils.getDateShort(d);
			log.debug("holidaytoday = " + holidaytoday);
			holiday = (ADMHOLIDAY) admholidayDao.findById(holidaytoday);
		} catch (Exception e) {
			holiday = null;
		}

		if (holiday != null) {

			data.put("Error", "非營業日");
			log.debug("Error >> 非營業日");
			batchresult.setSuccess(false);
			batchresult.setData(data);
			try {
				sysBatchDao.finish_FAIL2(batchName, batchresult.getData());
			} catch (Exception e) {
				log.error("sysBatchDao.finish_FAIL2 error >> {} ", e.getMessage());
			}

			return batchresult;
		}
		
		try {
			params.put("DATE_S", today);
			SimpleTemplate simpleTemplate = new SimpleTemplate(gd11Telcomm);
			log.info("setAfterSuccessQuery..");
			simpleTemplate.setAfterSuccessQuery(new AfterSuccessQuery() {

				public void execute(MVHImpl result) {
					if (goldPriceDao.findGoldPriceidIsExists(
							result.getValueByFieldName("DATE") + result.getValueByFieldName("TIME")) == true) {
						data.put("Error", "資料重複");
						log.debug("Error >> 資料重複");
						batchresult.setSuccess(false);
						batchresult.setData(data);
						return;
					}
					if (!"OKLR".equals(result.getValueByFieldName("TOPMSG"))) {
						throw TopMessageException.create((result.getValueByFieldName("TOPMSG")));
					}
					final GOLDPRICE t = new GOLDPRICE();

					t.setGOLDPRICEID(result.getValueByFieldName("DATE") + result.getValueByFieldName("TIME"));
					t.setQDATE(result.getValueByFieldName("DATE"));
					t.setQTIME(result.getValueByFieldName("TIME"));
					t.setBPRICE(result.getValueByFieldName("BPRICE"));
					t.setSPRICE(result.getValueByFieldName("SPRICE"));
					t.setPRICE1(result.getValueByFieldName("PRICE1"));
					t.setPRICE2(result.getValueByFieldName("PRICE2"));
					t.setPRICE3(result.getValueByFieldName("PRICE3"));
					t.setPRICE4(result.getValueByFieldName("PRICE4"));

					Date d = new Date();
					String today = DateTimeUtils.getDateShort(d);
					String time = DateTimeUtils.getTimeShort(d);
					log.debug("DATE = " + today);
					log.debug("TIME = " + time);

					t.setLASTDATE(today);
					t.setLASTTIME(time);
					goldPriceDao.save(t);
				}

			});
			simpleTemplate.setExceptionHandler(new ExceptionHandler() {

				public void handle(Row row, Exception e) {

					log.error("無法更新黃金存摺定期牌告.", e);
					// 新增SYSBATCH紀錄
					// sysBatchDao.update(SysBatchUtil.finish_FAIL(po,e.getMessage()));
				}

			});
			
		BatchCounter counter = simpleTemplate.execute(params);	
		} catch (TopMessageException e) {
			batchresult.setSuccess(false);
			data.put("TOPMSG", e.getMsgcode() + "  " + e.getMsgout());
			batchresult.setData(data);
			log.error("TopMessageException  !!", e);
		} catch (UncheckedException e) {
			batchresult.setSuccess(false);
			data.put("Error", e.getMessage());
			batchresult.setData(data);
			log.error("Exception 執行有誤 !!", e);
		}

		try {
			if (batchresult.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName,batchresult.getData());
			}
		}catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}

		return batchresult;

	}

}
