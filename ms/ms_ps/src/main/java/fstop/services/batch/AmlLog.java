package fstop.services.batch;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.notifier.NotifyAngent;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.SysParamDataDao;
import fstop.orm.dao.TxnAmlLogDao;
import fstop.orm.oao.Old_TxnAmlLogDao;
import fstop.orm.old.OLD_TXNAMLLOG;
import fstop.orm.po.SYSPARAMDATA;
import fstop.orm.po.TXNAMLLOG;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import fstop.util.FtpBase64;
import fstop.util.FtpPathProperties;
import fstop.util.JSONUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 每日產生上傳AML稽核軌跡記錄至 receive/AML_SYS_LOG_當日上傳年月日(yyyymmdd).TXT
 * @author Owner
 *
 */
@Slf4j
@RestController
@Batch(id="batch.AmlLog", name = "AML稽核軌跡記錄", description = " 每日產生上傳AML稽核軌跡記錄至 receive/AML_SYS_LOG_當日上傳年月日(yyyymmdd).TXT")
public class AmlLog  implements EmailUploadIf, BatchExecute {
	
	@Autowired
	private TxnAmlLogDao txnAmlLogDao;
	@Autowired
	private SysParamDataDao sysParamDataDao;
	
	@Autowired
	private Old_TxnAmlLogDao old_TxnAmlLogDao;
	
	private String ERRORMSG="";
	
	@Value("${isParallelCheck}")
	private String isParallelCheck;
	
	@Value("${doFTP}")
	private String doFTP;
	
	@Value("${crmn.ftp.ip}")
	private String crmn_ftp_ip;
	
	@Autowired
	private CheckServerHealth checkserverhealth;
	
	@Autowired
	private SysBatchDao sysBatchDao;
	
	@RequestMapping(value = "/batch/amlLog", method = {RequestMethod.POST},
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {
		List<String> safeArgs = ESAPIUtil.validStrList(args);
		int failcnt=0,okcnt=0,totalcnt=0;
		Hashtable rowcnt=new Hashtable();
		BatchResult result = new BatchResult();
		log.debug("INTO AML LOG BATCH ... ");
		
		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		}catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}
		Hashtable<String, String> params = new Hashtable();
		params.put("TXNIDT", DateTimeUtils.format("yyMMddHHmmss", new Date()));
		Date d = new Date();
		String today = DateTimeUtils.getCDateShort(d);
		log.debug(ESAPIUtil.vaildLog("Batch_Execute_Date = " + today));
		String str_StartDate = "";
		String str_EndDate = "";
		String DATEDATA = "";
		
		try {
			if (safeArgs.size() > 0) {
				str_StartDate = safeArgs.get(0);
			    str_EndDate = safeArgs.get(1);
			    DATEDATA=str_StartDate+str_EndDate;
				exec(rowcnt,DATEDATA);
			}
			else {
				exec(rowcnt,DATEDATA);
			}					
			failcnt=((Integer)rowcnt.get("FAILCNT")).intValue();
			okcnt=((Integer)rowcnt.get("OKCNT")).intValue();
			totalcnt=((Integer)rowcnt.get("TOTALCNT")).intValue();
			log.debug(ESAPIUtil.vaildLog("failcnt = " + failcnt));
			log.debug(ESAPIUtil.vaildLog("okcnt = " + okcnt));
			log.debug(ESAPIUtil.vaildLog("totalcnt = " + totalcnt));
			result.setSuccess(true);
			
		}
		catch(Exception e) {
			log.error("執行 Batch 有誤 !", e);
			ERRORMSG = e.getMessage();
			result.setSuccess(false);
		}
		
		BatchCounter bcnt=new BatchCounter();
		bcnt.setFailCount(failcnt);
		bcnt.setSuccessCount(okcnt);
		bcnt.setTotalCount(totalcnt);

		Map<String, Object> data = new HashMap();
		result.setBatchName(this.getClass().getSimpleName());
		data.put("COUNTER", bcnt);

	
		result.setData(data);
		
		Date startTime = new Date();
		HashMap<String, String> varmail = new HashMap<String,String>();
		varmail.put("SYSDATE", DateTimeUtils.format("yyyy/MM/dd", startTime));
		varmail.put("SYSTIME", DateTimeUtils.format("HH:mm:ss", startTime));
		varmail.put("SYSSTATUS", JSONUtils.toJson(data));
		varmail.put("ERRORMSG", ERRORMSG);	
		
		SYSPARAMDATA po = null;				
		po = sysParamDataDao.findById("NBSYS");
		String receivers = po.getADAPMAIL().replace(";", ",") ;	
		
		// call BillHunter
		boolean isSendSuccess = NotifyAngent.sendNotice("BATCH_AMLLOG_MAIL", varmail, receivers, "batch");

		log.info(ESAPIUtil.vaildLog("結束寄送批次作業結果通知: " + DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", startTime)));
		
		
		try {
			if (result.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL(batchName,ERRORMSG);
			}
		}catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}
		
		return result;
	}

	private void exec(Map rowcnt,String DATEDATA) throws IOException,Exception {
		
		Integer	totalcnt=0,failcnt=0,okcnt=0;
		Date d = new Date();
		//String today = DateTimeUtils.getDateShort(d);
		//today = today.substring(0, 8);

		FtpPathProperties nrcpro=new FtpPathProperties();
		String srcpath=nrcpro.getSrcPath("aml");
		//LocalTest
//		srcpath = "C:/Users/BenChien/Desktop/batchLocalTest/data/NNB_WLSR_SYS_LOG";
		log.debug(ESAPIUtil.vaildLog("srcpath = " + srcpath));
		String despath=nrcpro.getDesPath("aml");
		log.debug(ESAPIUtil.vaildLog("despath = " + despath));
		String strStartdate = "";
		String strEnddate = "";
		List<TXNAMLLOG> all=null;
		TXNAMLLOG record;
		String DATADATE="";
		if(DATEDATA.length()==0)
		{			
			//跑昨日資料
			java.util.Calendar calendar = java.util.Calendar.getInstance();   
			calendar.setTime(d);
			calendar.add(Calendar.DAY_OF_MONTH, -1);
			String yesterday = DateTimeUtils.getDateShort(calendar.getTime());
			yesterday = yesterday.substring(0, 8);	
			DATADATE = yesterday;
			yesterday=yesterday.substring(0,4)+"-"+yesterday.substring(4,6)+"-"+yesterday.substring(6,8)+"%";		
			log.warn(ESAPIUtil.vaildLog("getAmlLog yesterday:" + yesterday));
			
			all = txnAmlLogDao.getAmlLog(yesterday); 
			
			if("Y".equals(isParallelCheck)) {
				
				List<OLD_TXNAMLLOG> oldall = old_TxnAmlLogDao.getAmlLog(yesterday);
				log.debug("NB2.5 data size >> {} " , oldall.size());
				for(OLD_TXNAMLLOG oldeach:oldall) {
					TXNAMLLOG nnbConvert2nb3 = CodeUtil.objectCovert(TXNAMLLOG.class, oldeach);
					all.add(nnbConvert2nb3);
				}
			}
		}
		else
		{
			strStartdate = DATEDATA.substring(0,8);
			strStartdate = strStartdate.substring(0,4)+"-"+strStartdate.substring(4,6)+"-"+strStartdate.substring(6,8)+" 00:00:00";
			strEnddate = DATEDATA.substring(8);
			DATADATE = strEnddate;
			strEnddate = strEnddate.substring(0,4)+"-"+strEnddate.substring(4,6)+"-"+strEnddate.substring(6,8)+" 23:59:59";
			log.warn(ESAPIUtil.vaildLog("findByDateRange Sdate:" + strStartdate +"；Edate:"+ strEnddate));
			all = txnAmlLogDao.findByDateRange(strStartdate, strEnddate);
			
			if("Y".equals(isParallelCheck)) {
				List<OLD_TXNAMLLOG> oldall = old_TxnAmlLogDao.findByDateRange(strStartdate, strEnddate);
				log.debug("NB2.5 data size >> {} " , oldall.size());
				for(OLD_TXNAMLLOG oldeach:oldall) {
					TXNAMLLOG nnbConvert2nb3 = CodeUtil.objectCovert(TXNAMLLOG.class, oldeach);
					all.add(nnbConvert2nb3);
				}
			}
		}	
		log.warn(ESAPIUtil.vaildLog("File Name:"+srcpath + "." + DATADATE + ".TXT"));
		File tmpFile = new File(srcpath + "." + DATADATE + ".TXT");
		tmpFile.setWritable(true,true);
		tmpFile.setReadable(true, true);
		tmpFile.setExecutable(true,true);
		tmpFile.delete();
		OutputStream  fos = new FileOutputStream(tmpFile);
		BufferedWriter	out = new BufferedWriter(new OutputStreamWriter(fos, "BIG5"));
		
		for (int i=0; i < all.size(); i++) {
			   String paddingstr = "";
			   record = all.get(i);
			   String SOURCESYSTEM = record.getSOURCESYSTEM()==null ? "" : record.getSOURCESYSTEM();
			   for(int j=0;j<20-SOURCESYSTEM.length();j++)
			   {
				   paddingstr=paddingstr+" ";
			   }
			   SOURCESYSTEM+=paddingstr;
			   paddingstr="";
			   String SEARCHCODE = record.getSEARCHCODE()==null ? "" : record.getSEARCHCODE();
			   for(int j=0;j<20-SEARCHCODE.length();j++)
			   {
				   paddingstr=paddingstr+" ";
			   }		
			   SEARCHCODE+=paddingstr;
			   paddingstr="";
			   String BRANCHID = record.getBRANCHID()==null ? "" : record.getBRANCHID();
			   for(int j=0;j<3-BRANCHID.length();j++)
			   {
				   paddingstr=paddingstr+" ";
			   }	
			   BRANCHID+=paddingstr;
			   paddingstr="";
			   String BUSINESSUNIT = record.getBUSINESSUNIT()==null ? "" : record.getBUSINESSUNIT();
			   for(int j=0;j<3-BUSINESSUNIT.length();j++)
			   {
				   paddingstr=paddingstr+" ";
			   }	
			   BUSINESSUNIT+=paddingstr;
			   paddingstr="";
			   String ADOPID = record.getADOPID()==null ? "" : record.getADOPID();
			   for(int j=0;j<20-ADOPID.length();j++)
			   {
				   paddingstr=paddingstr+" ";
			   }	
			   ADOPID+=paddingstr;
			   paddingstr="";
			   String TS1 = record.getTS1()==null ? "" : record.getTS1();
			   for(int j=0;j<19-TS1.length();j++)
			   {
				   paddingstr=paddingstr+" ";
			   }	
			   TS1+=paddingstr;
			   paddingstr="";
			   String TS2 = record.getTS2()==null ? "" : record.getTS2();
			   for(int j=0;j<19-TS2.length();j++)
			   {
				   paddingstr=paddingstr+" ";
			   }	
			   TS2+=paddingstr;
			   paddingstr="";
			   String STATUS = record.getSTATUS()==null ? "" : record.getSTATUS();
			   for(int j=0;j<10-STATUS.length();j++)
			   {
				   paddingstr=paddingstr+" ";
			   }	
			   STATUS+=paddingstr;
			   paddingstr="";
			   String QUERY_CONTENT = record.getQUERY_CONTENT()==null ? "" : record.getQUERY_CONTENT();
			   int QUERY_LENGTH = 0;
			   Map<String, Object> QUERY_CONTENT_rtnMap = getStrlen(QUERY_CONTENT,"BIG5");
			   try {
				   QUERY_CONTENT = (String) QUERY_CONTENT_rtnMap.get("Big5Str");
				   QUERY_LENGTH = (int) QUERY_CONTENT_rtnMap.get("Big5StrLen");
				   log.warn(ESAPIUtil.vaildLog("QUERY_CONTENT STR:"+QUERY_CONTENT));
				   log.warn(ESAPIUtil.vaildLog("QUERY_CONTENT LENGTH:"+QUERY_LENGTH));
			   }catch(Exception e) {
				   log.warn(ESAPIUtil.vaildLog("count QUERY_CONTENT LENGTH FAIL:"+e.getMessage()));
			   }
			   for(int j=0;j<600-QUERY_LENGTH;j++)
			   {
				   paddingstr=paddingstr+" ";
			   }	
			   QUERY_CONTENT+=paddingstr;			   
			   paddingstr="";
			   String RESPOND_CONTENT = record.getRESPOND_CONTENT()==null ? "" : record.getRESPOND_CONTENT();
			   int RESPOND_LENGTH = 0;
			   Map<String, Object> RESPOND_CONTENT_rtnMap = getStrlen(RESPOND_CONTENT,"BIG5");
			   try {
				   RESPOND_CONTENT = (String) RESPOND_CONTENT_rtnMap.get("Big5Str");
				   RESPOND_LENGTH = (int) QUERY_CONTENT_rtnMap.get("Big5StrLen");
				   log.warn(ESAPIUtil.vaildLog("RESPOND_CONTENT STR:"+RESPOND_CONTENT));
				   log.warn(ESAPIUtil.vaildLog("RESPOND_CONTENT LENGTH:"+RESPOND_LENGTH));
			   }catch(Exception e) {
				   log.warn(ESAPIUtil.vaildLog("count RESPOND_CONTENT LENGTH FAIL:"+e.getMessage()));
			   }			   		   
			   for(int j=0;j<600-RESPOND_LENGTH;j++)
			   {
				   paddingstr=paddingstr+" ";
			   }	
			   RESPOND_CONTENT+=paddingstr;
			   
			   String f = SOURCESYSTEM +"|"+SEARCHCODE+"|"+BRANCHID+"|"+BUSINESSUNIT+"|"+ADOPID+"|"+TS1+"|"+TS2+"|"+STATUS+"|"+QUERY_CONTENT+"|"+RESPOND_CONTENT+"\r\n";

			   log.warn(ESAPIUtil.vaildLog("AmlLog.java f = " + f));
			   out.write(f);
			   okcnt++;
			   totalcnt++;
		}
        
		try {
			 out.flush();
			 out.close();
		}
		catch(Exception e){
			log.warn("AmlLog.java out close Exception:"+e.getMessage());
		}
		try {
			fos.close();
		}
		catch(Exception e){
			log.warn("AmlLog.java file close Exception:"+e.getMessage());
		}

		
		if ("Y".equals(doFTP)) {
			String ipn = crmn_ftp_ip ;
			String filename = (String)despath + "." + DATADATE + ".TXT";
	
			FtpBase64 ftpBase64 = new FtpBase64(ipn);
	//		System.out.println("tmpFile = " + tmpFile.getPath());
			log.info(ESAPIUtil.vaildLog("tmpFile >> " + tmpFile.getPath()));
			int rc=ftpBase64.upload_ASCII(filename, new FileInputStream(tmpFile));
			if(rc!=0)
			{
				log.debug("FTP 失敗(CRM_NEW 主機)");
	//			CheckServerHealth checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
				checkserverhealth.sendUnknowNotice("FTP 失敗(CRM_NEW 主機)");
				ERRORMSG = "ftp upload fail";
			}
		}
		rowcnt.put("OKCNT",okcnt);
		rowcnt.put("FAILCNT",failcnt);
		rowcnt.put("TOTALCNT",totalcnt);
		 
		//tmpFile.delete();
		
	}
	public Map<String,Object> getStrlen( String name , String endcoding ) 
            throws Exception{ 
		Map < String , Object> rtnMap = new HashMap<String,Object>();
        //按照指定編碼得到byte[] 
        byte [] b_name = name.getBytes( endcoding ) ; 
        int byteLen = b_name.length;
        String rtnStr = new String(b_name,"BIG5");
        rtnMap.put("Big5Str",  rtnStr);
        rtnMap.put("Big5StrLen", byteLen);
        return rtnMap; 
    } 

}
