package fstop.services.batch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.ESAPIUtil;

import fstop.notifier.NotifyAngent;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.TXNUSER;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import topibs.utility.NumericUtil;

/**
 * 每日產生上傳 email 修改的清單至 /DecodeFolder/CreditCard
 * @author Owner
 *
 */
@Slf4j
@RestController
@Batch(id="batch.credipaymail", name = "CrediPayMail.java", description = "每日產生上傳 email 修改的清單")
public class CrediPayMail  implements EmailUploadIf, BatchExecute {
	@Autowired
	private TxnUserDao txnUserDao;
	
	@Value("${batch.hostfile.path:}")
	private String hostfilepath;
	
	@Value("${host.hostdata.path}")
	private String hhdpath;
	
	String autopayflag ="N";
	
	
	@Value("${batch.otherfile.path:}")
	private String otherDir;
	
	@Value("${host.other.path}")
	private String hopath;
	
	@Autowired
	private SysBatchDao sysBatchDao;
	
	@RequestMapping(value = "/batch/crediPayMail", method = {RequestMethod.POST},
	            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody  List<String> args) {
		
		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}

		List<String> safeArgs = ESAPIUtil.validStrList(args);
		int	totalcnt=0,failcnt=0,okcnt=0;
		
		BatchResult batchresult = new BatchResult();
		batchresult.setBatchName(getClass().getSimpleName());
		Map<String, Object> data = new LinkedHashMap();
		
		String FullFileName=new String();
		String FullFileNameOK=new String();
		String FullFileName_Host=new String();
		String FullFileNameOK_Host=new String();
		String OKDate=null;
		InputStreamReader in = null;
		BufferedReader bin = null;
		String subject=new String();
		Date d = new Date();
		String today = getPreday();//檔案的今天日期要改為前一天
		String date = DateTimeUtils.getDateShort(d);
		String time = DateTimeUtils.getTimeShort(d);

		Hashtable sendmaildata = new Hashtable();
		String oldcusidno="FFFFFFFFFF";
		StringBuffer mailrecodbuf=new StringBuffer();
		String cusidn="";
		
		log.debug("EXEC DATE = " + date);
		log.debug("EXEC TIME = " + time);
		
		String fileokcnt=null;
		FullFileNameOK= hostfilepath + "OK_"+ today + ".TXT";
		//For Error log
		FullFileNameOK_Host= hhdpath + "OK_"+ today + ".TXT";
		
		File fileok = new File(FullFileNameOK);
		fileok.setWritable(true,true);
		fileok.setReadable(true, true);
		fileok.setExecutable(true,true);
		if(fileok.exists()!=true)
		{
			data.put("Error", FullFileNameOK_Host + " is not exist.");
			batchresult.setSuccess(false);
			batchresult.setData(data);
			
			try {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName,data);
			}catch (Exception e) {
				log.error("sysBatchDao.finish error >> {} ", e.getMessage());
			}
			
			return batchresult;
		}
		log.debug("OK_FileName = " + FullFileNameOK);
		
		try
		{
			in = new InputStreamReader(new FileInputStream(FullFileNameOK), "MS950");
			bin = new BufferedReader(in);
			
			if (!bin.ready()) {
				data.put("Error", "檔案為空(" + FullFileNameOK_Host + ")");
				batchresult.setData(data);
				in.close();
				bin.close();
				
				try {
					// 新增SYSBATCH紀錄
					sysBatchDao.finish_FAIL2(batchName,data);
				}catch (Exception e) {
					log.error("sysBatchDao.finish error >> {} ", e.getMessage());
				}
				
				return batchresult;
			}
			
			while(true)
			{
				subject = ESAPIUtil.validInput(bin.readLine(),"GeneralString", true);
				if(subject==null || subject.length()==0)
					break;
				// 資料日期
				OKDate = subject.substring(0, 7).trim();
				log.debug(ESAPIUtil.vaildLog("OKDate = " + OKDate));
				// 筆數
				fileokcnt = subject.substring(8, 16).trim();
				log.debug(ESAPIUtil.vaildLog("Fileokcnt = " + fileokcnt));
				
			}
		}
		catch(FileNotFoundException e) {
			log.error("找不到 (" + FullFileNameOK_Host + ")檔案," + e.getMessage());
			data.put("Error","找不到 (" + FullFileNameOK_Host + ")檔案");
			batchresult.setSuccess(false);
			batchresult.setData(data);
			
			try {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName,data);
			}catch (Exception e2) {
				log.error("sysBatchDao.finish error >> {} ", e2.getMessage());
			}
			
			return batchresult;
		}
		catch(IOException e) {
			log.error("IOException (" + FullFileName_Host + ")," + e.getMessage());
			data.put("Error","IOException (" + FullFileName_Host + ")");
			batchresult.setSuccess(false);
			batchresult.setData(data);
			
			try {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName,data);
			}catch (Exception e2) {
				log.error("sysBatchDao.finish error >> {} ", e2.getMessage());
			}
			
			return batchresult;
		}
		finally {
			try {
				in.close();
			} catch(Exception e){}
			in = null;
		}
		FullFileName=hostfilepath;
		FullFileName+="NOTICE_" + today + ".TXT";
		
		//For Error log
		FullFileName_Host= hhdpath + "NOTICE_" + today + ".TXT";
		
		log.debug("NOTICE_FileName = " + FullFileName);
		
		try {
			in = new InputStreamReader(new FileInputStream(FullFileName),"MS950");
			log.debug("Encode = " + in.getEncoding());
			bin = new BufferedReader(in);
			
			if (!bin.ready()) {
				data.put("Error", "檔案為空(" + FullFileName_Host + ")");
				batchresult.setData(data);
				in.close();
				bin.close();
				
				try {
					// 新增SYSBATCH紀錄
					sysBatchDao.finish_FAIL2(batchName,data);
				}catch (Exception e2) {
					log.error("sysBatchDao.finish error >> {} ", e2.getMessage());
				}
				
				return batchresult;
			}
			while(true)
			{
				subject = ESAPIUtil.validInput(bin.readLine(),"GeneralString", true);
				if(subject==null || subject.length()==0)
					break;
				log.debug(ESAPIUtil.vaildLog("subject = " + subject));
				byte[] subjectarray=subject.getBytes("MS950");
//				new HexDump(HexDump.HD_ASCII,"subjectarray",subjectarray,subjectarray.length);
				// 客戶統編
				byte[] cusidnarray=new byte[10];
				System.arraycopy(subjectarray,0,cusidnarray,0,cusidnarray.length);
				cusidn = new String(cusidnarray).trim();
				if(!cusidn.equals(oldcusidno) && !oldcusidno.equals("FFFFFFFFFF"))
				{
					int rc=0;
					int chkmailret=0;
					chkmailret=chkemail(sendmaildata,cusidn,oldcusidno);
					if(chkmailret==0)
					{
						sendmaildata.put("EMAILRECOD", mailrecodbuf.toString());
						rc=sendmail(sendmaildata);
						if(rc==1)
						{
							totalcnt++;
							failcnt++;
						}
						else
						{
							totalcnt++;
							okcnt++;
						}
					}
					mailrecodbuf=new StringBuffer();
					sendmaildata=new Hashtable();
				}
				
				oldcusidno=cusidn;
				

				log.debug("cusidn = " + cusidn);
				sendmaildata.put("CUSIDN", cusidn);
				// 卡號
				byte[] cardnumarray=new byte[16];
				System.arraycopy(subjectarray,cusidnarray.length+1,cardnumarray,0,cardnumarray.length);
				String cardnum = new String(cardnumarray).trim();
				
				//log.debug("cardnum = " + cardnum.substring(0, 4) + "-" + cardnum.substring(4, 8) + "-" + cardnum.substring(8, 9) + "***" + "-" + "**" + StrUtils.right(cardnum, 2));
				sendmaildata.put("CARDNUM", "");
				// 客戶姓名
				byte[] cusnamearray=new byte[30];
				System.arraycopy(subjectarray,cusidnarray.length+1+cardnumarray.length,cusnamearray,0,cusnamearray.length);
				String cusname = new String(cusnamearray,"MS950").trim();
				log.debug("cusname = " + cusname);
				sendmaildata.put("CUSNAME", cusname);
				// 資料日期
				byte[] datadatearray=new byte[7];
				System.arraycopy(subjectarray,cusidnarray.length+1+cardnumarray.length+cusnamearray.length,datadatearray,0,datadatearray.length);
				String datadate = new String(datadatearray).trim();
				log.debug("datadate YEAR = " + datadate.substring(0, 3));

				sendmaildata.put("YEAR", datadate.substring(0, 3));
				
				log.debug("datadate MON = " + Integer.toString(Integer.parseInt(datadate.substring(3, 5))-1));
				if(Integer.toString(Integer.parseInt(datadate.substring(3, 5))-1).equals("0"))
				{
					sendmaildata.put("MON", "12");
				}
				else
				{
					sendmaildata.put("MON", Integer.toString(Integer.parseInt(datadate.substring(3, 5))-1));
				}
				// 應繳總額  DBTTAMT
				byte[] dbttamtarray=new byte[12];
				System.arraycopy(subjectarray,cusidnarray.length+1+cardnumarray.length+cusnamearray.length+datadatearray.length,dbttamtarray,0,dbttamtarray.length);
				String dbttamt = new String(dbttamtarray).substring(0, 10);
				dbttamt = dbttamt.trim();
				//log.debug("dbttamt = " + NumericUtil.formatNumberString(dbttamt,2));
//				sendmaildata.put("DBTTAMT", NumericUtil.formatNumberString(dbttamt,2));
				log.debug("dbttamt = " + NumericUtil.formatNumberString(dbttamt,0));
				sendmaildata.put("DBTTAMT", NumericUtil.formatNumberString(dbttamt,0));
				
				// 最低應繳  DBDUAMT
				byte[] remitamarray=new byte[12];
				System.arraycopy(subjectarray,cusidnarray.length+1+cardnumarray.length+cusnamearray.length+datadatearray.length+dbttamtarray.length,remitamarray,0,remitamarray.length);
				String remitam = new String(remitamarray).substring(0, 10).trim();
//				log.debug("DBDUAMT = " + NumericUtil.formatNumberString(remitam, 2));
//				sendmaildata.put("DBDUAMT", NumericUtil.formatNumberString(remitam, 2));
				log.debug("DBDUAMT = " + NumericUtil.formatNumberString(remitam, 0));
				sendmaildata.put("DBDUAMT",NumericUtil.formatNumberString(remitam, 0));
				// 繳款截止日  DUEDATE
				byte[] duedatearray=new byte[7];
				System.arraycopy(subjectarray,cusidnarray.length+1+cardnumarray.length+cusnamearray.length+datadatearray.length+dbttamtarray.length+remitamarray.length,duedatearray,0,duedatearray.length);
				String duedate = new String(duedatearray).trim();
				log.debug("duedate = " + duedate.substring(0, 3) + "/" + duedate.substring(3, 5) + "/" + duedate.substring(5, 7));
				sendmaildata.put("DUEYEAR", duedate.substring(0, 3) );
				sendmaildata.put("DUEMON", duedate.substring(3, 5) );
				sendmaildata.put("DUEDAY",  duedate.substring(5, 7));

				// 自動轉帳扣繳註記   "Y"表客戶曾申請自動轉帳扣繳, "N"表客戶未曾申請
				byte[] autopayflagarray=new byte[1];
				System.arraycopy(subjectarray,cusidnarray.length+1+cardnumarray.length+cusnamearray.length+datadatearray.length+dbttamtarray.length+remitamarray.length+duedatearray.length,autopayflagarray,0,autopayflagarray.length);
				autopayflag = new String(autopayflagarray,"MS950").trim();
				log.debug("autopayflag = " + autopayflag);
				sendmaildata.put("AUTOPAYFLAG", autopayflag);
				
				sendmaildata.put("REMIND", "<TR><TD width=650><DIV align=left><FONT  SIZE=5><BR><B>謹慎理財 信用無價</font><BR><font size=3>信用卡循環信用年利率=本行基準利率+加碼利率(3%~15%)，上限為15%。<BR>預借現金手續費=每筆150元+每筆預借現金金額  x 2.5%，其他費率依本行網站公告。</B></FONT></DIV></TD></TR>");
				mailrecodbuf.append(editmail(sendmaildata));
			}
			if(!oldcusidno.equals("FFFFFFFFFF"))
			{
				int rc=0;
				int chkmailret=0;
				chkmailret=chkemail(sendmaildata,cusidn,oldcusidno);
				if(chkmailret==0)
				{
					sendmaildata.put("EMAILRECOD", mailrecodbuf.toString());
					
					rc=sendmail(sendmaildata);
					if(rc==1)
					{
						totalcnt++;
						failcnt++;
					}
					else
					{
						totalcnt++;
						okcnt++;
					}
				}
				mailrecodbuf=new StringBuffer();
				sendmaildata=new Hashtable();
			}


		}
		catch(FileNotFoundException e) {
			log.error("找不到 (" + FullFileName_Host + ")檔案," + e.getMessage());
			data.put("Error","找不到 (" + FullFileName_Host + ")檔案");
			batchresult.setSuccess(false);
			batchresult.setData(data);
			
			try {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName,data);
			}catch (Exception e2) {
				log.error("sysBatchDao.finish error >> {} ", e2.getMessage());
			}
			
			return batchresult;
		}
		catch(IOException e) {
			log.error("IOException (" + FullFileName_Host + ")," + e.getMessage());
			data.put("Error","IOException (" + FullFileName_Host + ")");
			batchresult.setSuccess(false);
			batchresult.setData(data);
			try {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName,data);
			}catch (Exception e2) {
				log.error("sysBatchDao.finish error >> {} ", e2.getMessage());
			}
			
			return batchresult;
		}
		finally {
			try {
				in.close();
				bin.close();
			} catch(Exception e){}
			in = null;
			bin = null;
		}
		

		Calendar c1 = Calendar.getInstance();
		c1.setTime(d);
		c1.add(Calendar.DATE,-1);
		String yesterday=DateTimeUtils.getDateShort(c1.getTime());
		// ....... today = yesterday
//		RuntimeExec runtimeExec=new RuntimeExec("/TBB/nb3/batch","/TBB/nb3/batch/credipaybak.sh " + today$1 + " " + yesterday$2);
//		if(runtimeExec.getcommstat()!=0)
//		{
//			log.debug("/TBB/nb3/batch/credipaybak.sh exec command error");
//			// mkdir /backup5y/20090901/email/
//		}
		
		log.debug("============ Backup Area Start============");
		log.debug("DO credipaybak.sh");
		log.debug("--Check Dir Start--");
		String backupdir = otherDir + "backup5y/" + yesterday;
		String backupdirHost = hopath + "backup5y/" + yesterday;
		log.debug("backupdir >>" + backupdirHost);
		File file = new File(backupdir);
		file.setWritable(true, true);
		file.setReadable(true, true);
		file.setExecutable(true, true);
		if (!file.exists()) {
			log.debug("mkdir >>" + backupdirHost);
			file.mkdir();
		}
		
		String emailbakdir = backupdir + "/email";
		String emailbakdirHost = backupdirHost + "/email";
		log.debug("emailbakdir >>" + emailbakdirHost);
	
		file = new File(emailbakdir);
		file.setWritable(true, true);
		file.setReadable(true, true);
		file.setExecutable(true, true);
		if (!file.exists()) {
			log.debug("mkdir >>" + emailbakdirHost);
			file.mkdir();
		}
		log.debug("-- Check Dir End --");
		log.debug("-- Move file Start --");
		
		String hostFile1path = hostfilepath + "NOTICE_" + today + ".TXT";
		String hostFile2path = hostfilepath + "OK_"+ today + ".TXT";
		
		String backDirShow =  hopath + "backup5y/"+yesterday+"/email/";
		
		
		File file1 = new File(hostFile1path);
		file1.setWritable(true, true);
		file1.setReadable(true, true);
		file1.setExecutable(true, true);
		File file1old = new File(emailbakdir + "/NOTICE_" + today + ".TXT");
		file1old.setWritable(true, true);
		file1old.setReadable(true, true);
		file1old.setExecutable(true, true);
		
		if (file1.exists()) {
			file1old.delete();
			try {
				FileUtils.copyFile(file1, file1old);
				log.debug("move {} to {} OK", "NOTICE_" + today + ".TXT", backDirShow);
				data.put("Backup_NOTICE.txt", "Success move "+"NOTICE_" + today + ".TXT"+" to "+backDirShow);
				file1.delete();
			} catch (IOException e) {
				log.error("move file error");
				data.put("Backup_NOTICE.txt", "Fail");
			}
		}

		File file2 = new File(hostFile2path);
		file2.setWritable(true, true);
		file2.setReadable(true, true);
		file2.setExecutable(true, true);
		File file2old = new File(emailbakdir + "/OK_"+ today + ".TXT");
		file2old.setWritable(true, true);
		file2old.setReadable(true, true);
		file2old.setExecutable(true, true);
		
		if (file2.exists()) {
			file2old.delete();
			try {
				FileUtils.copyFile(file2, file2old);
				log.debug("move {} to {} OK", "OK_"+ today + ".TXT", backDirShow);
				data.put("Backup_OK.txt", "Success move "+"OK_"+ today + ".TXT"+" to "+ backDirShow);
				file2.delete();
			} catch (IOException e) {
				log.error("move file error");
				data.put("Backup_OK.txt", "Fail");
			}
		}
		log.debug("-- Move file End --");
		log.debug("============ Backup Area End ============");
		
		
		BatchCounter bcnt=new BatchCounter();
		bcnt.setFailCount(failcnt);
		bcnt.setSuccessCount(okcnt);
		bcnt.setTotalCount(totalcnt);
		data.put("COUNTER", bcnt);
		
		data.put("FILEOKTOTALCOUNT", fileokcnt);

		batchresult.setSuccess(true);
		batchresult.setData(data);
		
		try {
			// 新增SYSBATCH紀錄
			sysBatchDao.finish_OK(batchName);
		}catch (Exception e2) {
			log.error("sysBatchDao.finish error >> {} ", e2.getMessage());
		}

		return batchresult;

	}
	
	public int sendmail(Hashtable maildata)
	{
		boolean isSendSuccess;
		if(autopayflag.equals("N")) {    //未申請自動代扣繳
			isSendSuccess = NotifyAngent.sendNotice("CREDITPAY", maildata, (String)maildata.get("MAILADDR"));
		}else {	//申請自動代扣繳
			isSendSuccess = NotifyAngent.sendNotice("CREDITPAY1", maildata, (String)maildata.get("MAILADDR"));
		}
		if(!isSendSuccess) {
			log.debug("發送 錯誤Email 失敗.(" + maildata.get("MAILADDR") + ")");
//			batchresult.getFlatValues().put("__MAILERR", "發送 Email 失敗.(" + email1 + ")");

			return(1);
		}
		log.debug("發送 Email 成功.(" + maildata.get("MAILADDR") + ")");

		return(0);
	}
	
	public String editmail(Hashtable maildata)
	{
		StringBuffer editmail=new StringBuffer();
		
		editmail.append("<TR>\r\n");
		editmail.append("<TD width=\"200\">\r\n");
		editmail.append("<DIV align=center><FONT SIZE=-1>").append(maildata.get("CARDNUM")).append("</FONT></DIV></TD>\r\n");
		editmail.append("<TD width=\"150\">\r\n");
		editmail.append("<DIV align=center><FONT SIZE=-1>").append(maildata.get("DBTTAMT")).append("</FONT></DIV></TD>\r\n");
		editmail.append("<TD width=\"150\">\r\n");
		editmail.append("<DIV align=center><FONT SIZE=-1>").append(maildata.get("DBDUAMT")).append("</FONT></DIV></TD>\r\n");
		editmail.append("</TR>\r\n");

		return(editmail.toString());
	}
	
	public int chkemail(Hashtable email,String cusidn,String oldcusidno)
	{
		String mailaddr = null;
		TXNUSER user = null;
		List<TXNUSER> listtxnUser;
		try {
			log.debug("OLDCUSIDN = " + oldcusidno);
			listtxnUser=txnUserDao.findByUserId(oldcusidno);
			user = listtxnUser.get(0);
			mailaddr = user.getDPMYEMAIL();
		}
		catch(Exception e){
			log.error("無法取得 USER ID (" + oldcusidno + ")," + e.getMessage());
			return(1);
		}
		
		String dpnotify = StrUtils.trim(user.getDPNOTIFY());
		Set<String> dpnotifySet = StrUtils.splitToSet("\\s*,\\s*", dpnotify);
		
		boolean isUserSetTxnNotify = dpnotifySet.contains("11");
		if(isUserSetTxnNotify==false)
		{
			log.error("The notify is't exist.(USER ID : " + oldcusidno + ") ");
			return(1);
		}

		if(mailaddr.length()==0 || mailaddr==null)
		{
			log.error("The mail is't exist.(USER ID : " + oldcusidno + ") ");
			return(1);
		}

		log.debug(ESAPIUtil.vaildLog("mailaddr = " + mailaddr));
		email.put("MAILADDR", mailaddr);
		return(0);

	}
	private String getPreday()
	{
		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_YEAR);
	    calendar.set(Calendar.DAY_OF_YEAR, day - 1);
	    String preday = DateTimeUtils.getDateShort(calendar.getTime()).replace("/", "");
		log.trace("preday in CrediPayMail.getPreday() = "+preday+"===");
		return preday;
	}
}
