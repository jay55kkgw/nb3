package fstop.services.batch;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.ESAPIUtil;

import fstop.orm.dao.AdmHolidayDao;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.TxnFundDataDao;
import fstop.orm.po.ADMHOLIDAY;
import fstop.orm.po.TXNFUNDDATA;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 基金資料擷取_更新版(從電文產生落地檔後，在更新db)
 *
 * @author Owner
 */
@Slf4j
@RestController
@Batch(id = "batch.c017", name = "基金資料擷取", description = "基金資料擷取_更新版(從電文產生落地檔後，在更新db)")
public class C017 implements BatchExecute {

	@Value("${batch.hostfile.path:}")
	private String pathname;

	@Value("${host.hostdata.path}")
	private String hostpath;

	@Autowired
	private TxnFundDataDao txnFundDataDao;

	@Autowired
	private AdmHolidayDao admholidayDao;
	
	@Autowired
	private SysBatchDao sysBatchDao;

	@RequestMapping(value = "/batch/c017", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {
		
		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}
		
		List<String> safeArgs = ESAPIUtil.validStrList(args);
		Date d = new Date();
		String today = DateTimeUtils.getCDateShort(d);

		BatchResult batchresult = new BatchResult();
		batchresult.setBatchName(getClass().getSimpleName());
		Map<String, Object> data = new HashMap();

		log.debug("Batch_Execute_Date = " + today);

		ADMHOLIDAY holiday = null;
		try {
			String holidaytoday = DateTimeUtils.getDateShort(d);
			holiday = (ADMHOLIDAY) admholidayDao.findById(holidaytoday);
			log.debug("Holiday = " + holiday);
		} catch (Exception e) {
			holiday = null;
		}

		if (holiday != null) {
			data.put("__TOPMSG", "ZXXX");
			data.put("__TOPMSGTEXT", "非營業日．");
			log.debug("__TOPMSGTEXT = " + "非營業日．");
			batchresult.setSuccess(false);
			batchresult.setData(data);
		} else {
			String filePath = pathname + "C017F.TXT";
			// For Error log
			String hostfilePath = hostpath + "C017F.TXT";
			log.debug("filePath >>{}", filePath);
			batchresult = readFile(filePath, hostfilePath);
		}
		
		try {
			if (batchresult.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName,batchresult.getData());
			}
		}catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}

		return batchresult;
	}

	private BatchResult readFile(String tempfile, String hostpath) {
		
		BatchResult batchresult = new BatchResult();
		Map<String, Object> data = new HashMap();
		BatchCounter bc = new BatchCounter();
		

		InputStreamReader in = null;
		BufferedReader bin = null;
		String subject;

		try {
			in = new InputStreamReader(new FileInputStream(tempfile), "BIG5");
			bin = new BufferedReader(in);
			int succcnt = 0;
			int failcnt = 0;

			if (!bin.ready()) {
				in.close();
                bin.close();
            	throw new Exception();
			}

			txnFundDataDao.deleteAll();
			while ((subject = bin.readLine()) != null && subject.length() != 0) {
				TXNFUNDDATA tt = new TXNFUNDDATA();
				String strtemp = subject;
				log.trace(ESAPIUtil.vaildLog("C017 LINE STR:" + subject));
				String TRANSCODEprt1 = strtemp.substring(0, strtemp.indexOf("#|")).trim();
				strtemp = strtemp.substring(strtemp.indexOf("#|") + 2);
				String TRANSCODEprt2 = strtemp.substring(0, strtemp.indexOf("#|")).trim();
				strtemp = strtemp.substring(strtemp.indexOf("#|") + 2);
				log.trace(ESAPIUtil.vaildLog("TRANSCODE:" + TRANSCODEprt1 + TRANSCODEprt2 + " LENGTH:"
						+ (TRANSCODEprt1 + TRANSCODEprt2).length()));
				tt.setTRANSCODE(TRANSCODEprt1 + TRANSCODEprt2);
				String TRANSCRY = strtemp.substring(0, strtemp.indexOf("#|")).trim();
				strtemp = strtemp.substring(strtemp.indexOf("#|") + 2);
				log.trace(ESAPIUtil.vaildLog("TRANSCRY:" + TRANSCRY + " LENGTH:" + TRANSCRY.length()));
				tt.setTRANSCRY(TRANSCRY);
				String FUNDLNAME = strtemp.substring(0, strtemp.indexOf("#|")).trim();
				strtemp = strtemp.substring(strtemp.indexOf("#|") + 2);
				log.trace(ESAPIUtil.vaildLog("FUNDLNAME:" + FUNDLNAME + " LENGTH:" + FUNDLNAME.length()));
				tt.setFUNDLNAME(FUNDLNAME);
				String FUNDSNAME = strtemp.substring(0, strtemp.indexOf("#|")).trim();
				strtemp = strtemp.substring(strtemp.indexOf("#|") + 2);
				log.trace(ESAPIUtil.vaildLog("FUNDSNAME:" + FUNDSNAME + " LENGTH:" + FUNDSNAME.length()));
				tt.setFUNDSNAME(FUNDSNAME);
				String COUNTRYTYPE = strtemp.substring(0, strtemp.indexOf("#|")).trim();
				strtemp = strtemp.substring(strtemp.indexOf("#|") + 2);
				log.trace(ESAPIUtil.vaildLog("COUNTRYTYPE:" + COUNTRYTYPE + " LENGTH:" + COUNTRYTYPE.length()));
				tt.setCOUNTRYTYPE(COUNTRYTYPE);
				String FUNDMARK = strtemp.substring(0, strtemp.indexOf("#|")).trim();
				strtemp = strtemp.substring(strtemp.indexOf("#|") + 2);
				log.trace(ESAPIUtil.vaildLog("FUNDMARK:" + FUNDMARK + " LENGTH:" + FUNDMARK.length()));
				tt.setFUNDMARK(FUNDMARK);
				String RISK = strtemp.substring(0, strtemp.indexOf("#|")).trim();
				strtemp = strtemp.substring(strtemp.indexOf("#|") + 2);
				log.trace(ESAPIUtil.vaildLog("RISK:" + RISK + " LENGTH:" + RISK.length()));
				tt.setRISK(RISK);
				String PERIODVAR = strtemp.substring(0, strtemp.indexOf("#|")).trim();
				strtemp = strtemp.substring(strtemp.indexOf("#|") + 2);
				log.trace("PERIODVAR:" + PERIODVAR + " LENGTH:" + PERIODVAR.length());
				tt.setPERIODVAR(PERIODVAR);
				String FUS98E = strtemp.substring(0, strtemp.indexOf("#|")).trim();
				strtemp = strtemp.substring(strtemp.indexOf("#|") + 2);
				log.trace("FUS98E:" + FUS98E + " LENGTH:" + FUS98E.length());
				tt.setFUS98E(FUS98E);
				String FUSMON = strtemp.substring(0, strtemp.indexOf("#|")).trim();
				strtemp = strtemp.substring(strtemp.indexOf("#|") + 2);
				log.trace("FUSMON:" + FUSMON + " LENGTH:" + FUSMON.length());
				tt.setFUSMON(FUSMON);
				//新增的欄位 但TABLE還沒開  所以先做處理但是不set
				String CBAMT = strtemp.substring(0, strtemp.indexOf("#|")).trim();
				strtemp = strtemp.substring(strtemp.indexOf("#|") + 2);
				log.trace("CBAMT:" + CBAMT + " LENGTH:" + CBAMT.length());
				tt.setCBAMT(CBAMT);
				String YBAMT = strtemp.substring(0, strtemp.indexOf("#|")).trim();
				strtemp = strtemp.substring(strtemp.indexOf("#|") + 2);
				log.trace("YBAMT:" + YBAMT + " LENGTH:" + YBAMT.length());
				tt.setYBAMT(YBAMT);
				String CBTAMT = strtemp.substring(0, strtemp.indexOf("#|")).trim();
				strtemp = strtemp.substring(strtemp.indexOf("#|") + 2);
				log.trace("CBTAMT:" + CBTAMT + " LENGTH:" + CBTAMT.length());
				tt.setCBTAMT(CBTAMT);
				String YBTAMT = strtemp.substring(0, strtemp.indexOf("#|")).trim();
				strtemp = strtemp.substring(strtemp.indexOf("#|") + 2);
				log.trace("YBTAMT:" + YBTAMT + " LENGTH:" + YBTAMT.length());
				tt.setYBTAMT(YBTAMT);
				String FUNDT = strtemp.substring(0, strtemp.indexOf("#|")).trim();
				strtemp = strtemp.substring(strtemp.indexOf("#|") + 2);
				log.trace("FUNDT:" + FUNDT + " LENGTH:" + FUNDT.length());
				tt.setFUNDT(FUNDT);
				String C30AMT = strtemp.substring(0, strtemp.indexOf("#|")).trim();
				strtemp = strtemp.substring(strtemp.indexOf("#|") + 2);
				log.trace("C30AMT:" + C30AMT + " LENGTH:" + C30AMT.length());
				tt.setC30AMT(C30AMT);
				String C30RAMT = strtemp.substring(0, strtemp.indexOf("#|")).trim();
				strtemp = strtemp.substring(strtemp.indexOf("#|") + 2);
				log.trace("C30RAMT:" + C30RAMT + " LENGTH:" + C30RAMT.length());
				tt.setC30RAMT(C30RAMT);
				String Y30AMT = strtemp.substring(0, strtemp.indexOf("#|")).trim();
				strtemp = strtemp.substring(strtemp.indexOf("#|") + 2);
				log.trace("Y30AMT:" + Y30AMT + " LENGTH:" + Y30AMT.length());
				tt.setY30AMT(Y30AMT);
				String Y30RAMT = strtemp.substring(0, strtemp.indexOf("#|")).trim();
				strtemp = strtemp.substring(strtemp.indexOf("#|") + 2);
				log.trace("Y30RAMT:" + Y30RAMT + " LENGTH:" + Y30RAMT.length());
				tt.setY30RAMT(Y30RAMT);
				String C302AMT = strtemp.substring(0, strtemp.indexOf("#|")).trim();
				strtemp = strtemp.substring(strtemp.indexOf("#|") + 2);
				log.trace("C302AMT:" + C302AMT + " LENGTH:" + C302AMT.length());
				tt.setC302AMT(C302AMT);
				String C302RAMT = strtemp.substring(0, strtemp.indexOf("#|")).trim();
				strtemp = strtemp.substring(strtemp.indexOf("#|") + 2);
				log.trace("C302RAMT:" + C302RAMT + " LENGTH:" + C302RAMT.length());
				tt.setC302RAMT(C302RAMT);
				String Y302AMT = strtemp.substring(0, strtemp.indexOf("#|")).trim();
				strtemp = strtemp.substring(strtemp.indexOf("#|") + 2);
				log.trace("Y302AMT:" + Y302AMT + " LENGTH:" + Y302AMT.length());
				tt.setY302AMT(Y302AMT);
				String Y302RAMT = strtemp.trim();
				log.trace("Y302RAMT:" + Y302RAMT + " LENGTH:" + Y302RAMT.length());
				tt.setY302RAMT(Y302RAMT);
				try {
					log.info("saveData...");
					txnFundDataDao.save(tt);
					log.info("saveData...end");
					succcnt++;
				} catch (Exception e) {
					failcnt++;
				}
			}

			Long l = null;
			l = txnFundDataDao.count();

			bc.setSuccessCount(succcnt);
			bc.setFailCount(failcnt);
			bc.setTotalCount(Integer.parseInt(String.valueOf(l)));
			data.put("COUNTER", bc);

			batchresult.setSuccess(true);
			batchresult.setBatchName(this.getClass().getSimpleName());
			batchresult.setData(data);
			
			log.debug("C017.java okcnt:" + succcnt + " failcnt:" + failcnt);
		} catch (FileNotFoundException e) {
			data.put("Error", "找不到檔案(" + hostpath + ")");
			batchresult.setData(data);
			batchresult.setSuccess(false);
		} catch (IOException e) {
			data.put("Error", "讀取檔案錯誤(" + hostpath + ")");
			batchresult.setData(data);
			batchresult.setSuccess(false);
		} catch (Exception e) {
			data.put("Error", "檔案為空(" + hostpath + ")");
			batchresult.setData(data);
			batchresult.setSuccess(false);
		} finally {
			try {
				in.close();
				bin.close();
			} catch (Exception e) {
			}
			in = null;
			bin = null;

			return batchresult;
		}
	}
}
