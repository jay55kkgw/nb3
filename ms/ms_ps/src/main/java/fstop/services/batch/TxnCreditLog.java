package fstop.services.batch;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.owasp.esapi.SafeFile;
import org.owasp.esapi.errors.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.ESAPIUtil;

import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.TxnCreditLogDao;
import fstop.orm.po.TXNCREDITLOG;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 每日產生昨日的交易記錄檔 - N816L信用卡掛失交易記錄
 * 
 * @author Owner
 * 
 */
@Slf4j
@RestController
@Batch(id = "batch.TxnCreditLog", name = "每日產生昨日的交易記錄檔 - N816L信用卡掛失交易記錄", description = "每日產生昨日的交易記錄檔 - N816L信用卡掛失交易記錄")
public class TxnCreditLog implements BatchExecute {

	@Autowired
	private TxnCreditLogDao txnCreditLogDao;

	@Value("${batch.hostfile.path:}")
	private String pathName;

	@Value("${batch.otherfile.path:}")
	private String otherDir;

	@Value("${host.hostdata.path}")
	private String hhdpath;

	@Value("${host.other.path}")
	private String hopath;

	public TxnCreditLog() {
	}

	@Autowired
	private SysBatchDao sysBatchDao;

	/**
	 * args[0] - Specific date (format: yyyyMMdd)
	 */
	@RequestMapping(value = "/batch/txnCreditLog", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	@Override
	public BatchResult execute(@RequestBody List<String> args) {

		BatchResult batchresult = new BatchResult();
		Map<String, Object> data = new LinkedHashMap();
		batchresult.setBatchName(getClass().getSimpleName());
		batchresult.setSuccess(true);

		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}

		List<String> safeArgs = ESAPIUtil.validStrList(args);
		int failcnt = 0, okcnt = 0, totalcnt = 0;
		Hashtable rowcnt = new Hashtable();
		log.info("TxnCreditLog Execute - Start");

		try {
			if (safeArgs.size() == 1) {
				log.info(ESAPIUtil.vaildLog("safeArgs[0]: " + safeArgs.get(0)));
				Date date = DateTimeUtils.parse("yyyyMMdd", safeArgs.get(0).replace("/", ""));
				exec(rowcnt, date);
			} else if (safeArgs.size() == 2) {
				log.info(ESAPIUtil.vaildLog("safeArgs[0]:" + safeArgs.get(0).replace("/", "")));
				log.info(ESAPIUtil.vaildLog("safeArgs[1]:" + safeArgs.get(1).replace("/", "")));
				exec(rowcnt, safeArgs.get(0).replace("/", ""), safeArgs.get(1).replace("/", ""));

			} else {
				exec(rowcnt);
				// 無指定日期才備份昨天
				log.debug("============ Backup Area Start============");

				log.debug("back up yesterday file");

				Date d = new Date();
				Calendar calendar = Calendar.getInstance();
				calendar.add(Calendar.DATE, -1);
				d = calendar.getTime();
				String yesterday = DateTimeUtils.getDateShort(d);
				log.debug("Yesterday >> {} ", yesterday);

				log.debug("--Check Dir Start--");

				String backupdir = otherDir + "backup5y/" + yesterday;
				// for show
				String backupdirHost = hopath + "backup5y/" + yesterday;

				log.debug("backupdir >>" + backupdirHost);

				File file = new File(backupdir);
				file.setWritable(true, true);
				file.setReadable(true, true);
				file.setExecutable(true, true);
				if (!file.exists()) {
					log.debug("mkdir >>" + backupdirHost);
					file.mkdir();
				}

				String tempbakdir = backupdir + "/temp";
				String tempbakdirHost = backupdirHost + "/temp";
				log.debug("tempbakdir >>" + tempbakdirHost);

				file = new File(tempbakdir);
				file.setWritable(true, true);
				file.setReadable(true, true);
				file.setExecutable(true, true);
				if (!file.exists()) {
					log.debug("mkdir >>" + tempbakdirHost);
					file.mkdir();
				}
				log.debug("-- Check Dir End --");
				log.debug("-- Move file Start --");

				String hostFilepath = String.format("%1$s%2$s_%3$s.txt", pathName, "TXN_CR_LOG", yesterday);
				String bakFilepath = String.format("%1$s%2$s_%3$s.txt", otherDir + "/backup5y/" + yesterday + "/temp/",
						"TXN_CR_LOG", yesterday);

				String backDirShow = hopath + "backup5y/" + yesterday + "/temp/";

				File file1 = new File(hostFilepath);
				file1.setWritable(true, true);
				file1.setReadable(true, true);
				file1.setExecutable(true, true);
				File filebak = new File(bakFilepath);
				filebak.setWritable(true, true);
				filebak.setReadable(true, true);
				filebak.setExecutable(true, true);

				if (file1.exists()) {
					try {
						FileUtils.copyFile(file1, filebak);
						log.debug("move {} to {} OK",
								String.format("%1$s%2$s_%3$s.txt", pathName, "TXN_CR_LOG", yesterday), backDirShow);
						data.put("Backup_Status", "Success move to " + backDirShow);
						file1.delete();
					} catch (IOException e) {
						log.error("move file error");
						data.put("Backup_Status", "Fail");
					}
				} else {
					data.put("Backup_Status", "No yesterday file to move");
				}

				log.debug("-- Move file End --");
				log.debug("============ Backup Area End ============");

			}

			failcnt = ((Integer) rowcnt.get("FAILCNT")).intValue();
			okcnt = ((Integer) rowcnt.get("OKCNT")).intValue();
			totalcnt = ((Integer) rowcnt.get("TOTALCNT")).intValue();
			log.debug("failcnt = " + failcnt);
			log.debug("okcnt = " + okcnt);
			log.debug("totalcnt = " + totalcnt);
		} catch (Exception e) {
			log.error("執行 Batch 有誤 !", e);
			batchresult.setSuccess(false);
			data.put("Error", e.getMessage());
			batchresult.setData(data);

			try {
				sysBatchDao.finish_FAIL2(batchName, batchresult.getData());
			} catch (Exception e2) {
				log.error("sysBatchDao.finish error >> {} ", e2.getMessage());
			}
			return batchresult;
		}

		BatchCounter bcnt = new BatchCounter();
		bcnt.setFailCount(failcnt);
		bcnt.setSuccessCount(okcnt);
		bcnt.setTotalCount(totalcnt);

		data.put("COUNTER", bcnt);

		batchresult.setData(data);

		log.info("TxnCreditLog Execute - End");

		try {
			// 新增SYSBATCH紀錄
			sysBatchDao.finish_OK(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.finish_OK error >> {} ", e.getMessage());
		}

		return batchresult;
	}

	private void exec(Hashtable rowcnt) throws IOException {
		exec(rowcnt, Calendar.getInstance().getTime());
	}

	private void exec(Hashtable rowcnt, Date date) throws IOException {
		Integer totalcnt = 0, failcnt = 0, okcnt = 0;

		String settleTime = "18:00";

		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(settleTime.split(":")[0]));
		cal.set(Calendar.MINUTE, Integer.parseInt(settleTime.split(":")[1]));
		cal.set(Calendar.SECOND, 0);
		Date findLogDateTime = cal.getTime();
		log.info("findLogDateTime = " + DateTimeUtils.getDatetime(findLogDateTime));

		String filePath = String.format("%1$s%2$s_%3$s.txt", pathName, "TXN_CR_LOG",
				DateTimeUtils.getDateShort(findLogDateTime));
		log.debug("filePath = " + filePath);

		File file;
		try {
			file = new File(filePath);
			file.setWritable(true, true);
			file.setReadable(true, true);
			file.setExecutable(true, true);
			// FileWriter fos = new FileWriter(file);
			// BufferedWriter out = new BufferedWriter(fos);
			// Path p = file.toPath();

			OutputStream fos = new FileOutputStream(file);
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(fos, "BIG5"));
			StringBuilder sb = new StringBuilder();
			List<TXNCREDITLOG> all = txnCreditLogDao.findBySpecificTimeRange(findLogDateTime);

			for (TXNCREDITLOG txn : all) {

				log.debug(ESAPIUtil.vaildLog("LOGID: " + txn.getLOGID() + ", CARDNUM: " + txn.getCARDNUM()));
				try {

					// 統編 10位英數字
					sb.append(StringUtils.rightPad(txn.getCUSIDN(), 10, ' '));

					// 卡號 16位數字
					sb.append(StringUtils.rightPad(txn.getCARDNUM(), 16, ' '));

					// 交易日期 8位數字 (西元)
					sb.append(txn.getTXNDATE());

					// 交易時間 6位數字 (HHmmss)
					sb.append(txn.getTXNTIME());

					// 交易結果 1位文字
					sb.append(txn.getSTATUS());

					// 預留 9位空字元
					sb.append(StringUtils.leftPad("", 8, ' '));

					// 行結尾符號
					sb.append("\n");

					// out.write(sb.toString());
					// Files.write(p, sb.toString().getBytes("BIG5"));

					okcnt++;
				} catch (Exception e) {
					log.error("Error, write [LOGID: " + txn.getLOGID() + ", CARDNUM: " + txn.getCARDNUM() + "], "
							+ e.getMessage());
					failcnt++;
				}
			}
			totalcnt = okcnt + failcnt;

			// byte[] strToBytes = sb.toString().getBytes("BIG5");
			// BufferedOutputStream outputStream = new BufferedOutputStream(new
			// FileOutputStream(file));
			// outputStream.write(strToBytes);
			// outputStream.close();
			out.write(sb.toString());
			try {
				out.close();
			} catch (Exception e) {
			}
			try {
				fos.close();
			} catch (Exception e) {
			}

			rowcnt.put("OKCNT", okcnt);
			rowcnt.put("FAILCNT", failcnt);
			rowcnt.put("TOTALCNT", totalcnt);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			log.error("NewSafeFile 錯誤.", e1);
		}
	}

	private void exec(Hashtable rowcnt, String Date1, String Date2) throws IOException {
		Integer totalcnt = 0, failcnt = 0, okcnt = 0;

		String filePath = String.format("%1$s%2$s_%3$s_%4$s.txt", pathName, "TXN_CR_LOG", Date1, Date2);
		log.debug("filePath = " + filePath);

		File file;
		try {
			file = new SafeFile(filePath);
			file.setWritable(true, true);
			file.setReadable(true, true);
			file.setExecutable(true, true);
			// Path p = file.toPath();
			// FileWriter fos = new FileWriter(file);
			// BufferedWriter out = new BufferedWriter(fos);

			OutputStream fos = new FileOutputStream(file);
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(fos, "BIG5"));
			StringBuilder sb = new StringBuilder();

			List<TXNCREDITLOG> all = txnCreditLogDao.findByDateRange(Date1, Date2);

			for (TXNCREDITLOG txn : all) {

				log.debug(ESAPIUtil.vaildLog("LOGID: " + txn.getLOGID() + ", CARDNUM: " + txn.getCARDNUM()));
				try {

					// 統編 10位英數字
					sb.append(StringUtils.rightPad(txn.getCUSIDN(), 10, ' '));

					// 卡號 16位數字
					sb.append(StringUtils.rightPad(txn.getCARDNUM(), 16, ' '));

					// 交易日期 8位數字 (西元)
					sb.append(txn.getTXNDATE());

					// 交易時間 6位數字 (HHmmss)
					sb.append(txn.getTXNTIME());

					// 交易結果 1位文字
					sb.append(txn.getSTATUS());

					// 預留 9位空字元
					sb.append(StringUtils.leftPad("", 8, ' '));

					// 行結尾符號
					sb.append("\n");

					// out.write(sb.toString());
					// Files.write(p, sb.toString().getBytes("BIG5"));
					okcnt++;
				} catch (Exception e) {
					log.error("Error, write [LOGID: " + txn.getLOGID() + ", CARDNUM: " + txn.getCARDNUM() + "], "
							+ e.getMessage());
					failcnt++;
				}
			}
			totalcnt = okcnt + failcnt;
			out.write(sb.toString());
			// BufferedOutputStream outputStream = new BufferedOutputStream(new
			// FileOutputStream(file));
			// byte[] strToBytes = sb.toString().getBytes("BIG5");
			// outputStream.write(strToBytes);
			// outputStream.close();

			try {
				out.close();
			} catch (Exception e) {
			}
			try {
				fos.close();
			} catch (Exception e) {
			}

			rowcnt.put("OKCNT", okcnt);
			rowcnt.put("FAILCNT", failcnt);
			rowcnt.put("TOTALCNT", totalcnt);
		} catch (ValidationException e1) {
			log.error("NewSafeFile 錯誤.", e1);
		}
	}
}
