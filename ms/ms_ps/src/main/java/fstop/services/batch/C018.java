package fstop.services.batch;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.ESAPIUtil;

import fstop.orm.dao.AdmHolidayDao;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.TxnFundCompanyDao;
import fstop.orm.po.ADMHOLIDAY;
import fstop.orm.po.TXNFUNDCOMPANY;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 基金公司資料擷取
 * @author Owner
 *
 */
@Slf4j
@RestController
@Batch(id="batch.c018", name = "基金資料擷取", description = "基金資料擷取_更新版(從電文產生落地檔後，在更新db)")
public class C018 implements BatchExecute{

    @Autowired
    private TxnFundCompanyDao txnFundCompanyDao;

    @Autowired
    private AdmHolidayDao admholidayDao;
	

	@Value("${batch.hostfile.path:}")
	private String pathname;
	
	@Value("${host.hostdata.path}")
	private String hostpath;
	
	@Autowired
	private SysBatchDao sysBatchDao;
	
    @RequestMapping(value = "/batch/c018", method = {RequestMethod.POST},
    produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
	public BatchResult execute(@RequestBody  List<String> args) {
    	
		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}

    	List<String> safeArgs = ESAPIUtil.validStrList(args);
		Date d = new Date();
		String today = DateTimeUtils.getCDateShort(d);
		
		
		BatchResult batchresult = new BatchResult();
		batchresult.setBatchName(getClass().getSimpleName());
		Map<String, Object> data = new HashMap();
		
		log.debug("Batch_Execute_Date = " + today);
		
		ADMHOLIDAY holiday = null;
		try {
			String holidaytoday = DateTimeUtils.getDateShort(d);
			holiday = (ADMHOLIDAY) admholidayDao.findById(holidaytoday);
			log.debug("Holiday = " + holiday); 
		}
		catch (Exception e) {
			holiday = null;					
		}

		if(holiday!=null)
		{
			data.put("__TOPMSG", "ZXXX");
			data.put("__TOPMSGTEXT", "非營業日．");
			log.debug("__TOPMSGTEXT = " + "非營業日．");
			batchresult.setSuccess(false);
			batchresult.setData(data);
		}else {
//			2019/10/14改成讀檔 BEN
			String filePath = pathname +"C018F.TXT";
        	log.debug("filePath >>{}",filePath);
        	
        	//For Error log
    		String hostfilePath = hostpath + "C018F.TXT";
    		log.debug("filePath >>{}", filePath);
        	batchresult = readFile(filePath,hostfilePath);
		}
		
		try {
			if (batchresult.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName,batchresult.getData());
			}
		}catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}
		

        return batchresult;
	}
    
    
       
    private BatchResult readFile(String tempfile,String hostpath) {
    	
    	BatchResult batchresult = new BatchResult();
    	Map<String, Object> data = new HashMap();
    	BatchCounter bc = new BatchCounter();
    	
    	
        InputStreamReader in = null;
        BufferedReader bin = null;
        String subject;
        
        try {
            in = new InputStreamReader(new FileInputStream(tempfile), "BIG5");
            bin = new BufferedReader(in);
        	int succcnt = 0;
			int failcnt = 0;
            
            if(!bin.ready()) {
            	in.close();
                bin.close();
            	throw new Exception();
			}
            
            txnFundCompanyDao.deleteAll();
            while ((subject = bin.readLine()) != null && subject.length() != 0) {
            	TXNFUNDCOMPANY tt = new TXNFUNDCOMPANY();
                String strtemp = subject;
                log.trace(ESAPIUtil.vaildLog("C018 LINE STR:" + subject));
                
                String companycode = strtemp.substring(0, strtemp.indexOf("#|")).trim();
                strtemp = strtemp.substring(strtemp.indexOf("#|") + 2);
                log.trace(ESAPIUtil.vaildLog("COMPANYCODE:" + companycode + " LENGTH:" + companycode.length()));
                tt.setCOMPANYCODE(companycode);
                
                String countrytype = strtemp.substring(0, strtemp.indexOf("#|")).trim();
                strtemp = strtemp.substring(strtemp.indexOf("#|") + 2);
                log.trace(ESAPIUtil.vaildLog("COUNTRYTYPE:" + countrytype + " LENGTH:" + countrytype.length()));
                tt.setCOUNTRYTYPE(countrytype);
                
                String companylname = strtemp.substring(0, strtemp.indexOf("#|")).trim();
                strtemp = strtemp.substring(strtemp.indexOf("#|") + 2);
                log.trace(ESAPIUtil.vaildLog("COMPANYLNAME:" + companylname + " LENGTH:" + companylname.length()));
                tt.setCOMPANYLNAME(companylname);
                
                String companysname = strtemp.substring(0, strtemp.indexOf("#|")).trim();
                strtemp = strtemp.substring(strtemp.indexOf("#|") + 2);
                log.trace(ESAPIUtil.vaildLog("COMPANYSNAME:" + companysname + " LENGTH:" + companysname.length()));
                tt.setCOMPANYSNAME(companysname);
                
                String fundhousemark = strtemp.substring(0, strtemp.indexOf("#|")).trim();
                strtemp = strtemp.substring(strtemp.indexOf("#|") + 2);
                log.trace(ESAPIUtil.vaildLog("FUNDHOUSEMARK:" + fundhousemark + " LENGTH:" + fundhousemark.length()));
                tt.setFUNDHOUSEMARK(fundhousemark);
       
                String fundgroupmark = strtemp.trim();
                log.trace(ESAPIUtil.vaildLog("FUNDGROUPMARK:" + fundgroupmark + " LENGTH:" + fundgroupmark.length()));
                tt.setFUNDGROUPMARK(fundgroupmark);
                
                try {
                	log.info("saveData...");
                	txnFundCompanyDao.save(tt);
                	log.info("saveData...end");
					succcnt++;
                } catch (Exception e) {
                    failcnt++;
                }
            }
            Long l = null;
			l = txnFundCompanyDao.count();
			bc.setSuccessCount(succcnt);
			bc.setFailCount(failcnt);
			bc.setTotalCount(Integer.parseInt(String.valueOf(l)));
			data.put("COUNTER", bc);
			
			batchresult.setSuccess(true);
			batchresult.setBatchName(this.getClass().getSimpleName());
			batchresult.setData(data);
			
			log.debug("C018.java okcnt:" + succcnt + " failcnt:" + failcnt);
        } catch (FileNotFoundException e) {
        	data.put("Error", "找不到檔案(" + hostpath + ")");
			batchresult.setData(data);
			batchresult.setSuccess(false);
        } catch (IOException e) {
        	data.put("Error", "讀取檔案錯誤(" + hostpath + ")");
			batchresult.setData(data);
			batchresult.setSuccess(false);
        }catch (Exception e) {
			data.put("Error", "檔案為空(" + hostpath + ")");
			batchresult.setData(data);
			batchresult.setSuccess(false);
        }finally {
            try {
                in.close();
                bin.close();
            } catch (Exception e) {
            }
            in = null;
            bin = null;
            
            return batchresult;
        } 
    }
}
