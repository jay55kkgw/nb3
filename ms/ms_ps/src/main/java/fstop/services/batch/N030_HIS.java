package fstop.services.batch;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.model.Rows;
import fstop.orm.dao.ItrN030_HISTORY_Dao;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.po.ITRN030_HISTORY;
import fstop.services.batch.annotation.Batch;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 外幣匯率更新
 * 
 * @author Owner
 * 
 */
@Slf4j
@RestController
@Batch(id = "batch.n030.his", name = "外幣歷史匯率更新", description = "外幣歷史匯率更新")
public class N030_HIS extends DispatchBatchExecute implements BatchExecute {

	// private DecimalFormat fmt = new DecimalFormat("#0");

	//改打N033收盤電文
	@Autowired
	@Qualifier("n033Telcomm")
	private TelCommExec n033Telcomm;

	@Autowired
	private ItrN030_HISTORY_Dao itrN030_HISTORY_Dao;

	@Autowired
	private SysBatchDao sysBatchDao;

	/**
	 * args[0] method name
	 */
	@RequestMapping(value = "/batch/n030_his", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	@Override
	public BatchResult execute(@RequestBody List<String> args) {
		List<String> safeArgs = ESAPIUtil.validStrList(args);
		log.info("batch N033_HIS...");
		log.info(ESAPIUtil.vaildLog("safeArgs: {}" + JSONUtils.toJson(safeArgs)));
		Map<String, Object> data = new HashMap();
		Date d = new Date();
		BatchResult batchresult = new BatchResult();
		boolean doBatch = true;
		MVHImpl result = null;
		List<Map<String, String>> saveList = new ArrayList<Map<String, String>>();
		Map<String, String> curMap = new HashMap<String, String>();
		curMap.put("01", "USD");
		curMap.put("02", "AUD");
		curMap.put("05", "CAD");
		curMap.put("06", "DEM");
		curMap.put("08", "HKD");
		curMap.put("10", "GBP");
		curMap.put("11", "SGD");
		curMap.put("12", "ZAR");
		curMap.put("13", "SEK");
		curMap.put("14", "CHF");
		curMap.put("15", "JPY");
		curMap.put("18", "THB");
		curMap.put("19", "EUR");
		curMap.put("20", "NZD");
		curMap.put("25", "CNY");
		log.info("CHECK TODAY DATA");
		doBatch = itrN030_HISTORY_Dao.doBatch( DateTimeUtils.getDateShort(d));
		log.info("Do N033 >> {}",doBatch);
		if (doBatch) {
			result = n033Telcomm.query(data);
			if ("99".equals(result.getFlatValues().get("TOPMSG"))) {
				Rows dataRows = result.getOccurs();
				List<Row> dataList = dataRows.getRows();
				for (Row each : dataList) {
					Map<String, String> poMap = new HashMap<String, String>();
					// 即期001、現金004、小額002  TYPE =0 即期 , 1=現金 , 2=小額
					if ("001".equals(each.getValue("TERM"))) {
						poMap.put("CUR", curMap.get(each.getValue("CRY")));
						poMap.put("BUY", each.getValue("BUYITR"));
						poMap.put("SELL", each.getValue("SELITR"));
						poMap.put("TYPE", "0");
						poMap.put("DATE", DateTimeUtils.getDateShort(d));
						saveList.add(poMap);
					}
					if ("004".equals(each.getValue("TERM"))) {
						poMap.put("CUR", curMap.get(each.getValue("CRY")));
						poMap.put("BUY", each.getValue("BUYITR"));
						poMap.put("SELL", each.getValue("SELITR"));
						poMap.put("TYPE", "1");
						poMap.put("DATE", DateTimeUtils.getDateShort(d));
						saveList.add(poMap);
					}
					if ("002".equals(each.getValue("TERM"))) {
						poMap.put("CUR", curMap.get(each.getValue("CRY")));
						poMap.put("BUY", each.getValue("BUYITR"));
						poMap.put("SELL", each.getValue("SELITR"));
						poMap.put("TYPE", "2");
						poMap.put("DATE", DateTimeUtils.getDateShort(d));
						saveList.add(poMap);
					}
				}
				
				for (Map<String, String> eachPo : saveList) {
					Calendar cal = Calendar.getInstance();
					eachPo.put("DATE", DateTimeUtils.getDateShort(cal.getTime()));
					ITRN030_HISTORY po = CodeUtil.objectCovert(ITRN030_HISTORY.class, eachPo);
					itrN030_HISTORY_Dao.save(po);
				}
				log.info("N033 List >> {} ", saveList);
			}
			batchresult.setBatchName(this.getClass().getSimpleName());
			if ("99".equals(result.getFlatValues().get("TOPMSG"))) {
				batchresult.setSuccess(true);
			} else {
				batchresult.setSuccess(false);
				data.put("N033_ERROR", result.getFlatValues().get("TOPMSG"));
			}

		}else {
			batchresult.setSuccess(true);
			data.put("N033_PASS", "已有今日收盤資料");
		}
		batchresult.setData(data);
		return batchresult;
	}
}
