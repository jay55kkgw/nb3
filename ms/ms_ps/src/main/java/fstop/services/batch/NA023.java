package fstop.services.batch;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.TxnCardApplyDao;
import fstop.orm.oao.Old_TxnCardApplyDao;
import fstop.orm.po.TXNCARDAPPLY;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import fstop.util.FtpBase64;
import fstop.util.FtpPathProperties;
import lombok.extern.slf4j.Slf4j;

/**
 * 更新信用卡申請狀態
 * 
 * @author Owner
 * 
 */
@Slf4j
@RestController
@Batch(id="batch.na023", name = "更新信用卡申請狀態", description = "更新信用卡申請狀態")
public class NA023  implements BatchExecute {
//	private log log = log.getlog(getClass());
	
	@Autowired
	private TxnCardApplyDao txnCardApplyDao;
	
	@Autowired
	private SysBatchDao sysBatchDao;
	
	@Value("${batch.otherfile.path}")
	private String otherpath;
	
	@Value("${eloan.ftp.ip}")
	private String eloan_ftp_ip;
	
	@Autowired
	CheckServerHealth checkserverhealth;
	
	@Autowired
	private Old_TxnCardApplyDao old_TxnCardApplyDao;
	
	@Value("${isParallelCheck}")
	private String isParallelCheck;
	
	public NA023(){}
	
	/**
	 * args[0] method name
	 */
	@RequestMapping(value = "/batch/na023", method = {RequestMethod.POST},
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {
		
		
		BatchResult batchresult = new BatchResult();
		Map<String, Object> msg = new LinkedHashMap();
		batchresult.setBatchName(getClass().getSimpleName());
		batchresult.setSuccess(true);
		
		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}
		
		
		InputStreamReader in = null;
		BufferedReader bin = null;
		InputStreamReader in1 = null;
		BufferedReader bin1 = null;		
		Date d = new Date();
		String today = DateTimeUtils.getDateShort(d);
		String time = DateTimeUtils.getTimeShort(d);
		
		log.debug("DATE = " + today);
		log.debug("TIME = " + time);
		String FullFileName=otherpath+"TOCREDITSTATUS.txt";
		FtpPathProperties nrcProperties = new FtpPathProperties();
		
		String pathname = nrcProperties.getDesPath("na022");
		boolean ftpstatus=ftpFileFromELOAN(pathname + "TOCREDITSTATUS.txt", FullFileName);		
		try {
			if(ftpstatus)
			{
				in = new InputStreamReader(new FileInputStream(FullFileName), "BIG5");		
				bin = new BufferedReader(in);
				int iAdd = 0;
				String data;			
				do {
					data = bin.readLine();
					if (data == null || data.length()==0) {
						break;
					}
					iAdd++;
				}
				while (true);
				log.debug("NA023 file size:"+iAdd);
				iAdd=iAdd/2;
				log.debug("NA023 Array size:"+iAdd);
				String[][]result = new String[iAdd][2];			
				in1 = new InputStreamReader(new FileInputStream(FullFileName), "BIG5");
				bin1 = new BufferedReader(in1);
				int record = 1;
				for(int i=0;i<iAdd;i++)
				{	
					data = bin1.readLine();
					//if (data == null || data.length()==0 || data.length()<8) {
						//break;
					//}
					if(data.indexOf("<RCVNO>")>-1)
					{	
						result[i][0]=data.substring(7);
						data = bin1.readLine();
						record++;
					}
					if(record%2==0)
					{    
						result[i][1]=data.substring(8);	
						record++;
					}
				
				
					log.debug("RCVNO = " + result[i][0]);
					log.debug("STATUS = " + result[i][1]); 
				}
				for(int i=0;i<iAdd;i++)
				{
					TXNCARDAPPLY user = txnCardApplyDao.setStatus(result[i][0],result[i][1]);
					if ("Y".equals(isParallelCheck)) {
						if(null==user) {
							old_TxnCardApplyDao.setStatus(result[i][0],result[i][1]);
						}
					}
				}
		    
			}
			else {
				log.error("FTP get TOCREDITSTATUS.txt  fail");
				msg.put("Error", "FTP get TOCREDITSTATUS.txt  fail");
				batchresult.setSuccess(false);
			}
		}
		catch(FileNotFoundException e) {
			msg.put("Error", "找不到對應的 ELOAN回饋檔(" + FullFileName + ")");
			batchresult.setSuccess(false);
			
//			throw new MsgTemplateNotFoundException("找不到對應的 ELOAN回饋檔(" + FullFileName + ").", e);
		}
		catch(IOException e) {
			msg.put("Error", "讀取 ELOAN回饋檔錯誤(" + FullFileName + ")");
			batchresult.setSuccess(false);
			
//			throw new ToRuntimeException("讀取 ELOAN回饋檔錯誤(" + FullFileName + ").", e);
		}
		finally {
			try {
				in.close();
				in1.close();
				bin.close();
				bin1.close(); 
			} catch(Exception e){}
			in = null;
			in1 = null;
		}
		
		batchresult.setData(msg);
		
		try {
			if (batchresult.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName, batchresult.getData());
			}
		} catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}
		
		
		return batchresult;

	}
	public boolean ftpFileFromELOAN(String eloanPath, String localPath) {
		//boolean isSuccess = false;
		
		log.info("@FTP@ " + " " + localPath + " " + eloanPath);
			String ip = eloan_ftp_ip;
			FtpBase64 ftpBase64 = new FtpBase64(ip);
			int rc=ftpBase64.download(eloanPath, localPath);
			if(rc!=0)
			{
				log.debug("FTP 下載失敗(ELOAN 主機一次) FILE："+eloanPath);
//				CheckServerHealth checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
				checkserverhealth.sendUnknowNotice("FTP 下載失敗(ELOAN 主機一次) FILE："+eloanPath);
				rc=ftpBase64.download(eloanPath, localPath);
				if(rc!=0)
				{	
					log.debug("FTP 下載失敗(ELOAN 主機二次) FILE："+eloanPath);
//					checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
					checkserverhealth.sendUnknowNotice("FTP 下載失敗(ELOAN 主機二次) FILE："+eloanPath);					
					return(false);
				}
				else
					return(true);
			}
		return true;
	}	
}
