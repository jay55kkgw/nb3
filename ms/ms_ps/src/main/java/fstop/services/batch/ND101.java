package fstop.services.batch;

import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.model.Rows;
import fstop.model.TelcommResult;
import fstop.orm.dao.SysParamDataDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.SYSPARAMDATA;
import fstop.orm.po.TXNUSER;
import fstop.services.impl.N660;
import fstop.telcomm.TelCommExec;
import fstop.util.*;
import lombok.extern.slf4j.Slf4j;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.owasp.esapi.SafeFile;
import org.owasp.esapi.errors.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;

import com.netbank.util.ESAPIUtil;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 台幣預約轉帳
 * 
 * @author Owner
 * 
 */
@Slf4j
public class ND101  implements BatchExecute {
//	private Logger logger = log.getLogger(getClass());

	private DecimalFormat fmt = new DecimalFormat("#0");
	
//	private int dataexist;

	@Autowired
	private TelCommExec nd10Telcomm;

	@Autowired
	private TxnUserDao txnUserDao;

	//private AdmCustTelnoDao admCustTelnoDao;

	@Autowired
	private SysParamDataDao sysParamDataDao;
	
//	@Autowired
//	private PathSetting pathSetting;
	
	@Autowired
	private N660 n660;
	
//	@Autowired
	private Map setting;
	
//	@Required
//	public void setSysParamDataDao(SysParamDataDao sysParamDataDao) {
//		this.sysParamDataDao = sysParamDataDao;
//	}
//
//	@Required
//	public void setTxnUserDao(TxnUserDao txnUserDao) {
//		this.txnUserDao = txnUserDao;
//	}
///*
//	@Required
//	public void setAdmCustTelnoDao(AdmCustTelnoDao admCustTelnoDao) {
//		this.admCustTelnoDao = admCustTelnoDao;
//	}
//*/
//	@Required
//	public void setSetting(Map setting) {
//		this.setting = setting;
//	}
//
//	@Required
//	public void setN660(N660 n660) {
//		this.n660 = n660;
//	}
//
//	@Required
//	public void setNd10Telcomm(TelCommExec telCommExec) {
//		this.nd10Telcomm = telCommExec;
//	}
//
//	@Required
//	public void setPathSetting(PathSetting pathSetting) {
//		this.pathSetting = pathSetting;
//	}
	
	
	/**
	 * args[0] method name
	 */
	public BatchResult execute(List<String> args) {
		int rc=0;
		int failcnt=0,okcnt=0,totalcnt=0;
		Date d = new Date();
		String today = DateTimeUtils.getCDateShort(d);
		String time = DateTimeUtils.getTimeShort(d);
		
//		dataexist=0;
		log.debug(ESAPIUtil.vaildLog("today = " + today));
		Hashtable params = new Hashtable();
		params.put("DATE", today);
		params.put("TIME", time);
		
		TelcommResult result = nd10Telcomm.query(params);
		
		FtpPathProperties nrcpro=new FtpPathProperties();
		String srcpath=nrcpro.getSrcPath("nd10xml").trim();
		log.debug(ESAPIUtil.vaildLog("srcpath = " + srcpath));
		String despath=nrcpro.getDesPath("nd10xml1").trim();
		log.debug(ESAPIUtil.vaildLog("despath = " + despath));
		
		//Absolute Path Traversal
		String LFILENAME = srcpath + today + "_nd10.xml";
		//
		
		File tmpFile = new File(LFILENAME);
		tmpFile.setWritable(true,true);
		tmpFile.setReadable(true, true);
		tmpFile.setExecutable(true,true);
		Hashtable rowcnt=new Hashtable();
		genernateFile(tmpFile, result,rowcnt);
		
		failcnt=((Integer)rowcnt.get("FAILCNT")).intValue();
		okcnt=((Integer)rowcnt.get("OKCNT")).intValue();
		totalcnt=((Integer)rowcnt.get("TOTALCNT")).intValue();
		log.debug("failcnt = " + failcnt);
		log.debug("okcnt = " + okcnt);
		log.debug("totalcnt = " + totalcnt);
		BatchCounter bcnt=new BatchCounter();
		bcnt.setFailCount(failcnt);
		bcnt.setSuccessCount(okcnt);
		bcnt.setTotalCount(totalcnt);
		//"trademsg.ok"
		
		//FTP File To Server
		
		if(okcnt>0)
		{
			try {
				ftpToServer(tmpFile,"");
			} catch (FileNotFoundException e) {
				log.error("找不到檔案.", e);
			}
		}
		
		//TODO 不刪
		//tmpFile.delete();
		
		Map<String, Object> data = new HashMap();

		data.put("COUNTER", bcnt);

		BatchResult batchresult = new BatchResult();
		batchresult.setSuccess(true);
		batchresult.setBatchName(getClass().getSimpleName());
		batchresult.setData(data);

		return batchresult;

	}
	
	private void genernateFile(File f, MVHImpl result,Hashtable rowcnt) {
		
		Row rr = null;
		int emailaddexist=0;
		
		Integer	totalcnt=0,failcnt=0,okcnt=0;
		Pattern regex_time = Pattern.compile("(\\d\\d/\\d\\d\\s+\\d\\d:\\d\\d)", Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
		Pattern regex = Pattern.compile("(\\d\\d/\\d\\d\\s\\s\\d\\d:\\d\\d)([^\\(]+)\\((.+)\\)", Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
		Pattern regex_nq = Pattern.compile("(\\d\\d/\\d\\d\\s\\s\\d\\d:\\d\\d)([^\\(]+)", Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
		
		final Element rootElement = new Element("NoXML");
		final Document document = new Document(rootElement); 
		 
		Rows rows = result.getOccurs();
		
		if(rows.getSize() == 0) {
			log.info("無回傳結果, 不產生檔案.");
		}
		
//		log.debug("---------------------");
//		for(int d = 0; d < rows.getSize(); d++) {
//			log.debug(d + " :" + rows.getRow(d).getValue("DATA"));
//		}
//		log.debug("---------------------");
		
		int i = 0;
		int rowNum = 1;
		StringBuffer sb = new StringBuffer();
		sb.setLength(0);
		String Oldcusidn="XXXXXXXXXX";
		Element noInfo=null;
		String oldtext="";
		while(true && rows.getSize()>0) {

			if(rows.getSize()==i)
			{
				noInfo = new Element("NoInfo");
				oldtext +="</table></td></tr>";
				Element nomessage=null;
				try
				{
					nomessage = new Element("NoMessage").setText(oldtext);
				}
				catch(Exception e)
				{
					nomessage = new Element("NoMessage").setText("");
				}
				noInfo.addContent(nomessage);
				if(emailaddexist >= 1)
					rootElement.addContent(noInfo);
				else
					log.error("The email is not exist.");
				break;
			}

			Row r = rows.getRow(i);
			Map<String, String> rm = new HashMap();
			
			String data = StrUtils.trim(r.getValue("DATA"));
			String type = StrUtils.trim(r.getValue("TYPE"));
			if(regex_time.matcher(data).find()) {
				Matcher matcher = regex.matcher(data);
				Matcher matcher_nq = regex_nq.matcher(data);
				if(matcher.find()) {
					String group0 = matcher.group(0);
					String time = matcher.group(1);
					String subtype = matcher.group(3);
					String subtype_desc = matcher.group(2);
					
					String name = "";
					TXNUSER user = null;
					List<TXNUSER> listtxnUser;
					try {
						log.debug("CUSIDN = " + r.getValue("CUSIDN"));
						listtxnUser=txnUserDao.findByUserId(r.getValue("CUSIDN"));
						user = listtxnUser.get(0);
						name = user.getDPUSERNAME();
					}
					catch(Exception e){
						log.error("無法取得 USER ID (" + r.getValue("CUSIDN") + ")," + e.getMessage());
						name = "";
					}
					//20120105 By Sox 瀘掉ＮＵＬＬ　－－＞　BYTE值＝0 [NULL] 換成 BYTE值＝20 [空白]  
					try {
						byte[] namefilter = name.getBytes("Big5");
						String checknull="N";
						for(int j=0;j<namefilter.length;j++)
						{
							if(namefilter[j]==0)
							{
								namefilter[j]=(byte)Integer.parseInt("20",16);
								checknull="Y";
							}		
						}
						if(checknull.equals("Y"))
							name=new String(namefilter,"Big5");
					}catch(Exception e) {
						name="";
					}
					log.debug(ESAPIUtil.vaildLog("name ===" + name +"==="));
					rm.put("CUSIDN", r.getValue("CUSIDN"));
					rm.put("ADDR", r.getValue("ADDR"));
					rm.put("NAME", name);
					rm.put("TIME", time);
					rm.put("TYPE", type);
					rm.put("SUBTYPE", subtype);
					rm.put("SUBTYPE_DESC", subtype_desc);
					String realData = StrUtils.trim(data.substring(group0.length()));
					sb.append(realData).append("\r\n");
				}
				else if(matcher_nq.find())
				{
					log.debug(ESAPIUtil.vaildLog("groupCount = " + matcher_nq.groupCount()));
					String group0 = matcher_nq.group(0);
					log.debug(ESAPIUtil.vaildLog("group group0 = " + group0));
					String time = matcher_nq.group(1);
					log.debug(ESAPIUtil.vaildLog("group time = " + time));
					String realData = StrUtils.trim(data.substring(time.length()));
					log.debug(ESAPIUtil.vaildLog("group realData = " + realData));
					String[] totadesc = realData.split(" ",2);
					log.debug(ESAPIUtil.vaildLog("totadesc.length = " + totadesc.length));
					for(int j=0;j<totadesc.length;j++)
					{
						log.debug("totadesc[" + j + "] = " + totadesc[j]);
					}
					Hashtable totadesctable=new Hashtable();
					n660.convmemo(totadesc[0],totadesctable);
					rm.put("CUSIDN", r.getValue("CUSIDN"));
					rm.put("ADDR", r.getValue("ADDR"));
					String name = "";
					TXNUSER user = null;
					List<TXNUSER> listtxnUser;
					try {
						log.debug("CUSIDN = " + r.getValue("CUSIDN"));
						listtxnUser=txnUserDao.findByUserId(r.getValue("CUSIDN"));
						user = listtxnUser.get(0);
						name = user.getDPUSERNAME();
					}
					catch(Exception e){
						log.error("無法取得 USER ID (" + r.getValue("CUSIDN") + ")," + e.getMessage());
						name = "";
					}
					//20120105 By Sox 瀘掉ＮＵＬＬ　－－＞　BYTE值＝0 [NULL] 換成 BYTE值＝20 [空白]  
					try {
						byte[] namefilter = name.getBytes("Big5");
						String checknull="N";
						for(int j=0;j<namefilter.length;j++)
						{
							if(namefilter[j]==0)
							{
								namefilter[j]=(byte)Integer.parseInt("20",16);
								checknull="Y";
							}		
						}
						if(checknull.equals("Y"))
							name=new String(namefilter,"Big5");
					}catch(Exception e) {
						name="";
					}
					log.debug(ESAPIUtil.vaildLog("name ===" + name +"==="));
					rm.put("NAME", name);
					rm.put("TIME", time);
					rm.put("TYPE", type);
					rm.put("SUBTYPE", (String)totadesctable.get("MEMO"));
					rm.put("SUBTYPE_DESC", (String)totadesctable.get("MEMODESC"));
					rm.put("DATA", totadesc[1]);
					sb.append(totadesc[1]).append("\r\n");
				}
				else
				{
					rm.put("TIME", "");
					rm.put("SUBTYPE_DESC", "");
					rm.put("SUBTYPE", "");
				}
				
				do {
					i++;
					if(i >= rows.getSize()) {
						i--;
						break;
					}
					Row _data_r = rows.getRow(i);
					String _data_data = StrUtils.trim(_data_r.getValue("DATA"));
					if(regex_time.matcher(_data_data).find()) {
						i--;
						break;
					}
					else {
						sb.append(_data_data).append("\r\n");
					}
				}
				while(true);
				
				rm.put("DATA", sb.toString());
				
			}
			else
			{
				rm.put("DATA", data);
			}
			
			if(!Oldcusidn.equals(r.getValue("CUSIDN")))
			{
				if(!Oldcusidn.equals("XXXXXXXXXX"))
				{
					Element nomessage=null;
					oldtext +="</table></td></tr>";
					log.debug("oldtext Oldcusidn <> CUSIDN = " + Oldcusidn + " oldtext = "+ oldtext);
					try
					{
						nomessage = new Element("NoMessage").setText(oldtext);
					}
					catch(Exception e)
					{
						nomessage = new Element("NoMessage").setText("");
					}
					noInfo.addContent(nomessage);
					if(emailaddexist>=1)
						rootElement.addContent(noInfo);
					else
						log.error("The email address is not exist.");
					oldtext="";
				}
				/*
				String telno=null;
				Hashtable<String, String> params = new Hashtable();
				String cusidn=r.getValue("CUSIDN");
				ADMCUSTTELNO custtelno = null;
				List<ADMCUSTTELNO> listadmcusttelno;
				try
				{
					listadmcusttelno = admCustTelnoDao.findByEmpId(cusidn);
					custtelno=listadmcusttelno.get(0);
					telno=custtelno.getADMTELNO();
				}
				catch(Exception e)
				{
					telno=" ";
				}
				log.debug("telno = " + telno);
				params.put("#TELNO", telno);
				*/
				String cusidn=r.getValue("CUSIDN");
				rm.put("TITLE", " ");
				Oldcusidn=cusidn;
				rr = new Row(rm);
				noInfo = new Element("NoInfo");
				int rc=createNodeInfoElement(rootElement, rr, noInfo);
				if(rc==0)
				{
					failcnt++;
				}
				else
				{
					okcnt++;
				}
				emailaddexist=rc;
				totalcnt++;
				rm.put("TITLE", "");
				sb.setLength(0);
			}

			rr = new Row(rm);
			oldtext += genernateMessageElement(rr);

			sb.setLength(0);
			rowNum++;
			i++;
			if(i >= rows.getSize())
			{
				oldtext +="</table></td></tr>";
				Element nomessage=null;
				try
				{
					nomessage = new Element("NoMessage").setText(oldtext);
				}
				catch(Exception e)
				{
					nomessage = new Element("NoMessage").setText("");
				}
				noInfo.addContent(nomessage);
				if(emailaddexist>=1)
					rootElement.addContent(noInfo);
				else
					log.error("The email address is not exist.");
				break;
			}
		}
		 
		rowcnt.put("OKCNT",okcnt);
		rowcnt.put("FAILCNT",failcnt);
		rowcnt.put("TOTALCNT",totalcnt);
		f.setWritable(true,true);
		f.setReadable(true, true);
		f.setExecutable(true,true);
		OutputXML(document, f);
	}

	private int createNodeInfoElement(final Element rootElement, Row r, Element noInfo) {

		 
//		 Element noInfo = new Element("NoInfo");
		 //int dpbillexist=0;
		 Element noflag = new Element("Noflag").setText("1");
		 noInfo.addContent(noflag);
		 
		 String cusidn = r.getValue("CUSIDN");
		 log.debug("createNodeInfoElement cusidn = " + cusidn);
		 Element noid = new Element("NoID").setText(cusidn);
		 noInfo.addContent(noid);

		 Element noname = new Element("NoNAME").setText(r.getValue("NAME"));
		 noInfo.addContent(noname);

		 String gender = "";
		 try {
			 if(cusidn.length()==10)
			 {
				 gender = cusidn.substring(1,2);
				 if("1".equals(gender) || "A".equals(gender) || "C".equals(gender))
					 gender = "M";
				 else if("2".equals(gender) || "B".equals(gender) || "D".equals(gender))
					 gender = "F";
				 else
					 gender = "M";
			 }
			 else
			 {
				 gender = "C";
			 }
		 }
		 catch(Exception e){}
		 log.debug("createNodeInfoElement NoSEX = " + gender);
		 Element nosex = new Element("NoSEX").setText(gender);
		 noInfo.addContent(nosex);

		 String email = r.getValue("ADDR");
		 if(email==null)
			 email="";
		 if("02".equals(r.getValue("TYPE"))) {
			 TXNUSER user = null;
			 List<TXNUSER> listtxnUser;
			try {
				log.debug("取得 USER 物件, #" + cusidn);
				listtxnUser=txnUserDao.findByUserId(cusidn);
				user = listtxnUser.get(0);
				email = user.getDPMYEMAIL();
				/* 20130719 By Sox 不用判斷NNB有沒有申請電子對帳單，因要寄電話銀行的對帳單 
				if(user.getDPBILL().equals("Y"))
					dpbillexist=1;
				*/	
			}
			catch(Exception e){
				log.error("無法取得 USER 的 Email (" + cusidn + "), " + e.getMessage());
				email = "";
			}
		 }
		 
		 log.debug(ESAPIUtil.vaildLog("createNodeInfoElement NoEMAIL = " + email));
		 Element noemail = new Element("NoEMAIL").setText(email.trim());
		 log.debug(ESAPIUtil.vaildLog("noemail.getText() = " + noemail.getText()));
		 noInfo.addContent(noemail);
		 //if(email.equals("") || dpbillexist==0)   //20130719 By Sox 不用判斷NNB有沒有申請電子對帳單，因要寄電話銀行的對帳單 
		if(email.equals(""))
		 	return(0);
		 else
			 return(1);
	}

	
	private String genernateMessageElement(Row r) {
		String n = "\r\n";
		String text=new String();
		if(r.getValue("TITLE")!=null)
		{
			text=r.getValue("TITLE");
			text += " <style type=\"text/css\">" + n +
				"<!--" + n +
				".p12 {" + n +
				"font-size: 12px;" + n +
				"line-height: 20px;" + n +
				"color: #000000;" + n +
				"}" + n +
				".bluep12 {" + n +
				"font-size: 12px;" + n +
				"line-height: 20px;" + n +
				"color: #129DE9;" + n +
				"}" + n +
				"-->" + n +
				"</style>" + n +
				"<tr>" + n +
				"<td width=\"100%\" bgcolor=\"#FFFFFF\"><div align=\"left\"><strong><span class=\"bluep12\">電子交易對帳單明細如下：</span></strong></div></td>" + n + 
				"</tr>" + n +
				"<tr>" + n +
				"<td bgcolor=\"#BBE3FF\" width=\"100%\">" + n +
				"  <table width=\"100%\" border=\"0\" align=\"center\" cellpadding=\"3\" cellspacing=\"2\" class=\"p12\">" + n + 
				"<tr  >" + n +
				"<td width=\"13%\" bordercolor=\"#C4E1F0\" bgcolor=\"#FFFFFF\"><div align=\"center\"><strong>交易時間</strong></div></td> " + n + 
				"<td width=\"15%\" bordercolor=\"#C4E1F0\" bgcolor=\"#FFFFFF\"><div align=\"center\"><strong>項目</strong></div></td> " + n + 
				"<td width=\"58%\" bordercolor=\"#C4E1F0\" bgcolor=\"#FFFFFF\"><div align=\"center\"><strong>通知內容</strong></div></td> " + n + 
				"<td width=\"14%\" bordercolor=\"#C4E1F0\" bgcolor=\"#FFFFFF\"><div align=\"center\"><strong>備註</strong></div></td> " + n + 
				"</tr>" + n ;
			r.setValue("TITLE", "");
		}
			StringReader sw = new StringReader(r.getValue("DATA"));
			BufferedReader bw = new BufferedReader(sw);
			String line = null;
			try {
				String ltime=null;
				String subtype_dasc=null;
				String lline=null;
				String subtype=null;
				while((line = bw.readLine()) != null) {
					log.debug("r.getValue(TIME) = " + r.getValue("TIME"));
					log.debug("line = " + line );
					if(r.getValue("TIME")!=null && r.getValue("TIME").length()>0)
					{
						ltime=datebrtime(r.getValue("TIME"));
						subtype_dasc=r.getValue("SUBTYPE_DESC");
						lline=line;
						subtype=r.getValue("SUBTYPE");
//						dataexist=1;
						r.setValue("TIME", "");
						r.setValue("SUBTYPE_DESC", "");
						r.setValue("SUBTYPE", "");
					}
					else
					{
						lline += "<br>" + line;
					}
				}

				text += "<tr>" + n +
				"<td bordercolor=\"#C4E1F0\" bgcolor=\"#FFFFFF\"><div align=\"center\"><p>" + ltime + "</p></div></td>" + n +
				"<td bordercolor=\"#C4E1F0\" bgcolor=\"#FFFFFF\"><div align=\"center\">" + subtype_dasc + "</div></td>" + n +
				"<td bordercolor=\"#C4E1F0\" bgcolor=\"#FFFFFF\"><p>" + lline + "</p></td>" + n + 
				"<td bordercolor=\"#C4E1F0\" bgcolor=\"#FFFFFF\"><div align=\"center\">" + subtype + "</div></td>" + n +
				"</tr>";

			}
			catch(IOException e)
			{
				log.error("genernateMessageElement error = " + e.getMessage());
			}
			/**
			 			"<td align='right'>1</td> " + n + 
			"<td >08/08 18:07轉入定存(VO) 轉出帳號: 001122XXX10存入金額: $10,000</td>" + n + 
			"</tr>" + n + "<tr>" + n + "<td align='right'>2</td> "
				+ n
				+ "<td >定期存款帳號: 001120XXX51 存單號碼: 0000010 機動利率 2.685%</td> "
				+ n + "</tr>" + n + 

			 */
			return(text);
	}

	public void OutputXML(Document docXML, File outputFile) {
		outputFile.setWritable(true,true);
		outputFile.setReadable(true, true);
		outputFile.setExecutable(true,true);
		Format fmt = Format.getPrettyFormat();
		fmt = fmt.setEncoding("big5");
		XMLOutputter xmlout = new XMLOutputter(fmt);
		OutputStreamWriter fwXML = null;
		try {
			fwXML = new OutputStreamWriter(new FileOutputStream(outputFile), "BIG5");
			xmlout.output(docXML, fwXML);
			fwXML.close();
		} catch (IOException e) {
			log.error("寫入XML有誤.", e);
		} finally {
			try {
				fwXML.close();
			} catch (Exception e) {
			}
		}

	}
	
	public synchronized int ftpToServer(File f, String uuid) throws FileNotFoundException {
		f.setWritable(true,true);
		f.setReadable(true, true);
		f.setExecutable(true,true);
		String okname="";
		String xmlname="";
		String filename="";
		if(uuid.equals(""))
		{
			okname = "trademsg.ok";
			xmlname = "trademsg.xml";
			filename = "nd10";
		}
		else
		{
			okname = uuid + ".ok";
			xmlname = uuid + ".xml";
			filename = "n660";
		}
		log.debug(ESAPIUtil.vaildLog("okname = " + okname));
		FtpPathProperties nrcpro=new FtpPathProperties();
		String srcpathok=nrcpro.getSrcPath(filename + "ok").trim();
		log.debug(ESAPIUtil.vaildLog("srcpathok = " + srcpathok + okname));
		
		//Absolute Path Traversal
		String LFILENAME = srcpathok + okname;
		//
		try {
		File tmpOkFile = new SafeFile(LFILENAME);
		tmpOkFile.setWritable(true,true);
		tmpOkFile.setReadable(true, true);
		tmpOkFile.setExecutable(true,true);
		//Path p = tmpOkFile.toPath();
		//FileWriter fw = null;
		
			//fw = new FileWriter(tmpOkFile);
			//fw.write(fmt.format(f.length()));
			//Files.write(p, fmt.format(f.length()).toString().getBytes("BIG5"));
		BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(tmpOkFile));
	    byte[] strToBytes = fmt.format(f.length()).getBytes("BIG5");
	    outputStream.write(strToBytes);
	    outputStream.close();
//		finally {
//			try {
//				fw.close();
//			}
//			catch(Exception e){}
//		}
		log.debug(ESAPIUtil.vaildLog("FullPath = " + f.getName()));
		log.debug(ESAPIUtil.vaildLog("tmpOkFile = " + okname));

		String ip = (String)setting.get("ftp.ip");
		String despathxml=nrcpro.getDesPath(filename + "xml1").trim();
		log.debug(ESAPIUtil.vaildLog("despathxml = " + despathxml + xmlname));

		FtpBase64 ftpBase64 = new FtpBase64(ip);
		int rc=ftpBase64.upload(despathxml + xmlname, new FileInputStream(f));
			//                      目地               來源
		if(rc!=0)
		{
			log.error(filename + " FTP 失敗(new BillHunter 主機)");
			CheckServerHealth checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
			SYSPARAMDATA po = null;
			try
			{
				po = sysParamDataDao.findById("NBSYS");
			}
			catch(Exception e1)
			{
				log.error("Query NBSYS table error" + e1);
				Map<String, Object> data = new HashMap();
				data.put("COUNTER", new BatchCounter());
				data.put("ERRMSG", filename + " FTP fail(new BillHunter host ) QUERY sysParamDataDao FAIL");

				BatchResult batchresult = new BatchResult();
				batchresult.setSuccess(false);
				batchresult.setBatchName(getClass().getSimpleName());
				batchresult.setData(data);
				return(1);
			}

			String emailap=po.getADAPMAIL() ;

			checkserverhealth.sendUserNotice(filename + " FTP 失敗(new BillHunter 主機)",emailap.trim(),"batch");
			return(1);
		}
			
		String despathok=nrcpro.getDesPath(filename + "ok1").trim();
		log.debug(ESAPIUtil.vaildLog("despathok = " + despathok + okname ));

		rc=ftpBase64.upload(despathok + okname, new FileInputStream(tmpOkFile));
		//                      目地               來源
		if(rc!=0)
		{
			log.error("ND10 FTP 失敗(new BillHunter 主機)");
			CheckServerHealth checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
			SYSPARAMDATA po = null;
			try
			{
				po = sysParamDataDao.findById("NBSYS");
			}
			catch(Exception e1)
			{
				log.error("Query NBSYS table error" + e1);
				Map<String, Object> data = new HashMap();
				data.put("COUNTER", new BatchCounter());
				data.put("ERRMSG", filename + " FTP fail(new BillHunter host ) QUERY sysParamDataDao FAIL");

				BatchResult batchresult = new BatchResult();
				batchresult.setSuccess(false);
				batchresult.setBatchName(getClass().getSimpleName());
				batchresult.setData(data);
				return(1);
			}

			String emailap=po.getADAPMAIL() ;

			checkserverhealth.sendUserNotice(filename + " FTP 失敗(new BillHunter 主機)",emailap.trim(),"batch");
			return(1);
		}
		} catch (IOException e) {
			log.error("寫入 trademsg.ok 錯誤.", e);
		} catch (ValidationException e) {
			log.error("NewSafeFile 錯誤.", e);
		}
		return(0);
	}

	public String datebrtime(String datesptime)
	{
		String date = new String();
		String SYear = new String();
		String datefmt=null;
		Pattern regex_time = Pattern.compile("(\\d\\d/\\d\\d)(\\s+)(\\d\\d:\\d\\d)", Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
		Matcher matcher = regex_time.matcher(datesptime);

		if(matcher.find())
		{
			String group0 = matcher.group(0);
			log.debug("group0 = " + group0);
			String grpdate = matcher.group(1);
			log.debug("grpdate = " + grpdate);
			String sp = matcher.group(2);
			log.debug("sp = " + sp);
			String grptime = matcher.group(3);
			log.debug("grptime = " + grptime);
			String mm = grpdate.substring(0, 2);
			log.debug("mm = " + mm);
				
			String dd = grpdate.substring(3, 5);
			log.debug("dd = " + dd);
				
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String t = sdf.format(new Date());
			log.debug("t = " + t);
			int IYear=new Integer(t.substring(0,4));
			log.debug("IYear = " + IYear);
			int IMon=new Integer(t.substring(4,6));
			log.debug("IMon = " + IMon);
			if ( t.substring(4,6).equals("01"))
			{
				if(IMon < new Integer(mm))
				{
					IYear--;
				}
					
			}
			String t1 = "000" + (IYear-1911);
			SYear= t1.substring(t1.length() - 3);
			log.debug("SYear + mm + dd= " + SYear + "/" + mm + "/" + dd + "<br>" + grptime);
 			datefmt = SYear + "/" + mm + "/" + dd + "<br>" + grptime;
			log.debug("datefmt = " + datefmt);

		 }

		 return datefmt;
	}
}
