package fstop.services.batch;

import java.util.Calendar;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.notifier.NotifyAngent;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.VerifyMailDao;
import fstop.orm.dao.VerifyMail_His_Dao;
import fstop.orm.po.VERIFYMAIL;
import fstop.orm.po.VERIFYMAIL_HIS;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * Email驗證信過期變更狀態 預計每日 12:05 執行 , 把過期的信件狀態改為4
 *
 * @author Owner
 */
@Slf4j
@RestController
@Batch(id = "batch.expiredverifymail", name = "驗證信過期更新", description = "更新已過期的驗證信狀態")
public class ExpiredVerifyMail implements BatchExecute {

	@Autowired
	private SysBatchDao sysBatchDao;

	@Autowired
	private VerifyMailDao verifyMailDao;
	
	@Autowired
	private VerifyMail_His_Dao verifyMail_His_Dao;
	
	@Value("${ms_env}")
	private String ms_env;

	@RequestMapping(value = "/batch/expiredverifymail", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {

		log.info("batch ExpiredVerifyMail...");

		BatchResult batchresult = new BatchResult();
		Map<String, Object> data = new LinkedHashMap();

		batchresult.setBatchName(getClass().getSimpleName());
		batchresult.setSuccess(true);

		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}

		List<String> safeArgs = ESAPIUtil.validStrList(args);

		try {
			String execdate = "";
			String perioddate = "";
			// 如果有一個參數 , 抓昨天到這個參數的時間
			if (safeArgs.size() == 1) {
				log.info(ESAPIUtil.vaildLog("arg[0]: " + safeArgs.get(0)));
				perioddate = safeArgs.get(0);
			} else {
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DAY_OF_MONTH, -1);
				execdate = DateTimeUtils.getDateShort(cal.getTime());
			}

			log.info("perioddate : {}", perioddate);
			log.info("execdate : {}", execdate);

			int failcnt=0,okcnt=0,totalcnt=0;
			Hashtable rowcnt=new Hashtable();
			
			List<VERIFYMAIL> expireDatas = verifyMailDao.findByExpireDate(execdate, perioddate);
			Calendar cal = Calendar.getInstance();
			if (expireDatas.size()>0) {
				for (VERIFYMAIL eachData : expireDatas) {
					String cusidn = eachData.getCUSIDN();
					eachData.setSTATUS("4");
					eachData.setLASTDATE(DateTimeUtils.getDateShort(cal.getTime()));
					eachData.setLASTTIME(DateTimeUtils.getTimeShort(cal.getTime()));
					String hisid = eachData.getREF_HISID();


					VERIFYMAIL_HIS his_po = verifyMail_His_Dao.findById(hisid);
					his_po.setSTATUS("4");
					his_po.setLASTDATE(DateTimeUtils.getDateShort(cal.getTime()));
					his_po.setLASTTIME(DateTimeUtils.getTimeShort(cal.getTime()));
					// 寄信通知 (新舊都要寄
					Map<String, String> mailMap = null;
					mailMap = CodeUtil.objectCovert(Map.class, eachData);
					mailMap.put("EXPIREDATE_FMT", DateTimeUtils.addSlash(mailMap.get("EXPIREDATE")));
					mailMap.put("NMAIL_MASK", CodeUtil.emailMask(mailMap.get("NMAIL")));
					
					StringBuilder receviers = new StringBuilder();
					
					switch (ms_env) {
					case "D":
						receviers.append(((String) mailMap.get("NMAIL")).indexOf("mail.tbb.com.tw")!=-1?(String) mailMap.get("NMAIL"):"H15D302B@mail.tbb.com.tw");
						receviers.append(",");
						receviers.append(((String) mailMap.get("OMAIL")).indexOf("mail.tbb.com.tw")!=-1?(String) mailMap.get("OMAIL"):"H15D302B@mail.tbb.com.tw");
						break;
					case "T":
						receviers.append(((String) mailMap.get("NMAIL")).indexOf("mail.tbb.com.tw")!=-1?(String) mailMap.get("NMAIL"):"H15D302B@mail.tbb.com.tw");
						receviers.append(",");
						receviers.append(((String) mailMap.get("OMAIL")).indexOf("mail.tbb.com.tw")!=-1?(String) mailMap.get("OMAIL"):"H15D302B@mail.tbb.com.tw");
						break;
					case "P":
						receviers.append((String)mailMap.get("NMAIL")+","+(String)mailMap.get("OMAIL"));
						break;
					}
					
					try {
						verifyMailDao.update(eachData);
						verifyMail_His_Dao.update(his_po);
						
						NotifyAngent.sendNotice("EXPIREDVERIFYMAIL", mailMap, receviers.toString());
						okcnt ++ ;
						totalcnt ++;
						
					}catch (Exception e) {
						failcnt++ ;
						totalcnt++ ;
						log.error("DATABASE err , CUSIDN :{} , Error: {}", cusidn , e );
					}
					
				}
			} else {
				log.info("NO DATA : {} ~ {}" , perioddate , execdate );
			}
			
			BatchCounter bcnt=new BatchCounter();
			bcnt.setFailCount(failcnt);
			bcnt.setSuccessCount(okcnt);
			bcnt.setTotalCount(totalcnt);
			data.put("COUNTER", bcnt);
			
			batchresult.setSuccess(true);
			batchresult.setBatchName(this.getClass().getSimpleName());
			batchresult.setData(data);
			
			
		} catch (Exception e) {

			log.error("執行 Batch 有誤 !", e);
			data.put("Error", e.getMessage());
			batchresult.setSuccess(false);
			batchresult.setData(data);

		}

		return batchresult;
	}

}
