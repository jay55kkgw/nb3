package fstop.services.batch;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.orm.dao.AdmEmpInfoDao;
import fstop.orm.dao.Nb3SysOpDao;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.TxnLogDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.oao.Old_AdmEmpinfoDao;
import fstop.orm.oao.Old_TxnLogDao;
import fstop.orm.oao.Old_TxnUserDao;
import fstop.orm.old.OLD_ADMEMPINFO;
import fstop.orm.old.OLD_TXNLOG;
import fstop.orm.old.OLD_TXNUSER;
import fstop.orm.po.ADMEMPINFO;
import fstop.orm.po.NB3SYSOP;
import fstop.orm.po.TXNLOG;
import fstop.orm.po.TXNUSER;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import fstop.util.FtpBase64;
import fstop.util.FtpPathProperties;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 產生半年行內網銀交易明細資料清單至 /TBB/nb3/data/NBCCM902 並把檔案拋至報表系統
 * 
 * @author Owner 每頁45筆
 */
@Slf4j
@RestController
@Batch(id = "batch.nnbReport_M902_HO", name = "行內網銀交易明細資料清單", description = "產生半年行內網銀交易明細資料清單至 /TBB/nb3/data/NBCCM902 並把檔案拋至報表系統")
public class NNBReport_M902_HO implements BatchExecute {
	// private log log = log.getlog(getClass());

	@Value("${reportn.ftp.ip}")
	private String reportn_ftp_ip;
	@Value("${batch.hostfile.path:}")
	private String host_dir;
	@Value("${batch.localfile.path:}")
	private String path;
	@Value("${prodf_path}")
	private String profpath;
	@Autowired
	private Nb3SysOpDao nb3SysOpDao;
	@Autowired
	private TxnLogDao txnLogDao;
	@Autowired
	private TxnUserDao txnUserDao;
	@Autowired
	private AdmEmpInfoDao admEmpInfoDao;
	
	
	@Autowired
	private Old_TxnLogDao old_TxnLogDao;
	@Autowired
	private Old_TxnUserDao old_TxnUserDao;
	@Autowired
	private Old_AdmEmpinfoDao old_AdmEmpinfoDao;
	
	@Autowired
	CheckServerHealth checkserverhealth;

	@Value("${isParallelCheck}")
	private String isParallelCheck;

	@Value("${doFTP}")
	private String doFTP;

	@Autowired
	private SysBatchDao sysBatchDao;

	@RequestMapping(value = "/batch/nnbReport_M902_HO", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {
		log.info("batch NNBReport_M902_HO...");
		List<String> safeArgs = ESAPIUtil.validStrList(args);
		
		BatchResult batchresult = new BatchResult();
		Map<String, Object> data = new LinkedHashMap();
		batchresult.setBatchName(getClass().getSimpleName());
		batchresult.setSuccess(true);

		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}
		
		
		log.debug("NNBReport_M902_HO - Start execute ");
		log.debug("網銀交易明細資料  ... ");
		int failcnt = 0, okcnt = 0, totalcnt = 0;
		Hashtable rowcnt = new Hashtable();
		Hashtable<String, String> params = new Hashtable();
		params.put("TXNIDT", DateTimeUtils.format("yyMMddHHmmss", new Date()));

		try {
			exec(rowcnt);
			failcnt = ((Integer) rowcnt.get("FAILCNT")).intValue();
			okcnt = ((Integer) rowcnt.get("OKCNT")).intValue();
			totalcnt = ((Integer) rowcnt.get("TOTALCNT")).intValue();
			log.debug(ESAPIUtil.vaildLog("failcnt = " + failcnt));
			log.debug(ESAPIUtil.vaildLog("okcnt = " + okcnt));
			log.debug(ESAPIUtil.vaildLog("totalcnt = " + totalcnt));
		} catch (Exception e) {
			log.error("執行 Batch 有誤 !", e);
			data.put("Error", e.getMessage());
			batchresult.setSuccess(false);
		}

		BatchCounter bcnt = new BatchCounter();
		bcnt.setFailCount(failcnt);
		bcnt.setSuccessCount(okcnt);
		bcnt.setTotalCount(totalcnt);


		data.put("COUNTER", bcnt);
		batchresult.setData(data);
		
		log.debug("NNBReport_M902_HO - End execute ");

		try {
			if (batchresult.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName, batchresult.getData());
			}
		} catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}

		return batchresult;
	}

	private void exec(Hashtable rowcnt) throws IOException {
		Integer totalcnt = 0, failcnt = 0, okcnt = 0;
		Date d = new Date();
		java.util.Calendar calendar = java.util.Calendar.getInstance();
		calendar.setTime(d);
		String nowdate = DateTimeUtils.getDateShort(d);
		String printdate = String.valueOf(Integer.parseInt(nowdate.substring(0, 4)) - 1911) + "/"
				+ nowdate.substring(4, 6) + "/" + nowdate.substring(6, 8);
		// calendar.add(calendar.MONTH,-1);
		d = calendar.getTime();
		String lastmonth = DateTimeUtils.getDateShort(d);
		lastmonth = lastmonth.substring(0, 6) + "%";
		String year = lastmonth.substring(0, 4);
		String mon = lastmonth.substring(4, 6);
		String sDay = "";
		String eDay = "";
		String pDay = "";
		String sYear = "";
		String pYear = "";

		log.debug(ESAPIUtil.vaildLog("lastmonth = " + lastmonth));
		log.debug(ESAPIUtil.vaildLog("Integer.parseInt(mon) = " + Integer.parseInt(mon)));

		if (Integer.parseInt(mon) <= 6) {
			sYear = String.valueOf(Integer.parseInt(year) - 1);
			pYear = String.valueOf(Integer.parseInt(year) - 1911 - 1);
			sDay = sYear + "0701";
			eDay = sYear + "1231";
			pDay = pYear + "/07/01 - " + pYear + "/12/31";
		} else {
			sYear = String.valueOf(Integer.parseInt(year));
			pYear = String.valueOf(Integer.parseInt(year) - 1911);
			sDay = sYear + "0101";
			eDay = sYear + "0630";
			pDay = pYear + "/01/01 - " + pYear + "/06/30";

		}

		log.debug(ESAPIUtil.vaildLog("sDay = " + sDay + ",  eDay = " + eDay + ", pDay" + pDay));

		FileReader fileStream = new FileReader(profpath + "ALL_BRH_IP_List_HO.txt");

		// FileReader fileStream = new
		// FileReader("/TBB/nnb/cfg/ALL_BRH_IP_List_HO.txt");
		BufferedReader bufferedStream = new BufferedReader(fileStream);
		String data;
		int iADD = 0;
		int record = 0;
		do {
			data = bufferedStream.readLine();
			if (data == null) {
				break;
			}
			iADD++;
		} while (true);
		String[][] result = new String[iADD + 1][2];

		FileReader fileStream1 = new FileReader(profpath + "ALL_BRH_IP_List_HO.txt");

		// FileReader fileStream1 = new
		// FileReader("/TBB/nnb/cfg/ALL_BRH_IP_List_HO.txt");

		BufferedReader bufferedStream1 = new BufferedReader(fileStream1);
		do {
			data = bufferedStream1.readLine();
			if (data == null) {
				break;
			}
			int token = -1;
			token = data.indexOf(",");
			if (token >= 0) {
				result[record][0] = data.substring(0, token);
				data = data.substring(token + 1);
				result[record][1] = data.substring(0);
				log.trace(ESAPIUtil.vaildLog("IPLIST:" + result[record][1]));
			} else {
				break;
			}
			record++;
		} while (true);
		String despath = path + "NBCCM902";
		log.debug(ESAPIUtil.vaildLog("despath = " + despath));

		File tmpFile = new File(despath);
		tmpFile.setWritable(true, true);
		tmpFile.setReadable(true, true);
		tmpFile.setExecutable(true, true);
		tmpFile.delete();
		OutputStream fos = new FileOutputStream(tmpFile);
		BufferedWriter out = new BufferedWriter(new OutputStreamWriter(fos, "BIG5"));
		String pageHeader1 = "1 報表代號 : CCM902                         非行員使用本行電腦辦理網路銀行所有業務之轉帳類交易明細表                    週    期 / 保存年限：半年報 / 15年\n";
		String pageHeader2 = "  資料日期 : " + pDay + "                          印表日期 : " + printdate
				+ "                                        頁    次 :   ";
		String pageHeader3 = "";
		String pageFooter = "9            	          經　辦 :                    　覆　核 :                    　主　管 :\n";
		String header1 = "     電腦IP位址         配置電腦行員      交易日期及交易時間            戶名          身分證字號       交易類別          金額      檢核日期  行員是否操作   備註\n";
		String header2 = "  ----------------     -------------     ------------------   --------------------- ------------ ----------------    -----------   ---------   ---------    ----\n";
		for (int i = 0; i < record; i++) { // 以分行依序查詢
			List<TXNLOG> all = txnLogDao.findByNNBReportM902_HO(sDay, eDay, result[i][1]);
			log.trace(ESAPIUtil.vaildLog("NB3 data size >> "+ all.size()));
			if ("Y".equals(isParallelCheck)) {
				List<OLD_TXNLOG> oldall = old_TxnLogDao.findByNNBReportM902_HO(sDay, eDay, result[i][1]);
				log.trace(ESAPIUtil.vaildLog("NB2.5 data size >> "+ oldall.size()));
				for(OLD_TXNLOG old:oldall) {
					TXNLOG nnbConvert2nb3 = CodeUtil.objectCovert(TXNLOG.class, old);
					all.add(nnbConvert2nb3);
				}
			}

			TXNLOG ft;

			if (result[i][0].equals("H15")) {
				pageHeader3 = "  需求單位 : " + result[i][0] + "  網段 : " + result[i][1]
						+ "                                                                             製表單位 : 資訊部\n\n\n\n";

			} else {
				pageHeader3 = "  需求單位 : " + result[i][0]
						+ "                                                                                                        製表單位 : 資訊部\n\n\n\n";
			}

			Double totalpage = Math.ceil(((float) all.size() / (float) 37));
			String totalpage1 = totalpage.toString().substring(0, totalpage.toString().indexOf("."));
			log.trace(ESAPIUtil.vaildLog("BRH:" + result[i][0] + ",IP:" + result[i][1] + ",totalpage:" + totalpage1 + ",allsize:"
					+ all.size()));
			if (totalpage == 0) {
				out.write(pageHeader1);
				out.write(pageHeader2 + "1/1\n");
				out.write(pageHeader3);
				out.write(header1);
				out.write(header2);
				out.write(
						"                     無網銀交易紀錄                                                                      ");
				for (int k = 0; k < 37; k++) {
					out.write("\n");
				}
				String Memo = " 備註說明：\n 一、「電腦IP位址」欄位未加註\"※\"者：屬「網銀專區工作站」。\n 　　「電腦IP位址」欄位加註\"※\"者：屬「一般個人電腦」。\n 二、「網銀專區工作站」須由作業主管指定抽樣方法調閱監視錄影，作檢核註記。\n 三、有下列情形者，請營業單位於每月10日前填具「違反使用本行電腦辦理網銀交易規範情形檢討表」擬訂改善措施，\n 　　於陳報單位主管後，備文擲回數位金融部。\n （一）行員於【網銀專區工作站】辦理非本人（含親屬）網路銀行交易\n （二）於【一般個人電腦】辦理非行員網銀交易\n";
				out.write(Memo);
				out.write(pageFooter);
			} else {
				for (int l = 0; l < totalpage; l++) {
					out.write(pageHeader1);
					out.write(pageHeader2 + (l + 1) + "/" + totalpage1 + "\n");
					out.write(pageHeader3);
					out.write(header1);
					out.write(header2);
					int counter = 0;
					for (int j = l * 37; j < all.size(); j++) {
						ft = all.get(j);
						List<TXNUSER> l_User = txnUserDao.findByUserId(ft.getADUSERID());
						if ("Y".equals(isParallelCheck) && l_User.size() == 0) {
							List<OLD_TXNUSER> old_l_User = old_TxnUserDao.findByUserId(ft.getADUSERID());
							for(OLD_TXNUSER user :old_l_User) {
								log.trace(ESAPIUtil.vaildLog("Get NB2.5 User >> "+ user.getDPSUERID()));
								TXNUSER nnbConvert2nb3 = CodeUtil.objectCovert(TXNUSER.class, user);
								l_User.add(nnbConvert2nb3);
							}
						}
						TXNUSER u = l_User.get(0);
						String space = "";
						String userip = ft.getADUSERIP();
						space = " ※";
						for (int m = 0; m < 15 - (ft.getADUSERIP().trim().length()) * 1; m++)
							space += " ";
						userip = space + userip;
						space = "";
						String LASTDATE = ft.getLASTDATE();
						String LASTTIME = ft.getLASTTIME();

						// 判斷是否為離職行員?
						String lDate = "";
						lDate = String.valueOf(Integer.parseInt(LASTDATE.substring(0, 4)) - 1911)
								+ LASTDATE.substring(4, 8);

						log.trace(ESAPIUtil.vaildLog("lDate = " + lDate));
						log.trace(ESAPIUtil.vaildLog("ft.getADUSERID() = " + ft.getADUSERID()));

						List<ADMEMPINFO> Lemp = admEmpInfoDao.findLeaveEmp(ft.getADUSERID(), lDate);
						if ("Y".equals(isParallelCheck) && Lemp.size() == 0) {
							List<OLD_ADMEMPINFO> old_l_Lemp = old_AdmEmpinfoDao.findLeaveEmp(ft.getADUSERID(), lDate);
							for(OLD_ADMEMPINFO old_l_user :old_l_Lemp) {
								log.trace(ESAPIUtil.vaildLog("Get NB2.5 ADMEMPINFO >> "+ old_l_user.getCUSID()));
								ADMEMPINFO nnbConvert2nb3 = CodeUtil.objectCovert(ADMEMPINFO.class, old_l_user);
								Lemp.add(nnbConvert2nb3);
							}
						}
						log.trace(ESAPIUtil.vaildLog("Lemp.size() = " + Lemp.size()));
						// 若有小於離職日ldate的資料，忽略
						if (Lemp.size() > 0) {
							continue;
						}

						String tempyear = String.valueOf(Integer.parseInt(LASTDATE.substring(0, 4)) - 1911) + ".";
						String tempmon = LASTDATE.substring(4, 6) + ".";
						String tempday = LASTDATE.substring(6, 8) + " ";
						String temphour = LASTTIME.substring(0, 2) + ":";
						String tempminute = LASTTIME.substring(2, 4) + ":";
						String tempsec = LASTTIME.substring(4, 6);
						String FULLDATETIME = tempyear + tempmon + tempday + temphour + tempminute + tempsec;
						for (int m = 0; m < 25 - (FULLDATETIME.trim().length()) * 1; m++)
							space += " ";
						FULLDATETIME = space + FULLDATETIME;
						space = "";
						String username = u.getDPUSERNAME();
						int word = StrUtils.countWordSpace(username);
						// log.debug("wordCount >> {}",word);

						for (int m = 0; m < 24 - word; m++)
							space += " ";
						username = space + username;
						space = "";
						String userid = ft.getADUSERID();
						for (int m = 0; m < 13 - (userid.trim().length()) * 1; m++)
							space += " ";
						userid = space + userid;
						space = "";
						// Map map = JSONUtils.json2map(ft.getADCONTENT());
						String adopid = ft.getADOPID();
						List<NB3SYSOP> l_sysop = nb3SysOpDao.findByADOPId(adopid);
						NB3SYSOP sysop = l_sysop.get(0);
						String typename = sysop.getADOPNAME();
						//因sysop menu調整而新增
						if(sysop.getURL().startsWith("/FCY") && sysop.getADOPNAME().indexOf("外幣")==-1) {
							typename = "外幣" + sysop.getADOPNAME();
						}
						if (typename.length() > 8)
							typename = typename.substring(0, 8);
						int word2 = StrUtils.countWordSpace(typename);
						for (int m = 0; m < 17 - word2; m++)
							space += " ";
						typename = space + typename;
						space = "";
						String txamt = ft.getADTXAMT();
						String adcurrency = "";
						// N174外幣定存幣別判斷
						if (ft.getADCURRENCY().indexOf("01") >= 0)
							adcurrency = "USD";
						else if (ft.getADCURRENCY().indexOf("02") >= 0)
							adcurrency = "AUD";
						else if (ft.getADCURRENCY().indexOf("05") >= 0)
							adcurrency = "CAD";
						else if (ft.getADCURRENCY().indexOf("08") >= 0)
							adcurrency = "HKD";
						else if (ft.getADCURRENCY().indexOf("10") >= 0)
							adcurrency = "GBP";
						else if (ft.getADCURRENCY().indexOf("11") >= 0)
							adcurrency = "SGD";
						else if (ft.getADCURRENCY().indexOf("12") >= 0)
							adcurrency = "ZAR";
						else if (ft.getADCURRENCY().indexOf("13") >= 0)
							adcurrency = "SEK";
						else if (ft.getADCURRENCY().indexOf("14") >= 0)
							adcurrency = "CHF";
						else if (ft.getADCURRENCY().indexOf("15") >= 0)
							adcurrency = "JPY";
						else if (ft.getADCURRENCY().indexOf("18") >= 0)
							adcurrency = "THB";
						else if (ft.getADCURRENCY().indexOf("19") >= 0)
							adcurrency = "EUR";
						else if (ft.getADCURRENCY().indexOf("20") >= 0)
							adcurrency = "NZD";
						else if (ft.getADCURRENCY().indexOf("25") >= 0)
							adcurrency = "CNY";
						else
							adcurrency = ft.getADCURRENCY();

						adcurrency = adcurrency + " ";
						String alltxamt = adcurrency + txamt;
						for (int m = 0; m < 15 - (alltxamt.trim().length()) * 1; m++)
							space += " ";
						alltxamt = space + alltxamt;
						space = "";
						for (int m = 0; m < 16; m++)
							space += " ";
						String f = userip + space + FULLDATETIME + username + userid + typename + alltxamt + "\n";
						log.debug(ESAPIUtil.vaildLog("Export File Content = " + f));
						out.write(f);
						okcnt++;
						totalcnt++;
						counter++;
						if ((all.size() % 37) != 0 && (j == all.size() - 1)) {
							int fill_space = 37 - (all.size() % 37);
							for (int k = 0; k < fill_space; k++) {
								out.write("\n");
							}
						}
						if (counter == 37)
							break;
					}
					String Memo = " 備註說明：\n 一、「電腦IP位址」欄位未加註\"※\"者：屬「網銀專區工作站」。\n 　　「電腦IP位址」欄位加註\"※\"者：屬「一般個人電腦」。\n 二、「網銀專區工作站」須由作業主管指定抽樣方法調閱監視錄影，作檢核註記。\n 三、有下列情形者，請營業單位於每月10日前填具「違反使用本行電腦辦理網銀交易規範情形檢討表」擬訂改善措施，\n 　　於陳報單位主管後，備文擲回數位金融部。\n （一）行員於【網銀專區工作站】辦理非本人（含親屬）網路銀行交易\n （二）於【一般個人電腦】辦理非行員網銀交易\n";
					out.write(Memo);
					out.write(pageFooter);
				}
			}
		}
		try {
			out.flush();
			out.close();
			if ("Y".equals(doFTP)) {
				// FTP機制啟動拋至報表系統主機
				FtpPathProperties nrcpro = new FtpPathProperties();
				String srcpath = nrcpro.getSrcPath("nnbreport_m902");
				log.debug(ESAPIUtil.vaildLog("srcpath = " + srcpath));
				// String despath1=nrcpro.getDesPath("nnbreport_m902");
				// log.debug("despath = " + despath1);
				String despath2 = nrcpro.getDesPath("nnbreport1_m902");
				log.debug(ESAPIUtil.vaildLog("despath1 = " + despath2));

				// String ip = (String)setting.get("ftp.ip");
				String ipn = reportn_ftp_ip;
				// FtpBase64 ftpBase64 = new FtpBase64(ip);
				// int rc=ftpBase64.upload(despath1, Path.dataFolder + "/NBCCM902");
				/*
				 * if(rc!=0) { log.debug("FTP 失敗(fundreport 舊主機)"); CheckServerHealth
				 * checkserverhealth =
				 * (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
				 * checkserverhealth.sendUnknowNotice("FTP 失敗(fundreport 舊主機)");
				 * log.debug("FTPSTATUS失敗"); } else { log.debug("FTPSTATUS成功"); }
				 */
				FtpBase64 ftpBase64 = new FtpBase64(ipn);
				int rc = ftpBase64.upload(despath2, path + "NBCCM902");
				if (rc != 0) {
					log.debug("FTP 失敗(fundreport 新主機)");
					// CheckServerHealth checkserverhealth =
					// (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
					checkserverhealth.sendUnknowNotice("FTP 失敗(fundreport 新主機)");
					log.debug("FTPSTATUS失敗");
				} else {
					log.debug("FTPSTATUS成功");
				}
			}
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		try {
			fos.close();
		} catch (Exception e) {
		}

		if (okcnt == 0) {
			rowcnt.put("OKCNT", okcnt);
			rowcnt.put("FAILCNT", failcnt);
			rowcnt.put("TOTALCNT", totalcnt);
			return;
		}
		rowcnt.put("OKCNT", okcnt);
		rowcnt.put("FAILCNT", failcnt);
		rowcnt.put("TOTALCNT", totalcnt);
		fileStream.close();
		bufferedStream.close();
		fileStream1.close();
		bufferedStream1.close();
	}
}
