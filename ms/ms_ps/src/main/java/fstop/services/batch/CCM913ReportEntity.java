package fstop.services.batch;

import java.util.ArrayList;
import java.util.List;

/**
 * Repot Pojo
 */
public class CCM913ReportEntity {
	
	public CCM913ReportEntity(String applyDate, String name, String ip,
			String identityId, String cardName, String secondApplyDate,
			String secondId, String secondName) {
		super();
		this.applyDate = applyDate;
		this.name = name;
		this.ip = ip;
		this.identityId = identityId;
		this.cardName = cardName;
		this.secondApplyDate = secondApplyDate;
		this.secondId = secondId;
		this.secondName = secondName;
	}
	
	public CCM913ReportEntity() {
	}

	private String applyDate;
    private String name;
    private String ip;
    private String identityId;
    private String cardName;
    private String secondApplyDate;
    private String secondId;
    private String secondName;
    
    
	public String getApplyDate() {
		return applyDate;
	}
	public void setApplyDate(String applyDate) {
		this.applyDate = applyDate;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getIdentityId() {
		return identityId;
	}
	public void setIdentityId(String identityId) {
		this.identityId = identityId;
	}
	public String getCardName() {
		return cardName;
	}
	public void setCardName(String cardName) {
		this.cardName = cardName;
	}
	public String getSecondId() {
		return secondId;
	}
	public void setSecondId(String secondId) {
		this.secondId = secondId;
	}
	public String getSecondName() {
		return secondName;
	}
	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}
    
	public List<CCM913ReportEntity> getTestDataList() {
		List<CCM913ReportEntity> entitys = new ArrayList<CCM913ReportEntity>();
        entitys.add(new CCM913ReportEntity("2020.07.07","客戶名","10.10.116.19","A222222227","萬事達卡","2020.07.07","B122222227","不同名"));
        entitys.add(new CCM913ReportEntity("2020.07.11","客戶名","10.10.116.19","A222222227","萬事達卡","2020.07.07","B222222227","不同名"));
        entitys.add(new CCM913ReportEntity("2020.07.15","客戶名","10.10.116.19","A222222227","萬事達卡","2020.07.07","B322222227","不同名"));
        entitys.add(new CCM913ReportEntity("2020.07.19","客戶名","10.10.116.19","A222222227","萬事達卡","2020.07.07","B422222227","不同名"));
        entitys.add(new CCM913ReportEntity("2020.07.22","客戶名","10.10.116.19","A222222227","萬事達卡","2020.07.07","B522222227","不同名"));
        entitys.add(new CCM913ReportEntity("2020.07.11","客戶名","10.10.116.19","A222222227","萬事達卡","2020.07.07","B222222227","不同名"));
        entitys.add(new CCM913ReportEntity("2020.07.15","客戶名","10.10.116.19","A222222227","萬事達卡","2020.07.07","B322222227","不同名"));
        entitys.add(new CCM913ReportEntity("2020.07.19","客戶名","10.10.116.19","A222222227","萬事達卡","2020.07.07","B422222227","不同名"));
        entitys.add(new CCM913ReportEntity("2020.07.22","客戶名","10.10.116.19","A222222227","萬事達卡","2020.07.07","B522222227","不同名"));
        entitys.add(new CCM913ReportEntity("2020.07.07","客戶名","10.10.115.161","Q123456353","JCB卡","2020.07.07","C122222227","不同名"));
        entitys.add(new CCM913ReportEntity("2020.07.07","客戶名","10.10.115.161","Q123456353","JCB卡","2020.07.07","C222222227","不同名"));
        entitys.add(new CCM913ReportEntity("2020.07.11","客戶名","10.10.115.161","Q123456353","JCB卡","2020.07.07","C322222227","不同名"));
        entitys.add(new CCM913ReportEntity("2020.07.12","客戶","255.255.255.1","Q123456353","JCB卡","2020.07.07","C422222227","不同名"));
        entitys.add(new CCM913ReportEntity("2020.07.22","客戶","255.255.255.1","Q123456353","JCB卡","2020.07.07","C522222227","不同名"));
        entitys.add(new CCM913ReportEntity("2020.07.12","客","25.25.25.1","Q123456353","JCB卡","2020.07.07","C422222227","不同名"));
        entitys.add(new CCM913ReportEntity("2020.07.22","客","25.25.25.1","Q223456353","JCB卡","2020.07.07","C522222227","不同名"));
        return entitys;
	}

	public String getSecondApplyDate() {
		return secondApplyDate;
	}

	public void setSecondApplyDate(String secondApplyDate) {
		this.secondApplyDate = secondApplyDate;
	}
}
