package fstop.services.batch;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

//import org.jdom.output.XMLOutputter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.exception.UncheckedException;
import fstop.notifier.NotifyAngent;
import fstop.orm.dao.SysParamDataDao;
import fstop.orm.po.SYSPARAMDATA;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 供批次作業發送執行結果訊息通知
 * 
 * @author Owner

 * 系統狀態: 成功 失敗
 * retry 3 times
 */
@Slf4j
@RestController
@Batch(id="batch.batchmailsender", name = "供批次作業發送執行結果訊息通知", description = "供批次作業發送執行結果訊息通知")
public class BatchMailSender extends DispatchBatchExecute implements BatchExecute {
	

	@Autowired
	private SysParamDataDao sysParamDataDao;
	
	@Value("${txnMail_receiver}")
	private String txnMail_receiver;
	
	@RequestMapping(value = "/batch/batchmailsender", method = {RequestMethod.POST},
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @Override
	public BatchResult execute(@RequestBody List<String> args) {
		List<String> safeArgs = ESAPIUtil.validStrList(args);
		log.debug(ESAPIUtil.vaildLog("execute>>{}"+safeArgs));
		String methodName = safeArgs.get(0);

//		List paramList = new ArrayList(Arrays.asList(args));
		 List paramList = new ArrayList();
	        paramList.addAll(safeArgs);
		log.debug(ESAPIUtil.vaildLog("execute2>>{}"+safeArgs));
		paramList.remove(0);
//		String[] params = new String[paramList.size()];
//		paramList.toArray(params);
//		Object[] params = new Object[paramList.size()];
//		params = paramList.toArray();
		try {
//			 return (BatchResult)getClass().getMethod(methodName,  new Class[] {String[].class})
//			 .invoke(this, new Object[] {params});
			 return invoke(methodName, paramList);
		} catch (TopMessageException e) {
			throw e;
		}
		catch(UncheckedException e) {
			throw e;
		} 
//		catch (NoSuchMethodException e) {
//			log.error("DispatchService 執行有誤 !!", e);
//			throw new ServiceBindingException("無法連結對應的 method, [" + methodName + "]", e);
//		} catch (IllegalAccessException e) {
//			log.error("DispatchService 執行有誤 !!", e);
//			throw new ServiceBindingException("DispatchService 執行錯誤, [" + methodName + "]", e);
//		} catch (InvocationTargetException e) {
//			if(e.getTargetException() instanceof TopMessageException)
//				throw (TopMessageException)e.getTargetException();
//
//			throw new ServiceBindingException("DispatchService 執行錯誤 !, [" + methodName + "]", e.getTargetException());
//		}		
	}
	
	/**
	 * args[0] 為執行的批次作業狀態代碼
	 * 
	 * 0: 一扣作業已完成
     * 1: 寫入 Timestamp 檔錯誤  
	 * 2: 一扣作業尚未執行完成
	 * 3: 一扣作業已執行完成,準備開始執行二扣作業
	 * 4: 讀取 Timestamp 檔錯誤
	 * 
	 * @param args
	 * @return
	 */
	public BatchResult executeone(List<String> args) {
//		public BatchResult executeone(String[] args) {
		log.debug("executeone.args>>{}",args);
		
		boolean sendResult = false;
		try {
			String transfer_No = args.get(0);
			String txntype = args.get(1);
			String msg = args.get(2);
//			String transfer_No = args[0];
//			String txntype = args[1];
//			String msg = args[2];
				
			sendResult = send(new Long(transfer_No), txntype, msg);
		}
		catch(Exception e) {
			log.error("執行 Batch 有誤 !", e);
		}

		Map<String, Object> data = new HashMap();

		data.put("COUNTER", new BatchCounter());

		BatchResult result = new BatchResult();
		result.setSuccess(sendResult);
		result.setBatchName(this.getClass().getSimpleName());
		result.setData(data);

		return result;
	}
	
	public boolean send(Long s_id, String txntype, String msg) {
		log.debug("txnMail_receiver >>"+txnMail_receiver);
		Date startTime = new Date();		
		Hashtable<String, String> varmail = new Hashtable();
		varmail.put("SYSDATE", DateTimeUtils.format("yyyy/MM/dd", startTime));
		varmail.put("SYSTIME", DateTimeUtils.format("HH:mm:ss", startTime));
		
		String tmplateName = "";
		
		if (txntype.equals("FX")) {
			txntype = "外幣";
			tmplateName="BATCH_TXNFX_NOTICE_MAIL";
		}
		else {
			txntype = "台幣";
			tmplateName="BATCH_TXNTW_NOTICE_MAIL";
		}
		
		log.info("開始寄送批次作業結果通知: " + DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", startTime));
				
		 /* 
		  * 0: 一扣作業已完成
		  * 1: 寫入 Timestamp 檔錯誤  
		  * 2: 一扣作業尚未執行完成,不可執行二扣作業
		  * 3: 一扣作業已執行完成,準備開始執行二扣作業
		  * 4: 讀取 Timestamp 檔錯誤
		  * 5: 一扣交易通知mail
		  * 6: 寫入 failmsgfile_1call檔錯誤
		  * 7: 二扣交易通知mail
		  * 8: 寫入 failmsgfile_2call檔錯誤
		  */		
		if (s_id == 0) {			
			varmail.put("SYSSTATUS", txntype + "一扣作業已完成");
			varmail.put("ERRORMSG", "");	
		}
		else if (s_id == 1) {			
			varmail.put("SYSSTATUS", txntype + "一扣寫入 Timestamp 檔錯誤");		
			varmail.put("ERRORMSG", msg);						
		}
		else if (s_id == 2) {
			varmail.put("SYSSTATUS", txntype + "一扣作業尚未執行完成,不可執行二扣作業");		
			varmail.put("ERRORMSG", "Timestamp 檔日期 = " + msg);					
		}
		else if (s_id == 3) {
			varmail.put("SYSSTATUS", txntype + "一扣作業已執行完成,準備開始執行二扣作業");
			varmail.put("ERRORMSG", "");
		}
		else if (s_id == 4) {
			varmail.put("SYSSTATUS", txntype + "二扣讀取 Timestamp 檔錯誤");			
			varmail.put("ERRORMSG", msg);					
		}else if(s_id == 5) {
			varmail.put("CONTEXT", msg);
			varmail.put("SYSDATE", DateTimeUtils.format("yyyyMMdd", startTime));
			varmail.put("TYPE", "("+txntype+"一扣)");
			String receivers = txnMail_receiver;	
			boolean isSendSuccess = NotifyAngent.sendNotice(tmplateName, varmail, receivers, "batch");
			return isSendSuccess;
		}else if( s_id==6) {
			varmail.put("SYSSTATUS", txntype + "一扣寫入 failmsgfile_1call.txt 檔錯誤");		
			varmail.put("ERRORMSG", msg);
		}else if(s_id == 7) {
			varmail.put("CONTEXT", msg);
			varmail.put("SYSDATE", DateTimeUtils.format("yyyyMMdd", startTime));
			varmail.put("TYPE", "("+txntype+"二扣)");
			String receivers = txnMail_receiver;
			boolean isSendSuccess = NotifyAngent.sendNotice(tmplateName, varmail, receivers, "batch");
			return isSendSuccess;
		}else if( s_id==8) {
			varmail.put("SYSSTATUS", txntype + "二扣寫入 failmsgfile_2call.txt 檔錯誤");		
			varmail.put("ERRORMSG", msg);
		}else if( s_id==9) {
			varmail.put("SYSSTATUS", txntype + "一扣作業執行失敗");		
			varmail.put("ERRORMSG", msg);
		}else if( s_id==10) {
			varmail.put("SYSSTATUS", txntype + "二扣作業執行失敗");		
			varmail.put("ERRORMSG", msg);
		}
		else {
			log.info("寄送批次作業結果通知狀態異常. 狀態碼=" + s_id);
			
			return false;
		}
		
		SYSPARAMDATA po = null;				
//		po = sysParamDataDao.get("NBSYS");
		po = sysParamDataDao.findById("NBSYS");
		String receivers = po.getADAPMAIL().replace(";", ",") ;	
		
		// call BillHunter
		boolean isSendSuccess = NotifyAngent.sendNotice("BATCH_TXN_MAIL", varmail, receivers, "batch");

		log.info("結束寄送批次作業結果通知: " + DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", startTime));
		
		
		return isSendSuccess;
	}
}
