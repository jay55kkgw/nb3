package fstop.services.batch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.rest.model.CommonPools;
import com.netbank.util.ESAPIUtil;

import fstop.exception.MsgTemplateNotFoundException;
import fstop.exception.ToRuntimeException;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.TxnAcnApplyDao;
import fstop.orm.po.TXNACNAPPLY;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import lombok.extern.slf4j.Slf4j;


/**
 * 刪除數位存款帳戶申請資料批次
 * 
 * @author Owner
 * 
 */
@Slf4j
@RestController
@Batch(id="batch.delAcn", name = "", description = "數位存款帳戶註銷，中心下傳檔案，清除txnacnapply DB")
public class DelAcn  implements BatchExecute {
	
	@Autowired
	private TxnAcnApplyDao txnAcnApplyDao;
	@Autowired
    private CommonPools commonPools;
	@Value("${batch.hostfile.path}")
	private String pathName;
	
	@Value("${host.hostdata.path}")
	private String hostpath;
	
	
	@Autowired
	private SysBatchDao sysBatchDao;
	
	/**
	 * args[0] method name
	 */
	@RequestMapping(value = "/batch/delAcn", method = {RequestMethod.POST},
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {
		log.debug("DelAcn BATCH ... ");
		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}
		List<String> safeArgs = ESAPIUtil.validStrList(args);
		String FullFileName=new String();
		String HostFileName=new String();
		InputStreamReader in = null;
		BufferedReader bin = null;
		String subject=new String();
		Date d = new Date();
		String today = DateTimeUtils.getDateShort(d);
		String time = DateTimeUtils.getTimeShort(d);
		
		BatchResult batchresult = new BatchResult();
		batchresult.setBatchName(getClass().getSimpleName());
		BatchCounter bc = new BatchCounter();
		Map<String, Object> data = new HashMap();
		 
		Long LOGID ;
		String ACN="";
		log.warn("DATE = " + today);
		log.warn("TIME = " + time);
		FullFileName=pathName;
		FullFileName+="DELETEACN.txt";
		HostFileName=hostpath;
		HostFileName+="DELETEACN.txt";
		log.warn("FullFileName = " + FullFileName);
		try {
			
			
			File file= new File(FullFileName);
			//Incorrect Permission Assignment For Critical Resources
			file.setWritable(true,true);
			file.setReadable(true, true);
			file.setExecutable(true,true);
			long timestamp = file.lastModified();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String fmtDate = sdf.format(timestamp);
			log.info("File's LastModify fmt >> {}",fmtDate);
			
			if(!today.equals(fmtDate)) {
				data.put("MSG", "DELETEACN.txt MODIFY TIME NOT TODAY , SKIP EXECUTE !");
				batchresult.setSuccess(true);
				batchresult.setBatchName(this.getClass().getSimpleName());
				batchresult.setData(data);
				try {
					// 新增SYSBATCH紀錄
					sysBatchDao.finish_OK(batchName);
				}catch (Exception e2) {
					log.error("sysBatchDao.finish error >> {} ", e2.getMessage());
				}
				return batchresult;
			}
			
			in = new InputStreamReader(new FileInputStream(FullFileName), "BIG5");
			bin = new BufferedReader(in);
			
			if (!bin.ready()) {
				data.put("Error", "檔案為空(" + HostFileName + ")");
				batchresult.setSuccess(false);
				batchresult.setData(data);
				in.close();
				bin.close();
				
				try {
					// 新增SYSBATCH紀錄
					sysBatchDao.finish_FAIL2(batchName,data);
				}catch (Exception e) {
					log.error("sysBatchDao.finish error >> {} ", e.getMessage());
				}
				
				return batchresult;
			}
			
			while (true) {
				subject = ESAPIUtil.validInput(bin.readLine(), "GeneralString", true);
				if (subject == null || subject.length() == 0) {
					break;
				}
				try {
					ACN = ((String)subject.trim()).substring(0,11);
					LOGID = Long.parseLong(commonPools.applyUtils.getLOGID(ACN));
					TXNACNAPPLY po = txnAcnApplyDao.findById(LOGID);
				
					if(LOGID!=null && LOGID>0 && po!=null)
					{	
						txnAcnApplyDao.delete(po);
						bc.incSuccess();
						bc.incTotalCount();
						log.warn(ESAPIUtil.vaildLog("Del ACN = " + po.getACN()));
						
					}
					else {
						log.warn(ESAPIUtil.vaildLog("DelAcn.java：找不到對應的數位存款帳戶--"+ACN));
						bc.incFail();
						bc.incTotalCount();
					}
				}
				catch(Exception e){
					log.warn(ESAPIUtil.vaildLog("DelAcn.java：找不到對應的數位存款帳戶--"+ACN));
					bc.incFail();
					bc.incTotalCount();
				}
			}
		}
		catch(FileNotFoundException e) {
			//throw new MsgTemplateNotFoundException("找不到對應的 template 檔(" + FullFileName + ").", e);
			log.error("找不到 (" + HostFileName + ")檔案," + e.getMessage());
			data.put("Error", "找不到檔案(" + HostFileName + ")");
			batchresult.setData(data);
			batchresult.setSuccess(false);
			try {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName,data);
			}catch (Exception e2) {
				log.error("sysBatchDao.finish error >> {} ", e.getMessage());
			}
			
			return batchresult;
		}
		catch(IOException e) {
			//throw new ToRuntimeException("讀取 template 檔錯誤(" + FullFileName + ").", e);
			log.error("IOException (" + HostFileName + ")," + e.getMessage());
			data.put("Error", "讀取檔案錯誤(" + HostFileName + ")");
			batchresult.setData(data);
			batchresult.setSuccess(false);
			try {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName,data);
			}catch (Exception e2) {
				log.error("sysBatchDao.finish error >> {} ", e.getMessage());
			}
			return batchresult;
		}
		finally {
			try {
				in.close();
				bin.close();
			} catch(Exception e){}
			in = null;
			bin = null;
		}

		data.put("COUNTER", bc);

		batchresult.setSuccess(true);
		batchresult.setData(data);
		
		try {
			// 新增SYSBATCH紀錄
			sysBatchDao.finish_OK(batchName);
		}catch (Exception e2) {
			log.error("sysBatchDao.finish error >> {} ", e2.getMessage());
		}

		return batchresult;

	}
}
