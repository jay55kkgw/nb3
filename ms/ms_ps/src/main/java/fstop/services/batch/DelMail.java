package fstop.services.batch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.ESAPIUtil;

import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.TxnFxSchPayDao;
import fstop.orm.dao.TxnTwSchPayDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.TXNFXSCHPAY;
import fstop.orm.po.TXNTWSCHPAY;
import fstop.orm.po.TXNUSER;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 台幣預約轉帳
 * 
 * @author Owner
 * 
 */
@Slf4j
@RestController
@Batch(id = "batch.delmail", name = "DelMail", description = "刪除客戶已註銷的資料檔")
public class DelMail implements BatchExecute {
	// private log log = log.getlog(getClass());

	@Autowired
	private TxnUserDao txnUserDao;

	@Autowired
	private TxnTwSchPayDao txnTwSchPayDao;
    
    @Autowired
	private TxnFxSchPayDao txnFxSchPayDao;

	@Value("${batch.hostfile.path:}")
	private String batch_hostfile_path;

	@Value("${host.hostdata.path}")
	private String hhdpath;
	
	@Autowired
	private SysBatchDao sysBatchDao;

	/**
	 * args[0] method name
	 */
	@RequestMapping(value = "/batch/delMail", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {
		
		log.debug("DelMail BATCH ... ");
		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}
		
		List<String> safeArgs = ESAPIUtil.validStrList(args);
		TXNUSER user = null;
		String FullFileName = new String();
		String FullFileName_Host = new String();
		InputStreamReader in = null;
		BufferedReader bin = null;
		String subject = new String();
		String mail = new String();
		String empno = new String();
		Date d = new Date();
		String today = DateTimeUtils.getDateShort(d);
		String time = DateTimeUtils.getTimeShort(d);

		
		BatchResult batchresult = new BatchResult();
		batchresult.setBatchName(getClass().getSimpleName());
		BatchCounter bc = new BatchCounter();
		Map<String, Object> data = new LinkedHashMap();
		int twpayokcnt = 0;
		int fxpayokcnt = 0;
		int twpayfailcnt = 0;
		int fxpayfailcnt = 0;
		Hashtable params = new Hashtable();
		// params.put("DATE", today);
		// params.put("TIME", time);
		log.trace("DATE = " + today);
		log.trace("TIME = " + time);
		// FullFileName=(String)setting.get("pathname");
		// FullFileName+=setting.get("filename");
		FullFileName = batch_hostfile_path;
		FullFileName += "delmail.txt";

		// For Error log
		FullFileName_Host = hhdpath;
		FullFileName_Host += "delmail.txt";

		log.trace("FullFileName = " + FullFileName);
		try {
			
			File file= new File(FullFileName);
			//Incorrect Permission Assignment For Critical Resources
			file.setWritable(true,true);
			file.setReadable(true, true);
			file.setExecutable(true,true);
			long timestamp = file.lastModified();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String fmtDate = sdf.format(timestamp);
			log.info("File's LastModify fmt >> {}",fmtDate);
			
			if(!today.equals(fmtDate)) {
				data.put("MSG", "delmail.txt MODIFY TIME NOT TODAY , SKIP EXECUTE !");
				batchresult.setSuccess(true);
				batchresult.setBatchName(this.getClass().getSimpleName());
				batchresult.setData(data);
				try {
					// 新增SYSBATCH紀錄
					sysBatchDao.finish_OK(batchName);
				}catch (Exception e2) {
					log.error("sysBatchDao.finish error >> {} ", e2.getMessage());
				}
				return batchresult;
			}
			in = new InputStreamReader(new FileInputStream(FullFileName), "BIG5");
			bin = new BufferedReader(in);
			while (true) {
				subject = ESAPIUtil.validInput(bin.readLine(), "GeneralString", true);
				if (subject == null || subject.length() == 0) {
					break;
				}
				try {
					TXNUSER po = txnUserDao.findById((String) subject.trim());
					txnUserDao.delete(po);
					bc.incSuccess();
					bc.incTotalCount();
				} catch (Exception e) {
					log.error("找不到對應的使用者.");
					// throw new ToRuntimeException("找不到對應的使用者.");
					bc.incFail();
					bc.incTotalCount();
				}
				log.debug(ESAPIUtil.vaildLog("UID = " + subject.trim()));
				
				
				List listtw = txnTwSchPayDao.findbyUserid((String) subject.trim());
				
				if (listtw != null && listtw.size() > 0) {
					int i = 0;
					while (i < listtw.size()) {
						log.debug(
								ESAPIUtil.vaildLog(subject.trim() + " --> TXNTWSCHPAYDAO count = " + listtw.size()));
						TXNTWSCHPAY txntwsch = (TXNTWSCHPAY) listtw.get(i);
						log.debug(ESAPIUtil.vaildLog("getDPTXSTATUS = " + txntwsch.getDPTXSTATUS()));
						if (txntwsch.getDPTXSTATUS().equals("0")) {
							try {
								txnTwSchPayDao.updataDptxstatus(String.valueOf(txntwsch.getDPSCHID()), "3");
								twpayokcnt++;
							} catch (Exception e) {
								log.error("update txnTwScheduleDao DPTXSTATUS = 3 fail.");
								twpayfailcnt++;
							}
						}
						i++;
					}
				}
				List listfx = txnFxSchPayDao.findValidSchedule((String) subject.trim());
				if (listfx != null && listfx.size() > 0) {
					log.debug(ESAPIUtil.vaildLog(subject.trim() + " --> txnFxScheduleDao count = " + listfx.size()));

					int i = 0;
					while (i < listfx.size()) {
						TXNFXSCHPAY txnfxsch = (TXNFXSCHPAY) listfx.get(i);
						log.debug(ESAPIUtil.vaildLog("getFXTXSTATUS = " + txnfxsch.getFXTXSTATUS()));
						if (txnfxsch.getFXTXSTATUS().equals("0")) {
							try {
								txnFxSchPayDao.updateStatusByFxUserID((String) subject.trim(), "3");
								fxpayokcnt++;
							} catch (Exception e) {
								log.error("update txnFxSchedule DPTXSTATUS = 3 faile.");
								fxpayfailcnt++;
							}
						}
						i++;
					}
				}
			}

		} catch (FileNotFoundException e) {
			// throw new MsgTemplateNotFoundException("找不到對應的 template 檔(" + FullFileName +
			// ").", e);
			log.error("找不到 (" + FullFileName_Host + ")檔案," + e.getMessage());
			data.put("Error", "找不到檔案(" + FullFileName_Host + ")");
			batchresult.setData(data);
			return batchresult;
		} catch (IOException e) {
			// throw new ToRuntimeException("讀取 template 檔錯誤(" + FullFileName + ").", e);
			log.error("IOException (" + FullFileName_Host + ")," + e.getMessage());
			data.put("Error", "讀取檔案錯誤(" + FullFileName_Host + ")");
			batchresult.setData(data);
			
			try {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName,data);
			}catch (Exception e2) {
				log.error("sysBatchDao.finish error >> {} ", e.getMessage());
			}
			
			return batchresult;
		}catch (Exception e) {
			log.error("Error >> {} ", e.getMessage());
			data.put("Error", e.getMessage());
			batchresult.setBatchName(getClass().getSimpleName());
			batchresult.setData(data);
			
			try {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName,data);
			}catch (Exception e2) {
				log.error("sysBatchDao.finish error >> {} ", e.getMessage());
			}
			
			return batchresult;
		} finally {
			try {
				in.close();
				bin.close();
			} catch (Exception e) {
			}
			in = null;
			bin = null;
		}

		data.put("Delete_USER", bc);
		data.put("ChangedTwRows", twpayokcnt);
		data.put("ChangedFxRows", fxpayokcnt);
		data.put("FailedTwRows", twpayfailcnt);
		data.put("FailedFxRows", fxpayfailcnt);
		batchresult.setSuccess(true);

		batchresult.setData(data);
		
		try {
			// 新增SYSBATCH紀錄
			sysBatchDao.finish_OK(batchName);
		}catch (Exception e2) {
			log.error("sysBatchDao.finish error >> {} ", e2.getMessage());
		}

		return batchresult;

	}
}
