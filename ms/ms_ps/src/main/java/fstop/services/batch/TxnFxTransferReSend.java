package fstop.services.batch;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.orm.dao.DbConnectDao;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.TxnFxSchPayDataDao;
import fstop.orm.po.TXNFXSCHPAYDATA;
import fstop.orm.po.TxnFxSchPayDataErrorCount;
import fstop.services.batch.annotation.Batch;
import fstop.services.mgn.B105;
import fstop.util.CSVWriter;
import fstop.util.UrlSetting;
import lombok.extern.slf4j.Slf4j;

/**
 * 外匯預約轉帳 2扣
 *
 * @author Owner
 *
 */

@Slf4j
@RestController
@Batch(id = "batch.TxnFxTransferReSend", name = "外匯預約交易二扣", description = "外匯預約交易二扣")
public class TxnFxTransferReSend extends DispatchBatchExecute implements BatchExecute {

	@Autowired
	private UrlSetting urlSetting;

	@Autowired
	private B105 b105;

	@Autowired
	private TxnFxSchPayDataDao txnFxSchPayDataDao; // 預約資料

	@Autowired
	DbConnectDao dbConnectDao;

	@Autowired
	private SysBatchDao sysBatchDao;

	/**
	 * args[0] method name
	 */
	@RequestMapping(value = "/batch/txnfxtransferresend", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	@Override
	public BatchResult execute(@RequestBody List<String> args) {

		List<String> safeArgs = ESAPIUtil.validStrList(args);

		// log.info(ESAPIUtil.vaildLog("safeArgs: {}"+ JSONUtils.toJson(safeArgs)));
		log.info(ESAPIUtil.vaildLog("TxnFxTransferResend_Proxy"));
		// 第一個為這個 bean 的 method Name
		String methodName = safeArgs.get(0);
		List paramList = new ArrayList();
		paramList.addAll(safeArgs);
		paramList.remove(0);
		// log.info(ESAPIUtil.vaildLog("methodName:"+ methodName +" param list: " +
		// JSONUtils.toJson(paramList)));
		return invoke(methodName, paramList);

	}

	/**
	 * 取得今日所有外幣預約轉帳的交易資料
	 *
	 * @return
	 */
	public BatchResult getAllList(List<String> getAllList) {

		BatchResult batchresult = new BatchResult();
		Map<String, Object> data = new LinkedHashMap();
		batchresult.setBatchName(getClass().getSimpleName());
		batchresult.setSuccess(true);

		// 新增SYSBATCH紀錄
		try {
			sysBatchDao.initSysBatchPo("TxnFxTransferResend");
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}

		Boolean conStatus = false;
		try {
			conStatus = dbConnectDao.getDBStatus();
		} catch (Exception e1) {
			log.error("DB error");
			log.error("Retry 1 (wait 5 sec)");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e4) {
			}
			try {
				conStatus = dbConnectDao.getDBStatus();
			} catch (Exception e) {
				log.error("DB error");
				log.error("Retry 2 (wait 5 sec)");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e4) {
				}
				try {
					conStatus = dbConnectDao.getDBStatus();
				} catch (Exception e2) {
					log.error("DB error");
					log.error("Retry 3 (wait 5 sec)");
					try {
						Thread.sleep(5000);
					} catch (InterruptedException e4) {
					}
					try {
						conStatus = dbConnectDao.getDBStatus();
					} catch (Exception e3) {
						log.error("DB error");
						log.error("Stop Retry");
						batchresult.setSuccess(false);
						batchresult.setErrorCode("DB_ERROR");
						data.put("Error", "DB_ERROR");
						batchresult.setData(data);

						try {
							sysBatchDao.finish_FAIL2("TxnFxTransferResend", batchresult.getData());
						} catch (Exception e5) {
							log.error("sysBatchDao.finish error >> {} ", e5.getMessage());
						}

						return batchresult;
					}
				}
			}
		}
		log.info("外匯預約轉帳 2扣 進入getAllList()~~ 取得今日所有外匯預約轉帳 2扣的交易資料");
		Date today = new Date();
		boolean isSuccess = false;
		// StringWriter sw = new StringWriter();
		OutputStream os = new ByteArrayOutputStream();
		final StringBuffer allIds = new StringBuffer();
		try {

			// List<TXNFXSCHPAYDATA> execDatas = new ArrayList();
			List<TXNFXSCHPAYDATA> allDatas = new ArrayList();
			// 預約需先檢查今天是否為假日
			boolean isholiday = b105.isHOLIDAY(new DateTime(today).toString("yyyyMMdd"));

			if (isholiday) {
				// 不執行
				batchresult.setSuccess(isSuccess);
				batchresult.setBatchName("TxnFxTransfer_getAllList");
				batchresult.setErrorCode("isHoliday");
				batchresult.setData(data);
				log.debug("getAllList isholiday!!!!!!!!!!!!!!!!!!");
				
				try {
						sysBatchDao.finish_FAIL("TxnFxTransferResend", batchresult.getErrorCode());
				} catch (Exception e) {
					log.error("sysBatchDao.finish error >> {} ", e.getMessage());
				}
				return batchresult;
			}else{
				allDatas = txnFxSchPayDataDao.findBy_sp_cal_fxschdate_second(new DateTime(today).toString("yyyyMMdd"));
				log.debug("@@@@@@ getAllList >>>>> {}", ESAPIUtil.vaildLog(CodeUtil.toJson(allDatas)));

				// 20200210 移至一扣執行完判斷 (能做二扣的為2 , 不能的上面sql就找不到了) 二扣只需讀列表
				// for (TXNFXSCHPAYDATA po : allDatas) {
				// if ("Y".equals(admMsgCodeDao.findFXAUTORESEND(po.getFXEXCODE().trim()))) {
				// log.debug(ESAPIUtil.vaildLog("findFXAUTORESEND Y " + po.toString()));
				// execDatas.add(po);
				// continue;
				// } else {
				// log.debug(ESAPIUtil.vaildLog("findFXAUTORESEND NO Y " + po.toString()));
				// po.setFXTXSTATUS("1");
				// txnFxSchPayDataDao.update(po);
				// }
				// }

			}
			log.debug(ESAPIUtil.vaildLog("@@@@@@ getAllList execDatas >>>>>>>>>>> " + allDatas));

			CSVWriter csvWriter = null;
			OutputStreamWriter osw = null;
			try {
				osw = new OutputStreamWriter(os);
				// step2
				csvWriter = new CSVWriter(osw);

				for (TXNFXSCHPAYDATA s : allDatas) {
					csvWriter.writeNext(new String[] { s.getPks().getFXSCHNO(), s.getPks().getFXSCHTXDATE(),
							s.getPks().getFXUSERID(), s.getADTXNO() });

					csvWriter.flush();
				}

				isSuccess = true;
			} catch (Exception e) {
				log.error("寫入 FX_FXSCHNOs 時錯誤.", e);
			} finally {
				try {
					csvWriter.close();
				} catch (Exception e) {
					log.error("csvWriter 時錯誤.", e);
				}
				try {
					os.close();
					osw.close();
				} catch (Exception e) {
					log.error("bw 時錯誤.", e);
				}
			}
		} catch (Exception e) {
			log.error("執行 Batch 有誤 !", e);
		}

		data.put("COUNTER", new BatchCounter());
		data.put("ALL_ID", allIds.toString());
		data.put("FX_FXSCHNOs", os.toString()); // 原本 fx_dpschids

		batchresult.setSuccess(isSuccess);
		batchresult.setData(data);

		return batchresult;
	}

	/**
	 * fxschno 為執行的預約編號 <BR>
	 * FXSCHTXDATE 預約日期 type
	 *
	 * @return
	 */
	public BatchResult executeone(List<String> args) {

		String serviceUrl = getServiceUrl("ms_fx", "txnfxtransferresend");
		log.info(ESAPIUtil.vaildLog("Proxy to " + serviceUrl + " , methodName:executeone"));
		List<String> params = new ArrayList<>();
		params.add("executeone");
		for (String arg : args) {
			params.add(arg);
		}
		RestTemplate restTemplate = new RestTemplate();
		BatchResult result = restTemplate.postForObject(serviceUrl, params, BatchResult.class);

		return result;

	}

	/**
	 * 取得今日所有外幣預約轉帳失敗產檔的交易資料
	 *
	 * @return
	 */
	public BatchResult getErrorFileList(List<String> args) {
		boolean hasData = true;
		BatchResult result = new BatchResult();
		Map<String, String> poMap = new HashMap();
		Map<String, String> msgMap = new HashMap();
		List<List<Map<String, String>>> resultList = new LinkedList();
		List<Map<String, String>> msgList = new ArrayList();
		List<Map<String, String>> dataList = new ArrayList();
		Date today = new Date();
		String todayFormat = new DateTime(today).toString("yyyyMMdd");

		List<TxnFxSchPayDataErrorCount> errorMSGDatas = txnFxSchPayDataDao.countErrorList2nd(todayFormat);
		if (errorMSGDatas.size() != 0) {
			for (TxnFxSchPayDataErrorCount each : errorMSGDatas) {
				msgMap = CodeUtil.objectCovert(Map.class, each);
				msgList.add(msgMap);
			}

			resultList.add(msgList);
		} else {
			resultList.add(new ArrayList<>());
		}

		result.setListdata(resultList);
		result.setSuccess(hasData);
		result.setBatchName("TxnFxTransferResend_getErrorFileList");

		// hasData = false;
		// result.setListdata(resultList);
		// result.setSuccess(hasData);
		// result.setBatchName("TxnFxTransfer_getErrorFileList");

		int total = txnFxSchPayDataDao.countMethod2nd(1);
		int success = txnFxSchPayDataDao.countMethod2nd(2);
		int custom_error = txnFxSchPayDataDao.countMethod2nd(3);
		int system_error = txnFxSchPayDataDao.countMethod2nd(4);
		int total_failed = txnFxSchPayDataDao.countMethod2nd(5);

		Map<String, Object> resultMap = new HashMap();
		resultMap.put("total", String.valueOf(total));
		resultMap.put("success", String.valueOf(success));
		resultMap.put("custom_error", String.valueOf(custom_error));
		resultMap.put("system_error", String.valueOf(system_error));
		resultMap.put("total_fail", String.valueOf(total_failed));

		result.setData(resultMap);

		try {
			// 新增SYSBATCH紀錄
			sysBatchDao.finish_OK("TxnFxTransferResend");
		} catch (Exception e) {
			log.error("sysBatchDao.finish_OK error >> {} ", e.getMessage());
		}

		return result;
	}

	public String getServiceUrl(String ms, String apiName) {
		String api = ms + "/batch/" + apiName;
		if (api.startsWith("/") == false)
			api = "/" + api;
		String s = "";
		switch (ms) {
		case "ms_tw":
			s += urlSetting.getMstw_Url();
			break;
		case "ms_cc":
			s += urlSetting.getMscc_Url();
			break;
		case "ms_loan":
			s += urlSetting.getMsloan_Url();
			break;
		case "ms_pay":
			s += urlSetting.getMspay_Url();
			break;
		case "ms_fx":
			s += urlSetting.getMsfx_Url();
			break;
		}
		// log.info("service url: " + ( s + api));
		return s + api;
	}
}
