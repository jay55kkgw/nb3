package fstop.services.batch;

import java.io.File;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.ESAPIUtil;

import fstop.notifier.NotifyAngent;
import fstop.orm.dao.AdmBankDao;
import fstop.orm.dao.AdmCurrencyDao;
import fstop.orm.dao.AdmMailLogDao;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.TxnFxSchPayDataDao;
import fstop.orm.dao.TxnTwSchPayDataDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.ADMBANK;
import fstop.orm.po.ADMCURRENCY;
import fstop.orm.po.ADMMAILLOG;
import fstop.orm.po.TXNFXSCHPAYDATA;
import fstop.orm.po.TXNRECMAILLOGRELATION;
import fstop.orm.po.TXNTWSCHPAYDATA;
import fstop.orm.po.TXNUSER;
import fstop.services.NoticeInfo;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import topibs.utility.NumericUtil;

/**
 * 即將扣帳通知withdraw
 * 
 * fxnotify
 * 
 * @author Owner 主旨：預約交易 項目:預約交易（轉帳繳費稅匯出匯款）即將扣帳通知 YYY/MM/DD 09:30:00
 */

@Slf4j
@RestController
@Batch(id = "batch.txnscheduleend", name = "網路銀行預約交易到期通知", description = "預約交易（轉帳繳費稅匯出匯款）即將扣帳通知")
public class TxnSchedule implements BatchExecute {
	// private log log = log.getlog("fstop_txntw");

	private DecimalFormat fmt = new DecimalFormat("#0");

	@Autowired
	private AdmMailLogDao admMailLogDao;

	@Autowired
	private TxnTwSchPayDataDao txnTwSchPayDataDao;

	@Autowired
	private TxnFxSchPayDataDao txnFxSchPayDataDao;

	@Autowired
	private TxnUserDao txnUserDao;

	@Autowired
	private AdmBankDao admBankDao;

	@Autowired
	private AdmCurrencyDao admCurrencyDao;

	@Autowired
	private SysBatchDao sysBatchDao;

	private TXNTWSCHPAYDATA tw = new TXNTWSCHPAYDATA();

	private TXNFXSCHPAYDATA fx = new TXNFXSCHPAYDATA();

	private TXNUSER txnuser = new TXNUSER();

	@Value("${batch.localfile.path:}")
	private String path;

	public TxnSchedule() {
	}

	/**
	 * args[0] method name
	 */
	@RequestMapping(value = "/batch/txnSchedule", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {

		log.info("batch TxnSchedule...");

		BatchResult batchresult = new BatchResult();
		String batchName = getClass().getSimpleName();
		batchresult.setBatchName(batchName);
		batchresult.setSuccess(true);
		List<String> safeArgs = ESAPIUtil.validStrList(args);
		Map<String, Object> data = new LinkedHashMap();
		log.info("Check checkpoint file");
		try {
			
			File cf = new File(path , "TxnScheduleCheckPoint.txt");
			cf.setWritable(true, true);
			cf.setReadable(true, true);
			cf.setExecutable(true, true);
			//新版首次執行,沒檔案  所以直接執行
			if(!cf.exists()) {
				try {
					sysBatchDao.initSysBatchPo(batchName);
				} catch (Exception e) {
					log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
				}
				exec(batchresult, batchName , data);
			}else {
			//之後執行 , 要比對
				String str_FileDate_c = FileUtils.readFileToString(cf, "UTF-8");

				log.info(ESAPIUtil.vaildLog("網路銀行預約交易到期通知 check point 日期 : " + str_FileDate_c));

				String str_NowDate_c = new DateTime().toString("yyyy/MM/dd");

				log.info(ESAPIUtil.vaildLog("網路銀行預約交易到期通知 check point 執行日期 : " + str_NowDate_c));

				// 如果不相等 執行
				if (!str_FileDate_c.equals(str_NowDate_c)) {
					// 新增SYSBATCH紀錄
					try {
						sysBatchDao.initSysBatchPo(batchName);
					} catch (Exception e) {
						log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
					}
					exec(batchresult, batchName , data);
					
				} else {
					// 已執行過 , 但若進來的話代表沒收到result 就更新資料庫成功時間後退出
					try {
						// 新增SYSBATCH紀錄
						sysBatchDao.finish_OK(batchName);
					} catch (Exception e) {
						log.error("sysBatchDao.finish_OK error >> {} ", e.getMessage());
					}
					
				}
			}
		} catch (Exception e) {
			log.error("Read Check Point file error" + e.getMessage());
			batchresult.setSuccess(false);
			batchresult.setErrorCode("Read Check Point file error" + e.getMessage());
		}
		return batchresult;
	}

	private void exec(BatchResult bs, String batchName , Map data) {
		
		try {
			
			File f = new File(path , "TxnScheduleCheckPoint.txt");
			f.setWritable(true, true);
			f.setReadable(true, true);
			f.setExecutable(true, true);
			f.createNewFile();
			DateTime now = new DateTime();
			String str_NowDate = now.toString("yyyy/MM/dd");
			FileUtils.writeStringToFile(f, str_NowDate, "UTF-8");
			
		} catch (Exception e) {
			log.error(ESAPIUtil.vaildLog("Write checkpoint file error :" + e.getMessage()));
			bs.setSuccess(false);
			bs.setErrorCode("Write checkpoint file error:" + e.getMessage());
			return;
		}
		

		// List<TXNFXSCHEDULE> txnfxschedule;
		// List<TXNTWSCHEDULE> txntwschedule;

		List<TXNTWSCHPAYDATA> txntwschpaydata;
		List<TXNFXSCHPAYDATA> txnfxschpaydata;

		List<TXNUSER> listtxnUser;
		Map varmail = new HashMap();
		String mail = new String();
		String empno = new String();
		Date d = new Date();
		
		int twNoNeedcnt = 0;
		int twNotFoundcnt = 0;
		int twNoMailcnt = 0;
		int twSucesscnt = 0;
		int twfailcnt = 0;
		int fxNoNeedcnt = 0;
		int fxNotFoundcnt = 0;
		int fxNoMailcnt = 0;
		int fxSucesscnt = 0;
		int fxfailcnt = 0;

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Calendar c1 = Calendar.getInstance();
		log.debug("now date = " + sdf.format(d));
		c1.setTime(d);
		log.debug("Date is : " + sdf.format(c1.getTime()));

		// String txdays=(String)setting.get("twdays");
		String txdays = "3";

		c1.add(Calendar.DATE, Integer.parseInt(txdays));
		String currdays = sdf.format(c1.getTime());
		log.info("Date + 3 days is : " + sdf.format(c1.getTime()));
		// txntwschedule=txnTwScheduleDao.findScheduleDueTo(c1.getTime());
		txntwschpaydata = txnTwSchPayDataDao.findScheduleDueTo(currdays);
		log.info("txntwschedule.size() = " + txntwschpaydata.size());
		for (int i = 0; i < txntwschpaydata.size(); i++) {

			varmail.clear();
			tw = txntwschpaydata.get(i);
			listtxnUser = txnUserDao.findByUserId(tw.getPks().getDPUSERID());
			log.debug("listtxnUser.size() = " + listtxnUser.size());
			if (listtxnUser == null || listtxnUser.size() == 0) {
				log.error(ESAPIUtil.vaildLog("User ID not found = " + tw.getPks().getDPUSERID()));
				twNotFoundcnt++;
				continue;
			}
			txnuser = listtxnUser.get(0);
			log.info(ESAPIUtil.vaildLog("Tw USER:" + txnuser.getDPSUERID()));
			String dpnotify = StrUtils.trim(txnuser.getDPNOTIFY());
			Set<String> dpnotifySet = StrUtils.splitToSet("\\s*,\\s*", dpnotify); // 1 : 轉帳/繳費稅/匯出匯款成功通知, 5
																					// 預約交易（轉帳繳費稅匯出匯款）結果通知

			boolean isUserSetTxnNotify = dpnotifySet.contains("4");
			if (isUserSetTxnNotify == false) {
				twNoNeedcnt++;
				continue;
			}
			String email = txnuser.getDPMYEMAIL();
			log.info(ESAPIUtil.vaildLog("EMAIL: " + email));
			if (email == null || email.length() == 0) {
				log.debug(ESAPIUtil.vaildLog(tw.getPks().getDPUSERID() + " email not found ."));
				twNoMailcnt++;
				continue;
			}

			// String dppermtdate = tw.getDPPERMTDATE();
			// log.debug("dppermtdate len = " + dppermtdate.length());
			// if(dppermtdate.length()==1)
			// dppermtdate = "0" + dppermtdate;
			// log.debug("dppermtdate = " + dppermtdate);
			// String dpfdate=tw.getDPFDATE();
			String dpfdatefmt = new String();
			dpfdatefmt = currdays;
			/*
			 * if(dppermtdate.length()==0 || dppermtdate.equals("  "))
			 * dpfdatefmt=dpfdate.substring(0, 4) + "/" + dpfdate.substring(4, 6) + "/" +
			 * dpfdate.substring(6, 8); else dpfdatefmt=dpfdate.substring(0, 4) + "/" +
			 * currdays.substring(4, 6) + "/" + dppermtdate.substring(0, 2);
			 */
			log.trace("dpsdatefmt = " + dpfdatefmt);
			// String dpftime=tw.getDPSTIME();
			// String dpftimefmt=new String();
			// dpftimefmt=dpftime.substring(0, 2) + ":" + dpftime.substring(2, 4) + ":" +
			// dpftime.substring(4, 6);
			String dpwdac = new String();
			String dpwdacstart = tw.getDPWDAC().trim();
			dpwdac = "050-臺灣企銀  " + StrUtils.left(dpwdacstart, 6) + "***" + StrUtils.right(dpwdacstart, 2);
			String dpsvbhtext = new String();
			ADMBANK admbank = admBankDao.getbank(tw.getDPSVBH());
			dpsvbhtext = tw.getDPSVBH() + "-" + admbank.getADBANKNAME() + " ";
			String dpsvacstart = tw.getDPSVAC().trim();
			dpsvbhtext = dpsvbhtext + StrUtils.left(dpsvacstart, 6) + "***" + StrUtils.right(dpsvacstart, 2);
			;
			String dptxamt = tw.getDPTXAMT();
			String dptxamtfmt = NumericUtil.formatNumberString(dptxamt, 0);
			String dptxmemo = tw.getDPTXMAILMEMO();
			if (dptxmemo == null || dptxmemo.length() == 0)
				dptxmemo = " ";

			varmail.put("#TXNAME", "台幣轉帳");
			varmail.put("#TRANTIME", dpfdatefmt);
			// varmail.put("#SEQNO", " ");
			varmail.put("#OUTACN", dpwdac);
			varmail.put("#ACN", dpsvbhtext);
			varmail.put("#OUTCURRENCY", "新台幣");
			varmail.put("#AMT", dptxamtfmt);
			varmail.put("#MEMO", dptxmemo);
			boolean isSendSuccess = NotifyAngent.sendNotice("WITHDRAWNOTIFY", varmail, email);
			if (!isSendSuccess) {
				log.error(ESAPIUtil.vaildLog("發送 Email 失敗.(" + email + ")"));
				twfailcnt++;
			} else {
				log.debug(ESAPIUtil.vaildLog("發送 Email 成功.(" + email + ")"));
				twSucesscnt++;
			}
		}
		log.info("TW notify end");
		data.put("twNoNeedcnt", twNoNeedcnt);
		data.put("twNotFoundcnt", twNotFoundcnt);
		data.put("twNoMailcnt", twNoMailcnt);
		data.put("twSucesscnt", twSucesscnt);
		data.put("twfailcnt", twfailcnt);
		
		c1 = Calendar.getInstance();
		log.debug("now date = " + sdf.format(d));
		c1.setTime(d);
		log.debug("Date is : " + sdf.format(c1.getTime()));
		// txdays=(String)setting.get("fxdays");
		String fxdays = "3";
		c1.add(Calendar.DATE, Integer.parseInt(fxdays));
		currdays = sdf.format(c1.getTime());
		log.info("Date + 3 days is : " + sdf.format(c1.getTime()));
		txnfxschpaydata = txnFxSchPayDataDao.findScheduleDueTo(currdays);
		log.info("txnfxschpaydata.size() = " + txnfxschpaydata.size());
		for (int i = 0; i < txnfxschpaydata.size(); i++) {
			varmail.clear();
			fx = txnfxschpaydata.get(i);
			log.debug(ESAPIUtil.vaildLog("fx.getFXUSERID() = " + fx.getPks().getFXUSERID()));
			log.debug(ESAPIUtil.vaildLog("fx.getFXSCHNO() = " + fx.getPks()));
			log.debug(ESAPIUtil.vaildLog("fx.getFXSVBH() = " + fx.getFXSVBH()));
			listtxnUser = txnUserDao.findByUserId(fx.getPks().getFXUSERID());
			log.trace("listtxnUser.size() = " + listtxnUser.size());
			if (listtxnUser == null || listtxnUser.size() == 0) {
				log.error(ESAPIUtil.vaildLog("User ID not found = " + fx.getPks().getFXUSERID()));
				fxNotFoundcnt++;
				continue;
			}
			txnuser = listtxnUser.get(0);
			log.info(ESAPIUtil.vaildLog("Fx USER:" + txnuser.getDPSUERID()));
			String dpnotify = StrUtils.trim(txnuser.getDPNOTIFY());
			Set<String> dpnotifySet = StrUtils.splitToSet("\\s*,\\s*", dpnotify);

			boolean isUserSetTxnNotify = dpnotifySet.contains("4");
			if (isUserSetTxnNotify == false) {
				fxNoNeedcnt++;
				continue;
			}
			String email = txnuser.getDPMYEMAIL();
			log.info(ESAPIUtil.vaildLog("EMAIL: " + email));
			if (email == null || email.length() == 0) {
				log.error(ESAPIUtil.vaildLog(fx.getPks().getFXUSERID() + " email not found ."));
				fxNoMailcnt++;
				continue;
			}
			// String fxpermtdate = fx.getFXPERMTDATE();
			// if(fxpermtdate.length()==1)
			// fxpermtdate = "0" + fxpermtdate;
			// log.debug("fxpermtdate = " + fxpermtdate);
			String fxfdatefmt = new String();
			// String fxfdate=fx.getFXSDATE();
			fxfdatefmt = currdays;
			/*
			 * if(fxpermtdate.length()==0 || fxpermtdate.equals("  "))
			 * fxfdatefmt=fxfdate.substring(0, 4) + "/" + fxfdate.substring(4, 6) + "/" +
			 * fxfdate.substring(6, 8); else fxfdatefmt=fxfdate.substring(0, 4) + "/" +
			 * currdays.substring(4, 6) + "/" + fxpermtdate.substring(0, 2);
			 */
			log.debug("fxfdatefmt = " + fxfdatefmt);
			String fxftimefmt = new String();
			// String fxftime=fx.getFXSTIME();
			// fxstimefmt=fxstime.substring(0, 2) + ":" + fxstime.substring(2, 4) + ":" +
			// fxstime.substring(4, 6);
			String fxwdac = new String();
			String fxwdacstart = fx.getFXWDAC().trim();
			fxwdac = "050-臺灣企銀  " + StrUtils.left(fxwdacstart, 6) + "***" + StrUtils.right(fxwdacstart, 2);
			String fxsvbhtext = new String();
			log.debug(ESAPIUtil.vaildLog("fx.getFXSVBH() = " + fx.getFXSVBH()));
			ADMBANK admbank = admBankDao.getbank(fx.getFXSVBH());
			fxsvbhtext = fx.getFXSVBH() + "-" + admbank.getADBANKNAME() + " ";
			String fxsvacstart = fx.getFXSVAC().trim();
			fxsvbhtext = fxsvbhtext + StrUtils.left(fxsvacstart, 6) + "***" + StrUtils.right(fxsvacstart, 2);
			String fxwdcurr = fx.getFXWDCURR();
			String fxwdamt = fx.getFXWDAMT();
			String fxwdamtfmt = NumericUtil.formatNumberString(fxwdamt, 0);
			log.debug(ESAPIUtil.vaildLog("fx.fxwdcurr() = " + fxwdcurr));
			String fxwdcurtext = new String();
			ADMCURRENCY cur1 = admCurrencyDao.findByAdcurrency(fxwdcurr);
			if (cur1 != null) {
				fxwdcurtext = cur1.getADCCYNAME(); // 轉出幣別
			} else
				fxwdcurtext = fxwdcurr;

			String fxsvcurtext = new String();
			String fxsvcurr = fx.getFXSVCURR();
			log.debug(ESAPIUtil.vaildLog("fx.fxsvcurr() = " + fxsvcurr));
			ADMCURRENCY cur = admCurrencyDao.findByAdcurrency(fxsvcurr);
			if (cur != null) {
				fxsvcurtext = cur.getADCCYNAME(); // 轉入幣別
			} else
				fxsvcurtext = fxsvcurr;

			String fxsvamt = fx.getFXSVAMT();
			String fxsvamtfmt = NumericUtil.formatNumberString(fxsvamt, 0);

			String fxtxmemo = fx.getFXTXMAILMEMO();
			if (fxtxmemo == null || fxtxmemo.length() == 0)
				fxtxmemo = " ";

			varmail.put("#TXNAME", "外幣轉帳");
			varmail.put("#TRANTIME", fxfdatefmt);
			varmail.put("#OUTACN", fxwdac);
			varmail.put("#ACN", fxsvbhtext);
			varmail.put("#OUTCURRENCY", fxwdcurtext);
			varmail.put("#AMT", fxwdamtfmt);
			varmail.put("#INCURRENCY", fxsvcurtext);
			varmail.put("#INAMT", fxsvamtfmt);
			varmail.put("#MEMO", fxtxmemo);
			boolean isSendSuccess = NotifyAngent.sendNotice("WITHDRAWNOTIFY", varmail, email);
			if (!isSendSuccess) {
				log.error(ESAPIUtil.vaildLog("發送 Email 失敗.(" + email + ")"));
				fxfailcnt++;
			} else {
				log.debug(ESAPIUtil.vaildLog("發送 Email 成功.(" + email + ")"));
				fxSucesscnt++;
			}
		}
		log.info("FX notify end");
		data.put("fxNoNeedcnt", fxNoNeedcnt);
		data.put("fxNotFoundcnt", fxNotFoundcnt);
		data.put("fxNoMailcnt", fxNoMailcnt);
		data.put("fxSucesscnt", fxSucesscnt);
		data.put("fxfailcnt", fxfailcnt);
		
		bs.setData(data);

		try {
			// 新增SYSBATCH紀錄
			sysBatchDao.finish_OK(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.finish_OK error >> {} ", e.getMessage());
		}
		
	}
	
	public void writeSendNoticeLog(final NoticeInfo info, final String __record_adtxno, String subject, String msg,
			String status) {
		ADMMAILLOG maillog = info.getADMMAILLOG();
		maillog.setADSENDSTATUS(status);
		maillog.setADMAILCONTENT(msg);
		maillog.setADMSUBJECT(subject);

		// ADMMAILCONTENT content = new ADMMAILCONTENT();
		// content.setADMAILCONTENT(msg);
		// content.setADSUBJECT(subject);

		Date d = new Date();
		TXNRECMAILLOGRELATION relation = new TXNRECMAILLOGRELATION();
		relation.setADTXNO(__record_adtxno);
		relation.setLASTDATE(DateTimeUtils.format("yyyyMMdd", d));
		relation.setLASTTIME(DateTimeUtils.format("HHmmss", d));

		try {
			admMailLogDao.writeRecordNotice(maillog, relation);
		} catch (Exception ex) {
			log.error("無法寫入 MAILLOG.", ex);
		}
	}

}
