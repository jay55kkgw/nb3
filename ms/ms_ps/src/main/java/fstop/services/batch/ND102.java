package fstop.services.batch;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.owasp.esapi.SafeFile;
import org.owasp.esapi.errors.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.model.Row;
import fstop.model.Rows;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.SysParamDataDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.oao.Old_TxnUserDao;
import fstop.orm.old.OLD_TXNUSER;
import fstop.orm.po.SYSPARAMDATA;
import fstop.orm.po.TXNUSER;
import fstop.services.batch.annotation.Batch;
import fstop.services.impl.N660;
import fstop.util.DateTimeUtils;
import fstop.util.FtpBase64;
import fstop.util.FtpPathProperties;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 
 * @author Owner
 * 
 */
@Slf4j
@RestController
@Batch(id = "batch.nd102", name = "網銀電子對帳單產生", description = "網銀電子對帳單產生")
public class ND102 implements BatchExecute {

	private DecimalFormat fmt = new DecimalFormat("#0");

	@Autowired
	private TxnUserDao txnUserDao;

	@Autowired
	private Old_TxnUserDao old_TxnUserDao;

	@Autowired
	private SysParamDataDao sysParamDataDao;

	@Value("${isParallelCheck}")
	private String isParallelCheck;

	@Value("${doFTP}")
	private String doFTP;

	@Autowired
	private N660 n660;

	@Autowired
	private CheckServerHealth checkserverhealth;

	@Value("${bh1.ftp.ip}")
	private String ftp_ip;
	@Value("${batch.hostfile.path}")
	private String pathname;

	@Autowired
	private SysBatchDao sysBatchDao;

	/**
	 * args[0] method name
	 */
	@RequestMapping(value = "/batch/nd102", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {
		log.info("INTO ND102 BATCH ... ");
		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}
		
		BatchResult batchresult = new BatchResult();
		batchresult.setBatchName(getClass().getSimpleName());
		Map<String, Object> data = new HashMap();
		
		
		int rc = 0;
		int failcnt = 0, okcnt = 0, totalcnt = 0;
		String filename = "FWKND10.TXT";
		Date d = new Date();
		String today = DateTimeUtils.getCDateShort(d);
		String time = DateTimeUtils.getTimeShort(d);
		String FullFileName = new String();
		InputStreamReader in = null;
		BufferedReader bin = null;
		String subject = new String();
		FullFileName = pathname;
		FullFileName += filename;
		log.debug(ESAPIUtil.vaildLog("FullFileName = " + FullFileName));

		Map<String, String> r = new HashMap();
		Rows rows = new Rows();
		int counter = 0;
		try {
			in = new InputStreamReader(new FileInputStream(FullFileName), "BIG5");
			bin = new BufferedReader(in);
			while (true) {
				subject = ESAPIUtil.validInput(bin.readLine(), "GeneralString", true);
				if (subject == null || subject.length() == 0) {
					break;
				}
				counter++;
			}
		} catch (Exception e) {
			log.error("ND102.java COUNTER FAIL:" + e.getMessage());
		} finally {
			try {
				in.close();
				bin.close();
			} catch (Exception e) {
			}
			in = null;
			bin = null;
		}
		String[][] result = new String[counter][5];
		try {
			in = new InputStreamReader(new FileInputStream(FullFileName), "BIG5");
			bin = new BufferedReader(in);
			int counter1 = 0;
			while (true) {
				subject = ESAPIUtil.validInput(bin.readLine(), "GeneralString", true);
				if (subject == null || subject.length() == 0) {
					break;
				}
				StringTokenizer st = new StringTokenizer(subject, ";");
				while (st.hasMoreTokens()) {
					try {
						String CUSIDN = st.nextToken().trim();
						result[counter1][0] = CUSIDN;
					} catch (Exception e) {
						result[counter1][0] = "";
					}
					try {
						String DATA = st.nextToken();
						result[counter1][1] = DATA;
					} catch (Exception e) {
						result[counter1][1] = "";
					}
					try {
						String TYPE = st.nextToken();
						result[counter1][2] = TYPE;
					} catch (Exception e) {
						result[counter1][2] = "";
					}
					try {
						String ADDR = st.nextToken().trim();
						result[counter1][3] = ADDR;
					} catch (Exception e) {
						result[counter1][3] = "";
					}
					try {
						String NAME = st.nextToken().trim();
						result[counter1][4] = (NAME == null ? "" : NAME);
					} catch (Exception e) {
						result[counter1][4] = "";
					}
				}
				counter1++;
				okcnt++;
				totalcnt++;
			}
			log.info(ESAPIUtil.vaildLog("ND102.java okcnt:" + okcnt));
		} catch (FileNotFoundException e) {
//			throw new MsgTemplateNotFoundException("找不到對應的 template 檔(" + FullFileName + ").", e);
			log.error("找不到對應的 template 檔(" + FullFileName + ").", e);
			data.put("Error", "找不到對應的 template 檔(" + FullFileName + ")");
			batchresult.setData(data);
			batchresult.setSuccess(false);
			
			try {
				sysBatchDao.finish_FAIL2(batchName, batchresult.getData());
			} catch (Exception e2) {
				log.error("sysBatchDao.finish error >> {} ", e2.getMessage());
			}
			
			return batchresult;
		} catch (IOException e) {
			log.error("讀取 template 檔錯誤(" + FullFileName + ").", e);
			data.put("Error", "讀取 template 檔錯誤(" + FullFileName + ")");
			batchresult.setData(data);
			batchresult.setSuccess(false);
			
			try {
				sysBatchDao.finish_FAIL2(batchName, batchresult.getData());
			} catch (Exception e2) {
				log.error("sysBatchDao.finish error >> {} ", e2.getMessage());
			}
			
			return batchresult;
//			throw new ToRuntimeException("讀取 template 檔錯誤(" + FullFileName + ").", e);
		} finally {
			try {
				in.close();
				bin.close();
			} catch (Exception e) {
			}
			in = null;
			bin = null;
		}

		FtpPathProperties nrcpro = new FtpPathProperties();
		String srcpath = nrcpro.getSrcPath("nd10xml").trim();
		log.debug(ESAPIUtil.vaildLog("srcpath = " + srcpath));
		String despath = nrcpro.getDesPath("nd10xml1").trim();
		log.debug(ESAPIUtil.vaildLog("despath = " + despath));

		// LTest
//		 srcpath = "C:\\Users\\benchien\\Desktop\\BatchTest\\other\\";

		// Absolute Path Traversal
		String LFILENAME = srcpath + today + "_nd10.xml";
		//

		File tmpFile = new File(LFILENAME);
		tmpFile.setWritable(true, true);
		tmpFile.setReadable(true, true);
		tmpFile.setExecutable(true, true);
		Hashtable rowcnt = new Hashtable();
		genernateFile(tmpFile, result, rowcnt);

		failcnt = ((Integer) rowcnt.get("FAILCNT")).intValue();
		okcnt = ((Integer) rowcnt.get("OKCNT")).intValue();
		totalcnt = ((Integer) rowcnt.get("TOTALCNT")).intValue();
		log.debug("failcnt = " + failcnt);
		log.debug("okcnt = " + okcnt);
		log.debug("totalcnt = " + totalcnt);
		BatchCounter bcnt = new BatchCounter();
		bcnt.setFailCount(failcnt);
		bcnt.setSuccessCount(okcnt);
		bcnt.setTotalCount(totalcnt);
		// "trademsg.ok"

		// FTP File To Server
		if (okcnt > 0) {
			try {
				int ftpresult = ftpToServer(tmpFile, "");
				if (ftpresult != 0) {
					data.put("Error", "Function ftpToServer Fail");
					data.put("COUNTER", bcnt);
					batchresult.setSuccess(false);
					batchresult.setData(data);
					try {
						sysBatchDao.finish_FAIL2(batchName, batchresult.getData());
					} catch (Exception e2) {
						log.error("sysBatchDao.finish error >> {} ", e2.getMessage());
					}
					
					return batchresult;
				} else {
					data.put("FTP_Status", "Sucecss");
				}
			} catch (FileNotFoundException e) {
				log.error("找不到對應的檔(" + tmpFile + ").", e);
				data.put("Error", "Function ftpToServer 找不到對應的 tmpFile 檔(" + tmpFile + ")");
				batchresult.setSuccess(false);
				
				try {
					sysBatchDao.finish_FAIL2(batchName, batchresult.getData());
				} catch (Exception e2) {
					log.error("sysBatchDao.finish error >> {} ", e2.getMessage());
				}
				
				return batchresult;
			}
		}

		// tmpFile.delete();
		data.put("COUNTER", bcnt);
		batchresult.setSuccess(true);
		batchresult.setData(data);
		
		try {
			// 新增SYSBATCH紀錄
			sysBatchDao.finish_OK(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.finish_OK error >> {} ", e.getMessage());
		}
		return batchresult;

	}

	private void genernateFile(File f, String[][] result, Hashtable rowcnt) {

		Row rr = null;
		int emailaddexist = 0;

		Integer totalcnt = 0, failcnt = 0, okcnt = 0;
		Pattern regex_time = Pattern.compile("(\\d\\d/\\d\\d\\s+\\d\\d:\\d\\d)",
				Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
		Pattern regex = Pattern.compile("(\\d\\d/\\d\\d\\s\\s\\d\\d:\\d\\d)([^\\(]+)\\((.+)\\)",
				Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
		Pattern regex_nq = Pattern.compile("(\\d\\d/\\d\\d\\s\\s\\d\\d:\\d\\d)([^\\(]+)",
				Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);

		final Element rootElement = new Element("NoXML");
		final Document document = new Document(rootElement);

		log.debug("ND102.java rows count:" + result.length);
		if (result.length == 0) {
			log.info("無回傳結果, 不產生檔案.");
		}

		int i = 0;
		int rowNum = 1;
		StringBuffer sb = new StringBuffer();
		sb.setLength(0);
		String Oldcusidn = "XXXXXXXXXX";
		Element noInfo = null;
		String oldtext = "";
		while (true && result.length > 0) {
			if (result.length == i) {
				noInfo = new Element("NoInfo");
				oldtext += "</table></td></tr>";
				Element nomessage = null;
				try {
					nomessage = new Element("NoMessage").setText(oldtext);
				} catch (Exception e) {
					nomessage = new Element("NoMessage").setText("");
				}
				noInfo.addContent(nomessage);
				if (emailaddexist >= 1)
					rootElement.addContent(noInfo);
				else
					log.debug("The email is not exist.");
					rootElement.addContent(noInfo);
				break;
			}

			Map<String, String> rm = new HashMap();

			String data = StrUtils.trim(result[i][1]);
			String type = StrUtils.trim(result[i][2]);
			if (regex_time.matcher(data).find()) {
				log.debug("regex_time.matcher boolean:true");
				Matcher matcher = regex.matcher(data);
				Matcher matcher_nq = regex_nq.matcher(data);
				if (matcher.find()) {
					log.debug("matcher boolean:true");
					String group0 = matcher.group(0);
					log.debug("group group0 = " + group0);
					String time = matcher.group(1);
					log.debug("group time = " + time);
					String subtype = matcher.group(3);
					log.debug("group subtype = " + subtype);
					String subtype_desc = matcher.group(2);
					log.debug("group subtype_desc = " + subtype_desc);
					String name = "";
					TXNUSER user = null;
					List<TXNUSER> listtxnUser;
					try {
						if (result[i][4].length() == 0) {
							log.debug("ND102.java genernateFile CUSIDN = " + result[i][0]);
							listtxnUser = txnUserDao.findByUserId(result[i][0]);
							// 如果找不到user 代表從舊網銀來
							if ("Y".equals(isParallelCheck)) {
								if (listtxnUser.size() == 0) {
									log.info("Start Find NNBUSER");
									List<OLD_TXNUSER> oldlisttxnUser = old_TxnUserDao.findByUserId(result[i][0]);
									for (OLD_TXNUSER usernnb : oldlisttxnUser) {
										log.trace(ESAPIUtil.vaildLog("Get NB2.5 User >> " + usernnb.getDPSUERID()));
										TXNUSER nnbConvert2nb3 = CodeUtil.objectCovert(TXNUSER.class, usernnb);
										listtxnUser.add(nnbConvert2nb3);
									}
								}
							}

							user = listtxnUser.get(0);
							name = user.getDPUSERNAME();
						} else {
							name = result[i][4];
						}
					} catch (Exception e) {
						log.debug("ND102.java genernateFile 無法取得 USER ID (" + result[i][0] + ")的NAME," + e.getMessage());
						name = "";
					}
					// 20120105 By Sox 瀘掉ＮＵＬＬ －－＞ BYTE值＝0 [NULL] 換成 BYTE值＝20 [空白]
					try {
						byte[] namefilter = name.getBytes("Big5");
						String checknull = "N";
						for (int j = 0; j < namefilter.length; j++) {
							if (namefilter[j] == 0) {
								namefilter[j] = (byte) Integer.parseInt("20", 16);
								checknull = "Y";
							}
						}
						if (checknull.equals("Y"))
							name = new String(namefilter, "Big5");
					} catch (Exception e) {
						name = "";
					}
					name = name.replaceAll("　", "");
					log.debug("ND102.java name ===" + name + "===");
					rm.put("CUSIDN", result[i][0]);
					rm.put("ADDR", result[i][3]);
					rm.put("NAME", name);
					rm.put("TIME", time);
					rm.put("TYPE", type);
					rm.put("SUBTYPE", subtype);
					rm.put("SUBTYPE_DESC", subtype_desc);
					String realData = StrUtils.trim(data.substring(group0.length()));
					sb.append(realData).append("\r\n");
				} else if (matcher_nq.find()) {
					log.debug("matcher_nq boolean:true");
					log.debug("groupCount = " + matcher_nq.groupCount());
					String group0 = matcher_nq.group(0);
					log.debug("group group0 = " + group0);
					String time = matcher_nq.group(1);
					log.debug("group time = " + time);
					String realData = StrUtils.trim(data.substring(time.length()));
					log.debug("group realData = " + realData);
					String[] totadesc = realData.split(" ", 2);
					log.debug("totadesc.length = " + totadesc.length);
					for (int j = 0; j < totadesc.length; j++) {
						log.debug("totadesc[" + j + "] = " + totadesc[j]);
					}
					Hashtable totadesctable = new Hashtable();
					n660.convmemo(totadesc[0], totadesctable);
					rm.put("CUSIDN", result[i][0]);
					rm.put("ADDR", result[i][3]);
					String name = "";
					TXNUSER user = null;
					List<TXNUSER> listtxnUser;
					try {
						if (result[i][4].length() == 0) {
							log.debug("ND102.java genernateFile CUSIDN = " + result[i][0]);
							listtxnUser = txnUserDao.findByUserId(result[i][0]);
							// 如果找不到user 代表從舊網銀來
							if ("Y".equals(isParallelCheck)) {
								if (listtxnUser.size() == 0) {
									log.info("Start Find NNBUSER");
									List<OLD_TXNUSER> oldlisttxnUser = old_TxnUserDao.findByUserId(result[i][0]);
									for (OLD_TXNUSER usernnb : oldlisttxnUser) {
										log.trace(ESAPIUtil.vaildLog("Get NB2.5 User >> " + usernnb.getDPSUERID()));
										TXNUSER nnbConvert2nb3 = CodeUtil.objectCovert(TXNUSER.class, usernnb);
										listtxnUser.add(nnbConvert2nb3);
									}
								}
							}
							user = listtxnUser.get(0);
							name = user.getDPUSERNAME();
						} else {
							name = result[i][4];
						}
					} catch (Exception e) {
						log.debug("無法取得 USER ID (" + result[i][0] + ")的NAME," + e.getMessage());
						name = "";
					}
					// 20120105 By Sox 瀘掉ＮＵＬＬ －－＞ BYTE值＝0 [NULL] 換成 BYTE值＝20 [空白]
					try {
						byte[] namefilter = name.getBytes("Big5");
						String checknull = "N";
						for (int j = 0; j < namefilter.length; j++) {
							if (namefilter[j] == 0) {
								namefilter[j] = (byte) Integer.parseInt("20", 16);
								checknull = "Y";
							}
						}
						if (checknull.equals("Y"))
							name = new String(namefilter, "Big5");
					} catch (Exception e) {
						name = "";
					}
					log.debug(ESAPIUtil.vaildLog("name ===" + name + "==="));
					rm.put("NAME", name);
					rm.put("TIME", time);
					rm.put("TYPE", type);
					rm.put("SUBTYPE", (String) totadesctable.get("MEMO"));
					rm.put("SUBTYPE_DESC", (String) totadesctable.get("MEMODESC"));
					rm.put("DATA", totadesc[1]);
					sb.append(totadesc[1]).append("\r\n");
				} else {
					log.debug("matcher and matcher_nq boolean:false");
					rm.put("TIME", "");
					rm.put("SUBTYPE_DESC", "");
					rm.put("SUBTYPE", "");
				}

				do {
					i++;
					if (i >= result.length) {
						i--;
						break;
					}
					// Row _data_r = rows.getRow(i);
					String _data_data = StrUtils.trim(result[i][1]);
					log.debug("_data_data:" + _data_data);
					if (regex_time.matcher(_data_data).find()) {
						log.debug("regex_time matcher _data_data boolean:true=>i--");
						i--;
						break;
					} else {
						sb.append(_data_data).append("\r\n");
					}
				} while (true);

				rm.put("DATA", sb.toString());

			} else {
				log.debug("regex_time.matcher boolean:false");
				rm.put("DATA", data);
			}

			if (!Oldcusidn.equals(result[i][0])) {
				if (!Oldcusidn.equals("XXXXXXXXXX")) {
					Element nomessage = null;
					oldtext += "</table></td></tr>";
					log.debug("oldtext Oldcusidn <> CUSIDN = " + Oldcusidn + " oldtext = " + oldtext);
					try {
						nomessage = new Element("NoMessage").setText(oldtext);
					} catch (Exception e) {
						nomessage = new Element("NoMessage").setText("");
					}
					noInfo.addContent(nomessage);
					if (emailaddexist >= 1)
						rootElement.addContent(noInfo);
					else
						rootElement.addContent(noInfo);
						log.debug("The email address is not exist.");
					oldtext = "";
				}

				String cusidn = result[i][0];
				rm.put("TITLE", " ");
				Oldcusidn = cusidn;
				rr = new Row(rm);
				noInfo = new Element("NoInfo");
				int rc = createNodeInfoElement(rootElement, rr, noInfo);
				if (rc == 0) {
					failcnt++;
				} else {
					okcnt++;
				}
				emailaddexist = rc;
				totalcnt++;
				rm.put("TITLE", "");
				sb.setLength(0);
			}

			rr = new Row(rm);
			oldtext += genernateMessageElement(rr);

			sb.setLength(0);
			rowNum++;
			i++;
			// if(i >= rows.getSize())
			if (i >= result.length) {
				oldtext += "</table></td></tr>";
				Element nomessage = null;
				try {
					nomessage = new Element("NoMessage").setText(oldtext);
				} catch (Exception e) {
					nomessage = new Element("NoMessage").setText("");
				}
				noInfo.addContent(nomessage);
				if (emailaddexist >= 1)
					rootElement.addContent(noInfo);
				else
					rootElement.addContent(noInfo);
					log.debug("The email address is not exist.");
				break;
			}
		}

		rowcnt.put("OKCNT", okcnt);
		rowcnt.put("FAILCNT", failcnt);
		rowcnt.put("TOTALCNT", totalcnt);
		f.setWritable(true, true);
		f.setReadable(true, true);
		f.setExecutable(true, true);
		OutputXML(document, f);
	}

	private int createNodeInfoElement(final Element rootElement, Row r, Element noInfo) {

		// Element noInfo = new Element("NoInfo");
		// int dpbillexist=0;
		Element noflag = new Element("Noflag").setText("1");
		noInfo.addContent(noflag);

		String cusidn = r.getValue("CUSIDN");
		log.debug("createNodeInfoElement cusidn = " + cusidn);
		Element noid = new Element("NoID").setText(cusidn);
		noInfo.addContent(noid);

		Element noname = new Element("NoNAME").setText(r.getValue("NAME") == null ? "" : r.getValue("NAME"));
		noInfo.addContent(noname);

		String gender = "";
		try {
			if (cusidn.length() == 10) {
				gender = cusidn.substring(1, 2);
				if ("1".equals(gender) || "A".equals(gender) || "C".equals(gender))
					gender = "M";
				else if ("2".equals(gender) || "B".equals(gender) || "D".equals(gender))
					gender = "F";
				else
					gender = "M";
			} else {
				gender = "C";
			}
		} catch (Exception e) {
		}
		log.debug("createNodeInfoElement NoSEX = " + gender);
		Element nosex = new Element("NoSEX").setText(gender);
		noInfo.addContent(nosex);

		String email = r.getValue("ADDR");
		if (email == null)
			email = "";
		if ("02".equals(r.getValue("TYPE"))) {
			TXNUSER user = null;
			List<TXNUSER> listtxnUser;
			try {
				log.debug("取得 USER 物件, #" + cusidn);
				listtxnUser = txnUserDao.findByUserId(cusidn);
				// 如果找不到user 代表從舊網銀來
				if ("Y".equals(isParallelCheck)) {
					if (listtxnUser.size() == 0) {
						log.info("Start Find NNBUSER");
						List<OLD_TXNUSER> oldlisttxnUser = old_TxnUserDao.findByUserId(cusidn);
						for (OLD_TXNUSER usernnb : oldlisttxnUser) {
							log.trace(ESAPIUtil.vaildLog("Get NB2.5 User >> " + usernnb.getDPSUERID()));
							TXNUSER nnbConvert2nb3 = CodeUtil.objectCovert(TXNUSER.class, usernnb);
							listtxnUser.add(nnbConvert2nb3);
						}
					}
				}
				user = listtxnUser.get(0);
				email = user.getDPMYEMAIL();
				/*
				 * 20130719 By Sox 不用判斷NNB有沒有申請電子對帳單，因要寄電話銀行的對帳單
				 * if(user.getDPBILL().equals("Y")) dpbillexist=1;
				 */
			} catch (Exception e) {
				log.debug("無法取得 USER 的 Email (" + cusidn + "), " + e.getMessage());
				email = "";
			}
		}

		log.debug(ESAPIUtil.vaildLog("createNodeInfoElement NoEMAIL = " + email));
		Element noemail = new Element("NoEMAIL").setText(email.trim());
		log.debug(ESAPIUtil.vaildLog("noemail.getText() = " + noemail.getText()));
		noInfo.addContent(noemail);
		// if(email.equals("") || dpbillexist==0) //20130719 By Sox
		// 不用判斷NNB有沒有申請電子對帳單，因要寄電話銀行的對帳單
		if (email.equals(""))
			return (0);
		else
			return (1);
	}

	private String genernateMessageElement(Row r) {
		String n = "\r\n";
		String text = new String();
		if (r.getValue("TITLE") != null) {
			text = r.getValue("TITLE");
			text += " <style type=\"text/css\">" + n + "<!--" + n + ".p12 {" + n + "font-size: 12px;" + n
					+ "line-height: 20px;" + n + "color: #000000;" + n + "}" + n + ".bluep12 {" + n + "font-size: 12px;"
					+ n + "line-height: 20px;" + n + "color: #129DE9;" + n + "}" + n + "-->" + n + "</style>" + n
					+ "<tr>" + n
					+ "<td width=\"100%\" bgcolor=\"#FFFFFF\"><div align=\"left\"><strong><span class=\"bluep12\">電子交易對帳單明細如下：</span></strong></div></td>"
					+ n + "</tr>" + n + "<tr>" + n + "<td bgcolor=\"#BBE3FF\" width=\"100%\">" + n
					+ "  <table width=\"100%\" border=\"0\" align=\"center\" cellpadding=\"3\" cellspacing=\"2\" class=\"p12\">"
					+ n + "<tr  >" + n
					+ "<td width=\"13%\" bordercolor=\"#C4E1F0\" bgcolor=\"#FFFFFF\"><div align=\"center\"><strong>交易時間</strong></div></td> "
					+ n
					+ "<td width=\"15%\" bordercolor=\"#C4E1F0\" bgcolor=\"#FFFFFF\"><div align=\"center\"><strong>項目</strong></div></td> "
					+ n
					+ "<td width=\"58%\" bordercolor=\"#C4E1F0\" bgcolor=\"#FFFFFF\"><div align=\"center\"><strong>通知內容</strong></div></td> "
					+ n
					+ "<td width=\"14%\" bordercolor=\"#C4E1F0\" bgcolor=\"#FFFFFF\"><div align=\"center\"><strong>備註</strong></div></td> "
					+ n + "</tr>" + n;
			r.setValue("TITLE", "");
		}
		StringReader sw = new StringReader(r.getValue("DATA"));
		BufferedReader bw = new BufferedReader(sw);
		String line = null;
		try {
			String ltime = null;
			String subtype_dasc = null;
			String lline = null;
			String subtype = null;
			int gencounter = 0;
			while ((line = bw.readLine()) != null) {
				gencounter++;
				log.debug("r.getValue(TIME) = " + r.getValue("TIME"));
				log.debug("COUNTER:" + gencounter + ";line = " + line);
				if (r.getValue("TIME") != null && r.getValue("TIME").length() > 0) {
					ltime = datebrtime(r.getValue("TIME"));
					subtype_dasc = r.getValue("SUBTYPE_DESC");
					lline = line;
					subtype = r.getValue("SUBTYPE");
					// dataexist=1;
					r.setValue("TIME", "");
					r.setValue("SUBTYPE_DESC", "");
					r.setValue("SUBTYPE", "");
				} else {
					lline += "<br>" + line;
				}
			}

			text += "<tr>" + n + "<td bordercolor=\"#C4E1F0\" bgcolor=\"#FFFFFF\"><div align=\"center\"><p>" + ltime
					+ "</p></div></td>" + n + "<td bordercolor=\"#C4E1F0\" bgcolor=\"#FFFFFF\"><div align=\"center\">"
					+ subtype_dasc + "</div></td>" + n + "<td bordercolor=\"#C4E1F0\" bgcolor=\"#FFFFFF\"><p>" + lline
					+ "</p></td>" + n + "<td bordercolor=\"#C4E1F0\" bgcolor=\"#FFFFFF\"><div align=\"center\">"
					+ subtype + "</div></td>" + n + "</tr>";

		} catch (IOException e) {
			log.error("genernateMessageElement error = " + e.getMessage());
		}
		/**
		 * "
		 * <td align='right'>1</td> " + n + "
		 * <td >08/08 18:07轉入定存(VO) 轉出帳號: 001122XXX10存入金額: $10,000</td>" + n + "
		 * </tr>
		 * " + n + "
		 * <tr>
		 * " + n + "
		 * <td align='right'>2</td> " + n + "
		 * <td >定期存款帳號: 001120XXX51 存單號碼: 0000010 機動利率 2.685%</td> " + n + "
		 * </tr>
		 * " + n +
		 * 
		 */
		return (text);
	}

	public void OutputXML(Document docXML, File outputFile) {
		outputFile.setWritable(true, true);
		outputFile.setReadable(true, true);
		outputFile.setExecutable(true, true);
		Format fmt = Format.getPrettyFormat();
		fmt = fmt.setEncoding("big5");
		XMLOutputter xmlout = new XMLOutputter(fmt);
		OutputStreamWriter fwXML = null;
		try {
			fwXML = new OutputStreamWriter(new FileOutputStream(outputFile), "BIG5");
			xmlout.output(docXML, fwXML);
			fwXML.close();
		} catch (IOException e) {
			log.debug("寫入XML有誤.", e);
		} finally {
			try {
				fwXML.close();
			} catch (Exception e) {
			}
		}

	}

	public synchronized int ftpToServer(File f, String uuid) throws FileNotFoundException {
		f.setWritable(true, true);
		f.setReadable(true, true);
		f.setExecutable(true, true);
		String okname = "";
		String xmlname = "";
		String filename = "";
		if (uuid.equals("")) {
			okname = "trademsg.ok";
			xmlname = "trademsg.xml";
			filename = "nd10";
		} else {
			okname = uuid + ".ok";
			xmlname = uuid + ".xml";
			filename = "n660";
		}
		log.debug("okname = " + okname);
		FtpPathProperties nrcpro = new FtpPathProperties();
		String srcpathok = nrcpro.getSrcPath(filename + "ok").trim();
		// //LTest
//		 srcpathok = "C:\\Users\\benchien\\Desktop\\BatchTest\\other\\";
		log.debug(ESAPIUtil.vaildLog("srcpathok = " + srcpathok + okname));
		try {
			// Absolute Path Traversal
			String LFILENAME = srcpathok + okname;
			//

			File tmpOkFile = new SafeFile(LFILENAME);
			tmpOkFile.setWritable(true, true);
			tmpOkFile.setReadable(true, true);
			tmpOkFile.setExecutable(true, true);
			// Path p = tmpOkFile.toPath();
			// FileWriter fw = null;

			// Files.write(p, fmt.format(f.length()).toString().getBytes("BIG5"));
			// fw = new FileWriter(tmpOkFile);
			// fw.write(fmt.format(f.length()));
			BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(tmpOkFile));
			byte[] strToBytes = fmt.format(f.length()).getBytes("BIG5");
			outputStream.write(strToBytes);
			outputStream.close();

			log.debug(ESAPIUtil.vaildLog("FullPath = " + f.getName()));
			log.debug("tmpOkFile = " + okname);

			String ip = ftp_ip;
			String despathxml = nrcpro.getDesPath(filename + "xml1").trim();
			log.debug(ESAPIUtil.vaildLog("despathxml = " + despathxml + xmlname));

			if ("Y".equals(doFTP)) {
				FtpBase64 ftpBase64 = new FtpBase64(ip);
				int rc = ftpBase64.upload(despathxml + xmlname, new FileInputStream(f));
				// 目地 來源
				if (rc != 0) {
					log.debug(filename + " FTP 失敗(BillHunter 主機)");
					// CheckServerHealth checkserverhealth =
					// (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
					SYSPARAMDATA po = null;
					try {
						po = sysParamDataDao.findById("NBSYS");
						String emailap = po.getADAPMAIL();

						checkserverhealth.sendUserNotice(filename + " FTP 失敗(BillHunter 主機)", emailap.trim(), "batch");
					} catch (Exception e1) {
						log.error("Query NBSYS table error" + e1);
						return (1);
					}

					return (1);
				}

				String despathok = nrcpro.getDesPath(filename + "ok1").trim();
				log.debug(ESAPIUtil.vaildLog("despathok = " + despathok + okname));

				rc = ftpBase64.upload(despathok + okname, new FileInputStream(tmpOkFile));
				// 目地 來源
				if (rc != 0) {
					log.debug("ND10 FTP 失敗(BillHunter 主機)");
					// CheckServerHealth checkserverhealth =
					// (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
					SYSPARAMDATA po = null;
					try {
						po = sysParamDataDao.findById("NBSYS");
						String emailap = po.getADAPMAIL();

						checkserverhealth.sendUserNotice(filename + " FTP 失敗(BillHunter 主機)", emailap.trim(), "batch");
					} catch (Exception e1) {
						log.error("Query NBSYS table error" + e1);
						return (1);
					}
				}
			}
		} catch (IOException e) {
			log.error("寫入 trademsg.ok 錯誤.", e);
			return (1);
		} catch (ValidationException e) {
			log.error("NewSafeFile 錯誤.", e);
			return (1);
		}
		return (0);

	}

	public String datebrtime(String datesptime) {
		String date = new String();
		String SYear = new String();
		String datefmt = null;
		Pattern regex_time = Pattern.compile("(\\d\\d/\\d\\d)(\\s+)(\\d\\d:\\d\\d)",
				Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
		Matcher matcher = regex_time.matcher(datesptime);

		if (matcher.find()) {
			String group0 = matcher.group(0);
			log.debug("group0 = " + group0);
			String grpdate = matcher.group(1);
			log.debug("grpdate = " + grpdate);
			String sp = matcher.group(2);
			log.debug("sp = " + sp);
			String grptime = matcher.group(3);
			log.debug("grptime = " + grptime);
			String mm = grpdate.substring(0, 2);
			log.debug("mm = " + mm);

			String dd = grpdate.substring(3, 5);
			log.debug("dd = " + dd);

			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String t = sdf.format(new Date());
			log.debug("t = " + t);
			int IYear = new Integer(t.substring(0, 4));
			log.debug("IYear = " + IYear);
			int IMon = new Integer(t.substring(4, 6));
			log.debug("IMon = " + IMon);
			if (t.substring(4, 6).equals("01")) {
				if (IMon < new Integer(mm)) {
					IYear--;
				}

			}
			String t1 = "000" + (IYear - 1911);
			SYear = t1.substring(t1.length() - 3);
			log.debug("SYear + mm + dd= " + SYear + "/" + mm + "/" + dd + "<br>" + grptime);
			datefmt = SYear + "/" + mm + "/" + dd + "<br>" + grptime;
			log.debug("datefmt = " + datefmt);

		}

		return datefmt;
	}
}
