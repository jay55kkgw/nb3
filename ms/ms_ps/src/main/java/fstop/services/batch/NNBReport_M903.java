package fstop.services.batch;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Date;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.orm.dao.AdmEmpInfoDao;
import fstop.orm.dao.Nb3SysOpDao;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.TxnLogDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.oao.Old_AdmEmpinfoDao;
import fstop.orm.oao.Old_TxnLogDao;
import fstop.orm.oao.Old_TxnUserDao;
import fstop.orm.old.OLD_ADMEMPINFO;
import fstop.orm.old.OLD_TXNLOG;
import fstop.orm.old.OLD_TXNUSER;
import fstop.orm.po.ADMEMPINFO;
import fstop.orm.po.NB3SYSOP;
import fstop.orm.po.TXNLOG;
import fstop.orm.po.TXNUSER;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import fstop.util.FtpBase64;
import fstop.util.FtpPathProperties;
import fstop.util.JSONUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 每月產生上月"非行員使用本行電腦（不含網銀專區）辦理網銀之轉帳類交易明細表"至 /TBB/nb3/data/NBCCM903 並把檔案拋至報表系統
 * 
 * @author Owner 每頁45筆
 */
@Slf4j
@RestController
@Batch(id = "batch.nnbReport_M903", name = "非行員使用本行電腦（不含網銀專區）辦理網銀之轉帳類交易明細表", description = "每月產生上月_非行員使用本行電腦（不含網銀專區）辦理網銀之轉帳類交易明細表")
public class NNBReport_M903 implements BatchExecute {
	// private log log = log.getlog(getClass());

	@Autowired
	private Nb3SysOpDao nb3SysOpDao;
	@Autowired
	private TxnLogDao txnLogDao;
	@Autowired
	private TxnUserDao txnUserDao;
	@Autowired
	private AdmEmpInfoDao admEmpInfoDao;

	@Autowired
	private Old_TxnLogDao old_TxnLogDao;
	@Autowired
	private Old_TxnUserDao old_TxnUserDao;
	@Autowired
	private Old_AdmEmpinfoDao old_AdmEmpinfoDao;

	@Value("${isParallelCheck}")
	private String isParallelCheck;

	@Value("${doFTP}")
	private String doFTP;

	@Autowired
	CheckServerHealth checkserverhealth;

	@Value("${prodf_path}")
	private String profpath;

	@Value("${reportn.ftp.ip}")
	private String reportn_ftp_ip;

	@Value("${batch.hostfile.path:}")
	private String host_dir;

	@Value("${batch.localfile.path:}")
	private String path;

	@Autowired
	private SysBatchDao sysBatchDao;

	@RequestMapping(value = "/batch/nnbReport_M903", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {

		log.info("batch NNBReport_M903...");

		List<String> safeArgs = ESAPIUtil.validStrList(args);
		log.debug("網銀交易明細資料CCM903  ... ");

		BatchResult batchresult = new BatchResult();
		Map<String, Object> data = new LinkedHashMap();
		batchresult.setBatchName(getClass().getSimpleName());
		batchresult.setSuccess(true);

		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}

		int failcnt = 0, okcnt = 0, totalcnt = 0;
		Hashtable rowcnt = new Hashtable();
		Hashtable<String, String> params = new Hashtable();
		params.put("TXNIDT", DateTimeUtils.format("yyMMddHHmmss", new Date()));
		// start Update 20180905
		log.debug(ESAPIUtil.vaildLog("safeArgs size:" + safeArgs.size()));
		String yyy = "";
		String mm = "";
		String lastmonth = "";
		if (safeArgs.size() == 1) {// 當批次有帶參數(帶參數是為了要產出特定月份的報表檔)
			yyy = safeArgs.get(0).substring(0, 3);// 民國年
			mm = safeArgs.get(0).substring(3);// 月份
		} else { // 批次未帶參數(未帶參數是要產出上個月的報表檔)
			Date d = new Date();
			java.util.Calendar calendar = java.util.Calendar.getInstance();
			calendar.setTime(d);
			calendar.add(calendar.MONTH, -1);
			d = calendar.getTime();
			lastmonth = DateTimeUtils.getDateShort(d);
			log.debug(ESAPIUtil.vaildLog("lastmonth not substring : " + lastmonth));
			yyy = String.valueOf(Integer.parseInt(lastmonth.substring(0, 4)) - 1911);
			mm = lastmonth.substring(4, 6);
		}
		log.debug(ESAPIUtil.vaildLog("yyy:" + yyy + " mm:" + mm));
		// end
		try {
			// start Update 20180905
			// exec(rowcnt);
			exec(rowcnt, yyy, mm);
			// end
			failcnt = ((Integer) rowcnt.get("FAILCNT")).intValue();
			okcnt = ((Integer) rowcnt.get("OKCNT")).intValue();
			totalcnt = ((Integer) rowcnt.get("TOTALCNT")).intValue();
			log.debug(ESAPIUtil.vaildLog("failcnt = " + failcnt));
			log.debug(ESAPIUtil.vaildLog("okcnt = " + okcnt));
			log.debug(ESAPIUtil.vaildLog("totalcnt = " + totalcnt));
		} catch (Exception e) {

			log.error("執行 Batch 有誤 !", e);
			data.put("Error", e.getMessage());
			batchresult.setSuccess(false);
		}

		BatchCounter bcnt = new BatchCounter();
		bcnt.setFailCount(failcnt);
		bcnt.setSuccessCount(okcnt);
		bcnt.setTotalCount(totalcnt);

		data.put("COUNTER", bcnt);
		batchresult.setData(data);

		log.debug("NNBReport_M903 - End execute ");

		try {
			if (batchresult.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName, batchresult.getData());
			}
		} catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}
		return batchresult;
	}

	private void exec(Hashtable rowcnt, String threeyear, String twomonth) throws IOException {
		Integer totalcnt = 0, failcnt = 0, okcnt = 0;
		Date d = new Date();
		java.util.Calendar calendar = java.util.Calendar.getInstance();
		calendar.setTime(d);
		String nowdate = DateTimeUtils.getDateShort(d);
		String printdate = String.valueOf(Integer.parseInt(nowdate.substring(0, 4)) - 1911) + "/"
				+ nowdate.substring(4, 6) + "/" + nowdate.substring(6, 8);
		calendar.add(calendar.MONTH, -1);
		d = calendar.getTime();
		// start Update 20180904
		// String lastmonth= DateTimeUtils.getDateShort(d);
		// lastmonth = lastmonth.substring(0, 6)+"%";
		// String year=lastmonth.substring(0,4);
		// String mon=lastmonth.substring(4,6);
		String lastmonth = String.valueOf(Integer.parseInt(threeyear) + 1911) + twomonth;
		lastmonth = lastmonth + "%";
		String year = String.valueOf(Integer.parseInt(threeyear) + 1911);
		String mon = twomonth;
		log.debug(ESAPIUtil.vaildLog("year:" + year + " mon:" + mon));
		// end 20180904
		String sEndDay = "";
		if (mon.equals("01") || mon.equals("03") || mon.equals("05") || mon.equals("07") || mon.equals("08")
				|| mon.equals("10") || mon.equals("12")) {
			sEndDay = "31";
		} else if (mon.equals("04") || mon.equals("06") || mon.equals("09") || mon.equals("11")) {
			sEndDay = "30";
		} else {
			if (mon.equals("02") && (Integer.parseInt(year) % 4 == 0) && (Integer.parseInt(year) % 100 != 0)) {
				sEndDay = "29";
			} else if (mon.equals("02")) {
				sEndDay = "28";
			}
		}
		// start Update 20180904
		// String Cyear=String.valueOf(Integer.parseInt(year)-1911);
		String Cyear = threeyear;
		log.debug(ESAPIUtil.vaildLog("Cyear = " + Cyear));
		// end
		log.debug(ESAPIUtil.vaildLog("lastmonth = " + lastmonth + ",mon=" + mon + ",year=" + year));

		FileReader fileStream = new FileReader(profpath + "ALL_BRH_IP_H57.txt");

		BufferedReader bufferedStream = new BufferedReader(fileStream);
		String data;
		int iADD = 0;
		int record = 0;
		do {
			data = bufferedStream.readLine();
			if (data == null) {
				break;
			}
			iADD++;
		} while (true);
		String[][] result = new String[iADD + 1][2];
		FileReader fileStream1 = new FileReader(profpath + "ALL_BRH_IP_H57.txt");
		BufferedReader bufferedStream1 = new BufferedReader(fileStream1);
		do {
			data = bufferedStream1.readLine();
			if (data == null) {
				break;
			}
			int token = -1;
			token = data.indexOf(",");
			if (token >= 0) {
				result[record][0] = data.substring(0, token);
				data = data.substring(token + 1);
				result[record][1] = data.substring(0);
				log.debug(ESAPIUtil.vaildLog("IPLIST:" + result[record][1]));
			} else {
				break;
			}
			record++;
		} while (true);
		String despath = path + "NBCCM903";
		log.debug(ESAPIUtil.vaildLog("despath = " + despath));

		File tmpFile = new File(despath);
		tmpFile.setWritable(true, true);
		tmpFile.setReadable(true, true);
		tmpFile.setExecutable(true, true);
		tmpFile.delete();
		OutputStream fos = new FileOutputStream(tmpFile);
		BufferedWriter out = new BufferedWriter(new OutputStreamWriter(fos, "BIG5"));
		String pageHeader1 = "1 報表代號 : CCM903                     非行員使用本行電腦（不含網銀專區）辦理網銀之轉帳類交易明細表                    週    期 / 保存年限：月報 / 3年\n";
		String pageHeader2 = "  資料日期 : " + Cyear + "/" + mon + "/01－" + Cyear + "/" + mon + "/" + sEndDay
				+ "                          印表日期 : " + printdate
				+ "                                         製表單位 : 資訊部\n";
		// String pageHeader3 = "";
		String pageFooter = "9            	          經　辦 :                    　覆　核 :                    　主　管 :\n";
		String header1 = "  單位代號    電 腦 IP 位 址    交易日期及交易時間    戶             名    身分證字號/統一編號    交    易    類    別    金          額    備       註\n";
		String header2 = "  --------    --------------    ------------------    -----------------    -------------------    --------------------    --------------    -----------\n";

		String strData = "";

		for (int i = 0; i < record; i++) { // 以分行依序查詢

			List<TXNLOG> all = txnLogDao.findByNNBReportM903(lastmonth, result[i][1]);
			log.trace(ESAPIUtil.vaildLog("NB 3 size >> " + all.size()));
			if ("Y".equals(isParallelCheck)) {
				List<OLD_TXNLOG> oldall = old_TxnLogDao.findByNNBReportM903(lastmonth, result[i][1]);
				log.trace(ESAPIUtil.vaildLog("NB 2.5 size >> " + oldall.size()));
				for (OLD_TXNLOG old : oldall) {
					TXNLOG nnbConvert2nb3 = CodeUtil.objectCovert(TXNLOG.class, old);
					all.add(nnbConvert2nb3);
				}
			}
			log.trace(ESAPIUtil.vaildLog("==CCM903== all.size() = " + all.size()));
			TXNLOG ft;
			String str_unit = result[i][0];
			// 單位代號10
			str_unit = "  " + str_unit + "         ";

			int counter = 0;
			for (int j = 0; j < all.size(); j++) {

				ft = all.get(j);
				List<TXNUSER> l_User = txnUserDao.findByUserId(ft.getADUSERID());

				if ("Y".equals(isParallelCheck) && l_User.size() == 0) {
					List<OLD_TXNUSER> old_l_User = old_TxnUserDao.findByUserId(ft.getADUSERID());
					for (OLD_TXNUSER user : old_l_User) {
						log.trace(ESAPIUtil.vaildLog("Get NB2.5 User >> " + user.getDPSUERID()));
						TXNUSER nnbConvert2nb3 = CodeUtil.objectCovert(TXNUSER.class, user);
						l_User.add(nnbConvert2nb3);
					}
				}

				TXNUSER u = l_User.get(0);
				String space = "";

				// IP018
				String userip = ft.getADUSERIP();
				String nbarea = userip.substring(userip.lastIndexOf(".") + 1); // 截取ip最後小數點後的數字，使用來判斷網銀專區ip
				if (nbarea.equals("151")) {
					continue;
				} else {
					for (int m = 0; m < 18 - (ft.getADUSERIP().trim().length()) * 1; m++)
						space += " ";
				}
				userip = userip + space;
				space = "";
				String LASTDATE = ft.getLASTDATE();
				String LASTTIME = ft.getLASTTIME();

				// 判斷是否為離職行員?
				String lDate = "";
				lDate = String.valueOf(Integer.parseInt(LASTDATE.substring(0, 4)) - 1911) + LASTDATE.substring(4, 8);

				log.trace(ESAPIUtil.vaildLog("lDate = " + lDate));
				log.trace(ESAPIUtil.vaildLog("ft.getADUSERID() = " + ft.getADUSERID()));

				List<ADMEMPINFO> Lemp = admEmpInfoDao.findLeaveEmp(ft.getADUSERID(), lDate);

				if ("Y".equals(isParallelCheck) && Lemp.size() == 0) {
					List<OLD_ADMEMPINFO> old_l_Lemp = old_AdmEmpinfoDao.findLeaveEmp(ft.getADUSERID(), lDate);
					for (OLD_ADMEMPINFO old_l_user : old_l_Lemp) {
						log.trace(ESAPIUtil.vaildLog("Get NB2.5 ADMEMPINFO >> " + old_l_user.getCUSID()));
						ADMEMPINFO nnbConvert2nb3 = CodeUtil.objectCovert(ADMEMPINFO.class, old_l_user);
						Lemp.add(nnbConvert2nb3);
					}
				}

				log.trace(ESAPIUtil.vaildLog("Lemp.size() = " + Lemp.size()));
				// 若有小於離職日ldate的資料，忽略
				if (Lemp.size() > 0) {
					continue;
				}

				// start Update 20180904
				// String
				// tempyear=String.valueOf(Integer.parseInt(LASTDATE.substring(0,4))-1911)+".";
				// String tempmon=LASTDATE.substring(4,6)+".";
				String tempyear = threeyear + ".";
				String tempmon = twomonth + ".";
				// end
				String tempday = LASTDATE.substring(6, 8) + " ";
				String temphour = LASTTIME.substring(0, 2) + ":";
				String tempminute = LASTTIME.substring(2, 4) + ":";
				String tempsec = LASTTIME.substring(4, 6);
				// 交易時間22
				String FULLDATETIME = tempyear + tempmon + tempday + temphour + tempminute + tempsec;
				for (int m = 0; m < 22 - (FULLDATETIME.trim().length()) * 1; m++)
					space += " ";
				FULLDATETIME = FULLDATETIME + space;
				space = "";
				// 戶名21
				String username = u.getDPUSERNAME();
				int word = StrUtils.countWordSpace(username);
				log.trace(ESAPIUtil.vaildLog("wordCount >> " + word));

				// for (int m=0; m < 21-(username.trim().length())*2; m++)
				for (int m = 0; m < 21 - word; m++)
					space += " ";
				username = username + space;
				space = "";
				// ID23
				String userid = ft.getADUSERID();
				for (int m = 0; m < 23 - (userid.trim().length()) * 1; m++)
					space += " ";
				userid = userid + space;
				space = "";

				try {
					Map map = JSONUtils.json2map(ft.getADCONTENT());
				} catch (Exception e) {
					log.error("ADCONTENT() error");
				}

				String adopid = ft.getADOPID();
				List<NB3SYSOP> l_sysop = nb3SysOpDao.findByADOPId(adopid);
				NB3SYSOP sysop = l_sysop.get(0);
				String typename = sysop.getADOPNAME();
				//因sysop menu調整而新增
				if(sysop.getURL().startsWith("/FCY") && sysop.getADOPNAME().indexOf("外幣")==-1) {
					typename = "外幣" + sysop.getADOPNAME();
				}
				if (typename.length() > 8)
					typename = typename.substring(0, 8);
				word = StrUtils.countWordSpace(typename);
				// 交易類別24
				for (int m = 0; m < 24 - word; m++)
					space += " ";
				typename = typename + space;
				space = "";
				String txamt = ft.getADTXAMT();
				String adcurrency = "";
				// N174外幣定存幣別判斷
				if (ft.getADCURRENCY().indexOf("01") >= 0)
					adcurrency = "USD";
				else if (ft.getADCURRENCY().indexOf("02") >= 0)
					adcurrency = "AUD";
				else if (ft.getADCURRENCY().indexOf("05") >= 0)
					adcurrency = "CAD";
				else if (ft.getADCURRENCY().indexOf("08") >= 0)
					adcurrency = "HKD";
				else if (ft.getADCURRENCY().indexOf("10") >= 0)
					adcurrency = "GBP";
				else if (ft.getADCURRENCY().indexOf("11") >= 0)
					adcurrency = "SGD";
				else if (ft.getADCURRENCY().indexOf("12") >= 0)
					adcurrency = "ZAR";
				else if (ft.getADCURRENCY().indexOf("13") >= 0)
					adcurrency = "SEK";
				else if (ft.getADCURRENCY().indexOf("14") >= 0)
					adcurrency = "CHF";
				else if (ft.getADCURRENCY().indexOf("15") >= 0)
					adcurrency = "JPY";
				else if (ft.getADCURRENCY().indexOf("18") >= 0)
					adcurrency = "THB";
				else if (ft.getADCURRENCY().indexOf("19") >= 0)
					adcurrency = "EUR";
				else if (ft.getADCURRENCY().indexOf("20") >= 0)
					adcurrency = "NZD";
				else if (ft.getADCURRENCY().indexOf("25") >= 0)
					adcurrency = "CNY";
				else
					adcurrency = ft.getADCURRENCY();

				adcurrency = adcurrency + " ";
				// 金額18
				String alltxamt = adcurrency + txamt;
				for (int m = 0; m < 18 - (alltxamt.trim().length()) * 1; m++)
					space += " ";
				alltxamt = alltxamt + space;
				space = "";
				strData = strData + str_unit + userip + FULLDATETIME + username + userid + typename + alltxamt + "\n#";
				okcnt++;
				totalcnt++;
				counter++;

				// System.out.println("==CCM903== okcnt = " + okcnt);
				// System.out.println("==CCM903== totalcnt = " + totalcnt);
				// System.out.println("==CCM903== counter = " + counter);

			}

			// System.out.println("==CCM903== strData = " + strData);
		}

		log.info(ESAPIUtil.vaildLog("==CCM903== totalcnt = " + totalcnt));
		log.info(ESAPIUtil.vaildLog("==CCM903== strData =  " + strData));

		String[] resultdata = new String[totalcnt];
		StringTokenizer st = new StringTokenizer(strData, "#", false);
		for (int i = 0; i < totalcnt; i++) {
			while (st.hasMoreElements()) {
				resultdata[i] = st.nextToken();
				// System.out.println("Token:" + resultdata[i]);
				break;
			}
		}

		log.info(ESAPIUtil.vaildLog("==CCM903== resultdata = " + resultdata.length));

		Double pageall_d = Math.ceil(((float) totalcnt / (float) 45));
		String pageall_s = pageall_d.toString().substring(0, pageall_d.toString().indexOf("."));
		int pageall = Integer.parseInt(pageall_s);
		log.info(ESAPIUtil.vaildLog("==CCM903== pageall = " + pageall));

		if (totalcnt == 0) {
			out.write(pageHeader1);
			out.write(pageHeader2);
			out.write(
					"  需求單位 : H57 數位金融部                                                                                             頁    次 : 1/1\n");
			out.write("\n\n\n");
			out.write(header1);
			out.write(header2);
			out.write(
					"   本月無資料                                                                                                               \n");
			for (int k = 0; k < 43; k++) {
				out.write("\n");
			}
			out.write(pageFooter);
		}

		boolean chkFoot = false;
		int chkcnt = 0;
		for (int ii = 1; ii <= pageall; ii++) {
			log.trace(ESAPIUtil.vaildLog("==CCM903== ii = " + ii));
			out.write(pageHeader1);
			out.write(pageHeader2);
			out.write(
					"  需求單位 : H57 數位金融部                                                                                             頁    次 : "
							+ ii + "/" + pageall + "\n");
			out.write("\n\n\n");
			out.write(header1);
			out.write(header2);
			chkFoot = true;
			if (totalcnt == 0) {
				out.write(
						"   本月無資料                                                                                                               \n");
				for (int k = 0; k < 43; k++) {
					out.write("\n");
				}
				out.write(pageFooter);
			} else {
				for (int jj = (ii - 1) * 45; jj < totalcnt; jj++) {
					log.trace(ESAPIUtil.vaildLog("==CCM903== jj = " + jj));
					log.trace(ESAPIUtil.vaildLog("==CCM903== resultdata[jj] =  " + resultdata[jj]));
					out.write(resultdata[jj]);
					chkcnt++;
					if (chkcnt == 45) {
						chkcnt = 0;
						break;
					}
				}
			}
			if (chkFoot) {
				log.info(ESAPIUtil.vaildLog("==CCM903== ii = " + ii + ", 45*ii-totalcnt = " + (45 * ii - totalcnt)));
				int fill_space = 45 * ii - totalcnt;
				for (int k = 0; k < fill_space; k++) {
					out.write("\n");
				}
				out.write(pageFooter);
			}
		}

		try {
			out.flush();
			out.close();
			if ("Y".equals(doFTP)) {
				// FTP機制啟動拋至報表系統主機
				FtpPathProperties nrcpro = new FtpPathProperties();
				String srcpath = nrcpro.getSrcPath("nnbreport_m903");
				log.debug(ESAPIUtil.vaildLog("srcpath = " + srcpath));
				String despath2 = nrcpro.getDesPath("nnbreport1_m903");
				log.debug(ESAPIUtil.vaildLog("despath1 = " + despath2));

				String ipn = reportn_ftp_ip;

				FtpBase64 ftpBase64 = new FtpBase64(ipn);
				int rc = ftpBase64.upload(despath2, path + "NBCCM903");
				if (rc != 0) {
					log.debug("FTP 失敗(fundreport 新主機)");
					// CheckServerHealth checkserverhealth = (CheckServerHealth) SpringBeanFactory
					// .getBean("checkserverhealth");
					checkserverhealth.sendUnknowNotice("FTP 失敗(fundreport 新主機)");
					log.debug("FTPSTATUS失敗");
				} else {
					log.debug("FTPSTATUS成功");
				}
			}
		} catch (Exception e) {
		}
		try {
			fos.close();
		} catch (Exception e) {
		}

		if (okcnt == 0) {
			rowcnt.put("OKCNT", okcnt);
			rowcnt.put("FAILCNT", failcnt);
			rowcnt.put("TOTALCNT", totalcnt);
			return;
		}
		rowcnt.put("OKCNT", okcnt);
		rowcnt.put("FAILCNT", failcnt);
		rowcnt.put("TOTALCNT", totalcnt);
		fileStream.close();
		bufferedStream.close();
		fileStream1.close();
		bufferedStream1.close();
	}
}
