package fstop.services.batch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.rest.model.CommonPools;
import com.netbank.util.ESAPIUtil;

import fstop.model.MVHImpl;
import fstop.notifier.NotifyAngent;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.TXNUSER;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import topibs.utility.NumericUtil;

/**
 * 基金
 * @author Owner
 *
 */
@Slf4j
@RestController
@Batch(id="batch.c051", name = "停損停利通知", description = "停損停利通知")
public class C051 implements BatchExecute{
	
	@Value("${batch.hostfile.path:}")
	private String pathname;
	
	@Value("${host.hostdata.path}")
	private String hostpath;
	
	@Autowired
	private TxnUserDao txnUserDao;
	
	@Autowired
	private CommonPools commonPools ;
	
	@Autowired
	private SysBatchDao sysBatchDao;

	private TXNUSER txnuser = new TXNUSER();
	
	
	@RequestMapping(value = "/batch/c051", method = {RequestMethod.POST},
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {
		
		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}
		
		List<String> safeArgs = ESAPIUtil.validStrList(args);
		Date d = new Date();
		String today = DateTimeUtils.getCDateShort(d);

		BatchResult batchresult = new BatchResult();
		
		log.debug("Batch_Execute_Date = " + today);
		
//		2019/10/09 修改成讀檔
		String filePath = pathname + "C051F.TXT";
		log.debug("filePath >>{}", filePath);
		
		//For Error log
		String hostfilePath = hostpath + "C051F.TXT";
		
		batchresult = readFile(filePath,hostfilePath);
		
		try {
			if (batchresult.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName,batchresult.getData());
			}
		}catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}
		
		return batchresult;
	}
	
	private BatchResult readFile(String tempfile ,String hostpath) {
		
		BatchResult batchresult = new BatchResult();
		Map<String, Object> data = new HashMap();
		
		Map<String, Object> resultData = new LinkedHashMap();
		
		InputStreamReader in = null;
		BufferedReader bin = null;
	  
		int totalcnt = 0, failcnt = 0, okcnt = 0, noneedcnt = 0, nousercnt = 0, cantfindmailcnt = 0;
		
		String Newcusidn="";
		String Oldcusidn="";
		List<TXNUSER> listtxnUser;
		String email=null;
		StringBuffer MailTextBuf=new StringBuffer();
		MailTextBuf.append("<table cellSpacing=0 cellPadding=0 width=600 frame=border >").append("\n");
		MailTextBuf.append("<tbody>").append("\n");
		MailTextBuf.append("<tr>").append("\n");
		MailTextBuf.append("<td width=\"100\"><DIV align= center><table width=100><tr><td width=\"100\"><DIV align= center>信託帳號</DIV></td></tr><tr><td width=\"100\"><DIV align= center>基金名稱</DIV></td></tr></table></DIV></td>").append("\n");
		MailTextBuf.append("<td width=\"100\"><DIV align= center>庫存單位數</DIV></td>").append("\n");
		MailTextBuf.append("<td width=\"40\"><DIV align= center>信託幣別</DIV></td>").append("\n");
		MailTextBuf.append("<td width=\"100\"><DIV align= center>信託金額</DIV></td>").append("\n");
		MailTextBuf.append("<td width=\"100\"><DIV align= center>參考基金損益</DIV></td>").append("\n");
		MailTextBuf.append("<td width=\"80\"><DIV align= center><FONT color=#ff0000>參考現值損益率</FONT></DIV></td>").append("\n");
		MailTextBuf.append("<td width=\"80\"><DIV align= center>預設損益率</DIV></TD>").append("\n");
		MailTextBuf.append("</tr>").append("\n");
		
		try {
			
			File file= new File(tempfile);
			//Incorrect Permission Assignment For Critical Resources
			file.setWritable(true,true);
			file.setReadable(true, true);
			file.setExecutable(true,true);
			long timestamp = file.lastModified();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String fmtDate = sdf.format(timestamp);
			log.info("File's LastModify fmt >> {}",fmtDate);
			Date d = new Date();
			String today = DateTimeUtils.getDateShort(d);
			
			if(!today.equals(fmtDate)) {
				data.put("MSG", "C051F.TXT MODIFY TIME NOT TODAY , SKIP EXECUTE !");
				batchresult.setSuccess(true);
				batchresult.setBatchName(this.getClass().getSimpleName());
				batchresult.setData(data);
				return batchresult;
			}
			
			in = new InputStreamReader(new FileInputStream(tempfile), "BIG5");
			bin = new BufferedReader(in);
			if(!bin.ready()) {
				in.close();
                bin.close();
            	throw new Exception();
			}
			
			String subject;
			
			while ((subject = ESAPIUtil.validInput(bin.readLine(),"GeneralString", true)) != null && subject.length() != 0) {
				Map<String, String> rowData = getData(subject);
				totalcnt++;
				if(!rowData.get("CUSIDN").equals(Oldcusidn) && !Oldcusidn.equals("") && email!=null)
				{
					MailTextBuf.append("</tbody>").append("\n");
					MailTextBuf.append("</table>").append("\n");
					Map<String, String> varmail = new HashMap();
					varmail.put("EMAILTEXT", MailTextBuf.toString());
					log.trace(ESAPIUtil.vaildLog("MailTextBuf = " + MailTextBuf.toString()));
					boolean isSendSuccess = NotifyAngent.sendNotice("C051", varmail, email.replace(";", ",").trim());
					if(!isSendSuccess) {
						log.error(ESAPIUtil.vaildLog("C051 發送 Email 失敗.(" + Oldcusidn + " " + email + ")"));
						failcnt++;
					}
					else
					{
						log.trace(ESAPIUtil.vaildLog("C051 發送 Email 成功.(" + Oldcusidn + " " + email + ")"));
						okcnt++;
					}
					email=null;
					MailTextBuf=null;
					MailTextBuf=new StringBuffer();
					MailTextBuf.append("<table cellSpacing=0 cellPadding=0 width=600 frame=border >").append("\n");
					MailTextBuf.append("<tbody>").append("\n");
					MailTextBuf.append("<tr>").append("\n");
					MailTextBuf.append("<td width=\"100\"><DIV align= center><table width=100><tr><td width=\"100\"><DIV align= center>信託帳號</DIV></td></tr><tr><td width=\"100\"><DIV align= center>基金名稱</DIV></td></tr></table></DIV></td>").append("\n");
					MailTextBuf.append("<td width=\"100\"><DIV align= center>庫存單位數</DIV></td>").append("\n");
					MailTextBuf.append("<td width=\"40\"><DIV align= center>信託幣別</DIV></td>").append("\n");
					MailTextBuf.append("<td width=\"100\"><DIV align= center>信託金額</DIV></td>").append("\n");
					MailTextBuf.append("<td width=\"100\"><DIV align= center>參考基金損益</DIV></td>").append("\n");
					MailTextBuf.append("<td width=\"80\"><DIV align= center><FONT color=#ff0000>參考現值損益率</FONT></DIV></td>").append("\n");
					MailTextBuf.append("<td width=\"80\"><DIV align= center>預設損益率</DIV></td>").append("\n");
					MailTextBuf.append("</tr>").append("\n");
					
				}
				
				log.trace(ESAPIUtil.vaildLog("CUSIDN = " + rowData.get("CUSIDN")));
				Oldcusidn=rowData.get("CUSIDN");
				listtxnUser = txnUserDao.findByUserId(rowData.get("CUSIDN"));
				log.trace("listtxnUser.size() = " + listtxnUser.size());
				if(listtxnUser==null || listtxnUser.size()==0)
				{
					listtxnUser=null;
					email=null;
					log.trace(ESAPIUtil.vaildLog("User ID not found = " + rowData.get("CUSIDN")));
					nousercnt++;
					continue;

				}

				txnuser=listtxnUser.get(0);
				String dpnotify = StrUtils.trim(txnuser.getDPNOTIFY());
				Set<String> dpnotifySet = StrUtils.splitToSet("\\s*,\\s*", dpnotify);
				
				boolean isUserSetTxnNotify = dpnotifySet.contains("14");
				if(isUserSetTxnNotify==false)
				{
					email=null;
					log.trace("Email 通知未被勾選 ");
					noneedcnt++;
					continue;
				}

				email=txnuser.getDPMYEMAIL();
				Newcusidn=txnuser.getDPSUERID();
				if(email==null || email.length()==0)
				{
					email=null;
					log.trace("Email 不存在");
					cantfindmailcnt++;
					continue;
				}

				MVHImpl fundData = (MVHImpl)commonPools.fundUtils.getFundData(rowData.get("TRANSCODE"));
				MailTextBuf.append("<tr>").append("\n");
				String cdno=rowData.get("CDNO");
				cdno = StrUtils.left(cdno, cdno.length() - 5) + "***" + StrUtils.right(cdno, 2);
				MailTextBuf.append("<td width=\"100\"><DIV align= center><table width=100><tr><td width=\"100\"><DIV align= center>").append(cdno).append("</DIV></td></tr><tr><td width=\"100\"><DIV align= center>").append((String)fundData.getValueByFieldName("FUNDSNAME")).append("</DIV></td></tr></table></DIV></td>").append("\n");
				MailTextBuf.append("<td width=\"100\"><DIV align= right>").append(NumericUtil.formatNumberString(rowData.get("ACUCOD")+rowData.get("ACUCOUNT"), 4)).append("</DIV></td>").append("\n");
				MailTextBuf.append("<td width=\"40\"><DIV align= center>").append(rowData.get("CRY")).append("</DIV></td>").append("\n");
				String totamtfmt= NumericUtil.formatNumberString(rowData.get("TOTCOD")+rowData.get("TOTAMT"), 2);
				MailTextBuf.append("<td width=\"100\"><DIV align= right>").append(totamtfmt).append("</DIV></td>").append("\n");
				String difamtfmt= NumericUtil.formatNumberString(rowData.get("DACOD")+rowData.get("DIFAMT"), 2);
				MailTextBuf.append("<td width=\"100\"><DIV align= right>").append(difamtfmt).append("</DIV></td>").append("\n");
				String difratfmt= NumericUtil.formatNumberString(rowData.get("DRCOD")+rowData.get("DIFRAT"), 3);
				MailTextBuf.append("<td width=\"80\"><DIV align= right><FONT color=#ff0000>").append(difratfmt+"%").append("</FONT></DIV></td>").append("\n");
				String setratfmt= NumericUtil.formatNumberString(rowData.get("SRCOD")+rowData.get("SETRAT"), 3);
				MailTextBuf.append("<td width=\"80\"><DIV align= right>").append(setratfmt+"%").append("</DIV></td>").append("\n");
				MailTextBuf.append("</tr>").append("\n");
				
				
				listtxnUser=null;
				txnuser=null;
			}
			if(email!=null )
			{
				MailTextBuf.append("</tbody>").append("\n");
				MailTextBuf.append("</table>").append("\n");
				Map<String, String> varmail = new HashMap();
				log.debug(ESAPIUtil.vaildLog("MailTextBuf = " + MailTextBuf.toString()));
				varmail.put("EMAILTEXT", MailTextBuf.toString());
				boolean isSendSuccess = NotifyAngent.sendNotice("C051", varmail, email.replace(";", ",").trim());
				if(!isSendSuccess) {
					log.error(ESAPIUtil.vaildLog("C051 發送 Email 失敗.(" + Newcusidn + " " + email + ")"));
					failcnt++;
				}
				else
				{
					log.debug(ESAPIUtil.vaildLog("C051 發送 Email 成功.(" + Newcusidn + " " + email + ")"));
					okcnt++;
				}
				MailTextBuf=null;
			}
			listtxnUser=null;
			txnuser=null;
			
			
			resultData.put("Send_Sucess", okcnt);
			resultData.put("Send_Fail", failcnt);
			resultData.put("No_Need_Send", noneedcnt);
			resultData.put("User_No_Email ", cantfindmailcnt);
			resultData.put("Cant_find_User", nousercnt);
			resultData.put("Total_Data", totalcnt);
			data.put("COUNTER", resultData);
			
			batchresult.setSuccess(true);
			batchresult.setBatchName(this.getClass().getSimpleName());
			batchresult.setData(data);
			
		} catch (FileNotFoundException e) {
			data.put("Error", "找不到檔案(" + hostpath + ")");
			batchresult.setData(data);
			batchresult.setSuccess(false);
		} catch (IOException e) {
			data.put("Error", "讀取檔案錯誤(" + hostpath + ")");
			batchresult.setData(data);
			batchresult.setSuccess(false);
		} catch (Exception e) {
			data.put("Error", "檔案為空(" + hostpath + ")");
			batchresult.setData(data);
			batchresult.setSuccess(false);
	    }finally {
			try {
				in.close();
				bin.close();
			} catch (Exception e) {
			}
			in = null;
			bin = null;
			
			return batchresult;
		}
		
	}
	
	public Map<String, String> getData(String record){
		 log.warn(ESAPIUtil.vaildLog("C051 LINE STR:" + record));
		Map<String, String> dataMap = new HashMap();
		String[] key = {"CUSIDN","CDNO","REFNO","TRANSCODE","ACUCOD","ACUCOUNT","CRY",
						"TOTCOD","TOTAMT","PRVCOD","PRVAMT","DACOD","DIFAMT","DRCOD",
						"DIFRAT","SRCOD","SETRAT","MIPCOD"};
		String[] data = record.split("#\\|");
      
       for(int i = 0;i<key.length;i++) {
       	dataMap.put(key[i],data[i].trim());
       }
       
       return dataMap;
	}
}

