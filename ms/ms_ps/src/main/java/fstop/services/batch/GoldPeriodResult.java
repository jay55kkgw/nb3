package fstop.services.batch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.ESAPIUtil;

import fstop.notifier.NotifyAngent;
import fstop.notifier.NotifyListener;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.TXNUSER;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import topibs.utility.NumericUtil;

/**
 * 黃金存摺定期定額即將扣款通知 FKALST1.TXT
 * 
 * @author Owner
 *
 */
@Slf4j
@RestController
@Batch(id = "batch.goldPeriodResult", name = "讀檔案，發email 通知", description = "讀檔案，發email 通知")
public class GoldPeriodResult implements EmailUploadIf, BatchExecute {

	@Autowired
	private TxnUserDao txnUserDao;

	@Autowired
	private SysBatchDao sysBatchDao;

	@Value("${batch.hostfile.path:}")
	private String pathName;

	@Value("${batch.otherfile.path:}")
	private String otherDir;

	@Value("${host.hostdata.path}")
	private String hhdpath;

	@Value("${host.other.path}")
	private String hopath;

	@RequestMapping(value = "/batch/goldPeriodResult", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {
		List<String> safeArgs = ESAPIUtil.validStrList(args);
		
		log.info("INTO GoldPeriodResult BATCH ... ");
		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}

		Map<String, Object> data = new HashMap();
		BatchResult batchresult = new BatchResult();
		batchresult.setBatchName(getClass().getSimpleName());
		Map<String, Object> resultData = new LinkedHashMap();

		int totalcnt = 0, failcnt = 0, okcnt = 0, noneedcnt = 0, nousercnt = 0, cantfindmailcnt = 0;
		String FullFileName = new String();
		String FullFileName_Host = new String();
		InputStreamReader in = null;
		BufferedReader bin = null;
		String subject = new String();
		Date d = new Date();
		String today = DateTimeUtils.getDateShort(d);
		String time = DateTimeUtils.getTimeShort(d);
		Hashtable sendmaildata = new Hashtable();

		List<String> errorList = new ArrayList();

		log.debug("DATE = " + today);
		log.debug("TIME = " + time);
		FullFileName = pathName;
		FullFileName += "FKALST1.TXT";

		// For Error log
		FullFileName_Host = hhdpath;
		FullFileName_Host += "FKALST1.TXT";
		
		log.debug("FullFileName = " + FullFileName);
		int dataCount = 0;
		try {
			in = new InputStreamReader(new FileInputStream(FullFileName), "MS950");
			bin = new BufferedReader(in);

			if (!bin.ready()) {
				data.put("Error", "檔案為空(" + FullFileName_Host + ")");
				batchresult.setData(data);
				batchresult.setSuccess(false);
				in.close();
				bin.close();
				try {
					sysBatchDao.finish_FAIL2(batchName, batchresult.getData());
				} catch (Exception e2) {
					log.error("sysBatchDao.finish error >> {} ", e2.getMessage());
				}
				return batchresult;
			}

			while ((subject = ESAPIUtil.validInput(bin.readLine(), "GeneralString", true)) != null
					&& subject.length() != 0) {
				dataCount++;
				log.debug("=============== data{} start=====================", dataCount);
				byte[] subjectarray = subject.getBytes("MS950");

				// 黃金存摺帳號
				byte[] acnarray = new byte[11];
				System.arraycopy(subjectarray, 0, acnarray, 0, acnarray.length);
				String acno = new String(acnarray).trim();

				sendmaildata.put("#ACN", StrUtils.left(acno, acno.length() - 5) + "***" + StrUtils.right(acno, 2));

				// 身分證號
				byte[] cusidnoarray = new byte[10];
				System.arraycopy(subjectarray, acnarray.length, cusidnoarray, 0, cusidnoarray.length);
				String cusidno = new String(cusidnoarray).trim();

				log.debug("cusidno = {} , acno = {} ", cusidno, acno);

				List<TXNUSER> listtxnUser;
				listtxnUser = txnUserDao.findByUserId(cusidno);
				log.debug("listtxnUser.size() = " + listtxnUser.size());
				if (listtxnUser == null || listtxnUser.size() == 0) {
					log.debug("User ID not found = " + cusidno);
					nousercnt++;
					totalcnt++;
					continue;
				}

				TXNUSER txnuser = listtxnUser.get(0);
				String dpnotify = StrUtils.trim(txnuser.getDPNOTIFY());
				Set<String> dpnotifySet = StrUtils.splitToSet("\\s*,\\s*", dpnotify);

				boolean isUserSetTxnNotify = dpnotifySet.contains("18");
				if (isUserSetTxnNotify == false) {
					log.debug(
							ESAPIUtil.vaildLog("User >> {} , no need to Send this Notify(18)" + txnuser.getDPSUERID()));
					noneedcnt++;
					totalcnt++;
					continue;
				}
				// 預約日期
				byte[] qtdatearray = new byte[2];
				System.arraycopy(subjectarray, acnarray.length + cusidnoarray.length, qtdatearray, 0,
						qtdatearray.length);
				String qtdate = new String(qtdatearray).trim();

				log.debug("appdate = " + qtdate);
				sendmaildata.put("#QTDATE", qtdate);

				// 約定扣款帳號
				byte[] svacnarray = new byte[11];
				System.arraycopy(subjectarray, acnarray.length + cusidnoarray.length + qtdatearray.length, svacnarray,
						0, svacnarray.length);
				String svacn = new String(svacnarray).trim();

				log.debug("svacn = " + svacn);
				sendmaildata.put("#OUTACN",
						StrUtils.left(svacn, svacn.length() - 5) + "***" + StrUtils.right(svacn, 2));

				// 約定扣款金額
				byte[] qtamtarray = new byte[13];
				System.arraycopy(subjectarray,
						acnarray.length + cusidnoarray.length + qtdatearray.length + svacnarray.length, qtamtarray, 0,
						qtamtarray.length);
				String qtamt = new String(qtamtarray).trim();
				log.debug("qtamt = " + NumericUtil.formatNumberString(qtamt, 0));
				sendmaildata.put("#QTAMT", NumericUtil.formatNumberString(qtamt, 0));

				// 手續費
				byte[] trnfeearray = new byte[11];
				System.arraycopy(subjectarray, acnarray.length + cusidnoarray.length + qtdatearray.length
						+ svacnarray.length + qtamtarray.length, trnfeearray, 0, trnfeearray.length);
				String trnfee = new String(trnfeearray).trim();
				log.debug("trnfee = " + NumericUtil.formatNumberString(trnfee, 0));
				sendmaildata.put("#TRNFEE", NumericUtil.formatNumberString(trnfee, 0));

				// 總扣款金額
				byte[] trnamtarray = new byte[13];
				System.arraycopy(subjectarray, acnarray.length + cusidnoarray.length + qtdatearray.length
						+ svacnarray.length + qtamtarray.length + trnfeearray.length, trnamtarray, 0,
						trnamtarray.length);

				String trnamt = new String(trnamtarray).trim();

				sendmaildata.put("#TRNAMT", NumericUtil.formatNumberString(trnamt, 0));

				int rc = chkemail(sendmaildata, cusidno);
				if (rc == 0) {

					rc = sendmail(sendmaildata);
					if (rc == 1) {
						totalcnt++;
						failcnt++;
						errorList.add("data" + dataCount + ", CUSIDN:" + txnuser.getDPSUERID() + ", Eamil:"
								+ sendmaildata.get("MAILADDR"));
					} else {
						totalcnt++;
						okcnt++;

					}

				} else {
					totalcnt++;
					cantfindmailcnt++;
				}

				log.debug("=============== data{} end=====================", dataCount);
			}

		} catch (FileNotFoundException e) {
			// throw new MsgTemplateNotFoundException("找不到對應的 template 檔(" + FullFileName +
			// ").", e);
			log.error("找不到 (" + FullFileName_Host + ")檔案," + e.getMessage());
			data.put("Error", "找不到檔案(" + FullFileName_Host + ")");
			batchresult.setData(data);
			batchresult.setSuccess(false);
			try {
				sysBatchDao.finish_FAIL2(batchName, batchresult.getData());
			} catch (Exception e2) {
				log.error("sysBatchDao.finish error >> {} ", e2.getMessage());
			}
			
			return batchresult;
		} catch (IOException e) {
			// throw new ToRuntimeException("讀取 template 檔錯誤(" + FullFileName + ").", e);
			log.error("IOException (" + FullFileName_Host + ")," + e.getMessage());
			data.put("Error", "讀取檔案錯誤(" + FullFileName_Host + ")");
			batchresult.setData(data);
			batchresult.setSuccess(false);
			try {
				sysBatchDao.finish_FAIL2(batchName, batchresult.getData());
			} catch (Exception e2) {
				log.error("sysBatchDao.finish error >> {} ", e2.getMessage());
			}
			
			return batchresult;
		} finally {
			try {
				in.close();
				bin.close();
			} catch (Exception e) {
			}
			in = null;
			bin = null;
		}

		Date nowdate = new Date();
		Calendar c1 = Calendar.getInstance();
		c1.setTime(nowdate);
		c1.add(Calendar.DATE, -1);
		String yesterday = DateTimeUtils.getDateShort(c1.getTime());

		// log.debug("run /TBB/nb3/other/bak_sh/goldperiodresultbak.sh");
		// RuntimeExec runtimeExec=new
		// RuntimeExec( otherDir ,"./goldperiodresultbak.sh " + yesterday);
		// if(runtimeExec.getcommstat()!=0)
		// {
		// log.debug("/TBB/nnb/batch/goldperiodresultbak.sh exec command error");
		//// mkdir /backup5y/20090901/email/
		// }

		log.debug("============ Backup Area ============");
		log.debug("DO goldperiodresultbak.sh");
		log.debug("--Check Dir Start--");
		String datadatedir = otherDir + "backup5y/" + yesterday;
		String datadatedirHost = hopath + "backup5y/" + yesterday;
		log.debug("data datedir >>" + datadatedirHost);
		File file = new File(datadatedir);
		file.setWritable(true, true);
		file.setReadable(true, true);
		file.setExecutable(true, true);
		if (!file.exists()) {
			log.debug("mkdir >>" + datadatedirHost);
			file.mkdir();
		}
		String emailbakdir = datadatedir + "/email";
		String emailbakdirHost = datadatedirHost + "/email";
		log.debug("email bakdir >>" + emailbakdirHost);
		file = new File(emailbakdir);
		file.setWritable(true, true);
		file.setReadable(true, true);
		file.setExecutable(true, true);
		if (!file.exists()) {
			log.debug("mkdir >>" + emailbakdirHost);
			file.mkdir();
		}

		String fkaprePath = pathName + "/FKALST1.TXT";
		
		String backDirShow =  emailbakdirHost+"/";

		File file1 = new File(fkaprePath);
		file1.setWritable(true, true);
		file1.setReadable(true, true);
		file1.setExecutable(true, true);
		File file2 = new File(emailbakdir + "/FKALST1.TXT");
		file2.setWritable(true, true);
		file2.setReadable(true, true);
		file2.setExecutable(true, true);
		// 檔案若存在則移除
		if (file1.exists()) {
			file2.delete();
			try {
				FileUtils.copyFile(file1, file2);
				log.debug("move {} to {} OK", "FKALST1.TXT", backDirShow);
				data.put("BackupStatus", "Success move FKALST1.TXT to "+backDirShow);
				file1.delete();
			} catch (IOException e) {
				log.error("move file error");
				data.put("BackupStatus", "Fail");
			}
		}
		
		log.debug("-- Move file End --");
		log.debug("============ Backup Area End ============");

		resultData.put("Send_Sucess", okcnt);
		resultData.put("Send_Fail", failcnt);
		resultData.put("No_Need_Send", noneedcnt);
		resultData.put("User_No_Email ", cantfindmailcnt);
		resultData.put("Cant_find_User", nousercnt);
		resultData.put("Total_Data", totalcnt);
		data.put("COUNTER", resultData);

		if (errorList.size() != 0) {
			data.put("ErrorDataList", errorList);
		} 
		batchresult.setSuccess(true);
		batchresult.setData(data);
		try {
			sysBatchDao.finish_OK(batchName);
		}catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}
		
		return batchresult;

	}

	public int sendmail(Hashtable maildata) {
		log.debug("<<<<< sendmail >>>>>");
		final StringBuffer status = new StringBuffer();
		NotifyListener listener = new NotifyListener() {

			public void fail(String reciver, String subject, String msg, Exception e) {
				status.append("Failed");
			}

			public void success(String reciver, String subject, String msg) {
				status.append("Success");
			}
		};

		NotifyAngent.sendNotice("GOLD_PERIOD_RESULT", maildata, (String) maildata.get("MAILADDR"), listener);

		if ("Success".equals(status.toString())) {
			log.debug("發送 Email 成功.(" + maildata.get("MAILADDR") + ")");
			return (0);
		} else {
			log.debug("發送 Email 失敗.(" + maildata.get("MAILADDR") + ")");
			return (1);
		}
	}

	public int chkemail(Hashtable email, String cusidn) {
		log.debug("<<<<< chkemail >>>>>");
		String mailaddr = null;
		TXNUSER user = null;
		List<TXNUSER> listtxnUser;
		try {
			log.debug("where cusidn = " + cusidn);
			listtxnUser = txnUserDao.findByUserId(cusidn);
			user = listtxnUser.get(0);
			mailaddr = user.getDPMYEMAIL().trim();
		} catch (Exception e) {
			log.error("無法取得 USER ID (" + cusidn + ")," + e.getMessage());
			return (1);
		}

		if (mailaddr.length() == 0 || mailaddr == null) {
			log.error(" ***** Mail is't exist.(USER ID : " + cusidn + ") ");
			return (1);
		}

		log.debug(ESAPIUtil.vaildLog("mailaddr = " + mailaddr));
		email.put("MAILADDR", mailaddr);
		return (0);

	}

}
