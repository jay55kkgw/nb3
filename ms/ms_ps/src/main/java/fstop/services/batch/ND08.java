package fstop.services.batch;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.EachRowCallback;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.notifier.NotifyAngent;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.SysParamDataDao;
import fstop.orm.po.SYSPARAMDATA;
import fstop.services.AfterSuccessQuery;
import fstop.services.batch.annotation.Batch;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 中心連線測試
 * 
 * @author Owner 每小時40分自動執行 寄件者:景森 DATE: TIME: 系統狀態: 成功 失敗 retry 3 times
 */
@Slf4j
@RestController
@Batch(id = "batch.nd08", name = "中心連線測試", description = "更新主機日期時間(與中心同步)")
public class ND08 extends DispatchBatchExecute implements BatchExecute {
	// private Logger logger = Logger.getLogger("fstop_txntw");

	private DecimalFormat fmt = new DecimalFormat("#0");

	@Autowired
	@Qualifier("nd08Telcomm")
	private TelCommExec nd08Telcomm;

	@Value("${WebService.SendMail.SenderMail}")
	private String emailsend;

	@Autowired
	private CheckServerHealth checkserverhealth;

	@Autowired
	private SysParamDataDao sysParamDataDao;

	@Autowired
	private SysBatchDao sysBatchDao;

	/**
	 * args[0] method name
	 */
	@RequestMapping(value = "/batch/nd08", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {

		log.info("batch ND08...");

		List<String> safeArgs = ESAPIUtil.validStrList(args);

		Hashtable<String, String> params = new Hashtable();

		BatchResult batchresult = new BatchResult();
		Map<String, Object> data = new LinkedHashMap();
		batchresult.setBatchName(getClass().getSimpleName());
		batchresult.setSuccess(true);

		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}

		SimpleTemplate simpleTemplate = new SimpleTemplate(nd08Telcomm);

		simpleTemplate.setAfterSuccessQuery(new AfterSuccessQuery() {

			public void execute(MVHImpl result) {
				SYSPARAMDATA po = null;
				Hashtable<String, String> varmail = new Hashtable();

				String hoststatus = null;
				po = sysParamDataDao.findById("NBSYS");
				if (po == null) {
					log.error("Execute query sysParamData table error");
					return;
				}

				String emailap = po.getADAPMAIL().replace(";", ",");
				log.debug("emailap = " + emailap);
				// String email1 = (String)setting.get("email1");

				String hostmsg = result.getValueByFieldName("MSGCOD");

				String hostdate = result.getValueByFieldName("DATE");
				if (hostdate == null) {
					Date d = new Date();
					hostdate = DateTimeUtils.getCDateShort(d);
				}

				data.put("HOSTDATE", hostdate);

				String hosttime = result.getValueByFieldName("TIME");
				if (hosttime == null) {
					Date d = new Date();
					hosttime = DateTimeUtils.getTimeShort(d);
				}

				data.put("HOSTTIME", hosttime);
				if (hostmsg.equals("0000")) {
					hoststatus = new String("成功");
					varmail.put("MACHNAME", " ");
				} else {
					hoststatus = new String("失敗");
					// CheckServerHealth checkserverhealth =
					// (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
					varmail.put("MACHNAME", "機器名稱：" + checkserverhealth.GetHostName());
				}
				varmail.put("EMAILSEND", emailsend);
				varmail.put("SYSDATE",
						hostdate.substring(0, 3) + "/" + hostdate.substring(3, 5) + "/" + hostdate.substring(5, 7));
				varmail.put("SYSTIME",
						hosttime.substring(0, 2) + ":" + hosttime.substring(2, 4) + ":" + hosttime.substring(4, 6));
				varmail.put("SYSSTATUS", hoststatus);
				// call BillHunter 成功不送MAIL 2017.04.18 By 景森
				// boolean isSendSuccess = NotifyAngent.sendNotice("ND08", varmail, emailap,
				// "batch");
				// if(!isSendSuccess) {
				// log.debug("發送 Email 失敗.(" + emailap + ")");
				// result.getFlatValues().put("__MAILERR", "發送 Email 失敗.(" + emailap + ")");
				// }

			}

		});

		simpleTemplate.setEachRowCallback(new EachRowCallback() {

			public void current(Row row) {
				// String userEmail = emailGetter.getUserEmail(idn);

			}

		});
		simpleTemplate.setExceptionHandler(new ExceptionHandler() {

			public void handle(Row row, Exception e) {

				log.error("無法更用戶通知訊息.", e);

			}

		});

		try {
			BatchCounter counter = simpleTemplate.execute(params);
		} catch (TopMessageException topmsg) {
			data.put("TOPMSG", topmsg.getMsgcode() + "  " + topmsg.getMsgout());
			log.error("TopMessageException  !!", topmsg);
			SYSPARAMDATA po = null;
			Hashtable<String, String> varmail = new Hashtable();
			Date d = new Date();
			String today = DateTimeUtils.getCDateShort(d);
			String time = DateTimeUtils.getTimeShort(d);

			String hoststatus = null;
			po = sysParamDataDao.findById("NBSYS");
			if (po == null) {
				
				log.debug("Query NBSYS table error");
				data.put("Error", "Query NBSYS table error");
				batchresult.setData(data);
				batchresult.setSuccess(false);
				try {
					sysBatchDao.finish_FAIL2(batchName, batchresult.getData());
				} catch (Exception e2) {
					log.error("sysBatchDao.finish error >> {} ", e2.getMessage());
				}
				
				return batchresult;
			}

			String emailap = po.getADAPMAIL();

			String hostmsg = topmsg.getMsgcode();
			String hostdate = today.substring(0, 3) + "/" + today.substring(3, 5) + "/" + today.substring(5, 7);
			if (hostdate == null)
				hostdate = "          ";
			String hosttime = time.substring(0, 2) + ":" + time.substring(2, 4) + ":" + time.substring(4, 6);
			if (hosttime == null)
				hosttime = "        ";
			hoststatus = hostmsg + " " + topmsg.getMsgout();

			varmail.put("EMAILSEND", emailsend);
			// CheckServerHealth checkserverhealth =
			// (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
			varmail.put("MACHNAME", "機器名稱：" + checkserverhealth.GetHostName());
			varmail.put("SYSDATE", hostdate);
			varmail.put("SYSTIME", hosttime);
			varmail.put("SYSSTATUS", hoststatus);

			// call BillHunter
			boolean isSendSuccess = NotifyAngent.sendNotice("ND08", varmail, emailap, "batch");
			if (!isSendSuccess) {
				log.debug("發送 Email 失敗.(" + emailap + ")");
				// batchresult.getFlatValues().put("__MAILERR", "發送 Email 失敗.(" + email1 + ")");
				data.put("Error", "發送 Email 失敗.(" + emailap + ")");
				batchresult.setData(data);
				batchresult.setSuccess(false);
				try {
					sysBatchDao.finish_FAIL2(batchName, batchresult.getData());
				} catch (Exception e2) {
					log.error("sysBatchDao.finish error >> {} ", e2.getMessage());
				}
				
				return batchresult;
			}
			log.debug("發送 Email 成功.(" + emailap + ")");

		}

		batchresult.setData(data);
		try {
			// 新增SYSBATCH紀錄
			sysBatchDao.finish_OK(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.finish_OK error >> {} ", e.getMessage());
		}

		return batchresult;

	}
}
