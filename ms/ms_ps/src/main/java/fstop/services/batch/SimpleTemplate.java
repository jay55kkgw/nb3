package fstop.services.batch;

import java.util.Hashtable;
import java.util.Map;

import fstop.exception.ToRuntimeException;
import fstop.exception.UncheckedException;
import fstop.model.EachRowCallback;
import fstop.model.Row;
import fstop.model.RowsSelecter;
import fstop.model.TelcommResult;
import fstop.services.AfterSuccessQuery;
import fstop.telcomm.TelCommExec;

public class SimpleTemplate {

	
	private TelCommExec telcomm;
	
	private EachRowCallback callback;
	
	private ExceptionHandler handler;
	
	private AfterSuccessQuery afterSuccessQuery;
	
	public SimpleTemplate(TelCommExec telcomm) {
		this.telcomm = telcomm;
	}
	
	public void setEachRowCallback(EachRowCallback callback) {
		this.callback = callback;
	}
	
	public void setExceptionHandler(ExceptionHandler handler) {
		this.handler = handler;
	}
	

	public void setAfterSuccessQuery(AfterSuccessQuery afterSuccessQuery) {
		this.afterSuccessQuery = afterSuccessQuery;
	}

	public BatchCounter execute(final Map<String, String> params) {
		
		if(handler == null)
			throw new ToRuntimeException("未設定 ExceptionHandler.");
		
		TelcommResult qresult = telcomm.query(params);
		
		if(afterSuccessQuery != null)
			afterSuccessQuery.execute(qresult);
		
		RowsSelecter rowselecter  = new RowsSelecter(qresult.getOccurs());
		final BatchCounter counter = new BatchCounter();
		rowselecter.each(new EachRowCallback() {

			public void current(Row row) {

				try {
					SimpleTemplate.this.callback.current(row);
					counter.incSuccess();
				} 
				catch(UncheckedException e) {
					counter.incFail();
					handler.handle(row, e);
				}
				finally {
					counter.incTotalCount();
				}
				
			}
			
		});
		
		return counter;
	}

}
