package fstop.services.batch;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import fstop.notifier.NotifyAngent;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.SysParamDao;
import fstop.orm.dao.TxnCSSSLogDao;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;




@Slf4j
@RestController
@Batch(id="batch.cssslog", name = "顧客滿意度調查表", description = "顧客滿意度調查表")
public class CsssLog implements BatchExecute {
//	private log log = log.getlog(getClass());
	
	@Autowired
	private TxnCSSSLogDao txnCSSSLogDao;
	
	@Autowired
	private SysParamDao sysParamDao;
	
	@Autowired
	private SysBatchDao sysBatchDao;
	
	@RequestMapping(value = "/batch/cssslog", method = {RequestMethod.POST},
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {
		
		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}
		
		BatchResult batchresult = new BatchResult();
		Map<String, Object> data = new HashMap();
		batchresult.setBatchName(batchName);
		
		try {
			// 開關沒開  直接退出
			if("N".equals(sysParamDao.getSysParam("isOpenCsssLog"))) {
				batchresult.setSuccess(true);
				data.put("MSG", "FUNCTION NOT OPEN , TABLE SYSPARAM isOpenCsssLog is N ");
				return batchresult;
			}
			
			
			Date dNow = new Date();
			String nowYear = DateTimeUtils.format("yyyy",dNow); //紀錄執行年
			String nowTime = DateTimeUtils.format("yyyyMMdd",dNow); //紀錄執行時間
			String yearMonth = nowTime.substring(0,6);
			
			//12月1日執行    yearMonth > 20201101 , nowTime > 20201130
			if("12".equals(nowTime.substring(4,6))) {
				yearMonth = nowYear+"11";
				nowTime = nowYear+"1130";
			}
			
			String emailap = sysParamDao.getSysParam("TxnCsssLogMail").replace(";", ",");
			String countNB1 = "";
			String countNB2 = "";
			String countNB3 = "";
			String countNB4 = "";
			String countNB5 = "";
			String countMB1 = "";
			String countMB2 = "";
			String countMB3 = "";
			String countMB4 = "";
			String countMB5 = "";
			String nextbtncnt = "";
			//註記功能統計項目
			String[][] funclist = {{"N130","臺幣帳戶明細查詢"},
								   {"N520","外幣帳戶明細查詢"},
								   {"N070","臺幣轉帳"},
								   {"N074","臺幣轉入綜存定存"},
								   {"N3003","提前償還本金"},
								   {"F001","買賣外幣"},
								   {"F002","匯出匯款"},
								   {"N174","轉入外幣綜存定存"},
								   {"C012","基金餘額損益查詢"},
								   {"B012","海外債券餘額損益查詢"},
								   {"C016","基金單筆申購"},
								   {"C017","基金定期投資申購"},
								   {"C021","基金轉換"},
								   {"C024","基金贖回"}};
			
			this.log.debug("CsssLog.java nowYear() YEAR===>" + nowYear);
			this.log.debug("CsssLog.java emailap===>" + emailap);			
			//TxnCSSSLogDao txnCSSSLogDao = (TxnCSSSLogDao)SpringBeanFactory.getBean("txnCSSSLogDao");
			String Nextbtncnt = txnCSSSLogDao.findByNextBtn(nowYear);
			StringBuffer MailTextBuf=new StringBuffer();
			MailTextBuf.append("<center><fonze size=5><b>顧客滿意度調查表各項統計</b></font></center>").append("\n");
			MailTextBuf.append("資料日期：").append(yearMonth).append("01~").append(nowTime).append("\n");
			MailTextBuf.append("<div align='left'><table border=1 cellSpacing=0 cellPadding=0 width=620 frame=border >").append("\n");
			MailTextBuf.append("<tr><td width='160'> </td><td width='60'><center>通路</center></td><td width='70'><center>非常滿意</center></td><td width='60'><center>滿意</center></td><td width='60'><center>普通</center></td><td width='60'><center>再加強</center></td><td width='80'><center>非常不滿意</center></td><td width='70'><center>下次填寫</center></td></tr>").append("\n");
			for(int i=0;i<funclist.length;i++){
				countNB1 =  txnCSSSLogDao.findByAdopid(nowYear, "NB3",funclist[i][0] , "5");
				countMB1 =  txnCSSSLogDao.findByAdopid(nowYear, "MB3",funclist[i][0] , "5");
				countNB2 =  txnCSSSLogDao.findByAdopid(nowYear, "NB3",funclist[i][0] , "4");
				countMB2 =  txnCSSSLogDao.findByAdopid(nowYear, "MB3",funclist[i][0] , "4");
				countNB3 =  txnCSSSLogDao.findByAdopid(nowYear, "NB3",funclist[i][0] , "3");
				countMB3 =  txnCSSSLogDao.findByAdopid(nowYear, "MB3",funclist[i][0] , "3");
				countNB4 =  txnCSSSLogDao.findByAdopid(nowYear, "NB3",funclist[i][0] , "2");
				countMB4 =  txnCSSSLogDao.findByAdopid(nowYear, "MB3",funclist[i][0] , "2");
				countNB5 =  txnCSSSLogDao.findByAdopid(nowYear, "NB3",funclist[i][0] , "1");
				countMB5 =  txnCSSSLogDao.findByAdopid(nowYear, "MB3",funclist[i][0] , "1");
				if(i==0)
					MailTextBuf.append("<tr><td width='160'><center>").append(funclist[i][1]).append("</center></td><td width='60'><center>NB3/MB3</center></td><td width='70'><center>").append(countNB1).append("/").append(countMB1).append("</center></td><td width='60'><center>").append(countNB2).append("/").append(countMB2).append("</center></td><td width='60'><center>").append(countNB3).append("/").append(countMB3).append("</center></td><td width='60'><center>").append(countNB4).append("/").append(countMB4).append("</center></td><td width='80'><center>").append(countNB5).append("/").append(countMB5).append("</center></td><td width='70' rowspan='14'><center>").append(Nextbtncnt).append("</center></td></tr>").append("\n");
				else
					MailTextBuf.append("<tr><td width='160'><center>").append(funclist[i][1]).append("</center></td><td width='60'><center>NB3/MB3</center></td><td width='70'><center>").append(countNB1).append("/").append(countMB1).append("</center></td><td width='60'><center>").append(countNB2).append("/").append(countMB2).append("</center></td><td width='60'><center>").append(countNB3).append("/").append(countMB3).append("</center></td><td width='60'><center>").append(countNB4).append("/").append(countMB4).append("</center></td><td width='80'><center>").append(countNB5).append("/").append(countMB5).append("</center></td></tr>").append("\n");
			}
			MailTextBuf.append("</table></div>").append("\n");
									
			Map<String, String> varmail = new HashMap();
			varmail.put("EMAILTEXT", MailTextBuf.toString());
			log.warn("MailTextBuf = " + MailTextBuf.toString());


			boolean isSendSuccess = NotifyAngent.sendNotice("TXNCSSS", varmail, emailap, "batch");
			if (!isSendSuccess) {
				log.error("發送 Email 失敗.");
				batchresult.setSuccess(false);
				data.put("MSG", "MAIL FAILED");
				return batchresult;
			}
		} catch ( Exception e) {
			log.error("CsssLog Error");
			data.put("MSG", "DAO FAILED");
			batchresult.setSuccess(false);
			return batchresult;
		}
		
		batchresult.setSuccess(true);
		data.put("MSG", "SUCCESS");
		batchresult.setData(data);
		
		try {
			if (batchresult.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName,batchresult.getData());
			}
		}catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}

		return batchresult;
	}
	
}
