package fstop.services.batch;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.ESAPIUtil;

import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.TxnCrlineAppDao;
import fstop.orm.po.TXNCRLINEAPP;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import fstop.util.FtpBase64;
import fstop.util.FtpPathProperties;
import fstop.util.SpringBeanFactory;
import lombok.extern.slf4j.Slf4j;
import topibs.utility.DateUtil;

/**
 * 每日產生昨日網銀信用卡額度調升線上申請清單至 /TBB/nb3/data/NBCCD914 並把檔案拋至報表系統
 * 
 * @author Owner 每頁45筆
 */

@Slf4j
@RestController
@Batch(id = "batch.nnbReport_D914", name = "信用卡額度調升線上申請清單NBCCD914", description = "每日產生昨日網銀信用卡額度調升線上申請清單至 /TBB/nb3/data/NBCCD914 並把檔案拋至報表系統")
public class NNBReport_D914 implements BatchExecute {

	@Autowired
	private TxnCrlineAppDao txnCrlineAppDao;

	@Value("${reportn.ftp.ip}")
	private String reportn_ftp_ip;

	@Value("${batch.localfile.path:}")
	private String path;

	@Value("${doFTP}")
	private String doFTP;

	@Autowired
	private SysBatchDao sysBatchDao;

	@RequestMapping(value = "/batch/nnbReport_D914", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {

		log.info("batch NNBReport_D914...");

		BatchResult batchresult = new BatchResult();
		Map<String, Object> data = new LinkedHashMap();
		batchresult.setBatchName(getClass().getSimpleName());
		batchresult.setSuccess(true);

		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}

		List<String> safeArgs = ESAPIUtil.validStrList(args);
		log.info("NNBReport_D914 - Start execute ");
		int failcnt = 0, okcnt = 0, totalcnt = 0;
		Hashtable rowcnt = new Hashtable();

		try {
			if (safeArgs.size() == 1) {
				log.info(ESAPIUtil.vaildLog("arg[0]: " + safeArgs.get(0)));
				Date date = DateTimeUtils.parse("yyyyMMdd", safeArgs.get(0));
				exec(rowcnt, date , null);
			}else if (safeArgs.size() == 2) {
				log.info(ESAPIUtil.vaildLog("arg[0]: " + safeArgs.get(0)));
				Date date = DateTimeUtils.parse("yyyyMMdd", safeArgs.get(0));
				log.info(ESAPIUtil.vaildLog("arg[1]: " + safeArgs.get(1)));
				Date date2 = DateTimeUtils.parse("yyyyMMdd", safeArgs.get(1));
				exec(rowcnt, date , date2);
			} else {
				exec(rowcnt);
			}
			failcnt = ((Integer) rowcnt.get("FAILCNT")).intValue();
			okcnt = ((Integer) rowcnt.get("OKCNT")).intValue();
			totalcnt = ((Integer) rowcnt.get("TOTALCNT")).intValue();
			log.debug("failcnt = " + failcnt);
			log.debug("okcnt = " + okcnt);
			log.debug("totalcnt = " + totalcnt);
		} catch (Exception e) {
			log.error("執行 Batch 有誤 !", e);
			data.put("Error", e.getMessage());
			batchresult.setSuccess(false);
		}

		BatchCounter bcnt = new BatchCounter();
		bcnt.setFailCount(failcnt);
		bcnt.setSuccessCount(okcnt);
		bcnt.setTotalCount(totalcnt);

		data.put("COUNTER", bcnt);
		batchresult.setData(data);

		log.info("NNBReport_D914 - End execute ");

		try {
			if (batchresult.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName, batchresult.getData());
			}
		} catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}
		return batchresult;
	}

	private void exec(Hashtable rowcnt) throws IOException {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, -1);
		exec(rowcnt, cal.getTime(),null);
	}

	private void exec(Hashtable rowcnt, final Date date , final Date date2) throws IOException {
		Integer totalcnt = 0, failcnt = 0, okcnt = 0;
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		// Date d2 = new Date();
		// d2.setDate(d2.getDate() - 1);
		// String yesterday = DateTimeUtils.getCDateShort(d2);
		// yesterday = yesterday.substring(0, 7);
		String lastdate = DateTimeUtils.format("yyyyMMdd", cal.getTime());
		Date d = new Date();
		String str_ReportDate = DateUtil.formatDate(DateUtil.getTaiwanDate(lastdate));
		String str_ReportDateTime = DateTimeUtils.getCDateTime(DateTimeUtils.getCDateShort(d),
				DateTimeUtils.getTimeShort(d));

		String despath = path + "NBCCD914";
		log.warn("despath = " + despath);

		File tmpFile = new File(despath);
		tmpFile.setWritable(true, true);
		tmpFile.setReadable(true, true);
		tmpFile.setExecutable(true, true);
		tmpFile.delete();
		OutputStream fos = new FileOutputStream(tmpFile);
		BufferedWriter out = new BufferedWriter(new OutputStreamWriter(fos, "BIG5"));
		String pageHeader1 = "1 報表代號 : CCD914                  　　　　　　　　網銀信用卡額度調升線上申請清單　　　　　　　　                                週期/保存年限：日報 /15年                       \n";
		String pageHeader2 = "  資料日期 : " + str_ReportDate + "                               印表日期 : "
				+ str_ReportDateTime + "                                                          頁    次 :   ";
		String pageHeader3 = "";
		String pageFooter = "9            	          經　辦 :                    　覆　核 :                    　主　管 :\n";
		String header3 = "  申請日期   申請時間  身分證字號  客戶姓名    信用卡卡號           目前額度   申請調升額度 申請調升期間 上傳財力件數  行動電話         IP          申請通路\n";
		String header4 = "  ============================================================================================================================================================ \n";
		String str_date = DateTimeUtils.format("yyyyMMdd", date);
		
		List<TXNCRLINEAPP> all = null;
		//單日
		if(null==date2) {
			all = txnCrlineAppDao.getDataByDate(str_date);
		//期間
		}else {
			String str_date2 = DateTimeUtils.format("yyyyMMdd", date2);
			all = txnCrlineAppDao.getDataByRangeDate(str_date,str_date2);
		}
		
		int totalreccount = 0;
		int tempcounter = 0;
		
		log.warn("all size = " + all.size());
		totalreccount = all.size();
		
		log.warn("totalreccount = " + totalreccount);
		pageHeader3 = "  需求單位 : H69                                                                                                                           製表單位 : 資訊部\n\n\n";
		Double totalpage = Math.ceil(((float) totalreccount / (float) 45));// 預設45筆
		String totalpage1 = totalpage.toString().substring(0, totalpage.toString().indexOf("."));
		log.warn("H69:" + "totalpage:" + totalpage1 + ",allsize:" + totalreccount);
		if (totalpage == 0) {
			out.write(pageHeader1);
			out.write(pageHeader2 + "1/1\n");
			out.write(pageHeader3);
			out.write(header3);
			out.write(header4);
			out.write(
					"                     無網銀申請紀錄                                                                     \n");
			for (int k = 0; k < 44; k++) {
				out.write("\n");
			}
			out.write(pageFooter);
		} else {
			for (int l = 0; l < totalpage; l++) {
				out.write(pageHeader1);
				out.write(pageHeader2 + (l + 1) + "/" + totalpage1 + "\n");
				out.write(pageHeader3);
				out.write(header3);
				out.write(header4);
				int counter = 0;
				for (int j = l * 45; j < totalreccount; j += counter) {
					log.warn("reportD914 j:" + j + " counter:" + counter);
					for (int n = tempcounter; n < all.size(); n++) {
						log.debug("n:" + n);
						String space = "";
						String f = "";
						String firstpadding = "  ";// 申請日期左邊兩個空白
						String applyDate = all.get(tempcounter).getPks().getCPRIMDATE();
						applyDate = transformDATE(applyDate);
						for (int m = 0; m < 11 - (applyDate.length()) * 1; m++)// 10為該欄位值到下一個欄位值之間的距離
						{
							space += " ";// 計算欄位與欄位之間的空白
						}
						applyDate+= space;
						space = "";// RESET空白數，為了計算下一個欄位的間隔
						
						String applyTime = all.get(tempcounter).getCPRIMTIME();
						applyTime = transformTime(applyTime);
						for (int m = 0; m < 10 - (applyTime.length()) * 1; m++)// 10為該欄位值到下一個欄位值之間的距離
						{
							space += " ";// 計算欄位與欄位之間的空白
						}
						applyTime+= space;
						space = "";// RESET空白數，為了計算下一個欄位的間隔
						
						String cusidn = all.get(tempcounter).getPks().getCPRIMID();
						for (int m = 0; m < 12 - (cusidn.length()) * 1; m++)// 10為該欄位值到下一個欄位值之間的距離
						{
							space += " ";// 計算欄位與欄位之間的空白
						}
						cusidn+= space;
						space = "";// RESET空白數，為了計算下一個欄位的間隔
						
						String cusname = all.get(tempcounter).getCPRIMCHNAME();
						for (int m = 0; m < 12 - (cusname.length()) * 2; m++)// 10為該欄位值到下一個欄位值之間的距離
						{
							space += " ";// 計算欄位與欄位之間的空白
						}
						cusname+= space;
						space = "";// RESET空白數，為了計算下一個欄位的間隔
						
						String cdno = all.get(tempcounter).getPks().getCARDNUM();
						cdno = transformCdno(cdno);
						for (int m = 0; m < 21 - (cdno.length()) * 1; m++)// 10為該欄位值到下一個欄位值之間的距離
						{
							space += " ";// 計算欄位與欄位之間的空白
						}
						cdno+= space;
						space = "";// RESET空白數，為了計算下一個欄位的間隔
						
						String ocrline = all.get(tempcounter).getOCRLINE();
						for (int m = 0; m < 11 - (ocrline.length()) * 1; m++)// 10為該欄位值到下一個欄位值之間的距離
						{
							space += " ";// 計算欄位與欄位之間的空白
						}
						ocrline+= space;
						space = "";// RESET空白數，為了計算下一個欄位的間隔
						
						String ncrline = all.get(tempcounter).getNCRLINE();
						for (int m = 0; m < 13 - (ncrline.length()) * 1; m++)// 10為該欄位值到下一個欄位值之間的距離
						{
							space += " ";// 計算欄位與欄位之間的空白
						}
						ncrline+= space;
						space = "";// RESET空白數，為了計算下一個欄位的間隔
						
						String crlinetpe = all.get(tempcounter).getCRLINETPE();
						if("P".equals(crlinetpe)) {
							crlinetpe = "永久";
							for (int m = 0; m < 19 - (crlinetpe.length()) * 2; m++)// 10為該欄位值到下一個欄位值之間的距離
							{
								space += " ";// 計算欄位與欄位之間的空白
							}
							crlinetpe+= space;
							
						}else {
							crlinetpe = transformDATE(all.get(tempcounter).getCRLINEBEGDATE())+"-"+transformDATE(all.get(tempcounter).getCRLINEENDDATE());
							for (int m = 0; m < 19 - (crlinetpe.length()) * 1; m++)// 10為該欄位值到下一個欄位值之間的距離
							{
								space += " ";// 計算欄位與欄位之間的空白
							}
							crlinetpe+= space;
						}
						space = "";// RESET空白數，為了計算下一個欄位的間隔
						
						String upfiles = all.get(tempcounter).getUPFILES();
						for (int m = 0; m < 8 - (upfiles.length()) * 1; m++)// 10為該欄位值到下一個欄位值之間的距離
						{
							space += " ";// 計算欄位與欄位之間的空白
						}
						upfiles+= space;
						space = "";// RESET空白數，為了計算下一個欄位的間隔
						
						String cprimcellulano = all.get(tempcounter).getCPRIMCELLULANO();
						for (int m = 0; m < 12 - (cprimcellulano.length()) * 1; m++)// 10為該欄位值到下一個欄位值之間的距離
						{
							space += " ";// 計算欄位與欄位之間的空白
						}
						cprimcellulano+= space;
						space = "";// RESET空白數，為了計算下一個欄位的間隔
						
						String ip = all.get(tempcounter).getIP();
						for (int m = 0; m < 17 - (ip.length()) * 1; m++)// 10為該欄位值到下一個欄位值之間的距離
						{
							space += " ";// 計算欄位與欄位之間的空白
						}
						ip+= space;
						space = "";// RESET空白數，為了計算下一個欄位的間隔
						
						String chantpe = all.get(tempcounter).getCHANTPE();
						if("NB".equals(chantpe)) {
							chantpe = "網路銀行";
						}else if ("MB".equals(chantpe)) {
							chantpe = "行動銀行";
						}
						for (int m = 0; m < 15 - (chantpe.length()) * 2; m++)// 10為該欄位值到下一個欄位值之間的距離
						{
							space += " ";// 計算欄位與欄位之間的空白
						}
						chantpe+= space;
						space = "";// RESET空白數，為了計算下一個欄位的間隔
						
//						//個人資料運用告知
//						//同意聲明書-版本別
//						String version1 = all.get(tempcounter).getVERSION1();
//						for (int m = 0; m < 20 - (version1.length()) * 1; m++)// 10為該欄位值到下一個欄位值之間的距離
//						{
//							space += " ";// 計算欄位與欄位之間的空白
//						}
//						version1+= space;
//						space = "";// RESET空白數，為了計算下一個欄位的間隔
//						
//						//個人網路銀行暨行動銀行
//						//服務條款-版本別
//						String version2 = all.get(tempcounter).getVERSION2();
//						for (int m = 0; m < 23 - (version2.length()) * 1; m++)// 10為該欄位值到下一個欄位值之間的距離
//						{
//							space += " ";// 計算欄位與欄位之間的空白
//						}
//						version2+= space;
//						space = "";// RESET空白數，為了計算下一個欄位的間隔
//						
//						String chkid = all.get(tempcounter).getCHKID();
//						if(" ".equals(chkid)) {
//							chkid="Y";
//						}else {
//							chkid="N";
//						}
//						for (int m = 0; m < 12 - (chkid.length()) * 1; m++)// 10為該欄位值到下一個欄位值之間的距離
//						{
//							space += " ";// 計算欄位與欄位之間的空白
//						}
//						chkid+= space;
//						space = "";// RESET空白數，為了計算下一個欄位的間隔
						
//						f = firstpadding + applyDate + applyTime + cusidn +cusname + cdno + ocrline + ncrline 
//								+ crlinetpe + upfiles + cprimcellulano + ip + chantpe + version1 + version2 + chkid + "\n";
						f = firstpadding + applyDate + applyTime + cusidn +cusname + cdno + ocrline + ncrline 
								+ crlinetpe + upfiles + cprimcellulano + ip + chantpe + "\n";
						
						out.write(f);
						okcnt++;
						totalcnt++;
						counter++;
						tempcounter++;
						if (counter % 45 == 0)
							break;
					}
					log.warn("counter:" + counter + " tempcounter:" + tempcounter);
					if ((totalreccount % 45) != 0 && (totalcnt == totalreccount)) {
						int fill_space = 45 - (totalreccount % 45);
						for (int k = 0; k < fill_space; k++) {
							out.write("\n");
						}
					}
					if (counter % 45 == 0)
						break;
				}
				out.write(pageFooter);
			}
		}
		try {
			out.flush();
			out.close();
			fos.close();
		}catch (Exception e) {
			log.error("close error");
		}
		try {
			if ("Y".equals(doFTP)) {
				// FTP機制啟動拋至報表系統主機
				FtpPathProperties nrcpro = new FtpPathProperties();
				String srcpath = nrcpro.getSrcPath("nnbreport_d914");
				log.warn(ESAPIUtil.vaildLog("srcpath = " + srcpath));
				String despath2 = nrcpro.getDesPath("nnbreport_d914");
				log.warn(ESAPIUtil.vaildLog("despath1 = " + despath2));
				String ipn = reportn_ftp_ip;
				FtpBase64 ftpBase64 = new FtpBase64(ipn);
				int rc = ftpBase64.upload(despath2, path + "/NBCCD914");
				// int rc=ftpBase64.upload(despath2, Path.dataFolder + "/NBCCD903");
				if (rc != 0) {
					log.warn("FTP 失敗(fundreport 新主機)");
					CheckServerHealth checkserverhealth = (CheckServerHealth) SpringBeanFactory
							.getBean("checkserverhealth");
					checkserverhealth.sendUnknowNotice("FTP 失敗(fundreport 新主機)");
					log.warn("FTPSTATUS失敗");
				} else {
					log.warn("FTPSTATUS成功");
				}
			}
		} catch (Exception e) {
			log.error("FTP error ");
		}
		
		if (okcnt == 0) {
			rowcnt.put("OKCNT", okcnt);
			rowcnt.put("FAILCNT", failcnt);
			rowcnt.put("TOTALCNT", totalcnt);
			return;
		}
		rowcnt.put("OKCNT", okcnt);
		rowcnt.put("FAILCNT", failcnt);
		rowcnt.put("TOTALCNT", totalcnt);
	}
	
	private String transformTime(String time) {
		return time.substring(0,2)+":"+time.subSequence(2, 4)+":"+time.substring(4);
	}
	
	private String transformCdno(String cdno) {
		return cdno.substring(0,4)+"-"+cdno.subSequence(4, 8)+"-"+cdno.substring(8,12)+"-"+cdno.substring(12);
	}
	
	private String transformDATE(String date) {
		String twDate = DateUtil.getTaiwanDate(date);
		return twDate.substring(0,3)+"/"+twDate.substring(3,5).replace("0", "")+"/"+twDate.substring(5);
	}
	
}
