package fstop.services.batch;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.ESAPIUtil;

import fstop.model.MVHImpl;
import fstop.model.Rows;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.oao.Old_TxnUserDao;
import fstop.orm.po.TXNUSER;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import fstop.util.FtpBase64;
import fstop.util.FtpPathProperties;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 每月產生非行員客戶於一般網銀留存行內電子郵件統計表"至 /TBB/nb3/data/NBCCM910 並把檔案拋至報表系統
 * 
 * @author Owner 每頁45筆
 */
@Slf4j
@RestController
@Batch(id = "batch.NNBReport_M910", name = "非行員客戶於一般網銀留存行內電子郵件統計表", description = "每月產生非行員客戶於一般網銀留存行內電子郵件統計表\"至 /TBB/nb3/data/NBCCM910 並把檔案拋至報表系統")
public class NNBReport_M910 implements BatchExecute {

	@Autowired
	private TxnUserDao txnUserDao;

	@Autowired
	private Old_TxnUserDao old_TxnUserDao;

	@Value("${prodf_path}")
	private String profpath;

	@Value("${batch.localfile.path:}")
	private String path;

	@Value("${reportn.ftp.ip}")
	private String reportn_ftp_ip;

	@Value("${isParallelCheck}")
	private String isParallelCheck;

	@Value("${doFTP}")
	private String doFTP;

	@Autowired
	private SysBatchDao sysBatchDao;

	@RequestMapping(value = "/batch/nnbReport_M910", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {

		log.info("batch NNBReport_M910...");

		BatchResult batchresult = new BatchResult();
		Map<String, Object> data = new LinkedHashMap();
		batchresult.setBatchName(getClass().getSimpleName());
		batchresult.setSuccess(true);

		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}

		List<String> safeArgs = ESAPIUtil.validStrList(args);
		log.info("網銀交易明細資料CCM910  ... ");
		int failcnt = 0, okcnt = 0, totalcnt = 0;
		Map rowcnt = new HashMap();
		// start Update 20180905
		log.info(ESAPIUtil.vaildLog("args size:" + safeArgs.size()));
		String yyy = "";
		String mm = "";
		String lastmonth = "";
		if (safeArgs.size() == 1) {// 當批次有帶參數(帶參數是為了要產出特定月份的報表檔)
			yyy = safeArgs.get(0).substring(0, 3);// 民國年
			mm = safeArgs.get(0).substring(3);// 月份
		} else { // 批次未帶參數(未帶參數是要產出上個月的報表檔)
			Date d = new Date();
			java.util.Calendar calendar = java.util.Calendar.getInstance();
			calendar.setTime(d);
			calendar.add(calendar.MONTH, -1);
			d = calendar.getTime();
			lastmonth = DateTimeUtils.getDateShort(d);
			log.debug(ESAPIUtil.vaildLog("lastmonth not substring : " + lastmonth));
			yyy = String.valueOf(Integer.parseInt(lastmonth.substring(0, 4)) - 1911);
			mm = lastmonth.substring(4, 6);
		}
		log.debug(ESAPIUtil.vaildLog("yyy:" + yyy + " mm:" + mm));
		// end
		try {
			// start Update 20180905
			// exec(rowcnt);
			exec(rowcnt, yyy, mm);
			// end
			failcnt = ((Integer) rowcnt.get("FAILCNT")).intValue();
			okcnt = ((Integer) rowcnt.get("OKCNT")).intValue();
			totalcnt = ((Integer) rowcnt.get("TOTALCNT")).intValue();
			log.debug(ESAPIUtil.vaildLog("failcnt = " + failcnt));
			log.debug(ESAPIUtil.vaildLog("okcnt = " + okcnt));
			log.debug(ESAPIUtil.vaildLog("totalcnt = " + totalcnt));
		} catch (Exception e) {
			log.error("執行 Batch 有誤 !", e);
			data.put("Error", e.getMessage());
			batchresult.setSuccess(false);
		}

		BatchCounter bcnt = new BatchCounter();
		bcnt.setFailCount(failcnt);
		bcnt.setSuccessCount(okcnt);
		bcnt.setTotalCount(totalcnt);

		data.put("COUNTER", bcnt);

		batchresult.setData(data);

		log.debug("NNBReport_M910 - End execute ");

		try {
			if (batchresult.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName, batchresult.getData());
			}
		} catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}
		return batchresult;
	}

	private void exec(Map rowcnt, String threeyear, String twomonth) throws IOException {
		Integer totalcnt = 0, failcnt = 0, okcnt = 0;
		Date d = new Date();
		java.util.Calendar calendar = java.util.Calendar.getInstance();
		calendar.setTime(d);
		String nowdate = DateTimeUtils.getDateShort(d);
		String printdate = String.valueOf(Integer.parseInt(nowdate.substring(0, 4)) - 1911) + "/"
				+ nowdate.substring(4, 6) + "/" + nowdate.substring(6, 8);
		calendar.add(calendar.MONTH, -1);
		d = calendar.getTime();

		String lastmonth = String.valueOf(Integer.parseInt(threeyear) + 1911) + twomonth;
		lastmonth = lastmonth + "%";

		String year = String.valueOf(Integer.parseInt(threeyear) + 1911);
		String mon = twomonth;
		log.debug(ESAPIUtil.vaildLog("year:" + year + " mon:" + mon));

		String sEndDay = "";
		if (mon.equals("01") || mon.equals("03") || mon.equals("05") || mon.equals("07") || mon.equals("08")
				|| mon.equals("10") || mon.equals("12")) {
			sEndDay = "31";
		} else if (mon.equals("04") || mon.equals("06") || mon.equals("09") || mon.equals("11")) {
			sEndDay = "30";
		} else {
			if (mon.equals("02") && (Integer.parseInt(year) % 4 == 0) && (Integer.parseInt(year) % 100 != 0)) {
				sEndDay = "29";
			} else if (mon.equals("02")) {
				sEndDay = "28";
			}
		}

		String Cyear = threeyear;
		log.debug(ESAPIUtil.vaildLog("Cyear = " + Cyear));
		// end
		log.debug(ESAPIUtil.vaildLog("lastmonth = " + lastmonth + ",mon=" + mon + ",year=" + year));
		// FileReader fileStream = new
		// FileReader("/TBB/nnb/cfg/ALL_BRH_List_CCM9H57.txt");//分行清單檔
		FileReader fileStream = new FileReader(profpath + "ALL_BRH_List_CCM9H57.txt");

		BufferedReader bufferedStream = new BufferedReader(fileStream);
		String data;
		int iADD = 0;
		int record = 0;
		do {
			data = ESAPIUtil.validInput(bufferedStream.readLine(), "GeneralString", true);
			// log.debug("BRH data:" + data);
			if (data == null) {
				break;
			}
			iADD++;// 計算分行總數
		} while (true);
		String[] result = new String[iADD];// 存放分行別
		// FileReader fileStream1 = new
		// FileReader("/TBB/nnb/cfg/ALL_BRH_List_CCM9H57.txt");//分行清單檔
		FileReader fileStream1 = new FileReader(profpath + "ALL_BRH_List_CCM9H57.txt");
		BufferedReader bufferedStream1 = new BufferedReader(fileStream1);
		do {
			data = ESAPIUtil.validInput(bufferedStream1.readLine(), "GeneralString", true);
			if (data == null) {
				break;
			}

			result[record] = data;// 存放分行別
			// log.debug("BRH LIST:"+result[record]);
			record++;
		} while (true);
		String despath = path + "NBCCM910";// 報表檔
		log.debug(ESAPIUtil.vaildLog("despath = " + despath));

		File tmpFile = new File(despath);
		tmpFile.setWritable(true, true);
		tmpFile.setReadable(true, true);
		tmpFile.setExecutable(true, true);
		tmpFile.delete();
		OutputStream fos = new FileOutputStream(tmpFile);
		BufferedWriter out = new BufferedWriter(new OutputStreamWriter(fos, "BIG5"));
		String pageHeader1 = "1 報表代號 : CCM910                     非行員客戶於一般網路銀行留存行內電子郵件統計表                    週    期 / 保存年限：月報 / 1年\n";
		String pageHeader2 = "  資料日期 : " + Cyear + "/" + mon + "/01－" + Cyear + "/" + mon + "/" + sEndDay
				+ "             印表日期 : " + printdate + "                                        頁    次 : ";
		String pageHeader3 = "";
		String pageFooter = "9            	          經　辦 :                    　覆　核 :                    　主　管 :\n";
		// String header1 = " 分行別 姓名 身分證字號 電子信箱 電子信箱持有者 處理情形 \n";
		String header1 = "  分行別    姓名       身分證字號    電子信箱                                              電子信箱持有者          處理情形    \n";
		String header2 = "  ------    -------    -----------   --------------------------------------------------    --------------          --------                                  \n";

		// String strData ="";
		// record = txnUserDao.recordTXNUSER();//計算TXNUSER總筆數
		log.debug(ESAPIUtil.vaildLog("record:" + record));
		TXNUSER tuer = null;
		List<TXNUSER> all = null;
		MVHImpl allresultxnusertmvh = null, resultxnusertmvh = null; // 同步 20200225
		MVHImpl allquerymvh = null;// 同步 TXNUSER查詢結果彙總資料
		int queryNum = 0;// 計算每家分行有幾筆資料
		String adbranchid = "";
		allresultxnusertmvh = txnUserDao.findByNNBReportM910();// 取得所有分行非行員清單
		log.debug("allresultxnusertmvh size >> {}", allresultxnusertmvh.getOccurs().getSize());
		if ("Y".equals(isParallelCheck)) {
			MVHImpl oldallresultxnusertmvh = null;
			oldallresultxnusertmvh = old_TxnUserDao.findByNNBReportM910();
			log.debug("oldallresultxnusertmvh size >> {}", oldallresultxnusertmvh.getOccurs().getSize());
			allresultxnusertmvh = getCombineRows(allresultxnusertmvh, oldallresultxnusertmvh);
		}

		for (int i = 0; i < record; i++) { // 以分行依序查詢
			if (!result[i].equals("H57")) {
				resultxnusertmvh = seperateList(allresultxnusertmvh, result[i]);
				queryNum = resultxnusertmvh.getValueOccurs("DPSUERID");// 計算非H57以外分行資料筆數
			} else {
				allquerymvh = seperateAllList(allresultxnusertmvh);
				queryNum = allquerymvh.getValueOccurs("DPSUERID");// 計算所有分行資料筆數
			}
			log.trace("queryNum:" + queryNum);
			Double totalpage = Math.ceil(((float) queryNum / (float) 37));// 計算每分行有幾頁資料
			String totalpage1 = totalpage.toString();
			log.trace(ESAPIUtil.vaildLog("totalpage1:" + totalpage1));
			pageHeader3 = "  需求單位 : " + result[i]
					+ "                                                                                          製表單位 : 資訊部\n\n\n\n";
			if (totalpage == 0)// 無筆數
			{
				out.write(pageHeader1);
				out.write(pageHeader2 + "1/1\n");// 顯示頁次
				out.write(pageHeader3);
				out.write(header1);
				out.write(header2);
				out.write(
						"                     無網銀交易紀錄                                                                      ");
				for (int k = 0; k < 37; k++) {
					out.write("\n");
				}

			} else {
				for (int l = 0; l < totalpage; l++) { // 需執行頁數
					log.trace(ESAPIUtil.vaildLog("pageHeader1 and pageHeader2 and pageHeader3  l:" + l));
					out.write(pageHeader1);
					out.write(pageHeader2 + (l + 1) + "/" + totalpage1 + "\n");// 顯示頁次
					out.write(pageHeader3);
					out.write(header1);
					out.write(header2);
					int counter = 0;

					// 計算報表每頁扣掉資料後還需換行次數，一頁最多37筆
					for (int j = l * 37; j < queryNum; j++) {
						// adbranchid =all.get(j).getADBRANCHID().trim();
						adbranchid = resultxnusertmvh.getValueByFieldName("ADBRANCHID", j + 1).trim();// 同步
																										// 增加分行檔由MVH物件取得
						log.trace(ESAPIUtil.vaildLog("if ADBRANCHID==>" + adbranchid + "<=="));

						if (adbranchid.equals(result[i])) {// 有分行資料就須寫入該分行清單內

							// String f = " "+tuer.getADBRANCHID()+" "+subStringName(tuer.getDPUSERNAME())+"
							// "+tuer.getDPSUERID()+"
							// "+email+admEmpInfoDao.getEMPNObyEMPMAIL(mailconvert(tuer.getDPMYEMAIL()))+"
							// "+"\n";//分行別+姓名+身分證字號+電子信箱+電子信箱持有者+處理情形;
							String f = "  " + resultxnusertmvh.getValueByFieldName("ADBRANCHID", j + 1) + "       "
									+ subStringName(resultxnusertmvh.getValueByFieldName("DPUSERNAME", j + 1)) + " "
									+ resultxnusertmvh.getValueByFieldName("DPSUERID", j + 1) + "    "
									+ leftSpace(resultxnusertmvh.getValueByFieldName("DPMYEMAIL", j + 1), 50, "")
									+ "         " + resultxnusertmvh.getValueByFieldName("EMPNO", j + 1) + "\n";// 分行別+姓名+身分證字號+電子信箱+電子信箱持有者+處理情形;
							log.debug(ESAPIUtil.vaildLog("ADBRANCHID NOT SPACE Content = " + f));
							out.write(f);// 將資料寫進落地檔NBCCM910
							okcnt++;
							counter++;
							if ((queryNum % 37) != 0 && (j == queryNum - 1)) {
								int fill_space = 37 - (queryNum % 37);
								for (int k = 0; k < fill_space; k++) {
									out.write("\n");
								}
							}
							if (counter == 38)// 第38行就換頁
								break;
						} else { // 報表內每家分行超過37行就換行
							// //處理email長度(TABLE開很長 但前端限定50 所以先以長度50來設定
							// String space="";
							// int word = StrUtils.countWordSpace(tuer.getDPMYEMAIL());
							// for (int m=0; m < 58-word; m++)
							// space+=" ";
							// String email = tuer.getDPMYEMAIL()+space;
							// space="";
							//
							// log.debug(ESAPIUtil.vaildLog("else ADBRANCHID==>"+adbranchid+"<=="));
							// if(adbranchid.equals("")){//如歸戶行為空值，需歸類於H57
							// log.debug("~~~~ADBRANCHID IS SPACE~~~~");
							// tuer = all.get(j);
							// String f = " "+tuer.getADBRANCHID()+" "+subStringName(tuer.getDPUSERNAME())+"
							// "+StringUtils.rightPad(tuer.getDPSUERID(), 10, ' ')+"
							// "+email+admEmpInfoDao.getEMPNObyEMPMAIL(mailconvert(tuer.getDPMYEMAIL()))+"
							// "+"\n";//分行別+姓名+身分證字號+電子信箱+處理情形;
							String f = "  " + allquerymvh.getValueByFieldName("ADBRANCHID", j + 1) + "          "
									+ subStringName(allquerymvh.getValueByFieldName("DPUSERNAME", j + 1)) + " "
									+ allquerymvh.getValueByFieldName("DPSUERID", j + 1) + "    "
									+ leftSpace(allquerymvh.getValueByFieldName("DPMYEMAIL", j + 1), 50, "")
									+ "         " + allquerymvh.getValueByFieldName("EMPNO", j + 1) + "\n";// 分行別+姓名+身分證字號+電子信箱+電子信箱持有者+處理情形;
							log.debug(ESAPIUtil.vaildLog("ADBRANCHID IS SPACE Content = " + f));
							if (counter == 38) {// 第38行就換頁
								break;
							}
							out.write(f);// 將資料寫進落地檔NBCCM910
							okcnt++;
							counter++;
							if ((queryNum % 37) != 0 && (j == queryNum - 1)) {
								int fill_space = 37 - (queryNum % 37);
								for (int k = 0; k < fill_space; k++) {
									out.write("\n");
								}
							}
						}
					} // END for 每頁幾筆 loop
				} // END for loop
			} // END IF totalpage!=0
		} // END for loop
		try {
			out.flush();
			out.close();
			if ("Y".equals(doFTP)) {
				// FTP機制啟動拋至報表系統主機
				FtpPathProperties nrcpro = new FtpPathProperties();
				String srcpath = nrcpro.getSrcPath("nnbreport_m910");
				log.debug(ESAPIUtil.vaildLog("srcpath = " + srcpath));
				// String despath1=nrcpro.getDesPath("nnbreport_m902");
				// log.debug("despath = " + despath1);
				String despath2 = nrcpro.getDesPath("nnbreport1_m910");
				log.debug(ESAPIUtil.vaildLog("despath1 = " + despath2));

				// String ip = (String)setting.get("ftp.ip");
				// String ipn = (String)setting.get("ftp.ipn");
				String ipn = reportn_ftp_ip;
				// FtpBase64 ftpBase64 = new FtpBase64(ip);
				// int rc=ftpBase64.upload(despath1, Path.dataFolder + "/NBCCM902");
				/*
				 * if(rc!=0) { log.debug("FTP 失敗(fundreport 舊主機)"); CheckServerHealth
				 * checkserverhealth =
				 * (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
				 * checkserverhealth.sendUnknowNotice("FTP 失敗(fundreport 舊主機)");
				 * log.debug("FTPSTATUS失敗"); } else { log.debug("FTPSTATUS成功"); }
				 */
				FtpBase64 ftpBase64 = new FtpBase64(ipn);
				int rc = ftpBase64.upload(despath2, path + "NBCCM910");
				if (rc != 0) {
					log.debug("FTP 失敗(fundreport 新主機)");
					CheckServerHealth checkserverhealth = (CheckServerHealth) SpringBeanFactory
							.getBean("checkserverhealth");
					checkserverhealth.sendUnknowNotice("FTP 失敗(fundreport 新主機)");
					log.debug("FTPSTATUS失敗");
				} else {
					log.debug("FTPSTATUS成功");
				}
			}
		} catch (Exception e) {
		}
		try {
			fos.close();
		} catch (Exception e) {
		}

		if (okcnt == 0) {
			rowcnt.put("OKCNT", okcnt);
			rowcnt.put("FAILCNT", failcnt);
			rowcnt.put("TOTALCNT", totalcnt);
			return;
		}
		rowcnt.put("OKCNT", okcnt/2);
		rowcnt.put("FAILCNT", failcnt);
		rowcnt.put("TOTALCNT", totalcnt);

	}

	// 擷取名字長度
	public String subStringName(String name) {
		String space = "";
		name = name.replace((char) 12288, ' ');// 將全形空格unicode轉成半形空白再trim，因trim只能去掉的字元是unicode小於32
		log.debug(ESAPIUtil.vaildLog("replaceAll name==>" + name + "<=="));
		name = name.trim();// 先清空白
		log.debug(ESAPIUtil.vaildLog("trim name==>" + name + "<=="));
		if (!name.equals("")) {// 姓名不為空值
			if (name.length() >= 5) {// 姓名超過五位只抓五位
				name = name.substring(0, 5);
				int word = StrUtils.countWordSpace(name);
				for (int m = 0; m < 10 - word; m++)
					space += " ";
				name = name + space;
			} else if (name.length() < 5) {// 姓名未超過五位，抓姓名總長度
				name = name.substring(0, name.length());
				int word = StrUtils.countWordSpace(name);
				for (int m = 0; m < 10 - word; m++)
					space += " ";
				name = name + space;
			}
		} else {
			int word = StrUtils.countWordSpace(name);
			for (int m = 0; m < 10 - word; m++)
				space += " ";
			name = name + space;
		}

		// name = leftSpace(name,5,"STRING");
		log.debug(ESAPIUtil.vaildLog("after name==>" + name + "<=="));

		return name;
	}

	// 左靠右補空白
	public String leftSpace(String nameValue, int Strlen, String type) {// 參數1:值， 參數2:欄位總長度， 參數3:型態

		String space = "";
		// 補上不足的空白
		if (type.equals("STRING")) {// 字串型態
			for (int i = 0; i < Strlen - (nameValue.length()); i++) {// 上限長度-名字長度會剩多少空白
				space += (char) 12288;
			}
		} else { // 數字型態
			for (int i = 0; i < Strlen - (nameValue.length()); i++) {// 上限長度-名字長度會剩多少空白
				space += (char) ' ';
			}
		}
		return nameValue + space;
	}

	// public List<TXNUSER> seperateList(List<TXNUSER> allList,String adbranchid){
	// List<TXNUSER> returnList = new LinkedList<TXNUSER>();
	// for(TXNUSER each : allList) {
	// Map mapPO = CodeUtil.objectCovert(Map.class,each);
	// if(adbranchid.equals(mapPO.get("ADBRANCHID"))){
	// returnList.add(each);
	// }
	// }
	// return returnList;
	// }

	private MVHImpl getCombineRows(MVHImpl nb3users, MVHImpl nnbusers) {
		Rows result = new Rows();
		Rows resultold = new Rows();
		result = nb3users.getOccurs();
		resultold = nnbusers.getOccurs();
		List<String> nb3list = new ArrayList();
		for (int i = 0; i < result.getSize(); i++) {
			nb3list.add(result.getRow(i).getValue("DPSUERID"));
		}

		for (int i = 0; i < resultold.getSize(); i++) {
			if (!nb3list.contains(resultold.getRow(i).getValue("DPSUERID"))) {
				result.addRow(resultold.getRow(i));
			}

		}
		MVHImpl mvhresult = new MVHImpl(result);
		return mvhresult;
	}

	// 將list轉成MVH物件
	public MVHImpl seperateList(MVHImpl allList, String adbranchid) {
		MVHImpl mvhresult = null;
		Rows result = new Rows();
		Rows newrows = new Rows();
		result = allList.getOccurs();
		for (int i = 0; i < result.getSize(); i++) {
			// logger.debug("seperateList
			// ADBRANCHID==>"+result.getRow(i).getValue("ADBRANCHID").trim()+"<==");
			if (adbranchid.equals(result.getRow(i).getValue("ADBRANCHID").trim())) {
				newrows.addRow(result.getRow(i));
			}
		}
		mvhresult = new MVHImpl(newrows);
		return mvhresult;
	}

	// START ADD 20200324 將所有list轉成MVH物件，要將所有資料彙總起來，資料不區分分行
	public MVHImpl seperateAllList(MVHImpl allList) {
		MVHImpl mvhresult = null;
		Rows result = new Rows();
		Rows newrows = new Rows();
		result = allList.getOccurs();
		for (int i = 0; i < result.getSize(); i++) {
			newrows.addRow(result.getRow(i));
		}
		mvhresult = new MVHImpl(newrows);
		return mvhresult;
	}

}
