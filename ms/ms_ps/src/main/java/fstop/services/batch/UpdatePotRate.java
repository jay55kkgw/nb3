package fstop.services.batch;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringReader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.ESAPIUtil;

import fstop.orm.dao.PotRateDao;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.po.POTRATE;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 每日更新適用利率 - CK01信用卡長期使用循環信用轉換
 * 
 * @author Owner
 * 
 */
@Slf4j
@RestController
@Batch(id = "batch.updatePotRate", name = "每日更新適用利率", description = "中心下傳檔案，更新 db potrate 表")
public class UpdatePotRate implements BatchExecute {

	@Autowired
	private PotRateDao potRateDao;

	@Value("${batch.hostfile.path:}")
	private String pathName;

	@Autowired
	private SysBatchDao sysBatchDao;

	/**
	 * args[0] method name
	 */
	@RequestMapping(value = "/batch/updatePotRate", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	@Override
	public BatchResult execute(@RequestBody List<String> args) {

		log.info("batch UpdatePotRate...");

		BatchResult batchresult = new BatchResult();
		Map<String, Object> data = new LinkedHashMap();
		batchresult.setBatchName(getClass().getSimpleName());
		batchresult.setSuccess(true);

		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}

		List<String> safeArgs = ESAPIUtil.validStrList(args);
		// cache = new CacheService("distributeCache");
		Date d = new Date();
		String today = DateTimeUtils.getDateShort(d);
		String time = DateTimeUtils.getTimeShort(d);
		int failcnt = 0, okcnt = 0, totalcnt = 0;
		Hashtable rowcnt = new Hashtable();
		log.warn("DATE = " + today);
		log.warn("TIME = " + time);
		log.warn("Execute - PotRate Start");

		try {
			exec(rowcnt);
			failcnt = ((Integer) rowcnt.get("FAILCNT")).intValue();
			okcnt = ((Integer) rowcnt.get("OKCNT")).intValue();
			totalcnt = ((Integer) rowcnt.get("TOTALCNT")).intValue();
			log.warn("failcnt = " + failcnt);
			log.warn("okcnt = " + okcnt);
			log.warn("totalcnt = " + totalcnt);
		} catch (Exception e) {
			log.error("執行 Batch 有誤 !", e);
			data.put("Error", e.getMessage());
			batchresult.setSuccess(false);
		}

		BatchCounter bcnt = new BatchCounter();
		bcnt.setFailCount(failcnt);
		bcnt.setSuccessCount(okcnt);
		bcnt.setTotalCount(totalcnt);

		data.put("COUNTER", bcnt);
		batchresult.setData(data);
		
		
		log.info("Execute - PotRate End");

		try {
			if (batchresult.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName, batchresult.getData());
			}
		} catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}
		
		return batchresult;
	}

	private void exec(Hashtable rowcnt) throws IOException {
		Integer totalcnt = 0, failcnt = 0, okcnt = 0;

		String filePath = String.format("%1$s%2$s", pathName, "POTRATE.TXT");
		log.debug("filePath = " + filePath);

		File file = new File(filePath);
		file.setWritable(true, true);
		file.setReadable(true, true);
		file.setExecutable(true, true);
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		String str = "";
		// if(cache.getCache("POTRATE_List") != null)
		// cache.setCache("POTRATE_List", null);

		potRateDao.deleteAll();
		log.warn("potRateDao.deleteAll after");
		while ((str = br.readLine()) != null && str.length() != 0) {
			try {
				POTRATE t = new POTRATE();
				// String[] fields = str.split(" ");
				t.setPOT(str.substring(0, 3));
				t.setPOTRATE(str.substring(4).trim());
				potRateDao.save(t);
				/*
				 * //新增將利率查詢結果寫入 cache 機制 ============= Start List<POTRATE> POTRATE_List = new
				 * ArrayList<POTRATE>(); if(cache.getCache("POTRATE_List") != null)POTRATE_List
				 * = (List<POTRATE>)cache.getCache("POTRATE_List");
				 * 
				 * POTRATE_List.add(t); cache.setCache("POTRATE_List", POTRATE_List);
				 * if(cache.getCache("POTRATE_List") != null) { List<POTRATE> POTRATE_CatchList
				 * = (List<POTRATE>)cache.getCache("POTRATE_List"); String CurrentRecord =
				 * POTRATE_CatchList.get(POTRATE_CatchList.size() - 1).getPOT(); CurrentRecord
				 * += " / " + POTRATE_CatchList.get(POTRATE_CatchList.size() - 1).getRATE();
				 * log.
				 * debug("POTRATE Add To Catche Record ====================================> " +
				 * CurrentRecord);
				 * log.debug("POTRATE Catche Length ====================================> " +
				 * POTRATE_CatchList.size()); } //新增將利率查詢結果寫入 cache 機制 ============= End
				 */
				log.warn("potRateDao.save POT:" + str.substring(0, 2) + " POTRATE:" + str.substring(3).trim());
				okcnt++;
			} catch (Exception e) {
				log.warn("Error, data format error: " + str);
				failcnt++;
			}
		}
		totalcnt = okcnt + failcnt;
		try {
			br.close();
		} catch (Exception e) {
		}
		try {
			fr.close();
		} catch (Exception e) {
		}
		rowcnt.put("OKCNT", okcnt);
		rowcnt.put("FAILCNT", failcnt);
		rowcnt.put("TOTALCNT", totalcnt);
	}
}
