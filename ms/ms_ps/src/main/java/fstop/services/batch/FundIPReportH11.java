package fstop.services.batch;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.rest.model.JSPUtils;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.model.Rows;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.TxnFundDataDao;
import fstop.orm.dao.TxnLogDao;
import fstop.orm.oao.Old_TxnLogDao;
import fstop.orm.old.OLD_TXNLOG;
import fstop.orm.po.TXNFUNDDATA;
import fstop.orm.po.TXNLOG;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * (稽核處)每月產生上月透過行外電腦辦理基金申購(轉換、贖回)交易資料清單至 /TBB/nnb/hostdata/FundipReportforH11
 * @author Simon
 * 
 */

@Slf4j
@RestController
@Batch(id="batch.fundIPReportH11", name = "產生基金上月外部IP重覆2次以上記錄(稽核處)", description = "讀取同一ip不同id大於兩次的交易紀錄清單，產生檔案，給中心讀取")
public class FundIPReportH11  implements BatchExecute {


	@Autowired
	private TxnLogDao txnLogDao;
	@Autowired
	private TxnFundDataDao txnFundDataDao;
	
	@Autowired
	private Old_TxnLogDao old_TxnLogDao;
	
	@Value("${isParallelCheck}")
	private String isParallelCheck;

	@Value("${batch.hostfile.path:}")
	private String hostDir;
	
	@Autowired
	private SysBatchDao sysBatchDao;

	@RequestMapping(value = "/batch/fundIPReportH11", method = {RequestMethod.POST},
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
	public BatchResult execute(@RequestBody List<String>  args) {
		
		List<String> safeArgs = ESAPIUtil.validStrList(args);
		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}

		log.debug("行外電腦辦理基金申購(轉換、贖回)交易資料(稽核處)  ... ");
		int failcnt=0,okcnt=0,totalcnt=0;
		Hashtable rowcnt=new Hashtable();
		Hashtable<String, String> params = new Hashtable();
		params.put("TXNIDT", DateTimeUtils.format("yyMMddHHmmss", new Date()));
		String str_StartDate = "";
		String str_EndDate = "";
		String DATEDATA = "";
		
		BatchResult result = new BatchResult();
		String ERRORMSG = "";
		try {
//			exec(rowcnt);
//			failcnt=((Integer)rowcnt.get("FAILCNT")).intValue();
//			okcnt=((Integer)rowcnt.get("OKCNT")).intValue();
//			totalcnt=((Integer)rowcnt.get("TOTALCNT")).intValue();
			if (safeArgs.size() > 0) {
				str_StartDate = safeArgs.get(0);
				str_EndDate = safeArgs.get(1);
				DATEDATA = str_StartDate + str_EndDate;
				exec(rowcnt, DATEDATA);
			} else {
				exec(rowcnt, DATEDATA);
			}
			failcnt = ((Integer) rowcnt.get("FAILCNT")).intValue();
			okcnt = ((Integer) rowcnt.get("OKCNT")).intValue();
			totalcnt = ((Integer) rowcnt.get("TOTALCNT")).intValue();
			log.debug("failcnt = " + failcnt);
			log.debug("okcnt = " + okcnt);
			log.debug("totalcnt = " + totalcnt);
			
			BatchCounter bcnt=new BatchCounter();
			bcnt.setFailCount(failcnt);
			bcnt.setSuccessCount(okcnt);
			bcnt.setTotalCount(totalcnt);
			
			Map<String, Object> data = new HashMap();

			data.put("COUNTER", bcnt);
			
			result.setSuccess(true);
			result.setBatchName(this.getClass().getSimpleName());
			result.setData(data);

			
		}
		catch(Exception e) {
			log.error("執行 Batch 有誤 !", e);
			result.setSuccess(false);
			ERRORMSG = e.getMessage();
			result.setErrorCode(ERRORMSG);
		}
		
		
		try {
			if (result.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL(batchName,ERRORMSG);
			}
		}catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}
		result.setBatchName(this.getClass().getSimpleName());
		return result;
	}

	private void exec(Hashtable rowcnt , String DATEDATA) throws IOException {
		Integer	totalcnt=0,failcnt=0,okcnt=0;
		//String recPattern = "%-3s%-7s%-6s%-12s%-10s%-11s%-2s%-4s%-24s%-3s%-15s%-15s\r\n";
		String strStartdate = "";
		String strEnddate = "";
		Rows all = null;
		Date d = new Date();
		//Date d2 = new Date();
		java.util.Calendar calendar = java.util.Calendar.getInstance();   
		calendar.setTime(d);
		//String nowdate= DateTimeUtils.getDateShort(d);		
		//String nowyear = String.valueOf(Integer.parseInt(nowdate.substring(0,4))-1911);
		//String nowmonth = nowdate.substring(4,6);
		if (DATEDATA.length() == 0) {
		//跑上個月資料
		calendar.add(calendar.MONTH,-1);			
		d = calendar.getTime();
		String lastmonth= DateTimeUtils.getDateShort(d);
		lastmonth = lastmonth.substring(0, 6)+"%";
		String year=lastmonth.substring(0,4);
		String mon=lastmonth.substring(4,6);
		String sEndDay = "";
		if (mon.equals("01") || mon.equals("03") || mon.equals("05") || mon.equals("07") || mon.equals("08") || mon.equals("10") || mon.equals("12")) {
			sEndDay = "31";
		}
		else if (mon.equals("04") || mon.equals("06") || mon.equals("09") || mon.equals("11")) {
			sEndDay = "30";
		}
		else {
			if (mon.equals("02") && (Integer.parseInt(year) % 4 == 0) && (Integer.parseInt(year) % 100 != 0)) {
				sEndDay = "29";
			}
			else if(mon.equals("02")){
				sEndDay = "28";
			}
		}
			
		 strStartdate = year + mon + "01";
		 strEnddate = year + mon + sEndDay;
		log.debug(ESAPIUtil.vaildLog("findByFundIPReportH11：Sdate：" + strStartdate +"；Edate："+ strEnddate));
		
		
		 all = txnLogDao.findByFundIPReportforH11(strStartdate, strEnddate);
		}else {
			strStartdate = DATEDATA.substring(0, 8);
			strEnddate = DATEDATA.substring(8);
			log.warn(ESAPIUtil.vaildLog("Sdate：" + strStartdate + "；Edate：" + strEnddate));
			all = txnLogDao.findByFundIPReportforH11(strStartdate, strEnddate);
		}
		
		if("Y".equals(isParallelCheck)) {
			//2.5產出的ROW 有額外新增欄位 "SRC":"2.5" 用來判斷這筆row要去哪裡抓資料
			Rows oldall = old_TxnLogDao.findByFundIPReportforH11(strStartdate, strEnddate);
			
			log.debug("NB 3 data size >> {}" , all.getSize());
			log.debug("NB 2.5 data size >> {}" , oldall.getSize());
			
			all.addRows(oldall);
		}
			
		TXNLOG ft;
		File tmpFile = new File(hostDir+"FundipReportforH11.txt");
		tmpFile.setWritable(true,true);
		tmpFile.setReadable(true, true);
		tmpFile.setExecutable(true,true);
		tmpFile.delete();
		OutputStream fos = new FileOutputStream(tmpFile);
		BufferedWriter out = new BufferedWriter(new OutputStreamWriter(fos, "BIG5"));
			
		for (int j=0; j < all.getSize(); j++) {
			
			log.debug(ESAPIUtil.vaildLog("findByFundIPDataReportH11：Sdate："+strStartdate +"；Edate："+ strEnddate));
			List<TXNLOG> all2 = new ArrayList<TXNLOG>();
			
			if("Y".equals(isParallelCheck)) {
//				List<TXNLOG> all2 = txnLogDao.findByFundIPDataReport(all.getRow(j).getValue("aduserid"),all.getRow(j).getValue("aduserip"),strStartdate, strEnddate);
//				List<OLD_TXNLOG> oldall2 = old_TxnLogDao.findByFundIPDataReport(all.getRow(j).getValue("aduserid"),all.getRow(j).getValue("aduserip"),strStartdate, strEnddate);
//				
//				for (OLD_TXNLOG nnb : oldall2) {
//						Map nnbConvert2Map = CodeUtil.objectCovert(Map.class, nnb);
//						TXNLOG nnbConvert2nb3 = CodeUtil.objectCovert(TXNLOG.class, nnbConvert2Map);
//						all2.add(nnbConvert2nb3);
//				}
				if(null!=all.getRow(j).getValue("SRC") && "2.5".equals(all.getRow(j).getValue("SRC"))) {
					List<OLD_TXNLOG> oldall2 = old_TxnLogDao.findByFundIPDataReport(all.getRow(j).getValue("aduserid"),all.getRow(j).getValue("aduserip"),strStartdate, strEnddate);
					for(OLD_TXNLOG old_data:oldall2) {
						TXNLOG nnbConvert2nb3 = CodeUtil.objectCovert(TXNLOG.class, old_data);
						all2.add(nnbConvert2nb3);
					}
				}
				
			}else {
				all2 = txnLogDao.findByFundIPDataReport(all.getRow(j).getValue("aduserid"),all.getRow(j).getValue("aduserip"),strStartdate, strEnddate);
			}
			String strMark = all.getRow(j).getValue("mark");
			for (int k=0; k < all2.size(); k++) {
				ft= all2.get(k);
				log.debug(ESAPIUtil.vaildLog("findByUserIdH11：USERID：" + ft.getADUSERID()));
				
				String space="";
				String userip=ft.getADUSERIP();
				for (int m=0; m < 15-(userip.trim().length())*1; m++)
					space+=" ";
				userip=userip+space;
				space="";
					
				String userid=ft.getADUSERID();
				for (int m=0; m < 10-(userid.trim().length())*1; m++)
					space+=" ";
				userid=userid+space;
				space="";
				log.debug(ESAPIUtil.vaildLog("userid：" + userid));
					
				Map map = JSONUtils.json2map(ft.getADCONTENT());
					
				String str_transcode = (String)map.get("TRANSCODE");
				log.debug("TRANSCODE：" + str_transcode);					
				String adopid=ft.getADOPID();
				log.debug("adopid：" + adopid);
				String typename;
				String str_crdno;
				String str_findLname;
				if(str_transcode != null && !str_transcode.equals("")){				
					
					List<TXNFUNDDATA> fdata = txnFundDataDao.findByFUNDLNAME(str_transcode);
					TXNFUNDDATA fd = fdata.get(0);
					
					str_findLname = JSPUtils.convertFullorHalf(fd.getFUNDLNAME().trim().replace(" ",""), 1);
					if(str_findLname.trim().length() > 24){
						str_findLname=str_findLname.trim().substring(0, 24);
					}
					log.debug("FUNDLNAME：" + str_findLname);
					for (int m=0; m < 48-(str_findLname.trim().length())*2; m++)
						space+=" ";
					str_findLname=str_findLname+space;
					space="";
				}
				else{
					str_findLname = "                                                ";
				}					
					
				if(adopid.equals("C016") || adopid.equals("C017")){
					typename = "購買";
					str_crdno = "           ";
				}
				else if(adopid.equals("C021")){
					typename = "轉換";
					str_crdno = (String)map.get("CREDITNO");
					if(StrUtils.isEmpty(str_crdno)||"null".equals(str_crdno)) {
						str_crdno = "           ";
					}
				}
				else if(adopid.equals("C024")){
					typename = "贖回";
					str_crdno = (String)map.get("CREDITNO");
					if(StrUtils.isEmpty(str_crdno)||"null".equals(str_crdno)) {
						str_crdno = "           ";
					}
				}
				else{
					typename = "    ";
					str_crdno = "           ";
				}
				log.debug("CRDNO：" + str_crdno);
				space="";
					
				//String txamt=ft.getADTXAMT().trim().replace(".","");
				String txamt=ft.getADTXAMT().trim();
				if(txamt.indexOf(".")>-1)
					txamt=txamt.substring(0,txamt.indexOf("."));	
				for (int m=0; m < 15-(txamt.trim().length())*1; m++)
					space+="0";
				txamt=space+txamt;
				space="";
					
				String adcurrency="";					
				if(ft.getADCURRENCY().length() > 0){
					adcurrency = ft.getADCURRENCY();
				}
				else{
					adcurrency="   ";
				}
				log.debug("ADCURRENCY：" + adcurrency);
				log.debug("MARK：" + strMark);
					
				String newlastdate=String.valueOf(Integer.parseInt(ft.getLASTDATE().substring(0,4))-1911)+ft.getLASTDATE().substring(4,8);
				String txtime=ft.getLASTTIME();
				//String f = String.format(recPattern, str_BRANCHID, newlastdate, txtime, username, userid, str_crdno, typename, str_transcode, str_findLname, adcurrency, txamt, userip);
				String f = newlastdate+txtime+userid+str_crdno+typename+str_transcode+str_findLname+adcurrency+txamt+userip+strMark+"\r\n";
				log.debug(ESAPIUtil.vaildLog("H11Export File Content = " + f));
				if((userip.indexOf("125.227.162.115") != -1) || (userip.indexOf("210.63.206.136") != -1)){
					log.debug("H11Export File Content = 行內對外IP不計");
				}
				else{
					if(str_transcode != null && !str_transcode.equals("")){
						out.write(f);
					}
					else{
						log.debug("H11Export File Content = 有null值略過");
					}
				}
				okcnt++;
				totalcnt++;
			}
		}			
		try {
			out.flush();
			out.close();
		}
		catch(Exception e){}
		try {
			fos.close();
		}
		catch(Exception e){}

		if(okcnt==0)
		{
			rowcnt.put("OKCNT",okcnt);
			rowcnt.put("FAILCNT",failcnt);
			rowcnt.put("TOTALCNT",totalcnt);
			return;
		}
		rowcnt.put("OKCNT",okcnt);
		rowcnt.put("FAILCNT",failcnt);
		rowcnt.put("TOTALCNT",totalcnt);
	}
}
