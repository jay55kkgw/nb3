package fstop.services.batch;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.orm.dao.AdmHolidayDao;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.TxnBondDataDao;
import fstop.orm.po.ADMHOLIDAY;
import fstop.orm.po.TXNBONDDATA;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 債券資料 ( 讀取主機落地檔後，更新DB )
 *
 * @author Owner
 */
@Slf4j
@RestController
@Batch(id = "batch.b024", name = "債券資料", description = "債券資料 ( 讀取主機落地檔後，更新DB )")
public class B024 implements BatchExecute {

	@Value("${batch.hostfile.path:}")
	private String pathname;

	@Value("${host.hostdata.path}")
	private String hostpath;

	@Autowired
	private TxnBondDataDao txnBondDataDao;

	@Autowired
	private AdmHolidayDao admholidayDao;
	
	@Autowired
	private SysBatchDao sysBatchDao;

	@RequestMapping(value = "/batch/b024", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {
		
		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}
		
		List<String> safeArgs = ESAPIUtil.validStrList(args);
		Date d = new Date();
		String today = DateTimeUtils.getCDateShort(d);

		BatchResult batchresult = new BatchResult();
		batchresult.setBatchName(getClass().getSimpleName());
		Map<String, Object> data = new HashMap();

		log.debug("Batch_Execute_Date = " + today);

		ADMHOLIDAY holiday = null;
		try {
			String holidaytoday = DateTimeUtils.getDateShort(d);
			holiday = (ADMHOLIDAY) admholidayDao.findById(holidaytoday);
			log.debug("Holiday = " + holiday);
		} catch (Exception e) {
			holiday = null;
		}

		if (holiday != null) {
			data.put("TOPMSG", "非營業日不執行");
			log.error("非營業日不執行");
			batchresult.setSuccess(true);
			batchresult.setData(data);
		} else {
			String filePath = pathname + "B024F.TXT";
			// For Error log
			String hostfilePath = hostpath + "B024F.TXT";
			log.debug("filePath >>{}", filePath);
			batchresult = readFile(filePath, hostfilePath);
		}
		
		try {
			if (batchresult.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName,batchresult.getData());
			}
		}catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}

		return batchresult;
	}

	private BatchResult readFile(String tempfile, String hostpath) {
		
		BatchResult batchresult = new BatchResult();
		Map<String, Object> data = new LinkedHashMap();
		BatchCounter bc = new BatchCounter();
		

		InputStreamReader in = null;
		BufferedReader bin = null;
		String subject;

		try {
			in = new InputStreamReader(new FileInputStream(tempfile), "BIG5");
			bin = new BufferedReader(in);
			int save_okcnt = 0;
			int update_okcnt = 0;
			int save_failcnt = 0;
			int update_failcnt = 0;
			int samedata_cnt = 0;
			int totaldata_cnt =0;
			

			if (!bin.ready()) {
				in.close();
                bin.close();
            	throw new Exception();
			}

//			txnBondDataDao.deleteAll();
			List<String> dataList_all = new ArrayList<String>();
			//搜全部
			List<TXNBONDDATA> poList_all = txnBondDataDao.findAll();
			int pos = 0;
			//轉成 PK,pos Map
			Map<String,Integer> pkposMap = new HashMap<String,Integer>();
			for(TXNBONDDATA each:poList_all) {
				pkposMap.put(each.getBONDCODE(),pos);
				pos++;
			}
			while ((subject = bin.readLine()) != null && subject.length() != 0) {
				//去空格拆資料
				String[] bondData = subject.replaceAll(" ","").replaceAll("　","").split("#\\|");
				
				//把B024F.TXT內所有BONDCODE 存起來 , 刪除的時候會用到
				dataList_all.add(bondData[0]);
				
				//第一個位置為 BONDCODE , 如果有在pkposMap裡 > 走更新 , 如果沒有在pkposMap裡 >走新增
				//有在POLIST 裡 , 比對資料一不一樣 , 一樣不做事 , 不一樣update
				if(pkposMap.containsKey(bondData[0])) {
					TXNBONDDATA poData = poList_all.get(pkposMap.get(bondData[0]));
					try {
						int respCode = compareData(poData , bondData);
						
						if(0==respCode) {
							update_okcnt++;
						} else {
							samedata_cnt++;
						}
						
					}catch (Exception e) {
						update_failcnt++;
						log.error("compareData() error , BONDCODE = {} ", bondData[0]);
					}
				}
				//沒有在POLIST 裡 , 就SAVE
				else {
					try {
						saveNewData(bondData);
						save_okcnt++;
					}catch (Exception e) {
						save_failcnt++;
						log.error("saveNewData() error , BONDCODE = {} ", bondData[0]);
					}
				}
				totaldata_cnt++;
			}

			Long l = null;
			l = txnBondDataDao.count();
			
			//如果檔案資料數小於資料庫總數 , 代表有資料要刪除
			Map deleteReult = new HashMap();
			if(totaldata_cnt<l) {
				deleteReult =deleteData(dataList_all , pkposMap);
			}else {
				deleteReult.put("ok","0");
				deleteReult.put("fail","0");
			}

			log.debug("B024.java save_okcnt:" + save_okcnt );
			log.debug("B024.java save_failcnt:" + save_failcnt );
			log.debug("B024.java update_okcnt:" + update_okcnt );
			log.debug("B024.java update_failcnt:" + update_failcnt );
			log.debug("B024.java del_okcnt:" + deleteReult.get("ok"));
			log.debug("B024.java del_failcnt:" + deleteReult.get("fail"));
			log.debug("B024.java samedata_cnt:" + samedata_cnt );
			log.debug("B024.java totaldata_cnt:" + totaldata_cnt );
			log.debug("TXNBONDDATA cnt :" + l );
			
			data.put("save_okcnt", save_okcnt);
			data.put("save_failcnt", save_failcnt);
			data.put("update_okcnt", update_okcnt);
			data.put("update_failcnt", update_failcnt);
			data.put("del_okcnt", deleteReult.get("ok")!=null?deleteReult.get("ok"):"0");
			data.put("del_failcnt", deleteReult.get("fail")!=null?deleteReult.get("fail"):"0");
			data.put("samedata_cnt", samedata_cnt);
			data.put("totaldata_cnt", totaldata_cnt);
			
			batchresult.setSuccess(true);
			batchresult.setBatchName(this.getClass().getSimpleName());
			batchresult.setData(data);
			
			
		} catch (FileNotFoundException e) {
			data.put("Error", "找不到檔案(" + hostpath + ")");
			batchresult.setData(data);
			batchresult.setSuccess(false);
		} catch (IOException e) {
			data.put("Error", "讀取檔案錯誤(" + hostpath + ")");
			batchresult.setData(data);
			batchresult.setSuccess(false);
		} catch (Exception e) {
			data.put("Error", "檔案為空(" + hostpath + ")");
			batchresult.setData(data);
			batchresult.setSuccess(false);
		} finally {
			try {
				in.close();
				bin.close();
			} catch (Exception e) {
			}
			in = null;
			bin = null;

			return batchresult;
		}
	}
	
	/**
	 * 
	 * @param podata
	 * @param bondData
	 * @return  0 = 新增 , 1=不變
	 */
	public int compareData(TXNBONDDATA poData , String[] bondData) {
		log.info("IN B024.java compareData()");
		//v1 版本
//		Map<String,String> poMap = CodeUtil.objectCovert(Map.class, poData);
//		boolean update = false;
//		int pos = 0;
//		for(String key:poMap.keySet()) {
//			if(poMap.get(key).equals(bondData[0])) {
//				pos++;
//				continue;
//			}else {
//				log.trace("Data diff");
//				log.trace("OLD po key = {} , data = {} " , key , poMap.get(key));
//				log.trace("compare data = {}  " , bondData[pos]);
//				update = true;
//				break;
//			}
//		}
//		if(update) {
//			saveNewData(bondData);
//		}else {
//			log.trace("Same Data Do Nothing");
//			log.trace("OLD PO >> {}" , poMap.toString());
//			log.trace("NEW DATA >> {}" , Arrays.toString(bondData));
//		}
		// v1 版本結束
		
		// v2 版本
		Map<String,String> poMap = CodeUtil.objectCovert(Map.class, poData);
		String[] old_data_array = new String[poMap.keySet().size()];
		int pos_v2 = 0;
		for(String key:poMap.keySet()) {
			old_data_array[pos_v2] = poMap.get(key);
			pos_v2++;
		}
		if(!Arrays.equals(bondData,old_data_array)) {
			updateData(bondData);
			log.trace("Update DATA");
			log.trace("OLD PO >> {}" , poMap.toString());
			log.trace("NEW DATA >> {}" , Arrays.toString(bondData));
			return 0;
		}else {
			log.trace("Same Data Do Nothing");
			log.trace("OLD PO >> {}" , poMap.toString());
			log.trace("NEW DATA >> {}" , Arrays.toString(bondData));
			return 1;
		}
		
		// v2 版本結束
	}
	
	/**
	 * Save TXNBONDDATA
	 * @param bondData
	 * 
	 */
	public void saveNewData(String[] bondData) {
		log.info("IN B024.java saveNewData()");
		TXNBONDDATA po = new TXNBONDDATA();
		po.setBONDCODE(bondData[0]);
		po.setBONDNAME(bondData[1]);
		po.setBONDCRY(bondData[2]);
		po.setDBUONLY(bondData[3]);
		po.setOBUONLY(bondData[4]);
		po.setRISK(bondData[5]);
		po.setMINBPRICE(bondData[6]);
		po.setPROGRESSIVEBPRICE(bondData[7]);
		po.setMINSPRICE(bondData[8]);
		po.setPROGRESSIVESPRICE(bondData[9]);
		txnBondDataDao.save(po);
	}
	
	/**
	 * Save TXNBONDDATA
	 * @param bondData
	 * 
	 */
	public void updateData(String[] bondData) {
		log.info("IN B024.java updateData()");
		TXNBONDDATA po = new TXNBONDDATA();
		po.setBONDCODE(bondData[0]);
		po.setBONDNAME(bondData[1]);
		po.setBONDCRY(bondData[2]);
		po.setDBUONLY(bondData[3]);
		po.setOBUONLY(bondData[4]);
		po.setRISK(bondData[5]);
		po.setMINBPRICE(bondData[6]);
		po.setPROGRESSIVEBPRICE(bondData[7]);
		po.setMINSPRICE(bondData[8]);
		po.setPROGRESSIVESPRICE(bondData[9]);
		txnBondDataDao.update(po);
	}
	
	
	/**
	 * Delete TXNBONDDATA
	 * @param newList
	 * @param pkposMap
	 */
	public Map deleteData(List<String> newList , Map<String, Integer> pkposMap) {
		log.info("IN B024.java deleteData()");
		Map rtnMap = new HashMap();
		List<String> diff = getDiff(newList,  new ArrayList<String>(pkposMap.keySet()));
		int okcnt = 0 ;
		int failcnt = 0;
		for(String pk:diff) {
			TXNBONDDATA po = new TXNBONDDATA();
			po.setBONDCODE(pk);
			try {
				txnBondDataDao.delete(po);
				okcnt++;
			}catch (Exception e) {
				log.error("delete failed >> {}" ,e.getMessage());
				failcnt++;
			}
		}
		rtnMap.put("ok", okcnt);
		rtnMap.put("fail", failcnt);
		return rtnMap;
	}
	
	/**
     * 找出兩個LIST 中不同的值
     * @param listA
     * @param listB
     * @return
     */
    public static List<String> getDiff(List<String> listA,List<String> listB){
        List<String> diff = new ArrayList<String>();
        List<String> maxList = listA;
        List<String> minList = listB;
        if (listB.size() > listA.size()) {
            maxList = listB;
            minList = listA;
        }
        Map<String, Integer> map = new HashMap<String, Integer>(maxList.size());
        for (String string : maxList) {
            map.put(string, 1);
        }
        for (String string : minList) {
            if (map.get(string) != null) {
                map.put(string, 2);
                continue;
            }
            diff.add(string);
        }
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            if (entry.getValue() == 1) {
                diff.add(entry.getKey());
            }
        }
        return diff;
    }
	
}
