package fstop.services.batch;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class BatchResult {
	public String batchName;
	
	public boolean isSuccess;

	public String errorCode;

	public Map<String, Object> data;
	
	public List<List<Map<String,String>>> listdata;

}
