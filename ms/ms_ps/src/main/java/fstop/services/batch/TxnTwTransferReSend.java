package fstop.services.batch;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.orm.dao.AdmMsgCodeDao;
import fstop.orm.dao.DbConnectDao;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.TxnTwSchPayDataDao;
import fstop.orm.po.TXNTWSCHPAYDATA;
import fstop.orm.po.TxnTwSchPayDataErrorCount;
import fstop.services.batch.annotation.Batch;
import fstop.util.CSVWriter;
import fstop.util.UrlSetting;
import lombok.extern.slf4j.Slf4j;

/**
 * 台幣預約轉帳二扣
 *
 * @author Owner
 */
@Slf4j
@RestController
@Batch(id = "batch.TxnTwTransferReSend", name = "台幣預約交易二扣", description = "台幣預約交易二扣")
public class TxnTwTransferReSend extends DispatchBatchExecute implements BatchExecute {

	@Autowired
	private UrlSetting urlSetting;

	@Autowired
	TxnTwSchPayDataDao txnTwSchPayDataDao; // 預約資料

	@Autowired
	AdmMsgCodeDao admMsgCodeDao;

	@Autowired
	DbConnectDao dbConnectDao;

	@Autowired
	private SysBatchDao sysBatchDao;

	/**
	 * args.get(0) method name
	 */

	/**
	 * private String transRecode[] =
	 * {"3303","E024","E033","E062","E063","E064","Z011","Z008","Z007","Z033","Z031","Z030","Z001","Z002",
	 * 20200110將清單註解 保留 E033,E062,E063,E064,E038
	 * "Z003","Z012","Z016","Z019","Z020","Z024","Z025","Z026","Z029","Z022","Z032","Z034","ZX99","Z010","E038"};
	 */
	private String transRecode[] = { "E033", "E062", "E063", "E064", "E038", "E185" };

	@RequestMapping(value = "/batch/txntwtransferresend", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	@Override
	public BatchResult execute(@RequestBody List<String> args) {
		List<String> safeArgs = ESAPIUtil.validStrList(args);

		// log.info(ESAPIUtil.vaildLog("safeArgs: {}"+ JSONUtils.toJson(safeArgs)));
		log.info(ESAPIUtil.vaildLog("TxnTwTransferReSend_Proxy"));
		// 第一個為這個 bean 的 method Name
		String methodName = safeArgs.get(0);
		List paramList = new ArrayList();
		paramList.addAll(safeArgs);
		paramList.remove(0);
		// log.info(ESAPIUtil.vaildLog("methodName:"+ methodName +" param list: " +
		// JSONUtils.toJson(paramList)));
		return invoke(methodName, paramList);
	}

	/**
	 * 取得今日所有台幣預約轉帳一扣"失敗"且"可重送(判斷依據:錯誤代碼)"交易資料
	 *
	 * @return
	 */
	public BatchResult getAllList(List<String> args) {

		BatchResult batchresult = new BatchResult();
		Map<String, Object> data = new LinkedHashMap();
		batchresult.setBatchName(getClass().getSimpleName());
		batchresult.setSuccess(true);

		// 新增SYSBATCH紀錄
		try {
			sysBatchDao.initSysBatchPo("TxnTwTransferReSend");
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}

		Boolean conStatus = false;
		try {
			conStatus = dbConnectDao.getDBStatus();
		} catch (Exception e1) {
			log.error("DB error");
			log.error("Retry 1 (wait 5 sec)");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e4) {
			}
			try {
				conStatus = dbConnectDao.getDBStatus();
			} catch (Exception e) {
				log.error("DB error");
				log.error("Retry 2 (wait 5 sec)");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e4) {
				}
				try {
					conStatus = dbConnectDao.getDBStatus();
				} catch (Exception e2) {
					log.error("DB error");
					log.error("Retry 3 (wait 5 sec)");
					try {
						Thread.sleep(5000);
					} catch (InterruptedException e4) {
					}
					try {
						conStatus = dbConnectDao.getDBStatus();
					} catch (Exception e3) {
						log.error("DB error");
						log.error("Stop Retry");
						batchresult.setSuccess(false);
						batchresult.setErrorCode("DB_ERROR");
						data.put("Error", "DB_ERROR");
						batchresult.setData(data);

						try {
							sysBatchDao.finish_FAIL2("TxnTwTransferReSend", batchresult.getData());
						} catch (Exception e5) {
							log.error("sysBatchDao.finish error >> {} ", e5.getMessage());
						}

						return batchresult;
					}
				}
			}
		}

		Date today = new Date();
		boolean isSuccess = false;
		// StringWriter sw = new StringWriter();
		final StringBuffer allIds = new StringBuffer();
		OutputStream os = new ByteArrayOutputStream();
		try {
			List<TXNTWSCHPAYDATA> execDatas = txnTwSchPayDataDao
					.SecondCallList(new DateTime(today).toString("yyyyMMdd"));

			// 20200210 移至一扣執行完判斷(能做二扣的為2 , 不能的上面sql就找不到了) 二扣只需讀列表
			// List<TXNTWSCHPAYDATA> execDatas = new ArrayList();
			// for(TXNTWSCHPAYDATA po : allDatas) {
			// if("Y".equals(admMsgCodeDao.findRESEND(po.getDPEXCODE().trim()))) {
			// log.trace(ESAPIUtil.vaildLog("ADTXNO:"+ po.getADTXNO()+" DPEXCODE:" +
			// po.getDPEXCODE() +" Resend O"));
			// execDatas.add(po);
			// continue;
			// }else {
			// log.trace(ESAPIUtil.vaildLog("ADTXNO:"+ po.getADTXNO()+" DPEXCODE:" +
			// po.getDPEXCODE() +" Resend X"));
			// po.setDPTXSTATUS("1");
			// txnTwSchPayDataDao.update(po);
			// }
			// }

			log.debug(ESAPIUtil.vaildLog("execDatas>>{}" + CodeUtil.toJson(execDatas)));

			CSVWriter csvWriter = null;
			OutputStreamWriter osw = null;
			try {
				osw = new OutputStreamWriter(os);

				csvWriter = new CSVWriter(osw);

				for (TXNTWSCHPAYDATA s : execDatas) {
					csvWriter.writeNext(new String[] { s.getPks().getDPSCHNO(), s.getPks().getDPSCHTXDATE(),
							s.getPks().getDPUSERID(), s.getMSADDR(), s.getADTXNO() });

					csvWriter.flush();
				}
				isSuccess = true;
			} catch (Exception e) {
				log.error("寫入 TW_DPSCHNOs 時錯誤.", e);
			} finally {
				try {
					csvWriter.close();
				} catch (Exception e) {
				}
				try {
					os.close();
					osw.close();
				} catch (Exception e) {
				}
			}
		} catch (Exception e) {
			log.error("執行 Batch 有誤 !", e);
		}

		data.put("COUNTER", new BatchCounter());
		data.put("ALL_ID", allIds.toString());
		data.put("TW_DPSCHNOs", os.toString()); // 原本 tw_dpschids

		batchresult.setSuccess(isSuccess);
		batchresult.setBatchName("TxnTwTransferResend_getAllList");
		batchresult.setData(data);

		return batchresult;
	}

	/**
	 * 取得今日所有台幣預約轉帳失敗產檔的交易資料
	 *
	 * @return
	 */
	public BatchResult getErrorFileList(List<String> args) {
		boolean hasData = false;
		BatchResult result = new BatchResult();
		Map<String, String> poMap = new HashMap();
		Map<String, String> msgMap = new HashMap();
		List<List<Map<String, String>>> resultList = new LinkedList();
		List<Map<String, String>> msgList = new ArrayList();
		List<Map<String, String>> dataList = new ArrayList();
		Date today = new Date();
		String todayFormat = new DateTime(today).toString("yyyyMMdd");

		List<TxnTwSchPayDataErrorCount> errorMSGDatas = txnTwSchPayDataDao.countErrorList2nd(todayFormat);
		if (errorMSGDatas.size() != 0) {
			for (TxnTwSchPayDataErrorCount each : errorMSGDatas) {
				msgMap = CodeUtil.objectCovert(Map.class, each);
				msgList.add(msgMap);
			}

			resultList.add(msgList);
		} else {
			resultList.add(new ArrayList<>());
		}

		List<TXNTWSCHPAYDATA> errorDatas = txnTwSchPayDataDao.findErrorList2nd(todayFormat);
		if (errorDatas.size() != 0) {
			for (TXNTWSCHPAYDATA po : errorDatas) {
				if (Arrays.asList(transRecode).contains(po.getDPEXCODE())) {
					poMap = CodeUtil.objectCovert(Map.class, po);
					Map pks = CodeUtil.objectCovert(Map.class, poMap.get("pks"));
					poMap.putAll(pks);
					poMap.remove("pks");
					poMap.remove("DPTITAINFO");
					poMap.remove("DPTOTAINFO");
					String DPEXCODEMSG = admMsgCodeDao.getErrorCodeMsgIn(poMap.get("DPEXCODE"));

					poMap.put("DPEXCODEMSG", DPEXCODEMSG);

					dataList.add(poMap);
				}
			}

			resultList.add(dataList);
		} else {
			resultList.add(new ArrayList<>());
		}

		result.setListdata(resultList);
		result.setSuccess(hasData);
		result.setBatchName("TxnTwTransferReSend_getErrorFileList");

		// hasData = false;
		// result.setListdata(resultList);
		// result.setSuccess(hasData);
		// result.setBatchName("TxnTwTransfer_getErrorFileList");

		int total = txnTwSchPayDataDao.countMethod2nd(1);
		int success = txnTwSchPayDataDao.countMethod2nd(2);
		int custom_error = txnTwSchPayDataDao.countMethod2nd(3);
		int system_error = txnTwSchPayDataDao.countMethod2nd(4);
		int total_failed = txnTwSchPayDataDao.countMethod2nd(5);

		Map<String, Object> resultMap = new HashMap();
		resultMap.put("total", String.valueOf(total));
		resultMap.put("success", String.valueOf(success));
		resultMap.put("custom_error", String.valueOf(custom_error));
		resultMap.put("system_error", String.valueOf(system_error));
		resultMap.put("total_fail", String.valueOf(total_failed));

		result.setData(resultMap);

		try {
			// 新增SYSBATCH紀錄
			sysBatchDao.finish_OK("TxnTwTransferReSend");
		} catch (Exception e) {
			log.error("sysBatchDao.finish_OK error >> {} ", e.getMessage());
		}

		return result;
	}

	/**
	 * 
	 * 
	 * @param args
	 * @return
	 */
	public BatchResult executeone(List<String> args) {

		String serviceUrl = getServiceUrl(args.get(4), "txntwtransferresend");
		log.info(ESAPIUtil.vaildLog("Proxy to " + serviceUrl + " , methodName:executeone"));
		List<String> params = new ArrayList<>();
		params.add("executeone");
		for (String arg : args) {
			params.add(arg);
		}
		RestTemplate restTemplate = new RestTemplate();
		BatchResult result = restTemplate.postForObject(serviceUrl, params, BatchResult.class);

		return result;

	}

	public String getServiceUrl(String ms, String apiName) {
		String api = ms + "/batch/" + apiName;
		if (api.startsWith("/") == false)
			api = "/" + api;
		String s = "";
		switch (ms) {
		case "ms_tw":
			s += urlSetting.getMstw_Url();
			break;
		case "ms_cc":
			s += urlSetting.getMscc_Url();
			break;
		case "ms_loan":
			s += urlSetting.getMsloan_Url();
			break;
		case "ms_pay":
			s += urlSetting.getMspay_Url();
			break;
		case "ms_fx":
			s += urlSetting.getMsfx_Url();
			break;
		}
		// log.info("service url: " + ( s + api));
		return s + api;
	}

}
