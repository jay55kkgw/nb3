package fstop.services.batch;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.orm.dao.Nb3SysOpDao;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.TxnLogDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.oao.Old_TxnLogDao;
import fstop.orm.oao.Old_TxnUserDao;
import fstop.orm.old.OLD_TXNLOG;
import fstop.orm.old.OLD_TXNUSER;
import fstop.orm.po.NB3SYSOP;
import fstop.orm.po.TXNLOG;
import fstop.orm.po.TXNUSER;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import fstop.util.FtpBase64;
import fstop.util.FtpPathProperties;
import fstop.util.JSONUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 每月產生上月行內基金交易明細資料清單至 /TBB/nnb/data/NBCCM901 並把檔案拋至報表系統
 * @author Owner
 * 每頁45筆
 */
@Slf4j
@RestController
@Batch(id="batch.fundReport_900", name = "每月產生上月行內基金交易明細資料", description = "每月產生上月行內基金交易明細資料清單至 /TBB/nb3/data/NBCCM901 並把檔案拋至報表系統")
public class FundReport_M901  implements BatchExecute {
	
	@Autowired
	private TxnLogDao txnLogDao;
	@Autowired
	private TxnUserDao txnUserDao;
	@Autowired
	private Nb3SysOpDao nb3SysOpDao;
	
	@Autowired
	private Old_TxnLogDao old_TxnLogDao;
	@Autowired
	private Old_TxnUserDao old_TxnUserDao;
	
	@Autowired
	private CheckServerHealth checkserverhealth;

	@Value("${batch.hostfile.path:}")
	private String host_dir;
	@Value("${batch.localfile.path:}")
	private String path;
	@Value("${reportn.ftp.ip}")
	private String reportn_ftp_ip;
	@Value("${prodf_path}")
	String profpath; 
	
	@Value("${isParallelCheck}")
	private String isParallelCheck;
	
	@Value("${doFTP}")
	private String doFTP;
	
	@Autowired
	private SysBatchDao sysBatchDao;
	
	@RequestMapping(value = "/batch/nnbReport_M901", method = {RequestMethod.POST},
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {
		List<String> safeArgs = ESAPIUtil.validStrList(args);
		log.debug("基金交易明細資料  ... ");
		int failcnt=0,okcnt=0,totalcnt=0;
		Hashtable rowcnt=new Hashtable();
		
		BatchResult result = new BatchResult();
		result.setBatchName(this.getClass().getSimpleName());
		Map<String, Object> data = new HashMap();
		String errormsg = "";
		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}

		try {
			exec(rowcnt,errormsg);
			failcnt=((Integer)rowcnt.get("FAILCNT")).intValue();
			okcnt=((Integer)rowcnt.get("OKCNT")).intValue();
			totalcnt=((Integer)rowcnt.get("TOTALCNT")).intValue();
			log.debug("failcnt = " + failcnt);
			log.debug("okcnt = " + okcnt);
			log.debug("totalcnt = " + totalcnt);
			
			if(!"".equals(errormsg)) {
				data.put("ERROR", errormsg);
				result.setSuccess(false);
				result.setData(data);
			}else {
				BatchCounter bcnt=new BatchCounter();
				bcnt.setFailCount(failcnt);
				bcnt.setSuccessCount(okcnt);
				bcnt.setTotalCount(totalcnt);
				data.put("COUNTER", bcnt);
				result.setSuccess(true);
				result.setData(data);
			}
			
		}
		catch(Exception e) {
			log.error("執行 Batch 有誤 !", e);
			data.put("ERROR", e.getMessage());
			result.setSuccess(false);
			result.setData(data);
		}
		
		try {
			if (result.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName,result.getData());
			}
		}catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}

		return result;
	}

	private void exec(Hashtable rowcnt , String errormsg) throws IOException {
		Integer	totalcnt=0,failcnt=0,okcnt=0;
		Date d = new Date();
		java.util.Calendar calendar = java.util.Calendar.getInstance();   
		calendar.setTime(d);
		String nowdate= DateTimeUtils.getDateShort(d);
		String printdate=String.valueOf(Integer.parseInt(nowdate.substring(0,4))-1911)+"/"+nowdate.substring(4,6)+"/"+nowdate.substring(6,8);
		calendar.add(calendar.MONTH,-1);
		d = calendar.getTime(); 
		String lastmonth= DateTimeUtils.getDateShort(d);
		lastmonth = lastmonth.substring(0, 6)+"%";
		String year=lastmonth.substring(0,4);
		String mon=lastmonth.substring(4,6);
		String sEndDay = "";
		if (mon.equals("01") || mon.equals("03") || mon.equals("05") || mon.equals("07") || mon.equals("08") || mon.equals("10") || mon.equals("12")) {
			sEndDay = "31";
		}
		else if (mon.equals("04") || mon.equals("06") || mon.equals("09") || mon.equals("11")) {
			sEndDay = "30";
		}
		else {
			if (mon.equals("02") && (Integer.parseInt(year) % 4 == 0) && (Integer.parseInt(year) % 100 != 0)) {
				sEndDay = "29";
			}
			else if(mon.equals("02")){
				sEndDay = "28";
			}
		}  
		String Cyear=String.valueOf(Integer.parseInt(year)-1911);
		log.debug("lastmonth = " + lastmonth+",mon="+mon+",year="+year);
		
		
		FileReader fileStream = new FileReader(profpath+"ALL_BRH_IP_List.txt");
		
		BufferedReader bufferedStream = new BufferedReader(fileStream);
		String data;
		int iADD = 0;
		int record = 0;
		do {
			data = bufferedStream.readLine();
			if (data == null) {
				break;
			}
			iADD++;
		}
		while (true);
		String[][] result = new String[iADD+1][2];
		
		
		FileReader fileStream1 = new FileReader(profpath+"ALL_BRH_IP_List.txt");
		
		BufferedReader bufferedStream1 = new BufferedReader(fileStream1);
		do {
			data = bufferedStream1.readLine();
			if (data == null) {
				break;
			}
			int token = -1;
			token = data.indexOf(",");
				if (token >= 0) {
					result[record][0] = data.substring(0, token);					
					data = data.substring(token + 1);
					result[record][1] = data.substring(0); 
				}
				else {
					break;
				}
			record++;
		}
		while (true);
		
		String despath= path + "NBCCM901";
		log.debug("despath = " + despath);

		File tmpFile = new File(despath);
		tmpFile.setWritable(true,true);
		tmpFile.setReadable(true, true);
		tmpFile.setExecutable(true,true);
		tmpFile.delete();
		OutputStream fos = new FileOutputStream(tmpFile);
		BufferedWriter out = new BufferedWriter(new OutputStreamWriter(fos, "BIG5"));
		String pageHeader1 = "1 報表代號 : CCM901                      透過理財人員電腦進行基金網路交易(申購、轉換、贖回)月報                          週    期 : 每  月                       \n";
		String pageHeader2 = "  資料日期 : " + Cyear + "/" + mon + "/01－" + Cyear + "/" + mon + "/" + sEndDay + "               印表日期 : " + printdate + "                             頁    次 :   ";
		String pageHeader3 = "";
		String pageFooter = "9            	          經　辦 :                    　覆　核 :                    　主　管 :\n";
		String header1 = "     電腦IP位址           配置電腦行員        交易日期及交易時間            戶名          身分證字號       交易類別          金額           檢核日期        備註\n";     
		String header2 = "  ----------------        -------------       ------------------   --------------------- ------------ ----------------    -----------       ---------       ----\n";       
		for (int i=0; i < record; i++) {    //以分行依序查詢 
			List<TXNLOG> all = txnLogDao.findByFundReportM901(lastmonth, result[i][0]);
			
			
			//平行驗證
			if("Y".equals(isParallelCheck)){
				List<OLD_TXNLOG> allold = old_TxnLogDao.findByFundReportM901(lastmonth, result[i][0]);
				
				log.debug(ESAPIUtil.vaildLog("NB 3 data size >> " + all.size()));
				log.debug(ESAPIUtil.vaildLog("NB 2.5 data size >> " + allold.size()));
				
				for(OLD_TXNLOG old:allold) {
					TXNLOG nnbConvert2nb3 = CodeUtil.objectCovert(TXNLOG.class, old);
					all.add(nnbConvert2nb3);
				}
			}
			
			TXNLOG ft;
			pageHeader3 = "  需求單位 : "	+ result[i][0] + "                                                                                                         製表單位 : 資訊部\n\n\n\n";				
			Double totalpage=Math.ceil(((float)all.size()/(float)45));
			String totalpage1=totalpage.toString().substring(0,totalpage.toString().indexOf("."));
//			log.debug("BRH:"+result[i][0]+",IP:"+result[i][1]+",totalpage:"+totalpage1+",allsize:"+all.size());
			if(totalpage==0)
			{
				out.write(pageHeader1);
				out.write(pageHeader2+"1/1\n");
				out.write(pageHeader3);
				out.write(header1);
				out.write(header2);
				out.write("                     無基金交易紀錄                                                                      ");
				for(int k=0;k<45;k++) {
				out.write("\n");	
				}
				out.write(pageFooter);
			}
			else {
				for (int l=0;l<totalpage;l++) {	
					out.write(pageHeader1);
					out.write(pageHeader2+(l+1)+"/"+totalpage1+"\n");
					out.write(pageHeader3);
					out.write(header1);
					out.write(header2);
					int counter=0;
					for (int j=l*45; j < all.size(); j++) {
						ft = all.get(j);
						List<TXNUSER> l_User = txnUserDao.findByUserId(ft.getADUSERID());
						//平行驗證
						if("Y".equals(isParallelCheck)){
							if(l_User.size()==0) {
								List<OLD_TXNUSER> old_l_User = old_TxnUserDao.findByUserId(ft.getADUSERID());
								for(OLD_TXNUSER user :old_l_User) {
									log.debug(ESAPIUtil.vaildLog("Find NB 2.5 User >> " +user.getDPSUERID()));
									TXNUSER nnbConvert2nb3 = CodeUtil.objectCovert(TXNUSER.class, user);
									l_User.add(nnbConvert2nb3);
								}
							}
						}
						
						
						TXNUSER u = l_User.get(0);
                        String space="";
						String userip=ft.getADUSERIP();
						for (int m=0; m < 18-(ft.getADUSERIP().trim().length())*1; m++)
							space+=" ";
						userip=space+userip;
						space="";
						String LASTDATE=ft.getLASTDATE();
						String LASTTIME=ft.getLASTTIME();
						String tempyear=String.valueOf(Integer.parseInt(LASTDATE.substring(0,4))-1911)+".";
						String tempmon=LASTDATE.substring(4,6)+".";
						String tempday=LASTDATE.substring(6,8)+" ";
						String temphour=LASTTIME.substring(0,2)+":";
						String tempminute=LASTTIME.substring(2,4);
						String FULLDATETIME=tempyear+tempmon+tempday+temphour+tempminute;
						for (int m=0; m < 25-(FULLDATETIME.trim().length())*1; m++)
							space+=" ";
						FULLDATETIME=space+FULLDATETIME;
						space="";
						String username=u.getDPUSERNAME();
						for (int m=0; m < 24-(username.trim().length())*2; m++)
							space+=" ";
						username=space+username;
						space="";						
						String userid=ft.getADUSERID();
						for (int m=0; m < 13-(userid.trim().length())*1; m++)
							space+=" ";
						userid=space+userid;
						space="";						
						Map map = JSONUtils.json2map(ft.getADCONTENT());
						String adopid=ft.getADOPID();            
						List<NB3SYSOP> l_sysop = nb3SysOpDao.findByADOPId(adopid);           
						NB3SYSOP sysop = l_sysop.get(0);            
						String typename=sysop.getADOPNAME();
						int word = StrUtils.countWordSpace(typename);
						for (int m=0; m < 17-word; m++)
							space+=" ";
						typename=space+typename;
						space="";												
						String txamt=ft.getADTXAMT();						
						String adcurrency=ft.getADCURRENCY()+" ";			
						String alltxamt=adcurrency+txamt;						
						for (int m=0; m < 15-(alltxamt.trim().length())*1; m++)
							space+=" ";
						alltxamt=space+alltxamt;
						space="";										
						for (int m=0; m < 21; m++)
							space+=" ";
						String f = userip+space+FULLDATETIME+username+userid+typename+alltxamt+"\n";
						log.debug(ESAPIUtil.vaildLog("Export File Content = " + f));
						out.write(f);
						okcnt++;
						totalcnt++;
						counter++;
						if((all.size() % 45)!=0 && (j==all.size()-1))
						{
							int fill_space=45-(all.size() % 45);
							for(int k=0;k<fill_space;k++) {
								out.write("\n");	
							}	
						}
						if(counter==45)
							break;					
					}
					out.write(pageFooter);
				}
			}
		}
		try {
			out.flush();
			out.close();
			
			if("Y".equals(doFTP)) {
				//FTP機制啟動拋至報表系統主機
				FtpPathProperties nrcpro=new FtpPathProperties();
				String srcpath=nrcpro.getSrcPath("fundreport_m901");
				log.debug(ESAPIUtil.vaildLog("srcpath = " + srcpath));
				//String despath1=nrcpro.getDesPath("fundreport_m901");
				//log.debug("despath = " + despath1);
				String despath2=nrcpro.getDesPath("fundreport1_m901");
				log.debug(ESAPIUtil.vaildLog("despath1 = " + despath2));
				//String ip = (String)setting.get("ftp.ip");
				String ipn = reportn_ftp_ip;
				//FtpBase64 ftpBase64 = new FtpBase64(ip);
				//int rc=ftpBase64.upload(despath1, Path.dataFolder + "/NBCCM901");
				/*
				if(rc!=0)
				{
					log.debug("FTP 失敗(fundreport 舊主機)");
					CheckServerHealth checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
					checkserverhealth.sendUnknowNotice("FTP 失敗(fundreport 舊主機)");
					log.debug("FTPSTATUS失敗");
				}
				else
				{
					log.debug("FTPSTATUS成功");
				}
				*/
				FtpBase64 ftpBase64 = new FtpBase64(ipn);
				int rc=ftpBase64.upload(despath2, path + "NBCCM901");
				if(rc!=0)
				{
					log.debug("FTP 失敗(fundreport 新主機)");
	//				CheckServerHealth checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
					checkserverhealth.sendUnknowNotice("FTP 失敗(fundreport 新主機)");
					log.debug("FTPSTATUS失敗");
					errormsg = "FTPSTATUS失敗";
					
				}
				else
				{
					log.debug("FTPSTATUS成功");
				}
			}
		}
		catch(Exception e){}
		try {
			fos.close();
		}
		catch(Exception e){}

		if(okcnt==0)
		{
			rowcnt.put("OKCNT",okcnt);
			rowcnt.put("FAILCNT",failcnt);
			rowcnt.put("TOTALCNT",totalcnt);
			return;
		}
		rowcnt.put("OKCNT",okcnt);
		rowcnt.put("FAILCNT",failcnt);
		rowcnt.put("TOTALCNT",totalcnt);
		
		fileStream.close();
		bufferedStream.close();
		fileStream1.close();
		bufferedStream1.close();
	}
}
