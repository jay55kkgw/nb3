package fstop.services.batch;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.orm.dao.AdmHotopDao;
import fstop.orm.dao.AdmLogInDao;
import fstop.orm.dao.Nb3SysOpDao;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.TxnLogDao;
import fstop.orm.po.ADMHOTOP;
import fstop.orm.po.ADMHOTOP_PK;
import fstop.orm.po.NB3SYSOP;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 每日更新使用者常用功能前五名
 * 
 * @author Owner
 *
 */
@Slf4j
@RestController
@Batch(id = "batch.UserHotop", name = "使用者最常使用功能前五名記錄", description = " 每日更新使用者常用功能前五名")
public class UserHotop implements EmailUploadIf, BatchExecute {

	private String ERRORMSG = "";
	@Autowired
	private AdmHotopDao admHotopDao;
	@Autowired
	private AdmLogInDao admLogInDao;
	@Autowired
	private TxnLogDao txnLogDao;
	@Autowired
	private SysBatchDao sysBatchDao;
	@Autowired
	private Nb3SysOpDao nb3SysOpDao;
	
	static Gson gs = new Gson();

	@RequestMapping(value = "/batch/userhotop", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {
		
		List<String> safeArgs = ESAPIUtil.validStrList(args);
		
		BatchResult batchresult = new BatchResult();
		Map<String, Object> data = new LinkedHashMap();
		batchresult.setSuccess(true);
		
		log.debug("INTO USERHOTOP BATCH ... ");

		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}
		
		try {
			List<String> loginUserList = new ArrayList<String>();
			//查詢昨天有誰做過登入
			loginUserList = admLogInDao.findYesterdayLogin();
			
			List<NB3SYSOP> menu = nb3SysOpDao.getUrlMenu();
			
			List<Map<String,String>> menuListMap = new ArrayList<Map<String,String>>();
			
			List<String> insertList = new ArrayList<String>();
			
			for(NB3SYSOP menuData:menu) {
				Map<String,String> adopidnUrlMap = new HashMap<String,String>();
				adopidnUrlMap.put("ADOPID", menuData.getADOPID());
				adopidnUrlMap.put("URL",menuData.getURL());
				menuListMap.add(adopidnUrlMap);
			}
			
			data.put("DoList", loginUserList.toString());
			
			for(String user:loginUserList) {
				//找出使用者使用次數最多的前十名功能
				List<Map<String,String>> top10data = txnLogDao.get3MTop10(user);
				
				//刪除此使用者資料
				log.info(ESAPIUtil.vaildLog("Delete USER DATA : " + user ));
				admHotopDao.delByAduserid(user);
				int i =0;
				for(Map<String,String> txnlogdata:top10data) {
					for(Map<String,String> eachMap:menuListMap) {
						if(txnlogdata.get("ADOPID").equals(eachMap.get("ADOPID"))) {
							if(checkLowerCaseUrl(eachMap.get("URL"))) {
								//重新新增這幾筆
//								ADMHOTOP po = new ADMHOTOP();
//								ADMHOTOP_PK popk = new ADMHOTOP_PK();
//								popk.setADOPID(eachMap.get("ADOPID"));
//								popk.setADUSERID(user);
//								po.setPks(popk);
//								po.setLASTDT(DateTimeUtils.format("yyyyMMdd", new Date())+"00000"+i);
//								admHotopDao.save(po);
								insertList.add(eachMap.get("ADOPID")+":"+txnlogdata.get("ADOPID_NUM")+" TIMES");
								admHotopDao.insertData(user, eachMap.get("ADOPID"), DateTimeUtils.format("yyyyMMdd", new Date())+"00000"+i);
								i++;
								break;
							}
						}
					}
					if(i==5) {break;};
				}
				log.info(ESAPIUtil.vaildLog("INSERT " + user +" DATA >> " + insertList.toString()));
				insertList.clear();
			}
			
		}catch (Exception e) {
			log.error("USERHOTOP Error >> {} ",e.getMessage());
			ERRORMSG += e.getMessage();
			batchresult.setSuccess(false);
			data.put("ERROR", e.getMessage());
		}

		try {
			if (batchresult.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL(batchName, ERRORMSG);
			}
		} catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}
		
		batchresult.setData(data);
		return batchresult;
	}
	
	public boolean checkLowerCaseUrl(String adopid) {
		boolean result = false;
		String lastUrl = adopid.substring(adopid.lastIndexOf("/")+1);
		String REGEX = "[^a-z]";//
		Pattern pat = Pattern.compile(REGEX); 
		Matcher mat = pat.matcher(lastUrl); 
		lastUrl = mat.replaceAll("");
		int i = 0;
		while(i < lastUrl.length()){
			char chr = lastUrl.charAt(i);
			if(Character.isLowerCase(chr)){
				result=true;
			}else {
				result=false;
			}
			i++;
		}
		return result;
	}
}
