package fstop.services.batch;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.ESAPIUtil;

import fstop.orm.dao.AdmHolidayDao;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.TxnFundValueDao;
import fstop.orm.po.ADMHOLIDAY;
import fstop.orm.po.TXNFUNDVALUE;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 基金淨值查詢
 * 
 * @author Owner
 *
 */
@Slf4j
@RestController
@Batch(id = "batch.c019", name = "基金基本資料同步", description = "基金基本資料同步(從電文產生落地檔後，在更新db)")
public class C019 implements BatchExecute {

	@Autowired
	private TxnFundValueDao txnFundValueDao;
	@Autowired
	private AdmHolidayDao admholidayDao;

	@Value("${batch.hostfile.path:}")
	private String pathname;
	
	@Value("${host.hostdata.path}")
	private String hostpath;
	
	@Autowired
	private SysBatchDao sysBatchDao;

	@RequestMapping(value = "/batch/c019", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {
		
		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}
		
		List<String> safeArgs = ESAPIUtil.validStrList(args);
		Date d = new Date();
		String today = DateTimeUtils.getCDateShort(d);
		
		BatchResult batchresult = new BatchResult();
		batchresult.setBatchName(getClass().getSimpleName());
		Map<String, Object> data = new HashMap();
		
		log.debug("Batch_Execute_Date = " + today);

		ADMHOLIDAY holiday = null;
		try {
			String holidaytoday = DateTimeUtils.getDateShort(d);
			holiday = (ADMHOLIDAY) admholidayDao.findById(holidaytoday);
			log.debug("Holiday = " + holiday);
		} catch (Exception e) {
			holiday = null;
		}

		if (holiday != null) {
			data.put("__TOPMSG", "ZXXX");
			data.put("__TOPMSGTEXT", "非營業日．");
			log.debug("__TOPMSGTEXT = " + "非營業日．");
			batchresult.setSuccess(false);
			batchresult.setData(data);
		}else {
			String filePath = pathname + "C019F.TXT";
			log.debug("filePath >>{}", filePath);
			
			//For Error log
			String hostfilePath = hostpath + "C019F.TXT";
			
			batchresult = readFile(filePath,hostfilePath);
		}
		
		try {
			if (batchresult.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName,batchresult.getData());
			}
		}catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}
		
		return batchresult;
	}

	private BatchResult readFile(String tempfile , String hostpath) {
		
		BatchResult batchresult = new BatchResult();
		Map<String, Object> data = new HashMap();
		BatchCounter bc = new BatchCounter();
		
		InputStreamReader in = null;
		BufferedReader bin = null;
		String subject;
		
		try {

			in = new InputStreamReader(new FileInputStream(tempfile), "BIG5");
			bin = new BufferedReader(in);
			int succcnt = 0;
			int failcnt = 0;
			
			if(!bin.ready()) {
				in.close();
                bin.close();
            	throw new Exception();
			}
			
			txnFundValueDao.deleteAll();
			while ((subject = bin.readLine()) != null && subject.length() != 0) {
				TXNFUNDVALUE tt = new TXNFUNDVALUE();
				String strtemp = subject;
				log.trace(ESAPIUtil.vaildLog("C019 LINE STR:" + subject));

				String TRANSCODEprt1 = strtemp.substring(0, strtemp.indexOf("#|")).trim();
                strtemp = strtemp.substring(strtemp.indexOf("#|") + 2);
                String TRANSCODEprt2 = strtemp.substring(0, strtemp.indexOf("#|")).trim();
                strtemp = strtemp.substring(strtemp.indexOf("#|") + 2);
                log.trace(ESAPIUtil.vaildLog("TRANSCODE:" + TRANSCODEprt1+TRANSCODEprt2 + " LENGTH:" + (TRANSCODEprt1+TRANSCODEprt2).length()));
                tt.setTRANSCODE(TRANSCODEprt1+TRANSCODEprt2);
                
				String REFUNDAMT = strtemp.substring(0, strtemp.indexOf("#|")).trim();
				strtemp = strtemp.substring(strtemp.indexOf("#|") + 2);
				log.trace(ESAPIUtil.vaildLog("REFUNDAMT:" + REFUNDAMT + " LENGTH:" + REFUNDAMT.length()));
				tt.setREFUNDAMT(REFUNDAMT);

				String TRADEDATE = strtemp.substring(0, strtemp.indexOf("#|")).trim();
				strtemp = strtemp.substring(strtemp.indexOf("#|") + 2);
				log.trace(ESAPIUtil.vaildLog("TRADEDATE:" + TRADEDATE + " LENGTH:" + TRADEDATE.length()));
				tt.setTRADEDATE(TRADEDATE);

				String FUNDREMARK = strtemp.trim();
				log.trace(ESAPIUtil.vaildLog("FUNDREMARK:" + FUNDREMARK + " LENGTH:" + FUNDREMARK.length()));
				tt.setFUNDREMARK(FUNDREMARK);

				try {
					log.info("saveData...");
					txnFundValueDao.save(tt);
					log.info("saveData...end");
					succcnt++;
				} catch (Exception e) {
					failcnt++;
				}
			}
			Long l = null;
			l = txnFundValueDao.count();

			bc.setSuccessCount(succcnt);
			bc.setFailCount(failcnt);
			bc.setTotalCount(Integer.parseInt(String.valueOf(l)));
			data.put("COUNTER", bc);
			
			batchresult.setSuccess(true);
			batchresult.setBatchName(this.getClass().getSimpleName());
			batchresult.setData(data);
			
			log.debug("C019.java okcnt:" + succcnt + " failcnt:" + failcnt);
		} catch (FileNotFoundException e) {
			data.put("Error", "找不到檔案(" + hostpath + ")");
			batchresult.setData(data);
			batchresult.setSuccess(false);
		} catch (IOException e) {
			data.put("Error", "讀取檔案錯誤(" + hostpath + ")");
			batchresult.setData(data);
			batchresult.setSuccess(false);
		} catch (Exception e) {
			data.put("Error", "檔案為空(" + hostpath + ")");
			batchresult.setData(data);
			batchresult.setSuccess(false);
		} finally {
			try {
				in.close();
				bin.close();
			} catch (Exception e) {
			}
			in = null;
			bin = null;
			
			return batchresult;
		}

	}

}
