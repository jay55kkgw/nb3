package fstop.services.batch;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.oao.Old_TxnUserDao;
import fstop.orm.old.OLD_TXNUSER;
import fstop.orm.po.TXNUSER;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import fstop.util.FtpBase64;
import fstop.util.FtpPathProperties;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 每日產生上傳 email 修改的清單至 /DecodeFolder/CreditCard
 * 
 * @author Owner
 *
 */
@Slf4j
@RestController
@Batch(id = "batch.emailUpload", name = "EmailUpload", description = "產生前一日電子帳單EMAIL異動資料")
public class EmailUpload implements EmailUploadIf, BatchExecute {
	// private log log = log.getlog(getClass());
	@Autowired
	private TxnUserDao txnUserDao;

	@Value("${bh1.ftp.ip:}")
	private String bh_ftp_ip1;

	@Autowired
	private Old_TxnUserDao old_TxnUserDao;

	@Value("${isParallelCheck}")
	private String isParallelCheck;

	@Value("${doFTP}")
	private String doFTP;

	@Autowired
	private CheckServerHealth checkserverhealth;
	
	@Autowired
	private SysBatchDao sysBatchDao;

	@RequestMapping(value = "/batch/emailUpload", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {
		
		log.debug("EmailUpload BATCH ... ");
		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}
		
		List<String> safeArgs = ESAPIUtil.validStrList(args);
		BatchResult result = new BatchResult();
		result.setBatchName(getClass().getSimpleName());
		BatchCounter bcnt = new BatchCounter();
		Map<String, Object> data = new LinkedHashMap();

		int failcnt = 0, okcnt = 0, totalcnt = 0, usercnt = 0;
		HashMap rowcnt = new HashMap();
	
		try {
			exec(rowcnt);
			failcnt = ((Integer) rowcnt.get("FAILCNT")).intValue();
			okcnt = ((Integer) rowcnt.get("OKCNT")).intValue();
			totalcnt = ((Integer) rowcnt.get("TOTALCNT")).intValue();

			log.debug("failcnt = " + failcnt);
			log.debug("okcnt = " + okcnt);
			log.debug("totalcnt = " + totalcnt);
		} catch (Exception e) {
			log.error("執行 Batch 有誤 !", e);
			rowcnt.put("Error", e.getMessage());
		}
		
		bcnt.setFailCount(failcnt);
		bcnt.setSuccessCount(okcnt);
		bcnt.setTotalCount(totalcnt);
		if (rowcnt.containsKey("Error")) {
			result.setSuccess(false);
			data.put("Error", rowcnt.get("Error"));
		} else {
			result.setSuccess(true);
			data.put("FTP_Status", rowcnt.get("FTP_Status"));
		}
		data.put("UserCNT", ((Integer) rowcnt.get("UserCNT")).intValue());
		data.put("COUNTER", bcnt);

		result.setData(data);
		
		try {
			if (result.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName,result.getData());
			}
		}catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}
		

		return result;
	}

	private void exec(HashMap rowcnt) throws IOException,Exception {
		Integer usercnt = 0, failcnt = 0, okcnt = 0;

		Calendar calendar = Calendar.getInstance();
		Date d = calendar.getTime();

		String today = DateTimeUtils.getCDateShort(d);
		today = today.substring(0, 7);
		String time = DateTimeUtils.getTimeShort(d);

		// tomorrow
		calendar.add(Calendar.DAY_OF_YEAR, 1);
		Date d3 = calendar.getTime();
		String tomorrow = DateTimeUtils.getCDateShort(d3);
		tomorrow = tomorrow.substring(0, 7);

		// yesterday
		calendar.add(Calendar.DAY_OF_YEAR, -2);
		Date d2 = calendar.getTime();
		String yesterday = DateTimeUtils.getCDateShort(d2);
		yesterday = yesterday.substring(0, 7);

		log.debug("today = " + today);
		log.debug("yesterday = " + yesterday);
		log.debug("tomorrow = " + tomorrow);

		FtpPathProperties nrcpro = new FtpPathProperties();
		String srcpath = nrcpro.getSrcPath("emailupload").trim();
		log.debug(ESAPIUtil.vaildLog("srcpath = " + srcpath));
		// //TODO Local host test
		// srcpath = "C:\\Users\\BenChien\\Desktop\\";
		String despath1 = nrcpro.getDesPath("emailupload1").trim();
		log.debug(ESAPIUtil.vaildLog("despath1 = " + despath1));
		// Absolute Path Traversal
		String absolutePath = srcpath + DateTimeUtils.getDateShort(d) + "_emailupload.txt";
		if (absolutePath.startsWith("/") == false) {
			absolutePath = "/" + absolutePath;
		}
		File tmpFile = new File(absolutePath);
		tmpFile.setWritable(true, true);
		tmpFile.setReadable(true, true);
		tmpFile.setExecutable(true, true);
		// FileWriter fos = new FileWriter(tmpFile);
		// BufferedWriter out = new BufferedWriter(fos);
		BufferedWriter out = new BufferedWriter(new FileWriter(tmpFile));
		List<TXNUSER> all = txnUserDao.getAllBillUserLast2Day(tomorrow, yesterday);

		if ("Y".equals(isParallelCheck)) {
			List<OLD_TXNUSER> oldall = old_TxnUserDao.getAllBillUserLast2Day(tomorrow, yesterday);
			
			log.debug("NB 3 data size >> {}" , all.size());
			log.debug("NB 2.5 data size >> {}" , oldall.size());
			
			all = getCombineList(all, oldall);

		}
		String recPattern = "%-11s%-3s%-60s%-1s%-7s%-6s%-1s%-6s%-3s%-2s\r\n";

		for (TXNUSER user : all) {
			usercnt++;
			log.debug(ESAPIUtil.vaildLog("USERID: " + user.getDPSUERID() + ", EMAILDATE: " + user.getEMAILDATE()));

			String p1 = ""; // 客戶統編
			String p2 = ""; // 業務類別
			String pEmail = ""; // 電子郵件地址
			String p4 = ""; // 異動代碼 A or D
			String pToday = today; // 異動日期
			String pTime = time; // 異動時間
			String p7 = "2"; // 異動來源
			String p8 = "NBUSER"; // 異動人員
			String bhcod = "";
			String p9 = ""; // 保留欄位
			String p10 = "";
			String p11 = "";

			p1 = user.getDPSUERID();
			pEmail = StrUtils.trim(user.getDPMYEMAIL());

			// boolean isAll = false;
			// isAll = (StrUtils.trim(user.getEMAILDATE()).startsWith(today) ||
			// StrUtils.trim(user.getEMAILDATE()).startsWith(yesterday));
			// log.debug("isAll = " + isAll);
			if (user.getADBRANCHID().length() == 0 || user.getADBRANCHID() == null
					|| user.getADBRANCHID().equals("   "))
				bhcod = "000";
			else
				bhcod = user.getADBRANCHID();

			if (StrUtils.trim(user.getBILLDATE()).startsWith(today)
					|| StrUtils.trim(user.getBILLDATE()).startsWith(yesterday)) {
				p2 = "NB";
				if ("Y".equalsIgnoreCase(user.getDPBILL()))
					p4 = "A";
				else
					p4 = "D";
				if ("A".equals(p4))
					pEmail = user.getDPMYEMAIL();
				pToday = StrUtils.left(user.getBILLDATE(), 7);
				log.debug("DPBILL pToday = " + pToday);
				pTime = StrUtils.right(user.getBILLDATE(), 6);
				log.debug("DPBILL pTime = " + pTime);
				String f = String.format(recPattern, p1, p2, pEmail, p4, pToday, pTime, p7, p8, bhcod, p9);
				out.write(f);
				okcnt++;
			}

			if (StrUtils.trim(user.getFUNDBILLDATE()).startsWith(today)
					|| StrUtils.trim(user.getFUNDBILLDATE()).startsWith(yesterday)) {
				p2 = "FD";
				if ("Y".equalsIgnoreCase(user.getDPFUNDBILL()))
					p4 = "A";
				else
					p4 = "D";
				if ("A".equals(p4))
					pEmail = user.getDPMYEMAIL();
				pToday = StrUtils.left(user.getFUNDBILLDATE(), 7);
				log.debug("FUNDBILL pToday = " + pToday);
				pTime = StrUtils.right(user.getFUNDBILLDATE(), 6);
				log.debug("FUNDBILL pTime = " + pTime);
				String f = String.format(recPattern, p1, p2, pEmail, p4, pToday, pTime, p7, p8, bhcod, p9);
				out.write(f);
				okcnt++;
			}

			if (StrUtils.trim(user.getCARDBILLDATE()).startsWith(today)
					|| StrUtils.trim(user.getCARDBILLDATE()).startsWith(yesterday)) {
				p2 = "CC";
				if ("Y".equalsIgnoreCase(user.getDPCARDBILL()))
					p4 = "A";
				else
					p4 = "D";
				if ("A".equals(p4))
					pEmail = user.getDPMYEMAIL();
				pToday = StrUtils.left(user.getCARDBILLDATE(), 7);
				log.debug("CARDBILL pToday = " + pToday);
				pTime = StrUtils.right(user.getCARDBILLDATE(), 6);
				log.debug("CARDBILL pTime = " + pTime);
				String f = String.format(recPattern, p1, p2, pEmail, p4, pToday, pTime, p7, p8, bhcod, p9);
				out.write(f);
				okcnt++;
			}

		} // end for TXNUSER

		try {
			out.close();
		} catch (Exception e) {
		}

		if ("Y".equals(doFTP)) {
			String ip1 = bh_ftp_ip1;

			// //TODO:本地ftp測試
			// ip1="192.168.50.191";

			FtpBase64 ftpBase641 = new FtpBase64(ip1);

			log.debug(ESAPIUtil.vaildLog("tmpFile = " + tmpFile.getPath()));
			int rc1 = ftpBase641.upload(despath1, new FileInputStream(tmpFile));
			// 目地 來源
			if (rc1 != 0) {
				log.debug(DateTimeUtils.getDateShort(d) + "_emailupload.txt FTP 失敗(new BillHunter 主機)");
				checkserverhealth
						.sendUnknowNotice(DateTimeUtils.getDateShort(d) + "_emailupload.txt FTP 失敗(new BillHunter 主機)");
				rowcnt.put("Error", "FTP Fail");
			}

			rowcnt.put("OKCNT", okcnt);
			rowcnt.put("FAILCNT", failcnt);
			rowcnt.put("TOTALCNT", okcnt);
			rowcnt.put("UserCNT", usercnt);
			rowcnt.put("FTP_Status", "Success");
		} else {
			rowcnt.put("OKCNT", okcnt);
			rowcnt.put("FAILCNT", failcnt);
			rowcnt.put("TOTALCNT", okcnt);
			rowcnt.put("UserCNT", usercnt);
		}
	}

	private List<TXNUSER> getCombineList(List<TXNUSER> nb3datas, List<OLD_TXNUSER> nnbdatas) {
		/*TXNUSER同步2.5  兩邊TXNUSER相同  
		 *合併規則: 找出 nnbdatas 裡"不"在 nb3datas的資料合併
		 *Ex: nnbdatas: A123456789 , A123456814
		 *	  nb3datas: A123456814 , W100089655
		 *則合併結果是 : A123456789 , A123456814(同步故資料相同,取nb3) , W100089655
		 */
		log.debug(ESAPIUtil.vaildLog("nb3datas >> {} " + nb3datas.toString()));
		log.debug(ESAPIUtil.vaildLog("nnbdatas >> {} " + nnbdatas.toString()));
		List<String> nb3userList = new ArrayList<String>();
		for (TXNUSER nb3 : nb3datas) {
			nb3userList.add(nb3.getDPSUERID());
		}
		for (OLD_TXNUSER nnb : nnbdatas) {
			if (!nb3userList.contains(nnb.getDPSUERID())) {
				TXNUSER nnbConvert2nb3 = CodeUtil.objectCovert(TXNUSER.class, nnb);
				nb3datas.add(nnbConvert2nb3);
			}
		}
		
		log.debug(ESAPIUtil.vaildLog("Combine nb3datas >> {} " + nb3datas.toString()));
		return nb3datas;
	}

}
