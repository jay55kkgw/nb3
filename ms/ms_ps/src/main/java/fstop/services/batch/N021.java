package fstop.services.batch;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.exception.UncheckedException;
import fstop.model.DBResult;
import fstop.model.EachRowCallback;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.orm.dao.ItrCountDao;
import fstop.orm.dao.ItrIntervalDao;
import fstop.orm.dao.ItrN021Dao;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.po.ITRCOUNT;
import fstop.orm.po.ITRN021;
import fstop.services.AfterSuccessQuery;
import fstop.services.batch.annotation.Batch;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 台幣預約轉帳
 * 
 * @author Owner
 * 
 */
@Slf4j
@RestController
@Batch(id = "batch.n021", name = "台幣存款利率更新", description = "台幣存款利率更新")
public class N021 extends DispatchBatchExecute implements BatchExecute {

	private DecimalFormat fmt = new DecimalFormat("#0");
	// @Value("${filepath:/tmp/}")
	// String filepath ;
	// @Value("${filename:N021.txt}")
	// String filename ;

	@Autowired
	@Qualifier("n021Telcomm")
	private TelCommExec n021Telcomm;

	@Autowired
	private ItrN021Dao itrN021Dao;
	@Autowired
	private ItrCountDao itrCountDao;

	// private ITRCOUNT itrcount;
	@Autowired
	private ItrIntervalDao itrIntervalDao;

	public N021() {
	}

	@Autowired
	private SysBatchDao sysBatchDao;

	/**
	 * args[0] method name
	 */
	@RequestMapping(value = "/batch/n021", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	@Override
	public BatchResult execute(@RequestBody List<String> args) {

		List<String> safeArgs = ESAPIUtil.validStrList(args);
		log.info("batch N021...");
		log.info(ESAPIUtil.vaildLog("safeArgs: {}" + JSONUtils.toJson(safeArgs)));
		Date d = new Date();
		String today = DateTimeUtils.getDateShort(d);
		String time = DateTimeUtils.getTimeShort(d);
		BatchResult batchresult = new BatchResult();
		Map<String, Object> data = new HashMap();
		batchresult.setBatchName(getClass().getSimpleName());
		batchresult.setSuccess(true);

		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}

		Hashtable params = new Hashtable();
		// params.put("DATE", today);
		// params.put("TIME", time);
		log.info("DATE = {}", today);
		log.info("TIME = {}", time);

		final StringBuffer recno_idBuff = new StringBuffer();
		recno_idBuff.append("0");
		try {
			final ITRN021 t = new ITRN021();

			SimpleTemplate simpleTemplate = new SimpleTemplate(n021Telcomm);

			simpleTemplate.setAfterSuccessQuery(new AfterSuccessQuery() {

				public void execute(MVHImpl result) {
					
					itrN021Dao.deleteAll();

					t.setHEADER(result.getValueByFieldName("HEADER"));
					t.setSEQ(result.getValueByFieldName("SEQ"));
					t.setDATE(result.getValueByFieldName("DATE"));
					t.setTIME(result.getValueByFieldName("TIME"));
					t.setCOUNT(result.getValueByFieldName("COUNT"));
				}

			});

			simpleTemplate.setEachRowCallback(new EachRowCallback() {

				public void current(Row row) {
					// ITRN021 t = new ITRN021();
					Integer recno_id = new Integer(recno_idBuff.toString()) + 1;
					recno_idBuff.setLength(0);
					recno_idBuff.append(recno_id);

					String reconid = new String();
					reconid = "00" + recno_idBuff.toString();
					t.setRECNO(StrUtils.right(reconid, 2));
					log.trace("reconid >> {}" + StrUtils.right(reconid, 2));
					// t.setHEADER(row.getValue("HEADER"));
					// t.setSEQ(row.getValue("SEQ"));
					// t.setDATE(row.getValue("DATE"));
					// t.setTIME(row.getValue("TIME"));
					// t.setCOUNT(row.getValue("COUNT"));
					t.setCOLOR(row.getValue("COLOR"));
					t.setACC(row.getValue("ACC"));
					String itrstr = row.getValue("ITRX");
					String[] itr = new String[10];
					log.trace("itrstr >> {} |", itrstr);
					log.trace("itr.length >> {} ", itr.length);
					for (int i = 0; i < itr.length; i++) {
						itr[i] = itrstr.substring(i * 7, 7 + i * 7);
						log.trace("itr[{}]={} |", i, itr[i]);
					}
					t.setITR1(itr[0]);
					t.setITR2(itr[1]);
					t.setITR3(itr[2]);
					t.setITR4(itr[3]);
					t.setITR5(itr[4]);
					t.setITR6(itr[5]);
					t.setITR7(itr[6]);
					t.setITR8(itr[7]);
					t.setITR9(itr[8]);
					t.setITR10(itr[9]);
					t.setFILL(row.getValue("FILL"));
					itrN021Dao.insert(t);
				}
			});

			simpleTemplate.setExceptionHandler(new ExceptionHandler() {

				public void handle(Row row, Exception e) {

					log.error("無法更新台幣存款利率利率資料.", e);

				}

			});

			BatchCounter counter = simpleTemplate.execute(params);
		} catch (TopMessageException e) {
			batchresult.setSuccess(false);
			data.put("TOPMSG", e.getMsgcode() + "  " + e.getMsgout());
			log.error("TopMessageException  !!", e);
		} catch (UncheckedException e) {
			batchresult.setSuccess(false);
			data.put("TOPMSG", "UncheckedException 執行有誤 !!");
			log.error("UncheckedException 執行有誤 !!", e);
		}

		// batchresult.setData(JSONUtils.toJson(data));

		try {
			if (batchresult.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName, batchresult.getData());
			}
		} catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}

		return batchresult;

	}

	// 現在不寫檔案改回傳內容 註解掉
	/**
	 * 
	 * @param tranname
	 * @return
	 */
	// int writedatfile(String tranname)
	// {
	// String FullFileName=new String();
	// String WBuffer=new String();
	// ITRINTERVAL t = new ITRINTERVAL();
	// try
	// {
	// t=itrIntervalDao.findById(tranname);
	//// t=itrIntervalDao.get(tranname);
	// WBuffer=t.getINTERVAL().trim();
	// }
	// catch(ObjectNotFoundException objnotfund)
	// {
	// WBuffer="30";
	// }
	// System.out.println("WBuffer = " + WBuffer);
	//
	// FullFileName=(String)setting.get("filepath");
	// FullFileName+=setting.get("filename");
	// System.out.println("FullFileName = " + FullFileName);
	//
	// PrintWriter pw = null;
	// try
	// {
	// pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new
	// FileOutputStream(new
	// File((String)setting.get("filepath"),(String)setting.get("filename")), true)
	// , "US-ASCII")));
	// System.out.println("WBuffer = " + WBuffer);
	// pw.println(WBuffer);
	// pw.flush();
	// }
	// catch(Exception e) {
	// throw new fstop.model.ModelException("載入 FieldTranslator Properties 錯誤 !",
	// e);
	// }
	//
	// finally {
	// try {
	// pw.close();
	// }
	// catch(Exception e) {
	// }
	// }
	//
	// return(0);
	// }

	public MVH query(Map<String, String> params) {
		List<ITRN021> qresult = itrN021Dao.getAll();
		addItrCount();
		MVHImpl mvh = new DBResult(qresult);
		// toHtml(mvh,"c:\\N021.html"); //舊網銀已經註解
		return mvh;
	}

	private void addItrCount() {
		Date d = new Date();
		String today = DateTimeUtils.getCDateShort(d);
		String time = DateTimeUtils.getTimeShort(d);
		ITRCOUNT it = null;
		int iCount = 1;// 若尚未有點選過，第一次寫入的次數值為一
		try {
			it = itrCountDao.findById("N021");
		} catch (ObjectNotFoundException e) {
			it = null;
		}
		if (it != null) {
			String count = it.getCOUNT();
			if (!"".equals(count) || count != null) {
				iCount = Integer.parseInt(count);// 把原本的次數找出來
				iCount++;// 原本的次數加一;
				it.setDATE(today);
				it.setTIME(time);
				it.setCOUNT(Integer.toString(iCount));
			}
		} else {
			it = new ITRCOUNT();
			it.setHEADER("N021");
			it.setDATE(today);
			it.setTIME(time);
			it.setCOUNT("1");

		}
		itrCountDao.save(it);
	}

	// TODO 不確定這個是否還有用到 ，舊網銀已經註解 所以整個先註解掉
	/**
	 * 
	 * @param data
	 * @param path
	 */
	// private void toHtml(MVH data, String path)
	// {
	// try{
	// File old = new File(path);
	// if(old.exists()) old.delete();
	// }catch (Exception e)
	// {}
	// int i_count = data.getValueOccurs("RECNO");
	// PrintWriter pw = null;
	// try {
	// pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new
	// FileOutputStream(new File(path), true) , "UTF-8")));
	// pw.println("<html>");
	// pw.println("<head>");
	// pw.println("<meta http-equiv=\"Content-Type\" content=\"text/html;
	// charset=UTF-8\">");
	// pw.println("</head>");
	// pw.println("<body>");
	// pw.println("<H3 ALIGN=CENTER></H>");
	// pw.println("<H3 ALIGN=\"CENTER\">
	// "+CommonPools.sysop.getApp("N021")+"</H><BR>");
	// pw.println("<H3 ALIGN=\"RIGHT\">年息
	// %&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</H>");
	// if(i_count > 0)
	// {
	// pw.println("<TABLE BORDER=\"2\" cellspacing=\"0\" ALIGN=\"CENTER\"><FONT
	// SIZE=\"-1\">");
	// pw.println("<tr><TH><FONT SIZE=\"-1\">級距</font><TH COLSPAN=\"2\"><FONT
	// SIZE=\"-1\">一般利率</font><TH COLSPAN=\"2\"><FONT
	// SIZE=\"-1\">大額壹億元(不含)以上</font><TH COLSPAN=\"2\"><FONT
	// SIZE=\"-1\">大額2000萬元(不含)以上</font><TH COLSPAN=\"2\"><FONT
	// SIZE=\"-1\">大額1000萬元(不含)以上</font><TH COLSPAN=\"2\"><FONT
	// SIZE=\"-1\">大額500萬元(不含)以上</font></tr>");
	// pw.println("<tr><TH><FONT SIZE=\"-1\">科目.期別</font><TH><FONT
	// SIZE=\"-1\">固定</font><TH><FONT SIZE=\"-1\">機動</font><TH><FONT
	// SIZE=\"-1\">固定</font><TH><FONT SIZE=\"-1\">機動</font><TH><FONT
	// SIZE=\"-1\">固定</font><TH><FONT SIZE=\"-1\">機動</font><TH><FONT
	// SIZE=\"-1\">固定</font><TH><FONT SIZE=\"-1\">機動</font><TH><FONT
	// SIZE=\"-1\">固定</font><TH><FONT SIZE=\"-1\">機動</font></tr>");
	// for (int i=0; i<i_count; i++)
	// {
	// pw.println("<TR BGCOLOR="+FieldTranslator.transfer("N021", "COLOR",
	// data.getValueByFieldName("COLOR", i+1))+">");
	// pw.println("<TD Align=\"right\"><FONT>"+data.getValueByFieldName("ACC",
	// i+1)+"</FONT>&nbsp;</td>");
	// pw.println("<TD Align=\"right\"><FONT>"+data.getValueByFieldName("ITR1",
	// i+1)+"</FONT>&nbsp;</td>");
	// pw.println("<TD Align=\"right\"><FONT>"+data.getValueByFieldName("ITR2",
	// i+1)+"</FONT>&nbsp;</td>");
	// pw.println("<TD Align=\"right\"><FONT>"+data.getValueByFieldName("ITR3",
	// i+1)+"</FONT>&nbsp;</td>");
	// pw.println("<TD Align=\"right\"><FONT>"+data.getValueByFieldName("ITR4",
	// i+1)+"</FONT>&nbsp;</td>");
	// pw.println("<TD Align=\"right\"><FONT>"+data.getValueByFieldName("ITR5",
	// i+1)+"</FONT>&nbsp;</td>");
	// pw.println("<TD Align=\"right\"><FONT>"+data.getValueByFieldName("ITR6",
	// i+1)+"</FONT>&nbsp;</td>");
	// pw.println("<TD Align=\"right\"><FONT>"+data.getValueByFieldName("ITR7",
	// i+1)+"</FONT>&nbsp;</td>");
	// pw.println("<TD Align=\"right\"><FONT>"+data.getValueByFieldName("ITR8",
	// i+1)+"</FONT>&nbsp;</td>");
	// pw.println("<TD Align=\"right\"><FONT>"+data.getValueByFieldName("ITR9",
	// i+1)+"</FONT>&nbsp;</td>");
	// pw.println("<TD Align=\"right\"><FONT>"+data.getValueByFieldName("ITR10",
	// i+1)+"</FONT>&nbsp;</td></TR>");
	// }
	// }else pw.println("查無資料!!!");
	// pw.println("<center><table border=0>");
	// pw.println("<tr><td><FONT SIZE=-1>註 :指定到期日之定期存款，其利率按實際存滿月數之相當期別同性質存款牌告利率計息。
	// 如無該期別之牌告利率，則依較低期別牌告利率計息。</td></tr>");
	// pw.println("</table></center>");
	// pw.println("<P><P><P><P>");
	// pw.println("<form>");
	// pw.println("<center><table border=0>");
	// pw.println("<tr><td><input type='button' value='回上一頁'
	// OnClick='history.back()'></td><td><input type='button' value='登出'
	// OnClick='javascript:self.close()'</td></tr></table></center>");
	// pw.println("</form>");
	// pw.println("</body>");
	// pw.println("</html>");
	// //pw.write(str);
	// }
	// catch(Exception e) {
	// log.error("", e);
	// }
	// finally {
	// try {
	// pw.close();
	// }
	// catch(Exception e) {
	// }
	// }
	//
	// }

}
