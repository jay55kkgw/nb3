package fstop.services.batch;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.ESAPIUtil;

import fstop.notification.Notification;
import fstop.notification.PushData;
import fstop.notification.TopicLabel;
import fstop.notifier.NotifyAngent;
import fstop.notifier.PushAgent;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.SysParamDataDao;
import fstop.orm.dao.TxnPhoneTokenDao;
import fstop.orm.po.SYSPARAMDATA;
import fstop.orm.po.TXNPHONETOKEN;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import fstop.util.thread.ThreadSemaphore;
import lombok.extern.slf4j.Slf4j;

/**
 * 生日推播通知
 * 
 * @author Owner 每天 10:30 自動執行 讀取/TBB/nb3/hostdata/XXXX.txt
 * 
 */
@Slf4j
@RestController
@Batch(id = "batch.NotifyBirth", name = "生日推播通知", description = "生日推播通知")
public class NotifyBirth implements BatchExecute {
//	String FullFileName = "C:/Users/BenChien/Desktop/batchLocalTest/hostdata/MBBIRDY.TXT";
	String FullFileName = "/tmp/batch/hostdata/MBBIRDY.TXT";
	String pushtext = "親愛的客戶您好：在這特別的日子，臺灣企銀祝您生日快樂！";

	// private Map setting;

	@Autowired
	private TxnPhoneTokenDao txnPhoneTokenDao;

	@Autowired
	private SysParamDataDao sysParamDataDao;

	private TXNPHONETOKEN txnphonetoken;

	public NotifyBirth() {
	}

	@Autowired
	private SysBatchDao sysBatchDao;

	/**
	 * args[0] method name
	 */
	@RequestMapping(value = "/batch/notifyBirth", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {

		log.info("batch NotifyBirth...");

		BatchResult batchresult = new BatchResult();
		Map<String, Object> data = new LinkedHashMap();
		batchresult.setBatchName(getClass().getSimpleName());
		batchresult.setSuccess(true);

		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}

		List<String> safeArgs = ESAPIUtil.validStrList(args);
		int totalcnt = 0, failcnt = 0, okcnt = 0, flushcnt = 0;
		PushData pushData;
		String token = null;
		String CUSIDN = null;
		// List<PushData> listPushData = new ArrayList<PushData>();

		// Map<String, Object> data = new HashMap();
		// data.put("COUNTER", new BatchCounter());

		// 從檔案取得要推播的ID
		List<String> listID = getIDfromFile();
		// null 有錯誤
		if (null == listID) {
			data.put("Error", "getIDfromFile Error");
			batchresult.setSuccess(false);
			batchresult.setData(data);
			try {
				sysBatchDao.finish_FAIL2(batchName, batchresult.getData());
			} catch (Exception e2) {
				log.error("sysBatchDao.finish error >> {} ", e2.getMessage());
			}

			return batchresult;
		}

		totalcnt = listID.size();
		// System.out.println("Total Birth ID=========>" + listID.size());
		log.info("Total Birth ID=========>" + listID.size());

		for (int i = 0; i < listID.size(); i++) {
			CUSIDN = listID.get(i);
			log.trace("CUSIDN {}", CUSIDN);
			try {
				txnphonetoken = txnPhoneTokenDao.getADPhoneToken(CUSIDN);
				if (txnphonetoken != null) {
					token = txnphonetoken.getPHONETOKEN();
					if (token != null && !token.equals("")) {

						okcnt++;
						token = txnphonetoken.getPHONETOKEN();
						log.info(ESAPIUtil.vaildLog("txnphonetoken:" + token));

						if (txnphonetoken.getPHONETYPE().equals("I")) {
							List<Object> arrdevices = new ArrayList<Object>();
							List<Object> arruser = new ArrayList<Object>();
							arrdevices.add(txnphonetoken.getPHONETOKEN());
							arruser.add(txnphonetoken.getDPUSERID());

							// Notification notification =
							// (Notification)SpringBeanFactory.getBean("notification");
							Notification notification = new Notification();
							// System.out.println("Send to APNS Start(" + token + ")");
							log.info(ESAPIUtil.vaildLog("Send to APNS Start:" + token));
							notification.notificationToiOS(arruser, arrdevices, pushtext, "5", 0, "");
							// System.out.println("Send to APNS End(" + token + ")");
							log.info(ESAPIUtil.vaildLog("Send to APNS End:" + token));

						} else if (txnphonetoken.getPHONETYPE().equals("A")) {
							if (token != null) {
								final List<PushData> listPushData = new ArrayList<PushData>();
								pushData = new PushData();
								Map<String, String> messageMap = new HashMap<String, String>();
								messageMap.put("TYPE", "TR");
								messageMap.put("MESSAGE", pushtext);

								pushData.setMessage(JSONUtils.map2json(messageMap));
								pushData.setTopic(TopicLabel.TOPICLABEL_ID + token);
								pushData.setCusId(CUSIDN);
								pushData.setToken(token);
								listPushData.add(pushData);

								log.debug(ESAPIUtil.vaildLog("listPushData.size: " + listPushData.size()));
								ThreadSemaphore threadSemaphore = null;
								try {
									threadSemaphore = new ThreadSemaphore(1);
									Runnable run = new Runnable() {
										public void run() {
											try {
												log.warn("PushAgent.sendNotice(listPushData) before");
												PushAgent.sendNotice(listPushData);
												log.warn("PushAgent.sendNotice(listPushData) after");
											} catch (Exception e) {
												log.warn("PushAgent.sendNotice(listPushData) Runnable run() error");
											}
										}
									};
									threadSemaphore.execute(run);
								} catch (Exception e) {
									log.warn("PushAgent.sendNotice(listPushData) ThreadSemaphore error");
								} finally {
									try {
										threadSemaphore.shutdown();
										log.warn("PushAgent.sendNotice(listPushData) threadSemaphore.shutdown()");
									} catch (Exception e) {
									}
								}
								// PushAgent.sendNotice(listPushData);

								// System.out.println("pushData.getTopic:" + TopicLabel.TOPICLABEL_ID + token);
								// System.out.println("pushData.getCusId:" + CUSIDN);
								log.info(ESAPIUtil.vaildLog("pushData.getTopic:" + TopicLabel.TOPICLABEL_ID + token));
								// log.info("pushData.getCusId:" + CUSIDN);
							}
						}
					} else {
						failcnt++;
						log.debug(ESAPIUtil.vaildLog("NotifyBirth CUSIDN = " + CUSIDN + " phonetoken not exist"));
					}
				} else {
					failcnt++;
					log.debug(ESAPIUtil
							.vaildLog("NotifyBirth CUSIDN = " + CUSIDN + " not register to notification service"));
				}
			} catch (Exception e) {
				failcnt++;
				log.error("NotifyBirth Exception:" + e.getMessage());
			}
		}

		// 批次完成
		BatchCounter bcnt = new BatchCounter();
		bcnt.setFailCount(failcnt);
		bcnt.setSuccessCount(okcnt);
		bcnt.setTotalCount(totalcnt);
		data.put("COUNTER", bcnt);

		batchresult.setData(data);

		Date startTime = new Date();
		Hashtable<String, String> varmail = new Hashtable();
		varmail.put("SYSDATE", DateTimeUtils.format("yyyy/MM/dd", startTime));
		varmail.put("SYSTIME", DateTimeUtils.format("HH:mm:ss", startTime));
		varmail.put("SYSSTATUS", JSONUtils.toJson(data));
		varmail.put("ERRORMSG", "");

		SYSPARAMDATA po = null;
		po = sysParamDataDao.findById("NBSYS");
		String receivers = po.getADAPMAIL().replace(";", ",");

		// call BillHunter
		boolean isSendSuccess = NotifyAngent.sendNotice("BATCH_BIRTH_MAIL", varmail, receivers, "batch");

		if (!isSendSuccess) {
			log.debug("NotifyBirth 發送 Email 失敗.(" + receivers + ")");
			// batchresult.getFlatValues().put("__MAILERR", "發送 Email 失敗.(" + email1 + ")");
			data.put("Error", "NotifyBirth 發送 Email 失敗.(" + receivers + ")");
			batchresult.setData(data);
		}

		log.info("結束寄送批次作業結果通知: " + DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", startTime));

		try {
			// 新增SYSBATCH紀錄
			sysBatchDao.finish_OK(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.finish_OK error >> {} ", e.getMessage());
		}

		return batchresult;
	}

	// 從檔案取得ID
	public List<String> getIDfromFile() {
		log.debug("getIDfromFile Start");

		// 推播統編
		List<String> listID = new ArrayList<String>();

		InputStreamReader in = null;
		BufferedReader bin = null;

		// if (listID != null){
		// listID.clear();
		// }

		try {
			in = new InputStreamReader(new FileInputStream(FullFileName), "BIG5");
			bin = new BufferedReader(in);
			String data;
			String id;
			String bitrh;
			do {
				data = bin.readLine();
				if (data == null || data.length() == 0) {
					break;
				}

				id = data.substring(0, 10).trim();
				bitrh = data.substring(10, 17).trim();
				// log.debug("id=========>" + id);
				// log.debug("bitrh=========>" + bitrh);

				// 判斷日期是否為今日
				String CurrentDate = DateTimeUtils.getCDateShort(new Date());
				if (bitrh.equals(CurrentDate)) {
					listID.add(id);
				}

			} while (true);
		} catch (Exception e) {
			log.error("getIDfromFile Error=========>" + e.toString());
			return null;
		} finally {
			try {
				in.close();
				bin.close();
			} catch (Exception e) {
			}
			in = null;
			log.debug("getIDfromFile End");
		}

		return listID;

	}
}
