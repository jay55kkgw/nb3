package fstop.services.batch;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.rest.model.JSPUtils;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.orm.dao.AdmMsgCodeDao;
import fstop.orm.dao.Nb3SysOpDao;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.TxnLogDao;
import fstop.orm.oao.Old_TxnLogDao;
import fstop.orm.old.OLD_TXNLOG;
import fstop.orm.po.NB3SYSOP;
import fstop.orm.po.TXNLOG;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import fstop.util.FtpBase64;
import fstop.util.FtpPathProperties;
import fstop.util.JSONUtils;
import lombok.extern.slf4j.Slf4j;

import fstop.orm.dao.TxnFundDataDao;
import fstop.orm.po.TXNFUNDDATA;
@Slf4j
@RestController
@Batch(id = "batch.TxnLogtoCrm", name = "", description = "")
public class TxnLogtoCrm implements BatchExecute {

	@Autowired
	private TxnFundDataDao txnFundDataDao;
	
	@Autowired
	private TxnLogDao txnLogdao;

	@Autowired
	private Old_TxnLogDao old_TxnLogDao;

	@Autowired
	private Nb3SysOpDao nb3sysOpDao;
	
	@Autowired
	private AdmMsgCodeDao admMsgCodeDao;

	@Value("${crmn.ftp.ip}")
	private String crmn_ftp_ip;

	@Value("${isParallelCheck}")
	private String isParallelCheck;

	@Value("${doFTP}")
	private String doFTP;

	@Autowired
	private SysBatchDao sysBatchDao;

	@RequestMapping(value = "/batch/txnlogtocrm", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {

		log.info("batch TxnLogtoCrm...");

		BatchResult batchresult = new BatchResult();
		Map<String, Object> data = new LinkedHashMap();
		batchresult.setBatchName(getClass().getSimpleName());
		batchresult.setSuccess(true);

		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error(ESAPIUtil.vaildLog("sysBatchDao.initSysBatchPo error >>" + e.getMessage()));
		}

		List<String> safeArgs = ESAPIUtil.validStrList(args);
		Integer totalcnt = 0, failcnt = 0, okcnt = 0;
		String startDate;
		Calendar rightNow = Calendar.getInstance();
		rightNow.setTime(new java.util.Date());
		Date dt1 = rightNow.getTime();

		if (safeArgs.size() > 0) {
			startDate = safeArgs.get(0);
		} else {
			startDate = DateTimeUtils.getDateShort(dt1);
			SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
			try {
				Date sDate = formatter.parse(startDate);
				log.debug(ESAPIUtil.vaildLog("sDate:" + sDate));
				Calendar rightnow = Calendar.getInstance();
				rightnow.setTime(sDate);
				rightnow.add(Calendar.DAY_OF_YEAR, -1);
				Date dt2 = rightnow.getTime();
				startDate = formatter.format(dt2);
				log.debug(ESAPIUtil.vaildLog("FINAL startDate:" + startDate));

			} catch (Exception e) {
				log.error(ESAPIUtil.vaildLog("DATE ERROR:" + e));
				startDate = DateTimeUtils.getDateShort(dt1);
			}
		}

		log.info(ESAPIUtil.vaildLog("startDate:" + startDate));

		List<TXNLOG> txnLogList = txnLogdao.getTxnLogforcrm(startDate);
		log.info(ESAPIUtil.vaildLog("NB 3 data size >> "+ txnLogList.size()));
		if ("Y".equals(isParallelCheck)) {
			List<OLD_TXNLOG> oldList = old_TxnLogDao.getTxnLogforcrm(startDate);

			log.info(ESAPIUtil.vaildLog("NB 2.5 data size >> "+ oldList.size()));

			for (OLD_TXNLOG old_txnlog : oldList) {
				TXNLOG nnbConvert2nb3 = CodeUtil.objectCovert(TXNLOG.class, old_txnlog);
				txnLogList.add(nnbConvert2nb3);
			}
		}

		Map<String, String> nb3sysOp = getNb3SYSOP();
		log.debug(ESAPIUtil.vaildLog("TxnLogtoCrm.java sysOp:" + nb3sysOp.size()));

		StringBuilder writeFileString = new StringBuilder();
		String ADTXNO = "";
		String ADUSERID = "";
		String ADOPID = "";
		String ADOPNAME = "";
		String ADTXAMT = "";
		String ADREQTYPE = "";
		String ADUSERIP = "";
		String ADEXCODE = "";
		String ADEXCODENAME = "";
		String LASTDATE = "";
		String LASTTIME = "";
		String LOGINTYPE = "";
		String Opt_Txt = "";
		String ADTXACNO = ""; //20200504 新增轉出帳號欄位
		String ADCURRENCY = ""; //20200611 新增轉出帳號欄位
		String TRANSCODE = "";//20210816新增 基金代碼
		String infundlname = "";//20210816新增 基金名稱
		TXNLOG txnLogEx = new TXNLOG();

		int count = txnLogList.size();
		log.info(ESAPIUtil.vaildLog("txnlogtocrm.java txnLogList size (ALL) : " + count));

		String datacount = String.format("%09d", count);

		try {

			FtpPathProperties nrcpro = new FtpPathProperties();
			String srcpath = nrcpro.getSrcPath("useractionrecord").trim();
			// Ltest
			// srcpath = "C:\\Users\\BenChien\\Desktop\\batchLocalTest\\data\\";
			String despath = nrcpro.getDesPath("useractionrecord").trim();
			String ip = crmn_ftp_ip;
			FtpBase64 ftpBase641 = new FtpBase64(ip);
			File saveFile = new File(srcpath + "EB_DTXNLOG.TXT." + startDate, "");
			saveFile.setWritable(true, true);
			saveFile.setReadable(true, true);
			saveFile.setExecutable(true, true);
			BufferedWriter fwriter = new BufferedWriter(
					new OutputStreamWriter(new FileOutputStream(saveFile), "BIG5"));
			// FileWriter fwriter = new FileWriter(new FileOutputStream(saveFile));

			// --------------產生String-----------------------------
			for (int i = 0; i < count; i++) {
				try {
					txnLogEx = txnLogList.get(i);

					ADTXNO = txnLogEx.getADTXNO();

					ADUSERID = txnLogEx.getADUSERID();

					ADOPID = txnLogEx.getADOPID();

					ADOPNAME = nb3sysOp.get(txnLogEx.getADOPID().trim()) == null ? ""
							: nb3sysOp.get(txnLogEx.getADOPID().trim());

					ADTXAMT = txnLogEx.getADTXAMT();

					ADREQTYPE = txnLogEx.getADREQTYPE();

					ADUSERIP = txnLogEx.getADUSERIP();
					
					ADEXCODE = txnLogEx.getADEXCODE();
					
					ADEXCODENAME = "";
					
					if(!"".equals(ADEXCODE.trim())) {
						ADEXCODENAME = admMsgCodeDao.getErrorCodeMsg(ADEXCODE).replaceAll(";|；", ",");
					}

					LASTDATE = txnLogEx.getLASTDATE();

					LASTTIME = txnLogEx.getLASTTIME();

					LOGINTYPE = txnLogEx.getLOGINTYPE();
					
					ADTXACNO = txnLogEx.getADTXACNO();//20200504 新增轉出帳號欄位
					
					ADCURRENCY = txnLogEx.getADCURRENCY();
					
					//20210816新增
					//先找txnlog電文中的基金代號，再依代號去找txnfunddata中對應的名稱
					String space="";
					Map map = JSONUtils.json2map(txnLogEx.getADCONTENT());
					String str_transcode = (String)map.get("TRANSCODE");//基金代碼4位
					log.debug("TRANSCODE：" + str_transcode);	
					String str_findLname;//基金名稱48位
					if(str_transcode != null && !str_transcode.equals("")){				
						
						List<TXNFUNDDATA> fdata = txnFundDataDao.findByFUNDLNAME(str_transcode);
						TXNFUNDDATA fd = fdata.get(0);
						
						str_findLname = JSPUtils.convertFullorHalf(fd.getFUNDLNAME().trim().replace(" ",""), 1);
						if(str_findLname.trim().length() > 24){
							str_findLname=str_findLname.trim().substring(0, 24);
						}
						log.debug("FUNDLNAME：" + str_findLname);
						for (int m=0; m < 48-(str_findLname.trim().length())*2; m++)
							space+=" ";
						str_findLname=str_findLname+space;
						space="";
					}
					else{
						str_transcode = "    ";
						str_findLname = "                                                ";
					}
					
					
					Opt_Txt = startDate + ";" + ADTXNO + ";" + ADUSERID + ";" + ADOPID + ";" + ADOPNAME + ";"+ ADTXACNO + ";" + ADTXAMT
							+ ";"+ ADCURRENCY + ";" + ADREQTYPE + ";" + ADUSERIP + ";" +ADEXCODE + ";" + ADEXCODENAME+ ";" + LASTDATE + ";" + LASTTIME + ";" + LOGINTYPE + ";" + str_transcode + ";" + str_findLname;
					log.debug(ESAPIUtil.vaildLog("Opt_Txt:" + Opt_Txt));
					writeFileString.append(Opt_Txt);
					log.debug(ESAPIUtil.vaildLog("Opt_Txt append"));
					writeFileString.append("\r\n");
					log.debug("rn");
					okcnt++;
					if ((i + 1) % 10000 == 0) {
						fwriter.write(writeFileString.toString());
						writeFileString.delete(0, writeFileString.length());
						// writeFileString ="" ;
						log.debug(ESAPIUtil.vaildLog("txnlogtocrm.java For Loop writeFileString:  " + i));
					}
				} catch (Exception e) {
					log.error(ESAPIUtil.vaildLog("txnlogtocrm.java ERROR :" + e.getMessage()));
					log.error(ESAPIUtil.vaildLog("txnlogtocrm.java ERROR : i=" + i + ", dateADTXNO:" + txnLogEx.getADTXNO()));
					failcnt++;
				}
				totalcnt++;
			}
			log.debug("txnlogtocrm.java For Loop create writeFileString Sccess");
			log.debug(ESAPIUtil.vaildLog("1writeFileString:" + writeFileString.toString()));
			// ---------------------------------------------------------------------------

			fwriter.write(writeFileString.toString());
			log.debug(ESAPIUtil.vaildLog("2writeFileString:" + writeFileString.toString()));
			fwriter.flush();
			log.debug("fwriter.flush()");
			fwriter.close();
			log.debug("fwriter.close()");

			log.debug("txnlogtocrm.java fwriter.write  Sccess");
			if ("Y".equals(doFTP)) {
				int rc = ftpBase641.upload(despath + "EB_DTXNLOG.TXT." + startDate, new FileInputStream(saveFile));
//				if (rc == 0) {
//					File deleteFile = new File(srcpath + "EB_DTXNLOG.TXT." + startDate);
//					deleteFile.setWritable(true, true);
//					deleteFile.setReadable(true, true);
//					deleteFile.setExecutable(true, true);
//					deleteFile.delete();
//				}
				log.info("txnlogtocrm.java FTP UPLOAD  Sccess");
			}

			try {
				log.debug("txnlogtocrm.java dir start");
				StringBuilder writeFileString_dir = new StringBuilder();
				FtpPathProperties nrcpro_dir = new FtpPathProperties();
				String srcpath_dir = nrcpro_dir.getSrcPath("useractionrecord").trim();
				// Ltest
				// srcpath_dir = "C:\\Users\\BenChien\\Desktop\\batchLocalTest\\data\\";
				String despath_dir = nrcpro_dir.getDesPath("useractionrecord").trim();
				log.debug(ESAPIUtil.vaildLog("txnlogtocrm.java dir srcpath_dir:" + srcpath_dir));
				log.debug(ESAPIUtil.vaildLog("txnlogtocrm.java dir despath_dir:" + despath_dir));
				String ip_dir = crmn_ftp_ip;
				log.debug(ESAPIUtil.vaildLog("txnlogtocrm.java dir ip_dir:" + ip_dir));
				FtpBase64 ftpBase641_dir = new FtpBase64(ip_dir);
				File saveFile_1 = new File(srcpath_dir + "dir.EB_DTXNLOG.TXT." + startDate);
				saveFile_1.setWritable(true, true);
				saveFile_1.setReadable(true, true);
				saveFile_1.setExecutable(true, true);
				// FileWriter fwriter_1 = new FileWriter(saveFile_1);
				OutputStream fwriter_1 = new FileOutputStream(saveFile_1);
				BufferedWriter out = new BufferedWriter(new OutputStreamWriter(fwriter_1, "BIG5"));
				// --------------產生dir_String-----------------------------
				String dir_txt = "EB_DTXNLOG.TXT." + startDate + " " + "9999" + " " + datacount;
				log.debug(ESAPIUtil.vaildLog("dir_txt:" + dir_txt));
				writeFileString_dir.append(dir_txt);
				log.debug(ESAPIUtil.vaildLog("writeFileString1:" + writeFileString_dir.toString()));
				// ---------------------------------------------------------------------------

				out.write(writeFileString_dir.toString());
				log.debug(ESAPIUtil.vaildLog("writeFileStringdir:" + writeFileString_dir.toString()));
				out.flush();
				log.debug("fwriter_dir.flush()");
				out.close();
				log.debug("fwriter_dir.close()");
				fwriter_1.close();

				log.debug("txnlogtocrm_dir.java fwriter.write  Sccess");
				if ("Y".equals(doFTP)) {
					int rc1 = ftpBase641_dir.upload(despath_dir + "dir.EB_DTXNLOG.TXT." + startDate,
							new FileInputStream(saveFile_1));
//					if (rc1 == 0) {
//						File deleteFile_dir = new File(srcpath_dir + "dir.EB_DTXNLOG.TXT." + startDate);
//						deleteFile_dir.setWritable(true, true);
//						deleteFile_dir.setReadable(true, true);
//						deleteFile_dir.setExecutable(true, true);
//						deleteFile_dir.delete();
//					}
					log.info("txnlogtocrm_dir.java FTP UPLOAD  Sccess");
				}

			} catch (Exception e) {
				log.error("TxnLogtoCrm_dir.java ERROR :" + e.getMessage());
			}

		} catch (Exception e) {
			log.error(ESAPIUtil.vaildLog("TxnLogtoCrm.java ERROR :" + e.getMessage()));
			data.put("Error", e.getMessage());
			batchresult.setSuccess(false);
			batchresult.setData(data);

			try {
				sysBatchDao.finish_FAIL2(batchName, batchresult.getData());
			} catch (Exception e2) {
				log.error(ESAPIUtil.vaildLog("sysBatchDao.finish error >>  " + e2.getMessage()));
			}
			return batchresult;

		}

		BatchCounter bcnt = new BatchCounter();
		bcnt.setFailCount(failcnt);
		bcnt.setSuccessCount(okcnt);
		bcnt.setTotalCount(totalcnt);

		data.put("COUNTER", bcnt);
		batchresult.setData(data);

		try {
			// 新增SYSBATCH紀錄
			sysBatchDao.finish_OK(batchName);
		} catch (Exception e) {
			log.error(ESAPIUtil.vaildLog("sysBatchDao.finish_OK error >>  " + e.getMessage()));
		}

		return batchresult;
	}

	public Map<String, String> getNb3SYSOP() {

		List<NB3SYSOP> qresult = nb3sysOpDao.findAll();
		int size = qresult.size();
		Map<String, String> map = new HashMap();
		for (int i = 0; i < size; i++) {
			map.put(qresult.get(i).getADOPID().trim(), qresult.get(i).getADOPNAME().trim());
		}
		return map;
	}
}
