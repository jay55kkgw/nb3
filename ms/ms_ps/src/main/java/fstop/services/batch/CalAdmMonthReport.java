package fstop.services.batch;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.ESAPIUtil;

import fstop.orm.dao.AdmMonthReportDao;
import fstop.orm.dao.SysBatchDao;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 功能說明 :批次月統計報表
 *
 */
@Slf4j
@RestController
@Batch(id="batch.calAdmMonthReport", name = "批次月統計報表", description = "批次月統計報表")
public class CalAdmMonthReport  extends DispatchBatchExecute implements BatchExecute {

	private DecimalFormat fmt = new DecimalFormat("#0");

	@Autowired
	private AdmMonthReportDao admMonthReportDao;
	
	@Autowired
	private SysBatchDao sysBatchDao;
	
	
	@RequestMapping(value = "/batch/calAdmMonthReport", method = {RequestMethod.POST},
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {
		
		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}
		
		List<String> safeArgs = ESAPIUtil.validStrList(args);
		
		BatchResult batchresult = new BatchResult();
		Map<String, Object> data = new HashMap();
		boolean rs = true;
		
		Date d = new Date();
		String today = DateTimeUtils.getDateShort(d);
		String time = DateTimeUtils.getTimeShort(d);
		
		log.info("EXEC CALADMMONTHREPORT");
		log.info("DATE = {}" , today);
		log.info("TIME = {}" , time);
		
		
		//執行上個月的 monthreport
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DATE, 1);
		calendar.add(Calendar.MONTH, -1);
		Date execDate = calendar.getTime();
		String yyyy = DateTimeUtils.format("yyyy", execDate);
		String mm = DateTimeUtils.format("MM", execDate);
		log.info(ESAPIUtil.vaildLog("safeArgs>>{}"+safeArgs));
		
		if(safeArgs.size() > 0) {
			if(safeArgs.get(0).length()==5) {
				yyyy = String.valueOf(Integer.parseInt(safeArgs.get(0).substring(0, 3))+1911);
				mm = safeArgs.get(0).substring(3);
			}else {
				yyyy = safeArgs.get(0).substring(0, 4);
				mm = safeArgs.get(0).substring(4);
			}
		}
		log.info(ESAPIUtil.vaildLog("yyyy={},mm={}"+yyyy+","+mm));
		
		try {
			// yyyy -> 2008, mm -> 02
			admMonthReportDao.execCalProcedure(yyyy, mm);
		}catch(Exception e){
			log.error("StroeProcError >> {}" ,e.getCause());
			data.put("ERRORMSG", e.getMessage());
			rs = false;
		}
		batchresult.setSuccess(rs);
		batchresult.setBatchName(getClass().getSimpleName());
		
		try {
			if (batchresult.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName,batchresult.getData());
			}
		}catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}
		
		return batchresult;
	}
}
