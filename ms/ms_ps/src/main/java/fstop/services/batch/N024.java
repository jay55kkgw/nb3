package fstop.services.batch;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringReader;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.ObjectNotFoundException;
//import org.jdom.Document;
//import org.jdom.Element;
//import org.jdom.output.Format;
//import org.jdom.output.XMLOutputter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.exception.UncheckedException;

//import webBranch.utility.FieldTranslator;

import fstop.model.DBResult;
import fstop.model.EachRowCallback;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.model.Rows;
import fstop.model.TelcommResult;
import fstop.orm.dao.ItrCountDao;
import fstop.orm.dao.ItrIntervalDao;
import fstop.orm.dao.ItrN024Dao;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.po.ITRCOUNT;
import fstop.orm.po.ITRINTERVAL;
import fstop.orm.po.ITRN022;
import fstop.orm.po.ITRN024;
import fstop.services.AfterSuccessQuery;
import fstop.services.batch.annotation.Batch;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.FtpTemplate;
import fstop.util.JSONUtils;
import fstop.util.PathSetting;
import fstop.util.StrUtils;
//import webBranch.utility.IMasterValuesHelper;
import lombok.extern.slf4j.Slf4j;

/**
 * 外幣放款利率更新
 * 
 * @author Owner
 * 
 */
@Slf4j
@RestController
@Batch(id = "batch.n024", name = "外幣放款利率更新", description = "外幣放款利率更新")
public class N024 extends DispatchBatchExecute implements BatchExecute {
	// private Logger logger = Logger.getLogger("fstop_txntw");

	private DecimalFormat fmt = new DecimalFormat("#0");

	@Autowired
	@Qualifier("n024Telcomm")
	private TelCommExec n024Telcomm;

	@Autowired
	private ItrN024Dao itrN024Dao;

	@Autowired
	private ItrCountDao itrCountDao;

	@Autowired
	private ItrIntervalDao itrIntervalDao;

	@Autowired
	private SysBatchDao sysBatchDao;

	public N024() {
	}

	/**
	 * args[0] method name
	 */
	@RequestMapping(value = "/batch/n024", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	@Override
	public BatchResult execute(@RequestBody List<String> args) {
		List<String> safeArgs = ESAPIUtil.validStrList(args);
		log.info("batch N024...");
		log.info(ESAPIUtil.vaildLog("safeArgs: {}" + JSONUtils.toJson(safeArgs)));
		Date d = new Date();
		String today = DateTimeUtils.getDateShort(d);
		String time = DateTimeUtils.getTimeShort(d);

		BatchResult batchresult = new BatchResult();
		Map<String, Object> data = new HashMap();
		batchresult.setBatchName(getClass().getSimpleName());
		batchresult.setSuccess(true);

		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}

		Hashtable params = new Hashtable();
		// params.put("DATE", today);
		// params.put("TIME", time);
		log.info("DATE = " + today);
		log.info("TIME = " + time);

		final StringBuffer recno_idBuff = new StringBuffer();
		recno_idBuff.append("0");
		try {
			final ITRN024 t = new ITRN024();

			SimpleTemplate simpleTemplate = new SimpleTemplate(n024Telcomm);
			log.info("setAfterSuccessQuery..");
			simpleTemplate.setAfterSuccessQuery(new AfterSuccessQuery() {

				public void execute(MVHImpl result) {
					itrN024Dao.deleteAll();

					t.setHEADER(result.getValueByFieldName("HEADER"));
					t.setSEQ(result.getValueByFieldName("SEQ"));
					t.setDATE(result.getValueByFieldName("DATE"));
					t.setTIME(result.getValueByFieldName("TIME"));
					t.setCOUNT(result.getValueByFieldName("COUNT"));
				}

			});

			log.info("setEachRowCallback..");
			simpleTemplate.setEachRowCallback(new EachRowCallback() {

				public void current(Row row) {
					// ITRN021 t = new ITRN021();

					// ---------------TODO table的自動編號還沒改好,改好的話這區塊刪掉------------------------------
					Integer recno_id = new Integer(recno_idBuff.toString()) + 1;
					recno_idBuff.setLength(0);
					recno_idBuff.append(recno_id);

					String reconid = new String();
					reconid = "00" + recno_id;
					t.setRECNO(StrUtils.right(reconid, 2));
					log.info("reconid = {}", StrUtils.right(reconid, 2));
					// ---------------------------------------------------------------------------------

					t.setCOLOR(row.getValue("COLOR"));
					t.setCRYNAME(row.getValue("CRYNAME"));
					t.setCRY(row.getValue("CRY"));
					String itrstr = row.getValue("ITRX");
					String[] itr = new String[2];
					log.trace("itrstr >> {} |", itrstr);
					log.trace("itr.length >> {} ", itr.length);
					for (int i = 0; i < itr.length; i++) {
						itr[i] = itrstr.substring(i * 7, 7 + i * 7);
						log.trace("itr[{}]={} |", i, itr[i]);
					}
					t.setITR1(itr[0]);
					t.setITR2(itr[1]);
					t.setFILL(row.getValue("FILL"));
					log.info("ITRN024 Save >>{}", t.toString());

					// TODO 如果table還沒改好
					itrN024Dao.insert(t);
					// TODO 如果table已改好
					// itrN024Dao.save(t);
				}
			});

			simpleTemplate.setExceptionHandler(new ExceptionHandler() {

				public void handle(Row row, Exception e) {

					log.error("無法更新外幣放款利率查詢.", e);

				}

			});
			BatchCounter counter = simpleTemplate.execute(params);
		} catch (TopMessageException e) {
			batchresult.setSuccess(false);
			data.put("TOPMSG", e.getMsgcode() + "  " + e.getMsgout());
			log.error("TopMessageException  !!", e);
		} catch (UncheckedException e) {
			batchresult.setSuccess(false);
			data.put("TOPMSG", "UncheckedException 執行有誤 !!");
			log.error("UncheckedException 執行有誤 !!", e);
		}

		try {
			if (batchresult.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName, batchresult.getData());
			}
		} catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}

		return batchresult;

	}


	public MVH query(Map<String, String> params) {
		List<ITRN024> qresult = itrN024Dao.getAll();
		addItrCount();
		MVHImpl mvh = new DBResult(qresult);
		// toHtml(mvh,"c:\\N024.html");
		return mvh;
	}

	private void addItrCount() {
		Date d = new Date();
		String today = DateTimeUtils.getCDateShort(d);
		String time = DateTimeUtils.getTimeShort(d);
		ITRCOUNT it = null;
		int iCount = 1;// 若尚未有點選過，第一次寫入的次數值為一
		try {
			it = itrCountDao.findById("N024");
		} catch (ObjectNotFoundException e) {
			it = null;
		}
		if (it != null) {
			String count = it.getCOUNT();
			if (!"".equals(count) || count != null) {
				iCount = Integer.parseInt(count);// 把原本的次數找出來
				iCount++;// 原本的次數加一;
				it.setDATE(today);
				it.setTIME(time);
				it.setCOUNT(Integer.toString(iCount));
			}
		} else {
			it = new ITRCOUNT();
			it.setHEADER("N024");
			it.setDATE(today);
			it.setTIME(time);
			it.setCOUNT("1");

		}
		itrCountDao.save(it);
	}

}
