package fstop.services.batch;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.TelcommResult;
import fstop.notifier.NotifyAngent;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.SysParamDataDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.SYSPARAMDATA;
import fstop.orm.po.TXNUSER;
import fstop.services.batch.annotation.Batch;
import fstop.telcomm.TelCommExec;
import fstop.util.CustomProperties;
import fstop.util.JSONUtils;
import fstop.util.SpringBeanFactory;
import lombok.extern.slf4j.Slf4j;

/**
 * 用戶通知訊息
 * 
 * @author Owner 16:53:00 自動執行 寄件者:景森
 * 
 */
@Slf4j
@RestController
@Batch(id = "batch.nd01", name = "定期性到期通知", description = "用戶通知訊息")
public class ND01 implements BatchExecute {
	// private log log = log.getlog(getClass());

	private CustomProperties realValue = new CustomProperties();

	private DecimalFormat fmt = new DecimalFormat("#0");

	@Autowired
	@Qualifier("nd01Telcomm")
	private TelCommExec nd01Telcomm;

	@Autowired
	private TxnUserDao txnUserDao;

	@Value("${prodf_path}")
	String profpath;

	// private PathSetting pathSetting;
	@Value("${batch.otherfile.path:}")
	private String pathname;

	@Autowired
	private SysParamDataDao sysParamDataDao;

	@Autowired
	private CheckServerHealth checkserverhealth;

	@Autowired
	private SysBatchDao sysBatchDao;

	public ND01() {
	}

	/**
	 * args[0] method name
	 */
	@RequestMapping(value = "/batch/nd01", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {
		log.info("batch ND01...");

		List<String> safeArgs = ESAPIUtil.validStrList(args);
		int totalcnt = 0, failcnt = 0, okcnt = 0;

		BatchResult batchresult = new BatchResult();
		Map<String, Object> data = new HashMap();
		batchresult.setBatchName(getClass().getSimpleName());
		batchresult.setSuccess(true);
		BatchCounter bcnt = new BatchCounter();

		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}

		Hashtable params = new Hashtable();
		Hashtable greetypetext = new Hashtable();
		Hashtable endtypetext = new Hashtable();
		Hashtable neednotify = new Hashtable();
		Map varmail = new HashMap();
		String mailadd = new String();
		String item = new String();
		String FullFileName = new String();

		List<TXNUSER> listtxnUser;
		int Iseqno = 1;
		String seqno = new String();

		seqno = String.valueOf(Iseqno);
		log.debug("seqno = " + seqno);
		params.put("SEQ", seqno);

		try {
			realValue.load(new InputStreamReader(new FileInputStream(profpath + "greetype.properties"), "UTF8"));
		} catch (Exception e) {
			// throw new fstop.model.ModelException("載入 FieldTranslator Properties 錯誤 !",
			// e);
			data.put("Error", "載入 Properties 錯誤");
			batchresult.setSuccess(false);
			batchresult.setData(data);
			log.error("載入Greetype Properties 錯誤", e);
			try {
				sysBatchDao.finish_FAIL2(batchName, batchresult.getData());
			} catch (Exception e2) {
				log.error("sysBatchDao.finish error >> {} ", e2.getMessage());
			}
			return batchresult;
		}
		try {
			do {
				varmail.clear();
				varmail.put("REPMEMO", "台企銀EMAIL通知");
				TelcommResult result = nd01Telcomm.query(params);
				String cusidn = result.getValueByFieldName("CUSIDN");
				if ((cusidn == null || cusidn.length() == 0) && result.getValueByFieldName("SEQ").equals("9999999")
						&& result.getValueByFieldName("TYPE").length() == 0) {
					log.debug("SEQ = 9999999 failcnt = " + failcnt + " okcnt = " + okcnt + " totalcnt = " + totalcnt);
					bcnt.setFailCount(failcnt);
					bcnt.setSuccessCount(okcnt);
					bcnt.setTotalCount(totalcnt);
					break;
				}
				if (!result.getValueByFieldName("TYPE").equals("01")) {
					exceptnotice(result);
					Iseqno++;
					seqno = String.valueOf(Iseqno);
					log.debug("seqno = " + seqno);
					params.put("SEQ", seqno);
					failcnt++;
					totalcnt++;
					continue;
				}
				String greetype = result.getValueByFieldName("GREETYPE");
				String name = null;
				String tail = null;
				if (greetype.substring(0, 3).equals("D06"))
					name = result.getValueByFieldName("NAME");
				else
					name = result.getValueByFieldName("NAME") + "，您好：";
				log.debug("name = " + name);
				greetypetext = GetGreeTypeText(greetype);
				if (greetypetext == null) {
					exceptnotice(cusidn, greetype);
					Iseqno++;
					seqno = String.valueOf(Iseqno);
					log.debug("seqno = " + seqno);
					params.put("SEQ", seqno);
					failcnt++;
					totalcnt++;
					continue;
				}
				try {
					log.debug("cusid = " + cusidn);
					listtxnUser = txnUserDao.findByUserId(cusidn);
					log.debug("listtxnUser.size() = " + listtxnUser.size());
					if (listtxnUser == null || listtxnUser.size() == 0) {
						Iseqno++;
						seqno = String.valueOf(Iseqno);
						log.debug("seqno = " + seqno);
						params.put("SEQ", seqno);
						failcnt++;
						totalcnt++;
						continue;
					}
					TXNUSER txnUser = listtxnUser.get(0);
					mailadd = txnUser.getDPMYEMAIL();
					log.debug(ESAPIUtil.vaildLog("mailadd from db = " + mailadd));
					log.debug(ESAPIUtil.vaildLog("txnUser.getDPNOTIFY() = " + txnUser.getDPNOTIFY()));
					neednotify.clear();
					neednotify = GetNotify(txnUser.getDPNOTIFY());
					if (neednotify == null) {
						Iseqno++;
						seqno = String.valueOf(Iseqno);
						log.debug("seqno = " + seqno);
						params.put("SEQ", seqno);
						continue;
					}
				} catch (Exception e) {
					log.error("get user id error = " + e.getMessage());
				}

				try {
					log.debug("ITEM = " + greetypetext.get("ITEM"));
					item = (String) neednotify.get(greetypetext.get("ITEM"));
					log.debug("ITEM = " + item);
					log.debug(ESAPIUtil.vaildLog("mailadd = " + mailadd));
					String newrepgreetype = new String();
					if (item != null && item.length() != 0 && mailadd != null
							|| greetypetext.get("ITEM").equals("98")) {
						log.debug("REPMEMO = " + greetypetext.get("GREETYPE"));
						varmail.put("REPMEMO", greetypetext.get("GREETYPE"));
						String data1 = result.getValueByFieldName("DATA1");
						String data2 = result.getValueByFieldName("DATA2");
						String data3 = result.getValueByFieldName("DATA3");
						String data4 = result.getValueByFieldName("DATA4");
						String data5 = result.getValueByFieldName("DATA5");
						String data6 = result.getValueByFieldName("DATA6");
						String data7 = result.getValueByFieldName("DATA7");
						String endtype = result.getValueByFieldName("ENDTYPE");

						if (endtype.equals("D0611"))
							tail = "您的憑證即將到期，到期後便無法使用，請辦理憑證更新如下：<ol><li>請於憑證到期前一個月內登入本行憑證註冊中心(https://portal.tbb.com.tw)辦理憑證更新。更新後之憑證將自原到期日起算，有效期間1年。</li><li>辦理憑證更新時，本行將於您指定之扣帳帳號扣取憑證費用(扣帳失敗則無法辦理更新)。</li><li>若您超逾憑證到期日未於線上辦理更新時，需攜帶身分證明文件、存摺、存款印鑑及載具親赴原申請憑證之分行辦理申請。</li><li>相關憑證申請下載操作流程，您可登入本【憑證註冊中心->線上說明->憑證作業流程】查詢。</li></ol>";
						log.debug("endtype = " + endtype);
						endtypetext = GetGreeTypeText(endtype);
						String repgreetype = (String) greetypetext.get("TEXT");
						log.debug("greetype = " + greetype + "| len = " + greetype.length());
						if (greetype.equals("D0101") || greetype.equals("D0301") || greetype.equals("D0302")
								|| greetype.equals("D0303")) {
							log.debug("result.getValueByFieldName(DATA7) = "
									+ result.getValueByFieldName("DATA7").trim());
							newrepgreetype = repgreetype.replaceAll("%BHNAME%",
									result.getValueByFieldName("DATA7").trim());
							if (greetype.equals("D0303")) {
								log.debug("data1 = " + data1);
								String years = data1.substring(7, 10);
								log.debug("years = " + years);
								String mons = data1.substring(11, 13);
								log.debug("mons = " + mons);
								newrepgreetype = newrepgreetype.replaceAll("%YEAR%", years.trim());
								newrepgreetype = newrepgreetype.replaceAll("%MONTH%", mons.trim());
								data1 = " ";
							}
						}
						log.debug("repgreetype = " + repgreetype);
						log.debug("newrepgreetype = " + newrepgreetype);
						varmail.put("REPGREETYPETEXT", newrepgreetype);
						varmail.put("REPNAME", name);
						if (data1 == null || data1.length() == 0)
							varmail.put("REPDATA1", " ");
						else
							varmail.put("REPDATA1", data1);
						if (data2 == null || data2.length() == 0)
							varmail.put("REPDATA2", " ");
						else
							varmail.put("REPDATA2", data2);
						if (data3 == null || data3.length() == 0)
							varmail.put("REPDATA3", " ");
						else
							varmail.put("REPDATA3", data3);
						if (data4 == null || data4.length() == 0)
							varmail.put("REPDATA4", " ");
						else
							varmail.put("REPDATA4", data4);
						if (data5 == null || data5.length() == 0)
							varmail.put("REPDATA5", " ");
						else
							varmail.put("REPDATA5", data5);
						if (data6 == null || data6.length() == 0)
							varmail.put("REPDATA6", " ");
						else
							varmail.put("REPDATA6", data6);
						if (data7 == null || data7.length() == 0 || greetype.equals("D0101") || greetype.equals("D0301")
								|| greetype.equals("D0302") || greetype.equals("D0303"))
							varmail.put("REPDATA7", " ");
						else
							varmail.put("REPDATA7", data7);
						if (tail == null || tail.length() == 0)
							varmail.put("REPTAIL", " ");
						else
							varmail.put("REPTAIL", tail);
						varmail.put("REPENDTYPETEXT", endtypetext.get("TEXT"));
						log.debug("endtypetext.get(TEXT) = " + endtypetext.get("TEXT"));
						boolean isSendSuccess = NotifyAngent.sendNotice("ND01", varmail, mailadd);
						if (!isSendSuccess) {
							log.error(ESAPIUtil.vaildLog("發送 Email 失敗.(" + mailadd + ")"));
							failcnt++;
						} else {
							log.debug(ESAPIUtil.vaildLog("發送 Email 成功.(" + mailadd + ")"));
							okcnt++;
						}
						totalcnt++;
					} else {
						failcnt++;
						totalcnt++;
					}
				} // try 2
				catch (Exception e) {
					log.debug("FAIL failcnt = " + failcnt + " totalcnt = " + totalcnt);
					failcnt++;
					totalcnt++;

				}

				if (result.getValueByFieldName("SEQ").equals("9999999")) {
					log.debug("SEQ = 9999999 failcnt = " + failcnt + " okcnt = " + okcnt + " totalcnt = " + totalcnt);

					bcnt.setFailCount(failcnt);
					bcnt.setSuccessCount(okcnt);
					bcnt.setTotalCount(totalcnt);

					break;
				}
				Iseqno++;
				seqno = String.valueOf(Iseqno);
				log.debug("seqno = " + seqno);
				params.put("SEQ", seqno);
			} while (true);
		} catch (TopMessageException e) {
			batchresult.setSuccess(false);
			data.put("TOPMSG", e.getMsgcode() + "  " + e.getMsgout());
			log.error("TopMessageException  !!", e);
		} catch (Exception e) {
			batchresult.setSuccess(false);
			data.put("Error", e.getMessage());
			log.error("Exception !!", e);
		}

		log.debug("AFTER failcnt = " + failcnt);
		log.debug("AFTER okcnt = " + okcnt);
		log.debug("AFTER totalcnt = " + totalcnt);

		data.put("COUNTER", bcnt);
		batchresult.setData(data);
		
		try {
			if (batchresult.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName,batchresult.getData());
			}
		}catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}


		return batchresult;

	}

	Hashtable GetGreeTypeText(String result) {
		Hashtable HResult = new Hashtable();
		try {
			String GetItem = result.trim() + ".item";
			log.debug("GetItem = " + GetItem);
			String item = realValue.getProperty(GetItem);
			HResult.put("ITEM", item);
		} catch (Exception e) {
			log.error("realValue.getProperty GetItem error");
			HResult.put("ITEM", "0");
			HResult.put("GREETYPE", "台企銀EMAIL通知");
			HResult.put("TEXT", " ");
			return (null);
		}
		String greetype = realValue.getProperty(result.trim() + ".greetype");
		log.debug("greetype = " + greetype);
		String text = realValue.getProperty(result.trim() + ".text");
		log.debug("text = " + text);
		HResult.put("GREETYPE", greetype);
		HResult.put("TEXT", text);
		return (HResult);
	}

	Hashtable GetNotify(String notify) {
		Hashtable HNotify = new Hashtable();
		String Notifyitem = new String();
		String Notifytext = new String();

		StringTokenizer st = new StringTokenizer(notify, ",");
		while (st.hasMoreTokens()) {
			Notifyitem = st.nextToken().trim();
			Notifytext = Notifyitem;
			HNotify.put(Notifyitem, Notifytext);
		}
		return (HNotify);
	}

	int SendAllMail(TelcommResult result, Hashtable rowcnt) {
		Integer totalcnt = 0, failcnt = 0, okcnt = 0;
		Map varmail = new HashMap();
		List<TXNUSER> listtxnUser;
		Hashtable endtypetext;

		// listtxnUser = txnUserDao.getAll();
		listtxnUser = txnUserDao.getEmailUser();
		for (int i = 0; i < listtxnUser.size(); i++) {
			endtypetext = null;
			TXNUSER txnUser = listtxnUser.get(i);
			String mailadd = txnUser.getDPMYEMAIL();
			if (mailadd == null || mailadd.length() == 0) {
				failcnt++;
				totalcnt++;
				continue;
			}
			String name = result.getValueByFieldName("NAME");
			String tail = null;
			String greetype = result.getValueByFieldName("GREETYPE");
			log.debug(ESAPIUtil.vaildLog("cusid = " + txnUser.getDPSUERID()));
			log.debug("greetype = " + greetype);
			Hashtable greetypetext = GetGreeTypeText(greetype);

			String data1 = result.getValueByFieldName("DATA1");
			log.debug("data1 = " + data1);
			String data2 = result.getValueByFieldName("DATA2");
			log.debug("data2 = " + data2);
			String data3 = result.getValueByFieldName("DATA3");
			log.debug("data3 = " + data3);
			String data4 = result.getValueByFieldName("DATA4");
			log.debug("data4 = " + data4);
			String data5 = result.getValueByFieldName("DATA5");
			log.debug("data5 = " + data5);
			String data6 = result.getValueByFieldName("DATA6");
			log.debug("data6 = " + data6);
			String data7 = result.getValueByFieldName("DATA7");
			log.debug("data7 = " + data7);
			String endtype = result.getValueByFieldName("ENDTYPE");
			if (endtype != null && endtype.length() > 0) {
				endtypetext = GetGreeTypeText(endtype);
				if (endtype.equals("D0611"))
					tail = "您的憑證即將到期，到期後便無法使用，請辦理憑證更新如下：<ol><li>請於憑證到期前一個月內登入本行憑證註冊中心(https://portal.tbb.com.tw)辦理憑證更新。更新後之憑證將自原到期日起算，有效期間1年。</li><li>辦理憑證更新時，本行將於您指定之扣帳帳號扣取憑證費用(扣帳失敗則無法辦理更新)。</li><li>若您超逾憑證到期日未於線上辦理更新時，需攜帶身分證明文件、存摺、存款印鑑及載具親赴原申請憑證之分行辦理申請。</li><li>相關憑證申請下載操作流程，您可登入本【憑證註冊中心->線上說明->憑證作業流程】查詢。</li></ol>";
			}
			varmail.put("REPMEMO", greetypetext.get("GREETYPE"));
			log.debug("REPMEMO = " + varmail.get("REPMEMO"));
			varmail.put("REPNAME", name);
			varmail.put("REPGREETYPETEXT", greetypetext.get("TEXT"));
			log.debug("REPGREETYPETEXT = " + varmail.get("REPGREETYPETEXT"));

			if (data1 == null || data1.length() == 0)
				varmail.put("REPDATA1", " ");
			else
				varmail.put("REPDATA1", data1);
			if (data2 == null || data2.length() == 0)
				varmail.put("REPDATA2", " ");
			else
				varmail.put("REPDATA2", data2);
			if (data3 == null || data3.length() == 0)
				varmail.put("REPDATA3", " ");
			else
				varmail.put("REPDATA3", data3);
			if (data4 == null || data4.length() == 0)
				varmail.put("REPDATA4", " ");
			else
				varmail.put("REPDATA4", data4);
			if (data5 == null || data5.length() == 0)
				varmail.put("REPDATA5", " ");
			else
				varmail.put("REPDATA5", data5);
			if (data6 == null || data6.length() == 0)
				varmail.put("REPDATA6", " ");
			else
				varmail.put("REPDATA6", data6);
			if (data7 == null || data7.length() == 0)
				varmail.put("REPDATA7", " ");
			else
				varmail.put("REPDATA7", data7);
			if (tail == null || tail.length() == 0)
				varmail.put("REPTAIL", " ");
			else
				varmail.put("REPTAIL", tail);
			if (endtypetext == null || endtypetext.get("TEXT") == null)
				varmail.put("REPENDTYPETEXT", " ");
			else
				varmail.put("REPENDTYPETEXT", endtypetext.get("TEXT"));
			log.debug("REPENDTYPETEXT = " + varmail.get("REPENDTYPETEXT"));
			boolean isSendSuccess = NotifyAngent.sendNotice("ND01", varmail, mailadd);
			if (!isSendSuccess) {
				log.error(ESAPIUtil.vaildLog("發送 Email 失敗.(" + mailadd + ")"));
				failcnt++;

			} else {
				log.debug(ESAPIUtil.vaildLog("發送 Email 成功.(" + mailadd + ")"));
				okcnt++;
			}
			totalcnt++;
		}
		rowcnt.put("OKCNT", okcnt);
		rowcnt.put("FAILCNT", failcnt);
		rowcnt.put("TOTALCNT", totalcnt);
		return (0);
	}

	public int exceptnotice(TelcommResult result) {
		List<TXNUSER> listtxnUser;
		String cusidn = result.getValueByFieldName("CUSIDN");

		log.debug(ESAPIUtil.vaildLog("cusid = " + cusidn));
		// CheckServerHealth checkserverhealth =
		// (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
		SYSPARAMDATA po = null;
		try {
			po = sysParamDataDao.findById("NBSYS");
		} catch (Exception e1) {
			log.error("Query NBSYS table error" + e1);
			return (1);
		}

		String emailap = po.getADAPMAIL();
		String type = result.getValueByFieldName("TYPE");
		String mailtext = "";
		if (cusidn == null || cusidn.equals(""))
			mailtext = "TYPE : " + type + " 不存在";
		else
			mailtext = "身份證:" + cusidn + " , TYPE : " + type + " 不存在";
		checkserverhealth.sendUserNotice(mailtext, emailap.trim(), "batch");

		return (0);
	}

	public int exceptnotice(String cusidn, String greetype) {
		log.debug("cusid = " + cusidn);
		// CheckServerHealth checkserverhealth =
		// (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
		SYSPARAMDATA po = null;
		String emailap = null;
		try {
			po = sysParamDataDao.findById("NBSYS");
			emailap = po.getADAPMAIL();
		} catch (Exception e1) {
			log.error("Query NBSYS table error" + e1);
			return (1);
		}

		String mailtext = "身份證:" + cusidn + " , GREETYPE : " + greetype + " 不存在";
		checkserverhealth.sendUserNotice(mailtext, emailap.trim(), "batch");

		return (0);
	}
}
