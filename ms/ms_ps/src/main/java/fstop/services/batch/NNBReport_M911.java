package fstop.services.batch;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.model.Rows;
import fstop.orm.dao.Mb3SysOpDao;
import fstop.orm.dao.Nb3SysOpDao;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.TxnLogDao;
import fstop.orm.oao.Old_TxnLogDao;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import fstop.util.FieldMapping;
import fstop.util.FtpBase64;
import fstop.util.FtpPathProperties;
import lombok.extern.slf4j.Slf4j;

/**
 * 每月產生客戶承作網路銀行交易異常情形表"至 /TBB/nnb/data/NBCCM911 並把檔案拋至報表系統
 * 
 * @author Owner 每頁45筆
 */
@Slf4j
@RestController
@Batch(id = "batch.NNBReport_M911", name = "客戶承作網路銀行交易異常情形表", description = "每月產生客戶承作網路銀行交易異常情形表\"至 /TBB/nnb/data/NBCCM911 並把檔案拋至報表系統")
public class NNBReport_M911 implements BatchExecute {

	@Autowired
	private TxnLogDao txnLogDao;
	
	@Autowired
	private Nb3SysOpDao nb3SysOpDao;
	
	@Autowired
	private Mb3SysOpDao mb3SysOpDao;

	@Value("${prodf_path}")
	private String profpath;

	@Value("${batch.localfile.path:}")
	private String path;

	@Value("${reportn.ftp.ip}")
	private String reportn_ftp_ip;

	@Autowired
	private Old_TxnLogDao old_TxnLogDao;

	@Value("${isParallelCheck}")
	private String isParallelCheck;

	@Value("${doFTP}")
	private String doFTP;

	@Autowired
	CheckServerHealth checkserverhealth;

	@Autowired
	FieldMapping fieldMapping;

	@Autowired
	private SysBatchDao sysBatchDao;

	@RequestMapping(value = "/batch/nnbReport_M911", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {

		log.info("batch NNBReport_M911...");

		BatchResult batchresult = new BatchResult();
		Map<String, Object> data = new LinkedHashMap();
		batchresult.setBatchName(getClass().getSimpleName());
		batchresult.setSuccess(true);

		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}

		List<String> safeArgs = ESAPIUtil.validStrList(args);
		log.debug("網銀交易明細資料CCM911  ... ");
		int failcnt = 0, okcnt = 0, totalcnt = 0;
		Map rowcnt = new HashMap();
		// start Update 20180905
		log.debug(ESAPIUtil.vaildLog("args size:" + safeArgs.size()));
		String yyy = "";
		String mm = "";
		String lastmonth = "";
		if (safeArgs.size() == 1) {// 當批次有帶參數(帶參數是為了要產出特定月份的報表檔)
			yyy = safeArgs.get(0).substring(0, 3);// 民國年
			mm = safeArgs.get(0).substring(3);// 月份
		} else { // 批次未帶參數(未帶參數是要產出上個月的報表檔)
			Date d = new Date();
			java.util.Calendar calendar = java.util.Calendar.getInstance();
			calendar.setTime(d);
			calendar.add(calendar.MONTH, -1);
			d = calendar.getTime();
			lastmonth = DateTimeUtils.getDateShort(d);
			log.debug(ESAPIUtil.vaildLog("lastmonth not substring : " + lastmonth));
			yyy = String.valueOf(Integer.parseInt(lastmonth.substring(0, 4)) - 1911);
			mm = lastmonth.substring(4, 6);
		}
		log.debug(ESAPIUtil.vaildLog("yyy:" + yyy + " mm:" + mm));
		// end
		try {
			// start Update 20180905
			// exec(rowcnt);
			exec(rowcnt, yyy, mm);
			// end
			failcnt = ((Integer) rowcnt.get("FAILCNT")).intValue();
			okcnt = ((Integer) rowcnt.get("OKCNT")).intValue();
			totalcnt = ((Integer) rowcnt.get("TOTALCNT")).intValue();
			log.debug(ESAPIUtil.vaildLog("failcnt = " + failcnt));
			log.debug(ESAPIUtil.vaildLog("okcnt = " + okcnt));
			log.debug(ESAPIUtil.vaildLog("totalcnt = " + totalcnt));
		} catch (Exception e) {
			log.error("執行 Batch 有誤 !", e);
			data.put("Error", e.getMessage());
			batchresult.setSuccess(false);
		}

		BatchCounter bcnt = new BatchCounter();
		bcnt.setFailCount(failcnt);
		bcnt.setSuccessCount(okcnt);
		bcnt.setTotalCount(totalcnt);

		data.put("COUNTER", bcnt);

		batchresult.setData(data);

		log.debug("NNBReport_M911 - End execute ");

		try {
			if (batchresult.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName, batchresult.getData());
			}
		} catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}
		return batchresult;
	}

	private void exec(Map rowcnt, String threeyear, String twomonth) throws IOException {
		Integer totalcnt = 0, failcnt = 0, okcnt = 0;
		Date d = new Date();
		java.util.Calendar calendar = java.util.Calendar.getInstance();
		calendar.setTime(d);
		String nowdate = DateTimeUtils.getDateShort(d);
		String printdate = String.valueOf(Integer.parseInt(nowdate.substring(0, 4)) - 1911) + "/"
				+ nowdate.substring(4, 6) + "/" + nowdate.substring(6, 8);
		calendar.add(calendar.MONTH, -1);
		d = calendar.getTime();
		// start Update 20180904
		// String lastmonth= DateTimeUtils.getDateShort(d);
		// lastmonth = lastmonth.substring(0, 6)+"%";
		// String year=lastmonth.substring(0,4);
		// String mon=lastmonth.substring(4,6);
		String lastmonth = String.valueOf(Integer.parseInt(threeyear) + 1911) + twomonth;
		lastmonth = lastmonth + "%";
		String year = String.valueOf(Integer.parseInt(threeyear) + 1911);
		String mon = twomonth;
		log.debug(ESAPIUtil.vaildLog("year:" + year + " mon:" + mon));
		// end 20180904
		String sEndDay = "";
		if (mon.equals("01") || mon.equals("03") || mon.equals("05") || mon.equals("07") || mon.equals("08")
				|| mon.equals("10") || mon.equals("12")) {
			sEndDay = "31";
		} else if (mon.equals("04") || mon.equals("06") || mon.equals("09") || mon.equals("11")) {
			sEndDay = "30";
		} else {
			if (mon.equals("02") && (Integer.parseInt(year) % 4 == 0) && (Integer.parseInt(year) % 100 != 0)) {
				sEndDay = "29";
			} else if (mon.equals("02")) {
				sEndDay = "28";
			}
		}
		// start Update 20180904
		// String Cyear=String.valueOf(Integer.parseInt(year)-1911);
		String Cyear = threeyear;
		log.debug(ESAPIUtil.vaildLog("Cyear = " + Cyear));
		// end
		log.debug(ESAPIUtil.vaildLog("lastmonth = " + lastmonth + ",mon=" + mon + ",year=" + year));
		FileReader fileStream = new FileReader(profpath + "ALL_BRH_List_CCM9H57.txt");
		// FileReader fileStream = new
		// FileReader("/TBB/nnb/cfg/ALL_BRH_List_CCM9H57.txt");//分行清單檔
		BufferedReader bufferedStream = new BufferedReader(fileStream);
		String data;
		int iADD = 0;
		int record = 0;
		do {
			data = ESAPIUtil.validInput(bufferedStream.readLine(), "GeneralString", true);
			// log.debug("BRH data:" + data);
			if (data == null) {
				break;
			}
			iADD++;// 計算分行總數
		} while (true);
		String[] result = new String[iADD];// 存放分行別
		// FileReader fileStream1 = new
		// FileReader("/TBB/nnb/cfg/ALL_BRH_List_CCM9H57.txt");//分行清單檔
		FileReader fileStream1 = new FileReader(profpath + "ALL_BRH_List_CCM9H57.txt");
		BufferedReader bufferedStream1 = new BufferedReader(fileStream1);
		do {
			data = ESAPIUtil.validInput(bufferedStream1.readLine(), "GeneralString", true);
			if (data == null) {
				break;
			}

			result[record] = data;// 存放分行別
			// log.debug("BRH LIST:"+result[record]);
			record++;
		} while (true);
		Date dstart = DateTimeUtils.getCDate2Date(Cyear + mon + "01");
		Date dend = DateTimeUtils.getCDate2Date(Cyear + mon + sEndDay);
		String stDate = DateTimeUtils.format("yyyyMMdd", dstart);// 查詢資料起日，須將日期轉成西元年
		String edDate = DateTimeUtils.format("yyyyMMdd", dend);// 查詢資料迄日，須將日期轉成西元年
		log.debug(ESAPIUtil.vaildLog("stDate:" + stDate + "  edDate:" + edDate));

		// String despath="/TBB/nnb/data/NBCCM911";//報表檔
		String despath = path + "NBCCM911";
		log.debug(ESAPIUtil.vaildLog("despath = " + despath));

		File tmpFile = new File(despath);
		tmpFile.setWritable(true, true);
		tmpFile.setReadable(true, true);
		tmpFile.setExecutable(true, true);
		tmpFile.delete();
		OutputStream fos = new FileOutputStream(tmpFile);
		BufferedWriter out = new BufferedWriter(new OutputStreamWriter(fos, "BIG5"));
		String pageHeader1 = "1 報表代號 : CCM911                     客戶承作網路銀行交易異常情形表                    週    期 / 保存年限：月報 / 1年\n";
		String pageHeader2 = "  資料日期 : " + Cyear + "/" + mon + "/01－" + Cyear + "/" + mon + "/" + sEndDay
				+ "             印表日期 : " + printdate + "                                        頁    次 : ";
		String pageHeader3 = "";
		String pageFooter = "9            	          經　辦 :                    　覆　核 :                    　主　管 :\n";
		String header1 = "  分行別  姓名          身分證字號   交易日期/時間   筆數  交易名稱                     通路         處理情形    \n";
		String header2 = "  ------  ----------    ----------   --------------  ----  --------------------         ----------   ---------                    \n";

		log.debug(ESAPIUtil.vaildLog("record:" + record));

		int queryNum = 0;// 計算每家分行有幾筆資料
		MVHImpl allresultxnlogtmvh = null, resultxnlogtmvh = null, resultxnusertmvh = null;
		MVHImpl allresultxnlogtmvhOri = null;
		MVHImpl alloldresultxnlogtmvh = null;
		
		// 20200511 同步
		MVHImpl isNotH57Datamvh = null;// 非H57分行資料
		MVHImpl allBranchDatamvh = null;// 查詢所有分行資料
		String logintype = "";// 通路代號
		String adopname = "";// 交易名稱
		String adtxacno = "";// N070轉出帳號
		// 找出全部資料
		allresultxnlogtmvhOri = (MVHImpl) txnLogDao.getTxnLogNum(stDate, edDate);
		
		//找出nb3sysop內的ADOPID及ADOPNAME
		Map <String , String > adopid_NB3_Map = nb3SysOpDao.m911AdopidMap();
		Map <String , String > adopid_MB3_Map = mb3SysOpDao.m911AdopidMap();
		Map <String , String > logintpMap = new HashMap<String , String>();
		logintpMap.put("NB", "一般網銀");
		logintpMap.put("MB", "行動銀行");
		logintpMap.put("QR", "行動銀行");
		
		//過濾非必要ADOPID
		Rows datarows = allresultxnlogtmvhOri.getOccurs();
		List<Row> listData = datarows.getRows();
		Rows newData = new Rows();
		for(Row eachData:listData) {
			//其中一個MAP有,加入此資料
			if(adopid_NB3_Map.containsKey(eachData.getValue("ADOPID"))||adopid_MB3_Map.containsKey(eachData.getValue("ADOPID"))) {
				newData.addRow(eachData);
			}
		}
		allresultxnlogtmvh = new MVHImpl(newData);
		
		if ("Y".equals(isParallelCheck)) {
			alloldresultxnlogtmvh = (MVHImpl) old_TxnLogDao.getTxnLogNum(stDate, edDate);
			log.debug("allresultxnlogtmvh size >> {}", allresultxnlogtmvh.getOccurs().getSize());
			log.debug("alloldresultxnlogtmvh size >> {}", alloldresultxnlogtmvh.getOccurs().getSize());

			allresultxnlogtmvhOri = combineMVH(allresultxnlogtmvh, alloldresultxnlogtmvh);
			//過濾非必要ADOPID
			Rows datarows2 = allresultxnlogtmvhOri.getOccurs();
			List<Row> listData2 = datarows2.getRows();
			Rows newData2 = new Rows();
			for(Row eachData2:listData2) {
				//其中一個MAP有,加入此資料
				if(adopid_NB3_Map.containsKey(eachData2.getValue("ADOPID"))||adopid_MB3_Map.containsKey(eachData2.getValue("ADOPID"))) {
					newData2.addRow(eachData2);
				}
			}
			allresultxnlogtmvh = new MVHImpl(newData2);
		}
		
		for (int i = 0; i < record; i++) { // 以分行依序查詢
			if (!result[i].equals("H57")) {
				isNotH57Datamvh = seperateList(allresultxnlogtmvh, result[i]);
				queryNum = isNotH57Datamvh.getValueOccurs("ADUSERID");

			} else { // 如歸戶行為空白時，歸類在H57
				allBranchDatamvh = seperateAllList(allresultxnlogtmvh);
				queryNum = allBranchDatamvh.getValueOccurs("ADUSERID");
			}
			log.debug(ESAPIUtil.vaildLog("queryNum:" + queryNum));

			Double totalpage = Math.ceil(((float) queryNum / (float) 37));// 計算每分行有幾頁資料
			String totalpage1 = totalpage.toString();
			log.debug(ESAPIUtil.vaildLog("totalpage1:" + totalpage1));
			pageHeader3 = "  需求單位 : " + result[i]
					+ "                                                                                          製表單位 : 資訊部\n\n\n\n";
			if (totalpage == 0)// 無筆數
			{
				out.write(pageHeader1);
				out.write(pageHeader2 + "1/1\n");// 顯示頁次
				out.write(pageHeader3);
				out.write(header1);
				out.write(header2);
				out.write(
						"                     無網銀交易紀錄                                                                      ");
				for (int k = 0; k < 37; k++) {
					out.write("\n");
				}

			} else {
				for (int l = 0; l < totalpage; l++) { // 需執行頁數
					// log.debug("pageHeader1 and pageHeader2 and pageHeader3 l:"+l);
					out.write(pageHeader1);
					out.write(pageHeader2 + (l + 1) + "/" + totalpage1 + "\n");// 顯示頁次
					out.write(pageHeader3);
					out.write(header1);
					out.write(header2);
					int counter = 0;

					// 計算報表每頁扣掉資料後還需換行次數，一頁最多37筆
					for (int j = l * 37; j < queryNum; j++) {
						log.debug(ESAPIUtil.vaildLog("ADBRANCHID:"
								+ isNotH57Datamvh.getValueByFieldName("ADBRANCHID", j + 1) + "   result:" + result[i]));
						if (isNotH57Datamvh.getValueByFieldName("ADBRANCHID", j + 1).equals(result[i])) {// 有分行資料就須寫入該分行清單內

							// logger.debug("ADOPID
							// if==>"+resultxnlogtmvh.getValueByFieldName("ADOPID",j+1)+"<==");
							if (isNotH57Datamvh.getValueByFieldName("LOGINTYPE", j + 1).trim().equals("")) {// 如logintype為空值，通路為一般網銀
								logintype = "NB";
							} else {
								logintype = isNotH57Datamvh.getValueByFieldName("LOGINTYPE", j + 1);
							}
							log.debug(ESAPIUtil.vaildLog("NOT H57 logintype:" + logintype));
							adopname = adopid_NB3_Map.get(isNotH57Datamvh.getValueByFieldName("ADOPID", j + 1))!=null?adopid_NB3_Map.get(isNotH57Datamvh.getValueByFieldName("ADOPID", j + 1)):adopid_MB3_Map.get(isNotH57Datamvh.getValueByFieldName("ADOPID", j + 1));
							String f = "  " + isNotH57Datamvh.getValueByFieldName("ADBRANCHID", j + 1) + "     "
									+ subStringName(isNotH57Datamvh.getValueByFieldName("DPUSERNAME", j + 1)) + "    "
									+ isNotH57Datamvh.getValueByFieldName("ADUSERID", j + 1) + "   "
									+ dateChgYYY(isNotH57Datamvh.getValueByFieldName("LASTDATE", j + 1)) + " "
									+ isNotH57Datamvh.getValueByFieldName("LTIME", j + 1) + "  "
									+ isNotH57Datamvh.getValueByFieldName("TIME", j + 1) + "     "
//									+ subStringTrans(this.getBeanMapValue("M911ADOPID",
//											isNotH57Datamvh.getValueByFieldName("ADOPID", j + 1)))
									+ subStringTrans(adopname)
									+ "         " + logintpMap.get(logintype) + "\n";// 分行別+姓名+身分證字號+日期+時間+交易名稱+通路+處理情形;

							if (counter == 38) {// 第38行就換頁
								break;
							}
							out.write(f);// 將資料寫進落地檔NBCCM911
							okcnt++;
							counter++;
							if ((queryNum % 37) != 0 && (j == queryNum - 1)) {
								int fill_space = 37 - (queryNum % 37);
								for (int k = 0; k < fill_space; k++) {
									out.write("\n");
								}
							}
						} else { // 報表內每家分行超過37行就換行
							log.debug(ESAPIUtil.vaildLog(
									"ADBRANCHID:" + allBranchDatamvh.getValueByFieldName("ADBRANCHID", j + 1)));
							// if(allBranchDatamvh.getValueByFieldName("ADBRANCHID",j+1).equals(""))
							// {//如歸戶行為空值，需歸類於H57

							// log.debug("ADOPID
							// else==>"+resultxnlogtmvh.getValueByFieldName("ADOPID",j+1)+"<==");
							if (allBranchDatamvh.getValueByFieldName("LOGINTYPE", j + 1).trim().equals("")) {//// 如logintype為空值，通路為一般網銀
								logintype = "NB";
							} else {
								logintype = allBranchDatamvh.getValueByFieldName("LOGINTYPE", j + 1);
							}
							adopname = adopid_NB3_Map.get(allBranchDatamvh.getValueByFieldName("ADOPID", j + 1))!=null?adopid_NB3_Map.get(allBranchDatamvh.getValueByFieldName("ADOPID", j + 1)):adopid_MB3_Map.get(allBranchDatamvh.getValueByFieldName("ADOPID", j + 1));
							log.debug(ESAPIUtil.vaildLog("IS H57 logintype:" + logintype));
							log.debug(ESAPIUtil.vaildLog(
									"IS H57 ADOPID:" + allBranchDatamvh.getValueByFieldName("ADOPID", j + 1)));

							if (allBranchDatamvh.getValueByFieldName("ADOPID", j + 1).equals("N070")) {
								adtxacno = allBranchDatamvh.getValueByFieldName("ADTXACNO", j + 1).substring(0, 3);// 取前三碼分行別
							} else {
								adtxacno = allBranchDatamvh.getValueByFieldName("ADBRANCHID", j + 1);
							}

							log.debug(ESAPIUtil.vaildLog("IS H57 adtxacno:" + adtxacno));
							String f = "  " + subStringBrans(adtxacno) + "  "
									+ subStringName(allBranchDatamvh.getValueByFieldName("DPUSERNAME", j + 1)) + "    "
									+ allBranchDatamvh.getValueByFieldName("ADUSERID", j + 1) + "   "
									+ dateChgYYY(allBranchDatamvh.getValueByFieldName("LASTDATE", j + 1)) + " "
									+ allBranchDatamvh.getValueByFieldName("LTIME", j + 1) + "  "
									+ allBranchDatamvh.getValueByFieldName("TIME", j + 1) + "     "
//									+ subStringTrans(this.getBeanMapValue("M911ADOPID",
//											allBranchDatamvh.getValueByFieldName("ADOPID", j + 1)))
									+ subStringTrans(adopname)
									+ "         " + logintpMap.get(logintype) + "\n";// 分行別+姓名+身分證字號+日期+時間+交易名稱+通路+處理情形;
							if (counter == 38) {// 第38行就換頁
								break;
							}
							out.write(f);// 將資料寫進落地檔NBCCM911
							okcnt++;
							counter++;
							if ((queryNum % 37) != 0 && (j == queryNum - 1)) {
								int fill_space = 37 - (queryNum % 37);
								for (int k = 0; k < fill_space; k++) {
									out.write("\n");// 寫入換行
								}
							}
							// }else{
							// counter++;
							// if(counter==38){//第38行就換頁
							// break;
							// }
							// if((queryNum % 37)!=0 && (j==queryNum-1))
							// {
							// int fill_space=37-(queryNum % 37);
							// for(int k=0;k<fill_space;k++) {
							// out.write("\n");//寫入換行
							// }
							// }
							// }

						}
					} // END for 每頁幾筆 loop
				} // END for loop
			} // END IF totalpage!=0
		} // END for loop
		try {
			out.flush();
			out.close();
			if ("Y".equals(doFTP)) {
				// FTP機制啟動拋至報表系統主機
				FtpPathProperties nrcpro = new FtpPathProperties();
				String srcpath = nrcpro.getSrcPath("nnbreport_m911");
				log.debug(ESAPIUtil.vaildLog("srcpath=" + srcpath));
				// String despath1=nrcpro.getDesPath("nnbreport_m902");
				// log.debug("despath = " + despath1);
				String despath2 = nrcpro.getDesPath("nnbreport1_m911");
				log.debug(ESAPIUtil.vaildLog("despath2=" + despath2));
				// String ipn = (String)setting.get("ftp.ipn");
				String ipn = reportn_ftp_ip;

				FtpBase64 ftpBase64 = new FtpBase64(ipn);
				int rc = ftpBase64.upload(despath2, path + "NBCCM911");
				if (rc != 0) {
					log.debug("FTP 失敗(fundreport 新主機)");
					// CheckServerHealth checkserverhealth =
					// (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
					checkserverhealth.sendUnknowNotice("FTP 失敗(fundreport 新主機)");
					log.debug("FTPSTATUS失敗");
				} else {
					log.debug("FTPSTATUS成功");
				}
			}
		} catch (Exception e) {
		}
		try {
			fos.close();
		} catch (Exception e) {
		}

		if (okcnt == 0) {
			rowcnt.put("OKCNT", okcnt);
			rowcnt.put("FAILCNT", failcnt);
			rowcnt.put("TOTALCNT", totalcnt);
			return;
		}
		rowcnt.put("OKCNT", okcnt/2);
		rowcnt.put("FAILCNT", failcnt);
		rowcnt.put("TOTALCNT", totalcnt);
		fileStream.close();
		bufferedStream.close();
		fileStream1.close();
		bufferedStream1.close();
	}

	// 擷取分行別長度
	public String subStringBrans(String brans) {

		brans = brans.replace((char) 12288, ' ');// 將全形空格unicode轉成半形空白再trim，因trim只能去掉的字元是unicode小於32
		log.debug(ESAPIUtil.vaildLog("replaceAll brans==>" + brans + "<=="));
		brans = brans.trim();// 先清空白
		log.debug(ESAPIUtil.vaildLog("trim brans==>" + brans + "<=="));
		if (!brans.equals("")) {// 分行別不為空值
			brans = brans.substring(0, 3);
		}
		log.debug(ESAPIUtil.vaildLog("before brans len==>" + brans.length() + "<=="));
		brans = leftSpace(brans, 6, "NUM");
		log.debug(ESAPIUtil.vaildLog("after brans==>" + brans + "<=="));

		return brans;
	}

	// 擷取名字長度
	public String subStringName(String name) {

		name = name.replace((char) 12288, ' ');// 將全形空格unicode轉成半形空白再trim，因trim只能去掉的字元是unicode小於32
		// log.debug("replaceAll name==>" + name+"<==");
		name = name.trim();// 先清空白
		// log.debug("trim name==>"+name+"<==");
		if (!name.equals("")) {// 姓名不為空值
			if (name.length() >= 5) {// 姓名超過五位只抓五位
				name = name.substring(0, 5);
			} else if (name.length() < 5) {// 姓名未超過五位，抓姓名總長度
				name = name.substring(0, name.length());
			}
		}
		// log.debug("before name len==>"+name.length()+"<==");
		name = leftSpace(name, 5, "STRING");
		// log.debug("after name==>"+name+"<==");

		return name;
	}

	// 擷取交易代號長度
	public String subStringTrans(String trans) {

		trans = trans.replace((char) 12288, ' ');// 將全形空格unicode轉成半形空白再trim，因trim只能去掉的字元是unicode小於32
		log.debug(ESAPIUtil.vaildLog("replaceAll trans==>" + trans + "<=="));
		trans = trans.trim();// 先清空白
		log.debug(ESAPIUtil.vaildLog("trim trans==>" + trans + "<=="));
		if (!trans.equals("")) {// 交易名稱不為空值
			if (trans.length() >= 10) {// 交易代號超過10位只抓10位
				trans = trans.substring(0, 10);
			}
		}
		log.debug(ESAPIUtil.vaildLog("before trans len==>" + trans.length() + "<=="));
		trans = convertToFullWidth(trans);
		trans = leftSpace(trans, 10, "STRING");// 參數1:欄位值 參數2:欄位總長度 參數3:欄位型態
		log.debug(ESAPIUtil.vaildLog("after trans==>" + trans + "<=="));

		return trans;
	}

	// 左靠右補空白
	public String leftSpace(String nameValue, int Strlen, String type) {// 參數1:值 參數2:欄位總長度 參數3:型態

		String space = "";
		// 補上不足的空白
		if (type.equals("STRING")) {// 字串型態
			for (int i = 0; i < Strlen - (nameValue.length()); i++) {// 上限長度-名字長度會剩多少空白
				space += (char) 12288;// 補全形空白
			}
		} else { // 數字型態
			for (int i = 0; i < Strlen - (nameValue.length()); i++) {// 上限長度-名字長度會剩多少空白
				space += (char) ' ';// 補半形空白
			}
		}
		return nameValue + space;
	}

	public String dateChgYYY(String yearmonda) {
		log.debug(ESAPIUtil.vaildLog("yearmonda==>" + yearmonda + "<=="));
		String yyymmdd = String.valueOf(Integer.parseInt(yearmonda.substring(0, 4)) - 1911) + "/"
				+ yearmonda.substring(4, 6) + "/" + yearmonda.substring(6);
		log.debug(ESAPIUtil.vaildLog("yyymmdd==>" + yyymmdd + "<=="));
		return yyymmdd;
	}

	/**
	 * 輸入beanId && beanMapKey Get beanMapValue
	 * 
	 * @param beanId
	 * @param beanMapKey
	 * @see spring-arg.xml
	 * @return get spring-arg.xml beanMapValue By beanId && beanMapKey, if any (or
	 *         empty String otherwise)
	 */
	public String getBeanMapValue(String beanId, String beanMapKey) {
		String getTypeBeanValue = "";
		try {
			getTypeBeanValue = CodeUtil.objectCovert(String.class,
					fieldMapping.getClass().getMethod("get" + beanId + beanMapKey.toUpperCase(), new Class[] {})
							.invoke(fieldMapping, new Object[] {}));
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return getTypeBeanValue;
	}

	private MVHImpl combineMVH(MVHImpl allresultxnlogtmvh, MVHImpl alloldresultxnlogtmvh) {
		Rows newRows = allresultxnlogtmvh.getOccurs();
		Rows oldRows = alloldresultxnlogtmvh.getOccurs();
		MVHImpl mvhresult = null;
		for (int i = 0; i < oldRows.getSize(); i++) {
			newRows.addRow(oldRows.getRow(i));
		}
		mvhresult = new MVHImpl(newRows);
		return mvhresult;
	}

	public MVHImpl seperateList(MVHImpl allList, String adbranchid) {
		MVHImpl mvhresult = null;
		Rows result = new Rows();
		Rows newrows = new Rows();
		result = allList.getOccurs();
		for (int i = 0; i < result.getSize(); i++) {
			if (adbranchid.equals(result.getRow(i).getValue("ADBRANCHID"))) {
				newrows.addRow(result.getRow(i));
			}
		}
		mvhresult = new MVHImpl(newrows);
		return mvhresult;
	}

	public MVHImpl seperateAllList(MVHImpl allList) {
		MVHImpl mvhresult = null;
		Rows result = new Rows();
		Rows newrows = new Rows();
		result = allList.getOccurs();
		for (int i = 0; i < result.getSize(); i++) {
			newrows.addRow(result.getRow(i));
		}
		mvhresult = new MVHImpl(newrows);
		return mvhresult;
	}
	
	public static String convertToFullWidth(String input) {
		if (input == null) {
			return null;
		}

		char[] charArray = input.toCharArray();
		for (int i = 0; i < charArray.length; i++) {
			int ic = (int) charArray[i];
			if (ic >= 33 && ic <= 126) {
				charArray[i] = (char) (ic + 65248);
			} else if (ic == 32) {
				charArray[i] = (char) 12288;

			}
		}
		return new String(charArray);
	}
}
