package fstop.services.batch;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.ESAPIUtil;

import fstop.notifier.NotifyAngent;
import fstop.orm.dao.AdmMailLogDao;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.TxnFxSchPayDao;
import fstop.orm.dao.TxnTwSchPayDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.ADMMAILCONTENT;
import fstop.orm.po.ADMMAILLOG;
import fstop.orm.po.TXNFXSCHPAY;
import fstop.orm.po.TXNRECMAILLOGRELATION;
import fstop.orm.po.TXNTWSCHPAY;
import fstop.orm.po.TXNUSER;
import fstop.services.NoticeInfo;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 扣帳到期通知withdraw
 * 
 * fxnotify
 * 
 * @author Owner 主旨：預約交易 項目:預約交易（轉帳繳費稅匯出匯款）即將扣帳通知 YYY/MM/DD 09:30:00
 */
@Slf4j
@RestController
@Batch(id = "batch.txnscheduleend", name = "扣帳到期通知", description = "預約交易（轉帳繳費稅匯出匯款）扣帳到期通知")
public class TxnScheduleEND implements BatchExecute {

	private DecimalFormat fmt = new DecimalFormat("#0");

	@Autowired
	private AdmMailLogDao admMailLogDao;

	@Autowired
	private TxnUserDao txnUserDao;

	@Autowired
	private TxnTwSchPayDao txnTwSchPayDao;

	@Autowired
	private TxnFxSchPayDao txnFxSchPayDao;

	@Autowired
	private SysBatchDao sysBatchDao;

	private TXNFXSCHPAY fx = new TXNFXSCHPAY();

	private TXNTWSCHPAY tw = new TXNTWSCHPAY();

	private TXNUSER txnuser = new TXNUSER();

	public TxnScheduleEND() {
	}

	/**
	 * args[0] method name
	 */
	@RequestMapping(value = "/batch/txnscheduleend", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {

		log.info("batch TxnScheduleEND...");

		BatchResult batchresult = new BatchResult();
		Map<String, Object> data = new LinkedHashMap();
		batchresult.setBatchName(getClass().getSimpleName());
		batchresult.setSuccess(true);

		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}

		List<String> safeArgs = ESAPIUtil.validStrList(args);
		List<TXNFXSCHPAY> txnfxschedule;
		List<TXNTWSCHPAY> txntwschedule;
		List<TXNUSER> listtxnUser;
		Map varmail = new HashMap();
		String mail = new String();
		String empno = new String();
		Date d = new Date();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Calendar c1 = Calendar.getInstance();
		log.debug("now date = " + sdf.format(d));
		c1.setTime(d);
		log.debug("Date is : " + sdf.format(c1.getTime()));
		// 暫時先固定值，之後再改成環境變數
		// String txdays=(String)setting.get("twdays");
		String txdays = "3";
		c1.add(Calendar.DATE, Integer.parseInt(txdays));
		String currdays = sdf.format(c1.getTime());
		log.debug("Date + 3 days is : " + sdf.format(c1.getTime()));
		txntwschedule = txnTwSchPayDao.findScheduleDueToEND(c1.getTime());
		log.debug("txntwschedule.size() = " + txntwschedule.size());
		for (int i = 0; i < txntwschedule.size(); i++) {
			varmail.clear();
			tw = txntwschedule.get(i);
			listtxnUser = txnUserDao.findByUserId(tw.getDPUSERID());
			log.debug("listtxnUser.size() = " + listtxnUser.size());
			if (listtxnUser == null || listtxnUser.size() == 0) {
				log.error(ESAPIUtil.vaildLog("User ID not found = " + tw.getDPUSERID()));
				continue;
			}
			txnuser = listtxnUser.get(0);
			String dpnotify = StrUtils.trim(txnuser.getDPNOTIFY());
			Set<String> dpnotifySet = StrUtils.splitToSet("\\s*,\\s*", dpnotify); // 1 : 轉帳/繳費稅/匯出匯款成功通知, 5
																					// 預約交易（轉帳繳費稅匯出匯款）結果通知
			log.error(ESAPIUtil.vaildLog("dpnotifySet >> " + dpnotifySet));

			boolean isUserSetTxnNotify = dpnotifySet.contains("20");
			if (isUserSetTxnNotify == false) {
				log.error("isUserSetTxnNotify id false");
				continue;
			}
			String email = txnuser.getDPMYEMAIL();
			if (email == null || email.length() == 0) {
				log.debug(ESAPIUtil.vaildLog(tw.getDPUSERID() + " email not found ."));
				continue;
			}

			String dptdate = tw.getDPTDATE();
			String year = dptdate.substring(0, 4);
			String month = dptdate.substring(4, 6);
			String day = dptdate.substring(6);
			log.debug(ESAPIUtil.vaildLog("dptdate ==" + dptdate));
			varmail.put("#YEAR", year);
			varmail.put("#MONTH", month);
			varmail.put("#DAY", day);

			boolean isSendSuccess = NotifyAngent.sendNotice("WITHDRAWENDNOTIFY", varmail, email);
			if (!isSendSuccess) {
				log.error(ESAPIUtil.vaildLog("發送 Email 失敗.(" + email + ")"));
			} else {
				log.debug(ESAPIUtil.vaildLog("發送 Email 成功.(" + email + ")"));
			}
		}
		c1 = Calendar.getInstance();
		log.debug("now date = " + sdf.format(d));
		c1.setTime(d);
		log.debug("Date is : " + sdf.format(c1.getTime()));

		// 暫時先固定值，之後再改成環境變數
		// txdays=(String)setting.get("fxdays");
		String fxdays = "3";
		c1.add(Calendar.DATE, Integer.parseInt(fxdays));
		currdays = sdf.format(c1.getTime());
		log.debug("Date + 3 days is : " + sdf.format(c1.getTime()));
		txnfxschedule = txnFxSchPayDao.findScheduleDueToEND(c1.getTime());
		log.debug("txnfxschedule.size() = " + txnfxschedule.size());
		for (int i = 0; i < txnfxschedule.size(); i++) {
			varmail.clear();
			fx = txnfxschedule.get(i);
			log.debug(ESAPIUtil.vaildLog("fx.getFXUSERID() = " + fx.getFXUSERID()));
			log.debug(ESAPIUtil.vaildLog("fx.getFXSCHID() = " + fx.getFXSCHID()));
			log.debug(ESAPIUtil.vaildLog("fx.getFXSVBH() = " + fx.getFXSVBH()));
			listtxnUser = txnUserDao.findByUserId(fx.getFXUSERID());
			log.trace("listtxnUser.size() = {}", listtxnUser.size());
			if (listtxnUser == null || listtxnUser.size() == 0) {
				log.error(ESAPIUtil.vaildLog("User ID not found = " + fx.getFXUSERID()));
				continue;
			}
			txnuser = listtxnUser.get(0);
			String dpnotify = StrUtils.trim(txnuser.getDPNOTIFY());
			Set<String> dpnotifySet = StrUtils.splitToSet("\\s*,\\s*", dpnotify);
			log.error(ESAPIUtil.vaildLog("dpnotifySet >> " + dpnotifySet));

			boolean isUserSetTxnNotify = dpnotifySet.contains("20");
			if (isUserSetTxnNotify == false) {
				log.error("isUserSetTxnNotify id false");
				continue;
			}
			String email = txnuser.getDPMYEMAIL();
			if (email == null || email.length() == 0) {
				log.error(ESAPIUtil.vaildLog(fx.getFXUSERID() + " email not found ."));
				continue;
			}
			String fxtdate = fx.getFXTDATE();
			String year = fxtdate.substring(0, 4);
			String month = fxtdate.substring(4, 6);
			String day = fxtdate.substring(6);
			log.debug(ESAPIUtil.vaildLog("fxtdate ==" + fxtdate));
			varmail.put("#YEAR", year);
			varmail.put("#MONTH", month);
			varmail.put("#DAY", day);

			boolean isSendSuccess = NotifyAngent.sendNotice("WITHDRAWENDNOTIFY", varmail, email);
			if (!isSendSuccess) {
				log.error(ESAPIUtil.vaildLog("發送 Email 失敗.(" + email + ")"));
			} else {
				log.debug(ESAPIUtil.vaildLog("發送 Email 成功.(" + email + ")"));
			}
		}
		Hashtable params = new Hashtable();
		// params.put("DATE", today);
		// params.put("TIME", time);

		data.put("COUNTER", new BatchCounter());
		batchresult.setData(data);

		try {
			// 新增SYSBATCH紀錄
			sysBatchDao.finish_OK(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.finish_OK error >> {} ", e.getMessage());
		}

		return batchresult;

	}

	public void writeSendNoticeLog(final NoticeInfo info, final String __record_adtxno, String subject, String msg,
			String status) {
		ADMMAILLOG maillog = info.getADMMAILLOG();
		maillog.setADSENDSTATUS(status);

		ADMMAILCONTENT content = new ADMMAILCONTENT();
		content.setADMAILCONTENT(msg);
		content.setADSUBJECT(subject);

		Date d = new Date();
		TXNRECMAILLOGRELATION relation = new TXNRECMAILLOGRELATION();
		relation.setADTXNO(__record_adtxno);
		relation.setLASTDATE(DateTimeUtils.format("yyyyMMdd", d));
		relation.setLASTTIME(DateTimeUtils.format("HHmmss", d));

		try {
			admMailLogDao.writeRecordNotice(maillog, relation);
		} catch (Exception ex) {
			log.error("無法寫入 MAILLOG.", ex);
		}
	}

}
