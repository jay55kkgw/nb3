package fstop.services.batch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.rest.model.CommonPools;
import com.netbank.util.ESAPIUtil;

import fstop.notifier.NotifyAngent;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.TXNUSER;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 基金預約開戶結果通知
 * @author Owner
 *
 */
@Slf4j
@RestController
@Batch(id="batch.c053", name = "基金預約開戶結果通知", description = "基金預約開戶結果通知")
public class C053 implements BatchExecute{

	@Value("${batch.hostfile.path:}")
	private String pathname;
	
	@Value("${host.hostdata.path}")
	private String hostpath;
	
	@Autowired
	private TxnUserDao txnUserDao;
	
	@Autowired
	private CommonPools commonPools;
	
	@Autowired
	private SysBatchDao sysBatchDao;
	
	@RequestMapping(value = "/batch/c053", method = {RequestMethod.POST},
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {
		
		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}
		
		List<String> safeArgs = ESAPIUtil.validStrList(args);
		Date d = new Date();
		String today = DateTimeUtils.getCDateShort(d);

		BatchResult batchresult = new BatchResult();
		
		log.debug("Batch_Execute_Date = " + today);
		
//		2019/10/09 修改成讀檔 
		String filePath = pathname + "C053F.TXT";
		log.debug("filePath >>{}", filePath);
		
		//For Error log
		String hostfilePath = hostpath + "C053F.TXT";
		
		batchresult = readFile(filePath,hostfilePath);
		
		try {
			if (batchresult.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName,batchresult.getData());
			}
		}catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}
		
		return batchresult;
	}
	
	private BatchResult readFile(String tempfile ,String hostpath) {
		
		BatchResult batchresult = new BatchResult();
		Map<String, Object> data = new HashMap();
		
		Map<String, Object> resultData = new LinkedHashMap();
		
		InputStreamReader in = null;
		BufferedReader bin = null;
		
		int totalcnt = 0, failcnt = 0, okcnt = 0, noneedcnt = 0, nousercnt = 0, cantfindmailcnt = 0;
		
		int i = 0;
		String email="";
		String StatusDESC="";
		
		try {
			File file= new File(tempfile);
			//Incorrect Permission Assignment For Critical Resources
			file.setWritable(true,true);
			file.setReadable(true, true);
			file.setExecutable(true,true);
			long timestamp = file.lastModified();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String fmtDate = sdf.format(timestamp);
			log.info("File's LastModify fmt >> {}",fmtDate);
			Date d = new Date();
			String today = DateTimeUtils.getDateShort(d);
			
			if(!today.equals(fmtDate)) {
				data.put("MSG", "C053F.TXT MODIFY TIME NOT TODAY , SKIP EXECUTE !");
				batchresult.setSuccess(true);
				batchresult.setBatchName(this.getClass().getSimpleName());
				batchresult.setData(data);
				return batchresult;
			}
			
			in = new InputStreamReader(new FileInputStream(tempfile), "BIG5");
			bin = new BufferedReader(in);
			
			if(!bin.ready()) {
				in.close();
                bin.close();
            	throw new Exception();
			}
			String subject;
			
			while ((subject = ESAPIUtil.validInput(bin.readLine(),"GeneralString", true)) != null && subject.length() != 0) {
				Map<String, String> rowData = getData(subject);
				totalcnt++;
				String RECODE=rowData.get("RECODE")==null ? "" : rowData.get("RECODE");
				email= rowData.get("EMAIL")==null ? "" : rowData.get("EMAIL");
				
				if(email.length()==0)
				{
					List<TXNUSER> listtxnUser= txnUserDao.findByUserId(rowData.get("CUSIDN"));
					log.trace("listtxnUser.size() = " + listtxnUser.size());
					if(listtxnUser==null || listtxnUser.size()==0)
					{
						email="";
						log.trace("CUSIDN 不存在");
						nousercnt++;
					}
					else
					{	
						TXNUSER txnuser=listtxnUser.get(0);            
						email=txnuser.getDPMYEMAIL();
						if(email==null || email.length()==0)
						{
							email="";
							log.trace("Email 不存在");
							cantfindmailcnt++;
						}					
					}
				}
				log.trace(ESAPIUtil.vaildLog(rowData.get("CUSIDN")+" RECODE:"+RECODE+" email:"+email));
				
				if(email!=null && email.length()>0)
				{	
					StringBuffer MailTextBuf=new StringBuffer();
					StatusDESC=RECODE.equals("01") ? "恭喜您完成本行『線上開立基金戶』，自即日起可透過網路銀行辦理基金交易服務。" : "『線上開立基金戶』<font color=red>啟用失敗</font>";
					MailTextBuf.append("<table cellSpacing=0 cellPadding=0 width=600 border=0 frame=border >").append("\n");
					MailTextBuf.append("<tbody>").append("\n");
					MailTextBuf.append("<tr><td align=left>親愛的客戶您好：</td></tr></br>").append("\n");
					MailTextBuf.append("<tr><td align=left>").append(StatusDESC).append("</td></tr>").append("\n");
					MailTextBuf.append("<tr><td align=left><BR><BR>祝您，事業順心，萬事如意!</td></tr>").append("\n");
					MailTextBuf.append("<tr><td align=left><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;臺灣企銀&nbsp;&nbsp;&nbsp;敬上</td></tr>").append("\n");
					MailTextBuf.append("</tr>").append("\n");
					MailTextBuf.append("</tbody>").append("\n");
					MailTextBuf.append("</table>").append("\n");			
					Map<String, String> varmail = new HashMap();
					varmail.put("EMAILTEXT", MailTextBuf.toString());
					log.trace("MailTextBuf = " + MailTextBuf.toString());
					boolean isSendSuccess = NotifyAngent.sendNotice("C053", varmail, email.replace(";", ",").trim());
					if(!isSendSuccess) {
						log.trace(ESAPIUtil.vaildLog("C053 發送 Email 失敗.(" +  rowData.get("CUSIDN") + " " + email + ")"));
						failcnt++;
					}
					else
					{
						log.trace(ESAPIUtil.vaildLog("C053發送 Email 成功.(" +  rowData.get("CUSIDN") + " " + email + ")"));
						okcnt++;
					}
				}
			}
			
			resultData.put("Send_Sucess", okcnt);
			resultData.put("Send_Fail", failcnt);
			resultData.put("No_Need_Send", noneedcnt);
			resultData.put("User_No_Email ", cantfindmailcnt);
			resultData.put("Cant_find_User", nousercnt);
			resultData.put("Total_Data", totalcnt);
			data.put("COUNTER", resultData);
			
			batchresult.setSuccess(true);
			batchresult.setBatchName(this.getClass().getSimpleName());
			batchresult.setData(data);
			
		} catch (FileNotFoundException e) {
			data.put("Error", "找不到檔案(" + hostpath + ")");
			batchresult.setData(data);
			batchresult.setSuccess(false);
		} catch (IOException e) {
			data.put("Error", "讀取檔案錯誤(" + hostpath + ")");
			batchresult.setData(data);
			batchresult.setSuccess(false);
		} catch (Exception e) {
			data.put("Error", "檔案為空(" + hostpath + ")");
			batchresult.setData(data);
			batchresult.setSuccess(false);
		}finally {
			try {
				in.close();
				bin.close();
			} catch (Exception e) {
			}
			in = null;
			bin = null;
			
			return batchresult;
		}
	}
	
	public Map<String, String> getData(String record){
		log.warn(ESAPIUtil.vaildLog("C053 LINE STR:" + record));
		Map<String, String> dataMap = new HashMap();
		String[] key = {"DATE","CUSIDN","EMAIL","RECODE"};
		String[] data = record.split("#\\|");
      
       for(int i = 0;i<key.length;i++) {
       	dataMap.put(key[i],data[i].trim());
       }
       
       return dataMap;
	}
}

