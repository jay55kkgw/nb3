package fstop.services.batch;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.exception.UncheckedException;
import fstop.model.DBResult;
import fstop.model.EachRowCallback;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.orm.dao.ItrCountDao;
import fstop.orm.dao.ItrIntervalDao;
import fstop.orm.dao.ItrN027Dao;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.po.ITRCOUNT;
import fstop.orm.po.ITRN027;
import fstop.services.AfterSuccessQuery;
import fstop.services.batch.annotation.Batch;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
/**
 * 台幣定存大額牌告利率批次程式
 * 
 * @author Owner
 *  
 */
@Slf4j
@RestController
@Batch(id="batch.n027", name = "台幣定存大額牌告利率更新", description = "台幣定存大額牌告利率更新")
public class N027 extends DispatchBatchExecute implements BatchExecute {
//	private Logger log = Logger.getLogger("fstop_txntw");

	private DecimalFormat fmt = new DecimalFormat("#0");
	@Autowired
	@Qualifier("n027Telcomm")
	private TelCommExec n027Telcomm;
	@Autowired
	private ItrN027Dao itrN027Dao;
	@Autowired
	private ItrCountDao itrCountDao;
	
//	private ItrCount itrcount;
	@Autowired
	private ItrIntervalDao itrIntervalDao;
	
	@Autowired
	private SysBatchDao sysBatchDao;

	public N027(){}
	
	/**
	 * args[0] method name
	 */
	@RequestMapping(value = "/batch/n027", method = {RequestMethod.POST},
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {
		List<String> safeArgs = ESAPIUtil.validStrList(args);
		log.info("batch N027...");
		log.info(ESAPIUtil.vaildLog("safeArgs: {}"+ JSONUtils.toJson(safeArgs)));
		
		Date d = new Date();
		String today = DateTimeUtils.getDateShort(d);
		String time = DateTimeUtils.getTimeShort(d);
		BatchResult batchresult = new BatchResult();
		Map<String, Object> data = new HashMap();
		batchresult.setBatchName(getClass().getSimpleName());
		batchresult.setSuccess(true);

		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}
		
		Hashtable params = new Hashtable();
		
		log.info("DATE = " + today);
		log.info("TIME = " + time);
		
		final StringBuffer recno_idBuff = new StringBuffer();
		recno_idBuff.append("0");
		try {
		final ITRN027 t = new ITRN027();
		
		SimpleTemplate simpleTemplate = new SimpleTemplate(n027Telcomm);
		
		simpleTemplate.setAfterSuccessQuery(new AfterSuccessQuery() {

			public void execute(MVHImpl result) {
				itrN027Dao.deleteAll();
				
				t.setHEADER(result.getValueByFieldName("HEADER"));
				t.setSEQ(result.getValueByFieldName("SEQ"));
				t.setBRHBDT(result.getValueByFieldName("BRHBDT"));
				t.setCOUNT(result.getValueByFieldName("COUNT"));
			}
			
		});

		simpleTemplate.setEachRowCallback(new EachRowCallback() {

			public void current(Row row) {
				Integer recno_id = new Integer(recno_idBuff.toString()) + 1;
				recno_idBuff.setLength(0);
				recno_idBuff.append(recno_id);
				String reconid=new String();
				reconid="00"+recno_idBuff.toString();
				t.setRECNO(StrUtils.right(reconid,2));
				log.trace("reconid =" + StrUtils.right(reconid,2));
				
				t.setTERM(row.getValue("TERM"));
				t.setITR1(row.getValue("ITR1"));
				t.setTITLE1(row.getValue("TITLE1"));
				t.setITR2(row.getValue("ITR2"));
				t.setTITLE2(row.getValue("TITLE2"));
				t.setITR3(row.getValue("ITR3"));
				t.setTITLE3(row.getValue("TITLE3"));
				t.setFILL(row.getValue("FILL"));
				itrN027Dao.insert(t);
			}
		});
		
		simpleTemplate.setExceptionHandler(new ExceptionHandler() {

			public void handle(Row row, Exception e) {
				
				log.error("無法更新台幣大額存款利率利率資料.", e);
				
			}
			
		});
		BatchCounter counter = simpleTemplate.execute(params);
		} catch (TopMessageException e) {
			batchresult.setSuccess(false);
			data.put("TOPMSG", e.getMsgcode() + "  " + e.getMsgout());
			log.error("TopMessageException  !!", e);
		} catch (UncheckedException e) {
			batchresult.setSuccess(false);
			data.put("TOPMSG", "UncheckedException 執行有誤 !!");
			log.error("UncheckedException 執行有誤 !!", e);
		}
		
		try {
			if (batchresult.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName, batchresult.getData());
			}
		} catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}

		return batchresult;

	}
	
	public MVH query(Map<String, String> params) {
		List<ITRN027> qresult = itrN027Dao.getAll();	
		addItrCount();
		MVHImpl mvh = new DBResult(qresult);
		return mvh;
	}
	
	private void addItrCount()
	{
		Date d = new Date();
		String today = DateTimeUtils.getCDateShort(d);
		String time = DateTimeUtils.getTimeShort(d);
		ITRCOUNT it = null;
		int iCount = 1;//若尚未有點選過，第一次寫入的次數值為一
		try {
			it = itrCountDao.findById("N027");
		}
		catch(ObjectNotFoundException e) {
			it = null;
		}
		if(it != null)
		{
			String count = it.getCOUNT();
			if(!"".equals(count) || count!= null)
			{
				iCount = Integer.parseInt(count);//把原本的次數找出來
				iCount++;//原本的次數加一;
				it.setDATE(today);
				it.setTIME(time);
				it.setCOUNT(Integer.toString(iCount));
			}
		}else
		{
			it = new ITRCOUNT();
			it.setHEADER("N027");
			it.setDATE(today);
			it.setTIME(time);
			it.setCOUNT("1");
			
		}
		itrCountDao.save(it);
	}
}
