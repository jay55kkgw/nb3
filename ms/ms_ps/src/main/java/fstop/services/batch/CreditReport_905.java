package fstop.services.batch;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.orm.dao.RevolvingCreditApplyDao;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.oao.Old_RevolvingCreditApplyDao;
import fstop.orm.old.OLD_REVOLVINGCREDITAPPLY;
import fstop.orm.po.REVOLVINGCREDITAPPLY;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import fstop.util.FtpBase64;
import fstop.util.FtpPathProperties;
import lombok.extern.slf4j.Slf4j;
import topibs.utility.NumericUtil;

/**
 * 每日產生昨日「申請長期使用循環轉換」明細表至 /TBB/nnb/data/NBCCD905 並把檔案拋至報表系統
 * @author Owner
 * 每頁1筆
 */
@Slf4j
@RestController
@Batch(id="batch.creditReport_905", name = "每日產生昨日「申請長期使用循環轉換」明細表至 /TBB/nnb/data/NBCCD905 並把檔案拋至報表系統", description = "每日產生昨日「申請長期使用循環轉換」明細表至 /TBB/nnb/data/NBCCD905 並把檔案拋至報表系統")
public class CreditReport_905  implements BatchExecute {
	

	
	@Value("${isParallelCheck}")
	private String isParallelCheck;
	@Autowired
	
	@Value("${doFTP}")
	private String doFTP;
	@Autowired
	private Old_RevolvingCreditApplyDao old_RevolvingCreditApplyDao;
	
	//=======================================
	
	@Autowired
	private RevolvingCreditApplyDao revolvingCreditApplyDao;
	
	@Autowired
	private CheckServerHealth checkserverhealth;
	
	@Value("${batch.localfile.path:}")
	private String path;
	
	@Value("${reportn.ftp.ip}")
	private String reportn_ftp_ip;
	
	private String ERRORMSG="";
	
	@Autowired
	private SysBatchDao sysBatchDao;
	
	@RequestMapping(value = "/batch/creditReport_905", method = {RequestMethod.POST},
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {
		List<String> safeArgs = ESAPIUtil.validStrList(args);
		log.debug("CreditReport_905 - Start execute ");
		
		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}
		
		int failcnt=0,okcnt=0,totalcnt=0;
		Hashtable rowcnt=new Hashtable();
		BatchResult result = new BatchResult();
		result.setBatchName(this.getClass().getSimpleName());

		try {
			if (safeArgs.size() == 1) {
				log.info(ESAPIUtil.vaildLog("safeArgs[0]: " + safeArgs.get(0)));
				Date date = DateTimeUtils.parse("yyyyMMdd", safeArgs.get(0));
				exec(rowcnt, date);
			} else {
				exec(rowcnt);
			}
			failcnt=((Integer)rowcnt.get("FAILCNT")).intValue();
			okcnt=((Integer)rowcnt.get("OKCNT")).intValue();
			totalcnt=((Integer)rowcnt.get("TOTALCNT")).intValue();
			log.debug("failcnt = " + failcnt);
			log.debug("okcnt = " + okcnt);
			log.debug("totalcnt = " + totalcnt);
		}
		catch(Exception e) {
			log.error("執行 Batch 有誤 !", e);
			ERRORMSG = e.getMessage();
			result.setSuccess(false);
		}
		
		BatchCounter bcnt=new BatchCounter();
		bcnt.setFailCount(failcnt);
		bcnt.setSuccessCount(okcnt);
		bcnt.setTotalCount(totalcnt);

		Map<String, Object> data = new HashMap();

		data.put("COUNTER", bcnt);

		result.setSuccess(true);
		result.setData(data);

		log.debug("CreditReport_905 - End execute ");
		
		try {
			if (result.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL(batchName,ERRORMSG);
			}
		}catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}
		
		return result;
	}

	private void exec(Hashtable rowcnt) throws IOException {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, -1);
		exec(rowcnt, cal.getTime());
	}
	
	private void exec(Hashtable rowcnt, final Date date) throws IOException {
		Integer	totalcnt=0,failcnt=0,okcnt=0;

		String despath=path+"NBCCD905";
		log.debug("despath = " + despath);

		Calendar cal = Calendar.getInstance();
		//列印日期
		String printDate = String.format("%3d/%02d/%02d %02d:%02d:%02d", 
						cal.get(Calendar.YEAR), cal.get(Calendar.MONTH)+1, cal.get(Calendar.DAY_OF_MONTH), 
						cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));
		
		//資料日期
		cal.setTime(date);
		String dataDate = String.format("%3d/%02d/%02d", cal.get(Calendar.YEAR), cal.get(Calendar.MONTH)+1, cal.get(Calendar.DAY_OF_MONTH));

		File tmpFile = new File(despath);
		tmpFile.setWritable(true,true);
		tmpFile.setReadable(true, true);
		tmpFile.setExecutable(true,true);
		tmpFile.delete();
		OutputStream fos = new FileOutputStream(tmpFile);
		BufferedWriter out = new BufferedWriter(new OutputStreamWriter(fos, "BIG5"));
		String pageHeader1 = "1 報表代號 : CCD905                                   長期使用循環轉換線上申請表                                     週期/保存年限 : 日報/結清後15年\n";
		String pageHeader2 = String.format("  需求單位 : H69                                      印表日期 : %s                                 頁    次 : ", printDate);
		String pageHeader3 = String.format("  資料日期 : %s\n", dataDate);
		
		String memo1 = "  1. 客戶符合申請條件，□同意其分期還款。\n";
		String memo2 = "     客戶不符合申請條件，□不同意其分期還款。\n";
		String memo3 = "  2. 分期期數____期，金額_________元，核定利率______%。\n";
		String memo4 = "  3. 分期金額將佔客戶原額度。\n";
		String memo5 = "  4. 陳閱後，轉作業科鍵機實施。\n";
		String pageFooter = "9 經  辦 :           	          襄　理 :           	          副　理 :           	          經　理 :\n";
		
		String header1 = "          姓名          身分證字號         卡 號              連絡電話            分期總金額         分期期數    核定利率    本金一期    第一期利息\n";
		String header2 = "  --------------------  ----------  -------------------  ------------------  --------------------   ----------  ----------  ----------  ------------\n";
		
		List<REVOLVINGCREDITAPPLY> recordList = revolvingCreditApplyDao.findByDate(DateTimeUtils.getDateShort(date));
		
		if("Y".equals(isParallelCheck)) {
			List<OLD_REVOLVINGCREDITAPPLY> oldrecordList = old_RevolvingCreditApplyDao.findByDate(DateTimeUtils.getDateShort(date));
			log.debug("NB 3 data size >> {}" , recordList.size());
			log.debug("NB 2.5 data size >> {}" , oldrecordList.size());
			// 狀況一 新個網有資料,舊個網無
			if(recordList.size()!=0 && oldrecordList.size()==0) {
				//使用l_CusList 不變
			}
			// 狀況二 新個網無資料 使用舊個網 
			if(recordList.size()==0 && oldrecordList.size()!=0) {
				for(OLD_REVOLVINGCREDITAPPLY data:oldrecordList) {
					REVOLVINGCREDITAPPLY nnbConvert2nb3 = CodeUtil.objectCovert(REVOLVINGCREDITAPPLY.class, data);
					recordList.add(nnbConvert2nb3);
				}
			}
			// 狀況三 兩邊都有資料 開始比對 
			if(recordList.size()!=0 && oldrecordList.size()!=0) {
				//TABLE TXNCUSINVATTRIB PK為DPUSERID , 如果兩邊有一樣的PK 用新的 , 如果沒有 用舊的
				recordList = getCombineList(recordList ,oldrecordList);
			}
		}
		
		
		REVOLVINGCREDITAPPLY record;
		Integer totalpage=recordList.size();
		//String totalpage1=totalpage.toString().substring(0,totalpage.toString().indexOf("."));
		log.debug("TotalPage:"+totalpage);
		if(totalpage==0)
		{
			out.write(pageHeader1);
			out.write(pageHeader2+"1/1\n");
			out.write(pageHeader3);
			out.write("\n");
			out.write(header1);
			out.write(header2);
			out.write("                     無申請紀錄                                                                      ");
			for(int k=0;k<47;k++) {
				out.write("\n");
			}
			out.write(pageFooter);
		}
		else {
			for (int l=0;l<totalpage;l++) {	
				out.write(pageHeader1);
				out.write(pageHeader2+(l+1)+"/"+totalpage+"\n");
				out.write(pageHeader3);
				out.write("\n");
				out.write(header1);
				out.write(header2);
				
				record = recordList.get(l);
				String space="";

				space="";
				String username="";
				if(record.getCUSNAME().length()>10)
					username = record.getCUSNAME().substring(0, 10).trim();
				else
					username = record.getCUSNAME();
				for (int m=0; m < 22-(username.length())*2; m++)
					space+=" ";
				username = space + username;

				space="";
				String userid = record.getCUSIDN().trim();
				for (int m=0; m < 12-(userid.length())*1; m++)
					space+=" ";
				userid = space + userid;
				
				space="";
				String cardnum = String.format("%s-%s-%s-%s", 
						record.getCARDNUM().substring(0, 4), record.getCARDNUM().substring(4, 8),
						record.getCARDNUM().substring(8, 12), record.getCARDNUM().substring(12, 16)).trim();
				for (int m=0; m < 21-(cardnum.length())*1; m++)
					space+=" ";
				cardnum = space + cardnum.trim();
				
				space="";
				String phone = "";
				if(record.getMPFONE().length()>0)
					phone = record.getMPFONE();
				else if(record.getOPHONE().length()>0)
					phone = record.getOPHONE();
				else
					phone = record.getHPHONE();
				//if (!("".equals(record.getMPFONE())))
				//	phone = record.getMPFONE();
				//else if (!("".equals(record.getOPHONE())))
				//	phone = record.getOPHONE();
				//else 
				//	phone = record.getHPHONE();
				phone = phone.substring(0, phone.length());
				for (int m=0; m < 20-(phone.length())*1; m++)
					space+=" ";
				phone = space + phone;
				
				space="";
				String amount = NumericUtil.formatNumberString(record.getAMOUNT(), 0).trim();
				for (int m=0; m < 22-(amount.length())*1; m++)
					space+=" ";
				amount = space + amount;
				
				space="";
				String period = record.getPERIOD().trim();
				for (int m=0; m < 13-(period.length())*1; m++)
					space+=" ";
				period = space + period;
				
				space="";
				String applyrate = record.getAPPLY_RATE().trim() + "%";
				for (int m=0; m < 12-(applyrate.length())*1; m++)
					space+=" ";
				applyrate = space + applyrate;

				space="";
				String firstamount = NumericUtil.formatNumberString(record.getFIRST_AMOUNT(), 0).trim();
				for (int m=0; m < 12-(firstamount.length())*1; m++)
					space+=" ";
				firstamount = space + firstamount;

				space="";
				String firstinterest = NumericUtil.formatNumberString(record.getFIRST_INTEREST(), 0).trim();
				for (int m=0; m < 12-(firstinterest.length())*1; m++)
					space+=" ";
				firstinterest = space + firstinterest;
				
				String f = username + userid + cardnum + phone + amount + period + applyrate + firstamount + firstinterest;
				log.debug(ESAPIUtil.vaildLog("Export File Content = " + f));
				out.write(f + "\n");
				okcnt++;
				totalcnt++;
				

				for (int k=0;k<21;k++) {
					out.write("\n");
				}
				out.write(memo1);
				out.write(memo2);
				out.write(memo3);
				out.write(memo4);
				out.write(memo5);
				for (int k=0;k<20;k++) {
					out.write("\n");
				}				
				out.write(pageFooter);
			}
		}

		try {
			out.flush();
			out.close();
			if ("Y".equals(doFTP)) {
				// FTP機制啟動拋至報表系統主機
				FtpPathProperties nrcpro = new FtpPathProperties();
				String srcpath = nrcpro.getSrcPath("creditreport_905");
				log.debug(ESAPIUtil.vaildLog("srcpath = " + srcpath));
				String despath2 = nrcpro.getDesPath("creditreport1_905");
				log.debug(ESAPIUtil.vaildLog("despath = " + despath2));
				String ipn = reportn_ftp_ip;
				FtpBase64 ftpBase64 = new FtpBase64(ipn);
				int rc = ftpBase64.upload(despath2, path + "/NBCCD905");
				if (rc != 0) {
					log.debug("FTP 失敗(fundreport 新主機)");
//					CheckServerHealth checkserverhealth = (CheckServerHealth) SpringBeanFactory
//							.getBean("checkserverhealth");
					checkserverhealth.sendUnknowNotice("FTP 失敗(fundreport 新主機)");
					log.debug("FTPSTATUS失敗");
				} else {
					log.debug("FTPSTATUS成功");
				}
			}
		}
		catch(Exception e){}
		try {
			fos.close();
		}
		catch(Exception e){}

		if(okcnt==0)
		{
			rowcnt.put("OKCNT",okcnt);
			rowcnt.put("FAILCNT",failcnt);
			rowcnt.put("TOTALCNT",totalcnt);
			return;
		}
		rowcnt.put("OKCNT",okcnt);
		rowcnt.put("FAILCNT",failcnt);
		rowcnt.put("TOTALCNT",totalcnt);
	}
	
	private List<REVOLVINGCREDITAPPLY> getCombineList(List<REVOLVINGCREDITAPPLY> nb3users ,List<OLD_REVOLVINGCREDITAPPLY> nnbusers){
		List<String> nb3list = new ArrayList();
		for(REVOLVINGCREDITAPPLY nb3user:nb3users) {
			nb3list.add(nb3user.getCUSIDN());
		}
		for(OLD_REVOLVINGCREDITAPPLY nnbuser : nnbusers) {
			if(!nb3list.contains(nnbuser.getCUSIDN())) {
				REVOLVINGCREDITAPPLY nnbConvert2nb3 = CodeUtil.objectCovert(REVOLVINGCREDITAPPLY.class, nnbuser);
				nb3users.add(nnbConvert2nb3);
			}
		}
		
		return nb3users;
	}
}
