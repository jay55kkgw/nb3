package fstop.services.batch;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.netbank.rest.model.JSPUtils;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.orm.dao.AdmCardDataDao;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.TxnCardApplyDao;
import fstop.orm.oao.Old_TxnCardApplyDao;
import fstop.orm.old.OLD_TXNCARDAPPLY;
import fstop.orm.po.ADMCARDDATA;
import fstop.orm.po.TXNCARDAPPLY;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import fstop.util.FtpBase64;
import fstop.util.FtpPathProperties;
import lombok.extern.slf4j.Slf4j;

/**
 * @Author John
 * @Date: 2020-09-08 11:12:35
 * @LastEditTime: 2020-09-08 11:12:35
 * @LastEditors: Please set LastEditors
 * @Description: 每月產生客戶承作網路銀行交易異常情形表"至 /TBB/nnb/data/NBCCM913 並把檔案拋至報表系統
 */
@Slf4j
@RestController
@Batch(id = "batch.NNBReport_M913", name = "客戶承作網路銀行交易異常情形表", description = "每月產生客戶承作網路銀行交易異常情形表至 /TBB/nnb/data/NBCCM913 並把檔案拋至報表系統")
public class NNBReport_M913 implements BatchExecute {

//	private Logger logger = Logger.getLogger(NNBReport_M913.class);
	
//    private static String DES_PATH = "/TBB/nnb/data/NBCCM913"; // 報表檔落地位置
    
	@Autowired
	private TxnCardApplyDao txnCardApplyDao;
	@Autowired
	private AdmCardDataDao admCardDataDao;	
	
	@Value("${batch.localfile.path:}")
	private String path;
	
	@Value("${reportn.ftp.ip}")
	private String reportn_ftp_ip;
	
	@Value("${isParallelCheck}")
	private String isParallelCheck;

	@Value("${doFTP}")
	private String doFTP;
	
	@Autowired
	private Old_TxnCardApplyDao old_TxnCardApplyDao;

	@Autowired
	CheckServerHealth checkserverhealth;
	
	@Autowired
	private SysBatchDao sysBatchDao;

	@RequestMapping(value = "/batch/nnbReport_M913", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> _params) {
		
		List<String> safeArgs = ESAPIUtil.validStrList(_params);
		log.info("batch NNBReport_M913...");
		log.info("產出同一IP位址不同客戶線上申請信用卡清單資料CCM913");

		BatchResult batchresult = new BatchResult();
		Map<String, Object> data = new LinkedHashMap();
		batchresult.setBatchName(getClass().getSimpleName());
		batchresult.setSuccess(true);

		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}

//		if (logger.isDebugEnabled()) {
//			logger.debug("產出同一IP位址不同客戶線上申請信用卡清單資料CCM913.... ");
//			logger.debug("_params len: " + _params.size());
//		}
		int failcnt = 0, okcnt = 0, totalcnt = 0;

		Map<String, Integer> rowcnt = new ConcurrentHashMap<String, Integer>();
		Map<String, String> params = new ConcurrentHashMap<String, String>();

		params.put("TXNIDT", DateTimeUtils.format("yyMMddHHmmss", new Date()));

		try {
			if (safeArgs.size() == 1) {
				// 當批次有帶參數(帶參數是為了要產出特定月份的報表檔)
				Date date = DateTimeUtils.parse("yyyyMMdd", _params.get(0));
				exec(rowcnt, date);
			} else {
				
				// 批次未帶參數(未帶參數是要產出 "上個月" 的報表檔)
				// Start Get Monthly Report Data (Default Date)
				exec(rowcnt);
				
				failcnt = ((Integer) rowcnt.get("FAILCNT")).intValue();
				okcnt = ((Integer) rowcnt.get("OKCNT")).intValue();
				totalcnt = ((Integer) rowcnt.get("TOTALCNT")).intValue();
				
				log.debug(ESAPIUtil.vaildLog("failcnt = " + failcnt));
				log.debug(ESAPIUtil.vaildLog("okcnt = " + okcnt));
				log.debug(ESAPIUtil.vaildLog("totalcnt = " + totalcnt));
//				if (logger.isDebugEnabled()) {
//					logger.debug("failcnt = " + failcnt);
//					logger.debug("okcnt = " + okcnt);
//					logger.debug("totalcnt = " + totalcnt);
//				}
			}
			
		} catch (Exception e) {
			log.error("執行 Batch 有誤 !", e);
			data.put("Error", e.getMessage());
			batchresult.setSuccess(false);
//			if (logger.isErrorEnabled()) {
//				logger.error("執行 Batch 有誤 !", e);
//			}
		}
		// end

		BatchCounter bcnt = new BatchCounter();
		bcnt.setFailCount(failcnt);
		bcnt.setSuccessCount(okcnt);
		bcnt.setTotalCount(totalcnt);

//		Map<String, Object> data = new ConcurrentHashMap<String, Object>();
		data.put("COUNTER", bcnt);

//		BatchResult result = new BatchResult();
//		result.setSuccess(true);
//		result.setBatchName(this.getClass().getSimpleName());
		batchresult.setData(data);
		
		
		log.debug("NNBReport_M913 - End execute ");

		try {
			if (batchresult.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName, batchresult.getData());
			}
		} catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}
		return batchresult;
		
	}
	private void exec(Map<String, Integer> rowcnt) throws Exception {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, -1);
		exec(rowcnt, cal.getTime());
	}

	private void exec(Map<String, Integer> rowcnt, final Date dataDate) throws Exception {
		
		Integer totalcnt = 0, failcnt = 0, okcnt = 0;
		
		java.util.Calendar dataDate_cal = java.util.Calendar.getInstance();
		dataDate_cal.setTime(dataDate);
		
		// 印表日期 格式 yyyy/MM/dd HH:mm:ss
		String reportPrintDate = DateTimeUtils.getDatetime(new Date());
		
		String year = DateTimeUtils.format("yyyy", dataDate_cal.getTime());
		String mon  = DateTimeUtils.format("MM", dataDate_cal.getTime());
		String day  = DateTimeUtils.format("dd", dataDate_cal.getTime());
		
		//end 20180904
//		String sEndDay = StringUtils.EMPTY;
//		sEndDay = getEndDateByMonth(year, mon, sEndDay);
//		String dStart = year + mon + "01"; // 查詢資料起日
//		String dEnd = year + mon + sEndDay; // 查詢資料迄日
		String dStart = year + mon + day ;
		String dEnd = year + mon + day ;
		
		
		log.info(ESAPIUtil.vaildLog("year: " + year + " mon: " + mon + " day: " + day));
//		log.info(ESAPIUtil.vaildLog("stDate:" + dStart + "  edDate:" + dEnd));
		log.info(ESAPIUtil.vaildLog("despath = " + path +"NBCCM913"));
		
//		if(logger.isInfoEnabled()){
//			logger.info("year: " + year + " mon: " + mon + " day: " + day);
//			logger.info("stDate:" + dStart + "  edDate:" + dEnd);
//			logger.info("despath = " + path+"NBCCM913");
//		}

        File tmpFile = new File(path+"NBCCM913");
        //Incorrect Permission Assignment For Critical Resources
      	tmpFile.setWritable(true,true);
      	tmpFile.setReadable(true, true);
      	tmpFile.setExecutable(true,true);
        tmpFile.delete(); //刪除舊檔
        
        
        String PAGE_HEADER1 = "1 報表代號 : CCM913                                              同一IP位址不同客戶線上申請信用卡清單                           週    期 : 日報 \n";
        // YYYY/MM/DD -YYYY 印表日期 : printdate
        String PAGE_HEADER2 = "  資料日期 : %s/%s/%s" + "                                        印表日期 : %s" + "                                   頁    次 : ";
        String PAGE_HEADER3 = "  需求單位 : H69                                                                                                                製表單位 : 資訊部\n\n\n\n";
        String BODY_HEADER1 = "                       客戶                                               同IP位置    同IP位置申請人       同IP位置申請人            同IP位置申請人 \n";
        String BODY_HEADER2 = "  申請日期           統一編號           客戶姓名            IP位置        申請日期      統一編號               姓名                    申請卡別  \n";
        String BODY_HEADER3 = "-------------    -----------------   --------------   -----------------  ----------  ----------------    -----------------   ---------------------------------------  \n";
        String PAGE_FOOTER = "9   經　辦 :                    　襄　理 :                    　副　理 :                    　經　理 :\n";

        int PAGE_SIZE = 37;
        
        
        OutputStream fos = new FileOutputStream(tmpFile);
        BufferedWriter out = new BufferedWriter(new OutputStreamWriter(fos, "BIG5"));
        
        List<TXNCARDAPPLY> allApplyRecord = txnCardApplyDao.getSameIpAnotherApplicant(dStart, dEnd);// 查詢TXNCARDAPPLY table 同IP 不同申請人線上申請信用卡
    	log.trace("NB3 data size >> {} ", allApplyRecord.size());
        if ("Y".equals(isParallelCheck)) {
        	List<OLD_TXNCARDAPPLY> old_allApplyRecord = old_TxnCardApplyDao.getSameIpAnotherApplicant(dStart, dEnd);// 查詢TXNCARDAPPLY table 同IP 不同申請人線上申請信用卡
        	log.trace("NB2.5 data size >> {} ", old_allApplyRecord.size());
        	for (OLD_TXNCARDAPPLY old : old_allApplyRecord) {
        		TXNCARDAPPLY nnbConvert2nb3 = CodeUtil.objectCovert(TXNCARDAPPLY.class, old);
        		allApplyRecord.add(nnbConvert2nb3);
			}
        }
        Map<String,String>allCards = getAllCardType(); // 取得所有信用卡類別
        
        //FIXME 假資料測試用
//        List<CCM913ReportEntity> rows = new CCM913ReportEntity().getTestDataList();
        List<CCM913ReportEntity> rows = groupByQueryResult(allApplyRecord);
        int rowSize = rows.size();// 資料筆數
        Double totalPage = Math.ceil(( (float) rowSize /(float) PAGE_SIZE)); // 計算每分行有幾頁資料
        String totalPageStr = String.format("%.0f" , totalPage);
        
        
        log.info(ESAPIUtil.vaildLog("Row Data Json String : " + new Gson().toJson(allApplyRecord)));
		log.info(ESAPIUtil.vaildLog("Filted Data Json String : " + new Gson().toJson(rows)));
		log.info(ESAPIUtil.vaildLog(String.format("All Page: %s , queryNum: %s , totalPageStr: %s" , totalPage , rowSize ,totalPageStr)));
        
//        if(logger.isInfoEnabled()){
//			logger.info("Row Data Json String : " + new Gson().toJson(allApplyRecord));
//			logger.info("Filted Data Json String : " + new Gson().toJson(rows));
//			logger.info(String.format("All Page: %s , queryNum: %s , totalPageStr: %s" , totalPage , rowSize ,totalPageStr));
//		}
        
		

        //替換字串中日期變數
        PAGE_HEADER2 = String.format(PAGE_HEADER2, year, mon, day, reportPrintDate);

        if (totalPage < 1) { // 無筆數
            StringBuilder content = new StringBuilder();

            content.append(PAGE_HEADER1)
                   .append(PAGE_HEADER2).append("1/1").append(IOUtils.LINE_SEPARATOR_UNIX) // 顯示頁次 並換行
                   .append(PAGE_HEADER3);

            content.append(BODY_HEADER1)
                   .append(BODY_HEADER2)
                   .append(BODY_HEADER3);

            content.append("                                          無同IP不同用戶線上申請信用卡紀錄                                                                      ");

            for (int k = 0; k < PAGE_SIZE; k++) {
                content.append(IOUtils.LINE_SEPARATOR_UNIX);
            }
            // 輸出資料
            IOUtils.write(content.toString(),out);
            
        } else {
        	String occurIp = StringUtils.EMPTY; //前一筆紀錄之IP
            Boolean isFirstOcc = Boolean.TRUE;	//若前一筆紀錄之IP為起始筆數
            for (int page = 0; page < totalPage; page++) {
                // 需執行頁數
            	log.info("Start page : " +  (page+1) );
                StringBuilder content = new StringBuilder();
                
                //報表表頭
                content.append(PAGE_HEADER1)
                	   .append(PAGE_HEADER2 + ( page + 1) + "/" + totalPageStr ).append(IOUtils.LINE_SEPARATOR_UNIX) //添加當下頁次
                	   .append(PAGE_HEADER3);

                //報表資料表頭
                content.append(BODY_HEADER1)
                	   .append(BODY_HEADER2)
                	   .append(BODY_HEADER3);
                
                // 輸出表頭資料
                IOUtils.write(content.toString(), out );
                
                
                int counter = 0;					//計算器
                
                // 計算報表每頁扣掉資料後還需換行次數，一頁最多xx筆
                for (int idx = page * PAGE_SIZE ; idx < rowSize; idx++) {
                	
                    if(StringUtils.equalsIgnoreCase(occurIp , rows.get(idx).getIp())){
                    	isFirstOcc = Boolean.FALSE;
                    }else {
                    	isFirstOcc = Boolean.TRUE;
                    }
                    occurIp = rows.get(idx).getIp();

                    //實際資料文字
                    StringBuilder rowDataContent = new StringBuilder();
                    //起始欄位間隔 5 space
                    fulfillColumnSpace (rowDataContent , ColumnSpace.INIT_COLUMN.columnSize ,StringUtils.EMPTY); 
                    
                    //日期 將資料庫日期轉為報表需求日期 "20200915" -> "2020.09.15" 
                    String reportDate = "" ;
                    if(StringUtils.isNotBlank(rows.get(idx).getApplyDate()))reportDate = DateTimeUtils.format("yyyy.MM.dd", DateTimeUtils.parse("yyyyMMdd", rows.get(idx).getApplyDate()));
                    if(isFirstOcc) {
                    	rowDataContent.append(reportDate); 
                    	fulfillColumnSpace(rowDataContent, ColumnSpace.APPLY_DATE.columnSize, rows.get(idx).getApplyDate());
                    } else { fulfillColumnSpace (rowDataContent , ColumnSpace.APPLY_DATE.columnSize ,StringUtils.EMPTY);}

                    //客戶統編
                    if(isFirstOcc) {
                    	rowDataContent.append(rows.get(idx).getIdentityId());
                    	fulfillColumnSpace(rowDataContent, ColumnSpace.IDENTITY_ID.columnSize, rows.get(idx).getIdentityId());
                    } else { fulfillColumnSpace (rowDataContent , ColumnSpace.IDENTITY_ID.columnSize ,StringUtils.EMPTY);}

                    //客戶姓名
                    if(isFirstOcc) {
                    	rowDataContent.append(JSPUtils.convertFullorHalf(rows.get(idx).getName(), 1));
                    	//中文姓名處理 (若有英數字轉換為全形)
                    	fulfillColumnSpace_ChineseChar(rowDataContent,ColumnSpace.CUST_NAME.columnSize, rows.get(idx).getName() , true);
                    	
                    } else {fulfillColumnSpace (rowDataContent , 15 ,StringUtils.EMPTY);}

                    //申辦IP位址
                    if(isFirstOcc) {
                    	rowDataContent.append(rows.get(idx).getIp());
                    	fulfillColumnSpace(rowDataContent, ColumnSpace.IP_ADDRESS.columnSize, rows.get(idx).getIp());
                    } else { fulfillColumnSpace (rowDataContent , ColumnSpace.IP_ADDRESS.columnSize + 3 ,StringUtils.EMPTY);}
                    
                    //同IP第二申請人日期
                    rowDataContent.append(rows.get(idx).getSecondApplyDate());
                    fulfillColumnSpace(rowDataContent, ColumnSpace.SECOND_APPLY_DATE.columnSize, rows.get(idx).getSecondId());
                    
                    //同IP第二申請人證號
                    rowDataContent.append(rows.get(idx).getSecondId());
                    fulfillColumnSpace(rowDataContent, ColumnSpace.SECOND_ID.columnSize, rows.get(idx).getSecondId());
                    
                    //同IP第二申請人名 (若有英文字轉換為全形)
                    rowDataContent.append(JSPUtils.convertFullorHalf(rows.get(idx).getSecondName(), 1));
                    //中文姓名處理
                    fulfillColumnSpace_ChineseChar(rowDataContent, ColumnSpace.SECOND_NAME.columnSize, rows.get(idx).getSecondName() , true); 
                    
                    
                    //申辦卡別 (卡片代碼轉換為文字 )
                    String realCardName = allCards.get(rows.get(idx).getCardName());
                    rowDataContent.append(realCardName);
                    rowDataContent.append(IOUtils.LINE_SEPARATOR_UNIX);

                    //輸出資料至檔案NBCCM913
                    IOUtils.write(rowDataContent.toString() , out );
                    log.debug(ESAPIUtil.vaildLog("Current Record Write data index : " +  idx ));
//                    if(logger.isDebugEnabled()){
//                    	logger.debug("Current Record Write data index : " +  idx );
//                    }
                    counter++;
                    okcnt++;
					totalcnt++;
                    
                    // 若達每頁筆數上限就換頁 並填滿換行至上限值缺少筆數
                    if (counter % (PAGE_SIZE + 1) ==0)
                        break;
                    
                    if ((rowSize % PAGE_SIZE) != 0 && (idx == rowSize - 1)) {
                        int fill_space = PAGE_SIZE - (rowSize % PAGE_SIZE);
                        for (int k = 0; k < fill_space; k++) {
                        	IOUtils.write(IOUtils.LINE_SEPARATOR_UNIX , out );
                        }
                    }

                }// END for loop 每頁幾筆 

                // 輸出報表頁尾
                IOUtils.write(PAGE_FOOTER , out );
            }// END for loop 頁數
            
        }// END IF totalpage!=0

		try {
			out.flush();
			out.close();
			if ("Y".equals(doFTP)) {
				// FTP機制啟動拋至報表系統主機
				FtpPathProperties nrcProperties = new FtpPathProperties();
				String srcpath = nrcProperties.getSrcPath("nnbreport_m913");
				String despath = nrcProperties.getDesPath("nnbreport1_m913");
				String ipn = reportn_ftp_ip;

				log.info(ESAPIUtil
						.vaildLog(String.format("srcpath : %s , despath : %s , ipn : %s", srcpath, despath, ipn)));

				// if(logger.isInfoEnabled()){
				// logger.info(String.format("srcpath : %s , despath : %s , ipn : %s", srcpath ,
				// despath ,ipn));
				// }

				FtpBase64 ftpBase64 = new FtpBase64(ipn);
				int rc = ftpBase64.upload(despath, path + "/NBCCM913");

				if (rc != 0) {
					log.debug("FTP 失敗(report 新主機)");
					// CheckServerHealth checkserverhealth = (CheckServerHealth)
					// SpringBeanFactory.getBean("checkserverhealth");
					checkserverhealth.sendUnknowNotice("FTP 失敗(report 新主機)");
					log.debug("FTPSTATUS失敗");
				} else {
					log.debug("FTPSTATUS成功");
				}
			}
		} catch (Exception e) {
			log.warn("Connect FTP Server Error" , e);
		}
		
		try {
			IOUtils.closeQuietly(fos);
		} catch (Exception e) {
			log.warn("Close File" , e);
		}

		if (okcnt == 0) {
			rowcnt.put("OKCNT", okcnt);
			rowcnt.put("FAILCNT", failcnt);
			rowcnt.put("TOTALCNT", totalcnt);
			return;
		}
		
		rowcnt.put("OKCNT", okcnt);
		rowcnt.put("FAILCNT", failcnt);
		rowcnt.put("TOTALCNT", totalcnt);
	}
	
	
	/**
     *  處理中文姓名欄位 填滿空格數
     * @param rowDataContent
     * @param columnSpace
     * @param chineseChar
     * @param isName
     */
    private static void fulfillColumnSpace_ChineseChar(StringBuilder rowDataContent, int columnSpace, String chineseChar , Boolean isName) {

        //判斷是否為姓名欄位 專門處理姓名欄位
        if(isName){
        	for (int i = 0; i < columnSpace - ((chineseChar.length()) * 2) + 4; i++) {
                rowDataContent.append(" ");
            }
//            for (int i = 0; i < (3 - chineseChar.length()) * 2; i++) {
//                rowDataContent.append("  ");
//            }
        }

    }
    
	/**
     * 添加欄位與欄位之間的間隔
     * 
     * @param outputRowStr
     * @param columnSpace 當前欄位起始點與下一個欄位起始點所間隔Byte數
     * @param currentStr 當前資料字串
     */
    private static void fulfillColumnSpace(StringBuilder outputRowStr, int columnSpace, String currentStr) {
    	
    	if(StringUtils.isBlank(currentStr)) currentStr = StringUtils.EMPTY;
        for (int i = 0; i < columnSpace - currentStr.length(); i++) {
        	outputRowStr.append(" ");
        }
    }
    
    /**
     *  計算每月最後一天
     * @param year
     * @param mon
     * @param sEndDay
     * @return
     */
	private String getEndDateByMonth(String year, String mon, String sEndDay) {
        if (StringUtils.contains("01,03,05,07,08,10,12", mon)) {
            sEndDay = "31";
        } else if (StringUtils.contains("04,06,09,11", mon)) {
            sEndDay = "30";
        } else {
            if (StringUtils.equalsIgnoreCase(mon, "02") && (Integer.parseInt(year) % 4 == 0)
                    && (Integer.parseInt(year) % 100 != 0)) {
                sEndDay = "29";
            } else if (mon.equals("02")) {
                sEndDay = "28";
            }
        }
        return sEndDay;
    }

	// 擷取分行別長度
	public String subStringBrans(String brans) {

		brans = brans.replace((char) 12288, ' ');// 將全形空格unicode轉成半形空白再trim，因trim只能去掉的字元是unicode小於32
		log.debug("replaceAll brans==>" + brans + "<==");
		brans = brans.trim();// 先清空白
		log.debug("trim brans==>" + brans + "<==");
		if (!brans.equals("")) {// 分行別不為空值
			brans = brans.substring(0, 3);
		}
		log.debug("before brans len==>" + brans.length() + "<==");
		brans = leftSpace(brans, 6, "NUM");
		log.debug("after brans==>" + brans + "<==");

		return brans;
	}

	// 擷取名字長度
	public String subStringName(String name) {

		name = name.replace((char) 12288, ' ');// 將全形空格unicode轉成半形空白再trim，因trim只能去掉的字元是unicode小於32
		// logger.debug("replaceAll name==>" + name+"<==");
		name = name.trim();// 先清空白
		// logger.debug("trim name==>"+name+"<==");
		if (!name.equals("")) {// 姓名不為空值
			if (name.length() >= 5) {// 姓名超過五位只抓五位
				name = name.substring(0, 5);
			} else if (name.length() < 5) {// 姓名未超過五位，抓姓名總長度
				name = name.substring(0, name.length());
			}
		}
		// logger.debug("before name len==>"+name.length()+"<==");
		name = leftSpace(name, 5, "STRING");
		// logger.debug("after name==>"+name+"<==");

		return name;
	}

	// 擷取交易代號長度
	public String subStringTrans(String trans) {

		trans = trans.replace((char) 12288, ' ');// 將全形空格unicode轉成半形空白再trim，因trim只能去掉的字元是unicode小於32
		log.debug("replaceAll trans==>" + trans + "<==");
		trans = trans.trim();// 先清空白
		log.debug("trim trans==>" + trans + "<==");
		if (!trans.equals("")) {// 交易名稱不為空值
			if (trans.length() >= 10) {// 交易代號超過10位只抓10位
				trans = trans.substring(0, 10);
			}
		}
		log.debug("before trans len==>" + trans.length() + "<==");
		trans = leftSpace(trans, 10, "STRING");// 參數1:欄位值 參數2:欄位總長度 參數3:欄位型態
		log.debug("after trans==>" + trans + "<==");

		return trans;
	}

	// 左靠右補空白
	public String leftSpace(String nameValue, int Strlen, String type) {// 參數1:值
																		// 參數2:欄位總長度
																		// 參數3:型態

		String space = "";
		// 補上不足的空白
		if (type.equals("STRING")) {// 字串型態
			for (int i = 0; i < Strlen - (nameValue.length()); i++) {// 上限長度-名字長度會剩多少空白
				space += (char) 12288;// 補全形空白
			}
		} else { // 數字型態
			for (int i = 0; i < Strlen - (nameValue.length()); i++) {// 上限長度-名字長度會剩多少空白
				space += (char) ' ';// 補半形空白
			}
		}
		return nameValue + space;
	}
	
	/**
     *  取得現行所有信用卡卡別
     * @return Map<String,String>
     */
	private Map<String,String> getAllCardType(){
		Map<String,String> cards = new HashMap<String, String>();
		
		List<ADMCARDDATA> allCardType = admCardDataDao.findAll();
		for (ADMCARDDATA admcarddata : allCardType) {
			cards.put(admcarddata.getCARDNO(), admcarddata.getCARDNAME());
		}
		
		return cards;
	}
	
	/**
     *   根據IP進行資料分組
     * @param records
     * @return Map
     */
	private List<CCM913ReportEntity> groupByQueryResult(List<TXNCARDAPPLY> records){
		log.info("TXNCARDAPPLY size : " + records.size());
		
		Map<String,List<CCM913ReportEntity>> groupBy = new LinkedHashMap<String, List<CCM913ReportEntity>>(); 
		Map<String,List<CCM913ReportEntity>> tempFirstRecord = new ConcurrentHashMap<String, List<CCM913ReportEntity>>();
		List<CCM913ReportEntity> filtedList = new ArrayList<CCM913ReportEntity>();
		
		for (TXNCARDAPPLY cardApply : records) {
			
			String indexIP = cardApply.getIP();
			String rcvNo = cardApply.getRCVNO();
			
			CCM913ReportEntity currentRecord = convertDaoResultToEntity(cardApply);

			if(tempFirstRecord.containsKey(indexIP)){
				//取得已存在第一筆紀錄
				CCM913ReportEntity firstRecord = tempFirstRecord.get(indexIP).get(0);
				
				Boolean isSamePerson = StringUtils.equalsIgnoreCase(firstRecord.getIdentityId(), currentRecord.getIdentityId());
				
				//若已存在同IP資料 將此筆Record現行 Id & Name 放入第二 ID& Name 
				currentRecord.setSecondId(currentRecord.getIdentityId());
				currentRecord.setSecondName(currentRecord.getName());
				currentRecord.setSecondApplyDate(currentRecord.getApplyDate());
				
				//將原ID & IP替換為第一筆
				currentRecord.setIdentityId(firstRecord.getIdentityId());
				currentRecord.setName(firstRecord.getName());
				currentRecord.setIp(firstRecord.getIp());
				
				if(!isSamePerson){ //若與第一筆不同ID才塞入
//FIXME	 移除舊寫法	filtedList.add(currentRecord);
					if(groupBy.get(indexIP).size()==0) { // 若當下list 取出size為空 , 則將當前資料的申請日改為第一筆的申請日
						currentRecord.setApplyDate(firstRecord.getApplyDate());
					}
					groupBy.get(indexIP).add(currentRecord);
				}
			}else {
			    List<CCM913ReportEntity> list = new ArrayList<CCM913ReportEntity>();
			    list.add(currentRecord);
			    tempFirstRecord.put(indexIP, list);
			    
			    groupBy.put(indexIP, new LinkedList<CCM913ReportEntity>()); //init List
			    
			}
		}

		for (Entry<String, List<CCM913ReportEntity>> entrySet : groupBy.entrySet()) {
			log.info("entry Key : " + entrySet.getKey());
			if(CollectionUtils.isNotEmpty(entrySet.getValue())){
				filtedList.addAll(entrySet.getValue());
			}
		}
//		for (List<CCM913ReportEntity> list : groupBy.values()) {
//		}
		log.info("filtedList size : " + filtedList.size());
		
		return filtedList;
	}
	
	/**
     *  轉換 ORM 物件至Report POJO
     * @param TXNCARDAPPLY
     * @return CCM913ReportEntity
     */
	private CCM913ReportEntity convertDaoResultToEntity (TXNCARDAPPLY cardApply){
		CCM913ReportEntity entity = new CCM913ReportEntity();
		
		entity.setApplyDate(cardApply.getLASTDATE());
		entity.setIdentityId(cardApply.getCPRIMID());
		entity.setName(cardApply.getCPRIMCHNAME());
		entity.setIp(cardApply.getIP());
		
		if(StringUtils.isNotBlank(cardApply.getCARDNAME1())){
			entity.setCardName(cardApply.getCARDNAME1());
		}else if(StringUtils.isNotBlank(cardApply.getCARDNAME2())){
			entity.setCardName(cardApply.getCARDNAME2());
		}else if(StringUtils.isNotBlank(cardApply.getCARDNAME3())){
			entity.setCardName(cardApply.getCARDNAME3());
		}
		
		return entity;
	}
	
	/**
     *  抽出各欄位間隔參數
     */
    private enum ColumnSpace {
    	INIT_COLUMN("InitialColumn", 2),
        APPLY_DATE("ApplyDate", 16),
        IDENTITY_ID("IdentityId", 21),
        CUST_NAME("CustomerName", 12),
        IP_ADDRESS("IPAddress", 17),
        SECOND_APPLY_DATE("SecondApplyDate", 16),
        SECOND_ID("SecondIdentityId", 21),
        SECOND_NAME("SecondCustomerName", 14);

        private final String columnName;
        private final int columnSize;
        
        private ColumnSpace(String columnName, int columnSize) {
            this.columnName = columnName;
            this.columnSize = columnSize;
        }
        public int getColumnSize() {
            return columnSize;
        }
        public String getColumnName() {
            return columnName;
        }
        
        public static int getColumnSizeByColumnName(String columnName) {
            for (ColumnSpace currEnum : ColumnSpace.values()) {
                if (currEnum.getColumnName().equals(columnName)) {
                    return currEnum.getColumnSize();
                }
            }
            return 0;
        }
    }

}
