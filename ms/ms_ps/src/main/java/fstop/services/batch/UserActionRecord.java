package fstop.services.batch;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.owasp.esapi.SafeFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.orm.dao.AdmOpLogDao;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.SysOpDao;
import fstop.orm.oao.Old_AdmOpLogDao;
import fstop.orm.old.OLD_ADMOPLOG;
import fstop.orm.po.ADMOPLOG;
import fstop.orm.po.SYSOP;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import fstop.util.FtpBase64;
import fstop.util.FtpPathProperties;
import fstop.util.JSONUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@Batch(id = "batch.userActionRecord", name = "後台的稽核軌跡紀錄檔案，ftp 給CRM 系統", description = "後台的稽核軌跡紀錄檔案，ftp 給CRM 系統")
public class UserActionRecord implements BatchExecute {
	@Autowired
	private AdmOpLogDao admOpLogDao;

	@Autowired
	private Old_AdmOpLogDao old_AdmOpLogDao;

	@Autowired
	private SysOpDao sysOpDao;

	@Value("${isParallelCheck}")
	private String isParallelCheck;
	@Value("${doFTP}")
	private String doFTP;

	@Value("${crmn.ftp.ip}")
	private String crmn_ftp_ip;

	@Autowired
	private SysBatchDao sysBatchDao;

	@RequestMapping(value = "/batch/userActionRecord", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {

		log.info("batch UserActionRecord...");

		BatchResult batchresult = new BatchResult();
		Map<String, Object> data = new LinkedHashMap();
		batchresult.setBatchName(getClass().getSimpleName());
		batchresult.setSuccess(true);
		BatchCounter bc = new BatchCounter();

		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}

		List<String> safeArgs = ESAPIUtil.validStrList(args);

		String yesterday = "";
		String yyyy = "";// 西元年
		String mm = "";// 月
		String dd = "";// 日

		if (safeArgs.size() == 1) {// 當批次有帶參數(帶參數是為了要產出特定月份的報表檔)
			yyyy = safeArgs.get(0).substring(0, 4);// 西元年
			mm = safeArgs.get(0).substring(4, 6);// 月份
			dd = safeArgs.get(0).substring(6, 8);// 日期
			yesterday = yyyy + mm + dd;
		} else {
			Calendar rightNow = Calendar.getInstance();
			rightNow.setTime(new java.util.Date());
			rightNow.add(Calendar.DAY_OF_YEAR, -1);
			Date dt1 = rightNow.getTime();
			yesterday = DateTimeUtils.getDateShort(dt1);
		}

		log.debug(ESAPIUtil.vaildLog("EXEC DATE >> {} " + yesterday));

		List<ADMOPLOG> admOpLogList = admOpLogDao.getAdmOpLogList(yesterday);
		log.debug("NB 3 data size >> {}", admOpLogList.size());

		if ("Y".equals(isParallelCheck)) {
			List<OLD_ADMOPLOG> admOpLogListOLD = old_AdmOpLogDao.getAdmOpLogList(yesterday);

			log.debug("NB 2.5 data size >> {}", admOpLogListOLD.size());

			for (OLD_ADMOPLOG old_data : admOpLogListOLD) {
				ADMOPLOG nnbConvert2nb3 = CodeUtil.objectCovert(ADMOPLOG.class, old_data);
				admOpLogList.add(nnbConvert2nb3);
			}
		}

		Map<String, String> jsonMap = new HashMap();
		Map<String, String> sysOp = getSYSOP();
		log.debug("UserActionRecord.java sysOp:" + sysOp.size());

		StringBuilder writeFileString = new StringBuilder();
		String Opt_Dttm = "";
		String tempDate = "";
		String tempTime = "";
		String Opt_Type_Cd = "";
		String User_ID = "";
		String ADOPNAME = "";
		String ADEXCODE = "";
		String Biz_Unit_Num = "";
		String Party_ID = "";
		String adtDate = "";
		String adfDate = "";
		String User_IP = "";
		String Opt_Txt = "";
		ADMOPLOG admOpLogEx = new ADMOPLOG();
		// ------------------
		int count = admOpLogList.size();
		log.debug("UserActionRecord.java admOpLogList size:" + count);
		// SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
		// SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		try {
			FtpPathProperties nrcpro = new FtpPathProperties();
			String srcpath = nrcpro.getSrcPath("useractionrecord").trim();
			// //LTest
			// srcpath = "C:\\Users\\BenChien\\Desktop\\batchLocalTest\\data\\";
			String despath = nrcpro.getDesPath("useractionrecord").trim();
			String validatePath = ESAPIUtil.vaildPathTraversal2(srcpath, "NNB_LOG." + yesterday + ".TXT");
			File saveFile = new SafeFile(validatePath);
			saveFile.setWritable(true, true);
			saveFile.setReadable(true, true);
			saveFile.setExecutable(true, true);
			// FileWriter fwriter = new FileWriter(saveFile);
			// Path p = saveFile.toPath();
			// --------------產生String-----------------------------
			for (int i = 0; i < count; i++) {
				try {
					admOpLogEx = admOpLogList.get(i);
					// Date today = format.parse(admOpLogEx.getADTXDATE()+admOpLogEx.getADTXTIME());
					// Opt_Dttm = dateformat.format(today).toString();
					tempDate = admOpLogEx.getADTXDATE();
					tempTime = admOpLogEx.getADTXTIME();
					Opt_Dttm = tempDate.substring(0, 4) + "-" + tempDate.substring(4, 6) + "-"
							+ tempDate.substring(6, 8) + " " + tempTime.substring(0, 2) + ":" + tempTime.substring(2, 4)
							+ ":" + tempTime.substring(4, 6);

					Opt_Type_Cd = admOpLogEx.getADOPITEM().equals(" ") ? "" : "0" + admOpLogEx.getADOPITEM();

					User_ID = admOpLogEx.getADUSERID();

					ADOPNAME = sysOp.get(admOpLogEx.getADOPID().trim()) == null ? ""
							: sysOp.get(admOpLogEx.getADOPID().trim());

					ADEXCODE = admOpLogEx.getADEXCODE();

					if (admOpLogEx.getADBCHCON() != null && admOpLogEx.getADBCHCON().length() > 0) {
						jsonMap = JSONUtils.json2map(admOpLogEx.getADBCHCON());
						// 新舊後臺程式KEY不同 在此作不同處理
						// (舊)
						Biz_Unit_Num = jsonMap.get("Branch") == null ? "" : jsonMap.get("Branch");
						Party_ID = jsonMap.get("ADUSERID") == null ? "" : jsonMap.get("ADUSERID");
						adtDate = jsonMap.get("ADTDATE") == null ? "" : jsonMap.get("ADTDATE");
						adfDate = jsonMap.get("ADFDATE") == null ? "" : jsonMap.get("ADFDATE");
						// (新)
						Party_ID = jsonMap.get("USERID") == null ? "" : jsonMap.get("USERID");
						adtDate = jsonMap.get("ENDDATE") == null ? "" : jsonMap.get("ENDDATE");
						adfDate = jsonMap.get("STARTDATE") == null ? "" : jsonMap.get("STARTDATE");
					}

					User_IP = admOpLogEx.getADUSERIP();

					Opt_Txt = User_ID + Opt_Type_Cd + ADOPNAME + adtDate + adfDate + ADEXCODE;

					writeFileString.append(String.format("%1$-19s", Opt_Dttm));
					writeFileString.append(String.format("%1$-50s", Opt_Type_Cd));
					writeFileString.append(String.format("%1$-20s", Biz_Unit_Num));
					writeFileString.append(String.format("%1$-20s", User_ID));
					writeFileString.append(String.format("%1$-50s", Party_ID));
					writeFileString.append(String.format("%1$-40s", User_IP));
					writeFileString.append(String.format("%1$-500s", Opt_Txt));
					writeFileString.append("\r\n");

					if ((i + 1) % 10000 == 0) {
						// fwriter.write(writeFileString.toString());
						// Files.write(p, writeFileString.toString().getBytes("BIG5"));
						BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(saveFile));
						byte[] strToBytes = writeFileString.toString().getBytes("BIG5");
						outputStream.write(strToBytes);
						outputStream.close();
						writeFileString.delete(0, writeFileString.length());
						log.debug("UserActionRecord.java For Loop writeFileString:  " + i);
					}
					bc.incSuccess();
					bc.incTotalCount();
				} catch (Exception e) {
					log.debug("UserActionRecord.java ERROR :" + e.getMessage());
					log.debug("UserActionRecord.java ERROR : i=" + i + ", datetime:" + admOpLogEx.getADTXDATE()
							+ admOpLogEx.getADTXTIME());
					bc.incFail();
					bc.incTotalCount();
				}
			}
			log.debug("UserActionRecord.java For Loop create writeFileString Sccess");
			// ---------------------------------------------------------------------------

			// fwriter.write(writeFileString.toString());
			// Files.write(p, writeFileString.toString().getBytes("BIG5"));
			BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(saveFile));
			byte[] strToBytes = writeFileString.toString().getBytes("BIG5");
			outputStream.write(strToBytes);
			outputStream.close();
			// fwriter.flush();
			// fwriter.close();
			log.debug("UserActionRecord.java fwriter.write  Sccess");
			data.put("COUNTER", bc);
			data.put("WriteFile", "Sccess");
			// ------------------

			if ("Y".equals(doFTP)) {
				String ip = crmn_ftp_ip;
				FtpBase64 ftpBase641 = new FtpBase64(ip);
				int rc = ftpBase641.upload(despath + "NNB_LOG." + yesterday + ".TXT", new FileInputStream(saveFile));
				if (rc == 0) {
					// 測試中暫時註解
					File deleteFile = new File(srcpath + "NNB_LOG." + yesterday + ".TXT");
					deleteFile.setWritable(true, true);
					deleteFile.setReadable(true, true);
					deleteFile.setExecutable(true, true);
					deleteFile.delete();
				} else {
					batchresult.setSuccess(false);
					data.put("Error", "FTP failed");
					batchresult.setData(data);
				}
				log.debug("UserActionRecord.java FTP UPLOAD  Sccess");
			}
		} catch (Exception e) {
			log.error("執行 Batch 有誤 !", e);
			data.put("Error", e.getMessage());
			batchresult.setSuccess(false);
		}

		data.put("COUNTER", bc);
		batchresult.setData(data);

		try {
			if (batchresult.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName, batchresult.getData());
			}
		} catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}

		return batchresult;
	}

	public Map getSYSOP() {

		List<SYSOP> qresult = sysOpDao.findAll();
		int size = qresult.size();
		Map<String, String> map = new HashMap();
		for (int i = 0; i < size; i++) {
			map.put(qresult.get(i).getADOPID().trim(), qresult.get(i).getADOPNAME().trim());
		}

		return map;
	}

}
