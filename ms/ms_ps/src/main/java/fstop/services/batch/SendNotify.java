package fstop.services.batch;

import java.io.UnsupportedEncodingException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.security.cert.X509Certificate;

import org.apache.log4j.Logger;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.ESAPIUtil;

import fstop.notification.Notification;
import fstop.notification.Notify;
import fstop.notification.PushData;
import fstop.notification.TopicLabel;
import fstop.orm.dao.AdmMrktDao;
import fstop.orm.dao.SysParamDataDao;
import fstop.orm.dao.TxnPhoneTokenDao;
import fstop.orm.po.ADMMRKT;
import fstop.orm.po.TXNPHONETOKEN;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 行銷活動訊息推播通知
 * 
 * @author Owner 每天整點10,11,14,15,16,17小時00分自動執行
 * 
 */
@Slf4j
@RestController
@Batch(id = "batch.SendNotify", name = "行銷活動訊息推播通知", description = "行銷活動訊息推播通知")
public class SendNotify implements BatchExecute {
	private Logger log = Logger.getLogger(getClass());
	
//	private Map setting;
	private SysParamDataDao sysParamDataDao;
	
	private AdmMrktDao admMrktDao;
	
	private TxnPhoneTokenDao txnPhoneTokenDao;
	
//	private List<TXNPHONETOKEN> listAndroid;
//	private List<TXNPHONETOKEN> listiOS;
//	
//	private Date notifydate;
	
	public SendNotify(){}
	
	/**
	 * args[0] method name
	 */
	@RequestMapping(value = "/batch/sendnotify", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	@Override
	public BatchResult execute(@RequestBody List<String> args) {
		Hashtable<String, String> params = new Hashtable();
			 
		Map<String, Object> data = new HashMap();
		data.put("COUNTER", new BatchCounter());

		
		// 取得要推播的訊息
		List<ADMMRKT> listAdmMrkt = getAdmMrkt();
		if(listAdmMrkt.size()>0){
			// 取得 Android Token 並推播
			//getAndroidAndNotify(listAdmMrkt);
			//log.debug("Total Android Device=========>" + listAndroid.size());
					
			// 取得 iOS Token 並推播
			getiOSAndNotify(listAdmMrkt);
			
			// 推播到 IBM Messagesight 主機
			getAndroidAndNotifyToMQTT(listAdmMrkt);

		}


		
		// 批次完成
		BatchResult batchresult = new BatchResult();
		batchresult.setSuccess(true);
		batchresult.setBatchName(getClass().getSimpleName());
		batchresult.setData(data);

		return batchresult;

	}
	
	public List<ADMMRKT> getAdmMrkt(){
		log.info("getAdmMrkt Start");
		List<ADMMRKT> listAdmMrkt = null;
		try {
			admMrktDao.Reset();
			// 取得目前需要推播的訊息
			listAdmMrkt = admMrktDao.getNotify();
			if(listAdmMrkt.size()>0) {
				for(ADMMRKT admmrkt : listAdmMrkt){
					log.debug(ESAPIUtil.vaildLog(admmrkt.getMTADHLK()));
					log.debug(ESAPIUtil.vaildLog(admmrkt.getMTADTYPE()));
				}
				log.info(ESAPIUtil.vaildLog( listAdmMrkt.size() +" Notifies Need to be send"));
			}else {
				log.info("Nothing to Send");
			}
			
		}
		catch(Exception e){
			log.error("getAdmMrkt Error=========>" + e.toString());
		}
		log.info("getAdmMrkt End");
		return listAdmMrkt!=null?listAdmMrkt:new ArrayList<>();
	}
	
	public void getAndroidAndNotify(List<ADMMRKT> admMrkt){
		log.debug("getAndroidAndNotify Start");
		List<Object> arrdevices = new ArrayList<Object>();
		List<TXNPHONETOKEN> listAndroid = new ArrayList<TXNPHONETOKEN>();
		try {
			listAndroid = txnPhoneTokenDao.getAndroid();
			for(TXNPHONETOKEN txnphonetoken : listAndroid){
				log.debug(ESAPIUtil.vaildLog("Android PHONETOKEN=========>" + txnphonetoken.getPHONETOKEN()));
				arrdevices.add(txnphonetoken.getPHONETOKEN());
			}
			
//			for (int i = 0; i < 30000; i++) {
//				UUID uuid = UUID.randomUUID();   
//				arrdevices.add(uuid.toString());
//			}
			
			// Start IBM jre 在ssl連線時，預設沒有設定SSLSocketFactoryImpl
//			Security.setProperty("ssl.SocketFactory.provider", "com.ibm.jsse2.SSLSocketFactoryImpl");
//			Security.setProperty("ssl.ServerSocketFactory.provider", "com.ibm.jsse2.SSLServerSocketFactoryImpl");
						
			
			TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				public void checkClientTrusted(X509Certificate[] certs,
						String authType) {
				}

				public void checkServerTrusted(X509Certificate[] certs,
						String authType) {
				}

				public void checkClientTrusted(java.security.cert.X509Certificate[] arg0, String arg1) throws CertificateException {
					// TODO 自動產生方法 Stub
					
				}

				public void checkServerTrusted(java.security.cert.X509Certificate[] arg0, String arg1) throws CertificateException {
					// TODO 自動產生方法 Stub
					
				}
			} };

			// Install the all-trusting trust manager
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			// Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {

				public boolean verify(String arg0, SSLSession arg1) {
					// TODO 自動產生方法 Stub
					return true;
				}
				
			};

			// Install the all-trusting host verifier
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
		
			Notification notification = new Notification();
			log.debug("GCM Start");			
			for(ADMMRKT admmrkt : admMrkt){
				log.debug("admmrkt.getMTADHLK=========>" + admmrkt.getMTADHLK());	
				log.debug("arrdevices.size=========>" + arrdevices.size());
				
				// 訊息格式 4@@88@@新年快樂@@http://www.tbb.com.tw
//				String sendMessage = admmrkt.getMTADTYPE()+ "@@" + String.valueOf(admmrkt.getMTID()) + "@@" + admmrkt.getMTADHLK();
//				if (admmrkt.getMTADTYPE().equals("4") && !admmrkt.getMTLINK().equals("")){
//					sendMessage += "@@" + admmrkt.getMTLINK();
//				}
//				log.debug("sendMessage=========>" + sendMessage);
				
				
				Map<String, String> messageMap = new HashMap<String, String>();
				messageMap.put("TYPE", "AD");
				messageMap.put("MTADTYPE", admmrkt.getMTADTYPE());
				messageMap.put("MTID", String.valueOf(admmrkt.getMTID()));
				messageMap.put("MTADHLK", admmrkt.getMTADHLK());
				messageMap.put("MTLINK", admmrkt.getMTADDRESS());
				String sendMessage = JSONUtils.map2json(messageMap);
				
				
				notification.notificationToAndroid(arrdevices, sendMessage);
				
				// 將推播訊息的狀態改為已發送
				Date notifydate = new Date();
				String str_CurrentDate = DateTimeUtils.format("yyyyMMdd", notifydate);	
				String str_CurrentTime = DateTimeUtils.format("HHmmss", notifydate);
				admmrkt.setMTNFDATE(str_CurrentDate);
				admmrkt.setMTNFTIME(str_CurrentTime);				
				log.debug("admmrkt.getMTNFDATE=========>" + admmrkt.getMTNFDATE());	
				log.debug("admmrkt.getMTNFTIME=========>" + admmrkt.getMTNFTIME());
				
				admMrktDao.setPo(admmrkt);
				admMrktDao.registerSendedNotify();
				
			}			
			log.debug("GCM End");
			
			
			//notification.notificationToAndroid(arrdevices, "台灣中小企銀推播測試");
			//notification.notificationToAndroid(arrdevices, "1@@網銀促銷推播@@http://tw.yahoo.com");
			//notification.notificationToAndroid(arrdevices, "2@@信用卡推播@@http://tw.yahoo.com");
			//notification.notificationToAndroid(arrdevices, "3@@行銷活動推播@@http://tw.yahoo.com");
			//notification.notificationToAndroid(arrdevices, "4@@新年快樂@@http://tw.yahoo.com");

			
		}
		catch(Exception e){
			log.debug("getAndroidAndNotify Error=========>" + e.toString());
		}
		log.debug("getAndroidAndNotify End");
	}
	
	public void getiOSAndNotify(List<ADMMRKT> admMrkt){
		log.info("getiOSAndNotify Start");
		List<Object> arrdevices = new ArrayList<Object>();
		List<Object> arruser = new ArrayList<Object>();
		List<TXNPHONETOKEN> listiOS = new ArrayList<TXNPHONETOKEN>();
		
		try {
			listiOS = txnPhoneTokenDao.getIOS();
			for(TXNPHONETOKEN txnphonetoken : listiOS){
				log.debug(ESAPIUtil.vaildLog("iOS PHONETOKEN=========>" + txnphonetoken.getPHONETOKEN()));
				arrdevices.add(txnphonetoken.getPHONETOKEN());
				arruser.add(txnphonetoken.getDPUSERID());
			}
			
//			for (int i = 0; i < 30000; i++) {
//				UUID uuid = UUID.randomUUID();   
//				arrdevices.add(uuid.toString());
//			}
			
			// IBM jre 在ssl連線時，預設沒有設定SSLSocketFactoryImpl
			//Security.setProperty("ssl.SocketFactory.provider", "com.ibm.jsse2.SSLSocketFactoryImpl");
			//Security.setProperty("ssl.ServerSocketFactory.provider", "com.ibm.jsse2.SSLServerSocketFactoryImpl");

//			Notification notification = (Notification)SpringBeanFactory.getBean("notification");
			Notification notification = new Notification();
			log.info("APNS Start");			
			for(ADMMRKT admmrkt : admMrkt){
				log.info(ESAPIUtil.vaildLog("admmrkt.getMTADHLK=========>" + admmrkt.getMTADHLK()));	
				log.info(ESAPIUtil.vaildLog("arrdevices.size=========>" + arrdevices.size()));
				log.info(ESAPIUtil.vaildLog("admmrkt.getMTADTYPE=========>" + admmrkt.getMTADTYPE()));
				notification.notificationToiOS(arruser, arrdevices, admmrkt.getMTADHLK(),
														   admmrkt.getMTADTYPE(),
														   admmrkt.getMTID(),
														   admmrkt.getMTADDRESS());
			}			
			log.info("APNS End");
			
		}
		catch(Exception e){
			log.info("getiOSAndNotify Error=========>" + e.toString());
		}
		log.info("getiOSAndNotify End");
	}
	
	public void getAndroidAndNotifyToMQTT(List<ADMMRKT> admMrkt){
		boolean result = false;
		String token=null;		
		log.info("getAndroidAndNotifyToMQTT Start");
		List<PushData> listPushData = new ArrayList<PushData>();
		
		
		try {
			List<TXNPHONETOKEN> listAndroid = txnPhoneTokenDao.getAndroid();
		
			for(ADMMRKT admmrkt : admMrkt){				
				for(TXNPHONETOKEN txnphonetoken : listAndroid){
					log.info(ESAPIUtil.vaildLog("Android PHONETOKEN=========>" + txnphonetoken.getPHONETOKEN()));
					token = txnphonetoken.getPHONETOKEN();				
					
					Map<String, String> adMap = new HashMap<String, String>();
					adMap.put("MTADTYPE", admmrkt.getMTADTYPE());
					adMap.put("MTID", String.valueOf(admmrkt.getMTID()));
					adMap.put("MTADHLK", admmrkt.getMTADHLK());
					
					Map<String, String> messageMap = new HashMap<String, String>();
					messageMap.put("TYPE", "AD");
					messageMap.put("MESSAGE", JSONUtils.map2json(adMap));
					messageMap.put("LINK", admmrkt.getMTADDRESS());
	
					String sendMessage = JSONUtils.map2json(messageMap);	
					PushData pushData = new PushData();
					pushData.setMessage(sendMessage);					
					pushData.setTopic(TopicLabel.TOPICLABEL_ID + token);
					pushData.setCusId(txnphonetoken.getDPUSERID());
					pushData.setToken(token);
					
					listPushData.add(pushData);					
				}	
				
//				Notify notify = (Notify)SpringBeanFactory.getBean("pushNotifier");
				Notify notify = new Notify();
				try {
					notify.publish(listPushData);
					result = true;
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
					result = false;
				} catch (MqttException e) {
					e.printStackTrace();
					result = false;
				}
				
				listPushData.clear();
				
				// 將推播訊息的狀態改為已發送
				Date notifydate = new Date();
				String str_CurrentDate = DateTimeUtils.format("yyyyMMdd", notifydate);	
				String str_CurrentTime = DateTimeUtils.format("HHmmss", notifydate);
				admmrkt.setMTNFDATE(str_CurrentDate);
				admmrkt.setMTNFTIME(str_CurrentTime);	
				admMrktDao.setPo(admmrkt);
				admMrktDao.registerSendedNotify();				
			}			
			log.info("getAndroidAndNotifyToMQTT End");
		}
		catch(Exception e){
			log.error("getAndroidAndNotifyToMQTT Error=========>" + e.toString());
		}
	}
}
