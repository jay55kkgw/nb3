package fstop.services.batch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.ESAPIUtil;

import fstop.notifier.NotifyAngent;
import fstop.orm.dao.AdmHolidayDao;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.TxnFundDataDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.ADMHOLIDAY;
import fstop.orm.po.TXNFUNDDATA;
import fstop.orm.po.TXNUSER;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import topibs.utility.NumericUtil;

/**
 * 基金
 * 
 * @author Owner
 *
 */
@Slf4j
@RestController
@Batch(id = "batch.c050", name = "基金預約交易結果通知", description = "基金預約交易結果通知(讀基金主機下傳的檔案，更新DB)")
public class C050 implements BatchExecute {

	@Value("${batch.hostfile.path:}")
	private String pathname;

	@Value("${host.hostdata.path}")
	private String hostpath;

	@Autowired
	private AdmHolidayDao admholidayDao;

	@Autowired
	private TxnUserDao txnUserDao;

	@Autowired
	private TxnFundDataDao txnFundDataDao;
	
	@Autowired
	private SysBatchDao sysBatchDao;
	
	private Map varmail = new HashMap();

	@RequestMapping(value = "/batch/c050", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {
		
		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}
		
		List<String> safeArgs = ESAPIUtil.validStrList(args);
		Date d = new Date();
		String today = DateTimeUtils.getCDateShort(d);
		
		BatchResult batchresult = new BatchResult();
		batchresult.setBatchName(getClass().getSimpleName());
		Map<String, Object> data = new HashMap();
		
		log.debug("Batch_Execute_Date = " + today);

		ADMHOLIDAY holiday = null;
		try {
			String holidaytoday = DateTimeUtils.getDateShort(d);
			holiday = (ADMHOLIDAY) admholidayDao.findById(holidaytoday);
			log.debug("Holiday = " + holiday);
		} catch (Exception e) {
			holiday = null;
		}

		if (holiday != null) {
			data.put("__TOPMSG", "ZXXX");
			data.put("__TOPMSGTEXT", "非營業日．");
			log.debug("__TOPMSGTEXT = " + "非營業日．");
			batchresult.setSuccess(false);
			batchresult.setData(data);
		} else {
			// 2019/10/09 修改成讀檔

			String filePath = pathname + "C050F.TXT";
			log.debug("filePath >>{}", filePath);

			// For Error log
			String hostfilePath = hostpath + "C050F.TXT";

			batchresult = readFile(filePath, hostfilePath);
		}
		
		try {
			if (batchresult.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName,batchresult.getData());
			}
		}catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}

		return batchresult;
	}

	private BatchResult readFile(String tempfile, String hostpath) {

		List<Map<String, String>> fundtype1011L = new ArrayList();

		List<Map<String, String>> fundtype2021L = new ArrayList();

		List<Map<String, String>> fundtype3031L = new ArrayList();

		List<Map<String, String>> fundtype99L = new ArrayList();

		BatchResult batchresult = new BatchResult();
		Map<String, Object> data = new HashMap();
		BatchCounter bc = new BatchCounter();
	
		InputStreamReader in = null;
		BufferedReader bin = null;
		String subject;

		try {
			
			File file= new File(tempfile);
			//Incorrect Permission Assignment For Critical Resources
			file.setWritable(true,true);
			file.setReadable(true, true);
			file.setExecutable(true,true);
			long timestamp = file.lastModified();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String fmtDate = sdf.format(timestamp);
			log.info("File's LastModify fmt >> {}",fmtDate);
			Date d = new Date();
			String today = DateTimeUtils.getDateShort(d);
			
			if(!today.equals(fmtDate)) {
				data.put("MSG", "C050F.TXT MODIFY TIME NOT TODAY , SKIP EXECUTE !");
				batchresult.setSuccess(true);
				batchresult.setBatchName(this.getClass().getSimpleName());
				batchresult.setData(data);
				return batchresult;
			}
			
			
			in = new InputStreamReader(new FileInputStream(tempfile), "BIG5");
			bin = new BufferedReader(in);
			int succcnt = 0;
			int failcnt = 0;

			if (!bin.ready()) {
				in.close();
                bin.close();
            	throw new Exception();
			}

			while ((subject = ESAPIUtil.validInput(bin.readLine(), "GeneralString", true)) != null
					&& subject.length() != 0) {
				Map<String, String> fundtype1011M = new HashMap();
				Map<String, String> fundtype2021M = new HashMap();
				Map<String, String> fundtype3031M = new HashMap();
				Map<String, String> fundtype99M = new HashMap();

				List<TXNUSER> listtxnUser;
				String strtemp = subject;
				log.trace(ESAPIUtil.vaildLog("C050 LINE STR:" + subject));
				String idn = strtemp.substring(0, strtemp.indexOf("#|")).trim();
				strtemp = strtemp.substring(strtemp.indexOf("#|") + 2);

				String idnstart = StrUtils.left(idn, idn.length() - 6) + "***" + StrUtils.right(idn, 3);

				String fundtype = strtemp.substring(0, strtemp.indexOf("#|")).trim();
				strtemp = strtemp.substring(strtemp.indexOf("#|") + 2);

				String dataString = strtemp.trim() + "                    ";
				log.debug(ESAPIUtil.vaildLog("dataString >> {}" + dataString));
				String email = new String();

				log.debug(ESAPIUtil.vaildLog("User ID = " + idn));
				log.debug(ESAPIUtil.vaildLog("User idnstart = " + idnstart));
				log.debug(ESAPIUtil.vaildLog("fundtype = " + fundtype));
				listtxnUser = txnUserDao.findByUserId(idn);

				log.debug("listtxnUser.size() = " + listtxnUser.size());
				if (listtxnUser == null || listtxnUser.size() == 0) {
					log.debug(ESAPIUtil.vaildLog("User ID not found = " + idn));
					continue;
				}

				TXNUSER txnuser = new TXNUSER();
				txnuser = listtxnUser.get(0);
				String dpnotify = StrUtils.trim(txnuser.getDPNOTIFY());
				Set<String> dpnotifySet = StrUtils.splitToSet("\\s*,\\s*", dpnotify);

				boolean isUserSetTxnNotify = dpnotifySet.contains("7");
				if (isUserSetTxnNotify == false) {
					continue;
				}

				email = txnuser.getDPMYEMAIL();
				if (fundtype.equals("10") || fundtype.equals("11")) {

					fundtype1011M = setfundtype1011(fundtype, dataString, fundtype1011M);
					fundtype1011L.add(fundtype1011M);

				} else if (fundtype.equals("20") || fundtype.equals("21")) {

					fundtype2021M = setfundtype2021(fundtype, dataString, fundtype2021M);
					fundtype2021L.add(fundtype2021M);

				} else if (fundtype.equals("30") || fundtype.equals("31")) {

					fundtype3031M = setfundtype3031(fundtype, dataString, fundtype3031M);
					fundtype3031L.add(fundtype3031M);

				} else if (fundtype.equals("99")) {

					fundtype99M = setfundtype99(fundtype, dataString, fundtype99M);
					fundtype99L.add(fundtype99M);

					int sendR = SendMail(idnstart, email, fundtype1011L, fundtype2021L, fundtype3031L, fundtype99L);
					fundtype1011L.clear();
					fundtype2021L.clear();
					fundtype3031L.clear();
					fundtype99L.clear();

					if (sendR == 0) {
						succcnt++;
					} else {
						failcnt++;
					}

				}
			}

			bc.setSuccessCount(succcnt);
			bc.setFailCount(failcnt);
			data.put("COUNTER", bc);

			batchresult.setSuccess(true);
			batchresult.setBatchName(this.getClass().getSimpleName());
			batchresult.setData(data);
			
			log.debug("C050.java okcnt:" + succcnt + " failcnt:" + failcnt);
			
		} catch (FileNotFoundException e) {
			data.put("Error", "找不到檔案(" + hostpath + ")");
			batchresult.setData(data);
			batchresult.setSuccess(false);
		} catch (IOException e) {
			data.put("Error", "讀取檔案錯誤(" + hostpath + ")");
			batchresult.setData(data);
			batchresult.setSuccess(false);
		}catch (Exception e) {
			data.put("Error", "檔案為空(" + hostpath + ")");
			batchresult.setData(data);
			batchresult.setSuccess(false);
		} finally {
			try {
				in.close();
				bin.close();
			} catch (Exception e) {
			}
			in = null;
			bin = null;
			
			return batchresult;
		}

	}

	private Map<String, String> setfundtype1011(String fundtype, String input, Map<String, String> fundtype1011M) {
		fundtype1011M.put("FUNDTYPE", fundtype);
		String rtncod = input.substring(0, 4);
		fundtype1011M.put("RTNCOD", rtncod);
		String cdno = input.substring(4, 15);
		fundtype1011M.put("CDNO", cdno);
		String fund = input.substring(15, 19);
		fundtype1011M.put("FUND", fund);

		TXNFUNDDATA po = txnFundDataDao.findById(fund);
		String fundtext = po.getFUNDSNAME();

		log.debug(ESAPIUtil.vaildLog("fundtext= " + fundtext));
		log.debug("fundtext len = " + fundtext.length());
		fundtype1011M.put("FUNDTEXT", fundtext);
		String fundamt = input.substring(19, 32);
		log.debug("fundamt len = " + fundamt.length());
		String fundamtfmt = NumericUtil.formatNumberString(fundamt.trim(), 2);
		log.debug(ESAPIUtil.vaildLog("fundamtfmt = " + fundamtfmt));
		log.debug("fundamtfmt len = " + fundamtfmt.length());
		fundtype1011M.put("FUNDAMT", fundamtfmt.trim());
		String fundfee = input.substring(32, 43);
		String fundfeefmt = NumericUtil.formatNumberString(fundfee, 2);
		fundtype1011M.put("FUNDFEE", fundfeefmt);
		String totalamt = input.substring(43, 56);
		String totalamtfmt = NumericUtil.formatNumberString(totalamt, 2);
		fundtype1011M.put("TOTALAMT", totalamtfmt);
		String cry = input.substring(56);
		fundtype1011M.put("CRY", cry);
		return fundtype1011M;
	}

	private Map<String, String> setfundtype2021(String fundtype, String input, Map<String, String> fundtype2021M) {
		fundtype2021M.put("FUNDTYPE", fundtype);
		String rtncod = input.substring(0, 4);
		fundtype2021M.put("RTNCOD", rtncod);
		String cdno = input.substring(4, 15);
		fundtype2021M.put("CDNO", cdno);
		String fund = input.substring(15, 19);
		fundtype2021M.put("FUND", fund);
		TXNFUNDDATA po = txnFundDataDao.findById(fund);
		String fundtext = po.getFUNDSNAME();
		fundtype2021M.put("FUNDTEXT", fundtext);
		String fundin = input.substring(19, 23);
		TXNFUNDDATA po2 = txnFundDataDao.findById(fundin);
		String fundintext = po.getFUNDSNAME();
		fundtype2021M.put("FUNDINTEXT", fundintext);
		String fundamt = input.substring(23, 36);
		String fundamtfmt = "                 " + NumericUtil.formatNumberString(fundamt, 2);
		fundtype2021M.put("FUNDAMT", StrUtils.right(fundamtfmt, 17));
		String fundfee1 = input.substring(36, 47);
		String fundfee1fmt = "              " + NumericUtil.formatNumberString(fundfee1, 2);
		fundtype2021M.put("FUNDFEE1", StrUtils.right(fundfee1fmt, 14));
		String fundfee2 = input.substring(47, 58);
		String fundfee2fmt = "              " + NumericUtil.formatNumberString(fundfee2, 2);
		fundtype2021M.put("FUNDFEE2", StrUtils.right(fundfee2fmt, 14));
		String fundfee3 = input.substring(58, 69);
		String fundfee3fmt = "              " + NumericUtil.formatNumberString(fundfee3, 2);
		fundtype2021M.put("FUNDFEE3", StrUtils.right(fundfee3fmt, 14));
		String totalamt = input.substring(69, 80);
		String totalamtfmt = "              " + NumericUtil.formatNumberString(totalamt, 2);
		fundtype2021M.put("TOTALAMT", StrUtils.right(totalamtfmt, 14));
		String cry = input.substring(80);
		fundtype2021M.put("CRY", cry);
		return fundtype2021M;
	}

	private Map<String, String> setfundtype3031(String fundtype, String input, Map<String, String> fundtype3031M) {
		fundtype3031M.put("FUNDTYPE", fundtype);
		String rtncod = input.substring(0, 4);
		fundtype3031M.put("RTNCOD", rtncod);
		String cdno = input.substring(4, 15);
		fundtype3031M.put("CDNO", cdno);
		String fund = input.substring(15, 19);
		fundtype3031M.put("FUND", fund);
		TXNFUNDDATA po = txnFundDataDao.findById(fund);
		String fundtext = po.getFUNDSNAME();

		fundtype3031M.put("FUNDTEXT", fundtext);
		String fundamt = input.substring(19, 32);
		String fundamtfmt = "                 " + NumericUtil.formatNumberString(fundamt, 2);
		fundtype3031M.put("FUNDAMT", StrUtils.right(fundamtfmt, 17));
		String cry = input.substring(32);
		fundtype3031M.put("CRY", cry);
		return fundtype3031M;
	}

	private Map<String, String> setfundtype99(String fundtype, String input, Map<String, String> fundtype99M) {
		fundtype99M.put("FUNDTYPE", fundtype);
		String rtncod = input.substring(0, 1);
		fundtype99M.put("RTNCOD1", rtncod);
		String email = input.substring(1);
		fundtype99M.put("EMAIL", email);
		return fundtype99M;
	}

	int SendMail(String uid, String email, List<Map<String, String>> fundtype1011L,
			List<Map<String, String>> fundtype2021L, List<Map<String, String>> fundtype3031L,
			List<Map<String, String>> fundtype99L) {
		Map<String, String> type1011 = null;
		Map<String, String> type2021 = null;
		Map<String, String> type3031 = null;
		int count10 = 0;
		int count11 = 0;
		int count20 = 0;
		int count21 = 0;
		int count30 = 0;
		int count31 = 0;
		StringBuffer TextBuf = new StringBuffer();
		StringBuffer TextDetail = new StringBuffer();
		StringBuffer TextDetailFail = new StringBuffer();

		for (int i = 0; i < fundtype1011L.size(); i++) {
			type1011 = fundtype1011L.get(i);
			if (type1011.get("FUNDTYPE").equals("10")) {
				TextDetail.append("<TR>").append("\n");
				TextDetail.append("<TD width=20%><DIV align=left><FONT >");
				TextDetail.append(type1011.get("FUNDTEXT"));
				TextDetail.append("</FONT></DIV></TD>").append("\n");
				TextDetail.append("<TD width=15%><DIV align=right><FONT >");
				// TextDetail.append("&nbsp").append("\n");
				TextDetail.append(type1011.get("FUNDAMT"));
				TextDetail.append("</FONT></DIV></TD>").append("\n");
				// TextDetail.append("&nbsp").append("\n");
				TextDetail.append("<TD width=10%><DIV align=right><FONT >");
				TextDetail.append(type1011.get("FUNDFEE"));
				TextDetail.append("</FONT></DIV></TD>").append("\n");
				// TextDetail.append("&nbsp").append("\n");
				TextDetail.append("<TD width=10%><DIV align=right><FONT >");
				TextDetail.append(type1011.get("TOTALAMT"));
				TextDetail.append("</FONT></DIV></TD>").append("\n");
				// TextDetail.append("&nbsp").append("\n");
				TextDetail.append("<TD width=25%><DIV align=left><FONT >");
				TextDetail.append(type1011.get("CRY"));
				TextDetail.append("</FONT></DIV></TD>").append("\n");
				// TextDetail.append("&nbsp").append("\n");
				TextDetail.append("<TD width=20%><DIV align=left><FONT >");
				TextDetail.append(type1011.get("CDNO"));
				TextDetail.append("</FONT></DIV></TD>").append("\n");
				TextDetail.append("</TR>").append("\n");
				// System.out.println("type = 10 TextDetail = " + TextDetail.toString());
				count10++;
			} else {
				TextDetailFail.append("<TR>").append("\n");
				TextDetailFail.append("<TD width=20%><DIV align=left><FONT >");
				TextDetailFail.append(type1011.get("FUNDTEXT"));
				TextDetailFail.append("</FONT></DIV></TD>").append("\n");
				;
				TextDetailFail.append("<TD width=15%><DIV align=right><FONT >");
				// TextDetail.append("&nbsp").append("\n");
				TextDetailFail.append(type1011.get("FUNDAMT"));
				TextDetailFail.append("</FONT></DIV></TD>").append("\n");
				TextDetailFail.append("<TD width=10%><DIV align=right><FONT >");
				// TextDetailFail.append("&nbsp").append("\n");
				TextDetailFail.append(type1011.get("FUNDFEE"));
				TextDetailFail.append("</FONT></DIV></TD>").append("\n");
				;
				TextDetailFail.append("<TD width=10%><DIV align=right><FONT >");
				// TextDetailFail.append("&nbsp").append("\n");
				TextDetailFail.append(type1011.get("TOTALAMT"));
				TextDetailFail.append("</FONT></DIV></TD>").append("\n");
				TextDetailFail.append("<TD width=30%><DIV align=left><FONT >");
				// TextDetailFail.append("&nbsp").append("\n");
				TextDetailFail.append(type1011.get("CRY"));
				TextDetailFail.append("</FONT></DIV></TD>").append("\n");
				TextDetailFail.append("<TD width=20%><DIV align=left><FONT >");
				// TextDetailFail.append("&nbsp").append("\n");
				TextDetailFail.append(type1011.get("RTNCOD"));
				TextDetailFail.append("</FONT></DIV></TD>").append("\n");
				TextDetailFail.append("</TR>").append("\n");
				// System.out.println("type = 11 TextDetailFail = " +
				// TextDetailFail.toString());
				count11++;
			}

		}
		if (count10 > 0) {
			// TextBuf.append("<BR><TABLE align=center cellSpacing=0 cellPadding=0
			// frame=border width=650>");
			// TextBuf.append("<TBODY>").append("\n");
			TextBuf.append("<TABLE align=center cellSpacing=0 cellPadding=0  frame=border width=640>").append("\n");
			TextBuf.append("<TR><TD height=26><DIV align=center><FONT >");
			TextBuf.append("基金申購交易成功查詢明細表");
			TextBuf.append("</FONT></DIV></TD></TR>").append("\n");
			TextBuf.append("</TABLE>");
			TextBuf.append("<TABLE align=center cellSpacing=0 cellPadding=0  frame=border width=640>").append("\n");
			;
			TextBuf.append("<TR><TD width=20%><DIV align=left><FONT >");
			TextBuf.append("基金名稱");
			TextBuf.append("</FONT></DIV></TD>").append("\n");
			TextBuf.append("<TD width=15%><DIV align=right><FONT >");
			TextBuf.append("申購金額");
			TextBuf.append("</FONT></DIV></TD>").append("\n");
			TextBuf.append("<TD width=10%><DIV align=right><FONT >");
			TextBuf.append("手續費");
			TextBuf.append("</FONT></DIV></TD>").append("\n");
			TextBuf.append("<TD width=10%><DIV align=right><FONT >");
			TextBuf.append("扣款金額");
			TextBuf.append("</FONT></DIV></TD>").append("\n");
			TextBuf.append("<TD width=30%><DIV align=left><FONT >");
			TextBuf.append("幣別");
			TextBuf.append("</FONT></DIV></TD>").append("\n");
			TextBuf.append("<TD width=20%><DIV align=left><FONT >");
			TextBuf.append("信託帳號");
			TextBuf.append("</FONT></DIV></TD></TR>").append("\n");
			TextBuf.append(TextDetail);
			TextBuf.append("</TABLE>").append("\n");
			TextBuf.append("<TABLE align=center cellSpacing=0 cellPadding=0  frame=border width=640>").append("\n");
			TextBuf.append("<TR><TD height=26><DIV align=left><FONT >");
			TextBuf.append("申購交易成功查詢結果 共" + count10 + "筆");
			TextBuf.append("</FONT></DIV></TD></TR>").append("\n");
			TextBuf.append("</TABLE>").append("\n");
			// TextBuf.append("</TBODY>");
			// TextBuf.append("</TABLE>");
			TextDetail.setLength(0);
		}

		if (count11 > 0) {
			// TextBuf.append("<BR><TABLE align=center cellSpacing=0 cellPadding=0
			// frame=border width=650>");
			// TextBuf.append("<TBODY>");
			TextBuf.append("<TABLE align=center cellSpacing=0 cellPadding=0  frame=border width=640>").append("\n");
			TextBuf.append("<TR><TD height=26><DIV align=center><FONT >");
			TextBuf.append("基金申購交易失敗查詢明細表");
			TextBuf.append("</FONT></DIV></TD></TR>").append("\n");
			TextBuf.append("</TABLE>").append("\n");
			TextBuf.append("<TABLE align=center cellSpacing=0 cellPadding=0  frame=border width=640>").append("\n");
			TextBuf.append("<TR><TD width=20%><DIV align=left><FONT >");
			TextBuf.append("基金名稱");
			TextBuf.append("</FONT></DIV></TD>").append("\n");
			TextBuf.append("<TD width=15%><DIV align=right><FONT >");
			TextBuf.append("申購金額");
			TextBuf.append("</FONT></DIV></TD>").append("\n");
			TextBuf.append("<TD width=10%><DIV align=right><FONT >");
			TextBuf.append("手續費");
			TextBuf.append("</FONT></DIV></TD>").append("\n");
			TextBuf.append("<TD width=10%><DIV align=right><FONT >");
			TextBuf.append("扣款金額");
			TextBuf.append("</FONT></DIV></TD>").append("\n");
			TextBuf.append("<TD width=30%><DIV align=left><FONT >");
			TextBuf.append("幣別");
			TextBuf.append("</FONT></DIV></TD>").append("\n");
			TextBuf.append("<TD width=20%><DIV align=left><FONT >");
			TextBuf.append("回應訊息");
			TextBuf.append("</FONT></DIV></TD></TR>").append("\n");
			TextBuf.append(TextDetailFail);
			TextBuf.append("</TABLE>").append("\n");
			TextBuf.append("<TABLE align=center cellSpacing=0 cellPadding=0  frame=border width=640>").append("\n");
			TextBuf.append("<TR><TD height=26><DIV align=left><FONT >");
			TextBuf.append("申購交易失敗查詢結果 共" + count11 + "筆");
			TextBuf.append("</FONT></DIV></TD></TR>").append("\n");
			TextBuf.append("</TABLE>").append("\n");
			// TextBuf.append("</TBODY>");
			// TextBuf.append("</TABLE>");
			TextDetailFail.setLength(0);
		}
		for (int i = 0; i < fundtype2021L.size(); i++) {
			type2021 = (Hashtable) fundtype2021L.get(i);

			if (type2021.get("FUNDTYPE").equals("20")) {
				TextDetail.append("<TR>").append("\n");
				TextDetail.append("<TD width=10%><DIV align=left><FONT >");
				TextDetail.append(type2021.get("CDNO"));
				TextDetail.append("</FONT></DIV></TD>").append("\n");
				TextDetail.append("<TD width=20%><DIV align=left><FONT >");
				// TextDetail.append("&nbsp").append("\n");
				TextDetail.append(type2021.get("FUNDTEXT"));
				TextDetail.append("</FONT></DIV></TD>").append("\n");
				TextDetail.append("<TD width=20%><DIV align=left><FONT >");
				// TextDetail.append("&nbsp").append("\n");
				TextDetail.append(type2021.get("FUNDINTEXT"));
				TextDetail.append("</FONT></DIV></TD>").append("\n");
				TextDetail.append("<TD width=10%><DIV align=right><FONT >");
				// TextDetail.append("&nbsp").append("\n");
				TextDetail.append(type2021.get("FUNDAMT"));
				TextDetail.append("</FONT></DIV></TD>").append("\n");
				TextDetail.append("<TD width=10%><DIV align=right><FONT >");
				// TextDetail.append("&nbsp").append("\n");
				TextDetail.append(type2021.get("FUNDFEE1"));
				TextDetail.append("</FONT></DIV></TD>").append("\n");
				TextDetail.append("<TD width=10%><DIV align=right><FONT >");
				// TextDetail.append("&nbsp").append("\n");
				TextDetail.append(type2021.get("FUNDFEE2"));
				TextDetail.append("</FONT></DIV></TD>").append("\n");
				TextDetail.append("<TD width=10%><DIV align=right><FONT >");
				// TextDetail.append("&nbsp").append("\n");
				TextDetail.append(type2021.get("TOTALAMT"));
				TextDetail.append("</FONT></DIV></TD>").append("\n");
				TextDetail.append("</TR>").append("\n");
				// System.out.println("type = 20 TextDetail = " + TextDetail.toString());
				count20++;
			} else {
				TextDetailFail.append("<TR>").append("\n");
				TextDetailFail.append("<TD width=10%><DIV align=left><FONT >");
				TextDetailFail.append(type2021.get("CDNO"));
				TextDetailFail.append("</FONT></DIV></TD>").append("\n");
				TextDetailFail.append("<TD width=10%><DIV align=left><FONT >");
				// TextDetailFail.append("&nbsp").append("\n");
				TextDetailFail.append(type2021.get("FUNDTEXT"));
				TextDetailFail.append("</FONT></DIV></TD>").append("\n");
				TextDetailFail.append("<TD width=20%><DIV align=left><FONT >");
				// TextDetailFail.append("&nbsp").append("\n");
				TextDetailFail.append(type2021.get("FUNDINTEXT"));
				TextDetailFail.append("</FONT></DIV></TD>").append("\n");
				TextDetailFail.append("<TD width=20%><DIV align=right><FONT >");
				// TextDetailFail.append("&nbsp").append("\n");
				TextDetailFail.append(type2021.get("FUNDAMT"));
				TextDetailFail.append("</FONT></DIV></TD>").append("\n");
				TextDetailFail.append("<TD width=10%><DIV align=right><FONT >");
				// TextDetailFail.append("&nbsp").append("\n");
				TextDetailFail.append(type2021.get("FUNDFEE1"));
				TextDetailFail.append("</FONT></DIV></TD>").append("\n");
				TextDetailFail.append("<TD width=10%><DIV align=right><FONT >");
				// TextDetailFail.append("&nbsp").append("\n");
				TextDetailFail.append(type2021.get("FUNDFEE2"));
				TextDetailFail.append("</FONT></DIV></TD>").append("\n");
				TextDetailFail.append("<TD width=10%><DIV align=right><FONT >");
				// TextDetailFail.append("&nbsp").append("\n");
				TextDetailFail.append(type2021.get("TOTALAMT"));
				TextDetailFail.append("</FONT></DIV></TD>").append("\n");
				TextDetailFail.append("<TD width=10%><DIV align=left><FONT >");
				// TextDetailFail.append("&nbsp").append("\n");
				TextDetailFail.append(type2021.get("RTNCOD"));
				TextDetailFail.append("</FONT></DIV></TD>").append("\n");
				TextDetailFail.append("<TR>").append("\n");
				// System.out.println("type = 21 TextDetailFail = " +
				// TextDetailFail.toString());
				count21++;
			}
		}
		if (count20 > 0) {
			// TextBuf.append("<BR><TABLE align=center cellSpacing=0 cellPadding=0
			// frame=border width=650>");
			// TextBuf.append("<TBODY>");
			TextBuf.append("<TABLE align=center cellSpacing=0 cellPadding=0  frame=border width=640>").append("\n");
			TextBuf.append("<TR><TD height=26><DIV align=center><FONT >");
			TextBuf.append("基金轉出交易成功查詢明細表");
			TextBuf.append("</FONT></DIV></TD></TR>").append("\n");
			TextBuf.append("</TABLE>").append("\n");
			TextBuf.append("<TABLE align=center cellSpacing=0 cellPadding=0  frame=border width=640>").append("\n");
			TextBuf.append("<TR><TD width=10%><DIV align=left><FONT >");
			TextBuf.append("信託帳號");
			TextBuf.append("</FONT></DIV></TD>").append("\n");
			TextBuf.append("<TD width=20%><DIV align=left><FONT >");
			TextBuf.append("轉出基金名稱");
			TextBuf.append("</FONT></DIV></TD>").append("\n");
			TextBuf.append("<TD width=20%><DIV align=left><FONT >");
			TextBuf.append("轉入基金名稱");
			TextBuf.append("</FONT></DIV></TD>").append("\n");
			TextBuf.append("<TD width=10%><DIV align=right><FONT >");
			TextBuf.append("轉出信託金額");
			TextBuf.append("</FONT></DIV></TD>").append("\n");
			TextBuf.append("<TD width=10%><DIV align=right><FONT >");
			TextBuf.append("補收手續費");
			TextBuf.append("</FONT></DIV></TD>").append("\n");
			TextBuf.append("<TD width=10%><DIV align=right><FONT >");
			TextBuf.append("本行轉換手續費");
			TextBuf.append("</FONT></DIV></TD>").append("\n");
			TextBuf.append("<TD width=10%><DIV align=right><FONT >");
			TextBuf.append("手續費總扣款金額");
			TextBuf.append("</FONT></DIV></TD></TR>").append("\n");
			TextBuf.append(TextDetail);
			TextBuf.append("</TABLE>").append("\n");
			TextBuf.append("<TABLE align=center cellSpacing=0 cellPadding=0  frame=border width=640>").append("\n");
			TextBuf.append("<TR><TD height=26><DIV align=left><FONT >");
			TextBuf.append("轉出交易成功查詢結果 共" + count20 + "筆");
			TextBuf.append("</FONT></DIV></TD></TR>").append("\n");
			TextBuf.append("</TABLE>").append("\n");
			// TextBuf.append("</TBODY>");
			// TextBuf.append("</TABLE>");
			TextDetail.setLength(0);
		}

		if (count21 > 0) {
			// TextBuf.append("<BR><TABLE align=center cellSpacing=0 cellPadding=0
			// frame=border width=650>");
			// TextBuf.append("<TBODY>");
			TextBuf.append("<TABLE align=center cellSpacing=0 cellPadding=0  frame=border width=640>").append("\n");
			TextBuf.append("<TR><TD height=26><DIV align=center><FONT >");
			TextBuf.append("基金轉出交易失敗查詢明細表");
			TextBuf.append("</FONT></DIV></TD></TR>").append("\n");
			TextBuf.append("</TABLE>").append("\n");
			TextBuf.append("<TABLE align=center cellSpacing=0 cellPadding=0  frame=border width=640>").append("\n");
			TextBuf.append("<TR><TD width=10%><DIV align=left><FONT >");
			TextBuf.append("信託帳號 ").append("\n");
			TextBuf.append("</FONT></DIV></TD>").append("\n");
			TextBuf.append("<TD width=20%><DIV align=left><FONT >");
			TextBuf.append("轉出基金名稱").append("\n");
			TextBuf.append("</FONT></DIV></TD>").append("\n");
			TextBuf.append("<TD width=20%><DIV align=left><FONT >");
			TextBuf.append("轉入基金名稱").append("\n");
			TextBuf.append("</FONT></DIV></TD>").append("\n");
			TextBuf.append("<TD width=10%><DIV align=right><FONT >");
			TextBuf.append("轉出信託金額").append("\n");
			TextBuf.append("</FONT></DIV></TD>").append("\n");
			TextBuf.append("<TD width=10%><DIV align=right><FONT >");
			TextBuf.append("補收手續費").append("\n");
			TextBuf.append("</FONT></DIV></TD>").append("\n");
			TextBuf.append("<TD width=10%><DIV align=right><FONT >");
			TextBuf.append("本行轉換手續費").append("\n");
			TextBuf.append("</FONT></DIV></TD>").append("\n");
			TextBuf.append("<TD width=10%><DIV align=right><FONT >");
			TextBuf.append("手續費總扣款金額").append("\n");
			TextBuf.append("</FONT></DIV></TD>").append("\n");
			TextBuf.append("<TD width=10%><DIV align=left><FONT >");
			TextBuf.append("回應訊息").append("\n");
			TextBuf.append("</FONT></DIV></TD></TR>").append("\n");
			TextBuf.append(TextDetailFail);
			TextBuf.append("</TABLE>").append("\n");
			TextBuf.append("<BR><TABLE align=center cellSpacing=0 cellPadding=0  frame=border width=640>").append("\n");
			TextBuf.append("<TR><TD height=26><DIV align=left><FONT >");
			TextBuf.append("<BR>轉出交易失敗查詢結果 共" + count21 + "筆");
			TextBuf.append("</FONT></DIV></TD></TR></TABLE>").append("\n");
			TextBuf.append("</FONT></DIV></TD></TR>").append("\n");
			TextBuf.append("</TABLE>").append("\n");
			// TextBuf.append("</TBODY>").append("\n");
			// TextBuf.append("</TABLE>").append("\n");
			TextDetailFail.setLength(0);
		}
		for (int i = 0; i < fundtype3031L.size(); i++) {
			type3031 = (Hashtable) fundtype3031L.get(i);
			if (type3031.get("FUNDTYPE").equals("30")) {
				TextDetail.append("<TR>").append("\n");
				TextDetail.append("<TD width=20%><DIV align=left><FONT >");
				TextDetail.append(type3031.get("FUNDTEXT"));
				TextDetail.append("</FONT></DIV></TD>").append("\n");
				TextDetail.append("<TD width=10%><DIV align=left><FONT >");
				TextDetail.append(type3031.get("CDNO"));
				TextDetail.append("</FONT></DIV></TD>").append("\n");
				TextDetail.append("<TD width=10%><DIV align=right><FONT >");
				TextDetail.append(type3031.get("FUNDAMT"));
				TextDetail.append("</FONT></DIV></TD>").append("\n");
				TextDetail.append("</TR>").append("\n");
				// System.out.println("type = 30 TextDetail = " + TextDetail.toString());
				count30++;
			} else {
				TextDetailFail.append("<TR>").append("\n");
				TextDetailFail.append("<TD width=20%><DIV align=left><FONT >");
				TextDetailFail.append(type3031.get("FUNDTEXT"));
				TextDetailFail.append("</FONT></DIV></TD>").append("\n");
				TextDetailFail.append("<TD width=10%><DIV align=left><FONT >");
				TextDetailFail.append(type3031.get("CDNO"));
				TextDetailFail.append("</FONT></DIV></TD>").append("\n");
				TextDetailFail.append("<TD width=10%><DIV align=right><FONT >");
				TextDetailFail.append(type3031.get("FUNDAMT"));
				TextDetailFail.append("</FONT></DIV></TD>").append("\n");
				TextDetailFail.append("<TD width=10%><DIV align=left><FONT >");
				TextDetailFail.append(type3031.get("RTNCOD"));
				TextDetailFail.append("</FONT></DIV></TD>").append("\n");
				TextDetailFail.append("</TR>").append("\n");
				// System.out.println("type = 31 TextDetailFail = " +
				// TextDetailFail.toString());
				count31++;
			}
		}
		if (count30 > 0) {
			// TextBuf.append("<BR><TABLE align=center cellSpacing=0 cellPadding=0
			// frame=border width=650>").append("\n");
			// TextBuf.append("<TBODY>").append("\n");
			TextBuf.append("<TABLE align=center cellSpacing=0 cellPadding=0  frame=border width=640>").append("\n");
			TextBuf.append("<TR><TD height=26><DIV align=center><FONT >");
			TextBuf.append("基金贖回交易成功查詢明細表");
			TextBuf.append("</FONT></DIV></TD></TR>").append("\n");
			TextBuf.append("</TABLE>").append("\n");
			TextBuf.append("<TABLE align=center cellSpacing=0 cellPadding=0  frame=border width=640>").append("\n");
			TextBuf.append("<TR><TD width=20%><DIV align=left><FONT >");
			TextBuf.append("基金名稱 ");
			TextBuf.append("</FONT></DIV></TD>").append("\n");
			TextBuf.append("<TD width=10%><DIV align=left><FONT >");
			TextBuf.append("信託號碼");
			TextBuf.append("</FONT></DIV></TD>").append("\n");
			TextBuf.append("<TD width=10%><DIV align=right><FONT >");
			TextBuf.append("贖回信託金額");
			TextBuf.append("</FONT></DIV></TD></TR>").append("\n");
			TextBuf.append(TextDetail);
			TextBuf.append("</TABLE>").append("\n");
			TextBuf.append("<TABLE align=center cellSpacing=0 cellPadding=0  frame=border width=640>").append("\n");
			TextBuf.append("<TR><TD height=26><DIV align=left><FONT >");
			TextBuf.append("贖回交易成功查詢結果 共" + count30 + "筆");
			TextBuf.append("</FONT></DIV></TD></TR>").append("\n");
			TextBuf.append("</TABLE>").append("\n");
			// TextBuf.append("</TBODY>").append("\n");
			// TextBuf.append("</TABLE>").append("\n");
			TextDetail.setLength(0);
		}

		if (count31 > 0) {
			// TextBuf.append("<BR><TABLE align=center cellSpacing=0 cellPadding=0
			// frame=border width=650>").append("\n");
			// TextBuf.append("<TBODY>").append("\n");
			TextBuf.append("<TABLE align=center cellSpacing=0 cellPadding=0  frame=border width=640>").append("\n");
			TextBuf.append("<TR><TD height=26><DIV align=center><FONT >");
			TextBuf.append("基金贖回交易失敗查詢明細表");
			TextBuf.append("</FONT></DIV></TD></TR>").append("\n");
			TextBuf.append("</TABLE>").append("\n");
			TextBuf.append("<TABLE align=center cellSpacing=0 cellPadding=0  frame=border width=640>").append("\n");
			TextBuf.append("<TR><TD width=20%><DIV align=left><FONT >");
			TextBuf.append("基金名稱");
			TextBuf.append("</FONT></DIV></TD>").append("\n");
			TextBuf.append("<TD width=10%><DIV align=left><FONT >");
			TextBuf.append("信託號碼");
			TextBuf.append("</FONT></DIV></TD>").append("\n");
			TextBuf.append("<TD width=10%><DIV align=left><FONT >");
			TextBuf.append("贖回信託金額");
			TextBuf.append("</FONT></DIV></TD>").append("\n");
			TextBuf.append("<TD width=10%><DIV align=left><FONT >");
			TextBuf.append("回應訊息");
			TextBuf.append("</FONT></DIV></TD></TR>").append("\n");
			TextBuf.append(TextDetailFail);
			TextBuf.append("</TABLE>").append("\n");
			TextBuf.append("<BR><TABLE align=center cellSpacing=0 cellPadding=0  frame=border width=640>").append("\n");
			TextBuf.append("<TR><TD height=26><DIV align=left><FONT >");
			TextBuf.append("贖回交易失敗查詢結果 共" + count31 + "筆");
			TextBuf.append("</FONT></DIV></TD></TR>").append("\n");
			TextBuf.append("</TABLE>").append("\n");
			// TextBuf.append("</TBODY>").append("\n");
			// TextBuf.append("</TABLE>").append("\n");
			TextDetailFail.setLength(0);
		}

		for (int i = 0; i < fundtype99L.size(); i++) {
			String MyMail = new String();

			Map<String, String> type = fundtype99L.get(i);
			String rtncod1 = (String) type.get("RTNCOD1");
			log.debug("RTNCOD1 = " + rtncod1);
			if (rtncod1.endsWith("1"))
				MyMail = (String) type.get("EMAIL");
			else if (email == null || email.length() == 0) {
				log.error("The email is not exist.");
				return (1);
			} else
				MyMail = email;
			log.debug("MyMail.replace(\";\", \",\").trim() = " + MyMail.replace(";", ",").trim());
			// varmail.put("MAILCUSIDNO",uid);
			varmail.put("MAILTEXT", TextBuf.toString());
			log.debug("MAILTEXT = " + TextBuf.toString());
			boolean isSendSuccess = NotifyAngent.sendNotice("C050", varmail, MyMail.replace(";", ",").trim());
			if (!isSendSuccess) {
				log.error(ESAPIUtil.vaildLog("C050 發送 Email 失敗.(" + MyMail + ")"));
			}
		}
		return (0);
	}

}
