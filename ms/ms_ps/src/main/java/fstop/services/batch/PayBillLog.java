package fstop.services.batch;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Date;

import org.apache.commons.lang.StringUtils;


import fstop.orm.dao.PayBillLogDao;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.po.PAYBILLLOG;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;

import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

import org.owasp.esapi.ESAPI;
import org.owasp.esapi.SafeFile;
import org.owasp.esapi.errors.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.ESAPIUtil;

/**
 * 
 * 產生qrcode信用卡繳費上月資料
 * 
 * @author Owner
 * 
 */
@Slf4j
@RestController
@Batch(id="batch.payBillLog", name = "產生qrcode信用卡繳費上月資料", description = "讀取繳費db，產生檔案，給中心讀取")
public class PayBillLog implements BatchExecute {


	@Autowired
	private PayBillLogDao payBillLogDao;
	
	@Value("${batch.hostfile.path:}")
	private String hostDir;
	
	public PayBillLog(){}
	
	@Autowired
	private SysBatchDao sysBatchDao;
	
	/**
	 * args[0] - Specific date (format: yyyyMMdd)
	 */
	@RequestMapping(value = "/batch/payBillLog", method = {RequestMethod.POST},
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {
		log.info("PayBillLog Execute - Start");

		BatchResult batchresult = new BatchResult();
		Map<String, Object> data = new LinkedHashMap();
		batchresult.setBatchName(getClass().getSimpleName());
		batchresult.setSuccess(true);

		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}
		
		List<String> safeArgs = ESAPIUtil.validStrList(args);
		int failcnt=0,okcnt=0,totalcnt=0;
		Hashtable rowcnt=new Hashtable();
		
		try {
			if (safeArgs.size() == 1) {
				log.info(ESAPIUtil.vaildLog("safeArgs[0]: " + safeArgs.get(0)));
				//Date date = DateTimeUtils.parse("yyyyMMdd", args[0]);
				exec(rowcnt, safeArgs.get(0));
			} else if(safeArgs.size() ==2) {
				log.info(ESAPIUtil.vaildLog("safeArgs[0]:"+safeArgs.get(0)));
				log.info(ESAPIUtil.vaildLog("safeArgs[1]:"+safeArgs.get(1)));
				exec(rowcnt,safeArgs.get(0),safeArgs.get(1));
				
			}
			else {
				exec(rowcnt);
			}
			failcnt=((Integer)rowcnt.get("FAILCNT")).intValue();
			okcnt=((Integer)rowcnt.get("OKCNT")).intValue();
			totalcnt=((Integer)rowcnt.get("TOTALCNT")).intValue();
			log.debug("failcnt = " + failcnt);
			log.debug("okcnt = " + okcnt);
			log.debug("totalcnt = " + totalcnt);
		}
		catch(Exception e) {
			log.error("執行 Batch 有誤 !", e);
			data.put("Error", e.getMessage());
			batchresult.setSuccess(false);
		}
		
		BatchCounter bcnt=new BatchCounter();
		bcnt.setFailCount(failcnt);
		bcnt.setSuccessCount(okcnt);
		bcnt.setTotalCount(totalcnt);


		data.put("COUNTER", bcnt);

		batchresult.setData(data);
		
		log.info("PayBillLog Execute - End");

		try {
			if (batchresult.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName, batchresult.getData());
			}
		} catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}
		return batchresult;
	}
	
	private void exec(Hashtable rowcnt) throws IOException {
		Date d = new Date();
		java.util.Calendar calendar = java.util.Calendar.getInstance();   
		calendar.setTime(d);
		String nowdate= DateTimeUtils.getDateShort(d);
		String printdate=String.valueOf(Integer.parseInt(nowdate.substring(0,4))-1911)+"/"+nowdate.substring(4,6)+"/"+nowdate.substring(6,8);
		calendar.add(calendar.MONTH,-1);
		d = calendar.getTime(); 
		String lastmonth= DateTimeUtils.getDateShort(d);
		lastmonth = lastmonth.substring(0, 6)+"%";
		exec(rowcnt, lastmonth);
	}

	private void exec(Hashtable rowcnt, String logdate) throws IOException {
		Integer	totalcnt=0,failcnt=0,okcnt=0;
			
		String filePath = hostDir+"PAYBILLLOG.TXT";
		log.info("filePath = " + filePath);
		File file;
		try {
			file = new SafeFile(filePath);
		file.setWritable(true,true);
		file.setReadable(true, true);
		file.setExecutable(true,true);
		//FileWriter fos = new FileWriter(file);
		//BufferedWriter out = new BufferedWriter(fos);
		//Path p = file.toPath();
		List<PAYBILLLOG> all = null;
		if(logdate.indexOf("%")>-1)
			all = payBillLogDao.findByMonth(logdate);
		else
			all = payBillLogDao.findByDate(logdate);
		
		BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file));
		for(PAYBILLLOG txn : all) {
			
			log.debug(ESAPIUtil.vaildLog("LOGID: " + txn.getLOGID() + ", CARDNUM: " + txn.getCARDNUM()));
			try {
				StringBuilder sb = new StringBuilder();
				//統編 10位英數字
				sb.append(StringUtils.rightPad(txn.getCUSIDN(), 10, ' '));
				
				//銷帳編號 16位數字
				sb.append(StringUtils.rightPad(txn.getCARDNUM(), 16, ' '));
				
				//轉帳來源 1位文字 (1:自行2:他行)
				sb.append(txn.getPAYSOURCE());
				
				//交易金額 9位數字 
				sb.append(StringUtils.leftPad(txn.getPAYAMT(), 9, '0'));
				
				//交易日期 8位文字 (yyyymmdd)
				sb.append(txn.getLOGDATE());
				
				// 預留15位空字元
				sb.append(StringUtils.leftPad("", 15, ' '));
				
				//行結尾符號
				sb.append("\n");
				
				//out.write(new String(StrUtils.Hex2Bin(ESAPI.encoder().encodeForHTML(StrUtils.toHex((sb.toString()).getBytes())).getBytes())));
				//Files.write(p, new String(StrUtils.Hex2Bin(ESAPI.encoder().encodeForHTML(StrUtils.toHex((sb.toString()).getBytes())).getBytes())).getBytes("BIG5"));
				
			    byte[] strToBytes = new String(StrUtils.Hex2Bin(ESAPI.encoder().encodeForHTML(StrUtils.toHex((sb.toString()).getBytes())).getBytes())).getBytes("BIG5");
			    outputStream.write(strToBytes);
				okcnt++;
			} catch (Exception e) {
				log.error("Error, write [LOGID: " + txn.getLOGID() + ", CARDNUM: " + txn.getCARDNUM() + "], " + e.getMessage());
				failcnt++;
			}
		}
		totalcnt=okcnt+failcnt;
		
		try {
			outputStream.close();
		} catch(Exception e){}

		rowcnt.put("OKCNT",okcnt);
		rowcnt.put("FAILCNT",failcnt);
		rowcnt.put("TOTALCNT",totalcnt);
		} catch (ValidationException e1) {
			log.error("NewSafeFile 錯誤.", e1);
		}
	}
	private void exec(Hashtable rowcnt, String startdate, String enddate) throws IOException {
		Integer	totalcnt=0,failcnt=0,okcnt=0;
				
		//String filePath = "/TBB/nnb/hostdata/PAYBILLLOG.TXT";
		String filePath = hostDir+"PAYBILLLOG.TXT";
		log.info("filePath = " + filePath);
		File file;
		try {
			file = new SafeFile(filePath);
		file.setWritable(true,true);
		file.setReadable(true, true);
		file.setExecutable(true,true);
		//FileWriter fos = new FileWriter(file);
		//BufferedWriter out = new BufferedWriter(fos);
		//Path p = file.toPath();
		List<PAYBILLLOG> all = payBillLogDao.findByDateRange(startdate,enddate);
				
		for(PAYBILLLOG txn : all) {
			
			log.debug(ESAPIUtil.vaildLog("LOGID: " + txn.getLOGID() + ", CARDNUM: " + txn.getCARDNUM()));
			try {
				StringBuilder sb = new StringBuilder();
				//統編 10位英數字
				sb.append(StringUtils.rightPad(txn.getCUSIDN(), 10, ' '));
				
				//銷帳編號 16位數字
				sb.append(StringUtils.rightPad(txn.getCARDNUM(), 16, ' '));
				
				//轉帳來源 1位文字 (1:自行2:他行)
				sb.append(txn.getPAYSOURCE());
				
				//交易金額 9位數字
				sb.append(StringUtils.leftPad(txn.getPAYAMT(), 9, '0'));
				
				//交易日期 8位文字 (yyyymmdd)
				sb.append(txn.getLOGDATE());
				
				// 預留5位空字元
				sb.append(StringUtils.leftPad("", 5, ' '));
				
				//行結尾符號
				sb.append("\n");
				//out.write(new String(StrUtils.Hex2Bin(ESAPI.encoder().encodeForHTML(StrUtils.toHex((sb.toString()).getBytes())).getBytes())));
				//Files.write(p, new String(StrUtils.Hex2Bin(ESAPI.encoder().encodeForHTML(StrUtils.toHex((sb.toString()).getBytes())).getBytes())).getBytes("BIG5"));
				BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file));
			    byte[] strToBytes = new String(StrUtils.Hex2Bin(ESAPI.encoder().encodeForHTML(StrUtils.toHex((sb.toString()).getBytes())).getBytes())).getBytes("BIG5");
			    outputStream.write(strToBytes);
			    outputStream.close();
				okcnt++;
			} catch (Exception e) {
				log.error("Error, write [LOGID: " + txn.getLOGID() + ", CARDNUM: " + txn.getCARDNUM() + "], " + e.getMessage());
				failcnt++;
			}
		}
		totalcnt=okcnt+failcnt;
		
//		try {
//			out.close();
//		} catch(Exception e){}
//		try {
//			fos.close();
//		} catch(Exception e){}

		rowcnt.put("OKCNT",okcnt);
		rowcnt.put("FAILCNT",failcnt);
		rowcnt.put("TOTALCNT",totalcnt);
		} catch (ValidationException e1) {
			// TODO Auto-generated catch block
			log.error("NewSafeFile 錯誤.", e1);
		}
	}	
	
}
