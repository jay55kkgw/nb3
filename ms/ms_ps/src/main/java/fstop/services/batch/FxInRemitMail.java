package fstop.services.batch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.rest.model.JSPUtils;
import com.netbank.util.ESAPIUtil;

import fstop.notification.Notification;
import fstop.notification.PushData;
import fstop.notification.TopicLabel;
import fstop.notifier.NotifyAngent;
import fstop.notifier.PushAgent;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.TxnPhoneTokenDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.TXNPHONETOKEN;
import fstop.orm.po.TXNUSER;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

//import fstop.notification.Notification;
//import fstop.notification.PushData;
//import fstop.notification.TopicLabel;
//import fstop.notification.PushAgent;

import topibs.utility.NumericUtil;

/**
 * 國外匯入匯款通知(Email & 推播)
 * 
 * @author Owner
 *
 */
@Slf4j
@RestController
@Batch(id = "batch.fxinRemitMail", name = "國外匯入匯款通知(Email & 推播)", description = "國外匯入匯款通知(Email & 推播)")
public class FxInRemitMail implements EmailUploadIf, BatchExecute {

	@Autowired
	private TxnUserDao txnUserDao;

	@Autowired
	private TxnPhoneTokenDao txnPhoneTokenDao;

	@Value("${batch.hostfile.path:}")
	private String pathname;

	@Value("${batch.otherfile.path:}")
	private String otherDir;

	@Value("${host.hostdata.path}")
	private String hhdpath;

	@Value("${host.other.path}")
	private String hopath;

	@Autowired
	private SysBatchDao sysBatchDao;

	@RequestMapping(value = "/batch/fxinRemitMail", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {

		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}

		List<String> safeArgs = ESAPIUtil.validStrList(args);
		int totalcnt = 0, failcnt = 0, okcnt = 0, flushcnt = 0;

		String FullFileName = new String();

		InputStreamReader in = null;
		BufferedReader bin = null;

		String subject = new String();
		String custid = new String();

		Date d = new Date();
		String today = DateTimeUtils.getDateShort(d);
		String time = DateTimeUtils.getTimeShort(d);
		Hashtable sendmaildata = new Hashtable();
		String oldcusidno = "FFFFFFFFFF";
		StringBuffer mailrecodbuf = new StringBuffer();
		String cusidn = "";

		Map<String, Object> data = new HashMap();
		BatchCounter bcnt = new BatchCounter();
		BatchResult batchresult = new BatchResult();
		batchresult.setBatchName(getClass().getSimpleName());

		List<PushData> listPushData = new ArrayList<PushData>();
		String token = null;
		String pushtext = "";
		PushData pushData;

		Hashtable params = new Hashtable();
		params.put("DATE", today);
		params.put("TIME", time);
		log.debug(ESAPIUtil.vaildLog("DATE = " + today));
		log.debug(ESAPIUtil.vaildLog("TIME = " + time));
		FullFileName = pathname;
		// FullFileName ="C:\\Users\\BenChien\\Desktop\\";
		FullFileName += "fxinremit.txt";
		log.debug(ESAPIUtil.vaildLog("FullFileName = " + FullFileName));

		try {
			in = new InputStreamReader(new FileInputStream(FullFileName), "MS950");
			bin = new BufferedReader(in);
			while (true) {
				subject = ESAPIUtil.validInput(bin.readLine(), "GeneralString", true);
				if (subject == null || subject.length() == 0) {
					break;
				}
				// 客戶統編
				byte[] subjectarray = subject.getBytes("MS950");

				byte[] cusidnarray = new byte[10];
				System.arraycopy(subjectarray, 0, cusidnarray, 0, cusidnarray.length);
				cusidn = new String(cusidnarray).trim();

				if (!cusidn.equals(oldcusidno) && !oldcusidno.equals("FFFFFFFFFF")) {
					int rc = 0;
					int chkmailret = 0;
					log.debug("call chkemail");
					log.debug(ESAPIUtil.vaildLog("call chkemail cusidn = " + cusidn));
					log.debug(ESAPIUtil.vaildLog("call chkemail oldcusidn = " + oldcusidno));
					chkmailret = chkemail(sendmaildata, cusidn, oldcusidno);
					if (chkmailret == 0) {
						sendmaildata.put("EMAILRECOD", mailrecodbuf.toString());
						rc = sendmail(sendmaildata);
						if (rc == 1) {
							totalcnt++;
							failcnt++;
						} else {
							totalcnt++;
							okcnt++;
						}
					}
					mailrecodbuf = new StringBuffer();
					sendmaildata = new Hashtable();
				}

				oldcusidno = cusidn;

				log.debug(ESAPIUtil.vaildLog("cusidn = " + cusidn));
				log.debug(ESAPIUtil.vaildLog("oldcusidn = " + oldcusidno));
				sendmaildata.put("CUSIDN", cusidn);
				// 通知日
				byte[] advdatearray = new byte[8];
				System.arraycopy(subjectarray, cusidnarray.length, advdatearray, 0, advdatearray.length);
				String advdate = new String(advdatearray).trim();

				d = DateTimeUtils.parse("yyyyMMdd", advdate);
				String datetime = DateTimeUtils.format("yyyy/MM/dd", d);

				log.debug(ESAPIUtil.vaildLog("datetime = " + datetime));
				sendmaildata.put("ADVDATE", datetime);
				// 匯款編號
				byte[] refnoarray = new byte[20];
				System.arraycopy(subjectarray, cusidnarray.length + advdatearray.length, refnoarray, 0,
						refnoarray.length);

				String refno = new String(refnoarray).trim();
				log.debug(ESAPIUtil.vaildLog("refno = " + refno));
				sendmaildata.put("REFNO", refno);
				// 匯款人
				byte[] ordcus1array = new byte[35];
				System.arraycopy(subjectarray, cusidnarray.length + advdatearray.length + refnoarray.length,
						ordcus1array, 0, ordcus1array.length);

				String ordcus1 = new String(ordcus1array, "MS950").trim();
				log.debug(ESAPIUtil.vaildLog("ordcus1 = " + ordcus1));
				sendmaildata.put("ORDCUS1", ordcus1);
				// 匯入幣別
				byte[] remitcyarray = new byte[3];
				System.arraycopy(subjectarray,
						cusidnarray.length + advdatearray.length + refnoarray.length + ordcus1array.length,
						remitcyarray, 0, remitcyarray.length);

				String remitcy = new String(remitcyarray, "MS950").trim();
				log.debug(ESAPIUtil.vaildLog("remitcy = " + remitcy));
				sendmaildata.put("REMITCY", remitcy);
				// 匯入金額
				byte[] remitamarray = new byte[15];
				System.arraycopy(subjectarray, cusidnarray.length + advdatearray.length + refnoarray.length
						+ ordcus1array.length + remitcyarray.length, remitamarray, 0, remitamarray.length);

				String remitam = new String(remitamarray).trim();
				log.debug(ESAPIUtil.vaildLog("remitam = " + NumericUtil.formatNumberString(remitam, 2)));
				sendmaildata.put("REMITAM", NumericUtil.formatNumberString(remitam, 2));

				mailrecodbuf.append(editmail(sendmaildata));

				// ==================================================================================
				// 發送推播通知開始
				TXNPHONETOKEN txnphonetoken = new TXNPHONETOKEN();
				txnphonetoken = txnPhoneTokenDao.getPhoneToken(cusidn);
				try {
					if (txnphonetoken != null) {
						if (txnphonetoken.getPHONETYPE().equals("I")) {
							List<Object> arrdevices = new ArrayList<Object>();
							List<Object> arruser = new ArrayList<Object>();
							arrdevices.add(txnphonetoken.getPHONETOKEN());
							arruser.add(txnphonetoken.getDPUSERID());
							pushtext = "親愛的客戶（" + JSPUtils.hideid(cusidn)
									+ "）：提醒您有一筆國外匯入匯款。本通知僅供參考，不作為交易憑證，實際入帳情形請查詢存款帳戶。";
							// Notification notification =
							// (Notification)SpringBeanFactory.getBean("notification");
							Notification notification = new Notification();
							// Notification notification = new Notification();
							log.info(ESAPIUtil.vaildLog("APNS Start(" + txnphonetoken.getPHONETOKEN() + ")"));
							// notification.notificationToiOS(arrdevices, "(帳號:" + r.getValue("ACN") + ")" +
							// memotext,
							// "5", 0, "");
							notification.notificationToiOS(arruser, arrdevices, pushtext, "5", 0, "");
							log.info(ESAPIUtil.vaildLog("APNS End(" + txnphonetoken.getPHONETOKEN() + ")"));

						} else if (txnphonetoken.getPHONETYPE().equals("A")) {
							token = txnphonetoken.getPHONETOKEN();
							if (token != null) {
								pushData = new PushData();
								pushtext = "親愛的客戶（" + JSPUtils.hideid(cusidn)
										+ "）：提醒您有一筆國外匯入匯款。本通知僅供參考，不作為交易憑證，實際入帳情形請查詢存款帳戶。";

								Map<String, String> messageMap = new HashMap<String, String>();
								messageMap.put("TYPE", "TR");
								messageMap.put("MESSAGE", pushtext);
								// pushData.setMessage("(帳號:" + r.getValue("ACN") + ")" + memotext);
								pushData.setMessage(JSONUtils.map2json(messageMap));
								pushData.setTopic(TopicLabel.TOPICLABEL_ID + token);
								pushData.setCusId(cusidn);
								pushData.setToken(token);

								listPushData.add(pushData);
								log.debug(ESAPIUtil.vaildLog("pushData.getTopic:" + TopicLabel.TOPICLABEL_ID + token));
								log.debug(ESAPIUtil.vaildLog("pushData.getCusId:" + cusidn));
							} else {
								log.debug(ESAPIUtil.vaildLog("N185 CUSIDN = " + cusidn + " PhoneToken不存在"));
							}
						}
					}
				} catch (Exception e) {
					log.error("notification area error");
				}
			}

			log.debug("listPushData.size: " + listPushData.size());
			try {
				PushAgent.sendNotice(listPushData);
			}catch(Exception e) {
				log.error("PushAgent.sendNotice Error");
			}
			// 發送推播通知結束
			// ==================================================================================

			if (!oldcusidno.equals("FFFFFFFFFF")) {
				int rc = 0;
				int chkmailret = 0;
				chkmailret = chkemail(sendmaildata, cusidn, oldcusidno);
				if (chkmailret == 0) {
					sendmaildata.put("EMAILRECOD", mailrecodbuf.toString());
					rc = sendmail(sendmaildata);
					if (rc == 1) {
						totalcnt++;
						failcnt++;
					} else {
						totalcnt++;
						okcnt++;
					}
				}
				mailrecodbuf = new StringBuffer();
				sendmaildata = new Hashtable();
			}

		} catch (FileNotFoundException e) {
			// throw new MsgTemplateNotFoundException("找不到對應的 template 檔(" + FullFileName +
			// ").", e);
			log.error("找不到 (" + FullFileName.replace("/tmp/batch/", "/TBB/nb3/") + ")檔案," + e.getMessage());
			data.put("Error", "找不到檔案(" + FullFileName.replace("/tmp/batch/", "/TBB/nb3/") + ")");
			batchresult.setData(data);
			batchresult.setSuccess(false);
			try {
				sysBatchDao.finish_FAIL2(batchName, data);
			} catch (Exception e2) {
				log.error("sysBatchDao.finish error >> {} ", e2.getMessage());
			}
			return batchresult;
		} catch (IOException e) {
			// throw new ToRuntimeException("讀取 template 檔錯誤(" + FullFileName + ").", e);
			log.error("IOException (" + FullFileName.replace("/tmp/batch/", "/TBB/nb3/") + ")," + e.getMessage());
			data.put("Error", "找不到檔案(" + FullFileName.replace("/tmp/batch/", "/TBB/nb3/") + ")");
			batchresult.setData(data);
			batchresult.setSuccess(false);

			try {
				sysBatchDao.finish_FAIL2(batchName, data);
			} catch (Exception e2) {
				log.error("sysBatchDao.finish error >> {} ", e2.getMessage());
			}

			return batchresult;
		} finally {
			try {
				in.close();
			} catch (Exception e) {
			}
			in = null;
		}

		Date nowdate = new Date();
		Calendar c1 = Calendar.getInstance();
		c1.setTime(nowdate);
		c1.add(Calendar.DATE, -1);
		String yesterday = DateTimeUtils.getDateShort(c1.getTime());

		// RuntimeExec runtimeExec=new
		// RuntimeExec("/TBB/nb3/batch","/TBB/nb3/batch/fxinremitbak.sh " + yesterday);
		// if(runtimeExec.getcommstat()!=0)
		// {
		// logger.debug("/TBB/nb3/batch/fxinremitbak.sh exec command error");
		// // mkdir /backup5y/20090901/email/
		// }

		log.debug("============ Backup Area ============");
		log.debug("DO fxinremitbak.sh");
		log.debug("--Check Dir Start--");
		String datadatedir = otherDir + "backup5y/" + yesterday;
		String datadatedirHost = hopath + "backup5y/" + yesterday;
		log.debug(ESAPIUtil.vaildLog("datadatedir >>" + datadatedirHost));
		File file = new File(datadatedir);
		file.setWritable(true, true);
		file.setReadable(true, true);
		file.setExecutable(true, true);
		if (!file.exists()) {
			log.debug(ESAPIUtil.vaildLog("mkdir >>" + datadatedirHost));
			file.mkdir();
		}
		String emailbakdir = datadatedir + "/email";
		String emailbakdirHost = datadatedirHost + "/email";
		log.debug(ESAPIUtil.vaildLog("emailbakdir >>" + emailbakdirHost));
		file = new File(emailbakdir);
		file.setWritable(true, true);
		file.setReadable(true, true);
		file.setExecutable(true, true);
		if (!file.exists()) {
			log.debug(ESAPIUtil.vaildLog("mkdir >>" + emailbakdirHost));
			file.mkdir();
		}
		log.debug("-- Check Dir End --");
		log.debug("-- Move file Start --");

		String fkaprePath = pathname + "fxinremit.txt";

		String backDirShow = emailbakdirHost + "/";

		File file1 = new File(fkaprePath);
		file1.setWritable(true, true);
		file1.setReadable(true, true);
		file1.setExecutable(true, true);
		File file2 = new File(emailbakdir + "/fxinremit.txt");
		file2.setWritable(true, true);
		file2.setReadable(true, true);
		file2.setExecutable(true, true);
		// 檔案若存在則移除
		if (file1.exists()) {
			file2.delete();
			try {
				FileUtils.copyFile(file1, file2);
				log.debug(ESAPIUtil.vaildLog("move fxinremit.txt to " + backDirShow + " OK"));
				data.put("BackupStatus", "Success");
				file1.delete();
			} catch (IOException e) {
				log.error("move file error");
				data.put("BackupStatus", "Fail");
			}
		}

		log.debug("-- Move file End --");
		log.debug("============ Backup Area End ============");

		bcnt.setFailCount(failcnt);
		bcnt.setSuccessCount(okcnt);
		bcnt.setTotalCount(totalcnt);
		data.put("COUNTER", bcnt);

		batchresult.setSuccess(true);
		batchresult.setData(data);
		
		try {
			// 新增SYSBATCH紀錄
			sysBatchDao.finish_OK(batchName);
			
		}catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}
		return batchresult;

	}

	public int sendmail(Hashtable maildata) {
		boolean isSendSuccess = NotifyAngent.sendNotice("FXINREMITNOTIFY", maildata, (String) maildata.get("MAILADDR"));
		if (!isSendSuccess) {
			log.debug(ESAPIUtil.vaildLog("發送 錯誤Email 失敗.(" + maildata.get("MAILADDR") + ")"));
			// batchresult.getFlatValues().put("__MAILERR", "發送 Email 失敗.(" + email1 + ")");
			Map<String, Object> data = new HashMap();
			data.put("COUNTER", new BatchCounter());

			BatchResult batchresult = new BatchResult();
			batchresult.setSuccess(false);
			batchresult.setBatchName(getClass().getSimpleName());
			batchresult.setData(data);

			return (1);
		}
		log.debug(ESAPIUtil.vaildLog("發送 Email 成功.(" + maildata.get("MAILADDR") + ")"));

		return (0);
	}

	public String editmail(Hashtable maildata) {
		StringBuffer editmail = new StringBuffer();

		editmail.append("<TR>\r\n");
		editmail.append("<TD width=\"100\">\r\n");
		editmail.append("<DIV align=center><FONT SIZE=-1>").append(maildata.get("ADVDATE"))
				.append("</FONT></DIV></TD>\r\n");
		editmail.append("<TD width=\"150\">\r\n");
		editmail.append("<DIV align=center><FONT SIZE=-1>").append(maildata.get("REFNO"))
				.append("</FONT></DIV></TD>\r\n");
		editmail.append("<TD width=\"100\">\r\n");
		editmail.append("<DIV align=center><FONT SIZE=-1>").append(maildata.get("ORDCUS1"))
				.append("</FONT></DIV></TD>\r\n");
		editmail.append("<TD width=\"150\">\r\n");
		editmail.append("<DIV align=center><FONT SIZE=-1>").append(maildata.get("REMITCY")).append("&nbsp;&nbsp;");
		editmail.append(maildata.get("REMITAM")).append("</FONT></DIV></TD>\r\n");
		editmail.append("</TR>\r\n");

		return (editmail.toString());
	}

	public int chkemail(Hashtable email, String cusidn, String oldcusidno) {
		String mailaddr = null;
		TXNUSER user = null;
		List<TXNUSER> listtxnUser;
		try {
			log.debug(ESAPIUtil.vaildLog("where OLDCUSIDN = " + oldcusidno));
			listtxnUser = txnUserDao.findByUserId(oldcusidno);
			user = listtxnUser.get(0);
			mailaddr = user.getDPMYEMAIL();
		} catch (Exception e) {
			log.error("無法取得 USER ID (" + oldcusidno + ")," + e.getMessage());
			return (1);
		}

		String dpnotify = StrUtils.trim(user.getDPNOTIFY());
		Set<String> dpnotifySet = StrUtils.splitToSet("\\s*,\\s*", dpnotify);

		boolean isUserSetTxnNotify = dpnotifySet.contains("12");
		if (isUserSetTxnNotify == false) {
			log.error("The notify is't exist.(USER ID : " + oldcusidno + ") ");
			return (1);
		}

		if (mailaddr.length() == 0 || mailaddr == null) {
			log.error("The mail is't exist.(USER ID : " + oldcusidno + ") ");
			return (1);
		}

		log.debug(ESAPIUtil.vaildLog("mailaddr = " + mailaddr));
		email.put("MAILADDR", mailaddr);
		return (0);

	}
}
