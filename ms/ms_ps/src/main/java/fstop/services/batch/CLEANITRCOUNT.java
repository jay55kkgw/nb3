package fstop.services.batch;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import fstop.orm.dao.ItrCountDao;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
/**
 * 
 * 
 * @author Owner
 * 
 */
@Slf4j
@RestController
@Batch(id="batch.cleanitrcount", name = "", description = "")
public class CLEANITRCOUNT  extends DispatchBatchExecute  implements BatchExecute {

	private DecimalFormat fmt = new DecimalFormat("#0");
	
	
	@Autowired
	private ItrCountDao itrCountDao;
	
	@Value("${batch.hostfile.path:}")
	private String hostDir;
	
	public CLEANITRCOUNT(){}
	
	/**
	 * args[0] method name
	 */
	@RequestMapping(value = "/batch/cleanitrcount", method = {RequestMethod.POST},
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {
		String FullFileName=new String();
		Date d = new Date();
		String today = DateTimeUtils.getDateShort(d);
		String time = DateTimeUtils.getTimeShort(d);
		String TmpCount;
		String WBuffer=new String();
		
		Hashtable params = new Hashtable();
//		params.put("DATE", today);
//		params.put("TIME", time);
		log.debug("DATE = " + today);
		log.debug("TIME = " + time);

		FullFileName=hostDir;
		FullFileName+="RateSum.txt";
		log.debug("FullFileName = " + FullFileName);
		TmpCount="000000000000" + itrCountDao.getCountSum();
		log.debug("TmpCount = " + TmpCount);
		WBuffer=today.substring(0, 8) + time + StrUtils.right(TmpCount, 12);
		log.debug("WBuffer = " + WBuffer);
		
		File tmp = new File(FullFileName);
		tmp.setWritable(true,true);
		tmp.setReadable(true, true);
		tmp.setExecutable(true,true);
		
		try 
		{
			OutputStream  fos = new FileOutputStream(tmp);
			BufferedWriter	out = new BufferedWriter(new OutputStreamWriter(fos, "BIG5"));
//			pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File((String)setting.get("filepath"),(String)setting.get("filename")), true) , "US-ASCII")));
			log.debug("WBuffer = " + WBuffer);
			out.write(WBuffer);
			out.flush();
			try {
				out.close();
				fos.close();
			}catch (Exception e) {
				log.error("out or fos close error" , e.getMessage());
			}
		}
		catch(Exception e) {
			
			Map<String, Object> data = new HashMap();

			data.put("TOPMSG", "寫入檔失敗");

			BatchResult batchresult = new BatchResult();
			batchresult.setSuccess(false);
			batchresult.setBatchName(getClass().getSimpleName());
			batchresult.setData(data);

			return batchresult;

		}
		
		itrCountDao.deleteAll();
		

		Map<String, Object> data = new HashMap();

		data.put("COUNTER", "1");
		data.put("TOPMSG", "寫入檔成功");

		BatchResult batchresult = new BatchResult();
		batchresult.setSuccess(true);
		batchresult.setBatchName(getClass().getSimpleName());
		batchresult.setData(data);

		return batchresult;

	}
}
