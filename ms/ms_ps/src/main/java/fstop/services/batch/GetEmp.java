package fstop.services.batch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

//import org.jdom.Document;
//import org.jdom.Element;
//import org.jdom.output.Format;
//import org.jdom.output.XMLOutputter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.ESAPIUtil;

import fstop.exception.MsgTemplateNotFoundException;
import fstop.exception.ToRuntimeException;
import fstop.orm.dao.AdmEmpInfoDao;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.po.ADMEMPINFO;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 更新行員基本資料
 * 
 * @author Owner
 * 
 */
@Slf4j
@RestController
@Batch(id = "batch.getEmp", name = "更新行員基本資料", description = "更新行員基本資料")
public class GetEmp extends DispatchBatchExecute implements BatchExecute {
	// private Logger logger = Logger.getLogger("fstop_txntw");

	@Autowired
	private AdmEmpInfoDao admEmpInfoDao;

	@Value("${batch.hostfile.path:}")
	private String pathName;

	@Autowired
	private SysBatchDao sysBatchDao;

	public GetEmp() {
	}

	/**
	 * args[0] method name
	 */
	@RequestMapping(value = "/batch/getEmp", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {
		List<String> safeArgs = ESAPIUtil.validStrList(args);
		log.info("INTO GetEmp BATCH ... ");
		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}

		BatchResult batchresult = new BatchResult();
		batchresult.setBatchName(getClass().getSimpleName());
		Map<String, Object> resultData = new HashMap();
		batchresult.setSuccess(true);

		ADMEMPINFO t = new ADMEMPINFO();
		String FullFileName = new String();
		InputStreamReader in = null;
		BufferedReader bin = null;
		InputStreamReader in1 = null;
		BufferedReader bin1 = null;
		Date d = new Date();
		String today = DateTimeUtils.getDateShort(d);
		String time = DateTimeUtils.getTimeShort(d);
		String Str_Ldate = "";

		Hashtable params = new Hashtable();
		// params.put("DATE", today);
		// params.put("TIME", time);
		log.info("DATE = {} ", today);
		log.info("TIME = {} ", time);
		FullFileName = pathName;
		FullFileName += "FWKEMP.txt";
		// FullFileName=args.get(0);
		// FullFileName+=args.get(1);
		log.debug("FullFileName = {} ", FullFileName);
		try {
			
			File file= new File(FullFileName);
			//Incorrect Permission Assignment For Critical Resources
			file.setWritable(true,true);
			file.setReadable(true, true);
			file.setExecutable(true,true);
			long timestamp = file.lastModified();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String fmtDate = sdf.format(timestamp);
			log.info("File's LastModify fmt >> {}",fmtDate);
			
			if(!today.equals(fmtDate)) {
				resultData.put("MSG", "FWKEMP.txt MODIFY TIME NOT TODAY , SKIP EXECUTE !");
				batchresult.setSuccess(true);
				batchresult.setBatchName(this.getClass().getSimpleName());
				batchresult.setData(resultData);
				try {
					// 新增SYSBATCH紀錄
					sysBatchDao.finish_OK(batchName);
				}catch (Exception e2) {
					log.error("sysBatchDao.finish error >> {} ", e2.getMessage());
				}
				return batchresult;
			}
			
			in = new InputStreamReader(new FileInputStream(FullFileName), "BIG5");
			bin = new BufferedReader(in);
			int iAdd = 0;
			String data;
			String Str_data = "";

			if (!bin.ready()) {
				in.close();
				bin.close();
				throw new Exception();
			}
			admEmpInfoDao.deleteAll();
			do {
				data = ESAPIUtil.validInput(bin.readLine(), "GeneralString", true);
				if (data == null || data.length() == 0 || data.length() < 6) {
					break;
				}
				iAdd++;
			} while (true);

			String[][] result = new String[iAdd][5];
			in1 = new InputStreamReader(new FileInputStream(FullFileName), "BIG5");
			bin1 = new BufferedReader(in1);
			int record = 0;
			String Str_chk;
			String mail_addr;
			do {
				data = ESAPIUtil.validInput(bin1.readLine(), "GeneralString", true);
				if (data == null || data.length() == 0 || data.length() < 6) {
					break;
				}

				// 判斷是否有資料重複
				// System.out.println("START : data = " + data + " Str_data = " + Str_data);
				if (data.equals(Str_data)) {
					log.debug(ESAPIUtil.vaildLog("DOUBLE : data >>" + data + "Str_data >>" + Str_data));
					continue;
				}

				result[record][0] = data.substring(3, 9);
				result[record][1] = data.substring(9, 19);

				log.trace("empno = {} ", result[record][0]);
				log.trace("CUSID = {} ", result[record][1]);

				Str_chk = data.substring(27, 28);

				if (data.indexOf('*') > 0) { // 有* 增加離職日期
					// Str_Ldate =
					// String.valueOf(Integer.parseInt(data.substring(27,30))+1911)+data.substring(30,34);
					// Str_chk = True 沒有位移一位
					if (Chk_num(Str_chk)) {
						Str_Ldate = data.substring(27, 34);
					} else {
						Str_Ldate = data.substring(28, 35);
					}
				} else {
					Str_Ldate = "";
				}

				log.trace(ESAPIUtil.vaildLog(
						"Str_chk = " + Str_chk + "  Chk_num = " + Chk_num(Str_chk) + "  Str_Ldate = " + Str_Ldate));

				result[record][2] = Str_Ldate;

				// 增加部門代號
				result[record][3] = data.substring(0, 3);
				log.trace("edpno = " + result[record][3]);

				// 增加email
				mail_addr = data.substring(35).trim();
				if (mail_addr.startsWith("*")) {
					mail_addr = mail_addr.substring(1);
				}
				result[record][4] = mail_addr;

				/*
				 * System.out.println("empno = " + result[record][0]);
				 * System.out.println("mail = " + ""); System.out.println("CUSID = " +
				 * result[record][1]); System.out.println("Str_Ldate = " + Str_Ldate);
				 * System.out.println("result[record][2] = " + result[record][2]);
				 * 
				 * System.out.println("YEAR = " + data.substring(27,30));
				 * System.out.println("DATE = " + data.substring(30,34));
				 * System.out.println("YEAR2 = " +
				 * String.valueOf(Integer.parseInt(data.substring(27,30))+1911)+data.substring(
				 * 30,34));
				 */

				record++;

				// 判斷是否有資料重複
				Str_data = data;
			} while (true);

			for (int i = 0; i < iAdd; i++) {
				try {
					t.setEMPNO(result[i][0]);
					t.setEMPMAIL(result[i][4]);
					t.setCUSID(result[i][1]);
					t.setLDATE(result[i][2]);
					t.setDEPNO(result[i][3]);
					admEmpInfoDao.insert(t);
				} catch (Exception e) {
					log.debug("error >> EMPNO {}", result[i][0]);
				}
			}

		} catch (FileNotFoundException e) {
			resultData.put("Error", "找不到檔案(" + FullFileName + ")");
			batchresult.setData(resultData);
			batchresult.setSuccess(false);
			// throw new MsgTemplateNotFoundException("找不到對應的 template 檔(" + FullFileName +
			// ").", e);
		} catch (IOException e) {
			resultData.put("Error", "讀取檔案錯誤(" + FullFileName + ")");
			batchresult.setData(resultData);
			batchresult.setSuccess(false);
			// throw new ToRuntimeException("讀取 template 檔錯誤(" + FullFileName + ").", e);
		} catch (Exception e) {
			resultData.put("Error", "檔案為空(" + FullFileName + ")");
			batchresult.setData(resultData);
			batchresult.setSuccess(false);
		} finally {
			try {
				in.close();
				in1.close();
				bin.close();
				bin1.close();
			} catch (Exception e) {
			}
			in = null;
			in1 = null;
		}

		batchresult.setData(resultData);

		try {
			if (batchresult.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName, batchresult.getData());
			}
		} catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}

		return batchresult;

	}

	// 確認是否為數字
	public boolean Chk_num(String Str_c) {
		String CHK_flag = "NO";
		for (int i = 0; i < 10; i++) {
			if (Str_c.equals(Integer.toString(i))) {
				CHK_flag = "YES";
				break;
			}
		}
		if (CHK_flag.equals("YES")) {
			return true;
		} else {
			return false;
		}

	}

}
