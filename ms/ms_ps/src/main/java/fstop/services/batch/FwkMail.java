package fstop.services.batch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.ESAPIUtil;

import fstop.exception.MsgTemplateNotFoundException;
import fstop.exception.ToRuntimeException;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.TXNUSER;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * EMAIL整併作業：讀取/TBB/nnb/hostdata/FWKMAIL.txt
 *              同步txnuser資料表格dpmyemail欄位
 * 
 * @author Owner
 * 
 */
@Slf4j
@RestController
@Batch(id="batch.FwkMail", name = "EMAIL整併作業", description = "讀取/TBB/nnb/hostdata/FWKMAIL.txt,同步txnuser資料表格dpmyemail欄位")
public class FwkMail  implements BatchExecute {
//	private log log = log.getlog(getClass());
	
	
	@Autowired
	private TxnUserDao txnUserDao;
	
	@Value("${batch.hostfile.path:}")
	private String pathname;
	
	@Value("${host.hostdata.path}")
	private String hostpath;
	
	@Autowired
	private SysBatchDao sysBatchDao;
	
	/**
	 * args[0] method name
	 */
	@RequestMapping(value = "/batch/fwkMail", method = {RequestMethod.POST},
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {
		
		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}
		
		BatchResult batchresult = new BatchResult();
		Map<String, Object> data = new HashMap();
		batchresult.setSuccess(true);
		batchresult.setBatchName(getClass().getSimpleName());
		BatchCounter bc = new BatchCounter();

		
		List<String> safeArgs = ESAPIUtil.validStrList(args);
		TXNUSER user = null;
		String FullFileName=new String();
		InputStreamReader in = null;
		BufferedReader bin = null;
		String subject=new String();
		String mail=new String();
		String id=new String();
		Date d = new Date();
		String today = DateTimeUtils.getDateShort(d);
		String time = DateTimeUtils.getTimeShort(d);
		
		Hashtable params = new Hashtable();
//		params.put("DATE", today);
//		params.put("TIME", time);
		log.info("DATE >> {} " , today);
		log.info("TIME >> {}  " , time);
		FullFileName=pathname;
//		FullFileName="C:\\Users\\BenChien\\Desktop\\";
		FullFileName+="FWKMAIL.txt";
		//for log 
		String FullHostFileName = hostpath + "FWKMAIL.txt";
		
		log.debug("FullFileName = " + FullFileName);
		try {
			
			File file= new File(FullFileName);
			//Incorrect Permission Assignment For Critical Resources
			file.setWritable(true,true);
			file.setReadable(true, true);
			file.setExecutable(true,true);
			long timestamp = file.lastModified();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String fmtDate = sdf.format(timestamp);
			log.info("File's LastModify fmt >> {}",fmtDate);
			
			if(!today.equals(fmtDate)) {
				data.put("MSG", "FWKMAIL.txt MODIFY TIME NOT TODAY , SKIP EXECUTE !");
				batchresult.setSuccess(true);
				batchresult.setBatchName(this.getClass().getSimpleName());
				batchresult.setData(data);
				try {
					// 新增SYSBATCH紀錄
					sysBatchDao.finish_OK(batchName);
				}catch (Exception e2) {
					log.error("sysBatchDao.finish error >> {} ", e2.getMessage());
				}
				return batchresult;
			}
			
			
			
			in = new InputStreamReader(new FileInputStream(FullFileName), "BIG5");
			bin = new BufferedReader(in);
			while (true) {
				subject = ESAPIUtil.validInput(bin.readLine(), "GeneralString", true);
				if (subject == null || subject.length() == 0) {
					break;
				}
				try {
						id=subject.substring(0,10).trim();
						mail=subject.substring(10,70).trim();
						List<TXNUSER> txnuser = new ArrayList<TXNUSER>();
	        			txnuser = txnUserDao.findByUserId(id);   
  						if(txnuser.size()>0) {
  							log.debug(ESAPIUtil.vaildLog("batch_fwkmail_find user："+id+" MAIL-->"+mail));
  							TXNUSER u = txnuser.get(0);
  							String emaildate="";
  							if(u.getEMAILDATE().trim().length()>=7)
  								emaildate=u.getEMAILDATE().trim().substring(0,7);
  							else
  								emaildate="0";
                            log.debug(ESAPIUtil.vaildLog("fwkmail date:"+subject.substring(70,77)+" txnuser emaildate:"+emaildate));
                            if(u.getEMAILDATE().trim()==null || u.getEMAILDATE().trim().length()==0 || u.getDPMYEMAIL().length()==0 || u.getDPMYEMAIL()==null)
                            {
  					 			txnUserDao.updateDPMYEMAIL(id, mail); 
  					 			log.debug(ESAPIUtil.vaildLog("batch_fwkmail_update email:"+id+"-->"+mail));                            	
  					 			bc.incSuccess();
  					 			bc.incTotalCount();
                            }
                            else if(Integer.parseInt(subject.substring(70,77))>Integer.parseInt(emaildate))
  			    			{
  					 			txnUserDao.updateDPMYEMAIL(id, mail); 
  					 			log.debug(ESAPIUtil.vaildLog("batch_fwkmail_update email:"+id+"-->"+mail));
  					 			bc.incSuccess();
  					 			bc.incTotalCount();
  			    			}
  						}
  						else 
  						{
  							log.debug(ESAPIUtil.vaildLog("batch_fwkmail_user not found："+id));
  							bc.incFail();
					 		bc.incTotalCount();
  						}
				}
				catch(Exception e){
					log.error(ESAPIUtil.vaildLog("batch_fwkmail_user:"+id +" exception:"+e.getMessage()));
//					throw new ToRuntimeException("找不到對應的使用者.");
				}
				//log.debug("FwkMail.java：UID = " + id);
			}

		}
		catch(FileNotFoundException e) {
			data.put("Error", "找不到檔案(" + FullHostFileName + ")");
			batchresult.setData(data);
			batchresult.setSuccess(false);
		}
		catch(IOException e) {
			data.put("Error", "讀取檔案錯誤(" + FullHostFileName + ")");
			batchresult.setData(data);
			batchresult.setSuccess(false);
		}
		finally {
			try {
				in.close();
				bin.close();
			} catch(Exception e){}
			in = null;
			bin = null; 
		}
		
		data.put("COUNTER", bc);
		batchresult.setData(data);

		try {
			if (batchresult.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName,batchresult.getData());
			}
		}catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}
		
		return batchresult;

	}
}
