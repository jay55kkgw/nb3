package fstop.services.batch;

import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.ESAPIUtil;

import fstop.orm.dao.AdmLogInOutDao;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@Batch(id="batch.ExpLogInOut", name = "", description = "")
public class ExpLogInOut implements BatchExecute {
	
	@Autowired
	private AdmLogInOutDao admLogInOutDao;
	
	@Value("${batch.backup5yPath:/backup5y/}")
	private String  backup5yPath;
	
	
	/**
	 * args[0] method name
	 */
	@RequestMapping(value = "/batch/loginout", method = {RequestMethod.POST},
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {
		List<String> safeArgs = ESAPIUtil.validStrList(args);
		
		Map<String, Object> data = new HashMap();

		BatchResult batchresult = new BatchResult();
		
		String outputD = "";
		
		int expResult = 1;
		int delResult = 1;
		
		Date d = new Date();
		String today = DateTimeUtils.getDateShort(d);
		String time = DateTimeUtils.getTimeShort(d);
		
		Hashtable params = new Hashtable();
//		params.put("DATE", today);
//		params.put("TIME", time);
		log.info("EXEC DATE = {}" , today);
		log.info("EXEC TIME = {}" , time);
		
		if(safeArgs != null && safeArgs.size() > 0) {
			outputD = safeArgs.get(0);
		}else {
			//199test
			outputD =backup5yPath;
			
		}
		
		log.debug(" outputPath >> {}" , backup5yPath);
		
		expResult = admLogInOutDao.sp_cal_expLoginout(outputD);
		
		if(0==expResult) {
			data.put("EXP Status", "OK");
			delResult=admLogInOutDao.sp_cal_delLoginout();
			if(0==delResult) {
				data.put("DEL Status", "OK");
				batchresult.setSuccess(true);
				batchresult.setBatchName(getClass().getSimpleName());
				batchresult.setData(data);
			}else {
				data.put("DEL Status", "Failed");
				batchresult.setSuccess(false);
				batchresult.setBatchName(getClass().getSimpleName());
				batchresult.setData(data);
			}
			
		}else {
			data.put("EXP Status", "failed");
			batchresult.setSuccess(false);
			batchresult.setBatchName(getClass().getSimpleName());
			batchresult.setData(data);
		}
		
		return batchresult;

	}
}
