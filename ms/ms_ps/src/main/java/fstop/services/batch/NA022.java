package fstop.services.batch;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.TxnCardApplyDao;
import fstop.orm.oao.Old_TxnCardApplyDao;
import fstop.orm.old.OLD_TXNCARDAPPLY;
import fstop.orm.po.TXNCARDAPPLY;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import fstop.util.FtpBase64;
import fstop.util.FtpPathProperties;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 每日產生昨日產生檔案清單列表 - 線上申請信用卡
 * 
 * @author Owner
 * 
 */
@Slf4j
@RestController
@Batch(id = "batch.na022", name = "線上申請信用卡清單列表", description = "每日產生昨日產生檔案清單列表 - 線上申請信用卡")
public class NA022 implements BatchExecute {
	// private log log = log.getlog(getClass());

	@Autowired
	private TxnCardApplyDao txnCardApplyDao;

	@Autowired
	private SysBatchDao sysBatchDao;
	
	@Value("${batch.otherfile.path}")
	private String otherpath;
	
	@Value("${eloan.ftp.ip}")
	private String eloan_ftp_ip;
	
	@Value("${isParallelCheck}")
	private String isParallelCheck;
	
	@Value("${doFTP}")
	private String doFTP;
	
	@Autowired
	CheckServerHealth checkserverhealth;
	
	@Autowired
	private Old_TxnCardApplyDao old_TxnCardApplyDao;

	public NA022() {
	}

	/**
	 * args[0] method name
	 */
	@RequestMapping(value = "/batch/na022", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> _params) {
		int failcnt = 0, okcnt = 0, totalcnt = 0;
		Hashtable rowcnt = new Hashtable();

		log.debug("INTO ELOAN UPLOAD ... ");

		BatchResult batchresult = new BatchResult();
		Map<String, Object> data = new LinkedHashMap();
		batchresult.setBatchName(getClass().getSimpleName());
		batchresult.setSuccess(true);

		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}
		
		
		Hashtable<String, String> params = new Hashtable();
		params.put("TXNIDT", DateTimeUtils.format("yyMMddHHmmss", new Date()));

		try {
			exec(rowcnt);
			failcnt = ((Integer) rowcnt.get("FAILCNT")).intValue();
			okcnt = ((Integer) rowcnt.get("OKCNT")).intValue();
			totalcnt = ((Integer) rowcnt.get("TOTALCNT")).intValue();
			log.debug("failcnt = " + failcnt);
			log.debug("okcnt = " + okcnt);
			log.debug("totalcnt = " + totalcnt);
		} catch (Exception e) {
			log.error("執行 Batch 有誤 !", e);
			batchresult.setSuccess(false);
			data.put("ERROR", e.getMessage());

		}

		BatchCounter bcnt = new BatchCounter();
		bcnt.setFailCount(failcnt);
		bcnt.setSuccessCount(okcnt);
		bcnt.setTotalCount(totalcnt);

		data.put("COUNTER", bcnt);
		batchresult.setData(data);
		
		try {
			if (batchresult.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName, batchresult.getData());
			}
		} catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}
		
		return batchresult;
	}

	private void exec(Map rowcnt) throws IOException {
		Integer totalcnt = 0, failcnt = 0, okcnt = 0;

		Date d = new Date();
		String today = DateTimeUtils.getCDateShort(d);
		today = today.substring(0, 7);
		// String time = DateTimeUtils.getTimeShort(d);

		Date d2 = new Date();
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		d2 = cal.getTime();
		String yesterday = DateTimeUtils.getCDateShort(d2);
		yesterday = yesterday.substring(0, 7);
		String lastdate = DateTimeUtils.format("yyyyMMdd", d2);

		log.debug("today = " + DateTimeUtils.getDateShort(d));
		log.debug("yesterday = " + DateTimeUtils.getDateShort(d2));
		// tmp/batch/other  /TBB/nb3/other
//		String srcpath = otherpath;
		// tmp/batch/data   /TBB/nb3/data
		FtpPathProperties nrcProperties = new FtpPathProperties();
		String srcpath = nrcProperties.getSrcPath("na022");
		String despath = nrcProperties.getDesPath("na022");
		
		log.debug("srcpath = " + srcpath);
		String FILENAME = "CREDITFILE" + today + ".txt";
		File tmpFile = new File(srcpath + FILENAME);
		//Incorrect Permission Assignment For Critical Resources
		tmpFile.setWritable(true,true);
		tmpFile.setReadable(true, true);
		tmpFile.setExecutable(true,true);
//		FileWriter fos = new FileWriter(tmpFile);
//		BufferedWriter out = new BufferedWriter(fos);
		BufferedWriter out = new BufferedWriter(new FileWriter(tmpFile));

		List<TXNCARDAPPLY> all = txnCardApplyDao.findByLastDate(lastdate);
		log.trace("NB3 data size >> {} ", all.size());
		if ("Y".equals(isParallelCheck)) {
			List<OLD_TXNCARDAPPLY> allold = old_TxnCardApplyDao.findByLastDate(lastdate);
			log.trace("NB2.5 data size >> {} ", allold.size());
			if(null!=allold) {
				for (OLD_TXNCARDAPPLY old : allold) {
	        		TXNCARDAPPLY nnbConvert2nb3 = CodeUtil.objectCovert(TXNCARDAPPLY.class, old);
	        		all.add(nnbConvert2nb3);
				}
			}
		}
		
		for (TXNCARDAPPLY card : all) {

			log.debug(ESAPIUtil.vaildLog("RCVNO: " + card.getRCVNO() + ", FILE1: " + card.getFILE1() + ", FILE2: " + card.getFILE2()
			+ ", FILE3: " + card.getFILE3() + ", FILE4: " + card.getFILE4() + ", FILE5: " + card.getFILE5()));

			String RCVNO = "<RCVNO>" + card.getRCVNO(); // 案件編號
			TXNCARDAPPLY user = txnCardApplyDao.setStatus(card.getRCVNO() , "1");// 狀態改為1:審核中
			if ("Y".equals(isParallelCheck)) {
				if(null==user) {
					OLD_TXNCARDAPPLY userold = old_TxnCardApplyDao.setStatus(card.getRCVNO(), "1");
				}
			}
			String RCVNOFILE = "<00>" + card.getRCVNO() + ".TXT"; // 案件檔案名稱
			String FILE1 = "<01>" + card.getFILE1(); // 圖檔1
			String FILE2 = "<02>" + card.getFILE2(); // 圖檔2
			String FILE3 = "<03>" + card.getFILE3(); // 圖檔3
			String FILE4 = "<04>" + card.getFILE4(); // 圖檔4
			String FILE5 = "<05>" + card.getFILE5(); // 圖檔5
			String FILE6 = "<06>" + card.getRCVNO() + ".pdf"; // 圖檔6
			String STATUS = "1"; // 異動狀態
			out.write(RCVNO);
			out.write("\n");
			out.write(RCVNOFILE);
			out.write("\n");
			out.write(FILE1);
			out.write("\n");
			out.write(FILE2);
			out.write("\n");
			out.write(FILE3);
			out.write("\n");
			out.write(FILE4);
			out.write("\n");
			out.write(FILE5);
			out.write("\n");
			out.write(FILE6);
			out.write("\n");
			okcnt++;
		}

		totalcnt = okcnt;

		try {
			out.close();
		} catch (Exception e) {
		}
//		try {
//			fos.close();
//		} catch (Exception e) {
//		}

		if (okcnt == 0) {
			rowcnt.put("OKCNT", okcnt);
			rowcnt.put("FAILCNT", failcnt);
			rowcnt.put("TOTALCNT", totalcnt);
			return;
		}
		String ip = eloan_ftp_ip;

		FtpBase64 ftpBase64 = new FtpBase64(ip);
		log.debug("tmpFile = " + tmpFile.getPath());
		int rc = ftpBase64.upload(despath + FILENAME, new FileInputStream(tmpFile));
		// 目地 來源
		if (rc != 0) {
			log.debug(FILENAME + " FTP 失敗(ELOAN 主機一次)");
//			CheckServerHealth checkserverhealth = (CheckServerHealth) SpringBeanFactory.getBean("checkserverhealth");
			checkserverhealth.sendUnknowNotice(FILENAME + " FTP 失敗(ELOAN 主機一次)");
			rc = ftpBase64.upload(despath + FILENAME, new FileInputStream(tmpFile));
			if (rc != 0) {
				log.debug(FILENAME + " FTP 失敗(ELOAN 主機二次)");
//				checkserverhealth = (CheckServerHealth) SpringBeanFactory.getBean("checkserverhealth");
				checkserverhealth.sendUnknowNotice(FILENAME + " FTP 失敗(ELOAN 主機二次)");
			}
		}

		rowcnt.put("OKCNT", okcnt);
		rowcnt.put("FAILCNT", failcnt);
		rowcnt.put("TOTALCNT", totalcnt);
	}
}
