package fstop.services.batch;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.exception.UncheckedException;
import fstop.model.DBResult;
import fstop.model.EachRowCallback;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.orm.dao.ItrCountDao;
import fstop.orm.dao.ItrIntervalDao;
import fstop.orm.dao.ItrN026Dao;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.po.ITRCOUNT;
import fstop.orm.po.ITRN026;
import fstop.services.AfterSuccessQuery;
import fstop.services.batch.annotation.Batch;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 債券及票券參考利率更新
 * 
 * @author Owner
 * 
 */
@Slf4j
@RestController
@Batch(id="batch.n026", name = "債券及票券參考利率更新", description = "債券及票券參考利率更新")
public class N026  extends DispatchBatchExecute implements BatchExecute {
//	private Logger logger = Logger.getLogger("fstop_txntw");

	private DecimalFormat fmt = new DecimalFormat("#0");
	@Autowired
	@Qualifier("n026Telcomm")
	private TelCommExec n026Telcomm;
	@Autowired
	private ItrN026Dao itrN026Dao;
	@Autowired
	private ItrCountDao itrCountDao;
	
//	private ItrCount itrcount;
	@Autowired
	private ItrIntervalDao itrIntervalDao;
	
	@Autowired
	private SysBatchDao sysBatchDao;

	
//	private Map setting;
//
//	@Required
//	public void setSetting(Map setting) {
//		this.setting = setting;
//	}
//
//	@Required
//	public void setItrIntervalDao(ItrIntervalDao itrIntervalDao) {
//		this.itrIntervalDao = itrIntervalDao;
//	}
//
//	@Required
//	public void setItrN026Dao(ItrN026Dao itrN026Dao) {
//		this.itrN026Dao = itrN026Dao;
//	}
//
//	@Required
//	public void setN026Telcomm(TelCommExec telCommExec) {
//		this.n026Telcomm = telCommExec;
//	}
//	
//	@Required
//	public void setItrCountDao(ItrCountDao itrCountDao) {
//		this.itrCountDao = itrCountDao;
//	}

	public N026(){}
	
	/**
	 * args[0] method name
	 */
	@RequestMapping(value = "/batch/n026", method = {RequestMethod.POST},
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {
		List<String> safeArgs = ESAPIUtil.validStrList(args);
		log.info("batch N026...");
		log.info(ESAPIUtil.vaildLog("safeArgs: {}"+ JSONUtils.toJson(safeArgs)));
		Date d = new Date();
		String today = DateTimeUtils.getDateShort(d);
		String time = DateTimeUtils.getTimeShort(d);
		
		BatchResult batchresult = new BatchResult();
		Map<String, Object> data = new HashMap();
		batchresult.setBatchName(getClass().getSimpleName());
		batchresult.setSuccess(true);

		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}
		
		Hashtable params = new Hashtable();
//		params.put("DATE", today);
//		params.put("TIME", time);
		log.info("DATE = " + today);
		log.info("TIME = " + time);
		
		final StringBuffer recno_idBuff = new StringBuffer();
		recno_idBuff.append("0");
		try {
		final ITRN026 t = new ITRN026();
		
		SimpleTemplate simpleTemplate = new SimpleTemplate(n026Telcomm);
		log.info("setAfterSuccessQuery..");
		simpleTemplate.setAfterSuccessQuery(new AfterSuccessQuery() {

			public void execute(MVHImpl result) {
				itrN026Dao.deleteAll();

				t.setHEADER(result.getValueByFieldName("HEADER"));
				t.setSEQ(result.getValueByFieldName("SEQ"));
				t.setDATE(result.getValueByFieldName("DATE"));
				t.setTIME(result.getValueByFieldName("TIME"));
				t.setCOUNT(result.getValueByFieldName("COUNT"));
			}
			
		});
		
		log.info("setEachRowCallback..");
		simpleTemplate.setEachRowCallback(new EachRowCallback() {

			public void current(Row row) {
//				ITRN021 t = new ITRN021();
				
//---------------TODO table的自動編號還沒改好,改好的話這區塊刪掉------------------------------
				Integer recno_id = new Integer(recno_idBuff.toString()) + 1;
				recno_idBuff.setLength(0);
				recno_idBuff.append(recno_id);	
				String reconid=new String();
				reconid="00"+recno_idBuff.toString();
				t.setRECNO(StrUtils.right(reconid,2));
				log.trace("reconid =" + StrUtils.right(reconid,2));
//---------------------------------------------------------------------------------				

				
				t.setCOLOR(row.getValue("COLOR"));
				t.setTERM(row.getValue("TERM"));
				String itrstr=row.getValue("ITRX");
				String[] itr=new String[6];
				log.trace("itrstr =" + itrstr + "|");
				log.trace("itr.length =" + itr.length);
				for(int i=0;i<itr.length;i++)
				{
					itr[i]=itrstr.substring(i*7, 7+i*7);
					log.trace("itr[" + i + "]=" + itr[i] + "|");
				}
				t.setITR1(itr[0]);
				t.setITR2(itr[1]);
				t.setITR3(itr[2]);
				t.setITR4(itr[3]);
				t.setITR5(itr[4]);
				t.setITR6(itr[5]);
				t.setFILL(row.getValue("FILL"));
				
				log.info("ITRN026 Save >>{}" ,t.toString());
//				TODO 如果table還沒改好
				itrN026Dao.insert(t);
//				TODO 如果table已改好
				//itrN026Dao.save(t);
			}
		});
		
		simpleTemplate.setExceptionHandler(new ExceptionHandler() {

			public void handle(Row row, Exception e) {
				
				log.error("無法更新債券及票券參考利率.", e);
				
			}
			
		});
		BatchCounter counter = simpleTemplate.execute(params);
		} catch (TopMessageException e) {
			batchresult.setSuccess(false);
			data.put("TOPMSG", e.getMsgcode() + "  " + e.getMsgout());
			log.error("TopMessageException  !!", e);
		} catch (UncheckedException e) {
			batchresult.setSuccess(false);
			data.put("TOPMSG", "UncheckedException 執行有誤 !!");
			log.error("UncheckedException 執行有誤 !!", e);
		}
		
		
		try {
			if (batchresult.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName, batchresult.getData());
			}
		} catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}
		
		return batchresult;

	}
	
//	現在不寫檔案改回傳內容 註解掉
//	int writedatfile(String tranname)
//	{
//		String FullFileName=new String();
//		String WBuffer=new String();
//		ITRINTERVAL t = new ITRINTERVAL();
//		try
//		{
//			t=itrIntervalDao.get(tranname);
//			WBuffer=t.getINTERVAL().trim();
//		}
//		catch(ObjectNotFoundException objnotfund)
//		{
//			WBuffer="30";
//		}
//		System.out.println("WBuffer = " + WBuffer);
//		
//		FullFileName=(String)setting.get("filepath");
//		FullFileName+=setting.get("filename");
//		System.out.println("FullFileName = " + FullFileName);
//		
//		PrintWriter pw = null;
//		try 
//		{
//			pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File((String)setting.get("filepath"),(String)setting.get("filename")), true) , "US-ASCII")));
//			System.out.println("WBuffer = " + WBuffer);
//			pw.println(WBuffer);
//			pw.flush();
//		}
//		catch(Exception e) {
//			throw new fstop.model.ModelException("載入 FieldTranslator Properties 錯誤 !", e);
//		}
//
//		finally {
//			try {
//				pw.close();
//			}
//			catch(Exception e) {
//			}
//		}
//
//		return(0);
//	}
	
	public MVH query(Map<String, String> params) {
		List<ITRN026> qresult = itrN026Dao.getAll();		
		addItrCount();		
		return new DBResult(qresult);
	}
	
	private void addItrCount()
	{
		Date d = new Date();
		String today = DateTimeUtils.getCDateShort(d);
		String time = DateTimeUtils.getTimeShort(d);
		ITRCOUNT it = null;
		int iCount = 1;//若尚未有點選過，第一次寫入的次數值為一
		try {
			it = itrCountDao.findById("N026");
		}
		catch(ObjectNotFoundException e) {
			it = null;
		}
		if(it != null)
		{
			String count = it.getCOUNT();
			if(!"".equals(count) || count!= null)
			{
				iCount = Integer.parseInt(count);//把原本的次數找出來
				iCount++;//原本的次數加一;
				it.setDATE(today);
				it.setTIME(time);
				it.setCOUNT(Integer.toString(iCount));
			}
		}else
		{
			it = new ITRCOUNT();
			it.setHEADER("N026");
			it.setDATE(today);
			it.setTIME(time);
			it.setCOUNT("1");
			
		}
		itrCountDao.save(it);
	}
}
