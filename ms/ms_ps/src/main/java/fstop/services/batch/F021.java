package fstop.services.batch;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.orm.dao.AdmHolidayDao;
import fstop.orm.dao.AdmMsgCodeDao;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.TxnFxRecordDao;
import fstop.orm.po.ADMHOLIDAY;
import fstop.orm.po.TXNFXRECORD;
import fstop.services.batch.annotation.Batch;
import fstop.telcomm.TelCommExec;
import fstop.util.CustomProperties;
import fstop.util.DateTimeUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 外幣關帳
 */
@Slf4j
@RestController
@Batch(id="batch.f021", name = "外幣關帳", description = "外幣關帳")
public class F021 implements BatchExecute {
//	private Logger logger = Logger.getLogger("fstop_txnfx");

    private CustomProperties realValue = new CustomProperties();

    private DecimalFormat fmt = new DecimalFormat("#0");

    @Autowired
    private TxnFxRecordDao txnFxRecordDao;

    @Autowired
    @Qualifier("f021Telcomm")
    private TelCommExec f021Telcomm;   //F021 電文

    @Autowired
    private AdmHolidayDao admholidayDao;

    @Autowired
	private SysBatchDao sysBatchDao;
    
    public F021() {
    }

    class CloseRecord {
        public String stan = "";
        public String custacc = "";
        public String benacc = "";
    }
    
    private List<String> pendingList = Arrays.asList("0202", "0203", "0301", "0302", "0901", "0902", "0903", "0906", "0907", "3303", "W109", "Z300", "E006", "E009", "E038", "Z200", "M301", "M310", "M311", "M350", "M422", "M423", "M427", "R999", "Z034", "Z033", "Z032", "Z031", "Z030", "Z029", "Z028", "Z027", "Z026", "Z025", "W052", "W053", "W054", "W056", "W059", "W063", "W064", "W067", "W076", "W077", "W097", "W098", "Z024", "Z023", "W103", "W104", "W112", "W700", "W701", "W702", "W703", "W704", "W705", "W706", "W707", "W999", "Z001", "Z002", "Z003", "Z004", "Z005", "Z006", "Z007", "Z008", "Z009", "Z010", "Z011", "Z012", "Z016", "Z019", "Z020", "W153", "W154", "W155", "M302", "W163", "W164", "W901", "W162", "W708");
    
    /**
     * args.get(0) method name
     */
    @RequestMapping(value = "/batch/f021", method = {RequestMethod.POST},
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public BatchResult execute(@RequestBody  List<String> args) {
    	List<String> safeArgs = ESAPIUtil.validStrList(args);

        log.info("開始執行外匯關帳" + (DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", new Date())));
		String ERRORMSG = "";
		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}

        //檢查今日是否為假日
        ADMHOLIDAY holiday = null;
        try {
            Date d = new Date();
            String today = DateTimeUtils.getCDateShort(d);
            log.info("[F021] today = " + today);

            String holidaytoday = DateTimeUtils.getDateShort(d);
            log.info("[F021] holidaytoday = " + holidaytoday);

            holiday = (ADMHOLIDAY) admholidayDao.findById(holidaytoday);
        } catch (Exception e) {
            holiday = null;
        }

        if (holiday != null) {
            Map<String, Object> data = new HashMap();

            data.put("COUNTER", new BatchCounter());
            data.put("__TOPMSG", "ZXXX");
            data.put("__TOPMSGTEXT", "非營業日．");

            log.info("[F021] __TOPMSGTEXT = " + "非營業日．");

            BatchResult batchresult = new BatchResult();
            batchresult.setSuccess(false);
            batchresult.setBatchName(getClass().getSimpleName());
            batchresult.setData(data);
            
        	try {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName,batchresult.getData());
			}catch (Exception e) {
				log.error("sysBatchDao.finish error >> {} ", e.getMessage());
			}
            
            return batchresult;
        }

        Map<String, Object> data = new HashMap();

		try {			
			if (safeArgs.size() > 0 && safeArgs.get(0).equals("U"))
				executeUpdate(args.get(1)); //ADTXNO
			else	
				executeMain(data);
		}
		catch(Exception e) {
			data.put("執行結果 :", "發生錯誤 ." + e.getMessage());
		}

        log.info("結束執行外匯關帳" + (DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", new Date())));

        BatchCounter counter = new BatchCounter();
        data.put("COUNTER", counter);

        BatchResult batchresult = new BatchResult();
        batchresult.setSuccess(true);
        batchresult.setBatchName(getClass().getSimpleName());
        batchresult.setData(data);
        
        try {
			// 新增SYSBATCH紀錄
			sysBatchDao.finish_OK(batchName);
		}catch (Exception e2) {
			log.error("sysBatchDao.finish error >> {} ", e2.getMessage());
		}
        
        
        return batchresult;
    }

    private void executeUpdate(String adtxno) {

        //外匯關帳
        Map<String, Object> data = new HashMap();

        List<CloseRecord> selfbankRecord = new ArrayList();
        List<CloseRecord> crossbankRecord = new ArrayList();


        String lastdate = DateTimeUtils.format("yyyyMMdd", new Date());

        TXNFXRECORD rec = txnFxRecordDao.findById(adtxno);

        /*
         * 2010/10/12 新增
         *
         * 判斷 F001 前置電文是否發送成功; 如果不成功,則不發送 F021 關帳通知(TXFLAG="U")
         */
        if (rec.getFXMSGSEQNO().indexOf("1") == -1) {
            return;
        }
		
		/*
			自行關帳, PCODE 要放 1000
			(轉入轉出帳號 前三碼皆為 893
			  或 轉入轉出帳號 前三碼皆不為 893)

			他行關帳, PCODE 要放 2000
		 */
        String str_ADOPID = null;

        String inAcc = StrUtils.trim(rec.getFXSVAC()); //轉入帳號
        String outAcc = StrUtils.trim(rec.getFXWDAC()); //轉出帳號
        str_ADOPID = rec.getADOPID();

        CloseRecord r = new CloseRecord();

        if (str_ADOPID.equals("F003"))
            r.custacc = ""; //轉出帳號
        else
            r.custacc = outAcc; //轉出帳號

        if (str_ADOPID.equals("F002"))
            r.benacc = "";      //轉入帳號(匯出匯款)
        else
            r.benacc = inAcc;   //轉入帳號

        String titainfojson=rec.getFXTITAINFO();
        Map titaMap = CodeUtil.fromJson(titainfojson, Map.class);
        
        r.stan = (String) titaMap.get("STAN");

        if (isSelf(inAcc, outAcc) &&
                (!str_ADOPID.equals("F002") && !str_ADOPID.equals("F003"))) {  //是否為自行

            //自行
            selfbankRecord.add(r);
        } else {
            //他行
            crossbankRecord.add(r);
        }

        boolean isSelfSuccess = false;
        boolean isSelfSendTelcomm = false;
        boolean isCrossSuccess = false;
        boolean isCrossSendTelcomm = false;
        int i_RetryTimes = 0;

        while (true) {

            //他行關帳
            try {
                //if (! isCrossSendTelcomm && crossbankRecord.size() > 0) {
                if (!isCrossSuccess && crossbankRecord.size() > 0) {

                    try {
                        Hashtable params = new Hashtable();

                        String occur = toTelcommContent(crossbankRecord);

                        params.put("CNT", crossbankRecord.size() + "");
                        params.put("F021DATA", occur);
                        params.put("PCODE", "2000");
                        params.put("TXFLAG", "U");
                        // params.put("FILLER", "A");  // 20160712 配合FX GATEWAY	修改固定走A通道

                        isCrossSendTelcomm = true;
                        f021Telcomm.query(params);

                        isCrossSuccess = true;
                    } catch (Exception e) {
                        isCrossSuccess = false;
                        String errMsg = "他行關帳發生錯誤(" + (isCrossSendTelcomm ? "已發送 F021 電文" : "未發送 F021電文") + ") ." + e.getMessage();
                        log.error(errMsg, e);
                        data.put("他行關帳錯誤訊息 :", errMsg);
                    }

                    if (isCrossSuccess) {
                        log.info("外匯他行已關帳.");
                        data.put("他行關帳執行結果 :", "成功");
                    } else {
                        log.info("外匯他行有問題.");
                        data.put("他行關帳執行結果 :", "失敗");
                    }

                }//end if (! isCrossSendTelcomm)

                else {
                    //isCrossSendTelcomm = true;     //ignore

                    isCrossSuccess = true;     //ignore
                }
            } finally {
            }

            //自行關帳
            try {
                //if (! isSelfSendTelcomm && selfbankRecord.size() > 0) {
                if (!isSelfSuccess && selfbankRecord.size() > 0) {

                    try {
                        Hashtable params = new Hashtable();

                        String occur = toTelcommContent(selfbankRecord);

                        params.put("CNT", selfbankRecord.size() + "");
                        params.put("F021DATA", occur);
                        params.put("PCODE", "1000");
                        params.put("TXFLAG", "U");
                        
                        isSelfSendTelcomm = true;
                        f021Telcomm.query(params);

                        isSelfSuccess = true;
                    } catch (Exception e) {
                        isSelfSuccess = false;
                        String errMsg = "自行關帳發生錯誤(" + (isSelfSendTelcomm ? "已發送 F021 電文" : "未發送 F021電文") + ") ." + e.getMessage();
                        log.error(errMsg, e);
                        data.put("自行關帳錯誤訊息 :", errMsg);
                    }

                    if (isSelfSuccess) {
                        log.info("外匯自行已關帳.");
                        data.put("自行關帳執行結果 :", "成功");
                    } else {
                        log.info("外匯自行有問題.");
                        data.put("自行關帳執行結果 :", "失敗");
                    }

                }//end if (! isSelfSendTelcomm)

                else {
                    //isSelfSendTelcomm = true;     //ignore

                    isSelfSuccess = true;     //ignore
                }
            } finally {
            }

            try {
                //if (isSelfSendTelcomm == true && isCrossSendTelcomm == true) {
                if (isSelfSuccess == true && isCrossSuccess == true) {
                    break;
                } else {
                    if (i_RetryTimes >= 10)
                        break;
                    else {
                        Thread.sleep(60000); //關帳通知有異常時,等候 1 分鐘再繼續發送

                        i_RetryTimes++;
                    }
                }
            } catch (Exception e) {
                String errMsg = "F021 -> Thread.sleep() 時發生錯誤 : " + e.getMessage();
                log.error(errMsg, e);
            }

        }//end while (true)
    }

    private void executeMain(Map<String, Object> data) {

        //外匯關帳

        List<CloseRecord> selfbankRecord = new ArrayList();
        List<CloseRecord> crossbankRecord = new ArrayList();


        String lastdate = DateTimeUtils.format("yyyyMMdd", new Date());
        List<TXNFXRECORD> fxOnlineResult = txnFxRecordDao.findOnlineFailed(lastdate , pendingList);
        List<TXNFXRECORD> fxScheduleResult = txnFxRecordDao.findScheduleFailed(lastdate , pendingList );
        
        
        /****** 過濾掉可自動重送,但不可人工重送之交易資料 START ******/
        AdmMsgCodeDao admMsgCodeDao = (AdmMsgCodeDao) SpringBeanFactory.getBean("admMsgCodeDao");

        for (int i = 0; i < fxOnlineResult.size(); i++) {
            TXNFXRECORD rec = (TXNFXRECORD) fxOnlineResult.get(i);

            String str_ResendFlag = admMsgCodeDao.findFXRESEND(rec.getFXEXCODE());
            if (str_ResendFlag.equals("N") ||
                    (!rec.getADOPID().equals("F001") && !rec.getADOPID().equals("F002") && !rec.getADOPID().equals("F003"))) {
                fxOnlineResult.remove(i);
                i--;
            }
        }

        for (int i = 0; i < fxScheduleResult.size(); i++) {
            TXNFXRECORD rec = (TXNFXRECORD) fxScheduleResult.get(i);

            String str_ResendFlag = admMsgCodeDao.findFXRESEND(rec.getFXEXCODE());
            if (str_ResendFlag.equals("N") ||
                    (!rec.getADOPID().equals("F001") && !rec.getADOPID().equals("F002"))) {
                fxScheduleResult.remove(i);
                i--;
            }
        }
        /****** 過濾掉可自動重送,但不可人工重送之交易資料 END ******/
		
		
		/*
			自行關帳, PCODE 要放 1000
			(轉入轉出帳號 前三碼皆為 893
			  或 轉入轉出帳號 前三碼皆不為 893)

			他行關帳, PCODE 要放 2000
		 */
        String str_ADOPID = null;
        List[] rAry = new List[]{
                fxOnlineResult, fxScheduleResult};

        for (List<TXNFXRECORD> recList : rAry) {
            for (TXNFXRECORD rec : recList) {
                String inAcc = StrUtils.trim(rec.getFXSVAC()).trim(); //轉入帳號
                String outAcc = StrUtils.trim(rec.getFXWDAC()).trim(); //轉出帳號
                str_ADOPID = rec.getADOPID();

                if (!str_ADOPID.equals("F001") && !str_ADOPID.equals("F002") && !str_ADOPID.equals("F003"))
                    continue;

                CloseRecord r = new CloseRecord();

                if (str_ADOPID.equals("F003"))
                    r.custacc = ""; //轉出帳號
                else
                    r.custacc = outAcc; //轉出帳號

                if (str_ADOPID.equals("F002"))
                    r.benacc = "";      //轉入帳號(匯出匯款)
                else
                    r.benacc = inAcc;   //轉入帳號
                
                String titainfojson=rec.getFXTITAINFO();
                Map titaMap = CodeUtil.fromJson(titainfojson, Map.class);
                
                r.stan = (String) titaMap.get("STAN");

                if (isSelf(inAcc, outAcc) &&
                        (!str_ADOPID.equals("F002") && !str_ADOPID.equals("F003"))) {  //是否為自行

                    //自行
                    selfbankRecord.add(r);
                } else {
                    //他行
                    crossbankRecord.add(r);
                }

            }

        }

        boolean isSelfSuccess = false;
        boolean isSelfSendTelcomm = false;
        boolean isCrossSuccess = false;
        boolean isCrossSendTelcomm = false;
        int i_RetryTimes = 0;

        while (true) {

            //他行關帳
            try {
                //if (! isCrossSendTelcomm) {
                if (!isCrossSuccess) {

                    try {
                        Hashtable params = new Hashtable();

                        String occur = toTelcommContent(crossbankRecord);

                        params.put("CNT", crossbankRecord.size() + "");
                        params.put("F021DATA", occur);
                        params.put("PCODE", "2000");
                        params.put("TXFLAG", "A");

                        isCrossSendTelcomm = true;
                        f021Telcomm.query(params);

                        isCrossSuccess = true;
                    } catch (Exception e) {
                        isCrossSuccess = false;
                        String errMsg = "他行關帳發生錯誤(" + (isCrossSendTelcomm ? "已發送 F021 電文" : "未發送 F021電文") + ") ." + e.getMessage();
                        log.error(errMsg, e);
                        data.put("他行關帳錯誤訊息 :", errMsg);
                    }

                    if (isCrossSuccess) {
                        log.info("外匯他行已關帳.");
                        data.put("他行關帳執行結果 :", "成功");
                    } else {
                        log.info("外匯他行有問題.");
                        data.put("他行關帳執行結果 :", "失敗");
                    }

                }//end if (! isCrossSendTelcomm)
            } finally {
            }

            //自行關帳
            try {
                //if (! isSelfSendTelcomm) {
                if (!isSelfSuccess) {

                    try {
                        Hashtable params = new Hashtable();

                        String occur = toTelcommContent(selfbankRecord);

                        params.put("CNT", selfbankRecord.size() + "");
                        params.put("F021DATA", occur);
                        params.put("PCODE", "1000");
                        params.put("TXFLAG", "A");

                        isSelfSendTelcomm = true;
                        f021Telcomm.query(params);

                        isSelfSuccess = true;
                    } catch (Exception e) {
                        isSelfSuccess = false;
                        String errMsg = "自行關帳發生錯誤(" + (isSelfSendTelcomm ? "已發送 F021 電文" : "未發送 F021電文") + ") ." + e.getMessage();
                        log.error(errMsg, e);
                        data.put("自行關帳錯誤訊息 :", errMsg);
                    }

                    if (isSelfSuccess) {
                        log.info("外匯自行已關帳.");
                        data.put("自行關帳執行結果 :", "成功");
                    } else {
                        log.info("外匯自行有問題.");
                        data.put("自行關帳執行結果 :", "失敗");
                    }

                }//end if (! isSelfSendTelcomm)
            } finally {
            }

            try {
                //if (isSelfSendTelcomm == true && isCrossSendTelcomm == true) {
                if (isSelfSuccess == true && isCrossSuccess == true) {
                    break;
                } else {
                    if (i_RetryTimes >= 2)
                        break;
                    else {
                        Thread.sleep(10000); //關帳通知有異常時,等候 1 分鐘再繼續發送
                        
                        i_RetryTimes++;
                    }
                }
            } catch (Exception e) {
                String errMsg = "F021 -> Thread.sleep() 時發生錯誤 : " + e.getMessage();
                log.error(errMsg, e);
            }

        }//end while (true)
    }

    //是否為自行
    private boolean isSelf(String inAcc, String outAcc) {
        return (inAcc.startsWith("893") && outAcc.startsWith("893"))
                || (!inAcc.startsWith("893") && !outAcc.startsWith("893"));
    }


    private String toTelcommContent(List<CloseRecord> records) {
        StringBuffer result = new StringBuffer();
        String stan = null;

        for (CloseRecord r : records) {
            //負號, 表示往左靠
            //STAN SEQNO CUSTACC BENACC
            stan = r.stan.trim();

            String s;

            if (stan.equals("")) {
                s = String.format("%s%s%-11s%-11s", StrUtils.repeat("0", 8),
                        StrUtils.repeat("0", 5), r.custacc, r.benacc);
            } else {
                s = String.format("%-8s%s%-11s%-11s", stan, StrUtils.repeat("0", 5), r.custacc, r.benacc);
            }

            result.append(s);
        }

        return result.toString();
    }


}
