package fstop.services.batch;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.exception.UncheckedException;
import fstop.model.DBResult;
import fstop.model.EachRowCallback;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.orm.dao.ItrCountDao;
import fstop.orm.dao.ItrIntervalDao;
import fstop.orm.dao.ItrN030Dao;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.po.ITRCOUNT;
import fstop.orm.po.ITRN030;
import fstop.services.AfterSuccessQuery;
import fstop.services.batch.annotation.Batch;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 外幣匯率更新
 * 
 * @author Owner
 * 
 */
@Slf4j
@RestController
@Batch(id = "batch.n030", name = "外幣匯率更新", description = "外幣匯率更新")
public class N030 extends DispatchBatchExecute implements BatchExecute {

	@Autowired
	@Qualifier("n031Telcomm")
	private TelCommExec n031Telcomm;

	@Autowired
	@Qualifier("n032Telcomm")
	private TelCommExec n032Telcomm;

	@Autowired
	private ItrN030Dao itrN030Dao;

	@Autowired
	private ItrCountDao itrCountDao;

	// @Autowired
	// private ItrCount itrcount;

	@Autowired
	private ItrIntervalDao itrIntervalDao;

	@Autowired
	private SysBatchDao sysBatchDao;

	/**
	 * args[0] method name
	 */
	@RequestMapping(value = "/batch/n030", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	@Override
	public BatchResult execute(@RequestBody List<String> args) {
		List<String> safeArgs = ESAPIUtil.validStrList(args);
		log.info("batch N030...");
		log.info(ESAPIUtil.vaildLog("safeArgs: {}" + JSONUtils.toJson(safeArgs)));
		// cache = new CacheService("distributeCache");

		BatchResult batchresult = new BatchResult();
		batchresult.setBatchName(getClass().getSimpleName());

		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}

		Date d = new Date();
		String today = DateTimeUtils.getDateShort(d);
		String time = DateTimeUtils.getTimeShort(d);

		batchresult.setSuccess(true);

		doN031_N032(today, time, batchresult);

		try {
			if (batchresult.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName, batchresult.getData());
			}
		} catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}

		return batchresult;

	}

	public MVH query(Map<String, String> params) {
		// cache = new CacheService("distributeCache");
		List<ITRN030> qresult = itrN030Dao.getAll();
		// addItrCount();

		Double N030HitCount = 0.0;

		N030HitCount++;

		if (qresult != null) {
			log.debug("Result Record ======================> {} ", qresult.size());
		} else {
			log.debug(">>>>>>>>>>>>>>>>> Result is null!! <<<<<<<<<<<<<<<<<<< ");
		}
		return new DBResult(qresult);
	}

	private void addItrCount() {
		Date d = new Date();
		String today = DateTimeUtils.getCDateShort(d);
		String time = DateTimeUtils.getTimeShort(d);
		ITRCOUNT it = null;
		int iCount = 1;// 若尚未有點選過，第一次寫入的次數值為一
		try {
			// it = itrCountDao.get("N030");
			it = itrCountDao.findById("N030");
		} catch (ObjectNotFoundException e) {
			it = null;
		}
		if (it != null) {
			String count = it.getCOUNT();
			if (!"".equals(count) || count != null) {
				iCount = Integer.parseInt(count);// 把原本的次數找出來
				iCount++;// 原本的次數加一;
				it.setDATE(today);
				it.setTIME(time);
				it.setCOUNT(Integer.toString(iCount));
			}
		} else {
			it = new ITRCOUNT();
			it.setHEADER("N030");
			it.setDATE(today);
			it.setTIME(time);
			it.setCOUNT("1");

		}
		itrCountDao.save(it);
	}

	private void addItrHitCount(Double HitNum) {
		Date d = new Date();
		String today = DateTimeUtils.getCDateShort(d);
		String time = DateTimeUtils.getTimeShort(d);
		ITRCOUNT it = null;
		Double iCount = 0.0;// 若尚未有點選過，第一次寫入的次數值為一
		try {
			// it = itrCountDao.get("N030");
			it = itrCountDao.findById("N030");
		} catch (ObjectNotFoundException e) {
			it = null;
		}
		if (it != null) {
			String count = it.getCOUNT();
			if (!"".equals(count) || count != null) {
				iCount = Double.parseDouble(count);// 把原本的次數找出來
				iCount += HitNum;
				it.setDATE(today);
				it.setTIME(time);
				String FinalCount = Double.toString(iCount);
				if (FinalCount.indexOf(".") > -1) {
					FinalCount = FinalCount.substring(0, FinalCount.indexOf("."));
				}
				it.setCOUNT(FinalCount);
			}
		} else {
			it = new ITRCOUNT();
			it.setHEADER("N030");
			it.setDATE(today);
			it.setTIME(time);
			it.setCOUNT("1");

		}
		itrCountDao.save(it);
	}

	public void putList(ITRN030 itrn030, List<ITRN030> tmpList) {
		tmpList.add(itrn030);
	}

	/**
	 * 合併&排序 N031及N032
	 * 
	 * @param
	 */
	public void doN031_N032(String today, String time, BatchResult batchresult) {

		Map<String, Object> data = new HashMap();

		final StringBuffer totaheader = new StringBuffer();
		final StringBuffer totaseq = new StringBuffer();
		final StringBuffer totadate = new StringBuffer();
		final StringBuffer totatime = new StringBuffer();
		final StringBuffer totaofcry = new StringBuffer();
		final StringBuffer totafill = new StringBuffer();

		HashMap params = new HashMap();
		List<ITRN030> tmpList = new LinkedList<ITRN030>();
		final StringBuffer recno_idBuff = new StringBuffer();
		recno_idBuff.append("0");
		try {
			SimpleTemplate simpleTemplate = new SimpleTemplate(n031Telcomm);
			simpleTemplate.setAfterSuccessQuery(new AfterSuccessQuery() {
				public void execute(MVHImpl result) {
					if(!"99".equals(result.getValueByFieldName("TOPMSG"))){
						throw TopMessageException.create(result.getValueByFieldName("TOPMSG"));
					}
					totaheader.append(result.getValueByFieldName("HEADER"));
					totaseq.append(result.getValueByFieldName("SEQ"));
					totadate.append(result.getValueByFieldName("DATE"));
					totatime.append(result.getValueByFieldName("TIME"));
					totaofcry.append(result.getValueByFieldName("OFCRY"));
					totafill.append(result.getValueByFieldName("FILL"));
				}
			});

//			log.info("setEachRowCallback..");
			simpleTemplate.setEachRowCallback(new EachRowCallback() {

				@SuppressWarnings("unchecked")
				public void current(Row row) {
					ITRN030 t = new ITRN030();

					String cry = row.getValue("CRY");
					if (cry == null || cry.equals(""))
						return;

					t.setHEADER(totaheader.toString());
					t.setSEQ(totaseq.toString());
					t.setDATE(totadate.toString());
					t.setTIME(totatime.toString());
					t.setOFCRY(totaofcry.toString());
					t.setFILL(totafill.toString());
					t.setCRY(cry);
					t.setBUYITR(row.getValue("BUYITR"));
					t.setSELITR(row.getValue("SELITR"));
					t.setTERM(row.getValue("TERM"));

					putList(t, tmpList);

				}
			});

			simpleTemplate.setExceptionHandler(new ExceptionHandler() {

				public void handle(Row row, Exception e) {

					log.error("無法更新外幣匯率(N030)查詢.", e);

				}

			});

			BatchCounter counter = simpleTemplate.execute(params);

		} catch (TopMessageException e) {
			data.put("N031_TOPMSG", e.getMsgcode() + ":" + e.getMsgout());
			log.error("N031 TopMessageException  !!", e);
		} catch (Exception e) {
			data.put("N031_ERROR", e.getMessage());
			log.error("N031 Exception 執行有誤 !!", e);
		}

		log.info("N031 FINISH");
		log.info("PREPARE TO DO N032");

		totaheader.setLength(0);
		totaseq.setLength(0);
		totadate.setLength(0);
		totatime.setLength(0);
		totaofcry.setLength(0);
		totafill.setLength(0);

		// n032
		try {
			SimpleTemplate simpleTemplate = new SimpleTemplate(n032Telcomm);
			simpleTemplate.setAfterSuccessQuery(new AfterSuccessQuery() {
				public void execute(MVHImpl result) {
					if(!"99".equals(result.getValueByFieldName("TOPMSG"))){
						throw TopMessageException.create(result.getValueByFieldName("TOPMSG"));
					}
					totaheader.append(result.getValueByFieldName("HEADER"));
					totaseq.append(result.getValueByFieldName("SEQ"));
					totadate.append(result.getValueByFieldName("DATE"));
					totatime.append(result.getValueByFieldName("TIME"));
					totaofcry.append(result.getValueByFieldName("OFCRY"));
					totafill.append(result.getValueByFieldName("FILL"));
				}
			});

//			log.info("setEachRowCallback..");
			simpleTemplate.setEachRowCallback(new EachRowCallback() {

				@SuppressWarnings("unchecked")
				public void current(Row row) {
					ITRN030 t = new ITRN030();

					String cry = row.getValue("CRY");
					if (cry == null || cry.equals(""))
						return;

					t.setHEADER(totaheader.toString());
					t.setSEQ(totaseq.toString());
					t.setDATE(totadate.toString());
					t.setTIME(totatime.toString());
					t.setOFCRY(totaofcry.toString());
					t.setFILL(totafill.toString());
					t.setCRY(cry);
					t.setBUYITR(row.getValue("BUYITR"));
					t.setSELITR(row.getValue("SELITR"));
					t.setTERM(row.getValue("TERM"));

					putList(t, tmpList);

				}
			});

			simpleTemplate.setExceptionHandler(new ExceptionHandler() {

				public void handle(Row row, Exception e) {

					log.error("無法更新外幣匯率(N030)查詢.", e);

				}

			});

			BatchCounter counter = simpleTemplate.execute(params);

		} catch (TopMessageException e) {
			data.put("N032_TOPMSG", e.getMsgcode() + ":" + e.getMsgout());
			log.error("N032 TopMessageException  !!", e);
		} catch (UncheckedException e) {
			data.put("N032_ERROR", e.getMessage());
			log.error("N032 Exception 執行有誤 !!", e);
		}
		
		log.info("N032 FINISH");
		log.info("PREPARE TO SAVE");

		if (tmpList.size() > 0) {
			try {
				saveListN031_N032(tmpList);
			} catch (Exception e) {
				data.put("DB_SAVE_ERROR", e.getMessage());
				log.error("Exception 執行有誤 !!", e);
			}
		} else {
			data.put("ERROR", "LIST_EMPTY");
		}
		
		//如果data有值  代表有地方出錯了
		if(data.size()>0) {
			batchresult.setSuccess(false);
			batchresult.setData(data);
		}
	}

	public void saveListN031_N032(List<ITRN030> tmpList) {
		itrN030Dao.deleteAll();
		LinkedHashMap<String, LinkedList<ITRN030>> insertMap = new LinkedHashMap<String, LinkedList<ITRN030>>();
		LinkedList<String> termOrderList = new LinkedList<String>();
		termOrderList.add("001");
		termOrderList.add("004");
		termOrderList.add("002");
		termOrderList.add("030");
		termOrderList.add("060");
		termOrderList.add("090");
		termOrderList.add("120");
		termOrderList.add("180");

		List<Integer> numbers = Stream.iterate(1, n -> n + 1).limit(25).collect(Collectors.toList());
		int count = 1;
		// 整理順序
		for (int number : numbers) {
			LinkedList<ITRN030> list = new LinkedList<ITRN030>();
			for (ITRN030 po : tmpList) {
				if (po.getCRY().equals(String.format("%02d", number))) {
					int position = termOrderList.indexOf(po.getTERM());
					list.add(position, po);
				}
			}
			insertMap.put(String.format("%02d", number), list);
		}

		log.info("n031_n032 insertMap >> {}", insertMap);

		for (String mapKey : insertMap.keySet()) {
			LinkedList<ITRN030> insertList = insertMap.get(mapKey);
			for (ITRN030 each : insertList) {
				each.setRECNO(String.format("%02d", count));
				count++;
				itrN030Dao.insert(each);
			}
		}

	}

}
