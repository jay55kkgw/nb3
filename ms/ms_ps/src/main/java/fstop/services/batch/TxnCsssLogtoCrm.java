package fstop.services.batch;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.ESAPIUtil;

import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.SysParamDao;
import fstop.orm.dao.TxnCSSSLogDao;
import fstop.orm.po.TXNCSSSLOG;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import fstop.util.FtpBase64;
import fstop.util.FtpPathProperties;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@Batch(id = "batch.TxnCsssLogtoCrm", name = "", description = "")
public class TxnCsssLogtoCrm implements BatchExecute {

	@Autowired
	private TxnCSSSLogDao TxnCsssLogDao;

	@Autowired
	private SysParamDao sysParamDao;
	
	private final String startMMDD = "TxnCsssLogMailStartDate";
	
	private final String endMMDD = "TxnCsssLogMailEndDate";
	
	@Value("${crmn.ftp.ip}")
	private String crmn_ftp_ip;

	@Value("${isParallelCheck}")
	private String isParallelCheck;

	@Value("${doFTP}")
	private String doFTP;

	@Autowired
	private SysBatchDao sysBatchDao;

	@RequestMapping(value = "/batch/txncssslogtocrm", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {

		log.info("batch TxnCsssLogtoCrm...");

		BatchResult batchresult = new BatchResult();
		Map<String, Object> data = new LinkedHashMap();
		batchresult.setBatchName(getClass().getSimpleName());
		batchresult.setSuccess(true);

		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error(ESAPIUtil.vaildLog("sysBatchDao.initSysBatchPo error >>" + e.getMessage()));
		}

		List<String> safeArgs = ESAPIUtil.validStrList(args);
		Integer totalcnt = 0, failcnt = 0, okcnt = 0;
		String startDate="";
		String endDate ="";
		
		if (safeArgs.size() > 0) {
			startDate = safeArgs.get(0);
			endDate = safeArgs.get(1);
		} else {
			startDate = DateTimeUtils.getDateShort(new Date());
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy");
			try {
				Calendar rightNow = Calendar.getInstance();
				rightNow.setTime(new java.util.Date());
			    rightNow.add(Calendar.YEAR, -1);
				Date dt2 = rightNow.getTime();
				startDate = formatter.format(dt2) + sysParamDao.getSysParam(startMMDD);
				endDate =  formatter.format(dt2) + sysParamDao.getSysParam(endMMDD);
				log.debug(ESAPIUtil.vaildLog("FINAL startDate:" + startDate));
				log.debug(ESAPIUtil.vaildLog("FINAL endDate:" + startDate));
				
			} catch (Exception e) {
				log.error(ESAPIUtil.vaildLog("DATE ERROR:" + e));
				startDate = DateTimeUtils.getDateShort(new java.util.Date());
				endDate = DateTimeUtils.getDateShort(new java.util.Date());
			}
		}

		log.info(ESAPIUtil.vaildLog("startDate:" + startDate));
		log.info(ESAPIUtil.vaildLog("endDate:" + endDate));
		List<TXNCSSSLOG> txnCsssLogList = TxnCsssLogDao.getTxnCsssLogforcrm(startDate, endDate);
		log.info(ESAPIUtil.vaildLog("NB 3 data size >> "+ txnCsssLogList.size()));
		totalcnt = txnCsssLogList.size();
		// if ("Y".equals(isParallelCheck)) {
		// 	List<OLD_TXNLOG> oldList = old_TxnLogDao.getTxnLogforcrm(startDate);

		// 	log.info(ESAPIUtil.vaildLog("NB 2.5 data size >> "+ oldList.size()));

		// 	for (OLD_TXNLOG old_txnlog : oldList) {
		// 		TXNCSSSLOG nnbConvert2nb3 = CodeUtil.objectCovert(TXNCSSSLOG.class, old_txnlog);
		// 		txnCsssLogList.add(nnbConvert2nb3);
		// 	}
		// }

		StringBuilder writeFileString = new StringBuilder();
		Long LOGID;
		String CUSIDN = "";
		String ADOPID = "";
		String ANSWER = "";
		String NEXTBTN = "";
		String LASTDATE = "";
		String LASTTIME = "";
		String CHANNEL = "";
		String MSG = "";
		String Opt_Txt = "";
		
		TXNCSSSLOG txnCsssLogEx = new TXNCSSSLOG();

		int count = txnCsssLogList.size();
		log.info(ESAPIUtil.vaildLog("TxnCsssLogtoCrm.java TxnCsssLog size (ALL) : " + count));

		String datacount = String.format("%09d", count);

		try {

			FtpPathProperties nrcpro = new FtpPathProperties();
			String srcpath = nrcpro.getSrcPath("useractionrecord").trim();
			// Ltest
			// srcpath = "C:\\Users\\BenChien\\Desktop\\batchLocalTest\\data\\";
			String despath = nrcpro.getDesPath("useractionrecord").trim();
			String ip = crmn_ftp_ip;
			FtpBase64 ftpBase641 = new FtpBase64(ip);
			File saveFile = new File(srcpath + "EB_YTXNCSSSLOG.TXT." + startDate, "");
			saveFile.setWritable(true, true);
			saveFile.setReadable(true, true);
			saveFile.setExecutable(true, true);
			BufferedWriter fwriter = new BufferedWriter(
					new OutputStreamWriter(new FileOutputStream(saveFile), "BIG5"));
			// FileWriter fwriter = new FileWriter(new FileOutputStream(saveFile));

			// --------------產生String-----------------------------
			for (int i = 0; i < count; i++) {
				try {
					txnCsssLogEx = txnCsssLogList.get(i);

					LOGID = txnCsssLogEx.getLOGID();

					CUSIDN = txnCsssLogEx.getCUSIDN();

					ADOPID = txnCsssLogEx.getADOPID();

					ANSWER = txnCsssLogEx.getANSWER();

					NEXTBTN = txnCsssLogEx.getNEXTBTN();

					LASTDATE = txnCsssLogEx.getLASTDATE();

					LASTTIME = txnCsssLogEx.getLASTTIME();

					CHANNEL = txnCsssLogEx.getCHANNEL();

					MSG = txnCsssLogEx.getMSG();
					MSG = MSG.replaceAll("\\R","");
					
					Opt_Txt = startDate + ";" + LOGID + ";" + CUSIDN + ";" + ADOPID + ";" + ANSWER + ";"+ NEXTBTN + ";" + LASTDATE + ";" + LASTTIME + ";" +  CHANNEL + ";"+ MSG ;
					log.debug(ESAPIUtil.vaildLog("Opt_Txt:" + Opt_Txt));
					writeFileString.append(Opt_Txt);
					log.debug(ESAPIUtil.vaildLog("Opt_Txt append"));
					writeFileString.append("\r\n");
					log.debug("rn");
					okcnt++;
					if ((i + 1) % 10000 == 0) {
						fwriter.write(writeFileString.toString());
						writeFileString.delete(0, writeFileString.length());
						// writeFileString ="" ;
						log.debug(ESAPIUtil.vaildLog("TxnCsssLogtoCrm.java For Loop writeFileString:  " + i));
					}
				} catch (Exception e) {
					log.error(ESAPIUtil.vaildLog("TxnCsssLogtoCrm.java ERROR :" + e.getMessage()));
					log.error(ESAPIUtil.vaildLog("TxnCsssLogtoCrm.java ERROR : i=" + i + ", dateADTXNO:" + txnCsssLogEx.getLOGID()));
					failcnt++;
				}
			}
			log.debug("TxnCsssLogtoCrm.java For Loop create writeFileString Sccess");
			log.debug(ESAPIUtil.vaildLog("1writeFileString:" + writeFileString.toString()));
			// ---------------------------------------------------------------------------

			fwriter.write(writeFileString.toString());
			log.debug(ESAPIUtil.vaildLog("2writeFileString:" + writeFileString.toString()));
			fwriter.flush();
			log.debug("fwriter.flush()");
			fwriter.close();
			log.debug("fwriter.close()");

			log.debug("TxnCsssLogtoCrm.java fwriter.write  Sccess");
			if ("Y".equals(doFTP)) {
				int rc = ftpBase641.upload(despath + "EB_YTXNCSSSLOG.TXT." + startDate, new FileInputStream(saveFile));
//				if (rc == 0) {
//					File deleteFile = new File(srcpath + "EB_DTXNCSSSLOG.TXT." + startDate);
//					deleteFile.setWritable(true, true);
//					deleteFile.setReadable(true, true);
//					deleteFile.setExecutable(true, true);
//					deleteFile.delete();
//				}
				log.info("TxnCsssLogtoCrm.java FTP UPLOAD  Sccess");
			}

			try {
				log.debug("TxnCsssLogtoCrm.java dir start");
				StringBuilder writeFileString_dir = new StringBuilder();
				FtpPathProperties nrcpro_dir = new FtpPathProperties();
				String srcpath_dir = nrcpro_dir.getSrcPath("useractionrecord").trim();
				// Ltest
				// srcpath_dir = "C:\\Users\\BenChien\\Desktop\\batchLocalTest\\data\\";
				String despath_dir = nrcpro_dir.getDesPath("useractionrecord").trim();
				log.debug(ESAPIUtil.vaildLog("TxnCsssLogtoCrm.java dir srcpath_dir:" + srcpath_dir));
				log.debug(ESAPIUtil.vaildLog("TxnCsssLogtoCrm.java dir despath_dir:" + despath_dir));
				String ip_dir = crmn_ftp_ip;
				log.debug(ESAPIUtil.vaildLog("TxnCsssLogtoCrm.java dir ip_dir:" + ip_dir));
				FtpBase64 ftpBase641_dir = new FtpBase64(ip_dir);
				File saveFile_1 = new File(srcpath_dir + "dir.EB_YTXNCSSSLOG.TXT." + startDate);
				saveFile_1.setWritable(true, true);
				saveFile_1.setReadable(true, true);
				saveFile_1.setExecutable(true, true);
				// FileWriter fwriter_1 = new FileWriter(saveFile_1);
				OutputStream fwriter_1 = new FileOutputStream(saveFile_1);
				BufferedWriter out = new BufferedWriter(new OutputStreamWriter(fwriter_1, "BIG5"));
				// --------------產生dir_String-----------------------------
				String dir_txt = "EB_YTXNCSSSLOG.TXT." + startDate + " " + "9999" + " " + datacount;
				log.debug(ESAPIUtil.vaildLog("dir_txt:" + dir_txt));
				writeFileString_dir.append(dir_txt);
				log.debug(ESAPIUtil.vaildLog("writeFileString1:" + writeFileString_dir.toString()));
				// ---------------------------------------------------------------------------

				out.write(writeFileString_dir.toString());
				log.debug(ESAPIUtil.vaildLog("writeFileStringdir:" + writeFileString_dir.toString()));
				out.flush();
				log.debug("fwriter_dir.flush()");
				out.close();
				log.debug("fwriter_dir.close()");
				fwriter_1.close();

				log.debug("TxnCsssLogtoCrm_dir.java fwriter.write  Sccess");
				if ("Y".equals(doFTP)) {
					int rc1 = ftpBase641_dir.upload(despath_dir + "dir.EB_YTXNCSSSLOG.TXT." + startDate,
							new FileInputStream(saveFile_1));
//					if (rc1 == 0) {
//						File deleteFile_dir = new File(srcpath_dir + "dir.EB_DTXNCSSSLOG.TXT." + startDate);
//						deleteFile_dir.setWritable(true, true);
//						deleteFile_dir.setReadable(true, true);
//						deleteFile_dir.setExecutable(true, true);
//						deleteFile_dir.delete();
//					}
					log.info("TxnCsssLogtoCrm_dir.java FTP UPLOAD  Sccess");
				}

			} catch (Exception e) {
				log.error("TxnCsssLogtoCrm_dir.java ERROR :" + e.getMessage());
			}

		} catch (Exception e) {
			log.error(ESAPIUtil.vaildLog("TxnCsssLogtoCrm.java ERROR :" + e.getMessage()));
			data.put("Error", e.getMessage());
			batchresult.setSuccess(false);
			batchresult.setData(data);

			try {
				sysBatchDao.finish_FAIL2(batchName, batchresult.getData());
			} catch (Exception e2) {
				log.error(ESAPIUtil.vaildLog("sysBatchDao.finish error >>  " + e2.getMessage()));
			}
			return batchresult;

		}
		
		BatchCounter bcnt = new BatchCounter();
		bcnt.setFailCount(failcnt);
		bcnt.setSuccessCount(okcnt);
		bcnt.setTotalCount(totalcnt);

		data.put("COUNTER", bcnt);
		batchresult.setData(data);

		try {
			// 新增SYSBATCH紀錄
			sysBatchDao.finish_OK(batchName);
		} catch (Exception e) {
			log.error(ESAPIUtil.vaildLog("sysBatchDao.finish_OK error >>  " + e.getMessage()));
		}

		return batchresult;
	}
}
