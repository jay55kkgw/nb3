package fstop.services.batch;

import java.util.Hashtable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.netbank.rest.model.pool.FundUtils;
import com.netbank.util.ESAPIUtil;

import fstop.model.MVHImpl;
import fstop.orm.dao.TxnFundDataDao;
import fstop.orm.po.TXNFUNDDATA;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import topibs.utility.NumericUtil;

/*
class FUNDTYPE1011
{
	String rtncod=new String();    // 4 bytes
	String cdno=new String(); // 11 bytes
	String fund=new String();      // 4 bytes
	String fundamt=new String();  // 13 bytes
	String totalamt=new String(); // 11 bytes
	String cry=new String();      // 20 bytes
}

class FUNDTYPE2021
{
	String rtncod=new String();    // 4 bytes
	String cdno=new String(); // 11 bytes
	String fund=new String();      // 4 bytes
	String fundin=new String();    // 4 bytes
	String fundamt=new String();  // 13 bytes
	String fundfee1=new String(); // 11 bytes
	String fundfee2=new String(); // 11 bytes
	String fundfee3=new String(); // 11 bytes
	String totalamt=new String(); // 11 bytes
	String cry=new String();      // 20 bytes
}

class FUNDTYPE3031
{
	String rtncod=new String();    // 4 bytes
	String cdno=new String(); // 11 bytes
	String fund=new String();      // 4 bytes
	String fundamt=new String();  // 13 bytes
	String cry=new String();      // 20 bytes
}

class FUNDTYPE99
{
	String rtncod=new String();    // 1 bytes
	String email=new String();     // 60 bytes
}
*/
@Slf4j
@Component
public class C050Parser
{
	//private Logger logger = Logger.getLogger(getClass());
	
	private Hashtable fundtype1011 = new Hashtable();
	private Hashtable fundtype2021 = new Hashtable();
	private Hashtable fundtype3031 = new Hashtable();
	private Hashtable fundtype99 = new Hashtable();
	
	
	//private FundUtils fundutils=(FundUtils)SpringBeanFactory.getBean("fundUtils");


	@Autowired
	private FundUtils fundUtils;
	
	@Autowired
	private TxnFundDataDao txnFundDataDao;


	int setfundtype1011(String fundtype,String input)
	{
		fundtype1011.put("FUNDTYPE", fundtype);
		String rtncod=input.substring(0, 4);
		fundtype1011.put("RTNCOD",rtncod);
		String cdno=input.substring(4, 15);
		fundtype1011.put("CDNO",cdno);
		String fund=input.substring(15, 19);
		fundtype1011.put("FUND",fund);
//		MVHImpl fundData = (MVHImpl)fundUtils.getFundData(fund);
//		String fundtext=fundData.getValueByFieldName("FUNDSNAME");
		TXNFUNDDATA po = txnFundDataDao.findById(fund);
		String fundtext = po.getFUNDSNAME();
		
		log.debug(ESAPIUtil.vaildLog("fundtext= " + fundtext));
		log.debug("fundtext len = " + fundtext.length());
//		StringBuffer fundtextsp=new StringBuffer();
//		for(int i=(fundtext.length()*2);i<36;i++)
//		{
//			fundtextsp.append("&nbsp");
//		}
//		System.out.println("fundtextsp = " + fundtextsp.toString());
		fundtype1011.put("FUNDTEXT", fundtext );
		String fundamt=input.substring(19, 32);
		log.debug("fundamt len = " + fundamt.length());
		String fundamtfmt=NumericUtil.formatNumberString(fundamt.trim(), 2);
		log.debug(ESAPIUtil.vaildLog("fundamtfmt = " + fundamtfmt));
		log.debug("fundamtfmt len = " + fundamtfmt.length());
//		StringBuffer fundamtsp=new StringBuffer();
//		for(int i=fundamtfmt.trim().length();i<17;i++)
//		{
//			fundamtsp.append("&nbsp");
//		}
//		System.out.println("fundamtsp = " + fundamtsp.toString());
		fundtype1011.put("FUNDAMT",fundamtfmt.trim());
		String fundfee=input.substring(32, 43);
		//                 12345678901234
		String fundfeefmt=NumericUtil.formatNumberString(fundfee, 2);
//		StringBuffer fundfeesp=new StringBuffer();
//		for(int i=fundfeefmt.length();i<14;i++)
//		{
//			fundfeesp.append("&nbsp");
//		}
		fundtype1011.put("FUNDFEE",fundfeefmt);
		String totalamt=input.substring(43, 56);
		//                  12345678901234567
		String totalamtfmt=NumericUtil.formatNumberString(totalamt, 2);
//		StringBuffer totalamtsp=new StringBuffer();
//		for(int i=totalamtfmt.length();i<17;i++)
//		{
//			totalamtsp.append("&nbsp");
//		}
		fundtype1011.put("TOTALAMT",totalamtfmt);
		String cry=input.substring(56, 76);
		fundtype1011.put("CRY",cry);
		return(0);
	}

	Hashtable getfundtype1011()
	{
		return(fundtype1011);
	}

	int setfundtype2021(String fundtype,String input)
	{
		fundtype2021.put("FUNDTYPE", fundtype);
		String rtncod=input.substring(0, 4);
		fundtype2021.put("RTNCOD", rtncod);
		String cdno=input.substring(4, 15);
		fundtype2021.put("CDNO", cdno);
		String fund=input.substring(15, 19);
		fundtype2021.put("FUND", fund);
//		MVHImpl fundData = (MVHImpl)fundUtils.getFundData(fund);
//		String fundtext=fundData.getValueByFieldName("FUNDSNAME");
		TXNFUNDDATA po = txnFundDataDao.findById(fund);
		String fundtext = po.getFUNDSNAME();
//		StringBuffer fundtextsp=new StringBuffer();
//		for(int i=(fundtext.length()*2);i<36;i++)
//		{
//			fundtextsp.append("&nbsp");
//		}
		fundtype2021.put("FUNDTEXT", fundtext);
		String fundin=input.substring(19, 23);
//		MVHImpl fundinData = (MVHImpl)fundUtils.getFundData(fundin);
//		String fundintext=fundinData.getValueByFieldName("FUNDSNAME");
		TXNFUNDDATA po2 = txnFundDataDao.findById(fundin);
		String fundintext = po.getFUNDSNAME();
//		StringBuffer fundintextsp=new StringBuffer();
//		for(int i=fundtext.length();i<36;i++)
//		{
//			fundintextsp.append("&nbsp");
//		}
		fundtype2021.put("FUNDINTEXT", fundintext);
		String fundamt=input.substring(23, 36);
		//                  12345678901234567
		String fundamtfmt="                 " + NumericUtil.formatNumberString(fundamt, 2);
		fundtype2021.put("FUNDAMT", StrUtils.right(fundamtfmt,17));
		String fundfee1=input.substring(36, 47);
		String fundfee1fmt="              " + NumericUtil.formatNumberString(fundfee1, 2);
		fundtype2021.put("FUNDFEE1", StrUtils.right(fundfee1fmt,14));
		String fundfee2=input.substring(47, 58);
		String fundfee2fmt="              " + NumericUtil.formatNumberString(fundfee2, 2);
		fundtype2021.put("FUNDFEE2", StrUtils.right(fundfee2fmt,14));
		String fundfee3=input.substring(58, 69);
		String fundfee3fmt="              " + NumericUtil.formatNumberString(fundfee3, 2);
		fundtype2021.put("FUNDFEE3", StrUtils.right(fundfee3fmt,14));
		String totalamt=input.substring(69, 80);
		String totalamtfmt="              " + NumericUtil.formatNumberString(totalamt, 2);
		fundtype2021.put("TOTALAMT", StrUtils.right(totalamtfmt,14));
		String cry=input.substring(80, 100);
		fundtype2021.put("CRY", cry);
		return(0);
	}

	Hashtable getfundtype2021()
	{
		return(fundtype2021);
	}
	
	int setfundtype3031(String fundtype,String input)
	{
		fundtype3031.put("FUNDTYPE", fundtype);
		String rtncod=input.substring(0, 4);
		fundtype3031.put("RTNCOD",rtncod);
		String cdno=input.substring(4, 15);
		fundtype3031.put("CDNO",cdno);
		String fund=input.substring(15, 19);
		fundtype3031.put("FUND",fund);
//		MVHImpl fundData = (MVHImpl)fundUtils.getFundData(fund);
//		String fundtext=fundData.getValueByFieldName("FUNDSNAME");
		
		TXNFUNDDATA po = txnFundDataDao.findById(fund);
		String fundtext = po.getFUNDSNAME();
		
//		StringBuffer fundtextsp=new StringBuffer();
//		for(int i=(fundtext.length()*2);i<36;i++)
//		{
//			fundtextsp.append(" ");
//		}
		fundtype3031.put("FUNDTEXT", fundtext );
		String fundamt=input.substring(19, 32);
		//                  12345678901234567
		String fundamtfmt="                 " + NumericUtil.formatNumberString(fundamt, 2);
		fundtype3031.put("FUNDAMT",StrUtils.right(fundamtfmt,17));
		String cry=input.substring(32, 52);
		fundtype3031.put("CRY",cry);
		return(0);
	}
	
	Hashtable getfundtype3031()
	{
		return(fundtype3031);
	}
	
	int setfundtype99(String fundtype,String input)
	{
		fundtype99.put("FUNDTYPE", fundtype);
		String rtncod=input.substring(0, 1);
		fundtype99.put("RTNCOD1", rtncod);
		String email=input.substring(1);
		fundtype99.put("EMAIL", email);
		return(0);
	}
	
	Hashtable getfundtype99()
	{
		return(fundtype99);
	}
	
}
