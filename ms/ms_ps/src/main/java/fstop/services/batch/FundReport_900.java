package fstop.services.batch;


import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.rest.model.FieldTranslator;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.TxnCusInvAttrHistDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.oao.Old_TxnCusInvAttrHistDao;
import fstop.orm.oao.Old_TxnUserDao;
import fstop.orm.old.OLD_TXNCUSINVATTRHIST;
import fstop.orm.old.OLD_TXNUSER;
import fstop.orm.po.TXNCUSINVATTRHIST;
import fstop.orm.po.TXNUSER;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import fstop.util.FtpBase64;
import fstop.util.FtpPathProperties;
import lombok.extern.slf4j.Slf4j;
import topibs.utility.DateUtil;


/**
 * 
 * 基金線上客戶投資屬性問卷調查表 - 每日報表
 * 
 * @author Owner
 * 
 */
@Slf4j
@RestController
@Batch(id="batch.fundReport_900", name = "基金線上客戶投資屬性問卷調查表 - 每日報表", description = "讀DB，產生指定規格檔案，送給報表系統")
public class FundReport_900 implements BatchExecute {
	

	@Autowired
	private TxnCusInvAttrHistDao txnCusInvAttrHistDao;
	
	@Autowired
	private Old_TxnCusInvAttrHistDao old_TxnCusInvAttrHistDao;
	
	@Autowired
	private TxnUserDao txnUserDao;
	
	@Autowired
	private Old_TxnUserDao old_TxnUserDao;
	
	@Value("${isParallelCheck}")
	private String isParallelCheck;
		
	@Value("${doFTP}")
	private String doFTP;
	
	@Autowired
	private SysBatchDao sysBatchDao;
	
	@Value("${reportn.ftp.ip:}")
	private String reportn_ftp_ip;
	@Value("${batch.localfile.path:}")
	private String path;
	@Autowired
	private CheckServerHealth checkserverhealth;
	
	public FundReport_900(){}
	
	/**
	 * args[0] method name
	 */
	@RequestMapping(value = "/batch/fundReport_900", method = {RequestMethod.POST},
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {
		
		List<String> safeArgs = ESAPIUtil.validStrList(args);

		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}

		BatchResult batchresult = new BatchResult();
		batchresult.setBatchName(getClass().getSimpleName());
		Map<String, Object> data = new LinkedHashMap();
		int i_Counter = 0;
		int i_BhCount = 0;
		int i_PageNum = 1;
		int i_LastDateCount = 0;
		
		Date d = new Date();
		String today = DateTimeUtils.getDateShort(d);
		String str_StartDate = "";
		String str_EndDate = "";
		String str_LastDate = "";
		String str_MARK1 = "";
		if (safeArgs.size() > 1) {
			str_StartDate = safeArgs.get(0);
		    str_EndDate = safeArgs.get(1);
		}
		else {
			today = safeArgs.get(0);		
		}
		
		try {
			log.debug(ESAPIUtil.vaildLog("FundReport_900 START_DATE = " + str_StartDate));
			log.debug(ESAPIUtil.vaildLog("FundReport_900 END_DATE = " + str_EndDate));			
			log.debug(ESAPIUtil.vaildLog("FundReport_900 EXEC_DATE = " + today));
						
			List<TXNCUSINVATTRHIST> l_CusList = null;
			List<String> l_LastDateList = null;
			
			List<String> old_l_LastDateList = null;
			//排序
			Set<Integer> set = new TreeSet<Integer>();
			
			if (str_StartDate.equals("")) {
				i_LastDateCount = 1;
			}				
			else {
				//取出期間內有資料的日期列表
				l_LastDateList = txnCusInvAttrHistDao.findByLastDateRange(str_StartDate, str_EndDate);
				
				//平行驗證 加上舊網銀期間內有資料的日期列表
				if("Y".equals(isParallelCheck)){
					old_l_LastDateList = old_TxnCusInvAttrHistDao.findByLastDateRange(str_StartDate, str_EndDate);
					
					log.debug(ESAPIUtil.vaildLog("NB 3 data size >> " + l_LastDateList.size()));
					log.debug(ESAPIUtil.vaildLog("NB 2.5 data size >> " + old_l_LastDateList.size()));
					
					//排序去重複
					//新網銀日期區間資料 
					for(String newTime:l_LastDateList) {
						set.add(Integer.valueOf(newTime));
					}
					//舊網銀日期區間資料
					for(String oldTime:old_l_LastDateList) {
						set.add(Integer.valueOf(oldTime));
					}
					
					l_LastDateList.clear();
					
					//取得不重複時間List
					Iterator iterator=set.iterator();
					while(iterator.hasNext()){
						l_LastDateList.add(String.valueOf(iterator.next()));
					}
					
					i_LastDateCount = l_LastDateList.size();
					log.debug(ESAPIUtil.vaildLog("Parallel Date Data rows >> "+i_LastDateCount));
				}else {
					
					i_LastDateCount = l_LastDateList.size();
					log.debug(ESAPIUtil.vaildLog(" Date Data rows >> " + i_LastDateCount));
					
				}
												
			}
			
			StringBuffer sb_AllBh = new StringBuffer();
			StringBuffer sb_EachBh = new StringBuffer();
			//FileWriter fw_Each, fw_All;				
			TXNCUSINVATTRHIST inv, lastInv, nextInv;
						
			for (int l=0; l < i_LastDateCount; l++) {
																
				if (str_StartDate.equals("")) {
					str_LastDate = today;
					log.debug(ESAPIUtil.vaildLog("Date (one) >>{} "+str_LastDate));
				}	
				else { 								
					str_LastDate = l_LastDateList.get(l);
					log.debug(ESAPIUtil.vaildLog("Date (period) >> {} "+str_LastDate));
				}
				
				String str_ReportDate = DateUtil.formatDate(DateUtil.getTaiwanDate(str_LastDate));
				String str_ReportDateTime = DateTimeUtils.getCDateTime(DateTimeUtils.getCDateShort(d), DateTimeUtils.getTimeShort(d));					
				
				boolean fillBlank, skipHeader, skipFooter, firstRecord = true;
				String str_CurrBranchId = "";
				String str_LastBranchId = "";
				String str_NextBranchId = "";
				//找出符合日期資料
				l_CusList = txnCusInvAttrHistDao.findByLastDate(str_LastDate);
				
				//平行驗證
				if("Y".equals(isParallelCheck)){
					List<OLD_TXNCUSINVATTRHIST> old_l_CusList = old_TxnCusInvAttrHistDao.findByLastDate(str_LastDate);
					// 狀況一 新個網有資料,舊個網無
					if(l_CusList.size()!=0 && old_l_CusList.size()==0) {
						//使用l_CusList 不變
					}
					// 狀況二 新個網無資料 使用舊個網 
					if(l_CusList.size()==0 && old_l_CusList.size()!=0) {
						for(OLD_TXNCUSINVATTRHIST eachdate:old_l_CusList) {
							TXNCUSINVATTRHIST nnbtonb3 = CodeUtil.objectCovert(TXNCUSINVATTRHIST.class, eachdate);
							l_CusList.add(nnbtonb3);
						}
						
					}
					// 狀況三 兩邊都有資料 開始比對 
					if(l_CusList.size()!=0 && old_l_CusList.size()!=0) {
						//TABLE TXNCUSINVATTRHIST PK為DPUSERID , 如果兩邊有一樣的PK 用新的 , 如果沒有 用舊的
						l_CusList = getCombineList(l_CusList ,old_l_CusList);
					}
				}
				
				log.debug(ESAPIUtil.vaildLog("Date  "+ str_LastDate +"Rows >> "+ l_CusList.size()));
				
				for (int i=0; i < l_CusList.size(); i++) {
								
					inv = l_CusList.get(i);
					str_CurrBranchId = inv.getLASTUSER();
					str_MARK1 = inv.getMARK1().equals("Y") ? "是" : "否";
					log.debug(ESAPIUtil.vaildLog("基金線上客戶投資屬性問卷調查表-每日報表: 分行代號 = {} " + inv.getLASTUSER()));				
					log.debug(ESAPIUtil.vaildLog("基金線上客戶投資屬性問卷調查表-每日報表: USERID = {} " + inv.getFDUSERID()));
					
					log.debug(ESAPIUtil.vaildLog("@@@ FundReport_900.java str_CurrBranchId >> {} " + str_CurrBranchId));
					
					if (i > 0) {
						lastInv = l_CusList.get(i-1);
						str_LastBranchId = lastInv.getLASTUSER();
					}
					else {
						str_LastBranchId = ""; //此時為第一筆
					}
					
					if (i+1 < l_CusList.size()) {
						nextInv = l_CusList.get(i+1);
						str_NextBranchId = nextInv.getLASTUSER();
					}
					else {
						str_NextBranchId = ""; //此時為最後一筆 
					}
					
					log.debug(ESAPIUtil.vaildLog("@@@ FundReport_900.java str_NextBranchId >> {} "+str_NextBranchId));					
					
					if ( !str_CurrBranchId.equals(str_LastBranchId) || firstRecord ) {
						skipHeader = false;
						//skipFooter = true;  
						skipFooter = false; //modify By Sox 20100812   
					}
					else {
						//skipHeader = true; 
						skipHeader = false; //modify By Sox 20100812
						skipFooter = false;					
					}
					
					log.debug(ESAPIUtil.vaildLog("@@@ FundReport_900.java skipFooter >> "+skipFooter));	
					
					
					if ( !str_CurrBranchId.equals(str_NextBranchId) && firstRecord ) {
						fillBlank = true;
						skipFooter = false;										
					}	
					else {
						//fillBlank = false;
						fillBlank = true;    //modify By Sox 20100812
						skipFooter = false;								
					}				
					
					if (!str_CurrBranchId.equals(str_NextBranchId) || !(firstRecord) )
						firstRecord = true;	
					else	
						firstRecord = false;	

					log.debug(ESAPIUtil.vaildLog("@@@ FundReport_900.java inv.getFDUSERID() >> " + inv.getFDUSERID()));

					
					//取得使用者名稱
					List<TXNUSER> l_User = txnUserDao.findByUserId(inv.getFDUSERID());
					
					//平行驗證 若新網銀找不到(l_User.size()==0)則找舊網銀
					if("Y".equals(isParallelCheck) && l_User.size()==0){
						List<OLD_TXNUSER> old_l_User = old_TxnUserDao.findByUserId(inv.getFDUSERID());
						for(OLD_TXNUSER user :old_l_User) {
							log.trace(ESAPIUtil.vaildLog("Get NB2.5 User >> "+ user.getDPSUERID()));
							TXNUSER nnbConvert2nb3 = CodeUtil.objectCovert(TXNUSER.class, user);
							l_User.add(nnbConvert2nb3);
						}
						log.debug(ESAPIUtil.vaildLog("Old User >> "+inv.getFDUSERID()));
					}
					
					
					log.debug(ESAPIUtil.vaildLog("@@@ FundReport_900.java l_User.size() >> "+l_User.size()));
					
					
                    //若查無此人則略過!!
					if (l_User.size() == 0) {
						log.debug(ESAPIUtil.vaildLog("@@@ Can't find user >> "+inv.getFDUSERID()));
						continue;
					}
					
					TXNUSER u = l_User.get(0);

					log.debug(ESAPIUtil.vaildLog("@@@ FundReport_900.java TXNUSER getDPSUERID() >> {} " + u.getDPSUERID()));
					
					//Header
					if (!skipHeader) {

						//20151102-週期:每日變更為日報, 加註保存年限
						//sb_EachBh.append("1 報表代號 : CCD900                            線上客戶投資屬性問卷調查表                                週    期 : 每  日\n");
						sb_EachBh.append("1 報表代號 : CCD900                            線上客戶投資屬性問卷調查表                                週期/保存年限：日報 / 客戶終止往來後5年\n");
						
						sb_EachBh.append("  資料日期 : " + str_ReportDate + "                         印表日期 : " + str_ReportDateTime + "                             頁    次 :   " + i_PageNum + " \n");
						sb_EachBh.append("  需求單位 : " + inv.getLASTUSER() + "                                                                                         製表單位 : 資訊部\n");
						sb_EachBh.append("\n\n");
					}    	
	                				
					log.debug(ESAPIUtil.vaildLog("@@@ FundReport_900.java inv.getLASTUSER() >> " +inv.getLASTUSER()));
					
					//Body
					sb_EachBh.append(" 客      戶 : " + u.getDPUSERNAME());
					
					log.debug(ESAPIUtil.vaildLog("@@@ FundReport_900.java u.getDPUSERNAME() >> {} "+u.getDPUSERNAME()));
					
					for (int k=0; k < 32-(u.getDPUSERNAME().trim().length())*2; k++)
						sb_EachBh.append(" ");
					
					//KYC 報表新增日期 及IP 20210319
					String formatedTime = inv.getLASTTIME();
					try {
						formatedTime = LocalDateTime.parse(inv.getLASTDATE() + inv.getLASTTIME() , DateTimeFormatter.ofPattern("yyyyMMddHHmmss"))
								.format(DateTimeFormatter.ofPattern("HH:mm:ss"));
					} catch (Exception e) {
						log.warn("Parse Time Error" , e);
					}
					
					String str_NewFromatDate = str_ReportDate + " " + formatedTime;
					sb_EachBh.append(" 日    期 : " + str_NewFromatDate);
					
					for (int k=0; k < 32-(str_NewFromatDate.trim().length()); k++)
						sb_EachBh.append(" ");
					
					sb_EachBh.append(" I P : " + inv.getADUSERIP() + "\n");
					for (int k=0; k < 32-(str_NewFromatDate.trim().length())*2; k++)
						sb_EachBh.append(" ");
					
					sb_EachBh.append(" 身分證字號 : " + inv.getFDUSERID());
					
					for (int k=0; k < 32-inv.getFDUSERID().length(); k++)
						sb_EachBh.append(" ");
					
					sb_EachBh.append("您的投資屬性為 : " + FieldTranslator.transfer("FDINVTYPE", inv.getFDINVTYPE()) + "\n\n");				
					if(inv.getFDUSERID().length()==10) {  //自然人版
					String Q11 = "";
					String Q13 = "";
					sb_EachBh.append(" 1. 您的年齡為何？\n");
					sb_EachBh.append("    " + FieldTranslator.transfer("FDQ11", inv.getFDQ1()) + "\n\n");
					sb_EachBh.append(" 2. 您的教育程度為何？\n");
					sb_EachBh.append("    " + FieldTranslator.transfer("FDQ12", inv.getFDQ2()) + "\n\n");
					sb_EachBh.append(" 3. 您的職業為何？\n");
					sb_EachBh.append("    " + FieldTranslator.transfer("FDQ13", inv.getFDQ3()) + "\n\n");
					sb_EachBh.append(" 4. 您個人/家庭年收入為(新臺幣)？\n");
					sb_EachBh.append("    " + FieldTranslator.transfer("FDQ14", inv.getFDQ4()) + "\n\n");
					sb_EachBh.append(" 5. 您個人所得與資金來源？\n");
					sb_EachBh.append("    " + FieldTranslator.transfer("FDQ15", inv.getFDQ5()) + "\n\n");
					sb_EachBh.append(" 6. 您目前擁有之有價證券與存款合計數為(新臺幣)？\n");
					sb_EachBh.append("    " + FieldTranslator.transfer("FDQ16", inv.getFDQ6()) + "\n\n");
					sb_EachBh.append(" 7. 您投資的主要目的與需求為？\n");
					sb_EachBh.append("    " + FieldTranslator.transfer("FDQ17", inv.getFDQ7()) + "\n\n");
					sb_EachBh.append(" 8. 您計劃從何時開始提領您投資的部份金額？(現金流量期望)\n");
					sb_EachBh.append("    " + FieldTranslator.transfer("FDQ18", inv.getFDQ8()) + "\n\n");
					sb_EachBh.append(" 9. 您對投資之期望報酬率為？\n");
					sb_EachBh.append("    " + FieldTranslator.transfer("FDQ19", inv.getFDQ9()) + "\n\n");
					sb_EachBh.append(" 10. 您投資金融商品預計投資期限多長？\n");
					sb_EachBh.append("    " + FieldTranslator.transfer("FDQ110", inv.getFDQ10()) + "\n\n");
					sb_EachBh.append(" 11. 對於您過去的投資經驗？(本題可複選，取得分最高者計算)\n");
					for(int j=0;j<inv.getFDQ11().length();j++)
					{
					   Q11+="    " + FieldTranslator.transfer("FDQ111", inv.getFDQ11().substring(j,j+1));	
					}
					log.debug(ESAPIUtil.vaildLog("CCD900_Q11 >> {} " + Q11));
					sb_EachBh.append(Q11 + "\n\n");
					sb_EachBh.append(" 12. 請問您從事投資理財之時間？(投資時間)\n");
					sb_EachBh.append("    " + FieldTranslator.transfer("FDQ112", inv.getFDQ12()) + "\n\n"); 
					sb_EachBh.append(" 13. 您對金融商品的認識？(本題可複選，取得分最高者計算)\n");
					for(int j=0;j<inv.getFDQ13().length();j++)
					{
					   Q13+="    " + FieldTranslator.transfer("FDQ113", inv.getFDQ13().substring(j,j+1));	
					}
					log.debug(ESAPIUtil.vaildLog("CCD900_Q13 >> {}" +Q13));
					sb_EachBh.append(Q13 + "\n\n");
					sb_EachBh.append(" 14. 您可承受的投資損失風險波動範圍？\n");					
					sb_EachBh.append("    " + FieldTranslator.transfer("FDQ114", inv.getFDQ14()) + "\n\n");	
					sb_EachBh.append(" 15. 您偏好以下列何種商品做為您投資理財配置？\n");
					sb_EachBh.append("    " + FieldTranslator.transfer("FDQ115", inv.getFDQ15()) + "\n");
					sb_EachBh.append(" 是否領有全民健康保險重大傷病證明？ "+ str_MARK1+"\n");
					/*
					//補足空行
		            	if (fillBlank) {					
		            		for (int j=0; j<1; j++) {					
		            			sb_EachBh.append("\n");					
		            		}//end for j					
		            	}
					
	                //Footer
		            	if (!skipFooter) {
						
		            		if (!fillBlank) {					
		            			for (int j=0; j<1; j++) {						
		            				sb_EachBh.append("\n");						
		            			}//end for j						
		            		}
		            	}*/	
					}
					else {  //法人版
						String Q7="";
						String Q9="";
						sb_EachBh.append(" 1. 貴公司已設立幾年?\n"); 
						sb_EachBh.append("    " + FieldTranslator.transfer("FDQ21", inv.getFDQ1()) + "\n\n");
						sb_EachBh.append(" 2. 資本額?\n");
						sb_EachBh.append("    " + FieldTranslator.transfer("FDQ22", inv.getFDQ2()) + "\n\n");
						sb_EachBh.append(" 3. 貴公司的行業?\n");
						sb_EachBh.append("    " + FieldTranslator.transfer("FDQ23", inv.getFDQ3()) + "\n\n");
						sb_EachBh.append(" 4. 貴公司之年營收?\n");
						sb_EachBh.append("    " + FieldTranslator.transfer("FDQ24", inv.getFDQ4()) + "\n\n");
						sb_EachBh.append(" 5. 目前擁有的資產(存款+投資)金額總計有？\n");
						sb_EachBh.append("    " + FieldTranslator.transfer("FDQ25", inv.getFDQ5()) + "\n\n");
						sb_EachBh.append(" 6. 貴公司可供投資的資金主要來源？\n");
						sb_EachBh.append("    " + FieldTranslator.transfer("FDQ26", inv.getFDQ6()) + "\n\n");
						sb_EachBh.append(" 7. 貴公司曾經投資下列何種商品？(本題可複選，取得分最高者計算)\n");
						for(int j=0;j<inv.getFDQ7().length();j++)
						{
						   Q7+="    " + FieldTranslator.transfer("FDQ27", inv.getFDQ7().substring(j,j+1));	
						}						
						sb_EachBh.append(Q7 + "\n\n");
						sb_EachBh.append(" 8. 貴公司從事投資理財時間？\n");
						sb_EachBh.append("    " + FieldTranslator.transfer("FDQ28", inv.getFDQ8()) + "\n\n");
						sb_EachBh.append(" 9. 貴公司對金融商品的知識?(本題可複選，取得分最高者計算)\n");
						for(int j=0;j<inv.getFDQ9().length();j++)
						{
						   Q9+="    " + FieldTranslator.transfer("FDQ29", inv.getFDQ9().substring(j,j+1));	
						}						
						sb_EachBh.append(Q9 + "\n\n");
						sb_EachBh.append(" 10. 請問貴公司的投資需求？\n");
						sb_EachBh.append("    " + FieldTranslator.transfer("FDQ210", inv.getFDQ10()) + "\n\n");
						sb_EachBh.append(" 11. 期望年報酬率為何?\n");
						sb_EachBh.append("    " + FieldTranslator.transfer("FDQ211", inv.getFDQ11()) + "\n\n");
						sb_EachBh.append(" 12. 可忍受的損失為何，超過就考慮執行停損?\n");
						sb_EachBh.append("    " + FieldTranslator.transfer("FDQ212", inv.getFDQ12()) + "\n\n");
						sb_EachBh.append(" 13. 交易目的為何?\n");
						sb_EachBh.append("    " + FieldTranslator.transfer("FDQ213", inv.getFDQ13()) + "\n\n"); 
						sb_EachBh.append(" 14. 貴公司認為下列商品者風險最高？\n");
						sb_EachBh.append("    " + FieldTranslator.transfer("FDQ214", inv.getFDQ14()) + "\n\n"); 												
						//補足空行
			            	if (fillBlank) {					
			            		for (int j=0; j<3; j++) {					
			            			sb_EachBh.append("\n");					
			            		}//end for j					
			            	}
						
		                //Footer
			            	if (!skipFooter) {
							
			            		if (!fillBlank) {					
			            			for (int j=0; j<3; j++) {						
			            				sb_EachBh.append("\n");						
			            			}//end for j						
			            		}
			            	}	
					}
						sb_EachBh.append("9            	          保　管　人 :                    　覆　核 :                    　主　管 :            	          ");
		                				
						//判斷右上方顯示之頁數
						if (! str_CurrBranchId.equals(str_NextBranchId)) { 
							
							//Reset 頁數
							i_PageNum = 1;
							i_BhCount++;
							
							//不同分行須產生不同之報表檔
//							fw_Each = new FileWriter("C:\\Users\\user\\Desktop\\NBCCD900_" + str_CurrBranchId + ( (str_StartDate.equals("")) ? "" : ("." + DateUtil.getTaiwanDate(str_LastDate)) ));					
//							fw_Each.write(sb_EachBh.toString());
//							fw_Each.flush();		
//							fw_Each.close();				

							if (sb_AllBh.length() != 0)
								sb_AllBh.append("\n");
							
							sb_AllBh.append(sb_EachBh.toString());						
							sb_EachBh.delete(0, sb_EachBh.length());
						}							                	      
						else {						
							//if (firstRecord) {	
							//	i_PageNum++;	
							//	
							//	sb_EachBh.append("\n");						
							//}
							  i_PageNum++;
							  sb_EachBh.append("\n");
						}
					
					
					i_Counter++;
					
				}//end for i (分行)
				
			}//end for l (日期)		
			
			if (i_Counter > 0) {				
				
				log.debug(ESAPIUtil.vaildLog("@@@ FundReport_900.java i_Counter >> " + i_Counter));
				
				OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(path + "/NBCCD900"), "BIG5");
				out.write(sb_AllBh.toString());
				out.flush();
				out.close();				
				
				log.debug("@@@ FundReport_900.java after WRITE FILE ");
			}
			else {
				log.debug("無資料 刪除空白NBCCD900 ");
				
				File f = new File(path + "/NBCCD900");
				f.setWritable(true, true);
				f.setReadable(true, true);
				f.setExecutable(true, true);
				f.delete();
				f = null;
				
				data.put("RESULT", "NBCCD900 No Data , Finish ");
				batchresult.setSuccess(true);		
				batchresult.setBatchName(getClass().getSimpleName());	
				batchresult.setData(data);
			}
			
			sb_EachBh = null;
			sb_AllBh = null;				
		}
		catch (Exception e) {
			
			log.error("基金線上客戶投資屬性問卷調查表-每日報表: Exception == " + e);	
			
			
			data.put("DATE", today);				
			data.put("RESULT", "NBCCD900 Failed");
			data.put("ERROR", e.getMessage());
			
			batchresult.setSuccess(false);		
			batchresult.setData(data);
			
			try {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName,batchresult.getData());
			}catch (Exception e2) {
				log.error("sysBatchDao.finish error >> {} ", e2.getMessage());
			}
			
			return batchresult;				
		}

		if(i_Counter>0)
		{
			if("Y".equals(doFTP)) {
				FtpPathProperties nrcpro=new FtpPathProperties();
				String srcpath=nrcpro.getSrcPath("fundreport_900");
				log.debug(ESAPIUtil.vaildLog("srcpath = " + srcpath));
				String despath1=nrcpro.getDesPath("fundreport1_900");
				log.debug(ESAPIUtil.vaildLog("despath1 = " + despath1));
				String ipn = reportn_ftp_ip;
				FtpBase64 ftpBase64 = new FtpBase64(ipn);
				int rc=ftpBase64.upload(despath1, path + "/NBCCD900");
				if(rc!=0)
				{
					log.debug("FTP 失敗(fundreport 新主機)");
					//CheckServerHealth checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
					checkserverhealth.sendUnknowNotice("FTP 失敗(fundreport 新主機)");
					data.put("RESULT", " NBCCD900 Success , FTP Fail ");
					data.put("COUNT", " Total " + i_Counter + " rows Data");
					data.put("PAGENUM", " Total " + i_BhCount + " Branchs");
					batchresult.setSuccess(false);
					batchresult.setData(data);
					
					try {
						// 新增SYSBATCH紀錄
						sysBatchDao.finish_FAIL2(batchName,data);
					}catch (Exception e2) {
						log.error("sysBatchDao.finish error >> {} ", e2.getMessage());
					}
					
					return batchresult;
				}
				else
				{
					data.put("RESULT", "NBCCD900 Success , FTP Success");
					data.put("COUNT", "Total " + i_Counter + " rows Data");
					data.put("PAGENUM", "Total " + i_BhCount + " Branchs");
					batchresult.setSuccess(true);
					batchresult.setData(data);
					
				}
			}else {
				data.put("RESULT", "NBCCD900 Success");
				data.put("COUNT", "Total " + i_Counter + " rows Data");
				data.put("PAGENUM", "Total " + i_BhCount + " Branchs");
				batchresult.setSuccess(true);
				batchresult.setData(data);
			}
		}
		
		try {
			// 新增SYSBATCH紀錄
			sysBatchDao.finish_OK(batchName);
		}catch (Exception e2) {
			log.error("sysBatchDao.finish error >> {} ", e2.getMessage());
		}
		
		return batchresult;
	}
	
	
	private List<TXNCUSINVATTRHIST> getCombineList(List<TXNCUSINVATTRHIST> nb3s ,List<OLD_TXNCUSINVATTRHIST> nnbs){
		List<String> nb3list = new ArrayList();
		for(TXNCUSINVATTRHIST nb3:nb3s) {
			nb3list.add(nb3.getFDUSERID());
		}
		
		for(OLD_TXNCUSINVATTRHIST nnb:nnbs) {
			if(!nb3list.contains(nnb.getFDUSERID())) {
				TXNCUSINVATTRHIST nnbtonb3 = CodeUtil.objectCovert(TXNCUSINVATTRHIST.class,nnb);
				nb3s.add(nnbtonb3);
			}
		}
		
		return nb3s;
	}
}
