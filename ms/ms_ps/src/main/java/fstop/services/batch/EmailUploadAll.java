package fstop.services.batch;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.oao.Old_TxnUserDao;
import fstop.orm.old.OLD_TXNUSER;
import fstop.orm.po.TXNUSER;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import fstop.util.FtpBase64;
import fstop.util.FtpPathProperties;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 每日產生上傳 email 修改的清單至 /DecodeFolder/CreditCard
 * 
 * @author Owner
 *
 */
@Slf4j
@RestController
@Batch(id = "batch.emailUploadAll", name = "EmailUploadAll", description = "產生電子帳單全部EMAIL異動資料")
public class EmailUploadAll implements EmailUploadIf, BatchExecute {
	@Autowired
	private TxnUserDao txnUserDao;

	@Autowired
	Old_TxnUserDao old_TxnUserDao;

	@Value("${isParallelCheck}")
	private String isParallelCheck;

	@Value("${doFTP}")
	private String doFTP;

	@Value("${bh1.ftp.ip:}")
	private String bh_ftp_ip1;

	@Autowired
	private CheckServerHealth checkserverhealth;

	@Autowired
	private SysBatchDao sysBatchDao;

	@RequestMapping(value = "/batch/emailUploadAll", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {

		List<String> safeArgs = ESAPIUtil.validStrList(args);
		log.debug("INTO EmailUpload ... ");
		String ERRORMSG = "";
		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}

		BatchResult result = new BatchResult();

		try {
			result = exec();
		} catch (Exception e) {
			log.error("執行 Batch 有誤 !", e);
			result.setBatchName(this.getClass().getSimpleName());
			result.setSuccess(false);
			ERRORMSG = e.getMessage();
			result.setErrorCode(ERRORMSG);

			
		}
		try {
			if (result.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				if (!"".equals(ERRORMSG)) {
					sysBatchDao.finish_FAIL(batchName, ERRORMSG);
				} else {
					sysBatchDao.finish_FAIL2(batchName, result.getData());
				}
			}
		} catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}
		result.setBatchName(this.getClass().getSimpleName());
		return result;
	}

	private BatchResult exec() throws IOException {
		int okcnt = 0, failcnt = 0, totalcnt = 0;

		Date d = new Date();
		String today = DateTimeUtils.getCDateShort(d);
		today = today.substring(0, 7);
		String time = DateTimeUtils.getTimeShort(d);

		BatchResult result = new BatchResult();
		BatchCounter bcnt = new BatchCounter();
		Map<String, Object> data = new LinkedHashMap();

		log.debug("getDateShort = " + DateTimeUtils.getDateShort(d));
		FtpPathProperties nrcpro = new FtpPathProperties();
		String srcpath = nrcpro.getSrcPath("emailuploadall").trim();
		// LTest
		// srcpath = "C:/Users/BenChien/Desktop/batchLocalTest/other/";
		log.debug(ESAPIUtil.vaildLog("srcpath = " + srcpath));
		String despath = nrcpro.getDesPath("emailuploadall").trim();
		log.debug(ESAPIUtil.vaildLog("despath = " + despath));
		String despath1 = nrcpro.getDesPath("emailuploadall1").trim();
		log.debug(ESAPIUtil.vaildLog("despath1 = " + despath1));

		// Absolute Path Traversal
		String LFILENAME = srcpath + DateTimeUtils.getDateShort(d) + "_emailuploadall.txt";
		//

		File tmpFile = new File(LFILENAME);
		tmpFile.setWritable(true, true);
		tmpFile.setReadable(true, true);
		tmpFile.setExecutable(true, true);
		// FileWriter fos = new FileWriter(tmpFile);
		// BufferedWriter out = new BufferedWriter(fos);
		BufferedWriter out = new BufferedWriter(new FileWriter(tmpFile));
		// List<TXNUSER> all = txnUserDao.getAll();
		List<TXNUSER> all = txnUserDao.getBillUser();

		if ("Y".equals(isParallelCheck)) {
			List<OLD_TXNUSER> oldall = old_TxnUserDao.getBillUser();

			log.debug("NB 3 data size >> {}", all.size());
			log.debug("NB 2.5 data size >> {}", oldall.size());

			all = getCombineList(all, oldall);

		}

		String recPattern = "%-11s%-3s%-60s%-1s%-7s%-6s%-1s%-6s%-3s%-2s\r\n";

		for (TXNUSER user : all) {

			log.debug(ESAPIUtil.vaildLog("USERID: " + user.getDPSUERID() + ", EMAILDATE: " + user.getEMAILDATE()));
			String p1 = ""; // 客戶統編
			String p2 = ""; // 業務類別
			String pEmail = ""; // 電子郵件地址
			String p4 = ""; // 異動代碼 A or D
			String pToday = today; // 異動日期
			String pTime = time; // 異動時間
			String p7 = "2"; // 異動來源
			String p8 = "NBUSER"; // 異動人員
			String bhcod = "";
			String p9 = ""; // 保留欄位
			String p10 = "";
			String p11 = "";

			p1 = user.getDPSUERID();
			pEmail = StrUtils.trim(user.getDPMYEMAIL());

			boolean isAll = false;

			isAll = StrUtils.trim(user.getEMAILDATE()).startsWith(today);

			if (user.getADBRANCHID().length() == 0 || user.getADBRANCHID() == null
					|| user.getADBRANCHID().equals("   "))
				bhcod = "000";
			else
				bhcod = user.getADBRANCHID();

			if (pEmail.length() > 0 && pEmail != null) {
				if ("Y".equalsIgnoreCase(user.getDPBILL())) {
					p4 = "A";
					p2 = "NB";
					String emaildate = StrUtils.left(user.getEMAILDATE(), 7);
					String billdate = StrUtils.left(user.getBILLDATE(), 7);
					if (emaildate.compareTo(billdate) > 0) {
						pToday = StrUtils.left(user.getEMAILDATE(), 7);
						pTime = StrUtils.right(user.getEMAILDATE(), 6);
					} else {
						pToday = StrUtils.left(user.getBILLDATE(), 7);
						pTime = StrUtils.right(user.getBILLDATE(), 6);
					}
					log.debug("DPBILL pToday = " + pToday);
					log.debug("DPBILL pTime = " + pTime);
					String f = String.format(recPattern, p1, p2, pEmail, p4, pToday, pTime, p7, p8, bhcod, p9);
					out.write(f);
					okcnt++;
				}
				if ("N".equalsIgnoreCase(user.getDPBILL())) {
					p4 = "D";
					p2 = "NB";
					String emaildate = StrUtils.left(user.getEMAILDATE(), 7);
					String billdate = StrUtils.left(user.getBILLDATE(), 7);
					if (emaildate.compareTo(billdate) > 0) {
						pToday = StrUtils.left(user.getEMAILDATE(), 7);
						pTime = StrUtils.right(user.getEMAILDATE(), 6);
					} else {
						pToday = StrUtils.left(user.getBILLDATE(), 7);
						pTime = StrUtils.right(user.getBILLDATE(), 6);
					}
					log.debug("DPBILL pToday = " + pToday);
					log.debug("DPBILL pTime = " + pTime);
					String f = String.format(recPattern, p1, p2, pEmail, p4, pToday, pTime, p7, p8, bhcod, p9);
					out.write(f);
					okcnt++;
				}
				if ("Y".equalsIgnoreCase(user.getDPFUNDBILL())) {
					p4 = "A";
					p2 = "FD";
					String emaildate = StrUtils.left(user.getEMAILDATE(), 7);
					String funddate = StrUtils.left(user.getFUNDBILLDATE(), 7);
					if (emaildate.compareTo(funddate) > 0) {
						pToday = StrUtils.left(user.getEMAILDATE(), 7);
						pTime = StrUtils.right(user.getEMAILDATE(), 6);
					} else {
						pToday = StrUtils.left(user.getFUNDBILLDATE(), 7);
						pTime = StrUtils.right(user.getFUNDBILLDATE(), 6);
					}
					log.debug("FUNDBILL pToday = " + pToday);
					log.debug("FUNDBILL pTime = " + pTime);
					String f = String.format(recPattern, p1, p2, pEmail, p4, pToday, pTime, p7, p8, bhcod, p9);
					out.write(f);
					okcnt++;
				}
				if ("N".equalsIgnoreCase(user.getDPFUNDBILL())) {
					p4 = "D";
					p2 = "FD";
					String emaildate = StrUtils.left(user.getEMAILDATE(), 7);
					String funddate = StrUtils.left(user.getFUNDBILLDATE(), 7);
					if (emaildate.compareTo(funddate) > 0) {
						pToday = StrUtils.left(user.getEMAILDATE(), 7);
						pTime = StrUtils.right(user.getEMAILDATE(), 6);
					} else {
						pToday = StrUtils.left(user.getFUNDBILLDATE(), 7);
						pTime = StrUtils.right(user.getFUNDBILLDATE(), 6);
					}
					log.debug("FUNDBILL pToday = " + pToday);
					log.debug("FUNDBILL pTime = " + pTime);
					String f = String.format(recPattern, p1, p2, pEmail, p4, pToday, pTime, p7, p8, bhcod, p9);
					out.write(f);
					okcnt++;
				}
				if ("Y".equalsIgnoreCase(user.getDPCARDBILL())) {
					p4 = "A";
					p2 = "CC";
					String emaildate = StrUtils.left(user.getEMAILDATE(), 7);
					String cardbilldate = StrUtils.left(user.getCARDBILLDATE(), 7);
					if (emaildate.compareTo(cardbilldate) > 0) {
						pToday = StrUtils.left(user.getEMAILDATE(), 7);
						pTime = StrUtils.right(user.getEMAILDATE(), 6);
					} else {
						pToday = StrUtils.left(user.getCARDBILLDATE(), 7);
						pTime = StrUtils.right(user.getCARDBILLDATE(), 6);
					}
					log.debug("CARDBILL pToday = " + pToday);
					log.debug("CARDBILL pTime = " + pTime);
					String f = String.format(recPattern, p1, p2, pEmail, p4, pToday, pTime, p7, p8, bhcod, p9);
					out.write(f);
					okcnt++;
				}
				if ("N".equalsIgnoreCase(user.getDPCARDBILL())) {
					p4 = "D";
					p2 = "CC";
					String emaildate = StrUtils.left(user.getEMAILDATE(), 7);
					String cardbilldate = StrUtils.left(user.getCARDBILLDATE(), 7);
					if (emaildate.compareTo(cardbilldate) > 0) {
						pToday = StrUtils.left(user.getEMAILDATE(), 7);
						pTime = StrUtils.right(user.getEMAILDATE(), 6);
					} else {
						pToday = StrUtils.left(user.getCARDBILLDATE(), 7);
						pTime = StrUtils.right(user.getCARDBILLDATE(), 6);
					}
					log.debug("CARDBILL pToday = " + pToday);
					log.debug("CARDBILL pTime = " + pTime);
					String f = String.format(recPattern, p1, p2, pEmail, p4, pToday, pTime, p7, p8, bhcod, p9);
					out.write(f);
					okcnt++;
				}
			}
		}

		try {
			out.close();
		} catch (Exception e) {
		}
		// try {
		// fos.close();
		// }
		// catch(Exception e){}

		if (okcnt == 0) {
			data.put("COUNTER", bcnt);
			result.setSuccess(true);
			return result;
		}
		if ("Y".equals(doFTP)) {
			String ip1 = bh_ftp_ip1;
			FtpBase64 ftpBase641 = new FtpBase64(ip1);
			log.debug(ESAPIUtil.vaildLog("tmpFile = " + tmpFile.getPath()));
			int rc1 = ftpBase641.upload(despath1, new FileInputStream(tmpFile));
			// 目地 來源
			if (rc1 != 0) {
				log.debug(DateTimeUtils.getDateShort(d) + "_emailuploadall.txt FTP 失敗(new BillHunter 主機)");
				checkserverhealth.sendUnknowNotice(
						DateTimeUtils.getDateShort(d) + "_emailuploadall.txt FTP 失敗(new BillHunter 主機)");
				data.put("Error", "FTP Fail");
				bcnt.setFailCount(failcnt);
				bcnt.setSuccessCount(okcnt);
				bcnt.setTotalCount(okcnt);
				data.put("COUNTER", bcnt);
				result.setData(data);
				result.setSuccess(false);
				return result;
			}

			bcnt.setFailCount(failcnt);
			bcnt.setSuccessCount(okcnt);
			bcnt.setTotalCount(okcnt);

			data.put("COUNTER", bcnt);
			data.put("FTP_Status", "Sucecss");
			result.setData(data);
			result.setSuccess(true);
		} else {
			bcnt.setFailCount(failcnt);
			bcnt.setSuccessCount(okcnt);
			bcnt.setTotalCount(okcnt);

			data.put("COUNTER", bcnt);
			result.setData(data);
			result.setSuccess(true);
		}
		return result;

	}

	private List<TXNUSER> getCombineList(List<TXNUSER> nb3datas, List<OLD_TXNUSER> nnbdatas) {
		/*
		 * TXNUSER同步2.5 兩邊TXNUSER相同 合併規則: 找出 nnbdatas 裡"不"在 nb3datas的資料合併 Ex: nnbdatas:
		 * A123456789 , A123456814 nb3datas: A123456814 , W100089655 則合併結果是 : A123456789
		 * , A123456814(同步故資料相同,取nb3) , W100089655
		 */
		log.debug(ESAPIUtil.vaildLog("nb3datas >> {} "+ nb3datas.toString()));
		log.debug(ESAPIUtil.vaildLog("nnbdatas >> {} "+ nnbdatas.toString()));
		List<String> nb3userList = new ArrayList<String>();
		for (TXNUSER nb3 : nb3datas) {
			nb3userList.add(nb3.getDPSUERID());
		}
		for (OLD_TXNUSER nnb : nnbdatas) {
			if (!nb3userList.contains(nnb.getDPSUERID())) {
				TXNUSER nnbConvert2nb3 = CodeUtil.objectCovert(TXNUSER.class, nnb);
				nb3datas.add(nnbConvert2nb3);
			}
		}

		log.debug(ESAPIUtil.vaildLog("Combine nb3datas >>  "+ nb3datas.toString()));
		return nb3datas;
	}
}
