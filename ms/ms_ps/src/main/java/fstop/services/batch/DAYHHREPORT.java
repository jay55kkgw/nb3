package fstop.services.batch;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.ESAPIUtil;

import fstop.orm.dao.AdmDayHHReportDao;
import fstop.orm.dao.SysBatchDao;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 功能說明 :批次日時統計報表
 *
 */
@Slf4j
@RestController
@Batch(id="batch.DAYHHREPORT", name = "批次日時統計報表", description = "批次日時統計報表")
public class DAYHHREPORT  extends DispatchBatchExecute implements BatchExecute {

	private DecimalFormat fmt = new DecimalFormat("#0");

	@Autowired
	private AdmDayHHReportDao admDayHHReportDao;
	
	@Autowired
	private SysBatchDao sysBatchDao;
	
	
	@RequestMapping(value = "/batch/caldayhhreport", method = {RequestMethod.POST},
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {
		
		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}
		
		List<String> safeArgs = ESAPIUtil.validStrList(args);
		
		BatchResult batchresult = new BatchResult();
		Map<String, Object> data = new HashMap();
		boolean rs = true;
		
		Date d = new Date();
		String today = DateTimeUtils.getDateShort(d);
		String time = DateTimeUtils.getTimeShort(d);
		
		log.info("DATE = {}" , today);
		log.info("TIME = {}" , time);
		
		String yyyy = "";
		String mm = "";
		String dd = "";
		
		if(safeArgs.get(0).length()==7) {
			yyyy = String.valueOf(Integer.parseInt(safeArgs.get(0).substring(0, 3))+1911);
			mm = safeArgs.get(0).substring(3,5);
			dd = safeArgs.get(0).substring(5);
		}else {
			 yyyy = safeArgs.get(0).substring(0, 4);
			 mm = safeArgs.get(0).substring(4,6);
			 dd = safeArgs.get(0).substring(6);
		}
		
		log.info(ESAPIUtil.vaildLog("yyyy={},mm={},dd={} "+yyyy+","+mm+","+dd ));
		try {
			admDayHHReportDao.execCalProcedure(yyyy, mm, dd);
		}catch(Exception e){
			log.error("StroeProcError >> {}" ,e.getCause());
			data.put("ERRORMSG", e.getMessage());
			rs = false;
		}
		
		batchresult.setSuccess(rs);
		batchresult.setBatchName(getClass().getSimpleName());
		
		try {
			if (batchresult.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName,batchresult.getData());
			}
		}catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}
		return batchresult;
	}	
}
