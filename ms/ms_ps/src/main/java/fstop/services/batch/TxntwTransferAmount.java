package fstop.services.batch;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.notifier.NotifyAngent;
import fstop.orm.dao.AdmHolidayDao;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.SysParamDao;
import fstop.orm.dao.TxnTwSchPayDataDao;
import fstop.orm.po.TXNTWSCHPAYDATA;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@Batch(id = "batch.txntwtransferamount", name = "次日預約跨行轉帳金額通知", description = "次日預約跨行轉帳金額通知")
public class TxntwTransferAmount implements BatchExecute {

	@Autowired
	private TxnTwSchPayDataDao txnTwSchPayDataDao;

	@Autowired
	private AdmHolidayDao admHolidayDao;

	@Autowired
	private SysParamDao sysParamDao;

	@Autowired
	private SysBatchDao sysBatchDao;

	// private Map setting;

	@RequestMapping(value = "/batch/txntwtransferamount", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {

		BatchResult batchresult = new BatchResult();
		batchresult.setSuccess(true);
		batchresult.setBatchName(getClass().getSimpleName());
		Map<String, Object> data = new LinkedHashMap();

		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}

		List<String> safeArgs = ESAPIUtil.validStrList(args);
		try {
			Hashtable<String, String> varmail = new Hashtable();
			NumberFormat format = NumberFormat.getInstance();
			Date dNow = new Date();
			Calendar cal = Calendar.getInstance();
			List<TXNTWSCHPAYDATA> AMTresult = new LinkedList();
			String emailap = sysParamDao.getSysParam("TXNTWAMTMAIL").replace(";", ",");
			//測試用
//			emailap = "H15D302B@mail.tbb.com.tw";
			ArrayList datelist = new ArrayList();
			ArrayList amtlist = new ArrayList();
			String DAMT = "";
			int total = 0;
			int count = 0;
			String nowTime = "";

			cal.setTime(dNow);
			nowTime = DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", cal.getTime()); // 紀錄執行時間
			cal.add(Calendar.DATE, +1); // 增加一日使日期為次日
			log.debug("org cal.getTime()===>" + DateTimeUtils.getTimeShort(cal.getTime()));
			log.debug("TXNTWAMTMAIL emailap===>" + emailap);

			while (true) {
				datelist.add(DateTimeUtils.format("yyyy/MM/dd", cal.getTime())); // 將日期儲存在datelist中

				log.debug(" fcal.getTime()===>" + DateTimeUtils.format("yyyy/MM/dd", cal.getTime()));
				log.debug(" admHolidayDao.isAdmholiday===>" + admHolidayDao.isAdmholiday(cal.getTime()));
				if (!admHolidayDao.isAdmholiday(cal.getTime())) { // 判斷是否為假日
					AMTresult = txnTwSchPayDataDao.findScheduleNextDateAMT(cal.getTime());
					int sum = 0;
					if (AMTresult.size() > 0) {
						for (TXNTWSCHPAYDATA ar : AMTresult) {
							if (ar.getDPTXAMT() == null || ar.getDPTXAMT() == "") {
								ar.setDPTXAMT("0");
								;
							}
							total += Integer.parseInt(ar.getDPTXAMT());
							sum += Integer.parseInt(ar.getDPTXAMT());
							log.debug(" sum===>" + sum);
						}
					} else {
						sum += 0;
						log.debug(" sum.0 ===>" + sum);
					}
					amtlist.add(format.format(sum)); // 將預約轉帳金額儲存在amtlist中
					break;
				} else { // 若為假日則增加計算日期一天
					AMTresult = txnTwSchPayDataDao.findScheduleNextDateAMT(cal.getTime());
					int sum = 0;
					if (AMTresult.size() > 0) {
						for (TXNTWSCHPAYDATA ar : AMTresult) {
							if (ar.getDPTXAMT() == null || ar.getDPTXAMT() == "") {
								ar.setDPTXAMT("0");
								;
							}
							total += Integer.parseInt(ar.getDPTXAMT());
							sum += Integer.parseInt(ar.getDPTXAMT());
							log.debug(" sum===>" + sum);
						}
					} else {
						sum += 0;
						log.debug(" sum.0 ===>" + sum);
					}
					amtlist.add(format.format(sum)); // 將預約轉帳金額儲存在amtlist中
					cal.add(Calendar.DATE, +1);
				}
				count++;
			}

			log.debug("TxntwTransferAmount datelist ===>" + datelist);
			log.debug("TxntwTransferAmount datelist.size() ===>" + datelist.size());
			log.debug("TxntwTransferAmount amtlist ===>" + amtlist);
			log.debug("TxntwTransferAmount amtlist.size() ===>" + amtlist.size());

			for (int i = 0; i < datelist.size(); i++) {
				log.debug("TxntwTransferAmount datelist.get(0) ===>" + datelist.get(0));
				if (datelist.get(i) != null && amtlist.get(i) != null) {
					DAMT += datelist.get(i) + " : " + amtlist.get(i) + "元\n";
				}
				if (i == 10) {
					break;
				}
			}

			log.debug("TxntwTransferAmount DAMT ===>" + String.valueOf(DAMT));
			log.debug("TxntwTransferAmount total===>" + String.valueOf(total));

			varmail.put("NEXTDATE", DAMT);
			varmail.put("NOWTIME", nowTime);
			varmail.put("TOTAL", String.valueOf(format.format(total)));

			boolean isSendSuccess = NotifyAngent.sendNotice("TRANSFERAMOUNT", varmail, emailap, "batch");
			if (!isSendSuccess) {
				log.error("發送 Email 失敗.(" + emailap + ")");
				batchresult.setSuccess(false);
				data.put("Error", "發送 Email 失敗.(" + emailap + ")");
			}
		} catch (TopMessageException topmsg) {

			log.error("TopMessageException >> {} ", topmsg.getMsgcode());
			batchresult.setSuccess(false);
			data.put("Error", topmsg.getMsgcode());
			
		} catch (Exception ex) {

			log.error("Exception >> {} ", ex);
			batchresult.setSuccess(false);
			data.put("Error",ex.getMessage());
		}
		
		try {
			if (batchresult.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName,batchresult.getData());
			}
		}catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}
		
		return batchresult;
	}

}
