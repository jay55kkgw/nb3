package fstop.services.batch;

import java.io.BufferedReader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.ESAPIUtil;

import bank.comm.StringUtil;
import topibs.utility.NumericUtil;
import fstop.notifier.NotifyAngent;
import fstop.notifier.NotifyListener;
import fstop.orm.dao.AdmMsgCodeDao;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.SysParamDataDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.ADMCUSTTELNO;
import fstop.orm.po.SYSPARAMDATA;
import fstop.orm.po.TXNUSER;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 黃金存摺預約結果通知 FKAPRE.TXT
 * 
 * @author Owner
 *
 */
@Slf4j
@RestController
@Batch(id = "batch.goldBuyNotify", name = "讀檔案，發email 通知", description = "讀檔案，發email 通知")
public class GoldBuyNotify implements EmailUploadIf, BatchExecute {

	@Autowired
	private TxnUserDao txnUserDao;
	@Autowired
	private AdmMsgCodeDao admMsgCodeDao;

	@Value("${batch.hostfile.path:}")
	private String pathName;

	@Value("${batch.otherfile.path:}")
	private String otherDir;

	@Value("${host.hostdata.path}")
	private String hhdpath;

	@Value("${host.other.path}")
	private String hopath;

	@Autowired
	private SysParamDataDao sysParamDataDao;

	@Autowired
	private SysBatchDao sysBatchDao;

	@RequestMapping(value = "/batch/goldBuyNotify", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {
		List<String> safeArgs = ESAPIUtil.validStrList(args);
		log.info("INTO GoldBuyNotify BATCH ... ");
		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}

		Map<String, Object> data = new HashMap();
		BatchResult batchresult = new BatchResult();
		batchresult.setBatchName(getClass().getSimpleName());
		Map<String, Object> resultData = new LinkedHashMap();

		int totalcnt = 0, failcnt = 0, okcnt = 0, noneedcnt = 0, nousercnt = 0, cantfindmailcnt = 0;
		ADMCUSTTELNO t = new ADMCUSTTELNO();
		String FullFileName = new String();
		String FullFileName_Host = new String();
		InputStreamReader in = null;
		BufferedReader bin = null;
		String subject = new String();
		String custid = new String();
		String telno = new String();
		Date d = new Date();
		String today = DateTimeUtils.getDateShort(d);
		String time = DateTimeUtils.getTimeShort(d);
		Hashtable sendmaildata = new Hashtable();
		String oldcusidno = "FFFFFFFFFF";
		StringBuffer mailrecodbuf = new StringBuffer();

		List<String> errorList = new ArrayList();

		log.debug("DATE = " + today);
		log.debug("TIME = " + time);

		FullFileName = pathName;
		FullFileName += "FKAPRE.TXT";
		// For Error log
		FullFileName_Host = hhdpath;
		FullFileName_Host += "FKAPRE.TXT";

		log.debug("FullFileName = " + FullFileName);

		int dataCount = 0;

		try {
			
			File file= new File(FullFileName);
			//Incorrect Permission Assignment For Critical Resources
			file.setWritable(true,true);
			file.setReadable(true, true);
			file.setExecutable(true,true);
			long timestamp = file.lastModified();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String fmtDate = sdf.format(timestamp);
			log.info("File's LastModify fmt >> {}",fmtDate);
			
			if(!today.equals(fmtDate)) {
				resultData.put("MSG", "FKAPRE.TXT MODIFY TIME NOT TODAY , SKIP EXECUTE !");
				batchresult.setSuccess(true);
				batchresult.setBatchName(this.getClass().getSimpleName());
				batchresult.setData(resultData);
				try {
					// 新增SYSBATCH紀錄
					sysBatchDao.finish_OK(batchName);
				}catch (Exception e2) {
					log.error("sysBatchDao.finish error >> {} ", e2.getMessage());
				}
				return batchresult;
			}
			
			in = new InputStreamReader(new FileInputStream(FullFileName), "MS950");
			bin = new BufferedReader(in);

			if (!bin.ready()) {
				data.put("Error", "檔案為空(" + FullFileName_Host + ")");
				batchresult.setData(data);
				batchresult.setSuccess(false);
				in.close();
				bin.close();
				try {
					sysBatchDao.finish_FAIL2(batchName, batchresult.getData());
				} catch (Exception e2) {
					log.error("sysBatchDao.finish error >> {} ", e2.getMessage());
				}
				return batchresult;
			}

			while ((subject = ESAPIUtil.validInput(bin.readLine(), "GeneralString", true)) != null
					&& subject.length() != 0) {

				dataCount++;
				log.debug("=============== data{} start=====================", dataCount);

				byte[] subjectarray = subject.getBytes("MS950");

				// 身分證號
				byte[] idnoarray = new byte[10];
				System.arraycopy(subjectarray, 0, idnoarray, 0, idnoarray.length);
				String idno = new String(idnoarray).trim();
				log.debug("idno = " + idno);

				List<TXNUSER> listtxnUser;
				listtxnUser = txnUserDao.findByUserId(idno);
				log.debug("listtxnUser.size() = " + listtxnUser.size());
				if (listtxnUser == null || listtxnUser.size() == 0) {
					log.debug("User ID not found = " + idno);
					nousercnt++;
					totalcnt++;
					continue;
				}

				TXNUSER txnuser = listtxnUser.get(0);
				String dpnotify = StrUtils.trim(txnuser.getDPNOTIFY());
				Set<String> dpnotifySet = StrUtils.splitToSet("\\s*,\\s*", dpnotify);

				boolean isUserSetTxnNotify = dpnotifySet.contains("17");
				if (isUserSetTxnNotify == false) {
					log.debug(
							ESAPIUtil.vaildLog("User >> {} , no need to Send this Notify(17)" + txnuser.getDPSUERID()));
					noneedcnt++;
					totalcnt++;
					continue;
				}

				// 黃金存摺帳號
				byte[] acnarray = new byte[11];
				System.arraycopy(subjectarray, idnoarray.length, acnarray, 0, acnarray.length);
				String acn = new String(acnarray).trim();

				// 預約日期
				byte[] appdatearray = new byte[7];
				System.arraycopy(subjectarray, idnoarray.length + acnarray.length, appdatearray, 0,
						appdatearray.length);
				String tmpappdate = new String(appdatearray).trim();

				// d = DateTimeUtils.parse("yyyyMMdd", tmpappdate);
				// String appdate = DateTimeUtils.format("yyyy/MM/dd", d);

				log.debug("appdate = " + tmpappdate);
				// 預約時間
				byte[] apptimearray = new byte[9];
				System.arraycopy(subjectarray, idnoarray.length + acnarray.length + appdatearray.length, apptimearray,
						0, apptimearray.length);

				String apptime = new String(apptimearray).trim();
				log.debug("apptime = " + tmpappdate.substring(0, 3) + "/" + tmpappdate.substring(3, 5) + "/"
						+ tmpappdate.substring(5, 7) + "-" + apptime.substring(0, 2) + ":" + apptime.substring(2, 4)
						+ ":" + apptime.substring(4, 6));
				sendmaildata.put("#TRANTIME",
						tmpappdate.substring(0, 3) + "/" + tmpappdate.substring(3, 5) + "/" + tmpappdate.substring(5, 7)
								+ "-" + apptime.substring(0, 2) + ":" + apptime.substring(2, 4) + ":"
								+ apptime.substring(4, 6));
				// 預約交易種類
				byte[] apptypearray = new byte[2];
				System.arraycopy(subjectarray,
						idnoarray.length + acnarray.length + appdatearray.length + apptimearray.length, apptypearray, 0,
						apptypearray.length);

				String apptype = new String(apptypearray).trim();
				log.debug("apptype = " + apptype);

				// 入扣帳日期
				byte[] trndayarray = new byte[7];
				System.arraycopy(subjectarray, idnoarray.length + acnarray.length + appdatearray.length
						+ apptimearray.length + apptypearray.length, trndayarray, 0, trndayarray.length);

				String tmptrnday = new String(trndayarray).trim();
				log.debug("tmptrnday = " + tmptrnday);
				// d = DateTimeUtils.parse("yyyyMMdd", tmptrnday);
				// String trnday = DateTimeUtils.format("yyyy/MM/dd", d);

				// sendmaildata.put("REMITCY", trnday);

				// 台幣存款帳號
				byte[] svacnarray = new byte[11];
				System.arraycopy(subjectarray, idnoarray.length + acnarray.length + appdatearray.length
						+ apptimearray.length + apptypearray.length + trndayarray.length, svacnarray, 0,
						svacnarray.length);

				String svacn = new String(svacnarray).trim();

				// sendmaildata.put("#OUTACN", StrUtils.left(svacn, svacn.length() - 5) + "***"
				// + StrUtils.right(svacn, 2));

				if (apptype.equals("01")) {
					log.debug("type = 01");
					log.debug("svacn = " + StrUtils.left(svacn, svacn.length() - 5) + "***" + StrUtils.right(svacn, 2));
					sendmaildata.put("#OUTACN",
							StrUtils.left(svacn, svacn.length() - 5) + "***" + StrUtils.right(svacn, 2));
					log.debug("acn = " + StrUtils.left(acn, acn.length() - 5) + "***" + StrUtils.right(acn, 2));
					sendmaildata.put("#ACN", StrUtils.left(acn, acn.length() - 5) + "***" + StrUtils.right(acn, 2));
					sendmaildata.put("#TRANTYPE", "黃金存摺買進");
					sendmaildata.put("GOLDTEMPLATE", "GOLD_SCH_BUY_NOTIFY");
				} else {
					log.debug("type = 02");
					log.debug("svacn = " + StrUtils.left(svacn, svacn.length() - 5) + "***" + StrUtils.right(svacn, 2));
					sendmaildata.put("#OUTACN", StrUtils.left(acn, acn.length() - 5) + "***" + StrUtils.right(acn, 2));
					log.debug("acn = " + StrUtils.left(acn, acn.length() - 5) + "***" + StrUtils.right(acn, 2));
					sendmaildata.put("#ACN",
							StrUtils.left(svacn, svacn.length() - 5) + "***" + StrUtils.right(svacn, 2));
					sendmaildata.put("#TRANTYPE", "黃金存摺回售");
					sendmaildata.put("GOLDTEMPLATE", "GOLD_SCH_SALE_NOTIFY");
				}

				// 黃金公克數
				byte[] gdbalarray = new byte[13];
				System.arraycopy(subjectarray,
						idnoarray.length + acnarray.length + appdatearray.length + apptimearray.length
								+ apptypearray.length + trndayarray.length + svacnarray.length,
						gdbalarray, 0, gdbalarray.length);

				String gdbal = new String(gdbalarray).trim();
				log.debug("gdbal = " + NumericUtil.formatNumberString(gdbal, 2));
				sendmaildata.put("#TRNGD", NumericUtil.formatNumberString(gdbal, 2));

				// 交易結果
				byte[] statusarray = new byte[4];
				System.arraycopy(subjectarray,
						idnoarray.length + acnarray.length + appdatearray.length + apptimearray.length
								+ apptypearray.length + trndayarray.length + svacnarray.length + gdbalarray.length,
						statusarray, 0, statusarray.length);

				String status = new String(statusarray).trim();
				log.debug("status = " + status);

				String statusdesc = "";
				try {
					statusdesc = admMsgCodeDao.getErrorCodeMsg(status);
					if (status.equals("0000"))
						sendmaildata.put("#MSG", "交易成功");
					else
						sendmaildata.put("#MSG", "交易失敗");
				} catch (Exception e) {
					sendmaildata.put("#MSG", "交易失敗");
					statusdesc = "";
				}
				log.debug(ESAPIUtil.vaildLog("statusdesc = " + statusdesc));

				if (!status.equals("0000")) {
					StringBuffer sub_msgdes = new StringBuffer();
					sub_msgdes.append("<TR>\n");
					sub_msgdes.append("<TD width=\"120\">\n");
					sub_msgdes.append("<DIV align=left><FONT>&nbsp;失敗原因</FONT></DIV></TD>\n");
					sub_msgdes.append("<TD width=\"480\">\n");
					if (status.equals("0000"))
						sub_msgdes.append("<DIV align=left><FONT >&nbsp;&nbsp;</FONT></DIV></TD>\n");
					else
						sub_msgdes.append("<DIV align=left><FONT >&nbsp;&nbsp;" + statusdesc + "</FONT></DIV></TD>\n");
					sub_msgdes.append("</TR>\n");
					log.debug(ESAPIUtil.vaildLog("sub_msgdes = " + sub_msgdes.toString()));
					sendmaildata.put("#DESC", sub_msgdes.toString());
				} else {
					sendmaildata.put("#DESC", "");
				}
				// 牌告單價
				byte[] pricearray = new byte[9];
				System.arraycopy(subjectarray,
						idnoarray.length + acnarray.length + appdatearray.length + apptimearray.length
								+ apptypearray.length + trndayarray.length + svacnarray.length + gdbalarray.length
								+ statusarray.length,
						pricearray, 0, pricearray.length);

				String price = new String(pricearray).trim();
				log.debug("price = " + NumericUtil.formatNumberString(price, 2));
				sendmaildata.put("#PRICE", NumericUtil.formatNumberString(price, 2));

				// 折讓後單價
				byte[] dispricearray = new byte[9];
				System.arraycopy(subjectarray,
						idnoarray.length + acnarray.length + appdatearray.length + apptimearray.length
								+ apptypearray.length + trndayarray.length + svacnarray.length + gdbalarray.length
								+ statusarray.length + pricearray.length,
						dispricearray, 0, dispricearray.length);

				String disprice = new String(dispricearray).trim();
				log.debug("disprice = " + NumericUtil.formatNumberString(disprice, 2));
				sendmaildata.put("#DISPRICE", NumericUtil.formatNumberString(disprice, 2));

				// 折讓率
				byte[] perdisarray = new byte[5];
				System.arraycopy(subjectarray,
						idnoarray.length + acnarray.length + appdatearray.length + apptimearray.length
								+ apptypearray.length + trndayarray.length + svacnarray.length + gdbalarray.length
								+ statusarray.length + pricearray.length + dispricearray.length,
						perdisarray, 0, perdisarray.length);

				String perdis = new String(perdisarray).trim();
				log.debug("perdis = " + NumericUtil.formatNumberString(perdis, 2) + " %");
				sendmaildata.put("#PERDIS", NumericUtil.formatNumberString(perdis, 2) + " %");

				// 手續費
				byte[] trnfeearray = new byte[11];
				System.arraycopy(subjectarray,
						idnoarray.length + acnarray.length + appdatearray.length + apptimearray.length
								+ apptypearray.length + trndayarray.length + svacnarray.length + gdbalarray.length
								+ statusarray.length + pricearray.length + dispricearray.length + perdisarray.length,
						trnfeearray, 0, trnfeearray.length);

				String trnfee = new String(trnfeearray).trim();
				log.debug("trnfee = " + NumericUtil.formatNumberString(trnfee, 0));
				sendmaildata.put("#TRNFEE", NumericUtil.formatNumberString(trnfee, 0));

				// 交易金額
				byte[] trnamtarray = new byte[11];
				System.arraycopy(subjectarray,
						idnoarray.length + acnarray.length + appdatearray.length + apptimearray.length
								+ apptypearray.length + trndayarray.length + svacnarray.length + gdbalarray.length
								+ statusarray.length + pricearray.length + dispricearray.length + perdisarray.length
								+ trnfeearray.length,
						trnamtarray, 0, trnamtarray.length);

				String trnamt = new String(trnamtarray).trim();
				log.debug("trnamt = " + NumericUtil.formatNumberString(trnamt, 0));
				sendmaildata.put("#TRNAMT", NumericUtil.formatNumberString(trnamt, 0));

				int rc = chkemail(sendmaildata, idno);
				if (rc == 0) {

					rc = sendmail(sendmaildata);
					if (rc == 1) {
						totalcnt++;
						failcnt++;
						errorList.add("data" + dataCount + ", CUSIDN:" + txnuser.getDPSUERID() + ", Eamil:"
								+ sendmaildata.get("MAILADDR"));
					} else {
						totalcnt++;
						okcnt++;
					}

				} else {
					totalcnt++;
					cantfindmailcnt++;
				}
				double d_TRNGD = Double
						.parseDouble((String) (StringUtil.left(gdbal, 11) + "." + StringUtil.right(gdbal, 2)));
				if (d_TRNGD >= 3000.00 && status.equals("0000")) {
					rc = notifybremp(sendmaildata);
					if (rc != 0) {
						data.put("notifybremp data" + dataCount, "Failed");
					}
				}

				log.debug("=============== data{} end=====================", dataCount);
			}

		} catch (FileNotFoundException e) {
			// throw new MsgTemplateNotFoundException("找不到對應的 template 檔(" + FullFileName +
			// ").", e);
			log.error("找不到 (" + FullFileName_Host + ")檔案," + e.getMessage());
			data.put("Error", "找不到檔案(" + FullFileName_Host + ")");
			batchresult.setData(data);
			batchresult.setSuccess(false);
			try {
				sysBatchDao.finish_FAIL2(batchName, batchresult.getData());
			} catch (Exception e2) {
				log.error("sysBatchDao.finish error >> {} ", e2.getMessage());
			}
			
			return batchresult;
		} catch (IOException e) {
			// throw new ToRuntimeException("讀取 template 檔錯誤(" + FullFileName + ").", e);
			log.error("IOException (" + FullFileName_Host + ")," + e.getMessage());
			data.put("Error", "讀取檔案錯誤(" + FullFileName_Host + ")");
			batchresult.setData(data);
			batchresult.setSuccess(false);
			try {
				sysBatchDao.finish_FAIL2(batchName, batchresult.getData());
			} catch (Exception e2) {
				log.error("sysBatchDao.finish error >> {} ", e2.getMessage());
			}
			
			return batchresult;
		} finally {
			try {
				in.close();
				bin.close();
			} catch (Exception e) {
			}
			in = null;
			bin=null;
		}

		Date nowdate = new Date();
		Calendar c1 = Calendar.getInstance();
		c1.setTime(nowdate);
		c1.add(Calendar.DATE, -1);
		String yesterday = DateTimeUtils.getDateShort(c1.getTime());

		// log.debug("run /TBB/nnb/batch/goldbuynotifybak.sh");
		// RuntimeExec runtimeExec=new
		// RuntimeExec("/TBB/nnb/batch","/TBB/nnb/batch/goldbuynotifybak.sh " +
		// yesterday);

		// if(runtimeExec.getcommstat()!=0)
		// {
		// logger.debug("/TBB/nnb/batch/goldbuynotifybak.sh exec command error");
		// mkdir /backup5y/20090901/email/
		// }

		log.debug("============ Backup Area ============");
		log.debug("DO goldbuynotifybak.sh");
		log.debug("--Check Dir Start--");
		String datadatedir = otherDir + "backup5y/" + yesterday;
		String datadatedirHost = hopath + "backup5y/" + yesterday;
		log.debug("datadatedir >>" + datadatedirHost);
		File file = new File(datadatedir);
		file.setWritable(true, true);
		file.setReadable(true, true);
		file.setExecutable(true, true);
		if (!file.exists()) {
			log.debug("mkdir >>" + datadatedirHost);
			file.mkdir();
		}
		String emailbakdir = datadatedir + "/email";
		String emailbakdirHost = datadatedirHost + "/email";
		log.debug("emailbakdir >>" + emailbakdirHost);
		file = new File(emailbakdir);
		file.setWritable(true, true);
		file.setReadable(true, true);
		file.setExecutable(true, true);
		if (!file.exists()) {
			log.debug("mkdir >>" + emailbakdirHost);
			file.mkdir();
		}
		log.debug("-- Check Dir End --");
		log.debug("-- Move file Start --");

		String fkaprePath = pathName + "FKAPRE.TXT";

		String backDirShow = emailbakdirHost + "/";

		File file1 = new File(fkaprePath);
		file1.setWritable(true, true);
		file1.setReadable(true, true);
		file1.setExecutable(true, true);
		File file2 = new File(emailbakdir + "/FKAPRE.TXT");
		file2.setWritable(true, true);
		file2.setReadable(true, true);
		file2.setExecutable(true, true);
		// 檔案若存在則移除
		if (file1.exists()) {
			file2.delete();
			try {
				FileUtils.copyFile(file1, file2);
				log.debug("move {} to {} OK", "FKAPRE.TXT", backDirShow);
				data.put("BackupStatus", "Success , move FKAPRE.TXT to " + backDirShow);
				file1.delete();
			} catch (IOException e) {
				log.error("move file error");
				data.put("BackupStatus", "Fail");
			}
		}

		log.debug("-- Move file End --");
		log.debug("============ Backup Area End ============");

		resultData.put("Send_Sucess", okcnt);
		resultData.put("Send_Fail", failcnt);
		resultData.put("No_Need_Send", noneedcnt);
		resultData.put("User_No_Email ", cantfindmailcnt);
		resultData.put("Cant_find_User", nousercnt);
		resultData.put("Total_Data", totalcnt);
		data.put("COUNTER", resultData);

		if (errorList.size() != 0) {
			data.put("ErrorDataList", errorList);
		}
		batchresult.setSuccess(true);
		batchresult.setData(data);
		try {
			sysBatchDao.finish_OK(batchName);
		}catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}
		
		
		return batchresult;

	}

	public int sendmail(Hashtable maildata) {
		log.debug("<<<<< sendmail >>>>>");
		final StringBuffer status = new StringBuffer();
		NotifyListener listener = new NotifyListener() {

			public void fail(String reciver, String subject, String msg, Exception e) {
				status.append("Failed");
			}

			public void success(String reciver, String subject, String msg) {
				status.append("Success");
			}
		};

		NotifyAngent.sendNotice((String) maildata.get("GOLDTEMPLATE"), maildata, (String) maildata.get("MAILADDR"),
				listener);

		if ("Success".equals(status.toString())) {
			log.debug(ESAPIUtil.vaildLog("發送 Email 成功.(" + maildata.get("MAILADDR") + ")"));
			return (0);
		} else {
			log.debug(ESAPIUtil.vaildLog("發送 Email 失敗.(" + maildata.get("MAILADDR") + ")"));
			return (1);
		}
	}

	public int chkemail(Hashtable email, String cusidn) {
		log.debug("<<<<< chkemail >>>>>");
		String mailaddr = null;
		TXNUSER user = null;
		List<TXNUSER> listtxnUser;
		try {
			log.debug("where cusidn = " + cusidn);
			listtxnUser = txnUserDao.findByUserId(cusidn);
			user = listtxnUser.get(0);
			mailaddr = user.getDPMYEMAIL().trim();
		} catch (Exception e) {
			log.error("無法取得 USER ID (" + cusidn + ")," + e.getMessage());
			return (1);
		}

		if (mailaddr.length() == 0 || mailaddr == null) {
			log.error("***** Mail is't exist.(USER ID : " + cusidn + ") ");
			return (1);
		}

		log.debug(ESAPIUtil.vaildLog("mailaddr = " + mailaddr));
		email.put("MAILADDR", mailaddr);
		return (0);

	}

	public int notifybremp(Hashtable maildata) {
		log.debug("<<<<< notifybremp >>>>>");

		boolean isExists = false;
		SYSPARAMDATA po = null;
		try {
			po = sysParamDataDao.findById("NBSYS");
			isExists = true;
		} catch (ObjectNotFoundException e) {
			isExists = false;
		}

		if (!isExists) {
			po = new SYSPARAMDATA();
			po.setADPK("NBSYS");
		}

		String str_Receiver = po.getADGDTXNMAIL().trim();

		//
		if (null == str_Receiver || str_Receiver.length() == 0) {
			log.debug("***** ADGDTXNMAIL is Empty");
			return (1);
		}
		final StringBuffer status = new StringBuffer();
		NotifyListener listener = new NotifyListener() {

			public void fail(String reciver, String subject, String msg, Exception e) {
				status.append("Failed");
			}

			public void success(String reciver, String subject, String msg) {
				status.append("Success");
			}
		};

		NotifyAngent.sendNotice("GOLD_NOTIFY_3KG", maildata, str_Receiver, listener);

		if ("Success".equals(status.toString())) {
			log.debug(ESAPIUtil.vaildLog("發送 Email 成功.(" + maildata.get("MAILADDR") + ")"));
			return (0);
		} else {
			log.debug(ESAPIUtil.vaildLog("發送 Email 失敗.(" + maildata.get("MAILADDR") + ")"));
			return (1);
		}
	}
}
