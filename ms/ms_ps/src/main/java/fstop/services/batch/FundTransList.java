package fstop.services.batch;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.orm.dao.Nb3SysOpDao;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.TxnFundDataDao;
import fstop.orm.dao.TxnLogDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.oao.Old_TxnLogDao;
import fstop.orm.oao.Old_TxnUserDao;
import fstop.orm.old.OLD_TXNLOG;
import fstop.orm.old.OLD_TXNUSER;
import fstop.orm.po.NB3SYSOP;
import fstop.orm.po.TXNFUNDDATA;
import fstop.orm.po.TXNLOG;
import fstop.orm.po.TXNUSER;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 每日產生昨日行內基金交易明細資料清單至 /TBB/nnb/hostdata/FUNDTRNLIST
 * @author Owner
 *
 */
@Slf4j
@RestController
@Batch(id="batch.FundTransList", name = "讀txnlog 申購轉換贖回，產生檔案，中心讀取", description = "讀txnlog 申購轉換贖回，產生檔案，中心讀取")
public class FundTransList  implements BatchExecute {

	@Autowired
	private TxnLogDao txnLogDao;
	@Autowired
	private TxnUserDao txnUserDao;
//	@Autowired
//	private SysOpDao SysOpDao;
	
	@Autowired
	private Old_TxnLogDao old_TxnLogDao;
	@Autowired
	private Old_TxnUserDao old_TxnUserDao;
	
	@Autowired
	private Nb3SysOpDao nb3SysOpDao;
	
	@Autowired
	private TxnFundDataDao txnFundDataDao;
	
	@Value("${isParallelCheck}")
	private String isParallelCheck;
	
	@Value("${doFTP}")
	private String doFTP;

	@Value("${batch.hostfile.path:}")
	private String hostDir;

	@Autowired
	private SysBatchDao sysBatchDao;

	@RequestMapping(value = "/batch/fundTransList", method = {RequestMethod.POST},
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {
		List<String> safeArgs = ESAPIUtil.validStrList(args);
		log.info("批次 行內基金交易明細資料  ... ");
		log.info("fundTransList 參數 : {}",safeArgs);

		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}

		int failcnt=0,okcnt=0,totalcnt=0;
		Hashtable rowcnt=new Hashtable();
		
		BatchResult result = new BatchResult();
		result.setBatchName(this.getClass().getSimpleName());
		Map<String, Object> data = new HashMap();
		String str_StartDate = "";
		String str_EndDate = "";
		String DATEDATA = "";
		
		
		try {
//			exec(rowcnt);
			//期間
			if (safeArgs.size() == 2) {
				str_StartDate = safeArgs.get(0);
				str_EndDate = safeArgs.get(1);
				DATEDATA = str_StartDate + str_EndDate;
				exec(rowcnt, DATEDATA);
			//特定日
			} else if (safeArgs.size() == 1) {
				DATEDATA = safeArgs.get(0);
				exec(rowcnt, DATEDATA);
			//昨日
			}else {
				exec(rowcnt, DATEDATA);
			}
			failcnt=((Integer)rowcnt.get("FAILCNT")).intValue();
			okcnt=((Integer)rowcnt.get("OKCNT")).intValue();
			totalcnt=((Integer)rowcnt.get("TOTALCNT")).intValue();
			log.debug(ESAPIUtil.vaildLog("failcnt = " + failcnt));
			log.debug(ESAPIUtil.vaildLog("okcnt = " + okcnt));
			log.debug(ESAPIUtil.vaildLog("totalcnt = " + totalcnt));
			
			BatchCounter bcnt=new BatchCounter();
			bcnt.setFailCount(failcnt);
			bcnt.setSuccessCount(okcnt);
			bcnt.setTotalCount(totalcnt);

			data.put("COUNTER", bcnt);

			result.setSuccess(true);
			result.setData(data);
		}
		catch(Exception e) {
			log.error("執行 Batch 有誤 !", e);
			data.put("ERROR", e.getMessage());
			result.setSuccess(false);
			result.setData(data);
		}
		
		try {
			if (result.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName,result.getData());
			}
		}catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}
		return result;
	}

	private void exec(Hashtable rowcnt,String DATEDATA) throws IOException {
		Integer	totalcnt=0,failcnt=0,okcnt=0;
		Date d = new Date();
		java.util.Calendar calendar = java.util.Calendar.getInstance();   
		calendar.setTime(d); 
		calendar.add(calendar.DAY_OF_MONTH,-1);
		d = calendar.getTime(); 
		String lastdate= DateTimeUtils.getDateShort(d);
		if (DATEDATA.length() == 0) {
			//昨日
			lastdate = lastdate.substring(0, 8);
		}else {
			//特定日(20211205) or 區間 (2020010120201231)
			lastdate = DATEDATA;
		}
		
		log.info(ESAPIUtil.vaildLog("執行日期 = " + lastdate));
		String despath=hostDir+"FUNDTRNLIST";
		log.debug(ESAPIUtil.vaildLog("despath = " + despath));

		File tmpFile = new File(despath);
		tmpFile.setWritable(true,true);
		tmpFile.setReadable(true, true);
		tmpFile.setExecutable(true,true);
		tmpFile.delete();
		OutputStream fos = new FileOutputStream(tmpFile);
		BufferedWriter out = new BufferedWriter(new OutputStreamWriter(fos, "BIG5"));
		
		List<TXNLOG> all = txnLogDao.findByTxnlogTime(lastdate);
		
		if("Y".equals(isParallelCheck)) {
			List<OLD_TXNLOG> oldall = old_TxnLogDao.findByTxnlogTime(lastdate);
			
			log.debug(ESAPIUtil.vaildLog("NB 3 data size >> " + all.size()));
			log.debug(ESAPIUtil.vaildLog("NB 2.5 data size >> " + oldall.size()));
			
			for(OLD_TXNLOG oldeach:oldall) {
				TXNLOG nnbConvert2nb3 = CodeUtil.objectCovert(TXNLOG.class, oldeach);
				all.add(nnbConvert2nb3);
			}
		}
		
		TXNLOG ft;
				
		for (int i=0; i < all.size(); i++) {
			ft = all.get(i);
			List<TXNUSER> l_User = txnUserDao.findByUserId(ft.getADUSERID());
			if("Y".equals(isParallelCheck)&&l_User.size()==0) {
				List<OLD_TXNUSER> old_l_User = old_TxnUserDao.findByUserId(ft.getADUSERID());
				for(OLD_TXNUSER user :old_l_User) {
					log.trace(ESAPIUtil.vaildLog("Get NB2.5 User >> "+ user.getDPSUERID()));
					TXNUSER nnbConvert2nb3 = CodeUtil.objectCovert(TXNUSER.class, user);
					l_User.add(nnbConvert2nb3);
				}
			}
			
			if(l_User.size()==0 || null==l_User) {
				log.error("TXNUSER 找不到此用戶 : {} , 跳過此筆資料" ,  ft.getADUSERID());
				failcnt++;
				continue;
			}
			
			TXNUSER u = l_User.get(0);
			String adsvbh="";
			if(ft.getADSVBH().equals(""))
				adsvbh=u.getADBRANCHID();
			else
				adsvbh=ft.getADSVBH();
			String userip=ft.getADUSERIP();
			String LASTDATE=ft.getLASTDATE();
            String LASTTIME=ft.getLASTTIME();           
            String username=u.getDPUSERNAME();
            String userid=ft.getADUSERID();
            Map map = JSONUtils.json2map(ft.getADCONTENT());
            String adopid=ft.getADOPID();            
            String creditno="";
            if(adopid.equals("C016") || adopid.equals("C017") || adopid.equals("C031"))
            	creditno="";
            else {
            	creditno=(String)map.get("CREDITNO");
            	if(StrUtils.isEmpty(creditno)) {
            		creditno=(String)map.get("CDNO");
            		if(StrUtils.isEmpty(creditno)) {
            			creditno="";
            		}
            	}
            }
            List<NB3SYSOP> l_sysop = nb3SysOpDao.findByADOPId(adopid);
            
            if(l_sysop.size()==0 || null==l_sysop) {
				log.error("NB3SYSOP 找不到此代號 : {} , 跳過此筆資料" ,  adopid);
				failcnt++;
				continue;
				
			}
            
            NB3SYSOP sysop = l_sysop.get(0);            
            String typename=sysop.getADOPNAME();
			String txamt=ft.getADTXAMT();
			String transcode=(String)map.get("TRANSCODE");
			String fundlname=(String)map.get("FUNDLNAME");
            if(StrUtils.isEmpty(fundlname))
            {
            	List<TXNFUNDDATA> fdata = txnFundDataDao.findByFUNDLNAME(transcode);
            	if(fdata.size()>0) {
            		TXNFUNDDATA fd = fdata.get(0);
            		fundlname = fd.getFUNDLNAME();
            	}else {
            		fundlname="";
            	}
            }
				
			String adcurrency=ft.getADCURRENCY().trim();
			String alerttype="";
			String alerttype1="";
			String alerttype2="";
			String alerttype3="";
			if(adopid.equals("C112"))
			{
				adopid="C111";
				alerttype1 = ((String)map.get("ALTERTYPE1") == null) ?  "" : (String)map.get("ALTERTYPE1");	
				alerttype2 = ((String)map.get("ALTERTYPE2") == null) ?  "" : (String)map.get("ALTERTYPE2");
				alerttype3 = ((String)map.get("ALTERTYPE3") == null) ?  "" : (String)map.get("ALTERTYPE3");

				if(alerttype1.equals("1"))
					alerttype="扣款金額";
				else if(alerttype2.equals("2"))
					alerttype="扣款日期";
				else if(alerttype3.equals("3"))
					alerttype="扣款狀態";	
				
				txamt="";
				transcode="";
			}
			else
			{	
			  alerttype="";
			}
			String intranscode="";
			String infundlname="";
			if(adopid.equals("C021") || adopid.equals("C032"))
			{
				intranscode=(String)map.get("INTRANSCODE");
	            List<TXNFUNDDATA> l_txnfunddata = txnFundDataDao.findByFUNDLNAME(intranscode);
	            if(l_txnfunddata.size()>0) {
	            	TXNFUNDDATA txnfunddata = l_txnfunddata.get(0);            
		            infundlname=txnfunddata.getFUNDLNAME();			
            	}else {
            		infundlname="";
            	}
			}
			String f = adsvbh+","+userip+","+LASTDATE+","+LASTTIME+","+username+","+userid+","+creditno+","+adopid+","+typename+","+txamt+","+transcode+","+fundlname+","+adcurrency+","+alerttype+","+intranscode+","+infundlname+"\r\n";
			log.debug(ESAPIUtil.vaildLog("Export File Content = " + f));
			out.write(f);
			okcnt++;
			totalcnt++;
		}

		try {
			out.flush();
			out.close();
		}
		catch(Exception e){}
		try {
			fos.close();
		}
		catch(Exception e){}

		if(okcnt==0)
		{
			rowcnt.put("OKCNT",okcnt);
			rowcnt.put("FAILCNT",failcnt);
			rowcnt.put("TOTALCNT",totalcnt);
			return;
		}

		rowcnt.put("OKCNT",okcnt);
		rowcnt.put("FAILCNT",failcnt);
		rowcnt.put("TOTALCNT",totalcnt);
		
	}
	
}
