package fstop.services.batch;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.orm.dao.TxnTrAccSetDao;
import fstop.orm.po.TXNTRACCSET;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class DecodeTxnTrAccSet {

	@Autowired
	TxnTrAccSetDao txnTrAccSetDao;
	
	@Value("${batch.localfile.path:}")
	private String data_path;

	/**
	 * args[0] method name
	 */
	@RequestMapping(value = "/batch/decodetxntraccset", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {
		log.info("batch decodeTxnTrAccSet...");
		
		List<String> safeArgs = ESAPIUtil.validStrList(args);
		
		BatchResult batchresult = new BatchResult();
		batchresult.setBatchName(getClass().getSimpleName());
		Map<String, Object> data = new LinkedHashMap();
		StringBuffer restore_data = new StringBuffer();
		OutputStreamWriter out = null;
		int s_cnt = 0;
		int f_cnt = 0;
		int sql_cnt = 0;
		String orgDPGONAME = "";
		
		
		List<TXNTRACCSET> list = txnTrAccSetDao.findByTime(args.get(0), args.get(1));
		log.debug("data >> {}", CodeUtil.toJson(list));
		
		sql_cnt = list.size();
		log.info("SQL SIZE = {} " , sql_cnt);
		
		//修改的檔案落地 , 備份成SQL檔
		restore_data.append("connect to DBPNB3;\n");
		restore_data.append("set schema = NB3RUNP;\n");
		try {
			for (TXNTRACCSET eachData : list) {
				//先把舊的DPGONAME資料存起來
				orgDPGONAME = eachData.getDPGONAME();
				eachData.setDPGONAME(StringEscapeUtils.unescapeHtml(eachData.getDPGONAME()));
				log.info("ready to update TXNTRACCSET.DPACCSETID : {}", eachData.getDPACCSETID());
				try {
					txnTrAccSetDao.update(eachData);
					// 更新成功的資料寫入落地檔
					restore_data.append("UPDATE TXNTRACCSET SET DPGONAME = '");
					restore_data.append(orgDPGONAME);
					restore_data.append("' WHERE DPACCSETID = '");
					restore_data.append(eachData.getDPACCSETID());
					restore_data.append("'; \n");
					s_cnt ++;
				} catch (Exception e) {
					log.error("TXNTRACCSET.DPACCSETID : {} , update error : {}", eachData.getDPACCSETID(), e);
					f_cnt ++;
				}
			}
			
			if(s_cnt >0) {
				try {
					restore_data.append("connect reset;");
					out = new OutputStreamWriter(new FileOutputStream(data_path + "/DecodeTxnTrAccSet_restore.sql"), "UTF-8");
					out.write(restore_data.toString());
				} catch (UnsupportedEncodingException e) {
					log.error("UnsupportedEncodingException : {}" , e);
				} catch (FileNotFoundException e) {
					log.error("FileNotFoundException : {}" , e);
				} catch (IOException e) {
					log.error("IOException : {}" , e);
				}finally {
					try {
						out.flush();
						out.close();
					} catch (IOException e) {
						log.error("flush or close error IOException : {}" , e);
					}
						
				}
				data.put("S_CNT", s_cnt);
				data.put("F_CNT", f_cnt);
				data.put("SQL_CNT", sql_cnt);
			}else {
				log.info("No Data to write");
				data.put("RESULT", "No Data to write");
			}
			
			//清空 restore_data
			restore_data.delete(0, restore_data.length());
			
			
			batchresult.setSuccess(true);		
			batchresult.setData(data);
			
		}catch (Exception e) {
			log.error("unknown error >> {}" , e);
			batchresult.setSuccess(false);
			data.put("ERROR", e.getMessage());
			batchresult.setData(data);
		}
		
		return batchresult;

	}

}
