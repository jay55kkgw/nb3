package fstop.services.batch;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.ESAPIUtil;

import fstop.notifier.NotifyAngent;
import fstop.orm.dao.RevolvingCreditApplyDao;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.SysParamDao;
import fstop.orm.dao.TxnCardApplyDao;
import fstop.orm.dao.TxnLogDao;
import fstop.orm.oao.Old_RevolvingCreditApplyDao;
import fstop.orm.oao.Old_TxnCardApplyDao;
import fstop.orm.oao.Old_TxnLogDao;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 來電分期約款簽署明細檔
 *
 * @author Owner
 */
@Slf4j
@RestController
@Batch(id = "batch.bank3Reort", name = "來電分期約款簽署明細檔", description = "產生來電分期約款簽署明細檔")
public class Bank3Report implements BatchExecute {

	@Autowired
	private TxnLogDao txnLogDao;

	@Autowired
	private TxnCardApplyDao txnCardApplyDao;

	@Autowired
	private RevolvingCreditApplyDao revolvingCreditApplyDao;

	@Autowired
	private SysParamDao sysParamDao;

	@Value("${isParallelCheck}")
	private String isParallelCheck;

	@Autowired
	private Old_TxnLogDao old_TxnLogDao;

	@Autowired
	private Old_RevolvingCreditApplyDao old_RevolvingCreditApplyDao;

	@Autowired
	private Old_TxnCardApplyDao old_TxnCardApplyDao;
	
	@Autowired
	private SysBatchDao sysBatchDao;

	@RequestMapping(value = "/batch/bank3Reort", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {
		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			
			sysBatchDao.initSysBatchPo(batchName);
			
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}
		
		List<String> safeArgs = ESAPIUtil.validStrList(args);
		boolean result = true;
		Map<String, Object> varmail = new HashMap();
		Map<String, Object> data = new HashMap();
		BatchResult batchresult = new BatchResult();
		
		try {
			Bank3ReportObj bank3ReportObj = new Bank3ReportObj();

			bank3ReportObj.setSDATE(DateTimeUtils.getPrevMonthFirstDay());
			bank3ReportObj.setEDATE(DateTimeUtils.getPrevMonthLastDay());
			bank3ReportObj.setN814_All(txnLogDao.getAdopidCount("N814", false));
			bank3ReportObj.setN814_Success(txnLogDao.getAdopidCount("N814", true));
			bank3ReportObj.setN8141_All(txnLogDao.getAdopidCount("N8141", false));
			bank3ReportObj.setN8141_Success(txnLogDao.getAdopidCount("N8141", true));
			if ("Y".equals(isParallelCheck)) {
				bank3ReportObj.setN814_All(
						txnLogDao.getAdopidCount("N814", false) + old_TxnLogDao.getAdopidCount("N814", false));
				log.debug("NB2.5 814_All count >> {} ",old_TxnLogDao.getAdopidCount("N814", false));
				log.debug("NB3 814_All count >> {} ",txnLogDao.getAdopidCount("N814", false));
				bank3ReportObj.setN814_Success(
						txnLogDao.getAdopidCount("N814", true) + old_TxnLogDao.getAdopidCount("N814", true));
				log.debug("NB2.5 N814_Success count >> {} ",old_TxnLogDao.getAdopidCount("N814", true));
				log.debug("NB3 N814_Success count >> {} ",txnLogDao.getAdopidCount("N814", true));
				bank3ReportObj.setN8141_All(
						txnLogDao.getAdopidCount("N8141", false) + old_TxnLogDao.getAdopidCount("N8141", false));
				log.debug("NB2.5 N8141_All count >> {} ",old_TxnLogDao.getAdopidCount("N8141", false));
				log.debug("NB3 N8141_All count >> {} ",txnLogDao.getAdopidCount("N8141", false));
				bank3ReportObj.setN8141_Success(
						txnLogDao.getAdopidCount("N8141", true) + old_TxnLogDao.getAdopidCount("N8141", true));
				log.debug("NB2.5 N8141_Success count >> {} ",old_TxnLogDao.getAdopidCount("N8141", true));
				log.debug("NB3 N8141_Success count >> {} ",txnLogDao.getAdopidCount("N8141", true));

			}

			String emailap = sysParamDao.getSysParam("ADBANK3MAIL").replace(";", ",");
			if (emailap.length() == 0 || "".equals(emailap)) {
				log.debug("no find ADBANK3MAIL !!!!");
			}

			log.debug("Bank3Report emailap===>" + emailap);
			log.debug("Bank3Report SDATE===>" + bank3ReportObj.getSDATE());
			log.debug("Bank3Report EDATE===>" + bank3ReportObj.getEDATE());
			log.debug("Bank3Report N814_All===>" + String.valueOf(bank3ReportObj.getN814_All()));
			log.debug("Bank3Report N814_Success===>" + String.valueOf(bank3ReportObj.getN814_Success()));
			log.debug("Bank3Report N8141_All===>" + String.valueOf(bank3ReportObj.getN8141_All()));
			log.debug("Bank3Report N8141_Success===>" + String.valueOf(bank3ReportObj.getN8141_Success()));

			varmail.put("SDATE", bank3ReportObj.getSDATE());
			varmail.put("EDATE", bank3ReportObj.getEDATE());
			varmail.put("N814_All", String.valueOf(bank3ReportObj.getN814_All()));
			varmail.put("N814_Success", String.valueOf(bank3ReportObj.getN814_Success()));
			varmail.put("N8141_All", String.valueOf(bank3ReportObj.getN8141_All()));
			varmail.put("N8141_Success", String.valueOf(bank3ReportObj.getN8141_Success()));

			varmail.put("CK01_All", String.valueOf(txnLogDao.getAdopidCount("CK01", false)));
			varmail.put("CK01_Success", String.valueOf(revolvingCreditApplyDao.getTxnCount()));
			varmail.put("NA02_All", String.valueOf(txnCardApplyDao.getTxnCount(false)));
			varmail.put("NA02_Success", String.valueOf(txnCardApplyDao.getTxnCount(true)));
			if ("Y".equals(isParallelCheck)) {
				varmail.put("CK01_All", String.valueOf(
						txnLogDao.getAdopidCount("CK01", false) + old_TxnLogDao.getAdopidCount("CK01", false)));
				log.debug("NB2.5 CK01 count >> {} ",old_TxnLogDao.getAdopidCount("CK01", false));
				log.debug("NB3 CK01 count >> {} ",txnLogDao.getAdopidCount("CK01", false));
				varmail.put("CK01_Success", String
						.valueOf(revolvingCreditApplyDao.getTxnCount() + old_RevolvingCreditApplyDao.getTxnCount()));
				log.debug("NB2.5 CK01_Success count >> {} ",old_RevolvingCreditApplyDao.getTxnCount());
				log.debug("NB3 CK01_Success count >> {} ",revolvingCreditApplyDao.getTxnCount());
				varmail.put("NA02_All",
						String.valueOf(txnCardApplyDao.getTxnCount(false) + old_TxnCardApplyDao.getTxnCount(false)));
				log.debug("NB2.5 NA02_All count >> {} ",old_TxnCardApplyDao.getTxnCount(false));
				log.debug("NB3 NA02_All count >> {} ",txnCardApplyDao.getTxnCount(false));
				varmail.put("NA02_Success",
						String.valueOf(txnCardApplyDao.getTxnCount(true) + old_TxnCardApplyDao.getTxnCount(true)));
				log.debug("NB2.5 NA02_Success count >> {} ",old_TxnCardApplyDao.getTxnCount(true));
				log.debug("NB3 NA02_Success count >> {} ",txnCardApplyDao.getTxnCount(true));
			}

			boolean isSendSuccess = NotifyAngent.sendNotice("Bank3Report", varmail, emailap, "batch");
			if (!isSendSuccess) {
				log.debug("發送 Email 失敗.({})", emailap);
				result = false;
				data.put("ERRORMSG", "發送 Email 失敗 :" + emailap );
			}
			
			batchresult.setData(varmail);
		} catch (Exception e) {
			log.debug("Exception : {} " , e.getMessage() );
			result = false;
			data.put("ERRORMSG", e.getMessage() );
		}

		batchresult.setBatchName(this.getClass().getSimpleName());
		batchresult.setSuccess(result);
		
		try {
			if (batchresult.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName,data);
			}
		}catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}
		
		return batchresult;

	}
}
