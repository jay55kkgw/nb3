package fstop.services.batch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
//import org.jdom.Document;
//import org.jdom.Element;
//import org.jdom.output.Format;
//import org.jdom.output.XMLOutputter;
//import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.exception.UncheckedException;
import fstop.model.EachRowCallback;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.model.Rows;
import fstop.model.TelcommResult;
import fstop.orm.dao.AdmCurrencyDao;
import fstop.orm.dao.AdmHolidayDao;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.po.ADMCURRENCY;
import fstop.orm.po.ADMHOLIDAY;
import fstop.services.AfterSuccessQuery;
import fstop.services.batch.annotation.Batch;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.FtpTemplate;
import fstop.util.JSONUtils;
import fstop.util.PathSetting;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 匯率擷取
 * 
 * @author Owner
 * 
 */
@Slf4j
@RestController
@Batch(id="batch.xmxr", name = "匯率擷取", description = "匯率擷取")
public class XMXR extends DispatchBatchExecute  implements BatchExecute {
	//private Logger logger = Logger.getLogger("fstop_txntw");

	private DecimalFormat fmt = new DecimalFormat("#0");
	
	@Autowired
    @Qualifier("xmxrTelcomm")
	private TelCommExec xmxrTelcomm;
	
	@Autowired
	private AdmCurrencyDao admCurrencyDao;
	@Autowired
	private AdmHolidayDao admholidayDao;
	
	@Autowired
	private SysBatchDao sysBatchDao;
	
	public XMXR(){}
	
	/**
	 * args[0] method name
	 */
	@RequestMapping(value = "/batch/xmxr", method = {RequestMethod.POST},
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @Override
	public BatchResult execute(@RequestBody List<String> args) {
		log.info("batch XMXR...");
		List<String> safeArgs = ESAPIUtil.validStrList(args);
		
		BatchResult batchresult = new BatchResult();
		Map<String, Object> data = new HashMap();
		batchresult.setBatchName(getClass().getSimpleName());
		batchresult.setSuccess(true);

		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}

		Date d = new Date();
		String today = DateTimeUtils.getDateShort(d);
//		String time = DateTimeUtils.getTimeShort(d);
		
		ADMHOLIDAY holiday = null;
		try {
			log.debug("DATE = " + today);
			holiday = (ADMHOLIDAY)admholidayDao.findById(today);
		}
		catch (Exception e) {
			holiday = null;					
		}

		if(holiday!=null)
		{

			data.put("Error", "非營業日");
			log.debug("Error >> 非營業日");
			batchresult.setSuccess(false);
			batchresult.setData(data);
			try {
				sysBatchDao.finish_FAIL2(batchName, batchresult.getData());
			} catch (Exception e) {
				log.error("sysBatchDao.finish_FAIL2 error >> {} ", e.getMessage());
			}

			return batchresult;
		}
		Hashtable params = new Hashtable();
//		params.put("DATE", today);
//		params.put("TIME", time);
//		System.out.println("DATE = " + today);
//		System.out.println("TIME = " + time);
		
		try {
		SimpleTemplate simpleTemplate = new SimpleTemplate(xmxrTelcomm);
		
		simpleTemplate.setAfterSuccessQuery(new AfterSuccessQuery() {

			public void execute(MVHImpl result) {
				if(!(result.getValueByFieldName("TOPMSG").equals("") || result.getValueByFieldName("TOPMSG").equals("0000")))
				{
					log.error("TOPMSG = " + result.getValueByFieldName("TOPMSG"));
					return;
				}
				admCurrencyDao.deleteAll();
				Date d = new Date();
				String today = DateTimeUtils.getDateShort(d);
				String time = DateTimeUtils.getTimeShort(d);
				ADMCURRENCY t = new ADMCURRENCY();
				
				t.setADCCYNO("00"); // ADCCYNO
				t.setADCURRENCY("TWD"); // ADCURRENCY
				t.setADCCYNAME("新臺幣");
				t.setADCCYENGNAME("New Taiwan Dollar");
				t.setADCCYCHSNAME("新台币");
				t.setBUY01("0000100000000");
				t.setSEL01("0000100000000");
				t.setBOOKRATE("0000100000000");
				t.setMSGCODE("    ");
				t.setLASTDATE(today);
				t.setLASTTIME(time);
				t.setLASTUSER("BATCH");
				admCurrencyDao.saveOrUpdate(t);
			}
			
		});

		simpleTemplate.setEachRowCallback(new EachRowCallback() {

			public void current(Row row) {
				Date d = new Date();
				String today = DateTimeUtils.getDateShort(d);
				String time = DateTimeUtils.getTimeShort(d);
				String crynameCHT=new String();
				String crynameENG=new String();
				String crynameCHS=new String();
				
				ADMCURRENCY t = new ADMCURRENCY();
				
				crynameCHT=getCurrencyName(1,row.getValue("CRYNAME"));
				crynameENG=getCurrencyName(2,row.getValue("CRYNAME"));
				crynameCHS=getCurrencyName(3,row.getValue("CRYNAME"));
				
				t.setADCCYNO(row.getValue("CRYNO")); // ADCCYNO
				t.setADCURRENCY(row.getValue("CRYNAME")); // ADCURRENCY
				t.setADCCYNAME(crynameCHT);
				t.setADCCYENGNAME(crynameENG);
				t.setADCCYCHSNAME(crynameCHS);
				
				log.debug("CRYNAME = " + row.getValue("CRYNAME") + "\nBUY01 = " + row.getValue("BUY01") + "\nSEL01 = " + row.getValue("SEL01"));
				
				if(row.getValue("BUY01").equals("0000000000000") || row.getValue("SEL01").equals("0000000000000"))
				{
					log.debug("BUY01=\"0000000000000\" || SEL01=\"0000000000000\"");
					return;
				}
				
				t.setBUY01(row.getValue("BUY01"));
				t.setSEL01(row.getValue("SEL01"));
				t.setBOOKRATE(row.getValue("BOOKRATE"));
				t.setMSGCODE("    ");
				t.setLASTDATE(today);
				t.setLASTTIME(time);
				t.setLASTUSER("BATCH");
				admCurrencyDao.saveOrUpdate(t);
				
			}
		});
		
		simpleTemplate.setExceptionHandler(new ExceptionHandler() {

			public void handle(Row row, Exception e) {
				
				log.error("無法更新幣別資料.", e);
				
			}
			
		});
		BatchCounter counter = simpleTemplate.execute(params);
		} catch (TopMessageException e) {
			batchresult.setSuccess(false);
			data.put("TOPMSG", e.getMsgcode() + "  " + e.getMsgout());
			log.error("TopMessageException  !!", e);
		} catch (UncheckedException e) {
			batchresult.setSuccess(false);
			data.put("Error", e.getMessage());
			log.error("Exception 執行有誤 !!", e);
		}

		try {
			if (batchresult.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName,batchresult.getData());
			}
		}catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}
		
		return batchresult;

	}
	
	//type >> 1 = CHT , 2 = ENG , 3 = CHS
	String getCurrencyName(int type ,String Currency)
	{
		String [][] currarry={
				{"USD","美金","US Dollar","美金"},
				{"TWD","新臺幣","New Taiwan Dollar","新台币"},
				{"AUD","澳幣","Australian Dollar","澳币"},
				{"ATS","澳地利幣","",""},//NO
				{"BEF","比利時法朗","",""},//NO
				{"CAD","加拿大幣","Canadian Dollar","加拿大币"},
				{"DEM","德國馬克","",""},//NO
				{"FRF","法國法朗","",""},//NO
				{"HKD","港幣","Hong Kong Dollar","港币"},
				{"NLG","荷蘭幣","",""},//NO
				{"GBP","英磅","British Pound","英磅"},
				{"SGD","新加坡幣","Singapore Dollar","新加坡币"},
				{"ZAR","南非幣","South African Rand","南非币"},
				{"SEK","瑞典幣","Swedish Krona","瑞典币"},
				{"CHF","瑞士法郎","Swiss Franc","瑞士法郎"},
				{"JPY","日圓","Japanese Yen","日圆"},
				{"ITL","義大利里拉","",""},//NO
				{"MYR","馬幣","Malaysian Ringgit","马币"},
				{"THB","泰幣","Thai Baht","泰币"},
				{"EUR","歐洲通貨-歐元","Euro","欧洲通货-欧元"},
				{"NZD","紐幣","New Zealand Dollar","纽币"},
				{"ESP","西班牙幣","",""},//NO
				{"IDR","印尼盾","Rupiah","印尼盾"},//NO
				{"KRW","韓元","won","韩元"},//NO
				{"PHP","菲律賓披索","peso","菲律宾披索"},//NO
				{"CNY","人民幣","Chinese Yuan Renminbi","人民币"},
				{"INR","印度盧比","",""},//NO
				{"RUB","俄國盧布","",""},//NO
				{"BRL","巴西幣","",""},//NO
				{"VND","越南盾","",""}};//NO
		String currname=new String("");
		for(int i=0;i<currarry.length;i++)
		{
			if(Currency.equals(currarry[i][0]))
			{
				currname=currarry[i][type];
				return(currname);
			}
		}
		return(currname);
	}
}
