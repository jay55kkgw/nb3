package fstop.services.batch;

import org.apache.log4j.Logger;

public class Bank3ReportObj {
	private Logger logger = Logger.getLogger(getClass());

	String SDATE;
	
	String EDATE;
	
	long N814_All;
	
	long N814_Success;
	
	long N8141_All;
	
	long N8141_Success;
	
	public String getEDATE() {
		return EDATE;
	}
	public void setEDATE(String edate) {
		EDATE = edate;
	}
	public String getSDATE() {
		return SDATE;
	}
	public void setSDATE(String sdate) {
		SDATE = sdate;
	}
	public long getN814_All() {
		return N814_All;
	}
	public void setN814_All(long all) {
		N814_All = all;
	}
	public long getN814_Success() {
		return N814_Success;
	}
	public void setN814_Success(long success) {
		N814_Success = success;
	}
	public long getN8141_All() {
		return N8141_All;
	}
	public void setN8141_All(long all) {
		N8141_All = all;
	}
	public long getN8141_Success() {
		return N8141_Success;
	}
	public void setN8141_Success(long success) {
		N8141_Success = success;
	}
	
}
