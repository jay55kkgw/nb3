package fstop.services.batch;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.owasp.esapi.SafeFile;
import org.owasp.esapi.errors.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.oao.Old_TxnUserDao;
import fstop.orm.old.OLD_TXNUSER;
import fstop.orm.po.TXNUSER;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import fstop.util.FtpBase64;
import fstop.util.FtpPathProperties;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 每日產生上傳 email 修改的清單至 /DecodeFolder/CreditCard
 * 
 * @author Owner
 *
 */

@Slf4j
@RestController
@Batch(id = "batch.stopDif", name = "每日產生需停損停利通知的名單檔給基金主機", description = "讀DB，產生名單檔案給基金主機")
public class STOPDIF implements EmailUploadIf, BatchExecute {

	@Autowired
	private TxnUserDao txnUserDao;

	@Autowired
	private Old_TxnUserDao old_TxnUserDao;

	@Value("${fund.ftp.ip:}")
	private String fund_ftp_ip;

	@Autowired
	private CheckServerHealth checkserverhealth;

	@Value("${isParallelCheck}")
	private String isParallelCheck;

	@Value("${doFTP}")
	private String doFTP;

	@Autowired
	private SysBatchDao sysBatchDao;

	@RequestMapping(value = "/batch/stopDif", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {

		BatchResult batchresult = new BatchResult();
		Map<String, Object> data = new LinkedHashMap();
		batchresult.setBatchName(getClass().getSimpleName());
		batchresult.setSuccess(true);

		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}

		List<String> safeArgs = ESAPIUtil.validStrList(args);

		log.info("STOPDIF 停損停利 ID  ... ");
		int failcnt = 0, okcnt = 0, totalcnt = 0;
		Hashtable rowcnt = new Hashtable();
		Hashtable<String, String> params = new Hashtable();
		params.put("TXNIDT", DateTimeUtils.format("yyMMddHHmmss", new Date()));

		try {
			exec(rowcnt);
			failcnt = ((Integer) rowcnt.get("FAILCNT")).intValue();
			okcnt = ((Integer) rowcnt.get("OKCNT")).intValue();
			totalcnt = ((Integer) rowcnt.get("TOTALCNT")).intValue();
			log.debug("failcnt = " + failcnt);
			log.debug("okcnt = " + okcnt);
			log.debug("totalcnt = " + totalcnt);
		} catch (Exception e) {
			log.error("執行 Batch 有誤 !", e);
			data.put("Error", e.getMessage());
			batchresult.setSuccess(false);
			batchresult.setData(data);

			try {
				sysBatchDao.finish_FAIL2(batchName, batchresult.getData());
			} catch (Exception e2) {
				log.error("sysBatchDao.finish error >> {} ", e2.getMessage());
			}
			return batchresult;
		}

		BatchCounter bcnt = new BatchCounter();
		bcnt.setFailCount(failcnt);
		bcnt.setSuccessCount(okcnt);
		bcnt.setTotalCount(totalcnt);

		data.put("COUNTER", bcnt);

		if ("Y".equals(doFTP)) {
			data.put("FTP_Status", rowcnt.get("FTP_Status"));
		}

		batchresult.setData(data);

		try {
			// 新增SYSBATCH紀錄
			sysBatchDao.finish_OK(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.finish_OK error >> {} ", e.getMessage());
		}

		return batchresult;
	}

	private void exec(Hashtable rowcnt) throws IOException {
		Integer totalcnt = 0, failcnt = 0, okcnt = 0;
		Date d = new Date();
		String today = DateTimeUtils.getDateShort(d);
		today = today.substring(0, 8);
		String time = DateTimeUtils.getTimeShort(d);

		FtpPathProperties nrcpro = new FtpPathProperties();
		String srcpath = nrcpro.getSrcPath("stopdif").trim();
		// srcpath = "C:\\Users\\BenChien\\Desktop\\REFCIF";
		log.debug(ESAPIUtil.vaildLog("srcpath = " + srcpath));
		// 原版
		String despath = nrcpro.getDesPath("stopdif").trim();
		// 測試用
		// String despath = nrcpro.getDesPath("stopdifTest").trim();
		log.debug(ESAPIUtil.vaildLog("despath = " + despath));
		File tmpFile;
		try {
			tmpFile = new SafeFile(srcpath);
			tmpFile.setWritable(true, true);
			tmpFile.setReadable(true, true);
			tmpFile.setExecutable(true, true);
			tmpFile.delete();
			// FileWriter fos = new FileWriter(tmpFile);
			// BufferedWriter out = new BufferedWriter(fos);
			// Path p = tmpFile.toPath();
			StringBuilder sb = new StringBuilder();
			List<TXNUSER> all = txnUserDao.getAllFundStopNotifyUser();

			if ("Y".equals(isParallelCheck)) {
				List<OLD_TXNUSER> all_old = old_TxnUserDao.getAllFundStopNotifyUser();
				log.trace("NB2.5 all_old >> {} ", all_old.size());
				all = combine(all, all_old);
			}

			log.debug("allListSize >> {}", all.size());

			String recPattern = "%-10s%-3s\r\n";

			for (TXNUSER user : all) {

				String pCustId = StrUtils.left(user.getDPSUERID().trim() + "          ", 10);
				String BrchID = "   "; // 電子郵件地址

				String f = String.format(recPattern, pCustId, BrchID);
				log.debug(ESAPIUtil.vaildLog("f = " + f));
				// out.write(f);
				// Files.write(p, f.getBytes("BIG5"));

				sb.append(f);
				okcnt++;
				totalcnt++;
			}

			byte[] strToBytes = sb.toString().getBytes("BIG5");
			BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(tmpFile));
			outputStream.write(strToBytes);
			outputStream.close();

			// try {
			// out.flush();
			// out.close();
			// }
			// catch(Exception e){}
			// try {
			// fos.close();
			// }
			// catch(Exception e){}
			if (okcnt == 0) {
				rowcnt.put("OKCNT", okcnt);
				rowcnt.put("FAILCNT", failcnt);
				rowcnt.put("TOTALCNT", totalcnt);
				return;
			}

			if ("Y".equals(doFTP)) {
				String ip = fund_ftp_ip;
				String filename = (String) despath;

				FtpBase64 ftpBase64 = new FtpBase64(ip);
				log.debug(ESAPIUtil.vaildLog("tmpFile = " + tmpFile.getPath()));
				int rc = ftpBase64.upload_ASCII(despath, new FileInputStream(tmpFile));
				// 目地 來源
				if (rc != 0) {
					log.debug("FTP 失敗(FUND 主機)");
					// CheckServerHealth checkserverhealth =
					// (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
					checkserverhealth.sendUnknowNotice("FTP 失敗(FUND 主機)");
					rowcnt.put("FTP_Status", "Failed");
				} else {
					rowcnt.put("FTP_Status", "Success");
				}
			}

			rowcnt.put("OKCNT", okcnt);
			rowcnt.put("FAILCNT", failcnt);
			rowcnt.put("TOTALCNT", totalcnt);

			// tmpFile.delete();
		} catch (ValidationException e) {
			// TODO Auto-generated catch block
			log.error("NewSafeFile 錯誤.", e);
		}

	}

	private List<TXNUSER> combine(List<TXNUSER> nb3users, List<OLD_TXNUSER> nnbusers) {
		for (OLD_TXNUSER nnbuser : nnbusers) {
			if (null == txnUserDao.findById(nnbuser.getDPSUERID())) {
				log.info(ESAPIUtil.vaildLog("Get NNB USER : " + nnbuser.getDPSUERID()));
				TXNUSER nnbConvert2nb3 = CodeUtil.objectCovert(TXNUSER.class, nnbuser);
				nb3users.add(nnbConvert2nb3);
			}
		}

		return nb3users;
	}

}
