package fstop.services.batch;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.TxnUser_BatchDao;
import fstop.orm.po.TXNUSER;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 更新客戶分行資料
 * 讀取檔案，更新客戶的 txnuser 分行檔內容
 *
 */
@Slf4j
@RestController
@Batch(id="batch.brhcod", name = "更新客戶分行資料", description = "讀取檔案，更新客戶的 txnuser 分行檔內容")
public class BrhCod  implements BatchExecute {
	private DecimalFormat fmt = new DecimalFormat("#0");
	
	@Autowired
	private TxnUser_BatchDao txnUser_BatchDao;
	
	@Value("${batch.hostfile.path:}")
	private String pathName;
	
	@Value("${host.hostdata.path}")
	private String hhdpath;
	
	@Value("${host.other.path}")
	private String hopath;
	
	@Autowired
	private SysBatchDao sysBatchDao;
	
	/**
	 * args[0] method name
	 */
    @RequestMapping(value = "/batch/brhcod", method = {RequestMethod.POST},
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @Override
	public BatchResult execute(@RequestBody List<String> args) {
    	
		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}
    	
    	List<String> safeArgs = ESAPIUtil.validStrList(args);
    	
    	BatchResult batchresult = new BatchResult();
    	batchresult.setBatchName(getClass().getSimpleName());
		Map<String, Object> data = new HashMap();
		BatchCounter bcnt=new BatchCounter();
    	boolean result = true;
		
		int	totalcnt=0,failcnt=0,okcnt=0,flushcnt=0;
		String FullFileName=new String();
		String FullFileName_Host = new String();
		InputStreamReader in = null;
		BufferedReader bin = null;
		String subject=new String();
		String custid=new String();
		String adbranchid=new String();
		Date d = new Date();
		String today = DateTimeUtils.getDateShort(d);
		String time = DateTimeUtils.getTimeShort(d);
		
		Hashtable params = new Hashtable();
		params.put("DATE", today);
		params.put("TIME", time);
		log.debug("DATE = " + today);
		log.debug("TIME = " + time);
		FullFileName=pathName;
		FullFileName+="brhcod.txt";
		//For Error log
		FullFileName_Host=hhdpath;
		FullFileName_Host+="brhcod.txt";
		
		log.debug("FullFileName = " + FullFileName);
		
		d = new Date();
		today = DateTimeUtils.getDateShort(d);
		time = DateTimeUtils.getTimeShort(d);
		log.debug("delete ok DATE = " + today);
		log.debug("delete ok TIME = " + time);
		
		try {
			
			Map<String,String> allUserMap = txnUser_BatchDao.userbrhcodMap();
			log.trace(ESAPIUtil.vaildLog("allUserMap >> " + allUserMap.toString()));
			
			in = new InputStreamReader(new FileInputStream(FullFileName), "BIG5");
			bin = new BufferedReader(in);
			
			if (!bin.ready()) {
				in.close();
                bin.close();
            	throw new Exception();
			}
			
			while(true)
			{
				//subject = bin.readLine();
				subject = ESAPIUtil.validInput(bin.readLine(),"GeneralString", true);
				if(subject==null || subject.length()==0)
					break;
				StringTokenizer st = new StringTokenizer(subject," ");
				while(st.hasMoreTokens())
				{
					String tmpbrid="";
					custid=st.nextToken().trim();
					try
					{
						tmpbrid=st.nextToken();
					}
					catch(Exception e)
					{
						tmpbrid="   ";
					}
					adbranchid=StrUtils.left(tmpbrid,3);
//	        		log.debug(ESAPIUtil.vaildLog("custid = " + custid + "|"));
//	        		log.debug(ESAPIUtil.vaildLog("adbranchid = " + adbranchid));
	        		if(adbranchid.equals("   ")) {
	        			continue;
	        		}
	        		
	        		if(allUserMap.containsKey(custid) && !adbranchid.equals(allUserMap.get(custid))) {
	        			log.trace(ESAPIUtil.vaildLog("custid : "+custid+" , Old adbranchid :" +allUserMap.get(custid)));
	        			try {
	        				log.trace("new adbranchid : {}" , adbranchid);
	        				txnUser_BatchDao.updateBranchid(custid,adbranchid);
		        			okcnt++;
	        			}catch(Exception e) {
	        				failcnt++;
	        				log.error("updateBranchid error " , e.getCause());
	        			}
	        		}
	        		
	        		totalcnt++;
				}
			}
			
			
			d = new Date();
			today = DateTimeUtils.getDateShort(d);
			time = DateTimeUtils.getTimeShort(d);
			log.debug("insert ok DATE = " + today);
			log.debug("insert ok TIME = " + time);

		}
		catch(FileNotFoundException e) {
			log.error("找不到 (" + FullFileName_Host + ")檔案," + e.getMessage());
			data.put("Error", "找不到檔案(" + FullFileName_Host + ")");
			batchresult.setData(data);
			result = false;
		}
		catch(IOException e) {
			log.error("IOException (" + FullFileName_Host + ")," + e.getMessage());
			data.put("Error", "讀取檔案錯誤(" + FullFileName_Host + ")");
			batchresult.setData(data);
			result = false;
		}
		catch(Exception e) {
			data.put("Error", "檔案為空(" + FullFileName_Host + ")");
			batchresult.setData(data);
			result = false;
		}
		finally {
			try {
				in.close();
				bin.close();
			} catch(Exception e){}
			in = null;
			bin = null;
		}
		
		bcnt.setFailCount(failcnt);
		bcnt.setSuccessCount(okcnt);
		bcnt.setTotalCount(totalcnt);
		data.put("COUNTER", bcnt);

		batchresult.setSuccess(result);
		batchresult.setData(data);

		try {
			if (batchresult.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName,data);
			}
		}catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}
		return batchresult;

	}
}
