package fstop.services.batch;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.owasp.esapi.ESAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.rest.model.JSPUtils;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.model.Rows;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.TxnFundDataDao;
import fstop.orm.dao.TxnLogDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.oao.Old_TxnLogDao;
import fstop.orm.oao.Old_TxnUserDao;
import fstop.orm.old.OLD_TXNLOG;
import fstop.orm.old.OLD_TXNUSER;
import fstop.orm.po.TXNFUNDDATA;
import fstop.orm.po.TXNLOG;
import fstop.orm.po.TXNUSER;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import fstop.util.FtpBase64;
import fstop.util.FtpPathProperties;
import fstop.util.JSONUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * (財管部)每月產生上月透過行外電腦辦理基金申購(大額、小額)、轉換、贖回交易資料清單至 /TBB/nnb/data/CNM416.YYYYMMDD.TXT
 * 
 * @author Simon
 * 
 */
@Slf4j
@RestController
@Batch(id = "batch.fundIPReport", name = "讀取同一ip不同id大於兩次的交易紀錄清單，產生檔案，ftp給財管系統", description = "讀取同一ip不同id大於兩次的交易紀錄清單，產生檔案，ftp給財管系統")
public class FundIPReport implements BatchExecute {

	@Autowired
	private TxnLogDao txnLogDao;
	@Autowired
	private TxnFundDataDao txnFundDataDao;
	@Autowired
	private TxnUserDao txnUserDao;
	@Autowired
	private Old_TxnLogDao old_TxnLogDao;
	@Autowired
	private Old_TxnUserDao old_TxnUserDao;

	@Autowired
	private CheckServerHealth checkserverhealth;
	
	@Autowired
	private SysBatchDao sysBatchDao;

	@Value("${wms.ftp.ip}")
	private String wms_ftp_ip;

	@Value("${isParallelCheck}")
	private String isParallelCheck;
	@Value("${doFTP}")
	private String doFTP;

	@RequestMapping(value = "/batch/fundIPReport", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {

		List<String> safeArgs = ESAPIUtil.validStrList(args);

		log.warn("行外電腦辦理基金申購、轉換、贖回、變更交易資料(財管系統)  ... ");
		
		BatchResult result = new BatchResult();
		String ERRORMSG = "";
		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}
		
		int failcnt = 0, okcnt = 0, totalcnt = 0;
		Hashtable rowcnt = new Hashtable();
		Hashtable<String, String> params = new Hashtable();
		params.put("TXNIDT", DateTimeUtils.format("yyMMddHHmmss", new Date()));
		String str_StartDate = "";
		String str_EndDate = "";
		String DATEDATA = "";
		try {

			if (safeArgs.size() > 0) {
				str_StartDate = safeArgs.get(0);
				str_EndDate = safeArgs.get(1);
				DATEDATA = str_StartDate + str_EndDate;
				exec(rowcnt, DATEDATA);
			} else {
				exec(rowcnt, DATEDATA);
			}
			failcnt = ((Integer) rowcnt.get("FAILCNT")).intValue();
			okcnt = ((Integer) rowcnt.get("OKCNT")).intValue();
			totalcnt = ((Integer) rowcnt.get("TOTALCNT")).intValue();
			
			
			BatchCounter bcnt = new BatchCounter();
			bcnt.setFailCount(failcnt);
			bcnt.setSuccessCount(okcnt);
			bcnt.setTotalCount(totalcnt);

			Map<String, Object> data = new HashMap();

			data.put("COUNTER", bcnt);
			result.setData(data);
			result.setSuccess(true);
			
			
			log.debug("failcnt = " + failcnt);
			log.debug("okcnt = " + okcnt);
			log.debug("totalcnt = " + totalcnt);
		} catch (Exception e) {
			log.error("執行 Batch 有誤 !", e);
			result.setSuccess(false);
			ERRORMSG = e.getMessage();
			result.setErrorCode(ERRORMSG);
		}
		result.setBatchName(this.getClass().getSimpleName());
		
		try {
			if (result.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL(batchName,ERRORMSG);
			}
		}catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}
		
		
		return result;
	}

	private void exec(Hashtable rowcnt, String DATEDATA) throws IOException {
		Integer totalcnt = 0, failcnt = 0, okcnt = 0;
		String strStartdate = "";
		String strEnddate = "";
		Rows all = null;
		Date d = new Date();
		java.util.Calendar calendar = java.util.Calendar.getInstance();
		calendar.setTime(d);
		
		String EXTEND_FLAG_OLD="1";  //分辨是否為報表延伸資料 1(原CNM416報表資料),2(延伸資料-同日同一客戶相近時間內(一小時內使用不同IP位址資料))
		
		if (DATEDATA.length() == 0) {
			// 跑上個月資料
			calendar.add(calendar.MONTH, -1);
			d = calendar.getTime();
			String lastmonth = DateTimeUtils.getDateShort(d);
			lastmonth = lastmonth.substring(0, 6) + "%";
			String year = lastmonth.substring(0, 4);
			String mon = lastmonth.substring(4, 6);
			String sEndDay = "";
			if (mon.equals("01") || mon.equals("03") || mon.equals("05") || mon.equals("07") || mon.equals("08")
					|| mon.equals("10") || mon.equals("12")) {
				sEndDay = "31";
			} else if (mon.equals("04") || mon.equals("06") || mon.equals("09") || mon.equals("11")) {
				sEndDay = "30";
			} else {
				if (mon.equals("02") && ((Integer.parseInt(year) % 4 == 0 && Integer.parseInt(year) % 100 != 0)
						|| (Integer.parseInt(year) % 400 == 0))) {
					sEndDay = "29";
				} else if (mon.equals("02")) {
					sEndDay = "28";
				}
			}

			strStartdate = year + mon + "01";
			strEnddate = year + mon + sEndDay;
			
			log.warn(ESAPIUtil.vaildLog("findByFundIPReport：Sdate：" + strStartdate + "；Edate：" + strEnddate));
			all = txnLogDao.findByFundIPReportforH681(strStartdate, strEnddate);
			
		} else {
			strStartdate = DATEDATA.substring(0, 8);
			strEnddate = DATEDATA.substring(8);
			log.warn(ESAPIUtil.vaildLog("findByFundIPReport1：Sdate：" + strStartdate + "；Edate：" + strEnddate));
			all = txnLogDao.findByFundIPReportforH681(strStartdate, strEnddate);
		}
		if ("Y".equals(isParallelCheck)) {
			Rows allOld = old_TxnLogDao.findByFundIPReportforH681(strStartdate, strEnddate);

			log.info(ESAPIUtil.vaildLog("NB 3 data size >> "+ all.getSize()));
			log.info(ESAPIUtil.vaildLog("NB 2.5 data size >> "+ allOld.getSize()));
			all.addRows(allOld);
		}else {
			log.info(ESAPIUtil.vaildLog("NB 3 data size >> "+ all.getSize()));
		}
		
		TXNUSER txnuser;
		TXNLOG ft;
		FtpPathProperties nrcpro = new FtpPathProperties();
		String srcpath = nrcpro.getSrcPath("fundipreport");
		log.warn(ESAPIUtil.vaildLog("srcpath = " + srcpath));
		String despath = nrcpro.getDesPath("fundipreport");
		log.warn(ESAPIUtil.vaildLog("despath = " + despath));
		String FILENAME = "CNM416." + strEnddate + ".TXT"; // 產生來源落地檔 路徑+檔名
		String WFILENAME = despath + FILENAME; // ftp目的地路徑及檔名
		// Absolute Path Traversal
		String LFILENAME = srcpath + FILENAME;
		// String LFILENAME = "C:\\Users\\BenChien\\Desktop\\batchLocalTest\\"+FILENAME;

		File tmpFile = new File(LFILENAME);
		tmpFile.setWritable(true, true);
		tmpFile.setReadable(true, true);
		tmpFile.setExecutable(true, true);
		tmpFile.delete();
		OutputStream fos = new FileOutputStream(tmpFile);
		BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(tmpFile), "BIG5"));

		for (int j = 0; j < all.getSize(); j++) {
			List<TXNLOG> all2 = new ArrayList<TXNLOG>();
			log.warn(ESAPIUtil.vaildLog("findByFundIPDataReport：Sdate：" + strStartdate + "；Edate：" + strEnddate));
			//2.5的資料 併行時判斷資料來源是否為舊網銀
			if("Y".equals(isParallelCheck)
				&& StrUtils.isNotEmpty(all.getRow(j).getValue("SRC"))
				&& "2.5".equals(all.getRow(j).getValue("SRC"))) {
				List<OLD_TXNLOG> old_list_data = old_TxnLogDao.findByFundIPDataReport1(
						all.getRow(j).getValue("aduserid"), all.getRow(j).getValue("aduserip"), strStartdate,
						strEnddate);
				for (OLD_TXNLOG nnbdata : old_list_data) {
					all2.add(CodeUtil.objectCovert(TXNLOG.class, nnbdata));
				}
			}else {
				all2 = txnLogDao.findByFundIPDataReport1(all.getRow(j).getValue("aduserid"),
						all.getRow(j).getValue("aduserip"), strStartdate, strEnddate);
			}

			for (int k = 0; k < all2.size(); k++) {
				ft = all2.get(k);
				log.warn(ESAPIUtil.vaildLog("findByUserId USERID：" + ft.getADUSERID()));

				String space = "";
				String userip = ft.getADUSERIP();
				for (int m = 0; m < 15 - (userip.trim().length()) * 1; m++)
					space += " ";
				userip = userip + space;
				space = "";
				String logintype = ft.getLOGINTYPE().trim();
				if (logintype.length() == 0)
					logintype = "NB";
				String userid = ft.getADUSERID();
				List<TXNUSER> listtxnUser = txnUserDao.findByUserId(userid);
				log.warn(ESAPIUtil.vaildLog("nb3 listtxnUser.size() = " + listtxnUser.size()));
				String adbranchid = "";
				String DPUSERNAME = "";
				
				if("Y".equals(isParallelCheck)) {
					if((listtxnUser == null || listtxnUser.size() == 0)) {
						log.warn(ESAPIUtil.vaildLog("User ID not found in NB3 , ID = " + userid));
						List<OLD_TXNUSER> old_listtxnUser = old_TxnUserDao.findByUserId(userid);
						if (old_listtxnUser == null || old_listtxnUser.size() == 0) {
							log.warn(ESAPIUtil.vaildLog("User ID not found in NB2.5 , ID = " + userid));
						}else {
							log.info("User found in NB2.5");
							for (OLD_TXNUSER user : old_listtxnUser) {
								TXNUSER nnbConvert2nb3 = CodeUtil.objectCovert(TXNUSER.class, user);
								listtxnUser.add(nnbConvert2nb3);
							}
							txnuser = listtxnUser.get(0);
							adbranchid = StrUtils.trim(txnuser.getADBRANCHID());
							DPUSERNAME = (txnuser.getDPUSERNAME()).trim();
						}
					}else {
						txnuser = listtxnUser.get(0);
						adbranchid = StrUtils.trim(txnuser.getADBRANCHID());
						DPUSERNAME = (txnuser.getDPUSERNAME()).trim();
					}
				}else {
					if((listtxnUser == null || listtxnUser.size() == 0)) {
						log.warn("User ID not found in NB3 = " + userid);
					}else {
						txnuser = listtxnUser.get(0);
						adbranchid = StrUtils.trim(txnuser.getADBRANCHID());
						DPUSERNAME = (txnuser.getDPUSERNAME()).trim();
					}
				}

				for (int m = 0; m < 3 - (adbranchid.trim().length()) * 1; m++)
					space += " ";
				adbranchid = adbranchid + space;
				space = "";
				log.warn(ESAPIUtil.vaildLog("adbranchid：" + adbranchid));
				for (int m = 0; m < 10 - (userid.trim().length()) * 1; m++)
					space += " ";
				userid = userid + space;
				space = "";
				log.warn(ESAPIUtil.vaildLog("userid：" + userid));
				Map map = JSONUtils.json2map(ft.getADCONTENT());

				String str_transcode = map.get("TRANSCODE") == null ? "" : (String) map.get("TRANSCODE");
				log.warn(ESAPIUtil.vaildLog("TRANSCODE：" + str_transcode));
				String adopid = ft.getADOPID() == null ? "" : ft.getADOPID();
				log.warn(ESAPIUtil.vaildLog("adopid：" + adopid));
				String typename;
				String str_findLname;
				if (adopid.equals("C111")) {
					str_transcode = (String) map.get("PAYTAG");
				}
				if (str_transcode != null && !str_transcode.equals("")) {
					List<TXNFUNDDATA> fdata = txnFundDataDao.findByFUNDLNAME(str_transcode);
					TXNFUNDDATA fd = fdata.get(0);

					str_findLname = JSPUtils.convertFullorHalf(fd.getFUNDLNAME().trim().replace(" ", ""), 1);
					if (str_findLname.trim().length() > 25) {
						str_findLname = str_findLname.trim().substring(0, 25);
					}
					
					//如果找不到 str_findLname 直接continue;
					if(StrUtils.isEmpty(str_findLname.trim())){
						continue;
					}
					
					log.warn(ESAPIUtil.vaildLog("FUNDLNAME：" + str_findLname));
					for (int m = 0; m < 50 - (str_findLname.trim().length()) * 2; m++)
						space += " ";
					str_findLname = str_findLname + space;
					space = "";
				} else {
					// 如果找不到 str_transcode 直接continue;
//					str_findLname = "                                                  ";
					continue;
				}
				str_transcode = str_transcode == null ? "" : str_transcode;
				for (int m = 0; m < 4 - (str_transcode.trim().length()) * 1; m++)
					space += " ";
				str_transcode = str_transcode + space;
				if (adopid.equals("C016") || adopid.equals("C017")) {
					typename = "申購　";
				} else if (adopid.equals("C021")) {
					typename = "轉換　";
				} else if (adopid.equals("C024")) {
					typename = "贖回　";
				} else if (adopid.equals("C111")) {
					typename = "變更　";
				} else if (adopid.equals("InvestAttr")) {
					typename = "KYC   ";
				} else {
					typename = "　　　";
				}
				log.warn(ESAPIUtil.vaildLog("typename：" + typename));
				space = "";

				String txamt = ft.getADTXAMT().trim();
				if (adopid.equals("C111")) {
					txamt = (map.get("PAYAMT") == null) ? "0" : (String) map.get("PAYAMT");
					txamt = txamt.replaceAll("\\s+", ""); // 修正數字前面有空白字元時，程式會當掉問題
					if (txamt.trim().length() == 0)
						txamt = "0";
					if (Integer.parseInt(txamt) == 0) {
						txamt = map.get("OPAYAMT") == null ? "0" : (String) map.get("OPAYAMT");
						if (txamt.trim().length() == 0)
							txamt = "0";
					}
				}
				txamt = txamt == null ? "0" : txamt;
				if (txamt.indexOf(".") > -1)
					txamt = txamt.substring(0, txamt.indexOf("."));
				for (int m = 0; m < 15 - (txamt.trim().length()) * 1; m++)
					space += "0";
				txamt = space + txamt;
				space = "";

				String adcurrency = "";
				if (ft.getADCURRENCY() != null) {
					adcurrency = ft.getADCURRENCY();
				} else {
					adcurrency = "   ";
				}
				if (adopid.equals("C111")) {
					adcurrency = map.get("PAYCUR") == null ? "   " : (String) map.get("PAYCUR");
					if (adcurrency.equals("新台幣"))
						adcurrency = "NTD";
					else if (adcurrency.equals("美金"))
						adcurrency = "USD";
					else if (adcurrency.equals("澳幣"))
						adcurrency = "AUD";
					else if (adcurrency.equals("加拿大幣"))
						adcurrency = "CAD";
					else if (adcurrency.equals("港幣"))
						adcurrency = "HKD";
					else if (adcurrency.equals("英磅"))
						adcurrency = "GBP";
					else if (adcurrency.equals("新加坡幣"))
						adcurrency = "SGD";
					else if (adcurrency.equals("南非幣"))
						adcurrency = "ZAR";
					else if (adcurrency.equals("瑞典幣"))
						adcurrency = "SEK";
					else if (adcurrency.equals("瑞士法郎"))
						adcurrency = "CHF";
					else if (adcurrency.equals("日圓"))
						adcurrency = "JPY";
					else if (adcurrency.equals("泰幣"))
						adcurrency = "THB";
					else if (adcurrency.equals("歐洲通貨-歐元"))
						adcurrency = "EUR";
					else if (adcurrency.equals("紐幣"))
						adcurrency = "NZD";
					else if (adcurrency.equals("人民幣"))
						adcurrency = "CNY";
				}
				for (int m = 0; m < 3 - (adcurrency.length()) * 1; m++)
					space += " ";
				adcurrency = space + adcurrency;
				log.warn(ESAPIUtil.vaildLog("ADCURRENCY：" + adcurrency));
				space = "";
				for (int m = 0; m < 20 - (DPUSERNAME.length() * 2); m++)
					space += " ";
				DPUSERNAME += space;
				String newlastdate = ft.getLASTDATE();
				String txtime = ft.getLASTTIME();
				if (adopid.equals("InvestAttr"))
					adopid = "KYC ";
				String f = adbranchid + newlastdate + txtime + userid + typename + adopid + str_transcode+ str_findLname + adcurrency + txamt + userip + logintype + strEnddate + EXTEND_FLAG_OLD +DPUSERNAME + "\r\n";
				log.warn(ESAPIUtil.vaildLog("Export File Content = " + f));
				if ((userip.indexOf("125.227.162.115") != -1) || (userip.indexOf("210.63.206.136") != -1)) {
					log.warn("Export File Content = 行內對外IP不計");
				} else {
					out.write(f);
				}
				okcnt++;
				totalcnt++;
			}
		}
		
		List<TXNLOG> extendReportList = new ArrayList<TXNLOG>();
		// 延伸報表資料-同日同一客戶相近時間內(一小時內使用不同IP位址資料
		extendReportList.addAll(txnLogDao.getSameIDAnotherIPFundReport(strStartdate, strEnddate));
		if("Y".equals(isParallelCheck)) {
			List<OLD_TXNLOG> nnbdatas = old_TxnLogDao.getSameIDAnotherIPFundReport(strStartdate, strEnddate);
			List<TXNLOG> nb3datas = new ArrayList<>();
			for (OLD_TXNLOG nnbdata : nnbdatas) {
				TXNLOG nnbConvert2nb3 = CodeUtil.objectCovert(TXNLOG.class, nnbdata);
				nb3datas.add(nnbConvert2nb3);
			}
			extendReportList.addAll(nb3datas);
		}
		appendExtendData(out, extendReportList, totalcnt, okcnt, strEnddate);
		
		
		try {
			out.flush();
			out.close();
		} catch (Exception e) {
		}
		try {
			fos.close();
		} catch (Exception e) {
		}
		if (okcnt == 0) {
			rowcnt.put("OKCNT", okcnt);
			rowcnt.put("FAILCNT", failcnt);
			rowcnt.put("TOTALCNT", totalcnt);
			// return;
		}

		if ("Y".equals(doFTP)) {
			String ip = wms_ftp_ip;
			FtpBase64 ftpBase64 = new FtpBase64(ip);
			log.warn(ESAPIUtil.vaildLog("tmpFile = " + tmpFile.getPath()));
			int rc = ftpBase64.upload_ASCII(WFILENAME, new FileInputStream(tmpFile));
			if (rc != 0) {
				log.warn("FTP 失敗一次(WMS 主機)");
				// CheckServerHealth checkserverhealth =
				// (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
				checkserverhealth.sendUnknowNotice("FTP 失敗一次(WMS 主機)");
				rc = ftpBase64.upload_ASCII(WFILENAME, new FileInputStream(tmpFile));
				if (rc != 0) {
					log.warn("FTP 失敗二次(WMS 主機)");
					checkserverhealth.sendUnknowNotice("FTP 失敗二次(WMS 主機)");
				}
			} else {
				log.warn("FundIPReport FTP successful");
			}
		}
		// 測試中先註解
		// boolean drc=tmpFile.delete();
		// if(drc)
		// log.warn("delete file successful:"+FILENAME);
		// else
		// log.warn("delete file fail:"+FILENAME);
		rowcnt.put("OKCNT", okcnt);
		rowcnt.put("FAILCNT", failcnt);
		rowcnt.put("TOTALCNT", totalcnt);
	}
	
	
	private void appendExtendData(BufferedWriter writer , List<TXNLOG> allGroupByData , Integer okcnt , Integer totalcnt ,String strEnddate) throws IOException {
		log.warn("===========Start AppendExtendData ===========");
		String EXTEND_FLAG_NEW="2";  //分辨是否為報表延伸資料 1(原CNM416報表資料),2(延伸資料-同日同一客戶相近時間內(一小時內使用不同IP位址資料))
		TXNLOG ft;
		TXNUSER txnuser;
		BufferedWriter out = writer;
		
		List<TXNLOG> all2 = groupByQueryResult(allGroupByData);
		for (int k=0; k < all2.size(); k++) {
			ft= all2.get(k);
			log.warn("AppendExtendData FindByUserId USERID：" + ft.getADUSERID());
			String space="";
			String userip=ft.getADUSERIP();
			for (int m=0; m < 15-(userip.trim().length())*1; m++)
				space+=" ";
			userip=userip+space;
			space="";
			String logintype=ft.getLOGINTYPE().trim();
			if(logintype.length()==0)
				logintype="NB";
			String userid=ft.getADUSERID();
			List<TXNUSER> listtxnUser= txnUserDao.findByUserId(userid);
			log.warn("AppendExtendData ListtxnUser.size() = " + listtxnUser.size());
			String adbranchid="";
			String DPUSERNAME="";
			
			if("Y".equals(isParallelCheck)) {
				if((listtxnUser == null || listtxnUser.size() == 0)) {
					log.warn(ESAPIUtil.vaildLog("User ID not found in NB3 , ID = " + userid));
					List<OLD_TXNUSER> old_listtxnUser = old_TxnUserDao.findByUserId(userid);
					if (old_listtxnUser == null || old_listtxnUser.size() == 0) {
						log.warn(ESAPIUtil.vaildLog("User ID not found in NB2.5 , ID = " + userid));
					}else {
						log.info("User found in NB2.5");
						for (OLD_TXNUSER user : old_listtxnUser) {
							TXNUSER nnbConvert2nb3 = CodeUtil.objectCovert(TXNUSER.class, user);
							listtxnUser.add(nnbConvert2nb3);
						}
						txnuser = listtxnUser.get(0);
						adbranchid = StrUtils.trim(txnuser.getADBRANCHID());
						DPUSERNAME = (txnuser.getDPUSERNAME()).trim();
					}
				}else {
					txnuser = listtxnUser.get(0);
					adbranchid = StrUtils.trim(txnuser.getADBRANCHID());
					DPUSERNAME = (txnuser.getDPUSERNAME()).trim();
				}
			}else {
				if((listtxnUser == null || listtxnUser.size() == 0)) {
					log.warn("User ID not found in NB3 = " + userid);
				}else {
					txnuser = listtxnUser.get(0);
					adbranchid = StrUtils.trim(txnuser.getADBRANCHID());
					DPUSERNAME = (txnuser.getDPUSERNAME()).trim();
				}
			}

			for (int m=0;m<3-(adbranchid.trim().length())*1;m++)
				space+=" ";
			adbranchid=adbranchid+space;
			space="";
			log.warn("adbranchid：" + adbranchid);
			for (int m=0; m < 10-(userid.trim().length())*1; m++)
				space+=" ";
			userid=userid+space;
			space="";
			log.warn("userid：" + userid);
			Map map = JSONUtils.json2map(ft.getADCONTENT());
			
			String str_transcode = map.get("TRANSCODE")==null ? "" : (String)map.get("TRANSCODE");
			log.warn("TRANSCODE：" + str_transcode);					
			String adopid=ft.getADOPID()==null ? "" : ft.getADOPID();
			log.warn("adopid：" + adopid);
			String typename;
			String str_findLname;
			if(adopid.equals("C111")){
				str_transcode = (String)map.get("PAYTAG");
			}
			if(str_transcode != null && !str_transcode.equals("")){
				List<TXNFUNDDATA> fdata = txnFundDataDao.findByFUNDLNAME(str_transcode);
				TXNFUNDDATA fd = fdata.get(0);
									
				str_findLname = JSPUtils.convertFullorHalf(fd.getFUNDLNAME().trim().replace(" ",""), 1);
				if(str_findLname.trim().length() > 25){
					str_findLname=str_findLname.trim().substring(0, 25);
				}
				log.warn("FUNDLNAME：" + str_findLname);
				for (int m=0; m < 50-(str_findLname.trim().length())*2; m++)
					space+=" ";
				str_findLname=str_findLname+space;
				space="";
			}
			else{
				str_findLname = "                                                  ";
			}
			str_transcode=str_transcode==null ? "" : str_transcode;
			for (int m=0; m < 4-(str_transcode.trim().length())*1; m++)
				space+=" ";
			str_transcode=str_transcode+space;					
			if(adopid.equals("C016") || adopid.equals("C017")){
				typename = "申購　";
			}
			else if(adopid.equals("C021")){
				typename = "轉換　";
			}
			else if(adopid.equals("C024")){
				typename = "贖回　";
			}
			else if(adopid.equals("C111")){
				typename = "變更　";
			}
			else if(adopid.equals("InvestAttr")){
				typename = "KYC   ";
			}
			else{
				typename = "　　　";
			}
			log.warn("typename：" + typename);
			space="";
			
			String txamt=ft.getADTXAMT().trim();
			if(adopid.equals("C111")){
				txamt=(map.get("PAYAMT")==null) ? "0" : (String)map.get("PAYAMT");
				txamt=txamt.replaceAll("\\s+", ""); //修正數字前面有空白字元時，程式會當掉問題
				if(txamt.trim().length()==0) txamt="0";
				if(Integer.parseInt(txamt)==0)
				{
					txamt=map.get("OPAYAMT")==null ? "0" : (String)map.get("OPAYAMT");
					if(txamt.trim().length()==0) txamt="0";
				}
			}
			txamt=txamt==null ? "0" : txamt;
			if(txamt.indexOf(".")>-1)
				txamt=txamt.substring(0,txamt.indexOf("."));					
			for (int m=0; m < 15-(txamt.trim().length())*1; m++)
				space+="0";
			txamt=space+txamt;
			space="";
			
			String adcurrency="";					
			if(ft.getADCURRENCY()!=null){
				adcurrency = ft.getADCURRENCY();
			}
			else{
				adcurrency="   ";
			}
			if(adopid.equals("C111"))
			{
				adcurrency=map.get("PAYCUR")==null ? "   " : (String)map.get("PAYCUR");
				if(adcurrency.equals("新台幣"))
					adcurrency="NTD";
				else if(adcurrency.equals("美金"))
					adcurrency="USD";
				else if(adcurrency.equals("澳幣"))
					adcurrency="AUD";
				else if(adcurrency.equals("加拿大幣"))
					adcurrency="CAD";
				else if(adcurrency.equals("港幣"))
					adcurrency="HKD";
				else if(adcurrency.equals("英磅"))
					adcurrency="GBP";
				else if(adcurrency.equals("新加坡幣"))
					adcurrency="SGD";
				else if(adcurrency.equals("南非幣"))
					adcurrency="ZAR";
				else if(adcurrency.equals("瑞典幣"))
					adcurrency="SEK";
				else if(adcurrency.equals("瑞士法郎"))
					adcurrency="CHF";
				else if(adcurrency.equals("日圓"))
					adcurrency="JPY";
				else if(adcurrency.equals("泰幣"))
					adcurrency="THB";
				else if(adcurrency.equals("歐洲通貨-歐元"))
					adcurrency="EUR";
				else if(adcurrency.equals("紐幣"))
					adcurrency="NZD";
				else if(adcurrency.equals("人民幣"))
					adcurrency="CNY";
			}
			for (int m=0;m<3-(adcurrency.length())*1;m++)
				space+=" ";
			adcurrency=space+adcurrency;
			log.warn("ADCURRENCY：" + adcurrency);
			space="";
			for (int m=0;m < 20-(DPUSERNAME.length()*2);m++)
				space+=" ";
			DPUSERNAME+=space;
			String newlastdate=ft.getLASTDATE();
			String txtime=ft.getLASTTIME();
			if(adopid.equals("InvestAttr"))
				adopid="KYC ";
			String f = adbranchid+newlastdate+txtime+userid+typename+adopid+str_transcode+str_findLname+adcurrency+txamt+userip+logintype+strEnddate+EXTEND_FLAG_NEW+DPUSERNAME+"\r\n";
			log.warn("Export File Content = " + f);
			if((userip.indexOf("125.227.162.115") != -1) || (userip.indexOf("210.63.206.136") != -1)){
				log.warn("Export File Content = 行內對外IP不計");
			}
			else{
				out.write(f);
			}
			okcnt++;
			totalcnt++;
		}
	try {
		out.flush();
	}
	catch(Exception e){}
	}
	
	/**
     *   根據IP進行資料分組
     * @param records
     * @return Map
     */
	private List<TXNLOG> groupByQueryResult(List<TXNLOG> records){
		log.info("TXNLOG size : " + records.size());
		
		Map<String,List<TXNLOG>> groupBy = new LinkedHashMap<String, List<TXNLOG>>(); 
		Map<String,List<TXNLOG>> tempRecord = new ConcurrentHashMap<String, List<TXNLOG>>();
		Map<String,List<TXNLOG>> groupByIdIp = new LinkedHashMap<String, List<TXNLOG>>();
		List<TXNLOG> filtedList = new ArrayList<TXNLOG>();
		
		//依照ID+Date
		for (TXNLOG txnlog : records) {
			if(groupBy.containsKey(txnlog.getADUSERID()+txnlog.getLASTDATE())){
				groupBy.get(txnlog.getADUSERID()+txnlog.getLASTDATE()).add(txnlog);
			}else {
				List<TXNLOG> txnlogs = new LinkedList<TXNLOG>();
				txnlogs.add(txnlog);
				groupBy.put(txnlog.getADUSERID()+txnlog.getLASTDATE(), txnlogs);
			}
		}
		
		log.info("groupBy size : " + groupBy.size());
		//同ID+日大於2次
		for (Entry<String, List<TXNLOG>> txnlogs : groupBy.entrySet()) {
			if(txnlogs.getValue().size()>1){
				
				for (int i = 0; i < txnlogs.getValue().size(); i++) {
					TXNLOG log = txnlogs.getValue().get(i);
					String hour = StringUtils.left(log.getLASTTIME(), 2);
					
					if(i+1 < txnlogs.getValue().size()){
						TXNLOG nextLog = txnlogs.getValue().get(i+1);
						String nextHour = StringUtils.left(nextLog.getLASTTIME(), 2);
						
						if(NumberUtils.isNumber(hour)&& NumberUtils.isNumber(nextHour)
								&& NumberUtils.toInt(nextHour)-NumberUtils.toInt(hour)<=1
								&& !StringUtils.equalsIgnoreCase(log.getADUSERIP(), nextLog.getADUSERIP())){
							filtedList.add(log);
							filtedList.add(nextLog);
						}
					}
				}
				
//				tempRecord.put(txnlogs.getKey(), txnlogs.getValue());
			}
		}
		
//		log.info("tempRecord size : " + tempRecord.size());
		//過濾IP出現不同兩次以上
//		for (Entry<String, List<TXNLOG>> txnlogs : tempRecord.entrySet()) {
//			Map<String,String> ips = new ConcurrentHashMap<String,String>();
//			for (TXNLOG log : txnlogs.getValue()) {
//				ips.put(log.getADUSERIP(), log.getADUSERID());
//			}
//			if(ips.size()>1){
//				groupByIdIp.put(txnlogs.getKey(), txnlogs.getValue());
//			}
//		}
		
//		log.info("groupByIdIp size : " + groupByIdIp.size());
//		for (Entry<String, List<TXNLOG>> entrySet : groupByIdIp.entrySet()) {
//			log.info("entry Key : " + entrySet.getKey());
//			if(CollectionUtils.isNotEmpty(entrySet.getValue())){
//				filtedList.addAll(entrySet.getValue());
//			}
//		}
		
		log.info("filtedList size : " + filtedList.size());
		
		return filtedList;
	}
	
	
}
