package fstop.services.batch;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.oao.Old_TxnUserDao;
import fstop.orm.old.OLD_TXNUSER;
import fstop.orm.po.TXNUSER;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import fstop.util.FtpBase64;
import fstop.util.FtpPathProperties;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 每日產生上傳 email 修改的清單至 /DecodeFolder/CreditCard
 * 
 * @author Owner
 *
 */
@Slf4j
@RestController
@Batch(id = "batch.emailUploadFund", name = "emailUploadFund", description = "讀 db 產生電子對帳單異動狀態file ， FTP 檔案給基金主機")
public class EmailUploadFund implements EmailUploadIf, BatchExecute {
	@Autowired
	private TxnUserDao txnUserDao;

	@Autowired
	Old_TxnUserDao old_TxnUserDao;

	@Value("${fund.ftp.ip:}")
	private String ftpip;

	@Value("${isParallelCheck}")
	private String isParallelCheck;

	@Value("${doFTP}")
	private String doFTP;

	@Autowired
	private CheckServerHealth checkserverhealth;

	@Autowired
	private SysBatchDao sysBatchDao;

	@RequestMapping(value = "/batch/emailUploadFund", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {
		log.debug("INTO EmailUploadFund ... ");
		List<String> safeArgs = ESAPIUtil.validStrList(args);
		int totalcnt = 0;
		int oldcnt = 0;
		int newcnt = 0;

		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}

		BatchResult result = new BatchResult();
		String ERRORMSG = "";

		try {
			result = exec();

		} catch (Exception e) {
			log.error("執行 Batch 有誤 !", e);
			result.setSuccess(false);
			ERRORMSG = e.getMessage();
			result.setErrorCode(ERRORMSG);
		}
		result.setBatchName(getClass().getSimpleName());

		try {
			if (result.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				if (!"".equals(ERRORMSG)) {
					sysBatchDao.finish_FAIL(batchName, ERRORMSG);
				} else {
					sysBatchDao.finish_FAIL2(batchName, result.getData());
				}
			}
		} catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}
		return result;
	}

	private BatchResult exec() throws IOException {
		int totalcnt = 0;
		int okcnt = 0;
		int failcnt = 0;

		BatchResult result = new BatchResult();
		BatchCounter bcnt = new BatchCounter();
		Map<String, Object> data = new LinkedHashMap();

		Date d = new Date();
		String today = DateTimeUtils.getCDateShort(d);
		today = today.substring(0, 7);
		String time = DateTimeUtils.getTimeShort(d);

		log.debug("getDateShort = " + DateTimeUtils.getDateShort(d));
		FtpPathProperties nrcpro = new FtpPathProperties();
		String srcpath = nrcpro.getSrcPath("emailuploadfund").trim();
		// // LTest
		// srcpath = "C:/Users/BenChien/Desktop/batchLocalTest/other/";
		log.debug(ESAPIUtil.vaildLog("srcpath = " + srcpath));
		String despath = nrcpro.getDesPath("emailuploadfund").trim();
		log.debug(ESAPIUtil.vaildLog("despath = " + despath));

		// Absolute Path Traversal
		String LFILENAME = srcpath + DateTimeUtils.getDateShort(d) + "_emailuploadfund.txt";
		//

		File tmpFile = new File(LFILENAME);
		tmpFile.setWritable(true, true);
		tmpFile.setReadable(true, true);
		tmpFile.setExecutable(true, true);
		// FileWriter fos = new FileWriter(tmpFile);
		// BufferedWriter out = new BufferedWriter(fos);
		BufferedWriter out = new BufferedWriter(new FileWriter(tmpFile));
		// List<TXNUSER> all = txnUserDao.getAll();
		List<TXNUSER> all = txnUserDao.getAllFundBillUser();

		// 平行驗證
		if ("Y".equals(isParallelCheck)) {
			List<OLD_TXNUSER> oldall = old_TxnUserDao.getAllFundBillUser();

			log.debug("NB 3 data size >> {}", all.size());
			log.debug("NB 2.5 data size >> {}", oldall.size());

			all = getCombineList(all, oldall);
		}

		String recPattern = "%-11s%-3s%-60s%-1s%-7s%-6s%-1s%-6s%-3s%-1s%-1s\r\n";

		for (TXNUSER user : all) {
			log.debug(ESAPIUtil.vaildLog("USERID: " + user.getDPSUERID() + " , EMAILDATE: " + user.getEMAILDATE()));
			String p1 = ""; // 客戶統編
			String p2 = ""; // 業務類別
			String pEmail = ""; // 電子郵件地址
			String p4 = ""; // 異動代碼 A or D
			String pToday = today; // 異動日期
			String pTime = time; // 異動時間
			String p7 = "2"; // 異動來源
			String p8 = "NBUSER"; // 異動人員
			String p9 = ""; // 分行代碼
			String p10 = "2"; // 對帳單交付方式 1: 紙本郵寄 2:E-MAIL 3: 本人親領
			String p11 = " ";

			p1 = user.getDPSUERID();
			pEmail = StrUtils.trim(user.getDPMYEMAIL());

			p2 = "FD";
			p10 = user.getFUNDBILLSENDMODE();
			if ("Y".equalsIgnoreCase(user.getDPFUNDBILL())) {
				pToday = user.getFUNDBILLDATE().substring(0, 7);
				pTime = user.getFUNDBILLDATE().substring(7);
				p4 = "A";
			} else {
				pToday = user.getFUNDBILLDATE().substring(0, 7);
				pTime = user.getFUNDBILLDATE().substring(7);
				p4 = "D";
			}

			if ("A".equals(p4))
				pEmail = user.getDPMYEMAIL();

			if (user.getADBRANCHID().length() == 0 || user.getADBRANCHID() == null
					|| user.getADBRANCHID().equals("   "))
				p9 = "000";
			else
				p9 = user.getADBRANCHID();

			log.debug("p9 = " + p9);

			String f = String.format(recPattern, p1, p2, pEmail, p4, pToday, pTime, p7, p8, p9, p10, p11);
			out.write(f);
			okcnt++;

		} // end for TXNUSER

		try {
			out.close();
		} catch (Exception e) {
		}

		if (okcnt == 0) {
			data.put("COUNTER", bcnt);
			result.setSuccess(true);
			return result;
		}

		if ("Y".equals(doFTP)) {
			String ip = ftpip;
			String filename = (String) despath;

			FtpBase64 ftpBase64 = new FtpBase64(ip);
			log.debug(ESAPIUtil.vaildLog("tmpFile = " + tmpFile.getPath()));
			int rc = ftpBase64.upload_ASCII(despath, new FileInputStream(tmpFile));
			// 目地 來源
			if (rc != 0) {
				log.debug(DateTimeUtils.getDateShort(d) + "_emailuploadfund.txt FTP 失敗(fund 主機)");
				// CheckServerHealth checkserverhealth =
				// (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
				checkserverhealth
						.sendUnknowNotice(DateTimeUtils.getDateShort(d) + "_emailuploadfund.txt FTP 失敗(fund 主機)");
				data.put("Error", "FTP Fail");
				bcnt.setFailCount(failcnt);
				bcnt.setSuccessCount(okcnt);
				bcnt.setTotalCount(okcnt);
				data.put("COUNTER", bcnt);
				result.setData(data);
				result.setSuccess(false);
				return result;

			}

			bcnt.setFailCount(failcnt);
			bcnt.setSuccessCount(okcnt);
			bcnt.setTotalCount(okcnt);

			data.put("COUNTER", bcnt);
			data.put("FTP_Status", "Sucecss");
			result.setData(data);
			result.setSuccess(true);

		} else {
			bcnt.setFailCount(failcnt);
			bcnt.setSuccessCount(okcnt);
			bcnt.setTotalCount(okcnt);

			data.put("COUNTER", bcnt);
			result.setData(data);
			result.setSuccess(true);
		}

		return result;
	}

	private List<TXNUSER> getCombineList(List<TXNUSER> nb3datas, List<OLD_TXNUSER> nnbdatas) {
		/*
		 * TXNUSER同步2.5 兩邊TXNUSER相同 合併規則: 找出 nnbdatas 裡"不"在 nb3datas的資料合併 Ex: nnbdatas:
		 * A123456789 , A123456814 nb3datas: A123456814 , W100089655 則合併結果是 : A123456789
		 * , A123456814(同步故資料相同,取nb3) , W100089655
		 */
		log.debug(ESAPIUtil.vaildLog("nb3datas >> {} "+ nb3datas.toString()));
		log.debug(ESAPIUtil.vaildLog("nnbdatas >> {} "+ nnbdatas.toString()));
		List<String> nb3userList = new ArrayList<String>();
		for (TXNUSER nb3 : nb3datas) {
			nb3userList.add(nb3.getDPSUERID());
		}
		for (OLD_TXNUSER nnb : nnbdatas) {
			if (nb3userList.contains(nnb.getDPSUERID())) {
				TXNUSER nnbConvert2nb3 = CodeUtil.objectCovert(TXNUSER.class, nnb);
				nb3datas.add(nnbConvert2nb3);
			}
		}

		log.debug(ESAPIUtil.vaildLog("Combine nb3datas >> {} "+ nb3datas.toString()));
		return nb3datas;
	}
}
