package fstop.services.batch;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.rest.model.CommonPools;
import com.netbank.rest.model.JSPUtils;
import com.netbank.util.ESAPIUtil;

import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.model.Rows;
import fstop.orm.dao.SysBatchDao;
import fstop.orm.dao.TxnCardApplyDao;
import fstop.orm.oao.Old_TxnCardApplyDao;
import fstop.orm.po.TXNCARDAPPLY;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import fstop.util.FtpBase64;
import fstop.util.FtpPathProperties;
import fstop.util.SpringBeanFactory;
import lombok.extern.slf4j.Slf4j;
import topibs.utility.DateUtil;

/**
 * 每日產生昨日網銀線上申請信用卡明細表至 /TBB/nb3/data/NBCCD903 並把檔案拋至報表系統
 * 
 * @author Owner 每頁45筆
 */

@Slf4j
@RestController
@Batch(id = "batch.nnbReport_D903", name = "每日產生昨日網銀線上申請信用卡明細表至 /TBB/nb3/data/NBCCD903 並把檔案拋至報表系統", description = "每日產生昨日網銀線上申請信用卡明細表至 /TBB/nnb/data/NBCCD903 並把檔案拋至報表系統")
public class NNBReport_D903 implements BatchExecute {

	@Autowired
	private TxnCardApplyDao txnCardApplyDao;

	@Autowired
	private Old_TxnCardApplyDao old_TxnCardApplyDao;

	@Value("${reportn.ftp.ip}")
	private String reportn_ftp_ip;

	@Value("${batch.hostfile.path:}")
	private String host_dir;

	@Value("${batch.localfile.path:}")
	private String path;

	@Value("${isParallelCheck}")
	private String isParallelCheck;

	@Value("${doFTP}")
	private String doFTP;

	@Autowired
	private CommonPools commonPools;

	@Autowired
	private SysBatchDao sysBatchDao;

	@RequestMapping(value = "/batch/nnbReport_D903", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BatchResult execute(@RequestBody List<String> args) {

		log.info("batch NNBReport_D903...");

		BatchResult batchresult = new BatchResult();
		Map<String, Object> data = new LinkedHashMap();
		batchresult.setBatchName(getClass().getSimpleName());
		batchresult.setSuccess(true);

		// 新增SYSBATCH紀錄
		String batchName = this.getClass().getSimpleName();
		try {
			sysBatchDao.initSysBatchPo(batchName);
		} catch (Exception e) {
			log.error("sysBatchDao.initSysBatchPo error >> {} ", e.getMessage());
		}

		List<String> safeArgs = ESAPIUtil.validStrList(args);
		log.debug("NNBReport_D903 - Start execute ");
		int failcnt = 0, okcnt = 0, totalcnt = 0;
		Hashtable rowcnt = new Hashtable();

		try {
			if (safeArgs.size() == 1) {
				log.info(ESAPIUtil.vaildLog("arg[0]: " + safeArgs.get(0)));
				Date date = DateTimeUtils.parse("yyyyMMdd", safeArgs.get(0));
				exec(rowcnt, date);
			} else {
				exec(rowcnt);
			}
			failcnt = ((Integer) rowcnt.get("FAILCNT")).intValue();
			okcnt = ((Integer) rowcnt.get("OKCNT")).intValue();
			totalcnt = ((Integer) rowcnt.get("TOTALCNT")).intValue();
			log.debug("failcnt = " + failcnt);
			log.debug("okcnt = " + okcnt);
			log.debug("totalcnt = " + totalcnt);
		} catch (Exception e) {
			log.error("執行 Batch 有誤 !", e);
			data.put("Error", e.getMessage());
			batchresult.setSuccess(false);
		}

		BatchCounter bcnt = new BatchCounter();
		bcnt.setFailCount(failcnt);
		bcnt.setSuccessCount(okcnt);
		bcnt.setTotalCount(totalcnt);

		data.put("COUNTER", bcnt);
		batchresult.setData(data);

		log.debug("NNBReport_D903 - End execute ");

		try {
			if (batchresult.isSuccess()) {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_OK(batchName);
			} else {
				// 新增SYSBATCH紀錄
				sysBatchDao.finish_FAIL2(batchName, batchresult.getData());
			}
		} catch (Exception e) {
			log.error("sysBatchDao.finish error >> {} ", e.getMessage());
		}
		return batchresult;
	}

	private void exec(Hashtable rowcnt) throws IOException {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, -1);
		exec(rowcnt, cal.getTime());
	}

	private void exec(Hashtable rowcnt, final Date date) throws IOException {
		Integer totalcnt = 0, failcnt = 0, okcnt = 0;
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		// Date d2 = new Date();
		// d2.setDate(d2.getDate() - 1);
		// String yesterday = DateTimeUtils.getCDateShort(d2);
		// yesterday = yesterday.substring(0, 7);
		String lastdate = DateTimeUtils.format("yyyyMMdd", cal.getTime());
		Date d = new Date();
		String str_ReportDate = DateUtil.formatDate(DateUtil.getTaiwanDate(lastdate));
		String str_ReportDateTime = DateTimeUtils.getCDateTime(DateTimeUtils.getCDateShort(d),
				DateTimeUtils.getTimeShort(d));

		String despath = path + "NBCCD903";
		log.warn("despath = " + despath);

		File tmpFile = new File(despath);
		tmpFile.setWritable(true, true);
		tmpFile.setReadable(true, true);
		tmpFile.setExecutable(true, true);
		tmpFile.delete();
		OutputStream fos = new FileOutputStream(tmpFile);
		BufferedWriter out = new BufferedWriter(new OutputStreamWriter(fos, "BIG5"));
		String pageHeader1 = "1 報表代號 : CCD903                  　　　　　　　　網銀線上申請信用卡明細表　　　　　　　　                            週期/保存年限：日報 /15年                       \n";
		String pageHeader2 = "  資料日期 : " + str_ReportDate + "                               印表日期 : "
				+ str_ReportDateTime + "                                       頁    次 :   ";
		String pageHeader3 = "";
		String pageFooter = "9            	          經　辦 :                    　覆　核 :                    　主　管 :\n";
		String header1 = "                                                                                   是否                                            同一IP                   \n";
		String header2 = "                                                                                   更換                                            申請                                           \n";
		String header3 = "  序號 客戶姓名  身分證字號  申請卡別                          申請日期  申請時間  姓名 國籍     勸募人員  行員編號  申請IP        次數 同意書版本 驗證機制                                            \n";
		String header4 = "  ================================================================================================================================================================\n";
		log.warn("lastdate = " + lastdate);
		// start Update 20181030 報表新增申請IP、IP申請次數、同意書版本欄位
		// List<TXNCARDAPPLY> all = txnCardApplyDao.findByLastDate1(lastdate);
		// List<TXNCARDAPPLY> all = txnCardApplyDao.addIPAndTimeAndVer(lastdate);
		Rows all = txnCardApplyDao.addIPAndTimeAndVer(lastdate);

		if ("Y".equals(isParallelCheck)) {
			Rows oldall = old_TxnCardApplyDao.addIPAndTimeAndVer(lastdate);
			log.debug("oldall size >> {}", oldall.getSize());
			all.addRows(oldall);
		}

		log.warn("all size = " + all.getSize());
		Rows newrows = new Rows();
		for (int i = 0; i < all.getSize(); i++) {
			newrows.addRow(all.getRow(i));
		}
		// end
		TXNCARDAPPLY ft;
		MVHImpl result = new MVHImpl(newrows);
		;// ADD 20181031
		log.debug("result==>" + result.dumpToString() + "<==");
		int totalreccount = 0;
		int tempcounter = 0;
		String[][] USERCARD = new String[all.getSize()][2];// update 20181031 更改長度取得方式
		for (int i = 0; i < all.getSize(); i++)// update 20181031 更改長度取得方式
		{
			int tempcnt = 0;
			// start Update 20181031 更改抓取欄位值方式
			// ft = all.get(i);
			// String CARDNAME1=ft.getCARDNAME1();
			// String CARDNAME2=ft.getCARDNAME2();
			// String CARDNAME3=ft.getCARDNAME3();
			String CARDNAME1 = result.getValueByFieldName("CARDNAME1", i + 1);
			String CARDNAME2 = result.getValueByFieldName("CARDNAME2", i + 1);
			String CARDNAME3 = result.getValueByFieldName("CARDNAME3", i + 1);
			// end
			if (CARDNAME1.length() > 0) {
				totalreccount++;
				tempcnt++;
			}
			if (CARDNAME2.length() > 0) {
				totalreccount++;
				tempcnt++;
			}
			if (CARDNAME3.length() > 0) {
				totalreccount++;
				tempcnt++;
			}
			// USERCARD[i][0] = ft.getCPRIMID();
			USERCARD[i][0] = result.getValueByFieldName("CPRIMID", i + 1);// udpate 20181031 更改抓取欄位值方式
			USERCARD[i][1] = String.valueOf(tempcnt);
		}
		log.warn("totalreccount = " + totalreccount);
		pageHeader3 = "  需求單位 : H69                                                                                                         製表單位 : 資訊部\n\n\n\n";
		Double totalpage = Math.ceil(((float) totalreccount / (float) 45));// 預設45筆
		String totalpage1 = totalpage.toString().substring(0, totalpage.toString().indexOf("."));
		log.warn("H69:" + "totalpage:" + totalpage1 + ",allsize:" + totalreccount);
		if (totalpage == 0) {
			out.write(pageHeader1);
			out.write(pageHeader2 + "1/1\n");
			out.write(pageHeader3);
			out.write(header1);
			out.write(header2);
			out.write(header3);// ADD 20181213
			out.write(header4);// ADD 20181213
			out.write(
					"                     無網銀交易紀錄                                                                     \n");
			for (int k = 0; k < 44; k++) {
				out.write("\n");
			}
			out.write(pageFooter);
		} else {
			for (int l = 0; l < totalpage; l++) {
				out.write(pageHeader1);
				out.write(pageHeader2 + (l + 1) + "/" + totalpage1 + "\n");
				out.write(pageHeader3);
				out.write(header1);
				out.write(header2);
				out.write(header3);// ADD 20181213
				out.write(header4);// ADD 20181213
				String card1 = "";
				String card2 = "";
				String card3 = "";
				int cardnum = 0;
				int counter = 0;
				for (int j = l * 45; j < totalreccount; j += counter) {
					log.warn("reportD903 j:" + j + " counter:" + counter);
					// log.warn("all.size():"+all.size());
					log.warn("all.size():" + all.getSize());
					for (int n = tempcounter; n < all.getSize(); n++) {

						log.warn("n:" + n);
						// ft=all.get(n);//udpate 20181031
						String space = "";
						String f = "";
						// String ECERT =ft.getECERT();
						// for (int m=0; m < 14-(ECERT.trim().length())*1; m++)
						// {
						// space+=" ";
						// }
						// ECERT+=space;
						// space="";
						String firstpadding = "  ";// 序號左邊兩個空白
						// String seqno=ft.getRCVNO().substring(12);//序號=抓案件編號後三碼
						String seqno = result.getValueByFieldName("RCVNO", n + 1).substring(12);// 序號=抓案件編號後三碼 udpate
																								// 20181031
						for (int m = 0; m < 5 - (seqno.length()) * 1; m++)// 7為該欄位值到下一個欄位值之間的距離
						{
							space += " ";// 計算欄位與欄位之間的空白
						}
						seqno += space;
						space = "";// RESET空白數，為了計算下一個欄位的間隔
						// String username=JSPUtils.convertFullorHalf(ft.getCPRIMCHNAME(),1);//中文姓名
						String username = JSPUtils.convertFullorHalf(result.getValueByFieldName("CPRIMCHNAME", n + 1),
								1);// 中文姓名 update 20181031
						for (int m = 0; m < 10 - (username.trim().length()) * 2; m++)// 15為該欄位值到下一個欄位值之間的距離
						{
							space += " ";// 計算欄位與欄位之間的空白
						}
						username += space;
						space = "";// RESET空白數，為了計算下一個欄位的間隔
						// String CUSIDN=ft.getCPRIMID();//身分證字號
						String CUSIDN = result.getValueByFieldName("CPRIMID", n + 1);// 身分證字號 update 20181031
						for (int m = 0; m < 12 - (CUSIDN.trim().length()) * 1; m++)// 15為該欄位值到下一個欄位值之間的距離
						{
							space += " ";// 計算欄位與欄位之間的空白
						}
						CUSIDN += space;
						space = "";// RESET空白數，為了計算下一個欄位的間隔
						cardnum = Integer.parseInt(USERCARD[n][1]);
						// String LASTDATE=ft.getLASTDATE();//異動日
						// String LASTTIME=ft.getLASTTIME();//異動時間
						String LASTDATE = result.getValueByFieldName("LASTDATE", n + 1);// 異動日 update 20181031
						String LASTTIME = result.getValueByFieldName("LASTTIME", n + 1);// 異動時間 update 20181031
						String tempyear = String.valueOf(Integer.parseInt(LASTDATE.substring(0, 4)) - 1911) + "/";
						String tempmon = LASTDATE.substring(4, 6) + "/";
						String tempday = LASTDATE.substring(6, 8);
						String FULLDATE = tempyear + tempmon + tempday;// 申請日期
						for (int m = 0; m < 10 - (FULLDATE.trim().length()) * 1; m++)// 13為該欄位值到下一個欄位值之間的距離
						{
							space += " ";// 計算欄位與欄位之間的空白
						}
						FULLDATE += space;
						space = "";// RESET空白數，為了計算下一個欄位的間隔
						String temphour = LASTTIME.substring(0, 2) + ":";
						String tempminute = LASTTIME.substring(2, 4) + ":";
						String tempsec = LASTTIME.substring(4, 6);
						String FULLTIME = temphour + tempminute + tempsec;// 申請時間
						for (int m = 0; m < 10 - (FULLTIME.trim().length()) * 1; m++)// 12為該欄位值到下一個欄位值之間的距離
						{
							space += " ";// 計算欄位與欄位之間的空白
						}
						FULLTIME += space;
						space = "";// RESET空白數，為了計算下一個欄位的間隔
						// String CHENAME_O =ft.getCHENAME();//是否更換過姓名(轉中文用)
						// String CHENAME_N =ft.getCHENAME();//是否更換過姓名(計算空白用)
						String CHENAME_O = "";
						String CHENAME_N = "";
						if (!result.getValueByFieldName("CHENAME", n + 1).trim().equals("")) {// 欄位不為空白
							CHENAME_O = result.getValueByFieldName("CHENAME", n + 1);// 是否更換過姓名(轉中文用) update 20181031
							CHENAME_O = wordswitchCHANGE_O(CHENAME_O);
							CHENAME_N = result.getValueByFieldName("CHENAME", n + 1);// 是否更換過姓名(計算空白用) update 20181031
							for (int m = 0; m < 5 - (CHENAME_N.trim().length()) * 2; m++)// 20為該欄位值到下一個欄位值之間的距離
							{
								space += " ";// 計算欄位與欄位之間的空白
							}
							CHENAME_N += space;
							CHENAME_N = CHENAME_N.substring(1);// 只留空白
						} else {
							for (int m = 0; m < 5 - (CHENAME_N.trim().length()) * 2; m++)// 20為該欄位值到下一個欄位值之間的距離
							{
								space += " ";// 計算欄位與欄位之間的空白
							}
							CHENAME_N += space;
						}
						space = "";// RESET空白數，為了計算下一個欄位的間隔
						// String CTRYDESC =ft.getCTRYDESC();//國籍
						String CTRYDESC = result.getValueByFieldName("CTRYDESC", n + 1);// 國籍 update 20181031
						for (int m = 0; m < 9 - (CTRYDESC.trim().length()) * 2; m++)// 11為該欄位值到下一個欄位值之間的距離
						{
							space += " ";// 計算欄位與欄位之間的空白
						}
						CTRYDESC += space;
						space = "";// RESET空白數，為了計算下一個欄位的間隔
						// String FDRSGSTAFF =(ft.getFDRSGSTAFF()==null ? "":ft.getFDRSGSTAFF());//勸募人員
						// update 20181031
						String FDRSGSTAFF = (result.getValueByFieldName("FDRSGSTAFF", n + 1) == null ? ""
								: result.getValueByFieldName("FDRSGSTAFF", n + 1));// 勸募人員 update
						if (!FDRSGSTAFF.trim().equals("")) {// 有值不為空白
							for (int m = 0; m < 10 - (FDRSGSTAFF.trim().length()) * 2; m++)// 12為該欄位值到下一個欄位值之間的距離
							{
								space += " ";// 計算欄位與欄位之間的空白
							}
							FDRSGSTAFF += space;
						} else if (FDRSGSTAFF.trim().equals("")) {// 全為空白
							for (int m = 0; m < 10 - (FDRSGSTAFF.trim().length()) * 2; m++)// 12為該欄位值到下一個欄位值之間的距離
							{
								space += " ";// 計算欄位與欄位之間的空白
							}
							FDRSGSTAFF += space;
						}
						space = "";// RESET空白數，為了計算下一個欄位的間隔
						// String BANKERNO =(ft.getBANKERNO()==null ?"":ft.getBANKERNO());//行員編號
						String BANKERNO = (result.getValueByFieldName("BANKERNO", n + 1) == null ? ""
								: result.getValueByFieldName("BANKERNO", n + 1));// 行員編號 update 20181031
						for (int m = 0; m < 10 - (BANKERNO.trim().length()) * 1; m++)// 12為該欄位值到下一個欄位值之間的距離
						{
							space += " ";// 計算欄位與欄位之間的空白
						}
						BANKERNO += space;
						space = "";// RESET空白數，為了計算下一個欄位的間隔
						// start Update 20181031 新增IP欄位
						// String IP=ft.getIP();//申請IP
						String IP = (result.getValueByFieldName("IP", n + 1) == null ? ""
								: result.getValueByFieldName("IP", n + 1));// 申請IP
						for (int m = 0; m < 14 - (IP.length()) * 1; m++)// 17為該欄位值到下一個欄位值之間的距離
						{
							space += " ";// 計算欄位與欄位之間的空白
						}
						IP += space;
						space = "";// RESET空白數，為了計算下一個欄位的間隔

						String TIME = (result.getValueByFieldName("TIME", n + 1) == null ? ""
								: result.getValueByFieldName("TIME", n + 1));// 申請IP次數
						for (int m = 0; m < 5 - (TIME.length()) * 1; m++)// 20為該欄位值到下一個欄位值之間的距離
						{
							space += " ";// 計算欄位與欄位之間的空白
						}
						TIME += space;
						space = "";// RESET空白數，為了計算下一個欄位的間隔

						String VERSION = (result.getValueByFieldName("VERSION", n + 1) == null ? ""
								: result.getValueByFieldName("VERSION", n + 1));// 同意書版本
						for (int m = 0; m < 11 - (VERSION.length()) * 1; m++)// 17為該欄位值到下一個欄位值之間的距離
						{
							space += " ";// 計算欄位與欄位之間的空白
						}
						VERSION += space;
						space = "";// RESET空白數，為了計算下一個欄位的間隔

						String ECERT = (result.getValueByFieldName("ECERT", n + 1) == null ? ""
								: result.getValueByFieldName("ECERT", n + 1));// 驗證機制
						ECERT = wordswitchECERT(ECERT);
						for (int m = 0; m < 10 - (ECERT.length()) * 1; m++)// 14為該欄位值到下一個欄位值之間的距離
						{
							space += " ";// 計算欄位與欄位之間的空白
						}
						ECERT += space;
						space = "";// RESET空白數，為了計算下一個欄位的間隔

						// String MEMO =(ft.getMEMO()==null ?"":ft.getMEMO());//備註
						String MEMO = (result.getValueByFieldName("MEMO", n + 1) == null ? ""
								: result.getValueByFieldName("MEMO", n + 1));// 備註 update 20181031
						for (int m = 0; m < 20 - (MEMO.trim().length()) * 2; m++)// 20為該欄位值到下一個欄位值之間的距離
						{
							space += " ";// 計算欄位與欄位之間的空白
						}
						MEMO += space;
						space = "";// RESET空白數，為了計算下一個欄位的間隔
						// end
						String beforepadding = "";
						String afterpadding = "";
						// if(ft.getCARDNAME1().length()>0)//第一張信用卡
						if (result.getValueByFieldName("CARDNAME1", n + 1).length() > 0)// 第一張信用卡 update 20181031
						{
							// update 20181031
							// card1=JSPUtils.convertFullorHalf(CommonPools.creditUtils.getCardNameByCardno(ft.getCARDNAME1()),1);//卡別
							card1 = JSPUtils.convertFullorHalf(commonPools.creditUtils
									.getCardNameByCardno(result.getValueByFieldName("CARDNAME1", n + 1)), 1);// 卡別
							for (int m = 0; m < 34 - (card1.trim().length()) * 2; m++)// 28為該欄位值到下一個欄位值之間的距離
							{
								space += " ";// 計算欄位與欄位之間的空白
							}
							card1 += space;
							// 資料列
							// 空白+序號+中文姓名+ID+卡別+申請日期+申請時間+是否更換過姓名+國籍+勸募人員+行員編號+備註
							f = firstpadding + seqno + username + CUSIDN + card1 + FULLDATE + FULLTIME + CHENAME_O
									+ CHENAME_N + CTRYDESC + FDRSGSTAFF + BANKERNO + IP + TIME + VERSION + ECERT + MEMO
									+ "\n";
							out.write(f);
							okcnt++;
							totalcnt++;
							counter++;
							log.warn(ESAPIUtil.vaildLog("Export CARDNAME1 Content = " + f + " counter:" + counter));
						}
						// 第一張信用卡 update 20190114 當沒選擇卡別時防呆處理
						if (result.getValueByFieldName("CARDNAME1", n + 1).length() == 0) {
							// update 20181031
							// card1=JSPUtils.convertFullorHalf(CommonPools.creditUtils.getCardNameByCardno(ft.getCARDNAME1()),1);//卡別
							log.warn(ESAPIUtil.vaildLog("card1 before:" + card1));
							card1 = result.getValueByFieldName("CARDNAME1", n + 1);// 卡別
							log.warn("card1 after:" + card1);
							for (int m = 0; m < 34 - (card1.trim().length()) * 2; m++)// 28為該欄位值到下一個欄位值之間的距離
							{
								space += " ";// 計算欄位與欄位之間的空白
							}
							card1 += space;
							// 資料列
							// 空白+序號+中文姓名+ID+卡別+申請日期+申請時間+是否更換過姓名+國籍+勸募人員+行員編號+備註
							f = firstpadding + seqno + username + CUSIDN + card1 + FULLDATE + FULLTIME + CHENAME_O
									+ CHENAME_N + CTRYDESC + FDRSGSTAFF + BANKERNO + IP + TIME + VERSION + ECERT + MEMO
									+ "\n";
							out.write(f);
							okcnt++;
							totalcnt++;
							counter++;
							log.warn(ESAPIUtil.vaildLog("Export CARDNAME1 Content = " + f + " counter:" + counter));
						}
						space = "";
						beforepadding = "";
						afterpadding = "";
						// if(ft.getCARDNAME2().length()>0)//第二張信用卡
						if (result.getValueByFieldName("CARDNAME2", n + 1).length() > 0)// 第二張信用卡 update 20181031
						{
							// card2=JSPUtils.convertFullorHalf(CommonPools.creditUtils.getCardNameByCardno(ft.getCARDNAME2()),1);
							card2 = JSPUtils.convertFullorHalf(commonPools.creditUtils
									.getCardNameByCardno(result.getValueByFieldName("CARDNAME2", n + 1)), 1);// update
																												// 20181031
							for (int m = 0; m < 7 - (card2.trim().length()) * 2; m++) {
								space += " ";
							}
							card2 += space;
							space = "";
							for (int m = 0; m < 66; m++) {
								space += " ";
							}
							beforepadding += space;
							space = "";
							for (int m = 0; m < 50; m++) {
								space += " ";
							}
							f = beforepadding + card2 + afterpadding + "\n";
							out.write(f);
							okcnt++;
							totalcnt++;
							counter++;
							log.warn(ESAPIUtil.vaildLog("Export CARDNAME2 Content = " + f + " counter:" + counter));
						}
						space = "";
						beforepadding = "";
						afterpadding = "";
						// if(ft.getCARDNAME3().length()>0)//第三張信用卡
						if (result.getValueByFieldName("CARDNAME3", n + 1).length() > 0)// 第三張信用卡 update 20181031
						{
							// card3=JSPUtils.convertFullorHalf(CommonPools.creditUtils.getCardNameByCardno(ft.getCARDNAME3()),1);
							card3 = JSPUtils.convertFullorHalf(commonPools.creditUtils
									.getCardNameByCardno(result.getValueByFieldName("CARDNAME3", n + 1)), 1);// update
																												// 20181031
							for (int m = 0; m < 10 - (card3.trim().length()) * 2; m++) {
								space += " ";
							}
							card3 += space;
							space = "";
							for (int m = 0; m < 66; m++) {
								space += " ";
							}
							beforepadding += space;
							space = "";
							for (int m = 0; m < 50; m++) {
								space += " ";
							}
							f = beforepadding + card3 + afterpadding + "\n";
							out.write(f);
							okcnt++;
							totalcnt++;
							counter++;
							log.warn(ESAPIUtil.vaildLog("Export CARDNAME3 Content = " + f + " counter:" + counter));
						}
						tempcounter++;
						if (counter % 45 == 0)
							break;
					}
					log.warn("counter:" + counter + " tempcounter:" + tempcounter);
					if ((totalreccount % 45) != 0 && (totalcnt == totalreccount)) {
						int fill_space = 45 - (totalreccount % 45);
						for (int k = 0; k < fill_space; k++) {
							out.write("\n");
						}
					}
					if (counter % 45 == 0)
						break;
				}
				out.write(pageFooter);
			}
		}
		try {
			out.flush();
			out.close();

			if ("Y".equals(doFTP)) {
				// FTP機制啟動拋至報表系統主機
				FtpPathProperties nrcpro = new FtpPathProperties();
				String srcpath = nrcpro.getSrcPath("nnbreport_d903");
				log.warn(ESAPIUtil.vaildLog("srcpath = " + srcpath));
				String despath2 = nrcpro.getDesPath("nnbreport_d903");
				log.warn(ESAPIUtil.vaildLog("despath1 = " + despath2));
				String ipn = reportn_ftp_ip;
				FtpBase64 ftpBase64 = new FtpBase64(ipn);
				int rc = ftpBase64.upload(despath2, path + "/NBCCD903");
				// int rc=ftpBase64.upload(despath2, Path.dataFolder + "/NBCCD903");
				if (rc != 0) {
					log.warn("FTP 失敗(fundreport 新主機)");
					CheckServerHealth checkserverhealth = (CheckServerHealth) SpringBeanFactory
							.getBean("checkserverhealth");
					checkserverhealth.sendUnknowNotice("FTP 失敗(fundreport 新主機)");
					log.warn("FTPSTATUS失敗");
				} else {
					log.warn("FTPSTATUS成功");
				}
			}
		} catch (Exception e) {
		}
		try {
			fos.close();
		} catch (Exception e) {
		}

		if (okcnt == 0) {
			rowcnt.put("OKCNT", okcnt);
			rowcnt.put("FAILCNT", failcnt);
			rowcnt.put("TOTALCNT", totalcnt);
			return;
		}
		rowcnt.put("OKCNT", okcnt);
		rowcnt.put("FAILCNT", failcnt);
		rowcnt.put("TOTALCNT", totalcnt);
	}

	public String wordswitchECERT(String ECERT) {
		// NA03.ECERT.#=
		// NA03.ECERT.1=晶片金融卡
		// NA03.ECERT.2=電子簽章
		// NA03.ECERT.3=它行信用卡
		// NA03.ECERT.4=自然人憑證
		switch (ECERT) {
		case "#":
			ECERT = "";
			break;
		case "1":
			ECERT = "晶片金融卡";
			break;
		case "2":
			ECERT = "電子簽章";
			break;
		case "3":
			ECERT = "它行信用卡";
			break;
		case "4":
			ECERT = "自然人憑證";
			break;
		}
		return ECERT;
	}

	public String wordswitchCHANGE_O(String str) {
		switch (str) {
		case "#":
			str = "　";
			break;
		case "1":
			str = "是";
			break;
		case "2":
			str = "否";
			break;
		}
		return str;
	}
}
