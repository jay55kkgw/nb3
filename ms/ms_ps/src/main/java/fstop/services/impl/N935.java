package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class N935 extends CommonService  {
	@Autowired
	@Qualifier("n935Telcomm")
	private TelCommExec n935Telcomm;
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public MVH doAction(Map params) { 

		//查詢
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);
		
		log.debug("N935 System Out test");
		
		TelcommResult telcommResult = n935Telcomm.query(params);
		telcommResult.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		telcommResult.getFlatValues().put("CMRECNUM", telcommResult.getValueOccurs("ACN") + "");
		
		return telcommResult;
	}


}
