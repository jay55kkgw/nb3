package fstop.services.impl;

import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.services.CommonService;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class XLSTXT extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());
	
	@Override
	public MVH doAction(Map params) {		
		MVHImpl result = new MVHImpl();
		String content = StrUtils.trim((String)params.get("CONTENT"));
		result.getFlatValues().put("content", content);
		return result;
	}
}