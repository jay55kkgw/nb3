package fstop.services.impl;

import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.util.ESAPIUtil;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.SpringBeanFactory;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N9422 extends CommonService {
	@Autowired
    @Qualifier("n9422Telcomm")
	private TelCommExec n9422Telcomm;
//	private Logger logger = Logger.getLogger(getClass());
	
	
	@Override
	public MVH doAction(Map params) {
//		Hashtable<String, String> params = params;				
		
		log.debug(ESAPIUtil.vaildLog("CMPASSWORD===>"+params.get("CMPASSWORD")));
		
//		TODO:pinnew先寫死，之後再做修改
//		String pinnew = com.netbank.util.DigestUtils.sha1(params.get("CMPASSWORD"), "Cp1047");
		String pinnew = "6BBAA92BBEDAE8B4891BE417C5A66F44B09D2CB9";
		
		log.debug("pinnew===>"+pinnew);
		params.put("PINNEW", pinnew);
		
		TelCommExec n951CMTelcomm;
		n951CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n951CMTelcomm");
		
		TelcommResult n950Result = n951CMTelcomm.query(params);
		
		return n950Result;
	}
}
