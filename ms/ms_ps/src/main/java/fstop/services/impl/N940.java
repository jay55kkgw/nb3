package fstop.services.impl;

import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import fstop.model.MVH;
import fstop.model.MVHUtils;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N940 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("n940Telcomm")
	private TelCommExec n940Telcomm;
	
//	@Required
//	public void setN940Telcomm(TelCommExec telcomm) {
//		n940Telcomm = telcomm;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) {
		params.put("DATE", DateTimeUtils.getCDateShort(new Date()));
		params.put("TIME", DateTimeUtils.format("HHmmss", new Date()));
		TelcommResult telcommResult = n940Telcomm.query(params);
		
		telcommResult.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));


		return telcommResult;
	}


}
