package fstop.services.impl;

import java.util.Date;
import java.util.Hashtable;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import fstop.model.IMasterValuesHelper;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.MVHUtils;
import fstop.model.RowsGroup;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N841 extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("n841Telcomm")
	private TelCommExec n841Telcomm;
	 
//	@Required
//	public void setN841Telcomm(TelCommExec telcomm) {
//		n841Telcomm = telcomm;
//	}
	
	@Override	
	public MVH doAction(Map params) {    
		Map<String, String> p = params;

		if("N8411".equals(p.get("trancode")))
			params.put("LOSTYPE", "011");
		
		if("N8412".equals(p.get("trancode")))
			params.put("LOSTYPE", "041");
		
		TelcommResult helper = n841Telcomm.query(params);
		helper.getFlatValues().put("CMQTIME",
				DateTimeUtils.getDatetime(new Date()));
		
		Map<String, IMasterValuesHelper> mresult = MVHUtils.groupByToTables(new MVHImpl(helper.getOccurs()), new String[]{"ACN"});
		int i = 1;
		for(Entry<String, IMasterValuesHelper> ent : mresult.entrySet()) {
			helper.addTable(ent.getValue(), "table_" + i);
			i++;
		}
		
		
		return helper;
	}
}
