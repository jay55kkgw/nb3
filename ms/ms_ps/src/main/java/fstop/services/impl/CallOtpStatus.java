package fstop.services.impl;

import org.apache.log4j.Logger;
import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.services.CommonService;
import fstop.services.ServerConfig;
import fstop.util.IPUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import ni.otp.Proc00F2A;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.util.ESAPIUtil;

import java.io.File;
import java.util.Map;
import java.util.Properties;


/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class CallOtpStatus extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());
	
//	private static String strServer="10.16.22.73";
//    private static String strPort="1911";	
	 private String setting;	
	  //讀取OTP IP、OTP PORT設定檔
	    public Properties loadPropertyFromFile() {
	    	String localIP = IPUtils.getLocalIp();
	    	String otppro=File.separator +"WEB-INF" + File.separator + "otpSettingT.properties";
	    	Properties p = new Properties();
	    	ServerConfig serverConfig = (ServerConfig)SpringBeanFactory.getBean("serverConfig");
			    	
	    	try {           	        	
	        	
	    		if(localIP.equals("10.16.22.17") || StrUtils.left(localIP, 10).equals("10.10.215."))
	    			otppro=File.separator +"WEB-INF" + File.separator + "otpSettingT.properties";
	    		else
	    			otppro=File.separator +"WEB-INF" + File.separator + "otpSetting.properties";
	    		p.load(serverConfig.getgetResourceAsStream(otppro));			
	        
	    	}catch (Exception e) {
	        	log.debug("e=======>"+e.toString());
	        }
	    	    	    	
	    	return p;    	
	    }       
	/**
	 * 
	 * @param params
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public MVH doAction(Map _params) {
		String otpIp = null;
		String otpPort = null;
		Map<String, String> params = _params;
		MVHImpl result = new MVHImpl();
		//若OTP有問題則把之相關程式mark起來:b203_1.jsp、CallOtpStatus.java、ApplicationContext-service.xml、CommonPools.java
		try{
			Properties p = this.loadPropertyFromFile();
	        
			otpIp = p.getProperty("OTPIP");
			otpPort = p.getProperty("OTPPORT");
			String [] cardId = new Proc00F2A().response((String)params.get("CUSIDN"),otpIp , otpPort);
			
			// CGI Stored XSS
//			for(int i=0; i < cardId.length;i++){
//				log.debug("cardId==============>"+cardId[i]);
//			}
			
			String returnCode = cardId[0];
			if ( returnCode.length()!= 8  )
	        {			
	            throw TopMessageException.create(returnCode);
	        }
			
			result.getFlatValues().put("MsgName", "正常:"+returnCode);
			
		}catch (TopMessageException top) {

			TopMessageException top1 = TopMessageException.create(top.getMsgcode());			
			result.getFlatValues().put("MsgName", top.getMsgout());			
			log.debug("CallOtpStatus TopMessageException==" + top);
			
			// CGI Stored XSS
//			log.debug("CallOtpStatus Msgcode==" + top1.getMsgcode());
			
			log.debug(ESAPIUtil.vaildLog("CallOtpStatus Msgout==" + top1.getMsgout()));
			log.debug(ESAPIUtil.vaildLog("CallOtpStatus TopMessageException==" + top));
			log.debug(ESAPIUtil.vaildLog("CallOtpStatus Msgcode==" + top1.getMsgcode()));
			log.debug(ESAPIUtil.vaildLog("CallOtpStatus Msgout==" + top1.getMsgout()));
			
		} catch (Exception e) {
			log.error("CallOtpStatus Exception==" + e);
			
			result.getFlatValues().put("MsgName", "系統異常");
		}
		
		return result;	
	}

}
