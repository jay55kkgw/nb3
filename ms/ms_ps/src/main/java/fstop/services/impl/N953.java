package fstop.services.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import topibs.utility.NumericUtil;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.orm.dao.TxnReqInfoDao;
import fstop.orm.dao.TxnTwRecordDao;
import fstop.orm.po.ADMMAILLOG;
import fstop.orm.po.TXNREQINFO;
import fstop.orm.po.TXNTWRECORD;
import fstop.services.BookingUtils;
import fstop.services.CommonService;
import fstop.services.DispatchService;
import fstop.services.NoticeInfo;
import fstop.services.NoticeInfoGetter;
import fstop.services.OnLineTransferNotice;
import fstop.services.ServiceBindingException;
import fstop.services.TelCommExecWrapper;
import fstop.services.WriteLogInterface;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N953 extends DispatchService {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("n953Telcomm")
	private TelCommExec n953Telcomm;
	
	@Override
	public String getActionMethod(Map<String, String> params) {
//		String txid = StrUtils.trim(params.get("TXID"));
//		log.debug("XXXX txid: " + txid);
//		if("N953".equals(txid))
			return "doN953";
//		else 
//			throw new ServiceBindingException("不支援的 TXID [" + txid+"].");
	}		

//	@Required
//	public void setN953Telcomm(TelCommExec telcomm) {
//		n953Telcomm = telcomm;
//	}
	
	@SuppressWarnings("unchecked")
	public MVH doN953(Map params) { 
		
		Date d = new Date();
		String currentDate = DateTimeUtils.format("yyyyMMdd", d);
		int currentTwYear = Integer.parseInt(currentDate.substring(0, 4)) - 1911;
		currentDate = currentTwYear + currentDate.substring(4, 8);
		String currentTime = DateTimeUtils.format("HHmmss", d);
		String UID = params.get("UID")==null ? "" : params.get("UID").toString();
		log.debug("N953.java UID:"+UID);
		params.put("DATE", currentDate);
		params.put("TIME", currentTime);
		
		log.debug("<====================================================== Prepare to call n953Telcomm ======================================================>");
		
		TelcommResult result = n953Telcomm.query(params);
		if(UID.length()>0)
			result.getFlatValues().put("UID", UID);
		return result;
	}

}
