package fstop.services.impl;

import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.model.DBResult;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.orm.dao.TxnAddressBookDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.oao.Old_TxnAddressBookDao;
import fstop.orm.old.OLD_TXNADDRESSBOOK;
import fstop.orm.po.TXNADDRESSBOOK;
import fstop.orm.po.TXNUSER;
import fstop.services.CommonService;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;
@Slf4j
public class N930EA extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	private TxnUserDao txnUserDao;

	@Autowired
	private TxnAddressBookDao txnAddressBookDao;
	
	@Value("${isSyncNNB}")
	private String  isSyncNNB;
	@Autowired
	Old_TxnAddressBookDao old_txnaddressbookdao;
	
	@Override
	public MVH doAction(Map _params) {
		String tmp_DPUSERID="";
		String tmp_DPABMAIL="";
		String tmp_DPGONAME="";
		Integer tmp_DPADDBKID=null;
		OLD_TXNADDRESSBOOK po = null;
		Map<String, String> params = _params;
		log.info("isSyncNNB>>{}",isSyncNNB); 
		MVHImpl result = new MVHImpl();
		if("QUERY".equalsIgnoreCase(params.get("EXECUTEFUNCTION"))) {
			String dpuserid = params.get("DPUSERID");
			TXNUSER txnUser = txnUserDao.findById(dpuserid);
			result.getFlatValues().put("DPMYEMAIL", txnUser.getDPMYEMAIL());
			List<TXNADDRESSBOOK> addrBook = txnAddressBookDao.findByDPUserID(dpuserid);
			if(addrBook != null) {
				DBResult dbresult = new DBResult(addrBook);
				dbresult.getFlatValues().put("DPMYEMAIL", txnUser.getDPMYEMAIL());
				return dbresult;
			}
		}

		if("UPDATE".equalsIgnoreCase(params.get("EXECUTEFUNCTION"))) {
			String dpaddbkid = params.get("DPADDBKID");
			TXNADDRESSBOOK addrBook = txnAddressBookDao.findById(new Integer(dpaddbkid));
			
			tmp_DPUSERID = addrBook.getDPUSERID();
			tmp_DPGONAME = addrBook.getDPGONAME();
			tmp_DPABMAIL = addrBook.getDPABMAIL();
			
			addrBook.setDPABMAIL(params.get("DPABMAIL"));
			addrBook.setDPGONAME(params.get("DPGONAME"));
			txnAddressBookDao.save(addrBook);
			result.getFlatValues().put("DPABMAIL", addrBook.getDPABMAIL());
			result.getFlatValues().put("DPGONAME", addrBook.getDPGONAME());
			
			
			try {
				log.debug("tmp_DPUSERID>>{}, tmp_DPGONAME>>{}, tmp_DPABMAIL>>{}",tmp_DPUSERID, tmp_DPGONAME, tmp_DPABMAIL);
				if("Y".equals(isSyncNNB)) {
					List<OLD_TXNADDRESSBOOK> list = old_txnaddressbookdao.findByInput(tmp_DPUSERID, tmp_DPGONAME, tmp_DPABMAIL);
					if(list !=null && !list.isEmpty()) {
						po = list.get(0);
						tmp_DPADDBKID = po.getDPADDBKID();
						po = CodeUtil.objectCovert(OLD_TXNADDRESSBOOK.class, addrBook);
						po.setDPADDBKID(tmp_DPADDBKID);
						old_txnaddressbookdao.update(po);
					}else {
						po = CodeUtil.objectCovert(OLD_TXNADDRESSBOOK.class, addrBook);
						po.setDPADDBKID(null);
						old_txnaddressbookdao.save(po);
					}
				}
			} catch (Exception e) {
				log.error("UPDATE OLD_TXNADDRESSBOOK ERROR ...{}",e);
			}
		}
		if("DELETE".equalsIgnoreCase(params.get("EXECUTEFUNCTION"))) {
			String dpaddbkid = params.get("DPADDBKID");
			
			TXNADDRESSBOOK addrBook = txnAddressBookDao.findById(new Integer(dpaddbkid));
			txnAddressBookDao.delete(addrBook);
			
			//TODO: 將removeById 改成 delete 加 findById
//			txnAddressBookDao.delete(txnAddressBookDao.findById(new Integer(dpaddbkid)));
			
			try {
				tmp_DPUSERID = addrBook.getDPUSERID();
				tmp_DPGONAME = addrBook.getDPGONAME();
				tmp_DPABMAIL = addrBook.getDPABMAIL();
				if("Y".equals(isSyncNNB)) {
					List<OLD_TXNADDRESSBOOK> list = old_txnaddressbookdao.findByInput(tmp_DPUSERID, tmp_DPGONAME, tmp_DPABMAIL);
					if(list !=null && !list.isEmpty()) {
						po = list.get(0);
						old_txnaddressbookdao.delete(po);
					}else {
						log.warn("OLD_TXNADDRESSBOOK is null no delete >>{} ,{} ,{} " , tmp_DPUSERID , tmp_DPGONAME,tmp_DPABMAIL );
					}
				}
			} catch (Exception e) {
				log.error("DELETE OLD_TXNADDRESSBOOK ERROR ...{}",e);
			}
			
		}
		if("INSERT".equalsIgnoreCase(params.get("EXECUTEFUNCTION"))) {
			String dpaddbkid = params.get("DPADDBKID");
			TXNADDRESSBOOK newAddrBook = new TXNADDRESSBOOK();

			newAddrBook.setDPUSERID(params.get("DPUSERID"));
			newAddrBook.setDPGONAME(params.get("DPGONAME"));
			newAddrBook.setDPABMAIL(params.get("DPABMAIL"));
			Date d = new Date();
			newAddrBook.setLASTDATE(DateTimeUtils.format("yyyyMMdd", d));
			newAddrBook.setLASTTIME(DateTimeUtils.format("HHmmss", d));

			txnAddressBookDao.save(newAddrBook);
			
			result.getFlatValues().put("DPUSERID", newAddrBook.getDPUSERID());
			result.getFlatValues().put("DPADDBKID", newAddrBook.getDPADDBKID().toString());
			result.getFlatValues().put("DPGONAME", newAddrBook.getDPGONAME());
			result.getFlatValues().put("DPABMAIL", newAddrBook.getDPABMAIL());
			
			try {
				tmp_DPUSERID = newAddrBook.getDPUSERID();
				tmp_DPGONAME = newAddrBook.getDPGONAME();
				tmp_DPABMAIL = newAddrBook.getDPABMAIL();
				if("Y".equals(isSyncNNB)) {
					List<OLD_TXNADDRESSBOOK> list = old_txnaddressbookdao.findByInput(tmp_DPUSERID, tmp_DPGONAME, tmp_DPABMAIL);
					if(list !=null && !list.isEmpty()) {
						po = list.get(0);
						log.warn(ESAPIUtil.vaildLog("has same data  nothing to do >> "+ CodeUtil.toJson(po)));
//						tmp_DPADDBKID = po.getDPADDBKID();
//						po = CodeUtil.objectCovert(OLD_TXNADDRESSBOOK.class, newAddrBook);
//						po.setDPADDBKID(tmp_DPADDBKID);
//						old_txnaddressbookdao.update(po);
					}else {
						po = CodeUtil.objectCovert(OLD_TXNADDRESSBOOK.class, newAddrBook);
						po.setDPADDBKID(null);
						old_txnaddressbookdao.save(po);
					}
				}
			} catch (Throwable e) {
				log.error("INSERT OLD_TXNADDRESSBOOK ERROR ...{}",e);
			}
		}
		return result;
	}
}
