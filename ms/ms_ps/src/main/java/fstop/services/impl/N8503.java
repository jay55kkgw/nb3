package fstop.services.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.util.CodeUtil;
import com.netbank.util.DateUtils;
import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.EachRowCallback;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.model.RowsSelecter;
import fstop.model.TelcommResult;
import fstop.orm.dao.TxnLogDao;
import fstop.orm.po.TXNLOG;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import fstop.util.StrUtils;
import javassist.expr.Instanceof;
import lombok.extern.slf4j.Slf4j;
@Slf4j
public class N8503 extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());
	
//	private TelCommExec n8503Telcomm;
//	 
//	private TelCommExec n841Telcomm;

	@Autowired
	@Qualifier("n8503Telcomm")
	private TelCommExec n8503Telcomm;
//	@Required
//	public void setN8503Telcomm(TelCommExec telcomm) {
//		n8503Telcomm = telcomm;
//	}
	
	@Autowired
	@Qualifier("n841Telcomm")
	private TelCommExec n841Telcomm;
//	@Required
//	public void setN841Telcomm(TelCommExec telcomm) {
//		n841Telcomm = telcomm;
//	}
	
	@Autowired
	private TxnLogDao txnlogdao;
	
	@Override	
	public MVH doAction(Map params) {

		MVHImpl helper = new MVHImpl();
		//將傳入的字串切割組成陣列
		log.trace(ESAPIUtil.vaildLog("GET ArrayParam : " + params.get("ArrayParam").toString()));
		String newparams = params.get("ArrayParam").toString();
		String[] arrayParams = newparams.split(";,");
		DateTime d1 = null;
		DateTime d2 = null;
		String pstimediff="";
		
//		String[] arrayParams = (String[])params.get("ArrayParam");
		Map cloneParams = new HashMap<>();
		cloneParams.putAll(params);
		if(arrayParams != null) {
			for(String p : arrayParams) {
				log.trace(ESAPIUtil.vaildLog("申請掛失 do : " + p)); //{}
//				Hashtable cloneParams = (Hashtable)params.clone();
				Map jb = JSONUtils.json2map(p);
				cloneParams.putAll(jb);
				Date d = new Date();
				cloneParams.put("DATE", DateTimeUtils.getCDateShort(d));
				cloneParams.put("TIME", DateTimeUtils.format("HHmmss", d));
				//TelcommResult tr = n8503Telcomm.query(cloneParams);
				MVHImpl tr = null;
				try {
					d1 = new DateTime();
					tr = n8503Telcomm.query(cloneParams);
					tr.getFlatValues().put("STATUS", "掛失成功");
				}
				catch(TopMessageException e) {
					tr = new MVHImpl();
//					log.trace("getMsgout>>{}",((TopMessageException) e.getCause()).getMsgout());
//					TODO 舊的方式會拿不到 之後跟JIMMY討論
//					tr.getFlatValues().put("STATUS", ((TopMessageException) e.getCause()).getMsgout());
					tr.getFlatValues().put("TOPMSG", e.getMsgcode());
					tr.getFlatValues().put("STATUS", e.getMsgout());
//					throw e;
				}
				finally {
					d2 = new DateTime();
					pstimediff = DateUtils.getDiffTimeMills(d1, d2);
					params.put("PSTIMEDIFF", pstimediff);
					tr.getFlatValues().putAll(jb);
					//寫TXNLOG
					try {
						txnlogdao.writeTxnLog(params,tr);
					}
					catch (Exception e) {
						log.error(ESAPIUtil.vaildLog("writeTxnLog ERROR >>>" + e));
					}
				}
				
//				helper.getFlatValues().put("TOPMSG", tr.getFlatValues().get("TOPMSG"));
				helper.getFlatValues().put("TOPMSG", "0");
				helper.getOccurs().addRow(new Row(tr.getFlatValues()));
			}
		}
		
		helper.getFlatValues().put("CMQTIME",
				DateTimeUtils.getDatetime(new Date()));
		
		return helper;
	}
}
