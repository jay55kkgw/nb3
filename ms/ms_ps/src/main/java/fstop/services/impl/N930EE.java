package fstop.services.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

import com.netbank.rest.service.Verify_mail_resend_Tel_Service;
import com.netbank.rest.service.Verify_mail_send_Tel_Service;
import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.notifier.NotifyAngent;
import fstop.orm.dao.AdmEmpInfoDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.dao.VerifyMailDao;
import fstop.orm.dao.VerifyMail_His_Dao;
import fstop.orm.po.TXNUSER;
import fstop.orm.po.VERIFYMAIL;
import fstop.orm.po.VERIFYMAIL_HIS;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.telcomm.cmd.CommandUtils;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N930EE extends CommonService {
	// private Logger logger = Logger.getLogger(getClass());

	@Autowired
	private TxnUserDao txnUserDao;
	@Autowired
	@Qualifier("n930Telcomm")
	private TelCommExec n930Telcomm; // 用戶電子郵箱位址變更
	@Autowired
	@Qualifier("n931Telcomm")
	private TelCommExec n931Telcomm; // 申請、取消電子交易對帳單
	@Autowired
	@Qualifier("c113Telcomm")
	private TelCommExec c113Telcomm;
	@Autowired
	@Qualifier("c015Telcomm")
	private TelCommExec c015Telcomm;
	@Autowired
	@Qualifier("n922Telcomm")
	private TelCommExec n922Telcomm;
	@Autowired
	private AdmEmpInfoDao admEmpInfoDao;

	@Autowired
	Verify_mail_send_Tel_Service verify_mail_send_Tel_Service;
	
	
	@Autowired
	Verify_mail_resend_Tel_Service verify_mail_resend_Tel_Service;

	@Autowired
	private VerifyMailDao verifyMailDao;

	@Autowired
	private VerifyMail_His_Dao verifyMail_His_Dao;

	@Autowired
	@Qualifier("n960Telcomm")
	private TelCommExec n960Telcomm;
	
	//方便測試用  跳過判斷行員
	@Value("${ms_env}")
	private String ms_env;
	

	@Override
	public MVH doAction(Map _params) {
		Map<String, String> params = _params;

		MVHImpl result = new MVHImpl();
		String EMAIL = "";
		if ("QUERY".equalsIgnoreCase(params.get("EXECUTEFUNCTION"))) {
			// 查DB
			String dpuserid = params.get("DPUSERID");
			TXNUSER txnUser = txnUserDao.findById(dpuserid);
			result.getFlatValues().put("DPMYEMAIL", txnUser.getDPMYEMAIL().toLowerCase());
			result.getFlatValues().put("TOPMSG", "0");
			result.getFlatValues().put("MSGCOD", "0");
		}

		if ("N930QUERY".equalsIgnoreCase(params.get("EXECUTEFUNCTION"))) {
			// N930 FLAG=01
			result = (MVHImpl) n930Telcomm.query(params);// 查詢網銀電子帳單申請狀態
		}

		if ("RESEND".equalsIgnoreCase(params.get("EXECUTEFUNCTION"))) {
			verify_mail_resend_Tel_Service.process("verify_mail_resend", params);
		}

		if ("UPDATE_TX".equalsIgnoreCase(params.get("EXECUTEFUNCTION"))) {
			// N930 FLAG= 02,03,04 06/07
			
			//2021/12/22新增
			params.put("CHECK", "Y");
			
			String flag = (String) params.get("FLAG");

			String UID = (String) params.get("CUSIDN");
			String userid = "";
			if (params.get("DPUSERID") != null) {
				userid = params.get("DPUSERID");
			}
			if (!(UID.equals(userid))) {
				throw TopMessageException.create("Z627");
			}

			EMAIL = (String) params.get("NEW_EMAIL").trim().toLowerCase();

			// 2019/10/16 更新 IKey驗證
			//網銀的IKey
			if ("1".equals(params.get("FGTXWAY"))) {
				flag = "03";
				params.put("FLAG", "03");
				if (params.containsKey("pkcs7Sign")) {
					List<String> commands = new ArrayList();
					commands.add("XMLCN()");

					CommandUtils.doBefore(commands, params);
				}
			// 網銀的蓋特是7 , 行銀的蓋特是5
			} else if ("7".equals(params.get("FGTXWAY"))||"5".equals(params.get("FGTXWAY"))) {
				flag = "04";
				params.put("FLAG", "04");
				params.put("CERTACN", StrUtils.repeat("C", 19));
			//網銀的晶片金融卡
			} else if ("2".equals(params.get("FGTXWAY"))) {
				flag = "02";
				params.put("FLAG", "02");
			//行銀的隨護是4 , 先用憑證的FLAG=03 
			}else if ("4".equals(params.get("FGTXWAY"))) {
				flag = "03";
				params.put("FLAG", "03");
			}
			
			
			log.info("UPDATE_TX CUSIDN : {} , FLAG: {} , ", UID ,flag );
			
			if("P".equals(ms_env)) {
				if ("mail.tbb.com.tw".toUpperCase().equals(EMAIL.substring(EMAIL.indexOf("@") + 1).toUpperCase())) {
					if (!admEmpInfoDao.isEmployeeAndEmpMail(UID, EMAIL)) { // 是否為行員信箱
						throw TopMessageException.create("Z625");
					}
				}
			}

			params.put("E_MAIL_ADR", EMAIL);

			MVHImpl res930 = new MVHImpl();

			res930 = n930Telcomm.query(params);// 先發N930更新用戶電子郵箱，如有錯誤會導到topException頁面去
			
			log.info("N930 PAR1 res: {}" , res930 );
			
			// 02,03,04 如果是 R002就是沒有重覆 , 要繼續做下面的CODE
			// 02,03,04 如果是 R001就是有重覆 , 不做下面的CODE
			switch (flag) {
			case "02":
			case "03":
			case "04":
				
				if ("R002".equals(res930.getFlatValues().get("TOPMSG"))) {
					res930.getFlatValues().put("TOPMSG", "");
					log.info("FLAG 02,03,04 N930 return R002 >> PASS");
				}

				// 如果是 02,03,04 出R001 代表重覆 , 這邊打完0234後打05然後寄MAIL然後回傳不做接下來的CODE
				if ("R001".equals(res930.getFlatValues().get("TOPMSG"))) {
					
					log.info("FLAG 02,03,04 N930 return R001 >> PREPARE TO SEND VERIFY MAIL");
					
					String BRHCOD = res930.getFlatValues().get("BRHCOD");
					String TLRNUM = res930.getFlatValues().get("TLRNUM");
					String TRNNUM = res930.getFlatValues().get("TRNNUM");
					String RDATE = res930.getFlatValues().get("RDATE");

					params.put("FLAG", "05");
					res930 = n930Telcomm.query(params);
					
					log.info("N930 PART2 (FLAG 05) res: {}" , res930 );
					
					MVHImpl res960 = new MVHImpl();
					res960 = n960Telcomm.query(params);

					Map<String, String> apiParam = new HashMap<String, String>();

					apiParam.put("BRHCOD", BRHCOD);
					apiParam.put("TLRNUM", TLRNUM);
					apiParam.put("TRNNUM", TRNNUM);
					apiParam.put("CUSIDN", UID);
					apiParam.put("O_MAILADDR", StrUtils.isNotEmpty(res960.getFlatValues().get("MAILADDR"))?res960.getFlatValues().get("MAILADDR").toLowerCase():"");
					apiParam.put("N_MAILADDR", res930.getFlatValues().get("E_MAIL_ADR").toLowerCase());
					apiParam.put("RDATE", RDATE);
					apiParam.put("CHANNEL", params.get("LOGINTYPE"));
					
					HashMap rtnMap = verify_mail_send_Tel_Service.process("verify_mail_send", apiParam);
					
					log.info("VERIFY_MAIL_SEND_TEL_SERVICE RTNMAP : {}" , rtnMap);
					
					if ("0".equals((String) rtnMap.get("msgCode"))) {
						result.getFlatValues().put("TOPMSG", "0");
						result.getFlatValues().put("MSGCOD", "0");
						result.getFlatValues().put("DPMYEMAIL", (String) params.get("NEW_EMAIL"));
					}
					return result;
				}
				break;
			case "06": //驗證EMAIL
				
				//如果打06 N930 錯誤  要先return 防止更新DB
				if (!"".equals(res930.getFlatValues().get("TOPMSG"))) {
					log.error("N930 FLAG 06 error : {}" , res930.getFlatValues().get("TOPMSG") );
					return res930 ;
				}
				
				try {
					// 06成功後要更新主表及附表狀態
					VERIFYMAIL po = verifyMailDao.findById(UID);
					String ref_his = po.getREF_HISID();
					VERIFYMAIL_HIS po_his = verifyMail_His_Dao.findById(ref_his);
	
					po.setSTATUS("1");
					Calendar cal = Calendar.getInstance();
					po.setLASTDATE(DateTimeUtils.getDateShort(cal.getTime()));
					po.setLASTTIME(DateTimeUtils.getTimeShort(cal.getTime()));
	
					po_his.setSTATUS("1");
					po_his.setLASTDATE(DateTimeUtils.getDateShort(cal.getTime()));
					po_his.setLASTTIME(DateTimeUtils.getTimeShort(cal.getTime()));
	
					verifyMailDao.save(po);
					verifyMail_His_Dao.save(po_his);
					// 06 從網頁進來執行驗證 , 給個FGTXWAY跳過C113電文的PINNEW();
					//
					params.put("FGTXWAY", "M");
				}catch (Exception e) {
					//資料庫更新錯誤 , 退掉 ? 
					log.error("AFTER FLAG 06 N930 , DB ERROR : {}",e);
					result.getFlatValues().put("TOPMSG", "R808");
					result.getFlatValues().put("MSGCOD", "R808");
				}
				break;
			case "07":
				// 07 N930 查詢有沒有重覆 R001是重覆 R002是PASS要改狀態回傳出去
				if ("R002".equals(res930.getFlatValues().get("TOPMSG"))) {
					log.info("FLAG 07 PASS");
					res930.getFlatValues().put("TOPMSG", "");
					return res930;
				}
				log.info("FLAG 07 DOUBLE MAIL");
			}

			// 其餘錯誤代碼 直接回傳 不做下面基金的部分
			if (StrUtils.isNotEmpty(res930.getFlatValues().get("TOPMSG"))) {
				log.error("N930 ERROR : {}", res930.getFlatValues().get("TOPMSG"));
				return res930;
			}

			String dpuserid = params.get("DPUSERID");
			TXNUSER txnUser = txnUserDao.findById(dpuserid);

			if (null != txnUser) {
				log.info("THIS USER:{} IS NB3 USER ", dpuserid);
				txnUser.setDPMYEMAIL(params.get("NEW_EMAIL").toLowerCase());

				// 設定完Email變更後，判斷，如果該使用者在txnuser裡的通知欄位getDPNOTIFY是空的，應主動新增一筆通知內容
				log.debug(ESAPIUtil.vaildLog("NEW_EMAIL==@" + (String) params.get("NEW_EMAIL") + "@"));
				String notify = txnUser.getDPNOTIFY();
				log.debug(ESAPIUtil.vaildLog("notify==@" + notify + "@"));
				if (!StrUtils.isEmpty((String) params.get("NEW_EMAIL"))) {// 新的Email不是空的才要去增加DPNOTIFY的值
					if (StrUtils.isEmpty(notify.trim())) {
						txnUser.setDPNOTIFY("1,2,3,4,5,6,7,8,9,10,11,12,13,14,15");// 主動新增一筆通知內容為全部打勾(二階之後要再增加二階才有的通知內容)
					}
				} else {// 新的EMail是空的，就把通知訊息內容全部清空
					if (!StrUtils.isEmpty(notify.trim()))
						txnUser.setDPNOTIFY("");

					// 補上n931取消電子交易對帳單之參數 根據 N931.java 124~129 182~185 得出取消電子對帳單 缺少的欄位為ITEM=10
					// 景森提供TYPE=02 2019/04/15 BEN
					params.put("TYPE", "02");
					params.put("ITEM", "10");

					MVHImpl n931res = (MVHImpl) n931Telcomm.query(params);// 取消電子交易對帳單

				}

				Date d = new Date();
				String dateTime = DateTimeUtils.getCDateShort(d) + DateTimeUtils.getTimeShort(d);
				// 設定完Email變更後，如果EMail是空的，電子對帳單裡的通知如果是Y要改成取消("N")，如果是""就先不用改
				String dpbill = txnUser.getDPBILL(); // 電子對帳單Y.申請 N.沒申請
				String dpfundbill = txnUser.getDPFUNDBILL();// 基金對帳單Y.申請 N.沒申請
				String dpcardbill = txnUser.getDPCARDBILL();// 信用卡帳單Y.申請 N.沒申請
				log.debug(ESAPIUtil.vaildLog("dpbill==@" + dpbill + "@"));
				if (StrUtils.isEmpty((String) params.get("NEW_EMAIL"))) {// Email是空的才要去改DPBILL的值是Y的為N
					if (dpbill.trim().equals("Y")) {
						txnUser.setDPBILL("N");// 由Y->N
						txnUser.setBILLDATE(dateTime);// 最後電子對帳單狀態異動日
					}
					if (dpfundbill.trim().equals("Y")) {
						txnUser.setDPFUNDBILL("N");// 由Y->N
						txnUser.setFUNDBILLDATE(dateTime);// 最後基金對帳單狀態異動日
					}
					if (dpcardbill.trim().equals("Y")) {
						txnUser.setDPCARDBILL("N");// 由Y->N
						txnUser.setCARDBILLDATE(dateTime);// 最後信用卡帳單狀態異動日
					}
				}

				txnUser.setEMAILDATE(dateTime);
				dateTime = dateTime.substring(0, 3) + "/" + dateTime.substring(3, 5) + "/" + dateTime.substring(5, 7)
						+ "  " + dateTime.substring(7, 9) + ":" + dateTime.substring(9, 11) + ":"
						+ dateTime.substring(11, dateTime.length());
				txnUserDao.save(txnUser);
				params.put("TRANTIME", dateTime);
				if (StrUtils.isNotEmpty(txnUser.getDPMYEMAIL())) {
					boolean isSendSuccess = NotifyAngent.sendNotice("N930EE", params, txnUser.getDPMYEMAIL());
					if (!isSendSuccess) {
						result.getFlatValues().put("__MAILERR", "發送 Email 失敗.");
					}
				}
				// N930 N931 END
			}else {
				log.error("THIS USER:{} IS NOT NB3 USER ", dpuserid);
			}

			// N922 + C系列電文START
			try {
				params.put("CUSIDN", (String) params.get("UID"));
				MVHImpl n922res = (MVHImpl) n922Telcomm.query(params);// 查詢網銀電子帳單申請狀態

				if ("".equals(n922res.getFlatValues().get("TOPMSG"))) {

					params.put("MSGCOD", n922res.getValueByFieldName("MSGCOD"));
					params.put("ACN1", n922res.getValueByFieldName("ACN1"));
					params.put("ACN2", n922res.getValueByFieldName("ACN2"));
					params.put("ACN3", n922res.getValueByFieldName("ACN3"));
					params.put("ACN4", n922res.getValueByFieldName("ACN4"));
					params.put("APLBRH", n922res.getValueByFieldName("APLBRH"));
					params.put("CUTTYPE", n922res.getValueByFieldName("CUTTYPE"));
					params.put("NAME", n922res.getValueByFieldName("NAME"));
					params.put("ICCOD", n922res.getValueByFieldName("ICCOD"));
					params.put("EMPNO", n922res.getValueByFieldName("EMPNO"));
					params.put("FDINVTYPE", n922res.getValueByFieldName("FDINVTYPE"));
					params.put("GETLTD", n922res.getValueByFieldName("GETLTD"));
					params.put("DEGREE", n922res.getValueByFieldName("DEGREE"));
					params.put("CAREER", n922res.getValueByFieldName("CAREER"));
					params.put("SALARY", n922res.getValueByFieldName("SALARY"));
					params.put("BRTHDY", n922res.getValueByFieldName("BRTHDY"));
					params.put("MARK1", n922res.getValueByFieldName("MARK1"));
					params.put("RISK7", n922res.getValueByFieldName("RISK7"));
					params.put("MARK3", n922res.getValueByFieldName("MARK3"));
					params.put("GETLTD7", n922res.getValueByFieldName("GETLTD7"));
					params.put("TXNIDT", DateTimeUtils.format("yyMMddHHmmss", new Date()));
					params.put("BRHCOD", n922res.getValueByFieldName("APLBRH"));
					params.put("CMQTIME", DateTimeUtils.getDatetime(new Date()));

					// N922 成功才有需要打 C系列電文
					try {

						// C015 上行需要 CUSIDN ( params ) , BRHCOD ( N922 下行 228行有加入 params )
						TelcommResult c015Result = c015Telcomm.query(params);

						if (!StrUtils.isEmpty((String) params.get("NEW_EMAIL").toLowerCase())) {
							// 新MAIL 不為空 , 維持原來型態
							params.put("BILLSENDMODE", c015Result.getValueByFieldName("BILLSENDMODE"));
						} else {
							// 新MAIL 為空 , 修改成1
							params.put("BILLSENDMODE", "1");
						}

						// C113 需要欄位
						params.put("ZIPCODE", c015Result.getValueByFieldName("ZIPCODE"));

						params.put("HTELPHONE", c015Result.getValueByFieldName("HTELPHONE"));

						params.put("OTELPHONE", c015Result.getValueByFieldName("OTELPHONE"));

						params.put("MTELPHONE", c015Result.getValueByFieldName("MTELPHONE"));

						// params 裡的 NEW_EMAIL
						params.put("EMAIL", EMAIL);

						params.put("ADDRESS", c015Result.getValueByFieldName("ADDRESS"));

						params.put("CMPASSWORD", "");

						try {
							// 投資人基本資料變更
							MVHImpl C113res = (MVHImpl) c113Telcomm.query(params);
						} catch (TopMessageException e) {
							log.error("C113ERROR:" + e.getMsgcode());

						}
					} catch (TopMessageException e) {
						log.error("C015ERROR:" + e.getMsgcode());
					}
				}else {
					log.error("n922ERRORMSG:" + n922res.getFlatValues().get("TOPMSG"));
				}

			} catch (TopMessageException e) {
				log.error("n922ERRORMSG:" + e.getMsgcode());
			}
			log.debug("N922end");

		}

		// result put 0 成功
		result.getFlatValues().put("TOPMSG", "0");
		result.getFlatValues().put("MSGCOD", "0");
		result.getFlatValues().put("DPMYEMAIL", (String) params.get("NEW_EMAIL"));
		return result;
	}
}