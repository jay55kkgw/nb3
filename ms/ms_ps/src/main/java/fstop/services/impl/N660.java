	package fstop.services.impl;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jdom2.Document;
import org.jdom2.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.model.Rows;
import fstop.notifier.MessageTO;
import fstop.notifier.MessageTemplate;
import fstop.notifier.NotifyAngent;
import fstop.orm.dao.AdmCustTelnoDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.ADMCUSTTELNO;
import fstop.orm.po.TXNUSER;
import fstop.services.CommonService;
import fstop.services.batch.BatchResult;
import fstop.services.batch.ND10;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.FtpPathProperties;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N660 extends CommonService  {
	//private log log = log.getlog(getClass());
	
	@Autowired
	private TxnUserDao txnUserDao;
	@Autowired
	private AdmCustTelnoDao admCustTelnoDao;
	
	@Autowired
	@Qualifier("n951CMTelcomm")
	private TelCommExec n951CMTelcomm;
	
	@Autowired
	@Qualifier("n660Telcomm")
	private TelCommExec n660Telcomm;
	
//	private PathSetting pathSetting;
	
	@Autowired
	private ND10 nd10;
	
//	@Required
//	public void setTxnUserDao(TxnUserDao txnUserDao) {
//		this.txnUserDao = txnUserDao;
//	}
//
//	@Required
//	public void setAdmCustTelnoDao(AdmCustTelnoDao admCustTelnoDao) {
//		this.admCustTelnoDao = admCustTelnoDao;
//	}
//
//	@Required
//	public void setPathSetting(PathSetting pathSetting) {
//		this.pathSetting = pathSetting;
//	}
//
//	@Required
//	public void setNd10(ND10 nd10) {
//		this.nd10 = nd10;
//	}
//
//	@Required
//	public void setN660Telcomm(TelCommExec telcomm) {
//		n660Telcomm = telcomm;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public MVH doAction(Map params) {
		
		if (params.get("FGTXWAY") == null) {
			throw TopMessageException.create("S302", (String) params.get("ADOPID"));
		} else if ("0".equals(params.get("FGTXWAY"))) { // 使用交易密碼
			// 2019.05.15 ian 改為N951
			MVHImpl n951Result = n951CMTelcomm.query(params);
			// 若驗證失敗, 則會發動 TopMessageException
//			params.put("FGTXWAY", fgtxway);					
		}
		params.put("TXID", "N660");
		MVHImpl result = n660Telcomm.query(params);
//		
//		StringBuffer data = new StringBuffer();
//		for(Row r : result.getOccurs().getRows()) {
//			data.append(r.getValue("DATA")).append("\r\n");
//		}

		String name = "";
		List<TXNUSER> listtxnUser;
		TXNUSER user = null;
		try {
			log.debug(ESAPIUtil.vaildLog("CUSIDN = " + params.get("UID")));
			listtxnUser = txnUserDao.findByUserId((String)params.get("UID"));
			user = listtxnUser.get(0);
			name = user.getDPUSERNAME();
		}
		catch(Exception e){
			log.error(ESAPIUtil.vaildLog("無法取得 USER ID (" + params.get("UID") + ")," + e.getMessage()));
			name = "";
		}

		log.debug(ESAPIUtil.vaildLog("name = " + name));

//		MVHImpl tmpResult = new MVHImpl();
		result.getFlatValues().put("CUSIDN", (String)params.get("UID"));
		result.getFlatValues().put("ADDR", user.getDPMYEMAIL());
		
		result.getFlatValues().put("NAME", name);
		
		
//FTP區無法連通 20190626
		String uuid=UUID.randomUUID().toString();
		
		FtpPathProperties nrcpro=new FtpPathProperties();
		String srcpath=nrcpro.getSrcPath("n660xml").trim();
		log.debug(ESAPIUtil.vaildLog("srcpath = " + srcpath));
		String despath=nrcpro.getDesPath("n660xml1").trim();
		log.debug(ESAPIUtil.vaildLog("despath = " + despath));
		
		//Absolute Path Traversal
		String LFILENAME = srcpath + "trademsg_N660_" + uuid + ".xml";
		//
		
		File tmpFile = new File(LFILENAME);
		tmpFile.setWritable(true,true);
		tmpFile.setReadable(true, true);
		tmpFile.setExecutable(true,true);
		
		log.debug("N660 f.getPath() = " + uuid );
		try {
			genernateFile(tmpFile, result);		
			BatchResult batchresult = new BatchResult();
			batchresult=nd10.ftpToServer(tmpFile,"trademsg_N660_" + uuid,batchresult);
			if(!batchresult.isSuccess()&&!"ENRD".equals(batchresult.getErrorCode()))
			{
				result.getFlatValues().put("TOPMSG", "ZXXX FTP失敗");
				result.getFlatValues().put("__TOPMSG", "ZXXX");
				result.getFlatValues().put("__TOPMSGTEXT", "FTP失敗");
				log.debug("ZXXX FTP失敗 ");
				//檔案上傳失敗
				result.getFlatValues().put("ABEND","FE0007");
			}else if(!batchresult.isSuccess()&&"ENRD".equals(batchresult.getErrorCode())) {
				result.getFlatValues().put("TOPMSG", "ENRD");
				result.getFlatValues().put("ABEND","ENRD");
				log.debug("NO N660 DATA ");
			}
			result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		}
		catch(FileNotFoundException e) {
			log.error("FileNotFoundException !!!");
		}finally {
			//tmpFile.delete();
		}
		return result;
	}

	public void genernateFile(File f, MVHImpl result) {
		f.setWritable(true,true);
		f.setReadable(true, true);
		f.setExecutable(true,true);
		Row rootRow = new Row(result.getFlatValues());
		
		Pattern regex_time = Pattern.compile("(\\d\\d/\\d\\d\\s+\\d\\d:\\d\\d)", Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
//		Pattern regex = Pattern.compile("(\\d\\d/\\d\\d\\s+\\d\\d:\\d\\d)([^(]+)(\\(([^\\s]+?)\\)?)\\s*(.+)", Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
		Pattern regex = Pattern.compile("(\\d\\d/\\d\\d\\s\\s\\d\\d:\\d\\d)([^\\(]+)\\((.+)\\)", Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
		Pattern regex_nq = Pattern.compile("(\\d\\d/\\d\\d\\s\\s\\d\\d:\\d\\d)([^\\(]+)", Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);

		
		final Element rootElement = new Element("NoXML");
		final Document document = new Document(rootElement); 
		 
		Rows rows = result.getOccurs();
		
		if(rows.getSize() == 0) {
			log.info("無回傳結果, 不產生檔案.");
			return;
		}
		
		List<Row> rowsResult = new ArrayList();
		int start=1;
		Hashtable ADate = new Hashtable();
		for(int i = 0; i < rows.getSize(); i++) {
			Row r = rows.getRow(i);
			
			String data = StrUtils.trim(r.getValue("DATA"));
			String type = StrUtils.trim(r.getValue("TYPE"));
			log.debug(ESAPIUtil.vaildLog("data = " + data));
			log.debug(ESAPIUtil.vaildLog("type = " + type));
			if(regex_time.matcher(data).find()) {
				
				Matcher matcher = regex.matcher(data);
				Matcher matcher_nq = regex_nq.matcher(data);
				if(matcher.find()) {
/*
					String group0 = matcher.group(0);
					String time = matcher.group(1);
					log.debug("group time = " + time);
					String subtype = StrUtils.trim(matcher.group(4));
					log.debug("group subtype = " + subtype);
					String subtype_desc = StrUtils.trim(matcher.group(2));
					log.debug("group subtype_desc = " + subtype_desc);
					String realData = StrUtils.trim(matcher.group(5));
					log.debug("group realData = " + realData);
*/

					String group0 = matcher.group(0);
					log.debug(ESAPIUtil.vaildLog("group group0 = " + group0));
					String time = matcher.group(1);
					log.debug(ESAPIUtil.vaildLog("group time = " + time));
					String subtype = matcher.group(3);
					log.debug(ESAPIUtil.vaildLog("group subtype = " + subtype));
					String subtype_desc = matcher.group(2);
					log.debug(ESAPIUtil.vaildLog("group subtype_desc = " + subtype_desc));
					String realData = StrUtils.trim(data.substring(group0.length()+1));
					log.debug(ESAPIUtil.vaildLog("group realData = " + realData));

					String name = rootRow.getValue("NAME");

					Map<String, String> rm = new HashMap();
					rm.put("CUSIDN", r.getValue("CUSIDN"));
					rm.put("ADDR", r.getValue("ADDR"));
					rm.put("NAME", name);
					rm.put("TIME", time);
					ADate.put("TIME", time);
					rm.put("TYPE", type);
					rm.put("SUBTYPE", subtype);
					rm.put("SUBTYPE_DESC", subtype_desc);
					rm.put("DATA", realData);
					if(start==1)
					{
						String telno=null;
						Hashtable<String, String> params = new Hashtable();
						String cusidn=result.getFlatValues().get("CUSIDN");
						ADMCUSTTELNO custtelno = null;
//						custtelno = getTelnoByCust(cusidn);
						if(custtelno==null)
						{
							telno=" ";
						}
						else
						{
							telno=custtelno.getADMTELNO();
						}
						params.put("#TELNO", telno);
//						String text=getND10Safe(params);
//						log.debug("text = " + text);
						rm.put("TITLE", " ");
						start=0;
					}
					
					Row rr = new Row(rm);
					rowsResult.add(rr);
				}
				else if(matcher_nq.find())
				{
					log.debug(ESAPIUtil.vaildLog("groupCount = " + matcher_nq.groupCount()));
					String group0 = matcher_nq.group(0);
					log.debug(ESAPIUtil.vaildLog("group group0 = " + group0));
					String time = matcher_nq.group(1);
					log.debug(ESAPIUtil.vaildLog("group time = " + time));
					String realData = StrUtils.trim(data.substring(time.length()));
					log.debug(ESAPIUtil.vaildLog("group realData = " + realData));
					String[] totadesc = realData.split(" ",2);
					log.debug("totadesc.length = " + totadesc.length);
					for(int j=0;j<totadesc.length;j++)
					{
						log.debug("totadesc[" + j + "] = " + totadesc[j]);
					}
					Hashtable totadesctable=new Hashtable();
					convmemo(totadesc[0],totadesctable);

					String name = rootRow.getValue("NAME");

					Map<String, String> rm = new HashMap();
					rm.put("CUSIDN", r.getValue("CUSIDN"));
					rm.put("ADDR", r.getValue("ADDR"));
					rm.put("NAME", name);
					rm.put("TIME", time);
					ADate.put("TIME", time);
					rm.put("TYPE", type);
					rm.put("SUBTYPE", (String)totadesctable.get("MEMO"));
					rm.put("SUBTYPE_DESC", (String)totadesctable.get("MEMODESC"));
					rm.put("DATA", totadesc[1]);
					if(start==1)
					{
						String telno=null;
						Hashtable<String, String> params = new Hashtable();
						String cusidn=result.getFlatValues().get("CUSIDN");
						ADMCUSTTELNO custtelno = null;
//						custtelno = getTelnoByCust(cusidn);
						if(custtelno==null)
						{
							telno=" ";
						}
						else
						{
							telno=custtelno.getADMTELNO();
						}
						params.put("#TELNO", telno);
//						String text=getND10Safe(params);
//						log.debug("text = " + text);
						rm.put("TITLE", " ");
						start=0;
					}
					
					Row rr = new Row(rm);
					rowsResult.add(rr);
				}
				else {
					Map<String, String> rm = new HashMap();
					rm.put("DATA", data);
					if(start==1)
					{
						String telno=null;
						Hashtable<String, String> params = new Hashtable();
						String cusidn=result.getFlatValues().get("CUSIDN");
						ADMCUSTTELNO custtelno = null;
//						custtelno = getTelnoByCust(cusidn);
						if(custtelno==null)
							telno=" ";
						else
							telno=custtelno.getADMTELNO();
						params.put("#TELNO", telno);
//						String text=getND10Safe(params);
//						log.debug("text = " + text);
						rm.put("TITLE", " ");
						start=0;
					}
					
					Row rr = new Row(rm);
					rowsResult.add(rr);
				}
			}
			else {
				log.debug("The time isn't exist.");
				log.debug(ESAPIUtil.vaildLog("The time isn't exist. data = " + data));
				Map<String, String> rm = new HashMap();
				rm.put("DATA", data);
				if(start==1)
				{
					String telno=null;
					Hashtable<String, String> params = new Hashtable();
					String cusidn=result.getFlatValues().get("CUSIDN");
					log.debug("cusidn = " + cusidn);
					ADMCUSTTELNO custtelno = null;
//					custtelno = getTelnoByCust(cusidn);
					if(custtelno==null)
					{
						telno=" ";
					}
					else
					{
						telno=custtelno.getADMTELNO();
					}
					params.put("#TELNO", telno);
					String text=getND10Safe(params);
					log.debug("text = " + text);
					rm.put("TITLE", text);
					start=0;
				}
				
				Row rr = new Row(rm);
				rowsResult.add(rr);
			}
		}
		
		StringBuffer sb = new StringBuffer();
		genernateMessageElement(new Rows(rowsResult), sb);
		
		Element nomessage = new Element("NoMessage");
		log.debug("NoMessage = " + sb.toString());
		nomessage.setText(sb.toString());
		createNodeInfoElement(rootElement, nomessage, rootRow,ADate);

		nd10.OutputXML(document, f);
	}

	public void createNodeInfoElement(final Element rootElement, final Element nomessage, Row r, Hashtable AcDate) {

		 
		 Element noInfo = new Element("NoInfo");
		 
		 Element noflag = new Element("Noflag").setText("1");
		 noInfo.addContent(noflag);
		 
		 String cusidn = r.getValue("CUSIDN");
		 Element noid = new Element("NoID").setText(cusidn);
		 noInfo.addContent(noid);

		 Element noname = new Element("NoNAME").setText(r.getValue("NAME"));
		 noInfo.addContent(noname);

		 String gender = "";
		 try {
			 if(cusidn.length()==10)
			 {
				 gender = cusidn.substring(1,2);
				 if("1".equals(gender) || "A".equals(gender) || "C".equals(gender))
					 gender = "M";
				 else if("2".equals(gender) || "B".equals(gender) || "D".equals(gender))
					 gender = "F";
				 else
					 gender = "M";
			 }
			 else
			 {
				 gender = "C";
			 }
		 }
		 catch(Exception e){}
		 Element nosex = new Element("NoSEX").setText(gender);
		 noInfo.addContent(nosex);

		 String email = r.getValue("ADDR");
		 if("02".equals(r.getValue("TYPE"))) {
				try {
					TXNUSER user = txnUserDao.findById(cusidn);
					email = user.getDPMYEMAIL();
				}
				catch(Exception e){}
		 }
		 
		 Element noemail = new Element("NoEMAIL").setText(email);
		 noInfo.addContent(noemail);
		 
		 String date = new String();
		 String SYear = new String();
		 String time = (String)AcDate.get("TIME");
		 log.debug("TIME = " + time);
		 Pattern regex_time = Pattern.compile("(\\d\\d/\\d\\d)", Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
		 Matcher matcher = regex_time.matcher(time);
		 Element nodate=null;
		 if(matcher.find()) {
				String group0 = matcher.group(0);
				log.debug("group0 = " + group0);
				String grpdate = matcher.group(1);
				log.debug("grpdate = " + grpdate);
				String mm = grpdate.substring(0, 2);
				log.debug("mm = " + mm);
				
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
				String t = sdf.format(new Date());
				log.debug("t = " + t);
				int IYear=new Integer(t.substring(0,4));
				log.debug("IYear = " + IYear);
				int IMon=new Integer(t.substring(4,6));
				log.debug("IMon = " + IMon);
				if ( t.substring(4,6).equals("01"))
				{
					if(IMon < new Integer(mm))
					{
						IYear--;
					}
					
				}
				String t1 = "0000" + IYear;
				SYear= t1.substring(t1.length() - 4);
				log.debug("SYear + mm = " + SYear + mm);
				nodate = new Element("NoDATE").setText(SYear + mm);
		 }
		 else
			 nodate = new Element("NoDATE").setText("");
		 noInfo.addContent(nodate);

		 noInfo.addContent(nomessage);
		 
		 rootElement.addContent(noInfo);
	}

	private void genernateMessageElement(Rows rows, StringBuffer sb) {
		String n = "\r\n";
		String text=new String();
		Row row = null;
		for(int i = 0; i < rows.getSize(); i++)
		{
			row = rows.getRow(i);
			if(row.getValue("TITLE")!=null)
			{
				text=row.getValue("TITLE");
			}
		}
		text += " <style type=\"text/css\">" + n +
			"<!--" + n +
			".p12 {" + n +
			"font-size: 12px;" + n +
			"line-height: 20px;" + n +
			"color: #000000;" + n +
			"}" + n +
			".bluep12 {" + n +
			"font-size: 12px;" + n +
			"line-height: 20px;" + n +
			"color: #129DE9;" + n +
			"}" + n +
			"-->" + n +
			"</style>" + n +
			"<tr>" + n +
			"<td width=\"100%\" bgcolor=\"#FFFFFF\"><div align=\"left\"><strong><span class=\"bluep12\">電子交易對帳單明細如下：</span></strong></div></td>" + n + 
			"</tr>" + n +
			"<tr>" + 
			"<td bgcolor=\"#BBE3FF\" width=\"100%\">" +n +
			"   <table width=\"100%\" border=\"0\" align=\"center\" cellpadding=\"3\" cellspacing=\"2\" class=\"p12\">" + n + 
			"<tr  >" + n +
			"<td width=\"13%\" bordercolor=\"#C4E1F0\" bgcolor=\"#FFFFFF\"><div align=\"center\"><strong>交易時間</strong></div></td> " + n + 
			"<td width=\"15%\" bordercolor=\"#C4E1F0\" bgcolor=\"#FFFFFF\"><div align=\"center\"><strong>項目</strong></div></td> " + n + 
			"<td width=\"58%\" bordercolor=\"#C4E1F0\" bgcolor=\"#FFFFFF\"><div align=\"center\"><strong>通知內容</strong></div></td> " + n + 
			"<td width=\"14%\" bordercolor=\"#C4E1F0\" bgcolor=\"#FFFFFF\"><div align=\"center\"><strong>備註</strong></div></td> " + n + 
			"</tr>" + n ;
	
		boolean    rowfirst=true;
		String ltime=null;
		String subtype_dasc=null;
		String lline=null;
		String subtype=null;
		for(int i = 0; i < rows.getSize(); i++) {
			row = rows.getRow(i);
			
			String data = StrUtils.trim(row.getValue("DATA"));
			log.debug(ESAPIUtil.vaildLog("data = " + data));
			boolean isStart = false;
			
			if(StrUtils.isNotEmpty(row.getValue("TIME"))) {
				if(!rowfirst)
				{
					text += "<tr>" + n +
						"<td bordercolor=\"#C4E1F0\" bgcolor=\"#FFFFFF\"><div align=\"center\"><p>" + ltime + "</p></div></td>" + n +
						"<td bordercolor=\"#C4E1F0\" bgcolor=\"#FFFFFF\"><div align=\"center\">" + subtype_dasc + "</div></td>" + n +
						"<td bordercolor=\"#C4E1F0\" bgcolor=\"#FFFFFF\"><p>" + lline + "</p></td>" + n + 
						"<td bordercolor=\"#C4E1F0\" bgcolor=\"#FFFFFF\"><div align=\"center\">" + subtype + "</div></td>" + n +
						"</tr>";
				}
				ltime=nd10.datebrtime(row.getValue("TIME"));
				subtype_dasc=row.getValue("SUBTYPE_DESC");
				lline=data;
				subtype=row.getValue("SUBTYPE");
				isStart = true;
				rowfirst=false;
				continue;
			}

//			if(isStart) {
//				isStart = false;
/*
				text += "<tr>" + n +
				"<td bordercolor='#C4E1F0' bgcolor='#FFFFFF'><div align='center'><p>" + ltime + "</p></div></td>" + n +
				"<td bordercolor='#C4E1F0' bgcolor='#FFFFFF'><div align='center'>" + subtype_dasc + "</div></td>" + n +
				"<td bordercolor='#C4E1F0' bgcolor='#FFFFFF'><p>" + lline + "</p></td>" + n + 
				"<td bordercolor='#C4E1F0' bgcolor='#FFFFFF'><div align='center'>" + subtype + "</div></td>" + n +
				"</tr>";
*/
//				ltime=nd10.datebrtime(row.getValue("TIME"));
//				subtype_dasc=row.getValue("SUBTYPE_DESC");
//				lline=data;
//				subtype=row.getValue("SUBTYPE");
//				continue;
//			}
			else
			{
				lline += "<br>" + data;
/*
				text += "<tr>" + n +
				"<td align='left'>&nbsp;</td>" + n +
				"<td align='left'>&nbsp;</td>" + n +
				"<td align='left'>" + data + "</td>" + n + 
				"<td align='left'>&nbsp;</td>" + n +
				"</tr>";
*/
			}
	
		}
		text += "<tr>" + n +
		"<td bordercolor=\"#C4E1F0\" bgcolor=\"#FFFFFF\"><div align=\"center\"><p>" + ltime + "</p></div></td>" + n +
		"<td bordercolor=\"#C4E1F0\" bgcolor=\"#FFFFFF\"><div align=\"center\">" + subtype_dasc + "</div></td>" + n +
		"<td bordercolor=\"#C4E1F0\" bgcolor=\"#FFFFFF\"><p>" + lline + "</p></td>" + n + 
		"<td bordercolor=\"#C4E1F0\" bgcolor=\"#FFFFFF\"><div align=\"center\">" + subtype + "</div></td>" + n +
		"</tr>";

		text +="</table></td></tr>";
		sb.append(text);

	}

	public ADMCUSTTELNO getTelnoByCust(String cusidn) {
		ADMCUSTTELNO custtelno=null;
		List<ADMCUSTTELNO> listadmcusttelno;
		try
		{
			listadmcusttelno = admCustTelnoDao.findByEmpId(cusidn);
			custtelno=listadmcusttelno.get(0);
		}
		catch(Exception e)
		{
			return null;
		}
		return custtelno;
	}
	
	public String getND10Safe(Map params)
	{
		String[] var=new String[256];
		String Text=null;
		StringBuffer TextBuf=new StringBuffer();
		String line=null;
		InputStreamReader in = null;
		try
		{
			
			log.debug("notifyTemplateFolder = " + NotifyAngent.notifyTemplateFolder);
			File f = new File(NotifyAngent.notifyTemplateFolder, "ND10SAFE.html");
			f.setWritable(true,true);
			f.setReadable(true, true);
			f.setExecutable(true,true);
			MessageTemplate template = new MessageTemplate(f);
			MessageTO message = new MessageTO(template, params);
			return(message.getSubject() + "\r\n" + message.getMsgTemplate().buildContent(params));
/*
			FileInputStream sw = new FileInputStream("ND10.html");
			in = new InputStreamReader(sw, "BIG5");
			BufferedReader bw = new BufferedReader(in);
			while((line = bw.readLine()) != null)
			{
				var=line.split("%");
				TextBuf.append(line);
				TextBuf.append("\r\n");
			}
*/
		}
		catch(Exception e)
		{
			log.error("Exception " + e.toString());
		}
		return(null);
	}
	
	public int convmemo(String abc,Hashtable memo)
	{
		byte[] str = null;
		byte[] bmemo=new byte[101];
		int bmemocnt=0;
		byte[] bmemodesc=new byte[101];
		int bmemodesccnt=0;
		try
		{
			log.debug("abc.getBytes().length = " + abc.getBytes().length);
			
			str=abc.getBytes("big5");
			log.debug("str length = " + str.length);
		}
		catch(Exception e)
		{
			log.error("convmemo error");
			return(1);
		}
		for(int i = 0 ;i<str.length ; )
		{
			if((str[i] & 0x80) == 0x80)
			{
				bmemodesc[bmemodesccnt]=str[i];
				i++;
				bmemodesccnt++;
				bmemodesc[bmemodesccnt]=str[i];
				i++;
				bmemodesccnt++;
			}
			else
			{
				bmemo[bmemocnt]=str[i];
				i++;
				bmemocnt++;
			}
		}
		byte[] smemo = new byte[bmemocnt];
		byte[] smemodesc = new byte[bmemodesccnt];
		System.arraycopy(bmemo, 0, smemo, 0, bmemocnt);
		System.arraycopy(bmemodesc, 0, smemodesc, 0, bmemodesccnt);
		memo.put("MEMO", new String(smemo));
		try {
			memo.put("MEMODESC", new String(smemodesc,"BIG5"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			log.error("convmemo String (Encoding BIG5) = " + e.getMessage());
			memo.put("MEMODESC", new String(smemodesc));
		}
		log.debug("MEMO = " + memo.get("MEMO"));
		log.debug("MEMODESC = " + memo.get("MEMODESC"));
		return(0);
	}
}
