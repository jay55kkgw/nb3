package fstop.services.impl;

import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N843 extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("n843Telcomm")
	private TelCommExec n843Telcomm;
	 
//	@Required
//	public void setN843Telcomm(TelCommExec telcomm) {
//		n843Telcomm = telcomm;
//	}
	
	@Override	
	public MVH doAction(Map params) {    
		Map<String, String> p = params;

		if("N8431".equals(p.get("trancode")))
			params.put("LOSTYPE", "022");
		
		if("N8432".equals(p.get("trancode")))
			params.put("LOSTYPE", "044");

		TelcommResult helper = n843Telcomm.query(params);
		helper.getFlatValues().put("CMQTIME",
				DateTimeUtils.getDatetime(new Date()));
		
		return helper;
	}
}
