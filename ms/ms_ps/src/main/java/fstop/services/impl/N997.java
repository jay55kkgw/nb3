package fstop.services.impl;

import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.DBResult;
import fstop.model.EachRowCallback;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.model.RowsSelecter;
import fstop.model.TelcommResult;
import fstop.orm.dao.AdmBankDao;
import fstop.orm.dao.TxnTrAccSetDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.ADMBANK;
import fstop.orm.po.TXNFUNDDATA;
import fstop.orm.po.TXNTRACCSET;
import fstop.orm.po.TXNUSER;
import fstop.services.CommonService;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N997 extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	private AdmBankDao admBankDao;

	@Autowired
	private TxnTrAccSetDao txnTrAccSetDao;
	
//	@Required
//	public void setAdmBankDao(AdmBankDao dao) {
//		this.admBankDao = dao;
//	}
//	
//	@Required
//	public void setTxnTrAccSetDao(TxnTrAccSetDao dao) {
//		this.txnTrAccSetDao = dao;
//	}
	
	@Override
	public MVH doAction(Map _params) {
		Map<String, String> params = _params;
		
		MVHImpl result = new MVHImpl();
		if("QUERY".equalsIgnoreCase(params.get("EXECUTEFUNCTION"))) {
			String dpuserid = params.get("DPUSERID");
			List<TXNTRACCSET> txntraccset = txnTrAccSetDao.getByUid(dpuserid);
			DBResult dbresult = new DBResult(txntraccset);
			result.getOccurs().addRows(dbresult.getOccurs());  //initial result's occurs part
			
			RowsSelecter selecter = new RowsSelecter(result.getOccurs());
			selecter.each(new EachRowCallback() {

				public void current(Row row) {
					try{
					ADMBANK admbank = admBankDao.findById(row.getValue("DPTRIBANK"));
					row.setValue("ADBANKNAME", admbank.getADBANKNAME());
					}catch(Exception e){row.setValue("ADBANKNAME","   ");}
				}
				
			});
		}
		/*
		if("QUERY1".equalsIgnoreCase(params.get("EXECUTEFUNCTION"))) {
			String dpuserid = params.get("DPUSERID");
			String dptrdacno = params.get("DPTRDACNO");
			try {
				List r = txnTrAccSetDao.findByDPGONAME(dpuserid, dptrdacno);	
				result = new DBResult(r);
			}
			finally {}		
		}
		*/		
		if("UPDATE".equalsIgnoreCase(params.get("EXECUTEFUNCTION"))) {
			String data = params.get("DPDATA");
			Map<String, String> paramx = JSONUtils.json2map(data);
			
			String dpuserid = params.get("DPUSERID");
			String dpaccsetid = params.get("DPACCSETID");  //id
			String dpgoname = params.get("DPGONAME");   
			String dptracno = params.get("DPTRACNO");   
			String dptribank = paramx.get("DPTRIBANK");   
			String dptrdacno = paramx.get("DPTRDACNO");
			
			// Heuristic SQL Injection
//			log.debug("Update Data ====================================> " + data);
//			log.debug("dpuserid ====================================> " + dpuserid);
//			log.debug("dpaccsetid ====================================> " + dpaccsetid);
//			log.debug("dpgoname ====================================> " + dpgoname);
//			log.debug("dptracno ====================================> " + dptracno);
//			log.debug("dptribank ====================================> " + dptribank);
//			log.debug("dptrdacno ====================================> " + dptrdacno);
			
			TXNTRACCSET txntraccset = txnTrAccSetDao.findById(new Integer(dpaccsetid));
			if(StrUtils.isNotEmpty(dptrdacno) && 
					!StrUtils.trim(dptrdacno).equalsIgnoreCase(txntraccset.getDPTRDACNO())) {

				if(txnTrAccSetDao.findDptrdacnoIsExists(dpuserid,dptribank, dptrdacno))
					throw TopMessageException.create("Z997");//"轉入帳號設定重復"
				
			}
			
			txntraccset.setDPGONAME(dpgoname);
			txntraccset.setDPTRACNO(dptracno);
			txntraccset.setDPGONAME(dpgoname);
			txntraccset.setDPTRIBANK(dptribank);
			txntraccset.setDPTRDACNO(dptrdacno);
			
			Date d = new Date();
			txntraccset.setLASTDATE(DateTimeUtils.format("yyyyMMdd", d));
			txntraccset.setLASTTIME(DateTimeUtils.format("HHmmss", d));
			txnTrAccSetDao.save(txntraccset);
		}
		if("DELETE".equalsIgnoreCase(params.get("EXECUTEFUNCTION"))) {
			String dpuserid = params.get("DPUSERID");
			String dpaccsetid = params.get("DPACCSETID");  //id
			// TODO:removeById 改成 delete 加  findById
			txnTrAccSetDao.delete(txnTrAccSetDao.findById(new Integer(dpaccsetid)));
		}
		if("INSERT".equalsIgnoreCase(params.get("EXECUTEFUNCTION"))) {
			
			if(StrUtils.isNotEmpty(params.get("DPDATA"))) {
				String data = params.get("DPDATA");
				log.debug(ESAPIUtil.vaildLog("Insert Data ====================================> " + data));
				Map<String, String> paramx = JSONUtils.json2map(data);
				params.put("DPTRIBANK", paramx.get("DPTRIBANK"));
				params.put("DPTRDACNO", paramx.get("DPTRDACNO"));
			}
			
			String dpuserid = params.get("DPUSERID");
			String dpgoname = params.get("DPGONAME");   
			String dptracno = params.get("DPTRACNO");   
			String dptribank = params.get("DPTRIBANK");   
			String dptrdacno = params.get("DPTRDACNO");

			log.debug(ESAPIUtil.vaildLog("dpuserid ====================================> " + dpuserid));
			log.debug(ESAPIUtil.vaildLog("dpgoname ====================================> " + dpgoname));
			log.debug(ESAPIUtil.vaildLog("dptracno ====================================> " + dptracno));
			log.debug(ESAPIUtil.vaildLog("dptribank ====================================> " + dptribank));
			log.debug(ESAPIUtil.vaildLog("dptrdacno ====================================> " + dptrdacno));
			
			if(StrUtils.isNotEmpty(dptrdacno) && 
					txnTrAccSetDao.findDptrdacnoIsExists(dpuserid,dptribank, dptrdacno)) {
				//throw new RecordExistsException("轉入帳號設定重復");
				throw TopMessageException.create("Z997");//"轉入帳號設定重復"
			}
			
			TXNTRACCSET txntraccset = new TXNTRACCSET();
			txntraccset.setDPUSERID(dpuserid);
			txntraccset.setDPGONAME(dpgoname);
			txntraccset.setDPTRACNO(dptracno);
			txntraccset.setDPGONAME(dpgoname);
			txntraccset.setDPTRIBANK(dptribank);
			txntraccset.setDPTRDACNO(dptrdacno);
			
			Date d = new Date();
			txntraccset.setLASTDATE(DateTimeUtils.format("yyyyMMdd", d));
			txntraccset.setLASTTIME(DateTimeUtils.format("HHmmss", d));
			txnTrAccSetDao.save(txntraccset);
			
		}
		log.debug("N997 dump!!!");
		result.dump();
		return result;
	}
	public MVH getQuery1(final String DPUSERID,final String DPTRIBANK,final String DPTRDACNO) {
		MVHImpl result = new MVHImpl();
		try {
				List<TXNTRACCSET> r = txnTrAccSetDao.findByDPGONAME(DPUSERID,DPTRIBANK, DPTRDACNO);
				TXNTRACCSET r1 = r.get(0);            
				String DPGONAME=r1.getDPGONAME();			    
				int DPACCSETID=r1.getDPACCSETID();
				result.getFlatValues().put("DPGONAME", DPGONAME);
				result.getFlatValues().put("DPACCSETID", String.valueOf(DPACCSETID));	
				
				// CGI Stored XSS
//				log.debug("N997.java DPGONAME:"+DPGONAME+" DPACCSETID:"+DPACCSETID);
				
		} catch (Exception e) {
			log.warn("Could't not query txntraccset");
		}
		log.debug("N997 getQuery1 dump!!!");
		result.dump();		
		return result;
	}	
}
