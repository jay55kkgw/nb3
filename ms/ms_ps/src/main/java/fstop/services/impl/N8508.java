package fstop.services.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.util.DateUtils;
import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.model.TelcommResult;
import fstop.orm.dao.TxnLogDao;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N8508 extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("n8508Telcomm")
	private TelCommExec n8508Telcomm;
	 
//	@Required
//	public void setN8508Telcomm(TelCommExec telcomm) {
//		n8508Telcomm = telcomm;
//	}
	
	@Autowired
	private TxnLogDao txnlogdao;
	
	@Override	
	public MVH doAction(Map params) {

		MVHImpl helper = new MVHImpl();
		//將傳入的字串切割組成陣列
		log.trace(ESAPIUtil.vaildLog("GET ArrayParam : " + params.get("ArrayParam").toString()));
		String newparams = params.get("ArrayParam").toString();
		String[] arrayParams = newparams.split(";,");
		DateTime d1 = null;
		DateTime d2 = null;
		String pstimediff="";

//		String[] arrayParams = (String[])params.get("ArrayParam");
		Map cloneParams = new HashMap<>();
		cloneParams.putAll(params);
		if(arrayParams != null) {
			for(String p : arrayParams) {
				log.debug(ESAPIUtil.vaildLog("申請掛失 do : " + p)); //{}
//				Map cloneParams = params;
				//cloneParams.putAll(params);
				Map jb = JSONUtils.json2map(p);
				cloneParams.putAll(jb);
				cloneParams.put("DATE", DateTimeUtils.getCDateShort(new Date()));
				cloneParams.put("TIME", DateTimeUtils.format("HHmmss", new Date()));
				MVHImpl tr = null;
				try {
					d1 = new DateTime();
					tr = n8508Telcomm.query(cloneParams);
					tr.getFlatValues().put("STATUS", "掛失成功");
				}
				catch(TopMessageException e) {
					tr = new MVHImpl();
					tr.getFlatValues().put("TOPMSG", e.getMsgcode());
					tr.getFlatValues().put("STATUS", e.getMsgout());
				}
				finally {
					d2 = new DateTime();
					pstimediff = DateUtils.getDiffTimeMills(d1, d2);
					params.put("PSTIMEDIFF", pstimediff);
					tr.getFlatValues().putAll(jb);
					//寫TXNLOG
					try {
						txnlogdao.writeTxnLog(params,tr);
					}
					catch (Exception e) {
						log.error(ESAPIUtil.vaildLog("writeTxnLog ERROR >>>" + e));
					}
				}
//				helper.getFlatValues().put("TOPMSG", tr.getFlatValues().get("TOPMSG"));
				helper.getFlatValues().put("TOPMSG", "0");
				helper.getOccurs().addRow(new Row(tr.getFlatValues()));
			}
		}
		
		
		helper.getFlatValues().put("CMQTIME",
				DateTimeUtils.getDatetime(new Date()));
		
		
		
		return helper;
	}



}
