package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N942 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());
	@Autowired
    @Qualifier("n942Telcomm")
	private TelCommExec n942Telcomm;
	
//	@Required
//	public void setN942Telcomm(TelCommExec telcomm) {
//		n942Telcomm = telcomm;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) {
		params.put("DATE", DateTimeUtils.getCDateShort(new Date()));
		params.put("TIME", DateTimeUtils.format("HHmmss", new Date()));
		TelcommResult telcommResult = n942Telcomm.query(params);
		
		telcommResult.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));


		return telcommResult;
	}


}
