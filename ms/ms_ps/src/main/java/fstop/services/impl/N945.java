package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;

@Transactional
public class N945 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());
	
	@Autowired
	@Qualifier("n945Telcomm")
	private TelCommExec n945Telcomm;
	
//	private TelCommExec n941Telcomm;
	
//	@Required
//	public void setN941Telcomm(TelCommExec telcomm) {
//		n941Telcomm = telcomm;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override	
	public MVH doAction(Map params) {
		params.put("DATE", DateTimeUtils.getCDateShort(new Date()));
		params.put("TIME", DateTimeUtils.format("HHmmss", new Date()));
		TelcommResult telcommResult = n945Telcomm.query(params);
		
		telcommResult.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));

		return telcommResult;
	}
	
//	/**
//	 * 
//	 * @param params
//	 * @return
//	 */
//	@SuppressWarnings("unchecked")
//	public MVH doAction(Hashtable params) {
//		params.put("DATE", DateTimeUtils.getCDateShort(new Date()));
//		params.put("TIME", DateTimeUtils.format("HHmmss", new Date()));
//		TelcommResult telcommResult = n941Telcomm.query(params);
//		
//		telcommResult.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
//
//		return telcommResult;
//	}


}
