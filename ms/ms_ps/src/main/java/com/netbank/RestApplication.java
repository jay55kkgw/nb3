package com.netbank;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ApplicationListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.ContextStartedEvent;
import org.yaml.snakeyaml.Yaml;

import java.util.TimeZone;

@SpringBootApplication
@ComponentScan(basePackages = {"com.netbank.rest", "fstop.util", "fstop.orm.dao", "com.netbank.telcomm","fstop.va","fstop.services.batch","fstop.services.eai","fstop.orm.oao","fstop.notifier","fstop.notification"})
@EntityScan( basePackages = {"com.netbank.rest.db", "fstop.orm.po","fstop.orm.old"})
@EnableCaching
@Slf4j
public class RestApplication extends SpringBootServletInitializer {

	@Autowired ConfigurableApplicationContext ctx;

	Yaml yaml = new Yaml();

	/**
	 * 啟動修改 測試
	 * @param args
	 */
	public static void main(String[] args) {
		//System.exit(SpringApplication.exit(SpringApplication.run(MonitorApplication.class, args)));
		SpringApplication application = new SpringApplication(RestApplication.class);

//		Properties properties = new Properties();
//		properties.put("spring.mvc.throw-exception-if-no-handler-found", true);
//		properties.put("spring.resources.add-mappings", false);
//		application.setDefaultProperties(properties);

		application.run(args);


	}

	@Bean
	public ApplicationListener applicationClosedListener() {
		return (ApplicationListener<ContextClosedEvent>) event -> {
			log.info("Application shutdown .....");

		};
	}

	@Bean
	public ApplicationListener applicationStartListener() {
		return (ApplicationListener<ContextStartedEvent>) event -> {
			log.info("Application start .....");
			log.info("TimeZone.getDefault().getDisplayName() : " + TimeZone.getDefault().getDisplayName());
		};
	}

	@Bean
	public CommandLineRunner run() {
		//關於JAP查詢，可以參考以下文件
		//http://www.ityouknow.com/springboot/2016/08/20/springboot(五)-spring-data-jpa的使用.html
		//
		return (args) -> {


		};
	}
}
