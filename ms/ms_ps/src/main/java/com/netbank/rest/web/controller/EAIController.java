package com.netbank.rest.web.controller;

import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.netbank.util.ESAPIUtil;

import fstop.services.eai.EAIUtils;
import fstop.util.SpringBeanFactory;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(value = "/EAI", produces = "text/xml")
public class EAIController {
    @Autowired
    ServletContext servletContext;
    
	@Autowired
	private EAIUtils eAIUtils;
	
    @PostConstruct
    public void init() {
        SpringBeanFactory.setApplicationContext(WebApplicationContextUtils.getWebApplicationContext(servletContext));
    }
	// 20201111 弱掃：Reflected XSS All Clients
	@RequestMapping(value = {"/N186"}, method = {RequestMethod.POST})
    @ResponseBody
    public String doAction(@RequestBody String req)
	{
        log.info("/EAI/N186 Request = {}", ESAPIUtil.vaildLog(req)); // 20201204 弱掃 Log Forging
        String result = eAIUtils.executeN186(req);
		
        return result;
	}
	
	@RequestMapping(value = {"/N221"}, method = {RequestMethod.POST})
    @ResponseBody
    public String doActionN221(@RequestBody String req)
	{
        log.info("/EAI/N221 Request = {}", ESAPIUtil.vaildLog(req)); // 20201204 弱掃 Log Forging
        String result = eAIUtils.executeN221(req);
		
        return result;
	}
	
	
}
