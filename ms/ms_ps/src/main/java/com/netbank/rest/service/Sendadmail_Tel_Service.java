package com.netbank.rest.service;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netbank.util.ESAPIUtil;

import fstop.notifier.MessageTO;
import fstop.notifier.MessageTemplate;
import fstop.notifier.Notifier;
import fstop.notifier.NotifyListener;
import lombok.extern.slf4j.Slf4j;

/**
 * 主要用來 1.丟入MVH前的前置處理，包含檢核; 2.後製MVH回覆的訊息;
 * 
 *
 */
@Service
@Slf4j
public class Sendadmail_Tel_Service extends Common_Tel_Service
{

	@Autowired
	private Notifier notifier;
	
	@Autowired
    ServletContext servletContext;

	@Override
	public HashMap pre_Processing(Map request)
	{
		return super.pre_Processing(request);
	}

	/**
	 * Map會含以下參數資料： <BR>
	 * String Subject：郵件主旨 <BR>
	 * String Content：郵件內容（純文字）<BR>
	 * String ContentType : "1":"表示使用使用者的內容" <BR>
	 * List<String> Receivers：收件者清單
	 * 
	 * return API 要回傳： HashMap<String, String> : key: mail address, value: “”: 成功，!=””: 失敗原因
	 * 
	 */
	@Override
	public HashMap process(String serviceID, Map request)
	{

		HashMap<String, Object> resultMap = new HashMap<String, Object>();

		try
		{
			// 預設就先設定是錯誤的狀況
			resultMap.put("msgCode", "FE0003");
			String contenttype = request.get("ContentType") == null ? "0" : request.get("ContentType").toString();
			String subject = request.get("Subject").toString();
			String content = request.get("Content").toString();
			List<String> receivers = (List<String>) request.get("Receivers");

			String lcontent = "";
			// 如果傳入"1": "表示使用使用者的內容"
			if ("1".equals(contenttype))
			{
				lcontent = content;
			}else if("2".equals(contenttype)) {
				String POSTFIX = ".html";
				String notifyTemplateFolder = servletContext.getRealPath("WEB-INF/xmlConfig/web/NoticeTemplate");
				File f = new File(notifyTemplateFolder, request.get("TMPKEY")  + POSTFIX);
				f.setWritable(true,true);
				f.setReadable(true, true);
				f.setExecutable(true,true);
				MessageTemplate template = new MessageTemplate(f);
				MessageTO message = new MessageTO(template, request);
				lcontent = message.getMsgTemplate().buildContent((Map) request.get("MAILPARAM"));
				log.debug("mail comtent : {} " , lcontent);
			}
			else
			{
				// 套用公版
				String lcontent1 = "<html>\n<head>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF8\" />\n"
						+ "<title>臺灣企銀網路銀行交易通知</title>\n" + "</head>\n" + "<body>\n" + "親愛的客戶您好:<BR>\n" + "";
				String lcontent2 = "<BR>\n 臺灣企銀　敬上<BR>\n ※ 請勿直接回覆此信，如有疑問請至臺灣企銀網站"
						+ "<a href=\"https://www.tbb.com.tw/q3.html?\">" + "客服信箱</a>留言，將盡速為您服務。\n</body>\n</html>\n";
				lcontent = lcontent1 + content + lcontent2;
			}

			for (String mailAddress : receivers)
			{

				NotifyListener listener = new NotifyListener()
				{
					public void fail(String reciver, String subject, String msg, Exception e)
					{
						log.debug(ESAPIUtil.vaildLog("寄送失敗  = " + reciver + " | msg= " + msg));
						
						resultMap.put(reciver, "錯誤訊息 : " + e.getMessage());
					}

					public void success(String reciver, String subject, String msg)
					{
						log.debug("寄送成功  = " + reciver + " | msg= " + msg);
						resultMap.put(reciver, "");
					}
				};
				notifier.sendmsg(mailAddress, subject, lcontent, listener);
			}
			// 都做完回0
			resultMap.put("msgCode", "0");

		}
		catch (NullPointerException e)
		{
			log.error("NullPointerException {}", e);
			resultMap.put("sedMailResultMessage", " 傳入參數有誤 接收參數  :  " + request + " : 應傳入參數 : "
					+ "	 * String Subject：郵件主旨 <BR>\n" + "	 * String Content：郵件內容（純文字）<BR>\n"
					+ "	 * List<String> Receivers：收件者清單");
		}
		catch (Exception e)
		{
			log.error("Exception {}", e);
			resultMap.put("sedMailResultMessage", e.getMessage());
		}
		return resultMap;
	}
}
