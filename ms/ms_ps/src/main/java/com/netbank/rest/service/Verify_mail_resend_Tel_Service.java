package com.netbank.rest.service;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.netbank.util.CodeUtil;

import fstop.notifier.NotifyAngent;
import fstop.orm.dao.VerifyMailDao;
import fstop.orm.dao.VerifyMail_His_Dao;
import fstop.orm.po.VERIFYMAIL;
import fstop.orm.po.VERIFYMAIL_HIS;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 主要用來 1.丟入MVH前的前置處理，包含檢核; 2.後製MVH回覆的訊息;
 * 
 * @author Hugo
 *
 */
@Service
@Slf4j
public class Verify_mail_resend_Tel_Service extends Common_Tel_Service {

	@Autowired
	private VerifyMailDao verifyMailDao;
	
	@Value("${ms_env}")
	private String ms_env;
	
	@Autowired
	private VerifyMail_His_Dao verifyMail_His_Dao;


	@Override
	public HashMap process(String serviceID, Map request) {
		request = pre_Processing2(request);
		log.info(" Verify_mail_resend Start , request : {}", request);
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();

		try {

			String cusidn = StrUtils.isNotEmpty((String) request.get("CUSIDN")) ? (String) request.get("CUSIDN") : "";

			if (cusidn.length() == 0) {
				rtnMap.put("msgCode", "1");
				rtnMap.put("message", "參數錯誤");
				rtnMap.put("result", false);
				rtnMap.put("data", null);
				return rtnMap;
			}
			
			VERIFYMAIL sendData = verifyMailDao.findActiveMailById(cusidn);
			
			Map<String,String> mailMap = null ; 
			if(null!=sendData) {
				
				mailMap = CodeUtil.objectCovert(Map.class, sendData);
				//串mail需要欄位
				mailMap = mailparam(mailMap);
				
				//收件者 方便測試收信
				//若環境為D or T , 如果寄信的地址不為mail.tbb.com.tw , 都先導到H15D302B@mail.tbb.com.tw
				//若環境為P 則直接拿資料庫內的NMAIL
				String receviers = "";
				switch (ms_env) {
				case "D":
					receviers = ((String) mailMap.get("NMAIL")).indexOf("mail.tbb.com.tw")!=-1?(String) mailMap.get("NMAIL"):"H15D302B@mail.tbb.com.tw";
					break;
				case "T":
					receviers = ((String) mailMap.get("NMAIL")).indexOf("mail.tbb.com.tw")!=-1?(String) mailMap.get("NMAIL"):"H15D302B@mail.tbb.com.tw";
					break;
				case "P":
					receviers = (String)mailMap.get("NMAIL");
					break;
				}
				
				NotifyAngent.sendNotice("VERIFYMAIL", mailMap, receviers);
				
				updateMainNinsertHis(sendData,request);
				
				Verify_mail_resend_out outpo = CodeUtil.objectCovert_Keylower(Verify_mail_resend_out.class, sendData);
				List<Map<String,String>> datas = new ArrayList<Map<String,String>>();
				datas.add(CodeUtil.objectCovert_Keylower(Map.class, outpo));
				
				rtnMap.put("msgCode", "0");
				rtnMap.put("message", "補發成功");
				rtnMap.put("result", true);
				rtnMap.put("data", datas);
				
			}else {
				rtnMap.put("msgCode", "1");
				rtnMap.put("message", "查無資料");
				rtnMap.put("result", false);
				rtnMap.put("data", null);
			}
			

		} catch (Exception e) {
			
			log.error("Verify_mail_resend error :{}" , e.getCause());
			rtnMap.put("msgCode", "1");
			rtnMap.put("message", "資料錯誤");
			rtnMap.put("result", false);
			rtnMap.put("data", null);
		}

		return rtnMap;
	}
	
	public Map<String,String> mailparam (Map orgMap) throws UnsupportedEncodingException{
		
		orgMap.put("EXPIREDATE_FMT", DateTimeUtils.addSlash((String) orgMap.get("EXPIREDATE")));
		orgMap.put("TXDATE_FMT", DateTimeUtils.addSlash((String) orgMap.get("TXDATE")));
		
		switch (ms_env) {
		case "D":
			orgMap.put("ENC_URL", "https://172.22.11.20/nb3/VERIFYMAIL/verify_page?url="+orgMap.get("ENC_URL"));
			break;
		case "T":
			orgMap.put("ENC_URL", "https://172.22.11.14/nb3/VERIFYMAIL/verify_page?url="+orgMap.get("ENC_URL"));
			break;
		case "P":
			orgMap.put("ENC_URL", "https://ebank.tbb.com.tw/nb3/VERIFYMAIL/verify_page?url="+orgMap.get("ENC_URL"));
			break;
		}
		//存進DB的驗證碼是加密過後 , 這邊要解密並放入 VERIFYCODE_REAL
		String VERIFYCODE_REAL = new String(Base64.getDecoder().decode((String)orgMap.get("VERIFYCODE")), "utf-8");
		orgMap.put("VERIFYCODE_REAL",VERIFYCODE_REAL);
		
		return orgMap;
	}
	
	
	public void updateMainNinsertHis (VERIFYMAIL sendData , Map request) throws UnsupportedEncodingException{
		String old_hisid = sendData.getREF_HISID();
		String new_hisid = UUID.randomUUID().toString();
		
		sendData.setREF_HISID(new_hisid);
		Calendar cal2 = Calendar.getInstance();
		String ld = DateTimeUtils.getDateShort(cal2.getTime());
		String lt = DateTimeUtils.getTimeShort(cal2.getTime());
		sendData.setLASTDATE(ld);
		sendData.setLASTTIME(lt);
		sendData.setCHANNEL((String)(request.containsKey("LOGINTYPE")?request.get("LOGINTYPE"):request.containsKey("CHANNEL")?request.get("CHANNEL"):"unknown"));
		
		VERIFYMAIL_HIS old_his_p = verifyMail_His_Dao.findById(old_hisid);
		old_his_p.setSTATUS("5"); //已被補發
		old_his_p.setLASTDATE(ld);
		old_his_p.setLASTTIME(lt);
		old_his_p.setCHANNEL((String)(request.containsKey("LOGINTYPE")?request.get("LOGINTYPE"):request.containsKey("CHANNEL")?request.get("CHANNEL"):"unknown"));
		
		
		VERIFYMAIL_HIS new_his_po = CodeUtil.objectCovert(VERIFYMAIL_HIS.class, sendData);
		new_his_po.setHISID(new_hisid);
		new_his_po.setSTATUS("0");
		

		verifyMailDao.update(sendData);
		verifyMail_His_Dao.update(old_his_p);
		verifyMail_His_Dao.save(new_his_po);
	}
}
