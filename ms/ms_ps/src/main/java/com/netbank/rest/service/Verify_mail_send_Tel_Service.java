package com.netbank.rest.service;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.netbank.util.CodeUtil;

import fstop.notifier.NotifyAngent;
import fstop.orm.dao.VerifyMailDao;
import fstop.orm.dao.VerifyMail_His_Dao;
import fstop.orm.po.VERIFYMAIL;
import fstop.orm.po.VERIFYMAIL_HIS;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 主要用來 1.丟入MVH前的前置處理，包含檢核; 2.後製MVH回覆的訊息;
 * 
 * @author Hugo
 *
 */
@Service
@Slf4j
public class Verify_mail_send_Tel_Service extends Common_Tel_Service {

	@Autowired
	private VerifyMailDao verifyMailDao;

	@Autowired
	private VerifyMail_His_Dao verifyMail_His_Dao;

	@Value("${ms_env}")
	private String ms_env;

	@Value("${VERIFY_MAIL_AES_KEY}")
	private String aes_key;


	@Override
	public HashMap process(String serviceID, Map request) {
		/*
		 * 測試資料 
		 * "RDATE": "1101221", 
		 * "HEADER": "N930", 
		 * "CUSIDN": "A123456814", 
		 * "BRHCOD":"NB", 
		 * "TRNNUM": "NB930", 
		 * "TLRNUM": "V01", 
		 * "O_MAILADDR": "test@gmail.com",
		 * "N_MAILADDR": "happy@gmail.com",
		 */
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		log.info("=== Verify_mail_send Start ===");
		request = pre_Processing2(request);
		log.info("Verify_mail_send Request : {}", request);

		try {
			// Step 0 前置作業

			// AES加密會用到
			request.put("seedForRandom", aes_key);
			// 新舊EMAIL轉成DB需要欄位
			request.put("NMAIL", ((String)request.get("N_MAILADDR")).toLowerCase());
			request.put("OMAIL", ((String)request.get("O_MAILADDR")).toLowerCase());

			// Step1
			// 判斷傳進來的值在主表裡面有沒有
			VERIFYMAIL exist = verifyMailDao.findById((String) request.get("CUSIDN"));
			// Step2
			// 沒有的話執行 insert 主表 , 並同步到新增到HIST
			if (null == exist) {
				// 原 REQUEST 只有 CUSDIN , TXDATE , NMAIL , OMAIL , BRHCOD ,TLRNUM , TRNNUM
				// FILL_PO >> EXPIREDATE(RDATE >> N221) , VERIFYCODE , URL ,
				// ENC_URL,STATUS,CHANNEL,LASTDATE,LASTTIME
				request = fill_po_insert(request);
				VERIFYMAIL new_po = CodeUtil.objectCovert(VERIFYMAIL.class, request);
				VERIFYMAIL_HIS new_his_po = CodeUtil.objectCovert(VERIFYMAIL_HIS.class, request);

				verifyMailDao.save(new_po);
				verifyMail_His_Dao.save(new_his_po);
			}
			// Step3
			// 有的話執行 update 主表 除了 CUSIDN的欄位 , 並同步新增到HIST
			else {
				// 主表狀態要更新成 0 , 附表要新增一筆 ,
				String ref_hisid = exist.getREF_HISID();
				request = fill_po_update(request);
				VERIFYMAIL update_po = CodeUtil.objectCovert(VERIFYMAIL.class, request);
				VERIFYMAIL_HIS new_his_po = CodeUtil.objectCovert(VERIFYMAIL_HIS.class, request);
				verifyMailDao.update(update_po);
				// 要確認附表的那筆狀態是什麼,是0要改3,1234不變
				VERIFYMAIL_HIS his_po = verifyMail_His_Dao.findById(ref_hisid);
				if (null != his_po && "0".equals(his_po.getSTATUS())) {
					his_po.setSTATUS("3");
					verifyMail_His_Dao.update(his_po);
					verifyMail_His_Dao.save(new_his_po);
				} else {
					verifyMail_His_Dao.save(new_his_po);
				}

			}
			// Step4 都新增 or 更新完了 要寄信
			// MAIL上面需要 VERIFYCODE , TXDATE_FMT , EXPIREDATE_FMT , ENC_URL
			// FORMAT一下日期 for Mail
			request.put("EXPIREDATE_FMT", DateTimeUtils.addSlash((String) request.get("EXPIREDATE")));
			request.put("TXDATE_FMT", DateTimeUtils.addSlash((String) request.get("TXDATE")));

			// 串一下開發/測試/正式 的ENC_URL
			switch (ms_env) {
			case "D":
				request.put("ENC_URL", "https://172.22.11.20/nb3/VERIFYMAIL/verify_page?url=" + request.get("ENC_URL"));
				NotifyAngent.sendNotice("VERIFYMAIL", request,
						((String) request.get("N_MAILADDR")).indexOf("mail.tbb.com.tw") != -1
								? (String) request.get("N_MAILADDR")
								: "H15D302B@mail.tbb.com.tw");
				break;
			case "T":
				request.put("ENC_URL", "https://172.22.11.14/nb3/VERIFYMAIL/verify_page?url=" + request.get("ENC_URL"));
				NotifyAngent.sendNotice("VERIFYMAIL", request,
						((String) request.get("N_MAILADDR")).indexOf("mail.tbb.com.tw") != -1
								? (String) request.get("N_MAILADDR")
								: "H15D302B@mail.tbb.com.tw");
				break;
			case "P":
				request.put("ENC_URL", "https://ebank.tbb.com.tw/nb3/VERIFYMAIL/verify_page?url=" + request.get("ENC_URL"));
				NotifyAngent.sendNotice("VERIFYMAIL", request, (String) request.get("N_MAILADDR"));
				break;
			}

			rtnMap.put("msgCode", "0");
			rtnMap.put("message", "寄送成功");
			rtnMap.put("result", true);

		} catch (Exception e) {

			log.error("Verify_mail_send_Tel_Service error , {} ", e);
			rtnMap.put("msgCode", "1");
			rtnMap.put("message", "寄送失敗 >>" + e.getCause());
			rtnMap.put("result", false);
		}

		return rtnMap;
	}

	// EXPIREDATE(RDATE >> N221) , VERIFYCODE , URL , ENC_URL , STATUS , CHANNEL
	// ,STATUS , LASTDATE , LASTTIME
	public Map<String, String> fill_po_insert(Map<String, String> request)
			throws InvalidKeyException, NoSuchAlgorithmException, UnsupportedEncodingException, NoSuchPaddingException,
			IllegalBlockSizeException, BadPaddingException {

		// EXPIREDATE(RDATE 電文221會給)
		String expiredate = "";
		if (StrUtils.isNotEmpty(request.get("RDATE"))) {
			expiredate = request.get("RDATE");
		} else {
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, 14);
			expiredate = DateTimeUtils.getDateShort(cal.getTime());
		}

		// expiredate轉西元轉字串
		request.put("EXPIREDATE", DateTimeUtils.getDateShort(DateTimeUtils.getCDate2Date(expiredate)));

		// VERIFYCODE
		String verifyCode = CodeUtil.getVerifyCOde(10);
		// 寄送EMAIL用
		request.put("VERIFYCODE_REAL", verifyCode);
		// VERIFYCODE加密進DB
		request.put("VERIFYCODE", Base64.getEncoder().encodeToString(verifyCode.getBytes()));

		// URL,ENC_URL
		Map<String, String> urlMap;
		urlMap = CodeUtil.getEnc_url(request);

		request.put("REF_HISID", urlMap.get("REF_HISID"));
		request.put("HISID", urlMap.get("REF_HISID"));
		request.put("URL", urlMap.get("URL"));
		request.put("ENC_URL", urlMap.get("ENC_URL"));

		// STATUS
		request.put("STATUS", "0");

		// CHANNEL
		request.put("CHANNEL", request.containsKey("LOGINTYPE")?request.get("LOGINTYPE"):request.containsKey("CHANNEL")?request.get("CHANNEL"):"unknown");

		// LASTDATE,LASTTIME,TXDATE
		Calendar cal2 = Calendar.getInstance();
		request.put("TXDATE", DateTimeUtils.getDateShort(cal2.getTime()));
		request.put("LASTDATE", DateTimeUtils.getDateShort(cal2.getTime()));
		request.put("LASTTIME", DateTimeUtils.getTimeShort(cal2.getTime()));

		return request;
	}

	// EXPIREDATE(RDATE >> N221) , VERIFYCODE , URL , ENC_URL , STATUS , CHANNEL
	// ,STATUS , LASTDATE , LASTTIME
	public Map<String, String> fill_po_update(Map<String, String> request)
			throws InvalidKeyException, NoSuchAlgorithmException, UnsupportedEncodingException, NoSuchPaddingException,
			IllegalBlockSizeException, BadPaddingException {

		// EXPIREDATE(RDATE 電文221 , 930 會給)
		String expiredate = "";
		if (StrUtils.isNotEmpty(request.get("RDATE"))) {
			expiredate = request.get("RDATE");
		} else {
			// 基本上不會走到這裡 , 因為221跟930應該都要給
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, 14);
			expiredate = DateTimeUtils.getDateShort(cal.getTime());
		}
		// expiredate轉西元轉字串
		request.put("EXPIREDATE", DateTimeUtils.getDateShort(DateTimeUtils.getCDate2Date(expiredate)));

		// VERIFYCODE
		String verifyCode = CodeUtil.getVerifyCOde(10);
		// 寄送EMAIL用
		request.put("VERIFYCODE_REAL", verifyCode);
		// VERIFYCODE加密進DB
		request.put("VERIFYCODE", Base64.getEncoder().encodeToString(verifyCode.getBytes()));

		// URL,ENC_URL
		Map<String, String> urlMap;
		urlMap = CodeUtil.getEnc_url(request);

		request.put("REF_HISID", urlMap.get("REF_HISID"));
		request.put("HISID", urlMap.get("REF_HISID"));
		request.put("URL", urlMap.get("URL"));
		request.put("ENC_URL", urlMap.get("ENC_URL"));
		
		// STATUS
		request.put("STATUS", "0");

		// CHANNEL
		request.put("CHANNEL", request.containsKey("LOGINTYPE")?request.get("LOGINTYPE"):request.containsKey("CHANNEL")?request.get("CHANNEL"):"unknown");

		// LASTDATE,LASTTIME,TXDATE
		Calendar cal2 = Calendar.getInstance();
		request.put("TXDATE", DateTimeUtils.getDateShort(cal2.getTime()));
		request.put("LASTDATE", DateTimeUtils.getDateShort(cal2.getTime()));
		request.put("LASTTIME", DateTimeUtils.getTimeShort(cal2.getTime()));

		return request;
	}
}
