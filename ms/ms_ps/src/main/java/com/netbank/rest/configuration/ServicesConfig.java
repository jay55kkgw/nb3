package com.netbank.rest.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import fstop.services.ServerConfig;
import fstop.services.TopMsgDBDetecter;
import fstop.services.TopMsgF003Detecter;
import fstop.services.TopMsgF007Detecter;
import fstop.services.TopMsgFXDetecter;
import fstop.services.TopMsgFundDetecter;
import fstop.services.TopMsgN320Detecter;
import fstop.services.TopMsgN950Detecter;
import fstop.services.batch.ND10;
import fstop.services.batch.ND101;
//import fstop.services.batch.GD11;
//import fstop.services.batch.ND10;
//import fstop.services.batch.ND101;
import fstop.services.impl.*;
//import fstop.services.impl.fx.F001Controller;
//import fstop.services.impl.fx.F002Controller;
//import fstop.services.impl.fx.F003Controller;
import fstop.services.mgn.B105;
import fstop.services.mgn.B106;
import fstop.services.mgn.NA20;
import fstop.va.VATransform;
import fstop.ws.eai.client.N701WebServiceClient;
import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class ServicesConfig {

    @Bean
    protected VATransform vatransform() throws Exception {
        return new VATransform();
    }
    @Bean
    protected N701WebServiceClient n701WebServiceClient() throws Exception {
        return new N701WebServiceClient();
    }
	
    @Bean
    protected TopMsgDBDetecter dbErrDetect() throws Exception {
        return new TopMsgDBDetecter();
    }

    @Bean
    protected TopMsgN320Detecter n320ErrDetect() throws Exception {
        return new TopMsgN320Detecter();
    }

    @Bean
    protected TopMsgFXDetecter fxErrDetect() throws Exception {
        return new TopMsgFXDetecter();
    }

    @Bean
    protected TopMsgF003Detecter f003ErrDetect() throws Exception {
        return new TopMsgF003Detecter();
    }

    @Bean
    protected TopMsgF007Detecter f007ErrDetect() throws Exception {
        return new TopMsgF007Detecter();
    }

    @Bean
    protected TopMsgFundDetecter fundErrDetect() throws Exception {
        return new TopMsgFundDetecter();
    }
    @Bean
    protected TopMsgN950Detecter n950ErrDetect() throws Exception {
        return new TopMsgN950Detecter();
    }
//    @Bean
//    protected InvAttrib_S invattrib_sService() throws Exception {
//        return new InvAttrib_S();
//    }
//    
//    @Bean
//    protected A1000 a1000Service() throws Exception {
//        return new A1000();
//    }
//    
//    @Bean
//    protected A2000 a2000Service() throws Exception {
//        return new A2000();
//    }
//    
//    @Bean
//    protected A2001 a2001Service() throws Exception {
//        return new A2001();
//    }
//    
//    @Bean
//    protected A3000 a3000Service() throws Exception {
//        return new A3000();
//    }
//    
//    @Bean
//    protected A3000_MAIL a3000_mailService() throws Exception {
//        return new A3000_MAIL();
//    }
//    
//    @Bean
//    protected Announce_N announce_nService() throws Exception {
//        return new Announce_N();
//    }
//    
//    @Bean
//    protected Announce_Y announce_yService() throws Exception {
//        return new Announce_Y();
//    }
//    
//    @Bean
//    protected B012 b012Service() throws Exception {
//        return new B012();
//    }
//    
//    @Bean
//    protected B013 b013Service() throws Exception {
//        return new B013();
//    }
//    
    @Bean
    protected B105 b105Service() throws Exception {
        return new B105();
    }
//    
    @Bean
    protected B106 b106Service() throws Exception {
        return new B106();
    }
    
    @Bean
    protected CallMobileCenter callmobilecenterService() throws Exception {
    	return new CallMobileCenter();
    }
//	
//    @Bean
//    protected C017_Public c017_publicService() throws Exception {
//        return new C017_Public();
//    }
//    
//    @Bean
//    protected C022_Public c022_publicService() throws Exception {
//        return new C022_Public();
//    }
//	
//	@Bean
//    protected N371 n371Service() throws Exception {
//        return new N371();
//    }
//	
//    @Bean
//    protected CN19 cn19Service() throws Exception {
//        return new CN19();
//    }
//    
//    @Bean
//    protected CN32 cn32Service() throws Exception {
//        return new CN32();
//    }
//    
//    @Bean
//    protected CN32_1 cn32_1Service() throws Exception {
//        return new CN32_1();
//    }
//    
//    @Bean
//    protected CN32_2 cn32_2Service() throws Exception {
//        return new CN32_2();
//    }
//    
//    @Bean
//    protected CN32D cn32dService() throws Exception {
//        return new CN32D();
//    }
//    
//    @Bean
//    protected CN32D_1 cn32d_1Service() throws Exception {
//        return new CN32D_1();
//    }
//	
//    @Bean
//    protected C002 c002Service() throws Exception {
//        return new C002();
//    }
//
//    @Bean
//    protected C012 c012Service() throws Exception {
//        return new C012();
//    }    
//
//    @Bean
//    protected C013 c013Service() throws Exception {
//        return new C013();
//    }
//    
//    @Bean
//    protected C014 c014Service() throws Exception {
//        return new C014();
//    }
//    
//    @Bean
//    protected C016_Public c016_publicService() throws Exception {
//        return new C016_Public();
//    }
//    @Bean
//    protected C031_Public c031_publicService() throws Exception {
//        return new C031_Public();
//    }
//    @Bean
//    protected C015 c015Service() throws Exception {
//        return new C015();
//    }
//    
//    @Bean
//    protected C017 c017Service() throws Exception {
//        return new C017();
//    }
//    
//    @Bean
//    protected C020 c020Service() throws Exception {
//        return new C020();
//    }
//    
//    @Bean
//    protected C021_Notice c021_noticeService() throws Exception {
//        return new C021_Notice();
//    }
//    
//    @Bean
//    protected C024 c024Service() throws Exception {
//        return new C024();
//    }  
//    
//    @Bean
//    protected C024_Notice c024_noticeService() throws Exception {
//        return new C024_Notice();
//    }
//    
//    @Bean
//    protected C032 c032Service() throws Exception {
//        return new C032();
//    }
//    
//    @Bean
//    protected C032_Notice c032_noticeService() throws Exception {
//        return new C032_Notice();
//    }
//    
//    @Bean
//    protected C032_Public c032_publicService() throws Exception {
//        return new C032_Public();
//    }
//    
//    @Bean
//    protected C033 c033Service() throws Exception {
//        return new C033();
//    }
//    
//    @Bean
//    protected C033_Notice c033_noticeService() throws Exception {
//        return new C033_Notice();
//    }
//    
//    @Bean
//    protected C111 c111Service() throws Exception {
//        return new C111();
//    }
//    
//    @Bean
//    protected C112 c112Service() throws Exception {
//        return new C112();
//    }
//    
//    @Bean
//    protected C113 c113Service() throws Exception {
//        return new C113();
//    }
//    
//    @Bean
//    protected C114 c114Service() throws Exception {
//        return new C114();
//    }
//    
//    @Bean
//    protected C115 c115Service() throws Exception {
//        return new C115();
//    }
//    
//    @Bean
//    protected C116 c116Service() throws Exception {
//        return new C116();
//    }
//    
//    @Bean
//    protected C117 c117Service() throws Exception {
//        return new C117();
//    }
//    
//    @Bean
//    protected C118 c118Service() throws Exception {
//        return new C118();
//    }
//    
//    @Bean
//    protected C119 c119Service() throws Exception {
//        return new C119();
//    }
//    
//    @Bean
//    protected CK01 ck01Service() throws Exception {
//        return new CK01();
//    }
//    
//    @Bean
//    protected CK01_1 ck01_1Service() throws Exception {
//        return new CK01_1();
//    }
//    
//    @Bean
//    protected C900 c900Service() throws Exception {
//        return new C900();
//    }
//    
//    @Bean
//    protected Confirm_N confirm_nService() throws Exception {
//        return new Confirm_N();
//    }
//    
//    @Bean
//    protected Confirm_Y confirm_yService() throws Exception {
//        return new Confirm_Y();
//    }
//    
//    @Bean
//    protected CS01 c01Service() throws Exception {
//        return new CS01();
//    }
//    
//    @Bean
//    protected F001N f001nService() throws Exception {
//        return new F001N();
//    }
//    
//    @Bean
//    protected F001R f001rService() throws Exception {
//        return new F001R();
//    }
//    
//    @Bean
//    protected F001S f001sService() throws Exception {
//        return new F001S();
//    }
//    
//    @Bean
//    protected F001T f001tService() throws Exception {
//        return new F001T();
//    }
//    
//    @Bean
//    protected F001T_1 f001t_1Service() throws Exception {
//        return new F001T_1();
//    }
//    
//    @Bean
//    protected F001Y f001yService() throws Exception {
//        return new F001Y();
//    }
//    
//    @Bean
//    protected F002_SWIFT f002_swiftService() throws Exception {
//        return new F002_SWIFT();
//    }
//    
//    @Bean
//    protected F002N f002nService() throws Exception {
//        return new F002N();
//    }
//
//    @Bean
//    protected F002R f002rService() throws Exception {
//        return new F002R();
//    }
//    
//    @Bean
//    protected F002S f002sService() throws Exception {
//        return new F002S();
//    }
//    
//    @Bean
//    protected F002T f002tService() throws Exception {
//        return new F002T();
//    }
//    
//    @Bean
//    protected F002T_1 f002t_1Service() throws Exception {
//        return new F002T_1();
//    }
//    
//    @Bean
//    protected F002Y f002y_1Service() throws Exception {
//        return new F002Y();
//    }
//
//    @Bean
//    protected F003R f003rService() throws Exception {
//        return new F003R();
//    }
//    
//    @Bean
//    protected F003T f003tService() throws Exception {
//        return new F003T();
//    }
//    
//    @Bean
//    protected F013 f013Service() throws Exception {
//        return new F013();
//    }
//    
//    @Bean
//    protected F015 f015Service() throws Exception {
//        return new F015();
//    }
//    
//    @Bean
//    protected F017 f017Service() throws Exception {
//        return new F017();
//    }
//    
//    @Bean
//    protected F017R f017rService() throws Exception {
//        return new F017R();
//    }
//    
//    @Bean
//    protected F017T f017tService() throws Exception {
//        return new F017T();
//    }
//    
//    @Bean
//    protected F021 f021Service() throws Exception {
//        return new F021();
//    }
//    
//    @Bean
//    protected F035 f035Service() throws Exception {
//        return new F035();
//    }
//    
//    @Bean
//    protected F037 f037Service() throws Exception {
//        return new F037();
//    }
//    
//    @Bean
//    protected F039 f039Service() throws Exception {
//        return new F039();
//    }
//    
//    @Bean
//    protected Fundemail fundemailService() throws Exception {
//        return new Fundemail();
//    }
//    
//    @Bean
//    protected GD01 gd01Service() throws Exception {
//        return new GD01();
//    }
//    
//    @Bean
//    protected GD011 gd011Service() throws Exception {
//        return new GD011();
//    }
//    
//    @Bean
//    protected GD02 gd02Service() throws Exception {
//        return new GD02();
//    }
//    
//    @Bean
//    protected GD11 gd11Service() throws Exception {
//        return new GD11();
//    }
//    
//    @Bean
//    protected InvAttrib_G invattrib_gService() throws Exception {
//        return new InvAttrib_G();
//    }
//    
//    @Bean
//    protected InvAttrib_R invattrib_rService() throws Exception {
//        return new InvAttrib_R();
//    }
//    
//    @Bean
//    protected N_Empty n_emptyService() throws Exception {
//        return new N_Empty();
//    }
//    
//    @Bean
//    protected NA01 na01Service() throws Exception {
//        return new NA01();
//    }
//    
//    @Bean
//    protected NA02 na02Service() throws Exception {
//        return new NA02();
//    }
//    
//    @Bean
//    protected NA021_1 na021_1Service() throws Exception {
//        return new NA021_1();
//    }
//    
//    @Bean
//    protected NA02Q na02qService() throws Exception {
//        return new NA02Q();
//    }
//    
//    @Bean
//    protected NA03 na03Service() throws Exception {
//        return new NA03();
//    }
//    
//    @Bean
//    protected NA032_1 na032_1Service() throws Exception {
//        return new NA032_1();
//    }
//    
//    @Bean
//    protected NA03Q na03qService() throws Exception {
//        return new NA03Q();
//    }
//    
//    @Bean
//    protected NA04 na04Service() throws Exception {
//        return new NA04();
//    }
//    
//    @Bean
//    protected NA04_1 na04_1Service() throws Exception {
//        return new NA04_1();
//    }
//    
//    @Bean
//    protected NA04BACK na04backService() throws Exception {
//        return new NA04BACK();
//    }
//    
    @Bean
    protected NA20 na20Service() throws Exception {
        return new NA20();
    }
//    
//    @Bean
//    protected NA30 na30Service() throws Exception {
//        return new NA30();
//    }
//    
//    @Bean
//    protected NA30_1 na30_1Service() throws Exception {
//        return new NA30_1();
//    }
//    
//    @Bean
//    protected NA30_AUTH na30_authService() throws Exception {
//        return new NA30_AUTH();
//    }
//    
//    @Bean
//    protected NA40 na40Service() throws Exception {
//        return new NA40();
//    }
//    
//    @Bean
//    protected NA40_1 na40_1Service() throws Exception {
//        return new NA40_1();
//    }
//    
//    @Bean
//    protected NA40_2 na40_2Service() throws Exception {
//        return new NA40_2();
//    }
//    
//    @Bean
//    protected NA50 na50Service() throws Exception {
//        return new NA50();
//    }
//    
//    @Bean
//    protected NA60 na60Service() throws Exception {
//        return new NA60();
//    }
//    
//    @Bean
//    protected NA70 na70Service() throws Exception {
//        return new NA70();
//    }
//    
//    @Bean
//    protected NA71 na71Service() throws Exception {
//        return new NA71();
//    }
//    
//    @Bean
//    protected NA80 na80Service() throws Exception {
//        return new NA80();
//    }
//    
//    @Bean
//    protected NA011 na011Service() throws Exception {
//        return new NA011();
//    }
//    
//    @Bean
//    protected NA021 na021Service() throws Exception {
//        return new NA021();
//    }
//    
//    @Bean
//    protected NB31 nb31Service() throws Exception {
//        return new NB31();
//    }
//    
//    @Bean
//    protected NB31_1 nb31_1Service() throws Exception {
//        return new NB31_1();
//    }
//    
//    @Bean
//    protected NB32 nb32Service() throws Exception {
//        return new NB32();
//    }
//    
//    @Bean
//    protected NB32_1 nb32_1Service() throws Exception {
//        return new NB32_1();
//    }
//    
//    @Bean
//    protected NB32_2 nb32_2Service() throws Exception {
//        return new NB32_2();
//    }
//    
//    @Bean
//    protected ND08 nd08Service() throws Exception {
//        return new ND08();
//    }
//    
//    @Bean
//    protected ND081 nd081Service() throws Exception {
//        return new ND081();
//    }
//    
//    @Bean
//    protected NHK1 nhk1Service() throws Exception {
//        return new NHK1();
//    }
//    
//    @Bean
//    protected NI02 ni02Service() throws Exception {
//        return new NI02();
//    }
//    
//    @Bean
//    protected NI04 ni04Service() throws Exception {
//        return new NI04();
//    }
//    
//    @Bean
//    protected NP02 np02Service() throws Exception {
//        return new NP02();
//    }
//    
    @Bean
    protected N920 n920Service() throws Exception {
        return new N920();
    }
    
//    @Bean
//    protected N615 n615Service() throws Exception {
//        return new N615();
//    }
//    
//    @Bean
//    protected N015 n015Service() throws Exception {
//    	return new N015();
//    }
//    
//	@Bean
//    protected N070 n070Service() throws Exception {
//    	return new N070();
//    }
//	
//	@Bean
//    protected N070_N n070_nService() throws Exception {
//    	return new N070_N();
//    }
//	
//	@Bean
//    protected N070_Y n070_yService() throws Exception {
//    	return new N070_Y();
//    }
//	
//    @Bean
//    protected N070A n070aService() throws Exception {
//        return new N070A();
//    }    
//
//    @Bean
//    protected N070B n070bService() throws Exception {
//        return new N070B();
//    }    
//    
//    @Bean
//    protected N073 n073Service() throws Exception {
//        return new N073();
//    }    
//    
//    @Bean
//    protected N074 n074Service() throws Exception {
//        return new N074();
//    }
//        
//	@Bean
//    protected N075 n075Service() throws Exception {
//    	return new N075();
//    }
//	
//    @Bean
//    protected N076 n076Service() throws Exception {
//        return new N076();
//    }
//    
//    @Bean
//    protected N077 n077Service() throws Exception {
//        return new N077();
//    }
//    @Bean
//    protected N078 n078Service() throws Exception {
//    	return new N078();
//    }
//    @Bean
//    protected N079 n079Service() throws Exception {
//        return new N079();
//    }
//    
//    @Bean
//    protected N090_01 n090_01Service() throws Exception {
//        return new N090_01();
//    }
//
//    @Bean
//    protected N090_02 n090_02Service() throws Exception {
//        return new N090_02();
//    }
//    
//    @Bean
//    protected N091_01 n091_01Service() throws Exception {
//        return new N091_01();
//    }
//    
//    @Bean
//    protected N091_02 n091_02Service() throws Exception {
//        return new N091_02();
//    }
//    
//    @Bean
//    protected N091_03 n091_03Service() throws Exception {
//        return new N091_03();
//    }
//
//    @Bean
//    protected N092 n092Service() throws Exception {
//        return new N092();
//    }
//
//    @Bean
//    protected N093_01 n093_01Service() throws Exception {
//        return new N093_01();
//    }
//    
//    @Bean
//    protected N093_02 n093_02Service() throws Exception {
//        return new N093_02();
//    }
//    
//    @Bean
//    protected N094 n094Service() throws Exception {
//        return new N094();
//    }
//    
//    @Bean
//    protected N100 n100Service() throws Exception {
//        return new N100();
//    }
//    
//    @Bean
//    protected N1011 n1011Service() throws Exception {
//        return new N1011();
//    }
//    
//    @Bean
//    protected N1010_RP n1010_rpService() throws Exception {
//        return new N1010_RP();
//    }
//    
//    @Bean
//    protected N1010_CP n1010_cpService() throws Exception {
//        return new N1010_CP();
//    }
//    
//    @Bean
//    protected N1012 n1012Service() throws Exception {
//        return new N1012();
//    }
//    
//    @Bean
//    protected N1013 n1013Service() throws Exception {
//        return new N1013();
//    }
//    
//    @Bean
//    protected N1014 n1014Service() throws Exception {
//        return new N1014();
//    }
//    
//    @Bean
//    protected N1015 n1015Service() throws Exception {
//        return new N1015();
//    }
//    
//    @Bean
//    protected N103 n103Service() throws Exception {
//        return new N103();
//    }
//    
//    @Bean
//    protected N104 n104Service() throws Exception {
//        return new N104();
//    }
//    
//    @Bean
//    protected N104_1 n104_1Service() throws Exception {
//        return new N104_1();
//    }
//    
//    @Bean
//    protected N104_2 n104_2Service() throws Exception {
//        return new N104_2();
//    }
//    
//    @Bean
//    protected N105 n105Service() throws Exception {
//        return new N105();
//    }
//    
//    @Bean
//    protected N106 n106Service() throws Exception {
//        return new N106();
//    }
//    
//    @Bean
//    protected N107 n107Service() throws Exception {
//        return new N107();
//    }
//    
    @Bean
    protected N110 n110Service() throws Exception {
        return new N110();
    }

//
//    @Bean
//    protected N130 n130Service() throws Exception {
//        return new N130();
//    }
//    
//	@Bean
//    protected N150 n150Service() throws Exception {
//        return new N150();
//    }
//	
//    @Bean
//    protected N171 n171Service() throws Exception {
//        return new N171();
//    }
//    
//    @Bean
//    protected N175_1 n175_1Service() throws Exception {
//        return new N175_1();
//    }
//    
//    @Bean
//    protected N175_2S n175_2SService() throws Exception {
//        return new N175_2S();
//    }
//    
//    @Bean
//    protected N177 n177Service() throws Exception {
//        return new N177();
//    }
//    
//    @Bean
//    protected N178 n178Service() throws Exception {
//        return new N178();
//    }
//    
//    @Bean
//    protected N190 n190Service() throws Exception {
//        return new N190();
//    }
//    
//    @Bean
//    protected N191 n191Service() throws Exception {
//        return new N191();
//    }
//    
//    @Bean
//    protected N192 n192Service() throws Exception {
//        return new N192();
//    }
//    
//    @Bean
//    protected N201 n201Service() throws Exception {
//        return new N201();
//    }    
//    
//    @Bean
//    protected N203 n203Service() throws Exception {
//        return new N203();
//    }
//    
//    @Bean
//    protected N203_VA n203_vaService() throws Exception {
//        return new N203_VA();
//    }
//    
//    @Bean
//    protected N204 n204Service() throws Exception {
//        return new N204();
//    }
//    
//    @Bean
//    protected N2041 n2041Service() throws Exception {
//        return new N2041();
//    }
//    
//    @Bean
//    protected N2042 n2042Service() throws Exception {
//        return new N2042();
//    }
//    
//    @Bean
//    protected N215A n215aService() throws Exception {
//        return new N215A();
//    }
//    
//    @Bean
//    protected N215A_1 n215a_1Service() throws Exception {
//        return new N215A_1();
//    }
//    
//    @Bean
//    protected N215D n215dService() throws Exception {
//        return new N215D();
//    }
//    
//    @Bean
//    protected N215D_1 n215d_1Service() throws Exception {
//        return new N215D_1();
//    }
//    
//    @Bean
//    protected N215FA n215faService() throws Exception {
//        return new N215FA();
//    }
//    
//    @Bean
//    protected N215FA_1 n215fa_1Service() throws Exception {
//        return new N215FA_1();
//    }
//    
//    @Bean
//    protected N215FD n215fdService() throws Exception {
//        return new N215FD();
//    }
//    
//    @Bean
//    protected N215FD_1 n215fd_1Service() throws Exception {
//        return new N215FD_1();
//    }
//    
//    @Bean
//    protected N215FM n215fmService() throws Exception {
//        return new N215FM();
//    }
//    
//    @Bean
//    protected N215FM_1 n215fm_1Service() throws Exception {
//        return new N215FM_1();
//    }
//    
//    @Bean
//    protected N215M n215mService() throws Exception {
//        return new N215M();
//    }
//    
//    @Bean
//    protected N215M_1 n215m_1Service() throws Exception {
//        return new N215M_1();
//    }
//    
//    @Bean
//    protected N215Q n215qService() throws Exception {
//        return new N215Q();
//    }
//    
//    @Bean
//    protected N283 n283Service() throws Exception {
//        return new N283();
//    }
//    
//    @Bean
//    protected N283_2 n283_2Service() throws Exception {
//        return new N283_2();
//    }
//    
//    @Bean
//    protected N3003 n3003Service() throws Exception {
//        return new N3003();
//    }
//    
//    @Bean
//    protected N323 n323Service() throws Exception {
//        return new N323();
//    }
//    
//    @Bean
//    protected N3231 n3231Service() throws Exception {
//        return new N3231();
//    }
//    
//    @Bean
//    protected N360 n360Service() throws Exception {
//        return new N360();
//    }
//    
//    @Bean
//    protected N361 n361Service() throws Exception {
//        return new N361();
//    }
//    
//    @Bean
//    protected N361_1 n361_1Service() throws Exception {
//        return new N361_1();
//    }
//    
//    @Bean
//    protected N366 n366Service() throws Exception {
//        return new N366();
//    }
//    
//    @Bean
//    protected N366_1 n366_1Service() throws Exception {
//        return new N366_1();
//    }
//    
//    @Bean
//    protected N367 n367Service() throws Exception {
//        return new N367();
//    }
//    
//    @Bean
//    protected N370 n370Service() throws Exception {
//        return new N370();
//    }
//    
//    @Bean
//    protected N372 n372Service() throws Exception {
//        return new N372();
//    }
//    
//    @Bean
//    protected N373 n373Service() throws Exception {
//        return new N373();
//    }
//    
//    @Bean
//    protected N374 n374Service() throws Exception {
//        return new N374();
//    }
//    
//    @Bean
//    protected N382 n382Service() throws Exception {
//        return new N382();
//    }
//    
//    @Bean
//    protected N383 n383Service() throws Exception {
//        return new N383();
//    }
//    
//    @Bean
//    protected N384 n384Service() throws Exception {
//        return new N384();
//    }
//    
//    @Bean
//    protected N392 n392Service() throws Exception {
//        return new N392();
//    }
//    
//    @Bean
//    protected N393 n393Service() throws Exception {
//        return new N393();
//    }
//    
//    @Bean
//    protected N394 n394Service() throws Exception {
//        return new N394();
//    }
//    
//    @Bean
//    protected N420 n420Service() throws Exception {
//        return new N420();
//    }
//    
//    @Bean
//    protected N421 n421Service() throws Exception {
//        return new N421();
//    }
//
//
//    @Bean
//    protected N510 n510Service() throws Exception {
//        return new N510();
//    }
//
//
//    @Bean
//    protected N520 n520Service() throws Exception {
//        return new N520();
//    }
//
//    @Bean
//    protected N520_FILE n520_fileService() throws Exception {
//        return new N520_FILE();
//    }
//
//    @Bean
//    protected N530 n530Service() throws Exception {
//        return new N530();
//    }
//
//    @Bean
//    protected N531 n531Service() throws Exception {
//        return new N531();
//    }
//    
//    @Bean
//    protected N551 n551Service() throws Exception {
//        return new N551();
//    }
//    
//    @Bean
//    protected N555 n555Service() throws Exception {
//        return new N555();
//    }
//    
//    @Bean
//    protected N556 n556Service() throws Exception {
//        return new N556();
//    }
//    
//    @Bean
//    protected N557 n557Service() throws Exception {
//        return new N557();
//    }
//	
//	@Bean
//    protected N558 n558Service() throws Exception {
//        return new N558();
//    }
//	
//	@Bean
//    protected N559 n559Service() throws Exception {
//        return new N559();
//    }
//	
//	@Bean
//    protected N560 n560Service() throws Exception {
//        return new N560();
//    }
//	
//	@Bean
//    protected N563 n563Service() throws Exception {
//        return new N563();
//    }
//	
//    @Bean
//    protected N564 n564Service() throws Exception{
//    	return new N564();
//    }
//	
//    @Bean
//    protected N565 n565Service() throws Exception{
//    	return new N565();
//    }
//    
//    @Bean
//    protected N566 n566Service() throws Exception {
//        return new N566();
//    }
//	
//	@Bean
//    protected N567 n567Service() throws Exception {
//        return new N567();
//    }
//	
//	@Bean
//    protected N568 n568Service() throws Exception {
//        return new N568();
//    }
//    
//    @Bean
//    protected N550 n550Service() throws Exception {
//        return new N550();
//    }
//    
//    @Bean
//    protected N570 n570Service() throws Exception {
//        return new N570();
//    }
//    
//    @Bean
//    protected N625 n625Service() throws Exception {
//    	return new N625();
//    }
//	
//	@Bean
//    protected N640 n640Service() throws Exception {
//        return new N640();
//    }
//    @Bean
//    protected N650 n650Service() throws Exception {
//    	return new N650();
//    }
//    
//    @Bean
//    protected N701 n701Service() throws Exception {
//    	return new N701();
//    }
//    
//    @Bean
//    protected N750A n750aService() throws Exception {
//    	return new N750A();
//    }
//    
//    @Bean
//    protected N750B n750bService() throws Exception {
//    	return new N750B();
//    }
//
//    @Bean
//    protected N750C n750cService() throws Exception {
//    	return new N750C();
//    }
//
//    @Bean
//    protected N750D n750dService() throws Exception {
//    	return new N750D();
//    }
//    
//    @Bean
//    protected N750E n750eService() throws Exception {
//    	return new N750E();
//    }
//    
//    @Bean
//    protected N750F n750fService() throws Exception {
//    	return new N750F();
//    }
//    
//    @Bean
//    protected N750G n750gService() throws Exception {
//    	return new N750G();
//    }
//    	
//    @Bean
//    protected N750H n750hService() throws Exception {
//    	return new N750H();
//    }
//    
//    @Bean
//    protected N790 n790Service() throws Exception {
//    	return new N790();
//    }
//    
//    @Bean
//    protected N810 n810Service() throws Exception {
//        return new N810();
//    }
//    
//    @Bean
//    protected N814 n814Service() throws Exception {
//        return new N814();
//    }
//    
//    @Bean
//    protected N815 n815Service() throws Exception {
//        return new N815();
//    }
//    
//    @Bean
//    protected N816A n816aService() throws Exception {
//        return new N816A();
//    }
//    
//    @Bean
//    protected N816L n816lService() throws Exception {
//        return new N816L();
//    }
//    
//    @Bean
//    protected N816L_1 n816l_1Service() throws Exception {
//        return new N816L_1();
//    }
//    
//    @Bean
//    protected N821 n821Service() throws Exception {
//        return new N821();
//    }
//    
//    @Bean
//    protected N830 n830Service() throws Exception {
//        return new N830();
//    }
//    
//    @Bean
//    protected N8301 n8301Service() throws Exception {
//        return new N8301();
//    }
//    
//    @Bean
//    protected N8302 n8302Service() throws Exception {
//        return new N8302();
//    }
//    
//    @Bean
//    protected N831 n831Service() throws Exception {
//        return new N831();
//    }
//    
//    @Bean
//    protected N835 n835Service() throws Exception {
//        return new N835();
//    }
//    
//    @Bean
//    protected N8330 n8330Service() throws Exception {
//        return new N8330();
//    }
//    
    @Bean
    protected N840 n840Service() throws Exception {
        return new N840();
    }
    
    @Bean
    protected N841 n841Service() throws Exception {
        return new N841();
    }
    
    @Bean
    protected N842 n842Service() throws Exception {
        return new N842();
    }
    
    @Bean
    protected N843 n843Service() throws Exception {
        return new N843();
    }
    
    @Bean
    protected N844 n844Service() throws Exception {
        return new N844();
    }
    
    @Bean
    protected N845 n845Service() throws Exception {
        return new N845();
    }
	
    @Bean
    protected N8501 n8501Service() throws Exception {
        return new N8501();
    }
    
    @Bean
    protected N8502 n8502Service() throws Exception {
        return new N8502();
    }
    
    @Bean
    protected N8503 n8503Service() throws Exception {
        return new N8503();
    }
    
    @Bean
    protected N8504 n8504Service() throws Exception {
        return new N8504();
    }
    
    @Bean
    protected N8505 n8505Service() throws Exception {
        return new N8505();
    }
    
    @Bean
    protected N8506 n8506Service() throws Exception {
        return new N8506();
    }
    
    @Bean
    protected N8507 n8507Service() throws Exception {
        return new N8507();
    }
    
    @Bean
    protected N8508 n8508Service() throws Exception {
        return new N8508();
    }
    
    @Bean
    protected N8509 n8509Service() throws Exception {
        return new N8509();
    }
    
	@Bean
    protected N850A n850aService() throws Exception {
        return new N850A();
    }
//    
//    @Bean
//    protected N860 n860Service() throws Exception {
//        return new N860();
//    }
//	
//    @Bean
//    protected N865 n865Service() throws Exception {
//        return new N865();
//    }
//    
    @Bean
    protected N870 n870Service() throws Exception {
        return new N870();
    }
    
    @Bean
    protected N871 n871Service() throws Exception {
        return new N871();
    }
//    
//    @Bean
//    protected N880 n880Service() throws Exception {
//        return new N880();
//    }
//    
//    @Bean
//    protected TD01 td01Service() throws Exception {
//        return new TD01();
//    }
//    
//    @Bean
//    protected TD02 td02Service() throws Exception {
//        return new TD02();
//    }
//    
//    @Bean
//    protected N900 n900Service() throws Exception{
//    	return new N900();
//    }
//    
//    @Bean
//    protected N911 n911Service() throws Exception{
//    	return new N911();
//    }
//    
//    @Bean
//    protected N912 n912Service() throws Exception{
//    	return new N912();
//    }
//    
//    @Bean
//    protected N913 n913Service() throws Exception{
//    	return new N913();
//    }
//    
    @Bean
    protected N930EA n930eaService() throws Exception{
    	return new N930EA();
    }
    
    @Bean
    protected N931 n931Service() throws Exception{
    	return new N931();
    }
    
    @Bean
    protected N934 n934Service() throws Exception{
    	return new N934();
    }
	
    @Bean
    protected N935 n935Service() throws Exception{
    	return new N935();
    }
	
    @Bean
    protected N940 n940Service() throws Exception{
    	return new N940();
    }
    
    @Bean
    protected N941 n941Service() throws Exception{
    	return new N941();
    }
    
    @Bean
    protected N942 n942Service() throws Exception{
    	return new N942();
    }
    
    @Bean
    protected N944 n944Service() throws Exception{
    	return new N944();
    }
    
    @Bean
    protected N945 n945Service() throws Exception{
    	return new N945();
    }
    
    @Bean
    protected N950 n950Service() throws Exception{
    	return new N950();
    }
    
    @Bean
    protected N951 n951Service() throws Exception{
    	return new N951();
    }
    
    @Bean
    protected N953 n953Service() throws Exception{
    	return new N953();
    }
    
    @Bean
    protected N960 n960Service() throws Exception{
    	return new N960();
    }
    
    @Bean
    protected N997 n997Service() throws Exception{
    	return new N997();
    }
//    
//    @Bean
//    protected N016 n016Service() throws Exception {
//        return new N016();
//    }
//    
//    @Bean
//    protected N014 n014Service() throws Exception {
//        return new N014();
//    }
//    
//    @Bean
//    protected N320 n320Service() throws Exception {
//        return new N320();
//    }
//    
//    @Bean
//    protected C021 c021Service() throws Exception {
//        return new C021();
//    }
//    
//    @Bean
//    protected N998 n998Service() throws Exception {
//        return new N998();
//    }
//    
    @Bean
    protected N930EE n930eeService() throws Exception {
        return new N930EE();
    }
    //<!-- 後台OTP狀態 -->
    @Bean
    protected ServerConfig serverConfig() throws Exception {
        return new ServerConfig();
    }

    @Bean
    protected CallOtpStatus callOtpStatus() throws Exception {
        return new CallOtpStatus();
    }
//
//	@Bean
//    protected N175_2 n175_2Service() throws Exception {
//        return new N175_2();
//    }
//	
//	@Bean
//    protected P001 p001Service() throws Exception {
//        return new P001();
//    }
//	
//	@Bean
//    protected P002 p002Service() throws Exception {
//        return new P002();
//    }
//	
//	@Bean
//    protected P003 p003Service() throws Exception {
//        return new P003();
//    }
//	
//	@Bean
//    protected P004 p004Service() throws Exception {
//        return new P004();
//    }
//	
//	@Bean
//    protected P005 p005Service() throws Exception {
//        return new P005();
//    }
//	
//	@Bean
//    protected P018 p018Service() throws Exception {
//        return new P018();
//    }
//	
//	@Bean
//    protected N174 n174Service() throws Exception {
//        return new N174();
//    }
//	
//	@Bean
//    protected N108 n108Service() throws Exception {
//        return new N108();
//    }
//	
//	@Bean
//    protected N072 n072Service() throws Exception {
//        return new N072();
//    }
//	
//    @Bean
//    protected F001Controller f001controllerService() throws Exception {
//        return new F001Controller();
//    }
//    
//    @Bean
//    protected F002Controller f002controllerService() throws Exception {
//        return new F002Controller();
//    }
//    
//    @Bean
//    protected F003Controller f003controllerService() throws Exception {
//        return new F003Controller();
//    }
//    
//    @Bean
//    protected PcaLotto pcalottoService() throws Exception {
//        return new PcaLotto();
//    }
//    
//    @Bean
//    protected PcaLotto2 pcalotto2Service() throws Exception {
//        return new PcaLotto2();
//    }
//    
//    @Bean
//    protected Q072 q072Service() throws Exception {
//        return new Q072();
//    }
//    
//    @Bean
//    protected Q072_1 q072_1Service() throws Exception {
//        return new Q072_1();
//    }
//    
//    @Bean
//    protected TD01_MAIL td01_mailService() throws Exception {
//        return new TD01_MAIL();
//    }
//    
    @Bean
    protected XLSTXT xlstxtService() throws Exception {
        return new XLSTXT();
    }

	@Bean
    protected N660 n660Service() throws Exception {
        return new N660();
    }
    
    @Bean
    protected ND10 nd10Service() throws Exception {
        return new ND10();
    }
    
    @Bean
    protected ND101 nd101Service() throws Exception {
        return new ND101();
    }
}
