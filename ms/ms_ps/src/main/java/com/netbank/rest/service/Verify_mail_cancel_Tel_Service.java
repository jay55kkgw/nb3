package com.netbank.rest.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.netbank.util.CodeUtil;

import fstop.orm.dao.VerifyMailDao;
import fstop.orm.dao.VerifyMail_His_Dao;
import fstop.orm.po.VERIFYMAIL;
import fstop.orm.po.VERIFYMAIL_HIS;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * Email 驗證信 註銷驗證信 註銷驗證信 主表STATUS改2 同步附表 "參數皆小寫"
 * 
 * @author Ben
 *
 */
@Service
@Slf4j
public class Verify_mail_cancel_Tel_Service extends Common_Tel_Service {

	@Autowired
	private VerifyMailDao verifyMail_Dao;
	
	@Autowired
	private VerifyMail_His_Dao verifyMail_His_Dao;

	@Override
	public HashMap pre_Processing(Map request) {
		// TODO Auto-generated method stub
		return super.pre_Processing(request);
	}

	@SuppressWarnings("unchecked")
	@Override
	public HashMap process(String serviceID, Map request) {
		request = pre_Processing2(request);
		log.info("Verify_mail_cancel Start , request : {}", request);
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		try {

			String cusidn = StrUtils.isNotEmpty((String) request.get("CUSIDN")) ? (String) request.get("CUSIDN") : "";

			if (cusidn.length() == 0) {
				rtnMap.put("msgCode", "1");
				rtnMap.put("message", "參數錯誤");
				rtnMap.put("result", false);
				rtnMap.put("data", null);
				return rtnMap;
			}

			VERIFYMAIL data = verifyMail_Dao.findById(cusidn);
			if (null != data) {
				String msg="";
				switch (data.getSTATUS()) {
				case "1":
					msg="已成功驗證";
					break;
				case "2":
					msg="已註銷";
					break;
				case "4":
					msg="已過期未驗證";
					break;
				}
				if ("0".equals(data.getSTATUS())) {
					
					String ref_HISID = data.getREF_HISID();
					
					VERIFYMAIL_HIS his = verifyMail_His_Dao.findById(ref_HISID);
					
					data.setCHANNEL(request.containsKey("LOGINTYPE")?(String) request.get("LOGINTYPE"):request.containsKey("CHANNEL")?(String) request.get("CHANNEL"):"unknown");
					his.setCHANNEL(request.containsKey("LOGINTYPE")?(String) request.get("LOGINTYPE"):request.containsKey("CHANNEL")?(String) request.get("CHANNEL"):"unknown");
					
					List rtnList = fill_cancel_po(data,his);
					
					verifyMail_Dao.update((VERIFYMAIL) rtnList.get(0));
					
					verifyMail_His_Dao.update((VERIFYMAIL_HIS) rtnList.get(1));
					
					rtnMap.put("msgCode", "0");
					rtnMap.put("message", "註銷成功");
					rtnMap.put("result", true);
					
				} else {
					log.info("===  Verify_mail_cancel No Need to Cancel ===");
					rtnMap.put("msgCode", "0");
					rtnMap.put("message", "Email狀態為" + msg +"，註銷失敗");
					rtnMap.put("result", true);
				}
			} else {
				log.info("=== Verify_mail_cancel Empty ===");
				rtnMap.put("msgCode", "1");
				rtnMap.put("message", "查無資料");
				rtnMap.put("result", false);
			}

		} catch (Exception e) {

			log.error("=== Verify_mail_query error === {} ", e.getMessage());

			rtnMap.put("msgCode", "1");
			rtnMap.put("message", "查詢錯誤:" + e.getMessage());
			rtnMap.put("result", false);

		}

		return rtnMap;
	}
	
	public List fill_cancel_po(VERIFYMAIL data , VERIFYMAIL_HIS his_po){
		Calendar cal2 = Calendar.getInstance();
		data.setSTATUS("2");
		data.setLASTDATE(DateTimeUtils.getDateShort(cal2.getTime()));
		data.setLASTTIME(DateTimeUtils.getTimeShort(cal2.getTime()));
		
		his_po.setSTATUS("2");
		his_po.setLASTDATE(DateTimeUtils.getDateShort(cal2.getTime()));
		his_po.setLASTTIME(DateTimeUtils.getTimeShort(cal2.getTime()));
		
		List<Object> rtnList = new LinkedList<>();
		
		rtnList.add(data);
		rtnList.add(his_po);
		
		return rtnList;
	}
	
}
