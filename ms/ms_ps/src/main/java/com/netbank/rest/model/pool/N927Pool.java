package com.netbank.rest.model.pool;

import java.util.Date;
import java.util.Hashtable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.netbank.rest.model.NewOneCallback;
import com.netbank.rest.model.Pool;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.TXNUSER;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;

@Component
public class N927Pool extends Pool{
	@Autowired
	@Qualifier("n927Telcomm")
	private TelCommExec n927Telcomm;

	@Autowired
	private TxnUserDao txnUserDao;	
		
	private NewOneCallback getNewOneCallback(final String uid) {
		NewOneCallback callback = new NewOneCallback() {

			public Object getNewOne() {

				Hashtable params = new Hashtable();
				params.put("CUSIDN", uid);
				Date d = new Date();
				String df = DateTimeUtils.getCDateShort(d);
				String tf = DateTimeUtils.format("HHmmss", d);
				params.put("DATE", df);
				params.put("TIME", tf);
				
				TelcommResult mvh = n927Telcomm.query(params);
				
				//同時以 N927.APLBRH 異動 TXNUSER.ADBRANCHID
				TXNUSER user = txnUserDao.findById(uid);
				
				user.setDPSUERID(uid);
				user.setADBRANCHID(mvh.getValueByFieldName("APLBRH"));
				
				txnUserDao.save(user);
				
				return mvh;
			}
		};		
		
		return callback;
	}
	
	/**
	 * N927黃金存摺風險屬性查詢
	 * @param uid  使用者的身份證號
	 * @return
	 */
	public MVH getData(final String idn) {
		final String uid = StrUtils.trim(idn).toUpperCase();
		if(uid.length() == 0){
			return new MVHImpl();
		}
		return (MVH)getPooledObject(uid,getNewOneCallback(uid));
	}
	
	public void removeGetDataCache(final String idn) {
		super.removePoolObject(idn);
	}
}