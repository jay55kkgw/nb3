package com.netbank.rest.configuration;

import java.util.HashMap;
import java.util.Map;

import org.apache.cxf.spring.boot.autoconfigure.CxfAutoConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import fstop.notification.Notification;
import fstop.notification.Notify;
import fstop.notifier.BHWebServiceNotifier;
import fstop.notifier.Notifier;
import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class NotifiterConfig {

	@Bean
	public Notifier notifier(@Value("${WebService.SendMail.URI}") String bh_webservices_uri,
			@Value("${WebService.SendMail.TemplateNo}") String bh_webservices_templateno,
			@Value("${WebService.SendMail.ProjectID}") String bh_webservices_projectid,
			@Value("WebService.SendMail.SenderMail") String bh_webservices_sendermail,
			@Value("${WebService.SendMail.SenderMail.Batch}") String bh_webservices_sendermail_batch) {

		BHWebServiceNotifier b = new fstop.notifier.BHWebServiceNotifier();

		Map<String, String> m = new HashMap();
		m.put("bh.webservices.uri", bh_webservices_uri);
		m.put("bh.webservices.templateno", bh_webservices_templateno);
		m.put("bh.webservices.projectid", bh_webservices_projectid);
		m.put("bh.webservices.sendermail", bh_webservices_sendermail);
		m.put("bh.webservices.sendermail.batch", bh_webservices_sendermail_batch);

		m.put("bh.webservices.mail_sendername", "臺灣企銀網路銀行");
		m.put("bh.webservices.mail_sendername.batch", "臺灣企銀網路銀行");

//		b.setSmtpSetting(m);
		return b;
	}
	
}
