package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netbank.rest.model.CommonPools;
import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.notifier.BHWebServiceNotifier;
import fstop.notifier.Notifier;
import fstop.notifier.NotifyListener;
import fstop.notifier.NotifyListenerAdapter;
import lombok.extern.slf4j.Slf4j;

/**
 * 主要用來 1.丟入MVH前的前置處理，包含檢核; 2.後製MVH回覆的訊息;
 * 
 *
 */
@Service
@Slf4j
public class Sendmail_Tel_Service extends Common_Tel_Service {

	@Autowired
	private Notifier notifier;
	
	boolean result ;
	
	
	@Override
	public HashMap pre_Processing(Map request) {
		// TODO Auto-generated method stub
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) {
		
		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		try {
			
			String lcontent=(String)request.get("ADMAILCONTENT");
			String lcontent1=lcontent.substring(0, lcontent.indexOf("</title>")) + "</title>";
			String lcontent2="<META http-equiv=Content-Type content=\"text/html; charset=big5\">\r\n" + "<STYLE type=text/css><!--\r\n" + ".Cron{border: 2px solid #000000;background-color: #ff0000;color: #FFFFFF;table-layout: auto;width: 100px;}\r\n" + "--></STYLE>"+ "<META content=\"MSHTML 6.00.6000.16735\" name=GENERATOR></HEAD>";
			String lcontent3="<body>\r\n<center><span class=\"Cron\" ><FONT color=#000000 size=200>補發</FONT></span></center>\r\n<BR>";
			String lcontent4="親愛的客戶您好:<BR>\r\n本行依據轉出人的指示，再次發送下列通知，請注意避免重複。本通知僅供參考，<BR>\r\n不作為交易憑證，實際交易結果依本行系統為準！<BR>\r\n";
			String lcontent5=lcontent.substring(lcontent.indexOf("<pre>"));
			
			NotifyListener listener = new NotifyListener() {
				public void fail(String reciver, String subject, String msg, Exception e) {
					log.debug(ESAPIUtil.vaildLog("補發失敗  = " + reciver + " | msg= " + msg));
					result = false;
				}

				public void success(String reciver, String subject, String msg) {
					log.debug("補發成功  = " + reciver + " | msg= " + msg);
					result = true;
				}
			};
			notifier.sendmsg((String)request.get("ADMAILACNO"), (String)request.get("ADMSUBJECT") + "補發", lcontent1 + lcontent2 + lcontent3 + lcontent4 + lcontent5, listener);
			
			if(result) {
				resultMap.put("sedMailResult", "0");
			}else {
				resultMap.put("sedMailResult", "1");
			}
			
		} catch (Exception e) {
			log.error("process error >> {}",e);
			resultMap.put("sedMailResult", "1");
		} finally {
			return resultMap;
		}
	}
}
