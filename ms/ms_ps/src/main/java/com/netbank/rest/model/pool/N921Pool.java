package com.netbank.rest.model.pool;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.netbank.rest.model.NewOneCallback;
import com.netbank.rest.model.UserPool;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.model.Rows;
import fstop.model.TelcommResult;
import fstop.orm.dao.AdmBankDao;
import fstop.services.impl.N130;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 約定帳號查詢
 * @author Owner
 *
 */
@Slf4j
@Component
public class N921Pool extends UserPool {
	@Autowired
	@Qualifier("n921Telcomm")
	private TelCommExec n921Telcomm;
	@Autowired
	private AdmBankDao admBankDao;
	
//	@Required
//	public void setN921Telcomm(TelCommExec telcomm) {
//		n921Telcomm = telcomm;
//	}
//	
//	@Required
//	public void setAdmBankDao(AdmBankDao admbankDao) {
//		this.admBankDao = admbankDao;
//	}

	private NewOneCallback getNewOneCallback(final String uid, final String type) {
		
		NewOneCallback callback = new NewOneCallback() {
	
			public Object getNewOne() {
	
				Map params = new HashMap<>();
				params.put("CUSIDN", uid);
				Date d = new Date();
				String df = DateTimeUtils.getCDateShort(d);
				String tf = DateTimeUtils.format("HHmmss", d);
				params.put("DATE", df);
				params.put("TIME", tf);
				params.put("TYPE", type);
				
				TelcommResult mvh = n921Telcomm.query(params);
				
			    //VALUE為行庫別及帳號的組合，如[050,12345678901]
			    //TEXT為行庫別、行庫名稱及帳號的組合，如[050-臺灣企銀 12345678901]
				Rows result = new Rows();
				result.addRows(mvh.getOccurs());
				
				for(Row r : result.getRows()) {
					Map<String, String> m = new HashMap();
					String bankcod = r.getValue("BNKCOD");
					String acn = r.getValue("ACN");
					
					m.put("BNKCOD", bankcod);
					m.put("ACN", acn);
					m.put("AGREE", "1"); // 設成是約定帳號  0: 非約定, 1: 約定
					r.setValue("VALUE", JSONUtils.map2json(m));
					
					String text = bankcod + "-" + getBankName(bankcod) + " " + acn;
					
					r.setValue("TEXT", text);
				}
				
				return result;
			}
			
		};
		
		return callback;
	}
	
	private String getBankName(final String bankcod) {
		String result = "";
		try {
			//admBankDao
			result = admBankDao.getbank(bankcod).getADBANKNAME();
			
		}
		catch(Exception e) {
			log.error("找不到分行的中文名稱(" + bankcod + ").");
		}
		return result;
	}
	
	
	/**
	 * 
	 * @param uid  使用者的身份證號
	 * @param acnotype  帳戶屬性
	 * @param trflag  轉出帳號註記
	 * acnotype
	 * ‘01’自行轉帳‘02’跨行轉帳
	 * ‘03’期貨轉帳‘04’繳信用卡款
	 * ‘05’活存轉定存 
	 * 
	 * @return
	 */
	public MVH getAcnoList(final String idn, final String[] acnotype) {
		Assert.notEmpty(acnotype, "輸入的 type 不可為 空"); 
		final String uid = StrUtils.trim(idn).toUpperCase();
		if(uid.length() == 0)
			return new MVHImpl();

		final Set<String> accounts = new HashSet();
		Rows rows = new Rows();
		if(acnotype != null) {
			for(String act : acnotype) {
				if(StrUtils.isNotEmpty(act)) {
					String type = StrUtils.right("00" + StrUtils.trim(act), 2);

					Rows r  = (Rows)getPooledObject(uid ,"N921_" + type, getNewOneCallback(uid, type));
					for(int i=0; i < r.getSize(); i++) {
						
						//不重複加入相同的銀行別加帳號
						String acount = r.getRow(i).getValue("BNKCOD") + "_" + r.getRow(i).getValue("ACN");
						if(accounts.contains(acount))
							continue;
						accounts.add(acount);
						
						rows.addRow(r.getRow(i));
					}
				}
			}
		}

		return new MVHImpl(rows);
	}

	/**
	 * 將 身份證字號 為KEY 放置在POOL 的 N921 資料刪除
	 * @param idn
	 */
	public void removeFromPool(String idn) {
		super.removePoolObject(idn);
	}
	
}
