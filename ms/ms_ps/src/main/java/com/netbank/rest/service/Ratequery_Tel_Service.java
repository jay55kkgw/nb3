package com.netbank.rest.service;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netbank.rest.model.CommonPools;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 主要用來 1.丟入MVH前的前置處理，包含檢核; 2.後製MVH回覆的訊息;
 * 
 * @author Hugo
 *
 */
@Service
@Slf4j
public class Ratequery_Tel_Service extends Common_Tel_Service {

	@Autowired
	CommonPools commonPools;

	@Override
	public HashMap pre_Processing(Map request) {
		// TODO Auto-generated method stub
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) {
		MVH mvh = null;
		String updateDate = "";
		String updateTime = "";
		String dateString = "";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		switch ((String) request.get("QUERYTYPE")) {
		case "N021":
			mvh = new MVHImpl();
			mvh = commonPools.n021.query(request);

			break;
		case "N022":
			mvh = new MVHImpl();
			mvh = commonPools.n022.query(request);
			if (((MVHImpl) mvh).getOccurs().getSize() > 0) {
				updateDate = ((MVHImpl) mvh).getOccurs().getRow(0).getValue("DATE");
				// 進行轉換
				dateString = sdf.format(DateTimeUtils.getCDate2Date(updateDate));
				((MVHImpl) mvh).getFlatValues().put("UPDATEDATE", dateString);
			}
			break;
		case "N023":
			mvh = new MVHImpl();
			mvh = commonPools.n023.query(request);
			break;
		case "N024":
			mvh = new MVHImpl();
			mvh = commonPools.n024.query(request);
			if (((MVHImpl) mvh).getOccurs().getSize() > 0) {
				updateDate = ((MVHImpl) mvh).getOccurs().getRow(0).getValue("DATE");
				// 進行轉換
				dateString = sdf.format(DateTimeUtils.getCDate2Date(updateDate));
				((MVHImpl) mvh).getFlatValues().put("UPDATEDATE", dateString);
			}
			break;
		case "N025":
			mvh = new MVHImpl();
			mvh = commonPools.n025.query(request);
			break;
		case "N026":
			mvh = new MVHImpl();
			mvh = commonPools.n026.query(request);
			if (((MVHImpl) mvh).getOccurs().getSize() > 0) {
				updateDate = DateTimeUtils.addSlash2(((MVHImpl) mvh).getOccurs().getRow(0).getValue("DATE"));
				updateTime = DateTimeUtils.addColon2(((MVHImpl) mvh).getOccurs().getRow(0).getValue("TIME"));
				((MVHImpl) mvh).getFlatValues().put("UPDATEDATE", updateDate);
				((MVHImpl) mvh).getFlatValues().put("UPDATETIME", updateTime);
			}
			break;
		case "N027":
			mvh = new MVHImpl();
			mvh = commonPools.n027.query(request);
			if (((MVHImpl) mvh).getOccurs().getSize() > 0) {
				updateDate = ((MVHImpl) mvh).getOccurs().getRow(0).getValue("BRHBDT");
				// 進行轉換
				dateString = sdf.format(DateTimeUtils.getCDate2Date(updateDate));
				((MVHImpl) mvh).getFlatValues().put("UPDATEDATE", dateString);
			}
			break;
		case "N030":
			mvh = new MVHImpl();
			mvh = commonPools.n030.query(request);
			if (((MVHImpl) mvh).getOccurs().getSize() > 0) {
				updateDate = DateTimeUtils.addSlash2(((MVHImpl) mvh).getOccurs().getRow(0).getValue("DATE"));
				updateTime = DateTimeUtils.addColon2(((MVHImpl) mvh).getOccurs().getRow(0).getValue("TIME"));
				((MVHImpl) mvh).getFlatValues().put("UPDATEDATETIME", updateDate+" "+updateTime);
			}
			break;
		}

		((MVHImpl) mvh).getFlatValues().put("TOPMSG", "");
		return data_Retouch(mvh);
	}

	@Override
	public HashMap data_Retouch(MVH mvh) {
		// TODO Auto-generated method stub
		return super.data_Retouch(mvh);
	}

	@Override
	public void setMsgCode(Map dataMap, Boolean data_Retouch_result) {
		// TODO Auto-generated method stub
		super.setMsgCode(dataMap, data_Retouch_result);
	}

}
