package com.netbank.rest.model.pool;

import java.util.Date;
import java.util.Hashtable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.netbank.rest.model.Pool;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;

@Component
public class N924Pool extends Pool {
	
	@Autowired
	@Qualifier("n924Telcomm")
	private TelCommExec n924Telcomm;
	
//	
//	public void setN924Telcomm(TelCommExec telcomm) {
//		n924Telcomm = telcomm;
//	}

//	private NewOneCallback getNewOneCallback(final String uid) {
//		NewOneCallback callback = new NewOneCallback() {
//
//			public Object getNewOne() {
//
//				return null;
//			}
//			
//		};		
//		
//		return callback;
//	}
//	
	
	/**
	 * N924 
	 * @param uid  使用者的身份證號
	 * @return
	 */
	public MVH getAcnoList(final String idn) {
		final String uid = StrUtils.trim(idn).toUpperCase();
		if(uid.length() == 0)
			return new MVHImpl();
		
		Hashtable params = new Hashtable();
		params.put("CUSIDN", uid);
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);
		
		MVHImpl mvh = n924Telcomm.query(params);
		
		return mvh;
		

	}
}
