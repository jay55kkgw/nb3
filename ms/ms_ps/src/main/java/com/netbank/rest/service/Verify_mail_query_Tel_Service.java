package com.netbank.rest.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.netbank.util.CodeUtil;

import fstop.orm.dao.VerifyMail_His_Dao;
import fstop.orm.po.VERIFYMAIL_HIS;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * Email 驗證信 查詢 API 
 * 查詢近三個月Email驗證信狀態
 * "參數皆小寫" 
 * 
 * @author Ben
 *
 */
@Service
@Slf4j
public class Verify_mail_query_Tel_Service extends Common_Tel_Service {
	
	@Autowired
	private VerifyMail_His_Dao verifyMail_His_Dao;
	
	@Value("${query_period:3}")
	String query_period;
	
	@SuppressWarnings("unchecked")
	@Override
	public HashMap process(String serviceID, Map request) {
		request = pre_Processing2(request);
		log.info("Verify_mail_query Start , request : {}" , request);
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		try {
			
			String cusidn =  StrUtils.isNotEmpty((String) request.get("CUSIDN"))?(String) request.get("CUSIDN"):"";
			
			if(cusidn.length()==0) {
				rtnMap.put("msgCode", "1");
				rtnMap.put("message", "參數錯誤");
				rtnMap.put("result", false);
				rtnMap.put("data", null);
				return rtnMap;
			}
			
			int month =  StrUtils.isNotEmpty((String) request.get("MONTH"))?Integer.parseInt((String) request.get("MONTH")):Integer.parseInt(query_period);
			List<VERIFYMAIL_HIS> dataList = verifyMail_His_Dao.findById_with_month(cusidn,month);
			List<Map<String,String>> datas = new ArrayList<Map<String,String>>();
			if(dataList.size()>0) {
				for(VERIFYMAIL_HIS eachdata:dataList) {
					Verify_mail_query_out outpo = CodeUtil.objectCovert_Keylower(Verify_mail_query_out.class, eachdata);
					datas.add(CodeUtil.objectCovert_Keylower(Map.class, outpo));
				}
				
				log.info("===  Verify_mail_query Success , size : {} ===" , datas.size());
				rtnMap.put("msgCode", "0");
				rtnMap.put("message", "查詢成功");
				rtnMap.put("result", true);
				rtnMap.put("data", datas);
				
			}else {
				log.info("=== Verify_mail_query Empty ===");
				rtnMap.put("msgCode", "1");
				rtnMap.put("message", "查無資料");
				rtnMap.put("result", false);
				rtnMap.put("data", null);
			}
			
		}catch (Exception e) {
			
			log.error("=== Verify_mail_query error === {} " , e.getMessage());
			
			rtnMap.put("msgCode", "1");
			rtnMap.put("message", "查詢錯誤:"+e.getMessage());
			rtnMap.put("result", false);
			rtnMap.put("data", null);
			
		}			
		
		return rtnMap;
	}

}
