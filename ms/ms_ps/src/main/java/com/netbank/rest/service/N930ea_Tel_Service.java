package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netbank.rest.model.CommonPools;
import com.netbank.util.AcnoTypeEnum;
import com.netbank.util.StrUtils;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.services.CommonService;
import fstop.util.SpringBeanFactory;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class N930ea_Tel_Service extends Common_Tel_Service {
	
	@Autowired
	private CommonPools commonPools;
	
	@Override
	public HashMap pre_Processing(Map request) {
		// TODO Auto-generated method stub
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) {
		MVH mvh = new MVHImpl();
		try {
			pre_Processing(request);
			mvh = commonPools.n930ea.doAction(request);
		}catch (TopMessageException e) {
			// TODO Auto-generated catch block
			log.error("process.error>>",e);
			MVHImpl m = new MVHImpl();
			m.getFlatValues().put("TOPMSG", e.getMsgcode());
			mvh = m;
		} catch (Exception e) {
			log.error("process.error>>{}",e);
			throw new RuntimeException(e);
		}finally {
//			TODO trur 做data_Retouch false 做msgCode 回覆處理
			return data_Retouch(mvh);
		}
	}
		
	@Override
	public HashMap data_Retouch(MVH mvh) {
		// TODO Auto-generated method stub
		return super.data_Retouch(mvh);
	}

}