package com.netbank.rest.service;

public class Verify_mail_resend_out {
	
	private String txdate;	
	private String expiredate;	
	private String nmail;
	private String channel;
	private String lastdate;
	private String lasttime;
	
	public String getTxdate() {
		return txdate;
	}
	public void setTxdate(String txdate) {
		this.txdate = txdate;
	}
	public String getExpiredate() {
		return expiredate;
	}
	public void setExpiredate(String expiredate) {
		this.expiredate = expiredate;
	}
	public String getNmail() {
		return nmail;
	}
	public void setNmail(String nmail) {
		this.nmail = nmail;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	public String getLastdate() {
		return lastdate;
	}
	public void setLastdate(String lastdate) {
		this.lastdate = lastdate;
	}
	public String getLasttime() {
		return lasttime;
	}
	public void setLasttime(String lasttime) {
		this.lasttime = lasttime;
	}
}
