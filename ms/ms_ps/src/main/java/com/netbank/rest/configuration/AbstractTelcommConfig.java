package com.netbank.rest.configuration;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;

import com.google.common.collect.ImmutableMap;
import com.netbank.telcomm.CommonTelcomm;

import fstop.services.TopMsgDBDetecter;
import fstop.services.TopMsgF003Detecter;
import fstop.services.TopMsgF007Detecter;
import fstop.services.TopMsgFXDetecter;
import fstop.services.TopMsgFundDetecter;
import fstop.services.TopMsgN320Detecter;
import fstop.services.TopMsgN950Detecter;
import lombok.extern.slf4j.Slf4j;

/**
 * ApplicationContext-telcomm.xml
 */
@Slf4j
public abstract class AbstractTelcommConfig {

    @Autowired
    @Qualifier("defaultFieldFormatDefine")
    private Map<String, String> defaultFieldFormatDefine;

    @Autowired
    @Qualifier("fxFieldFormatDefine")
    private Map<String, String> fxFieldFormatDefine;
    
    @Bean
    protected CommonTelcomm ck01Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("ck01.txt", "CK01");
        c.setDesc("線上申請長期使用循環信用檢核交易");
        c.setHOSTMSGID("CK01");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        return c;
    }

    @Bean
    protected CommonTelcomm b012Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("b012.txt", "B012");
        c.setDesc("債券餘額及損益查詢");
        c.setHOSTMSGID("B012");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
		c.setErrDetect(fundErrDetect);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(RECNO)"));
        return c;
    }    
    
    @Bean
    protected CommonTelcomm b013Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("b013.txt", "B013");
        c.setDesc("債券交易資料查詢");
        c.setHOSTMSGID("B013");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
		c.setErrDetect(fundErrDetect);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(RECNO)"));
        return c;
    }    
    @Bean
    protected CommonTelcomm b014Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("b014.txt", "B014");
        c.setDesc("債券配息資料查詢");
        c.setHOSTMSGID("B014");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
		c.setErrDetect(fundErrDetect);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(RECNO)"));
        return c;
    }    
    
    @Bean
    protected CommonTelcomm b015Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("b015.txt", "B015");
        c.setDesc("債券歷史交易資料查詢_依交易編號");
        c.setHOSTMSGID("B015");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
		c.setErrDetect(fundErrDetect);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(RECNO)"));
        return c;
    }    
    
    
    @Bean
    protected CommonTelcomm c012Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("c012.txt", "C012");
        c.setDesc("基金餘額及損益查詢");
        c.setHOSTMSGID("C012");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
		c.setErrDetect(fundErrDetect);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(RECNO)"));
        return c;
    }    
    
    @Bean
    protected CommonTelcomm c013Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("c013.txt", "C013");
        c.setDesc("基金交易資料查詢");
        c.setHOSTMSGID("C013");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
		c.setErrDetect(fundErrDetect);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(RECNO)"));
        return c;
    }
    
    @Bean
    protected CommonTelcomm c014Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("c014.txt", "C014");
        c.setDesc("基金交易明細");
        c.setHOSTMSGID("C014");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
		c.setErrDetect(fundErrDetect);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(RECNO)"));
        return c;
    }
    
    @Bean
    protected CommonTelcomm c015Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("c015.txt", "C015");
        c.setDesc("投資人基本資料變更查詢");
        c.setHOSTMSGID("C015");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(fundErrDetect);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(RECNO)"));
        return c;
    }
    
    @Bean
    protected CommonTelcomm c017Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("c017.txt", "C017");
        c.setDesc("(BATCH)");
        c.setHOSTMSGID("C017");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(RECNO)"));
        return c;
    }
    
    @Bean
    protected CommonTelcomm c018Telcomm() throws Exception {
    	CommonTelcomm c = createTelcomm("c018.txt", "C018");
    	c.setDesc("(BATCH)");
    	c.setHOSTMSGID("C018");
    	c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
    	c.setAfterCommand(Arrays.asList("OccursNumFromValue(RECNO)"));
    	return c;
    }
    
    @Bean
    protected CommonTelcomm c019Telcomm() throws Exception {
    	CommonTelcomm c = createTelcomm("c019.txt", "C019");
    	c.setDesc("(BATCH)");
    	c.setHOSTMSGID("C019");
    	c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
    	c.setAfterCommand(Arrays.asList("OccursNumFromValue(RECNO)"));
    	return c;
    }
    
    @Bean
    protected CommonTelcomm c050Telcomm() throws Exception {
    	CommonTelcomm c = createTelcomm("c050.txt", "C050");
    	c.setDesc("(BATCH)");
    	c.setHOSTMSGID("C050");
    	c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
    	c.setAfterCommand(Arrays.asList("OccursNumFromValue(RECNO)"));
    	return c;
    }
    
    @Bean
    protected CommonTelcomm c051Telcomm() throws Exception {
    	CommonTelcomm c = createTelcomm("c051.txt", "C051");
    	c.setDesc("(BATCH)");
    	c.setHOSTMSGID("C051");
    	c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
    	c.setAfterCommand(Arrays.asList("OccursNumFromValue(RECNO)"));
    	return c;
    }
    
    @Bean
    protected CommonTelcomm c020Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("c020.txt", "C020");
        c.setDesc("申購結果通知");
        c.setHOSTMSGID("C020");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
		c.setErrDetect(fundErrDetect);        
		c.setBeforeCommand(Arrays.asList(
				"PPSYNCN1()", //<!-- 使用 日期 -->
				"PINNEW1()" //<!-- 使用SH1 -->
        ));
        return c;
    }
    
    @Bean
    protected CommonTelcomm c016Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("c016.txt", "C016");
        c.setDesc("手續費檢核");
        c.setHOSTMSGID("C016");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
		c.setErrDetect(fundErrDetect);
        return c;
    }
    
    @Bean
    protected CommonTelcomm c024Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("c024.txt", "C024");
        c.setDesc("");
        c.setHOSTMSGID("C024");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
		c.setErrDetect(fundErrDetect);
        return c;
    }
    
    @Bean
    protected CommonTelcomm c025Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("c025.txt", "C025");
        c.setDesc("信用卡申購基金 - 手續費檢核");
        c.setHOSTMSGID("C025");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
		c.setErrDetect(fundErrDetect);
        return c;
    }
    
    @Bean
    protected CommonTelcomm c026Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("c026.txt", "C026");
        c.setDesc("信用卡申購基金結果通知");
        c.setHOSTMSGID("C026");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
		c.setErrDetect(fundErrDetect);        
		c.setBeforeCommand(Arrays.asList(
				"PPSYNCN1()", //<!-- 使用 日期 -->
				"PINNEW1()" //<!-- 使用SH1 -->
        ));
        return c;
    }
    
    
    @Bean
    protected CommonTelcomm c032Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("c032.txt", "C032");
        c.setDesc("");
        c.setHOSTMSGID("C032");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
		c.setErrDetect(fundErrDetect);
        return c;
    }
    
    @Bean
    protected CommonTelcomm c033Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("c033.txt", "C033");
        c.setDesc("");
        c.setHOSTMSGID("C033");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
		c.setErrDetect(fundErrDetect);
        return c;
    }
    
    @Bean
    protected CommonTelcomm c111Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("c111.txt", "C111");
        c.setDesc("基金定期投資約定變更查詢");
        c.setHOSTMSGID("C111");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
		c.setErrDetect(fundErrDetect);
		c.setFieldFormat(ImmutableMap.of("PAYAMT", "N:#0"));
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(RECNO)"));
        return c;
    }
    
    @Bean
    protected CommonTelcomm c11Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("c112.txt", "C112");
        c.setDesc("基金定期投資約定變更");
        c.setHOSTMSGID("C112");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
		c.setErrDetect(fundErrDetect);
		c.setFieldFormat(ImmutableMap.of("PAYAMT", "N:#0"));
        c.setBeforeCommand(Arrays.asList(
        		"PPSYNCN1()",//<!-- 使用 日期 -->
        		"PINNEW1()"//<!-- 使用SH1 -->
    		));
        return c;
    }
    
    @Bean
    protected CommonTelcomm c112Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("c112.txt", "C112");
        c.setDesc("基金定期投資約定變更");
        c.setHOSTMSGID("C112");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
		c.setErrDetect(fundErrDetect);
		c.setFieldFormat(ImmutableMap.of("PAYAMT", "N:#0"));
        c.setBeforeCommand(Arrays.asList(
        		"PPSYNCN1()",//<!-- 使用 日期 -->
        		"PINNEW1()"//<!-- 使用SH1 -->
    		));
        return c;
    }
    
    @Bean
    protected CommonTelcomm c113Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("c113.txt", "C113");
        c.setDesc("投資人基本資料變更");
        c.setHOSTMSGID("C113");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
		c.setErrDetect(fundErrDetect);
        c.setBeforeCommand(Arrays.asList(
        		"PPSYNCN1()",//<!-- 使用 日期 -->
        		"PINNEW1()"//<!-- 使用SH1 -->
        		));
        return c;
    }
    
    @Bean
    protected CommonTelcomm c114Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("c114.txt", "C114");
        c.setDesc("基金贖回通知");
        c.setHOSTMSGID("C114");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
		c.setErrDetect(fundErrDetect);
        c.setBeforeCommand(Arrays.asList(
        		"PPSYNCN1()",//<!-- 使用 日期 -->
        		"PINNEW1()"//<!-- 使用SH1 -->
		));
        return c;
    }
    
    @Bean
    protected CommonTelcomm c115Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("c115.txt", "C115");
        c.setDesc("定期投資約定資料查詢");
        c.setHOSTMSGID("C115");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
		c.setErrDetect(fundErrDetect);
		c.setFieldFormat(ImmutableMap.of("PAYAMT", "N:#0"));
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(RECNO)"));
        return c;
    }
    
    @Bean
    protected CommonTelcomm c116Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("c116.txt", "C116");
        c.setDesc("停損/停利列表");
        c.setHOSTMSGID("C116");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
		c.setErrDetect(fundErrDetect);
		c.setFieldFormat(ImmutableMap.of("PAYAMT", "N:#0"));
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(RECNO)"));
        return c;
    }
    
    @Bean
    protected CommonTelcomm c117Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("c117.txt", "C117");
        c.setDesc("停損/停利設定");
        c.setHOSTMSGID("C117");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
		c.setErrDetect(fundErrDetect);
        c.setBeforeCommand(Arrays.asList(
        		"PPSYNCN1()",//<!-- 使用 日期 -->
        		"PINNEW1()"//<!-- 使用SH1 -->
    		));
        return c;
    }
    
    @Bean
    protected CommonTelcomm c118Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("c118.txt", "C118");
        c.setDesc("自動贖回設定查詢");
        c.setHOSTMSGID("C118");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
		c.setErrDetect(fundErrDetect);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(RECNO)"));
        return c;
    }
    
    @Bean
    protected CommonTelcomm c119Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("c119.txt", "C119");
        c.setDesc("自動贖回設定");
        c.setHOSTMSGID("C119");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
		c.setErrDetect(fundErrDetect);
        c.setBeforeCommand(Arrays.asList(
        		"PPSYNCN1()",//<!-- 使用 日期 -->
        		"PINNEW1()"//<!-- 使用SH1 -->
    		));
        return c;
    }
    
    @Bean
    protected CommonTelcomm c052Telcomm() throws Exception {
    	CommonTelcomm c = createTelcomm("c052.txt", "C052");
    	c.setDesc("(BATCH)");
    	c.setHOSTMSGID("C052");
    	c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
    	c.setAfterCommand(Arrays.asList("OccursNumFromValue(RECNO)"));
    	return c;
    }
    
    @Bean
    protected CommonTelcomm td01Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("td01.txt", "TD01");
        c.setDesc("未出帳單明細查詢");
        c.setHOSTMSGID("TD01");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(RECNO)"));
        return c;
    }
    
    @Bean
    protected CommonTelcomm na30Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("na30.txt", "NA30");
        c.setDesc("金融卡線上申請網銀");
        c.setHOSTMSGID("NA30");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
        		"PINNEW1(HLOGINPIN,HLOGINPIN)",//<!-- 使用 日期 -->
        		"PINNEW1(HTRANSPIN,HTRANSPIN)"//<!-- 使用SH1 -->
    		));
        return c;
    }
    
    @Bean
    protected CommonTelcomm na30VerifyTelcomm() throws Exception {
        CommonTelcomm c = createTelcomm("na30.txt", "NA30");
        c.setDesc("金融卡線上申請網銀");
        c.setHOSTMSGID("NA30");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        return c;
    }
    
    @Bean
    protected CommonTelcomm na40Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("na40.txt", "NA40");
        c.setDesc("信用卡線上申請網銀");
        c.setHOSTMSGID("NA40");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
        		"PINNEW1(HLOGINPIN,HLOGINPIN)",//<!-- 使用 日期 -->
        		"PINNEW1(HTRANSPIN,HTRANSPIN)"//<!-- 使用SH1 -->
    		));
        return c;
    }
    
    @Bean
    protected CommonTelcomm na40VerifyTelcomm() throws Exception {
        CommonTelcomm c = createTelcomm("na40.txt", "NA40");
        c.setDesc("信用卡線上申請網銀");
        c.setHOSTMSGID("NA40");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        return c;
    }
    
    @Bean
    protected CommonTelcomm n810Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n810.txt", "N810");
        c.setDesc("信用卡持卡總覽");
        c.setHOSTMSGID("N810");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(CNT)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n813Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n813.txt", "N813");
        c.setDesc("VISA金融卡卡號查詢");
        c.setHOSTMSGID("N813");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(CNT)"));
        return c;
    }

    @Bean
    protected CommonTelcomm a1000Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("a1000.txt", "A1000");
        c.setDesc("帳戶總覽");
        c.setHOSTMSGID("A1000");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        return c;
    }

    @Bean
    protected CommonTelcomm a2000Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("a2000.txt", "A2000");
        c.setDesc("帳戶總覽");
        c.setHOSTMSGID("A2000");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        return c;
    }

    @Bean
    protected CommonTelcomm a2001Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("a2001.txt", "A2001");
        c.setDesc("帳戶總覽");
        c.setHOSTMSGID("A2001");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        return c;
    }

    @Bean
    protected CommonTelcomm n200Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n200.txt", "N200");
        c.setDesc("變更往來帳戶及信託業務通訊地址／電話");
        c.setHOSTMSGID("N200");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n523Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n523.txt", "N523");
        c.setDesc("外匯活存明細查詢 - 指定帳號");
        c.setHOSTMSGID("N523");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
                "PPSYNCN1()",		
				"PINNEW1()"
        ));
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(COUNT)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n263Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n263.txt", "N263");
        c.setDesc("香港分行帳戶存款指定期間交易明細查詢");
        c.setHOSTMSGID("N263");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
                "PPSYNCN1()",		
				"PINNEW1()"
        ));
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(COUNT)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n524Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n524.txt", "N524");
        c.setDesc("外匯活存明細查詢 - 全部帳號");
        c.setHOSTMSGID("N524");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
                "PPSYNCN1()",		
				"PINNEW1()"
        ));
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(COUNT)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n264Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n264.txt", "N264");
        c.setDesc("香港分行帳戶存款指定期間交易明細查詢");
        c.setHOSTMSGID("N264");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
                "PPSYNCN1()",		
				"PINNEW1()"
        ));
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(COUNT)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n530Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n530.txt", "N530");
        c.setDesc("外匯定存明細查詢");
        c.setHOSTMSGID("N530");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
                "PPSYNCN1()",		
				"PINNEW1()"
        ));
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(COUNT)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n510Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n510.txt", "N510");
        c.setDesc("外匯存款餘額查詢");
        c.setHOSTMSGID("N510");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(COUNT)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n570Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n570.txt", "N570");
        c.setDesc("變更外匯進口／出口／匯兌通訊地址／電話");
        c.setHOSTMSGID("N570");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n571ExecTelcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n571.txt", "N571");
        c.setDesc("變更外匯進口／出口／匯兌通訊地址／電話");
        c.setHOSTMSGID("N571");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n270Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n270.txt", "N270");
        c.setDesc("香港分行帳戶存款餘額查詢");
        c.setHOSTMSGID("N270");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(COUNT)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n015Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n015.txt", "N015");
        c.setDesc("支存當日不足扣票據明細");
        c.setHOSTMSGID("N015");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(CNT)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n110Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n110.txt", "N110");
        c.setDesc("");
        c.setHOSTMSGID("N110");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n130Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n130.txt", "N130");
        c.setDesc("活期性存款交易明細查詢");
        c.setHOSTMSGID("N130");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n133Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n133.txt", "N133");
        c.setDesc("N133活期性存款（全部帳號）交易明細查詢");
        c.setHOSTMSGID("N133");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n150Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n150.txt", "N150");
        c.setDesc("輕鬆理財存摺明細");
        c.setHOSTMSGID("N150");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
	protected CommonTelcomm n174Telcomm(@Qualifier("fxErrDetect") TopMsgFXDetecter errDetect) throws Exception {
    	CommonTelcomm c = createTelcomm("n174.txt", "N174");
    	c.setDesc("轉入外匯綜存定存");
    	c.setHOSTMSGID("N174");
    	c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
    	c.setErrDetect(errDetect);
    	c.setBeforeCommand(Arrays.asList(
    			"SYNC1()",
				"XMLCN()",
				"XMLCA()",
				"PPSYNCN1()",
				"PINNEW1()",
				/*
				 * 第一個參數設定, 請參考
				 * http://java.sun.com/developer/technicalArticles/Programming/sprintf/
				 * 若有小數點的設定,最後會被拿掉再做 MAC %16z 表示左靠右補0
				 */
				"MAC2({%11s,TSFACN} {%016.2f, AMOUNT} {%-10s, AMOUNT} {%19s, CERTACN})"
				));
       c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
       return c;
   }
	
    @Bean
    protected CommonTelcomm n322Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n322.txt", "N322");
        c.setDesc("基金下單線上問卷投資屬性註記");
        c.setHOSTMSGID("N322");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(fundErrDetect);
        c.setBeforeCommand(Arrays.asList(
        	"PPSYNCN1()",
    		"PINNEW1()"
        ));
        return c;
    }
    
    @Bean
    protected CommonTelcomm n323Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n323.txt", "N323");
        c.setDesc("");
        c.setHOSTMSGID("N323");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
		c.setErrDetect(fundErrDetect);
        c.setBeforeCommand(Arrays.asList(
	        		"PPSYNCN1()",//<!-- 使用 日期 -->
	        		"PINNEW1()"//<!-- 使用SH1 -->
        		));
        return c;
    }
    
    @Bean
    protected CommonTelcomm n323ExecTelcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n323_1.txt", "N323");
        c.setDesc("");
        c.setHOSTMSGID("N323");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
		c.setErrDetect(fundErrDetect);
        return c;
    }
    
    @Bean
    protected CommonTelcomm n360Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n360.txt", "N360");
        c.setDesc("");
        c.setHOSTMSGID("N360");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(fundErrDetect);
        return c;
    }
    
    @Bean
    protected CommonTelcomm n361Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n361.txt", "N361");
        c.setDesc("");
        c.setHOSTMSGID("N361");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(fundErrDetect);

        c.setBeforeCommand(Arrays.asList(

                "SYNC1()", //<!-- 使用 日期 -->				
				"XMLCN()", //<!-- 使用 日期 -->
				"XMLCA()", //<!-- 使用SH1 -->
//				<!--
//					第一個參數設定, 請參考
//					http://java.sun.com/developer/technicalArticles/Programming/sprintf/
//					若有小數點的設定,最後會被拿掉再做 MAC %16z 表示左靠右補0
//				-->
				"MAC2({%10s,CUSIDN} {%-11s, ACN_SSV} {%11s, ACN_FUD})"
        ));
        return c;
    }
    
    @Bean
    protected CommonTelcomm c053Telcomm() throws Exception {
    	CommonTelcomm c = createTelcomm("c053.txt", "C053");
    	c.setDesc("(BATCH)");
    	c.setHOSTMSGID("C053");
    	c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
    	c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
    	return c;
    }
    
	@Bean
    protected CommonTelcomm n370Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n370.txt", "N370");
        c.setDesc("即時入扣帳");
        c.setHOSTMSGID("N370");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(fundErrDetect);

        c.setBeforeCommand(Arrays.asList(

                "SYNC1()", //<!-- 使用 日期 -->				
				"PPSYNCN1()", //<!-- 使用 日期 -->
				"PINNEW1()", //<!-- 使用SH1 -->
//              <!-- 不使用 XMLCN XMLCA -->
//		    	<!-- 第一個參數設定, 請參考 http://java.sun.com/developer/technicalArticles/Programming/sprintf/ 
//		    		若有小數點的設定,最後會被拿掉再做 MAC
//		    		%16z 表示左靠右補0
//		    	-->
				"MAC2({%-11s, OUTACN} {%011.1f, AMT5} {%016d, INTSACN} )"
        ));

        c.setAfterCommand(Arrays.asList("CompareFields(ACN,OUTACN)"));


        return c;
    }
    
	@Bean
    protected CommonTelcomm n372Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n372.txt", "N372");
        c.setDesc("申購基金預約入扣帳交易");
        c.setHOSTMSGID("N372");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(fundErrDetect);

        c.setBeforeCommand(Arrays.asList(
                "SYNC1()", //<!-- 使用 日期 -->				
				"PPSYNCN1()", //<!-- 使用 日期 -->
				"PINNEW1()", //<!-- 使用SH1 -->
//              <!-- 不使用 XMLCN XMLCA -->
//		    	<!-- 第一個參數設定, 請參考 http://java.sun.com/developer/technicalArticles/Programming/sprintf/ 
//		    		若有小數點的設定,最後會被拿掉再做 MAC
//		    		%16z 表示左靠右補0
//		    	-->
				"MAC2({%-11s, OUTACN} {%011.1f, AMT5} {%016d, INTSACN} )"
        ));
        return c;
    }
	
	@Bean
    protected CommonTelcomm n373Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n373.txt", "N373");
        c.setDesc("");
        c.setHOSTMSGID("N373");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(fundErrDetect);

        c.setBeforeCommand(Arrays.asList(
                "SYNC1()", //<!-- 使用 日期 -->				
				"PPSYNCN1()", //<!-- 使用 日期 -->
				"PINNEW1()", //<!-- 使用SH1 -->
//              <!-- 不使用 XMLCN XMLCA -->
//		    	<!-- 第一個參數設定, 請參考 http://java.sun.com/developer/technicalArticles/Programming/sprintf/ 
//		    		若有小數點的設定,最後會被拿掉再做 MAC
//		    		%16z 表示左靠右補0
//		    	-->
				"MAC2({%-11s, OUTACN} {%011.1f, AMT5} {0000011111111111, INTSACN} )"
        ));
        return c;
    }
	
	@Bean
    protected CommonTelcomm n374Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n374.txt", "N374");
        c.setDesc("");
        c.setHOSTMSGID("N374");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(fundErrDetect);

        c.setBeforeCommand(Arrays.asList(
                "SYNC1()", //<!-- 使用 日期 -->				
				"PPSYNCN1()", //<!-- 使用 日期 -->
				"PINNEW1()", //<!-- 使用SH1 -->
//              <!-- 不使用 XMLCN XMLCA -->
//		    	<!-- 第一個參數設定, 請參考 http://java.sun.com/developer/technicalArticles/Programming/sprintf/ 
//		    		若有小數點的設定,最後會被拿掉再做 MAC
//		    		%16z 表示左靠右補0
//		    	-->
				"MAC2({%-11s, FUNDACN} {%014.2f, FUNDAMT} {%-10s, FUNDAMT} {0000000000000000, CREDITNO} )"
        ));
        return c;
    }
	
	@Bean
    protected CommonTelcomm n392Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n392.txt", "N392");
        c.setDesc("申購基金預約入扣帳取消");
        c.setHOSTMSGID("N392");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(fundErrDetect);

        c.setBeforeCommand(Arrays.asList(			
				"PPSYNCN1()", //<!-- 使用 日期 -->
				"PINNEW1()" //<!-- 使用SH1 -->
        ));
        return c;
    }
	
	@Bean
    protected CommonTelcomm n393Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n393.txt", "N393");
        c.setDesc("轉換基金預約入扣帳取消");
        c.setHOSTMSGID("N393");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(fundErrDetect);

        c.setBeforeCommand(Arrays.asList(			
				"PPSYNCN1()", //<!-- 使用 日期 -->
				"PINNEW1()" //<!-- 使用SH1 -->
        ));
        return c;
    }
	
	@Bean
    protected CommonTelcomm n394Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n394.txt", "N394");
        c.setDesc("贖回基金預約入扣帳取消");
        c.setHOSTMSGID("N394");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(fundErrDetect);

        c.setBeforeCommand(Arrays.asList(			
				"PPSYNCN1()", //<!-- 使用 日期 -->
				"PINNEW1()" //<!-- 使用SH1 -->
        ));
        return c;
    }
	
    @Bean
    protected CommonTelcomm n420Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n420.txt", "N420");
        c.setDesc("定期性存款明細查詢");
        c.setHOSTMSGID("N420");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setFieldFormat(ImmutableMap.of("ITR", "N:#0.000"));
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n421Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n421.txt", "N421");
        c.setDesc("綜存定期性存款明細查詢");
        c.setHOSTMSGID("N421");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n615Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n615.txt", "N615");
        c.setDesc("虛擬帳號入帳明細");
        c.setHOSTMSGID("N615");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n625Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n625.txt", "N625");
        c.setDesc("支存已兌現票據明細");
        c.setHOSTMSGID("N625");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n640Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n640.txt", "N640");
        c.setDesc("輕鬆理財證券交割明細");
        c.setHOSTMSGID("N640");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n650Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n650.txt", "N650");
        c.setDesc("輕鬆理財自動申購基金明細");
        c.setHOSTMSGID("N650");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setFieldFormat(ImmutableMap.<String, String>builder()
        		.put("FUNDAMT","N:#0")
				.put("FUNDPNT","N:#0.0000")
				.put("BYPRICE","N:#0.0000")
        		.put("ITR","N:#0.0000")
        		.put("REFAVL","N:#0.0000")
        		.put("BAL","N:#0.000")
        		.build()
        );
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n812Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n812.txt", "N812");
        c.setDesc("信用卡卡號查詢");
        c.setHOSTMSGID("N812");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(CNT)"));
        return c;
    }
    
    @Bean
    protected CommonTelcomm n832Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n832.txt", "N832");
        c.setDesc("繳款紀錄查詢");
        c.setHOSTMSGID("N832");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }
    
    @Bean
    protected CommonTelcomm n860Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n860.txt", "N860");
        c.setDesc("託收票據明細");
        c.setHOSTMSGID("N860");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setFieldFormat(ImmutableMap.of(
        		"AMOUNT","N:#0.00",
        		"REMAMT","N:#0.00",
        		"VALAMT","N:#0.00"
        ));
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(CNT)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n870Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n870.txt", "N870");
        c.setDesc("中央登錄債券餘額");
        c.setHOSTMSGID("N870");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n871Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n871.txt", "N871");
        c.setDesc("中央登錄債券明細");
        c.setHOSTMSGID("N871");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n900Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n900.txt", "N900");
        c.setDesc("變更信用卡通訊地址／電話");
        c.setHOSTMSGID("N900");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);
        c.setBeforeCommand(Arrays.asList(
        		"SYNC1()",//<!-- 使用 日期 -->
        		"PPSYNCN1()",//<!-- 使用 日期 -->
        		"PINNEW1()"//<!-- 使用SH1 -->
        ));
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n900ExecTelcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n900.txt", "N900");
        c.setDesc("變更信用卡通訊地址／電話");
        c.setHOSTMSGID("N900");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);
        
		/* asynchronous--彥博要在跟景森確認 */
        c.setBeforeCommand(Arrays.asList(
        		"SYNC1()", //<!-- 使用 日期 -->				
        		"XMLCN()", //
        		"XMLCA()", //
        		"PPSYNCN1()"
        ));
        
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n921Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n921.txt", "N921");
        c.setDesc("約定帳戶查詢");
        c.setHOSTMSGID("N921");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(COUNT)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n920Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n920.txt", "N920");
        c.setDesc("歸戶查詢");
        c.setHOSTMSGID("N920");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(COUNT)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n910Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n910.txt", "N910");
        c.setDesc("查詢密碼確認");
        c.setHOSTMSGID("N910");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
        		"PPSYNCN1()",
        		"PINNEW1()"
        ));
        return c;
    }
    
    @Bean
    protected CommonTelcomm n911Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n911.txt", "N911");
        c.setDesc("網路通行碼檢核");
        c.setHOSTMSGID("N911");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);
        
        c.setBeforeCommand(Arrays.asList(

                "PPSYNCN1()", //
                "PINNEW1()" //
        ));
        return c;
    }
    
    @Bean
    protected CommonTelcomm n912Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n912.txt", "N912");
        c.setDesc("業務權限及約定功能");
        c.setHOSTMSGID("N912");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);
        return c;
    }
    
    @Bean
    protected CommonTelcomm n922Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception{
        CommonTelcomm c = createTelcomm("n922.txt","N922");
        c.setDesc("基金下單歸戶帳號查詢");
        c.setHOSTMSGID("N922");
        c.setErrDetect(fundErrDetect);
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        return c;
    }
    @Bean
    protected CommonTelcomm c021Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception{
        CommonTelcomm c = createTelcomm("c021.txt","C021");
        c.setDesc("基金轉換交易查詢");
        c.setHOSTMSGID("C021");
        c.setErrDetect(fundErrDetect);
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        return c;
    }
    
	@Bean
    protected CommonTelcomm c022Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception{
        CommonTelcomm c = createTelcomm("c022.txt","C022");
        c.setDesc("基金手續費檢核");
        c.setHOSTMSGID("C022");
        c.setErrDetect(fundErrDetect);
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        return c;
    }
	
    @Bean
    protected CommonTelcomm c031Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception{
        CommonTelcomm c = createTelcomm("c031.txt","C031");
        c.setDesc("手續費檢核");
        c.setHOSTMSGID("C031");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
		c.setErrDetect(fundErrDetect);        
        return c;
    }
	
	@Bean
    protected CommonTelcomm n371Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception{
        CommonTelcomm c = createTelcomm("n371.txt","N371");
        c.setDesc("轉換基金即時入扣帳交易");
        c.setHOSTMSGID("N371");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(fundErrDetect);
        c.setBeforeCommand(Arrays.asList(
        		"SYNC1()",
        		"PPSYNCN1()",
        		"PINNEW1()",
        		"MAC2({%-11s, OUTACN} {%011.1f, AMT5} {%016d, INTSACN} )"
        ));
        return c;
    }
	
	@Bean
    protected CommonTelcomm c023Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception{
        CommonTelcomm c = createTelcomm("c023.txt","C023");
        c.setDesc("轉換交易結果通知");
        c.setHOSTMSGID("C023");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(fundErrDetect);
        return c;
    }
	
//	改為N944
//    @Bean
//    protected CommonTelcomm n940Telcomm() throws Exception {
//        CommonTelcomm c = createTelcomm("n940.txt", "N940");
//        c.setDesc("簽入密碼變更");
//        c.setHOSTMSGID("N940");
//        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
//        return c;
//    }

	@Bean
	protected CommonTelcomm n944Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
		CommonTelcomm c = createTelcomm("n944.txt", "N944");
		c.setDesc("簽入密碼變更");
		c.setHOSTMSGID("N944");
		c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
		c.setErrDetect(dbErrDetect);

        c.setBeforeCommand(Arrays.asList(
				"PPSYNCN1()", //<!-- 使用 日期 -->
				"PINOLD1(PINOLD)", //<!-- 使用SH1 -->
				"PINNEW1(PINNEW)"//<!-- 使用SH1 -->
        ));

        c.setAfterCommand(Arrays.asList("CompareFields(ACN,OUTACN)"));


        return c;
	}

//	改為N945
//  @Bean
//  protected CommonTelcomm n941Telcomm() throws Exception {
//      CommonTelcomm c = createTelcomm("n941.txt", "N941");
//      c.setDesc("執行交易密碼變更");
//      c.setHOSTMSGID("N941");
//      c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
//      return c;
//  }
		
	@Bean
	protected CommonTelcomm n945Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
		CommonTelcomm c = createTelcomm("n945.txt", "N945");
		c.setDesc("簽入密碼變更");
		c.setHOSTMSGID("N945");
		c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
		c.setErrDetect(dbErrDetect);

		c.setBeforeCommand(Arrays.asList(
				"PPSYNCN1()", //<!-- 使用 日期 -->
				"PINOLD1(PINOLD)", //<!-- 使用SH1 -->
				"PINNEW1(PINNEW)"//<!-- 使用SH1 -->
        ));

        return c;
	}

//	景森說只用n950CMTelcomm
    @Bean
    protected CommonTelcomm n950Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n950.txt", "N950");
        c.setDesc("查詢密碼確認");
        c.setHOSTMSGID("N950");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
        	"PPSYNCN1()",
    		"PINNEW1()"
        ));
        return c;
    }
	
//	景森說只用n951CMTelcomm
    @Bean
    protected CommonTelcomm n951Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n951.txt", "N951");
        c.setDesc("查詢密碼確認");
        c.setHOSTMSGID("N951");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
        	"PPSYNCN1()",
    		"PINNEW1()"
        ));
        return c;
    }
    
//	小郭說只用n951CMTelcomm
    @Bean
    protected CommonTelcomm n950CMTelcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
    	CommonTelcomm c = createTelcomm("n950.txt", "N950");
    	c.setDesc("查詢密碼確認");
    	c.setHOSTMSGID("N950");
    	c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
    	c.setErrDetect(dbErrDetect);
    	
    	c.setBeforeCommand(Arrays.asList(
    			"PPSYNCN1()",
    			"PINNEW1()"	
		));
    	return c;
    }

	@Bean
    protected CommonTelcomm n951CMTelcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n951.txt", "N951");
        c.setDesc("查詢密碼確認");
        c.setHOSTMSGID("N951");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);

        c.setBeforeCommand(Arrays.asList(
    		"PPSYNCN1()",
    		"PINNEW1()"	
		));
        return c;
    }
    
    @Bean
    protected CommonTelcomm n953Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n953.txt", "N953");
        c.setDesc("持卡人身份證字號查詢");
        c.setHOSTMSGID("N953");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        return c;
    }

    @Bean
    protected CommonTelcomm n954Telcomm(@Qualifier("n950ErrDetect") TopMsgN950Detecter n950ErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n954.txt", "N954");
        c.setDesc("預約轉帳晶片押碼認證");
        c.setHOSTMSGID("N954");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(n950ErrDetect);
        return c;
    }

    @Bean
    protected CommonTelcomm n940Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n940.txt", "N940");
        c.setDesc("簽入密碼變更");
        c.setHOSTMSGID("N940");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
				"PPSYNCN1()", //<!-- 使用 日期 -->
				"PINOLD1(PINOLD)", //<!-- 使用SH1 -->
				"PINNEW1(PINNEW)"//<!-- 使用SH1 -->
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n941Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n941.txt", "N941");
        c.setDesc("執行交易密碼變更");
        c.setHOSTMSGID("N941");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
				"PPSYNCN1()", //<!-- 使用 日期 -->
				"PINOLD1(PINOLD)", //<!-- 使用SH1 -->
				"PINNEW1(PINNEW)"//<!-- 使用SH1 -->
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n942Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n942.txt", "N942");
        c.setDesc("");
        c.setHOSTMSGID("N942");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        return c;
    }

    @Bean
    protected CommonTelcomm n553Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n553.txt", "N553");
        c.setDesc("外幣下期借款本息查詢查詢");
        c.setHOSTMSGID("N553");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }
    
    @Bean
    protected CommonTelcomm n014Telcomm(@Qualifier("n320ErrDetect") TopMsgN320Detecter n320ErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n014.txt", "N014");
        c.setDesc("下期借款本息查詢");
        c.setHOSTMSGID("N014");
        c.setErrDetect(n320ErrDetect);
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n016Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n016.txt", "N016");
        c.setDesc("本息償還明細查詢");
        c.setHOSTMSGID("N016");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setFieldFormat(ImmutableMap.of("BAL", "N:#0"));
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

	@Bean
    protected CommonTelcomm n070Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n070.txt", "N070");
        c.setDesc("");
        c.setHOSTMSGID("N070");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);

        c.setBeforeCommand(Arrays.asList(

                "SYNC1()", //<!-- 使用 日期 -->				
				"XMLCN()", //
				"XMLCA()", //
				"PPSYNCN1()", //
				"PINNEW1()", //
//                第一個參數設定, 請參考  http://java.sun.com/developer/technicalArticles/Programming/sprintf/
//                若有小數點的設定,最後會被拿掉再做 MAC %16z 表示左靠右補0
				"MAC2({%11s,ACN} {%-16s, TSFACN} {%010d, AMOUNT} {%19s, CERTACN})"
        ));

        c.setAfterCommand(Arrays.asList("CompareFields(ACN,OUTACN)"));


        return c;
    }
    
    @Bean
    protected CommonTelcomm n071Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
    	CommonTelcomm c = createTelcomm("n071.txt", "N071");
        c.setDesc("繳稅");
        c.setHOSTMSGID("N071");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);
        c.setBeforeCommand(Arrays.asList(

                "SYNC1()", //<!-- 使用 日期 -->				
                "XMLCN()", //
                "XMLCA()", //
                "PPSYNCN1()", //
                "PINNEW1()", //
//                第一個參數設定, 請參考  http://java.sun.com/developer/technicalArticles/Programming/sprintf/
//                若有小數點的設定,最後會被拿掉再做 MAC %16z 表示左靠右補0
				"MAC2({%11s,OUTACN} {%-16s, INTSACN} {%010d, AMOUNT} {%19s, CERTACN})"
        ));

        return c;
    }

    @Bean
    protected CommonTelcomm n073Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n073.txt", "N073");
        c.setDesc("繳納期貨保證金");
        c.setHOSTMSGID("N073");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);

        c.setBeforeCommand(Arrays.asList(

                "SYNC1()", //<!-- 使用 日期 -->				
                "XMLCN()", //
                "XMLCA()", //
                "PPSYNCN1()", //
                "PINNEW1()", //
//                第一個參數設定, 請參考  http://java.sun.com/developer/technicalArticles/Programming/sprintf/
//                若有小數點的設定,最後會被拿掉再做 MAC %16z 表示左靠右補0
				"MAC2({%-11s, ACN} {%-14z, INTSACN} {%010d, AMOUNT} {%19s, CERTACN})"
        ));
        return c;
    }
	
    @Bean
    protected CommonTelcomm n074Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n074.txt", "N074");
        c.setDesc("轉入臺幣綜存定存");
        c.setHOSTMSGID("N074");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setFieldFormat(ImmutableMap.of("ITR", "N:#0.0000"));
        c.setErrDetect(dbErrDetect);
        
        c.setBeforeCommand(Arrays.asList(
                "SYNC1()", //<!-- 使用 日期 -->				
                "XMLCN()", //
                "XMLCA()", //
                "PPSYNCN1()", //<!-- 使用 日期 -->
                "PINNEW1()", //!-- 使用SH1 -->
					//第一個參數設定, 請參考
					//http://java.sun.com/developer/technicalArticles/Programming/sprintf/
					//若有小數點的設定,最後會被拿掉再做 MAC %16z 表示左靠右補0
				"MAC2({%-11s,ACN} {%-16z, INTSACN} {%010d, AMOUNT} {%19s, CERTACN})"
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n075Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
    	CommonTelcomm c = createTelcomm("n075.txt", "N075");
        c.setDesc("電費");
        c.setHOSTMSGID("N075");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);

        c.setBeforeCommand(Arrays.asList(

                "SYNC1()", //<!-- 使用 日期 -->				
                "XMLCN()", //
                "XMLCA()", //
                "PPSYNCN1()", //
                "PINNEW1()", //
//                第一個參數設定, 請參考  http://java.sun.com/developer/technicalArticles/Programming/sprintf/
//                若有小數點的設定,最後會被拿掉再做 MAC %16z 表示左靠右補0
                "MAC2({%11s, OUTACN} {0000000000000000, INTSACN} {%010d, AMOUNT} {%19s, CERTACN})"
        ));

        return c;
    }

    @Bean
    protected CommonTelcomm n750aTelcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n750a.txt", "N750A");
        c.setDesc("臺灣省自來水費");
        c.setHOSTMSGID("N750A");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        
        c.setBeforeCommand(Arrays.asList(

                "SYNC1()", //<!-- 使用 日期 -->				
                "XMLCN()", //
                "XMLCA()", //
                "PPSYNCN1()", //
                "PINNEW1()", //
//                第一個參數設定, 請參考  http://java.sun.com/developer/technicalArticles/Programming/sprintf/
//                若有小數點的設定,最後會被拿掉再做 MAC %16z 表示左靠右補0
				"MAC2({%11s,OUTACN} {%08d, AMOUNT} {%19s,CERTACN})"
        ));
        
        return c;
    }

    @Bean
    protected CommonTelcomm n750bTelcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n750b.txt", "N750B");
        c.setDesc("台北市自來水費");
        c.setHOSTMSGID("N750B");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        
        c.setBeforeCommand(Arrays.asList(

                "SYNC1()", //<!-- 使用 日期 -->				
                "XMLCN()", //
                "XMLCA()", //
                "PPSYNCN1()", //
                "PINNEW1()", //
//                第一個參數設定, 請參考  http://java.sun.com/developer/technicalArticles/Programming/sprintf/
//                若有小數點的設定,最後會被拿掉再做 MAC %16z 表示左靠右補0
				"MAC2({%11s,OUTACN} {%08d, AMOUNT} {%19s,CERTACN})"
        ));
        
        return c;
    }

    @Bean
    protected CommonTelcomm n750cTelcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n750c.txt", "N750C");
        c.setDesc("勞保費");
        c.setHOSTMSGID("N750C");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        
        c.setBeforeCommand(Arrays.asList(

                "SYNC1()", //<!-- 使用 日期 -->				
                "XMLCN()", //
                "XMLCA()", //
                "PPSYNCN1()", //
                "PINNEW1()", //
//                第一個參數設定, 請參考  http://java.sun.com/developer/technicalArticles/Programming/sprintf/
//                若有小數點的設定,最後會被拿掉再做 MAC %16z 表示左靠右補0
				"MAC2({%11s,OUTACN} {%09d, AMOUNT} {%19s,CERTACN})"
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n750hTelcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n750h.txt", "N750H");
        c.setDesc("新制勞工退休準備金");
        c.setHOSTMSGID("N750H");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        
        c.setBeforeCommand(Arrays.asList(

                "SYNC1()", //<!-- 使用 日期 -->				
                "XMLCN()", //
                "XMLCA()", //
                "PPSYNCN1()", //
                "PINNEW1()", //
//                第一個參數設定, 請參考  http://java.sun.com/developer/technicalArticles/Programming/sprintf/
//                若有小數點的設定,最後會被拿掉再做 MAC %16z 表示左靠右補0
				"MAC2({%11s,OUTACN} {%09d, AMOUNT} {%19s,CERTACN})"
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n750dTelcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n750d.txt", "N750D");
        c.setDesc("和信電信費");
        c.setHOSTMSGID("N750D");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        
        c.setBeforeCommand(Arrays.asList(

                "SYNC1()", //<!-- 使用 日期 -->				
                "XMLCN()", //
                "XMLCA()", //
                "PPSYNCN1()", //
                "PINNEW1()", //
//                第一個參數設定, 請參考  http://java.sun.com/developer/technicalArticles/Programming/sprintf/
//                若有小數點的設定,最後會被拿掉再做 MAC %16z 表示左靠右補0
				"MAC2({%11s, OUTACN} {%14z, BARCODE} {%09d, AMOUNT} {%19s, CERTACN})"
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n079Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n079.txt", "N079");
        c.setDesc("綜存定存解約");
        c.setHOSTMSGID("N079");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        
		c.setBeforeCommand(Arrays.asList(
				"SYNC1()", // <!-- 使用 日期 -->
				"XMLCN()", //
				"XMLCA()", //
				"PPSYNCN1()", //<!-- 使用 日期 -->
				//第一個參數設定, 請參考
				//http://java.sun.com/developer/technicalArticles/Programming/sprintf/
				//若有小數點的設定,最後會被拿掉再做 MAC %16z 表示左靠右補0
				"MAC2({%11s, FDPACN} {%-7s, FDPNUM} {%19s, CERTACN})"));
        return c;
    }

    @Bean
    protected CommonTelcomm n079ExecTelcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n079_1.txt", "N079_1");
        c.setDesc("綜存定存解約");
        c.setHOSTMSGID("N079_1");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
				"SYNC1()", // <!-- 使用 日期 -->
				"XMLCN()", //
				"XMLCA()", //
				"PPSYNCN1()", //<!-- 使用 日期 -->
				"PINNEW1()", //<!-- 使用SH1 -->
				//第一個參數設定, 請參考
				//http://java.sun.com/developer/technicalArticles/Programming/sprintf/
				//若有小數點的設定,最後會被拿掉再做 MAC %16z 表示左靠右補0
				"MAC2({%11s, FDPACN} {%-7s, FDPNUM} {%19s, CERTACN})"));
        return c;
    }

	@Bean
	protected CommonTelcomm n077Telcomm() throws Exception {
		CommonTelcomm c = createTelcomm("n077.txt", "N077");
		c.setDesc("定期性存款－自動轉期");
		c.setHOSTMSGID("N077");
		c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);

		c.setBeforeCommand(Arrays.asList(

				"SYNC1()", // <!-- 使用 日期 -->
				"XMLCN()", //
				"XMLCA()", //
				"PPSYNCN1()", //
				"PINNEW1()", //
				// 第一個參數設定, 請參考
				// http://java.sun.com/developer/technicalArticles/Programming/sprintf/
				// 若有小數點的設定,最後會被拿掉再做 MAC %16z 表示左靠右補0
				"MAC2({%11s, FDPACN} {%-7s, FDPNUM} {%-11s, TSFACN} {%19s, CERTACN})"));
		return c;
	}

    @Bean
    protected CommonTelcomm n078Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n078.txt", "N078");
        c.setDesc("定期性存款－續存");
        c.setHOSTMSGID("N078");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setFieldFormat(ImmutableMap.of("ITR", "N:#0.0000"));
		c.setBeforeCommand(Arrays.asList(
				"SYNC1()",//使用 日期
				"XMLCN()",
				"XMLCA()",
				"PPSYNCN1()",//使用 日期
				"PINNEW1()", //<!-- 使用SH1 -->
//					第一個參數設定, 請參考
//					http://java.sun.com/developer/technicalArticles/Programming/sprintf/
//					若有小數點的設定,最後會被拿掉再做 MAC %16z 表示左靠右補0
				"MAC2({%11s, FDPACN} {%-7s, FDPNUM} {%-11s, TSFACN} {%19s, CERTACN})"));
        return c;
    }

    @Bean
    protected CommonTelcomm n076Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n076.txt", "N076");
        c.setDesc("轉入台幣零存整付定存");
        c.setHOSTMSGID("N076");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setFieldFormat(ImmutableMap.of("ITR", "N:#0"));
		c.setBeforeCommand(Arrays.asList(
				"SYNC1()",//使用 日期
				"XMLCN()",
				"XMLCA()",
				"PPSYNCN1()",//使用 日期
				"PINNEW1()", //<!-- 使用SH1 -->
//					第一個參數設定, 請參考
//				http://java.sun.com/developer/technicalArticles/Programming/sprintf/
//				若有小數點的設定,最後會被拿掉再做 MAC %16z 表示左靠右補0
				"MAC2({%11s, OUTACN} {%-11s, FDPACN} {%010d, AMOUNT} {%19s, CERTACN})"));
        return c;
    }
	
	@Bean
    protected CommonTelcomm n072Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n072.txt", "N072");
        c.setDesc("");
        c.setHOSTMSGID("N072");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);

        c.setBeforeCommand(Arrays.asList(

                "SYNC1()", //<!-- 使用 日期 -->				
				"XMLCN()", //
				"XMLCA()", //
				"PPSYNCN1()", //
				"PINNEW1()", //
//                第一個參數設定, 請參考  http://java.sun.com/developer/technicalArticles/Programming/sprintf/
//                若有小數點的設定,最後會被拿掉再做 MAC %16z 表示左靠右補0
				"MAC2({%-11s,ACN} {%-16s, CARDNUM} {%010d, AMOUNT} {%19s, CERTACN})"
        ));

        return c;
    }
	
	@Bean
    protected CommonTelcomm n072Telcomm_m(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("m072-1.txt", "M072-1");
        c.setDesc("");
        c.setHOSTMSGID("M072-1");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);

        c.setBeforeCommand(Arrays.asList(

                "SYNC1()", //<!-- 使用 日期 -->				
				"XMLCN()", //
				"XMLCA()", //
				"PPSYNCN1()", //
				"PINNEW1()", //
//                第一個參數設定, 請參考  http://java.sun.com/developer/technicalArticles/Programming/sprintf/
//                若有小數點的設定,最後會被拿掉再做 MAC %16z 表示左靠右補0
				"MAC2({%-11s,ACN} {%-16s, CARDNUM} {%010d, AMOUNT} {%19s, CERTACN})"
        ));

        return c;
    }
	
	
    @Bean
    protected CommonTelcomm n971Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n971.txt", "N971");
        c.setDesc("");
        c.setHOSTMSGID("N971");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        
        c.setBeforeCommand(Arrays.asList(
                "SYNC1()", //<!-- 使用 日期 -->				
				"XMLCN()", //
				"XMLCA()", //
				"PPSYNCN1()", //
				"PINNEW1()"
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n554Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n554.txt", "N554");
        c.setDesc("本息償還明細查詢查詢");
        c.setHOSTMSGID("N554");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        return c;
    }

    @Bean
    protected CommonTelcomm n552Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n552.txt", "N552");
        c.setDesc("借款明細查詢");
        c.setHOSTMSGID("N552");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        return c;
    }

    @Bean
    protected CommonTelcomm n320Telcomm(@Qualifier("n320ErrDetect") TopMsgN320Detecter n320ErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n320.txt", "N320");
        c.setDesc("借款明細查詢");
        c.setHOSTMSGID("N320");
        c.setErrDetect(n320ErrDetect);
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }   

    @Bean
    protected CommonTelcomm n550Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n550.txt", "N550");
        c.setDesc("當日信用狀通知查詢");
        c.setHOSTMSGID("N550");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n551Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n551.txt", "N551");
        c.setDesc("當日信用狀開狀查詢");
        c.setHOSTMSGID("N551");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n830Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n830.txt", "N830");
        c.setDesc("申請代扣繳");
        c.setHOSTMSGID("N830");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);
        c.setBeforeCommand(Arrays.asList(
				"XMLCN()",
				"XMLCA()",
				"PPSYNCN1()",//<!-- 使用 日期 -->
				"PINNEW1()"//<!-- 使用SH1 -->
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n8301Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n8301.txt", "N8301");
        c.setDesc("申請健保費代扣繳");
        c.setHOSTMSGID("N8301");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);
        c.setBeforeCommand(Arrays.asList(
				"XMLCN()",
				"XMLCA()",
				"PPSYNCN1()",//<!-- 使用 日期 -->
				"PINNEW1()"//<!-- 使用SH1 -->
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n8302Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n8302.txt", "N8302");
        c.setDesc("申請勞保費代扣繳");
        c.setHOSTMSGID("N8302");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);
        c.setBeforeCommand(Arrays.asList(
				"XMLCN()",
				"XMLCA()",
				"PPSYNCN1()",//<!-- 使用 日期 -->
				"PINNEW1()"//<!-- 使用SH1 -->
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n8302_1Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n8302_1.txt", "N8302_1");
        c.setDesc("舊制勞工退休準備金代扣繳");
        c.setHOSTMSGID("N8302_1");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);
        c.setBeforeCommand(Arrays.asList(
				"XMLCN()",
				"XMLCA()",
				"PPSYNCN1()",//<!-- 使用 日期 -->
				"PINNEW1()"//<!-- 使用SH1 -->
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n555Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n555.txt", "N555");
        c.setDesc("商業信用狀開狀查詢-到單查詢");
        c.setHOSTMSGID("N555");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n566Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n566.txt", "N566");
        c.setDesc("匯出匯款查詢");
        c.setHOSTMSGID("N566");
        c.setDefaultFieldFormatDefine(fxFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }
    
    @Bean
    protected CommonTelcomm n556Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n556.txt", "N556");
        c.setDesc("商業信用狀開狀查詢-明細查詢");
        c.setHOSTMSGID("N556");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }
    
    @Bean
    protected CommonTelcomm n557Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n557.txt", "N557");
        c.setDesc("擔保信用狀/保證函開狀查詢");
        c.setHOSTMSGID("N557");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n840Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n840.txt", "N840");
        c.setDesc("");
        c.setHOSTMSGID("N840");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n841Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n841.txt", "N841");
        c.setDesc("");
        c.setHOSTMSGID("N841");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n842Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n842.txt", "N842");
        c.setDesc("");
        c.setHOSTMSGID("N842");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(COUNT)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n843Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n843.txt", "N843");
        c.setDesc("");
        c.setHOSTMSGID("N843");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n844Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n844.txt", "N844");
        c.setDesc("");
        c.setHOSTMSGID("N844");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n845Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n845.txt", "N845");
        c.setDesc("");
        c.setHOSTMSGID("N845");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n8501Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n8501.txt", "N8501");
        c.setDesc("掛失申請");
        c.setHOSTMSGID("N8501");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);
        
        c.setBeforeCommand(Arrays.asList(
        		"PPSYNCN1()",
        		"PINNEW1()"
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n8502Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n8502.txt", "N8502");
        c.setDesc("掛失申請");
        c.setHOSTMSGID("N8502");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);
        
        c.setBeforeCommand(Arrays.asList(
        		"PPSYNCN1()",
        		"PINNEW1()"
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n8503Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n8503.txt", "N8503");
        c.setDesc("掛失申請");
        c.setHOSTMSGID("N8503");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);
        
        c.setBeforeCommand(Arrays.asList(
        		"PPSYNCN1()",
        		"PINNEW1()"
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n8504Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n8504.txt", "N8504");
        c.setDesc("掛失申請");
        c.setHOSTMSGID("N8504");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);
        
        c.setBeforeCommand(Arrays.asList(
        		"PPSYNCN1()",
        		"PINNEW1()"
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n8505Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n8505.txt", "N8505");
        c.setDesc("掛失申請");
        c.setHOSTMSGID("N8505");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);
        
        c.setBeforeCommand(Arrays.asList(
        		"PPSYNCN1()",
        		"PINNEW1()"
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n8506Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n8506.txt", "N8506");
        c.setDesc("掛失申請");
        c.setHOSTMSGID("N8506");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);
        
        c.setBeforeCommand(Arrays.asList(
        		"PPSYNCN1()",
        		"PINNEW1()"
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n8507Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n8507.txt", "N8507");
        c.setDesc("掛失申請");
        c.setHOSTMSGID("N8507");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);
        
        c.setBeforeCommand(Arrays.asList(
        		"PPSYNCN1()",
        		"PINNEW1()"
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n8508Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n8508.txt", "N8508");
        c.setDesc("掛失申請");
        c.setHOSTMSGID("N8508");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);
        
        c.setBeforeCommand(Arrays.asList(
        		"PPSYNCN1()",
        		"PINNEW1()"
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n8509Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n8509.txt", "N8509");
        c.setDesc("掛失申請");
        c.setHOSTMSGID("N8509");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);
        
        c.setBeforeCommand(Arrays.asList(
        		"PPSYNCN1()",
        		"PINNEW1()"
        ));
        return c;
    }
    
    @Bean
    protected CommonTelcomm n850ATelcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n850a.txt", "N850A");
        c.setDesc("黃金存褶、印鑑掛失申請");
        c.setHOSTMSGID("N850A");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);
        
        c.setBeforeCommand(Arrays.asList(
        		"PPSYNCN1()",
        		"PINNEW1()"
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm p001Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("p001.txt", "P001");
        c.setDesc("新增代收車號交易");
        c.setHOSTMSGID("P001");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        
        c.setBeforeCommand(Arrays.asList(
        		"PPSYNCN1()",
        		"PINNEW1()"
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm p002Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("p002.txt", "P002");
        c.setDesc("刪除代收車號交易");
        c.setHOSTMSGID("P002");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        
        c.setBeforeCommand(Arrays.asList(
        		"PPSYNCN1()",
        		"PINNEW1()"
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm p003Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("p003.txt", "P003");
        c.setDesc("查詢個人資料及申請代收狀況");
        c.setHOSTMSGID("P003");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(Count)"));
        return c;
    }

    @Bean
    protected CommonTelcomm p004Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("p004.txt", "P004");
        c.setDesc("查詢扣繳記錄");
        c.setHOSTMSGID("P004");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(Count)"));
        return c;
    }

    @Bean
    protected CommonTelcomm p005Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("p005.txt", "P005");
        c.setDesc("修改個人資料及扣款帳號");
        c.setHOSTMSGID("P005");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        
        c.setBeforeCommand(Arrays.asList(
        		"PPSYNCN1()",
        		"PINNEW1()"
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm p018Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("p018.txt", "P018");
        c.setDesc("查詢縣市別資料");
        c.setHOSTMSGID("P018");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        return c;
    }

    @Bean
    protected CommonTelcomm n1011Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n1011.txt", "N1011");
        c.setDesc("支票存款開戶申請");
        c.setHOSTMSGID("N1011");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
				"PPSYNCN1()",				//<!-- 使用 日期 -->
				"PINNEW1()"					//<!-- 使用SH1 -->
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n1012Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n1012.txt", "N1012");
        c.setDesc("空白票據申請");
        c.setHOSTMSGID("N1012");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);
        c.setBeforeCommand(Arrays.asList(
				"PPSYNCN1()",				//<!-- 使用 日期 -->
				"PINNEW1()"					//<!-- 使用SH1 -->
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n1013Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n1013.txt", "N1013");
        c.setDesc("利息所得扣繳憑單補發申請");
        c.setHOSTMSGID("N1013");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
				"PPSYNCN1()",				//<!-- 使用 日期 -->
				"PINNEW1()"					//<!-- 使用SH1 -->
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n1014Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n1014.txt", "N1014");
        c.setDesc("存款餘額證明申請");
        c.setHOSTMSGID("N1014");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);
        c.setBeforeCommand(Arrays.asList(
				"PPSYNCN1()",				//<!-- 使用 日期 -->
				"PINNEW1()"					//<!-- 使用SH1 -->
        ));
        return c;
    }

    //TODO:XML找不到
    @Bean
    protected CommonTelcomm n1015Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n1015.txt", "N1015");
        c.setDesc("");
        c.setHOSTMSGID("N1015");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        return c;
    }
    
    @Bean
    protected CommonTelcomm n105Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n105.txt", "N105");
        c.setDesc("個人房屋擔保借款繳息清單");
        c.setHOSTMSGID("N105");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n1060Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n1060.txt", "N1060");
        c.setDesc("利息所得扣繳憑單列印");
        c.setHOSTMSGID("N1060");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n1061Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n1061.txt", "N1061");
        c.setDesc("利息所得扣繳憑單列印");
        c.setHOSTMSGID("N1061");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        return c;
    }

    @Bean
    protected CommonTelcomm n660Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n660.txt", "N660");
        c.setDesc("補發電子交易對帳單");
        c.setHOSTMSGID("N660");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
				"PPSYNCN1()",				//<!-- 使用 日期 -->
				"PINNEW1()"					//<!-- 使用SH1 -->
        ));
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n930Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n930.txt", "N930");
        c.setDesc("用戶電子郵箱位址變更");
        c.setHOSTMSGID("N930");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
        		"SoftCertContext()",
                "IDGateCommand()"
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n931Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n931.txt", "N931");
        c.setDesc("申請、取消電子交易對帳單");
        c.setHOSTMSGID("N931");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
				"PPSYNCN1()", //<!-- 使用 日期 -->
				"PINNEW1()" //<!-- 使用SH1 -->
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n531Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n531.txt", "N531");
        c.setDesc("");
        c.setHOSTMSGID("N531");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(COUNT)"));
        return c;
    }

    @Bean
	protected CommonTelcomm n175_1Telcomm(@Qualifier("fxErrDetect") TopMsgFXDetecter errDetect) throws Exception {
	    CommonTelcomm c = createTelcomm("n175.txt", "N175");
	    c.setDesc("");
	    c.setHOSTMSGID("N175");
	    c.setErrDetect(errDetect);
	    c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
	    c.setBeforeCommand(Arrays.asList(
				"SYNC1()",				// 使用 日期
				"XMLCN()",
				"XMLCA()",
				"PPSYNCN1()",			// 使用 日期
				/*
				 * 第一個參數設定, 請參考
				 * http://java.sun.com/developer/technicalArticles/Programming/sprintf/
				 * 若有小數點的設定,最後會被拿掉再做 MAC %16z 表示左靠右補0
				 */
				"MAC2({%-11s,ACN} {%016.2f, AMT} {%-10s, AMT} {%19s, CERTACN})"
				));
	    return c;
	}

	@Bean
	protected CommonTelcomm n175_2Telcomm(@Qualifier("fxErrDetect") TopMsgFXDetecter errDetect) throws Exception {
	    CommonTelcomm c = createTelcomm("n175.txt", "N175");
	    c.setDesc("");
	    c.setHOSTMSGID("N175");
	    c.setErrDetect(errDetect);
	    c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
	    c.setBeforeCommand(Arrays.asList(
	    		"SYNC1()",				// 使用 日期
	    		"XMLCN()",
	    		"XMLCA()",
				"PPSYNCN1()",			// 使用 日期
				"PINNEW1()",			// 使用SH1
				/*
				 * 第一個參數設定, 請參考
				 * http://java.sun.com/developer/technicalArticles/Programming/sprintf/
				 * 若有小數點的設定,最後會被拿掉再做 MAC %16z 表示左靠右補0
				 */
				"MAC2({%-11s,ACN} {%016.2f, AMT} {%-10s, AMT} {%19s, CERTACN})"
				));
	    return c;
	}

	@Bean
    protected CommonTelcomm n177Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n177.txt", "N177");
        c.setDesc("外幣定期性存款－自動轉期");
        c.setHOSTMSGID("N177");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(

                "SYNC1()", 				// 使用 日期
				"XMLCN()",
				"XMLCA()",
				"PPSYNCN1()", 			// 使用 日期
				"PINNEW1()",			// 使用SH1
				/*
				 * 第一個參數設定, 請參考
				 * http://java.sun.com/developer/technicalArticles/Programming/sprintf/
				 * 若有小數點的設定,最後會被拿掉再做 MAC %16z 表示左靠右補0
				 */
				"MAC2({%11s, FYACN} {%-7s, FDPNUM} {%-11s, FYTSFAN} {%19s, CERTACN})"
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n178_1Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n178.txt", "N178");
        c.setDesc("外幣定存單到期續存");
        c.setHOSTMSGID("N178");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
                "SYNC1()", 				// 使用 日期
				"XMLCN()",
				"XMLCA()",
				"PPSYNCN1()", 			// 使用 日期
				/*
				 * 第一個參數設定, 請參考
				 * http://java.sun.com/developer/technicalArticles/Programming/sprintf/
				 * 若有小數點的設定,最後會被拿掉再做 MAC %16z 表示左靠右補0
				 */
				"MAC2({%11s, FYACN} {%-7s, FDPNUM} {%-11s, FYTSFAN} {%19s, CERTACN})"
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n178_2Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n178.txt", "N178");
        c.setDesc("外幣定存單到期續存");
        c.setHOSTMSGID("N178");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
                "SYNC1()", 				// 使用 日期
				"XMLCN()",
				"XMLCA()",
				"PPSYNCN1()", 			// 使用 日期
				"PINNEW1()",			// 使用SH1
				/*
				 * 第一個參數設定, 請參考
				 * http://java.sun.com/developer/technicalArticles/Programming/sprintf/
				 * 若有小數點的設定,最後會被拿掉再做 MAC %16z 表示左靠右補0
				 */
				"MAC2({%11s, FYACN} {%-7s, FDPNUM} {%-11s, FYTSFAN} {%19s, CERTACN})"
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n831Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n831.txt", "N831");
        c.setDesc("");
        c.setHOSTMSGID("N831");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
        		"XMLCN()",
        		"XMLCA()",
				"PPSYNCN1()",//<!-- 使用 日期 -->
				"PINNEW1()"//<!-- 使用SH1 -->
        ));
        return c;
    }
    
    @Bean
    protected CommonTelcomm n835Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n835.txt", "N835");
        c.setDesc("");
        c.setHOSTMSGID("N835");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);
        c.setBeforeCommand(Arrays.asList(
        		/*
        		 * 第一個參數設定, 請參考 http://java.sun.com/developer/technicalArticles/Programming/sprintf/
        		 * 若有小數點的設定,最後會被拿掉再做 MAC
        		 * %16z 表示左靠右補0
        		 */
                "SYNC1()", 				// 使用 日期
				"MAC2({%-16s,CARDNUM} {%-4s, EXPDATE} {%-3s, CHECKNO} {%-10s, ACNOZERO})",
				"MAC1({%[LEN]s,PRJCODE} {%[LEN]s, TRANSEQ} {%[LEN]s, PRCCODE} {%[LEN]s, CARDNUM} {%[LEN]s, EXPDATE} {%[LEN]s, CUSTIDN})"
        ));
        return c;
    }
    
    @Bean
    protected CommonTelcomm n815Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n815.txt", "N815");
        c.setDesc("各項費用代扣繳申請取消");
        c.setHOSTMSGID("N815");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
                "PPSYNCN1()",
                "PINNEW1()"
        ));
        return c;
    }


    @Bean
    protected CommonTelcomm na20Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("na20.txt", "NA20");
        c.setDesc("客戶基本資料查詢");
        c.setHOSTMSGID("NA20");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(COUNT)"));
        return c;
    }

    @Bean
    protected CommonTelcomm nk01Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("nk01.txt", "NK01");
        c.setDesc("換Key");
        c.setHOSTMSGID("NK01");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        return c;
    }

    @Bean
    protected CommonTelcomm n924Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n924.txt", "N924");
        c.setDesc("外匯指定期間帳號歸戶查詢");
        c.setHOSTMSGID("N924");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(COUNT)"));
        return c;
    }

    @Bean
    protected CommonTelcomm xmxrTelcomm() throws Exception {
        CommonTelcomm c = createTelcomm("xmxr.txt", "XMXR");
        c.setDesc("與主機對時");
        c.setHOSTMSGID("XMXR");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(CNT)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n021Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n021.txt", "N021");
        c.setDesc("");
        c.setHOSTMSGID("N021");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(COUNT)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n022Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n022.txt", "N022");
        c.setDesc("");
        c.setHOSTMSGID("N022");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(COUNT)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n023Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n023.txt", "N023");
        c.setDesc("");
        c.setHOSTMSGID("N023");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(COUNT)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n024Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n024.txt", "N024");
        c.setDesc("");
        c.setHOSTMSGID("N024");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(COUNT)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n025Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n025.txt", "N025");
        c.setDesc("");
        c.setHOSTMSGID("N025");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(COUNT)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n026Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n026.txt", "N026");
        c.setDesc("");
        c.setHOSTMSGID("N026");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(COUNT)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n030Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n030.txt", "N030");
        c.setDesc("");
        c.setHOSTMSGID("N030");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        return c;
    }
    
    @Bean
    protected CommonTelcomm n031Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n031.txt", "N031");
        c.setDesc("");
        c.setHOSTMSGID("N031");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        return c;
    }
    
    @Bean
    protected CommonTelcomm n032Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n032.txt", "N032");
        c.setDesc("");
        c.setHOSTMSGID("N032");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        return c;
    }
    
    @Bean
    protected CommonTelcomm n033Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n033.txt", "N033");
        c.setDesc("");
        c.setHOSTMSGID("N033");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        return c;
    }

    @Bean
    protected CommonTelcomm nd01Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("nd01.txt", "ND01");
        c.setDesc("與主機對時");
        c.setHOSTMSGID("ND01");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        return c;
    }

    @Bean
    protected CommonTelcomm nd08Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("nd08.txt", "ND08");
        c.setDesc("與主機對時");
        c.setHOSTMSGID("ND08");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm nd09Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("nd09.txt", "ND09");
        c.setDesc("");
        c.setHOSTMSGID("ND09");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm nd10Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("nd10.txt", "ND10");
        c.setDesc("");
        c.setHOSTMSGID("ND10");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm nd11Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("nd11.txt", "ND11");
        c.setDesc("");
        c.setHOSTMSGID("ND11");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm nd12Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("nd12.txt", "ND12");
        c.setDesc("");
        c.setHOSTMSGID("ND12");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm nd13Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("nd13.txt", "ND13");
        c.setDesc("");
        c.setHOSTMSGID("ND13");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm nd14Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("nd14.txt", "ND14");
        c.setDesc("");
        c.setHOSTMSGID("ND14");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm nd15Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("nd15.txt", "ND15");
        c.setDesc("");
        c.setHOSTMSGID("ND15");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm nd16Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("nd16.txt", "ND16");
        c.setDesc("");
        c.setHOSTMSGID("ND16");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n565Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n565.txt", "N565");
        c.setDesc("匯入匯款查詢");
        c.setHOSTMSGID("N565");
        c.setDefaultFieldFormatDefine(fxFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n558Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n558.txt", "N558");
        c.setDesc("進口到單查詢");
        c.setHOSTMSGID("N558");
        c.setDefaultFieldFormatDefine(fxFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n564Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n564.txt", "N564");
        c.setDesc("出口押匯查詢");
        c.setHOSTMSGID("N564");
        c.setDefaultFieldFormatDefine(fxFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n567Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n567.txt", "N567");
        c.setDesc("光票託收查詢");
        c.setHOSTMSGID("N567");
        c.setDefaultFieldFormatDefine(fxFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }
    
    //TODO: n568Telcomm xml找不到
    @Bean
    protected CommonTelcomm n568Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n568.txt", "N568");
        c.setDesc("");
        c.setHOSTMSGID("N568");
        c.setDefaultFieldFormatDefine(fxFieldFormatDefine);
        return c;
    }

    @Bean
    protected CommonTelcomm f013Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("f013.txt", "F013");
        c.setDesc("線上解款待解款電文");
        c.setHOSTMSGID("F013");
        c.setDefaultFieldFormatDefine(fxFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
                "PPSYNCN1()"
        ));
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

	    
    @Bean
    protected CommonTelcomm f031Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
    	CommonTelcomm c = createTelcomm("f031.txt", "F031");
    	c.setDesc("一般網銀外幣匯出匯款受款人約定檔擷取電文");
    	c.setHOSTMSGID("F031");
    	c.setErrDetect(dbErrDetect);
    	c.setDefaultFieldFormatDefine(fxFieldFormatDefine);
    	c.setAfterCommand(Arrays.asList("OccursNumFromValue(RCVCNT)"));
    	return c;
    }
	
    @Bean
    protected CommonTelcomm f035Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("f035.txt", "F035");
        c.setDesc("匯入匯款線上解款申請及註銷");
        c.setHOSTMSGID("F035");
        c.setDefaultFieldFormatDefine(fxFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
                "SYNC1()", 				// 使用 日期
				"PPSYNCN1()", 			// 使用 日期
				"PINNEW1()",			// 使用SH1
				"XMLCN()",
				"XMLCA()"
        ));
        return c;
    }
    
    //TODO:找不到對應規格
    @Bean
    protected CommonTelcomm f039Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("f039.txt", "F039");
        c.setDesc("");
        c.setHOSTMSGID("F039");
        c.setDefaultFieldFormatDefine(fxFieldFormatDefine);
        return c;
    }
    
    @Bean
    protected CommonTelcomm nhk1Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("nhk1.txt", "NHK1");
        c.setDesc("");
        c.setHOSTMSGID("NHK1");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
                "LOGINPIN()", 				// 使用 日期
				"TRANSPIN()" 			// 使用 日期
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n559Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n559.txt", "N559");
        c.setDesc("進口託收查詢");
        c.setHOSTMSGID("N559");
        c.setDefaultFieldFormatDefine(fxFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n560Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n560.txt", "N560");
        c.setDesc("信用狀項下出口託收查詢");
        c.setHOSTMSGID("N560");
        c.setDefaultFieldFormatDefine(fxFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n563Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n563.txt", "N563");
        c.setDesc("D/A、D/P出口託收查詢");
        c.setHOSTMSGID("N563");
        c.setDefaultFieldFormatDefine(fxFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n880Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n880.txt", "N880");
        c.setDesc("取消約定轉入帳號");
        c.setHOSTMSGID("N880");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
                "SYNC1()", //<!-- 使用 日期 -->				
                "PPSYNCN1()", //
                "PINNEW1()", //
				"XMLCN()", //
				"XMLCA()" //
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n750eTelcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n750e.txt", "N750E");
        c.setDesc("健保費");
        c.setHOSTMSGID("N750E");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        
        c.setBeforeCommand(Arrays.asList(

                "SYNC1()", //<!-- 使用 日期 -->				
                "XMLCN()", //
                "XMLCA()", //
                "PPSYNCN1()", //
                "PINNEW1()", //
//                第一個參數設定, 請參考  http://java.sun.com/developer/technicalArticles/Programming/sprintf/
//                若有小數點的設定,最後會被拿掉再做 MAC %16z 表示左靠右補0
				"MAC2({%11s,OUTACN} {%09d, AMOUNT} {%19s,CERTACN})"
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n750fTelcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n750f.txt", "N750F");
        c.setDesc("國民年金保險費");
        c.setHOSTMSGID("N750F");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        
        c.setBeforeCommand(Arrays.asList(

                "SYNC1()", //<!-- 使用 日期 -->				
                "XMLCN()", //
                "XMLCA()", //
                "PPSYNCN1()", //
                "PINNEW1()", //
//                第一個參數設定, 請參考  http://java.sun.com/developer/technicalArticles/Programming/sprintf/
//                若有小數點的設定,最後會被拿掉再做 MAC %16z 表示左靠右補0
				"MAC2({%11s,OUTACN} {%09d, AMOUNT} {%19s,CERTACN})"
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n750gTelcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n750g.txt", "N750G");
        c.setDesc("欣欣瓦斯費");
        c.setHOSTMSGID("N750G");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        
        c.setBeforeCommand(Arrays.asList(

                "SYNC1()", //<!-- 使用 日期 -->				
                "XMLCN()", //
                "XMLCA()", //
                "PPSYNCN1()", //
                "PINNEW1()", //
//                第一個參數設定, 請參考  http://java.sun.com/developer/technicalArticles/Programming/sprintf/
//                若有小數點的設定,最後會被拿掉再做 MAC %16z 表示左靠右補0
				"MAC2({%11s,OUTACN} {%09d, AMOUNT} {%19s,CERTACN})"
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n171Telcomm() throws Exception {
    	CommonTelcomm c = createTelcomm("n171.txt", "N171");
        c.setDesc("繳稅");
        c.setHOSTMSGID("N171");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);

        c.setBeforeCommand(Arrays.asList(

                "SYNC1()", //<!-- 使用 日期 -->				
                "PPSYNCN1()", //
                "PINNEW1()", //
				"XMLCN()", //
				"XMLCA()", //
//                第一個參數設定, 請參考  http://java.sun.com/developer/technicalArticles/Programming/sprintf/
//                若有小數點的設定,最後會被拿掉再做 MAC %16z 表示左靠右補0
				"MAC2({%11s, OUTACN} {%-16s, INTSACN} {%010d, AMOUNT} {%19s, CERTACN})"
        ));

        return c;
    }

    @Bean
    protected CommonTelcomm n108Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n108.txt", "N108");
        c.setDesc("");
        c.setHOSTMSGID("N108");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);

        c.setBeforeCommand(Arrays.asList(

                "SYNC1()", //<!-- 使用 日期 -->				
				"XMLCN()", //
				"XMLCA()", //
				"PPSYNCN1()", //
				"PINNEW1()", //
//                第一個參數設定, 請參考  http://java.sun.com/developer/technicalArticles/Programming/sprintf/
//                若有小數點的設定,最後會被拿掉再做 MAC %16z 表示左靠右補0
				"MAC2({%-11s,FDPACN} {%19s, CERTACN})"
        ));

        return c;
    }

    @Bean
    protected CommonTelcomm c900Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("c900.txt", "C900");
        c.setDesc("查詢信用卡客戶資料");
        c.setHOSTMSGID("C900");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        return c;
    }

    @Bean
    protected CommonTelcomm n8330Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n8330.txt", "N8330");
        c.setDesc("公用事業費用代扣繳申請查詢");
        c.setHOSTMSGID("N8330");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        return c;
    }

    @Bean
    protected CommonTelcomm f037Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("f037.txt", "F037");
        c.setDesc("外幣跨行匯款約定轉入帳號取消");
        c.setHOSTMSGID("F037");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
                "PPSYNCN1()", //
                "PINNEW1()", //
				"XMLCN()", //
				"XMLCA()" //
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm gd01Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("gd01.txt", "GD01");
        c.setDesc("黃金存摺牌告價格異動");
        c.setHOSTMSGID("GD01");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        return c;
    }

    @Bean
    protected CommonTelcomm gd02Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("gd02.txt", "GD02");
        c.setDesc("黃金存摺銷售資料查詢");
        c.setHOSTMSGID("GD02");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm gd11Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("gd11.txt", "GD11");
        c.setDesc("黃金存摺牌告資料查詢");
        c.setHOSTMSGID("GD11");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        return c;
    }

    @Bean
    protected CommonTelcomm n190Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n190.txt", "N190");
        c.setDesc("");
        c.setHOSTMSGID("N190");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        return c;
    }

    @Bean
    protected CommonTelcomm n191Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n191.txt", "N191");
        c.setDesc("黃金存摺交易明細查詢");
        c.setHOSTMSGID("N191");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n192Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n192.txt", "N192");
        c.setDesc("黃金定期定額約定資料查詢");
        c.setHOSTMSGID("N192");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n09001_1Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n09001.txt","N09001");
        c.setDesc("黃金存摺買進");
        c.setHOSTMSGID("N09001");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
	        "SYNC1()",			
			"XMLCN()",
			"XMLCA()",
			"PPSYNCN1()",
			"MAC2({%11s,ACN} {%11s,SVACN} {%1s,TRNAMT_SIGN} {%011d,TRNAMT} {%19s,CERTACN})"
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n09001_2Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n09001.txt","N09001");
        c.setDesc("黃金存摺買進");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setHOSTMSGID("N09001");
        c.setBeforeCommand(Arrays.asList(
	        "SYNC1()",			
			"XMLCN()",
			"XMLCA()",
			"PPSYNCN1()",
			"PINNEW1()",
			"MAC2({%11s,ACN} {%11s,SVACN} {%1s,TRNAMT_SIGN}	{%011d,TRNAMT} {%19s,CERTACN})"
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n09002_1Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n09002.txt","N09002");
        c.setDesc("黃金存摺回售");
        c.setHOSTMSGID("N09002");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
	        "SYNC1()",			
			"XMLCN()",
			"XMLCA()",
			"PPSYNCN1()",
			"MAC2({%11s,ACN} {%11s,SVACN} {%1s,TRNAMT_SIGN} {%011d,TRNAMT} {%19s,CERTACN})"
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n09002_2Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n09002.txt","N09002");
        c.setDesc("黃金存摺回售");
        c.setHOSTMSGID("N09002");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
	        "SYNC1()",			
			"XMLCN()",
			"XMLCA()",
			"PPSYNCN1()",
			"PINNEW1()",
			"MAC2({%11s,ACN} {%11s,SVACN} {%1s,TRNAMT_SIGN} {%011d,TRNAMT} {%19s,CERTACN})"
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n091Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n091.txt", "N091");
        c.setDesc("預約/取消黃金買進回售");
        c.setHOSTMSGID("N091");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
		c.setBeforeCommand(Arrays.asList(
				"SYNC1()",//<!-- 使用 日期 -->
				"XMLCN()",
				"XMLCA()",
				"PPSYNCN1()",//<!-- 使用 日期 -->
				"PINNEW1()",//<!-- 使用密碼 -->
				/*
				 * 第一個參數設定, 請參考
				 * http://java.sun.com/developer/technicalArticles/Programming/sprintf/
				 * 若有小數點的設定,最後會被拿掉再做 MAC %16z 表示左靠右補0
				 */
				"MAC2({%11s, ACN} {%11s, SVACN} {%1s, SIGN} {%014.2f,TRNGD} {%19s, CERTACN})"
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n092Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n092.txt", "N092");
        c.setDesc("黃金存摺買進／回售預約查詢");
        c.setHOSTMSGID("N092");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
		c.setBeforeCommand(Arrays.asList(
				"SYNC1()",//<!-- 使用 日期 -->
				"XMLCN()",
				"XMLCA()",
				"PPSYNCN1()",//<!-- 使用 日期 -->
				/*
				 * 第一個參數設定, 請參考
				 * http://java.sun.com/developer/technicalArticles/Programming/sprintf/
				 * 若有小數點的設定,最後會被拿掉再做 MAC %16z 表示左靠右補0
				 */
				"MAC2({%10s, CUSIDN} {%11s, ACN} {%19s, CERTACN})"
        ));
		c.setAfterCommand(Arrays.asList("OccursNumFromValue(COUNT)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n926Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n926.txt", "N926");
        c.setDesc("黃金存摺歸戶帳號查詢");
        c.setHOSTMSGID("N926");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
		c.setAfterCommand(Arrays.asList("OccursNumFromValue(COUNT)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n094QueryTelcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n094Query.txt", "N094Query");
        c.setDesc("繳納定期扣款失敗手續費-查詢");
        c.setHOSTMSGID("N094");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
		c.setBeforeCommand(Arrays.asList(
				"SYNC1()",//<!-- 使用 日期 -->
				"PPSYNCN1()",//<!-- 使用 日期 -->
				/*
				 * 第一個參數設定, 請參考
				 * http://java.sun.com/developer/technicalArticles/Programming/sprintf/
				 * 若有小數點的設定,最後會被拿掉再做 MAC %16z 表示左靠右補0
				 */
				"MAC2({%11s, ACN} {%11s, SVACN} {%19s, CERTACN})"
        ));
		c.setAfterCommand(Arrays.asList("OccursNumFromValue(ARY_CNT)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n094PayTelcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n094Pay.txt", "N094Pay");
        c.setDesc("繳納定期扣款失敗手續費-繳費");
        c.setHOSTMSGID("N094");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
		c.setBeforeCommand(Arrays.asList(
				"SYNC1()",//<!-- 使用 日期 -->
				"XMLCN()",
				"XMLCA()",
				"PPSYNCN1()",//<!-- 使用 日期 -->
				/*
				 * 第一個參數設定, 請參考
				 * http://java.sun.com/developer/technicalArticles/Programming/sprintf/
				 * 若有小數點的設定,最後會被拿掉再做 MAC %16z 表示左靠右補0
				 */
				"MAC2({%11s, ACN} {%11s, SVACN} {%19s, CERTACN})"
        ));
        return c;
    }

	@Bean
    protected CommonTelcomm n093Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n093.txt", "N093");
        c.setDesc("黃金存摺定期定額申購");
        c.setHOSTMSGID("N093");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
				"SYNC1()",//<!-- 使用 日期 -->
				"XMLCN()",
				"XMLCA()",
				"PPSYNCN1()",//<!-- 使用 日期 -->
				"PINNEW1()",//<!-- 使用SH1 -->
				/*
				 * 第一個參數設定, 請參考
				 * http://java.sun.com/developer/technicalArticles/Programming/sprintf/
				 * 若有小數點的設定,最後會被拿掉再做 MAC %16z 表示左靠右補0
				 */
				"MAC2({%11s, ACN} {%011d, AMT_06} {%011d, AMT_16} {%011d, AMT_26} {%19s, CERTACN})"
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n093QueryTelcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n093.txt", "N093");
        c.setDesc("黃金存摺定期定額查詢");
        c.setHOSTMSGID("N093");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setFieldFormat(ImmutableMap.of(
        		"AMT06", "N:#0", 
        		"AMT16", "N:#0", 
        		"AMT26", "N:#0"
        ));
        c.setBeforeCommand(Arrays.asList(
				"SYNC1()",//<!-- 使用 日期 -->
				"XMLCN()",
				"XMLCA()",
				"PPSYNCN1()",//<!-- 使用 日期 -->
				/*
				 * 第一個參數設定, 請參考
				 * http://java.sun.com/developer/technicalArticles/Programming/sprintf/
				 * 若有小數點的設定,最後會被拿掉再做 MAC %16z 表示左靠右補0
				 */
				"MAC2({%11s, ACN} {%011d, AMT_06} {%011d, AMT_16} {%011d, AMT_26} {%19s, CERTACN})"
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm na50Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("na50.txt", "NA50");
        c.setDesc("申請黃金存摺帳戶");
        c.setHOSTMSGID("NA50");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
		c.setBeforeCommand(Arrays.asList(
				"XMLCN()",
				"XMLCA()"
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm na60Telcomm(@Qualifier("n320ErrDetect") TopMsgN320Detecter n320ErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("na60.txt", "NA60");
        c.setDesc("申請黃金存摺網路交易");
        c.setHOSTMSGID("NA60");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(n320ErrDetect);
		c.setBeforeCommand(Arrays.asList(
				"XMLCN()",
				"XMLCA()"
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm na70Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("na70.txt", "NA70");
        c.setDesc("線上解鎖交易");
        c.setHOSTMSGID("NA70");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
				"XMLCN()",
				"XMLCA()"
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm na80Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("na80.txt", "NA80");
        c.setDesc("金融卡線上申請/取消轉帳功能");
        c.setHOSTMSGID("NA80");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
        		"PPSYNCN1()" 			// 使用 日期
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm np01Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("np01.txt", "NP01");
        c.setDesc("促銷活動獎次查詢");
        c.setHOSTMSGID("NP01");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(CNT)"));
        return c;
    }

    @Bean
    protected CommonTelcomm np02Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("np02.txt", "NP02");
        c.setDesc("線上抽獎交易");
        c.setHOSTMSGID("NP02");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);
        return c;
    }

    @Bean
    protected CommonTelcomm n382Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n382.txt", "N382");
        c.setDesc("申購基金預約入扣帳查詢");
        c.setHOSTMSGID("N382");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
		c.setErrDetect(fundErrDetect);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }
    
    @Bean
    protected CommonTelcomm n383Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n383.txt", "N383");
        c.setDesc("轉換基金預約入扣帳查詢");
        c.setHOSTMSGID("N383");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
		c.setErrDetect(fundErrDetect);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }
    
    @Bean
    protected CommonTelcomm n384Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n384.txt", "N384");
        c.setDesc("贖回基金預約入扣帳查詢");
        c.setHOSTMSGID("N384");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
		c.setErrDetect(fundErrDetect);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }
    
    @Bean
    protected CommonTelcomm n927Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fundErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n927.txt", "N927");
        c.setDesc("投資屬性查詢");
        c.setHOSTMSGID("N927");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(fundErrDetect);
        return c;
    }

    @Bean
    protected CommonTelcomm n100ExecTelcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n100.txt", "N100");
        c.setDesc("變更往來帳戶及信託業務通訊地址／電話");
        c.setHOSTMSGID("N100");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
		c.setBeforeCommand(Arrays.asList(
				"XMLCN()",
				"XMLCA()"
        ));
		c.setAfterCommand(Arrays.asList("OccursNumFromValue(COUNT)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n913Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n913.txt", "N913");
        c.setDesc("行動銀行啟用／關閉");
        c.setHOSTMSGID("N913");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);
        return c;
    }

    @Bean
    protected CommonTelcomm n070Telcomm_m(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("m070-1.txt", "M070-1");
        c.setDesc("");
        c.setHOSTMSGID("M070-1");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);
        c.setBeforeCommand(Arrays.asList(
				"SYNC1()",
				"XMLCN()",
				"XMLCA()",
				"PPSYNCN1()",
				"PINNEW1()",
				/*
				 * 第一個參數設定, 請參考
				 * http://java.sun.com/developer/technicalArticles/Programming/sprintf/
				 * 若有小數點的設定,最後會被拿掉再做 MAC %16z 表示左靠右補0
				 */
				"MAC2({%11s, ACN} {%-16s, TSFACN} {%010d, AMOUNT} {%19s, CERTACN})"
        ));
        c.setAfterCommand(Arrays.asList("CompareFields(ACN,OUTACN)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n075Telcomm_m(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("m075.txt", "M075");
        c.setDesc("電費");
        c.setHOSTMSGID("M075");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);
        c.setBeforeCommand(Arrays.asList(
				"SYNC1()",
				"XMLCN()",
				"XMLCA()",
				"PPSYNCN1()",
				"PINNEW1()",
				/*
				 * 第一個參數設定, 請參考
				 * http://java.sun.com/developer/technicalArticles/Programming/sprintf/
				 * 若有小數點的設定,最後會被拿掉再做 MAC %16z 表示左靠右補0
				 */
				"MAC2({%11s, OUTACN} {0000000000000000, INTSACN} {%010d, AMOUNT} {%19s, CERTACN})"
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n750aTelcomm_m() throws Exception {
        CommonTelcomm c = createTelcomm("m750-14.txt", "M750-14");
        c.setDesc("臺灣省自來水費");
        c.setHOSTMSGID("M750-14");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
				"SYNC1()",
				"XMLCN()",
				"XMLCA()",
				"PPSYNCN1()",
				"PINNEW1()",
				/*
				 * 第一個參數設定, 請參考
				 * http://java.sun.com/developer/technicalArticles/Programming/sprintf/
				 * 若有小數點的設定,最後會被拿掉再做 MAC %16z 表示左靠右補0
				 */
				"MAC2({%11s, OUTACN} {%08d, AMOUNT} {%19s, CERTACN})"
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n750bTelcomm_m() throws Exception {
        CommonTelcomm c = createTelcomm("m750-04.txt", "M750-04");
        c.setDesc("台北市自來水費");
        c.setHOSTMSGID("M750-04");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
				"SYNC1()",
				"XMLCN()",
				"XMLCA()",
				"PPSYNCN1()",
				"PINNEW1()",
				/*
				 * 第一個參數設定, 請參考
				 * http://java.sun.com/developer/technicalArticles/Programming/sprintf/
				 * 若有小數點的設定,最後會被拿掉再做 MAC %16z 表示左靠右補0
				 */
				"MAC2({%11s, OUTACN} {%08d, AMOUNT} {%19s, CERTACN})"
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n934Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n934.txt", "N934");
        c.setDesc("線上登錄/取消匯款通知作業");
        c.setHOSTMSGID("N934");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        //20200210 ian 確認無使用 ,註解以免call Va 造成 Z300
        //        c.setBeforeCommand(Arrays.asList(
        //				"PPSYNCN1()",
        //				"PINNEW1()"
        //        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n935Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n935.txt", "N935");
        c.setDesc("匯入匯款設定通知查詢");
        c.setHOSTMSGID("N935");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n185Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n185.txt", "N185");
        c.setDesc("國內匯入匯款通知");
        c.setHOSTMSGID("N185");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(REC_NO)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n3003Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("3003.txt", "3003");
        c.setDesc("借款還款");
        c.setHOSTMSGID("3003");
        c.setBeforeCommand(Arrays.asList(
				"SYNC1()"
        ));
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        return c;
    }

    @Bean
    protected CommonTelcomm n960Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n960.txt", "N960");
        c.setDesc("");
        c.setHOSTMSGID("N960");
        c.setErrDetect(dbErrDetect);
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        return c;
    }

    @Bean
    protected CommonTelcomm n932Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n932.txt", "N932");
        c.setDesc("");
        c.setHOSTMSGID("N932");
        c.setErrDetect(dbErrDetect);
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(COUNT)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n933Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n933.txt", "N933");
        c.setDesc("");
        c.setHOSTMSGID("N933");
        c.setErrDetect(dbErrDetect);
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(COUNT)"));
        return c;
    }

    @Bean
    protected CommonTelcomm n215Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n215.txt", "N215");
        c.setDesc("");
        c.setHOSTMSGID("N215");
        c.setErrDetect(dbErrDetect);
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        return c;
    }

    @Bean
    protected CommonTelcomm n201Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n201.txt", "N201");
        c.setDesc("");
        c.setHOSTMSGID("N201");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);
        return c;
    }

    @Bean
    protected CommonTelcomm n202Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n202.txt", "N202");
        c.setDesc("");
        c.setHOSTMSGID("N202");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);
        return c;
    }

    @Bean
    protected CommonTelcomm n365Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n365.txt", "N365");
        c.setDesc("");
        c.setHOSTMSGID("N365");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);
        return c;
    }

    @Bean
    protected CommonTelcomm n366Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n366.txt", "N366");
        c.setDesc("");
        c.setHOSTMSGID("N366");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);
        c.setBeforeCommand(Arrays.asList(
				"SYNC1()",		// 使用 日期
				"XMLCN()",
				"XMLCA()",
				/*
				 * 第一個參數設定, 請參考
				 * http://java.sun.com/developer/technicalArticles/Programming/sprintf/
				 * 若有小數點的設定,最後會被拿掉再做 MAC %16z 表示左靠右補0
				 */
				"MAC2({%10s,CUSIDN} {%11s, ACN} {%11s, INACN})"
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n367Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n367.txt", "N367");
        c.setDesc("黃金存摺結清銷戶作業");
        c.setHOSTMSGID("N367");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);
        c.setBeforeCommand(Arrays.asList(
				"SYNC1()",		// 使用 日期
				"XMLCN()",
				"XMLCA()",
				/*
				 * 第一個參數設定, 請參考
				 * http://java.sun.com/developer/technicalArticles/Programming/sprintf/
				 * 若有小數點的設定,最後會被拿掉再做 MAC %16z 表示左靠右補0
				 */
				"MAC2({%10s,CUSIDN} {%-11s, ACN})"
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm n103Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n103.txt", "N103");
        c.setDesc("");
        c.setHOSTMSGID("N103");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);
        return c;
    }
    
    //TODO:原xml為註解
    @Bean
    protected CommonTelcomm n107Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n107.txt", "N107");
        c.setDesc("扣繳憑單無紙化作業查詢");
        c.setHOSTMSGID("N107");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        return c;
    }

    @Bean
    protected CommonTelcomm n203Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n203.txt", "N203");
        c.setDesc("");
        c.setHOSTMSGID("N203");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);
        return c;
    }

    @Bean
    protected CommonTelcomm n204Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n204.txt", "N204");
        c.setDesc("");
        c.setHOSTMSGID("N204");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        return c;
    }

    @Bean
    protected CommonTelcomm n104Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("n104.txt", "N104");
        c.setDesc("");
        c.setHOSTMSGID("N104");
        c.setErrDetect(dbErrDetect);
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        return c;
    }

    @Bean
    protected CommonTelcomm nb30Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("nb30.txt", "NB30");
        c.setDesc("");
        c.setHOSTMSGID("NB30");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
		c.setBeforeCommand(Arrays.asList(
				"PPSYNCN1()",
				"PINNEW1(HLOGINPIN,HLOGINPIN)",
				"PINNEW1(HTRANSPIN,HTRANSPIN)"
        ));
        c.setErrDetect(dbErrDetect);
        return c;
    }

    @Bean
    protected CommonTelcomm nb31Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("nb31.txt", "NB31");
        c.setDesc("");
        c.setHOSTMSGID("NB31");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);
        return c;
    }

    @Bean
    protected CommonTelcomm nb32Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("nb32.txt", "NB32");
        c.setDesc("");
        c.setHOSTMSGID("NB32");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);
        return c;
    }
    
    @Bean
    protected CommonTelcomm ni02Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("ni02.txt", "Ni02");
        c.setDesc("預借現金密碼函補寄");
        c.setHOSTMSGID("Ni02");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
				"XMLCN()",
				"XMLCA()"
        ));
        return c;
    }
    
    @Bean
    protected CommonTelcomm ni04Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("ni04.txt", "NI04");
        c.setDesc("信用卡自動扣繳申請及取消申請");
        c.setHOSTMSGID("NI04");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
                "SYNC1()", 				//<!-- 使用 日期 -->
				"XMLCN()",
				"XMLCA()",
				"PPSYNCN1()", 			//<!-- 使用 日期 -->
				"PINNEW1()"				//<!-- 使用SH1 --
        ));
        return c;
    }
    
    @Bean
    protected CommonTelcomm n814Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n814.txt", "N814");
        c.setDesc("來電分期線上申請");
        c.setHOSTMSGID("N814");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        return c;
    }
    
    @Bean
    protected CommonTelcomm n816Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n816.txt", "N816");
        c.setDesc("檢核申請或掛失信用卡號交易");
        c.setHOSTMSGID("N816");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        return c;
    }
    
    @Bean
    protected CommonTelcomm n821Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n821.txt", "N821");
        c.setDesc("信用卡帳單補寄");
        c.setHOSTMSGID("N821");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
                "SYNC1()", 				//<!-- 使用 日期 -->
				"XMLCN()",
				"XMLCA()",
				"PPSYNCN1()", 			//<!-- 使用 日期 -->
				"PINNEW1()"				//<!-- 使用SH1 --
        ));
        return c;
    }
    
    @Bean
    protected CommonTelcomm n865Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n865.txt", "N865");
        c.setDesc("約定申請撤銷查詢");
        c.setHOSTMSGID("N865");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        return c;
    }

    @Bean
    protected CommonTelcomm cn19Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("cn19.txt", "CN19");
        c.setDesc("雙因素憑證註冊申請交易");
        c.setHOSTMSGID("CN19");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
        		"PPSYNCN2()",  //<!-- 使用 日期 -->
        		"PINNEW2(HTRANSPIN,HTRANSPIN)"
        ));
        return c;
    }

    @Bean
    protected CommonTelcomm cn29Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("cn29.txt", "CN29");
        c.setDesc("檢核帳號是否已啟用無卡提款");
        c.setHOSTMSGID("CN29");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);
        return c;
    }

    @Bean
    protected CommonTelcomm cn30Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("cn30.txt", "CN30");
        c.setDesc("簽署無卡提款同意書");
        c.setHOSTMSGID("CN30");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);
        return c;
    }

    @Bean
    protected CommonTelcomm cn32Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("cn32.txt", "CN32");
        c.setDesc("申請無卡提款服務交易");
        c.setHOSTMSGID("CN32");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setErrDetect(dbErrDetect);
        c.setBeforeCommand(Arrays.asList(
            	//	"PINBLOCK(CMPASSWORD)"
            ));
            //本功能無使用交易密碼
        return c;
    }

    @Bean
    protected CommonTelcomm a105Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("a105.txt", "A105");
        c.setDesc("");
        c.setHOSTMSGID("A105");
        c.setErrDetect(dbErrDetect);
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        return c;
    }

    @Bean
    protected CommonTelcomm n027Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("n027.txt", "N027");
        c.setDesc("");
        c.setHOSTMSGID("N027");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setAfterCommand(Arrays.asList("OccursNumFromValue(COUNT)"));
        return c;
    }

    @Bean
    protected CommonTelcomm na72Telcomm() throws Exception {
        CommonTelcomm c = createTelcomm("na72.txt", "NA72");
        c.setDesc("重設簽入、交易密碼使用者名稱交易");
        c.setHOSTMSGID("NA72");
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
        		"PPSYNCN1()",
        		"PINNEW1(HLOGINPIN,HLOGINPIN)"
        ));
        return c;
    }
    
    @Bean
    protected CommonTelcomm a106Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
    	CommonTelcomm c = createTelcomm("a106.txt", "A106");
    	c.setDesc("更新客戶資料");
    	c.setHOSTMSGID("NA72");
    	c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
    	c.setErrDetect(dbErrDetect);
    	return c;
    }
    
    @Bean
    protected CommonTelcomm cn28Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
    	CommonTelcomm c = createTelcomm("cn28.txt", "CN28");
    	c.setDesc("申請無卡提款帳號查詢");
    	c.setHOSTMSGID("CN28");
    	c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
    	c.setErrDetect(dbErrDetect);
    	c.setAfterCommand(Arrays.asList("OccursNumFromValue(CNT)"));
    	return c;
    }
    
    @Bean
    protected CommonTelcomm n580Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
    	CommonTelcomm c = createTelcomm("n580.txt", "N580");
    	c.setDesc("同意書申請交易");
    	c.setHOSTMSGID("N580");
    	c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
    	c.setErrDetect(dbErrDetect);
    	return c;
    }
    
    @Bean
    protected CommonTelcomm n581Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
    	CommonTelcomm c = createTelcomm("n581.txt", "N581");
    	c.setDesc("同意書申請交易");
    	c.setHOSTMSGID("N581");
    	c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
    	c.setErrDetect(dbErrDetect);
    	c.setAfterCommand(Arrays.asList("OccursNumFromValue(CNT)"));
    	return c;
    }
    
    @Bean
    protected CommonTelcomm n007Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
    	CommonTelcomm c = createTelcomm("n007.txt", "N007");
    	c.setDesc("數位帳戶拒絕開戶登錄交易");
    	c.setHOSTMSGID("N007");
    	c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
    	c.setErrDetect(dbErrDetect);
    	return c;
    }
    
    @Bean
    protected CommonTelcomm n687Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
    	CommonTelcomm c = createTelcomm("n687.txt", "N687");
    	c.setDesc("查詢虛擬帳號中文戶名");
    	c.setHOSTMSGID("N687");
    	c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
    	c.setErrDetect(dbErrDetect);
    	return c;
    }

    @Bean
    protected CommonTelcomm f001Telcomm(@Qualifier("fxErrDetect") TopMsgFXDetecter fxErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("f001.txt", "F001");
        c.setDesc("計價/央行申報及營業日檢核");
        c.setHOSTMSGID("F001");
		c.setErrDetect(fxErrDetect);
        c.setDefaultFieldFormatDefine(fxFieldFormatDefine);
        return c;
    }

    @Bean
    protected CommonTelcomm f003Telcomm(@Qualifier("f003ErrDetect") TopMsgF003Detecter f003ErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("f003.txt", "F003");
        c.setDesc("扣/入帳要求");
        c.setHOSTMSGID("F003");
		c.setErrDetect(f003ErrDetect);
        c.setDefaultFieldFormatDefine(fxFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
        		"SYNC1()",
        		"PPSYNCN1()",
        		"XMLCN()", 
				"XMLCA()"
        ));
        return c;
    }
    
    @Bean
    protected CommonTelcomm f005Telcomm(@Qualifier("fxErrDetect") TopMsgFXDetecter fxErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("f005.txt", "F005");
        c.setDesc("沖正部位");
        c.setHOSTMSGID("F005");
		c.setErrDetect(fxErrDetect);
        c.setDefaultFieldFormatDefine(fxFieldFormatDefine);
        return c;
    }
    
    @Bean
    protected CommonTelcomm f007Telcomm(@Qualifier("f007ErrDetect") TopMsgF007Detecter f007ErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("f007.txt", "F007");
        c.setDesc("查詢匯率");
        c.setHOSTMSGID("F007");
		c.setErrDetect(f007ErrDetect);
        c.setDefaultFieldFormatDefine(fxFieldFormatDefine);
        return c;
    }
    
    @Bean
    protected CommonTelcomm f011Telcomm(@Qualifier("fxErrDetect") TopMsgFXDetecter fxErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("f011.txt", "F011");
        c.setDesc("交易結束回存主機");
        c.setHOSTMSGID("F011");
		c.setErrDetect(fxErrDetect);
        c.setDefaultFieldFormatDefine(fxFieldFormatDefine);
        return c;
    }
    
    @Bean
    protected CommonTelcomm f015Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("f015.txt", "F015");
        c.setDesc("線上活存幣別結清查詢");
        c.setHOSTMSGID("F015");
        c.setErrDetect(dbErrDetect);
        c.setDefaultFieldFormatDefine(fxFieldFormatDefine);
        return c;
    }
    
    @Bean
    protected CommonTelcomm f017Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("f017.txt", "F017");
        c.setDesc("線上活存幣別結清");
        c.setHOSTMSGID("F017");
        c.setErrDetect(dbErrDetect);
        c.setDefaultFieldFormatDefine(fxFieldFormatDefine);
        c.setBeforeCommand(Arrays.asList(
        		"SYNC1()",
        		"PPSYNCN1()"
        ));
        return c;
    }
    
    @Bean
    protected CommonTelcomm f021Telcomm(@Qualifier("fundErrDetect") TopMsgFundDetecter fxErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("f021.txt", "F021");
        c.setDesc("關帳通知");
        c.setHOSTMSGID("F021");
        c.setErrDetect(fxErrDetect);
        c.setDefaultFieldFormatDefine(fxFieldFormatDefine);
        return c;
    }
    
    @Bean
    protected CommonTelcomm qr10Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("qr10.txt", "QR10");
        c.setDesc("檢查銷帳編號與統一編號是否一致");
        c.setHOSTMSGID("QR10");
        c.setErrDetect(dbErrDetect);
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        return c;
    }
    
    @Bean
    protected CommonTelcomm q072Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("q072.txt", "Q072");
        c.setDesc("QRCODE自行信用卡繳款");
        c.setHOSTMSGID("Q072");
        c.setErrDetect(dbErrDetect);
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        return c;
    }
    
    @Bean
    protected CommonTelcomm mervaTelcomm(@Qualifier("fxErrDetect") TopMsgFXDetecter fxErrDetect) throws Exception {
        CommonTelcomm c = createTelcomm("merva.txt", "MERVA");
        c.setDesc("merva");
        c.setHOSTMSGID("MERVA");
        c.setErrDetect(fxErrDetect);
        c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
        return c;
    }
    
    protected abstract CommonTelcomm createTelcomm(String mockfileName, String txid) throws Exception;
}
