package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.netbank.util.StrUtils;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.services.CommonService;
import fstop.util.SpringBeanFactory;
import lombok.extern.slf4j.Slf4j;

/**
 * 主要用來
 * 1.丟入MVH前的前置處理，包含檢核;
 * 2.後製MVH回覆的訊息;
 * @author JeffChang
 *
 */
@Service
@Slf4j
public class N840_Tel_Service extends Common_Tel_Service  {

	@Override
	public HashMap pre_Processing(Map request) {
		// TODO Auto-generated method stub
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) {
		MVH mvh = null;
		try {
			CommonService service = (CommonService)SpringBeanFactory.getBean(
			        String.format("%sService",
			                Optional.ofNullable(serviceID)
			                    .filter(s -> StrUtils.isNotEmpty(s)).map(s -> s.toLowerCase())
			                    .orElse(null)));
			 mvh = service.doAction(request);
			 
			 
		} catch (TopMessageException e) {
			// TODO Auto-generated catch block
			log.error("process.error>>",e);
			MVHImpl m = new MVHImpl();
			m.getFlatValues().put("TOPMSG", e.getMsgcode());
			mvh = m;
		} catch (Exception e) {
			log.error("process.error>>{}",e);
			throw new RuntimeException(e);
		}finally {
			//TODO:將trancode帶給data_Retouch
			((MVHImpl)mvh).getFlatValues().put("trancode", (String)request.get("trancode"));
//			TODO trur 做data_Retouch false 做msgCode 回覆處理
			return data_Retouch(mvh);
		}
	}

	@Override
	public HashMap data_Retouch(MVH mvh) {
		MVHImpl mvhimpl = null;
		LinkedList<Row> tmplist = null;
		Boolean result = Boolean.FALSE;
		HashMap<String,Object> dataMap = new HashMap<String,Object>();
		HashMap<String,String> rowMap = new HashMap<String,String>();
		LinkedList<HashMap<String,String>> rowlist = null;
    	try {
    		mvhimpl = (MVHImpl) mvh;
    	log.info("getFlatValues>>"+mvhimpl.getFlatValues());
    	log.info("result.getInputQueryData()>>"+mvhimpl.getInputQueryData());
    	log.info("result.getInputQueryData()>>"+mvhimpl.getOccurs().getRows());
			Iterator<Row> tmp = mvhimpl.getOccurs().getRows().iterator();
			tmplist = new LinkedList<Row>();
			rowlist = new LinkedList<HashMap<String,String>>();
			tmp.forEachRemaining(tmplist::add);
			
			log.info("datalist()>>"+tmplist);
			if(mvhimpl.getFlatValues().get("trancode").equals("N8401") || mvhimpl.getFlatValues().get("trancode").equals("N8402")) {
				for(Row  row:tmplist) {
					if(!row.getValue("ACN").substring(3, 5).equals("01")) {
						log.info("getValues>>"+row.getValues());
						rowMap = new HashMap<String,String>();
						rowMap.putAll(row.getValues());
						rowlist.add(rowMap);
					}
				}
			}else {
				for(Row  row:tmplist) {
					log.info("getValues>>"+row.getValues());
					rowMap = new HashMap<String,String>();
					rowMap.putAll(row.getValues());
					rowlist.add(rowMap);
				}
			}
			dataMap.putAll(mvhimpl.getFlatValues());
			dataMap.put("REC", rowlist);
			result = Boolean.TRUE;
			
		} catch (Exception e) {
			log.error("data_Retouch error >> {}",e);
		}finally {
			setMsgCode(dataMap,result);
		}
		return dataMap;
	}
	
	@Override
	public void setMsgCode(Map dataMap, Boolean data_Retouch_result) {
		// TODO Auto-generated method stub
		super.setMsgCode(dataMap, data_Retouch_result);
	}
	
}
