package com.netbank.rest.og.aop;

import java.util.Base64;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.netbank.util.ESAPIUtil;

import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Order(0)
@Aspect
@Component
public class SysLoggerAop {
	
	@Autowired HttpServletRequest req; 
	
//	@Autowired Tracer tracer;
	
	
//	@Pointcut(
//			"within(com.netbank.rest.web.controller..*) &&" +
//			"@annotation(requestMapping) &&" +
//			"@annotation(pathVariable) &&" +
//			"execution(* *(..))"
//			 )
//	public void controller(RequestMapping requestMapping ,PathVariable pathVariable) {}
//	
////	public Object doBefore() {
//	@Before("within(com.netbank.rest.web.controller..*) && controller(requestMapping , pathVariable)")
//		public Object doBefore(JoinPoint joinPoint ,RequestMapping requestMapping,PathVariable pathVariable) {
//		
//		log.debug("SysLoggerAop.doBefore...");
//		String cusidn = "";
//		try {
////			requestMapping.
//			log.trace("requestMapping>>{}",requestMapping.headers());
//			log.trace("pathVariable>>{}", pathVariable.value());
//			cusidn = req.getHeader("trace_cusidn");
//			cusidn = new String (Base64.getDecoder().decode(cusidn) ,"utf-8");
//			log.info("trace_cusidn>>{}",cusidn);
//			log.trace("traceId>>{}",tracer.currentSpan().context().traceId());
//			if(StrUtils.isNotEmpty(cusidn)) {
//				MDC.put("loginid",cusidn );
//			}
//		} catch (Exception e) {
//			log.error(e.toString());
//		}
//		return null;
//	}
	
	
	
	
	@Before("within(com.netbank.rest.web.controller..*)")
	public Object doBefore(JoinPoint joinPoint ) {
		log.debug("SysLoggerAop.doBefore...");
		String cusidn = "";
		String userid = "";
		String apiName = "";
		String adguid = "";
		Object[] args = null;
		
		try {
			log.debug("getArgs 0 >>{}",joinPoint.getArgs()[0]);
			cusidn = req.getHeader("trace_cusidn");
			userid = req.getHeader("trace_userid");
			adguid = req.getHeader("trace_adguid");
//			log.info(ESAPIUtil.vaildLog("trace_cusidn>>{}"+cusidn));
//			log.info(ESAPIUtil.vaildLog("trace_userid>>{}"+userid));
//			log.info(ESAPIUtil.vaildLog("trace_adguid>>{}"+adguid));
			
//			壓力測試用
			if(StrUtils.isNotEmpty(req.getParameter("PUUID"))) {
				adguid = req.getParameter("PUUID");
			}
			
			if(StrUtils.isNotEmpty(cusidn)) {
				cusidn = new String (Base64.getDecoder().decode(cusidn) ,"utf-8");
				log.info(ESAPIUtil.vaildLog("cusidn>>{}"+cusidn));
				MDC.put("loginid",cusidn );
			}
			if(StrUtils.isNotEmpty(userid)) {
				userid = new String (Base64.getDecoder().decode(userid) ,"utf-8");
				log.info(ESAPIUtil.vaildLog("userid>>{}"+userid));
				MDC.put("userid",userid );
			}
//			apiName = (String) joinPoint.getArgs()[0];
			try {
				args = joinPoint.getArgs();
				if(args !=null) {
					for(int i = 0 ; i < args.length ; i++) {
						if( args[i]  instanceof String) {
							apiName = (String) args[i];
						}
					}
				}
			} catch (Exception e) {
				log.error("{}",e);
			}
			
			
			if(apiName.startsWith("<?xml")) {
				apiName="N186";
			}
			
			if(apiName.contains("N221NB")) {
				apiName="N221";
			}
			
			
			log.info("apiName>>{}",apiName);
			
			if(StrUtils.isNotEmpty(apiName)) {
				MDC.put("apiName",apiName );
			}
			if(StrUtils.isNotEmpty(adguid)) {
				MDC.put("adguid",adguid );
			}
//			log.trace("traceId>>{}",tracer.currentSpan().context().traceId());
//			log.trace("traceIdString...>>{}",tracer.currentSpan().context().traceIdString());
			
		} catch (Exception e) {
			log.error(e.toString());
		}
		return null;
	}

	@After("within(com.netbank.rest.web.controller..*)")
	public Object doAfterLogin(JoinPoint joinPoint) {
		MDC.put("loginid","" );
		MDC.put("userid","" );
		MDC.put("apiName","" );
		MDC.put("adguid","" );
		return Boolean.TRUE;
	}
	
}
