package com.netbank.rest.e2e.bean;

import lombok.Data;

@Data
public class EncryptBySymKey_RS_Data  {

	private String cipher;
	
}
