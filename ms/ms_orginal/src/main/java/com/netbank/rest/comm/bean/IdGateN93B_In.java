package com.netbank.rest.comm.bean;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IdGateN93B_In {

	@JsonProperty @NotNull
	String IDGATEID;
	@JsonProperty @NotNull
	String DEVICEID;
	@JsonProperty @NotNull
	String SESSIONID;
	@JsonProperty
	String CHECKSESSIONID;
	@JsonProperty
	String LOGINTYPE;
}
