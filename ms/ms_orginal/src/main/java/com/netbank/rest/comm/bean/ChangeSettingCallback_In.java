package com.netbank.rest.comm.bean;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChangeSettingCallback_In {

	@NotNull
	String idgateID;
	@NotNull
	String sessionID;
	
	RegType type;
	

	@Getter
	@Setter
	public class RegType{
		String bio;
		String diagram;
		String pwd;
	}
}
