package com.netbank.rest.e2e;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.security.KeyFactory;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Hex;
import org.joda.time.DateTime;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.util.UriComponentsBuilder;

import com.netbank.rest.e2e.bean.CheckPasswordRule_RQ;
import com.netbank.rest.e2e.bean.CheckPasswordRule_RS;
import com.netbank.rest.e2e.bean.EncryptBySymKey_RQ;
import com.netbank.rest.e2e.bean.EncryptBySymKey_RS;
import com.netbank.rest.e2e.bean.Encryption;
import com.netbank.rest.e2e.bean.EncryptionData;
import com.netbank.rest.e2e.bean.GetPublicKey_RQ;
import com.netbank.rest.e2e.bean.GetPublicKey_RS;
import com.netbank.rest.e2e.bean.GetToken_RQ;
import com.netbank.rest.e2e.bean.GetToken_RQ_Data;
import com.netbank.rest.e2e.bean.GetToken_RS;
import com.netbank.rest.e2e.bean.Hash;
import com.netbank.rest.e2e.bean.PRule;
import com.netbank.rest.e2e.bean.ReturnPProcess;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;
import com.netbank.util.fstop.DateUtil;

import e2eEncKeySpc.E2EescCM;
import fstop.exception.TopMessageException;
import fstop.orm.dao.AdmMsgCodeDao;
import fstop.orm.po.ADMMSGCODE;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class E2EClient {

	public static GetToken_RS token_rs;
	
	public static final Integer HASH_MD5 = 1;
	public static final Integer HASH_SHA1 = 2;
	public static final Integer HASH_SHA256 = 3;
	public static final Integer HASH_SHA384 = 4;
	public static final Integer HASH_SHA512 = 5;
	
	public static final Integer KEY_MODE_ECB = 16;
	public static final Integer KEY_MODE_CBC = 32;
	public static final Integer KEY_MODE_CBC_PAD = 64;
	
	public static final Integer SESSIONKEY_TYPE_DES = 1;
	public static final Integer SESSIONKEY_TYPE_3DES = 2;
	public static final Integer SESSIONKEY_TYPE_AES128 = 3;
	public static final Integer SESSIONKEY_TYPE_AES256 = 4;
	
	public static final Integer KEY_TYPE_DES = 1;
	public static final Integer KEY_TYPE_2DES = 2;
	public static final Integer KEY_TYPE_3DES = 3;
	public static final Integer KEY_TYPE_AES128 = 4;
	public static final Integer KEY_TYPE_AES192 = 5;
	public static final Integer KEY_TYPE_AES256 = 6;
	public static final Integer KEY_TYPE_RC4 = 7;
	
	public static final Integer CIPHER_TYPE_PKCS1 = 1;
	public static final Integer CIPHER_TYPE_PKCS7 = 2;
	public static final String UTF_8 = "UTF-8";
	
	public static E2ETransform e2e() {
		return (E2ETransform) SpringBeanFactory.getBean("e2etransform");
		
	}
	
	
	/**
	 * 登入E2E 取得token
	 * @return
	 */
	public static Boolean login() {

		log.info("SysRestTimeDiffAop.restTimediff...");
		DateTime d1 = null;
		DateTime d2 = null;
		try {
			log.info("Before doAround.proceed..");
			d1 = new DateTime();
		} catch (Throwable e) {
			log.error("SysRestTimeDiffAop.timediff{}", e);
		}
		
//		String uri = "http://{ServletIP}:{PORT}/{APName}/rest/token/login";
		Boolean result = Boolean.FALSE;
		String json = "";
		json = getToken();
		if(StrUtils.isNotEmpty(json)) {
			token_rs = CodeUtil.fromJson(json, GetToken_RS.class);
			if(token_rs !=null) {
				log.info("AccessToken>>{}",token_rs.getData().getAccessToken());
				result = Boolean.TRUE;
			}
		}
		

		try {
			d2 = new DateTime();
			log.info("After doAround.proceed..");
			String pstimediff = DateUtil.getDiffTimeMills(d1, d2);
			log.warn("API>> E2EClient.login, pstimediff>>{}",pstimediff);
		} catch (Throwable e) {
			log.error("SysRestTimeDiffAop.timediff{}", e);
		}
		log.info("SysRestTimeDiffAop.timediff end...");
		
		return result;
	}
	
	/**
	 * 取得Token
	 * @return
	 */
	public static String getToken() {
//		String url = "http://127.0.0.1:9480/SS6/rest/token/login";
		String url = "";
		String apiName="rest/token/login";
		byte[] signature=null;
		String applicationName="";
		String signTime="";
		String result="";
		Map<String,String> params = null;
		GetToken_RQ rq = new GetToken_RQ();
		GetToken_RQ_Data data = new GetToken_RQ_Data();
		
		try {
//			url = e2e().e2e.getApiuri();
//			if(!url.endsWith("/")) {
//				url = url+"/";
//			}
//			url = url+apiName;
			url = getUrl(apiName);
			applicationName = e2e().e2e.getApplicationName();
			signTime= new DateTime().toString("yyyy/MM/dd HH:mm:ss");
			data.setApplicationName(applicationName);
			data.setSignTime(signTime);
			signature = sign(CodeUtil.toJson(data) , getPrivateKey());
			rq.setSignature(Base64.getEncoder().encodeToString(signature));
			rq.setData(data);
			params = CodeUtil.objectCovert(Map.class, rq);
			result = send(params, url, Integer.valueOf(e2e().e2e.toE2ETimeout) ,HttpMethod.POST );
			
		} catch (Exception e) {
			log.error("{}",e);
			throw TopMessageException.create("Z300");
		}
		return result;
	}
	
	/**
	 * 私鑰簽章
	 * @param data :簽章資料
	 * @param priKey :由E2EAPI 平台產生
	 * @return
	 * @throws Exception
	 */
	private static byte[] sign(String data, PrivateKey priKey) throws Exception {
		Signature rsa = Signature.getInstance("SHA256withRSA");
		rsa.initSign(priKey);
		rsa.update(data.getBytes("UTF-8"));
		return rsa.sign();
	}
	
	
	/**
	 * 去得由E2EAPI平台產生的prikey
	 * @return
	 * @throws Exception
	 */
	private static PrivateKey getPrivateKey() throws Exception {
		//String key = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCrbKnySBQo4IbKQKUBqk/WFPaSB0xFftjmdgE/J7TzyH7vyz4f8ZWanxgDM2D8wuoPfSqnxo4Jz/kTVCZy8b2i+qz2mfYXegSR/ksp76ZYE18TB7vCaNSPQjQ6Nij+2A2cXVmks2m+N9BRUymNafilhfZaKZDo0Qf1ifQM+a+0mv9oLYwY9pz3HKKQXnhkLoijZBkbduStU0bBOv5o1vsCA5f8tNMVjoKjTt/Ep0vufK1W+8w65c96IGTdS1wPdYz6vzuulCwPfhMlU7kp5Wqh85ZDzxHQs8o2m2YO1YpJqV2G28NOGQLrzVn6SXXtdnqF+xCVCDJWZ4mwpB3rgfbnAgMBAAECggEADU2riq/pUJrMGxoVNCHhfD24PPDwDFyhvJDf2+Ta+ChRIY7mTVhUQeGi3Wy5VCRUQct26fpQv5YyQDvBmB4Gl4VYsvKT1JKVlDbl4mVExYULVdC904KLLGr43qOhXtWpiL792/hIdoiiVA1eJilZMI9K5QhgxJCkS3vfQrqF1kAoHyppgvfyjk3l7V+bCEBIOo4gsxhzFR/OJAB0V7eIx5Ec1ukM9XDUgc2qb1AvWiEsuyrO40BHj+4PAaEeyuEELqCwRzRwVL9Fu08+UMpBr+JWF+IAhdJk0+VEP7CIdW08D/dCAg39dXXp3ABG6pOP83qOCvzzj8IQOrNkm6PZsQKBgQD4vbLd6uIZGAu9vx5nuOqC6MvJ4fRRw8l73HM/OYWkhUL05J0af/YpqBX1ohTBzUvv/VTVMvLfIB7a0XbUkKVQwSGxPzqH2JxlWPNl7BjwH6ht4HFlnx6U9mBZZoAPPdqiMxBj0ZedHSXzPYcXWTI+9ABCnk7XffZ9CJKYY/0jpQKBgQCwbVi4lHy7ICz3+ybgqEe5E/h8QzeRHyZee8DIidMs2xWUsoYPb8Ee9uKHbj5yqHZb4skwgoEkfMF3ifYhnm7V3PTgG/Xm/6JvWsXrJgMeFpO9qyNDtWmV+pA1Lcfu6ueFnE9ZyGjb60Z95Tib1+RE4Ufw0W04yKfQUU+rOmI6mwKBgQCcLZeqKPmPof51RBPWMk/kpECYYKwjzpeqko82Cmr0EswosYZHBkNMgoJPOVZpHPrqE5j5SEUgfa2B3PUMoEl8O9doz/dzJjmElOY2f5hk5Ku/WaY5lUABbfSvDWGosECfiQpockgM2Slj4aT5rwKSbmffbEC6oxdqCEDrOBTQFQKBgQCrUgONeeOFcsE4LVKbZc4AEzyArmUPL0h7B4E1O2EtdbGHK7Sy2LqX7vYavRs3DA+jOp5T1ObDH8Je6kFwdAD8/AtudLhdEHAT4fm/VrJATXw5+i3MWiWl1iKjAUnc347jV95WOR5aMCXLqwNFkme/22OYnkWJlpAe4laS16b7cQKBgCpi1xqKwH1xl5tj+MMYfHNJ5D6uz5Bu7B+ODsd4pz449cF41ytvsfqnPD8BBtPo6eKx8zC4AfW4lW7wwhVJ2ecxnJ8KTK1Er05QaeohntfDYikWp3G6wDZpWnKnIKAi4oy4rvpfkmc7GnmdJqXxRu9uldY8ivb09FKea5WQmuZ/";
		
		FileReader fis = null;
		BufferedReader bf  = null;
		String key = "";
		String apiKeyFile = "";
		try {
			apiKeyFile = e2e().e2e.getApiKeyFile();
			fis = new FileReader(apiKeyFile);
			log.info("APIKeyFile:{}",apiKeyFile);
			bf = new BufferedReader(fis);
			String strTemp = null;
			while((strTemp = bf.readLine()) != null) {
				if(!strTemp.startsWith("-----")) {
					key += strTemp;
				}
			}
		}finally {
			if(fis != null) {
				fis.close();
			}
		}
		log.info("getPrivateKey :[{}]",key);
//		PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(new BASE64Decoder().decodeBuffer(key));
//		PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(key));
		PKCS8EncodedKeySpec spec = E2EescCM.getSpec(key);
		KeyFactory kf = KeyFactory.getInstance("RSA");
		return kf.generatePrivate(spec);
	}
	
	/**
	 * 呼叫API取得公鑰
	 * @return
	 */
	public static String getPublicKey() {
		String publicStr = "";
		String url = "";
		String apiName="rest/AsymmetricKey/publickey";
		String result="";
		Map<String,String> params = null;
		GetPublicKey_RQ rq = new GetPublicKey_RQ();
		GetPublicKey_RS rs = null;
		try {
//			url = e2e().e2e.getApiuri();
//			if(!url.endsWith("/")) {
//				url = url+"/";
//			}
//			url = url+apiName;
			url = getUrl(apiName);
			rq.setCluster(e2e().e2e.getCluster());
			rq.setKeyName(e2e().e2e.getE2ekeyName());
			params = CodeUtil.objectCovert(Map.class, rq);
			result = send(params, url, Integer.valueOf(e2e().e2e.toE2ETimeout) ,HttpMethod.GET );
			
			if(StrUtils.isNotEmpty(result)) {
				rs = CodeUtil.fromJson(result, GetPublicKey_RS.class);
			}else {
				throw TopMessageException.create("Z300");
			}
			
			if(rs != null && "0".equals(rs.getErrorCode()) ) {
				publicStr = rs.getData().getPubKey();
				log.debug("publicStr>>{}",publicStr);
			}else {
				log.error("{}","getPublicKey rs error");
			}
			
		} catch (Exception e) {
			log.error("{}",e);
			throw TopMessageException.create("Z300");
		}
		return publicStr;
		
		
	}
	
	
	
	
	
	public static String getPPSYNC() {
		log.info("SysRestTimeDiffAop.restTimediff...");
		DateTime d1 = null;
		DateTime d2 = null;
		try {
			log.info("Before doAround.proceed..");
			d1 = new DateTime();
		} catch (Throwable e) {
			log.error("SysRestTimeDiffAop.timediff{}", e);
		}
		
		String cipher = "";
		byte[] c = null;
		try {
			if (login()) {
				cipher = encryptBySymKey(E2EClient.KEY_TYPE_2DES, E2EClient.e2e().e2e.getE2ekeyLabel(), E2EClient.KEY_MODE_CBC,
						getSYNC_IKEY().getBytes("CP937"), null, getICV());
				c = Base64.getDecoder().decode(cipher);
				cipher = Hex.encodeHexString(c).toUpperCase();
			}
			log.debug("cipher>>{}", cipher);
		} catch (Exception e) {
			log.error("{}", e);
			throw TopMessageException.create("Z300");
		}

		try {
			d2 = new DateTime();
			log.info("After doAround.proceed..");
			String pstimediff = DateUtil.getDiffTimeMills(d1, d2);
			log.warn("API>> E2EClient.getPPSYNC, pstimediff>>{}",pstimediff);
		} catch (Throwable e) {
			log.error("SysRestTimeDiffAop.timediff{}", e);
		}
		log.info("SysRestTimeDiffAop.timediff end...");
		
		return cipher;
	}
	
	
	
	
	public static String getPPSYNCN911() {
		log.info("SysRestTimeDiffAop.restTimediff...");
		DateTime d1 = null;
		DateTime d2 = null;
		try {
			log.info("Before doAround.proceed..");
			d1 = new DateTime();
		} catch (Throwable e) {
			log.error("SysRestTimeDiffAop.timediff{}", e);
		}
		
		String cipher = "";
		byte[] c =null;
		try {
			cipher = encryptBySymKey(E2EClient.KEY_TYPE_2DES, E2EClient.e2e().e2e.getE2ekeyLabel(), E2EClient.KEY_MODE_CBC, getSYNC_IKEY().getBytes("CP937"), null, getICV());
			c = Base64.getDecoder().decode(cipher);
			cipher = Hex.encodeHexString(c).toUpperCase();
			log.debug("cipher>>{}",cipher);
		} catch (Exception e) {
			log.error("{}",e);
			throw TopMessageException.create("Z300");
		}

		try {
			d2 = new DateTime();
			log.info("After doAround.proceed..");
			String pstimediff = DateUtil.getDiffTimeMills(d1, d2);
			log.warn("API>> E2EClient.getPPSYNCN911, pstimediff>>{}",pstimediff);
		} catch (Throwable e) {
			log.error("SysRestTimeDiffAop.timediff{}", e);
		}
		log.info("SysRestTimeDiffAop.timediff end...");
		
		
		return cipher;
	}
	
	
	public static String encryptBySymKey(Integer keyType, String keyName, Integer keyMode, byte[] b64Data, String data, byte[] iv) {
		String cipher = "";//bs64
		String url = "";
		String result = "";
		String apiName = "rest/SymmetricKey/encrypt";
		
		EncryptBySymKey_RQ rq = new EncryptBySymKey_RQ();
		EncryptBySymKey_RS rs = null;
		Map<String,String> params = null;
		
		try {
			url = getUrl(apiName);
			if(b64Data !=null) {
				b64Data = padding(b64Data);
				rq.setB64Data(Base64.getEncoder().encodeToString(b64Data));
			}else {
				rq.setData(data);
			}
			if(iv !=null) {
				rq.setIv(Base64.getEncoder().encodeToString(iv));
			}
			rq.setKeyName(keyName);
			rq.setKeyType(keyType.toString());
			rq.setCluster(e2e().e2e.getCluster());
			rq.setMode(keyMode.toString());
			rq.setEncoding(UTF_8);
			
			params = org.apache.commons.beanutils.BeanUtils.describe(rq);
			log.trace("params0>>{}",params);
			
			params = CodeUtil.objectCovert(Map.class, rq);
			log.trace("params1>>{}",params);
			result = send(params, url, Integer.valueOf(e2e().e2e.toE2ETimeout), HttpMethod.POST);
			
			if(StrUtils.isNotEmpty(result)) {
				rs = CodeUtil.fromJson(result, EncryptBySymKey_RS.class);
			}else {
				throw TopMessageException.create("Z300");
			}
			
			if(rs != null && "0".equals(rs.getErrorCode()) ) {
				cipher = rs.getData().getCipher();
				log.debug("cipher>>{}",cipher);
			}else {
				log.error("{}","encryptBySymKey rs error");
			}
		} catch (Exception e) {
			log.error("{}",e);
			throw TopMessageException.create("Z300");
		}
		
		
		
		
		
		
		return cipher;
	} 
	
	/**
	 * PINNEW 加解密
	 * @param rsaData 經過公鑰加密後的PINNEW
	 * @return
	 */
	public static String getPINNEW(String rsaData) {
		log.info("SysRestTimeDiffAop.restTimediff...");
		DateTime d1 = null;
		DateTime d2 = null;
		try {
			log.info("Before doAround.proceed..");
			d1 = new DateTime();
		} catch (Throwable e) {
			log.error("SysRestTimeDiffAop.timediff{}", e);
		}
		
		String pinnew = "";
		try {
			if(login()) {
				pinnew = checkPasswordRule(rsaData, e2e().e2e.getE2ekeyName(), e2e().e2e.getE2ekeyLabel(), getICV(), Boolean.FALSE);
				if (StrUtils.isNotEmpty(pinnew) && pinnew.length() >= 48) {
					pinnew = pinnew.substring(0, 48).toUpperCase();
				}
				log.debug("pinnew>>{}",pinnew);
			}else {
				log.warn("E2E login Fail");
			}
		} catch (Exception e) {
			log.error("{}", e);
			throw TopMessageException.create("Z300");
		}

		try {
			d2 = new DateTime();
			log.info("After doAround.proceed..");
			String pstimediff = DateUtil.getDiffTimeMills(d1, d2);
			log.warn("API>> E2EClient.getPINNEW, pstimediff>>{}",pstimediff);
		} catch (Throwable e) {
			log.error("SysRestTimeDiffAop.timediff{}", e);
		}
		log.info("SysRestTimeDiffAop.timediff end...");
		
		return pinnew;

	}
	
	
	public static String getPINNEWN911(String rsaData) {

		log.info("SysRestTimeDiffAop.restTimediff...");
		DateTime d1 = null;
		DateTime d2 = null;
		try {
			log.info("Before doAround.proceed..");
			d1 = new DateTime();
		} catch (Throwable e) {
			log.error("SysRestTimeDiffAop.timediff{}", e);
		}
		
		String pinnew = "";
		try {
				pinnew = checkPasswordRule(rsaData, e2e().e2e.getE2ekeyName(), e2e().e2e.getE2ekeyLabel(), getICV(), Boolean.FALSE);
				if (StrUtils.isNotEmpty(pinnew) && pinnew.length() >= 48) {
					pinnew = pinnew.substring(0, 48).toUpperCase();
					log.debug("pinnew>>{}",pinnew);
				}else {
					throw new Exception("checkPasswordRule fail...");
				}
		} catch (TopMessageException e) {
			log.error("{}", e);
			throw TopMessageException.create(e.getMsgcode());
		} catch (Exception e) {
			log.error("{}", e);
			throw TopMessageException.create("Z300");
		}

		try {
			d2 = new DateTime();
			log.info("After doAround.proceed..");
			String pstimediff = DateUtil.getDiffTimeMills(d1, d2);
			log.warn("API>> E2EClient.getPINNEWN911, pstimediff>>{}",pstimediff);
		} catch (Throwable e) {
			log.error("SysRestTimeDiffAop.timediff{}", e);
		}
		log.info("SysRestTimeDiffAop.timediff end...");
		
		return pinnew;
		
	}
	
	
	/**
	 * 檢查密碼邏輯
	 * @param rsaData 經過公鑰加密後的PINNEW
	 * @param rsakey 
	 * @param keyLabel
	 * @param iv
	 * @param isMemberUser
	 * @return
	 */
	public static String checkPasswordRule(String rsaData, String rsakey, String keyLabel, byte[] iv, boolean isMemberUser) {
		
		String retHex= "";
		String ivbs64= "";
		String url = "";
		String result = "";
		String apiName="rest/e2e/checkPasswordRule";
		CheckPasswordRule_RQ rq = new CheckPasswordRule_RQ();
		EncryptionData encryptionData = new EncryptionData();
		Encryption encryption = new Encryption();
		ReturnPProcess returnPassProcess = new ReturnPProcess();
		List<Hash> hashList = new LinkedList<Hash>();
		Map<String,String> params = null;
		CheckPasswordRule_RS rs = null;
		try {
//			url = e2e().e2e.getApiuri();
//			if(!url.endsWith("/")) {
//				url = url+"/";
//			}
//			url = url+apiName;
			url = getUrl(apiName);
			
			List<List<PRule>> ppRuleLList = new LinkedList<List<PRule>>();
			ivbs64 = Base64.getEncoder().encodeToString(iv);
			
			encryptionData.setData(rsaData);
			encryptionData.setDataType(CIPHER_TYPE_PKCS7.toString());
			encryptionData.setRsaKey(rsakey);//rsa key label
			
			encryption.setSymKey(keyLabel);
			encryption.setPostfix("");
			encryption.setPrefix("");
			encryption.setSymKeyIv(ivbs64);
			encryption.setSymKeyMode(KEY_MODE_CBC_PAD.toString());
			
			returnPassProcess.setEncryption(encryption);
			returnPassProcess.setHash(hashList);
			
			if(!isMemberUser) {
				PRule pRule = new PRule();
				pRule.setSpecialSymbol(Arrays.asList("all"));
				
				List<PRule> pRuleList = new LinkedList<PRule>();
				pRuleList.add(pRule);
				
				ppRuleLList.add(pRuleList);
				rq.setRtnEncoding("2");//1:BASE64 2:HEX	
				encryptionData.setWithTime("1");
			}else {
				rq.setRtnEncoding("1");//1:BASE64 2:HEX
			}
			
			rq.setCluster(e2e().e2e.getCluster());
			rq.setEncryptionData(encryptionData);
			rq.setPassRule(ppRuleLList);
			rq.setIgnoreCheckTime(Boolean.TRUE);
			rq.setReturnPassProcess(returnPassProcess);
			params = CodeUtil.objectCovert(Map.class, rq);
			result = send(params, url, Integer.valueOf(e2e().e2e.toE2ETimeout), HttpMethod.POST);
			
			if(StrUtils.isNotEmpty(result)) {
				rs = CodeUtil.fromJson(result, CheckPasswordRule_RS.class);
			}else {
				throw TopMessageException.create("FE0013");
			}
			
			if(rs != null && "0".equals(rs.getErrorCode()) ) {
				retHex = rs.getData().getEncPass();
				log.debug("retHex>>{}",retHex);
			}else {
				log.error("{}","checkPasswordRule rs error");
				AdmMsgCodeDao admMsgCodeDao = (AdmMsgCodeDao)SpringBeanFactory.getBean("admMsgCodeDao");
				ADMMSGCODE msgresult = admMsgCodeDao.isError("EE"+rs.getErrorCode());
				if (msgresult!=null) {
					throw TopMessageException.create("EE"+rs.getErrorCode());
				}else {
					throw TopMessageException.create("FE0013");
				}
			}
		} catch (TopMessageException e) {
			log.error("{}",e);
			throw TopMessageException.create(e.getMsgcode());
		} catch (Exception e) {
			log.error("{}",e);
			throw TopMessageException.create("FE0013");
		}
		return retHex;
	}
	
	
	
	
	
	
	public static String send(Map<String,String> params ,String url, Integer toE2ETimeout ,HttpMethod method) {
		String result = null;
		RestTemplate restTemplate = null;
		ClientHttpRequestFactory rf = null;
		MultiValueMap<String, String> multiValueMap=new LinkedMultiValueMap<>();
		try {
			log.info(ESAPIUtil.vaildLog("url>>{}"+url));
			if(!params.isEmpty()) {
				log.debug(ESAPIUtil.vaildLog("RESTfulClient.params.toJson>>{}"+CodeUtil.toJson(params)));
			}
			restTemplate = setHttpConfig(toE2ETimeout);
			
			
			HttpHeaders headers = new HttpHeaders();
//			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
			if(token_rs !=null && StrUtils.isNotEmpty(token_rs.getData().getAccessToken()) ) {
				headers.set("Authorization", "Bearer "+ token_rs.getData().getAccessToken());
			}else {
				log.warn("AccessToken is null , Authorization not setting...");
			}
			
			
			HttpEntity<Map<String, String>> entity = new HttpEntity<>(params, headers);
//			ResponseEntity<String> re  = restTemplate.postForEntity(url, entity, String.class );
			ResponseEntity<String> re  = null;
			if(method.matches(HttpMethod.GET.name())) {
				multiValueMap.setAll(params);
				UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(url).queryParams(multiValueMap);
				url = uriBuilder.toUriString();
			}
			re  = restTemplate.exchange(url, method, entity, String.class);
//			if(method.matches(HttpMethod.POST.name())) {
//				log.info("POST...");
//				re  = restTemplate.postForEntity(url, entity, String.class );
//			}
			
			result = re.getBody();
			
			log.trace(ESAPIUtil.vaildLog("result>>{}" +result));
			
			if(result == null) {
				throw new Exception("result == null ,電文接收異常");
			}
			
		} catch (ResourceAccessException e) {
			log.error("",e);
			throw e;
			
		} catch (Exception e) {
			log.error("",e);
		}
		
		
		return result;
		
	}
	
	
	
	/**
	 * 設定Http 參數
	 * https://codertw.com/%E7%A8%8B%E5%BC%8F%E8%AA%9E%E8%A8%80/12005/
	 * @param toTmraTimeout
	 * @return
	 * @throws KeyManagementException
	 * @throws NoSuchAlgorithmException
	 * @throws KeyStoreException
	 */
	public static RestTemplate setHttpConfig(Integer toE2ETimeout) throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
		Integer connectTimeout = 10*1000;
		Integer socketTimeout = toE2ETimeout*1000;
		HttpComponentsClientHttpRequestFactory rf =
				new HttpComponentsClientHttpRequestFactory();
		rf.setConnectTimeout(connectTimeout);
		rf.setReadTimeout(socketTimeout);
		log.trace("setHttpConfigI end..");
		return new RestTemplate(rf);
		
	}
	
	
	/**
	 * 目前SYNC PPSYNC key 邏輯相同
	 * @return
	 */
	public static String getSYNC_IKEY() {
		String ekey = "";
		try {
			String tmp =   new DateTime().toString("MMdd");
			log.trace("tmp>>{}",tmp);
			ekey =String.format("%08d", Integer.parseInt(tmp)+1) ;
			log.trace("ekey>>{}",ekey);
		} catch (NumberFormatException e) {
			log.error(e.toString());
		}
		return ekey;
	}
	
	public static byte[] getICV() throws UnsupportedEncodingException  {
		log.warn("getICV ....");
        String checkItem = "";
        GregorianCalendar cNow = new GregorianCalendar();
        int iYear = cNow.get(Calendar.YEAR);
       
        iYear = iYear - 1911;
        String sYear = String.valueOf(iYear);
        if(sYear.length() > 2)
            sYear = sYear.substring(1, 3);
       
        checkItem += sYear;
        int iMonth = cNow.get(Calendar.MONTH) + 1;
        int iDay = cNow.get(Calendar.DAY_OF_MONTH);
                
        if(iMonth < 10)
            checkItem += "0" + iMonth;
        else
            checkItem += "" + iMonth;
        if(iDay < 10)
            checkItem += "0" + iDay;
        else
            checkItem += "" + iDay;
       
        checkItem = checkItem + "00";
        log.warn("icv checkItem :" + checkItem);
        
        return checkItem.getBytes("IBM937");
	}
	
	public static byte[] padding(byte[] b64Data) {
		
		
		byte[] paddingData = (byte[]) null;
		if(b64Data != null) {
			byte[] encryptData = new byte[b64Data.length];
			System.arraycopy(b64Data, 0, encryptData, 0, b64Data.length);
			int remainder = encryptData.length % 8;
			if (remainder != 0) {
				paddingData = new byte[encryptData.length + (8 - remainder)];

				for (int i = 0; i < paddingData.length; ++i) {
					if (i < encryptData.length) {
						paddingData[i] = encryptData[i];
					} else {
						paddingData[i] = 0;
					}
				}
			} else {
				paddingData = encryptData;
			}
//			log.info("angus debug mode encryptData bPinData 5:"+new String(DigestUtils.toHexString(PaddingData)));
			log.info("angus debug mode encryptData bPinData 5:{}", Hex.encodeHex(paddingData));
		}
		return paddingData;
	}
	
	public static String getUrl(String apiName) {
		String url = e2e().e2e.getE2euri();
		if(!url.endsWith("/")) {
			url = url+"/";
		}
		return url = url+apiName;
	}
}
