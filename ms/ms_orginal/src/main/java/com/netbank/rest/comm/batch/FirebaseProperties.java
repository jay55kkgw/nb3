package com.netbank.rest.comm.batch;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@Data
public class FirebaseProperties {

//	@Value("${firebase_url}")
//	private String url;

	@Value("${firebase_connecttimeout}")
	private Integer connectTimeout;
	
	@Value("${firebase_useflag}")
	private String firebase_useflag;
	

	@Value("${firebase_jsonPath}")
	private String jsonPath;
	
	@Value("${firebase_type}")
	private String useType;
	
	@Value("${firebase_key}")
	private String firebaseKey;
}
