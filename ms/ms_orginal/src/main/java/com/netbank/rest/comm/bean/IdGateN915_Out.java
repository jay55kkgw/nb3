package com.netbank.rest.comm.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IdGateN915_Out {

	String MSGCOD;
	String OPNCODE;
	
}
