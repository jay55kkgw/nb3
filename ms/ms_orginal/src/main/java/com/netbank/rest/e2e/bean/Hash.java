package com.netbank.rest.e2e.bean;

import lombok.Data;

@Data
public class Hash {

	private String hashAlg;
	private String prefix;
	private String postfix;
}
