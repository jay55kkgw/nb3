package com.netbank.rest.comm.batch;

import java.sql.SQLInvalidAuthorizationSpecException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;

import fstop.exception.TopMessageException;
import fstop.model.TelcommResult;
import fstop.orm.dao.AdmNbStatusDao;
import fstop.telcomm.TelCommExec;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CheckSystemStatus {
	@Autowired
	@Qualifier("nd08Telcomm")
	private TelCommExec nd08Telcomm;
	
	@Autowired
	private AdmNbStatusDao admnbstatusdao;
	
	public  TelcommResult ConnectionCheck(Map params) {	
		//initialize
		TelcommResult helper= null;
		String dbconnect_status="false";
		String telconnect_status=null;
		String DB_LOG="";
		String TEL_LOG="";
		String TOPMSG="";
		//checking connection
		//cannot get tel exception since RESTUtil.java didn't throw out exception
		try {
			helper = nd08Telcomm.query(params);
			if(helper.getFlatValues().get("TOPMSG")!=null) {
				TOPMSG=helper.getFlatValues().get("TOPMSG");
				if(TOPMSG.equals("0000"))helper.getFlatValues().put("TOPMSG","0");
				telconnect_status="true";
			}else {
				helper.getFlatValues().put("TOPMSG","0");
				telconnect_status="false";		
			}
		}catch(ResourceAccessException  e) {
			log.debug("TEL1 : ",e);
			telconnect_status="false";
			TEL_LOG=e.getMessage();
		}catch(TopMessageException  e) {
			log.debug("TEL2 : ",e);
			telconnect_status="false";
			TEL_LOG=e.getMessage();
		}catch(Exception e) {
			log.debug("TEL3 : ",e);
			telconnect_status="false";
			TEL_LOG=e.getMessage();		
		}
		try {
			long tmp = admnbstatusdao.count();
			dbconnect_status="true";
		}catch(DataAccessException e) {
			log.debug("DB1 : ",e);
			dbconnect_status="false";
			Map<String,Object>errorame=new HashMap<String, Object>();
			errorame.put("data",e.getRootCause());
			DB_LOG=((SQLInvalidAuthorizationSpecException)errorame.get("data")).getLocalizedMessage();
		}catch(Exception e) {
			log.debug("DB2 : ",e);
			dbconnect_status="false";
			DB_LOG=e.getMessage();
			
		}
		//附值
		helper.getFlatValues().put("DB_LOG", DB_LOG);
		helper.getFlatValues().put("TEL_LOG", TEL_LOG);
		helper.getFlatValues().put("DBconnect", dbconnect_status);
		helper.getFlatValues().put("TELconnect", telconnect_status);
		return helper;
	}
}
