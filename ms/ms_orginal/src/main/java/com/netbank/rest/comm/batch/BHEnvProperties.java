package com.netbank.rest.comm.batch;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import fstop.services.CommonService;
import lombok.Data;

@Component
@Data
public class BHEnvProperties {
	
	@Value("${txnMail_receiver}")
	private String txnMail_receiver;
	
	@Value("${txnMail_env}")
	private String txnMail_env;

}
