package com.netbank.rest.comm.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IdGateN93A_Out {

	String RSPCOD;
	String CUSIDN;
	IdGateN939_Out N939;
}
