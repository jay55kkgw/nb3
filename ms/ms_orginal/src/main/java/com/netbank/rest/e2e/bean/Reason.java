package com.netbank.rest.e2e.bean;

import lombok.Data;

@Data
public class Reason {

	private String ruleName;
	private String param;
}
