package com.netbank.rest.e2e.bean;

import lombok.Data;

@Data
public class GetPublicKey_RS extends Base_RS {
	private GetPublicKey_RS_Data data;
}
