package com.netbank.rest.e2e.bean;

import lombok.Data;

@Data
public class CheckPasswordRule_RS extends Base_RS {
	private Reason reason;
	private CheckPasswordRule_RS_Data data;
}
