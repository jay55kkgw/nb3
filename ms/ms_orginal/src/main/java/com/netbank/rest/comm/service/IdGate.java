package com.netbank.rest.comm.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.netbank.rest.comm.bean.ChangeSettingCallback_In;
import com.netbank.rest.comm.bean.ChangeSettingCallback_Out;
import com.netbank.rest.comm.bean.CloseQuick_In;
import com.netbank.rest.comm.bean.CloseQuick_Out;
import com.netbank.rest.comm.bean.GetSessionId_In;
import com.netbank.rest.comm.bean.GetSessionId_Out;
import com.netbank.rest.comm.bean.Get_Device_Status_In;
import com.netbank.rest.comm.bean.Get_Device_Status_Out;
import com.netbank.rest.comm.bean.IdGateMappingDelete_In;
import com.netbank.rest.comm.bean.IdGateMappingQuery_In;
import com.netbank.rest.comm.bean.IdGateMappingQuery_Out;
import com.netbank.rest.comm.bean.IdGateMappingUpdate_In;
import com.netbank.rest.comm.bean.IdGateN915_In;
import com.netbank.rest.comm.bean.IdGateN915_Out;
import com.netbank.rest.comm.bean.IdGateN939_In;
import com.netbank.rest.comm.bean.IdGateN939_Out;
import com.netbank.rest.comm.bean.IdGateN93A_In;
import com.netbank.rest.comm.bean.IdGateN93A_Out;
import com.netbank.rest.comm.bean.IdGateN93B_In;
import com.netbank.rest.comm.bean.IdGateN955_In;
import com.netbank.rest.comm.bean.IdGateN955_Out;
import com.netbank.rest.comm.bean.IdGateQuerySession_In;
import com.netbank.rest.comm.bean.IdGateQuerySession_Out;
import com.netbank.rest.comm.bean.IdGateQuickSessionId_In;
import com.netbank.rest.comm.bean.IdGateQuickSessionId_Out;
import com.netbank.rest.comm.bean.LoginStatusCallback_In;
import com.netbank.rest.comm.bean.LoginStatusCallback_Out;
import com.netbank.rest.comm.bean.RegisterCallback_In;
import com.netbank.rest.comm.bean.RegisterCallback_Out;
import com.netbank.rest.comm.bean.SetSmallPay_In;
import com.netbank.rest.comm.bean.SetSmallPay_Out;
import com.netbank.rest.comm.bean.SvCancel_Txn_In;
import com.netbank.rest.comm.bean.SvCancel_Txn_Out;
import com.netbank.rest.comm.bean.SvCreate_Verify_Txn_In;
import com.netbank.rest.comm.bean.SvCreate_Verify_Txn_Out;
import com.netbank.rest.comm.bean.SvDeRegister_In;
import com.netbank.rest.comm.bean.SvDeRegister_Out;
import com.netbank.rest.comm.bean.SvDisable_Mob_Auth_In;
import com.netbank.rest.comm.bean.SvDisable_Mob_Auth_Out;
import com.netbank.rest.comm.bean.SvGet_Txn_Status_In;
import com.netbank.rest.comm.bean.SvGet_Txn_Status_Out;
import com.netbank.rest.comm.bean.SvLock_Device_In;
import com.netbank.rest.comm.bean.SvLock_Device_Out;
import com.netbank.rest.comm.bean.SvUnlock_Device_In;
import com.netbank.rest.comm.bean.SvUnlock_Device_Out;
import com.netbank.rest.comm.bean.TxnStatusCallback_In;
import com.netbank.rest.comm.bean.TxnStatusCallback_Out;
import com.netbank.rest.util.IDGateResult;
import com.netbank.rest.util.RESTUtil;
import com.netbank.util.CodeUtil;
import com.netbank.util.DateUtils;
import com.netbank.util.ESAPIUtil;
import com.netbank.util.NumericUtil;
import com.netbank.util.fstop.DateUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.IdGateHistoryDao;
import fstop.orm.dao.IdGate_Register_TmpDao;
import fstop.orm.dao.IdGate_Txn_StatusDao;
import fstop.orm.dao.QuickLoginMappingDao;
import fstop.orm.po.IDGATEHISTORY;
import fstop.orm.po.IDGATE_REGISTER_TMP;
import fstop.orm.po.IDGATE_TXN_STATUS;
import fstop.orm.po.IDGATE_TXN_STATUS_PK;
import fstop.orm.po.QUICKLOGINMAPPING;
import fstop.orm.po.QUICKLOGINMAPPING_PK;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.StrUtils;
import fstop.va.VaCheck;
import io.micrometer.shaded.org.pcollections.POrderedSet;
import lombok.extern.slf4j.Slf4j;


@Service
@Slf4j
public class IdGate extends CommonService{
	
	@Autowired
	RESTUtil restutil;
	@Value("${idGateTimeOut:120}")
	Integer idGateTimeOut;
	@Value("${checkIdGateStatus:Y}")
	public String checkIdGateStatus;
	@Autowired
	@Qualifier("n915Telcomm")
	private TelCommExec n915Telcomm;
	@Autowired
	@Qualifier("n93aTelcomm")
	private TelCommExec n93aTelcomm;
	@Autowired
	@Qualifier("n93bTelcomm")
	private TelCommExec n93bTelcomm;
	@Autowired
	@Qualifier("n955Telcomm")
	private TelCommExec n955Telcomm;
	@Autowired
	@Qualifier("n939Telcomm")
	private TelCommExec n939Telcomm;
	
	@Autowired
	QuickLoginMappingDao quickloginmappingdao;

	@Autowired
	IdGateHistoryDao idGateHistoryDao;
	
	@Autowired
	IdGate_Register_TmpDao idgate_register_tmpdao;
	
	@Autowired
	IdGate_Txn_StatusDao idgate_txn_statusdao;
//	開發
//	String idGatePath= "/Common_AP_SIT/WSI";
//	測試
	@Value("${idGatePath:/Common_AP/WSI}")
	String idGatePath= "/Common_AP/WSI";
	String idGateChanel= "iDGate";
	
	@Override	
	public MVH doAction(Map params) {
		String publickey ="";
		MVHImpl mvhimpl = new MVHImpl();
//		List list =null;
		try {
//			if(E2EClient.login()) {
//				publickey = E2EClient.getPublicKey();
//			}
//			if(StrUtils.isNotEmpty(publickey)) {
//				mvhimpl.getFlatValues().put("publicKey", "-----BEGIN RSA PUBLIC KEY-----"+publickey+"-----END RSA PUBLIC KEY-----");
//				mvhimpl.getFlatValues().put("TOPMSG", "0");
//			}else {
//				mvhimpl.getFlatValues().put("publicKey", publickey);
//				mvhimpl.getFlatValues().put("TOPMSG", "Z300");
//			}
			
		} catch (Exception e) {
			throw TopMessageException.create("Z300");
		}
		
		return mvhimpl;
	}
	
	/**
	 * IDGATE 申請啟用碼 、關閉
	 * @param in
	 * @return
	 */
	public IDGateResult idGateN915(IdGateN915_In in) {
		MVHImpl result = null;
		String msgcod = null;
		String opncode = null;
		IDGateResult rs = null;
		IdGateN915_Out out = null;
		try {
			rs = new IDGateResult();
			Map params = CodeUtil.objectCovert(Map.class, in);
			DateTime d1 = new DateTime().minusYears(1911);
			params.put("DATE", d1.toString("yyyMMdd"));
			params.put("TIME", d1.toString("HHmmss"));
			if(in.getFGTXWAY().equals("2")) {
				params.put("KIND", "2");
			}else if(in.getFGTXWAY().equals("4")) {
				params.put("KIND", "1");
			}else {
				params.put("KIND", "3");
			}
			result  = n915Telcomm.query(params);
			if(result != null) {
				msgcod = result.getFlatValues().get("MSGCOD");
				opncode = result.getFlatValues().get("OPNCODE");
			}
			out = new IdGateN915_Out();
			if(StrUtils.isEmpty(msgcod)) {
				rs.setResult(Boolean.TRUE);
				rs.setMsgCode("0");
				rs.setMsgName("");
				out.setOPNCODE(opncode);
			}else {
				rs.setResult(Boolean.FALSE);
				rs.setMsgCode(msgcod);
			}
			out.setMSGCOD(msgcod);
			rs.setData(out);
		} catch (TopMessageException e) {
			log.error("idGateN915.TopMessageException.error{}",e);
			rs.setMsgCode(e.getMsgcode());
			rs.setMessages(e.getMsgout());
			rs.setMsgName(e.getMsgout());	
		} catch (Exception e) {
			log.error("{}",e);
		}
		return rs;
	
	}

	public IDGateResult idGateN93A(IdGateN93A_In in) {
		IDGateResult rs = null;
		MVHImpl result = null;
		IdGateN93A_Out out = null;
		String msgcod = null;
		try {
			rs = new IDGateResult();
			Map params = CodeUtil.objectCovert(Map.class, in);
			DateTime d1 = new DateTime().minusYears(1911);
			params.put("DATE", d1.toString("yyyMMdd"));
			params.put("TIME", d1.toString("HHmmss"));
			result = n93aTelcomm.query(params);
			if(result != null) {
				msgcod = result.getFlatValues().get("RSPCOD");
			}
			out = new IdGateN93A_Out();
			if(msgcod.equals("0000")) {
				rs.setMsgCode("0");
				rs.setMsgName("");
				rs.setResult(Boolean.TRUE);
				out.setCUSIDN(result.getFlatValues().get("CUSIDN"));
				out.setRSPCOD(msgcod);
			}else {
				rs.setResult(Boolean.FALSE);
				rs.setMsgCode(msgcod);
			}
			rs.setData(out);

			IDGateResult n939rs = this.idGateN939Cusidn(in.getCUSIDN());
			out.setN939((IdGateN939_Out)n939rs.getData());
			rs.setData(out);
		} catch (TopMessageException e) {
			log.error("idGateN915.TopMessageException.error{}",e);
			rs.setMsgCode(e.getMsgcode());
			rs.setMessages(e.getMsgout());
			rs.setMsgName(e.getMsgout());	
		} catch (Exception e) {
			log.error("setQuickN936.error{}",e);
		}
		return rs;
	}

	public IDGateResult idGateN93B(IdGateN93B_In in) {
		IDGateResult rs = null;
		MVHImpl result = null;
		IdGateN93A_Out out = null;
		String msgcod = null;
		QUICKLOGINMAPPING po = null;
		QUICKLOGINMAPPING_PK pks = null;
		try {
			rs = new IDGateResult();
			
			Map params = CodeUtil.objectCovert(Map.class, in);
			DateTime d1 = new DateTime().minusYears(1911);
			params.put("DATE", d1.toString("yyyMMdd"));
			params.put("TIME", d1.toString("HHmmss"));
			String cusidn = quickloginmappingdao.getCusidn(in.getIDGATEID(), in.getDEVICEID());
			params.put("CUSIDN", cusidn);
			
			if(in.getCHECKSESSIONID() == null || !in.getCHECKSESSIONID().equals("N")) {
				
				pks = new QUICKLOGINMAPPING_PK();
				pks.setCUSIDN(cusidn);
				pks.setIDGATEID(in.getIDGATEID());
				po = quickloginmappingdao.findById(pks);
				if(po == null) {
					throw new TopMessageException("ZX99","查無驗證資料","查無驗證資料");
				}
				if(po.getQUICK_CALLBACK_TIME() == null) {
					throw new TopMessageException("ZX99","查無驗證資料","查無驗證資料");
				}
				Long nowTime = System.currentTimeMillis();
				log.debug("nowTime >> {}", nowTime);
				log.debug("dbTime >> {}", Long.valueOf(po.getQUICK_CALLBACK_TIME()));
				if(Long.valueOf(po.getQUICK_CALLBACK_TIME()) < nowTime) {
					throw new TopMessageException("ZX99","驗證資料逾時","驗證資料逾時");
				}
				if(!po.getQUICKID().equals(in.getSESSIONID())) {
					throw new TopMessageException("ZX99","session驗證錯誤","session驗證錯誤");
				}
				
			}
			
			result = n93bTelcomm.query(params);
			if(result != null) {
				msgcod = result.getFlatValues().get("MSGCOD");
			}
			out = new IdGateN93A_Out();
			if(StrUtils.isEmpty(msgcod)) {
				rs.setMsgCode("0");
				rs.setMsgName("");
				rs.setResult(Boolean.TRUE);
			}else {
				rs.setResult(Boolean.FALSE);
				rs.setMsgCode(msgcod);
			}
			rs.addAllData(result.getFlatValues());
			rs.addData("CUSIDN", params.get("CUSIDN"));
		} catch (TopMessageException e) {
			log.error("idGateN915.TopMessageException.error{}",e);
			rs.setMsgCode(e.getMsgcode());
			rs.setMessages(e.getMsgout());
			rs.setMsgName(e.getMsgout());	
		} catch (Exception e) {
			log.error("setQuickN936.error{}",e);
		}
		return rs;
	}
	
	/**
	 * 驗證啟用碼
	 * @param in
	 * @return
	 */
	public IDGateResult idGateN955(IdGateN955_In in) {
		MVHImpl result = null;
		String msgcod = null;
		IDGateResult rs = null;
		IdGateN955_Out out = null;
		String sessionid = null;
		IDGATE_REGISTER_TMP po = null;
		try {
			rs = new IDGateResult();
			Map params = CodeUtil.objectCovert(Map.class, in);
			DateTime d1 = new DateTime().minusYears(1911);
			params.put("DATE", d1.toString("yyyMMdd"));
			params.put("TIME", d1.toString("HHmmss"));
//		          送出前要呼叫IDGATE_PINKEY (單純作加密 不呼叫N951)
			result  = n955Telcomm.query(params);
			msgcod = result.getFlatValues().get("MSGCOD");
			if(result != null) {
				msgcod = result.getFlatValues().get("MSGCOD");
			}
			out = new IdGateN955_Out();
//		如果成功要產生一組UUID 當SessionID 寫入tabel 並回傳給APP
			if(msgcod.equals("0000")) {
				sessionid = UUID.randomUUID().toString();
				d1 = new DateTime();
				po = new IDGATE_REGISTER_TMP();
				po.setSESSIONID(sessionid);
				po.setDEVICEID(in.getDEVICEID());
				po.setDEVICENAME(in.getDEVICENAME());
				po.setDEVICETYPE(in.getDEVICETYPE());
				
				po.setDEVICEBRAND(in.getDEVICEBRAND());
				po.setDEVICEOS(in.getDEVICEOS());
				
				po.setLASTDATE(d1.toString("yyyyMMdd"));
				po.setLASTTIME(d1.toString("HHmmss"));
				po.setCUSIDN(in.getCUSIDN());
				po.setITEM("01");
				String pinkey = in.getPINKEY().substring(in.getPINKEY().length()-8, in.getPINKEY().length());
				po.setOPNCODE(pinkey);
				po.setITEM("02");
				idgate_register_tmpdao.save(po);
				
				rs.setResult(Boolean.TRUE);
				rs.setMsgCode("0");
				rs.setMsgName("");
				out.setSESSIONID(sessionid);
			}else {
				rs.setResult(Boolean.FALSE);
				rs.setMsgCode(msgcod);
			}
			out.setMSGCOD(msgcod);
			rs.setData(out);
		} catch (TopMessageException e) {
			log.error("idGateN955.TopMessageException.error{}",e);
			rs.setMsgCode(e.getMsgcode());
			rs.setMessages(e.getMsgout());
			rs.setMsgName(e.getMsgout());
		} catch (Exception e) {
			log.error("idGateN955.error{}",e);
		}
		return rs;
		
	}
	
	public IDGateResult idGateN939Cusidn(String cusidn) {
		IDGateResult rs = null;
		MVHImpl result = null;
		IdGateN939_Out out = null;
		String msgcod = null;
		try {
			rs = new IDGateResult();
			IdGateN939_In in = new IdGateN939_In();
			in.setCUSIDN(cusidn);
			Map params = CodeUtil.objectCovert(Map.class, in);
			DateTime d1 = new DateTime().minusYears(1911);
			params.put("DATE", d1.toString("yyyMMdd"));
			params.put("TIME", d1.toString("HHmmss"));
			result = n939Telcomm.query(params);
			if(result != null) {
				msgcod = result.getFlatValues().get("RSPCOD");
			}
			out = new IdGateN939_Out();
			if(msgcod.equals("0000")) {
				rs.setMsgCode("0");
				rs.setMsgName("");
				rs.setResult(Boolean.TRUE);
				out.setQCKLGIN(result.getFlatValues().get("QCKLGIN"));
				out.setQCKTRAN(result.getFlatValues().get("QCKTRAN"));
				out.setSMALLCNT(NumericUtil.fmtAmount(result.getFlatValues().get("SMALLCNT"), 0));
				out.setSMALLPAY(result.getFlatValues().get("SMALLPAY"));
				out.setRSPCOD(msgcod);
			}else {
				rs.setResult(Boolean.FALSE);
				rs.setMsgCode(msgcod);
			}
			rs.setData(out);
		} catch (TopMessageException e) {
			log.error("idGateN915.TopMessageException.error{}",e);
			rs.setMsgCode(e.getMsgcode());
			rs.setMessages(e.getMsgout());
			rs.setMsgName(e.getMsgout());	
		} catch (Exception e) {
			log.error("setQuickN936.error{}",e);
		}
		return rs;
	}
	
	public IDGateResult idGateN939(IdGateN939_In in) {
		IDGateResult rs = null;
		MVHImpl result = null;
		IdGateN939_Out out = null;
		String msgcod = null;
		try {
			rs = new IDGateResult();
			String cusidn = quickloginmappingdao.getCusidn(in.getIDGATEID(), in.getDEVICEID());
			if(cusidn == null) {
				throw new TopMessageException("ZX99","查無帳號","查無帳號");
			}
			rs = idGateN939Cusidn(cusidn);
//			in.setCUSIDN(cusidn);
//			Map params = CodeUtil.objectCovert(Map.class, in);
//			DateTime d1 = new DateTime().minusYears(1911);
//			params.put("DATE", d1.toString("yyyMMdd"));
//			params.put("TIME", d1.toString("HHmmss"));
//			result = n939Telcomm.query(params);
//			if(result != null) {
//				msgcod = result.getFlatValues().get("RSPCOD");
//			}
//			out = new IdGateN939_Out();
//			if(msgcod.equals("0000")) {
//				rs.setMsgCode("0");
//				rs.setMsgName("");
//				rs.setResult(Boolean.TRUE);
//				out.setQCKLGIN(result.getFlatValues().get("QCKLGIN"));
//				out.setQCKTRAN(result.getFlatValues().get("QCKTRAN"));
//				out.setSMALLCNT(NumericUtil.fmtAmount(result.getFlatValues().get("SMALLCNT"), 0));
//				out.setSMALLPAY(result.getFlatValues().get("SMALLPAY"));
//				out.setRSPCOD(msgcod);
//			}else {
//				rs.setResult(Boolean.FALSE);
//				rs.setMsgCode(msgcod);
//			}
//			rs.setData(out);
		} catch (TopMessageException e) {
			log.error("idGateN915.TopMessageException.error{}",e);
			rs.setMsgCode(e.getMsgcode());
			rs.setMessages(e.getMsgout());
			rs.setMsgName(e.getMsgout());	
		} catch (Exception e) {
			log.error("setQuickN936.error{}",e);
		}
		return rs;
	}
	
	public IDGateResult idGateQuerySession(IdGateQuerySession_In in) {
		IDGateResult rs = null;
		QUICKLOGINMAPPING po = null;
		IdGateQuerySession_Out out = null;
		String sessionid = null;
		try {
			rs = new IDGateResult();
			out = new IdGateQuerySession_Out();
			po = quickloginmappingdao.getDataByDeviceIdAndIdGateId(in.getDEVICEID(), in.getIDGATEID());
			log.debug("po1>>>>>>{}"+CodeUtil.toJson(po));
			if(po != null) {
				//抓取idGateId 狀態(與idgate server)
				Get_Device_Status_In sin = new Get_Device_Status_In();
				sin.setIdgateID(po.getPks().getIDGATEID());
				IDGateResult sRes = this.svGet_Device_Status(sin);
				if(sRes.getResult()) {
					//回存狀態回DB
					Get_Device_Status_Out sOut = (Get_Device_Status_Out)sRes.getData();
					po.setDEVICESTATUS(sOut.getMemberStatus());
					po.setVERIFYTYPE(this.getVerifyType(CodeUtil.objectCovert(Map.class, sOut.getType())));
					po.setLASTDATE(DateUtil.getDate(""));
					po.setLASTTIME(DateUtil.getTheTime(""));
					log.debug("po2>>>>>>{}"+CodeUtil.toJson(po));
					quickloginmappingdao.save(po);
					
					if(sOut.getMemberStatus().equals("0")) {
						if(in.getCREATESESSION().equals("Y")) {
							//產生session並存入DB
							sessionid = UUID.randomUUID().toString();
							DateTime d1 = new DateTime();
							IDGATE_REGISTER_TMP po_tmp =  new IDGATE_REGISTER_TMP();
							po_tmp.setSESSIONID(sessionid);
							po_tmp.setDEVICEID(po.getDEVICEID());
							po_tmp.setDEVICENAME(po.getDEVICENAME());
							po_tmp.setDEVICETYPE(po.getDEVICETYPE());
							po_tmp.setDEVICEBRAND(po.getDEVICEBRAND());
							po_tmp.setDEVICEOS(po.getDEVICEOS());
							po_tmp.setLASTDATE(d1.toString("yyyyMMdd"));
							po_tmp.setLASTTIME(d1.toString("HHmmss"));
							po_tmp.setCUSIDN(po.getPks().getCUSIDN());
							log.debug("po_tmp>>>>>>{}"+CodeUtil.toJson(po_tmp));
							idgate_register_tmpdao.save(po_tmp);
						}
					}
				}
//				//取得是否擁有快登資格
//				IdGateN939_In n939in = new IdGateN939_In();
//				n939in.setDEVICEID(in.getDEVICEID());
//				n939in.setIDGATEID(in.getIDGATEID());
//				IDGateResult n939rs = this.idGateN939(n939in);
//				if(!n939rs.getResult()) {
//					return n939rs;
//				}
//				IdGateN939_Out n939out = (IdGateN939_Out)n939rs.getData();
				//寫入回傳資料
//				out.setISHASQCKLGIN(n939out.getQCKLGIN());

				Map tmp  = CodeUtil.objectCovert(Map.class, po);
				Map pks = (Map) tmp.get("pks");
				tmp.put("CUSIDN", pks.get("CUSIDN"));
				tmp.put("IDGATEID", pks.get("IDGATEID"));
				tmp.remove("pks");
				log.debug("tmp>>>>>>{}"+CodeUtil.toJson(tmp));
				out.setData(tmp);
				out.setSESSIONID(sessionid);
				rs.setMsgCode("0");
				rs.setResult(Boolean.TRUE);
				log.debug("out>>>>>>{}"+CodeUtil.toJson(out));
				rs.setData(out);
			}else {
				rs.setMsgCode("ENRD");
				rs.setResult(Boolean.FALSE);
				rs.setMessages("查無資料");
				rs.setMsgName("查無資料");
				return rs;
			}
			
		} catch (Exception e) {
			log.error("idGateMappingQuery.error>>{}",e);
		}
		return rs;
	}
	
	/**
	 * 
	 * @return
	 */
	public IDGateResult idGateMappingQuery(IdGateMappingQuery_In in ) {
		IDGateResult rs = null;
		List<QUICKLOGINMAPPING> dataList = null; 
		List<Map> newDataList = null; 
		QUICKLOGINMAPPING po = null;
		IdGateMappingQuery_Out out = null;
		try {
			rs = new IDGateResult();
			out = new IdGateMappingQuery_Out();
			if(StrUtils.isNotEmpty(in.getDEVICEID()) && StrUtils.isNotEmpty(in.getIDGATEID())) {
				//有填入DEVICEID與IDGATEID查詢單筆
				po = quickloginmappingdao.getDataByInputData(in.getCUSIDN(), in.getIDGATEID(), in.getDEVICEID());
				log.debug("po1>>>>>>{}",CodeUtil.toJson(po));
				if(po==null) {
					rs.setMsgCode("ENRD");
					rs.setResult(Boolean.FALSE);
					rs.setMessages("查無資料");
					rs.setMsgName("查無資料");
					rs.setData(out);
					return rs;
				}else {
					newDataList = new LinkedList<Map>();

					//抓取idGateId 狀態(與idgate server)
					Get_Device_Status_In sin = new Get_Device_Status_In();
					sin.setIdgateID(po.getPks().getIDGATEID());
					IDGateResult sRes = this.svGet_Device_Status(sin);
					if(sRes.getResult()) {
						//回存狀態回DB
						Get_Device_Status_Out sOut = (Get_Device_Status_Out)sRes.getData();
						po.setDEVICESTATUS(sOut.getMemberStatus());
						po.setVERIFYTYPE(this.getVerifyType(CodeUtil.objectCovert(Map.class, sOut.getType())));
						po.setLASTDATE(DateUtil.getDate(""));
						po.setLASTTIME(DateUtil.getTheTime(""));
						log.debug("po2>>>>>>{}",CodeUtil.toJson(po));
						quickloginmappingdao.save(po);
						
						if(sOut.getMemberStatus().equals("0") || sOut.getMemberStatus().equals("1") || sOut.getMemberStatus().equals("2")) {
							//idgate狀態正常
							Map tmp  = CodeUtil.objectCovert(Map.class, po);
							Map pks = (Map) tmp.get("pks");
							tmp.put("CUSIDN", pks.get("CUSIDN"));
							tmp.put("IDGATEID", pks.get("IDGATEID"));
							tmp.remove("pks");
							newDataList.add(tmp);
						}
					}
					
				}
			}else {
				//全數查詢
				dataList = quickloginmappingdao.getDataByCusidn(in.getCUSIDN());
				log.debug("dataList>>>>>>{}",CodeUtil.toJson(dataList));
				if(dataList==null || dataList.isEmpty()) {
					rs.setMsgCode("ENRD");
					rs.setResult(Boolean.FALSE);
					rs.setMessages("查無資料");
					rs.setMsgName("查無資料");
					rs.setData(out);
					return rs;
				}else {
					newDataList = new LinkedList<Map>();
					for(QUICKLOGINMAPPING o:dataList) {

						//抓取idGateId 狀態(與idgate server)
						Get_Device_Status_In sin = new Get_Device_Status_In();
						sin.setIdgateID(o.getPks().getIDGATEID());
						IDGateResult sRes = this.svGet_Device_Status(sin);
						if(sRes.getResult()) {
							//回存狀態回DB
							Get_Device_Status_Out sOut = (Get_Device_Status_Out)sRes.getData();
							o.setDEVICESTATUS(sOut.getMemberStatus());
							o.setVERIFYTYPE(this.getVerifyType(CodeUtil.objectCovert(Map.class, sOut.getType())));
							o.setLASTDATE(DateUtil.getDate(""));
							o.setLASTTIME(DateUtil.getTheTime(""));
							quickloginmappingdao.save(o);
							if(sOut.getMemberStatus().equals("0") || sOut.getMemberStatus().equals("1") || sOut.getMemberStatus().equals("2")) {
								//idgate狀態正常
								Map tmp  = CodeUtil.objectCovert(Map.class, o);
								Map pks = (Map) tmp.get("pks");
								tmp.put("CUSIDN", pks.get("CUSIDN"));
								tmp.put("IDGATEID", pks.get("IDGATEID"));
								tmp.remove("pks");
								newDataList.add(tmp);
							}
						}
					}
				}
			}
			
			rs.setMsgCode("0");
			rs.setResult(Boolean.TRUE);
			log.debug("po end>>>>>>{}"+CodeUtil.toJson(po));
			log.debug("newDataList end>>>>>>{}"+CodeUtil.toJson(newDataList));
			out.setData(po);
			out.setREC(newDataList);
//			out.setREC(dataList);
			log.debug("out>>>>>>{}"+CodeUtil.toJson(out));
			rs.setData(out);
		} catch (Exception e) {
			log.error("idGateMappingQuery.error>>{}",e);
		}
		return rs;
	}
	
	public IDGateResult idGateMappingUpdate(IdGateMappingUpdate_In in) {
		IDGateResult rs = null;
		QUICKLOGINMAPPING po = null;
		try {
			rs = new IDGateResult();
			po = quickloginmappingdao.getDataByInputData(in.getCUSIDN(), in.getIDGATEID(), in.getDEVICEID());
			if(po==null) {
				rs.setMsgCode("ENRD");
				rs.setResult(Boolean.FALSE);
				rs.setMessages("查無資料");
				rs.setMsgName("查無資料");
				return rs;
			}else {
				//update db
				po.setDEVICENAME(in.getDEVICENAME());
				po.setLASTDATE(DateUtil.getDate(""));
				po.setLASTTIME(DateUtil.getTheTime(""));
				quickloginmappingdao.save(po);
				
				//查詢現在狀快
				IdGateMappingQuery_In qIn = new IdGateMappingQuery_In();
				qIn.setCUSIDN(in.getCUSIDN());
				IDGateResult qRs = this.idGateMappingQuery(qIn);
				
				rs.setData(qRs.getData());
				rs.setMsgCode("0");
				rs.setMsgName("");
				rs.setResult(Boolean.TRUE);
			}
		} catch (Exception e) {
			log.error("idGateMappingUpdate.error>>{}",e);
			rs.setMsgCode("FE0003");
			rs.setResult(Boolean.FALSE);
			rs.setMessages("寫入錯誤");
			rs.setMsgName("寫入錯誤");
			return rs;
		}
		return rs;
	}

	public IDGateResult idGateMappingDelete(IdGateMappingDelete_In in) {
		IDGateResult rs = null;
		QUICKLOGINMAPPING po = null;
		try {
			rs = new IDGateResult();
			po = quickloginmappingdao.getDataByInputData(in.getCUSIDN(), in.getIDGATEID(), in.getDEVICEID());
			if(po==null) {
				rs.setMsgCode("ENRD");
				rs.setResult(Boolean.FALSE);
				rs.setMessages("查無資料");
				rs.setMsgName("查無資料");
				return rs;
			}else {
				//刪除資料
//				quickloginmappingdao.delete(po);
				po.setDEVICESTATUS("9");
				quickloginmappingdao.save(po);
				
				//暫時註解不跟IDGATE Server註銷
				SvDeRegister_In din = new SvDeRegister_In();
				din.setIdgateID(in.getIDGATEID());
				this.svDeRegister(din);
				
				//查詢現在狀快
				IdGateMappingQuery_In qIn = new IdGateMappingQuery_In();
				qIn.setCUSIDN(in.getCUSIDN());
				IDGateResult qRs = this.idGateMappingQuery(qIn);

				rs.setData(qRs.getData());
				rs.setMsgCode("0");
				rs.setMsgName("");
				rs.setResult(Boolean.TRUE);
			}
		} catch (Exception e) {
			log.error("idGateMappingDelete.error>>{}",e);
			rs.setMsgCode("FE0003");
			rs.setResult(Boolean.FALSE);
			rs.setMessages("刪除錯誤");
			rs.setMsgName("刪除錯誤");
			return rs;
		}
		return rs;
	}
	
	public IDGateResult idGateQuickSessionId(IdGateQuickSessionId_In in) {
		IDGateResult rs = null;
		IDGATE_REGISTER_TMP po = null;
		String sessionid = null;
		IdGateQuickSessionId_Out out = null;
		log.debug("IdGateQuickSessionId_In >>>>>>>{}",CodeUtil.toJson(in));
		try {
			rs = new IDGateResult();
			out = new IdGateQuickSessionId_Out();
			sessionid = UUID.randomUUID().toString();
			DateTime d1 = new DateTime();
			po = new IDGATE_REGISTER_TMP();
			po.setSESSIONID(sessionid);
			po.setDEVICEID(in.getDEVICEID());
			po.setDEVICENAME(in.getDEVICENAME());
			po.setDEVICETYPE(in.getDEVICETYPE());
			po.setDEVICEBRAND(in.getDEVICEBRAND());
			po.setDEVICEOS(in.getDEVICEOS());
			po.setLASTDATE(d1.toString("yyyyMMdd"));
			po.setLASTTIME(d1.toString("HHmmss"));
			po.setITEM(in.getITEM());
			po.setCUSIDN(in.getCUSIDN());
			log.debug("po>>>>>>{}"+CodeUtil.toJson(po));
			idgate_register_tmpdao.save(po);
			rs.setResult(Boolean.TRUE);
			rs.setMsgCode("0");
			rs.setMsgName("");
			out.setSessionid(sessionid);
			rs.setData(out);
			log.debug("idGateQuickSessionId rs >>>>>>>{}",CodeUtil.toJson(rs));
		} catch (Exception e) {
			log.error("idGateQuickSessionId.error>>{}",e);
			rs.setMsgCode("FE0003");
			rs.setResult(Boolean.FALSE);
			rs.setMessages("快登快交開關失敗");
			rs.setMsgName("快登快交開關失敗");
			return rs;
		}
		return rs;
	}
	
	public IDGateResult closeQuick(CloseQuick_In in) {
		IDGateResult rs = null;
		CloseQuick_Out out = null;
		QUICKLOGINMAPPING po = null;
		QUICKLOGINMAPPING_PK qpks = null;
		QUICKLOGINMAPPING rpo = null;
		List<QUICKLOGINMAPPING> dataList = null; 
		List<Map> newDataList = null; 
		log.debug("CloseQuick_In >>>>>>>{}",CodeUtil.toJson(in));
		try {
			rs = new IDGateResult();
			out= new CloseQuick_Out();
			qpks = new QUICKLOGINMAPPING_PK();
			qpks.setCUSIDN(in.getCUSIDN());
			qpks.setIDGATEID(in.getIDGATEID());
			po = quickloginmappingdao.findById(qpks);
			log.debug("CloseQuick po >>>>>>>{}",CodeUtil.toJson(po));
			if(po==null) {
				rs.setMsgCode("ENRD");
				rs.setResult(Boolean.FALSE);
				rs.setMessages("查無資料");
				rs.setMsgName("查無資料");
				return rs;
			}else {
				if ("01".equals(in.getITEM())) {
					po.setQCKLGIN("N");
				}else {
					po.setQCKTRAN("N");
					po.setSMALLPAY("N");
				}
				po.setLASTDATE(DateUtil.getDate(""));
				po.setLASTTIME(DateUtil.getTheTime(""));
				quickloginmappingdao.save(po);
			}
			newDataList = new LinkedList<Map>();
			rpo = quickloginmappingdao.findById(qpks);
			log.debug("rpo >>>>> {}",CodeUtil.toJson(rpo));
			Map tmp  = CodeUtil.objectCovert(Map.class, rpo);
			Map pks = (Map) tmp.get("pks");
			tmp.put("CUSIDN", pks.get("CUSIDN"));
			tmp.put("IDGATEID", pks.get("IDGATEID"));
			tmp.remove("pks");
			newDataList.add(tmp);
			out.setREC(newDataList);
			rs.setData(out);
			rs.setMsgCode("0");
			rs.setMsgName("快登快交變更成功");
			rs.setResult(Boolean.TRUE);
			log.debug("CloseQuick rs >>>>>>>{}",CodeUtil.toJson(rs));
		} catch (Exception e) {
			log.error("idGateMappingDelete.error>>{}",e);
			rs.setMsgCode("FE0003");
			rs.setResult(Boolean.FALSE);
			rs.setMessages("快登快交變更失敗");
			rs.setMsgName("快登快交變更失敗");
			return rs;
		}
		return rs;
	}
	
	public IDGateResult setSmallPay(SetSmallPay_In in) {
		IDGateResult rs = null;
		SetSmallPay_Out out = null;
		QUICKLOGINMAPPING po = null;
		QUICKLOGINMAPPING_PK qpks = null;
		QUICKLOGINMAPPING rpo = null;
		List<Map> newDataList = null; 
		log.debug("SetSmallPay_In >>>>> {}",CodeUtil.toJson(in));
		try {
			rs = new IDGateResult();
			out = new SetSmallPay_Out();
			qpks = new QUICKLOGINMAPPING_PK();
			qpks.setCUSIDN(in.getCUSIDN());
			qpks.setIDGATEID(in.getIDGATEID());
			po = quickloginmappingdao.findById(qpks);
			log.debug("SetSmallPay po >>>>> {}",CodeUtil.toJson(po));
			if(po==null) {
				rs.setMsgCode("ENRD");
				rs.setResult(Boolean.FALSE);
				rs.setMessages("查無資料");
				rs.setMsgName("查無資料");
				return rs;
			}else {
				if ("01".equals(in.getTYPE())&&!"Y".equals(po.getQCKTRAN())) {
						rs.setResult(Boolean.FALSE);
						rs.setMessages("快速交易尚未開啟");
						rs.setMsgName("快速交易尚未開啟");
						return rs;
				}
				String SMALLPAY = "01".equals(in.getTYPE())?"Y":"N";
				po.setSMALLPAY(SMALLPAY);
				po.setLASTDATE(DateUtil.getDate(""));
				po.setLASTTIME(DateUtil.getTheTime(""));
				quickloginmappingdao.save(po);
			}
//			dataList = quickloginmappingdao.getDataByCusidn(in.getCUSIDN());
			newDataList = new LinkedList<Map>();
			rpo = quickloginmappingdao.findById(qpks);
			log.debug("rpo >>>>> {}",CodeUtil.toJson(rpo));
			Map tmp  = CodeUtil.objectCovert(Map.class, rpo);
			Map pks = (Map) tmp.get("pks");
			tmp.put("CUSIDN", pks.get("CUSIDN"));
			tmp.put("IDGATEID", pks.get("IDGATEID"));
			tmp.remove("pks");
			newDataList.add(tmp);
			out.setREC(newDataList);
			rs.setData(out);
			rs.setMsgCode("0");
			rs.setMsgName("小額支付變更成功");
			rs.setResult(Boolean.TRUE);
			log.debug("SetSmallPay rs >>>>> {}",CodeUtil.toJson(rs));
		} catch (Exception e) {
			log.error("idGateMappingDelete.error>>{}",e);
			rs.setMsgCode("FE0003");
			rs.setResult(Boolean.FALSE);
			rs.setMessages("小額支付變更失敗");
			rs.setMsgName("小額支付變更失敗");
			return rs;
		}
		return rs;
	}
	
	/**
	 * idgateID 狀態查詢
	 * @param in
	 * @return
	 */
	public IDGateResult svGet_Device_Status(Get_Device_Status_In in) {
		String json = null;
		HashMap<String,String> params = null;
		Get_Device_Status_Out out = null ;
		IDGateResult rs = null;
		try {
			in.setChannel(idGateChanel);
			in.setMethod("svGet_Device_Status");
			params = CodeUtil.objectCovert(HashMap.class, in);
			json = restutil.send2IdGate(params, idGatePath,idGateTimeOut ,HttpMethod.POST);
			rs = new IDGateResult();
			if(StrUtils.isNotEmpty(json)) {
				out = CodeUtil.fromJson(json, Get_Device_Status_Out.class);
				if("0000".equals(out.getReturnCode()) ) {
					rs.setResult(Boolean.TRUE);
					rs.setMsgCode("0");
					rs.setMsgName(out.getReturnMsg());
					rs.setData(out);
				}else {
					rs.setMsgCode(out.getReturnCode().toString());
					rs.setMsgName(out.getReturnMsg());
					rs.setData(out);
				}
			}else {
				log.warn("json is null");
			}
			
		} catch (Exception e) {
			log.error("{}",e);
			rs.setMsgCode("FE0010");
			rs.setResult(Boolean.FALSE);
		}
		return rs;
		
	}
	/**
	 * idgateID 狀態查詢
	 * @param in
	 * @return
	 */
	public IDGateResult svDisable_Mob_Auth(SvDisable_Mob_Auth_In in) {
		String json = null;
		HashMap<String,String> params = null;
		SvDisable_Mob_Auth_Out out = null ;
		IDGateResult rs = null;
		try {
			in.setChannel(idGateChanel);
			in.setMethod("svDisable_Device_Auth");
			params = CodeUtil.objectCovert(HashMap.class, in);
			json = restutil.send2IdGate(params, idGatePath,idGateTimeOut ,HttpMethod.POST);
			rs = new IDGateResult();
			if(StrUtils.isNotEmpty(json)) {
				out = CodeUtil.fromJson(json, SvDisable_Mob_Auth_Out.class);
				if("0000".equals(out.getReturnCode()) ) {
					rs.setResult(Boolean.TRUE);
					rs.setMsgCode("0");
					rs.setMsgName(out.getReturnMsg());
					rs.setData(out);
				}else {
					rs.setMsgCode(out.getReturnCode().toString());
					rs.setMsgName(out.getReturnMsg());
					rs.setData(out);
				}
			}else {
				log.warn("json is null");
			}
			
		} catch (Exception e) {
			log.error("{}",e);
			rs.setMsgCode("FE0010");
			rs.setResult(Boolean.FALSE);
		}
		return rs;
		
	}
	
	
	
	/**
	 * idgateID 解鎖
	 * @param in
	 * @return
	 */
	public IDGateResult svUnlock_Device(SvUnlock_Device_In in) {
		String json = null;
		HashMap<String,String> params = null;
		SvUnlock_Device_Out out = null ;
		IDGateResult rs = null;
		try {
			in.setChannel(idGateChanel);
			in.setMethod("svUnlock_Device");
			params = CodeUtil.objectCovert(HashMap.class, in);
			json = restutil.send2IdGate(params, idGatePath,idGateTimeOut ,HttpMethod.POST);
			rs = new IDGateResult();
			if(StrUtils.isNotEmpty(json)) {
				out = CodeUtil.fromJson(json, SvUnlock_Device_Out.class);
				if("0000".equals(out.getReturnCode()) ) {
					rs.setResult(Boolean.TRUE);
					rs.setMsgCode("0");
					rs.setMsgName(out.getReturnMsg());
					rs.setData(out);
				}else {
					rs.setMsgCode(out.getReturnCode().toString());
					rs.setMsgName(out.getReturnMsg());
					rs.setData(out);
				}
			}else {
				log.warn("json is null");
			}
			
		} catch (Exception e) {
			log.error("{}",e);
			rs.setMsgCode("FE0010");
			rs.setResult(Boolean.FALSE);
		}
		return rs;
		
	}
	
	/**
	 * idgateID 鎖定
	 * @param in
	 * @return
	 */
	public IDGateResult svLock_Device(SvLock_Device_In in) {
		String json = null;
		HashMap<String,String> params = null;
		SvLock_Device_Out out = null ;
		IDGateResult rs = null;
		try {
			in.setChannel(idGateChanel);
			in.setMethod("svLock_Device");
			params = CodeUtil.objectCovert(HashMap.class, in);
			json = restutil.send2IdGate(params, idGatePath,idGateTimeOut ,HttpMethod.POST);
			rs = new IDGateResult();
			if(StrUtils.isNotEmpty(json)) {
				out = CodeUtil.fromJson(json, SvLock_Device_Out.class);
				if("0000".equals(out.getReturnCode()) ) {
					rs.setResult(Boolean.TRUE);
					rs.setMsgCode("0");
					rs.setMsgName(out.getReturnMsg());
					rs.setData(out);
				}else {
					rs.setMsgCode(out.getReturnCode().toString());
					rs.setMsgName(out.getReturnMsg());
					rs.setData(out);
				}
			}else {
				log.warn("json is null");
			}
			
		} catch (Exception e) {
			log.error("{}",e);
			rs.setMsgCode("FE0010");
			rs.setResult(Boolean.FALSE);
		}
		return rs;
		
	}
	
	/**
	 * idgateID 註銷
	 * @param in
	 * @return
	 */
	public IDGateResult svDeRegister(SvDeRegister_In in) {
		String json = null;
		HashMap<String,String> params = null;
		SvDeRegister_Out out = null ;
		IDGateResult rs = null;
		try {
			in.setChannel(idGateChanel);
			in.setMethod("svDeRegister");
			params = CodeUtil.objectCovert(HashMap.class, in);
			json = restutil.send2IdGate(params, idGatePath,idGateTimeOut ,HttpMethod.POST);
			rs = new IDGateResult();
			if(StrUtils.isNotEmpty(json)) {
				out = CodeUtil.fromJson(json, SvDeRegister_Out.class);
				if("0000".equals(out.getReturnCode()) ) {
					rs.setResult(Boolean.TRUE);
					rs.setMsgCode("0");
					rs.setMsgName(out.getReturnMsg());
					rs.setData(out);
				}else {
					rs.setMsgCode(out.getReturnCode().toString());
					rs.setMsgName(out.getReturnMsg());
					rs.setData(out);
				}
			}else {
				log.warn("json is null");
			}
			
		} catch (Exception e) {
			log.error("{}",e);
			rs.setMsgCode("FE0010");
			rs.setResult(Boolean.FALSE);
		}
		return rs;
		
	}
	
	/**
	 * 由idgate Serer 主動呼叫
	 * 告訴ms快交狀態
	 * @param in
	 * @return
	 */
	public TxnStatusCallback_Out txnStatusCallback(TxnStatusCallback_In in) {
		TxnStatusCallback_Out out = null ;
		IDGATE_TXN_STATUS po = null;
		IDGATE_TXN_STATUS_PK pks = null;
		try {
			out  = new TxnStatusCallback_Out();
			pks = new IDGATE_TXN_STATUS_PK();
			
			pks.setIDGATEID(in.getIdgateID());
			pks.setTXNID(in.getTxnID());
			if (!"03".equals(in.getTxnStatus())) {
				pks.setSESSIONID(in.getSessionID());
				po = idgate_txn_statusdao.findById(pks);
			}else {
				po = idgate_txn_statusdao.findBytxnid(in.getIdgateID(), in.getTxnID());
			}
			
			
			if(po == null) {
				out.setReturnCode("0001");
				out.setReturnMsg("設定失敗,SessionID "+in.getSessionID()+"查無資料");
				return out;
			}else {
				po.setTXNSTATUS(in.getTxnStatus());
				po.setLASTDATE(DateUtil.getDate(""));
				po.setLASTTIME(DateUtil.getTheTime(""));

				log.trace(ESAPIUtil.vaildLog("po is >> " + po));
				idgate_txn_statusdao.save(po);
				
				out.setReturnCode("0000");
				out.setReturnMsg("修改成功");
			}
			
		} catch (Exception e) {
			log.error("{}",e);
			out.setReturnCode("0002");
			out.setReturnMsg("修改失敗,系統異常");
		}
		return out;
		
	}
	
	/**
	 * 由idgate Serer 主動呼叫
	 * 告訴ms登入狀態
	 * @param in
	 * @return
	 */
	public ChangeSettingCallback_Out changeSettingCallback(ChangeSettingCallback_In in) {
		ChangeSettingCallback_Out out = null ;
		QUICKLOGINMAPPING po = null;
		IDGATE_REGISTER_TMP idgate_register = null;
		try {
			out  = new ChangeSettingCallback_Out();
			idgate_register  = idgate_register_tmpdao.findById(in.getSessionID());
			if(idgate_register == null) {
				out.setReturnCode("0001");
				out.setReturnMsg("驗證失敗,SessionID "+in.getSessionID()+"查無資料");
				return out;
			}
			po = quickloginmappingdao.getDataByIdGateId(in.getIdgateID());
			if(po != null) {
				if(in.getType().getDiagram().equals("1") && in.getType().getBio().equals("1")) {

					if(po.getVERIFYTYPE() != null || po.getVERIFYTYPE().equals("") || !po.getVERIFYTYPE().equals("0")) {
						//過濾原有Type
						if(po.getVERIFYTYPE().equals("1")) {
							in.getType().setBio("0");
						}else if(po.getVERIFYTYPE().equals("2")) {
							in.getType().setDiagram("0");
						}else if(po.getVERIFYTYPE().equals("3")) {
							in.getType().setPwd("0");
						}
						//呼叫IDGATE Disable
						SvDisable_Mob_Auth_In disablein = new SvDisable_Mob_Auth_In();
						disablein.setIdgateID(in.getIdgateID());
						disablein.setAuthType(po.getVERIFYTYPE());
						IDGateResult disableout = svDisable_Mob_Auth(disablein);
						if(!disableout.getResult()) {
							out.setReturnCode(disableout.getMsgCode());
							out.setReturnMsg(disableout.getMsgName());
							return out;
						}
					}
				}
				
				Map<String,String> tmp = CodeUtil.objectCovert(Map.class, in.getType()) ;
				po.setVERIFYTYPE(getVerifyType(tmp));
				po.setLASTDATE(DateUtil.getDate(""));
				po.setLASTTIME(DateUtil.getTheTime(""));
				po.setUPDATEUSER("SYS");

				log.trace(ESAPIUtil.vaildLog("po is >> " + po));
				quickloginmappingdao.save(po);;
				
				out.setReturnCode("0000");
				out.setReturnMsg("修改成功");
			}else {
				out.setReturnCode("0001");
				out.setReturnMsg("修改失敗, IdGateId "+in.getIdgateID()+"查無資料");
				return out;
			}
		} catch (Exception e) {
			log.error("{}",e);
			out.setReturnCode("0002");
			out.setReturnMsg("修改失敗,系統異常");
		}
		return out;
		
	}
	
	/**
	 * 由idgate Serer 主動呼叫
	 * 告訴ms登入狀態
	 * @param in
	 * @return
	 */
	public LoginStatusCallback_Out loginStatusCallback(LoginStatusCallback_In in) {
		LoginStatusCallback_Out out = null ;
		IDGATE_REGISTER_TMP idgate_register = null;
		QUICKLOGINMAPPING_PK pks = null;
		QUICKLOGINMAPPING po = null;
		int quickLoginLimit = 1000 * 60 * 2;
		List<QUICKLOGINMAPPING> dataList = null; 
		log.debug("LoginStatusCallback_In >>>>>{}",CodeUtil.toJson(in));
		try {
			out  = new LoginStatusCallback_Out();
			idgate_register  = idgate_register_tmpdao.findById(in.getSessionID());
			log.debug("idgate_register >>>>>{}",CodeUtil.toJson(idgate_register));
			if(idgate_register == null) {
				out.setReturnCode("0001");
				out.setReturnMsg("登入失敗,SessionID "+in.getSessionID()+"查無資料");
				return out;
			}
			DateTimeFormatter format = DateTimeFormat.forPattern("yyyyMMddHHmmss");
			DateTime timeLast = format.parseDateTime(idgate_register.getLASTDATE()+idgate_register.getLASTTIME());
			DateTime timeNow = new DateTime();
			if(Float.valueOf(DateUtils.getDiffTimeMills(timeLast, timeNow)) <= quickLoginLimit) {
				dataList = quickloginmappingdao.getDataByCusidn(idgate_register.getCUSIDN());
				log.debug("quickloginmapping dataList >>>>>{}",CodeUtil.toJson(dataList));
				if(dataList != null && dataList.size() > 1) {
					out.setReturnCode("0002");
					out.setReturnMsg("註冊失敗,請先行註銷前面資料");
					return out;
				}else if(dataList != null && dataList.size() == 1){
					QUICKLOGINMAPPING qlpo = dataList.get(0);
					if (in.getIdgateID().equals(qlpo.getPks().getIDGATEID())) {
						qlpo.setDEVICEBRAND(idgate_register.getDEVICEBRAND());
						qlpo.setDEVICEID(idgate_register.getDEVICEID());
						qlpo.setDEVICENAME(idgate_register.getDEVICENAME());
						qlpo.setDEVICEOS(idgate_register.getDEVICEOS());
						qlpo.setDEVICETYPE(idgate_register.getDEVICETYPE());
						qlpo.setLASTDATE(idgate_register.getLASTDATE());
						qlpo.setLASTTIME(idgate_register.getLASTTIME());
						if (idgate_register.getOPNCODE()!=null&&idgate_register.getOPNCODE().length()>0) {
							qlpo.setISOPNCODE("Y");
						}
						if ("01".equals(idgate_register.getITEM())) {
							qlpo.setQCKLGIN("Y");
						}
						if("02".equals(idgate_register.getITEM())){
							qlpo.setQCKTRAN("Y");
						}
						qlpo.setQUICK_CALLBACK_TIME(String.valueOf(System.currentTimeMillis() + quickLoginLimit));
						qlpo.setQUICKID(in.getSessionID());
						log.trace(ESAPIUtil.vaildLog("qlpo is >> " + CodeUtil.toJson(qlpo)));
						quickloginmappingdao.save(qlpo);
						out.setReturnCode("0000");
						out.setReturnMsg("登入成功");
					}else {
						//代確認錯誤代碼及訊息
						out.setReturnCode("0002");
						out.setReturnMsg("登入失敗,請先行註銷前一筆");
						return out;
					}
				}else {
					pks = new QUICKLOGINMAPPING_PK();
					po = new QUICKLOGINMAPPING();
					pks.setCUSIDN(idgate_register.getCUSIDN());
					pks.setIDGATEID(in.getIdgateID());
					po.setPks(pks);
					po.setDEVICEBRAND(idgate_register.getDEVICEBRAND());
					po.setDEVICEID(idgate_register.getDEVICEID());
					po.setDEVICENAME(idgate_register.getDEVICENAME());
					po.setDEVICEOS(idgate_register.getDEVICEOS());
					po.setDEVICETYPE(idgate_register.getDEVICETYPE());
					po.setLASTDATE(idgate_register.getLASTDATE());
					po.setLASTTIME(idgate_register.getLASTTIME());
					if (idgate_register.getOPNCODE()!=null&&idgate_register.getOPNCODE().length()>0) {
						po.setISOPNCODE("Y");
					}
					if ("01".equals(idgate_register.getITEM())) {
						po.setQCKLGIN("Y");
					}
					if("02".equals(idgate_register.getITEM())){
						po.setQCKTRAN("Y");
					}
					po.setQUICK_CALLBACK_TIME(String.valueOf(System.currentTimeMillis() + quickLoginLimit));
					po.setQUICKID(in.getSessionID());
					log.trace(ESAPIUtil.vaildLog("po is >> " + CodeUtil.toJson(po)));
					quickloginmappingdao.save(po);
					
					out.setReturnCode("0000");
					out.setReturnMsg("登入成功");
				}
			}else {
				out.setReturnCode("0157");
				out.setReturnMsg("登入失敗,時間逾時");
				return out;
			}
			
		} catch (Exception e) {
			log.error("{}",e);
			out.setReturnCode("0002");
			out.setReturnMsg("登入失敗,系統異常");
		}
		return out;
	}
	
	
	/**
	 * 由idgate Serer 主動呼叫
	 * 告知銀行端新註冊的使用者的idgateID
	 * @param in
	 * @return
	 */
	public RegisterCallback_Out registerCallback(RegisterCallback_In in) {
		String json = null;
		RegisterCallback_Out out = null ;
		IDGATE_REGISTER_TMP idgate_register = null;
		QUICKLOGINMAPPING_PK pks = null;
		QUICKLOGINMAPPING po = null;
		List<QUICKLOGINMAPPING> dataList = null; 
//		RegisterCallback_In.RegType rt = null;
		log.debug("RegisterCallback_In >>>>>{}",CodeUtil.toJson(in));
		try {
			out  = new RegisterCallback_Out();
			idgate_register  = idgate_register_tmpdao.findById(in.getSessionID());
			log.debug("idgate_register >>>>>{}",CodeUtil.toJson(idgate_register));
			if(idgate_register == null) {
				out.setReturnCode("0001");
				out.setReturnMsg("註冊失敗,SessionID "+in.getSessionID()+"查無資料");
				return out;
			}
			dataList = quickloginmappingdao.getDataByCusidn(idgate_register.getCUSIDN());
			log.debug("quickloginmapping dataList >>>>>{}",CodeUtil.toJson(dataList));
			if(dataList != null && dataList.size() > 1) {
				out.setReturnCode("0002");
				out.setReturnMsg("註冊失敗,請先行註銷前一筆");
				return out;
			}else if(dataList != null && dataList.size() == 1){
				QUICKLOGINMAPPING qlpo = dataList.get(0);
				if (in.getIdgateID().equals(qlpo.getPks().getIDGATEID())) {
					qlpo.setDEVICEBRAND(idgate_register.getDEVICEBRAND());
					qlpo.setDEVICEID(idgate_register.getDEVICEID());
					qlpo.setDEVICENAME(idgate_register.getDEVICENAME());
					qlpo.setDEVICEOS(idgate_register.getDEVICEOS());
					qlpo.setDEVICETYPE(idgate_register.getDEVICETYPE());
					qlpo.setLASTDATE(idgate_register.getLASTDATE());
					qlpo.setLASTTIME(idgate_register.getLASTTIME());
					
					if (idgate_register.getOPNCODE()!=null&&idgate_register.getOPNCODE().length()>0) {
						qlpo.setISOPNCODE("Y");
					}
					if ("01".equals(idgate_register.getITEM())) {
						qlpo.setQCKLGIN("Y");
					}
					if("02".equals(idgate_register.getITEM())){
						qlpo.setQCKTRAN("Y");
					}
					log.trace(ESAPIUtil.vaildLog("qlpo is >> " + qlpo));
					quickloginmappingdao.save(qlpo);
					out.setReturnCode("0000");
					out.setReturnMsg("註冊成功");
				}else {
					//代確認錯誤代碼及訊息
					out.setReturnCode("0002");
					out.setReturnMsg("註冊失敗,請先行註銷前一筆");
					return out;
				}
			}else {
				pks = new QUICKLOGINMAPPING_PK();
				po = new QUICKLOGINMAPPING();
//				TODO idgate_register 還缺CUSIDN 先寫死 測試用;
				pks.setCUSIDN(idgate_register.getCUSIDN());
				pks.setIDGATEID(in.getIdgateID());
				po.setPks(pks);
				po.setDEVICEID(idgate_register.getDEVICEID());
				po.setDEVICENAME(idgate_register.getDEVICENAME());
				po.setDEVICETYPE(idgate_register.getDEVICETYPE());
				po.setDEVICEBRAND(idgate_register.getDEVICEBRAND());
				po.setDEVICEOS(idgate_register.getDEVICEOS());
//				TODO 這邊的驗證類型還要再確定
				Map<String,String> tmp = CodeUtil.objectCovert(Map.class, in.getRegType()) ;
				po.setVERIFYTYPE(getVerifyType(tmp));
				po.setDEVICESTATUS("0");
				po.setLASTDATE(DateUtil.getDate(""));
				po.setLASTTIME(DateUtil.getTheTime(""));
				po.setREGDATE(DateUtil.getDate(""));
				po.setREGTIME(DateUtil.getTheTime(""));
				if (idgate_register.getOPNCODE()!=null&&idgate_register.getOPNCODE().length()>0) {
					po.setISOPNCODE("Y");
				}
				if ("01".equals(idgate_register.getITEM())) {
					po.setQCKLGIN("Y");
				}
				if("02".equals(idgate_register.getITEM())){
					po.setQCKTRAN("Y");
				}
				//暫時先放空字串
				if(dataList == null || dataList.size() <= 0) {
					po.setISDEFAULT("Y");
				}else {
					po.setISDEFAULT("");
				}
				po.setERRORCNT("");
				po.setUPDATEUSER("SYS");
				
				log.trace(ESAPIUtil.vaildLog("po is >> " + po));
				quickloginmappingdao.save(po);
				
				out.setReturnCode("0000");
				out.setReturnMsg("註冊成功");
			}
		} catch (Exception e) {
			log.error("{}",e);
			out.setReturnCode("0002");
			out.setReturnMsg("註冊失敗,系統異常");
		}
		return out;
		
	}
	
	public String getVerifyType(Map<String,String> map) {
		String verifyType = "0";
		log.trace(ESAPIUtil.vaildLog("VerifyType is >> " + map));
		for(String key: map.keySet()) {
			String v = map.get(key);
			if("1".equals(v)) {
				switch (key) {
				case "bio":
					verifyType = "1";
					break;
				case "diagram":
					verifyType = "2";
					break;
				case "pwd":
					verifyType = "3";
					break;
				
				}
			}
		}//for end
		return verifyType;
	} 
	
//	public IDGateResult cocert2Result(String s) {
//		IDGateResult rs = new IDGateResult();
//		CodeUtil.o
//		
//		return null;
//		
//	}
	
	
	/**
	 * idgateID Txn驗證
	 * @param in
	 * @return
	 */
	public IDGateResult svGet_Txn_Status(SvGet_Txn_Status_In in) {
		String json = null;
		HashMap<String,String> params = null;
		SvGet_Txn_Status_Out out = null ;
		IDGateResult rs = null;
		try {
			rs = new IDGateResult();
			
			in.setChannel(idGateChanel);
			in.setMethod("svGet_Txn_Status");
			params = CodeUtil.objectCovert(HashMap.class, in);
			json = restutil.send2IdGate(params, idGatePath,idGateTimeOut ,HttpMethod.POST);
			
			if(StrUtils.isNotEmpty(json)) {
				out = CodeUtil.fromJson(json, SvGet_Txn_Status_Out.class);
				if("0000".equals(out.getReturnCode()) ) {
					rs.setResult(Boolean.TRUE);
					rs.setMsgCode("0");
					rs.setMsgName(out.getReturnMsg());
					rs.setData(out);
				}else {
					rs.setMsgCode(out.getReturnCode().toString());
					rs.setMsgName(out.getReturnMsg());
					rs.setData(out);
				}
			}else {
				log.warn("json is null");
			}
			
		} catch (Exception e) {
			log.error("{}",e);
			rs.setMsgCode("FE0010");
			rs.setResult(Boolean.FALSE);
		}
		return rs;
		
	}
	
	/**
	 * idgateID Txn取消
	 * @param in
	 * @return
	 */
	public IDGateResult svCancel_Txn(SvCancel_Txn_In in) {
		String json = null;
		HashMap<String,String> params = null;
		SvCancel_Txn_Out out = null ;
		IDGateResult rs = null;
		try {
			rs = new IDGateResult();
			
			in.setChannel(idGateChanel);
			in.setMethod("svCancel_Txn");
			params = CodeUtil.objectCovert(HashMap.class, in);
			json = restutil.send2IdGate(params, idGatePath,idGateTimeOut ,HttpMethod.POST);
			
			if(StrUtils.isNotEmpty(json)) {
				out = CodeUtil.fromJson(json, SvCancel_Txn_Out.class);
				if("0000".equals(out.getReturnCode()) ) {
					rs.setResult(Boolean.TRUE);
					rs.setMsgCode("0");
					rs.setMsgName(out.getReturnMsg());
					rs.setData(out);
				}else {
					rs.setMsgCode(out.getReturnCode().toString());
					rs.setMsgName(out.getReturnMsg());
					rs.setData(out);
				}
			}else {
				log.warn("json is null");
			}
			
		} catch (Exception e) {
			log.error("{}",e);
			rs.setMsgCode("FE0010");
			rs.setResult(Boolean.FALSE);
		}
		return rs;
		
	}
	
	public Map<String,Object> map2IdgateObject(Map<String,String> map){
		Map<String,Object> idGateObject = new HashMap<String,Object>();
		List<Map<String,String>> txnDetail = new ArrayList<Map<String,String>>();
		Map<String,String> txnDetailMap = new HashMap<String,String>();
		int i = 1;
		for (Map.Entry<String,String> entry : map.entrySet()) {
			txnDetailMap.put("Item" + i, entry.getKey());
			txnDetailMap.put("Value" + i, entry.getValue());
			i++;
		}
		txnDetail.add(txnDetailMap);
		idGateObject.put("TxnDetail", txnDetail);
		idGateObject.put("Timestamp", String.valueOf(System.currentTimeMillis()));
		return idGateObject;
	}
	
	/**
	 * idgateID Txn生成
	 * @param in
	 * @return
	 */
	public IDGateResult svCreate_Verify_Txn(SvCreate_Verify_Txn_In in) {
		String json = null;
		HashMap<String,String> params = null;
		SvCreate_Verify_Txn_Out out = null ;
		IDGateResult rs = null;
		String sessionid = null;
		String returnJson = null;
		try {
			rs = new IDGateResult();
			
			in.setChannel(idGateChanel);
			in.setMethod("svCreate_Verify_Txn");
			params = CodeUtil.objectCovert(HashMap.class, in);
			//txnData轉json字串與重設交易時間戳
			returnJson = CodeUtil.toJson(in.getTxnDataView());
			params.put("txnData", returnJson);
			params.remove("txnDataView");
			params.remove("adopid");
			
			json = restutil.send2IdGate(params, idGatePath,idGateTimeOut ,HttpMethod.POST);
			
			if(StrUtils.isNotEmpty(json)) {
				out = CodeUtil.fromJson(json, SvCreate_Verify_Txn_Out.class);
				if("0000".equals(out.getReturnCode()) ) {
					//存入DB紀錄 session id 與 txn id
					IDGATE_TXN_STATUS po = new IDGATE_TXN_STATUS();
					IDGATE_TXN_STATUS_PK pks = new IDGATE_TXN_STATUS_PK();
					sessionid = UUID.randomUUID().toString();
					pks.setIDGATEID(in.getIdgateID());
					pks.setSESSIONID(sessionid);
					pks.setTXNID(out.getTxnID());
					po.setPks(pks);
					po.setTXNCONTENT(CodeUtil.toJson(in.getTxnData()));
					po.setLASTDATE(DateUtil.getDate(""));
					po.setLASTTIME(DateUtil.getTheTime(""));
					po.setADOPID(in.getAdopid());
					po.setENCTXNID(out.getEnTxnID());
					idgate_txn_statusdao.save(po);
					
					out.setSessionid(sessionid);
					out.setReturnJson(returnJson);
					rs.setResult(Boolean.TRUE);
					rs.setMsgCode("0");
					rs.setMsgName(out.getReturnMsg());
					rs.setData(out);
				}else {
					rs.setMsgCode(out.getReturnCode().toString());
					rs.setMsgName(out.getReturnMsg());
					rs.setData(out);
				}
			}else {
				log.warn("json is null");
			}
			
		} catch (Exception e) {
			log.error("{}",e);
			rs.setMsgCode("FE0010");
			rs.setResult(Boolean.FALSE);
		}
		return rs;
		
	}
	
	/**
	 * 根據idgateid 和 txnid 取得sessionid
	 * @param in
	 * @return
	 */
	public IDGateResult getSessionId(GetSessionId_In in) {
		GetSessionId_Out out = null ;
		IDGateResult rs = null;
		String sessionid = null;
		log.debug("GetSessionId_In >>>>>> {}",CodeUtil.toJson(in));
		try {
			if (in.getTxnID()!=null&&in.getTxnID().length()>0) {
				sessionid = idgate_txn_statusdao.getSessionIdByTxnid(in.getIdgateID(), in.getTxnID());
			}else {
				sessionid = idgate_txn_statusdao.getSessionIdByEnctxnid(in.getIdgateID(), in.getEnctxnID());
			}
			log.debug("sessionid >>>>>> {}",sessionid);
			rs = new IDGateResult();
			out = new GetSessionId_Out();
//			TODO 如果查不到應該返回哪種錯誤代碼? ENRD?
			if(StrUtils.isEmpty(sessionid)) {
				out.setSessionid("");
				rs.setMsgCode("ENRD");
				rs.setResult(Boolean.FALSE);
				rs.setMessages("查無資料");
				rs.setMsgName("查無資料");
			}else {
				out.setSessionid(sessionid);
				rs.setMsgCode("0");
				rs.setMsgName("已查詢sessionid");
				rs.setResult(Boolean.TRUE);
			}
			rs.setData(out);
		} catch (Exception e) {
			log.error("{}",e);
//			TODO 錯誤代碼是否要重新定義?
			rs.setMsgCode("FE0010");
			rs.setResult(Boolean.FALSE);
		}
		return rs;
		
	}
}
