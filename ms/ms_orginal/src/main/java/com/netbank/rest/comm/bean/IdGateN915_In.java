package com.netbank.rest.comm.bean;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
//@Data
public class IdGateN915_In {

	@JsonProperty @NotNull
	String CUSIDN;
	@JsonProperty @NotNull
	String OPTION;
//	@JsonProperty
//	String TYPE;
	@JsonProperty
	String LOGINTYPE;
	@JsonProperty @NotNull
	String FGTXWAY;
	@JsonProperty
	String signature;
	@JsonProperty
	String CHIP_ACN;
	@JsonProperty
	String ICSEQ;
	@JsonProperty
	String BNKRA;
}
