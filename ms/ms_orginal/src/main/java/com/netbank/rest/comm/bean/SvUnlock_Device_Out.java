package com.netbank.rest.comm.bean;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SvUnlock_Device_Out {

	String returnCode;
	String returnMsg;
//	0:正常1:鎖定中2:驗證失敗過多而鎖定3:尚未完成註冊9:帳號已註銷
	String memberStatus;
	
}
