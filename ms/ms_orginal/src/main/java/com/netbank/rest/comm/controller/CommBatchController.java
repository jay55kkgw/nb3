package com.netbank.rest.comm.controller;

import java.sql.SQLInvalidAuthorizationSpecException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.netbank.rest.comm.batch.FindFileChange;
import com.netbank.rest.e2e.E2EClient;
import com.netbank.util.ESAPIUtil;

import fstop.orm.dao.AdmNbStatusDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.oao.Old_TxnUserDao;
import fstop.orm.po.SYSPARAM;
import fstop.util.JSONUtils;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(value = "/batch/com")
@Slf4j
public class CommBatchController {

	@Autowired
	FindFileChange findfilechange;

	@Autowired
	private ServletContext context;
	@Autowired
	private Old_TxnUserDao olddb;
	@Autowired
	private TxnUserDao newdb;

    @Autowired
    private AdmNbStatusDao admNbStatusDao;

	@RequestMapping(value = "/findfilechange", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public HashMap<String, Object> doAction(@RequestBody List<String> args) throws JsonProcessingException {

		log.info(ESAPIUtil.vaildLog(" args: {}"+ JSONUtils.toJson(args) ));
		log.info(" ContextPath: {}",context.getContextPath() );
		return findfilechange.findChange(context.getContextPath());

	}
	@RequestMapping(value = "/echo", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public HashMap<String, Object> echo(@RequestBody List<String> args) throws JsonProcessingException {
		
		log.info(ESAPIUtil.vaildLog(" args: {}"+ JSONUtils.toJson(args) ));
		HashMap<String, Object> retMap = new HashMap<String, Object>();
		retMap.put("isSuccess", Boolean.TRUE);
		retMap.put("batchName", "echo");
		
		return retMap;
		
	}
	@RequestMapping(value = "/daotest", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public HashMap<String, Object> daotest(@ModelAttribute @Valid SYSPARAM admCountry, BindingResult result) throws JsonProcessingException {
		
		log.info(ESAPIUtil.vaildLog(" args: {}"+ JSONUtils.toJson(admCountry) ));
		HashMap<String, Object> retMap = new HashMap<String, Object>();
		if ( result.hasErrors() ) {
            Map<String, String> errors = result.getFieldErrors().stream()
                .collect(
                    Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage)
                );
            retMap.put("isSuccess", Boolean.FALSE);
            retMap.put("batchName", "daotest");
            retMap.put("data", errors);
        } else {
            retMap.put("isSuccess", Boolean.TRUE);
            retMap.put("batchName", "daotest");
        }
		
		
		return retMap;
		
	}
	@RequestMapping(value = "/e2etest", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public HashMap<String, Object> daotest(@RequestBody List<String> args, BindingResult result) throws JsonProcessingException {
		List<String> safeArgs = ESAPIUtil.validStrList(args);
		HashMap<String, Object> retMap = new HashMap<String, Object>();
		try {
			retMap.put("isSuccess", Boolean.TRUE);
            retMap.put("batchName", "e2etest");
			String publicStr = "";
//			String rsaData = "IsqNO14eDjLbHXZONmRqxqe9gwN/20cfHLhBoWFPGMH6Uo6lAHfEaoWjd/a35/aItbvgh3yxT8TzjepuWiTtudP82vuspSJKOyUSzqnaEdxf2gha/gtKxFoesBPz180oBJyy7VBQFPoUc9Wy4u4PY6xvotO8QhCdfeeXdezA5bOD+hTYOhJWja6yva7CmtCmpB+8K3YNKSjwNmkAYnMUTxoTbygI93sWH5cqKC6PxxjnppzNb2u4jqj1JrXYjUBUj2xYQDBYD9/6Txzsc6kVmoj/mAfRQt8LEXhmf9wIGXBH4p/KE22YVsToBBoDfgViG6SwsKic/0usd+AzReM7og==";
			String rsaData = "hrDJpO/vmp4uAinvHuys4JrB0lD66EP7oRaIXvRDRWgB/tkGy6CF/bx+fcAGKzJ82jZK3Xv9P8KcvNID1OINgK3kX441VcxldILzytz0q2wbMGTxYgJbdIzy7sx4oShQ7sZaF+Fs4TtgGhQ/tN+K1GE3T1yNGS9Fk9iBFXT6ix6EPcuM44pAwRYaNsF7fotQH2vap20RtJR5HO7YTPKLBtRMWATFekz9annPbdHnyLoR2CbjjbBCUzMf5c+vyW7APfr1eZUbseCkJETCOkdqiBMo5gyP3zELSwxozecXb69ezFiBnIphZaIltXjKjhJZCnTkVrbGoNp+CiJADvoGMA==";
//			E2EClient.getToken();
            E2EClient.login();
            publicStr =E2EClient.getPublicKey();
            E2EClient.getPPSYNC();
            if(!safeArgs.isEmpty()) {
            	rsaData = safeArgs.get(0);
            }
            log.info(ESAPIUtil.vaildLog("rsaData>> "+rsaData));
			E2EClient.getPINNEW(rsaData);
			
		} catch (Exception e) {
			log.error("{}",e);
		}
		
		return retMap;
		
	}
	@RequestMapping(value = "/checkNNB_DB", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public HashMap<String, Object> checkNNB_DB(@RequestBody List<String> args) throws JsonProcessingException {
		
		log.info(ESAPIUtil.vaildLog(" args: {}"+ JSONUtils.toJson(args) ));
		HashMap<String, Object> retMap = new HashMap<String, Object>();
		try {
			Long Statusflag=olddb.checkStatus();
			retMap.put("isSuccess", Boolean.TRUE);
			retMap.put("batchName", "checkNNB_DB");
			retMap.put("data", Statusflag);
		}catch(  DataAccessException   e) {
			//針對jdbc發生連線授權失效的錯誤catch
			//spring 將所有DB連線錯誤放在DataAccessException中為了使不同DB能回吐相同的抽象錯誤訊息
			retMap.put("isSuccess", Boolean.FALSE);
			retMap.put("batchName", "checkNNB_DB");
			Map<String,Object>errorame=new HashMap<String, Object>();
			errorame.put("data",e.getRootCause());
			retMap.put("data",((SQLInvalidAuthorizationSpecException)errorame.get("data")).getLocalizedMessage());
		}
		catch(Exception e) {
			log.error("{}",e);
			retMap.put("isSuccess", Boolean.FALSE);
			retMap.put("batchName", "checkNNB_DB");
			retMap.put("data", e.getMessage());
		}
		return retMap;
		
	}
	@RequestMapping(value = "/checkNB3_DB", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public HashMap<String, Object> checkNB3_DB(@RequestBody List<String> args) throws JsonProcessingException {
		log.info(ESAPIUtil.vaildLog(" args: {}"+ JSONUtils.toJson(args) ));
		HashMap<String, Object> retMap = new HashMap<String, Object>();
		try {
			Long Statusflag=newdb.checkStatus();
			retMap.put("isSuccess", Boolean.TRUE);
			retMap.put("batchName", "checkNB3_DB");
			retMap.put("data", Statusflag);
		}catch(  DataAccessException   e) {
			retMap.put("isSuccess", Boolean.FALSE);
			retMap.put("batchName", "checkNB3_DB");
			Map<String,Object>errorame=new HashMap<String, Object>();
			errorame.put("data",e.getRootCause());
			retMap.put("data",((SQLInvalidAuthorizationSpecException)errorame.get("data")).getLocalizedMessage());
		}catch(Exception e) {
			retMap.put("isSuccess", Boolean.FALSE);
			retMap.put("batchName", "checkNB3_DB");
			retMap.put("data", e.getMessage());
		}
		return retMap;
		
	}
	
    @PostConstruct
    public void init() {
		try {
	        log.info("ContextPath >> {}",context.getContextPath());
	        admNbStatusDao.setServerStatus(context.getContextPath().replace("/", "").replace(" ", "").toUpperCase().split("_")[1]);
		}catch(Exception e) {
			log.error("error >> {}",e);
		}
    }
}
