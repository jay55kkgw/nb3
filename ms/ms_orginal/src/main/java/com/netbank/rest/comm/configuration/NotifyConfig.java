package com.netbank.rest.comm.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.netbank.rest.comm.batch.FirebaseBean;
import com.netbank.rest.comm.batch.NotifyValueBean;

import fstop.orm.dao.NFLogDao;
import lombok.extern.slf4j.Slf4j;


@Configuration
@Slf4j
public class NotifyConfig {
	
	@Bean
	protected NotifyValueBean notifyValueBean() throws Exception{
		return new  NotifyValueBean();
	}
	@Bean
	protected NFLogDao nFLogDao() throws Exception{
		return new  NFLogDao();
	}

	@Bean
	protected FirebaseBean firebaseBean() throws Exception{
		return new  FirebaseBean();
	}
}
