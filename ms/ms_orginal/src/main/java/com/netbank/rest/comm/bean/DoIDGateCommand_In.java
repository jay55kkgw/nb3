package com.netbank.rest.comm.bean;

import java.util.Map;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DoIDGateCommand_In {
	
	@JsonProperty @NotNull
	Map IN_DATAS;

}
