package com.netbank.rest.comm.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SvDisable_Mob_Auth_Out {

	String returnCode;
	String returnMsg;
}
