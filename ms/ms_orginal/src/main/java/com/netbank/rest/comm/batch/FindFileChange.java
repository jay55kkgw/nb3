package com.netbank.rest.comm.batch;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.netbank.util.CodeUtil;

import fstop.notifier.Notifier;
import fstop.orm.dao.SysParamDataDao;
import fstop.orm.po.SYSPARAMDATA;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class FindFileChange {

	@Autowired
	SysParamDataDao sysParamDataDao;
	
	DateTime initdate ;
	
	@Value("${scanPath:/opt/ibm/wlp/usr/servers/defaultServer/apps/expanded/}")
	String scanPath ="";
	@Autowired
	private Notifier notifier;
	
	
	/**
	 * 初始化基本日期時間，來當作基準
	 */
	@PostConstruct
	public void init() {
		initdate = new DateTime();
		log.info("initdate>>{}",initdate.toString("yyyy-MM-dd HH:mm:ss"));
	}
	  
	public HashMap<String,Object> findChange(String contextPath) {
//		String nb3path = "D:\\PROJ\\ox-wk-dev\\ms_batch_main\\src\\main\\java\\fstop\\batch\\main\\FindFileChange.java";
//		DateTime date = new DateTime();
//		DateTime before1H = date.minusHours(1);
//		File file = new File(nb3path);
//		DateTime filedate = new DateTime(file.lastModified());
//		log.info("filedate>>{}", filedate.toString("yyyy-MM-dd HH:mm:ss"));
//		isRange(filedate, 1);
		
		contextPath = contextPath.replace("/", "").trim().toUpperCase();
		
//		was 路徑
//		/opt/IBM/WebSphere/AppServer/profiles/AppSrv01/installedApps/DefaultCell01/nnb-ear.ear
//		liberty 路徑
//		/opt/ibm/wlp/usr/servers/defaultServer/apps/expanded/
//		scanPath = "D:\\PROJ\\ox-wk-dev\\ms_batch_main\\src\\";
		List<String> list = new LinkedList<String>();
		listDir(new File(scanPath), list);
		
		if(list!=null && !list.isEmpty()) {
			sendMail(contextPath, list);
		}
		  
		HashMap<String,Object> map = new HashMap<String,Object>();
		map.put("result", Boolean.TRUE);
		return map;
	}
	   
	
	public void sendMail(String contextPath ,List<String> list) {
//		NotifyAngent.
		HashMap<String, Object> map;
		String subject = contextPath + "檔案異動";
		String content = "";
		String mails = "";
		
		List<String> mailList = new ArrayList<String>();
		try {
			if (list != null && !list.isEmpty()) {
				mails = getAPmail();
				log.info("mails>>{}",mails);
				mailList.add(mails);
				content = getMailContent(list, contextPath);
//				SendAdMail_REST(subject, content, mailList,"1");
				
				log.info("mails>>{}",mails);
				for(String mail :mailList) {
					notifier.sendmsg(mail, subject, content, null);
				}
			}
		} catch (Exception e) {
			log.error("{}", e.toString());
		}
	}
	
	
	public String getMailContent(List<String> list,String contextPath) {
		StringBuffer sb = new StringBuffer();
		sb.append("<html>\n<head>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF8\" />\n");
		sb.append("<title>新個網檔案異動通知</title>\n");
		sb.append("</head>\n");
		sb.append("<body>\n");
		sb.append("主機名稱:"+ contextPath+"<BR>");
		sb.append("檔案異動清單:"+ CodeUtil.toJson(list));
		sb.append("</body>\n");
		sb.append("</html>\n");
		log.info("sb>>{}",sb.toString());
		
		return sb.toString();
	}
	
	
	public Boolean isRange(DateTime filedate, Integer range) {
		Boolean result = Boolean.FALSE;
//		DateTime date = new DateTime();
//		date = date.minusHours(range);
		log.debug("base date time>>{}",initdate.toString("yyyy-MM-dd HH:mm:ss"));
		DateTime date = initdate.minusMinutes(range);
		log.info("filedate>>{}", filedate.toString("yyyy-MM-dd HH:mm:ss"));
		result = filedate.isAfter(date);
		log.info("isRange>>{}", result);
		return result;

	}
	
	
	/**
	 * https://www.itread01.com/content/1547205846.html
	 * @param file
	 */
	public void listDir(File file , List<String> list ) {
//		List<String> list = new LinkedList<String>();
		
		log.trace("file.getName()>>{}",file.getName());
		if (file.isDirectory()) { // 是一個目錄
			// 列出目錄中的全部內容
			File results[] = file.listFiles();
			if (results != null) {
				for (int i = 0; i < results.length; i++) {
					listDir(results[i] ,list); // 繼續一次判斷
				}
			}
		} else { // 是檔案
			
			DateTime filedate = new DateTime(file.lastModified());
			if(isRange(filedate, 1)) {
				list.add(file.getPath());
			}
		}
		// file.delete(); //刪除!!!!! 根目錄,慎操作
		// 獲取完整路徑
		log.info("list>>{}",list);
	}
	
	public String getAPmail() {
		SYSPARAMDATA po = sysParamDataDao.findById("NBSYS");
		return  po.getADAPMAIL();
	}
	
	
}
