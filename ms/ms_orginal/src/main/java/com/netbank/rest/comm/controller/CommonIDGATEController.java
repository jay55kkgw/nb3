package com.netbank.rest.comm.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.rest.comm.bean.DoIDGateCommand_In;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.telcomm.cmd.CommandUtils;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(value = "/IDGATE/com")
@Slf4j
public class CommonIDGATEController {
	
	
	@RequestMapping(value = "/DoIDGateCommand" )
	@ResponseBody
	public HashMap doIDGateCommand( @Valid @RequestBody DoIDGateCommand_In in) {
    	log.info("doIDGateCommand....");
    	log.info(ESAPIUtil.vaildLog("idGateBookingCheck>> " + CodeUtil.toJson(in)));
    	HashMap rtnMap = new HashMap();
    	try {
    		Map<String,String> params = in.getIN_DATAS();
    		log.debug("params datas >>>> " + params);
			List<String> commands = new ArrayList();
			commands.add("IDGateCommand()");
			CommandUtils.doBefore(commands, params);
			rtnMap.put("result", true);
		} catch (TopMessageException e) {
			rtnMap.put("result", false);
			rtnMap.put("errMsg", e.getMsgcode());
			log.error("doIDGateCommand TopMessageException >> {}" , e.getMsgcode());
		} catch (Exception e) {
			rtnMap.put("result", false);
			rtnMap.put("errMsg", e.getMessage());
			log.error("doIDGateCommand Exception >> {}" , e.getMessage());
		}
		return rtnMap;

    }
}
