package com.netbank.rest.og.aop;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.netbank.util.ESAPIUtil;
import com.netbank.util.fstop.DateUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Aspect
@Component
public class SysTimeDiffAop {
	
	@Autowired HttpServletRequest req; 
	
	
	@Around("within(com.netbank.rest.web.controller..*)")
	public Object timediff(ProceedingJoinPoint pjp) {
		log.info("SysTimeDiffAop.timediff...");
		Object obj = null;
		DateTime d1 = null;
		DateTime d2 = null;
		String pstimediff = "";
		String api = "";
		try {
			try {
				api = req.getRequestURI().replaceFirst(req.getContextPath(), "") ;
			} catch (Throwable e) {
				log.error("timediff.error.partA>>{}",e);
			}
//			api = (String) pjp.getArgs()[0];
			log.info("Before doAround.proceed..");
			d1 = new DateTime();
			obj = pjp.proceed();
			d2 = new DateTime();
			log.info("After doAround.proceed..");
			pstimediff = DateUtil.getDiffTimeMills(d1, d2);
			log.warn("API>>{} ,pstimediff>>{}",ESAPIUtil.vaildLog(api),ESAPIUtil.vaildLog(pstimediff));
		} catch (Throwable e) {
			log.error("SysTimeDiffAop.timediff{}",e);
		}
		log.info("SysTimeDiffAop.timediff end...");
		return obj;
		
	}
	
	
	
	

}
