package com.netbank.rest.e2e.bean;

import lombok.Data;

@Data
public class Encryption {

	private String symKey;
	private String symKeyMode;
	private String symKeyIv;
	private String prefix;
	private String postfix;
}
