package com.netbank.rest.comm.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IdGateN939_Out {

	String RSPCOD;
	String SMALLCNT;
	String QCKLGIN;
	String QCKTRAN;
	String SMALLPAY;
}
