package com.netbank.rest.comm.bean;

import java.util.Map;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SvCreate_Verify_Txn_In {

	@JsonProperty @NotNull
	String idgateID;
	@JsonProperty @NotNull
	String deviceID;
	@JsonProperty @NotNull
	String authType;
	@JsonProperty 
	String title;
	@JsonProperty 
	Map<String,String> txnData;
	@JsonProperty 
	Object txnDataView;
	@JsonProperty @NotNull
	String push;
	@JsonProperty 
	String channel;
	@JsonProperty 
	String method;

	@JsonProperty 
	String adopid;
	@JsonProperty
	String LOGINTYPE;
}
