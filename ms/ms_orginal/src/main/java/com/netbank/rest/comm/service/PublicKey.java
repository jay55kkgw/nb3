package com.netbank.rest.comm.service;

import java.util.HashMap;
import java.util.Map;

import com.netbank.rest.e2e.E2EClient;
import com.netbank.rest.e2e.MB3_E2EClient;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.services.CommonService;
import fstop.util.StrUtils;

public class PublicKey extends CommonService{
	@Override	
	public MVH doAction(Map params) {
		String publickey ="";
		MVHImpl mvhimpl = new MVHImpl();
//		List list =null;
		try {
			String login_type = (String)params.get("LOGINTYPE");
			if("MB".equals(login_type)) {
				if(MB3_E2EClient.login()) {
					publickey = MB3_E2EClient.getPublicKey();
				}
			}else {
				if(E2EClient.login()) {
					publickey = E2EClient.getPublicKey();
				}
			}
			if(StrUtils.isNotEmpty(publickey)) {
				mvhimpl.getFlatValues().put("publicKey", "-----BEGIN RSA PUBLIC KEY-----"+publickey+"-----END RSA PUBLIC KEY-----");
				mvhimpl.getFlatValues().put("TOPMSG", "0");
//				mvhimpl.getOccurs().addRow(new Row(mvhimpl.getFlatValues()));
			}else {
				mvhimpl.getFlatValues().put("publicKey", publickey);
				mvhimpl.getFlatValues().put("TOPMSG", "Z300");
			}
			
//			mvhimpl.getFlatValues().put("publicKey", "test");
//			mvhimpl.getFlatValues().put("TOPMSG", "0");
		} catch (Exception e) {
			throw TopMessageException.create("Z300");
		}
		
		return mvhimpl;
	}
	
	
}
