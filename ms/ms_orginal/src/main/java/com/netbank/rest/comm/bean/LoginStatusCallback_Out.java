package com.netbank.rest.comm.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginStatusCallback_Out {

	String returnCode;
	String returnMsg;
}
