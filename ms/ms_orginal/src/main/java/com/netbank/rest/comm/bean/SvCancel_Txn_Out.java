package com.netbank.rest.comm.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SvCancel_Txn_Out {

	String returnCode;
	String returnMsg;
}
