package com.netbank.rest.e2e.bean;

import lombok.Data;

@Data
public class EncryptionData {

	private String data;
	private String dataType;
	private String rsaKey;
	private String withTime;
}
