package com.netbank.rest.comm.bean;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegisterCallback_Out {

	String returnCode;
	String returnMsg;
	
}
