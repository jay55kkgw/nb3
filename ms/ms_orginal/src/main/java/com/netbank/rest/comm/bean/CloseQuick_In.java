package com.netbank.rest.comm.bean;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CloseQuick_In {

	@JsonProperty @NotNull
	String IDGATEID;
	@JsonProperty @NotNull
	String ITEM;
	@JsonProperty @NotNull
	String CUSIDN;
}
