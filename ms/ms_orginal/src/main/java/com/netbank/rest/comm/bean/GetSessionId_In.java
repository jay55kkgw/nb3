package com.netbank.rest.comm.bean;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GetSessionId_In {

	@JsonProperty @NotNull
	String idgateID;
	@JsonProperty
	String enctxnID;
	@JsonProperty
	String txnID;
	
	String LOGINTYPE;
}
