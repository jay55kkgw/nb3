package com.netbank.rest.comm.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SvCreate_Verify_Txn_Out {

	String returnCode;
	String returnMsg;
	String txnID;
	String enTxnID;
	String sessionid;
	String returnJson;
}
