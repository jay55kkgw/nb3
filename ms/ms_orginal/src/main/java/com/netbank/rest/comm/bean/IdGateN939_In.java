package com.netbank.rest.comm.bean;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IdGateN939_In {

	@JsonProperty
	String CUSIDN;
	@JsonProperty @NotNull
	String IDGATEID;
	@JsonProperty @NotNull
	String DEVICEID;
	@JsonProperty
	String LOGINTYPE;
}
