package com.netbank.rest.comm.bean;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IdGateN93A_In {

	@JsonProperty @NotNull
	String CUSIDN;
	@JsonProperty @NotNull
	String ITEM;
	@JsonProperty @NotNull
	String TYPE;
	@JsonProperty
	String LOGINTYPE;
}
