package com.netbank.rest.e2e.bean;

import lombok.Data;

@Data
public class GetPublicKey_RQ {

	private String cluster;
	private String keyName;
}
