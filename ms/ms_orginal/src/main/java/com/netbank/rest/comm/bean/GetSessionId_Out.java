package com.netbank.rest.comm.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GetSessionId_Out {

//	String returnCode;
//	String returnMsg;
	String sessionid;
}
