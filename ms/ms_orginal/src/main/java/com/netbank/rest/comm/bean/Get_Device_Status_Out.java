package com.netbank.rest.comm.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Get_Device_Status_Out {

	String returnCode;
	String returnMsg;
	String memberStatus;
	FailCount   failCount;
	Type   type;
	
	@Getter
	@Setter
	public class Type {
		String diagram ;
		String bio ;
		String pwd ;
	}
	@Getter
	@Setter
	public class FailCount {
		String digital ;
		String pattern ;
	}
	
	
}
