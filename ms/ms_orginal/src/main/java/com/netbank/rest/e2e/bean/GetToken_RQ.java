package com.netbank.rest.e2e.bean;

import lombok.Data;

@Data
public class GetToken_RQ {

	private String signature;
	private GetToken_RQ_Data data;
}
