package com.netbank.rest.comm.bean;

import com.netbank.rest.comm.bean.ChangeSettingCallback_In.RegType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TxnStatusCallback_Out {

	String returnCode;
	String returnMsg;
}
