package com.netbank.rest.comm.bean;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TxnStatusCallback_In {

	@NotNull
	String idgateID;
	
	String sessionID;
	@NotNull
	String txnID;
	@NotNull
	String txnStatus;
}
