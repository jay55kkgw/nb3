package com.netbank.rest.e2e.bean;

import java.util.List;

import lombok.Data;

@Data
public class ReturnPProcess {

	private List<Hash> hash;
	private Encryption encryption;
//	private Integer rtnEncoding;
}
