package com.netbank.rest.comm.bean;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IdGateMappingQuery_Out {

	@JsonProperty 
	Object Data;
	@JsonProperty 
	Object REC;
	
	
}
