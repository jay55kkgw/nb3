package com.netbank.rest.comm.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SvGet_Txn_Status_Out {

	String returnCode;
	String returnMsg;
	String txnID;
	String txnStatus;
	String type;
}
