package com.netbank.rest.e2e.bean;

import lombok.Data;

@Data
public class GetToken_RS_Data extends Base_RS {
	
	private String accessToken;
	private String refreshToken;

}
