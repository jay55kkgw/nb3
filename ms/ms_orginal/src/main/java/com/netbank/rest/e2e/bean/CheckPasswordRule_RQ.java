package com.netbank.rest.e2e.bean;

import java.util.List;

import lombok.Data;

@Data
public class CheckPasswordRule_RQ {

	private String cluster;
	private EncryptionData encryptionData;
	private List<List<PRule>> passRule;
	private ReturnPProcess returnPassProcess;
	private Boolean ignoreCheckTime;
	private String rtnEncoding;
}
