package com.netbank.rest.e2e.bean;

import java.util.List;

import lombok.Data;

@Data
public class PRule {

	private String ruleName;
	private String ruleParam;
	private List<String> specialSymbol;
}
