package com.netbank.rest.og.aop;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import com.netbank.util.CodeUtil;

import fstop.orm.oao.Old_TxnAddressBookDao;
import fstop.orm.oao.Old_TxnCusInvAttrHistDao;
import fstop.orm.oao.Old_TxnCusInvAttribDao;
import fstop.orm.oao.Old_TxnTrAccSetDao;
import fstop.orm.oao.Old_TxnUserDao;
import fstop.orm.old.OLD_TXNADDRESSBOOK;
import fstop.orm.old.OLD_TXNCUSINVATTRHIST;
import fstop.orm.old.OLD_TXNCUSINVATTRIB;
import fstop.orm.old.OLD_TXNTRACCSET;
import fstop.orm.old.OLD_TXNUSER;
import fstop.orm.po.TXNADDRESSBOOK;
import fstop.orm.po.TXNCUSINVATTRHIST;
import fstop.orm.po.TXNCUSINVATTRIB;
import fstop.orm.po.TXNTRACCSET;
import fstop.orm.po.TXNUSER;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 功能說明 :同步NB2.5 TXNUSER
 *
 */
@Slf4j
@Aspect
@Component
@ConditionalOnProperty(name = "isSyncNNB", matchIfMissing = true, havingValue = "Y")
public class SyncNNBAop {

	@Autowired
	HttpServletRequest req;

	@Autowired
	Old_TxnUserDao old_txnuserdao;
	@Autowired
	Old_TxnTrAccSetDao old_txntraccsetdao;
	@Autowired
	Old_TxnCusInvAttribDao old_txncusinvattribdao;
	@Autowired
	Old_TxnCusInvAttrHistDao old_txncusinvattrhistdao;
	@Autowired
	Old_TxnAddressBookDao old_txnaddressbookdao;

	// @Value("isSyncNNB")
	// String isSyncNNB;

	/**
	 * 同步NB2.5 TXNUSER
	 * 
	 * @param pjp
	 * @return || within(fstop.orm.dao.TxnUserDao.update (..)) ||
	 *         within(fstop.orm.dao.TxnUserDao.saveOrUpdate (..))
	 */
	// @Around("within(fstop.orm.dao.TxnUserDao) && execution(save(..)) ")
	// @Around("execution( fstop.orm.dao.TxnUserDao.save(..) )")
	// @Around("within(* fstop.orm.dao.TxnUserDao.*(..)) ")
	// @Around("execution(* fstop.orm.dao.TxnUserDao.*(..)) ")
	public Object syncTXNUSER(ProceedingJoinPoint pjp) {
		log.info("SysTimeDiffAop.timediff...");
		Object obj = null;
		DateTime d1 = null;
		DateTime d2 = null;
		String method = "";
		try {
			method = pjp.getSignature().getName();
		} catch (Exception e) {
			log.error("{}", e);
		}
		log.trace("method>>{}", method);
		try {
			log.info("Before doAround.proceed..");
			obj = pjp.getThis();
			log.debug("Before doAround.proceed.obj>>{}", obj.getClass().getSimpleName());
			obj = pjp.proceed();
			log.debug("after doAround.proceed.obj>>{}", obj.getClass().getSimpleName());

		} catch (Throwable e) {
			log.error("SysTimeDiffAop.timediff{}", e);
		}
		log.info("SysTimeDiffAop.timediff end...");
		return obj;

	}

	// @Around("execution(* com.netbank.domain.orm.core.LegacyJpaRepository.*(..))
	@Around("execution(* fstop.orm.dao.TxnUserDao.*(..)) ")
	public Object syncNNBTxnUser(ProceedingJoinPoint pjp) {
		log.info("syncNNBTxnUserDao...");
		// log.debug("isSyncNNB>>{}", isSyncNNB);
		Object obj = null;
		TXNUSER beforeObj = null;
		Object[] objarray = null;
		String method = "";
		Boolean isSync = Boolean.FALSE;
		OLD_TXNUSER po = null;
		Integer schcount = null;
		String schdate = "";
		try {
			method = pjp.getSignature().getName();
		} catch (Exception e) {
			log.error("{}", e);
		}
		log.trace("method>>{}", method);
		try {
			// beforeObj = pjp.getThis();
			try {
				objarray = pjp.getArgs();
				log.debug("objarray>>{}", objarray);
				if (("save".equals(method) || "update".equals(method)) && objarray != null && objarray.length > 0) {
					for (Object o : Arrays.asList(objarray)) {
						if (o instanceof TXNUSER) {
							isSync = Boolean.TRUE;
							beforeObj = (TXNUSER) o;
						}
					}
				}
			} catch (Throwable e) {
				log.error("syncNNBTxnUserDao.partA.ERROR>>{}", e);
			}

			log.info("Before doAround.proceed..");
			obj = pjp.proceed();

			try {
				if (isSync) {
					log.info("save-->sync");
					po = old_txnuserdao.findById(beforeObj.getDPSUERID());
					if (po == null) {
						po = CodeUtil.objectCovert(OLD_TXNUSER.class, beforeObj);
						log.debug("save.po>>{}", CodeUtil.toJson(po));
						old_txnuserdao.save(po);
					} else {
						schcount = po.getSCHCOUNT() == null ? 0 : po.getSCHCOUNT();
						schdate = po.getSCHCNTDATE();
						po = CodeUtil.objectCovert(OLD_TXNUSER.class, beforeObj);
						po.setSCHCOUNT(schcount);
						po.setSCHCNTDATE(schdate);
						log.debug("update.po>>{}", CodeUtil.toJson(po));
						// old_txnuserdao.save(po);
						old_txnuserdao.update(po);
					}
					// old_txnuserdao.saveOrUpdate(po);
				}
			} catch (Throwable e) {
				log.error("syncNNBTxnUserDao.partB.ERROR>>{}", e);
			}

		} catch (Throwable e) {
			log.error("syncNNBTxnUserDao.ERROR{}", e);
		}
		log.info("syncNNBTxnUserDao end...");
		return obj;

	}

	/**
	 * 常用帳號同步NB2.5
	 * 
	 * @param pjp
	 * @return
	 */
	@Around("execution(* fstop.orm.dao.TxnTrAccSetDao.*(..)) ")
	public Object syncNNBTxnTrAccSet(ProceedingJoinPoint pjp) {
		log.info("syncNNBTxnTrAccSet...");
		Object obj = null;
		TXNTRACCSET beforeObj = null;
		Object[] objarray = null;
		String method = "";
		Boolean isSync = Boolean.FALSE;
		OLD_TXNTRACCSET po = null;

		try {
			method = pjp.getSignature().getName();
		} catch (Exception e) {
			log.error("{}", e);
		}
		log.trace("method>>{}", method);
		try {
			// beforeObj = pjp.getThis();
			try {
				objarray = pjp.getArgs();
				log.debug("objarray>>{}", objarray);
				if (("save".equals(method) || "update".equals(method) || "delete".equals(method)) && objarray != null
						&& objarray.length > 0) {
					for (Object o : Arrays.asList(objarray)) {
						if (o instanceof TXNTRACCSET) {
							isSync = Boolean.TRUE;
							beforeObj = (TXNTRACCSET) o;
						}
					}
				}
			} catch (Throwable e) {
				log.error("syncNNBTxnTrAccSet.partA.ERROR{}", e);
			}

			log.info("Before doAround.proceed..");
			obj = pjp.proceed();

			try {
				if (isSync) {
					Integer tmp = null;
					log.info("save or update -->sync");
					po = old_txntraccsetdao.findByDPGONAMEII(beforeObj.getDPUSERID(), beforeObj.getDPTRIBANK(),
							beforeObj.getDPTRDACNO());
					if (po == null) {
						po = CodeUtil.objectCovert(OLD_TXNTRACCSET.class, beforeObj);
						log.debug("save.po>>{}", CodeUtil.toJson(po));
						old_txntraccsetdao.save(po);
					} else {
						tmp = po.getDPACCSETID();
						po = CodeUtil.objectCovert(OLD_TXNTRACCSET.class, beforeObj);
						po.setDPACCSETID(tmp);
						log.debug("update.po>>{}", CodeUtil.toJson(po));
						// old_txnuserdao.save(po);
						old_txntraccsetdao.update(po);
					}
					// old_txnuserdao.saveOrUpdate(po);
				}

				if (isSync && "delete".equals(method)) {
					log.info("delete-->sync");
					Integer id = old_txntraccsetdao.getDpaccsetid(beforeObj.getDPUSERID(), beforeObj.getDPTRIBANK(),
							beforeObj.getDPTRDACNO());
					OLD_TXNTRACCSET delpo = new OLD_TXNTRACCSET();
					delpo.setDPACCSETID(id);
					old_txntraccsetdao.delete(delpo);
				}
			} catch (Throwable e) {
				log.error("syncNNBTxnTrAccSet.partB.ERROR{}", e);
			}

		} catch (Throwable e) {
			log.error("syncNNBTxnTrAccSet.ERROR{}", e);
		}
		log.info("syncNNBTxnTrAccSet end...");
		return obj;

	}

	/**
	 * 同步基金屬性
	 * 
	 * @param pjp
	 * @return
	 */
	@Around("execution(* fstop.orm.dao.TxnCusInvAttribDao.*(..)) ")
	public Object syncNNBTxnCusInvAttrib(ProceedingJoinPoint pjp) {
		log.info("syncNNBTxnCusInvAttrib...");
		Object obj = null;
		TXNCUSINVATTRIB beforeObj = null;
		Object[] objarray = null;
		String method = "";
		Boolean isSync = Boolean.FALSE;
		OLD_TXNCUSINVATTRIB po = null;

		try {
			method = pjp.getSignature().getName();
		} catch (Exception e) {
			log.error("{}", e);
		}
		log.trace("method>>{}", method);
		try {
			// beforeObj = pjp.getThis();
			try {
				objarray = pjp.getArgs();
				log.debug("objarray>>{}", objarray);
				if (("save".equals(method) || "update".equals(method) || "delete".equals(method)
						|| "saveOrUpdate".equals(method)) && objarray != null && objarray.length > 0) {
					for (Object o : Arrays.asList(objarray)) {
						if (o instanceof TXNCUSINVATTRIB) {
							isSync = Boolean.TRUE;
							beforeObj = (TXNCUSINVATTRIB) o;
						}
					}
				}
			} catch (Throwable e) {
				log.error("syncNNBTxnCusInvAttrib.partA.ERROR{}", e);
			}

			log.info("Before doAround.proceed..");
			obj = pjp.proceed();

			try {
				if (isSync) {
					log.info("save or update -->sync");
					po = old_txncusinvattribdao.findById(beforeObj.getFDUSERID());
					if (po == null) {
						po = CodeUtil.objectCovert(OLD_TXNCUSINVATTRIB.class, beforeObj);
						log.debug("save.po>>{}", CodeUtil.toJson(po));
						old_txncusinvattribdao.save(po);
					} else {
						po = CodeUtil.objectCovert(OLD_TXNCUSINVATTRIB.class, beforeObj);
						log.debug("update.po>>{}", CodeUtil.toJson(po));
						old_txncusinvattribdao.update(po);
					}
				}
				if (isSync && "delete".equals(method)) {
					log.info("delete-->sync");
					po = old_txncusinvattribdao.findById(beforeObj.getFDUSERID());
					if (po != null) {
						OLD_TXNCUSINVATTRIB delpo = new OLD_TXNCUSINVATTRIB();
						delpo.setFDUSERID(beforeObj.getFDUSERID());
						old_txncusinvattribdao.delete(delpo);
					} else {
						log.warn("delete-->sync po is null delete not waork userid >>{}", beforeObj.getFDUSERID());
					}
				}
			} catch (Throwable e) {
				log.error("syncNNBTxnCusInvAttrib.partB.ERROR{}", e);
			}

		} catch (Throwable e) {
			log.error("syncNNBTxnCusInvAttrib.ERROR{}", e);
		}
		log.info("syncNNBTxnCusInvAttrib end...");
		return obj;

	}

	/**
	 * 同步基金屬性歷史紀錄檔
	 * 
	 * @param pjp
	 * @return
	 */
	@Around("execution(* fstop.orm.dao.TxnCusInvAttrHistDao.*(..)) ")
	public Object syncNNBTxnCusInvAttrHist(ProceedingJoinPoint pjp) {
		log.info("syncNNBTxnCusInvAttrHist...");
		// log.debug("isSyncNNB>>{}", isSyncNNB);
		Object obj = null;
		TXNCUSINVATTRHIST beforeObj = null;
		Object[] objarray = null;
		String method = "";
		Boolean isSync = Boolean.FALSE;
		OLD_TXNCUSINVATTRHIST po = null;

		try {
			method = pjp.getSignature().getName();
		} catch (Exception e) {
			log.error("{}", e);
		}
		log.trace("method>>{}", method);
		try {
			// beforeObj = pjp.getThis();
			try {
				objarray = pjp.getArgs();
				log.debug("objarray>>{}", objarray);
				if (("save".equals(method) || "update".equals(method) || "delete".equals(method)
						|| "saveOrUpdate".equals(method)) && objarray != null && objarray.length > 0) {
					for (Object o : Arrays.asList(objarray)) {
						if (o instanceof TXNCUSINVATTRHIST) {
							isSync = Boolean.TRUE;
							beforeObj = (TXNCUSINVATTRHIST) o;
						}
					}
				}
			} catch (Throwable e) {
				log.error("syncNNBTxnCusInvAttrHist.partA.ERROR{}", e);
			}

			log.info("Before doAround.proceed..");
			obj = pjp.proceed();

			try {
				if (isSync) {
					// if (isSync && "Y".equals(isSyncNNB)) {
					log.info("save or update -->sync");
					po = CodeUtil.objectCovert(OLD_TXNCUSINVATTRHIST.class, beforeObj);
					po.setFDHISTID(null);
					log.debug("save.po>>{}", CodeUtil.toJson(po));
					old_txncusinvattrhistdao.save(po);
				}
			} catch (Throwable e) {
				log.error("syncNNBTxnCusInvAttrHist.partB.ERROR{}", e);
			}

		} catch (Throwable e) {
			log.error("syncNNBTxnCusInvAttrHist.ERROR{}", e);
		}
		log.info("syncNNBTxnCusInvAttrHist end...");
		return obj;

	}

	/**
	 * 這段暫時不使用
	 * 
	 * @param pjp
	 * @return
	 */
	// @Around("execution(* fstop.orm.dao.TxnAddressBookDao.*(..)) ")
	public Object syncNNBTxnAddressBook(ProceedingJoinPoint pjp) {
		log.info("syncNNBTxnAddressBook...");
		// log.debug("isSyncNNB>>{}", isSyncNNB);
		Object obj = null;
		TXNADDRESSBOOK beforeObj = null;
		Object[] objarray = null;
		String method = "";
		Boolean isSync = Boolean.FALSE;
		OLD_TXNADDRESSBOOK po = null;

		try {
			method = pjp.getSignature().getName();
		} catch (Exception e) {
			log.error("{}", e);
		}
		log.trace("method>>{}", method);
		try {
			// beforeObj = pjp.getThis();
			try {
				objarray = pjp.getArgs();
				log.debug("objarray>>{}", objarray);
				if (("save".equals(method) || "update".equals(method) || "delete".equals(method)
						|| "saveOrUpdate".equals(method)) && objarray != null && objarray.length > 0) {
					for (Object o : Arrays.asList(objarray)) {
						if (o instanceof TXNADDRESSBOOK) {
							isSync = Boolean.TRUE;
							beforeObj = (TXNADDRESSBOOK) o;
						}
					}
				}
			} catch (Throwable e) {
				log.error("syncNNBTxnAddressBook.partA.ERROR{}", e);
			}

			log.info("Before doAround.proceed..");
			obj = pjp.proceed();

			try {
				if (isSync) {
					log.info("save or update -->sync");
					po = old_txnaddressbookdao.findById(beforeObj.getDPADDBKID());
					if (po == null) {
						po = CodeUtil.objectCovert(OLD_TXNADDRESSBOOK.class, beforeObj);
						log.debug("save.po>>{}", CodeUtil.toJson(po));
						old_txnaddressbookdao.save(po);
					} else {
						po = CodeUtil.objectCovert(OLD_TXNADDRESSBOOK.class, beforeObj);
						log.debug("update.po>>{}", CodeUtil.toJson(po));
						old_txnaddressbookdao.update(po);
					}

					// List<OLD_TXNADDRESSBOOK> list=
					// old_txnaddressbookdao.findByInput(beforeObj.getDPUSERID(),beforeObj.getDPGONAME()
					// , beforeObj.getDPABMAIL());

				}
				if (isSync && "delete".equals(method)) {
					log.info("delete-->sync");
					po = old_txnaddressbookdao.findById(beforeObj.getDPADDBKID());
					if (po != null) {
						OLD_TXNADDRESSBOOK delpo = new OLD_TXNADDRESSBOOK();
						delpo.setDPADDBKID(beforeObj.getDPADDBKID());
						old_txnaddressbookdao.delete(delpo);
					} else {
						log.warn("delete-->sync po is null delete not waork userid >>{}", beforeObj.getDPADDBKID());
					}
				}
			} catch (Throwable e) {
				log.error("syncNNBTxnAddressBook.partB.ERROR{}", e);
			}

		} catch (Throwable e) {
			log.error("syncNNBTxnAddressBook.ERROR{}", e);
		}
		log.info("syncNNBTxnAddressBook end...");
		return obj;

	}

}
