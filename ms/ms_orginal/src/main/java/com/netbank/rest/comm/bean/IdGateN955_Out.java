package com.netbank.rest.comm.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IdGateN955_Out {

	String MSGCOD;
	String SESSIONID;
	
}
