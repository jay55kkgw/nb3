package com.netbank.rest.e2e.bean;

import lombok.Data;

@Data
public class GetPublicKey_RS_Data {

	private String pubKey;
}
