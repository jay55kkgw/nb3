package com.netbank.rest.comm.bean;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IdGateMappingQuery_In {

	@JsonProperty @NotNull
	String CUSIDN;
	@JsonProperty  
	String IDGATEID;
	@JsonProperty  
	String DEVICEID;

	@JsonProperty
	String LOGINTYPE;
}
