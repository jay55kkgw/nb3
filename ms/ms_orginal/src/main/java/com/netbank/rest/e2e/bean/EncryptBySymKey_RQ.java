package com.netbank.rest.e2e.bean;

import lombok.Data;

@Data
public class EncryptBySymKey_RQ {

	private String cluster;
	private String keyName;
	private String b64Data;
	private String data;
	private String encoding;
	private String iv;
	private String mode;
	private String keyType;
	
	
}
