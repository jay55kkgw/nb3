package com.netbank.rest.e2e.bean;

import lombok.Data;

@Data
public class GetToken_RS extends Base_RS {
	private GetToken_RS_Data data;
}
