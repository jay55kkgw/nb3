package com.netbank.rest.comm.bean;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SvCancel_Txn_In {

	@NotNull
	String idgateID;
	String channel;
	String method;
	String txnID;
}
