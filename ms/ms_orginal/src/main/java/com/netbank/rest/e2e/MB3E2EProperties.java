package com.netbank.rest.e2e;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@Data
public class MB3E2EProperties {
//	@Value("${apiKeyFile}")
//	String apiKeyFile ="D:/mock_data/trust/apiKeyNB3.pem";
//	@Value("${e2euri}")
//	String e2euri ="http://127.0.0.1:9480/SS6/";
//	@Value("${e2ekeyName}")
//	String e2ekeyName ="TESTNB3_RSA2048";
//	@Value("${e2ekeyLabel}")
//	String e2ekeyLabel ="B050NTKPP1O";
//	@Value("${isE2E}")
//	String isE2E ="Y";
	@Value("${mb3apiKeyFile}")
	String apiKeyFile ;
	@Value("${e2euri}")
	String e2euri ;
	@Value("${mb3e2ekeyName}")
	String e2ekeyName ;
	@Value("${mb3e2ekeyLabel}")
	String e2ekeyLabel;
	@Value("${isE2E}")
	String isE2E;
	@Value("${mb3ApplicationName}")
	String applicationName;
	String cluster ="E2EE";
	String toE2ETimeout ="30";
}
