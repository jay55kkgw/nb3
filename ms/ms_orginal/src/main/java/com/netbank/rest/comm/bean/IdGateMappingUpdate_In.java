package com.netbank.rest.comm.bean;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IdGateMappingUpdate_In {

	@JsonProperty @NotNull
	String CUSIDN;
	@JsonProperty @NotNull
	String IDGATEID;
	@JsonProperty @NotNull
	String DEVICEID;
	@JsonProperty @NotNull
	String TYPE;
	@JsonProperty @NotNull
	String DEVICENAME;
	@JsonProperty
	String LOGINTYPE;
}
