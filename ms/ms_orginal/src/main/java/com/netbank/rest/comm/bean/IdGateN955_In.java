package com.netbank.rest.comm.bean;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IdGateN955_In {

	@JsonProperty @NotNull
	String CUSIDN;
	@JsonProperty  @NotNull
	String DEVICEID;
	@JsonProperty 
	String DEVICENAME;
	@JsonProperty 
	String DEVICETYPE;
	@JsonProperty 
	String DEVICEBRAND;
	@JsonProperty 
	String DEVICEOS;
	@JsonProperty  @NotNull
	String PINKEY;
	@JsonProperty
	String LOGINTYPE;
}
