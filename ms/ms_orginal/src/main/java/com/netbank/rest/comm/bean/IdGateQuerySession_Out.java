package com.netbank.rest.comm.bean;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IdGateQuerySession_Out {


	@JsonProperty 
	Object Data;
	@JsonProperty 
	String SESSIONID;
//	@JsonProperty 
//	String ISHASQCKLGIN;
}
