package com.netbank.rest.og.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Configuration
public class TxnLogExecutorService {

    @Bean("txnlogExecutorService")
    public ExecutorService txnlogExecutorService(){
        return Executors.newFixedThreadPool(20);
    }
}
