package com.netbank.rest.comm.bean;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IdGateQuickSessionId_In {

	@JsonProperty @NotNull
	String DEVICEID;
	@JsonProperty @NotNull
	String DEVICENAME;
	@JsonProperty @NotNull
	String DEVICETYPE;
	@JsonProperty @NotNull
	String DEVICEBRAND;
	@JsonProperty @NotNull
	String DEVICEOS;
	@JsonProperty @NotNull
	String ITEM;
	@JsonProperty @NotNull
	String CUSIDN;
}
