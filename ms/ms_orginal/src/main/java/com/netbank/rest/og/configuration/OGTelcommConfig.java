package com.netbank.rest.og.configuration;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.collect.ImmutableMap;
import com.netbank.telcomm.CommonTelcomm;
import com.netbank.telcomm.TMRACommonTelcomm;

import fstop.services.TopMsgDBDetecter;
import fstop.services.TopMsgF003Detecter;
import fstop.services.TopMsgF007Detecter;
import fstop.services.TopMsgFXDetecter;
import fstop.services.TopMsgFundDetecter;
import fstop.services.TopMsgN320Detecter;
import lombok.extern.slf4j.Slf4j;

/**
 * ApplicationContext-telcomm.xml
 */
@Configuration
@Slf4j
public class OGTelcommConfig {

    @Autowired
    @Qualifier("defaultFieldFormatDefine")
    private Map<String, String> defaultFieldFormatDefine;

    @Autowired
    @Qualifier("fxFieldFormatDefine")
    private Map<String, String> fxFieldFormatDefine;
    
    @Bean
    protected CommonTelcomm n915Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
    	CommonTelcomm c = createTelcomm("n915.txt", "N915");
    	c.setDesc("IDGAT-申請啟用碼(啟用／關閉)");
    	c.setHOSTMSGID("N915");
    	c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
    	c.setErrDetect(dbErrDetect);
    	c.setBeforeCommand(Arrays.asList(
			"SoftCertContext()",
			"XMLCN()", //<!-- 使用 日期 -->
			"XMLCA()" //<!-- 使用SH1 -->
        ));
    	return c;
    }
    @Bean
    protected CommonTelcomm n93aTelcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
    	CommonTelcomm c = createTelcomm("n93a.txt", "N93A");
    	c.setDesc("快速登入設定交易");
    	c.setHOSTMSGID("N93A");
    	c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
    	c.setErrDetect(dbErrDetect);
    	return c;
    }    @Bean
    protected CommonTelcomm n93bTelcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
    	CommonTelcomm c = createTelcomm("n93b.txt", "N93B");
    	c.setDesc("快速登入資料查詢");
    	c.setHOSTMSGID("N93B");
    	c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
    	c.setErrDetect(dbErrDetect);
    	return c;
    }
    @Bean
    protected CommonTelcomm n939Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
    	CommonTelcomm c = createTelcomm("n939.txt", "N939");
    	c.setDesc("快速交易申請狀況查詢");
    	c.setHOSTMSGID("N939");
    	c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
    	c.setErrDetect(dbErrDetect);
    	return c;
    }
    @Bean
    protected CommonTelcomm n955Telcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
    	CommonTelcomm c = createTelcomm("n955.txt", "N955");
    	c.setDesc("IDGATE驗證啟用碼");
    	c.setHOSTMSGID("N955");
    	c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
    	c.setErrDetect(dbErrDetect);
    	c.setBeforeCommand(Arrays.asList(
				"PPSYNCN1()",
				"IDGATEPINKEY()"
        ));
    	return c;
    }
    
    /**
	 * pinew1 專用
	 * @param dbErrDetect
	 * @return
	 * @throws Exception
	 */
	@Bean
	protected CommonTelcomm n951PINTelcomm(@Qualifier("dbErrDetect") TopMsgDBDetecter dbErrDetect) throws Exception {
		CommonTelcomm c = createTelcomm("n951.txt", "N951");
		c.setDesc("查詢密碼確認");
		c.setHOSTMSGID("N951");
		c.setDefaultFieldFormatDefine(defaultFieldFormatDefine);
		c.setErrDetect(dbErrDetect);
		return c;
	}
    
    

//  @Value("${tmra.telcomm.url:http://localhost:8880/TMRA/com}")
//  private String restBaseUrl;
  @Value("${tmra.uri.host:http://localhost}")
  private String restHost;
 // @Value("${tmra.uri.port:8080}")
//  private String restPort;
  @Value("${tmra.uri.path:TMRA/com}")
  private String restPath;
  
  protected CommonTelcomm createTelcomm(String mockfileName, String txid) throws IOException {

	  TMRACommonTelcomm ret = new TMRACommonTelcomm();

      // http://localhost:8880/TMRA/com
      //String url = restHost + ":" + restPort + "/"+ restPath;
//      log.info("createTelcomm.url: {}", url);
      
      if(restHost.endsWith("/") == false) {
          restHost = restHost + "/";
      }
      String url = restHost + restPath;
      log.info("createTelcomm.url: {}", url);
      
      if(url.endsWith("/") == false) {
          url = url + "/";
      }

      url = url + txid.toLowerCase();
      ret.setUrl(url);

      ret.setTxid(txid);

      log.info("create rest mock telcomm, txid : {}, url: {}", ret.getTxid(), ret.getUrl());

      return ret;
  }


}
