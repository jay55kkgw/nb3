package com.netbank.rest.comm.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.netbank.rest.comm.service.PublicKey;
import com.netbank.rest.e2e.E2ETransform;
import com.netbank.rest.e2e.MB3_E2ETransform;

import lombok.extern.slf4j.Slf4j;


@Configuration
@Slf4j
public class CommServicesConfig {
	
	@Bean
	protected E2ETransform e2etransform() throws Exception{
		return new  E2ETransform();
	}
	@Bean
	protected MB3_E2ETransform mb3e2etransform() throws Exception{
		return new  MB3_E2ETransform();
	}
	@Bean
	protected PublicKey publickeyService() throws Exception{
		return new  PublicKey();
	}

}
