package com.netbank.rest.comm.bean;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegisterCallback_In {

	@NotNull
	String idgateID;
	@NotNull
	String channel;
	@NotNull
	String sessionID;
	
	RegType regType;
	

	@Getter
	@Setter
	public class RegType{
		String bio;
		String diagram;
		String pwd;
	}
	
}
