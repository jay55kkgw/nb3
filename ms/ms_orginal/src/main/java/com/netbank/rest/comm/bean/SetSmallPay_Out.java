package com.netbank.rest.comm.bean;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import fstop.orm.po.QUICKLOGINMAPPING;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SetSmallPay_Out {

	@JsonProperty 
	Object Data;
	@JsonProperty 
	Object REC;
}
