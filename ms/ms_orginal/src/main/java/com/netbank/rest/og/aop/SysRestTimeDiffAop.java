package com.netbank.rest.og.aop;

import java.util.Base64;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.joda.time.DateTime;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.netbank.util.fstop.DateUtil;

import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Aspect
@Component
public class SysRestTimeDiffAop {

	@Autowired
	HttpServletRequest req;

	@Around("within(com.netbank.rest.util.RESTUtil) && (execution(* send(..) )|| execution(* send2DataPower(..)) || execution(* send2IdGate(..)) ) ")
	public Object restTimediff(ProceedingJoinPoint pjp) {
		log.info("SysRestTimeDiffAop.restTimediff...");
		Object obj = null;
		// log.info("reqParam>>{}",(Map<String,String>)pjp.getArgs()[0]);;
		DateTime d1 = null;
		DateTime d2 = null;
		String pstimediff = "";
		String api = "";
		try {
			api = (String) pjp.getArgs()[1];
			api = api.substring(api.lastIndexOf("/")).replace("/", "");
		} catch (Exception e) {
			log.error("{}", e);
		}
		try {
			log.info("Before doAround.proceed..");
			d1 = new DateTime();
			obj = pjp.proceed();
			d2 = new DateTime();
			log.info("After doAround.proceed..");
			pstimediff = DateUtil.getDiffTimeMills(d1, d2);
			log.warn("API>>{} , pstimediff>>{}", api,pstimediff);
		} catch (Throwable e) {
			log.error("SysRestTimeDiffAop.timediff{}", e);
		}
		log.info("SysRestTimeDiffAop.timediff end...");
		return obj;

	}

}
