package com.netbank.rest.comm.batch;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import fstop.services.CommonService;
import lombok.Data;

@Component
@Data
public class NotifyValueProperties {
	
	@Value("${notify_clientId}")
	private String clientId;
	@Value("${notify_acount}")
	private String userName;
	@Value("${notify_pd}")
	private String pd;
	@Value("${notify_host}")
	private String notifyhost;
	@Value("${notify_port}")
	private String notifyport;
	
	@Value("${apns_key}")
	private String apnskey;
	@Value("${apns_keypd}")
	private String apnskeypd;
	@Value("${apns_production}")
	private String apnsproduction;
	
}
