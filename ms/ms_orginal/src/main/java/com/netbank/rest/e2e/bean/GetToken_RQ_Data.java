package com.netbank.rest.e2e.bean;

import lombok.Data;

@Data
public class GetToken_RQ_Data {

	private String applicationName;
	private String signTime;
	
}
