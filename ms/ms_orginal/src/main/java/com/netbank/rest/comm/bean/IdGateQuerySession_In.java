package com.netbank.rest.comm.bean;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IdGateQuerySession_In {

	@JsonProperty @NotNull
	String DEVICEID;
	@JsonProperty @NotNull
	String IDGATEID;
	@JsonProperty  
	String CREATESESSION;
	@JsonProperty
	String LOGINTYPE;
}
