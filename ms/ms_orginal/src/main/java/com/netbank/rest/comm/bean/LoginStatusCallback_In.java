package com.netbank.rest.comm.bean;

import javax.validation.constraints.NotNull;

import com.netbank.rest.comm.bean.RegisterCallback_In.RegType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginStatusCallback_In {

	@NotNull
	String idgateID;
	@NotNull
	String sessionID;
	@NotNull
	String status;
	
	String type;
}
