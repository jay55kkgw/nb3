package com.netbank.rest.comm.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChangeSettingCallback_Out {

	String returnCode;
	String returnMsg;
}
