package com.netbank.rest.comm.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.netbank.rest.comm.batch.BhEnvBean;

import lombok.extern.slf4j.Slf4j;


@Configuration
@Slf4j
public class BHEnvConfig {
	
	@Bean
	protected  BhEnvBean bhEnvBean() throws Exception{
		return new  BhEnvBean();
	}
	
}
