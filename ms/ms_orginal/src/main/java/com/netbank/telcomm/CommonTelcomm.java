package com.netbank.telcomm;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.core.appender.rolling.action.IfAccumulatedFileCount;
import org.joda.time.DateTime;

import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.Row;
import fstop.model.TelcommResult;
import fstop.orm.po.ADMMSGCODE;
import fstop.services.TopMsgDetecter;
import fstop.telcomm.TelCommExec;
import fstop.telcomm.TelCommParams;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import fstop.util.StrUtils;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public abstract class CommonTelcomm extends TelCommParams implements TelCommExec  {

	private String txid;


    private TopMsgDetecter errDetect;

	public abstract TelcommResult query(final Map params);

	public TelcommResult data_Format(List datalist) {
		TelcommResult tresult = new TelcommResult(datalist);

        if(errDetect != null) {
        	
        	// 功能代號
        	String header = StrUtils.trim(tresult.getFlatValues().get("HEADER"));
        	log.info("CommonTelcomm.data_Format.HEADER: {}", header);
        	
        	// 交易結果
        	String topMsg = StrUtils.trim(tresult.getFlatValues().get("TOPMSG"));
        	log.info("data_Format.original.TOPMSG: {}", topMsg);
        	
            /*
             * N071-跨行轉帳及繳稅
			 * 說明:下行欄位交易結果欄位可能是RSPCOD或SYNC，先處理把交易結果存到TOPMSG
             */
        	if(header.equals("N071")) {
        		String rspcod = tresult.getValueByFieldName("RSPCOD").trim(); // 回應代碼
        		String outacn = tresult.getValueByFieldName("OUTACN").trim(); // 轉出帳號
        		String sync = tresult.getValueByFieldName("SYNC"); // Sync.Check Item
        		
        		log.info("data_Format.rspcod: {}", rspcod);
        		log.info("data_Format.outacn: {}", outacn);
        		log.info("data_Format.sync: {}", sync);
        		
        		topMsg = n071resultProcess(topMsg, rspcod, outacn, sync);
        	}
        	
        	log.info("data_Format.topMsg: {}", topMsg);
        	
            ADMMSGCODE isMsgCode = null;
            
            isMsgCode = errDetect.isError(topMsg, tresult);
            
            log.info(ESAPIUtil.vaildLog("data_Format.msgcode: "+ isMsgCode));
            // 20200131update only for N070 N071 ErrorCode EUMP from Batch
            //C5E4D4D7 = >> EUMP
            if(("C5E4D4D7".equalsIgnoreCase(tresult.getFlatValues().get("SYNC"))||"EUMP".equalsIgnoreCase(tresult.getFlatValues().get("SYNC")))
            		&&tresult.getFlatValues().containsKey("__TRANS_STATUS")){
            	 throw new TopMessageException("EUMP", "", JSONUtils.toJson(tresult.getFlatValues()));
            }
            //
            // 交易類錯誤代碼欄位亦可為帳號，若資料庫找不到錯誤代碼原邏輯會視為成功，但不一定是成功，也有可能單純只是資料庫沒有對應的錯誤代碼，故另外加上長度及不為成功代碼的判斷
            String[] successCodes = {"R000","OKLR","OKOK","","0000000","0000","OKOV"};
            // 將N911 E161錯誤略過不throw TopMessageException
            try {
	            if(header.equals("N911") && topMsg != null) {
	            	if(topMsg.equals("E161") || topMsg.equals("E225") || topMsg.equals("E310")) {
	            		isMsgCode = null;
		            	topMsg = "0000";
	            	}
	            }
	            if( (header.equals("N855") || header.equals("N856")) && topMsg != null) {
	            	isMsgCode = null;
	            	topMsg = "0000";
	            }
            }catch(Exception e) {
            	log.error("set N911 E161 not error error >> {}" , e);
            }
            if (topMsg != null && isMsgCode == null && topMsg.length()<=6 && !Arrays.stream(successCodes).anyMatch(topMsg::equals)) {
                throw new TopMessageException(topMsg, "", "");
            }

            // 原邏輯
            if (topMsg != null && isMsgCode != null) {
                throw new TopMessageException(isMsgCode.getADMCODE(), isMsgCode.getADMSGIN(), isMsgCode.getADMSGOUT());
            }
            else {
            	// 不明白為什麼把TOPMSG改成__TOPMSG，先註解看看
//                tresult.getFlatValues().put("__TOPMSG", tresult.getFlatValues().get("TOPMSG"));
//                tresult.getFlatValues().put("TOPMSG", "");
            }
            
            
        }

        try {
        	log.info("FlatValues entrySet begin: {}", new DateTime().toString("yyyy/MM/dd HH:mm:ss"));
			for(Map.Entry<String, String> o : tresult.getFlatValues().entrySet()) {
			    String key = (o.getKey().lastIndexOf("_") > 0 &&  Character.isDigit( o.getKey().charAt(o.getKey().lastIndexOf("_") + 1))  ? o.getKey().substring(0, o.getKey().lastIndexOf("_")) : o.getKey());

			    if(getFieldFormat().containsKey(key)) {
			        //格式轉換
			        String tval = transferValue(getFieldFormat().get(key), o.getValue());
			        transferValue(tresult.getFlatValues(), o.getKey(), tval);
			    }
			    else if(getDefaultFieldFormatDefine().containsKey(key)) {
			        //格式轉換
			        String tval = transferValue(getDefaultFieldFormatDefine().get(key), o.getValue());
			        transferValue(tresult.getFlatValues(), o.getKey(), tval);
			    }
			}

			log.info("FlatValues entrySet end: {}", new DateTime().toString("yyyy/MM/dd HH:mm:ss"));
			for(Row row : tresult.getOccurs().getRows()) {
			    for(String o : row.getColumnNames()) {
			        if(getFieldFormat().containsKey(o)) {
			            //格式轉換
			            String tval = transferValue(getFieldFormat().get(o), row.getValue(o));
			            transferValue(row, o, tval);
			        }
			        else if(getDefaultFieldFormatDefine().containsKey(o)) {
			            //格式轉換
			            String tval = transferValue(getDefaultFieldFormatDefine().get(o), row.getValue(o));
			            transferValue(row, o, tval);
			        }
			    }
			}
			
			log.info("Occurs().getRows end: {}", new DateTime().toString("yyyy/MM/dd HH:mm:ss"));
		} catch (Exception e) {
			log.error("data_Format error >>{}",e);
		}

        return tresult;
	}
	
	
	public void transferValue(Map<String, String> flatValues, String fieldKey, String fieldValue) {
        flatValues.put(fieldKey, fieldValue);
    }

    public void transferValue(Row row, String fieldKey, String fieldValue) {
        row.setValue(fieldKey, fieldValue);
    }
    
    

    /*
     * 對格式為數字的值, 做 format, 若 format 失敗, 則回傳其原來的值
     */
    public String transferValue(String formatDefine, String value) {
        if(formatDefine.startsWith("D") || formatDefine.startsWith("d")) {
            try {
                return DateTimeUtils.format(formatDefine.substring(formatDefine.indexOf(":") +1), DateTimeUtils.parse(formatDefine.substring(formatDefine.indexOf(":") + 1), value));
            }
            catch(Exception e) {
                log.error(value + "," + formatDefine, e);
            }
        }
        else if(formatDefine.startsWith("N") || formatDefine.startsWith("n")) {
            try {
                String v = StrUtils.trim(value);
                if(v != null && v.length() > 0 && ((v.endsWith("-") || v.endsWith(" ") || v.endsWith("+")))) {
                    v = (v.substring(v.length() - 1, v.length()) + v.substring(0, v.length() -1)).trim();
                }
                if(v != null && v.length() > 0) {
                    String fmt = formatDefine.substring(formatDefine.indexOf(":") + 1);
                    if(fmt.lastIndexOf(".") > -1) {
                        int pow = fmt.length() - fmt.lastIndexOf(".") - 1;
                        double div = Math.pow(10, pow);
                        return   new DecimalFormat(fmt).format(new BigDecimal(v).divide(new BigDecimal(div)));
                    }
                    else {
                        return new DecimalFormat(fmt).format(new BigDecimal(v));
                    }
                }
                return v;
            }
            catch(Exception e) {
                log.error(value + "," + formatDefine, e);
            }
        }

        return value;
    }
    
    
    /*
     * N071-跨行轉帳及繳稅
	 * 說明:下行欄位交易結果欄位可能是RSPCOD或SYNC，先處理把交易結果存到TOPMSG
     */
    public String n071resultProcess(String topMsg, String rspcod, String outacn, String sync) {
    	
    	String resultTopmsg = topMsg;
    	
    	try {
			if( sync.equals("0302") || sync.equals("E089") ) {
				// 安控驗證失敗，交易結果欄位為SYNC
				log.info("N071.SYNC: {}", sync);
				resultTopmsg = sync;
				
			} else if( !rspcod.equals("") && !rspcod.equals("0000") ){
				// 先判斷RSPCOD，不為成功
				log.info("N071.RSPCOD: {}", rspcod);
				resultTopmsg = rspcod;
				
			} else if( rspcod.equals("") && outacn.equals("") ) {
				// 若RSPCOD及OUTACN都為空，再判斷SYNC，不為空則存進TOPMSG
				log.info("N071.SYNC: {}", sync);
				if( sync.length() > 0 ) {
					resultTopmsg = sync;
				} else {
					resultTopmsg = "ZX99";
				}
				
			} else if ( rspcod.equals("") && !outacn.equals("") ){
				// 20190531新增一種測試案例，ex:{HEADER":"N071","SYNC":"E038","RSPCOD":"","OUTACN":"01062790594"}
				log.info("N071.sync: {}", sync);
				resultTopmsg = sync;
				
			}
			
			// TODO 故成功應為RSPCOD、SYNC皆為空或0000，OUTACN有值沒值都可以?
			
    	}
        catch(Exception e) {
            log.error("n071resultProcess.error: {}", e);
        }
    	
		return resultTopmsg;
    }
    
}
