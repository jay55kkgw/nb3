package com.netbank.telcomm;

import com.netbank.http.MessageHttpClient;
import fstop.exception.TopMessageException;
import fstop.model.Row;
import fstop.model.TelcommResult;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import fstop.util.StrUtils;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import java.io.File;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@Slf4j
public class MockRestValueCommonTelcomm extends CommonTelcomm  {
    //Logger log = LoggerFactory.getLogger(getClass());

	@Value("${mock.telcomm.data.dir}")
	private String mockTelcommDataDir;

	private String txid;

	private String url;

	private String mockFileName;

	public TelcommResult query(final Map params) {
		MessageHttpClient client = new MessageHttpClient();
		HashMap<String, String> request = new HashMap();
		String retJSON = null;
		try {

			request.putAll(params);
			log.info("send request url: {}, begin: {}", url, new DateTime().toString("yyyy/MM/dd HH:mm:ss"));
			retJSON = client.send(url, request);
			log.info("send request url: {}, end: {}", url, new DateTime().toString("yyyy/MM/dd HH:mm:ss"));

			List r = null;
			r = JSONUtils.toList(retJSON);
			File f = new File(mockTelcommDataDir, mockFileName);
			f.setWritable(true,true);
			f.setReadable(true, true);
			f.setExecutable(true,true);
			if (f.exists() == false || f.length() < retJSON.length()) {
				FileUtils.writeStringToFile(f, retJSON, "UTF-8");
			}



			TelcommResult tresult = data_Format(r);
//			TelcommResult tresult = new TelcommResult(r);
//
//
//			for(Map.Entry<String, String> o : tresult.getFlatValues().entrySet()) {
//				String key = (o.getKey().lastIndexOf("_") > 0 &&  Character.isDigit( o.getKey().charAt(o.getKey().lastIndexOf("_") + 1))  ? o.getKey().substring(0, o.getKey().lastIndexOf("_")) : o.getKey());
//
//				if(getFieldFormat().containsKey(key)) {
//					//格式轉換
//					String tval = transferValue(getFieldFormat().get(key), o.getValue());
//					transferValue(tresult.getFlatValues(), o.getKey(), tval);
//				}
//				else if(getDefaultFieldFormatDefine().containsKey(key)) {
//					//格式轉換
//					String tval = transferValue(getDefaultFieldFormatDefine().get(key), o.getValue());
//					transferValue(tresult.getFlatValues(), o.getKey(), tval);
//				}
//			}
//
//			for(Row row : tresult.getOccurs().getRows()) {
//				for(String o : row.getColumnNames()) {
//					if(getFieldFormat().containsKey(o)) {
//						//格式轉換
//						String tval = transferValue(getFieldFormat().get(o), row.getValue(o));
//						transferValue(row, o, tval);
//					}
//					else if(getDefaultFieldFormatDefine().containsKey(o)) {
//						//格式轉換
//						String tval = transferValue(getDefaultFieldFormatDefine().get(o), row.getValue(o));
//						transferValue(row, o, tval);
//					}
//				}
//			}

			return tresult;

		} catch(Exception e) {
			String msg = "取得電文結果錯誤!\nurl : " + url +
					", request: " + JSONUtils.toJson(request) + "\n return: " + retJSON;
			log.error(msg);
			throw new TopMessageException(msg, e);
		}
	}
//
//
//	public void transferValue(Map<String, String> flatValues, String fieldKey, String fieldValue) {
//		flatValues.put(fieldKey, fieldValue);
//	}
//
//	public void transferValue(Row row, String fieldKey, String fieldValue) {
//		row.setValue(fieldKey, fieldValue);
//	}
//
//	/*
//	 * 對格式為數字的值, 做 format, 若 format 失敗, 則回傳其原來的值
//	 */
//	public String transferValue(String formatDefine, String value) {
//		if(formatDefine.startsWith("D") || formatDefine.startsWith("d")) {
//			try {
//				return DateTimeUtils.format(formatDefine.substring(formatDefine.indexOf(":") +1), DateTimeUtils.parse(formatDefine.substring(formatDefine.indexOf(":") + 1), value));
//			}
//			catch(Exception e) {
//				log.error(value + "," + formatDefine, e);
//			}
//		}
//		else if(formatDefine.startsWith("N") || formatDefine.startsWith("n")) {
//			try {
//				String v = StrUtils.trim(value);
//				if(v != null && v.length() > 0 && ((v.endsWith("-") || v.endsWith(" ") || v.endsWith("+")))) {
//					v = (v.substring(v.length() - 1, v.length()) + v.substring(0, v.length() -1)).trim();
//				}
//				if(v != null && v.length() > 0) {
//					String fmt = formatDefine.substring(formatDefine.indexOf(":") + 1);
//					if(fmt.lastIndexOf(".") > -1) {
//						int pow = fmt.length() - fmt.lastIndexOf(".") - 1;
//						double div = Math.pow(10, pow);
//						return   new DecimalFormat(fmt).format(new BigDecimal(v).divide(new BigDecimal(div)));
//					}
//					else {
//						return new DecimalFormat(fmt).format(new BigDecimal(v));
//					}
//				}
//				return v;
//			}
//			catch(Exception e) {
//				log.error(value + "," + formatDefine, e);
//			}
//		}
//
//		return value;
//	}


}
