package com.netbank.telcomm;

import java.lang.reflect.Type;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import fstop.model.TelcommResult;
import fstop.services.TopMsgDetecter;
import fstop.telcomm.TelCommExec;
import fstop.telcomm.TelCommParams;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public abstract class DataPower_CommonTelcomm extends TelCommParams implements TelCommExec  {
		
	private String txid;


    private TopMsgDetecter errDetect;

	public abstract TelcommResult query(final Map params);

	public TelcommResult data_Format(String data) {
		Gson gson = new Gson();
		Type type = new TypeToken<Map<String, String>>(){}.getType();
		Map<String, Object> dataMap = gson.fromJson(data, type);
		
		TelcommResult tresult = new TelcommResult(dataMap);
		return tresult;
	}
	
}
