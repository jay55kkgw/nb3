package com.netbank.telcomm;

import fstop.model.Row;
import fstop.model.TelcommResult;
import fstop.telcomm.TelCommExec;
import fstop.telcomm.TelCommParams;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import fstop.util.StrUtils;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

@Data
@Slf4j
public class MockFileDataCommonTelcomm extends CommonTelcomm  {
//
//	@Autowired
//	private QueryTemplate template;

	private String txid;

	private String mockResult;

	public TelcommResult query(final Map params) {
		
		return data_Format(JSONUtils.toList(mockResult));
	}
}
