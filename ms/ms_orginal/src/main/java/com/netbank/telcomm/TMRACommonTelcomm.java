package com.netbank.telcomm;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.client.ResourceAccessException;

import com.google.gson.Gson;
import com.netbank.rest.util.RESTUtil;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.TelcommResult;
import fstop.telcomm.cmd.CommandUtils;
import fstop.util.JSONUtils;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class TMRACommonTelcomm extends CommonTelcomm {

//	private String txid;
	
	@Value("${toTmraTimeout}")
	private Integer toTmraTimeout;
	@Value("${batachTimeout:600}")
	private Integer batachTimeout;

	@Value("#{'${spTimeoutList:C017,ND01,ND08,ND10,F021,XMXR,GD11,N021,N022,N023,N024,N025,N026,N030,}'}.split(',')")
//	@Value("#{'${blog-list}'.split(',')}")
	private List<String> spTimeoutList;
	private String url;

	@Autowired RESTUtil restutil;
	
	public TelcommResult query(final Map params) {
//		MessageHttpClient client = new MessageHttpClient();

		//為了帶正確的電文代碼 
		params.put("TXID", this.getHOSTMSGID());
		
		
		CommandUtils.doBefore(this.getBeforeCommand(), params);

		HashMap<String, String> request = new HashMap();
		String retJSON = null;
		try {

			request.putAll(params);
			log.info("send request url: {}, begin: {}", url, new DateTime().toString("yyyy/MM/dd HH:mm:ss"));
//			retJSON = client.send(url, request);
			toTmraTimeout = spTimeoutList.contains(params.get("TXID")) ? batachTimeout:toTmraTimeout;
			log.info("toTmraTimeout>>{}",toTmraTimeout);
//			retJSON = RESTfulClientUtil.send(request, url, toTmraTimeout);
			log.debug("restutil>>{}",restutil);
			retJSON = restutil.send(request, url, toTmraTimeout);
			log.info("send request url: {}, end: {}", url, new DateTime().toString("yyyy/MM/dd HH:mm:ss"));

			List r = null;
//			20191101 by  hugo  修正F001 電文回傳字串 null 時會被轉成"null"
			r = CodeUtil.fromJson(retJSON, List.class);
//			r = JSONUtils.toList(retJSON);
//			log.info("data_Retouch begin: {}", new DateTime().toString("yyyy/MM/dd HH:mm:ss"));
//			r = data_Retouch(retJSON);
//			log.info("data_Retouch end: {}", new DateTime().toString("yyyy/MM/dd HH:mm:ss"));
			
			
			//20200131update
			if("RESEND".equalsIgnoreCase((String)params.get("__TRANS_STATUS"))&&"N070".equalsIgnoreCase((String)params.get("ADOPID"))) {
				Map mapOri = JSONUtils.json2map(retJSON.substring(1,retJSON.length()-1));
				mapOri.put("__TRANS_STATUS", "RESEND");
				r = CodeUtil.fromJson("["+JSONUtils.map2json(mapOri)+"]", List.class);
			}
			//
			log.info(ESAPIUtil.vaildLog("data_Format.before: {}"+CodeUtil.toJson(r)));
            TelcommResult tresult  = data_Format(r);


			CommandUtils.doAfter(this.getAfterCommand(), tresult, params);

            return tresult;
		} catch (ResourceAccessException e) {
			log.error("",e);
			throw TopMessageException.create("FE0006");
			
		} catch (TopMessageException e) {
			String msg = "取得電文結果錯誤!\nurl : " + url + ", request: " + JSONUtils.toJson(request) + "\n return: "
					+ retJSON;
			log.error(msg);
			throw e;
		} catch (Exception e) {
			String msg = "取得電文結果錯誤!\nurl : " + url + ", request: " + JSONUtils.toJson(request) + "\n return: "
					+ retJSON;
			log.error(msg);
			throw e;
		}
	}
	
	public List<String> getSPTimeout() {
		List<String> list = new LinkedList<String>();
		list.add("C017");
		return list;
		
	}
	
	

	/**
	 * 資料格式轉換
	 * @param datalist
	 * @return
	 */
//	public TelcommResult data_Format(List datalist) {
//		TelcommResult tresult = new TelcommResult(datalist);
//
//
//        try {
//        	log.info("FlatValues entrySet begin: {}", new DateTime().toString("yyyy/MM/dd HH:mm:ss"));
//			for(Map.Entry<String, String> o : tresult.getFlatValues().entrySet()) {
//			    String key = (o.getKey().lastIndexOf("_") > 0 &&  Character.isDigit( o.getKey().charAt(o.getKey().lastIndexOf("_") + 1))  ? o.getKey().substring(0, o.getKey().lastIndexOf("_")) : o.getKey());
//
//			    if(getFieldFormat().containsKey(key)) {
//			        //格式轉換
//			        String tval = transferValue(getFieldFormat().get(key), o.getValue());
//			        transferValue(tresult.getFlatValues(), o.getKey(), tval);
//			    }
//			    else if(getDefaultFieldFormatDefine().containsKey(key)) {
//			        //格式轉換
//			        String tval = transferValue(getDefaultFieldFormatDefine().get(key), o.getValue());
//			        transferValue(tresult.getFlatValues(), o.getKey(), tval);
//			    }
//			}
//
//			log.info("FlatValues entrySet end: {}", new DateTime().toString("yyyy/MM/dd HH:mm:ss"));
//			for(Row row : tresult.getOccurs().getRows()) {
//			    for(String o : row.getColumnNames()) {
//			        if(getFieldFormat().containsKey(o)) {
//			            //格式轉換
//			            String tval = transferValue(getFieldFormat().get(o), row.getValue(o));
//			            transferValue(row, o, tval);
//			        }
//			        else if(getDefaultFieldFormatDefine().containsKey(o)) {
//			            //格式轉換
//			            String tval = transferValue(getDefaultFieldFormatDefine().get(o), row.getValue(o));
//			            transferValue(row, o, tval);
//			        }
//			    }
//			}
//			
//			log.info("Occurs().getRows end: {}", new DateTime().toString("yyyy/MM/dd HH:mm:ss"));
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			log.error("",e);
//		}
//
//
//        return tresult;
//	}
	
	
	public List data_Retouch(String json) {
		Gson gs = new Gson();
		Map map = gs.fromJson(json, Map.class);
		log.info("map>>{}", map);
		List<Map<String, String>> list = (List<Map<String, String>>) map.get("REC");
		List newlist = new LinkedList<>();
		log.info("list>>{}", list);

		List<String> colList = null;
		Map<String, List<String>> colMap = null;
		Map<String, Object> allMap = new HashMap<>();
		String OCCURS = "OCCURS_電文陣列";
		String ABEND = "OKOK";
		int page = 1;
		int cnt = 0;
//		TODO 先做keymap
		Map<String,String> tmpkeymap = list.get(0);
		Map<String,List<String>>   keymap = new HashMap<String,List<String>>() ;
		for(String key :tmpkeymap.keySet()) {
			keymap.put(key, new LinkedList<String>());
		}
		log.info("keymap>>{}",keymap);
		
		
		for (Map<String, String> tmpMap : list) {
			for (String key : tmpMap.keySet()) {
				
				if(keymap.containsKey(key)) {
					List tmplist = keymap.get(key);
//					log.info("tmplist>>{}",tmplist);
//					log.info("tmpMap.get(key)>>{}",tmpMap.get(key));
					tmplist.add(tmpMap.get(key));
					keymap.put(key, tmplist);
//					log.info("keymap2>>{}",keymap);
				}
				
				
			}

			if (list.size() == 20) {
				page++;
			}
			if ((list.indexOf(tmpMap) == list.size() - 1)) {
				ABEND = "OKLR";
			}
//			log.info("list.indexOf(tmpMap)>>{}", list.indexOf(tmpMap));
//			log.info("islast>>{}", (list.indexOf(tmpMap) == list.size() - 1));
//			log.info("cnt>>{}", cnt);
//			log.info("iscnt>>{}", (cnt == list.size() - 1));

			if ((cnt == list.size() - 1) || list.size() == 20) {
					allMap.put("ABEND", ABEND);
					allMap.put("OCCURS_電文陣列" + page, keymap);
					allMap.put("OCCURS_VECTOR_FOR_MULTIPLE_INDEX", keymap);
					allMap.put("REC_NO", map.get("REC_NO"));
					allMap.put("TOPMSG", map.get("TOPMSG"));
					allMap.put("TXID", map.get("TXID"));
					allMap.putAll(keymap);
			}
			cnt++;
		}
		log.info("cnt>>{}", cnt);
		newlist.add(allMap);
		log.info("newlist>>{}", newlist);
		log.info("newlist.json>>{}", gs.toJson(newlist));
		return newlist;
	}

//
//    public void transferValue(Map<String, String> flatValues, String fieldKey, String fieldValue) {
//        flatValues.put(fieldKey, fieldValue);
//    }
//
//    public void transferValue(Row row, String fieldKey, String fieldValue) {
//        row.setValue(fieldKey, fieldValue);
//    }

    /*
     * 對格式為數字的值, 做 format, 若 format 失敗, 則回傳其原來的值
     */
//    public String transferValue(String formatDefine, String value) {
//        if(formatDefine.startsWith("D") || formatDefine.startsWith("d")) {
//            try {
//                return DateTimeUtils.format(formatDefine.substring(formatDefine.indexOf(":") +1), DateTimeUtils.parse(formatDefine.substring(formatDefine.indexOf(":") + 1), value));
//            }
//            catch(Exception e) {
//                log.error(value + "," + formatDefine, e);
//            }
//        }
//        else if(formatDefine.startsWith("N") || formatDefine.startsWith("n")) {
//            try {
//                String v = StrUtils.trim(value);
//                if(v != null && v.length() > 0 && ((v.endsWith("-") || v.endsWith(" ") || v.endsWith("+")))) {
//                    v = (v.substring(v.length() - 1, v.length()) + v.substring(0, v.length() -1)).trim();
//                }
//                if(v != null && v.length() > 0) {
//                    String fmt = formatDefine.substring(formatDefine.indexOf(":") + 1);
//                    if(fmt.lastIndexOf(".") > -1) {
//                        int pow = fmt.length() - fmt.lastIndexOf(".") - 1;
//                        double div = Math.pow(10, pow);
//                        return   new DecimalFormat(fmt).format(new BigDecimal(v).divide(new BigDecimal(div)));
//                    }
//                    else {
//                        return new DecimalFormat(fmt).format(new BigDecimal(v));
//                    }
//                }
//                return v;
//            }
//            catch(Exception e) {
//                log.error(value + "," + formatDefine, e);
//            }
//        }
//
//        return value;
//    }
}
