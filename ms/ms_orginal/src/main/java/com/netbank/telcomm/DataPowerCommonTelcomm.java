package com.netbank.telcomm;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.client.ResourceAccessException;

import com.netbank.rest.util.RESTUtil;

import fstop.exception.TopMessageException;
import fstop.model.TelcommResult;
import fstop.telcomm.cmd.CommandUtils;
import fstop.util.JSONUtils;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class DataPowerCommonTelcomm extends DataPower_CommonTelcomm {

//	private String txid;
	
	@Value("${toDataPowerTimeout}")
	private Integer toDataPowerTimeout;
	@Value("${batachTimeout:600}")
	private Integer batachTimeout;

//	@Value("#{'${spTimeoutList:C017,}'}.split(',')")
//	private List<String> spTimeoutList;
	
	private String url;

	@Autowired RESTUtil restutil;
	
	public TelcommResult query(final Map params) {
//		MessageHttpClient client = new MessageHttpClient();

		//為了帶正確的電文代碼 
//		params.put("TXID", this.getHOSTMSGID());
		
		
		CommandUtils.doBefore(this.getBeforeCommand(), params);

		HashMap<String, String> request = new HashMap<String, String>();
		String retJSON = null;
		try {

			request.putAll(params);
			log.info("send request url: {}, begin: {}", url, new DateTime().toString("yyyy/MM/dd HH:mm:ss"));
			
//			toDataPowerTimeout = spTimeoutList.contains(params.get("TXID")) ? batachTimeout:toDataPowerTimeout;
			log.info("toDataPowerTimeout>>{}",toDataPowerTimeout);
			log.debug("restutil>>{}",restutil);
			
			retJSON = restutil.send2DataPower(request, url, toDataPowerTimeout);
			log.info("send request url: {}, end: {}", url, new DateTime().toString("yyyy/MM/dd HH:mm:ss"));

            TelcommResult tresult  = data_Format(retJSON);
            return tresult;
            
		} catch (ResourceAccessException e) {
			log.error("",e);
			throw TopMessageException.create("FE0006");
			
		} catch (TopMessageException e) {
			String msg = "取得電文結果錯誤!\nurl : " + url + ", request: " + JSONUtils.toJson(request) + "\n return: "
					+ retJSON;
			log.error(msg);
			throw e;
		} catch (Exception e) {
			String msg = "取得電文結果錯誤!\nurl : " + url + ", request: " + JSONUtils.toJson(request) + "\n return: "
					+ retJSON;
			log.error(msg);
			throw e;
		}
	}
	
}
