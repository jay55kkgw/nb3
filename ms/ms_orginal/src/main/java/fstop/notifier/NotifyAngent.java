package fstop.notifier;

import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.ServletContext;

import com.netbank.util.ESAPIUtil;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
@Slf4j
public class NotifyAngent implements Observer {
	
	private static Map<String, MessageTemplate> tmplPool = new HashMap();
	
	private static NotifyAngent instance = null;
	
	public static String notifyTemplateFolder = "";
	
	public final static String POSTFIX = ".html";
	
	public static NotifyAngent getInstance() {
		if(instance == null) {
			synchronized (NotifyAngent.class) {
				if(instance == null) {
					instance = new NotifyAngent();
				}
			}
		}
		return instance;
	}

	public void update(Observable o, Object arg) {
		if(arg instanceof ServletContext) {
			ServletContext servletContext = (ServletContext)arg;
			notifyTemplateFolder = servletContext.getRealPath("WEB-INF/xmlConfig/web/NoticeTemplate");
			log.debug("notifyTemplateFolder: {}", notifyTemplateFolder);
			File f = new File(notifyTemplateFolder);
			f.setWritable(true,true);
			f.setReadable(true, true);
			f.setExecutable(true,true);
			if(!f.exists())
				f.mkdir();
		}
	}

	public static boolean sendNotice(String tmplKey, Map params, String recivers) {
		return sendNotice(tmplKey, params, recivers, new NotifyListenerAdapter());
	}
	/*  2008/10/07 LCC 桑
	Dear all,
	  
	   昨日已經確認即時與預約轉帳 email 發送對象的規則如下:
	 
	1.          針對所有轉帳條件輸入網頁修改: 
	a.將"交易成功 Email 通知" 欄位提示詞修改為"轉出成功 Email 通知"
	 
	b.若使用者(付款者)設定[我的email]，且於通知服務中有約定”轉帳/繳費稅成功通知”，在"轉出成功 Email 通知"欄位中顯示兩個項目>>   通知本人:[我的email內容] 
	            另通知  :[                                            ] 通訊錄 
	  
	　通知本人的內容不可修改，另通知的對象由使用者自行指定。
	  於即時交易轉出成功時，發出成功通知 Email給上述兩個欄位的 email address
	 
	c.若使用者(付款者)沒有設定[我的email]，或於通知服務中沒有約定”轉帳/繳費稅成功通知”，在"轉出成功 Email 通知"欄位中顯示一個項目>>   電子信箱: [                                            ] 通訊錄 
	   
	　電子信箱的對象由使用者自行指定。
	  於即時交易轉出成功時，發出成功通知 Email給電子信箱欄位內的所有email address
	 
	d.即時交易轉出失敗一律不發送通知
	 
	e.同一封交易結果通知 Email ，不需要重複通知相同的收件者
	 
	2.     針對預約轉帳到期,轉帳後的通知方式如下
	　　ａ.若預約付款人有設定[我的email]，且於通知服務中有約定”預約交易結果通知"，需要發送交易結果（成功或失敗）通知 Email 到該使用者設定的 [我的email]中
	　　
	     b.若預約付款人沒有設定[我的email]，或通知服務中沒有約定”預約交易結果通知"，不需要發送交易結果（成功或失敗）通知 Email 給該使用者
	 
	　　ｃ.若轉出成功，需要再發送交易結果通知 Email 到該使用者約定於"轉出成功 Email 通知"欄位的所有email address
	 
	　　 d.若轉出失敗，不需要再發送交易結果通知 Email 給其他人
	 
	  e.同一封交易結果通知 Email ，不需要重複通知相同的收件者 
	 
	 */
	public static boolean sendNotice(String tmplKey, Map params, String recivers, NotifyListener listener) {
		
		if(StrUtils.isEmpty(recivers))
			return true;
		
		// 即時轉帳失敗不通知, 成功則通知 (通知畫面上所設定的)
		// 預約時,  成功通知(通知畫面上所設定的), USER 設定"轉出交易"為不收到, 則看 畫面上的欄位, 若有設定, 則畫面加自己都發送
		// 預約時, 看預約結果通知是否設定, 若有設定, 則失敗通知, 不看畫面
		
		//if(tmplPool.get(tmplKey) == null) {
			File f = new File(notifyTemplateFolder, tmplKey.toUpperCase() + POSTFIX);
			f.setWritable(true,true);
			f.setReadable(true, true);
			f.setExecutable(true,true);
			MessageTemplate template = new MessageTemplate(f);
			//tmplPool.put(tmplKey, template);
		//}
		Notifier notifier = (Notifier)SpringBeanFactory.getBean("notifier");
		//MessageTO message = new MessageTO(tmplPool.get(tmplKey), params);
		MessageTO message = new MessageTO(template, params);
		
		log.debug(ESAPIUtil.vaildLog("@@@ sendNotice message.getSubject() == "+message.getSubject()));
		
		return notifier.sendmsg(recivers, message.getSubject(), message.getMsgTemplate().buildContent(params), listener);
	}
	
	public static boolean sendNotice(String tmplKey, Map params, String recivers, String ProName) {
		return sendNotice(tmplKey, params, recivers, new NotifyListenerAdapter(), ProName);
	}

	public static boolean sendNotice(String tmplKey, Map params, String recivers, NotifyListener listener, String ProName) {
		
		if(StrUtils.isEmpty(recivers))
			return true;
		
		// 即時轉帳失敗不通知, 成功則通知 (通知畫面上所設定的)
		// 預約時,  成功通知(通知畫面上所設定的), USER 設定"轉出交易"為不收到, 則看 畫面上的欄位, 若有設定, 則畫面加自己都發送
		// 預約時, 看預約結果通知是否設定, 若有設定, 則失敗通知, 不看畫面
		
		//if(tmplPool.get(tmplKey) == null) {
			File f = new File(notifyTemplateFolder, tmplKey.toUpperCase() + POSTFIX);
			f.setWritable(true,true);
			f.setReadable(true, true);
			f.setExecutable(true,true);
			MessageTemplate template = new MessageTemplate(f);
			//tmplPool.put(tmplKey, template);
		//}
		Notifier notifier = (Notifier)SpringBeanFactory.getBean("notifier");
		//MessageTO message = new MessageTO(tmplPool.get(tmplKey), params);
		MessageTO message = new MessageTO(template, params);
		
		return notifier.sendmsg(recivers, message.getSubject(), message.getMsgTemplate().buildContent(params), listener, ProName);
	}
	
}
