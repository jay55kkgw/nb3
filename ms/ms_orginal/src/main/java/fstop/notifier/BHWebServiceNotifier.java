package fstop.notifier;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;

import com.netbank.util.ESAPIUtil;

import fstop.util.StrUtils;
import fstop.util.thread.ThreadSemaphore;
import fstop.ws.bh.client2.APISoap;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BHWebServiceNotifier implements Notifier {
	private Logger logger = Logger.getLogger(getClass());

	@Value("${smtp.host:}")
	private String smtpHost;
	//private String smtpHost = "";
	
	@Value("${smtp.port:}")
	private String smtpPort;
//	private String smtpPort = "";
	
	@Value("${smtp.account:}")
	private String smtpAccount;
//	private String smtpAccount ="";
	
	@Value("${smtp.password:}")
	private String smtpPW; // Use Of Hardcoded Password
//	private String smtpPassword = "";
	
	@Value("${smtp.mail_from:}")
	private String smtpMail_from;
//	private String smtpMail_from="";
	
	@Value("${smtp.mail_from.batch:}")
	private String smtpMailFromBatch;
//	private String smtpMailFromBatch ="";
	
	@Value("${WebService.SendMail.URI:}")
	private String bhWebservicesUri;
//	private String bhWebservicesUri = "";
	
	@Value("${WebService.SendMail.TemplateNo:}")
	private String bhWebservicesTemplateno;
//	private String bhWebservicesTemplateno ="4";
	
	@Value("${WebService.SendMail.ProjectID:}")
	private String bhWebservicesProjectid;
//	private String bhWebservicesProjectid="50002";

	@Value("${WebService.SendMail.SenderMail:}")
	private String bhWebservicesSendermail;
//	private String bhWebservicesSendermail="eatm@mail.tbb.com.tw";
	
	@Value("${WebService.SendMail.SenderMail.Batch:}")
	private String bhWebservicesSendermailBatch;
//	private String bhWebservicesSendermailBatch ="eatm@mail.tbb.com.tw";

	private String bhWebservicesMailSendername = "臺灣企銀";
	private String bhWebservicesMailSendernameBatch = "臺灣企銀";


	private String smtpMailSenderName = "臺灣企銀";

	private String smtpMailSenderNameBatch  = "臺灣企銀";

	private Map smtpSetting = new HashMap();

	@PostConstruct
	public void init() {
		smtpSetting.put("smtp.host", this.smtpHost);
		smtpSetting.put("smtp.port", this.smtpPort);
		smtpSetting.put("smtp.account", this.smtpAccount);
		smtpSetting.put("smtp.password", this.smtpPW);
		smtpSetting.put("smtp.mail_from", this.smtpMail_from);
		smtpSetting.put("smtp.mail_from.batch", this.smtpMailFromBatch);
		smtpSetting.put("smtp.mail_sendername", this.smtpMailSenderName);
		smtpSetting.put("smtp.mail_sendername.batch", this.smtpMailSenderNameBatch);

		smtpSetting.put("bh.webservices.uri", this.bhWebservicesUri);
		smtpSetting.put("bh.webservices.templateno", this.bhWebservicesTemplateno);
		smtpSetting.put("bh.webservices.projectid", this.bhWebservicesProjectid);
		smtpSetting.put("bh.webservices.sendermail", this.bhWebservicesSendermail);
		smtpSetting.put("bh.webservices.sendermail.batch", this.bhWebservicesSendermailBatch);
		smtpSetting.put("bh.webservices.mail_sendername", this.bhWebservicesMailSendername);
		smtpSetting.put("bh.webservices.mail_sendername.batch", this.bhWebservicesMailSendernameBatch);
	}



	private static <T> T createService(String serviceUrl, Class<T> clz) {
		JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
		factory.getInInterceptors().add(new LoggingInInterceptor());
		factory.getOutInterceptors().add(new LoggingOutInterceptor());
		
		factory.setServiceClass(clz);

		factory.setAddress(serviceUrl);

		T service = (T) factory.create();
		Client client = ClientProxy.getClient(service);

		HTTPConduit conduit = (HTTPConduit) client.getConduit();
		HTTPClientPolicy policy = conduit.getClient();
		policy.setReceiveTimeout(1 * 30 * 1000);

		conduit.setClient(policy);

		return service;
	}


	public boolean sendmsg(String recevier, String subject, String msgcontent, NotifyListener listener) {
		return sendmsgCC(recevier, null , subject, msgcontent, listener);
	}


	public boolean sendmsgCC(final String receviers, String ccReceviers, final String subject, final String msgcontent, final NotifyListener listener) {

		boolean result = false;
		try {
			
			String uri = StrUtils.trim((String)smtpSetting.get("bh.webservices.uri"));
			String templateno = StrUtils.trim((String)smtpSetting.get("bh.webservices.templateno"));
			String projectid = StrUtils.trim((String)smtpSetting.get("bh.webservices.projectid"));
			final String senderMail = StrUtils.trim((String)smtpSetting.get("bh.webservices.sendermail"));
			final String senderName = StrUtils.trim((String)smtpSetting.get("bh.webservices.mail_sendername"));
			
			logger.debug("senderName = " + senderName);
			log.debug("@@@ uri =="+uri);
			log.debug("@@@ templateno =="+templateno);
			log.debug("@@@ projectid =="+projectid);
			log.debug("@@@ senderMail =="+senderMail);
			log.debug("@@@ senderName =="+senderName);
			
			final StringBuffer mailto = new StringBuffer();
			if(StrUtils.isNotEmpty(receviers)) {
				String sendto = receviers.replaceAll(";", ",");
				String[] r = sendto.split(",");
				mailto.append("<Mail_Tos>");
				for(String ss : r) {
					mailto.append("<Mail_To><Name></Name><Email>" + ss.trim() + "</Email></Mail_To>");
				}
				mailto.append("</Mail_Tos>");
			}
			else {
				String sendto = ccReceviers.replaceAll(";", ",");
				String[] r = sendto.split(",");
				mailto.append("<Mail_Tos>");
				for(String ss : r) {
					mailto.append("<Mail_To><Name></Name><Email>" + ss.trim() + "</Email></Mail_To>");
				}
				mailto.append("</Mail_Tos>");
			}
			
			log.info("MAIL TO >>" + mailto.toString());
			log.info(ESAPIUtil.vaildLog("Subject  >>" + subject ));
//			APISoap soap = this.createService(uri, APISoap.class);
			 
			final int iTemplateno = new Integer(templateno);
			final int iProjectid = new Integer(projectid);
			final String replyEmail = "";
			ThreadSemaphore threadSemaphore = null;

			try {
				threadSemaphore = new ThreadSemaphore(1);
				Runnable run = new Runnable() {
					public void run() {
						APISoap soap = BHWebServiceNotifier.createService(uri, APISoap.class);
						String response = "";
						try {
							
							logger.debug("BHWebService before soap.sendMail_Now_Custom()");
							 
							response = soap.sendMailNowCustom(
									mailto.toString(), iTemplateno, iProjectid, 7, "<Variables><Variable><Key>CONTENT</Key><Value>" + htmlEntityEncode(msgcontent) + "</Value></Variable></Variables>", "",
									senderName,
									senderMail,
									subject,
									replyEmail); 


							
							//logger.debug("Soap:"+"<Variables><Variable><Key>CONTENT</Key><Value>" + htmlEntityEncode(msgcontent) + "</Value></Variable></Variables>");
							logger.debug("BHWebService after soap.sendMail_Now_Custom()");

							if(listener != null) {

								if("BH-0000".equalsIgnoreCase(response))
								{
									listener.success(receviers, subject, msgcontent);
								}
								else
								{
									listener.fail(receviers, subject, msgcontent, new RuntimeException("response : " + response));
								}
							}

							logger.debug("BHWebService response code == "+response);
						}
						catch(Exception e) {
							logger.error("BHWebService Runnable run() 發生錯誤 ..", e);
							listener.fail(receviers, subject, msgcontent, new RuntimeException("response : " + response));
						}
					}
				};

				threadSemaphore.execute(run);
			}
			catch(Exception e) {
				logger.error("BHWebService ThreadSemaphore 發生錯誤 ..", e);
			}
			finally {
				try {
					threadSemaphore.shutdown();

					logger.error("BHWebService : threadSemaphore.shutdown()");
				}
				catch(Exception e){}
			}

			result = true;
		}
		catch(Exception e) {
			logger.error("無法發送 Email.", e);
			if(listener != null)
				listener.fail(receviers, subject, msgcontent, e);
		}
		return result;
	}

	public String htmlEntityEncode( String s )
	{
		StringBuffer buf = new StringBuffer();
		int len = (s == null ? -1 : s.length());

		for ( int i = 0; i < len; i++ )
		{
			char c = s.charAt( i );
			if ( c>='a' && c<='z' || c>='A' && c<='Z' || c>='0' && c<='9' )
			{
				buf.append( c );
			}
			else
			{
				buf.append( "&#" + (int)c + ";" );
			}
		}
		return buf.toString();
	}

	public boolean sendmsg(String recevier, String subject, String msgcontent, NotifyListener listener, String ProName) {
		return sendmsgCC(recevier, null, subject, msgcontent, listener, ProName);
	}


	public boolean sendmsgCC(String receviers, String ccReceviers, String subject, String msgcontent, NotifyListener listener, String ProName) {

		boolean result = false;
		try {

			String uri = StrUtils.trim((String)smtpSetting.get("bh.webservices.uri"));
			String templateno = StrUtils.trim((String)smtpSetting.get("bh.webservices.templateno"));
			String projectid = StrUtils.trim((String)smtpSetting.get("bh.webservices.projectid"));
			String senderMail = StrUtils.trim((String)smtpSetting.get("bh.webservices.sendermail." + ProName));
			String senderName = StrUtils.trim((String)smtpSetting.get("bh.webservices.mail_sendername." + ProName));

			StringBuffer mailto = new StringBuffer();
			if(StrUtils.isNotEmpty(receviers)) {
				String sendto=receviers.replaceAll(";", ",");
				String[] r = sendto.split(",");
				mailto.append("<Mail_Tos>");
				for(String ss : r) {
					mailto.append("<Mail_To><Name></Name><Email>" + ss.trim() + "</Email></Mail_To>");
				}
				mailto.append("</Mail_Tos>");
			}

			APISoap soap = this.createService(uri, APISoap.class);

			int iTemplateno = new Integer(templateno);
			int iProjectid = new Integer(projectid);

			String replyEmail = "";
			
			String response = "";
			
			response = soap.sendMailNowCustom( 
					mailto.toString(), iTemplateno, iProjectid, 7, "<Variables><Variable><Key>CONTENT</Key><Value>" + htmlEntityEncode(msgcontent) + "</Value></Variable></Variables>", "",
					senderName,
					senderMail,
					subject,
					replyEmail);

			logger.debug("test timeout c");

			if(listener != null) {
				logger.debug("test timeout d");
				if("BH-0000".equalsIgnoreCase(response))
				{
					logger.debug("test timeout e");
					listener.success(receviers, subject, msgcontent);
					logger.debug("test timeout f");
				}
				else
				{
					logger.debug("test timeout g");
					listener.fail(receviers, subject, msgcontent, new RuntimeException("response : " + response));
					logger.debug("test timeout h");
				}
			}

			logger.debug("test timeout無錯誤.");

			result = true;
		}
		catch(Exception e) {
			logger.error("無法發送 Email.", e);
			if(listener != null)
				listener.fail(receviers, subject, msgcontent, e);
		}
		return result;
	}
}
