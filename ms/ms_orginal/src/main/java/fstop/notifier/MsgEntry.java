package fstop.notifier;

public class MsgEntry {
	public static enum ENTRYTYPE   {PARAM, TEXT};
	
	private ENTRYTYPE type;
	private String text;
	
	public MsgEntry(ENTRYTYPE type, String text) {
		this.type = type;
		this.text = text;
	}
	
	public ENTRYTYPE getType() {
		return type;
	}
	
	public String getText() {
		return text;
	}
}
