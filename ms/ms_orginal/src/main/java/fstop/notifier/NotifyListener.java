package fstop.notifier;

public interface NotifyListener {
	public void success(String reciver, String subject, String msg);
	
	public void fail(String reciver, String subject, String msg, Exception e);
}
