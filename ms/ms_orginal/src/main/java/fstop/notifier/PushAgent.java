package fstop.notifier;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.eclipse.paho.client.mqttv3.MqttException;

import com.netbank.rest.comm.batch.FirebaseBean;
import com.netbank.util.ESAPIUtil;

import fstop.firebase.FirebaseClient;
import fstop.notification.Notify;
import fstop.notification.PushData;
import fstop.notification.TopicLabel;
import fstop.orm.dao.TxnPhoneTokenDao;
import fstop.orm.po.TXNPHONETOKEN;
import fstop.util.SpringBeanFactory;
import fstop.util.thread.ThreadSemaphore;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PushAgent {

	public static FirebaseBean firebaseValue() {
		return (FirebaseBean) SpringBeanFactory.getBean("firebaseBean");
	}
	

	private static PushAgent instance = null;

	public static PushAgent getInstance() {
		if (instance == null) {
			synchronized (PushAgent.class) {
				if (instance == null) {
					instance = new PushAgent();
				}
			}
		}
		return instance;
	}

	public static void sendNotice(final String Uid, final String Useracn, final String Txname, final String Txntime) {
		boolean result = false;

		ThreadSemaphore threadSemaphore = null;
		try {
			threadSemaphore = new ThreadSemaphore(1);
			Runnable run = new Runnable() {
				public void run() {
					try {
						log.info(ESAPIUtil.vaildLog("dosendNotice before"));
						dosendNotice(Uid, Useracn, Txname, Txntime);
						log.info(ESAPIUtil.vaildLog("dosendNotice after"));
					} catch (Exception e) {
						log.error("sendNotice Runnable run() error");
					}
				}
			};
			threadSemaphore.execute(run);
		} catch (Exception e) {
			log.error("sendNotice ThreadSemaphore error");
		} finally {
			try {
				threadSemaphore.shutdown();
				log.info("sendNotice threadSemaphore.shutdown()");
			} catch (Exception e) {
			}
		}
	}

	private static boolean dosendNotice(String Uid, String Useracn, String Txname, String Txntime) {
		boolean result = false;

		TxnPhoneTokenDao txnPhoneTokenDao = (TxnPhoneTokenDao) SpringBeanFactory.getBean("txnPhoneTokenDao");
		TXNPHONETOKEN txnphonetoken = new TXNPHONETOKEN();

		txnphonetoken = txnPhoneTokenDao.getPhoneToken(Uid);
		String token = "";
		if (txnphonetoken != null)
			token = txnphonetoken.getPHONETOKEN();

		if (!token.equals("") && !Txname.equals("")) {
			PushData pushData = new PushData();
			pushData.setMessage("臺灣企銀提醒您，您有一筆" + Txname + "交易(帳號:" + Useracn + ")，交易時間：" + Txntime
					+ "。本通知僅供參考，不作為交易憑證，實際交易結果請至本行帳戶查詢！");
			pushData.setTopic(TopicLabel.TOPICLABEL_ID + token);
			pushData.setCusId(Uid);

//			Notify notify = (Notify) SpringBeanFactory.getBean("pushNotifier");

			if(firebaseValue().fbEnv.getFirebase_useflag().equals("Y")) {

				try {
					FirebaseClient.sendMessage(pushData);
					result = true;

				} catch (Exception e) {
//					e.printStackTrace();
					log.error("Exception : {}",e.getMessage());
					result = false;
				}
			}else {
				Notify notify = new Notify();
				try {
					notify.publish(pushData);
					result = true;
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
					result = false;
				} catch (MqttException e) {
					e.printStackTrace();
					result = false;
				}
			}
		} else {
			result = false;
		}

		return result;
	}

	public static void sendNotice(final List<PushData> listPushData) {
		boolean result = false;

		ThreadSemaphore threadSemaphore = null;
		try {
			threadSemaphore = new ThreadSemaphore(1);
			Runnable run = new Runnable() {
				public void run() {
					try {
						log.info(ESAPIUtil.vaildLog("dosendNotice(listPushData) before"));
						dosendNotice(listPushData);
						log.info(ESAPIUtil.vaildLog("dosendNotice(listPushData) after"));
					} catch (Exception e) {
						log.error("sendNotice(listPushData) Runnable run() error");
					}
				}
			};
			threadSemaphore.execute(run);
		} catch (Exception e) {
			log.error("sendNotice(listPushData) ThreadSemaphore error");
		} finally {
			try {
				threadSemaphore.shutdown();
				log.info("sendNotice(listPushData) threadSemaphore.shutdown()");
			} catch (Exception e) {
			}
		}
	}

	private static boolean dosendNotice(List<PushData> listPushData) {

		boolean result = true;
		try {
			if (listPushData.size() > 0) {
				if(firebaseValue().fbEnv.getFirebase_useflag().equals("Y")) {

					try {
						FirebaseClient.sendMessage(listPushData);
						result = true;

					} catch (Exception e) {
//						e.printStackTrace();
						log.error("Exception : {}",e.getMessage());
						result = false;
					}
				}else {
//					Notify notify = (Notify) SpringBeanFactory.getBean("notifier");
					 Notify notify = new Notify();
					try {
						notify.publish(listPushData);
						result = true;
					} catch (UnsupportedEncodingException e) {
//						e.printStackTrace();
						log.error("Exception : {}",e.getMessage());
						result = false;
					} catch (MqttException e) {
//						e.printStackTrace();
						log.error("Exception : {}",e.getMessage());
						result = false;
					}
				}
			}
		} catch (Exception e) {
			log.error("Exception : {}",e.getMessage());
//			e.printStackTrace();
		}
		return result;
	}
	
	public static void sendNotice(final List<PushData> listPushData,String phonetype) {
		boolean result = false;

		ThreadSemaphore threadSemaphore = null;
		try {
			threadSemaphore = new ThreadSemaphore(1);
			Runnable run = new Runnable() {
				public void run() {
					try {
						log.info(ESAPIUtil.vaildLog("dosendNotice(listPushData) before"));
						dosendNotice(listPushData,phonetype);
						log.info(ESAPIUtil.vaildLog("dosendNotice(listPushData) after"));
					} catch (Exception e) {
						log.error("sendNotice(listPushData) Runnable run() error");
					}
				}
			};
			threadSemaphore.execute(run);
		} catch (Exception e) {
			log.error("sendNotice(listPushData) ThreadSemaphore error");
		} finally {
			try {
				threadSemaphore.shutdown();
				log.info("sendNotice(listPushData) threadSemaphore.shutdown()");
			} catch (Exception e) {
			}
		}
	}

	private static boolean dosendNotice(List<PushData> listPushData , String phonetype) {

		boolean result = true;
		try {
			if (listPushData.size() > 0) {
				if(firebaseValue().fbEnv.getFirebase_useflag().equals("Y")) {

					try {
						if ("I".equals(phonetype)) {
							FirebaseClient.sendMessageForI(listPushData);
							result = true;
						}else {
							FirebaseClient.sendMessageForA(listPushData);
							result = true;
						}
					} catch (Exception e) {
//						e.printStackTrace();
						log.error("Exception : {}",e.getMessage());
						result = false;
					}
				}else {
//					Notify notify = (Notify) SpringBeanFactory.getBean("notifier");
					 Notify notify = new Notify();
					try {
						notify.publish(listPushData);
						result = true;
					} catch (UnsupportedEncodingException e) {
//						e.printStackTrace();
						log.error("Exception : {}",e.getMessage());
						result = false;
					} catch (MqttException e) {
//						e.printStackTrace();
						log.error("Exception : {}",e.getMessage());
						result = false;
					}
				}
			}
		} catch (Exception e) {
			log.error("Exception : {}",e.getMessage());
//			e.printStackTrace();
		}
		return result;
	}

}
