package fstop.notifier;

public interface Notifier {
	

	/**
	 * 
	 * @param receviers 收件者清單
	 * @param subject 郵件主旨
	 * @param msgcontent 郵件內容
	 * @param listener 
	 * @return
	 */
	public boolean sendmsg(String receviers, String subject, String msgcontent, NotifyListener listener);
	public boolean sendmsgCC(String receviers, String ccReceviers, String subject, String msgcontent, NotifyListener listener);
	
	public boolean sendmsg(String receviers, String subject, String msgcontent, NotifyListener listener, String ProName);
	public boolean sendmsgCC(String receviers, String ccReceviers, String subject, String msgcontent, NotifyListener listener, String ProName);
}
