package fstop.notifier;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import com.netbank.rest.comm.batch.BhEnvBean;
import com.netbank.rest.e2e.E2ETransform;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.exception.MsgTemplateNotFoundException;
import fstop.exception.ToRuntimeException;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;


@Slf4j
public class MessageTemplate {
	
	public static BhEnvBean bHEnv() {
		return (BhEnvBean) SpringBeanFactory.getBean("bhEnvBean");
		
	}
	private List<String> contentContentKeys = new ArrayList();

	private List<MsgEntry> contentMsgEntries = new ArrayList();
	
	private List<String> subjectContentKeys = new ArrayList();
	
	private List<MsgEntry> subjectMsgEntries = new ArrayList();
	
	
	public MessageTemplate(File tmpl) {
		tmpl.setWritable(true,true);
		tmpl.setReadable(true, true);
		tmpl.setExecutable(true,true);
		build(tmpl);
	}
	
	public MessageTemplate(InputStream inputStream) {
		
		build(inputStream);
		
	}
	
	
	private void build(File tmplFile) {
		tmplFile.setWritable(true,true);
		tmplFile.setReadable(true, true);
		tmplFile.setExecutable(true,true);
		FileInputStream in = null;
		try {
			in = new FileInputStream(tmplFile);
			build(in);
		}
		catch(FileNotFoundException e) {
			throw new MsgTemplateNotFoundException(e.getMessage(), e);
		}
		finally {
			try {
				in.close();
			}
			catch(Exception e){}
		}
	}

	private void build(InputStream inputStream) {
//		StringWriter sw = new StringWriter();
		String subject = "";
		String content = "";
		InputStreamReader in = null;
		BufferedReader bin = null;
		
		try {
			in = new InputStreamReader(inputStream, "BIG5");
			bin = new BufferedReader(in);
			subject = bin.readLine();
			log.debug("txnMail_env >> "+bHEnv().bHEnv.getTxnMail_env());
			switch (bHEnv().bHEnv.getTxnMail_env()) {
			case "D":
				subject = "[新世代網路銀行 開發環境] " + subject;
				break;
			case "T":
				subject = "[新世代網路銀行 測試環境] " + subject;	
				break;
			case "P":
				//20210706 MB需求 , 行動銀行之信件 , 不顯示新世代網路銀行
				//確認過, 所有 %_MB%.html 都有行動銀行之字樣 , 故若找到此字樣 , 不修改subject 
				if(subject.indexOf("行動銀行")>0) {
				//有找到 , 不動	
					break;
				}else {
					//找不到則照原來修改	
					//subject 有4種 >>  "臺灣企銀網路銀行"  "網路銀行"  "臺灣企銀"  無顯示
					// ====無顯示 判別=====
					if (subject.indexOf("臺灣企銀") < 0 && subject.indexOf("網路銀行") <0) {
						subject = "臺灣企銀 新世代網路銀行 " + subject;
						// ====臺灣企銀網路銀行 判別====
					} else if (subject.indexOf("臺灣企銀網路銀行") >= 0) {
						subject = subject.replace("臺灣企銀網路銀行", "臺灣企銀 新世代網路銀行 ");
						// ====網路銀行 判別====
					} else if (subject.indexOf("臺灣企銀") < 0 && subject.indexOf("網路銀行") >= 0) {
						subject = subject.replace("網路銀行", "臺灣企銀 新世代網路銀行 ");
						// ====臺灣企銀 判別====
					} else if (subject.indexOf("臺灣企銀") >= 0 && subject.indexOf("網路銀行") < 0) {
						subject = subject.replace("臺灣企銀", "臺灣企銀 新世代網路銀行 ");
					}
					break;
				}
			}
			
			String s = new String();
			
		    while((s = ESAPIUtil.validInputII(bin.readLine(),"GeneralString", true))!= null) {
//		    while((s= bin.readLine())!= null) {
		    	content += s + "\n";
		    }
		    log.debug("CONTENT >> {} ", content);
		}
		catch(IOException e) {
			throw new ToRuntimeException("讀取 template 檔錯誤 (By InputStream).", e);
		}
		finally {
			try {
				in.close();
			} catch(Exception e){}
			in = null;
		}
		
		parser(subject, subjectContentKeys, subjectMsgEntries);
		parser(content, contentContentKeys, contentMsgEntries);
		
		content = null;
	}

	private void parser(String content, List<String> contentKeys, List<MsgEntry> msgEntries)  {
		StringBuffer lastString = new StringBuffer(512);
		try {
			Pattern regex = Pattern.compile("%([^%]+?)%", Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
			Matcher regexMatcher = regex.matcher(content);
			while (regexMatcher.find()) {
				try {
					String contentKey = regexMatcher.group(1);
					
					contentKeys.add(contentKey);
					

					lastString.setLength(0);
					regexMatcher.appendReplacement(lastString, "");
					msgEntries.add(new MsgEntry(MsgEntry.ENTRYTYPE.TEXT, lastString.toString()));
					msgEntries.add(new MsgEntry(MsgEntry.ENTRYTYPE.PARAM, contentKey));
					
				} catch (IllegalStateException ex) {
					log.error(" parser IllegalStateException");
				} catch (IllegalArgumentException ex) {
					log.error(" parser IllegalArgumentException");
				} catch (IndexOutOfBoundsException ex) {
					log.error(" parser IndexOutOfBoundsException");
				} 
			}
			lastString.setLength(0);
			regexMatcher.appendTail(lastString);
			msgEntries.add(new MsgEntry(MsgEntry.ENTRYTYPE.TEXT, lastString.toString()));
		} catch (PatternSyntaxException ex) {
			
		}
	}
	
	public String getSubject(Map params) {
		StringBuffer msg = new StringBuffer();

		for(MsgEntry ent : getSubjectMsgEntries()) {
			if(ent.getType() == MsgEntry.ENTRYTYPE.PARAM) {
				msg.append(StrUtils.trim((String)params.get(ent.getText())));
			}
			else if(ent.getType() == MsgEntry.ENTRYTYPE.TEXT) {
				msg.append(ent.getText());
			}
		}
		return msg.toString();
	}

	public List<MsgEntry> getSubjectMsgEntries() {
		return subjectMsgEntries;
	}

	public List<String> getSubjectContentKeys() {
		return subjectContentKeys;
	}
	
	public List<MsgEntry> getMsgEntries() {
		return contentMsgEntries;
	}

	public List<String> getContentKeys() {
		return contentContentKeys;
	}

	public String buildContent(Map params) {
		StringBuffer msg = new StringBuffer();
		log.debug("buildContent param >> {} " , ESAPIUtil.vaildLog(CodeUtil.toJson(params)));
		for(MsgEntry ent : getMsgEntries()) {
			if(ent.getType() == MsgEntry.ENTRYTYPE.PARAM) {
/*
				StringBuffer	sb = new StringBuffer();
				String temp=StrUtils.trim((String)params.get(ent.getText()));
				for (int i = temp.indexOf("'"); i > 0;)
				{
					sb.append(temp.substring(0, i));
					sb.append("&#039");		// 1 個 單引號
					temp = temp.substring(i + 1);
					i = temp.indexOf("'");
				}
				sb.append(temp);
//				msg.append(StrUtils.trim((String)params.get(ent.getText())));
				msg.append(StrUtils.trim(sb.toString()));
*/				
				msg.append(StrUtils.trim((String)params.get(ent.getText())).replaceAll("'", "&#039"));
			}
			else if(ent.getType() == MsgEntry.ENTRYTYPE.TEXT) {
				msg.append(ent.getText());
			}
		}
		return msg.toString();
	}
	
	public static void main(String[] args) {
//		String subject = "臺灣企銀網路銀行交易通知" ;
//		String subject = "基金自動贖回通知";
//		String subject = "網路銀行生日推播批次作業結果通知";
//		String subject ="臺灣企銀BANK3.0線上申請統計";
//		String subject ="臺灣企銀 新世代網路銀行 登入成功通知";
		String subject = "XXXX檔案異動";
		
		if (subject.indexOf("臺灣企銀") < 0 && subject.indexOf("網路銀行") <0) {
			subject = "臺灣企銀 新世代網路銀行 " + subject;
			// ====臺灣企銀網路銀行 判別====
		} else if (subject.indexOf("臺灣企銀網路銀行") >= 0) {
			subject = subject.replace("臺灣企銀網路銀行", "臺灣企銀 新世代網路銀行 ");
			// ====網路銀行 判別====
		} else if (subject.indexOf("臺灣企銀") < 0 && subject.indexOf("網路銀行") >= 0) {
			subject = subject.replace("網路銀行", "臺灣企銀 新世代網路銀行 ");
			// ====臺灣企銀 判別====
		} else if (subject.indexOf("臺灣企銀") >= 0 && subject.indexOf("網路銀行") < 0) {
			subject = subject.replace("臺灣企銀", "臺灣企銀 新世代網路銀行 ");
		}
		System.out.println(subject);
		
	}



}
