package fstop.notifier;

import java.util.Map;

import com.netbank.util.ESAPIUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MessageTO {

	private MessageTemplate msgTemplate;
	private Map params;
	private String subject;
	private String content;
	
	public MessageTO(MessageTemplate template, Map params) {
		this.params = params;
		this.subject = template.getSubject(params);
		this.msgTemplate = template;
		this.content = template.buildContent(params);

		log.debug(ESAPIUtil.vaildLog("@@@ MessageTO params == "+params));				
		log.debug(ESAPIUtil.vaildLog("@@@ MessageTO this.subject == "+this.subject));		
	}
	
	public MessageTemplate getMsgTemplate() {
		return msgTemplate;
	}
	public void setMsgTemplate(MessageTemplate msgTemplate) {
		this.msgTemplate = msgTemplate;
	}
	public Map getParams() {
		return params;
	}
	public void setParams(Map params) {
		this.params = params;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
}
