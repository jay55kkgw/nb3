package fstop.services;

import fstop.model.TelcommResult;
import fstop.orm.po.ADMMSGCODE;

public interface TopMsgDetecter {

	public ADMMSGCODE isError(String topmsg, TelcommResult telcommResult);

}

