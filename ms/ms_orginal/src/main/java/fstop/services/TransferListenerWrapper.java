package fstop.services;

import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import fstop.model.MVHImpl;


public class TransferListenerWrapper implements Observer, TransferListener {
	private TransferListener listener;
	
	public TransferListener getListener() {
		return listener;
	}

	public TransferListenerWrapper(TransferListener listener) {
		this.listener = listener;
	}
	
	public void before(Map params) {
		listener.before(params);
	}
	
	public void doSuccess(){
		listener.doSuccess();
	}
	
	public void doFail(){
		listener.doFail();
	}
	
	public void doFinally(MVHImpl result){
		listener.doFinally(result);
	}
	
	public void update(Observable o, Object arg) {
		Object[] args = (Object[])arg;
		if("before".equalsIgnoreCase((String)args[0]))
			before((Map)args[1]);
		if("doFinally".equalsIgnoreCase((String)args[0]))
			doFinally((MVHImpl)args[1]);
		if("doFail".equalsIgnoreCase((String)args[0]))
			doFail();
		if("doSuccess".equalsIgnoreCase((String)args[0]))
			doSuccess();
	}
	
	public boolean equals(Object o) {
		boolean result = false;
		if(this == o)
			return true;
		if(listener != null) {
			if(o instanceof TransferListenerWrapper) {
				if(listener == ((TransferListenerWrapper)o).getListener())
					return true;
			}
		}
		return false;
	}
	
	public int hashCode() {
		if(listener != null)
			return listener.hashCode();
		else {
			return 0;
		}
	}
}
