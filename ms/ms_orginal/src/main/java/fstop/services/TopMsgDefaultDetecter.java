package fstop.services;

import fstop.exception.ToRuntimeException;
import fstop.model.TelcommResult;
import fstop.orm.dao.AdmMsgCodeDao;
import fstop.orm.po.ADMMSGCODE;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.util.HashSet;
import java.util.Set;

public class TopMsgDefaultDetecter implements TopMsgDetecter {
	private Logger logger = Logger.getLogger(getClass());

	static Set successTopMsg = new HashSet();
	
	static {
		successTopMsg.add("0000");
		successTopMsg.add("OKOK");
		successTopMsg.add("OKLR");
		successTopMsg.add("OKOV");
		successTopMsg.add("R000");
		successTopMsg.add("FPS3041");		
		successTopMsg.add("99");
	}
	
	
	private AdmMsgCodeDao admMsgCodeDao;

	@Required
	public void setAdmMsgCodeDao(AdmMsgCodeDao admMsgCodeDao) {
		this.admMsgCodeDao = admMsgCodeDao;
	}

	public ADMMSGCODE isError(String topmsg, TelcommResult telcommResult) {
		ADMMSGCODE result = null;
		try {
			if(topmsg != null && topmsg.length() == 0)
				return null;
			
			if(TopMsgDefaultDetecter.successTopMsg.contains(topmsg))
				return null;
			
			result = admMsgCodeDao.isError(topmsg);
			
			if(result != null) {
				return result;
			}
			
		}
		catch(Exception ex) {
			logger.error(ex);
			throw new ToRuntimeException("無法判斷錯誤訊息.", ex);
		}
		result = new ADMMSGCODE();
		result.setADMCODE(topmsg);
		return result;
	}

}
