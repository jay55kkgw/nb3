package fstop.services;

import fstop.model.MVH;

import java.util.Map;


public interface ParamDecorator {
	public void before(Map m);
	public void after(Map orgparams, Map params, MVH imaster);
}
