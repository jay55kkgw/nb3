package fstop.services;

import fstop.model.TelcommResult;
import fstop.orm.dao.AdmMsgCodeDao;
import fstop.orm.po.ADMMSGCODE;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

@Slf4j
public class TopMsgFXDetecter implements TopMsgDetecter {
//	private Logger log = Logger.getLogger(getClass());
	
	@Autowired
	private AdmMsgCodeDao admMsgCodeDao;

	public ADMMSGCODE isError(String topmsg, TelcommResult telcommResult) {

		if((! StrUtils.trim(topmsg).equals("")) && (StrUtils.trim(topmsg).replaceAll("0", "").length() == 0))
			return null;
		
		if(StrUtils.trim(topmsg).equalsIgnoreCase("MVA0000"))
			return null;
		
		if (StrUtils.trim(topmsg).length() < 4)
			topmsg = "Z010";     // 中心電文回應內容異常!!!
		
		//若是 MERVA 的回傳, 則第三碼之後
		if(StrUtils.trim(topmsg).startsWith("MVA")) {
			try {
				ADMMSGCODE msg = admMsgCodeDao.isError(topmsg.substring(3));
				return msg;
			}
			catch(Exception e) {}
		}
		
		try {
			ADMMSGCODE msg = admMsgCodeDao.isError(topmsg);
			
			if (msg == null)
				topmsg = "Z010";     // 中心電文回應內容異常!!!
			else
				return msg;
		}
		catch(Exception e) {}

		ADMMSGCODE result = new ADMMSGCODE();
		result.setADMCODE(StrUtils.trim(topmsg));
		
		return result;		
	}
	
}
