package fstop.services;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import fstop.util.JSONUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BookingUtils {
	static Set removeKeys = null;

	static {
		removeKeys = new HashSet();
		removeKeys.add("action");
		removeKeys.add("CMPASSWORD");
		removeKeys.add("N950PASSWORD");
		removeKeys.add("CMPWD");
		removeKeys.add("MAC");
		removeKeys.add("__MAC");
		removeKeys.add("__MAC64");
		//因為重送, 所以要保留 XMLCN, 跟 XMLCA
//		removeKeys.add("XMLCN");
//		removeKeys.add("XMLCA");
		removeKeys.add("PPSYNC");
		removeKeys.add("PINKEY");
		removeKeys.add("PINNEW");
		removeKeys.add("urlPath");
		removeKeys.add("LOGINTM");
		removeKeys.add("pkcs7Sign");
		removeKeys.add("__SIGN");
		removeKeys.add("__DEFINE");
		removeKeys.add("__TRANS_STATUS");
		removeKeys.add("__TRANS_RESEND_ADTXNO");
		removeKeys.add("__FXMSGSEQNO");
		removeKeys.add("__FXMSGSEQNO");
		removeKeys.add("");
	}
	
	public BookingUtils(){
	}

//	public static String requestInfo(HttpServletRequest request) {
//    	String type = request.getContentType();
//    	if(type != null && type.toLowerCase().startsWith("multipart/form-data")) {
//	    	if(WebServletUtils.getUploadFiles() != null) {
//	    		HashMap params = (HashMap)WebServletUtils.getUploadFiles().getParameters();
//	    		Map cloneParams = (Map)params.clone();
//	    		removeSensitiveKeys(cloneParams);
//	    		return JSONUtils.map2json(cloneParams);
//    		}
//	    	else {
//	    		return JSONUtils.map2json(new HashMap());
//	    	}
//    	}
//    	else {
//    		Map m = new HashMap();
//    		Enumeration enumer = request.getParameterNames();
//    		while(enumer.hasMoreElements()) {
//    			String key = (String)enumer.nextElement();
//    			m.put(key, request.getParameter(key));
//    		}
//    		removeSensitiveKeys(m);
//    		return JSONUtils.map2json(m);
//    	}
//		
//	}
	
	public static String requestInfo(Map params) {
				
//		Date d = new Date();
//		String str_CurrentDate = DateTimeUtils.format("yyyyMMdd", d);	
//		String str_CurrentTime = DateTimeUtils.format("HHmmss", d);		
//		String str_CMSDATE = null;
//		boolean b_checkFlag = true;
//		
//log.debug("@@@ BookingUtils.java params == "+params);
//
//		//台幣類
//		if (params.get("FGTXDATE") != null) {
//					
//		    if (params.get("FGTXDATE").toString().equals("2"))				
//				str_CMSDATE = ((params.get("CMDATE") != null) ? params.get("CMDATE").toString().replaceAll("/", "") : params.get("CMTRDATE").toString().replaceAll("/", ""));
//			else if (params.get("FGTXDATE").toString().equals("3")) 				
//				str_CMSDATE = params.get("CMSDATE").toString().replaceAll("/", "");		
//			else
//				b_checkFlag = false;
//		}
//		//外幣類
//		else if (params.get("FGTRDATE") != null) {
//
//			if (params.get("FGTRDATE").toString().equals("1"))				
//				str_CMSDATE = params.get("CMTRDATE").toString().replaceAll("/", "");
//			else if (params.get("FGTRDATE").toString().equals("2")) 				
//				str_CMSDATE = params.get("CMSDATE").toString().replaceAll("/", "");		
//			else
//				b_checkFlag = false;		
//		}
//		else {
//			b_checkFlag = false;		
//		}
//		
//		if (b_checkFlag) {
//			
//			if ( str_CurrentDate.equals(str_CMSDATE) || 
//				(! str_CurrentDate.equals(str_CMSDATE) && (Integer.valueOf(str_CurrentTime) > Integer.valueOf("235700"))) ) {
//					
//					throw TopMessageException.create("Z404", params.get("ADOPID").toString());
//			}			
//		}
		Map coloneParams = new HashMap<>();
		coloneParams.putAll(params);
//		Map coloneParams = (Map)params.clone();
		removeSensitiveKeys(coloneParams);
		return JSONUtils.map2json(coloneParams);
	}
	
	public static void removeSensitiveKeys(Map m) {
		for(Object rmKey : removeKeys) {
			m.remove(rmKey);
		}
	}
	
}
