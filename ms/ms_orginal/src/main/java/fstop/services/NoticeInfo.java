package fstop.services;

import java.util.HashMap;
import java.util.Map;

import fstop.orm.po.ADMMAILLOG;
import fstop.util.StrUtils;

public class NoticeInfo {
	private String dpretxstatus = "0";
	private String userMail = "";
	
	private String receiver = "";
	
	private String uid = "";
	
	private Map params = new HashMap();
	
	private String templateName = "transferNotify";
	
	private String ADTXNO = "";
	
	private ADMMAILLOG ADMMAILLOG = new ADMMAILLOG();
	
	private Throwable exception;
	
	private String txname = "";	// 交易項目
	private String txntime = "";	// 交易時間
	private String useracn = "";	// 通知者帳號

	public String getDpretxstatus() {
		return dpretxstatus;
	}

	public void setDpretxstatus(String dpretxstatus) {
		this.dpretxstatus = dpretxstatus;
	}

	public Throwable getException() {
		return exception;
	}

	public void setException(Throwable exception) {
		this.exception = exception;
	}

	public String getADTXNO() {
		return ADTXNO;
	}

	public void setADTXNO(String adtxno) {
		ADTXNO = adtxno;
	}

	public ADMMAILLOG getADMMAILLOG() {
		return ADMMAILLOG;
	}

	public void setADMMAILLOG(ADMMAILLOG admmaillog) {
		ADMMAILLOG = admmaillog;
	}

	public NoticeInfo(String uid, String userMail, String receiver){
		this.uid = StrUtils.trim(uid);
		this.userMail = StrUtils.trim(userMail);
		this.receiver = StrUtils.trim(receiver);
	}
	
	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}
	
	public boolean isProcessSuccess() {
		return (getException() == null);
	}

	
	public Map getParams() {
		return params;
	}

	public void setParams(Map params) {
		this.params = params;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getUserMail() {
		return userMail;
	}

	public void setUserMail(String userMail) {
		this.userMail = userMail;
	}

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public boolean isResend()
	{
		return(this.dpretxstatus.equals("5") ? true : false);
	}

	public String getTxname() {
		return txname;
	}

	public void setTxname(String txname) {
		this.txname = txname;
	}

	public String getUseracn() {
		return useracn;
	}

	public void setUseracn(String useracn) {
		this.useracn = useracn;
	}

	public String getTxntime() {
		return txntime;
	}

	public void setTxntime(String txntime) {
		this.txntime = txntime;
	}
	
}
