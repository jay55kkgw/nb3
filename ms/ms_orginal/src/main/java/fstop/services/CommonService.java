package fstop.services;

import fstop.exception.TopMessageException;
import fstop.exception.UncheckedException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.util.*;

public abstract class CommonService implements BOService {
	private Logger logger = Logger.getLogger(getClass());
	private String securityChk = "SECCHK.OPTIONAL";

	private ParamDecorator paramDecorator; 
	
	@Required
	public void setParamDecorator(ParamDecorator paramDecorator) {
		this.paramDecorator = paramDecorator;
	}	
	
	public void setSecurityChk(String securityCheck) {
		this.securityChk = securityCheck;
	}	
	
	public CommonService(){}
	
 	public List execute(Map params) {
 		
 		Map orgParams = new HashMap();
		orgParams.putAll(params);
		
 		
 		List result = new ArrayList();
		Hashtable msg = new Hashtable();
		MVH ret = new MVHImpl();
		try {
			if(paramDecorator != null) {
				paramDecorator.before(params);
			}
			else {
				logger.debug("@@ ParamDecorator is not setting !!");
			}
			ret = doAction(params);

			if(paramDecorator != null && ret != null && ret instanceof MVH) {
				paramDecorator.after(orgParams, params, ret);
				
			}
			msg.put("MSG", "執行成功 .");
		}
		catch(TopMessageException e) {
			throw e;
		}
		catch(UncheckedException e) {
			throw e;
		}
		catch(Exception e) {
//			logger.error("Service 執行有誤 !!", e);
			msg.put("ERR", StrUtils.trim(e.getMessage()));
			throw new ServiceExecuteException("Service 執行期間發生問題 !!", e);
		}
		result.add(ret);
		result.add(msg);
		return result;
	}

 	final static String SECCHK_OPTIONAL = "SECCHK.OPTIONAL";
 	final static String SECCHK_XMLCAONLY = "SECCHK.XMLCAONLY";
 	final static String SECCHK_PINKEYONLY = "SECCHK.PINKEYONLY";
 	final static String SECCHK_REQUIRED = "SECCHK.REQUIRED";
 	final static String SECCHK_BYRULE = "SECCHK.BYRULE";
 	
 	
 	private void securityCheck(Hashtable params) {
 		Set<String> chkvalues = new HashSet();
 		chkvalues.add(SECCHK_OPTIONAL);
 		chkvalues.add(SECCHK_XMLCAONLY);
 		chkvalues.add(SECCHK_PINKEYONLY);
 		chkvalues.add(SECCHK_REQUIRED);
 		chkvalues.add(SECCHK_BYRULE);
 		
 		if(!chkvalues.contains(this.securityChk)) {
 			throw new ServiceBindingException("錯誤的 security check !");
 		}
 		
 		String fgtxway = StrUtils.trim((String)params.get("FGTXWAY"));
 		
 		if(SECCHK_OPTIONAL.equals(securityChk)) {
 			//do nothing
 		}
 		else if(SECCHK_XMLCAONLY.equals(securityChk)) {
 			if(!"1".equals(fgtxway))
 				throw new ServiceBindingException("SECCHK_XMLCAONLY, 錯誤的 security check !");
 		}
 		else if(SECCHK_PINKEYONLY.equals(securityChk)) {
 			if(!"1".equals(fgtxway))
 				throw new ServiceBindingException("SECCHK_PINKEYONLY, 錯誤的 security check !");
 		}
 		else if(SECCHK_REQUIRED.equals(securityChk)) {
 			if(!("1".equals(fgtxway) || "0".equals(fgtxway)))
 				throw new ServiceBindingException("SECCHK_REQUIRED, 錯誤的 security check !");
 		}
 		else if(SECCHK_BYRULE.equals(securityChk)) {
 			String txid = (String)params.get("TXID");
 			try {
 				SecurityCheck check = (SecurityCheck) Class.forName("fstop.services.impl." + txid.toUpperCase() + "_SecChk").newInstance();
 				boolean b = check.check(params);
 				if(!b)
 					throw new ServiceBindingException("SECCHK_BYRULE, 檢核錯誤, 無法使用 BO !");
 			}
 			catch(ClassNotFoundException e) {
 				throw new ServiceBindingException("SECCHK_BYRULE, 找不到定義的 rule !");
 			} catch (InstantiationException e) {
 				throw new ServiceBindingException("SECCHK_BYRULE, 找不到定義的 rule !");
			} catch (IllegalAccessException e) {
 				throw new ServiceBindingException("SECCHK_BYRULE, 找不到定義的 rule !");
			}
			catch (ClassCastException e) {
 				throw new ServiceBindingException("SECCHK_BYRULE, 找定義的 rule, 但無法使用, ClassCastException !");
			}
 		}
 		
 	}

// 	public <T> T getThis(T obj) {
// 		return (T)SpringBeanFactory.getBean(obj.getClass().getSimpleName().toLowerCase());
// 	}
 	
	public abstract MVH doAction(Map params);


	
}
