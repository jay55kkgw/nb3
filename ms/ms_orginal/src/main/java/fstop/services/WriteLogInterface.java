package fstop.services;

import java.util.Map;

import fstop.model.MVHImpl;

public interface WriteLogInterface {
	
	public Object writeLog(final TelCommExecWrapper execwrapper, final Map<String, String> _params, final MVHImpl result);
	
}
