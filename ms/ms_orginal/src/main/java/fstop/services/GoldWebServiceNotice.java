package fstop.services;

import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import fstop.exception.TopMessageException;
import fstop.model.MVHImpl;
import fstop.notifier.NotifyAngent;
import fstop.notifier.NotifyListener;
import fstop.orm.dao.AdmMailLogDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.ADMMAILCONTENT;
import fstop.orm.po.ADMMAILLOG;
import fstop.orm.po.TXNRECMAILLOGRELATION;
import fstop.orm.po.TXNUSER;
import fstop.util.DateTimeUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;

public class GoldWebServiceNotice implements TransferNotice {
	private Logger logger = Logger.getLogger(getClass());
	
	final AdmMailLogDao admMailLogDao = (AdmMailLogDao)SpringBeanFactory.getBean("admMailLogDao");

	public void sendNotice(NoticeInfoGetter infogetter) {
		try {
			final NoticeInfo info = infogetter.getNoticeInfo();

			String _apMailList = StrUtils.trim(info.getUserMail()).replaceAll(";", ",");	//異常時要通知的mail
			String _mailList = StrUtils.trim(info.getReceiver()).replaceAll(";", ",");	//正常時要通知的mail
					
			String templateName = info.getTemplateName();
			if(StrUtils.isEmpty(templateName))
				templateName = "GoldRequest";
			
			final StringBuffer status = new StringBuffer();
			final StringBuffer subjectBuffer = new StringBuffer();
			final StringBuffer msgBuffer = new StringBuffer();
			
			if(info.isProcessSuccess()) 
			{//電文發送成功
				
				if (StrUtils.isEmpty(_mailList))
					return;
				
				NotifyListener listener = new NotifyListener() {

					public void fail(String reciver, String subject, String msg, Exception e) {
						status.append("N");
						subjectBuffer.append(subject);
						msgBuffer.append(msg);
					}

					public void success(String reciver, String subject, String msg) {
						status.append("Y");
						subjectBuffer.append(subject);
						msgBuffer.append(msg);
					}
					
				};
				
				info.getADMMAILLOG().setADMAILACNO(_mailList);
				NotifyAngent.sendNotice(templateName, info.getParams(), _mailList, listener);
				writeSendNoticeLog(info, "", subjectBuffer.toString(), msgBuffer.toString(), status.toString());
			}
			else
			{//電文發送失敗
				
				if (StrUtils.isEmpty(_apMailList))
					return;
				
				NotifyListener listener = new NotifyListener() {

					public void fail(String reciver, String subject, String msg, Exception e) {
						status.append("N");
						subjectBuffer.append(subject);
						msgBuffer.append(msg);
					}

					public void success(String reciver, String subject, String msg) {
						status.append("Y");
						subjectBuffer.append(subject);
						msgBuffer.append(msg);
					}
					
				};

				if(StrUtils.isNotEmpty(_apMailList)) {
					info.getADMMAILLOG().setADMAILACNO(_apMailList);
					if(info.getException() != null) {
						if(info.getException() instanceof TopMessageException) {
							TopMessageException e = (TopMessageException)info.getException();
							info.getParams().put("ADMSGCODE", e.getMsgcode());
							info.getParams().put("ADMESSAGE", e.getMsgout());
						}
						else {
							info.getParams().put("ADMSGCODE", "ZX99");
							info.getParams().put("ADMESSAGE", info.getException().getMessage());					
						}
					}
					NotifyAngent.sendNotice(templateName + "_fail" , info.getParams(), _apMailList, listener);
					writeSendNoticeLog(info, "", subjectBuffer.toString(), msgBuffer.toString(), status.toString());
				}
			}
		}
		catch(Exception e) {
			logger.error("發送 Email 失敗.", e);
		}
	}

	public void writeSendNoticeLog(final NoticeInfo info, final String __record_adtxno, String subject, String msg, String status) {
		ADMMAILLOG maillog = info.getADMMAILLOG();
		maillog.setADSENDSTATUS(status);
		
//		ADMMAILCONTENT content = new ADMMAILCONTENT();
		maillog.setADMAILCONTENT(msg);
		maillog.setADMSUBJECT(subject);		
		
		try {
			admMailLogDao.writeNotice(maillog);
		}
		catch(Exception ex) {
			logger.error("無法寫入 MAILLOG.", ex);
		}
	}

}
