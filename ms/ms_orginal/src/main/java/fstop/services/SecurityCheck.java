package fstop.services;

import java.util.Hashtable;

public interface SecurityCheck {
	public boolean check(Hashtable params);
}
