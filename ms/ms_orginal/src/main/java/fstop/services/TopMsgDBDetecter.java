package fstop.services;

import fstop.exception.ToRuntimeException;
import fstop.model.TelcommResult;
import fstop.orm.dao.AdmMsgCodeDao;
import fstop.orm.po.ADMMSGCODE;
import lombok.extern.slf4j.Slf4j;

import org.apache.log4j.Logger;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

@Slf4j
public class TopMsgDBDetecter implements TopMsgDetecter {
	
	@Autowired
	private AdmMsgCodeDao admMsgCodeDao;


	public ADMMSGCODE isError(String topmsg, TelcommResult telcommResult) {
		ADMMSGCODE result = null;
		try {
			// 空字串
			if(topmsg != null && topmsg.length() == 0) {
				return null;
			}else if (topmsg.length()>=11) {
				return null;
			}
			// 有錯誤訊息
			result = admMsgCodeDao.isError(topmsg);
			
		}
		catch(ObjectNotFoundException e) {
			log.error("{}",e);
		}
		catch(Exception ex) {
			throw new ToRuntimeException("無法判斷錯誤訊息.", ex);
		}
		return result;
	}
	
}
