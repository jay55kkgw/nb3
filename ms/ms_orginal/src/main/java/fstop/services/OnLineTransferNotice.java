package fstop.services;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;


import fstop.notifier.NotifyAngent;
import fstop.notifier.NotifyListener;
import fstop.orm.dao.AdmMailLogDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.ADMMAILCONTENT;
import fstop.orm.po.ADMMAILLOG;
import fstop.orm.po.TXNRECMAILLOGRELATION;
import fstop.orm.po.TXNUSER;
import fstop.util.DateTimeUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 即時轉帳通知
 * @author Owner
 *
 */
@Slf4j
public class OnLineTransferNotice implements TransferNotice {
	

	final AdmMailLogDao admMailLogDao = (AdmMailLogDao)SpringBeanFactory.getBean("admMailLogDao");
	final private TxnUserDao txnUserDao = (TxnUserDao)SpringBeanFactory.getBean("txnUserDao");

	public void sendNotice(NoticeInfoGetter infogetter) {
		try {
			final NoticeInfo info = infogetter.getNoticeInfo();

			TXNUSER txnUser = txnUserDao.findById(info.getUid());
			String dpnotify = StrUtils.trim(txnUser.getDPNOTIFY());
			Set<String> dpnotifySet = StrUtils.splitToSet("\\s*,\\s*", dpnotify);  //1 :  轉帳/繳費稅/匯出匯款成功通知
			
			boolean isUserSetTxnNotify = dpnotifySet.contains("1");
			String dpmyemail = StrUtils.trim(txnUser.getDPMYEMAIL());

			String _mailList = StrUtils.trim(info.getReceiver()).replaceAll(";", ",");
			
			Set mailListSet = new HashSet();
			for(String s : _mailList.split(",")) {
				s = StrUtils.trim(s);
				if(StrUtils.isNotEmpty(s))
					mailListSet.add(StrUtils.trim(s));
			}


			String templateName = info.getTemplateName();
			if(StrUtils.isEmpty(templateName))
				templateName = "transferNotify";

			final String __record_adtxno = info.getADTXNO();

			final StringBuffer status = new StringBuffer();
			final StringBuffer subjectBuffer = new StringBuffer();
			final StringBuffer msgBuffer = new StringBuffer();
			if(info.isProcessSuccess()) {  //轉帳成功
				if(isUserSetTxnNotify) {
					if(StrUtils.isNotEmpty(dpmyemail))
						mailListSet.add(dpmyemail);
				}
				_mailList = StrUtils.implode(",", mailListSet);
				
				NotifyListener listener = new NotifyListener() {

					public void fail(String reciver, String subject, String msg, Exception e) {
						status.append("N");
						subjectBuffer.append(subject);
						msgBuffer.append(msg);
					}

					public void success(String reciver, String subject, String msg) {
						status.append("Y");
						subjectBuffer.append(subject);
						msgBuffer.append(msg);
					}
				};
				info.getADMMAILLOG().setADMAILACNO(_mailList);
				NotifyAngent.sendNotice(templateName, info.getParams(), _mailList, listener);
				
				writeSendNoticeLog(info, __record_adtxno, subjectBuffer.toString(), msgBuffer.toString(), status.toString());
			}
			else {  //轉帳失敗
				//do nothing
			}
		}
		catch(Exception e) {
			log.error("發送 Email 失敗.", e);
		}
	}
	
	public void writeSendNoticeLog(final NoticeInfo info, final String __record_adtxno, String subject, String msg, String status) {
		ADMMAILLOG maillog = info.getADMMAILLOG();
		maillog.setADSENDSTATUS(status);
		
//		ADMMAILCONTENT content = new ADMMAILCONTENT();
		maillog.setADMAILCONTENT(msg);
		maillog.setADMSUBJECT(subject);
		
		Date d = new Date();
		TXNRECMAILLOGRELATION relation = new TXNRECMAILLOGRELATION();
		relation.setADTXNO(__record_adtxno);
		relation.setLASTDATE(DateTimeUtils.format("yyyyMMdd", d));
		relation.setLASTTIME(DateTimeUtils.format("HHmmss", d));
		
		try {
			admMailLogDao.writeRecordNotice(maillog,relation);
		}
		catch(Exception ex) {
			log.error("無法寫入 MAILLOG.", ex);
		}
	}
}
