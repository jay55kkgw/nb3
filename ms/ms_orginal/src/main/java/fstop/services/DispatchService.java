package fstop.services;

import fstop.exception.TopMessageException;
import fstop.exception.UncheckedException;
import fstop.model.MVH;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

@Slf4j
public abstract class DispatchService extends CommonService {
	
	public MVH doAction(Map params) {
	 
		try {
			 return (MVH)getClass().getMethod(getActionMethod(params),  new Class[] {Map.class})
			 .invoke(this, new Object[] {params});
			//result = (IMasterValuesHelper)MethodUtils.invokeMethod(this, getActionMethod(params), new Object[]{params});

		} catch (TopMessageException e) {
			throw e;
		}
		catch(UncheckedException e) {
			throw e;
		} catch (NoSuchMethodException e) {
			log.error("DispatchService 執行有誤 !!", e);
			throw new ServiceBindingException("無法連結對應的 method, [" + getActionMethod(params) + "]", e);
		} catch (IllegalAccessException e) {
			log.error("DispatchService 執行有誤 !!", e);
			throw new ServiceBindingException("DispatchService 執行錯誤, [" + getActionMethod(params) + "]", e);
		} catch (InvocationTargetException e) {
			if(e.getTargetException() instanceof TopMessageException)
				throw (TopMessageException)e.getTargetException();
			
			throw new ServiceBindingException("DispatchService 執行錯誤 !, [" + getActionMethod(params) + "]", e.getTargetException());
		}
	}
	
	public abstract String getActionMethod(Map<String, String> params);


	
}
