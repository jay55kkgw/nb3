package fstop.services;

import fstop.model.TelcommResult;
import fstop.orm.dao.AdmMsgCodeDao;
import fstop.orm.po.ADMMSGCODE;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

@Slf4j
public class TopMsgF003Detecter implements TopMsgDetecter {
//	private Logger log = Logger.getLogger(getClass());
	
	@Autowired
	private AdmMsgCodeDao admMsgCodeDao;

//	@Required
//	public void setAdmMsgCodeDao(AdmMsgCodeDao admMsgCodeDao) {
//		this.admMsgCodeDao = admMsgCodeDao;
//	}

	public ADMMSGCODE isError(String topmsg, TelcommResult telcommResult) {
		
		if ("0000".equals(topmsg))
			return null;
		else {
			if (telcommResult.getFlatValues().get("MSG_CODE") != null && ! "".equals(telcommResult.getFlatValues().get("MSG_CODE")))	
				topmsg = telcommResult.getFlatValues().get("MSG_CODE");
		}
		
		if (StrUtils.trim(topmsg).length() < 4)
			topmsg = "Z010";     // F003電文回應內容異常!!!
		
		try {
			ADMMSGCODE msg = admMsgCodeDao.isError(topmsg);
			
			if (msg == null)
				topmsg = "Z010";     // 中心電文回應內容異常!!!
			else			
				return msg;
		}
		catch(Exception e) {}

		ADMMSGCODE result = new ADMMSGCODE();
		result.setADMCODE(StrUtils.trim(topmsg));
		
		return result;		
	}
	
}
