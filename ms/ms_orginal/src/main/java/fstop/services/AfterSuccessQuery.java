package fstop.services;

import fstop.model.MVHImpl;

public interface AfterSuccessQuery {
	public void execute(MVHImpl result);
}
