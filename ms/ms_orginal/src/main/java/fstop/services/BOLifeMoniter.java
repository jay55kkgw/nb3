package fstop.services;

import java.util.HashMap;
import java.util.Map;

public class BOLifeMoniter {
	static ThreadLocal threadValues = new ThreadLocal();
	
	
	public static void clearValues() {
		if(threadValues.get() == null)
			threadValues.set(new HashMap());
		else 
			((Map)threadValues.get()).clear();
	}
	
	public static void setValue(String key, Object obj) {
		if(threadValues.get() == null)
			clearValues();
		((Map)threadValues.get()).put(key, obj);
	}
	
	public static Object getValue(String key) {
		if(threadValues.get() == null)
			clearValues();
		
		return ((Map)threadValues.get()).get(key);
	}
	
}
