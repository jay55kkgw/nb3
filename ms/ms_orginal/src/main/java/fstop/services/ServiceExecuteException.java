package fstop.services;

import fstop.exception.UncheckedException;

public class ServiceExecuteException extends UncheckedException { 

	private static final long serialVersionUID = 1L;
	public ServiceExecuteException(String msg) {
		super(msg);
	}
	public ServiceExecuteException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
