package fstop.services;

public interface NoticeInfoGetter {
	
	public NoticeInfo getNoticeInfo();
}
