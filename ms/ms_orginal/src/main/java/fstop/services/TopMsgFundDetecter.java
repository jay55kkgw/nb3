package fstop.services;

import fstop.model.TelcommResult;
import fstop.orm.dao.AdmMsgCodeDao;
import fstop.orm.po.ADMMSGCODE;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

@Slf4j
public class TopMsgFundDetecter implements TopMsgDetecter {
//	private Logger log = Logger.getLogger(getClass());
	
	@Autowired
	private AdmMsgCodeDao admMsgCodeDao;

//	@Required
//	public void setAdmMsgCodeDao(AdmMsgCodeDao admMsgCodeDao) {
//		this.admMsgCodeDao = admMsgCodeDao;
//	}

	public ADMMSGCODE isError(String topmsg, TelcommResult telcommResult) {
		
		if(StrUtils.trim(topmsg).replaceAll("0", "").length() == 0)
			return null;		
		
		if (StrUtils.trim(topmsg).length() < 4)
			topmsg = "Z010";     // 中心電文回應內容異常!!!		
		
		try {
			ADMMSGCODE msg = admMsgCodeDao.isError(topmsg);
			
			if (msg == null)
				topmsg = "Z010";     // 中心電文回應內容異常!!!
			else			
				return msg;
		}
		catch(Exception e) {}

		ADMMSGCODE result = new ADMMSGCODE();
		result.setADMCODE(StrUtils.trim(topmsg));
		
		return result;		
	}
	
}
