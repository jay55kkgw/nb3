package fstop.services;

import java.util.Date;
import java.util.Map;
import java.util.Observable;
import java.util.UUID;
import java.util.concurrent.Semaphore;

import org.hibernate.ObjectNotFoundException;

import com.netbank.util.ESAPIUtil;
import com.netbank.util.fstop.DateUtil;

import fstop.exception.ToRuntimeException;
import fstop.exception.TopMessageException;
import fstop.exception.UncheckedException;
import fstop.model.MVHImpl;
import fstop.orm.dao.AdmMsgCodeDao;
import fstop.orm.dao.SysDailySeqDao;
import fstop.orm.dao.TxnFxRecordDao;
import fstop.orm.dao.TxnFxSchPayDataDao;
import fstop.orm.dao.TxnGdRecordDao;
import fstop.orm.dao.TxnTwRecordDao;
import fstop.orm.po.ADMMSGCODE;
import fstop.orm.po.TXNFXRECORD;
import fstop.orm.po.TXNGDRECORD;
import fstop.orm.po.TXNTWRECORD;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.IPUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TelCommExecWrapper extends Observable
{
	// private Logger logger = Logger.getLogger(getClass());

	private final Semaphore sem = new Semaphore(1); // 同步鎖

	private TelCommExec exec;

	private AfterSuccessQuery afterQuery;

	int doQuery = 0;

	private Map<String, String> queryParams;

	private RuntimeException lastException = null;

	public Throwable getLastException()
	{
		return lastException;
	}

	public void setAfterQuery(AfterSuccessQuery afterQuery)
	{
		this.afterQuery = afterQuery;
	}

	public TelCommExecWrapper(TelCommExec exec)
	{
		this.exec = exec;
	}

	public MVHImpl query(Map params)
	{

		if (doQuery > 0)
			throw new ToRuntimeException("無法執行兩次QUERY.");

		this.queryParams = params;

		doQuery++;

		MVHImpl result = null;

		this.setChanged();
		this.notifyObservers(new Object[] { "before", params });

		try
		{

			if (false && IPUtils.isLocalTestIp())
				throw new RuntimeException("debug exception ... ");

			result = exec.query(params);

			this.setChanged();
			this.notifyObservers(new Object[] { "doSuccess" });
		}
		catch (UncheckedException e)
		{
			log.error("Exception Error : ", e);
			lastException = e;

			this.setChanged();
			this.notifyObservers(new Object[] { "doFail", e });
		}
		catch (RuntimeException e)
		{
			log.error("Exception Error : ", e);
			lastException = new ToRuntimeException(e.getMessage(), e);

			this.setChanged();
			this.notifyObservers(new Object[] { "doFail", e });
		}
		catch (Exception e)
		{
			log.error("Exception Error : ", e);
			lastException = new ToRuntimeException(e.getMessage(), e);

			this.setChanged();
			this.notifyObservers(new Object[] { "doFail", e });
		}
		finally
		{
			if (afterQuery != null)
			{
				try
				{
					afterQuery.execute(result);
				}
				catch (Exception e)
				{
					log.error("TelCommExecWrapper After Query Error.", e);
				}
			}

			this.setChanged();
			this.notifyObservers(new Object[] { "doFinally", result });
		}
		return result;
	}

	public void throwException()
	{
		log.debug("lastException>>{}", lastException);
		if (lastException != null)
			throw lastException;
	}

	public TXNTWRECORD createTxnTwRecord()
	{
		log.info("createTxnTwRecord");
		if (doQuery == 0)
		{
			log.warn("尚未執行 QUERY...");
			throw new ToRuntimeException("尚未執行 QUERY");
		}
		String trans_status = (String) queryParams.get("__TRANS_STATUS");
		if (StrUtils.isEmpty(trans_status))
		{
			trans_status = "ONLINE";
		}

		TxnTwRecordDao txnTwRecordDao = (TxnTwRecordDao) SpringBeanFactory.getBean("txnTwRecordDao");

		TXNTWRECORD record = new TXNTWRECORD();

		Date d = null;
		d = new Date();
		record.setADTXNO(UUID.randomUUID().toString());

		String topmsg = "";
		String date = DateTimeUtils.format("yyyyMMdd", d);
		String time = DateTimeUtils.format("HHmmss", d);

		record.setLASTDATE(date);
		record.setLASTTIME(time);
		record.setDPTXDATE(date);
		record.setDPTXTIME(time);

		/*
		 * 0. 成功 1. 失敗 2. 處理中
		 */
		String dptxstatus = "0";
		if (getLastException() instanceof TopMessageException)
		{
			topmsg = ((TopMessageException) getLastException()).getMsgcode();
			log.warn("get TopMessageException...");
			AdmMsgCodeDao admMsgCodeDao = (AdmMsgCodeDao) SpringBeanFactory.getBean("admMsgCodeDao");
			try
			{
				ADMMSGCODE po = null;
				try
				{
					// 有可能是不認識的 ADMMSGCODE
					po = admMsgCodeDao.findById(topmsg);
				}
				catch (Exception e)
				{
					log.error("找不到 MESSAGE CODE : " + topmsg);
				}

				if (po == null)
					po = new ADMMSGCODE();

				String resend = po.getADMRESEND(); // 可否提供台幣類交易人工重送
				String resend2 = po.getADMAUTOSEND(); // 可否提供台幣類交易自動重送

				// record.setDPRETXSTATUS("0"); // 不需重送

				// if (! trans_status.equals("RESEND")) {
				// if ("Y".equalsIgnoreCase(resend)
				// || "Y".equalsIgnoreCase(resend2))
				// record.setDPRETXSTATUS("5"); // 未重送(表示：未執行重送交易)
				// else
				// record.setDPRETXSTATUS("0"); // 不需重送
				// }

				// String resend = admMsgCodeDao.findRESEND(topmsg); // Y or N
				//
				// record.setDPRETXSTATUS("0"); // 不可重送
				// if ("Y".equalsIgnoreCase(resend))
				// record.setDPRETXSTATUS("5"); // 待重送
				// if ("N".equalsIgnoreCase(resend))
				// record.setDPRETXSTATUS("0"); // 不可重送

			}
			catch (ObjectNotFoundException e)
			{
			}

			dptxstatus = "1";
		}
		else if (getLastException() != null)
		{
			topmsg = "";
			dptxstatus = "1";
			// record.setDPRETXSTATUS("0");
		}
		else if (getLastException() == null)
		{
			topmsg = "";
			dptxstatus = "0";
			// record.setDPRETXSTATUS("0");
		}

		record.setDPREMAIL("0"); // 重發 email 次數
		record.setDPTXSTATUS(dptxstatus); // 交易執行狀態 0. 成功, 1. 失敗, 2. 處理中

		record.setDPEXCODE(topmsg);
		// record.setDPRETXNO("");

		if (queryParams.containsKey("__SCHID"))
		{
			record.setDPSCHID(new Long(queryParams.get("__SCHID")));
			// record.setDPTITAINFO(new Long(queryParams.get("__REQID")));
		}

		// TODO 重發email 次數有限制, 在重發時, 要把上次的重發次數帶入
		if ("RESEND".equalsIgnoreCase(trans_status))
		{
			String oldadtxno = queryParams.get("__TRANS_RESEND_ADTXNO");
			log.info(ESAPIUtil.vaildLog("OLDADTXNO = " + oldadtxno));
			TXNTWRECORD resendRecord = txnTwRecordDao.findById(queryParams.get("__TRANS_RESEND_ADTXNO"));
			// if ("1".equals(dptxstatus) && "0".equals(record.getDPRETXSTATUS())) // 交易失敗,
			// 且
			// topmessage
			// 為不可重送
			// resendRecord.setDPRETXSTATUS("4"); // 重送處理失敗
			// if ("0".equals(dptxstatus) && "0".equals(record.getDPRETXSTATUS())) // 交易成功
			// resendRecord.setDPRETXSTATUS("3"); // 重送完成

			// resendRecord.setDPRETXNO(record.getADTXNO());
			resendRecord.setLASTDATE(date);
			resendRecord.setLASTTIME(time);

			record.setDPREMAIL(resendRecord.getDPREMAIL()); // 帶入上次重發 EMAIL 的次數

			txnTwRecordDao.rewriteTxnRecord(resendRecord);
		}
		log.debug(ESAPIUtil.vaildLog("record>>{}"+ record));
		return record;
	}

	public TXNTWRECORD createTxnTwRecord(SysDailySeqDao sysDailySeqDao)
	{
		log.info("createTxnTwRecord");
		if (doQuery == 0)
		{
			log.warn("尚未執行 QUERY...");
			throw new ToRuntimeException("尚未執行 QUERY");
		}
		String trans_status = (String) queryParams.get("__TRANS_STATUS");
		if (StrUtils.isEmpty(trans_status))
		{
			trans_status = "ONLINE";
		}

		TxnTwRecordDao txnTwRecordDao = (TxnTwRecordDao) SpringBeanFactory.getBean("txnTwRecordDao");

		TXNTWRECORD record = new TXNTWRECORD();

		Date d = null;
		d = new Date();
		String adtxno = "";
//		// 預約一扣
//		if ("BATCH".equals((String) queryParams.get("__EXECWAY"))
//				&& "SEND".equals((String) queryParams.get("__TRANS_STATUS")))
//		{
//			adtxno = (String) queryParams.get("__TRANS_SEND_ADTXNO");
//			record.setADTXNO(adtxno);
//			// 預約二扣
//		}
//		else if ("BATCH".equals((String) queryParams.get("__EXECWAY"))
//				&& "RESEND".equals((String) queryParams.get("__TRANS_STATUS")))
//		{
//			adtxno = (String) queryParams.get("__TRANS_RESEND_ADTXNO");
//			record.setADTXNO(adtxno);
//		}
//		else
//		{
//			String pcseq = String.format("%05d", sysDailySeqDao.dailySeq("N070"));
//			log.info("pcseq>>{}", pcseq);
//			adtxno = DateUtil.getCurentDateTime("yyyyMMddHH") + pcseq;
//			log.info("adtxno>>{}", adtxno);
//			record.setADTXNO(adtxno);
//		}
		record.setADTXNO(UUID.randomUUID().toString());

		String topmsg = "";
		String date = DateTimeUtils.format("yyyyMMdd", d);
		String time = DateTimeUtils.format("HHmmss", d);

		record.setLASTDATE(date);
		record.setLASTTIME(time);
		record.setDPTXDATE(date);
		record.setDPTXTIME(time);

		/*
		 * 0. 成功 1. 失敗 2. 處理中
		 */
		String dptxstatus = "0";
		if (getLastException() instanceof TopMessageException)
		{
			topmsg = ((TopMessageException) getLastException()).getMsgcode();
			log.warn("get TopMessageException...");
			AdmMsgCodeDao admMsgCodeDao = (AdmMsgCodeDao) SpringBeanFactory.getBean("admMsgCodeDao");
			try
			{
				ADMMSGCODE po = null;
				try
				{
					// 有可能是不認識的 ADMMSGCODE
					po = admMsgCodeDao.findById(topmsg);
				}
				catch (Exception e)
				{
					log.error("找不到 MESSAGE CODE : " + topmsg);
				}

				if (po == null)
					po = new ADMMSGCODE();

				String resend = po.getADMRESEND(); // 可否提供台幣類交易人工重送
				String resend2 = po.getADMAUTOSEND(); // 可否提供台幣類交易自動重送

				// record.setDPRETXSTATUS("0"); // 不需重送

				// if (! trans_status.equals("RESEND")) {
				// if ("Y".equalsIgnoreCase(resend)
				// || "Y".equalsIgnoreCase(resend2))
				// record.setDPRETXSTATUS("5"); // 未重送(表示：未執行重送交易)
				// else
				// record.setDPRETXSTATUS("0"); // 不需重送
				// }

				// String resend = admMsgCodeDao.findRESEND(topmsg); // Y or N
				//
				// record.setDPRETXSTATUS("0"); // 不可重送
				// if ("Y".equalsIgnoreCase(resend))
				// record.setDPRETXSTATUS("5"); // 待重送
				// if ("N".equalsIgnoreCase(resend))
				// record.setDPRETXSTATUS("0"); // 不可重送

			}
			catch (ObjectNotFoundException e)
			{
			}
			if("EUMP".equalsIgnoreCase(topmsg)) {
				dptxstatus = "0";
				topmsg = "";
			}else {
				dptxstatus = "1";
			}
		}
		else if (getLastException() != null)
		{
			topmsg = "";
			dptxstatus = "1";
			// record.setDPRETXSTATUS("0");
			
		}
		else if (getLastException() == null)
		{
			topmsg = "";
			dptxstatus = "0";
			// record.setDPRETXSTATUS("0");
		}

		record.setDPREMAIL("0"); // 重發 email 次數
		record.setDPTXSTATUS(dptxstatus); // 交易執行狀態 0. 成功, 1. 失敗, 2. 處理中

		record.setDPEXCODE(topmsg);
		// record.setDPRETXNO("");

		if (queryParams.containsKey("__SCHID"))
		{
			record.setDPSCHID(new Long(queryParams.get("__SCHID")));
			// record.setDPTITAINFO(new Long(queryParams.get("__REQID")));
		}

		// TODO 重發email 次數有限制, 在重發時, 要把上次的重發次數帶入
		// TODO 這段先不做
//		if ("RESEND".equalsIgnoreCase(trans_status))
//		{
//			String oldadtxno = queryParams.get("__TRANS_RESEND_ADTXNO");
//			log.info("OLDADTXNO = " + oldadtxno);
//			TXNTWRECORD resendRecord = txnTwRecordDao.findById(queryParams.get("__TRANS_RESEND_ADTXNO"));
//			// if ("1".equals(dptxstatus) && "0".equals(record.getDPRETXSTATUS())) // 交易失敗,
//			// 且
//			// topmessage
//			// 為不可重送
//			// resendRecord.setDPRETXSTATUS("4"); // 重送處理失敗
//			// if ("0".equals(dptxstatus) && "0".equals(record.getDPRETXSTATUS())) // 交易成功
//			// resendRecord.setDPRETXSTATUS("3"); // 重送完成
//
//			// resendRecord.setDPRETXNO(record.getADTXNO());
//			resendRecord.setLASTDATE(date);
//			resendRecord.setLASTTIME(time);
//
//			record.setDPREMAIL(resendRecord.getDPREMAIL()); // 帶入上次重發 EMAIL 的次數
//
//			txnTwRecordDao.rewriteTxnRecord(resendRecord);
//
//			record = txnTwRecordDao.findById(queryParams.get("__TRANS_RESEND_ADTXNO"));
//		}
		log.debug("record>>{}", record);
		return record;
	}
	

	
	/**
	 * 記錄外幣交易紀錄 ,傳入外幣預約以利更新外幣預約DB
	 * @param params
	 * @param txnFxSchPayDataDao
	 * @return
	 */
	public TXNFXRECORD  createTxnFxRecordCheckupdatePayData(Map<String,String> params , TxnFxSchPayDataDao txnFxSchPayDataDao){
		
		txnFxSchPayDataDao.checkupdateTxnFxSchPayData(params);
		TXNFXRECORD  txnfxrecord = this.createTxnFxRecord();
		//	交易序號
		//	String stan = params.get("STAN");
		//	stan =	StrUtils.isEmpty(stan) ? "" : stan;
		//改用pk
		String fxschno = params.get("FXSCHNO");////預約批號
		String fxschtxdate = params.get("FXSCHTXDATE");//預約轉帳日
		String fxuserid = params.get("UID");// 使用者ID
		String fxmsgcontent = fxschno + fxschtxdate +fxuserid;
		txnfxrecord.setFXMSGCONTENT(fxmsgcontent);
		return txnfxrecord;
	}
	
	
	/**
	 * 
	 * 記錄外幣交易紀錄 
	 * 
	 * @param txnFxSchPayDataDao
	 * @return
	 */
	public TXNFXRECORD createTxnFxRecord()
	{

		if (doQuery == 0)
			throw new ToRuntimeException("尚未執行 QUERY");

		String trans_status = (String) queryParams.get("__TRANS_STATUS");
		if (StrUtils.isEmpty(trans_status))
		{
			trans_status = "ONLINE";
		}

		TxnFxRecordDao txnFxRecordDao = (TxnFxRecordDao) SpringBeanFactory.getBean("txnFxRecordDao");

		TXNFXRECORD record = new TXNFXRECORD();

		Date d = null;
		d = new Date();

		record.setADTXNO(UUID.randomUUID().toString());

		String topmsg = "";
		String date = DateTimeUtils.format("yyyyMMdd", d);
		String time = DateTimeUtils.format("HHmmss", d);

		record.setLASTDATE(date);
		record.setLASTTIME(time);
		record.setFXTXDATE(date);
		record.setFXTXTIME(time);

		/*
		 * 每次執行之轉帳交易(包含:即時,預約,重送)
		 * 
		 * FXTXSTATUS: 0. 成功 1. 失敗 2. 人工終止交易
		 */
		String txstatus = "0";

		log.debug("@@@ TelComm before getLastException() ");

		if (getLastException() instanceof TopMessageException)
		{
			topmsg = ((TopMessageException) getLastException()).getMsgcode();

			if (topmsg.length() > 6) // MERVA 的回傳長度為 7
				topmsg = StrUtils.right(topmsg, 4);

			log.debug("@@@ TelComm topmsg == " + topmsg);

			AdmMsgCodeDao admMsgCodeDao = (AdmMsgCodeDao) SpringBeanFactory.getBean("admMsgCodeDao");
			try
			{

				log.debug("@@@ TelComm before ADMMSGCODE !!! ");

				ADMMSGCODE po = null;
				try
				{
					// 有可能是不認識的 ADMMSGCODE
					po = admMsgCodeDao.findById(topmsg);
				}
				catch (Exception e)
				{
					log.error("找不到 MESSAGE CODE : " + topmsg);
				}

				if (po == null)
					po = new ADMMSGCODE();

				String resend = po.getADMRESENDFX(); // 可否提供外幣類交易人工重送
				String resendAuto = po.getADMAUTOSENDFX(); // 可否提供外幣類交易自動重送

				// 即時交易
				if (!queryParams.containsKey("__SCHID"))
				{
					if (TopMessageException.isInDB(topmsg) && resend.equals("Y"))
					{
						// record.setFXRETXSTATUS("5"); // 未重送(表示：未執行重送交易)
					}
					else
					{
						// record.setFXRETXSTATUS("0"); // 不需重送
					}
				}
				// 預約交易
				else
				{

					if (!trans_status.equals("RESEND"))
					{
						if ("Y".equalsIgnoreCase(resend) || "Y".equalsIgnoreCase(resendAuto))
						{
							// record.setFXRETXSTATUS("5"); // 未重送(表示：未執行重送交易)
						}

						else
						{
							// record.setFXRETXSTATUS("0"); // 不需重送
						}
					}
				}
				log.debug("@@@ TelComm resend == " + resend);
			}
			catch (ObjectNotFoundException e)
			{
				log.error("@@@ ObjectNotFoundException resend == ", e);
			}
			txstatus = "1";
		}
		else if (getLastException() != null)
		{
			log.debug("@@@ TelComm topmsg getLastException() != null");
			topmsg = "";
			txstatus = "1";
		}
		else if (getLastException() == null)
		{

			log.debug("@@@ TelComm topmsg getLastException() == null");
			topmsg = "";
			txstatus = "0";
		}

		record.setFXREMAIL("0"); // 重發 email 次數
		record.setFXTXSTATUS(txstatus);
		if (topmsg.length() > 6) // MERVA 的回傳長度為 7
			topmsg = StrUtils.right(topmsg, 4);
		record.setFXEXCODE(topmsg);

		if (queryParams.containsKey("__SCHID"))
		{
			record.setFXSCHID(new Long(queryParams.get("__SCHID")));
		}

		return record;
	}

	public TXNGDRECORD createTxnGdRecord()
	{

		if (doQuery == 0)
			throw new ToRuntimeException("尚未執行 QUERY");

		String trans_status = (String) queryParams.get("__TRANS_STATUS");
		if (StrUtils.isEmpty(trans_status))
		{
			trans_status = "ONLINE";
		}

		TxnGdRecordDao txnGdRecordDao = (TxnGdRecordDao) SpringBeanFactory.getBean("txnGdRecordDao");

		TXNGDRECORD record = new TXNGDRECORD();

		Date d = null;
		d = new Date();
		record.setADTXNO(UUID.randomUUID().toString());

		String topmsg = "";
		String date = DateTimeUtils.format("yyyyMMdd", d);
		String time = DateTimeUtils.format("HHmmss", d);

		record.setLASTDATE(date);
		record.setLASTTIME(time);
		record.setGDTXDATE(date);
		record.setGDTXTIME(time);

		/*
		 * 0. 成功 1. 失敗 2. 處理中
		 */
		String GDtxstatus = "0";
		if (getLastException() instanceof TopMessageException)
		{
			topmsg = ((TopMessageException) getLastException()).getMsgcode();

			AdmMsgCodeDao admMsgCodeDao = (AdmMsgCodeDao) SpringBeanFactory.getBean("admMsgCodeDao");
			try
			{
				ADMMSGCODE po = null;
				try
				{
					// 有可能是不認識的 ADMMSGCODE
					po = admMsgCodeDao.findById(topmsg);
				}
				catch (Exception e)
				{
					log.error("找不到 MESSAGE CODE : " + topmsg);
				}

				if (po == null)
					po = new ADMMSGCODE();

				String resend = po.getADMRESEND(); // 可否提供台幣類交易人工重送
				String resend2 = po.getADMAUTOSEND(); // 可否提供台幣類交易自動重送

				record.setGDRETXSTATUS("0"); // 不需重送

				if (!trans_status.equals("RESEND"))
				{
					if ("Y".equalsIgnoreCase(resend) || "Y".equalsIgnoreCase(resend2))
						record.setGDRETXSTATUS("5"); // 未重送(表示：未執行重送交易)
					else record.setGDRETXSTATUS("0"); // 不需重送
				}

				// String resend = admMsgCodeDao.findRESEND(topmsg); // Y or N
				//
				// record.setGDRETXSTATUS("0"); // 不可重送
				// if ("Y".equalsIgnoreCase(resend))
				// record.setGDRETXSTATUS("5"); // 待重送
				// if ("N".equalsIgnoreCase(resend))
				// record.setGDRETXSTATUS("0"); // 不可重送

			}
			catch (ObjectNotFoundException e)
			{
			}

			GDtxstatus = "1";
		}
		else if (getLastException() != null)
		{
			topmsg = "";
			GDtxstatus = "1";
			record.setGDRETXSTATUS("0");
		}
		else if (getLastException() == null)
		{
			topmsg = "";
			GDtxstatus = "0";
			record.setGDRETXSTATUS("0");
		}

		record.setGDREMAIL("0"); // 重發 email 次數
		record.setGDTXSTATUS(GDtxstatus); // 交易執行狀態 0. 成功, 1. 失敗, 2. 處理中

		record.setGDEXCODE(topmsg);
		record.setGDRETXNO("");

		// TODO 重發email 次數有限制, 在重發時, 要把上次的重發次數帶入
		if ("RESEND".equalsIgnoreCase(trans_status))
		{
			String oldadtxno = queryParams.get("__TRANS_RESEND_ADTXNO");
			log.info(ESAPIUtil.vaildLog("OLDADTXNO = " + oldadtxno));
			TXNGDRECORD resendRecord = txnGdRecordDao.findById(queryParams.get("__TRANS_RESEND_ADTXNO"));
			if ("1".equals(GDtxstatus) && "0".equals(record.getGDRETXSTATUS())) // 交易失敗,
																				// 且
																				// topmessage
																				// 為不可重送
				resendRecord.setGDRETXSTATUS("4"); // 重送處理失敗
			if ("0".equals(GDtxstatus) && "0".equals(record.getGDRETXSTATUS())) // 交易成功
				resendRecord.setGDRETXSTATUS("3"); // 重送完成

			resendRecord.setGDRETXNO(record.getADTXNO());
			resendRecord.setLASTDATE(date);
			resendRecord.setLASTTIME(time);

			record.setGDREMAIL(resendRecord.getGDREMAIL()); // 帶入上次重發 EMAIL 的次數

			txnGdRecordDao.rewriteTxnRecord(resendRecord);
		}

		return record;
	}

	public void addListener(TransferListener listener)
	{
		addObserver(new TransferListenerWrapper(listener));
	}

	public void removeListener(TransferListener listener)
	{
		deleteObserver(new TransferListenerWrapper(listener));
	}

}
