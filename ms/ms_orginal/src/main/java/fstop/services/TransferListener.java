package fstop.services;

import java.util.Map;

import fstop.model.MVHImpl;

public interface TransferListener {
	public void before(Map params);
	
	public void doSuccess();
	
	public void doFail();
	
	public void doFinally(MVHImpl queryresult);

}
