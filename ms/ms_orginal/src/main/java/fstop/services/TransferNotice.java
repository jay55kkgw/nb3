package fstop.services;

public interface TransferNotice {
	
	public void sendNotice(NoticeInfoGetter infoGetter);
	
	public void writeSendNoticeLog(final NoticeInfo info, final String __record_adtxno, String subject, String msg, String status);
}
