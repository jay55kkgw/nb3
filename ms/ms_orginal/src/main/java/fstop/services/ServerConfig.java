package fstop.services;

import java.io.InputStream;

import javax.servlet.ServletContext;

import org.apache.log4j.Logger;
import org.springframework.web.context.ServletContextAware;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ServerConfig implements ServletContextAware{     
	private Logger logger = Logger.getLogger(getClass());
	private String serverRootUrl;     
	private String contextPath;
	private ServletContext servletContext;
	
	public String getServerRootUrl()
	{ 
		return serverRootUrl; 
	}
	
	public String getContextPath()
	{ 
		return contextPath; 
	}
	
	public InputStream getgetResourceAsStream(String FileName)
	{
		  return servletContext.getResourceAsStream(FileName);
	}

	public void setServletContext(ServletContext servletContext)
	{         
		this.serverRootUrl=servletContext.getRealPath("");
		
		log.debug("serverRootUrl = " + serverRootUrl);
		
		this.contextPath = "/" + servletContext.getServletContextName();
		
		log.debug("ServletContextName = " + servletContext.getServletContextName());
		log.debug("ServerInfo = " + servletContext.getServerInfo());
		
		this.servletContext=servletContext;
	} 
} 
