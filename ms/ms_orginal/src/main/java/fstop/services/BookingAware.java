package fstop.services;

import java.util.Hashtable;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;

import fstop.model.MVHImpl;
import fstop.model.TelcommResult;

public interface BookingAware {

	public MVHImpl booking(Map<String, String> params);
	
	public MVHImpl online(Map<String, String> params, TransferNotice trNotice);
}
