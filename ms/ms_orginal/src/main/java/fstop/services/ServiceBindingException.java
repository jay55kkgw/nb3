package fstop.services;

import fstop.exception.UncheckedException;

public class ServiceBindingException extends UncheckedException { 
	public ServiceBindingException(String msg) {
		super(msg);
	}
	public ServiceBindingException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
