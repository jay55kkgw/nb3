package fstop.services;

import fstop.exception.ToRuntimeException;
import fstop.model.TelcommResult;
import fstop.orm.dao.AdmMsgCodeDao;
import fstop.orm.po.ADMMSGCODE;
import lombok.extern.slf4j.Slf4j;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

@Slf4j
public class TopMsgN950Detecter implements TopMsgDetecter {
//	private Logger log = Logger.getLogger(getClass());

	@Autowired
	private AdmMsgCodeDao admMsgCodeDao;

//	@Required
//	public void setAdmMsgCodeDao(AdmMsgCodeDao admMsgCodeDao) {
//		this.admMsgCodeDao = admMsgCodeDao;
//	}

	public ADMMSGCODE isError(String topmsg, TelcommResult telcommResult) {
		ADMMSGCODE result = null;
		try {
			if(topmsg != null && topmsg.length() == 0)
				return null;
			
			if(TopMsgDefaultDetecter.successTopMsg.contains(topmsg))
				return null;

			if("".equals(topmsg))
				return null;

			result = admMsgCodeDao.isError(topmsg);
			
			if(result != null)
				return result;
		}
		catch(Exception ex) {
			log.error("", ex);
			throw new ToRuntimeException("無法判斷錯誤訊息.", ex);
		}
		
		result = new ADMMSGCODE();
		result.setADMCODE(topmsg);
		return result;
	}

}
