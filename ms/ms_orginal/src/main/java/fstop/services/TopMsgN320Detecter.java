package fstop.services;

import fstop.exception.ToRuntimeException;
import fstop.model.TelcommResult;
import fstop.orm.dao.AdmMsgCodeDao;
import fstop.orm.po.ADMMSGCODE;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

import org.apache.log4j.Logger;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;

@Slf4j
public class TopMsgN320Detecter implements TopMsgDetecter {
//	private Logger log = Logger.getLogger(getClass());
	
	@Autowired
	private AdmMsgCodeDao admMsgCodeDao;

//	@Required
//	public void setAdmMsgCodeDao(AdmMsgCodeDao admMsgCodeDao) {
//		this.admMsgCodeDao = admMsgCodeDao;
//	}

	public ADMMSGCODE isError(String topmsg, TelcommResult telcommResult) {
		
		int occurs = new Integer(telcommResult.getFlatValues().get("__OCCURS"));
		
		if(occurs > 0)
			return null;
		
		List<String> topmsgs = ((TelcommResult)telcommResult).getTOPMSG();

		ADMMSGCODE result = null;
		try {
			for(String t : topmsgs) {
				if(StrUtils.isEmpty(t))
					continue;
				if(TopMsgDefaultDetecter.successTopMsg.contains(topmsg))
					continue;
				
				ADMMSGCODE msg = null;
				try {
					msg = admMsgCodeDao.isError(t);
					if(msg != null) {
						result = msg;
						break;
					}
				}
				catch(Exception e){}
				
				result = new ADMMSGCODE();
				result.setADMCODE(topmsg);
				break;
			}
		}
		catch(ObjectNotFoundException e) {
			log.error("", e);
		}
		catch(Exception ex) {
			throw new ToRuntimeException("無法判斷錯誤訊息.", ex);
		}
		return result;
	}
	
}
