package fstop.telcomm.cmd;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fstop.util.StrUtils;

public class CommandParser {

	String cmdPart;

	String valuePart;

	public String getCmdPart() {
		return cmdPart;
	}

	public String getValuePart() {
		return valuePart;
	}

	public CommandParser(String command) {
		parse(command);
		if(StrUtils.isEmpty(cmdPart) || StrUtils.isEmpty(valuePart))
			throw new CommandDefineFormatError("錯誤的設定 [" + command + "]");
	}

	public void parse(String command) {
		command = command.replaceAll("\\(\\)", "( )");
		Pattern p = Pattern.compile("([^()]+?)\\(([^()]*?)\\)");
		Matcher matcher = p.matcher(StrUtils.trim(command)); 
		if (matcher.find()) {
			cmdPart = matcher.group(1);
			valuePart = "";
			try {
				valuePart = matcher.group(2);
			}
			catch(Exception e){}
		}
		else {
			throw new CommandDefineFormatError("錯誤的設定 [" + command + "]");
		}
	}

}
