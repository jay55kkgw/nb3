package fstop.telcomm.cmd;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import fstop.model.TelcommResult;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CommandUtils {
	
	private static Logger log = Logger.getLogger(CommandUtils.class);
	
//	private static Logger getLogger() {
//		return Logger.getLogger(CommandUtils.class);
//	}

	public static void doBefore(List<String> beforeDefines, Map params) {
		for (String define : beforeDefines) {
			log.debug("doBefore define = " + define);
			
			/**
			 * 原本用來判斷是舊安控or新安控，決定是否做doBefore的邏輯，新個網一律走新安控不需再判斷
			 */
//			String TransPassUpdate = (String)params.get("TRANSPASSUPDATE");
//			String Phase2 = (String)params.get("PHASE2");
//			getLogger().debug("doBefore TransPassUpdate = " + TransPassUpdate);
//			getLogger().debug("doBefore Phase2 = " + Phase2);
//			if(StrUtils.isNotEmpty(TransPassUpdate))
//			{
//				if((TransPassUpdate.equals("1") || Phase2.equals("ON")) && 
//						(define.equals("PINKEY(N950PASSWORD)") || define.equals("PPSYNC()") || define.equals("PINKEY(CMPASSWORD)")))
//				{
//					getLogger().debug("doBefore TransPassUpdate = 1 ,define=PINKEY(N950PASSWORD),define=PPSYNC(),define=PINKEY(CMPASSWORD) continue");
//					continue;
//				}
//				else
//				if(TransPassUpdate.equals("0") && Phase2.equals("OFF") && 
//						(define.equals("PINNEW()") || define.equals("PPSYNCN()") || define.equals("PINNEW1()") || define.equals("PPSYNCN1()")))
//				{
//					getLogger().debug("doBefore TransPassUpdate = 0 ,define=PINNEW(),define=PPSYNCN() continue");
//					continue;
//				}
//			}
//			else
//			{
//				if(define.equals("PINNEW()") || define.equals("PPSYNCN()") || define.equals("PINNEW1()") || define.equals("PPSYNCN1()"))
//				{
//					getLogger().debug("doBefore define = PINNEW() , define = PPSYNCN() continue");
//					continue;
//				}
//			}
			doBefore(define, params);
		}
	}

	public static Object doBefore(String commandDefine, Map params) {
		if(StrUtils.isEmpty(commandDefine))
			return null;
		TelcommCommand commandAct = null;
		CommandParser p = new CommandParser(commandDefine);

		try {
			commandAct = (BeforeTelcommQueryCommand) Class.forName(
					CommandUtils.class.getPackage().getName() + "."
							+ p.getCmdPart()).newInstance();

			((BeforeTelcommQueryCommand) commandAct).setDefine(p.getValuePart());
			((BeforeTelcommQueryCommand) commandAct).setParams(params);
		} catch (InstantiationException e) {
			throw new CommandExecException(e.getMessage(), e);
		} catch (IllegalAccessException e) {
			throw new CommandExecException(e.getMessage(), e);
		} catch (ClassNotFoundException e) {
			throw new CommandExecException("找不到對應的 COMMAND. (" + p.getCmdPart() + ")", e);
		} catch (ClassCastException e) {
			throw new CommandExecException("錯誤的 COMMAND 設定 [" + commandDefine + "]", e);
		} catch (Exception e) {
			throw new CommandExecException(e.getMessage(), e);
		}

		return commandAct.doCommand();
	}

	public static void doAfter(List<String> afterDefines,
			TelcommResult queryResult, Map params) {
		for (String define : afterDefines) {
			doAfter(define, queryResult, params);
		}
	}

	public static Object doAfter(String commandDefine, TelcommResult queryResult, Map params) {
		if(StrUtils.isEmpty(commandDefine)) 
			return null;
		
		TelcommCommand commandAct = null;
		CommandParser p = new CommandParser(commandDefine);

		try {
			commandAct = (AfterTelcommQueryCommand) Class.forName(
					CommandUtils.class.getPackage().getName() + "."
							+ p.getCmdPart()).newInstance();
			((AfterTelcommQueryCommand) commandAct).setFieldName(p.getValuePart());
			((AfterTelcommQueryCommand) commandAct).setQueryResult(queryResult);
			((AfterTelcommQueryCommand) commandAct).setParams(params);
		} catch (InstantiationException e) {
			throw new CommandExecException(e.getMessage(), e);
		} catch (IllegalAccessException e) {
			throw new CommandExecException(e.getMessage(), e);
		} catch (ClassNotFoundException e) {
			throw new CommandExecException("找不到對應的 COMMAND. (" + p.getCmdPart() + ")", e);
		} catch (ClassCastException e) {
			throw new CommandExecException("錯誤的 COMMAND 設定 [" + commandDefine + "]", e);
		} catch (Exception e) {
			throw new CommandExecException(e.getMessage(), e);
		}

		return commandAct.doCommand();
	}

}
