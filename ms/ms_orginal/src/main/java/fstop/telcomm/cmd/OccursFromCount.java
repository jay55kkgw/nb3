package fstop.telcomm.cmd;

import lombok.extern.slf4j.Slf4j;

/**
 * 依某個欄位重覆的次數, 當作是總筆數
 * @author Owner
 *
 */
@Slf4j
public class OccursFromCount extends AfterTelcommQueryCommand {
	
	public String doCommand() {
		
		int count = this.getQueryResult().getValueOccurs(getFieldName());
		
		log.debug("FromCount '" + getFieldName() + "' count : " + count);
		
		
		
		return "success";
	}

}
