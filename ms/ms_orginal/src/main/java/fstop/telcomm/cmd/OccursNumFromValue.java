package fstop.telcomm.cmd;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import fstop.model.Row;
import fstop.model.Rows;
import fstop.model.TelcommResult;
import fstop.util.StrUtils;

public class OccursNumFromValue extends AfterTelcommQueryCommand  {
	private Logger logger = Logger.getLogger(getClass());
	public String doCommand() {
		
		
		if(true)
			return "success";
		
		TelcommResult result = this.getQueryResult();
		
		int count = -1;
		String c = "";
		try {
			List v = result.getTelcommVector();
			for(Object o : v) {
				Map rec = (Map)o;
				if(!rec.containsKey(getFieldName())) {
					throw new CommandExecException("回傳的資料無定義的欄位 ! (cmd: " + getClass().getSimpleName() + ", fieldName: " + this.getFieldName()+ ")");

				}
				c = (String)rec.get(this.getFieldName());
				
				if(StrUtils.isEmpty(c))
					continue;
				
				
				logger.debug("OCCRS NUM: " + c);
				if(count == -1)
					count = Integer.parseInt(c);
				else
					count += Integer.parseInt(c);
				
				c="";
			}
			
		}
		catch(CommandExecException e) {
			//count = -1;
			//throw e;
			logger.error(e.getMessage());
		}
		catch(Exception e) {
			//count = -1;
			//throw new CommandExecException("取得回傳筆數時發生錯誤 ! (cmd: " + getClass().getSimpleName() + ", fieldName: " + this.getFieldName()+ ", retValue: '" + c + "')", e);
			logger.error("取得回傳筆數時發生錯誤, (請檢查設定) ! (cmd: " + getClass().getSimpleName() + ", fieldName: " + this.getFieldName()+ ", retValue: '" + c + "')", e);
		}
		
		
		logger.debug("OCCRS NUM TOTAL : " + count);
		
		if(count != -1) {
			Rows rows = result.getOccurs();
			
			ArrayList<Row> rmRows = new ArrayList();
			for(int i=0; i < rows.getSize(); i++) {
				if(i >= count) {
					rmRows.add(rows.getRow(i));
					logger.debug("rmRows add : " + i);
				}
			}
			for(Row r : rmRows) {
				rows.removeRow(r);
			}
			
			logger.debug("CURRENT OCCURS : " + rows.getSize());
			
		}
		 
		logger.debug("\r\n================================");
		logger.debug("\r\n================================");
		return "success";
	}

}
