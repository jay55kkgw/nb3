package fstop.telcomm.cmd;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.netbank.rest.comm.bean.SvGet_Txn_Status_In;
import com.netbank.rest.comm.bean.SvGet_Txn_Status_Out;
import com.netbank.rest.comm.service.IdGate;
import com.netbank.rest.util.IDGateResult;
import com.netbank.util.CodeUtil;
import com.netbank.util.StrUtils;

import fstop.exception.TopMessageException;
import fstop.orm.dao.IdGate_Txn_StatusDao;
import fstop.orm.po.IDGATE_TXN_STATUS;
import fstop.orm.po.IDGATE_TXN_STATUS_PK;
import fstop.util.SpringBeanFactory;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class IDGateCommand extends BeforeTelcommQueryCommand {

//    @Autowired
    IdGate idgate;
    
//	@Autowired
	IdGate_Txn_StatusDao idgate_txn_statusdao;
	
	Boolean hasError = Boolean.TRUE;
    
	@Override
	public Object doCommand() {

		log.warn("@@@ IDGateCommand.doCommand()");	
		
		String define = this.getDefine();
		
		log.warn("@@@ IDGateCommand.doCommand() define=="+define);		

		Map<String, String> params = (Map)getParams();
		
		log.warn("@@@ IDGateCommand.doCommand() params = "+params);
		
		//批次而來跳過驗證 (做預約的時候已經驗過了)
		if("SEND".equals(params.get("__TRANS_STATUS"))||"RESEND".equals(params.get("__TRANS_STATUS"))) {
			return "success";
		}
		

		if("5".equals(getParams().get("FGTXWAY")) || "7".equals(getParams().get("FGTXWAY"))){//快速登入，FGTXWAY暫定
			if(StrUtils.isEmpty(params.get("idgateID")) || StrUtils.isEmpty(params.get("txnID")) || StrUtils.isEmpty(params.get("sessionID"))) {
				log.error("缺必要參數 idgateID>>{} , txnID>>{} , sessionID>>{}",params.get("idgateID"),params.get("txnID"),params.get("sessionID") );
				throw TopMessageException.create("Z300");
			}
			
//			if (params.get("idgateID").trim().length()==0 || params.get("txnID").trim().length()==0 || params.get("sessionID").trim().length()==0)
//				throw TopMessageException.create("Z300");
			
			
			IDGATE_TXN_STATUS_PK pks = new IDGATE_TXN_STATUS_PK();
			pks.setIDGATEID(params.get("idgateID").trim());
			pks.setSESSIONID(params.get("sessionID").trim());
			pks.setTXNID(params.get("txnID").trim());
			log.debug("idgate_txn_statusdao>>{}",idgate_txn_statusdao);
			idgate_txn_statusdao = idgate_txn_statusdao == null ?(IdGate_Txn_StatusDao) SpringBeanFactory.getBean("idGate_Txn_StatusDao"):idgate_txn_statusdao;
			log.debug("idgate_txn_statusdao1>>{}",idgate_txn_statusdao);
			IDGATE_TXN_STATUS po = idgate_txn_statusdao.findById(pks);
			if(po == null) {
				log.error("po is null...");
				throw TopMessageException.create("FE0011");
			}
			if(!po.getTXNSTATUS().equals("00")) {
				log.error("TXNSTATUS error>>{}",po.getTXNSTATUS());
				throw TopMessageException.create("FE0011");
			}
			Map<String,String> checkMap = CodeUtil.fromJson(po.getTXNCONTENT(), Map.class);
			for (Map.Entry<String,String> entry : checkMap.entrySet()) {
				if(!params.containsKey(entry.getKey())) {
					hasError = Boolean.FALSE;
					log.error("key not mapping entry.getKey() >>{} ,params get key by entry >>{}",entry.getKey() ,params.get(entry.getKey()));
//					throw TopMessageException.create("FE0011");
				}
				if(!hasError) {
					throw TopMessageException.create("FE0011");
				}
				hasError = Boolean.TRUE;
				if(!params.get(entry.getKey()).equals(entry.getValue())) {
					log.error("Value not mapping entry.getKey() >>{} ,entry.getValue() ,params.value >>{}",entry.getKey() ,entry.getValue() ,params.get(entry.getKey()));
					hasError = Boolean.FALSE;
//					throw TopMessageException.create("FE0011");
				}
				if(!hasError) {
					throw TopMessageException.create("FE0011");
				}
				
			}
			
			
			
			SvGet_Txn_Status_In in = new SvGet_Txn_Status_In();
			in.setIdgateID(params.get("idgateID").trim());
			in.setTxnID(params.get("txnID").trim());
			log.debug("idgate>>{}",idgate);
			idgate = idgate ==null? (IdGate) SpringBeanFactory.getBean("idGate"):idgate;
			log.debug("idgate1>>{}",idgate);
			IDGateResult outR = idgate.svGet_Txn_Status(in);
			if(!outR.getResult()) {
				throw TopMessageException.create("Z300");
			}
			SvGet_Txn_Status_Out out = (SvGet_Txn_Status_Out)outR.getData();
			if(!out.getTxnStatus().equals("00")) {
				String checkIdGateStatus = idgate.checkIdGateStatus;
				if(checkIdGateStatus.equals("Y")) {
					throw TopMessageException.create("Z300");
				}else {
					log.error("idgate txn status check fail, but now skip it");
				}
			}
		}
		return "success";
	}
	
}
