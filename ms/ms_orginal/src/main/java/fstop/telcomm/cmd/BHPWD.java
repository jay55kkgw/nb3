package fstop.telcomm.cmd;

import java.util.Map;

import bank.comm.ConvCode;
import fstop.exception.TelCommException;
import fstop.exception.TopMessageException;
import fstop.util.StrUtils;
import fstop.va.VaCheck;
import lombok.extern.slf4j.Slf4j;
import tw.com.top.ebank.Base64;


/**
 * BillHunter 電子帳單密碼壓碼 3DES後，再 Base64
 * @author Owner
 *
 */
@Slf4j
public class BHPWD extends BeforeTelcommQueryCommand {
	
	@Override
	public Object doCommand() {

		String define = this.getDefine();
		
		Map<String, String> params = this.getParams();

		String pw = StrUtils.trim(define);
		log.debug("pw=====" + pw);
		if(pw.length() == 0)
			throw new TelCommException("密碼不可為空.");
		
		byte[] plainTextBytes = pw.getBytes();
		String pwtoHex = ConvCode.toHexString(plainTextBytes);
		byte[] textByte; 
		
		if (pw.length() > 8) {
			pwtoHex = StrUtils.left( pwtoHex + "00000000000000000000000000000000", 32);
			log.debug("pwtoHex=====" + pwtoHex);
			String pw1 = Get3DES(pwtoHex.substring(0,16));
			String pw2 = Get3DES(pwtoHex.substring(16,32));
			textByte = hexToBytes(pw1 + pw2);			
			log.debug("pw1 + pw2=====" + pw1 + pw2);
		}			
		else {
			pwtoHex = StrUtils.left( pwtoHex + "0000000000000000", 16);
			log.debug("pwtoHex=====" + pwtoHex);
			String pw1 = Get3DES(pwtoHex);
			textByte = hexToBytes(pw1);			
			log.debug("pw1=====" + pw1);
		}
				
		String encodedCipherText = Base64.encodeBytes(textByte);
		log.debug("encodedCipherText=====" + encodedCipherText);
		params.put("BHPWD", encodedCipherText);
		
		return "success";
	}
		
	private String Get3DES(String Message) {
//old
//		// 押碼 3DES，必須傳送 Hex 字串值，上限16Bytes
//		String result = KeyClientUtils.doRequest(
//				IKeySpi.CREATE_SYNC, 
//				KeyLabel.KEYLABEL_BHKEY,
//				"0000000000000000", 
//				Message,
//				"0016");
//		log.debug("Message=====" + Message);
//		
//		if(!StrUtils.trim(result).startsWith("ERROR")) {
//			try {
//				log.debug("result=====" + result);
//				return result;
//	    	}
//	    	catch(RuntimeException e) {
//	    		throw TopMessageException.create("Z300"); //押解碼錯誤
//	    	}
//		}
//		else {
//			throw TopMessageException.create("Z300"); //押解碼錯誤
//		}
//	}
		
//new 
		String result = "";
		try {
				result = VaCheck.getMAC_BH(Message);
				if(result!=null&&result.length()>0) {
					try {
						log.warn("MAC_BH = " + result);
			    	}
			    	catch(RuntimeException e) {
			    		log.warn("getMAC_BH ERROR");
			    		throw TopMessageException.create("Z300"); //押解碼錯誤
			    	}
				}
				else {
					log.warn("getMAC_BH ERROR: result size null or zero");
					throw TopMessageException.create("Z300"); //押解碼錯誤
				}							
		}
		catch (Exception e)
		{
			log.warn("getMAC_BH ERROR:"+e.getMessage());
			throw TopMessageException.create("Z300");
		}	
		return result;
	}

	public static byte[] hexToBytes(String hexString) {

	    char[] hex = hexString.toCharArray();
	    //轉rawData長度減半
	    int length = hex.length / 2;
	    byte[] rawData = new byte[length];
	    for (int i = 0; i < length; i++) {
	      //先將hex資料轉10進位數值
	      int high = Character.digit(hex[i * 2], 16);
	      int low = Character.digit(hex[i * 2 + 1], 16);
	      //將第一個值的二進位值左平移4位,ex: 00001000 => 10000000 (8=>128)
	      //然後與第二個值的二進位值作聯集ex: 10000000 | 00001100 => 10001100 (137)
	      int value = (high << 4) | low;
	      //與FFFFFFFF作補集
	      if (value > 127)
	        value -= 256;
	      //最後轉回byte就OK
	      rawData[i] = (byte) value;
	    }
	    return rawData;
	}
}
