package fstop.telcomm.cmd;

import java.util.Map;

import org.apache.log4j.Logger;

import com.netbank.rest.e2e.E2EClient;
import com.netbank.rest.e2e.MB3_E2EClient;

import fstop.exception.TelCommException;
import fstop.exception.TopMessageException;
import fstop.model.MVHImpl;
import fstop.telcomm.TelCommExec;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import fstop.va.VaCheck;
import lombok.extern.slf4j.Slf4j;


/**
 * 依某個欄位重覆的次數, 當作是總筆數
 * @author Owner
 *
 */
@Slf4j
public class PINNEW1 extends BeforeTelcommQueryCommand {
	
	@Override
	public Object doCommand() {
		log.debug(" doCommand");
		String define = "";
		String pinkey = "PINNEW";
        String login_type = "";

		if(this.getDefine().indexOf(",") != -1){
			define = this.getDefine().substring(0, this.getDefine().indexOf(","));
			pinkey = this.getDefine().substring(this.getDefine().indexOf(",")+1);
		}
		
		Map<String, String> params = this.getParams();
        login_type = params.get("LOGINTYPE");
        log.warn("PINNEW1 doCommand login_type = " + login_type);

		log.warn("PINNEW1 doCommand define = " + define);
		log.warn("PINNEW1 doCommand pinkey = " + pinkey);
		log.warn("PINNEW1 doCommand params = " + params);
		String pinnew="";
		
		if(StrUtils.trim((String)getParams().get("FGTXWAY")).length() != 0) {
			log.debug("FGTXWAY length != 0");
			if(!"0".equals(getParams().get("FGTXWAY"))) {   // 不為交易密碼
				log.debug(getParams().get("TXID") + " SKIP PINKEY (FGTXWAY: " + getParams().get("FGTXWAY") + ")");
				return "skip";
			}
			else {
				if(StrUtils.trim((String)getParams().get("PINNEW")).length() == 48 || getParams().containsKey("__TRANS_STATUS")) {
//					因為改成E2E 後 預約紀錄的密碼是bs64 超過48 導致後續會打TMRA異常
					if("Y".equalsIgnoreCase(E2EClient.e2e().e2e.getIsE2E()) && StrUtils.trim((String)getParams().get("PINNEW")).length() >48) {
						params.put("PINNEW", params.put("PINNEW", (String)getParams().get("PINNEW")).substring(0,48) );
					}
					return "success";
				}


				if(StrUtils.isNotEmpty(params.get("PINNEW")))
				{
					log.debug("PINNEW1 doCommand params PINNEW = " + params.get("PINNEW"));
					pinnew=StrUtils.trim(params.get("PINNEW"));
				}
				else if(StrUtils.isNotEmpty(params.get("N950PASSWORD"))) // N094 無PINNEW 所以需要N950PASSWORD 為什麼 FGTXWAY=0 無CMPASSWORD 因為MAC會用到
				{
					log.debug("PINNEW1 doCommand params N950PASSWORD = " + params.get("N950PASSWORD"));
					pinnew=StrUtils.trim(params.get("N950PASSWORD"));
				}
				else
				{
						throw new TelCommException("上傳欄位中沒有 PINNEW .");
				}
				log.debug("PINNEW1 pinnew = " + pinnew);
				//做交易時, 選擇了使用 "交易密碼"
//				TelCommExec n951CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n951CMTelcomm");
				//發 N950
//				if((BOLifeMoniter.getValue("__DO_N950") == null)) {
//					BOLifeMoniter.setValue("__DO_N950", "true");
//					//發電文時, n950 會給 PPSYNC, 及 CMPASSWORD
//					//若 回傳 E004, 則會產生 TopMessageException
//					MVHImpl n950Result = n951CMTelcomm.query(params);
//					params.put("__DO_N950", "true");
//				}
				String result ="";
				try {
					if("Y".equalsIgnoreCase(E2EClient.e2e().e2e.getIsE2E())) {
                        if("MB".equals(login_type)) {
                            result = MB3_E2EClient.getPINNEW(pinnew);
                        }else {
                            result = E2EClient.getPINNEW(pinnew);
                        }
					}else {
						pinnew = StrUtils.left(pinnew + StrUtils.repeat("0", 48), 48);
						result = VaCheck.getPINNEW(pinnew,"PPKEY");
					}
					params.put(pinkey, result.toString()); 
					log.debug("FGTXWAY.pinkey>>{} , result>>{} ", pinkey,result);
				} catch (Exception e) {
					log.error("",e);
					throw TopMessageException.create("Z300");
				}
				TelCommExec n951PINTelcomm = (TelCommExec)SpringBeanFactory.getBean("n951PINTelcomm");
				MVHImpl n950Result = n951PINTelcomm.query(params);
//				params.put("__DO_N950", "true");
				return "success";
					
			}
		}
		else
		{
			log.debug("FGTXWAY length = 0");
			if(pinkey.equals("HLOGINPIN"))
			{
				log.debug("PINNEW1 doCommand params HLOGINPIN = " + params.get("HLOGINPIN"));
				pinnew=StrUtils.trim(params.get("HLOGINPIN"));
				pinkey="HLOGINPIN";
			}
			else
			if(pinkey.equals("HTRANSPIN"))
			{
				log.debug("PINNEW1 doCommand params HTRANSPIN = " + params.get("HTRANSPIN"));
				pinnew=StrUtils.trim(params.get("HTRANSPIN"));
				pinkey="HTRANSPIN";
			}
			else if(StrUtils.isNotEmpty(params.get("PINNEW")))
			{
				log.debug("PINNEW1 doCommand params PINNEW = " + params.get("PINNEW"));
				pinnew=StrUtils.trim(params.get("PINNEW"));
			}
			else
			if(StrUtils.isNotEmpty(params.get("N950PASSWORD")))
			{
				log.debug("PINNEW doCommand params N950PASSWORD = " + params.get("N950PASSWORD"));
				pinnew=StrUtils.trim(params.get("N950PASSWORD"));
			}
			else
			{
				
				//throw new TelCommException("上傳欄位中沒有 PINNEW .");
				pinnew="";
			}
		}

		params.put("__" + pinkey, pinnew);
		if(pinnew.length()==48 || getParams().containsKey("__TRANS_STATUS"))
			return "success";
		log.debug("pinnew1>>{}",pinnew);
		//右補 0
//		pinnew = StrUtils.left(pinnew + StrUtils.repeat("0", 48), 48);
//		log.debug("pinnew2>>{}",pinnew);

		
		
		
		
		String result = "";
		String isSuccess = "";
		try {
				if("Y".equalsIgnoreCase(E2EClient.e2e().e2e.getIsE2E())) {
					if("MB".equals(login_type)) {
                        result = MB3_E2EClient.getPINNEW(pinnew);
					}else {
                    	result = E2EClient.getPINNEW(pinnew);
                    }
				}else {
					pinnew = StrUtils.left(pinnew + StrUtils.repeat("0", 48), 48);
					log.debug("pinnew2>>{}",pinnew);
					
					result = VaCheck.getPINNEW(pinnew,"PPKEY");
				}
				params.put(pinkey, result.toString()); 
				
				log.warn(pinkey+" RC="+params.get(pinkey));
				
//				TelCommExec n951PINTelcomm = (TelCommExec)SpringBeanFactory.getBean("n951PINTelcomm");
//				MVHImpl n950Result = n951PINTelcomm.query(params);
//				params.put("__DO_N950", "true");
				
				isSuccess ="success";  
		}
		catch (Exception e)
		{
			isSuccess ="fail"; 
			log.warn("getPINNEW ERROR:"+e.getMessage());
			throw TopMessageException.create("Z300");
		}

		return isSuccess;
	}
	
	

}
