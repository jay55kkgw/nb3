package fstop.telcomm.cmd;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import fstop.exception.TelCommException;
import fstop.exception.TopMessageException;
import fstop.util.StrUtils;
import fstop.va.VaCheck;
import lombok.extern.slf4j.Slf4j;


/**
 * 依某個欄位重覆的次數, 當作是總筆數
 * @author Owner
 *
 */
@Slf4j
public class MAC2 extends BeforeTelcommQueryCommand {
//	private Logger log = Logger.getLogger(getClass());
	
	@Override
	public Object doCommand() {
		
		String define = this.getDefine();
		String CUSIDN = "";
		Map<String, String> params = (Map)getParams();
		log.warn("MAC2.params : " + params);
		
		//FGTXWAY 為交易機制
		if("0".equals(getParams().get("FGTXWAY"))) {   // 交易密碼
			if (getParams().containsKey("__SCHID"))
				getParams().put("CERTACN", StrUtils.repeat("2", 19));	//預約
			else
				getParams().put("CERTACN", StrUtils.repeat(" ", 19));	//即時
		}
		else if("1".equals(getParams().get("FGTXWAY"))) {  //憑證
			if (getParams().containsKey("__SCHID"))
				getParams().put("CERTACN", StrUtils.repeat("3", 19));	//預約
			else
				getParams().put("CERTACN", StrUtils.repeat("1", 19));	//即時
		}
		else if("2".equals(getParams().get("FGTXWAY"))){  //晶片金融卡
			params.put("XMLCN", "");
			params.put("XMLCA", "");
		}
		else if("3".equals(getParams().get("FGTXWAY"))){ //OTP驗證
			params.put("XMLCN", "");
			params.put("XMLCA", "");
	    }
		else if("4".equals(getParams().get("FGTXWAY"))){  //自然人憑證
			params.put("XMLCN", "");
			params.put("XMLCA", "");
			if (getParams().containsKey("__SCHID"))
				getParams().put("CERTACN", StrUtils.repeat("9", 19));	//預約
			else
				getParams().put("CERTACN", StrUtils.repeat("8", 19));	//即時
		}
		else if("5".equals(getParams().get("FGTXWAY"))){  //蓋特
			if (getParams().containsKey("__SCHID"))
				getParams().put("CERTACN", StrUtils.repeat("D", 19));	//預約
			else
				getParams().put("CERTACN", StrUtils.repeat("C", 19));	//即時
		}		
		else if("6".equals(getParams().get("FGTXWAY"))){  //自然人憑證
			params.put("XMLCN", "");
			params.put("XMLCA", "");
			getParams().put("CERTACN", StrUtils.repeat("B", 19));	//即時
		}		
		else if("7".equals(getParams().get("FGTXWAY"))){  //自然人憑證
			params.put("XMLCN", "");
			params.put("XMLCA", "");
		}
		else
		{
log.debug("@@@ MAC in else");
log.debug("@@@ MAC in getParams()=="+getParams());

			throw new TelCommException(" 無法決定交易型態.");
		}
		String trancode=getParams().get("trancode")==null ? "" : (String)getParams().get("trancode");
		log.debug("MAC.java trancode:"+trancode+"================");
		if(getParams().get("CUSIDN")!=null && (trancode.indexOf("N361")>-1 || trancode.indexOf("N366")>-1 || trancode.indexOf("N367")>-1))
		{
		    String newStr = "";
		    CUSIDN = (String)getParams().get("CUSIDN");
		    for ( int counter = 0; counter < CUSIDN.length(); counter++ ) {
		      log.debug(CUSIDN.substring(counter, counter + 1));
		      if (CUSIDN.charAt(counter) >= 'A' && CUSIDN.charAt(counter) <= 'Z' ) {
		        newStr = newStr + "0";
		      } else if ( CUSIDN.charAt(counter) >= 'a' && CUSIDN.charAt(counter) <= 'z' ) {
		        newStr = newStr + "0";
		      } else
		        newStr = newStr + CUSIDN.substring(counter, counter + 1);
		    }
		    params.put("CUSIDN", newStr);
		}
		String __mac = iteratorFmtPattern(define);
		params.put("__MAC", __mac);
	    String __mac64 = StrUtils.left(__mac + StrUtils.repeat("0", 64), 64);
		params.put("__MAC64", __mac64);
		params.put("__DEFINE", define);
		/*
		KeyClient client = new KeyClient();
		String result = KeyClientUtils.doRequest(
				IKeySpi.CREATE_MAC, 
				KeyLabel.KEYLABEL_MACKEY, //"B05000NNBTMAC"
				client.toEBCDIC(StrUtils.left(StrUtils.right(DateTimeUtils.getCDateShort(new Date()), 6) + "00000000", 8).getBytes()), 
				client.toEBCDIC(__mac64.getBytes()), 
				"0128");
		
		if(!StrUtils.trim(result).startsWith("ERROR")) {
			try {
	    		params.put("MAC", result.substring(0, 8));
	    	}
	    	catch(RuntimeException e) {
	    		throw TopMessageException.create("Z300"); //押解碼錯誤 
	    	}
		}
		else {
			throw TopMessageException.create("Z300"); //押解碼錯誤
		}
		*/
		String result = "";
		String isSuccess = "";
		try {
			
				result = VaCheck.getMAC(__mac64);
				if(result!=null&&result.length()>0) {
					try {
						log.warn("MAC = " + result.substring(0, 8));
			    		params.put("MAC", result.substring(0, 8));
			    	}
			    	catch(RuntimeException e) {
			    		log.warn("getMAC ERROR: result substring error");
			    		throw TopMessageException.create("Z300"); //押解碼錯誤
			    	}
				}
				else {
					log.warn("getMAC ERROR: result size null or zero");
					throw TopMessageException.create("Z300"); //押解碼錯誤
				}							
		}
		catch (Exception e)
		{
			log.warn("getMAC ERROR:"+e.getMessage());
			throw TopMessageException.create("Z300");
		}

		if(getParams().get("CUSIDN")!=null && (trancode.indexOf("N361")>-1 || trancode.indexOf("N366")>-1 || trancode.indexOf("N367")>-1))
			params.put("CUSIDN", CUSIDN);
		return "success";
	}
	
	private String iteratorFmtPattern(String define) {
		Map<String, Object> paramsClone = new HashMap();
		
		Set<String> seq = new LinkedHashSet();
		try {
			Pattern regex = Pattern.compile("\\{([^,]+?),(\\s*.+?)\\}\\s*");
			Matcher regexMatcher = regex.matcher(define);
			while (regexMatcher.find()) {
				String field = StrUtils.trim(regexMatcher.group(2));
				//log.debug(field + ": " + getParams().get(field));
				seq.add(field);
				if(!paramsClone.containsKey(field))  //若在 colne 裡不存在, 才從原始的 params 裡取值
					paramsClone.put(field, (String)getParams().get(field));  //don't trim the value
				eachFmt(paramsClone, StrUtils.trim(regexMatcher.group(1)), field);
			}
		} catch (PatternSyntaxException ex) {
			log.error("", ex);
		}
		
		StringBuffer result = new StringBuffer();
		for(String s : seq) {
			log.debug("append seq : " + s);
			result.append(paramsClone.get(s));
		}
		return result.toString().replaceAll("\\.", "");
	}
	
	private Pattern numPattern = Pattern.compile("%\\d(\\.*\\d*)+[d|f]");
	
	private Pattern zPattern = Pattern.compile("%(-?\\d+)z");   //左靠右補0
	
	private void eachFmt(Map params, String fmt, String field) {
		String r = "";
		if(numPattern.matcher(fmt).matches()) {
			if(fmt.endsWith("d")) {
				r = String.format(fmt, new Long((String)params.get(field)));
			} else {
				
				//為了跟電文 match，根據 format無條件捨去後面小數位。
				int decimal_location = 0;
				String fields = (String)params.get(field);
				if (fmt.indexOf(".") != -1 && fmt.indexOf("f") != -1){
					decimal_location = Integer.parseInt(fmt.substring(fmt.indexOf(".")+1, fmt.indexOf("f")));
					log.debug("decimal_location = "+decimal_location);
				}
				if (fields.indexOf(".") != -1 && fields.length()> fields.indexOf(".")+1+decimal_location){
					log.debug("change field before = "+fields);
					fields = fields.substring(0,fields.indexOf(".")+1+decimal_location);
					log.debug("change field after = "+fields);
				}
				
				r = String.format(fmt, new BigDecimal(fields));
			}
			r = r.replaceAll("\\.", "");  //把小數點拿掉
		}
		else if(zPattern.matcher(fmt).matches()) {
			fmt = fmt.replace("z", "s").replace("%", "%-").replace("--", "-");  // 變成 %-10s 左靠右補空白
			//log.debug("fmt: " + fmt);
			r = String.format(fmt, (String)params.get(field));
			r = r.replaceAll("\\s", "0");  //把空白換為 0
			//log.debug("r: " + r);
		}
		else if(fmt.endsWith("s")) {
			String digit = fmt.replaceAll("s", "").replaceAll("%", "-").replaceAll("--", "-");
			Integer num = Math.abs(new Integer(digit));
			r = String.format(fmt, (String)params.get(field));
			r = StrUtils.left(r, num);
		}
		else {
			r = String.format(fmt, params.get(field));
		}
		params.put(field, r);
	}
	
//	public static void main(String[] args) {
//		
//		String define="{%-11s, FUNDACN} {%014.2f, FUNDAMT} {%-10s, FUNDAMT} {9000000000000000, CREDITNO}";
//		//ACN,TSFACN,AMOUNT,CERTACN
//		
//		Hashtable h = new Hashtable();
//		h.put("FUNDACN", "10000000001");
//		//h.put("TSFACN", "20000100002");
//		h.put("FUNDAMT", "55000.00");
//		h.put("CREDITNO", "");
//		MAC2 mac = new MAC2();
//		mac.setDefine(define);
//		mac.setParams(h);
//		String __mac = mac.iteratorFmtPattern(define);
//		//mac.doCommand();
//		
//		log.debug(__mac);
//		log.debug("__mac length: " + __mac.length());
//		String __mac64 = StrUtils.left(__mac + StrUtils.repeat("0", 64), 64);
//		log.debug("__mac64: " + __mac64);
//		log.debug("\r\n-------------------------------------------------------------------");
//		String s = "%-11s".replaceAll("s", "").replaceAll("%", "-").replaceAll("--", "-");
//		log.debug("s = " + s);
//		//String p="%11s%16s%010d%19s";
//		log.debug("%11s\n", "1");
//		log.debug("%-11s\n", "1");
//		log.debug("%-11s\n", "1");
//		log.debug("%10d\n", 1);
//		log.debug("%010d\n", 1);
//	}
}
