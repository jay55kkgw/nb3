package fstop.telcomm.cmd;

import java.util.Map;

import org.apache.log4j.Logger;

import fstop.exception.TelCommException;
import fstop.exception.TopMessageException;
import fstop.util.StrUtils;


/**
 * 依某個欄位重覆的次數, 當作是總筆數
 * @author Owner
 *
 */
public class TRANSPIN extends BeforeTelcommQueryCommand {
	private Logger logger = Logger.getLogger(getClass());
	
	@Override
	public Object doCommand() {
		
		String define = this.getDefine();
		Map<String, String> params = this.getParams();

		if(params.containsKey("__TRANSPIN"))
			return "success";

		String transpinStr = StrUtils.trim(params.get("TRANSPIN"));
		params.put("__TRANSPIN", transpinStr);
		
		if(transpinStr.length() == 0)
			throw new TelCommException("上傳欄位中沒有 TRANSPIN .");
		
		//右補 0
		transpinStr = StrUtils.left(transpinStr + StrUtils.repeat("0", 48), 48);

/*
		String iv = DateTimeUtils.format("MMdd", new Date());
		int t = new Integer(iv);
		t = t + 1;
		
		iv = StrUtils.right("00000000" + t, 8);
*/
		StringBuffer resultBuffer = new StringBuffer();
		
//		KeyClient client = new KeyClient();
		boolean isSuccess = true;
		
		int[] strLens = new int[]{ 16, 32, 48};
		int start = 0;
		for(int i=0; i < strLens.length; i++) {
			String shaStr = transpinStr.substring(0, strLens[i]);
			start += strLens[i];
			String result ="";
//			String result = KeyClientUtils.doRequest(
//					IKeySpi.CREATE_MAC, 
//					KeyLabel.KEYLABEL_PPKEY,
//					client.toEBCDIC(StrUtils.left(StrUtils.right(DateTimeUtils.getCDateShort(new Date()), 6) + "00000000", 8).getBytes()), 
//					shaStr, 
//					StrUtils.right("0000" + strLens[i], 4));
	
			if(!StrUtils.trim(result).startsWith("ERROR")) {
				try {
					resultBuffer.append(result);
		    	}
		    	catch(RuntimeException e) {
		    		throw TopMessageException.create("Z300"); //押解碼錯誤 
		    	}
		    	isSuccess &= true;
			}
			else {
				isSuccess &= false;
				throw TopMessageException.create("Z300"); //押解碼錯誤
			}
		}

		if(isSuccess == true) {
			params.put("TRANSPIN", resultBuffer.toString());
			return "success";
		}
		else
			return "fail";
	}
	

}
