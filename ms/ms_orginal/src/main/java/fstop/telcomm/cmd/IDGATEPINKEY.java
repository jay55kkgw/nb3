package fstop.telcomm.cmd;

import java.util.Map;

import com.netbank.rest.e2e.E2EClient;
import com.netbank.rest.e2e.MB3_E2EClient;

import fstop.exception.TelCommException;
import fstop.exception.TopMessageException;
import fstop.model.MVHImpl;
import fstop.telcomm.TelCommExec;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import fstop.va.VaCheck;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 功能說明 :IDGATE 啟用碼加密，不發送電文
 *
 */
@Slf4j
public class IDGATEPINKEY extends BeforeTelcommQueryCommand {
	@Override
	public Object doCommand() {
		log.debug(" doCommand IDGATEPINKEY...");
		String define = "";
		String pinkey = "PINKEY";
        String login_type = "";

		if(this.getDefine().indexOf(",") != -1){
			define = this.getDefine().substring(0, this.getDefine().indexOf(","));
			pinkey = this.getDefine().substring(this.getDefine().indexOf(",")+1);
		}
		
		Map<String, String> params = this.getParams();
        login_type = params.get("LOGINTYPE");
        log.warn("PINNEW1 doCommand login_type = " + login_type);


		log.warn("IDGATEPINKEY doCommand define = " + define);
		log.warn("IDGATEPINKEY doCommand pinkey = " + pinkey);
		log.warn("IDGATEPINKEY doCommand params = " + params);
		String pinnew="";
		
		
		pinnew = params.get(pinkey);
		params.put("__" + pinkey, pinnew);
		if(pinnew.length()==48 || getParams().containsKey("__TRANS_STATUS"))
			return "success";
		log.debug("pinnew1>>{}",pinnew);
		//右補 0
//		pinnew = StrUtils.left(pinnew + StrUtils.repeat("0", 48), 48);
//		log.debug("pinnew2>>{}",pinnew);

		
		
		
		
		String result = "";
		String isSuccess = "";
		try {
				if("Y".equalsIgnoreCase(E2EClient.e2e().e2e.getIsE2E())) {
                    if("MB".equals(login_type)) {
                        result = MB3_E2EClient.getPINNEW(pinnew);
                    }else {
                    	result = E2EClient.getPINNEW(pinnew);
                    }
				}else {
					pinnew = StrUtils.left(pinnew + StrUtils.repeat("0", 48), 48);
					log.debug("pinnew2>>{}",pinnew);
					
					result = VaCheck.getPINNEW(pinnew,"PPKEY");
				}
				params.put(pinkey, result.toString()); 
				
				log.warn(pinkey+" RC="+params.get(pinkey));
				isSuccess ="success";  
		}
		catch (Exception e)
		{
			isSuccess ="fail"; 
			log.warn("IDGATEPINKEY ERROR:"+e.getMessage());
			throw TopMessageException.create("Z300");
		}

		return isSuccess;
	}
	
	

}
