package fstop.telcomm.cmd;

import fstop.exception.UncheckedException;

public class CommandDefineFormatError extends UncheckedException { 
  
	private static final long serialVersionUID = -8889760240232522705L;
	
	public CommandDefineFormatError(String msg) {
		super(msg);
	}
	
	public CommandDefineFormatError(String msg, Throwable cause) {
		super(msg, cause);
	}
}
