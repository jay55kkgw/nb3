package fstop.telcomm.cmd;

import java.util.Map;

import com.netbank.rest.e2e.E2EClient;
import com.netbank.rest.e2e.MB3_E2EClient;

import fstop.exception.TopMessageException;
import fstop.util.StrUtils;
import fstop.va.VaCheck;
import lombok.extern.slf4j.Slf4j;


/**
 * 依某個欄位重覆的次數, 當作是總筆數
 * @author Owner
 *
 */
@Slf4j
public class PINOLD1 extends BeforeTelcommQueryCommand {
	
	@Override
	public Object doCommand() {
		log.debug(" doCommand");
		
		String pinkey = "PINOLD";
        String login_type = "";

		log.warn("PINOLD1.getDefine(): " + this.getDefine());
		
		String define = this.getDefine() != null ? this.getDefine() : "";
		
		Map<String, String> params = this.getParams();
        login_type = params.get("LOGINTYPE");
        log.warn("PINNEW1 doCommand login_type = " + login_type);

		log.warn("PINOLD1 doCommand define = " + define);
		log.warn("PINOLD1 doCommand pinkey = " + pinkey);
		log.warn("PINOLD1 doCommand params = " + params);
		
		String pinnew = "";
		
		if(define != "") {
			log.debug("PINOLD1.define: " + define);
			if(StrUtils.trim((String)getParams().get(define)).length() == 48 ) {
				log.warn("PINOLD1.lenght() = " + StrUtils.trim((String)getParams().get(define)).length());
				return "success";
			}

			if(StrUtils.isNotEmpty(params.get(define))) {
				log.debug("PINOLD1 doCommand params PINNEW = " + params.get(define));
				pinnew = StrUtils.trim(params.get(define));
			}
			log.debug("PINOLD1 pinnew = " + pinnew);
		}
	
		params.put("__" + define, pinnew);
	
		//右補 0
//		pinnew = StrUtils.left(pinnew + StrUtils.repeat("0", 48), 48);
	
		String result = "";
		String isSuccess = "";
		try {
			if("Y".equalsIgnoreCase(E2EClient.e2e().e2e.getIsE2E())) {
                if("MB".equals(login_type)) {
                    result = MB3_E2EClient.getPINNEW(pinnew);
                }else {
                	result = E2EClient.getPINNEW(pinnew);
                }
			}else {
				//右補 0
				pinnew = StrUtils.left(pinnew + StrUtils.repeat("0", 48), 48);
				result = VaCheck.getPINNEW(pinnew,"PPKEY");
			}	
//				result = VaCheck.getPINNEW(pinnew,"PPKEY");
				params.put(define, result.toString()); 
				log.warn(define + " RC=" + params.get(define));
				isSuccess ="success";  
		}
		catch (Exception e)
		{
			isSuccess ="fail"; 
			log.warn("getPINNEW ERROR:"+e.getMessage());
			throw TopMessageException.create("Z300");
		}

		return isSuccess;
	}
	

}
