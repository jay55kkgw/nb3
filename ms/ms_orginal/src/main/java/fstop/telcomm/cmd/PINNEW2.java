package fstop.telcomm.cmd;

import java.util.Map;

import org.apache.log4j.Logger;

import fstop.exception.TelCommException;
import fstop.exception.TopMessageException;
import fstop.telcomm.TelCommExec;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import fstop.va.VaCheck;


/**
 * 依某個欄位重覆的次數, 當作是總筆數
 * @author Owner
 *
 */
public class PINNEW2 extends BeforeTelcommQueryCommand {
	private Logger logger = Logger.getLogger(getClass());
	
	@Override
	public Object doCommand() {
		logger.debug(" doCommand");
		String define = "";
		String pinkey = "PINNEW";

		if(this.getDefine().indexOf(",") != -1){
			define = this.getDefine().substring(0, this.getDefine().indexOf(","));
			pinkey = this.getDefine().substring(this.getDefine().indexOf(",")+1);
		}
		
		Map<String, String> params = this.getParams();

		logger.warn("PINNEW doCommand define = " + define);
		logger.warn("PINNEW doCommand pinkey = " + pinkey);
		logger.warn("PINNEW doCommand params = " + params);
		String pinnew="";
		
		if(StrUtils.trim((String)getParams().get("FGTXWAY")).length() != 0) {
			logger.debug("FGTXWAY length != 0");
			if(!"0".equals(getParams().get("FGTXWAY"))) {   // 不為交易密碼
				logger.debug(getParams().get("TXID") + " SKIP PINKEY (FGTXWAY: " + getParams().get("FGTXWAY") + ")");
				return "skip";
			}
			else {
				if(StrUtils.trim((String)getParams().get("PINNEW")).length() == 48 || getParams().containsKey("__TRANS_STATUS"))
					return "success";


				if(StrUtils.isNotEmpty(params.get("PINNEW")))
				{
					logger.debug("PINNEW doCommand params PINNEW = " + params.get("PINNEW"));
					pinnew=StrUtils.trim(params.get("PINNEW"));
				}
				else if(StrUtils.isNotEmpty(params.get("N950PASSWORD"))) // N094 無PINNEW 所以需要N950PASSWORD 為什麼 FGTXWAY=0 無CMPASSWORD 因為MAC會用到
				{
					logger.debug("PINNEW doCommand params N950PASSWORD = " + params.get("N950PASSWORD"));
					pinnew=StrUtils.trim(params.get("N950PASSWORD"));
				}
				else
				{
						throw new TelCommException("上傳欄位中沒有 PINNEW .");
				}
				logger.debug("PINNEW pinnew = " + pinnew);
				//做交易時, 選擇了使用 "交易密碼"
				TelCommExec n951CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n951CMTelcomm");
				//發 N950
//				if((BOLifeMoniter.getValue("__DO_N950") == null)) {
//					BOLifeMoniter.setValue("__DO_N950", "true");
//					//發電文時, n950 會給 PPSYNC, 及 CMPASSWORD
//					//若 回傳 E004, 則會產生 TopMessageException
//					MVHImpl n950Result = n951CMTelcomm.query(params);
//					params.put("__DO_N950", "true");
//				}
			}
		}
		else
		{
			logger.debug("FGTXWAY length = 0");
			if(pinkey.equals("HLOGINPIN"))
			{
				logger.debug("PINNEW doCommand params HLOGINPIN = " + params.get("HLOGINPIN"));
				pinnew=StrUtils.trim(params.get("HLOGINPIN"));
				pinkey="HLOGINPIN";
			}
			else
			if(pinkey.equals("HTRANSPIN"))
			{
				logger.debug("PINNEW doCommand params HTRANSPIN = " + params.get("HTRANSPIN"));
				pinnew=StrUtils.trim(params.get("HTRANSPIN"));
				pinkey="HTRANSPIN";
			}
			else if(StrUtils.isNotEmpty(params.get("PINNEW")))
			{
				logger.debug("PINNEW doCommand params PINNEW = " + params.get("PINNEW"));
				pinnew=StrUtils.trim(params.get("PINNEW"));
			}
			else
			if(StrUtils.isNotEmpty(params.get("N950PASSWORD")))
			{
				logger.debug("PINNEW doCommand params N950PASSWORD = " + params.get("N950PASSWORD"));
				pinnew=StrUtils.trim(params.get("N950PASSWORD"));
			}
			else
			{
				
				//throw new TelCommException("上傳欄位中沒有 PINNEW .");
				pinnew="";
			}
		}

		params.put("__" + pinkey, pinnew);
		if(pinnew.length()==48 || getParams().containsKey("__TRANS_STATUS"))
			return "success";

		//右補 0
		pinnew = StrUtils.left(pinnew + StrUtils.repeat("0", 48), 48);
/*
		String iv = DateTimeUtils.format("MMdd", new Date());
		int t = new Integer(iv);
		t = t + 1;
		
		iv = StrUtils.right("00000000" + t, 8);
*/

/*		
		StringBuffer resultBuffer = new StringBuffer();
		
		KeyClient client = new KeyClient();
		boolean isSuccess = true;
		
		int[] strLens = new int[]{ 16, 32, 48};
		int start = 0;
		for(int i=0; i < strLens.length; i++) {
			String shaStr = pinnew.substring(0, strLens[i]);
			start += strLens[i];
			logger.debug(params.get("TXID") + " doCommand call KeyClientUtils.doRequest");
			
			String result = KeyClientUtils.doRequest(
					IKeySpi.CREATE_MAC, 
					KeyLabel.KEYLABEL_PPKEY,
					client.toEBCDIC(StrUtils.left(StrUtils.right(DateTimeUtils.getCDateShort(new Date()), 6) + "00000000", 8).getBytes()), 
					shaStr, 
					StrUtils.right("0000" + strLens[i], 4));
	
			if(!StrUtils.trim(result).startsWith("ERROR")) {
				try {
					resultBuffer.append(result);
		    	}
		    	catch(RuntimeException e) {
		    		throw TopMessageException.create("Z300"); //押解碼錯誤 
		    	}
		    	isSuccess &= true;
			}
			else {
				isSuccess &= false;
	    		throw TopMessageException.create("Z300"); //押解碼錯誤
			}
		}
*/
		String result = "";
		String isSuccess = "";
		try {
				result = VaCheck.getPINNEW(pinnew,"PPKEY2");
				params.put(pinkey, result.toString()); 
				logger.warn(pinkey+" RC="+params.get(pinkey));
				isSuccess ="success";  
		}
		catch (Exception e)
		{
			isSuccess ="fail"; 
			logger.warn("getPINNEW ERROR:"+e.getMessage());
			throw TopMessageException.create("Z300");
		}

		return isSuccess;
	}
	

}
