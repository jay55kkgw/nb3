package fstop.telcomm.cmd;

import java.util.Map;

import org.apache.log4j.Logger;

import fstop.exception.TopMessageException;
import fstop.va.VaCheck;
import lombok.extern.slf4j.Slf4j;


/**
 * 依某個欄位重覆的次數, 當作是總筆數
 * @author Owner
 *
 */
@Slf4j
public class SYNC1 extends BeforeTelcommQueryCommand {
	private Logger log = Logger.getLogger(getClass());
	
	@Override
	public Object doCommand() {
		
		String define = this.getDefine();
		
		Map<String, String> params = this.getParams();
		log.warn("SYNC1.params : " + params);
		
		/*		
		String tommorrow = DateTimeUtils.format("MMdd", new Date());
		int t = new Integer(tommorrow);
		t = t + 1;
		tommorrow = StrUtils.right("00000000" + t, 8);
        KeyClient client = new KeyClient();
		String result = KeyClientUtils.doRequest(
				IKeySpi.CREATE_SYNC, 
				KeyLabel.KEYLABEL_MACKEY, //"B05000NNBTMAC", 
				client.toEBCDIC(StrUtils.left(StrUtils.right(DateTimeUtils.getCDateShort(new Date()), 6) + "00000000", 8).getBytes()),
				client.toEBCDIC(tommorrow.getBytes()), 
				"0016");
		*/
		String result="";
		try {
			result = VaCheck.getSYNC();
			if(result!=null&&result.length()>0) {
				try {
					log.warn("SYNC = " + result.substring(0, 8));
		    		params.put("SYNC", result.substring(0, 8));
		    	}
		    	catch(RuntimeException e) {
		    		throw TopMessageException.create("Z300"); //押解碼錯誤
		    	}
			}
			else {
				throw TopMessageException.create("Z300"); //押解碼錯誤

			}			 
		}
		catch (Exception e)
		{
			log.warn("getSYNC ERROR:"+e.getMessage()); 
			throw TopMessageException.create("Z300");
		}		
		 
		return "success";
	}
	

}
