package fstop.telcomm.cmd;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import fstop.exception.TopMessageException;
import fstop.model.TelcommResult;
import fstop.util.StrUtils;

/*
 * 比較上行電文的指定欄位與下行電文的指定欄位其值是否相同，比較結果不同時拋錯TopMessageException:Z009,
 * 此函數僅為防止下行電文錯位之補強，故若發生設定錯誤或非預期錯誤時不執行比較，交易繼續。
 * 例:CompareFields(ACN,OUTACN) -> ACN為上行電文欄位，OUTACN為下行電文欄位
 */
public class CompareFields extends AfterTelcommQueryCommand  {
	private Logger logger = Logger.getLogger(getClass());
	public String doCommand() {
		
		Map params = this.getParams();
		TelcommResult result = this.getQueryResult();
		
		String titaValue = "";
		String totaValue = "";
		try {
			String[] fieldNames = this.getFieldName().split(",");
			String titaName = fieldNames[0].trim();
			String totaName = fieldNames[1].trim();
			
			if (fieldNames.length != 2)
				throw new CommandExecException("電文欄位名稱的格式設定錯誤 ! (cmd: " + getClass().getSimpleName() + ", fieldNames: " + this.getFieldName()+ ")");
			
			if (!params.containsKey(titaName))
				throw new CommandExecException("上行電文無定義的欄位, (請檢查設定) ! (cmd: " + getClass().getSimpleName() + ", fieldNames: " + this.getFieldName()+ ")");
			
			titaValue = (String)params.get(titaName);
			
			List v = result.getTelcommVector();
			for(Object o : v) {
				Map rec = (Map)o;
				if(!rec.containsKey(fieldNames[1])) {
					throw new CommandExecException("下行電文無定義的欄位, (請檢查設定) ! (cmd: " + getClass().getSimpleName() + ", fieldNames: " + this.getFieldName()+ ")");
				}
				totaValue = (String)rec.get(totaName);
				
				if(StrUtils.isEmpty(totaValue))
					continue;
			}
			
			if (!titaValue.equals(totaValue))
				throw TopMessageException.create("Z009");
		}
		catch(CommandExecException e) {
			logger.error(e.getMessage());
		}
		catch(TopMessageException e) {
			logger.error("比對上行電文指定欄位值與下行電文指定欄位值不一致! (cmd: " + getClass().getSimpleName() + ", fieldNames: " + this.getFieldName()+ ", titaValue: '" + titaValue + "', totaValue: '" + totaValue + "')", e);
			throw e;
		}
		catch(Exception e) {
			logger.error("取得電文欄位資料時發生錯誤, (請檢查設定) ! (cmd: " + getClass().getSimpleName() + ", fieldNames: " + this.getFieldName()+ ", titaValue: '" + titaValue + "', totaValue: '" + totaValue + "')", e);
		}
		
		return "success";
	}

}
