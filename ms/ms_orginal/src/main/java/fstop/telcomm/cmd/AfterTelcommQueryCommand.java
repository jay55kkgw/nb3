package fstop.telcomm.cmd;

import fstop.model.TelcommResult;
import java.util.Hashtable;
import java.util.Map;

public abstract class AfterTelcommQueryCommand implements TelcommCommand {
	private String fieldName;

	private TelcommResult queryResult;	//發電文後結果
	
	private Map params;	//發電文前參數

	public AfterTelcommQueryCommand() {
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public TelcommResult getQueryResult() {
		return queryResult;
	}

	public void setQueryResult(TelcommResult queryResult) {
		this.queryResult = queryResult;
	}
	
	public Map getParams() {
		return params;
	}

	public void setParams(Map params) {
		this.params = params;
	}

	public abstract Object doCommand();
}
