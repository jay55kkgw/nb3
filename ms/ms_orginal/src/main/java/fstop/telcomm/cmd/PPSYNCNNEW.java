package fstop.telcomm.cmd;

import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.netbank.rest.e2e.E2EClient;
import com.netbank.rest.e2e.MB3_E2EClient;
import com.twca.framework.mb.TransSecureSrv;

import fstop.exception.TelCommException;
import fstop.exception.TopMessageException;
import fstop.model.MVHImpl;
import fstop.telcomm.TelCommExec;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import fstop.va.VaCheck;
import lombok.extern.slf4j.Slf4j;


/**
 * 依某個欄位重覆的次數, 當作是總筆數
 * @author Owner
 *
 */
@Slf4j
public class PPSYNCNNEW extends BeforeTelcommQueryCommand {

	@Override
	public Object doCommand() {
		log.debug("PPSYNCNNEW doCommand");
		
		Boolean isE2E = "Y".equalsIgnoreCase(E2EClient.e2e().e2e.getIsE2E());
		log.info("isE2E>>{}",isE2E);
		if(isE2E) {
			return callE2E();
		}else {
			return callVA();
		}
		
		
	}
	
	public String callE2E() {
		log.info("call E2E ...");
        String login_type = "";
        Map<String, String> params = this.getParams();
        login_type = params.get("LOGINTYPE");
        log.warn("PPSYNCN1.login_type : {}" , login_type);
        
		// --------------------------E2ELogin-------------------------------
		try {
            if("MB".equals(login_type)) {
                
                if(!MB3_E2EClient.login()) {
                    throw new Exception("MB3_E2EClient.login Fail...");
                }
            }else {
                
                if(!E2EClient.login()) {
                    throw new Exception("E2EClient.login Fail...");
                }
            }
		} catch (Exception e) {
			log.error("E2ELogin ERROR:"+e.getMessage());
			throw TopMessageException.create("Z300");
		}
		// ---------------------------E2ELogin-------------------------------
		
		// ---------------------------getPPSYNC-------------------------------
		log.warn("PPSYNCN1.params : " + params);
		
		String result = "";
		String status = "";
		
		try {
            if("MB".equals(login_type)) {
                
                result = MB3_E2EClient.getPPSYNCN911();
            }else {
            	result = E2EClient.getPPSYNCN911();
            }
			if(result!=null&&result.length()>0) {
				try {
					log.warn("PPSYNCN = " + result.substring(0, 8));
		    		params.put("PPSYNCN", result.substring(0, 8));
		    		status= "success";
		    		
		    	} catch(RuntimeException e) {
		    		status= "fail";
		    		throw TopMessageException.create("Z300"); //押解碼錯誤
		    	}
				
			} else {
				status= "fail";
				throw TopMessageException.create("Z300"); //押解碼錯誤
			}			 
			
		} catch (Exception e) {
			status= "fail";
			log.warn("getPPSYNC ERROR:"+e.getMessage()); 
			throw TopMessageException.create("Z300");
		}

		if("fail".equals(status)) {
			return status;
		}
		// ---------------------------getPPSYNC-------------------------------
		
		// ---------------------------getPINNEW-------------------------------
		String define = "";
		String pinkey = "PINNEW";

		if(this.getDefine().indexOf(",") != -1){
			define = this.getDefine().substring(0, this.getDefine().indexOf(","));
			pinkey = this.getDefine().substring(this.getDefine().indexOf(",")+1);
		}
		
		Map<String, String> params1 = this.getParams();

		log.warn("PINNEW1 doCommand define = " + define);
		log.warn("PINNEW1 doCommand pinkey = " + pinkey);
		log.warn("PINNEW1 doCommand params1 = " + params1);
		String pinnew="";
		
		if(pinkey.equals("HLOGINPIN")) {
			log.debug("PINNEW1 doCommand params1 HLOGINPIN = " + params1.get("HLOGINPIN"));
			pinnew=StrUtils.trim(params1.get("HLOGINPIN"));
			pinkey="HLOGINPIN";
			
		} else if(pinkey.equals("HTRANSPIN")) {
			log.debug("PINNEW1 doCommand params1 HTRANSPIN = " + params1.get("HTRANSPIN"));
			pinnew=StrUtils.trim(params1.get("HTRANSPIN"));
			pinkey="HTRANSPIN";
			
		} else if (StrUtils.isNotEmpty(params1.get("PINNEW"))) {
			log.debug("PINNEW1 doCommand params1 PINNEW = " + params1.get("PINNEW"));
			pinnew=StrUtils.trim(params1.get("PINNEW"));
			
		} else if(StrUtils.isNotEmpty(params1.get("N950PASSWORD"))) {
			log.debug("PINNEW doCommand params1 N950PASSWORD = " + params1.get("N950PASSWORD"));
			pinnew=StrUtils.trim(params1.get("N950PASSWORD"));
			
		} else {
			pinnew="";
		}

		params1.put("__" + pinkey, pinnew);
		if(pinnew.length()==48 || getParams().containsKey("__TRANS_STATUS"))
			return "success";
		log.debug("pinnew1>>{}",pinnew);
		
		String result1 = "";
		String isSuccess = "";
		try {

            if("MB".equals(login_type)) {
                result1 = MB3_E2EClient.getPINNEWN911(pinnew);
            }else {
            	result1 = E2EClient.getPINNEWN911(pinnew);
            }
			params1.put(pinkey, result1.toString()); 
			
			log.warn(pinkey+" RC="+params1.get(pinkey));
			
			
			isSuccess ="success";
		} catch (TopMessageException e) {
			log.error("{}", e);
			throw TopMessageException.create(e.getMsgcode());
		} catch (Exception e) {
			isSuccess ="fail"; 
			log.warn("getPINNEW ERROR:"+e.getMessage());
			throw TopMessageException.create("Z300");
		}
		// ---------------------------getPINNEW-------------------------------

		return isSuccess;
	} 
	
	public String callVA() {
		log.info("call VA ...");
		TransSecureSrv sso = null;
		String sTransSeq_N911 = UUID.randomUUID().toString();
		
		// ---------------------------ssoLogin-------------------------------
		try {
			sso = VaCheck.ssoLogin(sTransSeq_N911);
			
		} catch (Exception e) {
			log.warn("ssoLogin ERROR:"+e.getMessage());
			throw TopMessageException.create("Z300");
		}
		// ---------------------------ssoLogin-------------------------------
		
		// ---------------------------getPPSYNC-------------------------------
		Map<String, String> params = this.getParams();
		log.warn("PPSYNCN1.params : " + params);
		
		String result = "";
		String status = "";
		
		try {
			result = VaCheck.getPPSYNCN911("PPKEY", sso);
			
			if(result!=null&&result.length()>0) {
				try {
					log.warn("PPSYNCN = " + result.substring(0, 8));
		    		params.put("PPSYNCN", result.substring(0, 8));
		    		status= "success";
		    		
		    	} catch(RuntimeException e) {
		    		status= "fail";
		    		VaCheck.ssoLogout(sso, sTransSeq_N911);
		    		throw TopMessageException.create("Z300"); //押解碼錯誤
		    	}
				
			} else {
				status= "fail";
				VaCheck.ssoLogout(sso, sTransSeq_N911);
				throw TopMessageException.create("Z300"); //押解碼錯誤
			}			 
			
		} catch (Exception e) {
			status= "fail";
			log.warn("getPPSYNC ERROR:"+e.getMessage()); 
			throw TopMessageException.create("Z300");
		}

		if("fail".equals(status)) {
			VaCheck.ssoLogout(sso, sTransSeq_N911);
			return status;
		}
		// ---------------------------getPPSYNC-------------------------------
		
		// ---------------------------getPINNEW-------------------------------
		String define = "";
		String pinkey = "PINNEW";

		if(this.getDefine().indexOf(",") != -1){
			define = this.getDefine().substring(0, this.getDefine().indexOf(","));
			pinkey = this.getDefine().substring(this.getDefine().indexOf(",")+1);
		}
		
		Map<String, String> params1 = this.getParams();

		log.warn("PINNEW1 doCommand define = " + define);
		log.warn("PINNEW1 doCommand pinkey = " + pinkey);
		log.warn("PINNEW1 doCommand params1 = " + params1);
		String pinnew="";
		
		if(pinkey.equals("HLOGINPIN")) {
			log.debug("PINNEW1 doCommand params1 HLOGINPIN = " + params1.get("HLOGINPIN"));
			pinnew=StrUtils.trim(params1.get("HLOGINPIN"));
			pinkey="HLOGINPIN";
			
		} else if(pinkey.equals("HTRANSPIN")) {
			log.debug("PINNEW1 doCommand params1 HTRANSPIN = " + params1.get("HTRANSPIN"));
			pinnew=StrUtils.trim(params1.get("HTRANSPIN"));
			pinkey="HTRANSPIN";
			
		} else if (StrUtils.isNotEmpty(params1.get("PINNEW"))) {
			log.debug("PINNEW1 doCommand params1 PINNEW = " + params1.get("PINNEW"));
			pinnew=StrUtils.trim(params1.get("PINNEW"));
			
		} else if(StrUtils.isNotEmpty(params1.get("N950PASSWORD"))) {
			log.debug("PINNEW doCommand params1 N950PASSWORD = " + params1.get("N950PASSWORD"));
			pinnew=StrUtils.trim(params1.get("N950PASSWORD"));
			
		} else {
			pinnew="";
		}

		params1.put("__" + pinkey, pinnew);
		if(pinnew.length()==48 || getParams().containsKey("__TRANS_STATUS"))
			return "success";
		log.debug("pinnew1>>{}",pinnew);
		//右補 0
		pinnew = StrUtils.left(pinnew + StrUtils.repeat("0", 48), 48);
		log.debug("pinnew2>>{}",pinnew);
		
		String result1 = "";
		String isSuccess = "";
		try {
			result1 = VaCheck.getPINNEWN911(pinnew,"PPKEY",sso);
			params1.put(pinkey, result1.toString()); 
			
			log.warn(pinkey+" RC="+params1.get(pinkey));
			
			
			isSuccess ="success";
			
		} catch (Exception e) {
			isSuccess ="fail"; 
			log.warn("getPINNEW ERROR:"+e.getMessage());
			VaCheck.ssoLogout(sso, sTransSeq_N911);
			throw TopMessageException.create("Z300");
		}
		// ---------------------------getPINNEW-------------------------------

		// ---------------------------ssoLogout-------------------------------
		VaCheck.ssoLogout(sso, sTransSeq_N911);
		// ---------------------------ssoLogout-------------------------------
		
		return isSuccess;
	}
	

}
