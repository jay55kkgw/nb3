package fstop.telcomm.cmd;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fstop.exception.TelCommException;
import fstop.exception.TopMessageException;
import fstop.util.StrUtils;
import fstop.va.VaCheck;
import lombok.extern.slf4j.Slf4j;

/**
 * 依某個欄位重覆的次數, 當作是總筆數
 * @author Owner
 *
 */
@Slf4j
public class XMLCN extends BeforeTelcommQueryCommand {
//	private Logger log = Logger.getLogger(getClass());

	@Override
	public Object doCommand() {

		log.warn("@@@ XMLCN.doCommand()");	
		
		String define = this.getDefine();
		
		log.warn("@@@ XMLCN.doCommand() define=="+define);		

		Map<String, String> params = (Map)getParams();

		log.warn("@@@ XMLCN.doCommand() params = "+params);
		
		//FGTXWAY 為交易機制
		if("0".equals(getParams().get("FGTXWAY"))) {   // 交易密碼
			params.put("XMLCN", "");
			params.put("XMLCA", "");
			return "success";
		}
		else if("1".equals(getParams().get("FGTXWAY"))) {  //憑證
			if(StrUtils.isNotEmpty((String)getParams().get("XMLCN")) && StrUtils.isNotEmpty((String)getParams().get("XMLCA")))
				return "sucess";
		}
		else if("2".equals(getParams().get("FGTXWAY"))){  //晶片金融卡
			
			log.warn("@@@ XMLCN.doCommand() FGTXWAY=2");
			
			if ("F003".equals(getParams().get("TXID"))) {
				params.put("XMLCN", "CARD");
				params.put("XMLCA", "C");
				
			} else {
				params.put("XMLCN", "");
				params.put("XMLCA", "");
				
			}
			return "success";
		}
		else if("3".equals(getParams().get("FGTXWAY"))){//OTP驗證
			log.warn("@@@ XMLCN.doCommand() FGTXWAY=3");
			//外匯轉帳F003的OTP交易，XMLCN在BO已放OTP，XMLCA已放O
			String XMLCN = params.get("XMLCN");
			String XMLCA = params.get("XMLCA");
			if(XMLCN == null || XMLCN!="OTP")
			{
				params.put("XMLCN", "");
				params.put("XMLCA", "");
							
			}
			return "success";
		}
		else if("4".equals(getParams().get("FGTXWAY"))){//自然人憑證驗證
			log.warn("@@@ XMLCN.doCommand() FGTXWAY=4");
			params.put("XMLCN", "");
			params.put("XMLCA", "");
							
			return "success";
		}
		else if("5".equals(getParams().get("FGTXWAY"))){//快速交易
			log.warn("@@@ XMLCN.doCommand() FGTXWAY=5");
			params.put("XMLCN", "GET");
			params.put("XMLCA", "G");
			return "success";
		}		
		else if("6".equals(getParams().get("FGTXWAY"))){//小額交易
			log.warn("@@@ XMLCN.doCommand() FGTXWAY=6");
			params.put("XMLCN", "");
			params.put("XMLCA", "");
							
			return "success";
		}		
		else if("7".equals(getParams().get("FGTXWAY"))){//快速交易
			log.warn("@@@ XMLCN.doCommand() FGTXWAY=7");
			params.put("XMLCN", "");
			params.put("XMLCA", "");
			if ("F003".equals(getParams().get("TXID"))) {
				params.put("XMLCN", "GET");
				params.put("XMLCA", "G");
			}
			return "success";
		}		
		else
		{
			log.warn("@@@ XMLCN.doCommand() FGTXWAY else");			
			
			throw new TelCommException("( " + getParams().get("FGTXWAY") + " )  無法決定交易型態.");
		}

		log.warn("@@@ XMLCN.doCommand() FGTXWAY b4");
		
		//壓簽章字串比對
		String jsondc = StrUtils.trim(params.get("jsondc"));// 原JSON:DC定義為變數會有問題

		log.warn("@@@ XMLCN.doCommand() jsondc=="+jsondc);
		
		String __mac = StrUtils.trim(params.get("pkcs7Sign"));
		params.put("__SIGN", __mac);

		log.warn("@@@ XMLCN.doCommand() __mac=="+__mac);
		
//		SecSpi spi = new SecSpi();// iSecurity已改用VA
		//String result = spi.doVerifySign(__mac,jsondc);   //關閉i-security API
		String result = VaCheck.doVAVerify1(__mac, jsondc, "VARule1");  //使用VA API _mac:元件簽章後字串 jsondc:簽章前字串以utf-8 編碼過	
		log.warn("@@@ XMLCN.doCommand() result=="+result);
		
		log.warn("\r\n XMLCN RESULT : " + result);
		if (result == null || result.length()==0){
			/* TODO jimmy 2019
			CheckServerHealth checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
			checkserverhealth.sendZ300Notice("Z089");
			*/
			throw TopMessageException.create("Z089");
		}

		Pattern p = Pattern.compile("CN=([^\\|]+)", Pattern.CASE_INSENSITIVE);
		Pattern p2 = Pattern.compile("C=.+?,O=.+?,OU=(.+?),CN=[^\\|]+", Pattern.CASE_INSENSITIVE);

		log.warn("@@@ XMLCN.doCommand() b4 ERROR");
		
		if(!StrUtils.trim(result).startsWith("ERROR")) {

			log.warn("@@@ XMLCN.doCommand() IN ERROR XMLCN");
			
			//XMLCN
			try {
				Matcher m = p.matcher(result);
				String xmlcn = "";
				if(m.find()) {
					xmlcn = StrUtils.trim(m.group(1));
				}
				else {
					throw new RuntimeException("XMLCN: " + result + ", Not Match !");
				}

				log.warn("Put XMLCN: " + xmlcn);
				params.put("XMLCN", xmlcn);
			}
			catch(RuntimeException e) {
				throw new TelCommException("取得 XMLCN 錯誤 ! ", e);
			}

			log.warn("@@@ XMLCN.doCommand() IN ERROR XMLCA");
			
			//XMLCA
			try {
				Matcher m2 = p2.matcher(result);
				String xmlca = "";
				if(m2.find()) {
					try {
						xmlca = StrUtils.trim(m2.group(1)).substring(0, 1);
					}
					catch(Exception e) {
						log.error("", e);
						throw new RuntimeException("XMLCA: " + result + ", Can't Get XMLCA (" + e.getMessage() + ")!");
					}
				}
				else {
					throw new RuntimeException("XMLCA: " + result + ", Not Match !");
				}

				log.warn("Put XMLCA: " + xmlca);
				params.put("XMLCA", xmlca);
			}
			catch(RuntimeException e) {
				throw new TelCommException("取得 XMLCA 錯誤 ! ", e);
			}
		}
		else {
			
			log.warn("@@@ XMLCN.doCommand() IN ERROR else");
			/* TODO jimmy 2019
			CheckServerHealth checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
			checkserverhealth.sendZ300Notice("取得 XMLCN 或 XMLCA 錯誤 !");
			*/
			throw new TelCommException("取得 XMLCN 或 XMLCA 錯誤 ! ");
		}

		log.warn("@@@ XMLCN.doCommand() b4 return");
		
		return "success";
	}

//	public static void main(String[] args) {
//		Pattern p = Pattern.compile("CN=([^\\|]+)", Pattern.CASE_INSENSITIVE);
//		String cn = " C=TW,O=Finance,OU=TaiCA Test FXML CA,OU=0500000-TBB,OU=FXML,CN=A123456814-01-TTBB0030|";
//		Matcher m = p.matcher(cn);
//		if(m.find())
//			log.debug("XMLCN: " + StrUtils.trim(m.group(1)));
//
//		Pattern p2 = Pattern.compile("C=.+?,O=.+?,OU=(.+?),CN=[^\\|]+", Pattern.CASE_INSENSITIVE);
//		Matcher m2 = p2.matcher(cn);
//		if(m2.find())
//			log.debug("XMLCA: " + StrUtils.trim(m2.group(1)).substring(0, 1));
//
//
//	}

}
