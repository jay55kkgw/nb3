package fstop.telcomm.cmd;

import java.util.Map;

import fstop.exception.TopMessageException;
import fstop.util.StrUtils;
import fstop.va.VaCheck;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SoftCertContext extends BeforeTelcommQueryCommand {

	@Override
	public Object doCommand() {


		log.warn("@@@ SoftCertContext.doCommand()");	
		
		String define = this.getDefine();
		
		log.warn("@@@ SoftCertContext.doCommand() define=="+define);		

		Map<String, String> params = (Map)getParams();
		
		log.warn("@@@ SoftCertContext.doCommand() params = "+params);
		
		//批次而來跳過驗證
		if("SEND".equals(params.get("__TRANS_STATUS"))||"RESEND".equals(params.get("__TRANS_STATUS"))) {
			return "success";
		}
		
		
		if("4".equals(getParams().get("FGTXWAY"))){//自然人憑證驗證
			if (params.get("signature").trim().length()==0)
				throw TopMessageException.create("X101");
		}
		final boolean isSoftCert = StrUtils.isNotEmpty(params.get("signature")); // 是否為軟體憑證
		if (isSoftCert) {
			boolean b = VaCheck.doVAVerifyCN(params.get("signature"),params);

			if (b == false) {
				log.info("@@Soft signature validation Error.");

				throw TopMessageException.create("Z089");
			}
		}
		return "success";
		
	}
	
	public String getCN(Map<String, String> params) {
		String CN = "";
		if("4".equals(params.get("FGTXWAY"))){//自然人憑證驗證
			if (params.get("signature").trim().length()==0)
				throw TopMessageException.create("X101");
		}
		final boolean isSoftCert = StrUtils.isNotEmpty(params.get("signature")); // 是否為軟體憑證
		if (isSoftCert) {
			boolean b = VaCheck.doVAVerifyCN(params.get("signature"),params);

			if (b == false) {
				log.info("@@Soft signature validation Error.");

				throw TopMessageException.create("Z089");
			}else {
				CN = params.get("CN");
			}
		}
		
		return CN;
		
	}
}
