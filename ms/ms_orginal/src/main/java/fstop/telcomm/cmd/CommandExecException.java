package fstop.telcomm.cmd;

import fstop.exception.UncheckedException;

public class CommandExecException extends UncheckedException { 

	private static final long serialVersionUID = 1L;
	
	public CommandExecException(String msg) {
		super(msg);
	}
	public CommandExecException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
