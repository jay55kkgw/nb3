package fstop.telcomm.cmd;

import java.util.Map;

import org.apache.log4j.Logger;

import fstop.exception.TopMessageException;
import fstop.util.StrUtils;
import fstop.va.VaCheck;


/**
 * 依某個欄位重覆的次數, 當作是總筆數
 * @author Owner
 *
 */
public class PPSYNCN2 extends BeforeTelcommQueryCommand {
	private Logger logger = Logger.getLogger(getClass());
	
	@Override
	public Object doCommand() {

		//FGTXWAY 為交易機制
		if(StrUtils.trim((String)getParams().get("FGTXWAY")).length() != 0) {
			if(!"0".equals(getParams().get("FGTXWAY"))) {   // 不為交易密碼 ( 0 表示為交易密碼)
				logger.warn(getParams().get("TXID") + " SKIP PPSYNC (FGTXWAY: " + getParams().get("FGTXWAY") + ")");
				return "skip";
			}
		}
		
		String define = this.getDefine();
		
		Map<String, String> params = this.getParams();
		//SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		//String t3 = sdf.format(new Date());
		//int year = new Integer(t3.substring(0, 4)) - 1911;
		//String today = StrUtils.left(StrUtils.right(year + t3.substring(t3.length() - 4, t3.length()),6) + "00000000",8);

		//logger.debug("today = " + today);
		//String tommorrow = DateTimeUtils.format("MMdd", new Date());
		//int t = new Integer(tommorrow);
		//t = t + 1;
		//tommorrow = StrUtils.right("00000000" + t, 8);
		//logger.debug("tommorrow = " + tommorrow);
		//KeyClient client = new KeyClient();
		/*String result = KeyClientUtils.doRequest(
				IKeySpi.CREATE_SYNC, 
				KeyLabel.KEYLABEL_PPKEY, //"B05000NNBTKPP", 
				client.toEBCDIC(today.getBytes()), 
				client.toEBCDIC(tommorrow.getBytes()), 
				"0016");
		*/
		String result="";
		String status="";
		try {
			result = VaCheck.getPPSYNC("PPKEY2");
			if(result!=null&&result.length()>0) {
				try {
					logger.warn("PPSYNCN = " + result.substring(0, 8));
		    		params.put("PPSYNCN", result.substring(0, 8));
		    		status= "success";
		    	}
		    	catch(RuntimeException e) {
		    		status= "fail";
		    		throw TopMessageException.create("Z300"); //押解碼錯誤
		    	}
			}
			else {
				status= "fail";
				throw TopMessageException.create("Z300"); //押解碼錯誤

			}			 
		}
		catch (Exception e)
		{
			status= "fail";
			logger.warn("getPPSYNC ERROR:"+e.getMessage());
			throw TopMessageException.create("Z300");
		}

		
/*
        client.FUN_ID = IKeySpi.CREATE_SYNC;
        client.WORK_KEY = "B05000NNBTKPP1";
        client.IV = "0000000000000000";       //初始向量
        client.TEXT = client.toEBCDIC(tommorrow.getBytes());
        client.TEXT_SIZE = "0016";
*/        

		
		return status ;
	}
	

}
