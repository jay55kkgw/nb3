package fstop.telcomm.cmd;

import java.util.Map;

public abstract class BeforeTelcommQueryCommand  implements TelcommCommand {
	private String define;

	private Map params;

	public BeforeTelcommQueryCommand() {

	}

	public abstract Object doCommand();

	public String getDefine() {
		return define;
	}

	public void setDefine(String define) {
		this.define = define;
	}

	public Map getParams() {
		return params;
	}

	public void setParams(Map params) {
		this.params = params;
	}
}
