package fstop.telcomm.cmd;

import java.util.Map;

import org.apache.log4j.Logger;

import com.netbank.rest.e2e.E2EClient;
import com.netbank.rest.e2e.MB3_E2EClient;

import fstop.exception.TopMessageException;
import fstop.util.StrUtils;
import fstop.va.VaCheck;
import lombok.extern.slf4j.Slf4j;


/**
 * 壓PINBLOCK
 * @author SOX
 *
 */
@Slf4j
public class PINBLOCK extends BeforeTelcommQueryCommand {
//	private Logger logger = Logger.getLogger(getClass());

	@Override
	public Object doCommand() {

		String define = "";
		String pinkey = "CMPASSWORD";
        String login_type = "";

		//指定put回去Hashtable時的參數名字。
		if(this.getDefine().indexOf(",") != -1){
			define = this.getDefine().substring(0, this.getDefine().indexOf(","));
			pinkey = this.getDefine().substring(this.getDefine().indexOf(",")+1);
		}
		Map<String, String> params = this.getParams();
        login_type = params.get("LOGINTYPE");
        log.warn("PINNEW1 doCommand login_type = " + login_type);


		String pw = StrUtils.trim(params.get(pinkey));
		if(pw.length() == 0)
			log.debug("PINBLOCK pw length=0");
		
		String result = "";
		String isSuccess = "";
		if(pw.length()>0) {
			try {
				if("Y".equalsIgnoreCase(E2EClient.e2e().e2e.getIsE2E())) {
                    if("MB".equals(login_type)) {
                        result = MB3_E2EClient.getPINNEW(pw);
                    }else {
                    	result = E2EClient.getPINNEW(pw);
                    }
				}else {
					result = VaCheck.getPINBLOCK(pw);
				}	
					params.put(pinkey, result.toString()); 
					log.warn(pinkey+" RC="+params.get(pinkey));
					isSuccess ="success";
			}
			catch (Exception e)
			{
				isSuccess ="fail";
				log.warn("getPINBLOCK ERROR:"+e.getMessage());
				throw TopMessageException.create("Z300");
			}
		}

		return isSuccess;
	}


}
