package fstop.telcomm;

import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public interface QueryTemplate {
    void query(TelCommParams telcomm, Map<String, String> _params, Consumer<List> resultConsumer);
}
