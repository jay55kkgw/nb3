package fstop.telcomm;

public class TelcommObject implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private String mode;

	private String type;

	private String TXID = "";

	private String HOSTMSGID;
 

	public String getHOSTMSGID() {
		return HOSTMSGID;
	}

	public void setHOSTMSGID(String hostmsgid) {
		HOSTMSGID = hostmsgid;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getTXID() {
		return TXID;
	}

	public void setTXID(String txid) {
		TXID = txid;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
