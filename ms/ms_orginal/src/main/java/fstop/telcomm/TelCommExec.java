package fstop.telcomm;

import fstop.model.TelcommResult;

import java.util.Map;

public interface TelCommExec {
	TelcommResult query(Map params);
}
