package fstop.telcomm;

import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public abstract class TelCommParams extends TelcommObject {
	private String desc;
	
	// default 的設定
	private Map<String, String> defaultFieldFormatDefine = new HashMap();

	//by 不同電文的設定
	private Map<String, String> fieldFormat = new HashMap();
	
	private List<String> beforeCommand = new ArrayList<String>();
	private List<String> afterCommand = new ArrayList<String>();


}
