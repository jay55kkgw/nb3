package fstop.va;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.codec.binary.Hex;
import org.joda.time.DateTime;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;
import com.netbank.util.fstop.DateUtil;
import com.twca.framework.mb.TransSecureSrv;

import fstop.exception.TopMessageException;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
//
//import fstop.services.batch.CheckServerHealth;
//import fstop.util.DateTimeUtils;
@Slf4j
public class VaCheck {
	
	public static String TIME_PATTERN = "yyyyMMddHHmmss";
	
//	public static TransSecureSrv sso = null;
//	@Autowired
//	public static VAProperties va;
	
	
	public static VATransform getVA() {
		return (VATransform)SpringBeanFactory.getBean("vatransform");
	}
	
	/**
	 *  VA Check
	 */
	private static int Login(String sTransSeq) {
		String URI = getVA().va.getURI();
		String ID =  getVA().va.getID();
		String PWD = getVA().va.getPWD();
		String CONNECTTIMEOUT = getVA().va.getCONNECTTIMEOUT();
		String sid = null;
		int loginRtn = -1;
		DateTime d1 = null;
		DateTime d2 = null;
		
		log.warn("VA API URL:{} ID:{} PWD:{} CONNECTTIMEOUT:{}", URI, ID, PWD, CONNECTTIMEOUT);
		
		TransSecureSrv sso = null;
		
		try {
			sso = new TransSecureSrv(URI, "NNB");
//			sso.setConnectTimeout(15000); //定義15秒timeout
			int connectTimeOut = Integer.parseInt(CONNECTTIMEOUT);
			sso.setConnectTimeout(connectTimeOut*1000); // 依環境變數定義timeout
			
			d1 = new DateTime();
            loginRtn = sso.Login(ID,PWD, sTransSeq);
            
            if (loginRtn == 0) {
                sid = sso.GetSessionID();
                log.warn("VA Login() -> sid: " + sid + " | " + "sTransSeq: " + sTransSeq);
                
            } else {
            	log.warn("[VA] sso Login ErrorMsg: " + sso.GetErrorMsg());
            	throw TopMessageException.create("Z300");
            }            
            d2 = new DateTime();
            
		} catch (Exception e) {
			d2 = new DateTime();
			log.warn("[VA] occur Exception:{} " ,e.toString());
			throw TopMessageException.create("Z300");
			
		} finally {
			d2 = d2 ==null ?  new DateTime() :d2;
			log.info("VA.Login.pstimediff: {}", DateUtil.getDiffTimeMills(d1, d2));
		}
		return loginRtn;		
	}
	
	/**
	 * checkAuthenReq
	 */
	public static String checkAuthenReq(String sAuthenCode) {
		Date d = new Date();
//		String sTransSeq = DateTimeUtils.format(TIME_PATTERN, d);
		String sTransSeq = UUID.randomUUID().toString();
		String rtnstr = "";
		int authchkrtn = -1;
//		int loginRtn = Login(sTransSeq);
		
		DateTime d1 = new DateTime();
		
		String URI = getVA().va.getURI();
		String ID =  getVA().va.getID();
		String PWD = getVA().va.getPWD();
		String CONNECTTIMEOUT = getVA().va.getCONNECTTIMEOUT();
		String sid = null;
		int loginRtn = -1;
		
		log.warn("VA API URL:"+URI+" ID:"+ID+" PWD:"+PWD);
		TransSecureSrv sso = null;
		try {
			sso = new TransSecureSrv(URI, "NNB");
//			sso.setConnectTimeout(15000); //定義15秒timeout
			int connectTimeOut = Integer.parseInt(CONNECTTIMEOUT);
			sso.setConnectTimeout(connectTimeOut*1000); // 依環境變數定義timeout
			
            loginRtn = sso.Login(ID,PWD, sTransSeq);
            
            if (loginRtn == 0) {
                sid = sso.GetSessionID();
                log.warn("VA Login() -> sid: " + sid + " | " + "sTransSeq: " + sTransSeq);
            } else {
            	log.warn("[VA] sso Login ErrorMsg: " + sso.GetErrorMsg());
            	throw TopMessageException.create("Z300");
            }
            
		} catch (Exception e) {
			log.warn("[VA] occur Exception: " + e.toString());
			throw TopMessageException.create("Z300");
		}
		
		try {
			log.trace("loginRtn>>{}",loginRtn);
			log.trace("sso.GetErrorMsg()>>{}",sso.GetErrorMsg());
			
            if (loginRtn == 0) {
                sid = sso.GetSessionID();
                log.warn("checkAuthenReq -> sid: " + sid + " | " + "sTransSeq: " + sTransSeq);
                authchkrtn = sso.AuthenCheckReq(sAuthenCode);
                
                if (authchkrtn == 0) {
                	rtnstr = sso.GetAuthenCheckResp();
                	
                } else {
                	log.warn("[checkAuthenReq] sso authchkrtn ErrorMsg: " + sso.GetErrorMsg());
                	throw TopMessageException.create("Z300");
                }
                
            } else {
            	log.warn("[checkAuthenReq] sso Login ErrorMsg: " + sso.GetErrorMsg());
            	throw TopMessageException.create("Z300");
            }
            
		} catch (Exception e) {
			log.warn("[checkAuthenReq] occur Exception: " + e.toString());
			throw TopMessageException.create("Z300");
			
		} finally {
			try {
	            sso.Logout(sid, sTransSeq);
	            log.warn("logout -> sid: " + sid + " | " + "sTransSeq: " + sTransSeq);
	            DateTime d2 = new DateTime();
	            log.info("VA.checkAuthenReq.pstimediff: {}", DateUtil.getDiffTimeMills(d1, d2));
	        } catch (Exception fe){
	        	log.warn("[logout] finally occur Exception: " + fe.toString());
	        	throw TopMessageException.create("Z300");
	        }
		}
		
		
		return rtnstr;
	}
	
	/**
	 * VA logout
	 */
	private static void logout(String sid,String sTransSeq) {
		TransSecureSrv sso = null;
		DateTime d1 = null;
		DateTime d2 = null;
        try {
			d1 = new DateTime();
			sso.Logout(sid, sTransSeq);
           
            log.warn("logout -> sid: " + sid + " | " + "sTransSeq: " + sTransSeq);
            
        } catch (Exception fe){
        	log.warn("[logout] finally occur Exception: " + fe.toString());
        	throw TopMessageException.create("Z300");
        	
        } finally {
        	d2 = new DateTime();
        	log.info("VA.logout.pstimediff: {}", DateUtil.getDiffTimeMills(d1, d2));
		}
        sso = null;				
	}
	
	/**
	 * 自然人憑證驗章
	 */
	public static String doVAVerify(String sSignature, String jsondc, String RARules) {
		Date d = new Date();
		//String sTransSeq = DateTimeUtils.format(TIME_PATTERN, d);		
		String sTransSeq = UUID.randomUUID().toString();
		
//		int loginRtn = Login(sTransSeq);
		String URI = getVA().va.getURI();
		String ID =  getVA().va.getID();
		String PWD = getVA().va.getPWD();
		String CONNECTTIMEOUT = getVA().va.getCONNECTTIMEOUT();
		String sid = null;
		int loginRtn = -1;
		
		int checksignflag = -1;
		String certinfo = "";
		//byte[] sData1=sData.getBytes("UTF16-LE");
		
		log.warn("VA API URL:"+URI+" ID:"+ID+" PWD:"+PWD);
		TransSecureSrv sso = null;
		
		DateTime d1 = new DateTime();
		
		try {
			sso = new TransSecureSrv(URI, "NNB");
//			sso.setConnectTimeout(15000); //定義15秒timeout
			int connectTimeOut = Integer.parseInt(CONNECTTIMEOUT);
			sso.setConnectTimeout(connectTimeOut*1000); // 依環境變數定義timeout
			
			loginRtn = sso.Login(ID,PWD, sTransSeq);
			
            if (loginRtn == 0) {
    			sid = sso.GetSessionID();
                log.warn("VA Login() -> sid: " + sid + " | " + "sTransSeq: " + sTransSeq);
                
            } else {
            	log.warn("[VA] sso Login ErrorMsg: " + sso.GetErrorMsg());
            	throw TopMessageException.create("Z300");
            }
            
		} catch (Exception e) {
			log.warn("[VA] occur Exception: " + e.toString());
			throw TopMessageException.create("Z300");
		}
		
		try {
            if (loginRtn == 0) {
                sid = sso.GetSessionID();
                log.warn("VAVerify -> sid: " + sid + " | " + "sTransSeq: " + sTransSeq);
                checksignflag = sso.VAVerify(sTransSeq, sSignature, jsondc, "", sso.SS_FLAG_VERIFY_FULL, 0, d, true, RARules);
                if (checksignflag == 0) {
                	log.warn("[VAVerify] sso VAVerify Successful");
                	certinfo = sso.GetCert();
                	log.warn("[VAVerify] sso GetContent:" + sso.GetContent());
                	log.warn("[VAVerify] sso GetSubject:" + sso.GetSubject());
                	log.warn("[VAVerify] sso GetSerial:" + sso.GetSerial());
                	
                } else {
                	log.warn("[VAVerify] sso VAVerify ErrorMsg: " + sso.GetErrorMsg());
                	throw TopMessageException.create("Z300");
                }
            } else {
            	log.warn("[VAVerify] sso VAVerify ErrorMsg: " + sso.GetErrorMsg());
            	throw TopMessageException.create("Z300");
            }
            
		} catch (Exception e) {
			log.warn("[VAVerify] occur Exception: " + e.toString());
			throw TopMessageException.create("Z300");
			
		} finally {
			try {
	            sso.Logout(sid, sTransSeq);
	            log.warn("logout -> sid: " + sid + " | " + "sTransSeq: " + sTransSeq);
	            DateTime d2 = new DateTime();
	            log.info("VA.doVAVerify.pstimediff: {}", DateUtil.getDiffTimeMills(d1, d2));
	        } catch (Exception fe){
	        	log.warn("[logout] finally occur Exception: " + fe.toString());
	        	throw TopMessageException.create("Z300");
	        }
		}
		
		return certinfo;
	}

	/**
	 * doVerfyICS
	 */
	public static int doVerfyICS(String sB64Cert, String sIDN, Boolean bRtn) {
		int flag = -1;
		//Date d = new Date();
		//String sTransSeq = DateTimeUtils.format(TIME_PATTERN, d);
		String sTransSeq = UUID.randomUUID().toString();
//		int loginRtn = Login(sTransSeq);
		String URI = getVA().va.getURI();
		String ID =  getVA().va.getID();
		String PWD = getVA().va.getPWD();
		String CONNECTTIMEOUT = getVA().va.getCONNECTTIMEOUT();
		String sid = null;
		int loginRtn = -1;
		
		log.warn("VA API URL:"+URI+" ID:"+ID+" PWD:"+PWD);
		TransSecureSrv sso = null;
		
		DateTime d1 = new DateTime();
		
		try {
			sso = new TransSecureSrv(URI, "NNB");
//			sso.setConnectTimeout(15000); //定義15秒timeout
			int connectTimeOut = Integer.parseInt(CONNECTTIMEOUT);
			sso.setConnectTimeout(connectTimeOut*1000); // 依環境變數定義timeout
            loginRtn = sso.Login(ID,PWD, sTransSeq);
            
            if (loginRtn == 0) {
                sid = sso.GetSessionID();
                log.warn("VA Login() -> sid: " + sid + " | " + "sTransSeq: " + sTransSeq);
                
            } else {
            	log.warn("[VA] sso Login ErrorMsg: " + sso.GetErrorMsg());
            	throw TopMessageException.create("Z300");
            }
            
		} catch (Exception e) {
			log.warn("[VA] occur Exception: " + e.toString());
			throw TopMessageException.create("Z300");
		}
		
		try {    					
			if (loginRtn == 0) {
	            sid = sso.GetSessionID();
	            log.warn("VerifyICS -> sid: " + sid + " | " + "sTransSeq: " + sTransSeq);
	            flag = sso.VerifyICS(sTransSeq, sB64Cert, sIDN, true);

	            if (flag == 0) {
	               log.warn("[VerifyICS] sso VerifyICS Successful");
	               	
	            } else {
	               log.warn("[VerifyICS] sso VerifyICS ErrorMsg: " + sso.GetErrorMsg());
	               throw TopMessageException.create("Z300");
	            }
	             
	         } else {
            	log.warn("[VerifyICS] sso VerifyICS ErrorMsg: " + sso.GetErrorMsg());
            	throw TopMessageException.create("Z300");
	         }    

		} catch(Exception e) {
			log.warn("pkcs7.verify has error ===========>"+e);
			throw TopMessageException.create("Z300");
			
		} finally {
			try {
	            sso.Logout(sid, sTransSeq);
	            log.warn("logout -> sid: " + sid + " | " + "sTransSeq: " + sTransSeq);
	            DateTime d2 = new DateTime();
	            log.info("VA.doVerfyICS.pstimediff: {}", DateUtil.getDiffTimeMills(d1, d2));
	            
	        } catch (Exception fe){
	        	log.warn("[logout] finally occur Exception: " + fe.toString());
	        	throw TopMessageException.create("Z300");
	        }
		}
		
		
		return flag;
	}
	
	/**
	 * doVerfyICS
	 */
	public static byte[] getICV() throws UnsupportedEncodingException  {
		log.warn("getICV ....");
        String checkItem = "";
        GregorianCalendar cNow = new GregorianCalendar();
        int iYear = cNow.get(Calendar.YEAR);
       
        iYear = iYear - 1911;
        String sYear = String.valueOf(iYear);
        if(sYear.length() > 2)
            sYear = sYear.substring(1, 3);
       
        checkItem += sYear;
        int iMonth = cNow.get(Calendar.MONTH) + 1;
        int iDay = cNow.get(Calendar.DAY_OF_MONTH);
                
        if(iMonth < 10)
            checkItem += "0" + iMonth;
        else
            checkItem += "" + iMonth;
        if(iDay < 10)
            checkItem += "0" + iDay;
        else
            checkItem += "" + iDay;
       
        checkItem = checkItem + "00";
        log.warn("icv checkItem :" + checkItem);
        
        return checkItem.getBytes("IBM937");
	}
	
	/**
	 * 目前SYNC PPSYNC key 邏輯相同
	 * @return
	 */
	public static String getSYNC_IKEY() {
		String ekey = "";
		try {
			String tmp =   new DateTime().toString("MMdd");
			log.trace("tmp>>{}",tmp);
			ekey =String.format("%08d", Integer.parseInt(tmp)+1) ;
			log.trace("ekey>>{}",ekey);
			
		} catch (NumberFormatException e) {
			log.error(e.toString());
			throw TopMessageException.create("Z300");
		}
		return ekey;
	}
	
	/**
	 * getSYNC
	 */
	public static String getSYNC() throws Exception {
		String ekey = "";
		String ppsync= "";
//		20191031 by hugo from 景森 :邏輯改為日期轉數字+1 ex:1031+1 = 1032;
//		ekey = "0000"+DateTimeUtils.getNextDate("MMdd");
		ekey = getSYNC_IKEY();
		byte[] encrypt = null;
		byte[] newEncrypt =null;
		String KeyLabel = getVA().va.getKEYLABEL_MAC();
		byte[] data = null;
		byte[] icv = null;
		int iMode = TransSecureSrv.CIPHER_MODE_CBC;// 值是0
		int iPadding = TransSecureSrv.CIPHER_PADDING_ZERO;// 值是1
		int flag = -1;
		//Date d = new Date();
		//String sTransSeq = DateTimeUtils.format(TIME_PATTERN, d);		
		String sTransSeq = UUID.randomUUID().toString();
//		int loginRtn = Login(sTransSeq);
		
		String URI = getVA().va.getURI();
		String ID =  getVA().va.getID();
		String PWD = getVA().va.getPWD();
		String CONNECTTIMEOUT = getVA().va.getCONNECTTIMEOUT();
		String sid = null;
		int loginRtn = -1;
		
		log.warn("VA API URL:"+URI+" ID:"+ID+" PWD:"+PWD);
		TransSecureSrv sso = null;
		
		DateTime d1 = new DateTime();
		
		try {
			sso = new TransSecureSrv(URI, "NNB");
//			sso.setConnectTimeout(15000); //定義15秒timeout
			int connectTimeOut = Integer.parseInt(CONNECTTIMEOUT);
			sso.setConnectTimeout(connectTimeOut*1000); // 依環境變數定義timeout
            loginRtn = sso.Login(ID,PWD, sTransSeq);
            if (loginRtn == 0) {
                sid = sso.GetSessionID();
                log.warn("VA Login() -> sid: " + sid + " | " + "sTransSeq: " + sTransSeq);
            } else {
            	log.warn("[VA] sso Login ErrorMsg: " + sso.GetErrorMsg());
            	throw TopMessageException.create("Z300");
            }  
            
		} catch (Exception e) {
			log.warn("[VA] occur Exception: " + e.toString());
			throw TopMessageException.create("Z300");
		}
		
		try {    					
			if (loginRtn == 0) {
	            sid = sso.GetSessionID();
	            log.warn("getSYNC -> sid: " + sid + " | " + "sTransSeq: " + sTransSeq);
	 			data = ekey.getBytes("IBM937");
	 			log.warn("ekey:" + ekey);
	 			log.warn("KeyLabel :" + KeyLabel);
				icv = getICV();	             
	            flag = sso.SymEncrypt(sid, sTransSeq, KeyLabel, data, iMode, iPadding, icv);
				if (flag == 0) {
					encrypt = sso.GetCipherMAC();
					log.warn("[getSYNC] sso getSYNC Successful");
					log.warn("[getSYNC] encrypt.length>>" + encrypt.length);
					log.warn("[getSYNC] SYNC data(HEX) :" + new String(StrUtils.toHex(encrypt)));
					newEncrypt = new byte[4];
					// 取前4位
					if (encrypt != null && encrypt.length >= 4) {
						for (int i = 0; i < newEncrypt.length; i++) {
							newEncrypt[i] = encrypt[i];
						}
					}
					ppsync = StrUtils.toHex(newEncrypt);
					log.warn(" SYNC data(HEX) :" + ppsync);
					log.warn("encrypt ok !!");
					
				} else {
					log.warn("[getSYNC] sso getSYNC ErrorMsg: " + sso.GetErrorMsg());
					/*
					 * TODO jimmy 2019 CheckServerHealth checkserverhealth =
					 * (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
					 * checkserverhealth.sendZ300Notice("Z300");
					 */
					throw TopMessageException.create("Z300"); // 押解碼錯誤
				}
	         } else {
	            	log.warn("[getSYNC] sso Login fail ErrorMsg: " + sso.GetErrorMsg());
	            	/* TODO jimmy 2019
	            	CheckServerHealth checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
	            	checkserverhealth.sendZ300Notice("Z300");
	            	*/
	            	throw TopMessageException.create("Z300");  //押解碼錯誤
	         }    

		} catch (Exception e) {
			log.warn("getSYNC has error ===========>"+e.toString());
			/* TODO jimmy 2019
        	CheckServerHealth checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
        	checkserverhealth.sendZ300Notice("Z300");
        	*/
        	throw TopMessageException.create("Z300");  //押解碼錯誤
        	
		} finally {
			try {
	            sso.Logout(sid, sTransSeq);
	            log.warn("logout -> sid: " + sid + " | " + "sTransSeq: " + sTransSeq);
	            DateTime d2 = new DateTime();
	            log.info("VA.getSYNC.pstimediff: {}", DateUtil.getDiffTimeMills(d1, d2));
	            
	        } catch (Exception fe){
	        	log.warn("[logout] finally occur Exception: " + fe.toString());
	        	throw TopMessageException.create("Z300");
	        }
		}		
		// Ending.
		log.warn("getSYNC Finished.");
		return ppsync;
	}
	
	/**
	 * getPPSYNC
	 */
	public static String getPPSYNC(String KEYNAME) throws Exception {
		String ekey = "";
		String ppsync= "";
//		20191031 by hugo from 景森 :邏輯改為日期轉數字+1 ex:1031+1 = 1032;
//		ekey = "0000"+DateTimeUtils.getNextDate("MMdd");
		ekey = getSYNC_IKEY();
		byte[] encrypt = null;
		byte[] newEncrypt =null;
		String KeyLabel = "";
		if(KEYNAME.equals("PPKEY"))  //一般交易使用之KEY
			KeyLabel = getVA().va.getKEYLABEL_PPKEY();
		if(KEYNAME.equals("PPKEY2"))   //隨護神盾申請使用之KEY
			KeyLabel = getVA().va.getKEYLABEL_PPKEY2();
		byte[] data = null;
		byte[] icv = null;
		int iMode = TransSecureSrv.CIPHER_MODE_CBC;// 值是0
		int iPadding = TransSecureSrv.CIPHER_PADDING_ZERO;// 值是1
		int flag = -1;
		//Date d = new Date();
		//String sTransSeq = DateTimeUtils.format(TIME_PATTERN, d);		
		String sTransSeq = UUID.randomUUID().toString();
//		int loginRtn = Login(sTransSeq);
		String URI = getVA().va.getURI();
		String ID =  getVA().va.getID();
		String PWD = getVA().va.getPWD();
		String CONNECTTIMEOUT = getVA().va.getCONNECTTIMEOUT();
		String sid = null;
		int loginRtn = -1;
		
		log.warn("VA API URL:"+URI+" ID:"+ID+" PWD:"+PWD);
		TransSecureSrv sso = null;
		
		DateTime d1 = new DateTime();
		
		try {
			sso = new TransSecureSrv(URI, "NNB");
//			sso.setConnectTimeout(15000); //定義15秒timeout
			int connectTimeOut = Integer.parseInt(CONNECTTIMEOUT);
			sso.setConnectTimeout(connectTimeOut*1000); // 依環境變數定義timeout
            loginRtn = sso.Login(ID,PWD, sTransSeq);
            if (loginRtn == 0) {
                sid = sso.GetSessionID();
                log.warn("VA Login() -> sid: " + sid + " | " + "sTransSeq: " + sTransSeq);
                
            } else {
            	log.warn("[VA] sso Login ErrorMsg: " + sso.GetErrorMsg());
            	throw TopMessageException.create("Z300");
            }
            
		} catch (Exception e) {
			log.warn("[VA] occur Exception: " + e.toString());
			throw TopMessageException.create("Z300");
		}	
		
		try {    					
			if (loginRtn == 0) {
	            sid = sso.GetSessionID();
	            log.warn("getPPSYNC -> sid: " + sid + " | " + "sTransSeq: " + sTransSeq);
	 			data = ekey.getBytes("IBM937");
	 			log.warn("ekey:" + ekey);
	 			log.warn("KeyLabel :" + KeyLabel);
				icv = getICV();	             
	            flag = sso.SymEncrypt(sid, sTransSeq, KeyLabel, data, iMode, iPadding, icv);
	             if (flag == 0) {
	            	encrypt = sso.getCipher();
	                log.warn("[getPPSYNC] sso getPPSYNC Successful");
	            	log.warn("[getPPSYNC] encrypt.length>>"+encrypt.length);
	            	log.warn("[getPPSYNC] PPSYNC data(HEX) :"+ new String(StrUtils.toHex(encrypt)));
	            	newEncrypt =new byte[4];
//	    			取前4位
	    			if(encrypt !=null && encrypt.length>=4){
	    				for(int i =0 ;i < newEncrypt.length;i++){
	    					newEncrypt[i]=encrypt[i];
	    				}
	    			}	
	    			ppsync = StrUtils.toHex(newEncrypt);
	    			log.warn(" PPSYNC data(HEX) :"+ ppsync);
	    			log.warn("encrypt ok !!");
	    			
	             } else {
	                log.warn("[getPPSYNC] sso getPPSYNC ErrorMsg: " + sso.GetErrorMsg());
	                /* TODO jimmy 2019
	            	CheckServerHealth checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
	            	checkserverhealth.sendZ300Notice("Z300");
	            	*/
	            	throw TopMessageException.create("Z300");  //押解碼錯誤
	             }
	             
	         } else {
	            	log.warn("[getPPSYNC] sso Login fail ErrorMsg: " + sso.GetErrorMsg());
	            	/* TODO jimmy 2019
	            	CheckServerHealth checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
	            	checkserverhealth.sendZ300Notice("Z300");
	            	*/
	            	throw TopMessageException.create("Z300");  //押解碼錯誤
	         }    

		} catch(Exception e) {
			log.warn("getPPSYNC has error ===========>"+e.toString());
			/* TODO jimmy 2019
        	CheckServerHealth checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
        	checkserverhealth.sendZ300Notice("Z300");
        	*/
        	throw TopMessageException.create("Z300");  //押解碼錯誤
        	
		} finally {
			try {
	            sso.Logout(sid, sTransSeq);
	            log.warn("logout -> sid: " + sid + " | " + "sTransSeq: " + sTransSeq);
	            DateTime d2 = new DateTime();
	            log.info("VA.getPPSYNC.pstimediff: {}", DateUtil.getDiffTimeMills(d1, d2));
	            
	        } catch (Exception fe){
	        	log.warn("[logout] finally occur Exception: " + fe.toString());
	        	throw TopMessageException.create("Z300");
	        }
		}		
		// Ending.
		log.warn("getPPSYNC Finished.");
		return ppsync;
	}
	
	/**
	 * getPINNEW
	 */
	public static String getPINNEW(String datasources,String KEYNAME) throws Exception {
		String pinnew= "";
		byte[] encrypt = null;
		
		
        // 24bytes
		byte[] Encryptsrc = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
        byte[] newEncrypt = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
        
        if(StrUtils.isNotEmpty(datasources)) {
        	log.debug("datasources>>{}",datasources);
        	log.debug("datasources.length>>{}",datasources.length());
        }
        
        byte[] datasource1 = null;
        datasource1 = StrUtils.hexToByte(datasources);
		if(datasource1 !=null){
			for(int i =0 ;i < Encryptsrc.length;i++){
				Encryptsrc[i]=datasource1[i];
			}
		}	   
        //System.arraycopy(datasources, 0, EncryptData, 0, datasources.length);
        log.warn("datasources original:" + datasources+" length:"+datasources.length());
		log.warn("Encryptsrc:" + StrUtils.toHex(Encryptsrc)+"  Encryptsrc.length>>"+Encryptsrc.length);
		String KeyLabel = "";
	
		byte[] icv = null;
		int iMode = TransSecureSrv.CIPHER_MODE_CBC;// 值是0
		int iPadding = TransSecureSrv.CIPHER_PADDING_ZERO;// 值是1
		int flag = -1;
		//Date d = new Date();
		//String sTransSeq = DateTimeUtils.format(TIME_PATTERN, d);		
		String sTransSeq = UUID.randomUUID().toString();
//		int loginRtn = -1;
		String URI = getVA().va.getURI();
		String ID =  getVA().va.getID();
		String PWD = getVA().va.getPWD();
		String CONNECTTIMEOUT = getVA().va.getCONNECTTIMEOUT();
		String sid = null;
		int loginRtn = -1;
		
		log.warn("VA API URL:"+URI+" ID:"+ID+" PWD:"+PWD);
		TransSecureSrv sso = null;
		
		DateTime d1 = new DateTime();
		
		try {
			sso = new TransSecureSrv(URI, "NNB");
//			sso.setConnectTimeout(15000); //定義15秒timeout
			int connectTimeOut = Integer.parseInt(CONNECTTIMEOUT);
			sso.setConnectTimeout(connectTimeOut*1000); // 依環境變數定義timeout
            loginRtn = sso.Login(ID,PWD, sTransSeq);
            
            if (loginRtn == 0) {
                sid = sso.GetSessionID();
                log.warn("VA Login() -> sid: " + sid + " | " + "sTransSeq: " + sTransSeq);
            } else {
            	log.warn("[VA] sso Login ErrorMsg: " + sso.GetErrorMsg());
            	throw TopMessageException.create("Z300");
            }
            
		} catch (Exception e) {
			log.warn("[VA] occur Exception: " + e.toString());
			throw TopMessageException.create("Z300");
		}
		
		//byte[] decryptstr =null;
		try { 
			loginRtn = Login(sTransSeq);
			log.debug("loginRtn>>{}",loginRtn);
			if(KEYNAME.equals("PPKEY"))
				KeyLabel = getVA().va.getKEYLABEL_PPKEY();
			if(KEYNAME.equals("PPKEY2"))
				KeyLabel = getVA().va.getKEYLABEL_PPKEY2();
			
			if (loginRtn == 0) {
	            sid = sso.GetSessionID();
	            log.warn("getPINNEW -> sid: " + sid + " | " + "sTransSeq: " + sTransSeq);
	 			log.warn("pwd2SHA1:" + datasources);
	 			log.warn("KeyLabel :" + KeyLabel);
				icv = getICV();	             
	            flag = sso.SymEncrypt(sid, sTransSeq, KeyLabel, Encryptsrc, iMode, iPadding, icv);
				if (flag == 0) {
					encrypt = sso.getCipher();
					// int flag1 = sso.SymDecrypt(sid, sTransSeq, KeyLabel, encrypt, iMode,
					// iPadding, icv);
					// decryptstr = sso.getDecryptData();
					log.warn("[getPINNEW] getPINNEW data(HEX) :" + StrUtils.toHex(encrypt) + "  encrypt.length>>"
							+ encrypt.length);
					log.warn("[getPINNEW] getPINNEW data(HEX+new String) :" + new String(StrUtils.toHex(encrypt)));
					// 取前24位
					if (encrypt != null) {
						for (int i = 0; i < newEncrypt.length; i++) {
							newEncrypt[i] = encrypt[i];
						}
					}
					pinnew = StrUtils.toHex(newEncrypt);
					log.warn(" getPINNEW data(HEX) 48:" + pinnew);
					// log.warn("encrypt ok !!"+"decrypt string:"+new String(decryptstr));
					log.warn("encrypt ok !!");
					
				} else {
					log.warn("[getPINNEW] sso getPINNEW ErrorMsg: " + sso.GetErrorMsg());
					/*
					 * TODO jimmy 2019 CheckServerHealth checkserverhealth =
					 * (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
					 * checkserverhealth.sendZ300Notice("Z300");
					 */
					throw TopMessageException.create("Z300"); // 押解碼錯誤
				}
				
	         } else {
	            	log.warn("[getPINNEW] sso Login fail ErrorMsg: " + sso.GetErrorMsg());
	            	/* TODO jimmy 2019
	            	CheckServerHealth checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
	            	checkserverhealth.sendZ300Notice("Z300");
	            	*/
	            	throw TopMessageException.create("Z300");  //押解碼錯誤
	         }    

		} catch(Exception e) {
//			log.warn("getPINNEW has error ===========>"+e.toString());
			log.error("getPINNEW.error>>",e);
			/* TODO jimmy 2019
        	CheckServerHealth checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
        	checkserverhealth.sendZ300Notice("Z300");
        	*/
        	throw TopMessageException.create("Z300");  //押解碼錯誤
        	
		} finally {
			try {
	            sso.Logout(sid, sTransSeq);
	            log.warn("logout -> sid: " + sid + " | " + "sTransSeq: " + sTransSeq);
	            DateTime d2 = new DateTime();
	            log.info("VA.getPINNEW.pstimediff: {}", DateUtil.getDiffTimeMills(d1, d2));
	            
	        } catch (Exception fe){
	        	log.warn("[logout] finally occur Exception: " + fe.toString());
	        	throw TopMessageException.create("Z300");
	        }
		}		
		// Ending.
		log.warn("getPINNEW Finished.");  
		return pinnew;
	}
	
	/**
	 * getPINNEW
	 */
	public static String getPINBLOCK(String pinblock) throws Exception {
		byte[] encrypt = null;
		String KeyLabel = getVA().va.getKEYLABEL_PINBLOCK();
		pinblock = StrUtils.left(pinblock + StrUtils.repeat("F", 16), 16);
		//byte[] bDatax = new BigInteger(pinblock, 16).toByteArray();
		byte[] bDatax = pack(pinblock);
		byte[] icv = null;
		int iMode = TransSecureSrv.CIPHER_MODE_CBC;// 值是0
		int iPadding = TransSecureSrv.CIPHER_PADDING_ZERO;// 值是1
		int flag = -1;
		//Date d = new Date();
		//String sTransSeq = DateTimeUtils.format(TIME_PATTERN, d);		
		String sTransSeq = UUID.randomUUID().toString();
//		int loginRtn = -1;
		String URI = getVA().va.getURI();
		String ID =  getVA().va.getID();
		String PWD = getVA().va.getPWD();
		String CONNECTTIMEOUT = getVA().va.getCONNECTTIMEOUT();
		String sid = null;
		int loginRtn = -1;
		
		log.warn("VA API URL:"+URI+" ID:"+ID+" PWD:"+PWD);
		TransSecureSrv sso = null;
		
		DateTime d1 = new DateTime();
		
		try {
			sso = new TransSecureSrv(URI, "NNB");
//			sso.setConnectTimeout(15000); //定義15秒timeout
			int connectTimeOut = Integer.parseInt(CONNECTTIMEOUT);
			sso.setConnectTimeout(connectTimeOut*1000); // 依環境變數定義timeout
            loginRtn = sso.Login(ID,PWD, sTransSeq);
            
            if (loginRtn == 0) {
                sid = sso.GetSessionID();
                log.warn("VA Login() -> sid: " + sid + " | " + "sTransSeq: " + sTransSeq);
            } else {
            	log.warn("[VA] sso Login ErrorMsg: " + sso.GetErrorMsg());
            	throw TopMessageException.create("Z300");
            }
            
		} catch (Exception e) {
			log.warn("[VA] occur Exception: " + e.toString());
			throw TopMessageException.create("Z300");
		}
		
		String pinblock1 ="";
		try {    					
			if (loginRtn == 0) {
	            sid = sso.GetSessionID();
	            log.warn("getPINBLOCK -> sid: " + sid + " | " + "sTransSeq: " + sTransSeq);
	 			log.warn("KeyLabel :" + KeyLabel);
	 			log.warn("before encrypt pinblock :" + StrUtils.toHex(bDatax));
				icv = getIV();	             
	            flag = sso.SymEncrypt(sid, sTransSeq, KeyLabel, bDatax, iMode, iPadding, icv);
				if (flag == 0) {
					encrypt = sso.GetCipherMAC();
					log.warn("[getPINBLOCK] sso getPINBLOCK Successful");
					log.warn("[getPINBLOCK] encrypt.length>>" + encrypt.length);
					log.warn("[getPINBLOCK] PINBLOCK data(HEX) :" + StrUtils.toHex(encrypt));

					pinblock1 = StrUtils.toHex(encrypt);
					log.warn("PINBLOCK data(HEX) :" + pinblock1);
					log.warn("encrypt ok !!");
				} else {
					log.warn("[getPINBLOCK] sso getPINBLOCK ErrorMsg: " + sso.GetErrorMsg());
					/*
					 * TODO jimmy 2019 CheckServerHealth checkserverhealth =
					 * (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
					 * checkserverhealth.sendZ300Notice("Z300");
					 */
					throw TopMessageException.create("Z300"); // 押解碼錯誤
				}
	         } else {
            	log.warn("[getPINBLOCK] sso Login fail ErrorMsg: " + sso.GetErrorMsg());
            	/* TODO jimmy 2019
            	CheckServerHealth checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
            	checkserverhealth.sendZ300Notice("Z300");
            	*/
            	throw TopMessageException.create("Z300");  //押解碼錯誤
	         }    

		} catch(Exception e) {
			log.warn("getPINBLOCK has error ===========>"+e.toString());
			/* TODO jimmy 2019
        	CheckServerHealth checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
        	checkserverhealth.sendZ300Notice("Z300");
        	*/
        	throw TopMessageException.create("Z300");  //押解碼錯誤
        	
		} finally {
			try {
	            sso.Logout(sid, sTransSeq);
	            log.warn("logout -> sid: " + sid + " | " + "sTransSeq: " + sTransSeq);
	            DateTime d2 = new DateTime();
	            log.info("VA.getPINBLOCK.pstimediff: {}", DateUtil.getDiffTimeMills(d1, d2));
	            
	        } catch (Exception fe){
	        	log.warn("[logout] finally occur Exception: " + fe.toString());
	        	throw TopMessageException.create("Z300");
	        }
		}		
		// Ending.
		log.warn("getPINBLOCK Finished.");
		return pinblock1;
	}
	
	/**
	 * getIV
	 */
	private static byte[] getIV() {
		return new byte[]{0x00,0x00,0x00,0x00,
				0x00,0x00,0x00,0x00,
				0x00,0x00,0x00,0x00,
				0x00,0x00,0x00,0x00};
	}
	
	/**
	 * pack
	 */
	static byte[] pack(String str) {
		try {
			char[] cAray = str.toCharArray();
			byte[] bDatac = Hex.decodeHex(cAray);
			return bDatac;
		} catch (Exception e) {
			throw new RuntimeException("pack error !", e);
		}
	}
	
	/**
	 * I-KEY憑證驗章
	 */
	public static String doVAVerify1(String sSignature, String jsondc, String RARules) {
		Date d = new Date();
		//String sTransSeq = DateTimeUtils.format(TIME_PATTERN, d);		
		String sTransSeq = UUID.randomUUID().toString();
//		int loginRtn = -1;
		String URI = getVA().va.getURI();
		String ID =  getVA().va.getID();
		String PWD = getVA().va.getPWD();
		String CONNECTTIMEOUT = getVA().va.getCONNECTTIMEOUT();
		String sid = null;
		int loginRtn = -1;
		
		log.warn("VA API URL:"+URI+" ID:"+ID+" PWD:"+PWD);
		TransSecureSrv sso = null;
		
		DateTime d1 = new DateTime();
		
		try {
			sso = new TransSecureSrv(URI, "NNB");
//			sso.setConnectTimeout(15000); //定義15秒timeout
			int connectTimeOut = Integer.parseInt(CONNECTTIMEOUT);
			sso.setConnectTimeout(connectTimeOut*1000); // 依環境變數定義timeout
            loginRtn = sso.Login(ID,PWD, sTransSeq);
            if (loginRtn == 0) {
                sid = sso.GetSessionID();
                log.warn("VA Login() -> sid: " + sid + " | " + "sTransSeq: " + sTransSeq);
                
            } else {
            	log.warn("[VA] sso Login ErrorMsg: " + sso.GetErrorMsg());
            	throw TopMessageException.create("Z300");
            }
            
		} catch (Exception e) {
			log.warn("[VA] occur Exception: " + e.toString());
			throw TopMessageException.create("Z300");
		}
		
		int checksignflag = -1;
		String certinfo = "";
		//byte[] sData1=sData.getBytes("UTF16-LE");
		try {
            if (loginRtn == 0) {
                sid = sso.GetSessionID();
                log.warn("VAVerify1 -> sid: " + sid + " | " + "sTransSeq: " + sTransSeq);
                checksignflag = sso.VAVerify(sTransSeq, sSignature, jsondc, "", sso.SS_FLAG_VERIFY_FULL, sso.CHECKDB_RA_FXML, d, true, RARules);
                
                if (checksignflag == 0) {
                	log.warn("[VAVerify1] sso VAVerify1 Successful");
                	certinfo=sso.GetSubject()+"|";  //回傳憑證資訊
                	log.warn("[VAVerify1] sso GetContent:"+sso.GetContent());
                	log.warn("[VAVerify1] sso GetSubject:"+sso.GetSubject());
                	log.warn("[VAVerify1] sso GetSerial:"+sso.GetSerial());
                } else {
                	log.warn("[VAVerify1] sso VAVerify1 ErrorMsg: " + sso.GetErrorMsg());
                	throw TopMessageException.create("Z300");
                }
                
            } else {
            	log.warn("[VAVerify1] sso VAVerify1 ErrorMsg: " + sso.GetErrorMsg());
            	throw TopMessageException.create("Z300");
            }     
            
		} catch (Exception e) {
			log.warn("[VAVerify1] occur Exception: " + e.toString());
			throw TopMessageException.create("Z300");
			
		} finally {
			try {
	            sso.Logout(sid, sTransSeq);
	            log.warn("logout -> sid: " + sid + " | " + "sTransSeq: " + sTransSeq);
	            DateTime d2 = new DateTime();
	            log.info("VA.doVAVerify1.pstimediff: {}", DateUtil.getDiffTimeMills(d1, d2));
	            
	        } catch (Exception fe){
	        	log.warn("[logout] finally occur Exception: " + fe.toString());
	        	throw TopMessageException.create("Z300");
	        }
		}		
		return certinfo;
	}
	
	/**
	 * getMAC
	 */
	public static String getMAC(String dataSource) throws Exception{
		log.warn(ESAPIUtil.vaildLog("dataSource>>"+dataSource));
		String macData="";
		byte[] icv = null;
		String KeyLabel = getVA().va.getKEYLABEL_MAC();
		byte[] encrypt = null;
		int iMode = TransSecureSrv.CIPHER_MODE_CBC;// 值是0
		int iPadding = TransSecureSrv.CIPHER_PADDING_ZERO;// 值是1
		int flag = -1;
		//Date d = new Date();
		//String sTransSeq = DateTimeUtils.format(TIME_PATTERN, d);		
		String sTransSeq = UUID.randomUUID().toString();
//		int loginRtn = -1;
		String URI = getVA().va.getURI();
		String ID =  getVA().va.getID();
		String PWD = getVA().va.getPWD();
		String CONNECTTIMEOUT = getVA().va.getCONNECTTIMEOUT();
		String sid = null;
		int loginRtn = -1;
		
		log.warn("VA API URL:"+URI+" ID:"+ID+" PWD:"+PWD);
		TransSecureSrv sso = null;
		
		DateTime d1 = new DateTime();
		
		try {
			sso = new TransSecureSrv(URI, "NNB");
//			sso.setConnectTimeout(15000); //定義15秒timeout
			int connectTimeOut = Integer.parseInt(CONNECTTIMEOUT);
			sso.setConnectTimeout(connectTimeOut*1000); // 依環境變數定義timeout
            loginRtn = sso.Login(ID,PWD, sTransSeq);
            if (loginRtn == 0) {
                sid = sso.GetSessionID();
                log.warn("VA Login() -> sid: " + sid + " | " + "sTransSeq: " + sTransSeq);
                
            } else {
            	log.warn("[VA] sso Login ErrorMsg: " + sso.GetErrorMsg());
            	throw TopMessageException.create("Z300");
            }
            
		} catch (Exception e) {
			log.warn("[VA] occur Exception: " + e.toString());
			throw TopMessageException.create("Z300");
		}
		
		try {
			if (loginRtn == 0) {
				sid = sso.GetSessionID();
				byte[] data = dataSource.getBytes("IBM937");
				log.warn(ESAPIUtil.vaildLog("dataSource:"+ dataSource));
				log.warn("dataSource.length():" + dataSource.length());
				log.warn("KeyLabel :" + KeyLabel);
				log.warn("data.length :" + data.length);
				icv = getICV();
				flag = sso.SymEncrypt(sid, sTransSeq, KeyLabel, data, iMode, iPadding, icv);
				
				if (flag == 0) {
					encrypt = sso.GetCipherMAC();
					log.warn("encrypt.length>>" + encrypt.length);
					macData = StrUtils.toHex(encrypt);
					log.warn("macData (HEX) :" + macData);
					log.warn("encrypt ok !!");
				} else {
					log.warn("[getMAC] sso getMAC ErrorMsg: " + sso.GetErrorMsg());
					/*
					 * TODO jimmy 2019 CheckServerHealth checkserverhealth =
					 * (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
					 * checkserverhealth.sendZ300Notice("Z300");
					 */
					throw TopMessageException.create("Z300"); // 押解碼錯誤
				}
			}
			else {
            	log.warn("[getMAC] sso Login fail ErrorMsg: " + sso.GetErrorMsg());
            	/* TODO jimmy 2019
            	CheckServerHealth checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
            	checkserverhealth.sendZ300Notice("Z300");
            	*/
            	throw TopMessageException.create("Z300");  //押解碼錯誤				
			}
			
		} catch (Exception e) {
			log.warn("getMac.error>>"+e.toString());
			throw TopMessageException.create("Z300");  //押解碼錯誤
			
		} finally {
			try {
	            sso.Logout(sid, sTransSeq);
	            log.warn("logout -> sid: " + sid + " | " + "sTransSeq: " + sTransSeq);
	            DateTime d2 = new DateTime();
	            log.info("VA.getMAC.pstimediff: {}", DateUtil.getDiffTimeMills(d1, d2));
	        } catch (Exception fe){
	        	log.warn("[logout] finally occur Exception: " + fe.toString());
	        	throw TopMessageException.create("Z300");
	        }
		}			
		// Ending.
		log.warn("Finished.");
		return macData;		
	}
	
	/**
	 * getMAC_BH
	 */
	public static String getMAC_BH(String dataSource) throws  Exception{
		log.warn(ESAPIUtil.vaildLog("dataSource>>"+dataSource));
		String macData="";
		byte[] icv = null;
		String KeyLabel = getVA().va.getKEYLABEL_MAC_BH();
		byte[] encrypt = null;
		int iMode = TransSecureSrv.CIPHER_MODE_CBC;// 值是0
		int iPadding = TransSecureSrv.CIPHER_PADDING_ZERO;// 值是1
		int flag = -1;
		//Date d = new Date();
		//String sTransSeq = DateTimeUtils.format(TIME_PATTERN, d);		
		String sTransSeq = UUID.randomUUID().toString();
//		int loginRtn = -1;
		String URI = getVA().va.getURI();
		String ID =  getVA().va.getID();
		String PWD = getVA().va.getPWD();
		String CONNECTTIMEOUT = getVA().va.getCONNECTTIMEOUT();
		String sid = null;
		int loginRtn = -1;
		
		log.warn("VA API URL:" + URI + " ID:" + ID + " PWD:" + PWD);
		TransSecureSrv sso = null;

		DateTime d1 = new DateTime();

		try {
			sso = new TransSecureSrv(URI, "NNB");
//			sso.setConnectTimeout(15000); //定義15秒timeout
			int connectTimeOut = Integer.parseInt(CONNECTTIMEOUT);
			sso.setConnectTimeout(connectTimeOut * 1000); // 依環境變數定義timeout
			loginRtn = sso.Login(ID, PWD, sTransSeq);
			
			if (loginRtn == 0) {
				sid = sso.GetSessionID();
				log.warn("VA Login() -> sid: " + sid + " | " + "sTransSeq: " + sTransSeq);
				
			} else {
				log.warn("[VA] sso Login ErrorMsg: " + sso.GetErrorMsg());
				throw TopMessageException.create("Z300");
			}
			
		} catch (Exception e) {
			log.warn("[VA] occur Exception: " + e.toString());
			throw TopMessageException.create("Z300");
		}
		
		try {
			if (loginRtn == 0) {
				sid = sso.GetSessionID();
				// byte[] data = dataSource.getBytes("IBM937");
				byte[] data = hexToBytes(dataSource);
				log.warn("dataSource(HexToByte):" + dataSource);
				log.warn("dataSource.length():" + dataSource.length());
				log.warn("KeyLabel :" + KeyLabel);
				log.warn("data.length :" + data.length);
				flag = sso.SymEncrypt(sid, sTransSeq, KeyLabel, data, iMode, iPadding, icv);
				if (flag == 0) {
					encrypt = sso.getCipher();
					log.warn("encrypt.length>>" + encrypt.length);
					macData = StrUtils.toHex(encrypt);
					log.warn("macData (HEX) :" + macData);
					log.warn("encrypt ok !!");
					
				} else {
					log.warn("[getMAC_BH] sso getMAC_BH ErrorMsg: " + sso.GetErrorMsg());
					// TODO
					// CheckServerHealth checkserverhealth =
					// (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
					// checkserverhealth.sendZ300Notice("Z300");
					throw TopMessageException.create("Z300"); // 押解碼錯誤
				}
			} else {
				log.warn("[getMAC_BH] sso Login fail ErrorMsg: " + sso.GetErrorMsg());
				// CheckServerHealth checkserverhealth =
				// (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
				// checkserverhealth.sendZ300Notice("Z300");
				throw TopMessageException.create("Z300"); // 押解碼錯誤
			}
			
		} catch (Exception e) {
			log.warn("getMAC_BH.error>>"+e.toString());
			throw TopMessageException.create("Z300");  //押解碼錯誤
			
		} finally {
			try {
	            sso.Logout(sid, sTransSeq);
	            log.warn("logout -> sid: " + sid + " | " + "sTransSeq: " + sTransSeq);
	            DateTime d2 = new DateTime();
	            log.info("VA.getMAC_BH.pstimediff: {}", DateUtil.getDiffTimeMills(d1, d2));
	        } catch (Exception fe){
	        	log.warn("[logout] finally occur Exception: " + fe.toString());
	        	throw TopMessageException.create("Z300");
	        }
		}			
		// Ending.
		log.warn("Finished.");
		return macData;		
	}

	/**
	 * genVaMacKey
	 */
	public static byte[] genVaMacKey(String keyLabel, byte[] divData) throws Exception {
		log.debug("Run Va genMacKey !!");
		int loginRtn = -1;
		int encryptRtn = -1;
		String sid = null;
		int iMode = 1; //1:ECB/0:CBC
		int iPadding = 1; //1:Padding /0:PKCS#5 Padding 方式
		byte[] bIV = null; //Initial Vector，若使用 CBC Mode時必須傳入
		byte[] macKey = null;		
		String sTransSeq = UUID.randomUUID().toString();
		String URI = getVA().va.getURI();
		String ID =  getVA().va.getID();
		String PWD = getVA().va.getPWD();
		log.warn("VA API URL:"+URI+" ID:"+ID+" PWD:"+PWD);
		TransSecureSrv sso = null;
		
		DateTime d1 = new DateTime();
		
		try {
			log.debug("VaSecurity VAURL:"+URI);
			log.debug("VaSecurity VAAPNAME:"+ "NNB");
            sso = new TransSecureSrv(URI, "NNB");
            loginRtn = sso.Login(ID,PWD,sTransSeq);
            if (loginRtn == 0) {
                sid = sso.GetSessionID();
                log.debug("genVaMacKey -> sid: " + sid + " | " + "sTransSeq: " + sTransSeq + " | " + "keyLabel: " + keyLabel);
                encryptRtn = sso.SymEncrypt(sid, sTransSeq, keyLabel, divData, iMode, iPadding, bIV);

                if (encryptRtn == 0) {
                	macKey = sso.getCipher();
                	
                } else {
                	log.error("[VaSecurity.genVaMacKey] sso SymEncrypt ErrorMsg: " + sso.GetErrorMsg());
                	throw TopMessageException.create("Z300");
                }
                
            } else {
            	log.error("[VaSecurity.genVaMacKey] sso Login ErrorMsg: " + sso.GetErrorMsg());
            	throw TopMessageException.create("Z300");
            }
            
		} catch (Exception e) {
			log.error("[VaSecurity.genVaMacKey] occur Exception: " + e.toString());
			throw TopMessageException.create("Z300");
			
		} finally {
			try {
				sso.Logout(sid, sTransSeq);
				DateTime d2 = new DateTime();
				log.info("VA.genVaMacKey.pstimediff: {}", DateUtil.getDiffTimeMills(d1, d2));
				
			} catch (Exception fe){
				log.error("[VaSecurity.genVaMacKey] finally occur Exception: " + fe.toString());
				throw TopMessageException.create("Z300");
			}
			sso = null;
		}
		return macKey;
	}

	/**
	 * genVaMac
	 */
	public static byte[] genVaMac(byte[] keyValue, byte[] bIV, byte[] sourceData) throws Exception {
		log.debug("Run Va genMac !!");
		int loginRtn = -1;
		int encryptRtn = -1;
		String sid = null;
		String sKeyType = "DES2";
//				DES  – 採用DES，KeyValue長度必須為8 Bytes
//				DES2 – 採用DES2，KeyValue長度必須為16 Bytes
//				3DES – 採用3DES，KeyValue長度必須為24 Bytes
//				AES  – 採用AES，KeyValue長度必須為16或24 Bytes
		int iMode = 0; //1:ECB/0:CBC
		int iPadding = 1; //1:Padding /0:PKCS#5 Padding 方式
		byte[] macData = null;		
		String sTransSeq = UUID.randomUUID().toString();
		String URI = getVA().va.getURI();
		String ID =  getVA().va.getID();
		String PWD = getVA().va.getPWD();
		log.warn("VA API URL:"+URI+" ID:"+ID+" PWD:"+PWD);
		TransSecureSrv sso = null;
		
		DateTime d1 = new DateTime();
		
		try {
            sso = new TransSecureSrv(URI, "NNB");
            loginRtn = sso.Login(ID,PWD, sTransSeq);
            if (loginRtn == 0) {
                sid = sso.GetSessionID();
                log.debug("genVaMac -> sid: " + sid + " | " + "sTransSeq: " + sTransSeq);
                encryptRtn = sso.encryptByExplictKey(sTransSeq, sKeyType, keyValue, iPadding, iMode, bIV, sourceData);

                if (encryptRtn == 0) {
                	macData = sso.GetCipherMAC();
                	
                } else {
                	log.error("[VaSecurity.genVaMac] sso SymEncrypt ErrorMsg: " + sso.GetErrorMsg());
                	throw TopMessageException.create("Z300");
                }
            } else {
            	log.error("[VaSecurity.genVaMac] sso Login ErrorMsg: " + sso.GetErrorMsg());
            	throw TopMessageException.create("Z300");
            }
            
		} catch (Exception e) {
			log.error("[VaSecurity.genVaMac] occur Exception: " + e.toString());
			throw TopMessageException.create("Z300");
			
		} finally {
			try {
				sso.Logout(sid, sTransSeq);
				DateTime d2 = new DateTime();
				log.info("VA.genVaMac.pstimediff: {}", DateUtil.getDiffTimeMills(d1, d2));
			} catch (Exception fe){
				log.error("[VaSecurity.genVaMac] finally occur Exception: " + fe.toString());
				throw TopMessageException.create("Z300");
			}
			sso = null;
		}
		
		return macData;
	}

	/**
	 * genVaMac
	 */
	public static byte[] genVaMac3DES(byte[] keyValue, byte[] bIV, byte[] sourceData) throws Exception {
		log.debug("Run Va genMac !!");
		int loginRtn = -1;
		int encryptRtn = -1;
		String sid = null;
		String sKeyType = "3DES";
//				DES  – 採用DES，KeyValue長度必須為8 Bytes
//				DES2 – 採用DES2，KeyValue長度必須為16 Bytes
//				3DES – 採用3DES，KeyValue長度必須為24 Bytes
//				AES  – 採用AES，KeyValue長度必須為16或24 Bytes
		int iMode = 0; //1:ECB/0:CBC
		int iPadding = 1; //1:Padding /0:PKCS#5 Padding 方式
		byte[] macData = null;		
		String sTransSeq = UUID.randomUUID().toString();
		String URI = getVA().va.getURI();
		String ID =  getVA().va.getID();
		String PWD = getVA().va.getPWD();
		log.warn("VA API URL:"+URI+" ID:"+ID+" PWD:"+PWD);
		TransSecureSrv sso = null;
		
		DateTime d1 = new DateTime();
		
		try {
            sso = new TransSecureSrv(URI, "NNB");
            loginRtn = sso.Login(ID,PWD, sTransSeq);
            if (loginRtn == 0) {
                sid = sso.GetSessionID();
                log.debug("genVaMac -> sid: " + sid + " | " + "sTransSeq: " + sTransSeq);
                encryptRtn = sso.encryptByExplictKey(sTransSeq, sKeyType, keyValue, iPadding, iMode, bIV, sourceData);

                if (encryptRtn == 0) {
                	macData = sso.getCipher();
                	
                } else {
                	log.error("[VaSecurity.genVaMac] sso SymEncrypt ErrorMsg: " + sso.GetErrorMsg());
                	throw TopMessageException.create("Z300");
                }
            } else {
            	log.error("[VaSecurity.genVaMac] sso Login ErrorMsg: " + sso.GetErrorMsg());
            	throw TopMessageException.create("Z300");
            }
            
		} catch (Exception e) {
			log.error("[VaSecurity.genVaMac] occur Exception: " + e.toString());
			throw TopMessageException.create("Z300");
			
		} finally {
			try {
				sso.Logout(sid, sTransSeq);
				DateTime d2 = new DateTime();
				log.info("VA.genVaMac.pstimediff: {}", DateUtil.getDiffTimeMills(d1, d2));
			} catch (Exception fe){
				log.error("[VaSecurity.genVaMac] finally occur Exception: " + fe.toString());
				throw TopMessageException.create("Z300");
			}
			sso = null;
		}
		
		return macData;
	}

	/**
	 * genVaMacKey
	 */
	public static byte[] EBCEncrypt(byte[] keyValue, byte[] sourceData) throws Exception {
		log.debug("Run Va EBCEncrypt !!");
		int loginRtn = -1;
		int encryptRtn = -1;
		String sid = null;
        String sKeyType = "3DES";
		int iMode = 1; //1:ECB/0:CBC
		int iPadding = 1; //1:Padding /0:PKCS#5 Padding 方式
		byte[] bIV = null; //Initial Vector，若使用 CBC Mode時必須傳入
		byte[] macData = null;		
		String sTransSeq = UUID.randomUUID().toString();
		String URI = getVA().va.getURI();
		String ID =  getVA().va.getID();
		String PWD = getVA().va.getPWD();
		log.warn("VA API URL:"+URI+" ID:"+ID+" PWD:"+PWD);
		TransSecureSrv sso = null;
		
		DateTime d1 = new DateTime();
		
		try {
			log.debug("VaSecurity VAURL:"+URI);
			log.debug("VaSecurity VAAPNAME:"+ "NNB");
            sso = new TransSecureSrv(URI, "NNB");
            loginRtn = sso.Login(ID,PWD,sTransSeq);
            if (loginRtn == 0) {
                sid = sso.GetSessionID();
                log.debug("genVaMacKey -> sid: " + sid + " | " + "sTransSeq: " + sTransSeq + " | " + "keyValue: " + keyValue);
                encryptRtn = sso.encryptByExplictKey(sTransSeq, sKeyType, keyValue, iPadding, iMode, null, sourceData);

                if (encryptRtn == 0) {
                    macData = sso.getCipher();
                	
                } else {
                	log.error("[VaSecurity.genVaMacKey] sso SymEncrypt ErrorMsg: " + sso.GetErrorMsg());
                	throw TopMessageException.create("Z300");
                }
                
            } else {
            	log.error("[VaSecurity.genVaMacKey] sso Login ErrorMsg: " + sso.GetErrorMsg());
            	throw TopMessageException.create("Z300");
            }
            
		} catch (Exception e) {
			log.error("[VaSecurity.genVaMacKey] occur Exception: " + e.toString());
			throw TopMessageException.create("Z300");
			
		} finally {
			try {
				sso.Logout(sid, sTransSeq);
				DateTime d2 = new DateTime();
				log.info("VA.genVaMacKey.pstimediff: {}", DateUtil.getDiffTimeMills(d1, d2));
				
			} catch (Exception fe){
				log.error("[VaSecurity.genVaMacKey] finally occur Exception: " + fe.toString());
				throw TopMessageException.create("Z300");
			}
			sso = null;
		}
		return macData;
	}

	/**
	 * genVaMacKey
	 */
	public static byte[] EBCDecrypt(byte[] keyValue, byte[] sourceData) throws Exception {
		log.debug("Run Va EBCDecrypt !!");
		int loginRtn = -1;
		int encryptRtn = -1;
		String sid = null;
        String sKeyType = "3DES";
		int iMode = 1; //1:ECB/0:CBC
		int iPadding = 1; //1:Padding /0:PKCS#5 Padding 方式
		byte[] bIV = null; //Initial Vector，若使用 CBC Mode時必須傳入
		byte[] macData = null;		
		String sTransSeq = UUID.randomUUID().toString();
		String URI = getVA().va.getURI();
		String ID =  getVA().va.getID();
		String PWD = getVA().va.getPWD();
		log.warn("VA API URL:"+URI+" ID:"+ID+" PWD:"+PWD);
		TransSecureSrv sso = null;
		
		DateTime d1 = new DateTime();
		
		try {
			log.debug("VaSecurity VAURL:"+URI);
			log.debug("VaSecurity VAAPNAME:"+ "NNB");
            sso = new TransSecureSrv(URI, "NNB");
            loginRtn = sso.Login(ID,PWD,sTransSeq);
            if (loginRtn == 0) {
                sid = sso.GetSessionID();
                log.debug("genVaMacKey -> sid: " + sid + " | " + "sTransSeq: " + sTransSeq + " | " + "keyValue: " + keyValue);
                encryptRtn = sso.decryptByExplictKey(sTransSeq, sKeyType, keyValue, iPadding, iMode, null, sourceData);

                if (encryptRtn == 0) {
                    macData = sso.getDecryptData();
                	
                } else {
                	log.error("[VaSecurity.genVaMacKey] sso SymEncrypt ErrorMsg: " + sso.GetErrorMsg());
                	throw TopMessageException.create("Z300");
                }
                
            } else {
            	log.error("[VaSecurity.genVaMacKey] sso Login ErrorMsg: " + sso.GetErrorMsg());
            	throw TopMessageException.create("Z300");
            }
            
		} catch (Exception e) {
			log.error("[VaSecurity.genVaMacKey] occur Exception: " + e.toString());
			throw TopMessageException.create("Z300");
			
		} finally {
			try {
				sso.Logout(sid, sTransSeq);
				DateTime d2 = new DateTime();
				log.info("VA.genVaMacKey.pstimediff: {}", DateUtil.getDiffTimeMills(d1, d2));
				
			} catch (Exception fe){
				log.error("[VaSecurity.genVaMacKey] finally occur Exception: " + fe.toString());
				throw TopMessageException.create("Z300");
			}
			sso = null;
		}
		return macData;
	}

	/**
	 * hexToBytes
	 */
	public static byte[] hexToBytes(String hexString) {
		char[] hex = hexString.toCharArray();
		// 轉rawData長度減半
		int length = hex.length / 2;
		byte[] rawData = new byte[length];
		for (int i = 0; i < length; i++) {
			// 先將hex資料轉10進位數值
			int high = Character.digit(hex[i * 2], 16);
			int low = Character.digit(hex[i * 2 + 1], 16);
			// 將第一個值的二進位值左平移4位,ex: 00001000 => 10000000 (8=>128)
			// 然後與第二個值的二進位值作聯集ex: 10000000 | 00001100 => 10001100 (137)
			int value = (high << 4) | low;
			// 與FFFFFFFF作補集
			if (value > 127)
				value -= 256;
			// 最後轉回byte就OK
			rawData[i] = (byte) value;
		}
		return rawData;
	}

	
	/*
	 * 登入用(VA一次登入做PPSYNC及PINNEW)
	 */
	public static String getPPSYNCN911(String KEYNAME, TransSecureSrv sso) throws Exception {
		String ekey = "";
		String ppsync= "";
		ekey = getSYNC_IKEY();
		byte[] encrypt = null;
		byte[] newEncrypt =null;
		String KeyLabel = "";
		if(KEYNAME.equals("PPKEY"))  //一般交易使用之KEY
			KeyLabel = getVA().va.getKEYLABEL_PPKEY();
		if(KEYNAME.equals("PPKEY2"))   //隨護神盾申請使用之KEY
			KeyLabel = getVA().va.getKEYLABEL_PPKEY2();
		byte[] data = null;
		byte[] icv = null;
		int iMode = TransSecureSrv.CIPHER_MODE_CBC;// 值是0
		int iPadding = TransSecureSrv.CIPHER_PADDING_ZERO;// 值是1
		int flag = -1;
		String sTransSeq = UUID.randomUUID().toString();
		String URI = getVA().va.getURI();
		String ID =  getVA().va.getID();
		String PWD = getVA().va.getPWD();
		String sid = null;
		
		log.warn("VA API URL:"+URI+" ID:"+ID+" PWD:"+PWD);
		
		DateTime d1 = new DateTime();
		
		try {    					
            sid = sso.GetSessionID();
            log.warn("getPPSYNCN911 -> sid: " + sid + " | " + "sTransSeq: " + sTransSeq);
 			data = ekey.getBytes("IBM937");
 			log.warn("ekey:" + ekey);
 			log.warn("KeyLabel :" + KeyLabel);
			icv = getICV();	             
            flag = sso.SymEncrypt(sid, sTransSeq, KeyLabel, data, iMode, iPadding, icv);
			if (flag == 0) {
				encrypt = sso.getCipher();
				log.warn("[getPPSYNCN911] sso getPPSYNCN911 Successful");
				log.warn("[getPPSYNCN911] encrypt.length>>" + encrypt.length);
				log.warn("[getPPSYNCN911] getPPSYNCN911 data(HEX) :" + new String(StrUtils.toHex(encrypt)));
				newEncrypt = new byte[4];
				if (encrypt != null && encrypt.length >= 4) {
					for (int i = 0; i < newEncrypt.length; i++) {
						newEncrypt[i] = encrypt[i];
					}
				}
				ppsync = StrUtils.toHex(newEncrypt);
				log.warn(" getPPSYNCN911 data(HEX) :" + ppsync);
				log.warn("encrypt ok !!");
				
			} else {
				log.warn("[getPPSYNCN911] sso getPPSYNCN911 ErrorMsg: " + sso.GetErrorMsg());
				throw TopMessageException.create("Z300"); // 押解碼錯誤
			}
			
		} catch(Exception e) {
			log.warn("getPPSYNCN911 has error ===========>"+e.toString());
        	throw TopMessageException.create("Z300");  //押解碼錯誤
		}
		// Ending.
		log.warn("getPPSYNCN911 Finished.");
		
		DateTime d2 = new DateTime();
		log.info("VA.getPPSYNCN911.pstimediff: {}", DateUtil.getDiffTimeMills(d1, d2));
		
		return ppsync;
		
	}
	
	/*
	 * 登入用(VA一次登入做PPSYNC及PINNEW)
	 */
	public static String getPINNEWN911(String datasources, String KEYNAME, TransSecureSrv sso) throws Exception {
		String pinnew= "";
		byte[] encrypt = null;
		byte[] Encryptsrc = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
        byte[] newEncrypt = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
        
        if(StrUtils.isNotEmpty(datasources)) {
        	log.debug("datasources>>{}",datasources);
        	log.debug("datasources.length>>{}",datasources.length());
        }
        
        byte[] datasource1 = null;
        datasource1 = StrUtils.hexToByte(datasources);
		if(datasource1 !=null){
			for(int i =0 ;i < Encryptsrc.length;i++){
				Encryptsrc[i]=datasource1[i];
			}
		}	   
        log.warn("datasources original:" + datasources+" length:"+datasources.length());
		log.warn("Encryptsrc:" + StrUtils.toHex(Encryptsrc)+"  Encryptsrc.length>>"+Encryptsrc.length);
		String KeyLabel = "";
	
		byte[] icv = null;
		int iMode = TransSecureSrv.CIPHER_MODE_CBC;// 值是0
		int iPadding = TransSecureSrv.CIPHER_PADDING_ZERO;// 值是1
		int flag = -1;
		String sTransSeq = UUID.randomUUID().toString();
		String URI = getVA().va.getURI();
		String ID = getVA().va.getID();
		String PWD = getVA().va.getPWD();
		String sid = null;

		log.warn("VA API URL:" + URI + " ID:" + ID + " PWD:" + PWD);

		DateTime d1 = new DateTime();

		try {
			if (KEYNAME.equals("PPKEY"))
				KeyLabel = getVA().va.getKEYLABEL_PPKEY();
			if (KEYNAME.equals("PPKEY2"))
				KeyLabel = getVA().va.getKEYLABEL_PPKEY2();

			sid = sso.GetSessionID();
			log.warn("getPINNEWN911 -> sid: " + sid + " | " + "sTransSeq: " + sTransSeq);
			log.warn("pwd2SHA1:" + datasources);
			log.warn("KeyLabel :" + KeyLabel);
			icv = getICV();
			flag = sso.SymEncrypt(sid, sTransSeq, KeyLabel, Encryptsrc, iMode, iPadding, icv);
			if (flag == 0) {
				encrypt = sso.getCipher();
				log.warn("[getPINNEWN911] getPINNEWN911 data(HEX) :" + StrUtils.toHex(encrypt) + "  encrypt.length>>"
						+ encrypt.length);
				log.warn("[getPINNEWN911] getPINNEWN911 data(HEX+new String) :" + new String(StrUtils.toHex(encrypt)));
				if (encrypt != null) {
					for (int i = 0; i < newEncrypt.length; i++) {
						newEncrypt[i] = encrypt[i];
					}
				}
				pinnew = StrUtils.toHex(newEncrypt);
				log.warn(" getPINNEWN911 data(HEX) 48:" + pinnew);
				log.warn("encrypt ok !!");
				
			} else {
				log.warn("[getPINNEWN911] sso getPINNEWN911 ErrorMsg: " + sso.GetErrorMsg());
				throw TopMessageException.create("Z300"); // 押解碼錯誤
			}

		} catch (Exception e) {
			log.error("getPINNEWN911.error>>", e);
			throw TopMessageException.create("Z300"); // 押解碼錯誤
		}
		log.warn("getPINNEWN911 Finished.");

		DateTime d2 = new DateTime();
		log.info("VA.getPINNEWN911.pstimediff: {}", DateUtil.getDiffTimeMills(d1, d2));

		return pinnew;
	}	
	
	/**
	 *  VA Check
	 */
	public static TransSecureSrv ssoLogin(String sTransSeq_N911) {

		int loginRtn = -1;

		TransSecureSrv sso_N911 = null;
		String sid_N911 = null;
		
		String URI = getVA().va.getURI();
		String ID =  getVA().va.getID();
		String PWD = getVA().va.getPWD();
		String CONNECTTIMEOUT = getVA().va.getCONNECTTIMEOUT();
		
		log.warn("VA API URL:{} ID:{} PWD:{} CONNECTTIMEOUT:{}", URI, ID, PWD, CONNECTTIMEOUT);
		
		try {
			sso_N911 = new TransSecureSrv(URI, "NNB");
			int connectTimeOut = Integer.parseInt(CONNECTTIMEOUT);
			sso_N911.setConnectTimeout(connectTimeOut*1000); // 依環境變數定義timeout
			
			DateTime d1 = new DateTime();
			
            loginRtn = sso_N911.Login(ID, PWD, sTransSeq_N911);
            DateTime d2 = new DateTime();
			log.info("VA.ssoLogin.pstimediff: {}", DateUtil.getDiffTimeMills(d1, d2));
            
            if (loginRtn == 0) {
            	sid_N911 = sso_N911.GetSessionID();
                log.warn("VA ssoLogin() -> sid: " + sid_N911 + " | " + "sTransSeq: " + sTransSeq_N911);
                
            } else {
            	log.warn("[VA] sso ssoLogin ErrorMsg: " + sso_N911.GetErrorMsg());
            	throw TopMessageException.create("Z300");
            }
            
		} catch (Exception e) {
			log.warn("[VA] ssoLogin Exception:{} " ,e.toString());
			throw TopMessageException.create("Z300");
		}
		return sso_N911;		
	}
	
	/**
	 *  VA Check
	 */
	public static void ssoLogout(TransSecureSrv sso_N911, String sTransSeq_N911) {
		String sid_N911 = null;
		try {
			DateTime d1 = new DateTime();
			
			sid_N911 = sso_N911.GetSessionID();
            log.warn("VA ssoLogout() -> sid: " + sid_N911 + " | " + "sTransSeq: " + sTransSeq_N911);
            
			sso_N911.Logout(sid_N911, sTransSeq_N911);
            log.warn("logout -> sid: " + sid_N911 + " | " + "sTransSeq: " + sTransSeq_N911);
            sso_N911 = null;
            
            DateTime d2 = new DateTime();
			log.info("VA.ssoLogout.pstimediff: {}", DateUtil.getDiffTimeMills(d1, d2));
			
        } catch (Exception fe){
        	log.warn("[logout] finally occur Exception: " + fe.toString());
        	throw TopMessageException.create("Z300");
        }
	}
	
	public static boolean doVAVerifyCN(String sSignature,Map<String, String> params) {
		boolean isRightSignature = false;
		Date d = new Date();
		//String sTransSeq = DateTimeUtils.format(TIME_PATTERN, d);	
		String sTransSeq = UUID.randomUUID().toString().replaceAll("-", "");
        String sessionId  = "";
		
//		int loginRtn = Login(sTransSeq);
		String URI = getVA().va.getURI();
		String ID =  getVA().va.getID();
		String PWD = getVA().va.getPWD();
		String CONNECTTIMEOUT = getVA().va.getCONNECTTIMEOUT();
		
		log.warn("VA API URL:"+URI+" ID:"+ID+" PWD:"+PWD);
		TransSecureSrv sso = null;
		
		try {
			sso = new TransSecureSrv(URI, "MB");
//			sso.setConnectTimeout(15000); //定義15秒timeout
			int connectTimeOut = Integer.parseInt(CONNECTTIMEOUT);
			sso.setConnectTimeout(connectTimeOut*1000); // 依環境變數定義timeout

			int i = sso.Login(ID, PWD, sTransSeq);
			if (i != 0) {
				log.info("@@VA Login Fail.(" + i + ")");
			}
			else {
				sessionId = sso.GetSessionID();
				log.info("===@@VA params.getSIGNATURE() ===================================");
				log.info(ESAPIUtil.vaildLog(sSignature));
				log.info("===@@VA===================================");
				String rule = "VARule1";
				int verifyResult = sso.VAVerify("2500", sSignature, "", "", sso.SS_FLAG_VERIFY_FULL, sso.SS_FLAG_CHECKDB | sso.CHECKDB_RA_2FACTOR
						, new Date(), true, rule);
				if (verifyResult != 0) {
					log.info("@@verify fail.");
					log.info("Error Message : " + sso.GetErrorMsg());
				}
				else {
					log.info("==tss.GetContent()==============================");
					log.info(sso.GetContent());
					Map cnMap = CodeUtil.fromJson(sso.GetContent(), Map.class);
					params.put("CN", cnMap.containsKey("CN") ? (String)cnMap.get("CN") : (String)cnMap.get("cn"));
					log.info("=tss.GetSubject()===============================");
					log.info(sso.GetSubject());
					log.info(sso.GetSerial()); // 取得憑證序號
					log.info(sso.GetCert()); // 取得憑證
					log.info(sso.GetHiCertTailCitizenID()); // 取得新版自然人憑證身分證後  4  碼。
					log.info(sso.GetHiCertUniformOrganizationID()); // 取得新版工商憑證統一編號。
					log.info(sso.GetCertNotBefore()); // 取得憑證生效日期時間
					log.info(sso.GetCertNotAfter()); // 取得憑證到期日期時間
					log.info(sso.GetCertEMail()); // 取得憑證DN內的Email
					log.info(sso.GetTimeStamp()); // 取得簽章時戳

					log.info("================================");
					log.info("@@verify success ......");
					
					isRightSignature = true;
				}
                
            }
            
		} catch (Exception e) {
			log.warn("[VA] occur Exception: " + e.toString());
		}
		finally {
            if(sso!=null) {
                try {
                	sso.Logout(sessionId, sTransSeq);
                }
                catch(Exception e) {}
            }
        }

		return isRightSignature;
	}
}
