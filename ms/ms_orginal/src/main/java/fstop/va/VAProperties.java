package fstop.va;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@Data
public class VAProperties {

//	@Value("${va.URI}")
	@Value("${va.uri}")
	private String URI;
	@Value("${va.id}")
	private String ID;
	@Value("${va.pwd}")
	private String PWD;
	@Value("${va.KEYLABEL_PPKEY2}")
	private String KEYLABEL_PPKEY2;
	@Value("${va.KEYLABEL_PINBLOCK}")
	private String KEYLABEL_PINBLOCK;
	@Value("${va.KEYLABEL_PPKEY}")
	private String KEYLABEL_PPKEY;
	@Value("${va.KEYLABEL_MAC}")
	private String KEYLABEL_MAC;
	@Value("${va.KEYLABEL_MAC_BH}")
	private String KEYLABEL_MAC_BH;
	@Value("${va.timeout}")
	private String CONNECTTIMEOUT;
}
