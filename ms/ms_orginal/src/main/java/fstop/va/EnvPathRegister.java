package fstop.va;

import fstop.model.AdmoplogTranslator;
import fstop.model.B201Translator;
import fstop.model.DefaultTranslator;
import fstop.notifier.NotifyAngent;
import org.springframework.context.ApplicationContext;

import javax.servlet.ServletContext;

public class EnvPathRegister extends java.util.Observable {
	
	private static EnvPathRegister instance = null;
	
	public static String HOST_IP = "";

	public static EnvPathRegister getInstance() {
		if(instance == null) {
			synchronized (EnvPathRegister.class) {
				if(instance == null) {
					instance = new EnvPathRegister();
				}
			} 
		}
		return instance;
	}
	
	static {
		DefaultTranslator t = DefaultTranslator.getInstance();
		AdmoplogTranslator a = AdmoplogTranslator.getInstance();
		B201Translator     b = B201Translator.getInstance();
//		SpringBeanFactory s = SpringBeanFactory.getInstance();
		NotifyAngent m = NotifyAngent.getInstance();
		EnvPathRegister.getInstance().addObserver(t);
		EnvPathRegister.getInstance().addObserver(a);
		EnvPathRegister.getInstance().addObserver(b);
//		EnvPathRegister.getInstance().addObserver(s);
		EnvPathRegister.getInstance().addObserver(m);
	}
	
	private EnvPathRegister(){
	}
	
	
	public static void registerTranslatorPath(String path) {
		getInstance().setChanged();
		getInstance().notifyObservers("TranslatorPath:" + path);
	}
	
	public static void registerServletContext(ServletContext context) {
		getInstance().setChanged();
		getInstance().notifyObservers(context);
	}
	
	public static void registerApplicationContext(ApplicationContext context) {
		getInstance().setChanged();
		getInstance().notifyObservers(context);
	}
	
	public static void destoryContext() {
		getInstance().setChanged();
		getInstance().notifyObservers("DESTORY_CONTEXT");
	}
	public static void registerAdmoplogTranslatorPath(String path) {
		getInstance().setChanged();
		getInstance().notifyObservers("admOpLogTranslatorPath:" + path);
	}
	public static void registerB201TranslatorPath(String path) {
		getInstance().setChanged();
		getInstance().notifyObservers("B201TranslatorPath:" + path);
	}
}
