package fstop.ws.bean;

import lombok.Data;

@Data
public class BHIvrWebServiceOut5 {

	private String rdetail_24_r_c1;
	private String rdetail_24_r_c2;
	private String rdetail_24_r_c3;
	private String rdetail_24_r_c4;
	private String rdetail_24_r_c5;
	
}
