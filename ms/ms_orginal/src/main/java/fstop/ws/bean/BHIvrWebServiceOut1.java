package fstop.ws.bean;

import lombok.Data;

@Data
public class BHIvrWebServiceOut1 {

	private String r12_c1;
	private String r12_c2;
	private String r12_c3;
	private String r12_c4;
	private String r12_c5;
	private String r12_c6;
	private String r12_c7;
	private String r12_c8;
}
