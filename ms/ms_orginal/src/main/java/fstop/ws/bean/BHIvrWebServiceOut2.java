package fstop.ws.bean;

import lombok.Data;

@Data
public class BHIvrWebServiceOut2 {

	private String rdetail_201_r_c1;
	private String rdetail_201_r_c2;
	private String rdetail_201_r_c3;
	
}
