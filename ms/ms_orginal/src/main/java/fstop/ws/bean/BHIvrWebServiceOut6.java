package fstop.ws.bean;

import lombok.Data;

@Data
public class BHIvrWebServiceOut6 {

	private String rdetail_20_r1;
	private int detail_count;	//消費明細筆數
	
}
