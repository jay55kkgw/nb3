package fstop.ws.bean;

import fstop.ws.BHWebServiceObj;
import lombok.Data;

@Data
public class BHIvrWebServiceObj {

	private String rid;
	private String rcustid;
	private String billresult;
	private String billmonth;
	private String address;
	private String custname;
	private BHIvrWebServiceOut1[] bhivrout1=null;
	private BHIvrWebServiceOut2[] bhivrout2=null;
	private BHIvrWebServiceOut3[] bhivrout3=null;
	private BHIvrWebServiceOut4[] bhivrout4=null;
	private BHIvrWebServiceOut5[] bhivrout5=null;
	private BHIvrWebServiceOut6[] bhivrout6=null;
	private BHIvrWebServiceOut7[] bhivrout7=null;
}
