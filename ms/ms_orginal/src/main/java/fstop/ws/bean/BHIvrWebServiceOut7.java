package fstop.ws.bean;

import lombok.Data;

@Data
public class BHIvrWebServiceOut7 {

	private String rdetail_20_r2_c1;
	private String rdetail_20_r2_c2;
	private String rdetail_20_r2_c3;
	private String rdetail_20_r2_c4;
	private String rdetail_20_r2_c5;
	private String rdetail_20_r2_c6;
	private String rdetail_20_r2_c7;
	private String rdetail_20_r2_c8;
	private String rdetail_20_r2_c9;
	private String rdetail_20_r2_c10;
	
}
