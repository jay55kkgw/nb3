package fstop.ws.bean;

import lombok.Data;

@Data
public class BHIvrWebServiceOut3 {

	private String rdetail_202_r_c1;
	private String rdetail_202_r_c2;
	private String rdetail_202_r_c3;
	
}
