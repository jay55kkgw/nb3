package fstop.ws;

import fstop.ws.bh.client.*;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.ws.Holder;
import java.util.Vector;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

public class BHWebServiceTemplate {

	private Logger logger = Logger.getLogger(getClass());

	@Value("${WebService.URI}")
	private String webServiceUri;
	@Value("${WebService.ChangePwd.URI}")
	private String webService_ChangePwdUri;
	
	@Value("${WebService.ReceiveTimeout}")
	private Integer webService_ReceiveTimeout;
//
//	public  String getWebServiceUri() {
//		return webServiceUri;
//	}
//
//	public  String getWebService_ChangePwdUri() {
//		return webService_ChangePwdUri;
//	}
//
//	public void setWebServiceUri(String webServiceUri) {
//		this.webServiceUri = webServiceUri;
//	}
//
//	public void setWebService_ChangePwdUri(String webServiceUri) {
//		this.webService_ChangePwdUri = webServiceUri;
//	}




	private <T> T createService(String serviceUrl, Class<T> clz) {
		JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
		factory.getInInterceptors().add(new LoggingInInterceptor());
		factory.getOutInterceptors().add(new LoggingOutInterceptor());
		factory.setServiceClass(clz);

		factory.setAddress(serviceUrl);

		T service = (T) factory.create();
		Client client = ClientProxy.getClient(service);

		HTTPConduit conduit = (HTTPConduit) client.getConduit();
		HTTPClientPolicy policy = conduit.getClient();
		policy.setReceiveTimeout(webService_ReceiveTimeout * 1000);

		conduit.setClient(policy);

		return service;
	}
	/**
	 * GOAL:取得帳單HTML的相關資訊
	 * 
	 * Bill_no：帳單號碼(為查詢時，DS所回傳的Bill_no)
	 * Cust_ID：身份證ID或統編
	 * Query_Date：billhunter帳務日期(為查詢時，DS所回傳的Query_Date) ex:2008/10/7
	 * User_ID：客戶流水號(為查詢時，DS所回傳的userid)
	 * Project_No：billhunter的專案代號(為查詢時，DS所回傳的Project_No)
	 * 註：因新增此功能，Get_Result的回傳值多了兩個欄位->Query_Date、Project_No
	 */
	public String getBHWSHtml(String bill_no, String cust_id, int project_no, int user_id, String query_dt){

		String html = "";
		boolean isSuccess = true;

//		old code
//		BHWebServiceClient service = new BHWebServiceClient();
//		BHWebServiceSoap client = service.getBHWebServiceSoap(webServiceUri);
		BHWebServiceSoap client = createService(webServiceUri, BHWebServiceSoap.class);

		//old code
//		GetHtmlDocument req = GetHtmlDocument.Factory.newInstance();
//
//		req.addNewGetHtml();
//		req.getGetHtml().setBillNo(bill_no);
//		req.getGetHtml().setCustID(cust_id);
//		req.getGetHtml().setProjectNo(project_no);
//		req.getGetHtml().setUserID(user_id);
//		req.getGetHtml().setQueryDate(query_dt);
// old code
//		GetHtmlResponseDocument res = client.get_Html(req);
//		isSuccess = res.getGetHtmlResponse().getGetHtmlResult();
//
//		if (isSuccess) {
//			html = res.getGetHtmlResponse().getHtmlBody();
//		}

		Holder<Boolean> a = new Holder<Boolean>();
		Holder<String> b = new Holder<String>();
		client.getHtml(bill_no, cust_id, query_dt, user_id, project_no, a, b);

		html = b.value;
		
		logger.debug("@@@ BHWebServiceTemplate.getBHWSHtml() HTML is:" + html);
		
		return html;
	}

	/**
	 * GOAL:取得對帳單圖檔
	 * 
	 * Bill_no：帳單號碼(為查詢時，DS所回傳的Bill_no)
	 * Cust_ID：身份證ID或統編
	 * Query_Date：billhunter帳務日期(為查詢時，DS所回傳的Query_Date) ex:2008/10/7
	 * User_ID：客戶流水號(為查詢時，DS所回傳的userid)
	 * Project_No：billhunter的專案代號(為查詢時，DS所回傳的Project_No)
	 * 註：因新增此功能，Get_Result的回傳值多了兩個欄位->Query_Date、Project_No
	 */
	public byte[] getBHWSHtmlImg(String bill_no, String cust_id, int project_no, int user_id, String query_dt){

		byte[] retAry = null;

		BHWebServiceSoap client = createService(webServiceUri, BHWebServiceSoap.class);
		//old code
//		BHWebServiceClient service = new BHWebServiceClient();
//		BHWebServiceSoap client = service.getBHWebServiceSoap(webServiceUri);
//
//		GetHtmlImgDocument req = GetHtmlImgDocument.Factory.newInstance();
//		req.addNewGetHtmlImg();
//		req.getGetHtmlImg().setBillNo(bill_no);
//		req.getGetHtmlImg().setCustID(cust_id);
//		req.getGetHtmlImg().setProjectNo(project_no);
//		req.getGetHtmlImg().setUserID(user_id);
//		req.getGetHtmlImg().setQueryDate(query_dt);
//		GetHtmlImgResponseDocument res = client.get_Html_img(req);
//		boolean isSuccess = res.getGetHtmlImgResponse().getGetHtmlImgResult();
//
//		if (isSuccess){
//			retAry = res.getGetHtmlImgResponse().getGIFBtyeArray1();
//		}

		Holder<Boolean> a = new Holder<Boolean>();
		Holder<byte[]> b = new Holder<byte[]>();
		client.getHtmlImg(bill_no, cust_id, query_dt, user_id, project_no, a, b);
		retAry = b.value;

		return retAry;
	}

	/**
	 * GOAL:重發mail
	 * 
	 * 1.單筆資料補寄請傳入：result_no,bill_month,email  (以逗點分隔)
	 * 2.多筆資料補寄請轉入：result_no,bill_month,email|result_no,bill_month,email|result_no,bill_month,email (以|分隔)
	 * 註1：result_no為查詢時，DS所回傳的result_no
	 * 註2：bill_month為查詢時，DS所回傳的bill_month
	 * 註3：補寄帶入的email值，若要按照email log 寄送，就帶入查詢時，DS所回傳的email return值。若要寄到客戶的最新email，請帶入新的emai帳號
	 */
	public boolean sendBHWSReissue(BHWebServiceObj[] obj){

		if(obj == null){
			return false;
		}
		StringBuffer sb = new StringBuffer("");

		int standardlen = obj.length - 1;

		for (int i =0; i< obj.length; i++){
			if(obj[i].getResult_no() == null || "".equals(obj[i].getResult_no())){
				return false;
			}
			if(obj[i].getBill_Month() == null || "".equals(obj[i].getBill_Month())){
				return false;
			}
			if(obj[i].getEmail() == null || "".equals(obj[i].getEmail())){
				return false;
			}
			sb.append(obj[i].getResult_no()).append(",").append(obj[i].getBill_Month()).append(",").append(obj[i].getEmail());
			if(i != standardlen)
				sb.append("|");
		}

		BHWebServiceSoap client = createService(webServiceUri, BHWebServiceSoap.class);
		// old code
//		BHWebServiceClient service = new BHWebServiceClient();
//		BHWebServiceSoap client = service.getBHWebServiceSoap(webServiceUri);

//		SendReissueDocument req = SendReissueDocument.Factory.newInstance();
//		req.addNewSendReissue();
//
//		req.getSendReissue().setInputstring(sb.toString());
//		SendReissueResponseDocument res = client.send_Reissue(req);
//
//		boolean isSuccess = res.getSendReissueResponse().getSendReissueResult();

		boolean isSuccess = client.sendReissue(sb.toString());
		return isSuccess;
	}

	/**
	 * GOAL:取得帳單資訊
	 * 
	 * Bill_No	帳單編號 
	 * 一般卡=1, 商務卡=3, VISA金融卡=5
	 * 查詢所有帳單種類時，傳入格式為：「空字串」。查詢單一帳單種類時，傳入格式為：「bill_no」。查詢多個帳單種類時，傳入格式為：「bill_no,bill_no,bill_no,bill_no」
	 * 
	 * Cust_ID	身份證ID或統編	必填查詢條件，不得為空值
	 * SDate	專案寄送起日	可以使用空字串，便會查詢所有日期。(傳入格式為：「2008/1/1」或「」空字串)
	 * EDate	專案寄送迄日	可以使用空字串，便會查詢所有日期。(傳入格式為：「2008/1/1」或「」空字串)
	 */
	public BHWebServiceObj[] getBHWSResult(String bill_no, String cust_id, String start_dt, String end_dt){

		BHWebServiceSoap client = createService(webServiceUri, BHWebServiceSoap.class);
//old code
//		BHWebServiceClient service = new BHWebServiceClient();
//		BHWebServiceSoap client = service.getBHWebServiceSoap(webServiceUri);
//
//		GetResultDocument req = GetResultDocument.Factory.newInstance();
//		req.addNewGetResult();
//
//		req.getGetResult().setBillNo(bill_no);
//		req.getGetResult().setCustID(cust_id);
//		req.getGetResult().setSDate(start_dt);
//		req.getGetResult().setEDate(end_dt);
//		GetResultResponseDocument res = client.get_Result(req);
//
//		boolean isSuccess = res.getGetResultResponse().getGetResultResult();

		Holder<Boolean> a = new Holder<Boolean>();
		Holder<fstop.ws.bh.client.GetResultResponse.DS> b = new Holder<fstop.ws.bh.client.GetResultResponse.DS>();
		client.getResult(bill_no, cust_id, start_dt, end_dt, a, b);

		boolean isSuccess = a.value;
		if (isSuccess){
			Vector v = new Vector();
//			DS ds = res.getGetResultResponse().getDS(); old code
			GetResultResponse.DS ds = b.value;
			org.w3c.dom.Element elm = (org.w3c.dom.Element)ds.getAny();
//			old code
//			NodeList nodeList = ds.getDomNode().getChildNodes().item(1).getChildNodes().item(0).getChildNodes();
			NodeList nodeList = elm
					.getChildNodes()
//					.item(1)
//					.getChildNodes()
					.item(0)
					.getChildNodes();

			for(int i = 0; i < nodeList.getLength(); i++) {
				Node n = nodeList.item(i);
				BHWebServiceObj obj = new BHWebServiceObj();

				for(int j = 0; j < n.getChildNodes().getLength(); j++) {
					Node vp = n.getChildNodes().item(j);
					if("Result_no".equalsIgnoreCase(vp.getNodeName()))
						obj.setResult_no(vp.getChildNodes().getLength() == 0 ? "" : vp.getChildNodes().item(0).getNodeValue());

					if("Subject".equalsIgnoreCase(vp.getNodeName()))
						obj.setSubject(vp.getChildNodes().getLength() == 0 ? "" : vp.getChildNodes().item(0).getNodeValue());

					if("Bill_no".equalsIgnoreCase(vp.getNodeName()))
						obj.setBill_no(vp.getChildNodes().getLength() == 0 ? "" : vp.getChildNodes().item(0).getNodeValue());

					if("Bill_type".equalsIgnoreCase(vp.getNodeName()))
						obj.setBill_type(vp.getChildNodes().getLength() == 0 ? "" : vp.getChildNodes().item(0).getNodeValue());

					if("Send_date".equalsIgnoreCase(vp.getNodeName()))
						obj.setSend_date(vp.getChildNodes().getLength() == 0 ? "" : vp.getChildNodes().item(0).getNodeValue());

					if("Bill_Month".equalsIgnoreCase(vp.getNodeName()))
						obj.setBill_Month(vp.getChildNodes().getLength() == 0 ? "" : vp.getChildNodes().item(0).getNodeValue());

					if("Cust_Name".equalsIgnoreCase(vp.getNodeName()))
						obj.setCust_Name(vp.getChildNodes().getLength() == 0 ? "" : vp.getChildNodes().item(0).getNodeValue());

					if("email".equalsIgnoreCase(vp.getNodeName()))
						obj.setEmail(vp.getChildNodes().getLength() == 0 ? "" : vp.getChildNodes().item(0).getNodeValue());

					if("userid".equalsIgnoreCase(vp.getNodeName()))
						obj.setUserid(vp.getChildNodes().getLength() == 0 ? "" : vp.getChildNodes().item(0).getNodeValue());

					if("Mail_status".equalsIgnoreCase(vp.getNodeName()))
						obj.setMail_status(vp.getChildNodes().getLength() == 0 ? "" : vp.getChildNodes().item(0).getNodeValue());

					if("Project_No".equalsIgnoreCase(vp.getNodeName()))
						obj.setProject_No(vp.getChildNodes().getLength() == 0 ? "" : vp.getChildNodes().item(0).getNodeValue());

					if("Query_Date".equalsIgnoreCase(vp.getNodeName()))
						obj.setQuery_Date(vp.getChildNodes().getLength() == 0 ? "" : vp.getChildNodes().item(0).getNodeValue());	
					
					if("CARD".equalsIgnoreCase(vp.getNodeName()))
						obj.setCARD(vp.getChildNodes().getLength() == 0 ? "" : vp.getChildNodes().item(0).getNodeValue());
					
					if("Branch".equalsIgnoreCase(vp.getNodeName()))
						obj.setBranch(vp.getChildNodes().getLength() == 0 ? "" : vp.getChildNodes().item(0).getNodeValue());
				}
				if(!"9".equals(obj.getBill_no())){
					v.addElement(obj);
				}
			}

			if (v.size() > 0) {
				BHWebServiceObj[] ret = new BHWebServiceObj[v.size()];
				v.copyInto(ret);
				return ret;
			}
		}
		return null;
	}
	
	/**
	 * GOAL: 重置電子帳單密碼
	 * 
	 * 傳入參數如下：
	 * (1) Cust_id：身份證字號(必填)
	 * (2) Status：異動狀態(A：申請, R：重置, U：變更) (必填)
	 * (3) Oldpwd：舊密碼 (請輸入SHA1加密過的小寫字串)
	 * (4) newpwd：新密碼 (請輸入SHA1加密過的小寫字串)
	 * (5) email：web mail信箱(必填)
	 * (6) U_dt：異動時間(必填)
	 * (7) sys_ip：系統來源別(必填)
	 * 
	 * 回傳代碼如下：
	 * 00	呼叫成功，後續由通知信件處理
	 * 99	連線錯誤
	 * 01	來源IP不符安全性
	 * 02	舊密碼錯誤
	 * 03	查無客戶資料
	 * 04	參數錯誤
	 * 05	密碼資料有誤
	 * 
	 */
	public String changeBillPwd(String cust_id, String status, String oldpwd, String newpwd, 
			                    String email, String updateTime, String sys_ip)
	{
		logger.info("@@@ changeBillPwd() webService_ChangePwdUri == " + this.webService_ChangePwdUri);
		EasyuseWebChangepwdSoap client = createService(webService_ChangePwdUri, EasyuseWebChangepwdSoap.class);
//		ServiceClient service = new ServiceClient();
//		ServiceSoap client = service.getServiceSoap(this.webService_ChangePwdUri);
		String result = "";
		
		try {
//			old code
//			result = client.bank_check(cust_id, status, oldpwd, newpwd, email, updateTime, sys_ip);
			result = client.bankCheck(cust_id, status, oldpwd, newpwd, email, updateTime, sys_ip);
			logger.info("@@@ changeBillPwd request" );
			logger.info(ESAPIUtil.vaildLog("@@@ cus_id >>"+ cust_id));
			logger.info(ESAPIUtil.vaildLog("@@@ status >>"+ status));
			logger.info(ESAPIUtil.vaildLog("@@@ email >>"+ email));
			logger.info("@@@ updateTime >>"+ updateTime );
			logger.info("@@@ sys_ip >>"+ sys_ip  );
			logger.info("@@@ changeBillPwd() BankCheckResponse.getBankCheckResult() == " + result); 						
		}
		catch (Exception e)
		{
			logger.info("@@@ Exception in changeBillPwd() :" + e); 			
		}

		return result;
	}	

}
