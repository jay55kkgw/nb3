package fstop.ws;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.rmi.RemoteException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Value;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import fstop.exception.TopMessageException;
import fstop.util.StrUtils;
import fstop.ws.bean.BHIvrWebServiceObj;
import fstop.ws.bean.BHIvrWebServiceOut1;
import fstop.ws.bean.BHIvrWebServiceOut2;
import fstop.ws.bean.BHIvrWebServiceOut3;
import fstop.ws.bean.BHIvrWebServiceOut4;
import fstop.ws.bean.BHIvrWebServiceOut5;
import fstop.ws.bean.BHIvrWebServiceOut6;
import fstop.ws.bean.BHIvrWebServiceOut7;
import fstop.ws.bh.client.BHWebService;
import fstop.ws.bh.client.BHWebServiceSoap;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BHIVRWebServiceTemplate {

	@Value("${WebService.URI}")
	private String webServiceUri;
	@Value("${WebService.ReceiveTimeout}")
	private Integer webService_ReceiveTimeout;
	
	private <T> T createService(String serviceUrl, Class<T> clz) {
		JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
		factory.getInInterceptors().add(new LoggingInInterceptor());
		factory.getOutInterceptors().add(new LoggingOutInterceptor());
		factory.setServiceClass(clz);

		factory.setAddress(serviceUrl);

		T service = (T) factory.create();
		Client client = ClientProxy.getClient(service);

		HTTPConduit conduit = (HTTPConduit) client.getConduit();
		HTTPClientPolicy policy = conduit.getClient();
		policy.setReceiveTimeout(webService_ReceiveTimeout * 1000);

		conduit.setClient(policy);

		return service;
	}

	/**
	 * 取得電話服務機最近期信用卡帳單資訊
	 * rid : 識別碼(20121004155000)
	 * rcustid : 身分證ID(A123456789)
	 */
	public BHIvrWebServiceObj getBHIVRWSResult(String rid, String rcustid){
		BHIvrWebServiceObj bhivrob=new BHIvrWebServiceObj();
		try {
			BHWebServiceSoap client = createService(webServiceUri, BHWebServiceSoap.class);
			String Resp=client.ivrGETBILL(rid, rcustid);
			log.debug("before　Resp = " + Resp);
			Resp=Resp.replace("&", "＆");
			log.debug("after　Resp = " + Resp);
			Document doc=StringXmlToNodeList(Resp);
			bhivrob.setRid(getNodeValue(doc,"rid"));
			log.debug("getrid = " + bhivrob.getRid());
			bhivrob.setRcustid(getNodeValue(doc,"rcustid"));
			bhivrob.setBillresult(getNodeValue(doc,"billresult"));	
			
			if(!StrUtils.trim(getNodeValue(doc,"billresult")).equals("Y"))
			{				
				bhivrob.setBillmonth("");			
				bhivrob.setCustname("");			
				bhivrob.setAddress("");
				return bhivrob;
			}else{
				
				bhivrob.setBillmonth(getNodeValue(doc,"billmonth"));			
				bhivrob.setCustname(getNodeValue(doc,"custname"));			
				bhivrob.setAddress(getNodeValue(doc,"address"));
			}

			NodeList nodelist=doc.getElementsByTagName("r12_c1");
			log.debug("nodelist r12_c1 = " + nodelist);
			if(nodelist.getLength()>0)
			{
				BHIvrWebServiceOut1[] bhivrtb1=new BHIvrWebServiceOut1[nodelist.getLength()];
				for(int i=0;i<nodelist.getLength();i++)
				{
					bhivrtb1[i]=new BHIvrWebServiceOut1();
					bhivrtb1[i].setR12_c1(getNodeValueOcc(doc,"r12_c1",i));
					bhivrtb1[i].setR12_c2(getNodeValueOcc(doc,"r12_c2",i));
					bhivrtb1[i].setR12_c3(getNodeValueOcc(doc,"r12_c3",i));
					bhivrtb1[i].setR12_c4(getNodeValueOcc(doc,"r12_c4",i));
					bhivrtb1[i].setR12_c5(getNodeValueOcc(doc,"r12_c5",i));
					bhivrtb1[i].setR12_c6(getNodeValueOcc(doc,"r12_c6",i));
					bhivrtb1[i].setR12_c7(getNodeValueOcc(doc,"r12_c7",i));
				}
				bhivrob.setBhivrout1(bhivrtb1);
			}

			nodelist=doc.getElementsByTagName("rdetail_201_r_c1");
			log.debug("nodelist rdetail_201_r_c1 = " + nodelist);
			if(nodelist.getLength()>0)
			{
				BHIvrWebServiceOut2[] bhivrtb2=new BHIvrWebServiceOut2[nodelist.getLength()];
				for(int i=0;i<nodelist.getLength();i++)
				{
					bhivrtb2[i]=new BHIvrWebServiceOut2();
					bhivrtb2[i].setRdetail_201_r_c1(getNodeValueOcc(doc,"rdetail_201_r_c1",i));
					bhivrtb2[i].setRdetail_201_r_c2(getNodeValueOcc(doc,"rdetail_201_r_c2",i));
					bhivrtb2[i].setRdetail_201_r_c3(getNodeValueOcc(doc,"rdetail_201_r_c3",i));
				}
				bhivrob.setBhivrout2(bhivrtb2);
			}

			nodelist=doc.getElementsByTagName("rdetail_202_r_c1");
			log.debug("nodelist rdetail_202_r_c1 = " + nodelist);
			if(nodelist.getLength()>0)
			{
				BHIvrWebServiceOut3[] bhivrtb3=new BHIvrWebServiceOut3[nodelist.getLength()];
				for(int i=0;i<nodelist.getLength();i++)
				{
					bhivrtb3[i]=new BHIvrWebServiceOut3();
					bhivrtb3[i].setRdetail_202_r_c1(getNodeValueOcc(doc,"rdetail_202_r_c1",i));
					bhivrtb3[i].setRdetail_202_r_c2(getNodeValueOcc(doc,"rdetail_202_r_c2",i));
					bhivrtb3[i].setRdetail_202_r_c3(getNodeValueOcc(doc,"rdetail_202_r_c3",i));
				}
				bhivrob.setBhivrout3(bhivrtb3);
			}
			
			nodelist=doc.getElementsByTagName("rmsg_r");
			log.debug("nodelist rmsg_r = " + nodelist);
			if(nodelist.getLength()>0)
			{
				BHIvrWebServiceOut4[] bhivrtb4=new BHIvrWebServiceOut4[nodelist.getLength()];
				for(int i=0;i<nodelist.getLength();i++)
				{
					bhivrtb4[i]=new BHIvrWebServiceOut4();
					bhivrtb4[i].setRmsg_r(StrUtils.trim(getNodeValueOcc(doc,"rmsg_r",i)));
				}
				bhivrob.setBhivrout4(bhivrtb4);
			}
			
			nodelist=doc.getElementsByTagName("rdetail_24_r_c1");
			log.debug("nodelist rdetail_24_r_c1 = " + nodelist);
			BHIvrWebServiceOut5[] bhivrtb5=new BHIvrWebServiceOut5[nodelist.getLength()];
			if(nodelist.getLength()>0)
			{
				for(int i=0;i<nodelist.getLength();i++)
				{
					bhivrtb5[i]=new BHIvrWebServiceOut5();
					bhivrtb5[i].setRdetail_24_r_c1(getNodeValueOcc(doc,"rdetail_24_r_c1",i));
					bhivrtb5[i].setRdetail_24_r_c2(getNodeValueOcc(doc,"rdetail_24_r_c2",i));
					bhivrtb5[i].setRdetail_24_r_c3(getNodeValueOcc(doc,"rdetail_24_r_c3",i));
					bhivrtb5[i].setRdetail_24_r_c4(getNodeValueOcc(doc,"rdetail_24_r_c4",i));
					bhivrtb5[i].setRdetail_24_r_c5(getNodeValueOcc(doc,"rdetail_24_r_c5",i));
				}
				bhivrob.setBhivrout5(bhivrtb5);
			}
			
			nodelist=doc.getElementsByTagName("rdetail_20_r1");
			log.debug("nodelist rdetail_20_r1 = " + nodelist);
			BHIvrWebServiceOut6[] bhivrtb6=new BHIvrWebServiceOut6[nodelist.getLength()];
			if(nodelist.getLength()>0)
			{
				for(int i=0;i<nodelist.getLength();i++)
				{
					bhivrtb6[i]=new BHIvrWebServiceOut6();
					bhivrtb6[i].setRdetail_20_r1(getNodeValueOcc(doc,"rdetail_20_r1",i));
					bhivrtb6[i].setDetail_count(this.getDetailCount(doc,i));//每張卡別明細筆數	
				}
				bhivrob.setBhivrout6(bhivrtb6);
			}

			nodelist=doc.getElementsByTagName("rdetail_20_r2_c1");
			log.debug("nodelist rdetail_20_r2_c1 = " + nodelist);
			BHIvrWebServiceOut7[] bhivrtb7=new BHIvrWebServiceOut7[nodelist.getLength()];
			if(nodelist.getLength()>0)
			{
				for(int i=0;i<nodelist.getLength();i++)
				{
					bhivrtb7[i]=new BHIvrWebServiceOut7();
					bhivrtb7[i].setRdetail_20_r2_c1(getNodeValueOcc(doc,"rdetail_20_r2_c1",i));
					bhivrtb7[i].setRdetail_20_r2_c2(getNodeValueOcc(doc,"rdetail_20_r2_c2",i));
					bhivrtb7[i].setRdetail_20_r2_c3(getNodeValueOcc(doc,"rdetail_20_r2_c3",i));
					bhivrtb7[i].setRdetail_20_r2_c4(getNodeValueOcc(doc,"rdetail_20_r2_c4",i));
					bhivrtb7[i].setRdetail_20_r2_c5(getNodeValueOcc(doc,"rdetail_20_r2_c5",i));
					bhivrtb7[i].setRdetail_20_r2_c6(getNodeValueOcc(doc,"rdetail_20_r2_c6",i));
					bhivrtb7[i].setRdetail_20_r2_c7(getNodeValueOcc(doc,"rdetail_20_r2_c7",i));
					bhivrtb7[i].setRdetail_20_r2_c8(getNodeValueOcc(doc,"rdetail_20_r2_c8",i));					
				}
				bhivrob.setBhivrout7(bhivrtb7);
			}
			
			return bhivrob;
		} catch (ServiceException e) {
			// TODO 自動產生 catch 區塊
			e.printStackTrace();
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}

	public Document StringXmlToNodeList(String StringXml)
	{
		Document document=null;
		NodeList nodelist=null;
		try {
			Reader rr=new StringReader(StringXml); 
			DocumentBuilderFactory builderFactory=DocumentBuilderFactory.newInstance(); 
			DocumentBuilder domBuilder;
			domBuilder = builderFactory.newDocumentBuilder();
			document=domBuilder.parse(new InputSource(rr));
		} catch (ParserConfigurationException e) {
			// TODO 自動產生 catch 區塊
			e.printStackTrace();
			throw(TopMessageException.create("Z071",e.toString()));
		} catch (SAXException e) {
			// TODO 自動產生 catch 區塊
			e.printStackTrace();
			throw(TopMessageException.create("Z071",e.toString()));
		} catch (IOException e) {
			// TODO 自動產生 catch 區塊
			e.printStackTrace();
			throw(TopMessageException.create("Z071",e.toString()));
		}


		return(document);
	}
	
	private String getNodeValue(Document doc,String Tag)
	{
		String retvalue="";
		NodeList nodelist=doc.getElementsByTagName(Tag);
		System.out.println("NodeName = " + nodelist.item(0).getNodeName());
		System.out.println("NodeValue = " + nodelist.item(0).getChildNodes().item(0).getNodeValue());
		return(nodelist.item(0).getChildNodes().item(0).getNodeValue());
	}

	private String getNodeValueOcc(Document doc,String Tag,int index)
	{
		String retvalue="";
		NodeList nodelist=doc.getElementsByTagName(Tag);
		if(nodelist.item(index).hasChildNodes() == true){
			System.out.println("NodeName = " + nodelist.item(index).getNodeName());
			System.out.println("NodeValue = " + nodelist.item(index).getChildNodes().item(0).getNodeValue());
			return(nodelist.item(index).getChildNodes().item(0).getNodeValue());
		}else{
			
			return "";
		}
	}
	
	private int getDetailCount(Document doc, int index)
	{
		String retvalue="";
		NodeList nodelist=doc.getElementsByTagName("rdetail_20");
		Node node = nodelist.item(index);			
		int childNodesCount = node.getChildNodes().getLength();
		int result = childNodesCount - 2;	//扣除rdetail_20_r1及rdetail_20_r3即是rdetail_20_r2的筆數				
		
		return result; 
	}
}
