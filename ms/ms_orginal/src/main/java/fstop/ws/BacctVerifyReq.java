package fstop.ws;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="CreditVerifyReq", namespace = "http://www.focas.fisc.com.tw/FiscII/acctVerify")
public class BacctVerifyReq {
	
	private String mti;				// 交易類型
	private String cardNumber;		// 卡號
	private String processingCode;	// 處理代碼
	private String amt;				// 交易金額
	private String systemDateTime;	// 系統日期時間
	private String traceNumber;		// 端末交易序號
	private String localTime;		// 交易時間(local)
	private String localDate;		// 交易日期(local)
	private String expiredDate;		// 卡片效期
	private String posEntryMode;	// 端末設備型態
	private String acqBank;			// 收單行代碼
	private String srrn;			// 系統追蹤號
	private String authCode;		// 授權碼
	private String responseCode;	// 回應碼
	private String terminalId;		// 端末代號
	private String merchantId;		// 特店代號
	private String otherInfo;		// 補充資訊
	private String verifyCode;		// 驗證碼
	
	public String getMti() {
		return mti;
	}
	public void setMti(String mti) {
		this.mti = mti;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getProcessingCode() {
		return processingCode;
	}
	public void setProcessingCode(String processingCode) {
		this.processingCode = processingCode;
	}
	public String getAmt() {
		return amt;
	}
	public void setAmt(String amt) {
		this.amt = amt;
	}
	public String getSystemDateTime() {
		return systemDateTime;
	}
	public void setSystemDateTime(String systemDateTime) {
		this.systemDateTime = systemDateTime;
	}
	public String getTraceNumber() {
		return traceNumber;
	}
	public void setTraceNumber(String traceNumber) {
		this.traceNumber = traceNumber;
	}
	public String getLocalTime() {
		return localTime;
	}
	public void setLocalTime(String localTime) {
		this.localTime = localTime;
	}
	public String getLocalDate() {
		return localDate;
	}
	public void setLocalDate(String localDate) {
		this.localDate = localDate;
	}
	public String getExpiredDate() {
		return expiredDate;
	}
	public void setExpiredDate(String expiredDate) {
		this.expiredDate = expiredDate;
	}
	public String getPosEntryMode() {
		return posEntryMode;
	}
	public void setPosEntryMode(String posEntryMode) {
		this.posEntryMode = posEntryMode;
	}	
	public String getAcqBank() {
		return acqBank;
	}
	public void setAcqBank(String acqBank) {
		this.acqBank = acqBank;
	}	
	public String getSrrn() {
		return srrn;
	}
	public void setSrrn(String srrn) {
		this.srrn = srrn;
	}
	public String getAuthCode() {
		return authCode;
	}
	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}		
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	public String getTerminalId() {
		return terminalId;
	}
	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}	
	public String getOtherInfo() {
		return otherInfo;
	}
	public void setOtherInfo(String otherInfo) {
		this.otherInfo = otherInfo;
	}					
	public String getVerifyCode() {
		return verifyCode;
	}
	public void setVerifyCode(String verifyCode) {
		this.verifyCode = verifyCode;
	}
}
