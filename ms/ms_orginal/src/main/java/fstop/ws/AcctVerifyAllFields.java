package fstop.ws;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AcctVerifyAllFields {
//	private static Logger logger = Logger.getLogger(AcctVerifyAllFields.class);
	

	// 以下的資料由 "網路收單共用系統特店API 2.0--交易電文規格書.word"產生
	private static String allFiledsDescStr = "" 
			+ "0	交易類型	mti	N	4\r\n" 
			+ "2	卡(帳)號	cardNumber	N	1…19\r\n"
			+ "3	處理代碼	processingCode	N	6\r\n" 
			+ "4		交易金額	amt	N	12\r\n"
			+ "7		系統日期時間 systemDateTime	N	10\r\n" 
			+ "11		端末交易序號   traceNumber	N	6\r\n"
			+ "12		交易時間(local)	localTime	N	6\r\n" 
			+ "13		交易日期(local)	localDate	N	8\r\n"
			//+ "19		國別碼	 countryCode	N	3\r\n" 
			//+ "22		端末設備型態	 posEntryMode	N	3\r\n"
			//+ "25		設備能力代碼 posConditionCode	N	2\r\n" 
			+ "32		收單行代碼	acqBank	N	3\r\n"
			+ "37		系統追蹤號	srrn	N	12\r\n" 
			+ "39		回應碼	responseCode	AN	4\r\n"
			+ "41		端末代號	terminalId	AN	8\r\n" 
			+ "42		特店代號	merchantId	AN	15\r\n"
			//+ "43                 特店名稱	merchantName	ANS	1…60\r\n"
			//+ "47                 訂單編號	orderNumber	ANS	1…19\r\n" 
			+ "48		附加資料 otherInfo	ANS	1…999\r\n"
			//+ "49		幣別碼 txnCurrencyCode	N	3\r\n" 
			//+ "55		晶片資料 chipData	ANS	1…512\r\n"
			+ "64		驗證碼 verifyCode	H	64\r\n" 
			//+ "90		原始交易資訊 orgTxnData	ANS	1…999\r\n"
			//+ "129		支付工具類型  paymentType	N	2\r\n" 
			//+ "130		QR CODE 安全碼	secureCode	ANS	12\r\n"
			//+ "131		APP ID	appId	AN	1…36\r\n" 
			//+ "132		Server ID	hostId	AN	23\r\n"
			//+ "140		密文/明文資料	secureData	ANS	1…999"
			;

	public static Map<String, FieldDesc> fieldMap = new LinkedHashMap();
	public static Map<String, FieldDesc> fieldNumMap = new LinkedHashMap();

	static {
		allFiledsDescStr = allFiledsDescStr.replaceAll("\t", " ");
		log.info(allFiledsDescStr);
		log.info("----------------------------");
		try {
			Pattern regex = Pattern.compile("^(\\d+)\\s(.+?)\\s([A-Z0-9-_ ]+?)\\s([ANSHB]+?)\\s(:?(\\d+[…]\\d+)|(\\d+))",
					Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE | Pattern.MULTILINE);
			Matcher regexMatcher = regex.matcher(allFiledsDescStr);
			while (regexMatcher.find()) {
				FieldDesc desc = new FieldDesc();
				desc.setNo(regexMatcher.group(1));
				desc.setName(StrUtils.trim(regexMatcher.group(2)));
				desc.setField(StrUtils.trim(regexMatcher.group(3)));
				desc.setType(regexMatcher.group(4));
				
				String m = regexMatcher.group(5);
				boolean isRange = m.indexOf("…") > 0;
				if(isRange) {
					String[] cc = m.replaceAll("…", " ").split(" ");
					int min = Integer.valueOf(cc[0]);
					int max = Integer.valueOf(cc[1]);
					desc.setLen(max);
					desc.setMin(min);
					desc.setMax(max);
					
				}
				else {
					Integer iLen = Integer.valueOf(m);
					desc.setLen(iLen);
					desc.setMax(iLen);
					desc.setMin(iLen);
				}
				
				fieldMap.put(desc.getField(), desc);
				fieldNumMap.put(desc.getNo(), desc);
			}
		} catch (Exception ex) {
			throw new IllegalArgumentException("無法解析 allFiledsDescStr.", ex);
		}

	}

}
