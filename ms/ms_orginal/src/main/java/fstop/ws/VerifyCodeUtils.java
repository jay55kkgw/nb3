package fstop.ws;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import enc.Enc256Util;
import fstop.core.BeanUtils;
import fstop.util.JSONUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class VerifyCodeUtils {
//	static Logger logger = Logger.getLogger(VerifyCodeUtils.class);
	public static String genCode(Object o, String validParam) throws Exception {
		
		log.info("genCode(Object o, String validParam) 驗證參數: " + validParam);
		
		Map<String, String> objMap = BeanUtils.describe(o);
		objMap.remove("verifyCode");
		objMap.remove("class");

		log.info("obj : \n" + JSONUtils.toJson(objMap));

		return genCode(objMap,validParam);
	}
	
	//
	public static String genCode(Map objMap) throws Exception {
//		logger.info("驗證參數: " + ValidParam);
		List<String> keys = new ArrayList(objMap.keySet());
		Collections.sort(keys);

		StringBuilder sb = new StringBuilder();
		for(String k : keys) {
			sb.append(StrUtils.trim((String)objMap.get(k)));
		}



		return Enc256Util.encrypt(sb.toString() + "5E8B6E1998F421204C6576544FE1A26B44FC775982D8CE2E", "UTF-8");
	}

	public static String genCode(Map objMap, String validParam) throws Exception {
		
		log.info("genCode(Map objMap, String validParam) 驗證參數: " + validParam);
		
		List<String> keys = new ArrayList(objMap.keySet());
		Collections.sort(keys);

		StringBuilder sb = new StringBuilder();
		for(String k : keys) {
			sb.append(StrUtils.trim((String)objMap.get(k)));
		}

		return Enc256Util.encrypt(sb.toString() + validParam, "UTF-8");
	}
}
