
package fstop.ws.bh.client;

import javax.xml.ws.Endpoint;

import lombok.extern.slf4j.Slf4j;

/**
 * This class was generated by Apache CXF 3.1.17
 * 2019-03-18T17:35:15.725+08:00
 * Generated source version: 3.1.17
 * 
 */
@Slf4j
public class BHWebServiceSoap_BHWebServiceSoap12_Server{

    protected BHWebServiceSoap_BHWebServiceSoap12_Server() throws Exception {
    	log.debug("Starting Server");
        Object implementor = new BHWebServiceSoap12Impl();
        String address = "http://localhost/billhunter_app/bhwebservice.asmx";
        Endpoint.publish(address, implementor);
    }
    
//    public static void main(String args[]) throws Exception {
//        new BHWebServiceSoap_BHWebServiceSoap12_Server();
//        log.debug("Server ready..."); 
//        
//        Thread.sleep(5 * 60 * 1000); 
//        log.debug("Server exiting");
//        System.exit(0);
//    }
}
