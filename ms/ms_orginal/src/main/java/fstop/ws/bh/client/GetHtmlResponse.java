
package fstop.ws.bh.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type 的 Java 類別.
 * 
 * <p>下列綱要片段會指定此類別中包含的預期內容.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Get_HtmlResult" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="HtmlBody" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getHtmlResult",
    "htmlBody"
})
@XmlRootElement(name = "Get_HtmlResponse")
public class GetHtmlResponse {

    @XmlElement(name = "Get_HtmlResult")
    protected boolean getHtmlResult;
    @XmlElement(name = "HtmlBody")
    protected String htmlBody;

    /**
     * 取得 getHtmlResult 特性的值.
     * 
     */
    public boolean isGetHtmlResult() {
        return getHtmlResult;
    }

    /**
     * 設定 getHtmlResult 特性的值.
     * 
     */
    public void setGetHtmlResult(boolean value) {
        this.getHtmlResult = value;
    }

    /**
     * 取得 htmlBody 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHtmlBody() {
        return htmlBody;
    }

    /**
     * 設定 htmlBody 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHtmlBody(String value) {
        this.htmlBody = value;
    }

}
