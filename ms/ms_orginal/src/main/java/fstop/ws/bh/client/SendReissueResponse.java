
package fstop.ws.bh.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type 的 Java 類別.
 * 
 * <p>下列綱要片段會指定此類別中包含的預期內容.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Send_ReissueResult" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sendReissueResult"
})
@XmlRootElement(name = "Send_ReissueResponse")
public class SendReissueResponse {

    @XmlElement(name = "Send_ReissueResult")
    protected boolean sendReissueResult;

    /**
     * 取得 sendReissueResult 特性的值.
     * 
     */
    public boolean isSendReissueResult() {
        return sendReissueResult;
    }

    /**
     * 設定 sendReissueResult 特性的值.
     * 
     */
    public void setSendReissueResult(boolean value) {
        this.sendReissueResult = value;
    }

}
