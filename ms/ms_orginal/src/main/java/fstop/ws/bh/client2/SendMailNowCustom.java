
package fstop.ws.bh.client2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;



/**
 * <p>anonymous complex type 的 Java 類別.
 * 
 * <p>下列綱要片段會指定此類別中包含的預期內容.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Mail_Tos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Template_No" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="Project_Class" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="Variables" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Cust_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SenderName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SenderEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Subject" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ReplyEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "mailTos",
    "templateNo",
    "projectClass",
    "priority",
    "variables",
    "custID",
    "senderName",
    "senderEmail",
    "subject",
    "replyEmail"
})
@XmlRootElement(name = "SendMail_Now_Custom")
public class SendMailNowCustom {

	
    @XmlElement(name = "Mail_Tos")
    protected String  mailTos;
    @XmlElement(name = "Template_No")
    protected int templateNo;
    @XmlElement(name = "Project_Class")
    protected int projectClass;
    @XmlElement(name = "Priority")
    protected int priority;
    @XmlElement(name = "Variables")
    protected String  variables;
    @XmlElement(name = "Cust_ID")
    protected String custID;
    @XmlElement(name = "SenderName")
    protected String senderName;
    @XmlElement(name = "SenderEmail")
    protected String senderEmail;
    @XmlElement(name = "Subject")
    protected String subject;
    @XmlElement(name = "ReplyEmail")
    protected String replyEmail;
  
   

	public String getMailTos() {
		return mailTos;
	}

	public String getVariables() {
		return variables;
	}

	public void setMailTos(String mailTos) {
		this.mailTos = mailTos;
	}

	public void setVariables(String variables) {
		this.variables = variables;
	}

	/**
     * 取得 templateNo 特性的值.
     * 
     */
    public int getTemplateNo() {
        return templateNo;
    }

    /**
     * 設定 templateNo 特性的值.
     * 
     */
    public void setTemplateNo(int value) {
        this.templateNo = value;
    }

    /**
     * 取得 projectClass 特性的值.
     * 
     */
    public int getProjectClass() {
        return projectClass;
    }

    /**
     * 設定 projectClass 特性的值.
     * 
     */
    public void setProjectClass(int value) {
        this.projectClass = value;
    }

    /**
     * 取得 priority 特性的值.
     * 
     */
    public int getPriority() {
        return priority;
    }

    /**
     * 設定 priority 特性的值.
     * 
     */
    public void setPriority(int value) {
        this.priority = value;
    }

    

	/**
     * 取得 custID 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustID() {
        return custID;
    }

    /**
     * 設定 custID 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustID(String value) {
        this.custID = value;
    }

    /**
     * 取得 senderName 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderName() {
        return senderName;
    }

    /**
     * 設定 senderName 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderName(String value) {
        this.senderName = value;
    }

    /**
     * 取得 senderEmail 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderEmail() {
        return senderEmail;
    }

    /**
     * 設定 senderEmail 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderEmail(String value) {
        this.senderEmail = value;
    }

    /**
     * 取得 subject 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubject() {
        return subject;
    }

    /**
     * 設定 subject 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubject(String value) {
        this.subject = value;
    }

    /**
     * 取得 replyEmail 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReplyEmail() {
        return replyEmail;
    }

    /**
     * 設定 replyEmail 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReplyEmail(String value) {
        this.replyEmail = value;
    }

}
