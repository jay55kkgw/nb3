package fstop.ws.bh.client;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import fstop.ws.ebm.bean.GetBillInfoResponse;
import fstop.ws.ebm.bean.GetBillInfoResponseData;

@WebService(targetNamespace = "EnterpriseServerBase.WebService.DynamicWebcalling.TBBWebService", name = "AIOWebServiceSoap")
@XmlSeeAlso({ObjectFactory.class})
public interface AIOWebServiceSoap {
	
    @WebMethod(operationName = "GetBillInfo", action = "EnterpriseServerBase.WebService.DynamicWebcalling.TBBWebService/GetBillInfo")
    @RequestWrapper(localName = "GetBillInfo", targetNamespace = "EnterpriseServerBase.WebService.DynamicWebcalling.TBBWebService", className = "fstop.ws.ebm.bean.GetBillInfo")
    @ResponseWrapper(localName = "GetBillInfoResponse", targetNamespace = "EnterpriseServerBase.WebService.DynamicWebcalling.TBBWebService", className = "fstop.ws.ebm.bean.GetBillInfoResponse")
    @WebResult(name = "GetBillInfoResult", targetNamespace = "EnterpriseServerBase.WebService.DynamicWebcalling.TBBWebService")
    public GetBillInfoResponseData GetBillInfo(
            @WebParam(name = "cUkey", targetNamespace = "EnterpriseServerBase.WebService.DynamicWebcalling.TBBWebService")
                    String cUkey
    );

    @WebMethod(operationName = "SetBillSend", action = "EnterpriseServerBase.WebService.DynamicWebcalling.TBBWebService/SetBillSend")
    @RequestWrapper(localName = "SetBillSend", targetNamespace = "EnterpriseServerBase.WebService.DynamicWebcalling.TBBWebService", className = "fstop.ws.ebm.bean.SetBillSend")
    @ResponseWrapper(localName = "SetBillSendResponse", targetNamespace = "EnterpriseServerBase.WebService.DynamicWebcalling.TBBWebService", className = "fstop.ws.ebm.bean.SetBillSendResponse")
    @WebResult(name = "SetBillSendResult", targetNamespace = "EnterpriseServerBase.WebService.DynamicWebcalling.TBBWebService")
    public GetBillInfoResponseData SetBillSend(
            @WebParam(name = "cUkey", targetNamespace = "EnterpriseServerBase.WebService.DynamicWebcalling.TBBWebService")
                    String cUkey,
            @WebParam(name = "cDate", targetNamespace = "EnterpriseServerBase.WebService.DynamicWebcalling.TBBWebService")
            		String cDate
    );
}
