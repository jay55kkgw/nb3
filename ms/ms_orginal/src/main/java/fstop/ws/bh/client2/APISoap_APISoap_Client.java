
package fstop.ws.bh.client2;

import javax.xml.namespace.QName;

import lombok.extern.slf4j.Slf4j;

/**
 * This class was generated by Apache CXF 3.1.17
 * 2019-05-16T02:36:25.711+08:00
 * Generated source version: 3.1.17
 * 
 */
@Slf4j
public final class APISoap_APISoap_Client {

    private static final QName SERVICE_NAME = new QName("http://billhunter.com.tw/", "API");

    private APISoap_APISoap_Client() {
    }
//
//    public static void main(String args[]) throws java.lang.Exception {
//        URL wsdlURL = API.WSDL_LOCATION;
//        if (args.length > 0 && args[0] != null && !"".equals(args[0])) { 
//            File wsdlFile = new File(args[0]);
//            try {
//                if (wsdlFile.exists()) {
//                    wsdlURL = wsdlFile.toURI().toURL();
//                } else {
//                    wsdlURL = new URL(args[0]);
//                }
//            } catch (MalformedURLException e) {
//                e.printStackTrace();
//            }
//        }
//      
//        API ss = new API(wsdlURL, SERVICE_NAME);
//        APISoap port = ss.getAPISoap();  
//        
//        {
//        log.debug("Invoking sendMailNow...");
//        java.lang.String _sendMailNow_mailTos = "";
//        int _sendMailNow_templateNo = 0;
//        int _sendMailNow_projectClass = 0;
//        int _sendMailNow_priority = 0;
//        java.lang.String _sendMailNow_variables = "";
//        java.lang.String _sendMailNow_custID = "";
//        java.lang.String _sendMailNow__return = port.sendMailNow(_sendMailNow_mailTos, _sendMailNow_templateNo, _sendMailNow_projectClass, _sendMailNow_priority, _sendMailNow_variables, _sendMailNow_custID);
//        log.debug("sendMailNow.result=" + _sendMailNow__return);
//
//
//        }
//        {
//        log.debug("Invoking sendMailNowCustom...");
//        java.lang.String _sendMailNowCustom_mailTos = "";
//        int _sendMailNowCustom_templateNo = 0;
//        int _sendMailNowCustom_projectClass = 0;
//        int _sendMailNowCustom_priority = 0;
//        java.lang.String _sendMailNowCustom_variables = "";
//        java.lang.String _sendMailNowCustom_custID = "";
//        java.lang.String _sendMailNowCustom_senderName = "";
//        java.lang.String _sendMailNowCustom_senderEmail = "";
//        java.lang.String _sendMailNowCustom_subject = "";
//        java.lang.String _sendMailNowCustom_replyEmail = "";
//        java.lang.String _sendMailNowCustom__return = port.sendMailNowCustom(_sendMailNowCustom_mailTos, _sendMailNowCustom_templateNo, _sendMailNowCustom_projectClass, _sendMailNowCustom_priority, _sendMailNowCustom_variables, _sendMailNowCustom_custID, _sendMailNowCustom_senderName, _sendMailNowCustom_senderEmail, _sendMailNowCustom_subject, _sendMailNowCustom_replyEmail);
//        log.debug("sendMailNowCustom.result=" + _sendMailNowCustom__return);
//
//
//        }
//
//        System.exit(0);
//    }

}
