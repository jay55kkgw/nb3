
package fstop.ws.bh.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type 的 Java 類別.
 * 
 * <p>下列綱要片段會指定此類別中包含的預期內容.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Get_Html_imgResult" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="GIF_BtyeArray" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getHtmlImgResult",
    "gifBtyeArray"
})
@XmlRootElement(name = "Get_Html_imgResponse")
public class GetHtmlImgResponse {

    @XmlElement(name = "Get_Html_imgResult")
    protected boolean getHtmlImgResult;
    @XmlElement(name = "GIF_BtyeArray")
    protected byte[] gifBtyeArray;

    /**
     * 取得 getHtmlImgResult 特性的值.
     * 
     */
    public boolean isGetHtmlImgResult() {
        return getHtmlImgResult;
    }

    /**
     * 設定 getHtmlImgResult 特性的值.
     * 
     */
    public void setGetHtmlImgResult(boolean value) {
        this.getHtmlImgResult = value;
    }

    /**
     * 取得 gifBtyeArray 特性的值.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getGIFBtyeArray() {
        return gifBtyeArray;
    }

    /**
     * 設定 gifBtyeArray 特性的值.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setGIFBtyeArray(byte[] value) {
        this.gifBtyeArray = value;
    }

}
