
package fstop.ws.bh.client;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the fstop.ws.bh.client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: fstop.ws.bh.client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetResultResponse }
     * 
     */
    public GetResultResponse createGetResultResponse() {
        return new GetResultResponse();
    }

    /**
     * Create an instance of {@link GetResultTest }
     * 
     */
    public GetResultTest createGetResultTest() {
        return new GetResultTest();
    }

    /**
     * Create an instance of {@link GetResultTestResponse }
     * 
     */
    public GetResultTestResponse createGetResultTestResponse() {
        return new GetResultTestResponse();
    }

    /**
     * Create an instance of {@link GetResult }
     * 
     */
    public GetResult createGetResult() {
        return new GetResult();
    }

    /**
     * Create an instance of {@link GetResultResponse.DS }
     * 
     */
    public GetResultResponse.DS createGetResultResponseDS() {
        return new GetResultResponse.DS();
    }

    /**
     * Create an instance of {@link SendReissue }
     * 
     */
    public SendReissue createSendReissue() {
        return new SendReissue();
    }

    /**
     * Create an instance of {@link SendReissueResponse }
     * 
     */
    public SendReissueResponse createSendReissueResponse() {
        return new SendReissueResponse();
    }

    /**
     * Create an instance of {@link GetHtmlTest }
     * 
     */
    public GetHtmlTest createGetHtmlTest() {
        return new GetHtmlTest();
    }

    /**
     * Create an instance of {@link GetHtmlTestResponse }
     * 
     */
    public GetHtmlTestResponse createGetHtmlTestResponse() {
        return new GetHtmlTestResponse();
    }

    /**
     * Create an instance of {@link GetHtml }
     * 
     */
    public GetHtml createGetHtml() {
        return new GetHtml();
    }

    /**
     * Create an instance of {@link GetHtmlResponse }
     * 
     */
    public GetHtmlResponse createGetHtmlResponse() {
        return new GetHtmlResponse();
    }

    /**
     * Create an instance of {@link GetHtmlImgTest }
     * 
     */
    public GetHtmlImgTest createGetHtmlImgTest() {
        return new GetHtmlImgTest();
    }

    /**
     * Create an instance of {@link GetHtmlImgTestResponse }
     * 
     */
    public GetHtmlImgTestResponse createGetHtmlImgTestResponse() {
        return new GetHtmlImgTestResponse();
    }

    /**
     * Create an instance of {@link GetHtmlImg }
     * 
     */
    public GetHtmlImg createGetHtmlImg() {
        return new GetHtmlImg();
    }

    /**
     * Create an instance of {@link GetHtmlImgResponse }
     * 
     */
    public GetHtmlImgResponse createGetHtmlImgResponse() {
        return new GetHtmlImgResponse();
    }

    /**
     * Create an instance of {@link IVRGETBILL }
     * 
     */
    public IVRGETBILL createIVRGETBILL() {
        return new IVRGETBILL();
    }

    /**
     * Create an instance of {@link IVRGETBILLResponse }
     * 
     */
    public IVRGETBILLResponse createIVRGETBILLResponse() {
        return new IVRGETBILLResponse();
    }

    /**
     * Create an instance of {@link DisableAccount }
     * 
     */
    public DisableAccount createDisableAccount() {
        return new DisableAccount();
    }

    /**
     * Create an instance of {@link DisableAccountResponse }
     * 
     */
    public DisableAccountResponse createDisableAccountResponse() {
        return new DisableAccountResponse();
    }

    /**
     * Create an instance of {@link GetBarCodeByCustID }
     * 
     */
    public GetBarCodeByCustID createGetBarCodeByCustID() {
        return new GetBarCodeByCustID();
    }

    /**
     * Create an instance of {@link GetBarCodeByCustIDResponse }
     * 
     */
    public GetBarCodeByCustIDResponse createGetBarCodeByCustIDResponse() {
        return new GetBarCodeByCustIDResponse();
    }

    /**
     * Create an instance of {@link GetBarCodeByAtmNo }
     * 
     */
    public GetBarCodeByAtmNo createGetBarCodeByAtmNo() {
        return new GetBarCodeByAtmNo();
    }

    /**
     * Create an instance of {@link GetBarCodeByAtmNoResponse }
     * 
     */
    public GetBarCodeByAtmNoResponse createGetBarCodeByAtmNoResponse() {
        return new GetBarCodeByAtmNoResponse();
    }

}
