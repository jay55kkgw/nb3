package fstop.ws;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.joda.time.DateTime;
import org.owasp.esapi.ESAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;
import com.netbank.util.ResultCode;

import fstop.exception.TopMessageException;
import fstop.orm.dao.SysDailySeqDao;
import fstop.orm.dao.TxnAmlLogDao;
import fstop.orm.po.TXNAMLLOG;
import fstop.util.StrUtils;
import fstop.ws.aml.bean.AML001RQ;
import fstop.ws.aml.bean.AML001RQ.SearchNamesSoap;
import fstop.ws.aml.bean.AML001RQ.SearchNamesSoap.Branch;
import fstop.ws.aml.bean.AML001RQ.SearchNamesSoap.DateOfBirth;
import fstop.ws.aml.bean.AML001RQ.SearchNamesSoap.Names;
import fstop.ws.aml.client.SearchResponseSoap;
import fstop.ws.aml.newclient.BranchXml;
import fstop.ws.aml.newclient.DateOfBirthXml;
import fstop.ws.aml.newclient.MatchResultXmlNewZH;
import fstop.ws.aml.newclient.SearchRequestSoapNew;
import fstop.ws.aml.newclient.SearchResponseSoapNewZH;
import fstop.ws.aml.newclient.WlsrWsApplicationNewZH;
import lombok.extern.slf4j.Slf4j;


/**
 * 
 * 功能說明 :AML WebService 
 *
 */
@Slf4j
public class AMLWebServiceTemplate {


	@Value("${aml.uri}")
	private String webServiceUri;

	@Value("${aml.ConnectionTimeout}")
	private Integer amlConnectionTimeout;

	@Value("${aml.receiveTimeout}")
	private Integer amlReceiveTimeout;
//	private String webServiceUri = "http://127.0.0.1:8080/wlsr-ws/soap/new/listzh?wsdl";
//	private String webServiceUri = "http://127.0.0.1:8080/wlsr-ws/soap/list/eai?wsdl";
//	這個目前可以暫時不用 舊網銀寫法才需要
//	private String webServiceUri_1 = "http://127.0.0.1/wlsr-ws/soap/list/eai";
	@Autowired
	SysDailySeqDao sysDailySeqDao;
	
//	TODO 等table 開好
	@Autowired
	TxnAmlLogDao txnAmlLogDao;
	
	/**
	 * WebService 初始化
	 * @param serviceUrl
	 * @param clz
	 * @return
	 */
	public <T> T createService(String serviceUrl, Class<T> clz) {
//		int ConnectionTimeout = 5;
//		int receiveTimeout = 30;
		T service = null ;
		JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
		log.info("createService.serviceUrl>>{}",serviceUrl);
		try {
			factory.getInInterceptors().add(new LoggingInInterceptor());
			factory.getOutInterceptors().add(new LoggingOutInterceptor());
			factory.setServiceClass(clz);
			factory.setAddress(serviceUrl);
			service = (T) factory.create();
			log.info("ClientProxy...");
			setConnectionTimeout(service, amlConnectionTimeout, amlReceiveTimeout);
		} catch (Throwable e) {
			log.error("createService.Exception>>{}",e);
			throw TopMessageException.create(ResultCode.FE0004);
		}
		log.debug("service>>{}",service);
		return service;
	}
	
	/**
	 * 電文收送API
	 * @param source
	 * @return
	 */
	public <T> T sendAndReceive(Object source) {
		log.info("webServiceUri>>{}",webServiceUri);
		SearchResponseSoapNewZH rs =null;
		T t = null;
		try {
			log.info(ESAPIUtil.vaildLog("AML 上行...SearchRequestSoapNew>>{}"+CodeUtil.toJson(source)));
			WlsrWsApplicationNewZH client = createService(webServiceUri,  WlsrWsApplicationNewZH.class);
			rs = client.searchNamesSoap((SearchRequestSoapNew)source);
			log.debug(ESAPIUtil.vaildLog("rs>>{}"+rs));
			if(rs != null ) {
				log.debug("isHit>>{}",rs.isHit());
				log.debug(ESAPIUtil.vaildLog("rs.getMatches>>{}"+rs.getMatches()));
				showlog(rs);
				t =  (T) rs;
			}else {
				throw TopMessageException.create(ResultCode.FE0005);
			}
		} catch (Exception e) {
			log.error("sendAndReceive error >> {}",e);
			throw TopMessageException.create(ResultCode.FE0004);
		}
		
		return t;
	}
	
	public void showlog(SearchResponseSoapNewZH rs) {
		log.debug(ESAPIUtil.vaildLog("SearchResponseSoapNewZH to json>>{}"+CodeUtil.toJson(rs)));
//		if(rs.getMatches() !=null) {
//			
//		}
		
	}
	
	public boolean callAML(Map reqParam) {
			Map<String, Object> resultMap = callAML_Map(reqParam);
			boolean hit = (boolean)resultMap.get("hit");
			return hit;
	}
	
	public Map<String, Object> callAML_Map(Map reqParam) {
		Map<String, Object> resultMap = new HashMap<>();
		String searchCode= "";
		String s1= "";
		String s2= "";
		String ts1 ="";
		String query_content ="";
		String respond_content ="";
		String status ="";
		String businessunit ="";
		SearchRequestSoapNew rq  =  null;
		try{
			businessunit = StrUtils.isEmpty((String)reqParam.get("BUSINESSUNIT")) ?"H57":(String)reqParam.get("BUSINESSUNIT");
			
			DateTime dt = new DateTime();
			String cusidn = ESAPI.encoder().encodeForHTML(reqParam.get("UID")==null ? "" : reqParam.get("UID").toString());
			String reference= cusidn +"AML";
	    	String scanType="1";  //開戶掃描
//	    	String searchCode="NB3-"+cusidn;  
	    	ts1 = dt.toString("yyyy-MM-dd HH:mm:ss");
	    	s1= ts1;  
			String name = (String)reqParam.get("NAME");
			//為原住民姓名修正 
			name = name.equals("") ? (String)reqParam.get("CUSNAME") : name;
			String BRTHDY = (String)reqParam.get("BIRTHDAY");
			int yearnum = Integer.parseInt(BRTHDY.substring(0,3))+1911;
			String year = String.valueOf(yearnum);
			String month = BRTHDY.substring(3,5);
			String day = BRTHDY.substring(5,7);
			
			searchCode="NB3_"+s1.substring(0,10)+"_"+String.valueOf(sysDailySeqDao.dailySeq("AML"));
			rq  = new SearchRequestSoapNew();
			fstop.ws.aml.newclient.SearchRequestSoapNew.Names names = new fstop.ws.aml.newclient.SearchRequestSoapNew.Names();
			LinkedList<String> list = new LinkedList<String>();
			list.add(name);
			names.setName(list);
			BranchXml branch = new BranchXml();
			branch.setBranchId((String)reqParam.get("BRHCOD"));
			branch.setBusinessUnit(businessunit);
			//20210924 將交易中文名稱帶入給AML
			String sourceSystem = "NB3";
			switch ((String)reqParam.get("ADOPID")) {
			case "N104":
				sourceSystem += "_修改數存";
				break;
			case "N201":
				sourceSystem += "_線上預約開戶";
				break;
			case "N203":
				sourceSystem += "_線上開數存";
				break;
			case "N360":
				sourceSystem += "_預約開立基金戶";
				break;
			case "N361":
				sourceSystem += "_預約開立基金戶";
				break;
			case "NA50":
				sourceSystem += "_黃金存摺帳戶申請";
				break;
			default:
				break;
			}
			branch.setSourceSystem(sourceSystem);
			DateOfBirthXml dateOfBirth = new DateOfBirthXml();
			dateOfBirth.setYear(year);
			dateOfBirth.setMonth(month);
			dateOfBirth.setDay(day);
			rq.setNames(names);
			rq.setBranch(branch);
			rq.setDateOfBirth(dateOfBirth);
			rq.setAlert(Boolean.TRUE);
			rq.setScanType(scanType);
			rq.setReference(reference);
			rq.setSearchcode(searchCode);
//			NB3-W100089655
			query_content ="NAME:"+list.get(0)+",BOD:"+dateOfBirth.getYear()+dateOfBirth.getMonth()+dateOfBirth.getDay()+",ST:"+scanType;
			SearchResponseSoapNewZH rs = sendAndReceive (rq);
			s2 = new DateTime().toString("yyyy-MM-dd HH:mm:ss");
			if(rs != null) {
				respond_content = query_content;
				log.info(ESAPIUtil.vaildLog("respond_content>>{}"+respond_content));
				if(rs.getMatches() !=null && !rs.getMatches().isEmpty()) {
					respond_content = respond_content+",MATCH-SCORE:"+rs.getMatches().get(0).getScore()+" TGNAME:"+rs.getMatches().get(0).getTargetName()+" LSNAME:"+rs.getMatches().get(0).getListName()+" LSTYPE:"+rs.getMatches().get(0).getListType();    			    				
					log.info(ESAPIUtil.vaildLog("getMatches.respond_content>>{}"+respond_content));
				}
				//status = rs.isHit()==true ? "HIT" : "NOT-HIT";
				//return rs.isHit();
				String listType = "";
				if (rs.isHit()) {
					MatchResultXmlNewZH match= rs.getMatches().get(0);
					listType = match.getListType();
					status = "HIT:type=" + listType;
				} else {
					status = "NOT-HIT"; 
				}
			 
				resultMap.put("status", status);
				resultMap.put("hit",  rs.isHit());
				resultMap.put("listType", listType );
				return resultMap;
			}else {
				status = "NO-RESPOND";
				//return Boolean.FALSE;
				resultMap.put("status", status);
				resultMap.put("hit", Boolean.FALSE);
				resultMap.put("listType", "" );
				return resultMap;
			}
			
		}	
		catch(Exception e) {
			log.error(e.getMessage());
			status = "NO-RESPOND";
			//return Boolean.FALSE;
			resultMap.put("status", status);
			resultMap.put("hit", Boolean.FALSE);
			resultMap.put("listType", "" );
			return resultMap;
		}finally {
			try {
				TXNAMLLOG txnamllog = new TXNAMLLOG();
				txnamllog.setSOURCESYSTEM("NB3");
				txnamllog.setSEARCHCODE(searchCode);
				txnamllog.setBRANCHID(ESAPI.encoder().encodeForHTML((String)reqParam.get("BRHCOD")));
				txnamllog.setBUSINESSUNIT(businessunit);
				txnamllog.setADOPID(ESAPI.encoder().encodeForHTML( ((String)reqParam.get("ADOPID"))== null ? "" : ((String)reqParam.get("ADOPID")) ));
				txnamllog.setTS1(s1);
				txnamllog.setTS2(s2);
				txnamllog.setSTATUS(status);
				txnamllog.setQUERY_CONTENT(query_content);
				txnamllog.setRESPOND_CONTENT(respond_content);
				log.info("txnamllog>>{}",txnamllog.toString());
//				TODO 等table 開好
				txnAmlLogDao.save(txnamllog);
			}catch (Exception e) {
				log.error("TXNAMLLOG SAVE ERROR");
			}
		}
//		return true;
		
	
}
	
	
//	public <T> T sendAndReceive(Object source) {
//		log.info("webServiceUri>>{}",webServiceUri);
//		String telcomString = "";
//		SearchResponseSoapEAI rs =null;
//		T t = null;
//		try {
//			telcomString =  CodeUtil.marshalXML(source);
//			log.debug("telcomString>>\n{}",telcomString);
//			WlsrWsApplicationEAI client = createService(webServiceUri,  WlsrWsApplicationEAI.class);
//			rs =client.queryXmlString(telcomString);
//			log.debug("rs>>{}",rs);
//			if(rs != null && rs.getBlueStar() !=null && rs.getBlueStar().getSearchResponseSoap() !=null) {
//				log.debug("rs.getQueryXmlStringResult>>{}",rs.getQueryXmlStringResult());
//				log.debug("isHit>>{}",rs.getBlueStar().getSearchResponseSoap().isHit());
//				t =  (T) rs.getBlueStar().getSearchResponseSoap();
//			}else {
//				throw TopMessageException.create(ResultCode.FE0005);
//			}
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			log.error("",e);
//			throw TopMessageException.create(ResultCode.FE0004);
//		}
//		
//		return t;
//	}

	/**
	 * Time out設定	
	 * @param port
	 * @param ConnectionTimeout
	 * @param receiveTimeout
	 */
	public  void setConnectionTimeout(Object port , int ConnectionTimeout ,int receiveTimeout){
		Client client = ClientProxy.getClient(port);
		HTTPConduit http = (HTTPConduit) client.getConduit();
		HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();
		httpClientPolicy.setConnectionTimeout(ConnectionTimeout * 1000);
		httpClientPolicy.setReceiveTimeout(receiveTimeout * 1000);
		http.setClient(httpClientPolicy);
		
	}	
	
	public static void oldAMLTest() {
		AMLWebServiceTemplate eaiwebservicetemplate = new AMLWebServiceTemplate();
		AML001RQ rq  = new AML001RQ();
		SearchNamesSoap  searchNamesSoap = new SearchNamesSoap();
		Names names = new Names();
		LinkedList list = new LinkedList<String>();
		list.add("郭洪玉玉");
		names.setName(list);
		Branch branch = new Branch();
		branch.setBranchId("001");
		branch.setBusinessUnit("H57");
		branch.setSourceSystem("NNB-W100089655");
		DateOfBirth dateOfBirth = new DateOfBirth();
		dateOfBirth.setYear("1991");
		dateOfBirth.setMonth("01");
		dateOfBirth.setDay("01");
		searchNamesSoap.setNames(names);
		searchNamesSoap.setBranch(branch);
		searchNamesSoap.setDateOfBirth(dateOfBirth);
		rq.setMSGNAME("AML001");
		rq.setAPP("XML");
		rq.setCLIENTIP("::1");
		rq.setTYPE("01");
		rq.setSearchNamesSoap(searchNamesSoap);
		
		SearchResponseSoap rs = eaiwebservicetemplate.sendAndReceive (rq);
		log.debug("AML001RS.isHit>>"+rs.isHit());
	}
	
	
	public static void newAMLTest() {
		log.info("newAMLTest..");
		AMLWebServiceTemplate eaiwebservicetemplate = new AMLWebServiceTemplate();
		SearchRequestSoapNew rq  = new SearchRequestSoapNew();
		fstop.ws.aml.newclient.SearchRequestSoapNew.Names names = new fstop.ws.aml.newclient.SearchRequestSoapNew.Names();
		LinkedList<String> list = new LinkedList<String>();
		list.add("郭洪玉玉");
		names.setName(list);
		BranchXml branch = new BranchXml();
		branch.setBranchId("001");
		branch.setBusinessUnit("H57");
		branch.setSourceSystem("NNB-W100089655");
		DateOfBirthXml dateOfBirth = new DateOfBirthXml();
		dateOfBirth.setYear("1991");
		dateOfBirth.setMonth("01");
		dateOfBirth.setDay("01");
		rq.setNames(names);
		rq.setBranch(branch);
		rq.setDateOfBirth(dateOfBirth);
		
		SearchResponseSoapNewZH rs = eaiwebservicetemplate.sendAndReceive (rq);
		log.debug("AML001RS.isHit>>"+rs.isHit());
	}
	
//	public static void main(String[] args) {
//		newAMLTest();
//		
//	} 
	
}
