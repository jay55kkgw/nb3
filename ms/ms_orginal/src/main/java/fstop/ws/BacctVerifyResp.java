package fstop.ws;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="CreditVerifyResp")
public class BacctVerifyResp {
	
	private String mti;				//交易類型
	private String cardNumber;		//卡號
	private String processingCode;	//處理代碼
	private String amt;				//交易金額
	private String traceNumber;		//端末交易序號
	private String systemDateTime;	//系統日期時間
	private String acqBank;			//收單行代碼
	private String srrn;			//系統追蹤號
	private String responseCode;	//回應碼
	private String terminalId;		//端末代號
	private String merchantId;		//特店代號
	private String verifyCode;		//驗證碼
	
	public String getMti() {
		return mti;
	}
	public void setMti(String mti) {
		this.mti = mti;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getProcessingCode() {
		return processingCode;
	}
	public void setProcessingCode(String processingCode) {
		this.processingCode = processingCode;
	}
	public String getAmt() {
		return amt;
	}
	public void setAmt(String amt) {
		this.amt = amt;
	}
	public String getTraceNumber() {
		return traceNumber;
	}
	public void setTraceNumber(String traceNumber) {
		this.traceNumber = traceNumber;
	}
	public String getSystemDateTime() {
		return systemDateTime;
	}
	public void setSystemDateTime(String systemDateTime) {
		this.systemDateTime = systemDateTime;
	}
	public String getAcqBank() {
		return acqBank;
	}
	public void setAcqBank(String acqBank) {
		this.acqBank = acqBank;
	}		
	public String getSrrn() {
		return srrn;
	}
	public void setSrrn(String srrn) {
		this.srrn = srrn;
	}
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getTerminalId() {
		return terminalId;
	}
	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}				
	public String getVerifyCode() {
		return verifyCode;
	}
	public void setVerifyCode(String verifyCode) {
		this.verifyCode = verifyCode;
	}

}
