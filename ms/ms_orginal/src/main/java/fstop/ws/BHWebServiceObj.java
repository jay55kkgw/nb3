package fstop.ws;

import lombok.Data;

@Data
public class BHWebServiceObj {
	
	private String Result_no;
	private String Subject;
	private String Bill_no;
	private String Bill_type;
	private String Send_date;
	private String Bill_Month;
	private String Cust_Name;
	private String email;
	private String userid;
	private String Mail_status;
	private String Project_No;
	private String Query_Date;
	private String CARD;
	private String Branch;
//
//	public String getCARD() {
//		return CARD;
//	}
//	public void setCARD(String card) {
//		CARD = card;
//	}
//	public String getBranch() {
//		return Branch;
//	}
//	public void setBranch(String branch) {
//		Branch = branch;
//	}
//	public String getResult_no() {
//		return Result_no;
//	}
//	public void setResult_no(String result_no) {
//		Result_no = result_no;
//	}
//	public String getSubject() {
//		return Subject;
//	}
//	public void setSubject(String subject) {
//		Subject = subject;
//	}
//	public String getBill_no() {
//		return Bill_no;
//	}
//	public void setBill_no(String bill_no) {
//		Bill_no = bill_no;
//	}
//	public String getBill_type() {
//		return Bill_type;
//	}
//	public void setBill_type(String bill_type) {
//		Bill_type = bill_type;
//	}
//	public String getSend_date() {
//		return Send_date;
//	}
//	public void setSend_date(String send_date) {
//		Send_date = send_date;
//	}
//	public String getBill_Month() {
//		return Bill_Month;
//	}
//	public void setBill_Month(String bill_Month) {
//		Bill_Month = bill_Month;
//	}
//	public String getCust_Name() {
//		return Cust_Name;
//	}
//	public void setCust_Name(String cust_Name) {
//		Cust_Name = cust_Name;
//	}
//	public String getEmail() {
//		return email;
//	}
//	public void setEmail(String email) {
//		this.email = email;
//	}
//	public String getUserid() {
//		return userid;
//	}
//	public void setUserid(String userid) {
//		this.userid = userid;
//	}
//	public String getMail_status() {
//		return Mail_status;
//	}
//	public void setMail_status(String mail_status) {
//		Mail_status = mail_status;
//	}
//	public String getProject_No() {
//		return Project_No;
//	}
//	public void setProject_No(String project_No) {
//		Project_No = project_No;
//	}
//	public String getQuery_Date() {
//		return Query_Date;
//	}
//	public void setQuery_Date(String query_Date) {
//		Query_Date = query_Date;
//	}
//
	
	
}
