package fstop.ws;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.stream.StreamSource;

public class JAXBUtils {


    public static String marshaller(Object o) throws Exception {
        JAXBContext jaxbContext = JAXBContext.newInstance(o.getClass());
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        // output pretty printed
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        StringWriter writer = new StringWriter();
        jaxbMarshaller.marshal(o, writer);

        return writer.toString();
    }
    public static <T> T unmarshaller(String xmlStr, Class<T> xlz) throws Exception {

        JAXBContext jaxbContext = JAXBContext.newInstance(xlz);
        
        XMLInputFactory xif = XMLInputFactory.newFactory();
        xif.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, false);
        xif.setProperty(XMLInputFactory.SUPPORT_DTD, false);
        
        InputStream targetStream = new ByteArrayInputStream(xmlStr.getBytes());
        XMLStreamReader xsr = xif.createXMLStreamReader(targetStream,"UTF-8");
        
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

        T result = (T) jaxbUnmarshaller.unmarshal(xsr);

        return result;
    }






}
