package fstop.ws;

public class HostPARK003JsonRecord {

	private String zonecode; 	//縣市別
    private String carid;	  	//牌照號碼	
    private String carkind;	  	//車種   
    private String startdate;	//啟用日期/註冊日期
    private String account;  	//扣款帳號
    private String source;  	//來源
    private String status; 		//申請代收車號狀態
    
	public String getZonecode() {
		return zonecode;
	}
	public void setZonecode(String zonecode) {
		this.zonecode = zonecode;
	}
	public String getCarid() {
		return carid;
	}
	public void setCarid(String carid) {
		this.carid = carid;
	}
	public String getCarkind() {
		return carkind;
	}
	public void setCarkind(String carkind) {
		this.carkind = carkind;
	}	
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}	
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
