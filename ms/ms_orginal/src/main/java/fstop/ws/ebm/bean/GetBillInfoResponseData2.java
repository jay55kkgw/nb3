package fstop.ws.ebm.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "RTN_MSG",
    "RTN_DATA"
})
@XmlRootElement(name = "DATA")
@Data
public class GetBillInfoResponseData2 {
    protected GetBillInfoResponseDataMsg RTN_MSG;
    protected GetBillInfoResponseDataData RTN_DATA;
}
