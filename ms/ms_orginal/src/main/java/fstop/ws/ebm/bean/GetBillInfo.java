package fstop.ws.ebm.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cUkey"
})
@XmlRootElement(name = "GetBillInfo")
public class GetBillInfo {

    @XmlElement(name = "cUkey", namespace="EnterpriseServerBase.WebService.DynamicWebcalling.TBBWebService")
    protected String cUkey;

	public String getcUkey() {
		return cUkey;
	}

	public void setcUkey(String cUkey) {
		this.cUkey = cUkey;
	}
}
