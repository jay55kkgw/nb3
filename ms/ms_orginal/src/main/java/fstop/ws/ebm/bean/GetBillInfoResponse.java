package fstop.ws.ebm.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "GetBillInfoResult"
})
@XmlRootElement(name = "GetBillInfoResponse")
@Data
public class GetBillInfoResponse {

    @XmlElement(name = "GetBillInfoResult", namespace="EnterpriseServerBase.WebService.DynamicWebcalling.TBBWebService")
    protected GetBillInfoResponseData GetBillInfoResult;
}
