package fstop.ws.ebm.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cUkey",
    "cDate"
})
@XmlRootElement(name = "SetBillSend")
public class SetBillSend {

    @XmlElement(name = "cUkey", namespace="EnterpriseServerBase.WebService.DynamicWebcalling.TBBWebService")
    protected String cUkey;
    @XmlElement(name = "cDate", namespace="EnterpriseServerBase.WebService.DynamicWebcalling.TBBWebService")
    protected String cDate;

	public String getcDate() {
		return cDate;
	}

	public void setcDate(String cDate) {
		this.cDate = cDate;
	}
	public String getcUkey() {
		return cUkey;
	}

	public void setcUkey(String cUkey) {
		this.cUkey = cUkey;
	}
}
