package fstop.ws.ebm.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cRtnCode",
    "cRtnDesc"
})
@XmlRootElement(name = "RTN_MSG")
@Data
public class GetBillInfoResponseDataMsg {
    protected String cRtnCode;
    protected String cRtnDesc;
}
