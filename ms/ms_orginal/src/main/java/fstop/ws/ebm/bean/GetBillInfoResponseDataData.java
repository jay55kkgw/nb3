package fstop.ws.ebm.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cSDate",
    "cEmail"
})
@XmlRootElement(name = "RTN_DATA")
@Data
public class GetBillInfoResponseDataData {
    protected String cSDate;
    protected String cEmail;
}
