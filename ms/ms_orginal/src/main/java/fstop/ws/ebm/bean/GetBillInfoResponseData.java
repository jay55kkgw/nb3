package fstop.ws.ebm.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "DATA"
})
@XmlRootElement(name = "GetBillInfoResult")
@Data
public class GetBillInfoResponseData {

    @XmlElement(name = "DATA", namespace="")
    protected GetBillInfoResponseData2 DATA;
}
