package fstop.ws;

import fstop.ws.HostPARK003JsonRecord;
import java.util.*;

public class HostPARK003JsonResponse {

	private String respcode;   //回應代碼
    private String respstring;	//回應代碼敘述
    private String customerid;	//客戶ID
    private String email;		//Email
    private String phone;		//手機號碼	
    private String name;		//客戶姓名
    private String count;		//申請筆數	
    private List<HostPARK003JsonRecord>   cars = new ArrayList<HostPARK003JsonRecord>();//
    
	public String getRespcode() {
		return respcode;
	}
	public void setRespcode(String respcode) {
		this.respcode = respcode;
	}
	public String getRespstring() {
		return respstring;
	}
	public void setRespstring(String respstring) {
		this.respstring = respstring;
	}
	public String getCustomerid() {
		return customerid;
	}
	public void setCustomerid(String customerid) {
		this.customerid = customerid;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCount() {
		return count;
	}
	public void setCount(String count) {
		this.count = count;
	}
	public List<HostPARK003JsonRecord> getCars() {
		return cars;
	}
	public void setCars(List<HostPARK003JsonRecord> cars) {
		this.cars = cars;
	}
}
