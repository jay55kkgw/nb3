package fstop.ws.eai.bean;

import java.util.LinkedList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;
/**
 * 
 * 功能說明 :定義EAIRQ的基本屬性
 *
 */
@XmlRootElement(name="BlueStar")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class TD01RS  {
	
	@XmlAttribute(name="MsgName")
	private String MSGNAME;
	@XmlAttribute(name="RqUid")
	private String RQUID;
	@XmlAttribute(name="Status")
	private String STATUS;
	
	private String OFFSET;
	private String HEADER;
	private String MSGCOD;		
	private String TXNIDN ;
	private String FILLER01 ;
	private String TXNCNT ;
	
	private String CUSNAME ;
	private String CYCLE ;
	private String DUEDATE ;
	private String CURRATE ;
	private String PYMTACC ;
	private String CARDNUM ;
	private String RECCNT ;
	private String USERDATA ;
	
	private LinkedList<TD01RS_Data> REC ;

	@Override
	public String toString() {
		return "TD01RS [MSGNAME=" + MSGNAME + ", RQUID=" + RQUID + ", STATUS=" + STATUS + ", OFFSET=" + OFFSET
				+ ", HEADER=" + HEADER + ", MSGCOD=" + MSGCOD + ", TXNIDN=" + TXNIDN + ", FILLER01=" + FILLER01
				+ ", TXNCNT=" + TXNCNT + ", CUSNAME=" + CUSNAME + ", CYCLE=" + CYCLE + ", DUEDATE=" + DUEDATE
				+ ", CURRATE=" + CURRATE + ", PYMTACC=" + PYMTACC + ", CARDNUM=" + CARDNUM + ", RECCNT=" + RECCNT
				+ ", USERDATA=" + USERDATA + ", DETAIL=" + REC + "]";
	}
	
//	private String PURDATE ;
//	private String POSTDATE ;
//	private String DESCTXT ;
//	private String CRDNAME ;
//	private String CURRENCY ;
//	private String SRCAMNT ;
//	private String CURAMNT ;
	
	
	
	
	
	
	
	
	
	
	
	
	
}
