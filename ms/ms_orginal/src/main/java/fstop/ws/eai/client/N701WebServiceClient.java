package fstop.ws.eai.client;

import java.io.ByteArrayInputStream;
import java.util.Hashtable;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fstop.model.MVHImpl;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public final class N701WebServiceClient{
//	private Logger logger = Logger.getLogger(getClass());
	private Map setting;
	public void setSetting(Map setting) {
		this.setting = setting;
	}
    public MVHImpl processGo(Map params) throws Exception {
    	return process(params);
    }

	public N701WebServiceClient() {
    }
   
    /**
     * 呼叫WebService執行全國繳跨行轉帳約定撤銷
     * @param params (參數集合)
     * @return String (ResponseCode)
     * @throws Exception
     */
    private MVHImpl process(Map params) throws Exception {
        //TODO:本處未來要改
        
        log.warn("eaiWebService Invoking process...");
       	String wsdlURL = StrUtils.trim((String)setting.get("eai.webservices.uri"));
       	MVHImpl response = new MVHImpl();				
       	try {
//       		MsgHandlerClient client = new MsgHandlerClient(wsdlURL);
//       		MsgHandlerSoap port = (MsgHandlerSoap)client.getMsgHandlerSoap();
//       		SubmitXmlStringRequest submitXml = new SubmitXmlStringRequest();
//       		String strEAISend = composeN701(params);
//       		submitXml.setSRequest(strEAISend);
//       		logger.warn("eaiWebService strEAISend: " + strEAISend);
//       		SubmitXmlStringResponse submitXmlStringResponse = port.submitXmlString(submitXml);
//       		int rtn = submitXmlStringResponse.getSubmitXmlStringResult();
//       		logger.warn("eaiWebService ProcessCode: " + rtn);
//       		String strResponse = submitXmlStringResponse.getSResponse();  
//       		logger.warn("eaiWebService strResponse:"+strResponse);
//       		if(rtn==0)
//       		{
//       			response = decomposeN701EAI(strResponse);				
//       		}
//       		else
//       		{
//       			if(rtn==29000 || rtn==29002 || rtn==10001)
//       				response.getFlatValues().put("RSPCOD",String.valueOf(rtn));
//       			else
//       				response.getFlatValues().put("RSPCOD","ZX99");
//       		}
       	}
       	catch(Exception e) {
       		response.getFlatValues().put("RSPCOD","ZX99");
       		log.warn("N701.java process exception:"+e.getMessage());
       	}
		return response;
    }

	private String composeN701(Hashtable params) {
		String strEAISend = "";

		strEAISend = "<BlueStar xmlns:ns2=\"http://www.cedar.com.tw/bluestar/\" xmlns=\"\" MsgName=\"N701\" RqUid=\"24db8e81-7c28-49ce-9c4f-b4d7e3c04d75\">";
		//strEAISend += "<INPLEN/>";
		//strEAISend += "<INPZZZ>0</INPZZZ>";
		strEAISend += "<TRXCOD>TVO8MBBT</TRXCOD>";
		strEAISend += "<FILL01></FILL01>";
		strEAISend += "<HEADER>N701</HEADER>";
		strEAISend += "<VERSION>NB08</VERSION>";
		strEAISend += "<DATE>"+(params.get("DATE")==null ? "" : params.get("DATE"))+"</DATE>";
		strEAISend += "<TIME>"+(params.get("TIME")==null ? "" : params.get("TIME"))+"</TIME>";
		strEAISend += "<SYNC>"+(params.get("SYNC")==null ? "" : params.get("SYNC"))+"</SYNC>";
		strEAISend += "<PPSYNC>"+(params.get("PPSYNC")==null ? "" : params.get("PPSYNC"))+"</PPSYNC>";
		strEAISend += "<PINKEY></PINKEY>";
		strEAISend += "<CERTACN>"+(params.get("CERTACN")==null ? "" : params.get("CERTACN"))+"</CERTACN>";
		strEAISend += "<FLAG>"+(params.get("FLAG")==null ? "" : params.get("FLAG"))+"</FLAG>";
		strEAISend += "<BNKRA>"+(params.get("BNKRA")==null ? "" : params.get("BNKRA"))+"</BNKRA>";
		strEAISend += "<XMLCA>"+(params.get("XMLCA")==null ? "" : params.get("XMLCA"))+"</XMLCA>";
		strEAISend += "<XMLCN>"+(params.get("XMLCN")==null ? "" : params.get("XMLCN"))+"</XMLCN>";
		strEAISend += "<PPSYNCN>"+(params.get("PPSYNCN")==null ? "" : params.get("PPSYNCN"))+"</PPSYNCN>";
		strEAISend += "<PINNEW>"+(params.get("PINNEW")==null ? "" : params.get("PINNEW"))+"</PINNEW>";
		strEAISend += "<CUSIDN>"+(params.get("UID")==null ? "" : params.get("UID"))+"</CUSIDN>";
		strEAISend += "<ACN>"+(params.get("ACN")==null ? "" : params.get("ACN"))+"</ACN>";
		strEAISend += "<PCSEQ>"+(params.get("PCSEQ")==null ? "" : params.get("PCSEQ"))+"</PCSEQ>";
		strEAISend += "<TXID>"+(params.get("TXID1")==null ? "" : params.get("TXID1"))+"</TXID>";
		strEAISend += "<PATP>"+(params.get("PATP")==null ? "" : params.get("PATP"))+"</PATP>";
		///strEAISend += "<ATTIAC>"+(params.get("ATTIAC")==null ? "" : params.get("ATTIAC"))+"</ATTIAC>";
		//strEAISend += "<ATFEID>"+(params.get("ATFEID")==null ? "" : params.get("ATFEID"))+"</ATFEID>";		
		strEAISend += "</BlueStar>";
		
		log.warn("composeN701 = " + strEAISend);
		

		return strEAISend;
	}
	private MVHImpl decomposeN701EAI(String eaiResponse) throws Exception {
		
		MVHImpl response = new MVHImpl();
		log.warn("--------------- decomposeN701EAI START ---------------");
	    try {
	         // use the factory to create a documentbuilder
	    		DocumentBuilderFactory f = DocumentBuilderFactory.newInstance();
	    		DocumentBuilder b = f.newDocumentBuilder();
	    		Document doc = b.parse(new ByteArrayInputStream(eaiResponse.getBytes()));
	    		NodeList BlueStar = doc.getElementsByTagName("BlueStar");
	    		for (int i = 0; i < BlueStar.getLength(); i++) {
	    		    Element book = (Element) BlueStar.item(i);
	    		    Node OFFSET = book.getElementsByTagName("OFFSET").item(0);
	    		    Node HEADER = book.getElementsByTagName("HEADER").item(0);
	    		    Node SYNC = book.getElementsByTagName("SYNC").item(0);
	    		    Node RSPCOD = book.getElementsByTagName("RSPCOD").item(0);
	    		    Node PCSEQ = book.getElementsByTagName("PCSEQ").item(0);
	    		    Node STAN = book.getElementsByTagName("STAN").item(0);
	    		    Node ACN = book.getElementsByTagName("ACN").item(0);
	    		    Node DATE = book.getElementsByTagName("DATE").item(0);
	    		    Node TIME = book.getElementsByTagName("TIME").item(0);
	    		    
	    		    response.getFlatValues().put("OFFSET",OFFSET.getTextContent());
	    		    response.getFlatValues().put("HEADER",HEADER.getTextContent());
	    		    response.getFlatValues().put("SYNC",SYNC.getTextContent());
	    		    response.getFlatValues().put("RSPCOD",RSPCOD.getTextContent());
	    		    response.getFlatValues().put("PCSEQ",PCSEQ.getTextContent());
	    		    response.getFlatValues().put("STAN",STAN.getTextContent());
	    		    response.getFlatValues().put("ACN",ACN.getTextContent());
	    		    response.getFlatValues().put("DATE",DATE.getTextContent());
	    		    response.getFlatValues().put("TIME",TIME.getTextContent());
	    		    
	    		    log.warn("N701 NODE RSPCOD:"+RSPCOD.getTextContent());
	    		    //logger.warn("N701 Set FEE:"+response.getValueByFieldName("FEE"));
	    		    
	    		}
				log.warn("--------------- decomposeN701EAI END ---------------");			
	    } catch (Exception ex) {
	    	 log.warn("decomposeN701EAI Exception:"+ex.getMessage());
	    	 response.getFlatValues().put("RSPCOD","Z010");
	    }		
		return response;
	}    
}
