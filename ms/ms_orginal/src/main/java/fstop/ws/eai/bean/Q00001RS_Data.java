package fstop.ws.eai.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@XmlRootElement(name="data")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class Q00001RS_Data {

	private String OrgName;
	private String BillName;		
	private String CancelNo ;
	private String Amount ;
	private String PayDueDate ;
}
