package fstop.ws.eai.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.netbank.util.StrUtils;

import lombok.Data;
/**
 * 
 * 功能說明 :定義EAIRQ的基本屬性
 *
 */
@XmlRootElement(name="BlueStar")
@XmlAccessorType(XmlAccessType.FIELD)
//@XmlType(propOrder = {"FILLER01","FILLER02","CARDNUM","CUSIDN","PROPERTY","TXNCNT"})
@Data
public class C900RQ  {
//	public class C900RQ extends BaseRQ {
	@XmlAttribute(name="MsgName")
	private String MSGNAME;
	private String TRXCOD="MI62";//在EAI那邊會補空白長度10
//	private String FILL_X1;
//	private String TRANNAME="C900";
//	private String CTS;
	private String MSGTYPE="1304";
	private String Primarybitmap="4030810000010000";
	private String ACNDATALEN="16";
	private String PRIACN;
	private String STAN;
	private String DATE;
	private String TIME;
	private String DATE4;
	private String FUNCODE="305";
	private String INPREQLEN="175";
	private String TRACODE="CCCH";
	private String SERVCODE="IMSNB";
	private String TERMID;
	private String REQTYPE="NEW";
	private String NUMTRA;
	private String SEQ="171";
	private String NAME;
	private String DATEBRI;
	private String IDNUM;
	private String OFFPHONE;
	private String HOUPHONE;
	private String MOTMAINAME;
	private String REASON;
	private String NEWPIN;
	private String PERID;
	
}
