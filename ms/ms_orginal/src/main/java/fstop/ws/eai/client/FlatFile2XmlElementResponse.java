
package fstop.ws.eai.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type 的 Java 類別.
 * 
 * <p>下列綱要片段會指定此類別中包含的預期內容.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="FlatFile2XmlElementResult" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="RequestXmlElement" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;any/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "flatFile2XmlElementResult",
    "requestXmlElement"
})
@XmlRootElement(name = "FlatFile2XmlElementResponse")
public class FlatFile2XmlElementResponse {

    @XmlElement(name = "FlatFile2XmlElementResult")
    protected int flatFile2XmlElementResult;
    @XmlElement(name = "RequestXmlElement")
    protected FlatFile2XmlElementResponse.RequestXmlElement requestXmlElement;

    /**
     * 取得 flatFile2XmlElementResult 特性的值.
     * 
     */
    public int getFlatFile2XmlElementResult() {
        return flatFile2XmlElementResult;
    }

    /**
     * 設定 flatFile2XmlElementResult 特性的值.
     * 
     */
    public void setFlatFile2XmlElementResult(int value) {
        this.flatFile2XmlElementResult = value;
    }

    /**
     * 取得 requestXmlElement 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link FlatFile2XmlElementResponse.RequestXmlElement }
     *     
     */
    public FlatFile2XmlElementResponse.RequestXmlElement getRequestXmlElement() {
        return requestXmlElement;
    }

    /**
     * 設定 requestXmlElement 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link FlatFile2XmlElementResponse.RequestXmlElement }
     *     
     */
    public void setRequestXmlElement(FlatFile2XmlElementResponse.RequestXmlElement value) {
        this.requestXmlElement = value;
    }


    /**
     * <p>anonymous complex type 的 Java 類別.
     * 
     * <p>下列綱要片段會指定此類別中包含的預期內容.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;any/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "any"
    })
    public static class RequestXmlElement {

        @XmlAnyElement(lax = true)
        protected Object any;

        /**
         * 取得 any 特性的值.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getAny() {
            return any;
        }

        /**
         * 設定 any 特性的值.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setAny(Object value) {
            this.any = value;
        }

    }

}
