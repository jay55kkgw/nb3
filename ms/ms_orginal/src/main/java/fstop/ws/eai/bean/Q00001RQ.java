package fstop.ws.eai.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@XmlRootElement(name="BlueStar")
@XmlAccessorType(XmlAccessType.FIELD)
//@XmlType(propOrder = {"FILLER01","FILLER02","CARDNUM","CUSIDN","PROPERTY","TXNCNT"})
@Data
public class Q00001RQ{
	@XmlAttribute(name="MsgName")
	private String MSGNAME;
	@XmlAttribute(name="App")
	private String APP;
	
	private String SYSId;
	private String PWD;
	private String TYPE;
	private String TXNTYPE;
	private String IDentity;
	private String MAC;
}
