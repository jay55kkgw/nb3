
package fstop.ws.eai.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type 的 Java 類別.
 * 
 * <p>下列綱要片段會指定此類別中包含的預期內容.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="XmlString2FlatFileResult" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="tita" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "xmlString2FlatFileResult",
    "tita"
})
@XmlRootElement(name = "XmlString2FlatFileResponse")
public class XmlString2FlatFileResponse {

    @XmlElement(name = "XmlString2FlatFileResult")
    protected int xmlString2FlatFileResult;
    protected String tita;

    /**
     * 取得 xmlString2FlatFileResult 特性的值.
     * 
     */
    public int getXmlString2FlatFileResult() {
        return xmlString2FlatFileResult;
    }

    /**
     * 設定 xmlString2FlatFileResult 特性的值.
     * 
     */
    public void setXmlString2FlatFileResult(int value) {
        this.xmlString2FlatFileResult = value;
    }

    /**
     * 取得 tita 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTita() {
        return tita;
    }

    /**
     * 設定 tita 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTita(String value) {
        this.tita = value;
    }

}
