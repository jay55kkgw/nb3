package fstop.ws.eai.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Data;

/**
 * 電文XML物件
 */
//@XmlRootElement(namespace="http://www.cedar.com.tw/bluestar/")
@XmlRootElement(name="BlueStar")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"MSGNAME","RQUID" ,"STATUS", "DFH","OFFSET","HEADER","MSGCOD"})
@Data
public class BaseRS {
	
	@XmlAttribute(name="MsgName")
	public String MSGNAME;
	@XmlAttribute(name="RqUid")
	public String RQUID;
	@XmlAttribute(name="Status")
	public String STATUS;
	
	public String DFH;
	public String OFFSET;
	public String HEADER;
	public String MSGCOD;
	@Override
	public String toString() {
		return "BaseRS [MSGNAME=" + MSGNAME + ", RQUID=" + RQUID + ", STATUS=" + STATUS + ", DFH=" + DFH + ", OFFSET="
				+ OFFSET + ", HEADER=" + HEADER + ", MSGCOD=" + MSGCOD + "]";
	}
	
	
	
	
}
