package fstop.ws.eai.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.netbank.util.StrUtils;

import lombok.Data;
/**
 * 
 * 功能說明 :定義EAIRQ的基本屬性
 *
 */
@XmlRootElement(name="BlueStar")
@XmlAccessorType(XmlAccessType.FIELD)
//@XmlType(propOrder = {"FILLER01","FILLER02","CARDNUM","CUSIDN","PROPERTY","TXNCNT"})
@Data
public class CK01RQ extends BaseRQ {
	private String FILLER01;
	private String FILLER02;
	private String CARDNUM;
	private String CUSIDN;
	private String PROPERTY;
	private String TXNCNT;
	private String USERDATA;
}
