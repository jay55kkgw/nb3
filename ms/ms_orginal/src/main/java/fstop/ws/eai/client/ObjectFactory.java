
package fstop.ws.eai.client;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the fstop.ws.eai.client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: fstop.ws.eai.client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SubmitXmlResponse }
     * 
     */
    public SubmitXmlResponse createSubmitXmlResponse() {
        return new SubmitXmlResponse();
    }

    /**
     * Create an instance of {@link FlatFile2XmlElementResponse }
     * 
     */
    public FlatFile2XmlElementResponse createFlatFile2XmlElementResponse() {
        return new FlatFile2XmlElementResponse();
    }

    /**
     * Create an instance of {@link SubmitXmlSync }
     * 
     */
    public SubmitXmlSync createSubmitXmlSync() {
        return new SubmitXmlSync();
    }

    /**
     * Create an instance of {@link SubmitXmlSyncResponse }
     * 
     */
    public SubmitXmlSyncResponse createSubmitXmlSyncResponse() {
        return new SubmitXmlSyncResponse();
    }

    /**
     * Create an instance of {@link SubmitXml }
     * 
     */
    public SubmitXml createSubmitXml() {
        return new SubmitXml();
    }

    /**
     * Create an instance of {@link SubmitXmlResponse.Response }
     * 
     */
    public SubmitXmlResponse.Response createSubmitXmlResponseResponse() {
        return new SubmitXmlResponse.Response();
    }

    /**
     * Create an instance of {@link SubmitXmlStringRequest }
     * 
     */
    public SubmitXmlStringRequest createSubmitXmlStringRequest() {
        return new SubmitXmlStringRequest();
    }

    /**
     * Create an instance of {@link SubmitXmlStringResponse }
     * 
     */
    public SubmitXmlStringResponse createSubmitXmlStringResponse() {
        return new SubmitXmlStringResponse();
    }

    /**
     * Create an instance of {@link SubmitFlatFile }
     * 
     */
    public SubmitFlatFile createSubmitFlatFile() {
        return new SubmitFlatFile();
    }

    /**
     * Create an instance of {@link SubmitFlatFileResponse }
     * 
     */
    public SubmitFlatFileResponse createSubmitFlatFileResponse() {
        return new SubmitFlatFileResponse();
    }

    /**
     * Create an instance of {@link FlatFile2XmlString }
     * 
     */
    public FlatFile2XmlString createFlatFile2XmlString() {
        return new FlatFile2XmlString();
    }

    /**
     * Create an instance of {@link FlatFile2XmlStringResponse }
     * 
     */
    public FlatFile2XmlStringResponse createFlatFile2XmlStringResponse() {
        return new FlatFile2XmlStringResponse();
    }

    /**
     * Create an instance of {@link FlatFile2XmlElement }
     * 
     */
    public FlatFile2XmlElement createFlatFile2XmlElement() {
        return new FlatFile2XmlElement();
    }

    /**
     * Create an instance of {@link FlatFile2XmlElementResponse.RequestXmlElement }
     * 
     */
    public FlatFile2XmlElementResponse.RequestXmlElement createFlatFile2XmlElementResponseRequestXmlElement() {
        return new FlatFile2XmlElementResponse.RequestXmlElement();
    }

    /**
     * Create an instance of {@link XmlString2FlatFile }
     * 
     */
    public XmlString2FlatFile createXmlString2FlatFile() {
        return new XmlString2FlatFile();
    }

    /**
     * Create an instance of {@link XmlString2FlatFileResponse }
     * 
     */
    public XmlString2FlatFileResponse createXmlString2FlatFileResponse() {
        return new XmlString2FlatFileResponse();
    }

}
