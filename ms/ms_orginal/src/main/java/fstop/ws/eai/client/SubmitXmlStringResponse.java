
package fstop.ws.eai.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type 的 Java 類別.
 * 
 * <p>下列綱要片段會指定此類別中包含的預期內容.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SubmitXmlStringResult" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="sResponse" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "submitXmlStringResult",
    "sResponse"
})
@XmlRootElement(name = "SubmitXmlStringResponse")
public class SubmitXmlStringResponse {

    @XmlElement(name = "SubmitXmlStringResult")
    protected int submitXmlStringResult;
    protected String sResponse;

    /**
     * 取得 submitXmlStringResult 特性的值.
     * 
     */
    public int getSubmitXmlStringResult() {
        return submitXmlStringResult;
    }

    /**
     * 設定 submitXmlStringResult 特性的值.
     * 
     */
    public void setSubmitXmlStringResult(int value) {
        this.submitXmlStringResult = value;
    }

    /**
     * 取得 sResponse 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSResponse() {
        return sResponse;
    }

    /**
     * 設定 sResponse 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSResponse(String value) {
        this.sResponse = value;
    }

}
