package fstop.ws.eai.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.netbank.util.fstop.DateUtil;

import lombok.Data;
/**
 * 
 * 功能說明 :定義EAIRQ的基本屬性
 *
 */
@XmlRootElement(name="BlueStar")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"MSGNAME","TXNCOD","TXNIDN"})
@Data
public class BaseRQ {
	@XmlAttribute(name="MsgName")
	private String MSGNAME;
	private String TXNCOD = "NB01";
	private String TXNIDN;
}
