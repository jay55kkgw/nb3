package fstop.ws.eai.bean;

import java.util.LinkedList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;
/**
 * 
 * 功能說明 :定義EAIRS的基本屬性
 *
 */
@XmlRootElement(name="BlueStar")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class C900RS  {
	
	@XmlAttribute(name="MsgName")
	private String MSGNAME;
	@XmlAttribute(name="RqUid")
	private String RQUID;
	@XmlAttribute(name="Status")
	private String STATUS;
	
	private String TOTA ;
	
	
	
	
	
	
	
	
	
	
	
	
	
}
