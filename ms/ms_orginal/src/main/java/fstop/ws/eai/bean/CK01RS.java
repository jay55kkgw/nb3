package fstop.ws.eai.bean;

import java.util.LinkedList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.netbank.util.StrUtils;

import lombok.Data;
/**
 * 
 * 功能說明 :定義EAIRQ的基本屬性
 *
 */
@XmlRootElement(name="BlueStar")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class CK01RS  {
	
	@XmlAttribute(name="MsgName")
	public String MSGNAME;
	@XmlAttribute(name="RqUid")
	public String RQUID;
	@XmlAttribute(name="Status")
	public String STATUS;
	
	
	public String MSGCOD;		
	public String TXNIDN ;
	public String TXNCNT ;
	
	public String CUSTID ;
	public String CUSNAME ;
	public String HPHONE ;
	public String OPHONE ;
	public String MPFONE ;
	public String BLOCKFG ;
	public String CYUSDFG ;
	public String DELAYPAYFG ;
	public String CURRBAL ;
	public String CRLIMIT ;
	public String POT;
	public String CYCLE ;
	public String USERDATA ;
	private LinkedList<CK01RS_Data> REC ;
	@Override
	public String toString() {
		return "CK01RS [MSGNAME=" + MSGNAME + ", RQUID=" + RQUID + ", STATUS=" + STATUS + ", MSGCOD=" + MSGCOD + ", TXNIDN=" + TXNIDN + ", TXNCNT=" + TXNCNT
				+ ", CUSTID=" + CUSTID + ", CUSNAME=" + CUSNAME + ", HPHONE=" + HPHONE + ", OPHONE=" + OPHONE
				+ ", MPFONE=" + MPFONE + ", BLOCK_FG=" + BLOCKFG + ", CY_USD_FG=" + CYUSDFG
				+ ", DELAY_PAY_FG=" + DELAYPAYFG + ", CURRBAL=" + CURRBAL + ", CRLIMIT=" + CRLIMIT + ", POT=" +POT+", CYCLE="
				+ CYCLE + ", USERDATA=" + USERDATA+", REC=" + REC + "]";
	}

}
