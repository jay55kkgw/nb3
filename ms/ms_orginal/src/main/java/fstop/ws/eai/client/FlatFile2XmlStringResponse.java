
package fstop.ws.eai.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type 的 Java 類別.
 * 
 * <p>下列綱要片段會指定此類別中包含的預期內容.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="FlatFile2XmlStringResult" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="XmlString" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "flatFile2XmlStringResult",
    "xmlString"
})
@XmlRootElement(name = "FlatFile2XmlStringResponse")
public class FlatFile2XmlStringResponse {

    @XmlElement(name = "FlatFile2XmlStringResult")
    protected int flatFile2XmlStringResult;
    @XmlElement(name = "XmlString")
    protected String xmlString;

    /**
     * 取得 flatFile2XmlStringResult 特性的值.
     * 
     */
    public int getFlatFile2XmlStringResult() {
        return flatFile2XmlStringResult;
    }

    /**
     * 設定 flatFile2XmlStringResult 特性的值.
     * 
     */
    public void setFlatFile2XmlStringResult(int value) {
        this.flatFile2XmlStringResult = value;
    }

    /**
     * 取得 xmlString 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXmlString() {
        return xmlString;
    }

    /**
     * 設定 xmlString 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXmlString(String value) {
        this.xmlString = value;
    }

}
