
package fstop.ws.eai.client;

/**
 * Please modify this class to meet your needs
 * This class is not complete
 */

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;

import lombok.extern.slf4j.Slf4j;

/**
 * This class was generated by Apache CXF 3.1.17
 * 2019-03-25T19:35:39.951+08:00
 * Generated source version: 3.1.17
 * 
 */
@Slf4j
public final class MsgHandlerSoap_MsgHandlerSoap12_Client {

    private static final QName SERVICE_NAME = new QName("http://www.cedar.com.tw/bluestar/", "MsgHandler");

    private MsgHandlerSoap_MsgHandlerSoap12_Client() {
    }

//    public static void main(String args[]) throws java.lang.Exception {
//        URL wsdlURL = MsgHandler.WSDL_LOCATION;
//        if (args.length > 0 && args[0] != null && !"".equals(args[0])) { 
//            File wsdlFile = new File(args[0]);
//            try {
//                if (wsdlFile.exists()) {
//                    wsdlURL = wsdlFile.toURI().toURL();
//                } else {
//                    wsdlURL = new URL(args[0]);
//                }
//            } catch (MalformedURLException e) {
//                e.printStackTrace();
//            }
//        }
//      
//        MsgHandler ss = new MsgHandler(wsdlURL, SERVICE_NAME);
//        MsgHandlerSoap port = ss.getMsgHandlerSoap12();  
//        
//        {
//        	log.debug("Invoking xmlString2FlatFile...");
//        java.lang.String _xmlString2FlatFile_xmlRequest = "";
//        javax.xml.ws.Holder<java.lang.Integer> _xmlString2FlatFile_xmlString2FlatFileResult = new javax.xml.ws.Holder<java.lang.Integer>();
//        javax.xml.ws.Holder<java.lang.String> _xmlString2FlatFile_tita = new javax.xml.ws.Holder<java.lang.String>();
//        port.xmlString2FlatFile(_xmlString2FlatFile_xmlRequest, _xmlString2FlatFile_xmlString2FlatFileResult, _xmlString2FlatFile_tita);
//
//        log.debug("xmlString2FlatFile._xmlString2FlatFile_xmlString2FlatFileResult=" + _xmlString2FlatFile_xmlString2FlatFileResult.value);
//        log.debug("xmlString2FlatFile._xmlString2FlatFile_tita=" + _xmlString2FlatFile_tita.value);
//
//        }
//        {
//        	log.debug("Invoking flatFile2XmlString...");
//        java.lang.String _flatFile2XmlString_msgName = "";
//        java.lang.String _flatFile2XmlString_tota = "";
//        javax.xml.ws.Holder<java.lang.Integer> _flatFile2XmlString_flatFile2XmlStringResult = new javax.xml.ws.Holder<java.lang.Integer>();
//        javax.xml.ws.Holder<java.lang.String> _flatFile2XmlString_xmlString = new javax.xml.ws.Holder<java.lang.String>();
//        port.flatFile2XmlString(_flatFile2XmlString_msgName, _flatFile2XmlString_tota, _flatFile2XmlString_flatFile2XmlStringResult, _flatFile2XmlString_xmlString);
//
//        log.debug("flatFile2XmlString._flatFile2XmlString_flatFile2XmlStringResult=" + _flatFile2XmlString_flatFile2XmlStringResult.value);
//        log.debug("flatFile2XmlString._flatFile2XmlString_xmlString=" + _flatFile2XmlString_xmlString.value);
//
//        }
//        {
//        	log.debug("Invoking submitXmlSync...");
//        fstop.ws.eai.client.SubmitXmlSync _submitXmlSync_parameters = null;
//        fstop.ws.eai.client.SubmitXmlSyncResponse _submitXmlSync__return = port.submitXmlSync(_submitXmlSync_parameters);
//        log.debug("submitXmlSync.result=" + _submitXmlSync__return);
//
//
//        }
//        {
//        	log.debug("Invoking flatFile2XmlElement...");
//        java.lang.String _flatFile2XmlElement_msgName = "";
//        java.lang.String _flatFile2XmlElement_tota = "";
//        javax.xml.ws.Holder<java.lang.Integer> _flatFile2XmlElement_flatFile2XmlElementResult = new javax.xml.ws.Holder<java.lang.Integer>();
//        javax.xml.ws.Holder<fstop.ws.eai.client.FlatFile2XmlElementResponse.RequestXmlElement> _flatFile2XmlElement_requestXmlElement = new javax.xml.ws.Holder<fstop.ws.eai.client.FlatFile2XmlElementResponse.RequestXmlElement>();
//        port.flatFile2XmlElement(_flatFile2XmlElement_msgName, _flatFile2XmlElement_tota, _flatFile2XmlElement_flatFile2XmlElementResult, _flatFile2XmlElement_requestXmlElement);
//
//        log.debug("flatFile2XmlElement._flatFile2XmlElement_flatFile2XmlElementResult=" + _flatFile2XmlElement_flatFile2XmlElementResult.value);
//        log.debug("flatFile2XmlElement._flatFile2XmlElement_requestXmlElement=" + _flatFile2XmlElement_requestXmlElement.value);
//
//        }
//        {
//        	log.debug("Invoking submitXmlString...");
//        fstop.ws.eai.client.SubmitXmlStringRequest _submitXmlString_parameters = null;
//        fstop.ws.eai.client.SubmitXmlStringResponse _submitXmlString__return = port.submitXmlString(_submitXmlString_parameters);
//        log.debug("submitXmlString.result=" + _submitXmlString__return);
//
//
//        }
//        {
//        	log.debug("Invoking submitXml...");
//        fstop.ws.eai.client.SubmitXml _submitXml_parameters = null;
//        fstop.ws.eai.client.SubmitXmlResponse _submitXml__return = port.submitXml(_submitXml_parameters);
//        log.debug("submitXml.result=" + _submitXml__return);
//
//
//        }
//        {
//        	log.debug("Invoking submitFlatFile...");
//        java.lang.String _submitFlatFile_msgName = "";
//        java.lang.String _submitFlatFile_request = "";
//        javax.xml.ws.Holder<java.lang.Integer> _submitFlatFile_submitFlatFileResult = new javax.xml.ws.Holder<java.lang.Integer>();
//        javax.xml.ws.Holder<java.lang.String> _submitFlatFile_response = new javax.xml.ws.Holder<java.lang.String>();
//        port.submitFlatFile(_submitFlatFile_msgName, _submitFlatFile_request, _submitFlatFile_submitFlatFileResult, _submitFlatFile_response);
//
//        log.debug("submitFlatFile._submitFlatFile_submitFlatFileResult=" + _submitFlatFile_submitFlatFileResult.value);
//        log.debug("submitFlatFile._submitFlatFile_response=" + _submitFlatFile_response.value);
//
//        }
//
//        System.exit(0);
//    }

}
