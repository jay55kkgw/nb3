package fstop.ws.eai.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;
/**
 * 
 * 功能說明 :定義EAIRQ的基本屬性
 *
 */
@XmlRootElement(name="REC")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class TD01RS_Data  {
	
	private String PURDATE ;
	private String POSTDATE ;
	private String DESCTXT ;
	private String CRDNAME ;
	private String CURRENCY ;
	private String SRCAMNT ;
	private String CURAMNT ;
	
	
}
