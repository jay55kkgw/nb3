package fstop.ws.eai.bean;

import java.util.LinkedList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@XmlRootElement(name="BlueStar")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class Q00001RS {

	@XmlAttribute(name="MsgName")
	private String MSGNAME;
	@XmlAttribute(name="RqUid")
	private String RQUID;
	@XmlAttribute(name="Status")
	private String STATUS;
	@XmlAttribute(name="App")
	private String APP;
	

	private String TXNTYPE ;
	private String RESULTCODE ;
	private String MSG ;
	private String RECORDS ;
	private String StatusDesc ;
	private LinkedList<Q00001RS_Data> data ;
}
