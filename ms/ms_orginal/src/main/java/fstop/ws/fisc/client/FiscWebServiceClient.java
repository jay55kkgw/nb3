package fstop.ws.fisc.client;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;

import com.systex.tbb.axis.FiscServiceAPIStub;

import fstop.util.JSONUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

@Slf4j
public final class FiscWebServiceClient implements IFiscWebServiceClient {
	
	
	@Value("${WebService.fisc.N816.URI:}")
	private String fiscN816uri;
	
	private Map<String, String> setting;

	@PostConstruct
	public void init() {
		setting = new HashMap<>();
		setting.put("fisc.webservices.uri", fiscN816uri);
	}

	public FiscWebServiceClient() {
    }

    public Response processActive(Map params) throws Exception {
    	return process(params, 1);
    }

    public Response processLost(Map params) throws Exception {
    	return process(params, 2);
    }
    
    /**
     * 呼叫WebService執行開卡/掛失
     * @param params (開卡/掛失 參數集合)
     * @param type (類別 1:開卡, 2:掛失)
     * @return String (ResponseCode)
     * @throws Exception
     */
    private Response process(Map params, int type) throws Exception {
    	String wsdlURL = StrUtils.trim((String)setting.get("fisc.webservices.uri"));
    	log.debug("wsdlURL >>{}",wsdlURL);
    	log.debug("FiscWebService Invoking process...");
        FiscServiceAPIStub client = new FiscServiceAPIStub(wsdlURL);
        FiscServiceAPIStub.ProcessRequest processRequest = new FiscServiceAPIStub.ProcessRequest();
        FiscServiceAPIStub.FiscRequest fiscRequest = buildRequest(params, type);
        log.debug(ESAPIUtil.vaildLog("FiscWebService FiscRequest: " + JSONUtils.toJson(fiscRequest)));
        processRequest.setFiscRequest(fiscRequest);
        FiscServiceAPIStub.ProcessResponse procResponse = client.process(processRequest);
        log.debug(ESAPIUtil.vaildLog("FiscWebService ProcessCode: " + procResponse.getProcessCode()));
        
		Response response = new Response();
		response.setProcessCode(procResponse.getProcessCode());
		
		FiscServiceAPIStub.FiscResponse fiscResponse = null;
		if ("00".equals(procResponse.getProcessCode())) {
			fiscResponse = procResponse.getFiscResponse();
	        response.setReferenceNumber(fiscResponse.getReferenceNumber());
	        response.setAuthorizationCode(fiscResponse.getAuthorizationCode());
	        response.setResponseCode(fiscResponse.getResponseCode());
		} else {
			log.debug(ESAPIUtil.vaildLog("FiscWebService ErrorMsg: " + procResponse.getErrorMessage()));
		}
		log.debug(ESAPIUtil.vaildLog("FiscWebService ProcessResponse(JSON): " + JSONUtils.toJson(procResponse)));
		if (fiscResponse != null)
			log.debug(ESAPIUtil.vaildLog("FiscWebService FiscResponse(JSON): " + JSONUtils.toJson(fiscResponse)));
		
		log.debug("FiscWebService process done (return).");
        
        return response;
    }

	private FiscServiceAPIStub.FiscRequest buildRequest(Map params, int type) {
		FiscServiceAPIStub.FiscRequest request = new FiscServiceAPIStub.FiscRequest();
		/** 固定填值 **/
		//Message Type Indicator (MTI)
		request.setTpdu("0000000000");
		
		//Field_03 (Processing Code)
		request.setProcessingCode("000000");
		
		//Field_04 (Amount)
		request.setAmount("000000000100");
		
		//Field_11 (交易序號) 由系統產出，不需填寫
		//request.setTransactionNo("");

		//Field_22 (Pos Entry Mode)
		request.setPosEntryMode("0012");
		
		//Field_24 (Network ID)
		request.setNetworkId("0011");
		
		//Field_25 (Pos Condition code)
		request.setPosConditionCode("00");

		/** 參數填值 **/
		//Field_02 (交易卡號)
		request.setCardNo(params.get("CARDNUM").toString());
		
		//Field_14 (到期日: 西元年月201509取後四碼1509)
		request.setExpiredDate(StrUtils.right(params.get("EXP_DATE").toString(), 4));
		
		//依不同類別填值 (類別 1:開卡, 2:掛失)
		switch (type) {
		case 1: //開卡
			//Field_41 (Terminal ID)
			request.setTerminalId("05010001");

			//Field_42 (Merchant ID)
			request.setMerchantId(StringUtils.rightPad("05010001", 15, ' '));

			//Field_63 (生日/密碼)
			request.setField63(StringUtils.rightPad(params.get("ACT_PASS").toString(), 6, ' '));
			break;
		
		case 2: //掛失
			//Field_41 (Terminal ID)
			request.setTerminalId("05010002");
			
			//Field_42 (Merchant ID)
			request.setMerchantId(StringUtils.rightPad("05010002", 15, ' '));
			
			//Field_63 (連動掛失Flag)
			//EPC_FLAG 1：一般信用卡, 2：悠遊卡, 3：一卡通
			switch (Integer.parseInt(params.get("EPC_FLAG").toString())) {
			case 2:
				request.setField63("Y ");
				break;
			case 3:
				request.setField63(" Y");
				break;
			default:
				request.setField63("  ");
				break;
			}
			break;
		}
		return request;
	}
    
}
