package fstop.ws.fisc.client;

import java.util.Hashtable;
import java.util.Map;

public interface IFiscWebServiceClient {

	public Response processActive(Map params) throws Exception;
	
	public Response processLost(Map params) throws Exception;
	
}
