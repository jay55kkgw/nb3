package fstop.ws;

import java.util.HashMap;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BacctVerifyConfig {

//	private static Logger logger = Logger.getLogger(BacctVerifyConfig.class);
	
	/**
A. ■轉帳購物 MID/TID :  	050014945700001/20020001                          
B. ■財金共用平台MID/TID :  	050272417980001/20010001                
C. ■全國繳費MID/TID :  	050061369690001/20030001   

目前只使用 -> 財金共用平台 線上購物

	 */
	public static Map<String, String> wsInfo = new HashMap<String, String>();

	public static ThreadLocal<Boolean> isDebug = new ThreadLocal<Boolean>() {
		public Boolean initialValue() {
			return false;
		}
	};

	public static ConfigValueProvider configValueProvider = new ConfigValueProviderImpl();
	/*
	 * 取得驗證參數
	 * VerifyCode 在 押 SHA 256 時會用到
	 */
//	public static String getValidParam() {
//		return  configValueProvider.getValidParam();
//	}


	public static String getValue(String propertieName, String defaultValue) {
		return configValueProvider.getValue(propertieName, defaultValue);
	}

	/**
	 * 取得財金給的 hostId
	 * @return
	 */
//	public static String getHostId() {
//		return getValue("hostId", "05001037934070100000001");
//	}


	/**
	 * 取得財金給的 merchantId
	 * @return
	 */
//	public static String getMerchantId() {
//		return wsInfo.get("merchantId");
//	}

	/**
	 * 取得財金給的 merchantId
	 * @return
	 */
//	public static String getTerminalId() {
//		return wsInfo.get("terminalId");
//	}
//
//	static {
//
//		Map m = JSONUtils.json2map("{ acqBank : \"050\", " +
//				"merchantId: \"050272417980001\", " +  //特店代號
//				"terminalId : \"20010001\"," +
//				//original //"validParam : \"9F8C46F5409FEFB9493594B49E98F84C\", " +
//				//encrypt //"validParam : \"89BCB5EDB1B1DD961380C27577AED398\", " +  //C65F9199
////				"validParam : \"E2254DAF2BA54EE10DC0CE2CA7FC0D87\", " +  //C65F9199
////				"hostId : \"05001037934070100000001\", " +
//				"appId : \"TBBMBAPP001\" " +
//				"}");
//
//		wsInfo.putAll(m);
//	}


//	private static void test1() {
//		Validate.isTrue("050".equals(wsInfo.get("acqBank")));
//		Validate.isTrue("050272417980001".equals(wsInfo.get("merchantId")));
//		Validate.isTrue("20010001".equals(wsInfo.get("terminalId")));
//		//Validate.isTrue("56C3DC3F16CEF7A712C309EA9DF9B13C".equals(QRCodeConfig.getValidParam()));
//		Validate.isTrue("05001037934070100000001".equals(QRCodeConfig.getHostId()));
//	}
	
//	public static void main(String[] args) {
//		
////		test1();
////		log.debug("done.");
//		
//		
//		
//		
//	}	
}
