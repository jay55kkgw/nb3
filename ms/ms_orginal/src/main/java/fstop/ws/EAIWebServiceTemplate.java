package fstop.ws;

import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Value;

import com.netbank.util.CodeUtil;
import com.netbank.util.ResultCode;

import fstop.exception.TopMessageException;
import fstop.ws.eai.bean.C900RQ;
import fstop.ws.eai.bean.C900RS;
import fstop.ws.eai.bean.Q00001RQ;
import fstop.ws.eai.bean.Q00001RS;
import fstop.ws.eai.bean.TD01RS;
import fstop.ws.eai.client.MsgHandlerSoap;
import fstop.ws.eai.client.SubmitXmlStringRequest;
import fstop.ws.eai.client.SubmitXmlStringResponse;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 功能說明 :EAI WebService 
 *
 */
@Slf4j
public class EAIWebServiceTemplate {


	@Value("${eai.uri}")
	private String webServiceUri = "https://neaint.tbb.com.tw/blueStar/MsgHandler.asmx";
	

	@Value("${cert_path}")
	String cert_path = "C:\\trust\\focastestcer";
	@Value("${soci}")
	String soci = "changeit";
	
	@Value("${eai_isCNcheck:Y}")
	String isCNcheck="Y" ;
	
//	private String webServiceUri = "http://127.0.0.1:9980/blueStar/MsgHandler.asmx?WSDL";

	
	/**
	 * WebService 初始化
	 * @param serviceUrl
	 * @param clz
	 * @return
	 */
	public <T> T createService(String serviceUrl, Class<T> clz) {
		int ConnectionTimeout = 5;
		int receiveTimeout = 30;
		T service = null ;
		try {
			KeyStore trustStore = KeyStore.getInstance("JKS");
			trustStore.load(new FileInputStream(new File(cert_path)), soci.toCharArray());
	
			// Create and initialize the truststore manager
			TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
			tmf.init(trustStore);
	
			// Create and initialize the SSL context
//			SSLContext sslContext = SSLContext.getInstance("SSL");
			SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
			sslContext.init(null, tmf.getTrustManagers(), new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
	
			JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
			log.info("createService.serviceUrl>>{}",serviceUrl);
			
			factory.getInInterceptors().add(new LoggingInInterceptor());
			factory.getOutInterceptors().add(new LoggingOutInterceptor());
			factory.setServiceClass(clz);
			factory.setAddress(serviceUrl);
			service = (T) factory.create();
			log.info("ClientProxy...");
			

			
			Client client = ClientProxy.getClient(service);
			
			HTTPConduit conduit = (HTTPConduit) client.getConduit();
			HTTPClientPolicy policy = conduit.getClient();
			policy.setConnectionTimeout(ConnectionTimeout * 1000);
			policy.setReceiveTimeout(receiveTimeout * 1000);
			
			conduit.setClient(policy);
	
			// Set the TLS client parameters
			TLSClientParameters parameters = new TLSClientParameters();
			parameters.setSSLSocketFactory(sslContext.getSocketFactory());
			
			
			if("Y".equals(isCNcheck)) {
				parameters.setDisableCNCheck(true);
			}
			conduit.setTlsClientParameters(parameters);
			
//			setConnectionTimeout(service, ConnectionTimeout, receiveTimeout);
//			Client client = ClientProxy.getClient(service);
//			log.debug("client>>{}",client);
//			HTTPConduit conduit = (HTTPConduit) client.getConduit();
//			HTTPClientPolicy policy = conduit.getClient();
//			policy.setConnectionTimeout(ConnectionTimeout * 1000);
//			policy.setReceiveTimeout(receiveTimeout * 1000);
//			conduit.setClient(policy);
		} catch (Throwable e) {
			log.error("createService.Exception>>{}",e);
			throw TopMessageException.create(ResultCode.FE0004);
		}
		log.debug("service>>{}",service);
		return service;
	}
	
	/**
	 * 電文收送API
	 * @param source
	 * @param target
	 * @return
	 */
	public <T> T sendAndReceive(Object source, Class target) {
		log.info("webServiceUri>>{}",webServiceUri);
		int rtn = 1;
		String telcomString = "";
		SubmitXmlStringResponse rs =null;
		T t = null;
		try {
			telcomString =  CodeUtil.marshalXML(source);
			log.debug("telcomString>>\n{}",telcomString);
			MsgHandlerSoap client = createService(webServiceUri,  MsgHandlerSoap.class);
			SubmitXmlStringRequest req = new SubmitXmlStringRequest();
			req.setSRequest(telcomString);
//			setConnectionTimeout(client, ConnectionTimeout, receiveTimeout);
			rs = client.submitXmlString(req);
			log.debug("rs>>{}",rs);
			log.debug("rn>>{}",rs.getSubmitXmlStringResult());
			if(rs != null && rs.getSubmitXmlStringResult() ==0) {
				log.debug("rs.getSResponse()>>{}",rs.getSResponse());
				t =CodeUtil.unmarshalXML(target, rs.getSResponse());
			}else {
				throw TopMessageException.create(ResultCode.FE0005);
			}
		} catch (Exception e) {
			log.error("sendAndReceive error >> {}",e);
			throw TopMessageException.create(ResultCode.FE0004);
		}
		
		return t;
	}

	/**
	 * Time out設定	
	 * @param port
	 * @param ConnectionTimeout
	 * @param receiveTimeout
	 */
	public  void setConnectionTimeout(Object port , int ConnectionTimeout ,int receiveTimeout){
		Client client = ClientProxy.getClient(port);
		HTTPConduit http = (HTTPConduit) client.getConduit();
		HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();
		httpClientPolicy.setConnectionTimeout(ConnectionTimeout * 1000);
		httpClientPolicy.setReceiveTimeout(receiveTimeout * 1000);
		http.setClient(httpClientPolicy);
		
	}	
	
//	public static void main(String[] args) {
//		EAIWebServiceTemplate eaiwebservicetemplate = new EAIWebServiceTemplate();
//		String txncnt = "1";
//		HashMap<String, String> request = new HashMap<String,String>();
//		/*
////		baseMap.put("TXNCOD", String.format("%-8s", "NB01"));
//		request.put("MSGNAME", "TD01");
//		request.put("TXNIDN", "TD01");
////		txncnt = StrUtils.isEmpty("") ? String.format("%04d", Integer.parseInt(txncnt)):String.format("%04d", Integer.parseInt(request.get("TXNCNT")));
//		request.put("TXNCNT", "1");
////		request.put("CUSIDN", "A123456814");
//		request.put("CUSIDN", "W100089655");
//		request.put("PROPERTY", "A");
////		request.put("CARDNUM", "5122960000486107");
//		request.put("CARDNUM", "5588665000001106");
////		request.put("USERDATA", String.format("%49s", ""));
//		request.put("PROPERTY", "A");
//		TD01RQ rq  = CodeUtil.objectCovert(TD01RQ.class, request);
//		TD01RS rs = null;
//		rs = eaiwebservicetemplate.sendAndReceive (rq, TD01RS.class);
//		log.debug("TD01.rs>>"+rs.toString());
//		for(TD01RS_Data data  :rs.getREC()) {
//			log.debug("datastr>>"+data.toString());
//			log.debug("getCRDNAME>>"+data.getCRDNAME());
//		}
//		*/
//		request.put("MSGNAME", "C900");
//		request.put("TXNCOD",null);
//		request.put("PRIACN", "4938250000728113");
//		request.put("STAN", "205305");
//		request.put("DATE", "08"+new DateTime().toString("MMdd"));//080619
//		request.put("TIME", new DateTime().toString("HHmmss"));//205305
//		request.put("DATE4", new DateTime().toString("MMdd"));//0619
//		request.put("FUNCODE", "305");
//		request.put("INPREQLEN", "175");
//		request.put("TRACODE", "CCCH");
//		request.put("SERVCODE", "IMSNB");
//		request.put("TERMID", String.format("%-8s", ""));
//		request.put("REQTYPE", "NEW");
//		request.put("NUMTRA", "00");
//		request.put("SEQ", "171");
//		request.put("NAME", String.format("%-30s", ""));//沒有就是30空白
//		request.put("DATEBRI", "00000000");
//		request.put("IDNUM", String.format("%-15s", ""));//15空白
//		request.put("OFFPHONE", String.format("%-18s", ""));
//		request.put("HOUPHONE", String.format("%-18s", ""));
//		request.put("MOTMAINAME", String.format("%-30s", ""));
//		request.put("REASON"," ");
//		request.put("NEWPIN", String.format("%-16s", ""));
//		request.put("PERID", String.format("%-16s", "A123456814"));//補16空白
//		C900RQ c900  = CodeUtil.objectCovert(C900RQ.class, request);
//		C900RS c900RS = eaiwebservicetemplate.sendAndReceive (c900, C900RS.class);
//		log.debug("c900RS.rs>>"+c900RS.getTOTA());
////		request.put("MSGNAME", "CK01");
////		request.put("TXNIDN", "CK01");
////		CK01RQ crq  = CodeUtil.objectCovert(CK01RQ.class, request);
////		CK01RS crs = null;
////		crs = eaiwebservicetemplate.sendAndReceive (crq, CK01RS.class);
////		
////		log.debug("CK01.rs>>"+crs.toString());
//		
//		
//		
//	} 
	
//	public static void main(String[] args) {
//		Q00001RQ source = new Q00001RQ();
//		source.setMSGNAME("Q00001");
//		source.setAPP("XML");
//		source.setSYSId("CPBMB");
//		source.setPWD("1234");
//		source.setTYPE("01");
//		source.setTXNTYPE("Q00001");
//		source.setIDentity("A123456814");
//		source.setMAC("");
//		String telcomString = "";
//		telcomString =  CodeUtil.marshalXML(source);
//		log.error("telcomString>>\n{}",telcomString);
//		EAIWebServiceTemplate eaiwebservicetemplate = new EAIWebServiceTemplate();
//		Q00001RS rs = eaiwebservicetemplate.sendAndReceive(source, Q00001RS.class);
//		log.error("RS >> {}", rs);
//	}
}
