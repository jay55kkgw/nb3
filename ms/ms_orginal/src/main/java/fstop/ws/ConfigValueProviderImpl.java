package fstop.ws;

import java.io.File;
import java.util.Properties;

import org.apache.log4j.Logger;

import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import fstop.va.ServerConfig;
import lombok.extern.slf4j.Slf4j;
@Slf4j
public class ConfigValueProviderImpl implements ConfigValueProvider {

    private static Logger logger = Logger.getLogger(ConfigValueProviderImpl.class);

    /**
     * 取得驗證參數
     * @return
    */ 
//    public String getValidParam() {
//
//        QrCodeParamDao dao = (QrCodeParamDao) SpringBeanFactory.getBean("qrCodeParamDao");
//        QRCODEPARAM param = dao.findByName("validParam");
//
//        if(param != null) {
//            return param.getVAL();
//        }
//        else {
//            throw new RuntimeException("Can't found QR Code 'validParam' From DB.");
//        }
//    }

    private static Properties localProperties = null;

    /**
     * 取得acctVerify Gateway系統參數
     * @param name
     * @param defalutValue
     * @return
     */
    public String getValue(String name, String defalutValue) {
        loadPropertyFromFile();
        String val = localProperties.getProperty(name);
        logger.info(" ConfigValueProviderImpl getValue method val:"+val);
        if(StrUtils.isEmpty(val)) {//如GateWay URL為空值，帶預設URL
            return defalutValue;
        }
        return val;
    }

    /*
        * load properties file
    */
    public static void loadPropertyFromFile() {

        if(localProperties != null)
            return;

        String fileName = "bacctver_gw.properties";

        localProperties = new Properties();
        logger.debug("---------------loadPropertyFromFile in(" + fileName  + ")---------------");

        try {
            ServerConfig serverConfig = (ServerConfig) SpringBeanFactory.getBean("serverConfig");

            localProperties.load(serverConfig.getgetResourceAsStream(File.separator +"WEB-INF" + File.separator + fileName));
        }
        catch (Exception e) {
        	log.error("loadPropertyFromFile error>>{}", e);
        }
        logger.debug("---------------loadPropertyFromFile out(" + fileName  + ")---------------");
    }
}
