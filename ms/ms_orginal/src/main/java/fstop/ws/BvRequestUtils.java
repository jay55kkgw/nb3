package fstop.ws;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.util.Optional;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;

import com.netbank.util.ESAPIUtil;

import fstop.exception.QrTopMessageException;
import fstop.util.IPUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BvRequestUtils {
//    private static Logger logger = Logger.getLogger(BvRequestUtils.class);
    //private String baseUrl = "https://www.focas-test.fisc.com.tw/FOCAS_WS/API20/QRP/V2/";
    //private static String defaultBaseUrl = "http://localhost:8080/QRPGateway/B";  //for local test G/W

    //private static String defaultBaseUrl = "http://10.16.22.177:8080/FiscGateway/B";
    private static String defaultBaseUrlTest = "https://www.focas-test.fisc.com.tw/FOCAS_WS/API20/V1/FISCII/";//測試環境URL
    private static String defaultBaseUrlPro ="https://www.focas.fisc.com.tw/FOCAS_WS/API20/V1/FISCII/";//營運環境URL

//    public static void disableCertificateValidation() {
//
//        // Create a trust manager that does not validate certificate chains
//
//        TrustManager[] trustAllCerts = new TrustManager[] {
//
//                new X509TrustManager() {
//
//                    public X509Certificate[] getAcceptedIssuers1() {
//
//                        return new X509Certificate[0];
//
//                    }
//
//                    public void checkClientTrusted(X509Certificate[] certs, String authType) {
//                    }
//
//                    public void checkServerTrusted(X509Certificate[] certs, String authType) {
//                    }
//
//                    public void checkClientTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
//                            throws CertificateException {
//                        // TODO Auto-generated method stub
//
//                    }
//
//                    public void checkServerTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
//                            throws CertificateException {
//                        // TODO Auto-generated method stub
//
//                    }
//
//                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
//                        // TODO Auto-generated method stub
//                        return null;
//                    }
//
//                } };
//
//        // Ignore differences between given hostname and certificate hostname
//
//        HostnameVerifier hv = new HostnameVerifier() {
//
//            public boolean verify(String hostname, SSLSession session) {
//                return true;
//            }
//
//        };
//
//        // Install the all-trusting trust manager
//
//        try {
//
//            SSLContext sc = SSLContext.getInstance("SSL");
//
//            sc.init(null, trustAllCerts, new SecureRandom());
//
//            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
//
//            HttpsURLConnection.setDefaultHostnameVerifier(hv);
//
//        } catch (Exception e) {
//        }
//
//    }

    public static String doRequest(String urlAddress, String paramXml, Integer readTimeout) throws Exception {

        log.info("\nurlAddress: " +urlAddress+ "\n QR request:\n" + paramXml );

        URL baseURL = new URL(urlAddress);
        HttpURLConnection con = null;
        BufferedReader reader = null;
        try {
            con = (HttpURLConnection) baseURL.openConnection();
            con.setDoInput(true);
            con.setDoOutput(true);
            con.setUseCaches(false);
    		log.info("set timeout >> {}", readTimeout * 1000);
            con.setReadTimeout(readTimeout * 1000);

            con.setRequestMethod("POST");
            con.setFollowRedirects(true);


            con.setRequestProperty("Accept", "application/xml");
            con.setRequestProperty("Content-length", String.valueOf(paramXml.length()));
            con.setRequestProperty("Content-Type", "application/xml");

            OutputStream os = con.getOutputStream();
            os.write(paramXml.getBytes("UTF-8"));
            os.flush();

            int code = con.getResponseCode();

            log.info("Code=" + code);
            log.info("resp msg=" + con.getResponseMessage() + "");

            if(code != 200) {
                throw new QrTopMessageException("Q099", "無法執行acctVerify交易.");
                //throw new IllegalAccessException("doRequest retCode != 200 , url " + urlAddress);
            }

            StringBuilder result = new StringBuilder();

            InputStream is = con.getInputStream();

            reader = new BufferedReader(new InputStreamReader(is, "utf-8"));
            String line;

            while ((line = reader.readLine()) != null) {
                result.append(line);
            }

            String _sResponse = result.toString().trim();
            log.info(ESAPIUtil.vaildLog("\nQR request:\n" + paramXml + "\nQR response:\n" + _sResponse));

            return _sResponse;
        }
        catch(Exception e) {
            log.error("doRequest Error ! (url : " + urlAddress+ ")", e);
            throw new QrTopMessageException("Q099", "無法執行acct verify交易.", e);
        }
        finally {
            if(con != null) {
                con.disconnect();
            }
            if(reader != null) {
                try {
                    reader.close();
                }
                catch(Exception e){}

            }
        }
    }
    
    /**
     * 
     * @param urlAddress
     * @param paramXml
     * @param trustStorePath 信任憑證庫路徑及檔案
     * @param soci 信任憑證庫密碼
     * @return
     * @throws Exception
     */
    public static String doRequest(String urlAddress, String paramXml , String trustStorePath ,String soci, Integer readTimeout) throws Exception {
    	
    	log.info("\nurlAddress: " +urlAddress+ "\n QR request:\n" + paramXml );
    	
    	URL baseURL = new URL(urlAddress);
    	HttpURLConnection con = null;
    	//Fix Improper Restriction of XXE Ref
    	BufferedReader reader = null;
//    	XMLStreamReader reader = null;
    	try {
//    		String trustStorePath = "D:\\mock_data\\trust\\focastestcer";
//    		String soci = "changeit";
    		setSSLContext(trustStorePath, soci);
    		con = (HttpURLConnection) baseURL.openConnection();
    		con.setDoInput(true);
    		con.setDoOutput(true);
    		con.setUseCaches(false);
    		log.info("set timeout >> {}", readTimeout * 1000);
    		con.setReadTimeout(readTimeout * 1000);
    		
    		con.setRequestMethod("POST");
    		con.setFollowRedirects(true);
    		
    		
    		con.setRequestProperty("Accept", "application/xml");
    		con.setRequestProperty("Content-length", String.valueOf(paramXml.length()));
    		con.setRequestProperty("Content-Type", "application/xml");
    		
    		OutputStream os = con.getOutputStream();
    		os.write(paramXml.getBytes("UTF-8"));
    		os.flush();
    		
    		int code = con.getResponseCode();
    		
    		log.info("Code=" + code);
    		log.info("resp msg=" + con.getResponseMessage() + "");
    		
    		if(code != 200) {
    			throw new QrTopMessageException("Q099", "無法執行acctVerify交易.");
    			//throw new IllegalAccessException("doRequest retCode != 200 , url " + urlAddress);
    		}
    		
    		StringBuilder result = new StringBuilder();
    		
    		InputStream is = con.getInputStream();
    		
    		//Improper Restriction of XXE Ref
    		
//    		XMLInputFactory factory = XMLInputFactory.newFactory();
//    		factory.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, false);
//    		factory.setProperty(XMLInputFactory.SUPPORT_DTD, false);
//
//    		reader = (BufferedReader) factory.createXMLStreamReader(is,"utf-8");
//    		
//    		String line;
//    		
//    		while ((line = reader.readLine()) != null) {
//    			result.append(line);
//    		}

//old  		
    		reader = new BufferedReader(new InputStreamReader(is, "utf-8"));
    		String line;
    		
    		while ((line = reader.readLine()) != null) {
    			result.append(line);
    		}
    		
    		String _sResponse = result.toString().trim();
    		log.info(ESAPIUtil.vaildLog("\nQR request:\n" + paramXml + "\nQR response:\n" + _sResponse));
    		
    		return _sResponse;
    	}
    	catch(Exception e) {
    		log.error("doRequest Error ! (url : " + urlAddress+ ")", e);
    		throw new QrTopMessageException("Q099", "無法執行acct verify交易.", e);
    	}
    	finally {
    		if(con != null) {
    			con.disconnect();
    		}
    		if(reader != null) {
    			try {
    				reader.close();
    			}
    			catch(Exception e){}
    			
    		}
    	}
    }
    
    
    /**
	 * 設定 SSL Context, 如是自簽伺服器憑證, 要把對方的 public key 用 Keytool 匯入
	 * trust keystore
	 * @param trustStorePath		trust key store file		
	 * @param soci					avoid CheckMarks		
	 * @throws IOException
	 */
    public static void setSSLContext(String trustStorePath, String soci) throws IOException {
		FileInputStream keyStoreStream = null;
		FileInputStream trustStoreStream = null;
		try {
			log.info("setSSLContext...");
			log.debug("trustStorePath>>{} , soci>>{}",trustStorePath , soci);
			KeyStore trustStore = KeyStore.getInstance("JKS");
			trustStoreStream = new FileInputStream(new File(trustStorePath));
			trustStore.load(trustStoreStream, soci.toCharArray());
            
			TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
			tmf.init(trustStore);
			TrustManager[] tms = tmf.getTrustManagers();

			SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
			sslContext.init(null, tms, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
			log.info("setSSLContext...end");
		} catch (Exception e) {
			log.error("setSSLContext Error",e);
		} finally {
			if (keyStoreStream != null)
				keyStoreStream.close();
			if (trustStoreStream != null)
				trustStoreStream.close();
		}
    }
    
    
    //
    public static Optional<BacctVerifyResp> doRequest(BacctVerifyReq acctVerReq, String defaultBaseUrlPass, Integer readTimeout) {


        String action = "acctVerify";
        
        String localIP = IPUtils.getLocalIp();//取得IP位址
        String defaultBaseUrl ="";
        String propertiesTag="";//Properties檔案內TAG　NAME
        //TODO:先把正式機的URL註解，等之後改成環境變數
//        if(localIP.equals("10.16.22.17")){//測試環境
        	defaultBaseUrl = defaultBaseUrlPass;
        	propertiesTag="baseUrlT";
//        }else{							  //營運環境	
//        	defaultBaseUrl = defaultBaseUrlPro;
//        	propertiesTag="baseUrlP";
//        }
        log.info("BvRequestUtils defaultBaseUrl:" + defaultBaseUrl + "\r\n===============");
        log.info("BvRequestUtils propertiesTag:" + propertiesTag + "\r\n===============");
        //TODO:先不要呼叫BacctVerifyConfig.getValue，等之後再調
        String urlAddress = defaultBaseUrl + action;
//        String urlAddress = BacctVerifyConfig.getValue(propertiesTag, defaultBaseUrl) + action;
        
        
        log.info("BvRequestUtils urlAddress:" + urlAddress + "\r\n===============");
        try {
            String paramsXml = JAXBUtils.marshaller(acctVerReq);
            log.info(ESAPIUtil.vaildLog("==paramsXml XML ===============\r\n" + paramsXml + "\r\n==============="));
            String retXml = doRequest(urlAddress, paramsXml, readTimeout);
            log.info(ESAPIUtil.vaildLog("==retXml XML ===============\r\n" + retXml + "\r\n==============="));
            retXml = retXml.replaceAll(" xmlns=\".+?\"", "");
            log.info(ESAPIUtil.vaildLog("==bacctVerify return XML ===============\r\n" + retXml + "\r\n==============="));
            BacctVerifyResp req = JAXBUtils.unmarshaller(retXml, BacctVerifyResp.class);

            return Optional.of(req);
        } catch (Exception e) {
            log.error("doRequest(BacctVerifyReq) error.", e);
        }

        return Optional.empty();
    }
    
    /**
     * 
     * @param acctVerReq
     * @param defaultBaseUrlPass
     * @param trustStorePath
     * @param soci
     * @return
     */
    public static Optional<BacctVerifyResp> doRequest(BacctVerifyReq acctVerReq, String defaultBaseUrlPass , String trustStorePath ,String soci, Integer readTimeout) {
    	
    	
    	String action = "acctVerify";
    	
    	String localIP = IPUtils.getLocalIp();//取得IP位址
    	String defaultBaseUrl ="";
    	String propertiesTag="";//Properties檔案內TAG　NAME
    	//TODO:先把正式機的URL註解，等之後改成環境變數
//        if(localIP.equals("10.16.22.17")){//測試環境
    	defaultBaseUrl = defaultBaseUrlPass;
    	propertiesTag="baseUrlT";
//        }else{							  //營運環境	
//        	defaultBaseUrl = defaultBaseUrlPro;
//        	propertiesTag="baseUrlP";
//        }
    	log.info("BvRequestUtils defaultBaseUrl:" + defaultBaseUrl + "\r\n===============");
    	log.info("BvRequestUtils propertiesTag:" + propertiesTag + "\r\n===============");
    	//TODO:先不要呼叫BacctVerifyConfig.getValue，等之後再調
    	String urlAddress = defaultBaseUrl + action;
//        String urlAddress = BacctVerifyConfig.getValue(propertiesTag, defaultBaseUrl) + action;
    	
    	
    	log.info("BvRequestUtils urlAddress:" + urlAddress + "\r\n===============");
    	try {
    		String paramsXml = JAXBUtils.marshaller(acctVerReq);
    		log.info("==paramsXml XML ===============\r\n" + paramsXml + "\r\n===============");
    		String retXml = doRequest(urlAddress, paramsXml ,trustStorePath , soci, readTimeout);
    		log.info(ESAPIUtil.vaildLog("==retXml XML ===============\r\n" + retXml + "\r\n==============="));
    		retXml = retXml.replaceAll(" xmlns=\".+?\"", "");
    		log.info("==bacctVerify return XML ===============\r\n" + retXml + "\r\n===============");
    		BacctVerifyResp req = JAXBUtils.unmarshaller(retXml, BacctVerifyResp.class);
    		
    		return Optional.of(req);
    	} catch (Exception e) {
    		log.error("doRequest(BacctVerifyReq) error.", e);
    	}
    	
    	return Optional.empty();
    }

//    public static Optional<QrpPurchaseAuthResp> doRequest(QrpPurchaseAuthReq qrReq) {
//
//
//        String action = "purchaseAuth";
//        String urlAddress = QRCodeConfig.getValue("baseUrl", defaultBaseUrl) + action;
//        try {
//            String paramsXml = JAXBUtils.marshaller(qrReq);
//            String retXml = doRequest(urlAddress, paramsXml);
//            logger.info("==purchaseAuth return XML ===============\r\n" + retXml + "\r\n===============");
//            retXml = retXml.replaceAll(" xmlns=\".+?\"", "");
//            QrpPurchaseAuthResp req = JAXBUtils.unmarshaller(retXml, QrpPurchaseAuthResp.class);
//
//            return Optional.of(req);
//        } catch (Exception e) {
//            logger.error("doRequest(QrpPurchaseAuthReq)  error.", e);
//        }
//
//        return Optional.empty();
//    }


//    public static Optional<QrpBillAuthResp> doRequest(QrpBillAuthReq qrReq) {
//
//
//        String action = "billAuth";
//        String urlAddress = QRCodeConfig.getValue("baseUrl", defaultBaseUrl) + action;
//        try {
//            String paramsXml = JAXBUtils.marshaller(qrReq);
//            logger.info("==billAuth request XML ===============\r\n" + paramsXml + "\r\n===============");
//            String retXml = doRequest(urlAddress, paramsXml);
//            logger.info("==billAuth return XML ===============\r\n" + retXml + "\r\n===============");
//            retXml = retXml.replaceAll(" xmlns=\".+?\"", "");
//            QrpBillAuthResp req = JAXBUtils.unmarshaller(retXml, QrpBillAuthResp.class);
//
//            return Optional.of(req);
//        } catch (Exception e) {
//            logger.error("doRequest(QrpBillAuthResp)  error.", e);
//        }
//
//        return Optional.empty();
//    }


//    public static void main(String[] args) throws Exception {
////        String retXml = "<?xml version='1.0' encoding='UTF-8'?><QrpBillAuthResp xmlns=\"http://www.focas.fisc.com.tw/QRP/billAuth\"><mti>0210</mti><cardNumber>0000000762963329</cardNumber><processingCode>007000</processingCode><amt>000001500000</amt><systemDateTime>1129190007</systemDateTime><traceNumber>753164</traceNumber><acqBank>462</acqBank><srrn>009487802667</srrn><responseCode>0024</responseCode><terminalId>10001001</terminalId><merchantId>462263015610005</merchantId><verifyCode>AC29BA1845143BEFB9F6D6631782AC0929A7EAABD9C410FB91AF52FD1A12085B</verifyCode></QrpBillAuthResp>";
////
////        retXml = retXml.replaceAll(" xmlns=\".+?\"", "");
////        QrpBillAuthResp req = JAXBUtils.unmarshaller(retXml, QrpBillAuthResp.class);
////
////        log.debug(JSONUtils.toJson(req));
//
//
//    }
}
