package fstop.ws;

import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.SecureRandom;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;
import com.netbank.util.ResultCode;

import fstop.exception.TopMessageException;
import fstop.ws.aml.newclient.SearchRequestSoapNew;
import fstop.ws.aml.newclient.SearchResponseSoapNewZH;
import fstop.ws.aml.newclient.WlsrWsApplicationNewZH;
import fstop.ws.bh.client.AIOWebServiceSoap;
import fstop.ws.ebm.bean.GetBillInfoResponse;
import fstop.ws.ebm.bean.GetBillInfoResponseData;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AIOWebServiceTemplate {

	private Logger logger = Logger.getLogger(getClass());

	@Value("${AIOEBM_URI:https://ebm01.ebpp.com.tw:4434/AIOEBMWebService/TBBWebService.asmx?wsdl}")
	private String webServiceUri = "https://ebm01.ebpp.com.tw:4434/AIOEBMWebService/TBBWebService.asmx?wsdl";
	

	@Value("${cert_path}")
	String cert_path = "C:\\trust\\focastestcer";
	@Value("${soci}")
	String soci = "changeit";
	@Value("${AIOEBM_ReceiveTimeout:300}")
	private Integer webService_ReceiveTimeout = 300;

	private  <T> T createService(String serviceUrl, Class<T> clz) {
		T service = null;
		try
		{
			KeyStore trustStore = KeyStore.getInstance("JKS");
			trustStore.load(new FileInputStream(new File(cert_path)), soci.toCharArray());
	
			// Create and initialize the truststore manager
			TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
			tmf.init(trustStore);
	
			// Create and initialize the SSL context
			SSLContext sslContext = SSLContext.getInstance("SSL");
			sslContext.init(null, tmf.getTrustManagers(), new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
	
			
			JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
			factory.getInInterceptors().add(new LoggingInInterceptor());
			factory.getOutInterceptors().add(new LoggingOutInterceptor());
			factory.setServiceClass(clz);
	
			factory.setAddress(serviceUrl);
			
			service = (T) factory.create();
			
			Client client = ClientProxy.getClient(service);
			
			HTTPConduit conduit = (HTTPConduit) client.getConduit();
			HTTPClientPolicy policy = conduit.getClient();
			policy.setReceiveTimeout(webService_ReceiveTimeout * 1000);
			
			conduit.setClient(policy);
	
			// Set the TLS client parameters
			TLSClientParameters parameters = new TLSClientParameters();
			parameters.setSSLSocketFactory(sslContext.getSocketFactory());
			conduit.setTlsClientParameters(parameters);
		}
		catch (Exception e)
		{
			log.error(e.toString());
		}
		
		return service;
	}
	/**
	 * 電文收送API
	 * @param source
	 * @return
	 */
	public GetBillInfoResponseData getBillInfo(String cUkey) {
		log.info("webServiceUri>>{}",webServiceUri);
		GetBillInfoResponseData xml;
		try {
			AIOWebServiceSoap client = createService(webServiceUri,  AIOWebServiceSoap.class);
			xml = client.GetBillInfo(cUkey);
		} catch (Exception e) {
			log.error("sendAndReceive error >> {}",e);
			throw TopMessageException.create(ResultCode.FE0004);
		}
		
		return xml;
	}

	/**
	 * 電文收送API
	 * @param source
	 * @return
	 */
	public GetBillInfoResponseData SetBillSend(String cUkey,String cDate) {
		log.info("webServiceUri>>{}",webServiceUri);
		GetBillInfoResponseData xml;
		try {
			AIOWebServiceSoap client = createService(webServiceUri,  AIOWebServiceSoap.class);
			xml = client.SetBillSend(cUkey, cDate);
		} catch (Exception e) {
			log.error("sendAndReceive error >> {}",e);
			throw TopMessageException.create(ResultCode.FE0004);
		}
		
		return xml;
	}

	public static void main(String[] args) {
		AIOWebServiceTemplate eaiwebservicetemplate = new AIOWebServiceTemplate();
		GetBillInfoResponseData result = eaiwebservicetemplate.getBillInfo("A123456814");
		log.error("{}",result);
	} 
	
}
