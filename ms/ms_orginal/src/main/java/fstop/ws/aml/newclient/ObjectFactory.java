
package fstop.ws.aml.newclient;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the fstop.ws.aml.newclient package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SearchResponseSoap_QNAME = new QName("http://component.ws.wlsr.siork.com/", "SearchResponseSoap");
    private final static QName _SearchRequestSoap_QNAME = new QName("http://component.ws.wlsr.siork.com/", "searchRequestSoap");
    private final static QName _PolicyContextException_QNAME = new QName("http://component.ws.wlsr.siork.com/", "PolicyContextException");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: fstop.ws.aml.newclient
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link MatchResultXmlNewZH }
     * 
     */
    public MatchResultXmlNewZH createMatchResultXmlNewZH() {
        return new MatchResultXmlNewZH();
    }

    /**
     * Create an instance of {@link ListFilterXml }
     * 
     */
    public ListFilterXml createListFilterXml() {
        return new ListFilterXml();
    }

    /**
     * Create an instance of {@link SearchRequestSoapNew }
     * 
     */
    public SearchRequestSoapNew createSearchRequestSoapNew() {
        return new SearchRequestSoapNew();
    }

    /**
     * Create an instance of {@link SearchResponseSoapNewZH }
     * 
     */
    public SearchResponseSoapNewZH createSearchResponseSoapNewZH() {
        return new SearchResponseSoapNewZH();
    }

    /**
     * Create an instance of {@link PolicyContextException }
     * 
     */
    public PolicyContextException createPolicyContextException() {
        return new PolicyContextException();
    }

    /**
     * Create an instance of {@link DateOfBirthXml }
     * 
     */
    public DateOfBirthXml createDateOfBirthXml() {
        return new DateOfBirthXml();
    }

    /**
     * Create an instance of {@link BranchXml }
     * 
     */
    public BranchXml createBranchXml() {
        return new BranchXml();
    }

    /**
     * Create an instance of {@link WatchListEntryXml }
     * 
     */
    public WatchListEntryXml createWatchListEntryXml() {
        return new WatchListEntryXml();
    }

    /**
     * Create an instance of {@link EntryDateXml }
     * 
     */
    public EntryDateXml createEntryDateXml() {
        return new EntryDateXml();
    }

    /**
     * Create an instance of {@link ReferencesXml }
     * 
     */
    public ReferencesXml createReferencesXml() {
        return new ReferencesXml();
    }

    /**
     * Create an instance of {@link NameEntryXml }
     * 
     */
    public NameEntryXml createNameEntryXml() {
        return new NameEntryXml();
    }

    /**
     * Create an instance of {@link NameTokenXml }
     * 
     */
    public NameTokenXml createNameTokenXml() {
        return new NameTokenXml();
    }

    /**
     * Create an instance of {@link MatchResultXmlNewZH.Descriptions }
     * 
     */
    public MatchResultXmlNewZH.Descriptions createMatchResultXmlNewZHDescriptions() {
        return new MatchResultXmlNewZH.Descriptions();
    }

    /**
     * Create an instance of {@link MatchResultXmlNewZH.DescriptionZHs }
     * 
     */
    public MatchResultXmlNewZH.DescriptionZHs createMatchResultXmlNewZHDescriptionZHs() {
        return new MatchResultXmlNewZH.DescriptionZHs();
    }

    /**
     * Create an instance of {@link ListFilterXml.ListIds }
     * 
     */
    public ListFilterXml.ListIds createListFilterXmlListIds() {
        return new ListFilterXml.ListIds();
    }

    /**
     * Create an instance of {@link ListFilterXml.Categories }
     * 
     */
    public ListFilterXml.Categories createListFilterXmlCategories() {
        return new ListFilterXml.Categories();
    }

    /**
     * Create an instance of {@link ListFilterXml.Descriptions }
     * 
     */
    public ListFilterXml.Descriptions createListFilterXmlDescriptions() {
        return new ListFilterXml.Descriptions();
    }

    /**
     * Create an instance of {@link SearchRequestSoapNew.Names }
     * 
     */
    public SearchRequestSoapNew.Names createSearchRequestSoapNewNames() {
        return new SearchRequestSoapNew.Names();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchResponseSoapNewZH }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://component.ws.wlsr.siork.com/", name = "SearchResponseSoap")
    public JAXBElement<SearchResponseSoapNewZH> createSearchResponseSoap(SearchResponseSoapNewZH value) {
        return new JAXBElement<SearchResponseSoapNewZH>(_SearchResponseSoap_QNAME, SearchResponseSoapNewZH.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchRequestSoapNew }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://component.ws.wlsr.siork.com/", name = "searchRequestSoap")
    public JAXBElement<SearchRequestSoapNew> createSearchRequestSoap(SearchRequestSoapNew value) {
        return new JAXBElement<SearchRequestSoapNew>(_SearchRequestSoap_QNAME, SearchRequestSoapNew.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PolicyContextException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://component.ws.wlsr.siork.com/", name = "PolicyContextException")
    public JAXBElement<PolicyContextException> createPolicyContextException(PolicyContextException value) {
        return new JAXBElement<PolicyContextException>(_PolicyContextException_QNAME, PolicyContextException.class, null, value);
    }

}
