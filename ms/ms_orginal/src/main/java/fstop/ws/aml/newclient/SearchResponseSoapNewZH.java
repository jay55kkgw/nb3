
package fstop.ws.aml.newclient;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>searchResponseSoapNewZH complex type 的 Java 類別.
 * 
 * <p>下列綱要片段會指定此類別中包含的預期內容.
 * 
 * <pre>
 * &lt;complexType name="searchResponseSoapNewZH"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="referenceId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="hit" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="matches" type="{http://component.ws.wlsr.siork.com/}matchResultXmlNewZH" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="groupId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "searchResponseSoapNewZH", propOrder = {
    "referenceId",
    "hit",
    "matches",
    "groupId"
})
public class SearchResponseSoapNewZH {

    protected String referenceId;
    protected boolean hit;
    protected List<MatchResultXmlNewZH> matches;
    @XmlElement(required = true)
    protected String groupId;

    /**
     * 取得 referenceId 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenceId() {
        return referenceId;
    }

    /**
     * 設定 referenceId 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenceId(String value) {
        this.referenceId = value;
    }

    /**
     * 取得 hit 特性的值.
     * 
     */
    public boolean isHit() {
        return hit;
    }

    /**
     * 設定 hit 特性的值.
     * 
     */
    public void setHit(boolean value) {
        this.hit = value;
    }

    /**
     * Gets the value of the matches property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the matches property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMatches().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MatchResultXmlNewZH }
     * 
     * 
     */
    public List<MatchResultXmlNewZH> getMatches() {
        if (matches == null) {
            matches = new ArrayList<MatchResultXmlNewZH>();
        }
        return this.matches;
    }

    /**
     * 取得 groupId 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * 設定 groupId 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

}
