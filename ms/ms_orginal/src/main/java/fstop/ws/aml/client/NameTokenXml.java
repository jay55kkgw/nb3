
package fstop.ws.aml.client;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 
 * <pre>
 * &lt;complexType name="nameTokenXml"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="nameTokenCpa" type="{http://www.w3.org/2001/XMLSchema}int" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="maxCp" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="minCp" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "nameTokenXml", propOrder = {
    "nameTokenCpa",
    "maxCp",
    "minCp"
})
public class NameTokenXml {

    @XmlElement(type = Integer.class)
    protected List<Integer> nameTokenCpa;
    protected int maxCp;
    protected int minCp;

    /**
     * Gets the value of the nameTokenCpa property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the nameTokenCpa property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNameTokenCpa().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Integer }
     * 
     * 
     */
    public List<Integer> getNameTokenCpa() {
        if (nameTokenCpa == null) {
            nameTokenCpa = new ArrayList<Integer>();
        }
        return this.nameTokenCpa;
    }

    /**
     * 
     */
    public int getMaxCp() {
        return maxCp;
    }

    /**
     * 
     */
    public void setMaxCp(int value) {
        this.maxCp = value;
    }

    /**
     * 
     */
    public int getMinCp() {
        return minCp;
    }

    /**
     * 
     */
    public void setMinCp(int value) {
        this.minCp = value;
    }

}
