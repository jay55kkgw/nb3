
package fstop.ws.aml.newclient;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>searchRequestSoapNew complex type 的 Java 類別.
 * 
 * <p>下列綱要片段會指定此類別中包含的預期內容.
 * 
 * <pre>
 * &lt;complexType name="searchRequestSoapNew"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="names"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="dateOfBirth" type="{http://component.ws.wlsr.siork.com/}dateOfBirthXml" minOccurs="0"/&gt;
 *         &lt;element name="threshold" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/&gt;
 *         &lt;element name="searchcode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="reference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="scanType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="listFilter" type="{http://component.ws.wlsr.siork.com/}listFilterXml" minOccurs="0"/&gt;
 *         &lt;element name="alert" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="branch" type="{http://component.ws.wlsr.siork.com/}branchXml" minOccurs="0"/&gt;
 *         &lt;element name="groupId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "searchRequestSoapNew", propOrder = {
    "names",
    "dateOfBirth",
    "threshold",
    "searchcode",
    "reference",
    "scanType",
    "listFilter",
    "alert",
    "branch",
    "groupId"
})
public class SearchRequestSoapNew {

    @XmlElement(required = true)
    protected SearchRequestSoapNew.Names names;
    protected DateOfBirthXml dateOfBirth;
    protected Float threshold;
    protected String searchcode;
    protected String reference;
    protected String scanType;
    protected ListFilterXml listFilter;
    protected boolean alert;
    protected BranchXml branch;
    protected String groupId;

    /**
     * 取得 names 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link SearchRequestSoapNew.Names }
     *     
     */
    public SearchRequestSoapNew.Names getNames() {
        return names;
    }

    /**
     * 設定 names 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchRequestSoapNew.Names }
     *     
     */
    public void setNames(SearchRequestSoapNew.Names value) {
        this.names = value;
    }

    /**
     * 取得 dateOfBirth 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link DateOfBirthXml }
     *     
     */
    public DateOfBirthXml getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * 設定 dateOfBirth 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link DateOfBirthXml }
     *     
     */
    public void setDateOfBirth(DateOfBirthXml value) {
        this.dateOfBirth = value;
    }

    /**
     * 取得 threshold 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getThreshold() {
        return threshold;
    }

    /**
     * 設定 threshold 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setThreshold(Float value) {
        this.threshold = value;
    }

    /**
     * 取得 searchcode 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSearchcode() {
        return searchcode;
    }

    /**
     * 設定 searchcode 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSearchcode(String value) {
        this.searchcode = value;
    }

    /**
     * 取得 reference 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReference() {
        return reference;
    }

    /**
     * 設定 reference 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReference(String value) {
        this.reference = value;
    }

    /**
     * 取得 scanType 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScanType() {
        return scanType;
    }

    /**
     * 設定 scanType 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScanType(String value) {
        this.scanType = value;
    }

    /**
     * 取得 listFilter 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link ListFilterXml }
     *     
     */
    public ListFilterXml getListFilter() {
        return listFilter;
    }

    /**
     * 設定 listFilter 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link ListFilterXml }
     *     
     */
    public void setListFilter(ListFilterXml value) {
        this.listFilter = value;
    }

    /**
     * 取得 alert 特性的值.
     * 
     */
    public boolean isAlert() {
        return alert;
    }

    /**
     * 設定 alert 特性的值.
     * 
     */
    public void setAlert(boolean value) {
        this.alert = value;
    }

    /**
     * 取得 branch 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link BranchXml }
     *     
     */
    public BranchXml getBranch() {
        return branch;
    }

    /**
     * 設定 branch 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link BranchXml }
     *     
     */
    public void setBranch(BranchXml value) {
        this.branch = value;
    }

    /**
     * 取得 groupId 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * 設定 groupId 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }


    /**
     * <p>anonymous complex type 的 Java 類別.
     * 
     * <p>下列綱要片段會指定此類別中包含的預期內容.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "name"
    })
    public static class Names {

        @XmlElement(required = true)
        protected List<String> name;

        /**
         * Gets the value of the name property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the name property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getName().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getName() {
            if (name == null) {
                name = new ArrayList<String>();
            }
            return this.name;
        }
        /**
         * 自建
         * @param list
         */
        public void setName(List<String> list) {
        	this.name = list;
        }

    }

}
