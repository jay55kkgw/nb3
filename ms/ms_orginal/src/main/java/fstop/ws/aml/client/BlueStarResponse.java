
package fstop.ws.aml.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * <pre>
 * &lt;complexType name="blueStarResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="searchResponseSoap" type="{http://component.ws.wlsr.siork.com/}searchResponseSoap" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="MsgName" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="App" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="RqUid" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "blueStarResponse", propOrder = {
    "searchResponseSoap"
})
public class BlueStarResponse {

    protected SearchResponseSoap searchResponseSoap;
    @XmlAttribute(name = "MsgName")
    protected String msgName;
    @XmlAttribute(name = "App")
    protected String app;
    @XmlAttribute(name = "RqUid")
    protected String rqUid;

    /**
     * ���o searchResponseSoap �S�ʪ���.
     * 
     * @return
     *     possible object is
     *     {@link SearchResponseSoap }
     *     
     */
    public SearchResponseSoap getSearchResponseSoap() {
        return searchResponseSoap;
    }

    /**
     * 
     * @param value
     *     allowed object is
     *     {@link SearchResponseSoap }
     *     
     */
    public void setSearchResponseSoap(SearchResponseSoap value) {
        this.searchResponseSoap = value;
    }

    /**
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsgName() {
        return msgName;
    }

    /**
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsgName(String value) {
        this.msgName = value;
    }

    /**
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApp() {
        return app;
    }

    /**
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApp(String value) {
        this.app = value;
    }

    /**
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRqUid() {
        return rqUid;
    }

    /**
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRqUid(String value) {
        this.rqUid = value;
    }

}
