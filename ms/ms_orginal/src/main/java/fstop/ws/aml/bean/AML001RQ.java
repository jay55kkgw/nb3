package fstop.ws.aml.bean;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/**
 * 電文XML物件
 */
@XmlRootElement(name="BlueStar")
//@XmlRootElement(name="QueryXmlStringResult")
@XmlType(propOrder = { "searchNamesSoap" , "TYPE"})
//@XmlAccessorType(XmlAccessType.FIELD)
public class AML001RQ {
	private String MSGNAME;
	private String APP;
	private String CLIENTIP;
	private String TYPE;
	private SearchNamesSoap searchNamesSoap;
	
//	@XmlRootElement(name="BlueStar")
//	@XmlType(name="searchNamesSoap",propOrder={"names" , "dateOfBirth" , "alert" ,"branch"})
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class SearchNamesSoap{
		private Names names ;
		private DateOfBirth dateOfBirth ;
		private String alert ;
		private Branch branch ;
		
		
		@XmlType(name="names",propOrder={"name"})
		public static class Names{
//			private String name;
			private List<String> name;

			public List<String> getName() {
				return name;
			}

			public void setName(List<String> name) {
				this.name = name;
			}
			
			
		} // Names End
		
		
//		@XmlType(name="name",propOrder={"name"})
//		public static class Name{
//			private String name;
//			
//			public String getName() {
//				return name;
//			}
//
//			public void setName(String name) {
//				this.name = name;
//			}
//		} // Name End
		
		@XmlType(name="dateOfBirth",propOrder={"year" , "month" , "day"})
		@XmlAccessorType(XmlAccessType.FIELD)
		public static class DateOfBirth{
			private String year;
			private String month;
			private String day;
			
			public String getYear() {
				return year;
			}
			public void setYear(String year) {
				this.year = year;
			}
			public String getMonth() {
				return month;
			}
			public void setMonth(String month) {
				this.month = month;
			}
			public String getDay() {
				return day;
			}
			public void setDay(String day) {
				this.day = day;
			}
		}// DateOfBirth end
		
		
		@XmlType(name="branch",propOrder={"businessUnit" , "branchId" , "sourceSystem"})
		@XmlAccessorType(XmlAccessType.FIELD)
		public static class Branch{
			private String businessUnit;
			private String branchId;
			private String sourceSystem;
			public String getBusinessUnit() {
				return businessUnit;
			}
			public void setBusinessUnit(String businessUnit) {
				this.businessUnit = businessUnit;
			}
			public String getBranchId() {
				return branchId;
			}
			public void setBranchId(String branchId) {
				this.branchId = branchId;
			}
			public String getSourceSystem() {
				return sourceSystem;
			}
			public void setSourceSystem(String sourceSystem) {
				this.sourceSystem = sourceSystem;
			}
			
			
		}// Branch end
		

		public Names getNames() {
			return names;
		}

		public void setNames(Names names) {
			this.names = names;
		}

		
		
		public DateOfBirth getDateOfBirth() {
			return dateOfBirth;
		}

		

		public void setDateOfBirth(DateOfBirth dateOfBirth) {
			this.dateOfBirth = dateOfBirth;
		}

		public String getAlert() {
			return alert;
		}

		public void setAlert(String alert) {
			this.alert = alert;
		}

		public Branch getBranch() {
			return branch;
		}

		public void setBranch(Branch branch) {
			this.branch = branch;
		}
		
		
		
	}//SearchNamesSoap end
	
	@XmlAttribute(name="MsgName")
	public String getMSGNAME() {
		return MSGNAME;
	}
	public void setMSGNAME(String mSGNAME) {
		MSGNAME = mSGNAME;
	}
	@XmlAttribute(name="App")
	public String getAPP() {
		return APP;
	}
	public void setAPP(String aPP) {
		APP = aPP;
	}
	@XmlAttribute(name="ClientIP")
	public String getCLIENTIP() {
		return CLIENTIP;
	}
	public void setCLIENTIP(String cLIENTIP) {
		CLIENTIP = cLIENTIP;
	}
	public String getTYPE() {
		return TYPE;
	}
	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}
	public SearchNamesSoap getSearchNamesSoap() {
		return searchNamesSoap;
	}
	public void setSearchNamesSoap(SearchNamesSoap searchNamesSoap) {
		this.searchNamesSoap = searchNamesSoap;
	}
	
	
}
