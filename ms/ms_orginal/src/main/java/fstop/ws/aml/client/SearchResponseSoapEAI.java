
package fstop.ws.aml.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * <pre>
 * &lt;complexType name="searchResponseSoapEAI"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="QueryXmlStringResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element ref="{http://component.ws.wlsr.siork.com/}BlueStar" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "searchResponseSoapEAI", propOrder = {
    "queryXmlStringResult",
    "blueStar"
})
public class SearchResponseSoapEAI {

    @XmlElement(name = "QueryXmlStringResult")
    protected String queryXmlStringResult;
//    @XmlElement(name = "BlueStar", namespace = "http://component.ws.wlsr.siork.com/")
    @XmlElement(name = "BlueStar")
    protected BlueStarResponse blueStar;

    /**
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQueryXmlStringResult() {
        return queryXmlStringResult;
    }

    /**
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQueryXmlStringResult(String value) {
        this.queryXmlStringResult = value;
    }

    /**
     * 
     * @return
     *     possible object is
     *     {@link BlueStarResponse }
     *     
     */
    public BlueStarResponse getBlueStar() {
        return blueStar;
    }

    /**
     * 
     * @param value
     *     allowed object is
     *     {@link BlueStarResponse }
     *     
     */
    public void setBlueStar(BlueStarResponse value) {
        this.blueStar = value;
    }

}
