
package fstop.ws.aml.client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the fstop.ws.aml.client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _BlueStar_QNAME = new QName("http://component.ws.wlsr.siork.com/", "BlueStar");
    private final static QName _SearchResponseSoap_QNAME = new QName("http://component.ws.wlsr.siork.com/", "SearchResponseSoap");
    private final static QName _PolicyContextException_QNAME = new QName("http://component.ws.wlsr.siork.com/", "PolicyContextException");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: fstop.ws.aml.client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link BlueStarResponse }
     * 
     */
    public BlueStarResponse createBlueStarResponse() {
        return new BlueStarResponse();
    }

    /**
     * Create an instance of {@link SearchResponseSoap }
     * 
     */
    public SearchResponseSoap createSearchResponseSoap() {
        return new SearchResponseSoap();
    }

    /**
     * Create an instance of {@link PolicyContextException }
     * 
     */
    public PolicyContextException createPolicyContextException() {
        return new PolicyContextException();
    }

    /**
     * Create an instance of {@link SearchResponseSoapEAI }
     * 
     */
    public SearchResponseSoapEAI createSearchResponseSoapEAI() {
        return new SearchResponseSoapEAI();
    }

    /**
     * Create an instance of {@link MatchResultXml }
     * 
     */
    public MatchResultXml createMatchResultXml() {
        return new MatchResultXml();
    }

    /**
     * Create an instance of {@link WatchListEntryXml }
     * 
     */
    public WatchListEntryXml createWatchListEntryXml() {
        return new WatchListEntryXml();
    }

    /**
     * Create an instance of {@link EntryDateXml }
     * 
     */
    public EntryDateXml createEntryDateXml() {
        return new EntryDateXml();
    }

    /**
     * Create an instance of {@link ReferencesXml }
     * 
     */
    public ReferencesXml createReferencesXml() {
        return new ReferencesXml();
    }

    /**
     * Create an instance of {@link NameEntryXml }
     * 
     */
    public NameEntryXml createNameEntryXml() {
        return new NameEntryXml();
    }

    /**
     * Create an instance of {@link NameTokenXml }
     * 
     */
    public NameTokenXml createNameTokenXml() {
        return new NameTokenXml();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BlueStarResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://component.ws.wlsr.siork.com/", name = "BlueStar")
    public JAXBElement<BlueStarResponse> createBlueStar(BlueStarResponse value) {
        return new JAXBElement<BlueStarResponse>(_BlueStar_QNAME, BlueStarResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchResponseSoap }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://component.ws.wlsr.siork.com/", name = "SearchResponseSoap")
    public JAXBElement<SearchResponseSoap> createSearchResponseSoap(SearchResponseSoap value) {
        return new JAXBElement<SearchResponseSoap>(_SearchResponseSoap_QNAME, SearchResponseSoap.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PolicyContextException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://component.ws.wlsr.siork.com/", name = "PolicyContextException")
    public JAXBElement<PolicyContextException> createPolicyContextException(PolicyContextException value) {
        return new JAXBElement<PolicyContextException>(_PolicyContextException_QNAME, PolicyContextException.class, null, value);
    }

}
