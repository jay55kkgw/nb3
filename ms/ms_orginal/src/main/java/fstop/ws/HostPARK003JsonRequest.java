package fstop.ws;

public class HostPARK003JsonRequest {
	
	private String txncode;		//交易代號
    private String customerid; //身分ID
    private String zonecode;   //縣市別
    private String carid;   	//車號
    private String account;   	//扣款帳號
	
    public String getTxncode() {
		return txncode;
	}
	public void setTxncode(String txncode) {
		this.txncode = txncode;
	}
	public String getCustomerid() {
		return customerid;
	}
	public void setCustomerid(String customerid) {
		this.customerid = customerid;
	}
	public String getZonecode() {
		return zonecode;
	}
	public void setZonecode(String zonecode) {
		this.zonecode = zonecode;
	}
	public String getCarid() {
		return carid;
	}
	public void setCarid(String carid) {
		this.carid = carid;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
}
