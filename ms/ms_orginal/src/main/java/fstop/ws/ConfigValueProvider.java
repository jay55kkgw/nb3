package fstop.ws;

public interface ConfigValueProvider {
    /**
     * 取得驗證參數
     * @return
     */
//    String getValidParam();

    /**
     * 取得QR Code 系統參數
     * @param name
     * @param defalutValue
     * @return
     */
    String getValue(String name, String defalutValue);
}
