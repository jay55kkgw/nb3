package fstop.ws;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.Validate;

import fstop.util.JSONUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AcctVerifyErrorCodeMapping {
//	private static Logger logger = Logger.getLogger(AcctVerifyErrorCodeMapping.class);
	
	private static String errorStr = "" + 
			"0000	成功。\r\n" + 
			"0012	系統忙線中。\r\n" + 
			"0013	交易逾時。\r\n" +
			"0014	尚未執行驗證碼更新交易(跨境使用)。\r\n" +
			"0015	驗證碼更新失敗，請確認是否已設定初始驗證碼值(跨境使用)。\r\n" +
			"0018	驗證碼錯誤。\r\n" + 
			"0019	特店代號不存在。\r\n" + 
			"0020	端末代號不存在。\r\n" + 
			"0021	系統暫停服務。\r\n" + 
			"0023	退款(貨)金額超出原購物金額。\r\n" + 
			"0024	發卡行拒絕交易。\r\n" + 
			"0025	驗證碼初始化，請執行驗證碼更新交易。\r\n" + 
			"0026	驗證碼效期超出設定，請執行驗證碼更新交易。\r\n" + 
			"0027	原交易授權失敗。\r\n" + 
			"0028	該筆退款(貨)交易超出退款(貨)期限。\r\n" + 
			"0029	(原)交易處理中。\r\n" + 
			"0030	總手續費大於清算金額。\r\n" +
			"0031	清算金額*清算匯率不等於交易金額。。\r\n" +
			"0032	持卡人帳戶餘額不足。\r\n" +
			"0033	持卡人交易超出限額或限制次數。\r\n" +
			"0034	發卡行拒絕交易。\r\n" +
			"0050	綁定作業失敗—帳號不存在。\r\n" +
			"0071	收單行系統傳送的回應訊息驗證碼錯誤。\r\n" +			
			"0081	無相對應的原始交易資料。\r\n" + 
			"0082	此(購物/退貨)訂單交易無法重覆授權。\r\n" + 
			"0083	此訂單交易已取消。\r\n" +
			"0087	不支援此交易類型。\r\n" + 
			"0088	解密失敗。\r\n" + 
			"0089	驗證失敗。\r\n" + 
			"0097	請執行驗證碼更新交易。\r\n" +
			"0098	交易狀態未明。\r\n" + 
			"0099	不明原因異常。\r\n" + 
			"4000	交易傳送方式不符。\r\n" +
			"42xx	格式有錯。\r\n" + 
			"44xx	xx欄位應帶而未帶。\r\n" + 
			"46xx	欄位值不合規定。"+			
			"5001	跨境匯出P33查核驗證回覆碼。"+
			"5002	跨境匯出P33查核驗證回覆碼。"+
			"5003	跨境匯出P33查核驗證回覆碼。"+
			"C001	讀取匯率異常。"+
			"C002	REQ交易驗證檢核有誤。"+
			"H001	僅接受以POST傳送參數。"+
			"P040	收單行&特店&端末&訂單編號不可重複。"+
			"R051	已逾退貨期限，無法退貨。"+
			"R052	退貨編號不可與訂單編號一樣。"+
			"R053	收單行&特店&端末&退貨編號不可重複。"+
			"R054	平台代號與原交易平台代號不同。"+
			"R055	查無原訂單編號。"+
			"R056	查無原退貨編號。"+
			"R057	原訂單編號非成功交易。"+
			"R058	退貨總金額已超過原購貨金額。";
	
	
	private static Map<String, String> errorMap = new LinkedHashMap();

	/**
	 * 取得真正的錯誤訊息
	 * @param errorCode
	 * @return
	 */
	public static String getErrorString(String errorCode) {
		if(StrUtils.isEmpty(errorCode))
			return "";
		String ret = "";
		Integer errorStart = Integer.valueOf(errorCode.substring(0, 2));
		String tmp = StrUtils.right(errorCode, 2).toUpperCase();
		char f = tmp.charAt(0);
		String f1 = tmp.charAt(0) + "";
		String f2 = tmp.charAt(1) + "";
		Integer errorEnd = null;
		if(f >= 0x30 && f <= 0x39) {
			errorEnd = Integer.valueOf(tmp);
		}
		else {
			errorEnd = Integer.valueOf(f1, 16)  * 10 + Integer.valueOf(f2);
		}

		switch(errorStart) {
		case 42:
			ret = getField(errorEnd.toString())  + "格式有錯";
			break;
		case 44:
			ret = getField(errorEnd.toString())  + "欄位應帶而未帶";
			break;
		case 46:
			ret = getField(errorEnd.toString()) + "欄位值不合規定";
			break;
		default:
			ret = errorMap.get(errorCode);
		}
		return ret;
	}


	public static String getField(String error) {
		Map<String, FieldDesc> fieldNumMap = AcctVerifyAllFields.fieldNumMap;

		FieldDesc field = fieldNumMap.get(error);
		if(field == null) {
			return "";
		}
		else {
			return field.getField();
		}
	}

	static {
		errorStr = errorStr.replaceAll("\t", " ");
		//log.debug(errorStr);
		//log.debug("----------------------------");
		try {
			Pattern regex = Pattern.compile("^(.{4})\\s(.+?)$",
					Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE | Pattern.MULTILINE);
			Matcher regexMatcher = regex.matcher(errorStr);
			while (regexMatcher.find()) {
				
				errorMap.put(StrUtils.trim(regexMatcher.group(1)), StrUtils.trim(regexMatcher.group(2)));
			}
		} catch (Exception ex) {
			throw new IllegalArgumentException("無法解析 ErrorCodeMapping errorStr.", ex);
		}
	}
	
	public static void testError() {
		Validate.isTrue("無相對應的原始交易資料。".equalsIgnoreCase(getErrorString("0081")));
		Validate.isTrue("mti格式有錯".equalsIgnoreCase(getErrorString("4200")));
		Validate.isTrue("verifyCode欄位應帶而未帶".equalsIgnoreCase(getErrorString("4464")));
		Validate.isTrue("verifyCode欄位值不合規定".equalsIgnoreCase(getErrorString("4664")));
		Validate.isTrue("paymentType欄位值不合規定".equalsIgnoreCase(getErrorString("46C9")));

		String o = getErrorString("4211");
		log.debug(o);
	}
	
//	public static void main(String[] args) {
//		log.debug(getErrorString("4448"));
//		log.debug(JSONUtils.toJson(errorMap));
//		testError();
//	}
}
