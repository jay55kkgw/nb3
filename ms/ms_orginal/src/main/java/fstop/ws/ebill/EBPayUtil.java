package fstop.ws.ebill;

import java.io.File;
import java.io.FileInputStream;
import java.net.HttpURLConnection;
import java.security.KeyStore;
import java.security.SecureRandom;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;

import com.netbank.util.ESAPIUtil;
import com.tbb.ebill.util.MessageCode;
import com.tbb.ebill.xml.CheckRep;
import com.tbb.ebill.xml.CheckReq;
import com.tbb.ebill.xml.Envelope;
import com.tbb.ebill.xml.HcInfo;
import com.tbb.ebill.xml.MacInfo;
import com.tbb.ebill.xml.Message;
import com.tbb.ebill.xml.PayInfo;
import com.tbb.ebill.xml.PayRep;
import com.tbb.ebill.xml.PayReq;
import com.tbb.ebill.xml.checkinfo.CheckInfoCreditCard;
import com.tbb.ebill.xml.checkinfo.CheckInfoTaiWater;
import com.tbb.ebill.xml.payinfo.Cc;
import com.tbb.ebill.xml.payinfo.Ic;
import com.tbb.ebill.xml.payinfo.Id;
import com.tbb.ebill.xml.payinfo.PayInfoCreditCard;
import com.tbb.ebill.xml.payinfo.PayInfoIcCard;
import com.tbb.ebill.xml.payinfo.PayInfoIdAccount;
import com.tbb.ebill.xml.txninfo.TxnInfoCreditCard;
import com.tbb.ebill.xml.txninfo.TxnInfoTaiWater;

import fstop.core.BeanUtils;
import fstop.exception.TopMessageException;
import fstop.ws.ebill.bean.BillData;
import fstop.ws.ebill.bean.CCard;
import fstop.ws.ebill.bean.TaiWater;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EBPayUtil
{

	public static void setSSLContext(Object req, String cert_path, String soci, Integer connectionTimeout, Integer receiveTimeout)
	{
		try
		{
			KeyStore trustStore = KeyStore.getInstance("JKS");
			trustStore.load(new FileInputStream(new File(cert_path)), soci.toCharArray());

			// Create and initialize the truststore manager
			TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
			tmf.init(trustStore);

			// Create and initialize the SSL context
			SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
			sslContext.init(null, tmf.getTrustManagers(), new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());

			Client client = ClientProxy.getClient(req);
			HTTPConduit http = (HTTPConduit) client.getConduit();
			
	        HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();
	        httpClientPolicy.setConnectionTimeout(1000 * connectionTimeout);
	        httpClientPolicy.setReceiveTimeout(1000 * receiveTimeout);
	        http.setClient(httpClientPolicy);

			// Set the TLS client parameters
			TLSClientParameters parameters = new TLSClientParameters();
			parameters.setSSLSocketFactory(sslContext.getSocketFactory());
			http.setTlsClientParameters(parameters);
		}
		catch (Exception e)
		{
			log.error(e.toString());
		}
	}


	
	/**
	 * 
	 * 產生檢查銷帳編號與統一編號是否一致 XML
	 * @param billData
	 * @param vaChannel
	 * @param ebillKeyID
	 * @return
	 */
	public static String preCreditCard_CheckReq(CCard billData, String vaChannel, String ebillKeyID)
	{
		String sendEbillXmlMsg = "";
		try
		{
			CheckInfoCreditCard checkInfo = new CheckInfoCreditCard();
			BeanUtils.copyProperties(checkInfo, billData);
			
			CheckReq message = new CheckReq();
			BeanUtils.copyProperties(message, billData);
			message.setCheckInfo(checkInfo);
			
			Envelope envelope = new Envelope();
			envelope.setMessage(message);
			envelope.setMacInfo(new MacInfo());
			
			Class<?>[] c = { Envelope.class, MacInfo.class, Message.class, CheckReq.class, CheckInfoCreditCard.class };
			MacInfo macInfo = envelope.getMacInfo();
			
			sendEbillXmlMsg = GenXML.genReqXmlMsg(c, message);
			log.debug("preCreditCard_CheckReq sendEbillXmlMsg >>>>>>>>{}",sendEbillXmlMsg);
			GenXML.getMac(macInfo, sendEbillXmlMsg, vaChannel, ebillKeyID);
			
			sendEbillXmlMsg = GenXML.genReqXmlMsg(c, envelope);
			log.debug("preCreditCard_CheckReq sendEbillXmlMsg MAC>>>>>>>>{}",sendEbillXmlMsg);
			
		}
		catch (Exception e)
		{
			log.error(e.toString());
		}
		return sendEbillXmlMsg;
	}
	
	
	/**
	 * 檢查銷帳編號與統一編號是否一致 
	 * @param billData
	 * @param rcvEbillXmlMsg
	 * @param vaChannel
	 * @param ebillKeyID
	 */
	public static void CreditCard_CheckReq(CCard billData,  String rcvEbillXmlMsg, String vaChannel, String ebillKeyID)
	{

		try
		{
			Envelope envelope = new Envelope();
			Class<?>[] c = { Envelope.class, MacInfo.class, Message.class, CheckRep.class, HcInfo.class };
			envelope = GenXML.parseRepXmlMsg(c, rcvEbillXmlMsg);
			billData.setSend_ApiTxnNo(billData.getApiTxnNo());
			boolean vrfyMacRslt = false;
			vrfyMacRslt = GenXML.verifyMac(envelope.getMacInfo(), rcvEbillXmlMsg, vaChannel, ebillKeyID);
			log.info("vrfyMacRslt: " + vrfyMacRslt);
			billData.setVrfyMacRslt(vrfyMacRslt);
			if (vrfyMacRslt)
			{
				CheckRep message = (CheckRep) envelope.getMessage();
				BeanUtils.copyProperties(billData, message);
				log.info(ESAPIUtil.vaildLog("verify ApiTxnNo: " + billData.vrfyApiTxnNo()));
				if ("0910".equals(message.getMti()) && "7131".equals(message.getPcode()) && billData.vrfyApiTxnNo())
				{
					if ("0001".equals(message.getRcode()))
					{
						HcInfo hcInfo = message.getHcInfo();
						BeanUtils.copyProperties(billData, hcInfo);
					}
					billData.setTrnsCode(MessageCode.getCode(MessageCode.R0000)); // 僅表示交易流程有正常完成
				}
				else
				{
					billData.setTrnsCode(MessageCode.getCode(MessageCode.R2999)); // 解析接收的訊息與預期不符合
				}
			}
			else
			{
				billData.setTrnsCode(MessageCode.getCode(MessageCode.R0302)); // 訊息押碼錯誤
			}
		}
		catch (Exception e)
		{
			log.error(e.toString());
		}
	}
	
	/**
	 * 繳納本行國際信用卡帳款FOR QRCODE
	 * @param billData
	 * @param vaChannel
	 * @param ebillKeyID
	 * @return
	 */
	public static String preReqCreditCard_PayReq(CCard billData, String vaChannel, String ebillKeyID)
	{
		String sendEbillXmlMsg = "";
		try
		{
			EBPayUtil.chkTrnsCode(billData);

			// PAY_REQ
			TxnInfoCreditCard txnInfo = new TxnInfoCreditCard();
			BeanUtils.copyProperties(txnInfo, billData);

			PayReq message = new PayReq();
			BeanUtils.copyProperties(message, billData);


			PayInfo payInfo = null;
			if ("IC".equals(billData.getPayType()))
			{
				Ic ic = new Ic();
				BeanUtils.copyProperties(ic, billData);

				payInfo = new PayInfoIcCard();
				((PayInfoIcCard) payInfo).setIc(ic);
			}
			if ("ID".equals(billData.getPayType()))
			{
				Id id = new Id();
				BeanUtils.copyProperties(id, billData);

				payInfo = new PayInfoIdAccount();
				((PayInfoIdAccount) payInfo).setId(id);
			}
			if ("CC".equals(billData.getPayType()))
			{
				Cc cc = new Cc();
				BeanUtils.copyProperties(cc, billData);
				/*
				 * <ACQ_BIN> 若是VISA卡（卡號4打頭），請帶 455742 若是MasterCard卡（卡號5打頭），請帶 003741 若是JCB卡（卡號3打頭），請帶 88546000
				 * 若連財金，為固定以上規則
				 */
				if (cc.getCardNumber().startsWith("4"))
				{
					cc.setAcqBin("455742");
				}
				if (cc.getCardNumber().startsWith("5"))
				{
					cc.setAcqBin("003741");
				}
				if (cc.getCardNumber().startsWith("3"))
				{
					cc.setAcqBin("88546000");
				}

				payInfo = new PayInfoCreditCard();
				((PayInfoCreditCard) payInfo).setCc(cc);
			}
			message.setTxninfo(txnInfo);
			message.setPayinfo(payInfo);

			Envelope envelope = new Envelope();
			envelope.setMessage(message);
			envelope.setMacInfo(new MacInfo());

			Class<?>[] c = { Envelope.class, MacInfo.class, Message.class, PayReq.class, TxnInfoCreditCard.class,
					PayInfoIdAccount.class, PayInfoIcCard.class, PayInfoCreditCard.class, Ic.class, Id.class,
					Cc.class };

			MacInfo macInfo = envelope.getMacInfo();
			sendEbillXmlMsg = GenXML.genReqXmlMsg(c, message);

			GenXML.getMac(macInfo, sendEbillXmlMsg, vaChannel, ebillKeyID);
			log.debug(sendEbillXmlMsg);

			sendEbillXmlMsg = GenXML.genReqXmlMsg(c, envelope);
		}
		catch (Exception e)
		{
			log.error(e.toString());
		}
		return sendEbillXmlMsg;
	}
	
	/**
	 * 
	 * @param billData
	 * @param rcvEbillXmlMsg
	 * @param vaChannel
	 * @param ebillKeyID
	 */
	public static void preRepCreditCard_PayReq(CCard billData, String rcvEbillXmlMsg, String vaChannel,String ebillKeyID)
	{

		try
		{
			Envelope envelope = new Envelope();
			Class<?>[] c = { Envelope.class, MacInfo.class, Message.class, PayRep.class, TxnInfoCreditCard.class };
			envelope = GenXML.parseRepXmlMsg(c, rcvEbillXmlMsg);

			billData.setSend_ApiTxnNo(billData.getApiTxnNo());
			billData.setSend_SessionId(billData.getSessionId());

			boolean vrfyMacRslt = false;
			vrfyMacRslt = GenXML.verifyMac(envelope.getMacInfo(), rcvEbillXmlMsg, vaChannel, ebillKeyID);
			log.info("vrfyMacRslt: " + vrfyMacRslt);
			billData.setVrfyMacRslt(vrfyMacRslt);

			if (vrfyMacRslt)
			{
				PayRep message = (PayRep) envelope.getMessage();
				BeanUtils.copyProperties(billData, message);

				log.info(ESAPIUtil.vaildLog("verify ApiTxnNo: " + billData.vrfyApiTxnNo()));
				if ("0910".equals(message.getMti()) && "7150".equals(message.getPcode()) && billData.vrfyApiTxnNo())
				{
					if (billData.getRcodeDesc() != null && billData.getRcodeDesc().length() > 0)
					{
						billData.setRcodeDesc(new String(GenXML.hex2Byte(billData.getRcodeDesc().getBytes("UTF-8")), "UTF-8"));
					}
					if ("0001".equals(message.getRcode()))
					{
						log.info(ESAPIUtil.vaildLog("verify SessionId: " + billData.vrfySessionId()));
						if (billData.vrfySessionId())
						{
							TxnInfoTaiWater txnInfo = (TxnInfoTaiWater) message.getTxninfo();
							BeanUtils.copyProperties(billData, txnInfo);
						}
						else
						{
							billData.setTrnsCode(MessageCode.getCode(MessageCode.R2999)); // 解析接收的訊息與預期不符合
						}
					}
					billData.setTrnsCode(MessageCode.getCode(MessageCode.R0000)); // 僅表示交易流程有正常完成
				}
				else
				{
					billData.setTrnsCode(MessageCode.getCode(MessageCode.R2999)); // 解析接收的訊息與預期不符合
				}
			}
			else
			{
				billData.setTrnsCode(MessageCode.getCode(MessageCode.R0302)); // 訊息押碼錯誤
			}
		}
		catch (Exception e)
		{
			log.error(e.toString());
		}
	}
	
	

	public static String preReqTwWater_CheckReq(TaiWater billData, String vaChannel, String ebillKeyID)
	{
		String sendEbillXmlMsg = "";
		try
		{
			CheckInfoTaiWater checkInfo = new CheckInfoTaiWater();
			BeanUtils.copyProperties(checkInfo, billData);

			CheckReq message = new CheckReq();
			BeanUtils.copyProperties(message, billData);
			message.setCheckInfo(checkInfo);
			
//			message.setApiTxnNo("0000001");// db序號

			Envelope envelope = new Envelope();
			envelope.setMessage(message);
			envelope.setMacInfo(new MacInfo());
			Class<?>[] c = { Envelope.class, MacInfo.class, Message.class, CheckReq.class, CheckInfoTaiWater.class };

			MacInfo macInfo = envelope.getMacInfo();

			sendEbillXmlMsg = GenXML.genReqXmlMsg(c, message);

			GenXML.getMac(macInfo, sendEbillXmlMsg, vaChannel, ebillKeyID);
			log.debug(sendEbillXmlMsg);

			sendEbillXmlMsg = GenXML.genReqXmlMsg(c, envelope);
		}
		catch (TopMessageException e) {
			log.error(e.toString());
			throw e;
		}
		catch (Exception e)
		{
			log.error(e.toString());
		}
		return sendEbillXmlMsg;
	}

	public static void preRepTwWater_CheckReq(TaiWater billData, String rcvEbillXmlMsg, String vaChannel, String ebillKeyID)
	{

		try
		{
			Envelope envelope = new Envelope();
			Class<?>[] c = { Envelope.class, MacInfo.class, Message.class, CheckRep.class, HcInfo.class };
			envelope = GenXML.parseRepXmlMsg(c, rcvEbillXmlMsg);

			billData.setSend_ApiTxnNo(billData.getApiTxnNo());

			boolean vrfyMacRslt = false;
			vrfyMacRslt = GenXML.verifyMac(envelope.getMacInfo(), rcvEbillXmlMsg, vaChannel, ebillKeyID);
			log.info("vrfyMacRslt: " + vrfyMacRslt);
			billData.setVrfyMacRslt(vrfyMacRslt);

			if (vrfyMacRslt)
			{
				CheckRep message3 = (CheckRep) envelope.getMessage();
				BeanUtils.copyProperties(billData, message3);

				log.info(ESAPIUtil.vaildLog("verify ApiTxnNo: " + billData.vrfyApiTxnNo()));
				if ("0910".equals(message3.getMti()) && "7131".equals(message3.getPcode()) && billData.vrfyApiTxnNo())
				{
					if ("0001".equals(message3.getRcode()))
					{
						HcInfo hcInfo = message3.getHcInfo();
						BeanUtils.copyProperties(billData, hcInfo);
					}
					billData.setTrnsCode(MessageCode.getCode(MessageCode.R0000)); // 僅表示交易流程有正常完成
				}
				else
				{
					billData.setTrnsCode(MessageCode.getCode(MessageCode.R2999)); // 解析接收的訊息與預期不符合
				}
			}
			else
			{
				billData.setTrnsCode(MessageCode.getCode(MessageCode.R0302)); // 訊息押碼錯誤
			}
		}
		catch (TopMessageException e) {
			log.error(e.toString());
			throw e;
		}
		catch (Exception e)
		{
			log.error(e.toString());
		}
	}

	public static void chkTrnsCode(BillData billData) throws Exception
	{
		log.debug("TrnsCode: " + billData.getTrnsCode());
		if (MessageCode.getCode(MessageCode.R1002).equals(billData.getTrnsCode()))
		{
			throw new Exception(MessageCode.getCode(MessageCode.R1002)); // R1002:跨行交易序號重覆或繳費單位交易序號重覆
		}
		if (MessageCode.getCode(MessageCode.R0104).equals(billData.getTrnsCode()))
		{
			throw new Exception(MessageCode.getCode(MessageCode.R0104)); // R0104:訊息發生一般檢核類錯誤
		}
	}

	public static String preReqTwWater_PayReq(TaiWater billData, String vaChannel, String ebillKeyID)
	{
		String sendEbillXmlMsg = "";
		try
		{
			EBPayUtil.chkTrnsCode(billData);

			// PAY_REQ
			TxnInfoTaiWater txnInfo = new TxnInfoTaiWater();
			BeanUtils.copyProperties(txnInfo, billData);

			PayReq message = new PayReq();
			BeanUtils.copyProperties(message, billData);

//			message.setApiTxnNo("0000001");// db序號

			PayInfo payInfo = null;
			if ("IC".equals(billData.getPayType()))
			{
				Ic ic = new Ic();
				BeanUtils.copyProperties(ic, billData);

				payInfo = new PayInfoIcCard();
				((PayInfoIcCard) payInfo).setIc(ic);
			}
			if ("ID".equals(billData.getPayType()))
			{
				Id id = new Id();
				BeanUtils.copyProperties(id, billData);

				payInfo = new PayInfoIdAccount();
				((PayInfoIdAccount) payInfo).setId(id);
			}
			if ("CC".equals(billData.getPayType()))
			{
				Cc cc = new Cc();
				BeanUtils.copyProperties(cc, billData);
				/*
				 * <ACQ_BIN> 若是VISA卡（卡號4打頭），請帶 455742 若是MasterCard卡（卡號5打頭），請帶 003741 若是JCB卡（卡號3打頭），請帶 88546000
				 * 若連財金，為固定以上規則
				 */
				if (cc.getCardNumber().startsWith("4"))
				{
					cc.setAcqBin("455742");
				}
				if (cc.getCardNumber().startsWith("5"))
				{
					cc.setAcqBin("003741");
				}
				if (cc.getCardNumber().startsWith("3"))
				{
					cc.setAcqBin("88546000");
				}

				payInfo = new PayInfoCreditCard();
				((PayInfoCreditCard) payInfo).setCc(cc);
			}
			message.setTxninfo(txnInfo);
			message.setPayinfo(payInfo);

			Envelope envelope = new Envelope();
			envelope.setMessage(message);
			envelope.setMacInfo(new MacInfo());

			Class<?>[] c = { Envelope.class, MacInfo.class, Message.class, PayReq.class, TxnInfoTaiWater.class,
					PayInfoIdAccount.class, PayInfoIcCard.class, PayInfoCreditCard.class, Ic.class, Id.class,
					Cc.class };

			MacInfo macInfo = envelope.getMacInfo();
			sendEbillXmlMsg = GenXML.genReqXmlMsg(c, message);

			GenXML.getMac(macInfo, sendEbillXmlMsg, vaChannel, ebillKeyID);
			log.debug(sendEbillXmlMsg);

			sendEbillXmlMsg = GenXML.genReqXmlMsg(c, envelope);
		}
		catch (TopMessageException e) {
			log.error(e.toString());
			throw e;
		}
		catch (Exception e)
		{
			log.error(e.toString());
		}
		return sendEbillXmlMsg;
	}

	public static void preRepTwWater_PayReq(TaiWater billData, String rcvEbillXmlMsg, String vaChannel,	String ebillKeyID)
	{

		try
		{
			Envelope envelope = new Envelope();
			Class<?>[] c = { Envelope.class, MacInfo.class, Message.class, PayRep.class, TxnInfoTaiWater.class };
			envelope = GenXML.parseRepXmlMsg(c, rcvEbillXmlMsg);

			billData.setSend_ApiTxnNo(billData.getApiTxnNo());
			billData.setSend_SessionId(billData.getSessionId());

			boolean vrfyMacRslt = false;
			vrfyMacRslt = GenXML.verifyMac(envelope.getMacInfo(), rcvEbillXmlMsg, vaChannel, ebillKeyID);
			log.info("vrfyMacRslt: " + vrfyMacRslt);
			billData.setVrfyMacRslt(vrfyMacRslt);

			if (vrfyMacRslt)
			{
				PayRep message = (PayRep) envelope.getMessage();
				BeanUtils.copyProperties(billData, message);

				log.info(ESAPIUtil.vaildLog("verify ApiTxnNo: " + billData.vrfyApiTxnNo()));
				if ("0910".equals(message.getMti()) && "7150".equals(message.getPcode()) && billData.vrfyApiTxnNo())
				{
					if (billData.getRcodeDesc() != null && billData.getRcodeDesc().length() > 0)
					{
						billData.setRcodeDesc(new String(GenXML.hex2Byte(billData.getRcodeDesc().getBytes("UTF-8")), "UTF-8"));
					}
					if ("0001".equals(message.getRcode()))
					{
						log.info(ESAPIUtil.vaildLog("verify SessionId: " + billData.vrfySessionId()));
						if (billData.vrfySessionId())
						{
							TxnInfoTaiWater txnInfo = (TxnInfoTaiWater) message.getTxninfo();
							BeanUtils.copyProperties(billData, txnInfo);
						}
						else
						{
							billData.setTrnsCode(MessageCode.getCode(MessageCode.R2999)); // 解析接收的訊息與預期不符合
						}
					}
					billData.setTrnsCode(MessageCode.getCode(MessageCode.R0000)); // 僅表示交易流程有正常完成
				}
				else
				{
					billData.setTrnsCode(MessageCode.getCode(MessageCode.R2999)); // 解析接收的訊息與預期不符合
				}
			}
			else
			{
				billData.setTrnsCode(MessageCode.getCode(MessageCode.R0302)); // 訊息押碼錯誤
			}
		}
		catch (TopMessageException e) {
			log.error(e.toString());
			throw e;
		}
		catch (Exception e)
		{
			log.error(e.toString());
		}
	}

	/**
	 * 補位
	 * @param str 原始字串
	 * @param length 總長度
	 * @param lr L左補 / R右補
	 * @param padddingChar 補字串(僅限1個字)
	 * @return
	 */
	public static String padding(String str, int length, String lr, String padddingChar) {
		String result = str;
		if (str.length() >= length || padddingChar.length() > 1) {
			return str;
		}
		for (int i=0; i<(length-str.length()); i++) {
			if ("L".equals(lr)) {
				result = padddingChar + result;
			} else {
				result = result + padddingChar;
			}
		}
		return result;
	}
}
