package fstop.ws.ebill;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;

import com.tbb.ebill.webservice.client.ebill.IWebApi;

import fstop.util.SpringBeanFactory;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EbillBean {

	public static String queryReq(String sendEbillXmlMsg,String ebillWsdl,String cert_path,String soci, Integer connectionTimeout, Integer receiveTimeout) throws Exception {
		IWebApi req = req(ebillWsdl);
		EBPayUtil.setSSLContext(req, cert_path, soci, connectionTimeout, receiveTimeout);
		return req.queryBill(sendEbillXmlMsg);
	}

	public static String checkReq(String sendEbillXmlMsg,String ebillWsdl,String cert_path,String soci, Integer connectionTimeout, Integer receiveTimeout) throws Exception {
		IWebApi req = req(ebillWsdl);
		EBPayUtil.setSSLContext(req, cert_path, soci, connectionTimeout, receiveTimeout);
		return req.checkBill(sendEbillXmlMsg);
	}

	public static String payReq(String sendEbillXmlMsg,String ebillWsdl,String cert_path,String soci, Integer connectionTimeout, Integer receiveTimeout) throws Exception {
		IWebApi req = req(ebillWsdl);
		EBPayUtil.setSSLContext(req, cert_path, soci, connectionTimeout, receiveTimeout);
		return req.payBill(sendEbillXmlMsg);
	}

	public static String resendReq(String sendEbillXmlMsg,String ebillWsdl,String cert_path,String soci, Integer connectionTimeout, Integer receiveTimeout) throws Exception {
		IWebApi req = req(ebillWsdl);
		EBPayUtil.setSSLContext(req, cert_path, soci, connectionTimeout, receiveTimeout);
		return req.getResult(sendEbillXmlMsg);
	}

	private static com.tbb.ebill.webservice.client.ebill.IWebApi req(String ebillWsdl) throws Exception {
		URL url = null;
		try {
			log.info("wsdl: " + ebillWsdl);
			if (ebillWsdl.indexOf("https")==0){
				log.info("wsdl use https !!");
				url = new URL(ebillWsdl); //wsdl讀網址
			} else {
				log.info("wsdl use localfile !! " + Thread.currentThread().getContextClassLoader().getResource("ebillwebservice.svc.wsdl").getFile());
				url = new URL(Thread.currentThread().getContextClassLoader().getResource(ebillWsdl), ebillWsdl); //wsdl讀local的檔案
			}
			log.info("run-wsdl: " + url.getFile());
		} catch (MalformedURLException e) {
			e.getMessage();
		}
		
		QName SERVICE_NAME = new QName("http://tempuri.org/", "WebApi");
		com.tbb.ebill.webservice.client.ebill.WebApi webApi = new com.tbb.ebill.webservice.client.ebill.WebApi(url, SERVICE_NAME);
		return webApi.getBasicHttpBindingIWebApi();
	}
}
