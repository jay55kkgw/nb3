package fstop.ws.ebill.bean;

import org.springframework.beans.factory.annotation.Value;

public class BillData {
	
	//共用
	private String trnsNo;        //32 byte UUID
	private String access;        //NB/CNB/MB/EATM
	private String bill;
	private String action;
	private String payType = "IC";//IC|ID
	private boolean vrfyMacRslt;
	private String trnsCode;      //交易完成代碼:0000(交易完成)
	private String send_ApiTxnNo;
	private String send_SessionId;

	//EBILL 共用
	private String txnDateTime;
	private String apiTxnNo;
	private String srcId;
	private String rcode;
	private String feeRefId;
	private String sessionId;
	private String hc;
	private String stan;
	private String rcodeDesc;
	private String authCode;

	//安控
	private String cdkey;
	private String divdata; //HEX
	private String icvdata; //HEX
	private String macdata; //HEX

	//HC_INFO
	private String ic;
	private String id;
	private String cc;

	//PAY_INFO:common
	private String terminalType;
	private String terminalId;
	private String terminalCheckId;
	private String traBankId;
	private String traAccount;

	//PAY_INFO:IC
	private String icDateTime;
	private String icTxnNo;
	private String icBankInfo;
	private String tac;

	//PAY_INFO:ID
	private String payIdnBan;

	//PAY_INFO:CC
	private String cardNumber;
	private String expireDate;
	private String cvv2;
	private String acqBin;
	private String rrn;

	//RESEND
	private String orgTxnDateTime;
	private String orgApiTxnNo;
	private String orgRcode;
	private String orgStan; 

	public boolean vrfyApiTxnNo() {
		return apiTxnNo.equals(send_ApiTxnNo);
	}
	public boolean vrfySessionId() {
		return sessionId.equals(send_SessionId);
	}
	//===============================================================================

	public String getTrnsNo() {
		return trnsNo;
	}

	public String getSrcId() {
		return srcId;
	}
	
	public void setSrcId(String srcId) {
		this.srcId = srcId;
	}
	
	public void setTrnsNo(String trnsNo) {
		this.trnsNo = trnsNo;
	}

	public String getAccess() {
		return access;
	}

	public void setAccess(String access) {
		this.access = access;
	}

	public String getBill() {
		return bill;
	}

	public void setBill(String bill) {
		this.bill = bill;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public boolean isVrfyMacRslt() {
		return vrfyMacRslt;
	}

	public void setVrfyMacRslt(boolean vrfyMacRslt) {
		this.vrfyMacRslt = vrfyMacRslt;
	}

	public String getTrnsCode() {
		return trnsCode;
	}

	public void setTrnsCode(String trnsCode) {
		this.trnsCode = trnsCode;
	}

	public String getSend_ApiTxnNo() {
		return send_ApiTxnNo;
	}
	public void setSend_ApiTxnNo(String send_ApiTxnNo) {
		this.send_ApiTxnNo = send_ApiTxnNo;
	}

	public String getSend_SessionId() {
		return send_SessionId;
	}
	public void setSend_SessionId(String send_SessionId) {
		this.send_SessionId = send_SessionId;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getTxnDateTime() {
		return txnDateTime;
	}

	public void setTxnDateTime(String txnDateTime) {
		this.txnDateTime = txnDateTime;
	}

	public String getApiTxnNo() {
		return apiTxnNo;
	}

	public void setApiTxnNo(String apiTxnNo) {
		this.apiTxnNo = apiTxnNo;
	}

	public String getRcode() {
		return rcode;
	}

	public void setRcode(String rcode) {
		this.rcode = rcode;
	}

	public String getFeeRefId() {
		return feeRefId;
	}

	public void setFeeRefId(String feeRefId) {
		this.feeRefId = feeRefId;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getHc() {
		return hc;
	}

	public void setHc(String hc) {
		this.hc = hc;
	}

	public String getStan() {
		return stan;
	}

	public void setStan(String stan) {
		this.stan = stan;
	}

	public String getRcodeDesc() {
		return rcodeDesc;
	}

	public void setRcodeDesc(String rcodeDesc) {
		this.rcodeDesc = rcodeDesc;
	}

	public String getCdkey() {
		return cdkey;
	}

	public void setCdkey(String cdkey) {
		this.cdkey = cdkey;
	}

	public String getDivdata() {
		return divdata;
	}

	public void setDivdata(String divdata) {
		this.divdata = divdata;
	}

	public String getIcvdata() {
		return icvdata;
	}

	public void setIcvdata(String icvdata) {
		this.icvdata = icvdata;
	}

	public String getMacdata() {
		return macdata;
	}

	public void setMacdata(String macdata) {
		this.macdata = macdata;
	}

	public String getIc() {
		return ic;
	}

	public void setIc(String ic) {
		this.ic = ic;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTerminalType() {
		return terminalType;
	}

	public void setTerminalType(String terminalType) {
		this.terminalType = terminalType;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public String getTerminalCheckId() {
		return terminalCheckId;
	}

	public void setTerminalCheckId(String terminalCheckId) {
		this.terminalCheckId = terminalCheckId;
	}

	public String getTraBankId() {
		return traBankId;
	}

	public void setTraBankId(String traBankId) {
		this.traBankId = traBankId;
	}

	public String getTraAccount() {
		return traAccount;
	}

	public void setTraAccount(String traAccount) {
		this.traAccount = traAccount;
	}

	public String getIcDateTime() {
		return icDateTime;
	}

	public void setIcDateTime(String icDateTime) {
		this.icDateTime = icDateTime;
	}

	public String getIcTxnNo() {
		return icTxnNo;
	}

	public void setIcTxnNo(String icTxnNo) {
		this.icTxnNo = icTxnNo;
	}

	public String getIcBankInfo() {
		return icBankInfo;
	}

	public void setIcBankInfo(String icBankInfo) {
		this.icBankInfo = icBankInfo;
	}

	public String getTac() {
		return tac;
	}

	public void setTac(String tac) {
		this.tac = tac;
	}

	public String getPayIdnBan() {
		return payIdnBan;
	}

	public void setPayIdnBan(String payIdnBan) {
		this.payIdnBan = payIdnBan;
	}

	public String getOrgTxnDateTime() {
		return orgTxnDateTime;
	}

	public void setOrgTxnDateTime(String orgTxnDateTime) {
		this.orgTxnDateTime = orgTxnDateTime;
	}

	public String getOrgApiTxnNo() {
		return orgApiTxnNo;
	}

	public void setOrgApiTxnNo(String orgApiTxnNo) {
		this.orgApiTxnNo = orgApiTxnNo;
	}

	public String getOrgRcode() {
		return orgRcode;
	}

	public void setOrgRcode(String orgRcode) {
		this.orgRcode = orgRcode;
	}

	public String getOrgStan() {
		return orgStan;
	}

	public void setOrgStan(String orgStan) {
		this.orgStan = orgStan;
	}

	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}

	public String getCvv2() {
		return cvv2;
	}

	public void setCvv2(String cvv2) {
		this.cvv2 = cvv2;
	}

	public String getAcqBin() {
		return acqBin;
	}

	public void setAcqBin(String acqBin) {
		this.acqBin = acqBin;
	}

	public String getRrn() {
		return rrn;
	}

	public void setRrn(String rrn) {
		this.rrn = rrn;
	}

	public String getAuthCode() {
		return authCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}
}
