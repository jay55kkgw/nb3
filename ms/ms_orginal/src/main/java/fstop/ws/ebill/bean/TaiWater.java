package fstop.ws.ebill.bean;

public class TaiWater extends BillData {

	//CHECK_INFO
	//TXN_INFO
	private String paymentDeadLine;
	private String noticeNo;
	private String checkNo;
	private String txnAmount;

	public String getPaymentDeadLine() {
		return paymentDeadLine;
	}
	public void setPaymentDeadLine(String paymentDeadLine) {
		this.paymentDeadLine = paymentDeadLine;
	}
	public String getNoticeNo() {
		return noticeNo;
	}
	public void setNoticeNo(String noticeNo) {
		this.noticeNo = noticeNo;
	}
	public String getCheckNo() {
		return checkNo;
	}
	public void setCheckNo(String checkNo) {
		this.checkNo = checkNo;
	}
	public String getTxnAmount() {
		return txnAmount;
	}
	public void setTxnAmount(String txnAmount) {
		this.txnAmount = txnAmount;
	}
}
