package fstop.ws.ebill;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.transform.stream.StreamSource;

import com.linkway.isecurity.SecurityUtil;
import com.netbank.util.fstop.DateUtil;
import com.tbb.ebill.xml.Envelope;
import com.tbb.ebill.xml.MacInfo;

import encryptUtil.EncryptUtil;
import fstop.va.VaCheck;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GenXML {

	public static Envelope parseRepXmlMsg(Class<?>[] c, String rcvEbillXmlMsg) throws Exception {
		JAXBContext jaxbContext = JAXBContext.newInstance(c);
		javax.xml.bind.Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

		StreamSource source = new StreamSource(new java.io.StringReader(rcvEbillXmlMsg));
		JAXBElement<Envelope> envelop = jaxbUnmarshaller.unmarshal(source, Envelope.class);

		return envelop.getValue();
	}
	
	public static String genReqXmlMsg(Class<?>[] c, Object ebilldata) throws Exception{
		StringWriter sw = new StringWriter();
		JAXBContext jaxbContext = JAXBContext.newInstance(c);
		javax.xml.bind.Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, false);
		jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
		jaxbMarshaller.marshal(ebilldata, sw);
		String sendEbillXmlMsg = new String(sw.toString());
		log.debug(sendEbillXmlMsg);
		return sendEbillXmlMsg;
	}

	private static byte[] rcvEbillXmlGetMessage(String rcvEbillXml) throws Exception {
		byte[] message = null;
		rcvEbillXml = rcvEbillXml.replace("<ENVELOPE>", "");
		rcvEbillXml = rcvEbillXml.substring(0, rcvEbillXml.lastIndexOf("<MAC_INFO>"));
		message = rcvEbillXml.getBytes();

		return message;
	}
	
	public static boolean verifyMac(MacInfo macInfo, String rcvEbillXmlMsg, String vaChannel, String ebillKeyID) throws Exception {
		log.info("verifyMac fisc...");
		String keyLabel = "";
		String mac = "";
		byte[] divData = null;
		byte[] icv = null;
		byte[] macMessage = null;
		byte[] sourceData = null;
		byte[] macKey = null;
		byte[] macData = null;
		try {
			keyLabel = ebillKeyID;
			
			log.debug("keyLabel..."+keyLabel);
			divData = hex2Byte(macInfo.getDivData().getBytes());
			log.debug("divData..."+new String(divData));
			icv = hex2Byte(macInfo.getIcv().getBytes());
			log.debug("icv..."+new String(icv));
			//MacKey
			macKey = genMacKey(keyLabel, divData, vaChannel, ebillKeyID);
			log.debug("macKey..."+new String(macKey));
			//MacData
			macMessage = rcvEbillXmlGetMessage(rcvEbillXmlMsg);
			log.debug("macMessage: " + new String(macMessage));
			sourceData = sha256(macMessage);
			log.debug("sourceData: " + new String(sourceData));
			macData = genMac(keyLabel, macKey, icv, sourceData, vaChannel, ebillKeyID);
			log.debug("macData: " + new String(macData));
			//取運算結果(MAC)最後8bytes的前4bytes
			mac = get4byteMac(macData);
			log.debug("mac: " + mac);
			log.debug("(HEX) macInfo.getMac(): " + macInfo.getMac());
			
			if (macInfo.getMac().equals(mac)){
				log.info("verifyMac fisc ok !!");
				return true;
			} else {
				log.info("verifyMac fisc fales !!");
				return false;
			}

		} catch (Exception e) {
			log.error("[EbillVrfyMac.verifyMac] occur Exception: " + e.toString());
			return false;
		}
	}
	
	public static void getMac(MacInfo macInfo, String macMessage, String vaChannel, String ebillKeyID) throws Exception {
		log.info("gen fisc Mac...");
		String keyLabel = "";
		String mac = "";
		byte[] divData = null;
		byte[] icv = null;
		byte[] sourceData = null;
		byte[] macKey = null;
		byte[] macData = null;
		try {
			log.debug("keyLabel...");
			keyLabel = ebillKeyID;//env
			log.debug("divData...");
			divData = genDivData();
			log.debug("icv...");
			icv = genIcv();

			//MacKey
			log.debug("macKey...");
			macKey = genMacKey(keyLabel, divData, vaChannel, ebillKeyID);

			//MacData
			log.debug("macData...");
			log.debug("sourceData: " + new String(macMessage));
			sourceData = sha256(macMessage.getBytes());
			macData = genMac(keyLabel, macKey, icv, sourceData, vaChannel, ebillKeyID);

			//取運算結果(MAC)最後8bytes的前4bytes
			log.debug("get4byteMac...");
			mac = get4byteMac(macData);
			log.debug("mac..."+mac);
			macInfo.setKeyId(keyLabel.substring(1,8)); //取1-8位B050V001JTKSMACO -> 050V001
			macInfo.setDivData(new String(byte2Hex(divData)));
			macInfo.setIcv(new String(byte2Hex(icv)));
			macInfo.setMac(mac);
			log.info("gen fisc Mac ok!!");

		} catch (Exception e) {
			log.error("[EbillMac.getMac] occur Exception: " + e.toString());
			throw e;
		}
	}

	public static byte[] sha256(byte[] sourceData) throws Exception {
//		MessageDigest digest = MessageDigest.getInstance("SHA-256");
//		byte[] hash = digest.digest(sourceData);
//
//		return hash;
		byte[] hash = EncryptUtil.getData(sourceData);
		return hash;
	}
	
//	protected static byte[] genSecureRandom(int lens) throws Exception {
//		SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
//		byte[] seed = random.generateSeed(random.nextInt(55));
//		random.setSeed(seed);
//		byte bytes[] = new byte[lens];
//		random.nextBytes(bytes);
//
//		return bytes;
//	}
	public static byte[] genDivData() throws Exception {
		String date =DateUtil.getCurentDateTime("yyyyMMdd");
		String diva=String.format("%012d",  Long.parseLong(date));
		return diva.getBytes();
	}

	public static byte[] genIcv() throws Exception {
		String date =DateUtil.getCurentDateTime("yyyyMMdd");
		String icv=String.format("%08d",  Long.parseLong(date));
		return icv.getBytes();
	}

	protected static String get4byteMac(byte[] macData) throws Exception {
		byte[] mac = new byte[4];

		//取運算結果(MAC)最後8bytes的前4bytes
//		LOG.debug("get last 8 bytes before 4 bytes Mac...");
//		LOG.debug("(HEX) macData: " + new String(byte2Hex(macData)));
		System.arraycopy(macData, macData.length-8, mac, 0, 4);
//		LOG.debug("(HEX) 4 bytes mac: " + new String(byte2Hex(mac)));
//		LOG.debug("get last 8 bytes before 4 bytes Mac ok !!");

		return new String(byte2Hex(mac));
	}
	
	protected static byte[] genMac(String keyLabel, byte[] keyValue, byte[] icv, byte[] sourceData, String vaChannel, String ebillKeyID) throws Exception {
		byte[] macData = null;

		//LOG.debug("Macdata...");
		long startTime = System.currentTimeMillis();
		if (checkVA_CHANNEL(keyLabel, vaChannel) || ebillKeyID.equals(keyLabel)){
			macData = VaCheck.genVaMac(keyValue, icv, sourceData);
		} else {
			String macMethod = "DES2";
			int startPos = 0;
			macData = (new SecurityUtil()).dataMACByExplictKey(macMethod, keyValue, icv, sourceData, startPos, sourceData.length); //iSecurity
		}
		log.debug("(HEX) Macdata: " + new String(byte2Hex(macData)) + " | genMac_elapsedTime:" + (System.currentTimeMillis()-startTime));
		//LOG.debug("Macdata ok !!");

		return macData;
	}

	protected static byte[] genMacKey(String keyLabel, byte[] divData, String vaChannel, String ebillKeyID) throws Exception {
		String encMethod = "DES2";
		int startPos = 0;
		byte[] macKey = null;

		//LOG.debug("MacKey()...");
		long startTime = System.currentTimeMillis();
		log.debug("EbillSecurity keyLabel:"+keyLabel);
		log.debug("EbillSecurity AppEnv.getVA_CHANNEL:"+vaChannel);//env
		log.debug("EbillSecurity AppEnv.getEbillKeyId:"+ebillKeyID);//env
		if (checkVA_CHANNEL(keyLabel, vaChannel) || ebillKeyID.equals(keyLabel)){
			log.debug("checkVA_CHANNEL AppEnv.getEbillKeyId() keyLabel:"+keyLabel);
			macKey = VaCheck.genVaMacKey(keyLabel, divData);
		} else {
			log.debug("not checkVA_CHANNEL AppEnv.getEbillKeyId() keyLabel:"+keyLabel);			
			macKey = (new SecurityUtil()).encrypt(encMethod, keyLabel, divData, startPos, divData.length); //iSecurity
		}
		log.debug("(HEX) MacKey: " + new String(byte2Hex(macKey)) + " | genMacKey_elapsedTime:" + (System.currentTimeMillis()-startTime));
		//LOG.debug("MacKey ok !!");

		return macKey;
	}

	private static boolean checkVA_CHANNEL(String keyLabel, String vaChannel) throws Exception {
		for (String va:vaChannel.split(",")){
			if (va.equals(keyLabel)){
				return true;
			}
		}
		return false;
	}
	public static byte[] byte2Hex(byte[] bin) throws Exception {
		return com.linkway.isecurity.util.Utility.bin2Hex(bin);
	}

	public static byte[] hex2Byte(byte[] hex) throws Exception {
		return com.linkway.isecurity.util.Utility.hex2Bin(hex);
	}

}
