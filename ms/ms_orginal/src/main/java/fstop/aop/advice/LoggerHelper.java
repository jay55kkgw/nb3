package fstop.aop.advice;

import java.util.Date;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;

import fstop.orm.dao.SysLogDao;
import fstop.orm.dao.TxnLogDao;
import fstop.orm.po.SYSLOG;
import fstop.orm.po.TXNLOG;
//import fstop.services.batch.bean.DETAILCmd;
//import fstop.services.batch.bean.INITCmd;
//import fstop.services.batch.bean.PARAMSCmd;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LoggerHelper {
	
	private static ThreadLocal<LoggerObject> threadLoggerObject = new ThreadLocal();

	private static ThreadLocal<String> threadGUID = new ThreadLocal();
	
	private static Logger getSysLogLogger() {
		return Logger.getLogger("fstop_syslog");
	}
	
	private static Logger getLoggerHelperLogger() {
		return Logger.getLogger(LoggerHelper.class);
	}
	
	public static void initial() {
		if (threadGUID.get() == null) {
			threadGUID.set(UUID.randomUUID().toString());

			LoggerObject loggerObj = new LoggerObject();
			loggerObj.setGUID(threadGUID.get());
			threadLoggerObject.set(loggerObj);
		}
	}

	public static LoggerObject current() {

		LoggerObject loggerObj = threadLoggerObject.get();
		if(loggerObj == null) {
			LoggerHelper.initial();
			loggerObj = threadLoggerObject.get();
		}
		return loggerObj;
	}
	
	public static void clear() {
		getSysLogLogger().debug("clear .........");
		LoggerObject obj = LoggerHelper.current();
		obj.setEndTime(new Date());
		String str = String.format("- [DETAIL] [GUID:%s] [UID:%s] [ADOPID:%s] [BONAME:%s] [METHOD:%s] [IP:%s] [JSP:%s] [ST:%s] [ET:%s] [EXCODE:%s]"
				, obj.getGUID()
				, obj.getUid()
				, obj.getAdopid()
				, obj.getBoname()
				, obj.getMethodName()
				, obj.getRemoteIP()
				, obj.getJsp()
				, DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", obj.getStartTime())
				, DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", obj.getEndTime())
				, obj.getExceptionCode());
		
		
		getSysLogLogger().info(str);
		getSysLogLogger().info("[FINISH] " + threadGUID.get());
		
		Map<String, String> paramsMap = prepare(obj);
//批次不寫入SYSLOG 20200130
//		try {
//			boolean isSuccess = insertToSysLog(obj, paramsMap);
//		}
//		catch(Exception e){}
		
		//若是TXNLOG = TRUE 或 有失敗
		if("TRUE".equalsIgnoreCase(paramsMap.get("#TXNLOG")) ||
				StrUtils.isNotEmpty(obj.getExceptionCode())) {
			try {
				boolean isSuccess = insertToTxnLog(obj, paramsMap);
			}
			catch(Exception e){
				
			}
		}
		
		threadGUID.set(null);
		threadLoggerObject.set(null);
	}

	private static Map<String, String> prepare(LoggerObject obj) {
		return JSONUtils.json2map((StrUtils.isEmpty(obj.getRequestData()) ? "{}" : obj.getRequestData()));
	}

	private static boolean insertToSysLog(LoggerObject obj, final Map<String, String> paramsMap) {
		SysLogDao sysLogDao = (SysLogDao)SpringBeanFactory.getBean("sysLogDao");
		SYSLOG syslog = new SYSLOG();
		boolean isSuccess = false;
		try {
			syslog.setADUSERID(StrUtils.trim(paramsMap.get("UID")));
			syslog.setADFDATE(DateTimeUtils.format("yyyyMMdd", obj.getStartTime()));
			syslog.setADFTIME(DateTimeUtils.format("HHmmss", obj.getStartTime()));
			syslog.setADTDATE(DateTimeUtils.format("yyyyMMdd", obj.getEndTime()));
			syslog.setADTTIME(DateTimeUtils.format("HHmmss", obj.getEndTime()));
			syslog.setADSEQ(1);
			String uid = StrUtils.trim(paramsMap.get("UID"));
			if(StrUtils.isEmpty(uid)) {
				uid = StrUtils.repeat(" ", 10);
				return true;
			}
			syslog.setADTXNO(UUID.randomUUID().toString());
			syslog.setADUSERIP(StrUtils.trim(obj.getRemoteIP()));
			syslog.setADPAGENM(StrUtils.trim(obj.getJsp()));
			syslog.setADOPID(StrUtils.trim(obj.getAdopid()));
			syslog.setADBONM(StrUtils.trim(obj.getBoname()));
			syslog.setADMETHODNM(StrUtils.trim(obj.getMethodName()));
			syslog.setADEXCODE(StrUtils.trim(obj.getExceptionCode()));
			syslog.setADLOGTYPE("1");
			if(obj != null) {
				
//				if (BOLifeMoniter.getValue("STAN") != null) {					
//					Map m = JSONUtils.json2map(obj.getRequestData());
//					m.put("STAN", BOLifeMoniter.getValue("STAN"));
//					obj.setRequestData(JSONUtils.map2json(m));
//				}								
					
				syslog.setADCONTENT(StrUtils.trim(obj.getRequestData()));
				
				syslog.setFGTXWAY(StrUtils.trim(paramsMap.get("FGTXWAY")));
				syslog.setADTXTYPE(StrUtils.trim(paramsMap.get("#ADTXTYPE")));
				syslog.setADLOGGING(StrUtils.trim(paramsMap.get("#ADLOGGING")));

				try {
					syslog.setADTXACNO(StrUtils.trim(paramsMap.get(StrUtils.trim(paramsMap.get("#ADTXACNO")))));
				}
				catch(Exception e){}
				try {
					syslog.setADTXAMT(StrUtils.trim(paramsMap.get(StrUtils.trim(paramsMap.get("#ADTXAMT")))));
				}
				catch(Exception e){}
				try {
					syslog.setADCURRENCY(StrUtils.trim(paramsMap.get(StrUtils.trim(paramsMap.get("#ADCURRENCY")))));
				}
				catch(Exception e){}
				
			}
			
			syslog.setADGUID(obj.getGUID());
			sysLogDao.save(syslog);
			isSuccess = true;
			getLoggerHelperLogger().debug("寫入 SYSLOG 成功 (UUID: " + obj.getGUID() + ").");
		}
		catch(Exception e) {
			try {
				getLoggerHelperLogger().error("SysLog Object :" + JSONUtils.toJson(syslog));
			}
			catch(Exception ex){}
			getLoggerHelperLogger().error("寫入 SYSLOG 錯誤 (UUID: " + obj.getGUID() + ").", e);
		}
		finally {
			
		}
		return isSuccess;
	}
	
	private static boolean insertToTxnLog(LoggerObject obj, final Map<String, String> paramsMap) {
		TxnLogDao txnLogDao = (TxnLogDao)SpringBeanFactory.getBean("txnLogDao");
		TXNLOG txnlog = new TXNLOG();
		boolean isSuccess = false;
		try {
			String uid = StrUtils.trim(paramsMap.get("UID"));
			if(StrUtils.isEmpty(uid)) {
				uid = StrUtils.repeat(" ", 10);
				return true;
			}
			txnlog.setADTXNO(UUID.randomUUID().toString());
			txnlog.setADUSERID(uid);
			txnlog.setADUSERIP("127.0.0.1");
			txnlog.setADOPID(StrUtils.trim(obj.getAdopid()));
			if("EUMP".equalsIgnoreCase(obj.getExceptionCode())){
				txnlog.setADEXCODE("");
			}else {
				txnlog.setADEXCODE(StrUtils.trim(obj.getExceptionCode()));
			}
			
			if(obj != null) {
				
//				if (BOLifeMoniter.getValue("FXADTXNO") != null) {					
//					Map m = JSONUtils.json2map(obj.getRequestData());
//					m.put("FXADTXNO", BOLifeMoniter.getValue("FXADTXNO"));
//					obj.setRequestData(JSONUtils.map2json(m));
//				}		
				
				txnlog.setADCONTENT(StrUtils.trim(obj.getRequestData()));
				
				txnlog.setFGTXWAY(StrUtils.trim(paramsMap.get("FGTXWAY")));   //從畫面來的欄位
				
				txnlog.setADREQTYPE(StrUtils.trim(paramsMap.get("ADREQTYPE")));  //從畫面來的欄位
				
				txnlog.setLOGINTYPE(StrUtils.trim(paramsMap.get("LOGINTYPE")));
				// ADTXACNO、ADTXAMT、ADCURRENCY、ADSVBH、ADAGREEF
				// paramsMap.get(sKey) 是XML裡的指定欄位名稱
				String[] sKeys = new String[] {"#ADTXACNO", "#ADTXAMT", "#ADCURRENCY", "#ADSVBH", "#ADAGREEF"};   //從 Transaction XML 來的欄位
				for(int i=0; i < sKeys.length; i++) {
					try {
						String sKey = sKeys[i];
						String v = "";
						if(v.length() == 0 && !paramsMap.containsKey(paramsMap.get(sKey))) {
							v = StrUtils.trim(paramsMap.get(sKey));
						}
						else {
							v = StrUtils.trim(paramsMap.get(StrUtils.trim(paramsMap.get(sKey))));
						}
						switch (i) {
						case 0:
							txnlog.setADTXACNO(v);
							break;
						case 1:
							if (v.startsWith("0") && 
							    (txnlog.getADOPID().equals("F001") || txnlog.getADOPID().equals("F002") || txnlog.getADOPID().equals("F003"))) 
							{
								double d_TXAMT = Double.parseDouble(v);
								d_TXAMT = d_TXAMT / 100;
								v = String.valueOf(d_TXAMT);
							}
							
							txnlog.setADTXAMT(v);
							break;
						case 2:
							txnlog.setADCURRENCY(v);
							break;
						case 3:
							txnlog.setADSVBH(v);
							break;
						case 4:
							txnlog.setADAGREEF(v);
							break;
						}
					}
					catch(Exception e){}
				}
				
			}
			Date d = new Date();
			txnlog.setLASTDATE(DateTimeUtils.format("yyyyMMdd", d));
			txnlog.setLASTTIME(DateTimeUtils.format("HHmmss", d));
//			txnlog.setADGUID(obj.getGUID());
			log.debug("getADTXNO=" + txnlog.getADTXNO());
			log.debug("getADUSERID=" + txnlog.getADUSERID());
			log.debug("getADOPID=" + txnlog.getADOPID());
			log.debug("getFGTXWAY=" + txnlog.getFGTXWAY());
			log.debug("getADTXACNO=" + txnlog.getADTXACNO());
			log.debug("getADTXAMT=" + txnlog.getADTXAMT());
			log.debug("getADCURRENCY=" + txnlog.getADCURRENCY());
			log.debug("getADSVBH=" + txnlog.getADSVBH());
			log.debug("getADAGREEF=" + txnlog.getADAGREEF());
			log.debug("getADREQTYPE=" + txnlog.getADREQTYPE());
			log.debug("getADUSERIP=" + txnlog.getADUSERIP());
			log.debug("getADCONTENT=" + txnlog.getADCONTENT());
			log.debug("getADEXCODE=" + txnlog.getADEXCODE());
//			log.debug("getADGUID=" + txnlog.getADGUID());
			log.debug("getADTXID=" + txnlog.getADTXID());
			log.debug("getLASTDATE=" + txnlog.getLASTDATE());
			log.debug("getLASTTIME=" + txnlog.getLASTTIME());
			
			txnLogDao.save(txnlog);
			isSuccess = true;
			getLoggerHelperLogger().debug("寫入 TXNLOG 成功 (UUID: " + obj.getGUID() + ").");
		}
		catch(Exception e) {
			try {
				getLoggerHelperLogger().error("TxnLog Object :" + JSONUtils.toJson(txnlog));
			}
			catch(Exception ex){}
			getLoggerHelperLogger().error("寫入 TXNLOG 錯誤 (UUID: " + obj.getGUID() + ").", e);
		}
		finally {
			
		}
		return isSuccess;
	}
	
	public static void resetLogObject() {
		threadGUID.set(null);
		threadLoggerObject.set(null);
	}
	
	
}
