package fstop.aop.advice;

import java.util.Date;

public class LoggerObject {
	
	public boolean isLogging = false;
	
	private String GUID = ""; 

	private String uid = "";
	
	private String adopid = "";

	private String jsp = "";

	private String remoteIP = "";

	private String boname = "";
	
	private String requestData = "";
	
	private String methodName = "";
	
	private String exceptionCode = "";
	
	private Date startTime = new Date();

	private Date endTime = new Date();

	public boolean isLogging() {
		return isLogging;
	}

	public void setLogging(boolean isLogging) {
		this.isLogging = isLogging;
	}

	public String getExceptionCode() {
		return exceptionCode;
	}
 
	public void setExceptionCode(String exceptionCode) {
		this.exceptionCode = exceptionCode;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public String getRequestData() {
		return requestData;
	}

	public void setRequestData(String requestData) {
		this.requestData = requestData;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getBoname() {
		return boname;
	}

	public void setBoname(String boname) {
		this.boname = boname;
	}

	public String getRemoteIP() {
		return remoteIP;
	}

	public void setRemoteIP(String remoteIP) {
		this.remoteIP = remoteIP;
	}

	public String getJsp() {
		return jsp;
	}

	public void setJsp(String jsp) {
		this.jsp = jsp;
	}

	public String getAdopid() {
		return adopid;
	}

	public void setAdopid(String adopid) {
		this.adopid = adopid;
	}

	public String getGUID() {
		return GUID;
	}

	public void setGUID(String guid) {
		GUID = guid;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
}
