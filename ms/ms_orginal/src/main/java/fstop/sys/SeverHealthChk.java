package fstop.sys;

import org.hibernate.ObjectNotFoundException;

import fstop.orm.dao.AdmNbStatusDao;
import fstop.orm.po.ADMNBSTATUS;
import fstop.util.SpringBeanFactory;
import lombok.extern.slf4j.Slf4j;
import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;

@Slf4j
public class SeverHealthChk {
	
	private static String cacheKey = "SeverHealthChk.ADNBSTATUS";
	
	public static void setADNBSTATUS(String status) {
		
		Cache cache = (Cache)SpringBeanFactory.getBean("distributeCache");
		Element elm = new Element(cacheKey, status);
		cache.put(elm);
		
	}
	
	public static String getADNBSTATUS() {
		Cache cache = (Cache)SpringBeanFactory.getBean("distributeCache");
		Element elm = cache.get(cacheKey);
		if(elm != null) {			
			
			log.debug("SeverHealthChk.getADNBSTATUS() in cache == "+(String)elm.getValue());

			return (String)elm.getValue();			
		}
		else {

			log.debug("SeverHealthChk.getADNBSTATUS() not in cache !");
			
			String status = "N";
			AdmNbStatusDao admNbStatusDao = (AdmNbStatusDao)SpringBeanFactory.getBean("admNbStatusDao");
			ADMNBSTATUS po = null;
			try {
				po = admNbStatusDao.findById("TBBNB");
			}
			catch(ObjectNotFoundException e) {
				return "N";
			}
			
			if (po != null) {
				status = po.getADNBSTATUS();
			}
			
			elm = new Element(cacheKey, status);
			cache.put(elm);
			
			return status;
		}
	}

}
