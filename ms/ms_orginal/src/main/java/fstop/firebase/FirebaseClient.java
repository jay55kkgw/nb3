package fstop.firebase;

import java.io.FileInputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.google.api.services.storage.model.Notification;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.AndroidConfig;
import com.google.firebase.messaging.AndroidNotification;
import com.google.firebase.messaging.ApnsConfig;
import com.google.firebase.messaging.Aps;
import com.google.firebase.messaging.FcmOptions;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.Message;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.netbank.rest.comm.batch.FirebaseBean;
import com.netbank.rest.util.RESTUtil;

import fstop.notification.PushData;
import fstop.util.SpringBeanFactory;
import fstop.ws.AIOWebServiceTemplate;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FirebaseClient {
	
	public static String FIREBASE_FCM_URL = "https://fcm.googleapis.com/fcm/send";

	public static FirebaseBean firebaseValue() {
		return (FirebaseBean) SpringBeanFactory.getBean("firebaseBean");
	}

	public static RESTUtil restUtil() {
		return (RESTUtil) SpringBeanFactory.getBean("RESTUtil");
	}
	public static String sendMessage(String token, Map<String, String> datas) {
		String response = "";
		try {
			try {
				FirebaseApp.getInstance();
			}catch(IllegalStateException e) {
//				Resource resource = new ClassPathResource("firebase.json");
				FileInputStream serviceAccount = new FileInputStream(firebaseValue().fbEnv.getJsonPath());
			
				FirebaseOptions options = new FirebaseOptions.Builder()
//				  .setCredentials(GoogleCredentials.fromStream(resource.getInputStream()))
				  .setCredentials(GoogleCredentials.fromStream(serviceAccount))
//				  .setDatabaseUrl(firebaseValue().fbEnv.getUrl())
				  .setConnectTimeout(firebaseValue().fbEnv.getConnectTimeout())
				  .build();
				
				FirebaseApp.initializeApp(options);
			}
			Message message = Message.builder()
					.putAllData(datas)
				    .setToken(token)
				    .build();
			response = FirebaseMessaging.getInstance().send(message);
			log.trace("response >> {}", response);
		}catch(Exception e) {
			log.error("{}",e);
			
		}
		return response;
	}
	
	public static String sendOldMessage(Map<String, Object> datas) {
		String response = "";
		try {
			response = restUtil().send2fcm(datas, FIREBASE_FCM_URL, firebaseValue().fbEnv.getConnectTimeout(), firebaseValue().fbEnv.getFirebaseKey());
		}catch(Exception e) {
			log.error("{}",e);
			
		}
		return response;
		
	}
	//Android
	public static void sendMessageForA(List<PushData> listPushData) {
		try {
			if(firebaseValue().fbEnv.getUseType().equals("new")) {
				for(PushData pushData: listPushData) {
					Map<String,String> data = new HashMap<String,String>();
					data.put("Topic", pushData.getTopic());
					data.put("Message", pushData.getMessage());
					data.put("CusId", pushData.getCusId());
					FirebaseClient.sendMessage(pushData.getToken(),data);
				}
			}else {
				for(PushData pushData: listPushData) {
					Map<String,Object> message = new HashMap<String,Object>();
					Map<String,Object> data = new HashMap<String,Object>();
					data.put("Topic", pushData.getTopic());
					data.put("Message", pushData.getMessage());
					data.put("CusId", pushData.getCusId());
					message.put("data", data);
					message.put("notification", pushData.getNotification());
					message.put("to",pushData.getToken());
					FirebaseClient.sendOldMessage(message);
				}
			}
		}catch(Exception e) {
			log.error("{}",e);
		}
	} 
	//IOS
	public static void sendMessageForI(List<PushData> listPushData) {
		try {
			if(firebaseValue().fbEnv.getUseType().equals("new")) {
				for(PushData pushData: listPushData) {
					Map<String,String> data = new HashMap<String,String>();
					data.put("Topic", pushData.getTopic());
					data.put("Message", pushData.getMessage());
					data.put("CusId", pushData.getCusId());
					FirebaseClient.sendMessage(pushData.getToken(),data);
				}
			}else {
				for(PushData pushData: listPushData) {
					Map<String,Object> message = new HashMap<String,Object>();
					Map<String,Object> data = new HashMap<String,Object>();
					Map<String,Object> aaps = new HashMap<String,Object>();
					aaps.put("badge", String.valueOf(0));
					aaps.put("alert", pushData.getMessage());
					aaps.put("sound", "default");
					data.put("mtLink", "");
					data.put("mtId", String.valueOf(0));
					data.put("aaps", aaps);
					data.put("type", "5");
					message.put("data", data);
					message.put("notification", pushData.getNotification());
					message.put("to",pushData.getToken());
					FirebaseClient.sendOldMessage(message);
				}
			}
		}catch(Exception e) {
			log.error("{}",e);
		}
	}
	//Android
		public static void sendMessage(List<PushData> listPushData) {
			try {
				if(firebaseValue().fbEnv.getUseType().equals("new")) {
					for(PushData pushData: listPushData) {
						Map<String,String> data = new HashMap<String,String>();
						data.put("Topic", pushData.getTopic());
						data.put("Message", pushData.getMessage());
						data.put("CusId", pushData.getCusId());
						FirebaseClient.sendMessage(pushData.getToken(),data);
					}
				}else {
					for(PushData pushData: listPushData) {
						Map<String,Object> message = new HashMap<String,Object>();
						Map<String,Object> data = new HashMap<String,Object>();
						data.put("Topic", pushData.getTopic());
						data.put("Message", pushData.getMessage());
						data.put("CusId", pushData.getCusId());
						message.put("data", data);
						message.put("notification", pushData.getNotification());
						message.put("to",pushData.getToken());
						FirebaseClient.sendOldMessage(message);
					}
				}
			}catch(Exception e) {
				log.error("{}",e);
			}
		}

	public static void sendMessage(PushData pushData) {
		try {
			if(firebaseValue().fbEnv.getUseType().equals("new")) {
				Map<String,String> data = new HashMap<String,String>();
				data.put("Topic", pushData.getTopic());
				data.put("Message", pushData.getMessage());
				data.put("CusId", pushData.getCusId());
				FirebaseClient.sendMessage(pushData.getToken(),data);
			}else {
				Map<String,Object> message = new HashMap<String,Object>();
				Map<String,Object> data = new HashMap<String,Object>();
				data.put("Topic", pushData.getTopic());
				data.put("Message", pushData.getMessage());
				data.put("CusId", pushData.getCusId());
				message.put("data", data);
				message.put("to",pushData.getToken());
				FirebaseClient.sendOldMessage(message);
			}
		}catch(Exception e) {
			log.error("{}",e);
		}
	} 
	
//	public static void main(String[] args) {
//		FirebaseClient client = new FirebaseClient();
//		Map<String, String> datas = new HashMap<String, String>();
//		datas.put("aaa", "aa");
//		client.sendMessage("em0pQhWWHF9QQ2RcQRlWdW:APA91bG6deZacmarK35yejVOdNYABmRtCXLNMFyorDKigGDWTdDHdAdbdtk5lOcbgOyhz8-9yPOOwK7G12FEBnQDnUxf27R3KS5gruEwIv9ADCliYqktcCPEXCqfDfh5zpSMnp0uGciE", datas);
//	}
}
