package fstop.notification;

import java.util.Map;

public class PushData {
	String Topic;
	String Message;
	String CusId;
	String Token;
	Map<String, String> notification;
	
	public Map<String, String> getNotification() {
		return notification;
	}
	public void setNotification(Map<String, String> notification) {
		this.notification = notification;
	}
	public String getToken() {
		return Token;
	}
	public void setToken(String token) {
		Token = token;
	}
	public String getMessage() {
		return Message;
	}
	public void setMessage(String message) {
		Message = message;
	}
	public String getTopic() {
		return Topic;
	}
	public void setTopic(String topic) {
		Topic = topic;
	}
	public String getCusId() {
		return CusId;
	}
	public void setCusId(String cusId) {
		CusId = cusId;
	}
	
	
}
