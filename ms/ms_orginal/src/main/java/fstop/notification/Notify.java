package fstop.notification;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttTopic;
import org.eclipse.paho.client.mqttv3.persist.MqttDefaultFilePersistence;
import org.springframework.beans.factory.annotation.Value;

import com.netbank.rest.comm.batch.BhEnvBean;
import com.netbank.rest.comm.batch.NotifyValueBean;
import com.netbank.util.ESAPIUtil;

import fstop.orm.dao.NFLogDao;
import fstop.orm.po.NFLOG;
import fstop.util.DateTimeUtils;
import fstop.util.SpringBeanFactory;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Notify{
	
//	@Value("${notify_clientId}")
//	private String clientId;
//	@Value("${notify_acount}")
//	private String userName;
//	@Value("${notify_pd}")
//	private String pd;
//	@Value("${notify_host}")
//	private String notifyhost;
//	@Value("${notify_port}")
//	private String notifyport;
	
	public static NotifyValueBean notifyValue() {
		return (NotifyValueBean) SpringBeanFactory.getBean("notifyValueBean");
		
	}
	private String clientId = notifyValue().notifyValueProperties.getClientId();
	private String userName = notifyValue().notifyValueProperties.getUserName();
	private String pd = notifyValue().notifyValueProperties.getPd();
	private String notifyhost = notifyValue().notifyValueProperties.getNotifyhost();
	private String notifyport = notifyValue().notifyValueProperties.getNotifyport();
	
	
	
	private MqttClient client;
	
	
	private String brokerUrl;
	private boolean quietMode = false;
	private boolean cleanSession = false;
	private boolean retained = false;		// 是否保留訊息
	private int qos = 2;
	
	public Notify(){
		
	}
	
	public void publish(PushData pushData) throws UnsupportedEncodingException, MqttException {
		publish(pushData.getTopic(), pushData.getMessage());
	}
	
	public void publish(String topicName, String message) throws UnsupportedEncodingException, MqttException {
		byte[] payload = message.getBytes("utf-8");
		publish(topicName, qos, payload);
	}
		
	 /**
     * Performs a single publish
     * @param topicName the topic to publish to
     * @param qos the qos to publish at
     * @param payload the payload of the message to publish 
     * @throws MqttException
     * @throws UnsupportedEncodingException 
     */
	//加上 synchronized，透過Web Service Thread同時呼叫多次時，才不會出現 MQTT 連線中斷的問題
	synchronized private void publish(String topicName, int qos, byte[] payload) throws MqttException, UnsupportedEncodingException {
		init();
	
    	MqttConnectOptions conOpt;
    	conOpt = new MqttConnectOptions();
    	conOpt.setCleanSession(cleanSession);
    	conOpt.setUserName(userName);
    	conOpt.setPassword(pd.toCharArray());    	
    	// byte[] payload1 = new byte[0];	// 0長度byte陣列，可取消保留的訊息
    	// conOpt.setWill("topic/aaa", payload1, 2, false);
    	
    	// Connect to the server
    	client.connect(conOpt);
    	log.debug("Connected to "+brokerUrl);
    	
    	// Get an instance of the topic
    	MqttTopic topic = client.getTopic(topicName);
    	// Construct the message to publish
    	MqttMessage message = new MqttMessage(payload);
    	message.setQos(qos);
    	message.setRetained(retained);	
    	
    	// Publish the message
    	log.debug("Publishing to topic \""+topicName+"\" qos "+qos);
    	MqttDeliveryToken token = topic.publish(message);

    	// Wait until the message has been delivered to the server
    	token.waitForCompletion();

    	// Disconnect the client
    	client.disconnect();
    	log.debug("Disconnected");
    	
    }
	
	//	加上 synchronized，透過Web Service Thread同時呼叫多次時，才不會出現 MQTT 連線中斷的問題
	synchronized public void publish(List<PushData> listTopicMessage) throws MqttException, UnsupportedEncodingException {
		init();
//		File a = new File("/tmp/TBBNNB-tcp1016221271883/s-2.msg");
//		a.delete();
//		deleteDirectory(new File("/tmp/TBBNNB-tcp1016221271883"));
		String tmpTopic;
    	MqttConnectOptions conOpt;
    	conOpt = new MqttConnectOptions();
    	conOpt.setCleanSession(cleanSession);
    	conOpt.setUserName(userName);
    	conOpt.setPassword(pd.toCharArray());    	
    	// byte[] payload1 = new byte[0];	// 0長度byte陣列，可取消保留的訊息
    	// conOpt.setWill("topic/aaa", payload1, 2, false);
    	
    	// Connect to the server
    	client.connect(conOpt);
    	log.info(ESAPIUtil.vaildLog("MQTT Connected to "+brokerUrl));
    	log.debug("MQTT Connected to "+brokerUrl);
    	
    	String NFSTATUS = "";	// 推播狀態

    	MqttTopic topic;
    	MqttMessage message;
    	MqttDeliveryToken token;
    	    	
    	for(PushData topicmessage : listTopicMessage){
    		try {
    			tmpTopic = topicmessage.getTopic();
        		tmpTopic = tmpTopic.replace("#", "");
        		
        		topic = client.getTopic(tmpTopic);    		
        		message = new MqttMessage(topicmessage.getMessage().getBytes("utf-8"));
        		message.setQos(qos);
            	message.setRetained(retained);	
            	
            	log.info(ESAPIUtil.vaildLog("Publishing to topic \""+topicmessage.getTopic()+"\" message \""+topicmessage.getMessage()+"\" qos: "+qos));
               	token = topic.publish(message);
            	// Wait until the message has been delivered to the server
            	token.waitForCompletion();
            	log.info(ESAPIUtil.vaildLog("Publishing Complete to topic \""+topicmessage.getTopic()+"\" message \""+topicmessage.getMessage()+"\" qos: "+qos));
            	NFSTATUS = "Y";
            	
    		} catch (Exception e) {
				NFSTATUS = "N";
				log.error("MQTT ERROR e "+e.toString());
			} finally{
				// Save Log
            	Date d = new Date();
            	NFLogDao nfLogDao = (NFLogDao)SpringBeanFactory.getBean("nFLogDao");
            	NFLOG nfLog = new NFLOG();
            	nfLog.setNFUSERID(topicmessage.getCusId());
            	nfLog.setNFACCOUNT("");
            	nfLog.setNFPHONETOKEN(topicmessage.getToken());
            	nfLog.setNFPHONETYPE("A");
            	nfLog.setNFTOPIC(topicmessage.getTopic());
            	nfLog.setNFMESSAGE(topicmessage.getMessage());
            	nfLog.setNFSENDTIME(DateTimeUtils.format("yyyyMMdd", d) + DateTimeUtils.format("HHmmss", d));
            	nfLog.setNFSENDTYPE("");
            	nfLog.setNFSTATUS(NFSTATUS);
            	nfLogDao.save(nfLog);
			}
    		
    	}
    	
    	// Disconnect the client
    	client.disconnect();
    	log.info(ESAPIUtil.vaildLog("MQTT Disconnected"));
    }
	
	public void subscribe(String clientID, String topicName) throws MqttException {				
		String tmpDir = System.getProperty("java.io.tmpdir");
    	MqttDefaultFilePersistence dataStore = new MqttDefaultFilePersistence(tmpDir); 
    	
     	String ip = notifyhost;
		String port = notifyport;
		String brokerUrl = "tcp://" + ip + ":" + port;
   	
    	try {
    		MqttClient client = new MqttClient(this.brokerUrl, clientID, dataStore);
    		MqttConnectOptions conOpt;
        	conOpt = new MqttConnectOptions();
        	conOpt.setCleanSession(cleanSession);
        	conOpt.setUserName(userName);
        	conOpt.setPassword(pd.toCharArray());    	
        	client.connect(conOpt);
        	log.debug("Connected to "+brokerUrl);    	
        	client.subscribe(topicName, qos);
        	client.disconnect();
        	log.debug("Disconnected");
		} catch (MqttException e) {
			log.error("Unable to set up client: " + e.toString());
		}
    }
	
	private void init(){
		String tmpDir = System.getProperty("java.io.tmpdir");
		log.info(ESAPIUtil.vaildLog("tmpDir===>" + tmpDir));
    	MqttDefaultFilePersistence dataStore = new MqttDefaultFilePersistence(tmpDir); 
    	
    	String ip = notifyhost;
    	String port = notifyport;
		brokerUrl = "tcp://" + ip + ":" + port;
		
		Date d = new Date();
		clientId = clientId + DateTimeUtils.format("yyyyMMddHHmmssSSS", d);
		
		log.info(ESAPIUtil.vaildLog("brokerUrl===>" + brokerUrl));
		log.info(ESAPIUtil.vaildLog("userName===>" + userName));
		log.info(ESAPIUtil.vaildLog("clientId===>" + clientId));
   	
    	try {
			client = new MqttClient(this.brokerUrl, clientId, dataStore);
		} catch (MqttException e) {
			log.error("Unable to set up client: "+e.toString());
		}
	}
	
	public boolean deleteDirectory(File directory) {
	    if(directory.exists()){
	        File[] files = directory.listFiles();
	        if(null!=files){
	            for(int i=0; i<files.length; i++) {
	                if(files[i].isDirectory()) {
	                    deleteDirectory(files[i]);
	                }
	                else {
	                    files[i].delete();
	                }
	            }
	        }
	    }
	    return(directory.delete());
	}
}
