package fstop.notification;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;
import com.netbank.rest.comm.batch.NotifyValueBean;
import com.netbank.util.ESAPIUtil;

import fstop.orm.dao.NFLogDao;
import fstop.orm.po.NFLOG;
import fstop.util.DateTimeUtils;
import fstop.util.SpringBeanFactory;
import javapns.Push;
import javapns.notification.PushNotificationPayload;
import javapns.notification.PushedNotification;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Notification {
	
//	@Value("${apns_key}")
//	private String apnskey;
//	@Value("${apns_keypd}")
//	private String apnskeypd;
//	@Value("${apns_production}")
//	private String apnsproduction;
	
	
	public static NotifyValueBean notifyValue() {
		return (NotifyValueBean) SpringBeanFactory.getBean("notifyValueBean");
	}
	
	private String apnskey =  notifyValue().notifyValueProperties.getApnskey();
	private String apnskeypd =  notifyValue().notifyValueProperties.getApnskeypd();
	private String apnsproduction =  notifyValue().notifyValueProperties.getApnsproduction();
	
	
	
	private final String APIKEY = "AIzaSyAfgUGACLKBcsZMW1Dabgyai8q3VcZNy8I"; // GCM使用
	private int SEND_PER_QUANTITY; // 每次推播的 Token 數量
	private Sender sender;	
	
	
	public Notification(){
		
	}	
	

	/**
	 * @param devices
	 *            裝置 Token
	 * @param message
	 *            訊息內容，中文字70字以內 (payload 限制 256 Byte)
	 */
	public void notificationToiOS(List<Object> userid, List<Object> devices, String message, String Type, int ID, String Link) {
		try {
//		String KEYNAME = StrUtils.trim((String)apnsSetting.get("apns.key"));
//		String KEYPWD = StrUtils.trim((String)apnsSetting.get("apns.keypwd"));
//		String production = StrUtils.trim((String)apnsSetting.get("apns.production"));
		String KEYNAME = apnskey;
		String KEYPD = apnskeypd;
		String production = apnsproduction;
		
		String NFSTATUS = "";	// 推播狀態
		PushNotificationPayload payload = null;
		
//		System.out.println("KEYNAME===>" + KEYNAME);
//		//System.out.println("KEYPWD===>" + KEYPWD);
//		System.out.println("production===>" + production);
		log.debug(ESAPIUtil.vaildLog("KEYNAME===>" + KEYNAME));
		log.debug(ESAPIUtil.vaildLog("production===>" + production));
		
		
		boolean isPRODUCTION;
		if (production.equals("0"))
			isPRODUCTION = false;
		else
			isPRODUCTION = true;
		
		SEND_PER_QUANTITY = 5000;
		int totalSend = 0;
		
		List<Object> totalDevices = splitDevice(devices);
		int size = totalDevices.size();
		sender = new Sender(APIKEY);

		for (int i = 0; i < size; i++) {
			try {
				Thread.sleep(500);
				// 推播訊息，每5000筆發一次
				List<String> partialDevices = (List<String>) totalDevices.get(i);
				
//				System.out.println("notificationToiOS--->Send Message to iOS(" + (i + 1) + "/" + size + ")--->Token Quantity: "
//						+ partialDevices.size());
				log.info(ESAPIUtil.vaildLog("notificationToiOS--->Send Message to iOS(" + (i + 1) + "/" + size + ")--->Token Quantity: "
						+ partialDevices.size()));
				
				
				payload = PushNotificationPayload.complex();
				payload.addAlert(message);
				payload.addBadge(0);
				payload.addSound("default");
				payload.addCustomDictionary("type", Type);
				payload.addCustomDictionary("mtId", String.valueOf(ID));
				if (Link == null || !Type.equals("4"))	// 非文字類訊息不加入超連結
					Link ="";
				payload.addCustomDictionary("mtLink", Link.trim());
							
				
				List<PushedNotification> NOTIFICATIONS = Push.payload(payload, KEYNAME, KEYPD, isPRODUCTION, partialDevices);
				log.info(ESAPIUtil.vaildLog("APNS payload--->" + payload.getPayload().toString()));
//				System.out.println("APNS payload--->" + payload.getPayload().toString());
				for (PushedNotification NOTIFICATION : NOTIFICATIONS) {
		            if (NOTIFICATION.isSuccessful()) 
		            	totalSend += 1;		           
				}
				
				NFSTATUS = "Y";

			} catch (Exception e) {
				NFSTATUS = "N";
				log.error("APNS ERROR , e "+e.toString());
			} finally{
	        	// Save Log
	        	Date d = new Date();
	        	NFLogDao nfLogDao = (NFLogDao)SpringBeanFactory.getBean("nFLogDao");
	        	NFLOG nfLog = new NFLOG();
	        	nfLog.setNFUSERID(String.valueOf(userid.get(i)));
	        	nfLog.setNFACCOUNT("");
	        	nfLog.setNFPHONETOKEN(String.valueOf(devices.get(i)));
	        	nfLog.setNFPHONETYPE("I");
	        	nfLog.setNFTOPIC("");
	        	nfLog.setNFMESSAGE(payload.getPayload().toString());
	        	nfLog.setNFSENDTIME(DateTimeUtils.format("yyyyMMdd", d) + DateTimeUtils.format("HHmmss", d));
	        	nfLog.setNFSENDTYPE("");
	        	nfLog.setNFSTATUS(NFSTATUS);
	        	log.debug("nfLog po >> {} " , nfLog.toString());
	        	nfLogDao.save(nfLog);
			}
			
			log.info("==============================================================================");
			log.info("Finish Send Message to APNS, " + "Total Token: " + devices.size() + ", Success: " + totalSend);
			log.info("==============================================================================");
//			System.out.println("==============================================================================");
//			System.out.println("Finish Send Message to APNS, " + "Total Token: " + devices.size() + ", Success: " + totalSend);
//			System.out.println("==============================================================================");
		}
		}catch (Exception e) {
			log.error("notificationToiOS error , {}" , e.getMessage());
//			e.printStackTrace();
		}
	}

	/**
	 * 推播到Android，每次上限 500 個 Token
	 * 
	 * @param devices
	 *            裝置 Token
	 * @param message
	 *            訊息內容，訊息內容 4K 以下
	 */
	public void notificationToAndroid(List<Object> devices, String message) {
		SEND_PER_QUANTITY = 500;
		int totalSend = 0;
		
		List<Object> totalDevices = splitDevice(devices);
		int size = totalDevices.size();
		sender = new Sender(APIKEY);
		try {
			for (int i = 0; i < size; i++) {
				Thread.sleep(500);
				// 推播訊息，每500筆發一次
				List<String> partialDevices = (List<String>) totalDevices.get(i);
				
//				System.out.println("notificationToAndroid--->Send Message to Android(" + (i + 1) + "/" + size + ")--->Token Quantity: "
//						+ partialDevices.size());
				
				log.info(ESAPIUtil.vaildLog("notificationToAndroid--->Send Message to Android(" + (i + 1) + "/" + size + ")--->Token Quantity: "
						+ partialDevices.size()));
				
				String text = "";
				try {
					text = URLEncoder.encode(message, "UTF-8");
				} catch (UnsupportedEncodingException e1) {
					log.error("UnsupportedEncodingException");
//					e1.printStackTrace();
				}
				
				Message gcmMessage = new Message.Builder().addData("message", text).build();
				MulticastResult multicastResult;
				try {
					multicastResult = sender.send(gcmMessage, partialDevices, 5);
					// sender.sendNoRetry(message, devices);
//					System.out.println("notificationToAndroid--->sender.send");
					log.info(ESAPIUtil.vaildLog("notificationToAndroid--->sender.send"));
				} catch (IOException e) {
					log.error(ESAPIUtil.vaildLog("asyncSend error--->" + e.toString()));
					return;
				}

				List<Result> results = multicastResult.getResults();
				totalSend += multicastResult.getSuccess();
//				System.out.println("getMulticastId--->" + multicastResult.getMulticastId());
//				System.out.println("getSuccess--->" + multicastResult.getSuccess());
				log.info(ESAPIUtil.vaildLog("getMulticastId--->" + multicastResult.getMulticastId()));
				log.info(ESAPIUtil.vaildLog("getSuccess--->" + multicastResult.getSuccess()));
				
				for (int j = 0; j < partialDevices.size(); j++) {
					String regId = partialDevices.get(j);
					Result result = results.get(j);
					String messageId = result.getMessageId();
					//System.out.println("notificationToAndroid--->" + result.toString());
					log.info(ESAPIUtil.vaildLog("notificationToAndroid--->" + result.toString()));
				}
			}

		} catch (Exception e) {

		}
		
		log.info("==============================================================================");
		log.info("Finish Send Message to GCM, " + "Total Token: " + devices.size() + ", Success: " + totalSend);
		log.info("==============================================================================");
		
//		System.out.println("==============================================================================");
//		System.out.println("Finish Send Message to GCM, " + "Total Token: " + devices.size() + ", Success: " + totalSend);
//		System.out.println("==============================================================================");
	}

	/**
	 * 分割 Token
	 * 
	 * @param devices
	 * @return
	 */
	public List<Object> splitDevice(List<Object> devices) {
		List<Object> newList = new ArrayList<Object>();
		int size = devices.size();
		int count = (int) Math.floor(size / SEND_PER_QUANTITY); // 執行次數，去小數
		int remain = size % SEND_PER_QUANTITY; // 取餘數

		// 完美分割，直接分段取得
		if (remain == 0) {
			for (int i = 0; i < count; i++) {
				List<Object> cut = devices.subList(i * SEND_PER_QUANTITY,
						SEND_PER_QUANTITY * (i + 1));
				newList.add(cut);
			}
		} else {
			// 不能完美分割，先取得固定的數量，剩下的部分再一次取得
			for (int j = 0; j < count; j++) {
				List<Object> cut = devices.subList(j * SEND_PER_QUANTITY,
						SEND_PER_QUANTITY * (j + 1));
				newList.add(cut);
			}
			List<Object> cut1 = devices.subList(count * SEND_PER_QUANTITY, count
					* SEND_PER_QUANTITY + remain);
			newList.add(cut1);
		}
		return newList;
	}
}