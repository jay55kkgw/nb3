/*
 * DBUtility.java
 *
 * Created on 2002年8月2日, 下午 2:07
 */


package topibs.dao;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.sql.DataSource;

import lombok.extern.slf4j.Slf4j;
import topibs.exception.SQLUnConnectException;

/*for  getSystemDateTime() */
/*for  parse  TablePK.xml */
//import javax.xml.parsers.*;
//import org.xml.sax.*;
//import org.xml.sax.helpers.*;
//import org.w3c.dom.*;
//import org.apache.crimson.tree.*;

/**
 *
 * @author  chuliu
 * @version 
 */
@Slf4j
public class DBUtility 
{

	/** Creates new DBUtility */
	public DBUtility() 
	{
	}
	
	public static Connection getDBConnection() throws SQLUnConnectException
	{
		Connection con_connection;
		try 
		{
			//Class.forName("com.sybase.jdbc2.jdbc.SybDriver").newInstance();
			//con_connection = DriverManager.getConnection("jdbc:sybase:Tds:192.168.5.198:4100/TOPIBS", "TopPrg", "topprg");

			//			Hashtable env = new Hashtable();
			//			env.put(Context.INITIAL_CONTEXT_FACTORY, "com.ibm.websphere.naming.WsnInitialContextFactory");
			//			env.put(Context.PROVIDER_URL, "iiop://localhost:2809");
			//			Properties env = new Properties();
			//			env.put("java.naming.factory.initial", "com.ibm.websphere.naming.WsnInitialContextFactory");
			//			env.put("java.naming.provider.url", "corbaloc:iiop:localhost:2809");
			//			Context obj_initialcontext = new InitialContext(env);

			InitialContext obj_initialcontext = new InitialContext();
			DataSource obj_datasource = (DataSource) obj_initialcontext.lookup("jdbc/topibs");
			//DataSource obj_datasource = (DataSource) obj_initialcontext.lookup("java:comp/env/jdbc/topibs");
			con_connection = obj_datasource.getConnection();

			String	str_connection = con_connection.toString();
//			log.debug("@@@open  : I got the connection : " + con_connection.toString());
//			log.debug("DBUtility.getDBConnection() @@@opened : " + str_connection);
			return con_connection;
		} 
		catch (Exception exc) 
		{
			log.error("",exc);
			throw new SQLUnConnectException("");
		}
	}

	public static void closeDBConnection(Connection a_con_connection)
	{
		if (a_con_connection == null)
		{
			log.debug("@@@close : you want me to close a null connection");
			return;
		}
		
		try
		{
			if (a_con_connection.isClosed() == true)
			{
				log.debug("@@@close : you want me to close a closed connection : "
									+ a_con_connection.toString());
				return;
			}
		}
		catch (SQLException exc)
		{
			log.error("",exc);
			log.debug("@@@close : I have failed to check if the connection has closed : " + a_con_connection.toString());
			return;
		}
		
		String	str_connection = a_con_connection.toString();
//		log.debug("@@@close : I am going to close the connection : " + str_connection);
		try
		{
			a_con_connection.close();
//			log.debug("@@@close : I have closed the connection : " + str_connection);
//			log.debug("DBUtility.closeDBConnection() @@@closed : " + str_connection);
		}
		catch (SQLException exc)
		{
			log.error("",exc);
			log.debug("@@@close : I have failed to close the connection : " + str_connection);
		}
		catch (Exception exc)
		{
			log.error("",exc);
			log.debug("@@@close : I have failed to close the connection : " + str_connection);
		}
	}
	
	/* public static Vector getTablePrimaryKey(String tablename) throws Exception{
	
	    InputSource obj_xmlInp = new InputSource("http://localhost/TablePK.xml");
	    log.debug("obj_xmlInp int finished");
	    DocumentBuilderFactory obj_docBuilderFactory = DocumentBuilderFactory.newInstance();
	    DocumentBuilder obj_parser = obj_docBuilderFactory.newDocumentBuilder();
	    Document obj_doc = obj_parser.parse(obj_xmlInp);
	    Element obj_root = obj_doc.getDocumentElement();
	    //obj_root.normalize();
	    NodeList obj_RootNode=obj_root.getElementsByTagName("TABLENAME");     
	if (obj_RootNode==null)
	        log.debug("null");
	Node obj_node; 
	int i_index=0;
	Vector vtr_returnPK=new Vector();
	
	do
	{    
	      
	    obj_node=obj_RootNode.item(i_index);   
	        Element obj_child=(org.w3c.dom.Element)obj_node.getChildNodes();
	    if (obj_child.getAttributeNode("name").getValue().compareTo(tablename)==0)
	    {       	    
	        
	        vtr_returnPK.addElement(obj_node.getFirstChild().getNodeValue());
	     }
	    i_index++;
	 }while(i_index<obj_RootNode.getLength());    
	  return vtr_returnPK;
	}
	*/

}
