/*
 * TableMaintainDAO.java
 *
 * Created on 2002年6月18日, 下午 1:4
 */
package topibs.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

import fstop.util.exception.TopException;
import lombok.extern.slf4j.Slf4j;
import topibs.exception.DataBefImageErrorException;
import topibs.exception.SQLDataNotExistException;
import topibs.exception.SQLDeadlockException;
import topibs.exception.SQLDuplicateException;
import topibs.exception.SQLFieldNotExistException;
import topibs.exception.SQLLogException;
import topibs.exception.SQLNotAllowedNullException;
import topibs.exception.SQLRelationException;
import topibs.exception.SQLTableSchemaException;
import topibs.exception.SQLUnConnectException;
import topibs.utility.AmountUtil;
import topibs.utility.Machine;
import topibs.utility.SystemUtil;
import topibs.utility.TopLinkedHashMap;

/**
 * @author  chuliu
 * @version
 */
@Slf4j
public class TableMaintainDAO {
    //private Connection p_con_Database = null;

    /** Creates new TableMaintainDAO */
    public TableMaintainDAO() throws SQLUnConnectException {
//    	log.debug("TableMaintainDAO.TableMaintainDAO()");
//        try 
//        {
//        	log.debug("TableMaintainDAO.TableMaintainDAO() call DBUtility.getDBConnection()");
//            p_con_Database = DBUtility.getDBConnection();
//        } catch (Exception exc) {
//        	log.error("",exc);
//            throw new SQLUnConnectException();
//        }
    }

    public void closeDBConnection() {
//        try {
//            if (!p_con_Database.isClosed()) 
//            {
//            	log.debug("TableMaintainDAO.closeDBConnection() : " + p_con_Database.toString());
//                p_con_Database.close();
//            }   
//            
//            p_con_Database = null;
//        } catch (Exception exc) {
//        	log.error("",exc);
//        }
    }

    public void create(String a_str_TableName, TopLinkedHashMap a_htl_RecordData)
                       throws SQLTableSchemaException, SQLFieldNotExistException,
            SQLNotAllowedNullException, SQLRelationException,
            SQLDuplicateException, SQLUnConnectException, SQLException,
            TopException,SQLDeadlockException {
        a_htl_RecordData = CasttoString(a_htl_RecordData);
        insert(a_str_TableName, a_htl_RecordData, null);
    }

    public void create(
    				String           a_str_TableName, 
    				TopLinkedHashMap a_lhm_RecordData,
    				TopLinkedHashMap a_lhm_FieldsInfo
    					) throws SQLTableSchemaException, SQLFieldNotExistException,
            SQLNotAllowedNullException, SQLRelationException,
            SQLDuplicateException, SQLUnConnectException, SQLException,
            TopException,SQLDeadlockException
    {
        a_lhm_RecordData = CasttoString(a_lhm_RecordData);
        this.insert(a_str_TableName, a_lhm_RecordData, a_lhm_FieldsInfo);
    }

    public void store(String a_str_TableName, TopLinkedHashMap a_htl_RecordKey, TopLinkedHashMap a_htl_RecordData)
        throws SQLDataNotExistException, SQLTableSchemaException, DataBefImageErrorException, SQLFieldNotExistException,
            SQLNotAllowedNullException, SQLRelationException, SQLLogException, SQLException, SQLUnConnectException,
            TopException,SQLDeadlockException {
        a_htl_RecordData = CasttoString(a_htl_RecordData);
        update(a_str_TableName, a_htl_RecordKey, a_htl_RecordData, null, null);
    }

    public void store(
    				String           a_str_TableName, 
    				TopLinkedHashMap a_htl_RecordKey,
    				TopLinkedHashMap a_htl_RecordData,
    				TopLinkedHashMap a_lhm_FieldsInfo,
    				Vector           a_vtr_PrimaryKey
    					) throws SQLDataNotExistException, SQLTableSchemaException,
            DataBefImageErrorException, SQLFieldNotExistException,
            SQLNotAllowedNullException, SQLRelationException,
            SQLLogException, SQLException, SQLUnConnectException,
            TopException,SQLDeadlockException
    {
        a_htl_RecordData = CasttoString(a_htl_RecordData);
        update(a_str_TableName, a_htl_RecordKey, a_htl_RecordData, a_lhm_FieldsInfo, a_vtr_PrimaryKey);
    }

    public void remove(
    				String				a_str_TableName, 
    				String				a_str_MaintainUserID, 
    				TopLinkedHashMap a_htl_RecordKey,
    				TopLinkedHashMap a_lhm_FieldsInfo,
    				Vector				a_vtr_PrimaryKey
    					) throws SQLLogException, SQLTableSchemaException, SQLDataNotExistException, DataBefImageErrorException, SQLRelationException, SQLUnConnectException, SQLException,SQLDeadlockException
    {
    	boolean b_WriteLog = false;
    	
    	if (a_str_MaintainUserID == null)
    	{
    		b_WriteLog = false;
    	}
    	else
    	{
    		b_WriteLog = true;
    	}
    	
        a_htl_RecordKey = CasttoString(a_htl_RecordKey);
        
        delete(
        	a_str_TableName, 
        	b_WriteLog,
        	a_str_MaintainUserID, 
        	null,
        	a_htl_RecordKey,
        	null,
        	null,
        	a_lhm_FieldsInfo,
        	a_vtr_PrimaryKey
        		);
    }

    public void remove(
    				String				a_str_TableName,
    				boolean			a_b_WriteLog, 
    				String				a_str_MaintainUserID, 
    				String				a_str_ApproveID,
    				TopLinkedHashMap a_htl_RecordKey,
    				TopLinkedHashMap a_lhm_FieldsInfo,
    				Vector				a_vtr_PrimaryKey
    					) throws SQLLogException, SQLTableSchemaException, SQLDataNotExistException, DataBefImageErrorException, SQLRelationException, SQLUnConnectException, SQLException,SQLDeadlockException
    {
        a_htl_RecordKey = CasttoString(a_htl_RecordKey);
        
        delete(
        	a_str_TableName, 
        	a_b_WriteLog,
        	a_str_MaintainUserID, 
        	a_str_ApproveID,
        	a_htl_RecordKey,
        	null,
        	null,
        	a_lhm_FieldsInfo,
        	a_vtr_PrimaryKey
        		);
    }

    public void remove(
    				String				a_str_TableName, 
    				String				a_str_MaintainUserID, 
    				TopLinkedHashMap a_htl_RecordKey
    					) throws SQLLogException, SQLTableSchemaException, SQLDataNotExistException, DataBefImageErrorException, SQLRelationException, SQLUnConnectException, SQLException,SQLDeadlockException
    {
    	boolean b_WriteLog = false;
    	
    	if (a_str_MaintainUserID == null)
    	{
    		b_WriteLog = false;
    	}
    	else
    	{
    		b_WriteLog = true;
    	}
    	
        a_htl_RecordKey = CasttoString(a_htl_RecordKey);
        
        delete(
        	a_str_TableName, 
        	b_WriteLog,
        	a_str_MaintainUserID, 
        	null,
        	a_htl_RecordKey,
        	null,
        	null,
        	null,
        	null
        		);
    }

//    public void remove(
//    				String				a_str_TableName, 
//    				String				a_str_MaintainUserID, 
//    				TopLinkedHashMap	a_htl_RecordKey,
//    				String				a_str_SystemDate,
//    				String				a_str_SystemTime
//    					) throws SQLLogException, SQLTableSchemaException, SQLDataNotExistException, DataBefImageErrorException, SQLRelationException, SQLUnConnectException, SQLException 
//    {
//    	boolean b_WriteLog = false;
//    	
//    	if (a_str_MaintainUserID == null)
//    	{
//    		b_WriteLog = false;
//    	}
//    	else
//    	{
//    		b_WriteLog = true;
//    	}
//    	
//        a_htl_RecordKey = CasttoString(a_htl_RecordKey);
//
//        delete(
//        	a_str_TableName, 
//        	b_WriteLog,
//        	a_str_MaintainUserID, 
//        	null,
//        	a_htl_RecordKey, 
//        	a_str_SystemDate, 
//        	a_str_SystemTime,
//        	null,
//        	null
//        		);
//    }

    public void remove(
    				String				a_str_TableName, 
    				String				a_str_MaintainUserID, 
    				TopLinkedHashMap a_htl_RecordKey,
    				String				a_str_SystemDate,
    				String				a_str_SystemTime,
    				TopLinkedHashMap a_lhm_FieldsInfo,
    				Vector				a_vtr_PrimaryKey
    					) throws SQLLogException, SQLTableSchemaException, SQLDataNotExistException, DataBefImageErrorException, SQLRelationException, SQLUnConnectException, SQLException,SQLDeadlockException
    {
    	boolean b_WriteLog = false;
    	
    	if (a_str_MaintainUserID == null || a_str_MaintainUserID.equals(""))
    	{
    		b_WriteLog = false;
    	}
    	else
    	{
    		b_WriteLog = true;
    	}
    	
        a_htl_RecordKey = CasttoString(a_htl_RecordKey);

        delete(
        	a_str_TableName, 
        	b_WriteLog,
        	a_str_MaintainUserID, 
        	null,
        	a_htl_RecordKey, 
        	a_str_SystemDate, 
        	a_str_SystemTime,
        	a_lhm_FieldsInfo,
        	a_vtr_PrimaryKey
        		);
    }

    public TopLinkedHashMap findByPrimaryKey(
    							String a_str_TableName, 
    							TopLinkedHashMap a_htl_RecordKey,
    							TopLinkedHashMap a_lhm_FieldsInfo,
    							Vector           a_vtr_PrimaryKey
    								) throws SQLTableSchemaException, SQLDataNotExistException, SQLUnConnectException, SQLException, DataBefImageErrorException
    {
        a_htl_RecordKey = CasttoString(a_htl_RecordKey);
        return isExists(a_str_TableName, a_htl_RecordKey, a_lhm_FieldsInfo, a_vtr_PrimaryKey);
    }

    public TopLinkedHashMap findByPrimaryKey(String a_str_TableName, TopLinkedHashMap a_htl_RecordKey) throws SQLTableSchemaException, SQLDataNotExistException, SQLUnConnectException, SQLException, DataBefImageErrorException {
        a_htl_RecordKey = CasttoString(a_htl_RecordKey);
        return isExists(a_str_TableName, a_htl_RecordKey);
    }

    public TopLinkedHashMap findAllRecord(String a_str_TableName, Vector a_vtr_Fields, TopLinkedHashMap a_htl_Order) throws SQLTableSchemaException, SQLDataNotExistException, SQLUnConnectException, SQLException {
        return selectData(a_str_TableName, a_vtr_Fields, new TopLinkedHashMap(), a_htl_Order, null, null);
    }

    public TopLinkedHashMap findAllRecord(
    							String           a_str_TableName, 
    							Vector           a_vtr_Fields, 
    							TopLinkedHashMap a_lhm_Order,
    							TopLinkedHashMap a_lhm_FieldsInfo,
    							Vector           a_vtr_PrimaryKey
    								) throws SQLTableSchemaException, SQLDataNotExistException, SQLUnConnectException, SQLException
    {
        return selectData(
        			a_str_TableName, 
        			a_vtr_Fields, 
        			new TopLinkedHashMap(),
        			a_lhm_Order, 
        			a_lhm_FieldsInfo, 
        			a_vtr_PrimaryKey
        				);
    }

    public TopLinkedHashMap findByPartialKey(
    							String           a_str_TableName, 
    							Vector           a_vtr_Fields, 
    							TopLinkedHashMap a_htl_RecordKey,
    							TopLinkedHashMap a_htl_Order,
                                TopLinkedHashMap a_htl_FieldInfo,
                                Vector           a_vtr_PrimaryKeys
    							) throws SQLTableSchemaException, SQLDataNotExistException, SQLUnConnectException, SQLException
    {
        a_htl_RecordKey = CasttoString(a_htl_RecordKey);
        return selectData(a_str_TableName, a_vtr_Fields, a_htl_RecordKey, a_htl_Order, a_htl_FieldInfo, a_vtr_PrimaryKeys);
    }

    public TopLinkedHashMap findByPartialKey(
    							String a_str_TableName, 
    							Vector a_vtr_Fields, 
    							TopLinkedHashMap a_htl_RecordKey,
    							TopLinkedHashMap a_htl_Order
    							) throws SQLTableSchemaException, SQLDataNotExistException, SQLUnConnectException, SQLException {
        a_htl_RecordKey = CasttoString(a_htl_RecordKey);
        return selectData(a_str_TableName, a_vtr_Fields, a_htl_RecordKey, a_htl_Order, null, null);
    }

    public TopLinkedHashMap findByPartialKey(
    							String a_str_TableName, 
    							Vector a_vtr_Fields, 
    							String[][] a_str_RecordKey, 
    							TopLinkedHashMap a_htl_Order
    							) throws SQLTableSchemaException, SQLDataNotExistException, SQLException, Exception
    {
        return selectData(a_str_TableName, a_vtr_Fields, a_str_RecordKey, a_htl_Order, null, null);
    }

    public TopLinkedHashMap findByPartialKey(
    							String           a_str_TableName, 
    							Vector           a_vtr_Fields, 
    							String[][]       a_str_RecordKey, 
    							TopLinkedHashMap a_lhm_Order,
    							TopLinkedHashMap a_lhm_FieldsInfo,
    							Vector           a_vtr_PrimaryKey
    							) throws SQLTableSchemaException, SQLDataNotExistException, SQLException, Exception
    {
        return selectData(
        			a_str_TableName, 
        			a_vtr_Fields, 
        			a_str_RecordKey, 
        			a_lhm_Order, 
        			a_lhm_FieldsInfo, 
        			a_vtr_PrimaryKey
        				);
    }

    public void removeByPartialKey(
    				String a_str_TableName, 
    				String a_str_MaintainUserID, 
    				TopLinkedHashMap a_htl_RecordKey
    					) throws SQLLogException, SQLTableSchemaException, SQLDataNotExistException, SQLRelationException, SQLUnConnectException, SQLException,SQLDeadlockException
    {
        deleteByPartialKey(a_str_TableName, a_str_MaintainUserID, a_htl_RecordKey, null, null);
    }

    public void removeByPartialKey(
    				String           a_str_TableName, 
    				String           a_str_MaintainUserID, 
    				TopLinkedHashMap a_lhm_RecordKey,
    				TopLinkedHashMap a_lhm_FieldsInfo,
    				Vector           a_vtr_PrimaryKey
    					) throws SQLLogException, SQLTableSchemaException, SQLDataNotExistException, SQLRelationException, SQLUnConnectException, SQLException,SQLDeadlockException
    {
        deleteByPartialKey(
        			a_str_TableName, 
        			a_str_MaintainUserID, 
        			a_lhm_RecordKey, 
        			a_lhm_FieldsInfo, 
        			a_vtr_PrimaryKey
        				);
    }

    private void insert(
    				String           a_str_TableName, 
    				TopLinkedHashMap a_lhm_RecordData,
    				TopLinkedHashMap a_lhm_FieldsInfo
    					) throws SQLTableSchemaException, SQLFieldNotExistException,
            SQLNotAllowedNullException, SQLRelationException,
            SQLDuplicateException, SQLException, SQLUnConnectException,
            TopException,SQLDeadlockException
    {
        Statement stmt = null;
        String		str_value;
        Connection p_con_Database = null;

        try {
//            if (p_con_Database == null) 
//            {
//            	log.debug("TableMaintainDAO.insert() call DBUtility.getDBConnection()");
//                p_con_Database = DBUtility.getDBConnection();
//            }
			p_con_Database = DBUtility.getDBConnection();
            stmt = p_con_Database.createStatement();
            /* 取得 Table的欄位相關資訊 包括欄位名稱及欄位型態 */
            TopLinkedHashMap htl_FieldInfo;
            if (a_lhm_FieldsInfo == null)
            {
            	htl_FieldInfo = this.getTableInfo(a_str_TableName);
            }
            else
            {
            	htl_FieldInfo = a_lhm_FieldsInfo;
            }

            //放入SystemDate,SystemTime
            if (htl_FieldInfo.containsKey("SystemDate") == true)
            {
            	if (a_lhm_RecordData.containsKey("SystemDate") == false)
            	{
            		a_lhm_RecordData.put("SystemDate", Machine.getSystemDateTime().get("SystemDate"));
            	}
            }
            
            if (htl_FieldInfo.containsKey("SystemTime") == true)
            {
            	if (a_lhm_RecordData.containsKey("SystemTime") == false)
            	{
            		a_lhm_RecordData.put("SystemTime", Machine.getSystemDateTime().get("SystemTime"));
            	}
            }
            // log.debug(a_htl_RecordData);

            /*
            Properties env=new Properties();
            env.put("java.naming.factory.initial","com.sun.jndi.cosnaming.CNCtxFactory");
            env.put("java.naming.provider.url","iiop://127.0.0.1:9010");
            Context myc = new InitialContext(env);
            Object objref = myc.lookup("ejb/TableSchema");            
            ITableSchemaHome home = (ITableSchemaHome)PortableRemoteObject.narrow(objref,ITableSchemaHome.class);            
            ITableSchema ts = home.create();            
            htl_FieldInfo=ts.getFieldInfo(a_str_TableName);
            */
            StringBuffer str_partialStr1 = new StringBuffer(""); //組成 "(CtryCode,CtryName....
            StringBuffer str_partialStr2 = new StringBuffer(""); //組成 "('001','USA'.....
            Iterator itr_keys = htl_FieldInfo.keySet().iterator();
            String str_c = "";
            Object obj_key = null;

            while (itr_keys.hasNext()) {
                // log.debug(obj_key);
                obj_key = itr_keys.next();
                if (!(obj_key.equals((Object) "timestamp")) && a_lhm_RecordData.containsKey(obj_key)) {
                    str_partialStr1.append(str_c);
                    str_partialStr2.append(str_c);
                    str_partialStr1 = str_partialStr1.append((String) obj_key);

					str_value = (String) a_lhm_RecordData.get(obj_key);
                    if (((Integer) htl_FieldInfo.get(obj_key)).intValue() == Types.VARCHAR || ((Integer) htl_FieldInfo.get(obj_key)).intValue() == Types.CHAR || ((Integer) htl_FieldInfo.get(obj_key)).intValue() == Types.LONGVARCHAR)
                    {
                        str_partialStr2 = str_partialStr2.append("'").append(SystemUtil.formatSQLString(str_value)).append("'");
                    }
                    else
                    {
                    	// 若不去掉撇節，會被當作欄位的分隔，而發生 SQL 的 exception，說少了欄位。
                    	str_value = AmountUtil.deleteComma(str_value);
                        str_partialStr2 = str_partialStr2.append(str_value);
                    }
                    str_c = ",";
                } //end if 
            } //end while 

            String queryStr = "INSERT INTO " + a_str_TableName + " (" + str_partialStr1.toString() + ")" + " VALUES (" + str_partialStr2.toString() + ")";
            /*20030305*/
            //byte[] tmpbuf = queryStr.getBytes("MS950");
            //queryStr = new String(tmpbuf,"ISO8859_1");
            log.debug("queryString is: " + queryStr);
            int resultCount = stmt.executeUpdate(queryStr);
//			log.debug("***Done : "+ queryStr);
        } 
        catch (SQLException exc) {
            int i_errorcode = exc.getErrorCode();
            log.error("",exc);
            switch (i_errorcode) {
                case 207 :
                    throw new SQLFieldNotExistException(exc.getMessage());
                case 233 :
                    throw new SQLNotAllowedNullException(exc.getMessage());
                case 546 :
                    throw new SQLRelationException(exc.getMessage());
                case 2601 :
                    throw new SQLDuplicateException();
				case 1205 :
					throw new SQLDeadlockException(exc.getMessage());

                    /*                    String str_temp=null;
                                        try {
                                           str_temp=Message.getMsgDescByMsgCode("E90041");
                                        } catch  (Exception exc1)
                                        {}
                                        throw new SQLDuplicateException(a_str_TableName+str_temp); */
                default :
                	throw exc;
            }
        } catch (NullPointerException exc) {
              log.error("",exc);
              //Log4j for logging  -->  日後會加上,目前暫無
              throw new SQLException("發生錯誤!!!請通知系統人員", "DAO");
              //throw new SQLUnConnectException("未取得資料庫連結，發生錯誤!!!請通知系統人員");
        }catch (SQLUnConnectException exc) {
        	log.error("",exc);
           	throw exc;
        }
        catch (Exception exc) {
        	log.error("",exc);
			throw new SQLException(exc.getMessage());
        } finally {
            if (stmt != null)
                stmt.close();
            DBUtility.closeDBConnection(p_con_Database);
        }
    }

    private void update(
    				String a_str_TableName, 
    				TopLinkedHashMap a_htl_RecordKey,
    				TopLinkedHashMap a_htl_RecordData,
    				TopLinkedHashMap a_lhm_FieldsInfo,
    				Vector           a_vtr_PrimaryKey
    					) throws SQLDataNotExistException, SQLTableSchemaException,
            DataBefImageErrorException, SQLFieldNotExistException,
            SQLNotAllowedNullException, SQLRelationException,
            SQLLogException, SQLException, SQLUnConnectException,
            TopException,SQLDeadlockException
    {
        Vector vtr_PrimaryKeys = new Vector();
        TopLinkedHashMap htl_FieldInfo = null;
        String str_timestamp;
        String str_queryStr;
        Statement stmt = null;
        String str_WhereStmt;  //存"Where"後面的敘述
        Connection p_con_Database = null;
        String str_value;

        try {
            /* 取得 Table的欄位相關資訊 包括欄位名稱及欄位型態 */
            if (a_lhm_FieldsInfo == null)
            {
            	htl_FieldInfo = this.getTableInfo(a_str_TableName);
            }
            else
            {
            	htl_FieldInfo = a_lhm_FieldsInfo;
            }
            /* 取得 Table 的 PK*/
            if (a_vtr_PrimaryKey == null)
            {
            	vtr_PrimaryKeys = this.getPrimaryKey(a_str_TableName);
            }
            else
            {
            	vtr_PrimaryKeys = a_vtr_PrimaryKey;
            }
                       
            //放入SystemDate,SystemTime
            if (htl_FieldInfo.containsKey("SystemDate") == true)
            {
            	if (a_htl_RecordData.containsKey("SystemDate") == false)
            	{
            		a_htl_RecordData.put("SystemDate", Machine.getSystemDateTime().get("SystemDate"));
            	}
            }
            
            if (htl_FieldInfo.containsKey("SystemTime") == true)
            {
            	if (a_htl_RecordData.containsKey("SystemTime") == false)
            	{
            		a_htl_RecordData.put("SystemTime", Machine.getSystemDateTime().get("SystemTime"));
            	}
            }
            
/*
            //放入SystemDate,SystemTime
            a_htl_RecordData.put("SystemDate", Machine.getSystemDateTime().get("SystemDate"));
            a_htl_RecordData.put("SystemTime", Machine.getSystemDateTime().get("SystemTime"));

                       
            Properties env=new Properties();
            env.put("java.naming.factory.initial","com.sun.jndi.cosnaming.CNCtxFactory");
            env.put("java.naming.provider.url","iiop://127.0.0.1:9010");
            Context myc = new InitialContext(env);
            Object objref = myc.lookup("ejb/TableSchema");        
            ITableSchemaHome home = (ITableSchemaHome)PortableRemoteObject.narrow(objref,ITableSchemaHome.class);            
            ITableSchema ts = home.create();            
            vtr_PrimaryKeys=ts.getPrimaryKey(a_str_TableName);
            htl_FieldInfo=ts.getTableInfo(a_str_TableName);
*/
        }                        
		catch (SQLException exc) 
		{            
			int i_exc = exc.getErrorCode();
			switch (i_exc) 
			{
				case 1205 :
					throw new SQLDeadlockException(exc.getMessage());
					
				default:
					throw exc;					
			} //end switch
		}        
        catch (Exception exc) {
        	log.error("",exc);
			throw new SQLException(exc.getMessage());
        }

        //for (int i=0;i<vtr_PrimaryKeys.size();i++)
        //   htl_RecordKey.put((String)vtr_PrimaryKeys.get(i),a_htl_RecordData.get(vtr_PrimaryKeys.get(i)));
        /*if (a_htl_RecordData.containsKey("timestamp")) {
            str_timestamp = (String) (isExists(a_str_TableName, a_htl_RecordKey).get("timestamp"));
            if (!str_timestamp.equals((String) a_htl_RecordData.get("timestamp"))) {
                throw new DataBefImageErrorException("");
            }
        }*/

        StringBuffer str_partialStr1 = new StringBuffer(""); //組成 "CtryName='USA',ValueDays=0.....
        StringBuffer str_partialStr2 = new StringBuffer(""); //組成 "CtryCode='001' and PK='???'.....
        if (a_htl_RecordKey.isEmpty()) {
            str_partialStr2 = str_partialStr2.append("none");
        } else {
            Iterator itr_keys = htl_FieldInfo.keySet().iterator();
            Object obj_key = null;
            String str_and = "";
            while (itr_keys.hasNext()) {
                obj_key = itr_keys.next();
				str_value = (String) a_htl_RecordKey.get(obj_key);
                if ( a_htl_RecordKey.containsKey(obj_key) && !(obj_key.equals((Object) "timestamp")) ) {
                    str_partialStr2 = str_partialStr2.append(str_and);
                    if (((Integer) htl_FieldInfo.get(obj_key)).intValue() == Types.VARCHAR || ((Integer) htl_FieldInfo.get(obj_key)).intValue() == Types.CHAR || ((Integer) htl_FieldInfo.get(obj_key)).intValue() == Types.LONGVARCHAR)
                    {
						str_partialStr2 = str_partialStr2.append((String) obj_key).append("='").append(str_value).append("'");
                    }
                    else
                    {
						// 若不去掉撇節，會被當作欄位的分隔，而發生 SQL 的 exception，說少了欄位。
						str_value = AmountUtil.deleteComma(str_value);
                        str_partialStr2 = str_partialStr2.append((String) obj_key).append("=").append(str_value);
					}
                    str_and = " and ";
                }
            } //end --while (en_keys.hasMoreElements()){      
        }

        /*** 產生 where 後的敘述字串供 SQLDataNotExistException時使用 ***/
        if(a_htl_RecordKey.isEmpty()) {
            str_WhereStmt = "";
        }
        else {  
            str_WhereStmt = " where " + str_partialStr2.toString();
        }

        Iterator itr_keys = htl_FieldInfo.keySet().iterator();
        Object obj_key = null;
        String str_c = "";
        while (itr_keys.hasNext()) {
            obj_key = itr_keys.next();
            if (!(obj_key.equals((Object) "timestamp"))) {
                if (!vtr_PrimaryKeys.contains(obj_key) && a_htl_RecordData.containsKey(obj_key)) {
					str_value = (String) a_htl_RecordData.get(obj_key);
                    if (((Integer) htl_FieldInfo.get(obj_key)).intValue() == Types.VARCHAR || ((Integer) htl_FieldInfo.get(obj_key)).intValue() == Types.CHAR || ((Integer) htl_FieldInfo.get(obj_key)).intValue() == Types.LONGVARCHAR)
                    {
                        str_partialStr1 = str_partialStr1.append(str_c).append((String) obj_key).append("='").append(SystemUtil.formatSQLString(str_value)).append("'");
					}
                    else
                    {
						// 若不去掉撇節，會被當作欄位的分隔，而發生 SQL 的 exception，說少了欄位。
						str_value = AmountUtil.deleteComma(str_value);
                        str_partialStr1 = str_partialStr1.append(str_c).append((String) obj_key).append("=").append(str_value);
					}
                    str_c = ",";
                }
            } // if (!(obj_key.equals((Object)"timestamp"))){
        } //while (en_keys.hasMoreElements()) { 

        try {
//            if (p_con_Database == null) 
//            {
//            	log.debug("TableMaintainDAO.update() call DBUtility.getDBConnection()");
//                p_con_Database = DBUtility.getDBConnection();
//            }
			p_con_Database = DBUtility.getDBConnection();
            stmt = p_con_Database.createStatement();
            if (!str_partialStr2.toString().equals("none")){ //新增timestamp判斷！
                if (a_htl_RecordKey.containsKey("timestamp"))
                    str_partialStr2.append(" and timestamp=0x"+(String) a_htl_RecordKey.get("timestamp"));
                str_queryStr = "Update " + a_str_TableName + " Set " + str_partialStr1.toString() + " where " + str_partialStr2;
            }
            else{
                
                str_queryStr = "Update " + a_str_TableName + " Set " + str_partialStr1.toString();
                 if (a_htl_RecordKey.containsKey("timestamp"))
                    str_queryStr=str_queryStr+" where timestamp=0x"+(String) a_htl_RecordKey.get("timestamp");
            }
            /*20030305*/
            //str_queryStr=new String(str_queryStr.getBytes("MS950"),"ISO8859_1");
            log.debug("queryString is" + str_queryStr);
            int resultcount = stmt.executeUpdate(str_queryStr);
            
//			log.debug("***Done : "+ str_queryStr);
            
            if (resultcount == 0)
                throw new SQLDataNotExistException();
        } catch (SQLException exc_sql) {
        	log.error("",exc_sql);
            int i_exc = exc_sql.getErrorCode();
            switch (i_exc) {
                case 207 :
                    throw new SQLFieldNotExistException(exc_sql.getMessage());
                case 233 :
                    throw new SQLNotAllowedNullException(exc_sql.getMessage());
                case 546 :
                    throw new SQLRelationException(exc_sql.getMessage());
                case 2601 :
                    throw new SQLLogException();
				case 1205 :
					throw new SQLDeadlockException(exc_sql.getMessage());
                    
                default :
                    throw new SQLException(exc_sql.getMessage());
            }
        } catch (NullPointerException exc) {
                log.error("",exc);
                //Log4j for logging  -->  日後會加上,目前暫無
                throw new SQLException("發生錯誤!!!請通知系統人員", "DAO");
                //throw new SQLUnConnectException("未取得資料庫連結，發生錯誤!!!請通知系統人員");
          }catch (SQLUnConnectException exc) {
          	log.error("",exc);
             throw exc;
          } catch (SQLDataNotExistException exc) {
            ResultSet rs = stmt.executeQuery("select * from " + a_str_TableName + " NOHOLDLOCK " + str_WhereStmt);
            if(rs.next())
              throw new DataBefImageErrorException(exc.getMessage());
            else  
              throw exc;
        } catch (Exception exc) {
        	log.error("",exc);
			throw new SQLException(exc.getMessage());
        } finally {
            if (stmt != null)
                stmt.close();
            DBUtility.closeDBConnection(p_con_Database);
        } //end --finally

    } //end update

    private void delete(
				String				a_str_TableName, 
				boolean			a_b_WriteLog,
				String				a_str_MaintainUserID, 
				String				a_str_ApproveID,
				TopLinkedHashMap a_lhm_RecordKey,
				String				a_str_SystemDate,
				String				a_str_SystemTime,
				TopLinkedHashMap a_lhm_FieldsInfo,
				Vector				a_vtr_PrimaryKey
					) throws SQLTableSchemaException, SQLLogException, SQLDataNotExistException, SQLRelationException, SQLException, SQLUnConnectException, DataBefImageErrorException,SQLDeadlockException
	{

        Vector vtr_PrimaryKeys;
        TopLinkedHashMap lhm_FieldInfo;
        TopLinkedHashMap lhm_LogData  = null;
        TopLinkedHashMap lhm_DateTime = new TopLinkedHashMap();
        Statement stmt = null;
        boolean b_PK  = false;
        String str_SystemDate;
        String str_SystemTime;
        Connection p_con_Database = null;
        String str_msg = null;

        Iterator itr_columnSet = null;
        StringBuffer str_where = new StringBuffer(""); //組成 "CtryCode='001' and PK='???'....."
        String str_WhereWithoutTimeStamp = "";
        String str_and = "";
        String str_columnName = null;

		// 不 try catch. 若發生 exception, 就讓這個 exception 直接往外丟.
        lhm_LogData = isExists(a_str_TableName, a_lhm_RecordKey, a_lhm_FieldsInfo, a_vtr_PrimaryKey);

        try
        {
       		str_SystemDate = (String) (Machine.getSystemDateTime().get("SystemDate"));
       		str_SystemTime = (String) (Machine.getSystemDateTime().get("SystemTime"));
        }
        catch (Exception exc)
        {
        	log.error("",exc);
        	str_SystemDate = "99999999";
        	str_SystemTime = "999999";
        }

		if (a_b_WriteLog == true)
		{

			try
			{
				if (lhm_LogData.containsKey("timestamp") == true)
				{
                	lhm_LogData.remove("timestamp");
				}
                
                if (a_str_MaintainUserID != null && 
                	a_str_MaintainUserID.equals("") != true &&
                	a_lhm_FieldsInfo.containsKey("MaintainUserID") == true)
                {
                	lhm_LogData.put("MaintainUserID", a_str_MaintainUserID);
                }
                
                if (a_str_ApproveID != null && 
                	a_str_ApproveID.equals("") != true &&
                	a_lhm_FieldsInfo.containsKey("ApproveID") == true)
                {
                	lhm_LogData.put("ApproveID", a_str_ApproveID);
                	lhm_LogData.put("ApproveStatus", "Y");

	                if (a_str_SystemDate == null)
	                	lhm_LogData.put("ApproveDate", str_SystemDate);
	                else
	                	lhm_LogData.put("ApproveDate", a_str_SystemDate);
	                
	                if (a_str_SystemTime == null)
	                	lhm_LogData.put("ApproveTime", str_SystemTime);
	                else
	                	lhm_LogData.put("ApproveTime", a_str_SystemTime);
                }
                
                if (a_str_SystemDate == null)
                	lhm_LogData.put("SystemDate", str_SystemDate);
                else
                	lhm_LogData.put("SystemDate", a_str_SystemDate);
                
                if (a_str_SystemTime == null)
                	lhm_LogData.put("SystemTime", str_SystemTime);
                else
                	lhm_LogData.put("SystemTime", a_str_SystemTime);
                
                lhm_LogData.put("ModifyType", "D");
                this.create(a_str_TableName + "Log", lhm_LogData, a_lhm_FieldsInfo);
            } 
			catch (SQLException exc) 
			{            
				int i_exc = exc.getErrorCode();
				switch (i_exc) 
				{
					case 1205 :
						throw new SQLDeadlockException(exc.getMessage());
					
					default:
					    throw exc;					
				} //end switch
			}
            catch (Exception exc) 
            {
            	log.error("",exc);
				throw new SQLException(exc.getMessage());
            }
		} // end if (a_b_WriteLog == true)

        try 
        {
            // 取得 Table的欄位相關資訊 包括欄位名稱及欄位型態
            if (a_lhm_FieldsInfo == null)
            {
            	lhm_FieldInfo = this.getTableInfo(a_str_TableName);
            }
            else
            {
            	lhm_FieldInfo = a_lhm_FieldsInfo;
            }

            // 取得 Table 的 PK
            if (a_vtr_PrimaryKey == null)
            {
            	vtr_PrimaryKeys = this.getPrimaryKey(a_str_TableName);
            }
            else
            {
            	vtr_PrimaryKeys = a_vtr_PrimaryKey;
            }

			Iterator itr_keySet = a_lhm_RecordKey.keySet().iterator();
			while (itr_keySet.hasNext())
			{
				str_columnName = (String) itr_keySet.next();                            
                //略過timestamp欄位
                if ( str_columnName.equals("timestamp") )         
                    continue;	                
            
                str_where.append(str_and);    	
                
                if (((Integer) lhm_FieldInfo.get(str_columnName)).intValue() == Types.VARCHAR || ((Integer) lhm_FieldInfo.get(str_columnName)).intValue() == Types.CHAR || ((Integer) lhm_FieldInfo.get(str_columnName)).intValue() == Types.LONGVARCHAR)
                    str_where = str_where.append(str_columnName).append("='").append((String) a_lhm_RecordKey.get(str_columnName)).append("'");
                else
                    str_where = str_where.append(str_columnName).append("=").append((String) a_lhm_RecordKey.get(str_columnName));
                str_and = " and ";
			}
			
			str_WhereWithoutTimeStamp = str_where.toString();
						
			//新增timestamp判斷！
            if (a_lhm_RecordKey.containsKey("timestamp"))
            {
                str_where.append(" and timestamp=0x"+ (String)a_lhm_RecordKey.get("timestamp"));
            }            
			
        } 
		catch (SQLException exc) 
		{            
			int i_exc = exc.getErrorCode();
			switch (i_exc) 
			{
				case 1205 :
					throw new SQLDeadlockException(exc.getMessage());
					
				default:
					throw exc;					
			} //end switch
		}        
        catch (Exception exc) 
        {
        	log.error("",exc);
            throw new SQLException(exc.getMessage());
        }

        try 
        {
			p_con_Database = DBUtility.getDBConnection();
            stmt = p_con_Database.createStatement();
            String queryStr = "Delete from " + a_str_TableName + " where " + str_where;
            log.debug("queryString= " + queryStr);
            int i_resultcount = stmt.executeUpdate(queryStr);
			
//			log.debug("***Done : "+ queryStr);
			            			            
            if (i_resultcount == 0)
                throw new SQLDataNotExistException();
        } 
        catch (SQLException exc_sql) 
        {
        	log.error("",exc_sql);
            int i_exc = exc_sql.getErrorCode();
            switch (i_exc) 
            {
                case 546 :
                    throw new SQLRelationException(exc_sql.getMessage());
                case 2601 :
                    throw new SQLLogException();
				case 1205 :
					throw new SQLDeadlockException(exc_sql.getMessage());
					
				default:
				    throw exc_sql;	                    
            } //end switch
        } 
        catch (SQLDataNotExistException exc)
        {
        	ResultSet rs = stmt.executeQuery("select * from " + a_str_TableName + " NOHOLDLOCK " + str_WhereWithoutTimeStamp);
            if(rs.next())
              throw new DataBefImageErrorException(exc.getMessage());
            else  
              throw exc;
        } 
        catch (Exception exc) 
        {
        	log.error("",exc);
        	throw new SQLException(exc.getMessage());
        } 
        finally 
        {
            if (stmt != null)
                stmt.close();
            DBUtility.closeDBConnection(p_con_Database);
        } //end finally
    }

    /* 依PrimaryKey取得單一筆資料    */
    private TopLinkedHashMap isExists(
    							String           a_str_TableName, 
    							TopLinkedHashMap a_htl_RecordKey
    								) throws SQLTableSchemaException, SQLDataNotExistException, SQLException, SQLUnConnectException, DataBefImageErrorException
    {
    	
    	return isExists(a_str_TableName, a_htl_RecordKey, null, null);
    }
    
    /* 依PrimaryKey取得單一筆資料    */
    private TopLinkedHashMap isExists(
    							String a_str_TableName, 
    							TopLinkedHashMap a_lhm_RecordKey,
    							TopLinkedHashMap a_lhm_FieldsInfo,
    							Vector           a_vtr_PrimaryKey
    								) throws SQLTableSchemaException, SQLDataNotExistException, SQLException, SQLUnConnectException, DataBefImageErrorException
    {
        TopLinkedHashMap lhm_FieldInfo;
        Vector vtr_PrimaryKeys = null;
        Statement stmt = null;
        ResultSet rst_result = null;
        ResultSetMetaData rsmd = null;
        Connection p_con_Database = null;
        String str_WhereWithoutTimeStamp = "";
        
        try {
            /* 取得 Table的欄位相關資訊 包括欄位名稱及欄位型態 */
            if (a_lhm_FieldsInfo == null)
            {
            	lhm_FieldInfo = this.getTableInfo(a_str_TableName);
            }
            else
            {
            	lhm_FieldInfo = a_lhm_FieldsInfo;
            }
            /*            
                        Properties env=new Properties();
                        env.put("java.naming.factory.initial","com.sun.jndi.cosnaming.CNCtxFactory");
                        env.put("java.naming.provider.url","iiop://127.0.0.1:9010");
                        Context myc = new InitialContext(env);
                        Object objref = myc.lookup("ejb/TableSchema");            
                        ITableSchemaHome home = (ITableSchemaHome)PortableRemoteObject.narrow(objref,ITableSchemaHome.class);            
                        ITableSchema ts = home.create();
                        htl_FieldInfo=ts.getFieldInfo(a_str_TableName);
            */
            if (a_vtr_PrimaryKey == null)
            {
	            vtr_PrimaryKeys = new Vector((Collection) a_lhm_RecordKey.keySet());
            }
            else
            {
            	vtr_PrimaryKeys = a_vtr_PrimaryKey;
            }

        }
        catch (SQLDataNotExistException exc)
        {
        	throw exc;
        }
        catch (Exception exc)
        {
        	log.error("",exc);
            throw new SQLException(exc.getMessage());

        }

        String str_and = "";
        StringBuffer str_partialStr = new StringBuffer(""); //組成 "CtryCode='001' and PK='???'....."
        TopLinkedHashMap htl_rtnData = new TopLinkedHashMap();
        String str_columnName = "";
        
		Iterator itr_keySet = a_lhm_RecordKey.keySet().iterator();
		while (itr_keySet.hasNext())
		{
			str_columnName = (String) itr_keySet.next();
			
			//略過timestamp欄位
            if ( str_columnName.equals("timestamp") )         
                continue;	                
                    
            str_partialStr.append(str_and);
            if (((Integer) lhm_FieldInfo.get(str_columnName)).intValue() == Types.VARCHAR || ((Integer) lhm_FieldInfo.get(str_columnName)).intValue() == Types.CHAR || ((Integer) lhm_FieldInfo.get(str_columnName)).intValue() == Types.LONGVARCHAR)
                str_partialStr = str_partialStr.append(str_columnName).append("='").append((String) a_lhm_RecordKey.get(str_columnName)).append("'");
            else
                str_partialStr = str_partialStr.append(str_columnName).append("=").append((String) a_lhm_RecordKey.get(str_columnName));
            str_and = " and ";                               			
		} // end while
		
		str_WhereWithoutTimeStamp = str_partialStr.toString();
						
    	//新增timestamp判斷！
        if (a_lhm_RecordKey.containsKey("timestamp"))
        {
            str_partialStr.append(" and timestamp=0x"+ (String)a_lhm_RecordKey.get("timestamp"));
        }     

        String str_queryStr = "SELECT * FROM " + a_str_TableName + " NOHOLDLOCK " + " WHERE " + str_partialStr;
        try {
            /*20030305*/
            //str_queryStr=new String(str_queryStr.getBytes("MS950"),"ISO8859_1");
        } catch (Exception exc) {
        	log.error("",exc);
        }

        log.debug("queryString is: " + str_queryStr);

        try {
//            if (p_con_Database == null) 
//            {
//            	log.debug("TableMaintainDAO.isExists() call DBUtility.getDBConnection()");
//                p_con_Database = DBUtility.getDBConnection();
//            }
			p_con_Database = DBUtility.getDBConnection();
            stmt = p_con_Database.createStatement();
            Iterator itr_keys = lhm_FieldInfo.keySet().iterator();
            rst_result = stmt.executeQuery(str_queryStr);
            
//			log.debug("***Done : "+ str_queryStr);
            
            //rsmd = rst_result.getMetaData();
            Object obj_key = null;
            if (rst_result.next()) {
                while (itr_keys.hasNext()) {
                    obj_key = itr_keys.next();
                    if (!(rst_result.getString((String) obj_key) == null)) {

                        //                        htl_rtnData.put(obj_key,(new String(rst_result.getString((String)obj_key).getBytes("ISO8859_1"),"MS950")).trim());                    
                        /*20030305*/
                        //htl_rtnData.put(obj_key,(new String(rst_result.getString((String)obj_key).getBytes("ISO8859_1"),"ISO8859_1")).trim());                    
                        htl_rtnData.put(obj_key, (new String(rst_result.getString((String) obj_key))).trim());
                    } else if (((Integer) lhm_FieldInfo.get(obj_key)).intValue() == Types.CHAR || ((Integer) lhm_FieldInfo.get(obj_key)).intValue() == Types.LONGVARCHAR || ((Integer) lhm_FieldInfo.get(obj_key)).intValue() == Types.VARCHAR)
                        htl_rtnData.put(obj_key, "");
                    else
                        htl_rtnData.put(obj_key, "0");
                } //end while                       
            } else
                throw new SQLDataNotExistException();
        } catch (NullPointerException exc) {
        	log.error("",exc);
                //Log4j for logging  -->  日後會加上,目前暫無
                throw new SQLException("發生錯誤!!!請通知系統人員", "DAO");
                //throw new SQLUnConnectException("未取得資料庫連結，發生錯誤!!!請通知系統人員");
         }catch (SQLUnConnectException exc) {
         	log.error("",exc);
             throw exc;
        } catch (SQLDataNotExistException exc) {
        	ResultSet rs = stmt.executeQuery("select * from " + a_str_TableName + " NOHOLDLOCK " + " WHERE " + str_WhereWithoutTimeStamp);
            if(rs.next())
              throw new DataBefImageErrorException(exc.getMessage());
            else  
              throw exc;
        } catch (Exception exc) {
			log.error("",exc);
        	throw new SQLException(exc.getMessage());        	 
        } finally {
            if (rst_result != null) {
                rst_result.close();
            }
            if (stmt != null) {
                stmt.close();
            }
			DBUtility.closeDBConnection(p_con_Database);
        } //end--finally{ 
        return htl_rtnData;
    } // end isExists()    

    private TopLinkedHashMap selectData(
    							String           a_str_TableName, 
    							Vector           a_vtr_Fields, 
    							TopLinkedHashMap a_htl_RecordKey,
    							TopLinkedHashMap a_htl_Order,
    							TopLinkedHashMap a_lhm_FieldsInfo,
    							Vector           a_vtr_PrimaryKey
    							) throws SQLTableSchemaException, SQLDataNotExistException, SQLException, SQLUnConnectException
    {
        TopLinkedHashMap htl_rtnData = new TopLinkedHashMap();
        Vector vtr_PrimaryKeys;
        Vector vtr_Fields = new Vector();
        TopLinkedHashMap htl_FieldInfo;
        String str_queryStr;
        StringBuffer str_partialStr1 = new StringBuffer("");
        StringBuffer str_partialStr2 = new StringBuffer();
        StringBuffer str_partialStr3 = new StringBuffer();
        ResultSet rst_result = null, rst_rowcount = null;
        Statement stmt = null;
        Connection p_con_Database = null;

        ResultSetMetaData rsmd = null;
        try {
            /* 取得 Table的欄位相關資訊 包括欄位名稱及欄位型態 */
            if (a_lhm_FieldsInfo == null)
            {
            	htl_FieldInfo = this.getTableInfo(a_str_TableName);
            }
            else
            {
            	htl_FieldInfo = a_lhm_FieldsInfo;
            }
            /* 取得 Table 的 PK*/
            if (a_vtr_PrimaryKey == null)
            {
	            vtr_PrimaryKeys = this.getPrimaryKey(a_str_TableName);
            }
            else
            {
            	vtr_PrimaryKeys = a_vtr_PrimaryKey;
            }

            /*            Properties env=new Properties();
                        env.put("java.naming.factory.initial","com.sun.jndi.cosnaming.CNCtxFactory");
                        env.put("java.naming.provider.url","iiop://127.0.0.1:9010");
                        Context myc = new InitialContext(env);
                        Object objref = myc.lookup("ejb/TableSchema");
                        ITableSchemaHome home = (ITableSchemaHome)PortableRemoteObject.narrow(objref,ITableSchemaHome.class);
                        ITableSchema ts = home.create();
                        vtr_PrimaryKeys=ts.getPrimaryKey(a_str_TableName);
                        htl_FieldInfo=ts.getTableInfo(a_str_TableName);
            */
            vtr_Fields = (Vector) a_vtr_Fields.clone();
            for (int i = 0; i < vtr_PrimaryKeys.size(); i++) {
                if (a_vtr_Fields.contains(vtr_PrimaryKeys.get(i)))
                    a_vtr_Fields.remove(vtr_PrimaryKeys.get(i));
            }

            a_vtr_Fields.addAll(vtr_PrimaryKeys);

        } catch (Exception exc) {
        	log.error("",exc);
            throw new SQLException(exc.getMessage());
        }

        if (!a_htl_Order.isEmpty()) {
            str_partialStr1 = str_partialStr1.append("order by ");
            String str_c = "";
            Set set_keys = a_htl_Order.keySet();

            for (int i = 1; i < set_keys.size(); i++) {
                str_partialStr1.append(str_c);
                str_partialStr1.append((String) a_htl_Order.get("Field" + i));
                str_c = ",";
            }
            String str_sort = null;
            if (((String) a_htl_Order.get("Sort")).toLowerCase().equals("asc"))
                str_sort = "asc";
            else
                str_sort = "desc";
            str_partialStr1.append(" ").append(str_sort);
        } //end -- if (!a_htl_Order.isEmpty()){     

        if (a_htl_RecordKey.isEmpty())
            str_partialStr2 = str_partialStr2.append("1=1");
        else {
            Iterator itr_keys = htl_FieldInfo.keySet().iterator();
            Object obj_key = null;
            String str_and = "";
            while (itr_keys.hasNext()) {
                obj_key = itr_keys.next();
                String str_FieldValue = (String) a_htl_RecordKey.get(obj_key);
                if (a_htl_RecordKey.containsKey(obj_key)) {
                    str_partialStr2 = str_partialStr2.append(str_and);
                    if (((Integer) htl_FieldInfo.get(obj_key)).intValue() == Types.VARCHAR || ((Integer) htl_FieldInfo.get(obj_key)).intValue() == Types.CHAR || ((Integer) htl_FieldInfo.get(obj_key)).intValue() == Types.LONGVARCHAR)
                        str_partialStr2 = str_partialStr2.append((String) obj_key).append("='").append(str_FieldValue).append("'");
                    else
                        str_partialStr2 = str_partialStr2.append((String) obj_key).append("=").append(str_FieldValue);
                    str_and = " and ";
                } // end if 
            } //end while
        } //end else

        if (vtr_Fields.isEmpty()) {
            str_partialStr3 = str_partialStr3.append("*");
        } else {
            String str_c = "";
            for (int i = 0; i < a_vtr_Fields.size(); i++) {
                str_partialStr3.append(str_c);
                str_partialStr3.append((String) a_vtr_Fields.get(i));
                str_c = ",";
            }
        } //end -- if (vtr_Fields.isEmpty()){ 

        str_queryStr = "select " + str_partialStr3 + " from " + a_str_TableName + " NOHOLDLOCK " + " where " + str_partialStr2 + " " + str_partialStr1;

        try {
            /*20030305*/
            //str_queryStr=new String(str_queryStr.getBytes("MS950"),"ISO8859_1");            
            log.debug("queryStr= " + str_queryStr);
//            if (p_con_Database == null) 
//            {
//            	log.debug("TableMaintainDAO.selectData() call DBUtility.getDBConnection()");
//                p_con_Database = DBUtility.getDBConnection();
//            }
			p_con_Database = DBUtility.getDBConnection();
            stmt = p_con_Database.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            rst_result = stmt.executeQuery(str_queryStr);
            
//			log.debug("***Done : "+ str_queryStr);
            
            if (!rst_result.next())
                throw new SQLDataNotExistException();
            else {
                
                
                
                //取得總筆數
                //將 Record Point 指到最後一筆
//                rst_result.last();
                //取得最後一筆的 Row Index
//                int k = rst_result.getRow();
                //Record Point 移回第一筆
//                rst_result.first();

                
                rsmd = rst_result.getMetaData();
                int l = rsmd.getColumnCount();
                TopLinkedHashMap lhm_RowData = new TopLinkedHashMap();
//                TopLinkedHashMap[] htl_Data = new TopLinkedHashMap[k];
//                String[] str_KeyValue = new String[k];
				String str_KeyValue = "";


				do{
                    //for (int j=0;j<k;j++){
//                    int j=0;
					 	lhm_RowData = new TopLinkedHashMap();
                        str_KeyValue = "";
                        for (int i = 1; i <= l; i++) {
                            if (vtr_PrimaryKeys.contains(rsmd.getColumnName(i)))
								str_KeyValue = str_KeyValue + rst_result.getString(rsmd.getColumnName(i)).trim();

                            String str_temp;
                            if (rst_result.getString((String) rsmd.getColumnName(i)) == null) {
                                if (rsmd.getColumnType(i) == Types.CHAR || rsmd.getColumnType(i) == Types.LONGVARCHAR || rsmd.getColumnType(i) == Types.VARCHAR)
                                    str_temp = "";
                                else
                                    str_temp = "0";
                            } else
                                str_temp = rst_result.getString((String) rsmd.getColumnName(i));
                            /*20030305*/
                            //htl_Data[j].put(rsmd.getColumnName(i),(new String(str_temp.getBytes("ISO8859_1"),"MS950")).trim());
							lhm_RowData.put(rsmd.getColumnName(i), str_temp.trim());
                        } //end for     
                        htl_rtnData.put(str_KeyValue, lhm_RowData);
                    //}// end for
//                    j++;
                } while (rst_result.next()) ;
            } //end --if (!rst_result.next())
        } catch (NullPointerException exc) {
                log.error("",exc);
                //Log4j for logging  -->  日後會加上,目前暫無
                throw new SQLException("發生錯誤!!!請通知系統人員", "DAO");
                //throw new SQLUnConnectException("未取得資料庫連結，發生錯誤!!!請通知系統人員");
          }catch (SQLUnConnectException exc) {
          	log.error("",exc);
             throw exc;
        } catch (SQLDataNotExistException exc) {
        	// log.error("",exc);
            throw exc;
        } catch (Exception exc) {
        	log.error("",exc);
        } finally {
            if (rst_result != null)
                rst_result.close();
            if (rst_rowcount != null)
                rst_rowcount.close();
            if (stmt != null)
                stmt.close();
            DBUtility.closeDBConnection(p_con_Database);


        }
        return htl_rtnData;
    } // end selectData

    private TopLinkedHashMap selectData(
    							String           a_str_TableName, 
    							Vector           a_vtr_Fields, 
    							String[][]       a_str_RecordKey, 
    							TopLinkedHashMap a_htl_Order,
    							TopLinkedHashMap a_lhm_FieldsInfo,
    							Vector           a_vtr_PrimaryKey
    							) throws SQLTableSchemaException, SQLDataNotExistException, SQLUnConnectException, SQLException, Exception
    {
        TopLinkedHashMap htl_rtnData = new TopLinkedHashMap();
        Vector vtr_PrimaryKeys;
        Vector vtr_Fields = new Vector();
        TopLinkedHashMap htl_FieldInfo;
        String str_queryStr;
        StringBuffer str_partialStr1 = new StringBuffer("");
        StringBuffer str_partialStr2 = new StringBuffer();
        StringBuffer str_partialStr3 = new StringBuffer();
        ResultSet rst_result = null, rst_rowcount = null;
        Statement stmt = null;
        Connection p_con_Database = null;

        try {
            /* 取得 Table的欄位相關資訊 包括欄位名稱及欄位型態 */
            if (a_lhm_FieldsInfo == null)
            {
            	htl_FieldInfo = this.getTableInfo(a_str_TableName);
            }
            else
            {
            	htl_FieldInfo = a_lhm_FieldsInfo;
            }
            
            /* 取得 Table 的 PK*/
            if (a_vtr_PrimaryKey == null)
            {
            	vtr_PrimaryKeys = this.getPrimaryKey(a_str_TableName);
            }
            else
            {
            	vtr_PrimaryKeys = a_vtr_PrimaryKey;
            }

            /*            Properties env=new Properties();
                        env.put("java.naming.factory.initial","com.sun.jndi.cosnaming.CNCtxFactory");
                        env.put("java.naming.provider.url","iiop://127.0.0.1:9010");
                        Context myc = new InitialContext(env);
                        Object objref = myc.lookup("ejb/TableSchema");
                        log.debug("Lookup finished");
                        ITableSchemaHome home = (ITableSchemaHome)PortableRemoteObject.narrow(objref,ITableSchemaHome.class);
                        log.debug("Class cast finished");
                        ITableSchema ts = home.create();
                        log.debug("Create finished");
                        vtr_PrimaryKeys=ts.getPrimaryKey(a_str_TableName);
                        htl_FieldInfo=ts.getTableInfo(a_str_TableName);
            */
            vtr_Fields = (Vector) a_vtr_Fields.clone();
            for (int i = 0; i < vtr_PrimaryKeys.size(); i++) {
                if (a_vtr_Fields.contains(vtr_PrimaryKeys.get(i)))
                    a_vtr_Fields.remove(vtr_PrimaryKeys.get(i));
            }

            a_vtr_Fields.addAll(vtr_PrimaryKeys);

        } catch (Exception exc) {
        	log.error("",exc);
            throw new SQLException(exc.getMessage());
        }

//log.debug("order=" + a_htl_Order);
        if (!a_htl_Order.isEmpty()) {
            str_partialStr1 = str_partialStr1.append("order by ");
            String str_c = "";
            Set set_keys = a_htl_Order.keySet();
            for (int i = 1; i < set_keys.size(); i++) {
                str_partialStr1.append(str_c);
//log.debug(a_htl_Order.get("Field" + i));
                str_partialStr1.append((String) a_htl_Order.get("Field" + i));
                str_c = ",";
            }
            String str_sort = null;
            if (((String) a_htl_Order.get("Sort")).toLowerCase().equals("asc"))
                str_sort = "asc";
            else
                str_sort = "desc";
            str_partialStr1.append(" ").append(str_sort);
        } //end --if (!a_htl_Order.isEmpty()){  

//log.debug("str1=" + str_partialStr1);
        if (a_str_RecordKey.length == 0)
            str_partialStr2 = str_partialStr2.append("1=1");
        else {
            String str_and = "";
            String str_operator = "";
            for (int i = 0; i < a_str_RecordKey.length; i++) {
            	str_operator = " " + a_str_RecordKey[i][1] + " ";	// 解決 'like' 這類的運算符號的問題. 例如: MsgCode like '7%'
                str_partialStr2 = str_partialStr2.append(str_and);
                if (((Integer) htl_FieldInfo.get(a_str_RecordKey[i][0])).intValue() == Types.VARCHAR
                    || ((Integer) htl_FieldInfo.get(a_str_RecordKey[i][0])).intValue() == Types.CHAR
                    || ((Integer) htl_FieldInfo.get(a_str_RecordKey[i][0])).intValue() == Types.LONGVARCHAR)
                {
                    //str_partialStr2 = str_partialStr2.append(a_str_RecordKey[i][0]).append(a_str_RecordKey[i][1]).append("'").append(a_str_RecordKey[i][2]).append("'");
					str_partialStr2 = str_partialStr2.append(a_str_RecordKey[i][0]).append(str_operator).append("'").append(a_str_RecordKey[i][2]).append("'");
				}
                else
                {
                    //str_partialStr2 = str_partialStr2.append(a_str_RecordKey[i][0]).append(a_str_RecordKey[i][1]).append(a_str_RecordKey[i][2]);
					str_partialStr2 = str_partialStr2.append(a_str_RecordKey[i][0]).append(str_operator).append(a_str_RecordKey[i][2]);
				}
                str_and = " and ";
            } //end for 
        }

        if (vtr_Fields.isEmpty()) {
            str_partialStr3 = str_partialStr3.append("*");
        } else {
            String str_c = "";
            for (int i = 0; i < a_vtr_Fields.size(); i++) {
                str_partialStr3.append(str_c);
                str_partialStr3.append((String) a_vtr_Fields.get(i));
                str_c = ",";
            }
        }

        str_queryStr = "select " + str_partialStr3 + " from " + a_str_TableName + " NOHOLDLOCK " + " where " + str_partialStr2 + " " + str_partialStr1;
        /*20030305*/
        //str_queryStr=new String(str_queryStr.getBytes("MS950"),"ISO8859_1");
        log.debug("queryStr= " + str_queryStr);

        try {
//            if (p_con_Database == null) 
//            {
//            	log.debug("TableMaintainDAO.selectData() call DBUtility.getDBConnection()");
//                p_con_Database = DBUtility.getDBConnection();
//            }
			p_con_Database = DBUtility.getDBConnection();
            stmt = p_con_Database.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);

            rst_result = stmt.executeQuery(str_queryStr);
            
//			log.debug("***Done : "+ str_queryStr);
            
            if (!rst_result.next())
                throw new SQLDataNotExistException();
            else {
                //取得總筆數
                //將 Record Point 指到最後一筆
//                rst_result.last();
                //取得最後一筆的 Row Index
//                int k = rst_result.getRow();
                //Record Point 移回第一筆
//                rst_result.first();
                
                ResultSetMetaData rsmd = rst_result.getMetaData();
                int l = rsmd.getColumnCount();
                TopLinkedHashMap lhm_RowData = new TopLinkedHashMap();
//                TopLinkedHashMap[] htl_Data = new TopLinkedHashMap[k];
//                String[] str_KeyValue = new String[k];

                
				do{
                    //for (int j=0;j<k;j++){
//                    int j =0; 
						lhm_RowData = new TopLinkedHashMap();
                        String str_KeyValue = "";
                        for (int i = 1; i <= l; i++) {
                            if (vtr_PrimaryKeys.contains(rsmd.getColumnName(i)))
                                str_KeyValue = str_KeyValue + rst_result.getString(rsmd.getColumnName(i)).trim();

                            String str_temp;
                        
                            if (rst_result.getString((String) rsmd.getColumnName(i)) == null) {
                                if (rsmd.getColumnType(i) == Types.CHAR || rsmd.getColumnType(i) == Types.LONGVARCHAR || rsmd.getColumnType(i) == Types.VARCHAR)
                                    str_temp = "";
                                else
                                    str_temp = "0";
                            } else
                                str_temp = rst_result.getString((String) rsmd.getColumnName(i));
                            /*20030305*/
                            //htl_Data[j].put(rsmd.getColumnName(i),(new String(str_temp.getBytes("ISO8859_1"),"MS950")).trim());
							lhm_RowData.put(rsmd.getColumnName(i), str_temp.trim());
                        } //end for     

                        htl_rtnData.put(str_KeyValue, lhm_RowData);
                        //log.debug("~~~~~~DAO str_KeyValue[j]~~~~~~=" + str_KeyValue[j] + "=~~~~~~");
                        //log.debug("~~~~~~DAO htl_Data[j]~~~~~~=" + htl_Data[j] + "=~~~~~~");
                        //log.debug("~~~~~~DAO htl_rtnData(str_KeyValue[j],htl_Data[j])~~~~~~=" + htl_rtnData + "=~~~~~~");         
                    //}// end for
//                    j++;
                }while (rst_result.next());
            } //end --if (!rst_result.next()) 
          } catch (NullPointerException exc) {
                log.error("",exc);
                //Log4j for logging  -->  日後會加上,目前暫無
                throw new SQLException("發生錯誤!!!請通知系統人員", "DAO");
                //throw new SQLUnConnectException("未取得資料庫連結，發生錯誤!!!請通知系統人員");
          }catch (SQLUnConnectException exc) {
        	log.error("",exc);
             throw exc;
          }catch (SQLDataNotExistException exc)
          {
        	// log.error("",exc);
            throw exc;
          }catch(Exception exc) {          
          	log.error("",exc);
        } finally {
            if (rst_result != null)
                rst_result.close();
            if (rst_rowcount != null)
                rst_rowcount.close();
            if (stmt != null)
                stmt.close();
            DBUtility.closeDBConnection(p_con_Database);

        } //emd finally

        return htl_rtnData;
    }


    private void deleteByPartialKey(
    				String           a_str_TableName, 
    				String           a_str_MaintainUserID, 
    				TopLinkedHashMap a_htl_RecordKey,
    				TopLinkedHashMap a_lhm_FieldsInfo,
    				Vector           a_vtr_PrimaryKey
    					) throws SQLTableSchemaException, SQLLogException, SQLDataNotExistException, SQLRelationException, SQLUnConnectException, SQLDataNotExistException, SQLException,SQLDeadlockException
    {
        TopLinkedHashMap htl_LogData;
        Statement stmt = null;
        TopLinkedHashMap htl_LogDataDetail;
        TopLinkedHashMap htl_DateTime;
        TopLinkedHashMap htl_FieldInfo;
        Vector vtr_PrimaryKeys;
        Connection p_con_Database = null;
        try {
            if (a_str_MaintainUserID != null) {
                htl_LogData = selectData(a_str_TableName, new Vector(), a_htl_RecordKey, new TopLinkedHashMap(), null, null);
                Vector vtr_values = new Vector(htl_LogData.values());
                for (int i = 0; i < vtr_values.size(); i++) {
                    htl_LogDataDetail = (TopLinkedHashMap) vtr_values.get(i);
                    htl_LogDataDetail.put("MaintainUserID", a_str_MaintainUserID);
                    htl_LogDataDetail.put("SystemDate", Machine.getSystemDateTime().get("SystemDate"));
                    htl_LogDataDetail.put("SystemTime", Machine.getSystemDateTime().get("SystemTime"));
                    htl_LogDataDetail.put("ModifyType", "D");
                    create(a_str_TableName + "Log", htl_LogDataDetail);
                }
            } //if (a_str_MaintainUserID!=null)

            /* 取得 Table的欄位相關資訊 包括欄位名稱及欄位型態 */
            if (a_lhm_FieldsInfo == null)
            {
            	htl_FieldInfo = this.getTableInfo(a_str_TableName);
            }
            else
            {
            	htl_FieldInfo = a_lhm_FieldsInfo;
            }
            /* 取得 Table 的 PK*/
            if (a_vtr_PrimaryKey == null)
            {
            	vtr_PrimaryKeys = this.getPrimaryKey(a_str_TableName);
            }
            else
            {
            	vtr_PrimaryKeys = a_vtr_PrimaryKey;
            }

            /*            Properties env=new Properties();
                        env.put("java.naming.factory.initial","com.sun.jndi.cosnaming.CNCtxFactory");
                        env.put("java.naming.provider.url","iiop://127.0.0.1:9010");
                        Context myc = new InitialContext(env);
                        Object objref = myc.lookup("ejb/TableSchema");
                        log.debug("Lookup finished");
                        ITableSchemaHome home = (ITableSchemaHome)PortableRemoteObject.narrow(objref,ITableSchemaHome.class);
                        log.debug("Class cast finished");
                        ITableSchema ts = home.create();
                        log.debug("Create finished");
                        vtr_PrimaryKeys=ts.getPrimaryKey(a_str_TableName);
                        htl_FieldInfo=ts.getTableInfo(a_str_TableName);
            */

            StringBuffer str_partialStr = new StringBuffer(""); //組成 "CtryCode='001' and PK='???'....."
            if (a_htl_RecordKey.isEmpty())
                str_partialStr = str_partialStr.append("1=1");
            else {
                Iterator itr_keys = htl_FieldInfo.keySet().iterator();
                Object obj_key = null;
                String str_and = "";
                while (itr_keys.hasNext()) {
                    obj_key = itr_keys.next();
                    if (a_htl_RecordKey.containsKey(obj_key)) {
                        str_partialStr = str_partialStr.append(str_and);
                        if (((Integer) htl_FieldInfo.get(obj_key)).intValue() == Types.VARCHAR || ((Integer) htl_FieldInfo.get(obj_key)).intValue() == Types.CHAR || ((Integer) htl_FieldInfo.get(obj_key)).intValue() == Types.LONGVARCHAR)
                            str_partialStr = str_partialStr.append((String) obj_key).append("='").append((String) a_htl_RecordKey.get(obj_key)).append("'");
                        else
                            str_partialStr = str_partialStr.append((String) obj_key).append("=").append((String) a_htl_RecordKey.get(obj_key));
                        str_and = " and ";
                    } //end -- if (a_htl_RecordKey.containsKey(obj_key)) {
                } // end while
            } //end--if (a_htl_RecordKey.isEmpty()
//            if (p_con_Database == null) 
//            {
//            	log.debug("TableMaintainDAO.deleteByPartialKey() call DBUtility.getDBConnection()");
//                p_con_Database = DBUtility.getDBConnection();
//            }
			p_con_Database = DBUtility.getDBConnection();
            stmt = p_con_Database.createStatement();
            String queryStr = "Delete from " + a_str_TableName + " where " + str_partialStr;
            /*20030305*/
            //queryStr=new String(queryStr.getBytes("MS950"),"ISO8859_1");
            log.debug("queryString= " + queryStr);
            int i_resultcount = stmt.executeUpdate(queryStr);
			
//			log.debug("***Done : "+ queryStr);
            
            if (i_resultcount == 0)
                throw new SQLDataNotExistException();
        } catch (SQLException exc) {
        	log.error("",exc);
            int i_errorcode = exc.getErrorCode();
            switch (i_errorcode) {
                case 546 :
                    throw new SQLRelationException(exc.getMessage());
                case 2601 :
                    throw new SQLLogException();
				case 1205 :
					throw new SQLDeadlockException(exc.getMessage());

                default :
                    {
                        log.debug("default error");
                        throw exc;
                    }
            } //end --switch (i_errorcode){
          } catch (NullPointerException exc) {
                log.error("",exc);
                //Log4j for logging  -->  日後會加上,目前暫無
                throw new SQLException("發生錯誤!!!請通知系統人員", "DAO");
                //throw new SQLUnConnectException("未取得資料庫連結，發生錯誤!!!請通知系統人員");
          }catch (SQLUnConnectException exc) {
        	log.error("",exc);
             throw exc;
          }catch (SQLDataNotExistException exc)
          {
        	log.error("",exc);
            throw exc;
          } catch (Exception exc) {
          	log.error("",exc);
			throw new SQLException(exc.getMessage());
          } finally {
            if (stmt != null)
                stmt.close();
            DBUtility.closeDBConnection(p_con_Database);
        }

    }

    private TopLinkedHashMap CasttoString(TopLinkedHashMap htl_original) {
        Iterator itr_keys = htl_original.keySet().iterator();
        String str_key;
        int i = 0;
        String[] str_values = new String[htl_original.size()];
        Object obj_o = new Object();
        
        try
        {
	        while (itr_keys.hasNext()) {
	            str_key = (String) itr_keys.next();
	            obj_o = htl_original.get(str_key);
	            if (obj_o instanceof String)
	                str_values[i] = (String) obj_o;
	            if (obj_o instanceof Integer)
	                str_values[i] = ((Integer) obj_o).toString();
	            if (obj_o instanceof java.math.BigDecimal)
	                str_values[i] = ((java.math.BigDecimal) obj_o).toString();
	            if (obj_o instanceof Byte)
	                str_values[i] = ((Byte) obj_o).toString();
	            if (obj_o instanceof Short)
	                str_values[i] = ((Short) obj_o).toString();
	            if (obj_o instanceof Float)
	                str_values[i] = ((Float) obj_o).toString();
	            if (obj_o instanceof Double)
	                str_values[i] = ((Double) obj_o).toString();
	            if (obj_o instanceof java.math.BigInteger)
	                str_values[i] = ((java.math.BigInteger) obj_o).toString();
	            if (obj_o instanceof StringBuffer)
	                str_values[i] = ((StringBuffer) obj_o).toString();
	            htl_original.put(str_key, str_values[i]);
	            i++;
	        }
        }
        catch (Exception exc)
        {
        	log.error("",exc);
        	log.error("",exc);
        }

        return htl_original;
    }

    /* 取得 Table的欄位相關資訊 包括欄位名稱及欄位型態 */
    public TopLinkedHashMap getTableInfo(String a_str_TableName) throws Exception {

        return null;
    }

    /* 取得 Table 的 PK */
    public Vector getPrimaryKey(String a_str_TableName) throws Exception {

        return null;
    }

    // 依傳入的 SQL Statement 讀取資料,並 return Vector
    // 因為可能沒有 where 條件，所以是 return Vector，而不是 return TopLinkedHashMap。
    public static Vector executeQueryVtr(String a_str_Query) throws Exception 
    {
        ResultSet  rst_result     = null;
        Statement  stmt           = null;
        Connection p_con_Database = null;
        Vector	vtr_allRows		  = new Vector();

        try 
        {
            
            log.debug("***TableMaintainDAO.executeQueryVtr() = "+ a_str_Query);
            
            //Type= Scroll Insensitive(可 Scroll , 不顯示已被更改的資料
            //Cocurrency = Concur read only(不可修改資料)
			p_con_Database = DBUtility.getDBConnection();
            stmt = p_con_Database.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            rst_result = stmt.executeQuery(a_str_Query);
            
//			log.debug("***Done : "+ a_str_Query);
			            
			stmt.setFetchSize(1000);
            ResultSetMetaData rmd_result = rst_result.getMetaData();         
            int    i_ColumnCount  = rmd_result.getColumnCount();
            int i_RowCount = 0;
            double d_size = 0d;
            double d_row_size = 0d;
            double d_MaxSize = 50*1024*1024;
            String str_ColumnName  = null;
            String str_ColumnValue = null;

            while (rst_result.next())
            {
				i_RowCount++;
            	// new a TopLinkedHashMap 來放置目前這筆資料
            	TopLinkedHashMap lhm_thisRow = new TopLinkedHashMap();
            	
            	// 得到目前這筆資料的每個 ColumnName and ColumnValue
	            for (int i=1; i<=i_ColumnCount; ++i)
	            {
	            	str_ColumnName  = rmd_result.getColumnName(i);
	            	
					str_ColumnValue = rst_result.getString(str_ColumnName);
					
	            	//值為 Null, 依據欄位資料型態給預設值
	            	if (str_ColumnValue == null)
	            	{
	            		//為文字欄位,預設值為空白
						if ( rmd_result.getColumnType(i) == Types.VARCHAR
							|| rmd_result.getColumnType(i) == Types.CHAR
							|| rmd_result.getColumnType(i) == Types.LONGVARCHAR)
						{
							str_ColumnValue = "";
							d_size += str_ColumnName.trim().length()*2;
						}
						//為數字欄位,預設值為 0
						else
						{
							str_ColumnValue = "0";
							d_size += str_ColumnName.trim().length()*2 + 2;
						}						
	            	}
	            	else
	            	{
	            		str_ColumnValue = str_ColumnValue.trim();
						d_size += str_ColumnName.trim().length()*2 + str_ColumnValue.length()*2;
	            	}    	
	            	// TopLinkedHashMap 中的每個 entry 是 (ColumnName, ColumnValue)
	            	lhm_thisRow.put(str_ColumnName, str_ColumnValue);
	            }
				if(i_RowCount==1)
				{				
					d_row_size = d_size;					
				}	
									
				if(d_size > d_MaxSize)
				{
					throw new TopException("00012","TableMaintainDAO.executeQueryVtr()/RowCount="+i_RowCount+"/row_size="+d_row_size+"/AllSize="+d_size);
				}	        
	            
	            // 將此筆資料放入要回傳的資料中
	            vtr_allRows.add(lhm_thisRow);

            }                
            
        }
		catch (SQLException exc) 
		{            
			int i_exc = exc.getErrorCode();
			switch (i_exc) 
			{
				case 1205 :
					throw new SQLDeadlockException(exc.getMessage());
					
				default:
					throw exc;					
			} //end switch
		}                
        catch (NullPointerException exc)
        {
        	log.error("",exc);
            throw new SQLUnConnectException("未取得資料庫連結，發生錯誤!!!請通知系統人員");
        }
        catch(TopException exc)
        {
        	log.error("",exc);
        	throw exc;
        }
        catch(Exception exc) 
        {
        	log.error("",exc);
            throw new TopException("00001", exc.getMessage());
        }
        finally 
        {
        	// close all allocated resources, 好讓他人使用。
        	try
        	{
	        	if (rst_result != null)
	        	{
	        		rst_result.close();
	        	}
	        	if (stmt != null)
	        	{
	        		stmt.close();
	        	}
	        	DBUtility.closeDBConnection(p_con_Database);
        	}
        	catch (Exception exc)
        	{
        		log.error("",exc);
        		throw new TopException("00001", exc.getMessage());
        	}
        }

        return vtr_allRows;  
    } 
    
//	/** 
//	 * @author WUSHH
//	 * @param a_str_Query	
//	 * @param a_i_Page_NO	操作的當前頁面
//	 * @param a_str_PageAction	對頁面的操作動作（First 第一頁,Pre 上一頁,Next 下一頁,Last 最後一頁）
//	 * @param a_i_ItemNumsOfPage 每頁顯示的數量
//	 * @return
//	 * @throws Exception
//	 * 用於分頁進行數據的讀取
//	 */
//	public static Vector executeQueryVtr(String a_str_Query,int a_i_Page_NO,String a_str_PageAction,int a_i_ItemNumsOfPage) throws Exception 
//	{		
//		ResultSet  rst_result     = null;
//		Statement  stmt           = null;
//		Connection p_con_Database = null;
//		Vector	vtr_allRows		  = new Vector();
//
//		try 
//		{
//			log.debug("*Page***TableMaintainDAO.executeQueryVtr() = "+ a_str_Query);
//            
//			//Type= Scroll Insensitive(可 Scroll , 不顯示已被更改的資料
//			//Cocurrency = Concur read only(不可修改資料)
//			p_con_Database = DBUtility.getDBConnection();
//			stmt = p_con_Database.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
//			rst_result = stmt.executeQuery(a_str_Query);
//            
////			log.debug("***Done : "+ a_str_Query);
//			            			
//			ResultSetMetaData rmd_result = rst_result.getMetaData();
//			int    i_ColumnCount  = rmd_result.getColumnCount();
//			String str_ColumnName  = null;
//			String str_ColumnValue = null;
//			
//			//资料的总笔数
//			int iTotalCount = 0;
//			//当前页的笔数
//			int iPerPageCount = 0 ;			
//			//欲取得数据的页号
//			int iNowPageNO = 0 ;
//			//当前页
//			int iRealNowPageNO = 1;
//			
//			if("First".equals(a_str_PageAction))
//			{
//				iNowPageNO = 1;
//			}
//			else if("Pre".equals(a_str_PageAction))
//			{
//				iNowPageNO = a_i_Page_NO - 1;				
//			}
//			else if("Next".equals(a_str_PageAction))
//			{
//				iNowPageNO = a_i_Page_NO + 1;
//			}
//			else if("Last".equals(a_str_PageAction))
//			{
//				iNowPageNO = Integer.MAX_VALUE;
//			}
//			else
//			{
//				iNowPageNO = a_i_Page_NO;
//			}
//			
//			if(iNowPageNO <= 0)
//				iNowPageNO = 1;
//			
//			int iCurrCount = iNowPageNO * a_i_ItemNumsOfPage;
//			
//			for( ; rst_result.next() ; )
//			{
//				iTotalCount++;
//				//若为尾页,则每页都要写进Vector,直到最后一页
//				if( iNowPageNO == Integer.MAX_VALUE )
//				{				
//					iPerPageCount++;
//					//当已经存满每页的笔数，则清空，重新开始插入
//					if(iPerPageCount>a_i_ItemNumsOfPage)
//					{
//						vtr_allRows.clear();
//						iPerPageCount = 1;
//						iRealNowPageNO++;
//					}
//					setVectorFromResultSet(rst_result,vtr_allRows,rmd_result,i_ColumnCount);
//
//				}	
//				else
//				{
//					if(iTotalCount <= iCurrCount )
//					{					
//						iPerPageCount++;
//						//当已经存满每页的笔数，则清空，重新开始插入
//						if(iPerPageCount>a_i_ItemNumsOfPage)
//						{
//							vtr_allRows.clear();
//							iPerPageCount = 1;
//							iRealNowPageNO++;
//						}
//						setVectorFromResultSet(rst_result,vtr_allRows,rmd_result,i_ColumnCount);
//
//					}
//				}
//			} // end for
//			
//			TopLinkedHashMap lhm_PageInfo = new TopLinkedHashMap();
//			//記載頁面動作之後的當前頁數,當前頁面顯示的筆數,总页数
//			lhm_PageInfo.put("Page_NO",String.valueOf(iRealNowPageNO)); 
//			lhm_PageInfo.put("Count",String.valueOf(iPerPageCount));
//			
//			//modified by Steven,2004/11/17
//			lhm_PageInfo.put("Page_Nums",
//			                String.valueOf((int)Math.ceil((double)iTotalCount/(double)a_i_ItemNumsOfPage)));
//			
//			lhm_PageInfo.put("TotalCount",String.valueOf(iTotalCount));
//			//将页面的基本信息放在vector的第一个index上
//			vtr_allRows.add(0, lhm_PageInfo);
//            
//		}
//		catch (SQLException exc) 
//		{            
//			int i_exc = exc.getErrorCode();
//			switch (i_exc) 
//			{
//				case 1205 :
//					throw new SQLDeadlockException(exc.getMessage());
//					
//				default:
//					throw exc;					
//			} //end switch
//		}                		
//		catch (NullPointerException exc)
//		{
//			log.error("",exc);
//			throw new SQLUnConnectException("未取得資料庫連結，發生錯誤!!!請通知系統人員");
//		}
//		catch(Exception exc) 
//		{
//			log.error("",exc);
//			throw new TopException("00001", exc.getMessage());
//		}
//		finally 
//		{
//			// close all allocated resources, 好讓他人使用。
//			try
//			{
//				if (rst_result != null)
//				{
//					rst_result.close();
//				}
//				if (stmt != null)
//				{
//					stmt.close();
//				}
//				DBUtility.closeDBConnection(p_con_Database);
//			}
//			catch (Exception exc)
//			{
//				log.error("",exc);
//				throw new TopException("00001", exc.getMessage());
//			}
//		}
//
//		return vtr_allRows;  
//	}     

	/** 
	 * @author  Steven
	 * @param a_str_Query	
	 * @param a_i_Page_NO	操作的當前頁面
	 * @param a_str_PageAction	對頁面的操作動作（First 第一頁,Pre 上一頁,Next 下一頁,Last 最後一頁）
	 * @param a_i_ItemNumsOfPage 每頁顯示的數量
	 * 
	 * @return Vector ==>共包含兩個 lhm
	 *                                 第一個紀錄 : 當前頁次,總頁次,每頁資料筆數,執行之sql語句
	 *                                 第二個紀錄 : 各個資料 entry(已加上"_N")
	 *  
	 * @throws Exception
	 * 用於分頁進行數據的讀取
	 */
	public static Vector executeQueryVtr(String a_str_Query,int a_i_Page_NO,String a_str_PageAction,int a_i_ItemNumsOfPage) throws Exception 
	{		
		ResultSet  rst_result     = null;
		Statement  stmt           = null;
		Connection p_con_Database = null;
		Vector	vtr_allRows		  = new Vector();

		try 
		{
			log.debug("*Page***TableMaintainDAO.executeQueryVtr() = "+ a_str_Query);
            
			//Type = TYPE_SCROLL_INSENSITIVE(可 Scroll , 不顯示已被更改的資料)
			//Cocurrency = CONCUR_READ_ONLY(不可修改資料)
			p_con_Database = DBUtility.getDBConnection();
			stmt = p_con_Database.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			rst_result = stmt.executeQuery(a_str_Query);
 			            			
			ResultSetMetaData rmd_result = rst_result.getMetaData();
			int    i_ColumnCount  = rmd_result.getColumnCount();
			String str_ColumnName  = null;
			String str_ColumnValue = null;
			
			rst_result.last();
			int i_ResultSetRowCount = rst_result.getRow();

			//資料總頁數
			int i_TotalPageNum = (int)Math.ceil((double)i_ResultSetRowCount/(double)a_i_ItemNumsOfPage); 
									
			//当前页的笔数
			int iPerPageCount = 0 ;			
			//欲取得数据的页号
			int iNowPageNO = 0 ;
			//当前页
			int iRealNowPageNO = 1;
			
			if("First".equals(a_str_PageAction))
			{
				iNowPageNO = 1;
			}
			else if("Pre".equals(a_str_PageAction))
			{
				iNowPageNO = a_i_Page_NO - 1;
							
				if (iNowPageNO <= 0)
					iNowPageNO = 1;					
			}
			else if("Next".equals(a_str_PageAction))
			{				
				if (a_i_Page_NO < i_TotalPageNum)
					iNowPageNO = a_i_Page_NO + 1;
				else
					iNowPageNO = a_i_Page_NO;
			}
			else if("Last".equals(a_str_PageAction))
			{
				iNowPageNO = Integer.MAX_VALUE;
			}
			else
			{
				iNowPageNO = a_i_Page_NO;
			}
						
			int i_StartItemNO, i_EndItemNO;
													
			//若指定顯示最後一頁時
			if (iNowPageNO == Integer.MAX_VALUE)
			{
				//指定分頁顯示之最末筆
				i_EndItemNO = i_ResultSetRowCount;
				
				//當前頁次
				iNowPageNO = i_TotalPageNum;
				 
				//指定分頁顯示之最首筆
				if ((i_EndItemNO % a_i_ItemNumsOfPage) == 0)
					i_StartItemNO = i_EndItemNO - a_i_ItemNumsOfPage + 1;
				else	
					i_StartItemNO = ((i_EndItemNO / a_i_ItemNumsOfPage) * a_i_ItemNumsOfPage) + 1;
			}
			else
			{
				//指定分頁顯示之最末筆				
				if (i_TotalPageNum <= 1)
					i_EndItemNO = i_ResultSetRowCount;
				else
				{
					i_EndItemNO = iNowPageNO * a_i_ItemNumsOfPage;					

					if (i_EndItemNO > i_ResultSetRowCount)
						i_EndItemNO = i_ResultSetRowCount;					
				}
								
				//指定分頁顯示之最首筆
				if ((i_EndItemNO % a_i_ItemNumsOfPage) == 0)
					i_StartItemNO = i_EndItemNO - a_i_ItemNumsOfPage + 1;
				else	
					i_StartItemNO = ((i_EndItemNO / a_i_ItemNumsOfPage) * a_i_ItemNumsOfPage) + 1;
			}
							
			//移動 cursor 至指定的起始點前一筆
			if (i_StartItemNO == 1)
			{
				rst_result.first();	
			}
			else
			{
				rst_result.first();	
				rst_result.relative(i_StartItemNO - 1);
			}
						
			for (int i = i_StartItemNO ; i <= i_EndItemNO ; i++)
			{
				setVectorFromResultSet(rst_result,vtr_allRows,rmd_result,i_ColumnCount);
												
				rst_result.next();												
			} // end for
			
			TopLinkedHashMap lhm_PageInfo = new TopLinkedHashMap();
			
			//記載頁面動作之後的當前頁數,當前頁面顯示的筆數,总页数
			lhm_PageInfo.put("CurrentPageNO",String.valueOf(iNowPageNO)); 
			lhm_PageInfo.put("ItemNumsPerPage",String.valueOf(a_i_ItemNumsOfPage));			
			lhm_PageInfo.put("TotalPageNum",String.valueOf(i_TotalPageNum));
			lhm_PageInfo.put("sqlstatement",a_str_Query);			
			
			//将页面的基本信息放在vector的第一个index上
			vtr_allRows.add(0, lhm_PageInfo);            
		}
		catch (SQLException exc) 
		{            
			int i_exc = exc.getErrorCode();
			switch (i_exc) 
			{
				case 1205 :
					throw new SQLDeadlockException(exc.getMessage());
					
				default:
					throw exc;					
			} //end switch
		}                		
		catch (NullPointerException exc)
		{
			log.error("",exc);
			throw new SQLUnConnectException("未取得資料庫連結，發生錯誤!!!請通知系統人員");
		}
		catch(Exception exc) 
		{
			log.error("",exc);
			throw new TopException("00001", exc.getMessage());
		}
		finally 
		{
			// close all allocated resources, 好讓他人使用。
			try
			{
				if (rst_result != null)
				{
					rst_result.close();
				}
				if (stmt != null)
				{
					stmt.close();
				}
				DBUtility.closeDBConnection(p_con_Database);
			}
			catch (Exception exc)
			{
				log.error("",exc);
				throw new TopException("00001", exc.getMessage());
			}
		}

		return vtr_allRows;  
	}     
	
	private static void setVectorFromResultSet(ResultSet rst_result,Vector vtrReturn,ResultSetMetaData rmd_result,int i_ColumnCount)throws Exception
	{
		String str_ColumnName = "";
		String str_ColumnValue = "";
		TopLinkedHashMap lhm_thisRow = new TopLinkedHashMap();
		try
		{	            	
			// 得到目前這筆資料的每個 ColumnName and ColumnValue
			for (int j=1; j<=i_ColumnCount; ++j)
			{
				str_ColumnName  = rmd_result.getColumnName(j);
				str_ColumnValue = rst_result.getString(str_ColumnName);
						
				//值為 Null, 依據欄位資料型態給預設值
				if (str_ColumnValue == null)
				{
					//為文字欄位,預設值為空白
					if ( rmd_result.getColumnType(j) == Types.VARCHAR
						|| rmd_result.getColumnType(j) == Types.CHAR
						|| rmd_result.getColumnType(j) == Types.LONGVARCHAR)
					{
						str_ColumnValue = "";
					}
					else if (rmd_result.getColumnType(j) == Types.INTEGER)
					{
						str_ColumnValue = Integer.toString(rst_result.getInt(str_ColumnName));
					}	
					//為數字欄位,預設值為 0
					else
					{
						str_ColumnValue = "0";
					}	
				}
				else
				{
					if ((str_ColumnValue.trim()).equals(""))
						str_ColumnValue = "";
				}
		            	
				// TopLinkedHashMap 中的每個 entry 是 (ColumnName, ColumnValue)
				lhm_thisRow.put(str_ColumnName, str_ColumnValue);
				
			} // end for
		            
			// 將此筆資料放入要回傳的資料中
			vtrReturn.add(lhm_thisRow);
		}
		catch(Exception exc)
		{
			throw exc;
		}
		finally
		{
			lhm_thisRow = null;
		}
	}


    /* 依傳入的 SQL Statement 異動資料,並回傳 異動筆數   */
    public int executeUpdate(String a_str_SQL) throws Exception {
        Statement stmt=null;
        int i_affectCount;
        Connection p_con_Database = null;

        try {
            
//            if (p_con_Database == null) 
//            {
//            	log.debug("TableMaintainDAO.executeUpdate() call DBUtility.getDBConnection()");
//                p_con_Database = DBUtility.getDBConnection();
//            }
            
            log.debug("***updateStr= "+ a_str_SQL);
            
            //Type= Scroll Insensitive(可 Scroll , 不顯示已被更改的資料
            //Cocurrency = Concur read only(不可修改資料)
			p_con_Database = DBUtility.getDBConnection();
            stmt = p_con_Database.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            i_affectCount = stmt.executeUpdate(a_str_SQL);
			
//			log.debug("***Done : "+ a_str_SQL);
            
        }
		catch (SQLException exc) 
		{            
			int i_exc = exc.getErrorCode();
			switch (i_exc) 
			{
				case 1205 :
					throw new SQLDeadlockException(exc.getMessage());
					
				default:
					throw exc;					
			} //end switch
		}                        
        catch (NullPointerException exc)
        {
        	log.error("",exc);
            throw new SQLUnConnectException("未取得資料庫連結，發生錯誤!!!請通知系統人員");
        }
        catch(Exception exc) 
        {          
        	log.error("",exc);
            throw new TopException("00001", exc.getMessage() + " => " + a_str_SQL);
        }
        finally {          
            if (stmt != null) {
                stmt.close();
            }
            DBUtility.closeDBConnection(p_con_Database);

        }
          
        return i_affectCount;  
    } 

    
    public TopLinkedHashMap findByPrimaryKey(String a_str_TableName, TopLinkedHashMap a_htl_RecordKey, TopLinkedHashMap a_htl_FieldInfo) throws SQLTableSchemaException, SQLDataNotExistException, SQLUnConnectException, SQLException {
        a_htl_RecordKey = CasttoString(a_htl_RecordKey);
        return isExists(a_str_TableName, a_htl_RecordKey, a_htl_FieldInfo);
    }
    
    /* 依PrimaryKey取得單一筆資料    */
    private TopLinkedHashMap isExists(
    							String           a_str_TableName, 
    							TopLinkedHashMap a_lhm_RecordKey,
    							TopLinkedHashMap a_lhm_FieldInfo
    								) throws SQLTableSchemaException, SQLDataNotExistException, SQLException, SQLUnConnectException
    {
        TopLinkedHashMap lhm_FieldInfo;
        Vector vtr_PrimaryKeys = null;
        Statement stmt = null;
        ResultSet rst_result = null;
        Connection p_con_Database = null;
        //ResultSetMetaData rsmd = null;
        try {
            /* 取得 Table的欄位相關資訊 包括欄位名稱及欄位型態 */
            lhm_FieldInfo = a_lhm_FieldInfo;
            /*            
                        Properties env=new Properties();
                        env.put("java.naming.factory.initial","com.sun.jndi.cosnaming.CNCtxFactory");
                        env.put("java.naming.provider.url","iiop://127.0.0.1:9010");
                        Context myc = new InitialContext(env);
                        Object objref = myc.lookup("ejb/TableSchema");            
                        ITableSchemaHome home = (ITableSchemaHome)PortableRemoteObject.narrow(objref,ITableSchemaHome.class);            
                        ITableSchema ts = home.create();
                        htl_FieldInfo=ts.getFieldInfo(a_str_TableName);
            */
            vtr_PrimaryKeys = new Vector((Collection) a_lhm_RecordKey.keySet());


        }
        catch (Exception exc) 
        {
        	log.error("",exc);
            throw new SQLException(exc.getMessage());

        }

        String str_and = "";
        StringBuffer str_partialStr = new StringBuffer(""); //組成 "CtryCode='001' and PK='???'....."
        TopLinkedHashMap htl_rtnData = new TopLinkedHashMap();
        String str_columnName = "";

		Iterator itr_keySet = a_lhm_RecordKey.keySet().iterator();
		while (itr_keySet.hasNext())
		{
			str_columnName = (String) itr_keySet.next();
            str_partialStr.append(str_and);
            if (((Integer) lhm_FieldInfo.get(str_columnName)).intValue() == Types.VARCHAR || ((Integer) lhm_FieldInfo.get(str_columnName)).intValue() == Types.CHAR || ((Integer) lhm_FieldInfo.get(str_columnName)).intValue() == Types.LONGVARCHAR)
                str_partialStr = str_partialStr.append(str_columnName).append("='").append((String) a_lhm_RecordKey.get(str_columnName)).append("'");
            else
                str_partialStr = str_partialStr.append(str_columnName).append("=").append((String) a_lhm_RecordKey.get(str_columnName));
            str_and = " and ";
		}

        String str_queryStr = "SELECT * FROM " + a_str_TableName + " NOHOLDLOCK " + " WHERE " + str_partialStr;
        try {
            /*20030305*/
            //str_queryStr=new String(str_queryStr.getBytes("MS950"),"ISO8859_1");
        } catch (Exception exc) {
        	log.error("",exc);
        }

        log.debug("queryString is: " + str_queryStr);

        try {
//            if (p_con_Database == null) 
//            {
//            	log.debug("TableMaintainDAO.isExists() call DBUtility.getDBConnection()");
//                p_con_Database = DBUtility.getDBConnection();
//            }
			p_con_Database = DBUtility.getDBConnection();
            stmt = p_con_Database.createStatement();
            Iterator itr_keys = lhm_FieldInfo.keySet().iterator();
            rst_result = stmt.executeQuery(str_queryStr);
            
//			log.debug("***Done : "+ str_queryStr);
			            
            //rsmd = rst_result.getMetaData();
            Object obj_key = null;
            if (rst_result.next()) {
                while (itr_keys.hasNext()) {
                    obj_key = itr_keys.next();
                    if (!(rst_result.getString((String) obj_key) == null)) {

                        //                        htl_rtnData.put(obj_key,(new String(rst_result.getString((String)obj_key).getBytes("ISO8859_1"),"MS950")).trim());                    
                        /*20030305*/
                        //htl_rtnData.put(obj_key,(new String(rst_result.getString((String)obj_key).getBytes("ISO8859_1"),"ISO8859_1")).trim());                    
                        htl_rtnData.put(obj_key, (new String(rst_result.getString((String) obj_key))).trim());
                    } else if (((Integer) lhm_FieldInfo.get(obj_key)).intValue() == Types.CHAR || ((Integer) lhm_FieldInfo.get(obj_key)).intValue() == Types.LONGVARCHAR || ((Integer) lhm_FieldInfo.get(obj_key)).intValue() == Types.VARCHAR)
                        htl_rtnData.put(obj_key, "");
                    else
                        htl_rtnData.put(obj_key, "0");
                } //end while                       
            } else
                throw new SQLDataNotExistException();
        } catch (NullPointerException exc) {
        	log.error("",exc);
                //Log4j for logging  -->  日後會加上,目前暫無
                throw new SQLException("發生錯誤!!!請通知系統人員", "DAO");
                //throw new SQLUnConnectException("未取得資料庫連結，發生錯誤!!!請通知系統人員");
//         }catch (SQLUnConnectException exc) {
//        	log.error("",exc);
//             throw exc;
        } catch (SQLDataNotExistException exc) {
        	// log.error("",exc);
            throw exc;
        } catch (Exception exc) {
        	log.error("",exc);
        } finally {
            if (rst_result != null) {
                rst_result.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            DBUtility.closeDBConnection(p_con_Database);
        } //end--finally{ 
        return htl_rtnData;
    } // end isExists()    
    
    
    /**
     *  Add by Steven, 2003/11/12
     *  依傳入的 partial key 檢查 table 內該類的資料是否存在 
     * @return boolean
     */
    public boolean isPartialKeyExist(String a_str_TableName, String[][] a_str_RecordKey, TopLinkedHashMap a_htl_FieldInfo,
                                                             Vector  a_vtr_PrimaryKeys) throws SQLException
    {
    	boolean b_DataExist = false;
    	
        try
        {
            this.findByPartialKey(a_str_TableName, new Vector(), a_str_RecordKey, new TopLinkedHashMap());
        	b_DataExist = true;
        }
        catch(SQLDataNotExistException exc)
        {
        	b_DataExist = false;
        }		
        catch(SQLTableSchemaException exc)
        {
        	log.error("",exc);
        	throw new SQLException("Exception from TableMaintainDAO.isPartialKeyExist() : "+ exc.getMessage());
        }
        catch(Exception exc)
        {
        	log.error("",exc);
        	throw new SQLException("Exception from TableMaintainDAO.isPartialKeyExist() : "+ exc.getMessage());
        }		
    	
    	return b_DataExist;
    }
    
    	 
}
