package orginal.topibs.utility;

import java.math.BigDecimal;

import fstop.model.MVHImpl;
import lombok.extern.slf4j.Slf4j;
import topibs.utility.NumericUtil;

@Slf4j
public class MT103 {
	
	private StringBuffer stb_Msg = new StringBuffer();
	private MVHImpl f001Result = null;
	
	public void setF001Result(MVHImpl f001Result)
	{
		this.f001Result = f001Result;		
	}
	
    // 產生MT103的報文資料
    public String createMsg()
    {
        try
        {    
        	stb_Msg.append("{1:F01");
        	stb_Msg.append(f001Result.getValueByFieldName("M1SWIFT").substring(0, 8) + "B");
        	stb_Msg.append(f001Result.getValueByFieldName("M1SWIFT").substring(8));        	
        	stb_Msg.append("0000000000}");
        	log.debug("SWTKIND=="+f001Result.getValueByFieldName("SWTKIND"));
       		stb_Msg.append("{2:I103");
        	stb_Msg.append(f001Result.getValueByFieldName("M1SBBIC").substring(0, 8) + "X");
        	stb_Msg.append(f001Result.getValueByFieldName("M1SBBIC").substring(8));         	
        	stb_Msg.append("N}");
        	if(f001Result.getValueByFieldName("SWTKIND").equals("R")) {
        		stb_Msg.append("{3:{103:TWP}{108:");    //20130227 Sox：如果SWTKIND：R 代表走RTGS，103表頭要加TWP        	  
        	}
        	else	
        		stb_Msg.append("{3:{108:");
        	
            // TIA_TLID 需依照規則另外做判定, 如果 M1SWIFT 內容最後三碼為"XXX", 則填入"REM"
            // 若 M1SWIFT 內容最後三碼為"893", 則填入承做行(TXBRH)內容, 又, 如果承做行內容為"041", 則填入"REM"
            // 其餘狀況一律取 M1SWIFT 之最後三碼填入        	
        	String str_TLID = f001Result.getValueByFieldName("M1SWIFT").substring(8);  //發電者之分行代碼
    		if (str_TLID.equals("XXX"))
    			stb_Msg.append("REM"); 			
    		else if (str_TLID.equals("893")) {    			
    		    if ((f001Result.getValueByFieldName("TXBRH")).equals("041")) {
        			stb_Msg.append("REM");
    		    }
    		    else {
        			stb_Msg.append(f001Result.getValueByFieldName("TXBRH")); 
    		    }    			    			
    		}
    		else
    			stb_Msg.append(f001Result.getValueByFieldName("M1SWIFT").substring(8));     			
			
			//20201013 SWIFT GPI ADD	
			stb_Msg.append("}{111:");
			stb_Msg.append("001");	
    		
    		stb_Msg.append("}}");
    		
        	stb_Msg.append("{4:");
        	stb_Msg.append("\r\n");
        	
        	if (! "".equals(f001Result.getValueByFieldName("M1REFNO")))
        	{	
        		stb_Msg.append(":20:");
        		stb_Msg.append(f001Result.getValueByFieldName("M1REFNO"));
        		stb_Msg.append("\r\n");
        	}

        	if (! "".equals(f001Result.getValueByFieldName("M1OPCOD")))
        	{	
        		stb_Msg.append(":23B:");
        		stb_Msg.append(f001Result.getValueByFieldName("M1OPCOD"));
        		stb_Msg.append("\r\n");
        	}

        	if (! "".equals(f001Result.getValueByFieldName("M123TEL")))
        	{	
        		stb_Msg.append(":23E:");
        		stb_Msg.append(f001Result.getValueByFieldName("M123TEL"));
        		stb_Msg.append("\r\n");
        	}        	
        	
        	if (! "".equals(f001Result.getValueByFieldName("M132DAT")))
        	{	
        		stb_Msg.append(":32A:");
        		stb_Msg.append(f001Result.getValueByFieldName("M132DAT"));
        		stb_Msg.append(f001Result.getValueByFieldName("M132CCY"));
        		
        		String str = NumericUtil.formatNumberString(f001Result.getValueByFieldName("M132AMT"),2);	
        		str = str.replaceAll(",", "");
        		str = str.replaceAll("\\.", ",");
        		
        		//當幣別為TWD,JPY時,Tag32之金額欄位不可包含小數位
        		if ("JPY".equals(f001Result.getValueByFieldName("M132CCY")) || "TWD".equals(f001Result.getValueByFieldName("M132CCY")))
        		{
        			str = str.substring(0, str.indexOf(",")+1);        			
        		}
        		
        		stb_Msg.append(str);        		     		
        		stb_Msg.append("\r\n");
        	}
        	if ("BEN".equals(f001Result.getValueByFieldName("M171CHG")) || "SHA".equals(f001Result.getValueByFieldName("M171CHG")))
        	{	
        		stb_Msg.append(":33B:");
        		stb_Msg.append(f001Result.getValueByFieldName("M132CCY"));
        		System.out.println("MT103.java 33B:"+f001Result.getValueByFieldName("M132CCY")+" "+f001Result.getValueByFieldName("M133AMT"));
        		String str = "BEN".equals(f001Result.getValueByFieldName("M171CHG")) ? NumericUtil.formatNumberString(f001Result.getValueByFieldName("M133AMT"),2) : NumericUtil.formatNumberString(f001Result.getValueByFieldName("M132AMT"),2);	
         		str = str.replaceAll(",", "");
        		str = str.replaceAll("\\.", ",");
        		
        		//當幣別為TWD,JPY時,Tag33之金額欄位不可包含小數位
        		if ("JPY".equals(f001Result.getValueByFieldName("M132CCY")) || "TWD".equals(f001Result.getValueByFieldName("M132CCY")))
        		{
        			str = str.substring(0, str.indexOf(",")+1);        			
        		}
        		
        		stb_Msg.append(str);        		     		
        		stb_Msg.append("\r\n");
        	}        	
        	if (! "".equals(f001Result.getValueByFieldName("M150OPT")))
        	{	
        		if ("A".equals(f001Result.getValueByFieldName("M150OPT")) &&
        			(f001Result.getValueByFieldName("M150BIC") != null) &&
        			(! "".equals(f001Result.getValueByFieldName("M150BIC").trim())) )
        		{
            		stb_Msg.append(":50");
            		stb_Msg.append((" ".equals(f001Result.getValueByFieldName("M150OPT"))) ? "" : f001Result.getValueByFieldName("M150OPT"));        		
            		stb_Msg.append(":");        		
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M150ACC")));
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M150BIC")));        			
        		}
        		else if ( "K".equals(f001Result.getValueByFieldName("M150OPT")) &&
            			 (((f001Result.getValueByFieldName("M150AD1") != null) && (! "".equals(f001Result.getValueByFieldName("M150AD1").trim())))  || 
            			  ((f001Result.getValueByFieldName("M150AD2") != null) && (! "".equals(f001Result.getValueByFieldName("M150AD2").trim())))  ||
            			  ((f001Result.getValueByFieldName("M150AD3") != null) && (! "".equals(f001Result.getValueByFieldName("M150AD3").trim())))  ||
            			  ((f001Result.getValueByFieldName("M150AD4") != null) && (! "".equals(f001Result.getValueByFieldName("M150AD4").trim()))))  )
        		{
            		stb_Msg.append(":50");
            		stb_Msg.append((" ".equals(f001Result.getValueByFieldName("M150OPT"))) ? "" : f001Result.getValueByFieldName("M150OPT"));        		
            		stb_Msg.append(":");        		
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M150ACC")));       			
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M150AD1")));
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M150AD2")));      
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M150AD3"))); 
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M150AD4")));           			
        		}        			      	
        	}
        	
        	if (! "".equals(f001Result.getValueByFieldName("M153OPT")))
        	{	
        		if ("A".equals(f001Result.getValueByFieldName("M153OPT")) &&
            			(f001Result.getValueByFieldName("M153BIC") != null) &&
            			(! "".equals(f001Result.getValueByFieldName("M153BIC").trim())) )
            	{            		
            		stb_Msg.append(":53");
            		stb_Msg.append((" ".equals(f001Result.getValueByFieldName("M153OPT"))) ? "" : f001Result.getValueByFieldName("M153OPT"));            		
            		stb_Msg.append(":");        		
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M153ACC")));
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M153BIC")));     			
            	}
            	else if ( "D".equals(f001Result.getValueByFieldName("M153OPT")) &&
                		 (((f001Result.getValueByFieldName("M153AD1") != null) && (! "".equals(f001Result.getValueByFieldName("M153AD1").trim())))  || 
                		  ((f001Result.getValueByFieldName("M153AD2") != null) && (! "".equals(f001Result.getValueByFieldName("M153AD2").trim())))  ||
                		  ((f001Result.getValueByFieldName("M153AD3") != null) && (! "".equals(f001Result.getValueByFieldName("M153AD3").trim())))  ||
                		  ((f001Result.getValueByFieldName("M153AD4") != null) && (! "".equals(f001Result.getValueByFieldName("M153AD4").trim()))))  )
            	{
            		stb_Msg.append(":53");
            		stb_Msg.append((" ".equals(f001Result.getValueByFieldName("M153OPT"))) ? "" : f001Result.getValueByFieldName("M153OPT"));            		
            		stb_Msg.append(":");        		
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M153ACC")));            		
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M153AD1")));
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M153AD2")));      
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M153AD3"))); 
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M153AD4")));          			
            	}          		
        	}
        	
        	if (! "".equals(f001Result.getValueByFieldName("M154OPT")))
        	{	
        		if ("A".equals(f001Result.getValueByFieldName("M154OPT")) &&
            			(f001Result.getValueByFieldName("M154BIC") != null) &&
            			(! "".equals(f001Result.getValueByFieldName("M154BIC").trim())) )
            	{
            		stb_Msg.append(":54");
            		stb_Msg.append((" ".equals(f001Result.getValueByFieldName("M154OPT"))) ? "" : f001Result.getValueByFieldName("M154OPT"));            		
            		stb_Msg.append(":");        		
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M154ACC")));
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M154BIC")));     			
            	}
            	else if ( "D".equals(f001Result.getValueByFieldName("M154OPT")) &&
                		 (((f001Result.getValueByFieldName("M154AD1") != null) && (! "".equals(f001Result.getValueByFieldName("M154AD1").trim())))  || 
                		  ((f001Result.getValueByFieldName("M154AD2") != null) && (! "".equals(f001Result.getValueByFieldName("M154AD2").trim())))  ||
                		  ((f001Result.getValueByFieldName("M154AD3") != null) && (! "".equals(f001Result.getValueByFieldName("M154AD3").trim())))  ||
                		  ((f001Result.getValueByFieldName("M154AD4") != null) && (! "".equals(f001Result.getValueByFieldName("M154AD4").trim()))))  )
            	{
            		stb_Msg.append(":54");
            		stb_Msg.append((" ".equals(f001Result.getValueByFieldName("M154OPT"))) ? "" : f001Result.getValueByFieldName("M154OPT"));            		
            		stb_Msg.append(":");        		
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M154ACC")));            		
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M154AD1")));
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M154AD2")));      
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M154AD3"))); 
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M154AD4")));               			
            	}    		
        	}

        	if (! "".equals(f001Result.getValueByFieldName("M155OPT")))
        	{	
        		if ("A".equals(f001Result.getValueByFieldName("M155OPT")) &&
            			(f001Result.getValueByFieldName("M155BIC") != null) &&
            			(! "".equals(f001Result.getValueByFieldName("M155BIC").trim())) )
            	{
            		stb_Msg.append(":55");
            		stb_Msg.append((" ".equals(f001Result.getValueByFieldName("M155OPT"))) ? "" : f001Result.getValueByFieldName("M155OPT"));            		
            		stb_Msg.append(":");        		
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M155ACC")));
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M155BIC")));     			
            	}
            	else if ( "D".equals(f001Result.getValueByFieldName("M155OPT")) &&
                		 (((f001Result.getValueByFieldName("M155AD1") != null) && (! "".equals(f001Result.getValueByFieldName("M155AD1").trim())))  || 
                		  ((f001Result.getValueByFieldName("M155AD2") != null) && (! "".equals(f001Result.getValueByFieldName("M155AD2").trim())))  ||
                		  ((f001Result.getValueByFieldName("M155AD3") != null) && (! "".equals(f001Result.getValueByFieldName("M155AD3").trim())))  ||
                		  ((f001Result.getValueByFieldName("M155AD4") != null) && (! "".equals(f001Result.getValueByFieldName("M155AD4").trim()))))  )
            	{
            		stb_Msg.append(":55");
            		stb_Msg.append((" ".equals(f001Result.getValueByFieldName("M155OPT"))) ? "" : f001Result.getValueByFieldName("M155OPT"));            		
            		stb_Msg.append(":");        		
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M155ACC")));            		
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M155AD1")));
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M155AD2")));      
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M155AD3"))); 
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M155AD4")));               			
            	}    		
        	}
        	
        	if (! "".equals(f001Result.getValueByFieldName("M156OPT")))
        	{	
        		if ("A".equals(f001Result.getValueByFieldName("M156OPT")) &&
            			(f001Result.getValueByFieldName("M156BIC") != null) &&
            			(! "".equals(f001Result.getValueByFieldName("M156BIC").trim())) )
            	{
            		stb_Msg.append(":56");
            		stb_Msg.append((" ".equals(f001Result.getValueByFieldName("M156OPT"))) ? "" : f001Result.getValueByFieldName("M156OPT"));            		
            		stb_Msg.append(":");        		
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M156ACC")));
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M156BIC")));     			        			
            	}
            	else if ( "D".equals(f001Result.getValueByFieldName("M156OPT")) &&
                		 (((f001Result.getValueByFieldName("M156AD1") != null) && (! "".equals(f001Result.getValueByFieldName("M156AD1").trim())))  || 
                		  ((f001Result.getValueByFieldName("M156AD2") != null) && (! "".equals(f001Result.getValueByFieldName("M156AD2").trim())))  ||
                		  ((f001Result.getValueByFieldName("M156AD3") != null) && (! "".equals(f001Result.getValueByFieldName("M156AD3").trim())))  ||
                		  ((f001Result.getValueByFieldName("M156AD4") != null) && (! "".equals(f001Result.getValueByFieldName("M156AD4").trim()))))  )
            	{
            		stb_Msg.append(":56");
            		stb_Msg.append((" ".equals(f001Result.getValueByFieldName("M156OPT"))) ? "" : f001Result.getValueByFieldName("M156OPT"));            		
            		stb_Msg.append(":");        		
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M156ACC")));            		
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M156AD1")));
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M156AD2")));      
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M156AD3"))); 
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M156AD4")));               			
            	}            		        		
        	}
        	
        	if (! "".equals(f001Result.getValueByFieldName("M157OPT")))
        	{	
        		if ("A".equals(f001Result.getValueByFieldName("M157OPT")) &&
            			(f001Result.getValueByFieldName("M157BIC") != null) &&
            			(! "".equals(f001Result.getValueByFieldName("M157BIC").trim())) )
            	{
            		stb_Msg.append(":57");
            		stb_Msg.append((" ".equals(f001Result.getValueByFieldName("M157OPT"))) ? "" : f001Result.getValueByFieldName("M157OPT"));            		
            		stb_Msg.append(":");        		
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M157ACC")));
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M157BIC")));     			
            	}
            	else if ( "D".equals(f001Result.getValueByFieldName("M157OPT")) &&
                		 (((f001Result.getValueByFieldName("M157AD1") != null) && (! "".equals(f001Result.getValueByFieldName("M157AD1").trim())))  || 
                		  ((f001Result.getValueByFieldName("M157AD2") != null) && (! "".equals(f001Result.getValueByFieldName("M157AD2").trim())))  ||
                		  ((f001Result.getValueByFieldName("M157AD3") != null) && (! "".equals(f001Result.getValueByFieldName("M157AD3").trim())))  ||
                		  ((f001Result.getValueByFieldName("M157AD4") != null) && (! "".equals(f001Result.getValueByFieldName("M157AD4").trim()))))  )
            	{
            		stb_Msg.append(":57");
            		stb_Msg.append((" ".equals(f001Result.getValueByFieldName("M157OPT"))) ? "" : f001Result.getValueByFieldName("M157OPT"));            		
            		stb_Msg.append(":");        		
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M157ACC")));            		
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M157AD1")));
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M157AD2")));      
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M157AD3"))); 
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M157AD4")));      			
            	}            		
        	}
        	
        	if (! "".equals(f001Result.getValueByFieldName("M159ACC")))
        	{	
        		if ("A".equals(f001Result.getValueByFieldName("M159OPT")) &&
            			(f001Result.getValueByFieldName("M159BIC") != null) &&
            			(! "".equals(f001Result.getValueByFieldName("M159BIC").trim())) )
            	{
            		stb_Msg.append(":59");
            		stb_Msg.append((" ".equals(f001Result.getValueByFieldName("M159OPT"))) ? "" : f001Result.getValueByFieldName("M159OPT"));            		
            		stb_Msg.append(":");        		
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M159ACC")));
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M159BIC")));     			
            	}
            	else if ( "".equals(f001Result.getValueByFieldName("M159OPT").trim()) &&
                		 (((f001Result.getValueByFieldName("M159AD1") != null) && (! "".equals(f001Result.getValueByFieldName("M159AD1").trim())))  || 
                		  ((f001Result.getValueByFieldName("M159AD2") != null) && (! "".equals(f001Result.getValueByFieldName("M159AD2").trim())))  ||
                		  ((f001Result.getValueByFieldName("M159AD3") != null) && (! "".equals(f001Result.getValueByFieldName("M159AD3").trim())))  ||
                		  ((f001Result.getValueByFieldName("M159AD4") != null) && (! "".equals(f001Result.getValueByFieldName("M159AD4").trim()))))  )
            	{
            		stb_Msg.append(":59");
            		stb_Msg.append((" ".equals(f001Result.getValueByFieldName("M159OPT"))) ? "" : f001Result.getValueByFieldName("M159OPT"));            		
            		stb_Msg.append(":");        		
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M159ACC")));
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M159AD1")));
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M159AD2")));      
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M159AD3"))); 
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M159AD4")));        			
            	}            		
        	}

        	if (!("".equals(f001Result.getValueByFieldName("M170NA1"))) && !("".equals(f001Result.getValueByFieldName("M170NA2"))))
        	{	
        		stb_Msg.append(":70:");
        		stb_Msg.append(f001Result.getValueByFieldName("M170NA1"));
        		stb_Msg.append("\r\n");
        		stb_Msg.append(f001Result.getValueByFieldName("M170NA2"));
        		stb_Msg.append("\r\n");
        	}
        	if (!("".equals(f001Result.getValueByFieldName("M170NA1"))) && ("".equals(f001Result.getValueByFieldName("M170NA2"))))
        	{	
        		stb_Msg.append(":70:");
        		stb_Msg.append(f001Result.getValueByFieldName("M170NA1"));
        		stb_Msg.append("\r\n");
        	}        	
        	if (!("".equals(f001Result.getValueByFieldName("M170NA2"))) && ("".equals(f001Result.getValueByFieldName("M170NA1"))))
        	{	
        		stb_Msg.append(":70:");
        		stb_Msg.append(f001Result.getValueByFieldName("M170NA2"));
        		stb_Msg.append("\r\n");
        	}

        	if (! "".equals(f001Result.getValueByFieldName("M170NA3")))
        	{	
        		stb_Msg.append(f001Result.getValueByFieldName("M170NA3"));
        		stb_Msg.append("\r\n");
        	}

        	if (! "".equals(f001Result.getValueByFieldName("M170NA4")))
        	{	        		
        		stb_Msg.append(f001Result.getValueByFieldName("M170NA4"));
        		stb_Msg.append("\r\n");
        	}

        	if (! "".equals(f001Result.getValueByFieldName("M171CHG")))
        	{	
        		stb_Msg.append(":71A:");
        		stb_Msg.append(f001Result.getValueByFieldName("M171CHG"));
        		stb_Msg.append("\r\n");
        	}
        	if ("BEN".equals(f001Result.getValueByFieldName("M171CHG")))
        	{	
        		stb_Msg.append(":71F:");
        		stb_Msg.append(f001Result.getValueByFieldName("M132CCY"));
        		String o33B = NumericUtil.formatNumberString(f001Result.getValueByFieldName("M133AMT"),2);
        		o33B = o33B.replaceAll(",", "");
        		//o33B = o33B.replaceAll("\\.", ","); 
        		log.debug("MT103.java o33B:"+o33B);
        		String o32A = NumericUtil.formatNumberString(f001Result.getValueByFieldName("M132AMT"),2);	
        		o32A = o32A.replaceAll(",", "");
        		//o32A = o32A.replaceAll("\\.", ",");
        		log.debug("MT103.java o32A:"+o32A);
        		double diff71f = Double.parseDouble(o33B) - Double.parseDouble(o32A);     //71F(手續費金額)=33B(客戶匯出金額:轉入金額)-32A(實際匯出金額)
        		//DecimalFormat df = new DecimalFormat("0.00");
        		BigDecimal bd= new BigDecimal(diff71f);
        		bd=bd.setScale(2, BigDecimal.ROUND_HALF_UP);// 小數後面四位, 四捨五入
        		diff71f=bd.doubleValue();
        		String str = String.valueOf(diff71f);
        		log.debug("MT103.java str before format:"+str);
        		String str1 = str.substring(0,str.indexOf("."));
        		log.debug("MT103.java str1:"+str1);
        		String str2 = str.substring(str.indexOf(".")+1,str.length());
        		str2=str2.length()>2 ? str2.substring(0,2) : str2;
        		log.debug("MT103.java str2:"+str2);
        		String padding ="";
        		String padding1 = "";
        		for(int i=0;i<13-str1.length();i++)
        		{
        			padding+="0";
        		}
        		for(int i=0;i<2-str2.length();i++)
        		{
        			padding1+=0;
        		}
        		str = padding+str1+str2+padding1;
        		str = NumericUtil.formatNumberString(str,2);
        		str = str.replaceAll(",", "");
        		str = str.replaceAll("\\.", ",");
        		
        		//當幣別為TWD,JPY時,Tag71F之金額欄位不可包含小數位
        		if ("JPY".equals(f001Result.getValueByFieldName("M132CCY")) || "TWD".equals(f001Result.getValueByFieldName("M132CCY")))
        		{
        			str = str.substring(0, str.indexOf(",")+1);        			
        		} 
        		log.debug("MT103.java str after format:"+str);
        		stb_Msg.append(str);        		     		
        		stb_Msg.append("\r\n");        		
			}
			else if("SHA".equals(f001Result.getValueByFieldName("M171CHG"))){
        		stb_Msg.append(":71F:");
        		stb_Msg.append(f001Result.getValueByFieldName("M132CCY"));
        		stb_Msg.append("0,");        		     		
        		stb_Msg.append("\r\n");        		
        	}
        	
        	if (! "".equals(f001Result.getValueByFieldName("M172NA1")))
        	{	
        		stb_Msg.append(":72:");
        		stb_Msg.append(f001Result.getValueByFieldName("M172NA1"));
        		stb_Msg.append("\r\n");
        	}
        	
        	if (! "".equals(f001Result.getValueByFieldName("M172NA2")))
        	{	        	
        		stb_Msg.append(f001Result.getValueByFieldName("M172NA2"));
        		stb_Msg.append("\r\n");
        	}
        	
        	if (! "".equals(f001Result.getValueByFieldName("M172NA3")))
        	{	
        		stb_Msg.append(f001Result.getValueByFieldName("M172NA3"));
        		stb_Msg.append("\r\n");
        	}
        	
        	if (! "".equals(f001Result.getValueByFieldName("M172NA4")))
        	{	        		
        		stb_Msg.append(f001Result.getValueByFieldName("M172NA4"));
        		stb_Msg.append("\r\n");
        	}
        	
        	if (! "".equals(f001Result.getValueByFieldName("M172NA5")))
        	{	        		
        		stb_Msg.append(f001Result.getValueByFieldName("M172NA5"));
        		stb_Msg.append("\r\n");
        	}
        	
        	if (! "".equals(f001Result.getValueByFieldName("M172NA6")))
        	{	        		
        		stb_Msg.append(f001Result.getValueByFieldName("M172NA6"));
        		stb_Msg.append("\r\n");
        	}
			
			if (! "".equals(f001Result.getValueByFieldName("M177NA")))
        	{	
        		stb_Msg.append(":77B:");
        		stb_Msg.append(f001Result.getValueByFieldName("M177NA"));
        		stb_Msg.append("\r\n");
			} 
			
        	stb_Msg.append("-}");        	            
        } 
        catch (Exception exc) 
        {
            log.error("",exc);
        }        
        
        return stb_Msg.toString();
    } //end createMsg()   
	
    
    private String checkEmpty(String tagValue)
    {
    	if ("".equals(tagValue))
    	{
    		return "";
    	}
    	else
    	{
    		return tagValue + "\r\n";
    	}    	
    }
}
