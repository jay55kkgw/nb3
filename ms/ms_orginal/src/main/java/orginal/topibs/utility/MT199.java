package orginal.topibs.utility;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Value;

import fstop.model.MVHImpl;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MT199 {
	
	private StringBuffer stb_Msg = new StringBuffer();
	private MVHImpl f001Result = null;
	
	private String ms_env;
	
	public MT199(String env) {
		this.ms_env = env;
	}

	public void setF001Result(MVHImpl f001Result)
	{
		this.f001Result = f001Result;		
	}
	
    // 產生MT199的報文資料
    public String createMsg()
    {
        try
        {    
        	log.debug("ms_env >> {}", ms_env);
        	String MT199_B2_M1SBBIC="";
    		// String ip = IPUtils.getLocalIp();
			String fxstatus = ""; //匯 款狀態
			
			// 跟NB2.5判斷環境方式不同，NB2.5用ip，NB3改用環境變數
    		// if(ip.equals("172.22.11.30"))
    		// {	
    		// 	fxstatus="MBBTTWT0";
    		// 	MT199_B2_M1SBBIC="TRCKCHZ0XXX";
    		// }
    		// if(ip.equals("10.16.21.16"))
    		// {	
    		// 	fxstatus="MBBTTWTP";
    		// 	MT199_B2_M1SBBIC="TRCKCHZZXXX";
    		// }
    		// if(ip.equals("10.16.21.17"))
    		// {
    		// 	fxstatus="MBBTTWTP";
    		// 	MT199_B2_M1SBBIC="TRCKCHZZXXX";
			// }
			fxstatus = "P".equals(ms_env) ? "MBBTTWTP" : "MBBTTWT0";
			MT199_B2_M1SBBIC = "P".equals(ms_env) ? "TRCKCHZZXXX" : "TRCKCHZ0XXX";
    		
    		String  MT199_B1_M1SWIFT=fxstatus+"XXX";
    		
        	stb_Msg.append("{1:F01");
        	stb_Msg.append(MT199_B1_M1SWIFT.substring(0, 8) + "B");
        	stb_Msg.append(MT199_B1_M1SWIFT.substring(8));        	
        	stb_Msg.append("0000000000}");
        	
        	stb_Msg.append("{2:I199");
        	stb_Msg.append(MT199_B2_M1SBBIC.substring(0, 8) + "X");
        	stb_Msg.append(MT199_B2_M1SBBIC.substring(8));         	
        	stb_Msg.append("N}");
        	
        	stb_Msg.append("{3:{108:");
        	
            // TIA_TLID 需依照規則另外做判定, 如果 M1SWIFT 內容最後三碼為"XXX", 則填入"REM"
            // 若 M1SWIFT 內容最後三碼為"893", 則填入承做行(TXBRH)內容, 又, 如果承做行內容為"041", 則填入"REM"
            // 其餘狀況一律取 M1SWIFT 之最後三碼填入        	
        	String str_TLID = MT199_B1_M1SWIFT.substring(8);  //發電者之分行代碼
    		if (str_TLID.equals("XXX"))
    			stb_Msg.append("REM"); 			
    		else if (str_TLID.equals("893")) {    			
    		    if ((f001Result.getValueByFieldName("TXBRH")).equals("041")) {
        			stb_Msg.append("REM");
    		    }
    		    else {
        			stb_Msg.append(f001Result.getValueByFieldName("TXBRH")); 
    		    }    			    			
    		}
    		else
    			stb_Msg.append(MT199_B1_M1SWIFT.substring(8));     			
    			
    		//20201013 SWIFT GPI ADD	
    		stb_Msg.append("}{111:");
    		stb_Msg.append("001");
    		stb_Msg.append("}{121:");
    		stb_Msg.append(f001Result.getValueByFieldName("MT199_B3_M121NA1"));
    		
    		stb_Msg.append("}}");
    		
        	stb_Msg.append("{4:");
        	stb_Msg.append("\r\n");
        	
        	if (! "".equals(f001Result.getValueByFieldName("M1REFNO")))
        	{	
        		stb_Msg.append(":20:");
        		stb_Msg.append(f001Result.getValueByFieldName("M1REFNO"));
        		stb_Msg.append("\r\n");
        	}

        	if (! "".equals(f001Result.getValueByFieldName("M2REFNO")))
        	{	
        		stb_Msg.append(":21:");
        		stb_Msg.append(f001Result.getValueByFieldName("M2REFNO"));
        		stb_Msg.append("\r\n");
        	}
    		stb_Msg.append(":79:");
    		stb_Msg.append("//"+new SimpleDateFormat("yyMMddHHmm").format(new Date()).toString()+"+0800");
    		stb_Msg.append("\r\n");
    		stb_Msg.append("//ACCC");
    		stb_Msg.append("\r\n");

    		stb_Msg.append("//"+MT199_B1_M1SWIFT);//MBBTTWTP:PRODUCT,MBBTTWT0:TEST
    		stb_Msg.append("\r\n");
        	
//==================================================================
    		//轉入金額檢核
    		String PMTAMT = f001Result.getValueByFieldName("PMTAMT");
    		log.debug("F001 PMTAMT -->"+PMTAMT);
        		if (! "".equals(PMTAMT)) {
    			if(PMTAMT.substring(PMTAMT.length()-2).equals("00")) {
    				PMTAMT = String.valueOf(Integer.parseInt(PMTAMT.substring(0,PMTAMT.length()-2))+ ",");
    			}else {		
    				PMTAMT = String.valueOf(Integer.parseInt(PMTAMT.substring(0,PMTAMT.length()-2))) + "," + PMTAMT.substring(PMTAMT.length()-2);
    				if (f001Result.getValueByFieldName("PMTCCY").compareTo("JPY") == 0) 
    					PMTAMT = PMTAMT.substring(0,PMTAMT.length()-2);
    			}
    		}
        	log.debug("PMTAMT Step1-->"+PMTAMT);
    		
    		
    		//手續費檢核
    		String amt  =f001Result.getValueByFieldName("COMMAMT2");
    		log.debug("amt COMMAMT2 F001 -->"+amt);
    		if (! "".equals(amt)) {
    			if(amt.substring(amt.length()-2).equals("00")) {
    				amt = String.valueOf(Integer.parseInt(amt.substring(0,amt.length()-2))+ ",");
    			}else {		
    				amt = String.valueOf(Integer.parseInt(amt.substring(0,amt.length()-2))) + "," + amt.substring(amt.length()-2);
    				if (f001Result.getValueByFieldName("COMMCCY2").compareTo("JPY") == 0) 
    					amt = amt.substring(0,amt.length()-2);
    			}
    		} 
    		log.debug("amt  COMMAMT2 Step1-->"+amt);
    		log.debug("DETCHG -->"+f001Result.getValueByFieldName("DETCHG")+"<--");
    	    if(f001Result.getValueByFieldName("DETCHG").equals("OUR")){
    	    	
    	    	
    	    	/*
    	    	 * MT199_B4_M79NA4:入帳幣別+入帳金額
                 * MT199手續費檢核規則
                 * 如費用負擔別為OUR:入帳金額不變、不收本行手續費	 
    	    	 *
    	    	 */
    	    	
    	    	
    	    	if(!f001Result.getValueByFieldName("PMTCCY").equals("TWD")){
    	    		stb_Msg.append("//"+f001Result.getValueByFieldName("PMTCCY")+ PMTAMT);
    	    		stb_Msg.append("\r\n");
    	    	}else{
    	    	       		
    	    		/*
    	    		 * ORGCCY           :[JPY]                                                                                                  
                     * ORGAMT           :[000000001000000]
    	    		 * 
    	    		 */
    	    	  		
    	    		String Orgamt = f001Result.getValueByFieldName("ORGAMT");
    	    		
    	    		if (! "".equals(Orgamt)) {
    	    			if(Orgamt.substring(Orgamt.length()-2).equals("00")) {
    	    				Orgamt = String.valueOf(Integer.parseInt(Orgamt.substring(0,Orgamt.length()-2))+ ",");
    	    			}else {		
    	    				Orgamt = String.valueOf(Integer.parseInt(Orgamt.substring(0,Orgamt.length()-2))) + "," + Orgamt.substring(Orgamt.length()-2);
    	    				if (f001Result.getValueByFieldName("ORGCCY").compareTo("JPY") == 0) 
    	    					Orgamt = Orgamt.substring(0,Orgamt.length()-2);
    	    			} 
        	    		stb_Msg.append("//"+f001Result.getValueByFieldName("ORGCCY")+Orgamt);
        	    		stb_Msg.append("\r\n");    	    			    	    			
    	    		}   	    		
    	    	}
    	    	if(! "".equals(f001Result.getValueByFieldName("MT199_B4_M79NA5"))){
     	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("MT199_B4_M79NA5"));
     	    		stb_Msg.append("\r\n"); 
     	    		log.debug("//:71F:"+f001Result.getValueByFieldName("MT199_B4_M79NA5"));
     		    }
    	    	if(! "".equals(f001Result.getValueByFieldName("MT199_B4_M79NA6"))){
     	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("MT199_B4_M79NA6"));
     	    		stb_Msg.append("\r\n");  
     	    		log.debug("//:71F:"+f001Result.getValueByFieldName("MT199_B4_M79NA6"));
    		    }
    	    	if(! "".equals(f001Result.getValueByFieldName("MT199_B4_M79NA7"))){
     	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("MT199_B4_M79NA7"));
     	    		stb_Msg.append("\r\n");
     	    		log.debug("//:71F:"+f001Result.getValueByFieldName("MT199_B4_M79NA7"));
    		    }
    	    	if(! "".equals(f001Result.getValueByFieldName("MT199_B4_M79NA8"))){
     	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("MT199_B4_M79NA8"));
     	    		stb_Msg.append("\r\n");    	    			    	    			
     	    		log.debug("//:71F:"+f001Result.getValueByFieldName("MT199_B4_M79NA8"));
    		    }
    	    	if(! "".equals(f001Result.getValueByFieldName("MT199_B4_M79NA9"))){
     	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("MT199_B4_M79NA9"));
     	    		stb_Msg.append("\r\n");    	    			    	    			
     	    		log.debug("//:71F:"+f001Result.getValueByFieldName("MT199_B4_M79NA9"));
     		    }
    	    	if(! "".equals(f001Result.getValueByFieldName("MT199_B4_M79NA10"))){
     	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("MT199_B4_M79NA10"));
     	    		stb_Msg.append("\r\n");    	    			    	    			
     	    		log.debug("//:71F:"+f001Result.getValueByFieldName("MT199_B4_M79NA10"));
     		    }
    	    	if(! "".equals(f001Result.getValueByFieldName("MT199_B4_M79NA11"))){
     	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("MT199_B4_M79NA11"));
     	    		stb_Msg.append("\r\n");    	    			    	    			
     	    		log.debug("//:71F:"+f001Result.getValueByFieldName("MT199_B4_M79NA11"));
    		    }
    	    	if(! "".equals(f001Result.getValueByFieldName("MT199_B4_M79NA12"))){
     	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("MT199_B4_M79NA12"));
     	    		stb_Msg.append("\r\n");    	    			    	    			
     	    		log.debug("//:71F:"+f001Result.getValueByFieldName("MT199_B4_M79NA12"));
    		    }
    	    	if(! "".equals(f001Result.getValueByFieldName("MT199_B4_M79NA13"))){
     	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("MT199_B4_M79NA13"));
     	    		stb_Msg.append("\r\n");    	    			    	    			
     	    		log.debug("//:71F:"+f001Result.getValueByFieldName("MT199_B4_M79NA13"));
    		    }
    	    	if(! "".equals(f001Result.getValueByFieldName("MT199_B4_M79NA14"))){
     	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("MT199_B4_M79NA14"));
     	    		stb_Msg.append("\r\n");    	    			    	    			
     	    		log.debug("//:71F:"+f001Result.getValueByFieldName("MT199_B4_M79NA14"));
    		    }    	    	
    	    	
    	    }else{
    	    	if(f001Result.getValueByFieldName("PMTCCY").equals("TWD")){ 
     	    		
    	    		
    		    	
    	    		
    	    		/* 
    		    	 * 解款幣別為TWD  
    		         * 入款金額需扣除手續費 並轉成原幣別
    		         * 並多帶本行手續費欄位(twd)
    	    		 */
    		    	
    		    	 if(PMTAMT.substring(PMTAMT.length()-1).equals(",")){
    					 PMTAMT= PMTAMT.replace(',', '.')+"00";
    				 }else {
    					 PMTAMT= PMTAMT.replace(',', '.');
    				 }
    		    	 log.debug("PMTAMT Step2-->"+PMTAMT);	 
    				 String detchg = amt;
    				 
    				       
    				 if(detchg.substring(detchg.length()-1).equals(",")){
    					 detchg = detchg.replace(',', '.')+"00";
    				 }else {
    					 detchg = detchg.replace(',', '.');
    				 }
    				 log.debug("amt detchg Step2-->"+detchg);   
    		    	//判斷手續費為0 不必轉換原幣
    				if(detchg.equals("0.00")){
    					
    					String Orgamt = f001Result.getValueByFieldName("ORGAMT");
    					if (! "".equals(Orgamt)) {
    		    			if(Orgamt.substring(Orgamt.length()-2).equals("00")) {
    		    				Orgamt = String.valueOf(Integer.parseInt(Orgamt.substring(0,Orgamt.length()-2))+ ",");
    		    			}else {		
    		    				Orgamt = String.valueOf(Integer.parseInt(Orgamt.substring(0,Orgamt.length()-2))) + "," + Orgamt.substring(Orgamt.length()-2);
    		    				if (f001Result.getValueByFieldName("ORGCCY").compareTo("JPY") == 0) 
    		    					Orgamt = Orgamt.substring(0,Orgamt.length()-2);
    		    			}
    		    			
    		    			log.debug("NOT OUR TWD ORGCCY  DETCHQ = 0 ORGCCY:"+f001Result.getValueByFieldName("ORGCCY")+" ORGAMT:"+Orgamt);
            	    		stb_Msg.append("//"+f001Result.getValueByFieldName("ORGCCY")+Orgamt);
            	    		stb_Msg.append("\r\n");    	    			    	    			
    		    			
    		    		}
    					
    					
    					
    				}else{
    					String EXRATE = f001Result.getValueByFieldName("EXRATE");
    					log.debug("F001 EXRATE:"+EXRATE);
    					EXRATE = String.valueOf((EXRATE.substring(0,EXRATE.length()-8))) + "." + EXRATE.substring(EXRATE.length()-8) ;
    					log.debug("EXRATE FORMAT:"+EXRATE);
    					BigDecimal pmt = new BigDecimal(PMTAMT);
    					log.debug("pmt: PMTAMT BigDecimal:"+pmt);
    			    	BigDecimal charge = new BigDecimal(detchg);
    			    	log.debug("charge: detchg BigDecimal:"+charge);
    			    	BigDecimal  rate = new BigDecimal(EXRATE);
    			    	log.debug("rate: EXRATE BigDecimal:"+rate);
    			    	BigDecimal inwardamt  = new BigDecimal(0.00);
    			    	log.debug("inwardamt: 0.00 BigDecimal:"+inwardamt);
   			    	
    			    	String mt199amt=null;
    			    	
    			    	if(!f001Result.getValueByFieldName("ORGCCY").equals("JPY")){
    			    		
    			    		inwardamt =(pmt.subtract(charge)).divide(rate,2, BigDecimal.ROUND_HALF_UP);
    			    		mt199amt  = inwardamt.toString();
    			    		log.debug("ORGCCY NOT JPY inwardamt:"+inwardamt+"  mt199amt:"+mt199amt);
    			    		
    			    	}else{
    			    		
    			    		
    			    		inwardamt =(pmt.subtract(charge)).divide(rate,0, BigDecimal.ROUND_HALF_UP);
    			    		
    			    		mt199amt  = inwardamt.toString();
    			    		log.debug("ORGCCY JPY inwardamt:"+inwardamt+"  mt199amt:"+mt199amt);
    			    		
    			    	}
    			    	
    			    	if(mt199amt.contains(".")){
    			    		mt199amt = mt199amt.replace('.' , ',');
    			    	}else{
    			    		mt199amt = mt199amt+",";
    			    	}
    			    	log.debug("mt199amt format:"+mt199amt);
    			    	
    				   
    				    if(mt199amt.substring(mt199amt.length()-2).equals("00")) {
    				    	
    					    mt199amt = mt199amt.substring(0,mt199amt.length()-2 );
    					   				     	
    				    }
		    			log.debug("NOT OUR TWD ORGCCY  DETCHQ != 0 ORGCCY:"+f001Result.getValueByFieldName("ORGCCY")+" mt199amt:"+mt199amt);   						    
         	    		stb_Msg.append("//"+f001Result.getValueByFieldName("ORGCCY")+mt199amt);
         	    		stb_Msg.append("\r\n");    	    			    	    			
    				}
    		    	
    		    	
    		    	
     
    	    		 
    	    		 
    		    	if(! "".equals(f001Result.getValueByFieldName("MT199_B4_M79NA5"))){
         	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("MT199_B4_M79NA5"));
         	    		stb_Msg.append("\r\n");    	    			    	    			
        		    	if("".equals(f001Result.getValueByFieldName("MT199_B4_M79NA6"))){
             	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("COMMCCY2")+amt);
             	    		stb_Msg.append("\r\n");    	    			    	    			
    			    	}
    			    }else {
         	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("COMMCCY2")+amt);
         	    		stb_Msg.append("\r\n");    	    			    	    			
    			    }
    		    	
       		    	if(! "".equals(f001Result.getValueByFieldName("MT199_B4_M79NA6"))){
         	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("MT199_B4_M79NA6"));
         	    		stb_Msg.append("\r\n");    	    			    	    			
           		    	if("".equals(f001Result.getValueByFieldName("MT199_B4_M79NA7"))){
             	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("COMMCCY2")+amt);
             	    		stb_Msg.append("\r\n");    	    			    	    			
    			    	}
    			    }
       		    	if(! "".equals(f001Result.getValueByFieldName("MT199_B4_M79NA7"))){
         	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("MT199_B4_M79NA7"));
         	    		stb_Msg.append("\r\n");    	    			    	    			
           		    	if("".equals(f001Result.getValueByFieldName("MT199_B4_M79NA8"))){
             	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("COMMCCY2")+amt);
             	    		stb_Msg.append("\r\n");    	    			    	    			
    			    	}
    			    }
       		    	if(! "".equals(f001Result.getValueByFieldName("MT199_B4_M79NA8"))){
         	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("MT199_B4_M79NA8"));
         	    		stb_Msg.append("\r\n");    	    			    	    			
           		    	if("".equals(f001Result.getValueByFieldName("MT199_B4_M79NA9"))){
             	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("COMMCCY2")+amt);
             	    		stb_Msg.append("\r\n");    	    			    	    			
    			    	}
    			    }
       		    	if(! "".equals(f001Result.getValueByFieldName("MT199_B4_M79NA9"))){
         	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("MT199_B4_M79NA9"));
         	    		stb_Msg.append("\r\n");    	    			    	    			
           		    	if("".equals(f001Result.getValueByFieldName("MT199_B4_M79NA10"))){
             	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("COMMCCY2")+amt);
             	    		stb_Msg.append("\r\n");    	    			    	    			
    			    	}
    			    }
       		    	if(! "".equals(f001Result.getValueByFieldName("MT199_B4_M79NA10"))){
         	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("MT199_B4_M79NA10"));
         	    		stb_Msg.append("\r\n");    	    			    	    			
           		    	if("".equals(f001Result.getValueByFieldName("MT199_B4_M79NA11"))){
             	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("COMMCCY2")+amt);
             	    		stb_Msg.append("\r\n");    	    			    	    			
    			    	}
    			    }
       		    	if(! "".equals(f001Result.getValueByFieldName("MT199_B4_M79NA11"))){
         	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("MT199_B4_M79NA11"));
         	    		stb_Msg.append("\r\n");    	    			    	    			
           		    	if("".equals(f001Result.getValueByFieldName("MT199_B4_M79NA12"))){
             	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("COMMCCY2")+amt);
             	    		stb_Msg.append("\r\n");    	    			    	    			
    			    	}
    			    }
       		    	if(! "".equals(f001Result.getValueByFieldName("MT199_B4_M79NA12"))){
         	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("MT199_B4_M79NA12"));
         	    		stb_Msg.append("\r\n");    	    			    	    			
           		    	if("".equals(f001Result.getValueByFieldName("MT199_B4_M79NA13"))){
             	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("COMMCCY2")+amt);
             	    		stb_Msg.append("\r\n");    	    			    	    			
    			    	}
    			    }
       		    	if(! "".equals(f001Result.getValueByFieldName("MT199_B4_M79NA13"))){
         	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("MT199_B4_M79NA13"));
         	    		stb_Msg.append("\r\n");    	    			    	    			
           		    	if("".equals(f001Result.getValueByFieldName("MT199_B4_M79NA14"))){
             	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("COMMCCY2")+amt);
             	    		stb_Msg.append("\r\n");    	    			    	    			
    			    	}
    			    }
       		    	if(! "".equals(f001Result.getValueByFieldName("MT199_B4_M79NA14"))){
         	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("MT199_B4_M79NA14"));
         	    		stb_Msg.append("\r\n");    	    			    	    			
              	    	stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("COMMCCY2")+amt);
             	    	stb_Msg.append("\r\n");    	    			    	    			
     			    }
    	    		
    	    	}else{
    	    		
    	    		/*
    	    		 * 幣別不是TWD
                     * 匯入款金額需扣除手續費
                     * 並多帶本行手續費欄位(原幣別) 
    	    		 */
    	    		
    	    		
    	    		//手續費負擔別 :DETCHARG 
    	    		 
   		    	
    		    	//COMMAMT2         :[000000000000683]  
    		    	
    				    				
    				//計算 幣別不是TWD
    			    //匯入款金額需扣除手續費
    				
    				 if(PMTAMT.substring(PMTAMT.length()-1).equals(",")){
    					 PMTAMT= PMTAMT.replace(',', '.')+"00";
    				 }else {
    					 PMTAMT= PMTAMT.replace(',', '.');
    				 }
    				 
    				 
    				 String detchg = amt;
    				 
    				       
    				 if(detchg.substring(detchg.length()-1).equals(",")){
    					 detchg = detchg.replace(',', '.')+"00";
    				 }else {
    					 detchg = detchg.replace(',', '.');
    				 }
    				    
    				    
    	
    				    
    				 BigDecimal orgamt = new BigDecimal(PMTAMT);
    				 BigDecimal orgdetchg = new BigDecimal(detchg);
    				 String mt199amt = orgamt.subtract(orgdetchg).toString();
    				 
    				       
    				 if(mt199amt.substring(mt199amt.length()-2).equals("00")) {
    				    	
    				    mt199amt = mt199amt.replace('.' , ',').substring(0,mt199amt.length()-2 );
    				    	
    				 }else {
    				    	
    				    mt199amt = mt199amt.replace('.' , ',');	
    				 }
    				    
		    		log.debug("NOT OUR NOT TWD PMTCCY:"+f001Result.getValueByFieldName("PMTCCY")+" mt199amt:"+mt199amt);   						    
      	    		stb_Msg.append("//"+f001Result.getValueByFieldName("PMTCCY")+mt199amt);
      	    		stb_Msg.append("\r\n");    	    			    	    			
    				 
    		    		    	
    		    	if(! "".equals(f001Result.getValueByFieldName("MT199_B4_M79NA5"))){
         	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("MT199_B4_M79NA5"));
         	    		stb_Msg.append("\r\n");    	    			    	    			
        		    	if("".equals(f001Result.getValueByFieldName("MT199_B4_M79NA6"))){
             	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("COMMCCY2")+amt);
             	    		stb_Msg.append("\r\n");    	    			    	    			
    			    	}
    			    }else {
         	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("COMMCCY2")+amt);
         	    		stb_Msg.append("\r\n");    	    			    	    			    			    	
    			    }
       		    	if(! "".equals(f001Result.getValueByFieldName("MT199_B4_M79NA6"))){
         	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("MT199_B4_M79NA6"));
         	    		stb_Msg.append("\r\n");    	    			    	    			
           		    	if("".equals(f001Result.getValueByFieldName("MT199_B4_M79NA7"))){
             	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("COMMCCY2")+amt);
             	    		stb_Msg.append("\r\n");    	    			    	    			
    			    	}
    			    }
       		    	if(! "".equals(f001Result.getValueByFieldName("MT199_B4_M79NA7"))){
         	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("MT199_B4_M79NA7"));
         	    		stb_Msg.append("\r\n");    	    			    	    			
           		    	if("".equals(f001Result.getValueByFieldName("MT199_B4_M79NA8"))){
             	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("COMMCCY2")+amt);
             	    		stb_Msg.append("\r\n");    	    			    	    			
    			    	}
    			    }
       		    	if(! "".equals(f001Result.getValueByFieldName("MT199_B4_M79NA8"))){
         	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("MT199_B4_M79NA8"));
         	    		stb_Msg.append("\r\n");    	    			    	    			
           		    	if("".equals(f001Result.getValueByFieldName("MT199_B4_M79NA9"))){
             	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("COMMCCY2")+amt);
             	    		stb_Msg.append("\r\n");    	    			    	    			
    			    	}
    			    }
       		    	if(! "".equals(f001Result.getValueByFieldName("MT199_B4_M79NA9"))){
         	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("MT199_B4_M79NA9"));
         	    		stb_Msg.append("\r\n");    	    			    	    			
           		    	if("".equals(f001Result.getValueByFieldName("MT199_B4_M79NA10"))){
             	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("COMMCCY2")+amt);
             	    		stb_Msg.append("\r\n");    	    			    	    			
    			    	}
    			    }
       		    	if(! "".equals(f001Result.getValueByFieldName("MT199_B4_M79NA10"))){
         	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("MT199_B4_M79NA10"));
         	    		stb_Msg.append("\r\n");    	    			    	    			
           		    	if("".equals(f001Result.getValueByFieldName("MT199_B4_M79NA11"))){
             	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("COMMCCY2")+amt);
             	    		stb_Msg.append("\r\n");    	    			    	    			
    			    	}
    			    }
       		    	if(! "".equals(f001Result.getValueByFieldName("MT199_B4_M79NA11"))){
         	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("MT199_B4_M79NA11"));
         	    		stb_Msg.append("\r\n");    	    			    	    			
           		    	if("".equals(f001Result.getValueByFieldName("MT199_B4_M79NA12"))){
             	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("COMMCCY2")+amt);
             	    		stb_Msg.append("\r\n");    	    			    	    			
    			    	}
    			    }
       		    	if(! "".equals(f001Result.getValueByFieldName("MT199_B4_M79NA12"))){
         	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("MT199_B4_M79NA12"));
         	    		stb_Msg.append("\r\n");    	    			    	    			
           		    	if("".equals(f001Result.getValueByFieldName("MT199_B4_M79NA13"))){
             	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("COMMCCY2")+amt);
             	    		stb_Msg.append("\r\n");    	    			    	    			
    			    	}
    			    }
       		    	if(! "".equals(f001Result.getValueByFieldName("MT199_B4_M79NA13"))){
         	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("MT199_B4_M79NA13"));
         	    		stb_Msg.append("\r\n");    	    			    	    			
           		    	if("".equals(f001Result.getValueByFieldName("MT199_B4_M79NA14"))){
             	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("COMMCCY2")+amt);
             	    		stb_Msg.append("\r\n");    	    			    	    			
    			    	}
    			    }
       		    	if(! "".equals(f001Result.getValueByFieldName("MT199_B4_M79NA14"))){
         	    		stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("MT199_B4_M79NA14"));
         	    		stb_Msg.append("\r\n");    	    			    	    			
              	    	stb_Msg.append("//:71F:"+f001Result.getValueByFieldName("COMMCCY2")+amt);
             	    	stb_Msg.append("\r\n");    	    			    	    			
     			    }
    	    	}
    	      }
    	    	
    		
//==================================================================
        	        	
        	stb_Msg.append("-}");        	            
        } 
        catch (Exception exc) 
        {
        	log.error("");
        }        
        
        return stb_Msg.toString();
    } //end createMsg()   
	
    
    private String checkEmpty(String tagValue)
    {
    	if ("".equals(tagValue))
    	{
    		return "";
    	}
    	else
    	{
    		return tagValue + "\r\n";
    	}    	
    }
}
