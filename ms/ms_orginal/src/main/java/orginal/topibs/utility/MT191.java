package orginal.topibs.utility;

import org.springframework.beans.factory.annotation.Value;

import fstop.model.MVHImpl;
import lombok.extern.slf4j.Slf4j;
import topibs.utility.NumericUtil; 

@Slf4j
public class MT191 {
	
	private StringBuffer stb_Msg = new StringBuffer();
	private MVHImpl f001Result = null;
	
	@Value("${ms_env}")
	private String ms_env;

	public void setF001Result(MVHImpl f001Result)
	{
		this.f001Result = f001Result;		
	}
	
    // 產生MT191的報文資料
    public String createMsg()
    {
        try
        {    
    		// String ip = IPUtils.getLocalIp();
			String fxstatus = ""; 
			fxstatus = "P".equals(ms_env) ? 
				f001Result.getValueByFieldName("M1SWIFT").substring(0, 8) : 
				f001Result.getValueByFieldName("M1SWIFT").substring(0, 7)+"0";
    		// if(ip.equals("172.22.11.30"))
    		// {	
    		// 	fxstatus=f001Result.getValueByFieldName("M1SWIFT").substring(0, 7)+"0";
    		// }
    		// if(ip.equals("10.16.21.16"))
    		// {	
    		// 	fxstatus=f001Result.getValueByFieldName("M1SWIFT").substring(0, 8);
    		// }
    		// if(ip.equals("10.16.21.17"))
    		// {
    		// 	fxstatus=f001Result.getValueByFieldName("M1SWIFT").substring(0, 8);
			// }      
			
			
        	stb_Msg.append("{1:F01");
        	stb_Msg.append(fxstatus + "B");
        	stb_Msg.append(f001Result.getValueByFieldName("M1SWIFT").substring(8));        	
        	stb_Msg.append("0000000000}");
        	
        	stb_Msg.append("{2:I191");
        	stb_Msg.append(f001Result.getValueByFieldName("M1SBBIC").substring(0, 8) + "X");
        	stb_Msg.append(f001Result.getValueByFieldName("M1SBBIC").substring(8));         	
        	stb_Msg.append("N}");
        	
        	stb_Msg.append("{3:{108:");
        	
            // TIA_TLID 需依照規則另外做判定, 如果 M1SWIFT 內容最後三碼為"XXX", 則填入"REM"
            // 若 M1SWIFT 內容最後三碼為"893", 則填入承做行(TXBRH)內容, 又, 如果承做行內容為"041", 則填入"REM"
            // 其餘狀況一律取 M1SWIFT 之最後三碼填入        	
        	String str_TLID = f001Result.getValueByFieldName("M1SWIFT").substring(8);  //發電者之分行代碼
    		if (str_TLID.equals("XXX"))
    			stb_Msg.append("REM"); 			
    		else if (str_TLID.equals("893")) {    			
    		    if ((f001Result.getValueByFieldName("TXBRH")).equals("041")) {
        			stb_Msg.append("REM");
    		    }
    		    else {
        			stb_Msg.append(f001Result.getValueByFieldName("TXBRH")); 
    		    }    			    			
    		}
    		else
    			stb_Msg.append(f001Result.getValueByFieldName("M1SWIFT").substring(8));     			
    			
    		
    		stb_Msg.append("}}");
    		
        	stb_Msg.append("{4:");
        	stb_Msg.append("\r\n");
        	
        	if (! "".equals(f001Result.getValueByFieldName("M1REFNO")))
        	{	
        		stb_Msg.append(":20:");
        		stb_Msg.append(f001Result.getValueByFieldName("M1REFNO"));
        		stb_Msg.append("\r\n");
        	}

        	if (! "".equals(f001Result.getValueByFieldName("M2REFNO")))
        	{	
        		stb_Msg.append(":21:");
        		stb_Msg.append(f001Result.getValueByFieldName("M2REFNO"));
        		stb_Msg.append("\r\n");
        	}
        	
        	if (! "".equals(f001Result.getValueByFieldName("M232CCY")))
        	{	
        		stb_Msg.append(":32B:");
        		stb_Msg.append(f001Result.getValueByFieldName("M232CCY"));
        		
        		String str = NumericUtil.formatNumberString(f001Result.getValueByFieldName("M232AMT"),2);	
        		str = str.replaceAll(",", "");
        		str = str.replaceAll("\\.", ",");
        		
        		//當幣別為TWD,JPY時,Tag32之金額欄位不可包含小數位
        		if ("JPY".equals(f001Result.getValueByFieldName("M232CCY")) || "TWD".equals(f001Result.getValueByFieldName("M232CCY")))
        		{
        			str = str.substring(0, str.indexOf(",")+1);        			
        		}
        		
        		stb_Msg.append(str);        		     		
        		stb_Msg.append("\r\n");
        	}        	
        	
        	if (! "".equals(f001Result.getValueByFieldName("M252OPT")))
        	{	
        		if ("A".equals(f001Result.getValueByFieldName("M252OPT")) &&
            			(f001Result.getValueByFieldName("M252BIC") != null) &&
            			(! "".equals(f001Result.getValueByFieldName("M252BIC").trim())) )
            	{
            		stb_Msg.append(":52");
            		stb_Msg.append((" ".equals(f001Result.getValueByFieldName("M252OPT"))) ? "" : f001Result.getValueByFieldName("M252OPT"));
            		stb_Msg.append(":");        		
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M252ACC")));
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M252BIC")));		
            	}
            	else if ( "D".equals(f001Result.getValueByFieldName("M252OPT")) &&
                		 (((f001Result.getValueByFieldName("M252AD1") != null) && (! "".equals(f001Result.getValueByFieldName("M252AD1").trim())))  || 
                		  ((f001Result.getValueByFieldName("M252AD2") != null) && (! "".equals(f001Result.getValueByFieldName("M252AD2").trim())))  ||
                		  ((f001Result.getValueByFieldName("M252AD3") != null) && (! "".equals(f001Result.getValueByFieldName("M252AD3").trim())))  ||
                		  ((f001Result.getValueByFieldName("M252AD4") != null) && (! "".equals(f001Result.getValueByFieldName("M252AD4").trim()))))  )
            	{
            		stb_Msg.append(":52");
            		stb_Msg.append((" ".equals(f001Result.getValueByFieldName("M252OPT"))) ? "" : f001Result.getValueByFieldName("M252OPT"));
            		stb_Msg.append(":");        		
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M252ACC")));            		
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M252AD1")));
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M252AD2")));      
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M252AD3"))); 
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M252AD4")));         			
            	}            		
        	}
        	
        	if (! "".equals(f001Result.getValueByFieldName("M257OPT")))
        	{	
        		if ("A".equals(f001Result.getValueByFieldName("M257OPT")) &&
            			(f001Result.getValueByFieldName("M257BIC") != null) &&
            			(! "".equals(f001Result.getValueByFieldName("M257BIC").trim())) )
            	{
            		stb_Msg.append(":57");
            		stb_Msg.append((" ".equals(f001Result.getValueByFieldName("M257OPT"))) ? "" : f001Result.getValueByFieldName("M257OPT"));        		
            		stb_Msg.append(":");        		
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M257ACC")));
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M257BIC")));
            	}
            	else if ( "D".equals(f001Result.getValueByFieldName("M257OPT")) &&
                		 (((f001Result.getValueByFieldName("M257AD1") != null) && (! "".equals(f001Result.getValueByFieldName("M257AD1").trim())))  || 
                		  ((f001Result.getValueByFieldName("M257AD2") != null) && (! "".equals(f001Result.getValueByFieldName("M257AD2").trim())))  ||
                		  ((f001Result.getValueByFieldName("M257AD3") != null) && (! "".equals(f001Result.getValueByFieldName("M257AD3").trim())))  ||
                		  ((f001Result.getValueByFieldName("M257AD4") != null) && (! "".equals(f001Result.getValueByFieldName("M257AD4").trim()))))  )
            	{
            		stb_Msg.append(":57");
            		stb_Msg.append((" ".equals(f001Result.getValueByFieldName("M257OPT"))) ? "" : f001Result.getValueByFieldName("M257OPT"));        		
            		stb_Msg.append(":");        		
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M257ACC")));            		
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M257AD1")));
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M257AD2")));      
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M257AD3"))); 
            		stb_Msg.append(checkEmpty(f001Result.getValueByFieldName("M257AD4")));        			
            	}       
        	}        	
        	        	        	
        	if (! "".equals(f001Result.getValueByFieldName("M172NA1")))
        	{	
        		stb_Msg.append(":71B:");
        		stb_Msg.append(f001Result.getValueByFieldName("M172NA1"));
        		stb_Msg.append("\r\n");
        	}
        	
        	if (! "".equals(f001Result.getValueByFieldName("M172NA2")))
        	{	        	
        		stb_Msg.append(f001Result.getValueByFieldName("M172NA2"));
        		stb_Msg.append("\r\n");
        	}
        	
        	if (! "".equals(f001Result.getValueByFieldName("M172NA3")))
        	{	
        		stb_Msg.append(f001Result.getValueByFieldName("M172NA3"));
        		stb_Msg.append("\r\n");
        	}
        	
        	if (! "".equals(f001Result.getValueByFieldName("M172NA4")))
        	{	        		
        		stb_Msg.append(f001Result.getValueByFieldName("M172NA4"));
        		stb_Msg.append("\r\n");
        	}
        	
        	if (! "".equals(f001Result.getValueByFieldName("M172NA5")))
        	{	        		
        		stb_Msg.append(f001Result.getValueByFieldName("M172NA5"));
        		stb_Msg.append("\r\n");
        	}
        	
        	if (! "".equals(f001Result.getValueByFieldName("M172NA6")))
        	{	        		
        		stb_Msg.append(f001Result.getValueByFieldName("M172NA6"));
        		stb_Msg.append("\r\n");
        	}
        	
        	if (! "".equals(f001Result.getValueByFieldName("M272NA1")))
        	{	
        		stb_Msg.append(":72:");
        		stb_Msg.append(f001Result.getValueByFieldName("M272NA1"));
        		stb_Msg.append("\r\n");
        	}
        	
        	if (! "".equals(f001Result.getValueByFieldName("M272NA2")))
        	{	        	
        		stb_Msg.append(f001Result.getValueByFieldName("M272NA2"));
        		stb_Msg.append("\r\n");
        	}
        	
        	if (! "".equals(f001Result.getValueByFieldName("M272NA3")))
        	{	
        		stb_Msg.append(f001Result.getValueByFieldName("M272NA3"));
        		stb_Msg.append("\r\n");
        	}
        	
        	if (! "".equals(f001Result.getValueByFieldName("M272NA4")))
        	{	        		
        		stb_Msg.append(f001Result.getValueByFieldName("M272NA4"));
        		stb_Msg.append("\r\n");
        	}
        	
        	if (! "".equals(f001Result.getValueByFieldName("M272NA5")))
        	{	        		
        		stb_Msg.append(f001Result.getValueByFieldName("M272NA5"));
        		stb_Msg.append("\r\n");
        	}
        	
        	if (! "".equals(f001Result.getValueByFieldName("M272NA6")))
        	{	        		
        		stb_Msg.append(f001Result.getValueByFieldName("M272NA6"));
        		stb_Msg.append("\r\n");
        	}        	
        	        	
        	stb_Msg.append("-}");        	            
        } 
        catch (Exception exc) 
        {
            log.error("",exc);
        }        
        
        return stb_Msg.toString();
    } //end createMsg()   
	
    
    private String checkEmpty(String tagValue)
    {
    	if ("".equals(tagValue))
    	{
    		return "";
    	}
    	else
    	{
    		return tagValue + "\r\n";
    	}    	
    }
}
