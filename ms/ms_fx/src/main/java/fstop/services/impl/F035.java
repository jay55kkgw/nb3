package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.TXNUSER;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class F035 extends CommonService {

	@Autowired
	@Qualifier("f035Telcomm")
	private TelCommExec f035Telcomm;

	@Autowired
	@Qualifier("n951CMTelcomm")
	private TelCommExec n951CMTelcomm;

	@Autowired
	private TxnUserDao txnUserDao;

	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) {

		params.put("CUSIDN", params.get("UID"));

		/*** 驗證交易密碼 - 開始 ***/
		if (params.get("FGTXWAY") == null) {
			throw TopMessageException.create("S302", (String) params.get("ADOPID"));
		} else if ("0".equals(params.get("FGTXWAY"))) { // 使用交易密碼
			// 2019.05.15 ian 改為N951
			MVHImpl n951Result = n951CMTelcomm.query(params);
			// 若驗證失敗, 則會發動 TopMessageException
//			params.put("FGTXWAY", fgtxway);					
		}
		/*** 驗證交易密碼 - 結束 ***/

		TelcommResult telcommResult = f035Telcomm.query(params);
		telcommResult.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));

		TXNUSER user = txnUserDao.chkRecord(params.get("UID").toString());
		if (params.get("TXTYPE").equals("A"))
			user.setFXREMITFLAG("Y");
		else
			user.setFXREMITFLAG("N");

		txnUserDao.save(user);

		return telcommResult;
	}

}
