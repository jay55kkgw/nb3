package fstop.services.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.orm.dao.TxnFxRecordDao;
import fstop.orm.dao.TxnReqInfoDao;
import fstop.services.CommonService;
import fstop.services.ServiceBindingException;
import fstop.services.mgn.B105;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 外匯匯出匯款-即時
 * @author Owner
 *
 */
@Slf4j
public class F002R extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("f007Telcomm")
	private TelCommExec f007Telcomm;

	@Autowired
	private TxnFxRecordDao txnFxRecordDao;

	@Autowired
	private TxnReqInfoDao txnReqInfoDao;
//	@Autowired
//	private FxUtils util;
	@Autowired 
	private B105 b105;
	
	
//	@Required
//	public void setTxnReqInfoDao(TxnReqInfoDao txnReqInfoDao) {
//		this.txnReqInfoDao = txnReqInfoDao;
//	}
//	
//	@Required
//	public void setF007Telcomm(TelCommExec telcomm) {
//		f007Telcomm = telcomm;
//	}
//
//	@Required
//	public void setTxnFxRecordDao(TxnFxRecordDao txnFxRecordDao) {
//		this.txnFxRecordDao = txnFxRecordDao;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params) { 
		//TODO 在營業時間 09:30~15:30 外, 只能執行預約交易
		Map<String, String> params = _params;
		
		String fginacno = params.get("FGINACNO"); // 1 約定帳號, 2 非約定帳號

		String f = params.get("FGTRDATE");
		if("0".equals(f)) {  //		即時
			
//			TODO 測試時期暫時註解
			if (! b105.isFxBizTime()) {
				throw TopMessageException.create("Z400", "F002");
			}
			
			return online(params);
		}
		else {
			throw new ServiceBindingException("不為即時交易");
		}
		// 取得議價
	}
	
	
	public TelcommResult online(Map<String, String> params) {
		//TODO 組合議價電文所需要的欄位
		Date d = new Date();
		params.put("CMTRDATE", DateTimeUtils.format("yyyyMMdd", d));
		params.put("CMTRTIME", DateTimeUtils.format("HHmmss", d));

		params.put("FYACN", params.get("CUSTACC")); //轉出帳號
		//params.put("INACN", params.get("BENACC")); //轉入帳號
		params.put("INACN", ""); //轉入帳號
		
		params.put("OUT_CRY", params.get("PAYCCY")); //轉出幣別
		params.put("IN_CRY", params.get("REMITCY")); //轉入幣別
		if("1".equals(params.get("PAYREMIT"))) {
			params.put("AMTTYPE", "1");//金額類別
			params.put("CURAMT", params.get("CURAMT"));//轉出金額 (議價金額)
		}
		else { // "2".equals(params.get("PAYREMIT"))
			params.put("AMTTYPE", "2");//金額類別
			params.put("ATRAMT", params.get("CURAMT"));//轉入金額
		}		
		params.put("FLAG", "R");//交易狀態
		/*
	 		如果電文失敗或寫DB發生任何錯誤, 則發 F007的電文
	 		但是  FLAG 內容為 C 的電文
		*/	
		
		params.put("BGROENO", "");
		
		TopMessageException lastException = null;		
		TelcommResult result = null;  //議價電文
		
		try {
			result = f007Telcomm.query(params);  //議價電文
		}
		catch(Exception e) {			
			if(e instanceof TopMessageException) {
				lastException = (TopMessageException)e;
				throw lastException;
			}		
		}
				
		if(! checkSuccessTopMsg(result.getFlatValues().get("MSG_CODE"))) {
			throw new TopMessageException(result.getFlatValues().get("MSG_CODE"), "", "");			
		}		
		
		
/*		
		TXNFXRECORD record = new TXNFXRECORD();
			
		record.setADTXNO(DateTimeUtils.format("yyyyMMddHHmmss", new Date()) + params.get("UID"));

		String f = params.get("FGTRDATE");  //判斷即時還是預約

		record.setFXTXDATE(params.get("CMTRDATE"));
		record.setFXTXTIME(params.get("CMTRTIME"));

		record.setFXUSERID(params.get("UID"));
		record.setFXWDAC(params.get("CUSTACC"));  //轉出帳號
		record.setFXSVBH(params.get("__TRIN_BANKCOD"));  //轉入分行
		record.setFXWDAC(params.get("__TRIN_ACN"));  //轉入帳號
		record.setFXWDCURR(params.get("PAYCCY"));  //轉出幣別
		record.setFXSVCURR(params.get("REMITCY"));  //轉入幣別
		
		if("1".equals(params.get("PAYREMIT"))) {
			record.setFXWDAMT(params.get("CURAMT"));  // 轉出金額
		}
		else { // "2".equals(params.get("PAYREMIT"))
			record.setFXSVAMT(params.get("ATRAMT"));  // 轉入金額
		}
		
		record.setFXTXMEMO(params.get("CMTRMEMO"));
		record.setFXTXMAILS(params.get("CMTRMAIL"));
		record.setFXTXMAILMEMO(params.get("CMMAILMEMO"));  //CMMAILMEMO 對應的欄位

		params.put("BGROENO", result.getValueByFieldName("BGROENO"));  //議價編號
		params.put("RATE", result.getValueByFieldName("RATE"));  //匯率
		
		TXNREQINFO titainfo = new TXNREQINFO();
		titainfo.setREQINFO(BookingUtils.requestInfo(params));
		txnReqInfoDao.save(titainfo);
		
		
		record.setFXTITAINFO(titainfo.getREQID());
		
		record.setADOPID(params.get("TXID"));
		
		//這時候還不用記
		record.setFXTXCODE("R");
		
		record.setFXMSGSEQNO("A");
		
		txnFxRecordDao.save(record);
		
		result.getFlatValues().put("ADTXNO", record.getADTXNO());
*/		
		
		params.put("BGROENO", result.getValueByFieldName("BGROENO"));  //議價編號
		params.put("RATE", result.getValueByFieldName("RATE"));  //匯率
		
/*		
		if("1".equals(params.get("PAYREMIT"))) {   //轉出
			
				double dd = new Double(params.get("CURAMT")) * new Double(params.get("RATE"));
				result.getFlatValues().put("CURAMT", params.get("CURAMT"));  // 轉出金額
				result.getFlatValues().put("ATRAMT", dd + "");  // 轉入金額
			
		}
		else { // "2".equals(params.get("PAYREMIT"))   //轉入
			
				double dd = new Double(params.get("ATRAMT")) / new Double(params.get("RATE"));
				result.getFlatValues().put("CURAMT", dd + "");  // 轉入金額
				result.getFlatValues().put("ATRAMT", params.get("ATRAMT"));  // 轉入金額
			
		}
*/
		
		double dd;
		if("1".equals(params.get("PAYREMIT"))) {   //轉出

				if ("TWD".equals(params.get("OUT_CRY")))
					dd = new Double(params.get("CURAMT")) / new Double(params.get("RATE"));					
				else	
					dd = new Double(params.get("CURAMT")) * new Double(params.get("RATE"));
				
				result.getFlatValues().put("CURAMT", params.get("CURAMT"));  // 轉出金額
				result.getFlatValues().put("ATRAMT", dd + "");  // 轉入金額
			
		}
		else { // "2".equals(params.get("PAYREMIT"))   //轉入
			
				if ("TWD".equals(params.get("OUT_CRY")))
					dd = new Double(params.get("ATRAMT")) * new Double(params.get("RATE"));			
				else	
					dd = new Double(params.get("ATRAMT")) / new Double(params.get("RATE"));		
						
				result.getFlatValues().put("CURAMT", dd + "");  // 轉出金額
				result.getFlatValues().put("ATRAMT", params.get("ATRAMT"));  // 轉入金額			
		}
		
		return result;
	}
	
	private String getTITAInfo(Map<String, String> params) {
		Map<String, String> mr = new HashMap();
		//F001R 特別要記的欄位
		String[] fields = new String[]{
			"FGTRDATE" //判斷即時還是預約
			,"BGROENO" //議價編號
			,"RATE"	   //匯率
		};

		for(String f : fields) {
			mr.put(f, params.get(f)); 
		}
		return JSONUtils.map2json(mr);
	}
	
	private boolean checkSuccessTopMsg(String topmsg) {
		//若回傳為 000000, 或者為 空白, 則回傳 true
		try {
			Double d = Double.parseDouble(topmsg);
			if(d == 0d)
				return true;
		}
		catch(Exception e) {}
		return "".equals(topmsg);
	}	
}
