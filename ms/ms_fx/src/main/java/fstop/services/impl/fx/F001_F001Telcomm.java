package fstop.services.impl.fx;

import java.util.Hashtable;
import java.util.Map;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import fstop.exception.ToRuntimeException;
import fstop.exception.TopMessageException;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.services.impl.F001T;
import fstop.services.impl.N870;
import fstop.util.JSONUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class F001_F001Telcomm implements FxTelCommStep {
	
//	Logger logger = Logger.getLogger(getClass());

	private F001T f001t;
	
	private boolean isNeedDoF011WithFlagN = false; 

	private TelcommResult telcommResult = null; 
	
	public F001_F001Telcomm(F001T f001t) {
		this.f001t = f001t;
	}
	
	boolean isSkip = false;

	@Override
	public boolean isSkip() {
		return isSkip;
	}

	public void setSkip(boolean isSkip) {
		this.isSkip = isSkip;
	}

	public Exception lastException;

	@Override
	public TelcommResult query(Map _params) {
		Map<String, String> params = _params;
		
		if(isSkip()) {
			String oldResult = (String)params.get("__F001_RESULT");
			Map m = JSONUtils.json2map(oldResult);
			TelcommResult telResult = new TelcommResult(new Vector());
			telResult.getFlatValues().putAll(m);
			this.telcommResult = telResult;
			return telResult;
		}
		
		TelcommResult result = null;
		boolean isFail = false;
		String topmsg = "";
		try {
			telcommResult = f001t.doF001(params);
			result = telcommResult;
//			topmsg = result.getValueByFieldName("__TOPMSG");
//			if(!f001t.checkSuccessTopMsg(topmsg)) {
//				throw TopMessageException.create(topmsg);
//			}
			
			params.put("__F001_RESULT", JSONUtils.map2json(result.getFlatValues()));
			params.put("__FXMSGSEQNO", StrUtils.trim(params.get("__FXMSGSEQNO")) + "1");
		}
		catch(Exception e) {
			
			/*** New Add ***/			
			if (e instanceof TopMessageException) {
				
				/**
				 *  檢查 F001 回應之訊息代碼: 
				 *  1.若存在於資料庫 && 註記可人工重送("Y") ==> 進人工重送處理,不需發 F007C(幣轉時)/F011
				 *  2.其它 ==> 一律發 F007C(幣轉時)/F011		 
				 */	
				try {					
					topmsg = ((TopMessageException)e).getMsgcode();	

					isNeedDoF011WithFlagN = f001t.checkTopMsg(topmsg, params.get("__SCHID"));		
					isFail = isNeedDoF011WithFlagN;
					
					//進人工重送須記錄 F001.STAN 欄位值
					if ( ! this.isNeedDoF011WithFlagN )
						params.put("__FXMSGCONTENT", StrUtils.trim(params.get("STAN")));
				}
				catch(Exception ex) { }				
				
			}
			else {
				isNeedDoF011WithFlagN = true;			
				isFail = true;
			}

			lastException = e;
		}

		if(isFail && !StrUtils.trim(params.get("PAYCCY")).equalsIgnoreCase(params.get("REMITCY"))) {
			try {
				
//				if("1".equals(params.get("PAYREMIT"))) {
//					try {
//						double d = new Double(params.get("CURAMT")) * new Double(params.get("RATE"));
//						result.getFlatValues().put("CURAMT", d + "");  // 轉出金額
//					}
//					catch(Exception e){}
//				}
//				else { // "2".equals(params.get("PAYREMIT"))
//					try {
//						double d = new Double(params.get("ATRAMT")) * new Double(params.get("RATE"));
//						result.getFlatValues().put("ATRAMT", d + "");  // 轉入金額
//					}
//					catch(Exception e){}
//				}

				result = f001t.doF007C(params);
				
				params.put("__FXMSGSEQNO", StrUtils.trim(params.get("__FXMSGSEQNO")) + "5");					
			}
			catch(Exception e) {
				if(lastException != null) {
					log.error("F001 已發生 " + lastException.getMessage() + ", F007  時又發生 " + e.getMessage());
//					ToRuntimeException rex = new ToRuntimeException("F001 已發生 " + lastException.getMessage() + ", F007  時又發生 " + e.getMessage(), e);
//					lastException = rex;
				}
			}
		}
		
		return result;
	}

	@Override
	public Exception getLastException() {
		return lastException;
	}

	@Override
	public boolean isNeedDoF011WinFlagN() {
		return isNeedDoF011WithFlagN;
	}
	
	public TelcommResult getTelcommResult() {
		return telcommResult;
	}
	
}
