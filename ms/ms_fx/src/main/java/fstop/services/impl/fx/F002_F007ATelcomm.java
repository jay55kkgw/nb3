package fstop.services.impl.fx;

import java.util.Hashtable;
import java.util.Map;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import fstop.exception.ToRuntimeException;
import fstop.exception.TopMessageException;
import fstop.model.TelcommResult;
import fstop.services.impl.F002T;
import fstop.services.impl.N870;
import fstop.util.JSONUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class F002_F007ATelcomm implements FxTelCommStep {
//	Logger logger = Logger.getLogger(getClass());

	@Autowired
	private F002T f002t;
	
	public F002_F007ATelcomm(F002T f002t) {
		this.f002t = f002t;
	}
	
	public Exception lastException;
	
	boolean isSkip = false;

	@Override
	public boolean isSkip() {
		return isSkip;
	}

	public void setSkip(boolean isSkip) {
		this.isSkip = isSkip;
	}

	@Override
	public TelcommResult query(Map _params) {
		Map<String, String> params = _params;
		
		if(isSkip()) {
			TelcommResult telResult = new TelcommResult(new Vector());
			return telResult;
		}
		
		TelcommResult result = null;
		boolean isFail = false;
		String topmsg = "";
		boolean topmsgNotInDB = false;
		try {
			result = f002t.doF007A(params); 
			
			params.put("BGROENO", result.getValueByFieldName("BGROENO"));  //議價編號
			params.put("RATE", result.getValueByFieldName("RATE"));  //匯率
			
//			if("1".equals(params.get("PAYREMIT"))) {   //轉出
//				
//					double dd = new Double(params.get("CURAMT")) * new Double(params.get("RATE"));
//					result.getFlatValues().put("CURAMT", params.get("CURAMT"));  // 轉出金額
//					result.getFlatValues().put("ATRAMT", dd + "");  // 轉入金額
//				
//			}
//			else { // "2".equals(params.get("PAYREMIT"))   //轉入
//				
//					double dd = new Double(params.get("ATRAMT")) / new Double(params.get("RATE"));
//					result.getFlatValues().put("CURAMT", dd + "");  // 轉入金額
//					result.getFlatValues().put("ATRAMT", params.get("ATRAMT"));  // 轉入金額
//				
//			}
			
			params.put("__FXMSGSEQNO", StrUtils.trim(params.get("__FXMSGSEQNO")) + "0");
		}		
		catch(Exception e) {	
			
			/*** New Add ***/			
			if (e instanceof TopMessageException) {
				
				/**
				 *  檢查 F001 回應之訊息代碼: 
				 *  1.若存在於資料庫 && 註記可系統/人工重送("Y") ==> 進系統/人工重送處理,不需發 F007C
				 *  2.其它 ==> 一律發 F007C	 
				 */	
				try {					
					topmsg = ((TopMessageException)e).getMsgcode();							
					isFail = f002t.checkMervaTopMsg(topmsg, params.get("__SCHID"));					
				}
				catch(Exception ex) { }				
				
			}
			else {				
				isFail = true;
			}

			lastException = e;
		}
		
		
//		//若沖回議價部位失敗,進入人工處理流程
//		if(isFail) {
//			try {
//				result = f002t.doF007C(params);
//				
//				params.put("__FXMSGSEQNO", StrUtils.trim(params.get("__FXMSGSEQNO")) + "5");					
//			}
//			catch(Exception e) {
//				
//				logger.error("F007A 已發生 " + lastException.getMessage() + ", doF007C 時又發生 " + e.getMessage());				
//
//				lastException = e;
//			}
//		}
		
		
		return result;
	}

	@Override
	public Exception getLastException() {
		return lastException;
	}

	@Override
	public boolean isNeedDoF011WinFlagN() {
		return false;
	}
	

	
}
