package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.rest.model.CommonPools;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.services.DispatchService;
import fstop.services.ServiceBindingException;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N177 extends DispatchService  {
	@Autowired
	CommonPools commonPools;
	
	@Autowired
	@Qualifier("n530Service")
	private CommonService n530Service;
	
	@Autowired
    @Qualifier("n177Telcomm")
	private TelCommExec n177Telcomm;
	
	
	@Override
	public String getActionMethod(Map<String, String> params) {
		String txid = StrUtils.trim(params.get("TXID"));
		if("N177".equals(txid))
			return "doN530";
		else if("N177_1".equals(txid))
			return "doN177_1";
		else 
			throw new ServiceBindingException("不支援的 TXID [" + txid+"].");
	}
	
	public MVH doN530(Map params) {
		params.put("LOSTTYPE", "177");
		
		return n530Service.doAction(params);
	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public MVH doN177_1(Map<String, String> params) { 
		
		String TxWay = params.get("FGTXWAY");
		
		// 產生 CERTACN update by Blair -------------------------------------------- Start
		String certACN = "";
		if ("0".equals(TxWay))
		{
			while (certACN.length() < 19)
				certACN += " ";
		}
		else if ("1".equals(TxWay))
		{
			while (certACN.length() < 19)
				certACN += "1";
		}else if ("7".equals(TxWay))
		{
			while (certACN.length() < 19)
				certACN += "C";
		}
		else
		{
			while (certACN.length() < 19)
				certACN += "4";
		}
		params.put("CERTACN", certACN);
		// 產生 CERTACN update by Blair -------------------------------------------- End

		String flag = params.get("FGAUTXFTM");  //轉期次數
		if("Y".equals(flag)) { //99 次
			params.put("AUTXFTM", StrUtils.trim(params.get("AUTXFTM")));			
		}
		else{ //取消自動轉期(不轉期)
			params.put("AUTXFTM", "0");
		}
		
		params.put("CUSIDN", params.get("UID"));
		params.put("FLAG", "1"); //固定為 1：約定
		
		MVH cry = commonPools.ui.getCRY(params.get("CRY"));
		params.put("CRY", cry.getValueByFieldName("ADCCYNO"));
		
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);

		TelcommResult result = n177Telcomm.query(params);
		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));

		try {
			result.getFlatValues().put("CMTXTIME", 
					DateTimeUtils.getCDateTime(
							result.getValueByFieldName("TRNDATE"),
							result.getValueByFieldName("TRNTIME")));
		}
		catch(Exception e){
			log.error("", e);
		}
		
		return result;
	}



}
