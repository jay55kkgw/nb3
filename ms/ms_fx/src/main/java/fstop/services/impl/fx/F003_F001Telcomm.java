package fstop.services.impl.fx;

import java.util.Hashtable;
import java.util.Map;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import fstop.exception.ToRuntimeException;
import fstop.exception.TopMessageException;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.services.impl.F001T;
import fstop.services.impl.F003T;
import fstop.services.impl.N870;
import fstop.util.JSONUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class F003_F001Telcomm implements FxTelCommStep {
	
//	Logger logger = Logger.getLogger(getClass());

	@Autowired
	private F003T f003t;
	
	private boolean isNeedDoF011WithFlagN = false; 

	@Autowired
	private TelcommResult telcommResult = null; 
	
	public F003_F001Telcomm(F003T f003t) {
		this.f003t = f003t;
	}
	
	boolean isSkip = false;

	@Override
	public boolean isSkip() {
		return isSkip;
	}

	public void setSkip(boolean isSkip) {
		this.isSkip = isSkip;
	}

	@Autowired
	public Exception lastException;

	@Override
	public TelcommResult query(Map _params) {
		Map<String, String> params = _params;
		
		if(isSkip()) {
			String oldResult = (String)params.get("__F001_RESULT");
			Map m = JSONUtils.json2map(oldResult);
			TelcommResult telResult = new TelcommResult(new Vector());
			telResult.getFlatValues().putAll(m);
			this.telcommResult = telResult;
			return telResult;
		}
		
		TelcommResult result = null;
		boolean isFail = false;
		String topmsg = "";
		try {			
			telcommResult = f003t.doF001(params);
			result = telcommResult;

			params.put("__F001_RESULT", JSONUtils.map2json(result.getFlatValues()));
			params.put("__FXMSGSEQNO", StrUtils.trim(params.get("__FXMSGSEQNO")) + "1");							
		}
		catch(Exception e) {
			
			/*** New Add ***/			
			if (e instanceof TopMessageException) {
				
				/**
				 *  檢查 F001 回應之訊息代碼: 
				 *  1.若存在於資料庫 && 註記可人工重送("Y") ==> 進人工重送處理,不需發 F007C(幣轉時)/F011
				 *  2.其它 ==> 一律發 F007C(幣轉時)/F011		 
				 */	
				try {					
					topmsg = ((TopMessageException)e).getMsgcode();	
					
					isNeedDoF011WithFlagN = f003t.checkTopMsg(topmsg, params.get("__SCHID"));			
					isFail = isNeedDoF011WithFlagN;
					
					//進人工重送須記錄 F001.STAN 欄位值
					if ( ! this.isNeedDoF011WithFlagN )
						params.put("__FXMSGCONTENT", StrUtils.trim(params.get("STAN")));
				}
				catch(Exception ex) { }				
				
			}
			else {
				isNeedDoF011WithFlagN = true;			
				isFail = true;
			}

			lastException = e;
		}

		if(isFail && !StrUtils.trim(params.get("TXCCY")).equalsIgnoreCase(params.get("RETCCY"))) {
			try {
				result = f003t.doF007C(params);
				
				params.put("__FXMSGSEQNO", StrUtils.trim(params.get("__FXMSGSEQNO")) + "5");					
			}
			catch(Exception e) {
				if(lastException != null) {
					log.error("F001 已發生 " + lastException.getMessage() + ", F007  時又發生 " + e.getMessage());
//					ToRuntimeException rex = new ToRuntimeException("F001 已發生 " + lastException.getMessage() + ", F007  時又發生 " + e.getMessage(), e);
//					lastException = rex;
				}
			}
		}
		
		return result;
	}

	public Exception getLastException() {
		return lastException;
	}

	public boolean isNeedDoF011WinFlagN() {
		return isNeedDoF011WithFlagN;
	}
	
	public TelcommResult getTelcommResult() {
		return telcommResult;
	}
	
}
