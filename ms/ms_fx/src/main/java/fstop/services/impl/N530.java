package fstop.services.impl;

import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.model.GrpColumns;
import fstop.model.MVH;
import fstop.model.MVHUtils;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N530 extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("n530Telcomm")
	private TelCommExec n530Telcomm;
//
//	@Required
//	public void setN530Telcomm(TelCommExec telcomm) {
//		this.n530Telcomm = telcomm;
//	}
	
	@Override
	public MVH doAction(Map params) {
		
		TelcommResult helper = n530Telcomm.query(params);
		helper.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		helper.getFlatValues().put("CMRECNUM", helper.getValueOccurs("ACN") + "");
		try {
		
			String[] groupBy = new String[]{"CUID"};
			Map<GrpColumns, Double> sum = MVHUtils.group(helper.getOccurs(), groupBy).sum("BALANCE");
			int ind = 1;
			for(Entry<GrpColumns, Double> ent : sum.entrySet()) {
				GrpColumns grpCols = ent.getKey();

				Map<String, String> m = MVHUtils.toFlatValues(grpCols, "AMT", ind);
				helper.getFlatValues().putAll(m);
				helper.getFlatValues().put("FXTOTAMT_" + ind, ent.getValue() + "");
				ind++;
			}
		}
		finally {}
		try {
			
			String[] groupBy = new String[]{"CUID"};
			Map<GrpColumns, Double> sum = MVHUtils.group(helper.getOccurs(), groupBy).sum("NTDBAL");
			int ind = 1;
			for(Entry<GrpColumns, Double> ent : sum.entrySet()) {
				GrpColumns grpCols = ent.getKey();

				Map<String, String> m = MVHUtils.toFlatValues(grpCols, "NTDBAL", ind);
				helper.getFlatValues().putAll(m);
				helper.getFlatValues().put("FXTOTNTDBAL_" + ind, ent.getValue() + "");
				ind++;
			}
		}
		finally {}		
		return helper;
	}
}
