package fstop.services.impl;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.exception.ToRuntimeException;
import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.orm.dao.AdmMsgCodeDao;
import fstop.orm.dao.SysDailySeqDao;
import fstop.orm.dao.TxnFxRecordDao;
import fstop.orm.dao.TxnFxSchPayDataDao;
import fstop.orm.po.SYSPARAMDATA;
import fstop.orm.po.TXNFXRECORD;
import fstop.orm.po.TXNREQINFO;
import fstop.services.BOLifeMoniter;
import fstop.services.BookingUtils;
import fstop.services.CommonService;
import fstop.services.OnLineTransferNoticeFx;
import fstop.services.ServiceBindingException;
import fstop.services.TelCommExecWrapper;
import fstop.services.WriteLogInterface;
import fstop.services.impl.fx.F001Controller;
import fstop.services.mgn.B105;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import fstop.va.VaCheck;
import lombok.extern.slf4j.Slf4j;
import topibs.utility.NumericUtil;

/**
 * 外匯轉帳
 * @author Owner
 *
 */
@Slf4j
public class F001T extends CommonService implements WriteLogInterface{
	@Autowired
	private SysDailySeqDao sysDailySeqDao;

	@Autowired
	private F001Controller f001controller;
	
	@Autowired
	private B105 b105;

	@Autowired
	@Qualifier("f001Telcomm")
	private TelCommExec f001Telcomm;
	@Autowired
	@Qualifier("f003Telcomm")
	private TelCommExec f003Telcomm;
	@Autowired
	@Qualifier("f005Telcomm")
	private TelCommExec f005Telcomm;
	@Autowired
	@Qualifier("f007Telcomm")
	private TelCommExec f007Telcomm;
	@Autowired
	@Qualifier("f011Telcomm")
	private TelCommExec f011Telcomm;

	@Autowired
	private TxnFxRecordDao txnFxRecordDao;

	@Autowired
	private AdmMsgCodeDao admMsgCodeDao;
	

	@Autowired
	private TxnFxSchPayDataDao txnFxSchPayDataDao; // 預約資料


	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params) { 
		//TODO 在營業時間 09:30~15:30 外, 只能執行預約交易
		//TODO LOG 寫入的機制
		Map<String, String> params = _params;
		
		String fginacno = params.get("FGINACNO"); // 1 約定帳號, 2 非約定帳號
		
		//轉入帳號
		String trin_bnkcod = "";
		String trin_acn = "";
		String trin_agree = "";
		if("CMDAGREE".equals(fginacno)) {
			Map<String, String> jm = JSONUtils.json2map(params.get("INACNO1"));
			trin_bnkcod = jm.get("BNKCOD");
			trin_acn = jm.get("ACN");
			trin_agree = jm.get("AGREE");
			
		}
		else if("CMDISAGREE".equals(fginacno)) {  //非約定, 只能是台企銀 050
			trin_bnkcod = "050";
			trin_acn = params.get("INACNO2");
			trin_agree = "0";   // 0: 非約定, 1: 約定
		}
		
		params.put("__TRIN_BANKCOD", trin_bnkcod);
		params.put("__TRIN_ACN", trin_acn);
		params.put("__TRIN_AGREE", trin_agree);

/*		
log.debug("@@@ F001T.doAction() before BHOCheckUtils.checkBHOInAcno !");

		//BHO 轉入帳號檢核
		String str_UserInputAcno = (String)params.get("InAcno_1") + (String)params.get("InAcno_2") + (String)params.get("InAcno_3");
		String str_RandomValueAcno = (String)params.get("BHO_INACNO_VERIFY");		
		BHOCheckUtils.checkBHOInAcno(str_UserInputAcno, str_RandomValueAcno);		
*/
		
		String f = params.get("FGTRDATE");
		if("0".equals(f)) {  //		即時
			
			//判斷營業時間	,若交易最後提交時間  > (營業截止時間 + 5分鐘) => 須顯示錯誤訊息(Z409)
//			B105 b105 = (B105)SpringBeanFactory.getBean("b105".toLowerCase()); // 改用Autowired
//			SYSPARAMDATA sysParam = (SYSPARAMDATA)b105.getSysParamData();
//			
//			int i_MM = Integer.parseInt(sysParam.getADFXE1MM()) + 5;
//			String str_BizEnd = sysParam.getADFXE1HH() + String.valueOf(i_MM) + sysParam.getADFXE1SS();		
//			String str_CurrentTime = DateTimeUtils.getTimeShort(new Date());				
// 			if (str_CurrentTime.compareTo(str_BizEnd) > 0) {
//				throw TopMessageException.create("Z409", (String)params.get("ADOPID"));
//			}

			
			
		
			/*** 驗證交易密碼 - 開始 ***/
			if (params.get("FGTXWAY") == null) {
				throw TopMessageException.create("S302", (String)params.get("ADOPID"));				
			}
			else if("0".equals(params.get("FGTXWAY"))) {  //使用交易密碼
//				String TransPassUpdate = (String)params.get("TRANSPASSUPDATE");
//				String Phase2 = (String)params.get("PHASE2");
				TelCommExec n950CMTelcomm;
				
//				if(StrUtils.isNotEmpty(TransPassUpdate))
//				{
//					if(TransPassUpdate.equals("1") || Phase2.equals("ON"))
//					{
						n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n951CMTelcomm");
//					}
//					else
//					{
//						n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n950CMTelcomm");
//					}
//				}
//				else
//				{
//					n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n950CMTelcomm");
//				}
				
				// N950會發兩次所以需FGTXWAY=""，N950回應後再把FGTXWAY改成原值即可 2010.09.29
				String fgtxway=params.get("FGTXWAY");
				params.put("FGTXWAY", "");
				MVHImpl n950Result = n950CMTelcomm.query(params);
				//若驗證失敗, 則會發動 TopMessageException
				params.put("FGTXWAY", fgtxway);
				//F003 電文需要
				params.put("PINKEYFX",StrUtils.repeat("F2", 8));
			}		
			/*** 驗證交易密碼 - 結束 ***/
			

			MVHImpl result = new MVHImpl();
			result =  online(params);
			if(result == null)
				result = new MVHImpl();
			
			return result;
		}
		else {
			throw new ServiceBindingException("不為即時交易");
		}
	}

	public MVHImpl online(Map<String, String> _params) {
		
		//轉給 f001controller 去做 online 的動作
		OnLineTransferNoticeFx trNotice = new OnLineTransferNoticeFx();
		MVHImpl result = f001controller.online(_params, trNotice);
		
		return result;
	}

	public TelcommResult doF001(Map<String, String> params) {
		//TODO 組合F001電文所需要的欄位
		Date d = new Date();
		
		params.put("TERM_DATE", DateTimeUtils.format("yyyyMMdd", d));
		params.put("TERM_TIME", DateTimeUtils.format("HHmmss", d));
		String tranOut = StrUtils.trim(params.get("CUSTACC"));
		String tranIn = StrUtils.trim(params.get("__TRIN_ACN"));
		if(tranOut.startsWith("893") && tranIn.startsWith("893")) {
			params.put("PCODE", "000C");
			params.put("COUNTRY", "XA");
		}	
		else if(!tranOut.startsWith("893") && !tranIn.startsWith("893")) {
			params.put("PCODE", "000C");
			params.put("COUNTRY", "TW");			
		}	
		else {
			params.put("PCODE", "000B");
			params.put("COUNTRY", "XA");		
		}
		
		params.put("DEALNO", StrUtils.trim(params.get("BGROENO")));
		
		/*** New Add ***/
		
		if (StrUtils.isEmpty(params.get("STAN"))) {
//			params.put("STAN", "N" + StrUtils.right("0000000" + sysDailySeqDao.dailySeq("F001T"), 7) + "");
			params.put("STAN", sysDailySeqDao.getFXStan("F001T"));
			
			BOLifeMoniter.setValue("STAN", params.get("STAN"));
		}
		
		params.put("SEQNO", "00000");
		params.put("CUSTID", params.get("CUSIDN"));	
		
		/*** New Add ***/
		//params.put("MSGID", "");
		
		params.put("COMMCCY", ("#".equals(params.get("COMMCCY")))? params.get("PAYCCY"):params.get("COMMCCY"));	//手續費幣別			
		params.put("COMMACC", ("#".equals(params.get("COMMACC")))? params.get("CUSTACC"):params.get("COMMACC"));	//手續費帳號	
		
		params.put("CUSTACC", params.get("CUSTACC")); //轉出帳號
		params.put("BENACC", params.get("__TRIN_ACN")); //轉入帳號		
		
		if("1".equals(params.get("PAYREMIT"))) {
			params.put("AMTTYPE", "1");//金額類別
			params.put("CURAMT", params.get("CURAMT"));//轉出金額 (議價金額)			
			
			params.put("OUT_AMT", params.get("CURAMT"));//轉出金額 (議價金額) => 保留原始輸入欄位值				
		}
		else { // "2".equals(params.get("PAYREMIT"))
			params.put("AMTTYPE", "2");//金額類別
			params.put("ATRAMT", params.get("CURAMT"));//轉入金額
						
			params.put("IN_AMT", params.get("CURAMT"));//轉入金額	 => 保留原始輸入欄位值						
		}				
		 
		params.put("PAYDATE", StrUtils.trim(params.get("PAYDATE")).replaceAll("/", ""));  //付款日期
		
		params.put("CUSNM1", params.get("NAME")); //付款人名稱
		
		/*
	 		如果電文失敗或寫DB發生任何錯誤, 則發 F007的電文
	 		但是  FLAG 內容為 C 的電文
		*/
//外匯延長營業時間需求：當PCODE=000B，且營業時間超過3點30分後，不得執行交易回Z402		
//		B105 b105 = (B105)SpringBeanFactory.getBean("b105".toLowerCase());
		SYSPARAMDATA sysParam = (SYSPARAMDATA)b105.getSysParamData();		
		String str_CurrentTime = DateTimeUtils.getTimeShort(new Date());
		int i_MM = Integer.parseInt(sysParam.getADFXEMM()) + 5;
        String str_BizEnd = sysParam.getADFXEHH() + String.valueOf(i_MM) + sysParam.getADFXESS();
		
		if (str_CurrentTime.compareTo(str_BizEnd) > 0 && params.get("PCODE").toString().equals("000B")) {
			throw TopMessageException.create("Z402", (String)params.get("ADOPID"));
		}
		
		TelcommResult result = f001Telcomm.query(params);  //議價電文
		
		return result;
	}
	
	public TelcommResult doF003(MVHImpl resultF001, Map<String, String> params) {
		// FROM F001
		params.put("TXBRH", "");   //承作行
		params.put("CUSTPAY_ID", params.get("UID"));   //付款人統一編號
		params.put("ACCTID", params.get("CUSTACC"));   //F001 付款帳號
		params.put("ACCTTYPE", "");   //付款人帳戶類別
		params.put("ACCCUR", resultF001.getFlatValues().get("ORGCCY"));   //付款人轉出幣別
		params.put("ACCAMT", resultF001.getFlatValues().get("ORGAMT"));   //轉出金額
		
		params.put("CURCODE", resultF001.getFlatValues().get("PMTCCY"));   //付款人轉入幣別
		params.put("CURAMT", resultF001.getFlatValues().get("PMTAMT"));   //轉入金額
		
		
		/*** 紀錄F001之轉出轉入相關資料 ***/
		params.put("F001_ACCCUR", resultF001.getFlatValues().get("ORGCCY"));   //付款人轉出幣別
		params.put("F001_ACCAMT", resultF001.getFlatValues().get("ORGAMT"));   //轉出金額
		
		params.put("F001_CURCODE", resultF001.getFlatValues().get("PMTCCY"));   //付款人轉入幣別
		params.put("F001_CURAMT", resultF001.getFlatValues().get("PMTAMT"));   //轉入金額
		
		
		
		
		if (params.get("OUT_CRY") == null) {
			params.put("OUT_CRY", params.get("ACCCUR")); //轉出幣別
			params.put("IN_CRY", params.get("CURCODE")); //轉入幣別
		}
		
		params.put("BANK_TYPE", "");   //銀行代號類別
		params.put("PRCDT", params.get("PAYDATE"));   //付款日期
		

		//
		//付款人外匯議價號碼,左靠右補空白
		//(13-20)  8 碼為 交易密碼 MAC KEY SYNC
		//(21-36) 16 碼為 USERID  
		//
		//同幣別不會有 "BGROENO", 所以帶空白
		params.put("REFID", StrUtils.left(StrUtils.trim(params.get("BGROENO")) + StrUtils.repeat(" ", 12), 12));
		params.put("SYNC", StrUtils.trim(params.get("SYNC")));   // 8 碼
		params.put("USERID", StrUtils.left(params.get("USERID") + StrUtils.repeat(" ", 16), 16));  //議價編號
		
		params.put("PAYEE_ID", StrUtils.trim(params.get("BENID")));//收款人統一編號
		
		//收款人帳號 (11碼) + " " (9碼) + 收款人附言 (16 碼) 
		params.put("ACCTIDTO", StrUtils.trim(params.get("BENACC")) + StrUtils.repeat(" ", 9) + params.get("MEMO1")); 
		
		params.put("ACCTTP_TO", "");   //收款人帳戶類別
		
		params.put("FEEACCTID", params.get("COMMACC"));   //付款人手續費帳號
		
		params.put("FEEACT_TP", "");   //付款人手續費帳戶類別
		
		params.put("FEEACT_CRY", params.get("COMMCCY"));   //付款手續費帳戶幣別
		
		params.put("FEEBANK_TP", "");   //手續費銀行代號類別
		
		//params.put("TNSATION", resultF001.getFlatValues().get("ORGCOMM"));   //匯出匯款手續費金額
		//params.put("OTHERFEE", resultF001.getFlatValues().get("ORGCABLE"));   //郵電費
		
		params.put("TNSATION", resultF001.getFlatValues().get("COMMAMT"));   //匯出匯款手續費金額
		params.put("OTHERFEE", resultF001.getFlatValues().get("CABCHG"));   //郵電費
		
		params.put("DISFEE", resultF001.getFlatValues().get("COMMAMT"));   //折扣後手續費
		params.put("DISTOTH", resultF001.getFlatValues().get("CABCHG"));   //折扣後郵電費
		//params.put("RESENDFLAG", "");   //重送標記
		
		
		String str_First4Byte = "    ";
		String str_Flag = " "; 			
		if (! "0".equals(params.get("FGTRDATE"))) {   //若為預約交易			
			str_Flag = "Y";			
		}		
		
		if ("000B".equals(params.get("PCODE")))
			str_First4Byte = "FISC";		

		params.put("ACWTYPE", str_First4Byte + "            " + str_Flag);   //收款人帳戶類別				
		
		
		params.put("COMMCCY2", resultF001.getFlatValues().get("COMMCCY2"));   //入帳手續費幣別
		params.put("COMMAMT2", resultF001.getFlatValues().get("COMMAMT2"));   //入帳手續費金額
		params.put("SWFTCODE", resultF001.getFlatValues().get("M1SBBIC"));   //收款人 SWIFT CD
		params.put("BACH_MARK", "N");   //整批註記
		/* 20130805 因為要和行動整併所以將原本一行params.put("TXPSW", params.get("PINNEW"))改成如下判斷 */
		if(params.get("FGTXWAY").equals("3")){//交易機制為OTP
			params.put("XMLCA", "O");
			params.put("XMLCN", "OTP");
		}
		else {
			if("MB".equals(params.get("LOGINTYPE"))) {
//			20200914 edit by hugo __PINNEW 的長度如果超過48 表示是E2E BASE64  不適合打TMRA
				if(StrUtils.isNotEmpty(params.get("__PINNEW")) && (params.get("__PINNEW").length() <= 48 )) {
//					if(!"".equals(params.get("__PINNEW")) || null!=params.get("__PINNEW")) {
					params.put("TXPSW", params.get("__PINNEW"));   //押密後交易密碼  //for 舊MB
				}else {
					params.put("TXPSW", params.get("PINNEW"));   //押密後交易密碼  //for maybe 新MB
				}
			}else {
				params.put("TXPSW", params.get("PINNEW"));   //押密後交易密碼
			}
			
		}
		
		//壓 F003 的 MAC 欄位
		f003MAC(params);
		
//		外匯延長營業時間需求：當PCODE=000B，且營業時間超過3點30分後，不得執行交易回Z402		
//		B105 b105 = (B105)SpringBeanFactory.getBean("b105".toLowerCase());
		SYSPARAMDATA sysParam = (SYSPARAMDATA)b105.getSysParamData();		
		String str_CurrentTime = DateTimeUtils.getTimeShort(new Date());
		int i_MM = Integer.parseInt(sysParam.getADFXEMM()) + 5;
        String str_BizEnd = sysParam.getADFXEHH() + String.valueOf(i_MM) + sysParam.getADFXESS();
		
		if (str_CurrentTime.compareTo(str_BizEnd) > 0 && params.get("PCODE").toString().equals("000B")) {
			throw TopMessageException.create("Z402", (String)params.get("ADOPID"));
		}
				
		TelcommResult result = f003Telcomm.query(params);  //
		
		return result;
	}
	
	public TelcommResult doF005(Hashtable<String, String> params) {
		//TODO 組合F005電文所需要的欄位
		
		String tranOut = StrUtils.trim(params.get("CUSTACC"));
		String tranIn = StrUtils.trim(params.get("__TRIN_ACN"));
		if(tranOut.startsWith("893") && tranIn.startsWith("893"))
			params.put("PCODE", "000C");
		else if(!tranOut.startsWith("893") && !tranIn.startsWith("893"))
			params.put("PCODE", "000C");
		else
			params.put("PCODE", "000B");
		
		params.put("MSGID", "");
		//params.put("CUSTYPE", "1");   //TODO 之後會從 session 來
		
		params.put("CUSTACC", params.get("CUSTACC")); //轉出帳號
		params.put("BENACC", params.get("__TRIN_ACN")); //轉入帳號
		if("1".equals(params.get("PAYREMIT"))) {
			params.put("AMTTYPE", "1");//金額類別
			params.put("CURAMT", params.get("CURAMT"));//轉出金額 (議價金額)
		}
		else { // "2".equals(params.get("PAYREMIT"))
			params.put("AMTTYPE", "2");//金額類別
			params.put("ATRAMT", params.get("CURAMT"));//轉入金額			
		}
		
		/*
	 		如果電文失敗或寫DB發生任何錯誤, 則發 F007的電文
	 		但是  FLAG 內容為 C 的電文
		*/
//		外匯延長營業時間需求：當PCODE=000B，且營業時間超過3點30分後，不得執行交易回Z402		
//		B105 b105 = (B105)SpringBeanFactory.getBean("b105".toLowerCase());
		SYSPARAMDATA sysParam = (SYSPARAMDATA)b105.getSysParamData();		
		String str_CurrentTime = DateTimeUtils.getTimeShort(new Date());
		int i_MM = Integer.parseInt(sysParam.getADFXEMM()) + 5;
        String str_BizEnd = sysParam.getADFXEHH() + String.valueOf(i_MM) + sysParam.getADFXESS();
		
		if (str_CurrentTime.compareTo(str_BizEnd) > 0 && params.get("PCODE").toString().equals("000B")) {
			throw TopMessageException.create("Z402", (String)params.get("ADOPID"));
		}		
		
		TelcommResult result = f005Telcomm.query(params);  //
		
		return result;
	}
	
	public TelcommResult doF007C(Map<String, String> params) {
		//TODO 組合議價電文所需要的欄位
		params.put("FYACN", params.get("CUSTACC")); //轉出帳號
		params.put("INACN", (params.get("__TRIN_ACN").length()>11) ? params.get("__TRIN_ACN").substring(0,11) : params.get("__TRIN_ACN")); //轉入帳號

		params.put("OUT_CRY", params.get("PAYCCY")); //轉出幣別
		params.put("IN_CRY", params.get("REMITCY")); //轉入幣別
		if("1".equals(params.get("PAYREMIT"))) {
			params.put("AMTTYPE", "1");//金額類別
			params.put("CURAMT", params.get("CURAMT"));//轉出金額 (議價金額)						
		}
		else { // "2".equals(params.get("PAYREMIT"))
			params.put("AMTTYPE", "2");//金額類別
			params.put("ATRAMT", params.get("CURAMT"));//轉入金額
		}		
		params.put("FLAG", "C");  //交易狀態
		/*
	 		如果電文失敗或寫DB發生任何錯誤, 則發 F007的電文
	 		但是  FLAG 內容為 C 的電文
		*/
		
		
		TelcommResult result = f007Telcomm.query(params);  //議價電文
		
		return result;
	}
	
	public TelcommResult doF011(Map<String, String> params) {
		//TODO 組合F011電文所需要的欄位
		
		String tranOut = StrUtils.trim(params.get("CUSTACC"));
		String tranIn = StrUtils.trim(params.get("__TRIN_ACN"));
		if(tranOut.startsWith("893") && tranIn.startsWith("893"))
			params.put("PCODE", "000C");
		else if(!tranOut.startsWith("893") && !tranIn.startsWith("893"))
			params.put("PCODE", "000C");
		else
			params.put("PCODE", "000B");
		
		params.put("MSGID", "");
		//params.put("CUSTYPE", "1");   //TODO 之後會從 session 來
		
		
		params.put("CUSTACC", params.get("CUSTACC")); //轉出帳號
		params.put("BENACC", params.get("__TRIN_ACN")); //轉入帳號
		if("1".equals(params.get("PAYREMIT"))) {
			params.put("AMTTYPE", "1");//金額類別
			params.put("CURAMT", params.get("CURAMT"));//轉出金額 (議價金額)
		}
		else { // "2".equals(params.get("PAYREMIT"))
			params.put("AMTTYPE", "2");//金額類別
			params.put("ATRAMT", params.get("CURAMT"));//轉入金額
		}		
		
		/*
	 		如果電文失敗或寫DB發生任何錯誤, 則發 F007的電文
	 		但是  FLAG 內容為 C 的電文
		*/
//		外匯延長營業時間需求：當PCODE=000B，且營業時間超過3點30分後，不得執行交易回Z402		
//		B105 b105 = (B105)SpringBeanFactory.getBean("b105".toLowerCase());
		SYSPARAMDATA sysParam = (SYSPARAMDATA)b105.getSysParamData();		
		String str_CurrentTime = DateTimeUtils.getTimeShort(new Date());
		int i_MM = Integer.parseInt(sysParam.getADFXEMM()) + 5;
        String str_BizEnd = sysParam.getADFXEHH() + String.valueOf(i_MM) + sysParam.getADFXESS();
		
		if (str_CurrentTime.compareTo(str_BizEnd) > 0 && params.get("PCODE").toString().equals("000B")) {
			throw TopMessageException.create("Z402", (String)params.get("ADOPID"));
		}		
		TelcommResult result = f011Telcomm.query(params);   
		
		return result;
	}
	 

	/**
		有關FENB-003 電文押碼:
		 
		  將以下資料押碼                              
		1. 付款帳號          ACCTID       
		2. 付款人統一編號     CUSTPAY_ID   
		3. 付款人日期        PRCDT         
		4. 交易序號          STAN         
		 
		 
		 
		    25 '0' +    付款帳號   (11)    :    36
		   '000'   +   付款人統一編號 (8) :      11    ==>  ID 押碼遇字母轉為數字       A->1, B->2 , J->0, K->1...
		    付款人日期                     :     8
		    '0'   + 交易序號     (8)       :     9
		--------------------------------------------------------
		                                        64	
	 * @param params
	 */
	public static void f003MAC(Map<String, String> params) {
		String acctid = StrUtils.repeat("0", 25) + String.format("%-11s", params.get("ACCTID"));
		
		log.trace("f003MAC.acctid.length: {}", acctid.length());
//		log.debug("acctid length : " + acctid.length());
		
		String uid = params.get("CUSTPAY_ID");
		Pattern p = Pattern.compile("[A-Z]", Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
		
		Map<String, String> mv = new HashMap();
		mv.put("uid", uid);
		mv.put("stan", params.get("STAN"));  //STAN 的格式為 N00001
		
		//押碼遇字母轉為數字       A->1, B->2 , J->0, K->1...
		for(String key : mv.keySet()) {
			String value = mv.get(key);
			
			Matcher m = p.matcher(value);
			StringBuffer resultString = new StringBuffer();
			while(m.find()) {
				String eng = m.group().toLowerCase();
				int x = (eng.charAt(0) - 0x61 + 1) % 10;
				m.appendReplacement(resultString, x+"");
			}
			m.appendTail(resultString);
			
			mv.put(key, resultString.toString());
			//if("stan".equals(key))
//			System.out.printf("%s-> old: %s, new: %s\r\n", key, value, resultString.toString());
		}
		//統編有可能有7碼, 所以右補0
		String custpay_id = StrUtils.repeat("0", 3) + StrUtils.left(mv.get("uid") + "000000000", 8);  
		String stan =  "0" + mv.get("stan");  // 9 碼
		
		String prcdt = params.get("PRCDT");   // 8 碼
		log.debug("acctid.length: {}", acctid.length());
		log.debug("custpay_id.length: {}", custpay_id.length());
		log.debug("prcdt.length: {}",prcdt.length());
		log.debug("stan.length: {}", stan.length());
		
		String __mac64 = acctid + custpay_id + prcdt + stan;
		log.debug(" __mac64.length: {}", __mac64.length());
		log.debug(ESAPIUtil.vaildLog("__mac64: {} "+ __mac64));
		
		if(__mac64.length() != 64)
			throw new RuntimeException("錯誤的長度.");
		/*
		KeyClient client = new KeyClient();
		String result = KeyClientUtils.doRequest(
				IKeySpi.CREATE_MAC,
				KeyLabel.KEYLABEL_MACKEY, 
				client.toEBCDIC(StrUtils.left(StrUtils.right(DateTimeUtils.getCDateShort(new Date()), 6) + "00000000", 8).getBytes()), 
				client.toEBCDIC(__mac64.getBytes()), 
				"0128");
		
		if(!StrUtils.trim(result).startsWith("ERROR")) {
			try {
	    		params.put("MAC", result.substring(0, 8));
	    		params.put("MAC_SYNC", params.get("SYNC")); //4 碼
	    	}
	    	catch(RuntimeException e) {
	    		throw TopMessageException.create("Z300"); //押解碼錯誤 
	    	}
		}
		*/
		String result1="";
		try {
			result1 = VaCheck.getSYNC();
			if(result1!=null&&result1.length()>0) {
				try {
					log.debug("SYNC: {}", result1.substring(0, 8));
		    		params.put("SYNC", result1.substring(0, 8));
		    	}
		    	catch(RuntimeException e) {
		    		log.error("f003MAC.RuntimeException: ", e);
		    		throw TopMessageException.create("Z300"); //押解碼錯誤
		    	}
			}
			else {
				log.error("f003MAC.TopMessageException: Z300");
				throw TopMessageException.create("Z300"); //押解碼錯誤
			}			 
		}
		catch (Exception e)
		{
			log.error("getSYNC ERROR: ", e.getMessage());
		}				
		String result = "";
		try {			
				result = VaCheck.getMAC(__mac64);
				if(result!=null&&result.length()>0) {
					try {
						log.debug("MAC = : {}", result.substring(0, 8));
			    		params.put("MAC", result.substring(0, 8));
			    		params.put("MAC_SYNC", params.get("SYNC")); //4 碼
			    	}
			    	catch(RuntimeException e) {
			    		log.error("getMAC ERROR: ", e);
			    		log.error("getMAC ERROR: result substring error");
			    		throw TopMessageException.create("Z300"); //押解碼錯誤
			    	}
				}
				else {
					log.error("getMAC TopMessageException");
					log.error("getMAC ERROR: result size null or zero");
					throw TopMessageException.create("Z300"); //押解碼錯誤
				}							
		}
		catch (Exception e)
		{
			log.error("getMAC ERROR: ", e.getMessage());
			throw TopMessageException.create("Z300");
		}				
	}
	
	
	
	public TelcommResult doF007Y(Map _params) { 
		Map<String, String> params = _params;
		
		String fginacno = params.get("FGINACNO"); // 1 約定帳號, 2 非約定帳號
		
		//轉入帳號
		String trin_bnkcod = "";
		String trin_acn = "";
		String trin_agree = "";
		if("CMDAGREE".equals(fginacno)) {
			Map<String, String> jm = JSONUtils.json2map(params.get("INACNO1"));
			trin_bnkcod = jm.get("BNKCOD");
			trin_acn = jm.get("ACN");
			trin_agree = jm.get("AGREE");
			
		}
		else if("CMDISAGREE".equals(fginacno)) {  //非約定, 只能是台企銀 050
			trin_bnkcod = "050";
			trin_acn = params.get("INACNO2");
			trin_agree = "0";   // 0: 非約定, 1: 約定
		}
		
		params.put("__TRIN_BANKCOD", trin_bnkcod);
		params.put("__TRIN_ACN", trin_acn);
		params.put("__TRIN_AGREE", trin_agree);
		
		//TODO 組合議價電文所需要的欄位
		params.put("FYACN", params.get("CUSTACC")); //轉出帳號
		params.put("INACN", params.get("__TRIN_ACN")); //轉入帳號
		params.put("OUT_CRY", params.get("PAYCCY")); //轉出幣別
		params.put("IN_CRY", params.get("REMITCY")); //轉入幣別
		if("1".equals(params.get("PAYREMIT"))) {
			params.put("AMTTYPE", "1");//金額類別
			params.put("CURAMT", params.get("CURAMT"));//轉出金額 (議價金額)
						
			params.put("OUT_AMT", params.get("CURAMT"));//轉出金額 (議價金額) => 保留原始輸入欄位值			
		}
		else { // "2".equals(params.get("PAYREMIT"))
			params.put("AMTTYPE", "2");//金額類別
			params.put("ATRAMT", params.get("CURAMT"));//轉入金額
			
			params.put("IN_AMT", params.get("CURAMT"));//轉入金額	 => 保留原始輸入欄位值									
		}		
		
		params.put("FLAG", "Y");//交易狀態


		/*
		 	如果電文失敗或寫DB發生任何錯誤, 則發 F007的電文
		 	但是  FLAG 內容為 C 的電文
		 */
		
		
		TelcommResult result = f007Telcomm.query(params);  
		
		
		return result;
	}

	public boolean checkSuccessTopMsg(String topmsg) {
		//若回傳為 000000, 或者為 空白, 則回傳 true
		try {
			Double d = Double.parseDouble(topmsg);
			if(d == 0d)
				return true;
		}
		catch(Exception e) {}
		return "".equals(topmsg);
	}
	
	public Object writeLog(final TelCommExecWrapper execwrapper, final Map _params, final MVHImpl result) {
		Map<String,String> params = _params;
		DecimalFormat dft = new DecimalFormat("#0");
		DecimalFormat dftdot2 = new DecimalFormat("#0.00");

		// 建立交易紀錄log 並檢查是否需更新預約檔
		TXNFXRECORD record = execwrapper.createTxnFxRecordCheckupdatePayData(_params, txnFxSchPayDataDao);

		BOLifeMoniter.setValue("FXADTXNO", record.getADTXNO());	
		
		record.setADOPID("F001");
		
		record.setFXUSERID(params.get("UID"));
		record.setFXWDAC(params.get("CUSTACC"));  //轉出帳號
		record.setFXSVBH(params.get("__TRIN_BANKCOD"));  //轉入分行
		record.setFXSVAC(params.get("__TRIN_ACN"));  //轉入帳號
		record.setFXWDCURR(params.get("PAYCCY"));  //轉出幣別
		record.setFXSVCURR(params.get("REMITCY"));  //轉入幣別

		if (params.get("F001_ACCCUR") != null) {   

			try {
				record.setFXWDAMT(dftdot2.format(dft.parse(params.get("F001_ACCAMT")).doubleValue() / 100d));  // 轉出金額
				record.setFXSVAMT(dftdot2.format(dft.parse(params.get("F001_CURAMT")).doubleValue() / 100d));  // 轉入金額							
			} catch (Exception e) {
				log.error("",e);
			}
	
		} else {

			try {
				if (params.get("PAYREMIT").toString().equals("1")) {   // 轉帳類型為"轉出" 
					
					double d_Div = 1d;
					if (((String)params.get("OUT_AMT")).startsWith("0"))
						d_Div = 100d;								
					
					record.setFXWDAMT(dftdot2.format(dft.parse(params.get("OUT_AMT")).doubleValue() / d_Div));  // 轉出金額								
													
				}// end (PAYREMIT==1)
				
			} catch(Exception e) {
				log.error("",e);
			}
			
			try {	
				
				if (params.get("PAYREMIT").toString().equals("2")) {   // 轉帳類型為"轉入" 
					
					double d_Div = 1d;
					if (((String)params.get("IN_AMT")).startsWith("0"))
						d_Div = 100d;								
					
					record.setFXSVAMT(dftdot2.format(dft.parse(params.get("IN_AMT")).doubleValue() / d_Div));  // 轉入金額								
													
				}// end (PAYREMIT==2)

			} catch(Exception e){
				log.error("",e);
			}						
			
		}

		record.setFXTXMEMO(params.get("CMTRMEMO"));
		record.setFXTXMAILS(params.get("CMTRMAIL"));
		record.setFXTXMAILMEMO(params.get("CMMAILMEMO"));  //CMMAILMEMO 對應的欄位
		record.setFXEXRATE(StrUtils.trim(params.get("RATE")));//匯率

		try {
			String tmp1 = (params.get("DISFEE") == null ? "" : NumericUtil.formatNumberString(params.get("DISFEE"), 2));
			String tmp2 = (params.get("DISTOTH") == null ? "" : NumericUtil.formatNumberString(params.get("DISTOTH"), 2));
			record.setFXEFEE(tmp1 + "/" + tmp2);//手續費 + 郵電費		
			
			record.setFXEFEECCY(params.get("COMMCCY")); //手續費幣別
		} catch(Exception e){
			log.error("",e);
		}
		
		record.setFXTXCODE(params.get("FGTXWAY"));
		
		record.setFXMSGSEQNO(StrUtils.trim(params.get("__FXMSGSEQNO")));
		
		// 欄位長度過長資料庫塞不進去，故remove
		params.remove("pkcs7Sign"); 
		params.remove("jsondc");
		params.remove("__SIGN");
		params.remove("__F001_RESULT");
		params.remove("__F003_RESULT");
		record.setFXTITAINFO(CodeUtil.toJson(params));
		
		txnFxRecordDao.save(record);
		
		return record;
	}
	
	public Object writeLog_old(final TelCommExecWrapper execwrapper, final Map _params, final MVHImpl result) {
		Map<String,String> params = _params;
		DecimalFormat dft = new DecimalFormat("#0");
		DecimalFormat dftdot2 = new DecimalFormat("#0.00");
		
		TXNFXRECORD record = execwrapper.createTxnFxRecord();
		
		/*** For 存入 Txnlog ***/
		BOLifeMoniter.setValue("FXADTXNO", record.getADTXNO());	
		
		record.setADOPID("F001");
		
		record.setFXUSERID(params.get("UID"));
		record.setFXWDAC(params.get("CUSTACC"));  //轉出帳號
		record.setFXSVBH(params.get("__TRIN_BANKCOD"));  //轉入分行
		record.setFXSVAC(params.get("__TRIN_ACN"));  //轉入帳號
		record.setFXWDCURR(params.get("PAYCCY"));  //轉出幣別
		record.setFXSVCURR(params.get("REMITCY"));  //轉入幣別
		
//				record.setFXSCHNO(params.get("__SCHNO")==null?"" : params.get("__SCHNO"));
		
		if (params.get("F001_ACCCUR") != null) {   
			
			try {
				record.setFXWDAMT(dftdot2.format(dft.parse(params.get("F001_ACCAMT")).doubleValue() / 100d));  // 轉出金額
				record.setFXSVAMT(dftdot2.format(dft.parse(params.get("F001_CURAMT")).doubleValue() / 100d));  // 轉入金額							
			}
			catch (Exception e) {}
			
		}
		else {
			
			try {
				if (params.get("PAYREMIT").toString().equals("1")) {   // 轉帳類型為"轉出" 
					
					double d_Div = 1d;
					if (((String)params.get("OUT_AMT")).startsWith("0"))
						d_Div = 100d;								
					
					record.setFXWDAMT(dftdot2.format(dft.parse(params.get("OUT_AMT")).doubleValue() / d_Div));  // 轉出金額								
					
				}// end (PAYREMIT==1)
				
			}
			catch(Exception e) {}
			
			try {	
				
				if (params.get("PAYREMIT").toString().equals("2")) {   // 轉帳類型為"轉入" 
					
					double d_Div = 1d;
					if (((String)params.get("IN_AMT")).startsWith("0"))
						d_Div = 100d;								
					
					record.setFXSVAMT(dftdot2.format(dft.parse(params.get("IN_AMT")).doubleValue() / d_Div));  // 轉入金額								
					
				}// end (PAYREMIT==2)
				
			}
			catch(Exception e){}						
			
		}
		
		record.setFXTXMEMO(params.get("CMTRMEMO"));
		record.setFXTXMAILS(params.get("CMTRMAIL"));
		record.setFXTXMAILMEMO(params.get("CMMAILMEMO"));  //CMMAILMEMO 對應的欄位
		record.setFXEXRATE(StrUtils.trim(params.get("RATE")));//匯率
		
		try {
			String tmp1 = (params.get("DISFEE") == null ? "" : NumericUtil.formatNumberString(params.get("DISFEE"), 2));
			String tmp2 = (params.get("DISTOTH") == null ? "" : NumericUtil.formatNumberString(params.get("DISTOTH"), 2));
			record.setFXEFEE(tmp1 + "/" + tmp2);//手續費 + 郵電費		
			
			record.setFXEFEECCY(params.get("COMMCCY")); //手續費幣別
		}
		catch(Exception e){}
		
		record.setFXTXCODE(params.get("FGTXWAY"));
		
		record.setFXMSGSEQNO(StrUtils.trim(params.get("__FXMSGSEQNO")));
		
		
		TXNREQINFO titainfo = null;
		
		//if(record.getFXTITAINFO() == 0) {
		titainfo = new TXNREQINFO();
		
		titainfo.setREQINFO(BookingUtils.requestInfo(params));
		//}
		
		
		
//                String str_MsgCode = record.getFXEXCODE();                
//    			String fxFlag = admMsgCodeDao.findFXRESEND(str_MsgCode);
//    			
//    			//可人工重送
//				if (fxFlag.equals("Y"))	
//					record.setFXRETXSTATUS("5");
//				else
//					record.setFXRETXSTATUS("0");					
		
		
		/*** New Add ***/
		record.setFXMSGCONTENT((params.get("__FXMSGCONTENT")==null) ? "" : params.get("__FXMSGCONTENT"));			
		// 檢查是否需更新預約檔
		txnFxSchPayDataDao.checkupdateTxnFxSchPayData(params);
		
		txnFxRecordDao.writeTxnRecord(titainfo, record);
		
		return record;
	}
	
	/**
	 * 預約時用的
	 * @param params
	 * @return
	 */
	public TelcommResult doF007A(Map<String, String> params) {
		//TODO 組合議價電文所需要的欄位
		Date d = new Date();
		params.put("CMTRDATE", DateTimeUtils.format("yyyyMMdd", d));
		params.put("CMTRTIME", DateTimeUtils.format("HHmmss", d));
		
		params.put("FYACN", params.get("CUSTACC")); //轉出帳號
		params.put("INACN", params.get("__TRIN_ACN")); //轉入帳號
		params.put("OUT_CRY", params.get("PAYCCY")); //轉出幣別
		params.put("IN_CRY", params.get("REMITCY")); //轉入幣別
		if("1".equals(params.get("PAYREMIT"))) {
			params.put("AMTTYPE", "1");//金額類別
			params.put("CURAMT", params.get("CURAMT"));//轉出金額 (議價金額)			
			
			params.put("OUT_AMT", params.get("CURAMT"));//轉出金額 (議價金額) => 保留原始輸入欄位值				
		}
		else { // "2".equals(params.get("PAYREMIT"))
			params.put("AMTTYPE", "2");//金額類別
			params.put("ATRAMT", params.get("CURAMT"));//轉入金額
						
			params.put("IN_AMT", params.get("CURAMT"));//轉入金額	 => 保留原始輸入欄位值						
		}		
		params.put("FLAG", "A");//交易狀態
		/*
	 		如果電文失敗或寫DB發生任何錯誤, 則發 F007的電文
	 		但是  FLAG 內容為 C 的電文
		*/	
			
		TopMessageException lastException = null;		
		TelcommResult result = null;  //議價電文

		try {
			result = f007Telcomm.query(params);  //議價電文
		}
		catch(Exception e) {			
			if(e instanceof TopMessageException) {
				lastException = (TopMessageException)e;
				throw lastException;
			}
			else {
				throw new ToRuntimeException(e.getMessage(), e);
			}
		}
				
//		if(! checkSuccessTopMsg(result.getFlatValues().get("MSG_CODE"))) {
//			throw new TopMessageException(result.getFlatValues().get("MSG_CODE"), "", "");			
//		}		
//		
//		
//		params.put("RATE", result.getValueByFieldName("RATE"));
//		
//		/*** New Add ***/
//		double dd;
//		if("1".equals(params.get("PAYREMIT"))) {   //轉出
//
//				if ("TWD".equals(params.get("OUT_CRY")))
//					dd = new Double(params.get("CURAMT")) / new Double(params.get("RATE"));					
//				else	
//					dd = new Double(params.get("CURAMT")) * new Double(params.get("RATE"));
//				
//				result.getFlatValues().put("CURAMT", params.get("CURAMT"));  // 轉出金額
//				result.getFlatValues().put("ATRAMT", dd + "");  // 轉入金額
//			
//		}
//		else { // "2".equals(params.get("PAYREMIT"))   //轉入
//			
//				if ("TWD".equals(params.get("OUT_CRY")))
//					dd = new Double(params.get("ATRAMT")) * new Double(params.get("RATE"));			
//				else	
//					dd = new Double(params.get("ATRAMT")) / new Double(params.get("RATE"));		
//						
//				result.getFlatValues().put("CURAMT", dd + "");  // 轉出金額
//				result.getFlatValues().put("ATRAMT", params.get("ATRAMT"));  // 轉入金額			
//		}
		
		return result;	
	}
		
	/**
	 *  檢查電文回應之訊息代碼: 
	 *  
	 *  1.若存在於資料庫 && 註記可人工重送("Y") ==> 進人工重送處理
	 *  2.其它 ==> 一律交易結束			 
	 */	
	public boolean checkTopMsg(String topmsg) {
		
		boolean isFail = false;
		
		String fxFlag = admMsgCodeDao.findFXRESEND(topmsg);
		String fxAutoFlag = admMsgCodeDao.findFXAUTORESEND(topmsg);

		if (TopMessageException.isInDB(topmsg) && (fxFlag.equals("Y") || fxAutoFlag.equals("Y")) ) {
			isFail = false;  //進系統/人工重送處理			
		}
		else {
			isFail = true;	 //交易結束
		}

		return isFail;
	}
	
	/**
	 *  檢查電文回應之訊息代碼: 
	 *  
	 *  1.若存在於資料庫 && 註記可自動重送("Y") ==> 進自動重送處理
	 *  2.若存在於資料庫 && 註記可人工重送("Y") ==> 進人工重送處理
	 *  3.其它 ==> 一律交易結束			 
	 */	
	public boolean checkTopMsg(String topmsg, String schFlag) {
		
		boolean isFail = false;
		
		String fxFlag = admMsgCodeDao.findFXRESEND(topmsg);
		String fxAutoFlag = admMsgCodeDao.findFXAUTORESEND(topmsg);

		log.trace(ESAPIUtil.vaildLog("F001T.checkTopMsg().schFlag: {}"+ schFlag));
//		log.debug("@@@ F001T.checkTopMsg() schFlag == "+schFlag);
		
		//即時交易
		if (schFlag == null) { 
			if (TopMessageException.isInDB(topmsg) && fxFlag.equals("Y") ) {
				isFail = false;  //進人工重送處理			
			}
			else {
				isFail = true;	 //交易結束
			}			
		}
		//預約交易
		else { 			
			if (TopMessageException.isInDB(topmsg) && fxAutoFlag.equals("Y") ) {
				isFail = false;  //進自動重送處理			
			}
			else {
				isFail = true;	 //交易結束
			}						
		}

		return isFail;
	}			
}
