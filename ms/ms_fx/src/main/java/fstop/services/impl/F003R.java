package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.orm.dao.TxnFxRecordDao;
import fstop.orm.dao.TxnReqInfoDao;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;
import topibs.utility.NumericUtil;

/**
 * 外匯線上解款-取得議價部位
 * @author Owner
 *
 */
@Slf4j
public class F003R extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("f007Telcomm")
	private TelCommExec f007Telcomm;

	@Autowired
	private TxnFxRecordDao txnFxRecordDao;

	@Autowired
	private TxnReqInfoDao txnReqInfoDao;
	
//	@Required
//	public void setTxnReqInfoDao(TxnReqInfoDao txnReqInfoDao) {
//		this.txnReqInfoDao = txnReqInfoDao;
//	}
//
//	
//	@Required
//	public void setF007Telcomm(TelCommExec telcomm) {
//		f007Telcomm = telcomm;
//	}
//
//	@Required
//	public void setTxnFxRecordDao(TxnFxRecordDao txnFxRecordDao) {
//		this.txnFxRecordDao = txnFxRecordDao;
//	}

	/**
	 * 取得議價部位
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params) { 

		Map<String, String> params = _params;

		return online(params);
	}
	
	
	public TelcommResult online(Map<String, String> params) {
		//TODO 組合議價電文所需要的欄位
		Date d = new Date();
		params.put("CMTRDATE", DateTimeUtils.format("yyyyMMdd", d));
		params.put("CMTRTIME", DateTimeUtils.format("HHmmss", d));
		
		params.put("INACN", params.get("BENACC")); //轉入(解款)帳號
		params.put("OUT_CRY", params.get("TXCCY")); //轉出幣別
		params.put("IN_CRY", params.get("RETCCY")); //轉入(解款)幣別
		params.put("AMTTYPE", "1");//金額類別
		params.put("CURAMT", NumericUtil.formatNumberString(params.get("TXAMT"), 2).replaceAll(",", "")); //轉出(解款)金額	
		params.put("FLAG", "R");//交易狀態
		/*
	 		如果電文失敗或寫DB發生任何錯誤, 則發 F007的電文
	 		但是  FLAG 內容為 C 的電文
		*/	
		
		params.put("BGROENO", "");
		
		TopMessageException lastException = null;		
		TelcommResult result = null;  //議價電文
		
		try {
			result = f007Telcomm.query(params);  //議價電文
		}
		catch(Exception e) {
			if(e instanceof TopMessageException) {
				lastException = (TopMessageException)e;
				throw lastException;
			}		
		}
		
		if(! checkSuccessTopMsg(result.getFlatValues().get("MSG_CODE"))) {
			throw new TopMessageException(result.getFlatValues().get("MSG_CODE"), "", "");			
		}		
		

		params.put("BGROENO", result.getValueByFieldName("BGROENO"));  //議價編號
		params.put("RATE", result.getValueByFieldName("RATE"));  //匯率

		double dd;
//		if("1".equals(params.get("PAYREMIT"))) {   //轉出
//
				if ("TWD".equals(params.get("OUT_CRY")))
					dd = new Double(params.get("CURAMT")) / new Double(params.get("RATE"));					
				else	
					dd = new Double(params.get("CURAMT")) * new Double(params.get("RATE"));
				
				result.getFlatValues().put("CURAMT", params.get("CURAMT"));  // 轉出金額
				result.getFlatValues().put("ATRAMT", dd + "");  // 轉入金額
//			
//		}
//		else { // "2".equals(params.get("PAYREMIT"))   //轉入
//			
//				if ("TWD".equals(params.get("OUT_CRY")))
//					dd = new Double(params.get("ATRAMT")) * new Double(params.get("RATE"));			
//				else	
//					dd = new Double(params.get("ATRAMT")) / new Double(params.get("RATE"));		
//						
//				result.getFlatValues().put("CURAMT", dd + "");  // 轉出金額
//				result.getFlatValues().put("ATRAMT", params.get("ATRAMT"));  // 轉入金額			
//		}
		
		return result;
	}
	
	private boolean checkSuccessTopMsg(String topmsg) {
		//若回傳為 000000, 或者為 空白, 則回傳 true
		try {
			Double d = Double.parseDouble(topmsg);
			if(d == 0d)
				return true;
		}
		catch(Exception e) {}
		return "".equals(topmsg);
	}	
	
}
