package fstop.services.impl;

import fstop.model.*;
import fstop.orm.dao.AdmKeyValueDao;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class N130 extends CommonService {

	@Autowired
	@Qualifier("n130Telcomm")
	private TelCommExec n130Telcomm;
	@Autowired
	@Qualifier("n133Telcomm")
	private TelCommExec n133Telcomm;
	@Autowired
	private AdmKeyValueDao admKeyValueDao;
//
//	@Required
//	public void setN130Telcomm(TelCommExec telcomm) {
//		n130Telcomm = telcomm;
//	}
//
//	@Required
//	public void setN133Telcomm(TelCommExec telcomm) {
//		n133Telcomm = telcomm;
//	}
//
//	@Required
//	public void setAdmKeyValueDao(AdmKeyValueDao dao) {
//		admKeyValueDao = dao;
//	}

//	@Override
//	public String getActionMethod(Hashtable<String, String> params) {
//		/**
//		 * 若為空白, 則使用 N133 電文查詢全部帳號
//		 * 有帳號, 則使用 N523 來查
//		 */
//		return (!StrUtils.isEmpty(StrUtils.trim(params.get("ACN"))) ? "doN130Telcomm" : "doN133Telcomm");
//	}

//	private TelCommExec actionTelcomm;
//	private String actionDesc = "";

	/**
	 * 查詢指定帳號
	 * @param params
	 * @return
	 */
//	public MVH doN130Telcomm(Hashtable params) {
//		return getMVH(params);
//	}
	/**
	 * 查詢全部帳號
	 * @param params
	 * @return
	 */
//	public MVH doN133Telcomm(Hashtable params) {
//		return getMVH(params);
//	}

	/**
	 *
	 * @param params
	 * @return
	 */

	@Override
	public MVH doAction(Map params) {

		String period = (String)params.get("FGPERIOD");
		String enddate = "";
		String stadate = "";
		String periodStr = "";
		if("CMTODAY".equals(period)) { //今日
			stadate = DateTimeUtils.getCDateShort(new Date());
			enddate = stadate;
			periodStr = DateTimeUtils.format("yyyy/MM/dd", new Date());
		} else if("CMCURMON".equals(period)) { //本月
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.DATE, 1);
			Date dstart = cal.getTime();

			Date dend = new Date();
			stadate = DateTimeUtils.getCDateShort(dstart);
			enddate = DateTimeUtils.getCDateShort(dend);
			periodStr = DateTimeUtils.format("yyyy/MM/dd", dstart) + "~" + DateTimeUtils.format("yyyy/MM/dd", dend);
		} else if("CMLASTMON".equals(period)) { //上月
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.DATE, 1);
			cal.add(Calendar.MONTH, -1);
			Date dstart = cal.getTime();
			cal.add(Calendar.MONTH, 1);
			cal.add(Calendar.DATE, -1);
			Date dend = cal.getTime();
			stadate = DateTimeUtils.getCDateShort(dstart);
			enddate = DateTimeUtils.getCDateShort(dend);
			periodStr = DateTimeUtils.format("yyyy/MM/dd", dstart) + "~" + DateTimeUtils.format("yyyy/MM/dd", dend);
		} else if("CMNEARMON".equals(period)) { //最近一個月
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.MONTH, -1);
			Date dstart = cal.getTime();
			Date dend = new Date();
			stadate = DateTimeUtils.getCDateShort(dstart);
			enddate = DateTimeUtils.getCDateShort(dend);
			periodStr = DateTimeUtils.format("yyyy/MM/dd", dstart) + "~" + DateTimeUtils.format("yyyy/MM/dd", dend);
		} else if("CMLASTWEEK".equals(period)) { //最近一星期
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -7);
			Date dstart = cal.getTime();
			Date dend = new Date();
			stadate = DateTimeUtils.getCDateShort(dstart);
			enddate = DateTimeUtils.getCDateShort(dend);
			periodStr = DateTimeUtils.format("yyyy/MM/dd", dstart) + "~" + DateTimeUtils.format("yyyy/MM/dd", dend);
		} else if("CMPERIOD".equals(period)) { //指定日期
			stadate = (String)params.get("CMSDATE");
			enddate = (String)params.get("CMEDATE");
			stadate = StrUtils.trim(stadate).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
			enddate = StrUtils.trim(enddate).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
			if(stadate != null && stadate.length() == 8) {
				Date sd = DateTimeUtils.parse("yyyyMMdd", stadate);
				stadate = DateTimeUtils.getCDateShort(sd);
				periodStr = DateTimeUtils.format("yyyy/MM/dd", sd);
			}
			if(enddate != null && enddate.length() == 8) {
				Date sd = DateTimeUtils.parse("yyyyMMdd", enddate);
				enddate = DateTimeUtils.getCDateShort(sd);
				periodStr += "~" + DateTimeUtils.format("yyyy/MM/dd", sd);
			}
			else { //若未指定結束時間, 則使用今天日期
				enddate = DateTimeUtils.getCDateShort(new Date());
				periodStr += "~" + DateTimeUtils.format("yyyy/MM/dd", new Date());
			}
		}
		params.put("STADATE", stadate);
		params.put("ENDDATE", enddate);

		TelCommExec actionTelcomm = (!StrUtils.isEmpty(StrUtils.trim((String)params.get("ACN"))) ? n130Telcomm : n133Telcomm);

		TelcommResult telcommResult = actionTelcomm.query(params);
		telcommResult.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		telcommResult.getFlatValues().put("CMPERIOD", periodStr);
		telcommResult.getFlatValues().put("CMRECNUM", telcommResult.getValueOccurs("ACN") + "");

		RowsSelecter rowSelecter = new RowsSelecter(telcommResult.getOccurs());
		rowSelecter.each(new EachRowCallback(){

			public void current(Row row) {

				if("D".equalsIgnoreCase(row.getValue("CODDBCR"))) {
					row.setValue("DPWDAMT", row.getValue("AMTTRN"));
				}
				if("C".equalsIgnoreCase(row.getValue("CODDBCR"))) {
					row.setValue("DPSVAMT", row.getValue("AMTTRN"));
				}

				//使用 adm key value 轉換 memo
				String memo = row.getValue("MEMO");
				String r = "";
						
 				if("ACH".equalsIgnoreCase(row.getValue("TRNBRH")) && memo.matches("\\d{3}"))
				{
					r = admKeyValueDao.translator("MEMOACH", memo);	//20090924新增收付行為ACH且memo為三位整數之對映
				}
				else {
					r = admKeyValueDao.translator("MEMO", memo);
				}
				row.setValue("MEMO_C", r);
			}});
		Map<String, IMasterValuesHelper> tables = MVHUtils.groupByToTables(telcommResult, new String[]{"ACN"});
		MVHUtils.addTables(telcommResult, tables);

		return telcommResult;
	}
	
	

}
