package fstop.services.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.util.ESAPIUtil;

import topibs.utility.NumericUtil;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.orm.dao.TxnReqInfoDao;
import fstop.orm.dao.TxnTwRecordDao;
import fstop.orm.po.ADMMAILLOG;
import fstop.orm.po.TXNREQINFO;
import fstop.orm.po.TXNTWRECORD;
import fstop.services.BookingUtils;
import fstop.services.CommonService;
import fstop.services.DispatchService;
import fstop.services.NoticeInfo;
import fstop.services.NoticeInfoGetter;
import fstop.services.OnLineTransferNotice;
import fstop.services.ServiceBindingException;
import fstop.services.TelCommExecWrapper;
import fstop.services.WriteLogInterface;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 變更線上更改通訊地址、電話
 * @author Owner
 *
 */
@Slf4j
public class N570 extends DispatchService {
//	private Logger logger = Logger.getLogger(getClass());
	
//	private CommonService n570;

	@Autowired
    @Qualifier("n570Telcomm")
	private TelCommExec n570Telcomm;

	@Autowired
    @Qualifier("n571ExecTelcomm")
	private TelCommExec n571ExecTelcomm;
	
//	private TxnTwRecordDao txnTwRecordDao;
//	
//	private TxnReqInfoDao txnReqInfoDao;
	
//	@Required
//	public void setTxnReqInfoDao(TxnReqInfoDao txnReqInfoDao) {
//		this.txnReqInfoDao = txnReqInfoDao;
//	}
//	
//	@Required
//	public void setN570(CommonService service) {
//		n570 = service;
//	}
//	
//	@Required
//	public void setN570Telcomm(TelCommExec telcomm) {
//		n570Telcomm = telcomm;
//	}
//	@Required
//	public void setN571ExecTelcomm(TelCommExec telcomm) {
//		n571ExecTelcomm = telcomm;
//	}
//	
//	@Required
//	public void setTxnTwRecordDao(TxnTwRecordDao txnTwRecordDao) {
//		this.txnTwRecordDao = txnTwRecordDao;
//	}
	
	@Override
	public String getActionMethod(Map<String, String> params) {
		String txid = StrUtils.trim(params.get("TXID"));
		log.debug(ESAPIUtil.vaildLog("XXXX txid: " + txid));
		if("N570_1".equals(txid))
			return "doN570_1";
		else if("N570_2".equals(txid))
			return "doN570_2";
		else 
			throw new ServiceBindingException("不支援的 TXID [" + txid+"].");
	}
	@SuppressWarnings("unchecked")
	public MVH doN570_1(Map params) { 
		log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Do N570_1");
		
		TelcommResult result = n570Telcomm.query(params);
		
		return result;
	}

	@SuppressWarnings("unchecked")
	public MVH doN570_2(Map params) { 
		log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Do N570_2");

		MVHImpl result = new MVHImpl();
			
		final TelCommExecWrapper execwrapper = new TelCommExecWrapper(n571ExecTelcomm);
		result = execwrapper.query(params);

		return result;
	}
}
