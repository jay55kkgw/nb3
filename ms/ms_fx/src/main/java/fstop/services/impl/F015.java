package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class F015 extends CommonService {

	@Autowired
	@Qualifier("f015Telcomm")
	private TelCommExec f015Telcomm;

	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) {

		TelcommResult telcommResult = f015Telcomm.query(params);
		telcommResult.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));

		return telcommResult;
	}

}
