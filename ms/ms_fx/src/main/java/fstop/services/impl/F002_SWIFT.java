package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.orm.dao.TxnFxRecordDao;
import fstop.orm.po.TXNFXRECORD;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class F002_SWIFT extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("f007Telcomm")
	private TelCommExec f007Telcomm;

	@Autowired
	private TxnFxRecordDao txnFxRecordDao;
	
//	@Required
//	public void setF007Telcomm(TelCommExec telcomm) {
//		f007Telcomm = telcomm;
//	}
//
//	@Required
//	public void setTxnFxRecordDao(TxnFxRecordDao txnFxRecordDao) {
//		this.txnFxRecordDao = txnFxRecordDao;
//	}

	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params) { 
		Map<String, String> params = _params;
		
		String fginacno = params.get("FGINACNO"); // 1 約定帳號, 2 非約定帳號
		
		//轉入帳號
		String trin_bnkcod = "";
		String trin_acn = "";
		String trin_agree = "";
		if("CMDAGREE".equals(fginacno)) {
			Map<String, String> jm = JSONUtils.json2map(params.get("INACNO1"));
			trin_bnkcod = jm.get("BNKCOD");
			trin_acn = jm.get("ACN");
			trin_agree = jm.get("AGREE");
			
		}
		else if("CMDISAGREE".equals(fginacno)) {  //非約定, 只能是台企銀 050
			trin_bnkcod = "050";
			trin_acn = params.get("INACNO2");
			trin_agree = "0";   // 0: 非約定, 1: 約定
		}
		
		params.put("__TRIN_BANKCOD", trin_bnkcod);
		params.put("__TRIN_ACN", trin_acn);
		params.put("__TRIN_AGREE", trin_agree);
				
		
		
		
		//TODO 組合議價電文所需要的欄位
		params.put("FYACN", params.get("CUSTACC")); //轉出帳號
		params.put("INACN", params.get("__TRIN_ACN")); //轉入帳號
		params.put("OUT_CRY", params.get("PAYCCY")); //轉出幣別
		params.put("IN_CRY", params.get("REMITCY")); //轉入幣別
		if("1".equals(params.get("PAYREMIT"))) {
			params.put("AMTTYPE", "1");//金額類別
			params.put("CURAMT", params.get("CURAMT"));//轉出金額 (議價金額)
		}
		else { // "2".equals(params.get("PAYREMIT"))
			params.put("AMTTYPE", "2");//金額類別
			params.put("ATRAMT", params.get("CURAMT"));//轉入金額
		}		
		params.put("FLAG", "N");//交易狀態 
		/*
	 		如果電文失敗或寫DB發生任何錯誤, 則發 F007的電文
	 		但是  FLAG 內容為 C 的電文
		 */
		TelcommResult result = f007Telcomm.query(params);  //議價電文
		//TODO:findUniqueBy的第一個參數暫時先填入TXNFXRECORD.class
		TXNFXRECORD record = this.txnFxRecordDao.findUniqueBy(TXNFXRECORD.class, "ADTXNO",  params.get("ADTXNO"));
		//record.setADTXNO(DateTimeUtils.format("yyyyMMddHHmmss" + params.get("UID"), new Date()));
		String f = params.get("FGTRDATE");  //判斷即時還是預約
		
		Date d = new Date();
		record.setFXTXDATE(DateTimeUtils.format("yyyyMMdd", d));
		record.setFXTXTIME(DateTimeUtils.format("HHmmss", d));

		record.setFXUSERID(params.get("UID"));
		record.setFXWDAC(params.get("CUSTACC"));  //轉出帳號
		record.setFXSVBH(params.get("__TRIN_BANKCOD"));  //轉入分行
		record.setFXWDAC(params.get("__TRIN_ACN"));  //轉入帳號
		record.setFXWDCURR(params.get("PAYCCY"));  //轉出幣別
		record.setFXSVCURR(params.get("REMITCY"));  //轉入幣別

		if("1".equals(params.get("PAYREMIT"))) {
			record.setFXWDAMT(params.get("FXTRCURRENCY"));  // 轉出金額
		}
		else { // "2".equals(params.get("PAYREMIT"))
			record.setFXSVAMT(params.get("FXTRCURRENCY"));  // 轉入金額
		}
		 
		record.setFXTXMEMO(params.get("CMTRMEMO"));
		record.setFXTXMAILS(params.get("CMTRMAIL"));
		record.setFXTXMAILMEMO(params.get("CMMAILMEMO"));  //CMMAILMEMO 對應的欄位

		//params.put("BGROENO", result.getValueByFieldName("BGROENO"));  //議價編號
		//params.put("RATE", result.getValueByFieldName("RATE"));  //匯率
		//record.setFXTXINFO( getTITAInfo(params));
		//record.setADOPID(params.get("TXID"));

		//這時候還不用記
		record.setFXTXCODE("N");
		
		record.setFXMSGSEQNO("A");
		
		txnFxRecordDao.save(record);
		
		result.getFlatValues().put("ADTXNO", record.getADTXNO());
		return result;
	}



}
