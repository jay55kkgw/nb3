package fstop.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.rest.model.CommonPools;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.model.IMasterValuesHelper;
import fstop.exception.TopMessageException;
import fstop.model.EachRowCallback;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.MVHUtils;
import fstop.model.Row;
import fstop.model.Rows;
import fstop.model.RowsSelecter;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N650 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("n650Telcomm")
	private TelCommExec n650Telcomm;

	@Autowired
	private CommonPools commonPools;
//	@Required
//	public void setN650Telcomm(TelCommExec telcomm) {
//		n650Telcomm = telcomm;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params) {
		final Map<String, String> params = _params;
		
		MVHImpl result = new MVHImpl();
		
		queryAll(getAcnoList(params), params, result);
		
//		if(StrUtils.isEmpty((String)params.get("ACN"))) {
//			//查詢全部帳號
//			
//		}
//		else {
//			MVHImpl resultN650 = n650Telcomm.query(params);
//			resultN650.getOccurs().setValue("ACN", params.get("ACN"));
//			result.addTable(resultN650, params.get("ACN"));
//		
//		}
		
		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		result.getFlatValues().put("CMRECNUM", MVHUtils.sumTablesOccurs(result) + "");

		return result;
	}

	private List<String> getAcnoList(Map<String, String> params) {
		List<String> acnos = new ArrayList();
		if(StrUtils.isEmpty((String)params.get("ACN"))) {
			
			IMasterValuesHelper acnosMVH = commonPools.n920.getAcnoList(params.get("CUSIDN"), new String[]{"07"}, "");
			
			int requestCount = acnosMVH.getValueOccurs("ACN");
			for(int i=0; i < requestCount; i++) {
				String acn = acnosMVH.getValueByFieldName("ACN", i+1);
				acnos.add(acn);
			}
			
		}
		else {
			acnos.add(params.get("ACN"));
		}
		log.debug(ESAPIUtil.vaildLog("AcnoList >> {}"+ CodeUtil.toJson(acnos)));
		return acnos;
	}	
	
	
	public void queryAll(List<String> acnoList, Map<String, String> params, MVHImpl resultMVH) {
 		//IMasterValuesHelper acnos = CommonPools.n920.getAcnoList(params.get("UID"), new String[]{"07"}, "");

		List<String> currAcnoList = new ArrayList<String>();
		String doneAcnos = params.get("DONEACNOS");
		String[] doneAcnoList = null;
		if (doneAcnos == null)
			doneAcnoList = new String[0];
		else
			doneAcnoList = doneAcnos.split(",");
		
		//Rows rows = new Rows();
 		//int requestCount = acnos.getValueOccurs("ACN");
 		for(String acn : acnoList) {
 			
 			currAcnoList.add(acn);
 			
 			//已執行過的ACNO不重複執行
 			boolean hasDone = false;
 			for (int i = 0; i < doneAcnoList.length; i++)
 			{
 				if (doneAcnoList[i].equals(acn))
 				{
 					hasDone = true;
 					break;
 				}
 			}
 			if (hasDone)
 				continue;
 			
 			//String acn = acnos.getValueByFieldName("ACN", i+1);
 			params.put("ACN", acn);
 			try {
	 			MVHImpl result = n650Telcomm.query(params);
	 			params.remove("QUERYNEXT");	//一個QUERYNEXT只能用於一個ACNO
	 			result.getOccurs().setValue("ACN", acn);
	 			
	 			resultMVH.addTable(result, acn);
	 			
	 			String queryNext = result.getValueByFieldName("QUERYNEXT");
	 			if (!queryNext.equals(""))
	 			{
	 				resultMVH.getFlatValues().put("QUERYNEXT", queryNext);
	 				
	 				currAcnoList.remove(currAcnoList.size() - 1);
	 				StringBuffer sb = new StringBuffer();
	 				if (currAcnoList.size() > 0)
	 				{
		 		 		sb.append(currAcnoList.get(0));
		 		 		for (int i = 1; i < currAcnoList.size(); i++)
		 		 		{
		 		 			sb.append("," + currAcnoList.get(i));
		 		 		}
	 				}
	 		 		resultMVH.getFlatValues().put("DONEACNOS", sb.toString());
	 				
	 				break;	 				
	 			}

				// 20190402 JeffChang 為了得到回傳值
				resultMVH.getFlatValues().putAll(result.getFlatValues());
			}
			catch(TopMessageException ex) {
				MVHImpl helper = new MVHImpl();
				helper.getFlatValues().put("ACN", acn);
				helper.getFlatValues().put("TOPMSG", ex.getMsgcode());
				helper.getFlatValues().put("ADMSGIN", ex.getMsgin());
				helper.getFlatValues().put("ADMSGOUT", ex.getMsgout());
				resultMVH.addTable(helper, acn);
			}
 			
 		}
		//return new MVHImpl(rows);
	}

}
