package fstop.services.impl;

import java.util.Date;
import java.util.Hashtable;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import fstop.model.EachRowCallback;
import fstop.model.GrpColumns;
import fstop.model.IMasterValuesHelper;
import fstop.model.MVH;
import fstop.model.MVHUtils;
import fstop.model.Row;
import fstop.model.RowsGroup;
import fstop.model.RowsSelecter;
import fstop.model.TelcommResult;
import fstop.orm.dao.AdmKeyValueDao;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;

@Slf4j
public class N566 extends CommonService  {



	@Autowired
    @Qualifier("n566Telcomm")
	private TelCommExec n566Telcomm;
	
	private AdmKeyValueDao admKeyValueDao;


	@Override
	public MVH doAction(Map params) {
		String stadate = (String) params.get("CMSDATE");
		String enddate = (String) params.get("CMEDATE");

		stadate = StrUtils.trim(stadate).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
		enddate = StrUtils.trim(enddate).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
		
		String periodStr = "";
		if (stadate != null && stadate.length() == 8) {
			Date sd = DateTimeUtils.parse("yyyyMMdd", stadate);
			stadate = DateTimeUtils.format("yyyy-MM-dd", sd);
			periodStr = DateTimeUtils.format("yyyy/MM/dd", sd);
		}
		if (enddate != null && enddate.length() == 8) {
			Date sd = DateTimeUtils.parse("yyyyMMdd", enddate);
			enddate = DateTimeUtils.format("yyyy-MM-dd", sd);
			periodStr += "~" + DateTimeUtils.format("yyyy/MM/dd", sd);
		} else { // 若未指定結束時間, 則使用今天日期
			enddate = DateTimeUtils.getCDateShort(new Date());
			periodStr += "~" + DateTimeUtils.format("yyyy/MM/dd", new Date());
		}
		params.put("FDATE", stadate);
		params.put("TDATE", enddate);
				
		//繼續查詢FLAG
		if ("TRUE".equals(params.get("OKOVNEXT"))) {
			params.put("QUERYNEXT", "");
		}

		TelcommResult telcommResult = n566Telcomm.query(params);

		//查詢時間
		telcommResult.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));

		//查詢期間
		telcommResult.getFlatValues().put("CMPERIOD", periodStr);

		//資料總數
		telcommResult.getFlatValues().put("CMRECNUM", telcommResult.getValueOccurs("ACN") + "");

		//帳號
		telcommResult.getFlatValues().put("ACN", telcommResult.getValueByFieldName("ACN", 1));

		RowsSelecter rowSelecter = new RowsSelecter(telcommResult.getOccurs());
		
		rowSelecter.each(new EachRowCallback(){
		public void current(Row row) {			
			//使用 adm key value 轉換 memo
//			String r = admKeyValueDao.translator("MEMO", row.getValue("MEMO"));
//			row.setValue("MEMO", r);
		}});

		Map<String, IMasterValuesHelper> tables = MVHUtils.groupByToTables(telcommResult, new String[]{"ACN"});

		MVHUtils.addTables(telcommResult, tables);

		try {
			String[] groupBy = new String[]{"RREMITCY"};
			//RowsGroup grp = MVHUtils.group(telcommResult.getOccurs(), groupBy);
			RowsGroup grp = MVHUtils.group(telcommResult.getOccursByUID("CUSIDN", (String)params.get("UID")), groupBy);
			
			Map<GrpColumns, Double> sum = grp.sum("RREMITAM");
			int ind = 1;
			for(Entry<GrpColumns, Double> ent : sum.entrySet()) {
				GrpColumns grpCols = ent.getKey();
	
				Map<String, String> m = MVHUtils.toFlatValues(grpCols, "AMT", ind);
				telcommResult.getFlatValues().putAll(m);
				telcommResult.getFlatValues().put("FXSUBAMT_" + ind, ent.getValue() + "");				
				telcommResult.getFlatValues().put("FXSUBAMTRECNUM_" + ind, grp.getGroups().get(grpCols).getSelectedRows().size() + "");
				
				ind++;
			}
		}
		finally {}
		
		
		return telcommResult;
	}


}
