package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.services.DispatchService;
import fstop.services.ServiceBindingException;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class N178 extends DispatchService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("n530Service")
	private CommonService n530;

	@Autowired
    @Qualifier("n178_1Telcomm")
	private TelCommExec n178_1Telcomm;

	@Autowired
    @Qualifier("n178_2Telcomm")
	private TelCommExec n178_2Telcomm;
	
//	@Required
//	public void setN530(CommonService service) {
//		n530 = service;
//	}
//	
//	@Required
//	public void setN178_1Telcomm(TelCommExec telcomm) {
//		n178_1Telcomm = telcomm;
//	}
//
//	@Required
//	public void setN178_2Telcomm(TelCommExec telcomm) {
//		n178_2Telcomm = telcomm;
//	}
	
	@Override
	public String getActionMethod(Map<String, String> params) {
		String txid = StrUtils.trim(params.get("TXID"));
		if("N178".equals(txid))
			return "doN530";
		else if("N178_1".equals(txid))
			return "doN178_1";
		else if("N178_2".equals(txid))
			return "doN178_2";
		else 
			throw new ServiceBindingException("不支援的 TXID [" + txid+"].");
	}
	
	public MVH doN530(Map params) {
		
		params.put("LOSTTYPE", "178");
		
		return n530.doAction(params);
	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public MVH doN178_1(Map<String, String> params) { 
		
		params.put("CUSIDN", params.get("UID"));
		params.put("FLAG", "1"); //固定為 1：約定
		params.put("TXNTYPE", "01"); //01：選定存單						
			
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);

		TelcommResult result = n178_1Telcomm.query(params);
		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));

		try {
			result.getFlatValues().put("CMTXTIME", 
					DateTimeUtils.getCDateTime(
							result.getValueByFieldName("TRNDATE"),
							result.getValueByFieldName("TRNTIME")));
		}
		catch(Exception e){
			log.error(e.toString());			
		}
		
		return result;
	}

	@SuppressWarnings("unchecked")
	public MVH doN178_2(Map<String, String> params) { 
		
		String TxWay = params.get("FGTXWAY");
		
		// 產生 CERTACN update by Blair -------------------------------------------- Start
		String certACN = "";
		if ("0".equals(TxWay))
		{
			while (certACN.length() < 19)
				certACN += " ";
		}
		else if ("1".equals(TxWay))
		{
			while (certACN.length() < 19)
				certACN += "1";
		}else if ("7".equals(TxWay))
		{
			while (certACN.length() < 19)
				certACN += "C";
		}
		else
		{
			while (certACN.length() < 19)
				certACN += "4";
		}
		params.put("CERTACN", certACN);
		// 產生 CERTACN update by Blair -------------------------------------------- End

		params.put("CUSIDN", params.get("UID"));
		params.put("FLAG", "1"); //固定為 1：約定
		params.put("TXNTYPE", "02"); //02：確認續存
		params.put("NHITAX", params.get("NHITAX"));
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);
      
		TelcommResult result = n178_2Telcomm.query(params);
		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));

		try {
			result.getFlatValues().put("CMTXTIME", 
					DateTimeUtils.getCDateTime(
							result.getValueByFieldName("TRNDATE"),
							result.getValueByFieldName("TRNTIME")));
		}
		catch(Exception e){
			log.error(e.toString());			
		}
		
		return result;
	}
}
