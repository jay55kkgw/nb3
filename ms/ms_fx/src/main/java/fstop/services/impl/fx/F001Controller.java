package fstop.services.impl.fx;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.rest.model.CommonPools;
import com.netbank.rest.model.FieldTranslator;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.exception.ToRuntimeException;
import fstop.exception.UncheckedException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.orm.po.ADMMAILLOG;
import fstop.orm.po.TXNFXRECORD;
import fstop.services.BookingAware;
import fstop.services.NoticeInfo;
import fstop.services.NoticeInfoGetter;
import fstop.services.TelCommExecWrapper;
import fstop.services.TransferNotice;
import fstop.services.WriteLogInterface;
import fstop.services.impl.F001T;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import topibs.utility.NumericUtil;

@Slf4j
public class F001Controller implements BookingAware, WriteLogInterface {
//	Logger logger = Logger.getLogger(getClass());

	@Autowired
	private F001T f001t;
	@Autowired
	private CommonPools commonPools;

	@Autowired
	@Qualifier("n920Telcomm")
	private TelCommExec n920Telcomm;
	
//	@Required
//	public void setF001t(F001T f001t) {
//		this.f001t = f001t;
//	}

	@Override
	public MVHImpl booking(Map<String, String> params) {
		return null;
	}

	@Override
	public MVHImpl online(final Map<String, String> params, final TransferNotice trNotice) {
		//logger.info("{{{\r\n" + JSONUtils.map2json(params) + "\r\n}}}");
		
		String fginacno = params.get("FGINACNO"); // 1 約定帳號, 2 非約定帳號
		//轉入帳號
		String trin_bnkcod = "";
		String trin_acn = "";
		String trin_agree = "";
		if("CMDAGREE".equals(fginacno)) {
			Map<String, String> jm = JSONUtils.json2map(params.get("INACNO1"));
			trin_bnkcod = jm.get("BNKCOD");
			trin_acn = jm.get("ACN");
			trin_agree = jm.get("AGREE");
			
		}
		else if("CMDISAGREE".equals(fginacno)) {  //非約定, 只能是台企銀 050
			trin_bnkcod = "050";
			trin_acn = params.get("INACNO2");
			trin_agree = "0";   // 0: 非約定, 1: 約定
		}
		
		params.put("__TRIN_BANKCOD", trin_bnkcod);
		params.put("__TRIN_ACN", trin_acn);
		params.put("__TRIN_AGREE", trin_agree);
		params.put("__FXMSGSEQNO", StrUtils.isEmpty(params.get("__FXMSGSEQNO")) ? "" : params.get("__FXMSGSEQNO"));

		final F001_F007YTelcomm f007y = new F001_F007YTelcomm(f001t);
		final F001_F007ATelcomm f007a = new F001_F007ATelcomm(f001t);
		final F001_F001Telcomm f001 = new F001_F001Telcomm(f001t);
		final F001_F003Telcomm f003 = new F001_F003Telcomm(f001t, f001);
		final F001_F011Telcomm f011 = new F001_F011Telcomm(f001t);
		
		final List<FxTelCommStep> actions = new ArrayList<FxTelCommStep>();
		String trans_status = (String)params.get("__TRANS_STATUS");
		if(StrUtils.isEmpty(trans_status)) {
			trans_status = "ONLINE";
		}
		
		boolean isScheduleRecord = (StrUtils.trim(params.get("__SCHID")).length() > 0 && !"0".equals(StrUtils.trim(params.get("__SCHID"))));
		
		if("ONLINE".equalsIgnoreCase(trans_status)
				|| ("RESEND".equalsIgnoreCase(trans_status) && !isScheduleRecord)) {
			if(!StrUtils.trim(params.get("PAYCCY")).equalsIgnoreCase(params.get("REMITCY")))
				actions.add(f007y);
			actions.add(f001);
			actions.add(f003);
			actions.add(f011);
		}
		else {   //預約發送 or //重送
			//不同幣別時
			if(!StrUtils.trim(params.get("PAYCCY")).equalsIgnoreCase(params.get("REMITCY")))
				actions.add(f007a);
			actions.add(f001);
			actions.add(f003);
			actions.add(f011);
		}
		
		String FXMSGSEQNO = StrUtils.trim(params.get("__FXMSGSEQNO"));
		if(FXMSGSEQNO.length() == 0) {   //尚未開始執行
			f007a.setSkip(false);
			f007y.setSkip(false);
			f001.setSkip(false);
			f003.setSkip(false);
			f011.setSkip(false);		}
		else if(FXMSGSEQNO.endsWith("0")) {   // F007 成功 
			f007a.setSkip(true);
			f007y.setSkip(true);
			f001.setSkip(false);
			f003.setSkip(false);
			f011.setSkip(false);
		}		
		else if(FXMSGSEQNO.endsWith("1")) {  //F001 成功
			f007a.setSkip(true);
			f007y.setSkip(true);
			f001.setSkip(true);
			f003.setSkip(false);
			f011.setSkip(false);
		}
		else if(FXMSGSEQNO.endsWith("2")) {  // f003 成功
			f007a.setSkip(true);
			f007y.setSkip(true);
			f001.setSkip(true);
			f003.setSkip(true);
			f011.setSkip(false);
		}		
		else if(FXMSGSEQNO.endsWith("3")) {  //MERVA 成功
			f007a.setSkip(true);
			f007y.setSkip(true);
			f001.setSkip(true);
			f003.setSkip(true);
			f011.setSkip(false);
		}		
		else if(FXMSGSEQNO.endsWith("4")) {  //F011 成功 (結束電文)
			f007a.setSkip(true);
			f007y.setSkip(true);
			f001.setSkip(true);
			f003.setSkip(true);
			f011.setSkip(true);
		}			
		else if(FXMSGSEQNO.endsWith("5") || FXMSGSEQNO.endsWith("6")) {  // F007/F005 沖正成功
			f007a.setSkip(true);
			f007y.setSkip(true);
			f001.setSkip(true);
			f003.setSkip(true);
			f011.setSkip(false);
		}		
		
		List<String> commands = new ArrayList();
//		commands.add("PINKEY(CMPASSWORD)");
//		CommandUtils.doBefore(commands, params);
		final TelCommExecWrapper execwrapper = new TelCommExecWrapper(new TelCommExec() {

			public TelcommResult query(Map params) {
				for(FxTelCommStep step : actions) { 
					
					MVHImpl result = step.query(params);
					
					if(step.isNeedDoF011WinFlagN()) {
						params.put("OKFLAG", "N");
						f001t.doF011(params);
						
						params.put("__FXMSGSEQNO", StrUtils.trim((String)params.get("__FXMSGSEQNO")) + "4");	
												
						//進人工重送須記錄 F001.STAN 欄位值			
						params.put("__FXMSGCONTENT", StrUtils.trim((String)params.get("STAN")));									
					}
					
					if(step.getLastException() != null) {
						if(step.getLastException() instanceof UncheckedException)
							throw (UncheckedException)step.getLastException();
						else
							throw new ToRuntimeException(step.getLastException().getMessage(), step.getLastException());
					}
					
				}
				
				return f003.getTelcommResult();
			}
			
		});

		MVHImpl result = execwrapper.query(params);
		log.debug(ESAPIUtil.vaildLog("online.result " + result));
		
		final TXNFXRECORD record = (TXNFXRECORD)writeLog(execwrapper, params, result);

		final TelCommExecWrapper execwrapperFinal = execwrapper;
		trNotice.sendNotice(new NoticeInfoGetter() {

			// 付款人交易通知
			public NoticeInfo getNoticeInfo() { 
				NoticeInfo result = new NoticeInfo(params.get("UID"), params.get("DPMYEMAIL"), "");
				result.setTemplateName("FXNOTIFYF001ER");
				
//				result.setDpretxstatus(record.getFXRETXSTATUS());
				result.getParams().put("__RECORD_ADTXNO", record.getADTXNO());
				result.setADTXNO(record.getADTXNO());
				
				result.setException(execwrapperFinal.getLastException());
				String datetime = record.getFXTXDATE() + record.getFXTXTIME();
				Date d = DateTimeUtils.parse("yyyyMMddHHmmss", datetime);
				String trantime = DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", d);
				result.getParams().put("#TXNAME", "外匯轉帳");				
				result.getParams().put("#TRANTIME", trantime);
				result.getParams().put("#SEQNO", params.get("STAN"));
				
				String acn = record.getFXSVAC();
				try {
					acn = StrUtils.left(acn, acn.length() - 5) + "***" + StrUtils.right(acn, 2);
				} catch (Exception e) {
					log.error("", e);
				}
				
				String outacn = record.getFXWDAC();
				try {
					outacn = StrUtils.left(outacn, outacn.length() - 5) + "***" + StrUtils.right(outacn, 2);
				} catch (Exception e) {
					log.error("", e);
				}
				
				result.getParams().put("#INACN", acn);
				result.getParams().put("#OUTACN", outacn);
				String fxwdcurr="";
				String fxsvcurr="";
				String fxwdamtfmt ="";
				String fxsvamtfmt ="";

//				if("1".equals(params.get("PAYREMIT"))) { // 轉出
				fxwdcurr = params.get("F001_ACCCUR") ;

				log.debug(ESAPIUtil.vaildLog("F001_ACCCUR = " + params.get("F001_ACCCUR")));
				log.debug(ESAPIUtil.vaildLog("F001_ACCAMT = " + params.get("F001_ACCAMT")));

				
				fxwdamtfmt = NumericUtil.formatNumberString(params.get("F001_ACCAMT"), 2);
				result.getParams().put("#OUTCURRENCY", fxwdcurr);
				result.getParams().put("#OUTAMT", fxwdamtfmt + "元&nbsp;&nbsp;（未扣手續費）");
				
				log.debug(ESAPIUtil.vaildLog("F001_CURCODE = " + params.get("F001_CURCODE")));
				log.debug(ESAPIUtil.vaildLog("F001_CURAMT = " + params.get("F001_CURAMT")));
				fxsvcurr = params.get("F001_CURCODE") ;
				fxsvamtfmt = NumericUtil.formatNumberString(params.get("F001_CURAMT"), 2);
				result.getParams().put("#INCURRENCY", fxsvcurr);
				result.getParams().put("#INAMT", fxsvamtfmt + "元&nbsp;&nbsp;（未扣手續費）");

//				}
//				else { // "2".equals(params.get("PAYREMIT")) // 轉入
//					fxsvcurr = record.getFXSVCURR() ;
//					fxsvamtfmt = NumericUtil.formatNumberString(record.getFXSVAMT(), 2);
//					result.getParams().put("#INCURRENCY", fxsvcurr);
//					result.getParams().put("#INAMT", fxsvamtfmt + "元&nbsp;&nbsp;（未扣手續費）");
					
//					fxwdcurr = record.getFXWDCURR() ;
//					fxwdamtfmt = NumericUtil.formatNumberString(record.getFXWDAMT(), 2);
//					result.getParams().put("#OUTCURRENCY", fxwdcurr);
//					result.getParams().put("#OUTAMT", fxwdamtfmt + "元&nbsp;&nbsp;（未扣手續費）");

//				}				
				log.debug(ESAPIUtil.vaildLog("COMMCCY = " + params.get("COMMCCY")));
				result.getParams().put("#COMMCCY", params.get("COMMCCY"));
				log.debug(ESAPIUtil.vaildLog("COMMAMT = " + (params.get("DISFEE") == null ? " " : NumericUtil.formatNumberString(params.get("DISFEE"), 2))));
				result.getParams().put("#COMMAMT", (params.get("DISFEE") == null ? " " : NumericUtil.formatNumberString(params.get("DISFEE"), 2)));
				
				result.getParams().put("#RATE",(params.get("RATE")==null ? "" : StrUtils.trim(params.get("RATE"))));

//				if(params.get("RATE")!=null)
//				{
//					result.getParams().put("#RATECURRENCY",params.get("COMMCCY") == null ? "" : StrUtils.trim(params.get("COMMCCY")));
//				}

				result.getParams().put("#MEMO", params.get("CMMAILMEMO"));
				
				ADMMAILLOG admmaillog = new ADMMAILLOG();
				admmaillog.setADACNO(record.getFXWDAC());
				
				String trans_status = (String)params.get("__TRANS_STATUS");
				if(StrUtils.isEmpty(trans_status)) {
					trans_status = "FXONLINE";
				}
				else if("SEND".equals(trans_status)) {
					trans_status = "FXSCH";
				}
				else if("RESEND".equals(trans_status)) {
					trans_status = "FXSCHRE";
				}
				
				admmaillog.setADBATCHNO(trans_status);
				admmaillog.setADMAILACNO(StrUtils.trim(params.get("CMTRMAIL")));
				admmaillog.setADUSERID(record.getFXUSERID());
				admmaillog.setADUSERNAME("");
				admmaillog.setADSENDTYPE("3");
				admmaillog.setADSENDTIME(datetime);
				admmaillog.setADSENDSTATUS("");
				
				result.setADMMAILLOG(admmaillog);
				
				return result;
			}
			
		});

	

		if((params.get("CMTRMAIL").length()!=0) && (params.get("CMTRMAIL")!=null))
		{
			trNotice.sendNotice(new NoticeInfoGetter() {

				// 收款人交易通知
				public NoticeInfo getNoticeInfo() { 
					NoticeInfo result = new NoticeInfo("", "", params.get("CMTRMAIL"));
					result.setTemplateName("FXNOTIFYF001EE");
				
//					result.setDpretxstatus(record.getFXRETXSTATUS());
					result.getParams().put("__RECORD_ADTXNO", record.getADTXNO());
					result.setADTXNO(record.getADTXNO());
				
					result.setException(execwrapperFinal.getLastException());
					String datetime = record.getFXTXDATE() + record.getFXTXTIME();
					Date d = DateTimeUtils.parse("yyyyMMddHHmmss", datetime);
					String trantime = DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", d);
					result.getParams().put("#TXNAME", "外匯轉帳");				
					result.getParams().put("#TRANTIME", trantime);
					result.getParams().put("#SEQNO", params.get("STAN"));
				
					String acn = record.getFXSVAC();
					try {
						acn = StrUtils.left(acn, acn.length() - 5) + "***" + StrUtils.right(acn, 2);
					}
					catch(Exception e){}
				
					String outacn = record.getFXWDAC();
					try {
						outacn = StrUtils.left(outacn, outacn.length() - 5) + "***" + StrUtils.right(outacn, 2);
					}
					catch(Exception e){}
				
					result.getParams().put("#ACN", acn);
					result.getParams().put("#OUTACN", outacn);
				
					log.debug("F001_CURCODE = " + params.get("F001_CURCODE"));
					log.debug("F001_CURAMT = " + params.get("F001_CURAMT"));
				
					result.getParams().put("#OUTCURRENCY", params.get("F001_CURCODE"));
					result.getParams().put("#AMT", NumericUtil.formatNumberString(params.get("F001_CURAMT"), 2));

					result.getParams().put("#MEMO", params.get("CMMAILMEMO"));
				
					log.debug("COMMCCY2 = " + (params.get("COMMCCY2")== null ? " " : params.get("COMMCCY2")));
					log.debug("COMMAMT2 = " + (params.get("COMMAMT2") == null ? " " : NumericUtil.formatNumberString(params.get("COMMAMT2"), 2) + "元"));
					result.getParams().put("#COMMCCY2", (params.get("COMMCCY2")== null ? " " : params.get("COMMCCY2")));
					result.getParams().put("#COMMAMT2", (params.get("COMMAMT2") == null ? " " : NumericUtil.formatNumberString(params.get("COMMAMT2"), 2) + "元"));
				
					ADMMAILLOG admmaillog = new ADMMAILLOG();
					admmaillog.setADACNO(record.getFXWDAC());
				
					String trans_status = (String)params.get("__TRANS_STATUS");
					if(StrUtils.isEmpty(trans_status)) {
						trans_status = "FXONLINE";
					}
					else if("SEND".equals(trans_status)) {
						trans_status = "FXSCH";
					}
					else if("RESEND".equals(trans_status)) {
						trans_status = "FXSCHRE";
					}
				
					admmaillog.setADBATCHNO(trans_status);
					admmaillog.setADMAILACNO(StrUtils.trim(params.get("CMTRMAIL")));
					admmaillog.setADUSERID(record.getFXUSERID());
					admmaillog.setADUSERNAME("");
					admmaillog.setADSENDTYPE("3");
					admmaillog.setADSENDTIME(datetime);
					admmaillog.setADSENDSTATUS("");
				
					result.setADMMAILLOG(admmaillog);
				
					return result;
				}
			
			});
		}
		execwrapper.throwException();


		// 合併 F001 及 F003 的結果
		if(result != null) {
			log.trace(ESAPIUtil.vaildLog("F001Controller.before.Merge.params: {}"+ CodeUtil.toJson(params)));
			log.trace(ESAPIUtil.vaildLog("F001Controller.before.Merge.result: {}"+ CodeUtil.toJson(result.getFlatValues())));
			log.trace(ESAPIUtil.vaildLog("F001Controller.before.Merge.f001.getTelcommResult().getFlatValues(): {}"+CodeUtil.toJson(f001.getTelcommResult().getFlatValues())));
			
			// F003下行電文帳戶餘額欄位同名F001金額，故先另外存進TOTBAL
			result.getFlatValues().put("TOTBAL", result.getFlatValues().get("CURAMT"));
			
			// 合併 F001 及 F003 的結果
			result.getFlatValues().putAll(f001.getTelcommResult().getFlatValues());
			result.getFlatValues().put("ADTXNO", record.getADTXNO());
			
/**
 * TODO 交易單據
 *
			// 成功時產生交易單據			
			HttpServletRequest request = WebServletUtils.getRequest();
			HttpServletResponse response = WebServletUtils.getResponse();			
	
			request.setAttribute("PreviewHelper", result);		
			request.setAttribute("PreviewInputQueryData", params);

			try {
				String str_ResultHtml = JSPUtils.toString2("fx/F001CertPreview.jsp", request, response);	
				commonPools.fxUtils.updateFxCert(record.getADTXNO(), str_ResultHtml);
			}
			catch(Exception e) {
				throw new ToRuntimeException("產生交易單據發生錯誤.", e);
			}
 */			

			/**
			 * 交易單據
			 * TXNFXRECORD.FXCERT欄位改放交易單據所需資料的JSON
			 */
			MVHImpl mvhimpl = null;
			try {
				// 電文結果更新request參數
				result.getFlatValues().putAll(params);
				
				log.trace("F001Controller.after.Merge.result: {}", result.getFlatValues());
				
				// N920
				MVH mvh = new MVHImpl();
				mvh = commonPools.n920.getAcnoList(result.getFlatValues().get("UID"), new String[]{"01", "02", "03", "04", "05", "07"}, "Y");
				mvhimpl = (MVHImpl) mvh;
				
				
				// 列印時間
				result.getFlatValues().put("curentDateTime", DateTimeUtils.getDatetime(new Date()));
				// 姓名
				result.getFlatValues().put("NAME", mvhimpl.getFlatValues().get("NAME") );
				// 付款日期
				result.getFlatValues().put("currentDate", DateTimeUtils.getDateShort(new Date()));
				// 英文姓名
				StringBuffer sb = new StringBuffer();
				sb.append(result.getFlatValues().get("M150AD1"));
				if ( !result.getFlatValues().get("M150AD2").trim().equals("") ) {
					sb.append("<br>");
					sb.append(result.getFlatValues().get("M150AD2"));
				}
				if ( !result.getFlatValues().get("M150AD3").trim().equals("") ) {
					sb.append("<br>");
					sb.append(result.getFlatValues().get("M150AD3"));
				}
				if ( !result.getFlatValues().get("M150AD4").trim().equals("") ) {
					sb.append("<br>");
					sb.append(result.getFlatValues().get("M150AD4"));
				}
				result.getFlatValues().put("ENAME", sb.toString());
				// 轉出金額
				String str_Amt = result.getFlatValues().get("ORGAMT");
				String str_Ccy = result.getFlatValues().get("ORGCCY");
				if (str_Ccy.equals("TWD") || str_Ccy.equals("JPY")) {
					str_Amt = str_Amt.substring(0, str_Amt.length()-2);
				}
				result.getFlatValues().put("OUTAMT", NumericUtil.formatNumberStringByCcy(str_Amt, 2, str_Ccy));
				// 轉入金額
				String str_InAmt = result.getFlatValues().get("PMTAMT");
				String str_InCcy = result.getFlatValues().get("PMTCCY");
				if (str_InCcy.equals("TWD") || str_InCcy.equals("JPY")) {
					str_InAmt = str_InAmt.substring(0, str_InAmt.length()-2);
				}
				result.getFlatValues().put("INAMT", NumericUtil.formatNumberStringByCcy(str_InAmt, 2, str_InCcy));
				// 匯率
				String str_Rate = result.getFlatValues().get("EXRATE");	
				if (str_Rate == null) {
					str_Rate = "";
				} else if (str_Rate != null && str_Rate.replace("0","").equals("")) {
					str_Rate = "";
				} else {
					str_Rate = NumericUtil.formatNumberString(result.getFlatValues().get("EXRATE"),8);
				}
				result.getFlatValues().put("str_Rate", str_Rate);
				// 議價編號
				result.getFlatValues().put("BGROENO", (result.getFlatValues().get("BGROENO")==null)? "" : result.getFlatValues().get("BGROENO"));
				// 議價編號
				result.getFlatValues().put("BENTYPE", FieldTranslator.transfer("F002", "BENTYPE", result.getFlatValues().get("BENTYPE")));
				// 收款地區國別
				String str_custacc = result.getFlatValues().get("CUSTACC");
				String str_benacc = result.getFlatValues().get("BENACC");
				String str_Country = "TW";
				if ((! "893".equals(str_custacc.substring(0,3))) && ("893".equals(str_benacc.substring(0,3)))) {
					//D-O		
					str_Country = "XA";
				}
				result.getFlatValues().put("str_Country", str_Country);
				// 手續費金額
				String str_feeAmt = result.getFlatValues().get("COMMAMT");
				String str_feeCcy = result.getFlatValues().get("COMMCCY");
				if (str_feeCcy.equals("TWD") || str_feeCcy.equals("JPY")) {
					str_feeAmt = str_feeAmt.substring(0, str_feeAmt.length()-2);
				}
				result.getFlatValues().put("FEEAMT", NumericUtil.formatNumberStringByCcy(str_feeAmt, 2, str_feeCcy));
				// 郵電費金額
				String str_cabAmt = result.getFlatValues().get("CABCHG");
				String str_cabCcy = result.getFlatValues().get("COMMCCY");
				if (str_cabCcy.equals("TWD") || str_cabCcy.equals("JPY")) {
					str_cabAmt = str_cabAmt.substring(0, str_cabAmt.length()-2);
				}
				result.getFlatValues().put("CABAMT", NumericUtil.formatNumberStringByCcy(str_cabAmt, 2, str_cabCcy));
				
				
				// 放入TXNFXRECORD.FXCERT
				String str_Result = CodeUtil.toJson(result.getFlatValues());	
				commonPools.fxUtils.updateFxCert(record.getADTXNO(), str_Result);
				
			} catch(Exception e) {
				throw new ToRuntimeException("產生交易單據發生錯誤.", e);
			}
			
		}		
		
		return result;
		
		
	}
	

	@Override
	public Object writeLog(final TelCommExecWrapper execwrapper, final Map _params, final MVHImpl result) {
		Object obj = f001t.writeLog(execwrapper, _params, result);
		return obj;
	}

}
