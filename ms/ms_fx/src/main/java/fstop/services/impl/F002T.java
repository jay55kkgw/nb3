package fstop.services.impl;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.exception.ToRuntimeException;
import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.orm.dao.AdmMsgCodeDao;
import fstop.orm.dao.SysDailySeqDao;
import fstop.orm.dao.TxnFxRecordDao;
import fstop.orm.dao.TxnFxSchPayDataDao;
import fstop.orm.po.SYSPARAMDATA;
import fstop.orm.po.TXNFXRECORD;
import fstop.services.BOLifeMoniter;
import fstop.services.CommonService;
import fstop.services.OnLineTransferNoticeFx;
import fstop.services.ServiceBindingException;
import fstop.services.TelCommExecWrapper;
import fstop.services.impl.fx.F002Controller;
import fstop.services.mgn.B105;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import orginal.topibs.utility.MT103;
import orginal.topibs.utility.MT202;
import topibs.utility.NumericUtil;

/**
 * 外匯匯出匯款
 * 
 * @author Owner
 *
 */
@Slf4j
public class F002T extends CommonService
{
	// private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("f001Telcomm")
	private TelCommExec f001Telcomm;

	@Autowired
	@Qualifier("f003Telcomm")
	private TelCommExec f003Telcomm;

	@Autowired
	@Qualifier("f005Telcomm")
	private TelCommExec f005Telcomm;

	@Autowired
	@Qualifier("f007Telcomm")
	private TelCommExec f007Telcomm;

	@Autowired
	@Qualifier("f011Telcomm")
	private TelCommExec f011Telcomm;

	@Autowired
	@Qualifier("f021Telcomm")
	private TelCommExec f021Telcomm;

	@Autowired
	@Qualifier("mervaTelcomm")
	private TelCommExec mervaTelcomm;

	@Autowired
	@Qualifier("n951CMTelcomm")
	private TelCommExec n951CMTelcomm;

	@Autowired
	private TxnFxRecordDao txnFxRecordDao;// 外幣交易記錄檔

	@Autowired
	private AdmMsgCodeDao admMsgCodeDao;

	@Autowired
	private SysDailySeqDao sysDailySeqDao;

	@Autowired
	private TxnFxSchPayDataDao txnFxSchPayDataDao; // 預約資料

	@Autowired
	private F002Controller f002controller;

	@Autowired
	private B105 b105;

	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params)
	{
		// TODO 在營業時間 09:30~15:30 外, 只能執行預約交易
		// TODO LOG 寫入的機制
		Map<String, String> params = _params;

		String paydate = StrUtils.trim(params.get("PAYDATE")).replaceAll("/", "");
		params.put("PAYDATE", paydate);

		String f = params.get("FGTRDATE");
		if ("0".equals(f))
		{ // 即時

			// TODO 本機測試註解
			// 判斷營業時間 ,若交易最後提交時間 > (營業截止時間 + 5分鐘) => 須顯示錯誤訊息(Z409)
			SYSPARAMDATA sysParam = (SYSPARAMDATA) b105.getSysParamData();

			int i_MM = Integer.parseInt(sysParam.getADFXEMM()) + 5;
			String str_BizEnd = sysParam.getADFXEHH() + String.valueOf(i_MM) + sysParam.getADFXESS();
			String str_CurrentTime = DateTimeUtils.getTimeShort(new Date());

			if (str_CurrentTime.compareTo(str_BizEnd) > 0)
			{
				throw TopMessageException.create("Z409", (String) params.get("ADOPID"));
			}

			/*** 驗證交易密碼 - 開始 ***/
			if (params.get("FGTXWAY") == null)
			{
				throw TopMessageException.create("S302", (String) params.get("ADOPID"));
			}
			else if ("0".equals(params.get("FGTXWAY")))
			{
				// 20190518 ian 改為n951
				MVHImpl n951Result = n951CMTelcomm.query(params);

				// F003 電文需要
				params.put("PINKEYFX", StrUtils.repeat("F2", 8));
			}
			else if ("7".equals(params.get("FGTXWAY")))
			{
				// F003 電文需要
				params.put("PINKEYFX", StrUtils.repeat("F2", 8));
			}
			/*** 驗證交易密碼 - 結束 ***/

			MVHImpl result = new MVHImpl();
			result = online(params);
			if (result == null)
				result = new MVHImpl();

			return result;
		}
		else
		{
			throw new ServiceBindingException("不為即時交易");
		}
	}

	public MVHImpl online(Map<String, String> _params)
	{
		// 轉給 f001controller 去做 online 的動作
		OnLineTransferNoticeFx trNotice = new OnLineTransferNoticeFx();
		MVHImpl result = f002controller.online(_params, trNotice);
		return result;
	}

	public TelcommResult doF001(Map<String, String> params)
	{
		// TODO 組合F001電文所需要的欄位
		Date d = new Date();
		params.put("TERM_DATE", DateTimeUtils.format("yyyyMMdd", d));
		params.put("TERM_TIME", DateTimeUtils.format("HHmmss", d));
		String tranOut = StrUtils.trim(params.get("CUSTACC"));
		String tranIn = StrUtils.trim(params.get("BENACC"));
		// BENTYPE
		params.put("PCODE", "000A");
		params.put("DEALNO", StrUtils.trim(params.get("BGROENO")));

		/**
		 * 外幣交易取序號 不能重複 中心會擋 N : 舊各網 C : 企網 H :
		 */
		if (StrUtils.isEmpty(params.get("STAN"))) {
			params.put("STAN", sysDailySeqDao.getFXStan("F002T"));

			BOLifeMoniter.setValue("STAN", params.get("STAN"));
		}
		params.put("SEQNO", "00000");
		params.put("CUSTID", params.get("CUSIDN"));
		/*** New Add ***/
		// params.put("MSGID", "");
		params.put("COMMCCY", params.get("COMMCCY")); // 手續費幣別
		params.put("COMMACC", params.get("COMMACC")); // 手續費帳號
		params.put("CUSTACC", params.get("CUSTACC")); // 轉出帳號
		// params.put("BENACC", params.get("__TRIN_ACN")); //轉入帳號

		if ("1".equals(params.get("PAYREMIT")))
		{
			params.put("AMTTYPE", "1");// 金額類別
			params.put("CURAMT", params.get("CURAMT"));// 轉出金額 (議價金額)

			params.put("OUT_AMT", params.get("CURAMT"));// 轉出金額 (議價金額) => 保留原始輸入欄位值
		}
		else
		{
			// "2".equals(params.get("PAYREMIT"))
			params.put("AMTTYPE", "2");// 金額類別
			params.put("ATRAMT", params.get("CURAMT"));// 轉入金額

			params.put("IN_AMT", params.get("CURAMT"));// 轉入金額 => 保留原始輸入欄位值
		}

		/*
		 * 如果電文失敗或寫DB發生任何錯誤, 則發 F007的電文 但是 FLAG 內容為 C 的電文
		 */
		// 處理{匯款附言}欄位
		String str_Memo1 = params.get("MEMO1").toString();
		str_Memo1 = str_Memo1.replaceAll("\r", "");
		str_Memo1 = str_Memo1.replaceAll("\n", "");
		params.put("MEMO1", str_Memo1);
		if (str_Memo1.length() <= 35)
		{
			params.put("MEMO1", "");
			params.put("MEMO2", str_Memo1);
		}
		else if (str_Memo1.length() > 35 && str_Memo1.length() <= 70)
		{
			params.put("MEMO1", "");
			params.put("MEMO2", str_Memo1.substring(0, 35));
			params.put("MEMO3", str_Memo1.substring(35));
		}
		else if (str_Memo1.length() > 70 && str_Memo1.length() <= 105)
		{
			params.put("MEMO1", "");
			params.put("MEMO2", str_Memo1.substring(0, 35));
			params.put("MEMO3", str_Memo1.substring(35, 70));
			params.put("MEMO4", str_Memo1.substring(70));
		}
		/*
		 * else if (str_Memo1.length() > 105) { params.put("MEMO1", str_Memo1.substring(0,35)); params.put("MEMO2",
		 * str_Memo1.substring(35,70)); params.put("MEMO3", str_Memo1.substring(70,105)); params.put("MEMO4",
		 * str_Memo1.substring(105)); }
		 */
		params.put("CUSNM1", params.get("NAME")); // 付款人名稱

		// F037-[國別]欄位特別處理
		// Map map = JSONUtils.json2map((String)params.get("F002_F037_DATA"));
		params.put("COUNTRY", params.get("COUNTRY"));

		TelcommResult result = f001Telcomm.query(params); // 議價電文

		return result;
	}

	public TelcommResult doF003(MVHImpl resultF001, Map<String, String> params)
	{
		// FROM F001
		params.put("TXBRH", ""); // 承作行
		params.put("CUSTPAY_ID", params.get("UID")); // 付款人統一編號
		params.put("ACCTID", params.get("CUSTACC")); // F001 付款帳號
		params.put("ACCTTYPE", ""); // 付款人帳戶類別

		params.put("ACCCUR", resultF001.getFlatValues().get("ORGCCY")); // 付款人轉出幣別
		params.put("ACCAMT", resultF001.getFlatValues().get("ORGAMT")); // 轉出金額

		params.put("CURCODE", resultF001.getFlatValues().get("PMTCCY")); // 付款人轉入幣別
		params.put("CURAMT", resultF001.getFlatValues().get("PMTAMT")); // 轉入金額

		/*** F001 電文回應欄位 SWTKIND (SWIFT電文發送方式) ***/
		params.put("SWTKIND", resultF001.getFlatValues().get("SWTKIND")); // 轉入金額

		/*** 紀錄F001之轉出轉入相關資料 ***/
		params.put("F001_ACCCUR", resultF001.getFlatValues().get("ORGCCY")); // 付款人轉出幣別
		params.put("F001_ACCAMT", resultF001.getFlatValues().get("ORGAMT")); // 轉出金額

		params.put("F001_CURCODE", resultF001.getFlatValues().get("PMTCCY")); // 付款人轉入幣別
		params.put("F001_CURAMT", resultF001.getFlatValues().get("PMTAMT")); // 轉入金額

		if (params.get("OUT_CRY") == null)
		{
			params.put("OUT_CRY", params.get("ACCCUR")); // 轉出幣別
			params.put("IN_CRY", params.get("CURCODE")); // 轉入幣別
		}

		params.put("BANK_TYPE", ""); // 銀行代號類別
		params.put("PRCDT", params.get("PAYDATE")); // 付款日期

		//
		// 付款人外匯議價號碼,左靠右補空白
		// (13-20) 8 碼為 交易密碼 MAC KEY SYNC
		// (21-36) 16 碼為 USERID
		//
		// 同幣別不會有 "BGROENO", 所以帶空白
		params.put("REFID", StrUtils.left(StrUtils.trim(params.get("BGROENO")) + StrUtils.repeat(" ", 12), 12));
		params.put("SYNC", StrUtils.trim(params.get("SYNC"))); // 8 碼
		params.put("USERID", StrUtils.left(params.get("USERID") + StrUtils.repeat(" ", 16), 16)); // 議價編號

		params.put("PAYEE_ID", StrUtils.trim(params.get("BENID")));// 收款人統一編號

		params.put("ACCTIDTO", StrUtils.trim(params.get("BENACC"))); // 收款人帳號

		params.put("ACCTTP_TO", ""); // 收款人帳戶類別

		params.put("FEEACCTID", params.get("COMMACC")); // 付款人手續費帳號

		params.put("FEEACT_TP", ""); // 付款人手續費帳戶類別

		params.put("FEEACT_CRY", params.get("COMMCCY")); // 付款手續費帳戶幣別

		params.put("FEEBANK_TP", ""); // 手續費銀行代號類別

		// params.put("TNSATION", resultF001.getFlatValues().get("ORGCOMM")); //匯出匯款手續費金額
		// params.put("OTHERFEE", resultF001.getFlatValues().get("ORGCABLE")); //郵電費

		/*** [手續費]與[國外費用]相加,放入F003[手續費]金額欄位 ***/
		Double d_COMMAMT = Double.parseDouble(resultF001.getFlatValues().get("COMMAMT"));
		Double d_OURCHG = Double.parseDouble(resultF001.getFlatValues().get("OURCHG"));
		d_COMMAMT += d_OURCHG;
		Double d = new Double(d_COMMAMT);

		params.put("COMMAMT", resultF001.getFlatValues().get("COMMAMT")); // 匯出匯款手續費金額

		params.put("TNSATION", String.valueOf(d.intValue())); // 匯出匯款手續費 + 國外費用
		params.put("OTHERFEE", resultF001.getFlatValues().get("CABCHG")); // 郵電費
		if ("OUR".equals(resultF001.getValueByFieldName("M171CHG")))
			params.put("DISFEE", resultF001.getFlatValues().get("COMMAMT")); // 折扣後手續費 20181009修改為手續費
		else params.put("DISFEE", String.valueOf(d.intValue())); // 折扣後手續費 + 國外費用

		params.put("DISTOTH", resultF001.getFlatValues().get("CABCHG")); // 折扣後郵電費
		// params.put("RESENDFLAG", ""); //重送標記

		params.put("M1REFNO", resultF001.getFlatValues().get("M1REFNO")); // 銀行交易序號

		String str_First4Byte = "    ";
		String str_Flag = " ";
		if (!"0".equals(params.get("FGTRDATE")))
		{ // 若為預約交易
			str_Flag = "Y";
		}

		if ("000B".equals(params.get("PCODE")))
			str_First4Byte = "FISC";

		params.put("ACWTYPE", str_First4Byte + "            " + str_Flag); // 收款人帳戶類別
		params.put("COMMCCY2", resultF001.getFlatValues().get("COMMCCY2")); // 入帳手續費幣別
		params.put("COMMAMT2", resultF001.getFlatValues().get("COMMAMT2")); // 入帳手續費金額
		params.put("SWFTCODE", resultF001.getFlatValues().get("M1SBBIC")); // 收款人 SWIFT CD
		params.put("BACH_MARK", "N"); // 整批註記
		params.put("TXPSW", params.get("PINNEW")); // 押密後交易密碼

		// TODO 本機測試註解
		// 壓 F003 的 MAC 欄位
		f003MAC(params);

		TelcommResult result = f003Telcomm.query(params); //

		return result;
	}

	public TelcommResult doF005(Map<String, String> params)
	{
		// TODO 組合F005電文所需要的欄位

		String tranOut = StrUtils.trim(params.get("CUSTACC"));
		String tranIn = StrUtils.trim(params.get("BENACC")); // 轉入帳號

		params.put("PCODE", "000A");

		params.put("MSGID", "");
		// params.put("CUSTYPE", "1"); //TODO 之後會從 session 來

		params.put("CUSTACC", params.get("CUSTACC")); // 轉出帳號
		if ("1".equals(params.get("PAYREMIT")))
		{
			params.put("AMTTYPE", "1");// 金額類別
			params.put("CURAMT", params.get("CURAMT"));// 轉出金額 (議價金額)
		}
		else
		{
			// "2".equals(params.get("PAYREMIT"))
			params.put("AMTTYPE", "2");// 金額類別
			params.put("ATRAMT", params.get("CURAMT"));// 轉入金額
		}

		/*
		 * 如果電文失敗或寫DB發生任何錯誤, 則發 F007的電文 但是 FLAG 內容為 C 的電文
		 */

		TelcommResult result = f005Telcomm.query(params); //

		return result;
	}

	public TelcommResult doF007C(Map<String, String> params)
	{

		params.put("FYACN", params.get("CUSTACC")); // 轉出帳號
		// params.put("INACN",(params.get("BENACC").length()>11) ? params.get("BENACC").substring(0,11) :
		// params.get("BENACC")); //轉入帳號
		params.put("INACN", ""); // 轉入帳號

		params.put("OUT_CRY", params.get("PAYCCY")); // 轉出幣別
		params.put("IN_CRY", params.get("REMITCY")); // 轉入幣別
		if ("1".equals(params.get("PAYREMIT")))
		{
			params.put("AMTTYPE", "1");// 金額類別
			params.put("CURAMT", params.get("CURAMT"));// 轉出金額 (議價金額)
		}
		else
		{
			// "2".equals(params.get("PAYREMIT"))
			params.put("AMTTYPE", "2");// 金額類別
			params.put("ATRAMT", params.get("CURAMT"));// 轉入金額
		}
		params.put("FLAG", "C");// 交易狀態
		/*
		 * 如果電文失敗或寫DB發生任何錯誤, 則發 F007的電文 但是 FLAG 內容為 C 的電文
		 */
		TelcommResult result = f007Telcomm.query(params); // 議價電文

		return result;
	}

	public TelcommResult doF011(Map<String, String> params)
	{
		// TODO 組合F011電文所需要的欄位

		String tranOut = StrUtils.trim(params.get("CUSTACC"));
		String tranIn = StrUtils.trim(params.get("BENACC")); // 轉入帳號

		params.put("PCODE", "000A");

		params.put("MSGID", "");
		// params.put("CUSTYPE", "1"); //TODO 之後會從 session 來

		params.put("CUSTACC", params.get("CUSTACC")); // 轉出帳號

		if ("1".equals(params.get("PAYREMIT")))
		{
			params.put("AMTTYPE", "1");// 金額類別
			params.put("CURAMT", params.get("CURAMT"));// 轉出金額 (議價金額)
		}
		else
		{ // "2".equals(params.get("PAYREMIT"))
			params.put("AMTTYPE", "2");// 金額類別
			params.put("ATRAMT", params.get("CURAMT"));// 轉入金額
		}
		/*
		 * 如果電文失敗或寫DB發生任何錯誤, 則發 F007的電文 但是 FLAG 內容為 C 的電文
		 */
		TelcommResult result = f011Telcomm.query(params);

		return result;
	}

	public TelcommResult doMerva(Map<String, String> params, MVHImpl f001Result)
	{
		// TODO 組合 MERVA 電文所需要的欄位
		Date d = new Date();
		params.put("TIA-TERM-DATE", DateTimeUtils.format("yyyyMMdd", d));
		params.put("TIA-TERM-TIME", DateTimeUtils.format("HHmmss", d));

		// TIA_TLID 需依照規則另外做判定, 如果 M1SWIFT 內容最後三碼為"XXX", 則填入"REM"
		// 若 M1SWIFT 內容最後三碼為"893", 則填入承做行(TXBRH)內容, 又, 如果承做行內容為"041", 則填入"REM"
		// 其餘狀況一律取 M1SWIFT 之最後三碼填入
		String str_TLID = f001Result.getValueByFieldName("M1SWIFT").substring(8);
		if (str_TLID.equals("XXX"))
		{
			params.put("BRH-CODE", "XXX");
			params.put("TLID", "REM");
		}
		else if (str_TLID.equals("893"))
		{
			params.put("BRH-CODE", "893");

			if ((f001Result.getValueByFieldName("TXBRH")).equals("041"))
			{
				params.put("TLID", "REM");
			}
			else
			{
				params.put("TLID", f001Result.getValueByFieldName("TXBRH"));
			}
		}
		else
		{
			params.put("BRH-CODE", f001Result.getValueByFieldName("M1SWIFT").substring(8));
			params.put("TLID", f001Result.getValueByFieldName("M1SWIFT").substring(8));
		}

		params.put("WS-ID", f001Result.getValueByFieldName("STAN"));

		// FOO1 SWTKIND
		// 1 跟R時, 送 MT103
		// 2時, 送MT103+202
		StringBuffer FIDATA = new StringBuffer();
		log.debug("SWTKIND==" + f001Result.getValueByFieldName("SWTKIND"));
		MT103 mt103 = new MT103();
		mt103.setF001Result(f001Result);
		String mt103Data = mt103.createMsg();
		FIDATA.append(mt103Data);
		if ("2".equals(f001Result.getValueByFieldName("SWTKIND")))
		{
			MT202 mt202 = new MT202();
			mt202.setF001Result(f001Result);
			String mt202Data = mt202.createMsg();
			FIDATA.append(mt202Data);
		}

		// 長度要送
		params.put("DATALENGTH", FIDATA.length() + "");
		params.put("FIDATA", FIDATA.toString());

		TelcommResult result = mervaTelcomm.query(params);

		return result;
	}

	/**
	 * 有關FENB-003 電文押碼:
	 * 
	 * 將以下資料押碼 1. 付款帳號 ACCTID 2. 付款人統一編號 CUSTPAY_ID 3. 付款人日期 PRCDT 4. 交易序號 STAN
	 * 
	 * 
	 * 
	 * 25 '0' + 付款帳號 (11) : 36 '000' + 付款人統一編號 (8) : 11 ==> ID 押碼遇字母轉為數字 A->1, B->2 , J->0, K->1... 付款人日期 : 8 '0' + 交易序號
	 * (8) : 9 -------------------------------------------------------- 64
	 * 
	 * @param params
	 */
	public static void f003MAC(Map<String, String> params)
	{
		F001T.f003MAC(params);
	}

	public MVHImpl doF007Y(Map _params)
	{
		Map<String, String> params = _params;

		String fginacno = params.get("FGINACNO"); // 1 約定帳號, 2 非約定帳號

		// 轉入帳號
		String trin_bnkcod = "";
		String trin_acn = "";
		String trin_agree = "";
		if ("CMDAGREE".equals(fginacno))
		{
			Map<String, String> jm = JSONUtils.json2map(params.get("INACNO1"));
			trin_bnkcod = jm.get("BNKCOD");
			trin_acn = jm.get("ACN");
			trin_agree = jm.get("AGREE");

		}
		else if ("CMDISAGREE".equals(fginacno))
		{ // 非約定, 只能是台企銀 050
			trin_bnkcod = "050";
			trin_acn = params.get("INACNO2");
			trin_agree = "0"; // 0: 非約定, 1: 約定
		}

		params.put("__TRIN_BANKCOD", trin_bnkcod);
		params.put("__TRIN_ACN", trin_acn);
		params.put("__TRIN_AGREE", trin_agree);

		// TODO 組合議價電文所需要的欄位
		params.put("FYACN", params.get("CUSTACC")); // 轉出帳號
		params.put("INACN", ""); // 轉入帳號
		params.put("OUT_CRY", params.get("PAYCCY")); // 轉出幣別
		params.put("IN_CRY", params.get("REMITCY")); // 轉入幣別

		if ("1".equals(params.get("PAYREMIT")))
		{
			params.put("AMTTYPE", "1");// 金額類別
			params.put("CURAMT", params.get("CURAMT"));// 轉出金額 (議價金額)

			params.put("OUT_AMT", params.get("CURAMT"));// 轉出金額 (議價金額) => 保留原始輸入欄位值
		}
		else
		{ // "2".equals(params.get("PAYREMIT"))
			params.put("AMTTYPE", "2");// 金額類別
			params.put("ATRAMT", params.get("CURAMT"));// 轉入金額

			params.put("IN_AMT", params.get("CURAMT"));// 轉入金額 => 保留原始輸入欄位值
		}

		params.put("FLAG", "Y");// 交易狀態

		/*
		 * 如果電文失敗或寫DB發生任何錯誤, 則發 F007的電文 但是 FLAG 內容為 C 的電文
		 */

		TelcommResult result = f007Telcomm.query(params);

		return result;
	}

	public boolean checkSuccessTopMsg(String topmsg)
	{
		// 若回傳為 000000, 或者為 空白, 則回傳 true
		try
		{
			Double d = Double.parseDouble(topmsg);
			if (d == 0d)
				return true;
		}
		catch (Exception e)
		{
		}
		return "".equals(topmsg) || "MVA0000".equals(topmsg);
	}

	public Object writeLog(final TelCommExecWrapper execwrapper, final Map _params, final MVHImpl result)
	{

		DecimalFormat dft = new DecimalFormat("#0");
		DecimalFormat dftdot2 = new DecimalFormat("#0.00");

		Map<String, String> params = _params;
		// 建立交易紀錄log 並檢查是否需更新預約檔
		TXNFXRECORD record = execwrapper.createTxnFxRecordCheckupdatePayData(_params, txnFxSchPayDataDao);

		/*** For 存入 Txnlog ***/
		// BOLifeMoniter.setValue("FXADTXNO", record.getADTXNO());

		record.setADOPID("F002");
		record.setFXUSERID(params.get("UID"));//
		record.setFXWDAC(params.get("CUSTACC")); // 轉出帳號
		record.setFXSVBH(""); // 轉入分行
		record.setFXSVAC(StrUtils.trim(params.get("BENACC"))); // 轉入帳號
		record.setFXWDCURR(params.get("PAYCCY")); // 轉出幣別
		record.setFXSVCURR(params.get("REMITCY")); // 轉入幣別

		if (params.get("F001_ACCCUR") != null)
		{
			try
			{
				record.setFXWDAMT(dftdot2.format(dft.parse(params.get("F001_ACCAMT")).doubleValue() / 100d)); // 轉出金額
				record.setFXSVAMT(dftdot2.format(dft.parse(params.get("F001_CURAMT")).doubleValue() / 100d)); // 轉入金額
			}
			catch (Exception e)
			{
				log.error("{}", e);
			}
		}
		else
		{
			try
			{
				if (params.get("PAYREMIT").toString().equals("1"))
				{ // 轉帳類型為"轉出"

					double d_Div = 1d;
					if (((String) params.get("OUT_AMT")).startsWith("0"))
						d_Div = 100d;

					record.setFXWDAMT(dftdot2.format(dft.parse(params.get("OUT_AMT")).doubleValue() / d_Div)); // 轉出金額
				} // end (PAYREMIT==1)
			}
			catch (Exception e)
			{
				log.error("{}", e);
			}
			try
			{
				if (params.get("PAYREMIT").toString().equals("2"))
				{
					// 轉帳類型為"轉入"
					double d_Div = 1d;
					if (((String) params.get("IN_AMT")).startsWith("0"))
						d_Div = 100d;
					record.setFXSVAMT(dftdot2.format(dft.parse(params.get("IN_AMT")).doubleValue() / d_Div)); // 轉入金額
				} // end (PAYREMIT==2)
			}
			catch (Exception e)
			{
				log.error("{}", e);
			}
		}

		StringBuffer buf = new StringBuffer();
		if (params.get("MEMO1") != null)
			buf.append((String) params.get("MEMO1"));
		if (params.get("MEMO2") != null)
			buf.append("<br>" + (String) params.get("MEMO2"));
		if (params.get("MEMO3") != null)
			buf.append("<br>" + (String) params.get("MEMO3"));
		if (params.get("MEMO4") != null)
			buf.append("<br>" + (String) params.get("MEMO4"));
		record.setFXTXMEMO(buf.toString());
		record.setFXTXMAILS(params.get("CMTRMAIL"));
		record.setFXTXMAILMEMO(params.get("CMMAILMEMO")); // CMMAILMEMO 對應的欄位
		record.setFXEXRATE(StrUtils.trim(params.get("RATE")));// 匯率
		try
		{
			String tmp1 = (params.get("COMMAMT") == null ? ""
					: NumericUtil.formatNumberString(params.get("COMMAMT"), 2));
			String tmp2 = (params.get("DISTOTH") == null ? ""
					: NumericUtil.formatNumberString(params.get("DISTOTH"), 2));
			record.setFXEFEE(tmp1 + "/" + tmp2);// 手續費 + 郵電費
			record.setFXEFEECCY(params.get("COMMCCY")); // 手續費幣別
		}
		catch (Exception e)
		{
			log.error("{}", e);
		}
		record.setFXTXCODE(params.get("FGTXWAY"));
		record.setFXMSGSEQNO(StrUtils.trim(params.get("__FXMSGSEQNO")));

		/*** New Add ***/
		record.setFXMSGCONTENT((params.get("__FXMSGCONTENT") == null) ? "" : params.get("__FXMSGCONTENT"));
		
		// 欄位長度過長資料庫塞不進去，故remove
		params.remove("pkcs7Sign"); 
		params.remove("jsondc");
		params.remove("__SIGN");
		params.remove("__F001_RESULT");
		params.remove("__F003_RESULT");
		log.debug(ESAPIUtil.vaildLog("F002 record >>>>>>>>>>>>> " + record.toString()));
		record.setFXTITAINFO(CodeUtil.toJson(params));

		// 寫入 txnFxRecord
		txnFxRecordDao.save(record);

		return record;
	}

	public TelcommResult doF007A(Map<String, String> params)
	{
		// TODO 組合議價電文所需要的欄位
		Date d = new Date();
		params.put("CMTRDATE", DateTimeUtils.format("yyyyMMdd", d));
		params.put("CMTRTIME", DateTimeUtils.format("HHmmss", d));

		params.put("FYACN", params.get("CUSTACC")); // 轉出帳號
		// params.put("INACN", params.get("BENACC")); //轉入帳號
		params.put("INACN", ""); // 轉入帳號
		params.put("OUT_CRY", params.get("PAYCCY")); // 轉出幣別
		params.put("IN_CRY", params.get("REMITCY")); // 轉入幣別

		if ("1".equals(params.get("PAYREMIT")))
		{
			params.put("AMTTYPE", "1");// 金額類別
			params.put("CURAMT", params.get("CURAMT"));// 轉出金額 (議價金額)
			params.put("OUT_AMT", params.get("CURAMT"));// 轉出金額 (議價金額) => 保留原始輸入欄位值
		}
		else
		{ // "2".equals(params.get("PAYREMIT"))
			params.put("AMTTYPE", "2");// 金額類別
			params.put("ATRAMT", params.get("CURAMT"));// 轉入金額
			params.put("IN_AMT", params.get("CURAMT"));// 轉入金額 => 保留原始輸入欄位值
		}

		params.put("FLAG", "A");// 交易狀態
		/*
		 * 如果電文失敗或寫DB發生任何錯誤, 則發 F007的電文 但是 FLAG 內容為 C 的電文
		 */
		TopMessageException lastException = null;
		TelcommResult result = null; // 議價電文
		try
		{
			result = f007Telcomm.query(params); // 議價電文
		}
		catch (Exception e)
		{
			if (e instanceof TopMessageException)
			{
				lastException = (TopMessageException) e;
				throw lastException;
			}
			else
			{
				throw new ToRuntimeException(e.getMessage(), e);
			}
		}
		return result;
	}

	/**
	 * 檢查 MERVA 回應之訊息代碼: 1.若存在於資料庫 && 註記可系統/人工重送("Y") ==> 進系統/人工重送處理 2.其它 ==> 一律作 F005 沖正
	 */
	public boolean checkMervaTopMsg(String topmsg)
	{

		boolean isFail = false;
		String fxFlag = admMsgCodeDao.findFXRESEND(topmsg);
		String fxAutoFlag = admMsgCodeDao.findFXAUTORESEND(topmsg);
		log.debug("checkMervaTopMsg topmsg >>>>{}", topmsg);
		log.debug(ESAPIUtil.vaildLog("checkMervaTopMsg fxFlag >>>>" + fxFlag));
		log.debug(ESAPIUtil.vaildLog("checkMervaTopMsg fxAutoFlag >>>>{}" + fxAutoFlag));
		if (TopMessageException.isInDB(topmsg) && (fxFlag.equals("Y") || fxAutoFlag.equals("Y")))
		{
			isFail = false; // 進系統/人工重送處理
		}
		else
		{
			isFail = true; // 作 F005 沖正
		}

		return isFail;
	}

	/**
	 * 檢查電文回應之訊息代碼:
	 * 
	 * 1.若存在於資料庫 && 註記可自動重送("Y") ==> 進自動重送處理 2.若存在於資料庫 && 註記可人工重送("Y") ==> 進人工重送處理 3.其它 ==> 一律交易結束
	 */
	public boolean checkMervaTopMsg(String topmsg, String schFlag)
	{

		boolean isFail = false;

		String fxFlag = admMsgCodeDao.findFXRESEND(topmsg);
		String fxAutoFlag = admMsgCodeDao.findFXAUTORESEND(topmsg);

		log.debug(ESAPIUtil.vaildLog("checkMervaTopMsg topmsg >>>>{}"+ topmsg));
		log.debug(ESAPIUtil.vaildLog("checkMervaTopMsg schFlag >>>>{}"+ schFlag));
		log.debug(ESAPIUtil.vaildLog("checkMervaTopMsg fxFlag >>>>{}"+ fxFlag));
		log.debug(ESAPIUtil.vaildLog("checkMervaTopMsg fxAutoFlag >>>>{}"+ fxAutoFlag));

		log.debug(ESAPIUtil.vaildLog("@@@ F002T.checkMervaTopMsg() schFlag == " + schFlag));

		// 即時交易
		if (schFlag == null)
		{
			if (TopMessageException.isInDB(topmsg) && fxFlag.equals("Y"))
			{
				isFail = false; // 進人工重送處理
			}
			else
			{
				isFail = true; // 交易結束
			}
		}
		// 預約交易
		else
		{
			if (TopMessageException.isInDB(topmsg) && fxAutoFlag.equals("Y"))
			{
				isFail = false; // 進自動重送處理
			}
			else
			{
				isFail = true; // 交易結束
			}
		}
		return isFail;
	}
}
