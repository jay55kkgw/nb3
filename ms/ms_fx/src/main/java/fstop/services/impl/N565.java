package fstop.services.impl;

import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.model.GrpColumns;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.MVHUtils;
import fstop.model.RowsGroup;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N565 extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());
	@Autowired
    @Qualifier("n565Telcomm")
	private TelCommExec n565Telcomm;
	
//	@Required
//	public void setN565Telcomm(TelCommExec n565Telcomm) {
//		this.n565Telcomm = n565Telcomm;
//	}
	
	@Override
	public MVH doAction(Map params) {
		String enddate = "";
		String stadate = "";
		String periodStr = "";
	
		stadate = (String) params.get("CMSDATE");
		enddate = (String) params.get("CMEDATE");
		stadate = StrUtils.trim(stadate).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
		enddate = StrUtils.trim(enddate).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
		
		if (stadate != null && stadate.length() == 8) {
			Date sd = DateTimeUtils.parse("yyyyMMdd", stadate);
			stadate = DateTimeUtils.format("yyyy-MM-dd", sd);
			periodStr = DateTimeUtils.format("yyyy/MM/dd", sd);
		}
		if (enddate != null && enddate.length() == 8) {
			Date sd = DateTimeUtils.parse("yyyyMMdd", enddate);
			enddate = DateTimeUtils.format("yyyy-MM-dd", sd);
			periodStr += "~" + DateTimeUtils.format("yyyy/MM/dd", sd);
		} else { // 若未指定結束時間, 則使用今天日期
			enddate = DateTimeUtils.getDateShort(new Date());
			periodStr += "~" + DateTimeUtils.format("yyyy/MM/dd", new Date());
		}
		params.put("FDATE", stadate);
		params.put("TDATE", enddate);
		
		if ("TRUE".equals(params.get("OKOVNEXT"))) {
			params.put("QUERYNEXT", "");
		}	
				
		MVHImpl result = n565Telcomm.query(params);
		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		result.getFlatValues().put("CMRECNUM", result.getValueOccurs("RREFNO") + "");
		result.getFlatValues().put("CMPERIOD", periodStr);
		
		try {
			String[] groupBy = new String[]{"RREMITCY"};
			//RowsGroup grp = MVHUtils.group(result.getOccurs(), groupBy);			
			RowsGroup grp = MVHUtils.group(result.getOccursByUID("CUSIDN", (String)params.get("UID")), groupBy);
			
			Map<GrpColumns, Double> sum = grp.sum("RREMITAM");
			
			int ind = 1;
			for(Entry<GrpColumns, Double> ent : sum.entrySet()) {
				GrpColumns grpCols = ent.getKey();
	
				Map<String, String> m = MVHUtils.toFlatValues(grpCols, "AMT", ind);

				result.getFlatValues().putAll(m);
				result.getFlatValues().put("FXSUBAMT_" + ind, ent.getValue() + "");				
				result.getFlatValues().put("FXSUBAMTRECNUM_" + ind, grp.getGroups().get(grpCols).getSelectedRows().size() + "");
				
				ind++;
			}
		}
		finally {}
		
		
		return result;
	}
}
