package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N175_1 extends CommonService  {
	
	@Autowired
	@Qualifier("n175_1Telcomm")
	private TelCommExec n175_1Telcomm;
	
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	@SuppressWarnings("unchecked")
	public MVH doAction(Map params) { 
		Date d = new Date();
		params.put("DATE", DateTimeUtils.getCDateShort(d));
		params.put("TIME", DateTimeUtils.format("HHmmss", d));
		
		String stadate = (String)params.get("DPISDT");  // 民國年月日
		String enddate = (String)params.get("DUEDAT");  // 民國年月日
		stadate = StrUtils.trim(stadate).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
		enddate = StrUtils.trim(enddate).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
		
		params.put("DPISDT", stadate);
		params.put("DUEDAT", enddate);		
		params.put("TXNTYPE", "01");

		TelcommResult telcommResult = n175_1Telcomm.query(params);
		telcommResult.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));

		return telcommResult;
	}



}
