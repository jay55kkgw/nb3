package fstop.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.rest.comm.bean.DoIDGateCommand_In;
import com.netbank.rest.comm.controller.CommonIDGATEController;
import com.netbank.rest.model.pool.FxUtils;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import encryptUtil.EncryptUtil;
import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.AdmCurrencyDao;
import fstop.orm.dao.TxnFxRecordDao;
import fstop.orm.dao.TxnFxSchPayDao;
import fstop.orm.dao.TxnFxSchPayDataDao;
import fstop.orm.dao.TxnTwSchPayDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.ADMCURRENCY;
import fstop.orm.po.ADMMAILLOG;
import fstop.orm.po.TXNFXRECORD;
import fstop.orm.po.TXNFXSCHPAY;
import fstop.orm.po.TXNFXSCHPAYDATA;
import fstop.services.BookingAware;
import fstop.services.CommonService;
import fstop.services.NoticeInfo;
import fstop.services.NoticeInfoGetter;
import fstop.services.OnLineTransferNotice;
import fstop.services.TelCommExecWrapper;
import fstop.services.TransferNotice;
import fstop.services.WriteLogInterface;
import fstop.telcomm.TelCommExec;
import fstop.telcomm.cmd.CommandUtils;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import topibs.utility.NumericUtil;

/**
 * 轉入外匯綜存定存
 * 
 * @author Owner
 *
 */
@Slf4j
public class N174 extends CommonService implements BookingAware, WriteLogInterface {

	@Autowired
	@Qualifier("n174Telcomm")
	private TelCommExec n174Telcomm;

	@Autowired
	private FxUtils util;

	@Autowired
	private TxnFxRecordDao txnFxRecordDao;

	@Autowired
	private TxnFxSchPayDao txnFxSchPayDao;

	@Autowired
	private AdmCurrencyDao admCurrencyDao;

	@Autowired
	private TxnUserDao txnUserDao;

	@Autowired
	private TxnFxSchPayDataDao txnFxSchPayDataDao; // 預約資料

	@Autowired
	CommonIDGATEController commonIDGATEController;

	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	@SuppressWarnings("unchecked")
	public MVH doAction(Map _params) {
		Map<String, String> params = _params;

		String fginacno = params.get("FGINACNO"); // 1 約定帳號, 2 非約定帳號
		String trin_bnkcod = "";
		String trin_acn = "";
		String trin_agree = "";
		if ("CMDAGREE".equals(fginacno)) {
			Map<String, String> jm = JSONUtils.json2map(params.get("INACNO1"));
			trin_bnkcod = jm.get("BNKCOD");
			trin_acn = jm.get("ACN");
			trin_agree = jm.get("AGREE");
		} else if ("CMDISAGREE".equals(fginacno)) { // 非約定, 只能是台企銀 050
			trin_bnkcod = "050";
			trin_acn = params.get("INACNO2");
			trin_agree = "0"; // 0: 非約定, 1: 約定
		}

		params.put("__TRIN_BANKCOD", trin_bnkcod);
		params.put("__TRIN_ACN", trin_acn);
		params.put("__TRIN_AGREE", trin_agree);

		params.put("FYTSFAN", params.get("FYTSFAN")); // 轉出帳號
		params.put("OUTCRY", params.get("OUTCRY")); // 轉帳幣別

		params.put("TSFACN", (String) params.get("__TRIN_ACN"));

		// params.put("TYPCOD", params.get("TYPCOD")); //與送上來的欄位名稱相同. //存款期別

		params.put("AMOUNT", params.get("AMOUNT") + "." + params.get("AMOUNT_DIG"));

		params.put("INTMTH", params.get("INTMTH")); // 計息方式 0: 機動, 1: 固定

		if ("C".equals(params.get("CODE")) || "D".equals(params.get("CODE"))) {
			// 不轉存
		} else {
			// 轉存
			params.put("AUTXFTM", params.get("AUTXFTM") == null ? "" : (String) params.get("AUTXFTM"));
		}

		String fgtrdate = params.get("FGTRDATE");
		if ("0".equals(fgtrdate)) {
			// 即時
			if (!util.isFxBizTime()) {
				throw TopMessageException.create("Z400", "N174");
			}

			OnLineTransferNotice trNotice = new OnLineTransferNotice();

			return online(params, trNotice);
		} else if ("1".equals(fgtrdate) || "2".equals(fgtrdate)) {
			// 預約
			return booking(params);
		}

		return new MVHImpl();
	}

	public static boolean checkSuccessTopMsg(String topmsg) {
		// 若回傳為 0000, 則回傳 true
		try {
			Double d = Double.parseDouble(topmsg);
			if (d == 0d)
				return true;
		} catch (Exception e) {
		}

		return false;
	}

	public MVHImpl online(final Map<String, String> params, TransferNotice trNotice) {

		Date d = new Date();
		params.put("DATE", DateTimeUtils.getCDateShort(d));
		params.put("TIME", DateTimeUtils.format("HHmmss", d));

		//IDGATE CertACN修改=========start=====
		log.trace("<<< certACN-adding >>>");
		log.trace("fgtxway="+params.get("FGTXWAY"));
		String TxWay = params.get("FGTXWAY");
		String certACN = "";
		if ("7".equals(TxWay)) {
			while (certACN.length() < 19)
				certACN += "C";
		}		
		params.put("CERTACN", certACN);
		log.trace("CERTACN="+params.get("CERTACN"));
		//IDGATE CertACN修改=========end=====
		final TelCommExecWrapper execwrapper = new TelCommExecWrapper(n174Telcomm);

		MVHImpl result = execwrapper.query(params); // N174 電文

		final TXNFXRECORD record = (TXNFXRECORD) writeLog(execwrapper, params, result);

		final TelCommExecWrapper execwrapperFinal = execwrapper;
		trNotice.sendNotice(new NoticeInfoGetter() {

			public NoticeInfo getNoticeInfo() {
				NoticeInfo result = new NoticeInfo(params.get("UID"), params.get("DPMYEMAIL"), params.get("CMTRMAIL"));
				result.setTemplateName("FXDEPNOTIFY");

				// result.setDpretxstatus(record.getFXRETXSTATUS());
				result.setADTXNO(record.getADTXNO());

				result.setException(execwrapperFinal.getLastException());
				String datetime = record.getFXTXDATE() + record.getFXTXTIME();
				Date d = DateTimeUtils.parse("yyyyMMddHHmmss", datetime);
				String trantime = DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", d);

				result.getParams().put("#TXNAME", "外匯活存轉入外匯綜存定存");
				result.getParams().put("#TRANTIME", trantime);
				result.getParams().put("#SEQNO", "");

				String acn = record.getFXSVAC();
				try {
					acn = StrUtils.left(acn, acn.length() - 5) + "***" + StrUtils.right(acn, 2);
				} catch (Exception e) {
				}

				String outacn = record.getFXWDAC();
				try {
					outacn = StrUtils.left(outacn, outacn.length() - 5) + "***" + StrUtils.right(outacn, 2);
				} catch (Exception e) {
				}

				result.getParams().put("#ACN", acn);
				result.getParams().put("#OUTACN", outacn);
				result.getParams().put("#OUTCURRENCY", record.getFXWDCURR());
				String dptxamtfmt = NumericUtil.formatNumberString(record.getFXWDAMT(), 2);
				result.getParams().put("#AMT", dptxamtfmt);
				result.getParams().put("#MEMO", params.get("CMMAILMEMO"));

				ADMMAILLOG admmaillog = new ADMMAILLOG();
				admmaillog.setADACNO(record.getFXWDAC());

				String trans_status = (String) params.get("__TRANS_STATUS");
				if (StrUtils.isEmpty(trans_status)) {
					trans_status = "FXONLINE";
				} else if ("SEND".equals(trans_status)) {
					trans_status = "FXSCH";
				} else if ("RESEND".equals(trans_status)) {
					trans_status = "FXSCHRE";
				}

				admmaillog.setADBATCHNO(trans_status);
				admmaillog.setADMAILACNO(StrUtils.trim(params.get("CMTRMAIL")));
				admmaillog.setADUSERID(record.getFXUSERID());
				admmaillog.setADUSERNAME("");
				admmaillog.setADSENDTYPE("3");
				admmaillog.setADSENDTIME(datetime);
				admmaillog.setADSENDSTATUS("");

				result.setADMMAILLOG(admmaillog);

				return result;
			}

		});

		execwrapper.throwException();

		return result;
	}

	/**
	 * 預約交易
	 * 
	 * @param params
	 * @return
	 */
	@Transactional
	public MVHImpl booking(Map<String, String> params) {
		MVHImpl mvhImpl = new MVHImpl();

		try {
			TXNFXSCHPAY schedule = new TXNFXSCHPAY();
			//IDGATE CertACN修改=========start=====
			String TxWay = params.get("FGTXWAY");
			String certACN = "";
			if ("7".equals(TxWay)) {
				while (certACN.length() < 19)
					certACN += "D";
			}
			//IDGATE CertACN修改=========end=====
			log.info("***************************************預約編號***************************************************");
			// 預約編號(YYMMDD+三位流水號)
			String dptmpschno = "";
			dptmpschno = txnUserDao.getDptmpschno((String) params.get("UID"));
			log.info("***************************************預約編號***************************************************");
			params.put("ADREQTYPE", "S"); // 寫入 TXNLOG 時, S 表示為預約

			String sha1_Mac = "";
			String reqinfo_Str = "{}";

			String f = params.get("FGTRDATE"); // 判斷預約周期
			if ("1".equals(f)) { // 預約某一天
				params.put("CMTRDATE", StrUtils.trim(params.get("CMTRDATE")).replaceAll("/", ""));
				if (params.get("CMTRDATE").length() == 0) {
					throw TopMessageException.create("ZX99");
				} else {
					schedule.setFXPERMTDATE("");
					schedule.setFXFDATE(params.get("CMTRDATE"));
					schedule.setFXTDATE(params.get("CMTRDATE"));
					schedule.setFXTXTYPE("S");
				}
			} else { // 周期, 每月某一日
				params.put("CMSDATE", StrUtils.trim(params.get("CMSDATE")).replaceAll("/", ""));
				params.put("CMEDATE", StrUtils.trim(params.get("CMEDATE")).replaceAll("/", ""));
				if (params.get("CMPERIOD").length() == 0 || params.get("CMSDATE").length() == 0
						|| params.get("CMEDATE").length() == 0) {
					throw TopMessageException.create("ZX99");
				} else {
					schedule.setFXPERMTDATE(params.get("CMPERIOD"));
					schedule.setFXFDATE(params.get("CMSDATE"));
					schedule.setFXTDATE(params.get("CMEDATE"));
					schedule.setFXTXTYPE("C");
				}
			}

			schedule.setFXUSERID(params.get("UID"));// 使用者ID
			schedule.setFXWDAC(params.get("FYTSFAN")); // 轉出帳號
			schedule.setFXSVBH(params.get("__TRIN_BANKCOD")); // 轉入分行
			schedule.setFXSVAC(params.get("__TRIN_ACN")); // 轉入帳號

			ADMCURRENCY cur = admCurrencyDao.findByAdccyno(params.get("OUTCRY")); // OUTCRY 裡放的是 ADCCYNO
			if (cur != null) {
				schedule.setFXWDCURR(cur.getADCURRENCY()); // 轉出幣別
				schedule.setFXSVCURR(cur.getADCURRENCY()); // 轉入幣別
			}
			schedule.setFXWDAMT(params.get("AMOUNT")); // 轉出金額

			schedule.setFXTXMEMO(params.get("CMTRMEMO"));
			schedule.setFXTXMAILS(params.get("CMTRMAIL"));
			schedule.setFXTXMAILMEMO(params.get("CMMAILMEMO")); // CMMAILMEMO 對應的欄位

			//交易機制驗證
			//Ikey驗證
			if (params.containsKey("pkcs7Sign") && "1".equals(params.get("FGTXWAY"))) {
				List<String> commands = new ArrayList();
				commands.add("XMLCA()");
				commands.add("XMLCN()");

				CommandUtils.doBefore(commands, params);

				if (!StrUtils.trim(params.get("XMLCN")).toUpperCase()
						.startsWith(StrUtils.trim(params.get("UID")).toUpperCase())) {
					throw TopMessageException.create("Z089");
				}

				schedule.setXMLCA(StrUtils.trim(params.get("XMLCA")));
				schedule.setXMLCN(StrUtils.trim(params.get("XMLCN")));
			}
			// IDGATE驗證-----start
			else if ("7".equals(params.get("FGTXWAY"))) {
				DoIDGateCommand_In in = new DoIDGateCommand_In();
				in.setIN_DATAS(params);
				HashMap rtnMap = commonIDGATEController.doIDGateCommand(in);
				if (!(boolean) rtnMap.get("result")) {
					log.error("N174 BOOKING CHECK IDGateCommand() ERROR");
					switch ((String) rtnMap.get("errMsg")) {
					case "Z300":
						throw TopMessageException.create("Z300");
					case "FE0011":
						throw TopMessageException.create("FE0011");
					default:
						throw TopMessageException.create("ZX99");
					}
				}

			}
			// IDGATE驗證-----end
			
			// 因IKEY資料太長 先移除避免塞爆欄位
			params.remove("pkcs7Sign");
			params.remove("__SIGN");
			// 預約時上行電文必要資訊
			log.debug(ESAPIUtil.vaildLog("txntwschpay.params>>>{}" + CodeUtil.toJson(params)));
			reqinfo_Str = CodeUtil.toJson(params);
			sha1_Mac = EncryptUtil.toEncS1(reqinfo_Str, "UTF-8").substring(0, 4);
			//
			schedule.setFXSCHNO(dptmpschno);//// 預約編號
			schedule.setMAC(sha1_Mac);
			schedule.setFXTXINFO(reqinfo_Str);
			// 設定時間日期
			Date d = new Date();
			schedule.setFXSDATE(DateTimeUtils.format("yyyyMMdd", d));
			schedule.setFXSTIME(DateTimeUtils.format("HHmmss", d));
			schedule.setFXTXSTATUS("0");// 0: 預約
			schedule.setLASTDATE(schedule.getFXSDATE());
			schedule.setLASTTIME(schedule.getFXSTIME());
			// 設定時間日期 -END
			schedule.setADOPID("N174");// 操作功能ID(NB3SysOp.ADOPID)
			schedule.setFXTXCODE(params.get("FGTXWAY"));// 交易機制
			schedule.setLOGINTYPE(params.get("LOGINTYPE"));// NB登入或MB登入
			schedule.setMSADDR(TxnTwSchPayDao.MS_FX);// 所屬微服務

			log.debug(ESAPIUtil.vaildLog("schedule >>>>> " + schedule.toString()));

			txnFxSchPayDao.save(schedule);

			String reqinfo_addTXTime = "";
			String sha1_MacTime = "";

//			List<TXNFXSCHPAYDATA> dataList = txnFxSchPayDataDao.findByFXSCHNO(dptmpschno);
//			for (TXNFXSCHPAYDATA data : dataList) {
//				params.put("FXSCHTXDATE", data.getPks().getFXSCHTXDATE());
//				reqinfo_addTXTime = CodeUtil.toJson(params);
//				sha1_MacTime = EncryptUtil.toEncS1(reqinfo_addTXTime, null).substring(0, 4);
//				data.setMAC(sha1_MacTime);
//				txnFxSchPayDataDao.update(data);
//			}

			mvhImpl.getFlatValues().put("MSGCOD", "");
			mvhImpl.getFlatValues().put("TRDATE", params.get("TRDATE"));
			mvhImpl.getFlatValues().put("TSFACN", params.get("TSFACN"));
			mvhImpl.getFlatValues().put("CMTRMEMO", params.get("CMTRMEMO"));
		} catch (Exception e) {
			log.error("booking error >>{}", e);
		}
		return mvhImpl;
	}

	/**
	 * 紀錄 log
	 */
	public Object writeLog(TelCommExecWrapper execwrapper, Map<String, String> params, final MVHImpl result) {
		// 建立交易紀錄log 並檢查是否需更新預約檔
		TXNFXRECORD record = execwrapper.createTxnFxRecordCheckupdatePayData(params, txnFxSchPayDataDao);

		String date = record.getLASTDATE();
		String time = record.getLASTTIME();

		if (result != null) {
			try {
				date = DateTimeUtils.format("yyyyMMdd",
						DateTimeUtils.getCDate2Date(result.getValueByFieldName("TRNDATE")));
			} catch (Exception e) {
			}
			try {
				time = result.getValueByFieldName("TRNTIME");
			} catch (Exception e) {
			}
		}
		try {
			record.setADOPID("N174");
			record.setFXTXDATE(date);
			record.setFXTXTIME(time);
			record.setFXUSERID(params.get("UID"));
			record.setFXWDAC(params.get("FYTSFAN")); // 轉出帳號
			record.setFXSVBH(params.get("__TRIN_BANKCOD")); // 轉入分行
			record.setFXSVAC(params.get("__TRIN_ACN")); // 轉入帳號

			ADMCURRENCY cur = admCurrencyDao.findByAdccyno(params.get("OUTCRY")); // OUTCRY 裡放的是 ADCCYNO
			record.setFXWDCURR(cur.getADCURRENCY()); // 轉出幣別
			record.setFXSVCURR(cur.getADCURRENCY()); // 轉入幣別
			record.setFXWDAMT(params.get("AMOUNT")); // 轉出金額
			record.setFXSVAMT(""); // 轉入金額
			record.setFXTXMEMO(params.get("CMTRMEMO"));
			record.setFXTXMAILS(params.get("CMTRMAIL"));
			record.setFXTXMAILMEMO(params.get("CMMAILMEMO")); // CMMAILMEMO 對應的欄位
			// 欄位長度過長資料庫塞不進去，故remove
			params.remove("pkcs7Sign");
			params.remove("jsondc");
			params.remove("__SIGN");
			record.setFXTITAINFO(CodeUtil.toJson(params));
			log.debug(ESAPIUtil.vaildLog("N174 record >>>>>>>>>>>>> " + record.toString()));
			txnFxRecordDao.save(record);

		} catch (Exception e) {
			log.error("writeLog error", e);
		}
		return record;

	}
}
