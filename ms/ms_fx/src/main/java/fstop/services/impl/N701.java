package fstop.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.SysDailySeqDao;
import fstop.services.CommonService;
import fstop.services.OnLineTransferNotice;
import fstop.services.TransferNotice;
import fstop.telcomm.cmd.CommandUtils;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import fstop.ws.eai.client.N701WebServiceClient;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N701 extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	private N701WebServiceClient n701WSClient;	

	@Autowired
	private SysDailySeqDao sysDailySeqDao;
		
//	@Required
//	public void setN701WSClient(N701WebServiceClient n701WSClient) {
//		this.n701WSClient = n701WSClient;
//	}
//	
//	@Required
//	public void setSysDailySeqDao(SysDailySeqDao sysDailySeqDao) {
//		this.sysDailySeqDao = sysDailySeqDao;
//	}
			
	public N701(){}

	@Override
	public MVH doAction(Map _params) {
		Map<String, String> params = _params;
		
		OnLineTransferNotice trNotice = new OnLineTransferNotice();
		
		return online(params, trNotice);
	}

	public MVHImpl online(final Map<String, String> params, final TransferNotice trNotice) {
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);		
			
		//產生 CERTACN update by Blair -------------------------------------------- Start
		String TxWay = params.get("FGTXWAY");
		String certACN = "";
			
		if("0".equals(TxWay)){
			while(certACN.length() < 19)certACN += " ";
		}
			
		params.put("CERTACN", certACN);
		//產生 CERTACN update by Blair -------------------------------------------- End
		
		params.put("FLAG", "0");
		
		params.put("BNKRA", "050");   // 行庫別
		params.put("TRXCOD", "TVO9MBBT");
		params.put("FILL01", "");
		params.put("TRANNAME", "N951");
		params.put("HEADER", "N951");
		params.put("VERSION", "NB08");	
		params.put("PPSYNC", "");
		params.put("PPSYNCN", "");
		
		params.put("CUSIDN", params.get("UID"));
		params.put("ACN", params.get("ACN"));
		params.put("PCSEQ", StrUtils.right("00000" + sysDailySeqDao.dailySeq("N701")+"", 5));
		params.put("TXID1", params.get("TXID1"));
		params.put("PATP", params.get("PATP"));			
		
		List<String> commands = new ArrayList();
		commands.add("SYNC()");
		commands.add("PPSYNC()");
		commands.add("PPSYNCN()");
		commands.add("PINNEW()");
		CommandUtils.doBefore(commands, params);		
		
		MVHImpl n701result1 = new MVHImpl();
		try {
			n701result1 = n701WSClient.processGo(params);
			if(n701result1.getValueByFieldName("SYNC").equals("0302"))
				n701result1.getFlatValues().put("RSPCOD","0302");
			if(n701result1.getValueByFieldName("SYNC").equals("E089"))
				n701result1.getFlatValues().put("RSPCOD","E089");
			n701result1.getFlatValues().put("PCSEQ",n701result1.getValueByFieldName("PCSEQ"));
			n701result1.getFlatValues().put("STAN",n701result1.getValueByFieldName("STAN"));
			n701result1.getFlatValues().put("ACN",n701result1.getValueByFieldName("ACN"));
			n701result1.getFlatValues().put("DATE",n701result1.getValueByFieldName("DATE"));
			n701result1.getFlatValues().put("TIME",n701result1.getValueByFieldName("TIME"));			
				
			if(!"".equals(n701result1.getValueByFieldName("RSPCOD").trim()) && !"0000".equals(n701result1.getValueByFieldName("RSPCOD").trim()))
			{
				throw TopMessageException.create(n701result1.getValueByFieldName("RSPCOD").trim(),params.get("ADOPID")==null ? "" : params.get("ADOPID").toString());
			}
			if("".equals(n701result1.getValueByFieldName("RSPCOD").trim()) &&  "".equals(n701result1.getValueByFieldName("ACN").trim()))
			{
				if(n701result1.getValueByFieldName("SYNC").length()>0)
				{	
					n701result1.getFlatValues().put("RSPCOD",n701result1.getValueByFieldName("SYNC"));
					throw TopMessageException.create(n701result1.getValueByFieldName("SYNC"),params.get("ADOPID")==null ? "" : params.get("ADOPID").toString());	
				}
				else
				{	
					n701result1.getFlatValues().put("RSPCOD","ZX99");
					throw TopMessageException.create("ZX99",params.get("ADOPID")==null ? "" : params.get("ADOPID").toString());	
				}	
			}						
		}
		catch(Exception e) {
			log.warn("N701.java EAI Exception:"+e.getMessage());
			throw TopMessageException.create(n701result1.getValueByFieldName("RSPCOD")==null ? "ZX99" : n701result1.getValueByFieldName("RSPCOD"),params.get("ADOPID")==null ? "" : params.get("ADOPID").toString());
		}		
		return n701result1;		
	}
}
