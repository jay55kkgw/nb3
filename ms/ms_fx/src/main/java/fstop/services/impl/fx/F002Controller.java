package fstop.services.impl.fx;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.netbank.rest.model.CommonPools;
import com.netbank.util.CodeUtil;

import fstop.exception.ToRuntimeException;
import fstop.exception.UncheckedException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.orm.po.ADMMAILLOG;
import fstop.orm.po.TXNFXRECORD;
import fstop.services.BookingAware;
import fstop.services.NoticeInfo;
import fstop.services.NoticeInfoGetter;
import fstop.services.TelCommExecWrapper;
import fstop.services.TransferNotice;
import fstop.services.WriteLogInterface;
import fstop.services.impl.F002T;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import topibs.utility.NumericUtil;

@Slf4j
public class F002Controller implements BookingAware, WriteLogInterface
{

	@Autowired
	private F002T f002t;

	@Autowired
	private CommonPools commonPools;

	public MVHImpl booking(Map<String, String> params)
	{

		return null;
	}

	public MVHImpl online(final Map<String, String> params, final TransferNotice trNotice)
	{

		// logger.info("{{{\r\n" + JSONUtils.map2json(params) + "\r\n}}}");
		String paydate = StrUtils.trim(params.get("PAYDATE")).replaceAll("/", "");
		params.put("PAYDATE", paydate);

		final F002_F007YTelcomm f007y = new F002_F007YTelcomm(f002t);
		final F002_F007ATelcomm f007a = new F002_F007ATelcomm(f002t);
		final F002_F001Telcomm f001 = new F002_F001Telcomm(f002t);
		final F002_F003Telcomm f003 = new F002_F003Telcomm(f002t, f001);
		final F002_MervaTelcomm merva = new F002_MervaTelcomm(f002t, f001);
		final F002_F011Telcomm f011 = new F002_F011Telcomm(f002t);

		final List<FxTelCommStep> actions = new ArrayList<FxTelCommStep>();
		String trans_status = (String) params.get("__TRANS_STATUS");
		if (StrUtils.isEmpty(trans_status))
		{
			trans_status = "ONLINE";
		}

		boolean isScheduleRecord = (StrUtils.trim(params.get("__SCHID")).length() > 0
				&& !"0".equals(StrUtils.trim(params.get("__SCHID"))));

		if ("ONLINE".equalsIgnoreCase(trans_status) || ("RESEND".equalsIgnoreCase(trans_status) && !isScheduleRecord))
		{
			if (!StrUtils.trim(params.get("PAYCCY")).equalsIgnoreCase(params.get("REMITCY")))
				actions.add(f007y);
			actions.add(f001);
			actions.add(f003);
			actions.add(merva);
			actions.add(f011);
		}
		else
		{ // 預約發送
			if (!StrUtils.trim(params.get("PAYCCY")).equalsIgnoreCase(params.get("REMITCY")))
				actions.add(f007a);
			actions.add(f001);
			actions.add(f003);
			actions.add(merva);
			actions.add(f011);
		}

		String FXMSGSEQNO = StrUtils.trim(params.get("__FXMSGSEQNO"));

		if (FXMSGSEQNO.length() == 0)
		{ // 尚未開始執行
			f007a.setSkip(false);
			f007y.setSkip(false);
			f001.setSkip(false);
			f003.setSkip(false);
			merva.setSkip(false);
			f011.setSkip(false);
		}
		else if (FXMSGSEQNO.endsWith("0"))
		{ // F007 成功
			f007a.setSkip(true);
			f007y.setSkip(true);
			f001.setSkip(false);
			f003.setSkip(false);
			merva.setSkip(false);
			f011.setSkip(false);
		}
		else if (FXMSGSEQNO.endsWith("1"))
		{ // F001 成功
			f007a.setSkip(true);
			f007y.setSkip(true);
			f001.setSkip(true);
			f003.setSkip(false);
			merva.setSkip(false);
			f011.setSkip(false);
		}
		else if (FXMSGSEQNO.endsWith("2"))
		{ // f003 成功
			f007a.setSkip(true);
			f007y.setSkip(true);
			f001.setSkip(true);
			f003.setSkip(true);
			merva.setSkip(false);
			f011.setSkip(false);
		}
		else if (FXMSGSEQNO.endsWith("3"))
		{ // MERVA 成功
			f007a.setSkip(true);
			f007y.setSkip(true);
			f001.setSkip(true);
			f003.setSkip(true);
			merva.setSkip(true);
			f011.setSkip(false);
		}
		else if (FXMSGSEQNO.endsWith("4"))
		{ // F011 成功 (結束電文)
			f007a.setSkip(true);
			f007y.setSkip(true);
			f001.setSkip(true);
			f003.setSkip(true);
			merva.setSkip(true);
			f011.setSkip(true);
		}
		else if (FXMSGSEQNO.endsWith("5") || FXMSGSEQNO.endsWith("6"))
		{ // F007/F005 沖正成功
			f007a.setSkip(true);
			f007y.setSkip(true);
			f001.setSkip(true);
			f003.setSkip(true);
			merva.setSkip(true);
			f011.setSkip(false);
		}

		List<String> commands = new ArrayList();
		// commands.add("PINKEY(CMPASSWORD)");
		// CommandUtils.doBefore(commands, params);

		final TelCommExecWrapper execwrapper = new TelCommExecWrapper(new TelCommExec()
		{

			public TelcommResult query(Map params)
			{
				for (FxTelCommStep step : actions)
				{
					MVHImpl result = step.query(params);

					if (step.isNeedDoF011WinFlagN())
					{
						params.put("OKFLAG", "N");
						f002t.doF011(params);

						params.put("__FXMSGSEQNO", StrUtils.trim((String) params.get("__FXMSGSEQNO")) + "4");

						// 進人工重送須記錄 F001.STAN 欄位值
						params.put("__FXMSGCONTENT", StrUtils.trim((String) params.get("STAN")));
					}

					if (step.getLastException() != null)
					{
						if (step.getLastException() instanceof UncheckedException)
							throw (UncheckedException) step.getLastException();
						else throw new ToRuntimeException(step.getLastException().getMessage(), step.getLastException());
					}

				}

				return f003.getTelcommResult();
			}

		});

		MVHImpl result = execwrapper.query(params);

		MVHImpl f001Result = f001.getTelcommResult();

		if (f001Result != null)
		{
			params.put("BENNAME", f001Result.getValueByFieldName("M159AD1").length() == 0 ? " "
					: f001Result.getValueByFieldName("M159AD1"));
		}
		else params.put("BENNAME", "");

		final TXNFXRECORD record = (TXNFXRECORD) writeLog(execwrapper, params, result);

		final TelCommExecWrapper execwrapperFinal = execwrapper;
		trNotice.sendNotice(new NoticeInfoGetter()
		{

			/*** 付款人交易通知 ***/
			public NoticeInfo getNoticeInfo()
			{
				NoticeInfo result = new NoticeInfo(params.get("UID"), params.get("DPMYEMAIL"), params.get(""));
				result.setTemplateName("FXPAYER");

				// result.setDpretxstatus(record.getFXRETXSTATUS());
				result.getParams().put("__RECORD_ADTXNO", record.getADTXNO());
				result.setADTXNO(record.getADTXNO());
				result.setException(execwrapperFinal.getLastException());
				String datetime = record.getFXTXDATE() + record.getFXTXTIME();
				Date d = DateTimeUtils.parse("yyyyMMddHHmmss", datetime);
				String trandate = DateTimeUtils.format("yyyy/MM/dd", d);

				result.getParams().put("#TRANDATE", trandate);
				result.getParams().put("#SEQNO", params.get("STAN"));
				result.getParams().put("#BANKSEQNO", params.get("M1REFNO"));

				String acn = record.getFXSVAC();
				try
				{
					acn = StrUtils.left(acn, acn.length() - 5) + "***" + StrUtils.right(acn, 2);
				}
				catch (Exception e)
				{
				}

				String outacn = record.getFXWDAC();
				try
				{
					outacn = StrUtils.left(outacn, outacn.length() - 5) + "***" + StrUtils.right(outacn, 2);
				}
				catch (Exception e)
				{
				}

				result.getParams().put("#OUTACN", outacn + "&nbsp;&nbsp;" + params.get("NAME"));
				result.getParams().put("#INACN", acn + "&nbsp;&nbsp;" + params.get("BENNAME"));
				result.getParams().put("#PAYEEDATA", params.get("FXRCVADDR"));
				result.getParams().put("#PAYEEBANK", params.get("FXRCVBKCODE"));
				result.getParams().put("#PAYEEBANKDATA", params.get("FXRCVBKADDR"));
				String fxwdamt = NumericUtil.formatNumberString(record.getFXWDAMT(), 2);

				result.getParams().put("#OUTAMT", record.getFXWDCURR() + " " + fxwdamt + " 元");
				String fxsvamt = NumericUtil.formatNumberString(record.getFXSVAMT(), 2);

				result.getParams().put("#INAMT", record.getFXSVCURR() + " " + fxsvamt + " 元");
				result.getParams().put("#TRTATE", record.getFXEXRATE());

				int i_FeeIndex = record.getFXEFEE().indexOf("/");
				result.getParams().put("#FEE", (i_FeeIndex != -1 && i_FeeIndex != 0)
						? record.getFXEFEE().substring(0, i_FeeIndex)
						: "");
				result.getParams().put("#COST", (i_FeeIndex != -1 && i_FeeIndex != 0)
						? record.getFXEFEE().substring(i_FeeIndex + 1)
						: "");
				result.getParams().put("#MENO", params.get("CMMAILMEMO"));

				ADMMAILLOG admmaillog = new ADMMAILLOG();
				admmaillog.setADACNO(record.getFXWDAC());

				String trans_status = (String) params.get("__TRANS_STATUS");
				if (StrUtils.isEmpty(trans_status))
				{
					trans_status = "FXONLINE";
				}
				else if ("SEND".equals(trans_status))
				{
					trans_status = "FXSCH";
				}
				else if ("RESEND".equals(trans_status))
				{
					trans_status = "FXSCHRE";
				}

				admmaillog.setADBATCHNO(trans_status);
				admmaillog.setADMAILACNO(StrUtils.trim(params.get("CMTRMAIL")));
				admmaillog.setADUSERID(record.getFXUSERID());
				admmaillog.setADUSERNAME("");
				admmaillog.setADSENDTYPE("3");
				admmaillog.setADSENDTIME(datetime);
				admmaillog.setADSENDSTATUS("");

				result.setADMMAILLOG(admmaillog);

				return result;
			}
		});

		if (params.get("CMTRMAIL").length() != 0 && params.get("CMTRMAIL") != null)
		{
			trNotice.sendNotice(new NoticeInfoGetter()
			{

				/*** 收款人交易通知 ***/
				public NoticeInfo getNoticeInfo()
				{
					NoticeInfo result = new NoticeInfo("", "", params.get("CMTRMAIL"));
					result.setTemplateName("FXPAYEE");

					// result.setDpretxstatus(record.getFXRETXSTATUS());
					result.getParams().put("__RECORD_ADTXNO", record.getADTXNO());
					result.setADTXNO(record.getADTXNO());
					result.setException(execwrapperFinal.getLastException());
					String datetime = record.getFXTXDATE() + record.getFXTXTIME();
					Date d = DateTimeUtils.parse("yyyyMMddHHmmss", datetime);
					String trandate = DateTimeUtils.format("yyyy/MM/dd", d);

					result.getParams().put("#TRANDATE", trandate);
					result.getParams().put("#SEQNO", params.get("STAN"));
					result.getParams().put("#BANKSEQNO", params.get("M1REFNO"));

					String acn = record.getFXSVAC();
					try
					{
						acn = StrUtils.left(acn, acn.length() - 5) + "***" + StrUtils.right(acn, 2);
					}
					catch (Exception e)
					{
					}

					String outacn = record.getFXWDAC();
					try
					{
						outacn = StrUtils.left(outacn, outacn.length() - 5) + "***" + StrUtils.right(outacn, 2);
					}
					catch (Exception e)
					{
					}

					result.getParams().put("#OUTACN", outacn + "&nbsp;&nbsp;" + params.get("NAME"));
					result.getParams().put("#INACN", acn + "&nbsp;&nbsp;" + params.get("BENNAME"));
					result.getParams().put("#PAYEEDATA", params.get("FXRCVADDR"));
					result.getParams().put("#PAYEEBANK", params.get("FXRCVBKCODE"));
					result.getParams().put("#PAYEEBANKDATA", params.get("FXRCVBKADDR"));
					String fxsvamt = NumericUtil.formatNumberString(record.getFXSVAMT(), 2);
					result.getParams().put("#AMT", record.getFXSVCURR() + " " + fxsvamt + " 元");
					result.getParams().put("#MENO", params.get("CMMAILMEMO"));

					ADMMAILLOG admmaillog = new ADMMAILLOG();
					admmaillog.setADACNO(record.getFXWDAC());

					String trans_status = (String) params.get("__TRANS_STATUS");
					if (StrUtils.isEmpty(trans_status))
					{
						trans_status = "FXONLINE";
					}
					else if ("SEND".equals(trans_status))
					{
						trans_status = "FXSCH";
					}
					else if ("RESEND".equals(trans_status))
					{
						trans_status = "FXSCHRE";
					}

					admmaillog.setADBATCHNO(trans_status);
					admmaillog.setADMAILACNO(StrUtils.trim(params.get("CMTRMAIL")));
					admmaillog.setADUSERID(record.getFXUSERID());
					admmaillog.setADUSERNAME("");
					admmaillog.setADSENDTYPE("3");
					admmaillog.setADSENDTIME(datetime);
					admmaillog.setADSENDSTATUS("");

					result.setADMMAILLOG(admmaillog);

					return result;
				}

			});
		}

		execwrapper.throwException();

		// 合併 F001 及 F003 的結果
		if (result != null)
		{

			result.getFlatValues().putAll(f001.getTelcommResult().getFlatValues());
			result.getFlatValues().put("ADTXNO", record.getADTXNO());

			// 電文結果更新request參數
			result.getFlatValues().putAll(params);

			log.trace("F001Controller.after.Merge.result: {}", result.getFlatValues());

			/**
			 * 交易單據
			 * TXNFXRECORD.FXCERT欄位改放交易單據所需資料的JSON
			 */
			try
			{
				MVHImpl mvhimpl = null;
				// N920
				MVH mvh = new MVHImpl();
				mvh = commonPools.n920.getAcnoList(result.getFlatValues().get("UID"), new String[]{"01", "02", "03", "04", "05", "07"}, "Y");
				mvhimpl = (MVHImpl) mvh;
				
				
				// 列印時間
				result.getFlatValues().put("curentDateTime", DateTimeUtils.getDatetime(new Date()));
				// 姓名
				result.getFlatValues().put("NAME", mvhimpl.getFlatValues().get("NAME") );
				// 付款日期
				result.getFlatValues().put("currentDate", DateTimeUtils.getDateShort(new Date()));

				Map<String, String> callDataF002T = result.getFlatValues();
				// 英文姓名
				StringBuffer sb = new StringBuffer();
				StringBuffer enName = new StringBuffer();
				sb.append(callDataF002T.get("M150AD1"));
				if (!String.valueOf(callDataF002T.get("M150AD2")).trim().equals(""))
				{
					sb.append("<br>");
					sb.append(callDataF002T.get("M150AD2"));
					enName.append(callDataF002T.get("M150AD2"));
				}

				if (!String.valueOf(callDataF002T.get("M150AD3")).trim().equals(""))
				{
					sb.append("<br>");
					sb.append(callDataF002T.get("M150AD3"));
					enName.append(callDataF002T.get("M150AD3"));
				}

				if (!String.valueOf(callDataF002T.get("M150AD4")).trim().equals(""))
				{
					sb.append("<br>");
					sb.append(callDataF002T.get("M150AD4"));
					enName.append(callDataF002T.get("M150AD4"));
				}
				// 英文姓名<br>
				callDataF002T.put("ENNAMEbr", sb.toString());
				// 英文姓名
				callDataF002T.put("ENNAME", enName.toString());
				// 匯率
				String str_Rate = String.valueOf(callDataF002T.get("EXRATE"));
				if (str_Rate == null)
				{
					str_Rate = "";
				}
				else if (str_Rate != null && str_Rate.replace("0", "").equals(""))
				{
					str_Rate = "";
				}
				else
				{
					str_Rate = NumericUtil.formatNumberString(String.valueOf(callDataF002T.get("EXRATE")), 8);
				}
				// 付款金額
				String str_orgAmt = String.valueOf(callDataF002T.get("ORGAMT"));
				// 付款幣別
				String str_orgCcy = String.valueOf(callDataF002T.get("ORGCCY"));
				if (str_orgCcy.equals("TWD") || str_orgCcy.equals("JPY"))
				{
					str_orgAmt = str_orgAmt.substring(0, str_orgAmt.length() - 2);
				}
				str_orgAmt = NumericUtil.formatNumberStringByCcy(str_orgAmt, 2, str_orgCcy);
				// 收款金額
				String str_pmtAmt = String.valueOf(callDataF002T.get("PMTAMT"));
				// 收款幣別
				String str_pmtCcy = String.valueOf(callDataF002T.get("PMTCCY"));
				if (str_pmtCcy.equals("TWD") || str_pmtCcy.equals("JPY"))
				{
					str_pmtAmt = str_pmtAmt.substring(0, str_pmtAmt.length() - 2);
				}
				str_pmtAmt = NumericUtil.formatNumberStringByCcy(str_pmtAmt, 2, str_pmtCcy);
				// 手續費
				String str_commAmt = String.valueOf(callDataF002T.get("COMMAMT"));
				// 手續費幣別
				String str_commCcy = callDataF002T.get("COMMCCY");
				if (str_commCcy.equals("TWD") || str_commCcy.equals("JPY"))
				{
					str_commAmt = str_commAmt.substring(0, str_commAmt.length() - 2);
				}
				str_commAmt = NumericUtil.formatNumberStringByCcy(str_commAmt, 2, str_commCcy);
				// 郵電費
				String str_cabAmt = String.valueOf(callDataF002T.get("CABCHG"));
				if (str_commCcy.equals("TWD") || str_commCcy.equals("JPY"))
				{
					str_cabAmt = str_cabAmt.substring(0, str_cabAmt.length() - 2);
				}
				str_cabAmt = NumericUtil.formatNumberStringByCcy(str_cabAmt, 2, str_commCcy);
				// 國外費用
				String str_ourAmt = String.valueOf(callDataF002T.get("OURCHG"));
				if (str_commCcy.equals("TWD") || str_commCcy.equals("JPY"))
				{
					str_ourAmt = str_ourAmt.substring(0, str_ourAmt.length() - 2);
				}
				str_ourAmt = NumericUtil.formatNumberStringByCcy(str_ourAmt, 2, str_commCcy);
				result.getFlatValues().put("str_Rate", str_Rate);
				result.getFlatValues().put("str_orgAmt", str_orgAmt);
				result.getFlatValues().put("str_pmtAmt", str_pmtAmt);
				result.getFlatValues().put("str_commAmt", str_commAmt);
				result.getFlatValues().put("str_cabAmt", str_cabAmt);
				result.getFlatValues().put("str_ourAmt", str_ourAmt);
				result.getFlatValues().put("jspTitle", "外匯匯出匯款");
				// 放入TXNFXRECORD.FXCERT
				String str_Result = CodeUtil.toJson(result.getFlatValues());
				log.debug("F002Controller 交易單據  str_Result >>>>>>>>>>>>>>>>>>>>>>>>>>>  {}", str_Result);

				commonPools.fxUtils.updateFxCert(record.getADTXNO(), str_Result);
			}
			catch (Exception e)
			{
				throw new ToRuntimeException("產生交易單據發生錯誤.", e);
			}

		}
		return result;

	}

	public Object writeLog(final TelCommExecWrapper execwrapper, final Map _params, final MVHImpl result)
	{
		Object obj = f002t.writeLog(execwrapper, _params, result);
		return obj;
	}

}
