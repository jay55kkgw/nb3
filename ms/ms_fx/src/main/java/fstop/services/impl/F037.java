package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class F037 extends CommonService {

	@Autowired
	@Qualifier("f037Telcomm")
	private TelCommExec f037Telcomm;

	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) {
		log.debug("F037");
		Map cloneParams = params;
		
		// 即時交易選擇憑證，這裡要塞CERTACN成19個1
		if ("1".equals((String) cloneParams.get("FGTXWAY"))) {
			cloneParams.put("CERTACN", StrUtils.repeat("1", 19));
		// 即時交易選擇IDGATE，這裡要塞CERTACN成19個C
		}else if ("7".equals((String) cloneParams.get("FGTXWAY"))) {
			cloneParams.put("CERTACN", StrUtils.repeat("C", 19));
		}
			

		TelcommResult result = f037Telcomm.query(cloneParams);
//		updateDB((String)cloneParams.get("TWSCHID"),(String)cloneParams.get("TWCOUNT"),(String)cloneParams.get("FXSCHID"),(String)cloneParams.get("FXCOUNT"));
		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));

		return result;
	}



}
