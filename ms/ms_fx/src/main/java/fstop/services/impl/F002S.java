package fstop.services.impl;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.rest.comm.bean.DoIDGateCommand_In;
import com.netbank.rest.comm.controller.CommonIDGATEController;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import encryptUtil.EncryptUtil;
import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.TxnFxSchPayDao;
import fstop.orm.dao.TxnFxSchPayDataDao;
import fstop.orm.dao.TxnTwSchPayDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.TXNFXSCHPAY;
import fstop.orm.po.TXNFXSCHPAYDATA;
import fstop.services.BookingAware;
import fstop.services.CommonService;
import fstop.services.ServiceBindingException;
import fstop.services.TransferNotice;
import fstop.telcomm.TelCommExec;
import fstop.telcomm.cmd.CommandUtils;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 外匯匯出匯款-預約
 * 
 * @author Owner
 *
 */
@Slf4j
public class F002S extends CommonService implements BookingAware
{

	@Autowired
	@Qualifier("n951CMTelcomm")
	private TelCommExec n951CMTelcomm;

	@Autowired
	private TxnFxSchPayDao txnFxSchPayDao;
	
	@Autowired
	private TxnFxSchPayDataDao txnFxSchPayDataDao;

	@Autowired
	private TxnUserDao txnUserDao;

	@Autowired
	CommonIDGATEController commonIDGATEController;
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params)
	{

		/*
		 * 在批次跑預約時, F007 的電文裡, FLAG 為 A 如果電文失敗或寫DB發生任何錯誤, 則發 F007的電文 但是 FLAG 內容為 C 的電文
		 */

		// TODO 在營業時間 09:30~15:30 外, 只能執行預約交易
		Map<String, String> params = _params;

		String f = params.get("FGTRDATE");
		if ("1".equals(f) || "2".equals(f))
		{ // 預約

			if ("0".equals(params.get("FGTXWAY")))
			{
				// 20190518 ian 改為n951

				// 若驗證失敗, 則會發動 TopMessageException
				MVHImpl n951Result = n951CMTelcomm.query(params);
			}
			return booking(params);
		}
		else
		{
			throw new ServiceBindingException("不為預約交易");
		}
		// 取得議價
	}

	/**
	 * 新預約
	 */
	@Override
	public MVHImpl booking(Map<String, String> params)
	{

		DecimalFormat dft = new DecimalFormat("#0");
		DecimalFormat dftdot2 = new DecimalFormat("#0.00");
		MVHImpl mvhImpl = new MVHImpl();

		try
		{

			log.info("***************************************F002預約***************************************************");
			TXNFXSCHPAY schedule = new TXNFXSCHPAY();
			// 取得預約編號(YYMMDD+三位流水號)
			String dptmpschno = txnUserDao.getDptmpschno(params.get("UID"));
			String sha1_Mac = "";
			String reqinfo_Str = "{}";
			// 判斷 預約 周期
			String f = params.get("FGTRDATE");
			// 預約某一天
			if ("1".equals(f))
			{
				params.put("CMTRDATE", StrUtils.trim(params.get("CMTRDATE")).replaceAll("/", ""));
				if (params.get("CMTRDATE").length() == 0)
				{
					throw TopMessageException.create("ZX99");
				}
				else
				{
					schedule.setFXTXTYPE("S");
					schedule.setFXPERMTDATE("");
					schedule.setFXFDATE(params.get("CMTRDATE"));
					schedule.setFXTDATE(params.get("CMTRDATE"));
				}
			}
			// 周期, 每月某一日
			else if ("2".equals(f))
			{
				params.put("CMSDATE", StrUtils.trim(params.get("CMSDATE")).replaceAll("/", ""));
				params.put("CMEDATE", StrUtils.trim(params.get("CMEDATE")).replaceAll("/", ""));
				if (params.get("CMPERIOD").length() == 0 || params.get("CMSDATE").length() == 0
						|| params.get("CMEDATE").length() == 0)
				{
					throw TopMessageException.create("ZX99");
				}
				else
				{
					schedule.setFXTXTYPE("C");
					schedule.setFXPERMTDATE(params.get("CMPERIOD"));
					schedule.setFXFDATE(params.get("CMSDATE"));
					schedule.setFXTDATE(params.get("CMEDATE"));
				}
			}

			/*** New Add ***/
			// 轉出/轉入
			if ("1".equals(params.get("PAYREMIT")))
			{ // 轉出
				schedule.setFXWDAMT(dftdot2.format(dft.parse(params.get("CURAMT")).doubleValue())); // 轉出金額
				schedule.setFXSVAMT(""); // 轉入金額
			}
			else
			{ // "2".equals(params.get("PAYREMIT")) //轉入
				schedule.setFXWDAMT(""); // 轉出金額
				schedule.setFXSVAMT(dftdot2.format(dft.parse(params.get("CURAMT")).doubleValue())); // 轉入金額
			}

			if (params.containsKey("pkcs7Sign") && !"0".equals(params.get("FGTXWAY")) && !"7".equals(params.get("FGTXWAY")))
			{
				List<String> commands = new ArrayList();
				commands.add("XMLCA()");
				commands.add("XMLCN()");

				CommandUtils.doBefore(commands, params);
				if (!StrUtils.trim(params.get("XMLCN")).toUpperCase().startsWith(StrUtils.trim(params.get("UID")).toUpperCase()))
				{
					throw TopMessageException.create("Z089");
				}
				schedule.setXMLCA(StrUtils.trim(params.get("XMLCA")));
				schedule.setXMLCN(StrUtils.trim(params.get("XMLCN")));
			}

			// IDGATE驗證-----start
			if ("7".equals(params.get("FGTXWAY"))) {
				DoIDGateCommand_In in = new DoIDGateCommand_In();
				in.setIN_DATAS(params);
				HashMap rtnMap = commonIDGATEController.doIDGateCommand(in);
				if (!(boolean) rtnMap.get("result")) {
					log.error("N174 BOOKING CHECK IDGateCommand() ERROR");
					switch ((String) rtnMap.get("errMsg")) {
					case "Z300":
						throw TopMessageException.create("Z300");
					case "FE0011":
						throw TopMessageException.create("FE0011");
					default:
						throw TopMessageException.create("ZX99");
					}
				}

			}
			// IDGATE驗證-----end
			
			// 因IKEY資料太長 先移除避免塞爆欄位
			params.remove("pkcs7Sign");
			params.remove("__SIGN");
			// 預約時上行電文必要資訊
			log.trace(ESAPIUtil.vaildLog("txnfxschpay.params>>>{}"+CodeUtil.toJson(params)));
			reqinfo_Str = CodeUtil.toJson(params);
			sha1_Mac = EncryptUtil.toEncS1(reqinfo_Str, "UTF-8").substring(0, 4);
			//
			schedule.setFXSCHNO(dptmpschno);//預約編號
			schedule.setMAC(sha1_Mac);
			schedule.setFXTXINFO(reqinfo_Str);
			// 處理{匯款附言}欄位
			String str_Memo1 = params.get("MEMO1").toString();
			str_Memo1 = str_Memo1.replaceAll("\r", "");
			str_Memo1 = str_Memo1.replaceAll("\n", "");
			params.put("MEMO1", str_Memo1);
			params.put("ADREQTYPE", "S"); // 寫入 TXNLOG 時, S 表示為預約
			schedule.setFXTXMEMO(params.get("MEMO1")); // 匯款附言(名字就是 MEMO1 , 因為電文要使用)
			schedule.setFXTXMAILS(params.get("CMTRMAIL"));
			schedule.setFXTXMAILMEMO(params.get("CMMAILMEMO")); // CMMAILMEMO 對應的欄位
			// 設定時間日期
			Date d = new Date();
			schedule.setFXSDATE(DateTimeUtils.format("yyyyMMdd", d));
			schedule.setFXSTIME(DateTimeUtils.format("HHmmss", d));
			schedule.setLASTDATE(schedule.getFXSDATE());
			schedule.setLASTTIME(schedule.getFXSTIME());
			// 設定時間日期 -END
			schedule.setADOPID("F002");// 操作功能ID(NB3SysOp.ADOPID)
			schedule.setFXTXCODE(params.get("FGTXWAY"));// 交易機制
			schedule.setFXUSERID(params.get("UID"));//使用者ID
			
			schedule.setFXTXSTATUS("0");// 0: 預約
			schedule.setFXWDAC(params.get("CUSTACC")); // 轉出帳號
			schedule.setFXSVBH(""); // 轉入分行
			schedule.setFXSVAC(params.get("BENACC")); // 轉入帳號
			schedule.setFXWDCURR(params.get("PAYCCY")); // 轉出幣別
			schedule.setFXSVCURR(params.get("REMITCY")); // 轉入幣別
			
			schedule.setLOGINTYPE(params.get("LOGINTYPE"));// NB登入或MB登入
			schedule.setMSADDR(TxnTwSchPayDao.MS_FX);//所屬微服務
			
			log.debug(ESAPIUtil.vaildLog("schedule >>>>> " + schedule.toString()));
			//寫入預約 Table
			txnFxSchPayDao.save(schedule);
			
			
			String reqinfo_addTXTime ="";
			String sha1_MacTime ="" ;
			
//			List<TXNFXSCHPAYDATA> dataList = txnFxSchPayDataDao.findByFXSCHNO(dptmpschno);
//	        for(TXNFXSCHPAYDATA data:dataList) {
//	        	params.put("FXSCHTXDATE", data.getPks().getFXSCHTXDATE());
//	        	reqinfo_addTXTime =  CodeUtil.toJson(params);
//	        	sha1_MacTime = EncryptUtil.toEncS1(reqinfo_addTXTime, null).substring(0,4);
//	        	data.setMAC(sha1_MacTime);
//	        	txnFxSchPayDataDao.update(data);
//	        }

			mvhImpl.getFlatValues().put("MSGCOD", "");
			mvhImpl.getFlatValues().put("TRDATE", params.get("TRDATE"));
			mvhImpl.getFlatValues().put("TSFACN", params.get("TSFACN"));
			mvhImpl.getFlatValues().put("CMTRMEMO", params.get("CMTRMEMO"));

		}
		catch (Exception e)
		{
			log.error("booking >>>> {}", e);

		}
		return mvhImpl;
	}

	// @Override
	// public MVHImpl booking(Map<String, String> params) {
	// DateTimeUtils.removeSlash((Hashtable)params, new String[]{"CMSDATE",
	// "CMEDATE", "CMTRDATE"});
	//
	// DecimalFormat dft = new DecimalFormat("#0");
	// DecimalFormat dftdot2 = new DecimalFormat("#0.00");
	//
	// TXNFXSCHEDULE schedule = new TXNFXSCHEDULE();
	//
	// log.info("***************************************預約編號***************************************************");
	// //更新SCHCOUNT(每日預約計數)
	// int scnt = txnUserDao.incSchcount((String)params.get("UID"));
	//
	// //預約編號(YYMMDD+三位流水號)
	// String dptmpschno = "";
	// String ymd = DateTimeUtils.format("yyMMdd", new Date());
	// dptmpschno = ymd + StrUtils.right("000" + scnt, 3);
	//
	// //賽入預約編號
	// schedule.setFXSCHNO(dptmpschno);
	// log.info("***************************************預約編號***************************************************");
	//
	// schedule.setADOPID(DateTimeUtils.format("yyyyMMddHHmmss", new Date()) +
	// params.get("UID"));
	// String f = params.get("FGTRDATE"); //判斷 預約 周期
	// if("1".equals(f)) {
	// params.put("CMTRDATE", StrUtils.trim(params.get("CMTRDATE")).replaceAll("/",
	// ""));
	//
	// if(params.get("CMTRDATE").length()==0) {
	// throw TopMessageException.create("ZX99");
	// }
	// else {
	// schedule.setFXPERMTDATE("");
	// schedule.setFXFDATE(params.get("CMTRDATE"));
	// schedule.setFXTDATE(params.get("CMTRDATE"));
	// }
	// }
	// else {
	// params.put("CMSDATE", StrUtils.trim(params.get("CMSDATE")).replaceAll("/",
	// ""));
	// params.put("CMEDATE", StrUtils.trim(params.get("CMEDATE")).replaceAll("/",
	// ""));
	// if(params.get("CMPERIOD").length()==0 || params.get("CMSDATE").length()==0 ||
	// params.get("CMEDATE").length()==0) {
	// throw TopMessageException.create("ZX99");
	// }
	// else {
	// schedule.setFXPERMTDATE(params.get("CMPERIOD"));
	// schedule.setFXFDATE(params.get("CMSDATE"));
	// schedule.setFXTDATE(params.get("CMEDATE"));
	// }
	// }
	//
	// params.put("ADREQTYPE", "S"); //寫入 TXNLOG 時, S 表示為預約
	//
	// schedule.setFXUSERID(params.get("UID"));
	// schedule.setFXWDAC(params.get("CUSTACC")); //轉出帳號
	// schedule.setFXSVBH(""); //轉入分行
	// schedule.setFXSVAC(params.get("BENACC")); //轉入帳號
	// schedule.setFXWDCURR(params.get("PAYCCY")); //轉出幣別
	// schedule.setFXSVCURR(params.get("REMITCY")); //轉入幣別
	//
	//
	// /*** New Add ***/
	// try {
	// if ("1".equals(params.get("PAYREMIT"))) { // 轉出
	// //double dd = new Double(params.get("CURAMT")) * new
	// Double(params.get("RATE"));
	//
	// schedule.setFXWDAMT(dftdot2.format(dft.parse(params.get("CURAMT")).doubleValue()));
	// // 轉出金額
	// schedule.setFXSVAMT( ""); // 轉入金額
	//
	// } else { // "2".equals(params.get("PAYREMIT")) //轉入
	// //double dd = new Double(params.get("ATRAMT")) / new
	// Double(params.get("RATE"));
	//
	// schedule.setFXWDAMT( ""); // 轉出金額
	// schedule.setFXSVAMT(dftdot2.format(dft.parse(params.get("CURAMT")).doubleValue()));
	// // 轉入金額
	//
	// }
	// }
	// catch(Exception e) {}
	//
	// //處理{匯款附言}欄位
	// String str_Memo1 = params.get("MEMO1").toString();
	// str_Memo1=str_Memo1.replaceAll("\r", "");
	// str_Memo1=str_Memo1.replaceAll("\n", "");
	// params.put("MEMO1", str_Memo1);
	// schedule.setFXTXMEMO(params.get("MEMO1")); //匯款附言(名字就是 MEMO1 , 因為電文要使用)
	// schedule.setFXTXMAILS(params.get("CMTRMAIL"));
	// schedule.setFXTXMAILMEMO(params.get("CMMAILMEMO")); //CMMAILMEMO 對應的欄位
	//
	// schedule.setFXTXCODE(params.get("FGTXWAY"));
	//
	// Date d = new Date();
	// schedule.setFXSDATE(DateTimeUtils.format("yyyyMMdd", d));
	// schedule.setFXSTIME(DateTimeUtils.format("HHmmss", d));
	//
	// schedule.setFXTXSTATUS("0");
	// schedule.setLASTDATE(schedule.getFXSDATE());
	// schedule.setLASTTIME(schedule.getFXSTIME());
	//
	//
	// if(params.containsKey("pkcs7Sign") && !"0".equals(params.get("FGTXWAY"))) {
	// List<String> commands = new ArrayList();
	// commands.add("XMLCA()");
	// commands.add("XMLCN()");
	//
	// CommandUtils.doBefore(commands, params);
	//
	// if(!StrUtils.trim(params.get("XMLCN")).toUpperCase().startsWith(StrUtils.trim(params.get("UID")).toUpperCase()))
	// {
	// throw TopMessageException.create("Z089");
	// }
	//
	// schedule.setXMLCA(StrUtils.trim(params.get("XMLCA")));
	// schedule.setXMLCN(StrUtils.trim(params.get("XMLCN")));
	// }
	//
	//
	// TXNREQINFO titainfo = new TXNREQINFO();
	// titainfo.setREQINFO(BookingUtils.requestInfo(params));
	// txnReqInfoDao.save(titainfo);
	//
	// schedule.setFXTXINFO(titainfo.getREQID());
	//
	// String sha1 = DigestUtils.sha1(titainfo.getREQINFO()).substring(0, 4);
	// schedule.setMAC(sha1);
	//
	// schedule.setADOPID(params.get("ADOPID"));
	//
	// txnFxScheduleDao.save(schedule);
	//
	// return new MVHImpl();
	// }

//	private String getTITAInfo(Map<String, String> params)
//	{
//		Map<String, String> mr = new HashMap();
//		// F001S 特別要記的欄位
//		String[] fields = new String[] { "FGTRDATE" // 判斷即時還是預約
//		};
//
//		for (String f : fields)
//		{
//			mr.put(f, params.get(f));
//		}
//		return JSONUtils.map2json(mr);
//	}

	@Override
	public MVHImpl online(Map<String, String> params, TransferNotice trNotice)
	{
		return new MVHImpl();
	}
}
