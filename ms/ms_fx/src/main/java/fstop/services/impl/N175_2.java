package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.AdmMsgCodeDao;
import fstop.orm.dao.TxnFxRecordDao;
import fstop.orm.dao.TxnFxSchPayDataDao;
import fstop.orm.po.ADMMAILLOG;
import fstop.orm.po.TXNFXRECORD;
import fstop.orm.po.TXNREQINFO;
import fstop.services.BookingUtils;
import fstop.services.CommonService;
import fstop.services.NoticeInfo;
import fstop.services.NoticeInfoGetter;
import fstop.services.OnLineTransferNotice;
import fstop.services.TelCommExecWrapper;
import fstop.services.WriteLogInterface;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import topibs.utility.NumericUtil;

@Slf4j
public class N175_2 extends CommonService implements WriteLogInterface {

	@Autowired
	@Qualifier("n175_2Telcomm")
	private TelCommExec n175_2Telcomm;

	@Autowired
	private TxnFxRecordDao txnFxRecordDao;

	@Autowired
	private AdmMsgCodeDao admMsgCodeDao;

	@Autowired
	private TxnFxSchPayDataDao txnFxSchPayDataDao; // 預約資料

	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	@SuppressWarnings("unchecked")
	public MVH doAction(Map _params) {
		final Map<String, String> params = _params;
		Date d = new Date();
		params.put("DATE", DateTimeUtils.getCDateShort(d));
		params.put("TIME", DateTimeUtils.format("HHmmss", d));

		String stadate = (String) params.get("DPISDT"); // 民國年月日
		String enddate = (String) params.get("DUEDAT"); // 民國年月日
		stadate = StrUtils.trim(stadate).replaceAll("/", ""); // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
		enddate = StrUtils.trim(enddate).replaceAll("/", ""); // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd

		params.put("DPISDT", stadate);
		params.put("DUEDAT", enddate);
		params.put("TXNTYPE", "02");
		//IDGATE----20210526新增
		if("7".equals(params.get("FGTXWAY"))){
			String certACN = "";
			while (certACN.length() < 19)
				certACN += "C";
				params.put("CERTACN", certACN);
		}
		//IDGATE----
		TelCommExecWrapper execwrapper = new TelCommExecWrapper(n175_2Telcomm);

		MVHImpl result = execwrapper.query(params); // N175 電文

		// old code
//		final TXNFXRECORD record = (TXNFXRECORD) getThis(this).writeLog(execwrapper, params, result);
		// 因為CommonService的getThis方法被註解，所以直接用writeLog方法
		final TXNFXRECORD record = (TXNFXRECORD) writeLog(execwrapper, params, result);

		final TelCommExecWrapper execwrapperFinal = execwrapper;
		OnLineTransferNotice trNotice = new OnLineTransferNotice();

//	TODO 解決寄Email功能，無法讀取模版問題
		trNotice.sendNotice(new NoticeInfoGetter() {

			public NoticeInfo getNoticeInfo() {
				NoticeInfo result = new NoticeInfo(params.get("UID"), params.get("DPMYEMAIL"), params.get("CMTRMAIL"));
				result.setTemplateName("FXNOTIFY");

				result.setADTXNO(record.getADTXNO());

				result.setException(execwrapperFinal.getLastException());
				String datetime = record.getFXTXDATE() + record.getFXTXTIME();
				Date d = DateTimeUtils.parse("yyyyMMddHHmmss", datetime);
				String trantime = DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", d);
				result.getParams().put("#TXNAME", "外匯綜存定存解約");
				result.getParams().put("#TRANTIME", trantime);
				result.getParams().put("#SEQNO", "");

				String acn = record.getFXSVAC();
				try {
					acn = StrUtils.left(acn, acn.length() - 5) + "***" + StrUtils.right(acn, 2);
				} catch (Exception e) {
				}

				String outacn = record.getFXWDAC();
				try {
					outacn = StrUtils.left(outacn, outacn.length() - 5) + "***" + StrUtils.right(outacn, 2);
				} catch (Exception e) {
				}

				result.getParams().put("#ACN", acn);
				result.getParams().put("#OUTACN", outacn);
				result.getParams().put("#OUTCURRENCY", record.getFXWDCURR());
				String dptxamtfmt = NumericUtil.formatNumberString(record.getFXWDAMT(), 2);
				result.getParams().put("#AMT", dptxamtfmt);
				result.getParams().put("#MEMO", params.get("CMMAILMEMO"));
				result.getParams().put("#NHITAX", NumericUtil.formatNumberString(params.get("NHITAX"), 2));
				ADMMAILLOG admmaillog = new ADMMAILLOG();
				admmaillog.setADACNO(record.getFXWDAC());

				String trans_status = (String) params.get("__TRANS_STATUS");
				if (StrUtils.isEmpty(trans_status)) {
					trans_status = "FXONLINE";
				} else if ("SEND".equals(trans_status)) {
					trans_status = "FXSCH";
				} else if ("RESEND".equals(trans_status)) {
					trans_status = "FXSCHRE";
				}

				admmaillog.setADBATCHNO(trans_status);
				admmaillog.setADMAILACNO(StrUtils.trim(params.get("CMTRMAIL")));
				admmaillog.setADUSERID(record.getFXUSERID());
				admmaillog.setADUSERNAME("");
				admmaillog.setADSENDTYPE("3");
				admmaillog.setADSENDTIME(datetime);
				admmaillog.setADSENDSTATUS("");

				result.setADMMAILLOG(admmaillog);

				return result;
			}

		});

		execwrapper.throwException();

		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));

		return result;
	}

	public Object writeLog(TelCommExecWrapper execwrapper, Map<String, String> params, MVHImpl result) {

		log.debug("writeLog start");
		
		// 建立交易紀錄log 並檢查是否需更新預約檔
		TXNFXRECORD record = execwrapper.createTxnFxRecordCheckupdatePayData(params, txnFxSchPayDataDao);
		try {
			log.debug(ESAPIUtil.vaildLog("params >> {}" + CodeUtil.toJson(params)));
			//log.debug(ESAPIUtil.vaildLog("result >> {}" + CodeUtil.toJson(result)));會出事
			log.debug(ESAPIUtil.vaildLog("record >> {}" + CodeUtil.toJson(record)));

			record.setADOPID("N175");
			record.setFXUSERID(params.get("UID"));
			record.setFXWDAC(params.get("ACN")); // 轉出帳號
			record.setFXSVBH(""); // 轉入分行
			record.setFXSVAC(params.get("FDPNUM")); // 轉入帳號
			record.setFXWDCURR(params.get("CUID")); // 轉出幣別
			record.setFXSVCURR(params.get("CUID")); // 轉入幣別

			record.setFXWDAMT(params.get("AMT")); // 轉出金額

			record.setFXSVAMT(""); // 轉入金額

			record.setFXTXMEMO(params.get("CMTRMEMO"));
			record.setFXTXMAILS(params.get("CMTRMAIL"));
			record.setFXTXMAILMEMO(params.get("CMMAILMEMO")); // CMMAILMEMO 對應的欄位
			// 微服務移除欄位-預約編號

			String str_MsgCode = record.getFXEXCODE();
			log.debug(ESAPIUtil.vaildLog("str_MsgCode >> {}" + str_MsgCode));
			log.debug("admMsgCodeDao >> {}", admMsgCodeDao);

			String fxFlag = admMsgCodeDao.findFXRESEND(str_MsgCode);
			log.debug(ESAPIUtil.vaildLog("fxFlag >> {}" + fxFlag));

			// 欄位長度過長資料庫塞不進去，故remove
			params.remove("pkcs7Sign");
			params.remove("jsondc");
			params.remove("__SIGN");
			record.setFXTITAINFO(CodeUtil.toJson(params));
			// 檢查是否需更新預約檔
			log.debug(ESAPIUtil.vaildLog("N175 record >>>>>>>>>>>>> " + record.toString()));
			txnFxRecordDao.save(record);

		} catch (Exception e) {
			log.error("writeLog error", e);
		}
		return record;
	}

}
