package fstop.services.impl.fx;

import java.util.Hashtable;
import java.util.Map;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import fstop.exception.ToRuntimeException;
import fstop.exception.TopMessageException;
import fstop.model.TelcommResult;
import fstop.services.impl.F001T;
import fstop.services.impl.F003T;
import fstop.services.impl.N870;
import fstop.telcomm.TelCommExec;
import fstop.util.JSONUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class F003_F007YTelcomm implements FxTelCommStep {
//	Logger logger = Logger.getLogger(getClass());

	@Autowired
	private F003T f003t;
	
	public F003_F007YTelcomm(F003T f003t) {
		this.f003t = f003t;
	}
	
	public Exception lastException;
	
	boolean isSkip = false;

	@Override
	public boolean isSkip() {
		return isSkip;
	}

	public void setSkip(boolean isSkip) {
		this.isSkip = isSkip;
	}
	@Override
	public TelcommResult query(Map _params) {
		Map<String, String> params = _params;
		
		if(isSkip()) {
			TelcommResult telResult = new TelcommResult(new Vector());
			return telResult;
		}

		TelcommResult result = null;
		boolean isFail = false;
		String topmsg = "";
		boolean topmsgNotInDB = false;
		try {
			result = (TelcommResult)f003t.doF007Y(params); 
			
			params.put("BGROENO", result.getValueByFieldName("BGROENO"));  //議價編號
			params.put("RATE", result.getValueByFieldName("RATE"));  //匯率
			
			params.put("__FXMSGSEQNO", StrUtils.trim(params.get("__FXMSGSEQNO")) + "0");
		}
		catch(Exception e) {	
			
			/*** New Add ***/			
			if (e instanceof TopMessageException) {
				
				/**
				 *  檢查 F001 回應之訊息代碼: 
				 *  1.若存在於資料庫 && 註記可系統/人工重送("Y") ==> 進系統/人工重送處理,不需發 F007C
				 *  2.其它 ==> 一律發 F007C	 
				 */	
				try {					
					topmsg = ((TopMessageException)e).getMsgcode();							
					isFail = f003t.checkTopMsg(topmsg, params.get("__SCHID"));					
				}
				catch(Exception ex) { }				
				
			}
			else {				
				isFail = true;
			}

			lastException = e;
		}
		
		
//		//若沖回議價部位失敗,進入人工處理流程
//		if(isFail) {
//			try {
//				result = f003t.doF007C(params);
//				
//				params.put("__FXMSGSEQNO", StrUtils.trim(params.get("__FXMSGSEQNO")) + "5");					
//			}
//			catch(Exception e) {
//				
//				logger.error("F007Y 已發生 " + lastException.getMessage() + ", doF007C 時又發生 " + e.getMessage());				
//
//				lastException = e;
//			}
//		}
		
		return result;
	}

	@Override
	public Exception getLastException() {
		return lastException;
	}

	@Override
	public boolean isNeedDoF011WinFlagN() {
		return false;
	}
}
