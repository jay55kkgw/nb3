package fstop.services.impl.fx;

import java.util.Hashtable;
import java.util.Map;
import java.util.Vector;

import org.springframework.beans.factory.annotation.Autowired;

import fstop.exception.ToRuntimeException;
import fstop.exception.TopMessageException;
import fstop.model.TelcommResult;
import fstop.services.impl.F002T;
import fstop.services.impl.N870;
import fstop.util.JSONUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class F002_F003Telcomm implements FxTelCommStep {

	@Autowired
	private F002T f002t;

	@Autowired
	private F002_F001Telcomm f001TelcommCommand;

	private boolean isNeedDoF011WithFlagN = false;
	
	boolean isSkip = false;

	@Override
	public boolean isSkip() {
		return isSkip;
	}

	public void setSkip(boolean isSkip) {
		this.isSkip = isSkip;
	}
	public TelcommResult getTelcommResult() {
		return telcommResult;
	}

	public F002_F003Telcomm(F002T f002t, F002_F001Telcomm f001TelcommCommand) {
		this.f002t = f002t;
		this.f001TelcommCommand = f001TelcommCommand;
	}

	@Autowired
	public Exception lastException;

	@Autowired
	private TelcommResult telcommResult;

	@Override
	public TelcommResult query(Map _params) {
		Map<String, String> params = _params;
		
		if(isSkip()) {
			String oldResult = (String)params.get("__F003_RESULT");
			Map m = JSONUtils.json2map(oldResult);
			TelcommResult telResult = new TelcommResult(new Vector());
			telResult.getFlatValues().putAll(m);
			this.telcommResult = telResult;
			return telResult;
		}

		TelcommResult result = null;
		boolean isFail = false;
		String topmsg = "";
		try {
			result = f002t.doF003(f001TelcommCommand.getTelcommResult(), params);
//			topmsg = result.getValueByFieldName("__TOPMSG");
//			if(!f002t.checkSuccessTopMsg(topmsg))
//				throw TopMessageException.create(topmsg);
			telcommResult = result;
			
			params.put("__F003_RESULT", JSONUtils.map2json(result.getFlatValues()));
			params.put("__FXMSGSEQNO", StrUtils.trim(params.get("__FXMSGSEQNO")) + "2");
		}
		catch(Exception e) {
			
			/*** New Add ***/			
			if (e instanceof TopMessageException) {
				
				/**
				 *  檢查 F003 回應之訊息代碼: 
				 *  1.若存在於資料庫 && 註記可人工重送("Y") ==> 進人工重送處理,不需發 F011
				 *  2.其它 ==> 一律發 F011		 
				 */	
				try {					
					topmsg = ((TopMessageException)e).getMsgcode();	
					
					isNeedDoF011WithFlagN = f002t.checkMervaTopMsg(topmsg, params.get("__SCHID"));			
					isFail = isNeedDoF011WithFlagN;
					
					//進人工重送須記錄 F001.STAN 欄位值
					if ( ! this.isNeedDoF011WithFlagN )
						params.put("__FXMSGCONTENT", StrUtils.trim(params.get("STAN")));
				}
				catch(Exception ex) { }				
				
			}
			else {
				isNeedDoF011WithFlagN = true;			
				isFail = true;
			}
			
			lastException = e;
		}
		
		
//		if(isFail) {
//			isNeedDoF011WithFlagN = true;
//		}
		
		return result;
	}

	@Autowired
	public Exception getLastException() {
		return lastException;
	}

	@Autowired
	public boolean isNeedDoF011WinFlagN() {
		return isNeedDoF011WithFlagN;
	}
}
