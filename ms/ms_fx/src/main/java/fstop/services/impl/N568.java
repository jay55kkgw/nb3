package fstop.services.impl;

import java.util.Date;
import java.util.Hashtable;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import fstop.model.GrpColumns;
import fstop.model.MVH;
import fstop.model.MVHUtils;
import fstop.model.RowsGroup;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N568 extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("n568Telcomm")
	private TelCommExec n568Telcomm;

//	@Required
//	public void setN568Telcomm(TelCommExec telcomm) {
//		n568Telcomm = telcomm;
//	}

	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) {
		// 當日信用狀開狀查詢

		String enddate = "";
		String stadate = "";
		String periodStr = "";

		stadate = (String) params.get("FDATE");
		enddate = (String) params.get("TDATE");
		stadate = StrUtils.trim(stadate).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
		enddate = StrUtils.trim(enddate).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
		
		if (stadate != null && stadate.length() == 8) {
			Date sd = DateTimeUtils.parse("yyyyMMdd", stadate);
			stadate = DateTimeUtils.format("yyyy-MM-dd", sd);
			periodStr = DateTimeUtils.format("yyyy/MM/dd", sd);
		}
		if (enddate != null && enddate.length() == 8) {
			Date sd = DateTimeUtils.parse("yyyyMMdd", enddate);
			enddate = DateTimeUtils.format("yyyy-MM-dd", sd);
			periodStr += "~" + DateTimeUtils.format("yyyy/MM/dd", sd);
		} else { // 若未指定結束時間, 則使用今天日期
			enddate = DateTimeUtils.getDateShort(new Date());
			periodStr += "~" + DateTimeUtils.format("yyyy/MM/dd", new Date());
		}
		
		params.put("FDATE", stadate);
		params.put("TDATE", enddate);
		params.put("CUSIDN", params.get("UID"));
		
		TelcommResult telcommResult = n568Telcomm.query(params);
		telcommResult.getFlatValues().put("CMQTIME",
				DateTimeUtils.getDatetime(new Date()));

		telcommResult.getFlatValues().put("CMRECNUM",
				telcommResult.getValueOccurs("RREFNO") + "");

		telcommResult.getFlatValues().put("CMPERIOD", periodStr);

		return telcommResult;
	}

}
