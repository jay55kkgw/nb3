package fstop.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;
import fstop.model.IMasterValuesHelper;
import com.netbank.rest.model.pool.N920Pool;
import com.netbank.rest.model.CommonPools;
import fstop.exception.TopMessageException;
import fstop.model.EachRowCallback;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.MVHUtils;
import fstop.model.Row;
import fstop.model.Rows;
import fstop.model.RowsSelecter;
import fstop.model.TelcommResult;
import fstop.orm.dao.AdmKeyValueDao;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
@Slf4j
public class N640 extends CommonService  {
	private Logger logger = Logger.getLogger(getClass());
	@Autowired
    @Qualifier("n640Telcomm")
	private TelCommExec n640Telcomm;
	@Autowired
	private AdmKeyValueDao admKeyValueDao;
	@Autowired
	private CommonPools commonPools;
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) {
		String enddate = "";
		String stadate = "";
		String periodStr = "";
		
		stadate = (String) params.get("CMSDATE");
		enddate = (String) params.get("CMEDATE");
		stadate = StrUtils.trim(stadate).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
		enddate = StrUtils.trim(enddate).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
		
		if (stadate != null && stadate.length() == 8) {
			Date sd = DateTimeUtils.parse("yyyyMMdd", stadate);
			stadate = DateTimeUtils.getCDateShort(sd);
			periodStr = DateTimeUtils.format("yyyy/MM/dd", sd);
		}
		if (enddate != null && enddate.length() == 8) {
			Date sd = DateTimeUtils.parse("yyyyMMdd", enddate);
			enddate = DateTimeUtils.getCDateShort(sd);
			periodStr += "~" + DateTimeUtils.format("yyyy/MM/dd", sd);
		} else { // 若未指定結束時間, 則使用今天日期
			enddate = DateTimeUtils.getCDateShort(new Date());
			periodStr += "~" + DateTimeUtils.format("yyyy/MM/dd", new Date());
		}
		params.put("STADATE", stadate);
		params.put("ENDDATE", enddate);
		
		EachRowCallback eachRow = new EachRowCallback(){

			public void current(Row row) {
				
				if("D".equalsIgnoreCase(row.getValue("CODDBCR"))) {
					row.setValue("DPDCCODE", "支出");
				}
				if("C".equalsIgnoreCase(row.getValue("CODDBCR"))) {
					row.setValue("DPDCCODE", "收入");
				}
				
				//使用 adm key value 轉換 memo
				String r = admKeyValueDao.translator("MEMO", row.getValue("MEMO"));
				row.setValue("MEMO", r);
			}
		};

		MVHImpl result = new MVHImpl();
		
		queryAll(getAcnoList(params), params, result, eachRow);
		
//		if(StrUtils.isEmpty((String)params.get("ACN"))) {
//			//查詢全部帳號
//			
//		}
//		else {
//			MVHImpl resultN640 = n640Telcomm.query(params);
//			resultN640.getOccurs().setValue("ACN", (String)params.get("ACN"));
//
//			RowsSelecter rowSelecter = new RowsSelecter(resultN640.getOccurs());
//			rowSelecter.each(eachRow);
//			
//			result.addTable(resultN640, (String)params.get("ACN"));
//		}

		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		result.getFlatValues().put("CMPERIOD", periodStr);
		result.getFlatValues().put("CMRECNUM", MVHUtils.sumTablesOccurs(result) + "");

		return result;
	}
	
	private List<String> getAcnoList(Map<String, String> params) {
		List<String> acnos = new ArrayList();
		if(StrUtils.isEmpty((String)params.get("ACN"))) {
			
			IMasterValuesHelper acnosMVH = commonPools.n920.getAcnoList(params.get("CUSIDN"), new String[]{"07"}, "");
			
			int requestCount = acnosMVH.getValueOccurs("ACN");
			for(int i=0; i < requestCount; i++) {
				String acn = acnosMVH.getValueByFieldName("ACN", i+1);
				acnos.add(acn);
			}
			
		}
		else {
			acnos.add(params.get("ACN"));
		}
		return acnos;
	}	
	

	public void queryAll(List<String> acnoList, Map<String, String> params, MVHImpl resultMVH, EachRowCallback eachRow) {
 		//IMasterValuesHelper acnos = CommonPools.n920.getAcnoList(params.get("UID"), new String[]{"07"}, "");

		List<String> currAcnoList = new ArrayList<String>();
		String doneAcnos = params.get("DONEACNOS");
		String[] doneAcnoList = null;
		if (doneAcnos == null)
			doneAcnoList = new String[0];
		else
			doneAcnoList = doneAcnos.split(",");
		
 		//int requestCount = acnos.getValueOccurs("ACN");
 		for(String acn : acnoList) {
 			
 			currAcnoList.add(acn);
 			
 			//已執行過的ACNO不重複執行
 			boolean hasDone = false;
 			for (int i = 0; i < doneAcnoList.length; i++)
 			{
 				if (doneAcnoList[i].equals(acn))
 				{
 					hasDone = true;
 					break;
 				}
 			}
 			if (hasDone)
 				continue;
 			
 			//String acn = acnos.getValueByFieldName("ACN", i+1);
 			params.put("ACN", acn);
 			try {
	 			MVHImpl result = n640Telcomm.query(params);
	 			params.remove("QUERYNEXT");	//一個QUERYNEXT只能用於一個ACNO
	 			result.getOccurs().setValue("ACN", acn);
	 			result.getFlatValues().put("ACN", acn);
	 			
	 			//20190416Ian為了得到回傳值
	 			result.getOccurs().setValue("ABEND", result.getValueByFieldName("ABEND"));
	 			
	 			
	 			RowsSelecter rowSelecter = new RowsSelecter(result.getOccurs());
	 			rowSelecter.each(eachRow);
	 			
	 			resultMVH.addTable(result, acn);
	 			
	 			String queryNext = result.getValueByFieldName("QUERYNEXT");
	 			if (!queryNext.equals(""))
	 			{
	 				resultMVH.getFlatValues().put("QUERYNEXT", queryNext);
	 				
	 				currAcnoList.remove(currAcnoList.size() - 1);
	 				StringBuffer sb = new StringBuffer();
	 				if (currAcnoList.size() > 0)
	 				{
		 		 		sb.append(currAcnoList.get(0));
		 		 		for (int i = 1; i < currAcnoList.size(); i++)
		 		 		{
		 		 			sb.append("," + currAcnoList.get(i));
		 		 		}
	 				}
	 		 		resultMVH.getFlatValues().put("DONEACNOS", sb.toString());
	 				
	 				break;	 				
	 			}
	 			
			}
			catch(TopMessageException ex) {
				MVHImpl helper = new MVHImpl();
				helper.getFlatValues().put("ACN", acn);
				helper.getFlatValues().put("TOPMSG", ex.getMsgcode());
				helper.getFlatValues().put("ADMSGIN", ex.getMsgin());
				helper.getFlatValues().put("ADMSGOUT", ex.getMsgout());
				resultMVH.addTable(helper, acn);			
					
			}
 		}
	}
	

}
