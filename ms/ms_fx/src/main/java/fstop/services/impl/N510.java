package fstop.services.impl;

import fstop.exception.TopMessageException;
import fstop.model.*;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Hashtable;
import java.util.Map;
import java.util.Map.Entry;

@Slf4j
public class N510 extends CommonService {
	//private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("n510Telcomm")
	private TelCommExec n510Telcomm;
	
//	@Required
//	public void setN510Telcomm(TelCommExec telcomm) {
//		n510Telcomm = telcomm;
//	}
//
//	private TelCommExec n270Telcomm;
//
//	@Required
//	public void setN270Telcomm(TelCommExec telcomm) {
//		n270Telcomm = telcomm;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public MVH doAction(Map params) {
		//TODO LOSTYPE
		//params.put("LOSTYPE", "");
		//TODO TRNSRC
		//params.put("TRNSRC", "NB");


		MVHImpl resultN510 = null;
		try {
			resultN510 = n510Telcomm.query(params);
		}
		catch(TopMessageException e) {
			if(!"ENRD".equalsIgnoreCase(e.getMsgcode()))
				throw e;
		}
/*
		MVHImpl resultN270 = null;
		try {
			resultN270 = n270Telcomm.query(params);
		}
		catch(TopMessageException e) {
			if(!"ENRD".equalsIgnoreCase(e.getMsgcode()))
				throw e;			
		}
*/
		if(resultN510 == null && resultN510 == null)
			throw TopMessageException.create("ENRD");
		
		MVHImpl result = new MVHImpl();
		
		
		//20180321 BEN 新增 為了得到回傳值
		if(resultN510 != null) {
			result.getOccurs().addRows(resultN510.getOccurs());
			result.getFlatValues().putAll(resultN510.getFlatValues());
		}
/*		
		if(resultN270 != null)
			result.getOccurs().addRows(resultN270.getOccurs());
*/		
		
		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		result.getFlatValues().put("CMRECNUM", result.getValueOccurs("ACN") + "");

		try {
			String[] groupBy = new String[]{"CUID"};
			RowsGroup grp = MVHUtils.group(result.getOccurs(), groupBy);
			
			Map<GrpColumns, Double> sum = grp.sum("AVAILABLE");
			int ind = 1;
			for(Entry<GrpColumns, Double> ent : sum.entrySet()) {
				GrpColumns grpCols = ent.getKey();
	
				Map<String, String> m = MVHUtils.toFlatValues(grpCols, "AVA", ind);
				result.getFlatValues().putAll(m);
				result.getFlatValues().put("AVASUBAMT_" + ind, ent.getValue() + "");				
				result.getFlatValues().put("AVASUBAMTRECNUM_" + ind, grp.getGroups().get(grpCols).getSelectedRows().size() + "");
				
				ind++;
			}
		}
		finally {}

		try {
			String[] groupBy = new String[]{"CUID"};
			RowsGroup grp = MVHUtils.group(result.getOccurs(), groupBy);
			
			Map<GrpColumns, Double> sum = grp.sum("BALANCE");
			int ind = 1;
			for(Entry<GrpColumns, Double> ent : sum.entrySet()) {
				GrpColumns grpCols = ent.getKey();
	
				Map<String, String> m = MVHUtils.toFlatValues(grpCols, "BAL", ind);
				result.getFlatValues().putAll(m);
				result.getFlatValues().put("BALSUBAMT_" + ind, ent.getValue() + "");				
				result.getFlatValues().put("BALSUBAMTRECNUM_" + ind, grp.getGroups().get(grpCols).getSelectedRows().size() + "");
				
				ind++;
			}
		}
		finally {}
		try {
			String[] groupBy = new String[]{"CUID"};
			RowsGroup grp = MVHUtils.group(result.getOccurs(), groupBy);
			
			Map<GrpColumns, Double> sum = grp.sum("NTDBAL");
			int ind = 1;
			for(Entry<GrpColumns, Double> ent : sum.entrySet()) {
				GrpColumns grpCols = ent.getKey();
	
				Map<String, String> m = MVHUtils.toFlatValues(grpCols, "NTDBAL", ind);
				result.getFlatValues().putAll(m);
				result.getFlatValues().put("NTDBALSUBAMT_" + ind, ent.getValue() + "");				
				result.getFlatValues().put("NTDBALSUBAMTRECNUM_" + ind, grp.getGroups().get(grpCols).getSelectedRows().size() + "");
				
				ind++;
			}
		}
		finally {}		
		return result;
	}


}
