package fstop.services.impl.fx;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.netbank.rest.model.CommonPools;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.exception.ToRuntimeException;
import fstop.exception.UncheckedException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.orm.po.ADMMAILLOG;
import fstop.orm.po.TXNFXRECORD;
import fstop.services.BookingAware;
import fstop.services.NoticeInfo;
import fstop.services.NoticeInfoGetter;
import fstop.services.TelCommExecWrapper;
import fstop.services.TransferNotice;
import fstop.services.WriteLogInterface;
import fstop.services.impl.F003T;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import topibs.utility.NumericUtil;

@Slf4j
public class F003Controller implements BookingAware, WriteLogInterface
{
	// Logger logger = Logger.getLogger(getClass());

	@Autowired
	private F003T f003t;

	@Autowired
	private CommonPools commonPools;

	// @Required
	// public void setF003t(F003T f003t) {
	// this.f003t = f003t;
	// }

	@Override
	public MVHImpl booking(Map<String, String> params)
	{
		return null;
	}

	@Override
	public MVHImpl online(final Map<String, String> params, final TransferNotice trNotice)
	{
		// logger.info("{{{\r\n" + JSONUtils.map2json(params) + "\r\n}}}");

		params.put("__FXMSGSEQNO", StrUtils.isEmpty(params.get("__FXMSGSEQNO")) ? "" : params.get("__FXMSGSEQNO"));

		final F003_F007YTelcomm f007y = new F003_F007YTelcomm(f003t);
		final F003_F001Telcomm f001 = new F003_F001Telcomm(f003t);
		final F003_F003Telcomm f003 = new F003_F003Telcomm(f003t, f001);
		final F003_MervaTelcomm merva = new F003_MervaTelcomm(f003t, f001);
		final F003_F011Telcomm f011 = new F003_F011Telcomm(f003t);

		final List<FxTelCommStep> actions = new ArrayList<FxTelCommStep>();
		String trans_status = (String) params.get("__TRANS_STATUS");
		if (StrUtils.isEmpty(trans_status))
		{
			trans_status = "ONLINE";
		}

		boolean isScheduleRecord = (StrUtils.trim(params.get("__SCHID")).length() > 0
				&& !"0".equals(StrUtils.trim(params.get("__SCHID"))));

		if ("ONLINE".equalsIgnoreCase(trans_status) || ("RESEND".equalsIgnoreCase(trans_status) && !isScheduleRecord))
		{
			if (!StrUtils.trim(params.get("TXCCY")).equalsIgnoreCase(params.get("RETCCY")))
				actions.add(f007y);
			actions.add(f001);
			actions.add(f003);
			actions.add(merva);
			actions.add(f011);
		}
		else
		{
			// 重送
			// 不同幣別時
			if (!StrUtils.trim(params.get("TXCCY")).equalsIgnoreCase(params.get("RETCCY")))
				actions.add(f007y);
			actions.add(f001);
			actions.add(f003);
			actions.add(merva);
			actions.add(f011);
		}

		String FXMSGSEQNO = StrUtils.trim(params.get("__FXMSGSEQNO"));
		if (FXMSGSEQNO.length() == 0)
		{ // 尚未開始執行
			f007y.setSkip(false);
			f001.setSkip(false);
			f003.setSkip(false);
			merva.setSkip(false);
			f011.setSkip(false);
		}
		else if (FXMSGSEQNO.endsWith("0"))
		{ // F007 成功
			f007y.setSkip(true);
			f001.setSkip(false);
			f003.setSkip(false);
			merva.setSkip(false);
			f011.setSkip(false);
		}
		else if (FXMSGSEQNO.endsWith("1"))
		{ // F001 成功
			f007y.setSkip(true);
			f001.setSkip(true);
			f003.setSkip(false);
			merva.setSkip(false);
			f011.setSkip(false);
		}
		else if (FXMSGSEQNO.endsWith("2"))
		{ // f003 成功
			f007y.setSkip(true);
			f001.setSkip(true);
			f003.setSkip(true);
			merva.setSkip(false);
			f011.setSkip(false);
		}
		else if (FXMSGSEQNO.endsWith("3"))
		{ // MERVA 成功
			f007y.setSkip(true);
			f001.setSkip(true);
			f003.setSkip(true);
			merva.setSkip(true);
			f011.setSkip(false);
		}
		else if (FXMSGSEQNO.endsWith("4"))
		{ // F011 成功 (結束電文)
			f007y.setSkip(true);
			f001.setSkip(true);
			f003.setSkip(true);
			merva.setSkip(true);
			f011.setSkip(true);
		}
		else if (FXMSGSEQNO.endsWith("5") || FXMSGSEQNO.endsWith("6"))
		{ // F007/F005 沖正成功
			f007y.setSkip(true);
			f001.setSkip(true);
			f003.setSkip(true);
			merva.setSkip(true);
			f011.setSkip(false);
		}

		List<String> commands = new ArrayList();
		// commands.add("PINKEY(CMPASSWORD)");
		// CommandUtils.doBefore(commands, params);

		final TelCommExecWrapper execwrapper = new TelCommExecWrapper(new TelCommExec()
		{

			public TelcommResult query(Map params)
			{
				for (FxTelCommStep step : actions)
				{
					MVHImpl result = step.query(params);
					if (step.isNeedDoF011WinFlagN())
					{
						params.put("OKFLAG", "N");
						f003t.doF011(params);
						params.put("__FXMSGSEQNO", StrUtils.trim((String) params.get("__FXMSGSEQNO")) + "4");
						// 進人工重送須記錄 F001.STAN 欄位值
						params.put("__FXMSGCONTENT", StrUtils.trim((String) params.get("STAN")));
					}

					if (step.getLastException() != null)
					{
						if (step.getLastException() instanceof UncheckedException)
							throw (UncheckedException) step.getLastException();
						else throw new ToRuntimeException(step.getLastException().getMessage(), step.getLastException());
					}

				}

				return f003.getTelcommResult();
			}

		});

		MVHImpl result = execwrapper.query(params);

		MVHImpl f001Result = f001.getTelcommResult();

		if (f001Result != null)
		{
			// params.put("REFNO",f001Result.getValueByFieldName("REFNO"));
			// params.put("ORDACC",f001Result.getValueByFieldName("ORDACC"));
			params.put("PMTCCY", f001Result.getValueByFieldName("PMTCCY"));
			params.put("PMTAMT", f001Result.getValueByFieldName("PMTAMT"));
			params.put("ORGCCY", f001Result.getValueByFieldName("ORGCCY"));
			params.put("ORGAMT", f001Result.getValueByFieldName("ORGAMT"));
			params.put("EXRATE", f001Result.getValueByFieldName("EXRATE"));
		}
		// else
		// params.put("REFNO","");

		log.debug(ESAPIUtil.vaildLog("params >>> "+params));
		log.debug(ESAPIUtil.vaildLog("execwrapper >>> {}"+execwrapper));
		log.debug(ESAPIUtil.vaildLog("result >>> {}"+result));

		final TXNFXRECORD record = (TXNFXRECORD) writeLog(execwrapper, params, result);

		final TelCommExecWrapper execwrapperFinal = execwrapper;
		/*** 付款人交易通知 ***/
		trNotice.sendNotice(new NoticeInfoGetter()
		{

			public NoticeInfo getNoticeInfo()
			{
				NoticeInfo result = new NoticeInfo(params.get("UID"), params.get("DPMYEMAIL"), params.get("CMTRMAIL"));
				result.setTemplateName("FXNOTIFYF003");

				// result.setDpretxstatus(record.getFXRETXSTATUS());
				result.getParams().put("__RECORD_ADTXNO", record.getADTXNO());
				result.setADTXNO(record.getADTXNO());

				result.setException(execwrapperFinal.getLastException());
				String datetime = record.getFXTXDATE() + record.getFXTXTIME();
				Date d = DateTimeUtils.parse("yyyyMMddHHmmss", datetime);
				String trantime = DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", d);
				result.getParams().put("#TRANDATE", trantime);
				result.getParams().put("#SEQNO", params.get("STAN"));
				result.getParams().put("#BANKSEQNO", params.get("REFNO"));

				String acn = params.get("ORDACC");
				try
				{
					acn = StrUtils.left(acn, acn.length() - 5) + "***" + StrUtils.right(acn, 2);
				}
				catch (Exception e)
				{
				}

				String outacn = params.get("BENACC");
				try
				{
					outacn = StrUtils.left(outacn, outacn.length() - 5) + "***" + StrUtils.right(outacn, 2);
				}
				catch (Exception e)
				{
				}

				log.debug("OUTACN = " + outacn + "&nbsp;&nbsp;" + params.get("BENNAME"));
				result.getParams().put("#OUTACN", outacn + "&nbsp;&nbsp;" + params.get("BENNAME"));
				log.debug("INACN = " + acn + "&nbsp;&nbsp;" + params.get("ORDNAME"));
				result.getParams().put("#INACN", acn + "&nbsp;&nbsp;" + params.get("ORDNAME"));

				String payeedata = params.get("ORDNAME") + "<BR>" + params.get("ORDAD1") + "<BR>" + params.get("ORDAD2")
						+ "<BR>" + params.get("ORDAD3");
				result.getParams().put("#PAYEEDATA", payeedata);
				String payeebankdata = params.get("RCVBANK") + "<BR>" + params.get("RCVAD1") + "<BR>"
						+ params.get("RCVAD2") + "<BR>" + params.get("RCVAD3");
				log.debug("PAYEEBANKDATA = " + payeebankdata);
				result.getParams().put("#PAYEEBANKDATA", payeebankdata);

				String fxwdamt = NumericUtil.formatNumberString(params.get("PMTAMT"), 2);
				log.debug("OUTAMT = " + params.get("PMTCCY") + " " + fxwdamt);
				result.getParams().put("#OUTAMT", params.get("PMTCCY") + " " + fxwdamt);

				String fxsvamt = NumericUtil.formatNumberString(params.get("ORGAMT"), 2);
				log.debug("INAMT = " + params.get("ORGCCY") + " " + fxsvamt);
				result.getParams().put("#INAMT", params.get("ORGCCY") + " " + fxsvamt);
				String str_Rate = params.get("EXRATE");
				if (str_Rate == null)
					str_Rate = "";
				else if (str_Rate != null && str_Rate.replace("0", "").equals(""))
					str_Rate = "";
				else str_Rate = NumericUtil.formatNumberString(params.get("EXRATE"), 8);
				log.debug("TRTATE = " + str_Rate);
				result.getParams().put("#TRTATE", str_Rate);
				/*
				 * int i_FeeIndex = record.getFXEFEE().indexOf("/"); result.getParams().put("#FEE", (i_FeeIndex != -1 &&
				 * i_FeeIndex != 0)?record.getFXEFEE().substring(0, i_FeeIndex):"");
				 */
				String commamt2 = NumericUtil.formatNumberString(params.get("COMMAMT2"), 2);
				log.debug("FEE = " + params.get("COMMCCY2") + " " + commamt2);
				result.getParams().put("#FEE", params.get("COMMCCY2") + " " + commamt2);

				ADMMAILLOG admmaillog = new ADMMAILLOG();
				admmaillog.setADACNO(record.getFXWDAC());

				String trans_status = (String) params.get("__TRANS_STATUS");
				if (StrUtils.isEmpty(trans_status))
				{
					trans_status = "FXONLINE";
				}
				else if ("SEND".equals(trans_status))
				{
					trans_status = "FXSCH";
				}
				else if ("RESEND".equals(trans_status))
				{
					trans_status = "FXSCHRE";
				}

				admmaillog.setADBATCHNO(trans_status);
				admmaillog.setADMAILACNO(StrUtils.trim(params.get("CMTRMAIL")));
				admmaillog.setADUSERID(record.getFXUSERID());
				admmaillog.setADUSERNAME("");
				admmaillog.setADSENDTYPE("3");
				admmaillog.setADSENDTIME(datetime);
				admmaillog.setADSENDSTATUS("");

				result.setADMMAILLOG(admmaillog);

				return result;
			}

		});

		execwrapper.throwException();

		// 合併 F001 及 F003 的結果
		if (result != null)
		{

			result.getFlatValues().putAll(f001.getTelcommResult().getFlatValues());
			result.getFlatValues().put("ADTXNO", record.getADTXNO());

			// //成功時產生交易單據
			// 電文結果更新request參數
			result.getFlatValues().putAll(params);

			log.trace("F003Controller.after.Merge.result: {}", result.getFlatValues());

			/**
			 * 交易單據 TXNFXRECORD.FXCERT欄位改放交易單據所需資料的JSON
			 */
			try
			{
				MVHImpl mvhimpl = null;
				// N920
				MVH mvh = new MVHImpl();
				mvh = commonPools.n920.getAcnoList(result.getFlatValues().get("UID"), new String[] { "01", "02", "03",
						"04", "05", "07" }, "Y");
				mvhimpl = (MVHImpl) mvh;

				// 列印時間
				result.getFlatValues().put("curentDateTime", DateTimeUtils.getDatetime(new Date()));
				// 姓名
				result.getFlatValues().put("NAME", mvhimpl.getFlatValues().get("NAME"));
				// 付款日期
				result.getFlatValues().put("currentDate", DateTimeUtils.getDateShort(new Date()));

				Map<String, String> callDataF003T = result.getFlatValues();
				// 手續費
				// 費用合計
				String str_CommAmt = String.valueOf(callDataF003T.get("COMMAMT2"));
				String str_CommCcy = String.valueOf(callDataF003T.get("COMMCCY2"));
				if (str_CommCcy.equals("TWD") || str_CommCcy.equals("JPY"))
				{
					str_CommAmt = str_CommAmt.substring(0, str_CommAmt.length() - 2);
				}
				// 匯款金額
				String str_OrgAmt = String.valueOf(callDataF003T.get("ORGAMT"));
				String str_OrgCcy = String.valueOf(callDataF003T.get("ORGCCY"));
				if (str_OrgCcy.equals("TWD") || str_OrgCcy.equals("JPY"))
				{
					str_OrgAmt = str_OrgAmt.substring(0, str_OrgAmt.length() - 2);
				}
				// 解款金額
				String str_PmtAmt = String.valueOf(callDataF003T.get("PMTAMT"));
				String str_PmtCcy = String.valueOf(callDataF003T.get("PMTCCY"));
				if (str_PmtCcy.equals("TWD") || str_PmtCcy.equals("JPY"))
				{
					str_PmtAmt = str_PmtAmt.substring(0, str_PmtAmt.length() - 2);
				}
				// 匯率
				String str_Rate = String.valueOf(callDataF003T.get("EXRATE"));
				if (callDataF003T.get("EXRATE") == null)
				{
					str_Rate = "";
				}
				else if (str_Rate != null && str_Rate.replace("0", "").equals(""))
				{
					str_Rate = "";
				}
				else
				{
					str_Rate = NumericUtil.formatNumberString(str_Rate, 8);
				}
				result.getFlatValues().put("str_Rate", str_Rate);// 匯率
				result.getFlatValues().put("str_CommAmt", NumericUtil.formatNumberStringByCcy(str_CommAmt, 2, str_CommCcy));//// 手續費
				result.getFlatValues().put("str_OrgAmt", NumericUtil.formatNumberStringByCcy(str_OrgAmt, 2, str_OrgCcy));//// 匯款金額
				result.getFlatValues().put("str_PmtAmt", NumericUtil.formatNumberStringByCcy(str_PmtAmt, 2, str_PmtCcy));// //
				result.getFlatValues().put("jspTitle", "外匯匯入匯款線上解款");
				// 放入TXNFXRECORD.FXCERT
				String str_Result = CodeUtil.toJson(result.getFlatValues());
				log.debug("F003Controller 交易單據  str_Result >>>>>>>>>>>>>>>>>>>>>>>>>>>  {}", str_Result);
				commonPools.fxUtils.updateFxCert(record.getADTXNO(), str_Result);
			}
			catch (Exception e)
			{
				throw new ToRuntimeException("產生交易單據發生錯誤.", e);
			}
		}
		return result;
	}

	@Override
	public Object writeLog(final TelCommExecWrapper execwrapper, final Map _params, final MVHImpl result)
	{
		Object obj = f003t.writeLog(execwrapper, _params, result);
		return obj;
	}

}
