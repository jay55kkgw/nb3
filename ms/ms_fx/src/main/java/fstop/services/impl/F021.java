package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class F021 extends CommonService {

	@Autowired
	@Qualifier("f021Telcomm")
	private TelCommExec f021Telcomm;

	@Override
	public MVH doAction(Map params) {

		/*
		 * 跑迴圈 F021
		 * 
		 * 找出所有 pendding 的筆數發送
		 * 
		 * 
		 * PCODE = TxnFxRecord pending(Z014) 之PCODE TXFLAG = 'A' CNT = TxnFxRecord
		 * pending 之總筆數 STAN = TxnFxRecord pending 之STAN CUSTACC = TxnFxRecord pending
		 * 之FXWDAC BENACC = TxnFxRecord pending 之FXSVAC
		 * 
		 */

		TelcommResult helper = f021Telcomm.query(params);

		helper.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		// helper.getFlatValues().put("CMRECNUM", helper.getValueOccurs("ACN") + "");

		MVHImpl result = new MVHImpl();

		return result;
	}
}
