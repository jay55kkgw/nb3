package fstop.services.impl;

import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N555 extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("n555Telcomm")
	private TelCommExec n555Telcomm;

//	@Required
//	public void setN555Telcomm(TelCommExec telcomm) {
//		n555Telcomm = telcomm;
//	}

	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) {
		
		params.put("QUERYNEXT", "");
		
		TelcommResult telcommResult = n555Telcomm.query(params);
		telcommResult.getFlatValues().put("CMQTIME",
				DateTimeUtils.getDatetime(new Date()));

		telcommResult.getFlatValues().put("CMRECNUM",
				telcommResult.getValueOccurs("RIBNO") + "");


		return telcommResult;
	}

}
