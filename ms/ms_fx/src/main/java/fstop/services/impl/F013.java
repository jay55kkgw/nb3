package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class F013 extends CommonService {

	@Autowired
	@Qualifier("f013Telcomm")
	private TelCommExec f013Telcomm;

	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) {

		params.put("CUSTID", params.get("UID"));
		params.put("WS_ID", params.get("UID"));

		TelcommResult telcommResult = f013Telcomm.query(params);
		telcommResult.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));

		telcommResult.getFlatValues().put("CMRECNUM", telcommResult.getValueOccurs("RREFNO") + "");

		return telcommResult;
	}

}
