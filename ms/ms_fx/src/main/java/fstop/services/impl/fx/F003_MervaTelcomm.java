package fstop.services.impl.fx;

import java.util.Map;
import java.util.Vector;

import org.springframework.beans.factory.annotation.Autowired;

import fstop.exception.TopMessageException;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.orm.dao.AdmMsgCodeDao;
import fstop.services.impl.F003T;
import fstop.util.JSONUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class F003_MervaTelcomm implements FxTelCommStep {
	
//	Logger logger = Logger.getLogger(getClass());

	@Autowired
	private F003T f003t;
	
	private boolean isNeedDoF011WithFlagN = false; 

	@Autowired
	private TelcommResult telcommResult = null;

	@Autowired
	private F003_F001Telcomm f001TelcommCommand;

	@Autowired
	private AdmMsgCodeDao admMsgCodeDao;

//	@Required
//	public void setAdmMsgCodeDao(AdmMsgCodeDao admMsgCodeDao) {
//		this.admMsgCodeDao = admMsgCodeDao;
//	}
	
	boolean isSkip = false;

	@Override
	public boolean isSkip() {
		return isSkip;
	}

	public void setSkip(boolean isSkip) {
		this.isSkip = isSkip;
	}

	public F003_MervaTelcomm(F003T f003t, F003_F001Telcomm f001TelcommCommand) {
		this.f003t = f003t;
		this.f001TelcommCommand = f001TelcommCommand;
	}
	
	public Exception lastException;

	@Override
	public TelcommResult query(Map _params) {
		Map<String, String> params = _params;

		if(isSkip()) {
			String oldResult = (String)params.get("__MERVA_RESULT");
			Map m = JSONUtils.json2map(oldResult);
			TelcommResult telResult = new TelcommResult(new Vector());
			telResult.getFlatValues().putAll(m);
			this.telcommResult = telResult;
			return telResult;
		}
		
		TelcommResult result = null;
		boolean isFail = false;
		try {
			String str_SWTKIND = params.get("SWTKIND").toString();
						
			// CGI Stored XSS
//			log.debug("@@@ F003_MervTelecom str_SWTKIND == "+str_SWTKIND);

			
			/*** F001 電文回應欄位 SWTKIND (SWIFT電文發送方式)不為 3 時,不可 CALL MERVA 電文 ***/  //MT199開放可call merva電文  SWIFT GPI需求			
			//if (str_SWTKIND.equals("3")) {
				telcommResult = f003t.doMerva(params, f001TelcommCommand.getTelcommResult());
				result = telcommResult;
				
				params.put("__MERVA_RESULT", JSONUtils.map2json(result.getFlatValues()));			
				params.put("__FXMSGSEQNO", StrUtils.trim(params.get("__FXMSGSEQNO")) + "3");				
			//}// end if (str_SWTKIND.equals("1") || str_SWTKIND.equals("2"))
			//else {
			//	result = new TelcommResult(new Vector());
			//}
		}
		catch(Exception e) {
			isFail = true;
			if(e instanceof TopMessageException) {
				
				/**
				 *  檢查 MERVA 回應之訊息代碼: 
				 *  1.若存在於資料庫 && 註記可人工重送("Y") ==> 進人工重送處理
				 *  2.其它 ==> 一律作 F005 沖正				 
				 */	
				try {					
					String topmsg = ((TopMessageException)e).getMsgcode();
					
					if (topmsg.startsWith("MVA"))
						topmsg = ((TopMessageException)e).getMsgcode().substring(3);
						
					
					isNeedDoF011WithFlagN = f003t.checkTopMsg(topmsg, params.get("__SCHID"));	
					isFail = isNeedDoF011WithFlagN;
					
					//進人工重送須記錄 F001.STAN 欄位值
					if ( ! this.isNeedDoF011WithFlagN )
						params.put("__FXMSGCONTENT", StrUtils.trim(params.get("STAN")));
					
					// CGI Stored XSS
//					log.debug("@@@ F003_MERVA __FXMSGCONTENT == "+params.get("__FXMSGCONTENT"));	

				}
				catch(Exception ex) {
					log.error("", ex);
				}
				
			}			
			else {
				isNeedDoF011WithFlagN = true;			
				isFail = true;
			}
			
			lastException = e;
		}

		if(isFail) {
			try {
				params.remove("F005FAILFLAG");
								
				result = f003t.doF005(params);
				
				params.put("__FXMSGSEQNO", StrUtils.trim(params.get("__FXMSGSEQNO")) + "6");	
				isNeedDoF011WithFlagN = true;				
			}
			catch(Exception e) {
				
				if(lastException != null) {
					log.error("MERVA 已發生 " + lastException.getMessage() + ", F005  時又發生 " + e.getMessage());
				}
				
//				if(e instanceof TopMessageException) {
				
					/**
					 *  若 F005 回應之訊息代碼不為成功 ==> 一律進人工重送處理				 
					 */	
					try {					
						isNeedDoF011WithFlagN = false;
						params.put("F005FAILFLAG", "Y");
						
						//進人工重送須記錄 F001.STAN 欄位值
						if ( ! this.isNeedDoF011WithFlagN )
							params.put("__FXMSGCONTENT", StrUtils.trim(params.get("STAN")));
						
						// CGI Stored XSS
//						log.debug("@@@ F003_MervaTelcomm  __FXMSGCONTENT == "+params.get("__FXMSGCONTENT"));	

					}
					catch(Exception ex) { }
					
//				}			
//				else {
//					isNeedDoF011WithFlagN = true;			
//				}
				
				lastException = e;				
			}
		}
		
		return result;
	}

	@Override
	public Exception getLastException() {
		return lastException;
	}

	@Override
	public boolean isNeedDoF011WinFlagN() {
		return isNeedDoF011WithFlagN;
	}
	
	public MVHImpl getTelcommResult() {
		return telcommResult;
	}
	
	
}
