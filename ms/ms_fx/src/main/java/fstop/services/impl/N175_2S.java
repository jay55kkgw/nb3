package fstop.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.rest.comm.bean.DoIDGateCommand_In;
import com.netbank.rest.comm.controller.CommonIDGATEController;
import com.netbank.rest.model.pool.FxUtils;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import encryptUtil.EncryptUtil;
import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.AdmMsgCodeDao;
import fstop.orm.dao.TxnFxRecordDao;
import fstop.orm.dao.TxnFxSchPayDao;
import fstop.orm.dao.TxnFxSchPayDataDao;
import fstop.orm.dao.TxnTwSchPayDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.ADMMAILLOG;
import fstop.orm.po.TXNFXRECORD;
import fstop.orm.po.TXNFXSCHPAY;
import fstop.orm.po.TXNFXSCHPAYDATA;
import fstop.orm.po.TXNREQINFO;
import fstop.services.BookingAware;
import fstop.services.BookingUtils;
import fstop.services.CommonService;
import fstop.services.NoticeInfo;
import fstop.services.NoticeInfoGetter;
import fstop.services.OnLineTransferNotice;
import fstop.services.TelCommExecWrapper;
import fstop.services.TransferNotice;
import fstop.services.WriteLogInterface;
import fstop.telcomm.TelCommExec;
import fstop.telcomm.cmd.CommandUtils;
import fstop.util.DateTimeUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import topibs.utility.NumericUtil;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class N175_2S extends CommonService implements BookingAware,WriteLogInterface {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("n175_1Telcomm")
	private TelCommExec n175_1Telcomm;
	@Autowired
	@Qualifier("n175_2Telcomm")
	private TelCommExec n175_2Telcomm;
	@Autowired
	private TxnFxRecordDao txnFxRecordDao; 

	@Autowired
	private AdmMsgCodeDao admMsgCodeDao;

	@Autowired
	private TxnUserDao txnUserDao;
	
	@Autowired
	private TxnFxSchPayDao TxnFxSchPayDao;
	
	@Autowired
	private TxnFxSchPayDataDao txnFxSchPayDataDao; // 預約資料
	
	@Autowired
	CommonIDGATEController commonIDGATEController;

	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params) { 
		final Map<String, String> params = _params;

		/*** 驗證交易密碼 - 開始 ***/
		if (params.get("FGTXWAY") == null) {
			throw TopMessageException.create("S302", (String)params.get("ADOPID"));				
		}
		else if("0".equals(params.get("FGTXWAY"))) {  //使用交易密碼
			String TransPassUpdate = (String)params.get("TRANSPASSUPDATE");
			String Phase2 = (String)params.get("PHASE2");
			TelCommExec n950CMTelcomm;
			
			if(StrUtils.isNotEmpty(TransPassUpdate))
			{
				if(TransPassUpdate.equals("1") || Phase2.equals("ON"))
				{
					n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n951CMTelcomm");
				}
				else
				{
					n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n950CMTelcomm");
				}
			}
			else
			{
				n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n950CMTelcomm");
			}
			
			//N950會發兩次所以需FGTXWAY=""，N950回應後再把FGTXWAY改成原值即可 2010.09.29
			String fgtxway = (String)params.get("FGTXWAY");
			params.put("FGTXWAY", "");
			MVHImpl n950Result = n950CMTelcomm.query(params);
			
			//若驗證失敗, 則會發動 TopMessageException
			params.put("FGTXWAY", fgtxway);
		}else if("7".equals(params.get("FGTXWAY"))){
			String certACN = "";
			while (certACN.length() < 19)
				certACN += "C";
			params.put("CERTACN", certACN);
		}		
		/*** 驗證交易密碼 - 結束 ***/
		
		String stadate = (String)params.get("DPISDT");  // 民國年月日
		String enddate = (String)params.get("DUEDAT");  // 民國年月日
		stadate = StrUtils.trim(stadate).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
		enddate = StrUtils.trim(enddate).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
		
		params.put("DPISDT", stadate);
		params.put("DUEDAT", enddate);		
		params.put("TXNTYPE", "01");

		//2013/03/04 新增預約機制
		String fgtrdate = params.get("FGTRDATE");
		if("0".equals(fgtrdate)) {
			//即時
			FxUtils util = new FxUtils();
			if (! util.isFxBizTime()) {
				throw TopMessageException.create("Z400", "N175");
			}
			
			OnLineTransferNotice trNotice = new OnLineTransferNotice();
			
			return online(params, trNotice);
		}
		else if("1".equals(fgtrdate)) {
			//預約
			return booking(params);
		}
		
		return new MVHImpl();
	}

	@Override
	public Object writeLog(TelCommExecWrapper execwrapper, Map<String, String> params, MVHImpl result) {

		// 建立交易紀錄log 並檢查是否需更新預約檔
		TXNFXRECORD record = execwrapper.createTxnFxRecordCheckupdatePayData(params, txnFxSchPayDataDao);
		
		record.setADOPID("N175");
		record.setFXUSERID(params.get("UID"));
		record.setFXWDAC(params.get("ACN"));  //轉出帳號
		record.setFXSVBH("");  //轉入分行
		record.setFXSVAC(params.get("FDPNUM"));  //轉入帳號
		record.setFXWDCURR(params.get("CUID"));  //轉出幣別
		record.setFXSVCURR(params.get("CUID"));  //轉入幣別

		record.setFXWDAMT(params.get("AMT"));  // 轉出金額
	
		record.setFXSVAMT("");  // 轉入金額
		
		record.setFXTXMEMO(params.get("CMTRMEMO"));
		record.setFXTXMAILS(params.get("CMTRMAIL"));
		record.setFXTXMAILMEMO(params.get("CMMAILMEMO"));  //CMMAILMEMO 對應的欄位
		// 微服務移除欄位-預約編號
//		record.setFXSCHNO(params.get("__SCHNO")==null?"" : params.get("__SCHNO"));
		
		TXNREQINFO titainfo = null;
		// 微服務移除欄位-交易上行資訊
//		if(record.getFXTITAINFO() == 0) {
			titainfo = new TXNREQINFO();
			titainfo.setREQINFO(BookingUtils.requestInfo(params));
//		}
		
		
	    String str_MsgCode = record.getFXEXCODE();                
		String fxFlag = admMsgCodeDao.findFXRESEND(str_MsgCode);
		
		// 微服務移除欄位-重送交易狀態
		//可人工重送
//		if (fxFlag.equals("Y"))	
//			record.setFXRETXSTATUS("5");
//		else
//			record.setFXRETXSTATUS("0");	
		
		txnFxRecordDao.writeTxnRecord(titainfo, record);
		
		return record;
	}

	@Override
	public MVHImpl online(final Map<String, String> params, TransferNotice trNotice) {
		
		Date d = new Date();
		params.put("DATE", DateTimeUtils.getCDateShort(d));
		params.put("TIME", DateTimeUtils.format("HHmmss", d));

		TelCommExecWrapper execwrapper = new TelCommExecWrapper(n175_1Telcomm);		
		MVHImpl result = execwrapper.query(params); //N175_1 電文
		
//		execwrapper.throwException();
		if(null!=execwrapper.getLastException()) {
			execwrapper.throwException();
		}
		
		params.put("TXNTYPE", "02");		
		params.put("INT", result.getValueByFieldName("INT"));
		params.put("TAX", result.getValueByFieldName("TAX"));
		params.put("PAIAFTX", result.getValueByFieldName("PAIAFTX"));
		params.put("NHITAX", result.getValueByFieldName("NHITAX"));
		execwrapper = new TelCommExecWrapper(n175_2Telcomm);
		result = execwrapper.query(params); //N175 _2電文
		final TXNFXRECORD record = (TXNFXRECORD)writeLog(execwrapper, params, result);
			
		final TelCommExecWrapper execwrapperFinal = execwrapper;
		//OnLineTransferNotice trNotice = new OnLineTransferNotice();
		trNotice.sendNotice(new NoticeInfoGetter() {

			public NoticeInfo getNoticeInfo() { 
				NoticeInfo result = new NoticeInfo(params.get("UID"), params.get("DPMYEMAIL"), params.get("CMTRMAIL"));
				result.setTemplateName("FXNOTIFY");				
				result.setADTXNO(record.getADTXNO());				
				result.setException(execwrapperFinal.getLastException());
				String datetime = record.getFXTXDATE() + record.getFXTXTIME();
				Date d = DateTimeUtils.parse("yyyyMMddHHmmss", datetime);
				String trantime = DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", d);
				result.getParams().put("#TXNAME", "外匯綜存定存解約");					
				result.getParams().put("#TRANTIME", trantime);
				result.getParams().put("#SEQNO", "");
				
				String acn = record.getFXSVAC();
				try {
					acn = StrUtils.left(acn, acn.length() - 5) + "***" + StrUtils.right(acn, 2);
				}
				catch(Exception e){}
				
				String outacn = record.getFXWDAC();
				try {
					outacn = StrUtils.left(outacn, outacn.length() - 5) + "***" + StrUtils.right(outacn, 2);
				}
				catch(Exception e){}
				
				result.getParams().put("#ACN", acn);
				result.getParams().put("#OUTACN", outacn);
				result.getParams().put("#OUTCURRENCY", record.getFXWDCURR());
				String dptxamtfmt = NumericUtil.formatNumberString(record.getFXWDAMT(), 2);
				result.getParams().put("#AMT", dptxamtfmt);
				result.getParams().put("#MEMO", params.get("CMMAILMEMO"));
				result.getParams().put("#NHITAX", NumericUtil.formatNumberString(params.get("NHITAX"),2));
				
				ADMMAILLOG admmaillog = new ADMMAILLOG();
				admmaillog.setADACNO(record.getFXWDAC());
				
				String trans_status = (String)params.get("__TRANS_STATUS");
				if(StrUtils.isEmpty(trans_status)) {
					trans_status = "FXONLINE";
				}
				else if("SEND".equals(trans_status)) {
					trans_status = "FXSCH";
				}
				else if("RESEND".equals(trans_status)) {
					trans_status = "FXSCHRE";
				}
				
				admmaillog.setADBATCHNO(trans_status);
				admmaillog.setADMAILACNO(StrUtils.trim(params.get("CMTRMAIL")));
				admmaillog.setADUSERID(record.getFXUSERID());
				admmaillog.setADUSERNAME("");
				admmaillog.setADSENDTYPE("3");
				admmaillog.setADSENDTIME(datetime);
				admmaillog.setADSENDSTATUS("");
				
				result.setADMMAILLOG(admmaillog);
				
				return result;
			}
			
		});
		
		execwrapper.throwException();
		
		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		
		return result;
	}
	
	
	/**
	 * 預約交易 使用新的 TXNFXSCHPAY，邏輯與舊booking方法相同
	 * @param params
	 * @return
	 */
	@Override
	public MVHImpl booking(Map<String, String> params) {
		 	
		TXNFXSCHPAY schedule = new TXNFXSCHPAY();
		
		MVHImpl result = new MVHImpl();
		try
		{
			log.info("***************************************預約編號***************************************************");
			//預約編號(YYMMDD+三位流水號)
			String dptmpschno = "";
			dptmpschno = txnUserDao.getDptmpschno((String)params.get("UID"));
			//賽入預約編號
			schedule.setFXSCHNO(dptmpschno);
			log.info("***************************************預約編號***************************************************");
			
			schedule.setADOPID(DateTimeUtils.format("yyyyMMddHHmmss" , new Date()) + params.get("UID") );
			String f = params.get("FGTRDATE");  //判斷預約周期
			if("1".equals(f)) {  //預約某一天
				params.put("CMTRDATE", StrUtils.trim(params.get("CMTRDATE")).replaceAll("/", ""));
				if(params.get("CMTRDATE").length()==0)
				{
					throw TopMessageException.create("ZX99");
				}
				else {
					schedule.setFXPERMTDATE("");
					schedule.setFXFDATE(params.get("CMTRDATE"));
					schedule.setFXTDATE(params.get("CMTRDATE"));				
				}						
			}
	
			String sha1_Mac = "";
			String reqinfo_Str = "{}";
			
			params.put("ADREQTYPE", "S");   //寫入 TXNLOG 時, S 表示為預約
			
	
			schedule.setFXUSERID(params.get("UID"));
			schedule.setFXWDAC(params.get("ACN"));  //轉出帳號
			schedule.setFXSVBH("");  //轉入分行
			schedule.setFXSVAC(params.get("FDPNUM"));  //轉入帳號		
			schedule.setFXWDCURR(params.get("CUID"));  //轉出幣別
			schedule.setFXSVCURR(params.get("CUID"));  //轉入幣別		
			schedule.setFXWDAMT(params.get("AMT")); // 轉出金額
			schedule.setFXSVAMT("");  // 轉入金額
			
			schedule.setFXTXMEMO(params.get("CMTRMEMO"));
			schedule.setFXTXMAILS(params.get("CMTRMAIL"));
			schedule.setFXTXMAILMEMO(params.get("CMMAILMEMO"));  //CMMAILMEMO 對應的欄位
	
			Date d = new Date();
			schedule.setFXSDATE(DateTimeUtils.format("yyyyMMdd", d));
			schedule.setFXSTIME(DateTimeUtils.format("HHmmss", d));
			
//			schedule.setFXTXSTATUS("0");
			schedule.setLASTDATE(schedule.getFXSDATE());
			schedule.setLASTTIME(schedule.getFXSTIME());
			
			if(params.containsKey("pkcs7Sign") && "1".equals(params.get("FGTXWAY"))) {//IKEY
				List<String> commands = new ArrayList();
				commands.add("XMLCA()");
				commands.add("XMLCN()");
				
				CommandUtils.doBefore(commands, params);
				
				if(!StrUtils.trim(params.get("XMLCN")).toUpperCase().startsWith(StrUtils.trim(params.get("UID")).toUpperCase())) {
					throw TopMessageException.create("Z089");
				}
	
				schedule.setXMLCA(StrUtils.trim(params.get("XMLCA")));
				schedule.setXMLCN(StrUtils.trim(params.get("XMLCN")));
			}else if("7".equals(params.get("FGTXWAY"))){
				String certACN = "";
				while (certACN.length() < 19)
					certACN += "D";
				params.put("CERTACN", certACN);
				
				//IDGATE驗證-----start
				DoIDGateCommand_In in = new DoIDGateCommand_In();
				in.setIN_DATAS(params);
				HashMap rtnMap = commonIDGATEController.doIDGateCommand(in);
				if (!(boolean) rtnMap.get("result")) {
					log.error("N175 BOOKING CHECK IDGateCommand() ERROR");
					switch ((String) rtnMap.get("errMsg")) {
					case "Z300":
						throw TopMessageException.create("Z300");
					case "FE0011":
						throw TopMessageException.create("FE0011");
					default:
						throw TopMessageException.create("ZX99");
					}
				}
				//IDGATE驗證-----end
			}	
		
			schedule.setADOPID(params.get("ADOPID"));
			schedule.setFXTXCODE(params.get("FGTXWAY"));
			// 微服務新增欄位
			schedule.setFXTXTYPE("S");								// 單次轉帳
			schedule.setLOGINTYPE(params.get("LOGINTYPE"));							// NB登入或MB登入
			schedule.setMSADDR(TxnTwSchPayDao.MS_FX);//所屬微服務
			// 因IKEY資料太長 先移除避免塞爆欄位
			params.remove("pkcs7Sign");
			params.remove("__SIGN");
			// 預約時上行電文必要資訊
			log.debug(ESAPIUtil.vaildLog("txntwschpay.params>>>{}"+CodeUtil.toJson(params)));
			reqinfo_Str = CodeUtil.toJson(params);
			schedule.setFXTXINFO(reqinfo_Str);
			sha1_Mac = EncryptUtil.toEncS1(reqinfo_Str, "UTF-8").substring(0, 4);
			//
			// 微服務修改欄位
			schedule.setFXTXSTATUS("0");							// 預約狀態(0預約)
			log.debug(ESAPIUtil.vaildLog("TxnFxSchPayDao.save 參數 >> {}"+ CodeUtil.toJson(schedule)));		
			TxnFxSchPayDao.save(schedule);
			
			String reqinfo_addTXTime ="";
			String sha1_MacTime ="" ;
			
//			List<TXNFXSCHPAYDATA> dataList = txnFxSchPayDataDao.findByFXSCHNO(dptmpschno);
//	        for(TXNFXSCHPAYDATA data:dataList) {
//	        	params.put("FXSCHTXDATE", data.getPks().getFXSCHTXDATE());
//	        	reqinfo_addTXTime =  CodeUtil.toJson(params);
//	        	sha1_MacTime = EncryptUtil.toEncS1(reqinfo_addTXTime, null).substring(0,4);
//	        	data.setMAC(sha1_MacTime);
//	        	txnFxSchPayDataDao.update(data);
//	        }
			
			
			/**
			 * 舊bean邏輯
			 * 寫入成功，回傳空MVHImpl
			 * 寫入出Exception不處理，當成寫入失敗
			 * 微服務加入成功失敗訊息讓呼叫端方便判斷
			 */
			// TODO save後再select一次確定是否寫入成功 
			
			// 寫入成功
			result.getFlatValues().put("TOPMSG", "0");
			result.getFlatValues().put("msgName", "N175外匯綜存解約，預約檔寫入成功");
		}
		catch (Exception e)
		{
			log.error("N175外匯綜存解約，預約檔寫入資料庫失敗", e);
			// 寫入失敗
			// TODO 等景森休假回來討論寫入錯誤，要放什麼訊息，目前先塞ZX99
			result.getFlatValues().put("TOPMSG", "ZX99");
			throw TopMessageException.create("ZX99");
		}
		return result;
	}
	
	
	/**
	 * 預約交易 目前不使用 TXNFXSCHEDULE，所以註解
	 * @param params
	 * @return
	 */
//	@Override
//	public MVHImpl booking(Map<String, String> params) {
//		 	
//		TXNFXSCHEDULE schedule = new TXNFXSCHEDULE();
//		
//		log.info("***************************************預約編號***************************************************");
//		//更新SCHCOUNT(每日預約計數)
//		int scnt = txnUserDao.incSchcount((String)params.get("UID"));
//		
//		//預約編號(YYMMDD+三位流水號)
//		String dptmpschno = "";
//		String ymd = DateTimeUtils.format("yyMMdd", new Date());
//		dptmpschno = ymd + StrUtils.right("000" + scnt, 3); 
//		
//		//賽入預約編號
//		schedule.setFXSCHNO(dptmpschno);
//		log.info("***************************************預約編號***************************************************");
//		
//		schedule.setADOPID(DateTimeUtils.format("yyyyMMddHHmmss" , new Date()) + params.get("UID") );
//		String f = params.get("FGTRDATE");  //判斷預約周期
//		if("1".equals(f)) {  //預約某一天
//			params.put("CMTRDATE", StrUtils.trim(params.get("CMTRDATE")).replaceAll("/", ""));
//			if(params.get("CMTRDATE").length()==0)
//			{
//				throw TopMessageException.create("ZX99");
//			}
//			else {
//				schedule.setFXPERMTDATE("");
//				schedule.setFXFDATE(params.get("CMTRDATE"));
//				schedule.setFXTDATE(params.get("CMTRDATE"));				
//			}						
//		}
//
//		params.put("ADREQTYPE", "S");   //寫入 TXNLOG 時, S 表示為預約
//
//		schedule.setFXUSERID(params.get("UID"));
//		schedule.setFXWDAC(params.get("ACN"));  //轉出帳號
//		schedule.setFXSVBH("");  //轉入分行
//		schedule.setFXSVAC(params.get("FDPNUM"));  //轉入帳號		
//		schedule.setFXWDCURR(params.get("CUID"));  //轉出幣別
//		schedule.setFXSVCURR(params.get("CUID"));  //轉入幣別		
//		schedule.setFXWDAMT(params.get("AMT")); // 轉出金額
//		schedule.setFXSVAMT("");  // 轉入金額
//		
//		schedule.setFXTXMEMO(params.get("CMTRMEMO"));
//		schedule.setFXTXMAILS(params.get("CMTRMAIL"));
//		schedule.setFXTXMAILMEMO(params.get("CMMAILMEMO"));  //CMMAILMEMO 對應的欄位
//
//		Date d = new Date();
//		schedule.setFXSDATE(DateTimeUtils.format("yyyyMMdd", d));
//		schedule.setFXSTIME(DateTimeUtils.format("HHmmss", d));
//		
//		schedule.setFXTXSTATUS("0");
//		schedule.setLASTDATE(schedule.getFXSDATE());
//		schedule.setLASTTIME(schedule.getFXSTIME());
//		
//		if(params.containsKey("pkcs7Sign") && !"0".equals(params.get("FGTXWAY"))) {
//			List<String> commands = new ArrayList();
//			commands.add("XMLCA()");
//			commands.add("XMLCN()");
//			
//			CommandUtils.doBefore(commands, params);
//			
//			if(!StrUtils.trim(params.get("XMLCN")).toUpperCase().startsWith(StrUtils.trim(params.get("UID")).toUpperCase())) {
//				throw TopMessageException.create("Z089");
//			}
//
//			schedule.setXMLCA(StrUtils.trim(params.get("XMLCA")));
//			schedule.setXMLCN(StrUtils.trim(params.get("XMLCN")));
//		}
//		
//		TXNREQINFO titainfo = new TXNREQINFO();
//		titainfo.setREQINFO(BookingUtils.requestInfo(params));
//		txnReqInfoDao.save(titainfo);
//		
//		schedule.setFXTXINFO(titainfo.getREQID());
//		
//		String sha1 = DigestUtils.sha1(titainfo.getREQINFO()).substring(0, 4);
//		schedule.setMAC(sha1);
//		
//		
//		schedule.setADOPID(params.get("ADOPID"));
//
//		schedule.setFXTXCODE(params.get("FGTXWAY"));
//		
//		txnFxScheduleDao.save(schedule);
//		
//		return new MVHImpl();
//	}		
}
