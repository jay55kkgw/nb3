package fstop.services.impl;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.netbank.rest.comm.bean.DoIDGateCommand_In;
import com.netbank.rest.comm.controller.CommonIDGATEController;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import encryptUtil.EncryptUtil;
import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.TxnFxSchPayDao;
import fstop.orm.dao.TxnFxSchPayDataDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.TXNFXSCHPAY;
import fstop.services.BookingAware;
import fstop.services.CommonService;
import fstop.services.ServiceBindingException;
import fstop.services.TransferNotice;
import fstop.telcomm.TelCommExec;
import fstop.telcomm.cmd.CommandUtils;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 外匯	外匯結購售及轉帳-預約
 * @author Owner
 *
 */
@Slf4j
public class F001S extends CommonService implements BookingAware {
//	private Logger logger = Logger.getLogger(getClass());
	
//	private TelCommExec f001Telcomm;
//	private TelCommExec f003Telcomm;
//	private TelCommExec f005Telcomm;
//	private TelCommExec f007Telcomm;
//	private TelCommExec f011Telcomm;
//	private TelCommExec f021Telcomm;

	@Autowired
	private TxnFxSchPayDao txnFxSchPayDao;
	
	@Autowired
	private TxnFxSchPayDataDao txnFxSchPayDataDao;
	
//	@Autowired
//	private TxnFxScheduleDao txnFxScheduleDao;

//	@Autowired
//	private TxnReqInfoDao txnReqInfoDao;

	@Autowired
	private TxnUserDao txnUserDao;
	
	@Autowired
	CommonIDGATEController commonIDGATEController;
	
//	@Required
//	public void setTxnUserDao(TxnUserDao txnUserDao) {
//		this.txnUserDao = txnUserDao;
//	}
//	
//	@Required
//	public void setTxnReqInfoDao(TxnReqInfoDao txnReqInfoDao) {
//		this.txnReqInfoDao = txnReqInfoDao;
//	}
//
//	@Required
//	public void setF001Telcomm(TelCommExec telcomm) {
//		f001Telcomm = telcomm;
//	}
//
//	@Required
//	public void setF003Telcomm(TelCommExec telcomm) {
//		f003Telcomm = telcomm;
//	}
//
//	@Required
//	public void setF005Telcomm(TelCommExec telcomm) {
//		f005Telcomm = telcomm;
//	}
//
//	@Required
//	public void setF007Telcomm(TelCommExec telcomm) {
//		f007Telcomm = telcomm;
//	}
//
//	@Required
//	public void setF011Telcomm(TelCommExec telcomm) {
//		f011Telcomm = telcomm;
//	}
//
//	@Required
//	public void setF021Telcomm(TelCommExec telcomm) {
//		f021Telcomm = telcomm;
//	}
//
//	@Required
//	public void setTxnFxRecordDao(TxnFxRecordDao txnFxRecordDao) {
//		this.txnFxRecordDao = txnFxRecordDao;
//	}
//
//	@Required
//	public void setTxnFxScheduleDao(TxnFxScheduleDao txnFxScheduleDao) {
//		this.txnFxScheduleDao = txnFxScheduleDao;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params) { 
		
		/*
		 	在批次跑預約時, F007 的電文裡, FLAG 為 A
		 	如果電文失敗或寫DB發生任何錯誤, 則發 F007的電文
		 	但是  FLAG 內容為 C 的電文
		 */
	
		
		
		//TODO 在營業時間 09:30~15:30 外, 只能執行預約交易
		Map<String, String> params = _params;
		
		String fginacno = params.get("FGINACNO"); // 1 約定帳號, 2 非約定帳號
		
		//轉入帳號
		String trin_bnkcod = "";
		String trin_acn = "";
		String trin_agree = "";
		if("CMDAGREE".equals(fginacno)) {
			Map<String, String> jm = JSONUtils.json2map(params.get("INACNO1"));
			trin_bnkcod = jm.get("BNKCOD");
			trin_acn = jm.get("ACN");
			trin_agree = jm.get("AGREE");
		}
		else if("CMDISAGREE".equals(fginacno)) {  //非約定, 只能是台企銀 050
			trin_bnkcod = "050";
			trin_acn = params.get("INACNO2");
			trin_agree = "0";   // 0: 非約定, 1: 約定
		}

		params.put("__TRIN_BANKCOD", trin_bnkcod);
		params.put("__TRIN_ACN", trin_acn);
		params.put("__TRIN_AGREE", trin_agree);
		
/*		
		//BHO 轉入帳號檢核
		String str_UserInputAcno = (String)params.get("InAcno_1") + (String)params.get("InAcno_2") + (String)params.get("InAcno_3");
		String str_RandomValueAcno = (String)params.get("BHO_INACNO_VERIFY");		
		BHOCheckUtils.checkBHOInAcno(str_UserInputAcno, str_RandomValueAcno);		
*/		
		
		String f = params.get("FGTRDATE");
		if("1".equals(f) || "2".equals(f)) {  //		預約
			
			if("0".equals(params.get("FGTXWAY"))) {  //使用交易密碼, 做預約
//				String TransPassUpdate = (String)params.get("TRANSPASSUPDATE");
//				String Phase2 = (String)params.get("PHASE2");
				TelCommExec n950CMTelcomm;
				
//				if(StrUtils.isNotEmpty(TransPassUpdate))
//				{
//					if(TransPassUpdate.equals("1") || Phase2.equals("ON"))
//					{
						n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n951CMTelcomm");
//					}
//					else
//					{
//						n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n950CMTelcomm");
//					}
//				}
//				else
//				{
//					n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n950CMTelcomm");
//				}
				// N950會發兩次所以需FGTXWAY=""，N950回應後再把FGTXWAY改成原值即可
//				String fgtxway=params.get("FGTXWAY");
//				params.put("FGTXWAY", "");
				MVHImpl n950Result = n950CMTelcomm.query(params);
				//若驗證失敗, 則會發動 TopMessageException
//				params.put("FGTXWAY", fgtxway);
			} else if ("7".equals(params.get("FGTXWAY"))) {
				// 驗證裝置交易認證
				DoIDGateCommand_In in = new DoIDGateCommand_In();
				in.setIN_DATAS(params);
				HashMap rtnMap = commonIDGATEController.doIDGateCommand(in);
				if (!(boolean) rtnMap.get("result")) {
					log.error("F001S BOOKING CHECK IDGateCommand() ERROR");
					switch ((String) rtnMap.get("errMsg")) {
					case "Z300":
						throw TopMessageException.create("Z300");
					case "FE0011":
						throw TopMessageException.create("FE0011");
					default:
						throw TopMessageException.create("ZX99");
					}
				}

			}
			
			return booking(params);
		}
		else {
			throw new ServiceBindingException("不為預約交易");
		}

	}

	
	@Override
	public MVHImpl booking(Map<String, String> params) {
		MVHImpl mvhImpl = new MVHImpl();
		
		try {
			log.trace(ESAPIUtil.vaildLog("F001S.booking.params: {}"+CodeUtil.toJson(params)));
			
			DateTimeUtils.removeSlash((HashMap)params, new String[]{"CMSDATE", "CMEDATE", "CMTRDATE"});
			
			DecimalFormat dft = new DecimalFormat("#0");
			DecimalFormat dftdot2 = new DecimalFormat("#0.00");
			
			TXNFXSCHPAY schedule = new TXNFXSCHPAY();
			
			String sha1_Mac = "";
			String reqinfo_Str = "{}";
			
			// 取得預約編號(YYMMDD+三位流水號)
			String dptmpschno = txnUserDao.getDptmpschno(params.get("CUSIDN"));
			schedule.setFXSCHNO(dptmpschno);
			
			schedule.setADOPID(params.get("ADOPID"));
			
			String f = params.get("FGTRDATE");  //判斷 預約 周期
			// 特定某日
			if("1".equals(f)) {
				params.put("CMTRDATE", StrUtils.trim(params.get("CMTRDATE")).replaceAll("/", ""));
	            if(params.get("CMTRDATE").length()==0) {
					throw TopMessageException.create("ZX99");
	            }
	            else {
	            	schedule.setFXTXTYPE("S");
	    			schedule.setFXPERMTDATE("");
	    			schedule.setFXFDATE(params.get("CMTRDATE"));
	    			schedule.setFXTDATE(params.get("CMTRDATE"));            	
	            }
			} // 固定每月的某日
			else if ("2".equals(f)){
				params.put("CMSDATE", StrUtils.trim(params.get("CMSDATE")).replaceAll("/", ""));
				params.put("CMEDATE", StrUtils.trim(params.get("CMEDATE")).replaceAll("/", ""));
				if(params.get("CMPERIOD").length()==0 || params.get("CMSDATE").length()==0 || params.get("CMEDATE").length()==0) {
					throw TopMessageException.create("ZX99");				
				}
				else {
					schedule.setFXTXTYPE("C");
					schedule.setFXPERMTDATE(params.get("CMPERIOD"));
					schedule.setFXFDATE(params.get("CMSDATE"));
					schedule.setFXTDATE(params.get("CMEDATE"));				
				}
			}
			
			schedule.setFXUSERID(params.get("UID"));
			schedule.setFXWDAC(params.get("CUSTACC"));  //轉出帳號
			schedule.setFXSVBH(params.get("__TRIN_BANKCOD"));  //轉入分行
			schedule.setFXSVAC(params.get("__TRIN_ACN"));  //轉入帳號
			schedule.setFXWDCURR(params.get("PAYCCY"));  //轉出幣別
			schedule.setFXSVCURR(params.get("REMITCY"));  //轉入幣別
			
			// 轉出/轉入
			if ("1".equals(params.get("PAYREMIT"))) { // 轉出
				schedule.setFXWDAMT(dftdot2.format(dft.parse(params.get("CURAMT")).doubleValue())); // 轉出金額
				schedule.setFXSVAMT(""); // 轉入金額

			} else { // "2".equals(params.get("PAYREMIT")) //轉入
				schedule.setFXWDAMT(""); // 轉出金額
				schedule.setFXSVAMT(dftdot2.format(dft.parse(params.get("CURAMT")).doubleValue())); // 轉入金額

			}
			
			schedule.setFXTXMEMO(params.get("CMTRMEMO"));    //收款人附言
			schedule.setFXTXMAILS(params.get("CMTRMAIL"));
			schedule.setFXTXMAILMEMO(params.get("CMMAILMEMO"));  //CMMAILMEMO 對應的欄位
			
			schedule.setFXTXCODE(params.get("FGTXWAY"));
			
			Date d = new Date();
			schedule.setFXSDATE(DateTimeUtils.format("yyyyMMdd", d));
			schedule.setFXSTIME(DateTimeUtils.format("HHmmss", d));

			schedule.setFXTXSTATUS("0");
			schedule.setLASTDATE(schedule.getFXSDATE());
			schedule.setLASTTIME(schedule.getFXSTIME());

			if(params.containsKey("pkcs7Sign") && "1".equals(params.get("FGTXWAY"))) {
				List<String> commands = new ArrayList();
				commands.add("XMLCA()");
				commands.add("XMLCN()");
				
				CommandUtils.doBefore(commands, params);
				
				if(!StrUtils.trim(params.get("XMLCN")).toUpperCase().startsWith(StrUtils.trim(params.get("UID")).toUpperCase())) {
					throw TopMessageException.create("Z089");
				}
				
				schedule.setXMLCA(StrUtils.trim(params.get("XMLCA")));
				schedule.setXMLCN(StrUtils.trim(params.get("XMLCN")));
			}
			
			// TODO DPTXINFO放不下這兩個欄位，先拿掉，若預約交易執行時需要再看怎麼存
			params.remove("pkcs7Sign");
			params.remove("jsondc");
			reqinfo_Str =  CodeUtil.toJson(params);
			sha1_Mac = EncryptUtil.toEncS1(reqinfo_Str, "UTF-8").substring(0,4);
			schedule.setMAC(sha1_Mac);
			schedule.setFXTXINFO(reqinfo_Str);
			schedule.setLOGINTYPE(params.get("LOGINTYPE"));// NB登入或MB登入
			schedule.setMSADDR(TxnFxSchPayDao.MS_FX);
			
			log.trace(ESAPIUtil.vaildLog("F001S.booking.schedule: {}"+CodeUtil.toJson(schedule)));
			txnFxSchPayDao.save(schedule);
			
			String reqinfo_addTXTime ="";
			String sha1_MacTime ="" ;
			
//			List<TXNFXSCHPAYDATA> dataList = txnFxSchPayDataDao.findByFXSCHNO(dptmpschno);
//	        for(TXNFXSCHPAYDATA data:dataList) {
//	        	params.put("FXSCHTXDATE", data.getPks().getFXSCHTXDATE());
//	        	reqinfo_addTXTime =  CodeUtil.toJson(params);
//	        	sha1_MacTime = EncryptUtil.toEncS1(reqinfo_addTXTime, null).substring(0,4);
//	        	data.setMAC(sha1_MacTime);
//	        	txnFxSchPayDataDao.update(data);
//	        }
			
			
			mvhImpl.getFlatValues().put("TOPMSG", "0000"); // 預約成功
		
		} catch (TopMessageException e) {
			log.error("F001S.booking.TopMessageException: {}", e);
			mvhImpl.getFlatValues().put("TOPMSG", e.getMsgcode());
			
		} catch (Exception e) {
			log.error("F001S.booking.Exception: {}",e);
			mvhImpl.getFlatValues().put("TOPMSG", "FE0003"); // 預約失敗
		}
		
		return mvhImpl;
	}
	
	
/**
 *  booking_old
 *  
	@Override
	public MVHImpl booking(Map<String, String> params) {
 
		DateTimeUtils.removeSlash((Hashtable)params, new String[]{"CMSDATE", "CMEDATE", "CMTRDATE"});
		
		DecimalFormat dft = new DecimalFormat("#0");
		DecimalFormat dftdot2 = new DecimalFormat("#0.00");
		
		TXNFXSCHEDULE schedule = new TXNFXSCHEDULE();
		
		log.info("*************************************預約編號*****************************************************");
		//更新SCHCOUNT(每日預約計數)
		int scnt = txnUserDao.incSchcount((String)params.get("UID"));
		
		//預約編號(YYMMDD+三位流水號)
		String dptmpschno = "";
		String ymd = DateTimeUtils.format("yyMMdd", new Date());
		dptmpschno = ymd + StrUtils.right("000" + scnt, 3); 
		
		//賽入預約編號
		schedule.setFXSCHNO(dptmpschno);
		log.info("***************************************預約編號***************************************************");
		
		schedule.setADOPID(DateTimeUtils.format("yyyyMMddHHmmss", new Date()) + params.get("UID"));
		String f = params.get("FGTRDATE");  //判斷 預約 周期
		if("1".equals(f)) {
			params.put("CMTRDATE", StrUtils.trim(params.get("CMTRDATE")).replaceAll("/", ""));
            if(params.get("CMTRDATE").length()==0) {
				throw TopMessageException.create("ZX99");
            }
            else {
    			schedule.setFXPERMTDATE("");
    			schedule.setFXFDATE(params.get("CMTRDATE"));
    			schedule.setFXTDATE(params.get("CMTRDATE"));            	
            }
		}
		else {
			params.put("CMSDATE", StrUtils.trim(params.get("CMSDATE")).replaceAll("/", ""));
			params.put("CMEDATE", StrUtils.trim(params.get("CMEDATE")).replaceAll("/", ""));
			if(params.get("CMPERIOD").length()==0 || params.get("CMSDATE").length()==0 || params.get("CMEDATE").length()==0) {
				throw TopMessageException.create("ZX99");				
			}
			else {
				schedule.setFXPERMTDATE(params.get("CMPERIOD"));
				schedule.setFXFDATE(params.get("CMSDATE"));
				schedule.setFXTDATE(params.get("CMEDATE"));				
			}
		}
		
		params.put("ADREQTYPE", "S");   //寫入 TXNLOG 時, S 表示為預約

		schedule.setFXUSERID(params.get("UID"));
		schedule.setFXWDAC(params.get("CUSTACC"));  //轉出帳號
		schedule.setFXSVBH(params.get("__TRIN_BANKCOD"));  //轉入分行
		schedule.setFXSVAC(params.get("__TRIN_ACN"));  //轉入帳號
		schedule.setFXWDCURR(params.get("PAYCCY"));  //轉出幣別
		schedule.setFXSVCURR(params.get("REMITCY"));  //轉入幣別
		
		
		try {
			if ("1".equals(params.get("PAYREMIT"))) { // 轉出
				//double dd = new Double(params.get("CURAMT")) * new Double(params.get("RATE"));
				
				schedule.setFXWDAMT(dftdot2.format(dft.parse(params.get("CURAMT")).doubleValue())); // 轉出金額
				schedule.setFXSVAMT( ""); // 轉入金額

			} else { // "2".equals(params.get("PAYREMIT")) //轉入
				//double dd = new Double(params.get("ATRAMT")) / new Double(params.get("RATE"));
				
				schedule.setFXWDAMT( ""); // 轉出金額
				schedule.setFXSVAMT(dftdot2.format(dft.parse(params.get("CURAMT")).doubleValue())); // 轉入金額

			}
		}	
		catch(Exception e) {}
		
		
//		if ("1".equals(params.get("PAYREMIT"))) { // 轉出
//			//double dd = new Double(params.get("CURAMT")) * new Double(params.get("RATE"));
//			schedule.setFXWDAMT(params.get("CURAMT")); // 轉出金額
//			schedule.setFXSVAMT( ""); // 轉入金額
//
//		} else { // "2".equals(params.get("PAYREMIT")) //轉入
//
//			//double dd = new Double(params.get("ATRAMT")) / new Double(params.get("RATE"));
//			schedule.setFXWDAMT(""); //轉出金額
//			schedule.setFXSVAMT( params.get("CURAMT")); // 轉入金額
//		}

		schedule.setFXTXMEMO(params.get("CMTRMEMO"));    //收款人附言
		schedule.setFXTXMAILS(params.get("CMTRMAIL"));
		schedule.setFXTXMAILMEMO(params.get("CMMAILMEMO"));  //CMMAILMEMO 對應的欄位
		
		schedule.setFXTXCODE(params.get("FGTXWAY"));
		
		Date d = new Date();
		schedule.setFXSDATE(DateTimeUtils.format("yyyyMMdd", d));
		schedule.setFXSTIME(DateTimeUtils.format("HHmmss", d));

		schedule.setFXTXSTATUS("0");
		schedule.setLASTDATE(schedule.getFXSDATE());
		schedule.setLASTTIME(schedule.getFXSTIME());

		if(params.containsKey("pkcs7Sign") && !"0".equals(params.get("FGTXWAY"))) {
			List<String> commands = new ArrayList();
			commands.add("XMLCA()");
			commands.add("XMLCN()");
			
			CommandUtils.doBefore(commands, params);
			
			if(!StrUtils.trim(params.get("XMLCN")).toUpperCase().startsWith(StrUtils.trim(params.get("UID")).toUpperCase())) {
				throw TopMessageException.create("Z089");
			}
			
			schedule.setXMLCA(StrUtils.trim(params.get("XMLCA")));
			schedule.setXMLCN(StrUtils.trim(params.get("XMLCN")));
		}
		
		TXNREQINFO titainfo = new TXNREQINFO();
		titainfo.setREQINFO(BookingUtils.requestInfo(params));
		txnReqInfoDao.save(titainfo);
		
		schedule.setFXTXINFO(titainfo.getREQID());
		
		String sha1 = DigestUtils.sha1(titainfo.getREQINFO()).substring(0, 4);
		schedule.setMAC(sha1);
		
		schedule.setADOPID(params.get("ADOPID"));
		
		txnFxScheduleDao.save(schedule);
		
		return new MVHImpl();
	}
	
 */
	
	
	@Override
	public MVHImpl online(Map<String, String> params, TransferNotice trNotice) {
		return new MVHImpl();
	}
	
}
