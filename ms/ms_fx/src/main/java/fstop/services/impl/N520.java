package fstop.services.impl;

import fstop.exception.TopMessageException;
import fstop.model.*;
import fstop.services.DispatchService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;

@Slf4j
public class N520 extends DispatchService  {

	@Autowired
    @Qualifier("n523Telcomm")
	private TelCommExec n523Telcomm;
//	private TelCommExec n263Telcomm;

	@Autowired
    @Qualifier("n524Telcomm")
	private TelCommExec n524Telcomm;
//	private TelCommExec n264Telcomm;
	
//	@Required
//	public void setN263Telcomm(TelCommExec telcomm) {
//		n263Telcomm = telcomm;
//	}
//
//	@Required
//	public void setN264Telcomm(TelCommExec telcomm) {
//		n264Telcomm = telcomm;
//	}
//
//	@Required
//	public void setN523Telcomm(TelCommExec telcomm) {
//		n523Telcomm = telcomm;
//	}
//
//	@Required
//	public void setN524Telcomm(TelCommExec telcomm) {
//		n524Telcomm = telcomm;
//	}

	@Override
	public String getActionMethod(Map<String, String> params) {
		/**
		 * 若為空白, 則使用 N524 電文查詢全部帳號
		 * 有帳號, 則使用 N523 來查
		 */
		return (StrUtils.isEmpty(params.get("ACN")) ? "doN524Telcomm" : "doN523Telcomm");
	} 
	
	/**
	 * 查詢指定帳號
	 * @param params
	 * @return
	 */
	public IMasterValuesHelper doN523Telcomm(Map params) {
		params.put("TXID", "N523");
		return getMVH(params, new TelCommExec[]{n523Telcomm});
	}
	/**
	 * 查詢全部帳號
	 * @param params
	 * @return
	 */
	public IMasterValuesHelper doN524Telcomm(Map params) {
		params.put("TXID", "N524");
		return getMVH(params, new TelCommExec[]{n524Telcomm});
	}
	
	/**
	 * 僅有電文不同, 其他的動作皆相同, 在設定 actionTelcomm 之後, 再執行 doAction
	 * @param params
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public MVH getMVH(Map params, TelCommExec[] actionTelcomm) {
		
		String period = (String)params.get("FGPERIOD");
		String enddate = "";
		String stadate = "";
		String periodStr = "";
		if("CMTODAY".equals(period)) { //今日
			stadate = DateTimeUtils.getCDateShort(new Date());
			enddate = stadate;
			periodStr = DateTimeUtils.format("yyyy/MM/dd", new Date());
		} else if("CMCURMON".equals(period)) { //本月
			Calendar cal = Calendar.getInstance();
			Date dend = cal.getTime();

			cal.set(Calendar.DATE, 1);
			Date dstart = cal.getTime();
			stadate = DateTimeUtils.getCDateShort(dstart);
			enddate = DateTimeUtils.getCDateShort(dend);
			periodStr = DateTimeUtils.format("yyyy/MM/dd", dstart) + "~" + DateTimeUtils.format("yyyy/MM/dd", dend);
		} else if("CMLASTMON".equals(period)) { //上月
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.DATE, 1);
			cal.add(Calendar.MONTH, -1);
			Date dstart = cal.getTime();
			cal.add(Calendar.MONTH, 1);
			cal.add(Calendar.DATE, -1);
			Date dend = cal.getTime();
			stadate = DateTimeUtils.getCDateShort(dstart);
			enddate = DateTimeUtils.getCDateShort(dend);
			periodStr = DateTimeUtils.format("yyyy/MM/dd", dstart) + "~" + DateTimeUtils.format("yyyy/MM/dd", dend);
		} else if("CMNEARMON".equals(period)) { //最近一個月
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.MONTH, -1);
			Date dstart = cal.getTime();
			Date dend = new Date();
			stadate = DateTimeUtils.getCDateShort(dstart);
			enddate = DateTimeUtils.getCDateShort(dend);
			periodStr = DateTimeUtils.format("yyyy/MM/dd", dstart) + "~" + DateTimeUtils.format("yyyy/MM/dd", dend);

		} else if("CMLASTWEEK".equals(period)) { //最近一星期
			Calendar cal = Calendar.getInstance();
			Date dend = cal.getTime();
		
			cal.add(Calendar.DAY_OF_YEAR, -7);
			Date dstart = cal.getTime();
						
			stadate = DateTimeUtils.getCDateShort(dstart);
			enddate = DateTimeUtils.getCDateShort(dend);
			
			periodStr = DateTimeUtils.format("yyyy/MM/dd", dstart) + "~" + DateTimeUtils.format("yyyy/MM/dd", dend);
		} else if("CMPERIOD".equals(period)) { //指定日期
			stadate = (String)params.get("CMSDATE");
			enddate = (String)params.get("CMEDATE");
			stadate = StrUtils.trim(stadate).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
			enddate = StrUtils.trim(enddate).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
			
			if(stadate != null && stadate.length() == 8) {
				Date sd = DateTimeUtils.parse("yyyyMMdd", stadate);
				stadate = DateTimeUtils.getCDateShort(sd);
				periodStr = DateTimeUtils.format("yyyy/MM/dd", sd);
			}
			if(enddate != null && enddate.length() == 8) {
				Date sd = DateTimeUtils.parse("yyyyMMdd", enddate);
				enddate = DateTimeUtils.getCDateShort(sd);
				periodStr += "~" + DateTimeUtils.format("yyyy/MM/dd", sd);
			}
			else { //若未指定結束時間, 則使用今天日期
				enddate = DateTimeUtils.getCDateShort(new Date());
				periodStr += "~" + DateTimeUtils.format("yyyy/MM/dd", new Date());
			}
		}
		params.put("STADATE", stadate);
		params.put("ENDDATE", enddate);
		//DumpUtils.dump("N520 參詢參數", params);

		if ("TRUE".equals(params.get("OKOVNEXT"))) {
			params.put("QUERYNEXT", "");
		}		
		
		MVHImpl resultTW = null;
		try {
			resultTW = actionTelcomm[0].query(params);
		}
		catch(TopMessageException e) {
			//if(!"ENRD".equalsIgnoreCase(e.getMsgcode()))
				//throw e;	
		}
/*		
		MVHImpl resultHK = null;
		try {
			if (resultTW != null)
				params.put("CHGCOD", resultTW.getValueByFieldName("CHGCOD"));
			
			resultHK = actionTelcomm[1].query(params);
		}
		catch(TopMessageException e) {
			//if(!"ENRD".equalsIgnoreCase(e.getMsgcode()))
				//throw e;
		}
*/		
		if (resultTW == null /*&& resultHK == null*/)
			throw TopMessageException.create("ENRD");
		
		MVHImpl result = new MVHImpl();
		
		if(resultTW != null) {
			result.getOccurs().addRows(resultTW.getOccurs());
		}
/*		
		if(resultHK != null) {
			result.getOccurs().addRows(resultHK.getOccurs());
		}
*/		
		//20190320 Ian 為了得到回傳值
		result.getFlatValues().putAll(resultTW.getFlatValues());
		
		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		result.getFlatValues().put("CMPERIOD", periodStr);
		result.getFlatValues().put("CMRECNUM", result.getValueOccurs("ACN") + "");
		
		if (result.getValueOccurs("ACN") == 0)
			throw TopMessageException.create("ENRD");
		
		RowsSelecter rowSelecter = new RowsSelecter(result.getOccurs());
		rowSelecter.each(new EachRowCallback(){

			public void current(Row row) {
				
				if("1".equalsIgnoreCase(row.getValue("CORD"))) {
					row.setValue("FXPAYAMT", row.getValue("AMT"));
				}
				if("2".equalsIgnoreCase(row.getValue("CORD"))) {
					row.setValue("FXRECAMT", row.getValue("AMT"));
				}
				
			}});
		
		Map<String, IMasterValuesHelper> tables = MVHUtils.groupByToTables(result, new String[]{"ACN"});
		MVHUtils.addTables(result, tables);

		return result;
	}


}
