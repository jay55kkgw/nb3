package fstop.services.impl;

import java.util.Date;
import java.util.Hashtable;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import fstop.model.GrpColumns;
import fstop.model.MVH;
import fstop.model.MVHUtils;
import fstop.model.RowsGroup;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N557 extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());
	
	@Autowired
	@Qualifier("n557Telcomm")
	private TelCommExec n557Telcomm;

//	@Required
//	public void setN557Telcomm(TelCommExec telcomm) {
//		n557Telcomm = telcomm;
//	}

	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) {
		// 當日信用狀開狀查詢

		String enddate = "";
		String stadate = "";
		String periodStr = "";

		stadate = (String) params.get("FDATE");
		enddate = (String) params.get("TDATE");
		stadate = StrUtils.trim(stadate).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
		enddate = StrUtils.trim(enddate).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
		
		if (stadate != null && stadate.length() == 8) {
			Date sd = DateTimeUtils.parse("yyyyMMdd", stadate);
			stadate = DateTimeUtils.format("yyyy-MM-dd", sd);
			periodStr = DateTimeUtils.format("yyyy/MM/dd", sd);
		}
		if (enddate != null && enddate.length() == 8) {
			Date sd = DateTimeUtils.parse("yyyyMMdd", enddate);
			enddate = DateTimeUtils.format("yyyy-MM-dd", sd);
			periodStr += "~" + DateTimeUtils.format("yyyy/MM/dd", sd);
		} else { // 若未指定結束時間, 則使用今天日期
			enddate = DateTimeUtils.getDateShort(new Date());
			periodStr += "~" + DateTimeUtils.format("yyyy/MM/dd", new Date());
		}
		params.put("FDATE", stadate);
		params.put("TDATE", enddate);

		if ("TRUE".equals(params.get("OKOVNEXT"))) {
			params.put("QUERYNEXT", "");
		}		
		
		TelcommResult telcommResult = n557Telcomm.query(params);
		telcommResult.getFlatValues().put("CMQTIME",
				DateTimeUtils.getDatetime(new Date()));

		telcommResult.getFlatValues().put("CMRECNUM",
				telcommResult.getValueOccurs("RLGCCY") + "");

		telcommResult.getFlatValues().put("CMPERIOD", periodStr);
		//開狀金額
		try {
			String[] groupBy = new String[]{"RLGCCY"};
			RowsGroup grp = MVHUtils.group(telcommResult.getOccurs(), groupBy);
			Map<GrpColumns, Double> sum = grp.sum("RLGAMT");
			int ind = 1;
			for(Entry<GrpColumns, Double> ent : sum.entrySet()) {
				GrpColumns grpCols = ent.getKey();
	
				Map<String, String> m = MVHUtils.toFlatValues(grpCols, "AMT", ind);
				telcommResult.getFlatValues().putAll(m);
				telcommResult.getFlatValues().put("FXSUBAMT_" + ind, ent.getValue() + "");
				telcommResult.getFlatValues().put("FXSUBAMTRECNUM_" + ind, grp.getGroups().get(grpCols).getSelectedRows().size() + "");
				
				ind++;
			}
		}
		finally {}

		try {
			String[] groupBy = new String[]{"RLGCCY"};
			RowsGroup grp = MVHUtils.group(telcommResult.getOccurs(), groupBy);
			Map<GrpColumns, Double> sum = grp.sum("RLGOS");
			int ind = 1;
			for(Entry<GrpColumns, Double> ent : sum.entrySet()) {
				GrpColumns grpCols = ent.getKey();
	
				Map<String, String> m = MVHUtils.toFlatValues(grpCols, "BAL", ind);
				telcommResult.getFlatValues().putAll(m);
				telcommResult.getFlatValues().put("FXSUBBAL_" + ind, ent.getValue() + "");
				telcommResult.getFlatValues().put("FXSUBBALRECNUM_" + ind,  grp.getGroups().get(grpCols).getSelectedRows().size() + "");
				
				ind++;
			}
		}
		finally{}
 


		return telcommResult;
	}

}
