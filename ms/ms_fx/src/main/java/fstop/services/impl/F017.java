package fstop.services.impl;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.orm.dao.SysDailySeqDao;
import fstop.orm.dao.TxnFxRecordDao;
import fstop.orm.po.SYSPARAMDATA;
import fstop.orm.po.TXNFXRECORD;
import fstop.services.BOLifeMoniter;
import fstop.services.CommonService;
import fstop.services.TelCommExecWrapper;
import fstop.services.mgn.B105;
import fstop.telcomm.TelCommExec;
import fstop.telcomm.cmd.CommandUtils;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import fstop.va.VaCheck;
import lombok.extern.slf4j.Slf4j;
import topibs.utility.AmountUtil;
import topibs.utility.NumericUtil;

/**
 * 外幣結清銷戶
 * 
 * @author Owner
 *
 */
@Slf4j
public class F017 extends CommonService {

	@Autowired
	@Qualifier("f001Telcomm")
	private TelCommExec f001Telcomm;
	@Autowired
	@Qualifier("f017Telcomm")
	private TelCommExec f017Telcomm;
	@Autowired
	@Qualifier("f011Telcomm")
	private TelCommExec f011Telcomm;
	@Autowired
	private TxnFxRecordDao txnFxRecordDao;
	@Autowired
	private SysDailySeqDao sysDailySeqDao;
	@Autowired
	private B105 b105;

	@Override
	public MVH doAction(Map _params) {
		final Map<String, String> params = _params;

		MVHImpl result = new MVHImpl();
		String fgtxway = params.get("FGTXWAY").toString();
		// 判斷營業時間 ,若交易最後提交時間 > (營業截止時間 + 5分鐘) => 須顯示錯誤訊息(Z409)
//		B105 b105 = (B105)SpringBeanFactory.getBean("b105".toLowerCase());

		SYSPARAMDATA sysParam = (SYSPARAMDATA) b105.getSysParamData();

		int i_MM = Integer.parseInt(sysParam.getADFXEMM()) + 5;
		String str_BizEnd = sysParam.getADFXEHH() + String.valueOf(i_MM) + sysParam.getADFXESS();
		String str_CurrentTime = DateTimeUtils.getTimeShort(new Date());

		if (str_CurrentTime.compareTo(str_BizEnd) > 0) {
			throw TopMessageException.create("Z409", (String) params.get("ADOPID"));
		}
		if (params.containsKey("pkcs7Sign") && "1".equals(fgtxway)) {
			List<String> commands = new ArrayList();
			commands.add("XMLCA()");
			commands.add("XMLCN()");

			CommandUtils.doBefore(commands, params);

			if (!StrUtils.trim(params.get("XMLCN")).toUpperCase()
					.startsWith(StrUtils.trim(params.get("UID")).toUpperCase())) {
				throw TopMessageException.create("Z089");
			}
		}
		if (fgtxway.equals("1")) { // 電子簽章
			params.put("XMLCAYN", "Y");
		} else if (fgtxway.equals("2")) { // 晶片金融卡
			params.put("XMLCAYN", "N");
			params.put("XMLCA", "C");
			params.put("XMLCN", "CARD");
		} else if (fgtxway.equals("3")) { // OTP
			params.put("XMLCAYN", "N");
			params.put("XMLCA", "O");
			params.put("XMLCN", "OTP");
		}
		log.warn(ESAPIUtil.vaildLog("F017.java fgtxway:" + fgtxway + " XMLCA:" + params.get("XMLCA").toString()
				+ " XMLCN:" + params.get("XMLCN").toString()));
		Date d = new Date();
		params.put("CMTRDATE", DateTimeUtils.format("yyyyMMdd", d));
		params.put("CMTRTIME", DateTimeUtils.format("HHmmss", d));
		params.put("FYACN", params.get("FYACN") == null ? "" : params.get("FYACN")); // 轉出帳號
		params.put("INACN", params.get("TSFAN") == null ? "" : params.get("TSFAN")); // 轉入帳號
		params.put("OUT_CRY", params.get("CRY") == null ? "" : params.get("CRY")); // 轉出幣別
		params.put("IN_CRY", params.get("CURCODE") == null ? "" : params.get("CURCODE")); // 轉入幣別
		params.put("BGROENO", params.get("SFXGLSTNO") == null ? "" : params.get("SFXGLSTNO")); // 議價編號
		params.put("PCODE", "000C");
		params.put("PAYCCY", params.get("OUT_CRY")); // 轉出幣別
		params.put("REMITCY", params.get("IN_CRY")); // 轉入幣別
		String CURAMT = "";
		String ATRAMT = "";
		try {
			CURAMT = AmountUtil.deleteComma(
					NumericUtil.formatNumberString(params.get("AMTDHID") == null ? "0" : params.get("AMTDHID"), 2));
			ATRAMT = AmountUtil.deleteComma(
					NumericUtil.formatNumberString(params.get("ATRAMT") == null ? "0" : params.get("ATRAMT"), 2));
			log.warn(ESAPIUtil.vaildLog("F017.java CURAMT:" + CURAMT + " ATRAMT:" + ATRAMT));
			params.put("CURAMT", CURAMT);// 轉出金額 (議價金額)
			params.put("ATRAMT", ATRAMT);// 轉入金額
		} catch (Exception e) {
			log.warn("F017.java CURAMT deleteComma failure", e);
		}
		// TODO 先執行F001前置電文
		doF001(params);
		/*** New Add ***/
		if (params.get("STAN") == null) {
//			params.put("STAN", "N" + StrUtils.right("0000000" + sysDailySeqDao.dailySeq("F017"), 7) + "");
			params.put("STAN", sysDailySeqDao.getFXStan("F017T"));
			BOLifeMoniter.setValue("STAN", params.get("STAN"));
		}
		params.put("SEQNO", "00000");
		params.put("TXBRH", ""); // 承作行
		params.put("CUSTPAY_ID", params.get("UID")); // 付款人統一編號
		params.put("ACCTID", params.get("FYACN")); // F017 付款帳號
		params.put("ACCTTYPE", ""); // 付款人帳戶類別
		params.put("ACCCUR", params.get("OUT_CRY")); // 付款人轉出幣別
		params.put("ACCAMT", CURAMT); // 轉出金額
		params.put("CURCODE", params.get("IN_CRY")); // 付款人轉入幣別
		params.put("CURAMT1", ATRAMT); // 轉入金額
		params.put("BANK_TYPE", ""); // 銀行代號類別
		params.put("PRCDT", params.get("CMTRDATE")); // 付款日期

		//
		// 付款人外匯議價號碼,左靠右補空白
		// (13-20) 8 碼為 交易密碼 MAC KEY SYNC
		// (21-36) 16 碼為 USERID
		//
		// 同幣別不會有 "BGROENO", 所以帶空白
		params.put("REFID", StrUtils.left(StrUtils.trim(params.get("BGROENO")) + StrUtils.repeat(" ", 12), 12));
		params.put("SYNC", StrUtils.trim(params.get("SYNC"))); // 8 碼
		params.put("USERID", StrUtils.left(params.get("UID") + StrUtils.repeat(" ", 16), 16)); // 議價編號

		params.put("PAYEE_ID", StrUtils.trim(params.get("UID")));// 收款人統一編號

		// 收款人帳號 (11碼) + " " (9碼) + 收款人附言 (16 碼)
		params.put("ACCTIDTO", StrUtils.trim(params.get("TSFAN")) + StrUtils.repeat(" ", 9) + StrUtils.repeat(" ", 16));
		params.put("ACCTTP_TO", ""); // 收款人帳戶類別
		String str_First4Byte = "FISC";
		String str_Flag = " ";
		params.put("ACWTYPE", str_First4Byte + "            " + str_Flag); // 收款人帳戶類別
		params.put("BACH_MARK", "N"); // 整批註記
		params.put("TXPSW", ""); // 押密後交易密碼
		params.put("M_CUSIDN", "");
		params.put("PASS_O_MARK", " ");
		// 壓 F017 的 MAC 欄位
		f017MAC(params);
		result = f017Telcomm.query(params);
		doF011(params); // 結束需回打F011電文
		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		result.getFlatValues().put("ATRAMT", params.get("ATRAMT"));
		/*
		 * try { ((F017)SpringBeanFactory.getBean("f017")).writeLog(null, params,
		 * result); } catch(Exception e) { logger.error("寫入 F017 TXNFXRECORD 有誤.", e); }
		 */
		return result;
	}

	public Object writeLog(final TelCommExecWrapper execwrapper, final Map _params, final MVHImpl result) {
		Map<String, String> params = _params;
		DecimalFormat dft = new DecimalFormat("#0");
		DecimalFormat dftdot2 = new DecimalFormat("#0.00");
		params.put("__TRANS_STATUS", "ONLINE");
		TXNFXRECORD record = execwrapper.createTxnFxRecord();

		/*** For 存入 Txnlog ***/
		BOLifeMoniter.setValue("FXADTXNO", record.getADTXNO());

		record.setADOPID("F017");

		record.setFXUSERID(params.get("UID") == null ? "" : params.get("UID"));
		record.setFXWDAC(params.get("FYACN") == null ? "" : params.get("FYACN")); // 轉出帳號
		record.setFXSVBH("050"); // 轉入分行
		record.setFXSVAC(params.get("INACN") == null ? "" : params.get("INACN")); // 轉入帳號
		record.setFXWDCURR(params.get("OUT_CRY") == null ? "" : params.get("OUT_CRY")); // 轉出幣別
		record.setFXSVCURR(params.get("IN_CRY") == null ? "" : params.get("IN_CRY")); // 轉入幣別

		try {
			record.setFXWDAMT(dftdot2.format(
					dft.parse(params.get("AMTDHID") == null ? "0" : params.get("AMTDHID")).doubleValue() / 100d)); // 轉出金額
			record.setFXSVAMT(dftdot2
					.format(dft.parse(params.get("ATRAMT") == null ? "0" : params.get("ATRAMT")).doubleValue() / 100d)); // 轉入金額
		} catch (Exception e) {
		}

		record.setFXTXMEMO("");
		record.setFXTXMAILS("");
		record.setFXTXMAILMEMO(""); // CMMAILMEMO 對應的欄位
		record.setFXEXRATE(StrUtils.trim(params.get("RATE")));// 匯率

		try {
			String tmp1 = "";
			String tmp2 = "";
			tmp1 = (params.get("DISFEE") == null ? ""
					: AmountUtil.deleteComma(NumericUtil.formatNumberString(params.get("DISFEE"), 2)));
			tmp2 = (params.get("DISTOTH") == null ? ""
					: AmountUtil.deleteComma(NumericUtil.formatNumberString(params.get("DISTOTH"), 2)));
			record.setFXEFEE(tmp1 + "/" + tmp2);// 手續費 + 郵電費

			record.setFXEFEECCY(""); // 手續費幣別
		} catch (Exception e) {
		}

		record.setFXTXCODE(params.get("FGTXWAY"));

		record.setFXMSGSEQNO(StrUtils.trim(params.get("__FXMSGSEQNO")));

			

		/*** New Add ***/
		record.setFXMSGCONTENT((params.get("__FXMSGCONTENT") == null) ? "" : params.get("__FXMSGCONTENT"));

		// 20190522 edit by ian 改成不寫入 TXNREQINFO 只寫入TXNFXRECORD
		txnFxRecordDao.save(record);

		return record;
	}

	/**
	 * 有關FENB-017 電文押碼:
	 * 
	 * 將以下資料押碼 1. 付款帳號 FYACN 2. 付款人統一編號 UID 3. 付款人日期 PRCDT 4. 交易序號 STAN
	 * 
	 * 
	 * 
	 * 25 '0' + 付款帳號 (11) : 36 '000' + 付款人統一編號 (8) : 11 ==> ID 押碼遇字母轉為數字 A->1, B->2
	 * , J->0, K->1... 付款人日期 : 8 '0' + 交易序號 (8) : 9
	 * -------------------------------------------------------- 64
	 * 
	 * @param params
	 */
	public static void f017MAC(Map<String, String> params) {
		String acctid = StrUtils.repeat("0", 25) + String.format("%-11s", params.get("ACCTID"));
		log.debug("acctid length : " + acctid.length());
		String uid = params.get("CUSTPAY_ID");
		Pattern p = Pattern.compile("[A-Z]", Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);

		Map<String, String> mv = new HashMap();
		mv.put("uid", uid);
		mv.put("stan", params.get("STAN")); // STAN 的格式為 A00001

		// 押碼遇字母轉為數字 A->1, B->2 , J->0, K->1...
		for (String key : mv.keySet()) {
			String value = mv.get(key);

			Matcher m = p.matcher(value);
			StringBuffer resultString = new StringBuffer();
			while (m.find()) {
				String eng = m.group().toLowerCase();
				int x = (eng.charAt(0) - 0x61 + 1) % 10;
				m.appendReplacement(resultString, x + "");
			}
			m.appendTail(resultString);

			mv.put(key, resultString.toString());
			// if("stan".equals(key))
//				System.out.printf("%s-> old: %s, new: %s\r\n", key, value, resultString.toString()); // CGI Reflected XSS All Clients
		}
		// 統編有可能有7碼, 所以右補0
		String custpay_id = StrUtils.repeat("0", 3) + StrUtils.left(mv.get("uid") + "000000000", 8);
		String stan = "0" + mv.get("stan"); // 9 碼

		String prcdt = params.get("PRCDT"); // 8 碼
		log.debug("acctid len: " + acctid.length());
		log.debug("custpay_id len: " + custpay_id.length());
		log.debug("prcdt len: " + prcdt.length());
		log.debug("stan len: " + stan.length());

		String __mac64 = acctid + custpay_id + prcdt + stan;
		log.debug("F003 MAC 64 LENGTH:" + __mac64.length());
		log.debug(ESAPIUtil.vaildLog("F003 MAC: " + __mac64));
		if (__mac64.length() != 64)
			throw new RuntimeException("錯誤的長度.");
		/*
		 * KeyClient client = new KeyClient(); String result = KeyClientUtils.doRequest(
		 * IKeySpi.CREATE_MAC, KeyLabel.KEYLABEL_MACKEY,
		 * client.toEBCDIC(StrUtils.left(StrUtils.right(DateTimeUtils.getCDateShort(new
		 * Date()), 6) + "00000000", 8).getBytes()),
		 * client.toEBCDIC(__mac64.getBytes()), "0128");
		 * 
		 * if(!StrUtils.trim(result).startsWith("ERROR")) { try { params.put("MAC",
		 * result.substring(0, 8)); params.put("MAC_SYNC", params.get("SYNC")); //4 碼 }
		 * catch(RuntimeException e) { throw TopMessageException.create("Z300"); //押解碼錯誤
		 * } }
		 */
		String result1 = "";
		try {
			result1 = VaCheck.getSYNC();
			if (result1 != null && result1.length() > 0) {
				try {
//					log.debug("SYNC = " + result1.substring(0, 8)); // CGI Reflected XSS All Clients
					params.put("SYNC", result1.substring(0, 8));
				} catch (RuntimeException e) {
					throw TopMessageException.create("Z300"); // 押解碼錯誤
				}
			} else {
				throw TopMessageException.create("Z300"); // 押解碼錯誤

			}
		} catch (Exception e) {
			log.debug("getSYNC ERROR:" + e.getMessage());
		}
		String result = "";
		try {
			result = VaCheck.getMAC(__mac64);
			if (result != null && result.length() > 0) {
				try {
					log.debug("MAC = " + result.substring(0, 8));
					params.put("MAC", result.substring(0, 8));
					params.put("MAC_SYNC", params.get("SYNC")); // 4 碼
				} catch (RuntimeException e) {
					log.debug("getMAC ERROR: result substring error");
					throw TopMessageException.create("Z300"); // 押解碼錯誤
				}
			} else {
				log.debug("getMAC ERROR: result size null or zero");
				throw TopMessageException.create("Z300"); // 押解碼錯誤
			}
		} catch (Exception e) {
			log.debug("getMAC ERROR:" + e.getMessage());
			throw TopMessageException.create("Z300");
		}
	}

	public TelcommResult doF001(Map<String, String> params) {
		// TODO 組合F001電文所需要的欄位

		Date d = new Date();

		params.put("TERM_DATE", DateTimeUtils.format("yyyyMMdd", d));
		params.put("TERM_TIME", DateTimeUtils.format("HHmmss", d));
		String tranOut = StrUtils.trim(params.get("FYACN"));
		String tranIn = StrUtils.trim(params.get("INACN"));
		if (tranOut.startsWith("893") && tranIn.startsWith("893")) {
			params.put("PCODE", "000C");
			params.put("COUNTRY", "XA");
		} else if (!tranOut.startsWith("893") && !tranIn.startsWith("893")) {
			params.put("PCODE", "000C");
			params.put("COUNTRY", "TW");
		} else {
			params.put("PCODE", "000B");
			params.put("COUNTRY", "XA");
		}

		params.put("DEALNO", StrUtils.trim(params.get("BGROENO")));

		/*** New Add ***/
		if (params.get("STAN") == null) {
//			params.put("STAN", "N" + StrUtils.right("0000000" + sysDailySeqDao.dailySeq("F017T"), 7) + "");
			params.put("STAN", sysDailySeqDao.getFXStan("F017T"));
			BOLifeMoniter.setValue("STAN", params.get("STAN"));
		}

		params.put("SEQNO", "00000");
		params.put("CUSTID", params.get("UID"));
		params.put("CUSTYPE", params.get("CUSTYPE"));

		/*** New Add ***/
		// params.put("MSGID", "");

		params.put("COMMCCY", ""); // 手續費幣別
		params.put("COMMACC", ""); // 手續費帳號

		params.put("CUSTACC", params.get("FYACN")); // 轉出帳號
		params.put("BENACC", params.get("INACN")); // 轉入帳號

		params.put("AMTTYPE", "1");// 金額類別
		params.put("CURAMT", params.get("CURAMT"));// 轉出金額 (議價金額)
		params.put("OUT_AMT", params.get("CURAMT"));// 轉出金額 (議價金額) => 保留原始輸入欄位值

		params.put("PAYDATE", StrUtils.trim(params.get("TERM_DATE")).replaceAll("/", "")); // 付款日期

		params.put("CUSNM1", params.get("NAME")); // 付款人名稱
		params.put("CUSNM4", "DCL"); // 註銷交易flag
		params.put("PAYREMIT", "1");
		/*
		 * 如果電文失敗或寫DB發生任何錯誤, 則發 F007的電文 但是 FLAG 內容為 C 的電文
		 */
		TelcommResult result = null;
		result = f001Telcomm.query(params); // 議價電文

		return result;
	}

	public TelcommResult doF011(Map<String, String> params) {
		// TODO 組合F011電文所需要的欄位

		String tranOut = StrUtils.trim(params.get("FYACN"));
		String tranIn = StrUtils.trim(params.get("INACN"));
		if (tranOut.startsWith("893") && tranIn.startsWith("893"))
			params.put("PCODE", "000C");
		else if (!tranOut.startsWith("893") && !tranIn.startsWith("893"))
			params.put("PCODE", "000C");
		else
			params.put("PCODE", "000B");

		params.put("MSGID", "");
		params.put("CUSTYPE", params.get("CUSTYPE"));

		params.put("CUSTACC", params.get("FYACN")); // 轉出帳號
		params.put("PAYACC", params.get("FYACN")); // 轉出帳號
		params.put("BENACC", params.get("INACN")); // 轉入帳號
		params.put("AMTTYPE", "1");// 金額類別
		params.put("CURAMT", params.get("CURAMT"));// 轉出金額 (議價金額)

		/*
		 * 如果電文失敗或寫DB發生任何錯誤, 則發 F007的電文 但是 FLAG 內容為 C 的電文
		 */

		TelcommResult result = null;
		try {
			result = f011Telcomm.query(params); // 議價電文
		} catch (Exception e) {
			log.warn("f011Telcomm MSGCODE:" + e.getMessage());
		}
		return result;
	}
}
