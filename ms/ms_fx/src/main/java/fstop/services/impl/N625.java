package fstop.services.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.model.EachRowCallback;
import fstop.model.IMasterValuesHelper;
import fstop.model.MVH;
import fstop.model.MVHUtils;
import fstop.model.Row;
import fstop.model.RowsSelecter;
import fstop.model.TelcommResult;
import fstop.orm.dao.AdmKeyValueDao;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

//@Transactional
@Slf4j
public class N625 extends CommonService  {
	
	@Autowired
	@Qualifier("n625Telcomm")
	private TelCommExec n625Telcomm;
	
	@Autowired
	private AdmKeyValueDao admKeyValueDao;
	
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public MVH doAction(Map params) {
		String period = (String)params.get("FGPERIOD");
		String enddate = "";
		String stadate = "";
		String periodStr = "";
		if("CMTODAY".equals(period)) { //今日
			stadate = DateTimeUtils.getCDateShort(new Date());
			enddate = stadate;
			periodStr = DateTimeUtils.format("yyyy/MM/dd", new Date());
		} else if("CMCURMON".equals(period)) { //本月
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.DATE, 1);
			Date dstart = cal.getTime();
			
			Date dend = new Date();
			stadate = DateTimeUtils.getCDateShort(dstart);
			enddate = DateTimeUtils.getCDateShort(dend);
			periodStr = DateTimeUtils.format("yyyy/MM/dd", dstart) + "~" + DateTimeUtils.format("yyyy/MM/dd", dend);
		} else if("CMLASTMON".equals(period)) { //上月
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.DATE, 1);
			cal.add(Calendar.MONTH, -1);
			Date dstart = cal.getTime();
			cal.add(Calendar.MONTH, 1);
			cal.add(Calendar.DATE, -1);
			Date dend = cal.getTime();
			stadate = DateTimeUtils.getCDateShort(dstart);
			enddate = DateTimeUtils.getCDateShort(dend);
			periodStr = DateTimeUtils.format("yyyy/MM/dd", dstart) + "~" + DateTimeUtils.format("yyyy/MM/dd", dend);
		} else if("CMPERIOD".equals(period)) { //指定日期
			stadate = (String)params.get("CMSDATE");
			enddate = (String)params.get("CMEDATE");
			stadate = StrUtils.trim(stadate).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
			enddate = StrUtils.trim(enddate).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
			
			if(stadate != null && stadate.length() == 8) {
				Date sd = DateTimeUtils.parse("yyyyMMdd", stadate);
				stadate = DateTimeUtils.getCDateShort(sd);
				periodStr = DateTimeUtils.format("yyyy/MM/dd", sd);
			}
			if(enddate != null && enddate.length() == 8) {
				Date sd = DateTimeUtils.parse("yyyyMMdd", enddate);
				enddate = DateTimeUtils.getCDateShort(sd);
				periodStr += "~" + DateTimeUtils.format("yyyy/MM/dd", sd);
			}
			else { //若未指定結束時間, 則使用今天日期
				enddate = DateTimeUtils.getCDateShort(new Date());
				periodStr += "~" + DateTimeUtils.format("yyyy/MM/dd", new Date());
			}
		}
		params.put("STADATE", stadate);
		params.put("ENDDATE", enddate);
		
		//TODO UI 文件中的帳號, 怪怪的
		if(StrUtils.isNotEmpty((String)params.get("DPCKACNO"))) {
			params.put("ACN", params.get("DPCKACNO"));
		}
		
		TelcommResult telcommResult = n625Telcomm.query(params);
		
		telcommResult.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		telcommResult.getFlatValues().put("CMPERIOD", periodStr);
		telcommResult.getFlatValues().put("CMRECNUM", telcommResult.getValueOccurs("ACN") + "");

		RowsSelecter rowSelecter = new RowsSelecter(telcommResult.getOccurs());
		rowSelecter.each(new EachRowCallback(){

			public void current(Row row) {
				
				if("D".equalsIgnoreCase(row.getValue("CODDBCR"))) {
					row.setValue("DPWDAMT", row.getValue("AMT"));
				}
				if("C".equalsIgnoreCase(row.getValue("CODDBCR"))) {
					row.setValue("DPSVAMT", row.getValue("AMT"));
				}
				
				//使用 adm key value 轉換 memo
				String r = admKeyValueDao.translator("MEMO", row.getValue("MEMO"));
				row.setValue("MEMO_C", r);
			}});

		Map<String, IMasterValuesHelper> tables = MVHUtils.groupByToTables(telcommResult, new String[]{"ACN"});
		MVHUtils.addTables(telcommResult, tables);

		return telcommResult;
	}


}
