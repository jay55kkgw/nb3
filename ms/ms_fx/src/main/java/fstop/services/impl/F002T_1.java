package fstop.services.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fstop.exception.TelCommException;
import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.orm.dao.SysDailySeqDao;
import fstop.orm.dao.TxnFxRecordDao;
import fstop.orm.dao.TxnReqInfoDao;
import fstop.orm.po.TXNFXRECORD;
import fstop.services.CommonService;
import fstop.services.ServiceBindingException;
import fstop.services.TelCommExecWrapper;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import orginal.topibs.utility.MT103;
import orginal.topibs.utility.MT202;

/**
 * 外匯匯出匯款
 * @author Owner
 *
 */
@Slf4j
public class F002T_1 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());
	@Autowired
	private SysDailySeqDao sysDailySeqDao;

	@Autowired
	@Qualifier("f001Telcomm")
	private TelCommExec f001Telcomm;
	@Autowired
	@Qualifier("f003Telcomm")
	private TelCommExec f003Telcomm;
	@Autowired
	@Qualifier("f005Telcomm")
	private TelCommExec f005Telcomm;
	@Autowired
	@Qualifier("f007Telcomm")
	private TelCommExec f007Telcomm;
	@Autowired
	@Qualifier("f011Telcomm")
	private TelCommExec f011Telcomm;
	@Autowired
	@Qualifier("f021Telcomm")
	private TelCommExec f021Telcomm;
	@Autowired
	@Qualifier("mervaTelcomm")
	private TelCommExec mervaTelcomm;

	@Autowired
	private TxnFxRecordDao txnFxRecordDao;

	@Autowired
	private TxnReqInfoDao txnReqInfoDao;
	
//	@Required
//	public void setTxnReqInfoDao(TxnReqInfoDao txnReqInfoDao) {
//		this.txnReqInfoDao = txnReqInfoDao;
//	}	
//	
//	public void setSysDailySeqDao(SysDailySeqDao sysDailySeqDao) {
//		this.sysDailySeqDao = sysDailySeqDao;
//	}
//
//	public void setMervaTelcomm(TelCommExec mervaTelcomm) {
//		this.mervaTelcomm = mervaTelcomm;
//	}
//
//	@Required
//	public void setF001Telcomm(TelCommExec telcomm) {
//		f001Telcomm = telcomm;
//	}
//
//	@Required
//	public void setF003Telcomm(TelCommExec telcomm) {
//		f003Telcomm = telcomm;
//	}
//
//	@Required
//	public void setF005Telcomm(TelCommExec telcomm) {
//		f005Telcomm = telcomm;
//	}
//
//	@Required
//	public void setF007Telcomm(TelCommExec telcomm) {
//		f007Telcomm = telcomm;
//	}
//
//	@Required
//	public void setF011Telcomm(TelCommExec telcomm) {
//		f011Telcomm = telcomm;
//	}
//
//	@Required
//	public void setF021Telcomm(TelCommExec telcomm) {
//		f021Telcomm = telcomm;
//	}
//
//	@Required
//	public void setTxnFxRecordDao(TxnFxRecordDao txnFxRecordDao) {
//		this.txnFxRecordDao = txnFxRecordDao;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params) { 
		//TODO 在營業時間 09:30~15:30 外, 只能執行預約交易
		//TODO LOG 寫入的機制
		Map<String, String> params = _params;
		
		String paydate = StrUtils.trim(params.get("PAYDATE")).replaceAll("/", "");
		params.put("PAYDATE", paydate);
		
				
		String f = params.get("FGTRDATE");
		if("0".equals(f)) {  //		即時
			MVHImpl result = new MVHImpl();
			try {
				result =  online(params);
				if(result == null)
					result = new MVHImpl();
				
			}
			finally {
				 
				try {
					((F002T_1)SpringBeanFactory.getBean("f002t")).writeLog(null, _params, result);
				}
				catch(Exception e) {
					log.error("寫入 F002T TXNFXRECORD 有問題.", e);
				} 
			}
				  
			return result;
		}
		else {
			throw new ServiceBindingException("不為即時交易");
		}
	}
	
	public MVHImpl online(Map<String, String> _params) {

		TopMessageException lastException = null;
		Map<String, String> params = _params;
		
		//TODO 在f001發出後, 若主機回應失敗, BO 要發出 Exception, 攔到 exception 之後, 發沖正電文   

		//	F007 沖正部位 -> 在做帳務之前
		//
		//   f005 沖正帳務電文    只有外匯匯出匯款要沖正, 在入扣帳要求成功之後
		//
		//
		/*
		1. 計價/央行申報及營業日檢核(FENB-001 前置電文)
	    2. 主機A結果回應(FENB-001)
               	不明 -> 回應失敗    f007 去沖正   
        3. 主機A扣/入帳要求(FENB-003)
        4. 主機A結果回應(FENB-003)
               	不明 -> 人工處理    
        5. 交易結束回存主機A(FENB-011)
        6. 主機A結果回應(FENB-011)
                                          失敗、不明 -> 人工處理  
                                          
		*/
		
/*		
		boolean doF007C = false;
		TelcommResult result = null;
		TelcommResult resultF001 = null;
		try {
			resultF001 = doF001(params);
			result = resultF001;
		}
		catch(TelCommException e) {
			logger.error(e);
			//若是發送電文時, 發生問題
			doF007C = true;
		}
		catch(Exception e) {
			logger.error(e);
			doF007C = true;
		}
		if(checkSuccessTopMsg(resultF001.getFlatValues().get("__TOPMSG"))) {
			doF007C = false;
		}
		else {
			doF007C = true;
		}
*/
		
		MVHImpl result = null;		
		
		String str_BGROENO = params.get("BGROENO");  //議價編號
		String str_RATE = params.get("RATE");  //匯率
		
		//表示先前已取得議價部位
		if (str_BGROENO != null) {
			
			boolean doF007CC = false;
			MVHImpl resultF007y = null;		
			
			try {
				resultF007y = doF007Y(params);
			}
			catch(Exception e) {
				if(e instanceof TopMessageException)
					lastException = (TopMessageException)e;

				doF007CC = true;
			}
			
			if(checkSuccessTopMsg(resultF007y.getFlatValues().get("__TOPMSG"))) {
				doF007CC = false;
			}
			else {
				doF007CC = true;
				lastException = TopMessageException.create(resultF007y.getFlatValues().get("__TOPMSG"));
			}
			
			if(doF007CC) {
				result = doF007C(params);
				
				if(lastException != null)
					throw lastException;
				
				return result;
			}
		} //end if (str_BGROENO != null)
		
	
		boolean doF007C = false;
		MVHImpl resultF001 = null;
		MVHImpl resultF003 = null;
		
		
		
		try {
			resultF001 = doF001(params);
			result = resultF001;
		}
		catch(TelCommException e) {
			log.error("F001 TelCommException .. ", e);
			//若是發送電文時, 發生問題
			doF007C = true;
		}
		catch(Exception e) {
			if(e instanceof TopMessageException)
				lastException = (TopMessageException)e;
			log.error("F001 Exception .. ", e);
			doF007C = true;
		}

		if(resultF001 != null ) {
			if(checkSuccessTopMsg(resultF001.getFlatValues().get("__TOPMSG"))) {
				doF007C = false;
			}
			else {
				doF007C = true;
				lastException = TopMessageException.create(resultF001.getFlatValues().get("__TOPMSG"));
				
			}
			
		}
		else {
			doF007C = true;
		}
		
		//轉入及轉出同幣別不發 F007
		if((doF007C && !StrUtils.trim(params.get("PAYCCY")).equalsIgnoreCase(params.get("REMITCY")))) {
			//TODO 判斷 f001 是失敗的
			result = doF007C(params);
			
			params.put("OKFLAG", "N");
			result = doF011(params);  //交易結束回存主機
			
			if(lastException != null)
				throw lastException;
			
			return resultF001;
		}
		else if(doF007C) {
			params.put("OKFLAG", "N");
			result = doF011(params);  //交易結束回存主機
						
			if(lastException != null)
				throw lastException;

			return resultF001;
		}
		

		boolean isDof005 = false;
		boolean isDoMerva = false;
		String okflag = "Y";

		try {						
			result = doF003(resultF001, params);			
			resultF003 = result;												
		}
		catch(TelCommException e) {
			log.error("F003 TelCommException .. ", e);
			isDoMerva = false;
			okflag = "N";
		}
		catch(Exception e) {
			if(e instanceof TopMessageException)
				lastException = (TopMessageException)e;
			log.error("F003 Exception ..", e);
			isDoMerva = false;
			okflag = "N";
		}
		
		if(resultF003 != null ) {
			if(checkSuccessTopMsg(resultF003.getFlatValues().get("__TOPMSG"))) {
				isDoMerva = true;
				okflag = "Y";
			}
			else {
				isDoMerva = false;
				okflag = "N";
				lastException = TopMessageException.create(resultF003.getFlatValues().get("__TOPMSG"));
				
			}				
		}
		else if(resultF003 == null) {
			isDoMerva = false;
			okflag = "N";
		}		
		
		
		if (isDoMerva) {
			TelcommResult mervaResult = null;
			
			try {				
				mervaResult = doMerva(params, resultF001);
				
				// [__TOPMSG] = "MVA0000"
				if(checkSuccessTopMsg(mervaResult.getFlatValues().get("__TOPMSG"))) {
					isDof005 = false;
					okflag = "Y";					
				}
				else if("Z014".equals(mervaResult.getFlatValues().get("__TOPMSG"))) {
					isDof005 = false;
					okflag = "N";	
				}	
				else {
					isDof005 = true;
					okflag = "N";					
					lastException = TopMessageException.create(mervaResult.getFlatValues().get("__TOPMSG"));
				}
						
			}
			catch(TelCommException e) {
				log.error(e.toString());
				isDof005 = true;
			}
			catch(Exception e) {
				if(e instanceof TopMessageException)
					lastException = (TopMessageException)e;
				log.error(e.toString());
				isDof005 = true;
				okflag = "N";	
			}
		}
		
		if(isDof005) {
			result = doF005(params);
			
			params.put("OKFLAG", "N");
			result = doF011(params);  //交易結束回存主機
			
			if(lastException != null)
				throw lastException;		
			
			return resultF003;			
			//return result;
		}
		
		
		//無論如何都要發
		//除了 Pendding 的之外
		//TODO 判斷 pendding 的時候不送
		//TODO 發生 Exception 時, OKFLAG 要塞N
		params.put("OKFLAG", okflag);
		result = doF011(params);  //交易結束回存主機
		
		if(lastException != null)
			throw lastException;		
				
		return resultF003;		
		//return result;
	}

	public TelcommResult doF001(Map<String, String> params) {
		//TODO 組合F001電文所需要的欄位
		
		Date d = new Date();
		
		params.put("TERM_DATE", DateTimeUtils.format("yyyyMMdd", d));
		params.put("TERM_TIME", DateTimeUtils.format("HHmmss", d));
		String tranOut = StrUtils.trim(params.get("CUSTACC"));
		String tranIn = StrUtils.trim(params.get("BENACC"));

		//BENTYPE
		
		params.put("PCODE", "000A");
		
		//params.put("STAN", "N0000119");
		params.put("STAN", "N" + StrUtils.right("0000000" + sysDailySeqDao.dailySeq("F001T"), 7) + "");
		params.put("SEQNO", "00000");
		params.put("CUSTID", params.get("CUSIDN"));		
		params.put("MSGID", "");
		//params.put("CUSTYPE", "1");   //TODO 之後會從 session 來
		
		params.put("COMMCCY", params.get("COMMCCY"));	//手續費幣別			
		params.put("COMMACC", params.get("COMMACC"));	//手續費帳號	
		
		params.put("CUSTACC", params.get("CUSTACC")); //轉出帳號
		//params.put("BENACC", params.get("__TRIN_ACN")); //轉入帳號
		if("1".equals(params.get("PAYREMIT"))) {
			params.put("AMTTYPE", "1");//金額類別
			params.put("CURAMT", params.get("CURAMT"));//轉出金額 (議價金額)
		}
		else { // "2".equals(params.get("PAYREMIT"))
			params.put("AMTTYPE", "2");//金額類別
			params.put("ATRAMT", params.get("CURAMT"));//轉入金額
		}		
		
		/*
	 		如果電文失敗或寫DB發生任何錯誤, 則發 F007的電文
	 		但是  FLAG 內容為 C 的電文
		*/
		
		
		TelcommResult result = f001Telcomm.query(params);  //議價電文
		
		return result;
	}

	public TelcommResult doF003(MVHImpl resultF001, Map<String, String> params) {
		// FROM F001
		params.put("TXBRH", "");   //承作行
		params.put("CUSTPAY_ID", params.get("UID"));   //付款人統一編號
		params.put("ACCTID", params.get("CUSTACC"));   //F001 付款帳號
		params.put("ACCTTYPE", "");   //付款人帳戶類別
		params.put("ACCCUR", resultF001.getFlatValues().get("ORGCCY"));   //付款人轉出幣別
		params.put("ACCAMT", resultF001.getFlatValues().get("ORGAMT"));   //轉出金額
		
		params.put("CURCODE", resultF001.getFlatValues().get("PMTCCY"));   //付款人轉入幣別
		params.put("CURAMT", resultF001.getFlatValues().get("PMTAMT"));   //轉入金額
		
		params.put("BANK_TYPE", "");   //銀行代號類別
		params.put("PRCDT", params.get("PAYDATE"));   //付款日期
		

		//
		//付款人外匯議價號碼,左靠右補空白
		//(13-20)  8 碼為 交易密碼 MAC KEY SYNC
		//(21-36) 16 碼為 USERID  
		//
		//同幣別不會有 "BGROENO", 所以帶空白
		params.put("REFID", StrUtils.left(StrUtils.trim(params.get("BGROENO")) + StrUtils.repeat(" ", 12), 12));
		params.put("SYNC", StrUtils.trim(params.get("SYNC")));   // 8 碼
		params.put("USERID", StrUtils.left(params.get("USERID") + StrUtils.repeat(" ", 16), 16));  //議價編號
		
		params.put("PAYEE_ID", StrUtils.trim(params.get("BENID")));//收款人統一編號
		
		params.put("ACCTIDTO", StrUtils.trim(params.get("BENACC")));   //收款人帳號
		
		params.put("ACCTTP_TO", "");   //收款人帳戶類別
		
		params.put("FEEACCTID", params.get("COMMACC"));   //付款人手續費帳號
		
		params.put("FEEACT_TP", "");   //付款人手續費帳戶類別
		
		params.put("FEEACT_CRY", params.get("COMMCCY"));   //付款手續費帳戶幣別
		
		params.put("FEEBANK_TP", "");   //手續費銀行代號類別
		
		//params.put("TNSATION", resultF001.getFlatValues().get("ORGCOMM"));   //匯出匯款手續費金額
		//params.put("OTHERFEE", resultF001.getFlatValues().get("ORGCABLE"));   //郵電費
		
		Double d_COMMAMT = Double.parseDouble(resultF001.getFlatValues().get("COMMAMT"));
		Double d_OURCHG = Double.parseDouble(resultF001.getFlatValues().get("OURCHG"));
		d_COMMAMT += d_OURCHG;
		Double d = new Double(d_COMMAMT);
		if("OUR".equals(resultF001.getValueByFieldName("M171CHG")))
			params.put("TNSATION", String.valueOf(d.intValue()));   //匯出匯款手續費 + 國外費用
		else
			params.put("TNSATION", resultF001.getFlatValues().get("COMMAMT"));   //匯出匯款手續費金額
		
		params.put("OTHERFEE", resultF001.getFlatValues().get("CABCHG"));   //郵電費
		
		params.put("DISFEE", resultF001.getFlatValues().get("COMMAMT"));   //折扣後手續費
		params.put("DISTOTH", resultF001.getFlatValues().get("CABCHG"));   //折扣後郵電費
		params.put("RESENDFLAG", "");   //重送標記
		
		
		String str_First4Byte = "    ";
		String str_Flag = " "; 			
		if (! "0".equals(params.get("FGTRDATE"))) {   //若為預約交易			
			str_Flag = "Y";			
		}		
		
		if ("000B".equals(params.get("PCODE")))
			str_First4Byte = "FISC";		

		params.put("ACWTYPE", str_First4Byte + "            " + str_Flag);   //收款人帳戶類別			
		
		
		params.put("COMMCCY2", resultF001.getFlatValues().get("COMMCCY2"));   //入帳手續費幣別
		params.put("COMMAMT2", resultF001.getFlatValues().get("COMMAMT2"));   //入帳手續費金額
		params.put("SWFTCODE", resultF001.getFlatValues().get("M1SBBIC"));   //收款人 SWIFT CD
		params.put("BACH_MARK", "N");   //整批註記
		params.put("TXPSW", params.get("CMPASSWORD"));   //押密後交易密碼
		
		//壓 F003 的 MAC 欄位
		f003MAC(params);
		
		
		TelcommResult result = f003Telcomm.query(params);  //
		
		return result;
	}
	


	public TelcommResult doF005(Map<String, String> params) {
		//TODO 組合F005電文所需要的欄位
		
		String tranOut = StrUtils.trim(params.get("CUSTACC"));
		String tranIn = StrUtils.trim(params.get("BENACC")); //轉入帳號

		params.put("PCODE", "000A");
		
		params.put("MSGID", "");
		params.put("CUSTYPE", "1");   //TODO 之後會從 session 來
		
		params.put("CUSTACC", params.get("CUSTACC")); //轉出帳號
		if("1".equals(params.get("PAYREMIT"))) {
			params.put("AMTTYPE", "1");//金額類別
			params.put("CURAMT", params.get("CURAMT"));//轉出金額 (議價金額)
		}
		else { // "2".equals(params.get("PAYREMIT"))
			params.put("AMTTYPE", "2");//金額類別
			params.put("ATRAMT", params.get("CURAMT"));//轉入金額
		}		
		
		/*
	 		如果電文失敗或寫DB發生任何錯誤, 則發 F007的電文
	 		但是  FLAG 內容為 C 的電文
		*/
		
		
		TelcommResult result = f005Telcomm.query(params);  //
		
		return result;
	}
	
	public TelcommResult doF007C(Map<String, String> params) {
		//TODO 組合議價電文所需要的欄位
		params.put("FYACN", params.get("CUSTACC")); //轉出帳號
		params.put("INACN", params.get("BENACC")); //轉入帳號
		params.put("OUT_CRY", params.get("PAYCCY")); //轉出幣別
		params.put("IN_CRY", params.get("REMITCY")); //轉入幣別
		if("1".equals(params.get("PAYREMIT"))) {
			params.put("AMTTYPE", "1");//金額類別
			params.put("CURAMT", params.get("CURAMT"));//轉出金額 (議價金額)
		}
		else { // "2".equals(params.get("PAYREMIT"))
			params.put("AMTTYPE", "2");//金額類別
			params.put("ATRAMT", params.get("CURAMT"));//轉入金額
		}		
		params.put("FLAG", "C");//交易狀態
		/*
	 		如果電文失敗或寫DB發生任何錯誤, 則發 F007的電文
	 		但是  FLAG 內容為 C 的電文
		*/
		
		
		TelcommResult result = f007Telcomm.query(params);  //議價電文  
		
		if("1".equals(params.get("PAYREMIT"))) {
			try {
				double d = new Double(params.get("CURAMT")) * new Double(params.get("RATE"));
				result.getFlatValues().put("CURAMT", d + "");  // 轉出金額
			}
			catch(Exception e){}
		}
		else { // "2".equals(params.get("PAYREMIT"))
			try {
				double d = new Double(params.get("ATRAMT")) * new Double(params.get("RATE"));
				result.getFlatValues().put("ATRAMT", d + "");  // 轉入金額
			}
			catch(Exception e){}
		}
		
		
		return result;
	}
	
	public TelcommResult doF011(Map<String, String> params) {
		//TODO 組合F011電文所需要的欄位
		
		String tranOut = StrUtils.trim(params.get("CUSTACC"));
		String tranIn = StrUtils.trim(params.get("BENACC"));  //轉入帳號

		params.put("PCODE", "000A");
		
		params.put("MSGID", "");
		params.put("CUSTYPE", "1");   //TODO 之後會從 session 來
		
		
		params.put("CUSTACC", params.get("CUSTACC")); //轉出帳號
		
		if("1".equals(params.get("PAYREMIT"))) {
			params.put("AMTTYPE", "1");//金額類別
			params.put("CURAMT", params.get("CURAMT"));//轉出金額 (議價金額)
		}
		else { // "2".equals(params.get("PAYREMIT"))
			params.put("AMTTYPE", "2");//金額類別
			params.put("ATRAMT", params.get("CURAMT"));//轉入金額
		}		
		
		/*
	 		如果電文失敗或寫DB發生任何錯誤, 則發 F007的電文
	 		但是  FLAG 內容為 C 的電文
		*/
		
		TelcommResult result = f011Telcomm.query(params);   
		
		return result;
	}
	
	public TelcommResult doMerva(Map<String, String> params, MVHImpl f001Result) {
		//TODO 組合 MERVA 電文所需要的欄位
		Date d = new Date();
		params.put("TIA-TERM-DATE", DateTimeUtils.format("yyyyMMdd", d));
		params.put("TIA-TERM-TIME", DateTimeUtils.format("HHmmss", d));			
		
		String str_TLID = f001Result.getValueByFieldName("M1SWIFT").substring(8);
		if (str_TLID.equals("XXX"))	{
			params.put("BRH-CODE", "XXX");			
			params.put("TLID", "REM");								
		}	
		else {
			params.put("BRH-CODE", f001Result.getValueByFieldName("M1SWIFT").substring(8));
			params.put("TLID", f001Result.getValueByFieldName("M1SWIFT").substring(8));						
		}

		params.put("WS-ID", f001Result.getValueByFieldName("STAN"));	
		
		//FOO1 SWTKIND
		//1跟R 時, 送 MT103
		//2時, 送MT103+202
		StringBuffer FIDATA = new StringBuffer();	
		MT103 mt103 = new MT103();
		mt103.setF001Result(f001Result);
		String mt103Data = mt103.createMsg();
		FIDATA.append(mt103Data);
		if("2".equals(f001Result.getValueByFieldName("SWTKIND"))) {
			MT202 mt202 = new MT202();
			mt202.setF001Result(f001Result);
			String mt202Data = mt202.createMsg();
			FIDATA.append(mt202Data);
		}
		
		
		//長度要送
		params.put("DATALENGTH", FIDATA.length() + "");
		params.put("FIDATA", FIDATA.toString());
		
		TelcommResult result = mervaTelcomm.query(params);   
		
		return result;
	}
	
	private String getTITAInfo(Map<String, String> params) {
		Map<String, String> mr = new HashMap();
		//TODO F001 特別要記的欄位
		String[] fields = new String[]{
			"SRCFUND" //匯款分類編號
				,"BENTYPE" //收款人身份別
				,"MEMO1"   //收款人附言
		};

		for(String f : fields) {
			mr.put(f, params.get(f)); 
		}
		return JSONUtils.map2json(mr);
	}
	/**
	有關FENB-003 電文押碼:
	 
	  將以下資料押碼                              
	1. 付款帳號          ACCTID       
	2. 付款人統一編號     CUSTPAY_ID   
	3. 付款人日期        PRCDT         
	4. 交易序號          STAN         
	 
	 
	 
	    25 '0' +    付款帳號   (11)    :    36
	   '000'   +   付款人統一編號 (8) :      11    ==>  ID 押碼遇字母轉為數字       A->1, B->2 , J->0, K->1...
	    付款人日期                     :     8
	    '0'   + 交易序號     (8)       :     9
	--------------------------------------------------------
	                                        64	
	 * @param params
	 */
	public static void f003MAC(Map<String, String> params) {
		F001T.f003MAC(params);
	}

	public MVHImpl doF007Y(Map _params) { 
		Map<String, String> params = _params;
		
		String fginacno = params.get("FGINACNO"); // 1 約定帳號, 2 非約定帳號
		
		//轉入帳號
		String trin_bnkcod = "";
		String trin_acn = "";
		String trin_agree = "";
		if("CMDAGREE".equals(fginacno)) {
			Map<String, String> jm = JSONUtils.json2map(params.get("INACNO1"));
			trin_bnkcod = jm.get("BNKCOD");
			trin_acn = jm.get("ACN");
			trin_agree = jm.get("AGREE");
			
		}
		else if("CMDISAGREE".equals(fginacno)) {  //非約定, 只能是台企銀 050
			trin_bnkcod = "050";
			trin_acn = params.get("INACNO2");
			trin_agree = "0";   // 0: 非約定, 1: 約定
		}
		
		params.put("__TRIN_BANKCOD", trin_bnkcod);
		params.put("__TRIN_ACN", trin_acn);
		params.put("__TRIN_AGREE", trin_agree);
				
		
		
		//TODO 組合議價電文所需要的欄位
		params.put("FYACN", params.get("CUSTACC")); //轉出帳號
		params.put("INACN", params.get("__TRIN_ACN")); //入帳號
		params.put("OUT_CRY", params.get("PAYCCY")); //轉出幣別
		params.put("IN_CRY", params.get("REMITCY")); //轉入幣別
		if("1".equals(params.get("PAYREMIT"))) {
			params.put("AMTTYPE", "1");//金額類別
			params.put("CURAMT", params.get("CURAMT"));//轉出金額 (議價金額)
		}
		else { // "2".equals(params.get("PAYREMIT"))
			params.put("AMTTYPE", "2");//金額類別
			params.put("ATRAMT", params.get("CURAMT"));//轉入金額
		}		
		params.put("FLAG", "Y");//交易狀態


		/*
		 	如果電文失敗或寫DB發生任何錯誤, 則發 F007的電文
		 	但是  FLAG 內容為 C 的電文
		 */
		
		
		TelcommResult result = f007Telcomm.query(params);  
		
		
		return result;
	}
	
	public boolean checkSuccessTopMsg(String topmsg) {
		//若回傳為 000000, 或者為 空白, 則回傳 true
		try {
			Double d = Double.parseDouble(topmsg);
			if(d == 0d)
				return true;
		}
		catch(Exception e) {}
		return "".equals(topmsg) || "MVA0000".equals(topmsg);
	}
	
	
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public Object writeLog(final TelCommExecWrapper execwrapper, final Map _params, final MVHImpl result) {
		Map<String,String> params = _params;
		TXNFXRECORD record = execwrapper.createTxnFxRecord();

		record.setADOPID("F002");

		record.setFXUSERID(params.get("UID"));
		record.setFXWDAC(params.get("CUSTACC"));  //轉出帳號
		record.setFXSVBH("");  //轉入分行
		record.setFXWDAC(StrUtils.trim(params.get("BENACC")));  //轉入帳號
		record.setFXWDCURR(params.get("PAYCCY"));  //轉出幣別
		record.setFXSVCURR(params.get("REMITCY"));  //轉入幣別
		
//		record.setFXSCHNO(params.get("__SCHNO")==null?"" : params.get("__SCHNO"));
		
		if("1".equals(params.get("PAYREMIT"))) {
			record.setFXWDAMT(params.get("CURAMT"));  // 轉出金額
		}
		else { // "2".equals(params.get("PAYREMIT"))
			record.setFXSVAMT(params.get("CURAMT"));  // 轉入金額
		}
		//處理{匯款附言}欄位
		String str_Memo1 = params.get("MEMO1").toString();
		str_Memo1=str_Memo1.replaceAll("\r", "");
		str_Memo1=str_Memo1.replaceAll("\n", "");
		params.put("MEMO1", str_Memo1);		 
		record.setFXTXMEMO(params.get("MEMO1"));
		record.setFXTXMAILS(params.get("CMTRMAIL"));
		record.setFXTXMAILMEMO(params.get("CMMAILMEMO"));  //CMMAILMEMO 對應的欄位

		record.setFXEFEE("0");
		record.setFXTXCODE(params.get("FGTXWAY"));
		
		record.setFXMSGSEQNO("A");
		
//		TXNREQINFO titainfo = null;
//		if(record.getFXTITAINFO() == 0) {
//			titainfo = new TXNREQINFO();
//			titainfo.setREQINFO(BookingUtils.requestInfo(params));
//		}
//		
//		txnFxRecordDao.writeTxnRecord(titainfo, record);
		
		// 20190522 edit by ian 改成不寫入 TXNREQINFO 只寫入TXNFXRECORD
		txnFxRecordDao.save(record);

		
		return record;
		
	}
	public TelcommResult doF007A(Map<String, String> params) {
		//TODO 組合議價電文所需要的欄位
		Date d = new Date();
		params.put("CMTRDATE", DateTimeUtils.format("yyyyMMdd", d));
		params.put("CMTRTIME", DateTimeUtils.format("HHmmss", d));

		params.put("FYACN", params.get("CUSTACC")); //轉出帳號
		//params.put("INACN", params.get("BENACC")); //轉入帳號
		params.put("INACN", ""); //轉入帳號
		
		params.put("OUT_CRY", params.get("PAYCCY")); //轉出幣別
		params.put("IN_CRY", params.get("REMITCY")); //轉入幣別
		if("1".equals(params.get("PAYREMIT"))) {
			params.put("AMTTYPE", "1");//金額類別
			params.put("CURAMT", params.get("CURAMT"));//轉出金額 (議價金額)
		}
		else { // "2".equals(params.get("PAYREMIT"))
			params.put("AMTTYPE", "2");//金額類別
			params.put("ATRAMT", params.get("CURAMT"));//轉入金額
		}		
		params.put("FLAG", "A");//交易狀態
		/*
	 		如果電文失敗或寫DB發生任何錯誤, 則發 F007的電文
	 		但是  FLAG 內容為 C 的電文
		*/	
		
		
		TopMessageException lastException = null;		
		TelcommResult result = null;  //議價電文
		
		try {
			result = f007Telcomm.query(params);  //議價電文
		}
		catch(Exception e) {			
			if(e instanceof TopMessageException) {
				lastException = (TopMessageException)e;
				throw lastException;
			}		
		}
				
		if(! checkSuccessTopMsg(result.getFlatValues().get("MSG_CODE"))) {
			throw new TopMessageException(result.getFlatValues().get("MSG_CODE"), "", "");			
		}		
		
		
		
		if("1".equals(params.get("PAYREMIT"))) {   //轉出
			
				double dd = new Double(params.get("CURAMT")) * new Double(params.get("RATE"));
				result.getFlatValues().put("CURAMT", params.get("CURAMT"));  // 轉出金額
				result.getFlatValues().put("ATRAMT", dd + "");  // 轉入金額
			
		}
		else { // "2".equals(params.get("PAYREMIT"))   //轉入
			
				double dd = new Double(params.get("ATRAMT")) / new Double(params.get("RATE"));
				result.getFlatValues().put("CURAMT", dd + "");  // 轉入金額
				result.getFlatValues().put("ATRAMT", params.get("ATRAMT"));  // 轉入金額
			
		}
		
		return result;
	}
	
}
