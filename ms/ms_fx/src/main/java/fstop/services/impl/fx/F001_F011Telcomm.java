package fstop.services.impl.fx;

import java.util.Hashtable;
import java.util.Map;
import java.util.Vector;

import org.springframework.beans.factory.annotation.Autowired;

import fstop.exception.ToRuntimeException;
import fstop.exception.TopMessageException;
import fstop.model.TelcommResult;
import fstop.services.impl.F001T;
import fstop.services.impl.N870;
import fstop.telcomm.TelCommExec;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class F001_F011Telcomm implements FxTelCommStep {

	@Autowired
	private F001T f001t;
	
	private boolean isNeedDoF011WithFlagN = false; 
	
	private TelcommResult telcommResult = null; 
	
	public F001_F011Telcomm(F001T f001t) {
		this.f001t = f001t;
	}
	
	public Exception lastException;
	boolean isSkip = false;

	@Override
	public boolean isSkip() {
		return isSkip;
	}

	public void setSkip(boolean isSkip) {
		this.isSkip = isSkip;
	}

	@Override
	public TelcommResult query(Map _params) {
		Map<String, String> params = _params;
		
		if(isSkip()) {
			TelcommResult telResult = new TelcommResult(new Vector());
			return telResult;
		}
		
		String FXMSGSEQNO = StrUtils.trim(params.get("__FXMSGSEQNO"));
		if (FXMSGSEQNO.endsWith("5") || FXMSGSEQNO.endsWith("6"))
			params.put("OKFLAG", "N");
		else
			params.put("OKFLAG", "Y");
		
		TelcommResult result = null;
		try {
			result = f001t.doF011(params);
			params.put("__FXMSGSEQNO", StrUtils.trim(params.get("__FXMSGSEQNO")) + "4");
		}
		catch(Exception e) {
			lastException = e;
			
			//進人工重送須記錄 F001.STAN 欄位值			
			params.put("__FXMSGCONTENT", StrUtils.trim(params.get("STAN")));						
		}
		
		return result;
	}

	@Override
	public Exception getLastException() {
		return lastException;
	}

	@Override
	public boolean isNeedDoF011WinFlagN() {
		return isNeedDoF011WithFlagN;
	}
	
	public TelcommResult getTelcommResult() {
		return telcommResult;
	}
	
}
