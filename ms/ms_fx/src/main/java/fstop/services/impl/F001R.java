package fstop.services.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.rest.model.pool.FxUtils;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.orm.dao.TxnFxRecordDao;
import fstop.orm.dao.TxnFxScheduleDao;
import fstop.orm.dao.TxnReqInfoDao;
import fstop.orm.po.TXNFXRECORD;
import fstop.orm.po.TXNREQINFO;
import fstop.services.BookingUtils;
import fstop.services.CommonService;
import fstop.services.ServiceBindingException;
import fstop.services.mgn.B105;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 外匯匯出匯款-即時
 * @author Owner
 *
 */
@Slf4j
public class F001R extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("f007Telcomm")
	private TelCommExec f007Telcomm;

	@Autowired
	private B105 b105;
	
	@Autowired
	private TxnFxRecordDao txnFxRecordDao;

	@Autowired
	private TxnReqInfoDao txnReqInfoDao;
	
//	@Required
//	public void setTxnReqInfoDao(TxnReqInfoDao txnReqInfoDao) {
//		this.txnReqInfoDao = txnReqInfoDao;
//	}
//
//	
//	@Required
//	public void setF007Telcomm(TelCommExec telcomm) {
//		f007Telcomm = telcomm;
//	}
//
//	@Required
//	public void setTxnFxRecordDao(TxnFxRecordDao txnFxRecordDao) {
//		this.txnFxRecordDao = txnFxRecordDao;
//	}

	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params) { 
		//TODO 在營業時間 09:30~15:30 外, 只能執行預約交易
		Map<String, String> params = _params;
		
		String fginacno = params.get("FGINACNO"); // 1 約定帳號, 2 非約定帳號
		
		//轉入帳號
		String trin_bnkcod = "";
		String trin_acn = "";
		String trin_agree = "";
		if("CMDAGREE".equals(fginacno)) {
			Map<String, String> jm = JSONUtils.json2map(params.get("INACNO1"));
			trin_bnkcod = jm.get("BNKCOD");
			trin_acn = jm.get("ACN");
			trin_agree = jm.get("AGREE");
			
		}
		else if("CMDISAGREE".equals(fginacno)) {  //非約定, 只能是台企銀 050
			trin_bnkcod = "050";
			trin_acn = params.get("INACNO2");
			trin_agree = "0";   // 0: 非約定, 1: 約定
		}
		
		params.put("__TRIN_BANKCOD", trin_bnkcod);
		params.put("__TRIN_ACN", trin_acn);
		params.put("__TRIN_AGREE", trin_agree);
				
		String f = params.get("FGTRDATE");
		if("0".equals(f)) {  //		即時
			
			FxUtils util = new FxUtils();
//			if (! b105.isFxBizTime1()) {
//				throw TopMessageException.create("Z400", "F001");
//			}
			
			return online(params);
		}
		else {
			throw new ServiceBindingException("不為即時交易");
		}
		// 取得議價
	}
	
	
	public TelcommResult online(Map<String, String> params) {
		//TODO 組合議價電文所需要的欄位
		Date d = new Date();
		params.put("CMTRDATE", DateTimeUtils.format("yyyyMMdd", d));
		params.put("CMTRTIME", DateTimeUtils.format("HHmmss", d));
		
		params.put("FYACN", params.get("CUSTACC")); //轉出帳號
		params.put("INACN", params.get("__TRIN_ACN")); //轉入帳號
		params.put("OUT_CRY", params.get("PAYCCY")); //轉出幣別
		params.put("IN_CRY", params.get("REMITCY")); //轉入幣別
		if("1".equals(params.get("PAYREMIT"))) {
			params.put("AMTTYPE", "1");//金額類別
			params.put("CURAMT", params.get("CURAMT"));//轉出金額 (議價金額)
		}
		else { // "2".equals(params.get("PAYREMIT"))
			params.put("AMTTYPE", "2");//金額類別
			params.put("ATRAMT", params.get("CURAMT"));//轉入金額
		}		
		params.put("FLAG", "R");//交易狀態
		/*
	 		如果電文失敗或寫DB發生任何錯誤, 則發 F007的電文
	 		但是  FLAG 內容為 C 的電文
		*/	
		
		params.put("BGROENO", "");
		
		TopMessageException lastException = null;		
		TelcommResult result = null;  //議價電文
		
		try {
			result = f007Telcomm.query(params);  //議價電文
		}
		catch(Exception e) {
			if(e instanceof TopMessageException) {
				lastException = (TopMessageException)e;
				throw lastException;
			}		
		}
		
		if(! checkSuccessTopMsg(result.getFlatValues().get("MSG_CODE"))) {
			throw new TopMessageException(result.getFlatValues().get("MSG_CODE"), "", "");			
		}		
		

		params.put("BGROENO", result.getValueByFieldName("BGROENO"));  //議價編號
		params.put("RATE", result.getValueByFieldName("RATE"));  //匯率

		double dd;
		if("1".equals(params.get("PAYREMIT"))) {   //轉出

				if ("TWD".equals(params.get("OUT_CRY")))
					dd = new Double(params.get("CURAMT")) / new Double(params.get("RATE"));					
				else	
					dd = new Double(params.get("CURAMT")) * new Double(params.get("RATE"));
				
				result.getFlatValues().put("F007CURAMT", params.get("CURAMT"));  // 轉出金額
				result.getFlatValues().put("F007ATRAMT", dd + "");  // 轉入金額
			
		}
		else { // "2".equals(params.get("PAYREMIT"))   //轉入
			
				if ("TWD".equals(params.get("OUT_CRY")))
					dd = new Double(params.get("ATRAMT")) * new Double(params.get("RATE"));			
				else	
					dd = new Double(params.get("ATRAMT")) / new Double(params.get("RATE"));		
						
				result.getFlatValues().put("F007CURAMT", dd + "");  // 轉出金額
				result.getFlatValues().put("F007ATRAMT", params.get("ATRAMT"));  // 轉入金額			
		}
		
		return result;
	}
	
	private boolean checkSuccessTopMsg(String topmsg) {
		//若回傳為 000000, 或者為 空白, 則回傳 true
		try {
			Double d = Double.parseDouble(topmsg);
			if(d == 0d)
				return true;
		}
		catch(Exception e) {}
		return "".equals(topmsg);
	}	
	
}
