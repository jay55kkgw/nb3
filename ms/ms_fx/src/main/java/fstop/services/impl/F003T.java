package fstop.services.impl;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

import com.netbank.rest.comm.bean.DoIDGateCommand_In;
import com.netbank.rest.comm.controller.CommonIDGATEController;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.orm.dao.AdmMsgCodeDao;
import fstop.orm.dao.SysDailySeqDao;
import fstop.orm.dao.TxnFxRecordDao;
import fstop.orm.dao.TxnFxSchPayDataDao;
import fstop.orm.po.SYSPARAMDATA;
import fstop.orm.po.TXNFXRECORD;
import fstop.services.CommonService;
import fstop.services.OnLineTransferNotice;
import fstop.services.TelCommExecWrapper;
import fstop.services.WriteLogInterface;
import fstop.services.impl.fx.F003Controller;
import fstop.services.mgn.B105;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import fstop.va.VaCheck;
import lombok.extern.slf4j.Slf4j;
import orginal.topibs.utility.MT191;
import orginal.topibs.utility.MT199;
import topibs.utility.NumericUtil;

/**
 * 外匯線上解款
 * 
 * @author Owner
 *
 */
@Slf4j
public class F003T extends CommonService implements WriteLogInterface
{
	// private Logger logger = Logger.getLogger(getClass());
	@Autowired
	private SysDailySeqDao sysDailySeqDao;

	@Autowired
	private F003Controller f003controller;

	@Autowired
	@Qualifier("f001Telcomm")
	private TelCommExec f001Telcomm;

	@Autowired
	@Qualifier("f003Telcomm")
	private TelCommExec f003Telcomm;

	@Autowired
	@Qualifier("f005Telcomm")
	private TelCommExec f005Telcomm;

	@Autowired
	@Qualifier("f007Telcomm")
	private TelCommExec f007Telcomm;

	@Autowired
	@Qualifier("f011Telcomm")
	private TelCommExec f011Telcomm;

	@Autowired
	@Qualifier("mervaTelcomm")
	private TelCommExec mervaTelcomm;

	@Autowired
	@Qualifier("n951CMTelcomm")
	private TelCommExec n951CMTelcomm;

	@Autowired
	private TxnFxRecordDao txnFxRecordDao;

	@Autowired
	private AdmMsgCodeDao admMsgCodeDao;
	
	@Autowired
	private TxnFxSchPayDataDao txnFxSchPayDataDao; // 預約資料


	@Autowired
	private B105 b105;

	
	@Value("${ms_env}")
	private String ms_env;

	@Autowired
	CommonIDGATEController commonIDGATEController;

	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params)
	{
		// TODO 在營業時間 09:30~15:30 外, 只能執行預約交易
		// TODO LOG 寫入的機制
		Map<String, String> params = _params;

		// 判斷營業時間 ,若交易最後提交時間 > (營業截止時間 + 5分鐘) => 須顯示錯誤訊息(Z409)
		// B105 b105 = (B105)SpringBeanFactory.getBean("b105".toLowerCase());
		SYSPARAMDATA sysParam = (SYSPARAMDATA) b105.getSysParamData();

		int i_MM = Integer.parseInt(sysParam.getADFXEMM()) + 5;
		String str_BizEnd = sysParam.getADFXEHH() + String.valueOf(i_MM) + sysParam.getADFXESS();
		String str_CurrentTime = DateTimeUtils.getTimeShort(new Date());

		if (str_CurrentTime.compareTo(str_BizEnd) > 0)
		{
			throw TopMessageException.create("Z409", (String) params.get("ADOPID"));
		}

		/*** 驗證交易密碼 - 開始 ***/
		if (params.get("FGTXWAY") == null)
		{
			throw TopMessageException.create("S302", (String) params.get("ADOPID"));
		}
		//// 20190518 ian 改為n951
		else if ("0".equals(params.get("FGTXWAY")))
		{ // 使用交易密碼
			// String TransPassUpdate = (String)params.get("TRANSPASSUPDATE");
			// String Phase2 = (String)params.get("PHASE2");
			// TelCommExec n950CMTelcomm;
			//
			// if(StrUtils.isNotEmpty(TransPassUpdate))
			// {
			// if(TransPassUpdate.equals("1") || Phase2.equals("ON"))
			// {
			// n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n951CMTelcomm");
			// }
			// else
			// {
			// n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n950CMTelcomm");
			// }
			// }
			// else
			// {
			// n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n950CMTelcomm");
			// }
			// N950會發兩次所以需FGTXWAY=""，N950回應後再把FGTXWAY改成原值即可 2010.09.29
			// String fgtxway=params.get("FGTXWAY");
			// params.put("FGTXWAY", "");
			// MVHImpl n950Result = n950CMTelcomm.query(params);
			MVHImpl n951Result = n951CMTelcomm.query(params);
			// 若驗證失敗, 則會發動 TopMessageException
			// params.put("FGTXWAY", fgtxway);

			// F003 電文需要
			params.put("PINKEYFX", StrUtils.repeat("F2", 8));
		}else if ("7".equals(params.get("FGTXWAY"))) {
			//驗證裝置交易認證
			DoIDGateCommand_In in = new DoIDGateCommand_In();
			in.setIN_DATAS(params);
			HashMap rtnMap = commonIDGATEController.doIDGateCommand(in);
			if (!(boolean) rtnMap.get("result")) {
				log.error("N072 BOOKING CHECK IDGateCommand() ERROR");
				switch ((String) rtnMap.get("errMsg")) {
				case "Z300":
					throw TopMessageException.create("Z300");
				case "FE0011":
					throw TopMessageException.create("FE0011");
				default:
					throw TopMessageException.create("ZX99");
				}
			}

		}

		/*** 驗證交易密碼 - 結束 ***/

		MVHImpl result = new MVHImpl();
		result = online(params);
		if (result == null)
			result = new MVHImpl();

		return result;
	}

	public MVHImpl online(Map<String, String> _params)
	{

		// 轉給 f003controller 去做 online 的動作
		OnLineTransferNotice trNotice = new OnLineTransferNotice();
		MVHImpl result = f003controller.online(_params, trNotice);

		return result;
	}

	public TelcommResult doF001(Map<String, String> params)
	{
		// TODO 組合F001電文所需要的欄位
		Date d = new Date();

		params.put("TERM_DATE", DateTimeUtils.format("yyyyMMdd", d));
		params.put("TERM_TIME", DateTimeUtils.format("HHmmss", d));
		params.put("PCODE", "000D");
		params.put("COUNTRY", params.get("CNTRY"));
		params.put("DEALNO", StrUtils.trim(params.get("BGROENO")));

		/**
		 * 外幣交易取序號 不能重複 中心會擋 N : 舊各網 C : 企網 H :
		 */
		if (StrUtils.isEmpty(params.get("STAN"))) {
			params.put("STAN", sysDailySeqDao.getFXStan("F002T"));
		}

		params.put("SEQNO", "00000");

		params.put("CUSNM1", params.get("ORDNAME")); // 付款人名稱
		params.put("BENID", params.get("CUSTID")); // 收款人統編
		params.put("BENNM1", params.get("BENNAME")); // 收款人名稱

		String str_CUSTYPE = (String) params.get("CUSTYPE"); // 付款人身份別
		String str_BENTYPE = (String) params.get("BENTYPE"); // 受款人身分別
		params.put("CUSTYPE", str_CUSTYPE); // 付款人客戶身分別
		params.put("BENTYPE", str_BENTYPE); // 受款人身分別

		params.put("PAYREMIT", "1");// 金額類別(轉出)
		params.put("PAYCCY", params.get("TXCCY"));// 轉出幣別(匯入幣別)
		params.put("REMITCY", params.get("RETCCY"));// 轉入幣別(解款幣別)

		params.put("CURAMT", NumericUtil.formatNumberString(params.get("TXAMT"), 2).replaceAll(",", ""));// 轉出金額(匯入金額)

		// params.put("OUT_AMT", params.get("TXAMT"));//轉入金額 => 保留原始輸入欄位值

		params.put("RELREFNO", params.get("REFNO"));// 匯入匯款交易編號
		params.put("PAYDATE", DateTimeUtils.format("yyyyMMdd", d)); // 付款日期
		params.put("CUSTACC", params.get("ORDACC")); // 轉出帳號

		/*
		 * 如果電文失敗或寫DB發生任何錯誤, 則發 F007的電文 但是 FLAG 內容為 C 的電文
		 */
		TelcommResult result = f001Telcomm.query(params); // 議價電文

		return result;
	}

	public TelcommResult doF003(MVHImpl resultF001, Map<String, String> params)
	{
		// FROM F001
		log.debug("doF003 resultF001", resultF001.getFlatValues());
		log.debug(ESAPIUtil.vaildLog("doF003 params"+CodeUtil.toJson(params)));
		params.put("TXBRH", resultF001.getFlatValues().get("TXBRH")); // 承作行
		params.put("CUSTPAY_ID", params.get("UID")); // 付款人統一編號
		// params.put("ACCTID", params.get("CUSTACC")); //F001 付款帳號
		params.put("ACCTTYPE", ""); // 付款人帳戶類別
		params.put("ACCCUR", resultF001.getFlatValues().get("ORGCCY")); // 付款人轉出幣別
		params.put("ACCAMT", resultF001.getFlatValues().get("ORGAMT")); // 轉出金額

		params.put("CURCODE", resultF001.getFlatValues().get("PMTCCY")); // 付款人轉入幣別
		params.put("CURAMT", resultF001.getFlatValues().get("PMTAMT")); // 轉入金額

		/*** F001 電文回應欄位 SWTKIND (SWIFT電文發送方式) ***/
		params.put("SWTKIND", resultF001.getFlatValues().get("SWTKIND")); // 轉入金額

		/*** 紀錄F001之轉出轉入相關資料 ***/
		params.put("F001_ACCCUR", resultF001.getFlatValues().get("ORGCCY")); // 付款人轉出幣別
		params.put("F001_ACCAMT", resultF001.getFlatValues().get("ORGAMT")); // 轉出金額

		params.put("F001_CURCODE", resultF001.getFlatValues().get("PMTCCY")); // 付款人轉入幣別
		params.put("F001_CURAMT", resultF001.getFlatValues().get("PMTAMT")); // 轉入金額

		if (params.get("OUT_CRY") == null)
		{
			params.put("OUT_CRY", params.get("ACCCUR")); // 轉出幣別
			params.put("IN_CRY", params.get("CURCODE")); // 轉入幣別
		}

		params.put("BANK_TYPE", ""); // 銀行代號類別
		params.put("PRCDT", params.get("PAYDATE")); // 付款日期

		//
		// 付款人外匯議價號碼,左靠右補空白
		// (13-20) 8 碼為 交易密碼 MAC KEY SYNC
		// (21-36) 16 碼為 USERID
		//
		// 同幣別不會有 "BGROENO", 所以帶空白
		params.put("REFID", StrUtils.left(StrUtils.trim(params.get("BGROENO")) + StrUtils.repeat(" ", 12), 12));
		params.put("SYNC", StrUtils.trim(params.get("SYNC"))); // 8 碼
		params.put("USERID", StrUtils.left(params.get("USERID") + StrUtils.repeat(" ", 16), 16)); // 議價編號

		params.put("PAYEE_ID", StrUtils.trim(params.get("BENID")));// 收款人統一編號

		// params.put("ACCTIDTO", StrUtils.trim(params.get("BENACC"))); //收款人帳號
		params.put("ACCTIDTO", StrUtils.trim(params.get("BENACC")) + StrUtils.repeat(" ", 9)
				+ resultF001.getFlatValues().get("REFNO"));

		params.put("ACCTTP_TO", ""); // 收款人帳戶類別

		// params.put("FEEACCTID", params.get("COMMACC")); //付款人手續費帳號

		params.put("FEEACT_TP", ""); // 付款人手續費帳戶類別

		// params.put("FEEACT_CRY", params.get("COMMCCY")); //付款手續費帳戶幣別

		params.put("FEEBANK_TP", ""); // 手續費銀行代號類別

		// params.put("TNSATION", resultF001.getFlatValues().get("ORGCOMM")); //匯出匯款手續費金額
		// params.put("OTHERFEE", resultF001.getFlatValues().get("ORGCABLE")); //郵電費

		params.put("TNSATION", resultF001.getFlatValues().get("COMMAMT")); // 匯出匯款手續費金額
		params.put("OTHERFEE", resultF001.getFlatValues().get("CABCHG")); // 郵電費

		params.put("DISFEE", resultF001.getFlatValues().get("COMMAMT")); // 折扣後手續費
		params.put("DISTOTH", resultF001.getFlatValues().get("CABCHG")); // 折扣後郵電費
		// params.put("RESENDFLAG", ""); //重送標記

		String str_First4Byte = "    ";
		String str_Flag = " ";

		params.put("ACWTYPE", str_First4Byte + "            " + str_Flag); // 收款人帳戶類別

		params.put("COMMCCY2", resultF001.getFlatValues().get("COMMCCY2")); // 入帳手續費幣別
		params.put("COMMAMT2", resultF001.getFlatValues().get("COMMAMT2")); // 入帳手續費金額
		params.put("SWFTCODE", resultF001.getFlatValues().get("M1SBBIC")); // 收款人 SWIFT CD
		params.put("BACH_MARK", "N"); // 整批註記
		params.put("TXPSW", params.get("PINNEW")); // 押密後交易密碼

		// 壓 F003 的 MAC 欄位
		f003MAC(params);

		TelcommResult result = f003Telcomm.query(params); //

		return result;
	}

	public TelcommResult doF005(Map<String, String> params)
	{
		// 組合F005電文所需要的欄位

		params.put("PCODE", "000D");
		params.put("MSGID", "");

		// params.put("CUSTYPE", "1"); //TODO 之後會從 session 來
		// params.put("CUSTACC", params.get("CUSTACC")); //轉出帳號
		// params.put("BENACC", params.get("__TRIN_ACN")); //轉入帳號

		params.put("AMTTYPE", "1");// 金額類別
		params.put("CURAMT", NumericUtil.formatNumberString(params.get("TXAMT"), 2).replaceAll(",", "")); // 轉出(解款)金額

		/*
		 * 如果電文失敗或寫DB發生任何錯誤, 則發 F007的電文 但是 FLAG 內容為 C 的電文
		 */

		TelcommResult result = f005Telcomm.query(params); //

		return result;
	}

	public TelcommResult doF007C(Map<String, String> params)
	{
		// TODO 組合議價電文所需要的欄位

		params.put("FYACN", ""); // 轉出帳號
		params.put("INACN", params.get("BENACC")); // 轉入(解款)帳號
		params.put("OUT_CRY", params.get("TXCCY")); // 轉出幣別
		params.put("IN_CRY", params.get("RETCCY")); // 轉入(解款)幣別
		params.put("AMTTYPE", "1");// 金額類別
		params.put("CURAMT", NumericUtil.formatNumberString(params.get("TXAMT"), 2).replaceAll(",", "")); // 轉出(解款)金額
		params.put("FLAG", "C"); // 交易狀態

		/*
		 * 如果電文失敗或寫DB發生任何錯誤, 則發 F007的電文 但是 FLAG 內容為 C 的電文
		 */

		TelcommResult result = f007Telcomm.query(params); // 議價電文

		return result;
	}

	public TelcommResult doF011(Map<String, String> params)
	{
		// TODO 組合F011電文所需要的欄位

		params.put("AMTTYPE", "1");// 金額類別
		params.put("CURAMT", NumericUtil.formatNumberString(params.get("TXAMT"), 2).replaceAll(",", "")); // 轉出(解款)金額
		params.put("REMTYPE", params.get("BENTYPE")); // 國外匯款人身分別

		/*
		 * 如果電文失敗或寫DB發生任何錯誤, 則發 F007的電文 但是 FLAG 內容為 C 的電文
		 */

		TelcommResult result = f011Telcomm.query(params);

		return result;
	}

	public TelcommResult doMerva(Map<String, String> params, MVHImpl f001Result)
	{
		// TODO 組合 MERVA 電文所需要的欄位
		Date d = new Date();
		params.put("TIA-TERM-DATE", DateTimeUtils.format("yyyyMMdd", d));
		params.put("TIA-TERM-TIME", DateTimeUtils.format("HHmmss", d));

		log.debug(ESAPIUtil.vaildLog("@@@ F003T params == " + CodeUtil.toJson(params)));

		// TIA_TLID 需依照規則另外做判定, 如果 M1SWIFT 內容最後三碼為"XXX", 則填入"REM"
		// 若 M1SWIFT 內容最後三碼為"893", 則填入承做行(TXBRH)內容, 又, 如果承做行內容為"041", 則填入"REM"
        // 其餘狀況一律取 M1SWIFT 之最後三碼填入     
	    //START SWIFT GPI增加判斷
		String M1SWIFT = "";
		String str_TLID = "";
		if("3".equals(f001Result.getValueByFieldName("SWTKIND")))
			M1SWIFT = f001Result.getValueByFieldName("M1SWIFT");
		else
		{
			String fxstatus = ""; //匯 款狀態
			fxstatus = "P".equals(ms_env) ? "MBBTTWTP" : "MBBTTWT0";
					
//    		String ip = IPUtils.getLocalIp();
//    		if(ip.equals("172.22.11.30"))
//    		{	
//    			fxstatus="MBBTTWT0";
//    		}
//    		if(ip.equals("10.16.21.16"))
//    		{	
//    			fxstatus="MBBTTWTP";
//    		}
//    		if(ip.equals("10.16.21.17"))
//    		{
//    			fxstatus="MBBTTWTP";
//    		}
    		
    		M1SWIFT=fxstatus+"XXX";			
		}
		//str_TLID = f001Result.getValueByFieldName("M1SWIFT").substring(8);	
		str_TLID = M1SWIFT.substring(8);
	    //END SWIFT GPI增加判斷
		if (str_TLID.equals("XXX"))
		{
			params.put("BRH-CODE", "XXX");
			params.put("TLID", "REM");
		}
		else if (str_TLID.equals("893"))
		{
			params.put("BRH-CODE", "893");

			if ((f001Result.getValueByFieldName("TXBRH")).equals("041"))
			{
				params.put("TLID", "REM");
			}
			else
			{
				params.put("TLID", f001Result.getValueByFieldName("TXBRH"));
			}
		}
		else
		{
			params.put("BRH-CODE", f001Result.getValueByFieldName("M1SWIFT").substring(8));
			params.put("TLID", f001Result.getValueByFieldName("M1SWIFT").substring(8));
		}

		params.put("WS-ID", f001Result.getValueByFieldName("STAN"));

		// FOO1 SWTKIND
		// 3 時, 送 MT191
		StringBuffer FIDATA = new StringBuffer();
		if ("3".equals(f001Result.getValueByFieldName("SWTKIND")))
		{
			MT191 mt191 = new MT191();
			mt191.setF001Result(f001Result);
			String mt191Data = mt191.createMsg();
			FIDATA.append(mt191Data);
		}
		//SWIFT GPI 匯入匯款解款 MT199都需要發送
		f001Result.getFlatValues().put("DETCHG", params.get("DETCHG").toString());
		MT199 mt199 = new MT199(ms_env);
		mt199.setF001Result(f001Result);
		String mt199Data = mt199.createMsg();
		FIDATA.append(mt199Data);

		// 長度要送
		params.put("DATALENGTH", FIDATA.length() + "");
		params.put("FIDATA", FIDATA.toString());

		TelcommResult result = mervaTelcomm.query(params);

		return result;
	}

	/**
	 * 有關FENB-003 電文押碼: 將以下資料押碼 1. 付款帳號 ACCTID 2. 付款人統一編號 CUSTPAY_ID 3. 付款人日期 PRCDT 4. 交易序號 STAN 25 '0' + 付款帳號 (11) : 36
	 * '000' + 付款人統一編號 (8) : 11 ==> ID 押碼遇字母轉為數字 A->1, B->2 , J->0, K->1... 付款人日期 : 8 '0' + 交易序號 (8) : 9
	 * -------------------------------------------------------- 64
	 * 
	 * @param params
	 */
	public static void f003MAC(Map<String, String> params)
	{
		String acctid = StrUtils.repeat("0", 36);
		log.debug("acctid length : " + acctid.length());
		String uid = params.get("CUSTPAY_ID");
		Pattern p = Pattern.compile("[A-Z]", Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);

		Map<String, String> mv = new HashMap();
		mv.put("uid", uid);
		mv.put("stan", params.get("STAN")); // STAN 的格式為 N00001

		// 押碼遇字母轉為數字 A->1, B->2 , J->0, K->1...
		for (String key : mv.keySet())
		{
			String value = mv.get(key);

			Matcher m = p.matcher(value);
			StringBuffer resultString = new StringBuffer();
			while (m.find())
			{
				String eng = m.group().toLowerCase();
				int x = (eng.charAt(0) - 0x61 + 1) % 10;
				m.appendReplacement(resultString, x + "");
			}
			m.appendTail(resultString);

			mv.put(key, resultString.toString());
			// if("stan".equals(key))

			// Heuristic CGI Stored XSS
			// System.out.printf("%s-> old: %s, new: %s\r\n", key, value, resultString.toString());
		}
		// 統編有可能有7碼, 所以右補0
		String custpay_id = StrUtils.repeat("0", 3) + StrUtils.left(mv.get("uid") + "000000000", 8);
		String stan = "0" + mv.get("stan"); // 9 碼

		String prcdt = params.get("PRCDT"); // 8 碼
		log.debug("acctid len: " + acctid.length());
		log.debug("custpay_id len: " + custpay_id.length());
		log.debug("prcdt len: " + prcdt.length());
		log.debug("stan len: " + stan.length());

		String __mac64 = acctid + custpay_id + prcdt + stan;
		log.debug("F003 MAC 64 LENGTH:" + __mac64.length());
		log.debug(ESAPIUtil.vaildLog("F003 MAC: "+ __mac64));
		if (__mac64.length() != 64)
			throw new RuntimeException("錯誤的長度.");
		/*
		 * KeyClient client = new KeyClient(); String result = KeyClientUtils.doRequest( IKeySpi.CREATE_MAC,
		 * KeyLabel.KEYLABEL_MACKEY, client.toEBCDIC(StrUtils.left(StrUtils.right(DateTimeUtils.getCDateShort(new
		 * Date()), 6) + "00000000", 8).getBytes()), client.toEBCDIC(__mac64.getBytes()), "0128");
		 * 
		 * if(!StrUtils.trim(result).startsWith("ERROR")) { try { params.put("MAC", result.substring(0, 8));
		 * params.put("MAC_SYNC", params.get("SYNC")); //4 碼 } catch(RuntimeException e) { throw
		 * TopMessageException.create("Z300"); //押解碼錯誤 } }
		 */
		String result1 = "";
		try
		{
			result1 = VaCheck.getSYNC();
			if (result1 != null && result1.length() > 0)
			{
				try
				{
					log.debug("SYNC = " + result1.substring(0, 8));
					params.put("SYNC", result1.substring(0, 8));
				}
				catch (RuntimeException e)
				{
					throw TopMessageException.create("Z300"); // 押解碼錯誤
				}
			}
			else
			{
				throw TopMessageException.create("Z300"); // 押解碼錯誤

			}
		}
		catch (Exception e)
		{
			log.debug("getSYNC ERROR:" + e.getMessage());
		}
		String result = "";
		try
		{
			result = VaCheck.getMAC(__mac64);
			if (result != null && result.length() > 0)
			{
				try
				{
					log.debug("MAC = " + result.substring(0, 8));
					params.put("MAC", result.substring(0, 8));
					params.put("MAC_SYNC", params.get("SYNC")); // 4 碼
				}
				catch (RuntimeException e)
				{
					log.debug("getMAC ERROR: result substring error");
					throw TopMessageException.create("Z300"); // 押解碼錯誤
				}
			}
			else
			{
				log.debug("getMAC ERROR: result size null or zero");
				throw TopMessageException.create("Z300"); // 押解碼錯誤
			}
		}
		catch (Exception e)
		{
			log.debug("getMAC ERROR:" + e.getMessage());
			throw TopMessageException.create("Z300");
		}
	}

	public TelcommResult doF007Y(Map _params)
	{
		Map<String, String> params = _params;

		params.put("FYACN", ""); // 轉出帳號
		params.put("INACN", params.get("BENACC")); // 轉入(解款)帳號
		params.put("OUT_CRY", params.get("TXCCY")); // 轉出幣別
		params.put("IN_CRY", params.get("RETCCY")); // 轉入(解款)幣別
		params.put("AMTTYPE", "1");// 金額類別
		params.put("CURAMT", NumericUtil.formatNumberString(params.get("TXAMT"), 2).replaceAll(",", "")); // 轉出(解款)金額
		params.put("FLAG", "Y");// 交易狀態

		/*
		 * 如果電文失敗或寫DB發生任何錯誤, 則發 F007的電文 但是 FLAG 內容為 C 的電文
		 */
		TelcommResult result = f007Telcomm.query(params);

		return result;
	}

	public boolean checkSuccessTopMsg(String topmsg)
	{
		// 若回傳為 000000, 或者為 空白, 則回傳 true
		try
		{
			Double d = Double.parseDouble(topmsg);
			if (d == 0d)
				return true;
		}
		catch (Exception e)
		{
		}
		return "".equals(topmsg);
	}

	public Object writeLog(final TelCommExecWrapper execwrapper, final Map _params, final MVHImpl result)
	{
		Map<String, String> params = _params;
		DecimalFormat dft = new DecimalFormat("#0");
		DecimalFormat dftdot2 = new DecimalFormat("#0.00");

		// 建立交易紀錄log 並檢查是否需更新預約檔
		TXNFXRECORD record = execwrapper.createTxnFxRecordCheckupdatePayData(params, txnFxSchPayDataDao);


		log.debug(ESAPIUtil.vaildLog("writeLog params >>>>{}"+CodeUtil.toJson(params)));
		record.setADOPID("F003");
		record.setFXUSERID(params.get("UID"));
		record.setFXWDAC(params.get("ORDACC")); // 轉出帳號
		record.setFXSVBH(params.get("TXBRH")); // 轉入分行
		record.setFXSVAC(params.get("BENACC")); // 轉入帳號
		record.setFXWDCURR(params.get("TXCCY")); // 轉出幣別
		record.setFXSVCURR(params.get("RETCCY")); // 轉入幣別
		record.setFXTXMEMO(params.get("REFNO")); // 借用此欄位,紀錄匯入編號

		if (params.get("F001_ACCCUR") != null)
		{

			try
			{
				record.setFXWDAMT(dftdot2.format(dft.parse(params.get("F001_ACCAMT")).doubleValue() / 100d)); // 轉出金額
				record.setFXSVAMT(dftdot2.format(dft.parse(params.get("F001_CURAMT")).doubleValue() / 100d)); // 轉入金額
			}
			catch (Exception e)
			{
			}

		}
		else
		{

			double d_Div = 1d;
			if (((String) params.get("CURAMT")).startsWith("0"))
				d_Div = 100d;

			try
			{
				record.setFXWDAMT(dftdot2.format(dft.parse(params.get("CURAMT")).doubleValue() / d_Div)); // 轉出金額
			}
			catch (Exception e)
			{
			}

			if ("".equals(params.get("REMITAMT")))
			{
				record.setFXSVAMT(record.getFXWDAMT()); // 轉出金額
			}
			else
			{
				d_Div = 1d;
				if (((String) params.get("REMITAMT")).startsWith("0"))
					d_Div = 100d;

				try
				{
					record.setFXSVAMT(dftdot2.format(dft.parse(params.get("REMITAMT")).doubleValue() / d_Div)); // 轉入金額
				}
				catch (Exception e)
				{
				}
			}

		}

		log.debug(ESAPIUtil.vaildLog("@@@ F003T result.getFlatValues() getFXWDAMT == " + record.getFXWDAMT()));
		log.debug(ESAPIUtil.vaildLog("@@@ F003T result.getFlatValues() getFXSVAMT == " + record.getFXSVAMT()));

		record.setFXEXRATE(StrUtils.trim(params.get("RATE")));// 匯率

		try
		{
			String tmp1 = (params.get("COMMAMT2") == null ? ""
					: NumericUtil.formatNumberString(params.get("COMMAMT2"), 2));
			String tmp2 = (params.get("DISTOTH") == null ? ""
					: NumericUtil.formatNumberString(params.get("DISTOTH"), 2));
			record.setFXEFEE(tmp1 + "/" + tmp2);// 手續費 + 郵電費

			record.setFXEFEECCY((params.get("COMMCCY2") == null) ? "" : params.get("COMMCCY2")); // 手續費幣別
		}
		catch (Exception e)
		{
		}

		record.setFXTXCODE(params.get("FGTXWAY"));

		record.setFXMSGSEQNO(StrUtils.trim(params.get("__FXMSGSEQNO")));

		/*** New Add ***/
		record.setFXMSGCONTENT((params.get("__FXMSGCONTENT") == null) ? "" : params.get("__FXMSGCONTENT"));

		// 20190522 edit by ian 改成不寫入 TXNREQINFO 只寫入TXNFXRECORD

		// 欄位長度過長資料庫塞不進去，故remove
		params.remove("pkcs7Sign");
		params.remove("jsondc");
		params.remove("__SIGN");
		params.remove("__F001_RESULT");
		params.remove("__F003_RESULT");
		record.setFXTITAINFO(CodeUtil.toJson(params));
		log.debug(ESAPIUtil.vaildLog("F003T record >>>>>>>>>>>>> " + record.toString()));
		txnFxRecordDao.save(record);
		return record;
	}

	/**
	 * 檢查電文回應之訊息代碼:
	 * 
	 * 1.若存在於資料庫 && 註記可人工重送("Y") ==> 進人工重送處理 2.其它 ==> 一律交易結束
	 */
	public boolean checkTopMsg(String topmsg)
	{

		boolean isFail = false;

		String fxFlag = admMsgCodeDao.findFXRESEND(topmsg);
		String fxAutoFlag = admMsgCodeDao.findFXAUTORESEND(topmsg);

		if (TopMessageException.isInDB(topmsg) && (fxFlag.equals("Y") || fxAutoFlag.equals("Y")))
		{
			isFail = false; // 進系統/人工重送處理
		}
		else
		{
			isFail = true; // 交易結束
		}

		log.debug("@@@ F003T.checkMervaTopMsg() isFail == " + (isFail ? "true" : "false"));

		return isFail;
	}

	/**
	 * 檢查 MERVA 回應之訊息代碼: 1.若存在於資料庫 && 註記可系統/人工重送("Y") ==> 進系統/人工重送處理 2.其它 ==> 一律作 F005 沖正
	 */
	public boolean checkMervaTopMsg(String topmsg)
	{

		boolean isFail = false;

		String fxFlag = admMsgCodeDao.findFXRESEND(topmsg);
		String fxAutoFlag = admMsgCodeDao.findFXAUTORESEND(topmsg);

		if (TopMessageException.isInDB(topmsg) && (fxFlag.equals("Y") || fxAutoFlag.equals("Y")))
		{
			isFail = false; // 進系統/人工重送處理
		}
		else
		{
			isFail = true; // 作 F005 沖正
		}

		log.debug("@@@ F003T.checkMervaTopMsg() isFail == " + (isFail ? "true" : "false"));

		return isFail;
	}

	/**
	 * 檢查電文回應之訊息代碼:
	 * 
	 * 1.若存在於資料庫 && 註記可自動重送("Y") ==> 進自動重送處理 2.若存在於資料庫 && 註記可人工重送("Y") ==> 進人工重送處理 3.其它 ==> 一律交易結束
	 */
	public boolean checkTopMsg(String topmsg, String schFlag)
	{

		boolean isFail = false;

		String fxFlag = admMsgCodeDao.findFXRESEND(topmsg);
		String fxAutoFlag = admMsgCodeDao.findFXAUTORESEND(topmsg);

		log.debug(ESAPIUtil.vaildLog("@@@ F003T.checkTopMsg() schFlag == " + schFlag));

		// 即時交易
		if (schFlag == null)
		{
			if (TopMessageException.isInDB(topmsg) && fxFlag.equals("Y"))
			{
				isFail = false; // 進人工重送處理
			}
			else
			{
				isFail = true; // 交易結束
			}
		}
		// 預約交易
		else
		{
			if (TopMessageException.isInDB(topmsg) && fxAutoFlag.equals("Y"))
			{
				isFail = false; // 進自動重送處理
			}
			else
			{
				isFail = true; // 交易結束
			}
		}

		return isFail;
	}
}
