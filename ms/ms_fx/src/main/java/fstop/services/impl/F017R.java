package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;
import topibs.utility.AmountUtil;
import topibs.utility.NumericUtil;

/**
 * 外匯匯出匯款-即時
 * @author Owner
 *
 */
@Slf4j
public class F017R extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("f007Telcomm")
	private TelCommExec f007Telcomm;
		
//	@Required
//	public void setF007Telcomm(TelCommExec telcomm) {
//		f007Telcomm = telcomm;
//	}

	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params) { 
		Map<String, String> params = _params;
		//TODO 組合議價電文所需要的欄位
		Date d = new Date();
		params.put("CMTRDATE", DateTimeUtils.format("yyyyMMdd", d));
		params.put("CMTRTIME", DateTimeUtils.format("HHmmss", d));
		
		params.put("FYACN", params.get("FYACN")); //轉出帳號
		params.put("INACN", params.get("TSFAN")); //轉入帳號
		params.put("OUT_CRY", params.get("OUT_CRY")); //轉出幣別
		params.put("IN_CRY", params.get("IN_CRY")); //轉入幣別
		//if("1".equals(params.get("PAYREMIT"))) {
			params.put("AMTTYPE", "1");//金額類別
			String outamt=  "";
			try {
				outamt=AmountUtil.deleteComma(NumericUtil.formatNumberString(params.get("PAIAFTX"),2));
				params.put("CURAMT", outamt);//轉出金額 (議價金額)
				log.warn(ESAPIUtil.vaildLog("F017R.java CURAMT:"+params.get("CURAMT")));
			}
			catch(Exception e) {
				log.warn("F017R.java CURAMT deleteComma failure", e);
			}	
		//}
		/*
		else { // "2".equals(params.get("PAYREMIT"))
			params.put("AMTTYPE", "2");//金額類別
			params.put("ATRAMT", params.get("CURAMT"));//轉入金額
		}
		*/		
		params.put("FLAG", "R");//交易狀態
		/*
	 		如果電文失敗或寫DB發生任何錯誤, 則發 F007的電文
	 		但是  FLAG 內容為 C 的電文
		*/	
		
		params.put("BGROENO", "");
		
		TopMessageException lastException = null;		
		TelcommResult result = null;  //議價電文
		
		try {
			result = f007Telcomm.query(params);  //議價電文
		}
		catch(Exception e) {
			if(e instanceof TopMessageException) {
				lastException = (TopMessageException)e;
				throw lastException;
			}		
		}
		
		if(! checkSuccessTopMsg(result.getFlatValues().get("MSG_CODE"))) {
			throw new TopMessageException(result.getFlatValues().get("MSG_CODE"), "", "");			
		}		
		

		params.put("BGROENO", result.getValueByFieldName("BGROENO"));  //議價編號
		params.put("RATE", result.getValueByFieldName("RATE"));  //匯率

		double dd;
		String strCURAMT="";
		try {
			strCURAMT=AmountUtil.deleteComma(params.get("CURAMT"));
		}
		catch(Exception e) {
			log.warn("F017R.java CURAMT deleteComma failure", e);
		}			
		//if("1".equals(params.get("PAYREMIT"))) {   //轉出

				if ("TWD".equals(params.get("OUT_CRY")))
					dd = new Double(strCURAMT) / new Double(params.get("RATE"));					
				else	
					dd = new Double(strCURAMT) * new Double(params.get("RATE"));
				
				result.getFlatValues().put("CURAMT", params.get("CURAMT"));  // 轉出金額
				result.getFlatValues().put("ATRAMT", dd + "");  // 轉入金額
				result.getFlatValues().put("FYACN",params.get("FYACN"));  // 轉出帳號
				result.getFlatValues().put("OUT_CRY",params.get("OUT_CRY"));  // 轉出幣別
				result.getFlatValues().put("INACN",params.get("INACN"));  // 轉入帳號
				result.getFlatValues().put("IN_CRY",params.get("IN_CRY"));  // 轉入幣別
				result.getFlatValues().put("CMTRDATE",params.get("CMTRDATE"));  //交易日期
				result.getFlatValues().put("SRCFUND",params.get("SRCFUND")); //匯款分類
				result.getFlatValues().put("NAME",params.get("NAME")); //使用者姓名
				result.getFlatValues().put("CUSTYPE",params.get("CUSTYPE")); //客戶身分別
				
			
		//}
		/*
		else { // "2".equals(params.get("PAYREMIT"))   //轉入
			
				if ("TWD".equals(params.get("OUT_CRY")))
					dd = new Double(params.get("ATRAMT")) * new Double(params.get("RATE"));			
				else	
					dd = new Double(params.get("ATRAMT")) / new Double(params.get("RATE"));		
						
				result.getFlatValues().put("CURAMT", dd + "");  // 轉出金額
				result.getFlatValues().put("ATRAMT", params.get("ATRAMT"));  // 轉入金額			
		}
		*/
		return result;
	}
		
	private boolean checkSuccessTopMsg(String topmsg) {
		//若回傳為 000000, 或者為 空白, 則回傳 true
		try {
			Double d = Double.parseDouble(topmsg);
			if(d == 0d)
				return true;
		}
		catch(Exception e) {}
		return "".equals(topmsg);
	}	
	
}
