package fstop.services.batch;

public class BatchCounter {
	private int successCount = 0;
	
	private int failCount = 0;
	
	private int totalCount = 0;
	
	public void incSuccess() {
		successCount++;
	}
	
	public void incFail() {
		failCount++;
	}
	
	public void incTotalCount() {
		totalCount++;
	}

	public int getFailCount() {
		return failCount;
	}

	public void setFailCount(int failCount) {
		this.failCount = failCount;
	}

	public int getSuccessCount() {
		return successCount;
	}

	public void setSuccessCount(int successCount) {
		this.successCount = successCount;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
	
}
