package fstop.services.batch;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fstop.exception.TopMessageException;
import fstop.notifier.NotifyAngent;
import fstop.notifier.NotifyListener;
import fstop.orm.dao.AdmMailLogDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.ADMMAILCONTENT;
import fstop.orm.po.ADMMAILLOG;
import fstop.orm.po.TXNRECMAILLOGRELATION;
import fstop.orm.po.TXNUSER;
import fstop.services.NoticeInfo;
import fstop.services.NoticeInfoGetter;
import fstop.services.TransferNotice;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class ScheduleTransferNoticeFx implements TransferNotice
{

	@Autowired
	private AdmMailLogDao admMailLogDao;

	@Autowired
	private TxnUserDao txnUserDao;

	public void sendNotice(NoticeInfoGetter infogetter)
	{
		try
		{
			String dpmyemail = "";
			boolean isUserSetTxnScheduleNotify = false;
			log.debug("ScheduleTransferNoticeFx sendNotice");
			final NoticeInfo info = infogetter.getNoticeInfo();

			String _mailList = StrUtils.trim(info.getReceiver()).replaceAll(";", ",");
			log.debug("info.getReceiver().length() = " + info.getReceiver().length() + " ,info.getReceiver() = "
					+ info.getReceiver());
			log.debug("info.getUserMail().length() = " + info.getUserMail().length() + " ,info.getUserMail() = "
					+ info.getUserMail());
			if ((info.getUserMail().length() == 0 || info.getUserMail().length() != 0)
					&& info.getReceiver().length() == 0)
			{
				TXNUSER txnUser = txnUserDao.findById(info.getUid());
				String dpnotify = StrUtils.trim(txnUser.getDPNOTIFY());
				Set<String> dpnotifySet = StrUtils.splitToSet("\\s*,\\s*", dpnotify); // 1 : 轉帳/繳費稅/匯出匯款成功通知, 5
																						// 預約交易（轉帳繳費稅匯出匯款）結果通知

				boolean isUserSetTxnNotify = dpnotifySet.contains("1");
				isUserSetTxnScheduleNotify = dpnotifySet.contains("5");
				dpmyemail = StrUtils.trim(txnUser.getDPMYEMAIL());

				if (isUserSetTxnNotify == false && isUserSetTxnScheduleNotify == false)
				{// 若使用者有設定, 5 預約交易（轉帳繳費稅匯出匯款）結果通知 //若使用者有設定, 1 : 轉帳/繳費稅/匯出匯款成功通知
					return;
				}
				if (info.getUserMail().length() == 0)
					_mailList = dpmyemail;
				else _mailList = info.getUserMail().trim();
			}
			Set mailListSet = new HashSet();
			for (String s : _mailList.split(","))
			{
				s = StrUtils.trim(s);
				if (StrUtils.isNotEmpty(s))
					mailListSet.add(StrUtils.trim(s));
			}

			String templateName = info.getTemplateName();
			if (StrUtils.isEmpty(templateName))
				templateName = "transferNotify";
			log.debug("templateName = " + templateName);
			final String __record_adtxno = info.getADTXNO();

			final StringBuffer status = new StringBuffer();
			final StringBuffer subjectBuffer = new StringBuffer();
			final StringBuffer msgBuffer = new StringBuffer();

			// 轉帳成功
			if (info.isProcessSuccess())
			{
				/*
				 * //若使用者有設定, 5 預約交易（轉帳繳費稅匯出匯款）結果通知 if(isUserSetTxnScheduleNotify) { if(StrUtils.isNotEmpty(dpmyemail))
				 * mailListSet.add(dpmyemail); } //若使用者有設定, 1 : 轉帳/繳費稅/匯出匯款成功通知 if(isUserSetTxnNotify) {
				 * if(StrUtils.isNotEmpty(dpmyemail)) mailListSet.add(dpmyemail); }
				 */
				_mailList = StrUtils.implode(",", mailListSet);
				log.debug("_mailList = " + _mailList);
				NotifyListener listener = new NotifyListener()
				{

					public void fail(String reciver, String subject, String msg, Exception e)
					{
						status.append("N");
						subjectBuffer.append(subject);
						msgBuffer.append(msg);
					}

					public void success(String reciver, String subject, String msg)
					{
						status.append("Y");
						subjectBuffer.append(subject);
						msgBuffer.append(msg);
					}

				};

				info.getADMMAILLOG().setADMAILACNO(_mailList);
				boolean isSendSuccess = NotifyAngent.sendNotice(templateName, info.getParams(), _mailList, listener);
				if (!isSendSuccess)
				{
					log.error("轉帳成功:發送 Email 失敗.(" + _mailList + ")");
				}
				else
				{
					log.debug("轉帳成功:發送 Email 成功.(" + _mailList + ")");
				}
				writeSendNoticeLog(info, __record_adtxno, subjectBuffer.toString(), msgBuffer.toString(), status.toString());
			}

			// 轉帳失敗, 使用者設定 預約交易（轉帳繳費稅匯出匯款）結果通知
			// 971217 當Dpretxstatus==5 不需發MAIL
			log.debug("info.getDpretxstatus() = " + info.getDpretxstatus());
			log.debug("isResend = " + info.isResend());
			if (!info.isProcessSuccess() && isUserSetTxnScheduleNotify && !info.isResend())
			{
				// do nothing
				NotifyListener listener = new NotifyListener()
				{

					public void fail(String reciver, String subject, String msg, Exception e)
					{
						status.append("N");
						subjectBuffer.append(subject);
						msgBuffer.append(msg);
					}

					public void success(String reciver, String subject, String msg)
					{
						status.append("Y");
						subjectBuffer.append(subject);
						msgBuffer.append(msg);
					}

				};

				if (StrUtils.isNotEmpty(dpmyemail))
				{
					info.getADMMAILLOG().setADMAILACNO(dpmyemail);
					if (info.getException() != null)
					{
						if (info.getException() instanceof TopMessageException)
						{
							TopMessageException e = (TopMessageException) info.getException();
							info.getParams().put("ADMSGCODE", e.getMsgcode());
							info.getParams().put("ADMESSAGE", e.getMsgout());
						}
						else
						{
							info.getParams().put("ADMSGCODE", "ZX99");
							info.getParams().put("ADMESSAGE", info.getException().getMessage());
						}
					}
					boolean isSendSuccess = NotifyAngent.sendNotice(templateName
							+ "_fail", info.getParams(), dpmyemail, listener);
					if (!isSendSuccess)
					{
						log.error("轉帳失敗:發送 Email 失敗.(" + dpmyemail + ")");
					}
					else
					{
						log.debug("轉帳失敗:發送 Email 成功.(" + dpmyemail + ")");
					}
					writeSendNoticeLog(info, __record_adtxno, subjectBuffer.toString(), msgBuffer.toString(), status.toString());
				}
			}
		}
		catch (Exception e)
		{
			log.error("發送 Email 失敗.", e);
		}
	}

	public void writeSendNoticeLog(final NoticeInfo info, final String __record_adtxno, String subject, String msg,
			String status)
	{
		ADMMAILLOG maillog = info.getADMMAILLOG();
		maillog.setADSENDSTATUS(status);
		maillog.setADMAILCONTENT(msg);
		maillog.setADMSUBJECT(subject);


		Date d = new Date();
		TXNRECMAILLOGRELATION relation = new TXNRECMAILLOGRELATION();
		relation.setADTXNO(__record_adtxno);
		relation.setLASTDATE(DateTimeUtils.format("yyyyMMdd", d));
		relation.setLASTTIME(DateTimeUtils.format("HHmmss", d));

		try
		{
			admMailLogDao.writeRecordNotice(maillog, relation);
		}
		catch (Exception ex)
		{
			log.error("無法寫入 MAILLOG.", ex);
		}
	}
}
