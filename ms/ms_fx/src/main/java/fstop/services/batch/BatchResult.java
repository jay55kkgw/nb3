package fstop.services.batch;

import java.util.List;
import java.util.Map;

import lombok.Data;

@Data
public class BatchResult {
	public String batchName;
	
	public boolean isSuccess;

	public String errorCode;

	public Map<String, Object> data;
	

}

