package fstop.services.batch;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.joda.time.DateTime;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import encryptUtil.EncryptUtil;
import fstop.aop.advice.LoggerHelper;
import fstop.exception.TopMessageException;
import fstop.exception.UncheckedException;
import fstop.model.MVHImpl;
import fstop.orm.dao.AdmMsgCodeDao;
import fstop.orm.dao.TxnFxSchPayDao;
import fstop.orm.dao.TxnFxSchPayDataDao;
import fstop.orm.po.TXNFXSCHPAY;
import fstop.orm.po.TXNFXSCHPAYDATA;
import fstop.orm.po.TXNFXSCHPAYDATA_PK;
import fstop.services.BookingAware;
import fstop.services.TransferNotice;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 外匯預約轉帳
 *
 * @author Owner
 *
 */
@Slf4j
@RestController
@Batch(id = "batch.TxnFxTransfer", name = "外匯預約交易一扣", description = "外匯預約交易一扣")
public class TxnFxTransfer extends DispatchBatchExecute implements BatchExecute {

	@Autowired
	private ScheduleTransferNoticeFx scheduletransfernoticefx;// 發信

	@Autowired
	private TxnFxSchPayDataDao txnFxSchPayDataDao; // 預約資料

	@Autowired
	private TxnFxSchPayDao txnFxSchPayDao; // 預約主檔
	
	@Autowired
	AdmMsgCodeDao admMsgCodeDao;

	private DecimalFormat fmt = new DecimalFormat("#0");

	/**
	 * args[0] method name
	 */
	@RequestMapping(value = "/batch/txnfxtransfer", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	@Override
	public BatchResult execute(@RequestBody List<String> args) {
		List<String> safeArgs = ESAPIUtil.validStrList(args);

		log.info(ESAPIUtil.vaildLog("safeArgs: {}" + JSONUtils.toJson(safeArgs)));

		// 第一個為這個 bean 的 method Name
		String methodName = safeArgs.get(0);
		List paramList = new ArrayList();
		paramList.addAll(safeArgs);
		paramList.remove(0);
		log.info(ESAPIUtil.vaildLog("methodName: {}, param list: " + JSONUtils.toJson(paramList) + "," + methodName));
		return invoke(methodName, paramList);
	}

	/**
	 * fxschno 為執行的預約編號 <BR>
	 * FXSCHTXDATE 預約日期
	 *
	 * @return
	 */
	public BatchResult executeone(List<String> args) {
		log.debug("TxnFxTransfer executeone args>>{}", args);
		String FXSCHNO = args.get(0);
		String FXSCHTXDATE = args.get(1);
		String FXUSERID = args.get(2);
		boolean sendResult = false;
		try {
			sendResult = send(FXSCHNO, FXSCHTXDATE, FXUSERID);
		} catch (Exception e) {
			log.error("執行 Batch 有誤 !", e);
		}

		Map<String, Object> data = new HashMap();

		data.put("COUNTER", new BatchCounter());

		BatchResult result = new BatchResult();
		result.setSuccess(sendResult);
		result.setBatchName(getClass().getSimpleName());
		result.setData(data);

		return result;
	}

	/**
	 * 
	 * @param FXSCHNO     預約批號
	 * @param FXSCHTXDATE 預約轉帳日
	 * @param FXUSERID    使用者ID
	 * @return
	 */
	public boolean send(String FXSCHNO, String FXSCHTXDATE, String FXUSERID) {

		log.info("開始執行,send FXSCHNO: " + FXSCHNO + ", FXSCHTXDATE: " + FXSCHTXDATE + ","
				+ new DateTime().toString("yyyy/MM/dd HH:mm:ss"));
		// 組pk
		TXNFXSCHPAYDATA_PK pk = new TXNFXSCHPAYDATA_PK();
		pk.setFXSCHNO(FXSCHNO);
		pk.setFXSCHTXDATE(FXSCHTXDATE);
		pk.setFXUSERID(FXUSERID);

		// 依照PK找到副表
		TXNFXSCHPAYDATA txnFxSchPayData = txnFxSchPayDataDao.findById(pk);
		log.debug("send txnFxSchPayData {}", txnFxSchPayData.toString());
		String adopid = txnFxSchPayData.getADOPID();// 功能代號
		// 更新預約狀態
		updateStatus(txnFxSchPayData, "3", "");
		// 建立執行參數
		Map params = new HashMap<String, String>();
		//
		Map info = new HashMap();
		Optional<List<TXNFXSCHPAY>> osch = null;
		TXNFXSCHPAY schdata = null;
		String reqinfo = "";
		String reqinfo_addTXTime = "";
		String fxschtxdate_fxwdac_fxsvac = "";
		String digest1 ="";
		String digest2 ="";
		String mac ="";
		Boolean checkResult = true;
		try {
			osch = txnFxSchPayDao.findByFXSCHNO(txnFxSchPayData.getPks().getFXSCHNO(),adopid,FXUSERID);
			log.debug(ESAPIUtil.vaildLog("osch :  >>>>> " + osch));
			int size = osch.map(List::size).orElse(0);
			int position = 1;
			int getPosition = 0;
			
			switch (size) {
				case 0:
					log.error("未搜尋到對應的 TXNFXSCHPAY, FXSCHNO: " + txnFxSchPayData.getPks().getFXSCHNO());
					return false;
				case 1:
					schdata = osch.get().get(0);
					reqinfo = schdata.getFXTXINFO();
					mac = txnFxSchPayData.getMAC();
		            log.info(ESAPIUtil.vaildLog("ORG_MAC" + mac));
		            //Way1 reqinfo+DPSCHTXDATE
					reqinfo_addTXTime = reqinfo + txnFxSchPayData.getPks().getFXSCHTXDATE();
					digest1 = EncryptUtil.toEncS1(reqinfo_addTXTime, "UTF-8").substring(0,4);
					log.info(ESAPIUtil.vaildLog("WAY1_MAC" + digest1));
					
					//Way2 FXSCHTXDATE預約轉帳日 + FXWDAC轉出帳號 + FXSVAC轉入帳號
					fxschtxdate_fxwdac_fxsvac = txnFxSchPayData.getPks().getFXSCHTXDATE()+txnFxSchPayData.getFXWDAC()+txnFxSchPayData.getFXSVAC();
					digest2 = EncryptUtil.toEncS1(fxschtxdate_fxwdac_fxsvac, "UTF-8").substring(0,4);
					
					checkResult = digest1.equalsIgnoreCase(mac)?true:false;
					if(!checkResult) {
						log.info("MacCheck1 Failed");
						log.info("Start MacCheck2");
						checkResult = digest2.equalsIgnoreCase(mac)?true:false;
						if(!checkResult) {
							log.info("MacCheck2 Failed");
						}
					}
					getPosition = 0 ;
					break;
				default :
					log.warn("Find "+ size +" Rows Has Same FXSCHNO : "+ txnFxSchPayData.getPks().getFXSCHNO() 
							 + " , FXUSERID : "+ txnFxSchPayData.getPks().getFXUSERID() +" , ADOPID : "+ txnFxSchPayData.getADOPID());
					for(TXNFXSCHPAY eachData:osch.get()) {
						log.info("Check "+ position + " DATA's MAC");
						reqinfo = eachData.getFXTXINFO();
						
						mac = txnFxSchPayData.getMAC();
			            log.info(ESAPIUtil.vaildLog("ORG_MAC" + mac));
			            
			            //Way1 reqinfo+DPSCHTXDATE
						reqinfo_addTXTime = reqinfo + txnFxSchPayData.getPks().getFXSCHTXDATE();
						digest1 = EncryptUtil.toEncS1(reqinfo_addTXTime, "UTF-8").substring(0,4);
						log.info(ESAPIUtil.vaildLog("WAY1_MAC" + digest1));
						
						//Way2 FXSCHTXDATE預約轉帳日 + FXWDAC轉出帳號 + FXSVAC轉入帳號
						fxschtxdate_fxwdac_fxsvac = txnFxSchPayData.getPks().getFXSCHTXDATE()+txnFxSchPayData.getFXWDAC()+txnFxSchPayData.getFXSVAC();
						digest2 = EncryptUtil.toEncS1(fxschtxdate_fxwdac_fxsvac, "UTF-8").substring(0,4);
						
						checkResult = digest1.equalsIgnoreCase(mac)?true:false;
						if(!checkResult) {
							log.info("MacCheck1 Failed");
							log.info("Start MacCheck2");
							checkResult = digest2.equalsIgnoreCase(mac)?true:false;
							if(!checkResult) {
								log.info("MacCheck2 Failed");
							}
						}
						if(true==checkResult) {
							getPosition=position-1;
							schdata=osch.get().get(getPosition);
							break;
						}
						position++;
					}
					break;
			}
			
			if(!checkResult) {
       		 log.error("MAC檢核不相同(FXSCHNO: " + txnFxSchPayData.getPks().getFXSCHNO() + 
						  " FXSCHTXDATE:" + txnFxSchPayData.getPks().getFXSCHTXDATE() + 
						  " FXUSERID:" + txnFxSchPayData.getPks().getFXUSERID() + ")");
               updateStatus(txnFxSchPayData, "1", "MAC_FAIL");
            	return false;
			}
			
			Map mt = JSONUtils.json2map(reqinfo);
			
			if (!mt.containsKey("LOGINTYPE")) {
				mt.put("LOGINTYPE", osch.get().get(getPosition).getLOGINTYPE());
			}
			
			info.putAll(mt);
			
			log.info(ESAPIUtil.vaildLog("預約執行參數: " + reqinfo));
			
//			if (osch.isPresent()) {
//				schdata = osch.get();
//				String reqinfo = schdata.getFXTXINFO();
//				
//				String mac = txnFxSchPayData.getMAC();
//	            log.info(ESAPIUtil.vaildLog("ORG_MAC" + mac));
//				
//				//Way1 reqinfo+DPSCHTXDATE
//				reqinfo_addTXTime = reqinfo + txnFxSchPayData.getPks().getFXSCHTXDATE();
//				String digest1 = EncryptUtil.toEncS1(reqinfo_addTXTime, "UTF-8").substring(0,4);
//				log.info(ESAPIUtil.vaildLog("WAY1_MAC" + digest1));
//				
//                //Way2 FXSCHTXDATE預約轉帳日 + FXWDAC轉出帳號 + FXSVAC轉入帳號
//				newMacWay = txnFxSchPayData.getPks().getFXSCHTXDATE()+txnFxSchPayData.getFXWDAC()+txnFxSchPayData.getFXSVAC();
//                String digest2 = EncryptUtil.toEncS1(newMacWay, "UTF-8").substring(0,4);
//                log.info(ESAPIUtil.vaildLog("WAY2_MAC" + digest2));
//                
//                boolean check = false ;
//                //Way1 check if false, use Way2
//                check=mac.equalsIgnoreCase(digest1)?true:false;
//                //check MAC
//                if(check) {
//                	log.info("MacCheck1 OK");
//                	Map mt = JSONUtils.json2map(reqinfo);
//    				info.putAll(mt);
//    				log.info(ESAPIUtil.vaildLog("預約執行參數: " + reqinfo));
//                }else {
//                	log.info("MacCheck1 Failed");
//                	log.info("Start MacCheck2");
//                	 check=mac.equalsIgnoreCase(digest2)?true:false;
//                	 if(check) {
//                		log.info("MacCheck2 OK");
//                		 Map mt = JSONUtils.json2map(reqinfo);
//         				info.putAll(mt);
//         				log.info(ESAPIUtil.vaildLog("預約執行參數: " + reqinfo));
//                	 }else {
//                		 log.info("MacCheck2 Failed");
//                		 log.error("MAC檢核不相同(FXSCHNO: " + txnFxSchPayData.getPks().getFXSCHNO() + 
//       						  " FXSCHTXDATE:" + txnFxSchPayData.getPks().getFXSCHTXDATE() + 
//       						  " FXUSERID:" + txnFxSchPayData.getPks().getFXUSERID() + ")");
//                        updateStatus(txnFxSchPayData, "1", "MAC_FAIL");
//                     	return false;
//                	 }
//                }
//			} else {
//				log.info("未搜尋到對應的 TXNFXSCHPAY, FXSCHNO: " + txnFxSchPayData.getPks().getFXSCHNO());
//			}
		} catch (Exception e) {
			throw new RuntimeException("組合上傳的 tita 錯誤.", e);
		}

		params.putAll(info);
		// 交易序號
		params.put("FXSCHNO", FXSCHNO);
		// 時間
		params.put("FXSCHTXDATE", FXSCHTXDATE);
		
		//預約將密碼相關都清空
		log.info("CHANGE TO @@@@@@");
		params.put("CMPASSWORD", "@@@@@@");
		params.put("PINNEW", "@@@@@@");
		params.put("__PINNEW", "@@@@@@");
		if ("0".equals(params.get("FGTXWAY"))) { // 交易密碼
			params.put("PINKEY", StrUtils.repeat("F2", 8));
			params.put("PINKEYFX", StrUtils.repeat("F2", 8));
		} else if ("1".equals(params.get("FGTXWAY"))) { // ikey
			params.put("XMLCA", txnFxSchPayData.getXMLCA());
			params.put("XMLCN", txnFxSchPayData.getXMLCN());
			// 20130905因為使用IKEY做預約轉及時會發生錯誤所以把params.put("CMPASSWORD", "@@@@@@");改成空字串
			params.put("CMPASSWORD", "");
			params.put("PINNEW", "");
			params.put("__PINNEW", "");
		}

		params.put("CUSIDN", txnFxSchPayData.getPks().getFXUSERID());
		params.put("ADOPID", txnFxSchPayData.getADOPID());
		params.put("TXID", txnFxSchPayData.getADOPID());
		params.put("PAYDATE", DateTimeUtils.format("yyyyMMdd", new Date()));
		// 避免CERTACN 經過MAC2.java 時會被覆蓋 此參數必帶
		params.put("__SCHID", fmt.format(schdata.getFXSCHID()));
		// 一扣參數
		params.put("__TRANS_STATUS", "SEND");
		params.put("__EXECWAY", "BATCH");
		params.put("ADREQTYPE", ""); // 寫入 TXNLOG 時, 空值 表示為即時, 需計入 ADMMONTHREPORT 中

		// 控制 TXNLOG
		try {
			params = txnFxSchPayDataDao.upTxnlogparams(adopid, params);
			params.put("UID", StrUtils.trim((String) params.get("CUSIDN")));
			LoggerHelper.current().setAdopid(adopid);
			LoggerHelper.current().setUid(StrUtils.trim((String) params.get("CUSIDN")));
			LoggerHelper.current().setRequestData(JSONUtils.map2json(params));
		} catch (Exception e) {
			log.error("控制 TXNLOG 發生錯誤", e);
		}
		// 更新預約狀態
		updateStatus(txnFxSchPayData, "3", "");
		boolean isSuccess = false;
		try {
			isSuccess = reproduceTransaction(adopid, params, scheduletransfernoticefx, txnFxSchPayData);
			LoggerHelper.clear();
		} catch (Exception e) {
			log.error("發生錯誤.", e);
		}

		if (isSuccess) {
			updateStatus(txnFxSchPayData, "0", "");
		} else {
			// 已在rereproduceTransaction save過 移除
		}

		log.info(ESAPIUtil.vaildLog("執行結果: " + params.get("__SCHID") + ", " + (isSuccess ? "成功" : "失敗") + ", "
				+ new DateTime().toString("yyyy/MM/dd HH:mm:ss")));

		return isSuccess;
	}

	/**
	 * 
	 * 0 成功 <BR>
	 * 1 失敗 <BR>
	 * 2 其他(處理中)<BR>
	 * ‘’未執行 (預設)
	 * 
	 * @param schedule
	 * @param status
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateStatus(TXNFXSCHPAYDATA txnFxSchPayData, String status, String excode) {
		txnFxSchPayDataDao.updateStatus(txnFxSchPayData, status, excode);
	}

	/**
	 * 重新發送預約交易, 使用 service bean
	 * 
	 * @param adopid          交易代碼
	 * @param params          交易參數
	 * @param trNotice
	 * @param txnFxSchPayData 預約交易資料
	 * @return
	 */
	public boolean reproduceTransaction(String adopid, Map params, TransferNotice trNotice,
			TXNFXSCHPAYDATA txnFxSchPayData) {

		boolean result = true;
		BookingAware service = null;
		MVHImpl mvhimpl = null;
		String tita = "";
		String tota = "";

		log.debug("@@@@@@@@@@@@@ fx reproduceTransaction adopid :{}", adopid);
		try {
			if ("F001".equalsIgnoreCase(adopid)) {
				service = (BookingAware) SpringBeanFactory.getBean("f001controllerService");

			} else if ("F002".equalsIgnoreCase(adopid)) {
				service = (BookingAware) SpringBeanFactory.getBean("f002controllerService");

			} else if ("N175".equalsIgnoreCase(adopid)) {
				service = (BookingAware) SpringBeanFactory.getBean("n175_2SService");

			} else {
				service = (BookingAware) SpringBeanFactory.getBean(adopid.toLowerCase() + "Service");
			}
			log.debug("@@@@@@@@@@@@@ fx reproduceTransaction service >>>>>>>> {}", service);
			log.info(ESAPIUtil
					.vaildLog("@@@@@@@@@@@@@ fx reproduceTransaction params >>>>>>>> {} " + JSONUtils.toJson(params)));
			tita = CodeUtil.toJson(params);
			log.debug(ESAPIUtil.vaildLog("@@@@@@@@@@@@@ fx reproduceTransaction tita >>>>>>>> {} " + tita));
			MVHImpl mvh = service.online(params, trNotice);
			mvhimpl = (MVHImpl) mvh;
			Map<String, String> mvhimplmap = mvhimpl.getFlatValues();
			log.debug("@@@@@@@@@@@@@@ fx reproduceTransaction mvhimplmap >>>>>>>> {}", mvhimplmap);
			// 更新預約交易結果
			txnFxSchPayData = txnFxSchPayDataDao.upRetunToTxnfxschpaydata(txnFxSchPayData, mvhimplmap);
			tota = CodeUtil.toJson(mvhimplmap);
			log.debug("@@@@@@@@@@@@@@ fx reproduceTransaction tota >>>>>>>> {} ", tota);
			result = true;
		} catch (ClassCastException e) {
			log.error("對應的BEAN不為BookingAware.", e);
		} catch (NoSuchBeanDefinitionException e) {
			log.error("沒有對應的 BEAN.", e);
		} catch (Exception e) {
			// 控制 TXNLOG
			try {
				Throwable e_root = getRootCause(e);
				if (e_root instanceof TopMessageException) {
					log.error(ESAPIUtil.vaildLog("外匯轉帳失敗. (FXSCHID: " + params.get("__SCHID") + "), " + ((TopMessageException) e_root).getMsgcode()));
					String errorCode = StrUtils.trim(((TopMessageException) e_root).getMsgcode());
					String autoResendfx = admMsgCodeDao.findFXAUTORESEND(errorCode);
					//如果能自動重送,狀態為2(處理中),不能則為1(失敗)
                	if("Y".equalsIgnoreCase(autoResendfx)) {
                		 updateStatus(txnFxSchPayData, "2", errorCode);
                	}else if("N".equalsIgnoreCase(autoResendfx)){
                		 updateStatus(txnFxSchPayData, "1", errorCode);
                	}else if("".equalsIgnoreCase(autoResendfx)) { //資料庫找不到此錯誤代碼 先用FE0001
                		log.info("Can't find this error in Database !!!!");
                		updateStatus(txnFxSchPayData, "1", "FE0001");
                	}
					LoggerHelper.current().setExceptionCode(StrUtils.trim(((TopMessageException) e_root).getMsgcode()));
				
				} else {
					log.error(ESAPIUtil.vaildLog("Program Error >> "),e);
					updateStatus(txnFxSchPayData, "1", "ZX99");
					LoggerHelper.current().setExceptionCode("ZX99");
				}
			} catch (Exception ex) {
			}
			// *** 控制TXNLOG
			result = false;
		} finally {
			txnFxSchPayData.setFXTITAINFO(tita);
//			txnFxSchPayData.setFXTOTAINFO(tota);
			txnFxSchPayData.setFXTXDATE(DateTimeUtils.format("yyyyMMdd", new Date()));
			txnFxSchPayData.setFXTXTIME(DateTimeUtils.format("HHmmss", new Date()));
			log.debug(ESAPIUtil.vaildLog("@@@@@@@@@@@@@@ fx reproduceTransaction txnFxSchPayData >>>>>>>>>>>>> {}"
					+ txnFxSchPayData.toString()));
			txnFxSchPayDataDao.save(txnFxSchPayData);
		}
		return result;
	}

	public Throwable getRootCause(Throwable cause) {
		if (cause instanceof UncheckedException) {
			Throwable rootCause = ((UncheckedException) cause).getRootCause();
			Throwable result = null;
			result = (rootCause != null ? rootCause : cause);

			return result;
		} else {
			return (cause.getCause() != null ? cause.getCause() : cause);
		}
	}
}
