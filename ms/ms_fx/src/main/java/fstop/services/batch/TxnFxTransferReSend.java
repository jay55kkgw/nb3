package fstop.services.batch;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import encryptUtil.EncryptUtil;
import fstop.aop.advice.LoggerHelper;
import fstop.exception.TopMessageException;
import fstop.exception.UncheckedException;
import fstop.model.MVHImpl;
import fstop.orm.dao.TxnFxSchPayDao;
import fstop.orm.dao.TxnFxSchPayDataDao;
import fstop.orm.po.TXNFXSCHPAY;
import fstop.orm.po.TXNFXSCHPAYDATA;
import fstop.orm.po.TXNFXSCHPAYDATA_PK;
import fstop.services.BookingAware;
import fstop.services.TransferNotice;
import fstop.services.batch.annotation.Batch;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 外匯預約轉帳 2扣
 *
 * @author Owner
 *
 */

@Slf4j
@RestController
@Batch(id = "batch.TxnFxTransferReSend", name = "外匯預約交易二扣", description = "外匯預約交易二扣")
public class TxnFxTransferReSend extends DispatchBatchExecute implements BatchExecute {

	@Autowired
	private ScheduleTransferNoticeFx scheduletransfernoticefx;// 發信

	@Autowired
	private TxnFxSchPayDataDao txnFxSchPayDataDao; // 預約資料

	@Autowired
	private TxnFxSchPayDao txnFxSchPayDao; // 預約主檔

	private DecimalFormat fmt = new DecimalFormat("#0");

	/**
	 * args[0] method name
	 */
	@RequestMapping(value = "/batch/txnfxtransferresend", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	@Override
	public BatchResult execute(@RequestBody List<String> args) {
		List<String> safeArgs = ESAPIUtil.validStrList(args);

		log.info(ESAPIUtil.vaildLog("safeArgs: {}" + JSONUtils.toJson(safeArgs)));

		// 第一個為這個 bean 的 method Name
		String methodName = safeArgs.get(0);
		List paramList = new ArrayList();
		paramList.addAll(safeArgs);
		paramList.remove(0);
		log.info(ESAPIUtil.vaildLog("methodName: {}, param list: " + JSONUtils.toJson(paramList) + "," + methodName));
		return invoke(methodName, paramList);
	}

	/**
	 * fxschno 為執行的預約編號 <BR>
	 * FXSCHTXDATE 預約日期 type
	 *
	 * @return
	 */
	public BatchResult executeone(List<String> args) {
		log.debug("executeone args>>{}", args);
		String FXSCHNO = args.get(0);
		String FXSCHTXDATE = args.get(1);
		String FXUSERID = args.get(2);
		String type = args.get(3);
		boolean sendResult = false;
		try {
			sendResult = send(FXSCHNO, FXSCHTXDATE, FXUSERID, type);
		} catch (Exception e) {
			log.error("執行 Batch 有誤 !", e);
		}

		Map<String, Object> data = new HashMap();

		data.put("COUNTER", new BatchCounter());

		BatchResult result = new BatchResult();
		result.setSuccess(sendResult);
		result.setBatchName(getClass().getSimpleName());
		result.setData(data);

		return result;
	}

	/**
	 * 
	 * @param FXSCHNO
	 * @param FXSCHTXDATE
	 * @param tx_Type
	 * @return
	 */
	public boolean send(String FXSCHNO, String FXSCHTXDATE, String FXUSERID, String tx_Type) {

		Date startTime = new Date();
		log.info("開始執行, FXSCHNO: " + FXSCHNO + ", FXSCHTXDATE: " + FXSCHTXDATE + ","
				+ DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", startTime));

		TXNFXSCHPAYDATA_PK pk = new TXNFXSCHPAYDATA_PK();
		pk.setFXSCHNO(FXSCHNO);
		pk.setFXSCHTXDATE(FXSCHTXDATE);
		pk.setFXUSERID(FXUSERID);

		TXNFXSCHPAYDATA txnFxSchPayData = txnFxSchPayDataDao.findById(pk);

		String adopid = txnFxSchPayData.getADOPID();

		updateStatus(txnFxSchPayData, "3", "");

		Map params = new HashMap<String, String>();

		Map info = new HashMap();
		Optional<List<TXNFXSCHPAY>> osch = null;
		TXNFXSCHPAY schdata = null;
		String reqinfo = "";
		String reqinfo_addTXTime = "";
		String fxschtxdate_fxwdac_fxsvac = "";
		String digest1 ="";
		String digest2 ="";
		String mac ="";
		Boolean checkResult = true;
		try {
			osch = txnFxSchPayDao.findByFXSCHNO(txnFxSchPayData.getPks().getFXSCHNO(),adopid,FXUSERID);
			log.debug(ESAPIUtil.vaildLog("osch :  >>>>> " + osch));
			int size = osch.map(List::size).orElse(0);
			int position = 1;
			int getPosition = 0;
			
			switch (size) {
				case 0:
					log.error("未搜尋到對應的 TXNFXSCHPAY, FXSCHNO: " + txnFxSchPayData.getPks().getFXSCHNO());
					return false;
				case 1:
					schdata = osch.get().get(0);
					reqinfo = schdata.getFXTXINFO();
					mac = txnFxSchPayData.getMAC();
		            log.info(ESAPIUtil.vaildLog("ORG_MAC" + mac));
		            //Way1 reqinfo+DPSCHTXDATE
					reqinfo_addTXTime = reqinfo + txnFxSchPayData.getPks().getFXSCHTXDATE();
					digest1 = EncryptUtil.toEncS1(reqinfo_addTXTime, "UTF-8").substring(0,4);
					log.info(ESAPIUtil.vaildLog("WAY1_MAC" + digest1));
					
					//Way2 FXSCHTXDATE預約轉帳日 + FXWDAC轉出帳號 + FXSVAC轉入帳號
					fxschtxdate_fxwdac_fxsvac = txnFxSchPayData.getPks().getFXSCHTXDATE()+txnFxSchPayData.getFXWDAC()+txnFxSchPayData.getFXSVAC();
					digest2 = EncryptUtil.toEncS1(fxschtxdate_fxwdac_fxsvac, "UTF-8").substring(0,4);
					
					checkResult = digest1.equalsIgnoreCase(mac)?true:false;
					if(!checkResult) {
						log.info("MacCheck1 Failed");
						log.info("Start MacCheck2");
						checkResult = digest2.equalsIgnoreCase(mac)?true:false;
						if(!checkResult) {
							log.info("MacCheck2 Failed");
						}
					}
					getPosition = 0 ;
					break;
				default :
					log.warn("Find "+ size +" Rows Has Same FXSCHNO : "+ txnFxSchPayData.getPks().getFXSCHNO() 
							 + " , FXUSERID : "+ txnFxSchPayData.getPks().getFXUSERID() +" , ADOPID : "+ txnFxSchPayData.getADOPID());
					for(TXNFXSCHPAY eachData:osch.get()) {
						log.info("Check "+ position + " DATA's MAC");
						reqinfo = eachData.getFXTXINFO();
						
						mac = txnFxSchPayData.getMAC();
			            log.info(ESAPIUtil.vaildLog("ORG_MAC" + mac));
			            
			            //Way1 reqinfo+DPSCHTXDATE
						reqinfo_addTXTime = reqinfo + txnFxSchPayData.getPks().getFXSCHTXDATE();
						digest1 = EncryptUtil.toEncS1(reqinfo_addTXTime, "UTF-8").substring(0,4);
						log.info(ESAPIUtil.vaildLog("WAY1_MAC" + digest1));
						
						//Way2 FXSCHTXDATE預約轉帳日 + FXWDAC轉出帳號 + FXSVAC轉入帳號
						fxschtxdate_fxwdac_fxsvac = txnFxSchPayData.getPks().getFXSCHTXDATE()+txnFxSchPayData.getFXWDAC()+txnFxSchPayData.getFXSVAC();
						digest2 = EncryptUtil.toEncS1(fxschtxdate_fxwdac_fxsvac, "UTF-8").substring(0,4);
						
						checkResult = digest1.equalsIgnoreCase(mac)?true:false;
						if(!checkResult) {
							log.info("MacCheck1 Failed");
							log.info("Start MacCheck2");
							checkResult = digest2.equalsIgnoreCase(mac)?true:false;
							if(!checkResult) {
								log.info("MacCheck2 Failed");
							}
						}
						if(true==checkResult) {
							getPosition=position-1;
							schdata=osch.get().get(getPosition);
							break;
						}
						position++;
					}
					break;
			}
			
			if(!checkResult) {
       		 log.error("MAC檢核不相同(FXSCHNO: " + txnFxSchPayData.getPks().getFXSCHNO() + 
						  " FXSCHTXDATE:" + txnFxSchPayData.getPks().getFXSCHTXDATE() + 
						  " FXUSERID:" + txnFxSchPayData.getPks().getFXUSERID() + ")");
               updateStatus(txnFxSchPayData, "1", "MAC_FAIL");
            	return false;
			}
			
			Map mt = JSONUtils.json2map(reqinfo);
			
			if (!mt.containsKey("LOGINTYPE")) {
				mt.put("LOGINTYPE", osch.get().get(getPosition).getLOGINTYPE());
			}
			
			info.putAll(mt);
			
			log.info(ESAPIUtil.vaildLog("預約執行參數: " + reqinfo));
			
//			if (osch.isPresent()) {
//				schdata = osch.get();
//				String reqinfo = schdata.getFXTXINFO();
//				
//				String mac = txnFxSchPayData.getMAC();
//	            log.info(ESAPIUtil.vaildLog("ORG_MAC:" + mac));
//				
//				//Way1 reqinfo+DPSCHTXDATE
//				reqinfo_addTXTime = reqinfo + txnFxSchPayData.getPks().getFXSCHTXDATE();
//				String digest1 = EncryptUtil.toEncS1(reqinfo_addTXTime, "UTF-8").substring(0,4);
//				log.info(ESAPIUtil.vaildLog("WAY1_MAC:" + digest1));
//				
//                //Way2 FXSCHTXDATE預約轉帳日 + FXWDAC轉出帳號 + FXSVAC轉入帳號
//				newMacWay = txnFxSchPayData.getPks().getFXSCHTXDATE()+txnFxSchPayData.getFXWDAC()+txnFxSchPayData.getFXSVAC();
//                String digest2 = EncryptUtil.toEncS1(newMacWay, "UTF-8").substring(0,4);
//                log.info(ESAPIUtil.vaildLog("WAY2_MAC:" + digest2));
//                
//                boolean check = false ;
//                //Way1 check if false, use Way2
//                check=mac.equalsIgnoreCase(digest1)?true:false;
//                //check MAC
//                if(check) {
//                	log.info("MacCheck1 OK");
//                	Map mt = JSONUtils.json2map(reqinfo);
//    				info.putAll(mt);
//    				log.info(ESAPIUtil.vaildLog("預約執行參數: " + reqinfo));
//                }else {
//                	log.info("MacCheck1 Failed");
//                	log.info("Start MacCheck2");
//                	 check=mac.equalsIgnoreCase(digest2)?true:false;
//                	 if(check) {
//                		log.info("MacCheck2 OK");
//                		 Map mt = JSONUtils.json2map(reqinfo);
//         				info.putAll(mt);
//         				log.info(ESAPIUtil.vaildLog("預約執行參數: " + reqinfo));
//                	 }else {
//                		 log.info("MacCheck2 Failed");
//                		 log.error("MAC檢核不相同(FXSCHNO: " + txnFxSchPayData.getPks().getFXSCHNO() + 
//       						  " FXSCHTXDATE:" + txnFxSchPayData.getPks().getFXSCHTXDATE() + 
//       						  " FXUSERID:" + txnFxSchPayData.getPks().getFXUSERID() + ")");
//                        updateStatus(txnFxSchPayData, "1", "MAC_FAIL");
//                     	return false;
//                	 }
//                }
//			} else {
//				log.info("未搜尋到對應的 TXNFXSCHPAY, FXSCHNO: " + txnFxSchPayData.getPks().getFXSCHNO());
//			}
		} catch (Exception e) {
			throw new RuntimeException("組合上傳的 tita 錯誤.", e);
		}

		params.putAll(info);
		// 交易序號
		params.put("FXSCHNO", FXSCHNO);
		// 時間
		params.put("FXSCHTXDATE", FXSCHTXDATE);

		// 20130905因為使用IKEY做預約轉及時會發生錯誤所以把params.put("CMPASSWORD", "@@@@@@");改成空字串
		log.info("CHANGE TO @@@@@@");
		params.put("CMPASSWORD", "@@@@@@");
		params.put("PINNEW", "@@@@@@");
		params.put("__PINNEW", "@@@@@@");
		if ("0".equals(params.get("FGTXWAY"))) { // 交易密碼
			params.put("PINKEY", StrUtils.repeat("F2", 8));
			params.put("PINKEYFX", StrUtils.repeat("F2", 8));
		} else if ("1".equals(params.get("FGTXWAY"))) { // ikey
			params.put("XMLCA", txnFxSchPayData.getXMLCA());
			params.put("XMLCN", txnFxSchPayData.getXMLCN());
			// 20130905因為使用IKEY做預約轉及時會發生錯誤所以把params.put("CMPASSWORD", "@@@@@@");改成空字串
			params.put("CMPASSWORD", "");
		}

		params.put("CUSIDN", txnFxSchPayData.getPks().getFXUSERID());
		params.put("ADOPID", txnFxSchPayData.getADOPID());
		params.put("TXID", txnFxSchPayData.getADOPID());
		params.put("PAYDATE", DateTimeUtils.format("yyyyMMdd", new Date()));

		/*** 重送相關欄位處理 START ***/
		/*
		 * 只有重送時,要指定以下flag,表人工介入 =>
		 *
		 * F001: MSGID = 'RESEND' F003: RESENDFLAG = 'M'
		 */
		params.put("MSGID", "RESEND"); // F001
		params.put("RESENDFLAG", ("AUTO".equals(tx_Type) ? "Y" : "M")); // F003
		params.put("STAN", txnFxSchPayData.getADTXNO()); // 當重發F001時,須取出原交易的STAN進行重送

		/*** 重送相關欄位處理 END ***/
		// 避免CERTACN 經過MAC2.java 時會被覆蓋 此參數必帶
		params.put("__SCHID", fmt.format(schdata.getFXSCHID()));
//		params.put("__REQID", txnFxSchPayData.getFXTITAINFO());
		params.put("__TRANS_RESEND_ADTXNO", txnFxSchPayData.getADTXNO());

		params.put("__FXMSGSEQNO", txnFxSchPayData.getFXMSGSEQNO());
		params.put("ADREQTYPE", ""); // 寫入 TXNLOG 時, 空值 表示為即時, 需計入 ADMMONTHREPORT 中
		//二扣參數
		params.put("__TRANS_STATUS", "RESEND"); // 表示是預約轉即時, 還是重送
		params.put("__EXECWAY", "BATCH");
		params.put("ADREQTYPE", ""); // 寫入 TXNLOG 時, 空值 表示為即時, 需計入 ADMMONTHREPORT 中

		// 控制 TXNLOG
		try {
			params = txnFxSchPayDataDao.upTxnlogparams(adopid, params);
			LoggerHelper.current().setAdopid(adopid);
			LoggerHelper.current().setUid(StrUtils.trim((String) params.get("CUSIDN")));
			LoggerHelper.current().setRequestData(JSONUtils.map2json(params));
		} catch (Exception e) {
			log.error("發生錯誤.", e);
		}
		updateStatus(txnFxSchPayData, "3", "");

		boolean isSuccess = false;
		try {
			isSuccess = reproduceTransaction(adopid, params, scheduletransfernoticefx, txnFxSchPayData);
			LoggerHelper.clear();
		} catch (Exception e) {
			log.error("TxnFxTransferReSend send 發生錯誤.",e, e);
		}

		if (isSuccess) {
			updateStatus(txnFxSchPayData, "0", "");
		} else {
			// 已在rereproduceTransaction save過 移除
		}

		Date endTime = new Date();
		log.info(ESAPIUtil.vaildLog("執行結果: " + params.get("__SCHID") + ", " + (isSuccess ? "成功" : "失敗") + ", "
				+ DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", endTime)));
//		if (StrUtils.isNotEmpty(fxfdate) || StrUtils.isNotEmpty(fxtdate) && fxfdate.equals(fxtdate)) {
//			log.debug("生效日 ==截止日  更新 txnFxSchPayData Status = 5 ");
//
//			updateStatus(txnFxSchPayData, "5", "");
//		}

		return isSuccess;
	}

	/**
	 * 
	 * 0 成功 <BR>
	 * 1 失敗 <BR>
	 * 2 其他(處理中)<BR>
	 * ‘’未執行 (預設)
	 * 
	 * @param schedule
	 * @param status
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateStatus(TXNFXSCHPAYDATA schedule, String status, String excode) {
		txnFxSchPayDataDao.updateStatus(schedule, status, excode);
	}

	public boolean reproduceTransaction(String adopid, Map params, TransferNotice trNotice,
			TXNFXSCHPAYDATA txnFxSchPayData) {
		boolean result = true;
		BookingAware service = null;
		MVHImpl mvhimpl = null;
		String tita = "";
		String tota = "";
		log.debug("adopid :{}", adopid);
		try {
			if ("F001".equalsIgnoreCase(adopid)) {
				service = (BookingAware) SpringBeanFactory.getBean("f001controllerService");

			} else if ("F002".equalsIgnoreCase(adopid)) {
				service = (BookingAware) SpringBeanFactory.getBean("f002controllerService");

			} else if ("F003".equalsIgnoreCase(adopid)) {
				service = (BookingAware) SpringBeanFactory.getBean("f003controllerService");

			} else if ("N175".equalsIgnoreCase(adopid)) {
				service = (BookingAware) SpringBeanFactory.getBean("n175_2SService");

			} else {
				service = (BookingAware) SpringBeanFactory.getBean(adopid.toLowerCase() + "Service");
			}
			// 移除F005沖正失敗記號
			params.remove("F005FAILFLAG");
			log.info(ESAPIUtil.vaildLog("@@@@@@@@@@@@@@ reproduceTransaction params: {} " + JSONUtils.toJson(params)));
			
			tita = CodeUtil.toJson(params);
			log.info(ESAPIUtil.vaildLog("@@@@@@@@@@@@@ fx reproduceTransaction tita >>>>>>>> " + tita));
			log.debug("@@@@@@@@@@@@@ fx service >>>>>>>> {}", service);
			MVHImpl mvh = service.online(params, trNotice);
			mvhimpl = (MVHImpl) mvh;
			Map<String, String> mvhimplmap = mvhimpl.getFlatValues();
			log.debug("@@@@@@@@@@@@@@fx reproduceTransaction mvhimplmap >>> {}", mvhimplmap);
			// 下行
			tota = CodeUtil.toJson(mvh.getFlatValues());
			log.debug("@@@@@@@@@@@@@@ fx reproduceTransaction tota >>> ", tota);
			// 更新預約交易結果
			txnFxSchPayData = txnFxSchPayDataDao.upRetunToTxnfxschpaydata(txnFxSchPayData, mvhimplmap);
			result = true;
		} catch (ClassCastException e) {
			log.error("對應的BEAN不為BookingAware.", e);
		} catch (NoSuchBeanDefinitionException e) {
			log.error("沒有對應的 BEAN.", e);
		} catch (Exception e) {
			log.error(ESAPIUtil.vaildLog("外匯轉帳二扣失敗. (FXSCHID: " + params.get("__SCHID") + "), " + e.getMessage()), e);
			updateStatus(txnFxSchPayData, "1", null == e.getMessage() ? "NULL" : e.getMessage());
			// 控制 TXNLOG
			try {
				Throwable e_root = getRootCause(e);
				if (e_root instanceof TopMessageException) {
					LoggerHelper.current().setExceptionCode(StrUtils.trim(((TopMessageException) e_root).getMsgcode()));
				} else {
					LoggerHelper.current().setExceptionCode("ZX99");
				}
			} catch (Exception ex) {
			}
			// *** 控制TXNLOG
			result = false;
		} finally {
			txnFxSchPayData.setFXTITAINFO(tita);
//			txnFxSchPayData.setFXTOTAINFO(tota);
			txnFxSchPayData.setFXTXDATE(DateTimeUtils.format("yyyyMMdd", new Date()));
			txnFxSchPayData.setFXTXTIME(DateTimeUtils.format("HHmmss", new Date()));
			log.debug(ESAPIUtil.vaildLog("@@@@@@@@@@@@@@ txnFxSchPayData >>>>>>>>>>>>> "+ txnFxSchPayData.toString()));
			txnFxSchPayDataDao.save(txnFxSchPayData);
		}
		return result;
	}

	public Throwable getRootCause(Throwable cause) {
		if (cause instanceof UncheckedException) {
			Throwable rootCause = ((UncheckedException) cause).getRootCause();
			Throwable result = null;
			result = (rootCause != null ? rootCause : cause);

			return result;
		} else {
			return (cause.getCause() != null ? cause.getCause() : cause);
		}
	}

}
