package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.services.impl.N174;
import lombok.extern.slf4j.Slf4j;

/**
 * 主要用來
 * 1.丟入MVH前的前置處理，包含檢核;
 * 2.後製MVH回覆的訊息;
 * @author Hugo
 *
 */
@Service
@Slf4j
public class N174_Tel_Service extends Common_Tel_Service {
	
	@Autowired
	N174 n174;
	
	@Override
	public HashMap pre_Processing(Map request) {
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) {
		MVH mvh = new MVHImpl();
		HashMap result = new HashMap();
		try
		{
			mvh = n174.doAction(request);
			result = data_Retouch(mvh);
		}
		catch (TopMessageException e)
		{
			log.error("", e);
			result.put("msgCode",e.getMsgcode());
			result.put("msgName", getMessageByMsgCode(String.valueOf(result.get("msgCode"))));
		}
		return result;
	}

	@Override
	public HashMap data_Retouch(MVH mvh) {
		MVHImpl mvhimpl = null;
		Boolean result = Boolean.FALSE;
		HashMap<String,Object> dataMap = new HashMap<String,Object>();
    	try {
    		mvhimpl = (MVHImpl)mvh;
    		
    		String MSGCOD = mvhimpl.getFlatValues().get("MSGCOD");
    		log.debug("MSGCOD={}",MSGCOD);
    		
    		mvhimpl.getFlatValues().put("TOPMSG",MSGCOD);
    		
			dataMap.putAll(mvhimpl.getFlatValues());
			result = Boolean.TRUE;
			
		} catch (Exception e) {
			log.error("data_Retouch error >> {}",e);
		}finally {
			setMsgCode(dataMap,result);
		}
		return dataMap;
	}

	@Override
	public void setMsgCode(Map dataMap, Boolean data_Retouch_result) {
		super.setMsgCode(dataMap, data_Retouch_result);
	}

	
	
	
	
}
