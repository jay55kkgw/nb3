package com.netbank.rest.service;

import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
@ConditionalOnProperty(name = "quartz.enabled")
public class SchedulerService {

    @Autowired
    SchedulerFactoryBean schedulerFactoryBean;

    /**
     * 取得某一個JOB
     * @param jobName
     * @return
     * @throws SchedulerException
     */
    public JobDetail getJob(JobKey jobName) throws SchedulerException {

        Scheduler scheduler = schedulerFactoryBean.getScheduler();
        return scheduler.getJobDetail(jobName);

    }

    /**
     * 暫停某一個JOB
     * @param jobKey
     * @throws SchedulerException
     */
    public void pauseJob(JobKey jobKey) throws SchedulerException {
        Scheduler scheduler = schedulerFactoryBean.getScheduler();
        scheduler.pauseJob(jobKey);
    }

    /**
     * 繼續某一個JOB
     * @param jobKey
     * @throws SchedulerException
     */
    public void resumeJob(JobKey jobKey) throws SchedulerException {
        Scheduler scheduler = schedulerFactoryBean.getScheduler();
        scheduler.resumeJob(jobKey);
    }

    /**
     * 繼續某一個JOB
     * @param jobKey
     * @throws SchedulerException
     */
    public boolean deleteJob(JobKey jobKey) throws SchedulerException {
        Scheduler scheduler = schedulerFactoryBean.getScheduler();
        return scheduler.deleteJob(jobKey);
    }

    /**
     * trigger某一個JOB
     * @param jobKey
     * @throws SchedulerException
     */
    public void triggerJob(JobKey jobKey) throws SchedulerException {
        Scheduler scheduler = schedulerFactoryBean.getScheduler();
        scheduler.triggerJob(jobKey);
    }

    /**
     * 判斷某一個JOB是否正在執行
     * @param jobKey
     * @throws SchedulerException
     */
    public boolean checkJobInExceute(JobKey jobKey) throws SchedulerException {
        Scheduler scheduler = schedulerFactoryBean.getScheduler();

        List<JobExecutionContext> jobs = scheduler.getCurrentlyExecutingJobs();
        for (JobExecutionContext job : jobs) {

            JobDetail detail = job.getJobDetail();

            if (Objects.equals(jobKey.getName(), detail.getKey().getName()) && Objects.equals(jobKey.getGroup(), detail.getKey().getGroup())) {
                return true;
            }

        }

        return false;
    }

}
