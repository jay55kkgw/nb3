package com.netbank.rest.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.netbank.util.StrUtils;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.services.CommonService;
import fstop.util.SpringBeanFactory;
import lombok.extern.slf4j.Slf4j;

/**
 * 主要用來 1.丟入MVH前的前置處理，包含檢核; 2.後製MVH回覆的訊息;
 * 
 * @author Eric
 *
 */
@Service
@Slf4j
public class N510_Tel_Service extends Common_Tel_Service {

	@Override
	public HashMap pre_Processing(Map request) {
		// TODO Auto-generated method stub
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) {
		// TODO Auto-generated method stub
		return super.process(serviceID, request);
	}
	
	
	
	@Override
	public void setMsgCode(Map dataMap, Boolean data_Retouch_result) {
		// TODO Auto-generated method stub
		super.setMsgCode(dataMap, data_Retouch_result);
	}
	
	@Override
	public HashMap data_Retouch(MVH mvh) {
		MVHImpl mvhimpl = null;
		LinkedList<Row> tmplist = null;
		Boolean result = Boolean.FALSE;
		HashMap<String,Object> dataMap = new HashMap<String,Object>();
		HashMap<String,String> rowMap = new HashMap<String,String>();
		LinkedList<HashMap<String,String>> rowlist = null;
		LinkedList<HashMap<String,String>> rowlist2 = null;
		LinkedList<HashMap<String,String>> rowlist3 = null;
    	try {
    		mvhimpl = (MVHImpl) mvh;
    	log.info("getFlatValues>>"+mvhimpl.getFlatValues());
    	log.info("result.getInputQueryData()>>"+mvhimpl.getInputQueryData());
    	log.info("result.getInputQueryData()>>"+mvhimpl.getOccurs().getRows());
			Iterator<Row> tmp = mvhimpl.getOccurs().getRows().iterator();
			tmplist = new LinkedList<Row>();
			rowlist = new LinkedList<HashMap<String,String>>();
			rowlist2 = new LinkedList<HashMap<String,String>>();
			rowlist3 = new LinkedList<HashMap<String,String>>();
			tmp.forEachRemaining(tmplist::add);
			int count = 1;
			for(String key:mvhimpl.getFlatValues().keySet()) {
				if(key.startsWith("AVACUID")) {
					count++;
				}
			}
			
			
			for(int i = 1 ; i<count ; i++) {
				rowMap = new HashMap<String,String>();
				String avacuid = mvhimpl.getFlatValues().get("AVACUID_"+i);
				String avasubamt = mvhimpl.getFlatValues().get("AVASUBAMT_"+i);
				String avasubamtrecnum = mvhimpl.getFlatValues().get("AVASUBAMTRECNUM_"+i);
				if(StrUtils.isEmpty(avacuid)) {
					continue;
				}else {
				rowMap.put("CUID", avacuid);
				rowMap.put("AVAILABLE",avasubamt );
				rowMap.put("COUNT",avasubamtrecnum );
				rowlist2.add(rowMap);
				}
			}
			for(int i = 1 ; i<count ; i++) {
				rowMap = new HashMap<String,String>();
				String balcuid = mvhimpl.getFlatValues().get("BALCUID_"+i);
				String balsubamt = mvhimpl.getFlatValues().get("BALSUBAMT_"+i);
				String balsubamtrecnum = mvhimpl.getFlatValues().get("BALSUBAMTRECNUM_"+i);
				if(StrUtils.isEmpty(balcuid)) {
					continue;
				}else {
				rowMap.put("CUID", balcuid);
				rowMap.put("BALANCE",balsubamt );
				rowMap.put("COUNT",balsubamtrecnum );
				rowlist3.add(rowMap);
				}
			}
			log.info("datalist()>>"+tmplist);
			for(Row  row:tmplist) {
				if(StrUtils.isEmpty(row.getValue("CUID"))) {
					continue;
				}else {
				//log.info("getValues>>"+row.getValues());
				rowMap = new HashMap<String,String>();
				rowMap.putAll(row.getValues());
				rowlist.add(rowMap);
				}
			}
			dataMap.putAll(mvhimpl.getFlatValues());
			dataMap.put("ACNInfo", rowlist);
			dataMap.put("AVASum", rowlist2);
			dataMap.put("BALSum", rowlist3);
			result = Boolean.TRUE;
			
		} catch (Exception e) {
			log.error("data_Retouch error >> {}",e);
		}finally {
			
			setMsgCode(dataMap, result);
		}
		return dataMap;
	}
	
}
