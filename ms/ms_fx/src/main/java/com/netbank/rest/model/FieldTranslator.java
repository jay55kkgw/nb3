package com.netbank.rest.model;

import fstop.model.DefaultTranslator;
import fstop.model.AdmoplogTranslator;
import fstop.model.B201Translator;

public class FieldTranslator {
	public static String transfer(String name, String value) {
		return DefaultTranslator.getInstance().transfer("", name, value);
	}

	public static String transfer(String scope, String name, String value) {
		return DefaultTranslator.getInstance().transfer(scope, name, value);
	}
	
	public static String admoplogTransfer(String name) {
		return AdmoplogTranslator.getInstance().transfer("", name);
	}

	public static String admoplogTransfer(String scope, String name) {
		return AdmoplogTranslator.getInstance().transfer(scope, name);
	}
	
	public static String admoplogTransfer(String scope, String name, String value) {
		return AdmoplogTranslator.getInstance().transfer(scope, name, value);
	}
	
	public static String b201Translator(String scope, String name) {
		return B201Translator.getInstance().transfer(scope, name);
	}
	public static String b201Translator(String scope, String name, String value) {
		return B201Translator.getInstance().transfer(scope, name, value);
	}
	public static String[] b201GetKeySet(String adopid) {
		return B201Translator.getInstance().GetKeySet(adopid);
	}
}