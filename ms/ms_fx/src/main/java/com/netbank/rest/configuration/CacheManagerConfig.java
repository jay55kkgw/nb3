package com.netbank.rest.configuration;

import lombok.extern.slf4j.Slf4j;
import net.sf.ehcache.Cache;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 * Cache 設定 / ApplicationContext-cache.xml
 */
@Configuration
@Slf4j
public class CacheManagerConfig {

    @Resource
    private EhCacheCacheManager cacheManager;

    @Bean
    protected Cache readlogsCache() {
        return cacheManager.getCacheManager().getCache("fstop.READLOGS_CACHE");
    }


    @Bean
    protected Cache sysObjectCache() {
        return cacheManager.getCacheManager().getCache("sysObjectCache");
    }

    @Bean
    protected Cache userPoolCache() {
        return cacheManager.getCacheManager().getCache("userPoolCache");
    }

    @Bean
    protected Cache distributeCache() {
        return cacheManager.getCacheManager().getCache("distributeCache");
    }

    @Bean
    protected Cache fileSyncCache()  {
        return cacheManager.getCacheManager().getCache("fileSyncCache");
    }

}
