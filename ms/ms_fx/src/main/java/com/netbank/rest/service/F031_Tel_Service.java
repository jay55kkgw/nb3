package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netbank.rest.model.CommonPools;
import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 一般網銀外幣匯出匯款受款人約定檔擷取
 * 主要用來 1.丟入MVH前的前置處理，包含檢核; 2.後製MVH回覆的訊息;
 * 
 * @author Ian
 *
 */
@Service
@Slf4j
public class F031_Tel_Service extends Common_Tel_Service  {
	
	@Autowired
	private CommonPools commonPools;


	@Override
	public HashMap pre_Processing(Map request) {
		// TODO Auto-generated method stub
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) {
		MVH mvh = new MVHImpl();
		String cid = String.valueOf(request.get("CUSIDN"));
		log.debug(ESAPIUtil.vaildLog("cid : {}"+ cid));
		try {
			//一般網銀外幣匯出匯款受款人約定檔擷取
			mvh = commonPools.fxUtils.getRcvInfo(cid);
			//Exception TOPMSG 放 0000
			MVHImpl m = new MVHImpl();
			m = (MVHImpl) mvh;
			m.getFlatValues().put("TOPMSG","0000");
			
		} catch (TopMessageException e) {
			// TODO Auto-generated catch block
			log.error("process.error>>", e);
			MVHImpl m = new MVHImpl();
			m.getFlatValues().put("TOPMSG", e.getMsgcode());
			mvh = m;
		} catch (Exception e) {
			log.error("process.error>>{}", e);
			throw new RuntimeException(e);
		} finally {
			// TODO trur 做data_Retouch false 做msgCode 回覆處理
			return data_Retouch(mvh);
		}
	}

	@Override
	public HashMap data_Retouch(MVH mvh) {
		// TODO Auto-generated method stub
		return super.data_Retouch(mvh);
	}
	
	@Override
	public void setMsgCode(Map dataMap, Boolean data_Retouch_result) {
		// TODO Auto-generated method stub
		super.setMsgCode(dataMap, data_Retouch_result);
	}
	
}
