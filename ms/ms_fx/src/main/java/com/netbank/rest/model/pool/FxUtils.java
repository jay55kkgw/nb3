package com.netbank.rest.model.pool;

import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.netbank.rest.model.NewOneCallback;
import com.netbank.rest.model.UserPool;

import fstop.model.DBResult;
import fstop.model.EachRowCallback;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.model.Rows;
import fstop.model.RowsSelecter;
import fstop.model.TelcommResult;
import fstop.orm.dao.AdmBhContactDao;
import fstop.orm.dao.AdmCountryDao;
import fstop.orm.dao.AdmMsgCodeDao;
import fstop.orm.dao.AdmMyRemitMenuDao;
import fstop.orm.dao.AdmRemitMenuDao;
import fstop.orm.dao.TxnFxRecordDao;
//import fstop.orm.dao.TrCountrySetDao;
import fstop.orm.po.ADMMSGCODE;
import fstop.orm.po.ADMMYREMITMENU;
import fstop.orm.po.ADMREMITMENU;
import fstop.orm.po.TXNFXRECORD;
import fstop.services.mgn.B105;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class FxUtils extends UserPool {
	@Autowired
	@Qualifier("f031Telcomm")
	private TelCommExec f031Telcomm;
	
	@Autowired
	B105 b105;
	
	@Autowired
	private AdmRemitMenuDao admRemitMenuDao;
	
	@Autowired
	private AdmMyRemitMenuDao admMyRemitMenuDao;
	
	@Autowired
	private AdmBhContactDao admBhContactDao;
	
	@Autowired
	private TxnFxRecordDao txnFxRecordDao;
	
	@Autowired
	private AdmMsgCodeDao admMsgCodeDao;
	
	@Autowired
	private AdmCountryDao admCountryDao;

	//private TrCountrySetDao trCountrySetDao;
	
/*
	@Required
	public void setTrCountrySetDao(TrCountrySetDao dao) {
		this.trCountrySetDao = dao;
	}
*/	

	
	public MVH getRemitMenu(String ADRMTTYPE, String ADLKINDID, String ADMKINDID, String ADRMTID, String custype) {
		//由 DB 來
		if(StrUtils.isNotEmpty(ADLKINDID) || !("00".equals((ADMKINDID))) || StrUtils.isNotEmpty(ADRMTID)|| StrUtils.isNotEmpty(custype)) {
			List result = admRemitMenuDao.getRemitMenu(ADRMTTYPE, ADLKINDID, ADMKINDID, ADRMTID, custype);
			
			return new DBResult(result);
		}
		else {
			Rows all = admRemitMenuDao.getAllAdlKind(ADRMTTYPE);
			return new MVHImpl(all);
		}
	}
	
	private NewOneCallback getF031NewOneCallback(final String uid) {
		NewOneCallback callback = new NewOneCallback() {

			public Object getNewOne() {

				Hashtable params = new Hashtable();
				params.put("CUSIDN", uid);
				Date d = new Date();
				String df = DateTimeUtils.getCDateShort(d);
				String tf = DateTimeUtils.format("HHmmss", d);
				params.put("DATE", df);
				params.put("TIME", tf);
				params.put("WS-ID", uid);
				
				TelcommResult mvh = f031Telcomm.query(params);
					
				RowsSelecter rowsselecter = new RowsSelecter(mvh.getOccurs());
				rowsselecter.each(new EachRowCallback() {
					
					public void current(Row row) {
						//row.setValue("VALUE", row.getValue("CARDNUM"));	
						
						row.setValue("TEXT", row.getValue("BENACC") +"-"+ row.getValue("CCY"));					
					}
					
				});
				
				
				return mvh.getOccurs();
			}
			
		};		
		
		return callback;
	}

	/**
	 * 一般網銀外幣匯出匯款受款人約定檔擷取
	 * @param idn
	 * @return
	 */
	public MVH getRcvInfo(final String idn) {
		final String uid = StrUtils.trim(idn).toUpperCase();
		if(uid.length() == 0)
			return new MVHImpl();

		//Rows rows  = (Rows)getPooledObject(uid, "getRcvInfo", getF031NewOneCallback(uid));   //不使用 cache 的機制
		Rows rows = (Rows)getF031NewOneCallback(uid).getNewOne();
		MVHImpl result = new MVHImpl(rows);

		return result;
	}

	public void editMyRemitMenu(final String uid, final String type, final String rmtid, final String rmttype) {
		//刪除一筆
		if("D".equals(type)) {
			admMyRemitMenuDao.removeByUidRmtid(uid, rmtid, rmttype);
		}
		
		//新增一筆
		if("A".equals(type)) {
			if(admMyRemitMenuDao.findByUidRmtid(uid, rmtid, rmttype) == null) {
				ADMMYREMITMENU po = new ADMMYREMITMENU();
				po.setADUID(uid);
				po.setADRMTTYPE(rmttype);
				po.setADRMTID(rmtid);
				Date d = new Date();
				po.setLASTDATE(DateTimeUtils.format("yyyyMMdd", d));
				po.setLASTTIME(DateTimeUtils.format("HHmmss", d));
				po.setLASTUSER("SYS");
				admMyRemitMenuDao.save(po);
			}
		}
	}
	/*
	public void editMyCountryMenu(final String uid, final String type, final String cntrid) {
		//刪除一筆
		if("D".equals(type)) {
			trCountrySetDao.removeByUidCntrid(uid, cntrid);
		}
		
		//新增一筆
		if("A".equals(type)) {
			if(trCountrySetDao.findByUidCntrid(uid, cntrid) == null) {
				TRCOUNTRYSET po = new TRCOUNTRYSET();
				po.setIDNO(uid);
				po.setCNTR_ID(cntrid);
				Date d = new Date();
				po.setLASTDATE(DateTimeUtils.format("yyyyMMdd", d));
				po.setLASTTIME(DateTimeUtils.format("HHmmss", d));
				po.setLASTUSER("SYS");
				trCountrySetDao.save(po);
				log.debug("Insert MyCountryMenu IDNO:"+uid+" CNTR_ID:"+cntrid);
			}
		}
	}
	*/	
	public MVH getMyRemitMenu(final String uid) {
		
		//找出這個人的所有 REMITMENU
		MVHImpl result = new DBResult(admMyRemitMenuDao.findByUid(uid));
		
		for(int i=0; i < result.getOccurs().getSize(); i++) {
			Row r = result.getOccurs().getRow(i);
			//中文名稱對應
			//ADMREMITMENU.ADRMTID=ADMMYREMITMENU.ADRMTID
			ADMREMITMENU rm = admRemitMenuDao.findUniqueByRmtid(r.getValue("ADRMTTYPE"), r.getValue("ADRMTID"));
			if(rm != null) {
				r.setValue("ADRMTITEM", rm.getADRMTITEM());
				r.setValue("ADRMTDESC", rm.getADRMTDESC());
			}
			
			if (r.getValue("ADRMTID").equals("NUL") && r.getValue("ADRMTTYPE").equals("1")) {
				r.setValue("ADRMTITEM", "結購入外匯存款(台轉外)");
				r.setValue("ADRMTDESC", "結購入外匯存款(台轉外)");
			}
			else if (r.getValue("ADRMTID").equals("NUL") && r.getValue("ADRMTTYPE").equals("2")) {
				r.setValue("ADRMTITEM", "結售外匯存款(外轉台)");
				r.setValue("ADRMTDESC", "結售外匯存款(外轉台)");
			} 
		}
		
		return result;
	}
	/*
	public MVH getMyCountryMenu(final String uid) {
		
		//找出這個人的所有常見國別
		MVHImpl result = new DBResult(trCountrySetDao.findByUid(uid));
		
		for(int i=0; i < result.getOccurs().getSize(); i++) {
			Row r = result.getOccurs().getRow(i);
			//中文名稱對應
			//ADMCOUNTRY.ADCTRYCODE=TRCOUNTRYSET.CNTR_ID
			ADMCOUNTRY rm = admCountryDao.findUniqueByCntrid(r.getValue("CNTR_ID"));
			if(rm != null) {
				r.setValue("ADCTRYNAME", rm.getADCTRYNAME());
				r.setValue("ADCTRYENGNAME", rm.getADCTRYENGNAME());
			}
		}
		
		return result;
	}
	*/	
	public MVH getFxCountry() {
		
		//找出所有 國別
		MVHImpl result = new DBResult(admCountryDao.getAll());
		
		return result;
	}	
	/**
	 * 一般網銀分行聯絡方式資料檔擷取
	 * @param bhid
	 * @return
	 */	
	public MVH getAdmBhContact(String bhid) {

		MVHImpl result = new DBResult(admBhContactDao.findByBhID(bhid));
			
		return result;
	}		
	
	/**
	 * 將外匯交易單據之網頁內容寫入 TXNFXRECORD
	 * 
	 * @param adtxno
	 * @param fxCertContent
	 */
	public void updateFxCert(String adtxno, String fxCertContent)
	{
		
		log.debug("updateFxCert 交易編號  adtxno >>>>>>>>>>>>>>>>>>>>>>>>>>>  {}", adtxno);
		log.debug("updateFxCert 交易單據  fxCertContent >>>>>>>>>>>>>>>>>>>>>>>>>>>  {}", fxCertContent);
		TXNFXRECORD fr = null;

		try {
			fr = txnFxRecordDao.findById(adtxno);
		}
		catch(ObjectNotFoundException e) {
			fr = null;
		}
		
		if(fr != null)
		{
			fr.setFXCERT(fxCertContent); //交易單據網頁內容
		}

		txnFxRecordDao.save(fr);
	}	
	
	/**
	 * 查詢 TXNFXRECORD 外匯交易單據之網頁內容 
	 * 
	 * @param adtxno
	 */
	public String getFxCert(String adtxno)
	{
		String fxcert = "";
		TXNFXRECORD fr = null;

		try {
			fr = txnFxRecordDao.findById(adtxno);
		}
		catch(ObjectNotFoundException e) {
			fr = null;
		}
		
		if(fr != null)
		{
			fxcert = fr.getFXCERT(); //交易單據網頁內容
		}

		return fxcert;
	}		
	
	/**
	 * 一般網銀外匯業務營業時間檢查
	 * 
	 * @return
	 */	
	public boolean isHOLIDAY(String day) {

		return b105.isHOLIDAY(day);
	}			
	public boolean isFxBizTime() {

		return b105.isFxBizTime();
	}			
	public boolean isFxBizTime1() {

		return b105.isFxBizTime1();
	}			
	
	public boolean isErrorCode(String topmsg) {
		
		boolean b_Result = false;
		
		try {
			ADMMSGCODE msg = admMsgCodeDao.isError(topmsg);
			
			if (msg == null)
				b_Result = false;
			else			
				b_Result = true;
		}
		catch(Exception e) {}
		
		return b_Result;
	}
}
