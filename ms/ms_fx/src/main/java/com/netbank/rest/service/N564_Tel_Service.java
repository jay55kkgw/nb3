package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.netbank.util.NumericUtil;
import com.netbank.util.StrUtils;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import lombok.extern.slf4j.Slf4j;

/**
 * 主要用來
 * 1.丟入MVH前的前置處理，包含檢核;
 * 2.後製MVH回覆的訊息;
 * @author Hugo
 *
 */
@Service
@Slf4j
public class N564_Tel_Service extends Common_Tel_Service {

	@Override
	public HashMap pre_Processing(Map request) {
		// TODO Auto-generated method stub
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) {
		// TODO Auto-generated method stub
		return super.process(serviceID, request);
	}

	@Override
	public HashMap data_Retouch(MVH mvh) {
		MVHImpl mvhimpl = null;
		LinkedList<Row> tmplist = null;
		Boolean result = Boolean.FALSE;
		HashMap<String, Object> dataMap = new HashMap<String, Object>();
		HashMap<String, String> rowMap = new HashMap<String, String>();
		LinkedList<HashMap<String, String>> rowlist = null;
		LinkedList<HashMap<String, String>> cryrowlist = new LinkedList<HashMap<String, String>>();
		try {
			mvhimpl = (MVHImpl) mvh;
			log.info("getFlatValues>>" + mvhimpl.getFlatValues());
			log.info("result.getInputQueryData()>>" + mvhimpl.getInputQueryData());
			log.info("result.getInputQueryData()>>" + mvhimpl.getOccurs().getRows());
			Iterator<Row> tmp = mvhimpl.getOccurs().getRows().iterator();
			tmplist = new LinkedList<Row>();
			rowlist = new LinkedList<HashMap<String, String>>();
			tmp.forEachRemaining(tmplist::add);

			Map<String, String> flatValuesMap = mvhimpl.getFlatValues();
			int i = 1;
			while (i < flatValuesMap.size()) {
				HashMap<String, String> cryMap = new HashMap<String, String>();
				
				if (StrUtils.isNotEmpty(flatValuesMap.get("AMTRBILLCCY_" + i))) {
					cryMap.put("AMTRBILLCCY", flatValuesMap.get("AMTRBILLCCY_" + i));
					cryMap.put("FXTOTAMTRECNUM", flatValuesMap.get("FXTOTAMTRECNUM_" + i));
					cryMap.put("FXTOTAMT", NumericUtil.fmtAmount(flatValuesMap.get("FXTOTAMT_" + i),2));

					flatValuesMap.remove("AMTRBILLCCY_" + i);
					flatValuesMap.remove("FXTOTAMTRECNUM_" + i);
					flatValuesMap.remove("FXTOTAMT_" + i);

					cryrowlist.add(cryMap);
					i++;
				} else {
					i++;
				}
			}

			log.info("datalist()>>" + tmplist);
			for (Row row : tmplist) {
				log.info("getValues>>" + row.getValues());
				rowMap = new HashMap<String, String>();
				Map<String, String> eachRow = row.getValues();
				row.getValues().put("RBILLAMT", NumericUtil.fmtAmount(eachRow.get("RBILLAMT"),2));
				rowMap.putAll(row.getValues());
				rowlist.add(rowMap);
			}
			dataMap.putAll(mvhimpl.getFlatValues());
			dataMap.put("REC", rowlist);
			dataMap.put("CRY", cryrowlist);
			result = Boolean.TRUE;

		} catch (Exception e) {
			log.error("data_Retouch error >> {}",e);
		} finally {
			setMsgCode(dataMap, result);
		}
		return dataMap;
	}

	@Override
	public void setMsgCode(Map dataMap, Boolean data_Retouch_result) {
		// TODO Auto-generated method stub
		super.setMsgCode(dataMap, data_Retouch_result);
	}
}
