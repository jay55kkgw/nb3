package com.netbank.rest.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import fstop.services.ServerConfig;
import fstop.services.TopMsgDBDetecter;
import fstop.services.TopMsgF003Detecter;
import fstop.services.TopMsgF007Detecter;
import fstop.services.TopMsgFXDetecter;
import fstop.services.TopMsgFundDetecter;
import fstop.services.TopMsgN320Detecter;
import fstop.services.TopMsgN950Detecter;
import fstop.services.batch.CheckServerHealth;
import fstop.services.impl.CallMobileCenter;
import fstop.services.impl.F001N;
import fstop.services.impl.F001R;
import fstop.services.impl.F001S;
import fstop.services.impl.F001T;
import fstop.services.impl.F001T_1;
import fstop.services.impl.F001Y;
import fstop.services.impl.F002N;
import fstop.services.impl.F002R;
import fstop.services.impl.F002S;
import fstop.services.impl.F002T;
import fstop.services.impl.F002T_1;
import fstop.services.impl.F002Y;
import fstop.services.impl.F002_SWIFT;
import fstop.services.impl.F003R;
import fstop.services.impl.F003T;
import fstop.services.impl.F013;
import fstop.services.impl.F015;
import fstop.services.impl.F017;
import fstop.services.impl.F017R;
import fstop.services.impl.F017T;
import fstop.services.impl.F021;
import fstop.services.impl.F035;
import fstop.services.impl.F037;
import fstop.services.impl.F039;
import fstop.services.impl.N110;
import fstop.services.impl.N130;
import fstop.services.impl.N174;
import fstop.services.impl.N175_1;
import fstop.services.impl.N175_2;
import fstop.services.impl.N175_2S;
import fstop.services.impl.N177;
import fstop.services.impl.N178;
import fstop.services.impl.N510;
import fstop.services.impl.N520;
import fstop.services.impl.N520_FILE;
import fstop.services.impl.N530;
import fstop.services.impl.N531;
import fstop.services.impl.N550;
import fstop.services.impl.N551;
import fstop.services.impl.N555;
import fstop.services.impl.N556;
import fstop.services.impl.N557;
import fstop.services.impl.N558;
import fstop.services.impl.N559;
import fstop.services.impl.N560;
import fstop.services.impl.N563;
import fstop.services.impl.N564;
import fstop.services.impl.N565;
import fstop.services.impl.N566;
import fstop.services.impl.N567;
import fstop.services.impl.N568;
import fstop.services.impl.N570;
import fstop.services.impl.N615;
import fstop.services.impl.N625;
import fstop.services.impl.N640;
import fstop.services.impl.N650;
import fstop.services.impl.N701;
import fstop.services.impl.N870;
import fstop.services.impl.N920;
import fstop.services.impl.fx.F001Controller;
import fstop.services.impl.fx.F002Controller;
import fstop.services.impl.fx.F003Controller;
import fstop.services.mgn.B105;
import fstop.services.mgn.B106;
import fstop.va.VATransform;
import fstop.ws.eai.client.N701WebServiceClient;
import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class ServicesConfig
{

	@Bean
	protected VATransform vatransform() throws Exception
	{
		return new VATransform();
	}

//	@Bean
//	protected CheckServerHealth checkserverhealth() throws Exception
//	{
//		return new CheckServerHealth();
//	}

	@Bean
	protected N701WebServiceClient n701WebServiceClient() throws Exception
	{
		return new N701WebServiceClient();
	}

	@Bean
	protected TopMsgDBDetecter dbErrDetect() throws Exception
	{
		return new TopMsgDBDetecter();
	}

	@Bean
	protected TopMsgN320Detecter n320ErrDetect() throws Exception
	{
		return new TopMsgN320Detecter();
	}

	@Bean
	protected TopMsgFXDetecter fxErrDetect() throws Exception
	{
		return new TopMsgFXDetecter();
	}

	@Bean
	protected TopMsgF003Detecter f003ErrDetect() throws Exception
	{
		return new TopMsgF003Detecter();
	}

	@Bean
	protected TopMsgF007Detecter f007ErrDetect() throws Exception
	{
		return new TopMsgF007Detecter();
	}

	@Bean
	protected TopMsgFundDetecter fundErrDetect() throws Exception
	{
		return new TopMsgFundDetecter();
	}

	@Bean
	protected TopMsgN950Detecter n950ErrDetect() throws Exception
	{
		return new TopMsgN950Detecter();
	}

	@Bean
	protected B105 b105Service() throws Exception
	{
		return new B105();
	}

	@Bean
	protected B106 b106Service() throws Exception
	{
		return new B106();
	}
    @Bean
    protected CallMobileCenter callmobilecenterService() throws Exception {
    	return new CallMobileCenter();
    }
	@Bean
	protected F001N f001nService() throws Exception
	{
		return new F001N();
	}

	@Bean
	protected F001R f001rService() throws Exception
	{
		return new F001R();
	}

	@Bean
	protected F001S f001sService() throws Exception
	{
		return new F001S();
	}

	@Bean
	protected F001T f001tService() throws Exception
	{
		return new F001T();
	}

	@Bean
	protected F001T_1 f001t_1Service() throws Exception
	{
		return new F001T_1();
	}

	@Bean
	protected F001Y f001yService() throws Exception
	{
		return new F001Y();
	}

	@Bean
	protected F002_SWIFT f002_swiftService() throws Exception
	{
		return new F002_SWIFT();
	}

	@Bean
	protected F002N f002nService() throws Exception
	{
		return new F002N();
	}

	@Bean
	protected F002R f002rService() throws Exception
	{
		return new F002R();
	}

	@Bean
	protected F002S f002sService() throws Exception
	{
		return new F002S();
	}

	@Bean
	protected F002T f002tService() throws Exception
	{
		return new F002T();
	}

	@Bean
	protected F002T_1 f002t_1Service() throws Exception
	{
		return new F002T_1();
	}

	@Bean
	protected F002Y f002y_1Service() throws Exception
	{
		return new F002Y();
	}

	@Bean
	protected F003R f003rService() throws Exception
	{
		return new F003R();
	}

	@Bean
	protected F003T f003tService() throws Exception
	{
		return new F003T();
	}

	@Bean
	protected F013 f013Service() throws Exception
	{
		return new F013();
	}

	@Bean
	protected F015 f015Service() throws Exception
	{
		return new F015();
	}

	@Bean
	protected F017 f017Service() throws Exception
	{
		return new F017();
	}

	@Bean
	protected F017R f017rService() throws Exception
	{
		return new F017R();
	}

	@Bean
	protected F017T f017tService() throws Exception
	{
		return new F017T();
	}

	@Bean
	protected F021 f021Service() throws Exception
	{
		return new F021();
	}

	@Bean
	protected F035 f035Service() throws Exception
	{
		return new F035();
	}

	@Bean
	protected F037 f037Service() throws Exception
	{
		return new F037();
	}

	@Bean
	protected F039 f039Service() throws Exception
	{
		return new F039();
	}

	@Bean
	protected N920 n920Service() throws Exception
	{
		return new N920();
	}

	@Bean
	protected N615 n615Service() throws Exception
	{
		return new N615();
	}

	@Bean
	protected N110 n110Service() throws Exception
	{
		return new N110();
	}

	@Bean
	protected N130 n130Service() throws Exception
	{
		return new N130();
	}

	@Bean
	protected N175_1 n175_1Service() throws Exception
	{
		return new N175_1();
	}

	@Bean
	protected N175_2S n175_2SService() throws Exception
	{
		return new N175_2S();
	}

	@Bean
	protected N177 n177Service() throws Exception
	{
		return new N177();
	}

	@Bean
	protected N178 n178Service() throws Exception
	{
		return new N178();
	}

	@Bean
	protected N510 n510Service() throws Exception
	{
		return new N510();
	}

	@Bean
	protected N520 n520Service() throws Exception
	{
		return new N520();
	}

	@Bean
	protected N520_FILE n520_fileService() throws Exception
	{
		return new N520_FILE();
	}

	@Bean
	protected N530 n530Service() throws Exception
	{
		return new N530();
	}

	@Bean
	protected N531 n531Service() throws Exception
	{
		return new N531();
	}

	@Bean
	protected N551 n551Service() throws Exception
	{
		return new N551();
	}

	@Bean
	protected N555 n555Service() throws Exception
	{
		return new N555();
	}

	@Bean
	protected N556 n556Service() throws Exception
	{
		return new N556();
	}

	@Bean
	protected N557 n557Service() throws Exception
	{
		return new N557();
	}

	@Bean
	protected N558 n558Service() throws Exception
	{
		return new N558();
	}

	@Bean
	protected N559 n559Service() throws Exception
	{
		return new N559();
	}

	@Bean
	protected N560 n560Service() throws Exception
	{
		return new N560();
	}

	@Bean
	protected N563 n563Service() throws Exception
	{
		return new N563();
	}

	@Bean
	protected N564 n564Service() throws Exception
	{
		return new N564();
	}

	@Bean
	protected N565 n565Service() throws Exception
	{
		return new N565();
	}

	@Bean
	protected N566 n566Service() throws Exception
	{
		return new N566();
	}

	@Bean
	protected N567 n567Service() throws Exception
	{
		return new N567();
	}

	@Bean
	protected N568 n568Service() throws Exception
	{
		return new N568();
	}

	@Bean
	protected N550 n550Service() throws Exception
	{
		return new N550();
	}

	@Bean
	protected N570 n570Service() throws Exception
	{
		return new N570();
	}

	@Bean
	protected N625 n625Service() throws Exception
	{
		return new N625();
	}

	@Bean
	protected N640 n640Service() throws Exception
	{
		return new N640();
	}

	@Bean
	protected N650 n650Service() throws Exception
	{
		return new N650();
	}

	@Bean
	protected N701 n701Service() throws Exception
	{
		return new N701();
	}

	@Bean
	protected N870 n870Service() throws Exception
	{
		return new N870();
	}

	// <!-- 後台OTP狀態 -->
	@Bean
	protected ServerConfig serverConfig() throws Exception
	{
		return new ServerConfig();
	}

	@Bean
	protected N175_2 n175_2Service() throws Exception
	{
		return new N175_2();
	}

	@Bean
	protected N174 n174Service() throws Exception
	{
		return new N174();
	}

	@Bean
	protected F001Controller f001controllerService() throws Exception
	{
		return new F001Controller();
	}

	@Bean
	protected F002Controller f002controllerService() throws Exception
	{
		return new F002Controller();
	}

	@Bean
	protected F003Controller f003controllerService() throws Exception
	{
		return new F003Controller();
	}


}
