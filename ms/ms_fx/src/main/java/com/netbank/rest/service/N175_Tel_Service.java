package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.services.impl.N175_1;
import fstop.services.impl.N175_2;
import fstop.services.impl.N175_2S;
import lombok.extern.slf4j.Slf4j;

/**
 * 主要用來
 * 1.丟入MVH前的前置處理，包含檢核;
 * 2.後製MVH回覆的訊息;
 * @author Hugo
 *
 */
@Service
@Slf4j
public class N175_Tel_Service extends Common_Tel_Service
{
	
	@Autowired
	private N175_1 n175_1;
	
	@Autowired
	private N175_2 n175_2;
	
	@Autowired
	private N175_2S n175_2s;
	
	@Override
	public HashMap pre_Processing(Map request) {
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) {
		MVH mvh = new MVHImpl();
		try
		{
			boolean hasTxid = request.containsKey("TXID");
			if(hasTxid)
			{
				String txid = String.valueOf(request.get("TXID"));
				log.trace(ESAPIUtil.vaildLog("交易型態 TXID: >> {}"+ txid));
				switch(txid)
				{
					// 查詢
					case "N175_1":
						mvh = n175_1.doAction(request);
						break;
						
					// 交易
					case "N175_2":
						mvh = n175_2.doAction(request);
						break;
						
					// 預約
					case "N175_2S":
						// 讓N175_2S永遠打n951CMTelcomm
						request.put("TRANSPASSUPDATE", "1");
						mvh = n175_2s.doAction(request);
						break;
						
					default:
						log.error(ESAPIUtil.vaildLog("N175_REST無法決定交易型態，TXID 錯誤 >> {}"+ txid));
				}
			}
			else
			{
				log.error("N175_REST Unable to decide the transaction type, can not find TXID. "
						+ "無法決定交易型態，未輸入 TXID");
			}
		}
		catch (TopMessageException e) 
		{
			log.error("process.error >> ", e);
			MVHImpl errMvh = new MVHImpl();
			// 讓data_Retouch可以用TOPMSG查DB messageName
			errMvh.getFlatValues().put("TOPMSG", e.getMsgcode());
			mvh = errMvh;
		}
		catch (Exception e)
		{
			log.error("N175_Tel_Service process.error>>", e);
			throw new RuntimeException(e);
		}
		finally 
		{
			return data_Retouch(mvh);
		}
	}

	@Override
	public HashMap data_Retouch(MVH mvh) {
		return super.data_Retouch(mvh);
	}
	
}
