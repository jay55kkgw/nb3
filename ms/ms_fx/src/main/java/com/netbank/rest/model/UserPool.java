package com.netbank.rest.model;

import fstop.util.SpringBeanFactory;
import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class UserPool {

	/*
	 * 要等到 Spring 的環境都起來後, 才有辦法使用 SpringBeanFactory
	 */
	private Cache getCache() {
		return (Cache) SpringBeanFactory.getBean("userPoolCache");
	}

	
	public Object getPooledObject(String uid, String poolkey, NewOneCallback newone) {
		Cache cache = getCache();
		Map<String, Object> userpool = null;
		Element elm = cache.get(uid);
		if(elm != null) {
			userpool = (Map)elm.getValue();
		}
		else {
			Element newelm = new Element(uid, new ConcurrentHashMap());
			cache.put(newelm);
			userpool = (Map)newelm.getValue();
			cache.put(newelm);
		}

		if(userpool.containsKey(poolkey)) {
			return userpool.get(poolkey);
		}
		else {
			Object o = newone.getNewOne();

			userpool.put(poolkey, o);
			cache.put(new Element(uid, userpool));

			return o;
		}
	}

	public void removePoolObject(String uid) {
		Cache cache = getCache();
		cache.remove(uid);
	}

	public void removePoolObject(String uid, String poolkey) {
		Cache cache = getCache();
		Map<String, Object> userpool = null;
		Element elm = cache.get(uid);
		if(elm != null) {
			userpool = (Map)elm.getValue();
		}
		else {
			Element newelm = new Element(uid, new ConcurrentHashMap());
			cache.put(newelm);
			userpool = (Map)newelm.getValue();
			cache.put(newelm);
		}

		if(userpool.containsKey(poolkey)) {
			userpool.remove(poolkey);
			Element newelm = new Element(uid, userpool);
			cache.put(newelm);
		}
	}

}
