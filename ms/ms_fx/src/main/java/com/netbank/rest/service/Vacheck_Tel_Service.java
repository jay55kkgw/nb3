package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.netbank.util.ESAPIUtil;

import fstop.model.MVH;
import fstop.va.VaCheck;
import lombok.extern.slf4j.Slf4j;

/**
 * 自然人憑證驗證
 */
@Service
@Slf4j
public class Vacheck_Tel_Service extends Common_Tel_Service{

	@Override
	public HashMap pre_Processing(Map request){
		return super.pre_Processing(request);
	}
	@Override
	public HashMap process(String serviceID,Map request){
		log.debug(ESAPIUtil.vaildLog("request={}"+request));
		HashMap<String,String> map = new HashMap<String,String>();
		
		String response = VaCheck.checkAuthenReq((String)request.get("AUTHCODE"));
		log.debug("response={}",response);
		
		boolean result = false;
		if(!"".equals(response)){
			result = true;
			map.put("MSGCOD","0000");
			map.put("RESPONSE",response);
		}
		
		setMsgCode(map,result);
		
		return map;
	}
	@Override
	public HashMap data_Retouch(MVH mvh){
		return super.data_Retouch(mvh);
	}
}