package com.netbank.rest.model.pool;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.netbank.rest.model.NewOneCallback;
import com.netbank.rest.model.UserPool;

import fstop.model.EachRowCallback;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.model.RowsSelecter;
import fstop.model.TelcommResult;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;

@Component
public class N810Pool extends UserPool {

	@Autowired
	@Qualifier("n810Telcomm")
	private TelCommExec n810Telcomm;
//
//	@Required
//	public void setN810Telcomm(TelCommExec telcomm) {
//		n810Telcomm = telcomm;
//	}

	private NewOneCallback getNewOneCallback(final String uid) {
		NewOneCallback callback = new NewOneCallback() {

			public Object getNewOne() {
/*
				Hashtable params = new Hashtable();
				params.put("CUSIDN", uid);
				Date d = new Date();
				String df = DateTimeUtils.getCDateShort(d);
				String tf = DateTimeUtils.format("HHmmss", d);
				params.put("DATE", df);
				params.put("TIME", tf);
				
				TelcommResult mvh = n810Telcomm.query(params);
				
				RowsSelecter rowsselecter = new RowsSelecter(mvh.getOccurs());
				rowsselecter.each(new EachRowCallback() {

					public void current(Row row) {
						row.setValue("VALUE", row.getValue("CARDNUM"));
						row.setValue("TEXT", row.getValue("CRD_TYPE") +"-"+ row.getValue("CARDNUM"));
					}
					
				});
							
				return mvh.getOccurs();
*/			
				
				return null;
			}
			
		};		
		
		return callback;
	}
	
	
	/**
	 * N810 信用卡查詢
	 * @param uid  使用者的身份證號
	 * @return
	 */
	public MVH getAcnoList(final String idn) {
		final String uid = StrUtils.trim(idn).toUpperCase();
		if(uid.length() == 0)
			return new MVHImpl();
		
/*
		Rows rows  = (Rows)getPooledObject(uid, "N810.getAcnoList", getNewOneCallback(uid));
		MVHImpl result = new MVHImpl(rows);
*/
		
		Map params = new HashMap();
		params.put("CUSIDN", uid);
		params.put("LOGINTYPE", "NB");
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);
		
		TelcommResult mvh = n810Telcomm.query(params);
		
		RowsSelecter rowsselecter = new RowsSelecter(mvh.getOccurs());
		rowsselecter.each(new EachRowCallback() {

			public void current(Row row) {
				row.setValue("VALUE", row.getValue("CARDNUM"));
				row.setValue("TEXT", row.getValue("CRD_TYPE") +"-"+ row.getValue("CARDNUM"));
			}
			
		});
		
		return mvh;
	}
}
