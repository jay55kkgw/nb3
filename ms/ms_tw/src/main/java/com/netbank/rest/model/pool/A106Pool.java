package com.netbank.rest.model.pool;

import java.util.Hashtable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.netbank.rest.model.NewOneCallback;
import com.netbank.rest.model.UserPool;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.telcomm.TelCommExec;
import fstop.util.StrUtils;

@Component
public class A106Pool extends UserPool {
	
	@Autowired
    @Qualifier("a106Telcomm")
	private TelCommExec a106Telcomm;
	
//	private TelCommExec a106Telcomm;
//	
//	@Required
//	public void setA106Telcomm(TelCommExec telcomm) {
//		a106Telcomm = telcomm;
//	}
	
	private NewOneCallback getNewOneCallback(final String uid) {
		NewOneCallback callback = new NewOneCallback() {

			public Object getNewOne() {

				Hashtable params = new Hashtable();				
				params.put("FUN_TYPE", "R");
				params.put("CUSIDN", uid);
//				Date d = new Date();
//				String df = DateTimeUtils.getCDateShort(d);
//				String tf = DateTimeUtils.format("HHmmss", d);
//				params.put("DATE", df);
//				params.put("TIME", tf);								
				TelcommResult mvh = a106Telcomm.query(params);
				
				return mvh;
			}
			
		};		
		
		return callback;
	}
	
	
	/**
	 * 
	 * @param uid  使用者的身份證號
	 * @param 
	 * @param 
	 * @return
	 */
	public MVH getA106List(final String idn) {
		final String uid = StrUtils.trim(idn).toUpperCase();
		if(uid.length() == 0)
			return new MVHImpl();
		
		return (MVH)getPooledObject(uid, "getA106List", getNewOneCallback(uid));		
	}		
	
	/**
	 * 將 身份證字號 為KEY 放置在POOL 的A106 資料刪除
	 * @param idn
	 */
	public void removeFromPool(String idn) {
		super.removePoolObject(idn);
	}
	
}
