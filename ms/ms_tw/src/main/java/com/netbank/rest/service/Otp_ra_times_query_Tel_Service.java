package com.netbank.rest.service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netbank.util.fstop.DateUtil;

import fstop.orm.dao.TxnSmsLogDao;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 主要用來
 * 1.丟入MVH前的前置處理，包含檢核;
 * 2.後製MVH回覆的訊息;
 * @author Hugo
 *
 */
@Service
@Slf4j
public class Otp_ra_times_query_Tel_Service extends Common_Tel_Service{
	
	@Autowired
	private TxnSmsLogDao txnSmsLogDao;
	
	@Override
	public HashMap pre_Processing(Map request) {
		// TODO Auto-generated method stub
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) {
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		if (StrUtils.isEmpty((String) request.get("CUSIDN"))) {
			rtnMap.put("msgCode", "1");
			rtnMap.put("message", "參數錯誤");
		}

		try {

			Date d = new Date();
			DateTime d1 = new DateTime();
			int findcount = txnSmsLogDao.findTotalCountByInput((String) request.get("CUSIDN"),
					DateTimeUtils.format("yyyyMMdd", d), "SHD");
			DateTime d2 = new DateTime();
			String pstimediff = DateUtil.getDiffTimeMills(d1, d2);
			log.info("Otp_Ra_Times_Query_Tel_Service txnSmsLogDao.findTotalCountByInput time : {} " , pstimediff);
			
			if(findcount>=3) {
				rtnMap.put("msgCode", "1");
				rtnMap.put("message", "簡訊驗證碼已連續發送超過3次，本行得暫停簡訊驗證碼發送，待次日後始可恢復使用。");
			}else {
				rtnMap.put("msgCode", "0");
				rtnMap.put("message", "");
				rtnMap.put("COUNT", findcount);
			}
			
		} catch (Exception e) {
			
			rtnMap.put("msgCode", "1");
			rtnMap.put("message", "未知錯誤");
		
		}

		return rtnMap;
	}
	
}
