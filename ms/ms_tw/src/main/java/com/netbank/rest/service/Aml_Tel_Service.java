package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.ws.AMLWebServiceTemplate;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class Aml_Tel_Service extends Common_Tel_Service{
	//20190801新增forAML
	@Autowired
	private AMLWebServiceTemplate amlWebServiceTemplate;
	
	@Override
	public HashMap pre_Processing(Map request) {
		// TODO Auto-generated method stub
		return super.pre_Processing(request);
	}
	
	@Override
	public HashMap process(String serviceID, Map request) {
		MVH mvh = null;
		try {
			pre_Processing(request);
			MVHImpl m = new MVHImpl();
			//AML start
			if(getAmlmsg(request)) {			
				m.getFlatValues().put("occurMsg","Z616");
				m.getFlatValues().put("TOPMSG","Z616");
				m.getFlatValues().put("RESULT","F");
				mvh = m;
			}else{
				m.getFlatValues().put("RESULT","T");
				mvh = m;
			}

		} catch (TopMessageException e) {
			log.error("process.error>>{}",e);
			MVHImpl m = new MVHImpl();
			m.getFlatValues().put("TOPMSG", e.getMsgcode());
			mvh = m;
		} catch (Exception e) {
			log.error("process.error>>{}", e);
		} finally {
			return data_Retouch(mvh);
		}
	}

	
	
	@Override
	public HashMap data_Retouch(MVH mvh) {
		// TODO Auto-generated method stub
		return super.data_Retouch(mvh);
	}

	@Override
	public void setMsgCode(Map dataMap, Boolean data_Retouch_result) {
		// TODO Auto-generated method stub
		super.setMsgCode(dataMap, data_Retouch_result);
	}
	
	/**
	 * 
	 * @param reqParam
	 * @return
	 */
	public boolean getAmlmsg(Map reqParam) {
		return amlWebServiceTemplate.callAML(reqParam);
	}
	
	
	/**
	 * 20190910 by hugo
	 * 舊的AML 先註解日後再刪掉
	 * @param reqParam
	 * @return
	 */
//	public boolean getAmlmsg(Map reqParam) {
//		try{
//			String name = (String)reqParam.get("NAME");
//			String BRTHDY = (String)reqParam.get("BIRTHDAY");
//			int yearnum = Integer.parseInt(BRTHDY.substring(0,3))+1911;
//			String year = String.valueOf(yearnum);
//			String month = BRTHDY.substring(3,5);
//			String day = BRTHDY.substring(5,7);
//			
//			
//			AML001RQ rq  = new AML001RQ();
//			SearchNamesSoap  searchNamesSoap = new SearchNamesSoap();
//			Names names = new Names();
//			LinkedList list = new LinkedList<String>();
//			list.add(name);
//			names.setName(list);
//			Branch branch = new Branch();
//			branch.setBranchId((String)reqParam.get("BRHCOD"));
//			branch.setBusinessUnit("H57");
//			branch.setSourceSystem("NNB-W100089655");
//			DateOfBirth dateOfBirth = new DateOfBirth();
//			dateOfBirth.setYear(year);
//			dateOfBirth.setMonth(month);
//			dateOfBirth.setDay(day);
//			searchNamesSoap.setNames(names);
//			searchNamesSoap.setBranch(branch);
//			searchNamesSoap.setDateOfBirth(dateOfBirth);
//			rq.setMSGNAME("AML001");
//			rq.setAPP("XML");
//			rq.setCLIENTIP("::1");
//			rq.setTYPE("01");
//			rq.setSearchNamesSoap(searchNamesSoap);
//			
//			SearchResponseSoap rs = amlWebServiceTemplate.sendAndReceive (rq);
//			return rs.isHit();
//		}catch(Exception e) {
//			log.debug(e.toString());
//		}
//		return true;
//		
//	} 
}
