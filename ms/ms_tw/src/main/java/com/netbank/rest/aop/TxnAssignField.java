package com.netbank.rest.aop;

import fstop.orm.po.TXNLOG;

public enum TxnAssignField {

    NONE((l, val) -> {
    }),
    FGTXWAY((l, val) -> {
        l.setFGTXWAY(val);
    }),
    ADTXACNO((l, val) -> {
        l.setADTXACNO(val);
    }),
    ADUSERID((l, val) -> {
        l.setADUSERID(val);
    }),
    ADUSERIP((l, val) -> {
        l.setADUSERIP(val);
    }),
    ADTXAMT((l, val) -> {
        l.setADTXAMT(val);
    }),
    ADCURRENCY((l, val) -> {
        l.setADCURRENCY(val);
    }),
    ADSVBH((l, val) -> {
        l.setADSVBH(val);
    }),
    ADAGREEF((l, val) -> {   // 轉入帳號是約定或非約定帳號註記
        l.setADAGREEF(val);
    }),
    ADREQTYPE((l, val) -> {
        l.setADREQTYPE(val);
    }),;


    Assign2TxnLog assign = null;

    TxnAssignField(Assign2TxnLog assign) {
        this.assign = assign;
    }


    public void assignValue(TXNLOG l, String val) {
        assign.assign(l, val);
    }
}


interface Assign2TxnLog {
    void assign(TXNLOG l, String val);
}
