package com.netbank.rest.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import fstop.services.ServerConfig;
import fstop.services.TopMsgDBDetecter;
import fstop.services.TopMsgF003Detecter;
import fstop.services.TopMsgF007Detecter;
import fstop.services.TopMsgFXDetecter;
import fstop.services.TopMsgFundDetecter;
import fstop.services.TopMsgN320Detecter;
import fstop.services.TopMsgN950Detecter;
import fstop.services.impl.A1000;
import fstop.services.impl.A106;
import fstop.services.impl.CallMobileCenter;
import fstop.services.impl.CallOtpStatus;
import fstop.services.impl.GetCN;
import fstop.services.impl.N015;
import fstop.services.impl.N070;
import fstop.services.impl.N070_N;
import fstop.services.impl.N070_Y;
import fstop.services.impl.N074;
import fstop.services.impl.N076;
import fstop.services.impl.N077;
import fstop.services.impl.N078;
import fstop.services.impl.N079;
import fstop.services.impl.N106;
import fstop.services.impl.N108;
import fstop.services.impl.N110;
import fstop.services.impl.N130;
import fstop.services.impl.N150;
import fstop.services.impl.N205;
import fstop.services.impl.N420;
import fstop.services.impl.N421;
import fstop.services.impl.N615;
import fstop.services.impl.N625;
import fstop.services.impl.N640;
import fstop.services.impl.N650;
import fstop.services.impl.N855;
import fstop.services.impl.N855CancelData;
import fstop.services.impl.N855CheckMac;
import fstop.services.impl.N855Decrypt;
import fstop.services.impl.N856;
import fstop.services.impl.N858;
import fstop.services.impl.N860;
import fstop.services.impl.N861;
import fstop.services.impl.N870;
import fstop.services.impl.N871;
import fstop.services.impl.N911;
import fstop.services.impl.N912;
import fstop.services.impl.N920;
import fstop.services.impl.N93C;
import fstop.services.impl.N948;
import fstop.services.impl.N951;
import fstop.services.impl.N953;
import fstop.services.impl.NA70;
import fstop.services.impl.NA72;
import fstop.services.impl.NA721;
import fstop.services.impl.NP10;
import fstop.services.impl.NotifyApi;
import fstop.services.impl.SmsOtp;
import fstop.services.impl.SmsOtp_ra;
import fstop.services.impl.XLSTXT;
import fstop.va.VATransform;
import fstop.ws.eai.client.N701WebServiceClient;
import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class ServicesConfig {

    @Bean
    protected VATransform vatransform() throws Exception {
        return new VATransform();
    }
//    @Bean
//    protected CheckServerHealth checkserverhealth() throws Exception {
//    	return new CheckServerHealth();
//    }
    @Bean
    protected N701WebServiceClient n701WebServiceClient() throws Exception {
        return new N701WebServiceClient();
    }
	
    @Bean
    protected TopMsgDBDetecter dbErrDetect() throws Exception {
        return new TopMsgDBDetecter();
    }

    @Bean
    protected TopMsgN320Detecter n320ErrDetect() throws Exception {
        return new TopMsgN320Detecter();
    }

    @Bean
    protected TopMsgFXDetecter fxErrDetect() throws Exception {
        return new TopMsgFXDetecter();
    }

    @Bean
    protected TopMsgF003Detecter f003ErrDetect() throws Exception {
        return new TopMsgF003Detecter();
    }

    @Bean
    protected TopMsgF007Detecter f007ErrDetect() throws Exception {
        return new TopMsgF007Detecter();
    }

    @Bean
    protected TopMsgFundDetecter fundErrDetect() throws Exception {
        return new TopMsgFundDetecter();
    }
    @Bean
    protected TopMsgN950Detecter n950ErrDetect() throws Exception {
        return new TopMsgN950Detecter();
    }
    
    @Bean
    protected N920 n920Service() throws Exception {
        return new N920();
    }
    
    @Bean
    protected N615 n615Service() throws Exception {
        return new N615();
    }
    
    @Bean
    protected N015 n015Service() throws Exception {
    	return new N015();
    }
    @Bean
    protected CallMobileCenter callmobilecenterService() throws Exception {
    	return new CallMobileCenter();
    }
	@Bean
    protected N070 n070Service() throws Exception {
    	return new N070();
    }
	
	@Bean
    protected N070_N n070_nService() throws Exception {
    	return new N070_N();
    }
	
	@Bean
    protected N070_Y n070_yService() throws Exception {
    	return new N070_Y();
    }
	
    @Bean
    protected N074 n074Service() throws Exception {
        return new N074();
    }
        
    @Bean
    protected N076 n076Service() throws Exception {
        return new N076();
    }
    
    @Bean
    protected N077 n077Service() throws Exception {
        return new N077();
    }
    @Bean
    protected N078 n078Service() throws Exception {
    	return new N078();
    }
    @Bean
    protected N079 n079Service() throws Exception {
        return new N079();
    }
    
    @Bean
    protected N106 n106Service() throws Exception {
        return new N106();
    }
    
    @Bean
    protected N110 n110Service() throws Exception {
        return new N110();
    }


    @Bean
    protected N130 n130Service() throws Exception {
        return new N130();
    }
    
	@Bean
    protected N150 n150Service() throws Exception {
        return new N150();
    }
    
	@Bean
	protected N205 n205Service() throws Exception {
		return new N205();
	}
	
    @Bean
    protected N420 n420Service() throws Exception {
        return new N420();
    }
    
    @Bean
    protected N421 n421Service() throws Exception {
        return new N421();
    }

    
    @Bean
    protected N625 n625Service() throws Exception {
    	return new N625();
    }
	
	@Bean
    protected N640 n640Service() throws Exception {
        return new N640();
    }
    @Bean
    protected N650 n650Service() throws Exception {
    	return new N650();
    }
    
    @Bean
    protected N858 n858Service() throws Exception {
        return new N858();
    }
    @Bean
    protected N860 n860Service() throws Exception {
    	return new N860();
    }
	
    @Bean
    protected N870 n870Service() throws Exception {
        return new N870();
    }
    
    @Bean
    protected N871 n871Service() throws Exception {
        return new N871();
    }
    
    @Bean
    protected N911 n911Service() throws Exception{
    	return new N911();
    }
    
    @Bean
    protected N912 n912Service() throws Exception{
    	return new N912();
    }
    
    @Bean
    protected N948 n948Service() throws Exception{
    	return new N948();
    }
    
    @Bean
    protected N951 n951Service() throws Exception{
    	return new N951();
    }
    
    @Bean
    protected N953 n953Service() throws Exception{
    	return new N953();
    }
    
    //<!-- 後台OTP狀態 -->
    @Bean
    protected ServerConfig serverConfig() throws Exception {
        return new ServerConfig();
    }

    @Bean
    protected CallOtpStatus callOtpStatus() throws Exception {
        return new CallOtpStatus();
    }
	
	@Bean
    protected N108 n108Service() throws Exception {
        return new N108();
    }
	
    @Bean
    protected XLSTXT xlstxtService() throws Exception {
        return new XLSTXT();
    }

    @Bean
    protected SmsOtp smsotpService() throws Exception {
    	return new SmsOtp();
    }
    
    @Bean
    protected SmsOtp_ra smsotp_raService() throws Exception {
    	return new SmsOtp_ra();
    }
    
    @Bean
    protected NA70 na70Service() throws Exception {
    	return new NA70();
    }
    
    @Bean
    protected NA72 na72Service() throws Exception {
    	return new NA72();
    }
    
    @Bean
    protected NA721 na721Service() throws Exception {
        return new NA721();
    }
    
    @Bean
    protected A1000 a1000Service() throws Exception {
        return new A1000();
    }
    
    @Bean
    protected A106 a106Service() throws Exception {
        return new A106();
    }
    
    @Bean
    protected NP10 np10Service() throws Exception {
        return new NP10();
    }
    
    @Bean
    protected NotifyApi notifyapiService() throws Exception {
        return new NotifyApi();
    }
    
    @Bean
    protected GetCN getcnService() throws Exception {
        return new GetCN();
    }
    @Bean
    protected N855 n855Service() throws Exception {
    	return new N855();
    }

    @Bean
    protected N856 n856Service() throws Exception {
    	return new N856();
    }

    @Bean
    protected N861 n861Service() throws Exception {
        return new N861();
    }

    @Bean
    protected N855Decrypt n855decryptService() throws Exception {
    	return new N855Decrypt();
    }
    @Bean
    protected N855CheckMac n855checkmacService() throws Exception {
    	return new N855CheckMac();
    }
    @Bean
    protected N855CancelData n855canceldataService() throws Exception {
    	return new N855CancelData();
    }
    
    @Bean
    protected N93C n93cService() throws Exception {
    	return new N93C();
    }
}
