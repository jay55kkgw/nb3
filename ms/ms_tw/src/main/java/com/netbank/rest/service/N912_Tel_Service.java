package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.netbank.util.ESAPIUtil;

import fstop.model.MVH;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class N912_Tel_Service extends Common_Tel_Service  {
	@Override
	public HashMap pre_Processing(Map request) {
		log.trace(ESAPIUtil.vaildLog("N912.data_Retouch.pre_Processing : {}"+ request));
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) {
		log.trace(ESAPIUtil.vaildLog("N912.data_Retouch.process : {}"+ request));
		return super.process(serviceID, request);
	}

	@Override
	public HashMap data_Retouch(MVH mvh) {
		log.trace(ESAPIUtil.vaildLog("N912.data_Retouch.data_Retouch : "+ mvh));
		return super.data_Retouch(mvh);
	}

	@Override
	public void setMsgCode(Map dataMap, Boolean data_Retouch_result) {
		log.trace("N912.data_Retouch.setMsgCode : {}", dataMap);
		super.setMsgCode(dataMap, data_Retouch_result);
	}
	
	
}