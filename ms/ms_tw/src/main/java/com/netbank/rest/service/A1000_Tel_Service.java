package com.netbank.rest.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.netbank.util.ESAPIUtil;
import com.netbank.util.CodeUtil;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.model.TelcommResult;
import lombok.extern.slf4j.Slf4j;

/**
 * 主要用來
 * 1.丟入MVH前的前置處理，包含檢核;
 * 2.後製MVH回覆的訊息;
 * @author Hugo
 *
 */
@Service
@Slf4j
public class A1000_Tel_Service extends Common_Tel_Service{
	@Override
	public HashMap pre_Processing(Map request) {
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) {
		return super.process(serviceID, request);
	}

	@Override
	public HashMap data_Retouch(MVH mvh) {
		TelcommResult telcommresult = null;
		HashMap<String,Object> dataMap = new HashMap<String,Object>();
    	try {
    		telcommresult = (TelcommResult) mvh;
    		log.info(ESAPIUtil.vaildLog("data_Retouch.telcommresult: " + telcommresult));
	    	
			dataMap.putAll(telcommresult.getTelcommVectorMap());
			
		} catch (Exception e) {
			log.error("",e);
		} finally {
			setMsgCode(dataMap);
		}
		return dataMap;
	}

	private void setMsgCode(HashMap<String, Object> dataMap) {
		String val = "";
		
		try {
			log.trace(ESAPIUtil.vaildLog("setMsgCode.dataMap: {}"+CodeUtil.toJson(dataMap)));
			
			// 預設就先設定是錯誤的狀況
			dataMap.put("msgCode", "FE0003");
			
			if (dataMap.containsKey("code")) {
				val = (String) dataMap.get("code");
				log.trace(ESAPIUtil.vaildLog("val: {}"+ val));
				
				if ("0000".equals(val)) {
					dataMap.put("msgCode", "0");
					dataMap.put("msgName", "success");
					dataMap.put("result", true);
					
				} else {
					dataMap.put("msgCode", dataMap.get("code"));
					dataMap.put("msgName", dataMap.get("message"));
					dataMap.put("result", false);
				}
			}
			
		} catch (Exception e) {
			log.error("",e);
			dataMap.put("msgCode", "FE0003");
		}
		
	}

}
