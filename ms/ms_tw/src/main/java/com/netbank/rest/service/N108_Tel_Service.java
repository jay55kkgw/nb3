package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netbank.rest.model.CommonPools;
import com.netbank.util.AcnoTypeEnum;
import com.netbank.util.ESAPIUtil;
import com.netbank.util.StrUtils;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.services.CommonService;
import fstop.util.SpringBeanFactory;
import lombok.extern.slf4j.Slf4j;

/**
 * 主要用來 1.丟入MVH前的前置處理，包含檢核; 2.後製MVH回覆的訊息;
 * 
 * @author Hugo
 *
 */
@Service
@Slf4j
public class N108_Tel_Service extends Common_Tel_Service {

	@Autowired
	private CommonPools commonPools;

	@Override
	public HashMap pre_Processing(Map request) {
		// TODO Auto-generated method stub
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) {
		MVH mvh = new MVHImpl();
		String cid = String.valueOf(request.get("CUSIDN"));
		String type = String.valueOf(request.get("TYPE"));
		log.debug(ESAPIUtil.vaildLog("cid : {}" +cid));
		log.trace(ESAPIUtil.vaildLog("type : {}"+ type));
		try {
			if ("null".equals(type)) {
				CommonService service = (CommonService) SpringBeanFactory
						.getBean(String.format("%sService", Optional.ofNullable(serviceID)
								.filter(s -> StrUtils.isNotEmpty(s)).map(s -> s.toLowerCase()).orElse(null)));
				mvh = service.doAction(request);
			}else {
				//call N920Pool
				String typeUp = type.toUpperCase();
				String[] acnotype = AcnoTypeEnum.valueOf(typeUp).getAcnotype();
				String trflag = AcnoTypeEnum.valueOf(typeUp).getTrflag();
				mvh = commonPools.n920.getAcnoList(cid, acnotype, trflag);
			}	
					
		}catch (NullPointerException e) {
			log.error(ESAPIUtil.vaildLog("TYPE : {}" +type));
			log.error("process.error>>{}", e);
		}catch (IllegalArgumentException e) {
			log.error("",e);
			log.error(ESAPIUtil.vaildLog("TYPE : {}" +type));
			for (AcnoTypeEnum acnoTypeEnum : AcnoTypeEnum.values()) {
				log.error("TYPE Can only enter : {}" ,acnoTypeEnum.toString());
				}
		}catch (TopMessageException e) {
			// TODO Auto-generated catch block
			log.error("process.error>>",e);
			MVHImpl m = new MVHImpl();
			m.getFlatValues().put("TOPMSG", e.getMsgcode());
			mvh = m;
		} catch (Exception e) {
			log.error("process.error>>{}",e);
			throw new RuntimeException(e);
		}finally {
//			TODO trur 做data_Retouch false 做msgCode 回覆處理
			return data_Retouch(mvh);
		}
	}

	@Override
	public HashMap data_Retouch(MVH mvh) {
		// TODO Auto-generated method stub
		return super.data_Retouch(mvh);
	}

	@Override
	public void setMsgCode(Map dataMap, Boolean data_Retouch_result) {
		// TODO Auto-generated method stub
		super.setMsgCode(dataMap, data_Retouch_result);
	}

}
