package com.netbank.rest.web.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.netbank.rest.comm.bean.ChangeSettingCallback_In;
import com.netbank.rest.comm.bean.ChangeSettingCallback_Out;
import com.netbank.rest.comm.bean.CloseQuick_In;
import com.netbank.rest.comm.bean.GetSessionId_In;
import com.netbank.rest.comm.bean.Get_Device_Status_In;
import com.netbank.rest.comm.bean.IdGateMappingDelete_In;
import com.netbank.rest.comm.bean.IdGateMappingQuery_In;
import com.netbank.rest.comm.bean.IdGateMappingUpdate_In;
import com.netbank.rest.comm.bean.IdGateN915_In;
import com.netbank.rest.comm.bean.IdGateN939_In;
import com.netbank.rest.comm.bean.IdGateN939_Out;
import com.netbank.rest.comm.bean.IdGateN93A_In;
import com.netbank.rest.comm.bean.IdGateN93B_In;
import com.netbank.rest.comm.bean.IdGateN955_In;
import com.netbank.rest.comm.bean.IdGateQuerySession_In;
import com.netbank.rest.comm.bean.IdGateQuickSessionId_In;
import com.netbank.rest.comm.bean.LoginStatusCallback_In;
import com.netbank.rest.comm.bean.LoginStatusCallback_Out;
import com.netbank.rest.comm.bean.RegisterCallback_In;
import com.netbank.rest.comm.bean.RegisterCallback_Out;
import com.netbank.rest.comm.bean.SetSmallPay_In;
import com.netbank.rest.comm.bean.SvCancel_Txn_In;
import com.netbank.rest.comm.bean.SvCreate_Verify_Txn_In;
import com.netbank.rest.comm.bean.SvDeRegister_In;
import com.netbank.rest.comm.bean.SvGet_Txn_Status_In;
import com.netbank.rest.comm.bean.SvLock_Device_In;
import com.netbank.rest.comm.bean.SvUnlock_Device_In;
import com.netbank.rest.comm.bean.TxnStatusCallback_In;
import com.netbank.rest.comm.bean.TxnStatusCallback_Out;
import com.netbank.rest.comm.service.IdGate;
import com.netbank.rest.service.N110_Tel_Service;
import com.netbank.rest.service.REST_Service;
import com.netbank.rest.util.IDGateResult;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;
import com.netbank.util.StrUtils;
import com.netbank.util.fstop.DateUtil;

import fstop.model.MVH;
import fstop.orm.dao.IdGateHistoryDao;
import fstop.orm.dao.IdGate_Txn_StatusDao;
import fstop.orm.dao.QuickLoginMappingDao;
import fstop.orm.po.IDGATEHISTORY;
import fstop.orm.po.IDGATE_TXN_STATUS;
import fstop.orm.po.IDGATE_TXN_STATUS_PK;
import fstop.orm.po.QUICKLOGINMAPPING;
import fstop.services.CommonService;
import fstop.util.JSONUtils;
import fstop.util.SpringBeanFactory;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.validation.Valid;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

/**
 *
 */
@RestController
@RequestMapping(value = "/IDGATE")
@Slf4j
public class IdGateController {

	@Autowired
	QuickLoginMappingDao quickloginmappingdao;

	@Autowired
	IdGateHistoryDao idGateHistoryDao;
	
	@Autowired
	IdGate_Txn_StatusDao idgate_txn_statusdao;
	
    @Autowired
    ServletContext servletContext;

    @Autowired
    IdGate idgate;

	final public  String  ABEND ="ABEND";
	final public  String  RSPCOD ="RSPCOD";
	final public  String  RSPCODE ="RSPCODE";
	final public  String  TOPMSG ="TOPMSG";
	final public  String  MSGCOD ="MSGCOD";
	final public  String  RESPCOD ="RESPCOD";
	final public  String  RESPCODS ="Respcode";
	final public  String  ERRCODE ="ERRCODE";
	String[] msgkey = {ABEND,RSPCOD,RSPCODE,TOPMSG,MSGCOD,RESPCOD,ERRCODE,RESPCODS};
	List<String> list = Arrays.asList(msgkey);
	
    @PostConstruct
    public void init() {

        //註冊到 SpringBeanFactory
        SpringBeanFactory.setApplicationContext(WebApplicationContextUtils.getWebApplicationContext(servletContext));
    }

    //***************************************************
    //*                                                 *
    //*                      註冊                                              *
    //*                                                 *
    //***************************************************

    /**
     * 生成啟用碼
     * @param in
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/IdGateN915", method = {RequestMethod.POST},
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public Object idGateN915( @Valid @RequestBody IdGateN915_In in) throws JsonProcessingException {
        log.info("idGateN915....");
        log.info(ESAPIUtil.vaildLog("IdGateN915_In>> " + CodeUtil.toJson(in)));
        return data_Retouch(idgate.idGateN915(in));
    }
    /**
     * 啟用碼驗證 後續用registerCallback 做綁定(註冊)
     * @param in
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/IdGateN955", method = {RequestMethod.POST},
    		produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public Object idGateN955( @Valid @RequestBody IdGateN955_In in) throws JsonProcessingException {
    	log.info("idGateN955....");
    	log.info(ESAPIUtil.vaildLog("IdGateN955_In>> " + CodeUtil.toJson(in)));
    	return data_Retouch(idgate.idGateN955(in));
    }

    //***************************************************
    //*                                                 *
    //*                      登入                                              *
    //*                                                 *
    //***************************************************
    
    /**
     * 快速登入(對標N911)
     * @param in
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/IdGateN93B", method = {RequestMethod.POST},
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public Object idGateN93B( @Valid @RequestBody IdGateN93B_In in) throws JsonProcessingException {
        log.info("idGateN93B....");
        log.info(ESAPIUtil.vaildLog("IdGateN93B_In>> " + CodeUtil.toJson(in)));
//		IdGateN939_In n939in = new IdGateN939_In();
//		n939in.setDEVICEID(in.getDEVICEID());
//		n939in.setIDGATEID(in.getIDGATEID());
//		n939in.setLOGINTYPE(in.getLOGINTYPE());
//		IDGateResult n939rs = idgate.idGateN939(n939in);
//		if(!n939rs.getResult()) {
//			return data_Retouch(n939rs);
//		}
//		IdGateN939_Out n939out = (IdGateN939_Out)n939rs.getData();
//		if(!n939out.getRSPCOD().equals("0000")) {
//			return data_Retouch(n939rs);
//		}else if(!n939out.getQCKLGIN().equals("Y")){
//			n939rs.setData(null);
//			n939rs.setMsgCode("ZX99");
//			n939rs.setMsgName("未設定快速登入");
//			n939rs.setResult(Boolean.FALSE);
//			return data_Retouch(n939rs);
//		}
        return data_Retouch(idgate.idGateN93B(in));
    }

    //***************************************************
    //*                                                 *
    //*        設定快速登入、快速交易、小額交易                       *
    //*                                                 *
    //***************************************************
    
    /**
     * 設定快速登入、快速交易、小額交易
     * @param in
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/IdGateN93A", method = {RequestMethod.POST},
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public Object idGateN93A( @Valid @RequestBody IdGateN93A_In in) throws JsonProcessingException {
        log.info("idGateN93A....");
        log.info(ESAPIUtil.vaildLog("IdGateN93A_In>> " + CodeUtil.toJson(in)));
        return data_Retouch(idgate.idGateN93A(in));
    }
    
    /**
     * 取得快速登入、快速交易、小額交易設定
     * @param in
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/IdGateN939", method = {RequestMethod.POST},
    		produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public Object idGateN939( @Valid @RequestBody IdGateN939_In in) throws JsonProcessingException {
    	log.info("idGateN939....");
    	log.info(ESAPIUtil.vaildLog("IdGateN939_In>> " + CodeUtil.toJson(in)));
    	return data_Retouch(idgate.idGateN939(in));
    }

    //***************************************************
    //*                                                 *
    //*           QUICKLOGINMAPPING修改查詢                           *
    //*                                                 *
    //***************************************************
    
    @RequestMapping(value = "/IdGateQuerySession", method = {RequestMethod.POST},
    		produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public Object idGateQuerySession( @Valid @RequestBody IdGateQuerySession_In in) throws JsonProcessingException {
    	log.info("idGateQuerySession....");
    	log.info(ESAPIUtil.vaildLog("IdGateQuerySession_In>> " + CodeUtil.toJson(in)));
    	return data_Retouch(idgate.idGateQuerySession(in));
    }
    
    @RequestMapping(value = "/IdGateMappingQuery", method = {RequestMethod.POST},
    		produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public Object idGateMappingQuery( @Valid @RequestBody IdGateMappingQuery_In in) throws JsonProcessingException {
    	log.info("idGateMappingQuery....");
    	log.info(ESAPIUtil.vaildLog("IdGateMappingQuery_In>> " + CodeUtil.toJson(in)));
    	return data_Retouch(idgate.idGateMappingQuery(in));
    }
    
    @RequestMapping(value = "/IdGateMappingUpdate", method = {RequestMethod.POST},
    		produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public Object idGateMappingUpdate( @Valid @RequestBody IdGateMappingUpdate_In in) throws JsonProcessingException {
    	
    	log.info("idGateMappingUpdate....");
    	log.info(ESAPIUtil.vaildLog("idGateMappingUpdate_In>> " + CodeUtil.toJson(in)));
    	return data_Retouch(idgate.idGateMappingUpdate(in));
    }

    @RequestMapping(value = "/IdGateMappingDelete", method = {RequestMethod.POST},
    		produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public Object idGateMappingDelete( @Valid @RequestBody IdGateMappingDelete_In in) throws JsonProcessingException {
    	log.info("idGateMappingDelete....");
    	log.info(ESAPIUtil.vaildLog("idGateMappingDelete_In>> " + CodeUtil.toJson(in)));
    	return data_Retouch(idgate.idGateMappingDelete(in));
    }
    
    @RequestMapping(value = "/IdGateQuickSessionId", method = {RequestMethod.POST},
    		produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public Object idGateQuickSessionId( @Valid @RequestBody IdGateQuickSessionId_In in) throws JsonProcessingException {
    	log.info("idGateQuickSessionId....");
    	log.info(ESAPIUtil.vaildLog("IdGateQuickSessionId_In>> " + CodeUtil.toJson(in)));
    	return data_Retouch(idgate.idGateQuickSessionId(in));
    }
    
    @RequestMapping(value = "/CloseQuick", method = {RequestMethod.POST},
    		produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public Object closeQuick( @Valid @RequestBody CloseQuick_In in) throws JsonProcessingException {
    	log.info("closeQuick....");
    	log.info(ESAPIUtil.vaildLog("CloseQuick_In>> " + CodeUtil.toJson(in)));
    	return data_Retouch(idgate.closeQuick(in));
    }
    
    @RequestMapping(value = "/SetSmallPay", method = {RequestMethod.POST},
    		produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public Object setSmallPay( @Valid @RequestBody SetSmallPay_In in) throws JsonProcessingException {
    	log.info("setSmallPay....");
    	log.info(ESAPIUtil.vaildLog("setSmallPay_In>> " + CodeUtil.toJson(in)));
    	return data_Retouch(idgate.setSmallPay(in));
    }

    //***************************************************
    //*                                                 *
    //*               直達IDGATE SERVER                 *
    //*                                                 *
    //***************************************************
    
    /**
     * 取得裝置狀態
     * @param in
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/svGet_Device_Status", method = {RequestMethod.POST},
    		produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public Object svGet_Device_Status( @Valid @RequestBody Get_Device_Status_In in) throws JsonProcessingException {
    	log.info("svGet_Device_Status....");
    	log.info(ESAPIUtil.vaildLog("Get_Device_Status_In>>" + CodeUtil.toJson(in)));
    	return idgate.svGet_Device_Status(in);
    }
    /**
     * idgateID 解鎖
     * @param in
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/svUnlock_Device", method = {RequestMethod.POST},
    		produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public Object svUnlock_Device( @Valid @RequestBody SvUnlock_Device_In in) throws JsonProcessingException {
    	
    	log.info("svUnlock_Device....");
    	log.info(ESAPIUtil.vaildLog("SvUnlock_Device_In>> " + CodeUtil.toJson(in)));
    	
    	
    	return idgate.svUnlock_Device(in);
    	
    	
    }
    
    /**
     * idgateID 鎖定/停用
     * @param in
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/svLock_Device", method = {RequestMethod.POST},
    		produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public Object svLock_Device( @Valid @RequestBody SvLock_Device_In in) throws JsonProcessingException {
    	
    	log.info("svLock_Device....");
    	log.info(ESAPIUtil.vaildLog("SvLock_Device_In>> " + CodeUtil.toJson(in)));
    	
    	
    	return idgate.svLock_Device(in);
    	
    	
    }
    /**
     * idgateID 註銷
     * @param in
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/svDeRegister", method = {RequestMethod.POST},
    		produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public Object svDeRegister( @Valid @RequestBody SvDeRegister_In in) throws JsonProcessingException {
    	
    	log.info("svDeRegister....");
    	log.info(ESAPIUtil.vaildLog("SvDeRegister_In>> " + CodeUtil.toJson(in)));
    	
    	
    	return idgate.svDeRegister(in);
    	
    	
    }
    
    /**
     * idgateID Txn生成
     * @param in
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/svGet_Txn_Status", method = {RequestMethod.POST},
    		produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public Object svGet_Txn_Status( @Valid @RequestBody SvGet_Txn_Status_In in) throws JsonProcessingException {
    	
    	log.info("svGet_Txn_Status....");
    	log.info(ESAPIUtil.vaildLog("SvGet_Txn_Status_In>> " + CodeUtil.toJson(in)));
    	
    	
    	return data_Retouch(idgate.svGet_Txn_Status(in));
    	
    	
    }
    /**
     * idgateID Txn生成
     * @param in
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/svCancel_Txn", method = {RequestMethod.POST},
    		produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public Object svCancel_Txn( @Valid @RequestBody SvCancel_Txn_In in) throws JsonProcessingException {
    	
    	log.info("svCancel_Txn....");
    	log.info(ESAPIUtil.vaildLog("SvCancel_Txn_In>> " + CodeUtil.toJson(in)));
    	
    	
    	return data_Retouch(idgate.svCancel_Txn(in));
    	
    	
    }
    /**
     * idgateID Txn生成
     * @param in
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/svCreate_Verify_Txn", method = {RequestMethod.POST},
    		produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public Object svCreate_Verify_Txn( @Valid @RequestBody SvCreate_Verify_Txn_In in) throws JsonProcessingException {
    	
    	log.info("svCreate_Verify_Txn....");
    	log.info(ESAPIUtil.vaildLog("SvCreate_Verify_Txn_In>> " + CodeUtil.toJson(in)));
    	//解決 Reflected XSS All Clients
    	SvCreate_Verify_Txn_In okIn =CodeUtil.objectCovert(SvCreate_Verify_Txn_In.class, ESAPIUtil.validMap(CodeUtil.objectCovert(Map.class, in)));
    	log.trace(ESAPIUtil.vaildLog("SvCreate_Verify_Txn_OkIn>> " + CodeUtil.toJson(okIn)));
    	
    	QUICKLOGINMAPPING po = null;
    	IDGateResult sRes = new IDGateResult();
    	po = quickloginmappingdao.getDataByIdGateId(okIn.getIdgateID());
    	if (po!=null && "Y".equals(po.getQCKTRAN())) {
    		return data_Retouch(idgate.svCreate_Verify_Txn(okIn));
		}else {
			sRes.setMsgCode("ZX99");
			sRes.setMsgName("未設定快速交易");
			sRes.setResult(Boolean.FALSE);
			return data_Retouch(sRes);
		}
//		IdGateN939_In n939in = new IdGateN939_In();
//		n939in.setDEVICEID(okIn.getDeviceID());
//		n939in.setIDGATEID(okIn.getIdgateID());
//		n939in.setLOGINTYPE(okIn.getLOGINTYPE());
//		
//		IDGateResult n939rs = idgate.idGateN939(n939in);
//		if(!n939rs.getResult()) {
//			return data_Retouch(n939rs);
//		}
//		IdGateN939_Out n939out = (IdGateN939_Out)n939rs.getData();
//		if(!n939out.getRSPCOD().equals("0000")) {
//			return data_Retouch(n939rs);
//		}else if(!n939out.getQCKTRAN().equals("Y")){
//			n939rs.setData(null);
//			n939rs.setMsgCode("ZX99");
//			n939rs.setMsgName("未設定快速交易");
//			n939rs.setResult(Boolean.FALSE);
//			return data_Retouch(n939rs);
//		}
//    	
//    	return data_Retouch(idgate.svCreate_Verify_Txn(okIn));
    	
    	
    }
    
    @RequestMapping(value = "/GetSessionId", method = {RequestMethod.POST},
    		produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public Object getSessionId( @Valid @RequestBody GetSessionId_In in) throws JsonProcessingException {
    	
    	log.info("getSessionId....");
    	log.info(ESAPIUtil.vaildLog("getSessionId_In>> " + CodeUtil.toJson(in)));
    	//解決 Reflected XSS All Clients
    	GetSessionId_In okIn =CodeUtil.objectCovert(GetSessionId_In.class, ESAPIUtil.validMap(CodeUtil.objectCovert(Map.class, in)));
    	if(log.isTraceEnabled()) {
    		log.trace(ESAPIUtil.vaildLog("getSessionId_OkIn>> " + CodeUtil.toJson(okIn)));
    	}
    	
    	
    	return data_Retouch(idgate.getSessionId(okIn));
    	
    	
    }
    
    
    
    
    //***************************************************
    //*                                                 *
    //*                    Callback                     *
    //*                                                 *
    //***************************************************
     
    
    /**
    *
    *由idgate Serer 主動呼叫
	 *告知銀行端修改方式的使用者的idgateID與值
    * @param in
    * @return
    * @throws JsonProcessingException
    */
   @RequestMapping(value = "/txnStatusCallback", method = {RequestMethod.POST},
   		produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
   @ResponseBody
   public Object txnStatusCallback( @Valid @RequestBody TxnStatusCallback_In in, BindingResult result) {
	    
		Map<String, List<String>> errors = getErrorMap(result);
		if (errors.size() > 0) {
			TxnStatusCallback_Out out = new TxnStatusCallback_Out();
			out.setReturnCode("0002");
			out.setReturnMsg(CodeUtil.toJson(errors));
			return out;
		}

		//修改Reflected XSS All Clients
	    Map<String,String> reqParam = CodeUtil.objectCovert(Map.class, in);
	    Map<String,String> okMap = ESAPIUtil.validStrMap(reqParam);
	    TxnStatusCallback_In okIn = CodeUtil.objectCovert(TxnStatusCallback_In.class, okMap);
	    
	   	log.info("txnStatusCallback....");
	   	log.info(ESAPIUtil.vaildLog("TxnStatusCallback_In>> " + CodeUtil.toJson(okIn)));
	   	TxnStatusCallback_Out baseResult = idgate.txnStatusCallback(okIn);

		try {
			IDGATEHISTORY history = new IDGATEHISTORY();
			history.setID(UUID.randomUUID().toString());
			QUICKLOGINMAPPING qmp = quickloginmappingdao.getDataByIdGateId(in.getIdgateID());
			if(qmp != null) {
				history.setCUSIDN(qmp.getPks().getCUSIDN());
				history.setDEVICEBRAND(qmp.getDEVICEBRAND());
				history.setBINDSTATUS("0");
			}
			history.setIDGATEID(in.getIdgateID());
			IDGATE_TXN_STATUS_PK pks = new IDGATE_TXN_STATUS_PK();
			pks.setIDGATEID(in.getIdgateID());
			pks.setSESSIONID(in.getSessionID());
			pks.setTXNID(in.getTxnID());
			IDGATE_TXN_STATUS po = idgate_txn_statusdao.findById(pks);
			if(po != null) {
				history.setADOPID(po.getADOPID());
			}
			if(baseResult.getReturnCode().equals("0000")) {
				history.setSTATUS("0");
			}else {
				history.setSTATUS("1");
			}
			history.setTRANDATE(DateUtil.getDate(""));
			history.setTRANTIME(DateUtil.getTheTime(""));
			history.setLOGINTYPE("MB");
			idGateHistoryDao.save(history);
		} catch (Exception e) {
			log.error("idgate history 紀錄失敗 {}",e);
		}
		
	   	return baseResult;
   }
    
    /**
    *
    *由idgate Serer 主動呼叫
	 *告知銀行端修改方式的使用者的idgateID與值
    * @param in
    * @return
    * @throws JsonProcessingException
    */
   @RequestMapping(value = "/changeSettingCallback", method = {RequestMethod.POST},
   		produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
   @ResponseBody
   public Object changeSettingCallback( @Valid @RequestBody ChangeSettingCallback_In in, BindingResult result) {

		Map<String, List<String>> errors = getErrorMap(result);
		if (errors.size() > 0) {
			ChangeSettingCallback_Out out = new ChangeSettingCallback_Out();
			out.setReturnCode("0002");
			out.setReturnMsg(CodeUtil.toJson(errors));
			return out;
		}
		
		//修改Reflected XSS All Clients
	    Map<String,Object> reqParam = CodeUtil.objectCovert(Map.class, in);
	    Map<String,Object> okMap = ESAPIUtil.validMap(reqParam);
	    ChangeSettingCallback_In okIn = CodeUtil.objectCovert(ChangeSettingCallback_In.class, okMap);
	    
	   	log.info("changeSettingCallback....");
	   	log.info(ESAPIUtil.vaildLog("ChangeSettingCallback_In>> " + CodeUtil.toJson(okIn)));
	   	ChangeSettingCallback_Out baseResult = idgate.changeSettingCallback(okIn);

		try {
			IDGATEHISTORY history = new IDGATEHISTORY();
			history.setID(UUID.randomUUID().toString());
			QUICKLOGINMAPPING qmp = quickloginmappingdao.getDataByIdGateId(in.getIdgateID());
			if(qmp != null) {
				history.setCUSIDN(qmp.getPks().getCUSIDN());
				history.setDEVICEBRAND(qmp.getDEVICEBRAND());
				history.setBINDSTATUS("0");
			}
			history.setIDGATEID(in.getIdgateID());
			history.setADOPID("changeSettingCallback");
			if(baseResult.getReturnCode().equals("0000")) {
				history.setSTATUS("0");
			}else {
				history.setSTATUS("1");
			}
			history.setTRANDATE(DateUtil.getDate(""));
			history.setTRANTIME(DateUtil.getTheTime(""));
			history.setLOGINTYPE("MB");
			idGateHistoryDao.save(history);
		} catch (Exception e) {
			log.error("idgate history 紀錄失敗 {}",e);
		}
		
	   	return baseResult;
   }
    /**
     *
     *由idgate Serer 主動呼叫
	 *告知銀行端登入的使用者的idgateID
     * @param in
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/loginStatusCallback", method = {RequestMethod.POST},
    		produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public Object loginStatusCallback( @Valid @RequestBody LoginStatusCallback_In in, BindingResult result){

		Map<String, List<String>> errors = getErrorMap(result);
		if (errors.size() > 0) {
			LoginStatusCallback_Out out = new LoginStatusCallback_Out();
			out.setReturnCode("0002");
			out.setReturnMsg(CodeUtil.toJson(errors));
			return out;
		}

		//修改Reflected XSS All Clients
	    Map<String,String> reqParam = CodeUtil.objectCovert(Map.class, in);
	    Map<String,String> okMap = ESAPIUtil.validStrMap(reqParam);
	    LoginStatusCallback_In okIn = CodeUtil.objectCovert(LoginStatusCallback_In.class, okMap);
	    
    	log.info("loginStatusCallback....");
    	log.info(ESAPIUtil.vaildLog("LoginStatusCallback_In>> " + CodeUtil.toJson(okIn)));
    	LoginStatusCallback_Out baseResult = idgate.loginStatusCallback(okIn);
		try {
			IDGATEHISTORY history = new IDGATEHISTORY();
			history.setID(UUID.randomUUID().toString());
			QUICKLOGINMAPPING qmp = quickloginmappingdao.getDataByIdGateId(in.getIdgateID());
			if(qmp != null) {
				history.setCUSIDN(qmp.getPks().getCUSIDN());
				history.setDEVICEBRAND(qmp.getDEVICEBRAND());
				history.setBINDSTATUS("0");
			}
			history.setIDGATEID(in.getIdgateID());
			history.setADOPID("loginStatusCallback");
			if(baseResult.getReturnCode().equals("0000")) {
				history.setSTATUS("0");
			}else {
				history.setSTATUS("1");
			}
			if (qmp.getQCKLGIN()!=null && "Y".equals(qmp.getQCKLGIN())) {
				history.setADOPID("loginStatusQCKLGIN");
			}
			if (qmp.getQCKTRAN()!=null && "Y".equals(qmp.getQCKTRAN())) {
				history.setADOPID("loginStatusQCKTRAN");
			}
			history.setTRANDATE(DateUtil.getDate(""));
			history.setTRANTIME(DateUtil.getTheTime(""));
			history.setLOGINTYPE("MB");
			idGateHistoryDao.save(history);
		} catch (Exception e) {
			log.error("idgate history 紀錄失敗 {}",e);
		}
		
    	return baseResult;
    }
    /**
     *
     *由idgate Serer 主動呼叫
	 *告知銀行端新註冊的使用者的idgateID
     * @param in
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/registerCallback", method = {RequestMethod.POST},
    		produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public Object registerCallback( @Valid @RequestBody RegisterCallback_In in, BindingResult result) {

		Map<String, List<String>> errors = getErrorMap(result);
		if (errors.size() > 0) {
			RegisterCallback_Out out = new RegisterCallback_Out();
			out.setReturnCode("0002");
			out.setReturnMsg(CodeUtil.toJson(errors));
			return out;
		}

		//修改Reflected XSS All Clients
	    Map<String,Object> reqParam = CodeUtil.objectCovert(Map.class, in);
	    Map<String,Object> okMap = ESAPIUtil.validMap(reqParam);
	    RegisterCallback_In okIn = CodeUtil.objectCovert(RegisterCallback_In.class, okMap);
	    
    	log.info("registerCallback....");
    	log.info(ESAPIUtil.vaildLog( "RegisterCallback_In>> " + CodeUtil.toJson(okIn)));
    	RegisterCallback_Out baseResult = idgate.registerCallback(okIn);
		try {
			IDGATEHISTORY history = new IDGATEHISTORY();
			history.setID(UUID.randomUUID().toString());
			QUICKLOGINMAPPING qmp = quickloginmappingdao.getDataByIdGateId(in.getIdgateID());
			if(qmp != null) {
				history.setCUSIDN(qmp.getPks().getCUSIDN());
				history.setDEVICEBRAND(qmp.getDEVICEBRAND());
			}
			history.setIDGATEID(in.getIdgateID());
			if(baseResult.getReturnCode().equals("0000")) {
				history.setBINDSTATUS("0");
			}else {
				history.setBINDSTATUS(baseResult.getReturnCode());
			}
			history.setBINDDATE(DateUtil.getDate(""));
			history.setBINDTIME(DateUtil.getTheTime(""));
			
			history.setLOGINTYPE("MB");
			log.trace(ESAPIUtil.vaildLog("history is >> " + history));
			idGateHistoryDao.save(history);
		} catch (Exception e) {
			log.error("idgate history 紀錄失敗 {}",e);
		}
    	
    	return baseResult;
    	
    	
    }
    

    //***************************************************
    //*                                                 *
    //*                      工具                                             *
    //*                                                 *
    //***************************************************
    
    public HashMap data_Retouch(IDGateResult irs) {
		HashMap<String,Object> dataMap = new HashMap<String,Object>();
    	try {
    		dataMap.put("data",irs.getData());
    		dataMap.put("MSGCOD",irs.getMsgCode());
    		dataMap.put("msgName",irs.getMsgName());
    		dataMap.put("messages",irs.getMessages());
    		dataMap.put("result",irs.getResult());
		} catch (Exception e) {
			log.error("data_Retouch error >> {}",e);
		}finally {
			setMsgCode(dataMap);
		}
		return dataMap;
    	
    }
    
	public void setMsgCode(Map dataMap) {
		String val = "";
		try {
			for (String key : list) {
				if (dataMap.containsKey(key)) {
					val = (String) dataMap.get(key);
					log.trace(ESAPIUtil.vaildLog("val>>{}"+val));
					
					if ("R000".equals(val) || "OKLR".equals(val) || "OKOK".equals(val) || "".equals(val) 
							||"0000000".equals(val)|| "0000".equals(val)||"OKOV".equals(val)) {
						dataMap.put("msgCode", "0");
					} else {
						dataMap.put("msgCode", dataMap.get(key));
					}
					break;
				} 
			}
		} catch (Exception e) {
			log.error("setMsgCode error >> {}",e);
			dataMap.put("msgCode", "FE0003");
		}
	}
	

	/**
	 * 取得 BindingResult 錯誤結果
	 * @param result
	 * @return		Result HashMap, key: 欄位名稱, value: Bean Validation 失敗原因, 會有多筆, 所以用 LIST
	 */
	protected Map<String, List<String>> getErrorMap(BindingResult result) {
		Map<String, List<String>> errors = new HashMap<String, List<String>>();
		List<FieldError> fieldErrors = result.getFieldErrors();

		for ( FieldError fieldError: fieldErrors ) {
			if ( errors.keySet().contains(fieldError.getField())) {
				errors.get(fieldError.getField()).add(fieldError.getDefaultMessage());
			} else {
				List<String> ls = new ArrayList<String>();
				ls.add(fieldError.getDefaultMessage());
				
				errors.put(fieldError.getField(), ls);
				
			}
		}
		return errors;
	}
}
