package com.netbank.rest.aop;


import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Repeatable(TxnLoggerFields.class)
public @interface TxnLoggerField {
    TxnValueFrom from() default TxnValueFrom.IN;
    TxnAssignField assignField() default TxnAssignField.NONE;
    String paramName() default "";
    String value() default "";  // 固定值

}
