package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netbank.rest.model.CommonPools;
import com.netbank.util.AcnoTypeEnum;
import com.netbank.util.Collectionutils;
import com.netbank.util.ESAPIUtil;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import lombok.extern.slf4j.Slf4j;

/**
 * N922電文POOL
 */
@Service
@Slf4j
public class N921_Tel_Service extends Common_Tel_Service {

	@Autowired
	private CommonPools commonPools;

	@Override
	public HashMap pre_Processing(Map request) {
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) {
		String uid = (String) request.get("CUSIDN");
		String type = (String) request.get("TYPE");
		log.debug(ESAPIUtil.vaildLog("uid={}"+ uid));
		MVH mvh = new MVHImpl();
		// call N921Pool
		try {
			commonPools.n921.removeFromPool(uid);
			String typeUp = type.toUpperCase();
			String[] acnotype = AcnoTypeEnum.valueOf(typeUp).getAcnotype();
			mvh = commonPools.n921.getAcnoList(uid, acnotype);

		} catch (NullPointerException e) {
			log.error(ESAPIUtil.vaildLog("TYPE : {}" +type));
			log.error("process.error>>{}", e);
		} catch (IllegalArgumentException e) {
			log.error("", e);
			log.error(ESAPIUtil.vaildLog("TYPE : {}"+ type));
			for (AcnoTypeEnum acnoTypeEnum : AcnoTypeEnum.values()) {
				log.error("TYPE Can only enter : {}", acnoTypeEnum.toString());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error("", e);
		}
		return data_Retouch(mvh);
	}

	@Override
	public HashMap data_Retouch(MVH mvh) {
		MVHImpl mvhimpl = null;
		LinkedList<Row> tmplist = null;
		Boolean result = Boolean.FALSE;
		HashMap<String,Object> dataMap = new HashMap<String,Object>();
		HashMap<String,String> rowMap = new HashMap<String,String>();
		LinkedList<HashMap<String,String>> rowlist = null;
    	try {
    		mvhimpl = (MVHImpl) mvh;
    	log.info("getFlatValues>>"+mvhimpl.getFlatValues());
    	log.info("result.getInputQueryData()>>"+mvhimpl.getInputQueryData());
    	log.info("result.getInputQueryData()>>"+mvhimpl.getOccurs().getRows());
			Iterator<Row> tmp = mvhimpl.getOccurs().getRows().iterator();
			tmplist = new LinkedList<Row>();
			rowlist = new LinkedList<HashMap<String,String>>();
			tmp.forEachRemaining(tmplist::add);
			log.info("datalist()>>"+tmplist);
			for(Row  row:tmplist) {
				log.info("getValues>>"+row.getValues());
				rowMap = new HashMap<String,String>();
				rowMap.putAll(row.getValues());
				rowlist.add(rowMap);
			}
			dataMap.putAll(mvhimpl.getFlatValues());
			Collectionutils.removeMapEmptyValue(rowlist, "ACN");
			dataMap.put("REC", rowlist);
			result = Boolean.TRUE;
			dataMap.put("MSGCOD", "");
		} catch (Exception e) {
			log.error("data_Retouch error >> {}",e);
		}finally {
			setMsgCode(dataMap,result);
		}
		return dataMap;
	}

	@Override
	public void setMsgCode(Map dataMap, Boolean data_Retouch_result) {
		// TODO Auto-generated method stub
		super.setMsgCode(dataMap, data_Retouch_result);
	}
}
