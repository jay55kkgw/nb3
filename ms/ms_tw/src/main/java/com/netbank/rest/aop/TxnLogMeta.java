package com.netbank.rest.aop;

import com.google.gson.Gson;
import fstop.model.MVH;
import fstop.orm.po.TXNLOG;
import fstop.util.StrUtils;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class TxnLogMeta {

    TxnLogger txnLogger;
    TxnLoggerField[] txnLoggerFields;

    Map<String, Object> params;


    public TXNLOG toTxnLog(MVH outValue) {

        if (params == null)
            params = new HashMap<>();

        TXNLOG l = new TXNLOG();
        if (txnLogger != null) {
            l.setADOPID(txnLogger.ADOPID());
        }

        Gson g = new Gson();
        l.setADCONTENT(g.toJson(params));

        if (txnLoggerFields != null) {
            for (TxnLoggerField txnLoggerField : txnLoggerFields) {
                if (txnLoggerField.from() == TxnValueFrom.IN) {
                    String val = "";
                    if (StrUtils.isEmpty(txnLoggerField.paramName())) {
                        val = txnLoggerField.value();
                    } else {
                        Object o = params.get(txnLoggerField.paramName());
                        if (o != null) {
                            if (o instanceof String) {
                                val = (String) o;
                            } else {
                                val = o.toString();
                            }
                        }
                    }
                    txnLoggerField.assignField().assignValue(l, val);
                }
                if (txnLoggerField.from() == TxnValueFrom.OUT) {
                    if (outValue != null) {
                        String val = "";
                        if (StrUtils.isEmpty(txnLoggerField.paramName())) {
                            val = txnLoggerField.value();
                        } else {
                            val = outValue.getValueByFieldName(txnLoggerField.paramName());

                        }
                        txnLoggerField.assignField().assignValue(l, val);

                    }
                }
            }
        }


        return l;
    }

}
