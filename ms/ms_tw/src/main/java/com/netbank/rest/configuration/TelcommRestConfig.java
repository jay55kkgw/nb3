package com.netbank.rest.configuration;

import com.netbank.telcomm.CommonTelcomm;
import com.netbank.telcomm.DataPowerCommonTelcomm;
import com.netbank.telcomm.DataPower_CommonTelcomm;
import com.netbank.telcomm.MockRestValueCommonTelcomm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

@Configuration
@Slf4j
@ConditionalOnProperty(name = "mock.telcomm.datasource", matchIfMissing=true, havingValue = "REST")
public class TelcommRestConfig extends AbstractTelcommConfig {

    @Value("${mock.telcomm.url:http://localhost:8880/rest/}")
    private String mockRestBaseUrl;

    protected CommonTelcomm createTelcomm(String mockfileName, String txid) throws IOException {

        MockRestValueCommonTelcomm ret = new MockRestValueCommonTelcomm();

        ret.setMockFileName(mockfileName);
        String url = mockRestBaseUrl;
        if(url.endsWith("/") == false) {
            url = url + "/";
        }

        url = url + txid.toLowerCase() + "Telcomm";
        ret.setUrl(url);

        ret.setTxid(txid);

        log.info("create rest mock telcomm, txid : {}, url: {}", ret.getTxid(), ret.getUrl());

        return ret;
    }


    protected DataPower_CommonTelcomm createDataPowerTelcomm(String mockfileName, String txid) throws IOException {

    	DataPower_CommonTelcomm ret = new DataPowerCommonTelcomm();

        return ret;
    }
    
}
