package com.netbank.rest.aop;

import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.netbank.rest.web.context.UserContext;
import com.netbank.util.StrUtils;
import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.orm.dao.TxnLogDao;
import fstop.orm.po.TXNLOG;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.yaml.snakeyaml.Yaml;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutorService;

@Slf4j
@Aspect
@Component
public class TxnLoggerAop {


    static ThreadLocal<String> url = new ThreadLocal<String>();

    @Autowired
    private Yaml yaml;

    @Autowired
    @Qualifier("txnlogExecutorService")
    ExecutorService executorService;

    @Autowired
    private TxnLogDao txnLogDao;


    @Pointcut("@annotation(com.netbank.rest.aop.TxnLogger)")
    public void requestMapping() {
    }

    public Map<String, Object> logParams(JoinPoint joinPoint) {
//        MethodSignature signature = (MethodSignature) joinPoint.getSignature();


//        List<String> params = Arrays.asList(signature.getParameterNames());
        int c = 0;
        Map<String, Object> webParams = Maps.newHashMap();
        Object[] signatureArgs = joinPoint.getArgs();
        if(signatureArgs != null && signatureArgs.length > 0) {
            return (Map)signatureArgs[0];
        }
        return webParams;

    }

    private TxnLogMeta loadTxnMeta(JoinPoint joinPoint) {

        Map<String, Object> params = logParams(joinPoint);
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();

        TxnLogger txnLogger = method.getAnnotation(TxnLogger.class);
        TxnLoggerField[] fields = method.getAnnotationsByType(TxnLoggerField.class);


        TxnLogMeta ret = new TxnLogMeta();

        ret.setTxnLogger(txnLogger);
        ret.setTxnLoggerFields(fields);
        ret.setParams(params);

        return ret;
    }


    @Around("requestMapping() ")
    public Object execAround(ProceedingJoinPoint joinPoint) throws Throwable {

        // 由 header ("x-txnlog") 來控制是否要寫入 TxnLog
//        if(!"true".equalsIgnoreCase(UserContext.getRequest()
//                .map(r -> r.getHeader("x-txnlog"))
//                .filter(v -> v != null)
//                .orElse(""))) {
//            Object ret = joinPoint.proceed();
//            return ret;
//        }

        TxnLogMeta meta = loadTxnMeta(joinPoint);
        TXNLOG txnlog = null;
        Object ret = null;
        try {
            ret = joinPoint.proceed();
            txnlog = meta.toTxnLog((MVH) ret);
        } catch (Exception e) {
            try {
                txnlog = meta.toTxnLog((MVH) ret);
            } catch (Throwable ex) {
                log.error("無法產生 txnlog", e);
            }

            if (e instanceof TopMessageException) {
                TopMessageException ex = (TopMessageException) e;
                String ADEXCODE = ex.getMsgcode();
                String ADTXID = ex.getAppCode();
                txnlog.setADEXCODE(ADEXCODE);
                txnlog.setADTXID(ADTXID);
            }

        }

        try {

            DateTime now = new DateTime();

            String guid = UUID.randomUUID().toString().replaceAll("-", "");
            txnlog.setADGUID(guid);
            txnlog.setLOGINTYPE("");
            txnlog.setLOGINTYPE((String)meta.getParams().get("LOGINTYPE"));  ////  判斷NB:網銀，MB:行動
//            txnlog.setADUSERID(UserContext.getRequest()
//                    .map(r -> (String)r.getAttribute("ADUSERID"))
//                    .filter(c -> c != null)
//                    .orElse(""));
//            txnlog.setADUSERIP(UserContext.getRequest()
//                    .map(r -> (String)r.getAttribute("ADUSERIP"))
//                    .filter(c -> c != null)
//                    .orElse(""));
            String loginUser = StrUtils.trim((String)meta.getParams().get("CUSID"));
            txnlog.setADUSERID((String)meta.getParams().get("CUSID"));  ////  ADUSERID
//            String loginUser = UserContext.getRequest()
//                    .map(r -> (String) r.getAttribute("ADUSERID"))
//                    .filter(c -> c != null)
//                    .orElse("");
            txnlog.setLASTDATE(now.toString("yyyyMMdd"));
            txnlog.setLASTTIME(now.toString("HHmmss"));
            txnlog.setADTXNO(txnlog.getLASTDATE() + txnlog.getLASTTIME() + loginUser);


            Gson g = new Gson();
            log.info("txnlog: " + g.toJson(txnlog));

            final TXNLOG saveObject = txnlog;
//            executorService.submit(() -> {
//                try {
//                    txnLogDao.save(saveObject);
//                } catch (Throwable e) {
//                    log.error("無法寫入 txnlog.", e);
//                }
//            });
        } catch (Throwable e) {
            log.error("無法寫入 txnlog.", e);
        }

        return ret;
    }


}

