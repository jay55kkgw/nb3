package com.netbank.rest.configuration;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;

import com.netbank.telcomm.CommonTelcomm;
import com.netbank.telcomm.DataPowerCommonTelcomm;
import com.netbank.telcomm.DataPower_CommonTelcomm;
import com.netbank.telcomm.TMRACommonTelcomm;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
@ConditionalOnProperty(name = "mock.telcomm.datasource", matchIfMissing=true, havingValue = "TMRA")
public class TMRA_RestConfig extends AbstractTelcommConfig {

//    @Value("${tmra.telcomm.url:http://localhost:8880/TMRA/com}")
//    private String restBaseUrl;
    @Value("${tmra.uri.host:http://localhost}")
    private String restHost;
   // @Value("${tmra.uri.port:8080}")
   // private String restPort;
    @Value("${tmra.uri.path:TMRA/com}")
    private String restPath;
    
    @Value("${datapower.uri.host:https://localhost}")
    private String restHost_datapower;
    @Value("${datapower.uri.port:10443}")
    private String restPort_datapower;
    @Value("${datapower.uri.path:bds01/tw}")
    private String restPath_datapower;
    
    protected CommonTelcomm createTelcomm(String mockfileName, String txid) throws IOException {

        TMRACommonTelcomm ret = new TMRACommonTelcomm();

        // http://localhost:8880/TMRA/com
        //String url = restHost + ":" + restPort + "/"+ restPath;
//        log.info("createTelcomm.url: {}", url);
        
        if(restHost.endsWith("/") == false) {
            restHost = restHost + "/";
        }
        String url = restHost + restPath;
        log.info("createTelcomm.url: {}", url);
        
        if(url.endsWith("/") == false) {
            url = url + "/";
        }

        url = url + txid.toLowerCase();
        ret.setUrl(url);

        ret.setTxid(txid);

        log.info("create rest mock telcomm, txid : {}, url: {}", ret.getTxid(), ret.getUrl());

        return ret;
    }

    protected DataPower_CommonTelcomm createDataPowerTelcomm(String mockfileName, String txid) throws IOException {

    	DataPowerCommonTelcomm ret = new DataPowerCommonTelcomm();

//		restHost_datapower = "https://localhost";
//		restPort_datapower = "10443";
//		restPath_datapower = "bds01/tw";
		
		// https://localhost:10443/bds01/tw/a1000
        String url = restHost_datapower + ":" + restPort_datapower + "/"+ restPath_datapower;
        
        if(url.endsWith("/") == false) {
            url = url + "/";
        }

        url = url + txid.toLowerCase();
        
        log.info("createTelcomm.url: {}", url);
        ret.setUrl(url);

        ret.setTxid(txid);

        log.info("create rest mock telcomm, txid : {}, url: {}", ret.getTxid(), ret.getUrl());

        return ret;
    }
    


}
