package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import org.springframework.stereotype.Service;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class N640_Tel_Service extends Common_Tel_Service {
	@Override
	public HashMap pre_Processing(Map request) {
		// TODO Auto-generated method stub
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) {
		// TODO Auto-generated method stub
		return super.process(serviceID, request);
	}

	@Override
	public HashMap data_Retouch(MVH mvh) {
		MVHImpl mvhimpl = null;
		HashMap<String, String> rowMap = null;
		HashMap<String, Object> tableMap = null;
		HashMap<String, Object> dataMap = new HashMap<String, Object>();
		LinkedList<Row> tmplist = null;
		LinkedList<HashMap<String, String>> rowlist = null;
		LinkedList<HashMap<String, Object>> tableList = new LinkedList<HashMap<String, Object>>();
		String tableListTopMsg = "";
		try {
    		mvhimpl = (MVHImpl) mvh;
			log.info("getFlatValues>>"+mvhimpl.getFlatValues());
			tableList = new LinkedList<HashMap<String,Object>>();
    		for(Object key:mvhimpl.getTableKeys()) {
    			log.info("getTableKeys >> {}",(String)key);
    			tableMap = new HashMap<String,Object>();
    			Iterator<Row> tmp = ((MVHImpl)mvhimpl.getTableByName((String)key)).getOccurs().getRows().iterator();
    			tmplist = new LinkedList<Row>();
    			rowlist = new LinkedList<HashMap<String,String>>();
    			tmp.forEachRemaining(tmplist::add);
    			log.info("datalist()>>"+tmplist);
    			for(Row  row:tmplist) {
    				log.info("getValues>>"+row.getValues());
    				rowMap = new HashMap<String,String>();
    				rowMap.putAll(row.getValues());
    				rowlist.add(rowMap);
    				//放入 ABEND 於 table 
					tableMap.put(ABEND, row.getValues().get(ABEND));
    			}// for end
				super.setMsgCode(tableMap, Boolean.TRUE);
				//如果有Table 是 OKOV 則 tableListTopMsg 放OKOV
				if("OKOV".equals(tableMap.get("ABEND"))){
					tableListTopMsg = "OKOV";
				}
				rowlist.removeIf(Map::isEmpty);// 移除空白 OR null的Map
				rowlist.removeIf(Map -> null == Map.get("LSTLTD") || "".equals(Map.get("LSTLTD")) || "0000000".equals(Map.get("LSTLTD")));// 移除LSTLTD==空白 OR null OR　0000000的Map
				tableMap.put("TABLE", rowlist);
				tableMap.put("ACN", (String) key);
				tableList.add(tableMap);
				
			}//for end
			dataMap.putAll(mvhimpl.getFlatValues());
			dataMap.put("REC", tableList);
			//如果有多個Table
			if (tableList.size() > 1) {
			//如果有tableListTopMsg 是 OKOV 則 TOPMSG 放OKOV
					if ("OKOV".equals(tableListTopMsg)) {
						dataMap.put("TOPMSG", "OKOV");
					} else {
						dataMap.put("TOPMSG", "0000");
					}
					super.setMsgCode(dataMap, Boolean.TRUE);
			} else {
				dataMap.put("TOPMSG", tableMap.get(ABEND));
				super.setMsgCode(dataMap, Boolean.TRUE);
			}
			
		} catch (Exception e) {
			log.error("data_Retouch error >> {}",e);
		}
		return dataMap;
	}

	@Override
	public void setMsgCode(Map dataMap, Boolean data_Retouch_result) {
		// TODO Auto-generated method stub
		super.setMsgCode(dataMap, data_Retouch_result);
	}
}
