package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.netbank.util.ESAPIUtil;

import fstop.model.MVH;
import fstop.orm.po.ADMMSGCODE;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class N911_Tel_Service extends Common_Tel_Service  {
	@Override
	public HashMap pre_Processing(Map request) {
		// TODO Auto-generated method stub
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) {
		// TODO Auto-generated method stub
		return super.process(serviceID, request);
	}

	@Override
	public HashMap data_Retouch(MVH mvh) {
		log.trace(ESAPIUtil.vaildLog("N911.data_Retouch.request : "+ mvh));
		
		// 把電文結果不能定義為變數的欄位重新定義回傳RS
		HashMap<Object, Object> resultMap = new HashMap<Object, Object>();
		resultMap = super.data_Retouch(mvh);
		
		try {
			String space = resultMap.get("SPACE(5)").toString();
			log.trace("N911.data_Retouch.SPACE: {}", space);
			resultMap.put("SPACE", space);
			
			String windata = resultMap.get("WINDATA(5)").toString();
			log.trace("N911.data_Retouch.WINDATA: {}", windata);
			resultMap.put("WINDATA", windata);
			
			String area = resultMap.get("AREA(8)").toString();
			log.trace("N911.data_Retouch.AREA: {}", area);
			resultMap.put("AREA", area);
			
		} catch (Exception e) {
			log.error("data_Retouch error >> {}",e);
		}
		return resultMap;
	}

	@Override
	public void setMsgCode(Map dataMap, Boolean data_Retouch_result) {
		// TODO Auto-generated method stub
		super.setMsgCode(dataMap, data_Retouch_result);
	}
	
	
}