package com.netbank.rest.configuration;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;

import com.netbank.telcomm.CommonTelcomm;
import com.netbank.telcomm.DataPowerCommonTelcomm;
import com.netbank.telcomm.DataPower_CommonTelcomm;
import com.netbank.telcomm.MockFileDataCommonTelcomm;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
@ConditionalOnProperty(name = "mock.telcomm.datasource", havingValue = "FILEDATA")
public class TelcommFileDataConfig extends AbstractTelcommConfig {
    @Value("${mock.telcomm.data.dir}")
    private String mockTelcommDataDir;

    protected CommonTelcomm createTelcomm(String mofileName, String txid) throws IOException {
        MockFileDataCommonTelcomm ret = new MockFileDataCommonTelcomm();

        String data = "";
        try {
            if(new File(mockTelcommDataDir, mofileName).exists())
                data = FileUtils.readFileToString(new File(mockTelcommDataDir, mofileName), "UTF-8");
        } catch(Exception e) {
            log.error("無法讀取 MOCK 資料(file: " + new File(mockTelcommDataDir, mofileName).getAbsolutePath() + ")", e);
        }
        ret.setTxid(txid.toUpperCase());
        ret.setMockResult(data);
        return ret;
    }

    protected DataPower_CommonTelcomm createDataPowerTelcomm(String mockfileName, String txid) throws IOException {

    	DataPower_CommonTelcomm ret = new DataPowerCommonTelcomm();

        return ret;
    }

}
