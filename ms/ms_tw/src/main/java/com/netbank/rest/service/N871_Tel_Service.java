package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import org.springframework.stereotype.Service;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import lombok.extern.slf4j.Slf4j;

/**
 * 主要用來
 * 1.丟入MVH前的前置處理，包含檢核;
 * 2.後製MVH回覆的訊息;
 * @author JeffChang
 *
 */
@Service
@Slf4j
public class N871_Tel_Service extends Common_Tel_Service  {

	@Override
	public HashMap pre_Processing(Map request) {
		// TODO Auto-generated method stub
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) {
		// TODO Auto-generated method stub
		return super.process(serviceID, request);
	}

	@Override
	public HashMap data_Retouch(MVH mvh) {
		MVHImpl mvhimpl = null;
		LinkedList<Row> tmplist = null;
		Boolean result = Boolean.FALSE;
		HashMap<String,Object> dataMap = new HashMap<String,Object>();
		HashMap<String, Object> rowMap = new HashMap<String,Object>();
		LinkedList<HashMap<String,String>> rowlist = null;
		HashMap<String,Object> tableMap = null;
		LinkedList<HashMap<String,Object>> tableList = null;
    	try {
    		mvhimpl = (MVHImpl) mvh;
    		tableList = new LinkedList<HashMap<String,Object>>();
    		int count =0;
    		for(Object key:mvhimpl.getTableKeys()) {
    			tableMap = new HashMap<String,Object>();
    			log.info("getTableKeys >> {}",(String)key);
    			log.info("result.getInputQueryData()>> {}",((MVHImpl)mvhimpl.getTableByName((String)key)).getOccurs().getRows());
    			Iterator<Row> tmp = ((MVHImpl)mvhimpl.getTableByName((String)key)).getOccurs().getRows().iterator();
    			tmplist = new LinkedList<Row>();
    			rowlist = new LinkedList<HashMap<String,String>>();
    			tmp.forEachRemaining(tmplist::add);
    			
    			log.info("datalist()>>"+tmplist);
    			for(Row  row:tmplist) {
    					log.info("getValues>>"+row.getValues());
    					rowMap = new HashMap<String,Object>();
    					rowMap.putAll(row.getValues());
    					tableList.add(rowMap);
    			}
    		}
			dataMap.putAll(mvhimpl.getFlatValues());
			dataMap.put("REC", tableList);
			result = Boolean.TRUE;
//			if(tableList.size() == 0)
				dataMap.put("MSGCOD", "");
			
		} catch (Exception e) {
			log.error("data_Retouch error >> {}",e);
		}finally {
			setMsgCode(dataMap,result);
		}
		return dataMap;
	}
	
	@Override
	public void setMsgCode(Map dataMap, Boolean data_Retouch_result) {
		// TODO Auto-generated method stub
		super.setMsgCode(dataMap, data_Retouch_result);
		if("ENRD".equals(dataMap.get("msgCode"))) {
			dataMap.replace("msgCode", "0");
		}
	}
	
}
