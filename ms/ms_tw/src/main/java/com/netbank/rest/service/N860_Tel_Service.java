package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Vector;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netbank.rest.model.CommonPools;
import com.netbank.rest.model.FieldTranslator;
import com.netbank.util.AcnoTypeEnum;
import com.netbank.util.Collectionutils;
import com.netbank.util.StrUtils;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.util.SpringBeanFactory;
import lombok.extern.slf4j.Slf4j;

/**
 * 主要用來 1.丟入MVH前的前置處理，包含檢核; 2.後製MVH回覆的訊息;
 * 
 * @author Hugo
 *
 */
@Service
@Slf4j
public class N860_Tel_Service extends Common_Tel_Service {


	@Autowired
	private CommonPools commonPools;
	
	@Override
	public HashMap pre_Processing(Map request) {
		// TODO Auto-generated method stub
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) {
		// TODO Auto-generated method stub
		return super.process(serviceID, request);
	}

	@Override
	public HashMap data_Retouch(MVH mvh) {
		MVHImpl mvhimpl = null;
		LinkedList<Row> tmplist = null;
		Boolean result = Boolean.FALSE;
		HashMap<String,Object> dataMap = new HashMap<String,Object>();
		HashMap<String,String> rowMap = new HashMap<String,String>();
		LinkedList<HashMap<String,String>> rowlist = null;
		HashMap<String,Object> tableMap = null;
		LinkedList<HashMap<String,Object>> tableList = null;
    	try {
    		mvhimpl = (MVHImpl) mvh;
//    		log.info("getFlatValues>>"+mvhimpl.getFlatValues());
//    		log.info("result.getInputQueryData()>>"+mvhimpl.getInputQueryData());
//    		log.info("result.getTableKeys()>>"+mvhimpl.getTableKeys());
//    		log.info("result.getTableByName(00162965425)>>"+mvhimpl.getTableByName("00162965425"));
    		
    		tableList = new LinkedList<HashMap<String,Object>>();
    		for(Object key:mvhimpl.getTableKeys()) {
    			tableMap = new HashMap<String,Object>();
    			log.info("getTableKeys >> {}",(String)key);
    			log.info("result.getInputQueryData()>> {}",((MVHImpl)mvhimpl.getTableByName((String)key)).getOccurs().getRows());
    			Iterator<Row> tmp = ((MVHImpl)mvhimpl.getTableByName((String)key)).getOccurs().getRows().iterator();
    			tmplist = new LinkedList<Row>();
    			rowlist = new LinkedList<HashMap<String,String>>();
    			tmp.forEachRemaining(tmplist::add);
    			
    			log.info("datalist()>>"+tmplist);
    			for(Row  row:tmplist) {
    				log.info("getValues>>"+row.getValues());
    				rowMap = new HashMap<String,String>();
    				rowMap.putAll(row.getValues());
    				rowlist.add(rowMap);
    			}
    			rowlist.removeIf(Map::isEmpty);// 移除空白 OR null的Map
    			rowlist.removeIf(Map -> null == Map.get("PAYACN") || "".equals(Map.get("PAYACN")));// 移除ACN==空白 OR null的Map
    			tableMap.putAll(((MVHImpl)mvhimpl.getTableByName((String)key)).getFlatValues());
    			tableMap.put("TABLE", rowlist);
    			tableMap.put("ACN", (String)key);
//    			tableMap.put("TOPMSG", (String)topmsg);
//    			tableMap.put("MSGCOD", (String)msgcod);
    			super.setMsgCode(tableMap,  Boolean.TRUE);
    			tableList.add(tableMap);
    		}
			dataMap.putAll(mvhimpl.getFlatValues());
			dataMap.put("TYPE_STR", FieldTranslator.transfer("N860", "TYPE", (String)dataMap.get("TYPE")));
			dataMap.put("REC", tableList);
			result = Boolean.TRUE;
			

			if(tableList.size()>1) {
				for (HashMap<String, Object> hashMap : tableList)
				{
					if("0".equals(hashMap.get("msgCode"))) {
						dataMap.put("msgCode", "0");
						dataMap.put("TOPMSG", hashMap.get("TOPMSG"));
						dataMap.put("MSGCOD", hashMap.get("MSGCOD"));
						break;
					}
					else {
						dataMap.put("msgCode", hashMap.get("msgCode"));
						dataMap.put("TOPMSG", hashMap.get("TOPMSG"));
						dataMap.put("MSGCOD", hashMap.get("MSGCOD"));
					}
				}
				setMsgCode(dataMap, Boolean.TRUE);

			}else {
				dataMap.put("msgCode", tableMap.get("msgCode"));
				dataMap.put("TOPMSG", tableMap.get("TOPMSG"));
				dataMap.put("MSGCOD", tableMap.get("MSGCOD"));
				setMsgCode(dataMap, Boolean.TRUE);
			}
			
			
		} catch (Exception e) {
			log.error("data_Retouch error >> {}",e);
		}finally {
			setMsgCode(dataMap,result);
		}
		return dataMap;
		
		
	}

	@Override
	public void setMsgCode(Map dataMap, Boolean data_Retouch_result) {
		// TODO Auto-generated method stub
		super.setMsgCode(dataMap, data_Retouch_result);
	}
}
