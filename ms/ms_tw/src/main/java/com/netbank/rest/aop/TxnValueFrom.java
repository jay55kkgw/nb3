package com.netbank.rest.aop;

public enum TxnValueFrom {
    IN, OUT;
}
