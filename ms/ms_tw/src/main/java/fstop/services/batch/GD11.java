package fstop.services.batch;

import fstop.exception.TopMessageException;
import fstop.exception.UncheckedException;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.orm.dao.AdmHolidayDao;
import fstop.orm.dao.GoldPriceDao;
import fstop.orm.po.ADMHOLIDAY;
import fstop.orm.po.GOLDPRICE;
import fstop.services.AfterSuccessQuery;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.*;

/**
 * 台幣預約轉帳
 * 
 * @author Owner
 * 
 */
@Slf4j
public class GD11  implements BatchExecute {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("gd11Telcomm")
	private TelCommExec gd11Telcomm;

	@Autowired
	private GoldPriceDao goldPriceDao;
	
//	final private int totalcnt=0,failcnt=0,okcnt=0;

	@Autowired
	private AdmHolidayDao admholidayDao;
	
//	@Required
//	public void setAdmHolidayDao(AdmHolidayDao admholidayDao) {
//		this.admholidayDao = admholidayDao;
//	}
//	
//	@Required
//	public void setGoldPriceDao(GoldPriceDao goldpriceDao) {
//		this.goldPriceDao = goldpriceDao;
//	}
//
//	@Required
//	public void setGd11Telcomm(TelCommExec telCommExec) {
//		this.gd11Telcomm = telCommExec;
//	}
	
	/**
	 * args[0] method name
	 */
//	@Override
	public BatchResult execute(List<String> args) {
		Map params = new Hashtable();
		Date d = new Date();
		String today = DateTimeUtils.getCDateShort(d);
		log.debug("DATE_S = " + today);
		
		ADMHOLIDAY holiday = null;
		try {
			String holidaytoday = DateTimeUtils.getDateShort(d);
			log.debug("holidaytoday = " + holidaytoday);
			holiday = (ADMHOLIDAY)admholidayDao.findById(holidaytoday);
		}
		catch (Exception e) {
			holiday = null;					
		}

		if(holiday!=null)
		{
			Map<String, Object> data = new HashMap();

			data.put("COUNTER", new BatchCounter());
			data.put("__TOPMSG", "ZXXX");
			data.put("__TOPMSGTEXT", "非營業日．");
			log.debug("__TOPMSGTEXT = " + "非營業日．");
			BatchResult batchresult = new BatchResult();
			batchresult.setSuccess(false);
			batchresult.setBatchName(getClass().getSimpleName());
			batchresult.setData(data);

			return batchresult;
		}

		
		params.put("DATE_S", today);

		SimpleTemplate simpleTemplate = new SimpleTemplate(gd11Telcomm);
		
		simpleTemplate.setAfterSuccessQuery(new AfterSuccessQuery() {

			public void execute(MVHImpl result) {
				if(goldPriceDao.findGoldPriceidIsExists(result.getValueByFieldName("DATE")+result.getValueByFieldName("TIME"))==true)
					return;
				final GOLDPRICE t = new GOLDPRICE();
				
				t.setGOLDPRICEID(result.getValueByFieldName("DATE")+result.getValueByFieldName("TIME"));
				t.setQDATE(result.getValueByFieldName("DATE"));
				t.setQTIME(result.getValueByFieldName("TIME"));
				t.setBPRICE(result.getValueByFieldName("BPRICE"));
				t.setSPRICE(result.getValueByFieldName("SPRICE"));
				t.setPRICE1(result.getValueByFieldName("PRICE1"));
				t.setPRICE2(result.getValueByFieldName("PRICE2"));
				t.setPRICE3(result.getValueByFieldName("PRICE3"));
				t.setPRICE4(result.getValueByFieldName("PRICE4"));

				Date d = new Date();
				String today = DateTimeUtils.getDateShort(d);
				String time = DateTimeUtils.getTimeShort(d);
				log.debug("DATE = " + today);
				log.debug("TIME = " + time);

				t.setLASTDATE(today);
				t.setLASTTIME(time);
				goldPriceDao.insert(t);
    		}
			
		});
		simpleTemplate.setExceptionHandler(new ExceptionHandler() {

			public void handle(Row row, Exception e) {
				
				log.error("無法更新黃金存摺資料.", e);
				
			}
			
		});
		
		BatchResult batchresult = new BatchResult();
		Map<String, Object> data = new HashMap();
		try
		{
			BatchCounter counter = simpleTemplate.execute(params);
		} catch (TopMessageException e) {
			batchresult.setSuccess(false);
			data.put("TOPMSG", e.getMsgcode()+ "  " + e.getMsgout());
			log.error("TopMessageException 執行有誤 !!", e);
		}
		catch(UncheckedException e) {
			batchresult.setSuccess(false);
			data.put("TOPMSG", "UncheckedException 執行有誤 !!");
			log.error("UncheckedException 執行有誤 !!", e);
		}

		batchresult.setBatchName(getClass().getSimpleName());
		batchresult.setData(data);

		return batchresult;

	}
	
}
