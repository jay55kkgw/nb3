package fstop.services.batch;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.ESAPIUtil;

import fstop.model.DBResult;
import fstop.model.EachRowCallback;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.orm.dao.ItrCountDao;
import fstop.orm.dao.ItrIntervalDao;
import fstop.orm.dao.ItrN022Dao;
import fstop.orm.dao.ItrN023Dao;
import fstop.orm.po.ITRCOUNT;
import fstop.orm.po.ITRN022;
import fstop.orm.po.ITRN023;
import fstop.services.AfterSuccessQuery;
import fstop.services.batch.DispatchBatchExecute;
import fstop.services.batch.annotation.Batch;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 功能說明 :外幣存款利率更新
 *
 */

@Slf4j
public class N022  extends DispatchBatchExecute implements BatchExecute {

	private DecimalFormat fmt = new DecimalFormat("#0");
	
	@Autowired
	@Qualifier("n022Telcomm")
	private TelCommExec n022Telcomm;

	@Autowired
	private ItrN022Dao itrN022Dao;
	
	@Autowired
	private ItrN023Dao itrN023Dao;
	
	@Autowired
	private ItrCountDao itrCountDao;
	
	
	
	
	
	@RequestMapping(value = "/batch/n022", method = {RequestMethod.POST},
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @Override
	public BatchResult execute(@RequestBody List<String> args) {
		List<String> safeArgs = ESAPIUtil.validStrList(args);
		Date d = new Date();
		String today = DateTimeUtils.getDateShort(d);
		String time = DateTimeUtils.getTimeShort(d);
		
		Hashtable params = new Hashtable();
//		params.put("DATE", today);
//		params.put("TIME", time);
		log.info("DATE = {}" , today);
		log.info("TIME = {}" , time);
		
		final StringBuffer recno_idBuff = new StringBuffer();
		recno_idBuff.append("0");
		
		final ITRN022 t = new ITRN022();
		

		SimpleTemplate simpleTemplate = new SimpleTemplate(n022Telcomm);
		
		simpleTemplate.setAfterSuccessQuery(new AfterSuccessQuery() {

			public void execute(MVHImpl result) {
//				TODO 考慮移到後面統一做DELETE&SAVE
				itrN022Dao.deleteAll();

				t.setHEADER(result.getValueByFieldName("HEADER"));
				t.setSEQ(result.getValueByFieldName("SEQ"));
				t.setDATE(result.getValueByFieldName("DATADT")); //修改由抓DATE改為DATADT
				t.setTIME(result.getValueByFieldName("TIME"));
				t.setCOUNT(result.getValueByFieldName("COUNT"));
			}
			
		});

		simpleTemplate.setEachRowCallback(new EachRowCallback() {

			public void current(Row row) {
				
				Integer recno_id = new Integer(recno_idBuff.toString()) + 1;
				recno_idBuff.setLength(0);
				recno_idBuff.append(recno_id);
				
				String reconid=new String();
				reconid="00"+recno_idBuff.toString();
				t.setRECNO(StrUtils.right(reconid,2));
				log.debug("reconid = {}" , StrUtils.right(reconid,2));
				t.setCOLOR(row.getValue("COLOR"));
				t.setCRYNAME(row.getValue("CRYNAME"));
				t.setCRY(row.getValue("CRY"));
				String itrstr=row.getValue("ITRX");
				String[] itr=new String[20];
				log.debug("itrstr =" + itrstr + "|");
				log.debug("itr.length =" + itr.length);
				for(int i=0;i<itr.length;i++)
				{
					itr[i]=itrstr.substring(i*7, 7+i*7);
					log.trace("itr[" + i + "]=" + itr[i] + "|");
				}
				t.setITR1(itr[0]);
				t.setITR2(itr[1]);
				t.setITR3(itr[2]);
				t.setITR4(itr[3]);
				t.setITR5(itr[4]);
				t.setITR6(itr[5]);
				t.setITR7(itr[6]);
				t.setITR8(itr[7]);
				t.setITR9(itr[8]);
				t.setITR10(itr[9]);
				t.setITR11(itr[10]);
				t.setITR12(itr[11]);
				t.setITR13(itr[12]);
				t.setITR14(itr[13]);
				t.setITR15(itr[14]);
				t.setITR16(itr[15]);
				t.setITR17(itr[16]);
				t.setITR18(itr[17]);
				t.setITR19(itr[18]);
				t.setITR20(itr[19]);
				t.setFILL(row.getValue("FILL"));
				itrN022Dao.insert(t);
				
				
				
				
				
			}
		});
		
		simpleTemplate.setExceptionHandler(new ExceptionHandler() {

			public void handle(Row row, Exception e) {
				
				log.error("無法更新外幣利率查詢.", e);
				
			}
			
		});
		BatchCounter counter = simpleTemplate.execute(params);

//		writedatfile("N022");
		
		Map<String, Object> data = new HashMap();

		data.put("COUNTER", new BatchCounter());

		BatchResult batchresult = new BatchResult();
		batchresult.setSuccess(true);
		batchresult.setBatchName(getClass().getSimpleName());
		batchresult.setData(data);

		return batchresult;

	}
//	
//	int writedatfile(String tranname)
//	{
//		String FullFileName=new String();
//		String WBuffer=new String();
//		ITRINTERVAL t = new ITRINTERVAL();
//		try
//		{
//			t=itrIntervalDao.findById(tranname);
////			t=itrIntervalDao.get(tranname);
//			WBuffer=t.getINTERVAL().trim();
//		}
//		catch(ObjectNotFoundException objnotfund)
//		{
//			WBuffer="30";
//		}
//		log.info("WBuffer = " + WBuffer);
//		
//		FullFileName=(String)setting.get("filepath");
//		FullFileName+=setting.get("filename");
//		log.info("FullFileName = " + FullFileName);
//		
//		PrintWriter pw = null;
//		try 
//		{
//			pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File((String)setting.get("filepath"),(String)setting.get("filename")), true) , "US-ASCII")));
//			log.info("WBuffer = " + WBuffer);
//			pw.println(WBuffer);
//			pw.flush();
//		}
//		catch(Exception e) {
//			throw new fstop.model.ModelException("載入 FieldTranslator Properties 錯誤 !", e);
//		}
//
//		finally {
//			try {
//				pw.close();
//			}
//			catch(Exception e) {
//			}
//		}
//
//		return(0);
//	}
	
	public MVH query(Map<String, String> params) {
		List<ITRN022> qresult = itrN022Dao.getAll();
		List<ITRN023> qresult2 = itrN023Dao.getAll();
		addItrCount();
		MVHImpl mvh = new DBResult(qresult);
		MVHImpl mvh2 = new DBResult(qresult);
		//toHtml(mvh,mvh2,"c:\\N022.html");
		return mvh;
	}
	
	private void addItrCount()
	{
		Date d = new Date();
		String today = DateTimeUtils.getCDateShort(d);
		String time = DateTimeUtils.getTimeShort(d);
		ITRCOUNT it = null;
		int iCount = 1;//若尚未有點選過，第一次寫入的次數值為一
		try {
			it = itrCountDao.findById("N022");
//			it = itrCountDao.get("N022");
		}
		catch(ObjectNotFoundException e) {
			it = null;
		}
		if(it != null)
		{
			String count = it.getCOUNT();
			if(!"".equals(count) || count!= null)
			{
				iCount = Integer.parseInt(count);//把原本的次數找出來
				iCount++;//原本的次數加一;
				it.setDATE(today);
				it.setTIME(time);
				it.setCOUNT(Integer.toString(iCount));
			}
		}else
		{
			it = new ITRCOUNT();
			it.setHEADER("N022");
			it.setDATE(today);
			it.setTIME(time);
			it.setCOUNT("1");
			
		}
		itrCountDao.save(it);
	}
//	
//	private void toHtml(MVH data,MVH data2, String path)
//	{
//		Date d = new Date();
//		String today = DateTimeUtils.getCDateShort(d);
//		String time = DateTimeUtils.getTimeShort(d);
//		try{
//			File old = new File(path);
//			if(old.exists()) old.delete();
//		}catch (Exception e)
//		{}
//		int  i_count = data.getValueOccurs("RECNO");
//		int  i_count2 = data2.getValueOccurs("RECNO");
//		
//		PrintWriter pw = null;
//		try {
//			pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(path), true) , "UTF-8")));
//			pw.println("<html>");
//			pw.println("<head>"); 
//	    	pw.println("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">");
//	    	pw.println("</head>");
//	    	pw.println("<body>");
//	    	pw.println("<H3 ALIGN=CENTER></H>");
//	    	pw.println("<H3 ALIGN=\"CENTER\"> "+CommonPools.sysop.getApp("N022")+"</H><BR>");
//	    	
//	    	if(i_count > 0)
//	    	{
//	    		pw.println("<TABLE BORDER=\"2\" cellspacing=\"0\">");
//	    		pw.println("<TR><TH COLSPAN=\"22\">  查  詢  時  間:"+today+time+"</th></tr>");
//	    		pw.println("<TR><TH ROWSPAN=\"3\">幣別</th><TH COLSPAN=\"21\">存款期間</th></tr>");
//	    		pw.println("<TR><TH ROWSPAN=\"2\">代號</th><TH ROWSPAN=\"2\">活期存款</th><TH ROWSPAN=\"2\">1週</th><TH ROWSPAN=\"2\">2週</th><TH ROWSPAN=\"2\">3週</th><TH COLSPAN=\"2\">1個月</th><TH COLSPAN=\"2\">2個月</th><TH COLSPAN=\"2\">3個月</th><TH COLSPAN=\"2\">4個月</th><TH COLSPAN=\"2\">5個月</th><TH COLSPAN=\"2\">6個月</th><TH COLSPAN=\"2\">9個月</th><TH COLSPAN=\"2\">12個月</th></tr>");
//	    		pw.println("<TR><TH>固定</th><TH>機動</th><TH>固定</th><TH>機動</th><TH>固定</th><TH>機動</th><TH>固定</th><TH>機動</th><TH>固定</th><TH>機動</th><TH>固定</th><TH>機動</th><TH>固定</th><TH>機動</th><TH>固定</th><TH>機動</th></tr>");
//	    		for (int i=0; i<i_count; i++)
//	    		{
//	    			pw.println("<TR BGCOLOR="+FieldTranslator.transfer("N021", "COLOR", data.getValueByFieldName("COLOR", i+1))+"><TD Align=\"left\"><FONT>"+data.getValueByFieldName("CRYNAME", i+1)+"</FONT>&nbsp;</td>");
//	    			pw.println("<TD Align=\"left\"><FONT>"+data.getValueByFieldName("CRY", i+1)+"</FONT>&nbsp;</td>");
//	    			pw.println("<TD Align=\"right\"><FONT>"+data.getValueByFieldName("ITR1", i+1)+"</FONT>&nbsp;</td>");
//	    			pw.println("<TD Align=\"right\"><FONT>"+data.getValueByFieldName("ITR2", i+1)+"</FONT>&nbsp;</td>");
//	    			pw.println("<TD Align=\"right\"><FONT>"+data.getValueByFieldName("ITR3", i+1)+"</FONT>&nbsp;</td>");
//	    			pw.println("<TD Align=\"right\"><FONT>"+data.getValueByFieldName("ITR4", i+1)+"</FONT>&nbsp;</td>");
//	    			pw.println("<TD Align=\"right\"><FONT>"+data.getValueByFieldName("ITR5", i+1)+"</FONT>&nbsp;</td>");
//	    			pw.println("<TD Align=\"right\"><FONT>"+data.getValueByFieldName("ITR6", i+1)+"</FONT>&nbsp;</td>");
//	    			pw.println("<TD Align=\"right\"><FONT>"+data.getValueByFieldName("ITR7", i+1)+"</FONT>&nbsp;</td>");
//	    			pw.println("<TD Align=\"right\"><FONT>"+data.getValueByFieldName("ITR8", i+1)+"</FONT>&nbsp;</td>");
//	    			pw.println("<TD Align=\"right\"><FONT>"+data.getValueByFieldName("ITR9", i+1)+"</FONT>&nbsp;</td>");
//	    			pw.println("<TD Align=\"right\"><FONT>"+data.getValueByFieldName("ITR10", i+1)+"</FONT>&nbsp;</td>");
//	    			pw.println("<TD Align=\"right\"><FONT>"+data.getValueByFieldName("ITR11", i+1)+"</FONT>&nbsp;</td>");
//	    			pw.println("<TD Align=\"right\"><FONT>"+data.getValueByFieldName("ITR12", i+1)+"</FONT>&nbsp;</td>");
//	    			pw.println("<TD Align=\"right\"><FONT>"+data.getValueByFieldName("ITR13", i+1)+"</FONT>&nbsp;</td>");
//	    			pw.println("<TD Align=\"right\"><FONT>"+data.getValueByFieldName("ITR14", i+1)+"</FONT>&nbsp;</td>");
//	    			pw.println("<TD Align=\"right\"><FONT>"+data.getValueByFieldName("ITR15", i+1)+"</FONT>&nbsp;</td>");
//	    			pw.println("<TD Align=\"right\"><FONT>"+data.getValueByFieldName("ITR16", i+1)+"</FONT>&nbsp;</td>");
//	    			pw.println("<TD Align=\"right\"><FONT>"+data.getValueByFieldName("ITR17", i+1)+"</FONT>&nbsp;</td>");
//	    			pw.println("<TD Align=\"right\"><FONT>"+data.getValueByFieldName("ITR18", i+1)+"</FONT>&nbsp;</td>");
//	    			pw.println("<TD Align=\"right\"><FONT>"+data.getValueByFieldName("ITR19", i+1)+"</FONT>&nbsp;</td>");
//	    			pw.println("<TD Align=\"right\"><FONT>"+data.getValueByFieldName("ITR20", i+1)+"</FONT>&nbsp;</td></TR>");
//	    		}
//	    		pw.println("</table>");
//	    	}else pw.println("查無資料!!!");
//	    	pw.println("<P>");
//	    	pw.println("<P>");
//	    	pw.println("<P>");
//	    	pw.println("<P>");
//	    	
//	    	pw.println("<H3 ALIGN=\"CENTER\"> "+CommonPools.sysop.getApp("N023")+"</H><BR>");
//	    	if(i_count2 > 0)
//	    	{
//	    		pw.println("<TABLE BORDER=\"2\" cellspacing=\"0\">");
//	    		pw.println("<TR><TH COLSPAN=\"15\">  查  詢  時  間:"+today+time+"</th></tr>");
//	    		pw.println("<TR><TH ROWSPAN=\"2\">幣別</th><TH COLSPAN=\"14\">存款期間</th></tr>");
//	    		pw.println("<TR><TH>代號</th><TH>活期存款<TH>1週</th><TH>2週</th><TH>3週</th><TH>1個月</th><TH>2個月</th><TH>3個月</th><TH>4個月</th><TH>5個月</th><TH>6個月</th><TH>9個月</th><TH>12個月</th></tr>");
//	    		for (int i=0; i<i_count2; i++)
//	    		{
//	    			pw.println("<TR BGCOLOR="+FieldTranslator.transfer("N021", "COLOR", data2.getValueByFieldName("COLOR", i+1))+"><TD Align=\"left\"><FONT>"+data2.getValueByFieldName("CRYNAME", i+1)+"</FONT>&nbsp;</td>");
//	    			pw.println("<TD Align=\"left\"><FONT>"+data2.getValueByFieldName("CRY", i+1)+"</FONT>&nbsp;</td>");
//	    			pw.println("<TD Align=\"right\"><FONT>"+data2.getValueByFieldName("ITR1", i+1)+"</FONT>&nbsp;</td>");
//	    			pw.println("<TD Align=\"right\"><FONT>"+data2.getValueByFieldName("ITR2", i+1)+"</FONT>&nbsp;</td>");
//	    			pw.println("<TD Align=\"right\"><FONT>"+data2.getValueByFieldName("ITR3", i+1)+"</FONT>&nbsp;</td>");
//	    			pw.println("<TD Align=\"right\"><FONT>"+data2.getValueByFieldName("ITR4", i+1)+"</FONT>&nbsp;</td>");
//	    			pw.println("<TD Align=\"right\"><FONT>"+data2.getValueByFieldName("ITR5", i+1)+"</FONT>&nbsp;</td>");
//	    			pw.println("<TD Align=\"right\"><FONT>"+data2.getValueByFieldName("ITR6", i+1)+"</FONT>&nbsp;</td>");
//	    			pw.println("<TD Align=\"right\"><FONT>"+data2.getValueByFieldName("ITR7", i+1)+"</FONT>&nbsp;</td>");
//	    			pw.println("<TD Align=\"right\"><FONT>"+data2.getValueByFieldName("ITR8", i+1)+"</FONT>&nbsp;</td>");
//	    			pw.println("<TD Align=\"right\"><FONT>"+data2.getValueByFieldName("ITR9", i+1)+"</FONT>&nbsp;</td>");
//	    			pw.println("<TD Align=\"right\"><FONT>"+data2.getValueByFieldName("ITR10", i+1)+"</FONT>&nbsp;</td>");
//	    			pw.println("<TD Align=\"right\"><FONT>"+data2.getValueByFieldName("ITR11", i+1)+"</FONT>&nbsp;</td>");
//	    			pw.println("<TD Align=\"right\"><FONT>"+data2.getValueByFieldName("ITR12", i+1)+"</FONT>&nbsp;</td>");
//	    		}
//	    		pw.println("</table>");
//	    	}else pw.println("查無資料!!!");
//	    	
//	    	pw.println("<form>");
//	    	pw.println("<center><table border=0>");
//	    	pw.println("<tr><td><input type='button' value='回上一頁' OnClick='history.back()'></td><td><input type='button' value='登出' OnClick='javascript:self.close()'</td></tr></table></center>");
//	    	pw.println("</form>");
//	    	pw.println("</body>");
//	    	pw.println("</html>");
//			//pw.write(str);
//		}
//		catch(Exception e) {
//			log.error("", e);
//		}
//		finally {
//			try {
//				pw.close();
//			}
//			catch(Exception e) {
//			}
//		}
//		
//	}
}
