package fstop.services.batch;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class BatchResult {
	public String batchName;
	
	public boolean isSuccess;

	public String errorCode;

//	public String data;
	public Map<String, Object> data;
	
}
