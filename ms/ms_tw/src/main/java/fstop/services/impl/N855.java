package fstop.services.impl;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

import com.netbank.util.CodeUtil;
import com.netbank.util.DateUtils;
import com.netbank.util.ESAPIUtil;
import com.netbank.util.fstop.DateUtil;

import enc.Enc256Util;
import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.orm.dao.SysDailySeqDao;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.va.VaCheck;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N855 extends CommonService {

	@Autowired
	@Qualifier("n855Telcomm")
	private TelCommExec n855Telcomm;

	@Autowired
	private SysDailySeqDao sysDailySeqDao;

	@Value("${vaKeyLableFisc}")
	String vaKeyLable;

	@Override
	public MVH doAction(Map _params) {

		TelcommResult telcommResult = null;
		try {
			Map<String, String> params = _params;
			params.put("DATE", DateUtil.getTWDate(""));
			params.put("TIME", DateUtil.getTheTime(""));
			params.put("FISRID", params.get("SrcID"));
			params.put("FIKEID", params.get("KeyID"));
			params.put("FIDTTM", params.get("TxnDatetime"));
			params.put("FISTAN", params.get("STAN"));
			params.put("FIAYID", params.get("BillerBan"));
			params.put("FIBSTP", params.get("BusType"));
			params.put("FITXID", params.get("BillerID"));
			params.put("FIPATP", params.get("PaymentType"));
			params.put("FIFEID", params.get("FeeID"));
			params.put("FIIDNO", params.get("CustIdnBan_Dec"));
			params.put("FIACN1", params.get("CustBillerAcnt_Dec"));
			params.put("FIACN2", params.get("CustBankAcnt_Dec"));
			params.put("FIFINS", params.get("ReservedField"));
			Integer txseqInt = sysDailySeqDao.dailySeq("N855") % 100000;
			String txseq = paddingLeft(5, "0", txseqInt.toString());
			params.put("TXSEQ", txseq);

			telcommResult = n855Telcomm.query(params);

			// 產生divData、iv、macKey
			byte[] divData = VaCheck.hexToBytes(
					UUID.randomUUID().toString().replaceAll("-", "") + toHexString(DateUtil.getDate("").getBytes()));
			byte[] iv = VaCheck.getICV();
			byte[] macKey = VaCheck.genVaMacKey(vaKeyLable, divData); // MAC Session Key
			log.debug("divData>>{}", toHexString(divData));
			log.trace("iv >> {}", toHexString(iv));
			log.trace("macKey >> {}", toHexString(macKey));

			Map resultMap = telcommResult.getFlatValues();
			LinkedHashMap IBPD_Param = new LinkedHashMap();
			IBPD_Param.put("SrcID", _params.get("SrcID"));
			IBPD_Param.put("KeyID", _params.get("KeyID"));
			IBPD_Param.put("DivData", "");
			IBPD_Param.put("ICV", "");
			IBPD_Param.put("MAC", "");
			IBPD_Param.put("TxnDatetime", _params.get("TxnDatetime"));
			IBPD_Param.put("STAN", _params.get("STAN"));
			IBPD_Param.put("RCode", resultMap.get("FORCOD"));
			IBPD_Param.put("BusType", params.get("BusType"));
			IBPD_Param.put("TxnType", "00"); // *
			// 左靠右補空白
			String CUSIBAN = paddingRight(16, " ", (String) params.get("FIIDNO"));
			log.trace(ESAPIUtil.vaildLog("CUSIBAN >> " + CUSIBAN));
			IBPD_Param.put("CustIdnBan", toHexString(VaCheck.EBCEncrypt(macKey, CUSIBAN.getBytes())));
			// 左靠右補零
			String CUSTBNK = paddingRight(7, "0", "050");
			log.trace(ESAPIUtil.vaildLog("CUSTBNK >> " + CUSTBNK));
			IBPD_Param.put("CustBankId", CUSTBNK); // *

			IBPD_Param.put("BillerBan", params.get("BillerBan"));
			IBPD_Param.put("BillerID", params.get("BillerID"));
			IBPD_Param.put("PaymentType", params.get("PaymentType"));
			IBPD_Param.put("FeeID", params.get("FeeID"));
			// 右靠左補零
			String CSBNKACN = paddingLeft(16, "0", (String) params.get("FIACN2"));
			log.trace(ESAPIUtil.vaildLog("CSBNKACN >> " + CSBNKACN));
			IBPD_Param.put("CustBankAcnt", toHexString(VaCheck.EBCEncrypt(macKey, CSBNKACN.getBytes())));

			// 移除DivData、ICV、MAC進行壓碼
			LinkedHashMap IBPD_Param_tmp = (LinkedHashMap) IBPD_Param.clone();
			IBPD_Param_tmp.remove("DivData");
			IBPD_Param_tmp.remove("ICV");
			IBPD_Param_tmp.remove("MAC");

			log.trace(ESAPIUtil.vaildLog("IBPD_Param >> " + CodeUtil.toJson(IBPD_Param)));
			log.trace(ESAPIUtil.vaildLog("IBPD_Param_tmp >> " + CodeUtil.toJson(IBPD_Param_tmp)));
			String mac = makeMAC(macKey, iv, CodeUtil.toJson(IBPD_Param_tmp));

			// 放回DivData、ICV、MAC
			log.trace("mac >> {}", mac);
			if (mac.length() == 64) {
				mac = mac.substring(mac.length() - 16, mac.length() - 8);
			}
			IBPD_Param.put("DivData", toHexString(divData));
			IBPD_Param.put("ICV", toHexString(iv));
			IBPD_Param.put("MAC", mac);

			log.trace(ESAPIUtil.vaildLog("IBPD_Param >> " + CodeUtil.toJson(IBPD_Param)));
			telcommResult.getFlatValues().put("IBPD_Param", CodeUtil.toJson(IBPD_Param));
			if(telcommResult.getFlatValues().get("TOPMSG").equals("4001")) {
				telcommResult.getFlatValues().put("TOPMSG", "0000");
			}else {
				telcommResult.getFlatValues().put("TOPMSG", "I" + telcommResult.getFlatValues().get("TOPMSG"));
			}
		} catch (TopMessageException e) {
			log.error("N855 TopMessageException: >> {}", e);
			throw e;
		} catch (Exception e) {
			log.error("N855 Exception: >> {}", e);
		}
		return telcommResult;
	}

	public static String toHexString(byte[] block) {
		StringBuffer buf = new StringBuffer("");
		int len = block.length;
		for (int i = 0; i < len; i++) {
			CodeUtil.byte2hex(block[i], buf);
		}
		return buf.toString();
	}

	public static String paddingLeft(int len, String paddingString, String originalString) {
		String resultString = originalString;
		while (true) {
			if (resultString.length() < len) {
				resultString = paddingString + resultString;
			} else {
				break;
			}
		}
		return resultString;
	}

	public static String paddingRight(int len, String paddingString, String originalString) {
		String resultString = originalString;
		while (true) {
			if (resultString.length() < len) {
				resultString += paddingString;
			} else {
				break;
			}
		}
		return resultString;
	}

	public String makeMAC(byte[] macKey, byte[] iv, String data) {
		String result = "";
		try {
			log.trace("makeMAC");
			String c = Enc256Util.encrypt(data, "utf8");
			log.debug(ESAPIUtil.vaildLog("data sha256 >> {"+c+"}"));
			byte[] mac = VaCheck.genVaMac3DES(macKey, iv, VaCheck.hexToBytes(c));
			result = toHexString(mac);
		} catch (TopMessageException e) {
			log.error("makeMAC error: >> {}", e);
			throw e;
		} catch (Exception e) {
			log.error("{}", e);
		}
		return result;
	}
}
