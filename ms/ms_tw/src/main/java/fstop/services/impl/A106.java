package fstop.services.impl;

import java.util.Date;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.TxnLogDao;
import fstop.orm.po.TXNLOG;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.IPUtils;
import fstop.util.JSONUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Owner
 *
 */
//@Transactional
@Slf4j
public class A106 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());
//		
//	private TelCommExec a106Telcomm;
//	private TelCommExec n951Telcomm;
//		
//	@Required
//	public void setA106Telcomm(TelCommExec telcomm) {
//		a106Telcomm = telcomm;
//	}
//		
//	@Required
//	public void setN951Telcomm(TelCommExec telcomm) {
//		this.n951Telcomm = telcomm;
//	}
	
	@Autowired
	@Qualifier("a106Telcomm")
	private TelCommExec a106Telcomm;
	@Autowired
	@Qualifier("n951Telcomm")
	private TelCommExec n951Telcomm;
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 

		//查詢
		//logger.warn("A106.java CAREER1:"+params.get("CAREER1"));
		//logger.warn("A106.java CAREER2:"+params.get("CAREER2"));
		//logger.warn("A106.java COMPANYNAME:"+params.get("COMPANYNAME"));
		log.warn(ESAPIUtil.vaildLog("A106.java PARAM1:"+params.get("PARAM1")));
		log.warn(ESAPIUtil.vaildLog("A106.java PARAM2:"+params.get("PARAM2")));
		log.warn(ESAPIUtil.vaildLog("A106.java PARAM3:"+params.get("PARAM3")));
		log.warn(ESAPIUtil.vaildLog("A106.java PARAM4:"+params.get("PARAM4")));
		log.warn(ESAPIUtil.vaildLog("A106.java PARAM5:"+params.get("PARAM5")));
		log.warn(ESAPIUtil.vaildLog("A106.java params:"+params));
		String fundtype = params.get("FUN_TYPE").toString();		
		MVHImpl telcommResult = new MVHImpl();
//		MVHImpl telcommResult1 = new MVHImpl();
		String MSGCODE = "";
		
		try{
			if(fundtype.equals("R")) {//查詢
				
				telcommResult = a106Telcomm.query(params);
				
			}else if(fundtype.equals("U")) {//修改
				if("EATM".equals(params.get("PARAM2").toString()))//系統別為ATM
				{				
					Date d = new Date();
					String trin_transeq = "1234";
					String trin_issuer = "05000000";
					String trin_acnno = params.get("ACNNO").toString();
					String trin_icdttm = DateTimeUtils.format("yyyyMMddHHmmss", d);
					log.warn("ICDTTM ----------> " + trin_icdttm);
					String trin_iseqno = params.get("iSeqNo").toString();
					String trin_icmemo = "";
					String trin_tac_length = "000A";		
					String trin_tac = params.get("TAC").toString();
					log.warn(ESAPIUtil.vaildLog("TAC ----------> " + trin_tac_length + trin_tac + "(" + (trin_tac_length.length() + trin_tac.length()) + ")"));
					String trin_trmid = params.get("TRMID").toString();
				
					params.put("TRANSEQ", trin_transeq);
					params.put("ISSUER", trin_issuer);
					params.put("ACNNO", trin_acnno);
					params.put("ICDTTM", trin_icdttm);
					params.put("ICSEQ", trin_iseqno);
					params.put("ICMEMO", trin_icmemo);
					params.put("TAC_Length", trin_tac_length);
					params.put("TAC", trin_tac);
					params.put("TRMID", trin_trmid);	
					
				}else if(!"EATM".equals(params.get("PARAM2").toString())) {//系統別為MB、NNB
					
					log.warn("PARAM2 is not EATM");
					Date d = new Date();
					String df = DateTimeUtils.getCDateShort(d);
					String tf = DateTimeUtils.format("HHmmss", d);
					params.put("DATE", df);
					params.put("TIME", tf);
					params.put("FGTXWAY", "0");
					params.put("CUSIDN", (params.get("PARAM1") == null ? "":params.get("PARAM1").toString()));
					params.put("TRANSPASSUPDATE", "1");
					//TelCommExec n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n951CMTelcomm");					
					//MVHImpl n950Result = n950CMTelcomm.query(params);
					MVHImpl n950Result = n951Telcomm.query(params);
					//n950Result.dump();
					/*Date d = new Date();
					String df = DateTimeUtils.getCDateShort(d);
					String tf = DateTimeUtils.format("HHmmss", d);
					params.put("DATE", df);
					params.put("TIME", tf);
					params.put("N950PASSWORD", params.get("PD"));
					params.put("CUSIDN", (params.get("PARAM1") == null ? "":params.get("PARAM1").toString()));
					n951Telcomm.query(params);
					*/	
				}							
				params.put("CUSIDN", (params.get("PARAM1") == null ? "":params.get("PARAM1")));
				//params.put("CONPNAM", (params.get("COMPANYNAME") == null ? "":params.get("COMPANYNAME").toString()));
				log.debug(ESAPIUtil.vaildLog("A106 BBB CAREER1:"+params.get("CAREER1")));
				log.debug(ESAPIUtil.vaildLog("A106 BBB CONPNAM:"+params.get("CONPNAM")));				
				params.put("CAREER1", (!(params.get("PARAM4").equals("")) ? params.get("PARAM4"):params.get("CAREER1")));//職業
				params.put("CAREER2", (!(params.get("PARAM5").equals("")) ? params.get("PARAM5"):params.get("CAREER2")));//職稱
				
				//params.put("CONPNAM", ((!params.get("PARAM3").equals("")) ? params.get("PARAM3"):params.get("COMPANYNAME")));//任職機構
				int len = 0;
				if(!(params.get("PARAM3").equals(""))) {//客戶已填過任職機構
					len = 33 - params.get("PARAM3").toString().length();
					log.debug("A106 PARAM3 len===>"+len);
					params.put("CONPNAM", params.get("PARAM3"));//任職機構					
					log.debug(ESAPIUtil.vaildLog("A106 NNB CONPNAM:"+params.get("CONPNAM")+"<==="));
					log.debug(ESAPIUtil.vaildLog("A106 NNB ChanisesFullChar:"+toChanisesFullChar(params.get("CONPNAM").toString()+StrUtils.repeat("	",len))+"<==="));
					params.put("CONPNAM",toChanisesFullChar(params.get("CONPNAM").toString()+StrUtils.repeat("	",len)));//任職機構半形轉全形
				}else {									//客戶未填過任職機構
					if(!params.get("COMPANYNAME").equals("")) {
						len = 33 - params.get("COMPANYNAME").toString().length();
						log.debug("A106 COMPANYNAME len===>"+len);
						params.put("CONPNAM", params.get("COMPANYNAME"));//任職機構					
						log.debug(ESAPIUtil.vaildLog("A106 NNB CONPNAM:"+params.get("CONPNAM")+"<==="));
						log.debug(ESAPIUtil.vaildLog("A106 NNB ChanisesFullChar:"+toChanisesFullChar(params.get("CONPNAM").toString()+StrUtils.repeat("	",len))+"<==="));
						params.put("CONPNAM",toChanisesFullChar(params.get("CONPNAM").toString()+StrUtils.repeat("	",len)));//任職機構半形轉全形
					}else {
						params.put("CONPNAM", "");//任職機構
					}
					
				}
				
				//PARAM3不為空值，就帶PARAM3，反之為空值就帶畫面上的任職機構欄位，34是該欄位總長度
				//params.put("CONPNAM", ((!params.get("PARAM3").equals("")) ? StrUtils.left(params.get("PARAM3")+StrUtils.repeat("	",len),34):StrUtils.left(params.get("COMPANYNAME")+StrUtils.repeat("	",len),34)));//任職機構
//				if(params.get("PARAM2").equals("MB") || params.get("PARAM2").equals("EATM")) {
//					params.put("CONPNAM", params.get("PARAM3"));//任職機構					
//					log.debug("A106 MB or EATM CONPNAM:"+params.get("CONPNAM")+"<===");
//					log.debug("A106 MB or EATM ChanisesFullChar:"+toChanisesFullChar(params.get("CONPNAM").toString()+StrUtils.repeat("	",len+2))+"<===");
//					params.put("CONPNAM",toChanisesFullChar(params.get("CONPNAM").toString()+StrUtils.repeat("	",len+2)));//任職機構半形轉全形
//				}else{
				    //start Update 20190225
//					params.put("CONPNAM", params.get("COMPANYNAME"));//任職機構					
//					log.debug("A106 NNB CONPNAM:"+params.get("CONPNAM")+"<===");
//					log.debug("A106 NNB ChanisesFullChar:"+toChanisesFullChar(params.get("CONPNAM").toString()+StrUtils.repeat("	",len))+"<===");
//					params.put("CONPNAM",toChanisesFullChar(params.get("CONPNAM").toString()+StrUtils.repeat("	",len)));//任職機構半形轉全形
				    //end
//				}
//				if(!params.get("PARAM3").equals("")) {
//					params.put("CONPNAM", ((!params.get("PARAM3").equals("")) ? params.get("PARAM3"):params.get("COMPANYNAME")));//任職機構
//					log.debug("A106 COMPANYNAME:"+StrUtils.left(params.get("COMPANYNAME")+StrUtils.repeat("	",len),33)+"<===");
//					log.debug("A106 CONPNAM:"+params.get("CONPNAM")+"<===");
//					log.debug("A106 ChanisesFullChar:"+toChanisesFullChar(params.get("CONPNAM").toString()+StrUtils.repeat("	",len))+"<===");
//					params.put("CONPNAM",toChanisesFullChar(params.get("CONPNAM").toString()+StrUtils.repeat("	",len)));//任職機構半形轉全形
//				}
				log.debug(ESAPIUtil.vaildLog("A106 params After:"+params));
				telcommResult = a106Telcomm.query(params);
				
				if(params.get("PARAM2").equals("MB")) {
					String ADCONTENT = JSONUtils.map2json(params);
					TXNLOG txnlog = new TXNLOG();
					
					//寫入txnlog
					log.debug(ESAPIUtil.vaildLog("A106.java MB UUID:"+UUID.randomUUID().toString()));
					log.debug(ESAPIUtil.vaildLog("A106.java MB RemoteIP:"+IPUtils.getRemoteIP()));
					log.debug(ESAPIUtil.vaildLog("A106.java MB ADCONTENT:"+ADCONTENT));								
					TxnLogDao txnLogDao = (TxnLogDao)SpringBeanFactory.getBean("txnLogDao");
					
					txnlog.setADTXNO(UUID.randomUUID().toString());
					txnlog.setADEXCODE("");
					txnlog.setADUSERID((String)params.get("PARAM1"));
					txnlog.setADOPID("A106");
					txnlog.setFGTXWAY("0");
					txnlog.setADUSERIP(IPUtils.getRemoteIP());
					txnlog.setADCONTENT(ADCONTENT);
					txnlog.setADGUID(UUID.randomUUID().toString());
					txnlog.setLASTDATE(DateTimeUtils.format("yyyyMMdd", new Date()));
					txnlog.setLASTTIME(DateTimeUtils.format("HHmmss", new Date()));					
					txnlog.setLOGINTYPE("MB");
					txnLogDao.save(txnlog);
				}
			}//fundtype is U
//			telcommResult.getFlatValues().put("SMONEY",telcommResult1.getValueByFieldName("SAMT"));
//			telcommResult.getFlatValues().put("RENAME",telcommResult1.getValueByFieldName("RENAME"));
		} catch(TopMessageException e) {
//		   telcommResult.getFlatValues().put("SMONEY", "");
//		   telcommResult.getFlatValues().put("RENAME","");
		   //throw TopMessageException.create(e.getMsgcode());
			log.debug("TopMessageException e:"+e.getMessage());
			telcommResult.getFlatValues().put("TOPMSG", e.getMsgcode());
		} catch(Exception e){
			log.debug("Exception e:"+e.getMessage());
			telcommResult.getFlatValues().put("TOPMSG", "ZX99");
		}
		
		return telcommResult;
	}
	
	//半形轉全形
	public static String toChanisesFullChar(String s){
	    if(s==null || s.equals("")){
	      return "";
	    }
	    
	    char[] ca = s.toCharArray();
	    for(int i=0; i<ca.length; i++){
	      if(ca[i] > '\200'){    continue;   }      //超過這個應該都是中文字了…      
	      if(ca[i] == 32){    ca[i] = (char)12288;        continue;                  }  //半型空白轉成全型空白
	      if(Character.isLetterOrDigit(ca[i])){   ca[i] = (char)(ca[i] + 65248);  continue;  }  //是有定義的字、數字及符號
	      
	      ca[i] = (char)12288;  //其它不合要求的，全部轉成全型空白。
	    }
	    
	    return String.valueOf(ca);
	}

}
