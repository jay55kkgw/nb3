package fstop.services.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.model.MVH;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class NP10 extends CommonService {

	@Autowired
    @Qualifier("np10Telcomm")
	private TelCommExec np10Telcomm;
	
	@Override	
	public MVH doAction(Map params) {
		log.trace(ESAPIUtil.vaildLog("NP10.doAction.params: {}"+CodeUtil.toJson(params)));
		return np10Telcomm.query(params);
	}
}
