package fstop.services.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import enc.Enc256Util;
import encryptUtil.EncryptUtil;
import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.services.CommonService;
import fstop.va.VaCheck;
import lombok.extern.slf4j.Slf4j;


@Slf4j
public class N855CheckMac extends CommonService{

	@Value("${vaKeyLableFisc}")
	String vaKeyLable;
	
	@Override
	public MVH doAction(Map _params) { 

		MVHImpl result = new MVHImpl();
		try {
			byte[] divData = VaCheck.hexToBytes((String)_params.get("DivData"));
	        log.debug("divData>>{}",divData);
	        byte[] macKey = VaCheck.genVaMacKey(vaKeyLable, divData);
			byte[] iv = VaCheck.hexToBytes((String)_params.get("ICV"));
			String MAC =(String)_params.get("MAC");
			String IBPD_Param = (String)_params.get("IBPD_Param");
			log.debug(ESAPIUtil.vaildLog("IBPD_Param >> "+ IBPD_Param));

			String macOwn = makeMAC(macKey, iv, IBPD_Param);
			log.trace("mac >> {}", macOwn);
			if(macOwn.length() == 64) {
				macOwn = macOwn.substring(macOwn.length() - 16, macOwn.length() - 8);
			}
			if(macOwn.equals(MAC)) {
				result.getFlatValues().put("TOPMSG", "0000");
			}else {
				result.getFlatValues().put("TOPMSG", "ZX99");
			}
			
		}catch (TopMessageException e) {
			log.error("N855Decrypt TopMessageException: >> {}",e);
			throw e;
		}catch(Exception e) {
			log.error("{}", e);
		}
		return result;
	}
	
    public static String toHexString(byte[] block) 
    {
         StringBuffer buf = new StringBuffer("");
         int len = block.length;
         for (int i = 0; i < len; i++) {
        	 CodeUtil.byte2hex(block[i], buf);
         }
         return buf.toString();
    }
	public String makeMAC(byte[] macKey, byte[] iv, String data) {
		String result = "";
		try {
			log.trace("makeMAC");
			String c = Enc256Util.encrypt(data, "utf8");
	        log.debug(ESAPIUtil.vaildLog("data sha256 >> {"+c+"}"));
	        byte[] mac = VaCheck.genVaMac3DES(macKey, iv,  VaCheck.hexToBytes(c));
	        result = toHexString(mac);
		}catch (TopMessageException e) {
			log.error("makeMAC error: >> {}",e);
			throw e;
		}catch(Exception e) {
			log.error("{}", e);
		}
		return result;
	}
}
