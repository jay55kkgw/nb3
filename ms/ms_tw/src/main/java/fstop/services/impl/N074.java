package fstop.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import encryptUtil.EncryptUtil;
import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.SysDailySeqDao;
import fstop.orm.dao.TxnTwRecordDao;
import fstop.orm.dao.TxnTwSchPayDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.ADMMAILLOG;
import fstop.orm.po.TXNTWRECORD;
import fstop.orm.po.TXNTWSCHPAY;
import fstop.services.BookingAware;
import fstop.services.CommonService;
import fstop.services.NoticeInfo;
import fstop.services.NoticeInfoGetter;
import fstop.services.OnLineTransferNotice;
import fstop.services.TelCommExecWrapper;
import fstop.services.TransferNotice;
import fstop.services.WriteLogInterface;
import fstop.telcomm.TelCommExec;
import fstop.telcomm.cmd.CommandUtils;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import topibs.utility.NumericUtil;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class N074 extends CommonService implements BookingAware, WriteLogInterface {
	@Autowired
	@Qualifier("n074Telcomm")
	private TelCommExec n074Telcomm;

	@Autowired
	@Qualifier("n951CMTelcomm")
	private TelCommExec n951Telcomm;

	@Autowired
	@Qualifier("n954Telcomm")
	private TelCommExec n954Telcomm;

	@Autowired
	private TxnUserDao txnUserDao;
	
	@Autowired
	private SysDailySeqDao sysDailySeqDao;

	@Autowired
	private TxnTwSchPayDao txntwschpaydao;

	@Autowired
	private TxnTwRecordDao txnTwRecordDao;

	/**
	 * 執行轉定存
	 */
	public MVH doAction(Map _params) {
		Map<String, String> params = _params;

		// 晶片金融卡
		if ("2".equals(params.get("FGTXWAY").toString())) {
			Date d = new Date();
			String trin_transeq = "1234";
			String trin_issuer = "05000000";
			String trin_acnno = params.get("ACNNO");
			String trin_icdttm = DateTimeUtils.format("yyyyMMddHHmmss", d);
			log.debug("ICDTTM ----------> " + trin_icdttm);
			String trin_iseqno = params.get("iSeqNo");
			String trin_icmemo = "";
			String trin_tac_length = "000A";
			String trin_tac = params.get("TAC");
			log.debug(ESAPIUtil.vaildLog("TAC ----------> " + trin_tac_length + trin_tac + "("
					+ (trin_tac_length.length() + trin_tac.length()) + ")"));
			String trin_trmid = params.get("TRMID");

			params.put("TRANSEQ", trin_transeq);
			params.put("ISSUER", trin_issuer);
			params.put("ACNNO", trin_acnno);
			params.put("ICDTTM", trin_icdttm);
			params.put("ICSEQ", trin_iseqno);
			params.put("ICMEMO", trin_icmemo);
			params.put("TAC_Length", trin_tac_length);
			params.put("TAC", trin_tac);
			params.put("TRMID", trin_trmid);
		}

		String fgsvacno = params.get("FGSVACNO"); // 1 約定帳號, 2 非約定帳號
		// 轉入帳號
		String trin_bnkcod = "";
		String trin_acn = "";
		String trin_agree = "";
		if ("1".equals(fgsvacno)) {
			Map<String, String> jm = JSONUtils.json2map(params.get("DPAGACNO"));
			trin_bnkcod = jm.get("BNKCOD");
			trin_acn = jm.get("ACN");
			trin_agree = jm.get("AGREE");
		} else if ("2".equals(fgsvacno)) { // 非約定
			trin_bnkcod = "050";
			trin_acn = params.get("DPACNO");
			trin_agree = "0"; // 0: 非約定, 1: 約定
		}

		params.put("__TRIN_BANKCOD", trin_bnkcod);
		params.put("__TRIN_ACN", trin_acn);
		params.put("__TRIN_AGREE", trin_agree);

		/*
		 * //BHO 轉入帳號檢核 String str_UserInputAcno = (String)params.get("InAcno_1") +
		 * (String)params.get("InAcno_2") + (String)params.get("InAcno_3"); String
		 * str_RandomValueAcno = (String)params.get("BHO_INACNO_VERIFY");
		 * BHOCheckUtils.checkBHOInAcno(str_UserInputAcno, str_RandomValueAcno);
		 */

		String fgtdperiod = params.get("FGTDPERIOD");
		if ("1".equals(fgtdperiod)) { // 期別
			params.put("DUEDAT", "");
		} else if ("2".equals(fgtdperiod)) { // 指定到期日
			params.put("TERM", "99");
			String cmdate1 = StrUtils.trim(params.get("CMDATE1")).replaceAll("/", ""); // 將 yyyy/MM/dd 的 "/" 去掉, 變成
																						// yyyyMMdd
			params.put("DUEDAT", DateTimeUtils.getCDateShort(DateTimeUtils.parse("yyyyMMdd", cmdate1))); // 轉成民國年
		}

		String code = params.get("CODE"); // 到期是否轉期
		if ("1".equals(code)) {
			params.put("CODE", params.get("FGSVTYPE"));
		} else if ("0".equals(code)) {
			// 不轉期時轉期次數放00
			params.put("CODE", "0");
			params.put("TXTIMES", "00");
		}

		String fgtxdate = (String) params.get("FGTXDATE");

		if ("1".equals(fgtxdate)) { // 即時
			OnLineTransferNotice trNotice = new OnLineTransferNotice();

			return this.online(params, trNotice);
		} else if ("2".equals(fgtxdate)) { // 預約

			if ("0".equals(params.get("FGTXWAY"))) { // 使用交易密碼, 做預約
				MVHImpl n950Result = n951Telcomm.query(params);
				// 若驗證失敗, 則會發動 TopMessageException
			} else if ("2".equals(params.get("FGTXWAY"))) { // 使用晶片金融卡, 做預約
				MVHImpl n954Result = n954Telcomm.query(params);
				// 若驗證失敗, 則會發動 TopMessageException
			}

			return this.booking(params);
		}

		return new MVHImpl();
	}

	/**
	 * 新預約交易
	 * 
	 * @param params
	 * @return
	 */
	@Transactional
	public MVHImpl booking(Map<String, String> params) {

		log.trace(ESAPIUtil.vaildLog("newbooking params>>>{}" + params));
		TXNTWSCHPAY txntwschpay = new TXNTWSCHPAY();
		// 產生 CERTACN update by Blair -------------------------------------------- Start
		String TxWay = params.get("FGTXWAY");
		String certACN = "";
		String sha1_Mac = "";
		String reqinfo_Str = "{}";

		if ("0".equals(TxWay)) {
			while (certACN.length() < 19)
				certACN += "2";
		} else if ("1".equals(TxWay)) {
			while (certACN.length() < 19)
				certACN += "3";
		}else if ("7".equals(TxWay)) {
			while (certACN.length() < 19)
				certACN += "D";
		} else {
			while (certACN.length() < 19)
				certACN += "5";
		}

		params.put("CERTACN", certACN);

		// 操作功能ID
		txntwschpay.setADOPID(params.get("ADOPID"));
		// 使用者ID
		txntwschpay.setDPUSERID(params.get("CUSIDN"));
		// 轉出帳號
		txntwschpay.setDPWDAC(params.get("ACN"));
		// 轉入行庫代碼
		txntwschpay.setDPSVBH(params.get("__TRIN_BANKCOD"));
		// 轉入帳號/繳費代號
		txntwschpay.setDPSVAC(params.get("__TRIN_ACN"));
		// 轉帳金額
		txntwschpay.setDPTXAMT(params.get("AMOUNT"));
		// 備註
		txntwschpay.setDPTXMEMO(params.get("CMTRMEMO"));
		// 發送Mail清單
		txntwschpay.setDPTXMAILS(params.get("CMTRMAIL"));
		// Mail備註
		txntwschpay.setDPTXMAILMEMO(params.get("CMMAILMEMO"));
		// 交易機制
		txntwschpay.setDPTXCODE(TxWay);
		// 塞入預約編號
		String dptmpschno = txnUserDao.getDptmpschno(params.get("CUSIDN"));
		txntwschpay.setDPSCHNO(dptmpschno);
		// 預約狀態 0:成功 ，1:取消
		txntwschpay.setDPTXSTATUS("0");
		// 預設NB
		txntwschpay.setLOGINTYPE(params.get("LOGINTYPE"));
		// 寫入日期
		Date d = new Date();
		txntwschpay.setDPSDATE(DateTimeUtils.format("yyyyMMdd", d));
		txntwschpay.setDPSTIME(DateTimeUtils.format("HHmmss", d));

		txntwschpay.setLASTDATE(txntwschpay.getDPSDATE());
		txntwschpay.setLASTTIME(txntwschpay.getDPSTIME());

		String f = params.get("FGTXDATE");
		if ("2".equals(f)) { // 預約某一日
			params.put("CMDATE", StrUtils.trim(params.get("CMDATE")).replaceAll("/", ""));
			if (params.get("CMDATE").length() == 0) {
				throw TopMessageException.create("ZX99");
			} else {
				txntwschpay.setDPTXTYPE("S");
				txntwschpay.setDPPERMTDATE("");
				txntwschpay.setDPFDATE(params.get("CMDATE"));
				txntwschpay.setDPTDATE(params.get("CMDATE"));
			}
		} else if ("3".equals(f)) {
			params.put("CMSDATE", StrUtils.trim(params.get("CMSDATE")).replaceAll("/", ""));
			params.put("CMEDATE", StrUtils.trim(params.get("CMEDATE")).replaceAll("/", ""));
			if (params.get("CMDD").length() == 0 || params.get("CMSDATE").length() == 0
					|| params.get("CMEDATE").length() == 0) {
				throw TopMessageException.create("ZX99");
			} else {
				txntwschpay.setDPTXTYPE("C");
				txntwschpay.setDPPERMTDATE(params.get("CMDD"));
				txntwschpay.setDPFDATE(params.get("CMSDATE"));
				txntwschpay.setDPTDATE(params.get("CMEDATE"));
			}
		}

		if (params.containsKey("pkcs7Sign") && "1".equals(params.get("FGTXWAY"))) {
			List<String> commands = new ArrayList();
			commands.add("XMLCA()");
			commands.add("XMLCN()");

			CommandUtils.doBefore(commands, params);

			if (!StrUtils.trim(params.get("XMLCN")).toUpperCase()
					.startsWith(StrUtils.trim(params.get("UID")).toUpperCase())) {
				throw TopMessageException.create("Z089");
			}

			txntwschpay.setXMLCA(StrUtils.trim(params.get("XMLCA")));
			txntwschpay.setXMLCN(StrUtils.trim(params.get("XMLCN")));
		}

		// 因IKEY資料太長 先移除避免塞爆欄位
		params.remove("pkcs7Sign");
		params.remove("__SIGN");
		// 預約時上行電文必要資訊
		log.trace(ESAPIUtil.vaildLog("txntwschpay.params>>>{}" + params));
		reqinfo_Str = CodeUtil.toJson(params);
		sha1_Mac = EncryptUtil.toEncS1(reqinfo_Str, "UTF-8").substring(0,4);
		txntwschpay.setMAC(sha1_Mac);
		txntwschpay.setDPTXINFO(reqinfo_Str);
		txntwschpay.setMSADDR(TxnTwSchPayDao.MS_TW);

		log.trace(ESAPIUtil.vaildLog("txntwschpay>>>{}" + txntwschpay.toString()));
		txntwschpaydao.save(txntwschpay);

		MVHImpl result = new MVHImpl();

		try {

			result.getFlatValues().put("CMTXTIME", DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", d));
			// 20190506 Ian為了得到回傳值
			params.remove("PINNEW");
			params.remove("jsondc");
			result.getFlatValues().put("OUTACN", params.get("ACN"));
			result.getFlatValues().put("TOPMSG", params.get("ACN"));
			result.getFlatValues().putAll(params);

		} catch (Exception e) {
			log.error("{}", e);
		}
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Map<String, String> scheduleObjectMapping() {
		Map<String, String> map = new HashMap();
		map.put("DPUSERID", "UID");
		map.put("DPWDAC", "ACN"); // 轉出帳號
		map.put("DPSVBH", "__TRIN_BANKCOD"); // 轉入分行
		map.put("DPSVAC", "__TRIN_ACN"); // 轉入帳號, 使用原始輸入的來記錄
		map.put("DPTXAMT", "AMOUNT");
		map.put("DPTXMEMO", "CMTRMEMO");
		map.put("DPTXMAILS", "CMTRMAIL");
		map.put("DPTXMAILMEMO", "CMMAILMEMO"); // CMMAILMEMO 對應的欄位
		map.put("ADOPID", "ADOPID");
		map.put("DPTXCODE", "FGTXWAY");

		return map;
	}

	/**
	 * 
	 * @param params
	 * @return
	 */
	public MVHImpl online(final Map<String, String> params, TransferNotice trNotice) {
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);
		params.put("INTSACN", params.get("__TRIN_ACN")); // 轉入帳號
		params.put("FLAG", params.get("__TRIN_AGREE"));

		String f = params.get("FGTXDATE");
		if ("1".equals(f)) { // 即時

			// 產生 CERTACN update by Blair -------------------------------------------- Start
			String TxWay = params.get("FGTXWAY");
			String certACN = "";

			if ("0".equals(TxWay)) {
				while (certACN.length() < 19)
					certACN += " ";
			} else if ("1".equals(TxWay)) {
				while (certACN.length() < 19)
					certACN += "1";
			} else if ("7".equals(TxWay)) {
				while (certACN.length() < 19)
					certACN += "C";
			} else {
				while (certACN.length() < 19)
					certACN += "4";
			}
			params.put("CERTACN", certACN);
			// 產生 CERTACN update by Blair -------------------------------------------- End
		}

		final TelCommExecWrapper execwrapper = new TelCommExecWrapper(n074Telcomm);

		MVHImpl result = execwrapper.query(params);// 一開始取得電文回應的地方
		log.debug(ESAPIUtil.vaildLog("result = " + result));
		if (result == null)
			params.put("#FDPNUM", " ");
		else
			params.put("#FDPNUM",
					result.getValueByFieldName("FDPNUM") == null ? " " : result.getValueByFieldName("FDPNUM"));

		final TXNTWRECORD record = (TXNTWRECORD) this.writeLog(execwrapper, params, result);

		final TelCommExecWrapper execwrapperFinal = execwrapper;
		trNotice.sendNotice(new NoticeInfoGetter() {

			public NoticeInfo getNoticeInfo() {
				NoticeInfo result = new NoticeInfo(params.get("UID"), params.get("DPMYEMAIL"), params.get("CMTRMAIL"));

				// result.setDpretxstatus(record.getDPRETXSTATUS());
				result.setADTXNO(record.getADTXNO());

				result.setTemplateName("transferDepNotify");
				result.setException(execwrapperFinal.getLastException());
				String datetime = record.getDPTXDATE() + record.getDPTXTIME();
				Date d = DateTimeUtils.parse("yyyyMMddHHmmss", datetime);
				String trantime = DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", d);
				result.getParams().put("#TRANTIME", trantime);
				result.getParams().put("#SEQNO", record.getDPTXNO());

				String acn = record.getDPSVAC();
				try {
					acn = StrUtils.left(acn, acn.length() - 5) + "***" + StrUtils.right(acn, 2);
				} catch (Exception e) {
				}

				String outacn = record.getDPWDAC();
				try {
					outacn = StrUtils.left(outacn, outacn.length() - 5) + "***" + StrUtils.right(outacn, 2);
				} catch (Exception e) {
				}

				result.getParams().put("#ACN", acn);
				result.getParams().put("#OUTACN", outacn);
				result.getParams().put("#FDPNUM", params.get("#FDPNUM"));
				result.getParams().put("#OUTCURRENCY", "新台幣");
				String dptxamtfmt = NumericUtil.formatNumberString(record.getDPTXAMT(), 0);
				result.getParams().put("#AMT", dptxamtfmt);
				result.getParams().put("#MEMO", params.get("CMMAILMEMO"));

				ADMMAILLOG admmaillog = new ADMMAILLOG();
				admmaillog.setADACNO(record.getDPWDAC());

				String trans_status = (String) params.get("__TRANS_STATUS");
				if (StrUtils.isEmpty(trans_status)) {
					trans_status = "TWONLINE";
				} else if ("SEND".equals(trans_status)) {
					trans_status = "TWSCH";
				} else if ("RESEND".equals(trans_status)) {
					trans_status = "TWSCHRE";
				}

				admmaillog.setADBATCHNO(trans_status);
				admmaillog.setADMAILACNO(StrUtils.trim(params.get("CMTRMAIL")));
				admmaillog.setADUSERID(record.getDPUSERID());
				admmaillog.setADUSERNAME("");
				admmaillog.setADSENDTYPE("3");
				admmaillog.setADSENDTIME(datetime);
				admmaillog.setADSENDSTATUS("");

				result.setADMMAILLOG(admmaillog);

				return result;
			}

		});

		execwrapper.throwException();
		try {
			result.getFlatValues().put("CMTXTIME",
					DateTimeUtils.getCDateTime(result.getValueByFieldName("DATE"), result.getValueByFieldName("TIME")));
		} catch (Exception e) {
			log.error("", e);
		}

		return result;
	}

	/**
	 * 
	 */
	public Object writeLog(TelCommExecWrapper execwrapper, Map<String, String> params, final MVHImpl result) {
		TXNTWRECORD record = execwrapper.createTxnTwRecord(sysDailySeqDao);

		String date = record.getLASTDATE();
		String time = record.getLASTTIME();

		if (result != null) {
			try {
				date = DateTimeUtils.format("yyyyMMdd",
						DateTimeUtils.getCDate2Date(result.getValueByFieldName("DATE")));
			} catch (Exception e) {
			}
			try {
				time = result.getValueByFieldName("TIME");
			} catch (Exception e) {
			}
		}

		try {
			record.setDPUSERID(params.get("UID"));
			record.setADOPID(params.get("ADOPID"));// 操作功能ID(NB3SysOp.ADOPID)
			record.setDPTXDATE(date);// 轉帳日期
			record.setDPTXTIME(time);// 轉帳時間
			record.setDPWDAC(params.get("ACN"));// 轉出帳號
			record.setDPSVBH("050"); // 轉入行庫代碼
			record.setDPSVAC(params.get("INTSACN")); // 轉入帳號
			record.setDPTXAMT(params.get("AMOUNT"));// 轉帳金額
			record.setDPTXMEMO(params.get("CMTRMEMO"));// 備註
			record.setDPTXMAILS(params.get("CMTRMAIL"));
			record.setDPTXMAILMEMO(params.get("CMMAILMEMO")); // CMMAILMEMO 對應的欄位
			record.setDPEFEE("0");// 手續費
			record.setDPTXNO(""); // 跨行序號
			record.setDPTXCODE(params.get("FGTXWAY"));// 交易機制
			record.setLOGINTYPE(params.get("LOGINTYPE"));
			// 移除pkcs7Sign 避免資料過長
			params.remove("pkcs7Sign");
			params.remove("__SIGN");

			record.setDPTITAINFO(CodeUtil.toJson(params));

			log.trace(ESAPIUtil.vaildLog(" writeLog record :{}" + record));
			// 20190510 edit by ian 改成不寫入 TXNREQINFO 只寫入TXNTWRECORD
			txnTwRecordDao.save(record);
		} catch (Exception e) {
			// TODO: handle exception
			log.error("writeLog error", e);
		}

		return record;
	}
}
