package fstop.services.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class A1000 extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("a1000Telcomm")
	private TelCommExec a1000Telcomm;

	@Override
	public MVH doAction(Map params) {
		HashMap<String, String> map = new HashMap<String, String>();
		String cust_id = params.get("Cust_ID") != null ? params.get("Cust_ID").toString() : "";
		map.put("Cust_ID", cust_id);
		MVHImpl result = a1000Telcomm.query(map);
		return result;
	}
}
