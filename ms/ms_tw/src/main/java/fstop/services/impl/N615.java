package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.model.EachRowCallback;
import fstop.model.IMasterValuesHelper;
import fstop.model.MVH;
import fstop.model.MVHUtils;
import fstop.model.Row;
import fstop.model.RowsSelecter;
import fstop.model.TelcommResult;
import fstop.orm.dao.AdmKeyValueDao;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;


@Slf4j
public class N615 extends CommonService{
	
	@Autowired
	@Qualifier("n615Telcomm")
	private TelCommExec n615Telcomm;
		
	@Autowired
	private AdmKeyValueDao admKeyValueDao;

	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) {
		String stadate = (String) params.get("CMSDATE");
		String enddate = (String) params.get("CMEDATE");
		stadate = StrUtils.trim(stadate).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
		enddate = StrUtils.trim(enddate).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
		
		String periodStr = "";
		if (stadate != null && stadate.length() == 8) {
			Date sd = DateTimeUtils.parse("yyyyMMdd", stadate);
			stadate = DateTimeUtils.getCDateShort(sd);
			periodStr = DateTimeUtils.format("yyyy/MM/dd", sd);
		}
		if (enddate != null && enddate.length() == 8) {
			Date sd = DateTimeUtils.parse("yyyyMMdd", enddate);
			enddate = DateTimeUtils.getCDateShort(sd);
			periodStr += "~" + DateTimeUtils.format("yyyy/MM/dd", sd);
		} else { // 若未指定結束時間, 則使用今天日期
			enddate = DateTimeUtils.getCDateShort(new Date());
			periodStr += "~" + DateTimeUtils.format("yyyy/MM/dd", new Date());
		}

		params.put("STADATE", stadate);
		params.put("ENDDATE", enddate);
		
		TelcommResult telcommResult = n615Telcomm.query(params);
		
		telcommResult.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		telcommResult.getFlatValues().put("CMPERIOD", periodStr);
		telcommResult.getFlatValues().put("CMRECNUM", telcommResult.getValueOccurs("ACN") + "");
		telcommResult.getFlatValues().put("ACN", telcommResult.getValueByFieldName("ACN", 1));
		
		RowsSelecter rowSelecter = new RowsSelecter(telcommResult.getOccurs());
		rowSelecter.each(new EachRowCallback(){

			public void current(Row row) {			
//				使用 adm key value 轉換 memo
				String r = admKeyValueDao.translator("MEMO", row.getValue("MEMO"));
				row.setValue("MEMO_C", r);
			}});
		Map<String, IMasterValuesHelper> tables = MVHUtils.groupByToTables(telcommResult, new String[]{"ACN"});
		MVHUtils.addTables(telcommResult, tables);

		return telcommResult;
	}



}
