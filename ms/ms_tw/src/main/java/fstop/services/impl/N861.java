package fstop.services.impl;

import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import topibs.utility.NumericUtil;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.TxnReqInfoDao;
import fstop.orm.dao.TxnTwRecordDao;
import fstop.orm.po.ADMMAILLOG;
import fstop.orm.po.TXNREQINFO;
import fstop.orm.po.TXNTWRECORD;
import fstop.services.BookingUtils;
import fstop.services.CommonService;
import fstop.services.NoticeInfo;
import fstop.services.NoticeInfoGetter;
import fstop.services.OnLineTransferNotice;
import fstop.services.TelCommExecWrapper;
import fstop.services.WriteLogInterface;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 薪轉戶優惠次數查詢
 * @author jinhanhuang
 *
 */
@Slf4j
public class N861 extends CommonService {
	private Logger logger = Logger.getLogger(getClass());
	
	@Autowired
	@Qualifier("n861Telcomm")
	private TelCommExec n861Telcomm;
	
//	@Required
//	public void setN108Telcomm(TelCommExec telcomm) {
//		n108Telcomm = telcomm;
//	}

	/**
	 * 
	 * @param params
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public MVH doAction(Map _params) {
		final Map<String, String> params = _params;
		
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);

		MVHImpl result = n861Telcomm.query(params);
		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		
		return result;
	}
}
