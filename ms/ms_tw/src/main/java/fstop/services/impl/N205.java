package fstop.services.impl;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.RowsSelecter;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N205 extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("n205Telcomm")
	private TelCommExec n205Telcomm;

//	@Required
//	public void setN110Telcomm(TelCommExec n110Telcomm) {
//		this.n110Telcomm = n110Telcomm;
//	}

	@Override
	public MVH doAction(Map params) {
		MVHImpl result = n205Telcomm.query(params);
		try {
			String count = result.getValueByFieldName("REC_NO");
			if(Integer.parseInt(count) < 1) {
				log.debug("No Digital Deposit Acn");
				result.getFlatValues().put("TOPMSG", "ENRD");
				result.getFlatValues().put("ABEND", "ENRD");
				throw TopMessageException.create("ENRD");
			}
		}
		catch (Exception e) {
			// TODO: handle exception
			log.error("" + e);
		}
		return result;
	}
}
