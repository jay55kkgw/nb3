package fstop.services.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import lombok.extern.slf4j.Slf4j;
/**
 * 
 * 功能說明 :金融卡優惠次數查詢
 *
 */
@Slf4j
public class N858 extends CommonService {

	@Autowired
	@Qualifier("n858Telcomm")
	private TelCommExec n858Telcomm;


	@Override
	public MVH doAction(Map params) {
		MVHImpl result = n858Telcomm.query(params);
		return result;
	}
}
