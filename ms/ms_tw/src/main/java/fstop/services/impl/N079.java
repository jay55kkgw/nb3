package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.util.ESAPIUtil;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.orm.dao.TxnTwRecordDao;
import fstop.orm.po.ADMMAILLOG;
import fstop.orm.po.TXNTWRECORD;
import fstop.services.CommonService;
import fstop.services.DispatchService;
import fstop.services.NoticeInfo;
import fstop.services.NoticeInfoGetter;
import fstop.services.OnLineTransferNotice;
import fstop.services.ServiceBindingException;
import fstop.services.TelCommExecWrapper;
import fstop.services.WriteLogInterface;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import topibs.utility.NumericUtil;

/**
 * 台幣綜存定存解約
 * 
 * @author Owner
 *
 */
@Slf4j
public class N079 extends DispatchService implements WriteLogInterface {
	@Autowired
	private CommonService n421Service;
	@Autowired
	@Qualifier("n079Telcomm")
	private TelCommExec n079Telcomm;
	@Autowired
	@Qualifier("n079ExecTelcomm")
	private TelCommExec n079ExecTelcomm;
	@Autowired
	private TxnTwRecordDao txnTwRecordDao;

	@Override
	public String getActionMethod(Map<String, String> params) {
		String txid = StrUtils.trim(params.get("TXID"));
		log.debug(ESAPIUtil.vaildLog("N079 txid: {}" + txid));
		if ("N079".equals(txid))
			return "doN421";
		else if ("N079_1".equals(txid))
			return "doN079_1";
		else if ("N079_2".equals(txid))
			return "doN079_2";
		else
			throw new ServiceBindingException("不支援的 TXID [" + txid + "].");
	}

	public MVH doN421(Map<String, String> params) {
		return n421Service.doAction(params);
	}

	/**
	 * 
	 * @param params
	 * @return
	 */
	public MVH doN079_1(Map<String, String> params) {
		log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Do N079_1");

		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);

		params.put("TXNTYPE", "01");
		params.put("DPISDT", StrUtils.trim(params.get("DPISDT").toString()).replaceAll("/", ""));
		params.put("DUEDAT", StrUtils.trim(params.get("DUEDAT").toString()).replaceAll("/", ""));

		// TODO 格式應訂在電文格式裡
		// params.put("AMT",
		// StrUtils.trim(params.get("AMT").toString()).replaceAll("\\.", ""));
		params.put("AMT", StrUtils.trim(params.get("AMT").toString()).replaceAll(",", ""));
		// params.put("ITR", StrUtils.trim(params.get("ITR").toString()).replaceAll(".",
		// ""));

		TelcommResult result = n079Telcomm.query(params);
		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));

		try {
			result.getFlatValues().put("CMTXTIME", DateTimeUtils.getCDateTime(result.getValueByFieldName("TRNDATE"),
					result.getValueByFieldName("TRNTIME")));
		} catch (Exception e) {
			log.error("doN079_1 error >>{}", e);
		}

		return result;
	}

	public MVH doN079_2(Map _params) {
		log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Do N079_2");
		final Map<String, String> params = _params;
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);

		// 晶片金融卡
		if ("2".equals(params.get("FGTXWAY").toString())) {
			// Date d = new Date();
			String trin_transeq = "1234";
			String trin_issuer = "05000000";
			String trin_acnno = params.get("ACNNO").toString();
			String trin_icdttm = DateTimeUtils.format("yyyyMMddHHmmss", d);
			log.debug("ICDTTM ----------> {}", trin_icdttm);
			String trin_iseqno = params.get("iSeqNo").toString();
			String trin_icmemo = "";
			String trin_tac_length = "000A";
			String trin_tac = params.get("TAC").toString();
			log.debug("TAC ----------> {}",
					trin_tac_length + trin_tac + "(" + (trin_tac_length.length() + trin_tac.length()) + ")");
			String trin_trmid = params.get("TRMID").toString();

			params.put("TRANSEQ", trin_transeq);
			params.put("ISSUER", trin_issuer);
			params.put("ACNNO", trin_acnno);
			params.put("ICDTTM", trin_icdttm);
			params.put("ICSEQ", trin_iseqno);
			log.debug("ICSEQ ----------> {}", trin_iseqno);
			params.put("ICMEMO", trin_icmemo);
			params.put("TAC_Length", trin_tac_length);
			params.put("TAC", trin_tac);
			params.put("TRMID", trin_trmid);
		}

		// 產生 CERTACN update by Blair -------------------------------------------- Start
		String TxWay = params.get("FGTXWAY");
		String certACN = "";
		log.debug("FGTXWAY ---------------------------------------> {}", TxWay);
		if ("0".equals(TxWay)) {
			while (certACN.length() < 19)
				certACN += " ";
		} else if ("1".equals(TxWay)) {
			while (certACN.length() < 19)
				certACN += "1";
		} else if ("7".equals(TxWay)) {
			while (certACN.length() < 19)
				certACN += "C";
		}else {
			while (certACN.length() < 19)
				certACN += "4";
		}
		log.debug("CERTACN ---------------------------------------> " + certACN);
		params.put("CERTACN", certACN);
		// 產生 CERTACN update by Blair -------------------------------------------- End

		params.put("TXNTYPE", "02");
		params.put("DPISDT", StrUtils.trim(params.get("DPISDT").toString()).replaceAll("/", ""));
		params.put("DUEDAT", StrUtils.trim(params.get("DUEDAT").toString()).replaceAll("/", ""));
		// params.put("AMT", StrUtils.trim(params.get("AMT").toString()).replaceAll(".",
		// ""));
		params.put("AMT", StrUtils.trim(params.get("AMT").toString()).replaceAll(",", ""));
		// params.put("ITR", StrUtils.trim(params.get("ITR").toString()).replaceAll(".",
		// ""));
		final TelCommExecWrapper execwrapper = new TelCommExecWrapper(n079ExecTelcomm);
		MVHImpl result = execwrapper.query(params);

		if (result != null) {
			log.debug("INTPAY = " + result.getValueByFieldName("INTPAY"));
			String intpay = NumericUtil.formatNumberString(result.getValueByFieldName("INTPAY"), 0);
			log.debug("format intpay = " + intpay);
			params.put("#INTPAY", intpay);

			log.debug("TAX = " + result.getValueByFieldName("TAX"));
			String tax = NumericUtil.formatNumberString(result.getValueByFieldName("TAX"), 0);
			log.debug("format tax = " + tax);
			params.put("#TAX", tax);

			log.debug("INTRCV = " + result.getValueByFieldName("INTRCV"));
			String intrcv = NumericUtil.formatNumberString(result.getValueByFieldName("INTRCV"), 0);
			log.debug("format intrcv = " + intrcv);
			params.put("#INTRCV", intrcv);

			log.debug("PAIAFTX = " + result.getValueByFieldName("PAIAFTX"));
			String paiaftx = NumericUtil.formatNumberString(result.getValueByFieldName("PAIAFTX"), 0);
			String nhitax = NumericUtil.formatNumberString(result.getValueByFieldName("NHITAX"), 0);
			log.debug("format paiaftx = " + paiaftx);
			params.put("#PAIAFTX", paiaftx);
			params.put("#NHITAX", nhitax);
			log.debug("format nhitax = " + nhitax);
		}

		final TXNTWRECORD record = (TXNTWRECORD) this.writeLog(execwrapper, params, result);

		OnLineTransferNotice trNotice = new OnLineTransferNotice();
		final TelCommExecWrapper execwrapperFinal = execwrapper;
		trNotice.sendNotice(new NoticeInfoGetter() {

			public NoticeInfo getNoticeInfo() {
				NoticeInfo result = new NoticeInfo(params.get("UID"), params.get("DPMYEMAIL"), params.get("CMTRMAIL"));

				result.setADTXNO(record.getADTXNO());

				result.setTemplateName("TwDepCancelNotify");
				result.setException(execwrapperFinal.getLastException());
				String datetime = record.getDPTXDATE() + record.getDPTXTIME();
				log.debug("datetime = " + datetime);
				Date d = DateTimeUtils.parse("yyyyMMddHHmmss", datetime);
				log.debug("Hours = " + d.getHours());
				log.debug("Minutes = " + d.getMinutes());
				log.debug("Seconds = " + d.getSeconds());
				String trantime = DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", d);
				log.debug("trantime = " + trantime);
				result.getParams().put("#TRANTIME", trantime);

				String acn = record.getDPSVAC().trim();

				String outacn = record.getDPWDAC().trim();
				try {
					outacn = StrUtils.left(outacn, outacn.length() - 5) + "***" + StrUtils.right(outacn, 2);
				} catch (Exception e) {
				}

				result.getParams().put("#ACN", acn);
				result.getParams().put("#OUTACN", outacn);
				result.getParams().put("#OUTCURRENCY", "新台幣");
				String dptxamtfmt = NumericUtil.formatNumberString(record.getDPTXAMT(), 0);
				result.getParams().put("#AMT", dptxamtfmt);

				log.debug("format #INTPAY = " + params.get("#INTPAY"));
				result.getParams().put("#INTPAY", params.get("#INTPAY"));
				log.debug("format #TAX = " + params.get("#TAX"));
				result.getParams().put("#TAX", params.get("#TAX"));
				log.debug("format #INTRCV = " + params.get("#INTRCV"));
				result.getParams().put("#INTRCV", params.get("#INTRCV"));
				log.debug("format #PAIAFTX = " + params.get("#PAIAFTX"));
				result.getParams().put("#PAIAFTX", params.get("#PAIAFTX"));
				result.getParams().put("#NHITAX", params.get("#NHITAX"));

				ADMMAILLOG admmaillog = new ADMMAILLOG();
				admmaillog.setADACNO(record.getDPWDAC());

				String trans_status = (String) params.get("__TRANS_STATUS");
				if (StrUtils.isEmpty(trans_status)) {
					trans_status = "TWONLINE";
				} else if ("SEND".equals(trans_status)) {
					trans_status = "TWSCH";
				} else if ("RESEND".equals(trans_status)) {
					trans_status = "TWSCHRE";
				}

				admmaillog.setADBATCHNO(trans_status);
				admmaillog.setADMAILACNO(StrUtils.trim(params.get("CMTRMAIL")));
				admmaillog.setADUSERID(record.getDPUSERID());
				admmaillog.setADUSERNAME("");
				admmaillog.setADSENDTYPE("3");
				admmaillog.setADSENDTIME(datetime);
				admmaillog.setADSENDSTATUS("");

				result.setADMMAILLOG(admmaillog);

				return result;
			}

		});

		execwrapper.throwException();

		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		try {
			result.getFlatValues().put("CMTXTIME", DateTimeUtils.getCDateTime(result.getValueByFieldName("TRNDATE"),
					result.getValueByFieldName("TRNTIME")));
		} catch (Exception e) {
			log.error("", e);
		}

		return result;
	}

	/**
	 * 
	 */
	public Object writeLog(TelCommExecWrapper execwrapper, Map<String, String> params, final MVHImpl result) {

		TXNTWRECORD record = execwrapper.createTxnTwRecord();

		String date = record.getLASTDATE();
		String time = record.getLASTTIME();

		if (result != null) {
			try {
				date = DateTimeUtils.format("yyyyMMdd",
						DateTimeUtils.getCDate2Date(result.getValueByFieldName("TRNDATE")));
			} catch (Exception e) {
			}
			try {
				time = result.getValueByFieldName("TRNTIME");
			} catch (Exception e) {
			}
		}

		try {
			log.trace("params >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> {}", params);

			record.setDPUSERID(params.get("UID"));
			record.setADOPID(params.get("ADOPID"));
			record.setDPTXDATE(date);
			record.setDPTXTIME(time);
			record.setDPWDAC(params.get("FDPACN"));
			record.setDPSVBH("");
			record.setDPSVAC(params.get("FDPNUM"));
			record.setDPTXAMT(params.get("AMT"));
			//
			record.setDPTXMEMO(params.get("CMTRMEMO"));
			record.setDPTXMAILS(params.get("CMTRMAIL"));
			record.setDPTXMAILMEMO(params.get("CMMAILMEMO")); // CMMAILMEMO 對應的欄位

			record.setDPEFEE("0");
			record.setDPTXNO(""); // 跨行序號
			record.setDPTXCODE(params.get("FGTXWAY"));
			// 欄位長度過長資料庫塞不進去，故remove
			params.remove("pkcs7Sign");
			params.remove("jsondc");
			params.remove("__SIGN");
			// 20190510 edit by ian 改成不寫入 TXNREQINFO 只寫入TXNTWRECORD
			txnTwRecordDao.save(record);
		} catch (Exception e) {
			log.error("writeLog error", e);
		}
		return record;
	}
}
