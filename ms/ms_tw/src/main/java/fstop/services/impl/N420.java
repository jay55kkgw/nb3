package fstop.services.impl;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.model.EachRowCallback;
import fstop.model.MVH;
import fstop.model.Row;
import fstop.model.RowsSelecter;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N420 extends CommonService {

	@Autowired
    @Qualifier("n420Telcomm")
	private TelCommExec n420Telcomm;
	
//	@Required
//	public void setN420Telcomm(TelCommExec telcomm) {
//		n420Telcomm = telcomm;
//	}

	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	@SuppressWarnings("unchecked")
	public MVH doAction(Map params) {
		TelcommResult telcommResult = n420Telcomm.query(params);

		telcommResult.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		telcommResult.getFlatValues().put("CMRECNUM", telcommResult.getValueOccurs("ACN") + "");

		RowsSelecter rowSelecter = new RowsSelecter(telcommResult.getOccurs());
		rowSelecter.each(new EachRowCallback(){

			public void current(Row row) {
				
				if("0".equalsIgnoreCase(row.getValue("INTMTH"))) {
					row.setValue("DPINTTYPE", "機動");
				}
				if("1".equalsIgnoreCase(row.getValue("INTMTH"))) {
					row.setValue("DPINTTYPE", "固定");
				}
				
			}});

		DecimalFormat fmt = new DecimalFormat("0.00");
		Double amtfdp = (double)rowSelecter.sum("AMTFDP");;
		telcommResult.getFlatValues().put("DPTAMT", fmt.format(amtfdp));
		
		return telcommResult;
	}


}
