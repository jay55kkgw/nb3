package fstop.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.netbank.rest.model.JSPUtils;
import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.notification.Notification;
import fstop.notification.PushData;
import fstop.notification.TopicLabel;
import fstop.notifier.PushAgent;
import fstop.orm.dao.TxnPhoneTokenDao;
import fstop.orm.po.TXNPHONETOKEN;
import fstop.services.CommonService;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class NotifyApi extends CommonService  {
	
	@Autowired
	private TxnPhoneTokenDao txnPhoneTokenDao;

	@Value("${firebase_useflag}")
	private String firebase_useflag;
	
	@Override
	public MVH doAction(Map params) { 
		MVHImpl telcommResult = new MVHImpl();
		try {
			String cusidn = (String)params.get("CUSIDN");
			String pushtext = (String)params.get("pushtext");
			List<PushData> listPushData = new ArrayList<PushData>();
			// 發送推播通知開始
			TXNPHONETOKEN txnphonetoken = new TXNPHONETOKEN();
			txnphonetoken = txnPhoneTokenDao.getPhoneToken(cusidn);
			try {
				if (txnphonetoken != null) {
					if (txnphonetoken.getPHONETYPE().equals("I")) {
						if(firebase_useflag.equals("Y")) {

							String token = txnphonetoken.getPHONETOKEN();
							if (token != null) {
								PushData pushData = new PushData();
								Map<String, String> messageMap = new HashMap<String, String>();
//								messageMap.put("TYPE", "TR");
								messageMap.put("MESSAGE", pushtext);
								// pushData.setMessage("(帳號:" + r.getValue("ACN") + ")" + memotext);
								pushData.setMessage(pushtext);
								pushData.setTopic(TopicLabel.TOPICLABEL_ID + token);
								pushData.setCusId(cusidn);
								pushData.setToken(token);
								Map<String, String> notification = new HashMap<String, String>();
								notification.put("title", pushtext);
//								notification.put("title", TopicLabel.TOPICLABEL_ID + token);
//								notification.put("body", JSONUtils.map2json(messageMap));
								pushData.setNotification(notification);

								listPushData.add(pushData);
								log.debug(ESAPIUtil.vaildLog("pushData.getTopic:" + TopicLabel.TOPICLABEL_ID + token));
								log.debug(ESAPIUtil.vaildLog("pushData.getCusId:" + cusidn));
							} else {
								log.debug(ESAPIUtil.vaildLog("N185 CUSIDN = " + cusidn + " PhoneToken不存在"));
							}
							
						}else {

							List<Object> arrdevices = new ArrayList<Object>();
							List<Object> arruser = new ArrayList<Object>();
							arrdevices.add(txnphonetoken.getPHONETOKEN());
							arruser.add(txnphonetoken.getDPUSERID());
							// Notification notification =
							// (Notification)SpringBeanFactory.getBean("notification");
							Notification notification = new Notification();
							// Notification notification = new Notification();
							log.info(ESAPIUtil.vaildLog("APNS Start(" + txnphonetoken.getPHONETOKEN() + ")"));
							// notification.notificationToiOS(arrdevices, "(帳號:" + r.getValue("ACN") + ")" +
							// memotext,
							// "5", 0, "");
							notification.notificationToiOS(arruser, arrdevices, pushtext, "5", 0, "");
							log.info(ESAPIUtil.vaildLog("APNS End(" + txnphonetoken.getPHONETOKEN() + ")"));
						}

					} else if (txnphonetoken.getPHONETYPE().equals("A")) {
						String token = txnphonetoken.getPHONETOKEN();
						if (token != null) {
							PushData pushData = new PushData();
							Map<String, String> messageMap = new HashMap<String, String>();
							messageMap.put("TYPE", "TR");
							messageMap.put("MESSAGE", pushtext);
							// pushData.setMessage("(帳號:" + r.getValue("ACN") + ")" + memotext);
							pushData.setMessage(JSONUtils.map2json(messageMap));
							pushData.setTopic(TopicLabel.TOPICLABEL_ID + token);
							pushData.setCusId(cusidn);
							pushData.setToken(token);
							pushData.setNotification(null);

							listPushData.add(pushData);
							log.debug(ESAPIUtil.vaildLog("pushData.getTopic:" + TopicLabel.TOPICLABEL_ID + token));
							log.debug(ESAPIUtil.vaildLog("pushData.getCusId:" + cusidn));
						} else {
							log.debug(ESAPIUtil.vaildLog("N185 CUSIDN = " + cusidn + " PhoneToken不存在"));
						}
					}
				}
				telcommResult.getFlatValues().put("TOPMSG", "0000");
			} catch (Exception e) {
				telcommResult.getFlatValues().put("TOPMSG", "ZX99");
				log.error("notification area error");
			}
			log.debug("listPushData.size: " + listPushData.size());
			try {
				PushAgent.sendNotice(listPushData,txnphonetoken.getPHONETYPE());
			}catch(Exception e) {
				telcommResult.getFlatValues().put("TOPMSG", "ZX99");
				log.error("PushAgent.sendNotice Error");
			}
		}
		catch(TopMessageException top) {
			telcommResult.getFlatValues().put("TOPMSG", top.getMsgcode());
		}catch(Exception e){
			telcommResult.getFlatValues().put("TOPMSG", "ZX99");
		}finally {
			telcommResult.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		}
		return telcommResult;
	}
}
