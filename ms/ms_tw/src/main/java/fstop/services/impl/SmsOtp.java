package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

import com.netbank.rest.model.CommonPools;
import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.TxnSmsLogDao;
import fstop.orm.po.TXNSMSLOG;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.SMSOTPProperties;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import ni.otp.Proc00F2A;
import ni.otp.Proc00F8;
import ni.otp.Proc00FA;
import ni.otp.Proc00FB;
import tw.com.mitake.sms.MitakeSms;
import tw.com.mitake.sms.result.MitakeSmsSendResult;
import tw.com.mitake.sms.result.MitakeSmsSendResult.SendResult;
import tw.com.tbb.utils.GetConfigProperty;

/**
 * OTP驗證
 */
@Slf4j
public class SmsOtp extends CommonService {

	// private static Logger logger = Logger.getLogger(SmsOtp.class);

	// private static final String OTP_TYPE = "NNB";
	// private static final String OTP_SECOND = "120";
	private final String OTP_TYPE = "NNB";
	private final String OTP_SECOND = "120";

	@Autowired
	@Qualifier("n960Telcomm")
	private TelCommExec n960Telcomm;

	@Autowired
	private CommonPools commonPools;
	@Autowired
	private SMSOTPProperties smsotp;
	
	@Value("${sms_url}")
	private String smsurl;
	

	// private static TxnSmsLogDao txnSmsLogDao =
	// (TxnSmsLogDao)SpringBeanFactory.getBean("txnSmsLogDao");
	@Autowired
	private TxnSmsLogDao txnSmsLogDao;

	// public static MVH action(Map params) {
	public MVH action(Map params) {

		// logger.warn("===SmsOtp web service request data action method start ===");
		// logger.warn("SmsOtp getSessionId() = " + params.get("SessionId"));
		log.warn("===SmsOtp web service request data action method start ===");
		log.warn(ESAPIUtil.vaildLog("SmsOtp getSessionId()  = " + params.get("SessionId")));

		MVHImpl result = new MVHImpl();
		int retCode = 0;
		String otpIp = null;
		String otpPort = null;
		try {

			// logger.warn("=============上行電文============");
			// logger.warn("CUSIDN=====>>>>" + params.get("CUSIDN"));
			// logger.warn("OTPKEY=====>>>>" + params.get("OTPKEY"));
			log.warn("=============上行電文============");
			log.warn(ESAPIUtil.vaildLog("CUSIDN=====>>>>" + params.get("CUSIDN")));
			log.warn(ESAPIUtil.vaildLog("OTPKEY=====>>>>" + params.get("OTPKEY")));

			otpIp = smsotp.getOTPIP();
			otpPort = smsotp.getOTPPORT();

			String[] cardId = new Proc00F2A().response((String) params.get("CUSIDN"), otpIp, otpPort);
			for (int i = 0; i < cardId.length; i++) {

				// logger.warn("cardId==============>"+cardId[i]);
				log.warn(ESAPIUtil.vaildLog("cardId==============>" + cardId[i]));
			}

			if (cardId[0].length() != 8) {
				result.getFlatValues().put("MSGCOD", cardId[0]);
				throw TopMessageException.create(cardId[0]);
			}

			retCode = new Proc00F8().response((String) params.get("CUSIDN") + "-" + cardId[0],
					params.get("OTPKEY").toString(), otpIp, otpPort);
			// logger.warn("retCode=========>"+retCode);
			log.warn(ESAPIUtil.vaildLog("retCode=========>" + retCode));
			if (retCode != 1) {
				result.getFlatValues().put("MSGCOD", Integer.toString(retCode));
				throw TopMessageException.create(Integer.toString(retCode));
			}

			// 產生回應訊息
			result.getFlatValues().put("MSGCOD", "0000");
			result.getFlatValues().put("MSGSTR", "OTP密碼驗證成功");

		} catch (TopMessageException top) {
			TopMessageException top1 = TopMessageException.create(top.getMsgcode());

			result.getFlatValues().put("MSGCOD", top1.getMsgcode());
			result.getFlatValues().put("MSGSTR", top1.getMsgout());
			// logger.warn("SmsOtp.action() TopMessageException==" + top);
			// logger.warn("SmsOtp.action() Msgcode==" + top1.getMsgcode());
			// logger.warn("SmsOtp.action() Msgout==" + top1.getMsgout());
			log.warn("SmsOtp.action() TopMessageException==" + top);
			log.warn(ESAPIUtil.vaildLog("SmsOtp.action() Msgcode==" + top1.getMsgcode()));
			log.warn(ESAPIUtil.vaildLog("SmsOtp.action() Msgout==" + top1.getMsgout()));

		} catch (Exception e) {
			// logger.warn("SmsOtp.action() Exception==" + e);
			// logger.warn("SmsOtp MsgCode == ZX99 ");
			log.warn("SmsOtp.action() Exception==" + e);
			log.warn("SmsOtp MsgCode == ZX99 ");

			result.getFlatValues().put("MSGCOD", "ZX99");
			result.getFlatValues().put("MSGSTR", "系統異常!");
		}

		// logger.warn("===SmsOtp action method web service handle end ===");
		log.warn("===SmsOtp action method web service handle end ===");

		return result;
	}

	// 申請軟體式動態密碼
	// public static MVH registerSMSOTP(String userID) {
	public MVH registerSMSOTP(String userID) {

		// logger.warn("===SmsOtp registerSMSOTP method start ===");
		log.warn("===SmsOtp registerSMSOTP method start ===");

		MVHImpl result = new MVHImpl();
		int retCode = 0;
		String otpIp = null;
		String otpPort = null;
		try {

			// logger.warn("=============上行電文============");
			log.warn("=============上行電文============");

			otpIp = smsotp.getOTPIP();
			otpPort = smsotp.getOTPPORT();

			// 軟體式動態密碼申請
			retCode = new Proc00FA().response2(OTP_TYPE + "-" + userID, otpIp, otpPort, "RSPWD", "NNB");

			if (retCode != 1) {
				result.getFlatValues().put("MSGCOD", Integer.toString(retCode));
				return result;
				// throw TopMessageException.create(Integer.toString(retCode));
			}

			// 產生回應訊息
			result.getFlatValues().put("MSGCOD", "0000");
			result.getFlatValues().put("MSGSTR", "OTP密碼註冊成功");
			// logger.warn("===SmsOtp registerSMSOTP success===");
			log.warn("===SmsOtp registerSMSOTP success===");

		} catch (TopMessageException top) {
			TopMessageException top1 = TopMessageException.create(top.getMsgcode());

			// logger.warn("SmsOtp.action() Msgcode==" + top1.getMsgcode());
			// logger.warn("SmsOtp.action() Msgout==" + top1.getMsgout());
			// logger.warn("SmsOtp.registerSMSOTP() TopMessageException==" + top);
			// logger.warn("SmsOtp.registerSMSOTP() Msgcode==" + top1.getMsgcode());
			// logger.warn("SmsOtp.registerSMSOTP() Msgout==" + top1.getMsgout());
			log.warn(ESAPIUtil.vaildLog("SmsOtp.action() Msgcode==" + top1.getMsgcode()));
			log.warn(ESAPIUtil.vaildLog("SmsOtp.action() Msgout==" + top1.getMsgout()));
			log.warn("SmsOtp.registerSMSOTP() TopMessageException==" + top);
			log.warn(ESAPIUtil.vaildLog("SmsOtp.registerSMSOTP() Msgcode==" + top1.getMsgcode()));
			log.warn(ESAPIUtil.vaildLog("SmsOtp.registerSMSOTP() Msgout==" + top1.getMsgout()));

		} catch (Exception e) {
			// logger.warn("SmsOtp.registerSMSOTP() Exception==" + e);
			// logger.warn("SmsOtp MsgCode == ZX99 ");
			log.warn("SmsOtp.registerSMSOTP() Exception==" + e);
			log.warn("SmsOtp MsgCode == ZX99 ");

			result.getFlatValues().put("MSGCOD", "ZX99");
			result.getFlatValues().put("MSGSTR", "系統異常!");
		}

		// logger.warn("===SmsOtp registerSMSOTP method end===");
		log.warn("===SmsOtp registerSMSOTP method end===");

		return result;
	}

	// 取得軟體式動態密碼
	// public static MVH getSMSOTP(String userID, int count,String adopid) {
	public MVH getSMSOTP(String userID, int count, String adopid ,String msgType) {

		// logger.warn("===SmsOtp getSMSOTP method start ===");
		log.warn("===SmsOtp getSMSOTP method start ===");

		MVHImpl result = new MVHImpl();
		String otpIp = null;
		String otpPort = null;
		try {

			// logger.warn("=============上行電文============");
			log.warn("=============上行電文============");

			log.debug("smsotp>>>{}", smsotp);
			otpIp = smsotp.getOTPIP();
			otpPort = smsotp.getOTPPORT();
			log.debug("otpIp>>>" + otpIp + " otpPort>>>" + otpPort);

			// 軟體式動態密碼申請 (有效期120秒)
			String OTP = "";
			// logger.warn("申請 registerSMSOTP=====>>>> Proc00FB()==> C1="+ OTP_TYPE + "-" +
			// userID +" C2="+ OTP_SECOND +"C3=" + otpIp + "C4=" + otpPort);
			log.warn(ESAPIUtil.vaildLog("申請  registerSMSOTP=====>>>> Proc00FB()==> C1=" + OTP_TYPE + "-" + userID
					+ " C2=" + OTP_SECOND + "C3=" + otpIp + "C4=" + otpPort));
			OTP = new Proc00FB().response2(OTP_TYPE + "-" + userID, OTP_SECOND, otpIp, otpPort, "RSPWD", "NNB");

			if (OTP.equals("E6102")) { // 使用者帳號錯誤(尚未註冊)
				// logger.warn("Aotu registerSMSOTP=====>>>>" + userID);
				log.warn(ESAPIUtil.vaildLog("Aotu registerSMSOTP=====>>>>" + userID));
				MVH cmMsg = registerSMSOTP(userID);
				if (cmMsg.getValueByFieldName("MSGCOD").equals("0000")) {
					// logger.warn("註冊 registerSMSOTP=====>>>> Proc00FB()==> C1="+ OTP_TYPE + "-" +
					// userID +" C2="+ OTP_SECOND +"C3=" + otpIp + "C4=" + otpPort);
					log.warn(ESAPIUtil.vaildLog("註冊  registerSMSOTP=====>>>> Proc00FB()==> C1=" + OTP_TYPE + "-"
							+ userID + " C2=" + OTP_SECOND + "C3=" + otpIp + "C4=" + otpPort));
					OTP = new Proc00FB().response2(OTP_TYPE + "-" + userID, OTP_SECOND, otpIp, otpPort, "RSPWD", "NNB");
				} else {
					result.getFlatValues().put("MSGCOD", cmMsg.getValueByFieldName("MSGCOD"));
					return result;
					// throw TopMessageException.create(cmMsg.getMsgCode());
				}
			}

			if (OTP.length() != 8) { // 有錯誤訊息
				TopMessageException top1 = TopMessageException.create(OTP);

				result.getFlatValues().put("MSGCOD", top1.getMsgcode());
				result.getFlatValues().put("MSGSTR", top1.getMsgout());
				// logger.warn("SmsOtp.getSMSOTP() TopMessageException==" + OTP);
				// logger.warn("SmsOtp.getSMSOTP() Msgcode==" + top1.getMsgcode());
				// logger.warn("SmsOtp.getSMSOTP() Msgout==" + top1.getMsgout());
				log.warn(ESAPIUtil.vaildLog("SmsOtp.getSMSOTP() TopMessageException==" + OTP));
				log.warn(ESAPIUtil.vaildLog("SmsOtp.getSMSOTP() Msgcode==" + top1.getMsgcode()));
				log.warn(ESAPIUtil.vaildLog("SmsOtp.getSMSOTP() Msgout==" + top1.getMsgout()));

				return result;
				// throw TopMessageException.create(OTP);
			}

			// 發送簡訊
			String message = "";
			MVH phone =  null;
			String MOBTEL = "";
			if(msgType.startsWith("N822")) {
				MOBTEL = msgType.replace("N822", "");
				msgType = "N822";
			}
			else {
				phone = getPhoneNum(userID);
				MOBTEL = phone.getValueByFieldName("MOBTEL");
			}
			// logger.warn("SmsOtp.getSMSOTP() phone==" + MOBTEL);
			log.warn(ESAPIUtil.vaildLog("SmsOtp.getSMSOTP() phone==" + MOBTEL));
			if (MOBTEL.length() == 10) {
				switch (msgType) {
				case "IDGATE":
					message = "【臺灣企銀】您的申請裝置綁定註冊碼作業之驗證碼為" + OTP + "，請於2分鐘內完成驗證，切勿將啟用碼洩露予他人。";
					break;
				case "QUICK":
					message = "【臺灣企銀】您的台灣Pay快速交易設定作業之驗證碼為" + OTP + "，請於2分鐘內完成驗證，切勿將驗證碼洩露予他人。";
					break;
				case "OUTBD":
					message = "【臺灣企銀】您的跨境線上購物使用者身分確認程序之驗證碼為" + OTP + "，請於2分鐘內完成驗證，切勿將驗證碼洩露予他人。";
					break;
				case "N822":
					message = "【臺灣企銀】您的信用卡額度調升申請作業之驗證碼為" + OTP + "，請於2分鐘內完成驗證，切勿將驗證碼洩露予他人。";
					break;
				case "N855":
					message = "【臺灣企銀】您的全國性繳費帳戶約定作業之驗證碼為" + OTP + "，請於2分鐘內完成驗證，切勿將驗證碼洩露予他人。";
					break;
				case "PNTF":
					message = "【臺灣企銀】您的「手機門號收款帳號設定」作業之驗證碼為" + OTP + "，請於2分鐘內完成驗證，切勿將驗證碼洩露予他人。";
					break;
				default:
					message = "【臺灣企銀】您的重設密碼作業之驗證碼為" + OTP + "，請於2分鐘內完成驗證，切勿將驗證碼洩露予他人。";
					break;
				}
//				String message = "【臺灣企銀】您的重設密碼作業之驗證碼為" + OTP + "，請於2分鐘內完成驗證，切勿將驗證碼洩露予他人。";
				try {
					log.info(ESAPIUtil.vaildLog("SmsOtp.sendSMS message = " + message));
					sendSMS(OTP, userID, MOBTEL, message, count, adopid); // 發送OTP驗證碼至簡訊平台
					// logger.warn("SmsOtp.getSMSOTP() userID =" + userID + " OTP == " + OTP );
					log.warn(ESAPIUtil.vaildLog("SmsOtp.getSMSOTP() userID =" + userID + "  OTP == " + OTP));
					// 產生回應訊息
					result.getFlatValues().put("phone", MOBTEL);
					result.getFlatValues().put("MSGCOD", "0000");
					result.getFlatValues().put("MSGSTR", "OTP密碼取得成功");
					// logger.warn("===SmsOtp getSMSOTP success===");
					log.warn("===SmsOtp getSMSOTP success===");
				} catch (Exception e) {
					// logger.warn("SmsOtp.sendSMS Exception==" + e);
					log.warn("SmsOtp.sendSMS Exception==" + e);
					result.getFlatValues().put("MSGCOD", "ZX99");
					result.getFlatValues().put("MSGSTR", "傳送簡訊失敗");
					// logger.warn("===SmsOtp getSMSOTP fail===");
					log.warn("===SmsOtp getSMSOTP fail===");
				}
			} else {
				// logger.warn("smsotp stop send , because no PhoneNumber");
				log.warn("smsotp stop send , because no PhoneNumber");
				// 產生回應訊息
				TopMessageException top1 = TopMessageException.create(phone.getValueByFieldName("MSGCOD"));
				result.getFlatValues().put("MSGCOD", top1.getMsgcode());
				result.getFlatValues().put("MSGSTR", top1.getMsgout());
				// logger.warn("===SmsOtp getSMSOTP fail===");
				log.warn("===SmsOtp getSMSOTP fail===");
			}
		} catch (TopMessageException top) {
			TopMessageException top1 = TopMessageException.create(top.getMsgcode());

			result.getFlatValues().put("MSGCOD", top1.getMsgcode());
			result.getFlatValues().put("MSGSTR", top1.getMsgout());
			// logger.warn("SmsOtp.getSMSOTP() TopMessageException==" + top);
			// logger.warn("SmsOtp.getSMSOTP() Msgcode==" + top1.getMsgcode());
			// logger.warn("SmsOtp.getSMSOTP() Msgout==" + top1.getMsgout());
			log.warn("SmsOtp.getSMSOTP() TopMessageException==" + top);
			log.warn(ESAPIUtil.vaildLog("SmsOtp.getSMSOTP() Msgcode==" + top1.getMsgcode()));
			log.warn(ESAPIUtil.vaildLog("SmsOtp.getSMSOTP() Msgout==" + top1.getMsgout()));

		} catch (Exception e) {
			// logger.warn("SmsOtp.getSMSOTP() Exception==" + e);
			// logger.warn("SmsOtp MsgCode == ZX99 ");
			log.error("SmsOtp.getSMSOTP() Exception==" + e);
			log.warn("SmsOtp MsgCode == ZX99 ");

			result.getFlatValues().put("MSGCOD", "ZX99");
			result.getFlatValues().put("MSGSTR", "系統異常!");
		}

		// logger.warn("===SmsOtp getSMSOTP method end===");
		log.warn("===SmsOtp getSMSOTP method end===");

		return result;
	}

	// 驗證軟體式動態密碼
	// public static MVH verifySMSOTP(String userID, String otp) {
	public MVH verifySMSOTP(String userID, String otp) {

		// logger.warn("===SmsOtp verifySMSOTP method start ===");
		log.warn("===SmsOtp verifySMSOTP method start ===");

		MVHImpl result = new MVHImpl();
		int retCode = 0;
		String otpIp = null;
		String otpPort = null;
		try {

			// logger.warn("=============上行電文============");
			log.warn("=============上行電文============");

			otpIp = smsotp.getOTPIP();
			otpPort = smsotp.getOTPPORT();

			// 驗證軟體式動態密碼
			// logger.warn("驗證 verifySMSOTP=====>>>> Proc00F8()==> C1="+ OTP_TYPE + "-" +
			// userID +" C2="+ otp +"C3=" + otpIp + "C4=" + otpPort);
			log.warn(ESAPIUtil.vaildLog("驗證  verifySMSOTP=====>>>> Proc00F8()==> C1=" + OTP_TYPE + "-" + userID + " C2="
					+ otp + "C3=" + otpIp + "C4=" + otpPort));

			retCode = new Proc00F8().response2(OTP_TYPE + "-" + userID, otp, otpIp, otpPort, "RSPWD", "NNB");
			// logger.warn("SmsOtp verifySMSOTP retCode=========>"+retCode);
			log.warn("SmsOtp verifySMSOTP retCode=========>" + retCode);
			if (retCode != 1) {
				result.getFlatValues().put("MSGCOD", Integer.toString(retCode));
				result.getFlatValues().put("MSGSTR", "OTP密碼驗證失敗");
				return result;
				// throw TopMessageException.create(Integer.toString(retCode));
			}

			// 產生回應訊息
			result.getFlatValues().put("MSGCOD", "0000");
			result.getFlatValues().put("MSGSTR", "OTP密碼驗證成功");
			// logger.warn("===SmsOtp verifySMSOTP success===");
			log.warn("===SmsOtp verifySMSOTP success===");

		} catch (TopMessageException top) {
			TopMessageException top1 = TopMessageException.create(top.getMsgcode());

			result.getFlatValues().put("MSGCOD", top1.getMsgcode());
			result.getFlatValues().put("MSGSTR", top1.getMsgout());
			// logger.warn("SmsOtp() TopMessageException==" + top);
			// logger.warn("SmsOtp.verifySMSOTP() Msgcode==" + top1.getMsgcode());
			// logger.warn("SmsOtp.verifySMSOTP() Msgout==" + top1.getMsgout());
			log.warn("SmsOtp() TopMessageException==" + top);
			log.warn(ESAPIUtil.vaildLog("SmsOtp.verifySMSOTP() Msgcode==" + top1.getMsgcode()));
			log.warn(ESAPIUtil.vaildLog("SmsOtp.verifySMSOTP() Msgout==" + top1.getMsgout()));

		} catch (Exception e) {
			// logger.warn("SmsOtp.verifySMSOTP() Exception==" + e);
			// logger.warn("SmsOtp MsgCode == ZX99 ");
			log.warn("SmsOtp.verifySMSOTP() Exception==" + e);
			log.warn("SmsOtp MsgCode == ZX99 ");

			result.getFlatValues().put("MSGCOD", "ZX99");
			result.getFlatValues().put("MSGSTR", "系統異常!");
		}

		// logger.warn("===SmsOtp verifySMSOTP method end===");
		log.warn("===SmsOtp verifySMSOTP method end===");

		return result;
	}

	// 傳送簡訊
	// public static void sendSMS(String _otp,String _id,String _phone, String
	// _message, int _count,String adopid) {
	public void sendSMS(String _otp, String _id, String _phone, String _message, int _count, String adopid) {

		// logger.warn("===SmsOtp sendSMS method start ===");
		log.warn("===SmsOtp sendSMS method start ===");
		log.debug("Sms Url: " + GetConfigProperty.getValue("url"));
//		log.debug("Sms username: " + GetConfigProperty.getValue("username"));
//		log.debug("Sms userpw: " + GetConfigProperty.getValue("userpw"));
        
		try {
			MitakeSmsSendResult result = MitakeSms.send(_phone, _message);
			// logger.warn("Send SMS OTP to " + _phone + ", result: " + result.toString());
			// logger.warn("getAccountPoint===>" + result.getAccountPoint());
			log.warn(ESAPIUtil.vaildLog("Send SMS OTP to " + _phone + ", result: " + result.toString()));
			log.warn(ESAPIUtil.vaildLog("getAccountPoint===>" + result.getAccountPoint()));
			Date d = new Date();

			SendResult sendResult = result.getSendResult();
			if (sendResult != null) {

				String phoneNumber = sendResult.phoneNumber;
				String messageId = sendResult.messageId;
				String statusCode = sendResult.statusCode.getCode();

				if (messageId != null && (statusCode.equals("1") || statusCode.equals("2") || statusCode.equals("3"))) {
					// SMS發送成功，存Log
					// 缺存簡訊
					// logger.warn("Send SMS OTP to " + _phone + " successfully");
					log.warn(ESAPIUtil.vaildLog("Send SMS OTP to " + _phone + " successfully"));

				} else {
					// SMS發送失敗，存Log
					// logger.warn("Send SMS OTP to " + _phone + ", statusCode: " +
					// sendResult.statusCode.getCode());
					log.warn(ESAPIUtil.vaildLog(
							"Send SMS OTP to " + _phone + ", statusCode: " + sendResult.statusCode.getCode()));
					// 缺存簡訊
				}
				TXNSMSLOG tt = new TXNSMSLOG();

				tt.setADUSERID(_id);
				tt.setPHONE(_phone);
				tt.setSTATUSCODE(statusCode);
				tt.setMSGID(messageId);
				tt.setLASTDATE(DateTimeUtils.format("yyyyMMdd", d));
				tt.setLASTTIME(DateTimeUtils.format("HHmmss", d));
				tt.setADOPID(adopid);
				txnSmsLogDao.insert(tt);

			}
		} catch (Throwable e) {
			// logger.warn("sendSMS Exception:"+e.getMessage());
			// logger.warn("===SmsOtp sendSMS method fail ===");
			log.error("sendSMS Exception:" + e.getMessage());
			log.error("===SmsOtp sendSMS method fail ===");
		}
	}

	// public static MVH SoftAction(Map params) {
	public MVH doAction(Map params) {

		// logger.warn("===SmsOtp SoftAction service request data action method start
		// ===");
		// logger.warn("SmsOtp getFuncType = " + params.get("FuncType").toString());
		log.warn("===SmsOtp SoftAction service request data action method start ===");
		log.warn(ESAPIUtil.vaildLog("SmsOtp getFuncType  = " + params.get("FuncType").toString()));

		MVHImpl result = new MVHImpl();

		// 缺欄位合理性檢核
		String _FuncType = params.get("FuncType") == null ? "" : params.get("FuncType").toString(); // 0:發送 1:驗證
		String _OTP = params.get("OTP") == null ? "" : params.get("OTP").toString(); // SMSOTP VALUE
		String msgType = StrUtils.isEmpty((String)params.get("MSGTYPE")) ? "" : params.get("MSGTYPE").toString();
		String CUSIDN = "";
		String ADOPID = "";
		int findcount = 0;
		try {
			CUSIDN = (String) params.get("UID");
			//取得簡訊密碼時，不寫TXNLOG所以改SMSADOPID
			ADOPID = ((String) params.get("ADOPID")) == null || ((String) params.get("ADOPID")).equals("") ? (String) params.get("SMSADOPID") : (String) params.get("ADOPID"); // 依交易代號計算次數限制
			// logger.warn("CUSIDN = " + CUSIDN);
			log.warn(ESAPIUtil.vaildLog("CUSIDN = " + CUSIDN));

			// 目前簡訊發送成功次數
			Date d = new Date();
			// 使用者名稱及簽入密碼重設-->簡訊發送次數計算
			// logger.warn("ADOPID==>"+ADOPID);
			log.warn(ESAPIUtil.vaildLog("ADOPID==>" + ADOPID));
//			if (ADOPID != null && ADOPID.equals("NA72")) {
//				findcount = txnSmsLogDao.findNA72TotalCount(CUSIDN, DateTimeUtils.format("yyyyMMdd", d));
//				// logger.warn("NA72 findcount = " +findcount );
//				log.warn("NA72 findcount = " + findcount);
//			}
//			// 交易密碼重設-->簡訊發送次數計算
//			if (ADOPID != null && ADOPID.equals("NA721")) {
//				findcount = txnSmsLogDao.findNA721TotalCount(CUSIDN, DateTimeUtils.format("yyyyMMdd", d));
//				// logger.warn("NA721 findcount = " +findcount );
//				log.warn("NA721 findcount = " + findcount);
//			}
//			20200720 by hugo 次數查詢改用此API
			if (StrUtils.isNotEmpty(ADOPID)) {
				findcount = txnSmsLogDao.findTotalCountByInput(CUSIDN, DateTimeUtils.format("yyyyMMdd", d),ADOPID);
				log.warn("{} findcount = {}" ,ESAPIUtil.vaildLog(ADOPID) ,findcount);
			}
			log.warn(ESAPIUtil.vaildLog("ADOPID:" + ADOPID + " COUNTER:" + findcount));

			// 0:發送+發簡訊
			if ("0".equals(_FuncType)) {
				if (findcount >= 3) {
					// logger.warn("txnsmslog count>=3 throw XQ03");
					log.warn("txnsmslog count>=3 throw XQ03");
					throw TopMessageException.create("XQ03");
				} else {
					// logger.warn("getSMSOTP touch");
					log.warn("getSMSOTP touch");
					result = (MVHImpl) getSMSOTP(CUSIDN, findcount, ADOPID , msgType);
//					result = (MVHImpl) getSMSOTP(CUSIDN, findcount, ADOPID );
				}
			} else if ("1".equals(_FuncType)) {
				// logger.warn("verifySMSOTP touch");
				log.warn("verifySMSOTP touch");
				result = (MVHImpl) verifySMSOTP(CUSIDN, _OTP);
			} else if ("2".equals(_FuncType)) {// 2:只發簡訊通知客戶
				MVH cusPhone = getPhoneNum(CUSIDN);
				String MOBTEL = cusPhone.getValueByFieldName("MOBTEL");
				String message = "親愛的客戶您好：您今日的預約轉帳有異常訊息，請您至本行一般網路銀行或行動銀行之「預約交易查詢/取消查詢」確認訊息";
				log.warn("before only SMS message to customer");
				sendSMS(CUSIDN, MOBTEL, message, "ALL");
			}

		} catch (TopMessageException top) {
			result.getFlatValues().put("MSGCOD", top.getMsgcode());
			result.getFlatValues().put("MSGSTR", top.getMsgout());

			// logger.warn("SmsOtp SoftAction TopMessageException==" + top);
			// logger.warn("SmsOtp SoftAction MsgCode==" +
			// result.getValueByFieldName("MSGCOD")+ " MsgName==" +
			// result.getValueByFieldName("MSGSTR"));
			log.warn("SmsOtp SoftAction TopMessageException==" + top);
			log.warn("SmsOtp SoftAction MsgCode==" + result.getValueByFieldName("MSGCOD") + " MsgName=="
					+ result.getValueByFieldName("MSGSTR"));
		} catch (Exception e) {
			// logger.warn("SmsOtp SoftAction Exception==" + e);
			log.warn("SmsOtp SoftAction Exception==" + e);
			result.getFlatValues().put("MSGCOD", "ZX99");
			result.getFlatValues().put("MSGSTR", "系統異常");
		}

		// logger.warn("===SmsOtp SoftAction method web service handle end ===");
		log.warn("===SmsOtp SoftAction method web service handle end ===");

		return result;
	}

	// private static MVH getPhoneNum(String cusIdn) {
	private MVH getPhoneNum(String cusIdn) {
		// IMasterValuesHelper n960 = new MVHImpl();
		MVHImpl result = new MVHImpl();
		try {
			// CommonPools.n960.removeGetDataCache(cusIdn);
			// n960 = CommonPools.n960.getData(cusIdn);
			commonPools.n960.removeGetDataCache(cusIdn);
			MVH n960Result = commonPools.n960.getData(cusIdn);
			result.getFlatValues().put("MOBTEL", n960Result.getValueByFieldName("MOBTEL"));
			result.getFlatValues().put("MSGCOD", "0000");
			result.getFlatValues().put("MSGSTR", "取得行動電話號碼成功");
		} catch (TopMessageException e) {
			// logger.warn("getPhoneNum Exception MSGCOD:"+e.getMsgcode()+ "
			// MSGSTR:"+e.getMsgout());
			log.warn("getPhoneNum Exception MSGCOD:" + e.getMsgcode() + " MSGSTR:" + e.getMsgout());
			result.getFlatValues().put("MOBTEL", "");
			result.getFlatValues().put("MSGCOD", e.getMsgcode());
			result.getFlatValues().put("MSGSTR", e.getMsgout());
		}
		// logger.warn("=== MOBTEL ===" + result.getValueByFieldName("MOBTEL"));
		log.warn("=== MOBTEL ===" + result.getValueByFieldName("MOBTEL"));

		return result;
	}

	// 預約轉帳需通知客戶時只使用SMS簡訊通知
	public void sendSMS(String _id, String _phone, String _message, String adopid) {

		log.warn("===SmsOtp sendSMS method start ===");
		try {
			MitakeSmsSendResult result = MitakeSms.send(_phone, _message);
			log.warn(ESAPIUtil.vaildLog("Send SMS OTP to " + _phone + ", result: " + result.toString()));
			log.warn(ESAPIUtil.vaildLog("getAccountPoint===>" + result.getAccountPoint()));// 簡訊次數還剩多少次可發送，如該值為0，請到簡訊平台將帳號H15test重設次數
			Date d = new Date();

			SendResult sendResult = result.getSendResult();
			if (sendResult != null) {

				String phoneNumber = sendResult.phoneNumber;
				String messageId = sendResult.messageId;
				String statusCode = sendResult.statusCode.getCode();// 簡訊平台回覆錯誤代碼
				log.warn(ESAPIUtil.vaildLog("Send SMS statusCode:" + statusCode));
				// statusCode 0：預約傳送中 1:已送達業者 2:已送達業者 3:已送達業者 4:已送達手機 5:內容有錯誤 6:門號有錯誤 7:簡訊已停用
				// 8:逾時無送達 9:預約已取消
				if (messageId != null && (statusCode.equals("1") || statusCode.equals("2") || statusCode.equals("3"))) {
					// SMS發送成功，存Log
					// 缺存簡訊
					log.warn(ESAPIUtil.vaildLog("Send SMS OTP to " + _phone + " successfully"));

				} else {
					// SMS發送失敗，存Log
					log.warn(ESAPIUtil.vaildLog("Send SMS OTP to " + _phone + ", statusCode: " + sendResult.statusCode.getCode()));
					// 缺存簡訊
				}
				TXNSMSLOG tt = new TXNSMSLOG();

				tt.setADUSERID(_id);
				tt.setPHONE(_phone);
				tt.setSTATUSCODE(statusCode);
				tt.setMSGID(messageId);
				tt.setLASTDATE(DateTimeUtils.format("yyyyMMdd", d));
				tt.setLASTTIME(DateTimeUtils.format("HHmmss", d));
				tt.setADOPID(adopid);
				txnSmsLogDao.insert(tt);

			}
		} catch (Exception e) {
			log.warn("sendSMS Exception:" + e.getMessage());
			log.warn("===SmsOtp sendSMS method fail ===");
		}
	}

}
