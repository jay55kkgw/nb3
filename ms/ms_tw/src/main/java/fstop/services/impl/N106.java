package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class N106 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("n1060Telcomm")
	private TelCommExec n1060Telcomm;

	@Autowired
    @Qualifier("n1061Telcomm")
	private TelCommExec n1061Telcomm;
	
//	@Required
//	public void setN1060Telcomm(TelCommExec telcomm) {
//		n1060Telcomm = telcomm;
//	}
//	
//	@Required
//	public void setN1061Telcomm(TelCommExec telcomm) {
//		n1061Telcomm = telcomm;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 
		TelcommResult result = null;
		log.debug("aaaaaaaaaaaaaaaaaaaa106aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
		String txntype = (String)params.get("TXNTYPE");
		if(txntype.equals("00")) {
			if("7".equals(params.get("FGTXWAY"))) {  //idgate
				params.put("CERTACN", StrUtils.repeat("C", 19));	//即時
			}
			result = n1060Telcomm.query(params);
		}
		else
			result = n1061Telcomm.query(params);
		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));

		return result;
	}


}
