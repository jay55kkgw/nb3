package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N015 extends CommonService {

	@Autowired
	@Qualifier("n015Telcomm")
	private TelCommExec n015Telcomm;

	
	public MVH doAction(Map params) {
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);

		params.put("SEQ", "1");

		TelcommResult helper = n015Telcomm.query(params);
		helper.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		return helper;

	}
}
