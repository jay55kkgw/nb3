package fstop.services.impl;

import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import fstop.model.MVH;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

@Slf4j
public class N912 extends CommonService {

	@Autowired
    @Qualifier("n912Telcomm")
	private TelCommExec n912Telcomm;
	
	@Override	
	public MVH doAction(Map params) {
		log.trace(ESAPIUtil.vaildLog("N912.doAction.params: {}"+CodeUtil.toJson(params)));
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);
		return n912Telcomm.query(params);
	}
}
