package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.cmd.SoftCertContext;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GetCN  extends CommonService {

	public MVH doAction(Map params) {
		MVHImpl result = new MVHImpl();
		SoftCertContext scc = new SoftCertContext();
		result.getFlatValues().put("CN", scc.getCN(params));
		result.getFlatValues().put("TOPMSG", "0000");
		return result;
	}
}
