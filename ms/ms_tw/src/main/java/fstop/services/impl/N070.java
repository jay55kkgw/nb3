package fstop.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import encryptUtil.EncryptUtil;
import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.orm.dao.SysDailySeqDao;
import fstop.orm.dao.TxnReqInfoDao;
import fstop.orm.dao.TxnTwRecordDao;
import fstop.orm.dao.TxnTwSchPayDao;
import fstop.orm.dao.TxnTwSchPayDataDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.ADMMAILLOG;
import fstop.orm.po.TXNTWRECORD;
import fstop.orm.po.TXNTWSCHPAY;
import fstop.services.BookingAware;
import fstop.services.CommonService;
import fstop.services.NoticeInfo;
import fstop.services.NoticeInfoGetter;
import fstop.services.OnLineTransferNotice;
import fstop.services.ServiceBindingException;
import fstop.services.TelCommExecWrapper;
import fstop.services.TransferNotice;
import fstop.services.WriteLogInterface;
import fstop.telcomm.TelCommExec;
import fstop.telcomm.cmd.CommandUtils;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import topibs.utility.NumericUtil;

@Slf4j
public class N070 extends CommonService implements BookingAware, WriteLogInterface {

	@Autowired
	private TxnTwSchPayDao txntwschpaydao;
	
	@Autowired
	private TxnTwSchPayDataDao txntwschpaydatadao;

	@Autowired
	private TxnTwRecordDao txnTwRecordDao;

	@Autowired
	@Qualifier("n070Telcomm")
	private TelCommExec n070Telcomm;

	@Autowired
	@Qualifier("n070Telcomm_m")
	private TelCommExec n070Telcomm_m;

	@Autowired
	@Qualifier("n071Telcomm")
	private TelCommExec n071Telcomm;

	@Autowired
	@Qualifier("n971Telcomm")
	private TelCommExec n971Telcomm;

	@Autowired
	private SysDailySeqDao sysDailySeqDao;

	@Autowired
	private TxnReqInfoDao txnReqInfoDao;

	// @Autowired
	// private AdmMsgCodeDao admMsgCodeDao;

	@Autowired
	private TxnUserDao txnUserDao;

	// @Autowired
	// private N071WebServiceClient n071WSClient;

	public MVH doAction(Map _params) {
		Map<String, String> params = _params;
		Assert.hasText(params.get("TYPE"), "交易 " + params.get("TXID") + " TYPE 不可為空 .");

		String fgsvacno = params.get("FGSVACNO"); // 1 約定帳號, 2 非約定帳號
		log.debug(ESAPIUtil.vaildLog("fgsvacno>>>{}"+ fgsvacno));

		// 轉入帳號
		String trin_bnkcod = "";
		String trin_acn = "";
		String trin_agree = "";

		if ("1".equals(fgsvacno)) { // 約定或常用非約定帳號
			Map<String, String> jm = JSONUtils.json2map(params.get("DPAGACNO"));
			trin_bnkcod = jm.get("BNKCOD");
			trin_acn = jm.get("ACN");
			trin_agree = jm.get("AGREE");
		} else if ("2".equals(fgsvacno) || "3".equals(fgsvacno)) { // 非約定
			trin_bnkcod = params.get("DPBHNO");
			trin_acn = params.get("DPACNO");
			trin_agree = "0"; // 0: 非約定, 1: 約定
		}

		params.put("__TRIN_BANKCOD", trin_bnkcod);
		params.put("__TRIN_ORG_ACN", trin_acn);
		params.put("__TRIN_ACN", trin_acn);
		params.put("__TRIN_AGREE", trin_agree);

		/*
		 * //BHO 轉入帳號檢核 String str_UserInputAcno = (String)params.get("InAcno_1") +
		 * (String)params.get("InAcno_2") + (String)params.get("InAcno_3"); String
		 * str_RandomValueAcno = (String)params.get("BHO_INACNO_VERIFY");
		 * BHOCheckUtils.checkBHOInAcno(str_UserInputAcno, str_RandomValueAcno);
		 */

		// 晶片金融卡
		if ("2".equals(params.get("FGTXWAY").toString())) {
			Date d = new Date();
			String trin_transeq = "1234";
			String trin_issuer = "05000000";
			String trin_acnno = params.get("ACNNO");
			String trin_icdttm = DateTimeUtils.format("yyyyMMddHHmmss", d);
			log.debug("ICDTTM >>{} ", trin_icdttm);
			String trin_iseqno = params.get("iSeqNo");
			String trin_icmemo = "";
			String trin_tac_length = "000A";
			String trin_tac = params.get("TAC");
			log.debug(ESAPIUtil.vaildLog("TAC>>{}"+trin_tac_length + trin_tac + "(" + (trin_tac_length.length() + trin_tac.length()) + ")"));
			String trin_trmid = params.get("TRMID");

			params.put("TRANSEQ", trin_transeq);
			params.put("ISSUER", trin_issuer);
			params.put("ACNNO", trin_acnno);
			params.put("ICDTTM", trin_icdttm);
			params.put("ICSEQ", trin_iseqno);
			params.put("ICMEMO", trin_icmemo);
			params.put("TAC_Length", trin_tac_length);
			params.put("TAC", trin_tac);
			params.put("TRMID", trin_trmid);
		}

		String f = params.get("FGTXDATE");

		if ("1".equals(f)) { // 即時

			OnLineTransferNotice trNotice = new OnLineTransferNotice();

			return this.online(params, trNotice);
		} else if ("2".equals(f) || "3".equals(f)) { // 預約,"2" 特定日, "3" 預約 固定每月的 某一天

			if ("0".equals(params.get("FGTXWAY"))) { // 使用交易密碼, 做預約
				String TransPassUpdate = (String) params.get("TRANSPASSUPDATE");
				String Phase2 = (String) params.get("PHASE2");
				TelCommExec n950CMTelcomm;

				n950CMTelcomm = (TelCommExec) SpringBeanFactory.getBean("n951CMTelcomm");

				// if(StrUtils.isNotEmpty(TransPassUpdate))
				// {
				// if(TransPassUpdate.equals("1") || Phase2.equals("ON"))
				// {
				// n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n951CMTelcomm");
				// }
				// else
				// {
				// n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n950CMTelcomm");
				// }
				// }
				// else
				// {
				// n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n950CMTelcomm");
				// }
				MVHImpl n950Result = n950CMTelcomm.query(params);
				// 若驗證失敗, 則會發動 TopMessageException
			} else if ("2".equals(params.get("FGTXWAY"))) { // 使用晶片金融卡, 做預約

				TelCommExec n954Telcomm = (TelCommExec) SpringBeanFactory.getBean("n954Telcomm");
				MVHImpl n954Result = n954Telcomm.query(params);
				// 若驗證失敗, 則會發動 TopMessageException
			}

			return this.booking(params);
		} else {
			throw new ServiceBindingException("錯誤的 FGTXDATE TYPE.");
		}

	}

	/**
	 * 預約交易
	 * 
	 * @param params
	 * @return
	 */
	@Transactional
	public MVHImpl booking(Map<String, String> params) {
		log.trace(ESAPIUtil.vaildLog("bookingparams>>>{}"+ params));
		TXNTWSCHPAY txntwschpay = new TXNTWSCHPAY();

		// 產生 CERTACN update by Blair -------------------------------------------- Start
		String TxWay = params.get("FGTXWAY");
		String certACN = "";
		String sha1_Mac = "";
		String reqinfo_Str = "{}";
		if ("0".equals(TxWay)) {
			while (certACN.length() < 19)
				certACN += "2";
		} else if ("1".equals(TxWay)) {
			while (certACN.length() < 19)
				certACN += "3";
		} else if ("2".equals(TxWay)) {
			while (certACN.length() < 19)
				certACN += "5";
		} else if ("3".equals(TxWay)) {
			while (certACN.length() < 19)
				certACN += "7";
		} else if ("7".equals(TxWay)) {
			while (certACN.length() < 19)
				certACN += "D";
		}
		
		// 操作功能ID
		txntwschpay.setADOPID(params.get("ADOPID"));
		// 使用者ID
		txntwschpay.setDPUSERID(params.get("CUSIDN"));
		// 轉出帳號
		txntwschpay.setDPWDAC(params.get("ACN"));
		// 轉入行庫代碼
		txntwschpay.setDPSVBH(params.get("__TRIN_BANKCOD"));
		// 轉入帳號/繳費代號
		txntwschpay.setDPSVAC(params.get("__TRIN_ACN"));
		// 轉帳金額
		txntwschpay.setDPTXAMT(params.get("AMOUNT"));
		// 備註
		txntwschpay.setDPTXMEMO(params.get("CMTRMEMO"));
		// 發送Mail清單
		txntwschpay.setDPTXMAILS(params.get("CMTRMAIL"));
		// Mail備註
		txntwschpay.setDPTXMAILMEMO(params.get("CMMAILMEMO"));
		// 交易機制
		txntwschpay.setDPTXCODE(TxWay);
		// 賽入預約編號
		//String uuid = UUID.randomUUID().toString();
		// 取得預約編號(YYMMDD+三位流水號)
		String dptmpschno = txnUserDao.getDptmpschno(params.get("UID"));
		txntwschpay.setDPSCHNO(dptmpschno);
		// 預約狀態 
//		20190912 by hugo 0:成功 ，1:取消 
		txntwschpay.setDPTXSTATUS("0");
		
		// 預設NB
		txntwschpay.setLOGINTYPE(params.get("LOGINTYPE"));

		// 寫入日期
		Date d = new Date();
		txntwschpay.setDPSDATE(DateTimeUtils.format("yyyyMMdd", d));
		txntwschpay.setDPSTIME(DateTimeUtils.format("HHmmss", d));

		txntwschpay.setLASTDATE(txntwschpay.getDPSDATE());
		txntwschpay.setLASTTIME(txntwschpay.getDPSTIME());

		params.put("CERTACN", certACN);
		// 產生 CERTACN update by Blair -------------------------------------------- End

		String f = params.get("FGTXDATE");
		if ("2".equals(f)) { // 預約某一日
			params.put("CMDATE", StrUtils.trim(params.get("CMDATE")).replaceAll("/", ""));
			if (params.get("CMDATE").length() == 0) {
				throw TopMessageException.create("ZX99");
			} else {
				txntwschpay.setDPTXTYPE("S");
				txntwschpay.setDPPERMTDATE("");
				txntwschpay.setDPFDATE(params.get("CMDATE"));
				txntwschpay.setDPTDATE(params.get("CMDATE"));
			}
		} else if ("3".equals(f)) {
			params.put("CMSDATE", StrUtils.trim(params.get("CMSDATE")).replaceAll("/", ""));
			params.put("CMEDATE", StrUtils.trim(params.get("CMEDATE")).replaceAll("/", ""));
			if (params.get("CMDD").length() == 0 || params.get("CMSDATE").length() == 0
					|| params.get("CMEDATE").length() == 0) {
				throw TopMessageException.create("ZX99");
			} else {
				txntwschpay.setDPTXTYPE("C");
				txntwschpay.setDPPERMTDATE(params.get("CMDD"));
				txntwschpay.setDPFDATE(params.get("CMSDATE"));
				txntwschpay.setDPTDATE(params.get("CMEDATE"));
			}
		}

		// params.put("ADREQTYPE", "S"); //寫入 TXNLOG 時, S 表示為預約

		// Map<String, String> mapping = new HashMap();
		//
		// mapping.put("DPUSERID", "UID");
		// mapping.put("DPWDAC", "ACN"); // 轉出帳號
		// mapping.put("DPSVBH", "__TRIN_BANKCOD"); // 轉入分行
		// mapping.put("DPSVAC", "__TRIN_ACN"); // 轉入帳號, 使用原始輸入的來記錄
		// mapping.put("DPTXAMT", "AMOUNT");
		// mapping.put("DPTXMEMO", "CMTRMEMO");
		// mapping.put("DPTXMAILS", "CMTRMAIL");
		// mapping.put("DPTXMAILMEMO", "CMMAILMEMO"); // CMMAILMEMO 對應的欄位
		//
		// mapping.put("ADOPID", "ADOPID");
		//
		// mapping.put("DPTXCODE","FGTXWAY");
		//
		// for(String key : mapping.keySet()) {
		// String v = mapping.get(key);
		// try {
		// BeanUtils.setProperty(txntwschpay, key, params.get(v));
		// } catch (IllegalAccessException e) {
		// throw new ToRuntimeException("", e);
		// } catch (InvocationTargetException e) {
		// throw new ToRuntimeException("", e);
		// }
		// }

		if (params.containsKey("pkcs7Sign") && "1".equals(params.get("FGTXWAY"))) {
			List<String> commands = new ArrayList();
			commands.add("XMLCA()");
			commands.add("XMLCN()");

			CommandUtils.doBefore(commands, params);

			if (!StrUtils.trim(params.get("XMLCN")).toUpperCase()
					.startsWith(StrUtils.trim(params.get("UID")).toUpperCase())) {
				throw TopMessageException.create("Z089");
			}

			txntwschpay.setXMLCA(StrUtils.trim(params.get("XMLCA")));
			txntwschpay.setXMLCN(StrUtils.trim(params.get("XMLCN")));
		}

		// TODO MAC&預約上行先拿掉先塞空值，日後會再調整
		// TXNREQINFO titainfo = new TXNREQINFO();
		// titainfo.setREQINFO(BookingUtils.requestInfo(params));
		// txnReqInfoDao.save(titainfo);

		// txntwschpay.setDPTXINFO( titainfo.getREQID());
//		 String sha1 = DigestUtils.sha1(titainfo.getREQINFO()).substring(0, 4);
//		txntwschpay.setMAC(sha1);
		
//		因IKEY資料太長 先移除避免塞爆欄位
		params.remove("pkcs7Sign");
		params.remove("__SIGN");
		// 預約時上行電文必要資訊 
		log.trace(ESAPIUtil.vaildLog("txntwschpay.params>>>{}"+ params));
		reqinfo_Str =  CodeUtil.toJson(params);
		log.debug(ESAPIUtil.vaildLog("reqinfo_Str >> {}"+reqinfo_Str));
		sha1_Mac = EncryptUtil.toEncS1(reqinfo_Str, "UTF-8").substring(0,4);
		log.debug(ESAPIUtil.vaildLog("sha1_Mac >> {}" + sha1_Mac));
//		 String sha1 = DigestUtils.sha1(titainfo.getREQINFO()).substring(0, 4);
		txntwschpay.setMAC(sha1_Mac);
		txntwschpay.setDPTXINFO(reqinfo_Str);
		txntwschpay.setMSADDR(TxnTwSchPayDao.MS_TW);
		log.trace(ESAPIUtil.vaildLog("txntwschpay>>>{}"+ txntwschpay));
		txntwschpaydao.save(txntwschpay);
		
		MVHImpl result = new MVHImpl();

		try {
			result.getFlatValues().put("CMTXTIME", DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", d));
			result.getFlatValues().put("TOPMSG", "0000");
			result.getFlatValues().put("MSGCOD", "0000");
		} catch (Exception e) {
			log.error("", e);
		}

		return result;
	}

	public MVHImpl online(final Map<String, String> params, final TransferNotice trNotice) {
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		log.info(ESAPIUtil.vaildLog("N070.online.params>>{}"+ CodeUtil.toJson(params)));
		params.put("DATE", df);
		params.put("TIME", tf);

		String f = params.get("FGTXDATE");
		if ("1".equals(f)) { // 即時

			// 產生 CERTACN update by Blair -------------------------------------------- Start
			String TxWay = params.get("FGTXWAY");
			String certACN = "";

			if ("0".equals(TxWay)) {
				while (certACN.length() < 19)
					certACN += " ";
			} else if ("1".equals(TxWay)) {
				while (certACN.length() < 19)
					certACN += "1";
			} else if ("2".equals(TxWay)) {
				while (certACN.length() < 19)
					certACN += "4";
			} else if ("3".equals(TxWay)) {
				while (certACN.length() < 19)
					certACN += "6";
			} else if ("7".equals(TxWay)) {
				while (certACN.length() < 19)
					certACN += "C";
			}

			params.put("CERTACN", certACN);
			// 產生 CERTACN update by Blair -------------------------------------------- End
		}else {
			
		}

		// 當轉入帳號為本行帳號時，送收N070電文
		// 當轉入帳號為他行帳號時，應產生跨行序號當作上行電文欄位。
		// 當轉入帳號為他行帳號時，送收N071電文，
		// 須判斷下行的RSPCOD=”E901”時表示跨行交易尚未回傳資料，
		// 應繼續發送N071電文詢問，當RSPCOD不等於”E901”時發送N971電文

		// Default 為本行轉帳
		TelCommExec doexec = n070Telcomm;

		if (StrUtils.trim((String) params.get("LOGINTYPE")).equals("MB")
				&& "050".equals(params.get("__TRIN_BANKCOD"))) {
			doexec = n070Telcomm_m;
			log.info("n070Telcomm_m  .........");
		}
		// 當轉入帳號為他行帳號時，送收N071電文
		if (!"050".equals(params.get("__TRIN_BANKCOD"))) {
			// TODO EAI暫時無法使用先註解
			doexec = n071Telcomm;
			log.info("n071Telcomm  .........");
		}
		log.debug(ESAPIUtil.vaildLog("Bank Code ----------> " + params.get("__TRIN_BANKCOD")));
		TelCommExecWrapper execwrapper = null;
		if (doexec == n070Telcomm || doexec == n070Telcomm_m) {
			
			params.put("PCSEQ", StrUtils.right("00000" + sysDailySeqDao.dailySeq("N070") + "", 5));
			// String TransOutACN = params.get("TransferType") == "PD" ? params.get("ACN") :
			// params.get("ACNNO");
			String TransOutACN = params.get("ACN");
			String TransType = params.get("TransferType") == "PD" ? "0" : "1";

			if (TransOutACN.length() > 11) {
				TransOutACN = TransOutACN.substring(TransOutACN.length() - 11);
			}
			if ("Y".equals(params.get("isPhone"))) {
				params.put("TYPE", "04"); // 01:跨行轉帳 02:跨行繳稅 03:跨行繳費 04:手機門號轉
			}
			log.debug(ESAPIUtil.vaildLog("Curren ACN >>{}"+ TransOutACN));
			// log.info("n070Telcomm .........");
			params.put("ACN", TransOutACN); // 轉出帳號
			params.put("OUTACN", TransOutACN); // 轉出帳號
			log.debug(ESAPIUtil.vaildLog("Curren OUTACN >> {}"+ TransOutACN));
			params.put("TSFACN", params.get("__TRIN_ACN")); // 轉入帳號
			params.put("FLAG", params.get("__TRIN_AGREE"));
			// params.put("FLAG", TransType);

			execwrapper = new TelCommExecWrapper(doexec);

			params.put("__LOGTYPE", "N070");
		} else { // n071
					// 重發時, PCSEQ 需帶入上一次的
			params.put("PCSEQ", StrUtils.right("00000" + sysDailySeqDao.dailySeq("N070") + "", 5));

			params.put("TRNFLAG", "1");

			// 要看常用帳號裡的設定加上 N921 裡的, 下拉選單帶上來的值來判斷是否為 約定帳號
			// DB 裡的就不是
			params.put("FLAG", params.get("__TRIN_AGREE"));

			params.put("BNKRA", "050"); // 行庫別
			params.put("OUTACN", params.get("ACN"));
			params.put("INTSACN", params.get("__TRIN_ACN"));
			params.put("BANKIND", params.get("__TRIN_BANKCOD"));
			// params.put("AMOUNT", params.get("AMOUNT"));
			params.put("TRNDAT", StrUtils.right(DateTimeUtils.getCDateShort(new Date()), 6));
			params.put("TRNTIM", DateTimeUtils.getTimeShort(new Date()));
			if ("Y".equals(params.get("isPhone"))) {
				params.put("TXNTYPE", "04"); // 01:跨行轉帳 02:跨行繳稅 03:跨行繳費 04:手機門號轉
			}else {
				params.put("TXNTYPE", "01"); // 01:跨行轉帳 02:跨行繳稅 03:跨行繳費 04:手機門號轉
			}
			List<String> commands = new ArrayList();
			// commands.add("SYNC()");
			// commands.add("PPSYNC()");
			commands.add("SYNC1()");
			commands.add("PPSYNCN1()");
			commands.add("PINNEW1()");
			commands.add("MAC2({%11s,OUTACN} {%-16s, INTSACN} {%010d,AMOUNT} {%19s, CERTACN})");
			if ("1".equals(params.get("FGTXWAY"))) {
				commands.add("XMLCA()");
				commands.add("XMLCN()");
			}
			CommandUtils.doBefore(commands, params);
			log.debug("fisnsh doBefore...");
			if ("1".equals(params.get("FGTXWAY")) && !StrUtils.trim(params.get("XMLCN")).toUpperCase()
					.startsWith(StrUtils.trim(params.get("UID")).toUpperCase())) {
				throw TopMessageException.create("Z089");
			}
			if ("2".equals(params.get("FGTXWAY"))) {
//				params.put("TAC", params.get("TAC_Length") + params.get("TAC"));
				log.warn(ESAPIUtil.vaildLog("N071.java TAC:" + params.get("TAC")));
			}
			// eai 改 tmra
			execwrapper = new TelCommExecWrapper(doexec);
			params.put("__LOGTYPE", "N071");
		}
		
		
		
		MVHImpl result = null;
		try {
			//		建立防止2扣序號 __TRANS_STATUS表示預約轉及時由預約批次自行帶入
			if(StrUtils.isEmpty(params.get("__TRANS_STATUS")) ) {
				params.put("SEQTRN", getSeqTrn()) ;
			}
			log.info(ESAPIUtil.vaildLog("N070.query.params>>{}"+ CodeUtil.toJson(params)));
			result = execwrapper.query(params);
		} catch (TopMessageException e) {
			if ("E033".equals(e.getMsgcode())) {
				params.put("ADMSGCODE", e.getMsgcode());
				params.put("ADMESSAGE", e.getMessage());
			} else if ("E297".equals(e.getMsgcode())) {
				params.put("ADMSGCODE", e.getMsgcode());
				params.put("ADMESSAGE", e.getMessage());
			}else if("EUMP".equalsIgnoreCase(e.getMsgcode())) {
				Map totaMap = CodeUtil.fromJson(e.getMsgout(),Map.class);
				for( Object mapKey:totaMap.keySet()) {
					result.getFlatValues().put((String)mapKey,(String)(totaMap.get(mapKey)));
				}
			}
		}

		finally {

			final TXNTWRECORD record = (TXNTWRECORD) this.writeLog(execwrapper, params, result);
			
			final TelCommExecWrapper execwrapperFinal = execwrapper;
			
			try {
				trNotice.sendNotice(new NoticeInfoGetter() {

					public NoticeInfo getNoticeInfo() {
						NoticeInfo result = new NoticeInfo(params.get("UID"), params.get("DPMYEMAIL"),
								params.get("CMTRMAIL"));
						// 20190510 edit by hugo 表格異動 欄位已經移除
						// result.setDpretxstatus(record.getDPRETXSTATUS());
						// log.debug("Dpretxstatus = " + record.getDPRETXSTATUS());
						result.setADTXNO(record.getADTXNO());
						 
						Throwable e_root =execwrapperFinal.getLastException();
						if(e_root instanceof TopMessageException) {
							if("EUMP".equalsIgnoreCase(e_root.getMessage())){
								result.setException(null);
							}else {
								result.setException(execwrapperFinal.getLastException());
							}
						}else {
							result.setException(execwrapperFinal.getLastException());
						}
						
						if (StrUtils.trim((String) params.get("LOGINTYPE")).equals("MB"))
							result.setTemplateName("transferNotify_mb");
						else
							result.setTemplateName("transferNotify");

						String datetime = record.getDPTXDATE() + record.getDPTXTIME();
						Date d = DateTimeUtils.parse("yyyyMMddHHmmss", datetime);
						String trantime = DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", d);
						result.getParams().put("#TRANTIME", trantime);
						result.getParams().put("#SEQNO", record.getDPTXNO());

						String acn = record.getDPSVAC();
						try {
							acn = StrUtils.left(acn, acn.length() - 5) + "***" + StrUtils.right(acn, 2);
						} catch (Exception e) {
						}

						String outacn = record.getDPWDAC(); // 轉出帳號
						try {
							outacn = StrUtils.left(outacn, outacn.length() - 5) + "***" + StrUtils.right(outacn, 2);
						} catch (Exception e) {
							log.error("", e);
						}
						result.getParams().put("#ACN", acn);
						result.getParams().put("#OUTACN", outacn);
						result.getParams().put("#OUTCURRENCY", "新台幣");
						String dptxamtfmt = NumericUtil.formatNumberString(record.getDPTXAMT(), 0);
						result.getParams().put("#AMT", dptxamtfmt);
						result.getParams().put("#MEMO",
								params.get("ADMSGCODE") != null ? "請您參考網路銀行常見問題＞服務項目＞Q02：網路銀行交易的最高限額"
										: params.get("CMMAILMEMO"));
						result.getParams().put("ADMSGCODE", params.get("ADMSGCODE"));
						result.getParams().put("ADMESSAGE", params.get("ADMESSAGE"));

						ADMMAILLOG admmaillog = new ADMMAILLOG();
						admmaillog.setADACNO(record.getDPWDAC());

						String trans_status = (String) params.get("__TRANS_STATUS");
						if (StrUtils.isEmpty(trans_status)) {
							trans_status = "TWONLINE";
						} else if ("SEND".equals(trans_status)) {
							trans_status = "TWSCH";
						} else if ("RESEND".equals(trans_status)) {
							trans_status = "TWSCHRE";
						}

						admmaillog.setADBATCHNO(trans_status);
						admmaillog.setADMAILACNO(StrUtils.trim(params.get("CMTRMAIL")));
						admmaillog.setADUSERID(record.getDPUSERID());
						admmaillog.setADUSERNAME("");
						admmaillog.setADSENDTYPE("3");
						admmaillog.setADSENDTIME(datetime);
						admmaillog.setADSENDSTATUS("");

						result.setADMMAILLOG(admmaillog);

						return result;

					}

				});
			} catch (Throwable e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				log.error("N070.sendNotice.ERROR>>{}",e1);
			}



			try {
				result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
				result.getFlatValues().put("CMTXTIME", DateTimeUtils.getCDateTime(result.getValueByFieldName("DATE"),
						result.getValueByFieldName("TIME")));
			} catch (Exception e) {
				log.error("{}", e);
				
			}
			execwrapper.throwException();

			return result;
		}
	}

	
	public String getSeqTrn() {
		String seqtrn = "" , seq="";
		try {
			DateTime dt= new DateTime();
			seq = String.format("%07d", sysDailySeqDao.getSeq().intValue() );
			seqtrn = dt.toString("yyyyMMdd")+seq;
		} catch (Throwable e) {
			log.error("{}",e);
			throw new TopMessageException("FE0008", e);
		}
		return seqtrn;
	}
	
	
	
	/**
	 * 每三秒再查詢一次
	 * 
	 * @param params
	 * @return
	 */
	private TelcommResult sendN971(Map params) {
		TelcommResult result = null;
		String rspcod = "";
		long start = new Date().getTime();
		do {
			try {
				result = n971Telcomm.query(params);
			} catch (TopMessageException e) {
				if (!"E901".equals(e.getMsgcode())) {
					throw e;
				} else { // E901
					result = new TelcommResult(new Vector());
					result.getFlatValues().put("RSPCOD", "E901");
				}
			}
			rspcod = result.getValueByFieldName("RSPCOD");
			try {
				Thread.sleep(10000L);
			} catch (Exception e) {
			}

			if ((new Date().getTime() - start) > 5 * 60 * 1000L) {
				result = null;
				break;
			}
		} while ("E901".equals(rspcod));

		return result;
	}

	public Object writeLog(final TelCommExecWrapper execwrapper, final Map _params, final MVHImpl result) {

		Map<String, String> params = _params;
		
		if ("N070".equals(params.get("__LOGTYPE"))) {
			log.info("N070 INTO setAfterQuery ...");

			try {
				TXNTWRECORD record = execwrapper.createTxnTwRecord(sysDailySeqDao);
				
				log.info("createTxnTwRecord finsh...record>>{}", record);
				String date = record.getLASTDATE();
				String time = record.getLASTTIME();

				if (result != null) {
					try {
						date = DateTimeUtils.format("yyyyMMdd",
								DateTimeUtils.getCDate2Date(result.getValueByFieldName("DATE")));
					} catch (Exception e) {
						log.error("", e);
					}
					try {
						time = result.getValueByFieldName("TIME");
					} catch (Exception e) {
						log.error("", e);
					}
				} else {
					log.warn("result is null...");
				}
				log.info("record set other params...");
				// record.setDPSCHNO(params.get("__SCHNO")==null?"" : params.get("__SCHNO"));

				record.setDPUSERID(params.get("UID"));
				record.setADOPID(params.get("ADOPID"));// 操作功能ID(NB3SysOp.ADOPID)
				record.setDPTXDATE(date);// 轉帳日期
				record.setDPTXTIME(time);// 轉帳時間
				record.setDPWDAC(params.get("ACN"));// 轉出帳號
				record.setDPSVBH("050");// 轉入行庫代碼
				record.setDPSVAC(params.get("TSFACN"));// 轉入帳號/繳費代號
				record.setDPTXAMT(params.get("AMOUNT"));// 轉帳金額

				record.setDPTXMEMO(params.get("CMTRMEMO"));// 備註
				record.setDPTXMAILS(params.get("CMTRMAIL"));// 發送Mail清單
				record.setDPTXMAILMEMO(params.get("CMMAILMEMO"));// Mail備註 // + StrUtils.repeat("200", 100)); //CMMAILMEMO
																	// 對應的欄位

				record.setDPEFEE("0");// 手續費
				record.setDPTXNO(""); // 跨行序號

				record.setDPTXCODE(params.get("FGTXWAY"));// 交易機制
				record.setLOGINTYPE(params.get("LOGINTYPE"));
				//移除pkcs7Sign 避免資料過長
				params.remove("pkcs7Sign");
				params.remove("__SIGN");
				record.setDPTITAINFO(CodeUtil.toJson(params));
				// String pcseq = StrUtils.right("00000" + sysDailySeqDao.dailySeq("N070")+"",
				// 5)
//			log.debug("sysDailySeqDao>>{}", sysDailySeqDao);
//			String pcseq = String.format("%05d", sysDailySeqDao.dailySeq("N070"));
//			log.info("pcseq>>{}", pcseq);
//			String adtxno = DateUtil.getCurentDateTime("yyyyMMddHH") + pcseq;
//			log.info("adtxno>>{}", adtxno);

//			record.setPCSEQ(pcseq);
//			record.setADTXNO(adtxno);
				// 20190510 edit by hugo 改成不寫入 TXNREQINFO 只寫入TXNTWRECORD
				txnTwRecordDao.save(record);

				// TXNREQINFO titainfo = null;
				// if(record.getDPTITAINFO() == 0) {
				// titainfo = new TXNREQINFO();
				// titainfo.setREQINFO(BookingUtils.requestInfo(params));
				// }
				// txnTwRecordDao.writeTxnRecord(titainfo, record);

				return record;
			} catch (Exception e) {
				log.error("N070.writeLog.Exception>>",e);
			}
		} else if ("N071".equals(params.get("__LOGTYPE"))) {

			log.info("N071 INTO setAfterQuery ...");

			TXNTWRECORD record = execwrapper.createTxnTwRecord(sysDailySeqDao);

			String date = record.getLASTDATE();
			String time = record.getLASTTIME();

			if (result != null) {
				try {
					date = DateTimeUtils.format("yyyyMMdd",
							DateTimeUtils.getCDate2Date(result.getValueByFieldName("DATE")));
				} catch (Exception e) {
				}
				try {
					time = result.getValueByFieldName("TIME");
				} catch (Exception e) {
				}
			}
			// 20190510 edit by hugo 表格異動 欄位已經移除
			// record.setDPSCHNO(params.get("__SCHNO")==null?"" : params.get("__SCHNO"));

			record.setDPUSERID(params.get("UID"));
			record.setADOPID(params.get("ADOPID"));
			record.setDPTXDATE(date);
			record.setDPTXTIME(time);
			record.setDPWDAC(params.get("OUTACN"));
			record.setDPSVBH(params.get("BANKIND"));
			record.setDPSVAC(params.get("INTSACN"));
			record.setDPTXAMT(params.get("AMOUNT"));

			record.setDPTXMEMO(params.get("CMTRMEMO"));
			record.setDPTXMAILS(params.get("CMTRMAIL"));
			record.setDPTXMAILMEMO(params.get("CMMAILMEMO")); // CMMAILMEMO 對應的欄位

			if (result == null) {
				record.setDPEFEE("0");

				record.setDPTXNO(""); // 跨行序號
			} else {
				record.setDPEFEE(result.getValueByFieldName("FEE"));
				record.setDPTXNO(result.getValueByFieldName("STAN"));
			}
			record.setDPTXCODE(params.get("FGTXWAY"));
			record.setPCSEQ(params.get("PCSEQ")); // 交易序號
			// 20190510 edit by hugo 改成不寫入 TXNREQINFO 只寫入TXNTWRECORD
			txnTwRecordDao.save(record);

			// TXNREQINFO titainfo = null;
			// if(record.getDPTITAINFO() == 0) {
			// titainfo = new TXNREQINFO();
			// titainfo.setREQINFO(BookingUtils.requestInfo(params));
			// }
			//
			// txnTwRecordDao.writeTxnRecord(titainfo, record);

			return record;
		}

		return null;
	}

}
