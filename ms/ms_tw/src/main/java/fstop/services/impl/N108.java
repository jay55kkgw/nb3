package fstop.services.impl;

import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import topibs.utility.NumericUtil;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.TxnReqInfoDao;
import fstop.orm.dao.TxnTwRecordDao;
import fstop.orm.po.ADMMAILLOG;
import fstop.orm.po.TXNREQINFO;
import fstop.orm.po.TXNTWRECORD;
import fstop.services.BookingUtils;
import fstop.services.CommonService;
import fstop.services.NoticeInfo;
import fstop.services.NoticeInfoGetter;
import fstop.services.OnLineTransferNotice;
import fstop.services.TelCommExecWrapper;
import fstop.services.WriteLogInterface;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 台幣綜存質借功能取消
 * @author Owner
 *
 */
@Slf4j
public class N108 extends CommonService {
	private Logger logger = Logger.getLogger(getClass());
	
	@Autowired
	@Qualifier("n108Telcomm")
	private TelCommExec n108Telcomm;
	
//	@Required
//	public void setN108Telcomm(TelCommExec telcomm) {
//		n108Telcomm = telcomm;
//	}

	/**
	 * 
	 * @param params
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public MVH doAction(Map _params) {
		final Map<String, String> params = _params;
		
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);

		params.put("FLAG", "1");
		if("7".equals(params.get("FGTXWAY"))) {  //晶片金融卡
			params.put("CERTACN", StrUtils.repeat("C", 19));	//即時
		}
		
		//TODO
		log.debug("params="+params);
		
		MVHImpl result = n108Telcomm.query(params);
		
		try {
			result.getFlatValues().put("CMTXTIME", 
					DateTimeUtils.getCDateTime(result.getValueByFieldName("TRNDATE"),
											result.getValueByFieldName("TRNTIME")));
		}
		catch(Exception e){
			logger.error(e);
		}
		
		return result;
	}
}
