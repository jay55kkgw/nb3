package fstop.services.impl;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.rest.model.CommonPools;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.MVHUtils;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N860 extends CommonService  {
	private Logger logger = Logger.getLogger(getClass());
	
	@Autowired
	@Qualifier("n860Telcomm")
	private TelCommExec n860Telcomm;
	
	
	@Autowired
	CommonPools commonPools;
	/*@Required
	public void setN860Telcomm(TelCommExec telcomm) {
		n860Telcomm = telcomm;
	}*/
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params) {
		Map<String, String> params = _params;
		
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);
		
		String fg = StrUtils.trim((String)params.get("FGBRHCOD"));
		String ft = StrUtils.trim((String)params.get("FGTYPE"));
		String per = StrUtils.trim((String)params.get("FGPERIOD"));
		
		//當FGBRHCOD=”DPBHENT”時，將DPBHNO的值填入BRHCOD。
		//當FGBRHCOD=”DPBHALL”且FGTYPE=”DPCKTYPE1”時，將”02”填入TYPE。
		//當FGBRHCOD=”DPBHALL”且FGTYPE=”DPCKTYPE2”時，將”01”填入TYPE。
		//當FGBRHCOD=”DPBHALL”且FGTYPE=”DPCKTYPE3”時，將”05”填入TYPE。
		//當FGBRHCOD=”DPBHALL”且FGTYPE=”DPCKTYPE4”時，將”06”填入TYPE。
		//當FGBRHCOD=”DPBHALL”且FGTYPE=”DPCKTYPE5”時，將”07”填入TYPE。		
		if("DPBHALL".equals(fg))
			params.put("BRHCOD", "");
		
		if("DPBHENT".equals(fg))
			params.put("BRHCOD", StrUtils.trim((String)params.get("DPBHNO")));
		
		if("DPBHALL".equals(fg) && "DPCKTYPE1".equals(ft))
			params.put("TYPE", StrUtils.trim("02"));
		
		if("DPBHALL".equals(fg) && "DPCKTYPE2".equals(ft))
			params.put("TYPE", StrUtils.trim("01"));
		
		if("DPBHALL".equals(fg) && "DPCKTYPE3".equals(ft))
			params.put("TYPE", StrUtils.trim("05"));
		if("DPBHALL".equals(fg) && "DPCKTYPE4".equals(ft))
			params.put("TYPE", StrUtils.trim("06"));
		if("DPBHALL".equals(fg) && "DPCKTYPE5".equals(ft))
			params.put("TYPE", StrUtils.trim("07"));

		//  當FGBRHCOD=” DPBHENT”且FGTYPE=”DPCKTYPE1”時，將”04”填入TYPE。
		//  當FGBRHCOD=” DPBHENT”且FGTYPE=”DPCKTYPE2”時，將”03”填入TYPE。
		//  當FGBRHCOD=” DPBHENT”且FGTYPE=”DPCKTYPE3”時，將”08”填入TYPE。
		//  當FGBRHCOD=” DPBHENT”且FGTYPE=”DPCKTYPE4”時，將”09”填入TYPE。
		//  當FGBRHCOD=” DPBHENT”且FGTYPE=”DPCKTYPE5”時，將”10”填入TYPE。

		if ("DPBHENT".equals(fg) && "DPCKTYPE1".equals(ft))
			params.put("TYPE", StrUtils.trim("04"));

		if ("DPBHENT".equals(fg) && "DPCKTYPE2".equals(ft))
			params.put("TYPE", StrUtils.trim("03"));

		if ("DPBHENT".equals(fg) && "DPCKTYPE3".equals(ft))
			params.put("TYPE", StrUtils.trim("08"));
		if ("DPBHENT".equals(fg) && "DPCKTYPE4".equals(ft))
			params.put("TYPE", StrUtils.trim("09"));
		if ("DPBHENT".equals(fg) && "DPCKTYPE5".equals(ft))
			params.put("TYPE", StrUtils.trim("10"));
		
				

		//	當FGPERIOD=”DPPERIOD”時，將”01”填入ITEM，將STADATE及ENDDATE填滿空白。
		//	FGPERIOD=”DPPERIOD2”時，將”02”填入ITEM，將CMSDATE2的值填入STADATE，將CMEDATE2的值填入ENDDATE。
		//	FGPERIOD=”DPPERIOD3”時，將”03”填入ITEM，將CMSDATE3的值填入STADATE，將CMEDATE3的值填入ENDDATE。		

		String periodStr = "";
		String stadate = "";
		String enddate = "";
		
		if("DPPERIOD1".equals(per)) { 
			params.put("ITEM", "01");
			periodStr = "全部(已銷帳為二個月內)";
//			stadate = StrUtils.trim((String)params.get("CMSDATE2"));
//			enddate = StrUtils.trim((String)params.get("CMEDATE2"));	
		}
		
		if("DPPERIOD2".equals(per)) { 
			params.put("ITEM", "02");
			periodStr = "託收日：";
			stadate = StrUtils.trim((String)params.get("CMSDATE2"));
			enddate = StrUtils.trim((String)params.get("CMEDATE2"));	
		}
		
		if("DPPERIOD3".equals(per)) { 
			params.put("ITEM", "03");
			periodStr = "到期日：";
			stadate = StrUtils.trim((String)params.get("CMSDATE3"));
			enddate = StrUtils.trim((String)params.get("CMEDATE3"));			
		}
		
		if("DPPERIOD4".equals(per)) { 
			params.put("ITEM", "04");
			periodStr = "入帳日：";
			stadate = StrUtils.trim((String)params.get("CMSDATE4"));
			enddate = StrUtils.trim((String)params.get("CMEDATE4"));			
		}
		
		stadate = StrUtils.trim(stadate).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
		enddate = StrUtils.trim(enddate).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
		
		
		if(StrUtils.isNotEmpty(stadate) && StrUtils.isNotEmpty(enddate)) {
			if (stadate != null && stadate.length() == 8) {
				Date sd = DateTimeUtils.parse("yyyyMMdd", stadate);
				stadate = DateTimeUtils.getCDateShort(sd);
				periodStr += DateTimeUtils.format("yyyy/MM/dd", sd);
			}
			if (enddate != null && enddate.length() == 8) {
				Date sd = DateTimeUtils.parse("yyyyMMdd", enddate);
				enddate = DateTimeUtils.getCDateShort(sd);
				periodStr += "~" + DateTimeUtils.format("yyyy/MM/dd", sd);
			} else { // 若未指定結束時間, 則使用今天日期
				enddate = DateTimeUtils.getCDateShort(new Date());
				periodStr += "~" + DateTimeUtils.format("yyyy/MM/dd", new Date());
			}
	
			params.put("STADATE", stadate);
			params.put("ENDDATE", enddate);
		}
		
		MVHImpl result = new MVHImpl();
		
		queryAll(getAcnoList(params), params, result);
		
		log.debug(result.dumpToString());
//		
//		if(StrUtils.isEmpty((String)params.get("ACN"))) {
//			//查詢全部帳號
//			
//		}
//		else {
//			MVHImpl resultN860 = n860Telcomm.query(params);
//			resultN860.getOccurs().setValue("ACN", (String)params.get("ACN"));
//			
//			if (!result.isOccurs("COLLBRH")) {
//				result.getOccurs().setValue("COLLBRH", result.getValueByFieldName("COLLBRH"));
//			}
//			
//			result.addTable(resultN860, (String)params.get("ACN"));
//		}

		//REMCNT	PIC’9(7)’	託收票據總張數
		//REMAMT	PIC’9(13)V99’	託收票據總金額
		//VALCNT	PIC’9(7)’	已入帳總張數
		//VALAMT	PIC’9(13)V99’	已入帳總金額

		try {
			Double sum = MVHUtils.sumTablesFlatColumn(result, "REMCNT");
			DecimalFormat fmt = new DecimalFormat("#0");
			
			result.getFlatValues().put("REMCNT", fmt.format(sum));
		}
		finally{}
		try {
			Double sum = MVHUtils.sumTablesFlatColumn(result, "REMAMT");
			DecimalFormat fmt = new DecimalFormat("#0.00");
			
			result.getFlatValues().put("REMAMT", fmt.format(sum));
		}
		finally{}
		try {
			Double sum = MVHUtils.sumTablesFlatColumn(result, "VALCNT");
			DecimalFormat fmt = new DecimalFormat("#0");
			
			result.getFlatValues().put("VALCNT", fmt.format(sum));
		}
		finally{}
		try {
			Double sum = MVHUtils.sumTablesFlatColumn(result, "VALAMT");
			DecimalFormat fmt = new DecimalFormat("#0.00");
			
			result.getFlatValues().put("VALAMT", fmt.format(sum));
		}
		finally{}

		
		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		result.getFlatValues().put("CMPERIOD", periodStr);
		result.getFlatValues().put("CMRECNUM", MVHUtils.sumTablesOccurs(result) + "");
		result.getFlatValues().put("TYPE", params.get("TYPE"));

		return result;
	}
	
	
	private List<String> getAcnoList(Map<String, String> params) {
		List<String> acnos = new ArrayList();
		if(StrUtils.isEmpty((String)params.get("ACN"))) {
			
			fstop.model.IMasterValuesHelper acnosMVH = commonPools.n920.getAcnoList(params.get("CUSIDN"), new String[]{"01", "02", "03", "07"}, "");
			int requestCount = acnosMVH.getValueOccurs("ACN");
			for(int i=0; i < requestCount; i++) {
				String acn = acnosMVH.getValueByFieldName("ACN", i+1);
				acnos.add(acn);
			}
			
		}
		else {
			acnos.add(params.get("ACN"));
		}
		return acnos;
	}

	public void queryAll(List<String> acnoList, Map<String, String> params, MVHImpl resultMVH) {
 		//IMasterValuesHelper acnos = CommonPools.n920.getAcnoList(params.get("UID"), new String[]{"01", "02", "03", "07"}, "");

		List<String> currAcnoList = new ArrayList<String>();
		String doneAcnos = params.get("DONEACNOS");
		String[] doneAcnoList = null;
		if (doneAcnos == null)
			doneAcnoList = new String[0];
		else
			doneAcnoList = doneAcnos.split(",");
		
 		//int requestCount = acnos.getValueOccurs("ACN");
 		for(String acn : acnoList) {
 			
 			currAcnoList.add(acn);
 			
 			//已執行過的ACNO不重複執行
 			boolean hasDone = false;
 			for (int i = 0; i < doneAcnoList.length; i++)
 			{
 				if (doneAcnoList[i].equals(acn))
 				{
 					hasDone = true;
 					break;
 				}
 			}
 			if (hasDone)
 				continue;
 			
 			//String acn = acnos.getValueByFieldName("ACN", i+1);
 			params.put("ACN", acn);
 			try {
	 			TelcommResult result = n860Telcomm.query(params);
	 			params.remove("QUERYNEXT");	//一個QUERYNEXT只能用於一個ACNO
	 			result.getOccurs().setValue("ACN", acn);
	 			String msgcod =  result.getFlatValues().get("MSGCOD");
	 			
	 			if(!result.isOccurs("COLLBRH")) {
	 				result.getOccurs().setValue("COLLBRH", result.getValueByFieldName("COLLBRH"));
	 			}
	 			
	 			resultMVH.addTable(result, acn);
	 			
	 			/*20190327 Ben for setMsgcode
	 			resultMVH.getFlatValues().put("MSGCOD",resultMVH.getValueByFieldName("MSGCOD"));
	 			resultMVH.getFlatValues().put("ACN",resultMVH.getValueByFieldName("ACN"));
	 			*/
	 			String queryNext = result.getValueByFieldName("QUERYNEXT");
	 			if (!queryNext.equals(""))
	 			{
	 				resultMVH.getFlatValues().put("QUERYNEXT", queryNext);
	 				
	 				currAcnoList.remove(currAcnoList.size() - 1);
	 				StringBuffer sb = new StringBuffer();
	 				if (currAcnoList.size() > 0)
	 				{
		 		 		sb.append(currAcnoList.get(0));
		 		 		for (int i = 1; i < currAcnoList.size(); i++)
		 		 		{
		 		 			sb.append("," + currAcnoList.get(i));
		 		 		}
	 				}
	 		 		resultMVH.getFlatValues().put("DONEACNOS", sb.toString());
	 				
	 				break;	 				
	 			}
				// 20190410 JeffChang 為了得到回傳值
//				resultMVH.getFlatValues().putAll(result.getFlatValues());
 			}
 			catch(TopMessageException ex) {
				MVHImpl helper = new MVHImpl();
				helper.getFlatValues().put("ACN", acn);//為了得到ACN的回傳值
				helper.getFlatValues().put("TOPMSG", ex.getMsgcode());
				helper.getFlatValues().put("ADMSGIN", ex.getMsgin());
				helper.getFlatValues().put("ADMSGOUT", ex.getMsgout());
				resultMVH.addTable(helper, acn);			
 					
 			}
 		}
	}

}
