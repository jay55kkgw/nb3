package fstop.services.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.rest.model.CommonPools;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.model.Rows;
import fstop.orm.dao.TxnTwRecordDao;
import fstop.orm.po.ADMMAILLOG;
import fstop.orm.po.TXNTWRECORD;
import fstop.services.CommonService;
import fstop.services.DispatchService;
import fstop.services.NoticeInfo;
import fstop.services.NoticeInfoGetter;
import fstop.services.OnLineTransferNotice;
import fstop.services.ServiceBindingException;
import fstop.services.TelCommExecWrapper;
import fstop.services.WriteLogInterface;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import topibs.utility.NumericUtil;

/**
 * 轉入台幣零存整付定存
 * @author Owner
 *
 */
@Slf4j
public class N076 extends DispatchService implements WriteLogInterface {
	

	@Autowired
	private CommonService n420Service;
	@Autowired
	@Qualifier("n076Telcomm")
	private TelCommExec n076Telcomm;
	@Autowired
	private CommonPools commonPools;
	@Autowired
	private TxnTwRecordDao txnTwRecordDao;
	 

	@Override
	public String getActionMethod(Map<String, String> params) {
		String txid = StrUtils.trim(params.get("TXID"));

		if("N076".equals(txid))
			return "doN420";
		else if("N076_1".equals(txid))
			return "doN076";
		else 
			throw new ServiceBindingException("不支援的 TXID [" + txid+"].");
	}
	
	public MVH doN420(Map params) {
		Rows resultRows = new Rows();
		MVHImpl n921Mvh = (MVHImpl)commonPools.ui.getAgreeAcnoList(params.get("UID").toString(), new String[]{"09"}, false); 
		Rows n921Rows = n921Mvh.getOccurs();
		MVHImpl n420Mvh = (MVHImpl)n420Service.doAction(params);
		Rows n420Rows = n420Mvh.getOccurs();
		
		final Map<String, Row> inN921 = new HashMap();
		Rows notAgreeRows = new Rows();
		
		for (Row r : n921Rows.getRows())
		{
			//非本行濾掉
			if (!r.getValue("BNKCOD").equals("050"))
				continue;
			
			Map<String, String> m = JSONUtils.json2map(r.getValue("VALUE"));
			Row newRow = new Row(new HashMap());
			newRow.setValue("TEXT", r.getValue("TEXT"));
			newRow.setValue("AGREE", m.get("AGREE"));
			inN921.put(m.get("ACN"), newRow);
		}
		
		for (Row r : n420Rows.getRows())
		{
			//無存單號碼濾掉
			if (r.getValue("FDPNUM").equals("") || !r.getValue("TYPE").equals("5"))
				continue;
			
			String acn = r.getValue("ACN");
			String fdpnum = r.getValue("FDPNUM");
			String amount = r.getValue("AMTFDP");
			
			//非"75、77"科目的帳號濾掉
			if (!(acn.length() >= 5 && (acn.substring(3, 5).equals("75") || acn.substring(3, 5).equals("77"))))
				continue;
			
			if (inN921.containsKey(acn))
			{//N921結果集合中含有此筆帳號資料
				String agree = inN921.get(acn).getValue("AGREE");
				String text = inN921.get(acn).getValue("TEXT");
				
				//結果集合無資料時先加第一筆說明
				if (resultRows.getSize() == 0 && agree.equals("1"))
				{
					Row firstRow = new Row(new HashMap());
					firstRow.setValue("ACN", "#");
					firstRow.setValue("FDPNUM", "");
					firstRow.setValue("AMTFDP", "0");
					firstRow.setValue("TEXT", "--約定帳號--");
					resultRows.addRow(firstRow);
				}

				Row newRow = new Row(new HashMap());
				newRow.setValue("ACN", acn);
				newRow.setValue("FDPNUM", fdpnum);
				newRow.setValue("AMTFDP", amount);
				newRow.setValue("TEXT", text);
				
				if (agree.equals("1"))
				{//約定帳號
					resultRows.addRow(newRow);
				}
				else if (agree.equals("0"))
				{//非約定帳號
					notAgreeRows.addRow(newRow);
				}
			}
			else
			{//N921結果集合中未含有此筆帳號資料
				Row newRow = new Row(new HashMap());
				newRow.setValue("ACN", acn);
				newRow.setValue("FDPNUM", fdpnum);
				newRow.setValue("AMTFDP", amount);
				newRow.setValue("TEXT", "050-臺灣企銀 " + acn);
				notAgreeRows.addRow(newRow);
			}
		}
		
		//如有非約定帳號，在結果集合中增加一筆說明
		if (notAgreeRows.getSize() > 0)
		{
			Row firstRow = new Row(new HashMap());
			firstRow.setValue("ACN", "#");
			firstRow.setValue("FDPNUM", "");
			firstRow.setValue("AMTFDP", "0");
			firstRow.setValue("TEXT", "--非約定帳號--");
			resultRows.addRow(firstRow);
		}
		
		resultRows.addRows(notAgreeRows);
		MVHImpl result = new MVHImpl(resultRows);
		
		//20190320 Ian 為了得到回傳值
		result.getFlatValues().put("TOPMSG",n420Mvh.getValueByFieldName("TOPMSG"));
		
		return result;
	}

	
	/**
	 * 
	 * @param params
	 * @return
	 */
	public MVH doN076(Map _params) { 
		final Map<String, String> params = _params;
		
/*		
		//BHO 轉入帳號檢核
		String str_UserInputAcno = (String)params.get("InAcno_1") + (String)params.get("InAcno_2") + (String)params.get("InAcno_3");
		String str_RandomValueAcno = (String)params.get("BHO_INACNO_VERIFY");		
		BHOCheckUtils.checkBHOInAcno(str_UserInputAcno, str_RandomValueAcno);		
*/
		
		//晶片金融卡
		if("2".equals(params.get("FGTXWAY").toString()))
		{
			Date d = new Date();
			String trin_transeq = "1234";
			String trin_issuer = "05000000";
			String trin_acnno = params.get("ACNNO").toString();
			String trin_icdttm = DateTimeUtils.format("yyyyMMddHHmmss", d);
			log.debug("ICDTTM ----------> " + trin_icdttm);
			String trin_iseqno = params.get("iSeqNo").toString();
			String trin_icmemo = "";
			String trin_tac_length = "000A";		
			String trin_tac = params.get("TAC").toString();
			log.debug("TAC ----------> " + trin_tac_length + trin_tac + "(" + (trin_tac_length.length() + trin_tac.length()) + ")");
			String trin_trmid = params.get("TRMID").toString();
		
			params.put("TRANSEQ", trin_transeq);
			params.put("ISSUER", trin_issuer);
			params.put("ACNNO", trin_acnno);
			params.put("ICDTTM", trin_icdttm);
			params.put("ICSEQ", trin_iseqno);
			params.put("ICMEMO", trin_icmemo);
			params.put("TAC_Length", trin_tac_length);
			params.put("TAC", trin_tac);
			params.put("TRMID", trin_trmid);
		}

		//產生 CERTACN update by Blair -------------------------------------------- Start
		String TxWay = params.get("FGTXWAY");
		String certACN = "";
		
		if("0".equals(TxWay)){
			while(certACN.length() < 19)certACN += " ";
		}
		else if("1".equals(TxWay)){
			while(certACN.length() < 19)certACN += "1";
		}
		else if("7".equals(TxWay)){
			while(certACN.length() < 19)certACN += "C";
		}
		else {
			while(certACN.length() < 19)certACN += "4";
		}
		params.put("CERTACN", certACN);
		//產生 CERTACN update by Blair -------------------------------------------- End
		
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);

		final TelCommExecWrapper execwrapper = new TelCommExecWrapper(n076Telcomm);
		
		MVHImpl result = execwrapper.query(params);
		 
		final TXNTWRECORD record = (TXNTWRECORD)this.writeLog(execwrapper, params, result);

		
		OnLineTransferNotice trNotice = new OnLineTransferNotice();
		final TelCommExecWrapper execwrapperFinal = execwrapper;
		trNotice.sendNotice(new NoticeInfoGetter() {

			public NoticeInfo getNoticeInfo() {
				NoticeInfo result = new NoticeInfo(params.get("UID"), params.get("DPMYEMAIL"), params.get("CMTRMAIL"));
				
				result.setADTXNO(record.getADTXNO());
				
				result.setTemplateName("transferNotify");
				result.setException(execwrapperFinal.getLastException());
				String datetime = record.getDPTXDATE() + record.getDPTXTIME();
				Date d = DateTimeUtils.parse("yyyyMMddHHmmss", datetime);
				String trantime = DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", d);
				result.getParams().put("#TRANTIME", trantime);
				result.getParams().put("#SEQNO", record.getDPTXNO());
				
				String acn = record.getDPSVAC();
				try {
					acn = StrUtils.left(acn, acn.length() - 5) + "***" + StrUtils.right(acn, 2);
				}
				catch(Exception e){}
				
				String outacn = record.getDPWDAC();
				try {
					outacn = StrUtils.left(outacn, outacn.length() - 5) + "***" + StrUtils.right(outacn, 2);
				}
				catch(Exception e){}
				
				result.getParams().put("#ACN", acn);
				result.getParams().put("#OUTACN", outacn);
				result.getParams().put("#OUTCURRENCY", "新台幣");
				String dptxamtfmt = NumericUtil.formatNumberString(record.getDPTXAMT(), 0);
				result.getParams().put("#AMT", dptxamtfmt);
				result.getParams().put("#MEMO", params.get("CMMAILMEMO"));
				
				
				ADMMAILLOG admmaillog = new ADMMAILLOG();
				admmaillog.setADACNO(record.getDPWDAC());
				
				String trans_status = (String)params.get("__TRANS_STATUS");
				if(StrUtils.isEmpty(trans_status)) {
					trans_status = "TWONLINE";
				}
				else if("SEND".equals(trans_status)) {
					trans_status = "TWSCH";
				}
				else if("RESEND".equals(trans_status)) {
					trans_status = "TWSCHRE";
				}
				
				admmaillog.setADBATCHNO(trans_status);
				admmaillog.setADMAILACNO(StrUtils.trim(params.get("CMTRMAIL")));
				admmaillog.setADUSERID(record.getDPUSERID());
				admmaillog.setADUSERNAME("");
				admmaillog.setADSENDTYPE("3");
				admmaillog.setADSENDTIME(datetime);
				admmaillog.setADSENDSTATUS("");
				
				result.setADMMAILLOG(admmaillog);
				
				return result;
			}
			
		});
		
		
		
		execwrapper.throwException(); 
		
		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		/*
			CMADDRESS
			CMTRMAIL
			CMMAILMEMO
		*/

		
		
		//TODO 傳入的交易備註 "CMTRMEMO" 不知如何處理
		try {
			result.getFlatValues().put("CMTXTIME", 
					DateTimeUtils.getCDateTime(result.getValueByFieldName("TRNDATE"),
											result.getValueByFieldName("TRNTIME")));
		}
		catch(Exception e){
			log.error("",e);
		}
		
		return result;
	}

	
	public Object writeLog(TelCommExecWrapper execwrapper, Map<String, String> params, final MVHImpl result) {

		TXNTWRECORD record = execwrapper.createTxnTwRecord();

		String date = record.getLASTDATE();
		String time = record.getLASTTIME();
		
		if(result != null) {
			try {
				date = DateTimeUtils.format("yyyyMMdd", DateTimeUtils.getCDate2Date(result.getValueByFieldName("TRNDATE")));
			}
			catch(Exception e){}
			try {
				time = result.getValueByFieldName("TRNTIME");
			}
			catch(Exception e){}				
		} 
		
		record.setDPUSERID(params.get("UID"));
		record.setADOPID(params.get("ADOPID"));
		record.setDPTXDATE(date);
		record.setDPTXTIME(time);
		record.setDPWDAC(params.get("OUTACN"));
		record.setDPSVBH("");
		record.setDPSVAC(params.get("FDPACN"));
		record.setDPTXAMT(params.get("AMOUNT"));

		record.setDPTXMEMO(params.get("CMTRMEMO"));
		record.setDPTXMAILS(params.get("CMTRMAIL"));
		record.setDPTXMAILMEMO(params.get("CMMAILMEMO"));  //CMMAILMEMO 對應的欄位
		
		record.setDPEFEE("0");
		record.setDPTXNO("");  //跨行序號
		record.setDPTXCODE(params.get("FGTXWAY")); 
		log.trace(" writeLog record :{}", record);
		// 20190510 edit by ian 改成不寫入 TXNREQINFO 只寫入TXNTWRECORD
		txnTwRecordDao.save(record);
	
		return record;
	}
}
