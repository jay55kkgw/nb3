package fstop.services.impl;

import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import fstop.model.EachRowCallback;
import fstop.model.MVH;
import fstop.model.Row;
import fstop.model.RowsSelecter;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 綜存定期性存款明細查詢
 * @author Owner
 *
 */
@Slf4j
public class N421 extends CommonService  {
	
	@Autowired
	@Qualifier("n421Telcomm")
	private TelCommExec n421Telcomm;
	
//	@Required
//	public void setN421Telcomm(TelCommExec telcomm) {
//		n421Telcomm = telcomm;
//	}

	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) {

		TelcommResult telcommResult = n421Telcomm.query(params);

		telcommResult.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		telcommResult.getFlatValues().put("CMRECNUM", telcommResult.getValueOccurs("ACN") + "");

		RowsSelecter rowSelecter = new RowsSelecter(telcommResult.getOccurs());
		rowSelecter.each(new EachRowCallback(){

			public void current(Row row) {
				
				if("0".equalsIgnoreCase(row.getValue("INTMTH"))) {
					row.setValue("DPINTTYPE", "機動");
				}
				if("1".equalsIgnoreCase(row.getValue("INTMTH"))) {
					row.setValue("DPINTTYPE", "固定");
				}
				
				String status = "";
				if ("*".equals(row.getValue("STATUS1")))
				{
					status += "質押";
				}
				if ("*".equals(row.getValue("STATUS2")))
				{
					status += "質借";
				}
				if ("*".equals(row.getValue("STATUS3")) || 
					"*".equals(row.getValue("STATUS4")) || 
					"*".equals(row.getValue("STATUS5")))
				{
					status += "＊";
				}
				row.setValue("STATUS", status);
				
			}});

		Long amtfdp = (long)rowSelecter.sum("AMTFDP");;
		telcommResult.getFlatValues().put("DPTAMT", amtfdp + "");
		
		return telcommResult;
	}

}
