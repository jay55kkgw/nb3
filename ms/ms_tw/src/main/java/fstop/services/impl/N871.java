package fstop.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.rest.model.CommonPools;
import fstop.model.IMasterValuesHelper;
import fstop.exception.TopMessageException;
import fstop.model.EachRowCallback;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.MVHUtils;
import fstop.model.Row;
import fstop.model.RowsSelecter;
import fstop.orm.dao.AdmKeyValueDao;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N871 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("n871Telcomm")
	private TelCommExec n871Telcomm;
	@Autowired
	private AdmKeyValueDao admKeyValueDao;
	@Autowired
	private CommonPools commonPools;
	
//	@Required
//	public void setN871Telcomm(TelCommExec telcomm) {
//		n871Telcomm = telcomm;
//	}
//
//	@Required
//	public void setAdmKeyValueDao(AdmKeyValueDao admKeyValueDao) {
//		this.admKeyValueDao = admKeyValueDao;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) {
		String stadate = (String) params.get("CMSDATE");
		String enddate = (String) params.get("CMEDATE");
		stadate = StrUtils.trim(stadate).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
		enddate = StrUtils.trim(enddate).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
		
		String periodStr = "";
		if (stadate != null && stadate.length() == 8) {
			Date sd = DateTimeUtils.parse("yyyyMMdd", stadate);
			stadate = DateTimeUtils.getCDateShort(sd);
			periodStr = DateTimeUtils.format("yyyy/MM/dd", sd);
		}
		if (enddate != null && enddate.length() == 8) {
			Date sd = DateTimeUtils.parse("yyyyMMdd", enddate);
			enddate = DateTimeUtils.getCDateShort(sd);
			periodStr += "~" + DateTimeUtils.format("yyyy/MM/dd", sd);
		} else { // 若未指定結束時間, 則使用今天日期
			enddate = DateTimeUtils.getCDateShort(new Date());
			periodStr += "~" + DateTimeUtils.format("yyyy/MM/dd", new Date());
		}

		params.put("STADATE", stadate);
		params.put("ENDDATE", enddate);

		
		EachRowCallback eachRow = new EachRowCallback(){
			public void current(Row row) {
				//使用 adm key value 轉換 TRNTYP
				String r = admKeyValueDao.translator("TRNTYP", row.getValue("TRNTYP"));
				row.setValue("TRNTYP", r);
			}
		};
		
		MVHImpl result = new MVHImpl();
		
		queryAll(getAcnoList(params), params, result, eachRow);
		
//		if(StrUtils.isEmpty((String)params.get("ACN"))) {
//			//查詢全部帳號
//			queryAll(params, result, eachRow);
//		}
//		else {
//			MVHImpl resultN871 = n871Telcomm.query(params);;
//			final String acn = result.getFlatValues().get("ACN");
//			
//			resultN871.getOccurs().setValue("ACN", acn);
//			
//			RowsSelecter rowSelecter = new RowsSelecter(resultN871.getOccurs());
//			rowSelecter.each(eachRow);
//			
//			result.addTable(resultN871, acn);
//		}
		
		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		result.getFlatValues().put("CMPERIOD", periodStr);
		result.getFlatValues().put("CMRECNUM", MVHUtils.sumTablesOccurs(result) + "");
		


		return result;
	}

	private List<String> getAcnoList(Map<String, String> params) {
		List<String> acnos = new ArrayList();
		if(StrUtils.isEmpty((String)params.get("ACN"))) {
			
			IMasterValuesHelper acnosMVH = commonPools.n920.getAcnoList((String)params.get("CUSIDN"), new String[]{"08"}, "");
			
			int requestCount = acnosMVH.getValueOccurs("ACN");
			for(int i=0; i < requestCount; i++) {
				String acn = acnosMVH.getValueByFieldName("ACN", i+1);
				acnos.add(acn);
			}
			
		}
		else {
			acnos.add(params.get("ACN"));
		}
		return acnos;
	}	
	
	public void queryAll(List<String> acnoList, Map<String, String> params, MVHImpl resultMVH, EachRowCallback eachRow) {
		//IMasterValuesHelper acnos = CommonPools.n920.getAcnoList((String)params.get("UID"), new String[]{"08"}, "");

 		//int requestCount = acnos.getValueOccurs("ACN");
 		for(String acn : acnoList) {
 			//String acn = acnos.getValueByFieldName("ACN", i+1);
 			params.put("ACN", acn);
 			try {
	 			MVHImpl result = n871Telcomm.query(params);
	 			result.getOccurs().setValue("ACN", acn);
	 			
	 			RowsSelecter rowSelecter = new RowsSelecter(result.getOccurs());
				rowSelecter.each(eachRow);
				
	 			resultMVH.addTable(result, acn);
	 			resultMVH.getFlatValues().putAll(result.getFlatValues());
			}
			catch(TopMessageException ex) {
				MVHImpl helper = new MVHImpl();
				helper.getFlatValues().put("ACN", acn);
				helper.getFlatValues().put("TOPMSG", ex.getMsgcode());
				helper.getFlatValues().put("ADMSGIN", ex.getMsgin());
				helper.getFlatValues().put("ADMSGOUT", ex.getMsgout());
				resultMVH.addTable(helper, acn);
			} 			
 		}
	}


}
