package fstop.services.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.va.VaCheck;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N855Decrypt extends CommonService{

	@Value("${vaKeyLableFisc}")
	String vaKeyLable;
	
	@Override
	public MVH doAction(Map _params) { 

		MVHImpl result = new MVHImpl();
		try {
			byte[] divData = VaCheck.hexToBytes((String)_params.get("DivData"));
	        log.debug("divData>>{}",divData);
	        byte[] macKey = VaCheck.genVaMacKey(vaKeyLable, divData);
			String CustIdnBan = (String)_params.get("CustIdnBan");
			String CustBillerAcnt = (String)_params.get("CustBillerAcnt");
			String CustBankAcnt = (String)_params.get("CustBankAcnt");
			if(CustIdnBan != null && !CustIdnBan.equals("")) {
				_params.put("CustIdnBan_Dec", new String(VaCheck.EBCDecrypt(macKey, VaCheck.hexToBytes(CustIdnBan))));
			}
			if(CustBillerAcnt != null && !CustBillerAcnt.equals("")) {
				_params.put("CustBillerAcnt_Dec", new String(VaCheck.EBCDecrypt(macKey, VaCheck.hexToBytes(CustBillerAcnt))));
			}
			if(CustBankAcnt != null && !CustBankAcnt.equals("")) {
				_params.put("CustBankAcnt_Dec", new String(VaCheck.EBCDecrypt(macKey, VaCheck.hexToBytes(CustBankAcnt))));
			}
			result.getFlatValues().putAll(_params);
			result.getFlatValues().put("TOPMSG", "0000");
		}catch (TopMessageException e) {
			log.error("N855Decrypt TopMessageException: >> {}",e);
			throw e;
		}catch(Exception e) {
			log.error("{}", e);
		}
		return result;
	}
}
