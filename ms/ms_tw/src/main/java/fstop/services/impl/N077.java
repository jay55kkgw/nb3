package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.services.DispatchService;
import fstop.services.ServiceBindingException;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class N077 extends DispatchService  {
	
	//call N420.java
	@Autowired
	@Qualifier("n420Service")
	private CommonService n420Service;
	@Autowired
	@Qualifier("n077Telcomm")
	private TelCommExec n077Telcomm;
	
	
	@Override
	public String getActionMethod(Map<String, String> params) {
		String txid = StrUtils.trim(params.get("TXID"));
		if("N077".equals(txid))
			return "doN420";
		else if("N077_1".equals(txid))
			return "doN077_1";
		else 
			throw new ServiceBindingException("不支援的 TXID [" + txid+"].");
	}
	
	public MVH doN420(Map<String, String> params) {
		return n420Service.doAction(params);
	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	public MVH doN077_1(Map<String, String> params) { 
		
		//晶片金融卡
		if("2".equals(params.get("FGTXWAY").toString()))
		{
			Date d = new Date();
			String trin_transeq = "1234";
			String trin_issuer = "05000000";
			String trin_acnno = params.get("ACNNO").toString();
			String trin_icdttm = DateTimeUtils.format("yyyyMMddHHmmss", d);
			log.debug("ICDTTM ----------> " + trin_icdttm);
			String trin_iseqno = params.get("iSeqNo").toString();
			String trin_icmemo = "";
			String trin_tac_length = "000A";		
			String trin_tac = params.get("TAC").toString();
			log.debug("TAC ----------> " + trin_tac_length + trin_tac + "(" + (trin_tac_length.length() + trin_tac.length()) + ")");
			String trin_trmid = params.get("TRMID").toString();
		
			params.put("TRANSEQ", trin_transeq);
			params.put("ISSUER", trin_issuer);
			params.put("ACNNO", trin_acnno);
			params.put("ICDTTM", trin_icdttm);
			params.put("ICSEQ", trin_iseqno);
			params.put("ICMEMO", trin_icmemo);
			params.put("TAC_Length", trin_tac_length);
			params.put("TAC", trin_tac);
			params.put("TRMID", trin_trmid);
		}
		
		String fgsvtype = params.get("FGSVTYPE");  //轉存方式
		if("1".equals(fgsvtype)) { //指定轉入帳號
			params.put("CHGFLG", "Y");  //CHGFLG=Y表利息轉入帳號，CHGFLG=N表本金及利息一併轉期
			
			if(!StrUtils.trim(params.get("TSFACN")).equals(StrUtils.trim(params.get("DPSVACNO")))) {
				params.put("TSFACN", params.get("DPSVACNO"));
			}
			else {
				params.put("TSFACN", params.get("TSFACN"));
			}
		} else if("2".equals(fgsvtype)) { //本金及利息一併轉期
			params.put("CHGFLG", "N");
			params.put("TSFACN", "");
		}

		String fgrencnt = params.get("FGRENCNT");  //轉期次數
		if("1".equals(fgrencnt)) { //無限次數
			params.put("TRNCNT", "99");
			
		}
		else if("2".equals(fgrencnt)) { //有限次數
			
			params.put("TRNCNT", StrUtils.trim(params.get("DPRENCNT")));
			
		}
		else if("3".equals(fgrencnt)) { //取消自動轉期
			params.put("TRNCNT", "0");
			params.put("CHGFLG", "N");
			params.put("TSFACN", "");
		}

		//產生 CERTACN update by Blair -------------------------------------------- Start
		String TxWay = params.get("FGTXWAY");
		String certACN = "";
		
		if("0".equals(TxWay)){
			while(certACN.length() < 19)certACN += " ";
		}
		else if("1".equals(TxWay)){
			while(certACN.length() < 19)certACN += "1";
		}
		else if ("7".equals(TxWay)) {
			while (certACN.length() < 19)certACN += "C";
		}
		else {
			while(certACN.length() < 19)certACN += "4";
		}
		params.put("CERTACN", certACN);
		//產生 CERTACN update by Blair -------------------------------------------- End
		
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);

		TelcommResult result = n077Telcomm.query(params);
		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));

		try {
			result.getFlatValues().put("CMTXTIME", 
					DateTimeUtils.getCDateTime(
							result.getValueByFieldName("TRNDATE"),
							result.getValueByFieldName("TRNTIME")));
		}
		catch(Exception e){
			log.error("doN077_1 error >>{}", e);
		}
		
		return result;
	}



}
