package fstop.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.netbank.util.ESAPIUtil;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.ScRecordDao;
import fstop.orm.po.SCRECORD;
import fstop.services.CommonService;
import fstop.util.DateTimeUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class N070_N extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	private ScRecordDao scRecordDao;
	
//	@Required
//	public void setScRecordDao(ScRecordDao scRecordDao) {
//		this.scRecordDao = scRecordDao;
//	}

	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params) { 
		Map<String, String> params = _params;
		MVHImpl result = new MVHImpl();
		ScRecordDao scRecordDao = (ScRecordDao)SpringBeanFactory.getBean("scRecordDao");
  		List<SCRECORD> screcord = new ArrayList<SCRECORD>();
  		screcord = scRecordDao.getUserSCTN(StrUtils.trim(params.get("UID")),StrUtils.trim(params.get("SCTN")));   
  		log.debug(ESAPIUtil.vaildLog(StrUtils.trim(params.get("UID"))+" "+StrUtils.trim(params.get("SCTN"))+ "_N：SCRECORD SIZE-->"+ screcord.size()));
		Date d = new Date(); 
		if(screcord.size()==0) {      							
			try {
				scRecordDao.insertSCVersion(StrUtils.trim(params.get("UID")), StrUtils.trim(params.get("SCTN")), StrUtils.trim(params.get("SCVersion")), "N", DateTimeUtils.format("yyyyMMdd", d));
				log.debug(ESAPIUtil.vaildLog(StrUtils.trim(params.get("UID"))+" "+StrUtils.trim(params.get("SCTN"))+ "_N：Insert success"));
			}
			catch (Exception e) {		
				log.debug(ESAPIUtil.vaildLog(StrUtils.trim(params.get("UID"))+" "+StrUtils.trim(params.get("SCTN"))+ "_N：Insert fail"));
			}
		}
		else if(screcord.size()>0) {
			try {			
				scRecordDao.updateSCVersion(StrUtils.trim(params.get("UID")), StrUtils.trim(params.get("SCTN")), StrUtils.trim(params.get("SCVersion")), "N", DateTimeUtils.format("yyyyMMdd", d));   			
				log.debug(ESAPIUtil.vaildLog(StrUtils.trim(params.get("UID"))+" "+StrUtils.trim(params.get("SCTN"))+ "_N：Update success"));
			}
			catch (Exception e) {		
				log.debug(ESAPIUtil.vaildLog(StrUtils.trim(params.get("UID"))+" "+StrUtils.trim(params.get("SCTN"))+ "_N：Update fail"));				
			}			
		}
		return result;		
	}


}
