package fstop.services.impl;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.rest.model.JSPUtils;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.MVHUtils;
import fstop.model.RowsSelecter;
import fstop.model.TelcommResult;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.TXNUSER;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import fstop.notifier.NotifyAngent;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class NA721 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("na72Telcomm")
	private TelCommExec na72Telcomm;

	@Autowired
	private SmsOtp smsotpmap;
	
//	@Required
//	public void setNa72Telcomm(TelCommExec telcomm) {
//		na72Telcomm = telcomm;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 
		MVHImpl telcommResult = new MVHImpl();
		String FGTXWAY = params.get("FGTXWAY").toString();  //交易機制 2:晶片金融卡 		
		Date d = new Date();
		//晶片金融卡
		if("2".equals(FGTXWAY))
		{
			String trin_transeq = params.get("TRANSEQ").toString();
			String trin_issuer = params.get("ISSUER").toString();
			String trin_acnno = params.get("ACNNO").toString();
			String trin_icdttm = DateTimeUtils.format("yyyyMMddHHmmss", d);
			log.warn("ICDTTM ----------> " + trin_icdttm);
			String trin_iseqno = params.get("iSeqNo").toString();
			String trin_icmemo = "";
			String trin_tac_length = "000A";		
			String trin_tac = params.get("TAC").toString();
			log.warn(ESAPIUtil.vaildLog("TAC ----------> " + trin_tac_length + trin_tac + "(" + (trin_tac_length.length() + trin_tac.length()) + ")"));
			String trin_trmid = params.get("TRMID").toString();
		
			params.put("TRANSEQ", trin_transeq);
			params.put("ISSUER", trin_issuer);
			params.put("ACNNO", trin_acnno);
			params.put("ICDTTM", trin_icdttm);
			params.put("ICSEQ", trin_iseqno);
			params.put("ICMEMO", trin_icmemo);
			params.put("TAC_Length", trin_tac_length);
			params.put("TAC", trin_tac);
			params.put("TRMID", trin_trmid);
		}		
		//驗證OTP及發送NA72電文
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("FGTXWAY", "");  //HLOGINPIN加密需把FGTXWAY清成空值
		params.put("PHASE1", "");  //避免加解密錯誤
		params.put("PHASE2", "");  //避免加解密錯誤
		params.put("TRANSPASSUPDATE", "");  //避免加解密錯誤
		params.put("DATE", df);
		params.put("TIME", tf);
		params.put("CUSIDN", params.get("UID"));
		params.put("TYPE", "2"); //交易密碼
		params.put("FuncType","1"); //1:驗證
		log.warn(ESAPIUtil.vaildLog("NA721.java UID:"+params.get("UID")));
		MVHImpl smsotp = new MVHImpl();
		String MSGCOD = "";
		try {
			smsotp = (MVHImpl)smsotpmap.doAction(params);
			MSGCOD = smsotp.getValueByFieldName("MSGCOD");
			if(MSGCOD.equals("0000"))    //OTP驗證成功才能執行NA72 簽入密碼及使用者名稱重設
			{
				telcommResult = na72Telcomm.query(params);
				telcommResult.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
				log.warn("NA721.java na72Telcomm RSPCOD:"+telcommResult.getValueByFieldName("RSPCOD"));
				if(telcommResult.getValueByFieldName("RSPCOD").equals("0000") || telcommResult.getValueByFieldName("RSPCOD").trim().equals(""))
				{
					try {
						//避免取得客戶mail失敗出錯
						final TxnUserDao txnUserDao = (TxnUserDao)SpringBeanFactory.getBean("txnUserDao");
						TXNUSER txnUser = txnUserDao.findById(params.get("UID").toString());
						final String dpmyemail = StrUtils.trim(txnUser.getDPMYEMAIL() == null ? "" : txnUser.getDPMYEMAIL());					
						Hashtable<String, String> varmail1 = new Hashtable();
						if(dpmyemail!=null && dpmyemail.length()>0) {
							varmail1.put("SUB","線上重設網路銀行交易密碼成功通知");
							varmail1.put("MAILTEXT", getContent1(params.get("UID").toString(),d));
							if(!NotifyAngent.sendNotice("NA721",varmail1,dpmyemail)){
								log.warn(ESAPIUtil.vaildLog("線上重設網路銀行交易密碼發送Email失敗.(mail:"+dpmyemail+")"));
							}					
						}
						else {
							log.warn("dpmyemail not found");
						}
					}
					catch (Exception e) {
						log.error("get dpmyemail error:" + e);
					}
				}
			}
			else
			{
				telcommResult.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
				telcommResult.getFlatValues().put("RSPCOD", MSGCOD);
			}
		}
		catch(TopMessageException top) {
			telcommResult.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
			telcommResult.getFlatValues().put("RSPCOD", top.getMsgcode());
			log.warn("NA721.java query na72telcomm fail:"+top.getMsgcode());
		}
		return telcommResult;
	}
	@SuppressWarnings("unused")
	private String getContent1(String cusidn,Date d){
		StringBuilder sb = new StringBuilder ();
		String dotime = DateTimeUtils.getDatetime(d);
		sb.append("<table cellSpacing=0 cellPadding=0 width=600 border=0 frame=border >").append("\n");
		sb.append("<tbody>").append("\n");
		sb.append("<tr><td align=left>").append("親愛的客戶，您好：</td></tr></br>").append("\n");
		sb.append("<tr><td align=left>").append("您以身分證字號：").append(JSPUtils.hideid(cusidn)).append("</td></tr>").append("\n");
		sb.append("<tr><td align=left>於 ").append(dotime).append(" 辦理線上重設交易密碼，</td></tr>").append("\n");
		sb.append("<tr><td align=left>").append("煩請確認您是否於此期間進行重設作業，若您並未執行相關動作，</td></tr>").append("\n");
		sb.append("<tr><td align=left>").append("請立即與本行聯絡，以確保您的權益。</td></tr>").append("\n");
		sb.append("<tr><td align=left>").append("臺灣企銀貼心提醒您！</td></tr>").append("\n");
		sb.append("<tr><td align=rigth>臺灣企銀   敬啟</td></tr>").append("\n");
		sb.append("</tbody>").append("\n");
		sb.append("</table>").append("\n");
		return sb.toString();
	}	

}
