package fstop.services.impl;

import java.util.Date;

import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;
//@Transactional
@Slf4j
public class N920 extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());
	@Autowired
	@Qualifier("n920Telcomm")
	private TelCommExec n110Telcomm;
	
//	@Required
//	public void setN110Telcomm(TelCommExec n110Telcomm) {
//		this.n110Telcomm = n110Telcomm;
//	}
	
	@Override
	public MVH doAction(Map params) {		
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);
		
		TelcommResult helper = n110Telcomm.query(params);
		helper.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		helper.getFlatValues().put("CMRECNUM", helper.getValueOccurs("ACN") + "");
		return helper;
	}
}
