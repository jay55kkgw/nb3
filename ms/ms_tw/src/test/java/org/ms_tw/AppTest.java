package org.ms_tw;

import com.netbank.RestApplication;
import fstop.util.JSONUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.jndi.SimpleNamingContextBuilder;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.naming.NamingException;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@WebAppConfiguration
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {RestApplication.class})
public class AppTest {


    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mvc; // Mock MVC


    @Autowired
    protected MockHttpSession session;

    @Autowired
    protected MockHttpServletRequest request;


    @Autowired
    protected MockHttpServletResponse response;


    @BeforeClass
    public static void initDatasource() throws NamingException {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        dataSourceBuilder.url("jdbc:sqlserver://127.0.0.1:1401;database=PIC_MON");
        dataSourceBuilder.username("sa");
        dataSourceBuilder.password("n0v3llP@ssw0rd");

        DataSource dataSource =  dataSourceBuilder.build();

        System.out.println("bind datasource");
        SimpleNamingContextBuilder builder = new SimpleNamingContextBuilder();
        builder.bind("jdbc/nnb", dataSource);
        builder.activate();
    }

    @Test
    public void restTest() throws Exception {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();//建議使用這種

        Map<String, String> params = new HashMap<>();

        params.put("ADOPID", "C019");
        params.put("FGTXWAY", "1");
        params.put("ACNNO", "0149004000111");


        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/com_ignore/C019")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(JSONUtils.map2json(params)))
//                        .andDo(MockMvcResultHandlers.print())
                .andReturn();

        int status = result.getResponse().getStatus();
        log.info("response status: {}", status);
        Assert.assertEquals(200,status);

        String contentString =  result.getResponse().getContentAsString();
        System.out.println("--contentString-------");
        System.out.println(contentString);
        System.out.println("---------");
    }
}
