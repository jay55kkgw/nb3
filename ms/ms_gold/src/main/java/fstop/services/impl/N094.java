package fstop.services.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import topibs.utility.NumericUtil;
import com.netbank.rest.model.CommonPools;
import com.netbank.util.ESAPIUtil;

import fstop.model.IMasterValuesHelper;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.orm.dao.TxnReqInfoDao;
import fstop.orm.dao.TxnTwRecordDao;
import fstop.orm.po.ADMMAILLOG;
import fstop.orm.po.TXNREQINFO;
import fstop.orm.po.TXNTWRECORD;
import fstop.services.BookingUtils;
import fstop.services.CommonService;
import fstop.services.DispatchService;
import fstop.services.NoticeInfo;
import fstop.services.NoticeInfoGetter;
import fstop.services.OnLineTransferNotice;
import fstop.services.ServiceBindingException;
import fstop.services.TelCommExecWrapper;
import fstop.services.WriteLogInterface;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 繳納定期扣款失敗手續費
 * @author Owner
 *
 */
@Slf4j
public class N094 extends DispatchService implements WriteLogInterface {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("n094QueryTelcomm")
	private TelCommExec n094QueryTelcomm;

	@Autowired
    @Qualifier("n094PayTelcomm")
	private TelCommExec n094PayTelcomm;

	@Autowired
	private TxnTwRecordDao txnTwRecordDao;

//	@Autowired
//	private TxnReqInfoDao txnReqInfoDao;
	
//	@Required
//	public void setTxnReqInfoDao(TxnReqInfoDao txnReqInfoDao) {
//		this.txnReqInfoDao = txnReqInfoDao;
//	}
//	
//	@Required
//	public void setN094QueryTelcomm(TelCommExec telcomm) {
//		n094QueryTelcomm = telcomm;
//	}
//	@Required
//	public void setN094PayTelcomm(TelCommExec telcomm) {
//		n094PayTelcomm = telcomm;
//	}
//	
//	@Required
//	public void setTxnTwRecordDao(TxnTwRecordDao txnTwRecordDao) {
//		this.txnTwRecordDao = txnTwRecordDao;
//	}
	
	@Override
	public String getActionMethod(Map<String, String> params) {
		String txid = StrUtils.trim(params.get("TXID"));
		log.debug(ESAPIUtil.vaildLog("XXXX txid: " + txid));
		if("N094_1".equals(txid))
			return "doN094Query";
		else if("N094".equals(txid))
			return "doN094Pay";
		else 
			throw new ServiceBindingException("不支援的 TXID [" + txid+"].");
	}
	
	/**
	 * 查詢定期扣款失敗手續費
	 * @param params
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public MVH doN094Query(Map params) { 
		
		String uid = params.get("UID").toString();
				
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);
		params.put("TRNBDT", df);
		params.put("CUSIDN", uid);
		params.put("CUSIDN1", uid);
		params.put("SVACN", "");
		MVHImpl result = n094QueryTelcomm.query(params);
		
		return result;
	}

	/**
	 * 繳納定期扣款失敗手續費
	 * @param params
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public MVH doN094Pay(Map _params) { 
		final Map<String, String> params = _params;
		
		String uid = params.get("UID").toString();
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);
		params.put("TRNCOD", "02");	//正式交易
		params.put("TRNBDT", df);
		params.put("CUSIDN", uid);
		params.put("CUSIDN1", uid);

		
		if("0".equals(params.get("FGTXWAY"))) {  //使用交易密碼
//			String TransPassUpdate = (String)params.get("TRANSPASSUPDATE");
//			String Phase2 = (String)params.get("PHASE2");
			TelCommExec n950CMTelcomm;
			
//			if(StrUtils.isNotEmpty(TransPassUpdate))
//			{
//				if(TransPassUpdate.equals("1") || Phase2.equals("ON"))
//				{
					n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n951CMTelcomm");
//				}
//				else
//				{
//					n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n950CMTelcomm");
//				}
//			}
//			else
//			{
//				n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n950CMTelcomm");
//			}
			
			//N950會發兩次所以需FGTXWAY=""，N950回應後再把FGTXWAY改成原值即可 2010.09.29
			String fgtxway = (String)params.get("FGTXWAY");
			params.put("FGTXWAY", "");
			MVHImpl n950Result = n950CMTelcomm.query(params);
			
			//若驗證失敗, 則會發動 TopMessageException
			params.put("FGTXWAY", fgtxway);
		}		
		else if("7".equals(params.get("FGTXWAY"))) {  //晶片金融卡
			params.put("CERTACN", StrUtils.repeat("C", 19));	//即時
		}

		
		final TelCommExecWrapper execwrapper = new TelCommExecWrapper(n094PayTelcomm);
		MVHImpl result = execwrapper.query(params);
		
		//spring會找此class同名稱的bean，所以要注意class名與ApplicationContext-service檔裡是否一致
		final TXNTWRECORD record = (TXNTWRECORD)writeLog(execwrapper, params, result);

		/* 發送MAIL
		OnLineTransferNotice trNotice = new OnLineTransferNotice();
		
		final TelCommExecWrapper execwrapperFinal = execwrapper;
		trNotice.sendNotice(new NoticeInfoGetter() {

			public NoticeInfo getNoticeInfo() {
				NoticeInfo result = new NoticeInfo(params.get("UID"), params.get("DPMYEMAIL"), params.get("CMTRMAIL"));
				
				result.setADTXNO(record.getADTXNO());
				
				result.setTemplateName("TwDepCancelNotify");
				result.setException(execwrapperFinal.getLastException());
				String datetime = record.getDPTXDATE() + record.getDPTXTIME();
				log.debug("datetime = " + datetime);
				Date d = DateTimeUtils.parse("yyyyMMddHHmmss", datetime);
				log.debug("Hours = " + d.getHours());
				log.debug("Minutes = " + d.getMinutes());
				log.debug("Seconds = " + d.getSeconds());
				String trantime = DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", d);
				log.debug("trantime = " + trantime);
				result.getParams().put("#TRANTIME", trantime);
				
				String acn = record.getDPSVAC().trim();
//				try {
//					acn = StrUtils.left(acn, acn.length() - 5) + "***" + StrUtils.right(acn, 2);
//				}
//				catch(Exception e){}
				
				String outacn = record.getDPWDAC().trim();
				try {
					outacn = StrUtils.left(outacn, outacn.length() - 5) + "***" + StrUtils.right(outacn, 2);
				}
				catch(Exception e){}
				
				result.getParams().put("#ACN", acn);
				result.getParams().put("#OUTACN", outacn);
				result.getParams().put("#OUTCURRENCY", "新台幣");
				String dptxamtfmt = NumericUtil.formatNumberString(record.getDPTXAMT(), 0);
				result.getParams().put("#AMT", dptxamtfmt);

				log.debug("format #INTPAY = " + params.get("#INTPAY"));
				result.getParams().put("#INTPAY", params.get("#INTPAY"));
				log.debug("format #TAX = " + params.get("#TAX"));
				result.getParams().put("#TAX", params.get("#TAX"));
				log.debug("format #INTRCV = " + params.get("#INTRCV"));
				result.getParams().put("#INTRCV", params.get("#INTRCV"));
				log.debug("format #PAIAFTX = " + params.get("#PAIAFTX"));
				result.getParams().put("#PAIAFTX", params.get("#PAIAFTX"));

				
				ADMMAILLOG admmaillog = new ADMMAILLOG();
				admmaillog.setADACNO(record.getDPWDAC());
				
				String trans_status = (String)params.get("__TRANS_STATUS");
				if(StrUtils.isEmpty(trans_status)) {
					trans_status = "TWONLINE";
				}
				else if("SEND".equals(trans_status)) {
					trans_status = "TWSCH";
				}
				else if("RESEND".equals(trans_status)) {
					trans_status = "TWSCHRE";
				}
				
				admmaillog.setADBATCHNO(trans_status);
				admmaillog.setADMAILACNO(StrUtils.trim(params.get("CMTRMAIL")));
				admmaillog.setADUSERID(record.getDPUSERID());
				admmaillog.setADUSERNAME("");
				admmaillog.setADSENDTYPE("3");
				admmaillog.setADSENDTIME(datetime);
				admmaillog.setADSENDSTATUS("");
				
				result.setADMMAILLOG(admmaillog);
				
				return result;
			}
			
		});
		*/
		
		execwrapper.throwException();		

		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		try {
			result.getFlatValues().put("CMTXTIME", 
					DateTimeUtils.getCDateTime(
							result.getValueByFieldName("TRNDATE"),
							result.getValueByFieldName("TRNTIME")));
		}
		catch(Exception e){
			log.error(e.toString());			
		}
		
		return result;
	}

	@Override
	public Object writeLog(TelCommExecWrapper execwrapper, Map<String, String> params, final MVHImpl result) {

		TXNTWRECORD record = execwrapper.createTxnTwRecord();
		
		String date = record.getLASTDATE();
		String time = record.getLASTTIME();
		
		if(result != null) {
			try {
				date = DateTimeUtils.format("yyyyMMdd", DateTimeUtils.getCDate2Date(result.getValueByFieldName("TRNDATE")));
			}
			catch(Exception e){}
			try {
				time = result.getValueByFieldName("TRNTIME");
			}
			catch(Exception e){}				
		} 
		
		record.setDPUSERID(params.get("UID"));
		record.setADOPID(params.get("ADOPID"));
		record.setDPTXDATE(date);
		record.setDPTXTIME(time);
		record.setDPWDAC(params.get("SVACN"));
		record.setDPSVBH("050");
		record.setDPSVAC(params.get("ACN"));
		record.setDPTXAMT(params.get("TRNAMT"));

		record.setDPTXMEMO("");
		record.setDPTXMAILS("");
		record.setDPTXMAILMEMO("");  //CMMAILMEMO 對應的欄位
		
		record.setDPEFEE("0");
		record.setDPTXNO("");  //跨行序號
		record.setDPTXCODE(params.get("FGTXWAY"));

//		record.setDPSCHNO("");
		
		//暫時移除TXNREQINFO
		TXNREQINFO titainfo = null;
//		if(record.getDPTITAINFO() == 0) {
//			titainfo = new TXNREQINFO();
//			titainfo.setREQINFO(BookingUtils.requestInfo(params));
//		}
	
		txnTwRecordDao.writeTxnRecord(titainfo, record);
		
		return record;
	}
}
