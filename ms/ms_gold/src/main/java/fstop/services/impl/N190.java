package fstop.services.impl;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.rest.model.CommonPools;
import com.netbank.util.ESAPIUtil;

import fstop.model.IMasterValuesHelper;

import fstop.exception.TopMessageException;
import fstop.model.EachRowCallback;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.model.RowsSelecter;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N190 extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("n190Telcomm")
	private TelCommExec n190Telcomm;

	@Autowired
	private CommonPools commonPools;
//	@Required
//	public void setN190Telcomm(TelCommExec n190Telcomm) {
//		this.n190Telcomm = n190Telcomm;
//	}
	
	@Override
	public MVH doAction(Map params) {	
		MVHImpl result = new MVHImpl();
		queryAll(getAcnoList(params), params, result);
		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		result.getFlatValues().put("CMRECNUM", result.getTableCount() + "");
		return result;
	}

	private List<String> getAcnoList(Map<String, String> params) {
		List<String> acnos = new ArrayList();
		log.debug(ESAPIUtil.vaildLog("step0 N190.java ACN:"+(String)params.get("ACN")));
		IMasterValuesHelper acnosMVH;
		if(StrUtils.isEmpty((String)params.get("ACN"))) {
			
			acnosMVH = commonPools.n920.getAcnoList(params.get("CUSIDN"), new String[]{"07","09"}, "");//UID改CUSIDN
			
			int requestCount = acnosMVH.getValueOccurs("ACN");

			for(int i=0; i < requestCount; i++) {
				String acn = acnosMVH.getValueByFieldName("ACN", i+1);
				if(acn!=null)
				{	
					acnos.add(acn);
					log.debug(ESAPIUtil.vaildLog("step1 N190.java ACN:"+acn));
				}
			}
		}
		else {
				String trancode=((String)params.get("trancode"))==null ? "" : ((String)params.get("trancode"));
				log.debug(ESAPIUtil.vaildLog("step2 N190.java not empty ACN:"+(String)params.get("ACN")+" trancode:"+trancode));			
				if(trancode.indexOf("N190")>-1 || trancode.indexOf("A1000")>-1 || trancode.indexOf("A2000")>-1)
				{	
					acnosMVH = commonPools.n920.getAcnoList(params.get("CUSIDN"), new String[]{"07","09"}, "");
					
					int requestCount = acnosMVH.getValueOccurs("ACN");

					for(int i=0; i < requestCount; i++) {
						String acn = acnosMVH.getValueByFieldName("ACN", i+1);
						if(acn!=null)
						{	
							acnos.add(acn);
							log.debug(ESAPIUtil.vaildLog("step3 N190.java ACN:"+acn+" trancode:"+trancode));
						}
					}				
				}
				else
				{	
					acnos.add((String)params.get("ACN"));
					log.debug(ESAPIUtil.vaildLog("step4 N190.java not empty ACN:"+(String)params.get("ACN")+" trancode:"+trancode));			
				}
		}
		return acnos;
	}	

	public void queryAll(List<String> acnoList, Map<String, String> params, MVHImpl resultMVH) {
 		
		float totalTSFBAL = 0;
		float totalGDBAL = 0;
		float totalODFBAL = 0;
		float totalMKBAL = 0;
		
 		for(String acn : acnoList) {
 			log.debug(ESAPIUtil.vaildLog("N190.java queryAll() ACN:"+acn));
 			if(acn!=null)
 				params.put("ACN", acn);
 			try {
 				MVHImpl result = n190Telcomm.query(params); //此處不補捉錯誤，所以一遇有錯會中止不繼續查詢
 				resultMVH.addTable(result, acn);
	 		
 				float tsfbal = Float.parseFloat(result.getValueByFieldName("TSFBAL"));
 				totalTSFBAL += tsfbal;
 				float gdbal = Float.parseFloat(result.getValueByFieldName("GDBAL"));
 				totalGDBAL += gdbal;
 				float odfbal = Float.parseFloat(result.getValueByFieldName("ODFBAL"));
 				totalODFBAL += odfbal;
 				float mkbal = Float.parseFloat(result.getValueByFieldName("MKBAL"));
 				totalMKBAL += mkbal;
 			}
 			catch(TopMessageException ex) {
				MVHImpl result = new MVHImpl();
				result.getFlatValues().put("ACN", acn);
				result.getFlatValues().put("TOPMSG", ex.getMsgcode());
				result.getFlatValues().put("ADMSGIN", ex.getMsgin());
				result.getFlatValues().put("ADMSGOUT", ex.getMsgout());
				//resultMVH.addTable(result, acn);			
					
			} 			
 		}
 		
 		resultMVH.getFlatValues().put("TOTAL_TSFBAL", totalTSFBAL + "");
 		resultMVH.getFlatValues().put("TOTAL_GDBAL", totalGDBAL + "");
 		resultMVH.getFlatValues().put("TOTAL_ODFBAL", totalODFBAL + "");
 		resultMVH.getFlatValues().put("TOTAL_MKBAL", totalMKBAL + "");
	}
}
