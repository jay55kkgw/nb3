package fstop.services.impl;

import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.util.ESAPIUtil;

import fstop.exception.ToRuntimeException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.orm.dao.SysParamDataDao;
import fstop.orm.dao.TxnGdRecordDao;
import fstop.orm.dao.TxnTwRecordDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.ADMMAILLOG;
import fstop.orm.po.SYSPARAMDATA;
import fstop.orm.po.TXNGDRECORD;
import fstop.orm.po.TXNREQINFO;
import fstop.orm.po.TXNTWRECORD;
import fstop.orm.po.TXNUSER;
import fstop.services.BookingAware;
import fstop.services.BookingUtils;
import fstop.services.CommonService;
import fstop.services.NoticeInfo;
import fstop.services.NoticeInfoGetter;
import fstop.services.OnLineTransferNotice;
import fstop.services.TelCommExecWrapper;
import fstop.services.WriteLogInterface;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import topibs.utility.NumericUtil;

/**
 *   黃金定期定額變更交易
 */
@Slf4j
public class N093_02 extends CommonService  implements WriteLogInterface {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	private TxnUserDao txnUserDao;
	
	@Autowired
    @Qualifier("n093Telcomm")
	private TelCommExec n093Telcomm;	
	
	@Autowired
    @Qualifier("n093QueryTelcomm")
	private TelCommExec n093QueryTelcomm;

	@Autowired
	private TxnGdRecordDao txnGdRecordDao;
	
//	@Required
//	public void setTxnUserDao(TxnUserDao dao) {
//		this.txnUserDao = dao;
//	}
//	
//	@Required
//	public void setN093Telcomm(TelCommExec telcomm) {		
//		n093Telcomm = telcomm;
//	}
//
//	@Required
//	public void setN093QueryTelcomm(TelCommExec telcomm) {		
//		n093QueryTelcomm = telcomm;
//	}
//	
//	@Required
//	public void setTxnGdRecordDao(TxnGdRecordDao txnGdRecordDao) {
//		this.txnGdRecordDao = txnGdRecordDao;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(final Map params) { 
			Date d = new Date();
			params.put("DATE", DateTimeUtils.getCDateShort(d));
			params.put("TIME", DateTimeUtils.format("HHmmss", d));
			params.put("TRNBDT", DateTimeUtils.getCDateShort(d));		
			params.put("CUSIDN", params.get("UID"));
			params.put("CUSIDN1", params.get("UID"));
			params.put("TRNTYP", "11"); //定期定額
			
			MVHImpl result = null;
			String str_TRNCOD = (String)params.get("TRNCOD");

			//定期定額查詢
			if (str_TRNCOD.equals("00")) {
				
				result = n093QueryTelcomm.query(params);  //查詢
				
				result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
				
			}//end if (str_TRNCOD.equals("00"))
			
			//定期定額變更
			else if (str_TRNCOD.equals("02")) {
				
				//06日投資金額
				if (! params.get("AMT_06_N").equals("")) {			
					params.put("AMT_06", params.get("AMT_06_N"));
				}
				else {
					params.put("AMT_06", "0");
				}

				//16日投資金額
				if (! params.get("AMT_16_N").equals("")) {							
					params.put("AMT_16", params.get("AMT_16_N"));
				}
				else {
					params.put("AMT_16", "0");				
				}

				//26日投資金額
				if (! params.get("AMT_26_N").equals("")) {			
					params.put("AMT_26", params.get("AMT_26_N"));					
				}
				else {
					params.put("AMT_26", "0");				
				}
			
				//06日狀態變更
				if (params.get("PAYSTATUS_06") != null) {
					params.put("FLAG_06", params.get("PAYSTATUS_06"));				
				}
				else {
					params.put("FLAG_06", "");
				}

				//16日狀態變更				
				if (params.get("PAYSTATUS_16") != null) {
					params.put("FLAG_16", params.get("PAYSTATUS_16"));				
				}
				else {
					params.put("FLAG_16", "");
				}

				//26日狀態變更				
				if (params.get("PAYSTATUS_26") != null) {
					params.put("FLAG_26", params.get("PAYSTATUS_26"));				
				}
				else {
					params.put("FLAG_26", "");
				}
				
				//06日是否申購或變更
				if (params.get("DATE_06") != null) {
					//params.put("CHA_06", "Y");				
					params.put("DATE_06", "Y");					
				}
				else {
					//params.put("CHA_06", "N");
					params.put("DATE_06", "N");					
				}								

				//16日是否申購或變更			
				if (params.get("DATE_16") != null) {
					//params.put("CHA_16", "Y");
					params.put("DATE_16", "Y");					
				}
				else {
					//params.put("CHA_16", "N");
					params.put("DATE_16", "N");					
				}								

				//26日是否申購或變更				
				if (params.get("DATE_26") != null) {
					//params.put("CHA_26", "Y");
					params.put("DATE_26", "Y");					
				}
				else {
					//params.put("CHA_26", "N");
					params.put("DATE_26", "N");					
				}															
				
				if("7".equals(params.get("FGTXWAY"))) {  //IDGATE
					params.put("CERTACN", StrUtils.repeat("C", 19));	//即時
				}
				
				//正式交易	
				OnLineTransferNotice trNotice = new OnLineTransferNotice();
				final TelCommExecWrapper execwrapper = new TelCommExecWrapper(n093Telcomm);
				
				result = execwrapper.query(params);
				 
				//紀錄交易 log
				final TXNGDRECORD record = (TXNGDRECORD)writeLog(execwrapper, params, result);		
				
				execwrapper.throwException();
							
				final TelCommExecWrapper execwrapperFinal = execwrapper;
				boolean isUserSetTxnNotify=false;
				List<TXNUSER> listtxnUser;
				listtxnUser = txnUserDao.findByUserId((String)params.get("UID"));
				log.debug("listtxnUser.size() = " + listtxnUser.size());
				if(listtxnUser==null || listtxnUser.size()==0)
				{
					log.debug(ESAPIUtil.vaildLog("User ID not found = " + params.get("UID")));
				}
				else
				{

					TXNUSER txnuser=listtxnUser.get(0);
					String dpnotify = StrUtils.trim(txnuser.getDPNOTIFY());
					Set<String> dpnotifySet = StrUtils.splitToSet("\\s*,\\s*", dpnotify);
				
					isUserSetTxnNotify = dpnotifySet.contains("16");
				}
				
				//寄送交易訊息通知
				if (StrUtils.isNotEmpty((String)params.get("DPMYEMAIL")) && (isUserSetTxnNotify==true)) {			
					
					trNotice.sendNotice(new NoticeInfoGetter() {
						
						public NoticeInfo getNoticeInfo() {
							
							NoticeInfo result = new NoticeInfo((String)params.get("UID"), "", (String)params.get("DPMYEMAIL"));
							result.setTemplateName("GOLD_PERIOD_CHANGE_NOTIFY");
							
							result.setDpretxstatus(record.getGDRETXSTATUS());
							result.setADTXNO(record.getADTXNO());
							
							result.setException(execwrapperFinal.getLastException());
							String datetime = record.getGDTXDATE() + record.getGDTXTIME();
							Date d = DateTimeUtils.parse("yyyyMMddHHmmss", datetime);
							String trantime = DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", d);
											
							result.getParams().put("#TRANTIME", trantime);					
							
							String acn = record.getGDSVAC();
							try {						
								//acn = StrUtils.left(acn, 6) + StrUtils.repeat("*", acn.length() - 9) + StrUtils.right(acn, 3);
								acn = StrUtils.left(acn, acn.length() - 5) + "***" + StrUtils.right(acn, 2);
								log.debug("acn = " + acn);
							}
							catch(Exception e){}
							
							String outacn = record.getGDWDAC();
							try {
								outacn = StrUtils.left(outacn, outacn.length() - 5) + "***" + StrUtils.right(outacn, 2);
								log.debug("outacn = " + outacn);
							}
							catch(Exception e){}
							
							result.getParams().put("#ACN", acn);
							result.getParams().put("#OUTACN", outacn);										
							result.getParams().put("#AMT_06", NumericUtil.formatNumberString((String)params.get("AMT_06"), 0));
							result.getParams().put("#AMT_16", NumericUtil.formatNumberString((String)params.get("AMT_16"), 0));
							result.getParams().put("#AMT_26", NumericUtil.formatNumberString((String)params.get("AMT_26"), 0));
							
							//扣款狀態提示文字
							if (((String)params.get("FLAG_06")).equals("2"))
								result.getParams().put("#STATUS_06", "(暫停扣款)");
							else if (((String)params.get("FLAG_06")).equals("3"))
								result.getParams().put("#STATUS_06", "(終止扣款)");
							else if (((String)params.get("FLAG_06")).equals("4"))
								result.getParams().put("#STATUS_06", "(連續3次扣款失敗終止)");
							else
								result.getParams().put("#STATUS_06", "");
							
							if (((String)params.get("FLAG_16")).equals("2"))
								result.getParams().put("#STATUS_16", "(暫停扣款)");
							else if (((String)params.get("FLAG_16")).equals("3"))
								result.getParams().put("#STATUS_16", "(終止扣款)");
							else if (((String)params.get("FLAG_16")).equals("4"))
								result.getParams().put("#STATUS_16", "(連續3次扣款失敗終止)");
							else
								result.getParams().put("#STATUS_16", "");
							
							if (((String)params.get("FLAG_26")).equals("2"))
								result.getParams().put("#STATUS_26", "(暫停扣款)");
							else if (((String)params.get("FLAG_26")).equals("3"))
								result.getParams().put("#STATUS_26", "(終止扣款)");
							else if (((String)params.get("FLAG_26")).equals("4"))
								result.getParams().put("#STATUS_26", "(連續3次扣款失敗終止)");
							else
								result.getParams().put("#STATUS_26", "");
							
							
							ADMMAILLOG admmaillog = new ADMMAILLOG();
							admmaillog.setADACNO(record.getGDWDAC());
							
							String trans_status = (String)params.get("__TRANS_STATUS");
							if(StrUtils.isEmpty(trans_status)) {
								trans_status = "GDONLINE";
							}
							else if("SEND".equals(trans_status)) {
								trans_status = "GDSCH";
							}
							else if("RESEND".equals(trans_status)) {
								trans_status = "GDSCHRE";
							}
							
							admmaillog.setADBATCHNO(trans_status);
							admmaillog.setADMAILACNO(StrUtils.trim((String)params.get("DPMYEMAIL")));
							admmaillog.setADUSERID(record.getGDUSERID());
							admmaillog.setADUSERNAME("");
							admmaillog.setADSENDTYPE("3");
							admmaillog.setADSENDTIME(datetime);
							admmaillog.setADSENDSTATUS("");
							
							result.setADMMAILLOG(admmaillog);
							
							return result;
						}
						
					});
					
					execwrapper.throwException();
					
				}//end if (DPMYEMAIL)		
												
			}//end else if (str_TRNCOD.equals("02"))
			
			return result;
	}

	@Override
	public Object writeLog(TelCommExecWrapper execwrapper, Map<String, String> params, final MVHImpl result) {
		
		TXNGDRECORD record = execwrapper.createTxnGdRecord();
		
		String date = record.getLASTDATE();
		String time = record.getLASTTIME();
		
		if(result != null) {
			try {
				date = DateTimeUtils.format("yyyyMMdd", DateTimeUtils.getCDate2Date(result.getValueByFieldName("TRNBDT")));
			}
			catch(Exception e){}
			try {
				time = DateTimeUtils.format("HHmmss", new Date());
			}
			catch(Exception e){}				
		}

		record.setGDUSERID(params.get("UID"));
		record.setADOPID(params.get("ADOPID"));
		record.setGDTXDATE(date);
		record.setGDTXTIME(time);
		record.setGDWDAC(params.get("SVACN"));
		record.setGDSVBH("050");
		record.setGDSVAC(params.get("ACN"));
		record.setGDTXAMT((result != null) ? result.getValueByFieldName("AMT_06") : "0");   //06日投資金額
		record.setGDPRICE((result != null) ? result.getValueByFieldName("AMT_16") : "0");    //16日投資金額
		record.setGDDISCOUNT((result != null) ? result.getValueByFieldName("AMT_26") : "0");   //26日投資金額				
		record.setGDFEE1("0"); 	//手續費
		record.setGDFEE2("0");
		record.setGDTXCODE(params.get("FGTXWAY"));
		record.setGDTXMAILS(params.get("DPMYEMAIL"));			
		log.debug(ESAPIUtil.vaildLog("TXNGDRECORD>>>>>{}"+record.toString()));
		txnGdRecordDao.save(record);
//		TXNREQINFO titainfo = null;
//		if(record.getGDTITAINFO() == 0) {
//			titainfo = new TXNREQINFO();
//			titainfo.setREQINFO(BookingUtils.requestInfo(params));
//		}
//
//		txnGdRecordDao.writeTxnRecord(titainfo, record);

		return record;
	}
}
