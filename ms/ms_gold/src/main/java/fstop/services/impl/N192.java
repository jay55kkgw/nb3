package fstop.services.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import fstop.model.GrpColumns;
import fstop.model.MVH;
import fstop.model.MVHUtils;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;

@Transactional
public class N192 extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("n192Telcomm")
	private TelCommExec n192Telcomm;
	
//	@Required
//	public void setN192Telcomm(TelCommExec telcomm) {
//		this.n192Telcomm = telcomm;
//	}
//	
	@Override
	public MVH doAction(Map params) {
		
		String period = (String)params.get("FGPERIOD");
		String enddate = "";
		String stadate = "";
		String periodStr = "";
		String type = "";
		
		if("CMLASTDATE".equals(period)) { //最後一次異動
			periodStr = "最後一次異動";
			type = "1";
		} else if("CMPERIOD".equals(period)) { //指定日期
			stadate = (String)params.get("CMSDATE");
			enddate = (String)params.get("CMEDATE");
			stadate = StrUtils.trim(stadate).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
			enddate = StrUtils.trim(enddate).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
			
			if(stadate != null && stadate.length() == 8) {
				Date sd = DateTimeUtils.parse("yyyyMMdd", stadate);
				stadate = DateTimeUtils.getCDateShort(sd);
				periodStr = DateTimeUtils.format("yyyy/MM/dd", sd);
			}
			if(enddate != null && enddate.length() == 8) {
				Date sd = DateTimeUtils.parse("yyyyMMdd", enddate);
				enddate = DateTimeUtils.getCDateShort(sd);
				periodStr += "~" + DateTimeUtils.format("yyyy/MM/dd", sd);
			}
			else { //若未指定結束時間, 則使用今天日期
				enddate = DateTimeUtils.getCDateShort(new Date());
				periodStr += "~" + DateTimeUtils.format("yyyy/MM/dd", new Date());
			}

			type = "2";			
		}
		
		params.put("CUSIDN", params.get("UID"));
		params.put("TYPE", type);
		params.put("DATE_S", stadate);
		params.put("DATE_E", enddate);
		
		TelcommResult helper = n192Telcomm.query(params);
		helper.getFlatValues().put("CMPERIOD", periodStr);		
		
		return helper;
	}
}
