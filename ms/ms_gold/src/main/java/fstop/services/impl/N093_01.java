package fstop.services.impl;

import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.util.ESAPIUtil;

import fstop.exception.ToRuntimeException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.orm.dao.SysParamDataDao;
import fstop.orm.dao.TxnGdRecordDao;
import fstop.orm.dao.TxnTwRecordDao;
import fstop.orm.po.ADMMAILLOG;
import fstop.orm.po.SYSPARAMDATA;
import fstop.orm.po.TXNGDRECORD;
import fstop.orm.po.TXNREQINFO;
import fstop.orm.po.TXNTWRECORD;
import fstop.services.BookingAware;
import fstop.services.BookingUtils;
import fstop.services.CommonService;
import fstop.services.NoticeInfo;
import fstop.services.NoticeInfoGetter;
import fstop.services.OnLineTransferNotice;
import fstop.services.TelCommExecWrapper;
import fstop.services.WriteLogInterface;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import topibs.utility.NumericUtil;

/**
 *   黃金定期定額申購交易
 */
@Slf4j
public class N093_01 extends CommonService  implements WriteLogInterface {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("n093Telcomm")
	private TelCommExec n093Telcomm;

	@Autowired
	private TxnGdRecordDao txnGdRecordDao;
	
//	@Required
//	public void setN093Telcomm(TelCommExec telcomm) {		
//		n093Telcomm = telcomm;
//	}
//
//	@Required
//	public void setTxnGdRecordDao(TxnGdRecordDao txnGdRecordDao) {
//		this.txnGdRecordDao = txnGdRecordDao;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(final Map params) { 
			Date d = new Date();
			params.put("DATE", DateTimeUtils.getCDateShort(d));
			params.put("TIME", DateTimeUtils.format("HHmmss", d));
			params.put("TRNBDT", DateTimeUtils.getCDateShort(d));		
			params.put("CUSIDN", params.get("UID"));
			params.put("CUSIDN1", params.get("UID"));
			params.put("TRNTYP", "11"); //定期定額
			params.put("TRNCOD", "01"); //申購
//			params.put("SIGN_06", " ");						
//			params.put("SIGN_16", " ");
//			params.put("SIGN_26", " ");
			
			if (! params.get("AMT_06").equals("")) {			
				params.put("DATE_06", "Y");				
			}
			else {
				params.put("DATE_06", "N");
				params.put("AMT_06", "0");
			}

			if (! params.get("AMT_16").equals("")) {			
				params.put("DATE_16", "Y");				
			}
			else {
				params.put("DATE_16", "N");
				params.put("AMT_16", "0");				
			}

			if (! params.get("AMT_26").equals("")) {			
				params.put("DATE_26", "Y");				
			}
			else {
				params.put("DATE_26", "N");
				params.put("AMT_26", "0");				
			}
			
			MVHImpl result = null;
			if("7".equals(params.get("FGTXWAY"))) {  //晶片金融卡
				params.put("CERTACN", StrUtils.repeat("C", 19));	//即時
			}
			//正式交易	
			OnLineTransferNotice trNotice = new OnLineTransferNotice();
			final TelCommExecWrapper execwrapper = new TelCommExecWrapper(n093Telcomm);
			
			result = execwrapper.query(params);
			 
			//紀錄交易 log
			final TXNGDRECORD record = (TXNGDRECORD)writeLog(execwrapper, params, result);		
			
			execwrapper.throwException();
						
			final TelCommExecWrapper execwrapperFinal = execwrapper;

			//寄送交易訊息通知
			if (StrUtils.isNotEmpty((String)params.get("DPMYEMAIL"))) {			
				
				trNotice.sendNotice(new NoticeInfoGetter() {
					
					public NoticeInfo getNoticeInfo() {
						
						NoticeInfo result = new NoticeInfo((String)params.get("UID"), "", (String)params.get("DPMYEMAIL"));
						result.setTemplateName("GOLD_PERIOD_BUY_NOTIFY");
						
						result.setDpretxstatus(record.getGDRETXSTATUS());
						result.setADTXNO(record.getADTXNO());
						
						result.setException(execwrapperFinal.getLastException());
						String datetime = record.getGDTXDATE() + record.getGDTXTIME();
						Date d = DateTimeUtils.parse("yyyyMMddHHmmss", datetime);
						String trantime = DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", d);
										
						result.getParams().put("#TRANTIME", trantime);					
						
						String acn = record.getGDSVAC();
						try {						
							//acn = StrUtils.left(acn, 6) + StrUtils.repeat("*", acn.length() - 9) + StrUtils.right(acn, 3);
							acn = StrUtils.left(acn, acn.length() - 5) + "***" + StrUtils.right(acn, 2);
							log.debug("acn = " + acn);
						}
						catch(Exception e){}
						
						String outacn = record.getGDWDAC();
						try {
							outacn = StrUtils.left(outacn, outacn.length() - 5) + "***" + StrUtils.right(outacn, 2);
							log.debug("outacn = " + outacn);
						}
						catch(Exception e){}
						
						result.getParams().put("#ACN", acn);
						result.getParams().put("#OUTACN", outacn);										
						result.getParams().put("#AMT_06", NumericUtil.formatNumberString((String)params.get("AMT_06"), 0));
						result.getParams().put("#AMT_16", NumericUtil.formatNumberString((String)params.get("AMT_16"), 0));
						result.getParams().put("#AMT_26", NumericUtil.formatNumberString((String)params.get("AMT_26"), 0));
						
						ADMMAILLOG admmaillog = new ADMMAILLOG();
						admmaillog.setADACNO(record.getGDWDAC());
						
						String trans_status = (String)params.get("__TRANS_STATUS");
						if(StrUtils.isEmpty(trans_status)) {
							trans_status = "GDONLINE";
						}
						else if("SEND".equals(trans_status)) {
							trans_status = "GDSCH";
						}
						else if("RESEND".equals(trans_status)) {
							trans_status = "GDSCHRE";
						}
						
						admmaillog.setADBATCHNO(trans_status);
						admmaillog.setADMAILACNO(StrUtils.trim((String)params.get("DPMYEMAIL")));
						admmaillog.setADUSERID(record.getGDUSERID());
						admmaillog.setADUSERNAME("");
						admmaillog.setADSENDTYPE("3");
						admmaillog.setADSENDTIME(datetime);
						admmaillog.setADSENDSTATUS("");
						
						result.setADMMAILLOG(admmaillog);
						
						return result;
					}
					
				});
				
				execwrapper.throwException();
				
			}//end if (DPMYEMAIL)		
			
			return result;
	}

	@Override
	public Object writeLog(TelCommExecWrapper execwrapper, Map<String, String> params, final MVHImpl result) {
		
		TXNGDRECORD record = execwrapper.createTxnGdRecord();
		
		String date = record.getLASTDATE();
		String time = record.getLASTTIME();
		
		if(result != null) {
			try {
				date = DateTimeUtils.format("yyyyMMdd", DateTimeUtils.getCDate2Date(result.getValueByFieldName("TRNBDT")));
			}
			catch(Exception e){}
			try {
				time = DateTimeUtils.format("HHmmss", new Date());
			}
			catch(Exception e){}				
		}

		record.setGDUSERID(params.get("UID"));
		record.setADOPID(params.get("ADOPID"));
		record.setGDTXDATE(date);
		record.setGDTXTIME(time);
		record.setGDWDAC(params.get("SVACN"));
		record.setGDSVBH("050");
		record.setGDSVAC(params.get("ACN"));
		record.setGDTXAMT((result != null) ? result.getValueByFieldName("AMT_06") : "0");   //06日投資金額
		record.setGDPRICE((result != null) ? result.getValueByFieldName("AMT_16") : "0");    //16日投資金額
		record.setGDDISCOUNT((result != null) ? result.getValueByFieldName("AMT_26") : "0");   //26日投資金額				
		record.setGDFEE1("0"); 	//手續費
		record.setGDFEE2("0");
		record.setGDTXCODE(params.get("FGTXWAY"));
		record.setGDTXMAILS(params.get("DPMYEMAIL"));			
		log.debug(ESAPIUtil.vaildLog("TXNGDRECORD>>>>>{}"+record.toString()));
		txnGdRecordDao.save(record);
//		TXNREQINFO titainfo = null;
//		if(record.getGDTITAINFO() == 0) {
//			titainfo = new TXNREQINFO();
//			titainfo.setREQINFO(BookingUtils.requestInfo(params));
//		}
//
//		txnGdRecordDao.writeTxnRecord(titainfo, record);

		return record;
	}
}
