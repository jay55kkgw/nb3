package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.TxnGdRecordDao;
import fstop.orm.po.TXNGDRECORD;
import fstop.orm.po.TXNREQINFO;
import fstop.services.BookingUtils;
import fstop.services.CommonService;
import fstop.services.OnLineTransferNotice;
import fstop.services.TelCommExecWrapper;
import fstop.services.WriteLogInterface;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import topibs.utility.NumericUtil;

/**
 *   預約黃金回售交易
 */
@Slf4j
public class N091_02 extends CommonService  implements WriteLogInterface {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("n091Telcomm")
	private TelCommExec n091Telcomm;

	@Autowired
	private TxnGdRecordDao txnGdRecordDao;
	
//	@Required
//	public void setN091Telcomm(TelCommExec telcomm) {		
//		n091Telcomm = telcomm;
//	}
//
//	@Required
//	public void setTxnGdRecordDao(TxnGdRecordDao txnGdRecordDao) {
//		this.txnGdRecordDao = txnGdRecordDao;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(final Map params) { 
			Date d = new Date();
			params.put("DATE", DateTimeUtils.getCDateShort(d));
			params.put("TIME", DateTimeUtils.format("HHmmss", d));
			params.put("TRNBDT", DateTimeUtils.getCDateShort(d));		
			params.put("CUSIDN", params.get("UID"));
			params.put("TRNTYP", "03"); //預約
			params.put("APPTYPE", "02"); //回售	
			params.put("SIGN", " ");												
			if("7".equals(params.get("FGTXWAY"))) {  //idgate
				params.put("CERTACN", StrUtils.repeat("D", 19));	//預約
			}
			
			MVHImpl result = null;
		
			//正式交易	
			OnLineTransferNotice trNotice = new OnLineTransferNotice();
			final TelCommExecWrapper execwrapper = new TelCommExecWrapper(n091Telcomm);
			
			result = execwrapper.query(params);
			 
			//紀錄交易 log
			TXNGDRECORD record = (TXNGDRECORD)writeLog(execwrapper, params, result);		
			
			execwrapper.throwException();
						
			return result;
	}

	@Override
	public Object writeLog(TelCommExecWrapper execwrapper, Map<String, String> params, final MVHImpl result) {
		
		TXNGDRECORD record = execwrapper.createTxnGdRecord();
		
		String date = record.getLASTDATE();
		String time = record.getLASTTIME();
		
		if(result != null) {
			try {
				date = DateTimeUtils.format("yyyyMMdd", DateTimeUtils.getCDate2Date(result.getValueByFieldName("TRNBDT")));
			}
			catch(Exception e){}
			try {
				time = DateTimeUtils.format("HHmmss", new Date());
			}
			catch(Exception e){}				
		}

		record.setGDUSERID(params.get("UID"));
		record.setADOPID(params.get("ADOPID"));
		record.setGDTXDATE(date);
		record.setGDTXTIME(time);
		record.setGDWDAC(params.get("SVACN"));
		record.setGDSVBH("050");
		record.setGDSVAC(params.get("ACN"));
		record.setGDTXAMT((result != null) ? result.getValueByFieldName("TRNAMT") : "0");   //總扣款金額
		record.setGDWEIGHT(params.get("TRNGD"));   //交易公克數	
		record.setGDPRICE((result != null) ? result.getValueByFieldName("PRICE") : "0");    //黃金牌告單價
		record.setGDDISCOUNT((result != null) ? result.getValueByFieldName("DISPRICE") : "0");   //折讓後單價				
		record.setGDFEE1((result != null) ? NumericUtil.formatNumberString(result.getValueByFieldName("TRNFEE"), 0): "0"); 	//手續費
		record.setGDFEE2("0");
		record.setGDTXCODE(params.get("FGTXWAY"));
		record.setGDTXMAILS(params.get("DPMYEMAIL"));			
		
//		TXNREQINFO titainfo = null;
//		if(record.getGDTITAINFO() == 0) {
//			titainfo = new TXNREQINFO();
//			titainfo.setREQINFO(BookingUtils.requestInfo(params));
//		}
//
//		txnGdRecordDao.writeTxnRecord(titainfo, record);
		txnGdRecordDao.save(record);
		
		return record;
	}
}
