package fstop.services.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.rest.model.CommonPools;
import com.netbank.util.ESAPIUtil;

import topibs.utility.NumericUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.orm.dao.TxnReqInfoDao;
import fstop.orm.dao.TxnTwRecordDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.ADMMAILLOG;
import fstop.orm.po.TXNREQINFO;
import fstop.orm.po.TXNTWRECORD;
import fstop.orm.po.TXNUSER;
import fstop.services.BookingUtils;
import fstop.services.CommonService;
import fstop.services.DispatchService;
import fstop.services.NoticeInfo;
import fstop.services.NoticeInfoGetter;
import fstop.services.OnLineTransferNotice;
import fstop.services.ServiceBindingException;
import fstop.services.GoldWebServiceNotice;
import fstop.services.TelCommExecWrapper;
import fstop.services.WriteLogInterface;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import fstop.ws.AMLWebServiceTemplate;
import fstop.ws.aml.bean.AML001RQ;
import fstop.ws.aml.bean.AML001RQ.SearchNamesSoap;
import fstop.ws.aml.bean.AML001RQ.SearchNamesSoap.Branch;
import fstop.ws.aml.bean.AML001RQ.SearchNamesSoap.DateOfBirth;
import fstop.ws.aml.bean.AML001RQ.SearchNamesSoap.Names;
import fstop.ws.aml.client.SearchResponseSoap;
import lombok.extern.slf4j.Slf4j;

/**
 * 申請黃金存摺帳戶
 * @author Owner
 *
 */
@Slf4j
public class NA50 extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("na50Telcomm")
	private TelCommExec na50Telcomm;

	@Autowired
    @Qualifier("a105Telcomm")
	private TelCommExec a105Telcomm;

	@Autowired
	private AMLWebServiceTemplate amlWebServiceTemplate;
	
	@Autowired
	private CommonPools commonPools;
//	@Required
//	public void setNa50Telcomm(TelCommExec telcomm) {
//		na50Telcomm = telcomm;
//	}
	
	/**
	 * 發送申請黃金存摺帳戶電文
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params) {
		final Map<String, String> params = _params;
		
		MVHImpl result = null;
        MVHImpl result1 = null;
        
//		if(getAmlmsg(params)) {
//		    result = new MVHImpl();
//			result.getFlatValues().put("occurMsg", "Z616");
//		    return result;
//		}
        
        //自網頁取得UID及FGTXWAY資訊
		
		String uid = params.get("UID").toString();
		String fgtxway = params.get("FGTXWAY").toString();

        //獲取服務器端"當前日期"

		try
		{
			Date d = new Date();
	        //獲取日期的"年月日"
			String df = DateTimeUtils.getCDateShort(d);
	        //獲取日期的"時分秒"
			String tf = DateTimeUtils.format("HHmmss", d);
	        //輸入日期的"年月日"(電文)
			params.put("DATE", df);
	        //輸入日期的"時分秒"(電文)
			params.put("TIME", tf);
			if (fgtxway.equals("1"))
			{//電子簽章
				params.put("KIND", "2");
			}
			else
			{//晶片金融卡
				params.put("KIND", "1");
				params.put("ICSEQ", params.get("icSeq"));
				
				String chip_acn = params.get("accNo").trim();
				params.put("CHIP_ACN", chip_acn);
			}
	        //輸入統一編號(電文)
			params.put("CUSIDN", uid);
	        //輸入交易日期(電文)
			params.put("TRNBDT", df);
	        //輸入交易種類(電文)
			params.put("TRNTYP", "10");
	
	        //取得網頁上的出生年月日
			String cmdate1 = (String)params.get("CMDATE1");
	        //取得網頁上的身分證補發/換發/新發日期
			String cmdate2 = (String)params.get("CMDATE2");
			cmdate1 = StrUtils.trim(cmdate1).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
			cmdate2 = StrUtils.trim(cmdate2).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
			if(cmdate1 != null && cmdate1.length() == 8) {
				Date d1 = DateTimeUtils.parse("yyyyMMdd", cmdate1);
				cmdate1 = DateTimeUtils.getCDateShort(d1);
			}
			if(cmdate2 != null && cmdate2.length() == 8) {
				Date d2 = DateTimeUtils.parse("yyyyMMdd", cmdate2);
				cmdate2 = DateTimeUtils.getCDateShort(d2);
			}
	        //輸入出生年月日(電文)
			params.put("BRTHDY", cmdate1);
	        //輸入身分證補/換/新發日期(電文)
			params.put("DATECHA", cmdate2);
			
			//身份驗證
	        //輸入申請作業類別(電文)
			params.put("TYPE", "01");
	        //輸入交易註記碼(電文)
			params.put("TRNCOD", "00");	//查詢900-03
			result = na50Telcomm.query(params);
			
			//確認申請
	        //輸入申請作業類別(電文)
			params.put("TYPE", "02");
	        //輸入交易註記碼(電文)
			params.put("TRNCOD", "01");	//正式開戶
			result = na50Telcomm.query(params);
	
		}catch(TopMessageException e){
			log.warn("NA50.java na50 fail MSGCOD:"+ e.getMessage());
			throw e;
		}
		if(!result.getFlatValues().get("MSGCOD").equals("0000")) {
			return result;
		}
		
		//申請成功時發送Mail通知
		try
		{
			final TxnUserDao txnUserDao = (TxnUserDao)SpringBeanFactory.getBean("txnUserDao");
			TXNUSER txnUser = txnUserDao.findById(uid);
			final String dpmyemail = StrUtils.trim(txnUser.getDPMYEMAIL());
			if (StrUtils.isEmpty(dpmyemail))
				return result;
			
			final String resultGdacn =  result.getValueByFieldName("GDACN");
			final String resultFee =  result.getValueByFieldName("AMT_FEE");
			final String resultBrhnum =  result.getValueByFieldName("BRHNUM");
			final String resultSvacn =  params.get("SVACN");
			
			GoldWebServiceNotice trNotice = new GoldWebServiceNotice();
			trNotice.sendNotice(new NoticeInfoGetter() {
	
				public NoticeInfo getNoticeInfo() {
					NoticeInfo noticeResult = new NoticeInfo("", "", dpmyemail);
										
					noticeResult.setTemplateName("GOLD_APPLY_ACNO");
					
					Date d = new Date();
					String df = DateTimeUtils.format("yyyyMMdd", d);
					String tf = DateTimeUtils.format("HHmmss", d);
					String dateTime = DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", d);
					
					noticeResult.getParams().put("#TRANTIME", dateTime);
					noticeResult.getParams().put("#GDACN", StrUtils.left(resultGdacn, 6) + StrUtils.repeat("*", resultGdacn.length() - 9) + StrUtils.right(resultGdacn, 3));
					noticeResult.getParams().put("#AMT_FEE", resultFee);
					noticeResult.getParams().put("#BRHNUM", resultBrhnum);
					noticeResult.getParams().put("#SVACN", StrUtils.left(resultSvacn, resultSvacn.length() - 5) + "***" + StrUtils.right(resultSvacn, 2));
					
					ADMMAILLOG admmaillog = new ADMMAILLOG();
										
					admmaillog.setADBATCHNO("NA50");
					admmaillog.setADMAILACNO(dpmyemail);
					admmaillog.setADUSERID("SYS");
					admmaillog.setADUSERNAME("SYS");
					admmaillog.setADSENDTYPE("5");
					admmaillog.setADSENDTIME(df + tf);
					admmaillog.setADSENDSTATUS("");
					
					noticeResult.setADMMAILLOG(admmaillog);
					
					return noticeResult;
				}
				
			});
		}
		catch (Exception e)
		{
			log.error("發送 Email 失敗.", e);
		}

		String employer = convertToFullWidth(params.get("EMPLOYER").toString());
		try{
			log.warn(ESAPIUtil.vaildLog("NA50.java A105 CUSIDN:"+params.get("CUSIDN").toString()));
			
			//輸入類別(電文)
			params.put("TYPE", "U");
			log.warn(ESAPIUtil.vaildLog("NA50.java A105 TYPE:"+params.get("TYPE").toString()));
			
			//輸入金額種類(電文)
			params.put("CODE", "G");
			log.warn(ESAPIUtil.vaildLog("NA50.java A105 CODE:"+params.get("CODE").toString()));
			
			//輸入預期往來金額(電文)
			params.put("SAMT", params.get("S_MONEY").toString());
			log.warn(ESAPIUtil.vaildLog("NA50.java A105 SAMT:"+params.get("SAMT").toString()));
			
			log.warn(ESAPIUtil.vaildLog("NA50.java A105 RENAME:"+params.get("RENAME").toString()));
			
			//輸入黃金存摺帳號(電文)
			params.put("GDACN", result.getValueByFieldName("GDACN").toString());
			log.warn(ESAPIUtil.vaildLog("NA50.java A105 GDACN:"+params.get("GDACN").toString()));
			
			log.warn(ESAPIUtil.vaildLog("NA50.java A105 PURPOSE:"+params.get("PURPOSE").toString()));
			
			//輸入任職機構(電文)
			params.put("CONPNAM", employer);
			log.warn(ESAPIUtil.vaildLog("NA50.java A105 CONPNAM:"+params.get("CONPNAM").toString()));
			
			result1 = a105Telcomm.query(params);
		}catch(TopMessageException e){
			log.warn("NA50.java a105 fail MSGCOD:"+ e.getMessage());
		}
		result.addTable(result1);
		return result;
	}
	
	public static String convertToFullWidth(String input) { 
		if (input==null)
		{
			return null;
		}
		
		char[] charArray = input.toCharArray(); 
		for ( int i = 0 ; i < charArray.length; i ++ ) { 
			int ic =(int) charArray[i];
			if (ic>= 33 && ic<=126) { 
				charArray[i] =(char)(ic+65248); 
			} else if (ic == 32) { 
				charArray[i] = ( char )12288; 

			}
		} 
		return new String(charArray); 
	} 
	public boolean getAmlmsg(Map reqParam) {
		try{
			String BRTHDY = ((String)reqParam.get("CMDATE1")).replaceAll("/", "");
			String year = BRTHDY.substring(0,4);
			String month = BRTHDY.substring(4,6);
			String day = BRTHDY.substring(6,8);
			String CUSIDN = (String)reqParam.get("UID");
			String branchId = "";
			if(((String)reqParam.get("SVACN")).length() > 3) {
				branchId = ((String)reqParam.get("SVACN")).substring(0, 3);
			}  		
			TXNUSER user = null;
			try {
				final TxnUserDao txnUserDao = (TxnUserDao)SpringBeanFactory.getBean("txnUserDao");
				TXNUSER txnUser = txnUserDao.findById(CUSIDN);		
				if(user==null){
					log.warn("User ID not found = " + CUSIDN);
				}
				else{
					if(branchId.length()==0){
						branchId = StrUtils.trim(user.getADBRANCHID());
					}
				}								
			}
			catch (Exception e) {
				log.warn("TXNUSER EXCEPTION:" + e.getMessage());	
			}
	
			String name = "";
			try {
				commonPools.n960.removeGetDataCache(CUSIDN);
				MVH n960 = commonPools.n960.getData1(CUSIDN);   //AML取得中心基本資料
				if(n960.getValueByFieldName("NAME")!=null)
				{
					name = n960.getValueByFieldName("NAME");
				}	
			}
			catch (TopMessageException e) {
				log.warn("N960 ERROR EXCEPTION:"+e.getMessage());
			}
			
			
			AML001RQ rq  = new AML001RQ();
			SearchNamesSoap  searchNamesSoap = new SearchNamesSoap();
			Names names = new Names();
			LinkedList list = new LinkedList<String>();
			list.add(name);
			names.setName(list);
			Branch branch = new Branch();
			branch.setBranchId(branchId);
			branch.setBusinessUnit("H57");
			branch.setSourceSystem("NNB-W100089655");
			DateOfBirth dateOfBirth = new DateOfBirth();
			dateOfBirth.setYear(year);
			dateOfBirth.setMonth(month);
			dateOfBirth.setDay(day);
			searchNamesSoap.setNames(names);
			searchNamesSoap.setBranch(branch);
			searchNamesSoap.setDateOfBirth(dateOfBirth);
			rq.setMSGNAME("AML001");
			rq.setAPP("XML");
			rq.setCLIENTIP("::1");
			rq.setTYPE("01");
			rq.setSearchNamesSoap(searchNamesSoap);
			
			SearchResponseSoap rs = amlWebServiceTemplate.sendAndReceive (rq);
			return rs.isHit();
		}catch(Exception e) {
			log.debug(e.toString());
		}
		return true;
		
	} 
}
