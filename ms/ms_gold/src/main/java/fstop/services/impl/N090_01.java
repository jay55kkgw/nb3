package fstop.services.impl;

import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import fstop.exception.ToRuntimeException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.orm.dao.SysParamDataDao;
import fstop.orm.dao.TxnGdRecordDao;
import fstop.orm.dao.TxnTwRecordDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.ADMMAILLOG;
import fstop.orm.po.SYSPARAMDATA;
import fstop.orm.po.TXNGDRECORD;
import fstop.orm.po.TXNREQINFO;
import fstop.orm.po.TXNTWRECORD;
import fstop.orm.po.TXNUSER;
import fstop.services.BookingAware;
import fstop.services.BookingUtils;
import fstop.services.CommonService;
import fstop.services.NoticeInfo;
import fstop.services.NoticeInfoGetter;
import fstop.services.OnLineTransferNotice;
import fstop.services.TelCommExecWrapper;
import fstop.services.WriteLogInterface;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import fstop.util.thread.WebServletUtils;
import lombok.extern.slf4j.Slf4j;
import topibs.utility.NumericUtil;
import com.netbank.rest.model.CommonPools;
//TODO:JSPUtils的使用有待商確
import com.netbank.rest.model.JSPUtils;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

/**
 *   黃金買進交易
 */
@Slf4j
public class N090_01 extends CommonService  implements WriteLogInterface {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	private TxnUserDao txnUserDao;

	@Autowired
    @Qualifier("n09001_1Telcomm")
	private TelCommExec n09001_1Telcomm;

	@Autowired
    @Qualifier("n09001_2Telcomm")
	private TelCommExec n09001_2Telcomm;

	@Autowired
	private TxnGdRecordDao txnGdRecordDao;

	@Autowired
	private SysParamDataDao sysParamDataDao;
	
	@Autowired
	private CommonPools commonPools;
	
//	@Required
//	public void setTxnUserDao(TxnUserDao dao) {
//		this.txnUserDao = dao;
//	}
//	
//	@Required
//	public void setSysParamDataDao(SysParamDataDao sysParamDataDao) {
//		this.sysParamDataDao = sysParamDataDao;
//	}	
//	
//	@Required
//	public void setN09001_1Telcomm(TelCommExec telcomm) {		
//		n09001_1Telcomm = telcomm;
//	}
//
//	@Required
//	public void setN09001_2Telcomm(TelCommExec telcomm) {		
//		n09001_2Telcomm = telcomm;
//	}
//	
//	@Required
//	public void setTxnGdRecordDao(TxnGdRecordDao txnGdRecordDao) {
//		this.txnGdRecordDao = txnGdRecordDao;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(final Map params) { 
		Date d = new Date();
		params.put("DATE", DateTimeUtils.getCDateShort(d));
		params.put("TIME", DateTimeUtils.format("HHmmss", d));
		params.put("TRNBDT", DateTimeUtils.getCDateShort(d));		
		params.put("CUSIDN", params.get("UID"));

		MVHImpl result = null;
		String str_TRNCOD = (String)params.get("TRNCOD");
		
		if (str_TRNCOD.equals("01")) {					
			params.put("TRNAMT_SIGN", " ");
			params.put("TRNAMT", "0");			
			log.debug(ESAPIUtil.vaildLog("params={}"+CodeUtil.toJson(params)));
			
			result = n09001_1Telcomm.query(params);  //查詢單價
			
			result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));			
		}
		else {					
			String str_TRNFEE = (String)params.get("TRNFEE");
			String str_TRNAMT = (String)params.get("TRNAMT");
			String str_DISAMT = (String)params.get("DISAMT");						
			
			if ((str_TRNFEE.indexOf("+") != -1) || (str_TRNFEE.indexOf("-") != -1)) {
				params.put("TRNFEE_SIGN", str_TRNFEE.substring(0,1).equals("+") ? " " : "-");
				params.put("TRNFEE", str_TRNFEE.substring(1));				
			}
			
			if ((str_TRNAMT.indexOf("+") != -1) || (str_TRNAMT.indexOf("-") != -1)) {
				params.put("TRNAMT_SIGN", str_TRNAMT.substring(0,1).equals("+") ? " " : "-");
				params.put("TRNAMT", str_TRNAMT.substring(1));			
			}
			else {
				params.put("TRNAMT_SIGN", " ");			
			}

			if ((str_DISAMT.indexOf("+") != -1) || (str_DISAMT.indexOf("-") != -1)) {
				params.put("DISAMT_SIGN", str_DISAMT.substring(0,1).equals("+") ? " " : "-");
				params.put("DISAMT", str_DISAMT.substring(1));				
			}
						
			
			String str_PRICE = (String)params.get("PRICE");
			String str_DISPRICE = (String)params.get("DISPRICE");
			String str_PERDIS = (String)params.get("PERDIS");
			
			try {
				params.put("PRICE", NumericUtil.formatDouble(Double.parseDouble(str_PRICE) / 100, 2));
				params.put("DISPRICE", NumericUtil.formatDouble(Double.parseDouble(str_DISPRICE) / 100, 2));
				params.put("PERDIS", NumericUtil.formatDouble(Double.parseDouble(str_PERDIS) / 100, 2));				
			}
			catch (Exception e) {
				log.error("doAction error>>{}", e);
			}						
			
			if("7".equals(params.get("FGTXWAY"))) {  //idgate
				params.put("CERTACN", StrUtils.repeat("C", 19));	//即時
			}

			
			//正式交易	
			OnLineTransferNotice trNotice = new OnLineTransferNotice();
			final TelCommExecWrapper execwrapper = new TelCommExecWrapper(n09001_2Telcomm);
			
			result = execwrapper.query(params);
			 
			//紀錄交易 log
			final TXNGDRECORD record = (TXNGDRECORD)writeLog(execwrapper, params, result);
			
			execwrapper.throwException();
						
			final TelCommExecWrapper execwrapperFinal = execwrapper;

			boolean isUserSetTxnNotify=false;
			List<TXNUSER> listtxnUser;
			listtxnUser = txnUserDao.findByUserId((String)params.get("UID"));
			log.debug("listtxnUser.size() = " + listtxnUser.size());
			if(listtxnUser==null || listtxnUser.size()==0)
			{
				log.debug(ESAPIUtil.vaildLog("User ID not found = " + params.get("UID")));
			}
			else
			{

				TXNUSER txnuser=listtxnUser.get(0);
				String dpnotify = StrUtils.trim(txnuser.getDPNOTIFY());
				Set<String> dpnotifySet = StrUtils.splitToSet("\\s*,\\s*", dpnotify);
			
				isUserSetTxnNotify = dpnotifySet.contains("16");
			}
			//寄送交易訊息通知
			if (StrUtils.isNotEmpty((String)params.get("DPMYEMAIL")) && (isUserSetTxnNotify==true)) {			
				
				trNotice.sendNotice(new NoticeInfoGetter() {
					
					public NoticeInfo getNoticeInfo() {
						
						NoticeInfo result = new NoticeInfo((String)params.get("UID"), "", (String)params.get("DPMYEMAIL"));
						result.setTemplateName("GOLD_BUY_NOTIFY");
						
						result.setDpretxstatus(record.getGDRETXSTATUS());
						result.setADTXNO(record.getADTXNO());
						
						result.setException(execwrapperFinal.getLastException());
						String datetime = record.getGDTXDATE() + record.getGDTXTIME();
						Date d = DateTimeUtils.parse("yyyyMMddHHmmss", datetime);
						String trantime = DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", d);
										
						result.getParams().put("#TRANTIME", trantime);					
						
						String acn = record.getGDSVAC();
						try {						
							//acn = StrUtils.left(acn, 6) + StrUtils.repeat("*", acn.length() - 9) + StrUtils.right(acn, 3);
							acn = StrUtils.left(acn, acn.length() - 5) + "***" + StrUtils.right(acn, 2);
							log.debug("acn = " + acn);
						}
						catch(Exception e){}
						
						String outacn = record.getGDWDAC();
						try {
							outacn = StrUtils.left(outacn, outacn.length() - 5) + "***" + StrUtils.right(outacn, 2);
							log.debug("outacn = " + outacn);
						}
						catch(Exception e){}
						
						result.getParams().put("#ACN", acn);
						result.getParams().put("#OUTACN", outacn);										
						result.getParams().put("#TRNGD", (record.getGDWEIGHT().indexOf(".") != -1) ? 
								                         NumericUtil.formatNumberString(record.getGDWEIGHT(), 2) : 
								                         NumericUtil.formatNumberString(record.getGDWEIGHT(), 0));				
						
						result.getParams().put("#PRICE", NumericUtil.formatNumberString((String)params.get("PRICE"), 2));
						result.getParams().put("#DISPRICE", NumericUtil.formatNumberString((String)params.get("DISPRICE"), 2));
						result.getParams().put("#PERDIS", NumericUtil.formatNumberString((String)params.get("PERDIS"), 2) + " %");
						result.getParams().put("#TRNFEE", record.getGDFEE1());
						result.getParams().put("#TRNAMT", NumericUtil.formatNumberString(record.getGDTXAMT(), 0));
						
						
						ADMMAILLOG admmaillog = new ADMMAILLOG();
						admmaillog.setADACNO(record.getGDWDAC());
						
						String trans_status = (String)params.get("__TRANS_STATUS");
						if(StrUtils.isEmpty(trans_status)) {
							trans_status = "GDONLINE";
						}
						else if("SEND".equals(trans_status)) {
							trans_status = "GDSCH";
						}
						else if("RESEND".equals(trans_status)) {
							trans_status = "GDSCHRE";
						}
						
						admmaillog.setADBATCHNO(trans_status);
						admmaillog.setADMAILACNO(StrUtils.trim((String)params.get("DPMYEMAIL")));
						admmaillog.setADUSERID(record.getGDUSERID());
						admmaillog.setADUSERNAME("");
						admmaillog.setADSENDTYPE("3");
						admmaillog.setADSENDTIME(datetime);
						admmaillog.setADSENDSTATUS("");
						
						result.setADMMAILLOG(admmaillog);
						
						return result;
					}
					
				});
				
				execwrapper.throwException();
				
			}//end if (DPMYEMAIL)		
			
			
			//單筆買進數量3000公克(含)以上，應發email通知台銀bot233@mail.bot.com.tw
		    //及本行人員p070449@mail.tbb.com.tw、dailan@mail.tbb.com.tw
			double d_TRNGD = Double.parseDouble((String)params.get("TRNGD"));			
			if (d_TRNGD >= 3000) {			
				
				trNotice.sendNotice(new NoticeInfoGetter() {
					
					public NoticeInfo getNoticeInfo() {
												
						boolean isExists = false;
						SYSPARAMDATA po = null;
						try {
							po = sysParamDataDao.findById("NBSYS");
							isExists = true;
						}
						catch(ObjectNotFoundException e) {
							isExists = false;
						}
						
						if(!isExists) {
							po = new SYSPARAMDATA();
							po.setADPK("NBSYS");
						}
												
						String str_Receiver = po.getADGDTXNMAIL();
						NoticeInfo result = new NoticeInfo((String)params.get("UID"), "", str_Receiver);
						result.setTemplateName("GOLD_NOTIFY_3KG");
						
						result.setDpretxstatus(record.getGDRETXSTATUS());
						result.setADTXNO(record.getADTXNO());
						
						result.setException(execwrapperFinal.getLastException());
						String datetime = record.getGDTXDATE() + record.getGDTXTIME();
						Date d = DateTimeUtils.parse("yyyyMMddHHmmss", datetime);
						String trantime = DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", d);																
						
						result.getParams().put("#TRANTYPE", "黃金存摺銷售");
						result.getParams().put("#TRANTIME", trantime);																				
						result.getParams().put("#TRNGD", (record.getGDWEIGHT().indexOf(".") != -1) ? 
		                         NumericUtil.formatNumberString(record.getGDWEIGHT(), 2) : 
		                         NumericUtil.formatNumberString(record.getGDWEIGHT(), 0));					
			
						result.getParams().put("#PRICE", NumericUtil.formatNumberString((String)params.get("PRICE"), 2));
						result.getParams().put("#DISPRICE", NumericUtil.formatNumberString((String)params.get("DISPRICE"), 2));
						
						
						ADMMAILLOG admmaillog = new ADMMAILLOG();
						admmaillog.setADACNO(record.getGDWDAC());
						
						String trans_status = (String)params.get("__TRANS_STATUS");
						if(StrUtils.isEmpty(trans_status)) {
							trans_status = "GDONLINE";
						}
						else if("SEND".equals(trans_status)) {
							trans_status = "GDSCH";
						}
						else if("RESEND".equals(trans_status)) {
							trans_status = "GDSCHRE";
						}
						
						admmaillog.setADBATCHNO(trans_status);
						admmaillog.setADMAILACNO(str_Receiver);
						admmaillog.setADUSERID(record.getGDUSERID());
						admmaillog.setADUSERNAME("");
						admmaillog.setADSENDTYPE("3");
						admmaillog.setADSENDTIME(datetime);
						admmaillog.setADSENDSTATUS("");
						
						result.setADMMAILLOG(admmaillog);
						
						return result;
					}
					
				});
				
				execwrapper.throwException();
				
			}//end if (d_TRNGD >= 3000)		
			

			/*** 交易成功後,產生交易單據 ***/			
			HttpServletRequest request = WebServletUtils.getRequest();
			HttpServletResponse response = WebServletUtils.getResponse();			
			
			result.getFlatValues().put("ADTXNO", record.getADTXNO());	
			result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
			request.setAttribute("PreviewHelper", result);		
			
//			try {
//				String str_ResultHtml = JSPUtils.toString2("gold/N09001CertPreview.jsp", request, response);	
//				commonPools.goldUtils.updateGDCERT(record.getADTXNO(), str_ResultHtml);
//			}
//			catch(Exception e) {
//				throw new ToRuntimeException("黃金買進:產生交易單據發生錯誤.", e);
//			}			
			
		}//end else		
		
		return result;
	}

	@Override
	public Object writeLog(TelCommExecWrapper execwrapper, Map<String, String> params, final MVHImpl result) {
		
		TXNGDRECORD record = execwrapper.createTxnGdRecord();
		
		String date = record.getLASTDATE();
		String time = record.getLASTTIME();
		
		if(result != null) {
			try {
				date = DateTimeUtils.format("yyyyMMdd", DateTimeUtils.getCDate2Date(result.getValueByFieldName("TRNBDT")));
			}
			catch(Exception e){}
			try {
				time = DateTimeUtils.format("HHmmss", new Date());
			}
			catch(Exception e){}				
		}

		record.setGDUSERID(params.get("UID"));
		record.setADOPID(params.get("ADOPID"));
		record.setGDTXDATE(date);
		record.setGDTXTIME(time);
		record.setGDWDAC(params.get("SVACN"));
		record.setGDSVBH("050");
		record.setGDSVAC(params.get("ACN"));
		record.setGDTXAMT(params.get("TRNAMT"));   //總扣款金額
		record.setGDWEIGHT(params.get("TRNGD"));   //交易公克數	
		record.setGDPRICE(params.get("PRICE"));    //黃金牌告單價
		record.setGDDISCOUNT(params.get("DISPRICE"));   //折讓後單價				
		record.setGDFEE1(NumericUtil.formatNumberString((String)params.get("TRNFEE"), 0)); 	//手續費
		record.setGDFEE2("0");
		record.setGDTXCODE(params.get("FGTXWAY"));
		record.setGDTXMAILS(params.get("DPMYEMAIL"));			
		
//		TXNREQINFO titainfo = null;
//		if(record.getGDTITAINFO() == 0) {
//			titainfo = new TXNREQINFO();
//			titainfo.setREQINFO(BookingUtils.requestInfo(params));
//		}

//		txnGdRecordDao.writeTxnRecord(titainfo, record);
		txnGdRecordDao.save(record);
		
		
		return record;
	}
}
