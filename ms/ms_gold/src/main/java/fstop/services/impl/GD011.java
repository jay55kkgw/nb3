package fstop.services.impl;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.owasp.esapi.ESAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import fstop.model.DBResult;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.GoldPriceDao;
import fstop.orm.po.GOLDPRICE;
import fstop.services.CommonService;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;
import topibs.utility.DateUtil;
import topibs.utility.Machine;
import topibs.utility.TopLinkedHashMap;

@Slf4j
public class GD011 extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	private GoldPriceDao goldPriceDao;
	//private Vector vec = new Vector();
	private HashSet duphs = new HashSet();
//	@Required
//	public void setGoldPriceDao(GoldPriceDao goldPriceDao) {
//		this.goldPriceDao = goldPriceDao;
//	}


	@Override
	public MVH doAction(Map _params) {
		Map<String, String> params = _params;
		String querytype = params.get("QUERYTYPE");
		String periodfrom = "";//有指定查詢期間才要帶入起迄時間
		String periodto = "";
		if(null != querytype)
		{
			if(querytype.equals("PERIOD"))
			{
				periodfrom = params.get("CMSDATE");
				periodto = params.get("CMEDATE");
				periodfrom = ESAPI.encoder().encodeForHTML(DateUtil.getTaiwanDate(periodfrom.replaceAll("/","")));
				periodto   = ESAPI.encoder().encodeForHTML(DateUtil.getTaiwanDate(periodto.replaceAll("/","")));
			}
			if(querytype.equals("SOMEDAY"))
			{
				periodfrom = params.get("DATE");
			}
			if(querytype.equals("TODAYLAST"))
			{
				periodfrom = getToday();
			}
			if(querytype.equals("TODAY"))
			{
				periodfrom = getToday();
			}
			if(querytype.equals("LASTDAY"))
			{
				;
			}
			if(querytype.equals("LASTMON"))
			{
				String taiwandate = getToday();
				String year = taiwandate.substring(0,3);
				String mon = taiwandate.substring(3,5);
				String day = taiwandate.substring(5,7);
				String dayArr[] = {"31","28","31","30","31","30","31","31","30","31","30","31"};
				
				GregorianCalendar gCal = new GregorianCalendar();				
				if (gCal.isLeapYear(Integer.parseInt(year)+1911))						
					dayArr[1] = "29";
								
				int yeartoInt = Integer.parseInt(year);
				int montoInt = Integer.parseInt(mon);
				int dayInt = Integer.parseInt(day);
				int addMon = 0;
				if(montoInt == 1) 
				{
					montoInt = 12;
					yeartoInt = yeartoInt -1;
				}
				else montoInt = montoInt -1;
				log.debug("LASTMON=montoInt="+montoInt);
				log.debug("LASTMON=yeartoInt="+yeartoInt);
				if((dayInt +1 ) > Integer.parseInt(dayArr[montoInt -1]))
				{
					addMon = 1;
					day = "01";
				}else
				{
					if(dayInt+1 <10)
						day = "0"+Integer.toString(dayInt+1);
					else
						day = Integer.toString(dayInt+1);
				}
				log.debug("LASTMON=addMon="+addMon);
				log.debug("LASTMON=day="+day);
				if(yeartoInt <100)
					year = "0"+Integer.toString(yeartoInt);
				else
					year = Integer.toString(yeartoInt);
				if((montoInt+addMon) < 10 || (montoInt==9 && addMon==1))
					periodfrom = year + "0"+Integer.toString(montoInt)+day;
				else
					periodfrom = year + Integer.toString(montoInt)+day;
				periodto = taiwandate;
				log.debug("LASTMON=periodfrom="+periodfrom);
				log.debug("LASTMON=periodto="+periodto);
				
			}
			if(querytype.equals("LASTSIXMON"))
			{
				String taiwandate = getToday();
				String year = taiwandate.substring(0,3);
				String mon = taiwandate.substring(3,5);
				String day = taiwandate.substring(5,7);
				String dayArr[] = {"31","28","31","30","31","30","31","31","30","31","30","31"};
				
				GregorianCalendar gCal = new GregorianCalendar();				
				if (gCal.isLeapYear(Integer.parseInt(year)+1911))					
					dayArr[1] = "29";
				
				int yeartoInt = Integer.parseInt(year);
				int montoInt = Integer.parseInt(mon);
				int dayInt = Integer.parseInt(day);
				int addMon = 0;
				if(montoInt < 7) 
				{
					montoInt = 6+montoInt;
					yeartoInt = yeartoInt -1;
				}
				else montoInt = montoInt -6;
				log.debug("montoInt="+montoInt);
				log.debug("yeartoInt="+yeartoInt);
				if((dayInt +1 ) > Integer.parseInt(dayArr[montoInt -1]))
				{
					addMon = 1;
					day = "01";
				}else
				{
					if(dayInt+1 <10)
						day = "0"+Integer.toString(dayInt+1);
					else
						day = Integer.toString(dayInt+1);
				}
				log.debug("addMon="+addMon);
				log.debug("day="+day);
				if(yeartoInt <100)
					year = "0"+Integer.toString(yeartoInt);
				else
					year = Integer.toString(yeartoInt);
				if((montoInt+addMon) < 10 || (montoInt==9 && addMon==1))
					periodfrom = year + "0"+Integer.toString(montoInt)+day;
				else
					periodfrom = year + Integer.toString(montoInt)+day;
				periodto = taiwandate;
				log.debug("periodfrom="+periodfrom);
				log.debug("periodto="+periodto);
			}
			if(querytype.equals("LASTYEAR"))
			{
				String taiwandate = getToday();
				String year = taiwandate.substring(0,3);
				int yeartoInt = Integer.parseInt(year);
				yeartoInt = yeartoInt -1;		
				if(yeartoInt <100)
					year = "0"+Integer.toString(yeartoInt);
				else
					year = Integer.toString(yeartoInt);
				periodfrom = year + taiwandate.substring(3,taiwandate.length());
				periodto = taiwandate;
			}
		}
		
		List<GOLDPRICE> MAXRECOED = null;
		try{
		MAXRECOED = goldPriceDao.getRecord(querytype,periodfrom,periodto);
		}catch(Exception e){log.debug("Exception in GD011.java CALL clean getRecord()==============");}//如果發生Exception 不用處理，吐一個空的result回網頁顯示0筆資料
		
		MVHImpl result = new DBResult(MAXRECOED);
		//querytype="TODAYLAST";
		if(null != querytype && querytype.equals("TODAYLAST"))
		{
			log.debug("Y_MIN="+result.getValueByFieldName("BPRICE", 1));
			log.debug("Y_MAX="+result.getValueByFieldName("SPRICE", 1));
			result.getFlatValues().put("Y_MIN", result.getValueByFieldName("BPRICE", 1));
			result.getFlatValues().put("Y_MAX", result.getValueByFieldName("SPRICE", 1));
		}
		if(null != querytype && !querytype.equals("TODAYLAST"))
		{
			int i_Count = result.getValueOccurs("GOLDPRICEID");
			int r_Count = 0;
			if (i_Count != 0)
			{			
				String Bminmax[] = new String[i_Count];
				String Sminmax[] = new String[i_Count];
				for(int i=0;i<i_Count;i++)
				{
					Bminmax[i] = result.getValueByFieldName("BPRICE", i+1);
					log.debug("Bminmax[i]=="+Bminmax[i]);
				}
				for(int i=0;i<i_Count;i++)
				{
					Sminmax[i] = result.getValueByFieldName("SPRICE", i+1);
					log.debug("Sminmax[i]=="+Sminmax[i]);
				}
				sort(Bminmax);
				sort(Sminmax);
				log.debug("Bminmax.length="+Bminmax.length);
				log.debug("Bminmax[0]="+Bminmax[0]);
				log.debug("Bminmax[Bminmax.length]="+Bminmax[Bminmax.length-1]);
				log.debug("Sminmax.length="+Sminmax.length);
				log.debug("Sminmax[0]="+Sminmax[0]);
				log.debug("Sminmax[Sminmax.length]="+Sminmax[Sminmax.length-1]);
				result.getFlatValues().put("B_MIN", Bminmax[Bminmax.length-1]);
				result.getFlatValues().put("B_MAX", Bminmax[0]);
				result.getFlatValues().put("S_MIN", Sminmax[Sminmax.length-1]);
				result.getFlatValues().put("S_MAX", Sminmax[0]);
			}
		}
		if(periodfrom != "" && periodto !="")
		{
			periodfrom = DateTimeUtils.getDateShort(DateTimeUtils.getCDate2Date(periodfrom));
			periodto = DateTimeUtils.getDateShort(DateTimeUtils.getCDate2Date(periodto));
		}
		result.getFlatValues().put("PERIODFROM", DateUtil.formatDate(periodfrom));
		result.getFlatValues().put("PERIODTO", DateUtil.formatDate(periodto));
		//result.dump();
		return result;
	}
	
	/**
	 * 查詢當日之黃金存摺掛牌資料
	 * 
	 * @param str_CurrentDate
	 * @return
	 */
	public List<GOLDPRICE> getGoldPriceTodayRecord(String str_CurrentDate) {
			
		return goldPriceDao.getTODAYRecord(str_CurrentDate, "");
	}
	
	private void sort(String[] id)
	{
		int x = id.length;
		while (--x >= 0) {
			for (int j= 0;j<x;j++) {
			    int jj = Integer.parseInt(id[j]);
			    int ii = Integer.parseInt(id[j+1]);
				if (jj < ii) {
					String tmp = id[j];
					id[j] = id[j+1];
					id[j+1] = tmp;
				}
			}
		}
	}
	
	private String getToday()
	{
		TopLinkedHashMap date = null;
		try{
			date = Machine.getSystemDateTime();
			
		}catch(Exception e){return null;}		
		String today = DateUtil.getTaiwanDate((String)date.get("SystemDate")).replace("/", "");
		log.debug("today in GD011.getToday() = "+today+"===");
		return today;
	}
	
	private String getPreday()
	{
		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_YEAR);
	    calendar.set(Calendar.DAY_OF_YEAR, day - 1);
	    String preday = DateTimeUtils.getDateShort(calendar.getTime()).replace("/", "");
		log.debug("preday in GD011.getToday() = "+preday+"===");
		return preday;
	}
}
