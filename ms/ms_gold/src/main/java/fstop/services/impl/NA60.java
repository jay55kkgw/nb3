package fstop.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.ADMMAILLOG;
import fstop.orm.po.TXNUSER;
import fstop.services.CommonService;
import fstop.services.GoldWebServiceNotice;
import fstop.services.NoticeInfo;
import fstop.services.NoticeInfoGetter;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 申請黃金存摺網路交易
 * @author Owner
 *
 */
@Slf4j
public class NA60 extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("na60Telcomm")
	private TelCommExec na60Telcomm;
	
//	@Required
//	public void setNa60Telcomm(TelCommExec telcomm) {
//		na60Telcomm = telcomm;
//	}
	
	/**
	 * 發送申請黃金存摺網路交易電文
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params) {
		final Map<String, String> params = _params;
		
		MVHImpl result = new MVHImpl();
		
		String uid = params.get("UID").toString();
		String fgtxway = params.get("FGTXWAY").toString();
		String[] gdacnList = params.get("ACN").toString().split(",");
		
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		Integer icseq = 0;
		params.put("DATE", df);
		params.put("TIME", tf);
		if (fgtxway.equals("1"))
		{//電子簽章
			params.put("KIND", "2");
		}
		else
		{//晶片金融卡
			icseq = Integer.parseInt(params.get("icSeq"));
			params.put("KIND", "1");
			
			String chip_acn = params.get("accNo").trim();
			params.put("CHIP_ACN", chip_acn);
		}
		
		params.put("CUSIDN", uid);
		
		ArrayList<String> successAcnos = new ArrayList<String>();
		for (int i = 0; i < gdacnList.length; i++)
		{
			String gdAcno = gdacnList[i];
			params.put("GDACN", gdAcno);
	
			MVHImpl table = null;
			boolean firstQueryDone = false;
			
			if (!fgtxway.equals("1"))
			{//晶片金融卡要給序號
				Integer iIcseq = icseq + i;
				String sIcseq = "00000000" + iIcseq.toString();
				sIcseq = sIcseq.substring(sIcseq.length() - 8);
				params.put("ICSEQ", sIcseq);
			}
			
			//身份驗證
			try 
			{
				params.put("TYPE", "01");	//身份驗證
				table = na60Telcomm.query(params);
				firstQueryDone = true;
			}
			catch(TopMessageException e) {
				table = new MVHImpl();
				table.getFlatValues().put("TOPMSG", e.getMsgcode());
				table.getFlatValues().put("ADMSGIN", e.getMsgin());
				table.getFlatValues().put("ADMSGOUT", e.getMsgout());	
			}
			
			try
			{
				if (firstQueryDone)
				{//身份驗證無誤
					params.put("TYPE", "02");	//確認申請
					table = na60Telcomm.query(params);
					successAcnos.add(gdAcno);
				}
			}
			catch(TopMessageException e)
			{
				table = new MVHImpl();
				table.getFlatValues().put("TOPMSG", e.getMsgcode());
				table.getFlatValues().put("ADMSGIN", e.getMsgin());
				table.getFlatValues().put("ADMSGOUT", e.getMsgout());
			}
			
			table.getFlatValues().put("GDACN", gdacnList[i]);
			table.getFlatValues().put("SVACN", params.get("SVACN"));
			result.addTable(table);
		}
		
		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		
		//為所有申請成功的帳號發送一封Mail通知(失敗的不發Mail)
		if (successAcnos.isEmpty())
		{
			return result;
		}
		try
		{
			final TxnUserDao txnUserDao = (TxnUserDao)SpringBeanFactory.getBean("txnUserDao");
			TXNUSER txnUser = txnUserDao.findById(uid);
			final String dpmyemail = StrUtils.trim(txnUser.getDPMYEMAIL());
			if (StrUtils.isEmpty(dpmyemail))
				return result;
			
			String gdacnos = "";
			for (int i = 0; i < successAcnos.size(); i++)
			{
				String gdacno = successAcnos.get(i);
				gdacnos += StrUtils.left(gdacno, 6) + StrUtils.repeat("*", gdacno.length() - 9) + StrUtils.right(gdacno, 3) + "<br />";
			}
			final String resultGdacn =  gdacnos;
			final String resultSvacn =  params.get("SVACN");
			
			GoldWebServiceNotice trNotice = new GoldWebServiceNotice();
			trNotice.sendNotice(new NoticeInfoGetter() {
	
				public NoticeInfo getNoticeInfo() {
					NoticeInfo noticeResult = new NoticeInfo("", "", dpmyemail);
										
					noticeResult.setTemplateName("GOLD_APPLY_TRAN");
					
					Date d = new Date();
					String df = DateTimeUtils.format("yyyyMMdd", d);
					String tf = DateTimeUtils.format("HHmmss", d);
					String dateTime = DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", d);
					
					noticeResult.getParams().put("#TRANTIME", dateTime);
					noticeResult.getParams().put("#GDACN", resultGdacn);
					noticeResult.getParams().put("#SVACN", StrUtils.left(resultSvacn, resultSvacn.length() - 5) + "***" + StrUtils.right(resultSvacn, 2));
					
					ADMMAILLOG admmaillog = new ADMMAILLOG();
										
					admmaillog.setADBATCHNO("NA60");
					admmaillog.setADMAILACNO(dpmyemail);
					admmaillog.setADUSERID("SYS");
					admmaillog.setADUSERNAME("SYS");
					admmaillog.setADSENDTYPE("5");
					admmaillog.setADSENDTIME(df + tf);
					admmaillog.setADSENDSTATUS("");
					
					noticeResult.setADMMAILLOG(admmaillog);
					
					return noticeResult;
				}
				
			});
		}
		catch (Exception e)
		{
			log.error("發送 Email 失敗.", e);
		}
		
		return result;
	}
}
