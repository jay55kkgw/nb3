package fstop.services.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N850A extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());
	
	@Autowired
	@Qualifier("n850ATelcomm")
	private TelCommExec n850ATelcomm;
	 
//	@Required
//	public void setN850ATelcomm(TelCommExec telcomm) {
//		n850ATelcomm = telcomm;
//	}
	
	@Override	
	public MVH doAction(Map params) {

		MVHImpl helper = new MVHImpl();
		//將傳入的字串切割組成陣列
		log.trace(ESAPIUtil.vaildLog("GET ArrayParam : " + params.get("ArrayParam").toString()));
		String newparams = params.get("ArrayParam").toString();
		String[] arrayParams = newparams.split(";,");

//		String[] arrayParams = (String[])params.get("ArrayParam");
		Map cloneParams = new HashMap<>();
		cloneParams.putAll(params);
		if(arrayParams != null) {
			for(String p : arrayParams) {
				log.debug(ESAPIUtil.vaildLog("申請掛失 do : " + p)); //{}
//				Hashtable cloneParams = (Hashtable)params.clone();
				Map jb = JSONUtils.json2map(p);
				cloneParams.putAll(jb);
				cloneParams.put("DATE", DateTimeUtils.getCDateShort(new Date()));
				cloneParams.put("TIME", DateTimeUtils.format("HHmmss", new Date()));
				MVHImpl tr = null;
				try {
					tr = n850ATelcomm.query(cloneParams);
					tr.getFlatValues().put("STATUS", "掛失成功");
				}
				catch(TopMessageException e) {
					tr = new MVHImpl();
//					TODO 舊的方式會拿不到 之後跟JIMMY討論
//					tr.getFlatValues().put("STATUS", ((TopMessageException) e.getCause()).getMsgout());
					tr.getFlatValues().put("TOPMSG", e.getMsgcode());
					tr.getFlatValues().put("STATUS", e.getMsgout());
				}
				tr.getFlatValues().putAll(jb);
				helper.getFlatValues().put("TOPMSG", tr.getFlatValues().get("TOPMSG"));
				helper.getOccurs().addRow(new Row(tr.getFlatValues()));
			}
		}
		
		
		helper.getFlatValues().put("CMQTIME",
				DateTimeUtils.getDatetime(new Date()));
		
		
		
		return helper;
	}



}
