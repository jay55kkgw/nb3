package fstop.services.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.rest.model.CommonPools;
import fstop.model.IMasterValuesHelper;
import fstop.exception.TopMessageException;
import fstop.model.EachRowCallback;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.MVHUtils;
import fstop.model.Row;
import fstop.model.Rows;
import fstop.model.RowsSelecter;
import fstop.model.TelcommResult;
import fstop.orm.dao.AdmKeyValueDao;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N191 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("n191Telcomm")
	private TelCommExec n191Telcomm;
	@Autowired
	private AdmKeyValueDao admKeyValueDao;
	@Autowired
	private CommonPools commonPools;
	
//	@Required
//	public void setN191Telcomm(TelCommExec telcomm) {
//		n191Telcomm = telcomm;
//	}
//	
//	@Required
//	public void setAdmKeyValueDao(AdmKeyValueDao dao) {
//		admKeyValueDao = dao;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) {
		
		String period = (String)params.get("FGPERIOD");
		String enddate = "";
		String stadate = "";
		String periodStr = "";
		if("CMTODAY".equals(period)) { //今日
			stadate = DateTimeUtils.getCDateShort(new Date());
			enddate = stadate;
			periodStr = DateTimeUtils.format("yyyy/MM/dd", new Date());
		} else if("CMCURMON".equals(period)) { //本月
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.DATE, 1);
			Date dstart = cal.getTime();

			Date dend = new Date();
			stadate = DateTimeUtils.getCDateShort(dstart);
			enddate = DateTimeUtils.getCDateShort(dend);
			periodStr = DateTimeUtils.format("yyyy/MM/dd", dstart) + "~" + DateTimeUtils.format("yyyy/MM/dd", dend);
		} else if("CMLASTMON".equals(period)) { //上月
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.DATE, 1);
			cal.add(Calendar.MONTH, -1);
			Date dstart = cal.getTime();
			cal.add(Calendar.MONTH, 1);
			cal.add(Calendar.DATE, -1);
			Date dend = cal.getTime();
			stadate = DateTimeUtils.getCDateShort(dstart);
			enddate = DateTimeUtils.getCDateShort(dend);
			periodStr = DateTimeUtils.format("yyyy/MM/dd", dstart) + "~" + DateTimeUtils.format("yyyy/MM/dd", dend);
		} else if("CMNEARMON".equals(period)) { //最近一個月
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.MONTH, -1);
			Date dstart = cal.getTime();
			Date dend = new Date();
			stadate = DateTimeUtils.getCDateShort(dstart);
			enddate = DateTimeUtils.getCDateShort(dend);
			periodStr = DateTimeUtils.format("yyyy/MM/dd", dstart) + "~" + DateTimeUtils.format("yyyy/MM/dd", dend);
		} else if("CMLASTWEEK".equals(period)) { //最近一星期
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -7);
			Date dstart = cal.getTime();
			Date dend = new Date();
			stadate = DateTimeUtils.getCDateShort(dstart);
			enddate = DateTimeUtils.getCDateShort(dend);
			periodStr = DateTimeUtils.format("yyyy/MM/dd", dstart) + "~" + DateTimeUtils.format("yyyy/MM/dd", dend);
		} else if("CMPERIOD".equals(period)) { //指定日期
			stadate = (String)params.get("CMSDATE");
			enddate = (String)params.get("CMEDATE");
			stadate = StrUtils.trim(stadate).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
			enddate = StrUtils.trim(enddate).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
			if(stadate != null && stadate.length() == 8) {
				Date sd = DateTimeUtils.parse("yyyyMMdd", stadate);
				stadate = DateTimeUtils.getCDateShort(sd);
				periodStr = DateTimeUtils.format("yyyy/MM/dd", sd);
			}
			if(enddate != null && enddate.length() == 8) {
				Date sd = DateTimeUtils.parse("yyyyMMdd", enddate);
				enddate = DateTimeUtils.getCDateShort(sd);
				periodStr += "~" + DateTimeUtils.format("yyyy/MM/dd", sd);
			}
			else { //若未指定結束時間, 則使用今天日期
				enddate = DateTimeUtils.getCDateShort(new Date());
				periodStr += "~" + DateTimeUtils.format("yyyy/MM/dd", new Date());
			}
		}
		params.put("STADATE", stadate);
		params.put("ENDDATE", enddate);
		
		MVHImpl result = new MVHImpl();
		
		
		queryAll(getAcnoList(params), params, result);
		
		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		result.getFlatValues().put("CMPERIOD", periodStr);
		result.getFlatValues().put("CMRECNUM", MVHUtils.sumTablesOccurs(result) + "");

		return result;
	}
	
	private List<String> getAcnoList(Map<String, String> params) {
		List<String> acnos = new ArrayList();
		if(StrUtils.isEmpty((String)params.get("ACN"))) {
			
			IMasterValuesHelper acnosMVH = commonPools.n920.getAcnoList(params.get("UID"), new String[]{"07","09"}, "");
			
			int requestCount = acnosMVH.getValueOccurs("ACN");
			for(int i=0; i < requestCount; i++) {
				String acn = acnosMVH.getValueByFieldName("ACN", i+1);
				acnos.add(acn);
			}
			
		}
		else {
			acnos.add(params.get("ACN"));
		}
		return acnos;
	}	
	
	public void queryAll(List<String> acnoList, Map<String, String> params, MVHImpl resultMVH) {
 		
		List<String> currAcnoList = new ArrayList<String>();
		String doneAcnos = params.get("DONEACNOS");
		String[] doneAcnoList = null;
		if (doneAcnos == null)
			doneAcnoList = new String[0];
		else
			doneAcnoList = doneAcnos.split(",");
		
 		for(String acn : acnoList) {
 			
 			currAcnoList.add(acn);
 			
 			//已執行過的ACNO不重複執行
 			boolean hasDone = false;
 			for (int i = 0; i < doneAcnoList.length; i++)
 			{
 				if (doneAcnoList[i].equals(acn))
 				{
 					hasDone = true;
 					break;
 				}
 			}
 			if (hasDone)
 				continue;
 			
 			params.put("ACN", acn);
 			try {
	 			MVHImpl result = n191Telcomm.query(params);
	 			params.remove("QUERYNEXT");	//一個QUERYNEXT只能用於一個ACNO
	 			
	 			result.getOccurs().setValue("ACN", acn); 
	 			
	 			RowsSelecter rowSelecter = new RowsSelecter(result.getOccurs());
	 			rowSelecter.each(new EachRowCallback(){

	 				public void current(Row row) {
	 					
	 					if("1".equalsIgnoreCase(row.getValue("CODDBCR"))) {
	 						row.setValue("DPWDAMT", row.getValue("TRNGD"));
	 					}
	 					if("2".equalsIgnoreCase(row.getValue("CODDBCR"))) {
	 						row.setValue("DPSVAMT", row.getValue("TRNGD"));
	 					}
	 					
	 					//使用 adm key value 轉換 memo
	 					//改至service轉換
	 					//String r = admKeyValueDao.translator("GOLDMEMO", row.getValue("MEMO"));
	 					//row.setValue("MEMO", r);
	 				}});
	 			
	 			resultMVH.addTable(result, acn);
	 			
	 			String queryNext = result.getValueByFieldName("QUERYNEXT");
	 			if (!queryNext.equals(""))
	 			{
	 				resultMVH.getFlatValues().put("QUERYNEXT", queryNext);
	 				
	 				currAcnoList.remove(currAcnoList.size() - 1);
	 				StringBuffer sb = new StringBuffer();
	 				if (currAcnoList.size() > 0)
	 				{
		 		 		sb.append(currAcnoList.get(0));
		 		 		for (int i = 1; i < currAcnoList.size(); i++)
		 		 		{
		 		 			sb.append("," + currAcnoList.get(i));
		 		 		}
	 				}
	 		 		resultMVH.getFlatValues().put("DONEACNOS", sb.toString());
	 				
	 				break;	 				
	 			}
			}
			catch(TopMessageException ex) {
				MVHImpl helper = new MVHImpl();
				helper.getFlatValues().put("ACN", acn);
				helper.getFlatValues().put("TOPMSG", ex.getMsgcode());
				helper.getFlatValues().put("ADMSGIN", ex.getMsgin());
				helper.getFlatValues().put("ADMSGOUT", ex.getMsgout());
				resultMVH.addTable(helper, acn);			
					
			}
 		}
	}


}
