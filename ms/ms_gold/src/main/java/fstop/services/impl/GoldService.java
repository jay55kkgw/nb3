package fstop.services.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.rest.model.CommonPools;
import com.netbank.rest.ws.bean.Gold;
import com.netbank.rest.ws.bean.GoldProduct;
import com.netbank.rest.ws.bean.GoldSell;
import com.netbank.rest.ws.bean.GoldSellProduct;
import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.IMasterValuesHelper;
import fstop.model.MVHImpl;
import fstop.orm.dao.AdmHolidayDao;
import fstop.orm.po.ADMHOLIDAY;
import fstop.orm.po.ADMMAILLOG;
import fstop.services.BOService;
import fstop.services.GoldWebServiceNotice;
import fstop.services.NoticeInfo;
import fstop.services.NoticeInfoGetter;
import fstop.services.TelCommExecWrapper;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GoldService {

	
	@Autowired
	private CommonPools commonPools;
	@Autowired
    @Qualifier("gd01Telcomm")
	private TelCommExec gd01Telcomm;
	@Autowired
	@Qualifier("gd02Telcomm")
	private TelCommExec gd02Telcomm;
	@Autowired
	private AdmHolidayDao admholidayDao;

	
	public GoldSell informGoldValue(Gold gold) {
		Date d = new Date();
		ADMHOLIDAY holiday = null;
		try {
			String holidaytoday = DateTimeUtils.getDateShort(d);
//			holiday = (ADMHOLIDAY)admholidayDao.get(holidaytoday);
//			holiday = (ADMHOLIDAY)admholidayDao.find(holidaytoday);
			holiday = (ADMHOLIDAY)admholidayDao.findUniqueBy(ADMHOLIDAY.class, "ADHOLIDAYID", holidaytoday);
			log.warn(ESAPIUtil.vaildLog("Holiday = " + holiday));
		}
		catch (Exception e) {
			holiday = null;					
		}
		
	if(holiday==null)  //假日不執行黃金報價更新及銷售量回報 holiday==null -> 非假日   holiday!=null -> 假日
	{ 	 
		log.debug("=== gold web service request data start ===");
		log.debug("gold.getCnt()  = "+ gold.getCnt());
		log.debug("gold.getDate() = "+ gold.getDate());
		log.debug("gold.getKind() = "+ gold.getKind());
		log.debug("gold.getTime() = "+ gold.getTime()); 
		
		List<GoldProduct> goldProducts = gold.getGoldProduct();
		String msg = "(DATE: " + gold.getDate() + ", CNT: " + gold.getCnt() + ", TIME: " + gold.getTime() + "), ";
		
		if (goldProducts != null)
		{
			for(GoldProduct bean:goldProducts) {
				log.trace("GoldProduct>>{}",bean.toString());
			}
		}
		log.debug("=== gold web service request data end ===");
		
		log.debug("=== gold web service handle start " + msg + " ===");
		HashMap<String, String> params = new HashMap<String, String>();
		GoldSell goldSell = new GoldSell();
		String kind = gold.getKind();
		
		//kind = 0 報價
		//kind = 3 修改 
		//kind = 4 取消
		if (kind.equals("0") || kind.equals("3") || kind.equals("4"))
		{
			// 準備 GD01 資料
			try
			{
				params.put("TR_CLASS", kind);
				params.put("DATE", gold.getDate());
				params.put("CNT", gold.getCnt());
				params.put("TIME", gold.getTime());
				
				if (goldProducts != null)
				{
					Integer productCount = gold.getGoldProduct().size();
					params.put("TOTREC", Integer.toString(productCount));
					int i = 0;
					for(GoldProduct bean:goldProducts) {
						String msgNo = Integer.toString(i + 1);
						params.put("GOODNO" + msgNo,bean.getGoodno());
						params.put("SELL" + msgNo, bean.getSell());
						params.put("SELLT" + msgNo, bean.getSellt());
						params.put("BUY" + msgNo, bean.getBuy());
						params.put("DPDIFF" + msgNo, bean.getDpdiff());
						i++;
					}
				}
			}
			catch (Exception e)
			{
				goldSell.setMsgCode("ZX99");
				String errMsg = "報價資料有誤. " + msg;
				goldSell.setMessage(errMsg);
				log.error("臺灣銀行黃金存摺" + errMsg + e.getMessage(), e);
			}
			
			// 發送 GD01 電文報價
			try {
				final TelCommExecWrapper execwrapper = new TelCommExecWrapper(gd01Telcomm);
				this.queryGd01(execwrapper, params);
				execwrapper.throwException();
			}
			catch (TopMessageException topE)
			{
				goldSell.setMsgCode(topE.getMsgcode());
				goldSell.setMessage(topE.getMsgout());
				log.error("GD01報價失敗. " + msg + topE.getMessage(), topE);
			}
			catch (Exception e) {
				goldSell.setMsgCode("ZX99");
				String errMsg = "發送報價電文時發生異常. " + msg;
				goldSell.setMessage(errMsg);
				log.error("GD01" + errMsg + e.getMessage(), e);
			}
			
			//第一盤不送GD02
			if ((gold.getCnt().equals("1") || gold.getCnt().equals("01")) && StrUtils.isEmpty(goldSell.getMsgCode()))
			{
				goldSell.setMsgCode("0");
				goldSell.setMessage("OK");
			}
		}

		IMasterValuesHelper gd02Helper = null;
		
		// 檢查是否有發送GD01的錯誤
		if (StrUtils.isEmpty(goldSell.getMsgCode()))
		{
			// 準備 GD02 資料
			try
			{
				params = new HashMap<String, String>();
				params.put("DATE", gold.getDate());
				params.put("KIND", kind.equals("2") ? "1" : "0"); //台銀傳0,1,3,4表一般查詢=>0，傳2表結帳查詢=>1
			}
			catch (Exception e)
			{
				goldSell.setMsgCode("ZX99");
				String errMsg = "報價資料有誤. " + msg;
				goldSell.setMessage(errMsg);
				log.error("臺灣銀行黃金存摺" + errMsg + e.getMessage(), e);				
			}
			
			// 發送 GD02 電文查詢銷售資料
			try {
				final TelCommExecWrapper execwrapper = new TelCommExecWrapper(gd02Telcomm);
				gd02Helper = this.queryGd02(execwrapper, params);
				execwrapper.throwException();
				goldSell.setMsgCode("0");
				goldSell.setMessage("OK");
			}
			catch (TopMessageException topE)
			{
				goldSell.setMsgCode(topE.getMsgcode());
				goldSell.setMessage(topE.getMsgout());
				log.error("GD02銷售資料查詢失敗. " + msg + topE.getMessage(), topE);
			}
			catch (Exception e) {
				goldSell.setMsgCode("ZX99");
				String errMsg = "發送查詢銷售資料電文時發生異常. " + msg;
				goldSell.setMessage(errMsg);
				log.error("GD02" + errMsg + e.getMessage(), e);
			}
		}
		
		List <GoldSellProduct> goldSellProducts = null;
		//檢查GD02下行電文是否存在
		if (gd02Helper != null)
		{//以GD02下行電文組GoldSellProduct
			goldSell.setDate(gd02Helper.getValueByFieldName("DATE"));
			goldSell.setTime(gd02Helper.getValueByFieldName("TIME"));
			goldSell.setBank_no(gd02Helper.getValueByFieldName("Bank_no"));
			
			GoldSellProduct goldSellProduct = null;
			goldSellProducts = new LinkedList<GoldSellProduct>();
			int i_Count = gd02Helper.getValueOccurs("GOODNO");
			for (int i=0; i<i_Count; i++)
			{
				goldSellProduct = new GoldSellProduct();
				goldSellProduct.setDiscount_amt(gd02Helper.getValueByFieldName("Disc_amt", i+1));
				goldSellProduct.setDpdiff(gd02Helper.getValueByFieldName("Dpdiff", i+1));
				goldSellProduct.setGoodno(gd02Helper.getValueByFieldName("GOODNO", i+1));
				goldSellProduct.setSale_amt(gd02Helper.getValueByFieldName("Sale_amt", i+1));
				goldSellProduct.setSale_kind(gd02Helper.getValueByFieldName("Sale_kind", i+1));
				goldSellProduct.setSale_q(gd02Helper.getValueByFieldName("Sale_q", i+1));
				goldSellProduct.setTake_q(gd02Helper.getValueByFieldName("Take_q", i+1));
				goldSellProduct.setTax_amt(gd02Helper.getValueByFieldName("Tax_amt", i+1));
				goldSellProduct.setUnit_price(gd02Helper.getValueByFieldName("U_price", i+1));
				goldSellProduct.setUnit_price_g(gd02Helper.getValueByFieldName("U_price_g", i+1));
				goldSellProducts.add(goldSellProduct);
			}
			
			
		}
		else
		{//無GD02下行電文時不組GoldSellProduct
			Date today = new Date();
			goldSell.setDate(DateTimeUtils.format("yyyyMMdd", today));
			goldSell.setTime(DateTimeUtils.format("HHmmss", today));
			goldSell.setBank_no("050");
		}
		
		goldSell.setGoldSellProduct(goldSellProducts);
		
		log.debug("=== gold web service handle end " + msg + " ===");
		
		return goldSell;
		}
	else
	{	
		log.warn("gold web service stop -->holiday true");
		return null;
	}	
	}

	private void queryGd01(TelCommExecWrapper execwrapper, HashMap<String, String> params)
	{
		try
		{
			String kind = params.get("TR_CLASS");
			final String kindDesc = getKindDesc(kind);
			
			MVHImpl result = execwrapper.query(params);
			
			GoldWebServiceNotice trNotice = new GoldWebServiceNotice();
			Hashtable b106Params = new Hashtable();
		    b106Params.put("TABLE","SysParamData");
		    b106Params.put("ADPK","NBSYS");
		    //失敗時要通知的人員
		    final String apMail = commonPools.b106.doAction(b106Params).getValueByFieldName("ADGDMAIL");
		    //成功時要通知的人員
		    final String gd01Mail = commonPools.b106.doAction(b106Params).getValueByFieldName("ADGDMAIL");
			
			final TelCommExecWrapper execwrapperFinal = execwrapper;
			trNotice.sendNotice(new NoticeInfoGetter() {

				public NoticeInfo getNoticeInfo() {
					NoticeInfo result = new NoticeInfo("", apMail, gd01Mail);
										
					result.setTemplateName("GoldRequest");
					result.setException(execwrapperFinal.getLastException());
					
					Date d = new Date();
					String df = DateTimeUtils.format("yyyyMMdd", d);
					String tf = DateTimeUtils.format("HHmmss", d);
					String time = DateTimeUtils.format("HH:mm:ss", d);

					result.getParams().put("#TIME", time);
					result.getParams().put("#KINDDESC", kindDesc);

					ADMMAILLOG admmaillog = new ADMMAILLOG();
										
					admmaillog.setADBATCHNO("GD01");
					admmaillog.setADMAILACNO(gd01Mail);
					admmaillog.setADUSERID("SYS");
					admmaillog.setADUSERNAME("SYS");
					admmaillog.setADSENDTYPE("5");
					admmaillog.setADSENDTIME(df + tf);
					admmaillog.setADSENDSTATUS("");
					
					result.setADMMAILLOG(admmaillog);
					
					return result;
				}
				
			});
		}
		catch (Exception e)
		{
			log.error("發送 Email 失敗.", e);
		}
	}
	
	/***
	 * 取得台銀報價通知信中，報價類型的中文說明
	 * @param kind
	 * @return
	 */
	private String getKindDesc(String kind)
	{
		String result = "";
		
		if (kind.equals("3"))
			result = "修改";
		else if (kind.equals("4"))
			result = "取消";
		
		return result;
	}
	
	private IMasterValuesHelper queryGd02(TelCommExecWrapper execwrapper, HashMap<String, String> params)
	{
		IMasterValuesHelper result = null;
		
		try
		{		    
			result = execwrapper.query(params);
			
			GoldWebServiceNotice trNotice = new GoldWebServiceNotice();
			Map b106Params = new HashMap();
		    b106Params.put("TABLE","SysParamData");
		    b106Params.put("ADPK","NBSYS");
		    //失敗時要通知的人員
		    final String apMail = commonPools.b106.doAction(b106Params).getValueByFieldName("ADGDMAIL");
		    //成功時要通知的人員
		    final String gd02Mail = commonPools.b106.doAction(b106Params).getValueByFieldName("ADGDMAIL");
			
			final TelCommExecWrapper execwrapperFinal = execwrapper;
			trNotice.sendNotice(new NoticeInfoGetter() {

				public NoticeInfo getNoticeInfo() {
					NoticeInfo result = new NoticeInfo("", apMail, gd02Mail);
										
					result.setTemplateName("GoldResponse");
					result.setException(execwrapperFinal.getLastException());
					
					Date d = new Date();
					String df = DateTimeUtils.format("yyyyMMdd", d);
					String tf = DateTimeUtils.format("HHmmss", d);
					String time = DateTimeUtils.format("HH:mm:ss", d);

					result.getParams().put("#TIME", time);

					ADMMAILLOG admmaillog = new ADMMAILLOG();
										
					admmaillog.setADBATCHNO("GD02");
					admmaillog.setADMAILACNO(gd02Mail);
					admmaillog.setADUSERID("SYS");
					admmaillog.setADUSERNAME("SYS");
					admmaillog.setADSENDTYPE("5");
					admmaillog.setADSENDTIME(df + tf);
					admmaillog.setADSENDSTATUS("");
					
					result.setADMMAILLOG(admmaillog);
					
					return result;
				}
				
			});
		}
		catch (Exception e)
		{
			log.error("發送 Email 失敗.", e);
		}
		
		return result;
	}
	
}
