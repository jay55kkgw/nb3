package fstop.services.batch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;

import fstop.exception.TopMessageException;
import fstop.model.EachRowCallback;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.model.Rows;
import fstop.model.TelcommResult;
import fstop.notifier.NotifyAngent;
import fstop.orm.dao.SysParamDataDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.SYSPARAMDATA;
import fstop.orm.po.TXNUSER;
import fstop.services.AfterSuccessQuery;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import fstop.util.PathSetting;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 台幣預約轉帳
 * 
 * @author Owner 每小時40分自動執行
 * 寄件者:景森
 * DATE:
 * TIME:
 * 系統狀態: 成功 失敗
 * retry 3 times
 */
@Slf4j
public class ND08  implements BatchExecute {
//	private Logger logger = Logger.getLogger("fstop_txntw");

	private DecimalFormat fmt = new DecimalFormat("#0");

	@Autowired
	@Qualifier("nd08Telcomm")
	private TelCommExec nd08Telcomm;

	private Map setting;

	@Autowired
	private SysParamDataDao sysParamDataDao;
	
//	@Required
//	public void setSysParamDataDao(SysParamDataDao sysParamDataDao) {
//		this.sysParamDataDao = sysParamDataDao;
//	}
//
//	@Required
//	public void setNd08Telcomm(TelCommExec telCommExec) {
//		this.nd08Telcomm = telCommExec;
//	}
//
//	@Required
//	public void setSetting(Map setting) {
//		this.setting = setting;
//	}

	public ND08(){}
	
	/**
	 * args[0] method name
	 */
	public BatchResult execute(String[] args) {
		Hashtable<String, String> params = new Hashtable();

		SimpleTemplate simpleTemplate = new SimpleTemplate(nd08Telcomm);

		simpleTemplate.setAfterSuccessQuery(new AfterSuccessQuery() {

			public void execute(MVHImpl result) {
				SYSPARAMDATA po = null;
				Hashtable<String, String> varmail = new Hashtable();
				
				String hoststatus=null;
				po = sysParamDataDao.findById("NBSYS");
				if(po==null)
				{
					log.error("Execute query sysParamData table error");
					Map<String, Object> data = new HashMap();
					data.put("COUNTER", new BatchCounter());

					BatchResult batchresult = new BatchResult();
					batchresult.setSuccess(false);
					batchresult.setBatchName(getClass().getSimpleName());
					batchresult.setData(JSONUtils.toJson(data));
					return ;
				}

				String emailap=po.getADAPMAIL().replace(";", ",") ;
				log.debug("emailap = " + emailap);
//				String email1 = (String)setting.get("email1");

				String hostmsg=result.getValueByFieldName("MSGCOD");
				
				String hostdate=result.getValueByFieldName("DATE");
				if(hostdate==null)
				{
					Date d = new Date();
					hostdate=DateTimeUtils.getCDateShort(d);
				}
				String hosttime=result.getValueByFieldName("TIME");
				if(hosttime==null)
				{
					Date d = new Date();
					hosttime=DateTimeUtils.getTimeShort(d);
				}
				if(hostmsg.equals("0000"))
				{
					hoststatus=new String("成功");
					varmail.put("MACHNAME", " ");
				}
				else
				{
					hoststatus=new String("失敗");
					CheckServerHealth checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
					varmail.put("MACHNAME", "機器名稱：" + checkserverhealth.GetHostName());
				}
				varmail.put("EMAILSEND",(String)setting.get("emailsend"));
				varmail.put("SYSDATE", hostdate.substring(0, 3) + "/" + hostdate.substring(3, 5) + "/" + hostdate.substring(5, 7));
				varmail.put("SYSTIME", hosttime.substring(0, 2) + ":" + hosttime.substring(2, 4) + ":" + hosttime.substring(4, 6));
				varmail.put("SYSSTATUS", hoststatus); 
				// call BillHunter  成功不送MAIL 2017.04.18 By 景森
				//boolean isSendSuccess = NotifyAngent.sendNotice("ND08", varmail, emailap, "batch");
				//if(!isSendSuccess) {
				//	log.debug("發送 Email 失敗.(" + emailap + ")");
				//	result.getFlatValues().put("__MAILERR", "發送 Email 失敗.(" + emailap + ")");
				//}

			}
			
		});

		simpleTemplate.setEachRowCallback(new EachRowCallback() {

			public void current(Row row) {
//				String userEmail = emailGetter.getUserEmail(idn);

			}
			
		});
		simpleTemplate.setExceptionHandler(new ExceptionHandler() {

			public void handle(Row row, Exception e) {
				
				log.error("無法更用戶通知訊息.", e);
				
			}
			
		});
		
		try
		{
			BatchCounter counter = simpleTemplate.execute(params);
		}
		catch(TopMessageException topmsg)
		{
			SYSPARAMDATA po = null;
			Hashtable<String, String> varmail = new Hashtable();
			Date d = new Date();
			String today = DateTimeUtils.getCDateShort(d);
			String time = DateTimeUtils.getTimeShort(d);
			
			String hoststatus=null;
			po = sysParamDataDao.findById("NBSYS");
			if(po==null)
			{
				log.debug("Query NBSYS table error");
				Map<String, Object> data = new HashMap();
				data.put("COUNTER", new BatchCounter());

				BatchResult batchresult = new BatchResult();
				batchresult.setSuccess(false);
				batchresult.setBatchName(getClass().getSimpleName());
				batchresult.setData(JSONUtils.toJson(data));
				return batchresult;
			}

			String emailap=po.getADAPMAIL() ;

			String hostmsg=topmsg.getMsgcode();
			String hostdate=today.substring(0, 3) + "/" + today.substring(3, 5) + "/" + today.substring(5, 7);
			if(hostdate==null)
				hostdate="          ";
			String hosttime=time.substring(0, 2) + ":" + time.substring(2, 4) + ":" + time.substring(4, 6);
			if(hosttime==null)
				hosttime="        ";
			hoststatus=hostmsg + " " + topmsg.getMsgout();

			varmail.put("EMAILSEND",(String)setting.get("emailsend"));
			CheckServerHealth checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
			varmail.put("MACHNAME", "機器名稱：" + checkserverhealth.GetHostName());
			varmail.put("SYSDATE", hostdate);
			varmail.put("SYSTIME", hosttime);
			varmail.put("SYSSTATUS", hoststatus);

			// call BillHunter
			boolean isSendSuccess = NotifyAngent.sendNotice("ND08", varmail, emailap, "batch");
			if(!isSendSuccess) {
				log.debug("發送 錯誤Email 失敗.(" + emailap + ")");
//				batchresult.getFlatValues().put("__MAILERR", "發送 Email 失敗.(" + email1 + ")");
				Map<String, Object> data = new HashMap();
				data.put("COUNTER", new BatchCounter());

				BatchResult batchresult = new BatchResult();
				batchresult.setSuccess(false);
				batchresult.setBatchName(getClass().getSimpleName());
				batchresult.setData(JSONUtils.toJson(data));

				return batchresult;
			}
			log.debug("發送 Email 成功.(" + emailap + ")");

		}
		 
		Map<String, Object> data = new HashMap();
		data.put("COUNTER", new BatchCounter());

		BatchResult batchresult = new BatchResult();
		batchresult.setSuccess(true);
		batchresult.setBatchName(getClass().getSimpleName());
		batchresult.setData(JSONUtils.toJson(data));

		return batchresult;

	}
}
