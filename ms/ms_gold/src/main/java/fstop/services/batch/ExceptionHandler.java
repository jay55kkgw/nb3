package fstop.services.batch;

import fstop.model.Row;

public interface ExceptionHandler {
	public void handle(Row row, Exception e);
}
