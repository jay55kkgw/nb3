package com.netbank.rest.web.filter;

//import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.netbank.rest.web.context.UserContext;

import fstop.util.PathSetting;
import fstop.util.SpringBeanFactory;
import fstop.util.thread.WebServletUtils;
import fstop.va.EnvPathRegister;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SysFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        log.info("FSTOP SysFilter init ....");

        ServletContext servletContext = filterConfig.getServletContext();
        String prefix = servletContext.getRealPath("/");

        EnvPathRegister.registerServletContext(servletContext);
        EnvPathRegister.registerTranslatorPath(prefix + "/WEB-INF/field-mapping.properties");
        EnvPathRegister.registerAdmoplogTranslatorPath(prefix + "/WEB-INF/admoplog-mapping.properties");
        EnvPathRegister.registerB201TranslatorPath(prefix + "/WEB-INF/B201-mapping.properties");
        try {
            PathSetting pathSetting = (PathSetting) SpringBeanFactory.getBean("pathSetting");
            new File(pathSetting.getCfgPath()).mkdirs();
            new File(pathSetting.getAdvPath()).mkdirs();
            new File(pathSetting.getLogsPath()).mkdirs();
            new File(pathSetting.getTmpPath()).mkdirs();
            new File(pathSetting.getTransferDataPath()).mkdirs();
            new File(pathSetting.getCachePath()).mkdirs();
        }
        catch(Exception e) {
            log.error("SysFilter INIT Error. ", e);
        }
//        LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();

//		try {

//		String resourceName = "logback-"
//		+ filterConfig.getServletContext().getServletContextName()
//		+ ".xml";

//		JoranConfigurator configurator = new JoranConfigurator();
//		configurator.setContext(lc);
//		// the context was probably already configured by default
//		// configuration
//		// rules
//		lc.shutdownAndReset();
//		logger.info("LogBack Resource Xml File :" + logger);
//		configurator.doConfigure(getClass().getClassLoader().getResource(resourceName));
//		} catch (JoranException je) {
//		je.printStackTrace();
//		}
//		StatusPrinter.printIfErrorsOccured(lc);

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        log.info("into to sysfilter ...");
        try {
            HttpServletRequest request = (HttpServletRequest)servletRequest;
            HttpServletResponse response = (HttpServletResponse)servletResponse;
            WebServletUtils.initThreadLocal((HttpServletRequest)request, (HttpServletResponse)response);

            UserContext.setRequest((HttpServletRequest)servletRequest);
            UserContext.setResponse((HttpServletResponse)servletResponse);


//            String version = request.getHeader("app-version");
//            log.info("app-version: " + version);

//            if(StrUtils.isEmpty(version)) {
//                // 回傳錯誤
//                // 回傳 EmptyAppVersion
//                MessageResponse msgResponse = new MessageResponse();
//                msgResponse.setMsgCode("EmptyAppVersion");
//                msgResponse.setMessage("");
//                returnErrorToCLient(response, msgResponse);
//
//                return;
//            }

//            try {
//                Version currentVersion = new Version("3.1.1");
//                Version appVersion = new Version(version);

//                if (currentVersion.compareTo(appVersion) > 0) {
                    // 回傳 ErrorVersion
//                    MessageResponse msgResponse = new MessageResponse();
//                    msgResponse.setMsgCode("ErrorVersion");
//                    msgResponse.setMessage("APP版本錯誤");
//
//                    returnErrorToCLient(response, msgResponse);

//                    return;
//                }
//            } catch (Exception e) {
//                log.error("版本比對錯誤.", e);
//            }
//
//            String account = (String)session.getAttribute(Constraint.LOGIN_SESSION_KEY);
//            String opAccount = (String)session.getAttribute(Constraint.OPLOGIN_SESSION_KEY);
//            UserContext.browserIp.set(servletRequest.getRemoteAddr());
//
//            log.info("browser ip: " + servletRequest.getRemoteAddr());
//            if(StrUtils.isNotEmpty(account) || StrUtils.isNotEmpty(opAccount)) {
//                UserContext.setLoginBean(LoginBean.builder()
//                        .account(account)
//                        .opAccount(opAccount)
//                        .build());
//            }


            filterChain.doFilter(servletRequest, servletResponse);
        }
        finally {
            UserContext.setRequest(null);
            UserContext.setResponse(null);
            UserContext.setLoginBean(null);
            WebServletUtils.removeThreadLocal();
        }
    }

    @Override
    public void destroy() {

    }
//
//    private void returnErrorToCLient(HttpServletResponse res, MessageResponse msgResponse) {
//        res.setContentType("application/json; charset=utf-8");
//
//        OutputStream o = null;
//        try {
//            o = res.getOutputStream();
//
//            Gson gson = new GsonBuilder().setPrettyPrinting().create();
//            String json = gson.toJson(msgResponse);
//            o.write(json.getBytes("UTF-8"));
//            o.flush();
//        }
//        catch(Exception e) {
//            e.printStackTrace();
//        }
//        finally {
//            try {
//                if(o != null)
//                    o.close();
//            }
//            catch(Exception e) {}
//        }
//    }

}