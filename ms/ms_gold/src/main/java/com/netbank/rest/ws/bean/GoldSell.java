package com.netbank.rest.ws.bean;

import java.util.List;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
public class GoldSell {

	public String msgCode;
	
	public String message;
	
	public String date;
	
	public String time;
	
	public String bank_no;
	
	public List<GoldSellProduct> goldSellProduct;

	
}
