package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netbank.rest.model.CommonPools;
import com.netbank.util.StrUtils;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.services.CommonService;
import fstop.util.SpringBeanFactory;
import lombok.extern.slf4j.Slf4j;

/**
 * 主要用來
 * 1.丟入MVH前的前置處理，包含檢核;
 * 2.後製MVH回覆的訊息;
 * @author JeffChang
 *
 */
@Service
@Slf4j
public class N960_Tel_Service extends Common_Tel_Service {

	@Autowired
	CommonPools commonPools;
	
	@Override
	public HashMap pre_Processing(Map request) {
		// TODO Auto-generated method stub
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) 
	{
		MVH mvh = new MVHImpl();
		String nCheckNB = request.get("NCHECKNB") == null ? "" : request.get("NCHECKNB").toString();
		String cusidn = String.valueOf(request.get("CUSIDN"));
		try 
		{
			// 判斷要call N960 or N960Pool
			if(nCheckNB.equalsIgnoreCase("Y"))
			{
				mvh = commonPools.n960.getData1(cusidn);
			}
			else
			{
				CommonService service = (CommonService) SpringBeanFactory
						.getBean(String.format("%sService", Optional.ofNullable(serviceID)
								.filter(s -> StrUtils.isNotEmpty(s)).map(s -> s.toLowerCase()).orElse(null)));
				request.put("UID",cusidn);
				mvh = service.doAction(request);
			}
			
			if(StrUtils.isEmpty((String) request.get("LOGINTYPE"))) {
				MVHImpl mvhForRA = (MVHImpl) mvh ;
				mvh = new MVHImpl();
				((MVHImpl) mvh).getFlatValues().put("BRTHDY", mvhForRA.getValueByFieldName("BRTHDY"));
				((MVHImpl) mvh).getFlatValues().put("MAILADDR", mvhForRA.getValueByFieldName("MAILADDR"));
				((MVHImpl) mvh).getFlatValues().put("MOBTEL", mvhForRA.getValueByFieldName("MOBTEL"));
				((MVHImpl) mvh).getFlatValues().put("TOPMSG", mvhForRA.getValueByFieldName("TOPMSG"));
				
			}
		} 
		catch (TopMessageException e) 
		{
			log.error("process.error", e);
			MVHImpl m = new MVHImpl();
			m.getFlatValues().put("TOPMSG", e.getMsgcode());
			mvh = m;
		} 
		catch (Exception e) 
		{
			log.error("process.error", e);
			throw new RuntimeException(e);
		}
		finally 
		{
			return data_Retouch(mvh);
		}
	}

	@Override
	public HashMap data_Retouch(MVH mvh) {
		// TODO Auto-generated method stub
		return super.data_Retouch(mvh);
	}
	
	@Override
	public void setMsgCode(Map dataMap, Boolean data_Retouch_result) {
		// TODO Auto-generated method stub
		super.setMsgCode(dataMap, data_Retouch_result);
	}
	
}
