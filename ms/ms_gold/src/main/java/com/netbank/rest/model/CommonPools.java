package com.netbank.rest.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.netbank.rest.model.pool.ApplyUtils;
import com.netbank.rest.model.pool.CreditUtils;
import com.netbank.rest.model.pool.FundUtils;
import com.netbank.rest.model.pool.FxUtils;
import com.netbank.rest.model.pool.GoldUtils;
import com.netbank.rest.model.pool.N107Pool;
import com.netbank.rest.model.pool.N810Pool;
import com.netbank.rest.model.pool.N812Pool;
import com.netbank.rest.model.pool.N813Pool;
import com.netbank.rest.model.pool.N920Pool;
import com.netbank.rest.model.pool.N921Pool;
import com.netbank.rest.model.pool.N922Pool;
import com.netbank.rest.model.pool.N924Pool;
import com.netbank.rest.model.pool.N926Pool;
import com.netbank.rest.model.pool.N927Pool;
import com.netbank.rest.model.pool.N960Pool;
import com.netbank.rest.model.pool.N997Pool;
import com.netbank.rest.model.pool.UISelectListPool;

import fstop.services.mgn.B105;
import fstop.services.mgn.B106;
import fstop.va.VAProperties;
import fstop.ws.BHWebServiceTemplate;

//import fstop.model.pool.N107Pool;

@Component
public class CommonPools {
	@Autowired
	public UISelectListPool ui;
	@Autowired
	public  N924Pool n924;
	@Autowired
	public  N922Pool n922;
	@Autowired
	public  N927Pool n927;
	@Autowired
	public  N960Pool n960;
	/*
	public static N921Pool n921 = (N921Pool)FstopBeanFactory.getBean("n921Pool");
	public static N922Pool n922 = (N922Pool)FstopBeanFactory.getBean("n922Pool");
	public static N927Pool n927 = (N927Pool)FstopBeanFactory.getBean("n927Pool");
	public static N960Pool n960 = (N960Pool)FstopBeanFactory.getBean("n960Pool");
	*/
	@Autowired
	public  N921Pool n921;
	@Autowired
	public N920Pool n920;//(N920Pool)FstopBeanFactory.getBean("n920Pool");
	@Autowired
	public N810Pool n810; // = (N810Pool)FstopBeanFactory.getBean("n810Pool");
	@Autowired
	public CreditUtils creditUtils ;//= (CreditUtils)FstopBeanFactory.getBean("creditUtils");
	@Autowired
	public FundUtils fundUtils ;//= (FundUtils)FstopBeanFactory.getBean("fundUtils");
	@Autowired
	public GoldUtils goldUtils ;//= (GoldUtils)FstopBeanFactory.getBean("goldUtils");
	@Autowired
	public N926Pool n926; // = (N926Pool)FstopBeanFactory.getBean("n926Pool");
	@Autowired
	public ApplyUtils applyUtils; // = (ApplyUtils)FstopBeanFactory.getBean("applyUtils");
	@Autowired
	public N997Pool n997;
	/*
	public static NP01Pool np01 = (NP01Pool)FstopBeanFactory.getBean("np01Pool");
	public static N931 n931 = (N931)FstopBeanFactory.getBean("n931");
	public static N930EA n930ea = (N930EA)FstopBeanFactory.getBean("n930ea");
	public static N930EE n930ee = (N930EE)FstopBeanFactory.getBean("n930ee");
	public static N998 n998 = (N998)FstopBeanFactory.getBean("n998");
	public static N999 n999 = (N999)FstopBeanFactory.getBean("n999");
	public static SysOpPool sysop = (SysOpPool)FstopBeanFactory.getBean("sysOpPool");
	public static CommonService p018 = (CommonService)FstopBeanFactory.getBean("p018");
	public static FundUtils fundUtils = (FundUtils)FstopBeanFactory.getBean("fundUtils");
	public static FxUtils fxUtils = (FxUtils)FstopBeanFactory.getBean("fxUtils");
	public static GoldUtils goldUtils = (GoldUtils)FstopBeanFactory.getBean("goldUtils");
	public static LoanUtils loanUtils = (LoanUtils)FstopBeanFactory.getBean("loanUtils");
	public static CreditUtils creditUtils = (CreditUtils)FstopBeanFactory.getBean("creditUtils");
	public static ApplyUtils applyUtils = (ApplyUtils)FstopBeanFactory.getBean("applyUtils");  	
	public static N926Pool n926 = (N926Pool)FstopBeanFactory.getBean("n926Pool");
	*/
	@Autowired
	public FxUtils fxUtils;
	@Autowired
	public N813Pool n813; // = (N813Pool)FstopBeanFactory.getBean("n813Pool");
	@Autowired
	public N812Pool n812; // = (N812Pool)FstopBeanFactory.getBean("n812Pool");
	@Autowired
	public B105 b105;
	@Autowired
	public B106 b106;
	@Autowired
	public N107Pool n107;
	/*
	public static N812Pool n812 = (N812Pool)FstopBeanFactory.getBean("n812Pool");
//	public static N107Pool n107 = (N107Pool)FstopBeanFactory.getBean("n107Pool"); 
	//MGN 
	public static B103 b103 = (B103)FstopBeanFactory.getBean("b103");
	public static B104 b104 = (B104)FstopBeanFactory.getBean("b104");
	public static B401 b401 = (B401)FstopBeanFactory.getBean("b401");
	public static B301 b301 = (B301)FstopBeanFactory.getBean("b301");
	public static B201 b201 = (B201)FstopBeanFactory.getBean("b201");
	public static B105 b105 = (B105)FstopBeanFactory.getBean("b105");
	public static B106 b106 = (B106)FstopBeanFactory.getBean("b106");
	public static B107 b107 = (B107)FstopBeanFactory.getBean("b107");
	public static B110 b110 = (B110)FstopBeanFactory.getBean("b110");
	public static B111 b111 = (B111)FstopBeanFactory.getBean("b111");
	public static B109 b109 = (B109)FstopBeanFactory.getBean("b109");
	public static B501 b501 = (B501)FstopBeanFactory.getBean("b501");
	public static B801 b801 = (B801)FstopBeanFactory.getBean("b801");
	public static B802 b802 = (B802)FstopBeanFactory.getBean("b802");
	public static B901 b901 = (B901)FstopBeanFactory.getBean("b901");
	public static B902 b902 = (B902)FstopBeanFactory.getBean("b902");
	public static B903 b903 = (B903)FstopBeanFactory.getBean("b903");
	public static B905 b905 = (B905)FstopBeanFactory.getBean("b905");
	//MB專用
	public static BM1000 bm1000 = (BM1000)FstopBeanFactory.getBean("bm1000");
	public static BM2000 bm2000 = (BM2000)FstopBeanFactory.getBean("bm2000");
	public static BM3000 bm3000 = (BM3000)FstopBeanFactory.getBean("bm3000");
	public static BM5000 bm5000 = (BM5000)FstopBeanFactory.getBean("bm5000");
	public static BM6000 bm6000 = (BM6000)FstopBeanFactory.getBean("bm6000");
	public static CallOtpStatus callOtpStatus = (CallOtpStatus)FstopBeanFactory.getBean("callOtpStatus");
	//ITR-batch
	public static N021 n021 = (N021)FstopBeanFactory.getBean("n021");
	public static N022 n022 = (N022)FstopBeanFactory.getBean("n022");
	public static N023 n023 = (N023)FstopBeanFactory.getBean("n023");
	public static N024 n024 = (N024)FstopBeanFactory.getBean("n024");
	public static N025 n025 = (N025)FstopBeanFactory.getBean("n025");
	public static N026 n026 = (N026)FstopBeanFactory.getBean("n026");
	public static N030 n030 = (N030)FstopBeanFactory.getBean("n030");
	
	public static NHK1 nhk1 = (NHK1)FstopBeanFactory.getBean("nhk1");
	*/
	//BHWebService
	@Autowired
	public BHWebServiceTemplate bh; // = (BHWebServiceTemplate)FstopBeanFactory.getBean("bhWebServiceTemplate");

	/*
	//cab version
	public static CabVersionProperties cvp = (CabVersionProperties)FstopBeanFactory.getBean("cabVersionProperties");
	*/
	@Autowired
	public VAProperties va; // = (VAProperties) SpringBeanFactory.getBean("vaProperties");

	/*
	public static N027 n027 = (N027)FstopBeanFactory.getBean("n027");
	
	//線上申請信用卡取得user email
	public static NA03Q na03q = (NA03Q)FstopBeanFactory.getBean("na03q");
	//簡訊OTP參數取得
	public static SMSOTPProperties smsotp = (SMSOTPProperties)FstopBeanFactory.getBean("smsotpProperties");
		 */
}
