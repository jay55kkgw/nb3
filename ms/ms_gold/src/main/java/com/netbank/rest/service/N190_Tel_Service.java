package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import org.springframework.stereotype.Service;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class N190_Tel_Service extends Common_Tel_Service {
	@Override
	public HashMap pre_Processing(Map request) {
		// TODO Auto-generated method stub
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) {
		// TODO Auto-generated method stub
		return super.process(serviceID, request);
	}

	@Override
	public HashMap data_Retouch(MVH mvh) {
		MVHImpl mvhimpl = null;
		LinkedList<Row> tmplist = null;
		Boolean result = Boolean.FALSE;
		HashMap<String,Object> dataMap = new HashMap<String,Object>();
		HashMap<String,String> rowMap = new HashMap<String,String>();
		LinkedList<HashMap<String,String>> rowlist = null;
		HashMap<String,Object> tableMap = null;
		LinkedList<HashMap<String,Object>> tableList = null;
    	try {
    		mvhimpl = (MVHImpl) mvh;
    		log.info("getFlatValues>>"+mvhimpl.getFlatValues());
    		log.info("result.getInputQueryData()>>"+mvhimpl.getInputQueryData());
    		log.info("result.getTableKeys()>>"+mvhimpl.getTableKeys());
    		log.info("result.getTableByName(00162965425)>>"+mvhimpl.getTableByName("00162965425"));
    		
    		tableList = new LinkedList<HashMap<String,Object>>();
    		for(Object key:mvhimpl.getTableKeys()) {
    			tableMap = new HashMap<String,Object>();
    			log.info("getTableKeys >> {}",(String)key);
    			log.info("result.getInputQueryData()>> {}",((MVHImpl)mvhimpl.getTableByName((String)key)).getOccurs().getRows());
    			tableMap.putAll(((MVHImpl)mvhimpl.getTableByName((String)key)).getFlatValues());
    			super.setMsgCode(tableMap,  Boolean.TRUE);
    			if(!(tableMap.get("ACN").equals(""))) {
    				tableList.add(tableMap);
    			}
    			else {
    				//錯誤帳號筆數移除
    				Integer tempCMRECNUM=Integer.parseInt(mvhimpl.getFlatValues().get("CMRECNUM"))-1;
    				mvhimpl.getFlatValues().put("CMRECNUM",tempCMRECNUM.toString());
    				log.info("getFlatValues>>"+mvhimpl.getFlatValues().get("CMRECNUM"));
    			}
    		}
			dataMap.putAll(mvhimpl.getFlatValues());
			dataMap.put("REC", tableList);
			result = Boolean.TRUE;
			if(tableList.size()>1) {
				for (HashMap<String, Object> hashMap : tableList)
				{
					if("0".equals(hashMap.get("msgCode"))) {
						dataMap.put("msgCode", "0");
						dataMap.put("TOPMSG", hashMap.get("TOPMSG"));
						dataMap.put("MSGCOD", hashMap.get("MSGCOD"));
						break;
					}
					else {
						dataMap.put("msgCode", hashMap.get("msgCode"));
						dataMap.put("TOPMSG", hashMap.get("TOPMSG"));
						dataMap.put("MSGCOD", hashMap.get("MSGCOD"));
					}
				}
				setMsgCode(dataMap, Boolean.TRUE);

			}else {
				dataMap.put("msgCode", tableMap.get("msgCode"));
				dataMap.put("TOPMSG", tableMap.get("TOPMSG"));
				dataMap.put("MSGCOD", tableMap.get("MSGCOD"));
				setMsgCode(dataMap, Boolean.TRUE);
			}
		} catch (Exception e) {
			log.error("data_Retouch error >> {}",e);
			log.error("",e);
		}
		return dataMap;
	}

	@Override
	public void setMsgCode(Map dataMap, Boolean data_Retouch_result) {
		// TODO Auto-generated method stub
		super.setMsgCode(dataMap, data_Retouch_result);
	}
}
