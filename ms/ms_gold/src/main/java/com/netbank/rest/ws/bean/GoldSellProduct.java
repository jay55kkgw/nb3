package com.netbank.rest.ws.bean;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
public class GoldSellProduct {

	public String sale_kind;
	
	public String goodno;
	
	public String unit_price;
	
	public String sale_amt;
	
	public String tax_amt;
	
	public String discount_amt;
	
	public String sale_q;
	
	public String take_q;
	
	public String unit_price_g;
	
	public String dpdiff;

	
	
	
}
