package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.netbank.rest.model.FieldTranslator;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import lombok.extern.slf4j.Slf4j;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

/**
 * 主要用來
 * 1.丟入MVH前的前置處理，包含檢核;
 * 2.後製MVH回覆的訊息;
 * @author JeffChang
 *
 */
@Service
@Slf4j
public class N092_Tel_Service extends Common_Tel_Service  {

	@Override
	public HashMap pre_Processing(Map request) {
		// TODO Auto-generated method stub
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) {
		// TODO Auto-generated method stub
		return super.process(serviceID, request);
	}

	@Override
	public HashMap data_Retouch(MVH mvh) {
		MVHImpl mvhimpl = null;
		LinkedList<Row> tmplist = null;
		Boolean result = Boolean.FALSE;
		HashMap<String,Object> dataMap = new HashMap<String,Object>();
		HashMap<String,String> rowMap = new HashMap<String,String>();
		LinkedList<HashMap<String,String>> rowlist = null;
    	try {
    		mvhimpl = (MVHImpl) mvh;
    	log.info("getFlatValues>>"+mvhimpl.getFlatValues());
    	log.info("result.getInputQueryData()>>"+mvhimpl.getInputQueryData());
    	log.info("result.getInputQueryData()>>"+mvhimpl.getOccurs().getRows());
			Iterator<Row> tmp = mvhimpl.getOccurs().getRows().iterator();
			tmplist = new LinkedList<Row>();
			rowlist = new LinkedList<HashMap<String,String>>();
			tmp.forEachRemaining(tmplist::add);
			
			log.info("datalist()>>"+tmplist);
			for(Row  row:tmplist) {
				log.info("getValues>>"+row.getValues());
				rowMap = new HashMap<String,String>();
				rowMap.putAll(row.getValues());
				rowMap.put("STATUS_code", rowMap.get("STATUS"));
				rowMap.put("STATUS", FieldTranslator.transfer("N092", "STATUS", rowMap.get("STATUS")));
				rowlist.add(rowMap);
			}
			dataMap.putAll(mvhimpl.getFlatValues());
			dataMap.put("REC", rowlist);
			result = Boolean.TRUE;
			
		} catch (Exception e) {
			log.error("data_Retouch error >> {}",e);
		}finally {
			setMsgCode(dataMap,result);
		}
		return dataMap;
	}
	
	@Override
	public void setMsgCode(Map dataMap, Boolean data_Retouch_result) {
		String val = "";
		
		try {
			log.trace("data_Retouch_result>>{}",data_Retouch_result);
			log.trace(ESAPIUtil.vaildLog("dataMap>>{}"+dataMap));
//			預設就先設定是錯誤的狀況
			dataMap.put("msgCode", "FE0003");
			
			if(!data_Retouch_result) {
				return;
			}
			
			for (String key : list) {
				if (dataMap.containsKey(key)) {
					val = (String) dataMap.get(key);
					log.trace(ESAPIUtil.vaildLog("val>>{}"+val));
					if ("OKLR".equals(val) || "OKOK".equals(val) || "".equals(val) || "0000".equals(val)||"OKOV".equals(val)) {
						dataMap.put("msgCode", "0");
					}else {
						dataMap.put("msgCode", dataMap.get(key));
						break;
					}
				} // if end
			} // for end
			//新增 根據訊息代碼取得訊息
			if(null == dataMap.get("msgCode") || "0".equals(dataMap.get("msgCode"))) {
				log.debug(ESAPIUtil.vaildLog("dataMap.get(msgcode) >>>>>>> {}"+dataMap.get("msgCode")));
			}else {
				dataMap.put("msgName", getMessageByMsgCode(String.valueOf(dataMap.get("msgCode"))));
			}
			
			
		} catch (Exception e) {
			log.error("setMsgCode error >> {}",e);
			dataMap.put("msgCode", "FE0003");
		}
	}
	
}
