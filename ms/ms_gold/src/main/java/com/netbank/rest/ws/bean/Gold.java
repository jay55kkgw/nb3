package com.netbank.rest.ws.bean;

import java.util.List;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@Data
public class Gold {
	
	public String date;
	
	public String cnt;
	
	public String time;
	
	public String kind;
	
	public List<GoldProduct> goldProduct;


	
}
