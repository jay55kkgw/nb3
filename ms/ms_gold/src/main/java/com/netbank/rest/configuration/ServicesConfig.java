package com.netbank.rest.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import fstop.services.ServerConfig;
import fstop.services.TopMsgDBDetecter;
import fstop.services.TopMsgF003Detecter;
import fstop.services.TopMsgF007Detecter;
import fstop.services.TopMsgFXDetecter;
import fstop.services.TopMsgFundDetecter;
import fstop.services.TopMsgN320Detecter;
import fstop.services.TopMsgN950Detecter;
import fstop.services.batch.CheckServerHealth;
import fstop.services.batch.GD11;
import fstop.services.impl.CallMobileCenter;
import fstop.services.impl.GD011;
import fstop.services.impl.GoldService;
import fstop.services.impl.N090_01;
import fstop.services.impl.N090_02;
import fstop.services.impl.N091_01;
import fstop.services.impl.N091_02;
import fstop.services.impl.N091_03;
import fstop.services.impl.N092;
import fstop.services.impl.N093_01;
import fstop.services.impl.N093_02;
import fstop.services.impl.N094;
import fstop.services.impl.N110;
import fstop.services.impl.N190;
import fstop.services.impl.N191;
import fstop.services.impl.N192;
import fstop.services.impl.N850A;
import fstop.services.impl.N960;
import fstop.services.impl.NA50;
import fstop.services.impl.NA60;
import fstop.services.mgn.B105;
import fstop.services.mgn.B106;
import fstop.services.mgn.NA20;
import fstop.va.VATransform;
import fstop.ws.eai.client.N701WebServiceClient;
import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class ServicesConfig {
	
	
//	@Bean
//	public Gold gold() {
//		return new Gold();
//	}

	/**
	 * 台銀2台企 黃金報價
	 * @return
	 * @throws Exception
	 */
    @Bean
    protected GoldService goldservice() throws Exception {
        return new GoldService();
    }
    
    @Bean
    protected VATransform vatransform() throws Exception {
    	return new VATransform();
    }
    @Bean
    protected CheckServerHealth checkserverhealth() throws Exception {
    	return new CheckServerHealth();
    }
    @Bean
    protected CallMobileCenter callmobilecenterService() throws Exception {
    	return new CallMobileCenter();
    }
    @Bean
    protected N701WebServiceClient n701WebServiceClient() throws Exception {
        return new N701WebServiceClient();
    }
	
    @Bean
    protected TopMsgDBDetecter dbErrDetect() throws Exception {
        return new TopMsgDBDetecter();
    }

    @Bean
    protected TopMsgN320Detecter n320ErrDetect() throws Exception {
        return new TopMsgN320Detecter();
    }

    @Bean
    protected TopMsgFXDetecter fxErrDetect() throws Exception {
        return new TopMsgFXDetecter();
    }

    @Bean
    protected TopMsgF003Detecter f003ErrDetect() throws Exception {
        return new TopMsgF003Detecter();
    }

    @Bean
    protected TopMsgF007Detecter f007ErrDetect() throws Exception {
        return new TopMsgF007Detecter();
    }

    @Bean
    protected TopMsgFundDetecter fundErrDetect() throws Exception {
        return new TopMsgFundDetecter();
    }
    @Bean
    protected TopMsgN950Detecter n950ErrDetect() throws Exception {
        return new TopMsgN950Detecter();
    }
    @Bean
    protected B105 b105Service() throws Exception {
        return new B105();
    }
    
    @Bean
    protected B106 b106Service() throws Exception {
        return new B106();
    }
	
    @Bean
    protected GD011 gd011Service() throws Exception {
        return new GD011();
    }
    
    @Bean
    protected GD11 gd11Service() throws Exception {
        return new GD11();
    }
    
    @Bean
    protected NA20 na20Service() throws Exception {
        return new NA20();
    }
    
    @Bean
    protected NA50 na50Service() throws Exception {
        return new NA50();
    }
    
    @Bean
    protected NA60 na60Service() throws Exception {
        return new NA60();
    }
    
    @Bean
    protected N090_01 n090_01Service() throws Exception {
        return new N090_01();
    }

    @Bean
    protected N090_02 n090_02Service() throws Exception {
        return new N090_02();
    }
    
    @Bean
    protected N091_01 n091_01Service() throws Exception {
        return new N091_01();
    }
    
    @Bean
    protected N091_02 n091_02Service() throws Exception {
        return new N091_02();
    }
    
    @Bean
    protected N091_03 n091_03Service() throws Exception {
        return new N091_03();
    }

    @Bean
    protected N092 n092Service() throws Exception {
        return new N092();
    }

    @Bean
    protected N093_01 n093_01Service() throws Exception {
        return new N093_01();
    }
    
    @Bean
    protected N093_02 n093_02Service() throws Exception {
        return new N093_02();
    }
    
    @Bean
    protected N094 n094Service() throws Exception {
        return new N094();
    }
    
    @Bean
    protected N110 n110Service() throws Exception {
        return new N110();
    }

    @Bean
    protected N190 n190Service() throws Exception {
        return new N190();
    }
    
    @Bean
    protected N191 n191Service() throws Exception {
        return new N191();
    }
    
    @Bean
    protected N192 n192Service() throws Exception {
        return new N192();
    }
    
	@Bean
    protected N850A n850aService() throws Exception {
        return new N850A();
    }
    
    @Bean
    protected N960 n960Service() throws Exception{
    	return new N960();
    }
    //<!-- 後台OTP狀態 -->
    @Bean
    protected ServerConfig serverConfig() throws Exception {
        return new ServerConfig();
    }
}
