package com.netbank.rest.ws.bean;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
public class GoldProduct {
	
	public String currec;
	
	public String totrec;
	
	public String goodno;
	
	public String sell;
	
	public String sellt;
	
	public String buy;
	
	public String dpdiff;

	@Override
	public String toString() {
		return "GoldProduct [currec=" + currec + ", totrec=" + totrec + ", goodno=" + goodno + ", sell=" + sell
				+ ", sellt=" + sellt + ", buy=" + buy + ", dpdiff=" + dpdiff + "]";
	}

	
}
