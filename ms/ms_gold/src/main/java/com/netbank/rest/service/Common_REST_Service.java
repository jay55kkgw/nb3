package com.netbank.rest.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netbank.rest.ws.bean.Gold;
import com.netbank.rest.ws.bean.GoldSell;
import com.netbank.util.CodeUtil;
import com.netbank.util.StrUtils;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.orm.dao.AdmMsgCodeDao;
import fstop.orm.po.ADMMSGCODE;
import fstop.services.CommonService;
import fstop.services.impl.GoldService;
import fstop.util.SpringBeanFactory;
import lombok.extern.slf4j.Slf4j;
@Slf4j
@Service
public  class Common_REST_Service implements Tel_Service{

	@Autowired
	private AdmMsgCodeDao admMsgCodeDao;
	
	final public  String  ABEND ="ABEND";
	final public  String  RSPCOD ="RSPCOD";
	final public  String  RSPCODE ="RSPCODE";
	final public  String  TOPMSG ="TOPMSG";
	final public  String  MSGCOD ="MSGCOD";
	final public  String  RESPCOD ="RESPCOD";
	final public  String  RESPCODS ="Respcode";
	final public  String  ERRCODE ="ERRCODE";
	String[] msgkey = {ABEND,RSPCOD,RSPCODE,TOPMSG,MSGCOD,RESPCOD,ERRCODE,RESPCODS};
	List<String> list = Arrays.asList(msgkey);
	
	
	
	@Override
	public HashMap pre_Processing(Map request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HashMap process(String serviceID ,Map request) {
		HashMap map = null;
		Gold gold  = null;
		try {
			
			log.trace("aaa>>{}",String.format("%s",Optional.ofNullable(serviceID)
                    .filter(s -> StrUtils.isNotEmpty(s)).map(s -> s.toLowerCase())
                    .orElse(null)));
			
			GoldService service = (GoldService)SpringBeanFactory.getBean(
			        String.format("%s",
			                Optional.ofNullable(serviceID)
			                    .filter(s -> StrUtils.isNotEmpty(s)).map(s -> s.toLowerCase())
			                    .orElse(null)));
			
			gold  = CodeUtil.objectCovert(Gold.class, request);
			
			GoldSell goldsell  = service.informGoldValue(gold);
			
			map = CodeUtil.objectCovert(HashMap.class,goldsell);
		} catch (Exception e) {
			log.error("process.error>>{}",e);
		}finally {
//			TODO true 做data_Retouch false 做msgCode 回覆處理
			return map;
//			return data_Retouch(mvh);
		}
		
	}

	@Override
	public HashMap data_Retouch(MVH mvh) {
		MVHImpl mvhimpl = null;
		LinkedList<Row> tmplist = null;
		Boolean result = Boolean.FALSE;
		HashMap<String,Object> dataMap = new HashMap<String,Object>();
		HashMap<String,String> rowMap = new HashMap<String,String>();
		LinkedList<HashMap<String,String>> rowlist = null;
    	try {
    		mvhimpl = (MVHImpl) mvh;
    	log.info("getFlatValues>>"+mvhimpl.getFlatValues());
    	log.info("result.getInputQueryData()>>"+mvhimpl.getInputQueryData());
    	log.info("result.getInputQueryData()>>"+mvhimpl.getOccurs().getRows());
			Iterator<Row> tmp = mvhimpl.getOccurs().getRows().iterator();
			tmplist = new LinkedList<Row>();
			rowlist = new LinkedList<HashMap<String,String>>();
			tmp.forEachRemaining(tmplist::add);
			
			log.info("datalist()>>"+tmplist);
			for(Row  row:tmplist) {
				log.info("getValues>>"+row.getValues());
				rowMap = new HashMap<String,String>();
				rowMap.putAll(row.getValues());
				rowlist.add(rowMap);
			}
			dataMap.putAll(mvhimpl.getFlatValues());
			dataMap.put("REC", rowlist);
			result = Boolean.TRUE;
			
		} catch (Exception e) {
			log.error("data_Retouch error >> {}",e);
		}finally {
			if(result) {
				dataMap.put("msgCode", "0");
			}else {
				dataMap.put("msgCode", "FE0002");
			}
			
			
		}
		return dataMap;
	}

	public void setMsgCode(Map dataMap,Boolean data_Retouch_result) {
		String val = "";
		
		try {
			log.trace("data_Retouch_result>>{}",data_Retouch_result);
			log.trace("dataMap>>{}",dataMap);
//			預設就先設定是錯誤的狀況
			dataMap.put("msgCode", "FE0003");
			
			if(!data_Retouch_result) {
				return;
			}
			
			for (String key : list) {
				if (dataMap.containsKey(key)) {
					val = (String) dataMap.get(key);
					log.trace("val>>{}",val);
					
					if ("R000".equals(val) || "OKLR".equals(val) || "OKOK".equals(val) || "".equals(val) 
							||"0000000".equals(val)|| "0000".equals(val)||"OKOV".equals(val)) {
						
						dataMap.put("msgCode", "0");
						
					} else {
						dataMap.put("msgCode", dataMap.get(key));
					}
					break;
				} // if end
			} // for end
			
			//新增 根據訊息代碼取得訊息
			if(null == dataMap.get("msgCode") || "0".equals(dataMap.get("msgCode"))) {
				log.debug("dataMap.get(msgcode) >>>>>>> {}",dataMap.get("msgCode"));
			}else {
				dataMap.put("msgName", getMessageByMsgCode(String.valueOf(dataMap.get("msgCode"))));
			}
			
			
		} catch (Exception e) {
			log.error("setMsgCode error >>{}",e);
			dataMap.put("msgCode", "FE0003");
		}
	}
	
	
	/**
	 * 根據訊息代碼取得訊息
	 * 
	 * @param msgCode
	 * @return
	 */
	public String getMessageByMsgCode(String msgCode) {
		String message = "";
		ADMMSGCODE po = null;
		try {
				log.debug("getMessageByMsgCode msgCode >>>{}", msgCode);
				po = admMsgCodeDao.findById(msgCode);
				message = po.getADMSGOUT();
		} catch (Exception e) {
			log.error("getMessageByMsgCode error", e);
		}
		log.trace("message>>{}", message);
		return message;
	}
	
}
