package com.netbank.rest.configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@Slf4j
@PropertySource(value = "${profile.path}/va.properties${profile.active}",  ignoreResourceNotFound = true, encoding = "UTF-8")
@PropertySource(value = "${profile.path}/bh.properties${profile.active}",  ignoreResourceNotFound = true, encoding = "UTF-8")
@PropertySource(value = "${profile.path}/batch.properties${profile.active}",  ignoreResourceNotFound = true, encoding = "UTF-8")
@PropertySource(value = "${profile.path}/ftppath.properties${profile.active}",  ignoreResourceNotFound = true, encoding = "UTF-8")
@PropertySource(value = "${profile.path}/fisc.properties${profile.active}",  ignoreResourceNotFound = true, encoding = "UTF-8")
@PropertySource(value = "${profile.path}/ebill.properties${profile.active}",  ignoreResourceNotFound = true, encoding = "UTF-8")

public class PropertiesConfig {


}
