package bank.comm;

import java.util.*;
/**
 *實作FSTopResultSet介面以提供一可對已處理過之下行電文資料做存取之物件<br>
*如同java.sql.ResultSet,ResultSetImpl提供cursor以存取回傳資料中之各筆資料,<br>
*亦可將回傳資料轉為java.util.Vector物件以供處理
* @author 林季穎
 * @version 1.0
 * @see bank.comm.FSTopResultSet
 */
public class ResultSetImpl implements FSTopResultSet{
    /** cursor目前所在之位置
     */    
    private int count = 0;
    /** cursor在第一筆資料之前
     */    
    private final int BEFOREFIRST = 0;
    /** 資料總筆數
     */    
    private final int totalAmount = this.size();
    /** cursor在最後一筆資料之後
     */    
    private final int AFTERLAST = totalAmount+1;
    /** java.util.Vector物件,存放下行電文資料
     */    
    private Vector resultVector = new Vector();
    /**
        *將cursor移至所指定的資料位置
        *藉由一FSTopResultSet介面回傳
        *@param row 指定cursor至指定的第row筆資料
        * @return 如果cursor還在第一筆及最後一筆資料之間時,回傳true,否則回傳false
        * @throws CommunicationException 當FSTopResultSet中無資料(資料總筆數=0)時,丟出CommunicationException
        */
    public boolean absolute(int row) throws CommunicationException{
        boolean result = false;
        if(row==0){
            throw new CommunicationException("Index can not be 0");
        }
        if(row<0 && row>=(-1)*totalAmount){
            count = row+totalAmount+1;
            result = true;
        }else if(row<(-1)*totalAmount){
            count = BEFOREFIRST;
            result = false;
        }else if(row>0 && row<=totalAmount){
            count = row;
            result = true;
        }else if(row>totalAmount){
            count = AFTERLAST;
            result = false;
        }
        return result;
    }
    /**
        *將cursor移至最後一筆資料之後
        */
    public void afterLast(){
        count = AFTERLAST;
    }
    /**
        *將cursor移至第一筆資料之前
        */
    public void beforeFirst(){
        count = BEFOREFIRST;
    }
     /**
        *將cursor移至第一筆資料
        * @return 當FSTopResultSet中無資料,回傳false,否則回傳true
        */
    public boolean first(){
        if(resultVector!=null){
            count = 1;
            return true;
        }else{
            return false;
        }
    }
    /**
     * 回傳一筆資料中指定欄位名稱之值
     * @return 指定欄位名稱對應之值
     * @throws CommunicationException 當FSTopResultSet中無資料或該欄位名稱不存在時,丟出CommunicationException
     * @param varName 指定欄位名稱
     */
    public String getString(String varName) throws CommunicationException{
        if(resultVector!=null){
            Hashtable targetTable = (Hashtable)resultVector.get(count-1);
            String result = (String)targetTable.get(varName);
            if(result==null){
                throw new CommunicationException("No such key : "+varName);
            }
            return result;
        }else{
            throw new CommunicationException("Nothing in the CommResultSet");
        }
    }
    /**
        *回傳cursor目前所在之位置,若為第一筆資料,則回傳1,若為第二筆資料,則回傳2,以此類推
        * @return cursor目前所在之位置
        */
    public int getRow(){
        return count;
    }
    /**
        *檢查cursor是否位在最後一筆資料之後
        * @return 當cursor位在最後一筆資料之後,回傳true,否則回傳false
        */
    public boolean isAfterLast(){
        if(count==AFTERLAST){
            return true;
        }else{
            return false;
        }
    }
    /**
        *檢查cursor是否位在第一筆資料之前
        * @return 當cursor位在第一筆資料之前,回傳true,否則回傳false
        */
    public boolean isBeforeFirst(){
        if(count==BEFOREFIRST){
            return true;
        }else{
            return false;
        }
    }
     /**
        *檢查cursor是否位在第一筆資料
        * @return 當cursor位在第一筆資料,回傳true,否則回傳false
        */
    public boolean isFirst(){
        if(count==1){
            return true;
        }else{
            return false;
        }
    }
    /**
        *檢查cursor是否位在最後一筆資料
        * @return 當cursor位在最後一筆資料,回傳true,否則回傳false
        */
    public boolean isLast(){
        if(count==totalAmount){
            return true;
        }else{
            return false;
        }
    }
    /**
        *將cursor移至最後一筆資料
        * @return 當FSTopResultSet中無資料,回傳false,否則回傳true
        */
    public boolean last(){
        if(resultVector!=null){
            count = totalAmount;
            return true;
        }else{
            return false;
        }
    }    
    /**
        *將cursor移至下一筆資料
        * @return 當已無下一筆資料時,回傳false,否則回傳true
        */
    public boolean next(){
    	if (!(count == (totalAmount))){
    	    count+=1;
    	    return true;
    	}else{
    	    return false;
    	}
    }
    /**
        *回傳FSTopResultSet中資料總筆數
        * @return 資料總筆數
        */
    public int size(){
    	return totalAmount;
    }
    /**
        *將FSTopResultSet中之資料轉為java.util.Vector物件回傳
        * @return java.util.Vector物件,Vector中各元素為java.util.Hashtable物件,每個java.util.Hashtable物件包含一筆下傳電文之內容
        */
    public Vector toVector(){
        return this.resultVector;
    }
    /**
        *將傳入之java.util.Vector物件的內容紀錄於ResultSetImpl物件中
        *@param input 交易結果內容
        */
    public void setElement(Vector input){
        this.resultVector = input;
    }
}



