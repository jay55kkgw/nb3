package com.netbank.util;

import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DateUtils {
	static SimpleDateFormat dtF = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	static SimpleDateFormat dtD = new SimpleDateFormat("yyyyMMdd");
	static SimpleDateFormat dtT = new SimpleDateFormat("HHmmss");
	
    public static Date beEnd(Date d) {
        DateTime dt = new DateTime(d);
        dt = dt.plusMinutes(1);
        return dt.toDate();
    }

    public static Date beStart(Date d) {
        DateTime dt = new DateTime(d);
        return dt.toDate();
    }

    public static String formatDate(Date date, String pattern) {
        return formatDate(date, pattern, (TimeZone)null);
    }

    public static String formatDate(Date date, String pattern, TimeZone timeZone) {
        return formatDate(date, pattern, timeZone, (Locale)null);
    }

    public static String formatDate(Date date, String pattern, TimeZone timeZone, Locale locale) {
        if (date == null) {
            return null;
        } else {
            SimpleDateFormat sdf;
            if (locale == null) {
                sdf = new SimpleDateFormat(pattern);
            } else {
                sdf = new SimpleDateFormat(pattern, locale);
            }

            if (timeZone != null) {
                sdf.setTimeZone(timeZone);
            }

            return sdf.format(date);
        }
    }

    public static Date parseDate(String date, String pattern) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            return sdf.parse(date);
        } catch(Exception e) {
            throw new RuntimeException("parseDate Error. (date: " + date + ", pattern: " + pattern, e);
        }
    }
    /**
	 * 將 1080201 格式的日期, 變成 108/02/01
	 */
	public static String addSlash2(String dateShort) {
		return dateShort.substring(0, 3) + "/" + dateShort.substring(3, 5) + "/" + dateShort.substring(5, 7);
	}
	/**
	 * 回傳 0971011
	 * @param d
	 * @return
	 */
	public static String getCDateShort(String text) {
		String result = text.substring(4);
		result = new Integer(text.substring(0, 4)) - 1911 + result;
		if(result.length() == 6)
			result = "0" + result;
		return result;
	}
	
	/**
	 * 
	 *  <p>傳回格式化(HH:MM:SS)時間字串</p>
	 * @return a_str_time:未格式化之時間字串
	 * @see  old-core\DisplayUtil.java 
	 */  	 
	public static String formatTime(String a_str_time)
	{		
		try
		{
			if (a_str_time == null || a_str_time.equals(""))
				return "";
			
			//若傳入字串大於六位，後面無條件捨去
			return a_str_time.substring(0, 2) + ":" + a_str_time.substring(2, 4) + ":" + a_str_time.substring(4, 6);
		}
		catch (Exception exc) 
		{
			return "時間格式錯誤";
		}
	}
	
	/**
	 * 取得時間差:單位毫秒
	 * @param d1
	 * @param d2
	 * @return
	 */
	public static String getDiffTimeMills(DateTime d1 ,DateTime d2) {
		String ret ="";
		try {
			Long l = d1.getMillis() - d2.getMillis();
			BigDecimal bd = new BigDecimal(l).divide(new BigDecimal(1000)).abs().setScale(3);
			ret = bd.toString();
		} catch (Exception e) {
			log.error("{}",e);
		}
		return ret;
	}
	
}
