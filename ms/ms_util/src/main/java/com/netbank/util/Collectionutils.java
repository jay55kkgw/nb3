package com.netbank.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Collectionutils
{

	/**
	 * 移除 空的Map 或是 KEY為空的Map
	 * 勿使用Arrays.asList(arr) 轉換的 List 因為不能進行 add 和 remove 。
	 * @param rowlist
	 * @param str
	 * @return
	 * @exception java.lang.UnsupportedOperationException
	 */
	public static Collection<HashMap<String, String>> removeMapEmptyValue(Collection<HashMap<String, String>> rowlist,
			String str)
	{
		rowlist.removeIf(Map::isEmpty);// 移除空白 OR null的Map
		rowlist.removeIf(Map -> null == Map.get(str) || "".equals(Map.get(str)));// 移除ACN==空白 OR null的Map
		return rowlist;
	}

////	// 測試
//	public static void main(String[] args)
//	{
//		Map<String, String> map = new HashMap<>();
//		map.put("ACN", "");
//		map.put("A", "");
//		map.put("B", "B");
//		map.put("C", "C");
//		Map<String, String> map2 = new HashMap<>();
//		map2.put("ACN", "123456789");
//		Map<String, String> map3 = new HashMap<>();
//		List<Map<String, String>> list = new ArrayList<>();
//		list.add(map);
//		list.add(map2);
//		list.add(map3);
//		log.debug("index : " + list.size());
//		log.debug("list 處理前: " + list);
//		removeMapEmptyValue(list, "ACN");
//		log.debug("list 處理後: " + list);
//	}

}
