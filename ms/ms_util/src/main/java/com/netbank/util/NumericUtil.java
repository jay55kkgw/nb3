package com.netbank.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class NumericUtil {

	/**
	 * 數字字串加上千分位及希望的小數位數
	 * 
	 * @param text
	 *            輸入字串
	 * @param digital
	 *            小數幾位 若電文回傳為三位數,請至少輸入3以上以防誤差
	 * @return
	 */
	public static String fmtAmount(String text, int digits) {
		// 若text為null轉成0
		DecimalFormat df = null;
		BigDecimal number = null;
		try {
			text = StrUtils.isEmpty(text) ? "0" : text;
			df = null;
			String zeros = "";
			// 若text有帶","先移掉
			if (text.contains(",")) {
				text = text.replaceAll(",", "");
			}
			// 景毅有遇到數字尾帶負號的，多個判斷式
			if (text.substring(text.length() - 1).equals("-")) {
				text = text.substring(0, text.length() - 1);
			}

			if (digits != 0) {
				if (text.indexOf(".") > 0) {

					for (int i = 0; i < digits; i++) {
						zeros += "0";
					}
					df = new DecimalFormat("###,##0." + zeros);
				} else {
					for (int i = 0; i < digits; i++) {
						zeros += "0";
					}
					df = new DecimalFormat("###,##0." + zeros);
				}
			} else {
				df = new DecimalFormat("###,##0");
			}

			number = new BigDecimal(text);

		} catch (Exception e) {
			// TODO Auto-generated catch block
		}
		return df.format(number);
	}
	
	//直接加小數點
	public static String addDot(String text, int digits) {
		String pre = text.substring(0, text.length()-digits);
		String after =  text.substring(text.length()-digits);
		text = pre +"." +after;
		return text;
	}
	
//	public static void main(String[] args) {
//		log.debug(fmtAmount("0000500000000",2));
//		String test = String.valueOf((Integer.parseInt("0000143")));
//		log.debug(test);
//		log.debug(addDot(" 2345",3));
//	}
}