package com.netbank.util;

/**
 * 提供共用的系統訊息代碼
 * 
 *
 */
public class ResultCode {
	
	public static int SUCCESS = 200;
	
	public static int RECORD_NOT_FOUND = 1001;
	
//	系統正確代碼  解決沒有電文或電文未設定回覆訊息用
//	變數名稱 SYS_開頭，代碼SYS開頭
	public static String SYS_LOGIN = "SYS0001"; //登入成功
	public static String SYS_LOGOUT = "SYS0002"; //登出成功
	
	//通用
	public static String SYS_SUCCESS = "SUCCESS0001";//執行成功
	
//	系統錯誤代碼 FE開頭 ，解決沒有電文或電文未設定回覆訊息用	
//	變數名稱 SYS_ERROR開頭，代碼FE開頭
	public static String SYS_ERROR = "FE0001";
//	電文回覆異常
	public static String SYS_ERROR_TEL = "FE0002";
//	public static String SYS_ERROR_MESSAGE = "系統異常，請洽相關資訊人員。";
	public static String SYS_ERROR_MESSAGE = "";
//	EAI電文異常錯誤訊息
	public static String FE0004 = "FE0004";
//	EAI下行電文異常錯誤訊息
	public static String FE0005 = "FE0005";
}
