package com.netbank.util.fstop;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.joda.time.DateTime;

import com.netbank.util.StrUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DateUtil {
	public static int INTERCONVERSION = 1;
	public static int NOT_INTERCONVERSION = 2;
	/**
	 * 取得民國年
	 * 
	 * @return
	 */
	public static String getCurrentTWYear(){
		Calendar cal = Calendar.getInstance();
		String year = String.valueOf(cal.get(Calendar.YEAR)-1911);
		return year;
	}
	/**
	 * 取得目前民國年月日(YYYMMDD)
	 * @return
	 */
	public static String getTWDate(String sign){	
		if(StrUtils.isEmpty(sign)){sign="";}
		Calendar cal = Calendar.getInstance();	
		String theDate = null;
		String month = String.valueOf((cal.get(Calendar.MONTH)+1)/10) +""+ String.valueOf((cal.get(Calendar.MONTH)+1)%10);	
		String day= String.valueOf(cal.get(Calendar.DAY_OF_MONTH)/10) +""+ String.valueOf(cal.get(Calendar.DAY_OF_MONTH)%10);
			   theDate = getCurrentTWYear() +sign+ month+sign+ day;
		return theDate;
	}

	public static String getCurentDateTime(String pattern){
		pattern = StrUtils.isNotEmpty(pattern)?pattern:"yyyyMMddHHmmss";
		SimpleDateFormat sdFormat = new SimpleDateFormat(pattern);
	    Date current = new Date();
	    String d = sdFormat.format(current);
		return d;
	}
	
	
	/**
	 * 去得目前時間(HHMMSS)
	 * @param sign
	 * @return
	 */
	public static String getTheTime(String sign){
		if(StrUtils.isEmpty(sign)){sign="";}
		Calendar cal = Calendar.getInstance();
		String theTime = null;
		String theHours = String.valueOf(cal.get(Calendar.HOUR_OF_DAY)/10) +""+ String.valueOf(cal.get(Calendar.HOUR_OF_DAY)%10);
		String theMinutes = String.valueOf(cal.get(Calendar.MINUTE)/10) +""+ String.valueOf(cal.get(Calendar.MINUTE)%10);
		String theSeconds = String.valueOf(cal.get(Calendar.SECOND)/10) +""+ String.valueOf(cal.get(Calendar.SECOND)%10);
		theTime = theHours +sign+ theMinutes +sign+ theSeconds;
		return theTime;
	}
	
	
	public static String getDate(String sign){	
		if(StrUtils.isEmpty(sign)){sign="";}
		Calendar cal = Calendar.getInstance();	
		String theDate = null;
		String month = String.valueOf((cal.get(Calendar.MONTH)+1)/10) +""+ String.valueOf((cal.get(Calendar.MONTH)+1)%10);	
		String day= String.valueOf(cal.get(Calendar.DAY_OF_MONTH)/10) +""+ String.valueOf(cal.get(Calendar.DAY_OF_MONTH)%10);
			   theDate = cal.get(Calendar.YEAR) +sign+ month+sign+ day;
			   //log.debug("theDate>>"+theDate);
		return theDate;
	}
	
	/**
	 * 取得下個日期
	 * @param retPattern 輸入回傳的格式 ex yyyy/MM/dd =2016/07/16
	 * @return
	 */
	public static String getNextDate(String  retPattern) {
		  SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		  Calendar calendar = null;
		  Date date = null;
		try {
			date = format.parse(getDate(""));
			calendar = Calendar.getInstance();
			calendar.setTime(date);
			calendar.add(Calendar.DAY_OF_YEAR, 1);
			format.applyPattern(retPattern);
		} catch (ParseException e) {
			log.error("getNextDate error>>{}", e);
		}
		log.debug("next date>>"+format.format(calendar.getTime()));
		  return format.format(calendar.getTime()); 
		}
	
	
	/**
	 * 取得下N天日期
	 * @param retPattern
	 * @param num
	 * @return
	 */
	public static String getNext_N_Date(String  retPattern,Integer num) {
		  SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		  Calendar calendar = null;
		  Date date = null;
		try {
			date = format.parse(getDate(""));
			calendar = Calendar.getInstance();
			calendar.setTime(date);
			calendar.add(Calendar.DAY_OF_YEAR, num);
			format.applyPattern(retPattern);
		} catch (ParseException e) {
			log.error("getNext_N_Date error>>{}", e);
		}
		log.debug("next date>>"+format.format(calendar.getTime()));
		  return format.format(calendar.getTime()); 
		}
	
	/**
	 * 取得2個日期間的天數
	 * @param firstdate
	 * @param seconddate
	 * @return
	 */
	public static int getDiffDate(String firstdate ,String seconddate,String pattern){
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		Calendar calendar =Calendar.getInstance();
		Calendar calendar2 = Calendar.getInstance();
		long d1 = 0;
		long d2 = 0;
		BigDecimal diffdays = new BigDecimal(0) ; 
		try {
//			log.debug("firstdate>>"+firstdate+"seconddate>>"+seconddate);
			calendar.setTime(sdf.parse(firstdate));
			calendar2.setTime(sdf.parse(seconddate));
			d1 = calendar.getTimeInMillis();
			d2 = calendar2.getTimeInMillis();
			diffdays = new BigDecimal((d1-d2)/(1000*60*60*24)).abs();
		} catch (ParseException e) {
			log.error("getDiffDate error>>{}", e);
		}
		return diffdays.intValue()+1;
		
	}
	
	
	
	/**
	 * 取得2個日期間的天數
	 * @param firstdate
	 * @param seconddate
	 * @return
	 */
	public static int getDiffTimeStamp(String firstdate ,String seconddate){
		//log.debug("firstdate>>"+firstdate);
		//log.debug("seconddate>>"+seconddate);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar calendar =Calendar.getInstance();
		Calendar calendar2 = Calendar.getInstance();
		long d1 = 0;
		long d2 = 0;
		BigDecimal diffdays = new BigDecimal(0) ; 
		try {
//			log.debug("firstdate>>"+firstdate+"seconddate>>"+seconddate);
			calendar.setTime(sdf.parse(firstdate));
			calendar2.setTime(sdf.parse(seconddate));
			d1 = calendar.getTimeInMillis();
			d2 = calendar2.getTimeInMillis();
//			diffdays = new BigDecimal((d1-d2)/(1000*60*60*24)).abs();
			diffdays = new BigDecimal((d1-d2)/(1000)).abs();
		} catch (ParseException e) {
			log.error("getDiffTimeStamp error>>{}", e);
		}
		return diffdays.intValue()+1;
		
	}
	
	
	/**
	 * 西元年 民國年互轉
	 * @param type(1(INTERCONVERSION)=西元民國互轉;2(NOT_INTERCONVERSION)=不轉)
	 * @param AD
	 * @param beforeFormat(轉換前的格式)
	 * @param afterFormat(轉換後的格式)
	 * 範例
	 * convertDate("2013-05-08 21:10:10 ","yyyy-MM-dd HH:mm:ss","yyyyMMdd HH:mm:ss")
       convertDate("0102-05-08 21:20:10","yyyy-MM-dd HH:mm:ss","yyyyMMdd HH:mm:ss"
	 * @return
	 */
	public static String convertDate (int type, String AD,String beforeFormat,String afterFormat){//轉年月格式
        if (StrUtils.isEmpty(AD)) return "";
        SimpleDateFormat df4 = new SimpleDateFormat(beforeFormat);
        SimpleDateFormat df2 = new SimpleDateFormat(afterFormat);
        Calendar cal = Calendar.getInstance();
        String tmp = "";
        try {
            cal.setTime(df4.parse(AD));
            if(type == INTERCONVERSION){
            	if (cal.get(Calendar.YEAR) > 1492){
            		if(cal.get(Calendar.YEAR) -1911 <100){
            			tmp = "0";
            		}
                	cal.add(Calendar.YEAR, -1911);
                }else {
                	cal.add(Calendar.YEAR, +1911);
                }
            }
            return tmp+df2.format(cal.getTime());
        } catch (Exception e) {
        	log.error("convertDate error>>{}", e);
            return null;
        }
    }
	
	/**
	 * 此方法會因為作業系統語系不同而產生相對應的字串
	 * 輸入日期回傳星期幾
	 * @param dateString
	 * @return
	 * @throws ParseException
	 */
	public static String date2DayofWeek( String dateString ,String pattern ) throws ParseException
	{
		pattern = StrUtils.isEmpty(pattern)?"yyyy-MM-dd":pattern;
//	    SimpleDateFormat dateStringFormat = new SimpleDateFormat( "yyyy-MM-dd" );
	    SimpleDateFormat dateStringFormat = new SimpleDateFormat( pattern );
	    Date date = dateStringFormat.parse( dateString );
//	    SimpleDateFormat date2DayFormat = new SimpleDateFormat( "u" );
	    SimpleDateFormat date2DayFormat = new SimpleDateFormat( "E" );
	    //log.debug(""+date2DayFormat.format( date ));
	    return date2DayFormat.format( date );
	}
	public static enum Week {
		Mon,Tue,Wed,Thu,Fri,Sat,Sun
	}
	
	
	/**
	 * 
	 * @param dateString
	 * @param pattern
	 * @return
	 * @throws ParseException
	 */
	public static String date2DayofWeekII( String dateString ,String pattern ) throws ParseException
	{
		String ret = "";
		String tmp = "";
		pattern = StrUtils.isEmpty(pattern)?"yyyy-MM-dd":pattern;
//	    SimpleDateFormat dateStringFormat = new SimpleDateFormat( "yyyy-MM-dd" );
		SimpleDateFormat dateStringFormat = new SimpleDateFormat( pattern );
		Date date = dateStringFormat.parse( dateString );
//		GyMdkHmsSEDFwWahKzZ
//	    SimpleDateFormat date2DayFormat = new SimpleDateFormat( "u" );
		SimpleDateFormat date2DayFormat = new SimpleDateFormat( "E" );
		//log.debug(""+date2DayFormat.format( date ));
		//log.debug("isWindows>>"+OSValidator.isWindows());
		if(!OSValidator.isWindows()){
			tmp = date2DayFormat.format( date );
			Week week = Week.valueOf(tmp);
			//log.debug("week"+week);
			switch (week) {
			case Mon:
				ret ="星期一";
				break;
			case Tue:
				ret ="星期二";
				break;
			case Wed:
				ret ="星期三";
				break;
			case Thu:
				ret ="星期四";
				break;
			case Fri:
				ret ="星期五";
				break;
			case Sat:
				ret ="星期六";
				break;
			case Sun:
				ret ="星期日";
//				log.debug("this is sun");
				break;

			default:
				ret ="";
				break;
			}
		}else{
			ret = date2DayFormat.format( date );
		}
//		log.debug("ret"+ret);
		return ret;
	}
	

	/**
	 * 
	 *  <p>傳回格式化(HH:MM:SS)時間字串</p>
	 * @return a_str_time:未格式化之時間字串
	 * @see  old-core\DisplayUtil.java 
	 */  	 
	public static String formatTime(String a_str_time)
	{		
		try
		{
			if (a_str_time == null || a_str_time.equals(""))
				return "";
			
			//若傳入字串大於六位，後面無條件捨去
			return a_str_time.substring(0, 2) + ":" + a_str_time.substring(2, 4) + ":" + a_str_time.substring(4, 6);
		}
		catch (Exception exc) 
		{
			return "時間格式錯誤";
		}
	}
	
	
	/**
	 * <p>得到本月的第一天</p>
	 * @return 回傳的格式 yyyy/MM/dd  EX : 2018/09/01
	 * @see  old-core\DateTimeUtils.java 
	 */  
	public static String getMonthFirstDay() { 
		 SimpleDateFormat formatSlash = new SimpleDateFormat("yyyy/MM/dd");
	    Calendar calendar = Calendar.getInstance();
	    calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));   
	    return formatSlash.format(calendar.getTime()); 
	}
	
	/**   
	 * <p>得到上個月的第一天</p>
	 * @return  回傳的格式 yyyy/MM/dd  EX : 2018/08/01
	 * @see  old-core\DateTimeUtils.java 
	 */  
	public static String getPrevMonthFirstDay() { 
		SimpleDateFormat formatSlash = new SimpleDateFormat("yyyy/MM/dd");
	    Calendar calendar = Calendar.getInstance();
	    calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1);
	    calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));   
	    return formatSlash.format(calendar.getTime()); 
	}
	
	/**  
	 * 得到上個月的最後一天
	 * @return  回傳的格式 yyyy/MM/dd  EX :2018/08/31
	 * @see  old-core\DateTimeUtils.java 
	 */  
	public static String getPrevMonthLastDay() {   
		SimpleDateFormat formatSlash = new SimpleDateFormat("yyyy/MM/dd");
	    Calendar calendar = Calendar.getInstance();
	    calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1);
	    calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));   
	    return formatSlash.format(calendar.getTime()); 
	}  
	
	/**
	 * <p>處理日期區間的方法</p>
	 * @param reqParam
	 * @return 
	 *  Map<String, String>
	 *  returnParam.put("fgperiod", fgperiod);
	 * 	returnParam.put("cmsdate", cmsdate);
		returnParam.put("cmedate", cmedate);
		@see TBBNBAppsJava\fstop\services\impl\N130.java
	 */
	public static Map<String, String> periodProcessing(Map<String, String> reqParam){
 		String fgperiod = reqParam.get("FGPERIOD");//查詢時間
		String cmsdate = reqParam.get("CMSDATE"); //日期(起)
		String cmedate = reqParam.get("CMEDATE"); //日期(迄)
		SimpleDateFormat dtD = new SimpleDateFormat("yyyy/MM/dd");
		
		if("CMTODAY".equals(fgperiod)) { //今日
			cmsdate = dtD.format(new Date());
			cmedate = cmsdate;
		} else if("CMCURMON".equals(fgperiod)) { //本月
			cmsdate = DateUtil.getMonthFirstDay();//取得本月的第一天
			cmedate = dtD.format(new Date());//Date format
		} else if("CMLASTMON".equals(fgperiod)) { //上月
			 cmsdate = DateUtil.getPrevMonthFirstDay();//取得上個月的第一天  
			 cmedate = DateUtil.getPrevMonthLastDay();//取得上個月的最後一天 
		} else if("CMNEARMON".equals(fgperiod)) { //最近一個月
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.MONTH, -1);
			Date dstart = cal.getTime();
			Date dend = new Date();
			//Date format
			cmsdate = dtD.format(dstart);
			cmedate = dtD.format(dend);
		} else if("CMLASTWEEK".equals(fgperiod)) { //最近一星期
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -7);
			Date dstart = cal.getTime();
			Date dend = new Date();			
			//Date format
			cmsdate = dtD.format(dstart);
			cmedate = dtD.format(dend);
		} else if("CMPERIOD".equals(fgperiod)) { //指定日期
			
		}
		reqParam.put("fgperiod", fgperiod);
		reqParam.put("cmsdate", cmsdate);
		reqParam.put("cmedate", cmedate);
		return reqParam;
	}
	
	
	/**
	 * 取得明天日期，如果當前時間太晚(23:50:00~23:59:59)則為後天
	 * 
	 * @return
	 */
	public static String getTomorrowOrAfterTomorrowDate()
	{
		String resultDate = "-1";
		try
		{
			String dn = DateUtil.getNext_N_Date("yyyy/MM/dd",2);
			String dt = DateUtil.getNextDate("yyyy/MM/dd");
			Integer dsint = Integer.parseInt(DateUtil.getTheTime(""));
			Integer s_start = 235000;	// 23:50:00
			Integer s_end = 235959;		// 23:59:59
			if(dsint >= s_start && dsint <= s_end) {
				resultDate = dn;
			} else {
				resultDate = dt;
			}
		}
		catch (Exception e)
		{
			log.error("getTomorrowOrAfterTomorrowDate error>>{}", e);
		}
		return resultDate;
	}
	
	
	/**
	 * Ben增加 
	 * 
	 */
	static SimpleDateFormat dtF = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	static SimpleDateFormat dtD = new SimpleDateFormat("yyyyMMdd");
	static SimpleDateFormat dtT = new SimpleDateFormat("HHmmss");
	static Map<String, SimpleDateFormat> pool = new HashMap();
	
	public static String format(String pattern, Date d) {
		SimpleDateFormat sdf = getDateFormatFromPool(pattern);
		return sdf.format(d);		
	}
	private static SimpleDateFormat getDateFormatFromPool(String pattern) {
		SimpleDateFormat sdf = null;
		if(!pool.containsKey(pattern)) {
			sdf = new SimpleDateFormat(pattern);
			pool.put(pattern, sdf);
		}
		else {
			sdf = pool.get(pattern);
		}
		return sdf;
	}
	
	
	/**
	 * 取得時間差:單位毫秒
	 * @param d1
	 * @param d2
	 * @return
	 */
	public static String getDiffTimeMills(DateTime d1 ,DateTime d2) {
		String ret ="";
		try {
			Long l = d1.getMillis() - d2.getMillis();
			BigDecimal bd = new BigDecimal(l).divide(new BigDecimal(1000)).abs().setScale(3);
			ret = bd.toString();
		} catch (Exception e) {
			log.error("{}",e);
		}
		return ret;
	}
	
//	public static void main(String args[]) throws Exception {
////		log.debug(getMonthFirstDay());
////		log.debug(getPrevMonthFirstDay());
////		log.debug(getPrevMonthLastDay());
//		getNextDate("MMdd");
//		getNextDate("yyyyMMdd");
//		
//		getNext_N_Date("yyyy/MM/dd" ,2);
////		date2DayofWeek("20161016" , "yyyyMMdd");
////		date2DayofWeekII("20161016" , "yyyyMMdd")
//	}
	
	
	
}
