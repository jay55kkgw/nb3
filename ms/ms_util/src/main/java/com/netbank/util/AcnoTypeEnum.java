package com.netbank.util;

public enum AcnoTypeEnum {
	
	LOGIN("登入", new String[] {},"Y"),
	ACNO("臺幣存款帳號", new String[] { "01", "02", "03", "07" },""),
	CHECK_ACNO("支存帳號",new String[] { "01" },""), 
	OUT_ACNO("臺幣轉出帳號",new String[] { "01", "02", "03", "07" },"Y"),
	IN_ACNO("臺幣轉入帳號",new String[] { "01", "02" },"Y"),
	GOLD_ACNO("黃金存摺帳號",new String[]{"09","07"},""),
	FX_OUT_ACNO("外幣轉出帳號/手續費帳號",new String[]{"01", "02", "03", "04", "05", "07"}, "Y"),
	FX_OUT_DEP_ACNO("外幣定存轉出帳號", new String[]{"04", "05", "07"},"Y"),	
	INWARD_REMITTANC("匯入匯款查詢帳號", new String[]{"01", "02", "03", "04", "05", "06", "07", "08"}, "Y"),
	CLOSING_FCY_ACCOUNT("線上申請外匯存款帳戶結清銷戶", new String[]{"04"}, "Y"),
	CLOSING_FCY_ACCOUNT_IN("線上申請外匯存款帳戶結清銷戶轉入帳號頁用", new String[]{"02","03","06","07"}, ""),
	PURCHASE_FUND_DETAILS("輕鬆理財自動申購基金明細", new String[]{"07"}, ""),
	BOND_BOLANCE("中央登錄債券餘額", new String[]{"08"}, ""),
	MANAGEMENT("輕鬆理財戶帳號",new String[]{"07"}, ""),
	COLLATER("綜合存款帳戶",new String[] {"02","07"},""),
	CLOSING_TW_ACCOUNT("新臺幣存款帳戶結清銷戶", new String[] {"02", "03"}, ""),
	ACCSET("常用帳戶",new String[]{"01","02","03","04","05","06","07","08"}),
	TRANSFER_RECORD("晶片卡轉帳交易帳號",new String[]{"01", "02", "03", "04", "05", "06", "07", "08"}, ""),
	FX_DEPOSIT("外幣定存到期或解約轉入帳號", new String[] {"05", "07"}),
	FX_TRANSFER("外匯結購售/轉帳", new String[]{"01", "02", "06"} ),
	FX_DEPOSIT_N920("外幣定存到期或解約轉入帳號N920", new String[] {"04", "05", "07"}, ""),
	CERTAGREEACNOLIST("取得臺幣定存轉帳之約定/常用非約定帳號 臺幣定存使用", new String[]{"05"}),
	FX_DEPOSIT_AGREEACNOLIST("轉入外匯綜存定存的約定轉入帳號", new String[]{"07"}),
	CREDITCARD_PAY_INACNO("繳納本行信用卡款的約定轉入帳號",new String[]{"04"},Boolean.FALSE),
	GOLD_RESERVATION_QOERY_N920("黃金存摺帳號", new String[]{"07","09"}),
	GOLD_RESERVATION_QOERY("黃金存摺帳號", new String[]{"10"}),
	CLOSING_FCY_ACCOUNT_AGREEACNOS("線上申請外匯存款帳戶結清銷戶轉入台幣帳號", new String[]{"01"}),
	CLOSING_FCY_ACCOUNT_ACNOS1("線上申請外匯存款帳戶結清銷戶轉入外幣", new String[]{"06","07"}),
	FX_DEMAND_DEPOSIT_ACCOUNT("外幣活期性存款帳戶", new String[]{"04"}, ""),
	FUTURES_DEPOSIT_ACNO("期貨帳號", new String[]{"03"}, ""),
	OTHER_FEE_ACNO("其他費用轉入帳號", new String[]{"08", "02"}, ""),
	
	APPLYACNO("存款餘額申請帳號", new String[] { "01", "02", "03", "06", "07" }, "Y"), // 存款餘額證明申請-申請帳號: 20220124 修改邏輯 by 偉誠
	FEEACNO("手續費扣帳帳號", new String[] { "01", "02", "03", "07" }, "Y"), // 存款餘額證明申請-手續費扣帳帳號: 20210111 修改邏輯 by Sox
	EBILLAPPLYACNO("全國繳線上約定平台約定帳號", new String[] { "02", "03", "07" }, "");
	
	String description;
	String[] acnotype;
	String trflag;
	
	private AcnoTypeEnum(String description, String[] acnotype, String trflag) {
		this.description = description;
		this.acnotype = acnotype;
		this.trflag = trflag;
	}
	
	//n921pool
	private AcnoTypeEnum(String description, String[] acnotype) {
		this.description = description;
		this.acnotype = acnotype;
	}
	
	//n921pool
		private AcnoTypeEnum(String description, String[] acnotype , Boolean agreeOrNot) {
			this.description = description;
			this.acnotype = acnotype;
		}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String[] getAcnotype() {
		return acnotype;
	}

	public void setAcnotype(String[] acnotype) {
		this.acnotype = acnotype;
	}

	public String getTrflag() {
		return trflag;
	}

	public void setTrflag(String trflag) {
		this.trflag = trflag;
	}
	

}
