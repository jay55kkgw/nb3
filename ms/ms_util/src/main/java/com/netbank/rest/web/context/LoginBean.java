package com.netbank.rest.web.context;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

@Builder
public @Data class LoginBean {

    private String userId;
    private String username;

    @Tolerate
    public LoginBean(){}

}
