package com.netbank.rest.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import javax.servlet.http.HttpServletRequest;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
//import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
//import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.ssl.TrustStrategy;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.IntrusionException;
import org.owasp.esapi.errors.ValidationException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
//import org.apache.http.ssl.TrustStrategy;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 功能說明 :電文傳送Util
 *
 */
@Component
@Slf4j
public class RESTUtil {
	@Value("${isAgent}")
	String isAgent = "N";
	
	@Value("${cert_path}")
	String cert_path = "C:\\FSTOP\\tool\\cert\\DataPower\\focastestcer";
	@Value("${soci}")
	String soci = "changeit";
	@Value("${idgateUrl:https://127.0.0.1}")
	String idgateUrl ;
	@Value("${eloanApiUrl:https://127.0.0.1}")
	String eloanApiUrl ;
	
	
	
	/**
	 * 電文傳送 RESTful
	 * @param params
	 * @param url
	 * @return
	 * @throws Exception 
	 */
	@SuppressWarnings("unchecked")
	public  String send(Map params ,String url, Integer toTmraTimeout ) {
		String result = null;
		RestTemplate restTemplate = null;
		ClientHttpRequestFactory rf = null;
		try {
			log.trace(ESAPIUtil.vaildLog("url>>{}"+url));
			log.trace(ESAPIUtil.vaildLog("params>>{}"+CodeUtil.toJson(params)));
			if(!params.isEmpty()) {
				log.trace(ESAPIUtil.vaildLog("RESTfulClient.params.toJson>>{}"+CodeUtil.toJson(params)));
			}
			restTemplate = setHttpConfigII(toTmraTimeout);
			

			HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
			//String trace_cusidn =  request.getHeader("trace_cusidn");
			String trace_cusidn = getValidHeader(request,"trace_cusidn");
			log.debug(ESAPIUtil.vaildLog(("trace_cusidn>>{}"+trace_cusidn)));
			//String trace_userid = request.getHeader("trace_userid"); 
			String trace_userid = getValidHeader(request,"trace_userid");
			log.debug(ESAPIUtil.vaildLog(("trace_userid>>{}"+trace_userid)));
			//String trace_adguid = request.getHeader("trace_adguid"); 
			String trace_adguid = getValidHeader(request,"trace_adguid");
			log.debug(ESAPIUtil.vaildLog(("trace_adguid>>{}"+trace_adguid)));
			

//			壓力測試用
			if(StrUtils.isNotEmpty(request.getParameter("PUUID"))) {
				trace_adguid = getValidParameter(request,"PUUID");
			}
			
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.set("trace_cusidn", trace_cusidn);
			headers.set("trace_userid", trace_userid);
			headers.set("trace_adguid", trace_adguid);
			log.trace(ESAPIUtil.vaildLog("isAgent>>"+isAgent));
			if("Y".equalsIgnoreCase(isAgent)) {
				headers.set("User-Agent", "curl/7.29.0");
			}
//			connection.setRequestProperty("User-Agent", "curl/7.52.1");
			
			HttpEntity<Map<String, String>> entity = new HttpEntity<>(params, headers);
			ResponseEntity<String> re  = restTemplate.postForEntity(url, entity, String.class );
			
			result = re.getBody();
			
			log.trace(ESAPIUtil.vaildLog("result>>{}" +result));
			
			if(result == null) {
				throw new Exception("result == null ,電文接收異常");
			}
			
		} catch (ResourceAccessException e) {
			log.error("",e);
			throw e;
			
		} catch (Exception e) {
			log.error("",e);
		}
		
		
		return result;
		
	}
	
	// DataPower.query
	public String send2DataPower(Map<String, String> params ,String url, Integer toDataPowerTimeout ) {
		String result = null;
//		HashMap<String, Object> result = null;
		RestTemplate restTemplate = null;
		try {
			log.trace(ESAPIUtil.vaildLog("send2DataPower.url: {}"+ url));
			log.trace(ESAPIUtil.vaildLog("send2DataPower.params: {}"+ CodeUtil.toJson(params)));
			
			if(!params.isEmpty()) {
				log.trace(ESAPIUtil.vaildLog("RESTfulClient.params.toJson: {}"+ CodeUtil.toJson(params)));
			}
			
			restTemplate = setHttpConfigIIII(toDataPowerTimeout);
			
			result = restTemplate.postForObject(url, params, String.class);
			
			log.trace(ESAPIUtil.vaildLog("send2DataPower.result: {}"+ result));
			
			if(result == null) {
				throw new Exception("result == null ,電文接收異常");
			}
			
		} catch (ResourceAccessException e) {
			log.error("",e);
			throw e;
			
		} catch (Exception e) {
			log.error("",e);
		}
		
		return result;
	}
	

	public String send2fcm(Map<String, Object> params, String url, Integer timeout, String key) {
		String result = null;
		RestTemplate restTemplate = null;
		try {
			log.trace(ESAPIUtil.vaildLog("sendHttps.url: {}" + url));
			log.info(ESAPIUtil.vaildLog("sendHttps.params: {}" + CodeUtil.toJson(params)));
			
			if(!params.isEmpty()) {
				log.trace(ESAPIUtil.vaildLog("RESTfulClient.params.toJson: {}" + CodeUtil.toJson(params)));
			}
			
			restTemplate = setHttpConfigIIII(timeout);
		
	        Integer readTimeout = timeout*1000;
			Integer connectTimeout = 5*1000;
			Integer connectionRequestTimeout = 5*1000;
			
			// 在握手期間，如果URL的主機名和服務器的標識主機名不匹配，則驗證機制可以通過此接口的實現，程序來確定是否應該允許此連接
			HostnameVerifier hv = new HostnameVerifier() {
				@Override
				public boolean verify(String urlHostName, SSLSession session) {
					return true;
				}
			};
			HttpsURLConnection.setDefaultHostnameVerifier(hv);
			
//	            SSLConnectionSocketFactory ssLSocketFactory = buildSSLSocketFactory("PKCS12", cert_path, soci, Lists.newArrayList("TLSv1.2"), false);
            SSLConnectionSocketFactory ssLSocketFactory = buildSSLSocketFactory("JKS", cert_path, soci, Lists.newArrayList("TLSv1.2"), true);
            
            // Spring提供HttpComponentsClientHttpRequestFactory指定使用HttpClient作為底層實現HTTP請求
            HttpComponentsClientHttpRequestFactory httpRequestFactory = new HttpComponentsClientHttpRequestFactory(
                    HttpClients.custom().setSSLSocketFactory(ssLSocketFactory).build()
            );
            
            // 數據傳遞Timeout
            httpRequestFactory.setReadTimeout(readTimeout);
            // 連線Timeout
            httpRequestFactory.setConnectTimeout(connectTimeout);
            // request連線Timeout
            httpRequestFactory.setConnectionRequestTimeout(connectionRequestTimeout);
            
            
            restTemplate = new RestTemplate(httpRequestFactory);

			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.set("Authorization", "key=" + key);
			
			HttpEntity<Map<String, Object>> entity = new HttpEntity<>(params, headers);

			ResponseEntity<String> re  = restTemplate.postForEntity(url, entity, String.class );

			result = re.getBody();
			
			log.info(ESAPIUtil.vaildLog("sendHttps.result: {}" + result));
			
			if(result == null) {
				throw new Exception("result == null ,電文接收異常");
			}
			
		} catch (ResourceAccessException e) {
			log.error("",e);
			throw e;
			
		} catch (Exception e) {
			log.error("",e);
		}
		
		return result;
	}
	
	/**
	 * 
	 * @param params
	 * @param url
	 * @param toDataPowerTimeout
	 * @return
	 * @throws Exception 
	 */
	public String send2IdGate(Map<String, String> params ,String url, Integer toIdGateTimeout ,HttpMethod method ) throws Exception {
		String result = null;
		RestTemplate restTemplate = null;
		ClientHttpRequestFactory rf = null;
		ResponseEntity<String> re  = null;
		MultiValueMap<String, String> multiValueMap=new LinkedMultiValueMap<>();
		String tempurl = "";
		String signature = "";
		try {
			url = idgateUrl+url;
			log.info(ESAPIUtil.vaildLog("url>>{}"+url));
			if(!params.isEmpty()) {
				log.debug(ESAPIUtil.vaildLog("RESTfulClient.params.toJson>>"+CodeUtil.toJson(params)));
			}
			restTemplate = setHttpConfigIIII(toIdGateTimeout);
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
			headers.setAcceptCharset(Collections.singletonList(StandardCharsets.UTF_8 ));
			headers.add("apiKey", "API_Key");
//			參數轉URL
			multiValueMap.setAll(params);
			UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(url).queryParams(multiValueMap);
			tempurl = uriBuilder.toUriString();
			
			log.trace("url>>{}",url);
			log.trace(ESAPIUtil.vaildLog("params>> " + CodeUtil.toJson(params)));
			HttpEntity<Map<String, String>> entity = new HttpEntity<>(params, headers);
			restTemplate.getMessageConverters().add(new ObjectToUrlEncodedConverter(new ObjectMapper()));
			re  = restTemplate.exchange(url, method, entity, String.class);
			
//			log.trace(ESAPIUtil.vaildLog("re>>" + re));
			result = re.getBody();
			log.trace(ESAPIUtil.vaildLog("result>>{}" +result));
			
			if(result == null) {
				throw new Exception("result == null ,電文接收異常");
			}
			
		} catch (ResourceAccessException e) {
			log.error("ResourceAccessException>>{}",e);
			throw e;
			
		} catch (Exception e) {
			log.error("Exception>>{}",e);
			throw e;
		}
		return result;
	}
	
	/**
     * 建構SSLSocketFactory
     *
     * @param keyStoreType
     * @param keyFilePath
     * @param keyPassword
     * @param sslProtocols
     * @param auth 是否需要客戶端完全相信不安全證書
     * @return
     * @throws Exception
     */
    private SSLConnectionSocketFactory buildSSLSocketFactory(String keyStoreType, String certPath, String soci, List<String> sslProtocols, boolean auth) throws Exception {
        // 證書管理器，指定證書及證書類型
        KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        // KeyStore用於存放證書，創建對象時指定交換數字證書的加密標準
        KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
		keyStore.load(
			new FileInputStream(new File(certPath)),
			soci.toCharArray()
		);
        keyManagerFactory.init(keyStore, soci.toCharArray());
        
        SSLContext sslContext = SSLContext.getInstance("SSL");
        if (auth) {
            // 設置信任證書（繞過TrustStore驗證）
            TrustManager[] trustAllCerts = new TrustManager[1];
            TrustManager trustManager = new AuthX509TrustManager();
            trustAllCerts[0] = trustManager;
            sslContext.init(keyManagerFactory.getKeyManagers(), trustAllCerts, null);
            HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
            
        } else {
        	log.debug(ESAPIUtil.vaildLog("buildSSLSocketFactory.certPath: {}"+certPath));
            // 加載證書材料，重建ssl
            sslContext = SSLContexts.custom().loadTrustMaterial(new File(certPath), soci.toCharArray(), new TrustSelfSignedStrategy()).build();
        }

        SSLConnectionSocketFactory sslConnectionSocketFactory =
                new SSLConnectionSocketFactory(sslContext, sslProtocols.toArray(new String[sslProtocols.size()]),
                        null,
                        new HostnameVerifier() {
                            // 這裡不校驗主機名
                            @Override
                            public boolean verify(String urlHostName, SSLSession session) {
                                return true;
                            }
                        });

        return sslConnectionSocketFactory;
    }
    
	public RestTemplate setHttpConfigIIII(Integer toDataPowerTimeout) {
		// 新建RestTemplate对象
        RestTemplate restTemplate = null;
        		
        Integer readTimeout = toDataPowerTimeout*1000;
		Integer connectTimeout = 5*1000;
		Integer connectionRequestTimeout = 5*1000;
		
		try {
			// 在握手期間，如果URL的主機名和服務器的標識主機名不匹配，則驗證機制可以通過此接口的實現，程序來確定是否應該允許此連接
			HostnameVerifier hv = new HostnameVerifier() {
				@Override
				public boolean verify(String urlHostName, SSLSession session) {
					return true;
				}
			};
			HttpsURLConnection.setDefaultHostnameVerifier(hv);
			
//            SSLConnectionSocketFactory ssLSocketFactory = buildSSLSocketFactory("PKCS12", cert_path, soci, Lists.newArrayList("TLSv1.2"), false);
            SSLConnectionSocketFactory ssLSocketFactory = buildSSLSocketFactory("JKS", cert_path, soci, Lists.newArrayList("TLSv1.2"), false);
            
            // Spring提供HttpComponentsClientHttpRequestFactory指定使用HttpClient作為底層實現HTTP請求
            HttpComponentsClientHttpRequestFactory httpRequestFactory = new HttpComponentsClientHttpRequestFactory(
                    HttpClients.custom().setSSLSocketFactory(ssLSocketFactory).build()
            );
            
            // 數據傳遞Timeout
            httpRequestFactory.setReadTimeout(readTimeout);
            // 連線Timeout
            httpRequestFactory.setConnectTimeout(connectTimeout);
            // request連線Timeout
            httpRequestFactory.setConnectionRequestTimeout(connectionRequestTimeout);
            
            
            restTemplate = new RestTemplate(httpRequestFactory);

		} catch (Exception e) {
			log.error(e.toString());
		}
		return restTemplate;
	}

	public  RestTemplate setHttpConfig() {
		Integer connectTimeout = 5*1000;
		Integer socketTimeout = 55*1000;
		HttpComponentsClientHttpRequestFactory rf = new HttpComponentsClientHttpRequestFactory();
		rf.setConnectTimeout(connectTimeout);
		rf.setReadTimeout(socketTimeout);
		return new RestTemplate(rf);
		
	}
	/**
	 * 設定Http 參數
	 * https://codertw.com/%E7%A8%8B%E5%BC%8F%E8%AA%9E%E8%A8%80/12005/
	 * @param toTmraTimeout
	 * @return
	 * @throws KeyManagementException
	 * @throws NoSuchAlgorithmException
	 * @throws KeyStoreException
	 */
	public  RestTemplate setHttpConfigII(Integer toTmraTimeout) throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
		Integer connectTimeout = 5*1000;
		Integer socketTimeout = toTmraTimeout*1000;
		HttpComponentsClientHttpRequestFactory rf =
				new HttpComponentsClientHttpRequestFactory();
		rf.setConnectTimeout(connectTimeout);
		rf.setReadTimeout(socketTimeout);
		log.trace("setHttpConfigI end..");
		return new RestTemplate(rf);
		
	}
	
	/**
	 * 設定Http 參數
	 * https://codertw.com/%E7%A8%8B%E5%BC%8F%E8%AA%9E%E8%A8%80/12005/
	 * @param toTmraTimeout
	 * @return
	 * @throws KeyManagementException
	 * @throws NoSuchAlgorithmException
	 * @throws KeyStoreException
	 */
	public  RestTemplate setHttpConfig_eloanApi(Integer toEloanApiTimeout) throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
		Integer connectTimeout = 5*1000;
		Integer socketTimeout = toEloanApiTimeout*1000;
//		
//		
//		TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;
//
//		SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom()
//		        .loadTrustMaterial(null, acceptingTrustStrategy)
//		        .build();
//
//		SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);
//
//		CloseableHttpClient httpClient = HttpClients.custom()
//		        .setSSLSocketFactory(csf)
//		        .build();
//
//		HttpComponentsClientHttpRequestFactory requestFactory =
//		        new HttpComponentsClientHttpRequestFactory();
//		
//		requestFactory.setConnectTimeout(connectTimeout);
//		requestFactory.setReadTimeout(socketTimeout);
//
//		requestFactory.setHttpClient(httpClient);
//
//		log.trace("setHttpConfig_eloanApi end..");
//		return new RestTemplate(requestFactory);
		TrustManager[] trustAllCerts = new TrustManager[] {
                new X509TrustManager() {
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return new X509Certificate[0];
                    }
                    public void checkClientTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {
                    }
                    public void checkServerTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {
                    }
                }
        };  
        SSLContext sslContext = SSLContext.getInstance("SSL");
        sslContext.init(null, trustAllCerts, new java.security.SecureRandom()); 
        CloseableHttpClient httpClient = HttpClients.custom()
                .setSSLContext(sslContext)
                .setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                .build();   
        HttpComponentsClientHttpRequestFactory customRequestFactory = new HttpComponentsClientHttpRequestFactory();
        customRequestFactory.setHttpClient(httpClient);
        customRequestFactory.setConnectTimeout(connectTimeout);
        customRequestFactory.setReadTimeout(socketTimeout);
        return new RestTemplate(customRequestFactory);
//        return builder.requestFactory(() -> customRequestFactory).build(); 
		
	}
	
	
	
	public  RestTemplate setHttpConfigIII(Integer toTmraTimeout) throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
		Integer connectTimeout = 5*1000;
		Integer socketTimeout = toTmraTimeout*1000;
		List<String> list = new LinkedList<String>();
		list.add("TLSv2");
		log.trace("setHttpConfigIII..");
//		try {
//			SSLConnectionSocketFactory ssLSocketFactory = buildSSLSocketFactory("PKCS12", 
//					"D:\\mock_data\\trust\\focastestcer","changeit", 
//					list, true);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		try {
//			String cert_path = "C:\\FSTOP\\tool\\cert\\DataPower\\focastestcer";
			String cert_path = "C:\\FSTOP\\tool\\cert\\DataPower\\datapower.jks";
			String soci = "changeit";
			setSSLContext(cert_path, soci);
		} catch (IOException e) {
			log.error(e.toString());
		}
		
		HttpComponentsClientHttpRequestFactory rf =
				new HttpComponentsClientHttpRequestFactory();
		rf.setConnectTimeout(connectTimeout);
		rf.setReadTimeout(socketTimeout);
		log.trace("setHttpConfigI end..");
		return new RestTemplate(rf);
		
	}
	
	
	public  void setSSLContext(String trustStorePath, String soci) throws IOException {
		FileInputStream keyStoreStream = null;
		FileInputStream trustStoreStream = null;
		try {
			log.info("setSSLContext...");
			log.debug(ESAPIUtil.vaildLog("trustStorePath>> " + trustStorePath +" , soci>> "+ soci));
			KeyStore trustStore = KeyStore.getInstance("JKS");
			trustStoreStream = new FileInputStream(new File(trustStorePath));
			trustStore.load(trustStoreStream, soci.toCharArray());
            
			TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
			tmf.init(trustStore);
			TrustManager[] tms = tmf.getTrustManagers();

			SSLContext sslContext = SSLContext.getInstance("SSL");
			sslContext.init(null, tms, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
			log.info("setSSLContext...end");
		} catch (Exception e) {
			log.error("setSSLContext Error>>{}",e);
		} finally {
			if (keyStoreStream != null)
				keyStoreStream.close();
			if (trustStoreStream != null)
				trustStoreStream.close();
		}
    }
	
	
//	
//	public SSLConnectionSocketFactory buildSSLSocketFactory(String keyStoreType, String keyFilePath, String keyPassword,
//			List<String> sslProtocols, boolean auth) throws Exception {
//		// 證書管理器，指定證書及證書型別
//		KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
//		// KeyStore用於存放證書，建立物件時 指定交換數字證書的加密標準
//		KeyStore keyStore = KeyStore.getInstance(keyStoreType);
//		// InputStream inputStream =
//		// resourcePatternResolver.getResource(keyFilePath).getInputStream();
//		FileInputStream trustStoreStream = null;
//		trustStoreStream = new FileInputStream(new File(keyFilePath));
//		try {
//			// 新增證書
//			keyStore.load(trustStoreStream, keyPassword.toCharArray());
//		} finally {
//			// inputStream.close();
//			if(trustStoreStream != null) {
//				trustStoreStream.close();
//			}
//		}
//		keyManagerFactory.init(keyStore, keyPassword.toCharArray());
//		SSLContext sslContext = SSLContext.getInstance("SSL");
//		if (auth) {
//			// 設定信任證書（繞過TrustStore驗證）
//			// TrustManager[] trustAllCerts = new TrustManager[1];
//			// TrustManager trustManager = new AuthX509TrustManager();
//			// trustAllCerts[0] = trustManager;
//			// sslContext.init(keyManagerFactory.getKeyManagers(), trustAllCerts, null);
//			// HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
//		} else {
//			// 載入證書材料，構建sslContext
//			sslContext = SSLContexts.custom().loadKeyMaterial(keyStore, keyPassword.toCharArray()).build();
//		}
//		return sslContext;
//	}
	
	public static String getValidHeader( HttpServletRequest request, String name ){
		
		  String value = request.getHeader(name);
		  if(value==null) {
			  return null;
		  }
		  try {
			return ESAPI.validator().getValidInput("HTTP header value: " + value, value, "HTTPHeaderValue", value.length(), false);
		} catch (ValidationException e) {
			return null;
		} catch (IntrusionException e) {
			return null;
		}
	}
	
	public static String getValidParameter(HttpServletRequest request, String name) {
	   String value = request.getParameter(name);
	   if(value==null) {
		  return null;
	   }
	   try {
			return ESAPI.validator().getValidInput("Parameter value: " + value, value, "GeneralString", value.length(), false);
		} catch (ValidationException e) {
			return null;
		} catch (IntrusionException e) {
			return null;
		}
		
	}
	
	/**
	 * 
	 * @param params
	 * @param url
	 * @param toDataPowerTimeout
	 * @return
	 * @throws Exception 
	 */
	public String send2elaonApi(Map<String, String> params ,String url, Integer toEloanTimeout ,HttpMethod method ) throws Exception {
		String result = null;
		RestTemplate restTemplate = null;
		ClientHttpRequestFactory rf = null;
		ResponseEntity<String> re  = null;
		MultiValueMap<String, String> multiValueMap=new LinkedMultiValueMap<>();
		String tempurl = "";
		try {
			url = eloanApiUrl+url;
			log.info(ESAPIUtil.vaildLog("url>>{}"+url));
			if(!params.isEmpty()) {
				log.debug(ESAPIUtil.vaildLog("ELOAN RESTfulClient.params.toJson>>"+CodeUtil.toJson(params)));
			}
			restTemplate = setHttpConfig_eloanApi(toEloanTimeout);
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
			headers.setAcceptCharset(Collections.singletonList(StandardCharsets.UTF_8 ));
			
//			//Disabling SSL Certificate Validation
//			HttpsURLConnection.setDefaultHostnameVerifier((hostname, session) -> true);
			
//			參數轉URL
			multiValueMap.setAll(params);
			UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(url).queryParams(multiValueMap);
			tempurl = uriBuilder.toUriString();
			
			log.trace("url>>{}",url);
			log.trace(ESAPIUtil.vaildLog("params>> " + CodeUtil.toJson(params)));
			HttpEntity<Map<String, String>> entity = new HttpEntity<>(params, headers);
			restTemplate.getMessageConverters().add(new ObjectToUrlEncodedConverter(new ObjectMapper()));
			re  = restTemplate.exchange(url, method, entity, String.class);
			
//			log.trace(ESAPIUtil.vaildLog("re>>" + re));
			result = re.getBody();
			log.trace(ESAPIUtil.vaildLog("result>>{}" +result));
			
			if(result == null) {
				throw new Exception("result == null ,EloanAPI 接收異常");
			}
			
		} catch (ResourceAccessException e) {
			log.error("ResourceAccessException>>{}",e);
			throw e;
			
		} catch (Exception e) {
			log.error("Exception>>{}",e);
			throw e;
		}
		return result;
	}
	
}
