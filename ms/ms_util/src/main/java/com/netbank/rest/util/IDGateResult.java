package com.netbank.rest.util;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IDGateResult {

	private Boolean result = Boolean.FALSE;
	private String msgCode = "FE0003";
	private String msgName = "系統異常";
	private Object messages;
	private Object data;
	
	@SuppressWarnings("unchecked")
	public void addData(String key, Object value) {
		if(this.data == null){
			this.data = new HashMap<String, Object>();
		}
		
		((Map<String, Object>) this.data).put(key, value);
	}
	
	@SuppressWarnings("unchecked")
	public void addAllData(Map value) {
		if(this.data == null){
			this.data = new HashMap<String, Object>();
		}
		
		((Map<String, Object>) this.data).putAll(value);
	}
	
	@SuppressWarnings("unchecked")
	public void addMessages(String key, Object value) {
		if(this.messages == null ){
			this.messages = new HashMap<String, Object>();
		}
		
		((Map<String, Object>) this.messages).put(key, value);
	}
	
	@SuppressWarnings("unchecked")
	public void addAllMessages(Map value) {
		if(this.messages == null){
			this.messages = new HashMap<String, Object>();
		}
		
		((Map<String, Object>) this.messages).putAll(value);
	}
	
}
