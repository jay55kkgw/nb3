/*
 * Created on Nov 11, 2003
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package topibs.utility;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringBufferInputStream;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

import org.owasp.esapi.ESAPI;

import fstop.util.StrUtils;
import fstop.util.exception.TopException;
import lombok.extern.slf4j.Slf4j;

//import topibs.bo.DepBiz;

/**
 * @author jack_lin
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
@Slf4j
public class SystemUtil 
{
	public static String[] str_SeqnoArray = new String[] {"A","B","D","E","F","G","H","I",
											              "J","K","L","M","N","O","P","Q",
											              "R","S","T","U","V","W","X","Y","Z"};			

	public static TopLinkedHashMap multiRecordToLHM(TopLinkedHashMap a_lhm_source) throws TopException
	{
		String	str_count;
		TopLinkedHashMap lhm_rtnData = new TopLinkedHashMap();
		TopLinkedHashMap lhm_record = new TopLinkedHashMap();
		Iterator itr_recordData;
		Iterator itr_fieldData;
		String	str_key;
		String	str_value;
		try
		{
			itr_recordData = a_lhm_source.entrySet().iterator();  
			str_count = "0";
			
			while (itr_recordData.hasNext())
			{
				Map.Entry entryRecord = (Map.Entry)itr_recordData.next();
				lhm_record = (TopLinkedHashMap) entryRecord.getValue();
				str_count = String.valueOf(Integer.parseInt(str_count) + 1);
				
				itr_fieldData = lhm_record.entrySet().iterator();
				while (itr_fieldData.hasNext())
				{
					Map.Entry entryField = (Map.Entry)itr_fieldData.next();
					str_key = (String) entryField.getKey();
					str_value = (String) entryField.getValue();
					lhm_rtnData.put(str_key + "_" + str_count, str_value);
				}
			}
			//Added by Jackie Lee on 2004-02-18
			lhm_rtnData.put("Count",str_count);			
			return lhm_rtnData;
		}
		catch (Exception exc)
		{
			log.error("",exc);
			throw new TopException("00001", "SystemUtil.multiRecordToLHM()");
		}
	}

	public static TopLinkedHashMap multiRecordToLHM(TopLinkedHashMap a_lhm_source, TopLinkedHashMap a_lhm_fields) throws TopException
	{
		String	str_count;
		TopLinkedHashMap lhm_rtnData= new TopLinkedHashMap();
		TopLinkedHashMap lhm_record = new TopLinkedHashMap();
		Iterator	itr_recordData;
		Iterator	itr_fieldData;
		String	str_key;
		String	str_value;

		try
		{
			itr_recordData = a_lhm_source.entrySet().iterator();
			str_count = "0";
			while (itr_recordData.hasNext())
			{
				Map.Entry entryRecord = (Map.Entry)itr_recordData.next();
				lhm_record = (TopLinkedHashMap) entryRecord.getValue();
				str_count = String.valueOf(Integer.parseInt(str_count) + 1);
				itr_fieldData = lhm_record.entrySet().iterator(); 
				while (itr_fieldData.hasNext())
				{
					Map.Entry entryField = (Map.Entry)itr_fieldData.next();
					str_key = (String) entryField.getKey();
					if (! a_lhm_fields.containsKey(str_key))
					{	
						continue;
					}
					
					str_value = (String) entryField.getValue();
					lhm_rtnData.put(str_key + "_" + str_count, str_value);
				}
			}
			//Added by Jackie Lee on 2004-02-18
			lhm_rtnData.put("Count",str_count);	
			return lhm_rtnData;

		}
		catch (Exception exc)
		{
			log.error("",exc);
			throw new TopException("00001", "SystemUtil.multiRecordToLHM()");
		}
	}

	public static TopLinkedHashMap multiRecordToLHM(Vector a_vtr_source) throws TopException
	{
		String	str_count;
		TopLinkedHashMap lhm_rtnData = new TopLinkedHashMap();
		TopLinkedHashMap lhm_record = new TopLinkedHashMap();
		Iterator itr_fieldData;
		String	str_key;
		String	str_value;
		try
		{
			str_count = "0";
			int i_count = a_vtr_source.size();
		 	for(int i=0; i<i_count; ++i )
			{
				lhm_record = (TopLinkedHashMap) a_vtr_source.get(i);
				str_count = String.valueOf(Integer.parseInt(str_count) + 1);
				
				itr_fieldData = lhm_record.entrySet().iterator();
				while (itr_fieldData.hasNext())
				{
					Map.Entry entryField = (Map.Entry)itr_fieldData.next();
					str_key = (String) entryField.getKey();
					str_value = (String) entryField.getValue();
					lhm_rtnData.put(str_key + "_" + str_count, str_value);
				}
			}
			//Added by Jackie Lee on 2004-02-18
			lhm_rtnData.put("Count",str_count);	
			return lhm_rtnData;
		}
		catch (Exception exc)
		{
			log.error("",exc);
			throw new TopException("00001", "SystemUtil.multiRecordToLHM()");
		}
	}


	public static TopLinkedHashMap multiRecordToLHM(Vector a_vtr_source, TopLinkedHashMap a_lhm_fields) throws TopException
	{
		String	str_count;
		TopLinkedHashMap lhm_rtnData= new TopLinkedHashMap();
		TopLinkedHashMap lhm_record = new TopLinkedHashMap();
		Iterator	itr_recordData;
		Iterator	itr_fieldData;
		String	str_key;
		String	str_value;

		try
		{
			str_count = "0";
			int i_count = a_vtr_source.size();
			for(int i=0; i<i_count; ++i )
			{
				lhm_record = (TopLinkedHashMap) a_vtr_source.get(i);
				str_count = String.valueOf(Integer.parseInt(str_count) + 1);
				itr_fieldData = lhm_record.entrySet().iterator(); 
				while (itr_fieldData.hasNext())
				{
					Map.Entry entryField = (Map.Entry)itr_fieldData.next();
					str_key = (String) entryField.getKey();
					if (! a_lhm_fields.containsKey(str_key))
					{	
						continue;
					}
					
					str_value = (String) entryField.getValue();
					lhm_rtnData.put(str_key + "_" + str_count, str_value);
				}
			}
			//Added by Jackie Lee on 2004-02-18
			lhm_rtnData.put("Count",str_count);	
			return lhm_rtnData;

		}
		catch (Exception exc)
		{
			log.error("",exc);
			throw new TopException("00001", "SystemUtil.multiRecordToLHM()");
		}
	}


	
    /**
     * 使用時機：將a_htl_source中內容為 xxx_N的資料逐筆解到Vector(每一個item為一個TopLinkedHashMap), 並回傳之
     * @param	1. a_lhm_source -- 包含多個 xxx_N 的資料
     * 				2. a_i_RecordCount : 拆解的總筆數 , 同時為前述N的最大數
     * 				3. a_vtr_NumericFields(每一個item為一String)：在a_lhm_source中有可能KeyValue為""的數字型態的欄位名稱
     * 
     * @return Vector  逐筆拆解a_lhm_source後的結果, 每一個item為一個 TopLinkedHashMap
     */ 	
	public static Vector multiNToVector(TopLinkedHashMap a_lhm_source, int a_i_RecordCount, Vector a_vtr_NumericFields) throws TopException
	{
		Vector						vtr_rtnData			=	new	Vector();
		boolean					b_NumericFields;
		String						str_count;
		
		try
		{
			if	( a_vtr_NumericFields.size() == 0 )
				b_NumericFields = false;
			else	
				b_NumericFields = true;
									
			// 取得每一筆的所有資料 (同時判斷, 若有數字型態欄位, 則應將""值轉為"0")	
			for(int i=0; i<a_i_RecordCount; ++i )
			{
				TopLinkedHashMap lhm_RecordData	= new TopLinkedHashMap();
				lhm_RecordData = SystemUtil.extractMultiN(a_lhm_source, i+1);

				if	( b_NumericFields )
					SystemUtil.correctLHMNumericField(lhm_RecordData, a_vtr_NumericFields);

				str_count = String.valueOf(i + 1);
				log.debug("SystemUtil.multiNToVector() Record " + str_count + " = " + lhm_RecordData);
		
				vtr_rtnData.add(lhm_RecordData);
			}
			
			return vtr_rtnData;		
		
		}
		catch (Exception exc)
		{
			log.error("",exc);
			throw new TopException("00001", "SystemUtil.multiNToVector()");
		}
	}

	
    /**
     * 使用時機：若a_vtr_NumericFields各欄位在 a_htl_source 中的值為"", 則應改將該欄位值改成"0"
    * @param	1. a_lhm_source
     * 				2. a_vtr_NumericFields(每一個item為一String)：在a_lhm_source中有可能KeyValue為""的數字型態的欄位名稱
     * 											  
    */ 	
	public static void correctLHMNumericField(TopLinkedHashMap a_lhm_source, Vector a_vtr_NumericFields) throws TopException
	{
		String						str_KeyName;

		try
		{
			int i_count = a_vtr_NumericFields.size();
			
			for(int i=0; i<i_count; ++i )
			{
				str_KeyName	= (String) a_vtr_NumericFields.get(i);
			
				if	( ! a_lhm_source.containsKey(str_KeyName) )
					throw new TopException("00002", "SystemUtil.correctLHMNumericField()" + str_KeyName);
				else if (a_lhm_source.get(str_KeyName).equals(""))
					a_lhm_source.put(str_KeyName, "0");

			}

		}
		catch (TopException exc){
			log.error("",exc);
			throw exc;
		}
		catch (Exception exc) {
			log.error("",exc);
			throw new TopException("00001", "SystemUtil.correctLHMNumericField()");
		}
	}

	public static TopLinkedHashMap extractMultiN(TopLinkedHashMap a_lhm_source, int a_i_RowNo) throws TopException
	{
		TopLinkedHashMap lhm_rtnData = new TopLinkedHashMap();
		Iterator itr_recordData;
		String	str_key;
		String	str_value;
		String	str_lastKeyName;
		int	i_lastKeyNameLength;

		try
		{
			str_lastKeyName = "_" + String.valueOf(a_i_RowNo);
			i_lastKeyNameLength = str_lastKeyName.length();
			itr_recordData = a_lhm_source.entrySet().iterator();
			while (itr_recordData.hasNext())
			{
				Map.Entry entryRecord = (Map.Entry)itr_recordData.next();
				str_key = (String) entryRecord.getKey();
				
				if (! str_key.substring( str_key.length() - i_lastKeyNameLength).equals(str_lastKeyName) )
				{
					continue;	
				}
				str_value = (String) entryRecord.getValue();
				str_key = str_key.substring(0, str_key.length() - i_lastKeyNameLength);
				lhm_rtnData.put(str_key, str_value);
			}
			return lhm_rtnData;
		}
		catch (Exception exc)
		{
			log.error("",exc);
			throw new TopException("00001", "SystemUtil.extractMultiN()");
		}
	}
	
    /**
     * 使用時機：在包含主檔及明細檔的交易畫面中,取出其中主檔部分的資料(i.e. TopLinkedHashMap中不包含"_N"的欄位)
     * @param a_lhm_source -- 包含主檔及明細檔的畫面資料
     * @return TopLinkedHashMap -- 僅包含主檔部分的資料
     */ 	
	public static TopLinkedHashMap extractMainFromLHM(TopLinkedHashMap a_lhm_source) throws TopException
	{
		Iterator itr_recordData;
		String	str_key;
		String	str_UnderLine;
		TopLinkedHashMap lhm_RtnData = new TopLinkedHashMap();
		
		try
		{
			str_UnderLine = "_";
			itr_recordData = a_lhm_source.entrySet().iterator();
			while (itr_recordData.hasNext())
			{
				Map.Entry entryRecord = (Map.Entry)itr_recordData.next();
				str_key = (String) entryRecord.getKey();
				
				//若不包含"_N"的欄位則取出放入lhm_RtnData
				if ( str_key.indexOf(str_UnderLine) == -1 )
				{
					lhm_RtnData.put( str_key, entryRecord.getValue() );
				}				
			}
			
			return lhm_RtnData;
		}
		catch (Exception exc)
		{
			log.error("",exc);
			throw new TopException("00001", "SystemUtil.extractMainFromLHM()");
		}
	}

	
    /**
     * 使用時機：重整參數a_htl_source, 即自參數中取出SessionInfo(為一Hashtable)的部份資料放到參數中, 並將SessionInfo移除
     * @param	1. a_lhm_source -- 包含KeyName為"SessionInfo"的資料
     * 				2. a_vtr_KeyName (每一個Item為一String)
     * 											指欲自SessionInfo中取出的KeyName   或   欲取出的KeyName+","+新的KeyName
     * 											如 : BizDate  或   LoginUserID,MaintainUserID
     *                     p.s. 若a_vtr_KeyName為空, 代表將SessionInfo中全部的Key搬到a_htl_source
     * 											  
    */ 	
	public static void extractSessionInfoFromHTL(Hashtable a_htl_source, Vector a_vtr_KeyName) throws TopException
	{
		String	str_Item					= "";
		String	str_KeyName			= "";
		String	str_NewKeyName	= "";
		int i_count = 0;
		
		try
		{
			// 自參數中取出 SessionInfo 的資料
			Hashtable htl_SessionInfo =(Hashtable) a_htl_source.get("SessionInfo");

			// 若a_vtr_KeyName為空, 代表將SessionInfo中全部的Key搬到a_htl_source
			if	( a_vtr_KeyName.size() == 0 )  {
				for ( Iterator iter =  htl_SessionInfo.entrySet().iterator(); iter.hasNext(); ) {
						Map.Entry entry = (Map.Entry)iter.next();
						a_htl_source.put( (String)entry.getKey(), entry.getValue() ); 
				}
			}	else	{
	            //以迴圈方式讀取 a_vtr_KeyName 中的每一個項目, 並放到a_lhm_source中
				Enumeration enu = a_vtr_KeyName.elements();
				while (enu.hasMoreElements())
				{
					str_Item = (String) enu.nextElement();
		    		StringTokenizer tokenizer = new StringTokenizer(str_Item, ",");
					i_count = tokenizer.countTokens();
					if	( i_count == 1 )  {
						str_KeyName = str_Item;
						str_NewKeyName = str_Item;			
					} else  {
						str_KeyName			= tokenizer.nextToken();
						str_NewKeyName	= tokenizer.nextToken();			
					}
					
					a_htl_source.put( str_NewKeyName, htl_SessionInfo.get(str_KeyName) );
				}
			}
			
			// 自參數中移除 SessionInfo 的資料
			a_htl_source.remove("SessionInfo");
			
		}
		catch (Exception exc)
		{
			log.error("",exc);
			throw new TopException("00001", "SystemUtil.extractSessionInfoFromLHM()");
		}
	}
	
	/*********
	 * 解 FirstPKCode 
	 * 將 FirstPKCode 依 "," 拆解為多個 String，放置在回傳的 Vector 內
	 * @param a_str_FirstPKCode
	 * @return
	 * @throws TopException
	 */
	public static Vector extractFirstPKCode(String a_str_FirstPKCode) throws TopException
	{
		try
		{
			// 判斷傳入的 String 是否包含了空字串的 Key 值，亦即是字串包含 ",," 的狀況
			// 此時必須將 ",," 轉換成 ", ,"，否則無法正常使用 StringTokenizer
			StringBuffer stb_FirstPKCode = new StringBuffer(a_str_FirstPKCode);
			int i_Position;
			while((i_Position = stb_FirstPKCode.toString().indexOf(",,")) >= 0)
			{
				stb_FirstPKCode.delete(i_Position, i_Position + 2);
				stb_FirstPKCode.insert(i_Position, ", ,");
			}
			
			//拆解後將鍵值的各個欄位值置入回傳的 Vector 內
			Vector vtr_ComposeKeyValue = new Vector();
			StringTokenizer obj_strToken = new StringTokenizer(stb_FirstPKCode.toString(), ",");
			String str_KeyValue = "";
			while(obj_strToken.hasMoreTokens())
			{
				vtr_ComposeKeyValue.addElement(obj_strToken.nextToken().trim());
			}
			
			// 若傳入的鍵值是以 "," 結尾表示還有一個 Key 值是空白
			if (stb_FirstPKCode.toString().endsWith(","))
			{
				vtr_ComposeKeyValue.addElement("");
			}
			//add by wushh 20040823
			// 若傳入的鍵值是以 "," 开始表示還有一個 Key 值是空白
			if (stb_FirstPKCode.toString().startsWith(","))
			{
				vtr_ComposeKeyValue.add(0,"");
			}
			//add by wushh 20040823
			log.debug("" + vtr_ComposeKeyValue);
			
			return vtr_ComposeKeyValue;
		}
		catch (Exception exc)
		{
			log.error("",exc);
			throw new TopException("00001", "SystemUtil.extractFirstPKCode");
		}
	}
	
    /*********
     * 使用時機：自一個 TopLinkedHashMap 取值以不同的 Key 值
     * 			 加入另一個 TopLinkedHashMap 內
     * 方法說明：
     * 		以 a_str_NewKeyName 為 Key 值 put a_lhm_Source 內 
     * 		a_str_OldKeyName 的 Value 至 a_lhm_Target 內
     **********/ 
    public static void putToLHM(
    			TopLinkedHashMap a_lhm_Source,
				String a_str_OldKeyName,
    			TopLinkedHashMap a_lhm_Target,
    			String a_str_NewKeyName) throws TopException
	{
		try
		{
			if (!a_lhm_Source.containsKey(a_str_OldKeyName))
				throw new TopException("00002", "SystemUtil.putToLHM/Key " + a_str_OldKeyName + " not exist");
				
			a_lhm_Target.put(a_str_NewKeyName, (String) a_lhm_Source.get(a_str_OldKeyName));
		}
		catch (TopException exc)
		{
			log.error("",exc);
			throw exc;
		}
		catch (Exception exc)
		{
			log.error("",exc);
			throw new TopException("00001", "SystemUtil.putToLHM");
		}
	}		
    	
    /*********
     * 使用時機：自一個 TopLinkedHashMap 取值加入另一個 TopLinkedHashMap 內
     * 方法說明：
     * 		以 a_str_KeyName 為 Key 值 put a_lhm_Source 內
     * 		相同 Key Name 的 Value 至 a_lhm_Target 內
     * 特殊參數說明：若 a_str_KeyName 為空白或 null 則	
     * 				表示將 a_lhm_Source 內所有的 Key Name 及 Key Value
     * 				加入 a_lhm_Target 內
     **********/ 
    public static void putToLHM(
    		TopLinkedHashMap a_lhm_Source,
    		TopLinkedHashMap a_lhm_Target,
    		String a_str_KeyName) throws TopException
    {
		try
		{
			if (   a_str_KeyName == null 
				|| a_str_KeyName.equals(""))
			{
				Iterator itr_Set = a_lhm_Source.entrySet().iterator();
				while (itr_Set.hasNext())
				{
					Map.Entry entryItem = (Map.Entry) itr_Set.next();
					a_lhm_Target.put((String) entryItem.getKey(), 
									 (String) entryItem.getValue());
				}								
			}
			else
			{
				if (!a_lhm_Source.containsKey(a_str_KeyName))
					throw new TopException("00002", "SystemUtil.putToLHM.put/Key " + a_str_KeyName + " not exist");
				a_lhm_Target.put(a_str_KeyName, (String) a_lhm_Source.get(a_str_KeyName));
			}
		}
		catch (TopException exc)
		{
			log.error("",exc);
			throw exc;
		}
		catch (Exception exc)
		{
			log.error("",exc);
			throw new TopException("00001", "SystemUtil.putToLHM");
		}    	
    }

    /*********
     * 使用時機：自一個 TopLinkedHashMap 取值
     * 			 以加上 "_" + a_i_Index 為 Key 
     * 			 加入另一個 TopLinkedHashMap 內
     * 方法說明：
     * 		以 a_str_KeyName + "_" + a_i_Index 為 Key 值
     * 		put a_lhm_Source 內相同 Key Name 的 Value
     * 		至 a_lhm_Target 內
     * 特殊參數說明：若 a_str_KeyName 為空白或 null 則	
     * 				表示將 a_lhm_Source 內所有的 Key Name 及 Key Value
     * 				加入 a_lhm_Target 內
     **********/ 
    public static void putToLHM(
    		TopLinkedHashMap a_lhm_Source,
    		TopLinkedHashMap a_lhm_Target,
    		String a_str_KeyName,
    		int a_i_Index) throws TopException
    {
		try
		{
			if (   a_str_KeyName == null 
				|| a_str_KeyName.equals(""))
			{
				Iterator itr_Set = a_lhm_Source.entrySet().iterator();
				while (itr_Set.hasNext())
				{
					Map.Entry entryItem = (Map.Entry) itr_Set.next();
					a_lhm_Target.put((String) entryItem.getKey() + "_" + a_i_Index, 
									 (String) entryItem.getValue());
				}								
			}
			else
			{			
				if (!a_lhm_Source.containsKey(a_str_KeyName))
					throw new TopException("00002", "SystemUtil.putToLHM/Key " + a_str_KeyName + " not exist");
				a_lhm_Target.put(a_str_KeyName + "_" + a_i_Index, (String) a_lhm_Source.get(a_str_KeyName));
			}
		}
		catch (TopException exc)
		{
			log.error("",exc);
			throw exc;
		}
		catch (Exception exc)
		{
			log.error("",exc);
			throw new TopException("00001", "SystemUtil.putToLHM");
		}    	
    }  
    
    // 初始化 ApproveInfo 的內容
    public static void putEmptyApproveInfo(
    		TopLinkedHashMap a_lhm_RecordData) throws TopException
   	{
   		try
   		{
   			a_lhm_RecordData.put("ApproveID", "");
   			a_lhm_RecordData.put("ApproveStatus", "Z");
   			a_lhm_RecordData.put("ApproveDate", "");
   			a_lhm_RecordData.put("ApproveTime", "");
   		}
   		catch (Exception exc)
   		{
   			log.error("",exc);
   			throw new TopException("00001", "SystemUtil.putEmptyApproveInfo");
   		}
   	}  	
	
	// 置入已放行內容
	public static void putYesApproveInfo(
    		TopLinkedHashMap a_lhm_RecordData,
    		String a_str_ApproveID) throws TopException
   	{
   		try
   		{
   			a_lhm_RecordData.put("ApproveID", a_str_ApproveID);
   			a_lhm_RecordData.put("ApproveStatus", "Y");
   			a_lhm_RecordData.put("ApproveDate", (String) Machine.getSystemDateTime().get("SystemDate"));
   			a_lhm_RecordData.put("ApproveTime", (String) Machine.getSystemDateTime().get("SystemTime"));
   		}
   		catch (Exception exc)
   		{
   			log.error("",exc);
   			throw new TopException("00001", "SystemUtil.putYesApproveInfo");
   		}
   	}  	

//	// 置入系統自動放行內容
//	public static void putAutoYesApproveInfo(
//    		TopLinkedHashMap a_lhm_RecordData) throws TopException
//   	{
//   		try
//   		{
//   			a_lhm_RecordData.put("ApproveID", StaticCommon.getSystemTitle());
//   			a_lhm_RecordData.put("ApproveStatus", "Y");
//   			a_lhm_RecordData.put("ApproveDate", (String) Machine.getSystemDateTime().get("SystemDate"));
//   			a_lhm_RecordData.put("ApproveTime", (String) Machine.getSystemDateTime().get("SystemTime"));
//   		}
//   		catch (Exception exc)
//   		{
//   			log.error("",exc);
//   			throw new TopException("00001", "SystemUtil.putAutoYesApproveInfo");
//   		}
//   	}  	

	// 置入放行時選擇不放行的內容
	public static void putNoApproveInfo(
    		TopLinkedHashMap a_lhm_RecordData,
    		String a_str_ApproveID) throws TopException
   	{
   		try
   		{
   			a_lhm_RecordData.put("ApproveID", a_str_ApproveID);
   			a_lhm_RecordData.put("ApproveStatus", "N");
   			a_lhm_RecordData.put("ApproveDate", (String) Machine.getSystemDateTime().get("SystemDate"));
   			a_lhm_RecordData.put("ApproveTime", (String) Machine.getSystemDateTime().get("SystemTime"));
   		}
   		catch (Exception exc)
   		{
   			log.error("",exc);
   			throw new TopException("00001", "SystemUtil.putNoApproveInfo");
   		}
   	}

	// 置入踢回(ApproevStatus="K")的內容
	public static void putKickApproveInfo(
    		TopLinkedHashMap a_lhm_RecordData,
    		String a_str_ApproveID) throws TopException
   	{
   		try
   		{
   			a_lhm_RecordData.put("ApproveID", a_str_ApproveID);
   			a_lhm_RecordData.put("ApproveStatus", "K");
   			a_lhm_RecordData.put("ApproveDate", (String) Machine.getSystemDateTime().get("SystemDate"));
   			a_lhm_RecordData.put("ApproveTime", (String) Machine.getSystemDateTime().get("SystemTime"));
   		}
   		catch (Exception exc)
   		{
   			log.error("",exc);
   			throw new TopException("00001", "SystemUtil.putKickApproveInfo");
   		}
   	}

	// 維護時置入新增的異動狀態內容
	public static void putAddLastMaintainInfo(
    		TopLinkedHashMap a_lhm_RecordData,
    		String a_str_MaintainUserID) throws TopException
   	{
   		try
   		{
   			a_lhm_RecordData.put("MaintainUserID", a_str_MaintainUserID);
   			a_lhm_RecordData.put("ModifyType", "A");
   			a_lhm_RecordData.put("SystemDate", (String) Machine.getSystemDateTime().get("SystemDate"));
   			a_lhm_RecordData.put("SystemTime", (String) Machine.getSystemDateTime().get("SystemTime"));
   		}
   		catch (Exception exc)
   		{
   			log.error("",exc);
   			throw new TopException("00001", "SystemUtil.putAddLastMaintainInfo");
   		}
   	} 

	// 維護時置入修改的異動狀態內容
	public static void putModifyLastMaintainInfo(
    		TopLinkedHashMap a_lhm_RecordData,
    		String a_str_MaintainUserID) throws TopException
   	{
   		try
   		{
   			a_lhm_RecordData.put("MaintainUserID", a_str_MaintainUserID);
   			a_lhm_RecordData.put("ModifyType", "M");
   			a_lhm_RecordData.put("SystemDate", (String) Machine.getSystemDateTime().get("SystemDate"));
   			a_lhm_RecordData.put("SystemTime", (String) Machine.getSystemDateTime().get("SystemTime"));
   		}
   		catch (Exception exc)
   		{
   			log.error("",exc);
   			throw new TopException("00001", "SystemUtil.putModifyLastMaintainInfo");
   		}
   	}  	

	// 維護時置入註記為刪除的異動狀態內容
	public static void putDeleteLastMaintainInfo(
    		TopLinkedHashMap a_lhm_RecordData,
    		String a_str_MaintainUserID) throws TopException
   	{
   		try
   		{
   			a_lhm_RecordData.put("MaintainUserID", a_str_MaintainUserID);
   			a_lhm_RecordData.put("ModifyType", "D");
   			a_lhm_RecordData.put("SystemDate", (String) Machine.getSystemDateTime().get("SystemDate"));
   			a_lhm_RecordData.put("SystemTime", (String) Machine.getSystemDateTime().get("SystemTime"));
   		}
   		catch (Exception exc)
   		{
   			log.error("",exc);
   			throw new TopException("00001", "SystemUtil.putDeleteLastMaintainInfo");
   		}
   	}  
   	

//	// 置入系統自動新增的異動狀態內容
//	public static void putAutoAddLastMaintainInfo(
//    		TopLinkedHashMap a_lhm_RecordData) throws TopException
//   	{
//   		try
//   		{
//   			a_lhm_RecordData.put("MaintainUserID", StaticCommon.getSystemTitle());
//   			a_lhm_RecordData.put("ModifyType", "A");
//   			a_lhm_RecordData.put("SystemDate", (String) Machine.getSystemDateTime().get("SystemDate"));
//   			a_lhm_RecordData.put("SystemTime", (String) Machine.getSystemDateTime().get("SystemTime"));
//   		}
//   		catch (Exception exc)
//   		{
//   			log.error("",exc);
//   			throw new TopException("00001", "SystemUtil.putAutoAddLastMaintainInfo");
//   		}
//   	} 
   		
//	// 置入系統自動修改的異動狀態內容
//	public static void putAutoModifyLastMaintainInfo(
//			TopLinkedHashMap a_lhm_RecordData) throws TopException
//	{
//		try
//		{
//			a_lhm_RecordData.put("MaintainUserID", StaticCommon.getSystemTitle());
//			a_lhm_RecordData.put("ModifyType", "M");
//			a_lhm_RecordData.put("SystemDate", (String) Machine.getSystemDateTime().get("SystemDate"));
//			a_lhm_RecordData.put("SystemTime", (String) Machine.getSystemDateTime().get("SystemTime"));
//		}
//		catch (Exception exc)
//		{
//			log.error("",exc);
//			throw new TopException("00001", "SystemUtil.putAutoModifyLastMaintainInfo");
//		}
//	} 

//	// 維護時置入新增的異動狀態內容
//	public static void putAutoSettleLastMaintainInfo(
//			TopLinkedHashMap a_lhm_RecordData) throws TopException
//	{
//		try
//		{
//			a_lhm_RecordData.put("MaintainUserID", StaticCommon.getSystemTitle());
//			a_lhm_RecordData.put("ModifyType", "S");
//			a_lhm_RecordData.put("SystemDate", (String) Machine.getSystemDateTime().get("SystemDate"));
//			a_lhm_RecordData.put("SystemTime", (String) Machine.getSystemDateTime().get("SystemTime"));
//		}
//		catch (Exception exc)
//		{
//			log.error("",exc);
//			throw new TopException("00001", "SystemUtil.putAddLastMaintainInfo");
//		}
//	} 
	
	/********
	 * 記錄該筆記錄的交易員異動內容
	 * @param a_lhm_RecordData
	 * @param a_str_DealerID
	 * @throws TopException
	 */
	public static void putDealMaintainInfo(
		TopLinkedHashMap a_lhm_RecordData,
		String a_str_DealerID) throws TopException {
		try{
			a_lhm_RecordData.put("DealerID", a_str_DealerID);
			a_lhm_RecordData.put("DealDate", (String) Machine.getSystemDateTime().get("SystemDate"));
			a_lhm_RecordData.put("DealTime", (String) Machine.getSystemDateTime().get("SystemTime"));
		}
		catch (Exception exc){
			log.error("",exc);
			throw new TopException("00001", "SystemUtil.putDealMaintainInfo");
		}
	}

	/**********
	 * 記錄該筆記錄的 Chiedf 放行內容為未經放行的內容
	 * @param a_lhm_RecordData
	 * @throws TopException
	 */
	public static void putEmptyChiefDealInfo(
		TopLinkedHashMap a_lhm_RecordData) throws TopException {
		try{
			a_lhm_RecordData.put("ChiefDealerID", "");
			a_lhm_RecordData.put("ChiefDealDate", "");
			a_lhm_RecordData.put("ChiefDealTime", "");
			a_lhm_RecordData.put("ChiefDealStatus", "Z");		
		}
		catch (Exception exc){
			log.error("",exc);
			throw new TopException("00001", "SystemUtil.putEmptyChiefDealInfo");
		}
	}	
   	
   	/**
   	 * 記錄該筆記錄的 Chiedf 放行內容為放行的內容
   	 * @param a_lhm_RecordData
   	 * @param a_str_ChiefDealerID
   	 * @throws TopException
   	 */
	public static void putYesChiefDealInfo(
            TopLinkedHashMap a_lhm_RecordData, String a_str_ChiefDealerID) throws TopException {
		try{
			a_lhm_RecordData.put("ChiefDealerID", a_str_ChiefDealerID);
			a_lhm_RecordData.put("ChiefDealDate", (String) Machine.getSystemDateTime().get("SystemDate"));
			a_lhm_RecordData.put("ChiefDealTime", (String) Machine.getSystemDateTime().get("SystemTime"));
			a_lhm_RecordData.put("ChiefDealStatus", "Y");		
		}
		catch (Exception exc){
			log.error("",exc);
			throw new TopException("00001", "SystemUtil.putYesChiefDealInfo");
		}
	}
	
	/**
	 * 記錄該筆記錄的 Chiedf 放行內容為踢回的內容
	 * @param a_lhm_RecordData
	 * @param a_str_ChiefDealerID
	 * @throws TopException
	 */
	public static void putNoChiefDealInfo(
            TopLinkedHashMap a_lhm_RecordData, String a_str_ChiefDealerID) throws TopException {
		try{
			a_lhm_RecordData.put("ChiefDealerID", a_str_ChiefDealerID);
			a_lhm_RecordData.put("ChiefDealDate", (String) Machine.getSystemDateTime().get("SystemDate"));
			a_lhm_RecordData.put("ChiefDealTime", (String) Machine.getSystemDateTime().get("SystemTime"));
			a_lhm_RecordData.put("ChiefDealStatus", "N");		
		}
		catch (Exception exc){
			log.error("",exc);
			throw new TopException("00001", "SystemUtil.putNoChiefDealInfo");
		}
	}	
   	/**
   	 * 使用時機 : 將傳入的 String 轉化為符合SQL語法的 String. 
   	 *                     (e.g. 處理撇節的問題 : "Country's Code" --> "Country''s Code")
   	 * @param a_str_UnFormattedStr
   	 * @return String
   	 * @throws TopException
   	 */
	public static String formatSQLString(String a_str_UnFormattedStr) throws TopException
   	{
		String str_Delim = "'";
		StringBuffer stb_Buffer = new StringBuffer();
		
		try
		{			
			StringTokenizer obj_StrToken = new StringTokenizer(a_str_UnFormattedStr, str_Delim, true);
			
			int i = 0;
			while (obj_StrToken.hasMoreTokens()) 
			{
				String str_Token = obj_StrToken.nextToken();
				
				if (str_Token.equals("'"))
 				    stb_Buffer.append("''");
				else    
				    stb_Buffer.append(str_Token);                       
			}
			
            return stb_Buffer.toString();
		}
		catch (Exception exc)
		{
			log.error("",exc);
			throw new TopException("00001", "SystemUtil.formatSQLString()");
		}   		   	   	
   	}

    // 維護時置入新增的異動狀態內容
    // add By Hoy 2004/03/26
    public static void putSettleLastMaintainInfo(TopLinkedHashMap a_lhm_RecordData,
                                                                         String a_str_MaintainUserID) throws TopException
    {
        try
        {
            a_lhm_RecordData.put("MaintainUserID", a_str_MaintainUserID);
            a_lhm_RecordData.put("ModifyType", "S");
            a_lhm_RecordData.put("SystemDate", (String) Machine.getSystemDateTime().get("SystemDate"));
            a_lhm_RecordData.put("SystemTime", (String) Machine.getSystemDateTime().get("SystemTime"));
        }
        catch (Exception exc)
        {
            log.error("",exc);
            throw new TopException("00001", "SystemUtil.putSettleLastMaintainInfo");
        }
    } 

	// 初始化 WriteOffInfo 的內容
	public static void putEmptyWriteOffInfo(
			TopLinkedHashMap a_lhm_RecordData) throws TopException
	{
		try
		{
			a_lhm_RecordData.put("WriteOffID", "");
			a_lhm_RecordData.put("WriteOffDate", "");
			a_lhm_RecordData.put("WriteOffTime", "");
		}
		catch (Exception exc)
		{
			log.error("",exc);
			throw new TopException("00001", "SystemUtil.putEmptyWriteOffInfo");
		}
	}  	

	// 置入人工核銷的核銷內容
	public static void putWriteOffInfo(
			TopLinkedHashMap a_lhm_RecordData,
			String a_str_WriteOffID) throws TopException
	{
		try
		{
			a_lhm_RecordData.put("WriteOffID", a_str_WriteOffID);
			a_lhm_RecordData.put("WriteOffDate", (String) Machine.getSystemDateTime().get("SystemDate"));
			a_lhm_RecordData.put("WriteOffTime", (String) Machine.getSystemDateTime().get("SystemTime"));
		}
		catch (Exception exc)
		{
			log.error("",exc);
			throw new TopException("00001", "SystemUtil.putWriteOffInfo");
		}
	}

	// 置入人工核銷的核銷內容
//	public static void putAutoWriteOffInfo(
//			TopLinkedHashMap a_lhm_RecordData) throws TopException
//	{
//		try
//		{
//			a_lhm_RecordData.put("WriteOffID", StaticCommon.getSystemTitle());
//			a_lhm_RecordData.put("WriteOffDate", (String) Machine.getSystemDateTime().get("SystemDate"));
//			a_lhm_RecordData.put("WriteOffTime", (String) Machine.getSystemDateTime().get("SystemTime"));
//		}
//		catch (Exception exc)
//		{
//			log.error("",exc);
//			throw new TopException("00001", "SystemUtil.putAutoWriteOffInfo");
//		}
//	}
   			       		    	 		     	  	
	/**
	 * 取得報文重發時新的Tag20欄位值(自動加上最末碼的序號(A,B,D,E...Z))
	 * 
	 * @author Steven
	 * @param a_str_DealNo
	 * @return String
	 * @throws TopException
	 */	
	public static String getNewTag20Value(String a_str_DealNo) throws TopException
	{		
		String str_Seqno;
		
		try
		{				
			if (a_str_DealNo.length() > 15)
			{
				str_Seqno = a_str_DealNo.substring(15);
				
				for (int i=0;i<25;i++)
				{
					if (str_Seqno.equals(str_SeqnoArray[i]))
					{
						str_Seqno = str_SeqnoArray[i+1];
						break;
					}					    						
				}                            								
			}			    
			else
				str_Seqno = "A";
							   		
			return a_str_DealNo.substring(0,15) + str_Seqno;				   			        		
		}
		catch (Exception exc)
		{
			log.error("",exc);
			throw new TopException("00001", "SystemUtil.getNewTag20Value");
		}		
	}
   			       		    	 		     	  		   			
//	/**
//	 * 檢核交易踢回後是否會計帳起息日(ValueAccDate) <= 上一個營業日 (modify by teresa 20041207)
//	 * 
//	 * @param a_str_ValueAccDate
//	 * @param a_str_BizDate
//	 * @param a_str_DepNo
//	 * @param a_str_DealNo
//	 * @param a_str_Currency
//	 * @param a_d_Amt
//	 * @return String
//	 * @throws TopException
//	 */
//	public static String checkReleaseValueAccDate( String a_str_ValueAccDate, 
//																					String a_str_BizDate,
//																					String a_str_DepNo,
//																					String a_str_DealNo,
//																					String a_str_Currency,
//																					double a_d_Amt ) throws TopException
//	{
//		String str_Msg = "";
//		
//		try
//		{		
//			DepBiz	obj_DepBiz	=	new	DepBiz();
//			obj_DepBiz.initProperties(a_str_DepNo);
//
//			if ( ! ValueCheckUtil.isNull(a_str_ValueAccDate) && 
//			        a_str_ValueAccDate.compareTo(obj_DepBiz.getLastBizDate()) <= 0 )
//			{
//				str_Msg = "(处室=" + a_str_DepNo + 
//								 ",交易编号=" + a_str_DealNo + 
//								 ",币种=" + a_str_Currency + 
//								 ",金额=" + NumericUtil.formatDouble(a_d_Amt , 2) + 
//								 ",起息日=" + a_str_ValueAccDate + ")";												
//			}
//	        
//			return str_Msg;
//		}
//		catch (Exception exc)
//		{
//			log.error("",exc);
//			throw new TopException("00001", "SystemUtil.checkReleaseValueAccDate()");
//		}						
//	}
   			       		    	 		     	  	
	public static String transferUTF8(String a_str_Param)
	{
		String str_utf8 = "";
		
		try 
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(new StringBufferInputStream(a_str_Param),"UTF-8"));
			str_utf8 =new String(StrUtils.Hex2Bin(ESAPI.encoder().encodeForHTML(StrUtils.toHex((reader.readLine()).getBytes())).getBytes()));
		}
		catch (Exception exc)
		{
			log.error("",exc);
		}
		
		return str_utf8;
	}
}
