/*
* 檔案名：AmountUtil.java
* 文件描述：Java Class
* 修改人：廖偉銘
* 修改時間：2003年07月23日, 上午 10:00
* 修改單號：V1.004
*/
package topibs.utility;

import fstop.util.exception.TopException;
import lombok.extern.slf4j.Slf4j;

//import topibs.bo.*;
//import topibs.bo.rate.*;


/**
 * @類別名：AmountUtil.java
 * @功能概要：處理一些金額方面的問題
 * @author：nelson
 * @version：1.004
 * 
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 * 
 * 	修改日期：2003/12/15
 * 	修改人員：Rebecca
 * 	修改內容：為加快執行的速度，在 Static 的方法內即讀入 CurrInfo 的資料，
 * 			  以便運算時使用，而不須要每次運算時就去讀 CurrInfo 資料
 */	
@Slf4j
public class AmountUtil
{
//    public static TopLinkedHashMap p_lhm_CurrCollection;
//    
//    /** Execute during class loading */
//    static 
//    {
//        initCurrency();        
//    }
//    
//    /** Initial the unique Currency object */
//    private static void initCurrency()
//    {
//        try
//        {
//            p_lhm_CurrCollection = new TopLinkedHashMap();
//            
//            Vector vtr_Fields = new Vector();
//            vtr_Fields.addElement("CarryType");
//            vtr_Fields.addElement("Digital");
//            vtr_Fields.addElement("IntBaseType");
//            
//      		TopLinkedHashMap lhm_SelectCondition = new TopLinkedHashMap();
//      		lhm_SelectCondition.put("ApproveStatus", "Y");
//            
//            TableMaintainDAO obj_DAO = new TableMaintainDAO();
//            p_lhm_CurrCollection = obj_DAO.findByPartialKey(Currency.TABLENAME, 
//            												vtr_Fields, 
//            												lhm_SelectCondition, 
//            												new TopLinkedHashMap(), 
//            												Currency.getFieldsInfo(), 
//            												Currency.getPrimaryKey());   
//        }
//        catch (SQLDataNotExistException exc)
//        {
//        	Message.debug(exc);
//        }
//        catch (Exception exc)
//        {
//            Message.debug(exc);
//        }
//    }
//    
//    /*********
//     * 由於 AmoountUtil 在系統一開始時就會將幣別資料讀入 Static 內，
//     * 所以在幣別資料修改或刪除時，應將此筆資料自 static 內移除
//     * @param a_str_Currency
//     * @throws TopException
//     */
//    public static void removeFromStatic(String a_str_Currency){
//    	if (p_lhm_CurrCollection.containsKey(a_str_Currency)){
//    		p_lhm_CurrCollection.remove(a_str_Currency);
//    	}
//    }
//            
//    /**取得各幣別關於金額的小數位數及運算後的進位方式，並將金額作轉換換*/
//    public static double getFormatedAmt(String a_str_Ccy, double a_d_Amount) throws TopException
//    {
//        double d_Amount = 0;
//        int i_Digital = 0;
//        String str_CarryType = new String();
//        
//        try
//        {
//			TopLinkedHashMap lhm_CurrData = getCurrInfo(a_str_Ccy);			
//            i_Digital = getCurrDigital(lhm_CurrData);            
//            str_CarryType = getCurrCarryType(lhm_CurrData);            
//            d_Amount = AmountUtil.formateAmt(i_Digital, str_CarryType, a_d_Amount);
//        }
//        catch (TopException exc)
//        {
//            Message.debug(exc);
//            throw exc;
//        }
//        catch (Exception exc)
//        {
//            Message.debug(exc);
//            throw new TopException("00001", "");
//        }             
//        return d_Amount;
//    }
//    
//    /**依傳入的本金、利率、天數、幣種代碼計算利息*/
//    public double caculateInterest(double a_d_Principal, double a_d_IntRate, int a_i_Days, String a_str_Currency, String a_str_Year) throws TopException
//    {
//        double d_interest = 0;
//        int i_Digital = 0;
//        String str_CarryType = new String();
//        String str_IntBaseType = new String();
//        
//        try
//        {
//            //取得幣種資料內應計算的小數位數、進位方式及計息基準天數的值
//			TopLinkedHashMap lhm_CurrData = getCurrInfo(a_str_Currency);			
//            i_Digital = getCurrDigital(lhm_CurrData);            
//            str_CarryType = getCurrCarryType(lhm_CurrData);
//            str_IntBaseType = getCurrIntBaseType(lhm_CurrData);
//            
//            //判斷利息計算基準天數，並計算出利息
//            if(str_IntBaseType.equals("1") || str_IntBaseType.equals("4"))
//            {
//                d_interest = a_d_Principal * a_d_IntRate  / 100 *  a_i_Days / 360;
//            }
//            if(str_IntBaseType.equals("2") || str_IntBaseType.equals("5"))
//            {
//                d_interest = a_d_Principal * a_d_IntRate  / 100 *  a_i_Days / 365;
//            }
//            if(str_IntBaseType.equals("3") || str_IntBaseType.equals("6"))
//            {
//                Calendar obj_cal = Calendar.getInstance();
//                obj_cal.set(Integer.parseInt(a_str_Year), 12, 31);
//                int i_Year = obj_cal.get(Calendar.DAY_OF_YEAR);
//                
//                d_interest = a_d_Principal * a_d_IntRate  / 100 *  a_i_Days / i_Year;
//            }
//            
//            d_interest = AmountUtil.formateAmt(i_Digital, str_CarryType, d_interest);
//        }
//        catch (TopException exc)
//        {
//            Message.debug(exc);
//            throw exc;
//        }
//        catch (Exception exc)
//        {
//            Message.debug(exc);
//            throw new TopException("00001", "");
//        }      
//        return d_interest;          
//    }
//    
//	// 依傳入的本金、利率、起始日期、結束日期、幣別、年度、
//	// 利息計算基準天數
//	public static double calculateInterest(double a_d_Principal, 
//											 double a_d_IntRate, 
//											 String a_str_StartDate,
//											 String a_str_EndDate, 
//											 String a_str_Currency, 
//											 String a_str_Year,
//											 String a_str_IntBaseType) throws TopException
//	{
//		double d_interest = 0;
//		int i_Digital = 0;
//		String str_CarryType = new String();
//
//		try
//		{
//			//取得幣種資料內應計算的小數位數、進位方式及計息基準天數的值
//			TopLinkedHashMap lhm_CurrData = getCurrInfo(a_str_Currency);			
//			i_Digital = getCurrDigital(lhm_CurrData);            
//			str_CarryType = getCurrCarryType(lhm_CurrData);  
//			String str_IntBaseType = "";   
//			if (a_str_IntBaseType == null)
//				str_IntBaseType = getCurrIntBaseType(lhm_CurrData);
//			else
//				str_IntBaseType = a_str_IntBaseType;
//            
//			// 計算天數
//			int i_Days = DateUtil.calculateInterestDays(a_str_StartDate, a_str_EndDate, str_IntBaseType);
//			int i_YearDays = 0;
//			//判斷利息計算基準天數，並計算出利息
//			char c_IntBaseType = str_IntBaseType.charAt(0);
//			switch (c_IntBaseType)
//			{
//				case '1':
//				case '4':
//				case '7':
//					i_YearDays = 360;
//					break;
//				case '2':
//				case '5':
//				case '8':
//					i_YearDays = 365;
//					break;
//				case '3':
//				case '6':
//					Calendar obj_cal = Calendar.getInstance();
//					
//					/*** Modified bye Steven, 2004/08/23 ***/
//					if (a_str_Year.equals(""))
//					{
//						a_str_Year = a_str_StartDate.substring(0,4);
//					}
//		     
//					obj_cal.set(Integer.parseInt(a_str_Year), 11, 31);	     
//					
////					if (a_str_Year.equals(""))
////					{
////						a_str_Year = a_str_StartDate.substring(0,4);
////					}
////					else
////					{
////						obj_cal.set(Integer.parseInt(a_str_Year), 12, 31);
////					}
//
//					i_YearDays = obj_cal.get(Calendar.DAY_OF_YEAR);    
//					break;
//			}
//			d_interest = a_d_Principal * a_d_IntRate  / 100 *  i_Days / i_YearDays;
//			d_interest = AmountUtil.formateAmt(i_Digital, 
//											   str_CarryType, 
//											   d_interest);
//		}
//		catch (TopException exc)
//		{
//			Message.debug(exc);
//			throw exc;
//		}
//		catch (Exception exc)
//		{
//			Message.debug(exc);
//			throw new TopException("00001", "AmountUtil.calculateInterest");
//		}      
//		return d_interest;          
//	}
//    
//    // 依傳入的本金、利率、天數、幣別、年度、
//    // 利息計算基準天數
//    public static double calculateInterest(double a_d_Principal, 
//    										 double a_d_IntRate, 
//    										 int a_i_Days, 
//    										 String a_str_Currency, 
//    										 String a_str_Year,
//    										 String a_str_IntBaseType) throws TopException {
//        double d_interest = 0;
//        int i_Digital = 0;
//        String str_CarryType = new String();
//
//        try {
//            //取得幣種資料內應計算的小數位數、進位方式及計息基準天數的值
//			TopLinkedHashMap lhm_CurrData = getCurrInfo(a_str_Currency);			
//			i_Digital = getCurrDigital(lhm_CurrData);            
//			str_CarryType = getCurrCarryType(lhm_CurrData);  
//			String str_IntBaseType = "";   
//            if (a_str_IntBaseType == null)
//				str_IntBaseType = getCurrIntBaseType(lhm_CurrData);
//	        else
//	        	str_IntBaseType = a_str_IntBaseType;
//            
//            // 計算天數
//            int i_YearDays = 0;
//            //判斷利息計算基準天數，並計算出利息
//            char c_IntBaseType = str_IntBaseType.charAt(0);
//            switch (c_IntBaseType)
//            {
//            	case '1':
//            	case '4':
//				case '7':
//	                i_YearDays = 360;
//	                break;
//  				case '2':
//  				case '5':
//				case '8':
//	                i_YearDays = 365;
//	                break;
//  				case '3':
//  				case '6':
//	                Calendar obj_cal = Calendar.getInstance();
//	                if (a_str_Year.equals("")) {
//						a_str_Year = ((String) Machine.getSystemDateTime().get("SystemDate")).substring(0,4);
//	                }
//	                else {
//    	            	obj_cal.set(Integer.parseInt(a_str_Year), 12, 31);
//	                }
//        	        i_YearDays = obj_cal.get(Calendar.DAY_OF_YEAR);    
//	               	break;
//            }
//            d_interest = a_d_Principal * a_d_IntRate  / 100 *  a_i_Days / i_YearDays;
//            d_interest = AmountUtil.formateAmt(i_Digital, 
//            								   str_CarryType, 
//            								   d_interest);
//        }
//        catch (TopException exc) {
//            Message.debug(exc);
//            throw exc;
//        }
//        catch (Exception exc) {
//            Message.debug(exc);
//            throw new TopException("00001", "AmountUtil.calculateInterest");
//        }      
//        return d_interest;          
//    }
//
//    // 依傳入的本金、利率、起始日期、結束日期、
//    // 年度、利息計算基準天數、小數位數、進位方式
//    public double calculateInterest(double a_d_Principal, 
//    								 double a_d_IntRate, 
//    								 String a_str_StartDate,
//    								 String a_str_EndDate, 
//    								 String a_str_Year,
//    								 String a_str_IntBaseType,
//    								 int a_i_Digital,
//    								 String a_str_CarryType) throws TopException
//    {
//        double d_interest = 0;
//        char c_IntBaseType;
//        
//        try
//        {   
//	        c_IntBaseType = a_str_IntBaseType.charAt(0);
//            
//            // 計算天數
//            int i_Days = DateUtil.caculateDaysBetweenTwoDate(a_str_StartDate,
//            												 a_str_EndDate);
//            int i_YearDays = 0;
//            //判斷利息計算基準天數，並計算出利息
//            switch (c_IntBaseType)
//            {
//            	case '1':
//            	case '4':
//	                i_YearDays = 360;
//	                break;
//  				case '2':
//  				case '5':
//	                i_YearDays = 365;
//	                break;
//  				case '3':
//  				case '6':
//	                Calendar obj_cal = Calendar.getInstance();
//    	            obj_cal.set(Integer.parseInt(a_str_Year), 12, 31);
//        	        i_YearDays = obj_cal.get(Calendar.DAY_OF_YEAR);    
//	               	break;
//            }
//            
//            d_interest = a_d_Principal * a_d_IntRate  / 100 *  i_Days / i_YearDays;
//            d_interest = formateAmt(a_i_Digital, a_str_CarryType, d_interest);
//        }
//        catch (TopException exc)
//        {
//            Message.debug(exc);
//            throw exc;
//        }
//        catch (Exception exc)
//        {
//            Message.debug(exc);
//            throw new TopException("00001", "AmountUtil.calculateInterest");
//        }      
//        return d_interest;          
//    }
//    
//	/**
//	 * 計算提前還款利息
//	 * 傳來Vector裡面包含多個HashMap的Object
//	 * HashMap裡面包含的資料為單筆提前付款之資料
//	 * 而HashMap裡面包含的Key值必須有 RepayAmt 提前還款金額
//	 * 												  RepayDate 提前還款日期
//	 * 												  RepayType 提前還款的Type
//	 * 若要計算"息隨本清"的利息，則傳給一個空Vector即可
//	 * @author Jesse
//	 * @param a_vtr_RepayDetailData 多筆提前還款資料
//	 * @param a_d_Principal
//	 * @param a_str_StartDate 利息起日
//	 * @param a_str_EndDate 利息止日
//	 * @param a_str_Currency 
//	 * @param a_str_IntCalculateType
//	 * @param a_str_Year
//	 * @return
//	 * @throws TopException
//	 */
//	public double calculateRepayInterest(
//		Vector a_vtr_RepayDetailData,
//		double a_d_Principal,
//		double a_d_IntRate,
//		String a_str_StartDate,
//		String a_str_EndDate,
//		String a_str_Currency,
//		String a_str_IntCalculateType,
//		String a_str_Year) throws TopException {
//		
//		double d_Interest = 0; //止息日總利息
//		double d_RepayInterest = 0; //提前還款利息
//		double d_DealAmt = a_d_Principal; //原始本金
//		double d_RepayAmt = 0; //提前還款金額
//		String str_StartDate = a_str_StartDate; //起息日
//		String str_EndDate = a_str_EndDate; //止息日
//		String str_RepayDate = ""; //提前還款日
//		String str_RepayType = ""; //還款方式
//		//int i_Days = 0;
//		try {
//			//檢核參數
//			ValueCheckUtil.checkProperty("Principal", a_d_Principal, "AmountUtil.calculateRepayInterest()");
//			ValueCheckUtil.checkProperty("IntRate", a_d_IntRate, "AmountUtil.calculateRepayInterest()");
//			ValueCheckUtil.checkProperty("StartDate", a_str_StartDate, "AmountUtil.calculateRepayInterest()");
//			ValueCheckUtil.checkProperty("EndDate", a_str_EndDate, "AmountUtil.calculateRepayInterest()");
//			ValueCheckUtil.checkProperty("Currency", a_str_Currency, "AmountUtil.calculateRepayInterest()");
//			ValueCheckUtil.checkProperty("IntCalculateType", a_str_IntCalculateType, "AmountUtil.calculateRepayInterest()");
//			ValueCheckUtil.checkProperty("Year", a_str_Year, "AmountUtil.calculateRepayInterest()");
//			//取出Vector裡面每筆提前還款資料
//			for (int i = 0; i < a_vtr_RepayDetailData.size(); i++) {
//				TopLinkedHashMap lhm_Repaydata = (TopLinkedHashMap) a_vtr_RepayDetailData.get(i);
//				d_RepayAmt = NumericUtil.transfStringToDouble((String) lhm_Repaydata.get("RepayAmt"));
//				str_RepayDate = (String) lhm_Repaydata.get("RepayDate");
//				str_RepayType = (String) lhm_Repaydata.get("RepayType");
//				//若提前還款日小於起息日，則扣除該筆還款本金資料
//				if(str_RepayDate.compareTo(a_str_StartDate) > 0)
//				{
//					//1.只還本金 & 3.先還利息再還本金(計算方式相同)
//					if (str_RepayType.equals("1") || str_RepayType.equals("3")) {
//						//提前還款利息為起始日至提前還款日的"本金"利息
//						d_RepayInterest = AmountUtil.calculateInterest(d_DealAmt, a_d_IntRate, str_StartDate, str_RepayDate, a_str_Currency, a_str_Year, a_str_IntCalculateType);
//						str_StartDate = str_RepayDate;
//						d_DealAmt = d_DealAmt - d_RepayAmt;
//						d_Interest = d_Interest + d_RepayInterest;
//					//2.息隨本清
//					} else if (str_RepayType.equals("2")) {
//						//提前還款利息為起始日至提前還款日的"提前還款金額"利息
//						//d_RepayInterest = AmountUtil.calculateInterest(d_RepayAmt, a_d_IntRate, str_StartDate, str_RepayDate, a_str_Currency, a_str_Year, a_str_IntCalculateType);
//						d_DealAmt = d_DealAmt - d_RepayAmt;
//						//d_Interest = d_Interest + d_RepayInterest;
//					} else if (str_RepayType.equals("")) {
//						d_DealAmt = d_DealAmt - d_RepayAmt;
//					}
//					else {
//						throw new TopException("00006", "AmountUtil.calculateRepayInterest()");
//					}
//				} else {
//					d_DealAmt = d_DealAmt - d_RepayAmt;
//				}
//			}
//			d_RepayInterest = AmountUtil.calculateInterest(d_DealAmt, a_d_IntRate, str_StartDate, str_EndDate, a_str_Currency, a_str_Year, a_str_IntCalculateType);
//			d_Interest = d_Interest + d_RepayInterest;
//		}
//		catch (TopException exc) {
//			Message.debug(exc);
//			throw exc;
//		}
//		catch (Exception exc) {
//			Message.debug(exc);
//			throw new TopException("00001", "AmountUtil.calculateRepayInterest()");
//		}
//		return NumericUtil.formatDouble(d_Interest,"#.##");
//	}
//    
//    /**
//     * 計算提前還款利息
//	 * 傳來Vector裡面包含多個HashMap的Object
//	 * HashMap裡面包含的資料為單筆提前付款之資料
//	 * 而HashMap裡面包含的Key值必須有 RepayAmt 提前還款金額
//	 * 												  RepayDate 提前還款日期
//	 * 												  RepayType 提前還款的Type
//	 * 若要計算"息隨本清"的利息，則傳給一個空Vector即可
//     * @param a_vtr_RepayDetailData
//     * @param a_d_Principal
//     * @param a_d_IntRate
//     * @param a_str_StartDate
//     * @param a_str_EndDate
//     * @param a_str_Currency
//     * @param a_str_IntCalculateType
//     * @param a_str_Year
//     * @return
//     * @throws TopException
//     */
//	public double calculateRepaySDRInterest(
//										Vector a_vtr_RepayDetailData,
//										double a_d_Principal,
//										double a_d_IntRate,
//										String a_str_StartDate,
//										String a_str_EndDate,
//										String a_str_Currency,
//										String a_str_IntCalculateType,
//										String a_str_Year) throws TopException {
//	
//		double d_Interest = 0; //止息日總利息
//		double d_RepayInterest = 0; //提前還款利息
//		double d_DealAmt = a_d_Principal; //原始本金
//		double d_RepayAmt = 0; //提前還款金額
//		String str_StartDate = a_str_StartDate; //起息日
//		String str_EndDate = a_str_EndDate; //止息日
//		String str_RepayDate = ""; //提前還款日
//		String str_RepayType = ""; //還款方式
//		//int i_Days = 0;
//		try {
//			//檢核參數
//			ValueCheckUtil.checkProperty("Principal", a_d_Principal, "AmountUtil.calculateRepayInterest()");
//			ValueCheckUtil.checkProperty("IntRate", a_d_IntRate, "AmountUtil.calculateRepayInterest()");
//			ValueCheckUtil.checkProperty("StartDate", a_str_StartDate, "AmountUtil.calculateRepayInterest()");
//			ValueCheckUtil.checkProperty("EndDate", a_str_EndDate, "AmountUtil.calculateRepayInterest()");
//			//ValueCheckUtil.checkProperty("Currency", a_str_Currency, "AmountUtil.calculateRepayInterest()");
//			ValueCheckUtil.checkProperty("IntCalculateType", a_str_IntCalculateType, "AmountUtil.calculateRepayInterest()");
//			ValueCheckUtil.checkProperty("Year", a_str_Year, "AmountUtil.calculateRepayInterest()");
//			//取出Vector裡面每筆提前還款資料
//			for (int i = 0; i < a_vtr_RepayDetailData.size(); i++) {
//				TopLinkedHashMap lhm_Repaydata = (TopLinkedHashMap) a_vtr_RepayDetailData.get(i);
//				d_RepayAmt = NumericUtil.transfStringToDouble((String) lhm_Repaydata.get("RepayAmt"));
//				str_RepayDate = (String) lhm_Repaydata.get("RepayDate");
//				str_RepayType = (String) lhm_Repaydata.get("RepayType");
//				//若提前還款日小於起息日，則扣除該筆還款本金資料
//				if(str_RepayDate.compareTo(a_str_StartDate) > 0)
//				{
//					//1.只還本金 & 3.先還利息再還本金(計算方式相同)
//					if (str_RepayType.equals("1") || str_RepayType.equals("3")) {
//						//提前還款利息為起始日至提前還款日的"本金"利息
//						d_RepayInterest = AmountUtil.calculateSDRInterest(d_DealAmt, a_d_IntRate, str_StartDate, str_RepayDate, a_str_Year, a_str_IntCalculateType);
//						str_StartDate = str_RepayDate;
//						d_DealAmt = d_DealAmt - d_RepayAmt;
//						d_Interest = d_Interest + d_RepayInterest;
//					//2.息隨本清
//					} else if (str_RepayType.equals("2")) {
//						//提前還款利息為起始日至提前還款日的"提前還款金額"利息
//						//d_RepayInterest = AmountUtil.calculateInterest(d_RepayAmt, a_d_IntRate, str_StartDate, str_RepayDate, a_str_Currency, a_str_Year, a_str_IntCalculateType);
//						d_DealAmt = d_DealAmt - d_RepayAmt;
//						//d_Interest = d_Interest + d_RepayInterest;
//					}else if (str_RepayType.equals("")) {
//						d_DealAmt = d_DealAmt - d_RepayAmt;
//					} 
//					else {
//						throw new TopException("00006", "AmountUtil.calculateRepayInterest()");
//					}
//				} else {
//					d_DealAmt = d_DealAmt - d_RepayAmt;
//				}
//			}
//			d_RepayInterest = AmountUtil.calculateSDRInterest(d_DealAmt, a_d_IntRate, str_StartDate, str_EndDate, a_str_Year, a_str_IntCalculateType);
//			d_Interest = d_Interest + d_RepayInterest;
//		}
//		catch (TopException exc) {
//			Message.debug(exc);
//			throw exc;
//		}
//		catch (Exception exc) {
//			Message.debug(exc);
//			throw new TopException("00001", "AmountUtil.calculateRepayInterest()");
//		}
//		return NumericUtil.formatDouble(d_Interest,"#.##");
//	}
//    public static double calculateSDRInterest(double a_d_Principal, 
//    										    double a_d_IntRate, 
//    										    String a_str_StartDate,
//		    								    String a_str_EndDate, 
//    										    String a_str_Year,
//    										    String a_str_IntBaseType) throws TopException
//    {
//        double d_interest = 0;
//        int i_Digital = 0;
//        String str_CarryType = new String();
//        char c_IntBaseType;
//        
//        try
//        {
//	       	c_IntBaseType = a_str_IntBaseType.charAt(0);
//            
//			// 計算天數
//			int i_Days;
//			int i_StartDate, i_EndDate;
//			switch (c_IntBaseType)
//			{
//				case '4':
//				case '5':
//				case '6':
//					// 判斷起始日期及迄止日期是否為月底，若是月底則改為 30 號
//					if (DateUtil.isEndOfMonth(a_str_StartDate))
//						i_StartDate = 30;
//					else
//						i_StartDate = Integer.parseInt(a_str_StartDate.substring(6));
//						
//					if (DateUtil.isEndOfMonth(a_str_EndDate))
//						i_EndDate = 30;
//					else
//						i_EndDate = Integer.parseInt(a_str_EndDate.substring(6));						
//					
//					// 計算出整月的部份，再計算出未滿月的天數					
//					int i_Months;
//					if (i_StartDate > i_EndDate)
//					{
//						i_Months = DateUtil.calculateMonthsBetweenTwoMonth(a_str_StartDate,	a_str_EndDate) - 1;
//						i_Days = 30 - i_StartDate + i_EndDate;
//					}
//					else
//					{
//						i_Months = DateUtil.calculateMonthsBetweenTwoMonth(a_str_StartDate, a_str_EndDate);
//						
//						// 每月以 30 天計，但 2 月的月底仍以該月的月底計算計算
//						if (a_str_EndDate.substring(6).compareTo("30") > 0)
//							i_Days = i_EndDate - i_StartDate;
//						else
//							i_Days = Integer.parseInt(a_str_EndDate.substring(6)) - i_StartDate;
//					}						
//					i_Days += i_Months * 30;					
//					break;
//				default:
//				{
//					i_Days = DateUtil.caculateDaysBetweenTwoDate(a_str_StartDate,
//															 a_str_EndDate);
//				}
//			}
//
//            int i_YearDays = 0;
//            //判斷利息計算基準天數，並計算出利息
//            switch (c_IntBaseType)
//            {
//            	case '1':
//            	case '4':
//	                i_YearDays = 360;
//	                break;
//  				case '2':
//  				case '5':
//	                i_YearDays = 365;
//	                break;
//  				case '3':
//  				case '6':
//					Calendar obj_cal = Calendar.getInstance();
//					if (a_str_Year.equals(""))
//					{
//						a_str_Year = a_str_StartDate.substring(0,4);
//					}
//					else
//					{
//						obj_cal.set(Integer.parseInt(a_str_Year), 12, 31);
//					}
//					i_YearDays = obj_cal.get(Calendar.DAY_OF_YEAR);    
//	               	break;
//            }
//            
//            d_interest = a_d_Principal * a_d_IntRate  / 100 *  i_Days / i_YearDays;
//            d_interest = AmountUtil.formateAmt(2, 
//            								   "1", 
//            								   d_interest);
//        }
//        catch (TopException exc)
//        {
//            Message.debug(exc);
//            throw exc;
//        }
//        catch (Exception exc)
//        {
//            Message.debug(exc);
//            throw new TopException("00001", "AmountUtil.calculateSDRInterest");
//        }      
//        return d_interest;          
//    }
//
//	/**依傳入的本金、利率(日利率)、天數、幣種代碼計算利息*/
//	public double caculateInterest(
//			double a_d_Principal, 
//			double a_d_IntRate, 
//			int a_i_Days, 
//			String a_str_Currency) throws TopException
//	{
//		double d_interest = 0;
//		int i_Digital = 0;
//		String str_CarryType = new String();
//        
//		try
//		{
//			//取得幣種資料內應計算的小數位數、進位方式
//			TopLinkedHashMap lhm_CurrData = getCurrInfo(a_str_Currency);			
//			i_Digital = getCurrDigital(lhm_CurrData);            
//			str_CarryType = getCurrCarryType(lhm_CurrData);   
//            
//			// 計算出利息
//			d_interest = a_d_Principal * a_d_IntRate  / 100 *  a_i_Days;
//			d_interest = formateAmt(i_Digital, str_CarryType, d_interest);
//		}
//		catch (TopException exc)
//		{
//			Message.debug(exc);
//			throw exc;
//		}
//		catch (Exception exc)
//		{
//			Message.debug(exc);
//			throw new TopException("00001", "");
//		}      
//		return d_interest;          
//	}        
//
//    /*********
//     * 依傳入的幣種代碼(依據的幣種)、幣種代碼(欲換算後的幣種)、
//     * 金額(依據的金額)、匯率(依據的匯率)計算換算後的金額，
//     * 換算後的金額必須透過幣別(欲換算的幣種)
//     * 關於金額的小數位數及運算後的進位方式作轉換處理
//     *********/ 
//    public static double calculateAmount(String a_str_BaseCcy, 
//    									   String a_str_ConvertCcy,
//    									   double a_d_Amt,
//    									   double a_d_FxRate) throws TopException
//    {
//    	try
//    	{
//            if (a_d_FxRate == 0)
//            	throw new TopException("00006", "AmoutnUtil.calculateAmount/FxRate");
//            
//            // 取得換算後的幣種資料內應計算的小數位數、進位方式等內容
//			TopLinkedHashMap lhm_CurrData = getCurrInfo(a_str_ConvertCcy);			
//			int i_Digital = getCurrDigital(lhm_CurrData);            
//			String str_CarryType = getCurrCarryType(lhm_CurrData);
//            
//    		double d_ExchangeAmt = 0;
//    
//            // 取得交叉匯率報價方式資料內的報價方式的值
//            String str_Currency = "";
//            String str_CrossCcy = "";
//
//            // 兩種幣別均不是 SDR 時
//            if (!a_str_BaseCcy.equals("SDR")
//            	&& !a_str_ConvertCcy.equals("SDR"))
//            {
//            	if (a_str_BaseCcy.compareTo(a_str_ConvertCcy) > 0)
//            	{
//            		str_Currency = a_str_ConvertCcy;
//            		str_CrossCcy = a_str_BaseCcy;
//            	}
//            	else
//            	{
//            		str_Currency = a_str_BaseCcy;
//            		str_CrossCcy = a_str_ConvertCcy;
//            	}
//            	
//            	CrossOffer obj_CrossOffer = new CrossOffer();
//            	if (!obj_CrossOffer.initProperties(str_Currency, str_CrossCcy))
//            		throw new TopException("90042", "AmountUtil.calculateAmount/CrossOffer " + str_Currency + str_CrossCcy);
//            	char c_OfferType = obj_CrossOffer.getOfferType().charAt(0);
//            	
//            	switch(c_OfferType)
//            	{
//            		case '1':
//            			if (a_str_ConvertCcy.equals(str_CrossCcy))
//                            d_ExchangeAmt = a_d_Amt / a_d_FxRate;            			
//                        else
//                            d_ExchangeAmt = a_d_Amt * a_d_FxRate;                            
//            			break;
//            		case '2':
//            			if (a_str_ConvertCcy.equals(str_Currency))
//            				d_ExchangeAmt = a_d_Amt / a_d_FxRate;
//            			else
//            				d_ExchangeAmt = a_d_Amt * a_d_FxRate;            		
//            			break;
//            	}
//            }
//            else
//            {
//            	d_ExchangeAmt = a_d_Amt * a_d_FxRate;
//            }
//			
//			d_ExchangeAmt = AmountUtil.formateAmt(i_Digital, 
//												  str_CarryType, 
//												  d_ExchangeAmt);            
//            
//            return d_ExchangeAmt;
//    	}
//    	catch (TopException exc)
//    	{
//    		Message.debug(exc);
//    		throw exc;
//    	}
//    	catch (Exception exc)
//    	{
//    		Message.debug(exc);
//    		throw new TopException("00001", "AmountUtil.calculateAmount");
//    	}
//    }
//
//
//	/*********
//	 * 目的:供外匯買賣及外匯擇期計算金額
//	 * 依傳入的幣種代碼(依據的幣種)、幣種代碼(欲換算後的幣種)、
//	 * 金額(依據的金額)、匯率(依據的匯率)計算換算後的金額，
//	 * 換算後的金額必須透過幣別(欲換算的幣種)
//	 * 關於金額的小數位數及運算後的進位方式作轉換處理
//	 * 備註:當幣別為人民幣及日元時,匯率需除以 100
//	 *********/ 
//	public static double calculateAmountForFx(String a_str_BaseCcy, 
//										   String a_str_ConvertCcy,
//										   double a_d_Amt,
//										   double a_d_FxRate) throws TopException
//	{
//		try
//		{
//			if (a_d_FxRate == 0)
//				throw new TopException("00006", "AmoutnUtil.calculateAmountForFx/FxRate");
//            
//			// 取得換算後的幣種資料內應計算的小數位數、進位方式等內容
//			TopLinkedHashMap lhm_CurrData = getCurrInfo(a_str_ConvertCcy);			
//			int i_Digital = getCurrDigital(lhm_CurrData);            
//			String str_CarryType = getCurrCarryType(lhm_CurrData);
//            
//			double d_ExchangeAmt = 0;
//    
//			// 取得交叉匯率報價方式資料內的報價方式的值
//			String str_Currency = "";
//			String str_CrossCcy = "";
//
//			// 兩種幣別均不是 SDR 時
//			if (!a_str_BaseCcy.equals("SDR")
//				&& !a_str_ConvertCcy.equals("SDR"))
//			{
//				if (a_str_BaseCcy.compareTo(a_str_ConvertCcy) > 0)
//				{
//					str_Currency = a_str_ConvertCcy;
//					str_CrossCcy = a_str_BaseCcy;
//				}
//				else
//				{
//					str_Currency = a_str_BaseCcy;
//					str_CrossCcy = a_str_ConvertCcy;
//				}
//            	
//            	// ********以下這段與原 calculateAmount不同,其餘皆相同 ******
//				// 幣別為人民幣及日元時,匯率需除以 100
//            	if ( (str_Currency.equals("CNY") && str_CrossCcy.equals("JPY")) ||
//				     (str_Currency.equals("JPY") && str_CrossCcy.equals("CNY")))
//            	{
//					
//					a_d_FxRate = a_d_FxRate / 100;
//            	}
//            	//*************************************************************
//            	
//				CrossOffer obj_CrossOffer = new CrossOffer();
//				if (!obj_CrossOffer.initProperties(str_Currency, str_CrossCcy))
//					throw new TopException("90042", "AmountUtil.calculateAmountForFx/CrossOffer " + str_Currency + str_CrossCcy);
//				char c_OfferType = obj_CrossOffer.getOfferType().charAt(0);
//            	
//				switch(c_OfferType)
//				{
//					case '1':
//						if (a_str_ConvertCcy.equals(str_CrossCcy))
//							d_ExchangeAmt = a_d_Amt / a_d_FxRate;            			
//						else
//							d_ExchangeAmt = a_d_Amt * a_d_FxRate;                            
//						break;
//					case '2':
//						if (a_str_ConvertCcy.equals(str_Currency))
//							d_ExchangeAmt = a_d_Amt / a_d_FxRate;
//						else
//							d_ExchangeAmt = a_d_Amt * a_d_FxRate;            		
//						break;
//				}
//			}
//			else
//			{
//				d_ExchangeAmt = a_d_Amt * a_d_FxRate;
//			}
//			
//			d_ExchangeAmt = AmountUtil.formateAmt(i_Digital, 
//												  str_CarryType, 
//												  d_ExchangeAmt);            
//            
//			return d_ExchangeAmt;
//		}
//		catch (TopException exc)
//		{
//			Message.debug(exc);
//			throw exc;
//		}
//		catch (Exception exc)
//		{
//			Message.debug(exc);
//			throw new TopException("00001", "AmountUtil.calculateAmountForFx");
//		}
//	}
//    
//    /**********
//     * 依傳入的匯率(必須是決算牌價匯率)及折算後的幣別，計算折算後的金額
//     * @param a_str_ConvertCcy
//     * @param a_d_BaseAmt
//     * @param a_d_FxRate
//     * @return
//     * @throws TopException
//     */
//    public static double exchangeByCenterBankFxRate(
//    		String a_str_ConvertCcy,
//    		double a_d_BaseAmt,
//    		double a_d_FxRate) throws TopException
//    {
//    	try
//    	{
//    		double d_Amt;
//    		if (a_str_ConvertCcy.equals(StaticCommon.getHardCcy()))
//    			d_Amt = AmountUtil.getFormatedAmt(a_str_ConvertCcy, a_d_BaseAmt / a_d_FxRate * 100);
// 			else
// 				d_Amt = AmountUtil.getFormatedAmt(a_str_ConvertCcy, a_d_BaseAmt * a_d_FxRate / 100);
// 			
// 			return d_Amt;
//    	}
//    	catch (TopException exc)
//    	{
//    		Message.debug(exc);
//    		throw exc;
//    	}
//    	catch (Exception exc)
//    	{
//    		Message.debug(exc);
//    		throw new TopException("00001", "Amoutn.calculateAmount");
//    	}
//    }
//       
//    /*********
//     * 依傳入的年份及幣別取得決算牌價後，將金額轉換為所要求的幣別金額
//     * @param a_str_BaseCcy
//     * @param a_str_ConvertCcy
//     * @param a_str_Year
//     * @param a_d_BaseAmt
//     * @return
//     * @throws TopException
//     */
//    public static double exchangeByCenterBankFxRate(
//    		String a_str_BaseCcy,
//    		String a_str_ConvertCcy,
//    		String a_str_Year,
//    		double a_d_BaseAmt) throws TopException
//    {
//    	try
//    	{
//    		double d_FxRate;
//    		if (a_str_ConvertCcy.equals(StaticCommon.getHardCcy()))
//    			d_FxRate = CenterBankFxRate.getHardFxRate(a_str_Year, a_str_BaseCcy);
//    		else
//    			d_FxRate = CenterBankFxRate.getLocalFxRate(a_str_Year, a_str_BaseCcy);
//    			
//    		return AmountUtil.exchangeByCenterBankFxRate(a_str_ConvertCcy, a_d_BaseAmt, d_FxRate);
//    	}
//    	catch (Exception exc)
//    	{
//    		Message.debug(exc);
//    		throw new TopException("00001", "AmountUtil.calculateAmount");
//    	}
//    }
//
//	/**********
//	 * 將傳入的 BaseCcy 的 BaseAmt 金額依最後一次的牌價轉換成 ConvertCcy 的金額
//	 * @param a_str_BaseCcy
//	 * @param a_str_ConvertCcy
//	 * @param a_d_BaseAmt
//	 * @return 一個 TopLinkedHashMap 包含以下兩個鍵值
//	 * 			FxRate：折算匯率
//	 * 			Amount：折算金額
//	 * @throws TopException
//	 */
//	public static TopLinkedHashMap exchangeByBoardFxRate(
//			String a_str_BaseCcy,
//			String a_str_ConvertCcy,
//			double a_d_BaseAmt) throws TopException{
//				
//		try{
//			double d_FxRate;
//			
//			BoardFxRate obj_BoardFxRate = new BoardFxRate();
//			d_FxRate = obj_BoardFxRate.getCrossRate(a_str_BaseCcy, a_str_ConvertCcy);
//
//			double d_ExchangeAmt = AmountUtil.calculateAmount(a_str_BaseCcy, a_str_ConvertCcy,
//															   a_d_BaseAmt, d_FxRate);
//			
//			TopLinkedHashMap lhm_ReturnData = new TopLinkedHashMap();
//			lhm_ReturnData.put("FxRate", String.valueOf(d_FxRate));
//			lhm_ReturnData.put("Amount", String.valueOf(d_ExchangeAmt));
//			
//			return lhm_ReturnData;
//		}
//		catch (TopException exc){
//			Message.debug(exc);
//			throw exc;
//		}
//		catch (Exception exc){
//			Message.debug(exc);
//			throw new TopException("00001", "AmountUtil.exchangeByBoardFxRate");
//		}
//	}    
//
//	/**********
//	 * 將傳入的 BaseCcy 的 BaseAmt 金額依最後一次的市價轉換成 ConvertCcy 的金額
//	 * @param a_str_BaseCcy
//	 * @param a_str_ConvertCcy
//	 * @param a_d_BaseAmt
//	 * @return 一個 TopLinkedHashMap 包含以下兩個鍵值
//	 * 			FxRate：折算匯率
//	 * 			Amount：折算金額
//	 * @throws TopException
//	 */
//	public static TopLinkedHashMap exchangeByMarketFxRate(
//			String a_str_BaseCcy,
//			String a_str_ConvertCcy,
//			String a_str_DayNo,
//			double a_d_BaseAmt) throws TopException{
//				
//		try{
//			double d_FxRate;
//			
//			MarketFxRate obj_MarketFxRate = new MarketFxRate();
//			d_FxRate = obj_MarketFxRate.getCrossRate(a_str_BaseCcy, a_str_ConvertCcy, a_str_DayNo);
//			
//			double d_ExchangeAmt = AmountUtil.calculateAmount(a_str_BaseCcy, a_str_ConvertCcy,
//															   a_d_BaseAmt, d_FxRate);
//			
//			TopLinkedHashMap lhm_ReturnData = new TopLinkedHashMap();
//			lhm_ReturnData.put("FxRate", String.valueOf(d_FxRate));
//			lhm_ReturnData.put("Amount", String.valueOf(d_ExchangeAmt));
//			
//			return lhm_ReturnData;
//		}
//		catch (TopException exc){
//			Message.debug(exc);
//			throw exc;
//		}
//		catch (Exception exc){
//			Message.debug(exc);
//			throw new TopException("00001", "AmountUtil.exchangeByMarketFxRate");
//		}
//	}    
	    
    public static double formateAmt(int a_i_Digital, 
    								  String a_str_CarryType, 
    								  double a_d_UnformatedAmt) throws TopException
    {
        double d_formatedAmt = 0;
        
        try
        {
            //四捨五入            
            if(a_str_CarryType.equals("1"))
            {
                if(a_i_Digital != 0)
                {
                    double d_tmep = Math.pow(10, a_i_Digital);
                    d_formatedAmt = Math.round(a_d_UnformatedAmt * d_tmep) / d_tmep;
                }
                else
                {
                    d_formatedAmt = Math.round(a_d_UnformatedAmt);
                }
            }

			/**
			 * Modified by Steven, 2004/11/12
			 * 須判斷傳入數值的正負號,來取用不同的Math類別方法
			 */                      
            //無條件進位
            else if(a_str_CarryType.equals("2"))
            {
                if(a_i_Digital != 0)
                {
                    double d_tmep = Math.pow(10, a_i_Digital);
                    
					if (a_d_UnformatedAmt >= 0) 
                        d_formatedAmt = Math.ceil(a_d_UnformatedAmt * d_tmep) / d_tmep;
                    else
					    d_formatedAmt = Math.floor(a_d_UnformatedAmt * d_tmep) / d_tmep;
                }
                else
                {
					if (a_d_UnformatedAmt >= 0) 
                        d_formatedAmt = Math.ceil(a_d_UnformatedAmt);
                    else
					    d_formatedAmt = Math.floor(a_d_UnformatedAmt);
                }
            }
            
			/**
			 * Modified by Steven, 2004/11/12
			 * 須判斷傳入數值的正負號,來取用不同的Math類別方法
			 */                   
            //無條件捨去
            else if(a_str_CarryType.equals("3"))
            {
                if(a_i_Digital != 0)
                {
                    double d_tmep = Math.pow(10, a_i_Digital);
					
					if (a_d_UnformatedAmt >= 0)                    
                        d_formatedAmt = Math.floor(a_d_UnformatedAmt * d_tmep) / d_tmep;
                    else
					    d_formatedAmt = Math.ceil(a_d_UnformatedAmt * d_tmep) / d_tmep;
                }
                else
                {         	
                	if (a_d_UnformatedAmt >= 0)
					    d_formatedAmt = Math.floor(a_d_UnformatedAmt);
                	else
                        d_formatedAmt = Math.ceil(a_d_UnformatedAmt);
                }              
            }
        }
        catch (Exception exc)
        {
            log.error("",exc);
            throw new TopException("00001", "AmountUtil.formatedAmt");
        }      
        return d_formatedAmt;
    }
    
//    /**********
//     * 計算每期應還款本金金額
//     * @param a_str_StartDate：還款區間起日
//     * @param a_str_EndDate  ：還款區間迄日
//     * @param a_str_Frequency：還款週期
//     * @param a_d_Principal  ：本金金額
//     * @return：每期應還款金額
//     * @throws TopException
//     */
//    public double calculateRepayAmt(
//    		String a_str_StartDate, 
//    		String a_str_EndDate,
//    		String a_str_Frequency,
//    		double a_d_Principal,
//    		String a_str_Currency) throws TopException
//    {
//    	try
//    	{
//    		double d_RepayAmt = 0;
//    		
//    		int i_TotalDays = DateUtil.caculateDaysBetweenTwoDate(a_str_StartDate, a_str_EndDate);
//			String str_Frequency = a_str_Frequency;
//			int i_Interations;
//            
//			while (str_Frequency.charAt(0) == '0')
//				str_Frequency = str_Frequency.substring(1);
//             
//			// Modify By Rebecca 20040107
//			// 唐雪梅表示在輸入還款區間時，
//			// 所輸入的起日為第一次還款日，所輸入的迄日為最後一次還款日
//			// 所以必須將所算出的區間再加 1，才是確實的還款次數
//			
//			// 判斷結息週期種類
//			if (str_Frequency.equals("1D") || str_Frequency.equals("D"))
//			{
//				//每日還款
//				d_RepayAmt = AmountUtil.getFormatedAmt(a_str_Currency, a_d_Principal / (i_TotalDays + 1));
//			}
//			else if (str_Frequency.equals("1W") || str_Frequency.equals("W"))
//			{
//				// 每週還款
//				i_Interations = i_TotalDays / 7 + 1;
//				d_RepayAmt = AmountUtil.getFormatedAmt(a_str_Currency, a_d_Principal / i_Interations);
//			}
//			else if (str_Frequency.equals("10D"))
//			{
//				// 每旬還款
//				i_Interations = i_TotalDays / 10 + 1;
//				d_RepayAmt = AmountUtil.getFormatedAmt(a_str_Currency, a_d_Principal / i_Interations);
//			}
//			else if (str_Frequency.equals("15D"))
//			{
//				// 每半月還款
//				i_Interations = i_TotalDays / 15 + 1;
//				d_RepayAmt = AmountUtil.getFormatedAmt(a_str_Currency, a_d_Principal / i_Interations);				
//			}
//			else if (str_Frequency.equals("1M") || str_Frequency.equals("M") || str_Frequency.equals("30D"))
//			{
//				// 每月還款
//				i_Interations = i_TotalDays / 30 + 1;
//				d_RepayAmt = AmountUtil.getFormatedAmt(a_str_Currency, a_d_Principal / i_Interations);				
//			}
//			else if (str_Frequency.equals("3M") || str_Frequency.equals("90D"))
//			{
//				// 每季還款
//				i_Interations = i_TotalDays / 90 + 1;
//				d_RepayAmt = AmountUtil.getFormatedAmt(a_str_Currency, a_d_Principal / i_Interations);				
//			}
//			else if (str_Frequency.equals("120D"))
//			{
//				// 每四個月還款
//				i_Interations = i_TotalDays / 120 + 1;
//				d_RepayAmt = AmountUtil.getFormatedAmt(a_str_Currency, a_d_Principal / i_Interations);								
//			}
//			else if (str_Frequency.equals("6M") || str_Frequency.equals("180D"))
//			{
//				// 每半年還款
//				i_Interations = i_TotalDays / 182 + 1;
//				d_RepayAmt = AmountUtil.getFormatedAmt(a_str_Currency, a_d_Principal / i_Interations);				
//			}
//			else if (str_Frequency.equals("1Y") || str_Frequency.equals("Y") || str_Frequency.equals("360D"))
//			{
//				// 每年還款
//				i_Interations = i_TotalDays / 365 + 1;
//				d_RepayAmt = AmountUtil.getFormatedAmt(a_str_Currency, a_d_Principal / i_Interations);				
//			}
//			else
//			{
//				// 目前只處理以上幾種還款週期的資料
//				throw new TopException("00006", "AmountUtil.calculateRepayAmt/Frequency = " + a_str_Frequency);
//			}    		
//    		
//    		return d_RepayAmt;
//    	}
//    	catch (Exception exc)
//    	{
//    		Message.debug(exc);
//    		throw new TopException("00001", "AmountUtil.calculateRepayAmt");
//    	}
//    }
//    
//    /**
//     * 说明:依据计息方式取得该计息方式全年的天数
//     * @param a_str_IntCalculateType
//     * @param a_str_BizDate
//     * @return
//     */
//	public static int calIntCalculateTypeDays(String a_str_IntCalculateType,String a_str_BizDate)
//	{
//		int i_YearDays = 0;
//		String str_Year = a_str_BizDate.substring(0,4);
//		char c_IntBaseType = a_str_IntCalculateType.charAt(0);
//		switch (c_IntBaseType)
//		{
//			case '1':
//			case '4':
//			case '7':
//				i_YearDays = 360;
//				break;
//			case '2':
//			case '5':
//			case '8':
//				i_YearDays = 365;
//				break;
//			case '3':
//			case '6':
//				Calendar obj_cal = Calendar.getInstance();
//				obj_cal.set(Integer.parseInt(str_Year), 12, 31);
//				i_YearDays = obj_cal.get(Calendar.DAY_OF_YEAR);    
//				break;
//		}
//		return i_YearDays;
//	}
//    // 檢核金額的小數位數長度是否符合幣別內所設定的位數
//    public static boolean checkAmt(String a_str_Currency, double a_d_Amt) throws TopException
//    {
//		try
//		{
//			// 取得幣別的小數位數
//			TopLinkedHashMap lhm_CurrData = getCurrInfo(a_str_Currency);			
//            int i_Digital = getCurrDigital(lhm_CurrData);    			
//			
//			// 將傳入的金額格式化為 #0.兩位小數的字串
//			DecimalFormat obj_DecimalFormat = new DecimalFormat("#0.00");			
//			String str_Amt = obj_DecimalFormat.format(a_d_Amt);
//			
//			// 依小數位數判斷小數點後的字串是否合理
//			String str_DigitalValue;
//			switch (i_Digital)
//			{
//				// 不允許有小數
//				case 0:
//					// 取小數點後的兩個長度字串
//					str_DigitalValue = str_Amt.substring(str_Amt.length() - 2);
//					if (str_DigitalValue.equals("00"))
//						return true;
//					else
//						return false;
//				// 只允許一位小數
//				case 1:
//					// 取格式化字串的最後一個值判斷是否為 '0'
//					str_DigitalValue = str_Amt.substring(str_Amt.length() - 1);
//					if (str_DigitalValue.equals("0"))
//						return true;
//					else
//						return false;
//				default:
//					return true;
//			}
//		}
//		catch (Exception exc)
//		{
//			Message.debug(exc);
//			throw new TopException("00001", "AmoutUtil.checkAmt");
//		}
//    }
//    
//    // 取得幣別的作帳位數
//    public static int getDigital(String a_str_Currency) throws TopException {
//    	try {
//			TopLinkedHashMap lhm_RecordData = getCurrInfo(a_str_Currency);
//			
//			return getCurrDigital(lhm_RecordData);
//    	}
//    	catch (TopException exc){
//    		Message.debug(exc);
//    		throw exc;
//    	}
//    	catch (Exception exc) {
//    		Message.debug(exc);
//    		throw new TopException("00001", "AmountUtil.getDigital");
//    	}		
//    }

	public static String deleteComma(String a_str_String) throws TopException
	{
		int			i;
		StringBuffer	sb = new StringBuffer();
		String			s = a_str_String;
		
		try
		{
			if (s == null)
				return null;

			// 去掉撇節符號。例如：123,456.00 -> 123456.00
			for (i = s.indexOf(","); i > 0;)
			{
				sb.append(s.substring(0, i));
				s = s.substring(i + 1);
				i = s.indexOf(",");
			}
			sb.append(s);

			return sb.toString();
		}
        catch (Exception exc) 
        {
//        	exc.printStackTrace(System.out);
            throw new TopException("00001","AmountUtil.deleteComma() failed : " + a_str_String);
        }

	}	
	
//	private static TopLinkedHashMap getCurrInfo(
//			String a_str_Currency) throws TopException
//	{
//		try
//		{
//			if (!p_lhm_CurrCollection.containsKey(a_str_Currency))
//				initCurrency();
//			
//			if (!p_lhm_CurrCollection.containsKey(a_str_Currency))
//				throw new TopException("90042", "AmountUtil.getCurrInfo/Currency = " + a_str_Currency);
//			
//			TopLinkedHashMap lhm_CurrData = (TopLinkedHashMap) p_lhm_CurrCollection.get(a_str_Currency);
//
//			return lhm_CurrData;
//		}
//		catch (TopException exc)
//		{
//			Message.debug(exc);
//			throw exc;
//		}
//		catch (Exception exc)
//		{
//			Message.debug(exc);
//			throw new TopException("00001", "AmountUtil.getCurrInfo");
//		}
//	}
//	
//	private static String getCurrCarryType(
//			TopLinkedHashMap a_lhm_CurrData) throws TopException
//	{
//		try
//		{
//			String str_CarryType = (String) a_lhm_CurrData.get("CarryType");
//			return str_CarryType;		
//		}
//		catch (Exception exc)
//		{
//			Message.debug(exc);
//			throw new TopException("00001", "AmountUtil.getCurrCarryType");
//		}
//	}
//
//	private static int getCurrDigital(
//			TopLinkedHashMap a_lhm_CurrData) throws TopException
//	{
//		try
//		{
//			int i_Digital = NumericUtil.transfStringToInteger((String) a_lhm_CurrData.get("Digital"));
//			return i_Digital;		
//		}
//		catch (Exception exc)
//		{
//			Message.debug(exc);
//			throw new TopException("00001", "AmountUtil.getCurrDigital");
//		}
//	}		
//
//	private static String getCurrIntBaseType(
//			TopLinkedHashMap a_lhm_CurrData) throws TopException
//	{
//		try
//		{
//			String str_IntBaseType = (String) a_lhm_CurrData.get("IntBaseType");
//			return str_IntBaseType;
//		}
//		catch (Exception exc)
//		{
//			Message.debug(exc);
//			throw new TopException("00001", "AmountUtil.getCurrIntBaseType");
//		}
//	}
//	
//   /**
//	* 把double 變成有千分號的String
//	* eg: 123456789 --> 123,456,789.00
//	* 12-Apr-04 17:58:52
//	* @param a_d_number
//	* @param a_i_fractionLength
//	*/
//   public static String insertComma(double a_d_number, int a_i_fractionLength)
//	   throws TopException {
//	   String str_number= NumericUtil.formatDouble(a_d_number, a_i_fractionLength);
//	   StringBuffer obj_return= new StringBuffer(str_number);
//	   int i_IntegerLength= str_number.indexOf(".");
//	   while (i_IntegerLength > 3) {
//		   i_IntegerLength -= 3;
//		   obj_return.insert(i_IntegerLength, ',');
//	   }
//	   return obj_return.toString();
//   }
//
//   /**
//	* 把數值String變成有千分號的String
//	* eg: 123456789 --> 123,456,789.00
//	* 12-Apr-04 17:58:52
//	* @param a_str_number
//	* @param a_i_fractionLength
//	*/
//   public static String insertComma(String a_str_number, int a_i_fractionLength)
//	   throws TopException {
//	   String str_number=
//		   NumericUtil.formatDouble(
//			   NumericUtil.transfStringToDouble(a_str_number),
//			   a_i_fractionLength);
//	   StringBuffer obj_return= new StringBuffer(str_number);
//	   int i_IntegerLength= str_number.indexOf(".");
//	   while (i_IntegerLength > 3) {
//		   i_IntegerLength -= 3;
//		   obj_return.insert(i_IntegerLength, ',');
//	   }
//	   return obj_return.toString();
//   }
}
