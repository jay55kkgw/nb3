package topibs.utility;

import java.math.BigDecimal;
import java.text.DecimalFormat;

import com.netbank.util.ESAPIUtil;

import fstop.util.exception.TopException;
import lombok.extern.slf4j.Slf4j;
@Slf4j
public class NumericUtil 
{
	

	/**
	 * 數字字串加上千分位及希望的小數位數
	 * 
	 * @param text
	 *            輸入字串
	 * @param digital
	 *            小數幾位 若電文回傳為三位數,請至少輸入3以上以防誤差
	 * @return
	 */
	public static String fmtAmount(String text, int digits) {
		// 若text為null轉成0
		DecimalFormat df = null;
		BigDecimal number = null;
		try {
			text = text==null ? "0" : text;
			df = null;
			String zeros = "";
			// 若text有帶","先移掉
			if (text.contains(",")) {
				text = text.replaceAll(",", "");
			}
			// 景毅有遇到數字尾帶負號的，多個判斷式
			if (text.substring(text.length() - 1).equals("-")) {
				text = text.substring(0, text.length() - 1);
			}
			// 遇到數字尾帶正號的，多個判斷式
			if (text.substring(text.length() - 1).equals("+")) {
				text = text.substring(0, text.length() - 1);
			}
						
			if (digits != 0) {
				if (text.indexOf(".") > 0) {

					for (int i = 0; i < digits; i++) {
						zeros += "0";
					}
					df = new DecimalFormat("###,##0." + zeros);
				} else {
					for (int i = 0; i < digits; i++) {
						zeros += "0";
					}
					df = new DecimalFormat("###,##0." + zeros);
				}
			} else {
				df = new DecimalFormat("###,##0");
			}

			number = new BigDecimal(text);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.trace(ESAPIUtil.vaildLog("wrong text >> " + text)); 
		}
		return df.format(number);
	}
	
	public static String convertNumericToSWIFTString(double a_d_Numeric) throws TopException
	{
	     String str_Numeric = "" ;
	     String str_dummy = "" ;
	     String str_dummy1 = "" ;
	     String str_rtnString = "" ;
	     int i_signPosition ;
	     int i_strLength ;
	     
	     try 
	     {		       
		    // Modified by Steven, 2004/04/29    
  			DecimalFormat obj_DecimalFormat = new DecimalFormat("#0.00######");
			str_Numeric = obj_DecimalFormat.format(a_d_Numeric);
	        
			str_Numeric = str_Numeric.replace('.',','); 
			 
	        return str_Numeric ; 
             	 
	    }
        catch (Exception exc) 
        {
        	log.error("",exc);
            throw new TopException("00001","NumericUtil.convertNumericToSWIFTString(" + a_d_Numeric + ") failed");
       }
	}		

//	/*********
//	 * 將傳入的金額轉換成 SWIFT 的數字格式，並依幣別的小數位數來產生小數位數長度
//	 * @param a_d_Amount
//	 * @param a_str_Currency
//	 * @return
//	 * @throws TopException
//	 */
//	public static String convertAmountToSWIFTString(
//			double a_d_Amount, String a_str_Currency) throws TopException {
//	     
//		 try {		        
//		 	int i_Digital = AmountUtil.getDigital(a_str_Currency);
//		 	
//		 	String str_Format = "#0.";
//		 	for (int i_Countter = 0; i_Countter < i_Digital; i_Countter ++)
//		 		str_Format = str_Format + "0";
//		 		
//			DecimalFormat obj_DecimalFormat = new DecimalFormat(str_Format);
//			String str_Amount = obj_DecimalFormat.format(a_d_Amount);
//	        
//			str_Amount = str_Amount.replace('.',','); 
//			 
//			return str_Amount ; 
//             	 
//		}
//		catch (Exception exc) 
//		{
//			log.error("",exc);
//			throw new TopException("00001","NumericUtil.convertAmountToSWIFTString(" + a_d_Amount + ") failed");
//	   }
//	}			
	
	public static double transfStringToDouble(String a_str_String) throws TopException
	{
		double			d_result;
		
		try
		{
			if (a_str_String == null)
				return 0;

			a_str_String.trim();
			if (a_str_String.equals(""))
				return 0;

			// 去掉撇節符號。例如：123,456.00 -> 123456.00
			// 若不去掉，則 Double.parseDouble("123,456.00") 會發生 java.lang.NumberFormatException: 123,456.00
			String	s = AmountUtil.deleteComma(a_str_String);

			d_result = Double.parseDouble(s);
		}
        catch (Exception exc) 
        {
        	log.error("",exc);
            throw new TopException("00001","NumericUtil.transfStringToDouble(" + a_str_String + ") failed");
        }

		return d_result;
	}		
	
	public static int transfStringToInteger(String a_str_String) throws TopException
	{
		int		i_result;
		
		try
		{
			if (a_str_String == null)
				return 0;

			a_str_String.trim();
			if (a_str_String.equals(""))
				return 0;

			// 去掉撇節符號。例如：123,456 -> 123456
			// 若不去掉，則 Integer.parseInt("123,456") 會發生 java.lang.NumberFormatException: 123,456
			String	s = AmountUtil.deleteComma(a_str_String);

			i_result = Integer.parseInt(s);
		}
        catch (Exception exc) 
        {
        	log.error("",exc);
            throw new TopException("00001","NumericUtil.transfStringToInteger(" + a_str_String + ") failed");
        }

		return i_result;
	}		
	
	public static double formatDouble(double a_d_UnFormated,
								String a_str_Format) throws TopException
	{
		try
		{
			a_str_Format.trim();
			DecimalFormat obj_DecimalFormat = new DecimalFormat(a_str_Format);
			String str_Formated = obj_DecimalFormat.format(a_d_UnFormated);
			double d_Formated = Double.parseDouble(str_Formated);
			return d_Formated;
		}
		catch (Exception exc)
		{
			log.error("",exc);
			throw new TopException("00001", "NumericUtil.formatDouble(" + a_d_UnFormated + "," + a_str_Format + ") failed");
		}
	}

	public static String formatDouble(double a_d_UnFormated,
										String a_str_Format,
										int a_i_StrLength) throws TopException
	{
		try
		{
			a_str_Format.trim();
			DecimalFormat obj_DecimalFormat = new DecimalFormat(a_str_Format);
			String str_Formated = obj_DecimalFormat.format(a_d_UnFormated);
			for (int i_StrLength = str_Formated.length(); i_StrLength < a_i_StrLength; i_StrLength ++)
				str_Formated = " " + str_Formated;
			return str_Formated;
		}
		catch (Exception exc)
		{
			log.error("",exc);
			throw new TopException("00001", "NumericUtil.formatDouble(" + a_d_UnFormated + "," + a_str_Format + "," + a_i_StrLength + ") failed");
		}
	}
	
	public static String formatDouble(double a_d_UnFormated,
								int a_i_fractionLen) throws TopException
	{
		try
		{
			
			String str_format = "";
			for (int i =0 ; i< a_i_fractionLen ; i++)
			{
			  str_format += "0";
			}

			if (a_i_fractionLen==0)
			{
			  str_format = "#0";
			} else {
			  str_format = "#0." + str_format;
			}
			DecimalFormat obj_DecimalFormat = new DecimalFormat(str_format);
			String str_Formated = obj_DecimalFormat.format(a_d_UnFormated);
			return str_Formated;
		}
		catch (Exception exc)
		{
			log.error("",exc);
			throw new TopException("00001", "NumericUtil.formatDouble(" + a_d_UnFormated + "," + a_i_fractionLen + ") failed");
		}
	}

	public static String transferIntegerToString(int a_i_Integer, 
												   int a_i_StrLength, 
												   char a_c_Prefix) throws TopException
	{
		try
		{
			String str_TransferFromInteger = String.valueOf(a_i_Integer);		
			for (int i_Length = str_TransferFromInteger.length();
				 i_Length < a_i_StrLength; i_Length ++)
				str_TransferFromInteger = a_c_Prefix + str_TransferFromInteger;
			
			return str_TransferFromInteger;
		}
		catch (Exception exc)
		{
			log.error("",exc);
			throw new TopException("00001", "NumericUtil.transferIntegerToString");
		}		
	}


/**
	 * Added by Richard Yang on 2004-01-09
	 * 参数a_str_expr表示待格式化的数值字符串
	 * 参数a_i_decimal表示格式化后的数值字符串中小数点后面的位数
	 * Modified by Richard Yang on 2004-01-11
	 */	
	public static String formatNumberString(String a_str_expr, int a_i_decimal)
	{
	     String str_nocomma = "";
	     String str_intpart = "";
	     String str_decpart = "";
	     String str_result = "";
	     StringBuffer str_buffer = new StringBuffer();	     
	     double d_amt = 0;
	     double d_temp = 0;	      	
	     boolean b_negative = false;
	     
	  try 
	  {         	     
	     if(a_str_expr.equals(""))
	     {
	         return "";
	     }
	     else
	     {	
	         try {
	             str_nocomma = AmountUtil.deleteComma(a_str_expr);
	              
	     	     //判斷正負號
	     	     if (str_nocomma.indexOf("+") != -1)
	     	     {
	     	    	 int sign = str_nocomma.indexOf("+");
	     	    	 
	     	    	 if (sign == 0)
	     	    		str_nocomma = str_nocomma.substring(1);
	     	    	 else
	     	    		str_nocomma = str_nocomma.substring(0, str_nocomma.length()-1);
	     	     }
	     	     else if (str_nocomma.indexOf("-") != -1)
	     	     {
	     	    	 int sign = str_nocomma.indexOf("-");
	     	    	 
	     	    	 if (sign == 0)
	     	    		str_nocomma = str_nocomma.substring(1);
	     	    	 else 	     	  
	     	    		str_nocomma = str_nocomma.substring(0, str_nocomma.length()-1); 		
	     	    	 
	  	    		 b_negative = true;
	     	     }	     
	     	  
	         }catch(Exception e)
	         {
	        	 log.error("formatNumberString error>>{}", e);
	         }
	     }	       
	    
	     d_amt = Double.parseDouble(str_nocomma);	     
	     
	     if (a_i_decimal != 0 && str_nocomma.indexOf(".") == -1)
	     {          
	         d_temp = Math.pow(10, a_i_decimal);
	         //d_amt = Math.round( d_amt * d_temp ) / d_temp;  
	         
	         d_amt /= d_temp;
	     }
	     else if (a_i_decimal != 0 && str_nocomma.indexOf(".") != -1)
	     {
	     	 //do nothing        
	     }	   
	     else
	     {
	    	 d_amt = Math.round( d_amt );		    	 
	     }
	 
	     java.math.BigDecimal big = new java.math.BigDecimal(d_amt);
	     java.math.BigDecimal one = new java.math.BigDecimal(1.0);	     
	     str_nocomma = ( big.divide(one, a_i_decimal, java.math.BigDecimal.ROUND_HALF_UP) ).toString();	      
	     	
	     if(str_nocomma.indexOf(".") != -1){
	         str_intpart = str_nocomma.substring(0, str_nocomma.indexOf("."));
	         str_decpart = str_nocomma.substring(str_nocomma.indexOf(".") + 1);	         	        
	     }else
	     {
	         str_intpart = str_nocomma;
	         
	         if(a_i_decimal !=0){
	             str_decpart = new Double( Math.pow(10, (-1) * a_i_decimal)).toString();
	         }else 
	         {
	             str_decpart = "";	
	         }     
	     }
	    
	     if(str_intpart.length()%3 ==0)
	     {
	       	 for(int i=0;i<(str_intpart.length()/3);i++)
	       	 {
	       		  str_buffer.append(str_intpart.substring(i*3, i*3 + 3));
	       		  str_buffer.append(",");
	       	 }
	        	
	       	 str_result = (str_buffer.toString()).substring(0, str_buffer.toString().length()-1);	        	
	     }else  if(str_intpart.length()%3 ==1)
	     {
	       	  str_buffer.append(str_intpart.substring(0,1));
	          str_buffer.append(",");
	                	
	          for(int i=0;i<Math.floor((str_intpart.length()/3));i++)
	          {
	        	   str_buffer.append(str_intpart.substring(i*3+1, i*3 + 4));
	        	   str_buffer.append(",");
	          }
	        	
	          str_result = (str_buffer.toString()).substring(0, str_buffer.toString().length()-1);	        	
	     }else  if(str_intpart.length()%3 ==2)
	     {
	          str_buffer.append(str_intpart.substring(0,2));
	          str_buffer.append(",");
	                	
	          for(int i=0;i<Math.floor((str_intpart.length()/3));i++)
	          {
	        	   str_buffer.append(str_intpart.substring(i*3+2, i*3 + 5));
	        	   str_buffer.append(",");
	          }
	        	
	          str_result = (str_buffer.toString()).substring(0, str_buffer.toString().length()-1);	        	
	     }        
        
         if(a_i_decimal !=0)
         {
             str_result = str_result + "." + str_decpart;
         }else
         {
             str_result = str_result + str_decpart;	
         }           
	    
	     if(b_negative)
	     {
	     	str_result = "-" + str_result;  
	     }	     	
	  }  
	  catch (Exception exc)
	  {
		 str_result = "數值格式錯誤:" + a_str_expr;		   
	  }
	   
	   return str_result;		
	}	

	public static String formatNumberStringByCcy(String a_str_expr, int a_i_decimal, String a_str_ccy)
	{
	     String str_nocomma = "";
	     String str_intpart = "";
	     String str_decpart = "";
	     String str_result = "";
	     StringBuffer str_buffer = new StringBuffer();	     
	     double d_amt = 0;
	     double d_temp = 0;	      	
	     boolean b_negative = false;
	     
	  try 
	  {         	     
	     if(a_str_expr.equals(""))
	     {
	         return "";
	     }
	     else
	     {	
	         try {
	        	 
	        	 if (a_str_ccy.equals("TWD") || a_str_ccy.equals("JPY")) {
	        		 a_i_decimal = 0;
	        	 }
	        	 
	             str_nocomma = AmountUtil.deleteComma(a_str_expr);
	              
	     	     //判斷正負號
	     	     if (str_nocomma.indexOf("+") != -1)
	     	     {
	     	    	 int sign = str_nocomma.indexOf("+");
	     	    	 
	     	    	 if (sign == 0)
	     	    		str_nocomma = str_nocomma.substring(1);
	     	    	 else
	     	    		str_nocomma = str_nocomma.substring(0, str_nocomma.length()-1);
	     	     }
	     	     else if (str_nocomma.indexOf("-") != -1)
	     	     {
	     	    	 int sign = str_nocomma.indexOf("-");
	     	    	 
	     	    	 if (sign == 0)
	     	    		str_nocomma = str_nocomma.substring(1);
	     	    	 else 	     	  
	     	    		str_nocomma = str_nocomma.substring(0, str_nocomma.length()-1); 		
	     	    	 
	  	    		 b_negative = true;
	     	     }	     
	     	  
	         }catch(Exception e)
	         {
	        	 log.error("formatNumberStringByCcy error>>{}", e);
	         }
	     }	       
	    
	     d_amt = Double.parseDouble(str_nocomma);	     
	     
	     if (a_i_decimal != 0 && str_nocomma.indexOf(".") == -1)
	     {          
	         d_temp = Math.pow(10, a_i_decimal);
	         //d_amt = Math.round( d_amt * d_temp ) / d_temp;  
	         
	         d_amt /= d_temp;
	     }
	     else if (a_i_decimal != 0 && str_nocomma.indexOf(".") != -1)
	     {
	     	 //do nothing        
	     }	   
	     else
	     {
	    	 d_amt = Math.round( d_amt );		    	 
	     }
	 
	     java.math.BigDecimal big = new java.math.BigDecimal(d_amt);
	     java.math.BigDecimal one = new java.math.BigDecimal(1.0);	     
	     str_nocomma = ( big.divide(one, a_i_decimal, java.math.BigDecimal.ROUND_HALF_UP) ).toString();	      
	     	
	     if(str_nocomma.indexOf(".") != -1){
	         str_intpart = str_nocomma.substring(0, str_nocomma.indexOf("."));
	         str_decpart = str_nocomma.substring(str_nocomma.indexOf(".") + 1);	         	        
	     }else
	     {
	         str_intpart = str_nocomma;
	         
	         if(a_i_decimal !=0){
	             str_decpart = new Double( Math.pow(10, (-1) * a_i_decimal)).toString();
	         }else 
	         {
	             str_decpart = "";	
	         }     
	     }
	    
	     if(str_intpart.length()%3 ==0)
	     {
	       	 for(int i=0;i<(str_intpart.length()/3);i++)
	       	 {
	       		  str_buffer.append(str_intpart.substring(i*3, i*3 + 3));
	       		  str_buffer.append(",");
	       	 }
	        	
	       	 str_result = (str_buffer.toString()).substring(0, str_buffer.toString().length()-1);	        	
	     }else  if(str_intpart.length()%3 ==1)
	     {
	       	  str_buffer.append(str_intpart.substring(0,1));
	          str_buffer.append(",");
	                	
	          for(int i=0;i<Math.floor((str_intpart.length()/3));i++)
	          {
	        	   str_buffer.append(str_intpart.substring(i*3+1, i*3 + 4));
	        	   str_buffer.append(",");
	          }
	        	
	          str_result = (str_buffer.toString()).substring(0, str_buffer.toString().length()-1);	        	
	     }else  if(str_intpart.length()%3 ==2)
	     {
	          str_buffer.append(str_intpart.substring(0,2));
	          str_buffer.append(",");
	                	
	          for(int i=0;i<Math.floor((str_intpart.length()/3));i++)
	          {
	        	   str_buffer.append(str_intpart.substring(i*3+2, i*3 + 5));
	        	   str_buffer.append(",");
	          }
	        	
	          str_result = (str_buffer.toString()).substring(0, str_buffer.toString().length()-1);	        	
	     }        
        
         if(a_i_decimal !=0)
         {
             str_result = str_result + "." + str_decpart;
         }else
         {
             str_result = str_result + str_decpart;	
         }           
	    
	     if(b_negative)
	     {
	     	str_result = "-" + str_result;  
	     }	     	
	  }  
	  catch (Exception exc)
	  {
		 str_result = "數值格式錯誤:" + a_str_expr;		   
	  }
	   
	   return str_result;		
	}	
	
	/**
	 * Added by jack_lin on 2004-07-03
	 * 参数a_str_String要處理的字串
	 * 参数a_str_oldChar要被取代的舊字串
	 * a_str_NewChara_str_NewChar要取代的新字串
	 */	
	public static String replaceString(String a_str_String, String a_str_oldChar, String a_str_NewChar) throws TopException
	{
		int			i;
		StringBuffer	sb = new StringBuffer();
		String			s = a_str_String;
		
		try
		{
			if (s == null)
				return null;

			for (i = s.indexOf(a_str_oldChar); i > 0;)
			{
				sb.append(s.substring(0, i) + a_str_NewChar);
				s = s.substring(i + 1);
				i = s.indexOf(a_str_oldChar);
			}
			sb.append(s);

			return sb.toString();
		}
		catch (Exception exc) 
		{
//			exc.printStackTrace(System.out);
			throw new TopException("00001","NumericUtil.replaceString() failed : " + a_str_String);
		}

	}	
}
