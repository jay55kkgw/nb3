/*
* 檔案名：DateUtil
* 文件描述：
* 修改人：
* 修改時間：2003年5月21日, 上午 10:32
* 修改單號：
* 修改日期：2004/03/17 Modify By Rebecca
* 	依幣別取得該幣別適用的假日城市代碼方式變更，
* 	原來是依幣別取得該幣別資料內適用國家，再用適用國家取得該國家首都
* 	經清算中心提出修改，異動為
* 	依幣別取得城市資料內 "假日對應清算幣別" 為此幣別的城市代碼
*/

package topibs.utility;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
//import topibs.bo.*;

import fstop.util.DateTimeUtils;
import fstop.util.exception.TopException;
import lombok.extern.slf4j.Slf4j;


/**
 * @類別名:
 * @功能概要:
 * @author: jack_lin
 * @version: 1.03
 */
@Slf4j
public class DateUtil {
    
    // private property
	private static final String CLASSNAME = "DateUtil.";
	private static TopLinkedHashMap p_lhm_CurrencyCityMapping = new TopLinkedHashMap();
	private static TopLinkedHashMap p_lhm_CtryCityMapping = new TopLinkedHashMap();
	  
    public static String getDate(String a_str_BaseDate, int a_i_days) throws Exception {

        String str_Year,str_Month,str_Day;
        String str_ReturnData;
        
        try {                
            Calendar obj_calendar = Calendar.getInstance();         
             
			if (a_str_BaseDate.length() == 7)
			{
				str_Year  = a_str_BaseDate.substring(0,3);        	//年
				str_Month = a_str_BaseDate.substring(3,5);     		//月
				str_Day   = a_str_BaseDate.substring(5);         	    //日    				
			}
			else
			{
				str_Year  = a_str_BaseDate.substring(0,2);        	//年
				str_Month = a_str_BaseDate.substring(2,4);     		//月
				str_Day   = a_str_BaseDate.substring(4);         	    //日    					 
			}        
            
			str_ReturnData = str_Year + str_Month + str_Day;
			
            return str_ReturnData;		//取得年月日
        }
        catch (Exception exc){
        	log.error("",exc);
        	log.error("",exc);
            throw exc;
        }
    }
            
	/*** Modify by Steven 2005/04/29 ***/
    public static String getDateAddByMonth(String a_str_BaseDate, 
    										int a_i_Months) throws TopException
	{
		int i_Year;
		int i_Month;
		int i_Day;
		int i_FinalMonth = 0;
	    StringBuffer stb_Date = new StringBuffer();
	        		
        try 
        {
            // Calendar 資料的月份自 0 開始，0 代表 1 月
            Calendar obj_Calendar = Calendar.getInstance();

            if (a_str_BaseDate.length() == 7)
            {
				i_Year  = Integer.parseInt(a_str_BaseDate.substring(0,3));  	//年
				i_Month = Integer.parseInt(a_str_BaseDate.substring(3,5)); 		//月
				i_Day   = Integer.parseInt(a_str_BaseDate.substring(5));     	//日                        	
            }
            else
            {
				i_Year  = Integer.parseInt(a_str_BaseDate.substring(0,2));  	//年
				i_Month = Integer.parseInt(a_str_BaseDate.substring(2,4)); 		//月
				i_Day   = Integer.parseInt(a_str_BaseDate.substring(4));     	//日                               	
            }
            
            // 依 a_i_Month 累加月份
			if (i_Month + a_i_Months > 12)
			{
				int i_addYear = (int)Math.floor((i_Month + a_i_Months) / 12);
				i_Year += i_addYear;
				i_FinalMonth = i_Month + a_i_Months - (12 * i_addYear) - 1;
			}
			else
				i_FinalMonth = i_Month + a_i_Months - 1;

			if (i_FinalMonth < 10)
			{
				stb_Date.append(Integer.toString(i_Year));
				stb_Date.append("0");
				stb_Date.append(Integer.toString(i_FinalMonth));				 
			}
			else
			{
				stb_Date.append(Integer.toString(i_Year));
				stb_Date.append(Integer.toString(i_FinalMonth));								
			}
			
			if (i_Day < 10)
			{
				stb_Date.append("0");
				stb_Date.append(Integer.toString(i_Day));				 
			}
			else
			{
				stb_Date.append(Integer.toString(i_Day));								
			}
            
			// 由於 Date 為自動在設定日期檢核日期是否合理，若不合理會自動累加至合理日期
			// 例如設定 20030431 會變成 20030501
			// 當這種狀況發生時，必須將日期倒回 20030430

			String str_Month, str_Day;
			String  str_Date = stb_Date.toString();
			
			if (a_str_BaseDate.length() == 7)
			{
				str_Month = str_Date.substring(3, 5);	
				str_Day = str_Date.substring(5);
			}
			else
			{
				str_Month = str_Date.substring(2, 4);	
				str_Day = str_Date.substring(4);				
			}
			
			if (Integer.parseInt(str_Month) - 1 != i_FinalMonth)
			{
				str_Date = getDate(str_Date, 
								   NumericUtil.transfStringToInteger(str_Day) * -1);
			}
			// 若是月底日則必須往後推至月底日止
			else if (isEndOfMonth(a_str_BaseDate))
			{
				while (!isEndOfMonth(str_Date))
				{
					str_Date = getDate(str_Date, 1);
				}				
			}						

            return str_Date;		//取得年月日
        }
        catch (Exception exc)
        {
        	log.error("",exc);
            throw new TopException("00001", "DateUtil.getDateAddByMonth");
        }		
	}   
    
//    public static TopLinkedHashMap getDateAdd(int a_i_field, 
//    										   int a_i_amount, 
//    										   String a_str_Date) throws Exception {
//        String str_SystemDate,str_SystemTime,str_month,str_date;
//        String str_Year,str_Month,str_Day;
//        String str_ReturnData;
//        TopLinkedHashMap lhm_ReturnData = new TopLinkedHashMap();
//        
//        try {
//                
//            Calendar obj_calendar = Calendar.getInstance();         
//             
//            str_Year = a_str_Date.substring(0,4);        //年
//            str_Month = a_str_Date.substring(4,6);     //月
//            str_Day = a_str_Date.substring(6,8);         //日            
//
//            //設定 Calendar 的日期時間             
//            obj_calendar.set( Integer.parseInt(str_Year) , Integer.parseInt(str_Month)-1, Integer.parseInt(str_Day));
//
//            // 依 amount 累加(減)日期                                         
//            obj_calendar.add(a_i_field, a_i_amount);
//
//            /* 取得日期 格式: yyyymmdd */
//            SimpleDateFormat formatter = new SimpleDateFormat();
//            formatter.applyPattern("yyyyMMdd");
//            str_ReturnData = formatter.format( obj_calendar.getTime());
//            
//            lhm_ReturnData.put("Date", str_ReturnData.substring(0,8));      //年月日
//
//            return lhm_ReturnData;
//        }
//        catch (Exception exc){
//        	log.error("",exc);
//            throw new Exception("get SystemDateTime error");
//        }
//    }

    /*  累加(減)日期                */
    public static TopLinkedHashMap getDateAdd(int a_i_field,
                                              int a_i_amount,
                                              String a_str_Date,
                                              String a_str_Time) throws Exception {
        String str_SystemDate,str_SystemTime,str_month,str_date;
        String str_Year,str_Month,str_Day;
        String str_HH,str_MM,str_SS;
        String str_ReturnData;
        TopLinkedHashMap lhm_ReturnData = new TopLinkedHashMap();
        
        try {
                
            Calendar obj_calendar = Calendar.getInstance();         
             
            str_Year = a_str_Date.substring(0,4);        //年
            str_Month = a_str_Date.substring(4,6);     //月
            str_Day = a_str_Date.substring(6,8);         //日            
            str_HH = a_str_Time.substring(0,2);         //時
            str_MM = a_str_Time.substring(2,4);        //分
            str_SS = a_str_Time.substring(4,6);           //秒

            //設定 Calendar 的日期時間             
            obj_calendar.set( Integer.parseInt(str_Year) , Integer.parseInt(str_Month)-1, Integer.parseInt(str_Day),
                                         Integer.parseInt(str_HH), Integer.parseInt(str_MM), Integer.parseInt(str_SS));

            // 依 amount 累加(減)日期                                         
            obj_calendar.add(a_i_field, a_i_amount);

            /* 取得日期 格式: yyyymmddHHmmss */
            SimpleDateFormat formatter = new SimpleDateFormat();
            formatter.applyPattern("yyyyMMddHHmmss");
            str_ReturnData = formatter.format( obj_calendar.getTime());

            lhm_ReturnData.put("Date", str_ReturnData.substring(0,8));      //年月日
            lhm_ReturnData.put("Time", str_ReturnData.substring(8,14));      //時分秒
            return lhm_ReturnData;
        }
        catch (Exception exc){
        	log.error("",exc);
            throw new Exception("get SystemDateTime error");
        }
    }


//    public static TopLinkedHashMap getDateAdd(String a_str_field, 
//    										   int a_i_amount, 
//    										   String a_str_Date) throws Exception {
//        String str_SystemDate,str_SystemTime,str_month,str_date;
//        String str_Year,str_Month,str_Day;
//        String str_ReturnData;
//        TopLinkedHashMap lhm_ReturnData = new TopLinkedHashMap();
//        int i_field = 0;
//        
//        try {
//                
//            Calendar obj_calendar = Calendar.getInstance();         
//             
//            str_Year = a_str_Date.substring(0,4);        //年
//            str_Month = a_str_Date.substring(4,6);     //月
//            str_Day = a_str_Date.substring(6,8);         //日            
//
//
//			//(19)	轉換日期單位
//			if (a_str_field.equals("Y"))
//			{
//				i_field = Calendar.YEAR;
//			} else if (a_str_field.equals("M")) {
//				i_field = Calendar.MONTH;
//			} else if (a_str_field.equals("D") || a_str_field.equals("W")) {
//				i_field = Calendar.DATE;
//			}
//			//Add by Dino 20040618 11:00 start
//			//处理星期的情况
//			//Days = a_i_amount * 7
//			if(a_str_field.equals("W"))
//			{
//				a_i_amount = a_i_amount * 7;
//			}
//			//Add by Dino 20040618 11:00 end
//			
//            //設定 Calendar 的日期時間             
//            obj_calendar.set( Integer.parseInt(str_Year) , Integer.parseInt(str_Month)-1, Integer.parseInt(str_Day));
//
//            // 依 amount 累加(減)日期                                         
//            obj_calendar.add(i_field, a_i_amount);
//
//            /* 取得日期 格式: yyyymmdd */
//            SimpleDateFormat formatter = new SimpleDateFormat();
//            formatter.applyPattern("yyyyMMdd");
//            str_ReturnData = formatter.format( obj_calendar.getTime());
//            
//            lhm_ReturnData.put("Date", str_ReturnData.substring(0,8));      //年月日
//
//            return lhm_ReturnData;
//        }
//        catch (Exception exc){
//        	log.error("",exc);
//            throw new Exception("get SystemDateTime error");
//        }
//    }
    
    public static int caculateDaysBetweenTwoDate(String a_str_BeginDate, String a_str_EndDate) throws TopException
    {
        int i_Days;
        
        try
        {        	 
        	 if (a_str_BeginDate.indexOf("/") != -1) 
        	 { 		 
  				 a_str_BeginDate = a_str_BeginDate.replaceAll("/", "");      		 
        	 }
        		 
        	 if (a_str_EndDate.indexOf("/") != -1) 
        	 {
        		 a_str_BeginDate = a_str_EndDate.replaceAll("/", "");
        	 }        		 
        		 
             Calendar obj_BeginCalendar = Calendar.getInstance();
             Calendar obj_EndCalendar = Calendar.getInstance();
             Calendar obj_TempCalendar = Calendar.getInstance();

             obj_BeginCalendar.set(Integer.parseInt(a_str_BeginDate.substring(0, 4)),
                                               Integer.parseInt(a_str_BeginDate.substring(4, 6)) - 1, 
                                               Integer.parseInt(a_str_BeginDate.substring(6, 8)));
             obj_EndCalendar.set(Integer.parseInt(a_str_EndDate.substring(0, 4)),
                                            Integer.parseInt(a_str_EndDate.substring(4, 6)) - 1,
                                            Integer.parseInt(a_str_EndDate.substring(6, 8)));
       
             int i_BeginYear = Integer.parseInt(a_str_BeginDate.substring(0, 4));
             int i_EndYear = Integer.parseInt(a_str_EndDate.substring(0, 4));
             
             if (i_BeginYear == i_EndYear)
                i_Days = obj_EndCalendar.get(Calendar.DAY_OF_YEAR) - obj_BeginCalendar.get(Calendar.DAY_OF_YEAR);
             else
             {  
                obj_TempCalendar.set(Integer.parseInt(a_str_BeginDate.substring(0, 4)), 12 - 1, 31);
                i_Days = obj_TempCalendar.get(Calendar.DAY_OF_YEAR) - obj_BeginCalendar.get(Calendar.DAY_OF_YEAR);
                for (i_BeginYear ++; i_BeginYear < i_EndYear; i_BeginYear ++)
                {  
                    obj_TempCalendar.set(i_BeginYear, 12 - 1, 31);
                    i_Days += obj_TempCalendar.get(Calendar.DAY_OF_YEAR);
                }
                i_Days += obj_EndCalendar.get(Calendar.DAY_OF_YEAR);
             }
             return i_Days;
        }
        catch (Exception exc)
        {
            log.error("",exc);
            throw new TopException("00001", "");
        }
    }
    
//    // 依傳入的城市代碼及上次結息日及結息週期取得本次結息日(此結息日必為營業日)
//    public static String caculateIntDate(String a_str_CityCode,
//    									   String a_str_StartDate, 
//    									   String a_str_Frequency) throws TopException
//    {
//    	try
//    	{
//    		String str_IntDate = caculateDateByFrequency(a_str_StartDate, 
//    								 				     a_str_Frequency);
//			
//			// 計算所得的日期必須判斷是否為假日，若是假日則順延
//			str_IntDate = getNextBizDate(a_str_CityCode, str_IntDate);
//			
//    		return str_IntDate;
//    	}
//    	catch (TopException exc)
//    	{
//    		throw exc;
//    	}
//    	catch (Exception exc)
//    	{
//    		log.error("",exc);
//    		throw new TopException("00001", "DateUtile.caculateIntDate");
//    	}	
//    }
//    
//    // 依傳入的城市代碼及上次結息日及結息週期取得本次結息日
//    public static String caculateIntDate(String a_str_StartDate,
//    									   String a_str_Frequency) throws TopException
//    {
//    	try
//    	{
//    		String str_IntDate = caculateDateByFrequency(a_str_StartDate, 
//    								 				     a_str_Frequency);
//			
//    		return str_IntDate;
//    	}
//    	catch (TopException exc)
//    	{
//    		throw exc;
//    	}
//    	catch (Exception exc)
//    	{
//    		log.error("",exc);
//    		throw new TopException("00001", "DateUtile.caculateIntDate");
//    	}	
//    }
//
//	// 取得下一個計息日(以傳入的幣別的所在國首都讀取假日設定,若為假日則取得下一個營業日期)
//	public static String calNextIntDateByCcy(String a_str_Currency,
//											  String a_str_StartDate,
//											  String a_str_Frequency) throws TopException
//	{
//		try
//		{
//			// 依傳入的上次結息日及結息週期計算下次結息日
//			//String str_IntDate = DateUtil.caculateDateByFrequency(a_str_StartDate, 
//			//													  a_str_Frequency);
//																  
//			// Modify By Rebecca 2004/03/17
//			// 取得該幣別的使用國家的首都代碼
//			// City obj_City = new City();
//			// String str_CityCode = obj_City.getCapitalByCcy(a_str_Currency);
//			String str_CityCode = getCityCode(a_str_Currency);
//			
//			String str_Unit = a_str_Frequency.substring( a_str_Frequency.length() -1);
//			String str_DateAmount = a_str_Frequency.substring(0, a_str_Frequency.length() -1);
//			
//			//(3)	this.getDateAdd(str_Unit, str_DateAmount轉成int型態, a_str_StartDate) : str_IntDate
//			String str_IntDate = (String) getDateAdd(str_Unit, Integer.parseInt(str_DateAmount), a_str_StartDate).get("Date");
//		
//			// 找到最接近計算出的結息日的第一個營業日期
//			String str_NextIntDate = DateUtil.getNextBizDate(str_CityCode, str_IntDate);	
//			
//			return str_NextIntDate;
//		}
//		catch (TopException exc)
//		{
//			log.error("",exc);
//			throw exc;
//		}
//		catch (Exception exc)
//		{
//			log.error("",exc);
//			throw new TopException("00001", "DateUtil.calNextIntDateByCcy");
//		}
//	} 
//	
//	/**
//	 * Add by Dino 20040623 16:00
//	 * 说明：计算下一个计息日
//	 * 		为解决IRS中理论起日计算理论止日和取得当月最后一个营业日
//	 * @param a_str_Currency
//	 * @param a_str_StartDate
//	 * @param a_str_Frequency
//	 * @param a_b_BizDateFlag		传回的日期是否为营业日
//	 * @param a_b_IsMonthEnd		传回的日期是否为当月的最后一个营业日
//	 * @return
//	 * @throws TopException
//	 * 
//	 * sample:
//	 * a_str_Currency = "USD"
//	 * a_str_StartDate = "20040430"
//	 * a_str_Frequency = "006M"
//	 * 
//	 * 	(1).如果a_b_IsMonthEnd == true且a_b_BizDateFlag == true时，计算的结果应为所得当月的最后一个营业日。
//	 * 		计算的日期为20041031，但由于20041031不是营业日，所以取得当月的最后一个营业日，
//	 * 		即20041029(20041030也不为营业日)。
//	 *  (2).如果a_b_IsMonthEnd == true且a_b_BizDateFlag == false时，计算的结果为所得当月的最后一天。
//	 * 		计算的日期为20041031。
//	 * 
//	 *	(3).如果a_b_IsMonthEnd == false且a_b_BizDateFlag == true时，计算的结果应为一个营业日。
//	 *		计算的日期为20041030，但由于20041030和20041031都不为营业日，结果为20041101。
//	 *
//	 *	(4).如果a_b_IsMonthEnd == false且a_b_BizDateFlag == false时,直接计算所得的日期即为所需的日期，
//	 *		不用再作营业日的判断，结果为20041030。
//	 */
//	public static String calNextIntDateByCcy(String a_str_Currency,
//												  String a_str_StartDate,
//												  String a_str_Frequency,
//												  boolean a_b_BizDateFlag,
//												  boolean a_b_IsMonthEnd) throws TopException
//	{
//		try
//		{
//			String str_CityCode = getCityCode(a_str_Currency);
//		
//			String str_Unit = a_str_Frequency.substring( a_str_Frequency.length() -1);
//			String str_DateAmount = a_str_Frequency.substring(0, a_str_Frequency.length() -1);
//		
//			//(3)	this.getDateAdd(str_Unit, str_DateAmount轉成int型態, a_str_StartDate) : str_IntDate
//			String str_IntDate = (String) getDateAdd(str_Unit, Integer.parseInt(str_DateAmount), a_str_StartDate).get("Date");
//			
//			String str_BaseDate = a_str_StartDate.substring(0,6) + "01";
//			str_BaseDate =(String) getDateAdd(str_Unit, Integer.parseInt(str_DateAmount), str_BaseDate).get("Date");
//			String str_BaseMonth = str_BaseDate.substring(4,6);
//			String str_NextIntDate = str_IntDate;
//			//取得当月的最后一个营业日
//			if(a_b_IsMonthEnd)
//			{
//				String str_Days = getMonthDays(str_NextIntDate);
//				str_NextIntDate = str_IntDate.substring(0,6) + str_Days;
//				if(a_b_BizDateFlag)
//				{
//					str_NextIntDate = DateUtil.getLastBizDate(str_CityCode, str_NextIntDate);
//				}
//			}
//			else
//			{
//				if(a_b_BizDateFlag)
//				{
//					//找到最接近計算出的結息日的第一個營業日期	
//					str_NextIntDate = DateUtil.getNextBizDate(str_CityCode, str_IntDate);
//					
//					//假如向后取得的BizDate超过了当月，则向前取BizDate
//					if(!str_BaseMonth.equals(str_NextIntDate.substring(4,6)))
//					{
//						str_NextIntDate = DateUtil.getLastBizDate(str_CityCode, str_IntDate);
//					}
//				}
//					
//			}
//			return str_NextIntDate;
//		}
//		catch (TopException exc)
//		{
//			log.error("",exc);
//			throw exc;
//		}
//		catch (Exception exc)
//		{
//			log.error("",exc);
//			throw new TopException("00001", "DateUtil.calNextIntDateByCcy");
//		}
//	}
//	
//	public static String getMonthDays(String a_str_Date)
//	{
//		
//		String str_Days = "0";
//		try
//		{
//			String str_YearMonth = a_str_Date.substring(0,6);
//			String str_StartDate = str_YearMonth + "01";
//			String str_EndDate = (String)getDateAdd("M",1,str_StartDate).get("Date");
//			
//			int i_Days = caculateDaysBetweenTwoDate(str_StartDate,str_EndDate);
//			str_Days = String.valueOf(i_Days);
//		}
//		catch(Exception ex)
//		{
//			Message.debug(ex);
//		}
//		
//		return str_Days;
//	}
//    public static String getNextBizDate(String a_str_CityCode, 
//    		  				      		 String a_str_Date) throws TopException
//    {
//    	try
//    	{
//            Holiday obj_Holiday = new Holiday();
//            String str_Date = a_str_Date;
//
//			while (true)
//			{
//				if (obj_Holiday.isHoliday(a_str_CityCode, str_Date))
//					str_Date = getDate(str_Date, 1);
//				else
//					break;
//			}
//			return str_Date;
//    	}
//    	catch (TopException exc)
//    	{
//    		log.error("",exc);
//    		throw exc;
//    	}
//    	catch (Exception exc)
//    	{
//    		log.error("",exc);
//    		throw new TopException("00001", "DateUtil.getNextBizDate");
//    	}
//    }
//
//    /**
//     * 使用時機:
//     * 以傳入的營業日,自動依據StaticCommon中設定的CityCode,
//     * 取得下一個營業日.
//     * 
//     * @param a_str_BizDate
//     * @return str_NextBizDate
//     * @throws TopException
//     */ 
//	public static String getNextBizDate(String a_str_BizDate) throws TopException
//	{
//		String str_MethodName = DateUtil.CLASSNAME + "getNextBizDate()";
//		
//		try
//		{
//			Holiday obj_Holiday = new Holiday();
//			
//			//先取得下一個營業日
//			String str_NextBizDate = getDate(a_str_BizDate, 1);
//            String str_CityCode = StaticCommon.getCityCode();
//             
//			while (true)
//			{
//				if (obj_Holiday.isHoliday(str_CityCode, str_NextBizDate))
//					str_NextBizDate = getDate(str_NextBizDate, 1);
//				else
//					break;
//			}
//			
//			return str_NextBizDate;
//		}
//		catch (TopException exc)
//		{
//			log.error("",exc);
//			throw exc;
//		}
//		catch (Exception exc)
//		{
//			log.error("",exc);
//			throw new TopException("00001", str_MethodName);
//		}
//	}
//
//	/**
//	  * 使用時機 : 依據國家與營業日期取得下一個營業日
//	  * @param a_str_CtryCode
//	  * @param a_str_Date
//	  * @return String
//	  * @throws TopException
//	  */
//	 public static String getNextBizDateByCtry(
//	 		String a_str_CtryCode, String a_str_Date ) throws TopException {
//		 String str_MethodName = DateUtil.CLASSNAME + "getNextBizDateByCtry()";
//		
//		 try {	
//			 // 取得首都代碼
//			 String str_CityCode = getCityCodeByCtryCode(a_str_CtryCode);			
//			 String str_NextBizDate = DateUtil.getNextBizDate(str_CityCode, a_str_Date);
//
//			 return str_NextBizDate;
//		 }
//		 catch (TopException exc) {
//			 log.error("",exc);
//			 throw exc;
//		 }
//		 catch (Exception exc) {
//			 log.error("",exc);
//			 throw new TopException("00001", str_MethodName);
//		 }
//	 }    
//
//    /**
//     * 使用時機 : 依據幣別與營業日期取得下一個營業日
//     * @param a_str_Currency
//     * @param a_str_Date
//     * @return String
//     * @throws TopException
//     */
//	public static String getNextBizDateByCcy( String a_str_Currency, 
//										                                     String a_str_Date ) throws TopException
//	{
//		String str_MethodName = DateUtil.CLASSNAME + "getNextBizDateByCcy()";
//		
//		try
//		{	
//			// Modify By Rebecca  2004/03/16 
//			// 取得首都代碼
//			// City obj_City = new City();
//			// String str_CityCode = obj_City.getCapitalByCcy(a_str_Currency);
//			String str_CityCode = getCityCode(a_str_Currency);
//			
//	        String str_NextBizDate = DateUtil.getNextBizDate(str_CityCode, a_str_Date);
//
//			return str_NextBizDate;
//		}
//		catch (TopException exc)
//		{
//			log.error("",exc);
//			throw exc;
//		}
//		catch (Exception exc)
//		{
//			log.error("",exc);
//			throw new TopException("00001", str_MethodName);
//		}
//	}
//
//	public static String getLastBizDate(String a_str_CityCode, 
//										 String a_str_Date) throws TopException
//	{
//		try
//		{
//			Holiday obj_Holiday = new Holiday();
//			String str_Date = a_str_Date;
//
//			while (true)
//			{
//				if (obj_Holiday.isHoliday(a_str_CityCode, str_Date))
//					str_Date = getDate(str_Date, -1);
//				else
//					break;
//			}
//			return str_Date;
//		}
//		catch (TopException exc)
//		{
//			log.error("",exc);
//			throw exc;
//		}
//		catch (Exception exc)
//		{
//			log.error("",exc);
//			throw new TopException("00001", "DateUtil.getLastBizDate");
//		}
//	}	
//
//	/**
//	 * 使用時機 : 依據幣別與營業日期取得上一個營業日
//	 * @param a_str_Currency
//	 * @param a_str_Date
//	 * @return String
//	 * @throws TopException
//	 */
//	public static String getLastBizDateByCcy(
//		String a_str_Currency, String a_str_Date ) throws TopException
//	{
//		String str_MethodName = DateUtil.CLASSNAME + "getLastBizDateByCcy()";
//		
//		try
//		{	
//			String str_CityCode = getCityCode(a_str_Currency);
//			
//			String str_LastBizDate = DateUtil.getLastBizDate(str_CityCode, a_str_Date);
//
//			return str_LastBizDate;
//		}
//		catch (TopException exc)
//		{
//			log.error("",exc);
//			throw exc;
//		}
//		catch (Exception exc)
//		{
//			log.error("",exc);
//			throw new TopException("00001", str_MethodName);
//		}
//	}	
//	
//    // 取得交易實際出帳日期
//    // (若傳入的日期不為營業日，則往後推最接近的營業日回傳，
//    // 若傳入的日期為營業日，則回傳原日期)
//    public static String getRealAccDate(String a_str_Date) throws TopException
//    {
//    	try
//    	{
//            Holiday obj_Holiday = new Holiday();
//            String str_Date = a_str_Date;
//
//			// 取得使用單位所在城市
//			String str_CityCode = StaticCommon.getCityCode();
//			
//			return getNextBizDate(str_CityCode, str_Date);
//    	}
//    	catch (TopException exc)
//    	{
//    		log.error("",exc);
//    		throw exc;
//    	}
//    	catch (Exception exc)
//    	{
//    		log.error("",exc);
//    		throw new TopException("00001", "DateUtil.getNextBizDate");
//    	}
//    } 
//    
//    // 計算傳入日期是一星期的第幾天，並回傳
//    // 星期一到星期日的回傳值分別為 1～7
//    public static int getDateWeek(
//    	String a_str_Date) throws TopException
//    {
//    	try
//    	{
//	        Calendar obj_Calendar = Calendar.getInstance();         
//             
//	        //設定 Calendar 的日期 (月份的base是0)             
//            obj_Calendar.set(Integer.parseInt(a_str_Date.substring(0, 4)) , 
//            				 Integer.parseInt(a_str_Date.substring(4, 6)) - 1, 
//            				 Integer.parseInt(a_str_Date.substring(6, 8)));
//            				 
//            return Calendar.DAY_OF_WEEK;
//    	}
//    	catch (Exception exc)
//    	{
//    		log.error("",exc);
//    		throw new TopException("00001", "DateUtil.getDateWeek");
//    	}
//    }   
    
    // 判斷傳入的日期是否是月底日，回傳 true 表示該日期是月底，回傳 false 表示該日期不是底
    public static boolean isEndOfMonth(String a_str_Date) throws TopException
    {
    	try
    	{
    		String str_NextDate = getDate(a_str_Date, 1);
	    	if (str_NextDate.substring(0, 6).equals(a_str_Date.substring(0, 6)))
    			return false;
	    	else
    			return true;
    	}
    	catch (Exception exc)
    	{
    		log.error("",exc);
    		throw new TopException("00001", "DateUtil.isEndOfMonth");
    	}
    }
    
//    // 檢核傳入的日期是否為營業日(以傳入的國家代碼讀取假日設定)
//    public static boolean checkDateByCtry(
//    		String a_str_Date, String a_str_CtryCode) throws TopException{
//    	try{
//    		// 取得該國家的首都代碼
//    		String str_CityCode = getCityCodeByCtryCode(a_str_CtryCode);
//
//			// 判斷該日期在該城市是否是假日
//			return checkDateByCity(a_str_Date, str_CityCode);    		
//    	}
//		catch (TopException exc) {
//			log.error("",exc);
//			throw exc;
//		}
//		catch (Exception exc) {
//			log.error("",exc);
//			throw new TopException("00001", "DateUtil.checkDateByCtry");
//		}    	
//    }
//    
//    // 檢查傳入的日期是否為營業日(以傳入的城市代碼讀取假日設定)
//    public static boolean checkDateByCity(String a_str_Date,
//    									String a_str_CityCode) throws TopException
//    {
//    	try
//    	{
//    		Holiday obj_Holiday = new Holiday();
//    		return obj_Holiday.isHoliday(a_str_CityCode, a_str_Date);
//    	}
//    	catch (TopException exc)
//    	{
//    		log.error("",exc);
//    		throw exc;
//    	}
//    	catch (Exception exc)
//    	{
//    		log.error("",exc);
//    		throw new TopException("00001", "DateUtil.checkDateByCity");
//    	}
//    }
//    
//    public static boolean checkDateByCcy(String a_str_Date,
//    										String a_str_Currency) throws TopException
//    {
//    	try
//    	{
//			// Modify By Rebecca 2004/03/17
//    		// 取得使用該幣別的國家的首都，以便判斷該日期在該城市是否是假日
//    		// City obj_City = new City();
//    		// String str_CityCode = obj_City.getCapitalByCcy(a_str_Currency);
//    		String str_CityCode = getCityCode(a_str_Currency);
//    		
//    		// 判斷該日期在該城市是否是假日
//    		return checkDateByCity(a_str_Date, str_CityCode);
//    	}
//    	catch (TopException exc)
//    	{
//    		log.error("",exc);
//    		throw exc;
//    	}
//    	catch (Exception exc)
//    	{
//    		log.error("",exc);
//    		throw new TopException("00001", "DateUtil.checkDateByCcy");
//    	}    	
//    }
//    
//    // 取得各交易交割後發送電文的日期
//    public static String getSWIFTSendDate(String a_str_BizCenter,
//    										String a_str_BizType,
//    										String a_str_BizAttr,
//    										String a_str_ValueDate,
//		    								String a_str_Ccy,
//    										String a_str_PayCorrespID,
//    										String a_str_RcvCorrespID,
//    										String a_str_BizDate) throws TopException
//    {
//    	try
//    	{
//    		// 取得ValueDaysType值
//    		SubBizParam obj_SubBizParam = new SubBizParam();
//    		if (!obj_SubBizParam.initProperties(a_str_BizCenter, a_str_BizType, a_str_BizAttr))
//    			throw new TopException("90042", "DateUtil.getSWIFTSendDate -- " + a_str_BizCenter + a_str_BizType + a_str_BizAttr);
//    		String str_ValueDaysType = obj_SubBizParam.getValueDaysType();
//    		
//    		// 依交割天數種類計算交割電文應發送日期
//    		String str_SWIFTSendDate = "";
//    		char c_ValueDaysType = str_ValueDaysType.charAt(0);
//    		switch (c_ValueDaysType)
//    		{
//    			case '1':
//    				str_SWIFTSendDate = getSWIFTSendDateByCcyValueDays(a_str_Ccy, 
//    																   a_str_ValueDate, 
//    																   a_str_BizDate);
//    				break;
//    			case '2':
//    				str_SWIFTSendDate = getSWIFTSendDateByCorresp(a_str_PayCorrespID, 
//    															  a_str_ValueDate, 
//    															  a_str_BizDate);
//    				break;
//    			case '3':
//    				str_SWIFTSendDate = getSWIFTSendDateByCorrespAndCcy(a_str_Ccy, 
//    																    a_str_PayCorrespID, 
//    																    a_str_RcvCorrespID, 
//    																    a_str_ValueDate, 
//    																    a_str_BizDate);
//    				break;
//    			case '4':
//    				str_SWIFTSendDate = getSWIFTSendDateByCcy(a_str_Ccy, 
//    														  a_str_ValueDate, 
//    														  a_str_BizDate);
//    				break;
//    			default:
//    				throw new TopException("00006", "DateUtil.getSWIFTSendDate/ValueDaysType " + str_ValueDaysType);
//    		}
//    		
//    		//加上判斷本地是否為假日,若是則需取得上一營業日當作報文發送日
//			Holiday obj_Hol = new Holiday();
//    		if (obj_Hol.isHoliday(StaticCommon.getCityCode(),str_SWIFTSendDate) == true)
//				str_SWIFTSendDate = getLastBizDate(StaticCommon.getCityCode(),str_SWIFTSendDate);
//    		
//    		return str_SWIFTSendDate;
//    	}
//    	catch (TopException exc)
//    	{
//    		log.error("",exc);
//    		throw exc;
//    	}
//    	catch (Exception exc)
//    	{
//    		log.error("",exc);
//    		throw new TopException("00001", "DateUtil.getSWIFTSendDate");
//    	}
//    }
//
//    public static int calculateMonthsBetweenTwoMonth(String a_str_BeginMonth, 
//    												   String a_str_EndMonth) throws TopException
//    {
//        int i_Months;
//        
//        try
//        {
//			int i_StartYear = NumericUtil.transfStringToInteger(a_str_BeginMonth.substring(0, 4));
//			int i_EndYear = NumericUtil.transfStringToInteger(a_str_EndMonth.substring(0, 4));
//			int i_StartMonth = NumericUtil.transfStringToInteger(a_str_BeginMonth.substring(4, 6));
//			int i_EndMonth = NumericUtil.transfStringToInteger(a_str_EndMonth.substring(4, 6));
//			
//			i_Months = i_EndMonth - i_StartMonth + (i_EndYear - i_StartYear) * 12;
//			
//            return i_Months;
//        }
//        catch (Exception exc)
//        {
//            log.error("",exc);
//            throw new TopException("00001", "DateUtil.calculateMonthsBetweenTwoMonth");
//        }
//    }
//        
//	// 依傳入的起始日期、結束日期、幣別、年度、
//	// 計算計息天數
//	public static int calculateInterestDays( 
//			String a_str_StartDate,
//			String a_str_EndDate, 
//			String a_str_IntBaseType) throws TopException
//	{
//		try
//		{
//			char c_IntBaseType = a_str_IntBaseType.charAt(0);
//            
//			// 計算天數
//			int i_Days;
//			int i_StartDate, i_EndDate;
//			switch (c_IntBaseType)
//			{
//				// 每月為 30 天的計算方式
//				case '4':
//				case '5':
//				case '6':
//					// 判斷起始日期及迄止日期是否為月底，若是月底則改為 30 號
//					if (DateUtil.isEndOfMonth(a_str_StartDate))
//						i_StartDate = 30;
//					else
//						i_StartDate = Integer.parseInt(a_str_StartDate.substring(6));
//						
//					if (DateUtil.isEndOfMonth(a_str_EndDate))
//						i_EndDate = 30;
//					else
//						i_EndDate = Integer.parseInt(a_str_EndDate.substring(6));						
//					
//					// 計算出整月的部份，再計算出未滿月的天數					
//					int i_Months;
//					if (i_StartDate > i_EndDate)
//					{
//						i_Months = DateUtil.calculateMonthsBetweenTwoMonth(a_str_StartDate,	a_str_EndDate) - 1;
//						i_Days = 30 - i_StartDate + i_EndDate;
//					}
//					else
//					{
//						i_Months = DateUtil.calculateMonthsBetweenTwoMonth(a_str_StartDate, a_str_EndDate);
//						
//						// 每月以 30 天計，但 2 月的月底仍以該月的月底計算計算
//						if (a_str_EndDate.substring(6).compareTo("30") > 0)
//							i_Days = i_EndDate - i_StartDate;
//						else
//							i_Days = Integer.parseInt(a_str_EndDate.substring(6)) - i_StartDate;
//					}						
//					i_Days += i_Months * 30;
//					break;
//				case '7':
//				case '8':
//					int i_Month = calculateMonthsBetweenTwoMonth(a_str_StartDate.substring(0,6),a_str_EndDate.substring(0,6));
//					i_Days = i_Month * 30;
//					break;
//				default:
//				{
//					i_Days = DateUtil.caculateDaysBetweenTwoDate(a_str_StartDate,
//																 a_str_EndDate);
//				}
//			}
//			
//			return i_Days;
//		}
//		catch (TopException exc)
//		{
//			log.error("",exc);
//			throw exc;
//		}
//		catch (Exception exc)
//		{
//			log.error("",exc);
//			throw new TopException("00001", "DateUtil.calculateInterestDays");
//		}          
//	}
//	
//	/**********
//	 * 依傳入的營業日天數及幣別計算日期
//	 * @param a_str_Date
//	 * @param a_i_Days：其值 < 0 時，表示往前推算
//	 * 					 其值 > 0 時，表示往後推算
//	 * @param a_str_Currency
//	 * @param a_b_BizDateFlag：其值為 true 時所計算的天數為營業日天數
//	 * 							其值為 false 時所計算的天數不須要是營業日
//	 * @return
//	 * @throws TopException
//	 */
//	public static String calculateDate(
//		String a_str_Date,
//		int a_i_Days,
//		String a_str_Currency,
//		boolean a_b_BizDateFlag) throws TopException{
//			
//		try{
//			String str_Date = a_str_Date;
//			
//			if (a_b_BizDateFlag){
//				if (a_i_Days < 0){
//					for (int i_Count = -1; i_Count >= a_i_Days; i_Count --){
//						str_Date = DateUtil.getDate(str_Date, -1);
//						//Modify by Dino 20040629 12:30
//						//str_Date = DateUtil.getLastBizDate(a_str_Currency, str_Date);	
//						str_Date = DateUtil.getLastBizDateByCcy(a_str_Currency, str_Date);
//						//Modify by Dino 20040629 12:30
//					}
//				}
//				else{
//					for (int i_Count = 1; i_Count <= a_i_Days; i_Count ++){
//						str_Date = DateUtil.getDate(str_Date, 1);
//						str_Date = DateUtil.getNextBizDateByCcy(a_str_Currency, str_Date);
//					}
//				}
//			}
//			else{
//				str_Date = DateUtil.getDate(a_str_Date, a_i_Days);
//				if (a_i_Days < 0){
//					str_Date = DateUtil.getLastBizDateByCcy(a_str_Currency, str_Date);
//				}
//				else{
//					str_Date = DateUtil.getNextBizDateByCcy(a_str_Currency, str_Date);
//				}
//			}
//			
//			return str_Date;
//		}
//		catch (TopException exc){
//			log.error("",exc);
//			throw exc;
//		}
//		catch (Exception exc){
//			log.error("",exc);
//			throw new TopException("00001", "DateUtil.calculateDate");
//		}
//	}
//
//	/**********
//	 * 依傳入的營業日天數及幣別計算日期
//	 * @param a_str_Date
//	 * @param a_i_Days：其值 < 0 時，表示往前推算
//	 * 					 其值 > 0 時，表示往後推算
//	 * @param a_str_Currency
//	 * @param a_b_BizDateFlag：其值為 true 時所計算的天數為營業日天數
//	 * 							其值為 false 時所計算的天數不須要是營業日
//	 * @return
//	 * @throws TopException
//	 */
//	public static String calculateDateByCity(
//		String a_str_Date,
//		int a_i_Days,
//		String a_str_CityCode,
//		boolean a_b_BizDateFlag) throws TopException{
//			
//		try{
//			String str_Date = a_str_Date;
//			
//			if (a_b_BizDateFlag){
//				if (a_i_Days < 0){
//					for (int i_Count = -1; i_Count >= a_i_Days; i_Count --){
//						str_Date = DateUtil.getDate(str_Date, -1);
//						str_Date = DateUtil.getLastBizDate(a_str_CityCode, str_Date);
//					}
//				}
//				else{
//					for (int i_Count = 1; i_Count <= a_i_Days; i_Count ++){
//						str_Date = DateUtil.getDate(str_Date, 1);
//						str_Date = DateUtil.getNextBizDate(a_str_CityCode, str_Date);
//					}
//				}
//			}
//			else{
//				str_Date = DateUtil.getDate(a_str_Date, a_i_Days);
//				if (a_i_Days < 0){
//					str_Date = DateUtil.getLastBizDate(a_str_CityCode, str_Date);
//				}
//				else{
//					str_Date = DateUtil.getNextBizDate(a_str_CityCode, str_Date);
//				}
//			}
//			
//			return str_Date;
//		}
//		catch (TopException exc){
//			log.error("",exc);
//			throw exc;
//		}
//		catch (Exception exc){
//			log.error("",exc);
//			throw new TopException("00001", "DateUtil.calculateDate");
//		}
//	}	
//    
//    private static String caculateDateByFrequency(String a_str_StartDate,
//    												String a_str_Frequency) throws TopException
//    {
//    	try
//    	{
//            Calendar obj_StartCalendar = Calendar.getInstance();
//            String str_IntDate = new String();
//            String str_Frequency = a_str_Frequency;
//            
//            while (str_Frequency.charAt(0) == '0')
//            	str_Frequency = str_Frequency.substring(1);
//             
//    		// 判斷結息週期種類
//    		if (str_Frequency.equals("1D") || str_Frequency.equals("D"))
//    		{
//    			//每日結息
//            	str_IntDate = getDate(a_str_StartDate, 1);
//    		}
//    		else if (str_Frequency.equals("1W") || str_Frequency.equals("W"))
//    		{
//    			// 每週結息
//    			str_IntDate = getDate(a_str_StartDate, 7);
//    		}
//    		else if (str_Frequency.equals("10D"))
//    		{
//    			// 每旬結息
//    			str_IntDate = getDate(a_str_StartDate, 10);
//    		}
//    		else if (str_Frequency.equals("15D"))
//    		{
//    			// 每半月結息
//    			str_IntDate = getDate(a_str_StartDate, 15);
//    		}
//    		else if (str_Frequency.equals("1M") || str_Frequency.equals("M"))
//    		{
//    			// 每月結息
//				str_IntDate = getDateAddByMonth(a_str_StartDate, 1);
//    		}
//    		else if (str_Frequency.equals("3M"))
//    		{
//    			// 每季結息
//    			str_IntDate = getDateAddByMonth(a_str_StartDate, 3);
//    		}
//    		else if (str_Frequency.equals("6M"))
//    		{
//    			// 每半年結息
//    			str_IntDate = getDateAddByMonth(a_str_StartDate, 6);
//    		}
//    		else if (str_Frequency.equals("1Y") || str_Frequency.equals("Y"))
//    		{
//    			// 每年結息
//    			str_IntDate = getDateAddByMonth(a_str_StartDate, 12);
//    		}
//    		else
//    		{
//    			// 目前只處理以上幾種計息週期的資料
//    			throw new TopException("00006", "DateUtil.Frequency = " + a_str_Frequency);
//    		}
//    		
//    		return str_IntDate;
//    	}
//    	catch (TopException exc)
//    	{
//    		log.error("",exc);
//    		throw exc;
//    	}
//    	catch (Exception exc)
//    	{
//    		log.error("",exc);
//    		throw new TopException("00001", "DateUtil.caculateDateByFrequency");
//    	}	    	
//    }
//    
//    // 依據幣別之前置日數計算報文應發送日期
//    private static String getSWIFTSendDateByCcyValueDays(String a_str_Ccy,
//    									   	      		   String a_str_ValueDate,
//    							   	  			  		   String a_str_BizDate) throws TopException
//    {
//    	try
//    	{
//    		// 取得該幣別的交割前置日數
//    		Currency obj_Currency = new Currency();
//    		if (!obj_Currency.initProperties(a_str_Ccy))
//    			throw new TopException("90042", "DateUtil.getSWIFTSendDateByCcyValueDays -- " + a_str_Ccy);
//    		int i_ValueDays = obj_Currency.getValueDays();
//
//    		// Modify By Rebecca 2004/03/17
//    		// 取得使用該幣別的國家的首都，以便判斷該日期在該城市是否是假日
//    		// City obj_City = new City();
//    		// String str_CityCode = obj_City.getCapitalByCcy(a_str_Ccy);    		
//    		String str_CityCode = getCityCode(a_str_Ccy);
//    		
//    		// 依交割前置日數計算交割電文應發送日期
//    		String str_SWIFTSendDate = getSWIFTSendDateByValueDays(a_str_ValueDate,
//    															   i_ValueDays,
//    															   str_CityCode,
//    															   a_str_BizDate);   																	
//   
//    		return str_SWIFTSendDate;
//    	}
//    	catch (TopException exc)
//    	{
//    		log.error("",exc);
//    		throw exc;
//    	}
//    	catch (Exception exc)
//    	{
//    		log.error("",exc);
//    		throw new TopException("00001", "DateUtil.getSIWFTSendDateByCcyValueDays");
//    	}
//    }
//
//    // 依據扣款行所在國家的交割前置日數計算發文日期
//    private static String getSWIFTSendDateByCorresp(String a_str_CorrespID,
//    									   	         String a_str_ValueDate,
//    							   	  			  	 String a_str_BizDate) throws TopException
//    {
//    	try
//    	{
//    		// 取得該帳戶行所在國家的交割前置日數
//    		Cpty obj_Cpty = new Cpty();
//    		if (!obj_Cpty.initProperties(a_str_CorrespID))
//    			throw new TopException("90042", "DateUtil.getSWIFTSendDateByCorresp -- " + a_str_CorrespID);
//    		String str_CityCode = obj_Cpty.getCityCode();
//    		City obj_City = new City();
//    		if (!obj_City.initProperties(str_CityCode))
//    			throw new TopException("90042", "DateUtil.getSWIFTSendDateByCorresp -- " + obj_Cpty.getCityCode());
//    		Ctry obj_Ctry = new Ctry();
//    		if (!obj_Ctry.initProperties(obj_City.getCtryCode()))
//    			throw new TopException("90042", "DateUtil.getSWIFTSendDateByCorresp -- " + obj_City.getCtryCode());	
//    		int i_ValueDays = obj_Ctry.getValueDays();
//    		
//     		// 依交割前置日數計算交割電文應發送日期
//    		String str_SWIFTSendDate = getSWIFTSendDateByValueDays(a_str_ValueDate,
//    															   i_ValueDays,
//    															   str_CityCode,
//    															   a_str_BizDate);
//    		
//    		return str_SWIFTSendDate;
//    	}
//    	catch (TopException exc)
//    	{
//    		log.error("",exc);
//    		throw exc;
//    	}
//    	catch (Exception exc)
//    	{
//    		log.error("",exc);
//    		throw new TopException("00001", "DateUtil.getSIWFTSendDateByCcy");
//    	}    
//    }
//
//   // 依據幣別及扣款行所在國家的交割前置日數計算發文日期
//   private static String getSWIFTSendDateByCorrespAndCcy(String a_str_Ccy,
//   														  String a_str_PayCorrespID,
//   														  String a_str_RcvCorrespID,
//    									   	         	  String a_str_ValueDate,
//    							   	  			  	 	  String a_str_BizDate) throws TopException
//    {
//    	try
//    	{
//    		// 取得依扣帳行計算的電文發送日期
//    		String str_PaySendDate = getSWIFTSendDateByCorresp(a_str_PayCorrespID, 
//    														   a_str_ValueDate, 
//    														   a_str_BizDate);
//    
//    		// 取得依進帳行計算的電文發送日期
//    		String str_RcvSendDate = getSWIFTSendDateByCorresp(a_str_RcvCorrespID, 
//    														   a_str_ValueDate, 
//    														   a_str_BizDate);
//    		
//    		// 取得依幣別計算的電文發送日期
//			String str_CcySendDate = getSWIFTSendDateByCcyValueDays(a_str_Ccy,
//																	a_str_ValueDate,
//																	a_str_BizDate);
//    			
// 		    // 比較三個日期，找出最小的日期
// 		    String str_SWIFTSendDate = "";
//			int i_PaySendDate = NumericUtil.transfStringToInteger(str_PaySendDate);
//			int i_RcvSendDate = NumericUtil.transfStringToInteger(str_RcvSendDate);
//			int i_CcySendDate = NumericUtil.transfStringToInteger(str_CcySendDate);
//			if (i_PaySendDate > i_RcvSendDate)
//			{
//				if (i_RcvSendDate > i_CcySendDate)
//					str_SWIFTSendDate = str_CcySendDate;
//				else
//					str_SWIFTSendDate = str_RcvSendDate;
//			}
//			else
//			{
//				if (i_PaySendDate > i_CcySendDate)
//					str_SWIFTSendDate = str_CcySendDate;
//				else
//					str_SWIFTSendDate = str_PaySendDate;
//			}
//			
//			return str_SWIFTSendDate;				
//    	}
//    	catch (TopException exc)
//    	{
//    		log.error("",exc);
//    		throw exc;
//    	}
//    	catch (Exception exc)
//    	{
//    		log.error("",exc);
//    		throw new TopException("00001", "DateUtil.getSIWFTSendDateByCorrespAndCcy");
//    	}    
//    }
//   
//    // 依據美金前一天雜幣前兩天的前置日數規則計算報文應發送日期
//    private static String getSWIFTSendDateByCcy(String a_str_Ccy,
//    									   	      String a_str_ValueDate,
//    							   	  			  String a_str_BizDate) throws TopException
//    {
//    	try
//    	{
//    		// 取得該幣別的交割前置日數
//	   		int i_ValueDays;
//	   		if (a_str_Ccy.equals("USD"))
//    			i_ValueDays = 1;
//    		else
//    			i_ValueDays = 2;
//
//    		// 取得使用單位所在城市
//    		String str_CityCode = StaticCommon.getCityCode();    		
//    		
//    		// 依交割前置日數計算交割電文應發送日期
//    		String str_SWIFTSendDate = getSWIFTSendDateByValueDays(a_str_ValueDate,
//    															   i_ValueDays,
//    															   str_CityCode,
//    															   a_str_BizDate);   																	
//   
//    		return str_SWIFTSendDate;
//    	}
//    	catch (TopException exc)
//    	{
//    		log.error("",exc);
//    		throw exc;
//    	}
//    	catch (Exception exc)
//    	{
//    		log.error("",exc);
//    		throw new TopException("00001", "DateUtil.getSIWFTSendDateByCcy");
//    	}
//    }
// 
//    private static String getSWIFTSendDateByValueDays(String a_str_ValueDate,
//    												   int a_i_ValueDays,
//    												   String a_str_CityCode,
//    												   String a_str_BizDate) throws TopException
//    {
//    	try
//    	{
//    		// 依交割前置日數計算交割電文應發送日期
//    		String str_SWIFTSendDate = getDate(a_str_ValueDate, a_i_ValueDays * -1);
//    
//			// 找到必須為營業日的電文發送日期
//			while (true)
//			{
//				if (NumericUtil.transfStringToInteger(str_SWIFTSendDate)
//					< NumericUtil.transfStringToInteger(a_str_BizDate))
//				{
//					str_SWIFTSendDate = a_str_BizDate;
//					break;
//				}
//				
//				if (checkDateByCity(str_SWIFTSendDate, a_str_CityCode))
//					str_SWIFTSendDate = getDate(str_SWIFTSendDate, -1);
//				else
//					break;
//			}		
//    		
//    		return str_SWIFTSendDate;    		
//    	}
//    	catch (TopException exc)
//    	{
//    		log.error("",exc);
//    		throw exc;
//    	}
//    	catch (Exception exc)
//    	{
//    		log.error("",exc);
//    		throw new TopException("00001", "DateUtil.getSWIFTSendDateByValueDays");
//    	}
//    }    	
//    
//    // 依傳入的幣別取得此幣別適的的假日城市代碼
//    private static String getCityCode(String a_str_Currency) throws TopException {
//    	try {
//			String str_CityCode = "";
//    		if (!p_lhm_CurrencyCityMapping.containsKey(a_str_Currency)){
//				City obj_City = new City();
//				str_CityCode = obj_City.getCityCodeByCcy(a_str_Currency);
//				p_lhm_CurrencyCityMapping.put(a_str_Currency, str_CityCode);
//    		}
//    		else
//    			str_CityCode = (String) p_lhm_CurrencyCityMapping.get(a_str_Currency);
//
//    		return str_CityCode;
//    	}
//    	catch (TopException exc)
//    	{
//    		log.error("",exc);
//    		throw exc;
//    	}
//    	catch (Exception exc)
//    	{
//    		log.error("",exc);
//    		throw new TopException("00001", "DateUtil.getCityCode");
//    	}
//    }	
//    
//    private static String getCityCodeByCtryCode(String a_str_CtryCode) throws TopException{
//    	try{
//    		String str_CityCode = "";
//    		if (!p_lhm_CtryCityMapping.containsKey(a_str_CtryCode)){
//    			Ctry obj_Ctry = new Ctry();
//    			str_CityCode = obj_Ctry.getCapital(a_str_CtryCode);
//    			if (str_CityCode == null)
//    				throw new TopException("90042", "DateUtil.getCityCodeByCtryCode CtryCode = " + a_str_CtryCode);
//				p_lhm_CtryCityMapping.put(a_str_CtryCode, str_CityCode);		
//    		}
//    		else
//    			str_CityCode = (String) p_lhm_CtryCityMapping.get(a_str_CtryCode);
//    		
//    		return str_CityCode;
//    	}
//		catch (TopException exc)
//		{
//			log.error("",exc);
//			throw exc;
//		}
//		catch (Exception exc)
//		{
//			log.error("",exc);
//			throw new TopException("00001", "DateUtil.getCityCodeByCtryCode");
//		}    	
//    }

	/**
	 *  將公元年轉換為民國年
	 */
	public static String getTaiwanDate(String a_str_ADDate)
	{
		String str_TaiwanDate = null;
		String str_FirstDigit = "";
		
		try
		{
			int i_TaiwanYear = Integer.parseInt((String)a_str_ADDate.substring(2, 4)) + 100 -11;
			
		    if (i_TaiwanYear < 100)
		    	str_FirstDigit = "0";
		  
			str_TaiwanDate = str_FirstDigit + Integer.toString(i_TaiwanYear) + (String)a_str_ADDate.substring(4);

		}
		catch (Exception exc)
		{
			//log.error("",exc);
			//throw new TopException("00001", "DateUtil.getTaiwanDate()");
			str_TaiwanDate = "日期格式錯誤:" + a_str_ADDate;
		}    	
				
		return str_TaiwanDate;
	}
	
	/**
	 *  將日期加以格式化, 例: 097/10/10, 2008/10/10
	 */
	public static String formatDate(String a_str_ADDate)
	{
		StringBuffer stb_FormattedDate = new StringBuffer();
		
		try
		{			
			if (a_str_ADDate.indexOf("/") != -1)
				return a_str_ADDate;
			
			if (a_str_ADDate.length() == 6)
			{
				String year = a_str_ADDate.substring(0, 2);
				String month = a_str_ADDate.substring(2, 4);
				String day = a_str_ADDate.substring(4);

				stb_FormattedDate.append("0");
				stb_FormattedDate.append(year);
				stb_FormattedDate.append("/");
				stb_FormattedDate.append(month);
				stb_FormattedDate.append("/");
				stb_FormattedDate.append(day);		
			}
			else if (a_str_ADDate.length() == 7)
			{
				String year = a_str_ADDate.substring(0, 3);
				String month = a_str_ADDate.substring(3, 5);
				String day = a_str_ADDate.substring(5);
				
				stb_FormattedDate.append(year);
				stb_FormattedDate.append("/");
				stb_FormattedDate.append(month);
				stb_FormattedDate.append("/");
				stb_FormattedDate.append(day);		
			}
			else if (a_str_ADDate.length() == 8)
			{
				String year = null;
				
				if ("0".equals(a_str_ADDate.substring(0,1)))
					year = a_str_ADDate.substring(1, 4);										
				else
					year = a_str_ADDate.substring(0, 4);					
				
				String month = a_str_ADDate.substring(4, 6);
				String day = a_str_ADDate.substring(6);
				
				stb_FormattedDate.append(year);
				stb_FormattedDate.append("/");
				stb_FormattedDate.append(month);
				stb_FormattedDate.append("/");
				stb_FormattedDate.append(day);	
			}
		}
		catch (Exception exc)
		{
			//log.error("",exc);
			//throw new TopException("00001", "DateUtil.formatDate()");
			
			stb_FormattedDate.delete(0, stb_FormattedDate.length());
			stb_FormattedDate.append("日期格式錯誤:"+a_str_ADDate);
		}    	
				
		return stb_FormattedDate.toString();
	}
	
	/**
	 *  將時間加以格式化, 例: 13:10:10
	 */
	public static String formatTime(String a_str_Time)
	{
		StringBuffer stb_FormattedTime = new StringBuffer();
		
		try
		{			
			if (a_str_Time.indexOf(":") != -1)
				return a_str_Time;
			
			if (a_str_Time.length() == 6)
			{
				String hh = a_str_Time.substring(0, 2);
				String mm = a_str_Time.substring(2, 4);
				String ss = a_str_Time.substring(4);
				
				stb_FormattedTime.append(hh);
				stb_FormattedTime.append(":");
				stb_FormattedTime.append(mm);
				stb_FormattedTime.append(":");
				stb_FormattedTime.append(ss);		
			}
		}
		catch (Exception exc)
		{
			//log.error("",exc);
			//throw new TopException("00001", "DateUtil.formatDate()");
			
			stb_FormattedTime.delete(0, stb_FormattedTime.length());
			stb_FormattedTime.append("時間格式錯誤:"+a_str_Time);
		}    	
				
		return stb_FormattedTime.toString();
	}	
	
	/**
	 *  取得 <上N月> 的起迄日期
	 */
	public static Hashtable getLastMonthPeriod(String a_str_Type, int a_i_LastMonNum)
	{
		Hashtable htl_Date = new Hashtable();
		
		try
		{			
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.DATE, 1);
			cal.add(Calendar.MONTH, - a_i_LastMonNum);
			Date dstart = cal.getTime();
			cal.add(Calendar.MONTH, 1);
			cal.add(Calendar.DATE, - a_i_LastMonNum);
			Date dend = cal.getTime();
			String stadate;
			String enddate;
			
			if ("C".equals(a_str_Type)) 
			{	
				stadate = DateTimeUtils.getCDateShort(dstart);
			    enddate = DateTimeUtils.getCDateShort(dend);
			}
			else
			{
				  stadate = DateTimeUtils.getDateShort(dstart);
				  enddate = DateTimeUtils.getDateShort(dend);
			}
			
			htl_Date.put("START", stadate);
			htl_Date.put("END", enddate);
		}
		catch (Exception exc)
		{
			//log.error("",exc);
			
			log.debug("Exception from DateUtil.getLastMonthPeriod() : "+exc);			
		}    	
				
		return htl_Date;
	}		
	
}