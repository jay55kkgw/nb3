/*
 * Machine.java
 *
 * Created on 2002年8月2日, 上午 10:32
 */

package topibs.utility;

import java.util.Calendar;


/**
 *
 * @author  chuliu
 * @version 
 */
public class Machine {

    /** Creates new Machine */
    public Machine() {
    }
    public static TopLinkedHashMap getSystemDateTime() throws Exception {
        String str_SystemDate,str_SystemTime,str_month,str_date;
        TopLinkedHashMap htl_SystemDateTime=new TopLinkedHashMap();
        try {
             Calendar obj_calendar = Calendar.getInstance();         
             /* 取得系統日期 格式: yyyymmdd */
             if ((obj_calendar.get(Calendar.MONTH)+1)< 10 )
              str_month="0" +(obj_calendar.get(Calendar.MONTH)+1);
         else
            str_month=String.valueOf(obj_calendar.get(Calendar.MONTH)+1);

         if (obj_calendar.get(Calendar.DATE)< 10 )
            str_date="0" + obj_calendar.get(Calendar.DATE);
         else
            str_date=String.valueOf(obj_calendar.get(Calendar.DATE));

         str_SystemDate=obj_calendar.get(Calendar.YEAR) + str_month + str_date;

       /* 取得系統時間 格式(24小時進制): hhmmss */
         String str_hour,str_minute,str_second;

         if (obj_calendar.get(Calendar.HOUR)< 10 && obj_calendar.get(Calendar.AM_PM)==Calendar.AM)
            str_hour="0" + obj_calendar.get(Calendar.HOUR);
         else if (obj_calendar.get(Calendar.AM_PM)==Calendar.PM)
            str_hour=String.valueOf(obj_calendar.get(Calendar.HOUR)+12);
         else
            str_hour=String.valueOf(obj_calendar.get(Calendar.HOUR));

         if (obj_calendar.get(Calendar.MINUTE)< 10 )
            str_minute="0" + obj_calendar.get(Calendar.MINUTE);
         else
            str_minute=String.valueOf(obj_calendar.get(Calendar.MINUTE));

         if (obj_calendar.get(Calendar.SECOND)< 10 )
            str_second="0" + obj_calendar.get(Calendar.SECOND);
         else
            str_second=String.valueOf(obj_calendar.get(Calendar.SECOND));

         str_SystemTime=str_hour + str_minute + str_second ; 

     	 
         htl_SystemDateTime.put("SystemDate", DateUtil.formatDate(str_SystemDate));      
         htl_SystemDateTime.put("SystemTime", DateUtil.formatTime(str_SystemTime));
         htl_SystemDateTime.put("SystemDateTime", 
        		 				DateUtil.formatDate(str_SystemDate) + 
        		 				" " + 
        		 				DateUtil.formatTime(str_SystemTime)); 
         
         return htl_SystemDateTime;
        }
        catch (Exception e){throw new Exception("get SystemDateTime error");}
    }



}
