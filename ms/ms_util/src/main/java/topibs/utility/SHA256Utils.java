package topibs.utility;

import java.security.MessageDigest;
import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;


public class SHA256Utils {
	static Logger logger = Logger.getLogger(SHA256Utils.class);

	public static String encrypt(String s, String encoding) {

		logger.info("encrypt string : " + s);
		MessageDigest sha = null;

		try {
			sha = MessageDigest.getInstance("SHA-256");
			sha.update(s.getBytes(encoding));
		} catch (Exception e) {
			logger.error("encrypt error>>{}", e);
			return "";
		}

		return byte2hex(sha.digest());

	}

	public static String encrypt512(String s, String encoding) {

		logger.info("encrypt string : " + s);
		MessageDigest sha = null;

		try {
			sha = MessageDigest.getInstance("SHA-512");
			sha.update(s.getBytes(encoding));
		} catch (Exception e) {
			logger.error("encrypt error>>{}", e);
			return "";
		}

		return byte2hex(sha.digest());

	}
	private static String byte2hex(byte[] b) {
		String hs = "";
		String stmp = "";
		for (int n = 0; n < b.length; n++) {
			stmp = (java.lang.Integer.toHexString(b[n] & 0XFF));
			if (stmp.length() == 1)
				hs = hs + "0" + stmp;
			else
				hs = hs + stmp;
		}
		return hs.toUpperCase();
	}
	
//	public static void testSha256() {
//		String inputStr = "94320170314031410000100010800000000112233445566943000000000001";
//		String validParam = "5E8B6E1998F421204C6576544FE1A26B44FC775982D8CE2E"; //驗證參數
//		String ret = encrypt(inputStr + validParam, "utf-8");
//		Validate.isTrue("5CC1FD1E8E7ACC4E9A237FF7950329165906A8D86A578C56C22CBB533EE7B0DD".equals(ret));
//	}
	
//	public static void main(String[] args) {
//		testSha256();
//		logger.info("done.");
//	}
}
