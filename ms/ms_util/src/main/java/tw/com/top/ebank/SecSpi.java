package tw.com.top.ebank;

//-------------[java]--------------------------------------------------

import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.Locale;

import org.apache.log4j.PropertyConfigurator;

import com.hitrust.isecurity2_0.SignerInfo;
import com.hitrust.isecurity2_0.client.PKCS7SignatureProc;
import com.linkway.isecurity.BinaryValue;
import com.linkway.isecurity.CertInfo;
import com.linkway.isecurity.DESKeyValue;
import com.linkway.isecurity.KeyInfo;
import com.linkway.isecurity.KeyMangProc;
//import com.linkway.isecurity.PKCS7SignatureProc;
import com.linkway.isecurity.SecurityUtil;
//import com.linkway.isecurity.SignerInfo;
import com.linkway.isecurity.X500NameStruct;

import lombok.extern.slf4j.Slf4j;


//-------------[IBM]--------------------------------------------------
//import com.ibm.crypto.pkcs11impl.provider.IBMPKCS11Impl;


/*
#Triple DES cross domain key ' XOR 701ED7DD53580A7C D11DB42461D63CEA '
#KeyCDA         = 82B2D6EB33E36DCF21B7DBB45C110A54         check = 014E
#KeyCDB         = F2AC013660BB67B3F0AA6F903DC736BE         check = E044
#Key            = 701ED7DD53580A7CD11DB42461D63CEA         check = 64AB
 */

@Slf4j
public class SecSpi implements IKeySpi 
{
	private static final String DEBUG_CONFIG = "KeySvr.properties";
//	static Logger log = Logger.getLogger(SecSpi.class);  

	// The TEST KEY string 
	private static final String CDKey = "701ED7DD53580A7CD11DB42461D63CEA";
	private static final String CDCheck = "64AB";

	public static final String TIME_PATTERN = "yyyyMMddHHmmss";

	/**
	 * 工具函式: 輸出除錯訊息
	 * @param s      除錯訊息
	 * 
	 */        
	public static void debug(String s)
	{
		//log.debug(s);
		//log.debug(s);    
	}

	/**
	 * 工具函式: 輸出除錯訊息
	 * @param e      除錯訊息
	 * 
	 */        
	public static void debug(Exception e)
	{
		//log.debug("" + e);
		//log.debug("" + e);            
	}

	/**
	 * 工具函式: 輸出錯誤訊息
	 * @param s      錯誤訊息
	 * 
	 */        
	public static void error(String s)
	{
		//log.debug(s);
		//log.error(s);                        
	}

	/**
	 * 工具函式: 輸出錯誤訊息
	 * @param e      錯誤訊息
	 * 
	 */        
	public static void error(Exception e)
	{
		//log.debug("" + e);
		//log.error("" + e);                        
	}


	//可能要在此處 初始化 iSecurity 的物件
	private static SecurityUtil SecurityDes = new SecurityUtil();

	/*  generating key  on PKCS#11token and save it there    

Provider nss = new sun.security.pkcs11.SunPKCS11(configFile);
Security.insertProviderAt(nss, 1);  //you may not want it at highest priority
KeyStore ks = KeyStore.getInstance("PKCS11", nss);
ks.load(null, password);

KeyGenerator kg = KeyGenerator.getInstance("DESede",nss);
SecretKey tripleDesKey = kg.generateKey();
KeyStore.SecretKeyEntry skEntry = new KeyStore.SecretKeyEntry(tripleDesKey);
ks.setEntry(randAlias, skEntry, new KeyStore.PasswordProtection(password));




	 */

	/**
	 * Constructor
	 */
	public SecSpi()
	{
	}

	//無實際內容
	public boolean putRequest(String req)
	{
		return false;    
	}

	//無實際內容
	public String getResponse()
	{
		return null;
	}

	//派送功能
	//依照輸入的功能代碼來拆解參數字串，並呼叫相應的函式
	public String doRequest(String req)
	{
		int idx = 0;
		String result = IKeySpi.RC_ERR;  //先預設失敗

		String id = req.substring(idx, idx + 4);
		idx += 4;

		debug("REQ ID = " + id);

		try
		{
			//產生 MAC
			if (id.equals(IKeySpi.CREATE_MAC))
			{
				//此處輸入的 key frame 並非真正的 key，而是 key label
				//key label 後接空白填滿    	
				String keyFrame = req.substring(idx, idx + 48);
				idx += 48;

				String keyLabel = keyFrame.trim();

				debug("KEY FRAME = " + keyFrame);

				String initVector = req.substring(idx, idx + 16);
				idx += 16;

				debug("ICV = " + initVector);

				String textSize = req.substring(idx, idx + 4);
				idx += 4;

				int size = Integer.parseInt(textSize);

				debug("TEXT SIZE = " + textSize);

				String textData = req.substring(idx, idx + size);
				idx += size;

				debug("TEXT DATA = " + textData);

				String mac = doMAC(keyLabel, initVector, textData);

				//若是失敗
				if (mac == null)
				{
					return result;
				}

				//產生4位長度字串
				int i = mac.length();                
				String rSize = "0000" + String.valueOf(i);
				rSize = rSize.substring(rSize.length() - 4);

				result = IKeySpi.RC_OK + rSize + mac;

				return result;
			}    //Create MAC

			//產生 SYNC
			// 2008/06/18 sync 改為以 mac 方式計算
			if (id.equals(IKeySpi.CREATE_SYNC))
			{
				//此處輸入的 key frame 並非真正的 key，而是 key label
				//key label 後接空白填滿    	
				String keyFrame = req.substring(idx, idx + 48);
				idx += 48;

				String keyLabel = keyFrame.trim();

				debug("KEY FRAME = " + keyFrame);

				//
				String initVector = req.substring(idx, idx + 16);
				idx += 16;

				debug("ICV = " + initVector);
				//

				String textSize = req.substring(idx, idx + 4);
				idx += 4;

				int size = Integer.parseInt(textSize);

				debug("TEXT SIZE = " + textSize);

				String textData = req.substring(idx, idx + size);
				idx += size;

				debug("TEXT DATA = " + textData);

				//String sync = doSYNC(keyLabel, textData);
				String sync = doSYNC(keyLabel, initVector, textData);

				//若是失敗
				if (sync == null)
				{
					return result;
				}

				int i = sync.length();                
				String rSize = "0000" + String.valueOf(i);
				rSize = rSize.substring(rSize.length() - 4);

				result = IKeySpi.RC_OK + rSize + sync;

				return result;
			}    //Create SYNC

			//產生新基碼
			if (id.equals(IKeySpi.CREATE_KEY))
			{
				//此處輸入的 key frame 並非真正的 key，而是 key label
				//key label 後接空白填滿    	
				String masterKeyFrame = req.substring(idx, idx + 48);
				idx += 48;

				String masterKeyLabel = masterKeyFrame.trim();

				String newKeyFrame = req.substring(idx, idx + 48);
				idx += 48;

				String newKeyLabel = newKeyFrame.trim();

				String textSize = req.substring(idx, idx + 4);
				idx += 4;

				int size = Integer.parseInt(textSize);

				debug("TEXT SIZE = " + textSize);

				String textData = req.substring(idx, idx + size);
				idx += size;

				debug("TEXT DATA = " + textData);

				String rc = doGenKey(masterKeyLabel, newKeyLabel, textData);

				//若是失敗
				if (rc == null)
				{
					return result;
				}

				result = IKeySpi.RC_OK + "0000";

				return result;
			}    //Create new key


			//加密資料
			if (id.equals(IKeySpi.ENCRYPT_DATA))
			{
				//此處輸入的 key frame 並非真正的 key，而是 key label
				//key label 後接空白填滿    	
				String keyFrame = req.substring(idx, idx + 48);
				idx += 48;

				String keyLabel = keyFrame.trim();

				debug("KEY FRAME = " + keyFrame);

				String textSize = req.substring(idx, idx + 4);
				idx += 4;

				int size = Integer.parseInt(textSize);

				debug("TEXT SIZE = " + textSize);

				String textData = req.substring(idx, idx + size);
				idx += size;

				debug("TEXT DATA = " + textData);

				String rc = doEncrypt(keyLabel, textData);

				//若是失敗
				if (rc == null)
				{
					return result;
				}

				int i = rc.length();                
				String rSize = "0000" + String.valueOf(i);
				rSize = rSize.substring(rSize.length() - 4);

				result = IKeySpi.RC_OK + rSize + rc;

				return result;
			}    //Encrypt 

			//檢驗憑證
			if (id.equals(IKeySpi.VERIFY_SIGN))
			{

				String textSize = req.substring(idx, idx + 4);
				idx += 4;

				int size = Integer.parseInt(textSize);

				debug("TEXT SIZE = " + textSize);

				String textData = req.substring(idx, idx + size);
				idx += size;

				debug("TEXT DATA = " + textData);

				String rc = doVerifySign(textData);

				//若是失敗
				if (rc == null)
				{
					return result;
				}

				int i = rc.length();                
				String rSize = "0000" + String.valueOf(i);
				rSize = rSize.substring(rSize.length() - 4);

				result = IKeySpi.RC_OK + rSize + rc;

				return result;
			}    //Verify Sign

			//解密資料
			if (id.equals(IKeySpi.DECRYPT_DATA))
			{
				//此處輸入的 key frame 並非真正的 key，而是 key label
				//key label 後接空白填滿    	
				String keyFrame = req.substring(idx, idx + 48);
				idx += 48;

				String keyLabel = keyFrame.trim();

				debug("KEY FRAME = " + keyFrame);

				String textSize = req.substring(idx, idx + 4);
				idx += 4;

				int size = Integer.parseInt(textSize);

				debug("TEXT SIZE = " + textSize);

				String textData = req.substring(idx, idx + size);
				idx += size;

				debug("TEXT DATA = " + textData);

				String rc = doDecrypt(keyLabel, textData);

				//若是失敗
				if (rc == null)
				{
					return result;
				}

				int i = rc.length();                
				String rSize = "0000" + String.valueOf(i);
				rSize = rSize.substring(rSize.length() - 4);

				result = IKeySpi.RC_OK + rSize + rc;

				return result;
			}    //Decrypt 

			//變更基碼程序
			if (id.equals(IKeySpi.CHANGE_KEY))
			{
				//此處輸入的 key frame 並非真正的 key，而是 key label
				//key label 後接空白填滿    	
				String mastKeyFrame = req.substring(idx, idx + 48);
				idx += 48;

				String mastKeyLabel = mastKeyFrame.trim();

				debug("MAST KEY FRAME = " + mastKeyFrame);

				String workKeyFrame = req.substring(idx, idx + 48);
				idx += 48;

				String workKeyLabel = workKeyFrame.trim();

				debug("WORK KEY FRAME = " + workKeyFrame);

				String newKeyData = req.substring(idx, idx + 48);
				idx += 48;

				newKeyData = newKeyData.trim();

				debug("NEW KEY DATA = " + newKeyData);

				String textSize = req.substring(idx, idx + 4);
				idx += 4;

				int size = Integer.parseInt(textSize);

				debug("TEXT SIZE = " + textSize);

				String textData = req.substring(idx, idx + size);
				idx += size;

				debug("TEXT DATA = " + textData);

				String rc = doChangeKey(mastKeyLabel, workKeyLabel, newKeyData, textData);

				//若是失敗
				if (rc == null)
				{
					return result;
				}

				int i = rc.length();                
				String rSize = "0000" + String.valueOf(i);
				rSize = rSize.substring(rSize.length() - 4);

				result = IKeySpi.RC_OK + rSize + rc;

				return result;
			}

		}
		catch(Exception e)
		{
			error( e );
		}
		return result;
	}

	public String doEncrypt(String keyFrame, String data)
	{
		try
		{
			byte [] packData = pack(data);
			byte[] result = SecurityDes.encrypt("DES2", keyFrame, packData, 0, packData.length);    	
			return new String( unpack(result) );
		}
		catch(Exception e)
		{
			error(e);
		}
		return null;
	}

	public String doDecrypt(String keyFrame, String data)
	{
		try
		{
			byte [] packData = pack(data);
			byte[] result = SecurityDes.decrypt("DES2", keyFrame, packData, 0, packData.length);    	
			return new String( unpack(result) );
		}
		catch(Exception e)
		{
			error(e);
		}
		return null;
	}

	//public String doSYNC(String keyFrame, String data)
	public String doSYNC(String keyFrame, String initVector, String data)
	{
		//return doEncrypt(keyFrame, data);
		return doMAC(keyFrame, initVector, data);
	}

	public String doMAC(String keyFrame, String icv, String data)
	{
		try
		{
			byte [] packICV = pack(icv);
			byte [] packData = pack(data);
			byte[] result = SecurityDes.dataMAC("DES2", keyFrame, packICV, packData, 0, packData.length);    	
			return new String( unpack(result) );
		}
		catch(Exception e)
		{
			error(e);
		}
		return null;
	}


	public String doGenKey(String mastKeyFrame, String workKeyFrame, String keyData)
	{
		try
		{
			byte [] rnd = new byte[8];
			byte [] rnd1 = new byte[8];
			byte [] rnd2 = new byte[8];

			//先刪除再產生

			//產生一個空的 KeyMangProc ，不指定使用者及密碼
			//使用現有的 session 來處理基碼
			KeyMangProc keyManager = new KeyMangProc("","");

			//顯示 key 資訊
			/*
            KeyInfo[] keyInfo = keyManager.getKeyList ("All", "DES2");
            for (int i = 0; i < keyInfo.length; i++)
            {
            	debug("KEY LABEL = " + keyInfo[i].keyLabel);
            	debug("KEY LENG  = " + keyInfo[i].keyLength);
            }
			 */                        
			KeyInfo ki = new KeyInfo();

			ki.keyType = "Secure";
			ki.algorithm = "DES2";
			ki.keyLabel = workKeyFrame;

			//確保 key 不存在時可以正常產生 key
			try
			{
				keyManager.deleteKey(ki);
			}
			catch(Exception e1)
			{
				error(e1);
			}    

			ki.keyType = "Secure";
			ki.algorithm = "DES2";
			ki.keyLabel = workKeyFrame;
			ki.keyLength = 128;


			DESKeyValue	desKeyValue = new DESKeyValue();
			desKeyValue.keyLength = ki.keyLength;

			BinaryValue binValue = new BinaryValue();

			byte [] newKey = pack(keyData);

			debug("NEW KEY = " + new String(unpack(newKey)));

			binValue.length = newKey.length;
			binValue.value = newKey; 

			desKeyValue.keyValue = binValue;

			//載入明碼基碼 OK
			keyManager.loadDESKey (ki, desKeyValue);
			/*            
            KeyInfo ki2 = new KeyInfo();
            ki2.keyType = "Secure";
            ki2.algorithm = "DES2";
            ki2.keyLabel = mastKeyFrame;
            ki2.keyLength = 128;

            //載入加密基碼 OK
            keyManager.unWrapKey (ki2, ki, newKey);
			 */

			//產生新基碼 OK
			//byte[] newKey = SecurityDes.genEncryptedKeyWithRandom("DES2", workKeyFrame, mastKeyFrame, rnd, rnd1, rnd2);    	

			//byte[] newKey = pack(CDKey);
			//SecurityDes.loadEncryptedKey("DES2", workKeyFrame, mastKeyFrame, newKey);

			//置入新基碼
			//SecurityDes.loadEncryptedDESKeyWithRandom("DES2", workKeyFrame, mastKeyFrame, newKey, rnd, rnd1, rnd2);

			return "OK";
		}
		catch(Exception e)
		{
			error(e);
		}
		return null;
	}

	public String doVerifySign(String signData)
	{
		try
		{    		
			PKCS7SignatureProc pkcs7 = new PKCS7SignatureProc();

			if (pkcs7 == null)
			{
				error("Unable create PKCS7SignatureProc");
				return null;
			}

			pkcs7.parseSignedEnvelopString(signData);
			SignerInfo[] signer = pkcs7.getSigners();
			
			if (signer == null)
			{
				error("Unable to get signer");
				return null;
			}

			debug("get signer info = " + signer.length);

			//取得目前時間日期
			SimpleDateFormat sdf = new SimpleDateFormat ("", Locale.US); 
			Date today = new Date();
			sdf.applyPattern(TIME_PATTERN);
			String transID = sdf.format(today);

			String rSize = ""; 

			LinkedList list = new LinkedList();
			list.clear(); 

			for(int i=0; i < signer.length; i++)
			{
				//假設最多只有 999
				rSize = "000";
				rSize = rSize + String.valueOf(i);
				rSize = rSize.substring(rSize.length() - 3);
				pkcs7.setTransId(transID+ rSize);
				pkcs7.trnsId = transID + rSize;
				log.debug("@@@SecSpi.java pkcs7.trnsId one inside ==>"+transID + rSize);
				//驗證不過就 exception ...
				pkcs7.verify(signer[i]);

				CertInfo cert = new CertInfo( Base64.decode( signer[i].getSignerCert() ) );
				X500NameStruct x500 = cert.getSubjectName();

				//加入串列
				list.add(x500.getX500NameString());    			
			}
			pkcs7.getDataContentString();

			String names = "";
			//以 | 分隔串在一起
			for(int i=0; i < list.size(); i++)
			{
				names = names + (String) list.get(i) + "|" ;
			}

			return names;
		}
		catch(Exception e)
		{
			error(e);
		}
		finally
		{
		}
		return null;
	}

	public String doVerifySign(String signData, String jsonDc)
	{
		try
		{    		
			PKCS7SignatureProc pkcs7 = new PKCS7SignatureProc();
			SignerInfo[] signer = null;
			if (pkcs7 == null)
			{
				error("Unable create PKCS7SignatureProc");
				return null;
			}
			try{
			pkcs7.parseSignedEnvelopString(signData);
			log.debug("---- parseSignedEnvelopString ");
			signer = pkcs7.getSigners();
			}catch(Exception e){
				log.debug("@@@signer two inside ==>"+signer);
				log.error("", e);
			}
			
			log.debug(" --- ckSigner ---");
			if (signer == null)
			{
				error("Unable to get signer");
				return null;
			}

			debug("get signer info = " + signer.length);

			//取得目前時間日期
			SimpleDateFormat sdf = new SimpleDateFormat ("", Locale.US); 
			Date today = new Date();
			sdf.applyPattern(TIME_PATTERN);
			String transID = sdf.format(today);

			String rSize = ""; 

			LinkedList list = new LinkedList();
			list.clear(); 

			for(int i=0; i < signer.length; i++)
			{
				
				// show signer message
				log.debug("Signer #" + (i+1));
				log.debug("**** info ****");
				log.debug("1. DigestMethod: " + signer[i].getDigestMethod());
				log.debug("2. SignatureMethod: " + signer[i].getSignatureMethod());
				log.debug("3. getSignerCert: " + signer[i].getSignerCert());
				//假設最多只有 999
				rSize = "000";
				rSize = rSize + String.valueOf(i);
				rSize = rSize.substring(rSize.length() - 3);
				
				//pkcs7.trnsId = transID + rSize;
				//pkcs7.setTransId(rSize);
				pkcs7.setTransId("0004");
				log.debug("@@@SecSpi.java pkcs7.trnsId two inside ==>"+rSize);
				log.debug("@@@SecSpi.java pkcs7.setTransId(0004) two inside ==>"+"0004");
//				log.debug("@@@SecSpi.java signer[i] two inside ==>"+signer[i]);
				//驗證不過就 exception ...
				pkcs7.verify(signer[i]);
				log.debug(" ========= pkcs7.verify is suceess!! ========= ");
				CertInfo cert = new CertInfo( Base64.decode( signer[i].getSignerCert() ) );
				log.debug(" ========= decode done ========= ");
				X500NameStruct x500 = cert.getSubjectName();
				log.debug(" ========= cert.getSubjectName() done ========= ");
				//加入串列
				list.add(x500.getX500NameString());
				log.debug(" ========= list.add done ========= ");
			}
			pkcs7.getDataContentString();

			//由isecurity取回壓碼值與前台頁面壓傳入的壓碼值做比較
			log.debug("==============================================SecSpi==============================================");
			//log.debug("pkcs7.getDataContentString ==" + pkcs7.getDataContentString());
			//log.debug("jsonDc ==" + jsonDc);
			if(jsonDc.trim().equals(pkcs7.getDataContentString().trim())){
				//log.debug("pkcs7.getDataContentString() and jsonDc are the same!");
			} else {
				//log.debug("pkcs7.getDataContentString() and jsonDc are the different!");
			}    		
			//log.debug("==============================================SecSpi==============================================");

			String names = "";
			//以 | 分隔串在一起
			for(int i=0; i < list.size(); i++)
			{
				names = names + (String) list.get(i) + "|" ;
			}

			return names;
		}
		catch(Exception e)
		{
			log.debug("pkcs7.verify has error ===========>"+e);
			error(e);
		}
		finally
		{
		}
		return null;
	}

	public String doChangeKey(String mastKeyFrame, String workKeyFrame, String keyData, String data)
	{
		try
		{
			//先解開以 CD key 押密的 work key 
			String workKey = doDecrypt(mastKeyFrame, keyData);
			if (workKey == null)
			{
				error("DECRYPT WORK KEY ERROR");
				return null;
			}

			debug("work key = " + workKey);

			//注意： data 包含 sync data 及 要驗證的 sync
			// sync data 長度為 16 bytes , sync check 長度 16 bytes

			String syncData = data.substring(0, 16);
			String syncCheck = data.substring(16, 32);

			debug("sync data = " + syncData);
			debug("sync check = " + syncCheck);

			//public byte[] encryptByExplictKey(String encMethod, byte[] keyValue, byte[] sourceData, int startPos, int length)
			byte [] newKey = pack(workKey);
			byte [] checkData = pack(syncData);
			byte [] bCheck = SecurityDes.encryptByExplictKey("DES2", newKey, checkData, 0, checkData.length);
			if (bCheck == null)
			{
				error("generate sync error");
				return null;
			}

			checkData = unpack(bCheck);

			String check = new String(checkData);

			debug("generated sync = " + check);

			if (check.equals(syncCheck))
			{
				debug("sync check OK");
				//驗證完成後，更新基碼
				check = doGenKey(mastKeyFrame, workKeyFrame, workKey);
				if (check == null)
				{
					error("Update work key error");
					return null;
				}

				return check;
			}
			else
			{
				debug("sync check ERROR");
			}

		}
		catch(Exception e)
		{
			error(e);
		}
		finally 
		{
		}
		return null;
	}

	/**
	 * 將16進位資料壓縮為二進位值
	 * @param  hexStr    16進位字串
	 * @return byte []   壓縮後的二進位值資料陣列
	 */        
	public static byte [] pack(String hexStr)
	{
		int size = hexStr.length();

		if ( (size % 2) != 0 ) 
		{
			debug("error size = " + size);
			return null; 
		}

		int [] intData = new int[size / 2];
		try
		{
			int j = 0;
			for(int i = 0; i < size; i += 2 )
			{
				intData[j] =  Integer.parseInt(hexStr.substring(i, i + 2), 16) ;
				j++;
			}

			byte [] outputByte = new byte[intData.length];          
			for(int x = 0; x < intData.length; x++)
			{
				outputByte[x] = (byte) intData[x];
			}
			return outputByte;
		}
		catch(Exception e)
		{
			debug( e );
		}
		return null;
	}


	/**
	 * 將二進位資料展開為 16 進位值
	 * @param  bin       二進位資料陣列
	 * @return byte []   展開後的16 進位值資料陣列
	 */        
	public static byte[] unpack(byte[] bin)
	{
		int size = bin.length;        
		byte[] hex = new byte[2 * size];

		int i = 0;
		for (int j = 0; i < size; )
		{
			int rawByte = bin[i];
			if (rawByte < 0) rawByte += 256;

			int nibble = rawByte >> 4;
			if (nibble < 10)
			{
				hex[j] = (byte)(48 + nibble);  // 0 ~ 9
			}   
			else
			{
				hex[j] = (byte)(65 + nibble - 10);  // A ~ F
			}

			nibble = rawByte & 0x0000000F;
			if (nibble < 10)
			{
				hex[(j + 1)] = (byte)(48 + nibble);
			}    
			else
			{
				hex[(j + 1)] = (byte)(65 + nibble - 10);
			}    

			i++;
			j += 2;  //展開成雙倍長度
		}

		return hex;
	}

//	public static void main (String args []) 
//	{
//		//log4j initialize
//		PropertyConfigurator.configure(DEBUG_CONFIG);
//
//		try
//		{
//
//			SecSpi secSpi = new SecSpi();  
//
//			if (args.length > 0)
//			{
//				try
//				{
//					File file = new File(args[0]);
//
//					debug("FILE SIZE = " + file.length());
//
//					byte [] data = new byte[(int) file.length()];
//
//					FileInputStream fs = new FileInputStream(file);
//					fs.read(data); 
//					fs.close();
//
//					debug ("SIGN RESULT = " + secSpi.doVerifySign(new String(data)) );
//
//				}
//				catch(Exception e)
//				{
//					error(e);
//				}
//			}
//
//			//if (args.length > 0 && args[0].equals("1"))
//			//{
//			debug("SPI 1= " + secSpi.doGenKey("B05000NNBTKCD", "B05000NNBTKPP", "701ED7DD53580A7CD11DB42461D63CEA"));
//			debug("SPI 2= " + secSpi.doGenKey("B05000NNBTKCD", "B05000NNBTMAC", "701ED7DD53580A7CD11DB42461D63CEA"));
//			//}
//
//			debug("SPI 3= " + secSpi.doRequest("0002B05000NNBTKPP                                   00000000000000000032701ED7DD53580A7CD11DB42461D63CEA") );
//
//			SecurityUtil s = new SecurityUtil();
//			byte[] abc = s.encrypt("DES2", "B05000NNBTKPP", CDKey.getBytes(), 0, CDKey.length());
//
//			debug("BEFORE ENC = " + CDKey);
//			debug("AFTER  ENC = " + new String(unpack(abc)));
//		}
//		catch(Exception e)
//		{
//			debug(e.getMessage());
//			e.printStackTrace();
//		}
//		Runtime.getRuntime().exit(0);    
//	}    //main
}
