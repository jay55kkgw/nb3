package tw.com.top.ebank;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.owasp.esapi.SafeFile;

import lombok.extern.slf4j.Slf4j;

/**
 * <code>RFXConfigModel</code> 
 * 用來管理系統設定參數.
 * <p>
 * 使用者可以經由產生有預設值的設定檔，來執行工作。或者，修改設定檔的內容再執行。
 * 可以利用預設的設定檔來回復預設值。
 * 目前版本使用 java.util.ResourceBundle 來取得設定資料。
 * 使用記憶體來暫存設定值。
 * 產生新的設定檔時，使用一般的檔案IO函式，將記憶體中的設定值匯出至檔案中。
 * <p>
 * <p>
 * <p> <hr/>
 * <p>
 * @version 1.0
 * @author Andy Lee
 * @see RFXDataModel
 */
@Slf4j
public class RFXConfigModel
{
    private static final String DEBUG_CONFIG = "KeySvr.properties";
//    static Logger log = Logger.getLogger(RFXConfigModel.class);  

    /**
     * 設定檔名稱
     */    
    public static final String CFG_FILE = "KeySvrCfg";
    
    /**
     * 預設設定檔名稱
     */    
    public static final String DEFAULT_CFG_FILE = "KeySvrDefault";
        
    
    /**
     * 設定的節點元素
     * <p> AliveTime 以秒為單位
     * <p> UpdateTime 以毫秒為單位
     * <p> MemoryLimit 為閒置記憶體的百分比
     * <p> DispatchTime 以毫秒為單位
     */                     
    public static final String NODE_ITEM [] = {
    	                                           "SvrIP",
                                                   "TMPort", 
                                                   "ADMPort",
                                                   "TimeOutSeconds",
                                                   "QueueLimit",
                                                   "MemoryLimit",
                                                   "RetryLimit",
                                                   "Debug"
                                               };

    /**
     * 元素預設值
     */                     
    public static final String ITEM_DEFAULT [] = {
    	                                              "127.0.0.1",
                                                      "6001",  
                                                      "18131",
                                                      "30",
                                                      "10000",
                                                      "0.3",
                                                      "3",
                                                      "1"
                                                  };

    
    /**
     * DataModel 的資料進入點
     */
    private static Map ConfigData = null;

    /**
     * 設定檔讀取物件
     */
    private static ResourceBundle ConfigBundle = null;

    /**
     * 設定檔名稱
     */
    private static String ConfigFileName = CFG_FILE ;
    

    /**
     * 設定檔編碼
     */
    public static final String CFG_ENCODING = "ISO-8859-1";
    
    /**
     * Contructor or unnamed block
     * 建立類別層級的物件
     *
     */    
    static
    {        
        if (ConfigData == null)
        {                    
            //產生資料進入點
            ConfigData = Collections.synchronizedMap(new HashMap());                        
        }        
    }
       
    /**
     * Constructor
     * 使用預設的設定檔名稱
     */    
    public RFXConfigModel()
    {
    	
    }       

    /**
     * Constructor
     * 使用自訂的設定檔名稱
     */    
    public RFXConfigModel(String fileName)
    {
    	ConfigFileName = fileName;
    }       
    
    /**
     * 工具函式: 輸出除錯訊息
     * @param s      除錯訊息
     * 
     */        
    public static void debug(String s)
    {
        //log.debug(s);
        log.debug(s);
    }

    /**
     * 工具函式: 輸出除錯訊息
     * @param e      除錯訊息
     * 
     */        
    public static void debug(Exception e)
    {
        //log.debug("" + e);    
        log.debug("" + e);
    }

    /**
     * 工具函式: 輸出錯誤訊息
     * @param s      錯誤訊息
     * 
     */        
    public static void error(String s)
    {
        //log.debug(s);    
        log.error(s);
    }

    /**
     * 工具函式: 輸出錯誤訊息
     * @param e      錯誤訊息
     * 
     */        
    public static void error(Exception e)
    {
        //log.debug("" + e);    
        log.error("" + e);
    }
    
    /**
     * 工具函式: 將設定資料匯出到 設定 檔案中
     * @param   p         要匯出的設定資料
     * @param   fileName  匯出檔名
     * @return  boolean   成功為 true , 失敗為 false 
     * @throws  無
     * 
     */        
    public static boolean exportToFile(Map m, String fileName) 
    {
        try 
        {

            //FileOutputStream fileWriter = new FileOutputStream(fileName);
           // OutputStreamWriter outputWriter = new OutputStreamWriter(fileWriter, CFG_ENCODING);
        	File tmpFile = new SafeFile(fileName);
    		tmpFile.setWritable(true,true);
    		tmpFile.setReadable(true, true);
    		tmpFile.setExecutable(true,true);
        	//Path p = tmpFile.toPath();
            synchronized (m)
            {            
                Set st = m.keySet();
                Iterator iter = st.iterator();                                    
                while (iter.hasNext())        
                {
                
                    String key = (String) iter.next();
                    String value = (String) m.get(key);
                    String line = key + " = " + value + "\n";
                    
                 //  outputWriter.write( line, 0, line.length() );
                  //  Files.write(p,line.getBytes("BIG5"));
                    BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(tmpFile));
            	    byte[] strToBytes = line.getBytes("BIG5");
            	    outputStream.write(strToBytes,0,strToBytes.length);
            	    outputStream.close();
                }            
            }
            
            //outputWriter.close();
           // fileWriter.close();

        } 
        catch (Exception e) 
        {
            error("exportToFile:" + e);
            return false;
        }
        return true;
    }
                

    /**
     * 工具函式: 將設定資料由 xml 檔案匯入
     * @param   fileName  匯入檔名
     * @return  boolean   成功為 true , 失敗為 false 
     * @throws  無
     * 
     */        
    public static boolean importFromFile(String fileName)
    {
        try 
        {
        	ConfigFileName = fileName;
        	ConfigBundle = ResourceBundle.getBundle(ConfigFileName);
            
            //以 ConfigData 來同步存取
            synchronized (ConfigData)
            {
            	ConfigData.clear();            
                Enumeration en  = ConfigBundle.getKeys();
                while ( en.hasMoreElements() )        
                {               
                    String key = (String) en.nextElement();
                    String value =  ConfigBundle.getString(key);
                    ConfigData.put(key, value);
                }                            
            }
        } 
        catch (Exception e) 
        {
            error("importFromFile:" + e);
            return false;
        }
        return true;
    }

    /**
     * 將設定值清除
     * <p>
     * @return boolean   成功為 true , 失敗為 false 
     */        
    public static boolean resetConfig()
    {
        try
        {
        	if (ConfigData != null)
        	{
        		ConfigData.clear();
        	}   
        }
        catch(Exception e)
        {
            error("resetConfig:" + e);
            return false;
        }
        return true;        
    }
        
    /**
     * 建立預設的設定檔格式
     * @return boolean   成功為 true , 失敗為 false 
     */        
    public static boolean createDefaultConfig()
    {
        
        try
        {   
            Map m = new HashMap();
                                 
            //生成項定項目
            for(int j = 0; j < NODE_ITEM.length; j++)
            {
                m.put(NODE_ITEM[j], ITEM_DEFAULT[j]);
            }                
                          
            //輸出到預設的設定檔
            //需要加上副檔名              
            if (! exportToFile(m, DEFAULT_CFG_FILE + ".properties") )
            {
            	error("createDefaultConfig: error");
            	return false;
            }
        }
        catch(Exception e)
        {
            error("createDefaultConfig:" + e);
            return false;
        }
        return true;
    }
    
    /**
     * 載入預設值
     * @return boolean   成功為 true , 失敗為 false 
     */           
    public static boolean loadDefaultConfig()
    {
        File dir1 = new File (".");
        dir1.setWritable(true,true);
        dir1.setReadable(true, true);
        dir1.setExecutable(true,true);
        File file = new File(DEFAULT_CFG_FILE + ".properties");
        file.setWritable(true,true);
        file.setReadable(true, true);
        file.setExecutable(true,true);
        try
        {   
            debug("Current dir : " + dir1.getCanonicalPath());
            if (!file.exists())
            {
                error("loadDefaultConfig:" + DEFAULT_CFG_FILE + ".properties" + " " + "不存在");
                return false;
            }
            //將預設值由檔案中載入
            if ( ! importFromFile(DEFAULT_CFG_FILE) )
            {
                error("loadDefaultConfig: error ");
                return false;            	
            }
        }
        catch(Exception e)
        {                
            error("loadDefaultConfig:" + e);
            return false;
        }
        return true;    
    }

    /**
     * 使用內定的檔名載入使用者的設定值
     * @return boolean   成功為 true , 失敗為 false 
     */           
    public static boolean loadUserConfig()
    {
        File dir1 = new File (".");
        dir1.setWritable(true,true);
        dir1.setReadable(true, true);
        dir1.setExecutable(true,true);
        File file = new File(CFG_FILE + ".properties");
        file.setWritable(true,true);
        file.setReadable(true, true);
        file.setExecutable(true,true);
        try
        {
            debug("Current dir : " + dir1.getCanonicalPath());
            if (!file.exists())
            {
                error("loadConfig:" + CFG_FILE + ".properties" + " " + "不存在");
                return false;
            }
               
            //將預設值由檔案中載入
            if ( ! importFromFile(CFG_FILE) )
            {
                error("loadConfig: error ");
                return false;            	
            }
        }
        catch(Exception e)
        {                
            error("loadConfig:" + e);
            return false;
        }
        return true;    
    }

    /**
     * 使用指定的檔名載入使用者的設定值
     * @param   pathName  使用者指定的設定檔路徑及名稱
     * @return  boolean   成功為 true , 失敗為 false 
     */           
    public static boolean loadUserConfig(String pathName)
    {
        File dir1 = new File (".");
        dir1.setWritable(true,true);
        dir1.setReadable(true, true);
        dir1.setExecutable(true,true);
        File file = new File(pathName + ".properties");
        file.setWritable(true,true);
        file.setReadable(true, true);
        file.setExecutable(true,true);
        try
        {
            debug("Current dir : " + dir1.getCanonicalPath());
            if (!file.exists())
            {
                error("loadUserConfig:" + pathName + ".properties" + " " + "不存在");
                return false;
            }
               
            //將預設值由檔案中載入
            if ( ! importFromFile(pathName) )
            {
                error("loadUserConfig: error ");
                return false;
            }
        }
        catch(Exception e)
        {                
            error("loadUserConfig:" + e);
            return false;
        }
        return true;    
    }

    
    /**
     * 自動載入設定值
     * 當使用者自訂的設定檔存在時，載入使用者自訂的設定檔
     * 當使用者自訂的設定檔不存在時，載入系統預設的設定檔
     * 兩者均失敗，則失敗
     * @return boolean   成功為 true , 失敗為 false 
     */           
    public static boolean loadConfig()
    {
        boolean ret;
        ret = loadUserConfig();
        if (!ret)
        {
            ret = loadDefaultConfig();
            if (!ret)
            {
                return ret;
            }
        }
        return ret;
    }
    
    /**
     * 自動載入設定值
     * 當使用者自訂的設定檔存在時，載入使用者自訂的設定檔
     * 當使用者自訂的設定檔不存在時，載入系統預設的設定檔
     * 兩者均失敗，則失敗
     * @return boolean   成功為 true , 失敗為 false 
     */           
    public static boolean loadConfig(String pathName)
    {
        boolean ret = false;
        
        if (pathName == null) return ret;
        
        ret = loadUserConfig(pathName);
        if (!ret)
        {
            ret = loadConfig();
            if (!ret)
            {
                return ret;
            }
        }
        return ret;
    }
        
        
    /**
     * 指定節點及設定項目以取得設定值
     * @param   nodeName   節點名稱  (目前無作用)
     * @param   key        節點元素(設定項目)
     * @return  String     設定值
     */           
    public static String getConfig(String nodeName, String key)
    {
        if (ConfigData == null) return null;

        return (String) ConfigData.get( key );        
    }

    /**
     * 指定節點及設定項目以取得設定值，若設定項目不存在則使用輸入的預設值
     * @param   nodeName   節點名稱  (目前無作用)
     * @param   key        節點元素(設定項目)
     * @param   value      預設值
     * @return  String     設定值
     */           
    public static String getConfig(String nodeName, String key, String value)
    {
        if (ConfigData == null) return null;
        String t = (String) ConfigData.get ( key );
        if ( t == null ) 
        {
            t = value;
        }
        else if ( t.equals("") )
        {
        	t = value;
        }
        
        return t;        
    }
    
    /**
     * 取得節點之下一連串相關的設定值
     * @param  nodeName   節點名稱 (目前無作用)
     * @return Map    設定值的集合
     */           
    public static Map getConfigList(String nodeName)
    {                                                                 
        return ConfigData;                                                  
    }
    
           
    /**
     * 單元測試
     *
     */           
//    public static void main(String argv [])
//    {
//
//        //log4j initialize
//        PropertyConfigurator.configure(DEBUG_CONFIG);
//        
//        createDefaultConfig();
//        
//        loadUserConfig();
//
//        loadConfig("不存在");
//
//        loadConfig();
//
//        loadDefaultConfig();
//                                                      
//        String msg = "";
//                             
//        msg = RFXConfigModel.getConfig("RMDS", "TMPort");
//        debug("msg = " + msg);
//                                                                            
//        Map map = getConfigList("RIC");            
//        Set st = map.keySet();
//        Iterator iter = st.iterator();            
//        while (iter.hasNext())        
//        {
//            String key = (String) iter.next();
//            String value = (String) map.get(key);
//            debug("GET = " + key + " " + value);
//            
//        }
//        
//        exportToFile(map, CFG_FILE + ".properties" );
//                
//        resetConfig();        
//    }
    
}