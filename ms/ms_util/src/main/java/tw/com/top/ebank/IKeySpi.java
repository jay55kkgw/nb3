package tw.com.top.ebank;

/**
 * <code>IKeySpi</code>
 * 謎之界面
 * <p>
 * <p>主要用來處理未知的處理程序；實作者與使用者依照定義的傳回值來處理
 *    該處理的程序
 * <p>
 * @version 1.0
 * @author Andy Lee
 */ 
public interface IKeySpi
{
    //---- 功能代碼 ----
    //產生 MAC
    public static final String CREATE_MAC         = "0001";
    //產生 SYNC
    public static final String CREATE_SYNC        = "0002";
    //產生 KEY
    public static final String CREATE_KEY         = "0003";
    //加密資料
    public static final String ENCRYPT_DATA       = "0004";
    //簽章檢核
    public static final String VERIFY_SIGN        = "0005";
    //解密資料
    public static final String DECRYPT_DATA       = "0006";
    //變更基碼
    public static final String CHANGE_KEY         = "0007";

    //---- 處理代碼----
    //處理成功
    public static final String RC_OK              = "01";
    //處理失敗
    public static final String RC_ERR             = "02";
    
    /**
     * 謎之界面處理程序
     * @param   req          傳入的字串資料
     * @return  boolean      傳回傳送結果， true 成功 ， false 失敗
     */
    public boolean putRequest(String req);

    /**
     * 謎之界面處理程序
     * @return  String      傳回處理結果
     */    
    public String getResponse();
    
    /**
     * 謎之界面處理程序
     * @param   req          傳入的字串資料
     * @return  String       傳回傳送結果
     */
    public String doRequest(String req);

}
