package tw.com.top.ebank;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import lombok.extern.slf4j.Slf4j;

/**
 * <code>Dump</code>
 * 將 byte array 以 16 進位顯示 
 * <p>
 * @version 1.0
 * @author Andy Lee
 */
@Slf4j
public class Dump
{
    /**
     * 每行的前置字串
     */
    String sMargin;
    
    /**
     * 要顯示的 ascii 字串
     */    
    String sAscii;
        
    /**
     * 輸入串流 
     */    
    InputStream isData;
    int iByteInLine;
    int iByteInCluster;
    int iLinesInCluster;
    long lOffset;
    long lStartOffset;
    long iLength;
               
    /**
     * Constructor
     */                   
    public Dump ()
    {
      sAscii = "";
      iByteInLine = 0;
      iByteInCluster = 0;
      iLinesInCluster = 0;
      lStartOffset = 0;
    }

    /**
     * Constructor
     * @param    abIn    輸入的 byte 陣列
     */    
    public Dump (byte[] abIn)
    {
        this (new ByteArrayInputStream (abIn), "    ");
    }

    /**
     * Constructor
     * @param    abIn         輸入的 byte 陣列
     * @param    iLengthIn    要顯示的長度
     */    
    public Dump (byte[] abIn, int iLengthIn)
    {
        this (new ByteArrayInputStream (abIn), iLengthIn, "    ");        
    }
    
    /**
     * Constructor
     * @param    is           輸入串流
     * @param    sMarginIn    每行的前置字串
     */    
    public Dump (InputStream is, String sMarginIn)
    {
        this (is, Integer.MAX_VALUE, sMarginIn);
    }

    /**
     * Constructor
     * @param    is           輸入串流
     * @param    iLengthIn    要顯示的長度
     * @param    sMarginIn    每行的前置字串
     */    
    public Dump (InputStream is, int iLengthIn, String sMarginIn)
    {
        this ();
        isData = is;
        iLength = iLengthIn;
        sMargin = sMarginIn;
    }

    /**
     * Constructor
     * @param    is           輸入串流
     * @param    iLengthIn    要顯示的長度
     * @param    offset       要顯示的起始位置
     * @param    sMarginIn    每行的前置字串
     */    
    public Dump (InputStream is, int iLengthIn, long offset, String sMarginIn)
    {
        this ();
        isData = is;
        lStartOffset = offset;
        iLength = iLengthIn;
        sMargin = sMarginIn;
    }


    public void dump (OutputStream osIn)
    {
        int i;
        int iByte;
        StringBuffer sb;

        if (iLength <= 0) return;
        try
        {
            i = 0;
            lOffset = -1;
            sb = new StringBuffer ();
            newLine (sb, true);

            while ((iByte = isData.read ()) != -1)
            {
                if (lStartOffset > 0 && lOffset <= lStartOffset) 
                {
                    lOffset++;
                    continue;
                }
                                
                lOffset++;
                if (lOffset % 4096 == 0)  //buffer limit: 4096
                {
                    osIn.write (sb.toString().getBytes ());
                    sb = new StringBuffer ();
                }
                
                //write byte content
                byteOut (sb, iByte);
                if (++i >= iLength)
                {
                    break;
                }    
            }

            lastLine (sb);
            sb.append ("\n");
            osIn.write (sb.toString().getBytes ());
        }
        catch (Exception e)
        {
            log.debug ("dump(): " + e);
        }
    }


    public String toString ()
    {
        int i;
        int iByte;
        StringBuffer sb;

        if (iLength <= 0) return ("\n");
        try
        {
            i = 0;
            lOffset = -1;
            sb = new StringBuffer ();
            newLine (sb, true);

            while ((iByte = isData.read ()) != -1)
            {
                if (lStartOffset > 0 && lOffset != lStartOffset) 
                {
                    lOffset++;
                    continue;
                }

                lOffset++;
                byteOut (sb, iByte);
                if (++i >= iLength) break;
            }

            lastLine (sb);
            sb.append ("\n");
            return (sb.toString ());
        }
        catch (Exception e)
        {
            log.debug ("toString(): " + e);
        }
        return ("** dump error **");
    }


    void byteOut (StringBuffer sbIn, int iIn)
    {
        sbIn.append (toHexWidth (iIn , 2));
        
        //visible characters
        if ((iIn > 31) && (iIn < 128))
        {
            sAscii = sAscii + (char) iIn;
        }
        else
        {
            sAscii = sAscii + ".";
        }

        if (++iByteInCluster >= 2)
        {
            sbIn.append (" ");
            iByteInCluster = 0;
        }

        if (((iByteInLine + 1) % 8) == 0)
        {
            sbIn.append (" "); 
        }

        if (++iByteInLine >= 16)
        {
            newLine (sbIn, false);
        }
    }


    void newLine (StringBuffer sbIn, boolean bFirstLine)
    {
        if (!bFirstLine)
        {   
            //fill out the line 
            while (sAscii.length () < 16) 
            {
                sAscii = sAscii + " ";
            }
            sbIn.append ("   |" + sAscii + "|\n");
            iLinesInCluster++;
        }
        
        
        //one cluster is contain 8 lines
        if (iLinesInCluster >= 8)
        {
            sbIn.append ("\n");
            iLinesInCluster = 0;
        }
        
        sbIn.append (sMargin + toHexWidth (lOffset + lStartOffset + 1, 8) + ":  ");
        iByteInLine = 0;
        sAscii = "";
    }


    void lastLine (StringBuffer sbIn)
    {
        if (iByteInLine < 8)  sbIn.append (" ");
        if (iByteInLine < 16) sbIn.append (" ");

        while (++iByteInLine <= 16)
        {
            sbIn.append ("  ");
            //separate two bytes with one space
            if ((iByteInLine % 2) == 0) sbIn.append (" ");
        }
        
        //fill out
        while (sAscii.length () < 16) 
        {
            sAscii = sAscii + " ";
        }

        sbIn.append ("   |" + sAscii + "|\n");
        iByteInLine = 0;
        sAscii = "";
    }


    String toHexWidth (long lIn, int iWidth)
    {
        return (toHexWidth ((int) lIn, iWidth));
    }

    /**
     * Convert number to hex string
     */
    String toHexWidth (int iIn, int iWidth)
    {
        StringBuffer sbAnswer;
        int i;
        char c;
        int iWk;
        int iNibbleMask = 0x0000000f;

        sbAnswer = new StringBuffer ("");
        for (i = 0; i < iWidth; i++)
        {
            iWk = iIn >> (i * 4);
            iWk = iWk & iNibbleMask;
            c = (char) iWk;
            if (c < 10)
            {
                c += '0';
            }
            else
            {
                c = (char) (c - 10 + 'A');
            }
            sbAnswer.insert (0, c);
        }
        return (sbAnswer.toString ());
    }


//    public static void main (String[] args)
//    {
//        Dump dmp;
//        File f;
//        FileInputStream fis;
//        String s;
//        SimpleDateFormat sdf;
//
//        if (args.length < 1)
//        {
//            log.debug ("Usage: Dump filename [length] [offset]");
//            System.exit (1);
//        }
//
//        f = new File (args[0]);
//        try
//        {
//            String margin = "    ";
//            long  offset = 0;
//            int   dumpSize = -1;
//            
//            fis = new FileInputStream (f);
//
//            if (args.length == 2)
//            {
//                dumpSize = Integer.parseInt(args[1]);
//                dmp = new Dump (fis, dumpSize, margin);
//            }    
//            else if (args.length == 3)
//            {
//                dumpSize = Integer.parseInt(args[1]);
//                offset = Long.parseLong(args[2]);
//                dmp = new Dump (fis, dumpSize, offset, margin);                
//            }
//            else 
//            {
//                dmp = new Dump (fis, margin);                
//            }
//
////            log.debug ("\n\n");
//            s = margin + "FILE = " + args[0]; //f.getName ();
//            while (s.length () < 37) s = s + " ";
//            sdf = new SimpleDateFormat ("MM/dd/yyyy hh:mm:ss a"); // format date str
//            s = s + "DATE: " + sdf.format (new Date (f.lastModified ()));
////            log.debug (s);
//
////            log.debug (margin + "LENGTH=" + dumpSize + " OFFSET = " + offset);
////            log.debug (margin + "--------------------------------" + "------------------------------------------\n");
//
//            dmp.dump (System.out);             
//            // 
//             //ByteArrayOutputStream bs = new ByteArrayOutputStream();
//             //dmp.dump ( bs );
//             //log.debug(bs.toString("ascii"));
//            //                
//              
//            fis.close ();
//        }
//        catch (Exception e)
//        {
//            log.debug ("Exception: " + e);
//        }
//    }
}
