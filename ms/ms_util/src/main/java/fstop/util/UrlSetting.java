package fstop.util;

import org.springframework.beans.factory.annotation.Value;

import lombok.Data;

@Data
public class UrlSetting {
	
	@Value("${mstw_Url}")
	String mstw_Url;
	
	@Value("${mscc_Url}")
	String mscc_Url;
	
	@Value("${msloan_Url}")
	String msloan_Url;
	
	@Value("${mspay_Url}")
	String mspay_Url;
	
	@Value("${msfx_Url}")
	String msfx_Url;
}
