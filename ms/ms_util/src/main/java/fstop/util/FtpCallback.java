package fstop.util;

import java.io.IOException;

import org.apache.commons.net.ftp.FTPClient;

public interface FtpCallback {

	public int doFtpAction(FTPClient ftpclient) throws IOException;
	
}
