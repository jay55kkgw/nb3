package fstop.util.exception;

import com.netbank.exception.UncheckedException;

public class ToRuntimeException extends UncheckedException {
	
	private static final long serialVersionUID = 1L;
	
	public ToRuntimeException(String msg) {
		super(msg);
	}
	
	public ToRuntimeException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
