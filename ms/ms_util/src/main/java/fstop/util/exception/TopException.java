/*
 * TopException.java
 * 將常見的Exceptions(SQL種類的)或是商業邏輯上發生的錯誤訊息包成TopException, 以供eTOPFA_methods判別之用
 * Created on 2002年9月26日, 下午 2:48
 */

package fstop.util.exception;

/**
 *
 * @author  guile
 * @version 
 */
public class TopException extends Exception{
    private String p_str_errCode;

    /** Creates new TopException */
    public TopException(String a_str_errCode,String a_str_msg) {
        super(a_str_msg);
        p_str_errCode = a_str_errCode;
    }

    public String getErrCode() {
        return p_str_errCode;
    }    

	// 2003.09.27 added
    public String getMessage() {
    	String	str_msg;
    	
		str_msg = super.getMessage();
		str_msg = str_msg.replace('\n', ' ');
		return str_msg;
    }
}
