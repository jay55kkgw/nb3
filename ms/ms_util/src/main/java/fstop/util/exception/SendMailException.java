package fstop.util.exception;

import com.netbank.exception.UncheckedException;

public class SendMailException extends UncheckedException { 
	public SendMailException(String msg) {
		super(msg);
	}
	public SendMailException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
