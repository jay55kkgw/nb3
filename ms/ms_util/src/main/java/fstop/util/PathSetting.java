package fstop.util;

public class PathSetting {
	private String tmpPath = "/usr/IBM/fstop/logs/temp";
	
	private String advPath = "/usr/IBM/fstop/adv"; //廣告放置路徑

	private String cfgPath = "/usr/IBM/fstop/cfg"; //cfg 放置路徑

	private String logsPath = "/usr/IBM/fstop/logs"; //logs路徑

	private String transferDataPath = "/usr/IBM/fstop/data";  //轉帳產生的暫存檔放的位置
	
	private String cachePath = "/usr/IBM/fstop/cache";  //Cache 檔放置位置
	
	public String getCachePath() {
		return cachePath;
	}

	public void setCachePath(String cachePath) {
		this.cachePath = cachePath;
	}

	public String getLogsPath() {
		return logsPath;
	}

	public void setLogsPath(String logsPath) {
		this.logsPath = logsPath;
	}

	public String getTransferDataPath() {
		return transferDataPath;
	}

	public void setTransferDataPath(String transferDataPath) {
		this.transferDataPath = transferDataPath;
	}

	public String getAdvPath() {
		return advPath;
	}

	public void setAdvPath(String advPath) {
		this.advPath = advPath;
	}

	public String getTmpPath() {
		return tmpPath;
	}

	public void setTmpPath(String tmpPath) {
		this.tmpPath = tmpPath;
	}

	public String getCfgPath() {
		return cfgPath;
	}

	public void setCfgPath(String cfgPath) {
		this.cfgPath = cfgPath;
	}
	
	
}
