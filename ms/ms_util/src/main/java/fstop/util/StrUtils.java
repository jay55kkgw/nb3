package fstop.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.netbank.util.ESAPIUtil;

public class StrUtils {
	public static boolean isEmpty(String s) {
		return s == null || s.length() == 0;
	}

	public static boolean isNotEmpty(String s) {
		return !(s == null || s.length() == 0);
	}

	public static String trim(String s) {
		if (isEmpty(s))
			return "";
		return s.trim();
	}

	public static String right(String str, int len) {
		if (str.length() <= len)
			return str;
		return str.substring(str.length() - len);
	}

	public static String left(String str, int len) {
		if (str.length() <= len)
			return str;
		return str.substring(0, len);
	}

	public static String repeat(String str, int len) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < len; i++) {
			sb.append(str);
		}
		return sb.toString();
	}

	/**
	 * 若傳入的 v 為空字串或者為null, 則回傳 default value
	 * 
	 * @param v
	 * @param defaultValue
	 * @return
	 */
	public static String nvl(String v, String defaultValue) {
		return (isEmpty(v) ? defaultValue : v);
	}

	/**
	 * 將 arys 加上 delim 變成 String
	 * 
	 * @param delim
	 * @param arys
	 * @return
	 */
	public static String implode(String delim, String[] arys) {
		if (arys == null || arys.length == 0)
			return "";

		StringBuffer result = new StringBuffer();

		for (String x : arys) {
			if (result.length() != 0)
				result.append(delim);
			result.append(x);
		}

		return result.toString();
	}

	public static String implode(String delim, Set<String> arys) {
		if (arys == null || arys.size() == 0)
			return "";

		StringBuffer result = new StringBuffer();

		for (String x : arys) {
			if (result.length() != 0)
				result.append(delim);
			result.append(x);
		}

		return result.toString();
	}

	public static String implode(String delim, List<String> arys) {
		if (arys == null || arys.size() == 0)
			return "";

		StringBuffer result = new StringBuffer();

		for (String x : arys) {
			if (result.length() != 0)
				result.append(delim);
			result.append(x);
		}

		return result.toString();
	}

	/**
	 * 字串複製, 最長 4096 bytes
	 */
	public static void copyStream(InputStream in, OutputStream out) throws IOException {
		byte[] buf = new byte[4096];
		int len;
		while ((len = in.read(ESAPIUtil.validateByteArray(buf, true))) != -1)
			out.write(buf, 0, len);
	}

	/**
	 * 字串複製, 最長 4096 bytes
	 */
	public static void copyStream(Reader in, OutputStreamWriter out) throws IOException {
		char[] buf = new char[4096];
		int len;
		while ((len = in.read(buf)) != -1)
			out.write(buf, 0, len);
	}

	/**
	 * 字串複製, 最長 4096 bytes
	 */
	public static void copyStream(InputStream in, OutputStreamWriter out) throws IOException {

		byte[] buf1 = new byte[4096];
		char[] buf2 = new char[4096];
		int len, i;
		while ((len = in.read(ESAPIUtil.validateByteArray(buf1, true))) != -1) {
			for (i = 0; i < len; ++i)
				buf2[i] = (char) buf1[i];
			out.write(buf2, 0, len);
		}
	}

	/**
	 * 字串複製, 最長 4096 bytes
	 */
	public static void copyStream(Reader in, OutputStream out) throws IOException {
		char[] buf1 = new char[4096];
		byte[] buf2 = new byte[4096];
		int len, i;
		while ((len = in.read(buf1)) != -1) {
			for (i = 0; i < len; ++i)
				buf2[i] = (byte) buf1[i];
			out.write(buf2, 0, len);
		}
	}

	public static String toHex(byte[] b) {
		StringBuffer hex = new StringBuffer();
		for (int i = 0; i < b.length; i++) {
			hex.append("" + "0123456789ABCDEF".charAt(0xf & b[i] >> 4) + "0123456789ABCDEF".charAt(b[i] & 0xf));
		}
		return hex.toString();
	}

	public static byte[] Hex2Bin(byte[] hex) {
		byte[] bin = new byte[hex.length / 2];
		for (int i = 0, j = 0; i < bin.length; i++, j += 2) {
			int iL = hex[j] - '0';
			if (iL > 9) {
				iL -= 7;
			}
			iL <<= 4;
			//
			int iR = hex[j + 1] - '0';
			if (iR > 9) {
				iR -= 7;
			}
			bin[i] = (byte) (iL | iR);
		}
		return bin;
	}

	public static byte[] hexToByte(String hexStr) {
		byte[] bts = new byte[hexStr.length() / 2];
		for (int i = 0; i < bts.length; i++) {
			bts[i] = (byte) Integer.parseInt(hexStr.substring(2 * i, 2 * i + 2), 16);
		}
		return bts;
	}

	/**
	 * 左補零
	 * 
	 * @param org
	 * @param newLength
	 * @return
	 */
	public static String padZeroLeft(String org, int newLength) {
		return padOnLeft(org, (byte) 0x30, newLength);
	}

	/**
	 * 左補滿 當 newLength 的值小於 輸入字串長度時，回傳原有字串
	 * 
	 * @param org
	 *            原有的字串
	 * @param pad
	 *            要補滿的字元(byte)
	 * @param newLength
	 *            長度
	 * @return 補滿的字串
	 */
	public static String padOnLeft(String org, byte pad, int newLength) {
		if (org.length() > newLength) {
			return org;
		}

		byte[] newArr = new byte[newLength];

		Arrays.fill(newArr, pad);

		byte[] orgByteArr = org.getBytes();
		System.arraycopy(orgByteArr, 0, newArr, newArr.length - orgByteArr.length, orgByteArr.length);

		return new String(newArr);
	}

	public static Set<String> splitToSet(String delim, String value) {
		return new HashSet(Arrays.asList(value.split(delim)));
	}

	public static List<String> splitToList(String delim, String value) {
		List<String> result = new ArrayList();
		String[] ary = StrUtils.trim(value).split(delim);
		if (ary == null || ary.length == 0)
			return new ArrayList();
		for (String s : ary) {
			result.add(s);
		}
		return result;
	}

	// 遮蔽帳號：遮7、8、9碼(含信託帳號)
	public static String hideaccount(String originalStr) {
		String header = "";
		String middle = "";
		String tailer = "";
		String newstr = "";
		if (originalStr.length() > 0) {
			if (originalStr.length() == 9) {
				header = originalStr.substring(0, 6);
				middle = "***";
				tailer = "";
			} else if (originalStr.length() > 9) {
				header = originalStr.substring(0, 6);
				middle = "***";
				tailer = originalStr.substring(9);
			}
		}
		newstr = header + middle + tailer;
		return newstr;
	}

	/***
	 * 將字串按指定長度切割成子字串
	 * 
	 * @param src
	 *            被切割的字串
	 * @param length
	 *            子字串指定長度
	 * @return 子字串組
	 */
	public static String[] splitStringByLength(String src, int length) {
		// 檢查參數是否合法
		if (null == src || src.equals("")) {
			return null;
		}

		if (length <= 0) {
			return null;
		}

		int n = (src.length() + length - 1) / length; // 字串可以被切割成子字串的個數

		String[] split = new String[n];

		for (int i = 0; i < n; i++) {
			if (i < (n - 1)) {
				split[i] = src.substring(i * length, (i + 1) * length);
			} else {
				split[i] = src.substring(i * length);
			}
		}

		return split;
	}

	public static Integer countWordSpace(final String str) {
		int count =0;
		char character;
		for (int n = 0; n < str.length(); n++) {
            character = str.charAt(n);
            // 中文or全形算2
            if (Character.UnicodeBlock.of(character) != Character.UnicodeBlock.BASIC_LATIN || ! isHalfWidth(character)) {
            	count=count+2;
            	continue;
            }
            //半形算1
            count++;
        }
		
		return count;
	}
	
	/**
     * 判斷是否為半形
     *
     * @param character
     * @return
     */
    public static boolean isHalfWidth(char c)   {
        return '\u0000' <= c && c <= '\u00FF'
            || '\uFF61' <= c && c <= '\uFFDC'
            || '\uFFE8' <= c && c <= '\uFFEE' ;

    }

}
