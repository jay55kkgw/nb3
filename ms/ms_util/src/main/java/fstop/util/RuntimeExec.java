package fstop.util;

import java.io.File;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RuntimeExec {
//	private Logger logger = Logger.getLogger(getClass());

	private String outputString = "";

	private int exitVal;

	public RuntimeExec(String path, String command) {
		exec(path, command);
	}

	public String getOutputString() {
		return outputString;
	}

	private void exec(String path, String command) {

		try {
			exitVal=0;
			String osName = System.getProperty("os.name");
			log.debug("OS Name : " + osName);
			String[] cmd = new String[3];

			cmd = command.split(" ");

			Runtime rt = Runtime.getRuntime();
			System.out.print("Execing ==>");
			for(int i=0;i<cmd.length;i++)
			{
				System.out.print(cmd[i] + " ");
			}
			log.debug(" ");
			Process proc = rt.exec(cmd, null, new File(path));
			// any error message?
			StreamGobbler errorGobbler = new StreamGobbler(proc
					.getErrorStream(), "ERROR");

			// any output?
			StreamGobbler outputGobbler = new StreamGobbler(proc
					.getInputStream(), "OUTPUT");

			// kick them off
			errorGobbler.start();
			outputGobbler.start();

			// any error???
			exitVal = proc.waitFor();
			log.debug("ExitValue: " + exitVal);
			outputString = outputGobbler.getResult();
			if(errorGobbler.getResult().length()>0)
				exitVal=1;
			log.debug("ExitValue: " + exitVal);
			log.debug("RuntimeExec errorGobbler = " + errorGobbler.getResult());
			log.debug("RuntimeExec outputGobbler = " + outputString);
		} catch (Throwable t) {
			exitVal=1;
//			t.printStackTrace();
			throw new RuntimeException(t);
		}
	}
	public int getcommstat()
	{
		return(exitVal);
	}
}
