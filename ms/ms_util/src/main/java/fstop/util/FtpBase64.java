package fstop.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Hashtable;

import org.apache.log4j.Logger;

public class FtpBase64 {

	String HostIP="";
	String User="";
	String pw="";
	
	private Logger getLogger() {
		return Logger.getLogger(FtpBase64.class);
	}

	public FtpBase64(String ip)
	{
		Hashtable ipusrpw=new Hashtable();
		NetrcProperties nrcpro=new NetrcProperties();
		nrcpro.getUserPassword(ip, ipusrpw);
		HostIP=(String)ipusrpw.get("IP");
		User=(String)ipusrpw.get("USER");
		pw=(String)ipusrpw.get("PASSWD");
	}
	
	public int upload(final String folder, final String serverFileName, final FileInputStream fis)
	{
		try
		{
			getLogger().debug("HostIP = " + HostIP + "User = " + User );
			FtpTemplate ftpTemplate = new FtpTemplate(HostIP, new Integer("21"), User, pw);
			ftpTemplate.upload(folder, serverFileName, fis);
		}
		catch(Exception e)
		{
			return(1);
		}
		return(0);
	}
		
	public int upload_ASCII(final String serverFileName, final FileInputStream fis)
	{
		int rc=0;
		int i=serverFileName.lastIndexOf("/");
		String folder = serverFileName.substring(0, i);
		String FileName=serverFileName.substring(i+1).trim();
		getLogger().debug("folder = " + folder);
		getLogger().debug("Server FileName = " + FileName);
		try
		{
			getLogger().debug("ftp to host : HostIP = " + HostIP + "User = " + User );
			FtpTemplate ftpTemplate = new FtpTemplate(HostIP, new Integer("21"), User, pw);
			rc=ftpTemplate.upload_ASCII(folder, FileName, fis);
			getLogger().debug("ftp to host : upload return code = " + rc );
		}
		catch(Exception e)
		{
			getLogger().debug("ftp to host : upload FAIL " );
			return(1);
		}
		return(rc);
	}

	public int upload(final String serverFileName, final FileInputStream fis)
	{
		int rc=0;
		int i=serverFileName.lastIndexOf("/");
		String folder = serverFileName.substring(0, i);
		String FileName=serverFileName.substring(i+1).trim();
		getLogger().debug("folder = " + folder);
		getLogger().debug("Server FileName = " + FileName);
		try
		{
			getLogger().debug("ftp to host : HostIP = " + HostIP + "User = " + User );
			FtpTemplate ftpTemplate = new FtpTemplate(HostIP, new Integer("21"), User, pw);
			rc=ftpTemplate.upload(folder, FileName, fis);
			getLogger().debug("ftp to host : upload return code = " + rc );
		}
		catch(Exception e)
		{
			getLogger().debug("ftp to host : upload FAIL " );
			return(1);
		}
		return(rc);
	}
	
	public int upload(final String serverFileName, final String localFileName)
	{
		int rc=0;
		int i=serverFileName.lastIndexOf("/");
		String folder = serverFileName.substring(0, i);
		String FileName=serverFileName.substring(i+1).trim();
		getLogger().debug("folder = " + folder);
		getLogger().debug("Server FileName = " + FileName);
		try
		{
			getLogger().debug("ftp to host : HostIP = " + HostIP + "User = " + User );
			FtpTemplate ftpTemplate = new FtpTemplate(HostIP, new Integer("21"), User, pw);
			rc=ftpTemplate.upload(folder,FileName, new FileInputStream(new File(localFileName)));
			getLogger().debug("ftp to host : upload return code = " + rc );
		}
		catch(Exception e)
		{
			getLogger().debug("ftp to host : upload FAIL " ,e);
			return(1);
		}
		return(rc);
	}
	public int download(final String serverFileName, final String localFileName)
	{
		int rc=0;
		int i=serverFileName.lastIndexOf("/");
		String folder = serverFileName.substring(0, i);
		String FileName=serverFileName.substring(i+1).trim();
		getLogger().debug("folder = " + folder);
		getLogger().debug("Server FileName = " + FileName);
		try
		{
			getLogger().debug("ftp download host : HostIP = " + HostIP + "User = " + User );
			FtpTemplate ftpTemplate = new FtpTemplate(HostIP, new Integer("21"), User, pw);
			//rc=ftpTemplate.upload(folder,FileName, new FileInputStream(new File(localFileName)));
			rc=ftpTemplate.download(folder, FileName, new FileOutputStream(new File(localFileName)));
			getLogger().debug("ftp download host : download return code = " + rc );
		}
		catch(Exception e)
		{
			getLogger().debug("ftp download host : download FAIL " ,e);
			return(1);
		}
		return(rc);
	}	
}