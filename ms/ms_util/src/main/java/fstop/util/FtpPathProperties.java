package fstop.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.netbank.util.ESAPIUtil;

import lombok.extern.slf4j.Slf4j;
@Slf4j
public class FtpPathProperties extends Properties
{
	
	String filename="";
	BufferedReader buffer=null;
	static Hashtable fullpath = null;
	private File profd = null;
	static String ModLastDate="";
	private final String defaultpath = "/tmp/batch/properties/"+"ftppath.properties";
//	private final String defaultpath = "C:/Users/BenChien/Desktop/batchLocalTest/properties/"+"ftppath.properties";

	public FtpPathProperties()
	{
		try
		{	
			
			profd=new File(defaultpath);
			profd.setWritable(true,true);
			profd.setReadable(true, true);
			profd.setExecutable(true,true);
			this.load(new FileInputStream(profd));
		}
		catch(Exception e)
		{
			return;
		}
	}
	
	public FtpPathProperties(String filename)
	{

		try
		{
			profd=new File(filename);
			profd.setWritable(true,true);
			profd.setReadable(true, true);
			profd.setExecutable(true,true);
			this.load(new FileInputStream(filename));
		}
		catch(Exception e)
		{
			return;
		}
	}

	
	public int load(FileInputStream f) throws UnsupportedEncodingException,IOException
	{
		String SKey="",SKeyData="";
		String temp="";
		long modlast =profd.lastModified();
		SimpleDateFormat   formatter   =   new   SimpleDateFormat("yyyyMMddHHmmss");   
		String nowmodify   =   formatter.format(new   Date(modlast));
		if(ModLastDate.equals(nowmodify))
			return(1);
		else
		{
			fullpath=new Hashtable();
			ModLastDate=nowmodify;
        }
		String line=null;
		buffer = new BufferedReader( new InputStreamReader(f,"MS950") );
		while( (line=ESAPIUtil.validInput(buffer.readLine(), "GeneralString", true))!=null)
		{
			int offset = line.indexOf("=");
			temp = line.substring(0,offset).trim();
//			getLogger().debug("temp = " + temp);
			if(temp.indexOf(".src")>=0)
			{
				SKey=line.substring(0,offset).trim();
				SKeyData=line.substring(offset+1).trim();
			}
			else if(temp.indexOf(".des")>=0)
			{
				SKey=line.substring(0,offset).trim();
				SKeyData=line.substring(offset+1).trim();
			}
			else
			{
				continue;
			}
			fullpath.put(SKey, SKeyData);
			
		}
		return(0);
	}	
		
	public String getSrcPath(String key)
	{
		String r= (String)fullpath.get(key+".src");
		return r;
	}
	
	public String getDesPath(String key)
	{
		String r= (String)fullpath.get(key+".des");
		return r;
	}
}