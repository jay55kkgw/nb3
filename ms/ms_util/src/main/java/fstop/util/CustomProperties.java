package fstop.util;

import com.netbank.util.StrUtils;

import java.io.BufferedReader;
import java.io.Reader;
import java.util.Properties;

public class CustomProperties extends Properties {

	private static final long serialVersionUID = 4871510116010292933L;

	public void load(final Reader reader) {
		this.clear();
		BufferedReader bufreader = null;
		try {
			bufreader = new BufferedReader(reader);
			String line = null;
			while((line = bufreader.readLine()) != null) {
				String sline = com.netbank.util.StrUtils.trim(line);
				if(sline.indexOf('=') >=0) {
					if(sline.startsWith("#"))
						continue;
					
					String key = sline.substring(0, sline.indexOf('='));
					String value = StrUtils.trim(sline.substring(sline.indexOf('=') + 1));
					if(key != null && key.length() > 0)
						put(key, value);
				}
			}
		}
		catch(Exception e) {
			throw new RuntimeException("載入 Properties 有誤 !", e);
		}
		finally {
			try {
				reader.close();
			}
			catch(Exception e){}
		}
		
	}
}
