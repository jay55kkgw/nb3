package fstop.util;

import java.net.InetAddress;
//import fstop.util.thread.WebServletUtils;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import fstop.util.exception.ToRuntimeException;
import fstop.util.thread.WebServletUtils;

public class IPUtils {


	private static Logger getLogger() {
		return Logger.getLogger(IPUtils.class);
	}

	public static String getLocalIp() {
		try {
		    InetAddress addr = InetAddress.getLocalHost();

		    byte[] ipAddr = addr.getAddress();

		    String s =  (int)(ipAddr[0] & 0x00ff) + "." + (int)(ipAddr[1] & 0x00ff)  + "." +  (int)(ipAddr[2] & 0x00ff)  + "." + (int)(ipAddr[3] & 0x00ff);
		    return s;
		}
		catch(Exception e) {
			throw new ToRuntimeException(e.getMessage(), e);
		}
	}

	public static boolean isLocalTestIp() {
		if("10.10.215.95".equals(getLocalIp()))
			return true;
		if(getLocalIp().startsWith("192.168.11."))
			return true;

		return false;
	}


	public static String getRemoteIP() {
		HttpServletRequest request = WebServletUtils.getRequest();
		if(request != null) {

			if(getLogger().isDebugEnabled()) {
				StringBuffer sb = new StringBuffer();
				sb.append("== Request Header Start ... \n");
	
				java.util.Enumeration enumer = request.getHeaderNames();
				while(enumer.hasMoreElements()) {
					String xName = (String)enumer.nextElement();
					sb.append("[Header]" + xName + " : " + request.getHeader(xName) + "\n");
				}
	
				sb.append("== Request Header End \n");
				getLogger().debug(sb.toString());

			}

			//String ip = request.getHeader("x-forwarded-for");
			String ip = request.getRemoteAddr();
			if (ip == null || ip.length() == 0) {
				ip = request.getRemoteAddr();
			}

			//ip = request.getHeader("x-forwarded-for");
			if (StrUtils.isEmpty(ip) || "unknown".equalsIgnoreCase(ip)) {
				ip = request.getHeader("Proxy-Client-IP");
			}

			if (StrUtils.isEmpty(ip) || "unknown".equalsIgnoreCase(ip)) {
				ip = request.getHeader("WL-Proxy-Client-IP");
			}

			if (StrUtils.isEmpty(ip) || "unknown".equalsIgnoreCase(ip)) {
				ip = request.getRemoteAddr();
			}

			return ip;
		}
		else {
			return getLocalIp();
		}
	}

}
