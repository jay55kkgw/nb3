package fstop.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import fstop.util.exception.ToRuntimeException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DateTimeUtils {
	static SimpleDateFormat dtF = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	static SimpleDateFormat dtD = new SimpleDateFormat("yyyyMMdd");
	static SimpleDateFormat dtT = new SimpleDateFormat("HHmmss");
	static Map<String, SimpleDateFormat> pool = new HashMap();

	/**
	 * 將指定的 names 把 params 裡的值的日期, 如 2008/02/01 格式的日期, 變成 20080201
	 * @param params
	 * @param names
	 */
	public static void removeSlash(HashMap params, String[] names) {
		if(names != null && params != null) {
			for(String o : names) {
				String s = ((String)params.get(o)).replaceAll("/", "");
				params.put(o, s);
			}
		}
	}
	
	/**
	 * 將 20080201 格式的日期, 變成 2008/02/01
	 */
	public static String addSlash(String dateShort) {
		return dateShort.substring(0, 4) + "/" + dateShort.substring(4, 6) + "/" + dateShort.substring(6, 8);
	}
	public static String addSlash2(String dateShort) {
		return dateShort.substring(0, 3) + "/" + dateShort.substring(3, 5) + "/" + dateShort.substring(5);
	}
	
	public static String format(String pattern, Date d) {
		SimpleDateFormat sdf = getDateFormatFromPool(pattern);
		return sdf.format(d);		
	}

	public static Date parse(String pattern, String value) {
		try {
			SimpleDateFormat sdf = getDateFormatFromPool(pattern);
			return sdf.parse(value);
		}
		catch(ParseException e) {
			throw new ToRuntimeException("無法 Parse [" + value+ "] 成 Date Object.", e);
		}
	}

	private static SimpleDateFormat getDateFormatFromPool(String pattern) {
		SimpleDateFormat sdf = null;
		if(!pool.containsKey(pattern)) {
			sdf = new SimpleDateFormat(pattern);
			pool.put(pattern, sdf);
		}
		else {
			sdf = pool.get(pattern);
		}
		return sdf;
	}
	
	/**
	 * 回傳 2008/10/22 20:12:00
	 * @param d
	 * @return
	 */
	public static String getDatetime(Date d) {
		return dtF.format(d);
	}
	
	/**
	 * 回傳 2008/10/22
	 * @param d
	 * @return
	 */
	public static String getDateShort(Date d) {
		return dtD.format(d);
	}
	/**
	 * 回傳 0971011
	 * @param d
	 * @return
	 */
	public static String getCDateShort(Date d) {
		String dt = dtD.format(d);
		String result = dt.substring(4);
		result = new Integer(dt.substring(0, 4)) - 1911 + result;
		if(result.length() == 6)
			result = "0" + result;
		return result;
	}
	
	public static String getTimeShort(Date d) {
		return dtT.format(d);
	}
	
	
	/**
	 * 傳入民國日期及日期, 如 "0970501", "103020", 回傳 097/05/01 10:30:20
	 * @param cDateSort
	 * @param timeSort
	 * @return
	 */
	public static String getCDateTime(String cDateSort, String timeSort) {
		String result = cDateSort + " " + timeSort;
		return result.replaceAll("(\\d{3})(\\d{2})(\\d{2})(\\s)(\\d{2})(\\d{2})(\\d{2})", "$1/$2/$3 $5:$6:$7");
	}
	
	/**
	 * 傳入民國年月日, 如 "0970501"  回傳 Date 物件 -> 20080501
	 * @param cDateSort
	 * @return
	 */
	public static Date getCDate2Date(String cDateSort) {
		String s = (new Integer(cDateSort.substring(0, 3)) + 1911) + "";
		return DateTimeUtils.parse("yyyyMMdd", s + cDateSort.substring(3));
	}
	
	/**
	 * 傳入民國年月日, 如 "0970501"  回傳 Date 物件 -> 20080501
	 * @param cDateSort
	 * @return String 
	 */
	public static String getCDate2Date_STR(String cDateSort) {
		String s = (new Integer(cDateSort.substring(0, 3)) + 1911) + "";
		return s + cDateSort.substring(3);
	}
	
	/**
	 * 傳入民國年月, 如 "09705"  回傳 Date 物件 -> 200805
	 * @param cDateSort
	 * @return String
	 */
	public static String getCDateMonth2DateMonth_STR(String cDateSort) {
		String s = (new Integer(cDateSort.substring(0, 3)) + 1911) + "";
		return s + cDateSort.substring(3);
	}
	
	/**
	 * 加入時間符號 ":"
	 * @param dateShort
	 * @return
	 */
	public static String addColon(String dateShort) {
		return dateShort.substring(0, 2) + ":" + dateShort.substring(2, 4) + ":" + dateShort.substring(4, 6);
	}
	
	public static String addColon2(String dateShort) {
		return dateShort.substring(0, 2) + ":" + dateShort.substring(2, 4);
	}
	public static String getDate(){
		  SimpleDateFormat ft = new SimpleDateFormat("yyyy/MM/dd");
		  Date dd = new Date();
		  return ft.format(dd);
	}
	/**
	 * 算出time1跟time2間隔幾日 
	 * @param time1: yyyy/MM/dd  time2: yyyy/MM/dd
	 * @return quot
	 */	
	public static long getQuot(String time1, String time2){
		  long quot = 0;
		  SimpleDateFormat ft = new SimpleDateFormat("yyyy/MM/dd");
		  try {
		   Date date1 = ft.parse( time1 );
		   Date date2 = ft.parse( time2 );
		   quot = date1.getTime() - date2.getTime();
		   quot = quot / 1000 / 60 / 60 / 24;
		  } catch (ParseException e) {
			  log.error("getQuot error>>{}", e); 
		  }
		  return quot;
	}


	/**  
	 * 得到本月的第一天
	 * @return  
	 */  
	public static String getMonthFirstDay() {
	    Calendar calendar = Calendar.getInstance();
	    calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
	    return dtD.format(calendar.getTime()); 
	}
	
	/**  
	 * 得到本月的最後一天
	 * @return  
	 */  
	public static String getMonthLastDay() {
	    Calendar calendar = Calendar.getInstance();
	    calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
	    return dtD.format(calendar.getTime()); 
	}
	
	/**  
	 * 得到上個月的第一天
	 * @return  
	 */  
	public static String getPrevMonthFirstDay() {
	    Calendar calendar = Calendar.getInstance();
	    calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1);
	    calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
	    return dtD.format(calendar.getTime()); 
	}
	
	/**  
	 * 得到上個月的最後一天
	 * @return  
	 */  
	public static String getPrevMonthLastDay() {
	    Calendar calendar = Calendar.getInstance();
	    calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1);
	    calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
	    return dtD.format(calendar.getTime()); 
	} 
	
	/**  
	 * 得到某個月的第一天
	 * @return  
	 */  
	public static String getSpMonthFirstDay(String dateMonth) {
		String yyyy = dateMonth.substring(0,4);
		String mm = dateMonth.substring(4);
	    Calendar calendar = Calendar.getInstance();
	    calendar.set(Calendar.YEAR,  Integer.parseInt(yyyy));
	    calendar.set(Calendar.MONTH, Integer.parseInt(mm) - 1);
	    calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
	    return dtD.format(calendar.getTime()); 
	}
	
	/**  
	 * 得到某個月的最後一天
	 * @return  
	 */  
	public static String getSpMonthLastDay(String dateMonth) {
		String yyyy = dateMonth.substring(0,4);
		String mm = dateMonth.substring(4);
	    Calendar calendar = Calendar.getInstance();
	    calendar.set(Calendar.YEAR,  Integer.parseInt(yyyy));
	    calendar.set(Calendar.MONTH, Integer.parseInt(mm));
	    calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1);
	    calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
	    return dtD.format(calendar.getTime()); 
	} 
	
	/**
	 * 取得下個日期
	 * @param retPattern 輸入回傳的格式 ex yyyy/MM/dd =2016/07/16
	 * @return
	 */
	public static String getNextDate(String retPattern) {
		  SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		  Calendar calendar = null;
		  Date date = null;
		try {
			date = format.parse(getDate(""));
			calendar = Calendar.getInstance();
			calendar.setTime(date);
			calendar.add(Calendar.DAY_OF_YEAR, 1);
			format.applyPattern(retPattern);
		} catch (ParseException e) {
			log.error("getNextDate error>>{}", e);
		}
		//log.debug("next date>>"+format.format(calendar.getTime()));
		  return format.format(calendar.getTime()); 
		}	
	public static String getDate(String sign){
		if(StrUtils.isEmpty(sign)){sign="";}
		Calendar cal = Calendar.getInstance();
		String theDate = null;
		String month = String.valueOf((cal.get(Calendar.MONTH)+1)/10) +""+ String.valueOf((cal.get(Calendar.MONTH)+1)%10);
		String day= String.valueOf(cal.get(Calendar.DAY_OF_MONTH)/10) +""+ String.valueOf(cal.get(Calendar.DAY_OF_MONTH)%10);
			   theDate = cal.get(Calendar.YEAR) +sign+ month+sign+ day;
			   //log.debug("theDate>>"+theDate);
		return theDate; 
	}	
//	public static void main(String[] s) {
////		log.debug(DateTimeUtils.getCDateShort(new Date()));
////		log.debug(DateTimeUtils.getCDateTime("0970501", "103020"));
////		log.debug(DateTimeUtils.getCDate2Date("0970501"));
////		log.debug(DateTimeUtils.addSlash("20080501"));
//	}		
}
