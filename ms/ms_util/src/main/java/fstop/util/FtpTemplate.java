package fstop.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.log4j.Logger;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FtpTemplate {
	
	public static enum LISTTYPE {DIR, FILE, ALL}

	private String ip = "";

	private Integer port = 21;

	private String user = "";
	
	private String pw = "";
	
	private String byteEncoding = "8859_1";
	
	private String strEncoding = "MS950";
	

	public String getByteEncoding() {
		return byteEncoding;
	}

	public void setByteEncoding(String byteEncoding) {
		this.byteEncoding = byteEncoding;
	}

	public String getStrEncoding() {
		return strEncoding;
	}

	public void setStrEncoding(String strEncoding) {
		this.strEncoding = strEncoding;
	}

	public String getIp() {
		return ip;
	}
	 
	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getPassword() {
		return pw;
	}

	public void setPassword(String password) {
		this.pw = password;
	}
	
	public Integer getPort() {
		return port;
	}
	
	public void setPort(Integer port) {
		this.port = port;
	}
	
	public String getUser() {
		return user;
	}
	
	public void setUser(String user) {
		this.user = user;
	}

	public FtpTemplate(String ip, Integer port, String user, String pwd) {
		this.ip = ip;
		this.port = port;
		this.user = user;
		this.pw = pwd;
	}

	public List<String> listFiles(String folder) {
		return list(folder, LISTTYPE.FILE);
	}
	
	public List<String> listDirs(String folder) {
		return list(folder, LISTTYPE.DIR);
	}
	
	public List<String> list(final String folder, final LISTTYPE type) {
		final List result = new ArrayList();
		ftpAction(new FtpCallback() {
			public int doFtpAction(FTPClient client) throws IOException {
				client.changeWorkingDirectory(folder);
			    FTPFile[] list = client.listFiles();
				for (int i = 0; i < list.length; i++) {
					if(type == LISTTYPE.ALL) {
						//do nothing
					}
					else if(type != LISTTYPE.DIR && list[i].isDirectory()) {
						continue;
					}
					else if(type != LISTTYPE.FILE && list[i].isFile()) {
						continue;
					}
					String name = list[i].getName();
					result.add(new String(name.getBytes(byteEncoding), strEncoding));
				} 
				return(0);
			}
		});
		
		return result;
	}
		
	/**
	 * FTP 上傳檔案
	 * @param folder
	 * @param serverFileName
	 * @param fis
	 */
	public int upload_ASCII(final String folder, final String serverFileName, final FileInputStream fis) {
		int rc = ftpAction_ASCII(new FtpCallback() {
			public int doFtpAction(FTPClient client) throws IOException {
				boolean rc=false;
			    String wkdir=client.printWorkingDirectory();
			    log.debug("doFtpAction printWorkingDirectory wkdir = " + wkdir);
				log.debug("doFtpAction changeWorkingDirectory folder = " + folder);
				rc=client.changeWorkingDirectory(folder);
				log.debug("doFtpAction  changeWorkingDirectory rc = " + rc);
				if(rc==false)
				{
					return(3);
				}
				
				client.setBufferSize(1024);
				
				client.enterLocalPassiveMode();
				log.debug("ReplyString() >>{}" , client.getReplyString());
//				log.debug("doFtpAction deleteFile serverFileName = " + serverFileName);
				rc=client.deleteFile(serverFileName);
				log.debug("doFtpAction deleteFile rc = " + rc);
//				log.debug("doFtpAction storeFile serverFileName = " + serverFileName);
				rc=client.storeFile(serverFileName, fis);
				log.debug("storeFile ReplyString() >>{}" , client.getReplyString());
				if(rc==false)
				{
					return(4);
				}
				log.debug("doFtpAction storeFile rc = " + rc);
				return(0);
			}
		});
		return(rc);
	}	
	
	/**
	 * FTP 上傳檔案
	 * @param folder
	 * @param serverFileName
	 * @param fis
	 */
	public int upload(final String folder, final String serverFileName, final FileInputStream fis) {
		int rc=ftpAction(new FtpCallback() {
			public int doFtpAction(FTPClient client) throws IOException {
				boolean rc=false;
			    String wkdir=client.printWorkingDirectory();
			    log.debug("doFtpAction printWorkingDirectory wkdir = " + wkdir);
				log.debug("doFtpAction changeWorkingDirectory folder = " + folder);
				if(folder.length()>0) {
					rc=client.changeWorkingDirectory(folder);
//					//TODO:本地ftp測試 新增資料夾
					while(!rc) {
						client.makeDirectory(folder);
						rc=client.changeWorkingDirectory(folder);
					}
				}
				else
					rc=true;
				log.debug("doFtpAction  changeWorkingDirectory rc = " + rc);
				if(rc==false)
				{
					return(3);
				}
				
				
				client.setBufferSize(1024);
				
				client.enterLocalPassiveMode();
				log.debug("enterLocalPassiveMode ReplyString() >>{}" , client.getReplyString());
//				log.debug("doFtpAction deleteFile serverFileName = " + serverFileName);
				rc=client.deleteFile(serverFileName);
				log.debug("doFtpAction deleteFile rc = " + rc);
//				log.debug("doFtpAction storeFile serverFileName = " + serverFileName);
				rc=client.storeFile(serverFileName, fis);
				log.debug("storeFile ReplyString() >>{}" , client.getReplyString());
				if(rc==false)
				{
					return(4);
				}
				log.debug("doFtpAction storeFile rc = " + rc);
				return(0);
			}
		});
		return(rc);
	}	
	
	/**
	 * FTP 下載檔案
	 * @param folder
	 * @param serverFileName
	 * @param fos
	 */
	public int download(final String folder, final String serverFileName, final FileOutputStream fos) {
		int rc=0;
		rc=ftpAction(new FtpCallback() {
			public int doFtpAction(FTPClient client) throws IOException {
				client.changeWorkingDirectory(folder);
				client.enterLocalPassiveMode();
				log.debug(" download enterLocalPassiveMode ReplyString() >>{}" , client.getReplyString());
				client.retrieveFile(serverFileName, fos);
				return(0);
			}
		});
		return(rc);
	}	
		
	public int ftpAction_ASCII(FtpCallback callback) {
		int err=0;
	    FTPClient ftp = new FTPClient();
	    try {
//	      String destinationFolder = "/";
	    	log.debug("ftpAction  port = " + port);
	    	log.debug("ftpAction  ip = " + ip);
	    	log.debug("ftpAction  user = " + user);
	    	
//	    	//TODO FTP本地測試
//	    	pw = "ftpuser";
	    	
	    	ftp.setDefaultPort(port);
	    	ftp.connect(ip);
	    	boolean rc=ftp.login(user, pw);
	    	log.debug("ftpAction login rc = " + rc);
	    	if(rc!=true)
	    	{
	    		err=5;
	    	}
	    	else
	    	{
	    		rc=ftp.setFileType(ftp.ASCII_FILE_TYPE);
	    		log.debug("ftpAction setFileType rc = " + rc);
	    		if(rc!=true)
	    		{
	    			err=6;
	    			log.error("ftpAction  setFileType error = " + ftp.getReplyString());
	    		}
	    		else
	    		{
	    			String wkdir=ftp.printWorkingDirectory();
//	      log.debug("ftpAction printWorkingDirectory wkdir = " + wkdir);
	 
//	      boolean rc=ftp.changeWorkingDirectory(destinationFolder);
//	      log.debug("ftpAction changeWorkingDirectory rc = " + rc);
	    			err=callback.doFtpAction(ftp);
	    			log.debug("ftpAction doFtpAction rc = " + err);
	    		}
	    	}
	    }
	    catch (SocketException e)
	    {
	    	log.error("ftpAction_ASCII error>>{}", e);
	    	err=1;
	    }
	    catch (IOException e)
	    {
	    	log.error("ftpAction_ASCII error>>{}", e);
	    	err=2;
	    }
	    finally
	    {
			try
			{
				ftp.logout();
			}
			catch (IOException e)
			{
			}
			try
			{
				ftp.disconnect();
			}
			catch (IOException e)
			{
			}
	    }
	    return(err);
	}

	public int ftpAction(FtpCallback callback) {
		int err=0;
	    FTPClient ftp = new FTPClient();
	    try {
//	      String destinationFolder = "/";
	    	log.debug("ftpAction  port = " + port);
	    	log.debug("ftpAction  ip = " + ip);
	    	log.debug("ftpAction  user = " + user);
	    	
//	    	//TODO FTP本地測試
//	    	pw = "ftpuser";
	    	
	    	ftp.setDefaultPort(port);
	    	ftp.setConnectTimeout(5000); 
	    	ftp.setDataTimeout(120000); 
	    	ftp.connect(ip);
	    	
	    	boolean rc=ftp.login(user, pw);
	    	log.debug("ftpAction login rc = " + rc);
	    	if(rc!=true)
	    	{
	    		err=5;
	    	}
	    	else
	    	{
	    		rc=ftp.setFileType(ftp.BINARY_FILE_TYPE);
	    		log.debug("ftpAction setFileType rc = " + rc);
	    		if(rc!=true)
	    		{
	    			err=6;
	    			log.error("ftpAction  setFileType error = " + ftp.getReplyString());
	    		}
	    		else
	    		{
	    			String wkdir=ftp.printWorkingDirectory();
//	      log.debug("ftpAction printWorkingDirectory wkdir = " + wkdir);
	 
//	      boolean rc=ftp.changeWorkingDirectory(destinationFolder);
//	      log.debug("ftpAction changeWorkingDirectory rc = " + rc);
	    			err=callback.doFtpAction(ftp);
	    			log.debug("ftpAction doFtpAction rc = " + err);
	    		}
	    	}
	    }
	    catch (SocketException e)
	    {
	    	log.error("ftpAction error>>{}", e);
	    	err=1;
	    }
	    catch (IOException e)
	    {
	    	log.error("ftpAction error>>{}", e);
	    	err=2;
	    }
	    finally
	    {
			try
			{
				ftp.logout();
			}
			catch (IOException e)
			{
			}
			try
			{
				ftp.disconnect();
			}
			catch (IOException e)
			{
			}
	    }
	    return(err);
	}

//	public static void main(String args[]) {
//		
//		FtpTemplate template = new FtpTemplate("192.168.50.191", 21, "ftpuser", "ftpuser");
//		List<String> list = template.listFiles("/");
//		for(String s : list) {
//			log.debug("file: " + s);
//		}
//		List<String> listD = template.listDirs("/");
//		for(String s : listD) {
//			log.debug("Dir: " + s);
//		}
//
//	}
}
