package fstop.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Hashtable;
import java.util.Properties;

import com.netbank.util.ESAPIUtil;

import lombok.extern.slf4j.Slf4j;
@Slf4j
public class BatchDetailProperties extends Properties
{
	
	String filename="";
	BufferedReader buffer=null;
	static Hashtable fullpath = null;
	private File profd = null;
	static String ModLastDate="";
	private final String defaultpath = "/tmp/batch/properties/"+"batchdetail.properties";
//	private final String defaultpath = "C:/Users/BenChien/Desktop/batchLocalTest/properties/"+"batchdetail.properties";

	public BatchDetailProperties()
	{
		try
		{	
			
			profd=new File(defaultpath);
			profd.setWritable(true,true);
			profd.setReadable(true, true);
			profd.setExecutable(true,true);
			this.load(new FileInputStream(profd));
		}
		catch(Exception e)
		{
			return;
		}
	}
	
	public BatchDetailProperties(String filename)
	{

		try
		{
			profd=new File(filename);
			profd.setWritable(true,true);
			profd.setReadable(true, true);
			profd.setExecutable(true,true);
			this.load(new FileInputStream(filename));
		}
		catch(Exception e)
		{
			return;
		}
	}

	
	public int load(FileInputStream f) throws UnsupportedEncodingException,IOException
	{
		String batchName="",batchDetail="";
		String line=null;
		fullpath=new Hashtable();
		buffer = new BufferedReader( new InputStreamReader(f,"UTF-8") );
		while( (line=ESAPIUtil.validInput(buffer.readLine(), "GeneralString", true))!=null)
		{
			int Namepos = line.indexOf(",");
			batchName = line.substring(0,Namepos).trim();
			batchDetail = line.substring(Namepos+1);
			fullpath.put(batchName, batchDetail);
			
		}
		return(0);
	}	
		
	public String getDetail(String batchName)
	{
		String r= (String)fullpath.get(batchName);
		return r;
	}
	
}