package fstop.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Properties;

import com.netbank.util.ESAPIUtil;

import lombok.extern.slf4j.Slf4j;
import sun.misc.BASE64Decoder;
@Slf4j
public class NetrcProperties extends Properties
{
	String filename="";
	BufferedReader buffer=null;
	static HashMap hostip = null;
	static HashMap user = null;
	static HashMap pw = null;
	private File profd = null;
	static String ModLastDate="";
	private final String defaultpath = "/tmp/batch/properties/"+"netrc.properties";
	//private final String defaultpath = "C:/Users/BenChien/Desktop/batchLocalTest/properties/"+"netrc.properties";


	public NetrcProperties()
	{

		try
		{	
			
			profd=new File(defaultpath);
			//Resource resource = new ClassPathResource("properties/netrc.properties");
			//profd = resource.getFile();
			profd.setWritable(true,true);
			profd.setReadable(true, true);
			profd.setExecutable(true,true);
			this.load(new FileInputStream(profd));
		}
		catch(Exception e)
		{
			return;
		}
	}

	public NetrcProperties(String filename)
	{

		try
		{
			profd=new File(filename);
			profd.setWritable(true,true);
			profd.setReadable(true, true);
			profd.setExecutable(true,true);
			this.load(new FileInputStream(filename));
		}
		catch(Exception e)
		{
			return;
		}
	}

	
	synchronized public int load(FileInputStream f) throws UnsupportedEncodingException,IOException
	{
		String SHostIp="";
		String SUser="";
		String SPWEn="";
		String SPWDe="";
		long modlast =profd.lastModified();
		SimpleDateFormat   formatter   =   new   SimpleDateFormat("yyyyMMddHHmmss");   
		String nowmodify   =   formatter.format(new   Date(modlast));
//		getLogger().debug("nowmodify = " + nowmodify);
//		getLogger().debug("ModLastDate = " + ModLastDate);
		if(ModLastDate.equals(nowmodify))
				return(1);
		else
		{
        	hostip=new HashMap();
        	user=new HashMap();
        	pw=new HashMap();
        	ModLastDate=nowmodify;
        }
		String line=null;
		buffer = new BufferedReader( new InputStreamReader(f,"MS950") );
		while( (line=buffer.readLine())!=null)
		{
			int offset = line.indexOf("=");
			SHostIp = line.substring(0,offset).trim();
			log.debug(ESAPIUtil.vaildLog("load SHostIp :" + SHostIp));
			String propData = line.substring(offset+1).trim();
			offset = propData.indexOf(":");
			SUser=propData.substring(0, offset).trim();
			log.debug(ESAPIUtil.vaildLog("load SUser :" + SUser));
			SPWEn=propData.substring(offset+1).trim();
			SPWDe=getFromBASE64(SPWEn);
			hostip.put(SHostIp,SHostIp);
			user.put(SHostIp, SUser);
			pw.put(SHostIp, SPWDe);
		}
		return(0);
	}	
		
	synchronized public String getUser(String hostip)
	{
		String r= (String)user.get(hostip);
		return r;
	}
	
	synchronized public String getpassword(String hostip)
	{
		String r= (String)pw.get(hostip);
		return r;
	}
	
	synchronized public int getUserPassword(String hostip,Hashtable ipusrpwd)
	{
		try
		{
			ipusrpwd.put("IP",hostip);
			ipusrpwd.put("USER",(String)user.get(hostip));
			ipusrpwd.put("PASSWD",(String)pw.get(hostip));
		}
		catch(Exception e)
		{
			return 1;
		}
		return 0;
	}
	
	public String getFromBASE64(String s)
	{
		if (s == null) 
			return null;
		BASE64Decoder decoder = new BASE64Decoder();
		try {
			byte[] b = decoder.decodeBuffer(s);
			return new String(b);
		} catch (Exception e) {
		return null;
		}
	}
}