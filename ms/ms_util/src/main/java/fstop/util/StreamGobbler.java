package fstop.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.netbank.util.ESAPIUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class StreamGobbler extends Thread {
	InputStream is;

	String type;

	StringBuffer sb = new StringBuffer();
	
	StreamGobbler(InputStream is, String type) {
		this.is = is;
		this.type = type;
	}

	public void run() {
		try {
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			String line = null;
			while ((line = br.readLine()) != null) {
				log.debug(ESAPIUtil.vaildLog(type + ">" + line));
				sb.append(line);
			}
		} catch (IOException ioe) {
			log.error("run error>>{}", ioe);
		}
	}
	
	public String getResult() {
		return sb.toString();
	}
	
}
