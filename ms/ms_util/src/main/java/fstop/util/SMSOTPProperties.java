package fstop.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@Data
public class SMSOTPProperties {
	
	
	@Value("${smsotp.otpip}")
	private String OTPIP;
	@Value("${smsotp.otpport}")
	private String OTPPORT;
//
//
//	public String getOTPIP() {
//		return OTPIP;
//	}
//
//	public void setOTPIP(String otpip) {
//		this.OTPIP = otpip;
//	}
//
//	public String getOTPPORT() {
//		return OTPPORT;
//	}
//
//	public void setOTPPORT(String port) {
//		this.OTPPORT = port;
//	}
}
