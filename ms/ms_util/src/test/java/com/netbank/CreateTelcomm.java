package com.netbank;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;

import fstop.util.JSONUtils;
import fstop.util.StrUtils;
import lombok.Builder;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CreateTelcomm {
    static String nAll = "a1000Telcomm,帳戶總覽,A1000|a2000Telcomm,帳戶總覽,A2000|a2001Telcomm,帳戶總覽,A2001|n200Telcomm,變更往來帳戶及信託業務通訊地址／電話,N200|n523Telcomm,外匯活存明細查詢 - 指定帳號,N523|n263Telcomm,香港分行帳戶存款指定期間交易明細查詢,N263|n524Telcomm,外匯活存明細查詢 - 全部帳號,N524|n264Telcomm,香港分行帳戶存款指定期間交易明細查詢,N264|n322Telcomm,基金下單線上問卷投資屬性註記,N322|n530Telcomm,外匯定存明細查詢,N530|n510Telcomm,外匯存款餘額查詢,N510|n570Telcomm,變更外匯進口／出口／匯兌通訊地址／電話,N570|n571ExecTelcomm,變更外匯進口／出口／匯兌通訊地址／電話,N571|n270Telcomm,香港分行帳戶存款餘額查詢,N270|n015Telcomm,支存當日不足扣票據明細,N015|n110Telcomm,,N110|n130Telcomm,活期性存款交易明細查詢,N130|n133Telcomm,N133活期性存款（全部帳號）交易明細查詢,N133|n150Telcomm,輕鬆理財存摺明細,N150|n174Telcomm,轉入外匯綜存定存,N174|n420Telcomm,定期性存款明細查詢,N420|n421Telcomm,綜存定期性存款明細查詢,N421|n615Telcomm,虛擬帳號入帳明細,N615|n625Telcomm,支存已兌現票據明細,N625|n640Telcomm,輕鬆理財證券交割明細,N640|n650Telcomm,輕鬆理財自動申購基金明細,N650|n860Telcomm,託收票據明細,N860|n870Telcomm,中央登錄債券餘額,N870|n871Telcomm,中央登錄債券明細,N871|n900Telcomm,變更信用卡通訊地址／電話,N810|n900ExecTelcomm,變更信用卡通訊地址／電話,N900|n921Telcomm,約定帳戶查詢,N921|n920Telcomm,歸戶查詢,N920|n910Telcomm,查詢密碼確認,N910|n950Telcomm,查詢密碼確認,N950|n951Telcomm,查詢密碼確認,N951|n950CMTelcomm,查詢密碼確認,N950|n951CMTelcomm,查詢密碼確認,N951|n953Telcomm,持卡人身份證字號查詢,N953|n954Telcomm,預約轉帳晶片押碼認證,N954|n940Telcomm,簽入密碼變更,N940|n941Telcomm,執行交易密碼變更,N941|n942Telcomm,,N942|n553Telcomm,外幣下期借款本息查詢查詢,N553|n014Telcomm,下期借款本息查詢,N014|n016Telcomm,本息償還明細查詢,N016|n070Telcomm,,N070|n071Telcomm,,N071|n073Telcomm,繳納期貨保證金,N073|n074Telcomm,,N074|n075Telcomm,電費,N075|n750aTelcomm,臺灣省自來水費,N750A|n750bTelcomm,台北市自來水費,N750B|n750cTelcomm,勞保費,N750C|n750hTelcomm,新制勞工退休準備金,N750H|n750dTelcomm,和信電信費,N750D|n079Telcomm,綜存定存解約,N079|n079ExecTelcomm,綜存定存解約,N079_1|n077Telcomm,定期性存款－自動轉期,N077|n078Telcomm,定期性存款－續存,N078|n076Telcomm,轉入台幣零存整付定存,N076|n971Telcomm,,N971|n554Telcomm,本息償還明細查詢查詢,N554|n552Telcomm,借款明細查詢,N552|n320Telcomm,借款明細查詢,N320|n550Telcomm,當日信用狀通知查詢,N550|n551Telcomm,當日信用狀開狀查詢,N551|n830Telcomm,申請代扣繳,N830|n8301Telcomm,申請健保費代扣繳,N8301|n8302Telcomm,申請勞保費代扣繳,N8302|n8302_1Telcomm,舊制勞工退休準備金代扣繳,N8302_1|n555Telcomm,商業信用狀開狀查詢-到單查詢,N555|n556Telcomm,商業信用狀開狀查詢-明細查詢,N556|n557Telcomm,擔保信用狀/保證函開狀查詢,N557|n840Telcomm,,N840|n841Telcomm,,N841|n842Telcomm,,N842|n843Telcomm,,N843|n844Telcomm,,N844|n845Telcomm,,N845|n8501Telcomm,掛失申請,N8501|n8502Telcomm,掛失申請,N8502|n8503Telcomm,掛失申請,N8503|n8504Telcomm,掛失申請,N8504|n8505Telcomm,掛失申請,N8505|n8506Telcomm,掛失申請,N8506|n8507Telcomm,掛失申請,N8507|n8508Telcomm,掛失申請,N8508|n8509Telcomm,掛失申請,N8509|n850ATelcomm,黃金存褶、印鑑掛失申請,N850A|p001Telcomm,新增代收車號交易,P001|p002Telcomm,刪除代收車號交易,P002|p003Telcomm,查詢個人資料及申請代收狀況,P003|p004Telcomm,查詢扣繳記錄,P004|p005Telcomm,修改個人資料及扣款帳號,P005|p018Telcomm,查詢縣市別資料,P018|n1011Telcomm,支票存款開戶申請,N1011|n1012Telcomm,空白票據申請,N1012|n1013Telcomm,利息所得扣繳憑單補發申請,N1013|n1014Telcomm,存款餘額證明申請,N1014|n105Telcomm,個人房屋擔保借款繳息清單,N105|n1060Telcomm,利息所得扣繳憑單列印,N1060|n1061Telcomm,利息所得扣繳憑單列印,N1061|n660Telcomm,補發電子交易對帳單,N660|n930Telcomm,用戶電子郵箱位址變更,N930|n931Telcomm,申請、取消電子交易對帳單,N931|n531Telcomm,,N531|n175_1Telcomm,,N175|n175_2Telcomm,,N175|n177Telcomm,外幣定期性存款－自動轉期,N177|n178_1Telcomm,外幣定存單到期續存,N178|n178_2Telcomm,外幣定存單到期續存,N178|n831Telcomm,,N831|na20Telcomm,客戶基本資料查詢,NA20|nk01Telcomm,換Key,NK01|n924Telcomm,外匯指定期間帳號歸戶查詢,N924|xmxrTelcomm,與主機對時,XMXR|n021Telcomm,,N021|n022Telcomm,,N022|n023Telcomm,,N023|n024Telcomm,,N024|n025Telcomm,,N025|n026Telcomm,,N026|n030Telcomm,,N030|nd01Telcomm,與主機對時,ND01|nd08Telcomm,與主機對時,ND08|nd09Telcomm,,ND09|nd10Telcomm,,ND10|nd11Telcomm,,ND11|nd12Telcomm,,ND12|nd13Telcomm,,ND13|nd14Telcomm,,ND14|nd15Telcomm,,ND15|nd16Telcomm,,ND16|n565Telcomm,匯入匯款查詢,N565|n558Telcomm,進口到單查詢,N558|n564Telcomm,出口押匯查詢,N564|n567Telcomm,光票託收查詢,N567|n566Telcomm,匯出匯款查詢,N566|f013Telcomm,線上解款待解款電文,F013|f035Telcomm,匯入匯款線上解款申請及註銷,F035|nhk1Telcomm,,NHK1|n559Telcomm,進口託收查詢,N559|n560Telcomm,信用狀項下出口託收查詢,N560|n563Telcomm,D/A、D/P出口託收查詢,N563|n880Telcomm,取消約定轉入帳號,N880|n750eTelcomm,健保費,N750E|n750fTelcomm,國民年金保險費,N750F|n750gTelcomm,欣欣瓦斯費,N750G|n171Telcomm,繳稅,N171|n108Telcomm,台幣綜存質借功能取消,N108|c900Telcomm,查詢信用卡客戶資料,C900|n8330Telcomm,公用事業費用代扣繳申請查詢,N8330|f037Telcomm,外幣跨行匯款約定轉入帳號取消,F037|gd01Telcomm,黃金存摺牌告價格異動,GD01|gd02Telcomm,黃金存摺銷售資料查詢,GD02|gd11Telcomm,黃金存摺牌告資料查詢,GD11|n190Telcomm,,N190|n191Telcomm,黃金存摺交易明細查詢,N191|n192Telcomm,黃金定期定額約定資料查詢,N192|n09001_1Telcomm,黃金存摺買進,N09001|n09001_2Telcomm,黃金存摺買進,N09001|n09002_1Telcomm,黃金存摺回售,N09002|n09002_2Telcomm,黃金存摺回售,N09002|n091Telcomm,預約/取消黃金買進回售,N091|n092Telcomm,黃金存摺買進／回售預約查詢,N092|n926Telcomm,黃金存摺歸戶帳號查詢,N926|n094QueryTelcomm,繳納定期扣款失敗手續費-查詢,N094|n094PayTelcomm,繳納定期扣款失敗手續費-繳費,N094|n093Telcomm,黃金存摺定期定額申購,N093|n093QueryTelcomm,黃金存摺定期定額查詢,N093|na50Telcomm,申請黃金存摺帳戶,NA50|na60Telcomm,申請黃金存摺網路交易,NA60|na70Telcomm,線上解鎖交易,NA70|na80Telcomm,金融卡線上申請/取消轉帳功能,NA80|np01Telcomm,促銷活動獎次查詢,NP01|np02Telcomm,線上抽獎交易,NP02|n927Telcomm,投資屬性查詢,N927|n100ExecTelcomm,變更往來帳戶及信託業務通訊地址／電話,N100|n913Telcomm,行動銀行啟用／關閉,N913|n070Telcomm_m,,M070-1|n075Telcomm_m,電費,M075|n750aTelcomm_m,臺灣省自來水費,M750-14|n750bTelcomm_m,台北市自來水費,M750-04|n934Telcomm,線上登錄/取消匯款通知作業,N934|n935Telcomm,匯入匯款設定通知查詢,N935|n185Telcomm,國內匯入匯款通知,N185|n3003Telcomm,借款還款,3003|n960Telcomm,,N960|n932Telcomm,,N932|n933Telcomm,,N933|n215Telcomm,,N215|n201Telcomm,,N201|n202Telcomm,,N202|n365Telcomm,,N365|n366Telcomm,,N366|n367Telcomm,黃金存摺結清銷戶作業,N367|n103Telcomm,,N103|n203Telcomm,,N203|n204Telcomm,,N204|n104Telcomm,,N104|nb30Telcomm,,NB30|nb31Telcomm,,NB31|nb32Telcomm,,NB32|n865Telcomm,約定申請撤銷查詢,N865|cn19Telcomm,雙因素憑證註冊申請交易,CN19|cn29Telcomm,檢核帳號是否已啟用無卡提款,CN29|cn30Telcomm,簽署無卡提款同意書,CN30|cn32Telcomm,申請無卡提款服務交易,CN32|a105Telcomm,,A105|n027Telcomm,,N027|na72Telcomm,重設簽入、交易密碼使用者名稱交易,NA72|n922Telcomm,基金下單歸戶帳號查詢,N922|c021Telcomm,基金轉換交易查詢,C021|c022Telcomm,基金手續費檢核,C022|n371Telcomm,轉換基金即時入扣帳交易,N371|c023Telcomm,轉換交易結果通知,C023|c016Telcomm,基金手續費檢核,C016" +
            "";

    static String tmpl = "" +
            "\n" +
            "    @Bean\n" +
            "    protected CommonTelcomm {telcommId}() throws Exception {\n" +
            "        CommonTelcomm c = createMockTelcomm(\"{txid_l}.txt\", \"{txid}\");\n" +
            "         c.setDesc(\"{desc}\");\n" +
            "        c.setHOSTMSGID(\"{txid}\");;\n" +
            "        return c;\n" +
            "\n" +
            "\n" +
            "    }\n\n";

    @Value
    @Builder
    static class BeanInfo {
        String telcommId;
        String desc;
        String txid;
    }

    public static void main(String[] args) {

        List<String> sary = StrUtils.splitToList("\\|", nAll);

        List<BeanInfo> infos = sary.stream()
                .map(c -> StrUtils.splitToList(",", c))
                .filter(ary -> ary.size() > 0)
                .map(aa -> {
try {
    return BeanInfo.builder()
            .telcommId(aa.get(0))
            .desc(aa.get(1))
            .txid(aa.get(2))
            .build();
} catch(Exception e) {
    log.error("data error: {}", JSONUtils.toJson(aa));
}
return null;
                })
                .filter(o -> o != null)
                .collect(Collectors.toList());


        infos.stream()
                .peek(i -> {
                    String t = StringUtils.replace(tmpl, "{telcommId}", i.getTelcommId());
                    t = StringUtils.replace(t, "{txid_l}", i.getTxid().toLowerCase());
                    t = StringUtils.replace(t, "{txid}", i.getTxid().toUpperCase());
                    t = StringUtils.replace(t, "{desc}", i.getDesc());
                    log.debug(t);
                })
                .collect(Collectors.toList());

    }
}
