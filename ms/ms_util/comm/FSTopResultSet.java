package bank.comm;

import java.util.*;

/**
* 提供一可對已處理過之下行電文資料做存取之介面<br>
*如同java.sql.ResultSet,FSTopResultSet介面提供cursor以存取回傳資料中之各筆資料,<br>
*亦可將回傳資料轉為java.util.Vector物件以供處理
* @author 林季穎
 * @version 1.0
 * @see bank.comm.FSTopTransaction
 */
public interface FSTopResultSet {
    /**
        *將cursor移至所指定的資料位置
        *藉由一FSTopResultSet介面回傳
        *@param row 指定cursor至指定的第row筆資料
        * @return 如果cursor還在第一筆及最後一筆資料之間時,回傳true,否則回傳false
        * @throws CommunicationException 當FSTopResultSet中無資料(資料總筆數=0)時,丟出CommunicationException
        */
    public boolean absolute(int row) throws CommunicationException;
     /**
        *將cursor移至最後一筆資料之後
        */
    public void afterLast();
     /**
        *將cursor移至第一筆資料之前
        */
    public void beforeFirst();
     /**
        *將cursor移至第一筆資料
        * @return 當FSTopResultSet中無資料,回傳false,否則回傳true
        */
    public boolean first();
    /**
        *檢查cursor是否位在最後一筆資料之後
        * @return 當cursor位在最後一筆資料之後,回傳true,否則回傳false
        */
    public boolean isAfterLast();
    /**
        *檢查cursor是否位在第一筆資料之前
        * @return 當cursor位在第一筆資料之前,回傳true,否則回傳false
        */
    public boolean isBeforeFirst();
    /**
        *檢查cursor是否位在第一筆資料
        * @return 當cursor位在第一筆資料,回傳true,否則回傳false
        */
    public boolean isFirst();
    /**
        *檢查cursor是否位在最後一筆資料
        * @return 當cursor位在最後一筆資料,回傳true,否則回傳false
        */
    public boolean isLast();
    /**
        *將cursor移至最後一筆資料
        * @return 當FSTopResultSet中無資料,回傳false,否則回傳true
        */
    public boolean last();
    /**
        *將cursor移至下一筆資料
        * @return 當已無下一筆資料時,回傳false,否則回傳true
        */
    public boolean next();
    /**
        *回傳cursor目前所在之位置,若為第一筆資料,則回傳1,若為第二筆資料,則回傳2,以此類推
        * @return cursor目前所在之位置
        */
    public int getRow();
     /**
        *回傳一筆資料中指定欄位名稱之值
        * @return 指定欄位名稱所對應之值
        * @throws CommunicationException 當FSTopResultSet中無資料或該欄位名稱不存在時,丟出CommunicationException
        */
    public String getString(String varName) throws CommunicationException;
    /**
        *回傳FSTopResultSet中資料總筆數
        * @return 資料總筆數
        */
    public int size();
    /**
        *將FSTopResultSet中之資料轉為java.util.Vector物件回傳
        * @return java.util.Vector物件,Vector中各元素為java.util.Hashtable物件,每個java.util.Hashtable物件包含一筆下傳電文之內容
        */
    public Vector toVector();
}

