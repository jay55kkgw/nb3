package fstop.services.impl;

import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class N831 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("n831Telcomm")
	private TelCommExec n831Telcomm;
	
//	@Required
//	public void setN831Telcomm(TelCommExec telcomm) {
//		n831Telcomm = telcomm;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 
		
		Date d = new Date();
		params.put("DATE", DateTimeUtils.getCDateShort(d));
		params.put("TIME", DateTimeUtils.getTimeShort(d));
		//FGTXWAY 為交易機制
		if("0".equals(params.get("FGTXWAY"))) {   // 交易密碼
			params.put("CERTACN", StrUtils.repeat(" ", 19));	//即時
		}
		else if("1".equals(params.get("FGTXWAY"))) {  //憑證
			params.put("CERTACN", StrUtils.repeat("1", 19));	//即時
		}
		else if("2".equals(params.get("FGTXWAY"))) {  //晶片金融卡
			params.put("CERTACN", StrUtils.repeat("4", 19));	//即時
		}
		else if("7".equals(params.get("FGTXWAY"))) {  //IDGATE
			params.put("CERTACN", StrUtils.repeat("C", 19));	//即時
		}

		TelcommResult telcommResult = n831Telcomm.query(params);
		telcommResult.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));

		return telcommResult;
	}


}
