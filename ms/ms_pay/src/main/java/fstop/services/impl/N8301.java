package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.util.ESAPIUtil;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N8301 extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("n8301Telcomm")
	private TelCommExec n8301Telcomm;
	 
//	@Required
//	public void setN8301Telcomm(TelCommExec telcomm) {
//		n8301Telcomm = telcomm;
//	}
	
	@Override	
	public MVH doAction(Map params) {    
		Map<String, String> p = params;
		log.debug(ESAPIUtil.vaildLog("N8301 params -------------------------------------------------------> " + p));
		p.put("DATE", DateTimeUtils.getCDateShort(new Date()));
		p.put("TIME", DateTimeUtils.format("HHmmss", new Date()));
		//FGTXWAY 為交易機制
		if("0".equals(params.get("FGTXWAY"))) {   // 交易密碼
			params.put("CERTACN", StrUtils.repeat(" ", 19));	//即時
		}
		else if("1".equals(params.get("FGTXWAY"))) {  //憑證
			params.put("CERTACN", StrUtils.repeat("1", 19));	//即時
		}
		else if("2".equals(params.get("FGTXWAY"))) {  //晶片金融卡
			params.put("CERTACN", StrUtils.repeat("4", 19));	//即時
		}else if("7".equals(params.get("FGTXWAY"))) {  //晶片金融卡
			params.put("CERTACN", StrUtils.repeat("C", 19));	//即時
		}
		else if("7".equals(params.get("FGTXWAY"))) {  //IDGATE
			params.put("CERTACN", StrUtils.repeat("C", 19));	//即時
		}
		log.debug("N8301 Begin query -------------------------------------------------------> ");
		TelcommResult helper = n8301Telcomm.query(params);
		log.debug("N8301 After query -------------------------------------------------------> ");
		helper.getFlatValues().put("CMQTIME",
				DateTimeUtils.getDatetime(new Date()));
		
		//TODO 若回傳的碼是有 Error Code, D:\SohoProject\fstop\tbb\document\sa\Pierre\code.txt 的 code.txt, code.dat
		helper.getValueByFieldName("TSFACN");
		
		return helper;
	}
}
