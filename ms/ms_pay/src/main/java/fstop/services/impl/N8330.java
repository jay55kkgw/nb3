package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class N8330 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("n8330Telcomm")
	private TelCommExec n8330Telcomm;
	
//	@Required
//	public void setN8330Telcomm(TelCommExec telcomm) {
//		n8330Telcomm = telcomm;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 
		params.put("DATE", DateTimeUtils.getCDateShort(new Date()));
		params.put("TIME", DateTimeUtils.format("HHmmss", new Date()));
		TelcommResult result = n8330Telcomm.query(params);
		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));

		return result;
	}


}
