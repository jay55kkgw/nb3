package fstop.services.impl;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;
import java.util.UUID;

import javax.xml.namespace.QName;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.google.gson.Gson;
import com.netbank.util.ESAPIUtil;

import fstop.ws.ebill.bean.TaiWater;
import com.tbb.ebill.util.MessageCode;
import com.tbb.ebill.webservice.client.ebill.IWebApi;
import com.tbb.ebill.webservice.server.jaxws.WsEbillService;
import com.tbb.ebill.webservice.server.jaxws.WsEbillServiceImpl;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.TrnsDataDao;
import fstop.orm.dao.TrnsPayDao;
import fstop.orm.dao.TrnsXmlDao;
import fstop.orm.po.TRNSPAY;
import fstop.services.CommonService;
import fstop.util.DateTimeUtils;
import fstop.ws.ebill.EBPayUtil;
import fstop.ws.ebill.EbillBean;
import lombok.extern.slf4j.Slf4j;


/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class EBPay1_1 extends CommonService{
//	private Logger logger = Logger.getLogger(getClass());
	
	@Value("${EBILL_WSDL}")
	String ebillwsdl;

	@Value("${EBILL_SRCID}")
	String ebillsrcid;
	
	@Value("${cert_path}")
	String cert_path;

	@Value("${soci}")
	String soci;

	@Value("${VA_CHANNEL}")
	String vaChannel;

	@Value("${EBILL_KEYID}")
	String ebillKeyID;

	@Value("${EBILL_connectionTimeout}")
	Integer ebill_connectionTimeout;
	
	@Value("${EBILL_receiveTimeout}")
	Integer ebill_receiveTimeout;
	
	@Autowired
	TrnsDataDao trnsDataDao;
	@Autowired
	TrnsXmlDao trnsXmlDao;
	@Autowired
	TrnsPayDao trnsPayDao;
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 	
		
		MVHImpl result = new MVHImpl();
		log.debug("Start Do Job");
		result=doJob(params);						
		return result;
	}
	
	private MVHImpl doJob(Map params){
		String CMDATE1 = params.get("CMDATE1").toString();//代收期限-民國年
		String CMDATE2 = params.get("CMDATE2").toString();//代收期限-財金(轉西元年)
		String WAT_NO = params.get("WAT_NO").toString();
		String CHKCOD = params.get("CHKCOD").toString();
		String AMOUNT = params.get("AMOUNT").toString();
		String CARDNUM = params.get("CARDNUM").toString();
		String EXPDTA = params.get("EXPDTA").toString();
		String CHECKNO = params.get("CHECKNO").toString();
		MVHImpl result = new MVHImpl();
		String MSGCODE="";
        //呼叫全國繳費網 朝立中台
		TaiWater billData = new TaiWater();
		billData.setBill("TaiWater");
		billData.setAction("PayReq");
		billData.setAccess("NB");
		billData.setFeeRefId("00161002"); //00161002-台灣自來水股份有限公司-台灣省自來水水費
		billData.setSessionId(params.get("SessionId").toString());
		billData.setHc(params.get("FEE").toString());			
		//TXN_INFO
		billData.setPaymentDeadLine(CMDATE2);        //代收期限
		billData.setNoticeNo(WAT_NO);       		 //銷帳編號
		billData.setCheckNo(CHKCOD);                 //查核碼
		billData.setTxnAmount(AMOUNT);               //應繳總金額
		//PAY_INFO
		billData.setPayType("CC");
		billData.setCardNumber(CARDNUM); 			//卡號
		billData.setExpireDate(EXPDTA);             //卡片有效格式YYMM
		billData.setCvv2(CHECKNO);                  //卡片未3碼
		billData.setAcqBin("050");                  //收單機構代碼
		String uuid = UUID.randomUUID().toString();
		billData.setTrnsNo(uuid);
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyDDDHHmmssSSS");
			String df = sdf.format(new Date());
			billData.setRrn(df.substring(3, 15)); //太陽日YDDDHHMMSSTT
		}
		catch(Exception e) {
			log.warn("setRrn Exception:"+e.getMessage());
		}
		//billData.setTerminalId("12345678");             //端未設備代號
		billData.setTerminalId("99999999");             //端未設備代號
		billData.setTrnsCode(params.get("TrnsCode").toString());

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		String df = sdf.format(new Date());
		log.debug(df);
		billData.setTxnDateTime(df);
		billData.setSrcId(ebillsrcid);
		try{
			//存入交易TRNSDATA
			trnsDataDao.insertTrnsData(billData.getTrnsNo(), billData.getAccess(), billData.getBill(), billData.getAction(), billData.getTxnDateTime());
			//從 TRNSDATA 取得 apitxnno
			int apitxnno = trnsDataDao.queryTrnsData(billData.getTrnsNo());
			billData.setApiTxnNo(EBPayUtil.padding(Integer.toString(apitxnno), 7, "L", "0"));
			//存入TRNSCHECK
			TRNSPAY trnsPay = new TRNSPAY();
			trnsPay.setTXN_DATETIME(billData.getTxnDateTime());
			trnsPay.setRCODE(billData.getRcode());
			trnsPay.setRCODE_DESC(billData.getRcodeDesc());
			trnsPay.setSRC_ID(billData.getSrcId());
			trnsPay.setFEE_REF_ID(billData.getFeeRefId());
			trnsPay.setSESSION_ID(billData.getSessionId());
			trnsPay.setHC(billData.getHc());
			trnsPay.setTXN_AMOUNT(billData.getTxnAmount());
			trnsPay.setSTAN(billData.getStan());
			trnsPay.setPAYTYPE(billData.getPayType());
			trnsPay.setTRABANKID(billData.getTraBankId());
			trnsPay.setTRAACCOUNT(billData.getTraAccount());
			trnsPay.setPAYIDNBAN(billData.getPayIdnBan());
			trnsPay.setTRNSCODE(billData.getTrnsCode());
			trnsPay.setAPI_TXN_NO(apitxnno);
			trnsPay.setMTI("0900");
			trnsPay.setPCODE("7150");
			trnsPayDao.save(trnsPay);
			
			String sendEbillXmlMsg = EBPayUtil.preReqTwWater_PayReq(billData, vaChannel, ebillKeyID);
			//存入TRNSXML
			trnsXmlDao.insertTrnsXml(apitxnno, "0900", "7150", df, sendEbillXmlMsg);
			
			log.debug("preRepTwWater >> " + sendEbillXmlMsg);
			String rcvEbillXmlMsg = EbillBean.payReq(sendEbillXmlMsg, ebillwsdl, cert_path, soci, ebill_connectionTimeout, ebill_receiveTimeout);
			log.debug("result >> " + rcvEbillXmlMsg);
			//存入TRNSXML
			trnsXmlDao.insertTrnsXml(apitxnno, "0910", "7150", df, rcvEbillXmlMsg);
			
			EBPayUtil.preRepTwWater_PayReq(billData, rcvEbillXmlMsg, vaChannel, ebillKeyID);
			//存入TRNSCHECK
			trnsPay = trnsPayDao.findById(apitxnno);
			if(trnsPay!= null) {
				if(billData.getTxnDateTime() != null)
					trnsPay.setTXN_DATETIME(billData.getTxnDateTime());
				if(billData.getRcode() != null)
					trnsPay.setRCODE(billData.getRcode());
				if(billData.getRcodeDesc() != null)
					trnsPay.setRCODE_DESC(billData.getRcodeDesc());
				if(billData.getSrcId() != null)
					trnsPay.setSRC_ID(billData.getSrcId());
				if(billData.getFeeRefId() != null)
					trnsPay.setFEE_REF_ID(billData.getFeeRefId());
				if(billData.getSessionId() != null)
					trnsPay.setSESSION_ID(billData.getSessionId());
				if(billData.getHc() != null)
					trnsPay.setHC(billData.getHc());
				if(billData.getTxnAmount() != null)
					trnsPay.setTXN_AMOUNT(billData.getTxnAmount());
				if(billData.getStan() != null)
					trnsPay.setSTAN(billData.getStan());
				if(billData.getPayType() != null)
					trnsPay.setPAYTYPE(billData.getPayType());
				if(billData.getTraBankId() != null)
					trnsPay.setTRABANKID(billData.getTraBankId());
				if(billData.getTraAccount() != null)
					trnsPay.setTRAACCOUNT(billData.getTraAccount());
				else
					trnsPay.setTRAACCOUNT(CARDNUM);
				if(billData.getPayIdnBan() != null)
					trnsPay.setPAYIDNBAN(billData.getPayIdnBan());
				if(billData.getTrnsCode() != null)
					trnsPay.setTRNSCODE(billData.getTrnsCode());
				trnsPay.setAPI_TXN_NO(apitxnno);
				trnsPay.setMTI("0910");
				trnsPay.setPCODE("7150");
				trnsPayDao.update(trnsPay);
			}
			
			TaiWater rcvBillData = null;
			if (billData!=null){
				rcvBillData = billData;
				String VrfyMacRslt = rcvBillData.isVrfyMacRslt()==true ? "Y" : "N";
				String TrnsCode = rcvBillData.getTrnsCode()==null ? "" : rcvBillData.getTrnsCode();
				String Rcode = rcvBillData.getRcode()==null ? "" : rcvBillData.getRcode();					
				log.warn("VrfyMacRslt: " + rcvBillData.isVrfyMacRslt());
				log.warn("TrnsCode: " + rcvBillData.getTrnsCode());
				log.warn("TxnDateTime: " + rcvBillData.getTxnDateTime());
				log.warn("ApiTxnNo: " + rcvBillData.getApiTxnNo());
				log.warn("Rcode: " + rcvBillData.getRcode() + "(" + MessageCode.getCodeDesc(rcvBillData.getRcode()) + ")");
				log.warn("SessionId: " + rcvBillData.getSessionId());
				log.warn("Stan: " + rcvBillData.getStan());
				log.warn(ESAPIUtil.vaildLog("NoticeNo: " + rcvBillData.getNoticeNo()));
				log.warn("TxnAmount: " + rcvBillData.getTxnAmount());
				log.warn("AuthCode: " + rcvBillData.getAuthCode());
				if(!VrfyMacRslt.equals("Y") || !TrnsCode.equals("0000") || !Rcode.equals("0001"))  //ebill交易失敗
				{						
					if(!VrfyMacRslt.equals("Y"))
					{	
						result.getFlatValues().put("occurMSG","0302");
						MSGCODE="0302";
					}
					if(!TrnsCode.equals("0000") && Rcode.length()==0)
					{
						result.getFlatValues().put("occurMSG","ZX99");
						MSGCODE="ZX99";
					}
					if(VrfyMacRslt.equals("Y") && !Rcode.equals("0001"))
					{
						result.getFlatValues().put("occurMSG1",Rcode);
						result.getFlatValues().put("occurMSG1DESC",MessageCode.getCodeDesc(Rcode));
						MSGCODE=Rcode;
					}
					if(TrnsCode.equals("1002"))
					{
						result.getFlatValues().put("occurMSG","Z621");
						MSGCODE="Z621";
					}	
				}
				else
				{
					if(VrfyMacRslt.equals("Y") && TrnsCode.equals("0000") && Rcode.equals("0001"))
					{	
						result.getFlatValues().put("occurMSG","0000");  //ebill交易成功
						MSGCODE="0000";
						result.getFlatValues().put("CMDATE1",CMDATE1);
						result.getFlatValues().put("WAT_NO",WAT_NO);
						result.getFlatValues().put("CHKCOD",CHKCOD);
						result.getFlatValues().put("AMOUNT",AMOUNT);
						result.getFlatValues().put("CARDNUM",CARDNUM);
						result.getFlatValues().put("EXPDTA",EXPDTA);
						result.getFlatValues().put("CHECKNO",CHECKNO);
						result.getFlatValues().put("FEE",params.get("FEE").toString());
					}								
				}					
			}
			result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
			params.put("ADEXCODE", MSGCODE);
			log.warn("EBPay1_1.java ebill MSGCODE:"+MSGCODE);
		}catch(Exception e)
		{
			log.warn("EBPay1_1.java ebill Exception:"+e.getMessage());
			result.getFlatValues().put("occurMSG","ZX99");
			MSGCODE="ZX99";
			params.put("ADEXCODE", MSGCODE);								
		}
		return result;			
	}		
}
