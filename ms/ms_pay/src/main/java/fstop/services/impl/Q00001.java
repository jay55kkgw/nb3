package fstop.services.impl;

import java.util.LinkedList;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.netbank.util.CodeUtil;
import com.netbank.util.ResultCode;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.services.CommonService;
import fstop.ws.EAIWebServiceTemplate;
import fstop.ws.eai.bean.Q00001RQ;
import fstop.ws.eai.bean.Q00001RS;
import fstop.ws.eai.bean.Q00001RS_Data;
import fstop.ws.eai.bean.TD01RQ;
import fstop.ws.eai.bean.TD01RS;
import fstop.ws.eai.bean.TD01RS_Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Q00001  extends CommonService {

	@Autowired
	private EAIWebServiceTemplate eaiwebservicetemplate;

	@SuppressWarnings("unchecked")
	public MVH doAction(Map params) {
		MVHImpl result = new MVHImpl();
		try {
			Q00001RS rs = new Q00001RS();
			params.put("MSGNAME", "Q00001");
			params.put("APP", "XML");
			params.put("SYSId", "CPBMB");
			params.put("PWD", "1234");
			params.put("TXNTYPE", "Q00001");
			params.put("IDentity", params.get("CUSIDN"));
			params.put("MAC", "");

			rs = getEaiRS(params);

			MVHImpl helperTD01 = new MVHImpl();
			LinkedList<Q00001RS_Data> rsData = new LinkedList<Q00001RS_Data>();
			rsData = rs.getData();
			if(rsData != null) {
				// 抓取RECCNT做迴圈取資料
				for (int j = 0; j < rsData.size(); j++) {
					Map<String, String> data = CodeUtil.objectCovert(Map.class, rsData.get(j));
					log.trace("data row >>{}", data);
					Row row = new Row(data);
					result.getOccurs().addRow(row);
				}
			}
			result.getFlatValues().put("TXNTYPE",rs.getTXNTYPE());
			result.getFlatValues().put("RESULTCODE",rs.getRESULTCODE());
			result.getFlatValues().put("TOPMSG",rs.getRESULTCODE());
			result.getFlatValues().put("MSG",rs.getMSG());
			result.getFlatValues().put("RECORDS",rs.getRECORDS());
			result.getFlatValues().put("StatusDesc",rs.getStatusDesc());
		} catch (TopMessageException e) {
			throw e;
		}
		return result;
	}

	private Q00001RS getEaiRS(Map params) {
		Q00001RQ rq = CodeUtil.objectCovert(Q00001RQ.class, params);
		try {
			Q00001RS rs = eaiwebservicetemplate.sendAndReceive(rq, Q00001RS.class);
			return rs;
		} catch (TopMessageException e) {
			throw e;
		}

	}
}
