package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N830 extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("n830Telcomm")
	private TelCommExec n830Telcomm;
	 
//	@Required
//	public void setN830Telcomm(TelCommExec telcomm) {
//		n830Telcomm = telcomm;
//	}
	
	@Override	
	public MVH doAction(Map params) {    
		Map<String, String> p = params;

		p.put("DATE", DateTimeUtils.getCDateShort(new Date()));
		p.put("TIME", DateTimeUtils.format("HHmmss", new Date()));
		//FGTXWAY 為交易機制
		if("0".equals(params.get("FGTXWAY"))) {   // 交易密碼
			params.put("CERTACN", StrUtils.repeat(" ", 19));	//即時
		}
		else if("1".equals(params.get("FGTXWAY"))) {  //憑證
			params.put("CERTACN", StrUtils.repeat("1", 19));	//即時
		}
		else if("2".equals(params.get("FGTXWAY"))) {  //晶片金融卡
			params.put("CERTACN", StrUtils.repeat("4", 19));	//即時
		}else if("7".equals(params.get("FGTXWAY"))) {  //晶片金融卡
			params.put("CERTACN", StrUtils.repeat("C", 19));	//即時
		}
		else if("7".equals(params.get("FGTXWAY"))) {  //晶片金融卡
			params.put("CERTACN", StrUtils.repeat("C", 19));	//即時
		}
		else if("7".equals(params.get("FGTXWAY"))) {  //IDGATE
			params.put("CERTACN", StrUtils.repeat("C", 19));	//即時
		}
		TelcommResult helper = n830Telcomm.query(params);
		helper.getFlatValues().put("CMQTIME",
				DateTimeUtils.getDatetime(new Date()));
		
		helper.getValueByFieldName("TSFACN");
		
		
		return helper;
	}



}
