package fstop.services.impl;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

import javax.xml.namespace.QName;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import fstop.core.BeanUtils;
import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.TrnsCheckDao;
import fstop.orm.dao.TrnsDataDao;
import fstop.orm.dao.TrnsXmlDao;
import fstop.orm.po.TRNSCHECK;
import fstop.orm.po.TRNSDATA;
import fstop.services.CommonService;
import fstop.ws.ebill.EBPayUtil;
import fstop.ws.ebill.EbillBean;
import lombok.extern.slf4j.Slf4j;

import fstop.ws.ebill.bean.TaiWater;

import com.netbank.util.ESAPIUtil;
import com.tbb.ebill.util.MessageCode;
import com.tbb.ebill.util.StringUtil;
import com.tbb.ebill.webservice.client.ebill.IWebApi;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class EBPay1 extends CommonService{
//	private Logger logger = Logger.getLogger(getClass());
	
	
	@Value("${EBILL_WSDL}")
	String ebillwsdl;

	@Value("${EBILL_SRCID}")
	String ebillsrcid;
	
	@Value("${cert_path}")
	String cert_path;

	@Value("${soci}")
	String soci;

	@Value("${VA_CHANNEL}")
	String vaChannel;

	@Value("${EBILL_KEYID}")
	String ebillKeyID;
	
	@Value("${EBILL_connectionTimeout}")
	Integer ebill_connectionTimeout;
	
	@Value("${EBILL_receiveTimeout}")
	Integer ebill_receiveTimeout;
	
	@Autowired
	TrnsDataDao trnsDataDao;
	@Autowired
	TrnsCheckDao trnsCheckDao;
	@Autowired
	TrnsXmlDao trnsXmlDao;
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) {
		String CMDATE1 = params.get("CMDATE1").toString();   //代收期限-民國年
		String CMDATE2 = String.valueOf(Integer.parseInt((params.get("CMDATE1").toString()).substring(0,3))+1911)+CMDATE1.substring(3);  //代收期限-財金(轉西元年)
		String WAT_NO = params.get("WAT_NO").toString();	//銷帳編號
		String CHKCOD = params.get("CHKCOD").toString();	//查核碼
		String AMOUNT = params.get("AMOUNT").toString();    //應繳金額
		MVHImpl result = new MVHImpl();
		String FEE="";

		 //呼叫全國繳費網 朝立中台 ，取手續費
		TaiWater billData = new TaiWater();
		billData.setBill("TaiWater");
		billData.setAction("CheckReq");
		
		billData.setAccess("NB");
		billData.setFeeRefId("00161002"); //00161002-台灣自來水股份有限公司-台灣省自來水水費			
		String VrfyMacRslt ="";
		String TrnsCode = "";
		String Rcode = "";
		String SessionId = "";
		//TXN_INFO
		billData.setPaymentDeadLine(CMDATE2);        //代收期限
		billData.setNoticeNo(WAT_NO);       		 //銷帳編號
		billData.setCheckNo(CHKCOD);                 //查核碼
		billData.setTxnAmount(AMOUNT);               //應繳總金額
		String uuid = UUID.randomUUID().toString();
		billData.setTrnsNo(uuid);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		String df = sdf.format(new Date());
		log.debug(df);
		billData.setTxnDateTime(df);
		billData.setSrcId(ebillsrcid);
		try {
			//存入交易TRNSDATA
			trnsDataDao.insertTrnsData(billData.getTrnsNo(), billData.getAccess(), billData.getBill(), billData.getAction(), billData.getTxnDateTime());
			//從 TRNSDATA 取得 apitxnno
			int apitxnno = trnsDataDao.queryTrnsData(billData.getTrnsNo());
			billData.setApiTxnNo(EBPayUtil.padding(Integer.toString(apitxnno), 7, "L", "0"));
			//存入TRNSCHECK
			trnsCheckDao.insertTrnsCheck(apitxnno, billData.getFeeRefId(), billData.getSessionId(), billData.getSrcId(), billData.getRcode(), billData.getTrnsCode(), billData.getTxnDateTime());
			
			//產生 XML
			String sendEbillXmlMsg = EBPayUtil.preReqTwWater_CheckReq(billData, vaChannel, ebillKeyID);
			//存入TRNSXML
			trnsXmlDao.insertTrnsXml(apitxnno, "0900", "7131", df, sendEbillXmlMsg);
			
			//送出電文
			log.debug("preRepTwWater >> " + sendEbillXmlMsg);
			String rcvEbillXmlMsg = EbillBean.queryReq(sendEbillXmlMsg, ebillwsdl, cert_path, soci, ebill_connectionTimeout, ebill_receiveTimeout);
			log.debug("result >> " + rcvEbillXmlMsg);
			//存入TRNSXML
			trnsXmlDao.insertTrnsXml(apitxnno, "0910", "7131", df, rcvEbillXmlMsg);
			
			//解讀 XML
			EBPayUtil.preRepTwWater_CheckReq(billData,rcvEbillXmlMsg, vaChannel, ebillKeyID);
			//更新 TRNSCHECK
			trnsCheckDao.updateTrnsCheck(apitxnno, billData.getFeeRefId(), billData.getSessionId(), billData.getSrcId(), billData.getRcode(), billData.getTrnsCode(), billData.getTxnDateTime());
			
			TaiWater rcvBillData = null;
			if (billData!=null){
				rcvBillData = (TaiWater)billData;
				VrfyMacRslt=rcvBillData.isVrfyMacRslt()==true ? "Y" : "N";
				TrnsCode= rcvBillData.getTrnsCode()==null ? "" :  rcvBillData.getTrnsCode();
				Rcode = rcvBillData.getRcode() == null ? "" : rcvBillData.getRcode();
				SessionId = rcvBillData.getSessionId()==null ? "" : rcvBillData.getSessionId();
				FEE= rcvBillData.getCc()==null ? "0" : rcvBillData.getCc();
				log.warn(ESAPIUtil.vaildLog("EBPay1.java CMDATE1: " + CMDATE1));
				log.warn(ESAPIUtil.vaildLog("EBPay1.java CMDATE2: " + CMDATE2));
				log.warn(ESAPIUtil.vaildLog("EBPay1.java WAT_NO: " + WAT_NO));
				log.warn(ESAPIUtil.vaildLog("EBPay1.java CHKCOD: " + CHKCOD));
				log.warn(ESAPIUtil.vaildLog("EBPay1.java AMOUNT: " + AMOUNT));
				log.warn("EBPay1.java VrfyMacRslt: " + rcvBillData.isVrfyMacRslt()); // true mac成功
				log.warn("EBPay1.java TrnsCode: " + rcvBillData.getTrnsCode());   //0000  代表成功
				log.warn("EBPay1.java TxnDateTime: " + rcvBillData.getTxnDateTime());
				log.warn("EBPay1.java ApiTxnNo: " + rcvBillData.getApiTxnNo());
				log.warn("EBPay1.java Rcode: " + rcvBillData.getRcode() + "(" + MessageCode.getCodeDesc(rcvBillData.getRcode()) + ")");   //0001代表對方回應成功，反之則回應錯誤訊息
				log.warn("EBPay1.java SessionId: " + rcvBillData.getSessionId());   //成功會取得session id
				log.warn("EBPay1.java Cc: " + rcvBillData.getCc()); //手續費
				result.getFlatValues().put("CMQTIME", rcvBillData.getTxnDateTime());					
				result.getFlatValues().put("FEE", FEE);
				result.getFlatValues().put("VrfyMacRslt", VrfyMacRslt);
				result.getFlatValues().put("TrnsCode", TrnsCode);
				result.getFlatValues().put("Rcode", Rcode);
				result.getFlatValues().put("SessionId", SessionId);
				result.getFlatValues().put("CMDATE2", CMDATE2);
				if(VrfyMacRslt.equals("Y") && !Rcode.equals("0001"))
				{
					result.getFlatValues().put("RDESC", MessageCode.getCodeDesc(Rcode));
				}	
			}
		}catch (TopMessageException e) {
			log.error("EBPay1.java ebill Exception: >> {}",e);
			throw e;
		}catch(Exception e)
		{
			log.error("EBPay1.java ebill Exception: >> {}",e);
		}		 		
		return result;
	}

}
