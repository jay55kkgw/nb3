package fstop.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import encryptUtil.EncryptUtil;
import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.orm.dao.AdmMsgCodeDao;
import fstop.orm.dao.SysDailySeqDao;
import fstop.orm.dao.TxnReqInfoDao;
import fstop.orm.dao.TxnTwRecordDao;
import fstop.orm.dao.TxnTwSchPayDao;
import fstop.orm.dao.TxnTwSchPayDataDao;
import fstop.orm.dao.TxnTwScheduleDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.ADMMAILLOG;
import fstop.orm.po.TXNTWRECORD;
import fstop.orm.po.TXNTWSCHPAY;
import fstop.orm.po.TXNTWSCHPAYDATA;
import fstop.services.BookingAware;
import fstop.services.CommonService;
import fstop.services.NoticeInfo;
import fstop.services.NoticeInfoGetter;
import fstop.services.OnLineTransferNotice;
//import fstop.services.ScheduleTransferNotice;
//import fstop.services.ScheduleTransferReSendNotice;
import fstop.services.ServiceBindingException;
import fstop.services.TelCommExecWrapper;
import fstop.services.TransferNotice;
import fstop.services.WriteLogInterface;
//import fstop.services.batch.TxnFxTransfer;
import fstop.telcomm.TelCommExec;
import fstop.telcomm.cmd.CommandUtils;
import fstop.util.DateTimeUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import topibs.utility.NumericUtil;

@Slf4j
public class N171 extends CommonService implements BookingAware, WriteLogInterface {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	private TxnTwScheduleDao txnTwScheduleDao;

	@Autowired
	private TxnTwRecordDao txnTwRecordDao;

	@Autowired
    @Qualifier("n071Telcomm")
	private TelCommExec n071Telcomm;

	@Autowired
    @Qualifier("n971Telcomm")
	private TelCommExec n971Telcomm;

	@Autowired
	private TxnTwSchPayDao txntwschpaydao;
	
	@Autowired
	private TxnTwSchPayDataDao txntwschpaydatadao;
	
	@Autowired
	private SysDailySeqDao sysDailySeqDao;

	@Autowired
	private TxnReqInfoDao txnReqInfoDao;

	@Autowired
	private AdmMsgCodeDao admMsgCodeDao;

	@Autowired
	private TxnUserDao txnUserDao;


//	@Required
//	public void setN071WSClient(N071WebServiceClient n071WSClient) {
//		this.n071WSClient = n071WSClient;
//	}	
//		
//	@Required
//	public void setTxnUserDao(TxnUserDao txnUserDao) {
//		this.txnUserDao = txnUserDao;
//	}
//	
//	public void setAdmMsgCodeDao(AdmMsgCodeDao admMsgCodeDao) {
//		this.admMsgCodeDao = admMsgCodeDao;
//	}
//	
//	public void setTxnTwScheduleDao(TxnTwScheduleDao txnTwScheduleDao) {
//		this.txnTwScheduleDao = txnTwScheduleDao;
//	}
//	
//	@Required
//	public void setTxnTwRecordDao(TxnTwRecordDao txnTwRecordDao) {
//		this.txnTwRecordDao = txnTwRecordDao;
//	}
//	
//	@Required
//	public void setTxnReqInfoDao(TxnReqInfoDao txnreqinfodao) {
//		this.txnReqInfoDao = txnreqinfodao;
//	}
//
//	@Required
//	public void setN071Telcomm(TelCommExec telcomm) {
//		n071Telcomm = telcomm;
//	}
//
//	@Required
//	public void setN971Telcomm(TelCommExec telcomm) {
//		n971Telcomm = telcomm;
//	}
//	
//	public N171(){}
//	
//	@Required
//	public void setSysDailySeqDao(SysDailySeqDao sysDailySeqDao) {
//		this.sysDailySeqDao = sysDailySeqDao;
//	}

	@Override
	public MVH doAction(Map _params) {
		Map<String, String> params = _params;
		
		//轉入帳號
		String trin_acn = "";
		String taxType1 = params.get("TAXTYPE1");
		if ("11".equals(taxType1))
		{//綜合所得稅用銷帳編號
			trin_acn = params.get("INTSACN3");
			
			String paydue = (String)params.get("PAYDUED");
			paydue = StrUtils.trim(paydue).replaceAll("/", "");   // 將 yyy/MM/dd 的 "/" 去掉, 變成 yyyMMdd
			params.put("PAYDUED", StrUtils.right(paydue, 6));
		}
		else if ("15".equals(taxType1))
		{//營利事業所得稅用稽徵單位代號+統編
			trin_acn = params.get("INTSACN1") + params.get("INTSACN2");
		}
		params.put("__TRIN_ACN", trin_acn );

		//晶片金融卡
		if("2".equals(params.get("FGTXWAY").toString()))
		{
			Date d = new Date();
			String trin_transeq = "1234";
			String trin_issuer = "05000000";
			String trin_acnno = params.get("ACNNO");
			String trin_icdttm = DateTimeUtils.format("yyyyMMddHHmmss", d);
			log.trace("ICDTTM: {}", trin_icdttm);
			String trin_iseqno = params.get("iSeqNo");
			String trin_icmemo = "";
			String trin_tac_length = "000A";		
			String trin_tac = params.get("TAC");
			log.trace(ESAPIUtil.vaildLog("TAC: {}"+ trin_tac_length + trin_tac + "(" + (trin_tac_length.length() + trin_tac.length()) + ")"));
			String trin_trmid = params.get("TRMID");
		
			params.put("TRANSEQ", trin_transeq);
			params.put("ISSUER", trin_issuer);
			params.put("ACNNO", trin_acnno);
			params.put("ICDTTM", trin_icdttm);
			params.put("ICSEQ", trin_iseqno);
			params.put("ICMEMO", trin_icmemo);
			params.put("TAC_Length", trin_tac_length);
			params.put("TAC", trin_tac);
			params.put("TRMID", trin_trmid);
		}
		
		String f = params.get("FGTXDATE");
		if("1".equals(f)) {  //		即時
			
			OnLineTransferNotice trNotice = new OnLineTransferNotice();
		
			return online(params, trNotice);
		}
		else if("2".equals(f)) {  //預約
			
			if("0".equals(params.get("FGTXWAY"))) {  //使用交易密碼, 做預約
//				String TransPassUpdate = (String)params.get("TRANSPASSUPDATE");
//				String Phase2 = (String)params.get("PHASE2");
				TelCommExec n950CMTelcomm;
//				
//				if(StrUtils.isNotEmpty(TransPassUpdate))
//				{
//					if(TransPassUpdate.equals("1") || Phase2.equals("ON"))
//					{
						n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n951CMTelcomm");
//					}
//					else
//					{
//						n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n950CMTelcomm");
//					}
//				}
//				else
//				{
//					n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n950CMTelcomm");
//				}
				MVHImpl n950Result = n950CMTelcomm.query(params);
				//若驗證失敗, 則會發動 TopMessageException
			}
			else if("2".equals(params.get("FGTXWAY"))) {   //使用晶片金融卡, 做預約
				
				TelCommExec n954Telcomm = (TelCommExec)SpringBeanFactory.getBean("n954Telcomm");
				MVHImpl n954Result = n954Telcomm.query(params);
				//若驗證失敗, 則會發動 TopMessageException
			}
			
			return booking(params);
		}
		else {
			throw new ServiceBindingException("錯誤的 FGTXDATE TYPE.");
		}
		
	}
	
	
	/**
	 * 預約交易
	 * @param params
	 * @return
	 */
	@Override
	public MVHImpl booking(Map<String, String> params) {
		log.trace(ESAPIUtil.vaildLog("N071.booking.params: {}"+ params));
		TXNTWSCHPAY txntwschpay = new TXNTWSCHPAY();
		
		//產生 CERTACN update by Blair -------------------------------------------- Start
		String TxWay = params.get("FGTXWAY");
		String certACN = "";
		
		if ("0".equals(TxWay)) {
			while (certACN.length() < 19)
				certACN += "2";
		} else if ("1".equals(TxWay)) {
			while (certACN.length() < 19)
				certACN += "3";
		} else if ("7".equals(TxWay)) {
			while (certACN.length() < 19)
				certACN += "D";
		} else {
			while (certACN.length() < 19)
				certACN += "5";
		}
		params.put("CERTACN", certACN);
		//產生 CERTACN update by Blair -------------------------------------------- End
		
		
		// 操作功能ID
		txntwschpay.setADOPID(params.get("ADOPID"));
		// 使用者ID
		txntwschpay.setDPUSERID(params.get("CUSIDN"));
		// 轉出帳號
		txntwschpay.setDPWDAC(params.get("OUTACN"));
		// 轉入行庫代碼
		txntwschpay.setDPSVBH("050");
		// 轉入帳號/繳費代號
		txntwschpay.setDPSVAC(params.get("__TRIN_ACN"));
		// 轉帳金額
		txntwschpay.setDPTXAMT(params.get("AMOUNT"));
		// 備註
		txntwschpay.setDPTXMEMO(params.get("CMTRMEMO"));
		// 發送Mail清單
		txntwschpay.setDPTXMAILS(params.get("CMTRMAIL"));
		// Mail備註
		txntwschpay.setDPTXMAILMEMO(params.get("CMMAILMEMO"));
		// 交易機制
		txntwschpay.setDPTXCODE(TxWay);
		// 取得預約編號(YYMMDD+三位流水號)
		String dptmpschno = txnUserDao.getDptmpschno(params.get("CUSIDN"));
		txntwschpay.setDPSCHNO(dptmpschno);
		// 預約狀態
		txntwschpay.setDPTXSTATUS("0");
		
		// 系統別
		txntwschpay.setLOGINTYPE(params.get("LOGINTYPE"));

		// 寫入日期
		Date d = new Date();
		txntwschpay.setDPSDATE(DateTimeUtils.format("yyyyMMdd", d));
		txntwschpay.setDPSTIME(DateTimeUtils.format("HHmmss", d));

		txntwschpay.setLASTDATE(txntwschpay.getDPSDATE());
		txntwschpay.setLASTTIME(txntwschpay.getDPSTIME());
		
		String f = params.get("FGTXDATE");
		if ("2".equals(f)) { // 預約某一日
			params.put("CMDATE", StrUtils.trim(params.get("CMDATE")).replaceAll("/", ""));
			if (params.get("CMDATE").length() == 0) {
				throw TopMessageException.create("ZX99");
			} else {
				txntwschpay.setDPTXTYPE("S");
				txntwschpay.setDPPERMTDATE("");
				txntwschpay.setDPFDATE(params.get("CMDATE"));
				txntwschpay.setDPTDATE(params.get("CMDATE"));
			}
		} else if ("3".equals(f)) {
			params.put("CMSDATE", StrUtils.trim(params.get("CMSDATE")).replaceAll("/", ""));
			params.put("CMEDATE", StrUtils.trim(params.get("CMEDATE")).replaceAll("/", ""));
			if (params.get("CMDD").length() == 0 || params.get("CMSDATE").length() == 0
					|| params.get("CMEDATE").length() == 0) {
				throw TopMessageException.create("ZX99");
			} else {
				txntwschpay.setDPTXTYPE("C");
				txntwschpay.setDPPERMTDATE(params.get("CMDD"));
				txntwschpay.setDPFDATE(params.get("CMSDATE"));
				txntwschpay.setDPTDATE(params.get("CMEDATE"));
			}
		}
		
		if (params.containsKey("pkcs7Sign") && "1".equals(params.get("FGTXWAY"))) {
			List<String> commands = new ArrayList();
			commands.add("XMLCA()");
			commands.add("XMLCN()");

			CommandUtils.doBefore(commands, params);

			if (!StrUtils.trim(params.get("XMLCN")).toUpperCase()
					.startsWith(StrUtils.trim(params.get("UID")).toUpperCase())) {
				throw TopMessageException.create("Z089");
			}

			txntwschpay.setXMLCA(StrUtils.trim(params.get("XMLCA")));
			txntwschpay.setXMLCN(StrUtils.trim(params.get("XMLCN")));
		}
		
		// TODO DPTXINFO放不下這兩個欄位，先拿掉，若預約交易執行時需要再看怎麼存
		params.remove("pkcs7Sign");
		params.remove("jsondc");
		// 預約時上行電文必要資訊
		log.trace(ESAPIUtil.vaildLog("txntwschpay.params>>>{}"+ params));
		String reqinfo_Str = "{}";
		String sha1_Mac = "";
		reqinfo_Str =  CodeUtil.toJson(params);
		sha1_Mac = EncryptUtil.toEncS1(reqinfo_Str, "UTF-8").substring(0,4);
		
		txntwschpay.setMAC(sha1_Mac);
		txntwschpay.setDPTXINFO(reqinfo_Str);
		txntwschpay.setMSADDR(TxnTwSchPayDao.MS_PAY);
		
		log.trace(ESAPIUtil.vaildLog("txntwschpay: {}"+ txntwschpay));
		txntwschpaydao.save(txntwschpay);
		
		MVHImpl result =  new MVHImpl();
		
		try {
			result.getFlatValues().put("CMTXTIME", DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", d));
			result.getFlatValues().put("TOPMSG", "0000");
			result.getFlatValues().put("MSGCOD", "0000");
		}
		catch(Exception e){
			log.error("",e);
		}
		
		return result;
	}	
	
	/**
	 * 預約交易_舊網銀
	 * @param params
	 * @return
	 */
//	@Override
//	public MVHImpl booking(Map<String, String> params) {
//		TXNTWSCHEDULE txntwschedule = new TXNTWSCHEDULE();
//		
//		//產生 CERTACN update by Blair -------------------------------------------- Start
//		String TxWay = params.get("FGTXWAY");
//		String certACN = "";
//		
//		if("0".equals(TxWay)){
//			while(certACN.length() < 19)certACN += "2";
//		}
//		else if("1".equals(TxWay)){
//			while(certACN.length() < 19)certACN += "3";
//		}
//		else {
//			while(certACN.length() < 19)certACN += "5";
//		}
//		params.put("CERTACN", certACN);
//		//產生 CERTACN update by Blair -------------------------------------------- End
//		
//		log.info("***************************************預約編號***************************************************");
//		//更新SCHCOUNT(每日預約計數)
//		int scnt = txnUserDao.incSchcount((String)params.get("UID"));
//		
//		//預約編號(YYMMDD+三位流水號)
//		String dptmpschno = "";
//		String ymd = DateTimeUtils.format("yyMMdd", new Date());
//		dptmpschno = ymd + StrUtils.right("000" + scnt, 3); 
//		
//		//賽入預約編號
//		txntwschedule.setDPSCHNO(dptmpschno);
//		log.info("***************************************預約編號***************************************************");
//		
//		String f = params.get("FGTXDATE");
//		//預約某一日
//		params.put("CMDATE", StrUtils.trim(params.get("CMDATE")).replaceAll("/", ""));
//		if(params.get("CMDATE").length()==0)
//		{
//			throw TopMessageException.create("ZX99");
//		}
//		else {
//			txntwschedule.setDPPERMTDATE("");
//			txntwschedule.setDPFDATE(params.get("CMDATE"));
//			txntwschedule.setDPTDATE(params.get("CMDATE"));				
//		}		
//		params.put("ADREQTYPE", "S");   //寫入 TXNLOG 時, S 表示為預約
//		
//		Map<String, String> mapping = new HashMap();
//		
//		mapping.put("DPUSERID", "UID");
//		mapping.put("DPWDAC", "OUTACN"); // 轉出帳號
//		//mapping.put("DPSVBH", "__TRIN_BANKCOD"); // 轉入分行
//		mapping.put("DPSVAC", "__TRIN_ACN"); // 轉入帳號, 使用原始輸入的來記錄
//		mapping.put("DPTXAMT", "AMOUNT");
//		mapping.put("DPTXMEMO", "CMTRMEMO");
//		mapping.put("DPTXMAILS", "CMTRMAIL");
//		mapping.put("DPTXMAILMEMO", "CMMAILMEMO"); // CMMAILMEMO 對應的欄位
//
//		mapping.put("ADOPID", "ADOPID");
//
//		mapping.put("DPTXCODE","FGTXWAY");
//
//		for(String key : mapping.keySet()) {
//			String v = mapping.get(key);
//			try {
//				BeanUtils.setProperty(txntwschedule, key, params.get(v));
//			} catch (IllegalAccessException e) {
//				throw new ToRuntimeException("", e);
//			} catch (InvocationTargetException e) {
//				throw new ToRuntimeException("", e);
//			}
//		}
//		
//		if(params.containsKey("pkcs7Sign") && "1".equals(params.get("FGTXWAY"))) {
//			List<String> commands = new ArrayList();
//			commands.add("XMLCA()");
//			commands.add("XMLCN()");
//			
//			CommandUtils.doBefore(commands, params);
//			
//			if(!StrUtils.trim(params.get("XMLCN")).toUpperCase().startsWith(StrUtils.trim(params.get("UID")).toUpperCase())) {
//				throw TopMessageException.create("Z089");
//			}
//			
//			txntwschedule.setXMLCA(StrUtils.trim(params.get("XMLCA")));
//			txntwschedule.setXMLCN(StrUtils.trim(params.get("XMLCN")));
//		}
//		
//		
//		TXNREQINFO titainfo = new TXNREQINFO();
//		titainfo.setREQINFO(BookingUtils.requestInfo(params));
//		txnReqInfoDao.save(titainfo);
//		
//		txntwschedule.setDPTXINFO( titainfo.getREQID());
//		
//		String sha1 = DigestUtils.sha1(titainfo.getREQINFO()).substring(0, 4);
//		txntwschedule.setMAC(sha1);
//		
//		Date d = new Date();
//		txntwschedule.setDPSDATE(DateTimeUtils.format("yyyyMMdd", d));
//		txntwschedule.setDPSTIME(DateTimeUtils.format("HHmmss", d));
//		
//		txntwschedule.setDPTXSTATUS("0");
//		txntwschedule.setLASTDATE(txntwschedule.getDPSDATE());
//		txntwschedule.setLASTTIME(txntwschedule.getDPSTIME());
//
//		txnTwScheduleDao.save(txntwschedule);
//		
//		MVHImpl result =  new MVHImpl();
//		
//		try {
//			result.getFlatValues().put("CMTXTIME", DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", d));
//		}
//		catch(Exception e){
//			log.error(e.toString());			
//		}
//		
//		return result;
//	}
	

	@Override
	public MVHImpl online(final Map<String, String> params, final TransferNotice trNotice) {
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);

		String f = params.get("FGTXDATE");		
		if("1".equals(f)) {  //		即時
			
			//產生 CERTACN update by Blair -------------------------------------------- Start
			String TxWay = params.get("FGTXWAY");
			String certACN = "";
			
			if ("0".equals(TxWay)) {
				while (certACN.length() < 19)
					certACN += " ";
			} else if ("1".equals(TxWay)) {
				while (certACN.length() < 19)
					certACN += "1";
			} else if ("7".equals(TxWay)) {
				while (certACN.length() < 19)
					certACN += "C";
			} else {
				while (certACN.length() < 19)
					certACN += "4";
			}
			params.put("CERTACN", certACN);
			//產生 CERTACN update by Blair -------------------------------------------- End
		}
		
		TelCommExec doexec = n071Telcomm;
		
		final TelCommExecWrapper execwrapper = new TelCommExecWrapper(doexec);
		
		params.put("PCSEQ", StrUtils.right("00000" + sysDailySeqDao.dailySeq("N070")+"", 5));

		params.put("TRNFLAG", "1");
		params.put("FLAG", "0");
		
		params.put("BNKRA", "050");   // 行庫別
		params.put("INTSACN", params.get("__TRIN_ACN"));
		params.put("TRNDAT", StrUtils.right(DateTimeUtils.getCDateShort(new Date()), 6));
		params.put("TRNTIM", DateTimeUtils.getTimeShort(new Date()));
		params.put("BANKIND", "000");
		params.put("TXNTYPE", "02"); //01:跨行轉帳  02:跨行繳稅 03:跨行繳費
		params.put("PAYTYPE", params.get("TAXTYPE2"));

		List<String> commands = new ArrayList();
		commands.add("SYNC1()");
		commands.add("PPSYNCN1()");
		commands.add("PINNEW1()");
		commands.add("MAC2({%11s,OUTACN} {%-16s, INTSACN} {%010d,AMOUNT} {%19s, CERTACN})");							
		if("1".equals(params.get("FGTXWAY"))) {
			commands.add("XMLCA()");
			commands.add("XMLCN()");								
		}	
		CommandUtils.doBefore(commands, params);
		log.debug("fisnsh doBefore...");
		
		if("1".equals(params.get("FGTXWAY")) && !StrUtils.trim(params.get("XMLCN")).toUpperCase().startsWith(StrUtils.trim(params.get("UID")).toUpperCase())) {
			throw TopMessageException.create("Z089");
		}
		if("2".equals(params.get("FGTXWAY")))
		{
//			params.put("TAC", params.get("TAC_Length")+params.get("TAC"));
			log.warn(ESAPIUtil.vaildLog("N171.java TAC:"+params.get("TAC")));
		}
		
		MVHImpl result = execwrapper.query(params);
		
		//先發送N071電文，須判斷下行的RSPCOD=”E901”時表示跨行交易尚未回傳資料，
		//應繼續發送N971電文詢問，直至N971下行電文RSPCOD不為”E901”
		
//		TelCommExec doexec = n071Telcomm;
//		
//		TelCommExecWrapper execwrapper = null;
//	
//		//重發時, PCSEQ 需帶入上一次的  (與跨行轉帳使用同一個序號記錄)
//		params.put("PCSEQ", StrUtils.right("00000" + sysDailySeqDao.dailySeq("N070")+"", 5));
//
//		params.put("TRNFLAG", "1");
//		params.put("FLAG", "0");
//		
//		params.put("BNKRA", "050");   // 行庫別
//		params.put("INTSACN", params.get("__TRIN_ACN"));
//		params.put("TRNDAT", StrUtils.right(DateTimeUtils.getCDateShort(new Date()), 6));
//		params.put("TRNTIM", DateTimeUtils.getTimeShort(new Date()));
//		params.put("BANKIND", "000");
//		params.put("TXNTYPE", "02"); //01:跨行轉帳  02:跨行繳稅 03:跨行繳費
//		params.put("PAYTYPE", params.get("TAXTYPE2"));
//		List<String> commands = new ArrayList();
//		commands.add("SYNC1()");
//		commands.add("PPSYNCN1()");
//		commands.add("PINNEW1()");
//		commands.add("MAC2({%11s,OUTACN} {%-16s, INTSACN} {%010d,AMOUNT} {%19s, CERTACN})");							
//		if("1".equals(params.get("FGTXWAY"))) {
//			commands.add("XMLCA()");
//			commands.add("XMLCN()");								
//		}	
//		CommandUtils.doBefore(commands, params);
//		if("1".equals(params.get("FGTXWAY")) && !StrUtils.trim(params.get("XMLCN")).toUpperCase().startsWith(StrUtils.trim(params.get("UID")).toUpperCase())) {
//			throw TopMessageException.create("Z089");
//		}
//		if("2".equals(params.get("FGTXWAY")))
//		{
//			params.put("TAC", params.get("TAC_Length")+params.get("TAC"));
//			log.warn("N171.java TAC:"+params.get("TAC"));
//		}
//		
//
//		
//		//將發送電文的邏輯再包一層
//		TelCommExec execN171 = new TelCommExec() {
//
//			public TelcommResult query(Map params) {
//				TelcommResult n071result = new TelcommResult(new Vector());
//				MVHImpl n071result1 = new MVHImpl();
//				try {
//					n071result = n071Telcomm.query(params); 
					
/*
 * 原臺企銀開發中程式--要走EAI卻還沒走
 * 
 * 
					
					n071result1 = n071WSClient.processGo(params);
					if(n071result1.getValueByFieldName("SYNC").equals("0302"))
						n071result1.getFlatValues().put("RSPCOD","0302");
					if(n071result1.getValueByFieldName("SYNC").equals("E089"))
						n071result1.getFlatValues().put("RSPCOD","E089");
					n071result.getFlatValues().put("RSPCOD", n071result1.getValueByFieldName("RSPCOD"));
					n071result.getFlatValues().put("PCSEQ", n071result1.getValueByFieldName("PCSEQ"));
					n071result.getFlatValues().put("STAN", n071result1.getValueByFieldName("STAN"));
					n071result.getFlatValues().put("OUTACN", n071result1.getValueByFieldName("OUTACN"));
					n071result.getFlatValues().put("O_TOTBAL", n071result1.getValueByFieldName("O_TOTBAL"));
					n071result.getFlatValues().put("O_AVLBAL", n071result1.getValueByFieldName("O_AVLBAL"));
					n071result.getFlatValues().put("INTSACN", n071result1.getValueByFieldName("INTSACN"));
					n071result.getFlatValues().put("BANKID", n071result1.getValueByFieldName("BANKID"));
					n071result.getFlatValues().put("AMOUNT", n071result1.getValueByFieldName("AMOUNT"));
					n071result.getFlatValues().put("DATE", n071result1.getValueByFieldName("DATE"));
					n071result.getFlatValues().put("TIME", n071result1.getValueByFieldName("TIME"));
					n071result.getFlatValues().put("FEE", n071result1.getValueByFieldName("FEE"));
					n071result.getFlatValues().put("MAC", n071result1.getValueByFieldName("MAC"));
					n071result.getFlatValues().put("PRIZETIMES", n071result1.getValueByFieldName("W_CNT"));
					if(!"".equals(n071result1.getValueByFieldName("RSPCOD").trim()) && !"0000".equals(n071result1.getValueByFieldName("RSPCOD").trim()))
					{
						throw TopMessageException.create(n071result1.getValueByFieldName("RSPCOD").trim(),params.get("ADOPID")==null ? "" : params.get("ADOPID").toString());
					}
					if("".equals(n071result1.getValueByFieldName("RSPCOD").trim()) &&  "".equals(n071result1.getValueByFieldName("OUTACN").trim()))
					{
						if(n071result1.getValueByFieldName("SYNC").length()>0)
						{	
							n071result1.getFlatValues().put("RSPCOD",n071result1.getValueByFieldName("SYNC"));
							throw TopMessageException.create(n071result1.getValueByFieldName("SYNC"),params.get("ADOPID")==null ? "" : params.get("ADOPID").toString());	
						}
						else
						{	
							n071result1.getFlatValues().put("RSPCOD","ZX99");
							throw TopMessageException.create("ZX99",params.get("ADOPID")==null ? "" : params.get("ADOPID").toString());	
						}	
					}
*/

//				}
//				catch(Exception e) {
//					//n071result.getFlatValues().put("RSPCOD","ZX99");
//					log.warn("N171.java EAI Exception:"+e.getMessage());
//					throw TopMessageException.create(n071result1.getValueByFieldName("RSPCOD")==null ? "ZX99" : n071result1.getValueByFieldName("RSPCOD"),params.get("ADOPID")==null ? "" : params.get("ADOPID").toString());
//				}
//				
//				n071result.getFlatValues().put("RSPCOD",n071result1.getValueByFieldName("RSPCOD"));

//				catch(TopMessageException e) {
//					if(!"E901".equals(e.getMessage())) {
//						throw e;
//					}
//					else {  //E901
//						n071result = new TelcommResult(new Vector());
//						n071result.getFlatValues().put("RSPCOD", "E901");
//					}
//				}

//				try {
					//Thread.sleep(2L*1000L*60L);
//				}
//				catch(Exception e) {}

//				if("E901".equals(n071result.getFlatValues().get("RSPCOD"))) {
//					
//					TelcommResult result = (TelcommResult)sendN971(params);  //查詢是否完成
//					if(result == null) {
//						throw TopMessageException.create("E901");
//					}
//					return result;
//				}

//				return n071result;
//			}
//		};
		
//		execwrapper = new TelCommExecWrapper(execN171);

//		final MVHImpl result = execwrapper.query(params);
		
		final TXNTWRECORD record = (TXNTWRECORD)writeLog(execwrapper, params, result);
		
		final TelCommExecWrapper execwrapperFinal = execwrapper;
		trNotice.sendNotice(new NoticeInfoGetter() {

			public NoticeInfo getNoticeInfo() { 
				NoticeInfo result = new NoticeInfo(params.get("UID"), params.get("DPMYEMAIL"), params.get("CMTRMAIL"));
				
//				result.setDpretxstatus(record.getDPRETXSTATUS());
//				log.debug("Dpretxstatus = " + record.getDPRETXSTATUS());
				result.setADTXNO(record.getADTXNO());
				result.setException(execwrapperFinal.getLastException());
				
				result.setTemplateName("TAX");
				
				String datetime = record.getDPTXDATE() + record.getDPTXTIME();
				Date d = DateTimeUtils.parse("yyyyMMddHHmmss", datetime);
				String trantime = DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", d);
				result.getParams().put("#TRANTIME", trantime);
				result.getParams().put("#SEQNO", record.getDPTXNO());
				
				String acn = record.getDPSVAC();
				try {
					acn = StrUtils.left(acn, acn.length() - 5) + "***" + StrUtils.right(acn, 2);
				}
				catch(Exception e){}
				
				String outacn = record.getDPWDAC();  //轉出帳號
				try {
					outacn = StrUtils.left(outacn, outacn.length() - 5) + "***" + StrUtils.right(outacn, 2);
				}
				catch(Exception e){}
				
				result.getParams().put("#ACN", acn);
				result.getParams().put("#OUTACN", outacn);
				result.getParams().put("#OUTCURRENCY", "新台幣");
				String dptxamtfmt = NumericUtil.formatNumberString(record.getDPTXAMT(), 0);
				result.getParams().put("#AMT", dptxamtfmt);
				result.getParams().put("#MEMO", params.get("CMMAILMEMO"));
				
				ADMMAILLOG admmaillog = new ADMMAILLOG();
				admmaillog.setADACNO(record.getDPWDAC());
				
				String trans_status = (String)params.get("__TRANS_STATUS");
				if(StrUtils.isEmpty(trans_status)) {
					trans_status = "TWONLINE";
				}
				else if("SEND".equals(trans_status)) {
					trans_status = "TWSCH";
				}
				else if("RESEND".equals(trans_status)) {
					trans_status = "TWSCHRE";
				}
				
				admmaillog.setADBATCHNO(trans_status);
				admmaillog.setADMAILACNO(StrUtils.trim(params.get("CMTRMAIL")));
				admmaillog.setADUSERID(record.getDPUSERID());
				admmaillog.setADUSERNAME("");
				admmaillog.setADSENDTYPE("3");
				admmaillog.setADSENDTIME(datetime);
				admmaillog.setADSENDSTATUS("");
				
				result.setADMMAILLOG(admmaillog);
				
				return result;
			}
			
		});
		
		execwrapper.throwException();

		result.getFlatValues().put("CMQTIME",
				DateTimeUtils.getDatetime(new Date()));
		
		try {
			result.getFlatValues().put("CMTXTIME", DateTimeUtils.getCDateTime(result.getValueByFieldName("DATE"),
					result.getValueByFieldName("TIME")));
		}
		catch(Exception e){
			log.error(e.toString());
		}
		 
		return result;		
	}
	
    /**
     * 每三秒再查詢一次
     * @param params
     * @return
     */
	private TelcommResult sendN971(Map params) {
		TelcommResult result = null;
		String rspcod = "";
		long start = new Date().getTime();
		do {
			try {
				result = n971Telcomm.query(params);
			}
			catch(TopMessageException e) {
				if(!"E901".equals(e.getMsgcode())) {
					throw e;
				}
				else {  //E901
					result = new TelcommResult(new Vector());
					result.getFlatValues().put("RSPCOD", "E901");
				}
			}
			rspcod = result.getValueByFieldName("RSPCOD");
			try {
				Thread.sleep(15000L);
			}
			catch(Exception e) {}
			
			if((new Date().getTime() - start) > 5 * 60 * 1000L) {
				result = null;
				break;
			}
		} while("E901".equals(rspcod));
		
		return result;
	}

	@Override
	public Object writeLog(final TelCommExecWrapper execwrapper, final Map _params, final MVHImpl result) {	

		Map<String, String> params = _params;

		log.info("N171.writeLog...");

		TXNTWRECORD record = execwrapper.createTxnTwRecord(sysDailySeqDao);
		
		String date = record.getLASTDATE();
		String time = record.getLASTTIME();
		
//		if(result != null) {
//			try {
//				date = DateTimeUtils.format("yyyyMMdd", DateTimeUtils.getCDate2Date(result.getValueByFieldName("DATE")));
//			} catch (Exception e) {
//				log.error("", e);
//			}					
//			try {
//				time = result.getValueByFieldName("TIME");
//			} catch (Exception e){
//				log.error("", e);
//			}			
//		} else {
//			log.warn("result is null...");
//		}
//		log.info("record set other params...");
		
//		record.setDPSCHNO(params.get("__SCHNO")==null?"" : params.get("__SCHNO"));
		
		record.setDPUSERID(params.get("UID"));
		record.setADOPID(params.get("ADOPID"));
		record.setDPTXDATE(date);
		record.setDPTXTIME(time);
		record.setDPWDAC(params.get("OUTACN"));
		record.setDPSVBH(params.get("BANKIND"));
		record.setDPSVAC(params.get("__TRIN_ACN"));
		record.setDPTXAMT(params.get("AMOUNT"));

		record.setDPTXMEMO(params.get("CMTRMEMO"));
		record.setDPTXMAILS(params.get("CMTRMAIL"));
		record.setDPTXMAILMEMO(params.get("CMMAILMEMO"));  // CMMAILMEMO 對應的欄位

		if(result == null) {
			record.setDPEFEE("0");

			record.setDPTXNO("");  // 跨行序號
		}
		else {
			record.setDPEFEE(result.getValueByFieldName("FEE"));
			record.setDPTXNO(result.getValueByFieldName("STAN"));
		}
		
		record.setDPTXCODE(params.get("FGTXWAY"));
//		record.setPCSEQ(params.get("PCSEQ"));  //交易序號
		
		record.setLOGINTYPE(params.get("LOGINTYPE")); // ex:NB
		
		// IKEY生成XMLCA、XMLCN後即不需要pkcs7Sign、jsondc，欄位長度過長資料庫塞不進去，故remove
		params.remove("pkcs7Sign"); 
		params.remove("jsondc");
		params.remove("__SIGN");
		record.setDPTITAINFO(CodeUtil.toJson(params));
		
/**
 * 資料庫調整，會拿掉這張TABLE，先註解，By Kevin
 * 
		TXNREQINFO titainfo = null;
		if(record.getDPTITAINFO() == 0) {
			titainfo = new TXNREQINFO();
			titainfo.setREQINFO(BookingUtils.requestInfo(params));
		}
		
		txnTwRecordDao.writeTxnRecord(titainfo, record);
 */
		 
		txnTwRecordDao.save(record);
		
		return record;
	}
	
}
