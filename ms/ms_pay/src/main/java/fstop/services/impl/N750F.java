package fstop.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.rest.comm.bean.DoIDGateCommand_In;
import com.netbank.rest.comm.controller.CommonIDGATEController;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import encryptUtil.EncryptUtil;
import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.SysDailySeqDao;
import fstop.orm.dao.TxnReqInfoDao;
import fstop.orm.dao.TxnTwRecordDao;
import fstop.orm.dao.TxnTwSchPayDao;
import fstop.orm.dao.TxnTwSchPayDataDao;
import fstop.orm.dao.TxnTwScheduleDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.ADMMAILLOG;
import fstop.orm.po.TXNTWRECORD;
import fstop.orm.po.TXNTWSCHPAY;
import fstop.orm.po.TXNTWSCHPAYDATA;
import fstop.services.BookingAware;
import fstop.services.CommonService;
import fstop.services.NoticeInfo;
import fstop.services.NoticeInfoGetter;
import fstop.services.OnLineTransferNotice;
import fstop.services.TelCommExecWrapper;
import fstop.services.TransferNotice;
import fstop.services.WriteLogInterface;
import fstop.telcomm.TelCommExec;
import fstop.telcomm.cmd.CommandUtils;
import fstop.util.DateTimeUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import topibs.utility.NumericUtil;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class N750F extends CommonService implements BookingAware, WriteLogInterface {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("n750fTelcomm")
	private TelCommExec n750fTelcomm;

	@Autowired
	private TxnTwRecordDao txnTwRecordDao;
	
	@Autowired
	private TxnTwSchPayDao txntwschpaydao;
	
	@Autowired
	private TxnTwSchPayDataDao txntwschpaydatadao;

	@Autowired
	private TxnTwScheduleDao txnTwScheduleDao;

	@Autowired
	private TxnReqInfoDao txnReqInfoDao;

	@Autowired
	private TxnUserDao txnUserDao;
	
	@Autowired
	private SysDailySeqDao sysDailySeqDao;
	
	@Autowired
	CommonIDGATEController commonIDGATEController;
	
//	@Required
//	public void setTxnUserDao(TxnUserDao txnUserDao) {
//		this.txnUserDao = txnUserDao;
//	}
//	
//	@Required
//	public void setTxnReqInfoDao(TxnReqInfoDao txnReqInfoDao) {
//		this.txnReqInfoDao = txnReqInfoDao;
//	}
//
//	
//	@Required
//	public void setN750fTelcomm(TelCommExec telcomm) {
//		n750fTelcomm = telcomm;
//	}
//	
//	@Required
//	public void setTxnTwRecordDao(TxnTwRecordDao txnTwRecordDao) {
//		this.txnTwRecordDao = txnTwRecordDao;
//	}
//	
//	@Required
//	public void setTxnTwScheduleDao(TxnTwScheduleDao dao) {
//		this.txnTwScheduleDao = dao;
//	}
	

	@Override
	public MVH doAction(Map params) {
		
		//晶片金融卡
		if("2".equals(params.get("FGTXWAY").toString()))
		{
			Date d = new Date();
			String trin_transeq = "1234";
			String trin_issuer = "05000000";
			String trin_acnno = params.get("ACNNO").toString();
			String trin_icdttm = DateTimeUtils.format("yyyyMMddHHmmss", d);
			log.debug("ICDTTM ----------> " + trin_icdttm);
			String trin_iseqno = params.get("iSeqNo").toString();
			String trin_icmemo = "";
			String trin_tac_length = "000A";		
			String trin_tac = params.get("TAC").toString();
			log.debug(ESAPIUtil.vaildLog("TAC ----------> " + trin_tac_length + trin_tac + "(" + (trin_tac_length.length() + trin_tac.length()) + ")"));
			String trin_trmid = params.get("TRMID").toString();
		
			params.put("TRANSEQ", trin_transeq);
			params.put("ISSUER", trin_issuer);
			params.put("ACNNO", trin_acnno);
			params.put("ICDTTM", trin_icdttm);
			params.put("ICSEQ", trin_iseqno);
			params.put("ICMEMO", trin_icmemo);
			params.put("TAC_Length", trin_tac_length);
			params.put("TAC", trin_tac);
			params.put("TRMID", trin_trmid);
		}

		String fgtxdate = (String)params.get("FGTXDATE");
		
		if("1".equals(fgtxdate)) {  //即時
			OnLineTransferNotice trNotice = new OnLineTransferNotice();
			
			return online(params, trNotice);
		}
		else if("2".equals(fgtxdate)) {  //預約

			if("0".equals(params.get("FGTXWAY"))) {  //使用交易密碼, 做預約
//				String TransPassUpdate = (String)params.get("TRANSPASSUPDATE");
//				String Phase2 = (String)params.get("PHASE2");
				TelCommExec n950CMTelcomm;
//				
//				if(StrUtils.isNotEmpty(TransPassUpdate))
//				{
//					if(TransPassUpdate.equals("1") || Phase2.equals("ON"))
//					{
						n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n951CMTelcomm");
//					}
//					else
//					{
//						n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n950CMTelcomm");
//					}
//				}
//				else
//				{
//					n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n950CMTelcomm");
//				}
				MVHImpl n950Result = n950CMTelcomm.query(params);
				//若驗證失敗, 則會發動 TopMessageException
			}
			else if("2".equals(params.get("FGTXWAY"))) {   //使用晶片金融卡, 做預約
				
				TelCommExec n954Telcomm = (TelCommExec)SpringBeanFactory.getBean("n954Telcomm");
				MVHImpl n954Result = n954Telcomm.query(params);
				//若驗證失敗, 則會發動 TopMessageException
			}
						
			return booking(params);
		}
		
		return new MVHImpl();
	}
	
	
	public Map<String, String> scheduleObjectMapping() {
		Map<String, String> map = new HashMap();
		
		map.put("DPUSERID", "UID");
		map.put("DPWDAC", "OUTACN"); // 轉出帳號
		//map.put("DPSVBH", ""); // 轉入分行
		map.put("DPSVAC", "BARCODE2"); // 轉入帳號, 使用原始輸入的來記錄
		map.put("DPTXAMT", "AMOUNT");
		map.put("DPTXMEMO", "CMTRMEMO");
		map.put("DPTXMAILS", "CMTRMAIL");
		map.put("DPTXMAILMEMO", "CMMAILMEMO"); // CMMAILMEMO 對應的欄位

		map.put("ADOPID", "ADOPID");

		map.put("DPTXCODE","FGTXWAY"); //SSL: 1 or IKEY: 2
		
		/*
		txntwschedule.setDPUSERID(params.get("UID"));
		txntwschedule.setDPWDAC(params.get("OUTACN"));  //轉出帳號
		txntwschedule.setDPSVBH("");  //轉入分行
		txntwschedule.setDPSVAC(params.get("WAT_NO"));  //轉入帳號
		txntwschedule.setDPTXAMT(params.get("AMOUNT"));
		txntwschedule.setDPTXMEMO(params.get("CMTRMEMO"));
		txntwschedule.setDPTXMAILS(params.get("CMTRMAIL"));
		txntwschedule.setDPTXMAILMEMO(params.get("CMMAILMEMO"));  //CMMAILMEMO 對應的欄位

		txntwschedule.setADOPID(params.get("TXID"));
		*/
		return map;
	}
	
	
	@Override
	public MVHImpl booking(Map<String, String> params) {
		log.trace(ESAPIUtil.vaildLog("N750F.booking.params: {}"+ params));
		TXNTWSCHPAY txntwschpay = new TXNTWSCHPAY();
		
		// CERTACN
		String TxWay = params.get("FGTXWAY");
		String certACN = "";
		
		if("0".equals(TxWay)){
			while(certACN.length() < 19)certACN += "2";
		}
		else if("1".equals(TxWay)){
			while(certACN.length() < 19)certACN += "3";
		}
		else if("2".equals(TxWay)){
			while(certACN.length() < 19)certACN += "5";
		}
		else if("3".equals(TxWay)){
			while(certACN.length() < 19)certACN += "7";
		}
		else if("7".equals(TxWay)){
			while(certACN.length() < 19)certACN += "D";
			
			// IDGATE驗證-----start
			DoIDGateCommand_In in = new DoIDGateCommand_In();
			in.setIN_DATAS(params);
			HashMap rtnMap = commonIDGATEController.doIDGateCommand(in);
			if (!(boolean) rtnMap.get("result")) {
				log.error("N174 BOOKING CHECK IDGateCommand() ERROR");
				switch ((String) rtnMap.get("errMsg")) {
				case "Z300":
					throw TopMessageException.create("Z300");
				case "FE0011":
					throw TopMessageException.create("FE0011");
				default:
					throw TopMessageException.create("ZX99");
				}
			}
			// IDGATE驗證-----end
		}
		params.put("CERTACN", certACN);
		
		
		// 操作功能ID
		txntwschpay.setADOPID(params.get("ADOPID"));
		// 使用者ID
		txntwschpay.setDPUSERID(params.get("CUSIDN"));
		// 轉出帳號
		txntwschpay.setDPWDAC(params.get("OUTACN"));
		// 轉入行庫代碼
		txntwschpay.setDPSVBH("050");
		// 轉入帳號/繳費代號
		txntwschpay.setDPSVAC(params.get("BARCODE2"));
		// 轉帳金額
		txntwschpay.setDPTXAMT(params.get("AMOUNT"));
		// 備註
		txntwschpay.setDPTXMEMO(params.get("CMTRMEMO"));
		// 發送Mail清單
		txntwschpay.setDPTXMAILS(params.get("CMTRMAIL"));
		// Mail備註
		txntwschpay.setDPTXMAILMEMO(params.get("CMMAILMEMO"));
		// 交易機制
		txntwschpay.setDPTXCODE(TxWay);
		// 取得預約編號(YYMMDD+三位流水號)
		String dptmpschno = txnUserDao.getDptmpschno(params.get("CUSIDN"));
		txntwschpay.setDPSCHNO(dptmpschno);
		// 預約狀態
		txntwschpay.setDPTXSTATUS("0");
		
		// 系統別
		txntwschpay.setLOGINTYPE(params.get("LOGINTYPE"));

		// 寫入日期
		Date d = new Date();
		txntwschpay.setDPSDATE(DateTimeUtils.format("yyyyMMdd", d));
		txntwschpay.setDPSTIME(DateTimeUtils.format("HHmmss", d));

		txntwschpay.setLASTDATE(txntwschpay.getDPSDATE());
		txntwschpay.setLASTTIME(txntwschpay.getDPSTIME());

		String f = params.get("FGTXDATE");
		if ("2".equals(f)) { // 預約某一日
			params.put("CMDATE", StrUtils.trim(params.get("CMDATE")).replaceAll("/", ""));
			if (params.get("CMDATE").length() == 0) {
				throw TopMessageException.create("ZX99");
			} else {
				txntwschpay.setDPTXTYPE("S");
				txntwschpay.setDPPERMTDATE("");
				txntwschpay.setDPFDATE(params.get("CMDATE"));
				txntwschpay.setDPTDATE(params.get("CMDATE"));
			}
		} else if ("3".equals(f)) {
			params.put("CMSDATE", StrUtils.trim(params.get("CMSDATE")).replaceAll("/", ""));
			params.put("CMEDATE", StrUtils.trim(params.get("CMEDATE")).replaceAll("/", ""));
			if (params.get("CMDD").length() == 0 || params.get("CMSDATE").length() == 0
					|| params.get("CMEDATE").length() == 0) {
				throw TopMessageException.create("ZX99");
			} else {
				txntwschpay.setDPTXTYPE("C");
				txntwschpay.setDPPERMTDATE(params.get("CMDD"));
				txntwschpay.setDPFDATE(params.get("CMSDATE"));
				txntwschpay.setDPTDATE(params.get("CMEDATE"));
			}
		}
		
		if (params.containsKey("pkcs7Sign") && "1".equals(params.get("FGTXWAY"))) {
			List<String> commands = new ArrayList();
			commands.add("XMLCA()");
			commands.add("XMLCN()");

			CommandUtils.doBefore(commands, params);

			if (!StrUtils.trim(params.get("XMLCN")).toUpperCase()
					.startsWith(StrUtils.trim(params.get("UID")).toUpperCase())) {
				throw TopMessageException.create("Z089");
			}

			txntwschpay.setXMLCA(StrUtils.trim(params.get("XMLCA")));
			txntwschpay.setXMLCN(StrUtils.trim(params.get("XMLCN")));
		}
		
		// TODO DPTXINFO放不下這兩個欄位，先拿掉，若預約交易執行時需要再看怎麼存
		params.remove("pkcs7Sign");
		params.remove("jsondc");
		// 預約時上行電文必要資訊
		log.trace(ESAPIUtil.vaildLog("txntwschpay.params>>>{}"+ params));
		String reqinfo_Str = "{}";
		String sha1_Mac = "";
		reqinfo_Str =  CodeUtil.toJson(params);
		sha1_Mac = EncryptUtil.toEncS1(reqinfo_Str, "UTF-8").substring(0,4);
		
		txntwschpay.setMAC(sha1_Mac);
		txntwschpay.setDPTXINFO(reqinfo_Str);
		txntwschpay.setMSADDR(TxnTwSchPayDao.MS_PAY);
		
		log.trace(ESAPIUtil.vaildLog("txntwschpay: {}"+ txntwschpay));
		txntwschpaydao.save(txntwschpay);
		
		MVHImpl result =  new MVHImpl();
		
		try {
			result.getFlatValues().put("CMTXTIME", DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", d));
			result.getFlatValues().put("TOPMSG", "0000");
			result.getFlatValues().put("MSGCOD", "0000");
		}
		catch(Exception e){
			log.error("",e);
		}
		
		return result;
	}
	
	

//	@Override
//	public MVHImpl booking_old(Map<String, String> params) {
//		TXNTWSCHEDULE txntwschedule = new TXNTWSCHEDULE();
//		
//		//產生 CERTACN update by Blair -------------------------------------------- Start
//		String TxWay = params.get("FGTXWAY");
//		String certACN = "";
//		
//		if("0".equals(TxWay)){
//			while(certACN.length() < 19)certACN += "2";
//		}
//		else if("1".equals(TxWay)){
//			while(certACN.length() < 19)certACN += "3";
//		}
//		else {
//			while(certACN.length() < 19)certACN += "5";
//		}
//		params.put("CERTACN", certACN);
//		//產生 CERTACN update by Blair -------------------------------------------- End
//		
//		
//		log.info("***************************************預約編號***************************************************");
//		//更新SCHCOUNT(每日預約計數)
//		int scnt = txnUserDao.incSchcount((String)params.get("UID"));
//		
//		//預約編號(YYMMDD+三位流水號)
//		String dptmpschno = "";
//		String ymd = DateTimeUtils.format("yyMMdd", new Date());
//		dptmpschno = ymd + StrUtils.right("000" + scnt, 3); 
//		
//		//賽入預約編號
//		txntwschedule.setDPSCHNO(dptmpschno);
//		log.info("***************************************預約編號***************************************************");
//		
//		String f = params.get("FGTXDATE");
//		 //預約某一日
//		params.put("CMDATE", StrUtils.trim(params.get("CMDATE")).replaceAll("/", ""));
//		
//		txntwschedule.setDPPERMTDATE("");
//		txntwschedule.setDPFDATE(params.get("CMDATE"));
//		txntwschedule.setDPTDATE(params.get("CMDATE"));
//
//		params.put("ADREQTYPE", "S");   //寫入 TXNLOG 時, S 表示為預約
//
//		Map<String, String> mapping = scheduleObjectMapping();
//		for(String key : mapping.keySet()) {
//			String v = mapping.get(key);
//			try {
//				BeanUtils.setProperty(txntwschedule, key, params.get(v));
//			} catch (IllegalAccessException e) {
//				throw new ToRuntimeException("", e);
//			} catch (InvocationTargetException e) {
//				throw new ToRuntimeException("", e);
//			}
//		}
//
//		if(params.containsKey("pkcs7Sign") && "1".equals(params.get("FGTXWAY"))) {
//			List<String> commands = new ArrayList();
//			commands.add("XMLCA()");
//			commands.add("XMLCN()");
//			
//			CommandUtils.doBefore(commands, params);
//			
//			if(!StrUtils.trim(params.get("XMLCN")).toUpperCase().startsWith(StrUtils.trim(params.get("UID")).toUpperCase())) {
//				throw TopMessageException.create("Z089");
//			}
//
//			txntwschedule.setXMLCA(StrUtils.trim(params.get("XMLCA")));
//			txntwschedule.setXMLCN(StrUtils.trim(params.get("XMLCN")));
//		}
//		
//
//		TXNREQINFO titainfo = new TXNREQINFO();
//		titainfo.setREQINFO(BookingUtils.requestInfo(params));
//		txnReqInfoDao.save(titainfo);
//
//		txntwschedule.setDPTXINFO(titainfo.getREQID());
//		
//		String sha1 = DigestUtils.sha1(titainfo.getREQINFO()).substring(0, 4);
//		txntwschedule.setMAC(sha1);
//		
//		
//		Date d = new Date();
//		txntwschedule.setDPSDATE(DateTimeUtils.format("yyyyMMdd", d));
//		txntwschedule.setDPSTIME(DateTimeUtils.format("HHmmss", d));
// 
//		
//		txntwschedule.setDPTXSTATUS("0");
//		txntwschedule.setLASTDATE(txntwschedule.getDPSDATE());
//		txntwschedule.setLASTTIME(txntwschedule.getDPSTIME());
//		
//		txnTwScheduleDao.save(txntwschedule);
//
//		
//		MVHImpl result =  new MVHImpl();
//		
//		try {
//			result.getFlatValues().put("CMTXTIME", DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", d));
//		}
//		catch(Exception e){
//			log.error(e.toString());			
//		}
//		
//		return result;
//	}
	
		
	
	
	
	/**
	 * 國民年金保險費
	 * @param params
	 * @return
	 */
	@Override
	public MVHImpl online(final Map<String, String> params, TransferNotice trNotice) { 
		Date d = new Date();
		params.put("DATE", DateTimeUtils.getCDateShort(d));
		params.put("TIME", DateTimeUtils.getTimeShort(d));

		String f = params.get("FGTXDATE");		
		if("1".equals(f)) {  //		即時
		
			//產生 CERTACN update by Blair -------------------------------------------- Start
			String TxWay = params.get("FGTXWAY");
			String certACN = "";
			
			if("0".equals(TxWay)){
				while(certACN.length() < 19)certACN += " ";
			}
			else if("1".equals(TxWay)){
				while(certACN.length() < 19)certACN += "1";
			}
			else if("7".equals(TxWay)){
				while(certACN.length() < 19)certACN += "C";
			}
			else {
				while(certACN.length() < 19)certACN += "4";
			}
			
			params.put("CERTACN", certACN);
			//產生 CERTACN update by Blair -------------------------------------------- End
		}

		final TelCommExecWrapper execwrapper = new TelCommExecWrapper(n750fTelcomm);

		MVHImpl result = execwrapper.query(params);
		
		final TXNTWRECORD record = (TXNTWRECORD)writeLog(execwrapper, params, result);
		
		final TelCommExecWrapper execwrapperFinal = execwrapper;
		trNotice.sendNotice(new NoticeInfoGetter() {

			public NoticeInfo getNoticeInfo() { 
				NoticeInfo result = new NoticeInfo(params.get("UID"), params.get("DPMYEMAIL"), params.get("CMTRMAIL"));
				
//				result.setDpretxstatus(record.getDPRETXSTATUS());
				result.setADTXNO(record.getADTXNO());
				
				result.setTemplateName("NATIONAL");
				result.setException(execwrapperFinal.getLastException());
				String datetime = record.getDPTXDATE() + record.getDPTXTIME();
				Date d = DateTimeUtils.parse("yyyyMMddHHmmss", datetime);
				String trantime = DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", d);
				result.getParams().put("#TRANTIME", trantime);
				result.getParams().put("#SEQNO", record.getDPTXNO());
				
				String acn = record.getDPSVAC();
				try {
					acn = StrUtils.left(acn, acn.length() - 5) + "***" + StrUtils.right(acn, 2);
				}
				catch(Exception e){}
				
				String outacn = record.getDPWDAC();
				try {
					outacn = StrUtils.left(outacn, outacn.length() - 5) + "***" + StrUtils.right(outacn, 2);
				}
				catch(Exception e){}
				
				result.getParams().put("#ACN", acn);
				result.getParams().put("#OUTACN", outacn);
				result.getParams().put("#OUTCURRENCY", "新台幣");
				String dptxamtfmt = NumericUtil.formatNumberString(record.getDPTXAMT(), 0);
				result.getParams().put("#AMT", dptxamtfmt);
				result.getParams().put("#MEMO", params.get("CMMAILMEMO"));
				ADMMAILLOG admmaillog = new ADMMAILLOG();
				admmaillog.setADACNO(record.getDPWDAC());
				
				String trans_status = (String)params.get("__TRANS_STATUS");
				if(StrUtils.isEmpty(trans_status)) {
					trans_status = "TWONLINE";
				}
				else if("SEND".equals(trans_status)) {
					trans_status = "TWSCH";
				}
				else if("RESEND".equals(trans_status)) {
					trans_status = "TWSCHRE";
				}
				
				admmaillog.setADBATCHNO(trans_status);
				admmaillog.setADMAILACNO(StrUtils.trim(params.get("CMTRMAIL")));
				admmaillog.setADUSERID(record.getDPUSERID());
				admmaillog.setADUSERNAME("");
				admmaillog.setADSENDTYPE("3");
				admmaillog.setADSENDTIME(datetime);
				admmaillog.setADSENDSTATUS("");
				
				result.setADMMAILLOG(admmaillog);
				
				
				return result;
			}
			
		});

		
		execwrapper.throwException();
		
		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		
		try {
			result.getFlatValues().put("CMTXTIME", 
					DateTimeUtils.getCDateTime(
							result.getValueByFieldName("TRNDATE"),
							result.getValueByFieldName("TRNTIME")));
		}
		catch(Exception e){
			log.error(e.toString());			
		}
		
		return result;
	}

	@Override
	public Object writeLog(TelCommExecWrapper execwrapper, Map<String, String> params, final MVHImpl result) {
		
		TXNTWRECORD record = execwrapper.createTxnTwRecord(sysDailySeqDao);
		
		String date = record.getLASTDATE();
		String time = record.getLASTTIME();
		
//		if(result != null) {
//			try {
//				date = DateTimeUtils.format("yyyyMMdd", DateTimeUtils.getCDate2Date(result.getValueByFieldName("TRNDATE")));
//			}
//			catch(Exception e){}
//			try {
//				time = result.getValueByFieldName("TRNTIME");
//			}
//			catch(Exception e){}
//		}

		record.setDPUSERID(params.get("UID"));
		record.setADOPID(params.get("ADOPID"));
		record.setDPTXDATE(date);
		record.setDPTXTIME(time);
		record.setDPWDAC(params.get("OUTACN"));
		record.setDPSVBH("");
		record.setDPSVAC(params.get("BARCODE2"));
		record.setDPTXAMT(params.get("AMOUNT"));

		record.setDPTXMEMO(params.get("CMTRMEMO"));
		record.setDPTXMAILS(params.get("CMTRMAIL"));
		record.setDPTXMAILMEMO(params.get("CMMAILMEMO"));  //CMMAILMEMO 對應的欄位
		
		record.setDPEFEE("0");
		record.setDPTXNO("");  //跨行序號
		record.setDPTXCODE(params.get("FGTXWAY"));
		
		record.setLOGINTYPE(params.get("LOGINTYPE")); // ex:NB
		
		// IKEY生成XMLCA、XMLCN後即不需要pkcs7Sign、jsondc，欄位長度過長資料庫塞不進去，故remove
		params.remove("pkcs7Sign"); 
		params.remove("jsondc");
		params.remove("__SIGN");
		record.setDPTITAINFO(CodeUtil.toJson(params));
		
		txnTwRecordDao.save(record);
		
//		record.setDPSCHNO(params.get("__SCHNO")==null?"" : params.get("__SCHNO"));
		
//		TXNREQINFO titainfo = null;
//		if(record.getDPTITAINFO() == 0) {
//			titainfo = new TXNREQINFO();
//			titainfo.setREQINFO(BookingUtils.requestInfo(params));
//		}


//		txnTwRecordDao.writeTxnRecord(titainfo, record);
		
		return record;
	}	
}
