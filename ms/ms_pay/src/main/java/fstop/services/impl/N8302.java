package fstop.services.impl;

import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N8302 extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("n8302Telcomm")
	private TelCommExec n8302Telcomm;
	 
//	@Required
//	public void setN8302Telcomm(TelCommExec telcomm) {
//		n8302Telcomm = telcomm;
//	}

	@Autowired
    @Qualifier("n8302_1Telcomm")
	private TelCommExec n8302_1Telcomm;
	 
//	@Required
//	public void setN8302_1Telcomm(TelCommExec telcomm) {
//		n8302_1Telcomm = telcomm;
//	}
	
	
	@Override	
	public MVH doAction(Map params) {    
		Map<String, String> p = params;

		p.put("DATE", DateTimeUtils.getCDateShort(new Date()));
		p.put("TIME", DateTimeUtils.format("HHmmss", new Date()));
		//FGTXWAY 為交易機制
		if("0".equals(params.get("FGTXWAY"))) {   // 交易密碼
			params.put("CERTACN", StrUtils.repeat(" ", 19));	//即時
		}
		else if("1".equals(params.get("FGTXWAY"))) {  //憑證
			params.put("CERTACN", StrUtils.repeat("1", 19));	//即時
		}
		else if ("2".equals(params.get("FGTXWAY"))) { //晶片金融卡
			params.put("CERTACN", StrUtils.repeat("4", 19));	//即時
		}
		else if("7".equals(params.get("FGTXWAY"))) {  //IDGATE
			params.put("CERTACN", StrUtils.repeat("C", 19));	//即時
		}
		TelcommResult helper = "14".equals(params.get("TYPE").toString()) ? n8302_1Telcomm.query(params) : n8302Telcomm.query(params);
		helper.getFlatValues().put("CMQTIME",
				DateTimeUtils.getDatetime(new Date()));
		
		//TODO 若回傳的碼是有 Error Code, D:\SohoProject\fstop\tbb\document\sa\Pierre\code.txt 的 code.txt, code.dat
		helper.getValueByFieldName("TSFACN");
		
		
		return helper;
	}



}
