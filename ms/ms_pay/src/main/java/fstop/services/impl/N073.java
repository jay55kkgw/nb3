package fstop.services.impl;

import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import topibs.utility.NumericUtil;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.SysDailySeqDao;
import fstop.orm.dao.TxnReqInfoDao;
import fstop.orm.dao.TxnTwRecordDao;
import fstop.orm.po.ADMMAILLOG;
import fstop.orm.po.TXNREQINFO;
import fstop.orm.po.TXNTWRECORD;
import fstop.services.BookingUtils;
import fstop.services.CommonService;
import fstop.services.NoticeInfo;
import fstop.services.NoticeInfoGetter;
import fstop.services.OnLineTransferNotice;
import fstop.services.TelCommExecWrapper;
import fstop.services.WriteLogInterface;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 繳納期貨保證金
 * @author Owner
 *
 */
@Slf4j
public class N073 extends CommonService implements WriteLogInterface  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("n073Telcomm")
	private TelCommExec n073Telcomm;

	@Autowired
	private TxnTwRecordDao txnTwRecordDao;

	@Autowired
	private TxnReqInfoDao txnReqInfoDao;
	
	@Autowired
	private SysDailySeqDao sysDailySeqDao;
	
//	@Required
//	public void setTxnReqInfoDao(TxnReqInfoDao txnReqInfoDao) {
//		this.txnReqInfoDao = txnReqInfoDao;
//	}
//
//
//	@Required
//	public void setN073Telcomm(TelCommExec telcomm) {
//		n073Telcomm = telcomm;
//	}
//
//	@Required
//	public void setTxnTwRecordDao(TxnTwRecordDao txnTwRecordDao) {
//		this.txnTwRecordDao = txnTwRecordDao;
//	}

	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params) {
		final Map<String, String> params = _params;
		
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);
		
		String fgsvacno = params.get("FGSVACNO"); // 1 約定帳號, 2 非約定帳號
		
		//轉入帳號
		String trin_bnkcod = "";
		String trin_acn = "";
		String trin_agree = "";
		if("1".equals(fgsvacno)) {
			Map<String, String> jm = JSONUtils.json2map(params.get("DPAGACNO"));
			trin_bnkcod = jm.get("BNKCOD");
			trin_acn = jm.get("ACN");
			trin_agree = jm.get("AGREE");
		}
		else if("2".equals(fgsvacno)) {  //非約定
			trin_bnkcod = "050";
			trin_acn = params.get("DPACNO");
			trin_agree = "0";   // 0: 非約定, 1: 約定
		}
		
		params.put("__TRIN_BANKCOD", trin_bnkcod);
		params.put("__TRIN_ORG_ACN", trin_acn);
		params.put("__TRIN_ACN", trin_acn );
		params.put("__TRIN_AGREE", trin_agree);
		
		//晶片金融卡
		if("2".equals(params.get("FGTXWAY").toString()))
		{
			String trin_transeq = "1234";
			String trin_issuer = "05000000";
			String trin_acnno = params.get("ACNNO");
			String trin_icdttm = DateTimeUtils.format("yyyyMMddHHmmss", d);
			log.debug("ICDTTM ----------> " + trin_icdttm);
			String trin_iseqno = params.get("iSeqNo");
			String trin_icmemo = "";
			String trin_tac_length = "000A";		
			String trin_tac = params.get("TAC");
			log.debug(ESAPIUtil.vaildLog("TAC ----------> " + trin_tac_length + trin_tac + "(" + (trin_tac_length.length() + trin_tac.length()) + ")"));
			String trin_trmid = params.get("TRMID");
		
			params.put("TRANSEQ", trin_transeq);
			params.put("ISSUER", trin_issuer);
			params.put("ACNNO", trin_acnno);
			params.put("ICDTTM", trin_icdttm);
			params.put("ICSEQ", trin_iseqno);
			params.put("ICMEMO", trin_icmemo);
			params.put("TAC_Length", trin_tac_length);
			params.put("TAC", trin_tac);
			params.put("TRMID", trin_trmid);
		}
		
		//產生 CERTACN update by Blair -------------------------------------------- Start
		String TxWay = params.get("FGTXWAY");
		String certACN = "";
		
		if("0".equals(TxWay)){
			while(certACN.length() < 19)certACN += " ";
		}
		else if("1".equals(TxWay)){
			while(certACN.length() < 19)certACN += "1";
		}
		else if("7".equals(TxWay)){
			while(certACN.length() < 19)certACN += "C";
		}		
		else if("4".equals(TxWay)){
			while(certACN.length() < 19)certACN += "8";
		}		
		else {
			while(certACN.length() < 19)certACN += "4";
		}
		params.put("CERTACN", certACN);
		//產生 CERTACN update by Blair -------------------------------------------- End
		
		params.put("INTSACN", params.get("__TRIN_ACN"));
		params.put("FLAG", trin_agree);
		
		final TelCommExecWrapper execwrapper = new TelCommExecWrapper(n073Telcomm);
		
		MVHImpl result = execwrapper.query(params);
		
		final TXNTWRECORD record = (TXNTWRECORD)writeLog(execwrapper, params, result);
		
		execwrapper.throwException();
		
		final TelCommExecWrapper execwrapperFinal = execwrapper;
		OnLineTransferNotice trNotice = new OnLineTransferNotice();
		trNotice.sendNotice(new NoticeInfoGetter() {

			public NoticeInfo getNoticeInfo() { 
				NoticeInfo result = new NoticeInfo(params.get("UID"), params.get("DPMYEMAIL"), params.get("CMTRMAIL"));
				
				result.setADTXNO(record.getADTXNO());
				
				result.setTemplateName("transferNotify");
				result.setException(execwrapperFinal.getLastException());
				String datetime = record.getDPTXDATE() + record.getDPTXTIME();
				Date d = DateTimeUtils.parse("yyyyMMddHHmmss", datetime);
				String trantime = DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", d);
				result.getParams().put("#TRANTIME", trantime);
				result.getParams().put("#SEQNO", record.getDPTXNO());
				
				String acn = record.getDPSVAC();
				try {
					acn = StrUtils.left(acn, acn.length() - 5) + "***" + StrUtils.right(acn, 2);
				}
				catch(Exception e){}
				
				String outacn = record.getDPWDAC();
				try {
					outacn = StrUtils.left(outacn, outacn.length() - 5) + "***" + StrUtils.right(outacn, 2);
				}
				catch(Exception e){}
				
				result.getParams().put("#ACN", acn);
				result.getParams().put("#OUTACN", outacn);
				result.getParams().put("#OUTCURRENCY", "新台幣");
				String dptxamtfmt = NumericUtil.formatNumberString(record.getDPTXAMT(), 0);
				result.getParams().put("#AMT", dptxamtfmt);
				result.getParams().put("#MEMO", params.get("CMMAILMEMO"));
				
				ADMMAILLOG admmaillog = new ADMMAILLOG();
				admmaillog.setADACNO(record.getDPWDAC());
				
				String trans_status = (String)params.get("__TRANS_STATUS");
				if(StrUtils.isEmpty(trans_status)) {
					trans_status = "TWONLINE";
				}
				else if("SEND".equals(trans_status)) {
					trans_status = "TWSCH";
				}
				else if("RESEND".equals(trans_status)) {
					trans_status = "TWSCHRE";
				}
				
				admmaillog.setADBATCHNO(trans_status);
				admmaillog.setADMAILACNO(StrUtils.trim(params.get("CMTRMAIL")));
				admmaillog.setADUSERID(record.getDPUSERID());
				admmaillog.setADUSERNAME("");
				admmaillog.setADSENDTYPE("3");
				admmaillog.setADSENDTIME(datetime);
				admmaillog.setADSENDSTATUS("");
				
				result.setADMMAILLOG(admmaillog);
				
				
				return result;
			}
			
		});
		
		
		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		
		try {
			result.getFlatValues().put("CMTXTIME", 
					DateTimeUtils.getCDateTime(result.getValueByFieldName("DATE"),
											result.getValueByFieldName("TIME")));
		}
		catch(Exception e){
			log.error(e.toString());
		}
		
		return result;
	}

	@Override
	public Object writeLog(TelCommExecWrapper execwrapper, Map<String, String> params, final MVHImpl result) {

		TXNTWRECORD record = execwrapper.createTxnTwRecord(sysDailySeqDao);

		String date = record.getLASTDATE();
		String time = record.getLASTTIME();
		
//		if(result != null) {
//			try {
//				date = DateTimeUtils.format("yyyyMMdd", DateTimeUtils.getCDate2Date(result.getValueByFieldName("DATE")));
//			} catch (Exception e){
//				log.error("", e);
//			}
//			try {
//				time = result.getValueByFieldName("TIME");
//			} catch (Exception e){
//				log.error("", e);
//			}		
//		} else {
//			log.warn("result is null...");
//		}
//		log.info("record set other params...");
		
//		record.setDPSCHNO(params.get("__SCHNO")==null?"" : params.get("__SCHNO"));
		
		record.setDPUSERID(params.get("UID"));
		record.setADOPID(params.get("ADOPID"));
		record.setDPTXDATE(date);
		record.setDPTXTIME(time);
		record.setDPWDAC(params.get("ACN"));
		record.setDPSVBH("050");
		record.setDPSVAC(params.get("INTSACN"));
		record.setDPTXAMT(params.get("AMOUNT"));

		record.setDPTXMEMO(params.get("CMTRMEMO"));
		record.setDPTXMAILS(params.get("CMTRMAIL"));
		record.setDPTXMAILMEMO(params.get("CMMAILMEMO"));  //CMMAILMEMO 對應的欄位
		
		record.setDPEFEE("0");
		record.setDPTXNO("");  //跨行序號
		record.setDPTXCODE(params.get("FGTXWAY"));

		
		record.setLOGINTYPE(params.get("LOGINTYPE")); // ex:NB
		
		// IKEY生成XMLCA、XMLCN後即不需要pkcs7Sign、jsondc，欄位長度過長資料庫塞不進去，故remove
		params.remove("pkcs7Sign"); 
		params.remove("jsondc");
		params.remove("__SIGN");
		record.setDPTITAINFO(CodeUtil.toJson(params));

		
		txnTwRecordDao.save(record);
		
		return record;
	}
	
}
