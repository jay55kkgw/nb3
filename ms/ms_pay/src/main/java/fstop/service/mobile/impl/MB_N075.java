package fstop.service.mobile.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import encryptUtil.EncryptUtil;
import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.SysDailySeqDao;
import fstop.orm.dao.TxnTwRecordDao;
import fstop.orm.dao.TxnTwSchPayDao;
import fstop.orm.dao.TxnTwSchPayDataDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.ADMMAILLOG;
import fstop.orm.po.TXNTWRECORD;
import fstop.orm.po.TXNTWSCHPAY;
import fstop.orm.po.TXNTWSCHPAYDATA;
import fstop.services.BookingAware;
import fstop.services.CommonService;
import fstop.services.NoticeInfo;
import fstop.services.NoticeInfoGetter;
import fstop.services.OnLineTransferNotice;
import fstop.services.TelCommExecWrapper;
import fstop.services.TransferNotice;
import fstop.services.WriteLogInterface;
import fstop.telcomm.TelCommExec;
import fstop.telcomm.cmd.CommandUtils;
import fstop.util.DateTimeUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import topibs.utility.NumericUtil;

@Slf4j
public class MB_N075 extends CommonService implements BookingAware, WriteLogInterface {
//	private Logger logger = Logger.getLogger(getClass());
	
//	private TelCommExec n075Telcomm;
//	
//	private TelCommExec n075Telcomm_m;
//	
//	private TxnTwRecordDao txnTwRecordDao; 
//	
//	private TxnTwScheduleDao txnTwScheduleDao;
//	
//	private TxnReqInfoDao txnReqInfoDao;
//	
//	private TxnUserDao txnUserDao;
//	
//	@Required
//	public void setTxnUserDao(TxnUserDao txnUserDao) {
//		this.txnUserDao = txnUserDao;
//	}
//	
//	@Required
//	public void setTxnReqInfoDao(TxnReqInfoDao txnReqInfoDao) {
//		this.txnReqInfoDao = txnReqInfoDao;
//	}
//
//	
//	@Required
//	public void setN075Telcomm(TelCommExec telcomm) {
//		n075Telcomm = telcomm;
//	}
//	
//	@Required
//	public void setN075Telcomm_m(TelCommExec telcomm) {
//		n075Telcomm_m = telcomm;
//	}
//	
//	@Required
//	public void setTxnTwRecordDao(TxnTwRecordDao txnTwRecordDao) {
//		this.txnTwRecordDao = txnTwRecordDao;
//	}
//
//	
//	@Required
//	public void setTxnTwScheduleDao(TxnTwScheduleDao dao) {
//		this.txnTwScheduleDao = dao;
//	}

    public final static String MBQR_VERSION = "MBQR";
    
	@Autowired
	@Qualifier("n075Telcomm_m")
    private TelCommExec n075Telcomm;
	@Autowired
	@Qualifier("n075_mbqrTelcomm")
    private TelCommExec n075_mbqrTelcomm;
    
	@Autowired
	private TxnTwSchPayDao txntwschpaydao;
	@Autowired
	private TxnTwSchPayDataDao txntwschpaydatadao;
	@Autowired
	private TxnTwRecordDao txnTwRecordDao; 
	@Autowired
	private TxnUserDao txnUserDao;
	@Autowired
	private SysDailySeqDao sysDailySeqDao;
	
//	@Autowired
//	private TxnTwScheduleDao txnTwScheduleDao;
//	@Autowired
//	private TxnReqInfoDao txnReqInfoDao;
//	@Autowired
//	private TxnUserDao txnUserDao;
	
	
	public MVH doAction(Map params) { 

		//晶片金融卡
		if("2".equals(params.get("FGTXWAY").toString()))
		{
			Date d = new Date();
			String trin_transeq = "1234";
			String trin_issuer = "05000000";
			String trin_acnno = params.get("ACNNO").toString();
			String trin_icdttm = DateTimeUtils.format("yyyyMMddHHmmss", d);
			
			log.trace("doAction.ICDTTM: {}", trin_icdttm);
			
			String trin_iseqno = params.get("iSeqNo").toString();
			String trin_icmemo = "";
			String trin_tac_length = "000A";		
			String trin_tac = params.get("TAC").toString();
			
			log.trace(ESAPIUtil.vaildLog("doAction.TAC: " + trin_tac_length + trin_tac + "(" + (trin_tac_length.length() + trin_tac.length()) + ")"));
			
			String trin_trmid = params.get("TRMID").toString();
		
			params.put("TRANSEQ", trin_transeq);
			params.put("ISSUER", trin_issuer);
			params.put("ACNNO", trin_acnno);
			params.put("ICDTTM", trin_icdttm);
			params.put("ICSEQ", trin_iseqno);
			params.put("ICMEMO", trin_icmemo);
			params.put("TAC_Length", trin_tac_length);
			params.put("TAC", trin_tac);
			params.put("TRMID", trin_trmid);
		}
		String fgtxdate = (String)params.get("FGTXDATE");
		
		if("1".equals(fgtxdate)) {  //即時
			OnLineTransferNotice trNotice = new OnLineTransferNotice();
			
			return this.online(params, trNotice);
		}
		else if("2".equals(fgtxdate)) {  //預約
			
			if("0".equals(params.get("FGTXWAY"))) {  //使用交易密碼, 做預約
//				String TransPassUpdate = (String)params.get("TRANSPASSUPDATE");
//				String Phase2 = (String)params.get("PHASE2");
				TelCommExec n950CMTelcomm;
//				
//				if(StrUtils.isNotEmpty(TransPassUpdate))
//				{
//					if(TransPassUpdate.equals("1") || Phase2.equals("ON"))
//					{
						n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n951CMTelcomm");
//					}
//					else
//					{
//						n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n950CMTelcomm");
//					}
//				}
//				else
//				{
//					n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n950CMTelcomm");
//				}
				MVHImpl n950Result = n950CMTelcomm.query(params);
				//若驗證失敗, 則會發動 TopMessageException
			}
			else if("2".equals(params.get("FGTXWAY"))) {   //使用晶片金融卡, 做預約
				
				TelCommExec n954Telcomm = (TelCommExec)SpringBeanFactory.getBean("n954Telcomm");
				MVHImpl n954Result = n954Telcomm.query(params);
				//若驗證失敗, 則會發動 TopMessageException
			}
			
			return this.booking(params);
		}
		
		return new MVHImpl();

	}
	
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@SuppressWarnings("unchecked")
/*  20130804
 *  因為二扣時會發生 TXNTWRECORD table lock 所以先 MARK Transactional(noRollbackFor=RuntimeException.class)
	@Transactional(noRollbackFor=RuntimeException.class)
 */
	public MVHImpl online(Map _params, TransferNotice trNotice) { 
		final Map<String, String> params = _params;
		
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);

		String f = params.get("FGTXDATE");		
		if("1".equals(f)) {  //		即時
		
			//產生 CERTACN update by Blair -------------------------------------------- Start
			String TxWay = params.get("FGTXWAY");
			String certACN = "";
			
			if("0".equals(TxWay)){
				while(certACN.length() < 19)certACN += " ";
			}
			else if("1".equals(TxWay)){
				while(certACN.length() < 19)certACN += "1";
			}
			else if("2".equals(TxWay)){
				while(certACN.length() < 19)certACN += "4";
			}
			else if("3".equals(TxWay)){
				while(certACN.length() < 19)certACN += "6";
			}
            else if("4".equals(TxWay)){
                while(certACN.length() < 19)certACN += "8";
            }
            else if("5".equals(TxWay)){//快速交易
                while(certACN.length() < 19)certACN += "A";
            }
            else if("6".equals(TxWay)){//小額支付
                while(certACN.length() < 19)certACN += "B";
            }
			params.put("CERTACN", certACN);
			//產生 CERTACN update by Blair -------------------------------------------- End
		}
		
/**
 * 畫面欄位名稱改為電文欄位名稱，並且資料處理改在Service處理，故註解
		String limdat = (String)params.get("CMDATE1");
		limdat = StrUtils.trim(limdat).replaceAll("/", "");   // 將 yyy/MM/dd 的 "/" 去掉, 變成 yyyMMdd
		//Date dLimdat = DateTimeUtils.parse("yyyyMMdd", limdat);
		//limdat = DateTimeUtils.getCDateShort(dLimdat);
		//limdat = limdat.substring(1);	//原為六碼，20100421改為7碼，故此行註解掉
		params.put("LIMDAT", limdat);
 */		
		

        final TelCommExecWrapper execwrapper = new TelCommExecWrapper(n075TelcommSwitchByVersion(params.get("#version")));

		MVHImpl result = execwrapper.query(params);
		
		final TXNTWRECORD record = (TXNTWRECORD)this.writeLog(execwrapper, params, result);
		
		final TelCommExecWrapper execwrapperFinal = execwrapper;
		trNotice.sendNotice(new NoticeInfoGetter() {

			public NoticeInfo getNoticeInfo() {				
				NoticeInfo result = new NoticeInfo(params.get("UID"), params.get("DPMYEMAIL"), params.get("CMTRMAIL"));
				
				// 20190510 edit by hugo 表格異動 欄位已經移除
				// result.setDpretxstatus(record.getDPRETXSTATUS());
				result.setADTXNO(record.getADTXNO());
				result.setTemplateName("powerbill");
				result.setException(execwrapperFinal.getLastException());
				String datetime = record.getDPTXDATE() + record.getDPTXTIME();
				Date d = DateTimeUtils.parse("yyyyMMddHHmmss", datetime);
				String trantime = DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", d);
				result.getParams().put("#TRANTIME", trantime);
				result.getParams().put("#SEQNO", record.getDPTXNO());
				
				String acn = record.getDPSVAC();
				try {
					acn = StrUtils.left(acn, acn.length() - 4) + "***" + StrUtils.right(acn, 1);
				}
				catch(Exception e){}
				
				String outacn = record.getDPWDAC();
				try {
					outacn = StrUtils.left(outacn, outacn.length() - 5) + "***" + StrUtils.right(outacn, 2);
				}
				catch(Exception e){}
				
				result.getParams().put("#ACN", acn);
				result.getParams().put("#OUTACN", outacn);
				result.getParams().put("#OUTCURRENCY", "新台幣");
				String dptxamtfmt = NumericUtil.formatNumberString(record.getDPTXAMT(), 0);
				result.getParams().put("#AMT", dptxamtfmt);
				result.getParams().put("#MEMO", params.get("CMMAILMEMO"));
				
				ADMMAILLOG admmaillog = new ADMMAILLOG();
				admmaillog.setADACNO(record.getDPWDAC());
				
				String trans_status = (String)params.get("__TRANS_STATUS");
				if(StrUtils.isEmpty(trans_status)) {
					trans_status = "TWONLINE";
				}
				else if("SEND".equals(trans_status)) {
					trans_status = "TWSCH";
				}
				else if("RESEND".equals(trans_status)) {
					trans_status = "TWSCHRE";
				}
				
				admmaillog.setADBATCHNO(trans_status);
				admmaillog.setADMAILACNO(StrUtils.trim(params.get("CMTRMAIL")));
				admmaillog.setADUSERID(record.getDPUSERID());
				admmaillog.setADUSERNAME("");
				admmaillog.setADSENDTYPE("3");
				admmaillog.setADSENDTIME(datetime);
				admmaillog.setADSENDSTATUS("");
				
				result.setADMMAILLOG(admmaillog);
				
				return result;
			}
			
		});

		
		execwrapper.throwException();
		
		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date())); 
		
		try {
			result.getFlatValues().put("CMTXTIME", 
					DateTimeUtils.getCDateTime(result.getValueByFieldName("DATE"),
											result.getValueByFieldName("TIME")));
		}
		catch(Exception e){
			log.error("",e);			
		}
		
		return result;
	}
	public Map<String, String> scheduleObjectMapping() {
		Map<String, String> map = new HashMap();
		
		map.put("DPUSERID", "UID");
		map.put("DPWDAC", "OUTACN"); // 轉出帳號
		//map.put("DPSVBH", ""); // 轉入分行
		map.put("DPSVAC", "ELENUM"); // 轉入帳號, 使用原始輸入的來記錄
		map.put("DPTXAMT", "AMOUNT");
		map.put("DPTXMEMO", "CMTRMEMO");
		map.put("DPTXMAILS", "CMTRMAIL");
		map.put("DPTXMAILMEMO", "CMMAILMEMO"); // CMMAILMEMO 對應的欄位

		map.put("ADOPID", "ADOPID");

		map.put("DPTXCODE","FGTXWAY"); //SSL: 1 or IKEY: 2
		
		/*
		txntwschedule.setDPUSERID(params.get("UID"));
		txntwschedule.setDPWDAC(params.get("OUTACN"));  //轉出帳號
		txntwschedule.setDPSVBH("");  //轉入分行
		txntwschedule.setDPSVAC(params.get("ELENUM"));  //轉入帳號
		txntwschedule.setDPTXAMT(params.get("AMOUNT"));
		txntwschedule.setDPTXMEMO(params.get("CMTRMEMO"));
		txntwschedule.setDPTXMAILS(params.get("CMTRMAIL"));
		txntwschedule.setDPTXMAILMEMO(params.get("CMMAILMEMO"));  //CMMAILMEMO 對應的欄位

		txntwschedule.setADOPID(params.get("TXID"));
		*/
		return map;
	}

	@Transactional
	public MVHImpl booking(Map<String, String> params) {
		log.trace(ESAPIUtil.vaildLog("N075.booking.params: {}"+ params));
		TXNTWSCHPAY txntwschpay = new TXNTWSCHPAY();
		
		// CERTACN
		String TxWay = params.get("FGTXWAY");
		String certACN = "";
		
		if("0".equals(TxWay)){
			while(certACN.length() < 19)certACN += "2";
		}
		else if("1".equals(TxWay)){
			while(certACN.length() < 19)certACN += "3";
		}
		else if("2".equals(TxWay)){
			while(certACN.length() < 19)certACN += "5";
		}
		else if("3".equals(TxWay)){
			while(certACN.length() < 19)certACN += "7";
		}
        else if("4".equals(TxWay)){
            while(certACN.length() < 19)certACN += "9";
        }
		params.put("CERTACN", certACN);
		
		
		// 操作功能ID
		txntwschpay.setADOPID(params.get("ADOPID"));
		// 使用者ID
		txntwschpay.setDPUSERID(params.get("CUSIDN"));
		// 轉出帳號
		txntwschpay.setDPWDAC(params.get("OUTACN"));
		// 轉入行庫代碼
		txntwschpay.setDPSVBH("050");
		// 轉入帳號/繳費代號
		txntwschpay.setDPSVAC(params.get("ELENUM"));
		// 轉帳金額
		txntwschpay.setDPTXAMT(params.get("AMOUNT"));
		// 備註
		txntwschpay.setDPTXMEMO(params.get("CMTRMEMO"));
		// 發送Mail清單
		txntwschpay.setDPTXMAILS(params.get("CMTRMAIL"));
		// Mail備註
		txntwschpay.setDPTXMAILMEMO(params.get("CMMAILMEMO"));
		// 交易機制
		txntwschpay.setDPTXCODE(TxWay);
		// 取得預約編號(YYMMDD+三位流水號)
		String dptmpschno = txnUserDao.getDptmpschno(params.get("CUSIDN"));
		txntwschpay.setDPSCHNO(dptmpschno);
		
		// 預約狀態
		txntwschpay.setDPTXSTATUS("0");
		
		// 系統別
		txntwschpay.setLOGINTYPE(params.get("LOGINTYPE"));

		// 寫入日期
		Date d = new Date();
		txntwschpay.setDPSDATE(DateTimeUtils.format("yyyyMMdd", d));
		txntwschpay.setDPSTIME(DateTimeUtils.format("HHmmss", d));

		txntwschpay.setLASTDATE(txntwschpay.getDPSDATE());
		txntwschpay.setLASTTIME(txntwschpay.getDPSTIME());

		String f = params.get("FGTXDATE");
		if ("2".equals(f)) { // 預約某一日
			params.put("CMDATE", StrUtils.trim(params.get("CMDATE")).replaceAll("/", ""));
			if (params.get("CMDATE").length() == 0) {
				throw TopMessageException.create("ZX99");
			} else {
				txntwschpay.setDPTXTYPE("S");
				txntwschpay.setDPPERMTDATE("");
				txntwschpay.setDPFDATE(params.get("CMDATE"));
				txntwschpay.setDPTDATE(params.get("CMDATE"));
			}
		} else if ("3".equals(f)) {
			params.put("CMSDATE", StrUtils.trim(params.get("CMSDATE")).replaceAll("/", ""));
			params.put("CMEDATE", StrUtils.trim(params.get("CMEDATE")).replaceAll("/", ""));
			if (params.get("CMDD").length() == 0 || params.get("CMSDATE").length() == 0
					|| params.get("CMEDATE").length() == 0) {
				throw TopMessageException.create("ZX99");
			} else {
				txntwschpay.setDPTXTYPE("C");
				txntwschpay.setDPPERMTDATE(params.get("CMDD"));
				txntwschpay.setDPFDATE(params.get("CMSDATE"));
				txntwschpay.setDPTDATE(params.get("CMEDATE"));
			}
		}
		
		if (params.containsKey("pkcs7Sign") && "1".equals(params.get("FGTXWAY"))) {
			List<String> commands = new ArrayList();
			commands.add("XMLCA()");
			commands.add("XMLCN()");

			CommandUtils.doBefore(commands, params);

			if (!StrUtils.trim(params.get("XMLCN")).toUpperCase()
					.startsWith(StrUtils.trim(params.get("UID")).toUpperCase())) {
				throw TopMessageException.create("Z089");
			}

			txntwschpay.setXMLCA(StrUtils.trim(params.get("XMLCA")));
			txntwschpay.setXMLCN(StrUtils.trim(params.get("XMLCN")));
		}
		
		// TODO DPTXINFO放不下這兩個欄位，先拿掉，若預約交易執行時需要再看怎麼存
		params.remove("pkcs7Sign");
		params.remove("jsondc");
		// 預約時上行電文必要資訊
		log.trace(ESAPIUtil.vaildLog("txntwschpay.params>>>{}"+ params));
		String reqinfo_Str = "{}";
		String sha1_Mac = "";
		reqinfo_Str =  CodeUtil.toJson(params);
		sha1_Mac = EncryptUtil.toEncS1(reqinfo_Str, "UTF-8").substring(0,4);
		
		txntwschpay.setMAC(sha1_Mac);
		txntwschpay.setDPTXINFO(reqinfo_Str);
		txntwschpay.setMSADDR(TxnTwSchPayDao.MS_PAY);
		
		log.trace(ESAPIUtil.vaildLog("txntwschpay: {}"+ txntwschpay));
		txntwschpaydao.save(txntwschpay);
		
		MVHImpl result =  new MVHImpl();
		
		try {
			result.getFlatValues().put("CMTXTIME", DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", d));
			result.getFlatValues().put("TOPMSG", "0000");
			result.getFlatValues().put("MSGCOD", "0000");
		}
		catch(Exception e){
			log.error("",e);
		}
		
		return result;
	}

	
//	@Transactional
//	public MVHImpl booking_old(Map<String, String> params) {
//		TXNTWSCHEDULE txntwschedule = new TXNTWSCHEDULE();
//		
//		//產生 CERTACN update by Blair -------------------------------------------- Start
//		String TxWay = params.get("FGTXWAY");
//		String certACN = "";
//		
//		if("0".equals(TxWay)){
//			while(certACN.length() < 19)certACN += "2";
//		}
//		else if("1".equals(TxWay)){
//			while(certACN.length() < 19)certACN += "3";
//		}
//		else if("2".equals(TxWay)){
//			while(certACN.length() < 19)certACN += "5";
//		}
//		else if("3".equals(TxWay)){
//			while(certACN.length() < 19)certACN += "7";
//		}
//		params.put("CERTACN", certACN);
//		//產生 CERTACN update by Blair -------------------------------------------- End
//		
//		log.info("***************************************預約編號***************************************************");
//		//更新SCHCOUNT(每日預約計數)
//		int scnt = txnUserDao.incSchcount((String)params.get("UID"));
//		
//		//預約編號(YYMMDD+三位流水號)
//		String dptmpschno = "";
//		String ymd = DateTimeUtils.format("yyMMdd", new Date());
//		dptmpschno = ymd + StrUtils.right("000" + scnt, 3); 
//		
//		//賽入預約編號
//		txntwschedule.setDPSCHNO(dptmpschno);
//		log.info("***************************************預約編號***************************************************");
//		
//		//預約某一日
//		params.put("CMDATE", StrUtils.trim(params.get("CMDATE")).replaceAll("/", ""));
//		if(params.get("CMDATE").length()==0)
//		{
//			throw TopMessageException.create("ZX99");
//		}
//		else {
//			txntwschedule.setDPPERMTDATE("");
//			txntwschedule.setDPFDATE(params.get("CMDATE"));
//			txntwschedule.setDPTDATE(params.get("CMDATE"));				
//		}		
//		params.put("ADREQTYPE", "S");   //寫入 TXNLOG 時, S 表示為預約
//		
//		Map<String, String> mapping = scheduleObjectMapping();
//		for(String key : mapping.keySet()) {
//			String v = mapping.get(key);
//			try {
//				BeanUtils.setProperty(txntwschedule, key, params.get(v));
//			} catch (IllegalAccessException e) {
//				throw new ToRuntimeException("", e);
//			} catch (InvocationTargetException e) {
//				throw new ToRuntimeException("", e);
//			}
//		}
//		
//		if(params.containsKey("pkcs7Sign") && "1".equals(params.get("FGTXWAY"))) {
//			List<String> commands = new ArrayList();
//			commands.add("XMLCA()");
//			commands.add("XMLCN()");
//			
//			CommandUtils.doBefore(commands, params);
//			
//			if(!StrUtils.trim(params.get("XMLCN")).toUpperCase().startsWith(StrUtils.trim(params.get("UID")).toUpperCase())) {
//				throw TopMessageException.create("Z089");
//			}
//			
//			txntwschedule.setXMLCA(StrUtils.trim(params.get("XMLCA")));
//			txntwschedule.setXMLCN(StrUtils.trim(params.get("XMLCN")));
//		}
//		
//		
//		TXNREQINFO titainfo = new TXNREQINFO();
//		titainfo.setREQINFO(BookingUtils.requestInfo(params));
//		txnReqInfoDao.save(titainfo);
//		
//		txntwschedule.setDPTXINFO(titainfo.getREQID());
//		
//		String sha1 = DigestUtils.sha1(titainfo.getREQINFO()).substring(0, 4);
//		txntwschedule.setMAC(sha1);
//		
//		
//		Date d = new Date();
//		txntwschedule.setDPSDATE(DateTimeUtils.format("yyyyMMdd", d));
//		txntwschedule.setDPSTIME(DateTimeUtils.format("HHmmss", d));
//		
//		
//		txntwschedule.setDPTXSTATUS("0");
//		txntwschedule.setLASTDATE(txntwschedule.getDPSDATE());
//		txntwschedule.setLASTTIME(txntwschedule.getDPSTIME());
//		
//		txnTwScheduleDao.save(txntwschedule);
//		
//		
//		
//		MVHImpl result =  new MVHImpl();
//		
//		try {
//			result.getFlatValues().put("CMTXTIME", DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", d));
//		}
//		catch(Exception e){
//			log.error("",e);
//		}
//		
//		return result;
//	}
	
	public Object writeLog(TelCommExecWrapper execwrapper, Map<String, String> params, final MVHImpl result) {
		
		TXNTWRECORD record = execwrapper.createTxnTwRecord(sysDailySeqDao);
		
		String date = record.getLASTDATE();
		String time = record.getLASTTIME();
		
//		if(result != null) {
//			try {
//				date = DateTimeUtils.format("yyyyMMdd", 
//						DateTimeUtils.getCDate2Date(result.getValueByFieldName("DATE")));
//			} catch (Exception e){
//				log.error("", e);
//			}
//			try {
//				time = result.getValueByFieldName("TIME");
//			} catch (Exception e){
//				log.error("", e);
//			}			
//		} else {
//			log.warn("result is null...");
//		}
//		log.info("record set other params...");
		
		record.setDPUSERID(params.get("UID"));
		record.setADOPID(params.get("ADOPID"));
		record.setDPTXDATE(date);
		record.setDPTXTIME(time);
		record.setDPWDAC(params.get("OUTACN"));
		record.setDPSVBH("050");
		record.setDPSVAC(params.get("ELENUM"));
		record.setDPTXAMT(params.get("AMOUNT"));

		record.setDPTXMEMO(params.get("CMTRMEMO"));
		record.setDPTXMAILS(params.get("CMTRMAIL"));
		record.setDPTXMAILMEMO(params.get("CMMAILMEMO"));  //CMMAILMEMO 對應的欄位
		
		record.setDPEFEE("0");
		record.setDPTXNO("");  //跨行序號
		record.setDPTXCODE(params.get("FGTXWAY"));
		
		record.setLOGINTYPE(params.get("LOGINTYPE")); // ex:NB
		
		// IKEY生成XMLCA、XMLCN後即不需要pkcs7Sign、jsondc，欄位長度過長資料庫塞不進去，故remove
		params.remove("pkcs7Sign"); 
		params.remove("jsondc");
		params.remove("__SIGN");
		record.setDPTITAINFO(CodeUtil.toJson(params));
		
		// 20190510 edit by hugo 表格異動 欄位已經移除
		// record.setDPSCHNO(params.get("__SCHNO")==null?"" : params.get("__SCHNO"));
		
/**
 * 20190510 edit by hugo 改成不寫入 TXNREQINFO 只寫入TXNTWRECORD
 * 
		TXNREQINFO titainfo = null;
		if(record.getDPTITAINFO() == 0) {
			titainfo = new TXNREQINFO();
			titainfo.setREQINFO(BookingUtils.requestInfo(params));
		}

		txnTwRecordDao.writeTxnRecord(titainfo, record);
 */
		txnTwRecordDao.save(record);
		
		return record;
	} 
	
    private TelCommExec n075TelcommSwitchByVersion(String version) {
        if(MBQR_VERSION.equalsIgnoreCase(version)) {
            return this.n075_mbqrTelcomm;
        }
        else {
            return this.n075Telcomm;
        }
    }
}
