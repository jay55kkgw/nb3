package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netbank.util.ESAPIUtil;

import fstop.model.MVH;
import fstop.orm.po.ADMMSGCODE;
import fstop.services.TopMsgDBDetecter;
import fstop.services.TopMsgDetecter;
import lombok.extern.slf4j.Slf4j;

/**
 * 主要用來
 * 1.丟入MVH前的前置處理，包含檢核;
 * 2.後製MVH回覆的訊息;
 * @author Hugo
 *
 */
@Service
@Slf4j
public class N750c_Tel_Service extends Common_Tel_Service{
	@Autowired
	private TopMsgDBDetecter errDetect;
	
	@Override
	public HashMap pre_Processing(Map request) {
		log.trace(ESAPIUtil.vaildLog("N750C.pre_Processing.request : {}"+ request));
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) {
		log.trace(ESAPIUtil.vaildLog("N750C.process.request : {}"+ request));
		return super.process(serviceID, request);
	}

	@Override
	public HashMap data_Retouch(MVH mvh) {
		log.trace(ESAPIUtil.vaildLog("N750C.data_Retouch.request : "+ mvh));
		return super.data_Retouch(mvh);
	}

	@Override
	public void setMsgCode(Map dataMap, Boolean data_Retouch_result) {
		log.trace("N750C.data_Retouch.data_Retouch_result : {}", data_Retouch_result);
		super.setMsgCode(dataMap, data_Retouch_result);
	}
	
}
