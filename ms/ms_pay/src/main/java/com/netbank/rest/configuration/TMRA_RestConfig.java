package com.netbank.rest.configuration;

import com.netbank.telcomm.CommonTelcomm;
import com.netbank.telcomm.TMRACommonTelcomm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

@Configuration
@Slf4j
@ConditionalOnProperty(name = "mock.telcomm.datasource", matchIfMissing=true, havingValue = "TMRA")
public class TMRA_RestConfig extends AbstractTelcommConfig {

//    @Value("${tmra.telcomm.url:http://localhost:8880/TMRA/com}")
//    private String restBaseUrl;
    @Value("${tmra.uri.host:http://localhost}")
    private String restHost;
 //   @Value("${tmra.uri.port:8080}")
//    private String restPort;
    @Value("${tmra.uri.path:TMRA/com}")
    private String restPath;
    
    protected CommonTelcomm createTelcomm(String mockfileName, String txid) throws IOException {

    	TMRACommonTelcomm ret = new TMRACommonTelcomm();

        if(restHost.endsWith("/") == false) {
            restHost = restHost + "/";
        }
        String url = restHost + restPath;
        log.info("createTelcomm.url: {}", url);
        
        if(url.endsWith("/") == false) {
            url = url + "/";
        }

        url = url + txid.toLowerCase();
        ret.setUrl(url);

        ret.setTxid(txid);

        log.info("create rest mock telcomm, txid : {}, url: {}", ret.getTxid(), ret.getUrl());

        return ret;
    }


}
