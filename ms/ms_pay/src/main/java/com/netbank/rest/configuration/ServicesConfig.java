package com.netbank.rest.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import fstop.services.ServerConfig;
import fstop.services.TopMsgDBDetecter;
import fstop.services.TopMsgF003Detecter;
import fstop.services.TopMsgF007Detecter;
import fstop.services.TopMsgFXDetecter;
import fstop.services.TopMsgFundDetecter;
import fstop.services.TopMsgN320Detecter;
import fstop.services.TopMsgN950Detecter;
import fstop.services.impl.CallMobileCenter;
import fstop.services.impl.EBPay1;
import fstop.services.impl.EBPay1_1;
import fstop.services.impl.N070A;
import fstop.services.impl.N070B;
import fstop.services.impl.N073;
import fstop.services.impl.N075;
import fstop.services.impl.N110;
import fstop.services.impl.N171;
import fstop.services.impl.N687;
import fstop.services.impl.N750A;
import fstop.services.impl.N750B;
import fstop.services.impl.N750C;
import fstop.services.impl.N750E;
import fstop.services.impl.N750F;
import fstop.services.impl.N750G;
import fstop.services.impl.N750H;
import fstop.services.impl.N830;
import fstop.services.impl.N8301;
import fstop.services.impl.N8302;
import fstop.services.impl.N831;
import fstop.services.impl.N8330;
import fstop.services.impl.Q00001;
import fstop.services.impl.SmsOtp;
import fstop.va.VATransform;
import fstop.ws.eai.client.N701WebServiceClient;
import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class ServicesConfig {

    @Bean
    protected VATransform vatransform() throws Exception {
        return new VATransform();
    }
//    @Bean
//    protected CheckServerHealth checkserverhealth() throws Exception {
//    	return new CheckServerHealth();
//    }

    @Bean
    protected EBPay1 ebpay1Service() throws Exception {
        return new EBPay1();
    }    
    
    @Bean
    protected EBPay1_1 ebpay1_1Service() throws Exception {
        return new EBPay1_1();
    }    
    
    @Bean
    protected N701WebServiceClient n701WebServiceClient() throws Exception {
        return new N701WebServiceClient();
    }
	
    @Bean
    protected TopMsgDBDetecter dbErrDetect() throws Exception {
        return new TopMsgDBDetecter();
    }

    @Bean
    protected TopMsgN320Detecter n320ErrDetect() throws Exception {
        return new TopMsgN320Detecter();
    }

    @Bean
    protected TopMsgFXDetecter fxErrDetect() throws Exception {
        return new TopMsgFXDetecter();
    }

    @Bean
    protected TopMsgF003Detecter f003ErrDetect() throws Exception {
        return new TopMsgF003Detecter();
    }

    @Bean
    protected TopMsgF007Detecter f007ErrDetect() throws Exception {
        return new TopMsgF007Detecter();
    }

    @Bean
    protected TopMsgFundDetecter fundErrDetect() throws Exception {
        return new TopMsgFundDetecter();
    }
    @Bean
    protected TopMsgN950Detecter n950ErrDetect() throws Exception {
        return new TopMsgN950Detecter();
    }
    @Bean
    protected CallMobileCenter callmobilecenterService() throws Exception {
    	return new CallMobileCenter();
    }
    @Bean
    protected N070A n070aService() throws Exception {
        return new N070A();
    }    

    @Bean
    protected N070B n070bService() throws Exception {
        return new N070B();
    }    
    
    @Bean
    protected N073 n073Service() throws Exception {
        return new N073();
    }    
    
	@Bean
    protected N075 n075Service() throws Exception {
    	return new N075();
    }
    
    @Bean
    protected N110 n110Service() throws Exception {
        return new N110();
    }
	
    @Bean
    protected N171 n171Service() throws Exception {
        return new N171();
    }
    
    @Bean
    protected N750A n750aService() throws Exception {
    	return new N750A();
    }
    
    @Bean
    protected N750B n750bService() throws Exception {
    	return new N750B();
    }

    @Bean
    protected N750C n750cService() throws Exception {
    	return new N750C();
    }

    @Bean
    protected N750E n750eService() throws Exception {
    	return new N750E();
    }
    
    @Bean
    protected N750F n750fService() throws Exception {
    	return new N750F();
    }
    
    @Bean
    protected N750G n750gService() throws Exception {
    	return new N750G();
    }
    	
    @Bean
    protected N750H n750hService() throws Exception {
    	return new N750H();
    }
    
    @Bean
    protected N830 n830Service() throws Exception {
        return new N830();
    }
    
    @Bean
    protected N8301 n8301Service() throws Exception {
        return new N8301();
    }
    
    @Bean
    protected N8302 n8302Service() throws Exception {
        return new N8302();
    }
    
    @Bean
    protected N831 n831Service() throws Exception {
        return new N831();
    }
    
    @Bean
    protected N8330 n8330Service() throws Exception {
        return new N8330();
    }
    
    //<!-- 後台OTP狀態 -->
    @Bean
    protected ServerConfig serverConfig() throws Exception {
        return new ServerConfig();
    }
    
    @Bean
    protected SmsOtp smsotpService() throws Exception {
    	return new SmsOtp();
    }
    @Bean
    protected Q00001 q00001Service() throws Exception {
    	return new Q00001();
    }
    
    @Bean
    protected N687 n687Service() throws Exception {
        return new N687();
    }
}
