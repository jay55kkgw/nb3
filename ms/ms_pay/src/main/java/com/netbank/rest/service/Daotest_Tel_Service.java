package com.netbank.rest.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netbank.util.ESAPIUtil;

import fstop.model.MVH;
import fstop.orm.dao.AdmBankDao;
import fstop.orm.po.ADMBANK;
import lombok.extern.slf4j.Slf4j;
/**
 * DAO TEST
 *
 */
@Service
@Slf4j
public class Daotest_Tel_Service extends Common_Tel_Service
{
	
	@Autowired
	private AdmBankDao admBankDao;
	
	@Override
	public HashMap pre_Processing(Map request) {
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) {
		return super.process(serviceID, request);
	}

	@Override
	public HashMap data_Retouch(MVH mvh) {
		try
		{
			log.trace("Daotest_Tel_Service data_Retouch start");
			List<ADMBANK> banks = admBankDao.getAll();
			log.trace(ESAPIUtil.vaildLog("Daotest_Tel_Service admBankDao.getAll = "+ banks));
		}
		catch (Exception e)
		{
			log.error("Daotest_Tel_Service data_Retouch error", e);
		}
		
		return super.data_Retouch(mvh);
	}
}
