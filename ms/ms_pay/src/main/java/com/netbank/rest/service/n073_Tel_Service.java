package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netbank.util.ESAPIUtil;

import fstop.model.MVH;
import fstop.orm.po.ADMMSGCODE;
import fstop.services.TopMsgDBDetecter;
import lombok.extern.slf4j.Slf4j;

/**
 * 主要用來
 * 1.丟入MVH前的前置處理，包含檢核;
 * 2.後製MVH回覆的訊息;
 * @author Hugo
 *
 */
@Service
@Slf4j
public class n073_Tel_Service extends Common_Tel_Service {
	
	@Autowired
	private TopMsgDBDetecter errDetect;
	
	@Override
	public HashMap pre_Processing(Map request) {
		log.trace("N073.setMsgCode...");
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) {
		log.trace("N073.setMsgCode...");
		return super.process(serviceID, request);
	}

	@Override
	public HashMap data_Retouch(MVH mvh) {
		log.trace(ESAPIUtil.vaildLog("N073.data_Retouch.request : "+ mvh));
		
		// 判斷交易結果並回傳
		HashMap<Object, Object> resultMap = new HashMap<Object, Object>();
		resultMap = super.data_Retouch(mvh);
		
		try {
			// 失敗應為錯誤代碼，成功應為轉出帳號
			String topMsg = resultMap.get(TOPMSG).toString();
			log.trace(ESAPIUtil.vaildLog("N073.data_Retouch.topMsg: {}"+ topMsg));
				
			// TOPMSG是否為錯誤代碼
			ADMMSGCODE isMsgCode = errDetect.isError(topMsg, null);
			log.trace(ESAPIUtil.vaildLog("N073.data_Retouch.isMsgCode : " + isMsgCode));
			
			if (topMsg != null && isMsgCode != null) {
				// TOPMSG是錯誤代碼
				log.trace("N073.data_Retouch.isMsgCode...");
			}else {
				log.trace("N073.data_Retouch.isNotMsgCode...");
				resultMap.replace("TOPMSG", "0000");
			}
			super.setMsgCode(resultMap, Boolean.TRUE);
			
			
		} catch (Exception e) {
			log.error("data_Retouch error >> {}",e);
		}
		return resultMap;
	}

	@Override
	public void setMsgCode(Map dataMap, Boolean data_Retouch_result) {
		log.trace("N073.setMsgCode...");
		super.setMsgCode(dataMap, data_Retouch_result);
	}
}