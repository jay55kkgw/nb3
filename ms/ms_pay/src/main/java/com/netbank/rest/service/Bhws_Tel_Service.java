package com.netbank.rest.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.owasp.esapi.ESAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netbank.rest.model.CommonPools;
import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.ws.BHWebServiceObj;
import lombok.extern.slf4j.Slf4j;

/**
 * 主要用來 1.丟入MVH前的前置處理，包含檢核; 2.後製MVH回覆的訊息;
 * 
 *
 */
@Service
@Slf4j
public class Bhws_Tel_Service extends Common_Tel_Service {

	@Autowired
	private CommonPools commonPools;

	@Override
	public HashMap pre_Processing(Map request) {
		// TODO Auto-generated method stub
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) {
		MVH mvh = null;
		try {
			switch ((String) request.get("QUERYTYPE")) {
			case "BHWSHtml":
				mvh = new MVHImpl();
				mvh = getBHWSHtml(request);
				break;
			case "BHWSReissue":
				mvh = new MVHImpl();
				mvh = sendBHWSReissue(request);
				break;
			case "BHWSHtmlImg":
				mvh = getBHWSHtmlImg(request);
				break;
			}

		} catch (TopMessageException e) {
			log.error("process.error>>",e);
			MVHImpl m = new MVHImpl();
			m.getFlatValues().put("TOPMSG", e.getMsgcode());
			mvh = m;
		} catch (Exception e) {
			log.error("process.error>>{}", e);
			((MVHImpl) mvh).getFlatValues().put("TOPMSG", "Z605");
		} finally {
			// TODO trur 做data_Retouch false 做msgCode 回覆處理
			return data_Retouch(mvh);
		}
	}

	@Override
	public HashMap data_Retouch(MVH mvh) {
		// TODO Auto-generated method stub
		return super.data_Retouch(mvh);
	}

	@Override
	public void setMsgCode(Map dataMap, Boolean data_Retouch_result) {
		// TODO Auto-generated method stub
		super.setMsgCode(dataMap, data_Retouch_result);
	}

	private MVH getBHWSHtml(Map request) {
		MVH mvh = null;
		BHWebServiceObj[] bhobjs = null;
		BHWebServiceObj bhobj = null;
		Map m = new HashMap();
		HashMap map1 = new HashMap();
		List crdTypeList = new ArrayList();
		int counter = 0;
		String bh_userid = "";
		String errcode = "";
		String iserror = "N";
		String billNo = "";
		String projectNo = "";
		String userID = "";
		String queryDate = "";
		String resultNo = "";
		String billMonth = "";
		String email = "";
		String inwards = "";
		mvh = new MVHImpl();
		String cardtype = (String) request.get("CARDTYPE");
		String period = (String) request.get("FGPERIOD");
		String custID = (String) request.get("CUSIDN");

		String bill_no = (String) request.get("BILL_NO");
		String cust_id = custID;
		String start_dt = "";
		String end_dt = "";
		if (null == commonPools.bh.getBHWSResult(bill_no, cust_id, start_dt, end_dt)) {
			throw TopMessageException.create("Z605");
		}
		bhobjs = commonPools.bh.getBHWSResult(bill_no, cust_id, start_dt, end_dt);

		if (bhobjs.length > 0) {
			// 先取出有幾種card_type
			for (int i = 0; i < bhobjs.length; i++) {
				map1.put(bhobjs[i].getBill_Month(), bhobjs[i].getBill_Month()); // 把取得的帳單月份存在HashMap
				// log.debug("bhobjs:[trancode]:N820_2.jsp, [card]["+i+"]:" +
				// bhobjs[i].getCARD() + ", [Result_no]:" + bhobjs[i].getResult_no() + ",
				// [Query_Date]:" + bhobjs[i].getQuery_Date() + ", [Bill_Month]:" +
				// bhobjs[i].getBill_Month());
				log.trace("bhobjs:[trancode]:N820_2.jsp, [card][" + i + "]:" + bhobjs[i].getCARD() + ", [Result_no]:"
						+ bhobjs[i].getResult_no() + ", [Query_Date]:" + bhobjs[i].getQuery_Date() + ", [Bill_Month]:"
						+ bhobjs[i].getBill_Month());
				if (!crdTypeList.contains(bhobjs[i].getCARD()) && bhobjs[i].getCARD() != null) {
					log.debug(ESAPIUtil.vaildLog("crdTypeList:[trancode]:N820_2.jsp, [UID]:" + custID + "[period]:" + period
							+ ", [card][" + i + "]:" + bhobjs[i].getCARD() + ", [Userid]:" + bhobjs[i].getUserid()
							+ ", [Result_no]:" + bhobjs[i].getResult_no()));

					// crdTypeList.add(bhobjs[i].getCARD());
					crdTypeList.add(bhobjs[i].getResult_no());
				}
			}
			// 排序月份已方便取得本期月份、上期月份、上上期月份
			Object[] key = map1.keySet().toArray();
			// Arrays.sort(key);
			int[] key1 = new int[key.length];
			for (int i = 0; i < key.length; i++) {
				key1[i] = Integer.parseInt(map1.get(key[i]).toString());
				// log.debug("bill month:"+key[i]);
				log.trace("bill month >>{}", key[i]);
			}
			// log.debug("Original:"+Arrays.toString(key1));
			log.trace("Original >>{}", Arrays.toString(key1));
			Arrays.sort(key1);
			// log.debug("Sorted:"+Arrays.toString(key1));
			log.trace("Sorted >>{}", Arrays.toString(key1));
			counter = key1.length;
			// log.debug("total bill counter:"+counter);
			log.trace("total bill counter >>{}", counter);
			Calendar cal = Calendar.getInstance();
			int month = cal.get(Calendar.MONTH) + 1; // 取得當月月份
			int maxmonth = 0; // 本期月份
			int midmonth = 0; // 上期月份
			int minmonth = 0; // 上上期月份
			if (map1.containsValue(String.valueOf(month))) // 比對本期帳單是否有當月份資料，如沒有，以上月份帳單當作本期帳單
			{
				maxmonth = month;
				if (maxmonth == 0) {
					maxmonth = 12;
					midmonth = 11;
					minmonth = 10;
				} else if (maxmonth == 1) {
					midmonth = 12;
					minmonth = 11;
				} else if (maxmonth == 2) {
					midmonth = 1;
					minmonth = 12;
				} else {
					midmonth = maxmonth - 1;
					minmonth = maxmonth - 2;
				}
			} else {
				if (key1.length == 1)
					maxmonth = key1[0];
				else if (key1.length == 2) {
					maxmonth = key1[key1.length - 1];
					midmonth = key1[key1.length - 2];
					if (midmonth == 0) {
						minmonth = 11;
					} else if (midmonth == 1) {
						minmonth = 12;
					} else {
						minmonth = midmonth - 1;
					}
				} else if (key1.length == 3) {
					maxmonth = key1[key1.length - 1];
					midmonth = key1[key1.length - 2];
					minmonth = key1[key1.length - 3];
				} else {
					maxmonth = month - 1;
					if (maxmonth == 0) {
						maxmonth = 12;
						midmonth = 11;
						minmonth = 10;
					} else if (maxmonth == 1) {
						midmonth = 12;
						minmonth = 11;
					} else if (maxmonth == 2) {
						midmonth = 1;
						minmonth = 12;
					} else {
						midmonth = maxmonth - 1;
						minmonth = maxmonth - 2;
					}
				}
			}
			// log.debug("maxmonth="+maxmonth+"/midmonth="+midmonth+"/minmonth="+minmonth+"/counter="+counter);
			if ((period.equals("CMCURMON") && counter == 0) || (period.equals("CMLASTMON") && counter < 2)
					|| (period.equals("CMLAST2MON") && counter < 3)) {
				throw TopMessageException.create("Z605");
			}
			for (int k = 0; k < bhobjs.length; k++) {
				// 取出本期帳單
				if (period.equals("CMCURMON") && bhobjs[k].getBill_Month().equals(String.valueOf(maxmonth))) {
					m.put("CMCURMON" + "_" + bhobjs[k].getResult_no(), bhobjs[k]);
				}
				// 取出上期帳單
				if (period.equals("CMLASTMON") && bhobjs[k].getBill_Month().equals(String.valueOf(midmonth))) {
					m.put("CMLASTMON" + "_" + bhobjs[k].getResult_no(), bhobjs[k]);
				}
				// 取出上上期帳單
				if (period.equals("CMLAST2MON") && bhobjs[k].getBill_Month().equals(String.valueOf(minmonth))) {
					m.put("CMLAST2MON" + "_" + bhobjs[k].getResult_no(), bhobjs[k]);
				}
			}
		}

		for (int i = 0; i < crdTypeList.size(); i++) {
			// 取得 bill hunter 相關內容
			if (m.containsKey(period.trim() + "_" + crdTypeList.get(i))) {
				bhobj = (BHWebServiceObj) m.get(period.trim() + "_" + crdTypeList.get(i));

				billNo = bhobj.getBill_no();
				projectNo = bhobj.getProject_No();
				userID = bhobj.getUserid();
				queryDate = bhobj.getQuery_Date().substring(0, 10).replace("-", "/");
				resultNo = bhobj.getResult_no();
				billMonth = bhobj.getBill_Month();
				email = bhobj.getEmail();

				// 帳單內容
				log.debug(ESAPIUtil.vaildLog("[trancode]:N820_2.jsp, [UID]:" + custID + "[period]:" + period + ", [card]:"
						+ cardtype + ", [resultNo]:" + resultNo + ", [projectNo]:" + projectNo + ", [queryDate]:"
						+ queryDate + ", [userID]:" + userID + ", [billNo]:" + billNo
						+ ", [goal]:Bill Content, [stime]:" + new Date()));
				inwards += commonPools.bh.getBHWSHtml(billNo, custID, Integer.parseInt(projectNo),
						Integer.parseInt(userID), queryDate) + "<br>";
				log.debug(ESAPIUtil.vaildLog("[trancode]:N820_2.jsp, [UID]:" + custID + "[period]:" + period + ", [card]:"
						+ cardtype + ", [resultNo]:" + resultNo + ",[goal]:Bill Content, [etime]:" + new Date()));
			}
		}
		String inwardsJS = ESAPI.encoder().encodeForJavaScript(inwards);
		((MVHImpl) mvh).getFlatValues().put("INWARDS", inwards);
		((MVHImpl) mvh).getFlatValues().put("INWARDSJS", inwardsJS);
		((MVHImpl) mvh).getFlatValues().put("TOPMSG", "");
		return mvh;
	}

	private MVH sendBHWSReissue(Map request) {
		MVH mvh = null;
		BHWebServiceObj[] bhobjs = null;
		BHWebServiceObj bhobj = null;
		Map m = new HashMap();
		HashMap map1 = new HashMap();
		List crdTypeList = new ArrayList();
		int counter = 0;
		String bh_userid = "";
		String errcode = "";
		String iserror = "N";
		String billNo = "";
		String projectNo = "";
		String userID = "";
		String queryDate = "";
		String resultNo = "";
		String billMonth = "";
		String email = "";
		String inwards = "";
		boolean reissue = false;
		mvh = new MVHImpl();
		String cardtype = (String) request.get("CARDTYPE");
		String period = (String) request.get("FGPERIOD");
		String custID = (String) request.get("CUSIDN");

		String bill_no = (String) request.get("BILL_NO");
		String cust_id = custID;
		String start_dt = "";
		String end_dt = "";
		if (null == commonPools.bh.getBHWSResult(bill_no, cust_id, start_dt, end_dt)) {
			errcode = "Z605";
			reissue = false;
			throw TopMessageException.create("Z605");
		}
		bhobjs = commonPools.bh.getBHWSResult(bill_no, cust_id, start_dt, end_dt);

		if (bhobjs.length > 0) {
			// 先取出有幾種card_type
			for (int i = 0; i < bhobjs.length; i++) {
				map1.put(bhobjs[i].getBill_Month(), bhobjs[i].getBill_Month()); // 把取得的帳單月份存在HashMap
				// log.debug("bhobjs:[trancode]:N820_2.jsp, [card]["+i+"]:" +
				// bhobjs[i].getCARD() + ", [Result_no]:" + bhobjs[i].getResult_no() + ",
				// [Query_Date]:" + bhobjs[i].getQuery_Date() + ", [Bill_Month]:" +
				// bhobjs[i].getBill_Month());
				log.trace("bhobjs:[trancode]:N820_2.jsp, [card][" + i + "]:" + bhobjs[i].getCARD() + ", [Result_no]:"
						+ bhobjs[i].getResult_no() + ", [Query_Date]:" + bhobjs[i].getQuery_Date() + ", [Bill_Month]:"
						+ bhobjs[i].getBill_Month());
				if (!crdTypeList.contains(bhobjs[i].getCARD()) && bhobjs[i].getCARD() != null) {
					log.debug(ESAPIUtil.vaildLog("crdTypeList:[trancode]:N820_2.jsp, [UID]:" + custID + "[period]:" + period
							+ ", [card][" + i + "]:" + bhobjs[i].getCARD() + ", [Userid]:" + bhobjs[i].getUserid()
							+ ", [Result_no]:" + bhobjs[i].getResult_no()));

					// crdTypeList.add(bhobjs[i].getCARD());
					crdTypeList.add(bhobjs[i].getResult_no());
				}
			}
			// 排序月份已方便取得本期月份、上期月份、上上期月份
			Object[] key = map1.keySet().toArray();
			// Arrays.sort(key);
			int[] key1 = new int[key.length];
			for (int i = 0; i < key.length; i++) {
				key1[i] = Integer.parseInt(map1.get(key[i]).toString());
				// log.debug("bill month:"+key[i]);
				log.trace("bill month >>{}", key[i]);
			}
			// log.debug("Original:"+Arrays.toString(key1));
			log.trace("Original >>{}", Arrays.toString(key1));
			Arrays.sort(key1);
			// log.debug("Sorted:"+Arrays.toString(key1));
			log.trace("Sorted >>{}", Arrays.toString(key1));
			counter = key1.length;
			// log.debug("total bill counter:"+counter);
			log.trace("total bill counter >>{}", counter);
			Calendar cal = Calendar.getInstance();
			int month = cal.get(Calendar.MONTH) + 1; // 取得當月月份
			int maxmonth = 0; // 本期月份
			int midmonth = 0; // 上期月份
			int minmonth = 0; // 上上期月份
			if (map1.containsValue(String.valueOf(month))) // 比對本期帳單是否有當月份資料，如沒有，以上月份帳單當作本期帳單
			{
				maxmonth = month;
				if (maxmonth == 0) {
					maxmonth = 12;
					midmonth = 11;
					minmonth = 10;
				} else if (maxmonth == 1) {
					midmonth = 12;
					minmonth = 11;
				} else if (maxmonth == 2) {
					midmonth = 1;
					minmonth = 12;
				} else {
					midmonth = maxmonth - 1;
					minmonth = maxmonth - 2;
				}
			} else {
				if (key1.length == 1)
					maxmonth = key1[0];
				else if (key1.length == 2) {
					maxmonth = key1[key1.length - 1];
					midmonth = key1[key1.length - 2];
					if (midmonth == 0) {
						minmonth = 11;
					} else if (midmonth == 1) {
						minmonth = 12;
					} else {
						minmonth = midmonth - 1;
					}
				} else if (key1.length == 3) {
					maxmonth = key1[key1.length - 1];
					midmonth = key1[key1.length - 2];
					minmonth = key1[key1.length - 3];
				} else {
					maxmonth = month - 1;
					if (maxmonth == 0) {
						maxmonth = 12;
						midmonth = 11;
						minmonth = 10;
					} else if (maxmonth == 1) {
						midmonth = 12;
						minmonth = 11;
					} else if (maxmonth == 2) {
						midmonth = 1;
						minmonth = 12;
					} else {
						midmonth = maxmonth - 1;
						minmonth = maxmonth - 2;
					}
				}
			}
			// log.debug("maxmonth="+maxmonth+"/midmonth="+midmonth+"/minmonth="+minmonth+"/counter="+counter);
			if ((period.equals("CMCURMON") && counter == 0) || (period.equals("CMLASTMON") && counter < 2)
					|| (period.equals("CMLAST2MON") && counter < 3)) {
				errcode = "Z605";
				reissue = false;
				throw TopMessageException.create("Z605");
			}
			for (int k = 0; k < bhobjs.length; k++) {
				// 取出本期帳單
				if (period.equals("CMCURMON") && bhobjs[k].getBill_Month().equals(String.valueOf(maxmonth))) {
					m.put("CMCURMON" + "_" + bhobjs[k].getResult_no(), bhobjs[k]);
				}
				// 取出上期帳單
				if (period.equals("CMLASTMON") && bhobjs[k].getBill_Month().equals(String.valueOf(midmonth))) {
					m.put("CMLASTMON" + "_" + bhobjs[k].getResult_no(), bhobjs[k]);
				}
				// 取出上上期帳單
				if (period.equals("CMLAST2MON") && bhobjs[k].getBill_Month().equals(String.valueOf(minmonth))) {
					m.put("CMLAST2MON" + "_" + bhobjs[k].getResult_no(), bhobjs[k]);
				}
			}
		}

		for (int i = 0; i < crdTypeList.size(); i++) {
			// 取得 bill hunter 相關內容
			try {
				if (m.containsKey(period.trim() + "_" + crdTypeList.get(i))) {
					bhobj = (BHWebServiceObj) m.get(period.trim() + "_" + crdTypeList.get(i));

					billNo = bhobj.getBill_no();
					projectNo = bhobj.getProject_No();
					userID = bhobj.getUserid();
					queryDate = bhobj.getQuery_Date().substring(0, 10).replace("-", "/");
					resultNo = bhobj.getResult_no();
					billMonth = bhobj.getBill_Month();
					email = bhobj.getEmail();

					Vector v = new Vector();
					v.addElement(bhobj);
					log.debug(ESAPIUtil.vaildLog("[trancode]:N820_3.jsp, [v.size]:" + v.size() + ", [resultNo]:" + resultNo
							+ ", [billMonth]:" + billMonth + ", [email]:" + email));

					if (v.size() > 0) {
						BHWebServiceObj[] ret = new BHWebServiceObj[v.size()];
						v.copyInto(ret);
						reissue = commonPools.bh.sendBHWSReissue(ret);
					}
				}
			} catch (Exception e) {
				e.getStackTrace();
				errcode = "Z602";
				reissue = false;
				throw TopMessageException.create("Z602");
			}
		}

		((MVHImpl) mvh).getFlatValues().put("TOPMSG", "");
		return mvh;
	}

	private MVH getBHWSHtmlImg(Map request) {
		MVH mvh = null;
		BHWebServiceObj[] bhobjs = null;
		BHWebServiceObj bhobj = null;
		Map m = new HashMap();
		HashMap map1 = new HashMap();
		List crdTypeList = new ArrayList();
		int counter = 0;
		String bh_userid = "";
		String errcode = "";
		String iserror = "N";
		String billNo = "";
		String projectNo = "";
		String userID = "";
		String queryDate = "";
		String resultNo = "";
		String billMonth = "";
		String email = "";
		String inwards = "";
		mvh = new MVHImpl();
		String cardtype = (String) request.get("CARDTYPE");
		String period = (String) request.get("FGPERIOD");
		String custID = (String) request.get("CUSIDN");

		String bill_no = (String) request.get("BILL_NO");
		String cust_id = custID;
		String start_dt = "";
		String end_dt = "";
		if (null == commonPools.bh.getBHWSResult(bill_no, cust_id, start_dt, end_dt)) {
			throw TopMessageException.create("Z605");
		}
		bhobjs = commonPools.bh.getBHWSResult(bill_no, cust_id, start_dt, end_dt);

		if (bhobjs.length > 0) {
			// 先取出有幾種card_type
			for (int i = 0; i < bhobjs.length; i++) {
				map1.put(bhobjs[i].getBill_Month(), bhobjs[i].getBill_Month()); // 把取得的帳單月份存在HashMap
				// log.debug("bhobjs:[trancode]:N820_2.jsp, [card]["+i+"]:" +
				// bhobjs[i].getCARD() + ", [Result_no]:" + bhobjs[i].getResult_no() + ",
				// [Query_Date]:" + bhobjs[i].getQuery_Date() + ", [Bill_Month]:" +
				// bhobjs[i].getBill_Month());
				log.trace("bhobjs:[trancode]:N820_2.jsp, [card][" + i + "]:" + bhobjs[i].getCARD() + ", [Result_no]:"
						+ bhobjs[i].getResult_no() + ", [Query_Date]:" + bhobjs[i].getQuery_Date() + ", [Bill_Month]:"
						+ bhobjs[i].getBill_Month());
				if (!crdTypeList.contains(bhobjs[i].getCARD()) && bhobjs[i].getCARD() != null) {
					log.debug(ESAPIUtil.vaildLog("crdTypeList:[trancode]:N820_2.jsp, [UID]:" + custID + "[period]:" + period
							+ ", [card][" + i + "]:" + bhobjs[i].getCARD() + ", [Userid]:" + bhobjs[i].getUserid()
							+ ", [Result_no]:" + bhobjs[i].getResult_no()));

					// crdTypeList.add(bhobjs[i].getCARD());
					crdTypeList.add(bhobjs[i].getResult_no());
				}
			}
			// 排序月份已方便取得本期月份、上期月份、上上期月份
			Object[] key = map1.keySet().toArray();
			// Arrays.sort(key);
			int[] key1 = new int[key.length];
			for (int i = 0; i < key.length; i++) {
				key1[i] = Integer.parseInt(map1.get(key[i]).toString());
				// log.debug("bill month:"+key[i]);
				log.trace("bill month >>{}", key[i]);
			}
			// log.debug("Original:"+Arrays.toString(key1));
			log.trace("Original >>{}", Arrays.toString(key1));
			Arrays.sort(key1);
			// log.debug("Sorted:"+Arrays.toString(key1));
			log.trace("Sorted >>{}", Arrays.toString(key1));
			counter = key1.length;
			// log.debug("total bill counter:"+counter);
			log.trace("total bill counter >>{}", counter);
			Calendar cal = Calendar.getInstance();
			int month = cal.get(Calendar.MONTH) + 1; // 取得當月月份
			int maxmonth = 0; // 本期月份
			int midmonth = 0; // 上期月份
			int minmonth = 0; // 上上期月份
			if (map1.containsValue(String.valueOf(month))) // 比對本期帳單是否有當月份資料，如沒有，以上月份帳單當作本期帳單
			{
				maxmonth = month;
				if (maxmonth == 0) {
					maxmonth = 12;
					midmonth = 11;
					minmonth = 10;
				} else if (maxmonth == 1) {
					midmonth = 12;
					minmonth = 11;
				} else if (maxmonth == 2) {
					midmonth = 1;
					minmonth = 12;
				} else {
					midmonth = maxmonth - 1;
					minmonth = maxmonth - 2;
				}
			} else {
				if (key1.length == 1)
					maxmonth = key1[0];
				else if (key1.length == 2) {
					maxmonth = key1[key1.length - 1];
					midmonth = key1[key1.length - 2];
					if (midmonth == 0) {
						minmonth = 11;
					} else if (midmonth == 1) {
						minmonth = 12;
					} else {
						minmonth = midmonth - 1;
					}
				} else if (key1.length == 3) {
					maxmonth = key1[key1.length - 1];
					midmonth = key1[key1.length - 2];
					minmonth = key1[key1.length - 3];
				} else {
					maxmonth = month - 1;
					if (maxmonth == 0) {
						maxmonth = 12;
						midmonth = 11;
						minmonth = 10;
					} else if (maxmonth == 1) {
						midmonth = 12;
						minmonth = 11;
					} else if (maxmonth == 2) {
						midmonth = 1;
						minmonth = 12;
					} else {
						midmonth = maxmonth - 1;
						minmonth = maxmonth - 2;
					}
				}
			}
			// log.debug("maxmonth="+maxmonth+"/midmonth="+midmonth+"/minmonth="+minmonth+"/counter="+counter);
			if ((period.equals("CMCURMON") && counter == 0) || (period.equals("CMLASTMON") && counter < 2)
					|| (period.equals("CMLAST2MON") && counter < 3)) {
				throw TopMessageException.create("Z605");
			}
			for (int k = 0; k < bhobjs.length; k++) {
				// 取出本期帳單
				if (period.equals("CMCURMON") && bhobjs[k].getBill_Month().equals(String.valueOf(maxmonth))) {
					m.put("CMCURMON" + "_" + bhobjs[k].getResult_no(), bhobjs[k]);
				}
				// 取出上期帳單
				if (period.equals("CMLASTMON") && bhobjs[k].getBill_Month().equals(String.valueOf(midmonth))) {
					m.put("CMLASTMON" + "_" + bhobjs[k].getResult_no(), bhobjs[k]);
				}
				// 取出上上期帳單
				if (period.equals("CMLAST2MON") && bhobjs[k].getBill_Month().equals(String.valueOf(minmonth))) {
					m.put("CMLAST2MON" + "_" + bhobjs[k].getResult_no(), bhobjs[k]);
				}
			}
		}
		byte[] retary = new byte[0];
		for (int i = 0; i < crdTypeList.size(); i++) {
			try {
				// 取得 bill hunter 相關內容
				if (m.containsKey(period.trim() + "_" + crdTypeList.get(i))) {
					bhobj = (BHWebServiceObj) m.get(period.trim() + "_" + crdTypeList.get(i));

					billNo = bhobj.getBill_no();
					projectNo = bhobj.getProject_No();
					userID = bhobj.getUserid();
					queryDate = bhobj.getQuery_Date().substring(0, 10).replace("-", "/");
					resultNo = bhobj.getResult_no();
					billMonth = bhobj.getBill_Month();
					email = bhobj.getEmail();

					retary = commonPools.bh.getBHWSHtmlImg(billNo, custID, Integer.parseInt(projectNo),
							Integer.parseInt(userID), queryDate);
				}
			} catch (Exception e) {
				errcode = "Z601";
				throw TopMessageException.create("Z601");
			}
		}
		String retaryToBase64 = Base64.getEncoder().encodeToString(retary);
		((MVHImpl) mvh).getFlatValues().put("IMGB64",retaryToBase64);
		((MVHImpl) mvh).getFlatValues().put("TOPMSG", "");
		return mvh;
	}
}
