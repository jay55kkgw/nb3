package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.model.MVH;
import fstop.services.TopMsgDBDetecter;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class Ebpay1_1_Tel_Service extends Common_Tel_Service{
	@Autowired
	private TopMsgDBDetecter errDetect;
	
	@Override
	public HashMap pre_Processing(Map request) {
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) {
		return super.process(serviceID, request);
	}

	@Override
	public HashMap data_Retouch(MVH mvh) {
		return super.data_Retouch(mvh);
	}

	@Override
	public void setMsgCode(Map dataMap, Boolean data_Retouch_result) {
		super.setMsgCode(dataMap, data_Retouch_result);
	}
	
}
