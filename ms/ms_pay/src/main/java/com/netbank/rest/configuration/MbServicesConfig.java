package com.netbank.rest.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import fstop.service.mobile.impl.MB_N070A;
import fstop.service.mobile.impl.MB_N075;
import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class MbServicesConfig {
    @Bean
    protected MB_N075 mb_n075Service() throws Exception {
        return new MB_N075();
    }
    @Bean
    protected MB_N070A mb_n070aService() throws Exception {
        return new MB_N070A();
    }
}
