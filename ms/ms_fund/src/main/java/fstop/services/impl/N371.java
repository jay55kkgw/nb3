package fstop.services.impl;

import java.util.Date;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.rest.model.pool.FundUtils;
import com.netbank.util.ESAPIUtil;

import topibs.utility.NumericUtil;

import fstop.exception.ToRuntimeException;
import fstop.exception.TopMessageException;
import fstop.exception.UncheckedException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.orm.dao.TxnFundTraceLogDao;
import fstop.orm.po.TXNFUNDTRACELOG;
import fstop.services.BookingUtils;
import fstop.services.CommonService;
import fstop.services.NoticeInfo;
import fstop.services.NoticeInfoGetter;
import fstop.services.OnLineTransferNotice;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N371 extends CommonService {
	@Autowired
	@Qualifier("n371Telcomm")
	private TelCommExec n371Telcomm;
	@Autowired
	@Qualifier("c023Telcomm")
	private TelCommExec c023Telcomm;
	@Autowired
	private TxnFundTraceLogDao txnFundTraceLogDao;
	@Autowired
	private FundUtils fundUtils;

	/**
	 * 
	 * @param params
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public MVH doAction(Map _params) {
		Map<String, String> params = _params;
		if(null != params.get("CDNO") && !params.get("CDNO").equals("")) {
			params.put("CREDITNO", params.get("CDNO"));
		}

		Date d = new Date();
		params.put("DATE", DateTimeUtils.getCDateShort(d));
		params.put("TIME", DateTimeUtils.getTimeShort(d));

		TelcommResult helperN370 = null;
		TelcommResult helperC020 = null;
		Throwable exception = null;
		try {
			helperN370 = n371Telcomm.query(params);
			helperN370.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
			helperN370.getFlatValues().put("CDNO", helperN370.getFlatValues().get("CREDITNO"));

			// params.putAll(helperN370.getFlatValues());

			params.put("TXNIDT", DateTimeUtils.format("yyMMddHHmmss", new Date()));
			params.put("MAC", "");
			helperC020 = c023Telcomm.query(params);
			helperC020.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
			helperC020.getFlatValues().put("CDNO", helperC020.getFlatValues().get("CREDITNO"));
		} catch (UncheckedException e) {
			exception = e;
			throw e;
		} catch (Throwable e) {
			exception = e;
			throw new ToRuntimeException(e.getMessage(), e);
		} finally {
			boolean includeADTXNO = StrUtils.isNotEmpty(params.get("ADTXNO"));

			TXNFUNDTRACELOG logobj = new TXNFUNDTRACELOG();
			// Date d = new Date();
			if (includeADTXNO)
				logobj.setADTXNO(StrUtils.trim(params.get("ADTXNO")));
			else
				logobj.setADTXNO(UUID.randomUUID().toString());

			logobj.setFDUSERID(StrUtils.trim(params.get("UID")));
			logobj.setFDTXTIME(DateTimeUtils.format("yyyyMMddHHmmss", d));
			logobj.setFDAGREEFLAG(StrUtils.trim(params.get("FDAGREEFLAG")));
			logobj.setFDNOTICETYPE(StrUtils.trim(params.get("FDNOTICETYPE")));
			logobj.setFDPUBLICTYPE(StrUtils.trim(params.get("FDPUBLICTYPE")));
			logobj.setFDTXLOG(BookingUtils.requestInfo(params));
			if (exception != null) {
				logobj.setFDTXSTATUS("ZX99");
				if (exception instanceof TopMessageException) {
					logobj.setFDTXSTATUS(((TopMessageException) exception).getMsgcode());
				}
			}
			// 請吉米哥加
			// LoggerObject obj = LoggerHelper.current();
			// logobj.setADGUID(obj.getGUID());
			logobj.setADGUID(UUID.randomUUID().toString());
			txnFundTraceLogDao.save(logobj);

			if (helperC020 != null) {
				helperC020.getFlatValues().put("ADTXNO", logobj.getADTXNO());

				// 發送成功Email
				final String str_RESPCOD = helperC020.getValueByFieldName("RESPCOD"); // 回應訊息

				if (str_RESPCOD.equals("0000")) {
					final String str_TRANSCODE = helperC020.getValueByFieldName("TRANSCODE"); // 基金代碼
					MVHImpl fundData = (MVHImpl) fundUtils.getFundData(helperC020.getValueByFieldName("TRANSCODE"));
					final String str_TRANSCODE_NAME = fundData.getValueByFieldName("FUNDLNAME");
					final String str_INTRANSCODE = helperC020.getValueByFieldName("INTRANSCODE"); // 轉入基金
					fundData = (MVHImpl) fundUtils.getFundData(helperC020.getValueByFieldName("INTRANSCODE"));
					final String str_INTRANSCODE_NAME = fundData.getValueByFieldName("FUNDLNAME");
					final String str_FUNDAMT = NumericUtil.formatNumberString(helperC020.getValueByFieldName("FUNDAMT"),
							2); // 轉出信託金額
					final String str_AMT3 = NumericUtil.formatNumberString(helperC020.getValueByFieldName("AMT3"), 0); // 轉換手續費
					final String str_FUNCUR = helperC020.getValueByFieldName("CRY"); // 幣別
					final String str_UID = params.get("UID"); // ID
					final String str_DPMYEMAIL = params.get("DPMYEMAIL"); // DPMYEMAIL
					final String str_CMTRMAIL = params.get("CMTRMAIL"); // CMTRMAIL
					final String str_CMQTIME = helperC020.getValueByFieldName("CMQTIME"); // ID

					final String str_FCA2 = NumericUtil.formatNumberString(helperC020.getValueByFieldName("FCA2"), 2); // 手續費
					final String str_AMT5 = NumericUtil.formatNumberString(helperC020.getValueByFieldName("AMT5"), 2); // 扣款金額

					log.debug("str_RESPCOD = " + str_RESPCOD);
					log.debug("str_TRANSCODE = " + str_TRANSCODE);
					log.debug("str_TRANSCODE_NAME = " + str_TRANSCODE_NAME);
					log.debug("str_INTRANSCODE = " + str_INTRANSCODE);
					log.debug("str_INTRANSCODE_NAME = " + str_INTRANSCODE_NAME);
					log.debug("str_FUNDAMT = " + str_FUNDAMT);
					log.debug("str_AMT3 = " + str_AMT3);
					log.debug("str_FUNCUR = " + str_FUNCUR);
					log.debug(ESAPIUtil.vaildLog("str_UID = " + str_UID));
					log.debug(ESAPIUtil.vaildLog("str_DPMYEMAIL = " + str_DPMYEMAIL));
					log.debug(ESAPIUtil.vaildLog("str_CMTRMAIL = " + str_CMTRMAIL));
					log.debug("str_CMQTIME = " + str_CMQTIME);
					log.debug("str_FCA2 = " + str_FCA2);
					log.debug("str_AMT5 = " + str_AMT5);

					if (str_DPMYEMAIL != null && str_DPMYEMAIL.length() > 0) {
						OnLineTransferNotice trNotice = new OnLineTransferNotice();

						trNotice.sendNotice(new NoticeInfoGetter() {

							public NoticeInfo getNoticeInfo() {

								NoticeInfo result = new NoticeInfo(str_UID, str_DPMYEMAIL, str_CMTRMAIL);
								// 範本
								result.setTemplateName("FUNDNOTICE");
								result.getParams().put("#TITLE", "基金轉換通知");

								result.getParams().put("#TRANTIME", str_CMQTIME);

								result.getParams().put("#LINE_TITLE_1", "基金名稱");
								result.getParams().put("#LINE_CONTENT_1",
										"(" + str_TRANSCODE + ")" + str_TRANSCODE_NAME);

								result.getParams().put("#LINE_TITLE_2", "轉入基金");
								result.getParams().put("#LINE_CONTENT_2",
										"(" + str_INTRANSCODE + ")" + str_INTRANSCODE_NAME);

								result.getParams().put("#LINE_TITLE_3", "轉出信託金額");
								result.getParams().put("#LINE_CONTENT_3", str_FUNCUR + " " + str_FUNDAMT);

								result.getParams().put("#LINE_TITLE_4", "轉換手續費");
								result.getParams().put("#LINE_CONTENT_4", str_AMT3);

								result.getParams().put("#LINE_TITLE_5", "扣款金額");
								result.getParams().put("#LINE_CONTENT_5", str_AMT5);

								return result;
							}

						});
					} // 確認Email
				} // 0000
					// 發送成功Email

			} // != null
		}

		return helperC020;
	}

}
