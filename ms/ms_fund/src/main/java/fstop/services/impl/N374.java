package fstop.services.impl;

import java.util.Date;
import java.util.Hashtable;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import topibs.utility.NumericUtil;

import fstop.aop.advice.LoggerHelper;
import fstop.aop.advice.LoggerObject;
import fstop.exception.ToRuntimeException;
import fstop.exception.TopMessageException;
import fstop.exception.UncheckedException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;

import com.netbank.rest.model.FieldTranslator;
import com.netbank.rest.model.pool.FundUtils;
import com.netbank.util.ESAPIUtil;

import fstop.orm.dao.SysDailySeqDao;
import fstop.orm.dao.TxnFundTraceLogDao;
import fstop.orm.po.TXNFUNDTRACELOG;
import fstop.services.BookingUtils;
import fstop.services.CommonService;
import fstop.services.NoticeInfo;
import fstop.services.NoticeInfoGetter;
import fstop.services.OnLineTransferNotice;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
@Slf4j
public class N374 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("c033Telcomm")
	private TelCommExec c033Telcomm;
	@Autowired
	@Qualifier("n374Telcomm")
	private TelCommExec n374Telcomm;
	@Autowired
	private TxnFundTraceLogDao txnFundTraceLogDao;

	@Autowired
	private SysDailySeqDao sysDailySeqDao;
	@Autowired
	private FundUtils fundUtils;
	
//	@Required
//	public void setFundUtils(FundUtils fundUtils) {
//		this.fundUtils = fundUtils;
//	}
//
//	@Required
//	public void setSysDailySeqDao(SysDailySeqDao sysDailySeqDao) {
//		this.sysDailySeqDao = sysDailySeqDao;
//	}
//	
//	@Required
//	public void setC033Telcomm(TelCommExec telcomm) {
//		c033Telcomm = telcomm;
//	}
//
//	@Required
//	public void setN374Telcomm(TelCommExec telcomm) {
//		n374Telcomm = telcomm;
//	}
//
//	@Required
//	public void setTxnFundTraceLogDao(TxnFundTraceLogDao txnFundTraceLogDao) {
//		this.txnFundTraceLogDao = txnFundTraceLogDao;
//	}
//	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params) {
		Map<String, String> params = _params;
		if(null != params.get("CDNO") && !params.get("CDNO").equals("")) {
			params.put("CREDITNO", params.get("CDNO"));
		}

		params.put("TXNIDT", DateTimeUtils.format("yyMMddHHmmss", new Date()));
		params.put("SYSTRACE",
				StrUtils.right(StrUtils.repeat("0", 7) + sysDailySeqDao.dailySeq("C033")+"", 7));
		TelcommResult helper = null;
		TelcommResult helperN374 = null;
		Throwable exception = null;
		try {

			helper = c033Telcomm.query(params);
			helper.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
			
			String str_BANKID = (String)params.get("BANKID");
			
			params.putAll(helper.getFlatValues());
			params.put("BANKID", str_BANKID);
			
			Date d = new Date();
			params.put("DATE", DateTimeUtils.getCDateShort(d));
			params.put("TIME", DateTimeUtils.getTimeShort(d));
			
			helperN374 = n374Telcomm.query(params);
			
			helperN374.getFlatValues().put("CDNO", helperN374.getFlatValues().get("CREDITNO"));
			helper.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
			
			helper.getFlatValues().putAll(helperN374.getFlatValues());

		}
		catch(UncheckedException e) {
			exception = e;
			throw e;
		}
		catch(Throwable e) {
			exception = e;
			throw new ToRuntimeException(e.getMessage(), e);
		}
		finally {
			boolean includeADTXNO = StrUtils.isNotEmpty(params.get("ADTXNO"));
			
			TXNFUNDTRACELOG logobj = new TXNFUNDTRACELOG();
			Date d = new Date();
			if(includeADTXNO)
				logobj.setADTXNO(StrUtils.trim(params.get("ADTXNO")));
			else
				logobj.setADTXNO(UUID.randomUUID().toString());

			logobj.setFDUSERID(StrUtils.trim(params.get("UID")));
			logobj.setFDTXTIME(DateTimeUtils.format("yyyyMMddHHmmss", d));
			logobj.setFDAGREEFLAG(StrUtils.trim(params.get("FDAGREEFLAG")));
			logobj.setFDNOTICETYPE(StrUtils.trim(params.get("FDNOTICETYPE")));
			logobj.setFDPUBLICTYPE(StrUtils.trim(params.get("FDPUBLICTYPE")));
			logobj.setFDTXLOG(BookingUtils.requestInfo(params));
			if(exception != null) {
				logobj.setFDTXSTATUS("ZX99");
				if(exception instanceof TopMessageException) {
					logobj.setFDTXSTATUS(((TopMessageException)exception).getMsgcode());
				}
			}
			LoggerObject obj = LoggerHelper.current();
			logobj.setADGUID(obj.getGUID());
			txnFundTraceLogDao.save(logobj);
			
			if(helper != null){
				helper.getFlatValues().put("ADTXNO", logobj.getADTXNO());
			
				//發送成功Email 
				final String str_MSGCOD = helper.getValueByFieldName("MSGCOD");	//回應訊息
				
				log.debug("N374 str_MSGCOD = " + str_MSGCOD);
				
				if (str_MSGCOD.equals("0000")){
					final String str_TRANSCODE = helper.getValueByFieldName("TRANSCODE");	//基金代碼
					MVHImpl fundData = (MVHImpl)fundUtils.getFundData(helper.getValueByFieldName("TRANSCODE"));
					final String str_FUNDLNAME = fundData.getValueByFieldName("FUNDLNAME");//基金名稱
					final String str_FUNDAMT = NumericUtil.formatNumberString(helper.getValueByFieldName("FUNDAMT"), 2);	//申購金額
					final String str_FUNDCUR = helper.getValueByFieldName("FUNDCUR");	//幣別
					final String str_UID = params.get("UID");	//ID
					final String str_DPMYEMAIL = params.get("DPMYEMAIL");	//DPMYEMAIL
					final String str_CMTRMAIL = params.get("CMTRMAIL");	//CMTRMAIL
					final String str_CMQTIME = helper.getValueByFieldName("CMQTIME");	//ID
					String str_Bill = params.get("BILLSENDMODE");	//ID
					final String str_BillType = FieldTranslator.transfer("BILLSENDMODE",str_Bill);
					
					final String str_UNIT = NumericUtil.formatNumberString(helper.getValueByFieldName("UNIT"),4);
					log.debug("str_MSGCOD = " + str_MSGCOD);
					log.debug("str_TRANSCODE = " + str_TRANSCODE);
					log.debug("str_FUNDAMT = " + str_FUNDAMT);
					log.debug("str_FUNDLNAME = " + str_FUNDLNAME);
					log.debug("str_FUNDCUR = " + str_FUNDCUR);
					log.debug(ESAPIUtil.vaildLog("str_UID = " + str_UID));
					log.debug(ESAPIUtil.vaildLog("str_DPMYEMAIL = " + str_DPMYEMAIL));
					log.debug(ESAPIUtil.vaildLog("str_CMTRMAIL = " + str_CMTRMAIL));
					log.debug("str_CMQTIME = " + str_CMQTIME);
					log.debug(ESAPIUtil.vaildLog("str_BillType = " + str_BillType));
					log.debug("str_UNIT = " + str_UNIT);

				
				if (str_DPMYEMAIL!= null && str_DPMYEMAIL.length()>0){
				OnLineTransferNotice trNotice = new OnLineTransferNotice();
				trNotice.sendNotice(new NoticeInfoGetter() {
					public NoticeInfo getNoticeInfo() {
					
						NoticeInfo result = new NoticeInfo(str_UID,str_DPMYEMAIL,str_CMTRMAIL);
						//範本			
						result.setTemplateName("FUNDNOTICE");
					    result.getParams().put("#TITLE", "基金預約贖回通知");					

						result.getParams().put("#TRANTIME", str_CMQTIME);
			
					    result.getParams().put("#TRANTIME", str_CMQTIME);
						
						result.getParams().put("#LINE_TITLE_1", "基金名稱");
						result.getParams().put("#LINE_CONTENT_1", "(" + str_TRANSCODE + ")" +str_FUNDLNAME);

						result.getParams().put("#LINE_TITLE_2", "贖回信託金額");
						result.getParams().put("#LINE_CONTENT_2", str_FUNDCUR + " " + str_FUNDAMT);
						
						result.getParams().put("#LINE_TITLE_3", "贖回方式");
						result.getParams().put("#LINE_CONTENT_3", str_BillType);
							
						result.getParams().put("#LINE_TITLE_4", "贖回單位數");
						result.getParams().put("#LINE_CONTENT_4", str_UNIT);
						
						return result;
					}
					
				});
				}//確認Email
				}//0000
				//發送成功Email
			}//!= null
		}
		
		return helper;
	}


}
