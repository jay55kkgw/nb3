package fstop.services.impl;

import java.util.Date;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import topibs.utility.NumericUtil;

import fstop.aop.advice.LoggerHelper;
import fstop.aop.advice.LoggerObject;
import fstop.exception.ToRuntimeException;
import fstop.exception.TopMessageException;
import fstop.exception.UncheckedException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.orm.dao.TxnFundTraceLogDao;
import fstop.orm.po.TXNFUNDTRACELOG;
import fstop.services.BookingUtils;
import fstop.services.CommonService;
import fstop.services.NoticeInfo;
import fstop.services.NoticeInfoGetter;
import fstop.services.OnLineTransferNotice;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

import com.netbank.rest.model.pool.FundUtils;
import com.netbank.util.ESAPIUtil;

@Slf4j
public class N370 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("n370Telcomm")
	private TelCommExec n370Telcomm;
	@Autowired
    @Qualifier("c020Telcomm")
	private TelCommExec c020Telcomm;
	@Autowired
	private TxnFundTraceLogDao txnFundTraceLogDao;
	@Autowired
	private FundUtils fundUtils;
	
//	@Required
//	public void setN370Telcomm(TelCommExec telcomm) {
//		n370Telcomm = telcomm;
//	}
//
//	@Required
//	public void setC020Telcomm(TelCommExec telcomm) {
//		c020Telcomm = telcomm;
//	}
//
//	@Required
//	public void setTxnFundTraceLogDao(TxnFundTraceLogDao txnFundTraceLogDao) {
//		this.txnFundTraceLogDao = txnFundTraceLogDao;
//	}
//	
//	@Required
//	public void setFundUtils(FundUtils fundUtils) {
//		this.fundUtils = fundUtils;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params) { 
		Map<String, String> params = _params;
		log.debug(ESAPIUtil.vaildLog("@@@ FCA2 == "+params.get("FCA2")));

		
		Date d = new Date();
		params.put("DATE", DateTimeUtils.getCDateShort(d));
		params.put("TIME", DateTimeUtils.getTimeShort(d));
		//params.put("INTSACN", StrUtils.right("0000000000000000" + params.get("INTSACN"), 16));
		TelcommResult helperN370 = null;
		TelcommResult helperC020 = null;
		Throwable exception = null;
		try {
			helperN370 = n370Telcomm.query(params);
			helperN370.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
			
			params.putAll(helperN370.getFlatValues());
			
			
			params.put("TXNIDT", DateTimeUtils.format("yyMMddHHmmss", new Date()));
			params.put("MAC", "");
			helperC020 = c020Telcomm.query(params);
			helperC020.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
			helperC020.getFlatValues().put("PRIZETIMES", helperN370.getValueByFieldName("PRIZETIMES"));
		}
		catch(UncheckedException e) {
			exception = e;
			throw e;
		}
		catch(Throwable e) {
			exception = e;
			throw new ToRuntimeException(e.getMessage(), e);
		}
		finally {
			boolean includeADTXNO = StrUtils.isNotEmpty(params.get("ADTXNO"));
			
			TXNFUNDTRACELOG logobj = new TXNFUNDTRACELOG();
			//Date d = new Date();
			if(includeADTXNO)
				logobj.setADTXNO(StrUtils.trim(params.get("ADTXNO")));
			else
				logobj.setADTXNO(UUID.randomUUID().toString());

			logobj.setFDUSERID(StrUtils.trim(params.get("UID")));
			logobj.setFDTXTIME(DateTimeUtils.format("yyyyMMddHHmmss", d));
			logobj.setFDAGREEFLAG(StrUtils.trim(params.get("FDAGREEFLAG")));
			logobj.setFDNOTICETYPE(StrUtils.trim(params.get("FDNOTICETYPE")));
			logobj.setFDPUBLICTYPE(StrUtils.trim(params.get("FDPUBLICTYPE")));
			logobj.setFDTXLOG(BookingUtils.requestInfo(params));
			if(exception != null) {
				logobj.setFDTXSTATUS("ZX99");
				if(exception instanceof TopMessageException) {
					logobj.setFDTXSTATUS(((TopMessageException)exception).getMsgcode());
				}
			}
			LoggerObject obj = LoggerHelper.current();
			logobj.setADGUID(obj.getGUID());
			txnFundTraceLogDao.save(logobj);
			
			if(helperC020 != null){
				helperC020.getFlatValues().put("ADTXNO", logobj.getADTXNO());
		        
				
				
				//發送成功Email 
				final String str_RESPCOD = helperC020.getValueByFieldName("RESPCOD");	//回應訊息
				
				if (str_RESPCOD.equals("0000")){
				final String str_TRANSCODE = helperC020.getValueByFieldName("TRANSCODE");	//基金代碼
				MVHImpl fundData = (MVHImpl)fundUtils.getFundData(helperC020.getValueByFieldName("TRANSCODE"));
				final String str_FUNDLNAME = fundData.getValueByFieldName("FUNDLNAME");//基金名稱
				final String str_AMT3 = NumericUtil.formatNumberString(helperC020.getValueByFieldName("AMT3"),0);	//申購金額
				final String str_CUR = helperC020.getValueByFieldName("CUR");	//幣別
				final String str_UID = params.get("UID");	//ID
				final String str_DPMYEMAIL = params.get("DPMYEMAIL");	//DPMYEMAIL
				final String str_CMTRMAIL = params.get("CMTRMAIL");	//CMTRMAIL
				final String str_CMQTIME = helperC020.getValueByFieldName("CMQTIME");	//ID

				final String str_FCA2 = NumericUtil.formatNumberString(helperC020.getValueByFieldName("FCA2"), 2);	//手續費
				final String str_AMT5 = NumericUtil.formatNumberString(helperC020.getValueByFieldName("AMT5"), 2);	//扣款金額
				
				//後收的值
				final String str_FCA2_A = "0.00"; //後收手續費
				
				// helperC020.getValueByFieldName("AMT3") 格式 00100000 >> 要轉成 100,000.00
				final String str_AMT5_A =  NumericUtil.formatNumberString( String.valueOf( Double.parseDouble (helperC020.getValueByFieldName("AMT3") ) ) ,2);	//後收扣款金額
				
				log.debug("FEE_TYPE = "+ params.get("FEE_TYPE"));
				log.debug("str_RESPCOD = " + str_RESPCOD);
				log.debug("str_TRANSCODE = " + str_TRANSCODE);
				log.debug("str_AMT3 = " + str_AMT3); //1,000,000
				log.debug("str_FUNDLNAME = " + str_FUNDLNAME);
				log.debug("str_CUR = " + str_CUR);
				log.debug(ESAPIUtil.vaildLog("str_UID = " + str_UID));
				log.debug(ESAPIUtil.vaildLog("str_DPMYEMAIL = " + str_DPMYEMAIL));
				log.debug(ESAPIUtil.vaildLog("str_CMTRMAIL = " + str_CMTRMAIL));
				log.debug("str_CMQTIME = " + str_CMQTIME);
				log.debug("str_FCA2 = " + str_FCA2);
				log.debug("str_AMT5 = " + str_AMT5);
				
				log.debug("str_FCA2_A = " + str_FCA2_A);
				log.debug("str_AMT5_A = " + str_AMT5_A);
				
				if (str_DPMYEMAIL!= null && str_DPMYEMAIL.length()>0){
				OnLineTransferNotice trNotice = new OnLineTransferNotice();

				trNotice.sendNotice(new NoticeInfoGetter() {

					public NoticeInfo getNoticeInfo() {
					
						NoticeInfo result = new NoticeInfo(str_UID,str_DPMYEMAIL,str_CMTRMAIL);
						//範本			
						result.setTemplateName("FUNDNOTICE");
					    result.getParams().put("#TITLE", "基金申購通知");					

									
					    result.getParams().put("#TRANTIME", str_CMQTIME);
			
						result.getParams().put("#LINE_TITLE_1", "基金名稱");
						result.getParams().put("#LINE_CONTENT_1", "(" + str_TRANSCODE + ")" +str_FUNDLNAME);

						result.getParams().put("#LINE_TITLE_2", "申購金額");
						result.getParams().put("#LINE_CONTENT_2", str_CUR + " " + str_AMT3);
						
						result.getParams().put("#LINE_TITLE_3", "手續費");
						result.getParams().put("#LINE_CONTENT_3", str_FCA2);
							
						result.getParams().put("#LINE_TITLE_4", "扣款金額");
						result.getParams().put("#LINE_CONTENT_4", str_AMT5);
						
						//後收 修改 手續費 及 扣款金額 值
						if("A".equals(params.get("FEE_TYPE")) || "A".equals(params.get("FEETYPE"))) {
							result.getParams().put("#LINE_TITLE_3", "手續費");
							result.getParams().put("#LINE_CONTENT_3", str_FCA2_A);
								
							result.getParams().put("#LINE_TITLE_4", "扣款金額");
							result.getParams().put("#LINE_CONTENT_4", str_AMT5_A);
						}
						
						result.getParams().put("#LINE_TITLE_5", "幣別");
						result.getParams().put("#LINE_CONTENT_5", str_CUR);
						
												
						return result;
					}
					
				});
				}//確認Email
				}//0000 
				//發送成功Email 
				
			} //!= null
		
		}

		return helperC020;
	}
}
