package fstop.services.impl;

import java.util.Date;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.aop.advice.LoggerHelper;
import fstop.aop.advice.LoggerObject;
import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.orm.dao.TxnFundTraceLogDao;
import fstop.orm.po.TXNFUNDTRACELOG;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class C017_Public extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("c016Telcomm")
	private TelCommExec c016Telcomm;
	@Autowired
	@Qualifier("c025Telcomm")
	private TelCommExec c025Telcomm;
	@Autowired
	private TxnFundTraceLogDao txnFundTraceLogDao;
	
//	@Required
//	public void setC016Telcomm(TelCommExec telcomm) {
//		c016Telcomm = telcomm;
//	}
//
//	@Required
//	public void setC025Telcomm(TelCommExec telcomm) {
//		c025Telcomm = telcomm;
//	}
//
//	@Required
//	public void setTxnFundTraceLogDao(TxnFundTraceLogDao txnFundTraceLogDao) {
//		this.txnFundTraceLogDao = txnFundTraceLogDao;
//	}

	
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params) {
		Map<String, String> params = _params;
		params.put("TXNIDT", DateTimeUtils.format("yyMMddHHmmss", new Date()));

		params.put("STOP", StrUtils.right("000" + params.get("STOP"), 3));
		params.put("YIELD", StrUtils.right("000" + params.get("YIELD"), 3));

		//TODO Write TraceLog
		
		TelcommResult helper = null;
		try {
			if(!"3".equals(params.get("PAYTYPE"))) { // PAYTYPE <> 3
				helper = c016Telcomm.query(params);
			}
			else { 
				helper = c025Telcomm.query(params);
			}
	
			helper.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		}
		finally {
			boolean includeADTXNO = StrUtils.isNotEmpty(params.get("ADTXNO"));
			
			TXNFUNDTRACELOG logobj = new TXNFUNDTRACELOG();

			Date d = new Date();
			if(includeADTXNO)
				logobj.setADTXNO(StrUtils.trim(params.get("ADTXNO")));
			else
				logobj.setADTXNO(UUID.randomUUID().toString());

			logobj.setFDUSERID(StrUtils.trim(params.get("UID")));
			logobj.setFDTXTIME(DateTimeUtils.format("yyyyMMddHHmmss", d));
			logobj.setFDAGREEFLAG(StrUtils.trim(params.get("FDAGREEFLAG")));
			logobj.setFDNOTICETYPE(StrUtils.trim(params.get("FDNOTICETYPE")));
			logobj.setFDPUBLICTYPE(StrUtils.trim(params.get("FDPUBLICTYPE")));
			LoggerObject obj = LoggerHelper.current();
			logobj.setADGUID(obj.getGUID());
			txnFundTraceLogDao.save(logobj);
		
			if(helper != null)
			{
				helper.getFlatValues().put("ADTXNO", logobj.getADTXNO());
				helper.getFlatValues().put("XFLAG", params.get("FDPUBLICTYPE"));
			}
		} 
		
		
		return helper;
	}


}
