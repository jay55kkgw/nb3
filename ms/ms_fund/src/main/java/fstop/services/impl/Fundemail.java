package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.util.ESAPIUtil;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.notifier.NotifyAngent;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.TXNUSER;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Fundemail extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	private TxnUserDao txnUserDao;
	@Autowired
	@Qualifier("n930Telcomm")
	private TelCommExec n930Telcomm; //用戶電子郵箱位址變更
	
//	@Required
//	public void setTxnUserDao(TxnUserDao dao) {
//		this.txnUserDao = dao;
//	}
//	@Required
//	public void setN930Telcomm(TelCommExec telCommExec) {
//		this.n930Telcomm = telCommExec;
//	}
	
	
	@Override
	public MVH doAction(Map _params) {
		
		Map<String, String> params = _params;
				MVHImpl result = new MVHImpl();
		
		//改自N930EE	
        //if("UPDATE".equalsIgnoreCase(params.get("EXECUTEFUNCTION"))) {
			
			log.debug(ESAPIUtil.vaildLog("MM FundEmail. NEW_EMAIL=="+(String)params.get("NEW_EMAIL")));
			log.debug(ESAPIUtil.vaildLog("MM FundEmail. DPUSERID=="+(String)params.get("DPUSERID")));

				
			params.put("E_MAIL_ADR", (String)params.get("NEW_EMAIL"));
			params.put("FLAG", "02");  //更新需用02打中心更新
			MVHImpl res = (MVHImpl)n930Telcomm.query(params);//先發N930更新用戶電子郵箱，如有錯誤會導到topException頁面去
			
			String dpuserid = params.get("DPUSERID");
			TXNUSER txnUser = txnUserDao.findById(dpuserid);
			txnUser.setDPMYEMAIL(params.get("NEW_EMAIL"));
			
			//設定完Email變更後，判斷，如果該使用者在txnuser裡的通知欄位getDPNOTIFY是空的，應主動新增一筆通知內容
			log.debug(ESAPIUtil.vaildLog("FundEmail. NEW_EMAIL==@"+(String)params.get("NEW_EMAIL")+"@"));
			String notify = txnUser.getDPNOTIFY();
			log.debug(ESAPIUtil.vaildLog("FundEmail. notify==@"+notify+"@"));
			if(!StrUtils.isEmpty((String)params.get("NEW_EMAIL")))
			{//新的Email不是空的才要去增加DPNOTIFY的值				
				log.debug("FundEmail. insert");
				if(StrUtils.isEmpty(notify.trim()))
					txnUser.setDPNOTIFY("1,2,3,4,5,6,7,8,9,10,11,12,13,14,15");//主動新增一筆通知內容為全部打勾(二階之後要再增加二階才有的通知內容)
			}else
			{//新的EMail是空的，就把通知訊息內容全部清空
				if(!StrUtils.isEmpty(notify.trim()))
					txnUser.setDPNOTIFY("");
				//MVHImpl n931res = (MVHImpl)n931Telcomm.query(params);//取消電子交易對帳單
			}
			
			Date d = new Date();
			String dateTime = DateTimeUtils.getCDateShort(d) + DateTimeUtils.getTimeShort(d);
			//設定完Email變更後，如果EMail是空的，電子對帳單裡的通知如果是Y要改成取消("N")，如果是""就先不用改
			String dpbill     = txnUser.getDPBILL();    //電子對帳單Y.申請	N.沒申請
			String dpfundbill = txnUser.getDPFUNDBILL();//基金對帳單Y.申請	N.沒申請
			String dpcardbill = txnUser.getDPCARDBILL();//信用卡帳單Y.申請	N.沒申請
			log.debug(ESAPIUtil.vaildLog("dpbill==@"+dpbill+"@"));
			if(StrUtils.isEmpty((String)params.get("NEW_EMAIL")))
			{//Email是空的才要去改DPBILL的值是Y的為N				
				if(dpbill.trim().equals("Y"))
				{
					txnUser.setDPBILL("N");//由Y->N
					txnUser.setBILLDATE(dateTime);//最後電子對帳單狀態異動日
				}
				if(dpfundbill.trim().equals("Y"))
				{
					txnUser.setDPFUNDBILL("N");//由Y->N
					txnUser.setFUNDBILLDATE(dateTime);//最後基金對帳單狀態異動日
				}
				if(dpcardbill.trim().equals("Y"))
				{
					txnUser.setDPCARDBILL("N");//由Y->N
					txnUser.setCARDBILLDATE(dateTime);//最後信用卡帳單狀態異動日
				}
			}
			
			
			txnUser.setEMAILDATE(dateTime);
			dateTime = dateTime.substring(0,3)+"/"+dateTime.substring(3, 5)+"/"+dateTime.substring(5, 7)+"  "+dateTime.substring(7, 9)+":"+dateTime.substring(9, 11)+":"+dateTime.substring(11, dateTime.length());
			txnUserDao.save(txnUser);
			params.put("TRANTIME", dateTime);
			if(StrUtils.isNotEmpty(txnUser.getDPMYEMAIL())) {
				try{
					boolean isSendSuccess = NotifyAngent.sendNotice("N930EE", params, txnUser.getDPMYEMAIL());
					
					if(!isSendSuccess){
						result.getFlatValues().put("__MAILERR", "發送 Email 失敗.");
					}
				}
				catch(Throwable t){
//					t.printStackTrace();
				}
			}
			
			result.getFlatValues().put("DPMYEMAIL", txnUser.getDPMYEMAIL());
			
			result.getFlatValues().put("TOPMSG",res.getValueByFieldName("TOPMSG"));
		//}
		return result;
	}
}
