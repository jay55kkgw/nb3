package fstop.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

import com.netbank.rest.model.CommonPools;
import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.orm.dao.TxnCusInvAttrHistDao;
import fstop.orm.dao.TxnCusInvAttribDao;
import fstop.orm.dao.TxnFundTraceLogDao;
import fstop.orm.po.TXNCUSINVATTRHIST;
import fstop.orm.po.TXNCUSINVATTRIB;
import fstop.orm.po.TXNLOG;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.telcomm.cmd.CommandUtils;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class InvAttrib_S extends CommonService {
	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("n322Telcomm")
	private TelCommExec n322Telcomm;
	@Autowired
	private TxnCusInvAttribDao txnCusInvAttribDao;
	@Autowired
	private TxnFundTraceLogDao txnFundTraceLogDao;
	@Autowired
	private TxnCusInvAttrHistDao txnCusInvAttrHistDao;
	@Autowired
	private CommonPools commonPools;

	@Value("${answerHax:N}")
	String answerHax;
	
	@Value("${kyc_verion_natural_person:11012}")
	String kyc_verion_n;
	
	@Value("${kyc_verion_juristic_person:11012}")
	String kyc_verion_j;

	/**
	 * 
	 * @param params
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public MVH doAction(Map _params) {
		MVH rtnMVH = null;
		Map<String, String> params = _params;
		log.debug(ESAPIUtil.vaildLog("InvAttrib_S_doAction_params >> " + _params));
		if ("Y".equals((String) params.get("AGREE"))) {
			rtnMVH = doAction_Y(params);
		} else if ("N".equals((String) params.get("AGREE"))){
			rtnMVH = doAction_N(params);
		} else {
			rtnMVH = doAction_SCORE(params);
		}

		return rtnMVH;

	}

	private MVH doAction_Y(Map params) {
		log.info("InvAttrib_S_doAction_Y");
		params.put("CUSIDN", params.get("UID"));

		String UID = (String) params.get("UID");
		if ("0".equals(params.get("FGTXWAY")))
			params.put("KIND", "4");
		else
			params.put("KIND", (String) params.get("FGTXWAY"));
		if (params.containsKey("pkcs7Sign") && "1".equals(params.get("FGTXWAY"))) {
			List<String> commands = new ArrayList();
			commands.add("XMLCA()");
			commands.add("XMLCN()");

			CommandUtils.doBefore(commands, params);

			if (!StrUtils.trim((String) params.get("XMLCN")).toUpperCase()
					.startsWith(StrUtils.trim((String) params.get("UID")).toUpperCase())) {
				throw TopMessageException.create("Z089");
			}
		}
		// if(UID.length()==10) {
		/*** 驗證交易密碼 - 開始 ***/
		if (params.get("FGTXWAY") == null) {
			throw TopMessageException.create("S302", (String) params.get("ADOPID"));
		} else if ("0".equals(params.get("FGTXWAY"))) { // 使用交易密碼
			String TransPassUpdate = (String) params.get("TRANSPASSUPDATE");
			String Phase2 = (String) params.get("PHASE2");
			TelCommExec n950CMTelcomm;

			n950CMTelcomm = (TelCommExec) SpringBeanFactory.getBean("n951CMTelcomm");
			MVHImpl n950Result = n950CMTelcomm.query(params);

			// 若驗證失敗, 則會發動 TopMessageException
		}
		/*** 驗證交易密碼 - 結束 ***/

		/*** 修改記錄答案電文20180912 -開始 ***/
		String nAnswer[][] = new String[15][3];
		for (int i = 1; i <= nAnswer.length; i++) {
			int sc = 0;
			int sc2 = 0;

			if ((UID.length() != 10) && i == 15) {// 法人(14題)與自然人(15題)KYC問卷題目數量不同，需先判斷客戶身分
				nAnswer[i - 1][1] = "0";
				nAnswer[i - 1][2] = "0";
			} else {
				nAnswer[i - 1][0] = (String) params.get("FDQ" + i);

				for (int j = 1; j <= nAnswer[i - 1][0].length(); j++) {

					if (nAnswer[i - 1][0].substring(j - 1, j).equals("A")) {
						sc += 8;
					} else if (nAnswer[i - 1][0].substring(j - 1, j).equals("B")) {
						sc += 4;
					} else if (nAnswer[i - 1][0].substring(j - 1, j).equals("C")) {
						sc += 2;
					} else if (nAnswer[i - 1][0].substring(j - 1, j).equals("D")) {
						sc += 1;
					}
					if (nAnswer[i - 1][0].substring(j - 1, j).equals("E")) {
						sc2 += 8;
					} else if (nAnswer[i - 1][0].substring(j - 1, j).equals("F")) {
						sc2 += 4;
					} else if (nAnswer[i - 1][0].substring(j - 1, j).equals("G")) {
						sc2 += 2;
					} else if (nAnswer[i - 1][0].substring(j - 1, j).equals("H")) {
						sc2 += 1;
					}
				}
				nAnswer[i - 1][1] = String.valueOf(sc);
				nAnswer[i - 1][2] = String.valueOf(sc2);
			}
			log.warn("InvAttrib_S ----------> " + i + " " + nAnswer[i - 1][0] + ",sc: " + sc);
			log.warn("InvAttrib_S ----------> " + i + " " + nAnswer[i - 1][1] + ",sc2: " + sc2);
		}

		String[] HEX = new String[15];
		String allanswer = "";

		for (int k = 0; k < HEX.length; k++) {
			HEX[k] = Integer.toHexString(Integer.parseInt(nAnswer[k][1])).toUpperCase()
					+ Integer.toHexString(Integer.parseInt(nAnswer[k][2])).toUpperCase();
			log.warn("allanswer ----------> " + k + ":" + HEX[k]);
			allanswer += HEX[k];
		}

		log.debug("allanswer ----------> " + allanswer);

		/*** 修改記錄答案電文20180918 -結束 ***/

		params.put("DATA", params.get("FDINVTYPE"));

		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);
		params.put("UPD_FLG", params.get("UPD_FLG"));
		params.put("DEGREE", params.get("DEGREE"));
		params.put("CAREER", params.get("CAREER"));
		params.put("SALARY", params.get("SALARY"));
		
		//將KYC版本號變成環境變數 , 寫在fund或common內
		params.put("EDITON", (UID.length() == 10)?kyc_verion_n:kyc_verion_j);

		if (answerHax.equals("Y")) {
			params.put("ANSWER", allanswer); // 修改答案傳遞電文
		} else {
			params.put("ANSWER", params.get("ANSWER"));// 變更問卷答案傳遞電文內容 // 先不做hex-20200527,by hugo
		}

		params.put("MARK1", params.get("FDMARK1"));
		params.put("KIND", params.get("KIND")); // 交易機制
		params.put("BNKRA", "050"); // 行庫別
		if ("2".equals(params.get("FGTXWAY"))) {
			params.put("ACNNO", params.get("ACNNO")); // 晶片主帳號
			params.put("ICSEQ", params.get("ICSEQ")); // 晶片卡交易序號
		}else if("7".equals(params.get("FGTXWAY"))) {
			params.put("CERTACN", StrUtils.repeat("C", 19));
		}
		params.put("IP", params.get("ADUSERIP"));
		params.put("PCMAC", params.get("PCMAC"));
		params.put("DEVNAME", params.get("DEVNAME"));
		
		params.put("SCORE", params.get("FDSCORE"));

		
		TelcommResult n922 = null;
		logger.debug(ESAPIUtil.vaildLog("InvAttrib_S TXID:" + params.get("RTC")));
		if (((String) params.get("RTC")).substring(0, 1).equals("C")) {
			commonPools.n922.removeGetDataCache((String) params.get("UID"));
			n922 = (TelcommResult) commonPools.n922.getData((String) params.get("UID"));
		} else {
			commonPools.n927.removeGetDataCache((String) params.get("UID"));
			n922 = (TelcommResult) commonPools.n927.getData((String) params.get("UID"));
		}
		TXNCUSINVATTRIB attrib = new TXNCUSINVATTRIB();
		attrib.setFDUSERID((String) params.get("UID"));
		attrib.setFDQ1((String) params.get("FDQ1"));
		attrib.setFDQ2((String) params.get("FDQ2"));
		attrib.setFDQ3((String) params.get("FDQ3"));
		attrib.setFDQ4((String) params.get("FDQ4"));
		attrib.setFDQ5((String) params.get("FDQ5"));
		attrib.setFDSCORE((String) params.get("FDSCORE"));
		attrib.setFDINVTYPE((String) params.get("FDINVTYPE"));
		attrib.setLASTUSER(n922.getValueByFieldName("APLBRH")); // 簽約行
		attrib.setLASTDATE(DateTimeUtils.format("yyyyMMdd", new Date()));
		attrib.setLASTTIME(DateTimeUtils.format("HHmmss", new Date()));
		attrib.setFDQ6((String) params.get("FDQ6"));
		attrib.setFDQ7((String) params.get("FDQ7"));
		attrib.setFDQ8((String) params.get("FDQ8"));
		attrib.setFDQ9((String) params.get("FDQ9"));
		attrib.setFDQ10((String) params.get("FDQ10"));
		attrib.setFDQ11((String) params.get("FDQ11"));
		attrib.setFDQ12((String) params.get("FDQ12"));
		attrib.setFDQ13((String) params.get("FDQ13"));
		attrib.setFDQ14((String) params.get("FDQ14"));
		if (((String) params.get("UID")).length() == 10) {
			attrib.setFDQ15((String) params.get("FDQ15"));
			attrib.setMARK1((String) params.get("FDMARK1"));
		} else {
			attrib.setFDQ15("");
			attrib.setMARK1("");
		}
		attrib.setADUSERIP((String) params.get("ADUSERIP"));
		
		
		try {
			txnCusInvAttribDao.saveOrUpdate(attrib);
			
			TXNCUSINVATTRHIST updatePo = txnCusInvAttrHistDao.findById(Long.parseLong((String) params.get("FDHISTID")));
			updatePo.setAGREE("Y");
			updatePo.setLASTUSER(n922.getValueByFieldName("APLBRH")); // 簽約行
			
			txnCusInvAttrHistDao.update(updatePo);
			logger.debug("InvAttrib_S AttrHist Hist update");
			
			//20210325  修改流程  先寫完DB再打N322  , 
			//因為 有客戶會短時間連送兩筆 , 故資料庫 有UK , ID+LASTTIME
			//如果第一行txnCusInvAttribDao.save(attrib) 失敗了 代表已經有寫過一筆非常短間隔的 , 就進catch
			// 進catch後就不會打兩次 N322
			TelcommResult telcommResult = n322Telcomm.query(params);
			telcommResult.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
			n922.getFlatValues().putAll(telcommResult.getFlatValues());

		}catch(TopMessageException topex) {
			log.error(" n322Tel TopMessageException !!!!! >> {} " ,  topex.getMsgcode());
			throw topex ;
		}catch (Exception e) {
			log.error("doAction_Y Exception !!!!! >> {} " , e.getMessage());
		}
		
		n922.getFlatValues().put("RISK", (String) params.get("FDINVTYPE"));
		return n922;
	}

	private MVH doAction_N(Map params) {
		log.info("InvAttrib_S_doAction_N");
		TelcommResult n922 = null;
		if (((String) params.get("RTC")).substring(0, 1).equals("C")) {
			commonPools.n922.removeGetDataCache((String) params.get("UID"));
			n922 = (TelcommResult) commonPools.n922.getData((String) params.get("UID"));
		} else {
			commonPools.n927.removeGetDataCache((String) params.get("UID"));
			n922 = (TelcommResult) commonPools.n927.getData((String) params.get("UID"));
		}
		TXNCUSINVATTRHIST updatePo = txnCusInvAttrHistDao.findById(Long.parseLong((String) params.get("FDHISTID")));
		updatePo.setAGREE("N");
		txnCusInvAttrHistDao.update(updatePo);
		// TXNCUSINVATTRHIST attrhist = new TXNCUSINVATTRHIST();
		// attrhist.setFDUSERID((String) params.get("UID"));
		// attrhist.setFDQ1((String) params.get("FDQ1"));
		// attrhist.setFDQ2((String) params.get("FDQ2"));
		// attrhist.setFDQ3((String) params.get("FDQ3"));
		// attrhist.setFDQ4((String) params.get("FDQ4"));
		// attrhist.setFDQ5((String) params.get("FDQ5"));
		// attrhist.setFDSCORE((String) params.get("FDSCORE"));
		// attrhist.setFDINVTYPE((String) params.get("FDINVTYPE"));
		// attrhist.setLASTUSER(n922.getValueByFieldName("APLBRH")); // 簽約行
		// attrhist.setLASTDATE(DateTimeUtils.format("yyyyMMdd", new Date()));
		// attrhist.setLASTTIME(DateTimeUtils.format("HHmmss", new Date()));
		// attrhist.setFDQ6((String) params.get("FDQ6"));
		// attrhist.setFDQ7((String) params.get("FDQ7"));
		// attrhist.setFDQ8((String) params.get("FDQ8"));
		// attrhist.setFDQ9((String) params.get("FDQ9"));
		// attrhist.setFDQ10((String) params.get("FDQ10"));
		// attrhist.setFDQ11((String) params.get("FDQ11"));
		// attrhist.setFDQ12((String) params.get("FDQ12"));
		// attrhist.setFDQ13((String) params.get("FDQ13"));
		// attrhist.setFDQ14((String) params.get("FDQ14"));
		// attrhist.setADUSERIP((String) params.get("ADUSERIP"));
		// attrhist.setAGREE((String) params.get("AGREE"));
		// if (((String) params.get("UID")).length() == 10) {
		// attrhist.setFDQ15((String) params.get("FDQ15"));
		// attrhist.setMARK1((String) params.get("FDMARK1"));
		// } else {
		// attrhist.setFDQ15("");
		// attrhist.setMARK1("");
		// }
		// txnCusInvAttrHistDao.save(attrhist);
		logger.debug("InvAttrib_S AttrHist Hist update");

		String ADCONTENT = JSONUtils.map2json(params);
		// TXNLOG txnlog = new TXNLOG();
		// // 寫入txnlog
		// TxnLogDao txnLogDao = (TxnLogDao) SpringBeanFactory.getBean("txnLogDao");
		// txnlog.setADTXNO(UUID.randomUUID().toString());
		// txnlog.setADEXCODE("");
		// txnlog.setADUSERID((String) params.get("UID"));
		// txnlog.setADOPID("InvestAttr");
		// txnlog.setFGTXWAY((String) params.get("FGTXWAY"));
		// txnlog.setADUSERIP(IPUtils.getRemoteIP());
		// txnlog.setADCONTENT(ADCONTENT);
		// txnlog.setADGUID(UUID.randomUUID().toString());
		// txnlog.setLASTDATE(DateTimeUtils.format("yyyyMMdd", new Date()));
		// txnlog.setLASTTIME(DateTimeUtils.format("HHmmss", new Date()));
		// txnlog.setLOGINTYPE("");
		// txnLogDao.save(txnlog);

		return n922;

	}
	
	
	private MVH doAction_SCORE(Map params) {
		
		String allanswer = "";
		String CUSIDN = (String) params.get("UID");
		try {
			/*** 修改記錄答案電文20180912 -開始 ***/
			String nAnswer[][] = new String[15][3];
			for (int i = 1; i <= nAnswer.length; i++) {
				int sc = 0;
				int sc2 = 0;

				if ((CUSIDN.length() != 10) && i == 15) {// 法人(14題)與自然人(15題)KYC問卷題目數量不同，需先判斷客戶身分
					nAnswer[i - 1][1] = "0";
					nAnswer[i - 1][2] = "0";
				} else {
					nAnswer[i - 1][0] = (String) params.get("FDQ" + i);

					for (int j = 1; j <= nAnswer[i - 1][0].length(); j++) {

						if (nAnswer[i - 1][0].substring(j - 1, j).equals("A")) {
							sc += 8;
						} else if (nAnswer[i - 1][0].substring(j - 1, j).equals("B")) {
							sc += 4;
						} else if (nAnswer[i - 1][0].substring(j - 1, j).equals("C")) {
							sc += 2;
						} else if (nAnswer[i - 1][0].substring(j - 1, j).equals("D")) {
							sc += 1;
						}
						if (nAnswer[i - 1][0].substring(j - 1, j).equals("E")) {
							sc2 += 8;
						} else if (nAnswer[i - 1][0].substring(j - 1, j).equals("F")) {
							sc2 += 4;
						} else if (nAnswer[i - 1][0].substring(j - 1, j).equals("G")) {
							sc2 += 2;
						} else if (nAnswer[i - 1][0].substring(j - 1, j).equals("H")) {
							sc2 += 1;
						}
					}
					nAnswer[i - 1][1] = String.valueOf(sc);
					nAnswer[i - 1][2] = String.valueOf(sc2);
				}
				log.warn("InvAttrib_S ----------> " + i + " " + nAnswer[i - 1][0] + ",sc: " + sc);
				log.warn("InvAttrib_S ----------> " + i + " " + nAnswer[i - 1][1] + ",sc2: " + sc2);
			}

			String[] HEX = new String[15];

			for (int k = 0; k < HEX.length; k++) {
				HEX[k] = Integer.toHexString(Integer.parseInt(nAnswer[k][1])).toUpperCase()
						+ Integer.toHexString(Integer.parseInt(nAnswer[k][2])).toUpperCase();
				log.warn("allanswer ----------> " + k + ":" + HEX[k]);
				allanswer += HEX[k];
			}

		}catch (Exception e) {
			log.error("Processing ANSWER error >> {}" , e.getMessage());
			throw TopMessageException.create("FE0003");
		}
		log.debug("allanswer ----------> " + allanswer);

		/*** 修改記錄答案電文20180918 -結束 ***/
		
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.clear();
		
		params.put("CUSIDN", CUSIDN);
		params.put("TYPE", "02");
		params.put("DATE", df);
		params.put("TIME", tf);
		
		if (answerHax.equals("Y")) {
			params.put("ANSWER", allanswer); // 修改答案傳遞電文
		} else {
			params.put("ANSWER", params.get("ANSWER"));// 變更問卷答案傳遞電文內容 // 先不做hex-20200527,by hugo
		}

		TelcommResult n322 = null;
		try {
			
			n322 = n322Telcomm.query(params);
			n322.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
			
		}catch(TopMessageException topex) {
			log.error(" n322Tel TopMessageException !!!!! >> {} " ,  topex.getMsgcode());
			throw topex ;
		}catch (Exception e) {
			log.error(" unknown error !!!!! ");
			log.error(" n322Tel Exception !!!!! >> {} " , e.getMessage());
		}
		
		return n322;
	}
}
