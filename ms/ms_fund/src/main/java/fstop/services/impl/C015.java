package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import com.netbank.rest.model.pool.N922Pool;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class C015 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("c015Telcomm")
	private TelCommExec c015Telcomm;

	@Autowired
	private N922Pool n922Pool;
	
//	@Required
//	public void setC015Telcomm(TelCommExec telcomm) {
//		c015Telcomm = telcomm;
//	}
//	
//	@Required
//	public void setN922Pool(N922Pool pool) {
//		n922Pool = pool;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 
		params.put("TXNIDT", DateTimeUtils.format("yyMMddHHmmss", new Date()));
		
		
		MVH mvh = n922Pool.getData((String)params.get("UID"));
		String aplbrh = mvh.getValueByFieldName("APLBRH");
		params.put("BRHCOD", aplbrh);
			

		TelcommResult helper = c015Telcomm.query(params);
		helper.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		helper.getFlatValues().put("BRHCOD", aplbrh);
		return helper;
	}


}
