package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import fstop.model.DBResult;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.TxnFundValueDao;
import fstop.services.CommonService;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class C002 extends CommonService  {  
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	private TxnFundValueDao txnFundValueDao;
	
	
//	@Required
//	public void setTxnFundValueDao(TxnFundValueDao dao) {
//		txnFundValueDao = dao;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 
		
		MVHImpl result = new MVHImpl();
		MVHImpl helper = new DBResult(txnFundValueDao.findSNotes());
		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		result.getFlatValues().put("CMRECNUM", helper.getValueOccurs("TRANSCODE") + "");
		result.getOccurs().addRows(helper.getOccurs());
		return result;
	}


}
