package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.rest.model.CommonPools;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class C021 extends CommonService{
	
	@Autowired
	@Qualifier("n922Telcomm")
	private TelCommExec n922Telcomm;
	
	@Autowired
	@Qualifier("c021Telcomm")
	private TelCommExec c021Telcomm;
	
	@Autowired
	private CommonPools commonPools;
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public MVH doAction(Map _params) { 
		
		Map<String, String> params = _params;
		params.put("TXNIDT", DateTimeUtils.format("yyMMddHHmmss", new Date()));

		//TODO 週一～週五(限為本行營業日) 上午8:00~下午3:00 執行本項交易視為當日生效(含全部與部分轉換)。
		
		TelcommResult helper = c021Telcomm.query(params);
		helper.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));

		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);

		commonPools.n922.removeGetDataCache((String)params.get("UID"));
		MVHImpl helperN922 = (MVHImpl)commonPools.n922.getData((String)params.get("UID"));
		
		helper.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));

		helper.getFlatValues().putAll(helperN922.getFlatValues());
		
		String str_BackFlag = (String)_params.get("BackFlag");
		
		if (str_BackFlag != null && "TRUE".equals(str_BackFlag)) {
			helper.getFlatValues().put("BackFlag", str_BackFlag);
		}	
		
		return helper;
	}
}