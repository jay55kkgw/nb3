package fstop.services.impl;

import java.util.Date;
import java.util.Hashtable;
import java.util.Map;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.rest.model.pool.FundUtils;

import topibs.utility.NumericUtil;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.services.NoticeInfo;
import fstop.services.NoticeInfoGetter;
import fstop.services.OnLineTransferNotice;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;


/**
 * 基金預約交易查詢/取消
 * @author Owner
 *
 */
@Slf4j
public class N394 extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("n394Telcomm")
	private TelCommExec n394Telcomm;

	@Autowired
	private FundUtils fundUtils;

//	@Required
//	public void setN394Telcomm(TelCommExec telcomm) {
//		n394Telcomm = telcomm;
//	}
//	
//	@Required
//	public void setFundUtils(FundUtils fundUtils) {
//		this.fundUtils = fundUtils;
//	}	
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params) { 
		Map<String, String> params = _params;
		params.put("CREDITNO", params.get("CDNO"));
		
		TelcommResult helper = n394Telcomm.query(params);
		helper.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		helper.getFlatValues().put("CDNO", helper.getFlatValues().get("CREDITNO"));

		return helper;
	}


}
