package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class N3231 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("n323ExecTelcomm")
	private TelCommExec n323ExecTelcomm;

//	@Required
//	public void setN323ExecTelcomm(TelCommExec telcomm) {
//		n323ExecTelcomm = telcomm;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params) { 

		Map<String, String> params = _params;
		params.put("CUSIDN", params.get("UID"));
		params.put("TYPE", "03");

		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);
		TelcommResult telcommResult = n323ExecTelcomm.query(params);
		
		return telcommResult;		
	}
}
