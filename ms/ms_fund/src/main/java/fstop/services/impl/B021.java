package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class B021 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("b021Telcomm")
	private TelCommExec b021Telcomm;
	
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 
		params.put("TXNIDT", DateTimeUtils.format("yyMMddHHmmss", new Date()));
		
		if("MB".equals(params.get("LOGINTYPE"))) {
			params.put("TXNSIID", "A020000");
		}
		
		TelcommResult telcommResult = b021Telcomm.query(params);
		telcommResult.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		telcommResult.getFlatValues().put("CMRECNUM", telcommResult.getValueOccurs("O01") + "");

		return telcommResult;
	}


}
