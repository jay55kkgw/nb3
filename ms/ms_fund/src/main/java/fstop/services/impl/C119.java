package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;


import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class C119 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("c119Telcomm")
	private TelCommExec c119Telcomm;
	
//	@Required
//	public void setC119Telcomm(TelCommExec telcomm) {
//		c119Telcomm = telcomm;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 
		params.put("CREDITNO", params.get("CDNO"));

		params.put("TXNIDT", DateTimeUtils.format("yyMMddHHmmss", new Date()));

		TelcommResult helper = c119Telcomm.query(params);
		helper.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		helper.getFlatValues().put("CDNO", helper.getFlatValues().get("CREDITNO"));

		return helper;
	}


}
