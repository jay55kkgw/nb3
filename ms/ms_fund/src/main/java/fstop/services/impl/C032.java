package fstop.services.impl;

import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.rest.model.CommonPools;

import fstop.exception.TopMessageException;
import fstop.exception.UncheckedException;
import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.orm.dao.TxnFundTraceLogDao;
import fstop.orm.po.TXNFUNDTRACELOG;
import fstop.services.BookingUtils;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
@Slf4j
public class C032 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("c021Telcomm")
	private TelCommExec c021Telcomm;
	@Autowired
	@Qualifier("n922Telcomm")
	private TelCommExec n922Telcomm;
	@Autowired
	private TxnFundTraceLogDao txnFundTraceLogDao;
	@Autowired
	private CommonPools commonPools;

//	@Required
//	public void setC021Telcomm(TelCommExec telcomm) {
//		c021Telcomm = telcomm;
//	}
//
//	@Required
//	public void setN922Telcomm(TelCommExec telcomm) {
//		n922Telcomm = telcomm;
//	}
//
//
//
//
//	@Required
//	public void setTxnFundTraceLogDao(TxnFundTraceLogDao txnFundTraceLogDao) {
//		this.txnFundTraceLogDao = txnFundTraceLogDao;
//	}

	
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params) { 
		Map<String, String> params = _params;

		params.put("TXNIDT", DateTimeUtils.format("yyMMddHHmmss", new Date()));

		TelcommResult helper = null;
		helper = c021Telcomm.query(params);

		helper.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);
		
		//TelcommResult helperN922 = n922Telcomm.query(params);
		commonPools.n922.removeGetDataCache((String)params.get("UID"));
		TelcommResult helperN922 = (TelcommResult)commonPools.n922.getData((String)params.get("UID"));
		
		helper.getFlatValues().putAll(helperN922.getFlatValues());
		
		String str_BackFlag = (String)params.get("BackFlag");
		
		if (str_BackFlag != null && "TRUE".equals(str_BackFlag)) {
			helper.getFlatValues().put("BackFlag", str_BackFlag);
		}	

		return helper;
	}


}
