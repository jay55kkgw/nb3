package fstop.services.impl;

import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import com.netbank.telcomm.CommonTelcomm;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class C115 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("c115Telcomm")
	private TelCommExec c115Telcomm;
	
//	@Required
//	public void setC115Telcomm(TelCommExec telcomm) {
//		c115Telcomm = telcomm;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 
		params.put("TXNIDT", DateTimeUtils.format("yyMMddHHmmss", new Date()));

		TelcommResult helper = c115Telcomm.query(params);
		helper.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		helper.getFlatValues().put("CMRECNUM", helper.getValueOccurs("TRANSCODE") + "");

		

		return helper;
	}


}
