package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class C024 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("c024Telcomm")
	private TelCommExec c024Telcomm;
	
//	@Required
//	public void setC024Telcomm(TelCommExec telcomm) {
//		c024Telcomm = telcomm;
//	}
	
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params) { 
		Map<String, String> params = _params;
		params.put("TXNIDT", DateTimeUtils.format("yyMMddHHmmss", new Date()));

		//TODO 週一～週五(限為本行營業日) 上午8:00~下午3:00 執行本項交易視為當日生效(含全部與部分轉換)。
		
		TelcommResult helper = c024Telcomm.query(params);
		helper.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));

		return helper;
	}


}
