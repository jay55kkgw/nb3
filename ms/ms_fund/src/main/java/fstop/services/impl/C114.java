package fstop.services.impl;

import java.util.Date;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import topibs.utility.NumericUtil;

import fstop.aop.advice.LoggerHelper;
import fstop.aop.advice.LoggerObject;
import fstop.exception.ToRuntimeException;
import fstop.exception.TopMessageException;
import fstop.exception.UncheckedException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import com.netbank.rest.model.pool.FundUtils;
import com.netbank.util.ESAPIUtil;
import fstop.orm.dao.TxnFundTraceLogDao;
import fstop.orm.po.TXNFUNDTRACELOG;
import fstop.services.BookingUtils;
import fstop.services.CommonService;
import fstop.services.NoticeInfo;
import fstop.services.NoticeInfoGetter;
import fstop.services.OnLineTransferNotice;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

import com.netbank.rest.model.FieldTranslator;

@Slf4j
public class C114 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("c114Telcomm")
	private TelCommExec c114Telcomm;

	@Autowired
	private TxnFundTraceLogDao txnFundTraceLogDao;

	@Autowired
	private FundUtils fundUtils;

//	@Required
//	public void setFundUtils(FundUtils fundUtils) {
//		this.fundUtils = fundUtils;
//	}
//	
//	@Required
//	public void setC114Telcomm(TelCommExec telcomm) {
//		c114Telcomm = telcomm;
//	}
//	
//	@Required
//	public void setTxnFundTraceLogDao(TxnFundTraceLogDao txnFundTraceLogDao) {
//		this.txnFundTraceLogDao = txnFundTraceLogDao;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params) { 
		if(null != _params.get("CDNO") && !_params.get("CDNO").equals("")) {
			_params.put("CREDITNO", _params.get("CDNO"));
		}
		
		Map<String, String> params = _params;
		
		params.put("TXNIDT", DateTimeUtils.format("yyMMddHHmmss", new Date()));
		
		TelcommResult helper = null;
		Throwable exception = null;
		try {
			helper = c114Telcomm.query(params);
			helper.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
			helper.getFlatValues().put("CDNO", helper.getFlatValues().get("CREDITNO"));
		}
		catch(UncheckedException e) {
			exception = e;
			throw e;
		}
		catch(Throwable e) {
			exception = e;
			throw new ToRuntimeException(e.getMessage(), e);
		}
		finally {
			boolean includeADTXNO = StrUtils.isNotEmpty(params.get("ADTXNO"));
			
			TXNFUNDTRACELOG logobj = new TXNFUNDTRACELOG();
			Date d = new Date();
			if(includeADTXNO)
				logobj.setADTXNO(StrUtils.trim(params.get("ADTXNO")));
			else
				logobj.setADTXNO(UUID.randomUUID().toString());

			logobj.setFDUSERID(StrUtils.trim(params.get("UID")));
			logobj.setFDTXTIME(DateTimeUtils.format("yyyyMMddHHmmss", d));
			logobj.setFDAGREEFLAG(StrUtils.trim(params.get("FDAGREEFLAG")));
			logobj.setFDNOTICETYPE(StrUtils.trim(params.get("FDNOTICETYPE")));
			logobj.setFDPUBLICTYPE(StrUtils.trim(params.get("FDPUBLICTYPE")));
			logobj.setFDTXLOG(BookingUtils.requestInfo(params));
			if(exception != null) {
				logobj.setFDTXSTATUS("ZX99");
				if(exception instanceof TopMessageException) {
					logobj.setFDTXSTATUS(((TopMessageException)exception).getMsgcode());
				}
			}
			LoggerObject obj = LoggerHelper.current();
			logobj.setADGUID(obj.getGUID());
			txnFundTraceLogDao.save(logobj);
			
			if(helper != null){
				helper.getFlatValues().put("ADTXNO", logobj.getADTXNO());
		        
				//發送成功Email 
				final String str_RESPCOD = helper.getValueByFieldName("RESPCOD");	//回應訊息
				
				if (str_RESPCOD.equals("0000")){
				final String str_TRANSCODE = helper.getValueByFieldName("TRANSCODE");	//基金代碼
				MVHImpl fundData = (MVHImpl)fundUtils.getFundData(helper.getValueByFieldName("TRANSCODE"));
				final String str_FUNDLNAME = fundData.getValueByFieldName("FUNDLNAME");//基金名稱
				final String str_FUNDAMT = NumericUtil.formatNumberString(helper.getValueByFieldName("FUNDAMT"), 2);	//申購金額
				final String str_FUNDCUR = helper.getValueByFieldName("FUNDCUR");	//幣別
				final String str_UID = params.get("UID");	//ID
				final String str_DPMYEMAIL = params.get("DPMYEMAIL");	//DPMYEMAIL
				final String str_CMTRMAIL = params.get("CMTRMAIL");	//CMTRMAIL
				final String str_CMQTIME = helper.getValueByFieldName("CMQTIME");	//ID
				
				String str_Bill = params.get("BILLSENDMODE");	//ID
				final String str_BillType = FieldTranslator.transfer("BILLSENDMODE",str_Bill);
				
				final String str_UNIT = NumericUtil.formatNumberString(helper.getValueByFieldName("UNIT"),4);
				log.debug("str_RESPCOD = " + str_RESPCOD);
				log.debug("str_TRANSCODE = " + str_TRANSCODE);
				log.debug("str_FUNDAMT = " + str_FUNDAMT);
				log.debug("str_FUNDLNAME = " + str_FUNDLNAME);
				log.debug("str_FUNDCUR = " + str_FUNDCUR);
				log.debug(ESAPIUtil.vaildLog("str_UID = " + str_UID));
				log.debug(ESAPIUtil.vaildLog("str_DPMYEMAIL = " + str_DPMYEMAIL));
				log.debug(ESAPIUtil.vaildLog("str_CMTRMAIL = " + str_CMTRMAIL));
				log.debug("str_CMQTIME = " + str_CMQTIME);
				log.debug(ESAPIUtil.vaildLog("str_BillType = " + str_BillType));
				log.debug("str_UNIT = " + str_UNIT);

				if (str_DPMYEMAIL!= null && str_DPMYEMAIL.length()>0){
				OnLineTransferNotice trNotice = new OnLineTransferNotice();

				trNotice.sendNotice(new NoticeInfoGetter() {

					public NoticeInfo getNoticeInfo() {
					
						NoticeInfo result = new NoticeInfo(str_UID,str_DPMYEMAIL,str_CMTRMAIL);
						//範本			
						result.setTemplateName("FUNDNOTICE");
					    result.getParams().put("#TITLE", "基金贖回通知");					

					    result.getParams().put("#TRANTIME", str_CMQTIME);
			
						result.getParams().put("#LINE_TITLE_1", "基金名稱");
						result.getParams().put("#LINE_CONTENT_1", "(" + str_TRANSCODE + ")" +str_FUNDLNAME);

						result.getParams().put("#LINE_TITLE_2", "贖回信託金額");
						result.getParams().put("#LINE_CONTENT_2", str_FUNDCUR + " " + str_FUNDAMT);
						
						result.getParams().put("#LINE_TITLE_3", "贖回方式");
						result.getParams().put("#LINE_CONTENT_3", str_BillType);
							
						result.getParams().put("#LINE_TITLE_4", "贖回單位數");
						result.getParams().put("#LINE_CONTENT_4", str_UNIT);
																		
						return result;
					}
					
				});
				}//確認Email
				}//0000
				//發送成功Email 
			}//!= null	
		}


		return helper;
	}


}
