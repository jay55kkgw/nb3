package fstop.services.impl;

import java.util.Date;
import java.util.Hashtable;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import topibs.utility.NumericUtil;

import fstop.aop.advice.LoggerHelper;
import fstop.aop.advice.LoggerObject;
import fstop.exception.ToRuntimeException;
import fstop.exception.TopMessageException;
import fstop.exception.UncheckedException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import com.netbank.rest.model.pool.FundUtils;
import com.netbank.util.ESAPIUtil;

import fstop.orm.dao.TxnFundTraceLogDao;
import fstop.orm.po.TXNFUNDTRACELOG;
import fstop.services.BookingUtils;
import fstop.services.CommonService;
import fstop.services.NoticeInfo;
import fstop.services.NoticeInfoGetter;
import fstop.services.OnLineTransferNotice;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class C020 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("c020Telcomm")
	private TelCommExec c020Telcomm;
	@Autowired
    @Qualifier("c026Telcomm")
	private TelCommExec c026Telcomm;
	@Autowired
	private TxnFundTraceLogDao txnFundTraceLogDao;
	@Autowired
	private FundUtils fundUtils;
	
//	@Required
//	public void setC020Telcomm(TelCommExec telcomm) {
//		c020Telcomm = telcomm;
//	}
//
//	@Required
//	public void setC026Telcomm(TelCommExec telcomm) {
//		c026Telcomm = telcomm;
//	}
//
//	@Required
//	public void setTxnFundTraceLogDao(TxnFundTraceLogDao txnFundTraceLogDao) {
//		this.txnFundTraceLogDao = txnFundTraceLogDao;
//	}
//	
//	@Required
//	public void setFundUtils(FundUtils fundUtils) {
//		this.fundUtils = fundUtils;
//	}
	
	
	/**
	 * 
	 * @param params
	 * @return
	 * @.................
	 */
	@Override
	public MVH doAction(Map _params) { 
		Map<String, String> params = _params;
		
		params.put("TXNIDT", DateTimeUtils.format("yyMMddHHmmss", new Date()));
		
		TelcommResult helper = null;
		Throwable exception = null;
		try {
			if(!"3".equals(params.get("PAYTYPE"))) {  // PAYTYPE <> 3
				helper = c020Telcomm.query(params);
			}
			else { 
				helper = c026Telcomm.query(params);
			}

			helper.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));

		}
		catch(UncheckedException e) {
			exception = e;
			throw e;
		}
		catch(Throwable e) {
			exception = e;
			throw new ToRuntimeException(e.getMessage(), e);
		}
		finally {
			boolean includeADTXNO = StrUtils.isNotEmpty(params.get("ADTXNO"));
			
			TXNFUNDTRACELOG logobj = new TXNFUNDTRACELOG();
			Date d = new Date();
			if(includeADTXNO)
				logobj.setADTXNO(StrUtils.trim(params.get("ADTXNO")));
			else
				logobj.setADTXNO(UUID.randomUUID().toString());

			logobj.setFDUSERID(StrUtils.trim(params.get("UID")));
			logobj.setFDTXTIME(DateTimeUtils.format("yyyyMMddHHmmss", d));
			logobj.setFDAGREEFLAG(StrUtils.trim(params.get("FDAGREEFLAG")));
			logobj.setFDNOTICETYPE(StrUtils.trim(params.get("FDNOTICETYPE")));
			logobj.setFDPUBLICTYPE(StrUtils.trim(params.get("FDPUBLICTYPE")));
			logobj.setFDTXLOG(BookingUtils.requestInfo(params));
			if(exception != null) {
				logobj.setFDTXSTATUS("ZX99");
				if(exception instanceof TopMessageException) {
					logobj.setFDTXSTATUS(((TopMessageException)exception).getMsgcode());
				}
			}
			LoggerObject obj = LoggerHelper.current();
			logobj.setADGUID(obj.getGUID());
			txnFundTraceLogDao.save(logobj);
			
			if(helper != null){
				helper.getFlatValues().put("ADTXNO", logobj.getADTXNO());
		
				//發送成功Email 
				final String str_RESPCOD = helper.getValueByFieldName("RESPCOD");	//回應訊息
				
				if (str_RESPCOD.equals("0000")){
				final String str_TRANSCODE = helper.getValueByFieldName("TRANSCODE");	//基金代碼
				final String str_AMT3 = NumericUtil.formatNumberString(helper.getValueByFieldName("AMT3"), 0);	//申購金額
				MVHImpl fundData = (MVHImpl)fundUtils.getFundData(helper.getValueByFieldName("TRANSCODE"));
				final String str_FUNDLNAME = fundData.getValueByFieldName("FUNDLNAME");//基金名稱
				final String str_CUR = helper.getValueByFieldName("CUR");	//幣別
				final String str_UID = params.get("UID");	//ID
				final String str_DPMYEMAIL = params.get("DPMYEMAIL");	//DPMYEMAIL
				final String str_CMTRMAIL = params.get("CMTRMAIL");	//CMTRMAIL
				final String str_CMQTIME = helper.getValueByFieldName("CMQTIME");	//ID

				final String str_FCA2 = NumericUtil.formatNumberString(helper.getValueByFieldName("FCA2"), 2);	//手續費
				final String str_AMT5 = NumericUtil.formatNumberString(helper.getValueByFieldName("AMT5"), 2);	//扣款金額
				
				String str_Date = "";
				final String str_Date1;
				
				if (!"00".equals(helper.getValueByFieldName("PAYDAY1"))){
					str_Date = str_Date + helper.getValueByFieldName("PAYDAY1") + "日";
				}
				if (!"00".equals(helper.getValueByFieldName("PAYDAY2"))){
					str_Date = str_Date + helper.getValueByFieldName("PAYDAY2") + "日";
				}		
				if (!"00".equals(helper.getValueByFieldName("PAYDAY3"))){
					str_Date = str_Date + helper.getValueByFieldName("PAYDAY3") + "日";
				}
				if (!"00".equals(helper.getValueByFieldName("PAYDAY4"))){
					str_Date = str_Date + helper.getValueByFieldName("PAYDAY4") + "日";
				}
				if (!"00".equals(helper.getValueByFieldName("PAYDAY5"))){
					str_Date = str_Date + helper.getValueByFieldName("PAYDAY5") + "日";
				}
				if (!"00".equals(helper.getValueByFieldName("PAYDAY6"))){
					str_Date = str_Date + helper.getValueByFieldName("PAYDAY6") + "日";
				}
				
				//信用卡扣款只有6日
				if(!"3".equals(params.get("PAYTYPE"))) {
				  if (!"00".equals(helper.getValueByFieldName("PAYDAY7"))){
					  str_Date = str_Date + helper.getValueByFieldName("PAYDAY7") + "日";
				  }
				  if (!"00".equals(helper.getValueByFieldName("PAYDAY8"))){
					  str_Date = str_Date + helper.getValueByFieldName("PAYDAY8") + "日";
				  }
				  if (!"00".equals(helper.getValueByFieldName("PAYDAY9"))){
					  str_Date = str_Date + helper.getValueByFieldName("PAYDAY9") + "日";
				  }
				}
				
				str_Date1 = str_Date;
				log.debug("str_RESPCOD = " + str_RESPCOD);
				log.debug("str_TRANSCODE = " + str_TRANSCODE);
				log.debug("str_AMT3 = " + str_AMT3);
				log.debug("str_FUNDLNAME = " + str_FUNDLNAME);
				log.debug("str_CUR = " + str_CUR);
				log.debug(ESAPIUtil.vaildLog("str_UID = " + str_UID));
				log.debug(ESAPIUtil.vaildLog("str_DPMYEMAIL = " + str_DPMYEMAIL));
				log.debug(ESAPIUtil.vaildLog("str_CMTRMAIL = " + str_CMTRMAIL));
				log.debug("str_CMQTIME = " + str_CMQTIME);
				log.debug("str_FCA2 = " + str_FCA2);
				log.debug("str_AMT5 = " + str_AMT5);
				log.debug("str_Date = " + str_Date);

				if (str_DPMYEMAIL!= null && str_DPMYEMAIL.length()>0){
				OnLineTransferNotice trNotice = new OnLineTransferNotice();

				trNotice.sendNotice(new NoticeInfoGetter() {

					public NoticeInfo getNoticeInfo() {
					
						NoticeInfo result = new NoticeInfo(str_UID,str_DPMYEMAIL,str_CMTRMAIL);
						//範本			
						result.setTemplateName("FUNDNOTICE");
					    result.getParams().put("#TITLE", "基金定期投資通知");					

											
					    result.getParams().put("#TRANTIME", str_CMQTIME);
			
						result.getParams().put("#LINE_TITLE_1", "基金名稱");
						result.getParams().put("#LINE_CONTENT_1", str_FUNDLNAME);

						result.getParams().put("#LINE_TITLE_2", "每次扣<BR>款金額");
						result.getParams().put("#LINE_CONTENT_2", str_CUR + " " + str_AMT3);
						
						result.getParams().put("#LINE_TITLE_3", "手續費");
						result.getParams().put("#LINE_CONTENT_3", str_FCA2);
							
						result.getParams().put("#LINE_TITLE_4", "扣款金額");
						result.getParams().put("#LINE_CONTENT_4", str_AMT5);
						
						result.getParams().put("#LINE_TITLE_5", "幣別");
						result.getParams().put("#LINE_CONTENT_5", str_CUR);
						
						result.getParams().put("#LINE_TITLE_6", "每月扣款日");
						result.getParams().put("#LINE_CONTENT_6", str_Date1);
						
						
						return result;
					}
					
				});
				}//確認Email
				}//0000
				//發送成功Email
			} //!= null
		}
		log.debug("test");
		return helper;
	}


}
