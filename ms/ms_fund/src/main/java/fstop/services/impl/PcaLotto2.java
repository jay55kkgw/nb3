package fstop.services.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.rest.model.JSPUtils;
import com.netbank.util.ESAPIUtil;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.PcaLottoNoticeDao;
import fstop.orm.dao.PcaLottoShowDao;
import fstop.orm.po.PCALOTTONOTICE;
import fstop.services.CommonService;
import fstop.util.DateTimeUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import fstop.util.thread.WebServletUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Simon
 *
 */
@Slf4j
public class PcaLotto2 extends CommonService {
	
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	private PcaLottoNoticeDao pcaLottoNoticeDao;
		
//	@Required
//	public void setPcaLottoNoticeDao(PcaLottoNoticeDao pcaLottoNoticeDao) {
//		this.pcaLottoNoticeDao = pcaLottoNoticeDao;
//	}
	
	public PcaLotto2(){}
	
	/**
	 * 保誠人壽世代共享專案抽獎通知
	 * @param params
	 * @return MVH
	 */
	@Override
	public MVH doAction(Map _params) {
		Map<String, String> params = _params;
		MVHImpl result = new MVHImpl();
		PcaLottoNoticeDao pcaLottoNoticeDao = (PcaLottoNoticeDao)SpringBeanFactory.getBean("pcaLottoNoticeDao");
		
		Date d = new Date();
		String addr = StrUtils.trim(params.get("area")) + StrUtils.trim(params.get("area2")) + StrUtils.trim(params.get("address"));
		
		try {
			//判斷客戶當週是否已送出過抽獎資料--Start--Simon
			java.util.Calendar cldr=java.util.Calendar.getInstance(Locale.TAIWAN);
			//設定每週起日為星期一
			cldr.setFirstDayOfWeek(java.util.Calendar.MONDAY);
			Date dt1 = new Date(cldr.getTimeInMillis());
			log.debug("當前時間:"+DateTimeUtils.format("yyyyMMdd", dt1));
			cldr.set(java.util.Calendar.DAY_OF_WEEK, java.util.Calendar.MONDAY);
			Date dt2 = new Date(cldr.getTimeInMillis());
			log.debug("週一時間:"+ DateTimeUtils.format("yyyyMMdd", dt2));
			cldr.set(java.util.Calendar.DAY_OF_WEEK, java.util.Calendar.SUNDAY);
			Date dt3 = new Date(cldr.getTimeInMillis());
			log.debug("週日時間:"+ DateTimeUtils.format("yyyyMMdd", dt3));
			//判斷客戶當週是否已送出過抽獎資料--End--Simon
			
			List<PCALOTTONOTICE> pcadata = pcaLottoNoticeDao.checkPcaData(JSPUtils.convertFullorHalf(StrUtils.trim(params.get("name")), 1), StrUtils.trim(params.get("email")), DateTimeUtils.format("yyyyMMdd", dt2), DateTimeUtils.format("yyyyMMdd", dt3));
			log.debug("pcadata size：" + pcadata.size());
			//若客戶當週未送過抽獎資料才可儲存抽獎資料--Simon
			
			if(pcadata.size() == 0){
				//pcaLottoNoticeDao.insertPcaData(JSPUtils.convertFullorHalf(StrUtils.trim(params.get("name")), 1), 
				//								StrUtils.trim(params.get("tel")), JSPUtils.convertFullorHalf(addr, 1), 
				//								StrUtils.trim(params.get("email")), JSPUtils.convertFullorHalf(StrUtils.trim(params.get("ttb")), 1), 
				//								JSPUtils.convertFullorHalf(StrUtils.trim(params.get("animal")), 1), 
				//								JSPUtils.convertFullorHalf(StrUtils.trim(params.get("setcontext")), 1), 
				//								"SYS", DateTimeUtils.format("yyyyMMdd", d), DateTimeUtils.format("HHmmss", d));
				//記錄抽獎客戶資料
				
				PCALOTTONOTICE pcalotto = new PCALOTTONOTICE();
				
				log.debug(ESAPIUtil.vaildLog("PcaLotto.java NAME1:"+JSPUtils.convertFullorHalf(StrUtils.trim(params.get("name")), 1) + " NAME2:"+StrUtils.trim(params.get("name"))));
				pcalotto.setNAME(JSPUtils.convertFullorHalf(StrUtils.trim(params.get("name")), 1));
				
				log.debug(ESAPIUtil.vaildLog("PcaLotto.java TELPHONE:"+StrUtils.trim(params.get("tel"))));
				pcalotto.setTELPHONE(StrUtils.trim(params.get("tel")));
				
				log.debug(ESAPIUtil.vaildLog("PcaLotto.java ADDRESS1:"+JSPUtils.convertFullorHalf(addr, 1) + " ADDRESS2:"+addr));
				pcalotto.setADDRESS(JSPUtils.convertFullorHalf(addr, 1));
				
				log.debug(ESAPIUtil.vaildLog("PcaLotto.java EMAIL:"+StrUtils.trim(params.get("email"))));
//				pcalotto.setEMAIL(StrUtils.trim(params.get("email")));
				String email = StrUtils.trim(params.get("email"));
				pcalotto.setEMAIL(email.toLowerCase());
				
				log.debug(ESAPIUtil.vaildLog("PcaLotto.java BRANCHNAME1:"+JSPUtils.convertFullorHalf(StrUtils.trim(params.get("ttb")), 1) + " BRANCHNAME2:"+StrUtils.trim(params.get("ttb"))));
				pcalotto.setBRANCHNAME(JSPUtils.convertFullorHalf(StrUtils.trim(params.get("ttb")), 1));
				
				log.debug(ESAPIUtil.vaildLog("PcaLotto.java ANIMALKIND1:"+JSPUtils.convertFullorHalf(StrUtils.trim(params.get("animal")), 1) + " ANIMALKIND2:"+StrUtils.trim(params.get("animal"))));
				pcalotto.setANIMALKIND(JSPUtils.convertFullorHalf(StrUtils.trim(params.get("animal")), 1));
				
				log.debug(ESAPIUtil.vaildLog("PcaLotto.java MEMO1:"+JSPUtils.convertFullorHalf(StrUtils.trim(params.get("setcontext")), 1) + " MEMO2:"+StrUtils.trim(params.get("setcontext"))));
				pcalotto.setMEMO(JSPUtils.convertFullorHalf(StrUtils.trim(params.get("setcontext")), 1));
				
				//--新增儲存部分--start--
				log.debug(ESAPIUtil.vaildLog("PcaLotto.java SEX:"+StrUtils.trim(params.get("sex"))));
				pcalotto.setSEX(StrUtils.trim(params.get("sex")));
				
				log.debug(ESAPIUtil.vaildLog("PcaLotto.java AGE:"+StrUtils.trim(params.get("old"))));
				pcalotto.setAGE(StrUtils.trim(params.get("old")));
				//--新增儲存部分--end--
				
				pcalotto.setLASTUSER("SYS");
				pcalotto.setLASTDATE(DateTimeUtils.format("yyyyMMdd", d));
				pcalotto.setLASTTIME(DateTimeUtils.format("HHmmss", d));
				
				pcaLottoNoticeDao.save(pcalotto);
				
				log.debug(ESAPIUtil.vaildLog(JSPUtils.convertFullorHalf(StrUtils.trim(params.get("name")), 1)+" "+StrUtils.trim(params.get("name"))+" "+StrUtils.trim(params.get("email"))+ "PcaLottoNotice：Insert success"));
			
			
			
			
			}else{
				//自製網頁導向
				HttpServletRequest request = WebServletUtils.getRequest();
				HttpServletResponse response = WebServletUtils.getResponse();
				ServletContext context = request.getSession().getServletContext();
				request.setAttribute("pcalChk", "N");
				log.debug("pcalChk = N");
//				RequestDispatcher dispatcher = context.getRequestDispatcher("pcalife/PcaLotto2.jsp");
//				log.debug("forward to 'pcalife/PcaLotto2.jsp'");
//				dispatcher.forward(request, response);
				log.debug("本週已經參加過活動，無法再次參加");
				
			}
		
		}catch (Exception e) {
			log.error(ESAPIUtil.vaildLog(JSPUtils.convertFullorHalf(StrUtils.trim(params.get("name")), 1)+" "+StrUtils.trim(params.get("name"))+" "+StrUtils.trim(params.get("email"))+ "PcaLottoNotice：Insert fail"));
		}
		
		return result;
	}
}
