package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;


@Slf4j
public class C011 extends CommonService  {
	private Logger logger = Logger.getLogger(getClass());
	
	
	@Autowired
    @Qualifier("c011Telcomm")
	private TelCommExec c011Telcomm;
		
	/**
	 * 
	 * @param params
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public MVH doAction(Map params) { 
		params.put("TXNIDT", DateTimeUtils.format("yyMMddHHmmss", new Date()));
		
		TelcommResult helper = c011Telcomm.query(params);
		logger.warn("UID:"+helper.getValueByFieldName("CUSIDN")+"  FLAG:"+helper.getValueByFieldName("FLAG"));
		return helper;
	}


}
