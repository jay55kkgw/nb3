package fstop.services.impl;

import java.net.URLEncoder;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;

import fstop.aop.advice.LoggerHelper;
import fstop.aop.advice.LoggerObject;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.TxnFundTraceLogDao;
import fstop.orm.po.TXNFUNDTRACELOG;
import fstop.services.CommonService;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import fstop.va.VaCheck;
import lombok.extern.slf4j.Slf4j;
import fstop.util.JSONUtils;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class Ikeycheck extends CommonService  {

	
	
	@Override
	public MVH doAction(Map _params) { 
		MVHImpl resultMVH = new MVHImpl();
		Map<String, String> params = _params;
		String UID = params.get("UID");
		String jsondc = params.get("jsondc");
		String __mac = params.get("pkcs7Sign");
		
		String result = VaCheck.doVAVerify1(__mac, jsondc, "VARule1");
		
		String checkflag="FAIL";
		String XMLCN1="";
		if (result == null || result.length()==0){
			checkflag="FAIL";
		}
		else {
			Pattern p = Pattern.compile("CN=([^\\|]+)", Pattern.CASE_INSENSITIVE);
			Pattern p2 = Pattern.compile("C=.+?,O=.+?,OU=(.+?),CN=[^\\|]+", Pattern.CASE_INSENSITIVE);

			if(!StrUtils.trim(result).startsWith("ERROR")) {
				log.error("@@@ XMLCN.doCommand() IN ERROR XMLCN");
			try {
				Matcher m = p.matcher(result);
				String xmlcn = "";
				if(m.find()) {
					xmlcn = StrUtils.trim(m.group(1));
				}
				else {
					checkflag="FAIL";
				}

				System.out.println("Put XMLCN: " + xmlcn);
			    XMLCN1=xmlcn;
			}
			catch(RuntimeException e) {
				checkflag="FAIL";
			}			
			if(!XMLCN1.startsWith(UID.toUpperCase())) {
				checkflag="FAIL";
			}else {
				checkflag="SUCCESSFUL";
			}
		 }
		}
		resultMVH.getFlatValues().put("checkflag", checkflag);
		return resultMVH;	
	}


}
