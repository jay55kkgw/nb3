package fstop.services.impl;

import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import topibs.utility.NumericUtil;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.services.NoticeInfo;
import fstop.services.NoticeInfoGetter;
import fstop.services.OnLineTransferNotice;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

import com.netbank.rest.model.pool.FundUtils;
import com.netbank.util.ESAPIUtil;


@Slf4j
public class C112 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("c112Telcomm")
	private TelCommExec c112Telcomm;
	
	@Autowired
    @Qualifier("c112_skip_p_Telcomm")
	private TelCommExec c112_skip_p_Telcomm;
	
//	@Required
//	public void setC112Telcomm(TelCommExec telcomm) {
//		c112Telcomm = telcomm;
//	}

	@Autowired
	private FundUtils fundUtils;
	
//	@Required
//	public void setFundUtils(FundUtils fundUtils) {
//		this.fundUtils = fundUtils;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 
		if(null != params.get("CDNO") && !params.get("CDNO").equals("")) {
			params.put("CREDITNO", params.get("CDNO"));
		}
		Map params_res = params;
		params.put("TXNIDT", DateTimeUtils.format("yyMMddHHmmss", new Date()));
		log.warn(ESAPIUtil.vaildLog("C112.java ALTERTYPE："+(params.get("ALTERTYPE")==null ? "" : (String)params.get("ALTERTYPE"))));
		//若有變更扣款金額
		String alerttype1= params.get("ALTERTYPE1")==null ? "" : params.get("ALTERTYPE1").toString();
		//若有變更扣款日期
		String alerttype2= params.get("ALTERTYPE2")==null ? "" : params.get("ALTERTYPE2").toString();
		//若有變更扣款金額
		if (!alerttype1.equals("")) 			
			params.put("TYPE1", "1");		
		else 
			params.put("TYPE1", "0");
		//若有變更扣款日期
		if (!alerttype2.equals("")) 			
			params.put("TYPE2", "1");		
		else 
			params.put("TYPE2", "0");
		//若有變更扣款狀態
		if (! params.get("ALTERTYPE").toString().equals("")) {
			
			//若有變更恢復扣款
			if (params.get("ALTERTYPE").toString().equals("B2"))		
				params.put("TYPE5", "1");			
			else 
				params.put("TYPE5", "0");			

			//若有變更暫停扣款
			if (params.get("ALTERTYPE").toString().equals("B1")) 			
				params.put("TYPE4", "1");			
			else 
				params.put("TYPE4", "0");			

			//若有變更終止扣款
			if (params.get("ALTERTYPE").toString().equals("B0")) 			
				params.put("TYPE3", "1");			
			else 
				params.put("TYPE3", "0");								
			
		}		
		else {
			params.put("TYPE3", "0");		
			params.put("TYPE4", "0");
			params.put("TYPE5", "0");		
		}
		
		String payday1 = params.get("PAYDAY1")==null ? "" : (String)params.get("PAYDAY1");
		String payday2 = params.get("PAYDAY2")==null ? "" : (String)params.get("PAYDAY2");
		String payday3 = params.get("PAYDAY3")==null ? "" : (String)params.get("PAYDAY3");
		String payday4 = params.get("PAYDAY4")==null ? "" : (String)params.get("PAYDAY4");
		String payday5 = params.get("PAYDAY5")==null ? "" : (String)params.get("PAYDAY5");
		String payday6 = params.get("PAYDAY6")==null ? "" : (String)params.get("PAYDAY6");
		String payday7 = params.get("PAYDAY7")==null ? "" : (String)params.get("PAYDAY7");
		String payday8 = params.get("PAYDAY8")==null ? "" : (String)params.get("PAYDAY8");
		String payday9 = params.get("PAYDAY9")==null ? "" : (String)params.get("PAYDAY9");
		String RSKATT = params.get("RSKATT")==null ? "" : (String)params.get("RSKATT");
		params.remove("PAYDAY1");
		params.remove("PAYDAY2");
		params.remove("PAYDAY3");
		params.remove("PAYDAY4");
		params.remove("PAYDAY5");
		params.remove("PAYDAY6");
		params.remove("PAYDAY7");
		params.remove("PAYDAY8");
		params.remove("PAYDAY9");
		int ind = 1;
		if(StrUtils.isNotEmpty(payday1)) {
			params.put("PAYDAY" + ind, payday1);
			ind++;
		}
		if(StrUtils.isNotEmpty(payday2)) {
			params.put("PAYDAY" + ind, payday2);
			ind++;
		}
		if(StrUtils.isNotEmpty(payday3)) {
			params.put("PAYDAY" + ind, payday3);
			ind++;
		}
		if(StrUtils.isNotEmpty(payday4)) {
			params.put("PAYDAY" + ind, payday4);
			ind++;
		}
		if(StrUtils.isNotEmpty(payday5)) {
			params.put("PAYDAY" + ind, payday5);
			ind++;
		}
		if(StrUtils.isNotEmpty(payday6)) {
			params.put("PAYDAY" + ind, payday6);
			ind++;
		}
		if(StrUtils.isNotEmpty(payday7)) {
			params.put("PAYDAY" + ind, payday7);
			ind++;
		}
		if(StrUtils.isNotEmpty(payday8)) {
			params.put("PAYDAY" + ind, payday8);
			ind++;
		}
		if(StrUtils.isNotEmpty(payday9)) {
			params.put("PAYDAY" + ind, payday9);
			ind++;
		}
		params.put("RSKATT", RSKATT);
		String begD = StrUtils.trim((String)params.get("STOPBEGDAY")).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
		String endD = StrUtils.trim((String)params.get("STOPENDDAY")).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
		String resD = StrUtils.trim((String)params.get("RESTOREDAY")).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
		
		params.put("STOPBEGDAY", begD);
		params.put("STOPENDDAY", endD);
		params.put("RESTOREDAY", resD);
		
		
		TelcommResult helper = null;
		
		if(StrUtils.isNotEmpty((String) params.get("SKIP_P"))) {
			helper = c112_skip_p_Telcomm.query(params);
		}else {
			helper = c112Telcomm.query(params);
		}
		helper.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		helper.getFlatValues().put("CDNO", helper.getFlatValues().get("CREDITNO"));

		//發送成功Email 
		if(helper != null){
			final String str_RESPCOD = helper.getValueByFieldName("RESPCOD");	//回應訊息
			
			if (str_RESPCOD.equals("0000")){
			final String str_TRANSCODE = helper.getValueByFieldName("TRANSCODE");	//基金代碼
			
			String str_TypeFlag = (String)params.get("TYPEFLAG");
			String str_Type1 = str_TypeFlag.substring(0 ,1); //扣款金額
			String str_Type2 = str_TypeFlag.substring(1 ,2); //扣款日期
			String str_Type3 = str_TypeFlag.substring(2 ,3); //終止扣款
			String str_Type4 = str_TypeFlag.substring(3 ,4); //暫停扣款
			String str_Type5 = str_TypeFlag.substring(4);    //恢復扣款

			String str_ALTERTYPE1 = (params.get("ALTERTYPE1") != null) ? params.get("ALTERTYPE1").toString() : "";
			String str_ALTERTYPE2 = (params.get("ALTERTYPE2") != null) ? params.get("ALTERTYPE2").toString() : "";
			String str_ALTERTYPE3 = (params.get("ALTERTYPE3") != null) ? params.get("ALTERTYPE3").toString() : "";	
			String str_ALTERTYPE = (params.get("ALTERTYPE") != null) ? params.get("ALTERTYPE").toString() : "";
			
			String MIP = (String)params.get("MIP");

			String DAMT = (String)params.get("DAMT");
			
			log.debug(ESAPIUtil.vaildLog("MIP = " + MIP));
			log.debug(ESAPIUtil.vaildLog("DAMT = " + DAMT));

			
			
			String str_MIP = "";
	        String str_OPAYAMT ="";
	        String str_NPAYAMT ="";
	        if("Y".equals(MIP)) {
	        	str_MIP="定期不定額";
	            str_OPAYAMT="原不定額基準申購金額";
	            str_NPAYAMT="變更後不定額基準申購金額";
	        }	

	        else {
	            str_MIP="定期定額";
	            str_OPAYAMT="原每次定額申購金額";
	            str_NPAYAMT="變更後每次定額申購金額"; 
	        } 
			
	        final String str_MIP_ = str_MIP;
	        final String str_OPAYAMT_ = str_OPAYAMT;
	        final String str_NPAYAMT_ = str_NPAYAMT;
			
			final String str_FUNDLNAME = (String)params.get("FUNDLNAME");//基金名稱
			
			//
			String str_ALTERTYPE_chk = "";
			if (!str_ALTERTYPE1.equals(""))
				str_ALTERTYPE_chk = str_ALTERTYPE_chk + "金額  ";
			if (!str_ALTERTYPE2.equals(""))
				str_ALTERTYPE_chk = str_ALTERTYPE_chk + "日期  ";
			
			if (!str_ALTERTYPE3.equals(""))
				str_ALTERTYPE_chk = str_ALTERTYPE_chk + "扣款狀態  ";
			
			final String str_ALTERTYPE_chk_ = str_ALTERTYPE_chk;	
			
			
			String str_line_title_1 = "";
			String str_line_content_1 = "";

			
			if (! str_ALTERTYPE1.equals("")) {
				str_line_title_1 = str_OPAYAMT + "<BR>" + str_NPAYAMT;
				str_line_content_1 = (String)params.get("PAYCUR") + "" + NumericUtil.formatNumberString((String)params.get("OPAYAMT"), 0)
				                     +"<BR>"+ (String)params.get("PAYCUR") + "" + NumericUtil.formatNumberString((String)params.get("PAYAMT"), 0);
			}

			
			
			if (!str_ALTERTYPE2.equals("")) {
                    
				    if (!str_line_title_1.equals(""))
				    	str_line_title_1 = str_line_title_1 + "<BR>";
				    
				    if (!str_line_content_1.equals(""))
				    	str_line_content_1 = str_line_content_1 + "<BR>";
			
				    str_line_title_1 = str_line_title_1 + "原日期<BR>變更後日期";
			
					String str_Date_old = "";
					
					log.debug(ESAPIUtil.vaildLog("OPAYDAY1_res = " + params_res.get("OPAYDAY1")));
					log.debug(ESAPIUtil.vaildLog("OPAYDAY2_res = " + params_res.get("OPAYDAY2")));
					log.debug(ESAPIUtil.vaildLog("OPAYDAY3_res = " + params_res.get("OPAYDAY3")));
					log.debug(ESAPIUtil.vaildLog("OPAYDAY4_res = " + params_res.get("OPAYDAY4")));
					log.debug(ESAPIUtil.vaildLog("OPAYDAY5_res = " + params_res.get("OPAYDAY5")));
					log.debug(ESAPIUtil.vaildLog("OPAYDAY6_res = " + params_res.get("OPAYDAY6")));
					log.debug(ESAPIUtil.vaildLog("OPAYDAY7_res = " + params_res.get("OPAYDAY7")));
					log.debug(ESAPIUtil.vaildLog("OPAYDAY8_res = " + params_res.get("OPAYDAY8")));
					log.debug(ESAPIUtil.vaildLog("OPAYDAY9_res = " + params_res.get("OPAYDAY9")));
					
					
					String PAYDAY1_res = params_res.get("OPAYDAY1")==null ? "00" : (String)params_res.get("OPAYDAY1");
					String PAYDAY2_res = params_res.get("OPAYDAY2")==null ? "00" : (String)params_res.get("OPAYDAY2");
					String PAYDAY3_res = params_res.get("OPAYDAY3")==null ? "00" : (String)params_res.get("OPAYDAY3");
					String PAYDAY4_res = params_res.get("OPAYDAY4")==null ? "00" : (String)params_res.get("OPAYDAY4");
					String PAYDAY5_res = params_res.get("OPAYDAY5")==null ? "00" : (String)params_res.get("OPAYDAY5");
					String PAYDAY6_res = params_res.get("OPAYDAY6")==null ? "00" : (String)params_res.get("OPAYDAY6");
					String PAYDAY7_res = params_res.get("OPAYDAY7")==null ? "00" : (String)params_res.get("OPAYDAY7");
					String PAYDAY8_res = params_res.get("OPAYDAY8")==null ? "00" : (String)params_res.get("OPAYDAY8");
					String PAYDAY9_res = params_res.get("OPAYDAY9")==null ? "00" : (String)params_res.get("OPAYDAY9");

					log.debug(ESAPIUtil.vaildLog("PAYDAY1_res = " + PAYDAY1_res));
					log.debug(ESAPIUtil.vaildLog("PAYDAY2_res = " + PAYDAY2_res));
					log.debug(ESAPIUtil.vaildLog("PAYDAY3_res = " + PAYDAY3_res));
					log.debug(ESAPIUtil.vaildLog("PAYDAY4_res = " + PAYDAY4_res));
					log.debug(ESAPIUtil.vaildLog("PAYDAY5_res = " + PAYDAY5_res));
					log.debug(ESAPIUtil.vaildLog("PAYDAY6_res = " + PAYDAY6_res));
					log.debug(ESAPIUtil.vaildLog("PAYDAY7_res = " + PAYDAY7_res));
					log.debug(ESAPIUtil.vaildLog("PAYDAY8_res = " + PAYDAY8_res));
					log.debug(ESAPIUtil.vaildLog("PAYDAY9_res = " + PAYDAY9_res));

					
					
					if (!"00".equals(PAYDAY1_res)){
						str_Date_old = str_Date_old + PAYDAY1_res + "日";
					}
					if (!"00".equals(PAYDAY2_res)){
						str_Date_old = str_Date_old + PAYDAY2_res + "日";
					}		
					if (!"00".equals(PAYDAY3_res)){
						str_Date_old = str_Date_old + PAYDAY3_res + "日";
					}
					if (!"00".equals(PAYDAY4_res)){
						str_Date_old = str_Date_old + PAYDAY4_res + "日";
					}
					if (!"00".equals(PAYDAY5_res)){
						str_Date_old = str_Date_old + PAYDAY5_res + "日";
					}
					if (!"00".equals(PAYDAY6_res)){
						str_Date_old = str_Date_old + PAYDAY6_res + "日";
					}
					
					//信用卡扣款只有6日
					if(DAMT.equals("2")) {

					  if (!"00".equals(PAYDAY7_res)){
						  str_Date_old = str_Date_old + PAYDAY7_res + "日";
					  }
					  if (!"00".equals(PAYDAY8_res)){
						  str_Date_old = str_Date_old + PAYDAY8_res + "日";
					  }
					  if (!"00".equals(PAYDAY9_res)){
						  str_Date_old = str_Date_old + PAYDAY9_res + "日";
					  }
					}
			
                    String str_Date_new = "";
					
					if (!"00".equals(helper.getValueByFieldName("PAYDAY1"))){
						str_Date_new = str_Date_new + helper.getValueByFieldName("PAYDAY1") + "日";
					}
					if (!"00".equals(helper.getValueByFieldName("PAYDAY2"))){
						str_Date_new = str_Date_new + helper.getValueByFieldName("PAYDAY2") + "日";
					}		
					if (!"00".equals(helper.getValueByFieldName("PAYDAY3"))){
						str_Date_new = str_Date_new + helper.getValueByFieldName("PAYDAY3") + "日";
					}
					if (!"00".equals(helper.getValueByFieldName("PAYDAY4"))){
						str_Date_new = str_Date_new + helper.getValueByFieldName("PAYDAY4") + "日";
					}
					if (!"00".equals(helper.getValueByFieldName("PAYDAY5"))){
						str_Date_new = str_Date_new + helper.getValueByFieldName("PAYDAY5") + "日";
					}
					if (!"00".equals(helper.getValueByFieldName("PAYDAY6"))){
						str_Date_new = str_Date_new + helper.getValueByFieldName("PAYDAY6") + "日";
					}
					
					//信用卡扣款只有6日
					if(DAMT.equals("2")) {

					  if (!"00".equals(helper.getValueByFieldName("PAYDAY7"))){
						  str_Date_new = str_Date_new + helper.getValueByFieldName("PAYDAY7") + "日";
					  }
					  if (!"00".equals(helper.getValueByFieldName("PAYDAY8"))){
						  str_Date_new = str_Date_new + helper.getValueByFieldName("PAYDAY8") + "日";
					  }
					  if (!"00".equals(helper.getValueByFieldName("PAYDAY9"))){
						  str_Date_new = str_Date_new + helper.getValueByFieldName("PAYDAY9") + "日";
					  }
					}
			
					str_line_content_1 = str_line_content_1 + str_Date_old + "<BR>" + str_Date_new;
			}		
					
			
			if (! str_ALTERTYPE3.equals("")) {

				if (!str_line_title_1.equals(""))
			    	str_line_title_1 = str_line_title_1 + "<BR>";
			    
			    if (!str_line_content_1.equals(""))
			    	str_line_content_1 = str_line_content_1 + "<BR>";
				
				str_line_title_1 = str_line_title_1 + "原扣款狀態 <BR> 變更後扣款狀態";

      			String str_Before = "";
				if (!str_Type5.equals("1"))
					str_Before = "恢復扣款";
				else if (!str_Type4.equals("1"))
					str_Before = "暫停扣款";
				else if (!str_Type3.equals("1"))
					str_Before = "終止扣款";			
     
    			String str_After = "";
 				if (str_ALTERTYPE.equals("B2"))
					str_After = "恢復扣款";
				else if (str_ALTERTYPE.equals("B1"))
					str_After = "暫停扣款";
				else if (str_ALTERTYPE.equals("B0"))
					str_After = "終止扣款";			
 				
 				str_line_content_1 = str_line_content_1 + str_Before + "<BR>" +  str_After;
				}


			final String str_line_title =str_line_title_1;			
			final String str_line_content =str_line_content_1;

				
			
			
			final String str_UID = (String)params.get("UID");	//ID
			final String str_DPMYEMAIL = (String)params.get("DPMYEMAIL");	//DPMYEMAIL
			final String str_CMTRMAIL = (String)params.get("CMTRMAIL");	//CMTRMAIL
			final String str_CMQTIME = helper.getValueByFieldName("CMQTIME");	//ID


			
			log.debug("str_RESPCOD = " + str_RESPCOD);
			log.debug("str_TRANSCODE = " + str_TRANSCODE);
			log.debug(ESAPIUtil.vaildLog("str_UID = " + str_UID));
			log.debug(ESAPIUtil.vaildLog("str_DPMYEMAIL = " + str_DPMYEMAIL));
			log.debug(ESAPIUtil.vaildLog("str_CMTRMAIL = " + str_CMTRMAIL));
			log.debug("str_CMQTIME = " + str_CMQTIME);
			
			if (str_DPMYEMAIL!= null && str_DPMYEMAIL.length()>0){
			OnLineTransferNotice trNotice = new OnLineTransferNotice();

			trNotice.sendNotice(new NoticeInfoGetter() {

				public NoticeInfo getNoticeInfo() {
				
					NoticeInfo result = new NoticeInfo(str_UID,str_DPMYEMAIL,str_CMTRMAIL);
					//範本			
					result.setTemplateName("FUNDNOTICE");
				    result.getParams().put("#TITLE", "基金定期投資約定變更通知");					

				    result.getParams().put("#TRANTIME", str_CMQTIME);
		
					result.getParams().put("#LINE_TITLE_1", "基金名稱");
					result.getParams().put("#LINE_CONTENT_1", " " + str_TRANSCODE + " " + str_FUNDLNAME );//str_FUNDLNAME);

					result.getParams().put("#LINE_TITLE_2", "類別");
					result.getParams().put("#LINE_CONTENT_2", str_MIP_);
					
					result.getParams().put("#LINE_TITLE_3", "變更項目");
					result.getParams().put("#LINE_CONTENT_3", str_ALTERTYPE_chk_);
					
					result.getParams().put("#LINE_TITLE_4", str_line_title);
					result.getParams().put("#LINE_CONTENT_4", str_line_content);
					
					return result;
				}
				
			});
			}//確認Email
			}//0000
			//發送成功Email 
		} //!= null
		
		
		return helper;
		
	}
}
