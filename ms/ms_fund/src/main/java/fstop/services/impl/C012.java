package fstop.services.impl;

import java.util.Date;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import com.netbank.rest.model.pool.FundUtils;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class C012 extends CommonService  {  
	private Logger logger = Logger.getLogger(getClass());
	@Autowired
    @Qualifier("c012Telcomm")
	private TelCommExec c012Telcomm;
	@Autowired
	private FundUtils fundUtils;
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public MVH doAction(Map params) { 

		params.put("TXNIDT", DateTimeUtils.format("yyMMddHHmmss", new Date()));
		
		MVHImpl result = new MVHImpl();
		TelcommResult helper = c012Telcomm.query(params);
		int ind = 1;
		int ind2 = 1;
		MVHImpl table1 = new MVHImpl();
		MVHImpl table2 = new MVHImpl();//台幣總報酬率
		MVHImpl table3 = new MVHImpl();
		for(int i=helper.getOccurs().getSize(); i > 0; i--) {
			Row r = helper.getOccurs().getRow(i-1);
			MVHImpl fundData = (MVHImpl)fundUtils.getFundData(r.getValue("TRANSCODE"));
			logger.debug("TRANSCODE" + r.getValue("TRANSCODE"));
			if(StrUtils.trim(fundData.getValueByFieldName("FUNDLNAME")).length() == 0) {
				if (r.getValue("TRANSCODE").equals("0000"))
				{					
					//SUBCRY
					table1.getFlatValues().put("SUBTOTAMT" + "_" + ind, r.getValue("TOTAMT") + "");
					table1.getFlatValues().put("SUBCRY" + "_" + ind, r.getValue("CRY") + "");
					table1.getFlatValues().put("SUBREFVALUE" + "_" + ind, r.getValue("REFVALUE") + "");
					table1.getFlatValues().put("SUBLCYRAT" + "_" + ind, r.getValue("LCYRAT") + "");
					table1.getFlatValues().put("SUBFCYRAT" + "_" + ind, r.getValue("FCYRAT") + "");
					table1.getFlatValues().put("SUBDIFAMT" + "_" + ind, r.getValue("DIFAMT"));
					table1.getFlatValues().put("SUBDIFAMT2" + "_" + ind, r.getValue("DIFAMT2"));
					logger.debug("SUBTRANSCODE:" + r.getValue("TRANSCODE") +", SUBTOTAMT:" + r.getValue("TOTAMT") + ", SUBCRY: " + r.getValue("CRY") + ", SUBREFVALUE: " + r.getValue("REFVALUE") + ", SUBDIFAMT:" + r.getValue("DIFAMT")+ ", SUBLCYRAT:" + r.getValue("LCYRAT") + ", SUBFCYRAT:" + r.getValue("FCYRAT") + ", SUBDIFAMT2:" + r.getValue("DIFAMT2"));
					ind++;
					helper.getOccurs().removeRow(r);
				}
				if (r.getValue("TRANSCODE").equals("0001"))
				{					
					//SUBCRY
					table2.getFlatValues().put("UNALLOTAMT" + "_" + ind2, r.getValue("TOTAMT"));
					table2.getFlatValues().put("UNALLOTAMTCRY" + "_" + ind2, r.getValue("CRY"));
					ind2++;
					logger.debug("UNTRANSCODE:" + r.getValue("TRANSCODE") +", UNALLOTAMT:" + r.getValue("TOTAMT") + ", UNALLOTAMTCRY: " + r.getValue("CRY"));
					helper.getOccurs().removeRow(r);
				}
				
				if (r.getValue("TRANSCODE").equals("0002"))
				{
					table3.getFlatValues().put("TOTTWDAMT", r.getValue("TOTAMT"));
					table3.getFlatValues().put("TOTCRY", r.getValue("CRY"));
					table3.getFlatValues().put("TOTREFVALUE", r.getValue("REFVALUE"));
					table3.getFlatValues().put("TOTLCYRAT", r.getValue("LCYRAT") );
					table3.getFlatValues().put("TOTFCYRAT", r.getValue("FCYRAT") );
					table3.getFlatValues().put("TOTDIFAMT", r.getValue("DIFAMT") );
					table3.getFlatValues().put("TOTDIFAMT2", r.getValue("DIFAMT2") );
					logger.debug("TOTTRANSCODE:" + r.getValue("TRANSCODE") +", TOTTWDAMT:" + r.getValue("TOTAMT") + ", TOTCRY: " + r.getValue("CRY") + ", TOTREFVALUE: " + r.getValue("REFVALUE") + ", TOTDIFAMT:" + r.getValue("DIFAMT")+ ", TOTLCYRAT:" + r.getValue("LCYRAT") + ", TOTFCYRAT:" + r.getValue("FCYRAT") + ", TOTDIFAMT2:" + r.getValue("DIFAMT2"));
					helper.getOccurs().removeRow(r);
				}
				//DB找不到基金代號對應的名稱->塞空白，不刪除
				r.setValue("FUNDLNAME"," ");
			}
			else {
				r.setValue("FUNDLNAME", fundData.getValueByFieldName("FUNDLNAME"));
			}
		}
		
		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		result.getFlatValues().put("CMRECNUM", helper.getOccurs()+"");
		result.getFlatValues().put("SHWD", helper.getValueByFieldName("SHWD").toString());
		result.addTable(helper, "資訊");
		result.addTable(table1, "各幣別合計");
		result.addTable(table2, "未分配");
		result.addTable(table3, "約當台幣總報酬");
		
		return result;
	}
}
