package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.util.ESAPIUtil;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.services.PagesAware;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class C013 extends CommonService implements PagesAware {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("c013Telcomm")
	private TelCommExec c013Telcomm;
	
//	@Required
//	public void setC013Telcomm(TelCommExec telcomm) {
//		c013Telcomm = telcomm;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 
		if(null != params.get("CDNO") && !params.get("CDNO").equals("")) {
			params.put("CREDITNO", params.get("CDNO"));
		}
		
		params.put("TXNIDT", DateTimeUtils.format("yyMMddHHmmss", new Date()));

		String stadate = (params.get("BEGDATE") != null) ? (String)params.get("BEGDATE") : "00000000";  // 民國年月日
		String enddate = (params.get("ENDDATE") != null) ? (String)params.get("ENDDATE") : "99999999";  // 民國年月日
		
		stadate = StrUtils.trim(stadate).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
		enddate = StrUtils.trim(enddate).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
		
		params.put("BEGDATE", stadate);
		params.put("ENDDATE", enddate);

		log.debug(ESAPIUtil.vaildLog("@@@ C013 BEGDATE == " + params.get("BEGDATE")));
		log.debug(ESAPIUtil.vaildLog("@@@ C013 ENDDATE == " + params.get("ENDDATE")));

		if (! stadate.equals("")) {	
			Date dstart = DateTimeUtils.getCDate2Date(stadate);
			stadate = DateTimeUtils.format("yyyyMMdd", dstart);
		}

		if (! enddate.equals("")) {	
			Date dend = DateTimeUtils.getCDate2Date(enddate);
			enddate = DateTimeUtils.format("yyyyMMdd", dend);
		} 					
		
		String periodStr = "";
		if(stadate != null && stadate.length() == 8) {
			Date sd = DateTimeUtils.parse("yyyyMMdd", stadate);
			periodStr = DateTimeUtils.format("yyyy/MM/dd", sd);
		}
		if(StrUtils.isNotEmpty(enddate) && enddate.length() == 8) {
			Date sd = DateTimeUtils.parse("yyyyMMdd", enddate);
			periodStr += "~" + DateTimeUtils.format("yyyy/MM/dd", sd);
		}
		if(StrUtils.isEmpty(enddate)) { //若未指定結束時間, 則使用今天日期
			enddate = DateTimeUtils.format("yyyyMMdd", new Date());
			periodStr += "~" + DateTimeUtils.format("yyyy/MM/dd", new Date());
		}

		params.put("RECNO", "300");
		
		//TODO RECNO 讀取筆數不知道要不要塞
		
		TelcommResult telcommResult = c013Telcomm.query(params);
		telcommResult.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		telcommResult.getFlatValues().put("CMRECNUM", telcommResult.getValueOccurs("CREDITNO") + "");
		telcommResult.getFlatValues().put("CMPERIOD", periodStr);


		return telcommResult;
	}


}
