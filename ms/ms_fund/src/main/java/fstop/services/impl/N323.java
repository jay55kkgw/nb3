package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.util.ESAPIUtil;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class N323 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("n323Telcomm")
	private TelCommExec n323Telcomm;

//	@Required
//	public void setN323Telcomm(TelCommExec telcomm) {
//		n323Telcomm = telcomm;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params) { 

		Map<String, String> params = _params;
		params.put("CUSIDN", params.get("UID"));
	
		String UID=params.get("UID");
		String TYPE=params.get("TYPE")==null ? "" : params.get("TYPE");
        String AGE=params.get("AGE")==null ? "" : params.get("AGE");
        String MARK1=params.get("MARK1")==null ? "" : params.get("MARK1");
        String MARK3=params.get("MARK3")==null ? "" : params.get("MARK3");
        String DEGREE=params.get("DEGREE")==null ? "" : params.get("DEGREE");
        String RISK7=params.get("RISK7")==null ? "" : params.get("RISK7");

        log.debug(ESAPIUtil.vaildLog("UID:"+UID+" TYPE:"+TYPE+" AGE:"+AGE+" MARK1:"+MARK1+" MARK3:"+MARK3+" DEGREE:"+DEGREE+" RISK7:"+RISK7));
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);
		TelcommResult telcommResult = null;
		if(UID.length()==10 && TYPE.equals("01"))
		{	
			params.put("DEGREE", params.get("DEGREE"));
			params.put("MARK1", params.get("MARK1"));
		}
		else
		{
			params.put("DEGREE", "");
			params.put("MARK1", "");
			
		}
		telcommResult = n323Telcomm.query(params);
		
		return telcommResult;		
	}
}
