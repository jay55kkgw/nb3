package fstop.services.impl;

import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class C113 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("c113Telcomm")
	private TelCommExec c113Telcomm;
	
//	@Required
//	public void setC113Telcomm(TelCommExec telcomm) {
//		c113Telcomm = telcomm;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 

		params.put("TXNIDT", DateTimeUtils.format("yyMMddHHmmss", new Date()));
//		String[] chgs = new String[] {
//				"NZIPCODE",
//				"NADDRESS",
//				"NBILLSENDMODE",
//				"NHTELPHONE",
//				"NOTELPHONE",
//				"NMTELPHONE",
//			};
//			//
//			//把前頭的 N 去掉, 變成 KEY 塞回 params
//			//
//			for(String s : chgs) {
//				params.put(s.substring(1), params.get(s));
//			}
		
		TelcommResult helper = c113Telcomm.query(params);
		helper.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));

		return helper;
	}


}
