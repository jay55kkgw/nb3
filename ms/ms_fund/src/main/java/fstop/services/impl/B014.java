package fstop.services.impl;

import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.services.PagesAware;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
@Slf4j
public class B014 extends CommonService implements PagesAware {
	private Logger logger = Logger.getLogger(getClass());
	
	@Autowired
    @Qualifier("b014Telcomm")
	private TelCommExec b014Telcomm;
	
//	@Required
//	public void setB014Telcomm(TelCommExec telcomm) {
//		b014Telcomm = telcomm;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 
		params.put("TXNIDT", DateTimeUtils.format("yyMMddHHmmss", new Date()));

		String stadate = "00000000";
		String enddate = "21000000";
		
		params.put("BEGDATE", stadate);
		params.put("ENDDATE", enddate);

		log.info("@@@ B014 BEGDATE >>{}", stadate);
		log.info("@@@ B014 ENDDATE >>{}", enddate);

		params.put("RECNO", "300");
		
		//TODO RECNO 讀取筆數不知道要不要塞
		params.put("CREDITNO", params.get("CDNO"));
		TelcommResult telcommResult = b014Telcomm.query(params);
		telcommResult.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		telcommResult.getFlatValues().put("CMRECNUM", telcommResult.getValueOccurs("CREDITNO") + "");

		log.info("@@@ B014 ENDDDDD");
		
		return telcommResult;
	}

}
