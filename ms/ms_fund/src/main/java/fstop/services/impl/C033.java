package fstop.services.impl;

import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.rest.model.CommonPools;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.orm.dao.TxnFundTraceLogDao;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;
@Slf4j
public class C033 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("c024Telcomm")
	private TelCommExec c024Telcomm;
	@Autowired
	@Qualifier("n922Telcomm")
	private TelCommExec n922Telcomm;
	@Autowired
	private TxnFundTraceLogDao txnFundTraceLogDao;
	@Autowired
	private CommonPools commonPools;


//	@Required
//	public void setC024Telcomm(TelCommExec telcomm) {
//		c024Telcomm = telcomm;
//	}
//
//	@Required
//	public void setN922Telcomm(TelCommExec telcomm) {
//		n922Telcomm = telcomm;
//	}
//
//
//
//
//	@Required
//	public void setTxnFundTraceLogDao(TxnFundTraceLogDao txnFundTraceLogDao) {
//		this.txnFundTraceLogDao = txnFundTraceLogDao;
//	}

	
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 
		params.put("TXNIDT", DateTimeUtils.format("yyMMddHHmmss", new Date()));


		//TODO Write TraceLog
		
		TelcommResult helper = c024Telcomm.query(params);

		helper.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);
		
		//TelcommResult helperN922 = n922Telcomm.query(params);
		commonPools.n922.removeGetDataCache((String)params.get("UID"));
		TelcommResult helperN922 = (TelcommResult)commonPools.n922.getData((String)params.get("UID"));
		
		helper.getFlatValues().putAll(helperN922.getFlatValues());
		
		return helper;
	}


}
