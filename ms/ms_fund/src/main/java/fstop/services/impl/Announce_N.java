package fstop.services.impl;

import java.util.Date;
import java.util.Hashtable;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;

import fstop.aop.advice.LoggerHelper;
import fstop.aop.advice.LoggerObject;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.TxnFundTraceLogDao;
import fstop.orm.po.TXNFUNDTRACELOG;
import fstop.services.CommonService;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class Announce_N extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	private TxnFundTraceLogDao txnFundTraceLogDao;
	
//	@Required
//	public void setTxnFundTraceLogDao(TxnFundTraceLogDao txnFundTraceLogDao) {
//		this.txnFundTraceLogDao = txnFundTraceLogDao;
//	}

	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params) { 
		Map<String, String> params = _params;
		MVHImpl result = new MVHImpl();
		boolean includeADTXNO = StrUtils.isNotEmpty(params.get("ADTXNO"));
		
		TXNFUNDTRACELOG logobj = new TXNFUNDTRACELOG();

		Date d = new Date();
		if(includeADTXNO)
			logobj.setADTXNO(StrUtils.trim(params.get("ADTXNO")));
		else
			logobj.setADTXNO(UUID.randomUUID().toString());

		logobj.setFDUSERID(StrUtils.trim(params.get("UID")));
		logobj.setFDTXTIME(DateTimeUtils.format("yyyyMMddHHmmss", d));
		logobj.setFDAGREEFLAG(StrUtils.trim(params.get("FDAGREEFLAG")));
		logobj.setFDNOTICETYPE(StrUtils.trim(params.get("FDNOTICETYPE")));
		logobj.setFDPUBLICTYPE(StrUtils.trim(params.get("FDPUBLICTYPE")));
				
		Hashtable htl_Data = new Hashtable();
		htl_Data.put("FUNDLNAME", params.get("DISPLAYNAME"));
		htl_Data.put("TRANSCODE", params.get("DISPLAYCODE"));	
		htl_Data.put("BRHCOD", params.get("BRHCOD"));	
		
		logobj.setFDTXLOG(JSONUtils.map2json(htl_Data));
		
		LoggerObject obj = LoggerHelper.current();
		logobj.setADGUID(obj.getGUID());
		txnFundTraceLogDao.save(logobj);

		result.getFlatValues().put("ADTXNO", logobj.getADTXNO());
		return result;	
	}


}
