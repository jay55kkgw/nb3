package fstop.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.rest.model.pool.N920Pool;
import com.netbank.util.AcnoTypeEnum;
import com.netbank.util.ESAPIUtil;
import com.netbank.rest.model.CommonPools;

import fstop.exception.TopMessageException;
import fstop.model.EachRowCallback;
import fstop.model.IMasterValuesHelper;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.MVHUtils;
import fstop.model.Row;
import fstop.model.Rows;
import fstop.model.RowsSelecter;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N870 extends CommonService {
	// private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("n870Telcomm")
	private TelCommExec n870Telcomm;
	@Autowired
	private CommonPools commonPools;

	// @Required
	// public void setN870Telcomm(TelCommExec telcomm) {
	// n870Telcomm = telcomm;
	// }

	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) {

		// 但在電文的格式裡, 沒有 repeat 帳號(ACN)
		MVHImpl result = new MVHImpl();
		queryAll(getAcnoList(params), params, result);
		log.debug(result.toString());
		// if(StrUtils.isEmpty((String)params.get("ACN"))) {
		// //查詢全部帳號
		// queryAll(params, result);
		// }
		// else {
		// MVHImpl resultN870 = n870Telcomm.query(params);;
		// final String acn = result.getFlatValues().get("ACN");
		// resultN870.getOccurs().setValue("ACN", acn);
		//
		// result.addTable(resultN870, acn);
		// }

		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		result.getFlatValues().put("CMRECNUM", MVHUtils.sumTablesOccurs(result) + "");

		// Map<String, IMasterValuesHelper> tables = MVHUtils.groupByToTables(result,
		// new String[]{"ACN"});
		// MVHUtils.addTables(result, tables);

		return result;
	}

	private List<String> getAcnoList(Map<String, String> params) {
		List<String> acnos = new ArrayList();
		if (StrUtils.isEmpty((String) params.get("ACN1"))) {
			IMasterValuesHelper acnosMVH = commonPools.n920.getAcnoList(params.get("CUSIDN"), new String[] { "08" }, "");

			int requestCount = acnosMVH.getValueOccurs("ACN");
			for (int i = 0; i < requestCount; i++) {
				String acn = acnosMVH.getValueByFieldName("ACN", i + 1);
				acnos.add(acn);
			}
		} else {
			if (params.get("ACN1").substring(3, 5).equals("97"))
				acnos.add(params.get("ACN1"));
			else {
				IMasterValuesHelper acnosMVH = commonPools.n920.getAcnoList(params.get("UID"), new String[] { "08" }, "");

				int requestCount = acnosMVH.getValueOccurs("ACN");

				for (int i = 0; i < requestCount; i++) {
					String acn = acnosMVH.getValueByFieldName("ACN", i + 1);
					acnos.add(acn);
				}
			}
		}
		log.debug(ESAPIUtil.vaildLog("AcnoList >> {}"+ acnos));
		return acnos;
	}

	public void queryAll(List<String> acnoList, Map<String, String> params, MVHImpl resultMVH) {
		// IMasterValuesHelper acnos = CommonPools.n920.getAcnoList(params.get("UID"),
		// new String[]{"08"}, "");

		// Rows rows = new Rows();
		// int requestCount = acnos.getValueOccurs("ACN");
		for (String acn : acnoList) {
			// String acn = acnos.getValueByFieldName("ACN", i+1);
			params.put("ACN1", acn);
			try {
				MVHImpl result = n870Telcomm.query(params);
				result.getOccurs().setValue("ACN1", acn);
				resultMVH.addTable(result, acn);
				// 20190401 JeffChang 為了得到回傳值
				resultMVH.getFlatValues().putAll(result.getFlatValues());
			} catch (TopMessageException ex) {
				MVHImpl helper = new MVHImpl();
				helper.getFlatValues().put("ACN1", acn);
				helper.getFlatValues().put("TOPMSG", ex.getMsgcode());
				helper.getFlatValues().put("ADMSGIN", ex.getMsgin());
				helper.getFlatValues().put("ADMSGOUT", ex.getMsgout());
				resultMVH.addTable(helper, acn);
			}
		}
		// return new MVHImpl(rows);
	}

}
