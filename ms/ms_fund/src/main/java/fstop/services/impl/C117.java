package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;
import com.netbank.rest.model.pool.FundUtils;


@Slf4j
public class C117 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("c117Telcomm")
	private TelCommExec c117Telcomm;
	
//	@Required
//	public void setC117Telcomm(TelCommExec telcomm) {
//
//		c117Telcomm = telcomm;
//	}

	@Autowired
	private FundUtils fundUtils;
	
//	@Required
//	public void setFundUtils(FundUtils fundUtils) {
//		this.fundUtils = fundUtils;
//	}

	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 
		if(null != params.get("CDNO") && !params.get("CDNO").equals("")) {
			params.put("CREDITNO", params.get("CDNO"));
		}

		params.put("TXNIDT", DateTimeUtils.format("yyMMddHHmmss", new Date()));

		TelcommResult helper = c117Telcomm.query(params);
		helper.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		helper.getFlatValues().put("CDNO", helper.getFlatValues().get("CREDITNO"));


		return helper;
	}


}
