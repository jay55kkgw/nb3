package fstop.services.impl;

import java.util.Date;
import java.util.Hashtable;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import fstop.aop.advice.LoggerHelper;
import fstop.aop.advice.LoggerObject;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.orm.dao.SysDailySeqDao;
import fstop.orm.dao.TxnFundTraceLogDao;
import fstop.orm.po.TXNFUNDTRACELOG;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
@Slf4j
public class C032_Public extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	private TxnFundTraceLogDao txnFundTraceLogDao;
	@Autowired
	@Qualifier("c032Telcomm")
	private TelCommExec c032Telcomm;
	@Autowired
	private SysDailySeqDao sysDailySeqDao;


//	@Required
//	public void setSysDailySeqDao(SysDailySeqDao sysDailySeqDao) {
//		this.sysDailySeqDao = sysDailySeqDao;
//	}
//	
//
//	@Required
//	public void setC032Telcomm(TelCommExec telcomm) {
//		c032Telcomm = telcomm;
//	}
//
//	@Required
//	public void setTxnFundTraceLogDao(TxnFundTraceLogDao txnFundTraceLogDao) {
//		this.txnFundTraceLogDao = txnFundTraceLogDao;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params) { 
		Map<String, String> params = _params;
		if(null != params.get("CDNO") && !params.get("CDNO").equals("")) {
			params.put("CREDITNO", params.get("CDNO"));
		}
		params.put("TXNIDT", DateTimeUtils.format("yyMMddHHmmss", new Date()));
		params.put("SYSTRACE",
				StrUtils.right(StrUtils.repeat("0", 7) + sysDailySeqDao.dailySeq("C032")+"", 7));

		//TODO Write Trace LOG
		TelcommResult helper = null;
		try {
			helper = c032Telcomm.query(params);
			helper.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
			helper.getFlatValues().put("CDNO", helper.getFlatValues().get("CREDITNO"));
		}
		finally {
			boolean includeADTXNO = StrUtils.isNotEmpty(params.get("ADTXNO"));
			
			TXNFUNDTRACELOG logobj = new TXNFUNDTRACELOG();

			Date d = new Date();
			if(includeADTXNO)
				logobj.setADTXNO(StrUtils.trim(params.get("ADTXNO")));
			else
				logobj.setADTXNO(UUID.randomUUID().toString());

			logobj.setFDUSERID(StrUtils.trim(params.get("UID")));
			logobj.setFDTXTIME(DateTimeUtils.format("yyyyMMddHHmmss", d));
			logobj.setFDAGREEFLAG(StrUtils.trim(params.get("FDAGREEFLAG")));
			logobj.setFDNOTICETYPE(StrUtils.trim(params.get("FDNOTICETYPE")));
			logobj.setFDPUBLICTYPE(StrUtils.trim(params.get("FDPUBLICTYPE")));
			LoggerObject obj = LoggerHelper.current();
			logobj.setADGUID(obj.getGUID());
			txnFundTraceLogDao.save(logobj);
			
			if(helper != null) {
				helper.getFlatValues().put("ADTXNO", logobj.getADTXNO());
				helper.getFlatValues().put("XFLAG", params.get("FDPUBLICTYPE"));
			}
		}
		
		return helper;
	}


}
