package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import topibs.utility.NumericUtil;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.services.NoticeInfo;
import fstop.services.NoticeInfoGetter;
import fstop.services.OnLineTransferNotice;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

import com.netbank.rest.model.pool.FundUtils;
import com.netbank.util.ESAPIUtil;



@Slf4j
public class C118 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("c118Telcomm")
	private TelCommExec c118Telcomm;
	
//	@Required
//	public void setC118Telcomm(TelCommExec telcomm) {
//		c118Telcomm = telcomm;
//	}

	@Autowired
	private FundUtils fundUtils;

//	@Required
//	public void setFundUtils(FundUtils fundUtils) {
//		this.fundUtils = fundUtils;
//	}
	
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 
		params.put("TXNIDT", DateTimeUtils.format("yyMMddHHmmss", new Date()));
		
		TelcommResult helper = c118Telcomm.query(params);
		helper.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));

		//發送成功Email params
		if(helper != null){
			final String str_RESPCOD = helper.getValueByFieldName("RESPCOD");	//回應訊息
			
			if (str_RESPCOD.equals("0000")){
			final String str_TRANSCODE = helper.getValueByFieldName("TRANSCODE");	//基金代碼
			//MVHImpl fundData = (MVHImpl)helper.getFundData(helper.getValueByFieldName("TRANSCODE"));
			//final String str_FUNDLNAME = fundData.getValueByFieldName("FUNDLNAME");//基金名稱
			final String str_AMT3 = NumericUtil.formatNumberString(helper.getValueByFieldName("AMT3"),0);	//申購金額
			final String str_CUR = helper.getValueByFieldName("CUR");	//幣別
			final String str_UID = (String)params.get("UID");	//ID
			final String str_DPMYEMAIL = (String)params.get("DPMYEMAIL");	//DPMYEMAIL
			final String str_CMTRMAIL = (String)params.get("CMTRMAIL");	//CMTRMAIL
			final String str_CMQTIME = helper.getValueByFieldName("CMQTIME");	//ID

			final String str_FCA2 = NumericUtil.formatNumberString(helper.getValueByFieldName("FCA2"), 2);	//手續費
			final String str_AMT5 = NumericUtil.formatNumberString(helper.getValueByFieldName("AMT5"), 2);	//扣款金額

			
			log.debug("str_RESPCOD = " + str_RESPCOD);
			log.debug("str_TRANSCODE = " + str_TRANSCODE);
			log.debug("str_AMT3 = " + str_AMT3);
			//logger.debug("str_FUNDLNAME = " + str_FUNDLNAME);
			log.debug("str_CUR = " + str_CUR);
			log.debug(ESAPIUtil.vaildLog("str_UID = " + str_UID));
			log.debug(ESAPIUtil.vaildLog("str_DPMYEMAIL = " + str_DPMYEMAIL));
			log.debug(ESAPIUtil.vaildLog("str_CMTRMAIL = " + str_CMTRMAIL));
			log.debug("str_CMQTIME = " + str_CMQTIME);
			log.debug("str_FCA2 = " + str_FCA2);
			log.debug("str_AMT5 = " + str_AMT5);
			
			
			OnLineTransferNotice trNotice = new OnLineTransferNotice();

			trNotice.sendNotice(new NoticeInfoGetter() {

				public NoticeInfo getNoticeInfo() {
				
					NoticeInfo result = new NoticeInfo(str_UID,str_DPMYEMAIL,str_CMTRMAIL);
					//範本			
					result.setTemplateName("FUNDNOTICE");
				    result.getParams().put("#TITLE", "基金停損/停利通知設定通知");					

					
					//result.setDpretxstatus(record.getDPRETXSTATUS());
					//result.setADTXNO(record.getADTXNO());
						
					//result.setException(execwrapperFinal.getLastException());
					//String datetime = record.getDPTXDATE() + record.getDPTXTIME();

					//Date d = DateTimeUtils.parse("yyyyMMddHHmmss", datetime);
					//String trantime = DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", d);
				    result.getParams().put("#TRANTIME", str_CMQTIME);
		
					result.getParams().put("#LINE_TITLE_1", "基金名稱");
					result.getParams().put("#LINE_CONTENT_1", "(" + str_TRANSCODE + ")" );//str_FUNDLNAME);

					result.getParams().put("#LINE_TITLE_2", "申購金額");
					result.getParams().put("#LINE_CONTENT_2", str_CUR + " " + str_AMT3);
					
					result.getParams().put("#LINE_TITLE_3", "手續費");
					result.getParams().put("#LINE_CONTENT_3", str_FCA2);
						
					result.getParams().put("#LINE_TITLE_4", "扣款金額");
					result.getParams().put("#LINE_CONTENT_4", str_AMT5);
					
					result.getParams().put("#LINE_TITLE_5", "幣別");
					result.getParams().put("#LINE_CONTENT_5", str_CUR);
					
					
					
					//ADMMAILLOG admmaillog = new ADMMAILLOG();
					//admmaillog.setADACNO(record.getDPWDAC());
					/*
					String trans_status = (String)params.get("__TRANS_STATUS");
					if(StrUtils.isEmpty(trans_status)) {
						trans_status = "TWONLINE";
					}
					else if("SEND".equals(trans_status)) {
						trans_status = "TWSCH";
					}
					else if("RESEND".equals(trans_status)) {
						trans_status = "TWSCHRE";
					}
					
					admmaillog.setADBATCHNO(trans_status);
					admmaillog.setADMAILACNO(StrUtils.trim(params.get("CMTRMAIL")));
					admmaillog.setADUSERID(record.getDPUSERID());
					admmaillog.setADUSERNAME("");
					admmaillog.setADSENDTYPE("3");
					admmaillog.setADSENDTIME(datetime);
					admmaillog.setADSENDSTATUS("");
					
					result.setADMMAILLOG(admmaillog);
					*/
					return result;
				}
				
			});

			}//0000
			
		} //!= null		
		
		
		return helper;
	}


}
