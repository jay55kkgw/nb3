package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class C014 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("c014Telcomm")
	private TelCommExec c014Telcomm;
	
//	@Required
//	public void setC014Telcomm(TelCommExec telcomm) {
//		c014Telcomm = telcomm;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 
		params.put("CREDITNO", params.get("CDNO"));
		params.put("TXNIDT", DateTimeUtils.format("yyMMddHHmmss", new Date()));

		TelcommResult telcommResult = c014Telcomm.query(params);
		telcommResult.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		telcommResult.getFlatValues().put("CDNO", telcommResult.getFlatValues().get("CREDITNO"));

		return telcommResult;
	}


}
