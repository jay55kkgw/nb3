package com.netbank.rest.model;

public interface NewOneCallback {
	public Object getNewOne();
}
