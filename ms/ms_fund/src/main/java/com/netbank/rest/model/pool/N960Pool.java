package com.netbank.rest.model.pool;

import java.util.Date;
import java.util.Hashtable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.rest.model.NewOneCallback;
import com.netbank.rest.model.UserPool;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;

@Component
public class N960Pool extends UserPool {

	@Autowired
    @Qualifier("n960Telcomm")
	private TelCommExec n960Telcomm;
	
	
//	@Required
//	public void setN960Telcomm(TelCommExec telcomm) {
//		n960Telcomm = telcomm;
//	}

	
	private NewOneCallback getNewOneCallback(final String uid) {
		NewOneCallback callback = new NewOneCallback() {

			public Object getNewOne() {

				Hashtable params = new Hashtable();
				params.put("CUSIDN", uid);
				Date d = new Date();
				String df = DateTimeUtils.getCDateShort(d);
				String tf = DateTimeUtils.format("HHmmss", d);
				params.put("DATE", df);
				params.put("TIME", tf);
				
				TelcommResult mvh = n960Telcomm.query(params);
				
				return mvh;
			}
			
		};		
		
		return callback;
	}
	
	/**
	 * N960客戶基本資料查詢
	 * @param uid  使用者的身份證號
	 * @return
	 */
	public MVH getData(final String idn) {
		final String uid = StrUtils.trim(idn).toUpperCase();
		if(uid.length() == 0)
			return new MVHImpl();
		
		return (MVH)getPooledObject(uid, "N960.getData", getNewOneCallback(uid));
		//return (MVH)getNewOneCallback(uid).getNewOne();

	}
	public MVH getData1(final String idn) {  //AML使用
		final String uid = StrUtils.trim(idn).toUpperCase();
		if(uid.length() == 0)
			return new MVHImpl();
		
		return (MVH)getPooledObject(uid, "N960.getData", getNewOneCallback1(uid));
		//return (MVH)getNewOneCallback(uid).getNewOne();

	}
	private NewOneCallback getNewOneCallback1(final String uid) {
		NewOneCallback callback = new NewOneCallback() {

			public Object getNewOne() {

				Hashtable params = new Hashtable();
				params.put("CUSIDN", uid);
				Date d = new Date();
				String df = DateTimeUtils.getCDateShort(d);
				String tf = DateTimeUtils.format("HHmmss", d);
				params.put("DATE", df);
				params.put("TIME", tf);
				params.put("NCHECKNB", "Y");//不檢查網銀身份
				TelcommResult mvh = n960Telcomm.query(params);
				
				return mvh;
			}
			
		};		
		
		return callback;
	}	
	public void removeGetDataCache(final String idn) {
		this.removePoolObject(idn, "N960.getData");
	}
	
}
