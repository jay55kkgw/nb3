package com.netbank.rest.model.pool;

import java.util.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.rest.model.NewOneCallback;
import com.netbank.rest.model.Pool;

import fstop.model.DBResult;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.model.Rows;
import fstop.orm.dao.AdmCardDataDao;
import fstop.orm.dao.TxnCardApplyDao;
import fstop.orm.po.ADMCARDDATA;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import fstop.model.IMasterValuesHelper;
import topibs.utility.NumericUtil;
import fstop.core.BeanUtils;
import fstop.exception.ToRuntimeException;

@Component
public class CreditUtils extends Pool {
	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	private AdmCardDataDao admCardDataDao;
	@Autowired
	private TxnCardApplyDao txnCardApplyDao;
			
//	@Required
//	public void setAdmCardDataDao(AdmCardDataDao dao) {
//		this.admCardDataDao = dao;
//	}
//	
//	@Required
//	public void setTxnCardApplyDao(TxnCardApplyDao dao) {
//		this.txnCardApplyDao = dao;
//	}
	/**
	 * 取得卡片名稱，依據CARDNO
	 * @param 
	 * @return
	 */
	public String getCardNameByCardno(final String cardno) {
		String result = admCardDataDao.findCARDNAME(cardno);
		return result;
	}		

	/**
	 * 查詢一般卡全部資料
	 * @param 
	 * @return
	 */
	public MVH getNormalCard() {

		try {
			
			NewOneCallback newoneCallback = new NewOneCallback() {

				public Object getNewOne() {										
						// 取得全部
						List all = admCardDataDao.getNormalCard();
						return new DBResult(all);
				}
			};
			
			return (MVH)getPooledObject("getNormalCard()", newoneCallback);
		} catch (Exception e) {
			logger.warn("無法取得卡別資料.", e);
		}
		return new MVHImpl();
	}
		
	/**
	 * 取得卡片註記
	 * @param 
	 * @return
	 */
	public String getCardMemoByCardno(final String cardno) {
		String result = admCardDataDao.findCARDMEMO(cardno);
		return result;
	}	
	
	/**
	 * 依據舊戶已申請過的卡片，取得不含已申請過卡片的群組
	 * @param 
	 * @return
	 */
	public MVH getOtherCardGroup(final String cardtype) {
		logger.info("CreditUtil.java cardtype==>"+cardtype);		
		MVHImpl result = null;
		Rows rows = admCardDataDao.getOtherCardGroupDATA(cardtype);		
		Rows newrows=new Rows();
		for(int i =0; i < rows.getSize();i++){			
			newrows.addRow(rows.getRow(i));
		}		
		result = new MVHImpl(newrows);
		
		return result;	
	}
	
	/**
	 * 舊戶已申請過的卡片取得歸戶行
	 * @param 
	 * @return
	 */
	public MVH getBranchNo(final String uid) {
		logger.info("CreditUtil.java uid==>"+uid);		
		MVHImpl result = null;
		Rows rows = txnCardApplyDao.findBranchNo(uid);		
		Rows newrows=new Rows();
		for(int i =0; i < rows.getSize();i++){			
			newrows.addRow(rows.getRow(i));
		}		
		result = new MVHImpl(newrows);
		
		return result;	
	}
}
