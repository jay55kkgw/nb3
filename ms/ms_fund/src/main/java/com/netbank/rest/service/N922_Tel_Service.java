package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netbank.rest.model.CommonPools;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import lombok.extern.slf4j.Slf4j;

/**
 * N922電文POOL
 */
@Service
@Slf4j
public class N922_Tel_Service extends Common_Tel_Service {

	@Autowired
	private CommonPools commonPools;

	@Override
	public HashMap pre_Processing(Map request) {
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) {
		MVH mvh = null;
		try {
			pre_Processing(request);
			String  UID = (String) request.get("CUSIDN");
			commonPools.n922.removeGetDataCache(UID);

			mvh = new MVHImpl();
			mvh = commonPools.n922.getData(UID);

		} catch (TopMessageException e) {
			// TODO Auto-generated catch block
			log.error("process.error>>", e);
			MVHImpl m = new MVHImpl();
			m.getFlatValues().put("TOPMSG", e.getMsgcode());
			mvh = m;
		} catch (Exception e) {
			log.error("process.error>>{}", e);
			throw new RuntimeException(e);
		} finally {
			// TODO trur 做data_Retouch false 做msgCode 回覆處理
			return data_Retouch(mvh);
		}
	}

	@Override
	public HashMap data_Retouch(MVH mvh) {
		return super.data_Retouch(mvh);
	}
}
