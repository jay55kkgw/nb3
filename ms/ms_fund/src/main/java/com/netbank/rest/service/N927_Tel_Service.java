package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netbank.rest.model.CommonPools;
import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import lombok.extern.slf4j.Slf4j;

/**
 * N927電文POOL
 */
@Service
@Slf4j
public class N927_Tel_Service extends Common_Tel_Service{

	@Autowired
	private CommonPools commonPools;

	@Override
	public HashMap pre_Processing(Map request){
		return super.pre_Processing(request);
	}
	@Override
	public HashMap process(String serviceID,Map request){
		MVH mvh = new MVHImpl();
		try 
		{
			String UID = (String)request.get("CUSIDN");
			log.debug(ESAPIUtil.vaildLog("UID={}"+UID));
			
			commonPools.n927.removeGetDataCache(UID);
			
			mvh = commonPools.n927.getData(UID);
			
		} 
		catch (TopMessageException e) 
		{
			log.error("process.error", e);
			MVHImpl m = new MVHImpl();
			m.getFlatValues().put("TOPMSG", e.getMsgcode());
			mvh = m;
		} 
		catch (Exception e) 
		{
			log.error("process.error", e);
			throw new RuntimeException(e);
		}
		finally 
		{
			return data_Retouch(mvh);
		}
	}
	
	@Override
	public HashMap data_Retouch(MVH mvh){
		return super.data_Retouch(mvh);
	}
	
	@Override
	public void setMsgCode(Map dataMap, Boolean data_Retouch_result) {
		super.setMsgCode(dataMap, data_Retouch_result);
	}
}
