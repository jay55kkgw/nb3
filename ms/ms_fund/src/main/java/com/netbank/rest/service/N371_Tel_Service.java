package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.netbank.util.ESAPIUtil;

import fstop.model.MVH;
import lombok.extern.slf4j.Slf4j;

/**
 * 基金轉換交易
 */
@Service
@Slf4j
public class N371_Tel_Service extends Common_Tel_Service{

	@Override
	public HashMap pre_Processing(Map request){
		return super.pre_Processing(request);
	}
	@Override
	public HashMap process(String serviceID,Map request){
		String CUSIDN = (String)request.get("CUSIDN");
		log.debug(ESAPIUtil.vaildLog("CUSIDN={}"+CUSIDN));
		request.put("UID",CUSIDN);
		
		return super.process(serviceID,request);
	}
	@Override
	public HashMap data_Retouch(MVH mvh){
		return super.data_Retouch(mvh);
	}
}