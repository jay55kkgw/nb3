package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;
import fstop.model.MVH;
import lombok.extern.slf4j.Slf4j;

/**
 * 基金預約轉換交易
 */
@Service
@Slf4j
public class N373_Tel_Service extends Common_Tel_Service{

	@Override
	public HashMap pre_Processing(Map request){
		return super.pre_Processing(request);
	}
	@Override
	public HashMap process(String serviceID,Map request){
		return super.process(serviceID,request);
	}
	@Override
	public HashMap data_Retouch(MVH mvh){
		return super.data_Retouch(mvh);
	}
}