package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.stereotype.Service;

import com.netbank.util.ESAPIUtil;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import lombok.extern.slf4j.Slf4j;

/**
 * 預約申購基金手續費查詢
 */
@Service
@Slf4j
public class C031_public_Tel_Service extends Common_Tel_Service  {

	@Override
	public HashMap pre_Processing(Map request) {
		// TODO Auto-generated method stub
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) {
		String CUSIDN = (String)request.get("CUSIDN");
		log.debug(ESAPIUtil.vaildLog("CUSIDN={}"+CUSIDN));
		request.put("UID",CUSIDN);
		
		return super.process(serviceID, request);
	}

	@Override
	public HashMap data_Retouch(MVH mvh) {
		MVHImpl mvhimpl = null;
		Boolean result = Boolean.FALSE;
		HashMap<String,Object> dataMap = new HashMap<String,Object>();
    	try{
    		mvhimpl = (MVHImpl)mvh;
	    	log.debug("mvhimpl.getFlatValues()={}",mvhimpl.getFlatValues());
			
	    	//將所有KEY有"-"改成"_"
	    	Map<String,String> mvhMap = mvhimpl.getFlatValues();
	    	Map<String,String> newMVHMap = new HashMap<String,String>();
	    	
	    	for(Entry<String,String> entry : mvhMap.entrySet()){
	    		String key = entry.getKey();
	    		log.debug("key={}",key);
	    		String value = entry.getValue();
	    		log.debug("value={}",value);
	    		
	    		if(key.contains("-")){
	    			key = key.replaceAll("-","_");
	    			log.debug("key={}",key);
	    			newMVHMap.put(key,value);
	    		}
	    		else{
	    			newMVHMap.put(key,value);
	    		}
	    	}
			dataMap.putAll(newMVHMap);
			result = Boolean.TRUE;
		}
    	catch(Exception e){
    		log.error("data_Retouch error >> {}",e);
		}
    	finally{
			setMsgCode(dataMap,result);
		}
		return dataMap;
	}
	
	@Override
	public void setMsgCode(Map dataMap, Boolean data_Retouch_result) {
		// TODO Auto-generated method stub
		super.setMsgCode(dataMap, data_Retouch_result);
	}
	
}
