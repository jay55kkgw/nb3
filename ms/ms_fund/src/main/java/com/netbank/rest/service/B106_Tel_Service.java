package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netbank.rest.model.CommonPools;
import com.netbank.util.AcnoTypeEnum;
import com.netbank.util.ESAPIUtil;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class B106_Tel_Service extends Common_Tel_Service {

	@Autowired
	private CommonPools commonPools;

	@Override
	public HashMap pre_Processing(Map request) {
		// TODO Auto-generated method stub
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) {
		MVH mvh = new MVHImpl();
		String type = String.valueOf(request.get("TYPE"));
		String cid = String.valueOf(request.get("CUSIDN"));
		log.debug(ESAPIUtil.vaildLog("cid : {}" +cid));
		log.trace(ESAPIUtil.vaildLog("type : {}"+ type));
		try {
			
			//call b106
			String typeUp = type.toUpperCase();
			String[] acnotype = AcnoTypeEnum.valueOf(typeUp).getAcnotype();
			String trflag = AcnoTypeEnum.valueOf(typeUp).getTrflag();
			mvh = commonPools.b106.doAction(request);

		}catch (NullPointerException e) {
			log.error(ESAPIUtil.vaildLog("TYPE : {}" +type));
			log.error("process error >>",e);
		}catch (IllegalArgumentException e) {
			log.error("process error >> {}",e);
			log.error(ESAPIUtil.vaildLog("TYPE : {}" +type));
			for (AcnoTypeEnum acnoTypeEnum : AcnoTypeEnum.values()) {
				log.error("TYPE Can only enter : {}" ,acnoTypeEnum.toString());
				}
		}
		catch (Exception e) {
			log.error("",e);
		}
		return data_Retouch(mvh);
	}

	@Override
	public HashMap data_Retouch(MVH mvh) {
		// TODO Auto-generated method stub
		return super.data_Retouch(mvh);
	}

	@Override
	public void setMsgCode(Map dataMap, Boolean data_Retouch_result) {
		// TODO Auto-generated method stub
		dataMap.put("TOPMSG", "OKLR");
		super.setMsgCode(dataMap, data_Retouch_result);
	}
}
