package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import org.springframework.stereotype.Service;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import lombok.extern.slf4j.Slf4j;

/**
 * 主要用來
 * 1.丟入MVH前的前置處理，包含檢核;
 * 2.後製MVH回覆的訊息;
 * @author JeffChang
 *
 */
@Service
@Slf4j
public class C012_Tel_Service extends Common_Tel_Service  {

	@Override
	public HashMap pre_Processing(Map request) {
		// TODO Auto-generated method stub
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) {
		// TODO Auto-generated method stub
		return super.process(serviceID, request);
	}

	@Override
	public HashMap data_Retouch(MVH mvh) {
		MVHImpl mvhimpl = null;
		LinkedList<Row> tmplist = new LinkedList<Row>();
		HashMap<String,String> rowMap;
		Boolean result = Boolean.TRUE;
		HashMap<String,Object> dataMap = new HashMap<String,Object>();
		LinkedList<HashMap<String,String>> rowlist = new LinkedList<HashMap<String,String>>();
    	try {
    		mvhimpl = (MVHImpl) mvh;
			
			dataMap.putAll(mvhimpl.getFlatValues());
			
			MVHImpl info = (MVHImpl)mvhimpl.getTableByName("資訊");
			log.debug("info={}",info);
			Iterator<Row> tmp = info.getOccurs().getRows().iterator();
			tmp.forEachRemaining(tmplist::add);
			
			for(Row row:tmplist){
				row.getValues().put("CDNO", row.getValues().get("CREDITNO"));
				log.debug("row.getValues()={}",row.getValues());
				rowMap = new HashMap<String,String>();
				rowMap.putAll(row.getValues());
				rowlist.add(rowMap);
			}
			log.debug("rowlist={}",rowlist);
			dataMap.put("REC",rowlist);
			
			LinkedList<Map<String,String>> rowlist2 = new LinkedList<Map<String,String>>();
			MVHImpl cryinfo = (MVHImpl)mvhimpl.getTableByName("各幣別合計");
			
			int x = 1;
			
			while(true){
				log.debug("x={}",x);
				
				String SUBCRY = cryinfo.getValueByFieldName("SUBCRY_" + x);
				log.debug("SUBCRY={}",SUBCRY);
				
				if(SUBCRY != null && !"".equals(SUBCRY)){
					Map<String,String> map = new HashMap<String,String>();
					map.put("SUBCRY",cryinfo.getValueByFieldName("SUBCRY_" + x));
					map.put("SUBTOTAMT",cryinfo.getValueByFieldName("SUBTOTAMT_" + x));
					map.put("SUBREFVALUE",cryinfo.getValueByFieldName("SUBREFVALUE_" + x));
					map.put("SUBLCYRAT",cryinfo.getValueByFieldName("SUBLCYRAT_" + x));
					map.put("SUBFCYRAT",cryinfo.getValueByFieldName("SUBFCYRAT_" + x));
					map.put("SUBDIFAMT",cryinfo.getValueByFieldName("SUBDIFAMT_" + x));
					map.put("SUBDIFAMT2",cryinfo.getValueByFieldName("SUBDIFAMT2_" + x));
					
					rowlist2.add(map);
					
					x += 1;
				}
				else{
					break;
				}
			}
			log.debug("rowlist2={}",rowlist2);
			dataMap.put("REC2",rowlist2);
			
			LinkedList<Map<String,String>> rowlist3 = new LinkedList<Map<String,String>>();
			MVHImpl unAllocate = (MVHImpl)mvhimpl.getTableByName("未分配");
			x = 1;
			
			while(true){
				log.debug("x={}",x);
				
				String UNALLOTAMTCRY = unAllocate.getValueByFieldName("UNALLOTAMTCRY_" + x);
				log.debug("UNALLOTAMTCRY={}",UNALLOTAMTCRY);
				
				if(UNALLOTAMTCRY != null && !"".equals(UNALLOTAMTCRY)){
					Map<String,String> map = new HashMap<String,String>();
					map.put("UNALLOTAMTCRY",unAllocate.getValueByFieldName("UNALLOTAMTCRY_" + x));
					map.put("UNALLOTAMT",unAllocate.getValueByFieldName("UNALLOTAMT_" + x));
					
					rowlist3.add(map);
					
					x += 1;
				}
				else{
					break;
				}
			}
			log.debug("rowlist3={}",rowlist3);
			dataMap.put("REC3",rowlist3);
			
			MVHImpl twdreturn = (MVHImpl)mvhimpl.getTableByName("約當台幣總報酬");
			dataMap.putAll(twdreturn.getFlatValues());
			log.debug("twdreturn={}",twdreturn);
			
			result = Boolean.TRUE;
			dataMap.put("MSGCOD","0000");
			
		} catch (Exception e) {
			log.error("process.error>> {}", e);
		}finally {
			setMsgCode(dataMap,result);
		}
    	return dataMap;
	}
	
	@Override
	public void setMsgCode(Map dataMap, Boolean data_Retouch_result) {
		// TODO Auto-generated method stub
		super.setMsgCode(dataMap, data_Retouch_result);
	}
	
}
