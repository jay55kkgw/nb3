/*
 * SQLLogException.java
 *
 * Created on 2002年8月22日, 下午 6:38
 */

package topibs.exception;

/**
 *
 * @author  chuliu
 * @version 
 */
public class SQLLogException extends Exception {

    /** Creates new SQLLogException */
    public SQLLogException() {
        super("資料重覆");
    }
    public SQLLogException(String a_str_Exception) {
        super(a_str_Exception);
    }
}
