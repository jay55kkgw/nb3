/*
 * SQLUnConnectException.java
 *
 * Created on 2002年8月22日, 下午 6:10
 */

package topibs.exception;

/**
 *
 * @author  chuliu
 * @version 
 */
public class SQLUnConnectException extends Exception {

    /** Creates new SQLUnConnectException */
    public SQLUnConnectException() {
        super("取得資料庫連結，發生錯誤!!!請通知系統人員");
    }
    
    public SQLUnConnectException(String a_str_Exception) {
        super(a_str_Exception);
    }
}
