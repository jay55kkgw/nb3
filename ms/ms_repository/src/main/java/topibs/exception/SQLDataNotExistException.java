/*
 * SQLDataNotExistException.java
 *
 * Created on 2002年8月22日, 下午 5:31
 */

package topibs.exception;

/**
 *
 * @author  chuliu
 * @version 
 */
public class SQLDataNotExistException extends Exception {

    /** Creates new SQLDataNotExistException */
    public SQLDataNotExistException() {
        super("資料不存在");
    }
    public SQLDataNotExistException(String a_str_Exception) {
    super(a_str_Exception);
    }
}
