/*
 * SQLDuplicateException.java
 *
 * Created on 2002年8月22日, 下午 5:41
 */

package topibs.exception;

/**
 *
 * @author  chuliu
 * @version 
 */
public class SQLDuplicateException extends Exception {

    /** Creates new SQLDuplicateException */
    public SQLDuplicateException() {
        super("資料重覆");
    }
    public SQLDuplicateException(String a_str_Exception)  {
        super(a_str_Exception);
    }
}
