/*
 * SQLTableSchemaException.java
 *
 * Created on 2002年8月22日, 下午 6:40
 */

package topibs.exception;

/**
 *
 * @author  chuliu
 * @version 
 */
public class SQLTableSchemaException extends Exception {

    /** Creates new SQLTableSchemaException */
    public SQLTableSchemaException() {
        super("取得資料庫資訊時，發生錯次!!!請通知系統人員");
    }

    public SQLTableSchemaException(String a_str_Exception) {
        super(a_str_Exception);
    }
}
