/*
 * DataBefImageErrorException.java
 *
 * Created on 2002年8月22日, 下午 6:16
 */

package topibs.exception;

/**
 *
 * @author  chuliu
 * @version 
 */
public class DataBefImageErrorException extends Exception {

    /** Creates new DataBefImageErrorException */
    public DataBefImageErrorException() {
        super("欲修改的資料，已被異動");
    }
    public DataBefImageErrorException(String a_str_Exception) {
        super(a_str_Exception);
    }
}
