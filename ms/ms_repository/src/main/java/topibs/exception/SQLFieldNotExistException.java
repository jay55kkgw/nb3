/*
 * SQLFieldNotExistException.java
 *
 * Created on 2002年8月22日, 下午 5:51
 */

package topibs.exception;

/**
 *
 * @author  chuliu
 * @version 
 */
public class SQLFieldNotExistException extends Exception {

    /** Creates new SQLFieldNotExistException */
    public SQLFieldNotExistException() {
        super("欄位不存在");
    }
     public SQLFieldNotExistException(String a_str_Exception) {
        super(a_str_Exception);
    }
}
