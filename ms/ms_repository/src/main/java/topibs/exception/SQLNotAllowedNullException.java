/*
 * SQLNotAllowedNullException.java
 *
 * Created on 2002年8月22日, 下午 6:07
 */

package topibs.exception;

/**
 *
 * @author  chuliu
 * @version 
 */
public class SQLNotAllowedNullException extends Exception {

    /** Creates new SQLNotAllowedNullException */
    public SQLNotAllowedNullException() {
        super("欄位不允許null");
    }
     public SQLNotAllowedNullException(String a_str_Exception) {
        super(a_str_Exception);
    }
}
