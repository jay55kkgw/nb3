/*
 * SQLRelationException.java
 *
 * Created on 2002年8月22日, 下午 6:02
 */

package topibs.exception;

/**
 *
 * @author  chuliu
 * @version 
 */
public class SQLRelationException extends Exception {

    /** Creates new SQLRelationException */
    public SQLRelationException() {
        super("異動資料庫時，Foreign Key 發生錯誤");
    }
    public SQLRelationException(String a_str_Exception) {
        super(a_str_Exception);
    }
}
