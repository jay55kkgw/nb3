package com.netbank.domain.orm.core;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.netbank.util.ESAPIUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Transactional(
		value="nnbtransactionManager",
        readOnly = true,
        rollbackFor = {Throwable.class}
)
public abstract class NNBJpaRepository<T, ID extends Serializable> implements CoreRepository<T, ID>  {
    //protected static final Logger LOG = LoggerFactory.getLogger(LegacyJpaRepository.class);
//    @PersistenceContext
    @PersistenceContext(unitName="nnb")
//    @PersistenceContext(unitName="nnbentityManagerFactory")
    private EntityManager nnbentityManager;

    public NNBJpaRepository() {
    }

    public EntityManager getEntityManager() {
        return this.nnbentityManager;
    }

    public long count() {
        Class<T> klazz = this.getParameterizedTypeClass();
        log.trace("Getting count for {}", klazz.getSimpleName());
        Query query = this.getEntityManager().createQuery("SELECT count(c) FROM " + klazz.getSimpleName() + " c");

        try {
            return (Long)query.getSingleResult();
        } catch (NoResultException var4) {
            return 0L;
        }
    }

    public List<T> findAll() {
        Class<T> klazz = this.getParameterizedTypeClass();
        log.trace("Search for all {}", klazz.getSimpleName());
        Query query = new QueryAdapter(this.getEntityManager().createQuery("SELECT c FROM " + klazz.getSimpleName() + " c"));

        List<T> results = query.getResultList();
        if (null == results) {
            results = new ArrayList();
        }

        results = (List<T>) ESAPIUtil.validList(results);
        
        log.trace("Found {} {}", ((List)results).size(), klazz.getSimpleName());
        return (List)results;
    }

    public T findById(ID id) {
        Class<T> klazz = this.getParameterizedTypeClass();
        log.trace("Search for a {} with id {}", klazz.getSimpleName(), id);
        T o = this.getEntityManager().find(klazz, id);
        if (null != o) {
            log.trace("Found a {} to id {}", klazz.getSimpleName(), id);
        } else {
            log.trace("Didn't find any {} to id {}", klazz.getSimpleName(), id);
        }

        return o;
    }

    public void save(T entity) {
    	// Privacy Violation
//        log.trace("Persist entity {}", entity);
    	
        this.getEntityManager().persist(entity);
        this.getEntityManager().flush();
    }
    public void saveOrUpdate(T entity) {
    	// Privacy Violation
//        log.trace("Persist entity {}", entity);
    	Session session = this.getEntityManager().unwrap(Session.class);
    	session.saveOrUpdate(entity);
//    	this.getEntityManager().persist(entity);
//    	this.getEntityManager().flush();
    }

    public T update(T entity) {
        entity = this.getEntityManager().merge(entity);
        this.getEntityManager().flush();
        return entity;
    }
    public T updateI(ID id) {
    	return updateII(findById(id));
    }
    public T updateII(T entity) {
    	entity = this.getEntityManager().merge(entity);
    	this.getEntityManager().flush();
    	return entity;
    }

    public void removeById(ID id) {
        this.delete(findById(id));
    }
    
    
    public void delete(T entity) {
        T t = this.getEntityManager().merge(entity);
        this.getEntityManager().remove(t);
    }

    private Class<T> getParameterizedTypeClass() {
        return (Class)((ParameterizedType)this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    /**
     * 創建Query對像.
     * 對於需要first,max,fetchsize,cache,cacheRegion等諸多設置的函數,可以返回Query後自行設置.
     * 留意可以連續設置,如 dao.getQuery(hql).setMaxResult(100).setCacheable(true).list();
     *
     * @param values 可變參數
     *               用戶可以如下四種方式使用
     *               dao.getQuery(hql)
     *               dao.getQuery(hql,arg0);
     *               dao.getQuery(hql,arg0,arg1);
     *               dao.getQuery(hql,new Object[arg0,arg1,arg2])
     */
    public Query getQuery(String hql, Object... values) {
        hql = JPAUtils.replaceToJpaStyle(hql);
//        System.out.println("nnbentityManager is null: " + (nnbentityManager == null));
        Query q = nnbentityManager.createQuery(hql);
//        System.out.println("q is null: " + (q == null));
        Query query = new QueryAdapter(q);
        for (int i = 0; values != null && i < values.length; i++) {
            query.setParameter(i, values[i]);
        }
        return query;
    }


    /**
     * 創建Criteria對像
     *
     * @param criteriaFunction 可指定查詢條件
     */
    public Query getCriteriaQuery(Class<T> entityClass, BiFunction< Root, CriteriaBuilder, Predicate>... criteriaFunction) {

        CriteriaBuilder cb = nnbentityManager.getCriteriaBuilder();
        CriteriaQuery query = cb.createQuery(entityClass);
        Root entityRoot = query.from(entityClass);
        List<Predicate> filterPredicates = new ArrayList<>();
        for(BiFunction< Root, CriteriaBuilder, Predicate> func : criteriaFunction) {
            Predicate a = func.apply(entityRoot, cb);

            filterPredicates.add(a);
        }

        Predicate[] ary =filterPredicates.toArray(new Predicate[]{});
        query.where(cb.and(ary));

        return new QueryAdapter(nnbentityManager.createQuery(query));
    }



    /**
     * hql查詢.
     *
     * @param values 可變參數
     *               用戶可以如下四種方式使用
     *               dao.find(hql)
     *               dao.find(hql,arg0);
     *               dao.find(hql,arg0,arg1);
     *               dao.find(hql,new Object[arg0,arg1,arg2])
     */
    public List find(String hql, Object... values) {
        Query query = getQuery(hql, values);
        
        // Stored XSS
        List<T> results = query.getResultList();
        if (null == results) {
            results = new ArrayList();
        }
        results = (List<T>) ESAPIUtil.validList(results);
        
        return results;
    }

    /**
     * 根據屬性名和屬性值查詢對像.
     *
     * @return 符合條件的對象列表
     */
    public List<T> findBy(Class<T> entityClass, String name, Object value) {

        return getCriteriaQuery(entityClass, (root, cb) -> cb.equal(root.get(name), value)).getResultList();
    }

    /**
     * 根據屬性名和屬性值查詢唯一對像.
     *
     * @return 符合條件的唯一對像
     */
    public T findUniqueBy(Class<T> entityClass, String name, Object value) {

        return (T) getCriteriaQuery(entityClass, (root, cb) -> cb.equal(root.get(name), value)).getSingleResult();
    }

    /**
     * 根據屬性名和屬性值以Like AnyWhere方式查詢對像.
     */
    public List<T> findByLike(Class<T> entityClass, String name, String value) {

        return getCriteriaQuery(entityClass, (root, cb) -> cb.like(root.get(name), "%" + (value == null ? "" : value) + "%")).getResultList();
    }


    public SessionAdapter getSession() {
        return new SessionAdapter(this.getEntityManager());
    }


    /**
     * 分頁查詢函數，使用hql.
     *
     * @param pageNo 頁號,從0開始.
     */
    public Page pagedQuery(String hql, int pageNo, int pageSize, Object... values) {
        Assert.hasText(hql);
        //Count查詢
        String countQueryString = " select count (*) " + removeSelect(removeOrders(hql));
        List countlist = getQuery(countQueryString, values).getResultList();
        long totalCount = (Long) countlist.get(0);

        if (totalCount < 1) return new Page();
        //實際查詢返回分頁對像
        int startIndex = Page.getStartOfPage(pageNo, pageSize);
        Query query = getQuery(hql, values);
        List list = query.setFirstResult(startIndex).setMaxResults(pageSize).getResultList();

        return new Page(startIndex, totalCount, pageSize, list);
    }


    /**
     * 去除hql的select 子句，未考慮union的情況,，用於pagedQuery.
     */
    private static String removeSelect(String hql) {
        Assert.hasText(hql);
        int beginPos = hql.toLowerCase().indexOf("from");
        Assert.isTrue(beginPos != -1, " hql : " + hql + " must has a keyword 'from'");
        return hql.substring(beginPos);
    }

    /**
     * 去除hql的orderby 子句，用於pagedQuery.
     */
    private static String removeOrders(String hql) {
        Assert.hasText(hql);
        Pattern p = Pattern.compile("order\\s*by[\\w|\\W|\\s|\\S]*", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(hql);
        StringBuffer sb = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(sb, "");
        }
        m.appendTail(sb);
        return sb.toString();
    }

}
