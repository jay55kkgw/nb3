package fstop.exception;

import fstop.orm.dao.AdmMsgCodeDao;
import fstop.orm.po.ADMMSGCODE;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
//import fstop.util.regexHtmlString;


/**
 * QR Code 自訂的 Exception
 * @author Owner
 *
 */
public class QrTopMessageException extends UncheckedException {

	private static final long serialVersionUID = 7034496227113613816L;

	private String msgcode;
	private String msg;

	public QrTopMessageException(String msgcode, String msg) {
		super(compose(msgcode, msg));
		this.msgcode = msgcode;
		this.msg = msg;
	}


	public QrTopMessageException(String msgcode, String msg, Throwable cause) {
		super(compose(msgcode, msg), cause);
		this.msgcode = msgcode;
		this.msg = msg;
	}

	private static String compose(String msgcode, String msg) {
		return StrUtils.trim(msgcode) + ", " + msg;
	}
}
