package fstop.exception;

import fstop.orm.dao.AdmMsgCodeDao;
import fstop.orm.po.ADMMSGCODE;
import fstop.util.SpringBeanFactory;


/**
 * 電文發送後, TOPMSG 裡為錯誤代碼..
 * @author Owner
 *
 */
public class TopMessageException extends UncheckedException {

	private static final long serialVersionUID = 8088350703708513225L;

	private String appCode;
	
	private String msgcode;
	
	private String msgin;
	
	private String msgout;

	public String getMsgcode() {
		return msgcode;
	}

	public String getMsgin() {
		return msgin;
	}

	public String getMsgout() {
		return msgout;
	}

	public TopMessageException(String msgcode, String msgin, String msgout) {
		super(msgcode);
		this.msgcode = msgcode;
		this.msgin = msgin;
		this.msgout = msgout;
		
	}
	
	public TopMessageException(String msgcode, String msgin, String msgout, String adopid) {
		super(msgcode);
		this.msgcode = msgcode;
		this.msgin = msgin;
		this.msgout = msgout;
		this.appCode = adopid;		
	}

	public TopMessageException(String msgcode, Throwable cause) {
		super(msgcode, cause);
	}

	public String getAppCode() {
		return appCode;
	}

	public void setAppCode(String appCode) {
		this.appCode = appCode;
	}
	
	
	public static TopMessageException create(String msgcode) {
		return TopMessageException.create(msgcode, null);
	}
	
	public static TopMessageException create(String msgcode, String adopid) {
		AdmMsgCodeDao admMsgCodeDao = (AdmMsgCodeDao)SpringBeanFactory.getBean("admMsgCodeDao");
		ADMMSGCODE result = admMsgCodeDao.isError(msgcode);
		if(result != null) {
			return new TopMessageException(msgcode, result.getADMSGIN(), result.getADMSGOUT(), adopid);
		}
		else {
			return new TopMessageException(msgcode, "", "", adopid);
		}
	}
	
	public static boolean isInDB(String msgcode) {
		AdmMsgCodeDao admMsgCodeDao = (AdmMsgCodeDao)SpringBeanFactory.getBean("admMsgCodeDao");
		ADMMSGCODE result = admMsgCodeDao.isError(msgcode);
		if(result != null) {
			return true;
		}
		else {
			return false;
		}
	}
		
}
