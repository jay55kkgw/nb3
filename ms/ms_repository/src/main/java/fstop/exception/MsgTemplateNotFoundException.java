package fstop.exception;


public class MsgTemplateNotFoundException extends ToRuntimeException { 
	
	private static final long serialVersionUID = 1L;
	
	public MsgTemplateNotFoundException(String msg) {
		super(msg);
	}
	
	public MsgTemplateNotFoundException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
