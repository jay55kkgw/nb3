package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import fstop.orm.po.ITRINTERVAL;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Slf4j
@Repository
@Transactional
public class ItrIntervalDao extends LegacyJpaRepository<ITRINTERVAL, String> {
	protected Logger logger = Logger.getLogger(getClass());
	//public List<ITRINTERVAL> getAll() {
	//	return find("FROM ITRINTERVAL");
	//}
	
	

	public void deleteAll() {
		this.getSession().createSQLQuery("DELETE FROM ITRINTERVAL").executeUpdate();
	}

}
