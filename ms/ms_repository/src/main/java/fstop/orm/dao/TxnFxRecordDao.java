package fstop.orm.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.LegacyJpaRepository;

import fstop.orm.po.TXNFXRECORD;
import fstop.orm.po.TXNFXSCHEDULE;
import fstop.orm.po.TXNREQINFO;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Transactional
public class TxnFxRecordDao extends LegacyJpaRepository<TXNFXRECORD, String> {


	@Autowired
	TxnReqInfoDao txnReqInfoDao;


	@SuppressWarnings( { "unused", "unchecked" })
	public List<TXNFXRECORD> findByDateRange(String uid, String startDt, String endDt, String acn) {
		String commonQuery = "from TXNFXRECORD where  FXUSERID = ?  and FXTXDATE >= ? and FXTXDATE <= ? and FXTXSTATUS = '0' and FXRETXNO = '' ";
		Query query = null;
		if (acn.equals(""))
			query = getQuery(commonQuery, uid, startDt, endDt);
		else
			query = getQuery(commonQuery + "and FXWDAC = ? ", uid, startDt, endDt, acn);

		return query.getResultList();
	}

	@SuppressWarnings( { "unused", "unchecked" })
	public List<TXNFXRECORD> findSchduleResultByDateRange(String uid, String startDt, String endDt, String status) {
		List result = new ArrayList();
		//FXRETXNO = ''表示只取同一交易的最後一筆
		if(StrUtils.isEmpty(status)) { //全部
			try {
				result = findScheduleAllStatusByDateRange(uid, startDt, endDt);
			}
			finally {}
		}
		else if (status.equals("0")){ //成功
			try {
				String qhql = "from TXNFXRECORD where FXUSERID = ? and FXTXDATE >= ? and FXTXDATE <= ? and FXSCHID > 0 and FXTXSTATUS = ? and FXRETXNO = ''";
				Query query = getQuery(qhql, uid, startDt, endDt, status);
				result = query.getResultList();
			}
			finally{}
		}
		else if (status.equals("1")){ //失敗
			try {
				String qhql = "from TXNFXRECORD where FXUSERID = ? and FXTXDATE >= ? and FXTXDATE <= ? and FXSCHID > 0 and FXTXSTATUS = ? and FXRETXNO = '' and FXRETXSTATUS <> '1' and FXRETXSTATUS <> '2' and FXRETXSTATUS <> '5'";
				Query query = getQuery(qhql, uid, startDt, endDt, status);
				result = query.getResultList();
			}
			finally{}
		}
		else if (status.equals("2")){ //處理中
			try {
				result = findSchedulePendingByDateRange(uid, startDt, endDt, status);
			}
			finally{}
		}
		return result;
	}
	
	/**
	 * 找出指定使用者某一期間內的處理中預約交易(如包括查詢日則會加上查詢日尚未執行的預約到期交易)
	 * @param uid
	 * @param startDt
	 * @param endDt
	 * @param status
	 * @return
	 */
	public List<TXNFXRECORD> findSchedulePendingByDateRange(String uid, String startDt, String endDt, String status) {
		List result = null;
		
		Date today = new Date();
		String tyyyymmdd = DateTimeUtils.format("yyyyMMdd", today);
		int iToday = Integer.parseInt(tyyyymmdd);
		int iStartDate = Integer.parseInt(startDt);
		int iEndDate = Integer.parseInt(endDt);
		
		//判斷查詢期間是否包含今日
		if (iToday >= iStartDate && iToday <= iEndDate)
		{//查詢期間有包含今日，要多查TXNFXSCHEDULE檔今日未執行的交易
			String sql = "" +
			"select * from TXNFXRECORD where FXUSERID = ? and FXTXDATE >= ? and FXTXDATE <= ? and FXSCHID > 0 and FXTXSTATUS = '1' and FXRETXNO = '' and (FXRETXSTATUS = '1' or FXRETXSTATUS = '2' or FXRETXSTATUS = '5') " + 
			"union all " + 
			getSqlForRecordDueTo(today, uid) +
			"order by FXSCHNO ";
			
			log.debug("@@@　TxnFxRecordDao.findSchedulePendingByDateRange() SQL　== " + sql);
			System.out.flush();
			
			Query query = getSession().createSQLQuery(sql);
			query.unwrap(NativeQuery.class).addEntity("record", TXNFXRECORD.class);
			query.setParameter(0, uid);
			query.setParameter(1, startDt);
			query.setParameter(2, endDt);

			result = query.getResultList();
		}
		else
		{//查詢期間沒有包含今日，只查TXNFXRECORD檔
			String qhql = "from TXNFXRECORD where FXUSERID = ? and FXTXDATE >= ? and FXTXDATE <= ? and FXSCHID > 0 and FXTXSTATUS = '1' and FXRETXNO = '' and (FXRETXSTATUS = '1' or FXRETXSTATUS = '2' or FXRETXSTATUS = '5')";
			Query query = getQuery(qhql, uid, startDt, endDt, status);
			result = query.getResultList();
		}
		
		return result;
	}
	
	/**
	 * 找出指定使用者某一期間內的所有已執行預約交易(如包括查詢日則會加上查詢日尚未執行的預約到期交易)
	 * @param uid
	 * @param startDt
	 * @param endDt
	 * @return
	 */
//	請改寫 原始程式在下面
	public List<TXNFXRECORD> findScheduleAllStatusByDateRange(String uid, String startDt, String endDt) {
		return Collections.emptyList();
	}
//	public List<TXNFXRECORD> findScheduleAllStatusByDateRange(String uid, String startDt, String endDt) {
//		List result = null;
//
//		Date today = new Date();
//		String tyyyymmdd = DateTimeUtils.format("yyyyMMdd", today);
//		int iToday = Integer.parseInt(tyyyymmdd);
//		int iStartDate = Integer.parseInt(startDt);
//		int iEndDate = Integer.parseInt(endDt);
//
//		//判斷查詢期間是否包含今日
//		if (iToday >= iStartDate && iToday <= iEndDate)
//		{//查詢期間有包含今日，要多查TXNTWSCHEDULE檔今日未執行的交易
//			String sql = "" +
//			"select * from TXNFXRECORD where FXUSERID = ? and FXTXDATE >= ? and FXTXDATE <= ? and FXSCHID > 0 and FXRETXNO = '' " +
//			"union all " +
//			getSqlForRecordDueTo(today, uid) +
//			"order by FXSCHNO ";
//
//			log.debug("@@@　TXNFXRECORDDao.findScheduleAllStatusByDateRange() SQL　== " + sql);
//			System.out.flush();
//
//			Query query = getSession().createSQLQuery(sql);
//			query.unwrap(NativeQuery.class).addEntity("record", TXNFXRECORD.class);
//			query.setParameter(0, uid);
//			query.setParameter(1, startDt);
//			query.setParameter(2, endDt);
//
//			result = query.getResultList();
//
//			Connection aa = this.getSession().connection();
//			PreparedStatement pstmt = null;
//			try {
//				pstmt = aa.prepareStatement(sql);
//				pstmt.setString(0,uid);
//				pstmt.setString(1,startDt);
//				pstmt.setString(2,endDt);
//				pstmt.executeQuery(sql);
//				ResultSet rs = pstmt.executeQuery();
//				ResultSetMetaData rsmt = rs.getMetaData();
//				int count=rsmt.getColumnCount();
//		           while(rs.next()){
//		                Hashtable hash= new Hashtable();  //同一基金
//		                for(int i=1;i<=count;i++){
//		                     String key=rsmt.getColumnName(i).toLowerCase();
//		                     String value=rs.getString(i);
//		                     if(value==null) {value="";}else{value=value.trim();}
//		                     hash.put(key,value);
//		                }
//		                result.add(hash);
//		            }
//			}
//			catch(Exception e) {
//				log.debug("findScheduleAllStatusByDateRange Exception:"+e.getMessage());
//			}
//		}
//		else
//		{//查詢期間沒有包含今日，只查TXNFXRECORD檔
//			String qhql = "from TXNFXRECORD where FXUSERID = ? and FXTXDATE >= ? and FXTXDATE <= ? and FXSCHID > 0 and FXRETXNO = '' ";
//			Query query = getQuery(qhql, uid, startDt, endDt);
//			result = query.getResultList();
//		}
//
//		return result;
//	}
	
	/**
	 * 返回指定使用者某一日到期的尚未發送之預約交易SQL字串
	 * @param currentDate
	 * @param uid
	 * @return
	 * 2015/01/14 外幣預約結果查詢只要查到處理中資料會有問題
	 */
	private String getSqlForRecordDueTo(Date currentDate, String uid)
	{
		String due = DateTimeUtils.format("yyyyMMdd", currentDate);
		
		String sql = "" +
		"select X.FXSCHNO as ADTXNO, X.ADOPID, X.FXUSERID, '" + due + "' as FXTXDATE, '000000' as FXTXTIME, X.FXWDAC, X.FXSVBH, X.FXSVAC, X.FXWDCURR, X.FXWDAMT, X.FXSVCURR, X.FXSVAMT, X.FXTXMEMO, X.FXTXMAILS, X.FXTXMAILMEMO, X.FXTXCODE, 0 as FXTITAINFO, '' as FXTOTAINFO, X.FXSCHID, '' as FXEFEECCY, '' as FXEFEE, '' as FXEXRATE, '0' as FXREMAIL, '' as FXTXSTATUS, '' as FXEXCODE, '' as FXRETXNO, '0' as FXRETXSTATUS, '' as FXMSGSEQNO, '' as FXMSGCONTENT, '" + due + "' as LASTDATE, '000000' as LASTTIME, '' as FXCERT, X.FXSCHNO " +
		"from ( " +
		" SELECT distinct sch.* FROM                                                                                                  " +
		" 	TXNFXSCHEDULE sch , (" + TxnFxScheduleDao.getSqlForScheduleExcecNextDate(currentDate, uid)+ ") nextexec " +
		" 	where		                                                                                                         " +
		" 			nextexec.FXSCHID = sch.FXSCHID and " +
		"           nextexec.nextexecdate is not null and nextexec.nextexecdate = '" + due + "' " +		
		"           and ( ( FXTXSTATUS = '0' and  LASTDATE <= '" + due + "' )   " +
		"					 or ( FXTXSTATUS = '1' and  LASTDATE = '" + due + "'))  " + //不含狀態2是因有可能Record檔已有執行過此筆的資料，避免重複
		") X ";
		
		return sql;
	}

	/**
	 * 找出某一日的轉帳到期, 第二次轉帳 => 來源為預約檔(FXTXSTATUS = '1' and LASTDATE = 今天)
	 * @param currentDate
	 * @return
	 */
	public List<TXNFXSCHEDULE> findScheduleDueToReSend_S(Date currentDate) {
		String due = DateTimeUtils.format("yyyyMMdd", currentDate);
		Calendar cal = Calendar.getInstance();
		cal.setTime(currentDate);
		int yyyy = cal.get(Calendar.YEAR);
		int mm = cal.get(Calendar.MONTH) + 1;
		int dd = cal.get(Calendar.DATE);

		String sql = 
		"   SELECT distinct {sch.*} FROM                                                                                                  " +
		" 	TXNFXSCHEDULE sch , (" + TxnFxScheduleDao.getSqlForScheduleExcecNextDate(new Date()) + ") nextexec  " +
		" 	where	sch.FXSCHID = nextexec.FXSCHID and	" +
		"           nextexec.nextexecdate is not null and nextexec.nextexecdate = ?                         " +
		"           and ( sch.FXTXSTATUS = '1' and  sch.LASTDATE = ? ) and sch.LOGINTYPE <> ? ";
		
		
		Query query = getSession().createSQLQuery(sql);
		query.unwrap(NativeQuery.class).addEntity("sch", TXNFXSCHEDULE.class);

		

		String sql2 = 
			"   SELECT distinct {sch.*} FROM                                                                                                  " +
			" 	TXNFXSCHEDULE sch , (" + TxnFxScheduleDao.getSqlForScheduleExcecNextDate(new Date()) + ") nextexec  " +
			" 	where	sch.FXSCHID = nextexec.FXSCHID and	" +
			"           nextexec.nextexecdate is not null and nextexec.nextexecdate = '" + due + "' " +
			"           and ( sch.FXTXSTATUS = '1' and  sch.LASTDATE = '" + due + "' ) ";
		
		
		log.debug("@@@ TxnFxRecordDao.findScheduleDueToReSend() SQL is == "+sql2);


		query.setParameter(0, due);
		query.setParameter(1, due);
		query.setParameter(2, "MB");

//log.debug("@@@ TxnFxRecordDao.findScheduleDueToReSend() query.list() == "+query.getResultList());
		
		return query.getResultList();
	}

	/**
	 * 找出某一日的轉帳到期, 第二次轉帳 => 來源為轉帳紀錄檔(FXTXSTATUS = '0' or FXTXSTATUS = '5')
	 * @param currentDate
	 * @return
	 */
	public List<TXNFXRECORD> findScheduleDueToReSend_R(Date currentDate) {
		String due = DateTimeUtils.format("yyyyMMdd", currentDate);
		Calendar cal = Calendar.getInstance();
		cal.setTime(currentDate);
		int yyyy = cal.get(Calendar.YEAR);
		int mm = cal.get(Calendar.MONTH) + 1;
		int dd = cal.get(Calendar.DATE);

		String sql = "SELECT {fxRec.*} FROM TXNFXRECORD fxRec, " +
		" ( SELECT distinct rec.ADTXNO as ADTXNO_1 FROM                                                                                                  " +
		" 	TXNFXSCHEDULE sch , (" + TxnFxScheduleDao.getSqlForScheduleExcecNextDate(new Date()) + ") nextexec, TXNFXRECORD rec  " +
		" 	where	sch.FXSCHID = nextexec.FXSCHID and	                                                                                                         " +
		" 			nextexec.FXSCHID = rec.FXSCHID and " +
		"           nextexec.nextexecdate is not null and nextexec.nextexecdate = ?                         " +
		"           and (  (sch.FXTXSTATUS = '0' or sch.FXTXSTATUS = '5' ) and  sch.LASTDATE = ? and       " +
		"                   rec.FXRETXSTATUS = '5' and rec.LASTDATE = ? and rec.FXRETXNO = '' and sch.LOGINTYPE <> ? " +
		"               ) " +
		" ) t WHERE fxRec.ADTXNO = t.ADTXNO_1 and fxRec.FXRETXNO = '' " ;	
		
		Query query = getSession().createSQLQuery(sql);
		query.unwrap(NativeQuery.class).addEntity("fxRec", TXNFXRECORD.class);

		

		String sql2 = "SELECT {fxRec.*} FROM TXNFXRECORD fxRec, " +
		" ( SELECT distinct rec.ADTXNO as ADTXNO_1 FROM                                                                                                  " +
		" 	TXNFXSCHEDULE sch , (" + TxnFxScheduleDao.getSqlForScheduleExcecNextDate(new Date()) + ") nextexec, TXNFXRECORD rec  " +
		" 	where	sch.FXSCHID = nextexec.FXSCHID and	                                                                                                         " +
		" 			nextexec.FXSCHID = rec.FXSCHID and " +
		"           nextexec.nextexecdate is not null and nextexec.nextexecdate = '" + due + "'                         " +
		"           and (    (sch.FXTXSTATUS = '0' or sch.FXTXSTATUS = '5' ) and  sch.LASTDATE = '" + due + "' and       " +
		"                     rec.FXRETXSTATUS = '5' and rec.LASTDATE = '" + due + "' and rec.FXRETXNO = '' " +
		"               ) " +
		" ) t WHERE fxRec.ADTXNO = t.ADTXNO_1" ;
		
		
		log.debug("@@@ TxnFxRecordDao.findScheduleDueToReSend() SQL is == "+sql2);


		query.setParameter(0, due);
		query.setParameter(1, due);
		query.setParameter(2, due);
		query.setParameter(3, "MB");

//log.debug("@@@ TxnFxRecordDao.findScheduleDueToReSend() query.list() == "+query.getResultList());
		
		return query.getResultList();
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void writeTxnRecord(TXNREQINFO info ,TXNFXRECORD record) {
//		BOLifeMoniter.setValue("__TXNRECORD", record);
		if(info != null) {
//			BOLifeMoniter.setValue("__TITAINFO", info);
			//不寫入此表格，因為此表格在NB3不存在
//			txnReqInfoDao.save(info);
//			record.setFXTITAINFO(info.getREQID());
		}
		save( record);

	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void rewriteTxnRecord(TXNFXRECORD record) {
//		BOLifeMoniter.setValue("__TXNRECORD", record);
		save( record);
	}

	public List<TXNFXRECORD> findOnlineResendStatus(String lastdate, String[] retxstatus) {
		return findResendStatus("online", lastdate, retxstatus);
	}
	
	//new
	public List<TXNFXRECORD> findOnlineFailed(String lastdate , List<String> pendingList) {
		//即時交易 失敗
		String hqlp1 = "FROM TXNFXRECORD WHERE FXSCHID = 0 and FXTXSTATUS = '1' and LASTDATE = ? ";
		String hqlp2 = " AND FXEXCODE in ('" + StrUtils.implode("','", pendingList.toArray(new String[pendingList.size()])) + "') ";
		String hqlp3 = " ORDER BY LASTDATE DESC, LASTTIME DESC ";
		Query query = getQuery(hqlp1 + hqlp2 + hqlp3 , lastdate);
		return query.getResultList();
	}
	
	public List<TXNFXRECORD> findScheduleFailed(String lastdate , List<String> pendingList) {
		//預約交易 失敗
//		Query query = getQuery("FROM TXNFXRECORD " +
//				" WHERE FXSCHID > 0 and FXTXSTATUS = '1' "+
//				" and LASTDATE = ? " + 
//				//and FXRETXNO = '' " +
//				" ORDER BY LASTDATE DESC, LASTTIME DESC ", lastdate);
		String hqlp1 = "FROM TXNFXRECORD WHERE FXSCHID > 0 and FXTXSTATUS = '1' and LASTDATE = ? ";
		String hqlp2 = " AND FXEXCODE in ('" + StrUtils.implode("','", pendingList.toArray(new String[pendingList.size()])) + "') ";
		String hqlp3 = " ORDER BY LASTDATE DESC, LASTTIME DESC ";
		Query query = getQuery(hqlp1 + hqlp2 + hqlp3 , lastdate);
		return query.getResultList();
	}
	
	
	public List<TXNFXRECORD> findScheduleResendStatus(String lastdate, String[] retxstatus) {
		return findResendStatus("schedule", lastdate, retxstatus);
	}
	public List<TXNFXRECORD> findResendStatus(String func, String lastdate, String[] retxstatus) {
		String restatusCondition = "";
		if(retxstatus == null || retxstatus.length == 0)
			return new ArrayList();
		else if(retxstatus.length == 1)  {
			if("*".equals(retxstatus[0]))
				restatusCondition = "FXRETXSTATUS <> '0' ";
			else
				restatusCondition = "FXRETXSTATUS = '" + retxstatus[0] + "' ";
		}
		else if(retxstatus.length > 1)
			restatusCondition = "FXRETXSTATUS in ('" + StrUtils.implode("','", retxstatus) + "') ";

		String schidCondition = "";
		if("online".equalsIgnoreCase(func))
		schidCondition = " FXSCHID = 0 ";
		else if("schedule".equals(func))
			schidCondition = " FXSCHID > 0 ";

		/*
		Query query = getQuery("FROM TXNFXRECORD " +
				" WHERE " + schidCondition + " and FXTXSTATUS = '1' and  " + restatusCondition +
				" and LASTDATE = ? and FXRETXNO = '' " +
				" ORDER BY LASTDATE DESC, LASTTIME DESC ", lastdate);
		*/

		Query query = getQuery("FROM TXNFXRECORD " +
				" WHERE " + schidCondition + " and FXTXSTATUS = '1' and  " + restatusCondition +
				" and LASTDATE = ? " +   //and FXRETXNO = '' " +
				" ORDER BY LASTDATE DESC, LASTTIME DESC ", lastdate);
		
		log.debug("@@@ B402 SQL == "+
		"FROM TXNFXRECORD " +
		" WHERE " + schidCondition + " and FXTXSTATUS = '1' "+
		" and LASTDATE = '" + lastdate + "' " +    // and FXRETXNO = '' " +
		" ORDER BY LASTDATE DESC, LASTTIME DESC ");

		return query.getResultList();
	}

	/**
	 * 找到重送之後寫的那筆 TXNFXRECORD
	 * @param adtxno
	 * @return
	 */
	public TXNFXRECORD findResendRecord(String adtxno) {
		return this.findUniqueBy(TXNFXRECORD.class,"FXRETXNO", adtxno);
	}

	public void updateStatus(TXNFXRECORD record, String status) {
		try {
			Date d = new Date();
			record.setLASTDATE(DateTimeUtils.format("yyyyMMdd", d));
			record.setLASTTIME(DateTimeUtils.format("HHmmss", d));
//			record.setFXRETXSTATUS(status);

			save(record);
		}
		catch(Exception e) {

		}
		finally {

		}
	}

	/**
	 * 供<線上解款資料清單>查詢 ==> 是否有列為"待重送"之匯入匯款資料
	 * 
	 * @param refNo
	 * @return
	 */
	public boolean findByRefNo(String refNo) {
		
		String sql = "from TXNFXRECORD where FXTXMEMO= ? and FXRETXSTATUS='5'";		
		Query query = getQuery(sql, refNo);		

		if (query.getResultList().size() == 0)
			return false;
		else
			return true;	
	}	
}
