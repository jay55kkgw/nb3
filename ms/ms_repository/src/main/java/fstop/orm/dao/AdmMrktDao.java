package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import fstop.orm.po.ADMMRKT;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.Date;
import java.util.List;


@Slf4j
@Repository
public class AdmMrktDao extends LegacyJpaRepository<ADMMRKT, Integer>
{
	protected Logger logger = Logger.getLogger(getClass());
	protected String _MessageType;
	protected String _MessageStatus;
	protected String _MessageApprove;
	protected String _MessageStartMinDate;
	protected String _MessageStartMaxDate;
	protected String _MessageDueMinDate;
	protected String _MessageDueMaxDate;
	protected String _MessagePost;
	protected String _MessageTitle;
	protected ADMMRKT PO_admMrkt;
	protected String _RegistrStr;
	protected String _WithdrawnStr;
	protected String _NotifyStatus;
	protected String _Notify;
	
	//設定行銷類別 { 1:網銀促銷活動  2:信用卡優惠活動  3:行銷活動訊息  4:文字訊息推播}
	public void setMessageType(String messageType)
	{
		_MessageType = messageType;
	}
	
	//設定行銷狀態 { M:未核放  N:不同意  C:已核放 }
	public void setMessageStatus(String messageStatus)
	{
		_MessageStatus = messageStatus;
	}
	
	//設定審核結果 { 0:審核未過  1:審核過 }
	public void setMessageApprove(String messageApprove)
	{
		_MessageApprove = messageApprove;
	}
	
	//設定行銷上版起日(區間起值)
	public void setMessageStartMinDate(String messageStartMinDate)
	{
		_MessageStartMinDate = messageStartMinDate;
	}
	
	//設定行銷上版起日(區間迄值)
	public void setMessageStartMaxDate(String messageStartMaxDate)
	{
		_MessageStartMinDate = messageStartMaxDate;
	}
	
	//設定行銷上版迄日(區間起值)
	public void setMessageDueMinDate(String messageDueMinDate)
	{
		_MessageDueMinDate = messageDueMinDate;
	}
	
	//設定行銷上版迄日(區間起值)
	public void setMessageDueMaxDate(String messageDueMaxDate)
	{
		_MessageDueMaxDate = messageDueMaxDate;
	}
	
	//設定強迫公告
	public void setMessagePost(String messagePost)
	{
		_MessagePost = messagePost;
	}
	
	//設定行銷抬頭
	public void setMessageTitle(String messageTitle)
	{
		_MessageTitle = messageTitle;
	}
	
	//設定是否推播
	public void setNotify(String notify)
	{
		_Notify = notify;
	}
	
	public void setPo(ADMMRKT PO)
	{
		PO_admMrkt = PO;
	}

	public void setRegistrStr(String R_Str)
	{
		_RegistrStr = R_Str;
	}
	
	public void setWithdrawnStr(String W_Str)
	{
		_WithdrawnStr = W_Str;
	}
	
	public List<ADMMRKT> getConditionMessage(String ConditionStr)
	{
		String QueryStr = "FROM ADMMRKT ";
		QueryStr += "WHERE 1 = 1 " + ConditionStr;
		logger.debug("QueryStr==>"+QueryStr+"<===");//20150127
		List<ADMMRKT> qresult = find(QueryStr);
		return qresult;
	}
	
	public List<ADMMRKT> getMessage(int msgID)
	{
		String QueryStr = "FROM ADMMRKT ";
		QueryStr += "WHERE MTID = ?";
		
		List<ADMMRKT> qresult = find(QueryStr, msgID);
		return qresult;
	}
	
	public List<ADMMRKT> getAllApprove()
	{
		String QueryStr = "FROM ADMMRKT ";
		QueryStr += "WHERE MTADSTAT = 'C' ";
		QueryStr += "ORDER BY MTWEIGHT,MTSDATE||MTSTIME,MTID DESC";

		log.debug("QueryStr ===============================> " + QueryStr);
		
		List<ADMMRKT> qresult = find(QueryStr);
		return qresult;
	}
	
	public List<ADMMRKT> getAll()
	{
		String current = DateTimeUtils.format("yyyyMMddHHmm", new Date());
		
		log.debug("MessageType ===============================> " + _MessageType);
		log.debug("MessageStatus ===============================> " + _MessageStatus);
		log.debug("MessageApprove ===============================> " + _MessageApprove);
		log.debug("MessageStartMinDate ===============================> " + _MessageStartMinDate);
		log.debug("MessageStartMaxDate ===============================> " + _MessageStartMaxDate);
		log.debug("MessageDueMinDate ===============================> " + _MessageDueMinDate);
		log.debug("MessageDueMaxDate ===============================> " + _MessageDueMaxDate);
		log.debug("MessageTitle ===============================> " + _MessageTitle);
		
		String QueryStr = "FROM ADMMRKT ";
		QueryStr += "WHERE 1 = 1 ";
		if(_MessageStatus != null && _MessageStatus.length() > 0)
			QueryStr += "and MTADSTAT = '" + _MessageStatus + "' ";
		if(_MessageStartMinDate != null && _MessageStartMinDate.length() > 0)
			QueryStr += "and MTSDATE||MTSTIME >= '" + _MessageStartMinDate + "' ";
		if(_MessageStartMaxDate != null && _MessageStartMaxDate.length() > 0)
			QueryStr += "and MTSDATE||MTSTIME <= '" + _MessageStartMaxDate + "' ";
		if(_MessageDueMinDate != null && _MessageDueMinDate.length() > 0)
			QueryStr += "and MTEDATE||MTETIME >= '" + _MessageDueMinDate + "' ";
		if(_MessageDueMaxDate != null && _MessageDueMaxDate.length() > 0)
			QueryStr += "and MTEDATE||MTETIME <= '" + _MessageDueMaxDate + "' ";
		if(_MessageType != null && _MessageType.length() > 0)
			QueryStr += "and MTADTYPE = '" + _MessageType + "' ";		
		if(_MessageApprove != null && _MessageApprove.length() > 0)
			QueryStr += "and MTCHRES = '" + _MessageApprove + "' ";
		if(_MessagePost != null && _MessagePost.length() > 0)
			QueryStr += "and MTBULLETIN = '" + _MessagePost + "' ";	
		if(_MessageTitle != null && _MessageTitle.length() > 0)
			QueryStr += "and MTADHLK like '%" + _MessageTitle + "%' ";
		
		QueryStr += "ORDER BY MTWEIGHT,MTSDATE||MTSTIME,MTID DESC";

		log.debug("QueryStr ===============================> " + QueryStr);
		
		List<ADMMRKT> qresult = find(QueryStr);
		return qresult;
	}
	
	public List<ADMMRKT> getNotify()
	{
		String current = DateTimeUtils.format("yyyyMMddHHmm", new Date());
		
		String QueryStr = "FROM ADMMRKT ";
		QueryStr += "WHERE 1 = 1 ";
		QueryStr += "and MTSDATE||MTSTIME <= '" + current + "' ";
		QueryStr += "and MTEDATE||MTETIME >= '" + current + "' ";
		QueryStr += "and MTADSTAT = 'C' ";
		QueryStr += "and MTNF = '1' ";			// 需要推播
		QueryStr += "and MTNFSTAT = '0' ";		// 尚未推播
		QueryStr += "ORDER BY MTWEIGHT,MTSDATE||MTSTIME,MTID DESC";

		log.debug("QueryStr ===============================> " + QueryStr);
		
		List<ADMMRKT> qresult = find(QueryStr);
		return qresult;
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public int addMessage() {
		
		String insertStr = "INSERT INTO ADMMRKT(";
		insertStr += "MTUSERID, ";
		insertStr += "MTUSERNAME, ";
		insertStr += "MTTXDATE, ";
		insertStr += "MTTXTIME, ";
		insertStr += "MTSDATE, ";
		insertStr += "MTSTIME, ";
		insertStr += "MTEDATE, ";
		insertStr += "MTETIME, ";
		insertStr += "MTADTYPE, ";
		insertStr += "MTWEIGHT, ";
		insertStr += "MTADHLK, ";
		insertStr += "MTADCON, ";
		insertStr += "MTPICTYPE, ";
		insertStr += "MTRUNSHOW, ";
		insertStr += "MTBULLETIN, ";
		insertStr += "MTPICADD, ";
		insertStr += "MTPICDATA, ";
		insertStr += "MTADSTAT, ";
		insertStr += "MTCHRES, ";
		insertStr += "MTSUPNO, ";
		insertStr += "MTSUPNAME, ";
		insertStr += "MTSUPDATE, ";
		insertStr += "MTSUPTIME, ";
		insertStr += "MTBTNNAME, ";			//20141202
		insertStr += "MTADDRESS, ";			//20141202		
		insertStr += "MTMARQUEEPIC, ";		//20141204
		insertStr += "MTMARQUEEPICADD, ";	//20150112
		insertStr += "MTNF, ";			//20150106
		insertStr += "MTNFSTAT, ";		//20150106
		insertStr += "MTNFDATE, ";		//20150106
		insertStr += "MTNFTIME, ";		//20150106
		insertStr += "MTCF, ";		//20150812
		insertStr += "MTLOAN) ";		//20180717
		insertStr += "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		
		Query add1=this.getSession().createSQLQuery(insertStr);

		add1.setParameter(0, PO_admMrkt.getMTUSERID());
		add1.setParameter(1, PO_admMrkt.getMTUSERNAME());
		add1.setParameter(2, PO_admMrkt.getMTTXDATE());
		add1.setParameter(3, PO_admMrkt.getMTTXTIME());
		add1.setParameter(4, PO_admMrkt.getMTSDATE());
		add1.setParameter(5, PO_admMrkt.getMTSTIME());
		add1.setParameter(6, PO_admMrkt.getMTEDATE());
		add1.setParameter(7, PO_admMrkt.getMTETIME());
		add1.setParameter(8, PO_admMrkt.getMTADTYPE());
		add1.setParameter(9, PO_admMrkt.getMTWEIGHT());
		add1.setParameter(10, PO_admMrkt.getMTADHLK());
		add1.setParameter(11, PO_admMrkt.getMTADCON());
		add1.setParameter(12, PO_admMrkt.getMTPICTYPE());
		add1.setParameter(13, PO_admMrkt.getMTRUNSHOW());
		add1.setParameter(14, PO_admMrkt.getMTBULLETIN());
		add1.setParameter(15, PO_admMrkt.getMTPICADD());
		add1.setParameter(16, PO_admMrkt.getMTPICDATA());
		add1.setParameter(17, PO_admMrkt.getMTADSTAT());
		add1.setParameter(18, PO_admMrkt.getMTCHRES());
		add1.setParameter(19, PO_admMrkt.getMTSUPNO());
		add1.setParameter(20, PO_admMrkt.getMTSUPNAME());
		add1.setParameter(21, PO_admMrkt.getMTSUPDATE());
		add1.setParameter(22, PO_admMrkt.getMTSUPTIME());
		add1.setParameter(23, PO_admMrkt.getMTBTNNAME());//20141202
		add1.setParameter(24, PO_admMrkt.getMTADDRESS());//20141202
		add1.setParameter(25, PO_admMrkt.getMTMARQUEEPIC());//20141204
		add1.setParameter(26, PO_admMrkt.getMTMARQUEEPICADD());//20150112
		add1.setParameter(27, PO_admMrkt.getMTNF());//20150106
		add1.setParameter(28, PO_admMrkt.getMTNFSTAT());//20150106
		add1.setParameter(29, PO_admMrkt.getMTNFDATE());//20150106
		add1.setParameter(30, PO_admMrkt.getMTNFTIME());//20150106
		add1.setParameter(31, PO_admMrkt.getMTCF());//20150812
		add1.setParameter(32, PO_admMrkt.getMTLOAN());//20180717
	
		log.debug("MTPICTYPE====>"+PO_admMrkt.getMTPICTYPE());
		log.debug("MTRUNSHOW====>"+PO_admMrkt.getMTRUNSHOW());
		log.debug("MTBULLETIN===>"+PO_admMrkt.getMTBULLETIN());
		log.debug("MTPICADD=====>"+PO_admMrkt.getMTPICADD());
		log.debug("MTPICDATA====>"+PO_admMrkt.getMTPICDATA());
		log.debug("MTADSTAT=====>"+PO_admMrkt.getMTADSTAT());
		log.debug("MTCHRES======>"+PO_admMrkt.getMTCHRES());
		log.debug("MTSUPNO======>"+PO_admMrkt.getMTSUPNO());
		log.debug("MTSUPNAME====>"+PO_admMrkt.getMTSUPNAME());
		log.debug("MTSUPDATE====>"+PO_admMrkt.getMTSUPDATE());
		log.debug("MTSUPTIME====>"+PO_admMrkt.getMTSUPTIME());
		log.debug("MTBTNNAME====>"+PO_admMrkt.getMTBTNNAME());
		log.debug("MTADDRESS====>"+PO_admMrkt.getMTADDRESS());		
		log.debug("MTMARQUEEPIC=>"+PO_admMrkt.getMTMARQUEEPIC());
		log.debug("MTNF=========>"+PO_admMrkt.getMTNF());
		log.debug("MTNFSTAT=====>"+PO_admMrkt.getMTNFSTAT());
		log.debug("MTNFDATE=====>"+PO_admMrkt.getMTNFDATE());
		log.debug("MTNFTIME=====>"+PO_admMrkt.getMTNFTIME());
		log.debug("MTCF=====>"+PO_admMrkt.getMTCF());
		log.debug("MTLOAN=====>"+PO_admMrkt.getMTLOAN());

		int r2 = 0;
		try{
			r2 = add1.executeUpdate();
		}catch(Exception e){
			log.debug("e================>"+e);
		}
		
		log.debug("r2===>"+r2);
		return(r2);
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public int updateMessage()
	{
		int FieldCount = 0;
		int UpdateCount = 0;
		
		String insertStr = "UPDATE ADMMRKT SET ";
		if(PO_admMrkt.getMTUSERID() != null && PO_admMrkt.getMTUSERID().length() > 0)
		{
			insertStr += "MTUSERID = ? ";
			FieldCount++;
		}
		if(PO_admMrkt.getMTUSERNAME() != null && PO_admMrkt.getMTUSERNAME().length() > 0)
		{
			if(FieldCount > 0)insertStr += ", ";
			insertStr += "MTUSERNAME = ? ";
			FieldCount++;
		}
		if(PO_admMrkt.getMTTXDATE() != null && PO_admMrkt.getMTTXDATE().length() > 0)
		{
			if(FieldCount > 0)insertStr += ", ";
			insertStr += "MTTXDATE = ? ";
			FieldCount++;
		}
		if(PO_admMrkt.getMTTXTIME() != null && PO_admMrkt.getMTTXTIME().length() > 0)
		{
			if(FieldCount > 0)insertStr += ", ";
			insertStr += "MTTXTIME = ? ";
			FieldCount++;
		}
		if(PO_admMrkt.getMTSDATE() != null && PO_admMrkt.getMTSDATE().length() > 0)
		{
			if(FieldCount > 0)insertStr += ", ";
			insertStr += "MTSDATE = ? ";
			FieldCount++;
		}
		if(PO_admMrkt.getMTSTIME() != null && PO_admMrkt.getMTSTIME().length() > 0)
		{
			if(FieldCount > 0)insertStr += ", ";
			insertStr += "MTSTIME = ? ";
			FieldCount++;
		}
		if(PO_admMrkt.getMTEDATE() != null && PO_admMrkt.getMTEDATE().length() > 0)
		{
			if(FieldCount > 0)insertStr += ", ";
			insertStr += "MTEDATE = ? ";
			FieldCount++;
		}
		if(PO_admMrkt.getMTETIME() != null && PO_admMrkt.getMTETIME().length() > 0)
		{
			if(FieldCount > 0)insertStr += ", ";
			insertStr += "MTETIME = ? ";
			FieldCount++;
		}
		if(PO_admMrkt.getMTADTYPE() != null && PO_admMrkt.getMTADTYPE().length() > 0)
		{
			if(FieldCount > 0)insertStr += ", ";
			insertStr += "MTADTYPE = ? ";
			FieldCount++;
		}
		if(PO_admMrkt.getMTWEIGHT() != null && PO_admMrkt.getMTWEIGHT().length() > 0)
		{
			if(FieldCount > 0)insertStr += ", ";
			insertStr += "MTWEIGHT = ? ";
			FieldCount++;
		}
		if(PO_admMrkt.getMTADHLK() != null && PO_admMrkt.getMTADHLK().length() > 0)
		{
			if(FieldCount > 0)insertStr += ", ";
			insertStr += "MTADHLK = ? ";
			FieldCount++;
		}
		if(PO_admMrkt.getMTADCON() != null && PO_admMrkt.getMTADCON().length() > 0)
		{
			if(FieldCount > 0)insertStr += ", ";
			insertStr += "MTADCON = ? ";
			FieldCount++;
		}
		if(PO_admMrkt.getMTPICTYPE() != null && PO_admMrkt.getMTPICTYPE().length() > 0)
		{
			if(FieldCount > 0)insertStr += ", ";
			insertStr += "MTPICTYPE = ? ";
			FieldCount++;
		}
		if(PO_admMrkt.getMTRUNSHOW() != null && PO_admMrkt.getMTRUNSHOW().length() > 0)
		{
			if(FieldCount > 0)insertStr += ", ";
			insertStr += "MTRUNSHOW = ? ";
			FieldCount++;
		}
		if(PO_admMrkt.getMTBULLETIN() != null && PO_admMrkt.getMTBULLETIN().length() > 0)
		{
			if(FieldCount > 0)insertStr += ", ";
			insertStr += "MTBULLETIN = ? ";
			FieldCount++;
		}
		if(PO_admMrkt.getMTPICADD() != null && PO_admMrkt.getMTPICADD().length() > 0)
		{
			if(FieldCount > 0)insertStr += ", ";
			insertStr += "MTPICADD = ? ";
			FieldCount++;
		}
		if(PO_admMrkt.getMTPICDATA() != null && PO_admMrkt.getMTPICDATA().length() > 0)
		{
			if(FieldCount > 0)insertStr += ", ";
			insertStr += "MTPICDATA = ? ";
			FieldCount++;
		}
		if(PO_admMrkt.getMTADSTAT() != null && PO_admMrkt.getMTADSTAT().length() > 0)
		{
			if(FieldCount > 0)insertStr += ", ";
			insertStr += "MTADSTAT = ? ";
			FieldCount++;
		}
		if(PO_admMrkt.getMTCHRES() != null && PO_admMrkt.getMTCHRES().length() > 0)
		{
			if(FieldCount > 0)insertStr += ", ";
			insertStr += "MTCHRES = ? ";
			FieldCount++;
		}
		if(PO_admMrkt.getMTSUPNO() != null && PO_admMrkt.getMTSUPNO().length() > 0)
		{
			if(FieldCount > 0)insertStr += ", ";
			insertStr += "MTSUPNO = ? ";
			FieldCount++;
		}
		if(PO_admMrkt.getMTSUPNAME() != null && PO_admMrkt.getMTSUPNAME().length() > 0)
		{
			if(FieldCount > 0)insertStr += ", ";
			insertStr += "MTSUPNAME = ? ";
			FieldCount++;
		}
		if(PO_admMrkt.getMTSUPDATE() != null && PO_admMrkt.getMTSUPDATE().length() > 0)
		{
			if(FieldCount > 0)insertStr += ", ";
			insertStr += "MTSUPDATE = ? ";
			FieldCount++;
		}
		if(PO_admMrkt.getMTSUPTIME() != null && PO_admMrkt.getMTSUPTIME().length() > 0)
		{
			if(FieldCount > 0)insertStr += ", ";
			insertStr += "MTSUPTIME = ? ";
			FieldCount++;
		}
		if(PO_admMrkt.getMTBTNNAME() != null && PO_admMrkt.getMTBTNNAME().length() > 0)//20141202
		{
			if(FieldCount > 0)insertStr += ", ";
			insertStr += "MTBTNNAME = ? ";
			FieldCount++;
		}
		if(PO_admMrkt.getMTADDRESS() != null && PO_admMrkt.getMTADDRESS().length() > 0)//20141202
		{
			if(FieldCount > 0)insertStr += ", ";
			insertStr += "MTADDRESS = ? ";
			FieldCount++;
		}		
		if(PO_admMrkt.getMTMARQUEEPIC() != null && PO_admMrkt.getMTMARQUEEPIC().length() > 0)//20141204
		{
			if(FieldCount > 0)insertStr += ", ";
			insertStr += "MTMARQUEEPIC = ? ";
			FieldCount++;
		}
		if(PO_admMrkt.getMTMARQUEEPICADD() != null && PO_admMrkt.getMTMARQUEEPICADD().length() > 0)//20141204
		{
			if(FieldCount > 0)insertStr += ", ";
			insertStr += "MTMARQUEEPICADD = ? ";
			FieldCount++;
		}
		if(PO_admMrkt.getMTNF() != null && PO_admMrkt.getMTNF().length() > 0)//20150106
		{
			if(FieldCount > 0)insertStr += ", ";
			insertStr += "MTNF = ? ";
			FieldCount++;
		}
		if(PO_admMrkt.getMTNFSTAT() != null && PO_admMrkt.getMTNFSTAT().length() > 0)//20150106
		{
			if(FieldCount > 0)insertStr += ", ";
			insertStr += "MTNFSTAT = ? ";
			FieldCount++;
		}
		if(PO_admMrkt.getMTNFDATE() != null && PO_admMrkt.getMTNFDATE().length() > 0)//20150106
		{
			if(FieldCount > 0)insertStr += ", ";
			insertStr += "MTNFDATE = ? ";
			FieldCount++;
		}
		if(PO_admMrkt.getMTNFTIME() != null && PO_admMrkt.getMTNFTIME().length() > 0)//20150106
		{
			if(FieldCount > 0)insertStr += ", ";
			insertStr += "MTNFTIME = ? ";
			FieldCount++;
		}
		if(PO_admMrkt.getMTCF() != null && PO_admMrkt.getMTCF().length() > 0)//20150106
		{
			if(FieldCount > 0)insertStr += ", ";
			insertStr += "MTCF = ? ";
			FieldCount++;
		}
		
		if(PO_admMrkt.getMTLOAN() != null && PO_admMrkt.getMTLOAN().length() > 0)//20150106
		{
			if(FieldCount > 0)insertStr += ", ";
			insertStr += "MTLOAN = ? ";
			FieldCount++;
		}
		insertStr += "WHERE MTID = ?";
		
		Query add1=this.getSession().createSQLQuery(insertStr);
		
		if(PO_admMrkt.getMTUSERID() != null && PO_admMrkt.getMTUSERID().length() > 0)
		{
			add1.setParameter(UpdateCount, PO_admMrkt.getMTUSERID());
			UpdateCount++;
		}
		if(PO_admMrkt.getMTUSERNAME() != null && PO_admMrkt.getMTUSERNAME().length() > 0)
		{
			add1.setParameter(UpdateCount, PO_admMrkt.getMTUSERNAME());
			UpdateCount++;
		}
		if(PO_admMrkt.getMTTXDATE() != null && PO_admMrkt.getMTTXDATE().length() > 0)
		{
			add1.setParameter(UpdateCount, PO_admMrkt.getMTTXDATE());
			UpdateCount++;
		}
		if(PO_admMrkt.getMTTXTIME() != null && PO_admMrkt.getMTTXTIME().length() > 0)
		{
			add1.setParameter(UpdateCount, PO_admMrkt.getMTTXTIME());
			UpdateCount++;
		}
		if(PO_admMrkt.getMTSDATE() != null && PO_admMrkt.getMTSDATE().length() > 0)
		{
			add1.setParameter(UpdateCount, PO_admMrkt.getMTSDATE());
			UpdateCount++;
		}
		if(PO_admMrkt.getMTSTIME() != null && PO_admMrkt.getMTSTIME().length() > 0)
		{
			add1.setParameter(UpdateCount, PO_admMrkt.getMTSTIME());
			UpdateCount++;
		}
		if(PO_admMrkt.getMTEDATE() != null && PO_admMrkt.getMTEDATE().length() > 0)
		{
			add1.setParameter(UpdateCount, PO_admMrkt.getMTEDATE());
			UpdateCount++;
		}
		if(PO_admMrkt.getMTETIME() != null && PO_admMrkt.getMTETIME().length() > 0)
		{
			add1.setParameter(UpdateCount, PO_admMrkt.getMTETIME());
			UpdateCount++;
		}
		if(PO_admMrkt.getMTADTYPE() != null && PO_admMrkt.getMTADTYPE().length() > 0)
		{
			add1.setParameter(UpdateCount, PO_admMrkt.getMTADTYPE());
			UpdateCount++;
		}
		if(PO_admMrkt.getMTWEIGHT() != null && PO_admMrkt.getMTWEIGHT().length() > 0)
		{
			add1.setParameter(UpdateCount, PO_admMrkt.getMTWEIGHT());
			UpdateCount++;
		}
		if(PO_admMrkt.getMTADHLK() != null && PO_admMrkt.getMTADHLK().length() > 0)
		{
			add1.setParameter(UpdateCount, PO_admMrkt.getMTADHLK());
			UpdateCount++;
		}
		if(PO_admMrkt.getMTADCON() != null && PO_admMrkt.getMTADCON().length() > 0)
		{
			add1.setParameter(UpdateCount, PO_admMrkt.getMTADCON());
			UpdateCount++;
		}
		if(PO_admMrkt.getMTPICTYPE() != null && PO_admMrkt.getMTPICTYPE().length() > 0)
		{
			add1.setParameter(UpdateCount, PO_admMrkt.getMTPICTYPE());
			UpdateCount++;
		}
		if(PO_admMrkt.getMTRUNSHOW() != null && PO_admMrkt.getMTRUNSHOW().length() > 0)
		{
			add1.setParameter(UpdateCount, PO_admMrkt.getMTRUNSHOW());
			UpdateCount++;
		}
		if(PO_admMrkt.getMTBULLETIN() != null && PO_admMrkt.getMTBULLETIN().length() > 0)
		{
			add1.setParameter(UpdateCount, PO_admMrkt.getMTBULLETIN());
			UpdateCount++;
		}
		if(PO_admMrkt.getMTPICADD() != null && PO_admMrkt.getMTPICADD().length() > 0)
		{
			add1.setParameter(UpdateCount, PO_admMrkt.getMTPICADD());
			UpdateCount++;
		}
		if(PO_admMrkt.getMTPICDATA() != null && PO_admMrkt.getMTPICDATA().length() > 0)
		{
			add1.setParameter(UpdateCount, PO_admMrkt.getMTPICDATA());
			UpdateCount++;
		}
		if(PO_admMrkt.getMTADSTAT() != null && PO_admMrkt.getMTADSTAT().length() > 0)
		{
			add1.setParameter(UpdateCount, PO_admMrkt.getMTADSTAT());
			UpdateCount++;
		}
		if(PO_admMrkt.getMTCHRES() != null && PO_admMrkt.getMTCHRES().length() > 0)
		{
			add1.setParameter(UpdateCount, PO_admMrkt.getMTCHRES());
			UpdateCount++;
		}
		if(PO_admMrkt.getMTSUPNO() != null && PO_admMrkt.getMTSUPNO().length() > 0)
		{
			add1.setParameter(UpdateCount, PO_admMrkt.getMTSUPNO());
			UpdateCount++;
		}
		if(PO_admMrkt.getMTSUPNAME() != null && PO_admMrkt.getMTSUPNAME().length() > 0)
		{
			add1.setParameter(UpdateCount, PO_admMrkt.getMTSUPNAME());
			UpdateCount++;
		}
		if(PO_admMrkt.getMTSUPDATE() != null && PO_admMrkt.getMTSUPDATE().length() > 0)
		{
			add1.setParameter(UpdateCount, PO_admMrkt.getMTSUPDATE());
			UpdateCount++;
		}
		if(PO_admMrkt.getMTSUPTIME() != null && PO_admMrkt.getMTSUPTIME().length() > 0)
		{
			add1.setParameter(UpdateCount, PO_admMrkt.getMTSUPTIME());
			UpdateCount++;
		}
		if(PO_admMrkt.getMTBTNNAME() != null && PO_admMrkt.getMTBTNNAME().length() > 0)//20141202
		{
			add1.setParameter(UpdateCount, PO_admMrkt.getMTBTNNAME());
			UpdateCount++;
		}
		if(PO_admMrkt.getMTADDRESS() != null && PO_admMrkt.getMTADDRESS().length() > 0)//20141202
		{
			add1.setParameter(UpdateCount, PO_admMrkt.getMTADDRESS());
			UpdateCount++;
		}		
		if(PO_admMrkt.getMTMARQUEEPIC() != null && PO_admMrkt.getMTMARQUEEPIC().length() > 0)//20141204
		{
			add1.setParameter(UpdateCount, PO_admMrkt.getMTMARQUEEPIC());
			UpdateCount++;
		}
		if(PO_admMrkt.getMTMARQUEEPICADD() != null && PO_admMrkt.getMTMARQUEEPICADD().length() > 0)//20141204
		{
			add1.setParameter(UpdateCount, PO_admMrkt.getMTMARQUEEPICADD());
			UpdateCount++;
		}
		if(PO_admMrkt.getMTNF() != null && PO_admMrkt.getMTNF().length() > 0)//20150106
		{
			add1.setParameter(UpdateCount, PO_admMrkt.getMTNF());
			UpdateCount++;
		}
		if(PO_admMrkt.getMTNFSTAT() != null && PO_admMrkt.getMTNFSTAT().length() > 0)//20150106
		{
			add1.setParameter(UpdateCount, PO_admMrkt.getMTNFSTAT());
			UpdateCount++;
		}
		if(PO_admMrkt.getMTNFDATE() != null && PO_admMrkt.getMTNFDATE().length() > 0)//20150106
		{
			add1.setParameter(UpdateCount, PO_admMrkt.getMTNFDATE());
			UpdateCount++;
		}
		if(PO_admMrkt.getMTNFTIME() != null && PO_admMrkt.getMTNFTIME().length() > 0)//20150106
		{
			add1.setParameter(UpdateCount, PO_admMrkt.getMTNFTIME());
			UpdateCount++;
		}
		if(PO_admMrkt.getMTCF() != null && PO_admMrkt.getMTCF().length() > 0)//20150106
		{
			add1.setParameter(UpdateCount, PO_admMrkt.getMTCF());
			UpdateCount++;
		}
		
		// Heuristic CGI Stored XSS
//		log.debug("QueryStr ===========================================> " + add1);
		
		add1.setParameter(UpdateCount, PO_admMrkt.getMTID());

		int r2 = add1.executeUpdate();
		return(r2);
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public int registerMessage() {
		int r2 = 0;
		String[] msgIDarr = _RegistrStr.split(",");
		
		for(int i = 0; i < msgIDarr.length; i++)
		{
			String registerStr = "UPDATE ADMMRKT ";
			registerStr += "SET MTADSTAT = (CASE MTADSTAT ";
			registerStr += "WHEN '' THEN 'M' ";
			registerStr += "WHEN 'N' THEN 'R' ";
			registerStr += "END) ";
			registerStr += ", MTNFSTAT='0' ";
			registerStr += "WHERE MTID = " + msgIDarr[i];
			log.debug("registerStr ==================================================>" + registerStr);
			Query add1=this.getSession().createSQLQuery(registerStr);

			r2 += add1.executeUpdate();
		}
		return(r2);
	}
	

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public int registerSendedNotify() {
		int r2 = 0;	
		String registerStr = "UPDATE ADMMRKT ";
		registerStr += "SET MTNFSTAT = '1', ";
		registerStr += "MTNFDATE = '" + PO_admMrkt.getMTNFDATE() + "', ";
		registerStr += "MTNFTIME = '" + PO_admMrkt.getMTNFTIME() + "' ";
		registerStr += "WHERE MTID = " + PO_admMrkt.getMTID();
		log.debug("registerStr ==================================================>" + registerStr);
		Query add1=this.getSession().createSQLQuery(registerStr);

		r2 += add1.executeUpdate();
		return(r2);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public int withdrawnMessage() {
		int WithdrawnCount = 0;
		String withdrawnStr = "UPDATE ADMMRKT ";
		withdrawnStr += "SET MTADSTAT = 'N', MTCHRES = '2' ";
		withdrawnStr += "WHERE MTID IN(" + _WithdrawnStr + ")";
		Query add1=this.getSession().createSQLQuery(withdrawnStr);

		WithdrawnCount = add1.executeUpdate();
		
		return WithdrawnCount;
	}
	
	public void Reset()
	{
		setMessageType("");
		setMessageApprove("");
		setMessageStartMinDate("");
		setMessageStartMaxDate("");
		setMessageDueMinDate("");
		setMessageDueMaxDate("");
		setMessageTitle("");
	}
}

