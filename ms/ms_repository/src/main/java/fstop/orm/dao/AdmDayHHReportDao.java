package fstop.orm.dao;

import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.LegacyJpaRepository;

import fstop.orm.po.ADMDAYHHREPORT;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Transactional
public class AdmDayHHReportDao extends LegacyJpaRepository<ADMDAYHHREPORT, Long> {
	protected Logger logger = Logger.getLogger(getClass());
	
	
//	@Transactional(propagation = Propagation.NEVER)
	@Transactional
	public void execCalProcedure(String yyyy, String mm ,String dd) {
		Query query = getSession().createSQLQuery("{call CAL_ADMDAYHHREPORT(?,?,?)}");
		query.setParameter(0, yyyy);
		query.setParameter(1, mm);
		query.setParameter(2, dd);
		query.executeUpdate();		
	}
	
}
