package fstop.orm.dao;

import com.google.gson.Gson;
import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.orm.po.TXNTWSCHPAYDATA;
import fstop.orm.po.TXNTWSCHPAYDATA_PK;
import fstop.orm.po.TxnTwSchPayDataErrorCount;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang.StringEscapeUtils;
import org.hibernate.SQLQuery;
import org.hibernate.query.NativeQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.joda.time.DateTime;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import javax.transaction.Transactional;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Slf4j
@Repository
public class TxnTwSchPayDataDao extends LegacyJpaRepository<TXNTWSCHPAYDATA, TXNTWSCHPAYDATA_PK> {
	// private Logger log = LoggerFactory.getLogger(this.getClass());

	/**
	 * 取得某日期的台幣預約 (一扣)
	 *
	 * @param today
	 *            格式為 yyyyMMdd
	 * @param limit
	 *            一次傳回固定筆數，如無符合條件則回無筆數的list
	 * @return
	 */
	public List<TXNTWSCHPAYDATA> findByDateAndFirst(String today, int limit) {

		String sql = "FROM TXNTWSCHPAYDATA where pks.DPSCHTXDATE=:today and (DPRESEND is null or DPRESEND = '' or DPRESEND='0' )";

		Query query = getQuery(sql);
		query.setParameter("today", today);

		return query.getResultList();
	}

	// public List<TXNTWSCHPAYDATA> findByDateAndFirst(String today) {
	//
	//// String sql = "FROM TXNTWSCHPAYDATA m where m.pks.DPSCHTXDATE=:today and
	// (m.DPRESEND is null or m.DPRESEND = '' or m.DPRESEND='0' )";
	// String sql = "FROM TXNTWSCHPAYDATA m where m.pks.DPSCHTXDATE=:today and
	// m.MSADDR = 'ms_tw' and (m.DPRESEND is null or m.DPRESEND = '' or
	// m.DPRESEND='0' )";
	//
	// Query query = getQuery(sql);
	// query.setParameter("today", today);
	//
	// return query.getResultList();
	// }

	// 2019/10/01註解 BEN , 改成下面的function
	// public List<TXNTWSCHPAYDATA> findByDateAndFirst(String today , String msaddr)
	// {
	// StringBuffer sql = new StringBuffer();
	// sql.append(" SELECT * FROM TXNTWSCHPAYDATA s ");
	// sql.append(" Where exists (SELECT DPSCHNO FROM TXNTWSCHPAY ");
	// sql.append(" where DPSCHNO = s.DPSCHNO and DPTXSTATUS = '0' And
	// (LOGINTYPE='NB') AND MSADDR=:msaddr) ");
	// sql.append(" And (s.DPSCHTXDATE =:today) ");
	// sql.append(" And (s.DPTXSTATUS = '') ");
	// log.debug("findByDateAndFirst.sql>>{}",sql);
	// Query query = getSession().createNativeQuery(sql.toString(),
	// TXNTWSCHPAYDATA.class);
	//// SQLQuery query = (SQLQuery) getSession().createNativeQuery(sql.toString());
	//// query.setResultTransformer(Transformers.aliasToBean(TXNTWSCHPAYDATA.class));
	// query.setParameter("today", today);
	// query.setParameter("msaddr", msaddr);
	//
	// return query.getResultList();
	// }
	/**
	 * 台幣一扣找出當日全部列表_1
	 *
	 * @return
	 */
	public List<TXNTWSCHPAYDATA> findByDateAndFirst(String today) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT * FROM TXNTWSCHPAYDATA s ");
		sql.append(" Where exists (SELECT DPSCHNO FROM TXNTWSCHPAY  ");
		sql.append("       where DPSCHNO = s.DPSCHNO and DPTXSTATUS = '0' and DPUSERID=s.DPUSERID ) ");
		sql.append(" And (s.DPSCHTXDATE =:today) ");
		sql.append(" And  (s.DPTXSTATUS = '')");
		sql.append(" And  (s.ADTXNO = '')");
		log.debug("findByDateAndFirst.sql>>{}", sql);
		Query query = getSession().createNativeQuery(sql.toString(), TXNTWSCHPAYDATA.class);
		// SQLQuery query = (SQLQuery) getSession().createNativeQuery(sql.toString());
		// query.setResultTransformer(Transformers.aliasToBean(TXNTWSCHPAYDATA.class));
		query.setParameter("today", today);

		return query.getResultList();
	}

	/**
	 * 台幣一扣找出當日全部列表_2 ( 已加上交易序號 )
	 *
	 * @return
	 */
	public List<TXNTWSCHPAYDATA> findByDateAndExec(String today) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT * FROM TXNTWSCHPAYDATA s ");
		sql.append(" Where exists (SELECT DPSCHNO FROM TXNTWSCHPAY  ");
		sql.append("       where DPSCHNO = s.DPSCHNO and DPTXSTATUS = '0' and DPUSERID=s.DPUSERID )  ");
		sql.append(" And (s.DPSCHTXDATE =:today) ");
		sql.append(" And  (s.DPTXSTATUS = '') ");
		sql.append(" And  (s.ADTXNO != '' or s.ADTXNO is not null ) ");
		Query query = getSession().createNativeQuery(sql.toString(), TXNTWSCHPAYDATA.class);
		// SQLQuery query = (SQLQuery) getSession().createNativeQuery(sql.toString());
		// query.setResultTransformer(Transformers.aliasToBean(TXNTWSCHPAYDATA.class));
		query.setParameter("today", today);

		return ESAPIUtil.validStrList(query.getResultList());
	}

	/**
	 * 台幣二扣找出當日全部列表 ( 已加上交易序號 )
	 *
	 * @return
	 */
	public List<TXNTWSCHPAYDATA> SecondCallList(String today) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT * FROM TXNTWSCHPAYDATA s ");
		sql.append(" Where exists (SELECT DPSCHNO FROM TXNTWSCHPAY  ");
		sql.append("       where DPSCHNO = s.DPSCHNO and DPTXSTATUS = '0' and DPUSERID=s.DPUSERID)  ");
		sql.append(" And (s.DPSCHTXDATE =:today) ");
		sql.append(" And  (s.DPTXSTATUS = '2') ");
		sql.append(" And  (s.DPEXCODE != '') ");
		sql.append(" And  (s.ADTXNO != '' or s.ADTXNO is not null ) ");
		Query query = getSession().createNativeQuery(sql.toString(), TXNTWSCHPAYDATA.class);
		// SQLQuery query = (SQLQuery) getSession().createNativeQuery(sql.toString());
		// query.setResultTransformer(Transformers.aliasToBean(TXNTWSCHPAYDATA.class));
		query.setParameter("today", today);
		List<TXNTWSCHPAYDATA> list = (List<TXNTWSCHPAYDATA>) query.getResultList();
		for (TXNTWSCHPAYDATA each : list) {
			each.setDPTITAINFO("");
		}
		return ESAPIUtil.validStrList(list);
	}

	// 判斷是否有預約
	public String getDpschno(String cusidn, String bankcod, String acn) {
		List<TXNTWSCHPAYDATA> po = null;
		TXNTWSCHPAYDATA p_date = null;
		TXNTWSCHPAYDATA_PK p_key = null;
		String result = null;
		try {
			log.debug("TXNTWSCHPAYDATA_Dao >> getAll");
			// 今天之後
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String today = sdf.format(date);
			log.trace("DPSCHTXDATE_today>>>{}", today);
			String queryString = "SELECT * FROM TXNTWSCHPAYDATA WHERE DPUSERID  = :cusidn AND "
					+ " DPSVBH = :dpsvbh AND DPSVAC = :dpsvac AND DPSCHTXDATE >= :dpschtxdate";
			Query query = getSession().createQuery(queryString);
			query.setParameter("cusidn", cusidn);
			query.setParameter("dpsvbh", bankcod);
			query.setParameter("dpsvac", acn);
			query.setParameter("dpschtxdate", today);
			po = query.getResultList();
			if (po.size() != 0) {
				p_date = po.get(0);
				p_key = p_date.getPks();
				result = p_key.getDPSCHNO();
			} else {
				result = "";
			}
		} catch (Exception e) {
		}
		return result;
	}

	@Transactional
	public void updateStatus(TXNTWSCHPAYDATA data, String status, String excode) {
		log.debug("update TXNTWSCHPAYDATA(DPSCHNO: " + data.getPks().getDPSCHNO() + " DPSCHTXDATE:"
				+ data.getPks().getDPSCHTXDATE() + " DPUSERID:" + data.getPks().getDPUSERID() + ") status: " + status
				+ " , excode: " + excode);
		try {
			Date d = new Date();
			data.setLASTDATE(DateTimeUtils.format("yyyyMMdd", d));
			data.setLASTTIME(DateTimeUtils.format("HHmmss", d));
			data.setDPTXSTATUS(status);
			if (null != excode || !"".equals(excode)) {
				data.setDPEXCODE(excode);
			}


			update(data);
		} catch (Exception e) {
			log.error("TxnTwTransfer updateStatus Error.", e);
		}
	}

	public List<TXNTWSCHPAYDATA> findScheduleDueTo(String execDate) {
		log.debug("exec Date >> {}", execDate);

		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT * FROM TXNTWSCHPAYDATA s ");
		sql.append(" Where exists (SELECT DPSCHNO FROM TXNTWSCHPAY  ");
		sql.append("       where DPSCHNO = s.DPSCHNO and DPTXSTATUS = '0' and DPUSERID = s.DPUSERID )  ");
		sql.append(" And (s.DPSCHTXDATE =:execDate ) ");

		Query query = getSession().createNativeQuery(sql.toString(), TXNTWSCHPAYDATA.class);
		query.setParameter("execDate", execDate);

		return ESAPIUtil.validStrList(query.getResultList());
	}

	public List<TXNTWSCHPAYDATA> findByDPSCHNO(String DPSCHNO) {
		log.debug("DPSCHNO >> {}", DPSCHNO);
		String sql = "FROM TXNTWSCHPAYDATA where pks.DPSCHNO=:dpschno";
		Query query = getQuery(sql);
		query.setParameter("dpschno", DPSCHNO);

		return ESAPIUtil.validStrList(query.getResultList());
	}

	public List<TXNTWSCHPAYDATA> findErrorList(String today) {
		String sql = "FROM TXNTWSCHPAYDATA where pks.DPSCHTXDATE=:today and DPTXSTATUS not in('','0')";

		Query query = getQuery(sql);
		query.setParameter("today", today);
		
		if (query.getResultList().size() != 0) {
			return escapeList(query.getResultList());
		} else {
			return new ArrayList();
		}

	}
	
	public List<TXNTWSCHPAYDATA> findErrorList2nd(String today) {

		String sql = "FROM TXNTWSCHPAYDATA where pks.DPSCHTXDATE=:today and LASTTIME>'100000' and DPTXSTATUS not in('','0')";

		Query query = getQuery(sql);
		query.setParameter("today", today);

		if (query.getResultList().size() != 0) {
			return escapeList(query.getResultList());
		} else {
			return new ArrayList();
		}

	}

	/**
	 * according type to get data type >> 1 total type >> 2 success type >> 3 custom
	 * error type >> 4 system error type >> 5 total Failed
	 * 
	 */
	public int countMethod(int type) {
		DateTime now = new DateTime();
		String today = now.toString("yyyyMMdd");
		String sql = "";
		switch (type) {
		case 1:
			// 全部 過濾DPTXSTATUS為空 ( 日期為今天 但無執行 表示主表取消 )
			sql += "select count(*) as COUNT from TXNTWSCHPAYDATA where DPTXSTATUS<>'' and DPSCHTXDATE=:today and LASTDATE=:today and LASTTIME<'100000'";
			break;
		case 2:
			// 成功
			sql += "select count(*) as COUNT from TXNTWSCHPAYDATA where DPTXSTATUS='0' and DPSCHTXDATE=:today and LASTDATE=:today and LASTTIME<'100000'";
			break;
		case 3:
			// 客戶面錯誤
			sql += "select count(*) as COUNT from TXNTWSCHPAYDATA where (DPEXCODE like 'E%' or DPEXCODE like 'L%' or DPEXCODE like '1%') and DPSCHTXDATE=:today and LASTDATE=:today and LASTTIME<'100000'";
			break;
		case 4:
			// 系統面錯誤
			sql += "select count(*) as COUNT from TXNTWSCHPAYDATA where (DPEXCODE like 'Z%' or DPEXCODE like 'FE%' or DPEXCODE like 'MAC%' or DPEXCODE='3303' ) and DPSCHTXDATE=:today and LASTDATE=:today and LASTTIME<'100000'";
			break;
		case 5:
			// 失敗
			sql += "select count(*) as COUNT from TXNTWSCHPAYDATA where DPTXSTATUS not in ('','0') and DPSCHTXDATE=:today and LASTDATE=:today and LASTTIME<'100000'";
			break;
		}
		Query query = getSession().createSQLQuery(sql);
		query.setParameter("today", today);
		query.unwrap(NativeQuery.class).addScalar("COUNT", IntegerType.INSTANCE);
		List result = ESAPIUtil.validStrList(query.getResultList());

		return (Integer) result.get(0);
	}
	
	/**
	 * according type to get data type >> 1 total type >> 2 success type >> 3 custom
	 * error type >> 4 system error type >> 5 total Failed
	 * 
	 */
	public int countMethod2nd(int type) {
		DateTime now = new DateTime();
		String today = now.toString("yyyyMMdd");
		String sql = "";
		switch (type) {
		case 1:
			// 全部 過濾DPTXSTATUS為空 ( 日期為今天 但無執行 表示主表取消 )
			sql += "select count(*) as COUNT from TXNTWSCHPAYDATA where DPTXSTATUS<>'' and DPSCHTXDATE=:today and LASTDATE=:today and LASTTIME>'100000'";
			break;
		case 2:
			// 成功
			sql += "select count(*) as COUNT from TXNTWSCHPAYDATA where DPTXSTATUS='0' and DPSCHTXDATE=:today and LASTDATE=:today and LASTTIME>'100000'";
			break;
		case 3:
			// 客戶面錯誤
			sql += "select count(*) as COUNT from TXNTWSCHPAYDATA where (DPEXCODE like 'E%' or DPEXCODE like 'L%' or DPEXCODE like '1%') and DPSCHTXDATE=:today and LASTDATE=:today and LASTTIME>'100000'";
			break;
		case 4:
			// 系統面錯誤
			sql += "select count(*) as COUNT from TXNTWSCHPAYDATA where (DPEXCODE like 'Z%' or DPEXCODE like 'FE%' or DPEXCODE like 'MAC%' or DPEXCODE='3303' ) and DPSCHTXDATE=:today and LASTDATE=:today and LASTTIME>'100000'";
			break;
		case 5:
			// 失敗
			sql += "select count(*) as COUNT from TXNTWSCHPAYDATA where DPTXSTATUS not in ('','0') and DPSCHTXDATE=:today and LASTDATE=:today and LASTTIME>'100000'";
			break;
		}
		Query query = getSession().createSQLQuery(sql);
		query.setParameter("today", today);
		query.unwrap(NativeQuery.class).addScalar("COUNT", IntegerType.INSTANCE);
		List result = ESAPIUtil.validStrList(query.getResultList());

		return (Integer) result.get(0);
	}

	public List<TxnTwSchPayDataErrorCount> countErrorList(String today) {
		List<Map<String, String>> resultList = new ArrayList<>();
		String sql = "select A.DPEXCODE as DPEXCODE,B.ADMSGIN as DPEXCODEMSG ,count(A.DPEXCODE) as TIMES from TXNTWSCHPAYDATA as A,ADMMSGCODE B where A.LASTDATE=:today and A.DPEXCODE<>'' and A.DPEXCODE=B.ADMCODE group by A.DPEXCODE,B.ADMSGIN";
		Query query = getSession().createNativeQuery(sql, TxnTwSchPayDataErrorCount.class);
		query.setParameter("today", today);

		if (query.getResultList().size() != 0) {
			return ESAPIUtil.validStrList(query.getResultList());
		} else {
			return new ArrayList();
		}
	}
	
	public List<TxnTwSchPayDataErrorCount> countErrorList2nd(String today) {
		List<Map<String, String>> resultList = new ArrayList<>();
		String sql = "select A.DPEXCODE as DPEXCODE,B.ADMSGIN as DPEXCODEMSG ,count(A.DPEXCODE) as TIMES from TXNTWSCHPAYDATA as A,ADMMSGCODE B where A.LASTDATE=:today and A.LASTTIME>'100000' and A.DPEXCODE<>'' and A.DPEXCODE=B.ADMCODE group by A.DPEXCODE,B.ADMSGIN";
		Query query = getSession().createNativeQuery(sql, TxnTwSchPayDataErrorCount.class);
		query.setParameter("today", today);

		if (query.getResultList().size() != 0) {
			return ESAPIUtil.validStrList(query.getResultList());
		} else {
			return new ArrayList();
		}
	}

	public List<TXNTWSCHPAYDATA> findScheduleNextDateAMT(Date currentDate) {
		String nextday = DateTimeUtils.format("yyyyMMdd", currentDate);

		// 不判斷此預約主表是否取消 ( 景森說寧可多準備 )
		String sql = "FROM TXNTWSCHPAYDATA where DPSCHTXDATE=:nextday and DPSVBH <> '050'";
		Query query = getQuery(sql);
		query.setParameter("nextday", nextday);

		if (query.getResultList().size() != 0) {
			return ESAPIUtil.validStrList(query.getResultList());
		} else {
			return new ArrayList();
		}

		// 判斷此預約主表是否取消 ( 不用此 )
//		StringBuffer sql = new StringBuffer();
//		sql.append(" SELECT * FROM TXNTWSCHPAYDATA s ");
//		sql.append(" Where exists (SELECT DPSCHNO FROM TXNTWSCHPAY  ");
//		sql.append("       where DPSCHNO = s.DPSCHNO and DPTXSTATUS = '0' and DPUSERID=s.DPUSERID )  ");
//		sql.append(" And (s.DPSCHTXDATE =:nextday) ");
//		sql.append(" And  (s.DPTXSTATUS = '') ");
//		sql.append(" And  (s.DPSVBH <> '050') ");
//		Query query = getSession().createNativeQuery(sql.toString(), TXNTWSCHPAYDATA.class);
//		// SQLQuery query = (SQLQuery) getSession().createNativeQuery(sql.toString());
//		// query.setResultTransformer(Transformers.aliasToBean(TXNTWSCHPAYDATA.class));
//		query.setParameter("nextday", nextday);
//
//		return ESAPIUtil.validStrList(query.getResultList());
	}
	
	public static <T> List<T> escapeList(List<T> s) {
		List<T> l = new LinkedList();
		Class<Object> T;
		String json = "";
		String vStr = "";
		try {
			for (T each : s) {
				json = new Gson().toJson(each);
				vStr = StringEscapeUtils.escapeSql(json);
//				vStr = StringEscapeUtils.escapeHtml(json);
				// System.out.println("JSON :"+json);
				if (json.equals(vStr)) {
					l.add(each);
				} else {
					log.error("validate failed");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return l;
	}

}
