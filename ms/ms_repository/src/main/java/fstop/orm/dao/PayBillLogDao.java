package fstop.orm.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.query.NativeQuery;
import org.springframework.stereotype.Repository;

import com.netbank.domain.orm.core.LegacyJpaRepository;

import fstop.orm.po.PAYBILLLOG;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
public class PayBillLogDao extends LegacyJpaRepository<PAYBILLLOG, Long> {

	public List<PAYBILLLOG> getAll() {
		return find("FROM PAYBILLLOG ORDER BY LOGID");
	}

	public void deleteAll() {
		this.getSession().createSQLQuery("DELETE FROM PAYBILLLOG").executeUpdate();
	}

	@SuppressWarnings({ "unused", "unchecked" })
	public List<PAYBILLLOG> findByDate(String LOGDATE) {

		String sql = "from PAYBILLLOG where LOGDATE = ? and admcode in ('','0000') order by LOGDATE";

		Query query = getQuery(sql, LOGDATE);

		return query.getResultList();
	}

	public List<PAYBILLLOG> findByDateRange(String startdate, String enddate) {

		String sql = "from PAYBILLLOG where LOGDATE >= ? and LOGDATE <= ? and admcode in ('','0000') order by LOGDATE";

		Query query = getQuery(sql, startdate, enddate);

		return query.getResultList();
	}

	public List<PAYBILLLOG> findByMonth(String LOGDATE) {

		String sql = "select a.* from PAYBILLLOG a where a.LOGDATE like '" + LOGDATE
				+ "' and admcode in ('','0000') order by a.LOGDATE";
		Query query = getSession().createSQLQuery(sql);
		query.unwrap(NativeQuery.class).addEntity("a", PAYBILLLOG.class);

		return query.getResultList();
	}

	/**
	 * 
	 * 
	 * @param CARDNUM  信用卡卡號（銷帳編號16)
	 * @param PAYAMT   轉帳金額(9)
	 * @param BANKID   銀行代碼(3)
	 * @param ACCOUNT  帳號(11)
	 * @param REMOTEIP
	 * @param LOGDATE
	 * @return
	 */
	public PAYBILLLOG findByRec(String CARDNUM, String PAYAMT, String BANKID, String ACCOUNT, String REMOTEIP,
			String LOGDATE) {
		List<PAYBILLLOG> result = null;
		try {
			result = find("from PAYBILLLOG where CARDNUM = ? and PAYAMT = ? and BANKID = ? and ACCOUNT = ? and REMOTEIP = ? and LOGDATE = ? ", CARDNUM,PAYAMT,BANKID,ACCOUNT,REMOTEIP,LOGDATE);
		} catch (Exception e) {
			log.debug("findByRec Exception");
		}
		if (result!=null && result.size()>0)
			return result.get(0);
		else
			return null;

	}
}
