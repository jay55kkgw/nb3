package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;
import fstop.core.BeanUtils;
import fstop.orm.po.SYSLOG;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.*;

@Slf4j
@Repository
@Transactional
public class SysLogDao extends LegacyJpaRepository<SYSLOG, Long> {
	protected Logger logger = Logger.getLogger(getClass());

	static Set fxAdopid = new HashSet();
	
	static {
		fxAdopid.add("F001");
		fxAdopid.add("F002");
		fxAdopid.add("F003");
		fxAdopid.add("F004");
		fxAdopid.add("N174");
		fxAdopid.add("N175");
		fxAdopid.add("N177");
		fxAdopid.add("N178");
		fxAdopid.add("N510");
		fxAdopid.add("N520");
		fxAdopid.add("N530");
		fxAdopid.add("N550");
		fxAdopid.add("N551");
		fxAdopid.add("N555");
		fxAdopid.add("N556");
		fxAdopid.add("N557");
		fxAdopid.add("N558");
		fxAdopid.add("N559");
		fxAdopid.add("N559MENU");
		fxAdopid.add("N560");
		fxAdopid.add("N563");
		fxAdopid.add("N564");
		fxAdopid.add("N565");
		fxAdopid.add("N566");
		fxAdopid.add("N567");
	}
	
	public List findAllByPage(int start, int limit) {
		return (List)pagedQuery("FROM SYSLOG ", (start / limit) + 1, limit).getResult();
	}
	
	public long getTotalCount() {
		return (Long)(find("SELECT COUNT(*) FROM SYSLOG")).get(0);
	}
	
	public List findAllADEXCODE() {
		String condition = "  ";
		Query q = this.getSession().createQuery("SELECT ADMCODE FROM ADMMSGCODE");
		
		return q.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public Page getFindByDateRangePageObjectForB401(String startdt, String enddt, SYSLOG log, int start, int limit, String LOGINTYPE) {
		List<String> condition = new ArrayList();
		List<String> params = new ArrayList();
		
		//condition.add(" ADFDATE||ADFTIME >= ? and ADTDATE||ADTTIME <= ? AND length(ADLOGGING) > 0 ");
		
		condition.add(" ADFDATE||ADFTIME >= ? and ADTDATE||ADTTIME <= ? ");
		params.add(startdt);
		params.add(enddt);
		
		String[] values = new String[params.size()];
		params.toArray(values);

		StringBuffer c = new StringBuffer();
		
		for(String s : condition) {
			if(s.trim().length() > 0) {
				c.append(s);
				c.append(" AND ");
			}	
		}
//		log.debug("ForB401condition1===>"+condition+"<==");		
		if (! log.getADEXCODE().equals(""))
			c.append(" ADEXCODE = '"+log.getADEXCODE() + "'");
		else
			c.append(" ADEXCODE <> '' ");
		
		/**
		 * 交易來源
		 */
		if(LOGINTYPE.equals("'','NB'"))
		{
			c.append(" AND  LOGINTYPE in "+"("+LOGINTYPE+") ");
		}
		else if(LOGINTYPE.equals("'MB'"))
		{
			c.append(" AND  LOGINTYPE in "+"("+LOGINTYPE+") ");
		}
		else
		{
			c.append(" AND  LOGINTYPE in "+"("+LOGINTYPE+") ");
		}
		return pagedQuery("FROM SYSLOG " +
				"WHERE " +
				"" + c.toString() + 
				" " +
				"" +
				"", (start / limit) + 1, limit, values);
	}

	@SuppressWarnings("unchecked")
	public Page getFindByDateRangePageObjectForB401(String startdt, String enddt, SYSLOG log, int start, int limit, String txtype, String LOGINTYPE) {
		List<String> condition = new ArrayList();
		List<String> params = new ArrayList();
		
		//condition.add(" ADFDATE||ADFTIME >= ? and ADTDATE||ADTTIME <= ? AND length(ADLOGGING) > 0 ");
		
		condition.add(" ADFDATE||ADFTIME >= ? and ADTDATE||ADTTIME <= ? ");
		params.add(startdt);
		params.add(enddt);
		
		String[] values = new String[params.size()];
		params.toArray(values);

		StringBuffer c = new StringBuffer();
		
		for(String s : condition) {
			if(s.trim().length() > 0) {
				c.append(s);
				c.append(" AND ");
			}	
		}
//		log.debug("ForB401condition===>"+condition+"<===");		
		if (! log.getADEXCODE().equals(""))
			c.append(" ADEXCODE = '"+log.getADEXCODE() + "'");
		else
			c.append(" ADEXCODE <> '' ");
		
		
		/**
		 *  <交易種類>之條件判斷
		 */
		if (! txtype.equals("*")) {
			StringBuffer sql = new StringBuffer();
			
			if (txtype.equals("fx")) 
				sql.append(" AND adopid in (");
			else if (txtype.equals("tw")) 
				sql.append(" AND adopid not in (");
				
			Iterator<String> it = fxAdopid.iterator();
			int i=0;
			
			while(it.hasNext())
			{
				if (i > 0)
					sql.append(",");
					
				sql.append("'");
				sql.append(it.next());
				sql.append("'");
					
				i++;
			}
				
			sql.append(")");
			
			c.append(sql.toString());
		}
		
	
		/**
		 * 交易來源
		 */
		if(LOGINTYPE.equals("'','NB'"))
		{
			c.append(" AND  LOGINTYPE in "+"("+LOGINTYPE+") ");
		}
		else if(LOGINTYPE.equals("'MB'"))
		{
			c.append(" AND  LOGINTYPE in "+"("+LOGINTYPE+") ");
		}
		else
		{
			c.append(" AND  LOGINTYPE in "+"("+LOGINTYPE+") ");
		}
//		if(StrUtils.isNotEmpty(LOGINTYPE)) {
//			condition.add(" AND LOGINTYPE = ? ");
//			params.add(LOGINTYPE);
//		}
		
		return pagedQuery("FROM SYSLOG " +
				"WHERE " +
				"" + c.toString() + 
				" " +
				"" +
				"", (start / limit) + 1, limit, values);
	}
	
	@SuppressWarnings("unchecked")
	public Page getFindByDateRangePageObject(String startdt, String enddt, SYSLOG log, int start, int limit) {
		List<String> condition = new ArrayList();
		List<String> params = new ArrayList();
		
		condition.add(" ADFDATE||ADFTIME >= ? and ADFDATE||ADFTIME <= ? ");
		params.add(startdt);
		params.add(enddt);
		
		
		try {
			Map mparams = BeanUtils.describe(log);
			
			mparams.remove("ADFDATE");
			mparams.remove("ADFTIME");
			mparams.remove("ADTDATE");
			mparams.remove("ADTTIME");

			mparams.remove("class");
			mparams.remove("COMP_ID");
			
			for(Object s : mparams.keySet()) {
				String key = (String)s;
				if(mparams.get(key) == null)
					continue;
				try {
					String v = (String)mparams.get(key).toString();
					if(StrUtils.isNotEmpty(v)) {
						condition.add(" " + key + " = ? ");
						params.add(v);
					}
				}
				catch(Exception e){}
			}
			
		}
		catch(Exception e){}
		
		
		String[] values = new String[params.size()];
		params.toArray(values);
		
		StringBuffer c = new StringBuffer();
		for(String s : condition) {
			if(c.length() > 0)
				c.append(" AND ");
			c.append(s);
		}
		
		return pagedQuery("FROM SYSLOG " +
				"WHERE " +
				"" + c.toString() + 
				" " +
				"" +
				"", (start / limit) + 1, limit, values);
	}
	
	@SuppressWarnings("unchecked")
	public Page getFindByDateRangePageObjectForB201(String startdt, String enddt, String ADUSERID, String ADTXTYPE, String ADEXCODE, int start, int limit, String LOGINTYPE) {
		List<String> condition = new ArrayList();
		List<String> params = new ArrayList();
		
		condition.add(" ADFDATE||ADFTIME >= ? and ADFDATE||ADFTIME <= ? ");
		params.add(startdt);
		params.add(enddt);
		
		if(StrUtils.isNotEmpty(ADUSERID)) {
			condition.add(" ADUSERID = ? ");
			params.add(ADUSERID);
		}
		if(StrUtils.isNotEmpty(ADTXTYPE)) {
			condition.add(" ADTXTYPE = ? ");
			params.add(ADTXTYPE);
		}
		else {
			condition.add(" ADTXTYPE<>'F' ");
		}
		if(StrUtils.isNotEmpty(ADEXCODE)) {
			condition.add(" ADEXCODE = ? ");
			params.add(ADEXCODE);
		}
		if(StrUtils.isNotEmpty(LOGINTYPE)) {
			condition.add(" LOGINTYPE in ("+LOGINTYPE+") ");
			params.add(LOGINTYPE);
		}
		
		condition.add(" length(ADLOGGING) > 0 ");
		
		String[] values = new String[params.size()];
		params.toArray(values);
		
		StringBuffer c = new StringBuffer();
		for(String s : condition) {
			if(c.length() > 0)
				c.append(" AND ");
			c.append(s);
		}
		log.debug("ForB201condition===>"+condition+"<===");
		/**
		 * 交易來源
		 */
		if(LOGINTYPE.equals("'','NB'"))
		{
			c.append(" AND  LOGINTYPE in "+"("+LOGINTYPE+") ");
		}
		else if(LOGINTYPE.equals("'MB'"))
		{
			c.append(" AND  LOGINTYPE in "+"("+LOGINTYPE+") ");
		}
		else
		{
			c.append(" AND  LOGINTYPE in "+"("+LOGINTYPE+") ");
		}
		return pagedQuery("FROM SYSLOG " +
				"WHERE " +
				"" + c.toString() + 
				" " +
				"" +
				"", (start / limit) + 1, limit, values);
	}
	
	public List findByDateRange(String startdt, String enddt, SYSLOG log, int start, int limit) {
		return (List)getFindByDateRangePageObject(startdt, enddt, log, start, limit).getResult();
	}
	
	public Long findByDateRangeCount(String startdt, String enddt, SYSLOG log, int start, int limit) {
		return getFindByDateRangePageObject(startdt, enddt, log, start, limit).getTotalCount();
	}
	
	public List findByDateRangeForB401(String startdt, String enddt, SYSLOG log, int start, int limit, String logintype) {
		return (List)getFindByDateRangePageObjectForB401(startdt, enddt, log, start, limit, logintype).getResult();
	}

	public List findByDateRangeForB401(String startdt, String enddt, SYSLOG log, int start, int limit, String txtype, String logintype) {
		return (List)getFindByDateRangePageObjectForB401(startdt, enddt, log, start, limit, txtype, logintype).getResult();
	}
	
	public Long findByDateRangeCountForB401(String startdt, String enddt, SYSLOG log, int start, int limit, String logintype) {
		return getFindByDateRangePageObjectForB401(startdt, enddt, log, start, limit, logintype).getTotalCount();
	}

	public Long findByDateRangeCountForB401(String startdt, String enddt, SYSLOG log, int start, int limit, String txtype, String logintype) {
		return getFindByDateRangePageObjectForB401(startdt, enddt, log, start, limit, txtype, logintype).getTotalCount();
	}
	
	public List findByDateRangeForB201(String startdt, String enddt, String ADUSERID, String ADTXTYPE, String ADEXCODE, int start, int limit, String logintype) {
		return (List)getFindByDateRangePageObjectForB201(startdt, enddt, ADUSERID, ADTXTYPE, ADEXCODE, start, limit, logintype).getResult();
	}
	
	public Long findByDateRangeCountForB201(String startdt, String enddt, String ADUSERID, String ADTXTYPE, String ADEXCODE, int start, int limit, String logintype) {
		return getFindByDateRangePageObjectForB201(startdt, enddt, ADUSERID, ADTXTYPE, ADEXCODE, start, limit, logintype).getTotalCount();
	}
	public List<SYSLOG> getLoginType(String uid){
		return find("FROM SYSLOG WHERE LOGINTYPE<>'' and ADUSERID=?",uid);
	}
}
