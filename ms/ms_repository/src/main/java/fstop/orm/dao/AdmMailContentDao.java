package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import fstop.orm.po.ADMMAILCONTENT;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Slf4j
@Repository
@Transactional
public class AdmMailContentDao extends LegacyJpaRepository<ADMMAILCONTENT, Long> {
	protected Logger logger = Logger.getLogger(getClass());
	
	
}
