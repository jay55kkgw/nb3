package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;
import fstop.orm.po.TXNPHONETOKEN;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Repository
@Transactional
public class TxnPhoneTokenDao extends LegacyJpaRepository<TXNPHONETOKEN, String> {
	protected Logger logger = Logger.getLogger(getClass());
	
	public List<TXNPHONETOKEN> getAndroid() {
		return find("FROM TXNPHONETOKEN WHERE PHONETYPE = 'A' AND NOTIFYAD = 'Y' AND PHONETOKEN <> ''");
	}

	public List<TXNPHONETOKEN> getIOS() {
		return find("FROM TXNPHONETOKEN WHERE PHONETYPE = 'I' AND NOTIFYAD = 'Y' AND PHONETOKEN <> ''");
	}
	
	public TXNPHONETOKEN getADPhoneToken(String dpuserid) {
		List<TXNPHONETOKEN> result = find("FROM TXNPHONETOKEN WHERE DPUSERID = ? AND NOTIFYAD = 'Y'", dpuserid);
		if (result.size()>0)
			return result.get(0);
		else
			return null;
	}

	public TXNPHONETOKEN getPhoneToken(String dpuserid) {
		List<TXNPHONETOKEN> result = find("FROM TXNPHONETOKEN WHERE DPUSERID = ? AND NOTIFYTRANS = 'Y'", dpuserid);
		if (result.size()>0)
			return result.get(0);
		else
			return null;
//		return (TXNPHONETOKEN) find("FROM TXNPHONETOKEN WHERE DPUSERID = ?",
//				dpuserid).get(0);
	}

	@SuppressWarnings( { "unused", "unchecked" })
	public List<TXNPHONETOKEN> findByPhoneType(String type) {
		String sql = "SELECT PHONETOKEN FROM TxnPhoneToken WHERE PHONETYPE=? AND PHONETOKEN <> ''";
		Query query = getSession().createSQLQuery(sql);
		query.setParameter(0, type);
		return query.getResultList();
	}
	
	public int findTotalCount() {
		String sql = "SELECT COUNT(*) FROM TxnPhoneToken WHERE PHONETOKEN <> ''";
		Query query = getSession().createSQLQuery(sql);
		List result = query.getResultList();
		return (Integer)result.get(0);
	}
	
	public int findNFADCount() {
		String sql = "SELECT COUNT(*) FROM TxnPhoneToken WHERE NOTIFYAD='Y' AND PHONETOKEN <> ''";
		Query query = getSession().createSQLQuery(sql);
		List result = query.getResultList();
		return (Integer)result.get(0);
	}
	
	public int findNFTRANSCount() {
		String sql = "SELECT COUNT(*) FROM TxnPhoneToken WHERE NOTIFYTRANS='Y' AND PHONETOKEN <> ''";
		Query query = getSession().createSQLQuery(sql);
		List result = query.getResultList();
		return (Integer)result.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public Page findByUserID(String DPUSERID, int start, int limit) {
		List<String> condition = new ArrayList();
		List<String> params = new ArrayList();
		
		if(StrUtils.isNotEmpty(DPUSERID)) {
			condition.add(" DPUSERID = ? ");
			params.add(DPUSERID);
		}
		
		String[] values = new String[params.size()];
		params.toArray(values);
		
		condition.add(" 1 = 1 ");
		
		StringBuffer c = new StringBuffer();
		for(String s : condition) {
			if(c.length() > 0)
				c.append(" AND ");
			c.append(s);
		}
		
		return pagedQuery("FROM TXNPHONETOKEN " +
				  		"WHERE " + c.toString() + 
						" order by DPUSERID asc", (start / limit) + 1, limit, values);
	}	

}