package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.util.ESAPIUtil;

import fstop.orm.po.PCALOTTONOTICE;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.hibernate.query.NativeQuery;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.List;


@Slf4j
@Repository
@Transactional
public class PcaLottoNoticeDao extends LegacyJpaRepository<PCALOTTONOTICE, Long> {
	protected Logger logger = Logger.getLogger(getClass());	
	
	@SuppressWarnings( { "unused", "unchecked" })
	public List<PCALOTTONOTICE> checkPcaData(String name,String email,String startDate,String endDate) {
				
		String sql = "select * from pcalottonotice where name = ? and email = ? and lastdate >= ? and lastdate <= ?";
		
		Query query = getSession().createSQLQuery(sql);
		query.unwrap(NativeQuery.class).addEntity("a", PCALOTTONOTICE.class);
		log.debug(ESAPIUtil.vaildLog("startDate:"+startDate+"；endDate:"+endDate+"；name:"+name+"；email:"+email));
		log.debug("checkPcaData:"+sql);
		query.setParameter(0, name);
		query.setParameter(1, email);
		query.setParameter(2, startDate);
		query.setParameter(3, endDate);
		
		return query.getResultList();
	}
	
	//@Transactional(propagation = Propagation.REQUIRES_NEW)
	//public int insertPcaData(String NAME, String TELPHONE,String ADDRESS,String EMAIL,String BRANCHNAME,String ANIMALKIND, 
	//					     String MEMO,String LASTUSER,String LASTDATE,String LASTTIME) {
	//	
	//	SQLQuery add1=this.getSession().createSQLQuery("insert into PCALOTTONOTICE VALUES(Default,?,?,?,?,?,?,?,?,?,?)");
	//	add1.setString(0, NAME);  
	//	add1.setString(1, TELPHONE); 
	//	add1.setString(2, ADDRESS);
	//	add1.setString(3, EMAIL);
	//	add1.setString(4, BRANCHNAME);
	//	add1.setString(5, ANIMALKIND);  
	//	add1.setString(6, MEMO); 
	//	add1.setString(7, LASTUSER);
	//	add1.setString(8, LASTDATE);
	//	add1.setString(9, LASTTIME);		
	//	int r1 = add1.executeUpdate();
	//	return(r1);
	//}
}
