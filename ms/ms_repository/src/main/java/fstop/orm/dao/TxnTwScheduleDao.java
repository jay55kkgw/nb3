package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import fstop.orm.po.TXNTWSCHEDULE;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.hibernate.query.NativeQuery;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.text.DecimalFormat;
import java.util.*;


@Slf4j
@Repository
@Transactional
public class TxnTwScheduleDao extends LegacyJpaRepository<TXNTWSCHEDULE, Long> {
	Logger logger = Logger.getLogger(getClass());
	/**
	 * 找出目前某一USER所設定的預約交易
	 * @param uid
	 * @return
	 */
	@SuppressWarnings( { "unused", "unchecked" })
	public List<TXNTWSCHEDULE> findValidSchedule(String uid) {
		List result = new ArrayList();
		Query query = getQuery("from TXNTWSCHEDULE where  DPUSERID = ?  and DPTXSTATUS = '0' order by DPSCHID", uid);  //0表未執行或未全部執行完

		return query.getResultList();
	}

	public List<TXNTWSCHEDULE> findValidSchedule1(String uid) {
		Date d = new Date();	  
		String today = DateTimeUtils.format("yyyyMMdd", d);		
		List result = new ArrayList();
		Query query = getQuery("from TXNTWSCHEDULE where  DPUSERID = ?  and (DPTXSTATUS = '0' or DPTXSTATUS = '3') and DPTDATE >= ? order by DPTXSTATUS,DPSCHID", uid,today);  //0表未執行或未全部執行完

		return query.getResultList();
	}
	
	public void updateStatus(TXNTWSCHEDULE schedule, String status) {
		logger.debug("update schedule status: " + status);
		try {
			Date d = new Date();
			schedule.setLASTDATE(DateTimeUtils.format("yyyyMMdd", d));
			schedule.setLASTTIME(DateTimeUtils.format("HHmmss", d));
			schedule.setDPTXSTATUS(status);

			save(schedule);
		}
		catch(Exception e) {

		}
		finally {

		}
	}

	/**
	 * 找出下次轉帳日
	 * @param uid
	 * @return
	 */
	public Map<String, String> findNextDate(String uid) {
		DecimalFormat fmt = new DecimalFormat("#0");
		String sql = getSqlForScheduleExcecNextDate(new Date(), uid) + "  order by DPSCHID     ";

		Map<String, String> result = new HashMap();
		Query query = getSession().createQuery(sql);
		query.unwrap(NativeQuery.class)
				.addScalar( "DPSCHID", LongType.INSTANCE)
				.addScalar( "nextexecdate", StringType.INSTANCE);
		List qresult = query.getResultList();
		for(Object o : qresult) {
			Object[] oary = (Object[])o;
			result.put(fmt.format(oary[0]), (String)oary[1]);
		}
		return result;
	}

	public static String getSqlForScheduleExcecNextDate(Date currentDate) {
		return getSqlForScheduleExcecNextDate(currentDate, null);
	}

	public static String getSqlForScheduleExcecNextDate(Date currentDate, String uid) {
		Calendar calendar = Calendar.getInstance();


		Date today = currentDate;

		String tyyyymmdd = DateTimeUtils.format("yyyyMMdd", today);
		String tyy = DateTimeUtils.format("yyyy", today);

		String crlf = "\n";
		String execdateSql = "" +
		"SELECT DPSCHID, NEXTEXECDATE FROM ( " + crlf +
		"        SELECT S.DPSCHID, S.DPTDATE,                                                                                                                                                                   " + crlf +
		"        		CASE WHEN (S.DPFDATE = S.DPTDATE AND R.DPTXSTATUS = '0') OR (S.DPFDATE = S.DPTDATE AND R.DPTXSTATUS = '1' AND R.DPRETXSTATUS !='5')                                                                                                                                                          " + crlf +
		"						THEN S.NEXTMAXDPFDATE       " +
		"                    WHEN S.DPFDATE = S.DPTDATE" +
		"						THEN S.DPTDATE		                                                                                                                                         " + crlf +
		"					 WHEN INT(S.LASTDATE) >= INT(S.DPFDATE)                                                                                                                                              " + crlf +
		"					  AND (S.LASTDATE != '" + tyyyymmdd + "'                                                                                                                                         " + crlf +
		"					   OR (S.LASTDATE = '" + tyyyymmdd + "' AND R.LASTDATE = '"+tyyyymmdd+"' AND R.DPTXSTATUS = '0')                                                                                                              " + crlf +
		"					   OR (S.LASTDATE = '" + tyyyymmdd + "' AND R.LASTDATE = '"+tyyyymmdd+"' AND R.DPTXSTATUS = '1' AND R.DPRETXSTATUS !='5'))                                                                                    " + crlf +
		"					  AND INT(S.DPPERMTDATE) > INT(S.NEXTMAXLASTDD)                                                                                                                                  " + crlf +
		"						THEN S.NEXTMAXLASTDATE                                                                                                                                                            " + crlf +
		"					 WHEN INT(S.LASTDATE) >= INT(S.DPFDATE)                                                                                                                                              " + crlf +
		"					  AND (S.LASTDATE != '" + tyyyymmdd + "'                                                                                                                                         " + crlf +
		"					   OR (S.LASTDATE = '" + tyyyymmdd + "' AND R.LASTDATE = '"+tyyyymmdd+"' AND R.DPTXSTATUS = '0')                                                                                                               " + crlf +
		"					   OR (S.LASTDATE = '" + tyyyymmdd + "' AND R.LASTDATE = '"+tyyyymmdd+"' AND R.DPTXSTATUS = '1' AND R.DPRETXSTATUS !='5'))                                                                                     " + crlf +
		"				      AND INT(S.DPPERMTDATE) <= INT(S.NEXTMAXLASTDD)                                                                                                                                 " + crlf +
		"                 		THEN S.NEXTMAXLASTYY || S.NEXTMAXLASTMM || SUBSTR(DIGITS(INT(S.DPPERMTDATE)), 9)                                                                                                  " + crlf +
		"				 	 WHEN INT(S.LASTDATE) >= INT(S.DPFDATE) AND S.DPPERMTDATE='' AND S.LASTDATE = '" + tyyyymmdd + "'                                                                                                         " + crlf +
		"						THEN S.LASTDATE                                                                                                                                                                   " + crlf +
		"                  	 WHEN INT(S.LASTDATE) < INT(S.DPFDATE)  AND INT(S.MAXFXFYY || S.MAXFXFMM || SUBSTR(DIGITS(INT(S.DPPERMTDATE)), 9)) < INT(S.DPFDATE) AND INT(S.DPPERMTDATE) > INT(S.NEXTMAXFXFDD)     " + crlf +
		"                  		THEN S.NEXTMAXDPFDATE                                                                                                                                                             " + crlf +
		"                  	 WHEN INT(S.LASTDATE) < INT(S.DPFDATE)  AND INT(S.MAXFXFYY || S.MAXFXFMM || SUBSTR(DIGITS(INT(S.DPPERMTDATE)), 9)) < INT(S.DPFDATE) AND INT(S.DPPERMTDATE) <= INT(S.NEXTMAXFXFDD)    " + crlf +
		"                  		THEN S.NEXTMAXFXFYY || S.NEXTMAXFXFMM || SUBSTR(DIGITS(INT(S.DPPERMTDATE)), 9)                                                                                                    " + crlf +
		"                  	 WHEN INT(S.LASTDATE) < INT(S.DPFDATE) AND INT(S.MAXFXFYY || S.MAXFXFMM || SUBSTR(DIGITS(INT(S.DPPERMTDATE)), 9)) >= INT(S.DPFDATE) AND INT(S.DPPERMTDATE) > INT(S.MAXFXFDD)         " + crlf +
		"                  		THEN S.MAXDPFDATE                                                                                                                                                                 " + crlf +
		"                  	 WHEN INT(S.LASTDATE) < INT(S.DPFDATE) AND INT(S.MAXFXFYY || S.MAXFXFMM || SUBSTR(DIGITS(INT(S.DPPERMTDATE)), 9)) >= INT(S.DPFDATE) AND INT(S.DPPERMTDATE) <= INT(S.MAXFXFDD)        " + crlf +
		"                  		THEN S.MAXFXFYY || S.MAXFXFMM || SUBSTR(DIGITS(INT(S.DPPERMTDATE)), 9)                                                                                                            " + crlf +
		"                  	 ELSE '' END AS NEXTEXECDATE                                                                                                                                                         " + crlf +
		"        FROM (SELECT DPSCHID, DPTDATE, DPPERMTDATE, DPUSERID, DPTXSTATUS,                                                                                                                              " + crlf +
		"        			  LASTDATE,                                                                                                                                                                          " + crlf +
		"        			  REPLACE(CHAR(DATE(SUBSTR(LASTDATE,1,4)||'-'||SUBSTR(LASTDATE,5,2)||'-'||'01') + 2 MONTH - 1 DAY),'-','') AS NEXTMAXLASTDATE,                                                       " + crlf +
		"       			  SUBSTR(REPLACE(CHAR(DATE(SUBSTR(LASTDATE,1,4)||'-'||SUBSTR(LASTDATE,5,2)||'-'||'01') + 2 MONTH - 1 DAY),'-',''),1,4) AS NEXTMAXLASTYY,                                           " + crlf +
		"       			  SUBSTR(REPLACE(CHAR(DATE(SUBSTR(LASTDATE,1,4)||'-'||SUBSTR(LASTDATE,5,2)||'-'||'01') + 2 MONTH - 1 DAY),'-',''),5,2) AS NEXTMAXLASTMM,                                           " + crlf +
		"       			  SUBSTR(REPLACE(CHAR(DATE(SUBSTR(LASTDATE,1,4)||'-'||SUBSTR(LASTDATE,5,2)||'-'||'01') + 2 MONTH - 1 DAY),'-',''),7,2) AS NEXTMAXLASTDD,                                           " + crlf +
		"        			  DPFDATE,                                                                                                                                                                           " + crlf +
		"        			  REPLACE(CHAR(DATE(SUBSTR(DPFDATE,1,4)||'-'||SUBSTR(DPFDATE,5,2)||'-'||'01') + 1 MONTH - 1 DAY),'-','') AS MAXDPFDATE,                                                              " + crlf +
		"       			  SUBSTR(REPLACE(CHAR(DATE(SUBSTR(DPFDATE,1,4)||'-'||SUBSTR(DPFDATE,5,2)||'-'||'01') + 1 MONTH - 1 DAY),'-',''),1,4) AS MAXFXFYY,                                                  " + crlf +
		"       			  SUBSTR(REPLACE(CHAR(DATE(SUBSTR(DPFDATE,1,4)||'-'||SUBSTR(DPFDATE,5,2)||'-'||'01') + 1 MONTH - 1 DAY),'-',''),5,2) AS MAXFXFMM,                                                  " + crlf +
		"       			  SUBSTR(REPLACE(CHAR(DATE(SUBSTR(DPFDATE,1,4)||'-'||SUBSTR(DPFDATE,5,2)||'-'||'01') + 1 MONTH - 1 DAY),'-',''),7,2) AS MAXFXFDD,                                                  " + crlf +
		"       			  REPLACE(CHAR(DATE(SUBSTR(DPFDATE,1,4)||'-'||SUBSTR(DPFDATE,5,2)||'-'||'01') + 2 MONTH - 1 DAY),'-','') AS NEXTMAXDPFDATE,                                                        " + crlf +
		"       			  SUBSTR(REPLACE(CHAR(DATE(SUBSTR(DPFDATE,1,4)||'-'||SUBSTR(DPFDATE,5,2)||'-'||'01') + 2 MONTH - 1 DAY),'-',''),1,4) AS NEXTMAXFXFYY,                                              " + crlf +
		"       			  SUBSTR(REPLACE(CHAR(DATE(SUBSTR(DPFDATE,1,4)||'-'||SUBSTR(DPFDATE,5,2)||'-'||'01') + 2 MONTH - 1 DAY),'-',''),5,2) AS NEXTMAXFXFMM,                                              " + crlf +
		"       			  SUBSTR(REPLACE(CHAR(DATE(SUBSTR(DPFDATE,1,4)||'-'||SUBSTR(DPFDATE,5,2)||'-'||'01') + 2 MONTH - 1 DAY),'-',''),7,2) AS NEXTMAXFXFDD,                                              " + crlf +
		"                     LOGINTYPE                                                                                                                                                                        " + crlf +
		"        		FROM TXNTWSCHEDULE) S                                                                                                                                                                     " + crlf +
		"        			LEFT JOIN                                                                                                                                                                               " + crlf +
		"        	  (SELECT T1.DPSCHID, T1.DPTXSTATUS, T1.DPRETXSTATUS , T1.LASTDATE                                                                                                                                     " + crlf +
		"        	   FROM TXNTWRECORD T1,                                                                                                                                                                      " + crlf +
		"     		  	   (SELECT SUBSTR(CHAR(MAX(BIGINT(LASTDATE||LASTTIME))),1,8) AS LASTDATE, SUBSTR(CHAR(MAX(BIGINT(LASTDATE||LASTTIME))),9) AS LASTTIME, DPSCHID FROM TXNTWRECORD WHERE COALESCE(LASTDATE,'') != '' AND COALESCE(LASTTIME,'') != '' GROUP BY DPSCHID ) T2 " + crlf +
		"			   WHERE T1.DPSCHID = T2.DPSCHID AND T1.LASTDATE = T2.LASTDATE AND T1.LASTTIME = T2.LASTTIME AND T1.DPRETXSTATUS <> '3' AND T1.DPRETXSTATUS <> '4' )R                                                       " + crlf +
		"				  	ON S.DPSCHID = R.DPSCHID                                                                                                                                                              " + crlf +
		"        WHERE  " + (StrUtils.isEmpty(uid) ? "" : " S.DPUSERID='" + uid + "' AND ") + "(S.DPTXSTATUS <> '3' AND S.DPTXSTATUS <> '4' AND S.LOGINTYPE <> 'MB' )                                                                      " + crlf +
		"        ) S                                                                                                                                                                                            " + crlf +
		" WHERE LENGTH(NEXTEXECDATE) > 0 AND NEXTEXECDATE <= DPTDATE" ;
		//"AND NEXTEXECDATE >= '" + tyyyymmdd + "'                                                                                                                                " ;

		return execdateSql;
	}


	/**
	 * 找出某一日的轉帳到期, 第一次轉帳
	 * @param currentDate
	 * @return
	 */
	public List<TXNTWSCHEDULE> findScheduleDueTo(Date currentDate) {
		String due = DateTimeUtils.format("yyyyMMdd", currentDate);

		String sql = "" +
		" SELECT distinct {sch.*} FROM      " +
		" 	TXNTWSCHEDULE sch, (" + getSqlForScheduleExcecNextDate(currentDate) + ") nextexec " +
		" 	                                                                     " +
		" 	where	 sch.DPSCHID = nextexec.DPSCHID  and nextexec.nextexecdate = ?  " +
		"           and ( ( DPTXSTATUS = '0' and  LASTDATE <= ? )   " +
		"					 or ( DPTXSTATUS = '1' and  LASTDATE = ?)) and LOGINTYPE <> ? ";

		Query query = getSession().createQuery(sql);
		query.unwrap(NativeQuery.class).addEntity("sch", TXNTWSCHEDULE.class);

		query.setParameter(0, due);
		query.setParameter(1, due);
		query.setParameter(2, due);
		query.setParameter(3, "MB");

		return query.getResultList();
	}
	
	/**
	 * 取得某一日的應執行且尚未執行的預約筆數(不含一扣失敗可重送交易)
	 * @param currentDate
	 * @return
	 */
	public int countScheduleDueTo(Date currentDate) {
		String due = DateTimeUtils.format("yyyyMMdd", currentDate);

		String sql = "" +
		" SELECT distinct {sch.*} FROM      " +
		" 	TXNTWSCHEDULE sch, (" + getSqlForScheduleExcecNextDate(currentDate) + ") nextexec " +
		" 	                                                                     " +
		" 	where	 sch.DPSCHID = nextexec.DPSCHID  and nextexec.nextexecdate = ?  " +
		"           and not exists (select 1 from TXNTWRECORD record where record.DPSCHID = sch.DPSCHID)  " +
		"           and ( ( sch.DPTXSTATUS = '0' and  sch.LASTDATE <= ? )   " +
		"					 or ( sch.DPTXSTATUS = '1' and  sch.LASTDATE = ?))  ";

		Query query = getSession().createQuery(sql);
		query.unwrap(NativeQuery.class).addEntity("sch", TXNTWSCHEDULE.class);

		query.setParameter(0, due);
		query.setParameter(1, due);
		query.setParameter(2, due);

		return query.getResultList().size();
	}


	public List<TXNTWSCHEDULE> findScheduleBySchID (Vector v) {

		String sql = " SELECT sch.* FROM TXNTWSCHEDULE sch " ;

		if(!v.isEmpty()){
			sql += "WHERE DPSCHID IN (";
			for(int i = 0 ; i < v.size(); i++){
				sql += " " + (String)v.get(i) + " ";
				if(i != (v.size() - 1)){
					sql +=", ";
				}
			}
			sql += " )";
		}else {
			sql += "WHERE 1 != 1";
		}

		//預設以使用者預約編號排序
		sql += " ORDER BY DPSCHNO ";

		Query query = getSession().createQuery(sql);
		query.unwrap(NativeQuery.class).addEntity("sch", TXNTWSCHEDULE.class);

		return query.getResultList();
	}

	public List<TXNTWSCHEDULE> findTxStatus(String lastdate, String[] retxstatus, String logintype) {

		String restatusCondition = "";

		if(retxstatus == null || retxstatus.length == 0)
			return new ArrayList();
		else if(retxstatus.length == 1)  {
			restatusCondition = "DPTXSTATUS = '" + retxstatus[0] + "' ";
		}
		else if(retxstatus.length > 1)
			restatusCondition = "DPTXSTATUS in ('" + StrUtils.implode("','", retxstatus) + "') ";

		Query query = getQuery("FROM TXNTWSCHEDULE WHERE " + restatusCondition +
							   " and LASTDATE = ? and LOGINTYPE in ("+logintype+") ORDER BY LASTDATE DESC, LASTTIME DESC ", lastdate);

		return query.getResultList();
	}

	/**
	 * 根據 uid 跟 轉入帳號 查出下一執行日大於今日所有預約轉帳資料
	 * @param uid
	 * @param wdac
	 * @return List<TXNFXSCHEDULE>
	 */
	public List<TXNTWSCHEDULE> findScheduleBySVAC(String uid,String svbh, String svac) {

		String due = DateTimeUtils.format("yyyyMMdd", new Date());

		String sql = "" +
		" SELECT distinct {sch.*} FROM " +
		" TXNTWSCHEDULE sch , (" + getSqlForScheduleExcecNextDate(new Date(), uid) + ") nextexec " +
		" WHERE	sch.DPSCHID = nextexec.DPSCHID " +
		"   and nextexec.nextexecdate is not null " +
		"   and sch.DPSVAC = ? and sch.DPSVBH = ? " +
		"   and nextexec.nextexecdate > ? " ;

		Query query = getSession().createQuery(sql);
		query.unwrap(NativeQuery.class).addEntity("sch", TXNTWSCHEDULE.class);

		query.setParameter(0, svac);
		query.setParameter(1, svbh);
		query.setParameter(2, due);

		return query.getResultList();
	}
	
	/**
	 * 根據 uid 跟 轉入帳號 查出下一執行日等於今日所有預約轉帳資料
	 * @param uid
	 * @param wdac
	 * @return List<TXNFXSCHEDULE>
	 */
	public List<TXNTWSCHEDULE> findScheduleTodayBySVAC(String uid,String svbh, String svac) {

		String due = DateTimeUtils.format("yyyyMMdd", new Date());

		String sql = "" +
		" SELECT distinct {sch.*} FROM " +
		" TXNTWSCHEDULE sch , (" + getSqlForScheduleExcecNextDate(new Date(), uid) + ") nextexec " +
		" WHERE	sch.DPSCHID = nextexec.DPSCHID " +
		"   and nextexec.nextexecdate is not null " +
		"   and sch.DPSVAC = ? and sch.DPSVBH = ? " +
		"   and nextexec.nextexecdate = ? " ;//跟上面的方法只有這一行不一樣，>變成=

		Query query = getSession().createQuery(sql);
		query.unwrap(NativeQuery.class).addEntity("sch", TXNTWSCHEDULE.class);

		query.setParameter(0, svac);
		query.setParameter(1, svbh);
		query.setParameter(2, due);

		return query.getResultList();
	}

	/**
	 * 找出某一日的台幣週期性轉帳到期
	 * @param currentDate
	 * @return
	 */
	public List<TXNTWSCHEDULE> findScheduleDueToEND(Date currentDate) {
		String due = DateTimeUtils.format("yyyyMMdd", currentDate);

		String sql = "" +
		" SELECT distinct {sch.*} FROM    " +
		" 	TXNTWSCHEDULE sch" +
		" 	where sch.DPTDATE = ?  " +
		"   and  sch.DPPERMTDATE<>'' and  sch.DPTXSTATUS not in ('3','4') and  sch.LASTDATE <= ? " ;

		Query query = getSession().createQuery(sql);
		query.unwrap(NativeQuery.class).addEntity("sch", TXNTWSCHEDULE.class);

		query.setParameter(0, due);
		query.setParameter(1, due);

		return query.getResultList();
	}
	
	/**
	 * 找出某一日次日的台幣預約跨行轉帳金額
	 * @param currentDate
	 * @return
	 */
	public List<TXNTWSCHEDULE> findScheduleNextDateAMT(Date currentDate) {
		String due = DateTimeUtils.format("yyyyMMdd", currentDate);
		String shortdue = due.substring(6, 8);
		
		if("0".equals(due.substring(6, 7))){
			shortdue = due.substring(7, 8);
		}
		this.logger.debug("TxnTwScheduleDao.due  ===>" + due);
		this.logger.debug("TxnTwScheduleDao.due  ===>" + due.substring(6, 8));
		this.logger.debug("TxnTwScheduleDao.shortdue  ===>" + shortdue);		
		
		String sql = "" +
				"SELECT * from TXNTWSCHEDULE sch WHERE " +
				"( ( sch.DPPERMTDATE = '' and sch.DPFDATE = ? and sch.DPTDATE = ? ) or " +
				"( sch.DPPERMTDATE = ? and sch.DPFDATE <= ? and sch.DPTDATE >= ? ) ) and " +
				"sch.DPSVBH <> '050'";

		Query query = getSession().createQuery(sql);
		query.unwrap(NativeQuery.class).addEntity("sch", TXNTWSCHEDULE.class);

		query.setParameter(0, due);
		query.setParameter(1, due);
		query.setParameter(2, shortdue);
		query.setParameter(3, due);
		query.setParameter(4, due);
//		this.logger.debug("TxnTwScheduleDao.query.list()  ===>" + query.getResultList());
		
		return query.getResultList();
	}
	
	/**
	 * 根據 台幣預約ID DPSCHID 來更新 交易執行狀態 DPTXSTATUS
	 * @param dpschid
	 * @param dptxstatus
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateStatusByTwSchID(String dpschid, String dptxstatus){

		Query add1=this.getSession().createQuery("UPDATE TXNTWSCHEDULE SET DPTXSTATUS = ? WHERE DPSCHID = ?");

		add1.setParameter(0, dptxstatus);
		add1.setParameter(1, dpschid);

		int r2 = add1.executeUpdate();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateStatusByTwUserID(String dpuserid, String dptxstatus){

		Query add1=this.getSession().createQuery("UPDATE TXNTWSCHEDULE SET DPTXSTATUS = ? WHERE DPUSERID = ? AND DPTXSTATUS = '0'");

		add1.setParameter(0, dptxstatus);
		add1.setParameter(1, dpuserid);

		int r2 = add1.executeUpdate();
	}
	
	

}
