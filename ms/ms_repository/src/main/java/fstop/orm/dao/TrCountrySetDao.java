package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import fstop.orm.po.TRCOUNTRYSET;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Repository
@Transactional
public class TrCountrySetDao extends LegacyJpaRepository<TRCOUNTRYSET, String> {
	protected Logger logger = Logger.getLogger(getClass());
	public List<TRCOUNTRYSET> getAll() {
		return find("FROM TRCOUNTRYSET");
	}	
	public List findByUid(String uid) {
		return find("FROM TRCOUNTRYSET WHERE IDNO = ? ", uid);
	}
	
	public TRCOUNTRYSET findByUidCntrid(String uid, String cntrid) {
		List result = find("FROM TRCOUNTRYSET WHERE IDNO = ? and CNTR_ID = ? ", uid, cntrid);
		if(result.size() > 0) {
			return (TRCOUNTRYSET)result.get(0);
		}
		return null;
	}
	
	
	public void removeByUidCntrid(String uid, String cntrid) {
		getQuery("DELETE FROM TRCOUNTRYSET WHERE IDNO = ? and CNTR_ID = ? ", uid, cntrid)
			.executeUpdate();
	}	
}
