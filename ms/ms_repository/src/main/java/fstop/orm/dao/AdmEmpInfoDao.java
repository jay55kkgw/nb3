package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.util.ESAPIUtil;

import fstop.orm.po.ADMEMPINFO;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;


@Slf4j
@Repository
@Transactional
public class AdmEmpInfoDao extends LegacyJpaRepository<ADMEMPINFO, String> {
	protected Logger logger = Logger.getLogger(getClass());
	
	public ADMEMPINFO findorNewByID(String EMPNO) {
		ADMEMPINFO data = this.findById(EMPNO);
		if(data==null) {
			return new ADMEMPINFO();
		}else {
			return data;
		}
	}
	
	public List<ADMEMPINFO> getAll() {
		return find("FROM ADMEMPINFO ORDER BY EMPNO");
	}
	
	@SuppressWarnings({ "unused", "unchecked" })
	public List<ADMEMPINFO> findByEmpId(String EMPNO) {
		List result = new ArrayList();
		StringBuffer sb = new StringBuffer();
		Query query = getQuery("from ADMEMPINFO e where EMPNO = ?",
				EMPNO);
		//query.setMaxResults(100);
		return query.getResultList();
	}
	
	
	@SuppressWarnings({ "unused", "unchecked" })
	public List<ADMEMPINFO> findLeaveEmp(String CUSID, String LDATE) {
		 
		/*
		SQLQuery add1 = getSession().createSQLQuery("SELECT * FROM ADMEMPINFO WHERE CUSID = ? and LDATE < ?");
		add1.setParameter(0, CUSID);
		add1.setParameter(1, LDATE);
		List result0 = add1.list();
		return result0;
		*/
		
		log.debug(ESAPIUtil.vaildLog("CUSID = " + CUSID + "  LDATE = " + LDATE));
		List result = new ArrayList();
		StringBuffer sb = new StringBuffer();
        //找出小於離職日ldate的資料
		Query query = getQuery("from ADMEMPINFO e where CUSID=? and LDATE<>'' and LDATE > ?", 
				CUSID, LDATE);
		
		//query.setMaxResults(100);
		return query.getResultList();
		
	}
	
	@SuppressWarnings({ "unused", "unchecked" })
	public String getDepNo(String UserId)
	{
		log.debug("getDepNo UserId ================================================>" + UserId);
		List result = new ArrayList();
		StringBuffer sb = new StringBuffer();
		String ResultDepNo = "";
		//SQLQuery add1=null;
		String QueryStr = "from ADMEMPINFO where CUSID=?";
		
		log.debug("QueryStr ================================================>" + QueryStr);
		//try
		//{
		Query query = getQuery("from ADMEMPINFO where CUSID=?", UserId);
		List<ADMEMPINFO> QueryResult = query.getResultList();
			//SQLQuery query = this.getSession().createSQLQuery(QueryStr);
			//query.setParameter(0, UserId);
			//List<ADMEMPINFO> QueryResult = find("from ADMEMPINFO where CUSID='" + UserId + "'");
		//}
		//catch(Exception e)
		//{
			//log.debug("Query Ex ===========================================>" + e.getMessage());
		//}
		
		log.debug("QueryStr After ================================================>");
		//List<ADMEMPINFO> QueryResult = query.
		
		if(QueryResult.size() > 0)
		{
			String ResultDep = QueryResult.get(0).getDEPNO();
			
			// CGI Stored XSS
			log.debug("EMPNO");
//			log.debug("EMPNO() ======================================================> " + ResultDep);
			
			ResultDepNo = ResultDep != null ? ResultDep : "";
		}
		
		return ResultDepNo;
	}
	
	public String getEMPNObyEMPMAIL(String EMPMAIL)
	{
		String ResultEmpNo = "";
		log.debug("getEMPNO by Email ================================================>" + EMPMAIL);
		String QueryStr = "from ADMEMPINFO where CUSID=?";
		log.debug("QueryStr ================================================>" + QueryStr);
		//try
		//{
		Query query = getQuery("from ADMEMPINFO where EMPMAIL=?", EMPMAIL);
		List<ADMEMPINFO> QueryResult = ESAPIUtil.validStrList(query.getResultList());
		log.debug("QueryStr After ================================================>");
		
		if(QueryResult.size() > 0)
		{
			String Result = QueryResult.get(0).getEMPNO();
			
			// CGI Stored XSS
			log.debug("EMPNO");
//			log.debug("EMPNO() ======================================================> " + ResultDep);
			
			ResultEmpNo = Result != null ? Result : "";
		}
		
		return ResultEmpNo;
	}
	
	
	public void insert(ADMEMPINFO admempinfo) {
		int r2=0;
		try{
		
		
		String fillstr=new String();
		Query add1=null;
		
		add1=this.getSession().createSQLQuery("INSERT INTO ADMEMPINFO VALUES(? ,? ,? ,? ,? )");
		add1.setParameter(0, admempinfo.getEMPNO());
		log.debug("EMPNO = " + admempinfo.getEMPNO());
		add1.setParameter(1, admempinfo.getEMPMAIL());
		log.debug("EMPMAIL = " + admempinfo.getEMPMAIL());
		add1.setParameter(2, admempinfo.getCUSID());
		log.debug("CUSID = " + admempinfo.getCUSID());		
		add1.setParameter(3, admempinfo.getLDATE());
		log.debug("LDATE = " + admempinfo.getLDATE());		
		add1.setParameter(4, admempinfo.getDEPNO());
		log.debug("DEPNO = " + admempinfo.getDEPNO());
		r2 = add1.executeUpdate();
		log.debug("r2 = " + r2 + " NO = " + admempinfo.getEMPNO());	
		}
		catch (Exception e ){
			log.debug("ADMEMPINFO EXCEPTION = " + e);	
		}
	}

	public void deleteAll() {
		this.getSession().createSQLQuery("DELETE FROM ADMEMPINFO").executeUpdate();
	}
	
	public boolean isEmpMail(String empIdMail) {
		long count = (Long) (find(
				"SELECT COUNT(*) FROM ADMEMPINFO where EMPMAIL = ? ", empIdMail)).get(0);
		if (count == 0) {
			return false;
		} else {
			return true;
		}
	}
	
	public boolean isEmployee(String cusidn) {
		long count = (Long) (find(
				"SELECT COUNT(*) FROM ADMEMPINFO where CUSID = ? ", cusidn)).get(0);
		if (count == 0) {
			return false;
		} else {
			return true;
		}
	}

	public boolean isEmployeeAndEmpMail(String cusidn, String empIdMail) {
		long count = (Long) (find(
				"SELECT COUNT(*) FROM ADMEMPINFO where LDATE = '' AND CUSID = ? and EMPMAIL = ?", cusidn, empIdMail)).get(0);
		if (count == 0) {
			return false;
		} else {
			return true;
		}
	}	
	
	public boolean isEmpCUSID(String CUSID) {
		long count = (Long) (find(
				"SELECT COUNT(*) FROM ADMEMPINFO where LDATE='' and CUSID = ? ", CUSID)).get(0);
		if (count == 0) {
			return false;
		} else {
			return true;
		}
	}	

}