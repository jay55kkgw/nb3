package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import fstop.orm.po.REVOLVINGCREDITAPPLY;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Repository
@Transactional
public class RevolvingCreditApplyDao extends LegacyJpaRepository<REVOLVINGCREDITAPPLY, String> {

	public List<REVOLVINGCREDITAPPLY> findByDate(String date) {
		return find("FROM REVOLVINGCREDITAPPLY WHERE APPDATE = ? ORDER BY RCVNO", date); 
	}
	public boolean checkapply(String uid, String appdate) {
		List<REVOLVINGCREDITAPPLY> result = find("FROM REVOLVINGCREDITAPPLY WHERE CUSIDN = ? AND APPDATE = ?", uid,appdate);
		if (result.size()>0)
			return true;
		else
			return false;
	}	
	
	public long getTxnCount() {
		List<String> params = new ArrayList();
		String sdate = DateTimeUtils.getPrevMonthFirstDay();

		String stime = "000000";
		String edate = DateTimeUtils.getPrevMonthLastDay();

		String etime = "235959";
		params.add(sdate);
		params.add(stime);
		params.add(edate);
		params.add(etime);

		String[] values = new String[params.size()];
		params.toArray(values);

		String SQL = "SELECT COUNT(*) FROM REVOLVINGCREDITAPPLY WHERE APPDATE >= ? AND APPTIME >= ? AND APPDATE <= ? AND APPTIME <= ?";

		return ((Long) find(SQL, values).get(0)).longValue();
	}
}
