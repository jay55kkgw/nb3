package fstop.orm.dao;

import java.util.List;
import java.util.UUID;

import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.hibernate.query.NativeQuery;
import org.springframework.stereotype.Repository;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.util.ESAPIUtil;

import fstop.orm.po.TXNSMSLOG;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@Repository
public class TxnSmsLogDao extends LegacyJpaRepository<TXNSMSLOG, String> {
	protected Logger logger = Logger.getLogger(getClass());
	

	public List<TXNSMSLOG> findby(String _ID, String _Date) {
	String sql = "select * FROM TXNSMSLOG WHERE ADUSERID=? and LASTDATE=? and STATUSCODE in('1','2','3')";			
	Query query = getSession().createSQLQuery(sql);
	query.unwrap(NativeQuery.class).addEntity("a", TXNSMSLOG.class);

	query.setParameter(0, _ID);
	query.setParameter(1, _Date);	
	logger.warn("TxnSmsLogDao ADUSERID:"+_ID);
	
	return query.getResultList();
	
	
	}
	public int findNA72TotalCount(String _ID, String _Date) {
		String sql = "SELECT COUNT(*) FROM TXNSMSLOG WHERE ADUSERID=? and LASTDATE=? and STATUSCODE in('1','2','3') and ADOPID='NA72'";
		Query query = getSession().createSQLQuery(sql);
		query.setParameter(0, _ID);
		query.setParameter(1, _Date);	
		logger.warn(ESAPIUtil.vaildLog("TxnSmsLogDao findTotalCount ADUSERID:"+_ID +" ADOPID:NA72 LASTDATE:"+_Date));		
		List result = query.getResultList();
		logger.warn("TxnSmsLogDao findTotalCount:"+(Integer)result.get(0) );
		return (Integer)result.get(0);
	}	
	public int findNA721TotalCount(String _ID, String _Date) {
		String sql = "SELECT COUNT(*) FROM TXNSMSLOG WHERE ADUSERID=? and LASTDATE=? and STATUSCODE in('1','2','3') and ADOPID='NA721'";
		Query query = getSession().createSQLQuery(sql);
		query.setParameter(0, _ID);
		query.setParameter(1, _Date);	
		logger.warn(ESAPIUtil.vaildLog("TxnSmsLogDao findTotalCount ADUSERID:"+_ID +" ADOPID:NA721 LASTDATE:"+_Date));		
		List result = query.getResultList();
		logger.warn("TxnSmsLogDao findTotalCount:"+(Integer)result.get(0) );
		return (Integer)result.get(0);
	}
	
	public int findTotalCountByInput(String _ID , String _Date ,String adopid) {
		String sql = "SELECT COUNT(*) FROM TXNSMSLOG WHERE ADUSERID=? and LASTDATE=? and STATUSCODE in('1','2','3') and ADOPID=?";
		Query query = getSession().createSQLQuery(sql);
		query.setParameter(0, _ID);
		query.setParameter(1, _Date);	
		query.setParameter(2, adopid);	
		logger.warn(ESAPIUtil.vaildLog("TxnSmsLogDao findTotalCount ADUSERID:"+_ID +" ADOPID:"+adopid+" LASTDATE:"+_Date));		
		List result = query.getResultList();
		logger.warn("TxnSmsLogDao findTotalCount:"+(Integer)result.get(0) );
		return (Integer)result.get(0);
	}	
	
	public void insert(TXNSMSLOG _tt) {
		int r2=0;
		Query add1=null;
		
		//String chk_int = _tt.getCOUNT();
		//if (Integer.parseInt(chk_int)>1){  //update 
		//	add1=this.getSession().createSQLQuery("UPDATE USER_SMS SET COUNT=? WHERE SMS_ID=? AND SMS_DATE=?");
		//	add1.setString(0, UUID.randomUUID().toString());


		//	add1.setString(0, _sms.getCOUNT());
			//add1.setString(1, _sms.getSMS_ID());
			//add1.setString(2, _sms.getSMS_DATE());
		//}else{  //insert
		add1=this.getSession().createSQLQuery("INSERT INTO TXNSMSLOG VALUES( ? ,? ,? ,? ,? ,? ,? ,? )");
		add1.setParameter(0, UUID.randomUUID().toString());
		add1.setParameter(1, _tt.getADUSERID());
		add1.setParameter(2, _tt.getPHONE());
		add1.setParameter(3, _tt.getMSGID());
		add1.setParameter(4, _tt.getSTATUSCODE());
		add1.setParameter(5, _tt.getLASTDATE());
		add1.setParameter(6, _tt.getLASTTIME());
		add1.setParameter(7, _tt.getADOPID());

		
		    //add1.setString(1, _sms.getSMS_DATE());
		    //add1.setString(2, _sms.getCOUNT());
		//}				
		r2 = add1.executeUpdate();
	}
	
	
}
