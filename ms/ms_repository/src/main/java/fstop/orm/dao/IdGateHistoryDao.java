package fstop.orm.dao;

import java.util.List;
import java.util.function.BiFunction;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;
import com.netbank.domain.orm.core.SessionAdapter;

import fstop.orm.po.IDGATEHISTORY;
import lombok.extern.slf4j.Slf4j;

@Transactional(
		transactionManager="nb3transactionManager",
        rollbackFor = {Throwable.class}
)
@Repository
@Slf4j
public class IdGateHistoryDao extends LegacyJpaRepository<IDGATEHISTORY,String>{

}
