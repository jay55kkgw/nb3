package fstop.orm.dao;

import java.util.List;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.TXNCARDLOG;

@Slf4j
@Repository
@Transactional
public class TxnCardLogDao extends LegacyJpaRepository<TXNCARDLOG, Long> {

	public List<TXNCARDLOG> getAll() {
		return find("FROM TXNCARDLOG ORDER BY LOGID");
	}

	public void deleteAll() {
		this.getSession().createSQLQuery("DELETE FROM TXNCARDLOG").executeUpdate();
	}
}
