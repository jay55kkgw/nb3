package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import fstop.orm.po.SCRECORD;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.List;

@Slf4j
@Repository
@Transactional
public class ScRecordDao extends LegacyJpaRepository<SCRECORD, String> {
	protected Logger logger = Logger.getLogger(getClass());

	@SuppressWarnings( { "unused", "unchecked" })
	public List<SCRECORD> getUserAgreeSCTNVersion(String userid,String sctn) {

		String sql = "from SCRECORD where USERID = ?  and SCTN = ? and Agree='Y'";		
		Query query = getQuery(sql,userid,sctn);
		return query.getResultList();
	}
	@SuppressWarnings( { "unused", "unchecked" })
	public List<SCRECORD> getUserSCTN(String userid,String sctn) {

		String sql = "from SCRECORD where USERID = ?  and SCTN = ?";		
		Query query = getQuery(sql,userid,sctn);				
		return query.getResultList();
	}	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public int updateSCVersion(String USERID,String SCTN, String SCVERSION,String Agree,String AgreeDate) {
		Query add1=this.getSession().createSQLQuery("UPDATE SCRECORD SET SCVERSION = ? ,Agree = ?,AgreeDate = ? WHERE USERID = ? and SCTN = ?");
		add1.setParameter(0, SCVERSION);
		add1.setParameter(1, Agree);
		add1.setParameter(2, AgreeDate);
		add1.setParameter(3, USERID);
		add1.setParameter(4, SCTN);
		int r2 = add1.executeUpdate();
		return(r2);
	}
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public int insertSCVersion(String USERID,String SCTN, String SCVERSION,String Agree,String AgreeDate) {
		Query add1=this.getSession().createSQLQuery("insert into SCRECORD VALUES(?,?,?,?,?)");
		add1.setParameter(0, USERID);
		add1.setParameter(1, SCTN);
		add1.setParameter(2, SCVERSION);
		add1.setParameter(3, Agree);
		add1.setParameter(4, AgreeDate);
		int r2 = add1.executeUpdate();
		return(r2);
	}	
}