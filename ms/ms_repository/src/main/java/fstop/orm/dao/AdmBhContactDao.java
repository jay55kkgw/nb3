package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import fstop.orm.po.ADMBHCONTACT;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Repository
@Transactional
public class AdmBhContactDao extends LegacyJpaRepository<ADMBHCONTACT, Integer> {
	protected Logger logger = Logger.getLogger(getClass());
	
	public List findByBhID(String bhid) {
		return find("FROM ADMBHCONTACT WHERE ADBRANCHID = ? ", bhid);
	}
}
