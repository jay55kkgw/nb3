package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.util.ESAPIUtil;

import fstop.orm.po.ADMKEYVALUE;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Slf4j
@Repository
@Transactional
public class AdmKeyValueDao extends LegacyJpaRepository<ADMKEYVALUE, String> {
	
	
	public String translator(String category, String key) {
		String result = key;
		try {
			ADMKEYVALUE o = (ADMKEYVALUE)find("FROM ADMKEYVALUE WHERE CATEGORY=? and KEY=? ", category, key).iterator().next();
			result = o.getVALUE();
		}catch (NullPointerException e) {
			log.error(ESAPIUtil.vaildLog("NullPointerException category: " + category + ", key: " + key));
		}
		catch(Exception e) {
			log.error(ESAPIUtil.vaildLog("category: " + category + ", key: " + key));
			log.error("",e);
		}
		return result;
	}
}