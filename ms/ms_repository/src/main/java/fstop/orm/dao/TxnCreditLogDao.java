package fstop.orm.dao;

import java.util.Calendar;
import java.util.List;
import java.util.Date;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.TXNCREDITLOG;
import fstop.util.DateTimeUtils;

@Slf4j
@Repository
@Transactional
public class TxnCreditLogDao extends LegacyJpaRepository<TXNCREDITLOG, String> {

	public List<TXNCREDITLOG> findByDate(String date) {
		return find("FROM TXNCREDITLOG WHERE TXNDATE = ? ORDER BY TXNTIME", date);
	}
	
	/**
	 * 查詢特定時間範圍([date]-1日 ~ [date])
	 * @param date
	 * @return
	 */
	public List<TXNCREDITLOG> findBySpecificTimeRange(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DAY_OF_MONTH, -1);
		Date yesterday = cal.getTime();
		return find("FROM TXNCREDITLOG WHERE (TXNDATE = ? AND TXNTIME >= ?) OR (TXNDATE = ? AND TXNTIME < ?) ORDER BY LOGID", 
						DateTimeUtils.getDateShort(yesterday), DateTimeUtils.getTimeShort(yesterday), 
						DateTimeUtils.getDateShort(date), DateTimeUtils.getTimeShort(date));
	}

	public List<TXNCREDITLOG> findSucceedLog(String cardNum) {
		return find("FROM TXNCREDITLOG WHERE CARDNUM = ? and STATUS = 'Y'", cardNum);
	}
	/**
	 * 查詢特定日期範圍date1~date2
	 * @param date
	 * @return
	 */
	public List<TXNCREDITLOG> findByDateRange(String date1,String date2) {
		return find("FROM TXNCREDITLOG WHERE TXNDATE >= ? and TXNDATE <= ? ORDER BY LOGID",date1, date2);
	}
	
}
