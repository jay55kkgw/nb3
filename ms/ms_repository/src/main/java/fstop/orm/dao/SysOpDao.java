package fstop.orm.dao;

import java.util.ArrayList;
import java.util.List;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.SYSOP;
import fstop.orm.po.TXNFUNDDATA;
import fstop.orm.po.TXNUSER;

import javax.persistence.Query;

@Slf4j
@Repository
@Transactional
public class SysOpDao extends LegacyJpaRepository<SYSOP, String> {
	protected Logger logger = Logger.getLogger(getClass());

	@SuppressWarnings( { "unused", "unchecked" })
	public List<SYSOP> findByADOPId(String ADOPID) {
		
		String sql = "from SYSOP where ADOPID = ? ";
		
		Query query = getQuery(sql, ADOPID);
		
		return query.getResultList();
	}

	@SuppressWarnings( { "unused", "unchecked" })
	public List<SYSOP> getOfflineList() {
		
		//String ResultStr = "";
		String sql = "from SYSOP where ADOPALIVE = 'N' ";
		
		Query query = getQuery(sql);
		
		List<SYSOP> ResultList = query.getResultList();
		
		/*
		if(ResultList.size() > 0)
		{
			for(int i = 0; i < ResultList.size(); i++)
			{
				if(ResultStr.length() > 0)ResultStr += ",";
				ResultStr += ResultList.get(i).getADOPID();
			}
		}
		*/
		
		return ResultList;
	}

	@SuppressWarnings( { "unused", "unchecked" })
	public String updateStatus(String Items, String StatusCode) {
		String UpdateResult = "";
		
		try
		{
			Query updateQueryStr=this.getSession().createSQLQuery("UPDATE SYSOP SET ADOPALIVE = '" + StatusCode + "' WHERE ADOPID IN (" + Items + ")");
			log.debug("QueryStr =================================> " + updateQueryStr.toString());
			//updateQueryStr.setString(0, StatusCode);
			//updateQueryStr.setString(1, Items);
			
			int updateRecords = updateQueryStr.executeUpdate();
			
			UpdateResult = "Success," + StatusCode + "," + updateRecords;
		}
		catch(Exception UpdateSysOpEx)
		{
			UpdateResult = UpdateSysOpEx.getMessage();
		}
		
		return UpdateResult;
	}
} 
