package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import fstop.orm.po.ADMMYREMITMENU;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Slf4j
@Repository
@Transactional
public class AdmMyRemitMenuDao extends LegacyJpaRepository<ADMMYREMITMENU, Long> {
	protected Logger logger = Logger.getLogger(getClass());

	/**
	 * 找到某人的的 RemitMenu
	 * @param uid
	 * @return
	 */
	public List findByUid(String uid) {
		return find("FROM ADMMYREMITMENU WHERE ADUID = ? ", uid);
	}
	
	public ADMMYREMITMENU findByUidRmtid(String uid, String rmtid, String rmttype) {
		List result = find("FROM ADMMYREMITMENU WHERE ADUID = ? and ADRMTID = ? and ADRMTTYPE = ? ", uid, rmtid, rmttype);
		if(result.size() > 0) {
			return (ADMMYREMITMENU)result.get(0);
		}
		return null;
	}
	
	
	public void removeByUidRmtid(String uid, String rmtid, String rmttype) {
		getQuery("DELETE FROM ADMMYREMITMENU WHERE ADUID = ? and ADRMTID = ? and ADRMTTYPE = ? ", uid, rmtid, rmttype)
			.executeUpdate();
	}
	
	
	
	
}
