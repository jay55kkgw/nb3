package fstop.orm.dao;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.util.ESAPIUtil;

import fstop.orm.po.SYSDAILYSEQ;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Transactional
public class SysDailySeqDao extends LegacyJpaRepository<SYSDAILYSEQ, Serializable>
{

	private Object mutex = new Object();

	/**
	 * 
	 * 取外幣交易序號<BR>
	 * 目前外幣交易取序號規則如下，序號不能重複，外幣主機會擋 N＋序號（7位） 例如：N0000001 N：NNB <BR>
	 * C：企網 <BR>
	 * H：XXX <BR>
	 * A：NB3上線後交易序號第一碼 <BR>
	 */
	public String getFXStan(String TXID)
	{
		log.debug("getFXStan TXID>>>{}", TXID);
		
		String fXStan = "A" + StrUtils.right("0000000" + dailySeq("F001T"), 7) + "";
		
		return fXStan;
	}

	/**
	 * 取得 送中心主機所需的序號
	 * 
	 * @param appid
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Integer dailySeq(String appid)
	{
		synchronized (mutex)
		{

			Query query = getSession().createSQLQuery("UPDATE SYSDAILYSEQ SET ADSEQ=ADSEQ WHERE ADSEQID = ? ");
			query.setParameter(0, appid);
			log.debug("dailySeq.executeUpdate ...");
			int r = query.executeUpdate();
			log.debug("r >>{}", r);
			if (r == 0)
			{
				SYSDAILYSEQ seq = new SYSDAILYSEQ();
				seq.setADSEQID(appid);
				seq.setADSEQMEMO("");
				seq.setADSEQ(1);
				save(seq);
				return 1;
			}
			Query add1 = getSession().createSQLQuery("UPDATE SYSDAILYSEQ SET ADSEQ=ADSEQ+1 WHERE ADSEQID = ? ");
			add1.setParameter(0, appid);
			int r2 = add1.executeUpdate();

			Query query2 = getSession().createSQLQuery("SELECT ADSEQ FROM SYSDAILYSEQ WHERE ADSEQID = ? ");
			query2.setParameter(0, appid);
			List obj = query2.getResultList();

			return ((Integer) obj.get(0));
		}
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public BigInteger getSeq() {
		String sql = "VALUES NEXTVAL FOR SEQ_DPSCHTXNO";
		Query query = getSession().createSQLQuery(sql);
		List obj = query.getResultList();
		log.debug(ESAPIUtil.vaildLog("seqence >>"+obj.get(0)));
		return ((BigInteger)obj.get(0));
	}

}
