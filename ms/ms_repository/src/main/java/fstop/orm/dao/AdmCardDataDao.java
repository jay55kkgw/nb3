package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.util.ESAPIUtil;

import fstop.model.Row;
import fstop.model.Rows;
import fstop.orm.po.ADMCARDDATA;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Slf4j
@Repository
@Transactional
public class AdmCardDataDao extends LegacyJpaRepository<ADMCARDDATA, String> {
	protected Logger logger = Logger.getLogger(getClass());
	
	public List<ADMCARDDATA> getNormalCard() {
		return find("FROM ADMCARDDATA WHERE CARDATTR='C' ORDER BY CARDNO");
	}
	/**
	 * 查詢CARDNAME By CARDNO
	 * @return
	 */
	public String findCARDNAME(String cardno) {
		
		Query query=getSession().createSQLQuery("SELECT CARDNAME FROM ADMCARDDATA WHERE CARDNO = ? ");
		query.setParameter(0, cardno);
		
		List qresult = ESAPIUtil.validStrList(query.getResultList());
		return qresult.get(0).toString();
	}	
	/**
	 * 查詢CARDMEMO By CARDNO
	 * @return
	 */
	public String findCARDMEMO(String cardno) {
		
		Query query=getSession().createSQLQuery("SELECT CARDMEMO FROM ADMCARDDATA WHERE CARDNO = ? ");
		query.setParameter(0, cardno);
		List qresult = ESAPIUtil.validStrList(query.getResultList());
		return qresult.get(0).toString();
	}
	public List<ADMCARDDATA> getCardDATA(String cardno) {
		return find("FROM ADMCARDDATA WHERE CARDNO = ? ORDER BY CARDNO",cardno);
	}
	/*
	 * 取得申請過的卡片之外的其他卡片
	 * 20180919
	 * */
	public Rows getOtherCardGroupDATA(String cardtype) {
		logger.info("AdmCardDataDao.java cardtype==>"+cardtype);
		Rows result = new Rows();
		String query="";
		List<ADMCARDDATA> all = null;
		List list = new ArrayList();
		List list1 = new ArrayList();
		if(cardtype.equals("200") || cardtype.equals("300") || cardtype.equals("800")) {
			logger.info("AdmCardDataDao.java cardtype is 200 300 800");
			query = "FROM ADMCARDDATA WHERE CARDATTR='C' AND CARDTYPE NOT IN('200','300','800') ORDER BY CARDNO";						
//			logger.info("CreditUtil query==>"+query);					
			all=  find(query);
			logger.info("CreditUtil all size==>"+all.size());
			for(int i=0;i<all.size();i++) {				
				logger.info("org CreditUtil all CARDNAME["+i+"]==>"+all.get(i).getCARDNAME());
			}			
			list.add(0, all.get(23).getCARDNAME());			
			list.add(1, all.get(24).getCARDNAME());
			list.add(2, all.get(11).getCARDNAME());
			list.add(3, all.get(14).getCARDNAME());
			list.add(4, all.get(6).getCARDNAME());
			list.add(5, all.get(18).getCARDNAME());
			list.add(6, all.get(17).getCARDNAME());
			list.add(7, all.get(21).getCARDNAME());
			list.add(8, all.get(22).getCARDNAME());
			list.add(9, all.get(19).getCARDNAME());
			list.add(10, all.get(20).getCARDNAME());
			list.add(11, all.get(16).getCARDNAME());
			list.add(12, all.get(8).getCARDNAME());
			list.add(13, all.get(9).getCARDNAME());
			list.add(14, all.get(2).getCARDNAME());
			list.add(15, all.get(13).getCARDNAME());
			list.add(16, all.get(3).getCARDNAME());
			list.add(17, all.get(1).getCARDNAME());
			list.add(18, all.get(0).getCARDNAME());
			
			list1.add(0, all.get(23).getCARDNO());			
			list1.add(1, all.get(24).getCARDNO());
			list1.add(2, all.get(11).getCARDNO());
			list1.add(3, all.get(14).getCARDNO());
			list1.add(4, all.get(6).getCARDNO());
			list1.add(5, all.get(18).getCARDNO());
			list1.add(6, all.get(17).getCARDNO());
			list1.add(7, all.get(21).getCARDNO());
			list1.add(8, all.get(22).getCARDNO());
			list1.add(9, all.get(19).getCARDNO());
			list1.add(10, all.get(20).getCARDNO());
			list1.add(11, all.get(16).getCARDNO());
			list1.add(12, all.get(8).getCARDNO());
			list1.add(13, all.get(9).getCARDNO());
			list1.add(14, all.get(2).getCARDNO());
			list1.add(15, all.get(13).getCARDNO());
			list1.add(16, all.get(3).getCARDNO());
			list1.add(17, all.get(1).getCARDNO());
			list1.add(18, all.get(0).getCARDNO());
			
			for(int j=0;j<list.size();j++) {
				logger.info("after CreditUtil list CARDNAME["+j+"]==>"+list.get(j));
				Map rm = new HashMap();
				rm.put("CARDNO", list1.get(j));							
				rm.put("CARDNAME", list.get(j));			
				Row r = new Row(rm);
				result.addRow(r);
			}																			
		}
		if(cardtype.equals("906") || cardtype.equals("801") || cardtype.equals("304") || cardtype.equals("204")) {
			logger.info("AdmCardDataDao.java cardtype is 906 801 304 204");
			query = "FROM ADMCARDDATA WHERE CARDATTR='C' AND CARDTYPE NOT IN('906','801','304','204') ORDER BY CARDNO";
			all=  find(query);
			logger.info("CreditUtil all size==>"+all.size());
			for(int i=0;i<all.size();i++) {				
				logger.info("org CreditUtil all CARDNAME["+i+"]==>"+all.get(i).getCARDNAME());
			}
			list.add(0, all.get(22).getCARDNAME());
			list.add(1, all.get(23).getCARDNAME());
			list.add(2, all.get(13).getCARDNAME());
			list.add(3, all.get(20).getCARDNAME());
			list.add(4, all.get(21).getCARDNAME());
			list.add(5, all.get(18).getCARDNAME());
			list.add(6, all.get(19).getCARDNAME());
			list.add(7, all.get(17).getCARDNAME());
			list.add(8, all.get(10).getCARDNAME());
			list.add(9, all.get(11).getCARDNAME());
			list.add(10, all.get(4).getCARDNAME());
			list.add(11, all.get(15).getCARDNAME());
			list.add(12, all.get(5).getCARDNAME());
			list.add(13, all.get(8).getCARDNAME());
			list.add(14, all.get(1).getCARDNAME());
			list.add(15, all.get(0).getCARDNAME());
			list.add(16, all.get(3).getCARDNAME());
			list.add(17, all.get(2).getCARDNAME());
			
			list1.add(0, all.get(22).getCARDNO());
			list1.add(1, all.get(23).getCARDNO());
			list1.add(2, all.get(13).getCARDNO());
			list1.add(3, all.get(20).getCARDNO());
			list1.add(4, all.get(21).getCARDNO());
			list1.add(5, all.get(18).getCARDNO());
			list1.add(6, all.get(19).getCARDNO());
			list1.add(7, all.get(17).getCARDNO());
			list1.add(8, all.get(10).getCARDNO());
			list1.add(9, all.get(11).getCARDNO());
			list1.add(10, all.get(4).getCARDNO());
			list1.add(11, all.get(15).getCARDNO());
			list1.add(12, all.get(5).getCARDNO());
			list1.add(13, all.get(8).getCARDNO());
			list1.add(14, all.get(1).getCARDNO());
			list1.add(15, all.get(0).getCARDNO());
			list1.add(16, all.get(3).getCARDNO());
			list1.add(17, all.get(2).getCARDNO());
			
			for(int j=0;j<list.size();j++) {				
				Map rm = new HashMap();
				rm.put("CARDNO", list1.get(j));							
				rm.put("CARDNAME", list.get(j));			
				Row r = new Row(rm);
				result.addRow(r);
			}
		}
		if(cardtype.equals("903") || cardtype.equals("407")) {
			logger.info("AdmCardDataDao.java cardtype is 903 407");
			query = "FROM ADMCARDDATA WHERE CARDATTR='C' AND CARDTYPE NOT IN('903','407') ORDER BY CARDNO";
			all=  find(query);
			logger.info("CreditUtil all size==>"+all.size());
			for(int i=0;i<all.size();i++) {				
				logger.info("org CreditUtil all CARDNAME["+i+"]==>"+all.get(i).getCARDNAME());
			}
			list.add(0, all.get(24).getCARDNAME());
			list.add(1, all.get(25).getCARDNAME());
			list.add(2, all.get(12).getCARDNAME());
			list.add(3, all.get(15).getCARDNAME());
			list.add(4, all.get(8).getCARDNAME());
			list.add(5, all.get(19).getCARDNAME());
			list.add(6, all.get(18).getCARDNAME());
			list.add(7, all.get(22).getCARDNAME());
			list.add(8, all.get(23).getCARDNAME());
			list.add(9, all.get(20).getCARDNAME());
			list.add(10, all.get(21).getCARDNAME());
			list.add(11, all.get(17).getCARDNAME());
			list.add(12, all.get(10).getCARDNAME());
			list.add(13, all.get(14).getCARDNAME());
			list.add(14, all.get(4).getCARDNAME());
			list.add(15, all.get(7).getCARDNAME());
			list.add(16, all.get(1).getCARDNAME());
			list.add(17, all.get(0).getCARDNAME());
			list.add(18, all.get(3).getCARDNAME());
			list.add(19, all.get(2).getCARDNAME());
			
			list1.add(0, all.get(24).getCARDNO());
			list1.add(1, all.get(25).getCARDNO());
			list1.add(2, all.get(12).getCARDNO());
			list1.add(3, all.get(15).getCARDNO());
			list1.add(4, all.get(8).getCARDNO());
			list1.add(5, all.get(19).getCARDNO());
			list1.add(6, all.get(18).getCARDNO());
			list1.add(7, all.get(22).getCARDNO());
			list1.add(8, all.get(23).getCARDNO());
			list1.add(9, all.get(20).getCARDNO());
			list1.add(10, all.get(21).getCARDNO());
			list1.add(11, all.get(17).getCARDNO());
			list1.add(12, all.get(10).getCARDNO());
			list1.add(13, all.get(14).getCARDNO());
			list1.add(14, all.get(4).getCARDNO());
			list1.add(15, all.get(7).getCARDNO());
			list1.add(16, all.get(1).getCARDNO());
			list1.add(17, all.get(0).getCARDNO());
			list1.add(18, all.get(3).getCARDNO());
			list1.add(19, all.get(2).getCARDNO());
			
			for(int j=0;j<list.size();j++) {				
				Map rm = new HashMap();
				rm.put("CARDNO", list1.get(j));							
				rm.put("CARDNAME", list.get(j));			
				Row r = new Row(rm);
				result.addRow(r);
			}
		}
		if(cardtype.equals("905") || cardtype.equals("409")) {
			logger.info("AdmCardDataDao.java cardtype is 905 409");
			query = "FROM ADMCARDDATA WHERE CARDATTR='C' AND CARDTYPE NOT IN('905','409') ORDER BY CARDNO";
			all=  find(query);
			logger.info("CreditUtil all size==>"+all.size());
			for(int i=0;i<all.size();i++) {				
				logger.info("org CreditUtil all CARDNAME["+i+"]==>"+all.get(i).getCARDNAME());
			}
			list.add(0, all.get(24).getCARDNAME());
			list.add(1, all.get(25).getCARDNAME());
			list.add(2, all.get(13).getCARDNAME());
			list.add(3, all.get(15).getCARDNAME());
			list.add(4, all.get(8).getCARDNAME());
			list.add(5, all.get(19).getCARDNAME());
			list.add(6, all.get(18).getCARDNAME());
			list.add(7, all.get(22).getCARDNAME());
			list.add(8, all.get(23).getCARDNAME());
			list.add(9, all.get(20).getCARDNAME());
			list.add(10, all.get(21).getCARDNAME());
			list.add(11, all.get(17).getCARDNAME());
			list.add(12, all.get(10).getCARDNAME());
			list.add(13, all.get(11).getCARDNAME());
			list.add(14, all.get(4).getCARDNAME());
			list.add(15, all.get(7).getCARDNAME());
			list.add(16, all.get(1).getCARDNAME());
			list.add(17, all.get(0).getCARDNAME());
			list.add(18, all.get(3).getCARDNAME());
			list.add(19, all.get(2).getCARDNAME());
			
			list1.add(0, all.get(24).getCARDNO());
			list1.add(1, all.get(25).getCARDNO());
			list1.add(2, all.get(13).getCARDNO());
			list1.add(3, all.get(15).getCARDNO());
			list1.add(4, all.get(8).getCARDNO());
			list1.add(5, all.get(19).getCARDNO());
			list1.add(6, all.get(18).getCARDNO());
			list1.add(7, all.get(22).getCARDNO());
			list1.add(8, all.get(23).getCARDNO());
			list1.add(9, all.get(20).getCARDNO());
			list1.add(10, all.get(21).getCARDNO());
			list1.add(11, all.get(17).getCARDNO());
			list1.add(12, all.get(10).getCARDNO());
			list1.add(13, all.get(11).getCARDNO());
			list1.add(14, all.get(4).getCARDNO());
			list1.add(15, all.get(7).getCARDNO());
			list1.add(16, all.get(1).getCARDNO());
			list1.add(17, all.get(0).getCARDNO());
			list1.add(18, all.get(3).getCARDNO());
			list1.add(19, all.get(2).getCARDNO());                                                     
			
			for(int j=0;j<list.size();j++) {				
				Map rm = new HashMap();
				rm.put("CARDNO", list1.get(j));							
				rm.put("CARDNAME", list.get(j));			
				Row r = new Row(rm);
				result.addRow(r);
			}
		}
		if(cardtype.equals("911") || cardtype.equals("411")) {
			logger.info("AdmCardDataDao.java cardtype is 911 411");
			query = "FROM ADMCARDDATA WHERE CARDATTR='C' AND CARDTYPE NOT IN('911','411') ORDER BY CARDNO";
			all=  find(query);
			logger.info("CreditUtil all size==>"+all.size());
			for(int i=0;i<all.size();i++) {				
				logger.info("org CreditUtil all CARDNAME["+i+"]==>"+all.get(i).getCARDNAME());
			}
			list.add(0, all.get(24).getCARDNAME());
			list.add(1, all.get(25).getCARDNAME());
			list.add(2, all.get(13).getCARDNAME());
			list.add(3, all.get(16).getCARDNAME());
			list.add(4, all.get(9).getCARDNAME());
			list.add(5, all.get(19).getCARDNAME());
			list.add(6, all.get(18).getCARDNAME());
			list.add(7, all.get(22).getCARDNAME());
			list.add(8, all.get(23).getCARDNAME());
			list.add(9, all.get(20).getCARDNAME());
			list.add(10, all.get(21).getCARDNAME());
			list.add(11, all.get(11).getCARDNAME());
			list.add(12, all.get(4).getCARDNAME());
			list.add(13, all.get(15).getCARDNAME());
			list.add(14, all.get(5).getCARDNAME());
			list.add(15, all.get(8).getCARDNAME());
			list.add(16, all.get(1).getCARDNAME());
			list.add(17, all.get(0).getCARDNAME());
			list.add(18, all.get(3).getCARDNAME());
			list.add(19, all.get(2).getCARDNAME());
			
			list1.add(0, all.get(24).getCARDNO());
			list1.add(1, all.get(25).getCARDNO());
			list1.add(2, all.get(13).getCARDNO());
			list1.add(3, all.get(16).getCARDNO());
			list1.add(4, all.get(9).getCARDNO());
			list1.add(5, all.get(19).getCARDNO());
			list1.add(6, all.get(18).getCARDNO());
			list1.add(7, all.get(22).getCARDNO());
			list1.add(8, all.get(23).getCARDNO());
			list1.add(9, all.get(20).getCARDNO());
			list1.add(10, all.get(21).getCARDNO());
			list1.add(11, all.get(11).getCARDNO());
			list1.add(12, all.get(4).getCARDNO());
			list1.add(13, all.get(15).getCARDNO());
			list1.add(14, all.get(5).getCARDNO());
			list1.add(15, all.get(8).getCARDNO());
			list1.add(16, all.get(1).getCARDNO());
			list1.add(17, all.get(0).getCARDNO());
			list1.add(18, all.get(3).getCARDNO());
			list1.add(19, all.get(2).getCARDNO());           

			
			for(int j=0;j<list.size();j++) {				
				Map rm = new HashMap();
				rm.put("CARDNO", list1.get(j));							
				rm.put("CARDNAME", list.get(j));			
				Row r = new Row(rm);
				result.addRow(r);
			}
		}
		if(cardtype.equals("914") || cardtype.equals("915") || cardtype.equals("413") || cardtype.equals("415")) {
			logger.info("AdmCardDataDao.java cardtype is 914 915 413 415");
			query = "FROM ADMCARDDATA WHERE CARDATTR='C' AND CARDTYPE NOT IN('914','915','413','415') ORDER BY CARDNO";
			all=  find(query);
			logger.info("CreditUtil all size==>"+all.size());
			for(int i=0;i<all.size();i++) {				
				logger.info("org CreditUtil all CARDNAME["+i+"]==>"+all.get(i).getCARDNAME());
			}
			list.add(0, all.get(22).getCARDNAME());
			list.add(1, all.get(23).getCARDNAME());
			list.add(2, all.get(14).getCARDNAME());
			list.add(3, all.get(17).getCARDNAME());
			list.add(4, all.get(9).getCARDNAME());
			list.add(5, all.get(21).getCARDNAME());
			list.add(6, all.get(20).getCARDNAME());
			list.add(7, all.get(19).getCARDNAME());
			list.add(8, all.get(11).getCARDNAME());
			list.add(9, all.get(12).getCARDNAME());
			list.add(10, all.get(4).getCARDNAME());
			list.add(11, all.get(16).getCARDNAME());
			list.add(12, all.get(5).getCARDNAME());
			list.add(13, all.get(8).getCARDNAME());
			list.add(14, all.get(1).getCARDNAME());
			list.add(15, all.get(0).getCARDNAME());
			list.add(16, all.get(3).getCARDNAME());
			list.add(17, all.get(2).getCARDNAME());
			
			list1.add(0, all.get(22).getCARDNO());
			list1.add(1, all.get(23).getCARDNO());
			list1.add(2, all.get(14).getCARDNO());
			list1.add(3, all.get(17).getCARDNO());
			list1.add(4, all.get(9).getCARDNO());
			list1.add(5, all.get(21).getCARDNO());
			list1.add(6, all.get(20).getCARDNO());
			list1.add(7, all.get(19).getCARDNO());
			list1.add(8, all.get(11).getCARDNO());
			list1.add(9, all.get(12).getCARDNO());
			list1.add(10, all.get(4).getCARDNO());
			list1.add(11, all.get(16).getCARDNO());
			list1.add(12, all.get(5).getCARDNO());
			list1.add(13, all.get(8).getCARDNO());
			list1.add(14, all.get(1).getCARDNO());
			list1.add(15, all.get(0).getCARDNO());
			list1.add(16, all.get(3).getCARDNO());
			list1.add(17, all.get(2).getCARDNO());
			
			for(int j=0;j<list.size();j++) {				
				Map rm = new HashMap();
				rm.put("CARDNO", list1.get(j));							
				rm.put("CARDNAME", list.get(j));			
				Row r = new Row(rm);
				result.addRow(r);
			}
		}
		if(cardtype.equals("500") || cardtype.equals("400")) {
			logger.info("AdmCardDataDao.java cardtype is 500 400");
			query = "FROM ADMCARDDATA WHERE CARDATTR='C' AND CARDTYPE NOT IN('500','400') ORDER BY CARDNO";
			all=  find(query);
			logger.info("CreditUtil all size==>"+all.size());
			for(int i=0;i<all.size();i++) {				
				logger.info("org CreditUtil all CARDNAME["+i+"]==>"+all.get(i).getCARDNAME());
			}
			list.add(0, all.get(24).getCARDNAME());
			list.add(1, all.get(25).getCARDNAME());
			list.add(2, all.get(12).getCARDNAME());
			list.add(3, all.get(15).getCARDNAME());
			list.add(4, all.get(7).getCARDNAME());
			list.add(5, all.get(19).getCARDNAME());
			list.add(6, all.get(18).getCARDNAME());
			list.add(7, all.get(22).getCARDNAME());
			list.add(8, all.get(23).getCARDNAME());
			list.add(9, all.get(20).getCARDNAME());
			list.add(10, all.get(21).getCARDNAME());
			list.add(11, all.get(17).getCARDNAME());
			list.add(12, all.get(9).getCARDNAME());
			list.add(13, all.get(10).getCARDNAME());
			list.add(14, all.get(2).getCARDNAME());
			list.add(15, all.get(14).getCARDNAME());
			list.add(16, all.get(3).getCARDNAME());
			list.add(17, all.get(6).getCARDNAME());
			list.add(18, all.get(1).getCARDNAME());
			list.add(19, all.get(0).getCARDNAME());
			
			list1.add(0, all.get(24).getCARDNO());
			list1.add(1, all.get(25).getCARDNO());
			list1.add(2, all.get(12).getCARDNO());
			list1.add(3, all.get(15).getCARDNO());
			list1.add(4, all.get(7).getCARDNO());
			list1.add(5, all.get(19).getCARDNO());
			list1.add(6, all.get(18).getCARDNO());
			list1.add(7, all.get(22).getCARDNO());
			list1.add(8, all.get(23).getCARDNO());
			list1.add(9, all.get(20).getCARDNO());
			list1.add(10, all.get(21).getCARDNO());
			list1.add(11, all.get(17).getCARDNO());
			list1.add(12, all.get(9).getCARDNO());
			list1.add(13, all.get(10).getCARDNO());
			list1.add(14, all.get(2).getCARDNO());
			list1.add(15, all.get(14).getCARDNO());
			list1.add(16, all.get(3).getCARDNO());
			list1.add(17, all.get(6).getCARDNO());
			list1.add(18, all.get(1).getCARDNO());
			list1.add(19, all.get(0).getCARDNO());  
			
			for(int j=0;j<list.size();j++) {				
				Map rm = new HashMap();
				rm.put("CARDNO", list1.get(j));							
				rm.put("CARDNAME", list.get(j));			
				Row r = new Row(rm);
				result.addRow(r);
			}
		}
		if(cardtype.equals("902")) {
			logger.info("AdmCardDataDao.java cardtype is 902");
			query = "FROM ADMCARDDATA WHERE CARDATTR='C' AND CARDTYPE NOT IN('902') ORDER BY CARDNO";
			all=  find(query);
			logger.info("CreditUtil all size==>"+all.size());
			for(int i=0;i<all.size();i++) {				
				logger.info("org CreditUtil all CARDNAME["+i+"]==>"+all.get(i).getCARDNAME());
			}
			list.add(0, all.get(25).getCARDNAME());
			list.add(1, all.get(26).getCARDNAME());
			list.add(2, all.get(16).getCARDNAME());
			list.add(3, all.get(9).getCARDNAME());
			list.add(4, all.get(20).getCARDNAME());
			list.add(5, all.get(19).getCARDNAME());
			list.add(6, all.get(23).getCARDNAME());
			list.add(7, all.get(24).getCARDNAME());
			list.add(8, all.get(21).getCARDNAME());
			list.add(9, all.get(22).getCARDNAME());
			list.add(10, all.get(18).getCARDNAME());
			list.add(11, all.get(11).getCARDNAME());
			list.add(12, all.get(12).getCARDNAME());
			list.add(13, all.get(4).getCARDNAME());
			list.add(14, all.get(15).getCARDNAME());
			list.add(15, all.get(5).getCARDNAME());
			list.add(16, all.get(8).getCARDNAME());
			list.add(17, all.get(1).getCARDNAME());
			list.add(18, all.get(0).getCARDNAME());
			list.add(19, all.get(3).getCARDNAME());
			list.add(20, all.get(2).getCARDNAME());
			
			list1.add(0, all.get(25).getCARDNO());
			list1.add(1, all.get(26).getCARDNO());
			list1.add(2, all.get(16).getCARDNO());
			list1.add(3, all.get(9).getCARDNO());
			list1.add(4, all.get(20).getCARDNO());
			list1.add(5, all.get(19).getCARDNO());
			list1.add(6, all.get(23).getCARDNO());
			list1.add(7, all.get(24).getCARDNO());
			list1.add(8, all.get(21).getCARDNO());
			list1.add(9, all.get(22).getCARDNO());
			list1.add(10, all.get(18).getCARDNO());
			list1.add(11, all.get(11).getCARDNO());
			list1.add(12, all.get(12).getCARDNO());
			list1.add(13, all.get(4).getCARDNO());
			list1.add(14, all.get(15).getCARDNO());
			list1.add(15, all.get(5).getCARDNO());
			list1.add(16, all.get(8).getCARDNO());
			list1.add(17, all.get(1).getCARDNO());
			list1.add(18, all.get(0).getCARDNO());
			list1.add(19, all.get(3).getCARDNO());
			list1.add(20, all.get(2).getCARDNO());
			
			for(int j=0;j<list.size();j++) {				
				Map rm = new HashMap();
				rm.put("CARDNO", list1.get(j));							
				rm.put("CARDNAME", list.get(j));			
				Row r = new Row(rm);
				result.addRow(r);
			}
		}
		if(cardtype.equals("916") || cardtype.equals("917")) {
			logger.info("AdmCardDataDao.java cardtype is 916 917");
			query = "FROM ADMCARDDATA WHERE CARDATTR='C' AND CARDTYPE NOT IN('916','917') ORDER BY CARDNO";			
			all=  find(query);
			logger.info("CreditUtil all size==>"+all.size());
			for(int i=0;i<all.size();i++) {				
				logger.info("org CreditUtil all CARDNAME["+i+"]==>"+all.get(i).getCARDNAME());
			}
			list.add(0, all.get(14).getCARDNAME());
			list.add(1, all.get(17).getCARDNAME());
			list.add(2, all.get(9).getCARDNAME());
			list.add(3, all.get(21).getCARDNAME());
			list.add(4, all.get(20).getCARDNAME());
			list.add(5, all.get(24).getCARDNAME());
			list.add(6, all.get(25).getCARDNAME());
			list.add(7, all.get(22).getCARDNAME());
			list.add(8, all.get(23).getCARDNAME());
			list.add(9, all.get(19).getCARDNAME());
			list.add(10, all.get(11).getCARDNAME());
			list.add(11, all.get(12).getCARDNAME());
			list.add(12, all.get(4).getCARDNAME());
			list.add(13, all.get(16).getCARDNAME());
			list.add(14, all.get(5).getCARDNAME());
			list.add(15, all.get(8).getCARDNAME());
			list.add(16, all.get(1).getCARDNAME());
			list.add(17, all.get(0).getCARDNAME());
			list.add(18, all.get(3).getCARDNAME());
			list.add(19, all.get(2).getCARDNAME());	
			
			list1.add(0, all.get(14).getCARDNO());
			list1.add(1, all.get(17).getCARDNO());
			list1.add(2, all.get(9).getCARDNO());
			list1.add(3, all.get(21).getCARDNO());
			list1.add(4, all.get(20).getCARDNO());
			list1.add(5, all.get(24).getCARDNO());
			list1.add(6, all.get(25).getCARDNO());
			list1.add(7, all.get(22).getCARDNO());
			list1.add(8, all.get(23).getCARDNO());
			list1.add(9, all.get(19).getCARDNO());
			list1.add(10, all.get(11).getCARDNO());
			list1.add(11, all.get(12).getCARDNO());
			list1.add(12, all.get(4).getCARDNO());
			list1.add(13, all.get(16).getCARDNO());
			list1.add(14, all.get(5).getCARDNO());
			list1.add(15, all.get(8).getCARDNO());
			list1.add(16, all.get(1).getCARDNO());
			list1.add(17, all.get(0).getCARDNO());
			list1.add(18, all.get(3).getCARDNO());
			list1.add(19, all.get(2).getCARDNO());
			
			for(int j=0;j<list.size();j++) {				
				Map rm = new HashMap();
				rm.put("CARDNO", list1.get(j));							
				rm.put("CARDNAME", list.get(j));			
				Row r = new Row(rm);
				result.addRow(r);
			}
		}
		 
		return result;
	}
}