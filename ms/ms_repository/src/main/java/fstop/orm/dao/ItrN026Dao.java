package fstop.orm.dao;

import java.util.List;

import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.LegacyJpaRepository;

import fstop.orm.po.ITRN026;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@Repository
@Transactional
public class ItrN026Dao extends LegacyJpaRepository<ITRN026, String> {
	protected Logger logger = Logger.getLogger(getClass());

	public List<ITRN026> getAll() {
		return find("FROM ITRN026");
	}
	
	public void insert(ITRN026 itrn026) {
		int r2=0;
		String fillstr=new String();
		Query add1=null;
		
		add1=this.getSession().createSQLQuery("INSERT INTO ITRN026 VALUES(? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? )");
		add1.setParameter(0, itrn026.getRECNO());
		log.debug("RECNO = " + itrn026.getRECNO());
		add1.setParameter(1, itrn026.getHEADER());
		log.debug("HEADER = " + itrn026.getHEADER());
		add1.setParameter(2, itrn026.getSEQ());
		log.debug("SEQ = " + itrn026.getSEQ());
		add1.setParameter(3, itrn026.getDATE());
		log.debug("DATE = " + itrn026.getDATE());
		add1.setParameter(4, itrn026.getTIME());
		log.debug("TIME = " + itrn026.getTIME());
		add1.setParameter(5, itrn026.getCOUNT());
		log.debug("COUNT = " + itrn026.getCOUNT());
		add1.setParameter(6, itrn026.getCOLOR());
		log.debug("COLOR = " + itrn026.getCOLOR());
		add1.setParameter(7, itrn026.getTERM());
		log.debug("TERM = " + itrn026.getTERM());
		add1.setParameter(8, itrn026.getITR1());
		log.debug("ITR1 = " + itrn026.getITR1());
		add1.setParameter(9, itrn026.getITR2());
		log.debug("ITR2 = " + itrn026.getITR2());
		add1.setParameter(10, itrn026.getITR3());
		log.debug("ITR3 = " + itrn026.getITR3());
		add1.setParameter(11, itrn026.getITR4());
		log.debug("ITR4 = " + itrn026.getITR4());
		add1.setParameter(12, itrn026.getITR5());
		log.debug("ITR5 = " + itrn026.getITR5());
		add1.setParameter(13, itrn026.getITR6());
		log.debug("ITR6 = " + itrn026.getITR6());
		if(itrn026.getFILL()==null)
			fillstr=" ";
		else
			fillstr=itrn026.getFILL();
		log.debug("FILL = " + fillstr);
		add1.setParameter(14, fillstr);
		r2 = add1.executeUpdate();
	}

	public void deleteAll() {
		this.getSession().createSQLQuery("DELETE FROM ITRN026").executeUpdate();
	}
}
