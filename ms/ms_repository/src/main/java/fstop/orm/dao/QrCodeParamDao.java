package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import fstop.orm.po.QRCODEPARAM;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Repository
public class QrCodeParamDao extends LegacyJpaRepository<QRCODEPARAM, String> {
	protected Logger logger = Logger.getLogger(getClass());

	@Transactional(readOnly = true)
	public QRCODEPARAM findByName(String key) {
		List<QRCODEPARAM> list = find("FROM QRCODEPARAM WHERE NAME=? ", key);
		if(list.size() > 0) {
			return list.get(0);
		}
		else {
			return null;
		}
	}
}
