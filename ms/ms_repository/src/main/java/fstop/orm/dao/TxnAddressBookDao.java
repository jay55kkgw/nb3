package fstop.orm.dao;

import java.util.List;
import java.util.function.BiFunction;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;
import com.netbank.domain.orm.core.SessionAdapter;

import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.TXNADDRESSBOOK;

@Slf4j
@Repository
@Transactional(
		transactionManager="nb3transactionManager",
        rollbackFor = {Throwable.class}
)
public class TxnAddressBookDao extends LegacyJpaRepository<TXNADDRESSBOOK, Integer> {
	protected Logger logger = Logger.getLogger(getClass());
	
	
	public List<TXNADDRESSBOOK> findByDPUserID(String DPUSERID) {
		List<TXNADDRESSBOOK> result = find("FROM TXNADDRESSBOOK WHERE DPUSERID = ?", DPUSERID);
		
		return result;
		
	}


	@Override
	public EntityManager getEntityManager() {
		// TODO Auto-generated method stub
		return super.getEntityManager();
	}


	@Override
	public long count() {
		// TODO Auto-generated method stub
		return super.count();
	}


	@Override
	public List<TXNADDRESSBOOK> findAll() {
		// TODO Auto-generated method stub
		return super.findAll();
	}


	@Override
	public TXNADDRESSBOOK findById(Integer id) {
		// TODO Auto-generated method stub
		return super.findById(id);
	}


	@Override
	public void save(TXNADDRESSBOOK entity) {
		// TODO Auto-generated method stub
		super.save(entity);
	}


	@Override
	public TXNADDRESSBOOK update(TXNADDRESSBOOK entity) {
		// TODO Auto-generated method stub
		return super.update(entity);
	}


	@Override
	public void delete(TXNADDRESSBOOK entity) {
		// TODO Auto-generated method stub
		super.delete(entity);
	}


	@Override
	public Query getQuery(String hql, Object... values) {
		// TODO Auto-generated method stub
		return super.getQuery(hql, values);
	}


	@Override
	public Query getCriteriaQuery(Class<TXNADDRESSBOOK> entityClass,
			BiFunction<Root, CriteriaBuilder, Predicate>... criteriaFunction) {
		// TODO Auto-generated method stub
		return super.getCriteriaQuery(entityClass, criteriaFunction);
	}


	@Override
	public List find(String hql, Object... values) {
		// TODO Auto-generated method stub
		return super.find(hql, values);
	}


	@Override
	public List<TXNADDRESSBOOK> findBy(Class<TXNADDRESSBOOK> entityClass, String name, Object value) {
		// TODO Auto-generated method stub
		return super.findBy(entityClass, name, value);
	}


	@Override
	public TXNADDRESSBOOK findUniqueBy(Class<TXNADDRESSBOOK> entityClass, String name, Object value) {
		// TODO Auto-generated method stub
		return super.findUniqueBy(entityClass, name, value);
	}


	@Override
	public List<TXNADDRESSBOOK> findByLike(Class<TXNADDRESSBOOK> entityClass, String name, String value) {
		// TODO Auto-generated method stub
		return super.findByLike(entityClass, name, value);
	}


	@Override
	public SessionAdapter getSession() {
		// TODO Auto-generated method stub
		return super.getSession();
	}


	@Override
	public Page pagedQuery(String hql, int pageNo, int pageSize, Object... values) {
		// TODO Auto-generated method stub
		return super.pagedQuery(hql, pageNo, pageSize, values);
	}
	
}
