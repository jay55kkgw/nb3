package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.util.ESAPIUtil;

import fstop.orm.po.GOLDPRICE;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.List;

@Slf4j
@Repository
@Transactional
public class GoldPriceDao extends LegacyJpaRepository<GOLDPRICE, String> {
	protected Logger logger = Logger.getLogger(getClass());
	
	public List<GOLDPRICE> getGoldPriceid(String goldpriceid) {
		log.debug("getGoldPriceid goldpriceid = " + goldpriceid);
		return find("FROM GOLDPRICE e WHERE GOLDPRICEID = ? ORDER BY GOLDPRICEID", goldpriceid);
	}
	
	public List<GOLDPRICE> getMaxRecord() {
		log.debug("getMaxRecord = ");
		//SELECT * FROM GOLDPRICE WHERE GOLDPRICEID IN( SELECT GOLDPRICEID FROM GOLDPRICE WHERE QTIME IN (SELECT  MAX(QTIME)  FROM GOLDPRICE GROUP BY QDATE))
		return find("FROM GOLDPRICE e WHERE GOLDPRICEID IN( SELECT GOLDPRICEID FROM GOLDPRICE WHERE QTIME IN (SELECT  MAX(QTIME)  FROM GOLDPRICE GROUP BY QDATE))");
		
	}
	
	public List<GOLDPRICE> getRecord(String querytype,String from,String to) {
		List<GOLDPRICE> rc = null;
		//querytype = "TODAYLAST";
		if(querytype.equals("TODAYLAST"))
			rc = getTODAYLastRecord(from,to);
		if(querytype.equals("TODAY"))
			rc = getTODAYRecord(from,to);
		if(querytype.equals("SOMEDAY"))
			rc = getSOMEDAYRecord(from,to);
		if(querytype.equals("LASTDAY"))
			rc = getLASTDAYRecord(from,to);
		if(querytype.equals("LASTMON"))
			rc = getLASTMONRecord(from,to);
		if(querytype.equals("LASTSIXMON"))
			rc = getLASTSIXMONRecord(from,to);
		if(querytype.equals("LASTYEAR"))
			rc = getLASTYEARRecord(from,to);
		if(querytype.equals("PERIOD"))
		{
			rc = getPERIODRecord(from,to);
		}
		return rc;
	}
	
	public List<GOLDPRICE> getTODAYLastRecord(String datefrom ,String dateto) {
		
		return find("FROM GOLDPRICE e WHERE GOLDPRICEID IN(SELECT  MAX(GOLDPRICEID)  FROM GOLDPRICE WHERE QDATE = ?)",datefrom);
	}
	
	public List<GOLDPRICE> getTODAYRecord(String datefrom ,String dateto) {
		return find("FROM GOLDPRICE e WHERE QDATE = ?",datefrom);
	}

	public List<GOLDPRICE> getSOMEDAYRecord(String datefrom ,String dateto) {
		return find("FROM GOLDPRICE e WHERE QDATE = ?",datefrom);
	}
	
	public List<GOLDPRICE> getLASTDAYRecord(String datefrom ,String dateto) {
	//SELECT * FROM GOLDPRICE WHERE QDATE IN(SELECT MAX(QDATE) FROM GOLDPRICE)	
		return find("FROM GOLDPRICE e WHERE QDATE IN(SELECT MAX(QDATE) FROM GOLDPRICE)");
	}
	
	public List<GOLDPRICE> getLASTMONRecord(String datefrom ,String dateto) {
		return find("FROM GOLDPRICE e WHERE GOLDPRICEID IN(SELECT  MAX(GOLDPRICEID)  FROM GOLDPRICE GROUP BY QDATE) AND QDATE >= ? AND QDATE <= ? ORDER BY QDATE",datefrom,dateto);
	}
	
	public List<GOLDPRICE> getLASTSIXMONRecord(String datefrom ,String dateto) {
		return find("FROM GOLDPRICE e WHERE GOLDPRICEID IN(SELECT  MAX(GOLDPRICEID)  FROM GOLDPRICE GROUP BY QDATE) AND QDATE >= ? and QDATE <= ? ORDER BY QDATE",datefrom,dateto);
	}
	
	public List<GOLDPRICE> getLASTYEARRecord(String datefrom ,String dateto) {
		return find("FROM GOLDPRICE e WHERE GOLDPRICEID IN(SELECT  MAX(GOLDPRICEID)  FROM GOLDPRICE GROUP BY QDATE) AND QDATE >= ? and QDATE <= ? ORDER BY QDATE",datefrom,dateto);
	}
	
	public List<GOLDPRICE> getPERIODRecord(String datefrom ,String dateto) {
		//datefrom = "2008/01/01";
		//dateto = "2009/12/31";
		log.debug(ESAPIUtil.vaildLog("datefrom = "+datefrom+"==="));
		log.debug(ESAPIUtil.vaildLog("dateto = "+dateto+"==="));
		return find("FROM GOLDPRICE e WHERE GOLDPRICEID IN(SELECT  MAX(GOLDPRICEID)  FROM GOLDPRICE GROUP BY QDATE) AND QDATE >= ? and QDATE <= ? ORDER BY QDATE",datefrom,dateto);
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED)	
	public boolean findGoldPriceidIsExists(String goldpriceid) {
		log.debug("findGoldPriceidIsExists goldpriceid = " + goldpriceid);
		return ((Long)find("SELECT count(*) FROM GOLDPRICE WHERE GOLDPRICEID = ?",goldpriceid).iterator().next()) > 0;
	}
	
	public void insert(GOLDPRICE goldprice) {
		int r2=0;
		Query add1=null;
		add1=this.getSession().createSQLQuery("INSERT INTO GOLDPRICE VALUES(? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,?)");
		add1.setParameter(0, goldprice.getGOLDPRICEID());
		add1.setParameter(1, goldprice.getQDATE());
		add1.setParameter(2, goldprice.getQTIME());
		add1.setParameter(3, goldprice.getBPRICE());
		add1.setParameter(4, goldprice.getSPRICE());
		add1.setParameter(5, goldprice.getPRICE1());
		add1.setParameter(6, goldprice.getPRICE2());
		add1.setParameter(7, goldprice.getPRICE3());
		add1.setParameter(8, goldprice.getPRICE4());
		add1.setParameter(9, goldprice.getLASTDATE());
		add1.setParameter(10, goldprice.getLASTTIME());
		r2 = add1.executeUpdate();
	}
}
