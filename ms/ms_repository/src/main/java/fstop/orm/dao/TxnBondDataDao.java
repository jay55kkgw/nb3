package fstop.orm.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.util.CodeUtil;

import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.model.Rows;
import fstop.orm.po.TXNBONDDATA;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Transactional
public class TxnBondDataDao extends LegacyJpaRepository<TXNBONDDATA, String> {
	
	@Transactional(rollbackFor = Exception.class)
    public Boolean saveData(MVHImpl result) {
    	Boolean rs = Boolean.FALSE;
    	log.info("saveData...");
    	Rows rows = result.getOccurs();
    	TXNBONDDATA po =null;
    	try {
			deleteAll();
//			int i =0;
			for(Row r :rows.getRows()) {
				po = new TXNBONDDATA();
				po = CodeUtil.objectCovert(TXNBONDDATA.class, r.getValues());
				log.trace("row>>{}", CodeUtil.toJson(r.getValues()));
				log.trace("TXNBONDDATA>>{}", po.toString());
//				if(i==10) {
//					po = null;
//				}
				save(po);
//				i++;
			}
			log.info("saveData...end");
			rs = Boolean.TRUE;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e.toString());
			throw e;
		}
		return rs;
    }
	
	@Transactional(rollbackFor = Exception.class)
	public void deleteAll() {
		this.getSession().createQuery("DELETE FROM TXNBONDDATA").executeUpdate();
	}
}
