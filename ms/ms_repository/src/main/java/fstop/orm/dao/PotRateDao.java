package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import fstop.orm.po.POTRATE;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Repository
@Transactional
public class PotRateDao extends LegacyJpaRepository<POTRATE, String> {
	
	public String findByRate(String pot) {
		List<POTRATE> result = find("FROM POTRATE WHERE POT = ? ORDER BY POT", pot);
		if (result.size()>0)
			return result.get(0).getPOTRATE();
		else
			return "";
	}	
	public void deleteAll() {
		this.getSession().createQuery("DELETE FROM POTRATE").executeUpdate();
	}	
}
