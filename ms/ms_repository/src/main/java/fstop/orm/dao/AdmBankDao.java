package fstop.orm.dao;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.LegacyJpaRepository;

import fstop.orm.po.ADMBANK;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Transactional
public class AdmBankDao extends LegacyJpaRepository<ADMBANK, Serializable> {
	protected Logger logger = Logger.getLogger(getClass());
	
	public List<ADMBANK> getAll() {
		return find("FROM ADMBANK ORDER BY ADBANKID");
	}
	
	public ADMBANK getbank(String adbankid) {
//		return (ADMBANK)find("FROM ADMBANK WHERE ADBANKID = ?",adbankid).get(0);
		return findById(StrUtils.trim(adbankid));
	}
	
}