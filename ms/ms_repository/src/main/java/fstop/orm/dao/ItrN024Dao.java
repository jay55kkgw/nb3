package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import fstop.orm.po.ITRN024;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.List;

@Slf4j
@Repository
@Transactional
public class ItrN024Dao extends LegacyJpaRepository<ITRN024, String> {
	protected Logger logger = Logger.getLogger(getClass());

	public List<ITRN024> getAll() {
		return find("FROM ITRN024 ORDER BY RECNO");
	}
	
	public void insert(ITRN024 itrn024) {
		int r2=0;
		String fillstr=new String();
		Query add1=null;
		
		add1=this.getSession().createSQLQuery("INSERT INTO ITRN024 VALUES(? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,?)");
		add1.setParameter(0, itrn024.getRECNO());
		log.debug("RECNO = " + itrn024.getRECNO());
		add1.setParameter(1, itrn024.getHEADER());
		log.debug("HEADER = " + itrn024.getHEADER());
		add1.setParameter(2, itrn024.getSEQ());
		log.debug("SEQ = " + itrn024.getSEQ());
		add1.setParameter(3, itrn024.getDATE());
		log.debug("DATE = " + itrn024.getDATE());
		add1.setParameter(4, itrn024.getTIME());
		log.debug("TIME = " + itrn024.getTIME());
		add1.setParameter(5, itrn024.getCOUNT());
		log.debug("COUNT = " + itrn024.getCOUNT());
		add1.setParameter(6, itrn024.getCOLOR());
		log.debug("COLOR = " + itrn024.getCOLOR());
		add1.setParameter(7, itrn024.getCRYNAME());
		log.debug("CRYNAME = " + itrn024.getCRYNAME());
		add1.setParameter(8, itrn024.getCRY());
		log.debug("CRY = " + itrn024.getCRY());
		add1.setParameter(9, itrn024.getITR1());
		log.debug("ITR1 = " + itrn024.getITR1());
		add1.setParameter(10, itrn024.getITR2());
		log.debug("ITR2 = " + itrn024.getITR2());
		if(itrn024.getFILL()==null)
			fillstr=" ";
		else
			fillstr=itrn024.getFILL();
		log.debug("FILL = " + fillstr);
		add1.setParameter(11, fillstr);
		r2 = add1.executeUpdate();
	}

	public void deleteAll() {
		this.getSession().createSQLQuery("DELETE FROM ITRN024").executeUpdate();
	}
}
