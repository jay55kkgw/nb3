package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.util.ESAPIUtil;

import fstop.model.Row;
import fstop.model.Rows;
import fstop.orm.po.ADMZIPDATA;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Slf4j
@Repository
@Transactional
public class AdmZipDataDao extends LegacyJpaRepository<ADMZIPDATA, String> {
	protected Logger logger = Logger.getLogger(getClass());
	
	public List<ADMZIPDATA> getAll() {
		return find("FROM ADMZIPDATA ORDER BY ZIPCODE");
	}
	
	/**
	 * 查詢City
	 * @return
	 */
	public Rows findCity() {
		String sql="SELECT CITY FROM ADMZIPDATA GROUP BY CITY";
		Query query = getSession().createSQLQuery(sql);
		Rows result = new Rows();
		
		// Stored XSS
		List<Object> qresult = query.getResultList();
		if (null == qresult) {
			qresult = new ArrayList();
		}
		qresult = ESAPIUtil.validList(qresult);
//		List qresult = query.getResultList();
		
		Iterator it = qresult.iterator();
		while(it.hasNext()) {
			Map rm = new HashMap();
	        String element = it.next().toString();

			rm.put("CITY", element);			
			//log.debug("AdmZipDataDao.java City:"+ element);
			Row r = new Row(rm);
			result.addRow(r);
		}
		//log.debug("AdmZipDataDao.java findcity size:"+result.getSize());		
		return result;
	}	
	/**
	 * 查詢Area
	 * @return
	 */
	public Rows findArea(String city) {
		
		String sql = "SELECT AREA FROM ADMZIPDATA WHERE CITY = ? GROUP BY AREA";
		Query query = getSession().createSQLQuery(sql);
		query.setParameter(0, city);
		Rows result = new Rows();
		
		// Stored XSS
		List<Object> qresult = query.getResultList();
		if (null == qresult) {
			qresult = new ArrayList();
		}
		qresult = ESAPIUtil.validList(qresult);
//		List qresult = query.getResultList();
		
		Iterator it = qresult.iterator();
		while(it.hasNext()) {
			Map rm = new HashMap();
	        String element = it.next().toString();

			rm.put("AREA", element);			
			
			// CGI Stored XSS
//			log.debug("AdmZipDataDao.java area:"+ element);
			
			Row r = new Row(rm);
			result.addRow(r);
		}
		log.debug("AdmZipDataDao.java findarea size:"+result.getSize());		
		return result;
	}	
	/**
	 * 查詢ZIPCODE By CITY AREA
	 * @return
	 */
	public String findZipBycityarea(String city,String area) {
		
		Query query=getSession().createSQLQuery("SELECT ZIPCODE FROM ADMZIPDATA WHERE CITY = ? AND AREA = ? GROUP BY ZIPCODE");
		query.setParameter(0, city);
		query.setParameter(1, area);
		List qresult = query.getResultList();
		return qresult.get(0).toString();
	}	
}