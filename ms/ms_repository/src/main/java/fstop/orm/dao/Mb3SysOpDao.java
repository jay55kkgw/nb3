package fstop.orm.dao;


import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.hibernate.query.NativeQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.util.ESAPIUtil;

import fstop.orm.po.MB3SYSOP;
import fstop.orm.po.NB3SYSOP;
 
@Transactional
@Component
public class Mb3SysOpDao extends LegacyJpaRepository<MB3SYSOP, Serializable> {
	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	public Map<String,String> m911AdopidMap() {
		String sql = "SELECT *  FROM MB3SYSOP WHERE ADGPPARENT <>'GPE' ORDER BY ADOPID ASC";
		Query query = getSession().createSQLQuery(sql);
		query.unwrap(NativeQuery.class).addEntity("MB3SYSOP", MB3SYSOP.class);
		List<MB3SYSOP> vlist = ESAPIUtil.validStrList(query.getResultList());
		
		Map<String,String> rtnMap = new HashMap<String,String>();
		for(MB3SYSOP each:vlist) {
			rtnMap.put(each.getADOPID(), each.getADOPNAME());
		}
		 return rtnMap;
	}
	
} 
