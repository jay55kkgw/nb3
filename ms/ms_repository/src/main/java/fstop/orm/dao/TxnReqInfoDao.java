package fstop.orm.dao;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.LegacyJpaRepository;

import fstop.orm.po.TXNREQINFO;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Transactional
public class TxnReqInfoDao extends LegacyJpaRepository<TXNREQINFO, String> {
	protected Logger logger = Logger.getLogger(getClass());

	public String getReqInfo(String id) {
		TXNREQINFO info = findById(id);
		return info.getREQINFO();
	}
}
