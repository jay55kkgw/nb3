package fstop.orm.dao;


import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.hibernate.query.NativeQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.util.ESAPIUtil;

import fstop.orm.po.NB3SYSOP;
 
@Transactional
@Component
public class Nb3SysOpDao extends LegacyJpaRepository<NB3SYSOP, Serializable> {
	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	@SuppressWarnings( { "unused", "unchecked" })
	public List<NB3SYSOP> findByADOPId(String ADOPID) {
		
		String sql = "from NB3SYSOP where ADOPID = ? ";
		
		Query query = getQuery(sql, ADOPID);
		
		return ESAPIUtil.validStrList(query.getResultList());
	}

	@SuppressWarnings( { "unused", "unchecked" })
	public List<NB3SYSOP> getOfflineList() {
		
		//String ResultStr = "";
		String sql = "from NB3SYSOP where ADOPALIVE = 'N' ";
		
		Query query = getQuery(sql);
		
		/*
		if(ResultList.size() > 0)
		{
			for(int i = 0; i < ResultList.size(); i++)
			{
				if(ResultStr.length() > 0)ResultStr += ",";
				ResultStr += ResultList.get(i).getADOPID();
			}
		}
		*/
		
		return ESAPIUtil.validStrList(query.getResultList());
	}

	@SuppressWarnings( { "unused", "unchecked" })
	public String updateStatus(String Items, String StatusCode) {
		String UpdateResult = "";
		
		try
		{
			Query updateQueryStr=this.getSession().createSQLQuery("UPDATE NB3SYSOP SET ADOPALIVE = '" + StatusCode + "' WHERE ADOPID IN (" + Items + ")");
			log.debug("QueryStr =================================> " + updateQueryStr.toString());
			//updateQueryStr.setString(0, StatusCode);
			//updateQueryStr.setString(1, Items);
			
			int updateRecords = updateQueryStr.executeUpdate();
			
			UpdateResult = "Success," + StatusCode + "," + updateRecords;
		}
		catch(Exception UpdateSysOpEx)
		{
			UpdateResult = UpdateSysOpEx.getMessage();
		}
		
		return UpdateResult;
	}
	
	public List<NB3SYSOP> getUrlMenu() {
		String sql = "SELECT * from NB3SYSOP where URL <> '/' and ADOPAUTHTYPE <> ''";

		Query query = getSession().createSQLQuery(sql);
		query.unwrap(NativeQuery.class).addEntity("NB3SYSOP", NB3SYSOP.class);
		return ESAPIUtil.validStrList(query.getResultList());
	}
	
	public Map<String,String> m911AdopidMap() {
		String sql = "SELECT *  FROM NB3SYSOP WHERE ADGPPARENT <>'GPE' ORDER BY ADOPID ASC";
		Query query = getSession().createSQLQuery(sql);
		query.unwrap(NativeQuery.class).addEntity("NB3SYSOP", NB3SYSOP.class);
		List<NB3SYSOP> vlist = ESAPIUtil.validStrList(query.getResultList());
		
		Map<String,String> rtnMap = new HashMap<String,String>();
		for(NB3SYSOP each:vlist) {
			rtnMap.put(each.getADOPID(), each.getADOPNAME());
		}
		 return rtnMap;
	}
	
} 
