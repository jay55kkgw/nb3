package fstop.orm.dao;

import java.io.Serializable;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.LegacyJpaRepository;

import fstop.orm.po.TRNSXML;
import fstop.orm.po.TRNSXMLID;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Transactional
public class TrnsXmlDao  extends LegacyJpaRepository<TRNSXML, Serializable>{
	
	public void insertTrnsXml(int apitxnno, String mti, String pcode, String txn_datetime, String ebillXml) throws Exception {
		TRNSXMLID id = new TRNSXMLID(apitxnno, mti, pcode);
		TRNSXML trnsXml = new TRNSXML();
		trnsXml.setId(id);
		trnsXml.setTXN_DATETIME(txn_datetime);
		trnsXml.setXML(ebillXml);
		save(trnsXml);
	}

}
