package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;
import com.netbank.domain.orm.core.SessionAdapter;

import fstop.orm.po.ADMNBSTATUS;
import fstop.orm.po.TXNUSER;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;
import java.util.List;
import java.util.function.BiFunction;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Slf4j
@Repository
@Transactional
public class AdmNbStatusDao extends LegacyJpaRepository<ADMNBSTATUS, String> {
	protected Logger logger = Logger.getLogger(getClass());

    public void setServerStatus(String severName) {
    	try {
    		if(this.findById(severName) != null) {
    			ADMNBSTATUS data = findById(severName);
    			Date t = new Date();
    			data.setLASTDATE(DateTimeUtils.format("yyyyMMdd", t));
    			data.setLASTTIME(DateTimeUtils.format("HHmmss", t));
    			this.save(data);
    		}else {
    			ADMNBSTATUS data = new ADMNBSTATUS();
    			data.setADNBSTATUSID(severName);
    			data.setADNBSTATUS("Y");
    			data.setLASTUSER("SYS");
    			Date t = new Date();
    			data.setLASTDATE(DateTimeUtils.format("yyyyMMdd", t));
    			data.setLASTTIME(DateTimeUtils.format("HHmmss", t));
    			this.save(data);
    		}
    	}catch(Exception e) {
            log.error("取得 ADMNBSTATUS 有誤 ." + e.getMessage());
    	}
    }
	
}
