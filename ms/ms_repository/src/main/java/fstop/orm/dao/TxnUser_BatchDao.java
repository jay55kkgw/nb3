package fstop.orm.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.hibernate.exception.LockAcquisitionException;
import org.hibernate.query.NativeQuery;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;
import com.netbank.domain.orm.core.SessionAdapter;
import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.model.Rows;
import fstop.orm.po.TXNUSER;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository

@Transactional(
		transactionManager="nb3transactionManager",
        rollbackFor = {Throwable.class}
)
public class TxnUser_BatchDao extends LegacyJpaRepository<TXNUSER, String> {
	protected Logger logger = Logger.getLogger(getClass());
	
	public Map<String,String> userbrhcodMap(){
		try {
			Map<String,String> userbrhcodMap = new HashMap<String,String>();
			Query query = getSession().createSQLQuery("SELECT DPSUERID, ADBRANCHID FROM TXNUSER WITH UR");
			query.unwrap(NativeQuery.class)
				 .addScalar("DPSUERID", StringType.INSTANCE)
				 .addScalar("ADBRANCHID", StringType.INSTANCE);
			List polist = ESAPIUtil.validStrList(query.getResultList());
			Iterator it = polist.iterator();
			while(it.hasNext()) 
			{				
				Object[] o = (Object[])it.next();
				userbrhcodMap.put(o[0].toString(), o[1].toString());
			}
			return userbrhcodMap;
		}catch(Exception e) {
			log.error("{}", e);
			throw e;
		}
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateBranchid(String userId , String adbranchid){
		try {
			log.info("TxnUser start save >> {}", userId);
			Query add = this.getSession().createSQLQuery("UPDATE TXNUSER SET ADBRANCHID = ? WHERE DPSUERID = ?");
			add.setParameter(0, adbranchid);
			add.setParameter(1, userId);
			int r2 = add.executeUpdate();
			log.info("TxnUser finish save >> {}", userId);
		}catch(LockAcquisitionException e) {
			log.error("TxnUser LockAcquisitionException >> {}", userId);
			throw e;
		}
	}
	@Override
	public EntityManager getEntityManager() {
		// TODO Auto-generated method stub
		return super.getEntityManager();
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return super.count();
	}

	@Override
	public List<TXNUSER> findAll() {
		// TODO Auto-generated method stub
		return super.findAll();
	}

	@Override
	public TXNUSER findById(String id) {
		// TODO Auto-generated method stub
		return super.findById(id);
	}

	@Override
	public void save(TXNUSER entity) {
		try {
			// TODO Auto-generated method stub
			super.save(entity);
			log.info("TxnUser finish save >> {}", entity);
		}catch(LockAcquisitionException e) {
			log.error("TxnUser LockAcquisitionException >> {}", entity);
			throw e;
		}
	}

	@Override
	public TXNUSER update(TXNUSER entity) {
		try {
			// TODO Auto-generated method stub
			TXNUSER po = super.update(entity);
			log.info("TxnUser finish update >> {}", entity);
			return po;
		}catch(LockAcquisitionException e) {
			log.error("TxnUser LockAcquisitionException >> {}", entity);
			throw e;
		}
	}

	@Override
	public void delete(TXNUSER entity) {
		try {
			// TODO Auto-generated method stub
			super.delete(entity);
			log.info("TxnUser finish delete >> {}", entity);
		}catch(LockAcquisitionException e) {
			log.error("TxnUser LockAcquisitionException >> {}", entity);
			throw e;
		}
	}

	@Override
	public Query getQuery(String hql, Object... values) {
		// TODO Auto-generated method stub
		return super.getQuery(hql, values);
	}

	@Override
	public Query getCriteriaQuery(Class<TXNUSER> entityClass,
			BiFunction<Root, CriteriaBuilder, Predicate>... criteriaFunction) {
		// TODO Auto-generated method stub
		return super.getCriteriaQuery(entityClass, criteriaFunction);
	}

	@Override
	public List find(String hql, Object... values) {
		// TODO Auto-generated method stub
		return super.find(hql, values);
	}

	@Override
	public List<TXNUSER> findBy(Class<TXNUSER> entityClass, String name, Object value) {
		// TODO Auto-generated method stub
		return super.findBy(entityClass, name, value);
	}

	@Override
	public TXNUSER findUniqueBy(Class<TXNUSER> entityClass, String name, Object value) {
		// TODO Auto-generated method stub
		return super.findUniqueBy(entityClass, name, value);
	}

	@Override
	public List<TXNUSER> findByLike(Class<TXNUSER> entityClass, String name, String value) {
		// TODO Auto-generated method stub
		return super.findByLike(entityClass, name, value);
	}

	@Override
	public SessionAdapter getSession() {
		// TODO Auto-generated method stub
		return super.getSession();
	}

	@Override
	public Page pagedQuery(String hql, int pageNo, int pageSize, Object... values) {
		// TODO Auto-generated method stub
		return super.pagedQuery(hql, pageNo, pageSize, values);
	}
	
}