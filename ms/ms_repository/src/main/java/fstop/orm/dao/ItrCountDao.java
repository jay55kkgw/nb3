package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import fstop.orm.po.ITRCOUNT;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.List;


@Slf4j
@Repository
@Transactional
public class ItrCountDao extends LegacyJpaRepository<ITRCOUNT, String> {
	protected Logger logger = Logger.getLogger(getClass());
	public List<ITRCOUNT> getAll() {
		return find("FROM ITRCOUNT");
	}
	
	public String getCountSum()
	{
		Query query=getSession().createSQLQuery("SELECT sum(int(char(\"COUNT\")))  from itrcount");
		List qresult = query.getResultList();
		return((String)Integer.toString((Integer)qresult.get(0)));
	}

	public void deleteAll() {
		this.getSession().createSQLQuery("DELETE FROM ITRCOUNT").executeUpdate();
	}

}