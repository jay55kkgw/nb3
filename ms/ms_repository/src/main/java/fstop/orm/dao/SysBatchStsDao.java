package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import fstop.orm.po.SYSBATCHSTS;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Slf4j
@Repository
@Transactional
public class SysBatchStsDao extends LegacyJpaRepository<SYSBATCHSTS, String> {
	protected Logger logger = Logger.getLogger(getClass());
	
}
