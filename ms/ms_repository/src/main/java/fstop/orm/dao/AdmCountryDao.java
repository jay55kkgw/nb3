package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import fstop.orm.po.ADMCOUNTRY;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Slf4j
@Repository
@Transactional
public class AdmCountryDao extends LegacyJpaRepository<ADMCOUNTRY, String> {
	protected Logger logger = Logger.getLogger(getClass());
	public List<ADMCOUNTRY> getAll() {
		return find("FROM ADMCOUNTRY");
	}	
	public ADMCOUNTRY findUniqueByCntrid(String cntrid) {
		try {
			return (ADMCOUNTRY)find("FROM ADMCOUNTRY WHERE ADCTRYCODE = ? ", cntrid)
						.get(0);
		}
		catch(IndexOutOfBoundsException e) {}
		
		return null;
	}	
}
