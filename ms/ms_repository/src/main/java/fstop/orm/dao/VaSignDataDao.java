package fstop.orm.dao;

import java.util.List;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.VASIGNDATA;


@Slf4j
@Repository
@Transactional
public class VaSignDataDao extends LegacyJpaRepository<VASIGNDATA, String> {
	protected Logger logger = Logger.getLogger(getClass());
	public List<VASIGNDATA> getAll() {
		return find("FROM VASIGNDATA");
	}	
	public VASIGNDATA findUniqueByCUSIDN(String cusidn) {
		try {
			return (VASIGNDATA)find("FROM VASIGNDATA WHERE CUSIDN = ? ", cusidn)
						.get(0);
		}
		catch(IndexOutOfBoundsException e) {}
		
		return null;
	}	
}
