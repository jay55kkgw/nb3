package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import fstop.orm.po.HK_TRNS_DATA;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Repository
@Transactional
public class HkTrnsDataDao extends LegacyJpaRepository<HK_TRNS_DATA, String> {
	protected Logger logger = Logger.getLogger(getClass());
	
	
	public List<HK_TRNS_DATA> findByUserData(String EDIID , String HKFLAG , String VID) {
		
		ArrayList al = new ArrayList();
		String qStr = " FROM HK_TRNS_DATA ";
		int iNo= 0;
		Object obj[] = new Object[4];
		
		String tmpStr = "WHERE ";
		boolean first = true;
		if(!"".equals(EDIID))
		{
			if(first)
			{
				qStr = qStr+tmpStr+"EDIID = ? ";
				tmpStr = "and ";
				first = false;
			}else
			{
				qStr = qStr+tmpStr+"EDIID = ? ";
			}
			obj[iNo] = EDIID;
			iNo++;
		}
		if(!"".equals(HKFLAG))
		{
			if(first)
			{	
				qStr = qStr+tmpStr+" HKFLAG = ? ";
				tmpStr = "and ";
				first = false;
			}else
			{
				qStr = qStr+tmpStr+" HKFLAG = ? ";
			}
			obj[iNo] = HKFLAG;
			iNo++;
		}
		if(!"".equals(VID))
		{
			if(first)
			{
				qStr = qStr+tmpStr+" VID = ? ";
				tmpStr = "and ";
				first = false;
			}else
			{
				qStr = qStr+tmpStr+" VID = ? ";
			}
			obj[iNo] = VID;
			iNo++;
		}
		Object[] rcObj = new Object[iNo];
		for(int i=0;i<iNo;i++)
		{
			rcObj[i] = obj[i];
			log.debug("rcObj==="+rcObj[i]+"@@@");
		}
		return find(qStr,rcObj);
	}

	
}
