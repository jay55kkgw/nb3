package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import fstop.orm.po.TXNCASHAPPLY;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.List;

@Slf4j
@Repository
@Transactional
public class TxnCashApplyDao extends LegacyJpaRepository<TXNCASHAPPLY, String> {
	protected Logger logger = Logger.getLogger(getClass());


	/**
	 * 查詢某一筆案件編號
	 * @return
	 */
	public List<TXNCASHAPPLY> findByRCVNO(String rcvno) {
		return find("FROM TXNCASHAPPLY WHERE RCVNO = ?", rcvno);
	}
	/**
	 * 取得某一日的申請新鈔兑換筆數
	 * @param currentDate
	 * @return
	 */
	public int countCashRecord(String currentDate) {

		String sql = "from TXNCASHAPPLY where LASTDATE = ?";
		
		Query query = getQuery(sql, currentDate);
		return query.getResultList().size();
	}		
}