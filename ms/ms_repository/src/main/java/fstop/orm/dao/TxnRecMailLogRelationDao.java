package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import fstop.orm.po.TXNRECMAILLOGRELATION;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Repository
@Transactional
public class TxnRecMailLogRelationDao extends LegacyJpaRepository<TXNRECMAILLOGRELATION, Long> {
	protected Logger logger = Logger.getLogger(getClass());
	
	public List<TXNRECMAILLOGRELATION> findByTypeAndAdtxno(String type, String adtxno) {
		try {
			return getQuery("FROM TXNRECMAILLOGRELATION WHERE REC_TYPE = ? and ADTXNO = ? ", type, adtxno).getResultList();
		}
		catch(Exception e) {
			logger.error("findByTypeAndAdtxno Error!", e);
		}
		return null;
	}
}
