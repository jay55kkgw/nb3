package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import fstop.orm.po.TXNREJ;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Slf4j
@Repository
@Transactional
public class TxnRejDao extends LegacyJpaRepository<TXNREJ, Long> {

	public List<TXNREJ> getAll() {
		return find("FROM TXNREJ ORDER BY RID");
	}
}
