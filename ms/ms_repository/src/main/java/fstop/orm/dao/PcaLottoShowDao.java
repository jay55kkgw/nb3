package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import fstop.orm.po.PCALOTTOSHOW;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.hibernate.query.NativeQuery;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.List;

//import fstop.orm.po.PCALOTTONOTICE;


@Slf4j
@Repository
@Transactional
public class PcaLottoShowDao extends LegacyJpaRepository<PCALOTTOSHOW, Long> {
//	protected Logger logger = Logger.getLogger(getClass());	
	
	/**
	 * 依據id查詢捐款金額累積紀錄
	 * @param id
	 * @return
	 */
	@SuppressWarnings({ "unused", "unchecked" })
	public String getMoneyView(int id) {
		//List<PCALOTTOSHOW> l = find("from PCALOTTOSHOW where id = ?", id);
		String sql = "select * from pcalottoshow pcal where id = ?";
//		logger.info("select * from pcalottoshow pcal where id = ?");
		log.debug("sql = " + sql);
		Query query = getSession().createSQLQuery(sql);
		query.unwrap(NativeQuery.class).addEntity("pcal", PCALOTTOSHOW.class);
//		logger.info("id = " + id);
		log.debug("id = " + id);
		query.setParameter(0, id);
		List l =query.getResultList();
//		logger.info("List size = " + l.size());
		log.debug("List size = " + l.size());
		PCALOTTOSHOW pcal = null;
		String y = "";
		String m = "";
		String d = "";
		if(l.size()!=0){
			pcal = (PCALOTTOSHOW)l.get(0);
			
			// CGI Stored XSS
//			logger.info("pcal = " + pcal);
//			log.debug("pcal = " + pcal);
//			logger.info("pcal.getVIEWDATE() = " + pcal.getVIEWDATE());
//			log.debug("pcal.getVIEWDATE() = " + pcal.getVIEWDATE());
			
			//VIEWDATE字串範例：20170622
			if(pcal!=null && pcal.getVIEWDATE().length()==8){
				y = pcal.getVIEWDATE().substring(0,4);
				m = pcal.getVIEWDATE().substring(4,6);
				d = pcal.getVIEWDATE().substring(6,8);
			}
		}
		
		// CGI Stored XSS
//		log.debug("傳出值：'"+y+","+m+","+d+","+pcal.getMONEY()+"'");
		
		//年,月,日,捐款金額
		return y+","+m+","+d+","+pcal.getMONEY();
	}
}
