package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import fstop.orm.po.ADMMBSTATUS;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;


@Slf4j
@Repository
public class AdmMbStatusDao extends LegacyJpaRepository<ADMMBSTATUS,String> {
	
	protected Logger logger = Logger.getLogger(getClass());

}
