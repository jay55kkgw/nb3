package fstop.orm.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.util.ESAPIUtil;

import fstop.orm.po.TXNREQINFO;
import fstop.orm.po.TXNTWRECORD;
import fstop.orm.po.TXNTWSCHEDULE;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@Repository
@Transactional
public class TxnTwRecordDao extends LegacyJpaRepository<TXNTWRECORD, String> {
	protected Logger logger = Logger.getLogger(getClass());

	@Autowired
	TxnReqInfoDao txnReqInfoDao;


	@SuppressWarnings( { "unused", "unchecked" })
	public List<TXNTWRECORD> findByDateRange(String uid, String startDt, String endDt, String acn) {
		String commonQuery = "from TXNTWRECORD where DPUSERID = ? and DPTXDATE >= ? and DPTXDATE <= ? and DPTXSTATUS = '0' and DPRETXNO = '' ";
		Query query = null;
		if (acn.equals(""))
			query = getQuery(commonQuery, uid, startDt, endDt);
		else
			query = getQuery(commonQuery + "and DPWDAC = ? ", uid, startDt, endDt, acn);

		return query.getResultList();
	}

	@SuppressWarnings( { "unused", "unchecked" })
	public List<TXNTWRECORD> findSchduleResultByDateRange(String uid, String startDt, String endDt, String status) {
		List result = new ArrayList();
		//DPRETXNO = ''表示只取同一交易的最後一筆
		if(StrUtils.isEmpty(status)) { //全部
			try {
				result = findScheduleAllStatusByDateRange(uid, startDt, endDt);
			}
			finally {}
		}
		else if (status.equals("0")){ //成功
			try {
				String qhql = "from TXNTWRECORD where DPUSERID = ? and DPTXDATE >= ? and DPTXDATE <= ? and DPSCHID > 0 and DPTXSTATUS = ? and DPRETXNO = '' ";
				Query query = getQuery(qhql, uid, startDt, endDt, status);
				result = query.getResultList();
			}
			finally{}
		}
		else if (status.equals("1")){ //失敗
			try {
				String qhql = "from TXNTWRECORD where DPUSERID = ? and DPTXDATE >= ? and DPTXDATE <= ? and DPSCHID > 0 and DPTXSTATUS = ? and DPRETXNO = '' and DPRETXSTATUS <> '1' and DPRETXSTATUS <> '2' and DPRETXSTATUS <> '5'";
				Query query = getQuery(qhql, uid, startDt, endDt, status);
				result = query.getResultList();
			}
			finally{}
		}
		else if (status.equals("2")){ //處理中
			try {
				result = findSchedulePendingByDateRange(uid, startDt, endDt, status);
			}
			finally{}
		}
		return result;
	}
	
	/**
	 * 找出指定使用者某一期間內的處理中預約交易(如包括查詢日則會加上查詢日尚未執行的預約到期交易)
	 * @param uid
	 * @param startDt
	 * @param endDt
	 * @param status
	 * @return
	 */
	public List<TXNTWRECORD> findSchedulePendingByDateRange(String uid, String startDt, String endDt, String status) {
		List result = null;
		
		Date today = new Date();
		String tyyyymmdd = DateTimeUtils.format("yyyyMMdd", today);
		int iToday = Integer.parseInt(tyyyymmdd);
		int iStartDate = Integer.parseInt(startDt);
		int iEndDate = Integer.parseInt(endDt);
		
		//判斷查詢期間是否包含今日
		if (iToday >= iStartDate && iToday <= iEndDate)
		{//查詢期間有包含今日，要多查TXNTWSCHEDULE檔今日未執行的交易
			String sql = "" +
			"select * from TXNTWRECORD where DPUSERID = ? and DPTXDATE >= ? and DPTXDATE <= ? and DPSCHID > 0 and DPTXSTATUS = '1' and DPRETXNO = '' and (DPRETXSTATUS = '1' or DPRETXSTATUS = '2' or DPRETXSTATUS = '5') " + 
			"union all " + 
			getSqlForRecordDueTo(today, uid) +
			"order by DPSCHNO ";
			
			log.debug("@@@　TxnTwRecordDao.findSchedulePendingByDateRange() SQL　== " + sql);
			System.out.flush();
			
			Query query = getSession().createQuery(sql);
			query.unwrap(NativeQuery.class).addEntity("record", TXNTWRECORD.class);
			query.setParameter(0, uid);
			query.setParameter(1, startDt);
			query.setParameter(2, endDt);

			result = query.getResultList();
		}
		else
		{//查詢期間沒有包含今日，只查TXNTWRECORD檔
			String qhql = "from TXNTWRECORD where DPUSERID = ? and DPTXDATE >= ? and DPTXDATE <= ? and DPSCHID > 0 and DPTXSTATUS = '1' and DPRETXNO = '' and (DPRETXSTATUS = '1' or DPRETXSTATUS = '2' or DPRETXSTATUS = '5')";
			Query query = getQuery(qhql, uid, startDt, endDt, status);
			result = query.getResultList();
		}
		
		return result;
	}
	
	/**
	 * 找出指定使用者某一期間內的所有已執行預約交易(如包括查詢日則會加上查詢日尚未執行的預約到期交易)
	 * @param uid
	 * @param startDt
	 * @param endDt
	 * @return
	 */
	public List<TXNTWRECORD> findScheduleAllStatusByDateRange(String uid, String startDt, String endDt) {
		List result = null;
		
		Date today = new Date();
		String tyyyymmdd = DateTimeUtils.format("yyyyMMdd", today);
		int iToday = Integer.parseInt(tyyyymmdd);
		int iStartDate = Integer.parseInt(startDt);
		int iEndDate = Integer.parseInt(endDt);
		
		//判斷查詢期間是否包含今日
		if (iToday >= iStartDate && iToday <= iEndDate)
		{//查詢期間有包含今日，要多查TXNTWSCHEDULE檔今日未執行的交易
			String sql = "" +
			"select * from TXNTWRECORD where DPUSERID = ? and DPTXDATE >= ? and DPTXDATE <= ? and DPSCHID > 0 and DPRETXNO = '' " + 
			"union all " + 
			getSqlForRecordDueTo(today, uid) +
			"order by DPSCHNO ";
			
			log.debug("@@@　TxnTwRecordDao.findScheduleAllStatusByDateRange() SQL　== " + sql);
			System.out.flush();
			
			Query query = getSession().createQuery(sql);
			query.unwrap(NativeQuery.class).addEntity("record", TXNTWRECORD.class);
			query.setParameter(0, uid);
			query.setParameter(1, startDt);
			query.setParameter(2, endDt);

			result = query.getResultList();
		}
		else
		{//查詢期間沒有包含今日，只查TXNTWRECORD檔
			String qhql = "from TXNTWRECORD where DPUSERID = ? and DPTXDATE >= ? and DPTXDATE <= ? and DPSCHID > 0 and DPRETXNO = '' ";
			Query query = getQuery(qhql, uid, startDt, endDt);
			result = query.getResultList();
		}
		
		return result;
	}
	
	/**
	 * 返回指定使用者某一日到期的尚未發送之預約交易SQL字串
	 * @param currentDate
	 * @param uid
	 * @return
	 */
	private String getSqlForRecordDueTo(Date currentDate, String uid)
	{
		String due = DateTimeUtils.format("yyyyMMdd", currentDate);
		
		String sql = "" +
		"select X.DPSCHNO as ADTXNO, X.ADOPID, X.DPUSERID, '" + due + "' as DPTXDATE, '000000' as DPTXTIME, X.DPWDAC, X.DPSVBH, X.DPSVAC, X.DPTXAMT, X.DPTXMEMO, X.DPTXMAILS, X.DPTXMAILMEMO, X.DPSCHID, '0' as DPEFEE, '' as PCSEQ, '' as DPTXNO, X.DPTXCODE, 1 as DPTITAINFO, '' as DPTOTAINFO, '0' as DPREMAIL, '' as DPTXSTATUS, '' as DPEXCODE, '' as DPRETXNO, '0' as DPRETXSTATUS, '" + due + "' as LASTDATE, '000000' as LASTTIME, X.DPSCHNO " +
		"from ( " +
		" SELECT distinct sch.* FROM                                                                                                  " +
		" 	TXNTWSCHEDULE sch , (" + TxnTwScheduleDao.getSqlForScheduleExcecNextDate(currentDate, uid)+ ") nextexec " +
		" 	where		                                                                                                         " +
		" 			nextexec.DPSCHID = sch.DPSCHID and " +
		"           nextexec.nextexecdate is not null and nextexec.nextexecdate = '" + due + "' " +		
		"           and ( ( DPTXSTATUS = '0' and  LASTDATE <= '" + due + "' )   " +
		"					 or ( DPTXSTATUS = '1' and  LASTDATE = '" + due + "'))  " + //不含狀態2是因有可能Record檔已有執行過此筆的資料，避免重複
		") X ";
		
		return sql;
	}

	/**
	 * 找出某一日的轉帳到期, 第二次轉帳 => 來源為預約檔(DPTXSTATUS = '1' and LASTDATE = 今天)
	 * @param currentDate
	 * @return
	 */
	public List<TXNTWSCHEDULE> findScheduleDueToReSend_S(Date currentDate) {
		String due = DateTimeUtils.format("yyyyMMdd", currentDate);
		Calendar cal = Calendar.getInstance();
		cal.setTime(currentDate);
		int yyyy = cal.get(Calendar.YEAR);
		int mm = cal.get(Calendar.MONTH) + 1;
		int dd = cal.get(Calendar.DATE);

		String sql = "" +
		" SELECT distinct {sch.*} FROM                                                                                                  " +
		" 	TXNTWSCHEDULE sch , (" + TxnTwScheduleDao.getSqlForScheduleExcecNextDate(currentDate)+ ") nextexec " +
		" 	where		                                                                                                         " +
		" 			nextexec.DPSCHID = sch.DPSCHID and " +
		"           nextexec.nextexecdate is not null and nextexec.nextexecdate = ? " +		
		"           and  (sch.DPTXSTATUS = '1' and  sch.LASTDATE = ?) and sch.LOGINTYPE <> ? ";		
		
		
		log.debug("@@@　TxnTwRecordDao.findScheduleDueToReSend() SQL　== " + sql);
		System.out.flush();
		
		
		Query query = getSession().createQuery(sql);
		query.unwrap(NativeQuery.class).addEntity("sch", TXNTWSCHEDULE.class);
		
		query.setParameter(0, due);
		query.setParameter(1, due);
		query.setParameter(2, "MB");

		return query.getResultList();
	}

	/**
	 * 找出某一日的轉帳到期, 第二次轉帳 => 來源為轉帳紀錄檔(DPTXSTATUS = '0' or DPTXSTATUS = '5')
	 * @param currentDate
	 * @return
	 */
	public List<TXNTWRECORD> findScheduleDueToReSend_R(Date currentDate) {
		String due = DateTimeUtils.format("yyyyMMdd", currentDate);
		Calendar cal = Calendar.getInstance();
		cal.setTime(currentDate);
		int yyyy = cal.get(Calendar.YEAR);
		int mm = cal.get(Calendar.MONTH) + 1;
		int dd = cal.get(Calendar.DATE);

		String sql = "" +
		" SELECT distinct {rec.*} FROM                                                                                                  " +
		" 	TXNTWSCHEDULE sch , (" + TxnTwScheduleDao.getSqlForScheduleExcecNextDate(currentDate)+ ") nextexec, TXNTWRECORD rec  " +
		" 	where		                                                                                                         " +
		" 			nextexec.DPSCHID = rec.DPSCHID and " +
		"           nextexec.nextexecdate is not null and nextexec.nextexecdate = ?                         " +		
		"           and (  (sch.DPTXSTATUS = '0' or sch.DPTXSTATUS = '5') and  sch.LASTDATE = ? and       " +
		"                  rec.DPRETXSTATUS = '5' and rec.LASTDATE = ? and rec.DPRETXNO = '' " +		
		"               ) and sch.LOGINTYPE <> ? " ;
		
		
		
		log.debug("@@@　TxnTwRecordDao.findScheduleDueToReSend() SQL　== " + sql);
		System.out.flush();
		
		
		Query query = getSession().createQuery(sql);
		query.unwrap(NativeQuery.class).addEntity("rec", TXNTWRECORD.class);

		query.setParameter(0, due);
		query.setParameter(1, due);
		query.setParameter(2, due);
		query.setParameter(3, "MB");

		return query.getResultList();
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void writeTxnRecord(TXNREQINFO info ,TXNTWRECORD record) {
		if(record != null)
			logger.debug(ESAPIUtil.vaildLog("WRITE NEW TXNTWRECORD ADTXNO:" + record.getADTXNO()));

//		BOLifeMoniter.setValue("__TXNRECORD", record);
		if(info != null) {
//			BOLifeMoniter.setValue("__TITAINFO", info);
			txnReqInfoDao.save(info);
//			record.setDPTITAINFO(info.getREQID());
		}
		save( record);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void rewriteTxnRecord(TXNTWRECORD record) {
//		BOLifeMoniter.setValue("__TXNRECORD", record);
		save( record);
	}

	public List<TXNTWRECORD> findResendStatus(String lastdate, String[] retxstatus) {
		String restatusCondition = "";
		if(retxstatus == null || retxstatus.length == 0)
			return new ArrayList();
		else if(retxstatus.length == 1)  {
			if("*".equals(retxstatus[0]))
				restatusCondition = "DPRETXSTATUS <> '0' ";
			else
				restatusCondition = "DPRETXSTATUS = '" + retxstatus[0] + "' ";
		}
		else if(retxstatus.length > 1)
			restatusCondition = "DPRETXSTATUS in ('" + StrUtils.implode("','", retxstatus) + "') ";


		Query query = getQuery("FROM TXNTWRECORD " +
				" WHERE DPSCHID > 0 and DPTXSTATUS = '1' and  " +  restatusCondition +
				" and LASTDATE = ? " +      //and DPRETXNO = '' " +
				" ORDER BY LASTDATE DESC, LASTTIME DESC ", lastdate);

		return query.getResultList();
	}

	/**
	 * 找到重送之後寫的那筆 TXNTWRECORD
	 * @param adtxno
	 * @return
	 */
	public TXNTWRECORD findResendRecord(String adtxno) {
		return this.findUniqueBy(TXNTWRECORD.class,"DPRETXNO", adtxno);
	}

	public void updateStatus(TXNTWRECORD record, String status) {
		logger.debug("update TwRecord status: " + status);
		try {
			Date d = new Date();
			record.setLASTDATE(DateTimeUtils.format("yyyyMMdd", d));
			record.setLASTTIME(DateTimeUtils.format("HHmmss", d));
//			record.setDPRETXSTATUS(status);

			save(record);
		}
		catch(Exception e) {

		}
		finally {

		}
	}
	/**
	 * 取得某一日的預約交易結果總筆數
	 * @param currentDate
	 * @return
	 */
	public int countTWRecordTot(Date currentDate) {
		String due = DateTimeUtils.format("yyyyMMdd", currentDate);

		String sql = "" +
		"select rec.* from TxnTwRecord rec where rec.DPSCHID>0 and rec.DPTXDATE= ? ";

		Query query = getSession().createQuery(sql);
		query.unwrap(NativeQuery.class).addEntity("rec", TXNTWRECORD.class);
		query.setParameter(0, due);
		return query.getResultList().size();
	}
	/**
	 * 取得某一日的預約交易結果成功筆數
	 * @param currentDate
	 * @return
	 */
	public int countTWRecordSuc(Date currentDate) {
		String due = DateTimeUtils.format("yyyyMMdd", currentDate);

		String sql = "" +
		"select rec.* from TxnTwRecord rec where rec.DPSCHID>0 and rec.DPTXDATE= ?  and rec.DPEXCODE=''";

		Query query = getSession().createQuery(sql);
		query.unwrap(NativeQuery.class).addEntity("rec", TXNTWRECORD.class);
		query.setParameter(0, due);
		return query.getResultList().size();
	}
	/**
	 * 取得某一日的預約交易結果系統面失敗筆數
	 * @param currentDate
	 * @return
	 */
	public int countTWRecordFailS(Date currentDate) {
		String due = DateTimeUtils.format("yyyyMMdd", currentDate);

		String sql = "" +
		"select rec.* from TxnTwRecord rec where rec.DPSCHID>0 and rec.DPTXDATE= ?  and rec.DPEXCODE like 'Z%'";

		Query query = getSession().createQuery(sql);
		query.unwrap(NativeQuery.class).addEntity("rec", TXNTWRECORD.class);
		query.setParameter(0, due);
		return query.getResultList().size();
	}
	/**
	 * 取得某一日的預約交易結果客戶面操作失敗筆數
	 * @param currentDate
	 * @return
	 */
	public int countTWRecordFailC(Date currentDate) {
		String due = DateTimeUtils.format("yyyyMMdd", currentDate);

		String sql = "" +
		"select rec.* from TxnTwRecord rec where rec.DPSCHID>0 and rec.DPTXDATE= ?  and rec.DPEXCODE like 'E%'";

		Query query = getSession().createQuery(sql);
		query.unwrap(NativeQuery.class).addEntity("rec", TXNTWRECORD.class);
		query.setParameter(0, due);
		return query.getResultList().size();
	}			
}
