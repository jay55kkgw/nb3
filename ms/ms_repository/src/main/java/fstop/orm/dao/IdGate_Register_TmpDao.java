package fstop.orm.dao;

import java.io.Serializable;
import java.util.List;
import java.util.function.BiFunction;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;
import com.netbank.domain.orm.core.SessionAdapter;

import fstop.orm.po.IDGATE_REGISTER_TMP;
import fstop.orm.po.QUICKLOGINMAPPING;
import fstop.orm.po.QUICKLOGINMAPPING_PK;
import lombok.extern.slf4j.Slf4j;

@Transactional(
		transactionManager="nb3transactionManager",
        rollbackFor = {Throwable.class}
)
@Repository
@Slf4j
public class IdGate_Register_TmpDao extends LegacyJpaRepository<IDGATE_REGISTER_TMP,Serializable>{@Override
	
	
	
	
	
	
	
	
	
	
	public EntityManager getEntityManager() {
		// TODO Auto-generated method stub
		return super.getEntityManager();
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return super.count();
	}

	@Override
	public List<IDGATE_REGISTER_TMP> findAll() {
		// TODO Auto-generated method stub
		return super.findAll();
	}

	@Override
	public IDGATE_REGISTER_TMP findById(Serializable id) {
		// TODO Auto-generated method stub
		return super.findById(id);
	}

	@Override
	public void save(IDGATE_REGISTER_TMP entity) {
		// TODO Auto-generated method stub
		super.save(entity);
	}

	@Override
	public IDGATE_REGISTER_TMP update(IDGATE_REGISTER_TMP entity) {
		// TODO Auto-generated method stub
		return super.update(entity);
	}

	@Override
	public void delete(IDGATE_REGISTER_TMP entity) {
		// TODO Auto-generated method stub
		super.delete(entity);
	}

	@Override
	public Query getQuery(String hql, Object... values) {
		// TODO Auto-generated method stub
		return super.getQuery(hql, values);
	}

	@Override
	public Query getCriteriaQuery(Class<IDGATE_REGISTER_TMP> entityClass,
			BiFunction<Root, CriteriaBuilder, Predicate>... criteriaFunction) {
		// TODO Auto-generated method stub
		return super.getCriteriaQuery(entityClass, criteriaFunction);
	}

	@Override
	public List find(String hql, Object... values) {
		// TODO Auto-generated method stub
		return super.find(hql, values);
	}

	@Override
	public List<IDGATE_REGISTER_TMP> findBy(Class<IDGATE_REGISTER_TMP> entityClass, String name, Object value) {
		// TODO Auto-generated method stub
		return super.findBy(entityClass, name, value);
	}

	@Override
	public IDGATE_REGISTER_TMP findUniqueBy(Class<IDGATE_REGISTER_TMP> entityClass, String name, Object value) {
		// TODO Auto-generated method stub
		return super.findUniqueBy(entityClass, name, value);
	}

	@Override
	public List<IDGATE_REGISTER_TMP> findByLike(Class<IDGATE_REGISTER_TMP> entityClass, String name, String value) {
		// TODO Auto-generated method stub
		return super.findByLike(entityClass, name, value);
	}

	@Override
	public SessionAdapter getSession() {
		// TODO Auto-generated method stub
		return super.getSession();
	}

	@Override
	public Page pagedQuery(String hql, int pageNo, int pageSize, Object... values) {
		// TODO Auto-generated method stub
		return super.pagedQuery(hql, pageNo, pageSize, values);
	}

	
	
	
	
}