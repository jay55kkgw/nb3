package fstop.orm.dao;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.query.NativeQuery;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.TXNFXSCHEDULE;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;

import javax.persistence.Query;

@Slf4j
@Repository
@Transactional
public class TxnFxScheduleDao extends LegacyJpaRepository<TXNFXSCHEDULE, String> {

	@SuppressWarnings( { "unused", "unchecked" })
	public List<TXNFXSCHEDULE> findValidSchedule(String uid) {
		List result = new ArrayList();
		Query query = getQuery("from TXNFXSCHEDULE where  FXUSERID = ?  and FXTXSTATUS = '0' order by FXSCHID ", uid);  //0表未執行或未全部執行完

		return query.getResultList();
	}

	public List<TXNFXSCHEDULE> findValidSchedule1(String uid) {
		Date d = new Date();	  
		String today = DateTimeUtils.format("yyyyMMdd", d);				
		List result = new ArrayList();
		Query query = getQuery("from TXNFXSCHEDULE where  FXUSERID = ?  and (FXTXSTATUS = '0' or FXTXSTATUS = '3') and FXTDATE>= ? order by FXTXSTATUS,FXSCHID ", uid,today);  //0表未執行或未全部執行完

		return query.getResultList();
	}
	
	public static String getSqlForScheduleExcecNextDate(Date currentDate) {
		return getSqlForScheduleExcecNextDate(currentDate, null);
	}

	public static String getSqlForScheduleExcecNextDate(Date currentDate, String uid) {
		Calendar calendar = Calendar.getInstance();
		Date today = currentDate;

		String tyyyymmdd = DateTimeUtils.format("yyyyMMdd", today);
		String tyy = DateTimeUtils.format("yyyy", today);
		String tmm = DateTimeUtils.format("MM", today);
		String tdd = DateTimeUtils.format("dd", today);

		calendar.setTime(currentDate);
		calendar.add(Calendar.YEAR, 1);
		Date nextyear = calendar.getTime();
		String nextyy = DateTimeUtils.format("yyyy", nextyear);

		calendar.add(Calendar.YEAR, -2);
		Date preyear = calendar.getTime();
		String preyy = DateTimeUtils.format("yyyy", preyear);



		String crlf = "\n";
		String sql = "" +
		"SELECT DISTINCT SOUT.FXSCHID, RTRIM(CHAR(MIN(R.YYMMDD))) AS NEXTEXECDATE FROM (                                                                                                                " + crlf +
		"        SELECT S.FXSCHID, S.FXTDATE,                                                                                                                                                                          " + crlf +
		"        		CASE WHEN (S.FXFDATE = S.FXTDATE AND R.FXTXSTATUS = '0') OR (S.FXFDATE = S.FXTDATE AND R.FXTXSTATUS = '1' AND R.FXRETXSTATUS !='5')                                                                                                                                                     " + crlf +
		"						THEN S.NEXTMAXFXFDATE																							" +
		"					 WHEN S.FXFDATE = S.FXTDATE																				" +
		"						THEN S.FXTDATE	                                                                                                                                                                 " + crlf +
		// ???????? 修改預約轉及時且為假日，跨月會抓到下下個月約定日
		// 20150331 測試時發現有問題所以從84行挪到此
		"					 WHEN INT(S.LASTDATE) >= INT(S.FXFDATE)                                                                                                                                           " + crlf +
		"					  AND (S.LASTDATE != '"+tyyyymmdd+"'                                                                                                                                          " + crlf +
		"					   OR (S.LASTDATE = '"+tyyyymmdd+"' AND R.LASTDATE = '"+tyyyymmdd+"' AND R.FXTXSTATUS = '0')                                                                                                                " + crlf +
		"					   OR (S.LASTDATE = '"+tyyyymmdd+"' AND R.LASTDATE = '"+tyyyymmdd+"' AND R.FXTXSTATUS = '1' AND R.FXRETXSTATUS !='5'))                                                                                      " + crlf +
		"					  AND S.LASTMM = '" + tmm + "' AND INT(S.FXPERMTDATE) > INT(S.LASTDD)                                                                                                                               " + crlf +
		"                  		THEN S.LASTYY || S.LASTMM || SUBSTR(DIGITS(INT(S.FXPERMTDATE)), 9)                                                                                               " + crlf +
		// 20150331 END
		// ???????? END
		"					 WHEN INT(S.LASTDATE) >= INT(S.FXFDATE)                                                                                                                                           " + crlf +
		"					  AND (S.LASTDATE != '"+tyyyymmdd+"'                                                                                                                                          " + crlf +
		"					   OR (S.LASTDATE = '"+tyyyymmdd+"' AND R.LASTDATE = '"+tyyyymmdd+"' AND R.FXTXSTATUS = '0')                                                                                                               " + crlf +
		"					   OR (S.LASTDATE = '"+tyyyymmdd+"' AND R.LASTDATE = '"+tyyyymmdd+"' AND R.FXTXSTATUS = '1' AND R.FXRETXSTATUS !='5'))                                                                                     " + crlf +
		"					  AND INT(S.FXPERMTDATE) > INT(S.NEXTMAXLASTDD)                                                                                                                               " + crlf +
		"						THEN S.NEXTMAXLASTDATE                                                                                                                                                         " + crlf +
		"					 WHEN INT(S.LASTDATE) >= INT(S.FXFDATE)                                                                                                                                           " + crlf +
		"					  AND (S.LASTDATE != '"+tyyyymmdd+"'                                                                                                                                          " + crlf +
		"					   OR (S.LASTDATE = '"+tyyyymmdd+"' AND R.LASTDATE = '"+tyyyymmdd+"' AND R.FXTXSTATUS = '0')                                                                                                                " + crlf +
		"					   OR (S.LASTDATE = '"+tyyyymmdd+"' AND R.LASTDATE = '"+tyyyymmdd+"' AND R.FXTXSTATUS = '1' AND R.FXRETXSTATUS !='5'))                                                                                      " + crlf +
		"					  AND INT(S.FXPERMTDATE) <= INT(S.NEXTMAXLASTDD)                                                                                                                              " + crlf +
		"                  		THEN S.NEXTMAXLASTYY || S.NEXTMAXLASTMM || SUBSTR(DIGITS(INT(S.FXPERMTDATE)), 9)                                                                                               " + crlf +
		"					 WHEN INT(S.LASTDATE) >= INT(S.FXFDATE) AND S.LASTDATE = '"+tyyyymmdd+"'                                                                                                          " + crlf +
		"						THEN S.LASTDATE                                                                                                                                                                " + crlf +
		"                  	 WHEN INT(S.LASTDATE) < INT(S.FXFDATE)  AND INT(S.MAXFXFYY || S.MAXFXFMM || SUBSTR(DIGITS(INT(S.FXPERMTDATE)), 9)) < INT(S.FXFDATE) AND INT(S.FXPERMTDATE) > INT(S.NEXTMAXFXFDD)  " + crlf +
		"                  		THEN S.NEXTMAXFXFDATE                                                                                                                                                          " + crlf +
		"                  	 WHEN INT(S.LASTDATE) < INT(S.FXFDATE)  AND INT(S.MAXFXFYY || S.MAXFXFMM || SUBSTR(DIGITS(INT(S.FXPERMTDATE)), 9)) < INT(S.FXFDATE) AND INT(S.FXPERMTDATE) <= INT(S.NEXTMAXFXFDD) " + crlf +
		"                  		THEN S.NEXTMAXFXFYY || S.NEXTMAXFXFMM || SUBSTR(DIGITS(INT(S.FXPERMTDATE)), 9)                                                                                                 " + crlf +
		"                  	 WHEN INT(S.LASTDATE) < INT(S.FXFDATE) AND INT(S.MAXFXFYY || S.MAXFXFMM || SUBSTR(DIGITS(INT(S.FXPERMTDATE)), 9)) >= INT(S.FXFDATE) AND INT(S.FXPERMTDATE) > INT(S.MAXFXFDD)      " + crlf +
		"                  		THEN S.MAXFXFDATE                                                                                                                                                              " + crlf +
		"                  	 WHEN INT(S.LASTDATE) < INT(S.FXFDATE) AND INT(S.MAXFXFYY || S.MAXFXFMM || SUBSTR(DIGITS(INT(S.FXPERMTDATE)), 9)) >= INT(S.FXFDATE) AND INT(S.FXPERMTDATE) <= INT(S.MAXFXFDD)     " + crlf +
		"                  		THEN S.MAXFXFYY || S.MAXFXFMM || SUBSTR(DIGITS(INT(S.FXPERMTDATE)), 9)                                                                                                         " + crlf +
		"                  	 ELSE '' END AS NEXTEXECDATE                                                                                                                                                     " + crlf +
		"        FROM (SELECT FXSCHID, FXTDATE, FXPERMTDATE, FXUSERID, FXTXSTATUS, LASTTIME,                                                                                                                 " + crlf +
		"        			  LASTDATE,                                                                                                                                                                       " + crlf +
        "                     SUBSTR(LASTDATE,1,4) AS LASTYY,                                                                                                                                                 "  + crlf +
        "                     SUBSTR(LASTDATE,5,2) AS LASTMM,                                                                                                                                                 "  + crlf +
        "                     SUBSTR(LASTDATE,7,2) AS LASTDD,                                                                                                                                                 "  + crlf +
		"        			  REPLACE(CHAR(DATE(SUBSTR(LASTDATE,1,4)||'-'||SUBSTR(LASTDATE,5,2)||'-'||'01') + 2 MONTH - 1 DAY),'-','') AS NEXTMAXLASTDATE,                                                    " + crlf +
		"       			  SUBSTR(REPLACE(CHAR(DATE(SUBSTR(LASTDATE,1,4)||'-'||SUBSTR(LASTDATE,5,2)||'-'||'01') + 2 MONTH - 1 DAY),'-',''),1,4) AS NEXTMAXLASTYY,                                        " + crlf +
		"       			  SUBSTR(REPLACE(CHAR(DATE(SUBSTR(LASTDATE,1,4)||'-'||SUBSTR(LASTDATE,5,2)||'-'||'01') + 2 MONTH - 1 DAY),'-',''),5,2) AS NEXTMAXLASTMM,                                        " + crlf +
		"       			  SUBSTR(REPLACE(CHAR(DATE(SUBSTR(LASTDATE,1,4)||'-'||SUBSTR(LASTDATE,5,2)||'-'||'01') + 2 MONTH - 1 DAY),'-',''),7,2) AS NEXTMAXLASTDD,                                        " + crlf +
		"        			  FXFDATE,                                                                                                                                                                        " + crlf +
		"        			  REPLACE(CHAR(DATE(SUBSTR(FXFDATE,1,4)||'-'||SUBSTR(FXFDATE,5,2)||'-'||'01') + 1 MONTH - 1 DAY),'-','') AS MAXFXFDATE,                                                           " + crlf +
		"       			  SUBSTR(REPLACE(CHAR(DATE(SUBSTR(FXFDATE,1,4)||'-'||SUBSTR(FXFDATE,5,2)||'-'||'01') + 1 MONTH - 1 DAY),'-',''),1,4) AS MAXFXFYY,                                               " + crlf +
		"       			  SUBSTR(REPLACE(CHAR(DATE(SUBSTR(FXFDATE,1,4)||'-'||SUBSTR(FXFDATE,5,2)||'-'||'01') + 1 MONTH - 1 DAY),'-',''),5,2) AS MAXFXFMM,                                               " + crlf +
		"       			  SUBSTR(REPLACE(CHAR(DATE(SUBSTR(FXFDATE,1,4)||'-'||SUBSTR(FXFDATE,5,2)||'-'||'01') + 1 MONTH - 1 DAY),'-',''),7,2) AS MAXFXFDD,                                               " + crlf +
		"       			  REPLACE(CHAR(DATE(SUBSTR(FXFDATE,1,4)||'-'||SUBSTR(FXFDATE,5,2)||'-'||'01') + 2 MONTH - 1 DAY),'-','') AS NEXTMAXFXFDATE,                                                     " + crlf +
		"       			  SUBSTR(REPLACE(CHAR(DATE(SUBSTR(FXFDATE,1,4)||'-'||SUBSTR(FXFDATE,5,2)||'-'||'01') + 2 MONTH - 1 DAY),'-',''),1,4) AS NEXTMAXFXFYY,                                           " + crlf +
		"       			  SUBSTR(REPLACE(CHAR(DATE(SUBSTR(FXFDATE,1,4)||'-'||SUBSTR(FXFDATE,5,2)||'-'||'01') + 2 MONTH - 1 DAY),'-',''),5,2) AS NEXTMAXFXFMM,                                           " + crlf +
		"       			  SUBSTR(REPLACE(CHAR(DATE(SUBSTR(FXFDATE,1,4)||'-'||SUBSTR(FXFDATE,5,2)||'-'||'01') + 2 MONTH - 1 DAY),'-',''),7,2) AS NEXTMAXFXFDD,                                           " + crlf +
		"                     LOGINTYPE                                                                                                                                                                     " + crlf +
		"        		FROM TXNFXSCHEDULE) S                                                                                                                                                                  " + crlf +
		"        				LEFT JOIN                                                                                                                                                                            " + crlf +
		"        	  (SELECT T1.FXSCHID ,T1.FXTXSTATUS, T1.FXRETXSTATUS, T1.LASTDATE                                                                                                                                     " + crlf +
		"        	   FROM TXNFXRECORD T1,                                                                                                                                                                   " + crlf +
		"        		   (SELECT SUBSTR(CHAR(MAX(BIGINT(LASTDATE||LASTTIME))),1,8) AS LASTDATE, SUBSTR(CHAR(MAX(BIGINT(LASTDATE||LASTTIME))),9) AS LASTTIME, FXSCHID                                       " + crlf +
		"				    FROM TXNFXRECORD GROUP BY FXSCHID) T2                                                                                                                                            " + crlf +
		"			   WHERE T1.FXSCHID = T2.FXSCHID AND T1.LASTDATE = T2.LASTDATE AND T1.LASTTIME = T2.LASTTIME AND T1.FXRETXSTATUS <> '3' AND T1.FXRETXSTATUS <> '4' )R                                                       " + crlf +
		"					  	ON S.FXSCHID = R.FXSCHID                                                                                                                                                           " + crlf +
		"        WHERE " + (StrUtils.isEmpty(uid) ? "" : " S.FXUSERID = '" + uid + "' AND ")+ "(S.FXTXSTATUS <> '3' AND S.FXTXSTATUS <> '4' AND S.LOGINTYPE <> 'MB' )                                                                   " + crlf +
		"        ) SOUT ,                                                                                                                                                                                    " + crlf +
		"        (SELECT R.* FROM SYSDATERANGE R LEFT JOIN ADMHOLIDAY H ON R.YYMMDD = INT(H.ADHOLIDAYID) WHERE  R.YY IN ('"+preyy+"','"+tyy+"','"+nextyy+"') AND H.ADHOLIDAYID IS NULL) R                    " + crlf +
		" WHERE R.YYMMDD >= INT(SOUT.NEXTEXECDATE) AND SOUT.NEXTEXECDATE <= SOUT.FXTDATE                                                                                                                                                    " + crlf +
		" GROUP BY SOUT.FXSCHID";

		return sql;

	}

	public Map<String, String> findNextDate(String uid) {
		DecimalFormat fmt = new DecimalFormat("#0");

		log.debug("@@@ before TxnFxScheduleDao.findNextDate() getSqlForScheduleExcecNextDate ");

		String sql = getSqlForScheduleExcecNextDate(new Date(), uid) + " order by FXSCHID   ";

		log.debug("@@@ after TxnFxScheduleDao.findNextDate() getSqlForScheduleExcecNextDate ");

		Map<String, String> result = new HashMap();
		Query query = getSession().createSQLQuery(sql);
		query.unwrap(NativeQuery.class)
			.addScalar( "FXSCHID", LongType.INSTANCE)
			.addScalar( "nextexecdate", StringType.INSTANCE);

		List qresult = query.getResultList();


		log.debug("@@@ TxnFxScheduleDao.findNextDate() qresult.size() == "+qresult.size());



		for(Object o : qresult) {
			Object[] oary = (Object[])o;
			result.put(fmt.format(oary[0]), (String)oary[1]);
		}
		return result;
	}



	/**
	 * 找出某一日的轉帳到期, 第一次轉帳
	 * @param currentDate
	 * @return
	 */

	public List<TXNFXSCHEDULE> findScheduleDueTo(Date currentDate) {

		log.debug("@@@ TxnFxScheduleDao.findScheduleDueTo() 1 ");


		String due = DateTimeUtils.format("yyyyMMdd", currentDate);
		Calendar cal = Calendar.getInstance();
		cal.setTime(currentDate);
		int yyyy = cal.get(Calendar.YEAR);
		int mm = cal.get(Calendar.MONTH) + 1;
		int dd = cal.get(Calendar.DATE);

		String sql = "" +
		" SELECT distinct {sch.*} FROM   " +
		" 	TXNFXSCHEDULE sch , (" + getSqlForScheduleExcecNextDate(new Date()) + ") nextexec " +
		" 	                                                                   " +
		" 	where	sch.FXSCHID = nextexec.FXSCHID   and	                                                                                                         " +
		" 			nextexec.nextexecdate is not null and nextexec.nextexecdate = ?                         " +
		"           and ( ( FXTXSTATUS = '0' and  LASTDATE <= ? )   " +
		"					 or ( FXTXSTATUS = '1' and  LASTDATE = ?)) and LOGINTYPE <> ? " ;
		Query query = getSession().createSQLQuery(sql);
		query.unwrap(NativeQuery.class).addEntity("sch", TXNFXSCHEDULE.class);

		query.setParameter(0, due);
		query.setParameter(1, due);
		query.setParameter(2, due);
		query.setParameter(3, "MB");

		return query.getResultList();
	}
	
	/**
	 * 取得某一日的應執行且尚未執行的預約筆數(不含一扣失敗可重送交易)
	 * @param currentDate
	 * @return
	 */
	public int countScheduleDueTo(Date currentDate) {

		log.debug("@@@ TxnFxScheduleDao.findScheduleDueTo() 1 ");


		String due = DateTimeUtils.format("yyyyMMdd", currentDate);
		Calendar cal = Calendar.getInstance();
		cal.setTime(currentDate);
		int yyyy = cal.get(Calendar.YEAR);
		int mm = cal.get(Calendar.MONTH) + 1;
		int dd = cal.get(Calendar.DATE);

		String sql = "" +
		" SELECT distinct {sch.*} FROM   " +
		" 	TXNFXSCHEDULE sch , (" + getSqlForScheduleExcecNextDate(new Date()) + ") nextexec " +
		" 	                                                                   " +
		" 	where	sch.FXSCHID = nextexec.FXSCHID   and	                                                                                                         " +
		" 			nextexec.nextexecdate is not null and nextexec.nextexecdate = ?                         " +
		"           and not exists (select 1 from TXNFXRECORD record where record.FXSCHID = sch.FXSCHID)  " +
		"           and ( ( sch.FXTXSTATUS = '0' and  sch.LASTDATE <= ? )   " +
		"					 or ( sch.FXTXSTATUS = '1' and  sch.LASTDATE = ?))  " ;
		Query query = getSession().createSQLQuery(sql);
		query.unwrap(NativeQuery.class).addEntity("sch", TXNFXSCHEDULE.class);

		query.setParameter(0, due);
		query.setParameter(1, due);
		query.setParameter(2, due);

		return query.getResultList().size();
	}

	public void updateStatus(TXNFXSCHEDULE schedule, String status) {
		try {
			Date d = new Date();
			schedule.setLASTDATE(DateTimeUtils.format("yyyyMMdd", d));
			schedule.setLASTTIME(DateTimeUtils.format("HHmmss", d));
			schedule.setFXTXSTATUS(status);

			save(schedule);
		}
		catch(Exception e) {

		}
		finally {

		}
	}

	public List<TXNFXSCHEDULE> findScheduleBySchID (Vector v) {

		String sql = " SELECT sch.* FROM TXNFXSCHEDULE sch " ;

		if(!v.isEmpty()){
			sql += "WHERE FXSCHID IN (";
			for(int i = 0 ; i < v.size(); i++){
				sql += " " + (String)v.get(i) + " ";
				if(i != (v.size() - 1)){
					sql +=", ";
				}
			}
			sql += " )";
		}else {
			sql += "WHERE 1 != 1";
		}

		//預設以使用者預約編號排序
		sql += " ORDER BY FXSCHNO ";

		Query query = getSession().createSQLQuery(sql);
		query.unwrap(NativeQuery.class).addEntity("sch", TXNFXSCHEDULE.class);

		return query.getResultList();
	}

	public List<TXNFXSCHEDULE> findTxStatus(String lastdate, String[] retxstatus, String logintype) {
		String restatusCondition = "";
		if(retxstatus == null || retxstatus.length == 0)
			return new ArrayList();
		else if(retxstatus.length == 1)  {
			restatusCondition = "FXTXSTATUS = '" + retxstatus[0] + "' ";
		}
		else if(retxstatus.length > 1)
			restatusCondition = "FXTXSTATUS in ('" + StrUtils.implode("','", retxstatus) + "') ";


		Query query = getQuery("FROM TXNFXSCHEDULE WHERE " + restatusCondition +
				" and LASTDATE = ? and LOGINTYPE in ("+logintype+") ORDER BY LASTDATE DESC, LASTTIME DESC ", lastdate);

		return query.getResultList();
	}

	/**
	 * 根據 uid 跟 轉入帳號 查出下一執行日大於今日所有預約轉帳資料
	 * @return List<TXNFXSCHEDULE>
	 */
	public List<TXNFXSCHEDULE> findScheduleBySVAC(String uid,String cycc, String svac) {

		String due = DateTimeUtils.format("yyyyMMdd", new Date());

		String sql = "" +
		" SELECT distinct {sch.*} FROM " +
		" TXNFXSCHEDULE sch , (" + getSqlForScheduleExcecNextDate(new Date(), uid) + ") nextexec " +
		" WHERE	sch.FXSCHID = nextexec.FXSCHID " +
		"   and nextexec.nextexecdate is not null " +
		"   and sch.FXSVAC = ? and sch.FXSVCURR = ? " +
		"   and nextexec.nextexecdate > ? " ;

		Query query = getSession().createSQLQuery(sql);
		query.unwrap(NativeQuery.class).addEntity("sch", TXNFXSCHEDULE.class);

		query.setParameter(0, svac);
		query.setParameter(1, cycc);
		query.setParameter(2, due);

		return query.getResultList();
	}
	
	/**
	 * 根據 uid 跟 轉入帳號 查出下一執行日等於今日所有預約轉帳資料
	 * @return List<TXNFXSCHEDULE>
	 */
	public List<TXNFXSCHEDULE> findScheduleTodayBySVAC(String uid,String cycc, String svac) {

		String due = DateTimeUtils.format("yyyyMMdd", new Date());

		String sql = "" +
		" SELECT distinct {sch.*} FROM " +
		" TXNFXSCHEDULE sch , (" + getSqlForScheduleExcecNextDate(new Date(), uid) + ") nextexec " +
		" WHERE	sch.FXSCHID = nextexec.FXSCHID " +
		"   and nextexec.nextexecdate is not null " +
		"   and sch.FXSVAC = ? and sch.FXSVCURR = ?" +
		"   and nextexec.nextexecdate = ? " ;//跟上面的方法只有這一行不一樣，>變成=

		Query query = getSession().createSQLQuery(sql);
		query.unwrap(NativeQuery.class).addEntity("sch", TXNFXSCHEDULE.class);

		query.setParameter(0, svac);
		query.setParameter(1, cycc);
		query.setParameter(2, due);

		return query.getResultList();
	}
	/**
	 * 找出某一日的外幣週期性轉帳到期
	 * @param currentDate
	 * @return
	 */
	public List<TXNFXSCHEDULE> findScheduleDueToEND(Date currentDate) {

		String due = DateTimeUtils.format("yyyyMMdd", currentDate);

		String sql = "" +
		" SELECT distinct {sch.*} FROM   " +
		" 	TXNFXSCHEDULE sch " +
		" 	where	sch.FXTDATE = ? " +
		"           and sch.FXPERMTDATE<>'' and sch.FXTXSTATUS not in ('3','4') and  sch.LASTDATE <= ? " ;
		Query query = getSession().createSQLQuery(sql);
		query.unwrap(NativeQuery.class).addEntity("sch", TXNFXSCHEDULE.class);
		query.setParameter(0, due);
		query.setParameter(1, due);

		return query.getResultList();
	}
	
	/**
	 * 根據 外幣預約ID FXSCHID 來更新 交易執行狀態 FXTXSTATUS
	 * @param fxschid
	 * @param fxtxstatus
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateStatusByFxSchID(String fxschid, String fxtxstatus){

		Query add1=this.getSession().createSQLQuery("UPDATE TXNFXSCHEDULE SET FXTXSTATUS = ? WHERE FXSCHID = ?");

		add1.setParameter(0, fxtxstatus);
		add1.setParameter(1, fxschid);

		int r2 = add1.executeUpdate();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateStatusByFxUserID(String fxuserid, String fxtxstatus){


		Query add1=this.getSession().createSQLQuery("UPDATE TXNFXSCHEDULE SET FXTXSTATUS = ? WHERE FXUSERID = ? AND FXTXSTATUS = '0'");
	
		add1.setParameter(0, fxtxstatus);
		add1.setParameter(1, fxuserid);

		int r2 = add1.executeUpdate();

	}

}
