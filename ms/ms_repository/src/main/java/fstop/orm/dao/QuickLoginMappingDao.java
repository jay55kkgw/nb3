package fstop.orm.dao;

import java.util.List;
import java.util.function.BiFunction;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;
import com.netbank.domain.orm.core.SessionAdapter;

import fstop.orm.po.QUICKLOGINMAPPING;
import fstop.orm.po.QUICKLOGINMAPPING_PK;
import lombok.extern.slf4j.Slf4j;

@Transactional(
		transactionManager="nb3transactionManager",
        rollbackFor = {Throwable.class}
)
@Repository
@Slf4j
public class QuickLoginMappingDao extends LegacyJpaRepository<QUICKLOGINMAPPING,QUICKLOGINMAPPING_PK>{

	
	
	public List<QUICKLOGINMAPPING> getDataByCusidn(String cusidn){
		List<QUICKLOGINMAPPING> qresult = null;
		try
		{
			String queryString = " FROM fstop.orm.po.QUICKLOGINMAPPING WHERE CUSIDN = ? AND NOT DEVICESTATUS = '9'";
			qresult = find(queryString, cusidn );
//			String queryString = " FROM fstop.orm.po.QUICKLOGINMAPPING WHERE CUSIDN = :cusidn";
//			Query query = getSession().createQuery(queryString);
//			query.setParameter("cusidn", cusidn);
//			qresult = query.getResultList();
			
		}
		catch (Exception e)
		{
			log.error("getDataByCusidn error >> {}",e);
		}
		return qresult;
		
		
	}
	public QUICKLOGINMAPPING getDataByDeviceIdAndIdGateId(String deviceId, String idGateId){
		QUICKLOGINMAPPING po = null;
		List<QUICKLOGINMAPPING> qresult = null;
		try
		{
			String queryString = " FROM fstop.orm.po.QUICKLOGINMAPPING WHERE DEVICEID = ? AND IDGATEID = ?";
			qresult = find(queryString, deviceId, idGateId);
			if(qresult !=null && !qresult.isEmpty()) {
				po = qresult.get(0);
			}
			
		}
		catch (Exception e)
		{
			log.error("getDataByInputData error >> {}",e);
		}
		return po;
	}

	public QUICKLOGINMAPPING getDataByDeviceId(String deviceId){
		QUICKLOGINMAPPING po = null;
		List<QUICKLOGINMAPPING> qresult = null;
		try
		{
			String queryString = " FROM fstop.orm.po.QUICKLOGINMAPPING WHERE DEVICEID = ?";
			qresult = find(queryString, deviceId);

//			String queryString = " FROM fstop.orm.po.QUICKLOGINMAPPING WHERE CUSIDN = :cusidn AND IDGATEID = :idGateId AND DEVICEID = :deviceId";
//			Query query = getSession().createQuery(queryString);
//			query.setParameter("cusidn", cusidn);
//			query.setParameter("idGateId", idGateId);
//			query.setParameter("deviceId", deviceId);
//			qresult = query.getResultList();
			
			if(qresult !=null && !qresult.isEmpty()) {
				po = qresult.get(0);
			}
			
		}
		catch (Exception e)
		{
			log.error("getDataByInputData error >> {}",e);
		}
		return po;
	}

	public QUICKLOGINMAPPING getDataByIdGateId(String idGateId){
		QUICKLOGINMAPPING po = null;
		List<QUICKLOGINMAPPING> qresult = null;
		try
		{
			String queryString = " FROM fstop.orm.po.QUICKLOGINMAPPING WHERE IDGATEID = ?";
			qresult = find(queryString, idGateId);

//			String queryString = " FROM fstop.orm.po.QUICKLOGINMAPPING WHERE CUSIDN = :cusidn AND IDGATEID = :idGateId AND DEVICEID = :deviceId";
//			Query query = getSession().createQuery(queryString);
//			query.setParameter("cusidn", cusidn);
//			query.setParameter("idGateId", idGateId);
//			query.setParameter("deviceId", deviceId);
//			qresult = query.getResultList();
			
			if(qresult !=null && !qresult.isEmpty()) {
				po = qresult.get(0);
			}
			
		}
		catch (Exception e)
		{
			log.error("getDataByInputData error >> {}",e);
		}
		return po;
	}
	
	public QUICKLOGINMAPPING getDataByInputData(String cusidn ,String idGateId , String deviceId){
		QUICKLOGINMAPPING po = null;
		List<QUICKLOGINMAPPING> qresult = null;
		try
		{
			String queryString = " FROM fstop.orm.po.QUICKLOGINMAPPING WHERE CUSIDN = ? AND IDGATEID = ? AND DEVICEID = ?";
			qresult = find(queryString, cusidn , idGateId , deviceId);

//			String queryString = " FROM fstop.orm.po.QUICKLOGINMAPPING WHERE CUSIDN = :cusidn AND IDGATEID = :idGateId AND DEVICEID = :deviceId";
//			Query query = getSession().createQuery(queryString);
//			query.setParameter("cusidn", cusidn);
//			query.setParameter("idGateId", idGateId);
//			query.setParameter("deviceId", deviceId);
//			qresult = query.getResultList();
			
			if(qresult !=null && !qresult.isEmpty()) {
				po = qresult.get(0);
			}
			
		}
		catch (Exception e)
		{
			log.error("getDataByInputData error >> {}",e);
		}
		return po;
	}
	
	public String getCusidn(String idGateId , String deviceId){
		QUICKLOGINMAPPING po = null;
		String cusidn = null;
		List<QUICKLOGINMAPPING> qresult = null;
		try
		{
			String queryString = " FROM fstop.orm.po.QUICKLOGINMAPPING WHERE IDGATEID = ? AND DEVICEID = ? AND DEVICESTATUS = '0'";
			qresult = find(queryString, idGateId , deviceId);

//			String queryString = " FROM fstop.orm.po.QUICKLOGINMAPPING WHERE CUSIDN = :cusidn AND IDGATEID = :idGateId AND DEVICEID = :deviceId";
//			Query query = getSession().createQuery(queryString);
//			query.setParameter("cusidn", cusidn);
//			query.setParameter("idGateId", idGateId);
//			query.setParameter("deviceId", deviceId);
//			qresult = query.getResultList();
			
			if(qresult !=null && !qresult.isEmpty()) {
				po = qresult.get(0);
				cusidn = po.getPks().getCUSIDN();
			}
		}
		catch (Exception e)
		{
			log.error("getDataByInputData error >> {}",e);
		}
		return cusidn;
	}


	@Override
	public EntityManager getEntityManager() {
		// TODO Auto-generated method stub
		return super.getEntityManager();
	}


	@Override
	public long count() {
		// TODO Auto-generated method stub
		return super.count();
	}


	@Override
	public List<QUICKLOGINMAPPING> findAll() {
		// TODO Auto-generated method stub
		return super.findAll();
	}


	@Override
	public QUICKLOGINMAPPING findById(QUICKLOGINMAPPING_PK id) {
		// TODO Auto-generated method stub
		return super.findById(id);
	}


	@Override
	public void save(QUICKLOGINMAPPING entity) {
		// TODO Auto-generated method stub
		super.save(entity);
	}


	@Override
	public QUICKLOGINMAPPING update(QUICKLOGINMAPPING entity) {
		// TODO Auto-generated method stub
		return super.update(entity);
	}


	@Override
	public void delete(QUICKLOGINMAPPING entity) {
		// TODO Auto-generated method stub
		super.delete(entity);
	}


	@Override
	public Query getQuery(String hql, Object... values) {
		// TODO Auto-generated method stub
		return super.getQuery(hql, values);
	}


	@Override
	public Query getCriteriaQuery(Class<QUICKLOGINMAPPING> entityClass,
			BiFunction<Root, CriteriaBuilder, Predicate>... criteriaFunction) {
		// TODO Auto-generated method stub
		return super.getCriteriaQuery(entityClass, criteriaFunction);
	}


	@Override
	public List find(String hql, Object... values) {
		// TODO Auto-generated method stub
		return super.find(hql, values);
	}


	@Override
	public List<QUICKLOGINMAPPING> findBy(Class<QUICKLOGINMAPPING> entityClass, String name, Object value) {
		// TODO Auto-generated method stub
		return super.findBy(entityClass, name, value);
	}


	@Override
	public QUICKLOGINMAPPING findUniqueBy(Class<QUICKLOGINMAPPING> entityClass, String name, Object value) {
		// TODO Auto-generated method stub
		return super.findUniqueBy(entityClass, name, value);
	}


	@Override
	public List<QUICKLOGINMAPPING> findByLike(Class<QUICKLOGINMAPPING> entityClass, String name, String value) {
		// TODO Auto-generated method stub
		return super.findByLike(entityClass, name, value);
	}


	@Override
	public SessionAdapter getSession() {
		// TODO Auto-generated method stub
		return super.getSession();
	}


	@Override
	public Page pagedQuery(String hql, int pageNo, int pageSize, Object... values) {
		// TODO Auto-generated method stub
		return super.pagedQuery(hql, pageNo, pageSize, values);
	}
	
	
	
	
}