package fstop.orm.dao;

import java.util.List;

import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.LegacyJpaRepository;

import fstop.orm.po.TXNCRLINEAPP;
import fstop.orm.po.TXNCRLINEAPP_PK;
import lombok.extern.slf4j.Slf4j;

@Transactional(
		transactionManager="nb3transactionManager",
        rollbackFor = {Throwable.class}
)
@Repository
@Slf4j
public class TxnCrlineAppDao extends LegacyJpaRepository<TXNCRLINEAPP, TXNCRLINEAPP_PK> {
	protected Logger logger = Logger.getLogger(getClass());

//	@SuppressWarnings( { "unused", "unchecked" })
//	public List<TXNCARDAPPLY> findByLastDate(String lastDate) {
//		
//		String sql = "from TXNCARDAPPLY where LASTDATE <= ? AND STATUS='0' order by RCVNO ";
//		
//		Query query = getQuery(sql, lastDate);
//		
//		return query.getResultList();
//	}
	
	public List<TXNCRLINEAPP> getDataByDate(String execDate){
		String sql = "from TXNCRLINEAPP where CPRIMDATE = ? ";
		Query query = getQuery(sql, execDate);
		
		return query.getResultList();
	}
	
	public List<TXNCRLINEAPP> getDataByRangeDate(String Date1,String Date2){
		String sql = "from TXNCRLINEAPP where CPRIMDATE >= ? AND CPRIMDATE <= ? ";
		Query query = getQuery(sql, Date1 , Date2 );
		return query.getResultList();
	}
	
}