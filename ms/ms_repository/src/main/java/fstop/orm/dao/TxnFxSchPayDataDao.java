package fstop.orm.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;
import javax.transaction.Transactional;

import org.hibernate.query.NativeQuery;
import org.hibernate.type.IntegerType;
import org.joda.time.DateTime;
import org.springframework.stereotype.Repository;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.orm.po.TXNFXSCHPAYDATA;
import fstop.orm.po.TXNFXSCHPAYDATA_PK;
import fstop.orm.po.TXNTWSCHPAYDATA;
import fstop.orm.po.TxnFxSchPayDataErrorCount;
import fstop.orm.po.TxnTwSchPayDataErrorCount;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import topibs.utility.NumericUtil;

@Slf4j
@Repository
@Transactional
public class TxnFxSchPayDataDao extends LegacyJpaRepository<TXNFXSCHPAYDATA, TXNFXSCHPAYDATA_PK> {

	/**
	 * 預約r交易新增寫入TXNLog的值
	 * 
	 * @param adopid 交易代碼
	 * @param params 交易參數
	 * @return
	 */
	public Map<String, String> upTxnlogparams(String adopid, Map<String, String> params) {

		params.put("UID", params.get("CUSIDN"));
		params.put("ADREQTYPE", "B");
		// 將常用的提出來變預設參考F001
		params.put("#TXNLOG", "TRUE");
		params.put("#ADTXACNO", params.get("CUSTACC"));
		params.put("#ADTXAMT", params.get("ADTXAMT"));
		params.put("#ADCURRENCY", params.get("ADCURRENCY"));
		params.put("#ADSVBH", "050");
		params.put("#ADAGREEF", params.get("ADAGREEF"));
		params.put("#ADAGREEF", "N");
		// 依各功能改變預設寫入的值
		if ("F001".equalsIgnoreCase(adopid)) {
			// 將常用的提出來變預設參考F001
		} else if ("F002".equalsIgnoreCase(adopid)) {
			params.put("#ADSVBH", params.get("FXRCVBKCODE"));
		} else if ("F003".equalsIgnoreCase(adopid)) {
			params.put("#ADTXACNO", params.get("BENACC"));
			params.put("#ADTXAMT", params.get("TXAMT"));
			params.put("#ADCURRENCY", params.get("RETCCY"));
			params.put("#ADSVBH", params.get("RCVBANK"));
		} else if ("N175".equalsIgnoreCase(adopid)) {
			params.put("#ADTXACNO", params.get("FDPACN"));
			params.put("#ADTXAMT", params.get("AMT"));
			params.put("#ADCURRENCY", params.get("CUID"));
		} else if ("N174".equalsIgnoreCase(adopid)) {
			params.put("#ADTXACNO", params.get("FYTSFAN"));
			params.put("#ADTXAMT", params.get("AMOUNT"));
			params.put("#ADCURRENCY", params.get("OUTCRY"));
			params.put("ADAGREEF", params.get("ADAGREEF"));
		}
		return params;
	}

	/**
	 * 更新預約交易結果
	 * 
	 * @param txnfxschpaydata
	 * @param mvhimplmap
	 * @return
	 */
	public TXNFXSCHPAYDATA upRetunToTxnfxschpaydata(TXNFXSCHPAYDATA txnFxSchPayData, Map<String, String> mvhimplmap) {
		log.info("更新預約交易結果 !!!!!!!");
		// 取得交易序號
		String stan = StrUtils.isEmpty(mvhimplmap.get("STAN")) ? "" : mvhimplmap.get("STAN");
		// 手續費幣別
		String str_feeCcy = mvhimplmap.get("COMMCCY");
		// 手續費金額
		String str_feeAmt = StrUtils.isEmpty(mvhimplmap.get("COMMAMT")) ? "" : mvhimplmap.get("COMMAMT");
		// 郵電費金額
		String str_cabAmt = StrUtils.isEmpty(mvhimplmap.get("CABCHG")) ? "" : mvhimplmap.get("CABCHG");
		// 國外費用金額
		String str_ourAmt = StrUtils.isEmpty(mvhimplmap.get("OURCHG")) ? "" : mvhimplmap.get("OURCHG");
		// 匯率
		String str_Rate = StrUtils.isEmpty(mvhimplmap.get("EXRATE")) ? "" : mvhimplmap.get("EXRATE");
		// 匯率 format
		if (str_Rate != null && str_Rate.replace("0", "").equals("")) {
			str_Rate = "";
		} else {
			str_Rate = NumericUtil.formatNumberString(str_Rate, 8);
		}
		//// 如果是 手續費 & 郵電費 & 國外費用 需先做處理
		if ("TWD".equals(str_feeCcy) || "JPY".equals(str_feeCcy)) {
			str_feeAmt = str_feeAmt.substring(0, str_feeAmt.length() - 2);
			str_cabAmt = str_cabAmt.substring(0, str_cabAmt.length() - 2);
			str_ourAmt = str_ourAmt.substring(0, str_ourAmt.length() - 2);
		}
		// 手續費 & 郵電費 & 國外費用 format
		str_feeAmt = NumericUtil.formatNumberStringByCcy(str_feeAmt, 2, str_feeCcy);
		str_cabAmt = NumericUtil.formatNumberStringByCcy(str_cabAmt, 2, str_feeCcy);
		str_ourAmt = NumericUtil.formatNumberStringByCcy(str_ourAmt, 2, str_feeCcy);

		txnFxSchPayData.setADTXNO(stan);
		txnFxSchPayData.setFXEFEE(str_feeAmt);
		txnFxSchPayData.setFXTELFEE(str_cabAmt);
		txnFxSchPayData.setFXOURCHG(str_ourAmt);
		txnFxSchPayData.setFXEXRATE(str_Rate);
		return txnFxSchPayData;

	}

	public TXNFXSCHPAYDATA checkupdateTxnFxSchPayData(Map<String, String> params) {

		TXNFXSCHPAYDATA txnFxSchPayData = new TXNFXSCHPAYDATA();
		try {
			log.debug(ESAPIUtil.vaildLog("@@@@@@ 檢查是否更新預約交易檔 {}" + params.get("__EXECWAY")));
			log.debug(ESAPIUtil.vaildLog("@@@@@@ checkupdateTxnFxSchPayData  params{}" + CodeUtil.toJson(params)));
			// 預約交易
			if ("BATCH".equals((String) params.get("__EXECWAY"))) {
				log.debug(ESAPIUtil.vaildLog("@@@@@@ 更新預約交易檔 {}" + CodeUtil.toJson(params)));
				// 交易序號
				String stan = params.get("STAN");
				// 取得預約交易資料的條件
				TXNFXSCHPAYDATA_PK pk = new TXNFXSCHPAYDATA_PK();
				String fxschno = params.get("FXSCHNO");
				String fxschtxdate = params.get("FXSCHTXDATE");
				String fxuserid = params.get("UID");
				pk.setFXSCHNO(StrUtils.isEmpty(fxschno) ? "" : fxschno);
				pk.setFXSCHTXDATE(StrUtils.isEmpty(fxschtxdate) ? "" : fxschtxdate);
				pk.setFXUSERID(StrUtils.isEmpty(fxuserid) ? "" : fxuserid);
				// 取得預約交易資料物件
				txnFxSchPayData = this.findById(pk);
				// 預約一扣
				if ("SEND".equals((String) params.get("__TRANS_STATUS"))) {
					txnFxSchPayData.setADTXNO(StrUtils.isEmpty(stan) ? "" : stan);
				}
				// 預約二扣
				else if ("RESEND".equals((String) params.get("__TRANS_STATUS"))) {
					txnFxSchPayData.setADTXNO(StrUtils.isEmpty(stan) ? "" : stan);
				}
				// 更新預約檔
				this.update(txnFxSchPayData);
			} else {
				log.debug(ESAPIUtil.vaildLog("@@@@@@ 不更新更新預約交易檔 {}" + CodeUtil.toJson(params)));
			}

		} catch (Exception e) {
			log.error("checkupdateTxnFxSchPayData getAll Error.", e);
		}
		return txnFxSchPayData;

	}

	public List<TXNFXSCHPAYDATA> findByDateAndFirst(String today) {

		String sql = "FROM TXNFXSCHPAYDATA m where m.pks.FXSCHTXDATE=:today and (FXRESEND is null or FXRESEND = '' or FXRESEND='0' )";

		Query query = getQuery(sql);
		query.setParameter("today", today);
		log.debug(ESAPIUtil.vaildLog("findByDateAndFirst " + query.getResultList()));
		return query.getResultList();
	}

	@Transactional
	public void updateStatus(TXNFXSCHPAYDATA data, String status, String excode) {

		log.debug("update TXNFXSCHPAYDATA(FXSCHNO: " + data.getPks().getFXSCHNO() + " FXSCHTXDATE:"
				+ data.getPks().getFXSCHTXDATE() + " FXUSERID:" + data.getPks().getFXUSERID() + ") status: " + status
				+ ", excode: " + excode);
		try {
			Date d = new Date();
			data.setLASTDATE(DateTimeUtils.format("yyyyMMdd", d));
			data.setLASTTIME(DateTimeUtils.format("HHmmss", d));
			data.setFXTXSTATUS(status);
			if (null != excode || !"".equals(excode)) {
				data.setFXEXCODE(excode);
			}

			log.debug("TXNFXSCHPAYDATA updateStatus data >>>>> {} ", data.toString());

			save(data);
		} catch (Exception e) {
			log.error("TxnFxTransfer updateStatus Error.", e);
		}
	}

	/**
	 * 
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public List<TXNFXSCHPAYDATA> getAll(String cusidn, Map<String, String> reqParam) {
		List<TXNFXSCHPAYDATA> result = new ArrayList();
		try {
			log.debug("TXNFXSCHPAYDATA_Dao >> getAll");
			String dpfdate = reqParam.get("CMSDATE");
			dpfdate = dpfdate.replace("/", "");
			log.trace("dpfdate>>>{}", dpfdate);
			String dptdate = reqParam.get("CMEDATE");
			dptdate = dptdate.replace("/", "");
			log.trace("dptdate>>>{}", dptdate);
			String queryString = " FROM fstop.orm.po.TXNFXSCHPAYDATA WHERE FXUSERID  = :cusidn AND FXSCHTXDATE BETWEEN :dpfdate AND :dptdate";
			Query query = getSession().createQuery(queryString);
			query.setParameter("cusidn", cusidn);
			query.setParameter("dpfdate", dpfdate);
			query.setParameter("dptdate", dptdate);
			result = query.getResultList();
			log.debug("TXNFXSCHPAYDATADao result size >> {}", result.size());
		} catch (Exception e) {
			log.error("TxnFxTransfer getAll Error.", e);
		}
		return result;
	}

	/**
	 * 查詢DPTXSTATUS By FGTXSTATUS
	 * 
	 * @return
	 */
	public List<TXNFXSCHPAYDATA> getByUid(String cusidn, Map<String, String> reqParam) {
		List<TXNFXSCHPAYDATA> result = new ArrayList();
		String fgtxstatus = reqParam.get("FGTXSTATUS");
		log.trace("fgtxstatus>>>{}", fgtxstatus);
		String dpfdate = reqParam.get("CMSDATE");
		dpfdate = dpfdate.replace("/", "");
		log.trace("dpfdate>>>{}", dpfdate);
		String dptdate = reqParam.get("CMEDATE");
		dptdate = dptdate.replace("/", "");
		log.trace("dptdate>>>{}", dptdate);
		String queryString = " FROM fstop.orm.po.TXNFXSCHPAYDATA WHERE FXUSERID = :cusidn AND FXTXSTATUS = :fgtxstatus AND FXSCHTXDATE BETWEEN :dpfdate AND :dptdate";
		try {
			log.trace("cusidn>>{}", cusidn);
			log.trace("FGTXSTATUS>>{}", fgtxstatus);
			Query query = getSession().createQuery(queryString);
			query.setParameter("cusidn", cusidn);
			query.setParameter("fgtxstatus", fgtxstatus);
			query.setParameter("dpfdate", dpfdate);
			query.setParameter("dptdate", dptdate);
			result = query.getResultList();
			log.debug("TXNFXSCHPAYDATADao result size >> {}", result.size());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error("TxnFxSchPayData getByUid Error.", e);
		}
		return result;
	}

	/**
	 * 
	 * @param execDate
	 * @return
	 */
	public List<TXNFXSCHPAYDATA> findScheduleDueTo(String execDate) {

		log.debug("exec Date >> {}", execDate);
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT * FROM TXNFXSCHPAYDATA s ");
		sql.append(" Where exists (SELECT FXSCHNO FROM TXNFXSCHPAY  ");
		sql.append("       where FXSCHNO = s.FXSCHNO and FXTXSTATUS = '0' and FXUSERID=s.FXUSERID ) ");
		sql.append(" And (s.FXSCHTXDATE =:execDate) ");
		sql.append(" And  (s.FXTXSTATUS = '' or s.FXTXSTATUS is null )");
		sql.append(" And  (s.FXRESEND = '' or s.FXRESEND is null or s.FXRESEND='0' )");
		log.debug("FindScheduleDueTo.sql>>{}", sql);
		Query query = getSession().createNativeQuery(sql.toString(), TXNFXSCHPAYDATA.class);
//		String sql = "FROM TXNFXSCHPAYDATA where pks.FXSCHTXDATE=:execDate and (FXTXSTATUS is null or FXTXSTATUS = '') and (FXRESEND is null or FXRESEND = '' or FXRESEND='0' )";
//		Query query = getQuery(sql);
		
		query.setParameter("execDate", execDate);

		return ESAPIUtil.validStrList(query.getResultList());
	}

	/**
	 * 
	 * 撈一扣的資料 3. SELECT * FROM TXNFXSCHPAYDATA s Where exists (SELECT FXSCHNO FROM
	 * TXNFXSCHPAY where FXSCHNO = s.FXSCHNO and FXTXSTATUS = '0') --預約成功 And
	 * (s.FXSCHTXDATE between '20190831' and '20190902') And (s.FXTXSTATUS = '')
	 * --尚未執行 Order by s.FXSCHTXDATE
	 * 
	 * @param today
	 * @return
	 */
	public List<TXNFXSCHPAYDATA> findBy_sp_cal_fxschdate(String today) {
		// 執行 store procedure
		String startday = sp_cal_fxschdate(today);
		// 組SQL
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT * FROM TXNFXSCHPAYDATA s ");
		sql.append(" Where exists (SELECT FXSCHNO FROM TXNFXSCHPAY  ");
		sql.append(" where FXSCHNO = s.FXSCHNO and FXTXSTATUS = '0' and FXUSERID=s.FXUSERID)  ");
		sql.append(" And  (s.FXSCHTXDATE BETWEEN :startday AND :today)");
		sql.append(" And  (s.FXTXSTATUS = '') ");
		sql.append(" Order by s.FXSCHTXDATE ");
		log.debug("findBy_sp_cal_fxschdate.sql>>{}", sql);
		Query query = getSession().createNativeQuery(sql.toString(), TXNFXSCHPAYDATA.class);
		query.setParameter("startday", startday);
		query.setParameter("today", today);
		log.debug(ESAPIUtil.vaildLog("findBy_sp_cal_fxschdate " + query.getResultList()));
		List<TXNFXSCHPAYDATA> list = query.getResultList();
		log.debug("findBy_sp_cal_fxschdate  {}", ESAPIUtil.vaildLog(CodeUtil.toJson(list)));
		for (TXNFXSCHPAYDATA each : list) {
			each.setFXTITAINFO("");
			each.setFXTOTAINFO("");
		}
		log.debug("findBy_sp_cal_fxschdate process {}", ESAPIUtil.vaildLog(CodeUtil.toJson(list)));
		return ESAPIUtil.validStrList(list);
//		return ESAPIUtil.validStrList(query.getResultList());
	}

	/**
	 * 
	 * 撈二扣的資料
	 * 
	 * @param today
	 * @return
	 */
	public List<TXNFXSCHPAYDATA> findBy_sp_cal_fxschdate_second(String today) {

		// 執行 store procedure
		String startday = sp_cal_fxschdate(today);
		// 組SQL
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT * FROM TXNFXSCHPAYDATA s ");
		sql.append(" Where exists (SELECT FXSCHNO FROM TXNFXSCHPAY  ");
		sql.append(" where FXSCHNO = s.FXSCHNO and FXTXSTATUS = '0' and FXUSERID=s.FXUSERID)  ");
		sql.append(" And  (s.FXSCHTXDATE BETWEEN :startday AND :today)");
		sql.append(" And  (s.FXTXSTATUS = '2') ");
		sql.append(" And  (s.FXEXCODE != '') ");
		sql.append(" Order by s.FXSCHTXDATE ");
		log.debug("findBy_sp_cal_fxschdate.sql>>{}", sql);
		Query query = getSession().createNativeQuery(sql.toString(), TXNFXSCHPAYDATA.class);
		query.setParameter("startday", startday);
		query.setParameter("today", today);
		List<TXNFXSCHPAYDATA> list = query.getResultList();
		log.debug("findBy_sp_cal_fxschdate  {}", ESAPIUtil.vaildLog(CodeUtil.toJson(list)));
		for (TXNFXSCHPAYDATA each : list) {
			each.setFXTITAINFO("");
			each.setFXTOTAINFO("");
		}
		log.debug("findBy_sp_cal_fxschdate process {}", ESAPIUtil.vaildLog(CodeUtil.toJson(list)));
		return ESAPIUtil.validStrList(list);

	}

	/**
	 * 1.呼叫store procedure "SP_CAL_FXSCHDATE" 傳入兩個參數，INFXSCHTXDATE
	 * 要給值(=跑批當天日期)，例如:20190902 Execute: "SP_CAL_FXSCHDATE"(OUTFXSCHTXBEGDATE:=(''),
	 * INFXSCHTXDATE:=20190902 )
	 * 
	 * @param infxschtxdate 跑批當天日期
	 * @return
	 */
	public String sp_cal_fxschdate(String infxschtxdate) {

		String outfxschtxbegdate = infxschtxdate;
		try {
			log.debug("建立 StoredProcedureQuery >>> {} ", infxschtxdate);
			StoredProcedureQuery storedProcedure = getEntityManager()
					.createNamedStoredProcedureQuery("ADMHOLIDAY.SP_CAL_FXSCHDATE");
			// 2.回傳本次批次應執行的起始日，例如:20190831，若前一日是上班日(非假日)則會回傳批次當日:20190902
			// Results:
			// "SP_CAL_FXSCHDATE"(OUTFXSCHTXBEGDATE:=20190831, INFXSCHTXDATE:=20190902 )
			// set parameters
			log.debug("設定 StoredProcedureQuery >>> {}", infxschtxdate);
			storedProcedure.setParameter("INFXSCHTXDATE", infxschtxdate);
			// execute SP
			storedProcedure.execute();
			log.debug("取得 StoredProcedureQuery outfxschtxbegdate");
			outfxschtxbegdate = storedProcedure.getOutputParameterValue("OUTFXSCHTXBEGDATE").toString();
			log.debug("取得 StoredProcedureQuery outfxschtxbegdate {}", outfxschtxbegdate);
		} catch (Exception e) {
			log.error("sp_cal_fxschdate Exception {}", e);
		}

		return outfxschtxbegdate;
	}

	public List<TXNFXSCHPAYDATA> findByFXSCHNO(String FXSCHNO) {
		log.debug("FXSCHNO >> {}", FXSCHNO);
		String sql = "FROM TXNFXSCHPAYDATA where pks.FXSCHNO=:fxschno";
		Query query = getQuery(sql);
		query.setParameter("fxschno", FXSCHNO);

		return ESAPIUtil.validStrList(query.getResultList());
	}
	
	 public List<TXNFXSCHPAYDATA> findErrorList(String today) {

	        String sql = "FROM TXNFXSCHPAYDATA where pks.FXSCHTXDATE=:today and FXTXSTATUS not in('','0')";

	        Query query = getQuery(sql);
	        query.setParameter("today", today);
	        
	        if(query.getResultList().size()!=0) {
	        	 return ESAPIUtil.validStrList(query.getResultList());
	        }else {
	        	return new ArrayList();
	        }
	       
	 }
	
	/**
	 * according type to get data
	 * type >> 1 total 
	 * type >> 2 success 
     * type >> 3 custom error 
     * type >> 4 system error 
     * type >> 5 total Failed 
     * 
	 */
	 public int countMethod(int type) {
		 DateTime now = new DateTime();
		 String today = now.toString("yyyyMMdd");
		 String sql ="";
		 switch (type){
		 case 1:
			 sql+="select count(*) as COUNT from TXNFXSCHPAYDATA where FXTXSTATUS <>'' and (FXSCHTXDATE=:today or LASTDATE=:today) and LASTTIME<'100000'";
			 break;
		 case 2:
			 //成功
			 sql+="select count(*) as COUNT from TXNFXSCHPAYDATA where FXTXSTATUS='0' and (FXSCHTXDATE=:today or LASTDATE=:today) and LASTTIME<'100000'";
			 break;
		 case 3:
			//客戶面錯誤
			 sql+="select count(*) as COUNT from TXNFXSCHPAYDATA where (FXEXCODE like 'E%' or FXEXCODE like 'M%' or FXEXCODE like 'W%') and (FXSCHTXDATE=:today or LASTDATE=:today) and LASTTIME<'100000'";
			 break;
		 case 4:
			 //系統面錯誤
			 sql+="select count(*) as COUNT from TXNFXSCHPAYDATA where (FXEXCODE like 'Z%' or FXEXCODE like 'FE%' or FXEXCODE like 'MAC%' or FXEXCODE='3303') and (FXSCHTXDATE=:today or LASTDATE=:today) and LASTTIME<'100000'";
			 break;
		 case 5:
			 //失敗
			 sql+="select count(*) as COUNT from TXNFXSCHPAYDATA where FXTXSTATUS not in ('','0') and (FXSCHTXDATE=:today or LASTDATE=:today) and LASTTIME<'100000'";
			 break;
		 }
		 Query query = getSession().createSQLQuery(sql);
	     query.setParameter("today", today);
	     query.unwrap(NativeQuery.class).addScalar("COUNT", IntegerType.INSTANCE);
	     List result = ESAPIUtil.validStrList(query.getResultList());

		return (Integer) result.get(0);
	 }
	 
	 /**
		 * according type to get data
		 * type >> 1 total 
		 * type >> 2 success 
	     * type >> 3 custom error 
	     * type >> 4 system error 
	     * type >> 5 total Failed 
	     * 
		 */
		 public int countMethod2nd(int type) {
			 DateTime now = new DateTime();
			 String today = now.toString("yyyyMMdd");
			 String sql ="";
			 switch (type){
			 case 1:
				 sql+="select count(*) as COUNT from TXNFXSCHPAYDATA where FXTXSTATUS <>'' and (FXSCHTXDATE=:today or LASTDATE=:today) and LASTTIME>'100000'";
				 break;
			 case 2:
				 //成功
				 sql+="select count(*) as COUNT from TXNFXSCHPAYDATA where FXTXSTATUS='0' and (FXSCHTXDATE=:today or LASTDATE=:today) and LASTTIME>'100000'";
				 break;
			 case 3:
				//客戶面錯誤
				 sql+="select count(*) as COUNT from TXNFXSCHPAYDATA where (FXEXCODE like 'E%' or FXEXCODE like 'M%' or FXEXCODE like 'W%') and (FXSCHTXDATE=:today or LASTDATE=:today) and LASTTIME>'100000'";
				 break;
			 case 4:
				 //系統面錯誤
				 sql+="select count(*) as COUNT from TXNFXSCHPAYDATA where (FXEXCODE like 'Z%' or FXEXCODE like 'FE%' or FXEXCODE like 'MAC%' or FXEXCODE='3303') and (FXSCHTXDATE=:today or LASTDATE=:today) and LASTTIME>'100000'";
				 break;
			 case 5:
				 //失敗
				 sql+="select count(*) as COUNT from TXNFXSCHPAYDATA where FXTXSTATUS not in ('','0') and (FXSCHTXDATE=:today or LASTDATE=:today) and LASTTIME>'100000'";
				 break;
			 }
			 Query query = getSession().createSQLQuery(sql);
		     query.setParameter("today", today);
		     query.unwrap(NativeQuery.class).addScalar("COUNT", IntegerType.INSTANCE);
		     List result = ESAPIUtil.validStrList(query.getResultList());

			return (Integer) result.get(0);
		 }
	 
	 public List<TxnFxSchPayDataErrorCount> countErrorList(String today) {
		 List<Map<String,String>> resultList = new ArrayList<>();
		 String sql = "select A.FXEXCODE as FXEXCODE,B.ADMSGIN as FXEXCODEMSG ,count(A.FXEXCODE) as TIMES from TXNFXSCHPAYDATA as A,ADMMSGCODE B where A.LASTDATE=:today and A.FXEXCODE<>'' and A.FXEXCODE=B.ADMCODE group by A.FXEXCODE,B.ADMSGIN";
		 Query query = getSession().createNativeQuery(sql,TxnFxSchPayDataErrorCount.class);
		 query.setParameter("today", today);
		 
		 if(query.getResultList().size()!=0) {
        	 return ESAPIUtil.validStrList(query.getResultList());
        }else {
        	return new ArrayList();
        }
	 }
	 
	 public List<TxnFxSchPayDataErrorCount> countErrorList2nd(String today) {
		 List<Map<String,String>> resultList = new ArrayList<>();
		 String sql = "select A.FXEXCODE as FXEXCODE,B.ADMSGIN as FXEXCODEMSG ,count(A.FXEXCODE) as TIMES from TXNFXSCHPAYDATA as A,ADMMSGCODE B where A.LASTDATE=:today and A.LASTTIME>'100000' and A.FXEXCODE<>'' and A.FXEXCODE=B.ADMCODE group by A.FXEXCODE,B.ADMSGIN";
		 Query query = getSession().createNativeQuery(sql,TxnFxSchPayDataErrorCount.class);
		 query.setParameter("today", today);
		 
		 if(query.getResultList().size()!=0) {
        	 return ESAPIUtil.validStrList(query.getResultList());
        }else {
        	return new ArrayList();
        }
	 }

}