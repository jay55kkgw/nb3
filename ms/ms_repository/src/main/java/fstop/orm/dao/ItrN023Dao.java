package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import fstop.orm.po.ITRN023;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.List;

@Slf4j
@Repository
@Transactional
public class ItrN023Dao extends LegacyJpaRepository<ITRN023, String> {
	protected Logger logger = Logger.getLogger(getClass());

	public List<ITRN023> getAll() {
		return find("FROM ITRN023 ORDER BY RECNO");
	}
	
	public void insert(ITRN023 itrn023) {
		int r2=0;
		String fillstr=new String();
		Query add1=null;
		
		add1=this.getSession().createSQLQuery("INSERT INTO ITRN023 VALUES(? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,?)");
		add1.setParameter(0, itrn023.getRECNO());
		log.debug("RECNO = " + itrn023.getRECNO());
		add1.setParameter(1, itrn023.getHEADER());
		log.debug("HEADER = " + itrn023.getHEADER());
		add1.setParameter(2, itrn023.getSEQ());
		log.debug("SEQ = " + itrn023.getSEQ());
		add1.setParameter(3, itrn023.getDATE());
		log.debug("DATE = " + itrn023.getDATE());
		add1.setParameter(4, itrn023.getTIME());
		log.debug("TIME = " + itrn023.getTIME());
		add1.setParameter(5, itrn023.getCOUNT());
		log.debug("COUNT = " + itrn023.getCOUNT());
		add1.setParameter(6, itrn023.getCOLOR());
		log.debug("COLOR = " + itrn023.getCOLOR());
		add1.setParameter(7, itrn023.getCRYNAME());
		log.debug("CRYNAME = " + itrn023.getCRYNAME());
		add1.setParameter(8, itrn023.getCRY());
		log.debug("CRY = " + itrn023.getCRY());
		add1.setParameter(9, itrn023.getITR1());
		log.debug("ITR1 = " + itrn023.getITR1());
		add1.setParameter(10, itrn023.getITR2());
		log.debug("ITR2 = " + itrn023.getITR2());
		add1.setParameter(11, itrn023.getITR3());
		log.debug("ITR3 = " + itrn023.getITR3());
		add1.setParameter(12, itrn023.getITR4());
		log.debug("ITR4 = " + itrn023.getITR4());
		add1.setParameter(13, itrn023.getITR5());
		log.debug("ITR5 = " + itrn023.getITR5());
		add1.setParameter(14, itrn023.getITR6());
		log.debug("ITR6 = " + itrn023.getITR6());
		add1.setParameter(15, itrn023.getITR7());
		log.debug("ITR7 = " + itrn023.getITR7());
		add1.setParameter(16, itrn023.getITR8());
		log.debug("ITR8 = " + itrn023.getITR8());
		add1.setParameter(17, itrn023.getITR9());
		log.debug("ITR9 = " + itrn023.getITR9());
		add1.setParameter(18, itrn023.getITR10());
		log.debug("ITR10 = " + itrn023.getITR10());
		add1.setParameter(19, itrn023.getITR11());
		log.debug("ITR11 = " + itrn023.getITR11());
		add1.setParameter(20, itrn023.getITR12());
		log.debug("ITR12 = " + itrn023.getITR12());
		if(itrn023.getFILL()==null)
			fillstr=" ";
		else
			fillstr=itrn023.getFILL();
		log.debug("FILL = " + fillstr);
		add1.setParameter(21, fillstr);
		r2 = add1.executeUpdate();
	}

	public void deleteAll() {
		this.getSession().createSQLQuery("DELETE FROM ITRN023").executeUpdate();
	}
}
