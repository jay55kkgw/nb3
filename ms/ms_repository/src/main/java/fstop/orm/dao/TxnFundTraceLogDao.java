package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import fstop.orm.po.TXNFUNDTRACELOG;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.hibernate.query.NativeQuery;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.List;

@Slf4j
@Repository
@Transactional
public class TxnFundTraceLogDao extends LegacyJpaRepository<TXNFUNDTRACELOG, Long> {
	protected Logger logger = Logger.getLogger(getClass());
	
	@SuppressWarnings( { "unused", "unchecked" })
	public List<TXNFUNDTRACELOG> findByFdTxTime(String lastDate) {
					
		String sql = "select distinct a.* from TXNFUNDTRACELOG a, TXNLOG b where a.FDTXTIME like ? and a.FDNOTICETYPE='3' " + 
		 " and b.LASTDATE = ? and a.FDUSERID = b.ADUSERID and b.ADEXCODE = '' and b.FGTXWAY <> '' " + 
		 " and b.ADOPID like 'C0%' and LOCATE(a.ADTXNO, b.ADCONTENT) > 0 order by a.FDTXLOG";		

		
		Query query = getSession().createSQLQuery(sql);
		query.unwrap(NativeQuery.class).addEntity("a", TXNFUNDTRACELOG.class);
		
		query.setParameter(0, lastDate + "%");
		query.setParameter(1, lastDate);	
		
		return query.getResultList();
	}	

	@SuppressWarnings( { "unused", "unchecked" })
	public List<String> findByFdTxTimeRange(String startDate, String endDate) {
		
		String sql = "select distinct substr(a.FDTXTIME, 1, 8) from TXNFUNDTRACELOG a, TXNLOG b where a.FDTXTIME >= ? and a.FDTXTIME <= ? and a.FDNOTICETYPE='3' " + 
            		 " and b.LASTDATE >= ? and b.LASTDATE <= ? and a.FDUSERID = b.ADUSERID and b.ADEXCODE = '' and b.FGTXWAY <> '' " + 
		             " and b.ADOPID like 'C0%' and LOCATE(a.ADTXNO, b.ADCONTENT) > 0 order by substr(a.FDTXTIME, 1, 8)";	
		
		Query query = getQuery(sql, startDate + "000000", endDate + "235959", startDate, endDate);			
		
		return query.getResultList();
	}		
	
	@SuppressWarnings( { "unused", "unchecked" })
	public List<String> findByAdtxno(String adtxno, String lastDate) {
		
		String sql = "select cast(ADCONTENT as varchar(2000)) from TXNLOG where LASTDATE = '" + lastDate + "' and FGTXWAY <> '' and LOCATE('" + adtxno + "' , ADCONTENT) > 0 ";	
		
		Query query = getSession().createSQLQuery(sql);
		
		return query.getResultList();
	}		
		
}
