package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import fstop.orm.po.ADMCURRENCY;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.List;


@Slf4j
@Repository
@Transactional
public class AdmCurrencyDao extends LegacyJpaRepository<ADMCURRENCY, String> {
	protected Logger logger = Logger.getLogger(getClass());

	public List<ADMCURRENCY> getAll() {
		return find("FROM ADMCURRENCY");
	}
	
	public ADMCURRENCY findByAdccyno(String adccyno) {
		ADMCURRENCY result = null;
		try {
			result = (ADMCURRENCY)find("FROM ADMCURRENCY WHERE ADCCYNO = ? ", adccyno).get(0);
		}
		catch(Exception e) {}
		return result;
	}

	public ADMCURRENCY findByAdcurrency(String adcurrency) {
		ADMCURRENCY result = null;
		try {
			result = (ADMCURRENCY)find("FROM ADMCURRENCY WHERE ADCURRENCY = ? ", adcurrency).get(0);
		}
		catch(IndexOutOfBoundsException outofbound)
		{
			return(result);
		}
		catch(Exception e)
		{
			log.error(" findByAdcurrency>>{}", e);
			return(result);
		}
		return result;
	}
	
	public void saveOrUpdate(ADMCURRENCY admcurrency)
	{
		SQLQuery add1=null;
		ADMCURRENCY result = findByAdcurrency(admcurrency.getADCURRENCY());
		if(result==null)
		{
			save(admcurrency);
		}
		else
		{
			update(admcurrency);
		}
	}

	public void insert(ADMCURRENCY admcurrency) {
		int r2=0;
		Query add1=null;
		add1=this.getSession().createSQLQuery("INSERT INTO ADMCURRENCY VALUES(? ,? ,? ,? ,? ,? ,? ,? ,? ,?)");
		add1.setParameter(0, admcurrency.getADCURRENCY());
		add1.setParameter(1, admcurrency.getADCCYNO());
		add1.setParameter(2, admcurrency.getADCCYNAME());
		add1.setParameter(3, admcurrency.getBUY01());
		add1.setParameter(4, admcurrency.getSEL01());
		add1.setParameter(5, admcurrency.getBOOKRATE());
		add1.setParameter(6, admcurrency.getMSGCODE());
		add1.setParameter(7, admcurrency.getLASTUSER());
		add1.setParameter(8, admcurrency.getLASTDATE());
		add1.setParameter(9, admcurrency.getLASTTIME());
		r2 = add1.executeUpdate();
	}

	public ADMCURRENCY update(ADMCURRENCY admcurrency) {
		Query add1=this.getSession().createSQLQuery("UPDATE ADMCURRENCY SET ADCCYNO = ? ,ADCCYNAME = ? ,BUY01 = ? ,SEL01 = ? ,BOOKRATE = ? ,MSGCODE = ? ,LASTUSER = ? ,LASTDATE = ? ,LASTTIME = ? WHERE ADCURRENCY = ?");
		add1.setParameter(0, admcurrency.getADCCYNO());
		add1.setParameter(1, admcurrency.getADCCYNAME());
		add1.setParameter(2, admcurrency.getBUY01());
		add1.setParameter(3, admcurrency.getSEL01());
		add1.setParameter(4, admcurrency.getBOOKRATE());
		add1.setParameter(5, admcurrency.getMSGCODE());
		add1.setParameter(6, admcurrency.getLASTUSER());
		add1.setParameter(7, admcurrency.getLASTDATE());
		add1.setParameter(8, admcurrency.getLASTTIME());
		add1.setParameter(9, admcurrency.getADCURRENCY());
		int r2 = add1.executeUpdate();
		return admcurrency;
	}

	public void deleteAll() {
		this.getSession().createSQLQuery("DELETE FROM ADMCURRENCY").executeUpdate();
	}
}
