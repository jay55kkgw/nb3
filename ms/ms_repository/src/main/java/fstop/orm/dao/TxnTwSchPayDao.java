package fstop.orm.dao;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.Query;
import javax.transaction.Transactional;

import org.hibernate.query.NativeQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.util.ESAPIUtil;

import fstop.orm.po.TXNTWSCHEDULE;
import fstop.orm.po.TXNTWSCHPAY;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Transactional
public class TxnTwSchPayDao extends LegacyJpaRepository<TXNTWSCHPAY, String> {
//	private Logger log = LoggerFactory.getLogger(this.getClass());

	public static String MS_TW = "ms_tw";
	public static String MS_CC = "ms_cc";
	public static String MS_FUND = "ms_fund";
	public static String MS_FX = "ms_fx";
	public static String MS_GOLD = "ms_gold";
	public static String MS_OLS = "ms_ols";
	public static String MS_OLA = "ms_ola";
	public static String MS_PAY = "ms_pay";
	public static String MS_PS = "ms_ps";
	public static String MS_LOAN = "ms_loan";
	
	
	public List<TXNTWSCHPAY> getAll() {
		
		String sql = "FROM TXNTWSCHPAY";

		Query query = getSession().createQuery(sql);
		
		return query.getResultList();
	}

	/*
	依照 DPSCHNO（預約批號Guid(36位)） 找出預約主檔
	 */
	public Optional<TXNTWSCHPAY> findByDPSCHNO(String DPSCHNO) {

		String sql = "FROM TXNTWSCHPAY where DPSCHNO=:dpschno ";

		Query query = getQuery(sql);
		query.setParameter("dpschno", DPSCHNO);

		List<TXNTWSCHPAY> results = ESAPIUtil.validStrList(query.getResultList());
		if(results.size() > 0) {
			return Optional.of(results.get(0));
		} else {
			return Optional.empty();
		}

//		return Optional.of((List<TXNTWSCHPAY>)query.getResultList())
//				.filter(o -> o.size() > 0)
//				.map(o -> o.get(0));
	}
	
	
	/*
	依照 DPSCHNO（預約批號Guid(36位)） 找出預約主檔
	 */
	public Optional<List<TXNTWSCHPAY>> findSCHPAY(String DPSCHNO,String ADOPID,String DPUSERID) {

		String sql = "FROM TXNTWSCHPAY where DPSCHNO=:dpschno and ADOPID=:adopid and DPUSERID=:dpuserid";

		Query query = getQuery(sql);
		query.setParameter("dpschno", DPSCHNO);
		query.setParameter("adopid", ADOPID);
		query.setParameter("dpuserid", DPUSERID);

		List<TXNTWSCHPAY> results = query.getResultList();
		if(results.size() > 0) {
			return Optional.of(results);
		} else {
			return Optional.empty();
		}

//		return Optional.of((List<TXNTWSCHPAY>)query.getResultList())
//				.filter(o -> o.size() > 0)
//				.map(o -> o.get(0));
	}


	
	/**
	 * 根據 台幣預約ID DPSCHID 來更新 交易執行狀態 DPTXSTATUS
	 * @param dpschid
	 * @param dptxstatus
	 */
	public void updataDptxstatus(String twschid ,String dptxstatus) {
			log.debug("TXNTWSCHPAY_Dao >> updataDptxstatus");
			
			Query add1 = getSession().createQuery("UPDATE TXNTWSCHPAY SET DPTXSTATUS = ? WHERE DPSCHID = ?");
			add1.setParameter(0, dptxstatus);
			add1.setParameter(1, twschid);

			int r2 = add1.executeUpdate();
	}
	

	/**
	 * 找出某一日的台幣週期性轉帳到期
	 * @param currentDate
	 * @return
	 */
	public List<TXNTWSCHPAY> findScheduleDueToEND(Date currentDate) {
		String due = DateTimeUtils.format("yyyyMMdd", currentDate);

		String sql = "" +
		" SELECT distinct {sch.*} FROM    " +
		" 	TXNTWSCHPAY sch" +
		" 	where sch.DPTDATE = ?  " +
		"   and  sch.DPPERMTDATE<>'' and  sch.DPTXSTATUS not in ('3','4') and  sch.LASTDATE <= ? " ;

		Query query = getSession().createQuery(sql);
		query.unwrap(NativeQuery.class).addEntity("sch", TXNTWSCHPAY.class);

		query.setParameter(0, due);
		query.setParameter(1, due);

		return ESAPIUtil.validStrList(query.getResultList());
	}
	
	
	/**
	 * 找出目前某一USER所設定的預約交易
	 * @param currentDate
	 * @return
	 */
	public List<TXNTWSCHPAY> findbyUserid(String DPUSERID) {

		String sql = "FROM TXNTWSCHPAY where DPUSERID=:dpuserid and DPTXSTATUS = '0' order by DPSCHID ";
		
		Query query = getQuery(sql);
		query.setParameter("dpuserid", DPUSERID);
		
		return ESAPIUtil.validStrList(query.getResultList());
	}
	
}