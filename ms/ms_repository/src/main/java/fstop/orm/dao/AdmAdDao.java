package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import fstop.orm.po.ADMAD;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;


@Slf4j
@Repository
@Transactional
public class AdmAdDao extends LegacyJpaRepository<ADMAD, Long> {
	protected Logger logger = Logger.getLogger(getClass());
	
	public List<ADMAD> getAll() {
		return find("FROM ADMAD ORDER BY ADID");
	}
	
	/**
	 * 查詢未過期的 ADMAD
	 * @return
	 */
	public List<ADMAD> findNotExpired() {
		String current = DateTimeUtils.format("yyyyMMddHHmm", new Date());
		current += "00";
		return find("FROM ADMAD WHERE (ADSDATE||ADSTIME <= ? and ADEDATE||ADETIME >= ?)   OR (ADSDATE||ADSTIME >= ? )" +
				" ORDER BY ADID", current, current, current);
	}
	
	/**
	 * 查詢未過期且未核放
	 * @return
	 */
	public List<ADMAD> findNotExpiredAndNotAudit() {
		String current = DateTimeUtils.format("yyyyMMddHHmm", new Date());
		current += "00";
		return find("FROM ADMAD WHERE ((ADSDATE||ADSTIME <= ? and ADEDATE||ADETIME >= ?)   OR (ADSDATE||ADSTIME >= ? )) " +
				" AND ADADSTAT='M' ORDER BY ADID", current, current, current);
	}
	
	/**
	 * 查詢未過期且已核放
	 * @return
	 */
	public List<ADMAD> findNotExpiredAndAudit() {
		String current = DateTimeUtils.format("yyyyMMddHHmm", new Date());
		current += "00";
		return find("FROM ADMAD WHERE (ADSDATE||ADSTIME <= ? and ADEDATE||ADETIME >= ?) " +
				" AND ADADSTAT='C' ORDER BY ADID", current, current);
	}
	
	/**
	 * 依據 ADADTYPE 查詢
	 * @return
	 */
	public List<ADMAD> findByAdtype(String adtype) { 
		return find("FROM ADMAD WHERE ADADTYPE=? ORDER BY ADID", adtype);
	}

	/**
	 * 查詢日期期間內, 會上版的廣告有哪些
	 * ADSDATE||ADSTIME 為上版的日期, yyyyMMddHHmmss
	 * @return
	 */
	public List<ADMAD> findByActiveInDateRange(String startDt, String endDt) { 
		return find("FROM ADMAD WHERE ADSDATE||ADSTIME >= ? and ADSDATE||ADSTIME <= ? ORDER BY ADID", startDt, endDt);
	}

}