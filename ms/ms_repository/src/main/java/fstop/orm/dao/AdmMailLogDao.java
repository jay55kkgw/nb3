package fstop.orm.dao;

import java.util.List;

import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.LegacyJpaRepository;

import fstop.orm.po.ADMMAILLOG;
import fstop.orm.po.TXNRECMAILLOGRELATION;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@Repository
@Transactional
public class AdmMailLogDao extends LegacyJpaRepository<ADMMAILLOG, Long> {
	protected Logger logger = Logger.getLogger(getClass());

	@Autowired
	AdmMailContentDao admMailContentDao;

	@Autowired
	TxnRecMailLogRelationDao txnRecMailLogRelationDao;

	
	public List<ADMMAILLOG> findByDateRange(String  ADUSERID, String ADSENDSTATUS, String startDateTime, String endDatetime) {
		if("".equals(ADSENDSTATUS)) {
			Query query = getQuery("FROM ADMMAILLOG WHERE ADUSERID='" + ADUSERID + "' and ADSENDTIME >= '" + startDateTime + "' and ADSENDTIME <= '" + endDatetime + "' ");
		
			return query.getResultList();

		}
		else {
			Query query = getQuery("FROM ADMMAILLOG WHERE ADUSERID = ? and ADSENDTIME >= ? and ADSENDTIME <= ? and ADSENDSTATUS = ? ", ADUSERID, startDateTime, endDatetime, ADSENDSTATUS);
		
			return query.getResultList();
		}
	}
	
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	//public void writeRecordNotice(ADMMAILLOG maillog, ADMMAILCONTENT mailcontent, TXNRECMAILLOGRELATION relation) {
	public void writeRecordNotice(ADMMAILLOG maillog,TXNRECMAILLOGRELATION relation) {
//		admMailContentDao.save(mailcontent);
//		maillog.setADMAILID(mailcontent.getADMAILID());
		save(maillog);
		relation.setADMAILLOGID(maillog.getADMAILLOGID());
		relation.setREC_TYPE(maillog.getADBATCHNO().substring(0, 2));
		txnRecMailLogRelationDao.save(relation);
	}
	
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	//public void writeNotice(ADMMAILLOG maillog, ADMMAILCONTENT mailcontent) {
	public void writeNotice(ADMMAILLOG maillog) {
//		admMailContentDao.save(mailcontent);
//		maillog.setADMAILID(mailcontent.getADMAILID());
		save(maillog);
	}
	
}
