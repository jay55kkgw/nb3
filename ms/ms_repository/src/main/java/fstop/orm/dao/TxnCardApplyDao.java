package fstop.orm.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.hibernate.query.NativeQuery;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.LegacyJpaRepository;

import fstop.exception.TopMessageException;
import fstop.model.Row;
import fstop.model.Rows;
import fstop.orm.po.TXNCARDAPPLY;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Transactional
public class TxnCardApplyDao extends LegacyJpaRepository<TXNCARDAPPLY, String> {
	protected Logger logger = Logger.getLogger(getClass());

	@SuppressWarnings( { "unused", "unchecked" })
	public List<TXNCARDAPPLY> findByLastDate(String lastDate) {
		
		String sql = "from TXNCARDAPPLY where LASTDATE <= ? AND STATUS='0' order by RCVNO ";
		
		Query query = getQuery(sql, lastDate);
		
		return query.getResultList();
	}
	@SuppressWarnings( { "unused", "unchecked" })
	public List<TXNCARDAPPLY> findByLastDate1(String lastDate) {
		
		String sql = "from TXNCARDAPPLY where LASTDATE = ? order by RCVNO ";
		
		Query query = getQuery(sql, lastDate);		
		
		return query.getResultList();
	}	
	@SuppressWarnings( { "unused", "unchecked" })
	public List<TXNCARDAPPLY> findByUID(String UID) {
		
		String sql = "from TXNCARDAPPLY where CPRIMID = ? order by CPRIMID ";
		
		Query query = getQuery(sql, UID);		
		
		return query.getResultList();
	}	
	/**
	 * 取得某一日的申請信用卡筆數
	 * @param currentDate
	 * @return
	 */
	public int countCreditRecord(Date currentDate) {

		String due = DateTimeUtils.format("yyyyMMdd", currentDate);
		String sql = "from TXNCARDAPPLY where LASTDATE = ? order by CPRIMID ";
		
		Query query = getQuery(sql, due);		
		return query.getResultList().size();
	}	
	/**
	 * 查詢某一筆案件編號
	 * @return
	 */
	public List<TXNCARDAPPLY> findByRCVNO(String rcvno) {
		return find("FROM TXNCARDAPPLY WHERE RCVNO = ?", rcvno);
	}
	/**
	 * 查詢客戶申請案件
	 * @return
	 */
	public List<TXNCARDAPPLY> findByCUSID(String CUSID) {
		return find("FROM TXNCARDAPPLY WHERE CPRIMID = ? order by RCVNO", CUSID);
	}
	/**
	 * 變更客戶申請狀態 1:審核中 2:補件 3:不核准 4:核准
	 * @return
	 */
	@SuppressWarnings({ "unused", "unchecked" })
	public TXNCARDAPPLY setStatus(String RCVNO ,String STATUS) {
		
		//String status = "1";
		int isUserExist = -1;
		TXNCARDAPPLY userdata = null;
		List<TXNCARDAPPLY> userdataList = null;
		try {
//			userdata = findById(StrUtils.trim(RCVNO));
			String sql = "from TXNCARDAPPLY where RCVNO = ? ";
			Query query = getQuery(sql, RCVNO );	
			
			userdataList=query.getResultList();
			
			if(userdataList.size()>0) {
				userdata= userdataList.get(0);
				isUserExist = 1;
			}else {
				log.error("CAN'T FIND MATCH DATA IN NB3");
				isUserExist = 0;
			}
			
		}
		catch(Exception e) {
			isUserExist = 0;
			log.error("Exception :"+e);
		}
		if(isUserExist == 1 ) {
			try {
				userdata.setSTATUS(STATUS);
				save(userdata);
				log.debug("Update TXNCARDAPPLY Success");
				return userdata;
			}catch (Exception e) {
				log.error("Save TXNCARDAPPLY Error");
			}
		}

		return null;
	}
	@SuppressWarnings( { "unused", "unchecked" })
	public List<String> findByLastDateRange(String startDate, String endDate) {
		
		String sql = "SELECT distinct LASTDATE from TXNCARDAPPLY where LASTDATE >= ? and LASTDATE <= ? order by LASTDATE ";
		Query query = getQuery(sql, startDate, endDate);			
		
		return query.getResultList();
	}
	
	public long getTxnCount(boolean isSuccess) {
		List<String> params = new ArrayList();
		String sdate = DateTimeUtils.getPrevMonthFirstDay();

		String stime = "000000";
		String edate = DateTimeUtils.getPrevMonthLastDay();

		String etime = "235959";
		params.add(sdate);
		params.add(stime);
		params.add(edate);
		params.add(etime);

		String[] values = new String[params.size()];
		params.toArray(values);

		String SQL = "SELECT COUNT(*) FROM TXNCARDAPPLY WHERE LASTDATE >= ? AND LASTTIME >= ? AND LASTDATE <= ? AND LASTTIME <= ?";
		if (isSuccess) {
			SQL = SQL + " AND STATUS = '4' ";
		}
		return ((Long) find(SQL, values).get(0)).longValue();
	}
	/*
	 * ADD 20180927
	 * 舊戶取得申請信用卡的歸戶行
	 * 
	 */
	@SuppressWarnings( { "unused", "unchecked" })
	public Rows findBranchNo(String UID) {
		
		Rows result = new Rows();
		List<TXNCARDAPPLY> all = null;
		List list = new ArrayList();		
		String sql = "from TXNCARDAPPLY where CPRIMID = ? AND BRNO !='' order by CPRIMID ";
		
		all=  find(sql,UID);
		logger.info("all size==>"+all.size());
		if(all.size() !=0) {
			for(int i=0;i<all.size();i++) {				
				list.add(i, all.get(i).getBRNO());//取得歸戶行
			}
			for(int j=0;j<list.size();j++) {			
				Map rm = new HashMap();
				rm.put("BRNO", list.get(j));										
				Row r = new Row(rm);
				result.addRow(r);
			}
		}else {
			Map rm = new HashMap();
			rm.put("BRNO", "");										
			Row r = new Row(rm);
			result.addRow(r);
		}

		return result;
	}
	
	public Rows addIPAndTimeAndVer(String lastDate) {
		logger.info("====addIPAndTimeAndVer method====");
		StringBuffer sbf = new StringBuffer();
		String sql = "SELECT * FROM(SELECT RCVNO,CPRIMCHNAME,CPRIMID,CARDNAME1,CARDNAME2,CARDNAME3,LASTDATE,LASTTIME,CHENAME,CTRYDESC,FDRSGSTAFF,BANKERNO,MEMO,IP,COUNT(*)as TIME,VERSION,ECERT FROM TXNCARDAPPLY GROUP BY RCVNO,CPRIMCHNAME,CPRIMID,CARDNAME1,CARDNAME2,CARDNAME3,LASTDATE,LASTTIME,CHENAME,CTRYDESC,FDRSGSTAFF,BANKERNO,MEMO,IP,VERSION,ECERT)AS A WHERE LASTDATE = ? order by RCVNO DESC";		
		logger.info("====sbf string===="+sbf.toString());
		Rows result = new Rows();
		
		try
		{								
			Query query = this.getSession().createQuery(sql);	
			query.setParameter(0, lastDate);
			List qresult = query.getResultList();
			Iterator it = qresult.iterator();
			while(it.hasNext()) 
			{				
				Object[] o = (Object[])it.next();
				Map rm = new HashMap();
				int t = -1;												
				rm.put("RCVNO", o[++t].toString());							
				rm.put("CPRIMCHNAME", (o[++t]==null ? "" : o[t].toString()));
				rm.put("CPRIMID", (o[++t]==null ? "" : o[t].toString()));
				rm.put("CARDNAME1", (o[++t]==null ? "" : o[t].toString()));
				rm.put("CARDNAME2", (o[++t]==null ? "" : o[t].toString()));
				rm.put("CARDNAME3", (o[++t]==null ? "" : o[t].toString()));
				rm.put("LASTDATE", (o[++t]==null ? "" : o[t].toString()));
				rm.put("LASTTIME", (o[++t]==null ? "" : o[t].toString()));
				rm.put("CHENAME", (o[++t]==null ? "" : o[t].toString()));
				rm.put("CTRYDESC", (o[++t]==null ? "" : o[t].toString()));
				rm.put("FDRSGSTAFF", (o[++t]==null ? "" : o[t].toString()));
				rm.put("BANKERNO", (o[++t]==null ? "" : o[t].toString()));
				rm.put("MEMO", (o[++t]==null ? "" : o[t].toString()));
				rm.put("IP", (o[++t]==null ? "" : o[t].toString()));
				rm.put("TIME", (o[++t]==null ? "" : o[t].toString()));
				rm.put("VERSION", (o[++t]==null ? "" : o[t].toString()));
				rm.put("ECERT", (o[++t]==null ? "" : o[t].toString()));
				
				Row r = new Row(rm);
				result.addRow(r);
			}
			
			return result;
		}
		catch(Exception e)
		{
			
			log.debug("error >> {}", e);
			throw(TopMessageException.create("ZX99"));
		}		
	}
	
	
	/*
	 * Add 20200914 報表 CCM913 資料
	 *  同IP 不同申請人線上申請信用卡
	*/
	@SuppressWarnings( { "unused", "unchecked" })
	public List<TXNCARDAPPLY> getSameIpAnotherApplicant(String startDate ,String lastDate) {
		logger.info("====getSameIpAnotherApplicant method====");
		
		StringBuffer nativeSql = new StringBuffer();
		//月報SQL 先註解
//		nativeSql.append(" SELECT * FROM TXNCARDAPPLY ");
//		nativeSql.append(" WHERE IP IN ( ");
//		nativeSql.append("  	SELECT IP FROM TXNCARDAPPLY WHERE IP IN  ");
//		nativeSql.append(" 		( ");
//		nativeSql.append("			SELECT IP  FROM TXNCARDAPPLY WHERE 1=1 AND LASTDATE >= ? AND LASTDATE <= ?  "); //Parameter1 & Parameter2
//		nativeSql.append("			AND STATUS = '4' "); // 狀態為核准件 
//		nativeSql.append(" 		) ");
//		nativeSql.append("  AND STATUS = '4' "); // 狀態為核准件 
//		nativeSql.append(" 	GROUP BY IP ");
//		nativeSql.append("     HAVING (COUNT(IP) > 1 ) ");
//		nativeSql.append("  ) ");
//		nativeSql.append(" AND STATUS = '4' "); 
//		nativeSql.append(" ORDER BY LASTDATE DESC, IP ");
		
		//日報SQL
		nativeSql.append(" SELECT * FROM TXNCARDAPPLY ");
		nativeSql.append(" WHERE IP IN ( ");
		nativeSql.append("  	SELECT IP FROM TXNCARDAPPLY WHERE IP IN  ");
		nativeSql.append(" 		( ");
		nativeSql.append("			SELECT IP  FROM TXNCARDAPPLY WHERE 1=1 AND LASTDATE >= ? AND LASTDATE <= ?  "); //Parameter1 & Parameter2
//		nativeSql.append("			AND STATUS = '4' "); // 狀態為核准件 
		nativeSql.append(" 		) ");
//		nativeSql.append("  AND STATUS = '4' "); // 狀態為核准件 
		nativeSql.append(" 	GROUP BY IP ");
		nativeSql.append("     HAVING (COUNT(IP) > 1 ) ");
		nativeSql.append("  ) ");
//		nativeSql.append(" AND STATUS = '4' "); 
		nativeSql.append(" ORDER BY LASTDATE DESC, IP ");
		
		
		logger.info("====SQL string====" + nativeSql.toString());
		
		//FIXME For Test 測試完後移除 
//		startDate = "20180201";
//		lastDate = "20200831";
		
		try {
			Query query = getSession().createSQLQuery(nativeSql.toString()); //不支援複雜subquery
			query.unwrap(NativeQuery.class).addEntity(TXNCARDAPPLY.class);
			
			query.setParameter(0, startDate);
			query.setParameter(1, lastDate);
			
			List<TXNCARDAPPLY> result = query.getResultList();
			
			return result;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw(TopMessageException.create("ZX99"));
		}
	}
}