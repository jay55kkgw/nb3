package fstop.orm.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.LegacyJpaRepository;

import fstop.orm.po.ITRN024;
import fstop.orm.po.ITRN030;
import fstop.orm.po.ITRN030_HISTORY;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Transactional
public class ItrN030_HISTORY_Dao extends LegacyJpaRepository<ITRN030_HISTORY, String> {
	protected Logger logger = Logger.getLogger(getClass());
	
	@SuppressWarnings("unchecked")
	public boolean doBatch(String date) {
		boolean doBatch = true;
		try {
			List<ITRN030_HISTORY> result = new ArrayList<ITRN030_HISTORY>();

			result = find("FROM ITRN030_HISTORY WHERE DATE = ?" , date);
			
			if(result.size()>0) {
				doBatch = false;
			}
			
		}catch (Exception e) {
			log.error("ITRN030_HISTORY ERROR , {}",e);
		}
		return doBatch;
	}
}
