package fstop.orm.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.orm.po.ADMMSGCODE;
import fstop.orm.po.IDGATE_TXN_STATUS;
import fstop.orm.po.IDGATE_TXN_STATUS_PK;
import lombok.extern.slf4j.Slf4j;

@Transactional(transactionManager = "nb3transactionManager", rollbackFor = { Throwable.class })
@Repository
@Slf4j
public class IdGate_Txn_StatusDao extends LegacyJpaRepository<IDGATE_TXN_STATUS, IDGATE_TXN_STATUS_PK> {

	@Transactional(readOnly = true)
	public String getSessionIdByTxnid(String idgateid , String txnid) {
		log.debug("idgateid = "+idgateid+" txnid = "+ txnid);
		String sessionId = "";
		String sql = "FROM IDGATE_TXN_STATUS WHERE IDGATEID=? AND TXNID= ? ";
		Query query = getQuery(sql,  idgateid , txnid );
		List<IDGATE_TXN_STATUS> qresult = query.getResultList();
		if (null == qresult) {
			qresult = new ArrayList();
		}
//		List<IDGATE_TXN_STATUS> qresult = find("FROM IDGATE_TXN_STATUS WHERE IDGATEID=? AND TXNID= ?  ",  idgateid , txnid );
		log.debug("qresult >>>>>{}",CodeUtil.toJson(qresult));
		if(qresult != null && !qresult.isEmpty()) {
			sessionId = qresult.get(0).getPks().getSESSIONID();
		}
		return sessionId;
	}
	
	@Transactional(readOnly = true)
	public String getSessionIdByEnctxnid(String idgateid , String enctxnid) {
		log.debug("idgateid = "+idgateid+" enctxnid = "+ enctxnid);
		String sessionId = "";
		String sql = "FROM IDGATE_TXN_STATUS WHERE IDGATEID=? AND ENCTXNID= ?  ";
		Query query = getQuery(sql,  idgateid , enctxnid );
		List<IDGATE_TXN_STATUS> qresult = query.getResultList();
		if (null == qresult) {
			qresult = new ArrayList();
		}
//		List<IDGATE_TXN_STATUS> qresult = find("FROM IDGATE_TXN_STATUS WHERE IDGATEID=? AND ENCTXNID= ?  ",  idgateid , enctxnid );
		log.debug("qresult >>>>>{}",CodeUtil.toJson(qresult));
		if(qresult != null && !qresult.isEmpty()) {
			sessionId = qresult.get(0).getPks().getSESSIONID();
		}
		return sessionId;
	}
	
	@Transactional(readOnly = true)
	public IDGATE_TXN_STATUS findBytxnid(String idgateid , String txnid) {
		log.debug("idgateid = "+idgateid+" enctxnid = "+ txnid);
		IDGATE_TXN_STATUS po = null;
		String sql = "FROM IDGATE_TXN_STATUS WHERE IDGATEID=? AND TXNID= ?  ";
		Query query = getQuery(sql,  idgateid , txnid );
		List<IDGATE_TXN_STATUS> qresult = query.getResultList();
		if (null == qresult) {
			qresult = new ArrayList();
		}
//		List<IDGATE_TXN_STATUS> qresult = find("FROM IDGATE_TXN_STATUS WHERE IDGATEID=? AND ENCTXNID= ?  ",  idgateid , enctxnid );
		if(qresult != null && !qresult.isEmpty()) {
			log.debug("qresult >>>>>{}",CodeUtil.toJson(qresult));
			po = qresult.get(0);
		}
		return po;
	}
}
