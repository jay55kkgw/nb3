package fstop.orm.dao;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.query.NativeQuery;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.joda.time.DateTime;
import org.owasp.esapi.SafeFile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.model.Rows;
import fstop.orm.po.ADMOPLOG;
import fstop.orm.po.TXNLOG;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Transactional
public class TxnLogDao extends LegacyJpaRepository<TXNLOG, String> {
	protected Logger logger = Logger.getLogger(getClass());

	public List findAllByPage(int start, int limit) {
		return (List) pagedQuery("FROM TXNLOG ", (start / limit) + 1, limit).getResult();
	}

	@SuppressWarnings("unchecked")
	public Page getFindByDateRangePageObjectForB201(String startdt, String enddt, String ADUSERID, String QTXTYPE,
			String ADEXCODE, int start, int limit, String LOGINTYPE) {
		List<String> condition = new ArrayList();
		List<String> params = new ArrayList();

		condition.add(" LASTDATE||LASTTIME >= ? and LASTDATE||LASTTIME <= ? ");
		params.add(startdt);
		params.add(enddt);

		if (StrUtils.isNotEmpty(ADUSERID)) {
			condition.add(" ADUSERID = ? ");
			params.add(ADUSERID);
		}

		if ("*".equals(QTXTYPE)) {
			condition.add(
					" ADOPID in (SELECT ADOPID FROM SYSOPGROUP WHERE ADOPGROUP='MENUGPN2' or ADOPGROUP='MENUGPN3' or ADOPGROUP='MENUGPN5' or ADOPGROUP='MENUGPN6' or ADOPGROUP='MENUGPN7' or ADOPGROUP='MENUGPN8' or ADOPGROUP='MENUGPN9')");
		} else if (StrUtils.isNotEmpty(QTXTYPE)) { // 這是 SYSGROUP 裡的 ADOPGROUP, 找出ADOPGROUP都為 QTXTYPE 的 ADOPID
			condition.add(
					" ADOPID in (SELECT ADOPID FROM SYSOPGROUP WHERE ADOPGROUP='" + StrUtils.trim(QTXTYPE) + "') ");
		}

		if (StrUtils.isNotEmpty(ADEXCODE)) {
			condition.add(" ADEXCODE = ? ");
			params.add(ADEXCODE);
		}
		// LOGINTYPE表示 交易來源， MB 或 NB
		// if(StrUtils.isNotEmpty(LOGINTYPE)){
		// condition.add(" LOGINTYPE = ? ");
		// params.add(LOGINTYPE);
		// }

		// condition.add(" length(ADLOGGING) > 0 ");

		String[] values = new String[params.size()];
		params.toArray(values);

		StringBuffer c = new StringBuffer();
		for (String s : condition) {
			if (c.length() > 0)
				c.append(" AND ");
			c.append(s);
		}
		/**
		 * 交易來源
		 */
		if (LOGINTYPE.equals("'','NB'")) {
			c.append(" AND  LOGINTYPE in " + "(" + LOGINTYPE + ") ");
		} else if (LOGINTYPE.equals("'MB'")) {
			c.append(" AND  LOGINTYPE in " + "(" + LOGINTYPE + ") ");
		} else {
			c.append(" AND  LOGINTYPE in " + "(" + LOGINTYPE + ") ");
		}
		return pagedQuery(
				"FROM TXNLOG " + "WHERE " + "" + c.toString() + " " + "" + "" + " ORDER BY LASTDATE || LASTTIME",
				(start / limit) + 1, limit, values);
	}

	@SuppressWarnings({ "unused", "unchecked" })
	public List<TXNLOG> findByTxnlogTime(String lastDate) {
		String sql ="";
		List<TXNLOG> rtnList = null;
		if(lastDate.length()==8) {
			
			sql = "select a.* from txnlog a,txnuser b,nb3sysop c where a.aduserid=b.dpsuerid and a.ADEXCODE='' and a.adopid in ('C016','C017','C021','C024','C031','C032','C033','C112') and a.adopid=c.adopid and a.lastdate = ?  and a.ADUSERIP like '10.%' order by a.aduserip";
			Query query = getSession().createSQLQuery(sql);
			query.unwrap(NativeQuery.class).addEntity("a", TXNLOG.class);

			query.setParameter(0, lastDate);
			rtnList =  query.getResultList();
			
		}else {
			String lastDateS = lastDate.substring(0,8);
			String lastDateE = lastDate.substring(8);
			sql = "select a.* from txnlog a,txnuser b,nb3sysop c where a.aduserid=b.dpsuerid and a.ADEXCODE='' and a.adopid in ('C016','C017','C021','C024','C031','C032','C033','C112') and a.adopid=c.adopid and a.lastdate >= ? and a.lastdate <= ? and a.ADUSERIP like '10.%' order by a.aduserip";
			Query query = getSession().createSQLQuery(sql);
			query.unwrap(NativeQuery.class).addEntity("a", TXNLOG.class);

			query.setParameter(0, lastDateS);
			query.setParameter(1, lastDateE);
			rtnList =  query.getResultList();
		}
		return rtnList;
		
	}

	@SuppressWarnings({ "unused", "unchecked" })
	public List<TXNLOG> findByFundReportM901(String lastDate, String BRH) {

		// String sql = "select a.* from txnlog a,txnuser b,sysop c where
		// a.aduserid=b.dpsuerid and a.ADEXCODE='' and a.adopid in
		// ('C016','C017','C021','C024','C031','C032','C033') and a.adopid=c.adopid and
		// a.lastdate like ? and a.ADUSERIP in (select ADBRANCHIP from ADMBRANCHIP where
		// ADBRANCHID = ?) order by a.aduserip";
		String sql = "select a.* from txnlog a,txnuser b,nb3sysop c where a.aduserid=b.dpsuerid and a.ADEXCODE='' and a.adopid in ('C016','C017','C021','C024','C031','C032','C033') and a.adopid=c.adopid and a.lastdate like '"
				+ lastDate + "' and a.ADUSERIP in (select ADBRANCHIP from ADMBRANCHIP where ADBRANCHID = '" + BRH
				+ "') order by a.aduserip";
		// log.debug("CCM901:"+sql);
		Query query = getSession().createSQLQuery(sql);
		query.unwrap(NativeQuery.class).addEntity("a", TXNLOG.class);

		// query.setParameter(0, lastDate);
		// query.setParameter(1, BRH);

		return query.getResultList();
	}

	@SuppressWarnings({ "unused", "unchecked" })
	public List<TXNLOG> findByNNBReportM902(String lastDate, String BRH) {
		String sql = "";
		if (BRH.indexOf("|") >= 0) // 部室超過一個網段的處理方式
		{
			String[] names = BRH.split("|");
			String connectSQL = "";
			int counter = 0;
			for (String name : names) {
				if (counter == 0)
					connectSQL = connectSQL + "a.aduserip like '" + name + "'";
				else if (counter > 0)
					connectSQL = connectSQL + " or a.aduserip like '" + name + "'";
				counter++;
			}
			sql = "select a.* from txnlog a,txnuser b,nb3sysop c where a.aduserid=b.dpsuerid and a.ADEXCODE='' and a.adopid in ('N174','N175','N177','N178','F001','F002','F003','N09001','N09002','N094','N09301','N09302','N070','N072','N073','N074','N075','N076','N077','N078','N079','N750A','N750B','N750C','N750E','N750F','N750H','N750G','C016','C017','C021','C024','C031','C032','C033') and a.adopid=c.adopid and a.aduserid not in (select cusid from admempinfo) and a.lastdate like ?  and ("
					+ connectSQL + ") order by a.aduserip,a.lastdate,a.lasttime";
			log.debug("CCM902:" + sql);
			Query query = getSession().createSQLQuery(sql);
			query.unwrap(NativeQuery.class).addEntity("a", TXNLOG.class);
			query.setParameter(0, lastDate);
			return query.getResultList();
		} else {
			sql = "select a.* from txnlog a,txnuser b,nb3sysop c where a.aduserid=b.dpsuerid and a.ADEXCODE='' and a.adopid in ('N174','N175','N177','N178','F001','F002','F003','N09001','N09002','N094','N09301','N09302','N070','N072','N073','N074','N075','N076','N077','N078','N079','N750A','N750B','N750C','N750E','N750F','N750H','N750G','C016','C017','C021','C024','C031','C032','C033') and a.adopid=c.adopid and a.aduserid not in (select cusid from admempinfo) and a.lastdate like ?  and a.ADUSERIP like ?  order by a.aduserip,a.lastdate,a.lasttime";
			log.debug("CCM902:" + sql);
			Query query = getSession().createSQLQuery(sql);
			query.unwrap(NativeQuery.class).addEntity("a", TXNLOG.class);
			query.setParameter(0, lastDate);
			query.setParameter(1, BRH);
			return query.getResultList();
		}
	}

	@SuppressWarnings({ "unused", "unchecked" })
	public List<TXNLOG> findByNNBReportM903(String lastDate, String BRH) {
		String sql = "";
		log.debug("CCM903:BRH = " + BRH);
		if (BRH.indexOf("|") >= 0) // 部室超過一個網段的處理方式
		{
			String[] names = BRH.split("|");
			String connectSQL = "";
			int counter = 0;
			for (String name : names) {
				if (counter == 0)
					connectSQL = connectSQL + "a.aduserip like '" + name + "'";
				else if (counter > 0)
					connectSQL = connectSQL + " or a.aduserip like '" + name + "'";
				counter++;
			}
			sql = "select a.* from txnlog a,txnuser b,nb3sysop c where a.aduserid=b.dpsuerid and a.ADEXCODE='' and a.adopid in ('N174','N175','N177','N178','F001','F002','F003','N09001','N09002','N094','N09301','N09302','N070','N072','N073','N074','N075','N076','N077','N078','N079','N750A','N750B','N750C','N750E','N750F','N750H','N750G','C016','C017','C021','C024','C031','C032','C033') and a.adopid=c.adopid and a.aduserid not in (select cusid from admempinfo) and a.lastdate like ?  and ("
					+ connectSQL + ") order by a.aduserip,a.lastdate,a.lasttime";
			log.debug("CCM903:" + sql);
			Query query = getSession().createSQLQuery(sql);
			query.unwrap(NativeQuery.class).addEntity("a", TXNLOG.class);
			query.setParameter(0, lastDate);
			return query.getResultList();
		} else {

			sql = "select a.* from txnlog a,txnuser b,nb3sysop c where a.aduserid=b.dpsuerid and a.ADEXCODE='' and a.adopid in ('N174','N175','N177','N178','F001','F002','F003','N09001','N09002','N094','N09301','N09302','N070','N072','N073','N074','N075','N076','N077','N078','N079','N750A','N750B','N750C','N750E','N750F','N750H','N750G','C016','C017','C021','C024','C031','C032','C033') and a.adopid=c.adopid and a.aduserid not in (select cusid from admempinfo) and a.lastdate like ?  and a.ADUSERIP like ?  order by a.aduserip,a.lastdate,a.lasttime";
			log.debug("CCM903:" + sql);
			Query query = getSession().createSQLQuery(sql);
			query.unwrap(NativeQuery.class).addEntity("a", TXNLOG.class);
			query.setParameter(0, lastDate);
			query.setParameter(1, BRH);
			return query.getResultList();
		}
	}

	@SuppressWarnings({ "unused", "unchecked" })
	public List<TXNLOG> findByNNBReportM902_HO(String sDate, String eDate, String BRH) {
		// log.debug("CCM902_HO: sDate= " + sDate + " eDate= " + eDate + " BRH= " +
		// BRH);
		String sql = "";
		if (BRH.indexOf("|") >= 0) // 部室超過一個網段的處理方式
		{
			String[] names = BRH.split("|");
			String connectSQL = "";
			int counter = 0;
			for (String name : names) {
				if (counter == 0)
					connectSQL = connectSQL + "a.aduserip like '" + name + "'";
				else if (counter > 0)
					connectSQL = connectSQL + " or a.aduserip like '" + name + "'";
				counter++;
			}
			sql = "select a.* from txnlog a,txnuser b,nb3sysop c where a.aduserid=b.dpsuerid and a.ADEXCODE='' and a.adopid in ('N174','N175','N177','N178','F001','F002','F003','N09001','N09002','N094','N09301','N09302','N070','N072','N073','N074','N075','N076','N077','N078','N079','N750A','N750B','N750C','N750E','N750F','N750H','N750G','C016','C017','C021','C024','C031','C032','C033') and a.adopid=c.adopid and a.aduserid not in (select cusid from admempinfo) and a.lastdate >= ? and a.lastdate <=? and ("
					+ connectSQL + ") order by a.aduserip,a.lastdate,a.lasttime";
			log.debug("CCM902_HO:" + sql);
			Query query = getSession().createSQLQuery(sql);
			query.unwrap(NativeQuery.class).addEntity("a", TXNLOG.class);
			query.setParameter(0, sDate);
			query.setParameter(1, eDate);
			return query.getResultList();
		} else {
			sql = "select a.* from txnlog a,txnuser b,nb3sysop c where a.aduserid=b.dpsuerid and a.ADEXCODE='' and a.adopid in ('N174','N175','N177','N178','F001','F002','F003','N09001','N09002','N094','N09301','N09302','N070','N072','N073','N074','N075','N076','N077','N078','N079','N750A','N750B','N750C','N750E','N750F','N750H','N750G','C016','C017','C021','C024','C031','C032','C033') and a.adopid=c.adopid and a.aduserid not in (select cusid from admempinfo) and a.lastdate >=? and a.lastdate <=? and a.ADUSERIP like ?  order by a.aduserip,a.lastdate,a.lasttime";
			log.debug("CCM902_HO:" + sql);
			Query query = getSession().createSQLQuery(sql);
			query.unwrap(NativeQuery.class).addEntity("a", TXNLOG.class);
			query.setParameter(0, sDate);
			query.setParameter(1, eDate);
			query.setParameter(2, BRH);
			return query.getResultList();
		}
	}

	public List findByDateRangeForB201(String startdt, String enddt, String ADUSERID, String QTXTYPE, String ADEXCODE,
			int start, int limit, String LOGINTYPE) {
		return (List) getFindByDateRangePageObjectForB201(startdt, enddt, ADUSERID, QTXTYPE, ADEXCODE, start, limit,
				LOGINTYPE).getResult();
	}

	public Long findByDateRangeCountForB201(String startdt, String enddt, String ADUSERID, String QTXTYPE,
			String ADEXCODE, int start, int limit, String LOGINTYPE) {
		return getFindByDateRangePageObjectForB201(startdt, enddt, ADUSERID, QTXTYPE, ADEXCODE, start, limit, LOGINTYPE)
				.getTotalCount();
	}

	// public List<TXNLOG> getLoginType(String loginType){
	// String sql = "SELECT LOGINTYPE FROM TXNLOG a WHERE LOGINTYPE =
	// '"+loginType+"' ";
	// SQLQuery query = getSession().createSQLQuery(sql).addEntity("a",
	// TXNLOG.class);
	// return query.list();
	// }
	public List<TXNLOG> getLoginType(String uid) {
		return find("FROM TXNLOG WHERE LOGINTYPE<>'' and ADUSERID=?", uid);
	}

	/**
	 * 查詢每月交易代號的數量
	 * 
	 * @param Adopid
	 * @param isSuccess
	 *            該交易是否成功
	 * @return
	 */
	public long getAdopidCount(String Adopid, boolean isSuccess) {
		List<String> params = new ArrayList<String>();
		String sdate = DateTimeUtils.getPrevMonthFirstDay();
		// String sdate = "20150901";
		String stime = "000000";
		String edate = DateTimeUtils.getPrevMonthLastDay();
		// String edate = "20150930";
		String etime = "235959";
		params.add(Adopid);
		params.add(sdate);
		params.add(stime);
		params.add(edate);
		params.add(etime);

		String[] values = new String[params.size()];
		params.toArray(values);

		String SQL = "SELECT COUNT(*) FROM TXNLOG WHERE ADOPID = ? AND LASTDATE >= ? AND LASTTIME >= ? AND LASTDATE <= ? AND LASTTIME <= ?";
		if (isSuccess) {
			SQL += " AND ADEXCODE = '' "; // ADEXCODE沒有值代表交易成功
		}
		return (Long) (find(SQL, values)).get(0);
	}
	
	public long getAdopidCountCK01Sp(boolean isSuccess) {
		
		/*
		 * 
		 * NB3的CK01 TXNLOG紀錄的模式不一樣
		 * 進CK01第一頁 記CK01 
		 * 結果頁 記CK01_1
		 * 
		 * 但舊網銀 是指記一個   進第一頁 如果失敗 記一次
		 * 
		 */
		
		
		List<String> params = new ArrayList<String>();
		String sdate = DateTimeUtils.getPrevMonthFirstDay();
		// String sdate = "20150901";
		String stime = "000000";
		String edate = DateTimeUtils.getPrevMonthLastDay();
		// String edate = "20150930";
		String etime = "235959";
		params.add("CK01%");
		params.add(sdate);
		params.add(stime);
		params.add(edate);
		params.add(etime);

		String[] values = new String[params.size()];
		params.toArray(values);

		String SQL = "SELECT COUNT(*) FROM TXNLOG WHERE ADOPID like ? AND LASTDATE >= ? AND LASTTIME >= ? AND LASTDATE <= ? AND LASTTIME <= ?";
		long part1 = (Long) (find(SQL, values)).get(0);
		
		
		
		return (Long) (find(SQL, values)).get(0);
	}


	/**
	 * 查詢來電分期約款簽屬完成申請與完成取消明細 N814 and N8141
	 * 
	 * @return
	 */
	public Rows getN814_N8141_Detail() {
		Rows result = new Rows();
		String SQL = "SELECT VARCHAR(TXNLOG.LASTDATE) AS LASTDATE, VARCHAR(TXNLOG.LASTTIME) AS LASTTIME, TXNUSER.DPUSERNAME, TXNUSER.DPSUERID, "
				+ "CASE WHEN ADOPID = 'N814' THEN 'V' ELSE '' END AS JOINSUCCESS, "
				+ "CASE WHEN ADOPID = 'N8141' THEN 'V' ELSE '' END AS CANCELSUCCESS " + "FROM TXNLOG, TXNUSER "
				+ "WHERE TXNLOG.ADUSERID = TXNUSER.DPSUERID AND " + "(ADOPID = 'N814' OR ADOPID = 'N8141') AND "
				+ "ADEXCODE = '' AND "
				+ "TXNLOG.LASTDATE >= ? AND TXNLOG.LASTTIME >= ? AND TXNLOG.LASTDATE <= ? AND TXNLOG.LASTTIME <= ? "
				+ "ORDER BY TXNLOG.LASTDATE ASC, TXNLOG.LASTTIME ASC ";

		Query query = getSession().createSQLQuery(SQL);
		String sdate = DateTimeUtils.getPrevMonthFirstDay();
		// String sdate = "20150901";
		String stime = "000000";
		String edate = DateTimeUtils.getPrevMonthLastDay();
		// String edate = "20150930";
		String etime = "235959";
		query.setParameter(0, sdate);
		query.setParameter(1, stime);
		query.setParameter(2, edate);
		query.setParameter(3, etime);

		// Stored XSS
		List<Object> qresult = query.getResultList();
		if (null == qresult) {
			qresult = new ArrayList();
		}
		qresult = ESAPIUtil.validList(qresult);
		// List qresult = query.getResultList();

		Iterator it = qresult.iterator();
		while (it.hasNext()) {
			Object[] o = (Object[]) it.next();
			Map rm = new HashMap();
			int t = -1;

			rm.put("LASTDATE", o[++t].toString());
			rm.put("LASTTIME", o[++t].toString());
			rm.put("DPUSERNAME", o[++t].toString());
			rm.put("DPSUERID", o[++t].toString());
			rm.put("JOINSUCCESS", o[++t].toString());
			rm.put("CANCELSUCCESS", o[++t].toString());

			// log.debug("@@@ Data report t =="+t);
			// log.debug("@@@ Data report o.length =="+o.length);
			// log.debug("@@@ Data report o-0 =="+o[0].toString());
			// log.debug("@@@ Data report o-1 =="+o[1].toString());
			// log.debug("@@@ Data report o-2 =="+o[2].toString());
			// log.debug("@@@ Data report o-3 =="+o[3].toString());
			// log.debug("@@@ Data report o-4 =="+o[4].toString());
			// log.debug("@@@ Data report o-5 =="+o[5].toString());

			Row r = new Row(rm);
			result.addRow(r);
		}

		return result;
	}

	/**
	 * 查詢行外電腦辦理基金申購(贖回)交易資料for財管部
	 * 
	 * @author Simon
	 * @param startDate,endDate
	 * @return Rows
	 */
	@SuppressWarnings({ "unused", "unchecked" })
	public Rows findByFundIPReportforH68(String startDate, String endDate) {
		Rows result = new Rows();
		String sql = "select distinct b.aduserid,c.aduserip from (select count(*) cnt, aduserip from (select distinct aduserid,aduserip from txnlog where aduserip not like '10.%' and adopid in ('C016','C017','C021','C024') and aduserid <> 'BATCH' and adexcode = '' and lastdate >= ? and lastdate <= ?) a group by aduserip having count(*) > 4) c left join txnlog b on b.aduserip = c.aduserip where b.adopid in ('C016','C017','C021','C024') and b.aduserid <> 'BATCH' and b.adexcode = '' and b.lastdate >= ? and b.lastdate <= ?";

		// for testing
		// String sql = "select distinct b.aduserid,c.aduserip from (select count(*)
		// cnt, aduserip from (select distinct aduserid,aduserip from txnlog where
		// aduserip not like '10.%' and adopid in ('C016','C017','C021','C024') and
		// aduserid <> 'BATCH' and adexcode = '' and lastdate >= ? and lastdate <= ?) a
		// group by aduserip) c left join txnlog b on b.aduserip = c.aduserip where
		// b.adopid in ('C016','C017','C021','C024') and b.aduserid <> 'BATCH' and
		// b.adexcode = '' and b.lastdate >= ? and b.lastdate <= ?";

		Query query = getSession().createSQLQuery(sql);
		log.debug("startDate:" + startDate + "；endDate:" + endDate);
		log.debug("FundIP68:" + sql);
		query.setParameter(0, startDate);
		query.setParameter(1, endDate);
		query.setParameter(2, startDate);
		query.setParameter(3, endDate);

		// Stored XSS
		List<Object> qresult = query.getResultList();
		if (null == qresult) {
			qresult = new ArrayList();
		}
		qresult = ESAPIUtil.validList(qresult);
		// List qresult = query.getResultList();

		Iterator it = qresult.iterator();
		while (it.hasNext()) {
			Object[] o = (Object[]) it.next();
			Map rm = new HashMap();
			int t = -1;

			rm.put("aduserid", o[++t].toString());
			rm.put("aduserip", o[++t].toString());

			// log.debug("@@@ FundIPData68 report t =="+t);
			// log.debug("@@@ FundIPData68 report o.length =="+o.length);
			// log.debug("@@@ FundIPData68 report o-0 =="+o[0].toString());
			// log.debug("@@@ FundIPData68 report o-1 =="+o[1].toString());
			Row r = new Row(rm);
			result.addRow(r);
		}

		return result;
	}

	/**
	 * 查詢行外電腦辦理基金申購、轉換、贖回、定期投資約定變更交易資料for財富管理系統
	 * 
	 * @author Sox
	 * @param startDate,endDate
	 * @return Rows
	 */
	@SuppressWarnings({ "unused", "unchecked" })
	public Rows findByFundIPReportforH681(String startDate, String endDate) {
		Rows result = new Rows();
		String sql = "select distinct b.aduserid,c.aduserip from (select count(*) cnt, aduserip from (select distinct aduserid,aduserip from txnlog where aduserip not like '10.%' and adopid in ('C016','C017','C021','C024','C111','InvestAttr') and aduserid <> 'BATCH' and adexcode = '' and lastdate >= ? and lastdate <= ?) a group by aduserip having count(*) > 1) c left join txnlog b on b.aduserip = c.aduserip where b.adopid in ('C016','C017','C021','C024','C111','InvestAttr') and b.aduserid <> 'BATCH' and b.adexcode = '' and b.lastdate >= ? and b.lastdate <= ?";

		Query query = getSession().createSQLQuery(sql);
		log.debug(ESAPIUtil.vaildLog("startDate:" + startDate + "；endDate:" + endDate));
		log.debug(ESAPIUtil.vaildLog("FundIP:" + sql));
		query.setParameter(0, startDate);
		query.setParameter(1, endDate);
		query.setParameter(2, startDate);
		query.setParameter(3, endDate);

		// Stored XSS
		List<Object> qresult = query.getResultList();
		if (null == qresult) {
			qresult = new ArrayList();
		}
		qresult = ESAPIUtil.validList(qresult);
		// List qresult = query.getResultList();

		Iterator it = qresult.iterator();
		while (it.hasNext()) {
			Object[] o = (Object[]) it.next();
			Map rm = new HashMap();
			int t = -1;

			rm.put("aduserid", o[++t].toString());
			rm.put("aduserip", o[++t].toString());

			Row r = new Row(rm);
			result.addRow(r);
		}

		return result;
	}

	/**
	 * 查詢行外電腦辦理基金申購(贖回)交易資料for稽核處
	 * 
	 * @author Simon
	 * @param startDate,endDate
	 * @return Rows
	 */
	@SuppressWarnings({ "unused", "unchecked" })
	public Rows findByFundIPReportforH11(String startDate, String endDate) {
		Rows result = new Rows();
		String sql = ""
				+ "select distinct b.aduserid,c.aduserip,'*' mark from (select count(*) cnt, aduserip from (select distinct aduserid,aduserip from txnlog where aduserip not like '10.%' and adopid in ('C016','C017','C021','C024') and aduserid <> 'BATCH' and adexcode = '' and lastdate >= ? and lastdate <= ?) a group by aduserip having count(*) > 4) c left join txnlog b on b.aduserip = c.aduserip where b.adopid in ('C016','C017','C021','C024') and b.aduserid <> 'BATCH' and b.adexcode = '' and b.lastdate >= ? and b.lastdate <= ? "
				+ "union all "
				+ "select distinct b.aduserid,c.aduserip,' ' mark from (select count(*) cnt, aduserip from (select distinct aduserid,aduserip from txnlog where aduserip not like '10.%' and adopid in ('C016','C017','C021','C024') and aduserid <> 'BATCH' and adexcode = '' and lastdate >= ? and lastdate <= ?) a group by aduserip having count(*) < 5) c left join txnlog b on b.aduserip = c.aduserip where b.adopid in ('C016','C017','C021','C024') and b.aduserid <> 'BATCH' and b.adexcode = '' and b.lastdate >= ? and b.lastdate <= ? ";

		// for testing
		// "select distinct b.aduserid,c.aduserip,'*' as mark from (select count(*) cnt,
		// aduserip from (select distinct aduserid,aduserip from txnlog where aduserip
		// not like '10.%' and adopid like 'C%' and aduserid <> 'BATCH') a group by
		// aduserip having count(*) > 4) c left join txnlog b on b.aduserip = c.aduserip
		// where b.adopid like 'C%' and b.aduserid <> 'BATCH' " +
		// "union all " +
		// "select distinct b.aduserid,c.aduserip,' ' as mark from (select count(*) cnt,
		// aduserip from (select distinct aduserid,aduserip from txnlog where aduserip
		// not like '10.%' and adopid like 'C%' and aduserid <> 'BATCH') a group by
		// aduserip having count(*) < 5) c left join txnlog b on b.aduserip = c.aduserip
		// where b.adopid like 'C%' and b.aduserid <> 'BATCH' ";

		Query query = getSession().createSQLQuery(sql);
		log.debug(ESAPIUtil.vaildLog("startDate:" + startDate + "；endDate:" + endDate));
		log.debug(ESAPIUtil.vaildLog("FundIP11_NB3:" + sql));
		query.setParameter(0, startDate);
		query.setParameter(1, endDate);
		query.setParameter(2, startDate);
		query.setParameter(3, endDate);
		query.setParameter(4, startDate);
		query.setParameter(5, endDate);
		query.setParameter(6, startDate);
		query.setParameter(7, endDate);

		// Stored XSS
		List<Object> qresult = query.getResultList();
		if (null == qresult) {
			qresult = new ArrayList();
		}
		qresult = ESAPIUtil.validList(qresult);
		// List qresult = query.getResultList();

		Iterator it = qresult.iterator();
		while (it.hasNext()) {
			Object[] o = (Object[]) it.next();
			Map rm = new HashMap();
			int t = -1;

			rm.put("aduserid", o[++t].toString());
			rm.put("aduserip", o[++t].toString());
			rm.put("mark", o[++t].toString());

			// log.debug("@@@ FundIPData11 report t =="+t);
			// log.debug("@@@ FundIPData11 report o.length =="+o.length);
			// log.debug("@@@ FundIPData11 report o-0 =="+o[0].toString());
			// log.debug("@@@ FundIPData11 report o-1 =="+o[1].toString());
			// log.debug("@@@ FundIPData11 report o-2 =="+o[2].toString());
			Row r = new Row(rm);
			result.addRow(r);
		}

		return result;
	}

	/**
	 * 查詢行外電腦辦理基金申購(贖回)交易資料
	 * 
	 * @author Simon
	 * @param userid,userip,startDate,endDate
	 * @return List
	 */
	@SuppressWarnings({ "unused", "unchecked" })
	public List<TXNLOG> findByFundIPDataReport(String userid, String userip, String startDate, String endDate) {

		String sql = "select * from txnlog where aduserid = ? and aduserip = ? and lastdate >= ? and lastdate <= ? and adopid in ('C016','C017','C021','C024') and aduserid <> 'BATCH' and adexcode = '' order by aduserip, lastdate, lasttime";

		// for testing
		// String sql = "select * from txnlog where aduserid = ? and aduserip = ? and
		// adopid in ('C016','C017','C021','C024') and aduserid <> 'BATCH' and adexcode
		// = '' order by aduserip, lastdate, lasttime";

		Query query = getSession().createSQLQuery(sql);
		query.unwrap(NativeQuery.class).addEntity("a", TXNLOG.class);
		// log.debug("startDate:"+startDate+"；endDate:"+endDate+"；userid:"+userid+"；userip:"+userip);
		log.debug("FundIPData:" + sql);
		query.setParameter(0, userid);
		query.setParameter(1, userip);
		query.setParameter(2, startDate);
		query.setParameter(3, endDate);

		return ESAPIUtil.validStrList(query.getResultList());
	}

	/**
	 * 查詢行外電腦辦理基金申購、轉換、贖回、定期投資約定變更交易資料
	 * 
	 * @author Sox
	 * @param userid,userip,startDate,endDate
	 * @return List
	 */
	@SuppressWarnings({ "unused", "unchecked" })
	public List<TXNLOG> findByFundIPDataReport1(String userid, String userip, String startDate, String endDate) {

		String sql = "select * from txnlog where aduserid = ? and aduserip = ? and lastdate >= ? and lastdate <= ? and adopid in ('C016','C017','C021','C024','C111','InvestAttr') and aduserid <> 'BATCH' and adexcode = '' and aduserip <> '' order by aduserip, lastdate, lasttime";
		// for testing
		// String sql = "select * from txnlog where aduserid = ? and aduserip = ? and
		// adopid in ('C016','C017','C021','C024') and aduserid <> 'BATCH' and adexcode
		// = '' order by aduserip, lastdate, lasttime";

		Query query = getSession().createSQLQuery(sql);
		query.unwrap(NativeQuery.class).addEntity("a", TXNLOG.class);
		// log.debug("startDate:"+startDate+"；endDate:"+endDate+"；userid:"+userid+"；userip:"+userip);
		log.debug("FundIPData1:" + sql);
		query.setParameter(0, userid);
		query.setParameter(1, userip);
		query.setParameter(2, startDate);
		query.setParameter(3, endDate);

//		return ESAPIUtil.validStrList(query.getResultList());
		return query.getResultList();
	}

	public void getTxnLog(String srcpath, String despath, String statrDate, int totelCount) {
		List<Date> dateList = new LinkedList();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Date dt;
		Date temp;
		try {
			dt = sdf.parse(statrDate);
			temp = dt;
			dateList.add(dt);
			for (int i = 0; i < totelCount; i++) {
				Calendar rightNow = Calendar.getInstance();
				rightNow.setTime(temp);
				rightNow.add(Calendar.MONTH, 1);
				Date dt1 = rightNow.getTime();
				temp = dt1;
				dateList.add(dt1);
			}
		} catch (Exception e) {

		}
		Map<String, String> jsonMap = new HashMap();
		String TRANSCODE = "";
		String FUNDLNAME = "";
		List<String> documentList = new LinkedList();
		try {
			BufferedReader br = null;
			FileReader fr = null;
			for (int j = 0; j < dateList.size(); j++) {
				br = new BufferedReader(new FileReader(
						srcpath + "/" + DateTimeUtils.getDateShort(dateList.get(j)).substring(0, 6) + ".txt"));
				String thisLine = null;
				String[] strArray = null;
				while ((thisLine = br.readLine()) != null) {
					try {
						jsonMap = JSONUtils.json2map(thisLine);
						TRANSCODE = jsonMap.get("TRANSCODE") == null ? "N" : jsonMap.get("TRANSCODE");
						FUNDLNAME = jsonMap.get("FUNDLNAME") == null ? "N" : jsonMap.get("FUNDLNAME");
						documentList.add("," + TRANSCODE + "," + FUNDLNAME);
					} catch (Exception e) {
						logger.warn("json parse error:" + e.getMessage());
						documentList.add("," + "" + "," + "");
						continue;
					}
				}
				String validatePath = ESAPIUtil.vaildPathTraversal2(despath,
						DateTimeUtils.getDateShort(dateList.get(j)).substring(0, 6) + "_OUTPUT.txt");
				// File saveFile = new SafeFile(despath
				// + "/"
				// + DateTimeUtils.getDateShort(dateList.get(j))
				// .substring(0, 6) + "_OUTPUT.txt");
				File saveFile = new SafeFile(validatePath);
				saveFile.setWritable(true, true);
				saveFile.setReadable(true, true);
				saveFile.setExecutable(true, true);
				// FileWriter fwriter = new FileWriter(saveFile);
				// Path p = saveFile.toPath();
				for (String txnLog : documentList) {
					logger.warn(txnLog);
					// fwriter.write(txnLog + "\r\n");
					// Files.write(p, (txnLog + "\r\n").getBytes("BIG5"));
					BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(saveFile));
					byte[] strToBytes = (txnLog + "\r\n").getBytes("BIG5");
					outputStream.write(strToBytes);
					outputStream.close();
				}
				// fwriter.flush();
				// fwriter.close();
				documentList = new LinkedList();
			}
		} catch (Exception e) {
			logger.warn("file io exception:" + e.getMessage());
		}
	}

	/**
	 * 查詢基金申購、轉換、贖回、定期投資約定變更交易資料for金管會查核
	 * 
	 * @author Sox
	 * @param startDate,endDate
	 * @return Rows
	 */
	@SuppressWarnings({ "unused", "unchecked" })
	public Rows findByFundIPReportforH682(String startDate, String endDate) {
		Rows result = new Rows();
		String sql = "select distinct aduserid,aduserip from txnlog where adopid in ('C016','C017','C021','C024','C111') and aduserid <> 'BATCH' and adexcode = '' and lastdate >= ? and lastdate <= ?";

		Query query = getSession().createSQLQuery(sql);
		log.debug("startDate:" + startDate + "；endDate:" + endDate);
		log.debug("FundIP:" + sql);
		query.setParameter(0, startDate);
		query.setParameter(1, endDate);

		// Stored XSS
		List<Object> qresult = query.getResultList();
		if (null == qresult) {
			qresult = new ArrayList();
		}
		qresult = ESAPIUtil.validList(qresult);
		// List qresult = query.getResultList();

		Iterator it = qresult.iterator();
		while (it.hasNext()) {
			Object[] o = (Object[]) it.next();
			Map rm = new HashMap();
			int t = -1;

			rm.put("aduserid", o[++t].toString());
			rm.put("aduserip", o[++t].toString());

			Row r = new Row(rm);
			result.addRow(r);
		}

		return result;
	}

	public MVH getTxnLogNum(String stDate, String edDate) {

		logger.debug("stDate:=====>" + stDate + "  edDate:" + edDate + " <====");
		MVHImpl mvhresult = null;

		Rows result = new Rows();
//		StringBuffer sql = new StringBuffer();
//		sql.append(
//				" SELECT tur.ADBRANCHID AS ADBRANCHID,tur.DPUSERNAME AS DPUSERNAME,tl.ADUSERID AS ADUSERID,tl.ADTXACNO AS ADTXACNO,tl.LASTDATE AS LASTDATE,left(tl.LASTTIME,4) AS LTIME,tl.ADOPID AS ADOPID,tl.LOGINTYPE AS LOGINTYPE,COUNT(*) AS TIME ");
//		sql.append(" FROM TXNLOG tl,TXNUSER tur ");
//		//sql.append(" WHERE tl.ADUSERID=tur.DPSUERID AND tl.LASTDATE >=? AND tl.LASTDATE <=? AND tl.ADOPID !='' AND ");
//		sql.append(" WHERE tl.ADUSERID=tur.DPSUERID AND tl.LASTDATE >=? AND tl.LASTDATE <=? AND ");
//		sql.append(" tl.ADOPID NOT IN(SELECT ADOPID FROM NB3SYSOP WHERE ADGPPARENT ='GPE' ORDER BY ADOPID ASC) ");
//		sql.append(
//				" GROUP BY tur.ADBRANCHID,tur.DPUSERNAME,tl.ADUSERID,tl.ADTXACNO,tl.LASTDATE,left(tl.LASTTIME,4),tl.ADOPID,tl.LOGINTYPE ");
//		sql.append(" HAVING COUNT(*) >= 4 ");
//		sql.append(" ORDER BY tur.ADBRANCHID,tl.LASTDATE,LTIME ASC ");
		String sql = "SELECT tur.ADBRANCHID AS ADBRANCHID,tur.DPUSERNAME AS DPUSERNAME,tl.ADUSERID AS ADUSERID,tl.ADTXACNO AS ADTXACNO,tl.LASTDATE AS LASTDATE,left(tl.LASTTIME,4) AS LTIME,tl.ADOPID AS ADOPID,tl.LOGINTYPE AS LOGINTYPE,COUNT(*) AS TIME FROM TXNLOG tl,TXNUSER tur WHERE tl.ADUSERID=tur.DPSUERID  AND tl.LASTDATE >=? AND tl.LASTDATE <=?  AND tl.ADREQTYPE<>'B' AND tl.ADOPID<>'' AND tl.ADOPID NOT IN(SELECT ADOPID FROM NB3SYSOP WHERE ADGPPARENT ='GPE' ORDER BY ADOPID ASC) GROUP BY tur.ADBRANCHID,tur.DPUSERNAME,tl.ADUSERID,tl.ADTXACNO,tl.LASTDATE,left(tl.LASTTIME,4),tl.ADOPID,tl.LOGINTYPE HAVING COUNT(*) >= 4 ORDER BY tur.ADBRANCHID,tl.LASTDATE,LTIME ASC";		
		

		try {
			Query query = getSession().createSQLQuery(sql.toString());
			query.setParameter(0, stDate);// 起日
			query.setParameter(1, edDate);// 迄日
			// query.setParameter(2, branchNo);//迄日

			query.unwrap(NativeQuery.class).addScalar("ADBRANCHID", StringType.INSTANCE);
			query.unwrap(NativeQuery.class).addScalar("DPUSERNAME", StringType.INSTANCE);
			query.unwrap(NativeQuery.class).addScalar("ADUSERID", StringType.INSTANCE);
			query.unwrap(NativeQuery.class).addScalar("ADTXACNO", StringType.INSTANCE);
			query.unwrap(NativeQuery.class).addScalar("LASTDATE", StringType.INSTANCE);
			query.unwrap(NativeQuery.class).addScalar("LTIME", StringType.INSTANCE);
			query.unwrap(NativeQuery.class).addScalar("ADOPID", StringType.INSTANCE);
			query.unwrap(NativeQuery.class).addScalar("LOGINTYPE", StringType.INSTANCE);
			query.unwrap(NativeQuery.class).addScalar("TIME", StringType.INSTANCE);

			List qresult = ESAPIUtil.validStrList(query.getResultList());
			Iterator it = qresult.iterator();
			while (it.hasNext()) {
				Object[] o = (Object[]) it.next();
				Map rm = new HashMap();
				int t = -1;
				rm.put("ADBRANCHID", o[++t].toString());
				rm.put("DPUSERNAME", (o[++t] == null ? "" : o[t].toString()));
				rm.put("ADUSERID", (o[++t] == null ? "" : o[t].toString()));
				rm.put("ADTXACNO", (o[++t] == null ? "" : o[t].toString()));
				rm.put("LASTDATE", (o[++t] == null ? "" : o[t].toString()));
				rm.put("LTIME", (o[++t] == null ? "" : o[t].toString()));
				rm.put("ADOPID", (o[++t] == null ? "" : o[t].toString()));
				rm.put("LOGINTYPE", (o[++t] == null ? "" : o[t].toString()));
				rm.put("TIME", (o[++t] == null ? "" : o[t].toString()));
				Row r = new Row(rm);
				result.addRow(r);
			}

			Rows newrows = new Rows();
			for (int i = 0; i < result.getSize(); i++) {
				newrows.addRow(result.getRow(i));
			}
			mvhresult = new MVHImpl(newrows);
			return mvhresult;
		} catch (Exception e) {
			throw (TopMessageException.create("ZX99"));
		}
	}

	public void writeTxnLog(Map params, MVHImpl req) {
		TXNLOG po = null;
		try {
			String acn = req.getFlatValues().get("ACN");
			String adexcode = (req.getFlatValues().get("TOPMSG").equals(acn)) ? "" : req.getFlatValues().get("TOPMSG");
			log.debug("TXNLOG ACN >>>" + acn);
			log.debug("TXNLOG ADEXCODE >>>" + adexcode);
			po = new TXNLOG();
			log.debug("data>>>" + req.getFlatValues());
			po.setADTXNO(UUID.randomUUID().toString());
			po.setADUSERID((String) params.get("CUSIDN"));
			po.setADOPID((String) params.get("ADOPID"));
			po.setADUSERIP((String) params.get("ADUSERIP"));
			po.setADGUID((String) params.get("ADGUID"));
			po.setFGTXWAY((String) params.get("FGTXWAY"));
			po.setLOGINTYPE("NB");
			po.setADTXACNO((String) req.getFlatValues().get("ACN"));
			po.setADEXCODE(adexcode);
			// po.setADCONTENT(CodeUtil.toJson(data));
			po.setADCONTENT(CodeUtil.toJsonCustom(req.getFlatValues()));
			po.setLASTDATE(new DateTime().toString("yyyyMMdd"));
			po.setLASTTIME(new DateTime().toString("HHmmss"));
			// TODO 等欄位確定開之後，在打開
			po.setPSTIMEDIFF((String) params.get("PSTIMEDIFF"));
			log.debug(ESAPIUtil.vaildLog("TXNLOG PO >>>" + po.toString()));
			save(po);
		} catch (Exception e) {
			log.error(ESAPIUtil.vaildLog("writeTxnLog ERROR >>>" + e));
		}
	}

	// 抓取交易紀錄資料
	public List<TXNLOG> getTxnLogforcrm(String lastDate) {

		String sql = "select * from txnlog where lastdate = ? WITH UR";

		Query query = getSession().createSQLQuery(sql);
		query.unwrap(NativeQuery.class).addEntity("TXNLOG", TXNLOG.class);
		query.setParameter(0, lastDate);

		return ESAPIUtil.validStrList(query.getResultList());
	}
	
	/**
	 * 查詢USER 常用交易TOP5
	 * @param ADUSERID 
	 * @return
	 */
	public List<Map<String,String>> get3MTop10(String adusrid) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		String yesterday = DateTimeUtils.format("yyyyMMdd", cal.getTime());
		cal.add(Calendar.MONTH, -3);
		String before3month = DateTimeUtils.format("yyyyMMdd", cal.getTime());
		String SQL = "SELECT COUNT(ADOPID) AS ADOPID_NUM, ADOPID FROM TXNLOG " + 
				"WHERE ADUSERID = ? AND LOGINTYPE = 'NB' AND ADOPID <> 'A106' AND ADREQTYPE <> 'B' " + 
				"AND LASTDATE >= ? AND LASTDATE <= ? "+
				"GROUP BY ADOPID " + 
				"ORDER BY ADOPID_NUM DESC "+
				"LIMIT 10";
		Query query = getSession().createSQLQuery(SQL);
		query.setParameter(0, adusrid);
		query.setParameter(1, before3month);
		query.setParameter(2, yesterday);
		query.unwrap(NativeQuery.class).addScalar("ADOPID_NUM", StringType.INSTANCE);
		query.unwrap(NativeQuery.class).addScalar("ADOPID", StringType.INSTANCE);
		List qresult = ESAPIUtil.validStrList(query.getResultList());
		List<Map<String,String>>returnList = new ArrayList<Map<String,String>>();
		Iterator it = qresult.iterator();
		while (it.hasNext()) {
			Object[] o = (Object[]) it.next();
			Map rm = new HashMap();
			int t = -1;
			rm.put("ADOPID_NUM", o[++t].toString());
			rm.put("ADOPID", (o[++t] == null ? "" : o[t].toString()));
			returnList.add(rm);
		}
		return returnList;
	}
	
	/*
	 * Add 20201216 報表 CNM416 資料
	 *  同一申請人(ID)  不同IP交易資料
	*/
	public List<TXNLOG> getSameIDAnotherIPFundReport(String startDate , String lastDate) {
		log.info("====getSameIpAnotherApplicant method====");
		StringBuffer nativeSql = new StringBuffer();
		nativeSql.append(" SELECT * FROM TXNLOG WHERE ADUSERID IN ");
		nativeSql.append("   (SELECT ADUSERID FROM TXNLOG WHERE ADUSERID IN ");
		nativeSql.append("        (SELECT ADUSERID FROM TXNLOG WHERE 1=1 ");
		nativeSql.append("           AND adopid in ('C016','C017','C021','C024','C111','InvestAttr') ");
		nativeSql.append("           AND aduserip not like '10.%' ");
		nativeSql.append("           AND aduserid <> 'BATCH' AND adexcode = '' AND LASTDATE >= ? AND LASTDATE <= ? ) ");
		nativeSql.append("      AND adopid in ('C016','C017','C021','C024','C111','InvestAttr') ");
		nativeSql.append("      AND aduserip not like '10.%' ");
		nativeSql.append("      AND aduserid <> 'BATCH' AND adexcode = '' AND LASTDATE >= ? AND LASTDATE <= ? ");
		nativeSql.append("    GROUP BY ADUSERID ");
		nativeSql.append("    HAVING COUNT(ADUSERID) > 1) ");
		nativeSql.append(" AND adopid in ('C016','C017','C021','C024','C111','InvestAttr') ");
		nativeSql.append(" AND aduserip not like '10.%' ");
		nativeSql.append(" AND aduserid <> 'BATCH' AND adexcode = '' AND LASTDATE >= ? AND LASTDATE <= ? ");
		nativeSql.append(" ORDER BY LASTDATE DESC,LASTTIME ");
		
		Query query = getSession().createSQLQuery(nativeSql.toString());
		query.setParameter(0, startDate);
		query.setParameter(1, lastDate);
		query.setParameter(2, startDate);
		query.setParameter(3, lastDate);
		query.setParameter(4, startDate);
		query.setParameter(5, lastDate);
		query.unwrap(NativeQuery.class).addEntity("TXNLOG", TXNLOG.class);
		
		List qresult = ESAPIUtil.validStrList(query.getResultList());
		return qresult;
	}
}
