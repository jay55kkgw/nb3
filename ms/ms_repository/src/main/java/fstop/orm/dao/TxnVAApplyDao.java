package fstop.orm.dao;

import java.util.List;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.TXNVAAPPLY;


@Slf4j
@Repository
@Transactional
public class TxnVAApplyDao extends LegacyJpaRepository<TXNVAAPPLY, Long> {

	public List<TXNVAAPPLY> getAll() {
		return find("FROM TXNVAAPPLY ORDER BY LOGID");
	}

	public void deleteAll() {
		this.getSession().createSQLQuery("DELETE FROM TXNVAAPPLY").executeUpdate();
	}
}
