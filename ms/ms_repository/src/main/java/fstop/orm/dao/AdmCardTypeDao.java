package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import fstop.orm.po.ADMCARDTYPE;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Slf4j
@Repository
@Transactional
public class AdmCardTypeDao extends LegacyJpaRepository<ADMCARDTYPE, String> {
	protected Logger logger = Logger.getLogger(getClass());
	
	
	public List<ADMCARDTYPE> findByTypeNameDesc(String ADTYPEID, String ADTYPENAME, String ADTYPEDESC) {
		
		return find("FROM ADMCARDTYPE WHERE ADTYPEID = ? and  ADTYPENAME = ? and ADTYPEDESC = ?", ADTYPEID, ADTYPENAME, ADTYPEDESC);
	}
	
}
