package fstop.orm.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.hibernate.exception.LockAcquisitionException;
import org.hibernate.query.NativeQuery;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;
import com.netbank.domain.orm.core.SessionAdapter;
import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.model.Rows;
import fstop.orm.po.TXNFXSCHPAYDATA;
import fstop.orm.po.TXNUSER;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository

@Transactional(
		transactionManager="nb3transactionManager",
        rollbackFor = {Throwable.class}
)
public class TxnUserDao extends LegacyJpaRepository<TXNUSER, String> {
	protected Logger logger = Logger.getLogger(getClass());

	@SuppressWarnings({ "unused", "unchecked" })
	public List<TXNUSER> findByUserId(String DPSUERID) {
		List result = new ArrayList();
		StringBuffer sb = new StringBuffer();
		Query query = getQuery("from TXNUSER e where DPSUERID = ?", DPSUERID);
		// query.setMaxResults(100);
		return ESAPIUtil.validStrList(query.getResultList());
	}

	public List<TXNUSER> getAll() {

		return find("FROM TXNUSER ORDER BY DPSUERID");
	}

	public List<TXNUSER> getEmailUser() {
		return find("FROM TXNUSER WHERE DPMYEMAIL <> '' ORDER BY DPSUERID");
	}

	public List<TXNUSER> getBillUser() {
		return find(
				"FROM TXNUSER WHERE DPMYEMAIL <> '' AND (DPBILL <> '' OR DPFUNDBILL <> '' OR DPCARDBILL <> '') ORDER BY DPSUERID");
	}

	public List<TXNUSER> getAllFundBillUser() {
		return find(
				"FROM TXNUSER WHERE DPFUNDBILL <> '' and FUNDBILLDATE <> '' and LENGTH(DPMYEMAIL) > 0 ORDER BY DPSUERID");
	}

	public List<TXNUSER> getAllFundStopNotifyUser() {
		try {
			StringBuffer sql = new StringBuffer();
			sql.append(" SELECT * FROM TXNUSER WHERE DPNOTIFY like '%14%' and LENGTH(DPMYEMAIL) > 0 ORDER BY DPSUERID ");
			sql.append(" WITH UR");
			log.debug("getAllFundStopNotifyUser.sql>>{}", sql);
			Query query = getSession().createNativeQuery(sql.toString(), TXNUSER.class);
			return ESAPIUtil.validStrList(query.getResultList());
		}catch(Exception e) {
			log.error("{}", e);
			throw e;
		}
	}

	public List<TXNUSER> getAllBillUserLast2Day(String tomorrow, String yesterday) {
		/*
		 * return find("FROM TXNUSER WHERE ((BILLDATE < '" + tomorrow +
		 * "' and BILLDATE >= '" + yesterday + "') " + " or (FUNDBILLDATE < '" +
		 * tomorrow + "' and FUNDBILLDATE >= '" + yesterday + "') " +
		 * " or (CARDBILLDATE < '" + tomorrow + "' and CARDBILLDATE >= '" + yesterday +
		 * "')) " + " ORDER BY DPSUERID");
		 */
		String subyesterday = yesterday.substring(0, 7);
		return find("FROM TXNUSER WHERE ((BILLDATE like '" + subyesterday + "%') "
				+ " or (FUNDBILLDATE like '" + subyesterday + "%') " + " or (CARDBILLDATE like '" + subyesterday
				+ "%')) " + " ORDER BY DPSUERID");
	}

	@SuppressWarnings({ "unused", "unchecked" })
	public boolean checkFundAgreeVersion(String version, String uid) {

		TXNUSER user = findById(StrUtils.trim(uid));

		if (version.equals(user.getFUNDAGREEVERSION()))
			return true;
		else
			return false;
	}

	@SuppressWarnings({ "unused", "unchecked" })
	public TXNUSER chkRecord(String DPSUERID, String DPUSERNAME) {
		// public TXNUSER chkRecord(String DPSUERID) {

		String dfNotify = "16,17,18,19";// 黃金存摺相關的要預設啟用通知
		TXNUSER user = null;
		int isUserExist = -1;
		try {
			user = findById(StrUtils.trim(DPSUERID));
			log.debug("" + user.getDPSUERID());
			isUserExist = ((user == null) ? 0 : 1);
		} catch (Exception e) {
			isUserExist = 0;
		}
		if (isUserExist == 0) {
			TXNUSER u = new TXNUSER();
			u.setDPSUERID(DPSUERID);
			u.setSCHCOUNT(new Integer(0));
			u.setASKERRTIMES(new Integer(0));
			u.setDPUSERNAME(DPUSERNAME);
			u.setDPNOTIFY(dfNotify);// 黃金存摺相關的要預設啟用通知
			save(u);
			return u;
		} else if (isUserExist == 1) {
			user.setDPUSERNAME(DPUSERNAME);
			save(user);

			return user;
		}

		return null;
	}

	public List<String> getAllUserEmailNotEmpty() {
		Query query = getQuery("SELECT DPMYEMAIL FROM TXNUSER  WHERE LENGTH(DPMYEMAIL) > 0 ORDER BY DPSUERID");

		return ESAPIUtil.validStrList(query.getResultList());
	}

	public List<String> getAllUserEmailNotEmpty1() {
		Query query = getQuery("SELECT DPSUERID FROM TXNUSER  WHERE LENGTH(DPMYEMAIL) > 0 ORDER BY DPSUERID");

		return ESAPIUtil.validStrList(query.getResultList());
	}

	public String getAllUserEmailNotEmpty2(String cusidn) {
		Query query = getQuery("SELECT DPMYEMAIL FROM TXNUSER  WHERE DPSUERID = ?");
		query.setParameter(0, cusidn);
		List qresult = ESAPIUtil.validStrList(query.getResultList());
		return qresult.get(0).toString();
	}

	public List<String> getAllUserEmailApplied() {
		Query query = getQuery(
				"SELECT DPMYEMAIL FROM TXNUSER  WHERE LENGTH(DPMYEMAIL) > 0 and DPNOTIFY like '%15%' ORDER BY DPSUERID");

		return ESAPIUtil.validStrList(query.getResultList());
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateSchcount(String DPSUERID) {
		try {

			Query add1 = this.getSession().createSQLQuery("UPDATE TXNUSER SET SCHCOUNT = SCHCOUNT + 1 WHERE DPSUERID = ?");
	
			add1.setParameter(0, DPSUERID);
	
			int r2 = add1.executeUpdate();
			log.info("TxnUser finish updateSchcount >> {}", DPSUERID);
		}catch(LockAcquisitionException e) {
			log.error("TxnUser LockAcquisitionException >> {}", DPSUERID);
			throw e;
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public int incSchcount(String DPSUERID) {

		try {
	
			// 判斷是否換日
			Query add0 = getSession().createSQLQuery("SELECT SCHCNTDATE FROM TXNUSER WHERE DPSUERID = ?");
			add0.setParameter(0, DPSUERID);
			List result0 = ESAPIUtil.validStrList(add0.getResultList());
	
			String schcntdate = (String) result0.get(0) == null ? "" : (String) result0.get(0); // 最後拿編號日期
			String sysdate = DateTimeUtils.format("yyyyMMdd", new Date());
	
			Query add1 = null;
	
			if (!schcntdate.equals(sysdate)) {
				add1 = getSession().createSQLQuery("UPDATE TXNUSER SET SCHCOUNT = 1, SCHCNTDATE = ? WHERE DPSUERID = ?");
				add1.setParameter(0, sysdate);
				add1.setParameter(1, DPSUERID);
			} else {
				add1 = getSession().createSQLQuery("UPDATE TXNUSER SET SCHCOUNT = SCHCOUNT + 1 WHERE DPSUERID = ?");
				add1.setParameter(0, DPSUERID);
			}
	
			int r2 = add1.executeUpdate();
	
			Query add2 = getSession().createSQLQuery("SELECT SCHCOUNT FROM TXNUSER WHERE DPSUERID = ? ");
			add2.setParameter(0, DPSUERID);
			add2.unwrap(NativeQuery.class).addScalar("SCHCOUNT", IntegerType.INSTANCE);
			List result = ESAPIUtil.validStrList(add2.getResultList());

			log.info("TxnUser finish incSchcount >> {}", DPSUERID);
			return (Integer) result.get(0);
		}catch(LockAcquisitionException e) {
			log.error("TxnUser LockAcquisitionException >> {}", DPSUERID);
			throw e;
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public int updateAdBranchID(String DPSUERID, String ADBRANCHID) {

		try {
			Query add1 = this.getSession().createSQLQuery("UPDATE TXNUSER SET ADBRANCHID = ? WHERE DPSUERID = ?");
	
			add1.setParameter(0, ADBRANCHID);
			add1.setParameter(1, DPSUERID);
	
			int r2 = add1.executeUpdate();
			log.info("TxnUser finish updateAdBranchID >> {}", DPSUERID);
			return (r2);
		}catch(LockAcquisitionException e) {
			log.error("TxnUser LockAcquisitionException >> {}", DPSUERID);
			throw e;
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public int updateFundAgreeVersion(String DPSUERID, String VERSION) {
		try {

			Query add1 = this.getSession().createSQLQuery("UPDATE TXNUSER SET FUNDAGREEVERSION = ? WHERE DPSUERID = ?");
	
			add1.setParameter(0, VERSION);
			add1.setParameter(1, DPSUERID);
	
			int r2 = add1.executeUpdate();
			log.info("TxnUser finish updateFundAgreeVersion >> {}", DPSUERID);
			return (r2);
		}catch(LockAcquisitionException e) {
			log.error("TxnUser LockAcquisitionException >> {}", DPSUERID);
			throw e;
		}
	}

	/**
	 * 信用卡、金融卡申請網銀，如果使用者不在時建立一個使用者。
	 * 
	 * @param DPSUERID
	 * @return
	 */
	@SuppressWarnings({ "unused", "unchecked" })
	public TXNUSER chkRecord(String DPSUERID) {

		TXNUSER user = null;
		int isUserExist = -1;
		try {
			// user = super.get(StrUtils.trim(DPSUERID));
			user = findById(StrUtils.trim(DPSUERID));
			// log.debug("" + user.getDPSUERID());
			isUserExist = ((user == null) ? 0 : 1);
		} catch (Exception e) {
			isUserExist = 0;
		}
		if (isUserExist == 0) {
			TXNUSER u = new TXNUSER();
			u.setDPSUERID(DPSUERID);
			u.setSCHCOUNT(new Integer(0));
			u.setASKERRTIMES(new Integer(0));
			u.setCCARDFLAG("0");

			save(u);
			return u;
		} else if (isUserExist == 1) {
			return user;
		}

		return null;
	}
	
	/**
	 * 信用卡、金融卡申請網銀，如果使用者不在時建立一個使用者。
	 * 
	 * @param DPSUERID
	 * @return
	 */
	@SuppressWarnings({ "unused", "unchecked" })
	public TXNUSER chkRecordNotify(String DPSUERID, String MAILADDR, boolean ASK, boolean Card) {
		
		TXNUSER user = null;
		boolean isUserExist = false;
		String notify = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21";
		Date d = new Date();
		try {
			// user = super.get(StrUtils.trim(DPSUERID));
			user = findById(StrUtils.trim(DPSUERID));
			// log.debug("" + user.getDPSUERID());
			isUserExist = ((user == null) ? false : true);
		} catch (Exception e) {
			log.error("FIND TXNUSER ERROR >>>{}",e);
			return user;
		}
		
		try {
			if (!isUserExist) {
				user = new TXNUSER();
				user.setDPSUERID(DPSUERID);
				user.setSCHCOUNT(new Integer(0));
				user.setASKERRTIMES(new Integer(0));
				if(Card) {
					user.setCCARDFLAG("2");
				}
				else {
					user.setCCARDFLAG("0");
				}
				
			} else {
				if(ASK) {
					user.setASKERRTIMES(new Integer(0));
				}
				if(Card) {
					user.setCCARDFLAG("2");
				}
			}
			user.setDPMYEMAIL(MAILADDR);
			user.setEMAILDATE(DateTimeUtils.getCDateShort(d) + DateTimeUtils.getTimeShort(d));
			user.setDPNOTIFY(notify);
			user.setDPBILL("Y");  //啟用電子帳單申請狀態
			user.setBILLDATE(DateTimeUtils.getCDateShort(d) + DateTimeUtils.getTimeShort(d));
			
			save(user);
			log.info("TXNUSER SAVE SUCCESS");
		}
		catch (Exception e) {
			// TODO: handle exception
			log.error("UPDATE TXNUSER ERROR>>>{}" , e);
		}
		return user;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public int updateDPMYEMAIL(String DPSUERID, String EMAIL) {
		try {

			Date d = new Date();
			String date = DateTimeUtils.getCDateShort(d);
			String time = DateTimeUtils.getTimeShort(d);
	
			Query add1 = this.getSession()
					.createSQLQuery("UPDATE TXNUSER SET DPMYEMAIL = ? ,EMAILDATE = ? WHERE DPSUERID = ?");
	
			add1.setParameter(0, EMAIL);
			add1.setParameter(1, date + time);
			add1.setParameter(2, DPSUERID);
			int r2 = add1.executeUpdate();
			log.info("TxnUser finish updateDPMYEMAIL >> {}", DPSUERID);
			return (r2);
		}catch(LockAcquisitionException e) {
			log.error("TxnUser LockAcquisitionException >> {}", DPSUERID);
			throw e;
		}
	}

	/**
	 * 取得預約編號
	 * 
	 * @param CUSIDN
	 * @return
	 */
	public String getDptmpschno(String CUSIDN) {
		// 更新SCHCOUNT(每日預約計數)
		int scnt = this.incSchcount(CUSIDN);
		// 預約編號(YYMMDD+三位流水號)
		String dptmpschno = "";
		String ymd = DateTimeUtils.format("yyMMddHH", new Date());
		dptmpschno = ymd + StrUtils.right("000" + scnt, 3);
		return dptmpschno;
	}

	// START 20191029 查詢非行員清單明細
	public MVHImpl findByNNBReportM910() {
		log.debug("IN findByNNBReportM910");
		Rows result = new Rows();
		MVHImpl mvhresult = null;
		
//		String sql = "SELECT T.ADBRANCHID,T.DPUSERNAME,T.DPSUERID,T.DPMYEMAIL,amf.EMPNO FROM TXNUSER T LEFT JOIN ADMEMPINFO amf ON T.DPSUERID =amf.CUSID WHERE T.DPSUERID NOT IN(SELECT CUSID FROM ADMEMPINFO WHERE LDATE = '' AND CUSID !='' AND EMPMAIL LIKE '%mail.tbb.com.tw%') AND T.DPMYEMAIL LIKE '%mail.tbb.com.tw%' AND amf.EMPNO !='' ORDER BY ADBRANCHID ASC";
		String sql = "SELECT T.ADBRANCHID,T.DPUSERNAME,T.DPSUERID,T.DPMYEMAIL,amf.EMPNO FROM TXNUSER T LEFT JOIN ADMEMPINFO amf ON T.DPMYEMAIL =amf.EMPMAIL  WHERE T.DPMYEMAIL LIKE '%mail.tbb.com.tw' AND T.DPSUERID NOT IN (SELECT CUSID FROM ADMEMPINFO WHERE EMPNO!='' AND CUSID!='') and amf.LDATE='' ORDER BY ADBRANCHID ASC";
		try {
			Query query = getSession().createSQLQuery(sql);

			query.unwrap(NativeQuery.class).addScalar("ADBRANCHID", StringType.INSTANCE);
			query.unwrap(NativeQuery.class).addScalar("DPUSERNAME", StringType.INSTANCE);
			query.unwrap(NativeQuery.class).addScalar("DPSUERID", StringType.INSTANCE);
			query.unwrap(NativeQuery.class).addScalar("DPMYEMAIL", StringType.INSTANCE);
			query.unwrap(NativeQuery.class).addScalar("EMPNO", StringType.INSTANCE);

			List qresult = query.getResultList();
			Iterator it = qresult.iterator();
			while (it.hasNext()) {
				Object[] o = (Object[]) it.next();
				Map rm = new HashMap();
				int t = -1;
				rm.put("ADBRANCHID", o[++t].toString());
				rm.put("DPUSERNAME", (o[++t] == null ? "" : o[t].toString()));
				rm.put("DPSUERID", (o[++t] == null ? "" : o[t].toString()));
				rm.put("DPMYEMAIL", (o[++t] == null ? "" : o[t].toString()));
				rm.put("EMPNO", (o[++t] == null ? "" : o[t].toString()));

				Row r = new Row(rm);
				result.addRow(r);
			}

			mvhresult = new MVHImpl(result);

			return mvhresult;

		} catch (Exception e) {
			throw (TopMessageException.create("ZX99"));
		}
	}
	
	public long checkStatus() {
		String SQL = "SELECT COUNT(*) FROM TXNUSER";
		return ((Long) find(SQL,null).get(0)).longValue();
	}

	@Override
	public EntityManager getEntityManager() {
		// TODO Auto-generated method stub
		return super.getEntityManager();
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return super.count();
	}

	@Override
	public List<TXNUSER> findAll() {
		// TODO Auto-generated method stub
		return super.findAll();
	}

	@Override
	public TXNUSER findById(String id) {
		// TODO Auto-generated method stub
		return super.findById(id);
	}

	@Override
	public void save(TXNUSER entity) {
		try {
			// TODO Auto-generated method stub
			super.save(entity);
			log.info("TxnUser finish save >> {}", entity);
		}catch(LockAcquisitionException e) {
			log.error("TxnUser LockAcquisitionException >> {}", entity);
			throw e;
		}
	}

	@Override
	public TXNUSER update(TXNUSER entity) {
		try {
			// TODO Auto-generated method stub
			TXNUSER result = super.update(entity);
			log.info("TxnUser finish update >> {}", entity);
			return result;
		}catch(LockAcquisitionException e) {
			log.error("TxnUser LockAcquisitionException >> {}", entity);
			throw e;
		}
	}

	@Override
	public void delete(TXNUSER entity) {
		try {
		// TODO Auto-generated method stub
			super.delete(entity);
			log.info("TxnUser finish delete >> {}", entity);
		}catch(LockAcquisitionException e) {
			log.error("TxnUser LockAcquisitionException >> {}", entity);
			throw e;
		}
	}

	@Override
	public Query getQuery(String hql, Object... values) {
		// TODO Auto-generated method stub
		return super.getQuery(hql, values);
	}

	@Override
	public Query getCriteriaQuery(Class<TXNUSER> entityClass,
			BiFunction<Root, CriteriaBuilder, Predicate>... criteriaFunction) {
		// TODO Auto-generated method stub
		return super.getCriteriaQuery(entityClass, criteriaFunction);
	}

	@Override
	public List find(String hql, Object... values) {
		// TODO Auto-generated method stub
		return super.find(hql, values);
	}

	@Override
	public List<TXNUSER> findBy(Class<TXNUSER> entityClass, String name, Object value) {
		// TODO Auto-generated method stub
		return super.findBy(entityClass, name, value);
	}

	@Override
	public TXNUSER findUniqueBy(Class<TXNUSER> entityClass, String name, Object value) {
		// TODO Auto-generated method stub
		return super.findUniqueBy(entityClass, name, value);
	}

	@Override
	public List<TXNUSER> findByLike(Class<TXNUSER> entityClass, String name, String value) {
		// TODO Auto-generated method stub
		return super.findByLike(entityClass, name, value);
	}

	@Override
	public SessionAdapter getSession() {
		// TODO Auto-generated method stub
		return super.getSession();
	}

	@Override
	public Page pagedQuery(String hql, int pageNo, int pageSize, Object... values) {
		// TODO Auto-generated method stub
		return super.pagedQuery(hql, pageNo, pageSize, values);
	}
	
	
	
	
}