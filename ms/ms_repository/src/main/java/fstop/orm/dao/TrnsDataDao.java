package fstop.orm.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.tbb.ebill.util.MessageCode;
import com.tbb.ebill.util.StringUtil;

import fstop.core.BeanUtils;
import fstop.orm.po.SYSDAILYSEQ;
import fstop.orm.po.TRNSDATA;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Transactional
public class TrnsDataDao extends LegacyJpaRepository<TRNSDATA, Serializable>{
	
	public void insertTrnsData(String trnsno, String access, String bill, String action, String txn_datetime){
		TRNSDATA trnsData = new TRNSDATA();
		trnsData.setTRNSNO(trnsno == null ? "":trnsno);
		trnsData.setACCESS(access == null ? "":access);
		trnsData.setBILL(bill == null ? "":bill);
		trnsData.setACTION(action == null ? "":action);
		trnsData.setTXN_DATETIME(txn_datetime == null ? "":txn_datetime);
		save(trnsData);
	}
	
	public int queryTrnsData(String trnsNo) throws Exception{
		TRNSDATA trnsData = null;
		try {
			log.trace("trnsNo >> {}", trnsNo);
			trnsData = (TRNSDATA)find("FROM TRNSDATA WHERE TRNSNO = ? ", trnsNo).get(0);
			if (trnsData==null){
				throw new Exception(MessageCode.getCode(MessageCode.R1004));
			} else {
//				return StringUtil.padding(Integer.toString(trnsData.getAPI_TXN_NO()), 7, "L", "0");
				return trnsData.getAPI_TXN_NO();
			}
		} catch (Exception e){
			try {
				Thread.sleep(1000);
				trnsData = (TRNSDATA)find("FROM TRNSDATA WHERE TRNSNO = ? ", trnsNo).get(0);
				if (trnsData==null){
					throw new Exception(MessageCode.getCode(MessageCode.R1004));
				} else {
//					return StringUtil.padding(Integer.toString(trnsData.getAPI_TXN_NO()), 7, "L", "0");
					return trnsData.getAPI_TXN_NO();
				}
			} catch (Exception ex){
				throw new Exception(MessageCode.getCode(MessageCode.R1004));
			}
		}
	}
	
}
