package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.util.ESAPIUtil;

import fstop.model.Row;
import fstop.model.Rows;
import fstop.orm.po.ADMHOTOP;
import fstop.orm.po.ADMLOGIN;
import fstop.orm.po.ADMLOGINOUT;
import fstop.orm.po.TXNLOG;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.hibernate.query.NativeQuery;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Parameter;
import javax.persistence.ParameterMode;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Slf4j
@Repository
@Transactional
public class AdmLogInDao extends LegacyJpaRepository<ADMLOGIN, String> {
	protected Logger logger = Logger.getLogger(getClass());

	public List<String> findYesterdayLogin() {
		List<String> idList = new ArrayList<String>();
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE,-1);
		String yesterday = DateTimeUtils.format("yyyyMMdd", cal.getTime());
		Query query = getSession().createSQLQuery("SELECT * FROM ADMLOGIN WHERE LOGINTIME LIKE ?");
		query.unwrap(NativeQuery.class).addEntity("ADMLOGIN", ADMLOGIN.class);
		query.setParameter(0,yesterday+"%");
		List<ADMLOGIN> qrresult = ESAPIUtil.validStrList(query.getResultList());
		
		for(ADMLOGIN data:qrresult) {
			idList.add(data.getADUSERID());
		}
		
		return idList;
		
	}
}
