package fstop.orm.dao;

import java.util.List;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.ITRN027;

import javax.persistence.Query;


@Slf4j
@Repository
@Transactional
public class ItrN027Dao extends LegacyJpaRepository<ITRN027, String> {
	protected Logger logger = Logger.getLogger(getClass());

	public List<ITRN027> getAll() {
		return find("FROM ITRN027 ORDER BY RECNO");
	}
	 
	public void insert(ITRN027 itrn027) {
		int r2=0;
		String fillstr=new String();
		Query add1=null;
		
		add1=this.getSession().createSQLQuery("INSERT INTO ITRN027 VALUES(? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,?)");
		add1.setParameter(0, itrn027.getRECNO());
		log.debug("RECNO = " + itrn027.getRECNO());
		add1.setParameter(1, itrn027.getHEADER());
		log.debug("HEADER = " + itrn027.getHEADER());
		add1.setParameter(2, itrn027.getSEQ());
		log.debug("SEQ = " + itrn027.getSEQ());
		add1.setParameter(3, itrn027.getBRHBDT());
		log.debug("DATE = " + itrn027.getBRHBDT());
		add1.setParameter(4, itrn027.getCOUNT());
		log.debug("COUNT = " + itrn027.getCOUNT());
		add1.setParameter(5, itrn027.getTERM());
		log.debug("TERM = " + itrn027.getTERM());
		add1.setParameter(6, itrn027.getITR1());
		log.debug("ITR1 = " + itrn027.getITR1());
		add1.setParameter(7, itrn027.getTITLE1());
		log.debug("TITLE1 = " + itrn027.getTITLE1());
		add1.setParameter(8, itrn027.getITR2());
		log.debug("ITR2 = " + itrn027.getITR2());
		add1.setParameter(9, itrn027.getTITLE2());
		log.debug("TITLE2 = " + itrn027.getTITLE2());
		add1.setParameter(10, itrn027.getITR3());
		log.debug("ITR3 = " + itrn027.getITR3());
		add1.setParameter(11, itrn027.getTITLE3());
		log.debug("TITLE3 = " + itrn027.getTITLE3());

		if(itrn027.getFILL()==null)
			fillstr=" ";
		else
			fillstr=itrn027.getFILL();
		log.debug("FILL = " + fillstr);
		add1.setParameter(12, fillstr);
		r2 = add1.executeUpdate();
	}

	public void deleteAll() {
		this.getSession().createSQLQuery("DELETE FROM itrn027").executeUpdate();
	}
}
