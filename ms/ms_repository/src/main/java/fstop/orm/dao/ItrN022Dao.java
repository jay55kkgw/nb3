package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import fstop.orm.po.ITRN022;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.List;

@Slf4j
@Repository
@Transactional
public class ItrN022Dao extends LegacyJpaRepository<ITRN022, String> {
	protected Logger logger = Logger.getLogger(getClass());

	public List<ITRN022> getAll() {
		return find("FROM ITRN022 ORDER BY RECNO");
	}
	
	public void insert(ITRN022 itrn022) {
		int r2=0;
		String fillstr=new String();
		Query add1=null;
		
		add1=this.getSession().createSQLQuery("INSERT INTO ITRN022 VALUES(? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? )");
		add1.setParameter(0, itrn022.getRECNO());
		log.debug("RECNO = " + itrn022.getRECNO());
		add1.setParameter(1, itrn022.getHEADER());
		log.debug("HEADER = " + itrn022.getHEADER());
		add1.setParameter(2, itrn022.getSEQ());
		log.debug("SEQ = " + itrn022.getSEQ());
		add1.setParameter(3, itrn022.getDATE());
		log.debug("DATE = " + itrn022.getDATE());
		add1.setParameter(4, itrn022.getTIME());
		log.debug("TIME = " + itrn022.getTIME());
		add1.setParameter(5, itrn022.getCOUNT());
		log.debug("COUNT = " + itrn022.getCOUNT());
		add1.setParameter(6, itrn022.getCOLOR());
		log.debug("COLOR = " + itrn022.getCOLOR());
		add1.setParameter(7, itrn022.getCRYNAME());
		log.debug("CRYNAME = " + itrn022.getCRYNAME());
		add1.setParameter(8, itrn022.getCRY());
		log.debug("CRY = " + itrn022.getCRY());
		add1.setParameter(9, itrn022.getITR1());
		log.debug("ITR1 = " + itrn022.getITR1());
		add1.setParameter(10, itrn022.getITR2());
		log.debug("ITR2 = " + itrn022.getITR2());
		add1.setParameter(11, itrn022.getITR3());
		log.debug("ITR3 = " + itrn022.getITR3());
		add1.setParameter(12, itrn022.getITR4());
		log.debug("ITR4 = " + itrn022.getITR4());
		add1.setParameter(13, itrn022.getITR5());
		log.debug("ITR5 = " + itrn022.getITR5());
		add1.setParameter(14, itrn022.getITR6());
		log.debug("ITR6 = " + itrn022.getITR6());
		add1.setParameter(15, itrn022.getITR7());
		log.debug("ITR7 = " + itrn022.getITR7());
		add1.setParameter(16, itrn022.getITR8());
		log.debug("ITR8 = " + itrn022.getITR8());
		add1.setParameter(17, itrn022.getITR9());
		log.debug("ITR9 = " + itrn022.getITR9());
		add1.setParameter(18, itrn022.getITR10());
		log.debug("ITR10 = " + itrn022.getITR10());
		add1.setParameter(19, itrn022.getITR11());
		log.debug("ITR11 = " + itrn022.getITR11());
		add1.setParameter(20, itrn022.getITR12());
		log.debug("ITR12 = " + itrn022.getITR12());
		add1.setParameter(21, itrn022.getITR13());
		log.debug("ITR13 = " + itrn022.getITR13());
		add1.setParameter(22, itrn022.getITR14());
		log.debug("ITR14 = " + itrn022.getITR14());
		add1.setParameter(23, itrn022.getITR15());
		log.debug("ITR15 = " + itrn022.getITR15());
		add1.setParameter(24, itrn022.getITR16());
		log.debug("ITR16 = " + itrn022.getITR16());
		add1.setParameter(25, itrn022.getITR17());
		log.debug("ITR17 = " + itrn022.getITR17());
		add1.setParameter(26, itrn022.getITR18());
		log.debug("ITR18 = " + itrn022.getITR18());
		add1.setParameter(27, itrn022.getITR19());
		log.debug("ITR19 = " + itrn022.getITR19());
		add1.setParameter(28, itrn022.getITR20());
		log.debug("ITR20 = " + itrn022.getITR20());
		if(itrn022.getFILL()==null)
			fillstr=" ";
		else
			fillstr=itrn022.getFILL();
		log.debug("FILL = " + fillstr);
		add1.setParameter(29, fillstr);
		r2 = add1.executeUpdate();
	}

	public void deleteAll() {
		this.getSession().createSQLQuery("DELETE FROM ITRN022").executeUpdate();
	}
}
