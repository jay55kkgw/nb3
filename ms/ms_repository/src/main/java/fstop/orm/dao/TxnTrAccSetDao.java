package fstop.orm.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.query.NativeQuery;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;
import com.netbank.domain.orm.core.SessionAdapter;
import com.netbank.util.ESAPIUtil;

import fstop.orm.po.TXNTRACCSET;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@Repository
@Transactional(
		transactionManager="nb3transactionManager",
        rollbackFor = {Throwable.class}
)
public class TxnTrAccSetDao extends LegacyJpaRepository<TXNTRACCSET, Integer> {

	
	
	@Override
	public EntityManager getEntityManager() {
		// TODO Auto-generated method stub
		return super.getEntityManager();
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return super.count();
	}

	@Override
	public List<TXNTRACCSET> findAll() {
		// TODO Auto-generated method stub
		return super.findAll();
	}

	@Override
	public TXNTRACCSET findById(Integer id) {
		// TODO Auto-generated method stub
		return super.findById(id);
	}

	@Override
	public void save(TXNTRACCSET entity) {
		// TODO Auto-generated method stub
		super.save(entity);
	}

	@Override
	public TXNTRACCSET update(TXNTRACCSET entity) {
		// TODO Auto-generated method stub
		return super.update(entity);
	}

	@Override
	public void delete(TXNTRACCSET entity) {
		// TODO Auto-generated method stub
		super.delete(entity);
	}

	@Override
	public Query getQuery(String hql, Object... values) {
		// TODO Auto-generated method stub
		return super.getQuery(hql, values);
	}

	@Override
	public Query getCriteriaQuery(Class<TXNTRACCSET> entityClass,
			BiFunction<Root, CriteriaBuilder, Predicate>... criteriaFunction) {
		// TODO Auto-generated method stub
		return super.getCriteriaQuery(entityClass, criteriaFunction);
	}

	@Override
	public List find(String hql, Object... values) {
		// TODO Auto-generated method stub
		return super.find(hql, values);
	}

	@Override
	public List<TXNTRACCSET> findBy(Class<TXNTRACCSET> entityClass, String name, Object value) {
		// TODO Auto-generated method stub
		return super.findBy(entityClass, name, value);
	}

	@Override
	public TXNTRACCSET findUniqueBy(Class<TXNTRACCSET> entityClass, String name, Object value) {
		// TODO Auto-generated method stub
		return super.findUniqueBy(entityClass, name, value);
	}

	@Override
	public List<TXNTRACCSET> findByLike(Class<TXNTRACCSET> entityClass, String name, String value) {
		// TODO Auto-generated method stub
		return super.findByLike(entityClass, name, value);
	}

	@Override
	public SessionAdapter getSession() {
		// TODO Auto-generated method stub
		return super.getSession();
	}

	@Override
	public Page pagedQuery(String hql, int pageNo, int pageSize, Object... values) {
		// TODO Auto-generated method stub
		return super.pagedQuery(hql, pageNo, pageSize, values);
	}

	public List<TXNTRACCSET> getByUid(String uid) {
		return find("FROM TXNTRACCSET e WHERE DPUSERID = ? ORDER BY DPTRIBANK, DPTRACNO", uid);
	}
	
	//判斷資料庫是否有約定帳號資料
	public boolean findDptrdacnoIsExists(String idn,String DPTRIBANK, String DPTRDACNO) {
		Query query = this.getSession().createQuery("SELECT * FROM TXNTRACCSET WHERE DPUSERID=? and DPTRIBANK = ? and DPTRDACNO = ?");
		query.setParameter(0, idn);
		query.setParameter(1, DPTRIBANK);
		query.setParameter(2, DPTRDACNO);
		
		List list = query.getResultList();
		
		if(list.size() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	
	
	//取出該約定帳號資料PK
	public int getDpaccsetid(String idn,String DPTRIBANK, String DPTRDACNO) {
		List<TXNTRACCSET> list = null;
		TXNTRACCSET po = null;
		int result = 0;
		try {
			Query query = getSession().createQuery("SELECT * FROM TXNTRACCSET WHERE DPUSERID=? and DPTRIBANK = ? and DPTRDACNO = ?");
			query.setParameter(0, idn);
			query.setParameter(1, DPTRIBANK);
			query.setParameter(2, DPTRDACNO);
			
			list = query.getResultList();
			
			if(list !=null && !list.isEmpty()) {
				result = list.get(0).getDPACCSETID();
			}
		} catch (Exception e) {
			log.error("getDpaccsetid error>>{}", e);
		}
		return result;
	}
	
	public List<TXNTRACCSET> findByDPGONAME(String DPUSERID,String DPTRIBANK,String DPTRDACNO) {
		return find("from TXNTRACCSET where DPUSERID = ? AND DPTRIBANK = ? AND DPTRDACNO = ? ORDER BY DPTRIBANK, DPTRACNO", DPUSERID,DPTRIBANK,DPTRDACNO);
	}
	
	public List<TXNTRACCSET> findByTime(String stTime, String edTime){
		List<TXNTRACCSET> list = new ArrayList<TXNTRACCSET>();
		try {
			Query query = getSession().createQuery("SELECT * FROM TXNTRACCSET WHERE CONCAT(LASTDATE, LASTTIME) >=  ? AND CONCAT(LASTDATE, LASTTIME) <= ?");
			query.setParameter(0, stTime);
			query.setParameter(1, edTime);
			query.unwrap(NativeQuery.class).addEntity("TXNTRACCSET", TXNTRACCSET.class);
			
			list = ESAPIUtil.validStrList(query.getResultList());
			
		} catch (Exception e) {
			log.error("findByTime error>>{}", e);
		}
		return list;
	}
}
