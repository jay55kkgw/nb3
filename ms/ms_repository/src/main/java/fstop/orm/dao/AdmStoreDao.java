package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import fstop.orm.po.ADMSTORE;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.Date;
import java.util.List;


@Slf4j
@Repository
public class AdmStoreDao extends LegacyJpaRepository<ADMSTORE, Integer> {
	
	protected Logger logger = Logger.getLogger(getClass());
	protected String _MessageType;
	protected String _MessageStatus;
	protected String _MessageApprove;
	protected String _MessageStartMinDate;
	protected String _MessageStartMaxDate;
	protected String _MessageDueMinDate;
	protected String _MessageDueMaxDate;
	protected ADMSTORE PO_admStore;
	protected String _MessageTitle;
	
	//設定行銷類別 { 1:分期付款商店  2:特約折扣商店  }
	public void setMessageType(String messageType)
	{
		_MessageType = messageType;
	}
	
	//設定行銷狀態 { M:未核放  N:不同意  C:已核放 }
	public void setMessageStatus(String messageStatus)
	{
		_MessageStatus = messageStatus;
	}
	
	//設定審核結果 { 0:審核未過  1:審核過 }
	public void setMessageApprove(String messageApprove)
	{
		_MessageApprove = messageApprove;
	}
	
	//設定行銷上版起日(區間起值)
	public void setMessageStartMinDate(String messageStartMinDate)
	{
		_MessageStartMinDate = messageStartMinDate;
	}
	
	//設定行銷上版起日(區間迄值)
	public void setMessageStartMaxDate(String messageStartMaxDate)
	{
		_MessageStartMinDate = messageStartMaxDate;
	}
	
	//設定行銷上版迄日(區間起值)
	public void setMessageDueMinDate(String messageDueMinDate)
	{
		_MessageDueMinDate = messageDueMinDate;
	}
	
	//設定行銷上版迄日(區間起值)
	public void setMessageDueMaxDate(String messageDueMaxDate)
	{
		_MessageDueMaxDate = messageDueMaxDate;
	}
	
	//設定特店標題
	public void setMessageTitle(String messageTitle)
	{
		_MessageTitle = messageTitle;
	}
	
	public void setPo(ADMSTORE PO)
	{
		PO_admStore = PO;
	}

	
	public List<ADMSTORE> getMessage(int msgID)
	{
		String QueryStr = "FROM ADMSTORE ";
		QueryStr += "WHERE STID = ?";
		
		List<ADMSTORE> qresult = find(QueryStr, msgID);
		return qresult;
	}
	public List<ADMSTORE> getConditionMessage(String ConditionStr)
	{
		String QueryStr = "FROM ADMSTORE ";
		QueryStr += "WHERE 1 = 1 " + ConditionStr;

		List<ADMSTORE> qresult = find(QueryStr);
		return qresult;
	}
	
	public List<ADMSTORE> getAll()
	{
		String current = DateTimeUtils.format("yyyyMMddHHmm", new Date());
		current += "00";
		
		log.debug("MessageType ===============================> " + _MessageType);
		log.debug("MessageStatus ===============================> " + _MessageStatus);
		log.debug("MessageApprove ===============================> " + _MessageApprove);
		log.debug("MessageStartMinDate ===============================> " + _MessageStartMinDate);
		log.debug("MessageStartMaxDate ===============================> " + _MessageStartMaxDate);
		log.debug("MessageDueMinDate ===============================> " + _MessageDueMinDate);
		log.debug("MessageDueMaxDate ===============================> " + _MessageDueMaxDate);
		log.debug("MessageTitle ===============================> " + _MessageTitle);
		
		String QueryStr = "FROM ADMSTORE ";
		QueryStr += "WHERE 1 = 1 ";
		if(_MessageStatus != null && _MessageStatus.length() > 0)
			QueryStr += "and STADSTAT = '" + _MessageStatus + "' ";
		if(_MessageStartMinDate != null && _MessageStartMinDate.length() > 0)
			QueryStr += "and SDATE||STIME >= '" + _MessageStartMinDate + "' ";
		if(_MessageStartMaxDate != null && _MessageStartMaxDate.length() > 0)
			QueryStr += "and SDATE||STIME <= '" + _MessageStartMaxDate + "' ";
		if(_MessageDueMinDate != null && _MessageDueMinDate.length() > 0)
			QueryStr += "and EDATE||ETIME >= '" + _MessageDueMinDate + "' ";
		if(_MessageDueMaxDate != null && _MessageDueMaxDate.length() > 0)
			QueryStr += "and EDATE||ETIME <= '" + _MessageDueMaxDate + "' ";
		if(_MessageType != null && _MessageType.length() > 0)
			QueryStr += "and ADTYPE = '" + _MessageType + "' ";		
		if(_MessageApprove != null && _MessageApprove.length() > 0)
			QueryStr += "and STCHRES = '" + _MessageApprove + "' ";
		if(_MessageTitle != null && _MessageTitle.length() > 0)
			QueryStr += "and STADHLK like '%" + _MessageTitle + "%' ";
		
		QueryStr += "ORDER BY STID,SDATE||STIME DESC";

		log.debug("QueryStr ===============================> " + QueryStr);
		
		List<ADMSTORE> qresult = find(QueryStr);
		return qresult;
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public int addMessage() {
		
		String insertStr = "INSERT INTO ADMSTORE( ";
		insertStr += "USERID, ";
		insertStr += "USERNAME, ";
		insertStr += "TXDATE, ";
		insertStr += "TXTIME, ";
		insertStr += "SDATE, ";
		insertStr += "STIME, ";
		insertStr += "EDATE, ";
		insertStr += "ETIME, ";
		insertStr += "ADTYPE, ";
		insertStr += "STTYPE, ";
		insertStr += "STWEIGHT, ";
		insertStr += "STADHLK, ";
		insertStr += "STPICADD, ";
		insertStr += "STPICDATA, ";
		insertStr += "STTEL, ";
		insertStr += "STADCON, ";
		insertStr += "STADNOTE, ";
		insertStr += "STADINFO, ";
		insertStr += "STADSTAT, ";
		insertStr += "STCHRES, ";
		insertStr += "STSUPNO, ";
		insertStr += "STSUPNAME, ";
		insertStr += "STSUPDATE, ";
		insertStr += "STSUPTIME) ";

		insertStr += "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

		Query add1 = this.getSession().createSQLQuery(insertStr);

		add1.setParameter(0, PO_admStore.getUSERID());
		add1.setParameter(1, PO_admStore.getUSERNAME());
		add1.setParameter(2, PO_admStore.getTXDATE());
		add1.setParameter(3, PO_admStore.getTXTIME());
		add1.setParameter(4, PO_admStore.getSDATE());
		add1.setParameter(5, PO_admStore.getSTIME());
		add1.setParameter(6, PO_admStore.getEDATE());
		add1.setParameter(7, PO_admStore.getETIME());
		add1.setParameter(8, PO_admStore.getADTYPE());
		add1.setParameter(9, PO_admStore.getSTTYPE());
		add1.setParameter(10, PO_admStore.getSTWEIGHT());
		add1.setParameter(11, PO_admStore.getSTADHLK());
		add1.setParameter(12, PO_admStore.getSTPICADD());
		add1.setParameter(13, PO_admStore.getSTPICDATA());
		add1.setParameter(14, PO_admStore.getSTTEL());
		add1.setParameter(15, PO_admStore.getSTADCON());
		add1.setParameter(16, PO_admStore.getSTADNOTE());
		add1.setParameter(17, PO_admStore.getSTADINFO());
		add1.setParameter(18, PO_admStore.getSTADSTAT());
		add1.setParameter(19, PO_admStore.getSTCHRES());
		add1.setParameter(20, PO_admStore.getSTSUPNO());
		add1.setParameter(21, PO_admStore.getSTSUPNAME());
		add1.setParameter(22, PO_admStore.getSTSUPDATE());
		add1.setParameter(23, PO_admStore.getSTSUPTIME());

		int r2 = add1.executeUpdate();
		return(r2);
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public int updateMessage() {
		
		String insertStr = "UPDATE ADMSTORE SET ";
		insertStr += "USERID = ?, ";
		insertStr += "USERNAME = ?, ";
		insertStr += "TXDATE = ?, ";
		insertStr += "TXTIME = ?, ";
		insertStr += "SDATE = ?, ";
		insertStr += "STIME = ?, ";
		insertStr += "EDATE = ?, ";
		insertStr += "ETIME = ?, ";
		insertStr += "ADTYPE = ?, ";
		insertStr += "STTYPE = ?, ";
		insertStr += "STWEIGHT = ?, ";
		insertStr += "STADHLK = ?, ";
		insertStr += "STPICADD = ?, ";
		insertStr += "STPICDATA = ?, ";
		insertStr += "STTEL = ?, ";
		insertStr += "STADCON = ?, ";
		insertStr += "STADNOTE = ?, ";
		insertStr += "STADINFO = ?, ";
		insertStr += "STADSTAT = ?, ";
		insertStr += "STCHRES = ?, ";
		insertStr += "STSUPNO = ?, ";
		insertStr += "STSUPNAME = ?, ";
		insertStr += "STSUPDATE = ?, ";
		insertStr += "STSUPTIME  = ? ";
		insertStr += "WHERE STID = ?";
		
		Query add1=this.getSession().createSQLQuery(insertStr);

		add1.setParameter(0, PO_admStore.getUSERID());
		add1.setParameter(1, PO_admStore.getUSERNAME());
		add1.setParameter(2, PO_admStore.getTXDATE());
		add1.setParameter(3, PO_admStore.getTXTIME());
		add1.setParameter(4, PO_admStore.getSDATE());
		add1.setParameter(5, PO_admStore.getSTIME());
		add1.setParameter(6, PO_admStore.getEDATE());
		add1.setParameter(7, PO_admStore.getETIME());
		add1.setParameter(8, PO_admStore.getADTYPE());
		add1.setParameter(9, PO_admStore.getSTTYPE());
		add1.setParameter(10, PO_admStore.getSTWEIGHT());
		add1.setParameter(11, PO_admStore.getSTADHLK());
		add1.setParameter(12, PO_admStore.getSTPICADD());
		add1.setParameter(13, PO_admStore.getSTPICDATA());
		add1.setParameter(14, PO_admStore.getSTTEL());
		add1.setParameter(15, PO_admStore.getSTADCON());
		add1.setParameter(16, PO_admStore.getSTADNOTE());
		add1.setParameter(17, PO_admStore.getSTADINFO());
		add1.setParameter(18, PO_admStore.getSTADSTAT());
		add1.setParameter(19, PO_admStore.getSTCHRES());
		add1.setParameter(20, PO_admStore.getSTSUPNO());
		add1.setParameter(21, PO_admStore.getSTSUPNAME());
		add1.setParameter(22, PO_admStore.getSTSUPDATE());
		add1.setParameter(23, PO_admStore.getSTSUPTIME());
		add1.setParameter(24, PO_admStore.getSTID());

		int r2 = add1.executeUpdate();
		return(r2);
	}
	public void Reset()
	{
		setMessageType("");
		setMessageApprove("");
		setMessageStartMinDate("");
		setMessageStartMaxDate("");
		setMessageDueMinDate("");
		setMessageDueMaxDate("");
		setMessageTitle("");
	}
	
}
