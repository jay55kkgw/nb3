package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import fstop.orm.po.ADMBRANCH;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Slf4j
@Repository
@Transactional
public class AdmBranchDao extends LegacyJpaRepository<ADMBRANCH, String> {
	protected Logger logger = Logger.getLogger(getClass());

	@Transactional(propagation = Propagation.NOT_SUPPORTED)
	public ADMBRANCH getByACN(String acn) {
		return findById(acn.substring(0, 3));
	}

	public List findByBhID(String bhid) {
		return find("FROM ADMBRANCH WHERE ADBRANCHID = ? ", bhid);
	}

	public List<ADMBRANCH> getAll() {
		return find("FROM ADMBRANCH ORDER BY ADBRANCHID");
	}

	/**
	 * 查詢郵遞區號前三碼一樣的分行
	 * 
	 * @return
	 */
	public List<ADMBRANCH> findZipBh(String ZIPCODE) {
		return find("FROM ADMBRANCH WHERE POSTCODE = ? ", ZIPCODE);
	}

	public boolean isBranchMail(String branchCode) {
			return ((Long) find(
					"SELECT COUNT(*) FROM ADMBRANCH where ADBRANCHID = ? ",
					branchCode).iterator().next()) != 0;	
	}
}