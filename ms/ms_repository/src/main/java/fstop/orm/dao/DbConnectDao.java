package fstop.orm.dao;

import org.springframework.stereotype.Repository;

import com.netbank.domain.orm.core.LegacyJpaRepository;

import fstop.orm.po.SYSPARAMDATA;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
public class DbConnectDao extends LegacyJpaRepository<SYSPARAMDATA, Long> {
	public boolean getDBStatus() throws Exception {
		boolean result = true;
		// 20200504 update 先確定DB連線為OK
		long count = count();
		return result;
	}
}
