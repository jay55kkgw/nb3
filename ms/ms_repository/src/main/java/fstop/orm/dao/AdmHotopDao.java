package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.util.ESAPIUtil;

import fstop.orm.po.ADMHOTOP;
import fstop.orm.po.ADMHOTOP_PK;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;


@Slf4j
@Repository
@Transactional
public class AdmHotopDao extends LegacyJpaRepository<ADMHOTOP, ADMHOTOP_PK> {
	protected Logger logger = Logger.getLogger(getClass());
	
	
//	public boolean clearADHORDER() {
//		getSession().createQuery("UPDATE ADMHOTOP SET ADHORDER=''    ").executeUpdate();
//		return true;
//	}
//	
//	public List<ADMHOTOP> findByAdhorderIsNotNull() {
//		return this.find("FROM ADMHOTOP WHERE ADHORDER <> '' ORDER BY ADHORDER");
//	}
//	
	public List<String> findLogin() {
		List<String> returnList = new ArrayList<String>();
		List<ADMHOTOP> sqlresult = this.find("FROM ADMHOTOP WHERE ADOPID = 'LOGIN'");
		for(ADMHOTOP data:sqlresult) {
			returnList.add(data.getPks().getADUSERID());
		}
		return returnList;
	}
	
	public void delByAduserid(String aduserid) {
		Query query = getSession().createSQLQuery(
				"DELETE FROM ADMHOTOP WHERE ADUSERID =?");
		query.setParameter(0, aduserid);
		query.executeUpdate();
	}
	
	public void insertData(String aduserid , String adopid , String lastdt) {
		Query query = getSession().createSQLQuery(
				"INSERT INTO ADMHOTOP(ADUSERID,ADOPID,LASTDT) VALUES(? ,? ,?)");
		query.setParameter(0, aduserid);
		query.setParameter(1, adopid);
		query.setParameter(2, lastdt);
		query.executeUpdate();
	}
	
}
