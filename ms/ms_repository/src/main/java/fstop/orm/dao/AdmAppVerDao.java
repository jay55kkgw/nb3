package fstop.orm.dao;

import java.util.Hashtable;
import java.util.List;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;

import fstop.orm.po.ADMAPPVER;
import org.springframework.stereotype.Repository;


@Slf4j
@Repository
public class AdmAppVerDao extends LegacyJpaRepository<ADMAPPVER, Integer> {

	protected Logger logger = Logger.getLogger(getClass());


	public List<ADMAPPVER> getVerData(Hashtable<String, String> params)
	{
		log.debug("parsms.get(APPOS)==>"+params.get("ADOS")+"<==");
		log.debug("parsms.get(ADMODEL)==>"+params.get("ADMODEL")+"<==");
		log.debug("parsms.get(ADVERNO)==>"+params.get("ADVERNO")+"<==");
	
		String queryStr = "FROM ADMAPPVER  ";
		queryStr +=" WHERE 1=1 ";
		if(params.get("ADOS") != null && ((String)params.get("ADOS")).length() > 0 && !((String)params.get("ADOS")).equals("#"))
			queryStr += "AND APPOS = '" + params.get("ADOS") + "' ";
		if(params.get("ADMODEL") != null && ((String)params.get("ADMODEL")).length() > 0)
			queryStr += "AND APPMODELNO = '" + params.get("ADMODEL") + "' ";
		if(params.get("ADVERNO") != null && ((String)params.get("ADVERNO")).length() > 0)
			queryStr += "AND VERNO = '" + params.get("ADVERNO") + "' ";
		
		queryStr += "ORDER BY APPID, APPOS DESC";
		logger.debug("queryStr ===============================> " + queryStr);
		List<ADMAPPVER> result = find(queryStr);
		
		return result;
	}
}
