package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.util.ESAPIUtil;

import fstop.orm.po.TXNLOANAPPLY;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.Date;
import java.util.List;

@Slf4j
@Repository
@Transactional
public class TxnLoanApplyDao extends LegacyJpaRepository<TXNLOANAPPLY, String> {
	protected Logger logger = Logger.getLogger(getClass());

	@SuppressWarnings( { "unused", "unchecked" })
	public List<TXNLOANAPPLY> findByLastDate(String lastDate) {
		
		String sql = "from TXNLOANAPPLY where LASTDATE <= ? AND STATUS='0' order by APPIDNO ";
		
		Query query = getQuery(sql, lastDate);
		
		return ESAPIUtil.validStrList(query.getResultList());
	}
	@SuppressWarnings( { "unused", "unchecked" })
	public List<TXNLOANAPPLY> findByUID(String UID) {
		
		String sql = "from TXNLOANAPPLY where APPIDNO = ? order by APPIDNO ";
		
		Query query = getQuery(sql, UID);		
		
		return query.getResultList();
	}	
	/**
	 * 取得某一日的申請貸款筆數
	 * @param currentDate
	 * @return
	 */
	public int countLoanRecord(Date currentDate) {

		String due = DateTimeUtils.format("yyyyMMdd", currentDate);
		String sql = "from TXNLOANAPPLY where LASTDATE = ? order by APPIDNO ";
		
		Query query = getQuery(sql, due);		
		return query.getResultList().size();
	}	
	/**
	 * 查詢某一筆案件編號
	 * @return
	 */
	public List<TXNLOANAPPLY> findByRCVNO(String rcvno) {
		return find("FROM TXNLOANAPPLY WHERE RCVNO = ?", rcvno);
	}
	/**
	 * 查詢客戶申請案件
	 * @return
	 */
	public List<TXNLOANAPPLY> findByCUSID(String CUSID) {
		return find("FROM TXNLOANAPPLY WHERE APPIDNO = ?", CUSID);
	}
	/**
	 * 變更客戶申請狀態 1:送件中 2:核准 3:不核准
	 * @return
	 */
	@SuppressWarnings({ "unused", "unchecked" })
	public TXNLOANAPPLY setStatus(String RCVNO,String STATUS) {
		
		//String status = "1";
		int isUserExist = -1;
		TXNLOANAPPLY loan = null;
		try {
			loan = findById(StrUtils.trim(RCVNO));
			log.debug("update RCVNO:" + loan.getRCVNO());
			isUserExist = ((loan == null) ? 0 : 1);
		}
		catch(Exception e) {
			isUserExist = 0;
		}
		if(isUserExist == 1 ) {
			
            loan.setSTATUS(STATUS);
			save(loan);
			return loan;
		}

		return null;
	}
	/**
	 * 取得客戶已核准申請案件
	 * @param cusid
	 * @return
	 */
	public List<TXNLOANAPPLY> getRcvno(String cusid) {									
		return find("FROM TXNLOANAPPLY WHERE APPIDNO = ? AND STATUS ='2' order by APPIDNO", cusid);
	}
}