package fstop.orm.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.LegacyJpaRepository;

import fstop.orm.po.ITRN024;
import fstop.orm.po.ITRN030;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Transactional
public class ItrN030Dao extends LegacyJpaRepository<ITRN030, String> {
	protected Logger logger = Logger.getLogger(getClass());
	// private CacheService cache;

	@SuppressWarnings({ "unchecked" })
	public List<ITRN030> getAll() {
		List<ITRN030> result = new ArrayList<ITRN030>();

		// Hashtable<String, Object> N030_Record = new Hashtable<String, Object>();
		result = find("FROM ITRN030 ORDER BY RECNO");
		// N030_Record.put("N030_Record", result);

		log.debug("N030 Result Str ===========> ");

		return result;
	}
	
	public void insert(ITRN030 itrn030) {
		int r2=0;
		String fillstr=new String();
		Query add1=null;
		
		add1=this.getSession().createSQLQuery("INSERT INTO ITRN030 VALUES(? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,?)");
		add1.setParameter(0, itrn030.getRECNO());
		log.debug("RECNO = " + itrn030.getRECNO());
		add1.setParameter(1, itrn030.getHEADER());
		log.debug("HEADER = " + itrn030.getHEADER());
		add1.setParameter(2, itrn030.getSEQ());
		log.debug("SEQ = " + itrn030.getSEQ());
		add1.setParameter(3, itrn030.getDATE());
		log.debug("DATE = " + itrn030.getDATE());
		add1.setParameter(4, itrn030.getTIME());
		log.debug("TIME = " + itrn030.getTIME());
		add1.setParameter(5, itrn030.getOFCRY());
		log.debug("OFCRY = " + itrn030.getOFCRY());
		add1.setParameter(6, itrn030.getCRY());
		log.debug("CRY = " + itrn030.getCRY());
		add1.setParameter(7, itrn030.getBUYITR());
		log.debug("BUYITR = " + itrn030.getBUYITR());
		add1.setParameter(8, itrn030.getSELITR());
		log.debug("SELITR = " + itrn030.getSELITR());
		add1.setParameter(9, itrn030.getTERM());
		log.debug("TERM = " + itrn030.getTERM());
		if(itrn030.getFILL()==null)
			fillstr=" ";
		else
			fillstr=itrn030.getFILL();
		log.debug("FILL = " + fillstr);
		add1.setParameter(10, fillstr);
		r2 = add1.executeUpdate();
	}

	public void deleteAll() {
		this.getSession().createQuery("DELETE FROM ITRN030").executeUpdate();
	}
	
}
