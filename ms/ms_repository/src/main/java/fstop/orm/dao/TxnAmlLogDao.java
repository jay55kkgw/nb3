package fstop.orm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.LegacyJpaRepository;

import fstop.orm.po.TXNAMLLOG;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@Repository
@Transactional
public class TxnAmlLogDao extends LegacyJpaRepository<TXNAMLLOG, String> {

	public List<TXNAMLLOG> findByDate(String date) {
		return find("FROM TXNAMLLOG WHERE TS1 = ? ORDER BY TS1", date);
	}
	/**
	 * 查詢昨日資料
	 * @param date
	 * @return
	 */
	public List<TXNAMLLOG> getAmlLog(String date1) {
		return find("FROM TXNAMLLOG WHERE TS1 LIKE ? ORDER BY LOGID",date1);
	}	
	/**
	 * 查詢特定日期範圍date1~date2
	 * @param date
	 * @return
	 */
	public List<TXNAMLLOG> findByDateRange(String date1,String date2) {
		return find("FROM TXNAMLLOG WHERE TS1 >= ? and TS1 <= ? ORDER BY LOGID",date1, date2);
	}
}
