package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.util.ESAPIUtil;

import fstop.model.Row;
import fstop.model.Rows;
import fstop.orm.po.ADMMONTHREPORT;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.hibernate.query.NativeQuery;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Slf4j
@Repository
@Transactional
public class AdmMonthReportDao extends LegacyJpaRepository<ADMMONTHREPORT, Long> {
	protected Logger logger = Logger.getLogger(getClass());
	
	
	private SysOpGroupDao sysOpGropuDao;
	private void setSysOpGroupDao(SysOpGroupDao dao) {
		this.sysOpGropuDao = dao;
	}
	
	//各類交易數統計表
	public Rows findTransactionReport(String startYYMM, String endYYMM, String logintype) {
	
		Rows result = new Rows();
		String sql = "" + 
		"SELECT SUM(ADCOUNT) AS ADCOUNT, SUM(ADAMOUNT) AS ADAMOUNT, ADUSERTYPE, ADBKFLAG, GROUPNAME FROM                                                                                " + 
		"(                                                                                                                                                                              " + 
		"    SELECT                                                                                                                                                                     " + 
		"        A.ADCOUNT,                                                                                                                                                             " + 
		"        A.ADAMOUNT,                                                                                                                                                            " + 
		"        CASE WHEN SUBSTR(S.ADOPGROUP, 1, 6) IN ('NBGP11', 'NBGP12', 'NBGP13', 'NBGP14', 'NBGP19') THEN '' ELSE A.ADUSERTYPE END AS ADUSERTYPE,                                           " + 
		"        CASE WHEN SUBSTR(S.ADOPGROUP, 1, 6) IN ('NBGP11', 'NBGP12', 'NBGP13', 'NBGP14', 'NBGP19') THEN '' ELSE A.ADBKFLAG END AS ADBKFLAG,                                               " + 
		"        CASE WHEN SUBSTR(S.ADOPGROUP, 1, 6) IN ('NBGP15', 'NBGP16', 'NBGP17', 'NBGP18') THEN 'NBGP151617-' || A.ADUSERTYPE || A.ADBKFLAG ELSE SUBSTR(S.ADOPGROUP, 1, 6) END AS GROUPNAME " + 
		"    FROM ADMMONTHREPORT A LEFT JOIN SYSOPGROUP S ON A.ADOPID = S.ADOPID AND S.ADOPGROUP LIKE 'NBGP%'                                                                           " + 
		"    WHERE A.ADTXSTATUS='0' AND A.CMYYYYMM >= ? AND A.CMYYYYMM <= ?  AND A.LOGINTYPE in ("+logintype+")                                                                                                           " + 
		") X                                                                                                                                                                            " + 
		"GROUP BY ADUSERTYPE, ADBKFLAG, GROUPNAME                                                                                                                                       " + 
		"ORDER BY GROUPNAME                                                                                                                                                             ";
		
		Query query = getSession().createSQLQuery( sql);
		query.setParameter(0, startYYMM);
		query.setParameter(1, endYYMM);
		//query.setParameter(2, logintype);
		query.unwrap(NativeQuery.class).addScalar("ADCOUNT", LongType.INSTANCE)
		 .addScalar("ADAMOUNT", LongType.INSTANCE)
		 .addScalar("ADUSERTYPE", StringType.INSTANCE)
		 .addScalar("ADBKFLAG", StringType.INSTANCE)
		 .addScalar("GROUPNAME", StringType.INSTANCE);

		DecimalFormat df = new DecimalFormat("##0");
		
		// Stored XSS
		List<Object> qresult = query.getResultList();
		if (null == qresult) {
			qresult = new ArrayList();
		}
		qresult = ESAPIUtil.validList(qresult);
//		List qresult = query.getResultList();
		
		Iterator it = qresult.iterator();
		while(it.hasNext()) {
			Object[] o = (Object[])it.next();
			Map rm = new HashMap();
			int t = -1;
						
			rm.put("ADCOUNT", df.format(o[++t]));					
			rm.put("ADAMOUNT", df.format(o[++t]));
			rm.put("ADUSERTYPE", o[++t].toString());
			rm.put("ADBKFLAG", o[++t].toString());
			
			/*
			log.debug("@@@ month report t =="+t);
			log.debug("@@@ month report o.length =="+o.length);
			log.debug("@@@ month report o-0 =="+o[0].toString());
			log.debug("@@@ month report o-1 =="+o[1].toString());
			log.debug("@@@ month report o-2 =="+o[2].toString());
			log.debug("@@@ month report o-3 =="+o[3].toString());
			log.debug("@@@ month report o-4 =="+o[4].toString());
			*/
			
			if (o[t+1] != null)
				rm.put("GROUPNAME", o[++t].toString());
			else
				rm.put("GROUPNAME", "");
				
			Row r = new Row(rm);
			result.addRow(r);
		}
		
		return result;
	}
	
	//一般網路銀行交易量統計表
	public Rows findTransactionReportCount(String startYYMM, String endYYMM, int grpnameLength, String[] gpnames, String logintype) {

		String condition = ""; 
		
		if(gpnames != null && gpnames.length > 0) {
			for(String s : gpnames) {
				if(StrUtils.isNotEmpty(s)) {
					if(condition.length() > 0)
						condition += " OR ";
					condition += " ADOPGROUP like '" + s + "%' ";
				}
			}
		}
		if(StrUtils.isNotEmpty(condition))
			condition = " AND ( " + condition + ") ";
		
		//TYPE: 0 -> 企業戶, 1 個人戶, 2  小計
		//UNIT: 0  筆數, 1 金額
		String sql = "" +	
			" SELECT  " +
			"  SUM( (case when ADBKFLAG ='0' and  ADTXCODE = '0' and ADTXSTATUS = '0' then ADAMOUNT else 0 end)) as AMT000 , " +
			"  SUM( (case when ADBKFLAG ='0' and  ADTXCODE = '0' and ADTXSTATUS = '1' then ADAMOUNT else 0 end)) as AMT001 , " +
			"  SUM( (case when ADBKFLAG ='0' and  ADTXCODE = '1' and ADTXSTATUS = '0' then ADAMOUNT else 0 end)) as AMT010 , " +
			"  SUM( (case when ADBKFLAG ='0' and  ADTXCODE = '1' and ADTXSTATUS = '1' then ADAMOUNT else 0 end)) as AMT011 , " +
			"  SUM( (case when ADBKFLAG ='0' and  ADTXCODE = '2' and ADTXSTATUS = '0' then ADAMOUNT else 0 end)) as AMT020 , " +
			"  SUM( (case when ADBKFLAG ='0' and  ADTXCODE = '2' and ADTXSTATUS = '1' then ADAMOUNT else 0 end)) as AMT021 , " +
			"  SUM( (case when ADBKFLAG ='0' and  ADTXCODE = '3' and ADTXSTATUS = '0' then ADAMOUNT else 0 end)) as AMT030 , " +
			"  SUM( (case when ADBKFLAG ='0' and  ADTXCODE = '3' and ADTXSTATUS = '1' then ADAMOUNT else 0 end)) as AMT031 , " +			
			"  SUM( (case when ADBKFLAG ='1' and  ADTXCODE = '0' and ADTXSTATUS = '0' then ADAMOUNT else 0 end)) as AMT100 , " +
			"  SUM( (case when ADBKFLAG ='1' and  ADTXCODE = '0' and ADTXSTATUS = '1' then ADAMOUNT else 0 end)) as AMT101 , " +
			"  SUM( (case when ADBKFLAG ='1' and  ADTXCODE = '1' and ADTXSTATUS = '0' then ADAMOUNT else 0 end)) as AMT110 , " +
			"  SUM( (case when ADBKFLAG ='1' and  ADTXCODE = '1' and ADTXSTATUS = '1' then ADAMOUNT else 0 end)) as AMT111 , " +
			"  SUM( (case when ADBKFLAG ='1' and  ADTXCODE = '2' and ADTXSTATUS = '0' then ADAMOUNT else 0 end)) as AMT120 , " +
			"  SUM( (case when ADBKFLAG ='1' and  ADTXCODE = '2' and ADTXSTATUS = '1' then ADAMOUNT else 0 end)) as AMT121 , " +
			"  SUM( (case when ADBKFLAG ='1' and  ADTXCODE = '3' and ADTXSTATUS = '0' then ADAMOUNT else 0 end)) as AMT130 , " +
			"  SUM( (case when ADBKFLAG ='1' and  ADTXCODE = '3' and ADTXSTATUS = '1' then ADAMOUNT else 0 end)) as AMT131 , " +
			"  SUM( (case when ADBKFLAG ='0' and  ADTXCODE = '0' and ADTXSTATUS = '0' then ADCOUNT else 0 end)) as CNT000 , " +
			"  SUM( (case when ADBKFLAG ='0' and  ADTXCODE = '0' and ADTXSTATUS = '1' then ADCOUNT else 0 end)) as CNT001 , " +
			"  SUM( (case when ADBKFLAG ='0' and  ADTXCODE = '1' and ADTXSTATUS = '0' then ADCOUNT else 0 end)) as CNT010 , " +
			"  SUM( (case when ADBKFLAG ='0' and  ADTXCODE = '1' and ADTXSTATUS = '1' then ADCOUNT else 0 end)) as CNT011 , " +
			"  SUM( (case when ADBKFLAG ='0' and  ADTXCODE = '2' and ADTXSTATUS = '0' then ADCOUNT else 0 end)) as CNT020 , " +
			"  SUM( (case when ADBKFLAG ='0' and  ADTXCODE = '2' and ADTXSTATUS = '1' then ADCOUNT else 0 end)) as CNT021 , " +
			"  SUM( (case when ADBKFLAG ='0' and  ADTXCODE = '3' and ADTXSTATUS = '0' then ADCOUNT else 0 end)) as CNT030 , " +
			"  SUM( (case when ADBKFLAG ='0' and  ADTXCODE = '3' and ADTXSTATUS = '1' then ADCOUNT else 0 end)) as CNT031 , " +
			"  SUM( (case when ADBKFLAG ='1' and  ADTXCODE = '0' and ADTXSTATUS = '0' then ADCOUNT else 0 end)) as CNT100 , " +
			"  SUM( (case when ADBKFLAG ='1' and  ADTXCODE = '0' and ADTXSTATUS = '1' then ADCOUNT else 0 end)) as CNT101 , " +
			"  SUM( (case when ADBKFLAG ='1' and  ADTXCODE = '1' and ADTXSTATUS = '0' then ADCOUNT else 0 end)) as CNT110 , " +
			"  SUM( (case when ADBKFLAG ='1' and  ADTXCODE = '1' and ADTXSTATUS = '1' then ADCOUNT else 0 end)) as CNT111 , " +
			"  SUM( (case when ADBKFLAG ='1' and  ADTXCODE = '2' and ADTXSTATUS = '0' then ADCOUNT else 0 end)) as CNT120 , " +
			"  SUM( (case when ADBKFLAG ='1' and  ADTXCODE = '2' and ADTXSTATUS = '1' then ADCOUNT else 0 end)) as CNT121 , " +
			"  SUM( (case when ADBKFLAG ='1' and  ADTXCODE = '3' and ADTXSTATUS = '0' then ADCOUNT else 0 end)) as CNT130 , " +
			"  SUM( (case when ADBKFLAG ='1' and  ADTXCODE = '3' and ADTXSTATUS = '1' then ADCOUNT else 0 end)) as CNT131 , " +
			"  SUM( (case when ADTXSTATUS = '0' then ADAMOUNT else 0 end )) as AMT_SUCCESS_SUM , " +
			"  SUM( (case when ADTXSTATUS = '1' then ADAMOUNT else 0 end )) as AMT_FAIL_SUM , " +
			"  SUM( (case when ADTXSTATUS = '0' then ADCOUNT  else 0 end )) as CNT_SUCCESS_SUM , " +
			"  SUM( (case when ADTXSTATUS = '1' then ADCOUNT  else 0 end )) as CNT_FAIL_SUM , " +
			"  varchar(ADOPGROUP) as ADOPGROUP , " +
			"  ADUSERTYPE,  " +
			"  ADOPNAME  " +
			" FROM " +
			"" +
			"" +
			"   (" +
			"     SELECT  substr(s.ADOPGROUP, 1, " + grpnameLength + ") as ADOPGROUP, ap.ADOPNAME, a.ADTXSTATUS, a.ADAMOUNT, a.ADCOUNT, case when substr(ADOPGROUP, 1, 6) in ('NBGP11', 'NBGP12', 'NBGP13', 'NBGP14', 'NBGP15', 'NBGP18') then varchar(ADOPGROUP) else varchar(a.ADUSERTYPE) end as  ADUSERTYPE , case when substr(ADOPGROUP, 1, 6) in ('NBGP11', 'NBGP12', 'NBGP13', 'NBGP14', 'NBGP15', 'NBGP18') then '0' else a.ADBKFLAG end as ADBKFLAG, a.CMYYYYMM, case when a.ADTXCODE = '' then '0' else a.ADTXCODE end as ADTXCODE, a.LOGINTYPE   " +
			"     FROM ADMMONTHREPORT a left join SYSOPGROUP s on a.ADOPID=s.ADOPID  left join SYSOP ap on substr(s.ADOPGROUP, 1, " + grpnameLength + ")=ap.ADOPID " +
			"     where s.ADOPGROUP like 'NBGP%' " + condition + 
			"   ) s " +
			"   " +
			"" +
			"" +
			"   WHERE    CMYYYYMM >= '" + startYYMM + "' and CMYYYYMM <= '"+endYYMM+"' AND LOGINTYPE in ("+logintype+") " + 
			" GROUP BY ADOPGROUP, ADOPNAME, ADUSERTYPE  ORDER BY ADOPGROUP, ADOPNAME, ADUSERTYPE  ASC ";

		
		Query query = getSession().createSQLQuery(sql);
		
		logger.debug("\r\n[SQL:]\r\n" + sql);
		
		//query.setParameter(0, startYYMM);
		//query.setParameter(1, endYYMM);
		
		Rows result = new Rows();
		//String[] ary = query.getReturnAliases();
		
		// Stored XSS
		List<Object> qresult = query.getResultList();
		if (null == qresult) {
			qresult = new ArrayList();
		}
		qresult = ESAPIUtil.validList(qresult);
//		List qresult = query.getResultList();
		
		DecimalFormat df = new DecimalFormat("##0");
		Iterator it = qresult.iterator();
		while(it.hasNext()) {
			Object[] o = (Object[])it.next();
			Map rm = new HashMap();
			int t = -1;

			rm.put("AMT000", df.format(o[++t]));
			rm.put("AMT001", df.format(o[++t]));
			rm.put("AMT010", df.format(o[++t]));
			rm.put("AMT011", df.format(o[++t]));
			rm.put("AMT020", df.format(o[++t]));
			rm.put("AMT021", df.format(o[++t]));
			rm.put("AMT030", df.format(o[++t]));
			rm.put("AMT031", df.format(o[++t]));
			rm.put("AMT100", df.format(o[++t]));
			rm.put("AMT101", df.format(o[++t]));
			rm.put("AMT110", df.format(o[++t]));
			rm.put("AMT111", df.format(o[++t]));
			rm.put("AMT120", df.format(o[++t]));
			rm.put("AMT121", df.format(o[++t]));
			rm.put("AMT130", df.format(o[++t]));
			rm.put("AMT131", df.format(o[++t]));
			rm.put("CNT000", df.format(o[++t]));
			rm.put("CNT001", df.format(o[++t]));
			rm.put("CNT010", df.format(o[++t]));
			rm.put("CNT011", df.format(o[++t]));
			rm.put("CNT020", df.format(o[++t]));
			rm.put("CNT021", df.format(o[++t]));
			rm.put("CNT030", df.format(o[++t]));
			rm.put("CNT031", df.format(o[++t]));
			rm.put("CNT100", df.format(o[++t]));
			rm.put("CNT101", df.format(o[++t]));
			rm.put("CNT110", df.format(o[++t]));
			rm.put("CNT111", df.format(o[++t]));
			rm.put("CNT120", df.format(o[++t]));
			rm.put("CNT121", df.format(o[++t]));
			rm.put("CNT130", df.format(o[++t]));
			rm.put("CNT131", df.format(o[++t]));
			rm.put("AMT_SUCCESS_SUM", df.format(o[++t]));
			rm.put("AMT_FAIL_SUM", df.format(o[++t]));
			rm.put("CNT_SUCCESS_SUM", df.format(o[++t]));
			rm.put("CNT_FAIL_SUM", df.format(o[++t]));
			rm.put("ADOPGROUP", o[++t].toString());
			rm.put("ADUSERTYPE", o[++t].toString());
			rm.put("ADOPNAME", o[++t].toString());
			
			Row r = new Row(rm);
			result.addRow(r);
		}
		
		return result;
	}
	
	 
	public Rows findTransactionReportAgree(String startYYMM, String endYYMM, int grpnameLength, String[] gpnames) {

		String condition = ""; 
		
		if(gpnames != null && gpnames.length > 0) {
			for(String s : gpnames) {
				if(StrUtils.isNotEmpty(s)) {
					if(condition.length() > 0)
						condition += " OR ";
					condition += " ADOPGROUP like'" + s + "%' ";
				}
			}
		}
		if(StrUtils.isNotEmpty(condition))
			condition = " AND ( " + condition + ") ";

		//TYPE: 0 -> 企業戶, 1 個人戶, 2  小計
		//UNIT: 0  筆數, 1 金額
		String sql = "" +	
			" SELECT  " +
			"  SUM( (case when ADUSERTYPE ='0' and  ADTXCODE = '0' and ADTXSTATUS = '0' then ADAMOUNT else 0 end)) as AMT000 , " +
			"  SUM( (case when ADUSERTYPE ='0' and  ADTXCODE = '0' and ADTXSTATUS = '1' then ADAMOUNT else 0 end)) as AMT001 , " +
			"  SUM( (case when ADUSERTYPE ='0' and  ADTXCODE = '1' and ADTXSTATUS = '0' then ADAMOUNT else 0 end)) as AMT010 , " +
			"  SUM( (case when ADUSERTYPE ='0' and  ADTXCODE = '1' and ADTXSTATUS = '1' then ADAMOUNT else 0 end)) as AMT011 , " +
			"  SUM( (case when ADUSERTYPE ='0' and  ADTXCODE = '2' and ADTXSTATUS = '0' then ADAMOUNT else 0 end)) as AMT020 , " +
			"  SUM( (case when ADUSERTYPE ='0' and  ADTXCODE = '2' and ADTXSTATUS = '1' then ADAMOUNT else 0 end)) as AMT021 , " +
			"  SUM( (case when ADUSERTYPE ='0' and  ADTXCODE = '3' and ADTXSTATUS = '0' then ADAMOUNT else 0 end)) as AMT030 , " +
			"  SUM( (case when ADUSERTYPE ='0' and  ADTXCODE = '3' and ADTXSTATUS = '1' then ADAMOUNT else 0 end)) as AMT031 , " +			
			"  SUM( (case when ADUSERTYPE ='1' and  ADTXCODE = '0' and ADTXSTATUS = '0' then ADAMOUNT else 0 end)) as AMT100 , " +
			"  SUM( (case when ADUSERTYPE ='1' and  ADTXCODE = '0' and ADTXSTATUS = '1' then ADAMOUNT else 0 end)) as AMT101 , " +
			"  SUM( (case when ADUSERTYPE ='1' and  ADTXCODE = '1' and ADTXSTATUS = '0' then ADAMOUNT else 0 end)) as AMT110 , " +
			"  SUM( (case when ADUSERTYPE ='1' and  ADTXCODE = '1' and ADTXSTATUS = '1' then ADAMOUNT else 0 end)) as AMT111 , " +
			"  SUM( (case when ADUSERTYPE ='1' and  ADTXCODE = '2' and ADTXSTATUS = '0' then ADAMOUNT else 0 end)) as AMT120 , " +
			"  SUM( (case when ADUSERTYPE ='1' and  ADTXCODE = '2' and ADTXSTATUS = '1' then ADAMOUNT else 0 end)) as AMT121 , " +
			"  SUM( (case when ADUSERTYPE ='1' and  ADTXCODE = '3' and ADTXSTATUS = '0' then ADAMOUNT else 0 end)) as AMT130 , " +
			"  SUM( (case when ADUSERTYPE ='1' and  ADTXCODE = '3' and ADTXSTATUS = '1' then ADAMOUNT else 0 end)) as AMT131 , " +			
			"  SUM( (case when ADUSERTYPE ='0' and  ADTXCODE = '0' and ADTXSTATUS = '0' then ADCOUNT else 0 end)) as CNT000 , " +
			"  SUM( (case when ADUSERTYPE ='0' and  ADTXCODE = '0' and ADTXSTATUS = '1' then ADCOUNT else 0 end)) as CNT001 , " +
			"  SUM( (case when ADUSERTYPE ='0' and  ADTXCODE = '1' and ADTXSTATUS = '0' then ADCOUNT else 0 end)) as CNT010 , " +
			"  SUM( (case when ADUSERTYPE ='0' and  ADTXCODE = '1' and ADTXSTATUS = '1' then ADCOUNT else 0 end)) as CNT011 , " +
			"  SUM( (case when ADUSERTYPE ='0' and  ADTXCODE = '2' and ADTXSTATUS = '0' then ADCOUNT else 0 end)) as CNT020 , " +
			"  SUM( (case when ADUSERTYPE ='0' and  ADTXCODE = '2' and ADTXSTATUS = '1' then ADCOUNT else 0 end)) as CNT021 , " +
			"  SUM( (case when ADUSERTYPE ='0' and  ADTXCODE = '3' and ADTXSTATUS = '0' then ADCOUNT else 0 end)) as CNT030 , " +
			"  SUM( (case when ADUSERTYPE ='0' and  ADTXCODE = '3' and ADTXSTATUS = '1' then ADCOUNT else 0 end)) as CNT031 , " +
			"  SUM( (case when ADUSERTYPE ='1' and  ADTXCODE = '0' and ADTXSTATUS = '0' then ADCOUNT else 0 end)) as CNT100 , " +
			"  SUM( (case when ADUSERTYPE ='1' and  ADTXCODE = '0' and ADTXSTATUS = '1' then ADCOUNT else 0 end)) as CNT101 , " +
			"  SUM( (case when ADUSERTYPE ='1' and  ADTXCODE = '1' and ADTXSTATUS = '0' then ADCOUNT else 0 end)) as CNT110 , " +
			"  SUM( (case when ADUSERTYPE ='1' and  ADTXCODE = '1' and ADTXSTATUS = '1' then ADCOUNT else 0 end)) as CNT111 , " +
			"  SUM( (case when ADUSERTYPE ='1' and  ADTXCODE = '2' and ADTXSTATUS = '0' then ADCOUNT else 0 end)) as CNT120 , " +
			"  SUM( (case when ADUSERTYPE ='1' and  ADTXCODE = '2' and ADTXSTATUS = '1' then ADCOUNT else 0 end)) as CNT121 , " +
			"  SUM( (case when ADUSERTYPE ='1' and  ADTXCODE = '3' and ADTXSTATUS = '0' then ADCOUNT else 0 end)) as CNT130 , " +
			"  SUM( (case when ADUSERTYPE ='1' and  ADTXCODE = '3' and ADTXSTATUS = '1' then ADCOUNT else 0 end)) as CNT131 , " +			
			"  SUM( (case when ADTXSTATUS = '0' then ADAMOUNT else 0 end )) as AMT_SUCCESS_SUM , " +
			"  SUM( (case when ADTXSTATUS = '1' then ADAMOUNT else 0 end )) as AMT_FAIL_SUM , " +
			"  SUM( (case when ADTXSTATUS = '0' then ADCOUNT  else 0 end )) as CNT_SUCCESS_SUM , " +
			"  SUM( (case when ADTXSTATUS = '1' then ADCOUNT  else 0 end )) as CNT_FAIL_SUM , " +
			"  varchar(ADOPGROUP) as ADOPGROUP , " +
			"  ADAGREEF,  " +
			"  ADOPNAME  " +
			" FROM " +
			"" +
			"" +
			"   (" +
			"     SELECT  substr(s.ADOPGROUP, 1, " + grpnameLength + ") as ADOPGROUP, a.ADOPID, ap.ADOPNAME, a.ADTXSTATUS, a.ADAMOUNT, a.ADCOUNT, a.ADUSERTYPE, a.CMYYYYMM, a.ADTXCODE, a.ADAGREEF, a.LOGINTYPE   " +
			"     FROM ADMMONTHREPORT a left join " +
			"     SYSOPGROUP s on a.ADOPID=s.ADOPID  left join SYSOP ap on substr(s.ADOPGROUP, 1, " + grpnameLength + ")=ap.ADOPID where s.ADOPGROUP like 'NB%' " + condition + 
			"   ) s " +
			"   " +
			"" +
			"" +
			"   WHERE    CMYYYYMM >= '" + startYYMM + "' and CMYYYYMM <= '"+endYYMM+"' " + 
			" GROUP BY ADOPGROUP, ADOPNAME, ADAGREEF  ORDER BY ADOPGROUP, ADOPNAME, ADAGREEF  ASC ";

		
		Query query = getSession().createSQLQuery(sql);
		
		logger.debug("\r\n[SQL:]\r\n" + sql);
		
		//query.setParameter(0, startYYMM);
		//query.setParameter(1, endYYMM);
		
		Rows result = new Rows();
		//String[] ary = query.getReturnAliases();
		
		// Stored XSS
		List<Object> qresult = query.getResultList();
		if (null == qresult) {
			qresult = new ArrayList();
		}
		qresult = ESAPIUtil.validList(qresult);
//		List qresult = query.getResultList();
		
		DecimalFormat df = new DecimalFormat("##0");
		Iterator it = qresult.iterator();
		while(it.hasNext()) {
			Object[] o = (Object[])it.next();
			Map rm = new HashMap();
			int t = -1;

			rm.put("AMT000", df.format(o[++t]));			
			rm.put("AMT001", df.format(o[++t]));
			rm.put("AMT010", df.format(o[++t]));
			rm.put("AMT011", df.format(o[++t]));
			rm.put("AMT020", df.format(o[++t]));
			rm.put("AMT021", df.format(o[++t]));
			rm.put("AMT030", df.format(o[++t]));
			rm.put("AMT031", df.format(o[++t]));
			rm.put("AMT100", df.format(o[++t]));
			rm.put("AMT101", df.format(o[++t]));
			rm.put("AMT110", df.format(o[++t]));
			rm.put("AMT111", df.format(o[++t]));
			rm.put("AMT120", df.format(o[++t]));
			rm.put("AMT121", df.format(o[++t]));
			rm.put("AMT130", df.format(o[++t]));
			rm.put("AMT131", df.format(o[++t]));
			rm.put("CNT000", df.format(o[++t]));
			rm.put("CNT001", df.format(o[++t]));
			rm.put("CNT010", df.format(o[++t]));
			rm.put("CNT011", df.format(o[++t]));
			rm.put("CNT020", df.format(o[++t]));
			rm.put("CNT021", df.format(o[++t]));
			rm.put("CNT030", df.format(o[++t]));
			rm.put("CNT031", df.format(o[++t]));
			rm.put("CNT100", df.format(o[++t]));
			rm.put("CNT101", df.format(o[++t]));
			rm.put("CNT110", df.format(o[++t]));
			rm.put("CNT111", df.format(o[++t]));
			rm.put("CNT120", df.format(o[++t]));
			rm.put("CNT121", df.format(o[++t]));
			rm.put("CNT130", df.format(o[++t]));
			rm.put("CNT131", df.format(o[++t]));
			rm.put("AMT_SUCCESS_SUM", df.format(o[++t]));
			rm.put("AMT_FAIL_SUM", df.format(o[++t]));
			rm.put("CNT_SUCCESS_SUM", df.format(o[++t]));
			rm.put("CNT_FAIL_SUM", df.format(o[++t]));
			rm.put("ADOPGROUP", o[++t].toString());
			rm.put("ADAGREEF", o[++t].toString());
			rm.put("ADOPNAME", o[++t].toString());
			
			Row r = new Row(rm);
			result.addRow(r);
		}
		
		return result;
	}
	
	
	
	
	public Rows findTransactionReportCountUsingAdopid(String startYYMM, String endYYMM, String[] gpnames,String logintype) {

		String condition = ""; 
		
		if(gpnames != null && gpnames.length > 0) {
			for(String s : gpnames) {
				if(StrUtils.isNotEmpty(s)) {
					if(condition.length() > 0)
						condition += " OR ";
					condition += " ADOPGROUP like'" + s + "%' ";
				}
			}
		}
		if(StrUtils.isNotEmpty(condition))
			condition = " AND ( " + condition + ") ";

		//TYPE: 0 -> 企業戶, 1 個人戶, 2  小計
		//UNIT: 0  筆數, 1 金額
		String sql = "" +	
			" SELECT  " +
			"  SUM( (case when ADBKFLAG ='0' and  ADTXCODE = '0' and ADTXSTATUS = '0' then ADAMOUNT else 0 end)) as AMT000 , " +
			"  SUM( (case when ADBKFLAG ='0' and  ADTXCODE = '0' and ADTXSTATUS = '1' then ADAMOUNT else 0 end)) as AMT001 , " +
			"  SUM( (case when ADBKFLAG ='0' and  ADTXCODE = '1' and ADTXSTATUS = '0' then ADAMOUNT else 0 end)) as AMT010 , " +
			"  SUM( (case when ADBKFLAG ='0' and  ADTXCODE = '1' and ADTXSTATUS = '1' then ADAMOUNT else 0 end)) as AMT011 , " +
			"  SUM( (case when ADBKFLAG ='1' and  ADTXCODE = '0' and ADTXSTATUS = '0' then ADAMOUNT else 0 end)) as AMT100 , " +
			"  SUM( (case when ADBKFLAG ='1' and  ADTXCODE = '0' and ADTXSTATUS = '1' then ADAMOUNT else 0 end)) as AMT101 , " +
			"  SUM( (case when ADBKFLAG ='1' and  ADTXCODE = '1' and ADTXSTATUS = '0' then ADAMOUNT else 0 end)) as AMT110 , " +
			"  SUM( (case when ADBKFLAG ='1' and  ADTXCODE = '1' and ADTXSTATUS = '1' then ADAMOUNT else 0 end)) as AMT111 , " +
			"  SUM( (case when ADBKFLAG ='1' and  ADTXCODE = '2' and ADTXSTATUS = '0' then ADAMOUNT else 0 end)) as AMT120 , " +
			"  SUM( (case when ADBKFLAG ='1' and  ADTXCODE = '2' and ADTXSTATUS = '1' then ADAMOUNT else 0 end)) as AMT121 , " +
			"  SUM( (case when ADBKFLAG ='1' and  ADTXCODE = '3' and ADTXSTATUS = '0' then ADAMOUNT else 0 end)) as AMT130 , " +
			"  SUM( (case when ADBKFLAG ='1' and  ADTXCODE = '3' and ADTXSTATUS = '1' then ADAMOUNT else 0 end)) as AMT131 , " +			
			"  SUM( (case when ADBKFLAG ='0' and  ADTXCODE = '0' and ADTXSTATUS = '0' then ADCOUNT else 0 end)) as CNT000 , " +
			"  SUM( (case when ADBKFLAG ='0' and  ADTXCODE = '0' and ADTXSTATUS = '1' then ADCOUNT else 0 end)) as CNT001 , " +
			"  SUM( (case when ADBKFLAG ='0' and  ADTXCODE = '1' and ADTXSTATUS = '0' then ADCOUNT else 0 end)) as CNT010 , " +
			"  SUM( (case when ADBKFLAG ='0' and  ADTXCODE = '1' and ADTXSTATUS = '1' then ADCOUNT else 0 end)) as CNT011 , " +
			"  SUM( (case when ADBKFLAG ='1' and  ADTXCODE = '0' and ADTXSTATUS = '0' then ADCOUNT else 0 end)) as CNT100 , " +
			"  SUM( (case when ADBKFLAG ='1' and  ADTXCODE = '0' and ADTXSTATUS = '1' then ADCOUNT else 0 end)) as CNT101 , " +
			"  SUM( (case when ADBKFLAG ='1' and  ADTXCODE = '1' and ADTXSTATUS = '0' then ADCOUNT else 0 end)) as CNT110 , " +
			"  SUM( (case when ADBKFLAG ='1' and  ADTXCODE = '1' and ADTXSTATUS = '1' then ADCOUNT else 0 end)) as CNT111 , " +
			"  SUM( (case when ADBKFLAG ='1' and  ADTXCODE = '2' and ADTXSTATUS = '0' then ADCOUNT else 0 end)) as CNT120 , " +
			"  SUM( (case when ADBKFLAG ='1' and  ADTXCODE = '2' and ADTXSTATUS = '1' then ADCOUNT else 0 end)) as CNT121 , " +
			"  SUM( (case when ADBKFLAG ='1' and  ADTXCODE = '3' and ADTXSTATUS = '0' then ADCOUNT else 0 end)) as CNT130 , " +
			"  SUM( (case when ADBKFLAG ='1' and  ADTXCODE = '3' and ADTXSTATUS = '1' then ADCOUNT else 0 end)) as CNT131 , " +
			"  SUM( (case when ADTXSTATUS = '0' then ADAMOUNT else 0 end )) as AMT_SUCCESS_SUM , " +
			"  SUM( (case when ADTXSTATUS = '1' then ADAMOUNT else 0 end )) as AMT_FAIL_SUM , " +
			"  SUM( (case when ADTXSTATUS = '0' then ADCOUNT  else 0 end )) as CNT_SUCCESS_SUM , " +
			"  SUM( (case when ADTXSTATUS = '1' then ADCOUNT  else 0 end )) as CNT_FAIL_SUM , " +
			"  varchar(ADOPID) as ADOPGROUP , " +
			"  ADUSERTYPE," +
			"  ADOPNAME  " +
			" FROM " +
			"" +
			"" +
			"   (" +
			"     SELECT  substr(s.ADOPGROUP, 1, 6) as ADOPGROUP, a.ADOPID, ap.ADOPNAME, a.ADTXSTATUS, a.ADAMOUNT, a.ADLEVEL, a.ADCOUNT, a.LOGINTYPE " +
			"             case when substr(ADOPGROUP, 1, 4) = 'NBGP15' then varchar(ADOPGROUP) else varchar(a.ADUSERTYPE) end as  ADUSERTYPE , " +
			"             a.ADBKFLAG, a.CMYYYYMM, a.ADTXCODE   " +
			"     FROM ADMMONTHREPORT a left join " +
			"     SYSOPGROUP s on a.ADOPID=s.ADOPID left join SYSOP ap on s.ADOPID=ap.ADOPID  where s.ADOPGROUP like 'NB%' " + condition + 
			"   ) s " +
			"   " +
			"" +
			"" +
			"   WHERE    CMYYYYMM >= '" + startYYMM + "' and CMYYYYMM <= '"+endYYMM+"' AND LOGINTYPE in ("+logintype+") " + 
			" GROUP BY ADOPID, ADOPNAME, ADUSERTYPE  ORDER BY ADOPID, ADOPNAME, ADUSERTYPE  ASC ";

		
		Query query = getSession().createSQLQuery(sql);
		
		logger.debug("\r\n[SQL:]\r\n" + sql);
		
		//query.setParameter(0, startYYMM);
		//query.setParameter(1, endYYMM); 
		
		Rows result = new Rows();
		//String[] ary = query.getReturnAliases();
		
		// Stored XSS
		List<Object> qresult = query.getResultList();
		if (null == qresult) {
			qresult = new ArrayList();
		}
		qresult = ESAPIUtil.validList(qresult);
//		List qresult = query.getResultList();
		
		DecimalFormat df = new DecimalFormat("##0");
		Iterator it = qresult.iterator();
		while(it.hasNext()) {
			Object[] o = (Object[])it.next();
			Map rm = new HashMap();
			int t = -1;

			rm.put("AMT000", df.format(o[++t]));
			rm.put("AMT001", df.format(o[++t]));
			rm.put("AMT010", df.format(o[++t]));
			rm.put("AMT011", df.format(o[++t]));
			rm.put("AMT100", df.format(o[++t]));
			rm.put("AMT101", df.format(o[++t]));
			rm.put("AMT110", df.format(o[++t]));
			rm.put("AMT111", df.format(o[++t]));
			rm.put("AMT120", df.format(o[++t]));
			rm.put("AMT121", df.format(o[++t]));
			rm.put("AMT130", df.format(o[++t]));
			rm.put("AMT131", df.format(o[++t]));
			rm.put("CNT000", df.format(o[++t]));
			rm.put("CNT001", df.format(o[++t]));
			rm.put("CNT010", df.format(o[++t]));
			rm.put("CNT011", df.format(o[++t]));
			rm.put("CNT100", df.format(o[++t]));
			rm.put("CNT101", df.format(o[++t]));
			rm.put("CNT110", df.format(o[++t]));
			rm.put("CNT111", df.format(o[++t]));
			rm.put("CNT120", df.format(o[++t]));
			rm.put("CNT121", df.format(o[++t]));
			rm.put("CNT130", df.format(o[++t]));
			rm.put("CNT131", df.format(o[++t]));
			rm.put("AMT_SUCCESS_SUM", df.format(o[++t]));
			rm.put("AMT_FAIL_SUM", df.format(o[++t]));
			rm.put("CNT_SUCCESS_SUM", df.format(o[++t]));
			rm.put("CNT_FAIL_SUM", df.format(o[++t]));
			rm.put("ADOPGROUP", o[++t].toString());
			rm.put("ADUSERTYPE", o[++t].toString());
			rm.put("ADOPNAME", o[++t].toString());
			
			Row r = new Row(rm);
			result.addRow(r);
		}
		
		return result;
	}
	
	//級距,
	public Rows findTransactionReportLevel(String startYYMM, String endYYMM, int grpnameLength, String[] gpnames) {

		String condition = ""; 
		
		if(gpnames != null && gpnames.length > 0) {
			for(String s : gpnames) {
				if(StrUtils.isNotEmpty(s)) {
					if(condition.length() > 0)
						condition += " OR ";
					condition += " ADOPGROUP like'" + s + "%' ";
				}
			}
		}
		if(StrUtils.isNotEmpty(condition))
			condition = " AND ( " + condition + ") ";

		//TYPE: 0 -> 企業戶, 1 個人戶, 2  小計
		//UNIT: 0  筆數, 1 金額
		String sql = "" +	
			" SELECT  " +
			"  SUM( (case when ADLEVEL ='00' then ADCOUNT else 0 end)) as CNT00 , " +
			"  SUM( (case when ADLEVEL ='01' then ADCOUNT else 0 end)) as CNT01 , " +
			"  SUM( (case when ADLEVEL ='02' then ADCOUNT else 0 end)) as CNT02 , " +
			"  SUM( (case when ADLEVEL ='03' then ADCOUNT else 0 end)) as CNT03 , " +
			"  SUM( (case when ADLEVEL ='04' then ADCOUNT else 0 end)) as CNT04 , " +
			"  SUM( (case when ADLEVEL ='05' then ADCOUNT else 0 end)) as CNT05 , " +
			"  SUM( (case when ADLEVEL ='06' then ADCOUNT else 0 end)) as CNT06 , " +
			"  SUM( (case when ADLEVEL ='07' then ADCOUNT else 0 end)) as CNT07 , " +
			"  SUM( (case when ADLEVEL ='08' then ADCOUNT else 0 end)) as CNT08 , " +
			"  SUM( (case when ADLEVEL ='09' then ADCOUNT else 0 end)) as CNT09 , " +
			"  SUM( (case when ADLEVEL ='10' then ADCOUNT else 0 end)) as CNT10 , " +
			"  SUM( (case when ADLEVEL ='11' then ADCOUNT else 0 end)) as CNT11 , " +
			"  SUM( (case when ADLEVEL ='12' then ADCOUNT else 0 end)) as CNT12 , " +
			"  SUM( (case when ADLEVEL ='13' then ADCOUNT else 0 end)) as CNT13 , " +
			"  varchar(ADOPGROUP) as ADOPGROUP , " +
			"  ADAGREEF  " +
			" FROM " +
			"" +
			"" +
			"   (" +
			"     SELECT  substr(s.ADOPGROUP, 1, " + grpnameLength + ") as ADOPGROUP, a.ADOPID, a.ADLEVEL, a.ADCOUNT, a.CMYYYYMM, a.ADAGREEF   " +
			"     FROM ADMMONTHREPORT a left join " +
			"     SYSOPGROUP s on a.ADOPID=s.ADOPID where s.ADOPGROUP like 'NB%' " + condition + " AND ADTXSTATUS='0'   " + 
			"   ) s " +
			"   " +
			"" +
			"" +
			"   WHERE    CMYYYYMM >= '" + startYYMM + "' and CMYYYYMM <= '"+endYYMM+"' " + 
			" GROUP BY ADOPGROUP, ADAGREEF  ORDER BY ADOPGROUP, ADAGREEF  ASC ";

		
		Query query = getSession().createSQLQuery(sql);
		
		logger.debug("\r\n[SQL:]\r\n" + sql);
		
		//query.setParameter(0, startYYMM);
		//query.setParameter(1, endYYMM);
		
		Rows result = new Rows();
		//String[] ary = query.getReturnAliases();
		
		// Stored XSS
		List<Object> qresult = query.getResultList();
		if (null == qresult) {
			qresult = new ArrayList();
		}
		qresult = ESAPIUtil.validList(qresult);
//		List qresult = query.getResultList();
		
		DecimalFormat df = new DecimalFormat("##0");
		Iterator it = qresult.iterator();
		while(it.hasNext()) {
			Object[] o = (Object[])it.next();
			Map rm = new HashMap();
			int t = -1;

			rm.put("CNT00", df.format(o[++t]));
			rm.put("CNT01", df.format(o[++t]));
			rm.put("CNT02", df.format(o[++t]));
			rm.put("CNT03", df.format(o[++t]));
			rm.put("CNT04", df.format(o[++t]));
			rm.put("CNT05", df.format(o[++t]));
			rm.put("CNT06", df.format(o[++t]));
			rm.put("CNT07", df.format(o[++t]));
			rm.put("CNT08", df.format(o[++t]));
			rm.put("CNT09", df.format(o[++t]));
			rm.put("CNT10", df.format(o[++t]));
			rm.put("CNT11", df.format(o[++t]));
			rm.put("CNT12", df.format(o[++t]));
			rm.put("CNT13", df.format(o[++t]));
			rm.put("ADOPGROUP", o[++t].toString());
			rm.put("ADAGREEF", o[++t].toString());
			
			Row r = new Row(rm);
			result.addRow(r);
		}
		
		return result;
	}
	
//	@Transactional(propagation = Propagation.NEVER)
	@Transactional
	public void execCalProcedure(String yyyy, String mm) {
		Query query = getSession().createSQLQuery("{call CAL_ADMMONTHREPORT(?,?)}");
		query.setParameter(0, yyyy);
		query.setParameter(1, mm);
		query.executeUpdate();		
	}
	
}
