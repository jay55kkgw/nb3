package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.model.Rows;
import fstop.orm.po.TXNFUNDDATA;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.hibernate.query.NativeQuery;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.List;

@Slf4j
@Repository
@Transactional
public class TxnFundDataDao extends LegacyJpaRepository<TXNFUNDDATA, String> {
	
	@Transactional(rollbackFor = Exception.class)
    public Boolean saveData(MVHImpl result) {
    	Boolean rs = Boolean.FALSE;
    	log.info("saveData...");
    	Rows rows = result.getOccurs();
    	TXNFUNDDATA po =null;
    	try {
			deleteAll();
//			int i =0;
			for(Row r :rows.getRows()) {
				po = new TXNFUNDDATA();
				po = CodeUtil.objectCovert(TXNFUNDDATA.class, r.getValues());
				log.trace("row>>{}", CodeUtil.toJson(r.getValues()));
				log.trace("TXNFUNDDATA>>{}", po.toString());
//				if(i==10) {
//					po = null;
//				}
				save(po);
//				i++;
			}
			log.info("saveData...end");
			rs = Boolean.TRUE;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e.toString());
			throw e;
		}
		return rs;
    }
	
	
	public List<TXNFUNDDATA> findByCompany(String cmp_code) {
		return find("FROM TXNFUNDDATA WHERE SUBSTRING(TRANSCODE, 1, 2) = ? order by TRANSCODE", cmp_code);
	}
	
	public List<TXNFUNDDATA> findByFundGroupMark(String group_mark) {
		
		String sql = "SELECT a.* FROM TXNFUNDDATA a, TXNFUNDCOMPANY b WHERE b.FUNDGROUPMARK = ? " +
					 " and SUBSTR(a.TRANSCODE, 1, 2) = b.COMPANYCODE order by a.TRANSCODE";
		
		Query query = getSession().createSQLQuery(sql);
		query.unwrap(NativeQuery.class).addEntity("a", TXNFUNDDATA.class);
		query.setParameter(0, group_mark);						
		
		return ESAPIUtil.validStrList(query.getResultList());
	}
	@SuppressWarnings( { "unused", "unchecked" })
	public List<TXNFUNDDATA> findByFUNDLNAME(String TRANSCODE) {
		
		String sql = "from TXNFUNDDATA where TRANSCODE = ? order by TRANSCODE";
		
		Query query = getQuery(sql, TRANSCODE);		
		
		return ESAPIUtil.validStrList(query.getResultList());
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deleteAll() {
		this.getSession().createQuery("DELETE FROM TXNFUNDDATA").executeUpdate();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void insert(TXNFUNDDATA _tt) {
		try {
			int retcode=0;
			Query add1=null;
			add1 =this.getSession().createSQLQuery("INSERT INTO TXNFUNDDATA VALUES( ? ,? ,? ,? ,? ,? ,? ,? ,? )");
			add1.setParameter(0, _tt.getTRANSCODE());
			add1.setParameter(1, _tt.getTRANSCRY());
			add1.setParameter(2, _tt.getFUNDLNAME());
			add1.setParameter(3, _tt.getFUNDSNAME());
			add1.setParameter(4, _tt.getCOUNTRYTYPE());
			add1.setParameter(5, _tt.getFUNDMARK());
			add1.setParameter(6, _tt.getRISK());
			add1.setParameter(7, _tt.getPERIODVAR());
			add1.setParameter(8, _tt.getFUS98E());
			retcode=add1.executeUpdate();
			log.warn("insert retcode:"+retcode+" TRANSCODE:"+_tt.getTRANSCODE());
		}catch (RuntimeException e)
		{
			log.warn("insert txnfunddata failed");
		}
	}	
}
