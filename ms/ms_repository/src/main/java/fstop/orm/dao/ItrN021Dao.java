package fstop.orm.dao;

import java.util.List;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.ITRN021;

import javax.persistence.Query;

@Slf4j
@Repository
@Transactional
public class ItrN021Dao extends LegacyJpaRepository<ITRN021, String> {

	public List<ITRN021> getAll() {
		return find("FROM ITRN021 ORDER BY RECNO");
	}
	
	public void insert(ITRN021 itrn021) {
		int r2=0;
		String fillstr=new String();
		Query add1=null;
		
		add1=this.getSession().createSQLQuery("INSERT INTO ITRN021 VALUES(? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,?)");
		add1.setParameter(0, itrn021.getRECNO());
		log.debug("RECNO = " + itrn021.getRECNO());
		add1.setParameter(1, itrn021.getHEADER());
		log.debug("HEADER = " + itrn021.getHEADER());
		add1.setParameter(2, itrn021.getSEQ());
		log.debug("SEQ = " + itrn021.getSEQ());
		add1.setParameter(3, itrn021.getDATE());
		log.debug("DATE = " + itrn021.getDATE());
		add1.setParameter(4, itrn021.getTIME());
		log.debug("TIME = " + itrn021.getTIME());
		add1.setParameter(5, itrn021.getCOUNT());
		log.debug("COUNT = " + itrn021.getCOUNT());
		add1.setParameter(6, itrn021.getCOLOR());
		log.debug("COLOR = " + itrn021.getCOLOR());
		add1.setParameter(7, itrn021.getACC());
		log.debug("ACC = " + itrn021.getACC());
		add1.setParameter(8, itrn021.getITR1());
		log.debug("ITR1 = " + itrn021.getITR1());
		add1.setParameter(9, itrn021.getITR2());
		log.debug("ITR2 = " + itrn021.getITR2());
		add1.setParameter(10, itrn021.getITR3());
		log.debug("ITR3 = " + itrn021.getITR3());
		add1.setParameter(11, itrn021.getITR4());
		log.debug("ITR4 = " + itrn021.getITR4());
		add1.setParameter(12, itrn021.getITR5());
		log.debug("ITR5 = " + itrn021.getITR5());
		add1.setParameter(13, itrn021.getITR6());
		log.debug("ITR6 = " + itrn021.getITR6());
		add1.setParameter(14, itrn021.getITR7());
		log.debug("ITR7 = " + itrn021.getITR7());
		add1.setParameter(15, itrn021.getITR8());
		log.debug("ITR8 = " + itrn021.getITR8());
		add1.setParameter(16, itrn021.getITR9());
		log.debug("ITR9 = " + itrn021.getITR9());
		add1.setParameter(17, itrn021.getITR10());
		log.debug("ITR10 = " + itrn021.getITR10());
		if(itrn021.getFILL()==null)
			fillstr=" ";
		else
			fillstr=itrn021.getFILL();
		log.debug("FILL = " + fillstr);
		add1.setParameter(18, fillstr);
		r2 = add1.executeUpdate();
	}

	public void deleteAll() {
		this.getSession().createSQLQuery("DELETE FROM ITRN021").executeUpdate();
	}
}
