package fstop.orm.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;

import fstop.orm.po.NFLOG;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
public class NFLogDao extends LegacyJpaRepository<NFLOG, String> {
	protected Logger logger = Logger.getLogger(getClass());
	
	@SuppressWarnings("unchecked")
	public Page findByUserID(String startdt, String enddt, String DPUSERID, int start, int limit, String state) {
		List<String> condition = new ArrayList();
		List<String> params = new ArrayList();
		
		condition.add(" NFSENDTIME >= ? and NFSENDTIME <= ? ");
		params.add(startdt);
		params.add(enddt);
		
		if(StrUtils.isNotEmpty(DPUSERID)) {
			condition.add(" NFUSERID = ? ");
			params.add(DPUSERID);
		}
		
		String[] values = new String[params.size()];
		params.toArray(values);
		
		condition.add(" 1 = 1 ");
		
		StringBuffer c = new StringBuffer();
		for(String s : condition) {
			if(c.length() > 0)
				c.append(" AND ");
			c.append(s);
		}
		/**
		 * 狀態
		 */
		if(state.equals("'Y'")){
			c.append(" AND NFSTATUS in "+"("+state+") ");
		}
		else if(state.equals("'N'")){
			c.append(" AND NFSTATUS in "+"("+state+") ");
		}		
		return pagedQuery("FROM NFLOG " +
				  		"WHERE " + c.toString() + 
						" order by NFUSERID asc", (start / limit) + 1, limit, values);
	}	

}