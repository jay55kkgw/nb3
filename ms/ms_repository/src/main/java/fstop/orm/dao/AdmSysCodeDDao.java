package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.util.ESAPIUtil;

import fstop.model.Row;
import fstop.model.Rows;
import fstop.orm.po.ADMSYSCODED;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.hibernate.query.NativeQuery;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


@Slf4j
@Repository
public class AdmSysCodeDDao extends LegacyJpaRepository<ADMSYSCODED,String> {
	protected Logger logger = Logger.getLogger(getClass());
	
	@SuppressWarnings("unchecked")
	public Rows getCounty(String codeKind)
	{
			Rows result = new Rows();
			StringBuffer sql = new StringBuffer();
			sql.append(" SELECT * FROM ADMSYSCODED  ");
			sql.append(" WHERE CODEKIND=? ");
			sql.append(" ORDER BY SORTORDER ");
			
//			Session ses=getSession();
			Query query = getSession().createSQLQuery(sql.toString());
			query.setParameter(0,codeKind);
			logger.debug("query===>>>"+codeKind+"<==");
			query.unwrap(NativeQuery.class).addScalar("CODEKIND", StringType.INSTANCE)
			.addScalar("CODEID", StringType.INSTANCE)
			.addScalar("CODEPARENT", StringType.INSTANCE)
			.addScalar("CODENAME", StringType.INSTANCE)
			.addScalar("CODESNAME", StringType.INSTANCE)
			.addScalar("ISENABLE", StringType.INSTANCE)
			.addScalar("SORTORDER", StringType.INSTANCE)
			.addScalar("MEMO", StringType.INSTANCE);
			
			// Stored XSS
			List<Object> qresult = query.getResultList();
			if (null == qresult) {
				qresult = new ArrayList();
			}
			qresult = ESAPIUtil.validList(qresult);
//			List qresult = query.getResultList();
			
			Iterator it = qresult.iterator();
			while(it.hasNext()) {				
				Object[] o = (Object[])it.next();
				Map rm = new HashMap();
				int t = -1;												
				rm.put("CODEKIND", o[++t].toString());							
				rm.put("CODEID", (o[++t]==null ? "" : o[t].toString()));
				rm.put("CODEPARENT", (o[++t]==null ? "" : o[t].toString()));
				rm.put("CODENAME", (o[++t]==null ? "" : o[t].toString()));
				rm.put("CODESNAME", (o[++t]==null ? "" : o[t].toString()));
				rm.put("ISENABLE", (o[++t]==null ? "" : o[t].toString()));
				rm.put("SORTORDER", (o[++t]==null ? "" : o[t].toString()));
				rm.put("MEMO", (o[++t]==null ? "" : o[t].toString()));
				Row r = new Row(rm);
				result.addRow(r);
			}
			
			return result;	
	}
	/**
	 * ADMAPPBRANCH mapping ADMSYSCODED 
	 * 由代碼mapping至中文
	 * @return result
	 */
	public Rows getBranchMappingSysCode()
	{
		Rows result = new Rows();
		StringBuffer sql = new StringBuffer();
		
		sql.append(" SELECT c.CODENAME ");
		sql.append(" FROM ADMAPPBRANCH b,ADMSYSCODED c ");
		sql.append(" WHERE b.BHCOUNTY = c.CODEID ");
		//sql.append(" ON() ");
		//sql.append(" ");

		Query query = getSession().createSQLQuery(sql.toString());

		query.unwrap(NativeQuery.class).addScalar("CODENAME", StringType.INSTANCE);
		
		// Stored XSS
		List<Object> qresult = query.getResultList();
		if (null == qresult) {
			qresult = new ArrayList();
		}
		qresult = ESAPIUtil.validList(qresult);
//		List qresult = query.getResultList();
		
		Iterator it = qresult.iterator();
		while(it.hasNext()) 
		{				
			Object[] o = (Object[])it.next();
			Map rm = new HashMap();
			int t = -1;	
			
			rm.put("CODENAME", (o[++t]==null ? "" : o[t].toString()));
			Row r = new Row(rm);
			result.addRow(r);
		}
		
		return result;
	}
}
