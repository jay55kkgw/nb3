package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.util.ESAPIUtil;

import fstop.model.Row;
import fstop.model.Rows;
import fstop.orm.po.ADMREMITMENU;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Slf4j
@Repository
@Transactional
public class AdmRemitMenuDao extends LegacyJpaRepository<ADMREMITMENU, Long> {
	protected Logger logger = Logger.getLogger(getClass());

	/**
	 * 依據傳入參數取得 AdmRemitMenu table符合之資料, 若全部傳入值皆為 ""時,表帶出所有大類(ADLKINDID)資料
	 * 
	 * @return
	 */
	public List<ADMREMITMENU> getRemitMenu(String ADRMTTYPE, String ADLKINDID, String ADMKINDID,
			String ADRMTID, String custype) {

		StringBuffer qp = new StringBuffer();
		List<String> pp = new ArrayList();
		if(StrUtils.isNotEmpty(ADLKINDID)) {
			if(qp.length() > 0)
				qp.append(" and ");
			qp.append(" ADLKINDID = ? ");
			pp.add(ADLKINDID);
		}
		
		if(StrUtils.isNotEmpty(ADMKINDID)) {
			if(qp.length() > 0)
				qp.append(" and ");
			qp.append(" ADMKINDID = ? ");
			pp.add(ADMKINDID);
		}
		else {
			qp.append(" and ");
			qp.append(" ADMKINDID <> '00' ");			
		}
		
		if(StrUtils.isNotEmpty(ADRMTID)) {
			if(qp.length() > 0)
				qp.append(" and ");
			
			if ("*".equals(ADRMTID))
				qp.append(" ADRMTID <> '' ");
			else {
				qp.append(" ADRMTID = ? ");			
				pp.add(ADRMTID);
			}
		}
		else {
			qp.append(" and ");
			qp.append(" ADRMTID = '' ");			
		}
		
		if(StrUtils.isNotEmpty(ADRMTTYPE)) {
			if(qp.length() > 0)
				qp.append(" and ");
			qp.append(" ADRMTTYPE = ? ");
			pp.add(ADRMTTYPE);			
		}
		
		if(StrUtils.isNotEmpty(custype)) {
			if(qp.length() > 0)
				qp.append(" and ");
			
			if (custype.equals("1") || custype.equals("2"))
				custype = "1";
			else if (custype.equals("3"))
				custype = "2";
			else
				custype = "3";
				
			qp.append(" ADRMTEETYPE" + custype + " = 'Y' ");
		}
		
		String[] params = new String[pp.size()];
		pp.toArray(params);
		
		logger.debug("SQL : FROM ADMREMITMENU WHERE " + qp.toString());
		
		return find("FROM ADMREMITMENU WHERE " + qp.toString(), params);

	}

	/**
	 * 依據傳入參數取得 AdmRemitMenu table 符合之資料 
	 * (管理端選單維護使用)
	 * 
	 * @return
	 */
	public List<ADMREMITMENU> getAdminRemitMenu(String ADRMTTYPE, 
												String ADLKINDID, 
												String ADMKINDID,
												String ADRMTID, 
												String custype) {

		StringBuffer qp = new StringBuffer();
		List<String> pp = new ArrayList();
		if(StrUtils.isNotEmpty(ADLKINDID)) {
			if(qp.length() > 0)
				qp.append(" and ");
			qp.append(" ADLKINDID = ? ");
			pp.add(ADLKINDID);
		}
		
		if(StrUtils.isNotEmpty(ADMKINDID)) {
			if(qp.length() > 0)
				qp.append(" and ");
			qp.append(" (ADMKINDID = '00' or ADMKINDID = ?) ");
			pp.add(ADMKINDID);
		}		
		
		if(StrUtils.isNotEmpty(ADRMTTYPE)) {
			if(qp.length() > 0)
				qp.append(" and ");
			qp.append(" ADRMTTYPE = ? ");
			pp.add(ADRMTTYPE);			
		}
		
		if(StrUtils.isNotEmpty(custype)) {
			if(qp.length() > 0)
				qp.append(" and ");
			
			if (custype.equals("1") || custype.equals("2"))
				custype = "1";
			else if (custype.equals("3"))
				custype = "2";
			else
				custype = "3";
			
			qp.append(" ADRMTEETYPE" + custype + " = 'Y' ");
		}
		
		String[] params = new String[pp.size()];
		pp.toArray(params);
		
		logger.debug("SQL : FROM ADMREMITMENU WHERE " + qp.toString());
		
		return find("FROM ADMREMITMENU WHERE " + qp.toString(), params);
	}
		
	public Rows getAllAdlKind(String adrmttype) {
		Query query = getSession().createQuery("SELECT DISTINCT ADLKINDID, ADMKINDID, ADRMTITEM, ADRMTDESC " +
				" FROM ADMREMITMENU WHERE ADRMTTYPE = ? and ADRMTID = '' and ADMKINDID = '00' ORDER BY ADLKINDID ");
		query.setParameter(0, adrmttype);
		
		// Stored XSS
		List<Object> qresult = query.getResultList();
		if (null == qresult) {
			qresult = new ArrayList();
		}
		qresult = ESAPIUtil.validList(qresult);
//		List lresult = query.getResultList();
		
		Rows result = new Rows();
		
		for(Object o : qresult) {
			Object[] or = (Object[])o;
			Map<String, String> rowdata = new HashMap();
			rowdata.put("ADLKINDID", or[0].toString());
			rowdata.put("ADMKINDID", or[1].toString());
			rowdata.put("ADRMTITEM", or[2].toString());
			rowdata.put("ADRMTDESC", or[3].toString());
			logger.debug(ESAPIUtil.vaildLog("Object type = " + o.getClass().getSimpleName()));
			Row row = new Row(rowdata);
			result.addRow(row);
		}
		
		return result;

	}
	
	/**
	 * 找到與資料庫的同一筆資料
	 * @param po
	 * @return
	 */
	public List<ADMREMITMENU> findAttrsEquals(ADMREMITMENU po) {
		String[] fields = {
				"ADRMTID",
				"ADRMTITEM",
				"ADRMTDESC",
				"ADCHKMK",
				"ADRMTEETYPE1",
				"ADRMTEETYPE2",
				"ADRMTEETYPE3"
			};
		String[] params = {
				po.getADRMTID(),
				po.getADRMTITEM(),
				po.getADRMTDESC(),
				po.getADCHKMK(),
				po.getADRMTEETYPE1(),
				po.getADRMTEETYPE2(),
				po.getADRMTEETYPE3()
			}; 
		String where = StrUtils.implode(" = ? and ", fields);
		return this.find("FROM ADMREMITMENU WHERE " + where + " 1=1 ", params);
	}

	/**
	 * 就查詢全部資料, 依ADLKINDID 、ADMKINDID、 ADRMTID 順序做排序
	 * @return
	 */
	public List<ADMREMITMENU> findAllAndSort(String ADRMTTYPE) {
		return find("FROM ADMREMITMENU WHERE ADRMTTYPE='"+ ADRMTTYPE +"' ORDER BY ADRMTTYPE, ADLKINDID, ADMKINDID,  ADRMTID ");
	}

	public ADMREMITMENU findUniqueByRmtid(String rmttype, String rmtid) {
		try {
			return (ADMREMITMENU)find("FROM ADMREMITMENU WHERE ADRMTTYPE = ? and ADRMTID = ? ", rmttype, rmtid)
						.get(0);
		}
		catch(IndexOutOfBoundsException e) {}
		
		return null;
	}
}
