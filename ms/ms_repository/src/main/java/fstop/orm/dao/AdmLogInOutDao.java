package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.util.ESAPIUtil;

import fstop.model.Row;
import fstop.model.Rows;
import fstop.orm.po.ADMLOGINOUT;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Parameter;
import javax.persistence.ParameterMode;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Slf4j
@Repository
@Transactional
public class AdmLogInOutDao extends LegacyJpaRepository<ADMLOGINOUT, Long> {
	protected Logger logger = Logger.getLogger(getClass());

	/*
	 * 
	 */
	public Rows findLoginReportByMonth(String startYYMM, String endYYMM, String logintype) {
		Rows result = new Rows();

		// log.debug("logintype==>>"+logintype+","+logintype2+"<====");
		Query query = getSession().createSQLQuery("" + " SELECT varchar(CMYYYYMM) as cmyyyymm, count(*) as ADSUMROW,"
				+ "  sum(case when ADUSERTYPE = '1' then 1 else 0 end ) as ADENTERP , "
				+ "  sum(case when ADUSERTYPE = '0' then 1 else 0 end ) as ADPERSON  " +

				" FROM ADMLOGINOUT WHERE LOGINOUT='0' and CMYYYYMM >= ? and CMYYYYMM <= ? " + " and LOGINTYPE in ("
				+ logintype + ") " + " GROUP BY CMYYYYMM ORDER BY CMYYYYMM ASC ");
		// log.debug("logintype1111==>>"+logintype+","+logintype2+"<====");
		query.setParameter(0, startYYMM);
		query.setParameter(1, endYYMM);
		// query.setParameter(2, logintype);

		// Stored XSS
		List<Object> qresult = query.getResultList();
		if (null == qresult) {
			qresult = new ArrayList();
		}
		qresult = ESAPIUtil.validList(qresult);
		// List qresult = query.getResultList();

		Iterator it = qresult.iterator();
		while (it.hasNext()) {
			Object[] o = (Object[]) it.next();
			Map rm = new HashMap();
			rm.put("CMYYYYMM", o[0].toString());
			rm.put("ADSUMROW", o[1].toString());
			rm.put("ADENTERP", o[2].toString());
			rm.put("ADPERSON", o[3].toString());

			Row r = new Row(rm);
			result.addRow(r);
		}

		return result;
	}

	public Rows findLoginReportByDate(String startYYMMDD, String endYYMMDD, String logintype) {
		Rows result = new Rows();

		Query query = getSession()
				.createSQLQuery("" + " SELECT varchar(CMYYYYMM) || varchar(CMDD) AS yyyymmdd, count(*) as ADSUMROW, "
						+ " sum(case when ADUSERTYPE = '1' then 1 else 0 end ) as ADENTERP , "
						+ " sum(case when ADUSERTYPE = '0' then 1 else 0 end ) as ADPERSON  " +

						" FROM ADMLOGINOUT WHERE LOGINOUT='0' and CMYYYYMM || CMDD >= ? and CMYYYYMM || CMDD <= ? "
						+ " and LOGINTYPE in (" + logintype + ") "
						+ " GROUP BY CMYYYYMM, CMDD ORDER BY CMYYYYMM, CMDD ASC ");

		query.setParameter(0, startYYMMDD);
		query.setParameter(1, endYYMMDD);
		// query.setParameter(2, logintype);

		// Stored XSS
		List<Object> qresult = query.getResultList();
		if (null == qresult) {
			qresult = new ArrayList();
		}
		qresult = ESAPIUtil.validList(qresult);
		// List qresult = query.getResultList();

		Iterator it = qresult.iterator();
		while (it.hasNext()) {
			Object[] o = (Object[]) it.next();
			Map rm = new HashMap();
			rm.put("CMYYYYMM", o[0].toString());
			rm.put("ADSUMROW", o[1].toString());
			rm.put("ADENTERP", o[2].toString());
			rm.put("ADPERSON", o[3].toString());

			Row r = new Row(rm);
			result.addRow(r);
		}

		return result;
	}

	public Rows getLoginType(String logintype) {
		Rows result = new Rows();
		Query query = getSession()
				.createSQLQuery("" + "SELECT LOGINTYPE " + "FROM ADMLOGINOUT " + "WHERE LOGINTYPE = ? ");
		query.setParameter(0, logintype);

		// Stored XSS
		List<Object> qresult = query.getResultList();
		if (null == qresult) {
			qresult = new ArrayList();
		}
		qresult = ESAPIUtil.validList(qresult);
		// List qresult = query.getResultList();

		Iterator it = qresult.iterator();
		while (it.hasNext()) {
			Object[] o = (Object[]) it.next();
			Map rm = new HashMap();
			rm.put("LOGINTYPE", o[0].toString());
			Row r = new Row(rm);
			result.addRow(r);
		}

		return result;
	}
	
	public List<ADMLOGINOUT> findby(String sID, String logindate, String logintype) {
		String CMYYYYMM = logindate.substring(0, 6);
		String CMDD = logindate.substring(6, 8);
		logintype = logintype.equals("NB") ? "" : logintype;
		String sql = "select * FROM ADMLOGINOUT where ADUSERID=? AND CMYYYYMM=? AND CMDD=? AND LOGINTYPE=? AND LOGINOUT='0'";
		Query query = getSession().createSQLQuery(sql);

		query.setParameter(0, sID);
		query.setParameter(1, CMYYYYMM);
		query.setParameter(2, CMDD);
		query.setParameter(3, logintype);
		return query.getResultList();

	}

	/**
	 * 1.呼叫store procedure "EXP_ADMLOGINOUT" 傳入一個參數。
	 * 
	 * @param outputDirectory
	 *            輸出資料夾
	 * @return
	 */
	public Integer sp_cal_expLoginout(String outputDirectory) {
		int result = 1;
		try {

			StoredProcedureQuery storedProcedure = getEntityManager().createStoredProcedureQuery("EXP_ADMLOGINOUT")
					.registerStoredProcedureParameter(0, String.class, ParameterMode.IN)
					.registerStoredProcedureParameter(1, String.class, ParameterMode.IN)
					.registerStoredProcedureParameter(2, String.class, ParameterMode.IN)
					.registerStoredProcedureParameter(3, String.class, ParameterMode.IN)
					.registerStoredProcedureParameter(4, Integer.class, ParameterMode.OUT)
					.registerStoredProcedureParameter(5, String.class, ParameterMode.OUT)
					.registerStoredProcedureParameter(6, String.class, ParameterMode.OUT);

			storedProcedure.setParameter(0, "").setParameter(1, "").setParameter(2, "").setParameter(3,
					outputDirectory);

			storedProcedure.execute();

			result = (int) storedProcedure.getOutputParameterValue(4);

			log.debug("EXP_ADMLOGINOUT result >>{}", result);
		} catch (Exception e) {
			log.error("EXP_ADMLOGINOUT Exception {}", e);
			return result;
		}

		return result;
	}

	/**
	 * 1.呼叫store procedure "DEL_ADMLOGINOUT"
	 * 
	 * @param 
	 *        
	 * @return result
	 */
	public Integer sp_cal_delLoginout() {
		int result = 1;
		try {

			StoredProcedureQuery storedProcedure = getEntityManager().createStoredProcedureQuery("DEL_ADMLOGINOUT")
					.registerStoredProcedureParameter(0, String.class, ParameterMode.IN)
					.registerStoredProcedureParameter(1, String.class, ParameterMode.IN)
					.registerStoredProcedureParameter(2, String.class, ParameterMode.IN)
					.registerStoredProcedureParameter(3, Integer.class, ParameterMode.OUT)
					.registerStoredProcedureParameter(4, String.class, ParameterMode.OUT)
					.registerStoredProcedureParameter(5, String.class, ParameterMode.OUT);

			storedProcedure.setParameter(0, "").setParameter(1, "").setParameter(2, "");


			storedProcedure.execute();

			result = (int) storedProcedure.getOutputParameterValue(3);

			log.debug("DEL_ADMLOGINOUT result >>{}", result);

		} catch (Exception e) {
			log.error("DEL_ADMLOGINOUT Exception {}", e);
			return result;
		}

		return result;
	}
}
