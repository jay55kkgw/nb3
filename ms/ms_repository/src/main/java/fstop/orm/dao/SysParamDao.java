package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import fstop.orm.po.SYSPARAM;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.List;

@Slf4j
@Repository
@Transactional
public class SysParamDao extends LegacyJpaRepository<SYSPARAM, String> {

	protected Logger logger = Logger.getLogger(getClass());

	public List<SYSPARAM> getAll() {
		return find("FROM SYSPARAM");
	}
	
	public String getSysParam(String paramName) {
		List<SYSPARAM> result = find("FROM SYSPARAM WHERE ADPARAMNAME = ?", paramName);
		if (result.size()>0)
			return result.get(0).getADPARAMVALUE();
		else
			return "";
	}
	
	public boolean update(String paramName, String paramValue){
		Query add1 = getSession().createSQLQuery("UPDATE SYSPARAM SET ADPARAMVALUE = ? WHERE ADPARAMNAME = ?");
		add1.setParameter(0, paramValue);
		add1.setParameter(1, paramName);
		
		int r2 = add1.executeUpdate();
		return true;
	}
}