package fstop.orm.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.util.ESAPIUtil;
import com.netbank.util.fstop.DateUtil;

import fstop.orm.po.SYSBATCH;
import fstop.util.BatchDetailProperties;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Transactional
public class SysBatchDao extends LegacyJpaRepository<SYSBATCH, String> {
	protected Logger logger = Logger.getLogger(getClass());

	public void initSysBatchPo(String batchName) {
		List<Object> values = new ArrayList();
		SYSBATCH po = findById(batchName);
		DateTime d1 = new DateTime();
		String str_startDate = d1.toString("yyyyMMdd");
		String str_startTime = d1.toString("HHmmss");
		if (null == po) {
			BatchDetailProperties bdpro = new BatchDetailProperties();
			String batchDtail = bdpro.getDetail(batchName);
			values.add(batchName);
			values.addAll(Arrays.asList(batchDtail.split(",")));
			values.add(str_startDate);
			values.add(str_startTime);
			log.debug(ESAPIUtil.vaildLog("values >> " + values.toString()));
			String SQL = "INSERT INTO SYSBATCH(ADBATID,ADBATNAME,PEROID,ADBATSDATE,ADBATSTIME) VALUES(?,?,?,?,?)";
			Query query = getSession().createSQLQuery(SQL);
			for (int i = 0; values != null && i < values.size(); i++) {
				query.setParameter(i, values.get(i));
			}
			query.executeUpdate();
		} else {
			po.setADBATSDATE(str_startDate);
			po.setADBATSTIME(str_startTime);
			po.setEXECMESSAGE("");
			po.setEXECUTIONTIME("");
			po.setADBATEDATE("");
			po.setADBATETIME("");
			update(po);
		}
	}

	public SYSBATCH finish_OK(String batchName) {
		SYSBATCH po = findById(batchName);
		DateTime d2 = new DateTime();
		po.setADBATEDATE(d2.toString("yyyyMMdd"));
		po.setADBATETIME(d2.toString("HHmmss"));
		
		String str_startDateTime = po.getADBATSDATE() + " " + po.getADBATSTIME();
		DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyyMMdd HHmmss");
		DateTime d1 = formatter.parseDateTime(str_startDateTime);
		po.setEXECUTIONTIME(DateUtil.getDiffTimeMills(d1, d2));

		return po;
	}

	public SYSBATCH finish_FAIL(String batchName, String errorMsg) {
		SYSBATCH po = findById(batchName);
		DateTime d2 = new DateTime();
		po.setADBATEDATE(d2.toString("yyyyMMdd"));
		po.setADBATETIME(d2.toString("HHmmss"));

		String str_startDateTime = po.getADBATSDATE() + " " + po.getADBATSTIME();
		DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyyMMdd HHmmss");
		DateTime d1 = formatter.parseDateTime(str_startDateTime);
		po.setEXECUTIONTIME(DateUtil.getDiffTimeMills(d1, d2));
		po.setEXECMESSAGE(errorMsg);
		return po;
	}

	public SYSBATCH finish_FAIL2(String batchName, Map errorMsg) {
		SYSBATCH po = findById(batchName);
		DateTime d2 = new DateTime();
		po.setADBATEDATE(d2.toString("yyyyMMdd"));
		po.setADBATETIME(d2.toString("HHmmss"));

		String str_startDateTime = po.getADBATSDATE() + " " + po.getADBATSTIME();
		DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyyMMdd HHmmss");
		DateTime d1 = formatter.parseDateTime(str_startDateTime);
		po.setEXECUTIONTIME(DateUtil.getDiffTimeMills(d1, d2));
		po.setEXECMESSAGE(errorMsg.toString());
		return po;
	}
}
