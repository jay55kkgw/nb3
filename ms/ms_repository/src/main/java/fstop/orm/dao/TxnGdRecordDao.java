package fstop.orm.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;

import fstop.orm.po.TXNGDRECORD;
import fstop.orm.po.TXNREQINFO;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Transactional
public class TxnGdRecordDao extends LegacyJpaRepository<TXNGDRECORD, String> {
	protected Logger logger = Logger.getLogger(getClass());
	
	@SuppressWarnings("unchecked")
	public Page findByDateRange(String startdt, String enddt, String ADUSERID, int start, int limit) {
		List<String> condition = new ArrayList();
		List<String> params = new ArrayList();
		
		condition.add(" LASTDATE||LASTTIME >= ? and LASTDATE||LASTTIME <= ? ");
		params.add(startdt);
		params.add(enddt);
		
		if(StrUtils.isNotEmpty(ADUSERID)) {
			condition.add(" GDUSERID = ? ");
			params.add(ADUSERID);
		}
		
		String[] values = new String[params.size()];
		params.toArray(values);
		
		StringBuffer c = new StringBuffer();
		for(String s : condition) {
			if(c.length() > 0)
				c.append(" AND ");
			c.append(s);
		}
		
		return pagedQuery("FROM TXNGDRECORD " +
				  		"WHERE " + c.toString() + 
						" order by LASTDATE desc, LASTTIME desc, ADOPID asc", (start / limit) + 1, limit, values);
	}	

	@Autowired
	TxnReqInfoDao txnReqInfoDao;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void writeTxnRecord(TXNREQINFO info ,TXNGDRECORD record) {
		if(record != null)
			logger.debug("WRITE NEW TXNGDRECORD ADTXNO:" + record.getADTXNO());

//		BOLifeMoniter.setValue("__TXNRECORD", record);
		if(info != null) {
//			BOLifeMoniter.setValue("__TITAINFO", info);
			txnReqInfoDao.save(info);
			record.setGDTITAINFO(info.getREQID());
		}
		save( record);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void rewriteTxnRecord(TXNGDRECORD record) {
//		BOLifeMoniter.setValue("__TXNRECORD", record);
		save( record);
	}

}
