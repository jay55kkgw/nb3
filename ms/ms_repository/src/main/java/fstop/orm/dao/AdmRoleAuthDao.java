package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import fstop.orm.po.ADMROLEAUTH;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Slf4j
@Repository
@Transactional
public class AdmRoleAuthDao extends LegacyJpaRepository<ADMROLEAUTH, Integer> {
	protected Logger logger = Logger.getLogger(getClass());
	
	public List queryAll() {
		return find("FROM ADMROLEAUTH ORDER BY ADROLENO, ADSTAFFNO");
	}

	public boolean findRolenoStaffnoExists(String roleno, String staffno) {
		return ((Long)find("SELECT COUNT(*) FROM ADMROLEAUTH WHERE ADROLENO=? AND ADSTAFFNO=? ", roleno, staffno).iterator().next()) > 0;
	}
	
	public boolean findStaffnoExists(String roleno) {
		return ((Long)find("SELECT COUNT(*) FROM ADMROLEAUTH WHERE ADROLENO=? AND ADSTAFFNO<>'' ", roleno).iterator().next()) > 0;
	}
	
	public String findRolenoAuth(String roleno) {
		String result = "";
		List qresult = find("SELECT ADAUTH FROM ADMROLEAUTH WHERE ADROLENO=? AND ADSTAFFNO='' ", roleno);
		
		if(qresult.size() > 0)
			result = (String)qresult.iterator().next();
		
		return result;
	}
	
	public String findRolenoAuth(String roleno, String staffno) {
		String result = "";
		List qresult = find("SELECT ADAUTH FROM ADMROLEAUTH WHERE ADROLENO=? AND ADSTAFFNO=?  ", roleno, staffno);
		
		if(qresult.size() > 0)
			result = (String)qresult.iterator().next();
		
		return result;
	}
	
	
}
