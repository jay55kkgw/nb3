package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;
import fstop.orm.po.ADMOPLOG;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.hibernate.query.NativeQuery;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;


@Slf4j
@Repository
@Transactional
public class AdmOpLogDao extends LegacyJpaRepository<ADMOPLOG, String> {
	protected Logger logger = Logger.getLogger(getClass());
	
	
	@SuppressWarnings("unchecked")
	public Page getFindByDateRangePageObject(String startdt, String enddt, String userid, String txitem, int start, int limit) {
		List<String> condition = new ArrayList();
		List<String> params = new ArrayList();
		
		condition.add(" ADTXDATE||ADTXTIME >= ? and ADTXDATE||ADTXTIME <= ? ");
		params.add(startdt);
		params.add(enddt);
		
		if(StrUtils.isNotEmpty(userid)) {
			condition.add(" ADUSERID = ? ");
			params.add(userid);
		}
		if(StrUtils.isNotEmpty(txitem)) {
			if("login".equals(txitem)) {
				condition.add(" (ADTXITEM = 'login' or ADTXITEM = 'logout' )");
			}
			else {
				condition.add("  ADTXITEM = ?  ");//注意，這裡要的ADOPID跟帶上來的ADTXITEM的值(二者其實是一樣的)
				params.add(txitem);
			}
		}
		
		String[] values = new String[params.size()];
		params.toArray(values);
		
		StringBuffer c = new StringBuffer();
		for(String s : condition) {
			if(c.length() > 0)
				c.append(" AND ");
			c.append(s);
		}
		
		return pagedQuery("FROM ADMOPLOG " +
				" WHERE " +
				" " + c.toString() + 
				" ORDER BY ADTXDATE DESC, ADTXTIME DESC" +
				"", (start / limit) + 1, limit, values);
	}
	
	public List findByDateRange(String startdt, String enddt, String userid, String txitem, int start, int limit) {
		return (List)getFindByDateRangePageObject(startdt, enddt, userid, txitem, start, limit).getResult();
	}
	
	public Long findByDateRangeCount(String startdt, String enddt, String userid, String txitem, int start, int limit) {
		return getFindByDateRangePageObject(startdt, enddt, userid, txitem, start, limit).getTotalCount();
	}
	
	public List<ADMOPLOG> getAdmOpLogList(String date) {
		Query query = getSession().createSQLQuery("select * FROM ADMOPLOG where ADTXDATE = ?");
		query.unwrap(NativeQuery.class).addEntity("AdmOpLog", ADMOPLOG.class);
		query.setParameter(0, date);	
		return query.getResultList();
	}
	
}
