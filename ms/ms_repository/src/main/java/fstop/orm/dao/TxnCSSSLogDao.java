package fstop.orm.dao;

import org.hibernate.query.NativeQuery;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.util.ESAPIUtil;

import fstop.orm.po.TXNCSSSLOG;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Transactional
public class TxnCSSSLogDao extends LegacyJpaRepository<TXNCSSSLOG, String> {

	public boolean checkDoCnt(String cusidn,String year) {  //每年確認是否有作過一次，排除按下次再填的case  true：不在顯示畫面  false：跳出畫面填寫
		long count = (Long) (find(
				"SELECT COUNT(*) FROM TXNCSSSLOG  WHERE CUSIDN = ? AND ANSWER<>'' AND NEXTBTN<>'Y' AND LASTDATE LIKE ?", cusidn, year+"%")).get(0);
		log.debug("TxnCSSSLogDao checkDoCnt:"+count);
		if (count >= 1) {
			return true;
		} else {
			return false;
		}
	}		
	/**
	 * 查詢下次再填超過3次
	 * @param cusidn,year
	 * @return
	 */
	public boolean checkNextBtnCnt(String cusidn,String year) {  //每年確認是否有按過下次再填3次以上    true：不在顯示畫面  false：跳出畫面填寫
		long count = (Long) (find(
				"SELECT COUNT(*) FROM TXNCSSSLOG  WHERE CUSIDN = ? AND NEXTBTN='Y' AND LASTDATE LIKE ?", cusidn, year+"%")).get(0);
		System.out.println("TxnCSSSLogDao checkNextBtnCnt:"+count);
		if (count >= 3) {
			return true;
		} else {
			return false;
		}
	}		
	/**
	 * 統計報表查詢 By ADOPID
	 * @param year,channel,adopid,answer
	 * @return
	 */
	public String findByAdopid(String year,String channel,String adopid,String answer) {  
		long count = (Long) (find(
				"SELECT COUNT(*) FROM TXNCSSSLOG  WHERE channel = ? AND adopid = ? AND answer = ? AND LASTDATE like ?", channel,adopid,answer, year+"%")).get(0);
		System.out.println("TxnCSSSLogDao findByAdopid:"+count);
		return String.valueOf(count);
	}			
	/**
	 * 統計報表查詢 By NEXTBTN
	 * @param date
	 * @return
	 */
	public String findByNextBtn(String year) {  
		long count = (Long) (find(
				"SELECT COUNT(*) FROM TXNCSSSLOG  WHERE NEXTBTN = 'Y'  and lastdate like ?", year+"%")).get(0);
		System.out.println("TxnCSSSLogDao findByNextBtn:"+count);
		return String.valueOf(count);
	}
	
	public List<TXNCSSSLOG> getTxnCsssLogforcrm(String startDate, String endDate){
		try {			
			String sql = "select * from txncssslog where lastdate >= ? and lastdate<=?";
			
			Query query = getSession().createSQLQuery(sql);
			query.unwrap(NativeQuery.class).addEntity("TXNCSSSLOG", TXNCSSSLOG.class);
			query.setParameter(0, startDate);
			query.setParameter(1, endDate);
			return ESAPIUtil.validStrList(query.getResultList());
		}catch(Exception e) {
			log.debug("getTxnCsssLogforcrm error>>>{}",e);
			return new ArrayList<TXNCSSSLOG>();
		}

	}
}
