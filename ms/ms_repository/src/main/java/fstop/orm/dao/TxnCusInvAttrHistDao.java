package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;
import com.netbank.domain.orm.core.SessionAdapter;
import com.netbank.util.ESAPIUtil;

import fstop.orm.po.TXNCUSINVATTRHIST;
import fstop.orm.po.TXNCUSINVATTRIB;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import java.util.List;
import java.util.function.BiFunction;

@Slf4j
@Repository
@Transactional(
		transactionManager="nb3transactionManager",
        rollbackFor = {Throwable.class}
)
public class TxnCusInvAttrHistDao extends LegacyJpaRepository<TXNCUSINVATTRHIST, Long> {

	
	
	@Override
	public EntityManager getEntityManager() {
		// TODO Auto-generated method stub
		return super.getEntityManager();
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return super.count();
	}

	@Override
	public List<TXNCUSINVATTRHIST> findAll() {
		// TODO Auto-generated method stub
		return super.findAll();
	}

	@Override
	public TXNCUSINVATTRHIST findById(Long id) {
		// TODO Auto-generated method stub
		return super.findById(id);
	}

	@Override
	public void save(TXNCUSINVATTRHIST entity) {
		// TODO Auto-generated method stub
		super.save(entity);
	}

	@Override
	public TXNCUSINVATTRHIST update(TXNCUSINVATTRHIST entity) {
		// TODO Auto-generated method stub
		return super.update(entity);
	}

	@Override
	public void delete(TXNCUSINVATTRHIST entity) {
		// TODO Auto-generated method stub
		super.delete(entity);
	}

	@Override
	public Query getQuery(String hql, Object... values) {
		// TODO Auto-generated method stub
		return super.getQuery(hql, values);
	}

	@Override
	public Query getCriteriaQuery(Class<TXNCUSINVATTRHIST> entityClass,
			BiFunction<Root, CriteriaBuilder, Predicate>... criteriaFunction) {
		// TODO Auto-generated method stub
		return super.getCriteriaQuery(entityClass, criteriaFunction);
	}

	@Override
	public List find(String hql, Object... values) {
		// TODO Auto-generated method stub
		return super.find(hql, values);
	}

	@Override
	public List<TXNCUSINVATTRHIST> findBy(Class<TXNCUSINVATTRHIST> entityClass, String name, Object value) {
		// TODO Auto-generated method stub
		return super.findBy(entityClass, name, value);
	}

	@Override
	public TXNCUSINVATTRHIST findUniqueBy(Class<TXNCUSINVATTRHIST> entityClass, String name, Object value) {
		// TODO Auto-generated method stub
		return super.findUniqueBy(entityClass, name, value);
	}

	@Override
	public List<TXNCUSINVATTRHIST> findByLike(Class<TXNCUSINVATTRHIST> entityClass, String name, String value) {
		// TODO Auto-generated method stub
		return super.findByLike(entityClass, name, value);
	}

	@Override
	public SessionAdapter getSession() {
		// TODO Auto-generated method stub
		return super.getSession();
	}

	@Override
	public Page pagedQuery(String hql, int pageNo, int pageSize, Object... values) {
		// TODO Auto-generated method stub
		return super.pagedQuery(hql, pageNo, pageSize, values);
	}

	@SuppressWarnings( { "unused", "unchecked" })
	public List<TXNCUSINVATTRHIST> findUserKycCount(String fundUserId, String lastDate) {
	//public int findUserKycCount(String fundUserId, String lastDate) {
		
		String sql = "from TXNCUSINVATTRHIST where FDUSERID = ? and LASTDATE = ? order by LASTDATE ";
		
		Query query = getQuery(sql, fundUserId, lastDate);
		
		return query.getResultList();
		//return query.list().size();
	}
	
	@SuppressWarnings( { "unused", "unchecked" })
	public List<String> findByLastDateRange(String startDate, String endDate) {
		
		String sql = "SELECT distinct LASTDATE from TXNCUSINVATTRHIST where LASTDATE >= ? and LASTDATE <= ? and AGREE='Y' order by LASTDATE ";

//		SQLQuery query = getSession().createSQLQuery(sql);
//
//		query.setParameter(0, startDate);
//		query.setParameter(1, endDate);

		Query query = getQuery(sql, startDate, endDate);			
		
		return ESAPIUtil.validStrList(query.getResultList());
	}
	
	@SuppressWarnings( { "unused", "unchecked" })
	public List<TXNCUSINVATTRHIST> findByLastDate(String lastDate) {
		
		String sql = "from TXNCUSINVATTRHIST where LASTDATE = ? and AGREE='Y' order by LASTUSER ";
		
		Query query = getQuery(sql, lastDate);
		
		return ESAPIUtil.validStrList(query.getResultList());
	}	
	
}