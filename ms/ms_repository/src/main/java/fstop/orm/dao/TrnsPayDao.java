package fstop.orm.dao;

import java.io.Serializable;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.LegacyJpaRepository;

import fstop.orm.po.TRNSPAY;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Transactional
public class TrnsPayDao extends LegacyJpaRepository<TRNSPAY, Serializable> {
}
