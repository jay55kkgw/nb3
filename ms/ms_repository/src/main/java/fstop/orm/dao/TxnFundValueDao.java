package fstop.orm.dao;

import java.util.List;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.TXNFUNDVALUE;

@Slf4j
@Repository
@Transactional
public class TxnFundValueDao extends LegacyJpaRepository<TXNFUNDVALUE, String> {
	protected Logger logger = Logger.getLogger(getClass());
	
	
	public List<TXNFUNDVALUE> findSNotes() {
		return this.findBy(TXNFUNDVALUE.class,"FUNDREMARK", "C");
	}
	
	public void deleteAll() {
		this.getSession().createQuery("DELETE FROM TXNFUNDVALUE").executeUpdate();
	}
	
}
