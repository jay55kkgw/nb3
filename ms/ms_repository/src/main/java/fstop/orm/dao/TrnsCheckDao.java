package fstop.orm.dao;

import java.io.Serializable;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.LegacyJpaRepository;

import fstop.orm.po.TRNSCHECK;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Transactional
public class TrnsCheckDao extends LegacyJpaRepository<TRNSCHECK, Serializable>{

	public void insertTrnsCheck(int apiTxnNo, String feeRefId, String sessionNo, String srcId, String rCode, String trnsCode, String txn_datetime) throws Exception {
		TRNSCHECK trnsCheck = new TRNSCHECK();
		trnsCheck.setAPI_TXN_NO(apiTxnNo);
		trnsCheck.setFEE_REF_ID(feeRefId);
		trnsCheck.setSESSION_ID(sessionNo);
		trnsCheck.setSRC_ID(srcId);
		trnsCheck.setTRNSCODE(trnsCode);
		trnsCheck.setTXN_DATETIME(txn_datetime);
		trnsCheck.setRCODE(rCode);
		trnsCheck.setMTI("0900");
		trnsCheck.setPCODE("7131");
		save(trnsCheck);
	}

	public void updateTrnsCheck(int apiTxnNo, String feeRefId, String sessionNo, String srcId, String rCode, String trnsCode, String txn_datetime) throws Exception {
		TRNSCHECK trnsCheck = findById(apiTxnNo);
		if(trnsCheck != null) {
			trnsCheck.setFEE_REF_ID(feeRefId);
			trnsCheck.setSESSION_ID(sessionNo);
			trnsCheck.setSRC_ID(srcId);
			trnsCheck.setTRNSCODE(trnsCode);
			trnsCheck.setTXN_DATETIME(txn_datetime);
			trnsCheck.setRCODE(rCode);
			trnsCheck.setMTI("0910");
			trnsCheck.setPCODE("7131");
			update(trnsCheck);
		}
	}
}
