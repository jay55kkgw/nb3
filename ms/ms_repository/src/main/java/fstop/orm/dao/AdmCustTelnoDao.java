package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import fstop.orm.po.ADMCUSTTELNO;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;


@Slf4j
@Repository
@Transactional
public class AdmCustTelnoDao extends LegacyJpaRepository<ADMCUSTTELNO, String> {
	protected Logger logger = Logger.getLogger(getClass());
	
	public List<ADMCUSTTELNO> getAll() {
		return find("FROM ADMCUSTTELNO ORDER BY ADMCUSTID");
	}
	
	@SuppressWarnings({ "unused", "unchecked" })
	public List<ADMCUSTTELNO> findByEmpId(String ADMCUSTID) {
		List result = new ArrayList();
		StringBuffer sb = new StringBuffer();
		Query query = getQuery("from ADMCUSTTELNO e where ADMCUSTID = ?",
				ADMCUSTID);
		//query.setMaxResults(100);
		return query.getResultList();
	}
	
	public void insert(ADMCUSTTELNO admcusttelno) {
		int r2=0;
		String fillstr=new String();
		Query add1=null;
		
		add1=this.getSession().createSQLQuery("INSERT INTO ADMCUSTTELNO VALUES(? ,? ,? ,? )");
		add1.setParameter(0, admcusttelno.getADMCUSTID());
		log.debug("ADMCUSTID = " + admcusttelno.getADMCUSTID());
		add1.setParameter(1, admcusttelno.getADMTELNO());
		log.debug("ADMTELNO = " + admcusttelno.getADMTELNO());
		add1.setParameter(2, admcusttelno.getADMDATE());
		log.debug("ADMDATE = " + admcusttelno.getADMDATE());
		add1.setParameter(3, admcusttelno.getADMTIME());
		log.debug("ADMTIME = " + admcusttelno.getADMTIME());
		r2 = add1.executeUpdate();
	}

	public void deleteAll() {
		this.getSession().createSQLQuery("DELETE FROM ADMCUSTTELNO").executeUpdate();
	}

	public void deleteByID(String admcustid) {
		Query add1= getSession().createSQLQuery("DELETE FROM ADMCUSTTELNO where ADMCUSTID like ?");
		add1.setParameter(0, admcustid);
		add1.executeUpdate();
	}
	
}