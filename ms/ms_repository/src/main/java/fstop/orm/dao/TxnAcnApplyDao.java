package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import fstop.orm.po.TXNACNAPPLY;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.query.NativeQuery;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Slf4j
@Repository
@Transactional
public class TxnAcnApplyDao extends LegacyJpaRepository<TXNACNAPPLY, Long> {

	public List<TXNACNAPPLY> getAll() {
		return find("FROM TXNACNAPPLY ORDER BY LOGID");
	}

	public boolean getacnapplycnt(String cusidn) {
		List<TXNACNAPPLY> result = find(
				"FROM TXNACNAPPLY WHERE CUSIDN = ? AND ACNTYPE IS NOT NULL",
				cusidn);
		if (result.size() > 0)
			return true;
		else
			return false;
	}

	public void deleteAll() {
		this.getSession().createSQLQuery("DELETE FROM TXNACNAPPLY")
				.executeUpdate();
	}

	public String findByCUSIDN(String cusidn) {

		Query query = getSession()
				.createSQLQuery(
						"SELECT ACNTYPE FROM TXNACNAPPLY WHERE CUSIDN = ? AND ACNTYPE IS NOT NULL ORDER BY LASTDATE DESC, LASTTIME DESC");
		query.setParameter(0, cusidn);
		List qresult = query.getResultList();
		return qresult.get(0).toString();
	}

	public String findNAMEByCUSIDN(String cusidn) {

		Query query = getSession()
				.createSQLQuery(
						"SELECT NAME FROM TXNACNAPPLY WHERE CUSIDN = ? AND ACNTYPE IS NOT NULL ORDER BY LASTDATE DESC, LASTTIME DESC");
		query.setParameter(0, cusidn);
		List qresult = query.getResultList();
		return qresult.get(0).toString();
	}

	public String findACNByCUSIDN(String cusidn) {

		Query query = getSession()
				.createSQLQuery(
						"SELECT ACN FROM TXNACNAPPLY WHERE CUSIDN = ? AND ACNTYPE IS NOT NULL ORDER BY LASTDATE DESC, LASTTIME DESC");
		query.setParameter(0, cusidn);
		List qresult = query.getResultList();
		return qresult.get(0).toString();
	}

	public String findBRHCODByCUSIDN(String cusidn) {

		Query query = getSession()
				.createSQLQuery(
						"SELECT BRHCOD FROM TXNACNAPPLY WHERE CUSIDN = ? AND ACNTYPE IS NOT NULL ORDER BY LASTDATE DESC, LASTTIME DESC");
		query.setParameter(0, cusidn);
		List qresult = query.getResultList();
		return qresult.get(0).toString();
	}

	public String findPURPOSEByCUSIDN(String cusidn) {

		Query query = getSession()
				.createSQLQuery(
						"SELECT PURPOSE FROM TXNACNAPPLY WHERE CUSIDN = ? AND ACNTYPE IS NOT NULL ORDER BY LASTDATE DESC, LASTTIME DESC");
		query.setParameter(0, cusidn);
		List qresult = query.getResultList();
		return qresult.get(0).toString();
	}

	public String findPREASONByCUSIDN(String cusidn) {

		Query query = getSession()
				.createSQLQuery(
						"SELECT PREASON FROM TXNACNAPPLY WHERE CUSIDN = ? AND ACNTYPE IS NOT NULL ORDER BY LASTDATE DESC, LASTTIME DESC");
		query.setParameter(0, cusidn);
		List qresult = query.getResultList();
		return qresult.get(0).toString();
	}

	public String findBDEALINGByCUSIDN(String cusidn) {

		Query query = getSession()
				.createSQLQuery(
						"SELECT BDEALING FROM TXNACNAPPLY WHERE CUSIDN = ? AND ACNTYPE IS NOT NULL ORDER BY LASTDATE DESC, LASTTIME DESC");
		query.setParameter(0, cusidn);
		List qresult = query.getResultList();
		return qresult.get(0).toString();
	}

	public String findBREASONByCUSIDN(String cusidn) {

		Query query = getSession()
				.createSQLQuery(
						"SELECT BREASON FROM TXNACNAPPLY WHERE CUSIDN = ? AND ACNTYPE IS NOT NULL ORDER BY LASTDATE DESC, LASTTIME DESC");
		query.setParameter(0, cusidn);
		List qresult = query.getResultList();
		return qresult.get(0).toString();
	}
	
	public String findEMPLOYERByCUSIDN(String cusidn) {

		Query query = getSession()
				.createSQLQuery(
						"SELECT EMPLOYER FROM TXNACNAPPLY WHERE CUSIDN = ? AND ACNTYPE IS NOT NULL ORDER BY LASTDATE DESC, LASTTIME DESC");
		query.setParameter(0, cusidn);
		List qresult = query.getResultList();
		return qresult.get(0).toString();
	}

	public String findCUSIDNByMAILADDRCELPHONE(String mail, String cellPhone) {

		Query query = getSession()
				.createSQLQuery(
						"SELECT CUSIDN FROM TXNACNAPPLY WHERE (  MAILADDR = ? OR ( CELPHONE = ?  AND CELPHONE <> '' ) ) AND ACNTYPE IS NOT NULL ORDER BY LASTDATE DESC, LASTTIME DESC");
		query.setParameter(0, mail);
		query.setParameter(1, cellPhone);

		List qresult = query.getResultList();
		if (qresult.size() > 0) {
			return qresult.get(0).toString();
		} else {
			return " ";
		}

	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public int updateAcnApplyData(String CUSIDN, String BRHCOD, String PURPOSE,
			String PREASON, String BDEALING, String BREASON, String IP,String EMPLOYER) {
		// String jsonParam = JSONDC.replace("'", "\"");
		// Map params = JSONUtils.json2map(jsonParam);
		Query add1 = this
				.getSession()
				.createSQLQuery(
						"UPDATE TXNACNAPPLY SET PURPOSE=? ,PREASON=? ,BDEALING=? ,BREASON=? ,LASTDATE=? ,LASTTIME=? ,IP=?, EMPLOYER=?  WHERE CUSIDN = ? and BRHCOD = ? AND ACNTYPE IS NOT NULL" +
						" AND LOGID IN (SELECT MAX(LOGID) AS ID FROM TXNACNAPPLY GROUP BY CUSIDN, BRHCOD)");
		Date d = new Date();
		add1.setParameter(0, PURPOSE);
		add1.setParameter(1, PREASON);
		add1.setParameter(2, BDEALING);
		add1.setParameter(3, BREASON);
		add1.setParameter(4, DateTimeUtils.format("yyyyMMdd", d));
		add1.setParameter(5, DateTimeUtils.format("HHmmss", d));
		add1.setParameter(6, IP);
		add1.setParameter(7, EMPLOYER);//20180920 add 任職機構
		add1.setParameter(8, CUSIDN);
		add1.setParameter(9, BRHCOD);
		int r2 = add1.executeUpdate();
		return r2;
	}

	public String findLOGID(String acn) {

		Query query = getSession().createSQLQuery(
				"SELECT LOGID FROM TXNACNAPPLY WHERE ACN = ?");
		query.setParameter(0, acn);
		List qresult = query.getResultList();
		return qresult.get(0).toString();
	}

	public TXNACNAPPLY getApplyDataByCUSIDN(String cusidn) {
		List<TXNACNAPPLY> result = find(
				"FROM TXNACNAPPLY WHERE CUSIDN = ? AND ACNTYPE IS NOT NULL",
				cusidn);
		if (result.size() == 0) {
			TXNACNAPPLY txnAcnApply = new TXNACNAPPLY();
			return txnAcnApply;
		} else {
			return result.get(0);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public int updateApplyData(String CARDAPPLY, String NAATAPPLY,
			String CChargeApply, String CROSSAPPLY, String CRDTYP, String LMT,
			String CUSIDN) {
		// String jsonParam = JSONDC.replace("'", "\"");
		// Map params = JSONUtils.json2map(jsonParam);
		Query add1 = this
				.getSession()
				.createSQLQuery(
						"UPDATE TXNACNAPPLY SET CARDAPPLY=? ,NAATAPPLY=? ,CChargeApply=? ,CROSSAPPLY=? ,LASTDATE=? ,LASTTIME=? ,CRDTYP=? ,LMT=?   WHERE CUSIDN = ? AND ACNTYPE IS NOT NULL" +
						" AND LOGID IN (SELECT MAX(LOGID) AS ID FROM TXNACNAPPLY GROUP BY CUSIDN)");
		Date d = new Date();
		add1.setParameter(0, CARDAPPLY);
		add1.setParameter(1, NAATAPPLY);
		add1.setParameter(2, CChargeApply);
		add1.setParameter(3, CROSSAPPLY);
		add1.setParameter(4, DateTimeUtils.format("yyyyMMdd", d));
		add1.setParameter(5, DateTimeUtils.format("HHmmss", d));
		add1.setParameter(6, CRDTYP);
		add1.setParameter(7, LMT);
		add1.setParameter(8, CUSIDN);
		int r2 = add1.executeUpdate();
		return r2;
	}

	public boolean isRelated(String ip, String mail, String cellPhone) {
		List<TXNACNAPPLY> result = find("FROM TXNACNAPPLY WHERE IP = ?", ip);
		if (result.size() > 0) {
			for (TXNACNAPPLY t : result) {
				if (t.getMAILADDR().equals(mail)
						|| t.getCELPHONE().equals(cellPhone)) {
					return true;
				}
			}
		} else {
			return false;
		}
		return false;
	}

	public List<TXNACNAPPLY> findByBRANCH(String BRHCOD) {
		return find("FROM TXNACNAPPLY WHERE BRHCOD = ? ORDER BY LOGID", BRHCOD);
	}

	public List<TXNACNAPPLY> findByBRANCH(String BRHCOD, String date) {
		return find("FROM TXNACNAPPLY WHERE BRHCOD = ? AND LASTDATE = ? ORDER BY LOGID",BRHCOD, date);
	}	

	public List<TXNACNAPPLY> findByBRANCHwithoutACNDetail(String BRHCOD,List<String> ACNSQL) {
		String sql = "SELECT * FROM TXNACNAPPLY WHERE BRHCOD = ? AND ACN NOT IN (:detail) order by MAILADDR";
			Query query1 = getSession().createSQLQuery(sql);
			query1.unwrap(NativeQuery.class).addEntity("record1", TXNACNAPPLY.class);
			query1.setParameter(0, BRHCOD);
			//query1.setParameterList("detail", ACNSQL);
			query1.setParameter("detail", ACNSQL);
			List<TXNACNAPPLY> result = query1.getResultList();

		return result;
	}		

	public List<TXNACNAPPLY> getSameBranchSameMailOrPhone(String MAILADDR,String CELPHONE,int count,List<String> ACNSQL,String BRHCOD,String ACN) {   //取得與該分行客戶電子郵件與電話相同客戶
	
		
		String sql = "SELECT * FROM TXNACNAPPLY WHERE (  MAILADDR = ? OR ( CELPHONE = ?  AND CELPHONE <> '' ) ) AND BRHCOD = ?  AND ACN NOT IN (:detail) order by MAILADDR";
		String sql1 = "SELECT * FROM TXNACNAPPLY WHERE   MAILADDR = ? AND  CELPHONE = ?  AND ACN = ? AND BRHCOD = ? order by MAILADDR";
		String sql2 = "SELECT * FROM TXNACNAPPLY WHERE ( (  MAILADDR = ? OR ( CELPHONE = ?  AND CELPHONE <> '' ) ) AND BRHCOD <> ? AND ACN NOT IN (:detail)) order by MAILADDR";

		List<String> nACNSQL = new LinkedList();
		nACNSQL.add(ACN);
		nACNSQL.addAll(ACNSQL);
		
		List<TXNACNAPPLY> txnAcnList = new LinkedList();
		
			Query query = getSession().createSQLQuery(sql1);
			query.unwrap(NativeQuery.class).addEntity("record2", TXNACNAPPLY.class);  //撈取該帳戶
			query.setParameter(0, MAILADDR);
			query.setParameter(1, CELPHONE);
			query.setParameter(2, ACN);
			query.setParameter(3, BRHCOD);
			List<TXNACNAPPLY> result = query.getResultList();
	
			Query query1 = getSession().createSQLQuery(sql);
			query1.unwrap(NativeQuery.class).addEntity("record", TXNACNAPPLY.class);  //撈取同分行之帳戶
			query1.setParameter(0, MAILADDR);
			query1.setParameter(1, CELPHONE);
			query1.setParameter(2, BRHCOD);			
//			query1.setParameterList("detail", nACNSQL);
			query1.setParameter("detail", nACNSQL);
			List<TXNACNAPPLY> result1 = query1.getResultList();
			
			Query query2 = getSession().createSQLQuery(sql2);
			query2.unwrap(NativeQuery.class).addEntity("record1", TXNACNAPPLY.class);  //撈取不同分行之帳戶
			query2.setParameter(0, MAILADDR);
			query2.setParameter(1, CELPHONE);
			query2.setParameter(2, BRHCOD);
//			query2.setParameterList("detail", ACNSQL);
			query2.setParameter("detail", ACNSQL);
			List<TXNACNAPPLY> result2 = query2.getResultList();

			if (result1.size() > 0 || result2.size() > 0) {
				for (TXNACNAPPLY ta : result) {
					TXNACNAPPLY txn = new TXNACNAPPLY();					
					txn.setACN(ta.getACN());
					txn.setMAILADDR(ta.getMAILADDR());
					txn.setNAME(ta.getNAME());
					txn.setCUSIDN(ta.getCUSIDN());
					txn.setCELPHONE(ta.getCELPHONE());
					txn.setBRHCOD(ta.getBRHCOD());
					txn.setFAX("  C" + count);					
					txnAcnList.add(txn);
				}
			}			
			
			if (result1.size() > 0) {
				for (TXNACNAPPLY ta : result1) {
					TXNACNAPPLY txn = new TXNACNAPPLY();					
					txn.setACN(ta.getACN());
					txn.setMAILADDR(ta.getMAILADDR());
					txn.setNAME(ta.getNAME());
					txn.setCUSIDN(ta.getCUSIDN());
					txn.setCELPHONE(ta.getCELPHONE());
					txn.setBRHCOD(ta.getBRHCOD());
					txn.setFAX("  C" + count);					
					txnAcnList.add(txn);
				}
			}


			if (result2.size() > 0) {
				for (TXNACNAPPLY ta : result2) {
					TXNACNAPPLY txn = new TXNACNAPPLY();					
					txn.setACN(ta.getACN());
					txn.setMAILADDR(ta.getMAILADDR());
					txn.setNAME(ta.getNAME());
					txn.setCUSIDN(ta.getCUSIDN());
					txn.setCELPHONE(ta.getCELPHONE());
					txn.setBRHCOD(ta.getBRHCOD());
					txn.setFAX("  C" + count);					
					txnAcnList.add(txn);
				}
			}			

		return txnAcnList;
	}	
	
	public List<TXNACNAPPLY> getSameMailOrPhone(String MAILADDR,String CELPHONE,int count,String ACNSQL,String REFSQL) {
		String sql = "SELECT * FROM TXNACNAPPLY WHERE (  MAILADDR = ? OR ( CELPHONE = ?  AND CELPHONE <> '' ) ) AND ACN NOT IN ("+ACNSQL+","+REFSQL+") order by MAILADDR";
		List<TXNACNAPPLY> txnAcnList = new LinkedList();
			Query query1 = getSession().createSQLQuery(sql);
			query1.unwrap(NativeQuery.class).addEntity("record1", TXNACNAPPLY.class);
			query1.setParameter(0, MAILADDR);
			query1.setParameter(1, CELPHONE);
			List<TXNACNAPPLY> result1 = query1.getResultList();
			
			if (result1.size() > 2) {
				for (TXNACNAPPLY ta : result1) {
					TXNACNAPPLY txn = new TXNACNAPPLY();					
					txn.setACN(ta.getACN());
					txn.setMAILADDR(ta.getMAILADDR());
					txn.setNAME(ta.getNAME());
					txn.setCUSIDN(ta.getCUSIDN());
					txn.setCELPHONE(ta.getCELPHONE());
					txn.setBRHCOD(ta.getBRHCOD());
					txn.setFAX("  C" + count);					
					txnAcnList.add(txn);
				}
			}

		return txnAcnList;
	}		
	
	public List<TXNACNAPPLY> getSameBranchSameMail(String MAILADDR,int count,List<String> ACNSQL,String BRHCOD) {   //取得電子郵件相同的客戶(按分行排列)
	
		
		String sql1 = "SELECT * FROM TXNACNAPPLY WHERE   MAILADDR = ? AND BRHCOD = ?  AND ACN NOT IN (:detail) order by MAILADDR";
		String sql2 = "SELECT * FROM TXNACNAPPLY WHERE  MAILADDR = ? AND BRHCOD <> ? AND ACN NOT IN (:detail) order by MAILADDR";
		
		List<String> nACNSQL = new LinkedList();
		nACNSQL.addAll(ACNSQL);
		
		List<TXNACNAPPLY> txnAcnList = new LinkedList();
		List<TXNACNAPPLY> result1 = new LinkedList();
		List<TXNACNAPPLY> result2 = new LinkedList();

			Query query1 = getSession().createSQLQuery(sql1);
			query1.unwrap(NativeQuery.class).addEntity("record", TXNACNAPPLY.class);  //撈取同分行之帳戶
			query1.setParameter(0, MAILADDR);
			query1.setParameter(1, BRHCOD);			
//			query1.setParameterList("detail", nACNSQL);
			query1.setParameter("detail", nACNSQL);
			result1 = query1.getResultList();
			
			Query query2 = getSession().createSQLQuery(sql2);
			query2.unwrap(NativeQuery.class).addEntity("record1", TXNACNAPPLY.class);  //撈取不同分行之帳戶
			query2.setParameter(0, MAILADDR);
			query2.setParameter(1, BRHCOD);
//			query2.setParameterList("detail", ACNSQL);
			query2.setParameter("detail", ACNSQL);
			result2 = query2.getResultList();

			if (result1.size()+result2.size() >= 2 || result1.size() >= 2 || result2.size() >= 2) {
				for (TXNACNAPPLY ta : result1) {
					TXNACNAPPLY txn = new TXNACNAPPLY();					
					txn.setACN(ta.getACN());
					txn.setMAILADDR(ta.getMAILADDR());
					txn.setNAME(ta.getNAME());
					txn.setCUSIDN(ta.getCUSIDN());
					txn.setCELPHONE(ta.getCELPHONE());
					txn.setBRHCOD(ta.getBRHCOD());
					txn.setFAX("  D" + count);					
					txnAcnList.add(txn);
				}
				for (TXNACNAPPLY ta : result2) {
					TXNACNAPPLY txn = new TXNACNAPPLY();					
					txn.setACN(ta.getACN());
					txn.setMAILADDR(ta.getMAILADDR());
					txn.setNAME(ta.getNAME());
					txn.setCUSIDN(ta.getCUSIDN());
					txn.setCELPHONE(ta.getCELPHONE());
					txn.setBRHCOD(ta.getBRHCOD());
					txn.setFAX("  D" + count);					
					txnAcnList.add(txn);
				}				
			}					

		return txnAcnList;
	}
	
	public List<TXNACNAPPLY> getSameBranchSamePhone(String CELPHONE,int count,List<String> ACNSQL,String BRHCOD) {   //取得電子郵件相同的客戶(按分行排列)
			
		String sql1 = "SELECT * FROM TXNACNAPPLY WHERE   ( CELPHONE = ?  AND CELPHONE <> '' )  AND BRHCOD = ?  AND ACN NOT IN (:detail) order by MAILADDR";
		String sql2 = "SELECT * FROM TXNACNAPPLY WHERE  ( CELPHONE = ?  AND CELPHONE <> '' )  AND BRHCOD <> ? AND ACN NOT IN (:detail) order by MAILADDR";
			
		List<String> nACNSQL = new LinkedList();
		nACNSQL.addAll(ACNSQL);
		
		List<TXNACNAPPLY> txnAcnList = new LinkedList();
		List<TXNACNAPPLY> result1 = new LinkedList();
		List<TXNACNAPPLY> result2 = new LinkedList();
	
			Query query1 = getSession().createSQLQuery(sql1);
			query1.unwrap(NativeQuery.class).addEntity("record", TXNACNAPPLY.class);  //撈取同分行之帳戶
			query1.setParameter(0, CELPHONE);
			query1.setParameter(1, BRHCOD);			
//			query1.setParameterList("detail", nACNSQL);
			query1.setParameter("detail", nACNSQL);
			result1 = query1.getResultList();
			
			Query query2 = getSession().createSQLQuery(sql2);
			query2.unwrap(NativeQuery.class).addEntity("record1", TXNACNAPPLY.class);  //撈取不同分行之帳戶
			query2.setParameter(0, CELPHONE);
			query2.setParameter(1, BRHCOD);
//			query2.setParameterList("detail", ACNSQL);
			query2.setParameter("detail", ACNSQL);
			result2 = query2.getResultList();

			if (result1.size()+result2.size() >= 2 || result1.size() >= 2 || result2.size() >= 2) {
				for (TXNACNAPPLY ta : result1) {
					TXNACNAPPLY txn = new TXNACNAPPLY();					
					txn.setACN(ta.getACN());
					txn.setMAILADDR(ta.getMAILADDR());
					txn.setNAME(ta.getNAME());
					txn.setCUSIDN(ta.getCUSIDN());
					txn.setCELPHONE(ta.getCELPHONE());
					txn.setBRHCOD(ta.getBRHCOD());
					txn.setFAX("  C" + count);					
					txnAcnList.add(txn);
				}
				for (TXNACNAPPLY ta : result2) {
					TXNACNAPPLY txn = new TXNACNAPPLY();					
					txn.setACN(ta.getACN());
					txn.setMAILADDR(ta.getMAILADDR());
					txn.setNAME(ta.getNAME());
					txn.setCUSIDN(ta.getCUSIDN());
					txn.setCELPHONE(ta.getCELPHONE());
					txn.setBRHCOD(ta.getBRHCOD());
					txn.setFAX("  C" + count);					
					txnAcnList.add(txn);
				}				
			}					

		return txnAcnList;
	}
	
}
