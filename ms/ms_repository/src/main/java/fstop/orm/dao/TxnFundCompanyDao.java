package fstop.orm.dao;

import java.util.List;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.TXNFUNDCOMPANY;

@Slf4j
@Repository
@Transactional
public class TxnFundCompanyDao extends LegacyJpaRepository<TXNFUNDCOMPANY, String> {
	protected Logger logger = Logger.getLogger(getClass());
	
	
	
	public void deleteAll() {
		this.getSession().createQuery("DELETE FROM TXNFUNDCOMPANY").executeUpdate();
	}

	public List findAllAndSort() {
		return find("FROM TXNFUNDCOMPANY WHERE FUNDHOUSEMARK='Y' ORDER BY COUNTRYTYPE DESC ");
	}
	
	public List findOverseaAndSort() {
		return find("FROM TXNFUNDCOMPANY WHERE FUNDHOUSEMARK='Y' and COUNTRYTYPE<> 'C' ORDER BY COUNTRYTYPE DESC ");
	}
	
}
