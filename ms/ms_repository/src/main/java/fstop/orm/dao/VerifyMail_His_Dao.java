package fstop.orm.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.function.BiFunction;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.hibernate.query.NativeQuery;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;
import com.netbank.domain.orm.core.SessionAdapter;

import fstop.orm.po.VERIFYMAIL_HIS;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository

@Transactional(
		transactionManager="nb3transactionManager",
        rollbackFor = {Throwable.class}
)
public class VerifyMail_His_Dao extends LegacyJpaRepository<VERIFYMAIL_HIS, String> {
	protected Logger logger = Logger.getLogger(getClass());
	
	
	@SuppressWarnings("unchecked")
	public List<VERIFYMAIL_HIS> findById_with_month (String cusidn , int month) {
		List<VERIFYMAIL_HIS> rtnList = new ArrayList<VERIFYMAIL_HIS>();
		try {
			
			Calendar cal = Calendar.getInstance();
			String sDate = DateTimeUtils.getDateShort(cal.getTime());
			cal.add(Calendar.MONTH, -month);
			String eDate = DateTimeUtils.getDateShort(cal.getTime());
			
			StringBuilder sb = new StringBuilder();
			sb.append("SELECT * FROM VERIFYMAIL_HIS WHERE CUSIDN = ? AND LASTDATE <= ? AND LASTDATE >= ? ORDER BY LASTDATE DESC , LASTTIME DESC , STATUS");
			Query query = getSession().createSQLQuery(sb.toString());
			query.unwrap(NativeQuery.class).addEntity("VERIFYMAIL_HIS", VERIFYMAIL_HIS.class);
			query.setParameter(0, cusidn);
			query.setParameter(1, sDate);
			query.setParameter(2, eDate);
			
			rtnList = query.getResultList();
		
		}catch (Exception e) {
			log.error("=== findById_with_month error ===  {}" , e.getMessage());
		}
		
		return rtnList;
	}
	
	
	@Override
	public EntityManager getEntityManager() {
		// TODO Auto-generated method stub
		return super.getEntityManager();
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return super.count();
	}

	@Override
	public List<VERIFYMAIL_HIS> findAll() {
		// TODO Auto-generated method stub
		return super.findAll();
	}

	@Override
	public void save(VERIFYMAIL_HIS entity) {
		// TODO Auto-generated method stub
		super.save(entity);
	}

	@Override
	public VERIFYMAIL_HIS update(VERIFYMAIL_HIS entity) {
		// TODO Auto-generated method stub
		return super.update(entity);
	}

	@Override
	public void delete(VERIFYMAIL_HIS entity) {
		// TODO Auto-generated method stub
		super.delete(entity);
	}

	@Override
	public Query getQuery(String hql, Object... values) {
		// TODO Auto-generated method stub
		return super.getQuery(hql, values);
	}

	@Override
	public Query getCriteriaQuery(Class<VERIFYMAIL_HIS> entityClass,
			BiFunction<Root, CriteriaBuilder, Predicate>... criteriaFunction) {
		// TODO Auto-generated method stub
		return super.getCriteriaQuery(entityClass, criteriaFunction);
	}

	@Override
	public List find(String hql, Object... values) {
		// TODO Auto-generated method stub
		return super.find(hql, values);
	}

	@Override
	public List<VERIFYMAIL_HIS> findBy(Class<VERIFYMAIL_HIS> entityClass, String name, Object value) {
		// TODO Auto-generated method stub
		return super.findBy(entityClass, name, value);
	}

	@Override
	public VERIFYMAIL_HIS findUniqueBy(Class<VERIFYMAIL_HIS> entityClass, String name, Object value) {
		// TODO Auto-generated method stub
		return super.findUniqueBy(entityClass, name, value);
	}

	@Override
	public List<VERIFYMAIL_HIS> findByLike(Class<VERIFYMAIL_HIS> entityClass, String name, String value) {
		// TODO Auto-generated method stub
		return super.findByLike(entityClass, name, value);
	}

	@Override
	public SessionAdapter getSession() {
		// TODO Auto-generated method stub
		return super.getSession();
	}

	@Override
	public Page pagedQuery(String hql, int pageNo, int pageSize, Object... values) {
		// TODO Auto-generated method stub
		return super.pagedQuery(hql, pageNo, pageSize, values);
	}
	
	
	
	
}