package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.util.ESAPIUtil;

import fstop.orm.po.ADMHOLIDAY;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.Date;
import java.util.List;


@Slf4j
@Repository
@Transactional
public class AdmHolidayDao extends LegacyJpaRepository<ADMHOLIDAY, String> {
	protected Logger logger = Logger.getLogger(getClass());
	/**
	 * 判斷該日是否為營業日
	 * @param uid
	 * @return
	 */
	@SuppressWarnings( { "unused", "unchecked" })
	public String findADREMARK(Date date) {

		String due = DateTimeUtils.format("yyyyMMdd", date);	
		this.logger.debug("due  ===>" + due);		
		
		Query query = getSession().createSQLQuery(
				"SELECT ADREMARK FROM ADMHOLIDAY WHERE ADHOLIDAYID = ?");
		query.setParameter(0, due);
		List qresult = query.getResultList();
		this.logger.debug(ESAPIUtil.vaildLog("qresult  ===>" + qresult));	
		String result = "";
		
		if(qresult.isEmpty()){
			result = "";
		}else{
			result = qresult.get(0).toString();
		}
		
		return result;
	}
	
	public boolean isAdmholiday(Date date) {

		String due = DateTimeUtils.format("yyyyMMdd", date);	
		this.logger.debug("due  ===>" + due);		
		
		Query query = getSession().createSQLQuery(
				"SELECT * FROM ADMHOLIDAY WHERE ADHOLIDAYID = ?");
		query.setParameter(0, due);
		List qresult = query.getResultList();
		this.logger.debug(ESAPIUtil.vaildLog("qresult  ===>" + qresult));	
		String result = "";
		
		if(!qresult.isEmpty()){
			return true;
		}else{
			return false;
		}
		

	}	

}
