package fstop.orm.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.util.ESAPIUtil;

import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.model.Rows;
import fstop.orm.po.SYSOPGROUP;
import fstop.util.StrUtils;

import javax.persistence.Query;

@Slf4j
@Repository
@Transactional
public class SysOpGroupDao extends LegacyJpaRepository<SYSOPGROUP, String> {
	protected Logger logger = Logger.getLogger(getClass());

	public Rows getGroupChilds(String gpauth) {
		String sql = "" +	
		"SELECT A.ADOPID AS VALUE, B.ADOPNAME AS TEXT "                +
		"FROM SYSOPGROUP A LEFT JOIN SYSOP B ON A.ADOPID = B.ADOPID "  +
		"WHERE A.ADOPGROUP = ? "                                       + 
		"ORDER BY A.ADSEQ "                                            ;

		javax.persistence.Query query = getSession().createSQLQuery(sql);
		query.setParameter(0, gpauth);
		
		Rows result = new Rows();
		
		// Stored XSS
		List<Object> qresult = query.getResultList();
		if (null == qresult) {
			qresult = new ArrayList();
		}
		qresult = ESAPIUtil.validList(qresult);
//		List qresult = query.getResultList();
		
		Iterator it = qresult.iterator();
		while(it.hasNext()) {
			Object[] o = (Object[])it.next();
			Map rm = new HashMap();
			int t = -1;

			rm.put("VALUE", o[++t].toString());
			rm.put("TEXT", o[++t].toString());
			
			Row r = new Row(rm);
			result.addRow(r);
		}
		
		return result;
	}
	
	private void fetchAllApp(MVHImpl m , List resultList) {
		for (String key : (Vector<String>) m.getTableKeys()) {
			MVHImpl t = (MVHImpl) m.getTableByName(key);
			// log.debug(StrUtils.repeat("\t", deep) + key + ", " +
			// t.getOccurs().getSize() + "," + t.getValueByFieldName("TEXT"));

			for (int j = 0; j < t.getValueOccurs("ADOPID"); j++) {
				if (!"Y".equals(t.getValueByFieldName("ADISPARENT"))) {
					// log.debug(StrUtils.repeat("\t", deep + 1) +
					// "OCCURS: " + t.getValueByFieldName("ADOPID", j+1) + "," +
					// t.getValueByFieldName("ADOPNAME", j+1));

					resultList.add(t.getValueByFieldName("TEXT"));
					resultList.add(t.getValueByFieldName("ADOPID", j + 1));
					resultList.add(t.getValueByFieldName("ADOPNAME", j + 1));
				}
			}

			fetchAllApp(t, resultList);
		}
	}
	
	public Rows findGroupApp(String gpauth) {
		MVHImpl qresult = getGroup(gpauth);
		List lresult = new ArrayList();
		fetchAllApp(qresult, lresult);
		Rows result = new Rows();
		for(int i =0; i < lresult.size(); ) {
			Map rm = new HashMap();
			rm.put("GROUPNAME", lresult.get(i));
			rm.put("ADOPID", lresult.get(i+1));
			rm.put("ADOPNAME", lresult.get(i+2));
			
			Row r = new Row(rm);
			result.addRow(r);
			i+=3;
		}
		return result;
	}
	
	public MVHImpl getGroup(String gpauth) {
		return getGroup(gpauth, new String[]{});
	}
	public MVHImpl getGroup(String gpauth, String[] adopids) {
		
		MVHImpl result = new MVHImpl();
		
		List root = new java.util.LinkedList<String>();
		List desc = new java.util.LinkedList<String>();
		
		Query query = getQuery("SELECT  s.ADOPGROUP, o.ADOPNAME " +
				"	from SYSOPGROUP s, SYSOP o where  s.ADOPGROUP = o.ADOPID  AND s.ADOPGROUP = ?  ",
				gpauth);

		Object[] oret = (Object[])query.getResultList().iterator().next();
		root.add(oret[0]);
		desc.add(oret[1]);
		
		MVHImpl mvh = getGroup(gpauth, adopids, result, root, desc);
		
		//return mvh;
		
		if(result.getTableByName(gpauth) == null)
			return new MVHImpl();
		else
			return (MVHImpl)result.getTableByName(gpauth);
	}
	
	public MVHImpl getGroup(String gpauth, String[] adopids, MVHImpl mvh, List<String> grpList, List<String> grpListDesc) {
		try {
			Query query = getQuery("SELECT  s.ADOPGROUPID, s.ADOPGROUP, s.ADOPID, s.ADISPARENT, o.ADOPNAME " +
					"	from SYSOPGROUP s, SYSOP o where  s.ADOPID = o.ADOPID  AND s.ADOPGROUP = ? AND ADISPARENT='Y' ORDER BY s.ADSEQ",
					gpauth);
			
			List qresult = query.getResultList();
			
			Iterator it = qresult.iterator();
			while(it.hasNext()) {
				Object[] o = (Object[])it.next();
				String adopgroupid = (String)o[0];
				String adopgroup = (String)o[1];
				String adopid = (String)o[2];
				String adisparent = (String)o[3];
				String adopname = (String)o[4];
				
				//if("Y".equalsIgnoreCase(adisparent)) {
				
					grpList.add(adopid);
					grpListDesc.add(adopname);
					
					getGroup(adopid, adopids, mvh, grpList, grpListDesc);
	
					grpList.remove(adopid);
					grpListDesc.remove(adopname);
			}
		}
		finally {}
		
		try {
					StringBuffer sb = new StringBuffer();
					if(adopids != null && adopids.length > 0) {
						for(String ap : adopids) {
							if(sb.length() > 0 && StrUtils.trim(ap).length() > 0)
								sb.append(",");
							sb.append(" '").append(ap).append("' ");
						}
					}
					String condition = (sb.length() > 0 ) ? " AND s.ADOPID IN (" + sb.toString() + ") " : "";
					
					// logger.debug("SQL: SELECT s.ADOPGROUPID, s.ADOPGROUP,
					// s.ADOPID, s.ADISPARENT, o.ADOPNAME " +
					// " from SYSOPGROUP s, SYSOP o where s.ADOPID = o.ADOPID
					// AND s.ADOPGROUP = ? " + condition);
					// logger.debug("adopgroup: '" + adopgroup + "'");
					Query query2 = getQuery("SELECT s.ADOPGROUPID, s.ADOPGROUP, s.ADOPID, s.ADISPARENT, o.ADOPNAME " +
							"	from SYSOPGROUP s, SYSOP o where  s.ADOPID = o.ADOPID  AND s.ADOPGROUP = ?  AND ADISPARENT='N' " + condition + " ORDER BY s.ADSEQ" ,
							gpauth);
					
					// Stored XSS
					List<Object> qresult2 = query2.getResultList();
					if (null == qresult2) {
						qresult2 = new ArrayList();
					}
					qresult2 = ESAPIUtil.validList(qresult2);
//					List qresult2 = query2.getResultList();
					
					if(qresult2.size() > 0  ) {
						
						MVHImpl lastMVHImpl = mvh;
						int c = -1;  
						for(String grpName : grpList) {
							c++;
							logger.debug(ESAPIUtil.vaildLog("\t" + grpName));
						
							MVHImpl m = (MVHImpl)lastMVHImpl.getTableByName(grpName);
							if(m == null) {
								MVHImpl newMvh = new MVHImpl();
								lastMVHImpl.addTable(newMvh, grpName);
								logger.debug(ESAPIUtil.vaildLog("add table: " + grpName + " -- size: " + grpList.size()));
								newMvh.getFlatValues().put("TEXT", grpListDesc.get(c));
								m = newMvh;
							}
							lastMVHImpl = m;
							
						}
					
							Rows rows = new Rows();
							String gp = "";
							for(int j=0; j < qresult2.size(); j++) {
								Object[] ox = (Object[])qresult2.get(j);

								String _adopgroupid = (String)ox[0];
								String _adopgroup = (String)ox[1];
								String _adopid = (String)ox[2];
								String _adisparent = (String)ox[3];
								String _adopname = (String)ox[4];
								Map rm = new HashMap();
								rm.put("ADOPGROUPID", _adopgroupid);
								rm.put("ADOPGROUP", _adopgroup);
								rm.put("ADOPID", _adopid);
								rm.put("ADISPARENT", _adisparent);
								rm.put("ADOPNAME", _adopname);
								
								Row r = new Row(rm);
								rows.addRow(r);
							}
							//MVHImpl newmvh = new MVHImpl(rows);
							
							//newmvh.getFlatValues().put("TEXT", StrUtils.trim(grpListDesc.get(grpListDesc.size() - 1)));
							logger.debug(ESAPIUtil.vaildLog("add table ++ " + gpauth));
							lastMVHImpl.getOccurs().addRows(rows);
						
						}
						
					
					
					
		}
		finally{} 
		
		return mvh;
	}
	
	public boolean isQueryGroup(String adopid) {
		try {
			if (adopid == null)
				return false;
			
			Integer l = (Integer)find("SELECT 1 FROM SYSOPGROUP WHERE ADOPGROUP=? and ADOPID=?", "QUERYGROUP", adopid).iterator().next();
			if(l == null )
				return false;
			else if(l == 1)
				return true;
		}
		catch(Exception e) {
			logger.error("isQueryGroup = "+e);
		}
		
		return false;
	}

	public boolean isFundMenuGroup(String trancode) {
		try {
			//Integer l = (Integer)find("SELECT 1 FROM SYSOPGROUP WHERE ADOPGROUP='MENUGPN4' and ADOPID=? and NOT EXISTS (select 1 from SYSOPGROUP WHERE ADOPGROUP='NBGP11' and ADOPID=?)", trancode, trancode).iterator().next();
			Integer l = (Integer)find("SELECT 1 FROM SYSOPGROUP WHERE ADOPGROUP='MENUGPN4' and ADOPID=?", trancode).iterator().next();
			if(l == null )
				return false;
			else if(l == 1)
				return true;
		}
		catch(Exception e) {
			logger.error("isFundMenuGroup = "+e);
		}
		
		return false;
	}	
	
	public boolean isFxMenuGroup(String trancode) {
		try {
			if (trancode.equals("F001") || trancode.equals("F002") || trancode.equals("F003"))
				return true;
			else
				return false;
		}
		catch(Exception e) {
			logger.error("isFxMenuGroup = "+e);
		}
		
		return false;
	}		
		
} 
