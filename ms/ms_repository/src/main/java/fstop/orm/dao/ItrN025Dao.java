package fstop.orm.dao;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import fstop.orm.po.ITRN025;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.List;

@Slf4j
@Repository
@Transactional
public class ItrN025Dao extends LegacyJpaRepository<ITRN025, String> {
	protected Logger logger = Logger.getLogger(getClass());

	public List<ITRN025> getAll() {
		return find("FROM ITRN025");
	}
	
	public void insert(ITRN025 itrn025) {
		int r2=0;
		String fillstr=new String();
		Query add1=null;
		
		add1=this.getSession().createSQLQuery("INSERT INTO ITRN025 VALUES(? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,?)");
		add1.setParameter(0, itrn025.getRECNO());
		log.debug("RECNO = " + itrn025.getRECNO());
		add1.setParameter(1, itrn025.getHEADER());
		log.debug("HEADER = " + itrn025.getHEADER());
		add1.setParameter(2, itrn025.getSEQ());
		log.debug("SEQ = " + itrn025.getSEQ());
		add1.setParameter(3, itrn025.getDATE());
		log.debug("DATE = " + itrn025.getDATE());
		add1.setParameter(4, itrn025.getTIME());
		log.debug("TIME = " + itrn025.getTIME());
		add1.setParameter(5, itrn025.getCOUNT());
		log.debug("COUNT = " + itrn025.getCOUNT());
		add1.setParameter(6, itrn025.getCOLOR());
		log.debug("COLOR = " + itrn025.getCOLOR());
		add1.setParameter(7, itrn025.getACC());
		log.debug("ACC = " + itrn025.getACC());
		add1.setParameter(8, (itrn025.getITR1()==null ? "       ":itrn025.getITR1()));
		log.debug("ITR1 = " + itrn025.getITR1());
		add1.setParameter(9, itrn025.getMARK());
		log.debug("MARK = " + (itrn025.getMARK()==null ? " ":itrn025.getMARK()));
		add1.setParameter(10, itrn025.getITR2());
		log.debug("ITR2 = " + (itrn025.getITR2()==null ? "       ":itrn025.getITR2()));
		if(itrn025.getFILL()==null)
			fillstr=" ";
		else
			fillstr=itrn025.getFILL();
		log.debug("FILL = " + fillstr);
		add1.setParameter(11, fillstr);
		r2 = add1.executeUpdate();
	}

	public void deleteAll() {
		this.getSession().createSQLQuery("DELETE FROM ITRN025").executeUpdate();
	}
}
