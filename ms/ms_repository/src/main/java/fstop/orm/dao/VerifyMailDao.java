package fstop.orm.dao;

import java.util.List;
import java.util.function.BiFunction;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.hibernate.query.NativeQuery;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.LegacyJpaRepository;
import com.netbank.domain.orm.core.Page;
import com.netbank.domain.orm.core.SessionAdapter;

import fstop.orm.po.VERIFYMAIL;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository

@Transactional(transactionManager = "nb3transactionManager", rollbackFor = { Throwable.class })
public class VerifyMailDao extends LegacyJpaRepository<VERIFYMAIL, String> {
	protected Logger logger = Logger.getLogger(getClass());

	public VERIFYMAIL findActiveMailById(String cusidn) {

		VERIFYMAIL data = null;
		try {
			StringBuilder sb = new StringBuilder();
			sb.append("SELECT * FROM VERIFYMAIL WHERE CUSIDN = ? AND STATUS = 0 ");
			Query query = getSession().createSQLQuery(sb.toString());
			query.unwrap(NativeQuery.class).addEntity("VERIFYMAIL", VERIFYMAIL.class);
			query.setParameter(0, cusidn);

			data = (VERIFYMAIL) query.getResultList().get(0);

		} catch (Exception e) {

			log.error("=== findActiveMailById error ===  {}", e.getMessage());
		}
		return data;
	}

	public List<VERIFYMAIL> findByExpireDate(String execdate , String perioddate) {

		List<VERIFYMAIL> datas = null;
		try {
			StringBuilder sb = new StringBuilder();
			if(StrUtils.isEmpty(perioddate)) {
				sb.append("SELECT * FROM VERIFYMAIL WHERE STATUS='0' AND EXPIREDATE = ? ");
				Query query = getSession().createSQLQuery(sb.toString());
				query.setParameter(0, execdate);
				query.unwrap(NativeQuery.class).addEntity("VERIFYMAIL", VERIFYMAIL.class);
				datas = (List<VERIFYMAIL>)query.getResultList();
			}else {
				sb.append("SELECT * FROM VERIFYMAIL WHERE STATUS='0' AND EXPIREDATE <= ? AND EXPIREDATE >= ?");
				Query query = getSession().createSQLQuery(sb.toString());
				query.setParameter(0, execdate);
				query.setParameter(1, perioddate);
				query.unwrap(NativeQuery.class).addEntity("VERIFYMAIL", VERIFYMAIL.class);
				datas = (List<VERIFYMAIL>)query.getResultList();
			}

		} catch (Exception e) {

			log.error("=== findByExpireDate error ===  {}", e);
		}
		return datas;
	}

	@Override
	public EntityManager getEntityManager() {
		// TODO Auto-generated method stub
		return super.getEntityManager();
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return super.count();
	}

	@Override
	public List<VERIFYMAIL> findAll() {
		// TODO Auto-generated method stub
		return super.findAll();
	}

	@Override
	public VERIFYMAIL findById(String id) {
		// TODO Auto-generated method stub
		return super.findById(id);
	}

	@Override
	public void save(VERIFYMAIL entity) {
		// TODO Auto-generated method stub
		super.save(entity);
	}

	@Override
	public VERIFYMAIL update(VERIFYMAIL entity) {
		// TODO Auto-generated method stub
		return super.update(entity);
	}

	@Override
	public void delete(VERIFYMAIL entity) {
		// TODO Auto-generated method stub
		super.delete(entity);
	}

	@Override
	public Query getQuery(String hql, Object... values) {
		// TODO Auto-generated method stub
		return super.getQuery(hql, values);
	}

	@Override
	public Query getCriteriaQuery(Class<VERIFYMAIL> entityClass,
			BiFunction<Root, CriteriaBuilder, Predicate>... criteriaFunction) {
		// TODO Auto-generated method stub
		return super.getCriteriaQuery(entityClass, criteriaFunction);
	}

	@Override
	public List find(String hql, Object... values) {
		// TODO Auto-generated method stub
		return super.find(hql, values);
	}

	@Override
	public List<VERIFYMAIL> findBy(Class<VERIFYMAIL> entityClass, String name, Object value) {
		// TODO Auto-generated method stub
		return super.findBy(entityClass, name, value);
	}

	@Override
	public VERIFYMAIL findUniqueBy(Class<VERIFYMAIL> entityClass, String name, Object value) {
		// TODO Auto-generated method stub
		return super.findUniqueBy(entityClass, name, value);
	}

	@Override
	public List<VERIFYMAIL> findByLike(Class<VERIFYMAIL> entityClass, String name, String value) {
		// TODO Auto-generated method stub
		return super.findByLike(entityClass, name, value);
	}

	@Override
	public SessionAdapter getSession() {
		// TODO Auto-generated method stub
		return super.getSession();
	}

	@Override
	public Page pagedQuery(String hql, int pageNo, int pageSize, Object... values) {
		// TODO Auto-generated method stub
		return super.pagedQuery(hql, pageNo, pageSize, values);
	}

}