package fstop.orm.old;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * 
 * 功能說明 :外匯帳務交易之記錄檔
 *
 */
@Entity
@Table(name = "TXNFXRECORD")
@Data
public class OLD_TXNFXRECORD implements Serializable
{

	private static final long serialVersionUID = 8083836611043536458L;

	@Id
	private String ADTXNO;// 交易編號

	private String ADOPID = "";// 操作功能ID

	private String FXUSERID = "";// 使用者ID

	private String FXTXDATE = "";// 實際轉帳日期

	private String FXTXTIME = "";// 實際轉帳時間

	private String FXWDAC = "";// 轉出帳號

	private String FXSVBH = "";// 轉入行庫代碼

	private String FXSVAC = "";// 轉入帳號

	private String FXWDCURR = "";// 轉出幣別

	private String FXWDAMT = "";// 轉出金額

	private String FXSVCURR = "";// 轉入幣別

	private String FXSVAMT = "";// 轉入金額

	private String FXTXMEMO = "";// 備註

	private String FXTXMAILS = "";// 發送Mail清單

	private String FXTXMAILMEMO = "";// Mail備註

	private String FXTXCODE = "";// 交易機制

	private String FXTITAINFO = "";// 交易上行資訊

	private String FXTOTAINFO = "";// 交易下行資訊

	private Long FXSCHID = 0L;// 預約交易ID

	private String FXEFEECCY = "";// 手續費幣別

	private String FXEFEE = "";// 手續費

	private String FXTELFEE = "";// 郵電費

	private String FXOURCHG = "";// 國外費用

	private String FXEXRATE = "";// 匯率

	@Column(nullable = false)
	private String FXCERT = "";// 交易單據

	private String FXREMAIL = "";// 重發Mail次數

	private String FXTXSTATUS = "";// 交易執行狀態

	private String FXEXCODE = "";// 交易錯誤代碼

	private String FXMSGSEQNO = "";// 電文傳送狀態

	private String FXMSGCONTENT = ""; // STAN

	private String LASTDATE = "";// 最後異動日期

	private String LASTTIME = "";// 最後異動時間

	private String LOGINTYPE = "NB";// NB登入或MB登入

}
