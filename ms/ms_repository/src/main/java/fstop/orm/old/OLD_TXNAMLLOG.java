package fstop.orm.old;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

import lombok.Data;

@Entity(name="fstop.orm.old.OLD_TXNAMLLOG")
@Table(name = "TXNAMLLOG")
@Data
public class OLD_TXNAMLLOG implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1402219664126630215L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long LOGID;
	
	private String SOURCESYSTEM = "";

	private String SEARCHCODE = "";

	private String BRANCHID = "";

	private String BUSINESSUNIT = "";

	private String ADOPID = "";

	private String TS1 = "";

	private String TS2 = "";

	private String STATUS = "";

	private String QUERY_CONTENT = "";

	private String RESPOND_CONTENT = "";

	@Override
	public String toString() {
		return "TXNAMLLOG [LOGID=" + LOGID + ", SOURCESYSTEM=" + SOURCESYSTEM + ", SEARCHCODE=" + SEARCHCODE
				+ ", BRANCHID=" + BRANCHID + ", BUSINESSUNIT=" + BUSINESSUNIT + ", ADOPID=" + ADOPID + ", TS1=" + TS1
				+ ", TS2=" + TS2 + ", STATUS=" + STATUS + ", QUERY_CONTENT=" + QUERY_CONTENT + ", RESPOND_CONTENT="
				+ RESPOND_CONTENT + "]";
	}
	
	
	
	
}
