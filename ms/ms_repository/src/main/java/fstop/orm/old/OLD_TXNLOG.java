package fstop.orm.old;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="fstop.orm.old.OLD_TXNLOG")
@Table(name = "TXNLOG")
public class OLD_TXNLOG implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5608037463377926298L;

	@Id
	private String ADTXNO = "";

	private String ADUSERID = "";

	private String ADOPID = "";

	private String FGTXWAY = "";

	private String ADTXACNO = "";

	private String ADTXAMT = "";

	private String ADCURRENCY = "";

	private String ADSVBH = "";

	private String ADAGREEF = "";

	private String ADREQTYPE = "";

	private String ADUSERIP = "";

	private String ADCONTENT = "";

	private String ADEXCODE = "";

	private String LASTDATE = "";

	private String LASTTIME = "";
	
	private String ADTXID = "";  //指出哪一個電文有誤
	
	private String ADGUID = "";
	
	private String LOGINTYPE = "";//判斷NB:網銀，MB:行動

	public String getADTXNO() {
		return ADTXNO;
	}

	public void setADTXNO(String aDTXNO) {
		ADTXNO = aDTXNO;
	}

	public String getADUSERID() {
		return ADUSERID;
	}

	public void setADUSERID(String aDUSERID) {
		ADUSERID = aDUSERID;
	}

	public String getADOPID() {
		return ADOPID;
	}

	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}

	public String getFGTXWAY() {
		return FGTXWAY;
	}

	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}

	public String getADTXACNO() {
		return ADTXACNO;
	}

	public void setADTXACNO(String aDTXACNO) {
		ADTXACNO = aDTXACNO;
	}

	public String getADTXAMT() {
		return ADTXAMT;
	}

	public void setADTXAMT(String aDTXAMT) {
		ADTXAMT = aDTXAMT;
	}

	public String getADCURRENCY() {
		return ADCURRENCY;
	}

	public void setADCURRENCY(String aDCURRENCY) {
		ADCURRENCY = aDCURRENCY;
	}

	public String getADSVBH() {
		return ADSVBH;
	}

	public void setADSVBH(String aDSVBH) {
		ADSVBH = aDSVBH;
	}

	public String getADAGREEF() {
		return ADAGREEF;
	}

	public void setADAGREEF(String aDAGREEF) {
		ADAGREEF = aDAGREEF;
	}

	public String getADREQTYPE() {
		return ADREQTYPE;
	}

	public void setADREQTYPE(String aDREQTYPE) {
		ADREQTYPE = aDREQTYPE;
	}

	public String getADUSERIP() {
		return ADUSERIP;
	}

	public void setADUSERIP(String aDUSERIP) {
		ADUSERIP = aDUSERIP;
	}

	public String getADCONTENT() {
		return ADCONTENT;
	}

	public void setADCONTENT(String aDCONTENT) {
		ADCONTENT = aDCONTENT;
	}

	public String getADEXCODE() {
		return ADEXCODE;
	}

	public void setADEXCODE(String aDEXCODE) {
		ADEXCODE = aDEXCODE;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lASTDATE) {
		LASTDATE = lASTDATE;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}

	public String getADTXID() {
		return ADTXID;
	}

	public void setADTXID(String aDTXID) {
		ADTXID = aDTXID;
	}

	public String getADGUID() {
		return ADGUID;
	}

	public void setADGUID(String aDGUID) {
		ADGUID = aDGUID;
	}

	public String getLOGINTYPE() {
		return LOGINTYPE;
	}

	public void setLOGINTYPE(String lOGINTYPE) {
		LOGINTYPE = lOGINTYPE;
	}


}
