package fstop.orm.old;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity(name="fstop.orm.old.OLD_SYSOP")
@Table(name = "SYSOP")
public class OLD_SYSOP implements Serializable {

	@Id
	//@GeneratedValue(strategy = GenerationType.AUTO)
	private String ADOPID;

	private String ADOPNAME;

	private String ADOPMEMO;

	private String ADOPALIVE;

	private String LASTUSER;

	private String LASTDATE;

	private String LASTTIME;

	public String toString() {
		return new ToStringBuilder(this).append("adopid", getADOPID())
				.toString();
	}

	public String getADOPALIVE() {
		return ADOPALIVE;
	}

	public void setADOPALIVE(String adopalive) {
		ADOPALIVE = adopalive;
	}

	public String getADOPID() {
		return ADOPID;
	}

	public void setADOPID(String adopid) {
		ADOPID = adopid;
	}

	public String getADOPMEMO() {
		return ADOPMEMO;
	}

	public void setADOPMEMO(String adopmemo) {
		ADOPMEMO = adopmemo;
	}

	public String getADOPNAME() {
		return ADOPNAME;
	}

	public void setADOPNAME(String adopname) {
		ADOPNAME = adopname;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

	public String getLASTUSER() {
		return LASTUSER;
	}

	public void setLASTUSER(String lastuser) {
		LASTUSER = lastuser;
	}

}
