package fstop.orm.old;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "TXNCUSINVATTRIB")
public class OLD_TXNCUSINVATTRIB implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -343213267843693137L;

	@Id
	private String FDUSERID;

	private String FDQ1;
	private String FDQ2;
	private String FDQ3;
	private String FDQ4;
	private String FDQ5;
	private String FDSCORE;
	private String FDINVTYPE;
	private String LASTUSER;
	private String LASTDATE;
	private String LASTTIME;
	private String FDQ6;
	private String FDQ7;
	private String FDQ8;
	private String FDQ9;
	private String FDQ10;
	private String FDQ11;
	private String FDQ12;
	private String FDQ13;
	private String FDQ14;
	private String FDQ15;
	private String MARK1;
	private String ADUSERIP;
	
	public String getFDINVTYPE() {
		return FDINVTYPE;
	}
	public void setFDINVTYPE(String fdinvtype) {
		FDINVTYPE = fdinvtype;
	}
	public String getFDQ1() {
		return FDQ1;
	}
	public void setFDQ1(String fdq1) {
		FDQ1 = fdq1;
	}
	public String getFDQ2() {
		return FDQ2;
	}
	public void setFDQ2(String fdq2) {
		FDQ2 = fdq2;
	}
	public String getFDQ3() {
		return FDQ3;
	}
	public void setFDQ3(String fdq3) {
		FDQ3 = fdq3;
	}
	public String getFDQ4() {
		return FDQ4;
	}
	public void setFDQ4(String fdq4) {
		FDQ4 = fdq4;
	}
	public String getFDQ5() {
		return FDQ5;
	}
	public void setFDQ5(String fdq5) {
		FDQ5 = fdq5;
	}
	public String getFDSCORE() {
		return FDSCORE;
	}
	public void setFDSCORE(String fdscore) {
		FDSCORE = fdscore;
	}
	public String getFDUSERID() {
		return FDUSERID;
	}
	public void setFDUSERID(String fduserid) {
		FDUSERID = fduserid;
	}
	
	public String getLASTUSER() {
		return LASTUSER;
	}
	public void setLASTUSER(String lastuser) {
		LASTUSER = lastuser;
	}
	public String getLASTDATE() {
		return LASTDATE;
	}
	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}
	public String getLASTTIME() {
		return LASTTIME;
	}
	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}	
	public String getFDQ6() {
		return FDQ6;
	}
	public void setFDQ6(String fdq6) {
		FDQ6 = fdq6;
	}
	public String getFDQ7() {
		return FDQ7;
	}
	public void setFDQ7(String fdq7) {
		FDQ7 = fdq7;
	}
	public String getFDQ8() {
		return FDQ8;
	}
	public void setFDQ8(String fdq8) {
		FDQ8 = fdq8;
	}
	public String getFDQ9() {
		return FDQ9;
	}
	public void setFDQ9(String fdq9) {
		FDQ9 = fdq9;
	}
	public String getFDQ10() {
		return FDQ10;
	}
	public void setFDQ10(String fdq10) {
		FDQ10 = fdq10;
	}
	public String getFDQ11() {
		return FDQ11;
	}
	public void setFDQ11(String fdq11) {
		FDQ11 = fdq11;
	}
	public String getFDQ12() {
		return FDQ12;
	}
	public void setFDQ12(String fdq12) {
		FDQ12 = fdq12;
	}
	public String getFDQ13() {
		return FDQ13;
	}
	public void setFDQ13(String fdq13) {
		FDQ13 = fdq13;
	}	
	public String getFDQ14() {
		return FDQ14;
	}
	public void setFDQ14(String fdq14) {
		FDQ14 = fdq14;
	}	
	public String getFDQ15() {
		return FDQ15;
	}
	public void setFDQ15(String fdq15) {
		FDQ15 = fdq15;
	}
	public String getMARK1() {
		return MARK1;
	}
	public void setMARK1(String mark1) {
		MARK1 = mark1;
	}
	public String getADUSERIP() {
		return ADUSERIP;
	}
	public void setADUSERIP(String aDUSERIP) {
		ADUSERIP = aDUSERIP;
	}
}
