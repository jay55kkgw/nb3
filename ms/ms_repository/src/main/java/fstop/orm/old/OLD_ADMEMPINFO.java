package fstop.orm.old;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity(name="fstop.orm.old.OLD_ADMEMPINFO")
@Table(name = "ADMEMPINFO")
public class OLD_ADMEMPINFO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5182385157591962371L;

	@Id
	private String EMPNO;

	private String EMPMAIL;
	
	private String CUSID;

	private String LDATE;
	
	private String DEPNO;
	
	public String toString() {
		return new ToStringBuilder(this).append("empno", getEMPNO())
				.toString();
	}

	public String getEMPNO() {
		return EMPNO;
	}

	public void setEMPNO(String empno) {
		EMPNO = empno;
	}

	public String getEMPMAIL() {
		return EMPMAIL;
	}

	public void setEMPMAIL(String empmail) {
		EMPMAIL = empmail;
	}
	
	public String getCUSID() {
		return CUSID;
	}

	public void setCUSID(String cusid) {
		CUSID = cusid;
	}

	public String getLDATE() {
		return LDATE;
	}

	public void setLDATE(String ldate) {
		LDATE = ldate;
	}
	
	public String getDEPNO() {
		return DEPNO;
	}

	public void setDEPNO(String depno) {
		DEPNO = depno;
	}
	
}
