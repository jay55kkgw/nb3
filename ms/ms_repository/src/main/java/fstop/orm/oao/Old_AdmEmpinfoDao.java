package fstop.orm.oao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.NNBJpaRepository;
import com.netbank.util.ESAPIUtil;

import fstop.orm.old.OLD_ADMEMPINFO;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Transactional(value = "nnbtransactionManager")
public class Old_AdmEmpinfoDao extends NNBJpaRepository<OLD_ADMEMPINFO, String> {

	// For NNBReport_M902.java
	public List<OLD_ADMEMPINFO> findLeaveEmp(String CUSID, String LDATE) {

		/*
		 * SQLQuery add1 = getSession().
		 * createSQLQuery("SELECT * FROM ADMEMPINFO WHERE CUSID = ? and LDATE < ?");
		 * add1.setParameter(0, CUSID); add1.setParameter(1, LDATE); List result0 =
		 * add1.list(); return result0;
		 */

		log.debug(ESAPIUtil.vaildLog("CUSID = " + CUSID + "  LDATE = " + LDATE));
		List result = new ArrayList();
		StringBuffer sb = new StringBuffer();
		// 找出小於離職日ldate的資料
		Query query = getQuery("from fstop.orm.old.OLD_ADMEMPINFO e where CUSID=? and LDATE<>'' and LDATE > ?", CUSID,
				LDATE);

		// query.setMaxResults(100);
		return query.getResultList();

	}

}
