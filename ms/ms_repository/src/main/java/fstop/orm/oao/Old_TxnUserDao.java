package fstop.orm.oao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.query.NativeQuery;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.NNBJpaRepository;
import com.netbank.domain.orm.core.Page;
import com.netbank.domain.orm.core.SessionAdapter;
import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.model.Rows;
import fstop.orm.old.OLD_TXNUSER;
import fstop.orm.po.TXNUSER;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Transactional(value = "nnbtransactionManager")
public class Old_TxnUserDao extends NNBJpaRepository<OLD_TXNUSER, String> {
	// protected Logger logger = Logger.getLogger(getClass());

	@Transactional(value = "nnbtransactionManager", readOnly = true)
	public void deleteSave(String id, String adbranchid) {

		OLD_TXNUSER oldpo = findById(id);
		oldpo.setADBRANCHID(adbranchid);
		update(oldpo);
	}

	@Override
	public EntityManager getEntityManager() {
		// TODO Auto-generated method stub
		return super.getEntityManager();
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return super.count();
	}

	@Override
	public List<OLD_TXNUSER> findAll() {
		// TODO Auto-generated method stub
		return super.findAll();
	}

	@Override
	public OLD_TXNUSER findById(String id) {
		// TODO Auto-generated method stub
		return super.findById(id);
	}

	@Override
	public void save(OLD_TXNUSER entity) {
		// TODO Auto-generated method stub
		super.save(entity);
	}

	@Override
	public void saveOrUpdate(OLD_TXNUSER entity) {
		// TODO Auto-generated method stub
		super.saveOrUpdate(entity);
	}

	@Override
	public OLD_TXNUSER update(OLD_TXNUSER entity) {
		// TODO Auto-generated method stub
		return super.update(entity);
	}

	@Override
	public OLD_TXNUSER updateI(String id) {
		// TODO Auto-generated method stub
		return super.updateI(id);
	}

	@Override
	public OLD_TXNUSER updateII(OLD_TXNUSER entity) {
		// TODO Auto-generated method stub
		return super.updateII(entity);
	}

	@Override
	public void removeById(String id) {
		// TODO Auto-generated method stub
		super.removeById(id);
	}

	@Override
	public void delete(OLD_TXNUSER entity) {
		// TODO Auto-generated method stub
		super.delete(entity);
	}

	@Override
	public Query getQuery(String hql, Object... values) {
		// TODO Auto-generated method stub
		return super.getQuery(hql, values);
	}

	@Override
	public Query getCriteriaQuery(Class<OLD_TXNUSER> entityClass,
			BiFunction<Root, CriteriaBuilder, Predicate>... criteriaFunction) {
		// TODO Auto-generated method stub
		return super.getCriteriaQuery(entityClass, criteriaFunction);
	}

	@Override
	public List find(String hql, Object... values) {
		// TODO Auto-generated method stub
		return super.find(hql, values);
	}

	@Override
	public List<OLD_TXNUSER> findBy(Class<OLD_TXNUSER> entityClass, String name, Object value) {
		// TODO Auto-generated method stub
		return super.findBy(entityClass, name, value);
	}

	@Override
	public OLD_TXNUSER findUniqueBy(Class<OLD_TXNUSER> entityClass, String name, Object value) {
		// TODO Auto-generated method stub
		return super.findUniqueBy(entityClass, name, value);
	}

	@Override
	public List<OLD_TXNUSER> findByLike(Class<OLD_TXNUSER> entityClass, String name, String value) {
		// TODO Auto-generated method stub
		return super.findByLike(entityClass, name, value);
	}

	@Override
	public SessionAdapter getSession() {
		// TODO Auto-generated method stub
		return super.getSession();
	}

	@Override
	public Page pagedQuery(String hql, int pageNo, int pageSize, Object... values) {
		// TODO Auto-generated method stub
		return super.pagedQuery(hql, pageNo, pageSize, values);
	}

	// For EmailUpload.java
	public List<OLD_TXNUSER> getAllBillUserLast2Day(String tomorrow, String yesterday) {
		/*
		 * return find("FROM TXNUSER WHERE ((BILLDATE < '" + tomorrow +
		 * "' and BILLDATE >= '" + yesterday + "') " + " or (FUNDBILLDATE < '" +
		 * tomorrow + "' and FUNDBILLDATE >= '" + yesterday + "') " +
		 * " or (CARDBILLDATE < '" + tomorrow + "' and CARDBILLDATE >= '" + yesterday +
		 * "')) " + " ORDER BY DPSUERID");
		 */
		String subyesterday = yesterday.substring(0, 7);
		return find("FROM fstop.orm.old.OLD_TXNUSER WHERE ((BILLDATE like '" + subyesterday + "%') "
				+ " or (FUNDBILLDATE like '" + subyesterday + "%') " + " or (CARDBILLDATE like '" + subyesterday
				+ "%')) " + " ORDER BY DPSUERID");
	}

	// For EmailUploadAll.java
	public List<OLD_TXNUSER> getBillUser() {
		return find(
				"FROM fstop.orm.old.OLD_TXNUSER WHERE DPMYEMAIL <> '' AND (DPBILL <> '' OR DPFUNDBILL <> '' OR DPCARDBILL <> '') ORDER BY DPSUERID");
	}

	// For EmailUploadFund.java
	public List<OLD_TXNUSER> getAllFundBillUser() {
		return find(
				"FROM fstop.orm.old.OLD_TXNUSER WHERE DPFUNDBILL <> '' and FUNDBILLDATE <> '' and LENGTH(DPMYEMAIL) > 0 ORDER BY DPSUERID");
	}

	//
	public List<OLD_TXNUSER> findByUserId(String DPSUERID) {
		List result = new ArrayList();
		StringBuffer sb = new StringBuffer();
		Query query = getQuery("from fstop.orm.old.OLD_TXNUSER e where DPSUERID = ?", DPSUERID);
		// query.setMaxResults(100);
		return ESAPIUtil.validStrList(query.getResultList());
	}

	// For STOPDIF.java
	public List<OLD_TXNUSER> getAllFundStopNotifyUser() {
		return find(
				"FROM fstop.orm.old.OLD_TXNUSER WHERE DPNOTIFY like '%14%' and LENGTH(DPMYEMAIL) > 0 ORDER BY DPSUERID");
	}

	// For NNBReport_M910.java
	public MVHImpl findByNNBReportM910() {

		log.debug("IN findByNNBReportM910");
		Rows result = new Rows();
		MVHImpl mvhresult = null;

		String sql = "SELECT T.ADBRANCHID,T.DPUSERNAME,T.DPSUERID,T.DPMYEMAIL,amf.EMPNO FROM TXNUSER T LEFT JOIN ADMEMPINFO amf ON T.DPMYEMAIL =amf.EMPMAIL  WHERE T.DPMYEMAIL LIKE '%mail.tbb.com.tw' AND T.DPSUERID NOT IN (SELECT CUSID FROM ADMEMPINFO WHERE EMPNO!='' AND CUSID!='') and amf.LDATE='' ORDER BY ADBRANCHID ASC";

		try {
			Query query = getSession().createSQLQuery(sql.toString());

			query.unwrap(NativeQuery.class).addScalar("ADBRANCHID", StringType.INSTANCE);
			query.unwrap(NativeQuery.class).addScalar("DPUSERNAME", StringType.INSTANCE);
			query.unwrap(NativeQuery.class).addScalar("DPSUERID", StringType.INSTANCE);
			query.unwrap(NativeQuery.class).addScalar("DPMYEMAIL", StringType.INSTANCE);
			query.unwrap(NativeQuery.class).addScalar("EMPNO", StringType.INSTANCE);

			List qresult = query.getResultList();
			Iterator it = qresult.iterator();
			while (it.hasNext()) {
				Object[] o = (Object[]) it.next();
				Map rm = new HashMap();
				int t = -1;
				rm.put("ADBRANCHID", o[++t].toString());
				rm.put("DPUSERNAME", (o[++t] == null ? "" : o[t].toString()));
				rm.put("DPSUERID", (o[++t] == null ? "" : o[t].toString()));
				rm.put("DPMYEMAIL", (o[++t] == null ? "" : o[t].toString()));
				rm.put("EMPNO", (o[++t] == null ? "" : o[t].toString()));

				Row r = new Row(rm);
				result.addRow(r);
			}

			mvhresult = new MVHImpl(result);

			return mvhresult;

		} catch (Exception e) {
			e.printStackTrace();
			throw (TopMessageException.create("ZX99"));
		}

	}
	
	public long checkStatus() {
		String SQL = "SELECT COUNT(*) FROM TXNUSER";
		return ((Long) find(SQL,null).get(0)).longValue();
	}
}