package fstop.orm.oao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.query.NativeQuery;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.NNBJpaRepository;
import com.netbank.domain.orm.core.Page;
import com.netbank.domain.orm.core.SessionAdapter;
import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.model.Rows;
import fstop.orm.old.OLD_TXNLOG;
import fstop.orm.old.OLD_TXNUSER;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Transactional(value = "nnbtransactionManager")
public class Old_TxnLogDao extends NNBJpaRepository<OLD_TXNLOG, String> {
	// protected Logger log = Logger.getLogger(getClass());

	@Override
	public EntityManager getEntityManager() {
		// TODO Auto-generated method stub
		return super.getEntityManager();
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return super.count();
	}

	@Override
	public List<OLD_TXNLOG> findAll() {
		// TODO Auto-generated method stub
		return super.findAll();
	}

	@Override
	public OLD_TXNLOG findById(String id) {
		// TODO Auto-generated method stub
		return super.findById(id);
	}

	@Override
	public void save(OLD_TXNLOG entity) {
		// TODO Auto-generated method stub
		super.save(entity);
	}

	@Override
	public void saveOrUpdate(OLD_TXNLOG entity) {
		// TODO Auto-generated method stub
		super.saveOrUpdate(entity);
	}

	@Override
	public OLD_TXNLOG update(OLD_TXNLOG entity) {
		// TODO Auto-generated method stub
		return super.update(entity);
	}

	@Override
	public OLD_TXNLOG updateI(String id) {
		// TODO Auto-generated method stub
		return super.updateI(id);
	}

	@Override
	public OLD_TXNLOG updateII(OLD_TXNLOG entity) {
		// TODO Auto-generated method stub
		return super.updateII(entity);
	}

	@Override
	public void removeById(String id) {
		// TODO Auto-generated method stub
		super.removeById(id);
	}

	@Override
	public void delete(OLD_TXNLOG entity) {
		// TODO Auto-generated method stub
		super.delete(entity);
	}

	@Override
	public Query getQuery(String hql, Object... values) {
		// TODO Auto-generated method stub
		return super.getQuery(hql, values);
	}

	@Override
	public Query getCriteriaQuery(Class<OLD_TXNLOG> entityClass,
			BiFunction<Root, CriteriaBuilder, Predicate>... criteriaFunction) {
		// TODO Auto-generated method stub
		return super.getCriteriaQuery(entityClass, criteriaFunction);
	}

	@Override
	public List find(String hql, Object... values) {
		// TODO Auto-generated method stub
		return super.find(hql, values);
	}

	@Override
	public List<OLD_TXNLOG> findBy(Class<OLD_TXNLOG> entityClass, String name, Object value) {
		// TODO Auto-generated method stub
		return super.findBy(entityClass, name, value);
	}

	@Override
	public OLD_TXNLOG findUniqueBy(Class<OLD_TXNLOG> entityClass, String name, Object value) {
		// TODO Auto-generated method stub
		return super.findUniqueBy(entityClass, name, value);
	}

	@Override
	public List<OLD_TXNLOG> findByLike(Class<OLD_TXNLOG> entityClass, String name, String value) {
		// TODO Auto-generated method stub
		return super.findByLike(entityClass, name, value);
	}

	@Override
	public SessionAdapter getSession() {
		// TODO Auto-generated method stub
		return super.getSession();
	}

	@Override
	public Page pagedQuery(String hql, int pageNo, int pageSize, Object... values) {
		// TODO Auto-generated method stub
		return super.pagedQuery(hql, pageNo, pageSize, values);
	}

	// For FundTransList.java
	public List<OLD_TXNLOG> findByTxnlogTime(String lastDate) {

		String sql = "select a.* from TXNLOG a,TXNUSER b,SYSOP c where a.aduserid=b.dpsuerid and a.ADEXCODE='' and a.adopid in ('C016','C017','C021','C024','C031','C032','C033','C111') and a.adopid=c.adopid and a.lastdate = ?  and a.ADUSERIP like '10.%' order by a.aduserip";

		Query query = getSession().createSQLQuery(sql);
		query.unwrap(NativeQuery.class).addEntity("a", OLD_TXNLOG.class);

		query.setParameter(0, lastDate);

		return query.getResultList();
	}

	// For FundReport_M901.java
	public List<OLD_TXNLOG> findByFundReportM901(String lastDate, String BRH) {

		String sql = "select a.* from TXNLOG a,TXNUSER b, SYSOP c where a.aduserid=b.dpsuerid and a.ADEXCODE='' and a.adopid in ('C016','C017','C021','C024','C031','C032','C033') and a.adopid=c.adopid and a.lastdate like '"
				+ lastDate + "' and a.ADUSERIP in (select ADBRANCHIP from ADMBRANCHIP where ADBRANCHID = '" + BRH
				+ "') order by a.aduserip";
		Query query = getSession().createSQLQuery(sql);
		query.unwrap(NativeQuery.class).addEntity("a", OLD_TXNLOG.class);

		return ESAPIUtil.validStrList(query.getResultList());
	}

	// For NNBReport_M902.java
	public List<OLD_TXNLOG> findByNNBReportM902(String lastDate, String BRH) {
		String sql = "";
		if (BRH.indexOf("|") >= 0) // 部室超過一個網段的處理方式
		{
			String[] names = BRH.split("|");
			String connectSQL = "";
			int counter = 0;
			for (String name : names) {
				if (counter == 0)
					connectSQL = connectSQL + "a.aduserip like '" + name + "'";
				else if (counter > 0)
					connectSQL = connectSQL + " or a.aduserip like '" + name + "'";
				counter++;
			}
			sql = "select a.* from TXNLOG a,TXNUSER b,SYSOP c where a.aduserid=b.dpsuerid and a.ADEXCODE='' and a.adopid in ('N174','N175','N177','N178','F001','F002','F003','N09001','N09002','N094','N09301','N09302','N070','N072','N073','N074','N075','N076','N077','N078','N079','N750A','N750B','N750C','N750E','N750F','N750H','N750G','C016','C017','C021','C024','C031','C032','C033') and a.adopid=c.adopid and a.aduserid not in (select cusid from admempinfo) and a.lastdate like ?  and ("
					+ connectSQL + ") order by a.aduserip,a.lastdate,a.lasttime";
			log.debug("CCM902:" + sql);
			Query query = getSession().createSQLQuery(sql);
			query.unwrap(NativeQuery.class).addEntity("a", OLD_TXNLOG.class);
			query.setParameter(0, lastDate);
			return query.getResultList();
		} else {
			sql = "select a.* from TXNLOG a,TXNUSER b,SYSOP c where a.aduserid=b.dpsuerid and a.ADEXCODE='' and a.adopid in ('N174','N175','N177','N178','F001','F002','F003','N09001','N09002','N094','N09301','N09302','N070','N072','N073','N074','N075','N076','N077','N078','N079','N750A','N750B','N750C','N750E','N750F','N750H','N750G','C016','C017','C021','C024','C031','C032','C033') and a.adopid=c.adopid and a.aduserid not in (select cusid from admempinfo) and a.lastdate like ?  and a.ADUSERIP like ?  order by a.aduserip,a.lastdate,a.lasttime";
			log.debug("CCM902:" + sql);
			Query query = getSession().createSQLQuery(sql);
			query.unwrap(NativeQuery.class).addEntity("a", OLD_TXNLOG.class);
			query.setParameter(0, lastDate);
			query.setParameter(1, BRH);
			return query.getResultList();
		}
	}

	// For NNBReport_M902_HO.java
	public List<OLD_TXNLOG> findByNNBReportM902_HO(String sDate, String eDate, String BRH) {
		// log.debug("CCM902_HO: sDate= " + sDate + " eDate= " + eDate + " BRH= " +
		// BRH);
		String sql = "";
		if (BRH.indexOf("|") >= 0) // 部室超過一個網段的處理方式
		{
			String[] names = BRH.split("|");
			String connectSQL = "";
			int counter = 0;
			for (String name : names) {
				if (counter == 0)
					connectSQL = connectSQL + "a.aduserip like '" + name + "'";
				else if (counter > 0)
					connectSQL = connectSQL + " or a.aduserip like '" + name + "'";
				counter++;
			}
			sql = "select a.* from TXNLOG a,TXNUSER b,SYSOP c where a.aduserid=b.dpsuerid and a.ADEXCODE='' and a.adopid in ('N174','N175','N177','N178','F001','F002','F003','N09001','N09002','N094','N09301','N09302','N070','N072','N073','N074','N075','N076','N077','N078','N079','N750A','N750B','N750C','N750E','N750F','N750H','N750G','C016','C017','C021','C024','C031','C032','C033') and a.adopid=c.adopid and a.aduserid not in (select cusid from admempinfo) and a.lastdate >= ? and a.lastdate <=? and ("
					+ connectSQL + ") order by a.aduserip,a.lastdate,a.lasttime";
			log.debug("CCM902_HO:" + sql);
			Query query = getSession().createSQLQuery(sql);
			query.unwrap(NativeQuery.class).addEntity("a", OLD_TXNLOG.class);
			query.setParameter(0, sDate);
			query.setParameter(1, eDate);
			return query.getResultList();
		} else {
			sql = "select a.* from TXNLOG a,TXNUSER b,SYSOP c where a.aduserid=b.dpsuerid and a.ADEXCODE='' and a.adopid in ('N174','N175','N177','N178','F001','F002','F003','N09001','N09002','N094','N09301','N09302','N070','N072','N073','N074','N075','N076','N077','N078','N079','N750A','N750B','N750C','N750E','N750F','N750H','N750G','C016','C017','C021','C024','C031','C032','C033') and a.adopid=c.adopid and a.aduserid not in (select cusid from admempinfo) and a.lastdate >=? and a.lastdate <=? and a.ADUSERIP like ?  order by a.aduserip,a.lastdate,a.lasttime";
			log.debug("CCM902_HO:" + sql);
			Query query = getSession().createSQLQuery(sql);
			query.unwrap(NativeQuery.class).addEntity("a", OLD_TXNLOG.class);
			query.setParameter(0, sDate);
			query.setParameter(1, eDate);
			query.setParameter(2, BRH);
			return query.getResultList();
		}
	}

	// For NNBReport_M903.java
	public List<OLD_TXNLOG> findByNNBReportM903(String lastDate, String BRH) {
		String sql = "";
		log.debug("CCM903:BRH = " + BRH);
		if (BRH.indexOf("|") >= 0) // 部室超過一個網段的處理方式
		{
			String[] names = BRH.split("|");
			String connectSQL = "";
			int counter = 0;
			for (String name : names) {
				if (counter == 0)
					connectSQL = connectSQL + "a.aduserip like '" + name + "'";
				else if (counter > 0)
					connectSQL = connectSQL + " or a.aduserip like '" + name + "'";
				counter++;
			}
			sql = "select a.* from TXNLOG a,TXNUSER b,SYSOP c where a.aduserid=b.dpsuerid and a.ADEXCODE='' and a.adopid in ('N174','N175','N177','N178','F001','F002','F003','N09001','N09002','N094','N09301','N09302','N070','N072','N073','N074','N075','N076','N077','N078','N079','N750A','N750B','N750C','N750E','N750F','N750H','N750G','C016','C017','C021','C024','C031','C032','C033') and a.adopid=c.adopid and a.aduserid not in (select cusid from ADMEMPINFO) and a.lastdate like ?  and ("
					+ connectSQL + ") order by a.aduserip,a.lastdate,a.lasttime";
			log.debug("CCM903:" + sql);
			Query query = getSession().createSQLQuery(sql);
			query.unwrap(NativeQuery.class).addEntity("a", OLD_TXNLOG.class);
			query.setParameter(0, lastDate);
			return query.getResultList();
		} else {

			sql = "select a.* from TXNLOG a,TXNUSER b,SYSOP c where a.aduserid=b.dpsuerid and a.ADEXCODE='' and a.adopid in ('N174','N175','N177','N178','F001','F002','F003','N09001','N09002','N094','N09301','N09302','N070','N072','N073','N074','N075','N076','N077','N078','N079','N750A','N750B','N750C','N750E','N750F','N750H','N750G','C016','C017','C021','C024','C031','C032','C033') and a.adopid=c.adopid and a.aduserid not in (select cusid from ADMEMPINFO) and a.lastdate like ?  and a.ADUSERIP like ?  order by a.aduserip,a.lastdate,a.lasttime";
			log.debug("CCM903:" + sql);
			Query query = getSession().createSQLQuery(sql);
			query.unwrap(NativeQuery.class).addEntity("a", OLD_TXNLOG.class);
			query.setParameter(0, lastDate);
			query.setParameter(1, BRH);
			return query.getResultList();
		}
	}

	// for TxnLogtoCrm.java
	// 抓取交易紀錄資料
	public List<OLD_TXNLOG> getTxnLogforcrm(String lastDate) {

		String sql = "select * from TXNLOG where lastdate = ?";

		Query query = getSession().createSQLQuery(sql);
		query.unwrap(NativeQuery.class).addEntity("OLD_TXNLOG", OLD_TXNLOG.class);
		query.setParameter(0, lastDate);

		return ESAPIUtil.validStrList(query.getResultList());
	}

	// for Bank3Report.java
	public long getAdopidCount(String Adopid, boolean isSuccess) {
		List<String> params = new ArrayList<String>();
		String sdate = DateTimeUtils.getPrevMonthFirstDay();
		// String sdate = "20150901";
		String stime = "000000";
		String edate = DateTimeUtils.getPrevMonthLastDay();
		// String edate = "20150930";
		String etime = "235959";
		params.add(Adopid);
		params.add(sdate);
		params.add(stime);
		params.add(edate);
		params.add(etime);

		String[] values = new String[params.size()];
		params.toArray(values);

		String SQL = "SELECT COUNT(*) FROM fstop.orm.old.OLD_TXNLOG WHERE ADOPID = ? AND LASTDATE >= ? AND LASTTIME >= ? AND LASTDATE <= ? AND LASTTIME <= ?";
		if (isSuccess) {
			SQL += " AND ADEXCODE = '' "; // ADEXCODE沒有值代表交易成功
		}
		return (Long) (find(SQL, values)).get(0);
	}

	/**
	 * 查詢行外電腦辦理基金申購(贖回)交易資料for稽核處
	 * 
	 * @author Simon
	 * @param startDate,endDate
	 * @return Rows
	 */
	@SuppressWarnings({ "unused", "unchecked" })
	public Rows findByFundIPReportforH11(String startDate, String endDate) {
		Rows result = new Rows();
		String sql = ""
				+ "select distinct b.aduserid,c.aduserip,'*' mark from (select count(*) cnt, aduserip from (select distinct aduserid,aduserip from TXNLOG where aduserip not like '10.%' and adopid in ('C016','C017','C021','C024') and aduserid <> 'BATCH' and adexcode = '' and lastdate >= ? and lastdate <= ?) a group by aduserip having count(*) > 4) c left join TXNLOG b on b.aduserip = c.aduserip where b.adopid in ('C016','C017','C021','C024') and b.aduserid <> 'BATCH' and b.adexcode = '' and b.lastdate >= ? and b.lastdate <= ? "
				+ "union all "
				+ "select distinct b.aduserid,c.aduserip,' ' mark from (select count(*) cnt, aduserip from (select distinct aduserid,aduserip from TXNLOG where aduserip not like '10.%' and adopid in ('C016','C017','C021','C024') and aduserid <> 'BATCH' and adexcode = '' and lastdate >= ? and lastdate <= ?) a group by aduserip having count(*) < 5) c left join TXNLOG b on b.aduserip = c.aduserip where b.adopid in ('C016','C017','C021','C024') and b.aduserid <> 'BATCH' and b.adexcode = '' and b.lastdate >= ? and b.lastdate <= ? ";

		Query query = getSession().createSQLQuery(sql);
		log.debug(ESAPIUtil.vaildLog("startDate:" + startDate + "；endDate:" + endDate));
		log.debug(ESAPIUtil.vaildLog("FundIP11_NB2.5:" + sql));
		query.setParameter(0, startDate);
		query.setParameter(1, endDate);
		query.setParameter(2, startDate);
		query.setParameter(3, endDate);
		query.setParameter(4, startDate);
		query.setParameter(5, endDate);
		query.setParameter(6, startDate);
		query.setParameter(7, endDate);

		// Stored XSS
		List<Object> qresult = query.getResultList();
		if (null == qresult) {
			qresult = new ArrayList();
		}
		qresult = ESAPIUtil.validList(qresult);
		// List qresult = query.getResultList();

		Iterator it = qresult.iterator();
		while (it.hasNext()) {
			Object[] o = (Object[]) it.next();
			Map rm = new HashMap();
			int t = -1;

			rm.put("aduserid", o[++t].toString());
			rm.put("aduserip", o[++t].toString());
			rm.put("mark", o[++t].toString());
			rm.put("SRC", "2.5");

			// log.debug("@@@ FundIPData11 report t =="+t);
			// log.debug("@@@ FundIPData11 report o.length =="+o.length);
			// log.debug("@@@ FundIPData11 report o-0 =="+o[0].toString());
			// log.debug("@@@ FundIPData11 report o-1 =="+o[1].toString());
			// log.debug("@@@ FundIPData11 report o-2 =="+o[2].toString());
			Row r = new Row(rm);
			result.addRow(r);
		}

		return result;
	}

	/**
	 * 查詢行外電腦辦理基金申購(贖回)交易資料
	 * 
	 * @author Simon
	 * @param userid,userip,startDate,endDate
	 * @return List
	 */
	@SuppressWarnings({ "unused", "unchecked" })
	public List<OLD_TXNLOG> findByFundIPDataReport(String userid, String userip, String startDate, String endDate) {

		String sql = "select * from TXNLOG where aduserid = ? and aduserip = ? and lastdate >= ? and lastdate <= ? and adopid in ('C016','C017','C021','C024') and aduserid <> 'BATCH' and adexcode = '' order by aduserip, lastdate, lasttime";

		// for testing
		// String sql = "select * from txnlog where aduserid = ? and aduserip = ? and
		// adopid in ('C016','C017','C021','C024') and aduserid <> 'BATCH' and adexcode
		// = '' order by aduserip, lastdate, lasttime";

		Query query = getSession().createSQLQuery(sql);
		query.unwrap(NativeQuery.class).addEntity("a", OLD_TXNLOG.class);
		// log.debug("startDate:"+startDate+"；endDate:"+endDate+"；userid:"+userid+"；userip:"+userip);
		log.debug("FundIPData:" + sql);
		query.setParameter(0, userid);
		query.setParameter(1, userip);
		query.setParameter(2, startDate);
		query.setParameter(3, endDate);

		return ESAPIUtil.validStrList(query.getResultList());
	}

	// for Bank3Report.java
	/**
	 * 查詢來電分期約款簽屬完成申請與完成取消明細 N814 and N8141
	 * 
	 * @return
	 */
	public Rows getN814_N8141_Detail() {
		Rows result = new Rows();
		String SQL = "SELECT VARCHAR(a.LASTDATE) AS LASTDATE, VARCHAR(a.LASTTIME) AS LASTTIME, b.DPUSERNAME, b.DPSUERID, "
				+ "CASE WHEN ADOPID = 'N814' THEN 'V' ELSE '' END AS JOINSUCCESS, "
				+ "CASE WHEN ADOPID = 'N8141' THEN 'V' ELSE '' END AS CANCELSUCCESS "
				+ "FROM fstop.orm.old.OLD_TXNLOG a , fstop.orm.old.OLD_TXNUSER b "
				+ "WHERE a.ADUSERID = b.DPSUERID AND " + "(ADOPID = 'N814' OR ADOPID = 'N8141') AND "
				+ "ADEXCODE = '' AND " + "a.LASTDATE >= ? AND a.LASTTIME >= ? AND a.LASTDATE <= ? AND a.LASTTIME <= ? "
				+ "ORDER BY a.LASTDATE ASC, a.LASTTIME ASC ";

		Query query = getSession().createSQLQuery(SQL);
		String sdate = DateTimeUtils.getPrevMonthFirstDay();
		// String sdate = "20150901";
		String stime = "000000";
		String edate = DateTimeUtils.getPrevMonthLastDay();
		// String edate = "20150930";
		String etime = "235959";
		query.setParameter(0, sdate);
		query.setParameter(1, stime);
		query.setParameter(2, edate);
		query.setParameter(3, etime);
		query.unwrap(NativeQuery.class).addEntity("a", OLD_TXNLOG.class);
		query.unwrap(NativeQuery.class).addEntity("b", OLD_TXNUSER.class);
		// Stored XSS
		List<Object> qresult = query.getResultList();
		if (null == qresult) {
			qresult = new ArrayList();
		}
		qresult = ESAPIUtil.validList(qresult);
		// List qresult = query.getResultList();

		Iterator it = qresult.iterator();
		while (it.hasNext()) {
			Object[] o = (Object[]) it.next();
			Map rm = new HashMap();
			int t = -1;

			rm.put("LASTDATE", o[++t].toString());
			rm.put("LASTTIME", o[++t].toString());
			rm.put("DPUSERNAME", o[++t].toString());
			rm.put("DPSUERID", o[++t].toString());
			rm.put("JOINSUCCESS", o[++t].toString());
			rm.put("CANCELSUCCESS", o[++t].toString());

			// log.debug("@@@ Data report t =="+t);
			// log.debug("@@@ Data report o.length =="+o.length);
			// log.debug("@@@ Data report o-0 =="+o[0].toString());
			// log.debug("@@@ Data report o-1 =="+o[1].toString());
			// log.debug("@@@ Data report o-2 =="+o[2].toString());
			// log.debug("@@@ Data report o-3 =="+o[3].toString());
			// log.debug("@@@ Data report o-4 =="+o[4].toString());
			// log.debug("@@@ Data report o-5 =="+o[5].toString());

			Row r = new Row(rm);
			result.addRow(r);
		}

		return result;
	}

	// for FundIPReport.java
	@SuppressWarnings({ "unused", "unchecked" })
	public Rows findByFundIPReportforH681(String startDate, String endDate) {
		Rows result = new Rows();
		String sql = "select distinct b.aduserid,c.aduserip from (select count(*) cnt, aduserip from (select distinct aduserid,aduserip from TXNLOG where aduserip not like '10.%' and adopid in ('C016','C017','C021','C024','C111','InvestAttr') and aduserid <> 'BATCH' and adexcode = '' and lastdate >= ? and lastdate <= ?) a group by aduserip having count(*) > 1) c left join TXNLOG b on b.aduserip = c.aduserip where b.adopid in ('C016','C017','C021','C024','C111','InvestAttr') and b.aduserid <> 'BATCH' and b.adexcode = '' and b.lastdate >= ? and b.lastdate <= ?";

		Query query = getSession().createSQLQuery(sql);

		log.debug(ESAPIUtil.vaildLog("startDate:" + startDate + "；endDate:" + endDate));
		log.debug(ESAPIUtil.vaildLog("FundIP:" + sql));
		query.setParameter(0, startDate);
		query.setParameter(1, endDate);
		query.setParameter(2, startDate);
		query.setParameter(3, endDate);

		// Stored XSS
		List<Object> qresult = query.getResultList();
		if (null == qresult) {
			qresult = new ArrayList();
		}
		qresult = ESAPIUtil.validList(qresult);
		// List qresult = query.getResultList();

		Iterator it = qresult.iterator();
		while (it.hasNext()) {
			Object[] o = (Object[]) it.next();
			Map rm = new HashMap();
			int t = -1;

			rm.put("aduserid", o[++t].toString());
			rm.put("aduserip", o[++t].toString());
			rm.put("SRC", "2.5");

			Row r = new Row(rm);
			result.addRow(r);
		}

		return result;
	}
	
	@SuppressWarnings({ "unused", "unchecked" })
	public List<OLD_TXNLOG> findByFundIPDataReport1(String userid, String userip, String startDate, String endDate) {

		String sql = "select * from txnlog where aduserid = ? and aduserip = ? and lastdate >= ? and lastdate <= ? and adopid in ('C016','C017','C021','C024','C111','InvestAttr') and aduserid <> 'BATCH' and adexcode = '' and aduserip <> '' order by aduserip, lastdate, lasttime";
		// for testing
		// String sql = "select * from txnlog where aduserid = ? and aduserip = ? and
		// adopid in ('C016','C017','C021','C024') and aduserid <> 'BATCH' and adexcode
		// = '' order by aduserip, lastdate, lasttime";

		Query query = getSession().createSQLQuery(sql);
		query.unwrap(NativeQuery.class).addEntity("a", OLD_TXNLOG.class);
		// log.debug("startDate:"+startDate+"；endDate:"+endDate+"；userid:"+userid+"；userip:"+userip);
		log.debug("FundIPData1:" + sql);
		query.setParameter(0, userid);
		query.setParameter(1, userip);
		query.setParameter(2, startDate);
		query.setParameter(3, endDate);

		return ESAPIUtil.validStrList(query.getResultList());
	}
	
	
	// For NNBReport_M911.java
	public MVH getTxnLogNum(String stDate, String edDate) {

		log.debug("stDate:=====>" + stDate + "  edDate:" + edDate + "<====");
		MVHImpl mvhresult = null;

		Rows result = new Rows();
//		StringBuffer sql = new StringBuffer();
//		sql.append(
//				" SELECT tur.ADBRANCHID AS ADBRANCHID,tur.DPUSERNAME AS DPUSERNAME,tl.ADUSERID AS ADUSERID,tl.ADTXACNO AS ADTXACNO,tl.LASTDATE AS LASTDATE,left(tl.LASTTIME,4) AS LTIME,tl.ADOPID AS ADOPID,tl.LOGINTYPE AS LOGINTYPE,COUNT(*) AS TIME ");
//		sql.append(" FROM TXNLOG tl,TXNUSER tur ");
//		// sql.append(" WHERE tl.ADUSERID=tur.DPSUERID AND tl.LASTDATE >=? AND
//		// tl.LASTDATE <=? AND tl.ADOPID !='' AND ");
//		sql.append(" WHERE tl.ADUSERID=tur.DPSUERID AND tl.LASTDATE >=? AND tl.LASTDATE <=? AND ");
//		sql.append(
//				" tl.ADOPID NOT IN(SELECT ADOPID FROM SYSOPGROUP WHERE ADOPGROUP ='QUERYGROUP' ORDER BY ADOPID ASC) ");
//		sql.append(
//				" GROUP BY tur.ADBRANCHID,tur.DPUSERNAME,tl.ADUSERID,tl.ADTXACNO,tl.LASTDATE,left(tl.LASTTIME,4),tl.ADOPID,tl.LOGINTYPE ");
//		sql.append(" HAVING COUNT(*) >= 4 ");
//		sql.append(" ORDER BY tur.ADBRANCHID,tl.LASTDATE,LTIME ASC ");
		String sql = "SELECT tur.ADBRANCHID AS ADBRANCHID,tur.DPUSERNAME AS DPUSERNAME,tl.ADUSERID AS ADUSERID,tl.ADTXACNO AS ADTXACNO,tl.LASTDATE AS LASTDATE,left(tl.LASTTIME,4) AS LTIME,tl.ADOPID AS ADOPID,tl.LOGINTYPE AS LOGINTYPE,COUNT(*) AS TIME FROM TXNLOG tl,TXNUSER tur WHERE tl.ADUSERID=tur.DPSUERID  AND tl.LASTDATE >=? AND tl.LASTDATE <=?  AND tl.ADREQTYPE<>'B' AND tl.ADOPID<>'' AND tl.ADOPID NOT IN(SELECT ADOPID FROM SYSOPGROUP WHERE ADOPGROUP ='QUERYGROUP' ORDER BY ADOPID ASC) GROUP BY tur.ADBRANCHID,tur.DPUSERNAME,tl.ADUSERID,tl.ADTXACNO,tl.LASTDATE,left(tl.LASTTIME,4),tl.ADOPID,tl.LOGINTYPE HAVING COUNT(*) >= 4 ORDER BY tur.ADBRANCHID,tl.LASTDATE,LTIME ASC";	

		try {
			Query query = getSession().createSQLQuery(sql.toString());
			query.setParameter(0, stDate);// 起日
			query.setParameter(1, edDate);// 迄日
			// query.setParameter(2, branchNo);// 分行別

			query.unwrap(NativeQuery.class).addScalar("ADBRANCHID", StringType.INSTANCE);
			query.unwrap(NativeQuery.class).addScalar("DPUSERNAME", StringType.INSTANCE);
			query.unwrap(NativeQuery.class).addScalar("ADUSERID", StringType.INSTANCE);
			query.unwrap(NativeQuery.class).addScalar("ADTXACNO", StringType.INSTANCE);
			query.unwrap(NativeQuery.class).addScalar("LASTDATE", StringType.INSTANCE);
			query.unwrap(NativeQuery.class).addScalar("LTIME", StringType.INSTANCE);
			query.unwrap(NativeQuery.class).addScalar("ADOPID", StringType.INSTANCE);
			query.unwrap(NativeQuery.class).addScalar("LOGINTYPE", StringType.INSTANCE);
			query.unwrap(NativeQuery.class).addScalar("TIME", StringType.INSTANCE);

			List qresult = ESAPIUtil.validStrList(query.getResultList());
			Iterator it = qresult.iterator();
			while (it.hasNext()) {
				Object[] o = (Object[]) it.next();
				Map rm = new HashMap();
				int t = -1;
				rm.put("ADBRANCHID", o[++t].toString());
				rm.put("DPUSERNAME", (o[++t] == null ? "" : o[t].toString()));
				rm.put("ADUSERID", (o[++t] == null ? "" : o[t].toString()));
				rm.put("ADTXACNO", (o[++t] == null ? "" : o[t].toString()));
				rm.put("LASTDATE", (o[++t] == null ? "" : o[t].toString()));
				rm.put("LTIME", (o[++t] == null ? "" : o[t].toString()));
				rm.put("ADOPID", (o[++t] == null ? "" : o[t].toString()));
				rm.put("LOGINTYPE", (o[++t] == null ? "" : o[t].toString()));
				rm.put("TIME", (o[++t] == null ? "" : o[t].toString()));
				Row r = new Row(rm);
				result.addRow(r);
			}

			Rows newrows = new Rows();
			for (int i = 0; i < result.getSize(); i++) {
				newrows.addRow(result.getRow(i));
			}
			mvhresult = new MVHImpl(newrows);
			return mvhresult;
		} catch (Exception e) {
			throw (TopMessageException.create("ZX99"));
		}
	}
	
	/*
	 * Add 20201216 報表 CNM416 資料
	 *  同一申請人(ID)  不同IP交易資料
	*/
	@SuppressWarnings( { "unused", "unchecked" })
	public List<OLD_TXNLOG> getSameIDAnotherIPFundReport(String startDate ,String lastDate) {
		log.info("====getSameIpAnotherApplicant method====");
		
		StringBuffer nativeSql = new StringBuffer();
		
		nativeSql.append(" SELECT * FROM TXNLOG WHERE ADUSERID IN ");
		nativeSql.append("   (SELECT ADUSERID FROM TXNLOG WHERE ADUSERID IN ");
		nativeSql.append("        (SELECT ADUSERID FROM TXNLOG WHERE 1=1 ");
		nativeSql.append("           AND adopid in ('C016','C017','C021','C024','C111','InvestAttr') ");
		nativeSql.append("           AND aduserip not like '10.%' ");
		nativeSql.append("           AND aduserid <> 'BATCH' AND adexcode = '' AND LASTDATE >= ? AND LASTDATE <= ? ) ");
		nativeSql.append("      AND adopid in ('C016','C017','C021','C024','C111','InvestAttr') ");
		nativeSql.append("      AND aduserip not like '10.%' ");
		nativeSql.append("      AND aduserid <> 'BATCH' AND adexcode = '' AND LASTDATE >= ? AND LASTDATE <= ? ");
		nativeSql.append("    GROUP BY ADUSERID ");
		nativeSql.append("    HAVING COUNT(ADUSERID) > 1) ");
		nativeSql.append(" AND adopid in ('C016','C017','C021','C024','C111','InvestAttr') ");
		nativeSql.append(" AND aduserip not like '10.%' ");
		nativeSql.append(" AND aduserid <> 'BATCH' AND adexcode = '' AND LASTDATE >= ? AND LASTDATE <= ? ");
		nativeSql.append(" ORDER BY LASTDATE DESC,LASTTIME ");
		
		
		log.info("====SQL string====" + nativeSql.toString());
		
		//FIXME For Test 測試完後移除 
//		startDate = "20200101";
//		lastDate = "20210101";
		
		try {
			Query query = getSession().createSQLQuery(nativeSql.toString()); //不支援複雜subquery
			
			query.setParameter(0, startDate);
			query.setParameter(1, lastDate);
			query.setParameter(2, startDate);
			query.setParameter(3, lastDate);
			query.setParameter(4, startDate);
			query.setParameter(5, lastDate);
			query.unwrap(NativeQuery.class).addEntity("TXNLOG", OLD_TXNLOG.class);
			
			return ESAPIUtil.validStrList(query.getResultList());
			
		}
		catch(Exception e)
		{	
			log.error(ESAPIUtil.vaildLog("getSameIDAnotherIPFundReport error : " + e.getMessage()));
			List<OLD_TXNLOG> rtnList = new ArrayList<OLD_TXNLOG>();
			return rtnList ;
		}
	}

}