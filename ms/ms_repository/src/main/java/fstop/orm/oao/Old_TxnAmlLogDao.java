package fstop.orm.oao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.query.NativeQuery;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.NNBJpaRepository;

import fstop.orm.old.OLD_TXNAMLLOG;
import fstop.orm.po.TXNAMLLOG;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Transactional(value = "nnbtransactionManager")
public class Old_TxnAmlLogDao extends NNBJpaRepository<OLD_TXNAMLLOG, Long> {

	// for AmlLog.java
	/**
	 * 查詢昨日資料
	 * 
	 * @param date
	 * @return
	 */
	public List<OLD_TXNAMLLOG> getAmlLog(String date1) {
		return find("FROM fstop.orm.old.OLD_TXNAMLLOG WHERE TS1 LIKE ? ORDER BY LOGID", date1);
	}

	/**
	 * 查詢特定日期範圍date1~date2
	 * 
	 * @param date
	 * @return
	 */
	public List<OLD_TXNAMLLOG> findByDateRange(String date1, String date2) {
		 return find("FROM fstop.orm.old.OLD_TXNAMLLOG WHERE TS1 >= ? and TS1 <= ? ORDER BY LOGID",date1, date2);

//		Query query = getSession().createSQLQuery("select * FROM fstop.orm.old.OLD_TXNAMLLOG WHERE TS1 >= ? and TS1 <= ? ORDER BY LOGID");
//		query.unwrap(NativeQuery.class).addEntity("TXNAMLLOG", OLD_TXNAMLLOG.class);
//		query.setParameter(0, date1);
//		query.setParameter(1, date2);
//		return query.getResultList();
	}

}
