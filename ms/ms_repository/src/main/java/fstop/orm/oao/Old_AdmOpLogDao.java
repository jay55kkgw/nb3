package fstop.orm.oao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.query.NativeQuery;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.NNBJpaRepository;
import com.netbank.util.ESAPIUtil;

import fstop.orm.old.OLD_ADMEMPINFO;
import fstop.orm.old.OLD_ADMOPLOG;
import fstop.orm.po.ADMOPLOG;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Transactional(value = "nnbtransactionManager")
public class Old_AdmOpLogDao extends NNBJpaRepository<OLD_ADMOPLOG, String> {

	// for UserActionRecord.java
	public List<OLD_ADMOPLOG> getAdmOpLogList(String date) {
//		Query query = getSession().createSQLQuery("select * FROM fstop.orm.old.OLD_ADMOPLOG where ADTXDATE = ?");
		Query query = getQuery("FROM fstop.orm.old.OLD_ADMOPLOG where ADTXDATE = ?", date);
//		query.unwrap(NativeQuery.class).addEntity("AdmOpLog", OLD_ADMOPLOG.class);
		return query.getResultList();
	}
}
