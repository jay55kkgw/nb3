package fstop.orm.oao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.NNBJpaRepository;

import fstop.orm.old.OLD_REVOLVINGCREDITAPPLY;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Transactional(value = "nnbtransactionManager")
public class Old_RevolvingCreditApplyDao extends NNBJpaRepository<OLD_REVOLVINGCREDITAPPLY, Long> {

	// for Bank3Report.java
	public long getTxnCount() {
		List<String> params = new ArrayList();
		String sdate = DateTimeUtils.getPrevMonthFirstDay();

		String stime = "000000";
		String edate = DateTimeUtils.getPrevMonthLastDay();

		String etime = "235959";
		params.add(sdate);
		params.add(stime);
		params.add(edate);
		params.add(etime);

		String[] values = new String[params.size()];
		params.toArray(values);

		String SQL = "SELECT COUNT(*) FROM fstop.orm.old.OLD_REVOLVINGCREDITAPPLY WHERE APPDATE >= ? AND APPTIME >= ? AND APPDATE <= ? AND APPTIME <= ?";

		return ((Long) find(SQL, values).get(0)).longValue();
	}

	// For CreditReport_904.java , CreditReport_905.java
	public List<OLD_REVOLVINGCREDITAPPLY> findByDate(String date) {
		return find("FROM fstop.orm.old.OLD_REVOLVINGCREDITAPPLY WHERE APPDATE = ? ORDER BY RCVNO", date);
	}

}
