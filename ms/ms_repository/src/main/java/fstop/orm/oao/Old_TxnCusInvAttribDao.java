package fstop.orm.oao;

import java.util.List;
import java.util.function.BiFunction;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.NNBJpaRepository;
import com.netbank.domain.orm.core.Page;
import com.netbank.domain.orm.core.SessionAdapter;
import com.netbank.util.ESAPIUtil;

import fstop.orm.old.OLD_TXNCUSINVATTRIB;
import fstop.orm.po.TXNCUSINVATTRIB;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Transactional(
		transactionManager="nnbtransactionManager",
        rollbackFor = {Throwable.class}
)
public class Old_TxnCusInvAttribDao extends NNBJpaRepository<OLD_TXNCUSINVATTRIB, String> {
	
	

	@Override
	public EntityManager getEntityManager() {
		// TODO Auto-generated method stub
		return super.getEntityManager();
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return super.count();
	}

	@Override
	public List<OLD_TXNCUSINVATTRIB> findAll() {
		// TODO Auto-generated method stub
		return super.findAll();
	}

	@Override
	public OLD_TXNCUSINVATTRIB findById(String id) {
		// TODO Auto-generated method stub
		return super.findById(id);
	}

	@Override
	public void save(OLD_TXNCUSINVATTRIB entity) {
		// TODO Auto-generated method stub
		super.save(entity);
	}

	@Override
	public OLD_TXNCUSINVATTRIB update(OLD_TXNCUSINVATTRIB entity) {
		// TODO Auto-generated method stub
		return super.update(entity);
	}

	@Override
	public OLD_TXNCUSINVATTRIB updateI(String id) {
		// TODO Auto-generated method stub
		return super.updateI(id);
	}

	@Override
	public OLD_TXNCUSINVATTRIB updateII(OLD_TXNCUSINVATTRIB entity) {
		// TODO Auto-generated method stub
		return super.updateII(entity);
	}

	@Override
	public void removeById(String id) {
		// TODO Auto-generated method stub
		super.removeById(id);
	}

	@Override
	public void delete(OLD_TXNCUSINVATTRIB entity) {
		// TODO Auto-generated method stub
		super.delete(entity);
	}

	@Override
	public Query getQuery(String hql, Object... values) {
		// TODO Auto-generated method stub
		return super.getQuery(hql, values);
	}

	@Override
	public Query getCriteriaQuery(Class<OLD_TXNCUSINVATTRIB> entityClass,
			BiFunction<Root, CriteriaBuilder, Predicate>... criteriaFunction) {
		// TODO Auto-generated method stub
		return super.getCriteriaQuery(entityClass, criteriaFunction);
	}

	@Override
	public List find(String hql, Object... values) {
		// TODO Auto-generated method stub
		return super.find(hql, values);
	}

	@Override
	public List<OLD_TXNCUSINVATTRIB> findBy(Class<OLD_TXNCUSINVATTRIB> entityClass, String name, Object value) {
		// TODO Auto-generated method stub
		return super.findBy(entityClass, name, value);
	}

	@Override
	public OLD_TXNCUSINVATTRIB findUniqueBy(Class<OLD_TXNCUSINVATTRIB> entityClass, String name, Object value) {
		// TODO Auto-generated method stub
		return super.findUniqueBy(entityClass, name, value);
	}

	@Override
	public List<OLD_TXNCUSINVATTRIB> findByLike(Class<OLD_TXNCUSINVATTRIB> entityClass, String name, String value) {
		// TODO Auto-generated method stub
		return super.findByLike(entityClass, name, value);
	}

	@Override
	public SessionAdapter getSession() {
		// TODO Auto-generated method stub
		return super.getSession();
	}

	@Override
	public Page pagedQuery(String hql, int pageNo, int pageSize, Object... values) {
		// TODO Auto-generated method stub
		return super.pagedQuery(hql, pageNo, pageSize, values);
	}

	@SuppressWarnings( { "unused", "unchecked" })
	public List<OLD_TXNCUSINVATTRIB> findByLastDate(String lastDate) {
		
		String sql = "from OLD_TXNCUSINVATTRIB where LASTDATE = ? order by LASTUSER ";
		
		Query query = getQuery(sql, lastDate);
		
		return ESAPIUtil.validStrList(query.getResultList());
	}	

	@SuppressWarnings( { "unused", "unchecked" })
	public List<String> findByLastDateRange(String startDate, String endDate) {
		
		String sql = "SELECT distinct LASTDATE from OLD_TXNCUSINVATTRIB where LASTDATE >= ? and LASTDATE <= ? order by LASTDATE ";

//		SQLQuery query = getSession().createSQLQuery(sql);
//
//		query.setParameter(0, startDate);
//		query.setParameter(1, endDate);

		Query query = getQuery(sql, startDate, endDate);			
		
		return ESAPIUtil.validStrList(query.getResultList());
	}	
	
	
	public void saveOrUpdate(OLD_TXNCUSINVATTRIB data) {
		try {
			if(findById(data.getFDUSERID()) != null) {
				update(data);
			}else {
				save(data);
			}
		}catch(Exception e){
			log.warn(e.getMessage());
		}
	}
}