package fstop.orm.oao;

import java.util.List;
import java.util.function.BiFunction;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.domain.orm.core.NNBJpaRepository;
import com.netbank.domain.orm.core.Page;
import com.netbank.domain.orm.core.SessionAdapter;

import fstop.orm.old.OLD_TXNADDRESSBOOK;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Transactional(
		transactionManager="nnbtransactionManager",
        rollbackFor = {Throwable.class}
)
public class Old_TxnAddressBookDao extends NNBJpaRepository<OLD_TXNADDRESSBOOK, Integer> {
	
	
	public List<OLD_TXNADDRESSBOOK> findByDPUserID(String DPUSERID) {
		List<OLD_TXNADDRESSBOOK> result = find("FROM OLD_TXNADDRESSBOOK WHERE DPUSERID = ?", DPUSERID);
		
		return result;
		
	}
	public List<OLD_TXNADDRESSBOOK> findByInput(String DPUSERID ,String DPGONAME,String DPABMAIL) {
		List<OLD_TXNADDRESSBOOK> result = find("FROM fstop.orm.old.OLD_TXNADDRESSBOOK WHERE DPUSERID = ? AND DPGONAME = ? AND DPABMAIL = ? ", DPUSERID ,DPGONAME , DPABMAIL);
		
		return result;
		
	}

	@Override
	public EntityManager getEntityManager() {
		// TODO Auto-generated method stub
		return super.getEntityManager();
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return super.count();
	}

	@Override
	public List<OLD_TXNADDRESSBOOK> findAll() {
		// TODO Auto-generated method stub
		return super.findAll();
	}

	@Override
	public OLD_TXNADDRESSBOOK findById(Integer id) {
		// TODO Auto-generated method stub
		return super.findById(id);
	}

	@Override
	public void save(OLD_TXNADDRESSBOOK entity) {
		// TODO Auto-generated method stub
		super.save(entity);
	}

	@Override
	public OLD_TXNADDRESSBOOK update(OLD_TXNADDRESSBOOK entity) {
		// TODO Auto-generated method stub
		return super.update(entity);
	}

	@Override
	public void delete(OLD_TXNADDRESSBOOK entity) {
		// TODO Auto-generated method stub
		super.delete(entity);
	}

	@Override
	public Query getQuery(String hql, Object... values) {
		// TODO Auto-generated method stub
		return super.getQuery(hql, values);
	}

	@Override
	public Query getCriteriaQuery(Class<OLD_TXNADDRESSBOOK> entityClass,
			BiFunction<Root, CriteriaBuilder, Predicate>... criteriaFunction) {
		// TODO Auto-generated method stub
		return super.getCriteriaQuery(entityClass, criteriaFunction);
	}

	@Override
	public List find(String hql, Object... values) {
		// TODO Auto-generated method stub
		return super.find(hql, values);
	}

	@Override
	public List<OLD_TXNADDRESSBOOK> findBy(Class<OLD_TXNADDRESSBOOK> entityClass, String name, Object value) {
		// TODO Auto-generated method stub
		return super.findBy(entityClass, name, value);
	}

	@Override
	public OLD_TXNADDRESSBOOK findUniqueBy(Class<OLD_TXNADDRESSBOOK> entityClass, String name, Object value) {
		// TODO Auto-generated method stub
		return super.findUniqueBy(entityClass, name, value);
	}

	@Override
	public List<OLD_TXNADDRESSBOOK> findByLike(Class<OLD_TXNADDRESSBOOK> entityClass, String name, String value) {
		// TODO Auto-generated method stub
		return super.findByLike(entityClass, name, value);
	}

	@Override
	public SessionAdapter getSession() {
		// TODO Auto-generated method stub
		return super.getSession();
	}

	@Override
	public Page pagedQuery(String hql, int pageNo, int pageSize, Object... values) {
		// TODO Auto-generated method stub
		return super.pagedQuery(hql, pageNo, pageSize, values);
	}


	
	
}
