package fstop.orm.po;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

import lombok.Data;

@Entity
@Table(name = "ADMCURRENCY")
@Data
public class ADMCURRENCY implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1355452936209405374L;

	@Id
	private String ADCURRENCY;

	private String ADCCYNO = "";

	private String ADCCYNAME = "";
	
	private String BUY01 = "";
	
	private String SEL01 = "";
	
	private String BOOKRATE = "";
	
	private String MSGCODE = "";

	private String LASTUSER = "";

	private String LASTDATE = "";

	private String LASTTIME = "";
	
	private String ADCCYENGNAME = "";
	
	private String ADCCYCHSNAME = "";
	
}
