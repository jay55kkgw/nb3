package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

import lombok.Data;

@Entity
@Table(name = "TXNFUNDDATA")
@Data
public class TXNFUNDDATA implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9045181281599371846L;

	@Id
	private String TRANSCODE;

	private String TRANSCRY;

	private String FUNDLNAME;

	private String FUNDSNAME;

	private String COUNTRYTYPE;

	private String FUNDMARK;

	private String RISK;

	private String PERIODVAR;
	
	private String FUS98E;
	
	private String FUSMON;
	
	private String CBAMT;
	
	private String YBAMT;
	
	private String CBTAMT;
	
	private String YBTAMT;
	
	private String FUNDT;
	
	private String C30AMT;
	
	private String C30RAMT;
	
	private String Y30AMT;
	
	private String Y30RAMT;
	
	private String C302AMT;
	
	private String C302RAMT;
	
	private String Y302AMT;
	
	private String Y302RAMT;

	@Override
	public String toString() {
		return "TXNFUNDDATA [TRANSCODE=" + TRANSCODE + ", TRANSCRY=" + TRANSCRY + ", FUNDLNAME=" + FUNDLNAME
				+ ", FUNDSNAME=" + FUNDSNAME + ", COUNTRYTYPE=" + COUNTRYTYPE + ", FUNDMARK=" + FUNDMARK
				+ ", RISK=" + RISK + ", PERIODVAR=" + PERIODVAR + ", FUS98E=" + FUS98E + ", FUSMON=" + FUSMON
				+ ", CBAMT=" + CBAMT + ", YBAMT=" + YBAMT + ", CBTAMT=" + CBTAMT + ", YBTAMT=" + YBTAMT
				+ ", FUNDT=" + FUNDT + ", C30AMT=" + C30AMT + ", C30RAMT=" + C30RAMT + ", Y30AMT=" + Y30AMT
				+ ", Y30RAMT=" + Y30RAMT + ", C302AMT=" + C302AMT + ", C302RAMT=" + C302RAMT 
				+ ", Y302AMT=" + Y302AMT + ", Y302RAMT=" + Y302RAMT + "]";
	} 
}
