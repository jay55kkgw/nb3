package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

import lombok.Data;

@Entity
@Table(name = "MB3SYSOP")
@Data
public class MB3SYSOP implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5276584167052329132L;
	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String DPACCSETID;

	private String ADOPID;

	private String ADOPNAME;

	private String ADOPENGNAME;

	private String ADOPCHSNAME;
	
	private String ADOPMEMO;

	private String ADOPALIVE;
	
	private String ADOPAUTHTYPE;

	private String ADSEQ;
	
	private String ADOPGROUPID;
	
	private String ISANONYMOUS;
	
	private String URL;
	
	private String ISPOPUP;
	
	private String ISBLOCKAML;     
	
	private String ADGPPARENT;
	
	private String ADOPGROUP;
	
	private String LASTUSER;

	private String LASTDATE;

	private String LASTTIME;
	
}
