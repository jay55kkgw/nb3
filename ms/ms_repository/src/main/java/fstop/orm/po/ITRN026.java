package fstop.orm.po;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

import lombok.Data;

@Entity
@Table(name = "ITRN026")
@Data
public class ITRN026 implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8831776594750332299L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String RECNO;

	private String HEADER;

	private String SEQ;

	private String DATE;

	private String TIME;

	private String COUNT;
	
	private String COLOR;
	
	private String TERM;
	
	private String ITR1;
	
	private String ITR2;
	
	private String ITR3;
	
	private String ITR4;
	
	private String ITR5;
	
	private String ITR6;
	
	private String FILL;

	@Override
	public String toString() {
		return "ITRN026 [RECNO=" + RECNO + ", HEADER=" + HEADER + ", SEQ=" + SEQ + ", DATE=" + DATE + ", TIME=" + TIME
				+ ", COUNT=" + COUNT + ", COLOR=" + COLOR + ", TERM=" + TERM + ", ITR1=" + ITR1 + ", ITR2=" + ITR2
				+ ", ITR3=" + ITR3 + ", ITR4=" + ITR4 + ", ITR5=" + ITR5 + ", ITR6=" + ITR6 + ", FILL=" + FILL + "]";
	}
	
}
