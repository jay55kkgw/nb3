package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "TXNPHONETOKEN")
public class TXNPHONETOKEN implements Serializable {
	
	//一定要有 @Id，否則啟動伺服器時會發生錯誤
	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String DPUSERID;

	private String PHONETOKEN;

	private String PHONETYPE;
	
	private String NOTIFYAD;
	
	private String NOTIFYTRANS;

	private String LASTDATE;

	private String LASTTIME;

//	public String toString() {
//		return new ToStringBuilder(this).append("phonetoken", getPHONETOKEN())
//				.toString();
//	}

	public String getDPUSERID() {
		return DPUSERID;
	}

	public void setDPUSERID(String dpuserid) {
		DPUSERID = dpuserid;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

	public String getPHONETOKEN() {
		return PHONETOKEN;
	}

	public void setPHONETOKEN(String phonetoken) {
		PHONETOKEN = phonetoken;
	}

	public String getPHONETYPE() {
		return PHONETYPE;
	}

	public void setPHONETYPE(String phonetype) {
		PHONETYPE = phonetype;
	}

	public String getNOTIFYAD() {
		return NOTIFYAD;
	}

	public void setNOTIFYAD(String notifyad) {
		NOTIFYAD = notifyad;
	}

	public String getNOTIFYTRANS() {
		return NOTIFYTRANS;
	}

	public void setNOTIFYTRANS(String notifytrans) {
		NOTIFYTRANS = notifytrans;
	}
	
}
