package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "TXNFUNDAPPLY")
public class TXNFUNDAPPLY implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1014197847472213956L;

	@Id
	private String USERID;

	private String ACN_SSV = "";

	private String ACN_FUD  = "";

	private String BILLMTH  = "";

	private String KIND  = "";

	private String MSGCODE  = "";

	private String IP  = "";

	private String LASTDATE  = "";

	private String LASTTIME  = "";

	public String getUSERID() {
		return USERID;
	}

	public void setUSERID(String uSERID) {
		USERID = uSERID;
	}

	public String getACN_SSV() {
		return ACN_SSV;
	}

	public void setACN_SSV(String aCN_SSV) {
		ACN_SSV = aCN_SSV;
	}

	public String getACN_FUD() {
		return ACN_FUD;
	}

	public void setACN_FUD(String aCN_FUD) {
		ACN_FUD = aCN_FUD;
	}

	public String getBILLMTH() {
		return BILLMTH;
	}

	public void setBILLMTH(String bILLMTH) {
		BILLMTH = bILLMTH;
	}

	public String getKIND() {
		return KIND;
	}

	public void setKIND(String kIND) {
		KIND = kIND;
	}

	public String getMSGCODE() {
		return MSGCODE;
	}

	public void setMSGCODE(String mSGCODE) {
		MSGCODE = mSGCODE;
	}

	public String getIP() {
		return IP;
	}

	public void setIP(String iP) {
		IP = iP;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lASTDATE) {
		LASTDATE = lASTDATE;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}
}
