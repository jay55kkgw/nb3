package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "VERIFYMAIL_HIS")
public class VERIFYMAIL_HIS implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8431696039661881677L;

	@Id
	private String HISID ;
	private String CUSIDN ;
	private String TXDATE ;
	private String EXPIREDATE ;
	private String NMAIL ;
	private String OMAIL ;
	private String VERIFYCODE ;
	private String URL ;
	private String ENC_URL ;
	private String STATUS ;
	private String LASTDATE ;
	private String LASTTIME ;
	private String CHANNEL ;
	private String BRHCOD ;
	private String TLRNUM ;
	private String TRNNUM ;
	
	public String getHISID() {
		return HISID;
	}
	public void setHISID(String hISID) {
		HISID = hISID;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getTXDATE() {
		return TXDATE;
	}
	public void setTXDATE(String tXDATE) {
		TXDATE = tXDATE;
	}
	public String getEXPIREDATE() {
		return EXPIREDATE;
	}
	public void setEXPIREDATE(String eXPIREDATE) {
		EXPIREDATE = eXPIREDATE;
	}
	public String getNMAIL() {
		return NMAIL;
	}
	public void setNMAIL(String nMAIL) {
		NMAIL = nMAIL;
	}
	public String getOMAIL() {
		return OMAIL;
	}
	public void setOMAIL(String oMAIL) {
		OMAIL = oMAIL;
	}
	public String getVERIFYCODE() {
		return VERIFYCODE;
	}
	public void setVERIFYCODE(String vERIFYCODE) {
		VERIFYCODE = vERIFYCODE;
	}
	public String getURL() {
		return URL;
	}
	public void setURL(String uRL) {
		URL = uRL;
	}
	public String getENC_URL() {
		return ENC_URL;
	}
	public void setENC_URL(String eNC_URL) {
		ENC_URL = eNC_URL;
	}
	public String getSTATUS() {
		return STATUS;
	}
	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}
	public String getLASTDATE() {
		return LASTDATE;
	}
	public void setLASTDATE(String lASTDATE) {
		LASTDATE = lASTDATE;
	}
	public String getLASTTIME() {
		return LASTTIME;
	}
	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}
	public String getCHANNEL() {
		return CHANNEL;
	}
	public void setCHANNEL(String cHANNEL) {
		CHANNEL = cHANNEL;
	}
	public String getBRHCOD() {
		return BRHCOD;
	}
	public void setBRHCOD(String bRHCOD) {
		BRHCOD = bRHCOD;
	}
	public String getTLRNUM() {
		return TLRNUM;
	}
	public void setTLRNUM(String tLRNUM) {
		TLRNUM = tLRNUM;
	}
	public String getTRNNUM() {
		return TRNNUM;
	}
	public void setTRNNUM(String tRNNUM) {
		TRNNUM = tRNNUM;
	}
	
}
