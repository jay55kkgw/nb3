package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "ADMMBSTATUS")
public class ADMMBSTATUS implements Serializable{

	private String ADMBSTATUSID;	//行動應用系統狀態檔ID	
	
	private String ADMBSTATUS;	//行動銀行應用系統狀態	
	
	private String LASTUSER;	//最後修改者	
	
	private String LASTDATE;	//最後修改日	
	
	private String LASTTIME;	//最後修改時間	
    @Id
	public String getADMBSTATUSID() {
		return ADMBSTATUSID;
	}

	public void setADMBSTATUSID(String admbstatusid) {
		ADMBSTATUSID = admbstatusid;
	}

	public String getADMBSTATUS() {
		return ADMBSTATUS;
	}

	public void setADMBSTATUS(String admbstatus) {
		ADMBSTATUS = admbstatus;
	}

	public String getLASTUSER() {
		return LASTUSER;
	}

	public void setLASTUSER(String lastuser) {
		LASTUSER = lastuser;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

}
