package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
//優惠特店檔
@Entity
@Table(name = "ADMSTORE")
public class ADMSTORE implements Serializable {

	private Integer STID;	//特店ID	
	
	private String USERID ="";	//使用者代號	
	
	private String USERNAME ="";	//使用者名稱	
	
	private String TXDATE ="";	//交易日期	
	
	private String TXTIME ="";	//交易時間	
	
	private String SDATE ="";	//特店上版起日	
	
	private String STIME ="";	//特店上版起時	
	
	private String EDATE ="";	//特店上版迄日	
	
	private String ETIME ="";	//特店上版迄時	
	
	private String ADTYPE ="";	//特店類別	
	
	private String STTYPE ="";	//商店分類	
	
	private String STWEIGHT ="";	//權重	
	
	private String STADHLK ="";	//標題	
	
	private String STPICADD ="";	//圖片路徑	
	
	private String STPICDATA;	//圖片	
	
	private String STTEL ="";	//服務專線	
	
	private String STADCON ="";	//特店優惠-文字敘述	分期優惠內容/優惠內容	
	
	private String STADNOTE ="";	//注意事項	
	
	private String STADINFO ="";	//分店資訊	
	
	private String STADSTAT ="";	//特店狀態	
	
	private String STCHRES ="";	//審核結果	
	
	private String STSUPNO ="";	//審核主管代號	
	
	private String STSUPNAME ="";	//審核主管名	
	
	private String STSUPDATE ="";	//審核日期	
	
	private String STSUPTIME ="";	//審核時間	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getSTID() {
		return STID;
	}

	public void setSTID(Integer stid) {
		STID = stid;
	}

	public String getUSERID() {
		return USERID;
	}

	public void setUSERID(String userid) {
		USERID = userid;
	}

	public String getUSERNAME() {
		return USERNAME;
	}

	public void setUSERNAME(String username) {
		USERNAME = username;
	}

	public String getTXDATE() {
		return TXDATE;
	}

	public void setTXDATE(String txdate) {
		TXDATE = txdate;
	}

	public String getTXTIME() {
		return TXTIME;
	}

	public void setTXTIME(String txtime) {
		TXTIME = txtime;
	}

	public String getSDATE() {
		return SDATE;
	}

	public void setSDATE(String sdate) {
		SDATE = sdate;
	}

	public String getSTIME() {
		return STIME;
	}

	public void setSTIME(String stime) {
		STIME = stime;
	}

	public String getEDATE() {
		return EDATE;
	}

	public void setEDATE(String edate) {
		EDATE = edate;
	}

	public String getETIME() {
		return ETIME;
	}

	public void setETIME(String etime) {
		ETIME = etime;
	}

	public String getADTYPE() {
		return ADTYPE;
	}

	public void setADTYPE(String adtype) {
		ADTYPE = adtype;
	}

	public String getSTTYPE() {
		return STTYPE;
	}

	public void setSTTYPE(String sttype) {
		STTYPE = sttype;
	}

	public String getSTWEIGHT() {
		return STWEIGHT;
	}

	public void setSTWEIGHT(String stweight) {
		STWEIGHT = stweight;
	}

	public String getSTADHLK() {
		return STADHLK;
	}

	public void setSTADHLK(String stadhlk) {
		STADHLK = stadhlk;
	}

	public String getSTPICADD() {
		return STPICADD;
	}

	public void setSTPICADD(String stpicadd) {
		STPICADD = stpicadd;
	}

	public String getSTPICDATA() {
		return STPICDATA;
	}

	public void setSTPICDATA(String stpicdata) {
		STPICDATA = stpicdata;
	}

	public String getSTTEL() {
		return STTEL;
	}

	public void setSTTEL(String sttel) {
		STTEL = sttel;
	}

	public String getSTADCON() {
		return STADCON;
	}

	public void setSTADCON(String stadcon) {
		STADCON = stadcon;
	}

	public String getSTADNOTE() {
		return STADNOTE;
	}

	public void setSTADNOTE(String stadnote) {
		STADNOTE = stadnote;
	}

	public String getSTADINFO() {
		return STADINFO;
	}

	public void setSTADINFO(String stadinfo) {
		STADINFO = stadinfo;
	}

	public String getSTADSTAT() {
		return STADSTAT;
	}

	public void setSTADSTAT(String stadstat) {
		STADSTAT = stadstat;
	}

	public String getSTCHRES() {
		return STCHRES;
	}

	public void setSTCHRES(String stchres) {
		STCHRES = stchres;
	}

	public String getSTSUPNO() {
		return STSUPNO;
	}

	public void setSTSUPNO(String stsupno) {
		STSUPNO = stsupno;
	}

	public String getSTSUPNAME() {
		return STSUPNAME;
	}

	public void setSTSUPNAME(String stsupname) {
		STSUPNAME = stsupname;
	}

	public String getSTSUPDATE() {
		return STSUPDATE;
	}

	public void setSTSUPDATE(String stsupdate) {
		STSUPDATE = stsupdate;
	}

	public String getSTSUPTIME() {
		return STSUPTIME;
	}

	public void setSTSUPTIME(String stsuptime) {
		STSUPTIME = stsuptime;
	}

}
