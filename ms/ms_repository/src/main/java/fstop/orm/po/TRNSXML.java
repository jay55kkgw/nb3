package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TRNSXML")
public class TRNSXML implements Serializable{

	@EmbeddedId
	private TRNSXMLID id;
	
	private String TXN_DATETIME;
	
	private String XML;
	
	public TRNSXMLID getId() {
		return id;
	}
	public void setId(TRNSXMLID id) {
		this.id = id;
	}
	
	public String getTXN_DATETIME() {
		return TXN_DATETIME;
	}

	public String getXML() {
		return XML;
	}

	public void setTXN_DATETIME(String tXN_DATETIME) {
		TXN_DATETIME = tXN_DATETIME;
	}

	public void setXML(String xML) {
		XML = xML;
	}
	
}
