package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "ADMDAYHHREPORT")
@Data
public class ADMDAYHHREPORT implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2478657396534810895L;

	@Id
	private ADMDAYHHREPORT_PK pks;// 複合組鍵

	private String ADOPID = "";

	private String ADUSERTYPE = "";

	private String ADAGREEF = "";

	private String ADBKFLAG = "";

	private String ADLEVEL = "";

	private String ADTXSTATUS = "";

	private String ADTXCODE = "";

	private String ADCOUNT = "";

	private String ADAMOUNT = "";

	private String LOGINTYPE = "";

	@Override
	public String toString() {
		return "ADMDAYHHREPORT [pks=" + pks + ", ADOPID=" + ADOPID + ", ADUSERTYPE=" + ADUSERTYPE + ", ADAGREEF="
				+ ADAGREEF + ", ADBKFLAG=" + ADBKFLAG + ", ADLEVEL=" + ADLEVEL + ", ADTXSTATUS=" + ADTXSTATUS
				+ ", ADTXCODE=" + ADTXCODE + ", ADCOUNT=" + ADCOUNT + ", ADAMOUNT=" + ADAMOUNT + ", LOGINTYPE="
				+ LOGINTYPE + "]";
	}	
	
}
