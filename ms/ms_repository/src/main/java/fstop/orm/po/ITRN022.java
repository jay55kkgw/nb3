package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "ITRN022")
@Data
public class ITRN022 implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9121211128127253452L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String RECNO;

	private String HEADER;

	private String SEQ;

	private String DATE;

	private String TIME;

	private String COUNT;
	
	private String COLOR;
	
	private String CRYNAME;
	
	private String CRY;
	
	private String ITR1;
	
	private String ITR2;
	
	private String ITR3;
	
	private String ITR4;
	
	private String ITR5;
	
	private String ITR6;
	
	private String ITR7;
	
	private String ITR8;
	
	private String ITR9;
	
	private String ITR10;
	
	private String ITR11;
	
	private String ITR12;
	
	private String ITR13;
	
	private String ITR14;
	
	private String ITR15;
	
	private String ITR16;
	
	private String ITR17;
	
	private String ITR18;
	
	private String ITR19;
	
	private String ITR20;
	
	private String FILL;

	@Override
	public String toString() {
		return "ITRN022 [RECNO=" + RECNO + ", HEADER=" + HEADER + ", SEQ=" + SEQ + ", DATE=" + DATE + ", TIME=" + TIME
				+ ", COUNT=" + COUNT + ", COLOR=" + COLOR + ", CRYNAME=" + CRYNAME + ", CRY=" + CRY + ", ITR1=" + ITR1
				+ ", ITR2=" + ITR2 + ", ITR3=" + ITR3 + ", ITR4=" + ITR4 + ", ITR5=" + ITR5 + ", ITR6=" + ITR6
				+ ", ITR7=" + ITR7 + ", ITR8=" + ITR8 + ", ITR9=" + ITR9 + ", ITR10=" + ITR10 + ", ITR11=" + ITR11
				+ ", ITR12=" + ITR12 + ", ITR13=" + ITR13 + ", ITR14=" + ITR14 + ", ITR15=" + ITR15 + ", ITR16=" + ITR16
				+ ", ITR17=" + ITR17 + ", ITR18=" + ITR18 + ", ITR19=" + ITR19 + ", ITR20=" + ITR20 + ", FILL=" + FILL
				+ "]";
	}
	
	


}
