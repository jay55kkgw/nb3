package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;

@Entity
@Table(name = "TXNCREDITLOG")
public class TXNCREDITLOG implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long LOGID;
	
	private String CUSIDN = "";

	private String CARDNUM = "";

	private String TXNDATE = "";

	private String TXNTIME = "";

	private String STATUS = "";

	private String FISC_REFNO = "";

	private String FISC_AUTHCODE = "";

	private String FISC_RESPCODE = "";

	public Long getLOGID() {
		return LOGID;
	}

	public void setLOGID(Long logid) {
		LOGID = logid;
	}

	public String getCARDNUM() {
		return CARDNUM;
	}

	public void setCARDNUM(String cardnum) {
		CARDNUM = cardnum;
	}

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cusidn) {
		CUSIDN = cusidn;
	}

	public String getFISC_AUTHCODE() {
		return FISC_AUTHCODE;
	}

	public void setFISC_AUTHCODE(String fisc_authcode) {
		FISC_AUTHCODE = fisc_authcode;
	}

	public String getFISC_REFNO() {
		return FISC_REFNO;
	}

	public void setFISC_REFNO(String fisc_refno) {
		FISC_REFNO = fisc_refno;
	}

	public String getFISC_RESPCODE() {
		return FISC_RESPCODE;
	}

	public void setFISC_RESPCODE(String fisc_respcode) {
		FISC_RESPCODE = fisc_respcode;
	}

	public String getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(String status) {
		STATUS = status;
	}

	public String getTXNDATE() {
		return TXNDATE;
	}

	public void setTXNDATE(String txndate) {
		TXNDATE = txndate;
	}

	public String getTXNTIME() {
		return TXNTIME;
	}

	public void setTXNTIME(String txntime) {
		TXNTIME = txntime;
	}

}
