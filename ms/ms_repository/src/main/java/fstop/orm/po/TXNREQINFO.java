package fstop.orm.po;

import java.io.Serializable;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

import fstop.util.JSONUtils;

@Entity
@Table(name = "TXNREQINFO")
public class TXNREQINFO implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long REQID;

	private String REQINFO;

	public Long getREQID() {
		return REQID;
	}

	public void setREQID(Long reqid) {
		REQID = reqid;
	}

	public String toString() {
		return new ToStringBuilder(this).append("reqid", getREQID())
				.toString();
	}

	public String getREQINFO() {
		
		Map<String, String> paramsMap = JSONUtils.json2map(REQINFO);
		paramsMap.remove("N950PASSWORD");
		paramsMap.remove("CMPASSWORD");
		paramsMap.remove("CMPWD");		
				
		return JSONUtils.map2json(paramsMap);
	}

	public void setREQINFO(String reqinfo) {
		
		Map<String, String> paramsMap = JSONUtils.json2map(reqinfo);
		paramsMap.remove("N950PASSWORD");
		paramsMap.remove("CMPASSWORD");
		paramsMap.remove("CMPWD");

		REQINFO = JSONUtils.map2json(paramsMap);
	}

}
