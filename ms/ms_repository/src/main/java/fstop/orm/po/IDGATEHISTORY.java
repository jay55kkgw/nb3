package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "IDGATEHISTORY")
@Data
public class IDGATEHISTORY implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8978724485758893029L;
	@Id
	private String ID;
	private String CUSIDN;
	private String IDGATEID;
	private String DEVICEBRAND;
	private String BINDSTATUS;
	private String BINDDATE ;
	private String BINDTIME;
	private String ADOPID;
	private String STATUS;
	private String TRANDATE;
	private String TRANTIME;
	private String LOGINTYPE;
}
