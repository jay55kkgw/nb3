package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "HK_TRNS_DATA")
public class HK_TRNS_DATA implements Serializable {

	@Id
	private String EDIID = "";
	private String HKFLAG = "";
	private String SYSKIND = "";
	private String CUSCODE = "";
	private String IPADDR = "";
	private String APPLYDATE = "";
	private String APPLYTIME = "";
	private String VID = "";
	private String USERID  = "";
	private String LOGINDATE = "";
	private String MDYDATE = "";
	private String MDYTIME = "";
	private String EMAIL = "";
	private String IDFLAG = "";
	private String IDNCHG = "";
	
	
	public String toString() {
		return new ToStringBuilder(this).append("ediid", getEDIID())
				.toString();
	}


	public String getAPPLYDATE() {
		return APPLYDATE;
	}


	public void setAPPLYDATE(String applydate) {
		APPLYDATE = applydate;
	}


	public String getAPPLYTIME() {
		return APPLYTIME;
	}


	public void setAPPLYTIME(String applytime) {
		APPLYTIME = applytime;
	}


	public String getCUSCODE() {
		return CUSCODE;
	}


	public void setCUSCODE(String cuscode) {
		CUSCODE = cuscode;
	}


	public String getEDIID() {
		return EDIID;
	}


	public void setEDIID(String ediid) {
		EDIID = ediid;
	}


	public String getEMAIL() {
		return EMAIL;
	}


	public void setEMAIL(String email) {
		EMAIL = email;
	}


	public String getHKFLAG() {
		return HKFLAG;
	}


	public void setHKFLAG(String hkflag) {
		HKFLAG = hkflag;
	}


	public String getIDFLAG() {
		return IDFLAG;
	}


	public void setIDFLAG(String idflag) {
		IDFLAG = idflag;
	}


	public String getIPADDR() {
		return IPADDR;
	}


	public void setIPADDR(String ipaddr) {
		IPADDR = ipaddr;
	}


	public String getLOGINDATE() {
		return LOGINDATE;
	}


	public void setLOGINDATE(String logindate) {
		LOGINDATE = logindate;
	}


	public String getMDYDATE() {
		return MDYDATE;
	}


	public void setMDYDATE(String mdydate) {
		MDYDATE = mdydate;
	}


	public String getMDYTIME() {
		return MDYTIME;
	}


	public void setMDYTIME(String mdytime) {
		MDYTIME = mdytime;
	}


	public String getSYSKIND() {
		return SYSKIND;
	}


	public void setSYSKIND(String syskind) {
		SYSKIND = syskind;
	}


	public String getUSERID() {
		return USERID;
	}


	public void setUSERID(String userid) {
		USERID = userid;
	}


	public String getVID() {
		return VID;
	}


	public void setVID(String vid) {
		VID = vid;
	}


	public String getIDNCHG() {
		return IDNCHG;
	}


	public void setIDNCHG(String idnchg) {
		IDNCHG = idnchg;
	}	

}
