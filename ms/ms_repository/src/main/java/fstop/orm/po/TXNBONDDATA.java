package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

import lombok.Data;

@Entity
@Table(name = "TXNBONDDATA")
@Data
public class TXNBONDDATA implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5744311718684622283L;

	@Id
	private String BONDCODE;

	private String BONDNAME;

	private String BONDCRY;

	private String DBUONLY;

	private String OBUONLY;

	private String RISK;

	private String MINBPRICE;

	private String PROGRESSIVEBPRICE;
	
	private String MINSPRICE;
	
	private String PROGRESSIVESPRICE;
	
}
