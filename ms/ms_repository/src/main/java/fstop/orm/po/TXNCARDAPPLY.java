package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TXNCARDAPPLY")
public class TXNCARDAPPLY implements Serializable {

	@Id
	private String RCVNO = "";
	private String CFU2 = "";
	private String CARDNAME1  = "";
	private String CARDNAME2  = "";
	private String CARDNAME3  = "";
	private String VARSTR2  = "";
	private String OLAGREEN1  = "";
	private String OLAGREEN2  = "";
	private String OLAGREEN3  = "";
	private String OLAGREEN4  = "";
	private String CPRIMCHNAME  = "";
	private String CPRIMENGNAME = "";
	private String CPRIMBIRTHDAY = "";
	private String MPRIMEDUCATION = "";
	private String CPRIMID = "";
	private String MPRIMMARRIAGE = "";
	private String CPRIMADDR2 = "";
	private String CPRIMHOMETELNO2A = "";
	private String CPRIMHOMETELNO2B = "";
	private String CPRIMADDR = "";
	private String CPRIMHOMETELNO1A = "";
	private String CPRIMHOMETELNO1B = "";	
	private String MPRIMADDR1COND = "";
	private String CPRIMCELLULANO1 = "";
	private String MBILLTO = "";
	private String MCARDTO = "";
	private String VARSTR3 = "";
	private String CPRIMEMAIL = "";
	private String CPRIMJOBTYPE = "";
	private String CPRIMCOMPANY = "";
	private String CPRIMJOBTITLE = "";
	private String CPRIMSALARY = "";
	private String WORKYEARS = "";
	private String WORKMONTHS = "";
	private String CPRIMADDR3 = "";
	private String CPRIMOFFICETELNO1A = "";
	private String CPRIMOFFICETELNO1B = "";
	private String CPRIMOFFICETELNO1C = "";
	private String CNOTE2 = "";
	private String MCASH = "";
	private String CNOTE1 = "";
	private String CNOTE3 = "";
	private String ECERT = "";
	private String FILE1 = "";
	private String FILE2 = "";
	private String FILE3 = "";
	private String FILE4 = "";
	private String FILE5 = "";
	private String STATUS = "";
	private String VERSION = "";
	private String IP = "";
	private String LASTDATE = "";
	private String LASTTIME = "";
	private String BRNO = "";
	private String CHENAME = "";//是否更換過姓名
	private String CTRYDESC = "";//國籍
	private String FDRSGSTAFF = "";//勸募人員
	private String BANKERNO = "";//行員編號
	private String MEMO = "";//備註		

	public String getRCVNO() {
		return RCVNO;
	}

	public void setRCVNO(String rcvno) {
		RCVNO = rcvno;
	}

	public String getCFU2() {
		return CFU2;
	}

	public void setCFU2(String cfu2) {
		CFU2 = cfu2;
	}

	public String getCARDNAME1() {
		return CARDNAME1;
	}

	public void setCARDNAME1(String cardname1) {
		CARDNAME1 = cardname1;
	}

	public String getCARDNAME2() {
		return CARDNAME2;
	}

	public void setCARDNAME2(String cardname2) {
		CARDNAME2 = cardname2;
	}

	public String getCARDNAME3() {
		return CARDNAME3;
	}

	public void setCARDNAME3(String cardname3) {
		CARDNAME3 = cardname3;
	}

	public String getVARSTR2() {
		return VARSTR2;
	}

	public void setVARSTR2(String varstr2) {
		VARSTR2 = varstr2;
	}

	public String getOLAGREEN1() {
		return OLAGREEN1;
	}

	public void setOLAGREEN1(String olagreen1) {
		OLAGREEN1 = olagreen1;
	}

	public String getOLAGREEN2() {
		return OLAGREEN2;
	}

	public void setOLAGREEN2(String olagreen2) {
		OLAGREEN2 = olagreen2;
	}

	public String getOLAGREEN3() {
		return OLAGREEN3;
	}

	public void setOLAGREEN3(String olagreen3) {
		OLAGREEN3 = olagreen3;
	}

	public String getOLAGREEN4() {
		return OLAGREEN4;
	}

	public void setOLAGREEN4(String olagreen4) {
		OLAGREEN4 = olagreen4;
	}

	public String getCPRIMCHNAME() {
		return CPRIMCHNAME;
	}

	public void setCPRIMCHNAME(String chname) {
		CPRIMCHNAME = chname;
	}	

	public String getCPRIMENGNAME() {
		return CPRIMENGNAME;
	}

	public void setCPRIMENGNAME(String engname) {
		CPRIMENGNAME = engname;
	}	

	public String getCPRIMBIRTHDAY() {
		return CPRIMBIRTHDAY;
	}

	public void setCPRIMBIRTHDAY(String birthday) {
		CPRIMBIRTHDAY = birthday;
	}

	public String getMPRIMEDUCATION() {
		return MPRIMEDUCATION;
	}

	public void setMPRIMEDUCATION(String education) {
		MPRIMEDUCATION = education;
	}

	public String getCPRIMID() {
		return CPRIMID;
	}

	public void setCPRIMID(String primid) {
		CPRIMID = primid;
	}

	public String getMPRIMMARRIAGE() {
		return MPRIMMARRIAGE;
	}

	public void setMPRIMMARRIAGE(String marriage) {
		MPRIMMARRIAGE = marriage;
	}

	public String getCPRIMADDR2() {
		return CPRIMADDR2;
	}

	public void setCPRIMADDR2(String addr2) {
		CPRIMADDR2 = addr2;
	}	

	public String getCPRIMHOMETELNO2A() {
		return CPRIMHOMETELNO2A;
	}

	public void setCPRIMHOMETELNO2A(String hometelno2a) {
		CPRIMHOMETELNO2A = hometelno2a;
	}

	public String getCPRIMHOMETELNO2B() {
		return CPRIMHOMETELNO2B;
	}

	public void setCPRIMHOMETELNO2B(String hometelno2b) {
		CPRIMHOMETELNO2B = hometelno2b;
	}

	public String getCPRIMADDR() {
		return CPRIMADDR;
	}

	public void setCPRIMADDR(String addr) {
		CPRIMADDR = addr;
	}	

	public String getCPRIMHOMETELNO1A() {
		return CPRIMHOMETELNO1A;
	}

	public void setCPRIMHOMETELNO1A(String hometelno1a) {
		CPRIMHOMETELNO1A = hometelno1a;
	}

	public String getCPRIMHOMETELNO1B() {
		return CPRIMHOMETELNO1B;
	}

	public void setCPRIMHOMETELNO1B(String hometelno1b) {
		CPRIMHOMETELNO1B = hometelno1b;
	}

	public String getMPRIMADDR1COND() {
		return MPRIMADDR1COND;
	}

	public void setMPRIMADDR1COND(String addr1cond) {
		MPRIMADDR1COND = addr1cond;
	}	

	public String getCPRIMCELLULANO1() {
		return CPRIMCELLULANO1;
	}

	public void setCPRIMCELLULANO1(String cellulano1) {
		CPRIMCELLULANO1 = cellulano1;
	}	

	public String getMBILLTO() {
		return MBILLTO;
	}

	public void setMBILLTO(String mbillto) {
		MBILLTO = mbillto;
	}

	public String getMCARDTO() {
		return MCARDTO;
	}

	public void setMCARDTO(String mcardto) {
		MCARDTO = mcardto;
	}

	public String getVARSTR3() {
		return VARSTR3;
	}

	public void setVARSTR3(String varstr3) {
		VARSTR3 = varstr3;
	}	

	public String getCPRIMEMAIL() {
		return CPRIMEMAIL;
	}

	public void setCPRIMEMAIL(String email) {
		CPRIMEMAIL = email;
	}	

	public String getCPRIMJOBTYPE() {
		return CPRIMJOBTYPE;
	}

	public void setCPRIMJOBTYPE(String jobtype) {
		CPRIMJOBTYPE = jobtype;
	}	

	public String getCPRIMCOMPANY() {
		return CPRIMCOMPANY;
	}

	public void setCPRIMCOMPANY(String company) {
		CPRIMCOMPANY = company;
	}

	public String getCPRIMJOBTITLE() {
		return CPRIMJOBTITLE;
	}

	public void setCPRIMJOBTITLE(String jobtitle) {
		CPRIMJOBTITLE = jobtitle;
	}	

	public String getCPRIMSALARY() {
		return CPRIMSALARY;
	}

	public void setCPRIMSALARY(String salary) {
		CPRIMSALARY = salary;
	}	

	public String getWORKYEARS() {
		return WORKYEARS;
	}

	public void setWORKYEARS(String workyears) {
		WORKYEARS = workyears;
	}	

	public String getWORKMONTHS() {
		return WORKMONTHS;
	}

	public void setWORKMONTHS(String workmonths) {
		WORKMONTHS = workmonths;
	}

	public String getCPRIMADDR3() {
		return CPRIMADDR3;
	}

	public void setCPRIMADDR3(String addr3) {
		CPRIMADDR3 = addr3;
	}	

	public String getCPRIMOFFICETELNO1A() {
		return CPRIMOFFICETELNO1A;
	}

	public void setCPRIMOFFICETELNO1A(String officetelno1a) {
		CPRIMOFFICETELNO1A = officetelno1a;
	}

	public String getCPRIMOFFICETELNO1B() {
		return CPRIMOFFICETELNO1B;
	}

	public void setCPRIMOFFICETELNO1B(String officetelno1b) {
		CPRIMOFFICETELNO1B = officetelno1b;
	}	

	public String getCPRIMOFFICETELNO1C() {
		return CPRIMOFFICETELNO1C;
	}

	public void setCPRIMOFFICETELNO1C(String officetelno1c) {
		CPRIMOFFICETELNO1C = officetelno1c;
	}	

	public String getCNOTE2() {
		return CNOTE2;
	}

	public void setCNOTE2(String cnote2) {
		CNOTE2 = cnote2;
	}	

	public String getMCASH() {
		return MCASH;
	}

	public void setMCASH(String mcash) {
		MCASH = mcash;
	}	

	public String getCNOTE1() {
		return CNOTE1;
	}

	public void setCNOTE1(String cnote1) {
		CNOTE1 = cnote1;
	}

	public String getCNOTE3() {
		return CNOTE3;
	}

	public void setCNOTE3(String cnote3) {
		CNOTE3 = cnote3;
	}	
	
	public String getECERT() {
		return ECERT;
	}

	public void setECERT(String ecert) {
		ECERT = ecert;
	}	
	
	public String getFILE1() {
		return FILE1;
	}

	public void setFILE1(String file1) {
		FILE1 = file1;
	}
	
	public String getFILE2() {
		return FILE2;
	}

	public void setFILE2(String file2) {
		FILE2 = file2;
	}	
	
	public String getFILE3() {
		return FILE3;
	}

	public void setFILE3(String file3) {
		FILE3 = file3;
	}
	public String getFILE4() {
		return FILE4;
	}

	public void setFILE4(String file4) {
		FILE4 = file4;
	}
	public String getFILE5() {
		return FILE5;
	}

	public void setFILE5(String file5) {
		FILE5 = file5;
	}		
	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}
	
	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}
	
	public String getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(String status) {
		STATUS = status;
	}	
	
	public String getVERSION() {
		return VERSION;
	}

	public void setVERSION(String version) {
		VERSION = version;
	}	
	public String getIP() {
		return IP;
	}

	public void setIP(String ip) {
		IP = ip;
	}
	public String getBRNO() {
		return BRNO;
	}

	public void setBRNO(String brno) {
		BRNO = brno;
	}			

	public String getCHENAME() {
		return CHENAME;
	}

	public void setCHENAME(String chename) {
		CHENAME = chename;
	}

	public String getCTRYDESC() {
		return CTRYDESC;
	}

	public void setCTRYDESC(String ctrydesc) {
		CTRYDESC = ctrydesc;
	}

	public String getFDRSGSTAFF() {
		return FDRSGSTAFF;
	}

	public void setFDRSGSTAFF(String fdrsgstaff) {
		FDRSGSTAFF = fdrsgstaff;
	}
	
	public String getBANKERNO() {
		return BANKERNO;
	}

	public void setBANKERNO(String bankerno) {
		BANKERNO = bankerno;
	}
	
	public String getMEMO() {
		return MEMO;
	}

	public void setMEMO(String memo) {
		MEMO = memo;
	}
}
