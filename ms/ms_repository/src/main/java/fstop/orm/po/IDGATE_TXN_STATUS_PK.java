package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Embeddable;

import lombok.Data;

@Embeddable
@Data
public class IDGATE_TXN_STATUS_PK  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5491873221952721452L;
	
	private String SESSIONID;
	private String IDGATEID;
	private String TXNID;
}
