package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;

@Entity
@Table(name = "PCALOTTONOTICE")
public class PCALOTTONOTICE implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long PCAID;
	
	private String NAME = "";

	private String TELPHONE = "";

	private String ADDRESS = "";

	private String EMAIL = "";

	private String BRANCHNAME = "";

	private String ANIMALKIND = "";

	private String MEMO = "";

	private String LASTUSER = "";

	private String LASTDATE = "";

	private String LASTTIME = "";
	
	//----20170614--start
	
	private String SEX = "";	//性別
	
	private String AGE = "";	//年齡

	public String getSEX() {
		return SEX;
	}

	public void setSEX(String sEX) {
		SEX = sEX;
	}

	public String getAGE() {
		return AGE;
	}

	public void setAGE(String aGE) {
		AGE = aGE;
	}
	
	//----20170614--end

	public Long getPCAID() {
		return PCAID;
	}

	public void setPCAID(Long pcaid) {
		PCAID = pcaid;
	}

	public String getNAME() {
		return NAME;
	}

	public void setNAME(String name) {
		NAME = name;
	}

	public String getTELPHONE() {
		return TELPHONE;
	}

	public void setTELPHONE(String telphone) {
		TELPHONE = telphone;
	}

	public String getADDRESS() {
		return ADDRESS;
	}

	public void setADDRESS(String address) {
		ADDRESS = address;
	}

	public String getEMAIL() {
		return EMAIL;
	}

	public void setEMAIL(String email) {
		EMAIL = email;
	}

	public String getBRANCHNAME() {
		return BRANCHNAME;
	}

	public void setBRANCHNAME(String branchname) {
		BRANCHNAME = branchname;
	}

	public String getANIMALKIND() {
		return ANIMALKIND;
	}

	public void setANIMALKIND(String animalkind) {
		ANIMALKIND = animalkind;
	}

	public String getMEMO() {
		return MEMO;
	}

	public void setMEMO(String memo) {
		MEMO = memo;
	}
	
	public String getLASTUSER() {
		return LASTUSER;
	}
	
	public void setLASTUSER(String lastuser) {
		LASTUSER = lastuser;
	}
	
	public String getLASTDATE() {
		return LASTDATE;
	}
	
	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}
	
	public String getLASTTIME() {
		return LASTTIME;
	}
	
	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}
}
