package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "TXNUSER")
public class TXNUSER implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1014197847472213956L;

	@Id
	private String DPSUERID;

	private String DPUSERNAME = "";

	private String DPBILL  = "";

	private String DPMENU  = "";

	private String DPOVERVIEW  = "";

	private String DPMYEMAIL  = "";

	private String DPNOTIFY  = "";

	private String DPFUNDBILL  = "";

	private String DPCARDBILL  = "";

	private String LASTDATE  = "";

	private String LASTTIME  = "";

	private String BILLDATE = "";

	private String FUNDBILLDATE = "";

	private String CARDBILLDATE = "";

	private String EMAILDATE = "";

	private Integer SCHCOUNT;

	private String SCHCNTDATE = "";

	private String ADBRANCHID = "";

	private String FUNDAGREEVERSION = "";

	private String FUNDBILLSENDMODE = "";

	private Integer ASKERRTIMES;

	private String CCARDFLAG = "";
	
	private String RESETTIME = "";

	private String FXREMITFLAG = "";
	
	
	public String getDPUSERNAME() {
		return DPUSERNAME;
	}

	public void setDPUSERNAME(String dpusername) {
		DPUSERNAME = dpusername;
	}

	public String getSCHCNTDATE() {
		return SCHCNTDATE;
	}

	public void setSCHCNTDATE(String schcntdate) {
		SCHCNTDATE = schcntdate;
	}

	public Integer getSCHCOUNT() {
		return SCHCOUNT;
	}

	public void setSCHCOUNT(Integer schcount) {
		SCHCOUNT = schcount;
	}

	public String getBILLDATE() {
		return BILLDATE;
	}

	public void setBILLDATE(String billdate) {
		BILLDATE = billdate;
	}

	public String getCARDBILLDATE() {
		return CARDBILLDATE;
	}

	public void setCARDBILLDATE(String cardbilldate) {
		CARDBILLDATE = cardbilldate;
	}

	public String getEMAILDATE() {
		return EMAILDATE;
	}

	public void setEMAILDATE(String emaildate) {
		EMAILDATE = emaildate;
	}

	public String getFUNDBILLDATE() {
		return FUNDBILLDATE;
	}

	public void setFUNDBILLDATE(String fundbilldate) {
		FUNDBILLDATE = fundbilldate;
	}

	public String toString() {
		return new ToStringBuilder(this).append("dpsuerid", getDPSUERID())
				.toString();
	}

	public String getDPBILL() {
		return DPBILL;
	}

	public void setDPBILL(String dpbill) {
		DPBILL = dpbill;
	}

	public String getDPMENU() {
		return DPMENU;
	}

	public void setDPMENU(String dpmenu) {
		DPMENU = dpmenu;
	}

	public String getDPMYEMAIL() {
		return DPMYEMAIL;
	}

	public void setDPMYEMAIL(String dpmyemail) {
		DPMYEMAIL = dpmyemail;
	}

	public String getDPNOTIFY() {
		return DPNOTIFY;
	}

	public void setDPNOTIFY(String dpnotify) {
		DPNOTIFY = dpnotify;
	}

	public String getDPOVERVIEW() {
		return DPOVERVIEW;
	}

	public void setDPOVERVIEW(String dpoverview) {
		DPOVERVIEW = dpoverview;
	}

	public String getDPSUERID() {
		return DPSUERID;
	}

	public void setDPSUERID(String dpsuerid) {
		DPSUERID = dpsuerid;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

	public String getDPCARDBILL() {
		return DPCARDBILL;
	}

	public void setDPCARDBILL(String dpcardbill) {
		DPCARDBILL = dpcardbill;
	}

	public String getDPFUNDBILL() {
		return DPFUNDBILL;
	}

	public void setDPFUNDBILL(String dpfundbill) {
		DPFUNDBILL = dpfundbill;
	}

	public String getADBRANCHID() {
		return ADBRANCHID;
	}

	public void setADBRANCHID(String adbranchid) {
		ADBRANCHID = adbranchid;
	}

	public String getFUNDAGREEVERSION() {
		return FUNDAGREEVERSION;
	}

	public void setFUNDAGREEVERSION(String version) {
		FUNDAGREEVERSION = version;
	}

	public String getFUNDBILLSENDMODE() {
		return FUNDBILLSENDMODE;
	}

	public void setFUNDBILLSENDMODE(String mode) {
		FUNDBILLSENDMODE = mode;
	}

	public Integer getASKERRTIMES() {
		return ASKERRTIMES;
	}

	public void setASKERRTIMES(Integer askerrtimes) {
		ASKERRTIMES = askerrtimes;
	}

	public String getCCARDFLAG() {
		return CCARDFLAG;
	}

	public void setCCARDFLAG(String ccardflag) {
		CCARDFLAG = ccardflag;
	}

	public String getFXREMITFLAG() {
		return FXREMITFLAG;
	}

	public void setFXREMITFLAG(String fxremitflag) {
		FXREMITFLAG = fxremitflag;
	}

	public String getRESETTIME() {
		return RESETTIME;
	}

	public void setRESETTIME(String resettime) {
		RESETTIME = resettime;
	}


}
