package fstop.orm.po;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "ADMZIPDATA")
public class ADMZIPDATA implements Serializable {

	@Id
	private String ZIPCODE;

	private String CITY;

	private String AREA;

	private String ROAD;


	public String getZIPCODE() {
		return ZIPCODE;
	}

	public void setZIPCODE(String zipcode) {
		ZIPCODE = zipcode;
	}

	public String getCITY() {
		return CITY;
	}

	public void setCITY(String city) {
		CITY = city;
	}

	public String getAREA() {
		return AREA;
	}

	public void setAREA(String area) {
		AREA = area;
	}

	public String getROAD() {
		return ROAD;
	}

	public void setROAD(String road) {
		ROAD = road;
	}
}
