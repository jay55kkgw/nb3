package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TRNSRESEND")
public class TRNSRESEND implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long API_TXN_NO;
	
	private String TXN_DATETIME;

	private String MTI;
	
	private String PCODE;
	
	private String RCODE;
	
	private String SRC_ID;
	
	private String ORG_API_TXN_NO;
	
	private String ORG_TXN_DATETIME;
	
	private String ORG_RCODE;
	
	private String ORG_STAN;
	
	private String TRNSCODE;

	public Long getAPI_TXN_NO() {
		return API_TXN_NO;
	}

	public String getTXN_DATETIME() {
		return TXN_DATETIME;
	}

	public String getMTI() {
		return MTI;
	}

	public String getPCODE() {
		return PCODE;
	}

	public String getRCODE() {
		return RCODE;
	}

	public String getSRC_ID() {
		return SRC_ID;
	}

	public String getORG_API_TXN_NO() {
		return ORG_API_TXN_NO;
	}

	public String getORG_TXN_DATETIME() {
		return ORG_TXN_DATETIME;
	}

	public String getORG_RCODE() {
		return ORG_RCODE;
	}

	public String getORG_STAN() {
		return ORG_STAN;
	}

	public String getTRNSCODE() {
		return TRNSCODE;
	}

	public void setAPI_TXN_NO(Long aPI_TXN_NO) {
		API_TXN_NO = aPI_TXN_NO;
	}

	public void setTXN_DATETIME(String tXN_DATETIME) {
		TXN_DATETIME = tXN_DATETIME;
	}

	public void setMTI(String mTI) {
		MTI = mTI;
	}

	public void setPCODE(String pCODE) {
		PCODE = pCODE;
	}

	public void setRCODE(String rCODE) {
		RCODE = rCODE;
	}

	public void setSRC_ID(String sRC_ID) {
		SRC_ID = sRC_ID;
	}

	public void setORG_API_TXN_NO(String oRG_API_TXN_NO) {
		ORG_API_TXN_NO = oRG_API_TXN_NO;
	}

	public void setORG_TXN_DATETIME(String oRG_TXN_DATETIME) {
		ORG_TXN_DATETIME = oRG_TXN_DATETIME;
	}

	public void setORG_RCODE(String oRG_RCODE) {
		ORG_RCODE = oRG_RCODE;
	}

	public void setORG_STAN(String oRG_STAN) {
		ORG_STAN = oRG_STAN;
	}

	public void setTRNSCODE(String tRNSCODE) {
		TRNSCODE = tRNSCODE;
	}
	
}
