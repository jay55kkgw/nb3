package fstop.orm.po;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TXNCARDLOG")
public class TXNCARDLOG implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long LOGID;

	private String DPSUERID = "";

	private String CARDTYPE = "";

	private String CRADNO = "";

	private String STATUS = "";

	private String OPERATETYPE = "";

	private String ADMCODE = "";

	private String REMOTEIP = "";

	private String LOGDATE = "";

	private String LOGTIME = "";

	public String getADMCODE() {
		return ADMCODE;
	}

	public void setADMCODE(String admcode) {
		ADMCODE = admcode;
	}

	public String getCARDTYPE() {
		return CARDTYPE;
	}

	public void setCARDTYPE(String cardtype) {
		CARDTYPE = cardtype;
	}

	public String getCRADNO() {
		return CRADNO;
	}

	public void setCRADNO(String cradno) {
		CRADNO = cradno;
	}

	public String getDPSUERID() {
		return DPSUERID;
	}

	public void setDPSUERID(String dpsuerid) {
		DPSUERID = dpsuerid;
	}

	public String getLOGDATE() {
		return LOGDATE;
	}

	public void setLOGDATE(String logdate) {
		LOGDATE = logdate;
	}

	public Long getLOGID() {
		return LOGID;
	}

	public void setLOGID(Long logid) {
		LOGID = logid;
	}

	public String getLOGTIME() {
		return LOGTIME;
	}

	public void setLOGTIME(String logtime) {
		LOGTIME = logtime;
	}

	public String getOPERATETYPE() {
		return OPERATETYPE;
	}

	public void setOPERATETYPE(String operatetype) {
		OPERATETYPE = operatetype;
	}

	public String getREMOTEIP() {
		return REMOTEIP;
	}

	public void setREMOTEIP(String remoteip) {
		REMOTEIP = remoteip;
	}

	public String getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(String status) {
		STATUS = status;
	}


}
