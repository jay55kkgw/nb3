package fstop.orm.po;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "QRCODE_PARAMS")
public class QRCODEPARAM implements Serializable {

	@Id
	private String NAME;
	
	private String VAL;
	
	public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	public String getVAL() {
		return VAL;
	}
	public void setVAL(String vAL) {
		VAL = vAL;
	}
	
	
}
