package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "ADMNBSTATUS")
public class ADMNBSTATUS implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2778260428076610503L;

	@Id
	private String ADNBSTATUSID;

	private String ADNBSTATUS;

	private String LASTUSER;

	private String LASTDATE;

	private String LASTTIME;

	public String toString() {
		return new ToStringBuilder(this).append("adnbstatusid",
				getADNBSTATUSID()).toString();
	}

	public String getADNBSTATUS() {
		return ADNBSTATUS;
	}

	public void setADNBSTATUS(String adnbstatus) {
		ADNBSTATUS = adnbstatus;
	}

	public String getADNBSTATUSID() {
		return ADNBSTATUSID;
	}

	public void setADNBSTATUSID(String adnbstatusid) {
		ADNBSTATUSID = adnbstatusid;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

	public String getLASTUSER() {
		return LASTUSER;
	}

	public void setLASTUSER(String lastuser) {
		LASTUSER = lastuser;
	}

}
