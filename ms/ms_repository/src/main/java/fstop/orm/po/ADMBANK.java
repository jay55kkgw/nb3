package fstop.orm.po;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "ADMBANK")
public class ADMBANK implements Serializable {

	@Id
	private String ADBANKID;

	private String ADBANKNAME;

	private String LASTUSER;

	private String LASTDATE;

	private String LASTTIME;

//	private Set TXNTWRECORDS;
//
//	private Set TXNFXRECORDS;
//
//	private Set TXNTRACCSETS;
//
//	private Set TXNFXSCHEDULES;
//
//	private Set TXNTWSCHEDULES;

	public String toString() {
		return new ToStringBuilder(this).append("adbankid", getADBANKID())
				.toString();
	}

	public String getADBANKID() {
		return ADBANKID;
	}

	public void setADBANKID(String adbankid) {
		ADBANKID = adbankid;
	}

	public String getADBANKNAME() {
		return ADBANKNAME;
	}

	public void setADBANKNAME(String adbankname) {
		ADBANKNAME = adbankname;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

	public String getLASTUSER() {
		return LASTUSER;
	}

	public void setLASTUSER(String lastuser) {
		LASTUSER = lastuser;
	}
//
//	public Set getTXNFXRECORDS() {
//		return TXNFXRECORDS;
//	}
//
//	public void setTXNFXRECORDS(Set txnfxrecords) {
//		TXNFXRECORDS = txnfxrecords;
//	}
//
//	public Set getTXNFXSCHEDULES() {
//		return TXNFXSCHEDULES;
//	}
//
//	public void setTXNFXSCHEDULES(Set txnfxschedules) {
//		TXNFXSCHEDULES = txnfxschedules;
//	}
//
//	public Set getTXNTRACCSETS() {
//		return TXNTRACCSETS;
//	}
//
//	public void setTXNTRACCSETS(Set txntraccsets) {
//		TXNTRACCSETS = txntraccsets;
//	}
//
//	public Set getTXNTWRECORDS() {
//		return TXNTWRECORDS;
//	}
//
//	public void setTXNTWRECORDS(Set txntwrecords) {
//		TXNTWRECORDS = txntwrecords;
//	}
//
//	public Set getTXNTWSCHEDULES() {
//		return TXNTWSCHEDULES;
//	}
//
//	public void setTXNTWSCHEDULES(Set txntwschedules) {
//		TXNTWSCHEDULES = txntwschedules;
//	}

}
