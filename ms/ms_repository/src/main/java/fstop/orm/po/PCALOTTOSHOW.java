package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PCALOTTOSHOW")
public class PCALOTTOSHOW implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long ID;
	
	private String MONEY = "";
	
	private String VIEWDATE = "";
	
	public Long getID() {
		return ID;
	}
	
	public void setID(Long iD) {
		ID = iD;
	}
	
	public String getMONEY() {
		return MONEY;
	}
	
	public void setMONEY(String mONEY) {
		MONEY = mONEY;
	}
	
	public String getVIEWDATE() {
		return VIEWDATE;
	}
	
	public void setVIEWDATE(String vIEWDATE) {
		VIEWDATE = vIEWDATE;
	}
}
