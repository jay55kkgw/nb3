package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;

@Entity
@Table(name = "TXNREJ")
public class TXNREJ implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long RID;
	
	private String NAME = "";	
	
	private String CUSIDN = "";

	private String DIGVERSION = "";

	private String ACNVERSION = "";	
	
	private String BIRTHDAY = "";

	private String APPLYDAY = "";

	private String CMDATE = "";

	private String CITYCHA = "";

	private String PURPOSE = "";

	private String JCICFAILED = "";

	private String BLACKLIST = "";

	public String getNAME() {
		return NAME;
	}

	public void setNAME(String name) {
		NAME = name;
	}	
	
	public String getAPPLYDAY() {
		return APPLYDAY;
	}

	public void setAPPLYDAY(String applyday) {
		APPLYDAY = applyday;
	}

	public String getBIRTHDAY() {
		return BIRTHDAY;
	}

	public void setBIRTHDAY(String birthday) {
		BIRTHDAY = birthday;
	}

	public String getBLACKLIST() {
		return BLACKLIST;
	}

	public void setBLACKLIST(String blacklist) {
		BLACKLIST = blacklist;
	}

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cusidn) {
		CUSIDN = cusidn;
	}

	public String getJCICFAILED() {
		return JCICFAILED;
	}

	public void setJCICFAILED(String jcicfailed) {
		JCICFAILED = jcicfailed;
	}

	public String getPURPOSE() {
		return PURPOSE;
	}

	public void setPURPOSE(String purpose) {
		PURPOSE = purpose;
	}

	public String getCITYCHA() {
		return CITYCHA;
	}

	public void setCITYCHA(String citycha) {
		CITYCHA = citycha;
	}

	public String getCMDATE() {
		return CMDATE;
	}

	public void setCMDATE(String cmdate) {
		CMDATE = cmdate;
	}

	public Long getRID() {
		return RID;
	}

	public void setRID(Long rid) {
		RID = rid;
	}

	public String getDIGVERSION() {
		return DIGVERSION;
	}

	public void setDIGVERSION(String digversion) {
		DIGVERSION = digversion;
	}
	
	public String getACNVERSION() {
		return ACNVERSION;
	}

	public void setACNVERSION(String acnversion) {
		ACNVERSION = acnversion;
	}		
}
