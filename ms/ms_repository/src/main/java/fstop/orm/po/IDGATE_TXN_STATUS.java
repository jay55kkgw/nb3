package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "IDGATE_TXN_STATUS")
@Data
public class IDGATE_TXN_STATUS  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7735653341773285393L;

	@Id
	private IDGATE_TXN_STATUS_PK pks;

	private String TXNCONTENT;
	private String TXNSTATUS;
	private String LASTDATE;
	private String LASTTIME;
	private String ADOPID;
	private String ENCTXNID;
}
