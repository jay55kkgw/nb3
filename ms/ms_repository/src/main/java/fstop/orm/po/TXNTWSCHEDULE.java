package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * 

 * @author Owner
 *
 */
@Entity
@Table(name = "TXNTWSCHEDULE")
public class TXNTWSCHEDULE implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long DPSCHID;

	private String ADOPID = "";
	private String DPUSERID = "";
	private String DPPERMTDATE = "";
	private String DPFDATE = "";
	private String DPTDATE = "";
	private String DPNEXTDATE = "";
	private String DPWDAC = "";
	private String DPSVBH = "";
	private String DPSVAC = "";
	private String DPTXAMT = "";
	private String DPTXMEMO = "";
	private String DPTXMAILS = "";
	private String DPTXMAILMEMO = "";
	private String DPTXCODE = "";
	private Long DPTXINFO = 0l;
	private String DPSDATE = "";
	private String DPSTIME = "";
	private String DPTXSTATUS = "";
	private String XMLCA = "";
	private String XMLCN = "";
	private String MAC = "";
	private String LASTDATE = "";
	private String LASTTIME = "";
	private String DPSCHNO = "";
	private String LOGINTYPE = ""; //NB:網銀 ， MB:行動 ，預設為NB
	

	public String getLASTDATE() {
		return LASTDATE;
	}


	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}


	public String getLASTTIME() {
		return LASTTIME;
	}


	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}


	public String toString() {
		return new ToStringBuilder(this).append("dpschid", getDPSCHID())
				.toString();
	}


	public String getADOPID() {
		return ADOPID;
	}


	public void setADOPID(String adopid) {
		ADOPID = adopid;
	}


	public String getDPFDATE() {
		return DPFDATE;
	}


	public void setDPFDATE(String dpfdate) {
		DPFDATE = dpfdate;
	}



	public String getDPNEXTDATE() {
		return DPNEXTDATE;
	}


	public void setDPNEXTDATE(String dpnextdate) {
		DPNEXTDATE = dpnextdate;
	}


	public String getDPPERMTDATE() {
		return DPPERMTDATE;
	}


	public void setDPPERMTDATE(String dppermtdate) {
		DPPERMTDATE = dppermtdate;
	}


	public Long getDPSCHID() {
		return DPSCHID;
	}


	public void setDPSCHID(Long dpschid) {
		DPSCHID = dpschid;
	}


	public String getDPSDATE() {
		return DPSDATE;
	}


	public void setDPSDATE(String dpsdate) {
		DPSDATE = dpsdate;
	}


	public String getDPSTIME() {
		return DPSTIME;
	}


	public void setDPSTIME(String dpstime) {
		DPSTIME = dpstime;
	}


	public String getDPSVAC() {
		return DPSVAC;
	}


	public void setDPSVAC(String dpsvac) {
		DPSVAC = dpsvac;
	}


	public String getDPSVBH() {
		return DPSVBH;
	}


	public void setDPSVBH(String dpsvbh) {
		DPSVBH = dpsvbh;
	}


	public String getDPTDATE() {
		return DPTDATE;
	}


	public void setDPTDATE(String dptdate) {
		DPTDATE = dptdate;
	}


	public String getDPTXAMT() {
		return DPTXAMT;
	}


	public void setDPTXAMT(String dptxamt) {
		DPTXAMT = dptxamt;
	}


	public String getDPTXCODE() {
		return DPTXCODE;
	}


	public void setDPTXCODE(String dptxcode) {
		DPTXCODE = dptxcode;
	}


	public Long getDPTXINFO() {
		return DPTXINFO;
	}


	public void setDPTXINFO(Long dptxinfo) {
		DPTXINFO = dptxinfo;
	}


	public String getDPTXMAILMEMO() {
		return DPTXMAILMEMO;
	}


	public void setDPTXMAILMEMO(String dptxmailmemo) {
		DPTXMAILMEMO = dptxmailmemo;
	}


	public String getDPTXMAILS() {
		return DPTXMAILS;
	}


	public void setDPTXMAILS(String dptxmails) {
		DPTXMAILS = dptxmails;
	}


	public String getDPTXMEMO() {
		return DPTXMEMO;
	}


	public void setDPTXMEMO(String dptxmemo) {
		DPTXMEMO = dptxmemo;
	}


	public String getDPTXSTATUS() {
		return DPTXSTATUS;
	}


	public void setDPTXSTATUS(String dptxstatus) {
		DPTXSTATUS = dptxstatus;
	}


	public String getDPUSERID() {
		return DPUSERID;
	}


	public void setDPUSERID(String dpuserid) {
		DPUSERID = dpuserid;
	}


	public String getDPWDAC() {
		return DPWDAC;
	}


	public void setDPWDAC(String dpwdac) {
		DPWDAC = dpwdac;
	}


	public String getMAC() {
		return MAC;
	}


	public void setMAC(String mac) {
		MAC = mac;
	}


	public String getXMLCA() {
		return XMLCA;
	}


	public void setXMLCA(String xmlca) {
		XMLCA = xmlca;
	}


	public String getXMLCN() {
		return XMLCN;
	}


	public void setXMLCN(String xmlcn) {
		XMLCN = xmlcn;
	}


	public String getDPSCHNO() {
		return DPSCHNO;
	}


	public void setDPSCHNO(String dpschno) {
		DPSCHNO = dpschno;
	}


	public String getLOGINTYPE() {
		return LOGINTYPE;
	}


	public void setLOGINTYPE(String logintype) {
		LOGINTYPE = logintype;
	}

}
