package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Embeddable;

import lombok.Data;

@Embeddable
@Data
public class ADMDAYHHREPORT_PK implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8620690242231379885L;
	
	private String CMYYYYMMDD = "";		//預約批號
	private String CMHH = "";//預約轉帳日	

}
