package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "ITRN024")
@Data
public class ITRN024 implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1519070606891396886L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String RECNO;

	private String HEADER;

	private String SEQ;

	private String DATE;

	private String TIME;

	private String COUNT;
	
	private String COLOR;
	
	private String CRYNAME;
	
	private String CRY;
	
	private String ITR1;
	
	private String ITR2;
	
	private String FILL;

	@Override
	public String toString() {
		return "ITRN024 [RECNO=" + RECNO + ", HEADER=" + HEADER + ", SEQ=" + SEQ + ", DATE=" + DATE + ", TIME=" + TIME
				+ ", COUNT=" + COUNT + ", COLOR=" + COLOR + ", CRYNAME=" + CRYNAME + ", CRY=" + CRY + ", ITR1=" + ITR1
				+ ", ITR2=" + ITR2 + ", FILL=" + FILL + "]";
	}
	
}
