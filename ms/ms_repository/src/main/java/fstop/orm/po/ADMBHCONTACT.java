package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "ADMBHCONTACT")
public class ADMBHCONTACT implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer ADBHCONTID;

	private String ADBRANCHID;

	private String ADBRANCHNAME;
	
	private String ADCONTACTIID;

	private String ADCONTACTNAME;

	private String ADCONTACTEMAIL;

	private String ADCONTACTTEL;

	private String LASTUSER;

	private String LASTDATE;

	private String LASTTIME;

	public String toString() {
		return new ToStringBuilder(this).append("adbhcontid", getADBHCONTID())
				.toString();
	}

	public Integer getADBHCONTID() {
		return ADBHCONTID;
	}

	public void setADBHCONTID(Integer adbhcontid) {
		ADBHCONTID = adbhcontid;
	}

	public String getADBRANCHID() {
		return ADBRANCHID;
	}

	public void setADBRANCHID(String adbranchid) {
		ADBRANCHID = adbranchid;
	}
	
	public void setADBRANCHNAME(String adbranchname) {
		ADBRANCHNAME = adbranchname;
	}

	public String getADBRANCHNAME() {
		return ADBRANCHNAME;
	}
	
	public String getADCONTACTEMAIL() {
		return ADCONTACTEMAIL;
	}

	public void setADCONTACTEMAIL(String adcontactemail) {
		ADCONTACTEMAIL = adcontactemail;
	}

	public String getADCONTACTIID() {
		return ADCONTACTIID;
	}

	public void setADCONTACTIID(String adcontactiid) {
		ADCONTACTIID = adcontactiid;
	}

	public String getADCONTACTNAME() {
		return ADCONTACTNAME;
	}

	public void setADCONTACTNAME(String adcontactname) {
		ADCONTACTNAME = adcontactname;
	}

	public String getADCONTACTTEL() {
		return ADCONTACTTEL;
	}

	public void setADCONTACTTEL(String adcontacttel) {
		ADCONTACTTEL = adcontacttel;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

	public String getLASTUSER() {
		return LASTUSER;
	}

	public void setLASTUSER(String lastuser) {
		LASTUSER = lastuser;
	}

}
