package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "SYSPARAMDATA")
public class SYSPARAMDATA implements Serializable {

	@Id
	private String ADPK;

	private String ADNAME = "";

	@Column(name="ADPWD")
	private String ADPW = "";

	private String ADAPNAME = "";

	private String ADFXSHH = "";

	private String ADFXSMM = "";

	private String ADFXSSS = "";

	private String ADFXEHH = "";

	private String ADFXEMM = "";

	private String ADFXESS = "";

	private String ADFDSHH = "";

	private String ADFDSMM = "";

	private String ADFDSSS = "";

	private String ADFDEHH = "";

	private String ADFDEMM = "";

	private String ADFDESS = "";

	private String ADPORTALJNDI = "";

	private String ADNBJNDI = "";

	private String ADSESSIONTO = "";

	private String ADOPMAIL = "";

	private String ADAPMAIL = "";

	private String ADSECMAIL = "";

	private String ADSPMAIL = "";

	private String ADFDSVRIP = "";

	private String ADMAILSVRIP = "";

	private String ADPARKSVRIP = "";

	private String ADARSVRIP = "";

	private String ADFDRETRYTIMES = "";

	private String ADPKRETRYTIMES = "";

	private String ADRESENDTIMES = "";

	private String ADBHMAILACNORULE = "";

	private String LASTUSER = "";

	private String LASTDATE = "";

	private String LASTTIME = "";

	private String ADPORTALURL = "";

	private String ADPORTALURL_ADM = "";
	
	private String ADGDMAIL = "";
	
	private String ADGDTXNMAIL = "";

	private String ADGDSHH = "";

	private String ADGDSMM = "";

	private String ADGDSSS = "";

	private String ADGDEHH = "";

	private String ADGDEMM = "";

	private String ADGDESS = "";
	
	private String KYCDATE = "";
	private String SCN070 = "";
	private String SCF002 = "";
	
	private String ADFX1SHH = "";

	private String ADFX1SMM = "";

	private String ADFX1SSS = "";

	private String ADFXE1HH = "";

	private String ADFXE1MM = "";

	private String ADFXE1SS = "";

	
	public String toString() {
		return new ToStringBuilder(this).append("adpk", getADPK()).toString();
	}

	public String getADAPMAIL() {
		return ADAPMAIL;
	}

	public void setADAPMAIL(String adapmail) {
		ADAPMAIL = adapmail;
	}

	public String getADAPNAME() {
		return ADAPNAME;
	}

	public void setADAPNAME(String adapname) {
		ADAPNAME = adapname;
	}

	public String getADARSVRIP() {
		return ADARSVRIP;
	}

	public void setADARSVRIP(String adarsvrip) {
		ADARSVRIP = adarsvrip;
	}

	public String getADBHMAILACNORULE() {
		return ADBHMAILACNORULE;
	}

	public void setADBHMAILACNORULE(String adbhmailacnorule) {
		ADBHMAILACNORULE = adbhmailacnorule;
	}

	public String getADFDEHH() {
		return ADFDEHH;
	}

	public void setADFDEHH(String adfdehh) {
		ADFDEHH = adfdehh;
	}

	public String getADFDEMM() {
		return ADFDEMM;
	}

	public void setADFDEMM(String adfdemm) {
		ADFDEMM = adfdemm;
	}

	public String getADFDESS() {
		return ADFDESS;
	}

	public void setADFDESS(String adfdess) {
		ADFDESS = adfdess;
	}

	public String getADFDRETRYTIMES() {
		return ADFDRETRYTIMES;
	}

	public void setADFDRETRYTIMES(String adfdretrytimes) {
		ADFDRETRYTIMES = adfdretrytimes;
	}

	public String getADFDSHH() {
		return ADFDSHH;
	}

	public void setADFDSHH(String adfdshh) {
		ADFDSHH = adfdshh;
	}

	public String getADFDSMM() {
		return ADFDSMM;
	}

	public void setADFDSMM(String adfdsmm) {
		ADFDSMM = adfdsmm;
	}

	public String getADFDSSS() {
		return ADFDSSS;
	}

	public void setADFDSSS(String adfdsss) {
		ADFDSSS = adfdsss;
	}

	public String getADFDSVRIP() {
		return ADFDSVRIP;
	}

	public void setADFDSVRIP(String adfdsvrip) {
		ADFDSVRIP = adfdsvrip;
	}

	public String getADFXEHH() {
		return ADFXEHH;
	}

	public void setADFXEHH(String adfxehh) {
		ADFXEHH = adfxehh;
	}

	public String getADFXEMM() {
		return ADFXEMM;
	}

	public void setADFXEMM(String adfxemm) {
		ADFXEMM = adfxemm;
	}

	public String getADFXESS() {
		return ADFXESS;
	}

	public void setADFXESS(String adfxess) {
		ADFXESS = adfxess;
	}

	public String getADFXSHH() {
		return ADFXSHH;
	}

	public void setADFXSHH(String adfxshh) {
		ADFXSHH = adfxshh;
	}

	public String getADFXSMM() {
		return ADFXSMM;
	}

	public void setADFXSMM(String adfxsmm) {
		ADFXSMM = adfxsmm;
	}

	public String getADFXSSS() {
		return ADFXSSS;
	}

	public void setADFXSSS(String adfxsss) {
		ADFXSSS = adfxsss;
	}

	public String getADMAILSVRIP() {
		return ADMAILSVRIP;
	}

	public void setADMAILSVRIP(String admailsvrip) {
		ADMAILSVRIP = admailsvrip;
	}

	public String getADNAME() {
		return ADNAME;
	}

	public void setADNAME(String adname) {
		ADNAME = adname;
	}

	public String getADNBJNDI() {
		return ADNBJNDI;
	}

	public void setADNBJNDI(String adnbjndi) {
		ADNBJNDI = adnbjndi;
	}

	public String getADOPMAIL() {
		return ADOPMAIL;
	}

	public void setADOPMAIL(String adopmail) {
		ADOPMAIL = adopmail;
	}

	public String getADPARKSVRIP() {
		return ADPARKSVRIP;
	}

	public void setADPARKSVRIP(String adparksvrip) {
		ADPARKSVRIP = adparksvrip;
	}

	public String getADPK() {
		return ADPK;
	}

	public void setADPK(String adpk) {
		ADPK = adpk;
	}

	public String getADPKRETRYTIMES() {
		return ADPKRETRYTIMES;
	}

	public void setADPKRETRYTIMES(String adpkretrytimes) {
		ADPKRETRYTIMES = adpkretrytimes;
	}

	public String getADPORTALJNDI() {
		return ADPORTALJNDI;
	}

	public void setADPORTALJNDI(String adportaljndi) {
		ADPORTALJNDI = adportaljndi;
	}

	public String getADPWD() {
		return ADPW;
	}

	public void setADPWD(String adpwd) {
		ADPW = adpwd;
	}

	public String getADRESENDTIMES() {
		return ADRESENDTIMES;
	}

	public void setADRESENDTIMES(String adresendtimes) {
		ADRESENDTIMES = adresendtimes;
	}

	public String getADSECMAIL() {
		return ADSECMAIL;
	}

	public void setADSECMAIL(String adsecmail) {
		ADSECMAIL = adsecmail;
	}

	public String getADSESSIONTO() {
		return ADSESSIONTO;
	}

	public void setADSESSIONTO(String adsessionto) {
		ADSESSIONTO = adsessionto;
	}

	public String getADSPMAIL() {
		return ADSPMAIL;
	}

	public void setADSPMAIL(String adspmail) {
		ADSPMAIL = adspmail;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

	public String getLASTUSER() {
		return LASTUSER;
	}

	public void setLASTUSER(String lastuser) {
		LASTUSER = lastuser;
	}
	
	public String getADPORTALURL() {
		return ADPORTALURL;
	}

	public void setADPORTALURL(String url) {
		ADPORTALURL = url;
	}
	
	public String getADPORTALURL_ADM() {
		return ADPORTALURL_ADM;
	}

	public void setADPORTALURL_ADM(String url) {
		ADPORTALURL_ADM = url;
	}	

	public String getADGDMAIL() {
		return ADGDMAIL;
	}

	public void setADGDMAIL(String adgdmail) {
		ADGDMAIL = adgdmail;
	}
	
	public String getADGDTXNMAIL() {
		return ADGDTXNMAIL;
	}

	public void setADGDTXNMAIL(String adgdtxnmail) {
		ADGDTXNMAIL = adgdtxnmail;
	}	
	
	public String getADGDSHH() {
		return ADGDSHH;
	}

	public void setADGDSHH(String adgdshh) {
		ADGDSHH = adgdshh;
	}

	public String getADGDSMM() {
		return ADGDSMM;
	}

	public void setADGDSMM(String adgdsmm) {
		ADGDSMM = adgdsmm;
	}

	public String getADGDSSS() {
		return ADGDSSS;
	}

	public void setADGDSSS(String adgdsss) {
		ADGDSSS = adgdsss;
	}

	public String getADGDEHH() {
		return ADGDEHH;
	}

	public void setADGDEHH(String adgdehh) {
		ADGDEHH = adgdehh;
	}

	public String getADGDEMM() {
		return ADGDEMM;
	}

	public void setADGDEMM(String adgdemm) {
		ADGDEMM = adgdemm;
	}

	public String getADGDESS() {
		return ADGDESS;
	}

	public void setADGDESS(String adgdess) {
		ADGDESS = adgdess;
	}
	public String getKYCDATE() {
		return KYCDATE;
	}

	public void setKYCDATE(String kycdate) {
		KYCDATE= kycdate;
	}		
	public String getSCN070() {
		return SCN070;
	}

	public void setSCN070(String scn070) {
		SCN070= scn070;
	}	
	public String getSCF002() {
		return SCF002;
	}

	public void setSCF002(String scf002) {
		SCF002= scf002;
	}
	public String getADFXE1HH() {
		return ADFXE1HH;
	}

	public void setADFXE1HH(String adfxehh) {
		ADFXE1HH = adfxehh;
	}

	public String getADFXE1MM() {
		return ADFXE1MM;
	}

	public void setADFXE1MM(String adfxemm) {
		ADFXE1MM = adfxemm;
	}

	public String getADFXE1SS() {
		return ADFXE1SS;
	}

	public void setADFXE1SS(String adfxess) {
		ADFXE1SS = adfxess;
	}

	public String getADFX1SHH() {
		return ADFX1SHH;
	}

	public void setADFX1SHH(String adfxshh) {
		ADFX1SHH = adfxshh;
	}

	public String getADFX1SMM() {
		return ADFX1SMM;
	}

	public void setADFX1SMM(String adfxsmm) {
		ADFX1SMM = adfxsmm;
	}

	public String getADFX1SSS() {
		return ADFX1SSS;
	}

	public void setADFX1SSS(String adfxsss) {
		ADFX1SSS = adfxsss;
	}	
}
