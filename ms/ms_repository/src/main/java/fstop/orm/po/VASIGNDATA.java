package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "VASIGNDATA")
public class VASIGNDATA implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long LOGID;
	
	private String CUSIDN;

	private byte[] PLAINTEXT;

	private byte[] SIGNEDTEXT;

	private String CHECKSIGN;

	private String IP;

	private String LASTDATE;
	
	private String LASTTIME;


	public Long getLOGID() {
		return LOGID;
	}

	public void setLOGID(Long logid) {
		LOGID = logid;
	}

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cusidn) {
		CUSIDN = cusidn;
	}
	
    public byte[] getPLAINTEXT() {
        return PLAINTEXT;
    }
 
    public void setPLAINTEXT(byte[] plaintext) {
        PLAINTEXT = plaintext;
    }
    
	public byte[] getSIGNEDTEXT() {
		return SIGNEDTEXT;
	}

	public void setSIGNEDTEXT(byte[] signedtext) {
		SIGNEDTEXT = signedtext;
	}

	public String getCHECKSIGN() {
		return CHECKSIGN;
	}

	public void setCHECKSIGN(String checksign) {
		CHECKSIGN = checksign;
	}

	public String getIP() {
		return IP;
	}

	public void setIP(String ip) {
		IP = ip;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}
}
