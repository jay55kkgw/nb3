package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "ADMAD")
public class ADMAD implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long ADID;

	private String ADUSERID = "";

	private String ADUSERNAME = "";

	private String ADTXDATE = "";

	private String ADTXTIME = "";

	private String ADSDATE = "";

	private String ADSTIME = "";

	private String ADEDATE = "";

	private String ADETIME = "";

	private String ADADTYPE = "";

	private String ADADHLK = "";

	private String ADADFROM = "";

	private String ADADCON1 = "";

	private String ADADCON2 = "";

	private String ADADCON3 = "";

	private String ADPICADD = "";

	private String ADADSTAT = "";

	private String ADCHRES = "";

	private String ADSUPNO = "";

	private String ADSUPNAME = "";

	private String ADSUPDATE = "";

	private String ADSUPTIME = "";

	private String ADADSERNO = "";
	
	public String toString() {
		return new ToStringBuilder(this).append("adid", getADID())
				.toString();
	}	

	public String getADADCON1() {
		return ADADCON1;
	}

	public void setADADCON1(String adadcon1) {
		ADADCON1 = adadcon1;
	}

	public String getADADCON2() {
		return ADADCON2;
	}

	public void setADADCON2(String adadcon2) {
		ADADCON2 = adadcon2;
	}

	public String getADADCON3() {
		return ADADCON3;
	}

	public void setADADCON3(String adadcon3) {
		ADADCON3 = adadcon3;
	}

	public String getADADFROM() {
		return ADADFROM;
	}

	public void setADADFROM(String adadfrom) {
		ADADFROM = adadfrom;
	}

	public String getADADHLK() {
		return ADADHLK;
	}

	public void setADADHLK(String adadhlk) {
		ADADHLK = adadhlk;
	}

	public String getADADSERNO() {
		return ADADSERNO;
	}

	public void setADADSERNO(String adadserno) {
		ADADSERNO = adadserno;
	}

	public String getADADSTAT() {
		return ADADSTAT;
	}

	public void setADADSTAT(String adadstat) {
		ADADSTAT = adadstat;
	}

	public String getADADTYPE() {
		return ADADTYPE;
	}

	public void setADADTYPE(String adadtype) {
		ADADTYPE = adadtype;
	}

	public String getADCHRES() {
		return ADCHRES;
	}

	public void setADCHRES(String adchres) {
		ADCHRES = adchres;
	}

	public String getADEDATE() {
		return ADEDATE;
	}

	public void setADEDATE(String adedate) {
		ADEDATE = adedate;
	}

	public String getADETIME() {
		return ADETIME;
	}

	public void setADETIME(String adetime) {
		ADETIME = adetime;
	}

	public Long getADID() {
		return ADID;
	}

	public void setADID(Long adid) {
		ADID = adid;
	}

	public String getADPICADD() {
		return ADPICADD;
	}

	public void setADPICADD(String adpicadd) {
		ADPICADD = adpicadd;
	}

	public String getADSDATE() {
		return ADSDATE;
	}

	public void setADSDATE(String adsdate) {
		ADSDATE = adsdate;
	}

	public String getADSTIME() {
		return ADSTIME;
	}

	public void setADSTIME(String adstime) {
		ADSTIME = adstime;
	}

	public String getADSUPDATE() {
		return ADSUPDATE;
	}

	public void setADSUPDATE(String adsupdate) {
		ADSUPDATE = adsupdate;
	}

	public String getADSUPNAME() {
		return ADSUPNAME;
	}

	public void setADSUPNAME(String adsupname) {
		ADSUPNAME = adsupname;
	}

	public String getADSUPNO() {
		return ADSUPNO;
	}

	public void setADSUPNO(String adsupno) {
		ADSUPNO = adsupno;
	}

	public String getADSUPTIME() {
		return ADSUPTIME;
	}

	public void setADSUPTIME(String adsuptime) {
		ADSUPTIME = adsuptime;
	}

	public String getADTXDATE() {
		return ADTXDATE;
	}

	public void setADTXDATE(String adtxdate) {
		ADTXDATE = adtxdate;
	}

	public String getADTXTIME() {
		return ADTXTIME;
	}

	public void setADTXTIME(String adtxtime) {
		ADTXTIME = adtxtime;
	}

	public String getADUSERID() {
		return ADUSERID;
	}

	public void setADUSERID(String aduserid) {
		ADUSERID = aduserid;
	}

	public String getADUSERNAME() {
		return ADUSERNAME;
	}

	public void setADUSERNAME(String adusername) {
		ADUSERNAME = adusername;
	}
}
