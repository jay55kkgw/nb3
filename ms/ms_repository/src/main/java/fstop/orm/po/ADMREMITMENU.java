package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "ADMREMITMENU")
public class ADMREMITMENU implements Serializable { 

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long ADSEQNO;

	private String ADRMTTYPE = "";
	private String ADRMTID = "";
	private String ADLKINDID = "";
	private String ADMKINDID = "";
	private String ADRMTITEM = "";
	private String ADRMTDESC = "";
	private String ADCHKMK = "";
	private String ADRMTEETYPE1 = "";
	private String ADRMTEETYPE2 = "";
	private String ADRMTEETYPE3 = "";
	private String LASTUSER = "";
	private String LASTDATE = "";
	private String LASTTIME = "";


	public String toString() {
		return new ToStringBuilder(this).append("adseqno", getADSEQNO())
				.toString();
	}


	public String getADCHKMK() {
		return ADCHKMK;
	}


	public void setADCHKMK(String adchkmk) {
		ADCHKMK = adchkmk;
	}


	public String getADLKINDID() {
		return ADLKINDID;
	}


	public void setADLKINDID(String adlkindid) {
		ADLKINDID = adlkindid;
	}


	public String getADMKINDID() {
		return ADMKINDID;
	}


	public void setADMKINDID(String admkindid) {
		ADMKINDID = admkindid;
	}


	public String getADRMTDESC() {
		return ADRMTDESC;
	}


	public void setADRMTDESC(String adrmtdesc) {
		ADRMTDESC = adrmtdesc;
	}


	public String getADRMTEETYPE1() {
		return ADRMTEETYPE1;
	}


	public void setADRMTEETYPE1(String adrmteetype1) {
		ADRMTEETYPE1 = adrmteetype1;
	}


	public String getADRMTEETYPE2() {
		return ADRMTEETYPE2;
	}


	public void setADRMTEETYPE2(String adrmteetype2) {
		ADRMTEETYPE2 = adrmteetype2;
	}


	public String getADRMTEETYPE3() {
		return ADRMTEETYPE3;
	}


	public void setADRMTEETYPE3(String adrmteetype3) {
		ADRMTEETYPE3 = adrmteetype3;
	}


	public String getADRMTID() {
		return ADRMTID;
	}


	public void setADRMTID(String adrmtid) {
		ADRMTID = adrmtid;
	}


	public String getADRMTITEM() {
		return ADRMTITEM;
	}


	public void setADRMTITEM(String adrmtitem) {
		ADRMTITEM = adrmtitem;
	}


	public String getADRMTTYPE() {
		return ADRMTTYPE;
	}


	public void setADRMTTYPE(String adrmttype) {
		ADRMTTYPE = adrmttype;
	}


	public Long getADSEQNO() {
		return ADSEQNO;
	}


	public void setADSEQNO(Long adseqno) {
		ADSEQNO = adseqno;
	}


	public String getLASTDATE() {
		return LASTDATE;
	}


	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}


	public String getLASTTIME() {
		return LASTTIME;
	}


	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}


	public String getLASTUSER() {
		return LASTUSER;
	}


	public void setLASTUSER(String lastuser) {
		LASTUSER = lastuser;
	}


}
