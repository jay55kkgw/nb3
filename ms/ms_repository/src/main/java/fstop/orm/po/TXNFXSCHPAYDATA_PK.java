package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Embeddable;

import lombok.Data;

@Embeddable
@Data
public class TXNFXSCHPAYDATA_PK implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3519383984459541388L;
	/**
	 * 
	 */
	
	
	private String FXSCHNO = "";		//預約批號
	private String FXSCHTXDATE = "";//預約轉帳日	
	private String FXUSERID ="";//使用者ID


}
