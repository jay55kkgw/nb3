package fstop.orm.po;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

import lombok.Data;

@Entity
@Table(name = "SYSDAILYSEQ")
@Data
public class SYSDAILYSEQ {
	@Id
	private String ADSEQID;

	private Integer ADSEQ;

	private String ADSEQMEMO;


	@Override
	public String toString() {
		return "SYSDAILYSEQ [ADSEQID=" + ADSEQID + "]";
	}
}
