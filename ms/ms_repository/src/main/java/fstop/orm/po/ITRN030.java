package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

import lombok.Data;

@Entity
@Table(name = "ITRN030")
@Data
public class ITRN030 implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1161004147613165466L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String RECNO;

	private String HEADER;

	private String SEQ;

	private String DATE;

	private String TIME;

	private String OFCRY;
	
	private String CRY;
	
	private String BUYITR;
	
	private String SELITR;
	
	private String TERM;

	private String FILL;

	@Override
	public String toString() {
		return "ITRN030 [RECNO=" + RECNO + ", HEADER=" + HEADER + ", SEQ=" + SEQ + ", DATE=" + DATE + ", TIME=" + TIME
				+ ", OFCRY=" + OFCRY + ", CRY=" + CRY + ", BUYITR=" + BUYITR + ", SELITR=" + SELITR + ", TERM=" + TERM
				+ ", FILL=" + FILL + "]";
	}
}
