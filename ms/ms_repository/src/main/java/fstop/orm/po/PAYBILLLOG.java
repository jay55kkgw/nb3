package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PAYBILLLOG")
public class PAYBILLLOG implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3386121115490019577L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long LOGID;

	private String CUSIDN = "";

	private String CARDNUM = "";

	private String PAYSOURCE = "";

	private String PAYAMT = "";

	private String BANKID = "";

	private String ACCOUNT = "";

	private String ADMCODE = "";

	private String REMOTEIP = "";

	private String LOGDATE = "";

	private String LOGTIME = "";

	public String getADMCODE() {
		return ADMCODE;
	}

	public void setADMCODE(String admcode) {
		ADMCODE = admcode == null ? "" : admcode.trim();
	}

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cusidn) {
		CUSIDN = cusidn == null ? "" : cusidn.trim();
	}

	public String getCARDNUM() {
		return CARDNUM;
	}

	public void setCARDNUM(String cardnum) {
		CARDNUM = cardnum == null ? "" : cardnum.trim();
	}

	public String getPAYSOURCE() {
		return PAYSOURCE;
	}

	public void setPAYSOURCE(String paysource) {
		PAYSOURCE = paysource == null ? "" : paysource.trim();
	}

	public String getLOGDATE() {
		return LOGDATE;
	}

	public void setLOGDATE(String logdate) {
		LOGDATE = logdate == null ? "" : logdate.trim();
	}

	public Long getLOGID() {
		return LOGID;
	}

	public void setLOGID(Long logid) {
		LOGID = logid;
	}

	public String getLOGTIME() {
		return LOGTIME;
	}

	public void setLOGTIME(String logtime) {
		LOGTIME = logtime == null ? "" : logtime.trim();
	}

	public String getPAYAMT() {
		return PAYAMT;
	}

	public void setPAYAMT(String payamt) {
		PAYAMT = payamt == null ? "" : payamt.trim();
	}

	public String getREMOTEIP() {
		return REMOTEIP;
	}

	public void setREMOTEIP(String remoteip) {
		REMOTEIP = remoteip == null ? "" : remoteip.trim();
	}

	public String getBANKID() {
		return BANKID;
	}

	public void setBANKID(String bankid) {
		BANKID = bankid == null ? "" : bankid.trim();
	}

	public String getACCOUNT() {
		return ACCOUNT;
	}

	public void setACCOUNT(String account) {
		ACCOUNT = account == null ? "" : account.trim();
	}

}
