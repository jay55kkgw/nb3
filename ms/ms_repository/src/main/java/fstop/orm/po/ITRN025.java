package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

import lombok.Data;

@Entity
@Table(name = "ITRN025")
@Data
public class ITRN025 implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2294443346901314801L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String RECNO;

	private String HEADER;

	private String SEQ;

	private String DATE;

	private String TIME;

	private String COUNT;
	
	private String COLOR;
	
	private String ACC;
	
	private String ITR1;
	
	private String MARK;

	private String ITR2;
	
	private String FILL;

	@Override
	public String toString() {
		return "ITRN025 [RECNO=" + RECNO + ", HEADER=" + HEADER + ", SEQ=" + SEQ + ", DATE=" + DATE + ", TIME=" + TIME
				+ ", COUNT=" + COUNT + ", COLOR=" + COLOR + ", ACC=" + ACC + ", ITR1=" + ITR1 + ", MARK=" + MARK
				+ ", ITR2=" + ITR2 + ", FILL=" + FILL + "]";
	}
	
}
