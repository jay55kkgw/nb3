package fstop.orm.po;


import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 批次JOB狀態控制檔
 */
@Entity
@Table(name = "SYSBATCHSTS")
@Data
public class SYSBATCHSTS implements Serializable {

    @Id
    private String ADGUID; // guid識別碼 varchar(50) PRIMARY KEY NOT NULL,
    private String ADBATID; // 批次作業ID varchar(10) NOT NULL,
    private String ADBATSDATE; // 批次開始日期 char(8) DEFAULT '' NOT NULL,
    private String ADBATSTIME; // 批次開始時間 char(6) DEFAULT '' NOT NULL,
    private String ADOPID; // 交易代號(可用電文代號或也可自定) varchar(16) DEFAULT '' NOT NULL,
    private String BATFILEFLAG; //是否有產整批檔 Y 有/ N 無(預設) char(1) DEFAULT '0' NOT NULL,
    private String TOTALCOUNT; // 整批檔總筆數, 右靠左補零 char(6) DEFAULT '' NOT NULL,
    private String ADBATFILENAME; // 提出檔案名稱 varchar(50) DEFAULT '' NOT NULL,
    private String ADBATEDATE; // 批次結束日期 char(8) DEFAULT '' NOT NULL,
    private String ADBATETIME; // 批次結束時間 char(6) DEFAULT '' NOT NULL,
    private String ADBATSTATUS; // 狀態 char(1) DEFAULT '0' NOT NULL,
    private String ADERRMSG; // 異外異外文字 varchar(1000) DEFAULT '' NOT NULL
}
