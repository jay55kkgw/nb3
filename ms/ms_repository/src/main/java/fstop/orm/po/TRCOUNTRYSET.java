package fstop.orm.po;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "TRCOUNTRYSET")
public class TRCOUNTRYSET implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long ADSEQNO;
	private String IDNO;

	private String CNTR_ID;

	private String LASTUSER;

	private String LASTDATE;

	private String LASTTIME;

	public String toString() {
		return new ToStringBuilder(this).append("adseqno", getADSEQNO())
				.toString();
	}
	public Long getADSEQNO() {
		return ADSEQNO;
	}


	public void setADSEQNO(Long adseqno) {
		ADSEQNO = adseqno;
	}	
	public String getIDNO() {
		return IDNO;
	}

	public void setIDNO(String idno) {
		IDNO = idno;
	}

	public String getCNTR_ID() {
		return CNTR_ID;
	}

	public void setCNTR_ID(String cntr_id) {
		CNTR_ID = cntr_id;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

	public String getLASTUSER() {
		return LASTUSER;
	}

	public void setLASTUSER(String lastuser) {
		LASTUSER = lastuser;
	}

}
