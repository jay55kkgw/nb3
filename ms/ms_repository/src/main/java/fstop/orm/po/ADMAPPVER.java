package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
//手機版本控管
@Entity
@Table(name = "ADMAPPVER")
public class ADMAPPVER implements Serializable{
	
	private Integer APPID;	    //  ID
	
	private String USERID;		//  使用者代號
	
	private String USERNAME;	//  使用者名稱
	
	private String TXDATE;		//  交易日期
	
	private String TXTIME;		//  交易時間
	
	private String APPOS;		//  手機系統
	
	private String APPMODELNO;	//  手機型號
	
	private String VERNO;		//  版號
	
	private String VERDATE;	//  版本日期
	
	private String VERSTATUS;	//  版本狀態
	
	private String VERMSG;		//  提示訊息
	
	private String VERURL;		//  下載網址
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getAPPID() {
		return APPID;
	}
	public void setAPPID(Integer appid) {
		APPID = appid;
	}
	public String getUSERID() {
		return USERID;
	}
	public void setUSERID(String userid) {
		USERID = userid;
	}
	public String getUSERNAME() {
		return USERNAME;
	}
	public void setUSERNAME(String username) {
		USERNAME = username;
	}
	public String getTXDATE() {
		return TXDATE;
	}
	public void setTXDATE(String txdate) {
		TXDATE = txdate;
	}
	public String getTXTIME() {
		return TXTIME;
	}
	public void setTXTIME(String txtime) {
		TXTIME = txtime;
	}
	public String getAPPOS() {
		return APPOS;
	}
	public void setAPPOS(String appos) {
		APPOS = appos;
	}
	public String getAPPMODELNO() {
		return APPMODELNO;
	}
	public void setAPPMODELNO(String elNO) {
		APPMODELNO = elNO;
	}
	public String getVERNO() {
		return VERNO;
	}
	public void setVERNO(String verno) {
		VERNO = verno;
	}
	public String getVERDATE() {
		return VERDATE;
	}
	public void setVERDATE(String verdate) {
		VERDATE = verdate;
	}
	public String getVERSTATUS() {
		return VERSTATUS;
	}
	public void setVERSTATUS(String stATUS) {
		VERSTATUS = stATUS;
	}
	public String getVERMSG() {
		return VERMSG;
	}
	public void setVERMSG(String vermsg) {
		VERMSG = vermsg;
	}
	public String getVERURL() {
		return VERURL;
	}
	public void setVERURL(String verurl) {
		VERURL = verurl;
	}


}
