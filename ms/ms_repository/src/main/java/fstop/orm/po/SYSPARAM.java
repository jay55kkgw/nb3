package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "SYSPARAM")
@Data
public class SYSPARAM{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer ADPARAMID;

	private String ADPARAMNAME;

	private String ADPARAMVALUE;

	private String ADPARAMMEMO;

}
