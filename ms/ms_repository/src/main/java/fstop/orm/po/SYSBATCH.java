package fstop.orm.po;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 批次作業執行紀錄檔
 */
@Entity
@Table(name = "SYSBATCH")
@Data
public class SYSBATCH implements Serializable {

	
	//@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private String ADBATID;  //批次作業ID（程式名稱） varchar(50) PRIMARY KEY NOT NULL,

//	private String ADBATRUN;

	private String ADBATNAME; // 批次中文名稱 varchar(120) NOT NULL,
	private String ADBATSDATE; // 批次執行日期 char(8) DEFAULT '' NOT NULL,
	private String ADBATSTIME; // 批次執行時間 char(6) DEFAULT '' NOT NULL,
	private String ADBATEDATE; // 批次結束日期 char(8) DEFAULT '' NOT NULL,
	private String ADBATETIME; // 批次結束時間 char(6) DEFAULT '' NOT NULL,
	private String LOCKSTATUS; // （暫不使用）鎖定 Y: 鎖定執行中， N: 未執行/ 執行結束 char(1) DEFAULT 'N' NOT NULL,
	private String INVALIDFLAG; // （暫不使用）啟用 / 取消 Y: 有效， N: 己停用 char(1) DEFAULT 'Y' NOT NULL
	private String PEROID; //Ex: 每天8:00~17:00 每半小時執行
	private String EXECUTIONTIME; // Default 0
	private String EXECMESSAGE; //'': 執行成功 非'': 錯誤訊息


}
