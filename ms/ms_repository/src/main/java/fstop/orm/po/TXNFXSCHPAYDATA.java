package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "TXNFXSCHPAYDATA")
@Data
public class TXNFXSCHPAYDATA implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7032373023952252161L;

	@Id
	private TXNFXSCHPAYDATA_PK pks;// 複合組鍵
	
	//移至pk
//	private String FXUSERID = "";// 使用者ID

	private String FXEFEECCY = "";// 手續費幣別

	private String FXTELFEE = "";// 郵電費

	private String FXOURCHG = "";// 國外費用

	private String FXEXRATE = "";// 匯率

	private String FXTXSTATUS = "";// 交易執行狀態0成功 : 1失敗 :2其他(處理中)

	private String ADOPID = "";// 操作功能ID

	private String FXWDAC = "";// 轉出帳號

	private String FXWDCURR = "";// 轉出幣別

	private String FXWDAMT = "";// 轉出金額

	private String FXSVBH = "";// 轉入行庫代碼

	private String FXSVAC = "";// 轉入帳號/繳費代號

	private String FXSVCURR = "";// 轉入幣別

	private String FXSVAMT = "";// 轉入金額

	private String FXTXMEMO = "";// 備註

	private String FXTXMAILS = "";// 發送Mail清單

	private String FXTXMAILMEMO = "";// Mail備註

	private String FXTXCODE = "";// 交易機制

	private String XMLCA = "";// CA 識別碼

	private String XMLCN = "";// 憑證ＣＮ

	private String MAC = "";

	private String FXTXDATE = "";// 實際轉帳日期

	private String FXTXTIME = "";// 實際轉帳時間

	private String PCSEQ = "";// 交易序號

	private String ADTXNO = "";// 中心主機交易辨識序號，防主機二扣使用

	private String FXTITAINFO = "";// 交易上行資訊

	private String FXTOTAINFO = "";// 交易下行資訊

	private String FXEFEE = "";// 手續費

	private String FXEXCODE = "";// 主機回應碼/交易錯誤代碼

	private String FXRESEND = "";// 重送次數

	private String FXMSGSEQNO = "";// 電文傳送狀態

	private String FXCERT = "";// 交易單據

	private String LASTDATE = "";// 最後異動日期

	private String LASTTIME = "";// 最後異動時間
	
}
