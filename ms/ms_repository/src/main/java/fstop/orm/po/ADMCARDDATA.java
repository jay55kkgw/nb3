package fstop.orm.po;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ADMCARDDATA")
public class ADMCARDDATA implements Serializable {

	@Id
	private String CARDNO;

	private String CARDTYPE;

	private String CARDNAME;

	private String CARDMEMO;

	private String CARDATTR;


	public String getCARDNO() {
		return CARDNO;
	}

	public void setCARDNO(String cardno) {
		CARDNO = cardno;
	}

	public String getCARDTYPE() {
		return CARDTYPE;
	}

	public void setCARDTYPE(String cardtype) {
		CARDTYPE = cardtype;
	}

	public String getCARDNAME() {
		return CARDNAME;
	}

	public void setCARDNAME(String cardname) {
		CARDNAME = cardname;
	}

	public String getCARDMEMO() {
		return CARDMEMO;
	}

	public void setCARDMEMO(String cardmemo) {
		CARDMEMO = cardmemo;
	}

	public String getCARDATTR() {
		return CARDATTR;
	}

	public void setCARDATTR(String cardattr) {
		CARDATTR = cardattr;
	}	
}
