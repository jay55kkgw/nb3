package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TXNSMSLOG")
public class TXNSMSLOG implements Serializable  {

	@Id
	private String ADTXNO;
	private String ADUSERID;
	private String PHONE;
	private String MSGID;
	private String STATUSCODE;
	private String LASTDATE;
	private String LASTTIME;
	private String ADOPID;
	
	public String getADTXNO() {
		return ADTXNO;
	}
	public void setADTXNO(String adtxno) {
		ADTXNO = adtxno;
	}
	public String getADUSERID() {
		return ADUSERID;
	}
	public void setADUSERID(String aduserid) {
		ADUSERID = aduserid;
	}
	public String getLASTDATE() {
		return LASTDATE;
	}
	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}
	public String getLASTTIME() {
		return LASTTIME;
	}
	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}
	public String getMSGID() {
		return MSGID;
	}
	public void setMSGID(String msgid) {
		MSGID = msgid;
	}
	public String getPHONE() {
		return PHONE;
	}
	public void setPHONE(String phone) {
		PHONE = phone;
	}
	public String getSTATUSCODE() {
		return STATUSCODE;
	}
	public void setSTATUSCODE(String statuscode) {
		STATUSCODE = statuscode;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String adopid) {
		ADOPID = adopid;
	}	
}
