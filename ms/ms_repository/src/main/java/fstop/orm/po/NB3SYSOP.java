package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

import lombok.Data;

@Entity
@Table(name = "NB3SYSOP")
@Data
public class NB3SYSOP implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8905384155005175428L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String DPACCSETID;

	private String ADOPID;

	private String ADOPNAME;

	private String ADOPENGNAME;

	private String ADOPCHSNAME;
	
	private String ADOPMEMO;

	private String ADOPALIVE;

	private String LASTUSER;

	private String LASTDATE;

	private String LASTTIME;
	
	private String ADSEQ;
	
	private String ADOPGROUPID;
	
	private String ISANONYMOUS;
	
	private String URL;
	
	private String ISPOPUP;
	
	private String ADOPAUTHTYPE;

	@Override
	public String toString() {
		return "NB3SYSOP [DPACCSETID=" + DPACCSETID + ", ADOPID=" + ADOPID + ", ADOPNAME=" + ADOPNAME + ", ADOPENGNAME="
				+ ADOPENGNAME + ", ADOPCHSNAME=" + ADOPCHSNAME + ", ADOPMEMO=" + ADOPMEMO + ", ADOPALIVE=" + ADOPALIVE
				+ ", LASTUSER=" + LASTUSER + ", LASTDATE=" + LASTDATE + ", LASTTIME=" + LASTTIME + ", ADSEQ=" + ADSEQ
				+ ", ADOPGROUPID=" + ADOPGROUPID + ", ISANONYMOUS=" + ISANONYMOUS + ", URL=" + URL + ", ISPOPUP="
				+ ISPOPUP + ", ADOPAUTHTYPE=" + ADOPAUTHTYPE + "]";
	}

	

	
}
