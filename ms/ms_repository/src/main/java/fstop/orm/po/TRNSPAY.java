package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TRNSPAY")
public class TRNSPAY implements Serializable{

	@Id
	private int API_TXN_NO;

	private String TXN_DATETIME;

	private String MTI;

	private String PCODE;

	private String RCODE;

	private String RCODE_DESC;

	private String SRC_ID;

	private String FEE_REF_ID;

	private String SESSION_ID;

	private String HC;

	private String TXN_AMOUNT;

	private String STAN;

	private String PAYTYPE;

	private String TRABANKID;

	private String TRAACCOUNT;

	private String PAYIDNBAN;

	private String TRNSCODE;

	public int getAPI_TXN_NO() {
		return API_TXN_NO;
	}

	public String getTXN_DATETIME() {
		return TXN_DATETIME;
	}

	public String getMTI() {
		return MTI;
	}

	public String getPCODE() {
		return PCODE;
	}

	public String getRCODE() {
		return RCODE;
	}

	public String getRCODE_DESC() {
		return RCODE_DESC;
	}

	public String getSRC_ID() {
		return SRC_ID;
	}

	public String getFEE_REF_ID() {
		return FEE_REF_ID;
	}

	public String getSESSION_ID() {
		return SESSION_ID;
	}

	public String getHC() {
		return HC;
	}

	public String getTXN_AMOUNT() {
		return TXN_AMOUNT;
	}

	public String getSTAN() {
		return STAN;
	}

	public String getPAYTYPE() {
		return PAYTYPE;
	}

	public String getTRABANKID() {
		return TRABANKID;
	}

	public String getTRAACCOUNT() {
		return TRAACCOUNT;
	}

	public String getPAYIDNBAN() {
		return PAYIDNBAN;
	}

	public String getTRNSCODE() {
		return TRNSCODE;
	}

	public void setAPI_TXN_NO(int aPI_TXN_NO) {
		API_TXN_NO = aPI_TXN_NO;
	}

	public void setTXN_DATETIME(String tXN_DATETIME) {
		TXN_DATETIME = tXN_DATETIME;
	}

	public void setMTI(String mTI) {
		MTI = mTI;
	}

	public void setPCODE(String pCODE) {
		PCODE = pCODE;
	}

	public void setRCODE(String rCODE) {
		RCODE = rCODE;
	}

	public void setRCODE_DESC(String rCODE_DESC) {
		RCODE_DESC = rCODE_DESC;
	}

	public void setSRC_ID(String sRC_ID) {
		SRC_ID = sRC_ID;
	}

	public void setFEE_REF_ID(String fEE_REF_ID) {
		FEE_REF_ID = fEE_REF_ID;
	}

	public void setSESSION_ID(String sESSION_ID) {
		SESSION_ID = sESSION_ID;
	}

	public void setHC(String hC) {
		HC = hC;
	}

	public void setTXN_AMOUNT(String tXN_AMOUNT) {
		TXN_AMOUNT = tXN_AMOUNT;
	}

	public void setSTAN(String sTAN) {
		STAN = sTAN;
	}

	public void setPAYTYPE(String pAYTYPE) {
		PAYTYPE = pAYTYPE;
	}

	public void setTRABANKID(String tRABANKID) {
		TRABANKID = tRABANKID;
	}

	public void setTRAACCOUNT(String tRAACCOUNT) {
		TRAACCOUNT = tRAACCOUNT;
	}

	public void setPAYIDNBAN(String pAYIDNBAN) {
		PAYIDNBAN = pAYIDNBAN;
	}

	public void setTRNSCODE(String tRNSCODE) {
		TRNSCODE = tRNSCODE;
	}
}
