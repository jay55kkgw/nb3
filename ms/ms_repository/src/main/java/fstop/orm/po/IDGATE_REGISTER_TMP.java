package fstop.orm.po;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

import lombok.Data;

@Entity
@Table(name = "IDGATE_REGISTER_TMP")
@Data
public class IDGATE_REGISTER_TMP implements Serializable {

	

	/**
	 * 
	 */
	private static final long serialVersionUID = 4437780126447880204L;
	@Id
	private String SESSIONID;
	private String DEVICEID;
	private String DEVICENAME;
	private String DEVICETYPE;
	private String DEVICEBRAND;
	private String DEVICEOS;
	private String LASTDATE;
	private String LASTTIME;
	private String CUSIDN;
	private String OPNCODE;
	private String ITEM;

	
}
