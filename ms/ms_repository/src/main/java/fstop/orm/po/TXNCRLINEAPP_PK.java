package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Embeddable;

import lombok.Data;

@Embeddable
@Data
public class TXNCRLINEAPP_PK  implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1190821716205547874L;
	
	private String CPRIMDATE;
	private String CPRIMID;
	private String CARDNUM;
}
