package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TXNCASHAPPLY")
public class TXNCASHAPPLY implements Serializable {

	@Id
	private String RCVNO = "";
	private String USERNAME = "";
	private String EMAIL  = "";
	private String ACCOUNT  = "";
	private String BRANCHID  = "";
	private Integer NUMTWOK; 
	private Integer NUMONEK;
	private Integer NUMFIVEH;
	private Integer NUMTWOH;
	private Integer NUMONEH;
	private Integer NUMTWENTY;
	private Integer AMOUNT;
	private String USERIP = "";
	private String LASTDATE = "";
	private String LASTTIME = "";
	
	public String getRCVNO() {
		return RCVNO;
	}

	public void setRCVNO(String rcvno) {
		RCVNO = rcvno;
	}

	public String getUSERNAME() {
		return USERNAME;
	}

	public void setUSERNAME(String username) {
		USERNAME = username;
	}

	public String getEMAIL() {
		return EMAIL;
	}

	public void setEMAIL(String email) {
		EMAIL = email;
	}

	public String getACCOUNT() {
		return ACCOUNT;
	}

	public void setACCOUNT(String account) {
		ACCOUNT = account;
	}

	public String getBRANCHID() {
		return BRANCHID;
	}

	public void setBRANCHID(String branchid) {
		BRANCHID = branchid;
	}

	public Integer getNUMTWOK() {
		return NUMTWOK;
	}

	public void setNUMTWOK(Integer numtwok) {
		NUMTWOK = numtwok;
	}

	public Integer getNUMONEK() {
		return NUMONEK;
	}

	public void setNUMONEK(Integer numonek) {
		NUMONEK = numonek;
	}

	public Integer getNUMFIVEH() {
		return NUMFIVEH;
	}

	public void setNUMFIVEH(Integer numfiveh) {
		NUMFIVEH = numfiveh;
	}

	public Integer getNUMTWOH() {
		return NUMTWOH;
	}

	public void setNUMTWOH(Integer numtwoh) {
		NUMTWOH = numtwoh;
	}

	public Integer getNUMONEH() {
		return NUMONEH;
	}

	public void setNUMONEH(Integer numoneh) {
		NUMONEH = numoneh;
	}

	public Integer getNUMTWENTY() {
		return NUMTWENTY;
	}

	public void setNUMTWENTY(Integer numtwenty) {
		NUMTWENTY = numtwenty;
	}	

	public Integer getAMOUNT() {
		return AMOUNT;
	}

	public void setAMOUNT(Integer amount) {
		AMOUNT = amount;
	}	

	public String getUSERIP() {
		return USERIP;
	}

	public void setUSERIP(String userip) {
		USERIP = userip;
	}	
	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}
	
	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}
}
