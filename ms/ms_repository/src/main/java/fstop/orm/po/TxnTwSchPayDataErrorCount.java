package fstop.orm.po;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
public class TxnTwSchPayDataErrorCount {
	@Id
	private String DPEXCODE;
	private String DPEXCODEMSG;
	private String TIMES;
}
