package fstop.orm.po;

import lombok.Data;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class TXNTWSCHPAYDATA_PK implements Serializable {

	private static final long serialVersionUID = 1914490891589621321L;

	private String DPSCHNO = "";	//預約批號Guid(36位)
	
	private String DPSCHTXDATE = "";//預約轉帳日
	
	private String DPUSERID = "";//使用者ID 
	

	public TXNTWSCHPAYDATA_PK() {
	}


	public TXNTWSCHPAYDATA_PK(String dPSCHNO, String dPSCHTXDATE , String dPUSERID) {
		super();
		DPSCHNO = dPSCHNO;
		DPSCHTXDATE = dPSCHTXDATE;
		DPUSERID = dPUSERID;
	}


	public String getDPSCHNO() {
		return DPSCHNO;
	}


	public void setDPSCHNO(String dPSCHNO) {
		DPSCHNO = dPSCHNO;
	}


	public String getDPSCHTXDATE() {
		return DPSCHTXDATE;
	}


	public void setDPSCHTXDATE(String dPSCHTXDATE) {
		DPSCHTXDATE = dPSCHTXDATE;
	}


	public String getDPUSERID() {
		return DPUSERID;
	}


	public void setDPUSERID(String dPUSERID) {
		DPUSERID = dPUSERID;
	}

//
//	@Override
//	public int hashCode() {
//		final int prime = 31;
//		int result = 1;
//		result = prime * result + ((DPSCHNO == null) ? 0 : DPSCHNO.hashCode());
//		result = prime * result + ((DPSCHTXDATE == null) ? 0 : DPSCHTXDATE.hashCode());
//		return result;
//	}
//
//
//	@Override
//	public boolean equals(Object obj) {
//		if (this == obj)
//			return true;
//		if (obj == null)
//			return false;
//		if (getClass() != obj.getClass())
//			return false;
//		TXNTWSCHPAYDATA_PK other = (TXNTWSCHPAYDATA_PK) obj;
//		if (DPSCHNO == null) {
//			if (other.DPSCHNO != null)
//				return false;
//		} else if (!DPSCHNO.equals(other.DPSCHNO))
//			return false;
//		if (DPSCHTXDATE == null) {
//			if (other.DPSCHTXDATE != null)
//				return false;
//		} else if (!DPSCHTXDATE.equals(other.DPSCHTXDATE))
//			return false;
//		return true;
//	}
//
//


}
