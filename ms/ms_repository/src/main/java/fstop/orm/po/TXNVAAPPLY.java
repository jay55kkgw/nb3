package fstop.orm.po;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TXNVAAPPLY")
public class TXNVAAPPLY implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long LOGID;

	private String USERID = "";

	private String STATUS = "";

	private String VERSION = "";

	private String MSGCODE = "";

	private String IP = "";

	private String LASTDATE = "";

	private String LASTTIME = "";

	public String getUSERID() {
		return USERID;
	}

	public void setUSERID(String userid) {
		USERID = userid;
	}

	public String getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(String status) {
		STATUS = status;
	}

	public String getVERSION() {
		return VERSION;
	}

	public void setVERSION(String version) {
		VERSION = version;
	}

	public String getMSGCODE() {
		return MSGCODE;
	}

	public void setMSGCODE(String msgcode) {
		MSGCODE = msgcode;
	}

	public String getIP() {
		return IP;
	}

	public void setIP(String ip) {
		IP = ip;
	}

	public Long getLOGID() {
		return LOGID;
	}

	public void setLOGID(Long logid) {
		LOGID = logid;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}
}
