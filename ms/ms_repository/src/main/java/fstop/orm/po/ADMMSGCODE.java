package fstop.orm.po;

import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "ADMMSGCODE")
public class ADMMSGCODE implements Serializable {

	@Id
	private String ADMCODE;

	private String ADMRESEND = "";//可否提供台幣類交易人工重送 Y:可人工重送N:否

	private String ADMEXCE = "";   //需列入異常事件通知否

	private String ADMSGIN = ""; //訊息說明

	private String ADMSGOUT = "";//客戶訊息內容

	private String LASTUSER = "";//最後修改者

	private String LASTDATE = "";//最後修改日

	private String LASTTIME = "";//最後修改時間

	private String ADMRESENDFX = "";//可否提供外幣類交易人工重送  Y:可人工重送N:否

	private String ADMAUTOSEND = "";//可否提供台幣類交易自動重送  Y:可自動重送N:否
	
	private String ADMAUTOSENDFX = "";//可否提供外幣類交易自動重送	Y:可自動重送N:否
	
	public String toString() {
		return new ToStringBuilder(this).append("admcode", getADMCODE())
				.toString();
	}

	public String getADMCODE() {
		return ADMCODE;
	}

	public void setADMCODE(String admcode) {
		ADMCODE = admcode;
	}

	public String getADMEXCE() {
		return ADMEXCE;
	}

	public void setADMEXCE(String admexce) {
		ADMEXCE = admexce;
	}

	public String getADMRESEND() {
		return ADMRESEND;
	}

	public void setADMRESEND(String admresend) {
		ADMRESEND = admresend;
	}

	public String getADMSGIN() {
		return ADMSGIN;
	}

	public void setADMSGIN(String admsgin) {
		ADMSGIN = admsgin;
	}

	public String getADMSGOUT() {
		return ADMSGOUT;
	}

	public void setADMSGOUT(String admsgout) {
		ADMSGOUT = admsgout;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

	public String getLASTUSER() {
		return LASTUSER;
	}

	public void setLASTUSER(String lastuser) {
		LASTUSER = lastuser;
	}

	public String getADMRESENDFX() {
		return ADMRESENDFX;
	}

	public void setADMRESENDFX(String admresendfx) {
		ADMRESENDFX = admresendfx;
	}
	
	public String getADMAUTOSEND() {
		return ADMAUTOSEND;
	}

	public void setADMAUTOSEND(String admautosend) {
		ADMAUTOSEND = admautosend;
	}
	
	public String getADMAUTOSENDFX() {
		return ADMAUTOSENDFX;
	}

	public void setADMAUTOSENDFX(String admautosendfx) {
		ADMAUTOSENDFX = admautosendfx;
	}	
}
