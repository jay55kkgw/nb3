package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "TXNCRLINEAPP")
@Data
public class TXNCRLINEAPP implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5072410764396595949L;

	@Id
	TXNCRLINEAPP_PK pks;
	
	private String CPRIMTIME;
	private String CPRIMCHNAME;
	private String OCRLINE;
	private String NCRLINE;
	private String CRLINETPE;
	private String CRLINEBEGDATE;
	private String CRLINEENDDATE;
	private String UPFILES;
	private String CPRIMCELLULANO;
	private String IP;
	private String CHANTPE;
	private String VERSION1;
	private String VERSION2;
	private String CHKID;
	
	
	
}
