package fstop.orm.po;

import java.io.Serializable;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import fstop.util.JSONUtils;

@Entity
@Table(name = "SYSLOG")
public class SYSLOG implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long SYSLOGID;

	private String ADUSERID;

	private String ADFDATE;

	private String ADFTIME;

	private String ADTDATE;

	private String ADTTIME;

	private Integer ADSEQ;	

	private String ADTXNO = "";

	private String ADPAGENM = "";

	private String ADOPID = "";

	private String ADBONM = "";

	private String ADMETHODNM = "";

	private String ADUSERIP = "";

	private String ADLOGTYPE = "";
	
	private String ADCONTENT = "";

	private String ADEXCODE = "";

	private String ADLOGGING = "";

	private String ADTXACNO = "";

	private String ADTXAMT = "";

	private String ADTXTYPE = "";
	
	private String ADCURRENCY = "";
	
	private String FGTXWAY = "";
	
	private String ADTXID = "";  //指出哪一個電文有誤
	
	private String ADGUID = "";
	
	private String LOGINTYPE = ""; //NB:網銀 ， MB:行動 ，預設為NB
	
	public String getADGUID() {
		return ADGUID;
	}

	public void setADGUID(String adguid) {
		ADGUID = adguid;
	}

	public String getFGTXWAY() {
		return FGTXWAY;
	}

	public void setFGTXWAY(String fgtxway) {
		FGTXWAY = fgtxway;
	}

	public String getADCURRENCY() {
		return ADCURRENCY;
	}

	public void setADCURRENCY(String adcurrency) {
		ADCURRENCY = adcurrency;
	}

	public String getADTXTYPE() {
		return ADTXTYPE;
	}

	public void setADTXTYPE(String adtxtype) {
		ADTXTYPE = adtxtype;
	}

	public String getADLOGGING() {
		return ADLOGGING;
	}

	public void setADLOGGING(String adlogging) {
		ADLOGGING = adlogging;
	}

	public String toString() {
		return new ToStringBuilder(this).append("syslogid", getSYSLOGID())
				.toString();
	}

	public String getADBONM() {
		return ADBONM;
	}

	public void setADBONM(String adbonm) {
		ADBONM = adbonm;
	}

	public String getADCONTENT() {
		
		Map<String, String> paramsMap = JSONUtils.json2map(ADCONTENT);
		paramsMap.remove("N950PASSWORD");
		paramsMap.remove("CMPASSWORD");
		paramsMap.remove("CMPWD");		
				
		return JSONUtils.map2json(paramsMap);
	}

	public void setADCONTENT(String adcontent) {
		
		Map<String, String> paramsMap = JSONUtils.json2map(adcontent);
		paramsMap.remove("N950PASSWORD");
		paramsMap.remove("CMPASSWORD");
		paramsMap.remove("CMPWD");
		
		ADCONTENT = JSONUtils.map2json(paramsMap);
	}

	public String getADEXCODE() {
		return ADEXCODE;
	}

	public void setADEXCODE(String adexcode) {
		ADEXCODE = adexcode;
	}

	public String getADLOGTYPE() {
		return ADLOGTYPE;
	}

	public void setADLOGTYPE(String adlogtype) {
		ADLOGTYPE = adlogtype;
	}

	public String getADMETHODNM() {
		return ADMETHODNM;
	}

	public void setADMETHODNM(String admethodnm) {
		ADMETHODNM = admethodnm;
	}

	public String getADOPID() {
		return ADOPID;
	}

	public void setADOPID(String adopid) {
		ADOPID = adopid;
	}

	public String getADPAGENM() {
		return ADPAGENM;
	}

	public void setADPAGENM(String adpagenm) {
		ADPAGENM = adpagenm;
	}

	public String getADTXNO() {
		return ADTXNO;
	}

	public void setADTXNO(String adtxno) {
		ADTXNO = adtxno;
	}

	public String getADUSERIP() {
		return ADUSERIP;
	}

	public void setADUSERIP(String aduserip) {
		ADUSERIP = aduserip;
	}

	public String getADTXACNO() {
		return ADTXACNO;
	}

	public void setADTXACNO(String adtxacno) {
		ADTXACNO = adtxacno;
	}

	public String getADTXAMT() {
		return ADTXAMT;
	}

	public void setADTXAMT(String adtxamt) {
		ADTXAMT = adtxamt;
	}

	public String getADTXID() {
		return ADTXID;
	}

	public void setADTXID(String adtxid) {
		ADTXID = adtxid;
	}

	public Long getSYSLOGID() {
		return SYSLOGID;
	}

	public void setSYSLOGID(Long syslogid) {
		SYSLOGID = syslogid;
	}

	public String getADFDATE() {
		return ADFDATE;
	}

	public void setADFDATE(String adfdate) {
		ADFDATE = adfdate;
	}

	public String getADFTIME() {
		return ADFTIME;
	}

	public void setADFTIME(String adftime) {
		ADFTIME = adftime;
	}

	public Integer getADSEQ() {
		return ADSEQ;
	}

	public void setADSEQ(Integer adseq) {
		ADSEQ = adseq;
	}

	public String getADTDATE() {
		return ADTDATE;
	}

	public void setADTDATE(String adtdate) {
		ADTDATE = adtdate;
	}

	public String getADTTIME() {
		return ADTTIME;
	}

	public void setADTTIME(String adttime) {
		ADTTIME = adttime;
	}

	public String getADUSERID() {
		return ADUSERID;
	}

	public void setADUSERID(String aduserid) {
		ADUSERID = aduserid;
	}

	public String getLOGINTYPE() {
		return LOGINTYPE;
	}

	public void setLOGINTYPE(String logintype) {
		LOGINTYPE = logintype;
	}

}
