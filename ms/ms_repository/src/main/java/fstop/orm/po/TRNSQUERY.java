package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TRNSQUERY")
public class TRNSQUERY implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long API_TXN_NO;

	private String TXN_DATETIME;

	private String MTI;

	private String PCODE;

	private String RCODE;

	private String RCODE_DESC;

	private String SRC_ID;

	private String FEE_REF_ID;

	private String SESSION_ID;

	private String TRNSCODE;

	public Long getAPI_TXN_NO() {
		return API_TXN_NO;
	}

	public void setAPI_TXN_NO(Long aPI_TXN_NO) {
		API_TXN_NO = aPI_TXN_NO;
	}

	public String getTXN_DATETIME() {
		return TXN_DATETIME;
	}

	public void setTXN_DATETIME(String tXN_DATETIME) {
		TXN_DATETIME = tXN_DATETIME;
	}

	public String getMTI() {
		return MTI;
	}

	public void setMTI(String mTI) {
		MTI = mTI;
	}

	public String getPCODE() {
		return PCODE;
	}

	public void setPCODE(String pCODE) {
		PCODE = pCODE;
	}

	public String getRCODE() {
		return RCODE;
	}

	public void setRCODE(String rCODE) {
		RCODE = rCODE;
	}

	public String getRCODE_DESC() {
		return RCODE_DESC;
	}

	public void setRCODE_DESC(String rCODE_DESC) {
		RCODE_DESC = rCODE_DESC;
	}

	public String getSRC_ID() {
		return SRC_ID;
	}

	public void setSRC_ID(String sRC_ID) {
		SRC_ID = sRC_ID;
	}

	public String getFEE_REF_ID() {
		return FEE_REF_ID;
	}

	public void setFEE_REF_ID(String fEE_REF_ID) {
		FEE_REF_ID = fEE_REF_ID;
	}

	public String getSESSION_ID() {
		return SESSION_ID;
	}

	public void setSESSION_ID(String sESSION_ID) {
		SESSION_ID = sESSION_ID;
	}

	public String getTRNSCODE() {
		return TRNSCODE;
	}

	public void setTRNSCODE(String tRNSCODE) {
		TRNSCODE = tRNSCODE;
	}
	
}
