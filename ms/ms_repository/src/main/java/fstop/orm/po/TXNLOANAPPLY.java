package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TXNLOANAPPLY")
public class TXNLOANAPPLY implements Serializable {

	@Id
	private String RCVNO = "";
	private String APPKIND = "";
	private String APPAMT  = "";
	private String PURPOSE  = "";
	private String PERIODS  = "";
	private String BANKID  = "";
	private String BANKNA  = "";
	private String SEX  = "";
	private String APPNAME  = "";
	private String APPIDNO  = "";
	private String BRTHDT  = "";
	private String EDUS = "";
	private String MARITAL = "";
	private String CHILDNO = "";
	private String CPHONE = "";
	private String PHONE_01 = "";
	private String PHONE_02 = "";
	private String PHONE_03 = "";
	private String PHONE_11 = "";
	private String PHONE_12 = "";
	private String HOUSE = "";
	private String MONEY11 = "";	
	private String MONEY12 = "";
	private String ZIP1 = "";
	private String CITY1 = "";
	private String ZONE1 = "";
	private String ADDR1 = "";
	private String ZIP2 = "";
	private String CITY2 = "";
	private String ZONE2 = "";
	private String ADDR2 = "";
	private String ZIP3 = "";
	private String CITY3 = "";
	private String ZONE3 = "";
	private String ADDR3 = "";
	private String EMAIL = "";
	private String CAREERNAME = "";
	private String CAREERIDNO = "";
	private String ZIP4 = "";
	private String CITY4 = "";
	private String ZONE4 = "";
	private String ADDR4 = "";
	private String PHONE_41 = "";
	private String PHONE_42 = "";
	private String PHONE_43 = "";
	private String OFFICEY = "";
	private String OFFICEM = "";
	private String PROFNO = "";
	private String INCOME = "";
	private String PRECARNAME = "";
	private String PRETKY = "";
	private String PRETKM = "";
	private String CKS1 = "";
	private String RELTYPE1 = "";
	private String RELNAME1 = "";
	private String RELID1 = "";
	private String CKS2 = "";
	private String RELTYPE2 = "";
	private String RELNAME2 = "";
	private String RELID2 = "";
	private String CKS3 = "";
	private String RELTYPE3 = "";
	private String RELNAME3 = "";
	private String RELID3 = "";
	private String CKS4 = "";
	private String RELTYPE4 = "";
	private String RELNAME4 = "";
	private String RELID4 = "";
	private String CKP1 = "";
	private String OTHCONM1 = "";
	private String OTHCOID1 = "";
	private String CKM1 = "";
	private String MOTHCONM1 = "";
	private String MOTHCOID1 = "";
	private String ECERT = "";
	private String FILE1 = "";
	private String FILE11 = "";
	private String FILE2 = "";
	private String FILE3 = "";
	private String LASTDATE = "";
	private String LASTTIME = "";
	private String STATUS = "";
	private String PAYSOURCE = "";
	private String VERSION = "";
	private String IP = "";
	private String LOGINTYPE = "";
	
	public String getRCVNO() {
		return RCVNO;
	}

	public void setRCVNO(String rcvno) {
		RCVNO = rcvno;
	}

	public String getAPPKIND() {
		return APPKIND;
	}

	public void setAPPKIND(String appkind) {
		APPKIND = appkind;
	}

	public String getAPPAMT() {
		return APPAMT;
	}

	public void setAPPAMT(String appamt) {
		APPAMT = appamt;
	}

	public String getPURPOSE() {
		return PURPOSE;
	}

	public void setPURPOSE(String purpose) {
		PURPOSE = purpose;
	}

	public String getPERIODS() {
		return PERIODS;
	}

	public void setPERIODS(String periods) {
		PERIODS = periods;
	}

	public String getBANKID() {
		return BANKID;
	}

	public void setBANKID(String bankid) {
		BANKID = bankid;
	}

	public String getBANKNA() {
		return BANKNA;
	}

	public void setBANKNA(String bankna) {
		BANKNA = bankna;
	}

	public String getSEX() {
		return SEX;
	}

	public void setSEX(String sex) {
		SEX = sex;
	}

	public String getAPPNAME() {
		return APPNAME;
	}

	public void setAPPNAME(String appname) {
		APPNAME = appname;
	}

	public String getAPPIDNO() {
		return APPIDNO;
	}

	public void setAPPIDNO(String appidno) {
		APPIDNO = appidno;
	}

	public String getBRTHDT() {
		return BRTHDT;
	}

	public void setBRTHDT(String brthdt) {
		BRTHDT = brthdt;
	}	

	public String getEDUS() {
		return EDUS;
	}

	public void setEDUS(String edus) {
		EDUS = edus;
	}	

	public String getMARITAL() {
		return MARITAL;
	}

	public void setMARITAL(String marital) {
		MARITAL = marital;
	}

	public String getCHILDNO() {
		return CHILDNO;
	}

	public void setCHILDNO(String childno) {
		CHILDNO = childno;
	}

	public String getCPHONE() {
		return CPHONE;
	}

	public void setCPHONE(String cphone) {
		CPHONE = cphone;
	}

	public String getPHONE_01() {
		return PHONE_01;
	}

	public void setPHONE_01(String phone01) {
		PHONE_01 = phone01;
	}

	public String getPHONE_02() {
		return PHONE_02;
	}

	public void setPHONE_02(String phone02) {
		PHONE_02 = phone02;
	}	

	public String getPHONE_03() {
		return PHONE_03;
	}

	public void setPHONE_03(String phone03) {
		PHONE_03 = phone03;
	}

	public String getPHONE_11() {
		return PHONE_11;
	}

	public void setPHONE_11(String phone11) {
		PHONE_11 = phone11;
	}

	public String getPHONE_12() {
		return PHONE_12;
	}

	public void setPHONE_12(String phone12) {
		PHONE_12 = phone12;
	}	

	public String getHOUSE() {
		return HOUSE;
	}

	public void setHOUSE(String house) {
		HOUSE = house;
	}

	public String getMONEY11() {
		return MONEY11;
	}

	public void setMONEY11(String money11) {
		MONEY11 = money11;
	}

	public String getMONEY12() {
		return MONEY12;
	}

	public void setMONEY12(String money12) {
		MONEY12 = money12;
	}	

	public String getZIP1() {
		return ZIP1;
	}

	public void setZIP1(String zip1) {
		ZIP1 = zip1;
	}	

	public String getCITY1() {
		return CITY1;
	}

	public void setCITY1(String city1) {
		CITY1 = city1;
	}

	public String getZONE1() {
		return ZONE1;
	}

	public void setZONE1(String zone1) {
		ZONE1 = zone1;
	}

	public String getADDR1() {
		return ADDR1;
	}

	public void setADDR1(String addr1) {
		ADDR1 = addr1;
	}	

	public String getZIP2() {
		return ZIP2;
	}

	public void setZIP2(String zip2) {
		ZIP2 = zip2;
	}	

	public String getCITY2() {
		return CITY2;
	}

	public void setCITY2(String city2) {
		CITY2 = city2;
	}	

	public String getZONE2() {
		return ZONE2;
	}

	public void setZONE2(String zone2) {
		ZONE2 = zone2;
	}

	public String getADDR2() {
		return ADDR2;
	}

	public void setADDR2(String addr2) {
		ADDR2 = addr2;
	}	

	public String getZIP3() {
		return ZIP3;
	}

	public void setZIP3(String zip3) {
		ZIP3 = zip3;
	}	

	public String getCITY3() {
		return CITY3;
	}

	public void setCITY3(String city3) {
		CITY3 = city3;
	}	

	public String getZONE3() {
		return ZONE3;
	}

	public void setZONE3(String zone3) {
		ZONE3 = zone3;
	}

	public String getADDR3() {
		return ADDR3;
	}

	public void setADDR3(String addr3) {
		ADDR3 = addr3;
	}	

	public String getEMAIL() {
		return EMAIL;
	}

	public void setEMAIL(String email) {
		EMAIL = email;
	}

	public String getCAREERNAME() {
		return CAREERNAME;
	}

	public void setCAREERNAME(String careername) {
		CAREERNAME = careername;
	}	

	public String getCAREERIDNO() {
		return CAREERIDNO;
	}

	public void setCAREERIDNO(String careeridno) {
		CAREERIDNO = careeridno;
	}	

	public String getZIP4() {
		return ZIP4;
	}

	public void setZIP4(String zip4) {
		ZIP4 = zip4;
	}	

	public String getCITY4() {
		return CITY4;
	}

	public void setCITY4(String city4) {
		CITY4 = city4;
	}	

	public String getZONE4() {
		return ZONE4;
	}

	public void setZONE4(String zone4) {
		ZONE4 = zone4;
	}

	public String getADDR4() {
		return ADDR4;
	}

	public void setADDR4(String addr4) {
		ADDR4 = addr4;
	}	

	public String getPHONE_41() {
		return PHONE_41;
	}

	public void setPHONE_41(String phone41) {
		PHONE_41 = phone41;
	}

	public String getPHONE_42() {
		return PHONE_42;
	}

	public void setPHONE_42(String phone42) {
		PHONE_42 = phone42;
	}	

	public String getPHONE_43() {
		return PHONE_43;
	}

	public void setPHONE_43(String phone43) {
		PHONE_43 = phone43;
	}	

	public String getOFFICEY() {
		return OFFICEY;
	}

	public void setOFFICEY(String officey) {
		OFFICEY = officey;
	}	

	public String getOFFICEM() {
		return OFFICEM;
	}

	public void setOFFICEM(String officem) {
		OFFICEM = officem;
	}	

	public String getPROFNO() {
		return PROFNO;
	}

	public void setPROFNO(String profno) {
		PROFNO = profno;
	}	

	public String getINCOME() {
		return INCOME;
	}

	public void setINCOME(String income) {
		INCOME = income;
	}

	public String getPRECARNAME() {
		return PRECARNAME;
	}

	public void setPRECARNAME(String precarname) {
		PRECARNAME = precarname;
	}

	public String getPRETKY() {
		return PRETKY;
	}

	public void setPRETKY(String pretky) {
		PRETKY = pretky;
	}

	public String getPRETKM() {
		return PRETKM;
	}

	public void setPRETKM(String pretkm) {
		PRETKM = pretkm;
	}

	public String getCKS1() {
		return CKS1;
	}

	public void setCKS1(String cks1) {
		CKS1 = cks1;
	}	

	public String getRELTYPE1() {
		return RELTYPE1;
	}

	public void setRELTYPE1(String reltype1) {
		RELTYPE1 = reltype1;
	}	

	public String getRELNAME1() {
		return RELNAME1;
	}

	public void setRELNAME1(String relname1) {
		RELNAME1 = relname1;
	}	

	public String getRELID1() {
		return RELID1;
	}

	public void setRELID1(String relid1) {
		RELID1 = relid1;
	}

	public String getCKS2() {
		return CKS2;
	}

	public void setCKS2(String cks2) {
		CKS2 = cks2;
	}	

	public String getRELTYPE2() {
		return RELTYPE2;
	}

	public void setRELTYPE2(String reltype2) {
		RELTYPE2 = reltype2;
	}	

	public String getRELNAME2() {
		return RELNAME2;
	}

	public void setRELNAME2(String relname2) {
		RELNAME2 = relname2;
	}	

	public String getRELID2() {
		return RELID2;
	}

	public void setRELID2(String relid2) {
		RELID2 = relid2;
	}	

	public String getCKS3() {
		return CKS3;
	}

	public void setCKS3(String cks3) {
		CKS3 = cks3;
	}	

	public String getRELTYPE3() {
		return RELTYPE3;
	}

	public void setRELTYPE3(String reltype3) {
		RELTYPE3 = reltype3;
	}	

	public String getRELNAME3() {
		return RELNAME3;
	}

	public void setRELNAME3(String relname3) {
		RELNAME3 = relname3;
	}	

	public String getRELID3() {
		return RELID3;
	}

	public void setRELID3(String relid3) {
		RELID3 = relid3;
	}

	public String getCKS4() {
		return CKS4;
	}

	public void setCKS4(String cks4) {
		CKS4 = cks4;
	}	

	public String getRELTYPE4() {
		return RELTYPE4;
	}

	public void setRELTYPE4(String reltype4) {
		RELTYPE4 = reltype4;
	}	

	public String getRELNAME4() {
		return RELNAME4;
	}

	public void setRELNAME4(String relname4) {
		RELNAME4 = relname4;
	}	

	public String getRELID4() {
		return RELID4;
	}

	public void setRELID4(String relid4) {
		RELID4 = relid4;
	}

	public String getCKP1() {
		return CKP1;
	}

	public void setCKP1(String ckp1) {
		CKP1 = ckp1;
	}	

	public String getOTHCONM1() {
		return OTHCONM1;
	}

	public void setOTHCONM1(String othconm1) {
		OTHCONM1 = othconm1;
	}

	public String getOTHCOID1() {
		return OTHCOID1;
	}

	public void setOTHCOID1(String othcoid1) {
		OTHCOID1 = othcoid1;
	}	

	public String getCKM1() {
		return CKM1;
	}

	public void setCKM1(String ckm1) {
		CKM1 = ckm1;
	}	

	public String getMOTHCONM1() {
		return MOTHCONM1;
	}

	public void setMOTHCONM1(String mothconm1) {
		MOTHCONM1 = mothconm1;
	}	

	public String getMOTHCOID1() {
		return MOTHCOID1;
	}

	public void setMOTHCOID1(String mothcoid1) {
		MOTHCOID1 = mothcoid1;
	}
	
	public String getECERT() {
		return ECERT;
	}

	public void setECERT(String ecert) {
		ECERT = ecert;
	}	
	
	public String getFILE1() {
		return FILE1;
	}

	public void setFILE1(String file1) {
		FILE1 = file1;
	}
	
	public String getFILE2() {
		return FILE2;
	}

	public void setFILE2(String file2) {
		FILE2 = file2;
	}	
	
	public String getFILE3() {
		return FILE3;
	}

	public void setFILE3(String file3) {
		FILE3 = file3;
	}
	
	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}
	
	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}
	
	public String getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(String status) {
		STATUS = status;
	}	

	
	public String getPAYSOURCE() {
		return PAYSOURCE;
	}

	public void setPAYSOURCE(String paysource) {
		PAYSOURCE = paysource;
	}
	public String getVERSION() {
		return VERSION;
	}

	public void setVERSION(String version) {
		VERSION = version;
	}	
	public String getIP() {
		return IP;
	}

	public void setIP(String ip) {
		IP = ip;
	}

	public String getFILE11() {
		return FILE11;
	}

	public void setFILE11(String file11) {
		FILE11 = file11;
	}

	public String getLOGINTYPE() {
		return LOGINTYPE;
	}

	public void setLOGINTYPE(String lOGINTYPE) {
		LOGINTYPE = lOGINTYPE;
	}	
}
