package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;

@Entity
@Table(name = "REVOLVINGCREDITAPPLY")
public class REVOLVINGCREDITAPPLY implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long RCVNO;
	
	private String CUSNAME = "";
	
	private String CUSIDN = "";
	
	private String HPHONE = "";
	
	private String OPHONE = "";
	
	private String MPFONE = "";

	private String CARDNUM = "";
	
	private String CURRBAL = "";
	
	private String POT = "";
	
	private String RATE = "";
	
	private String CRLIMIT = "";
	
	private String AMOUNT = "";
	
	private String PERIOD = "";
	
	private String APPLY_RATE = "";
	
	private String FIRST_AMOUNT = "";
	
	private String FIRST_INTEREST = "";
	
	private String PERIOD_AMOUNT = "";

	private String APPDATE = "";

	private String APPTIME = "";
	
	//--20170603新增欄位，因應聯徵---start----
	
	private String USER_IP = "";	//來源IP
	private String CHK_RESULT = "";	//身分驗證結果
	private String MATTER_VER = "";	//約定事項版本
	
	public String getCHK_RESULT() {
		return CHK_RESULT;
	}

	public void setCHK_RESULT(String chk_result) {
		CHK_RESULT = chk_result;
	}

	public String getMATTER_VER() {
		return MATTER_VER;
	}

	public void setMATTER_VER(String matter_ver) {
		MATTER_VER = matter_ver;
	}

	public String getUSER_IP() {
		return USER_IP;
	}

	public void setUSER_IP(String user_ip) {
		USER_IP = user_ip;
	}
	
	//--20170603新增欄位---end-------

	public String getAMOUNT() {
		return AMOUNT;
	}

	public void setAMOUNT(String amount) {
		AMOUNT = amount;
	}

	public String getAPPDATE() {
		return APPDATE;
	}

	public void setAPPDATE(String appdate) {
		APPDATE = appdate;
	}

	public String getAPPLY_RATE() {
		return APPLY_RATE;
	}

	public void setAPPLY_RATE(String apply_rate) {
		APPLY_RATE = apply_rate;
	}

	public String getAPPTIME() {
		return APPTIME;
	}

	public void setAPPTIME(String apptime) {
		APPTIME = apptime;
	}

	public String getCARDNUM() {
		return CARDNUM;
	}

	public void setCARDNUM(String cardnum) {
		CARDNUM = cardnum;
	}

	public String getCRLIMIT() {
		return CRLIMIT;
	}

	public void setCRLIMIT(String crlimit) {
		CRLIMIT = crlimit;
	}

	public String getCURRBAL() {
		return CURRBAL;
	}

	public void setCURRBAL(String currbal) {
		CURRBAL = currbal;
	}

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cusidn) {
		CUSIDN = cusidn;
	}

	public String getCUSNAME() {
		return CUSNAME;
	}

	public void setCUSNAME(String cusname) {
		CUSNAME = cusname;
	}

	public String getFIRST_AMOUNT() {
		return FIRST_AMOUNT;
	}

	public void setFIRST_AMOUNT(String first_amount) {
		FIRST_AMOUNT = first_amount;
	}

	public String getFIRST_INTEREST() {
		return FIRST_INTEREST;
	}

	public void setFIRST_INTEREST(String first_interest) {
		FIRST_INTEREST = first_interest;
	}

	public String getHPHONE() {
		return HPHONE;
	}

	public void setHPHONE(String hphone) {
		HPHONE = hphone;
	}

	public String getMPFONE() {
		return MPFONE;
	}

	public void setMPFONE(String mpfone) {
		MPFONE = mpfone;
	}

	public String getOPHONE() {
		return OPHONE;
	}

	public void setOPHONE(String ophone) {
		OPHONE = ophone;
	}

	public String getPERIOD() {
		return PERIOD;
	}

	public void setPERIOD(String period) {
		PERIOD = period;
	}

	public String getPERIOD_AMOUNT() {
		return PERIOD_AMOUNT;
	}

	public void setPERIOD_AMOUNT(String period_amount) {
		PERIOD_AMOUNT = period_amount;
	}

	public String getPOT() {
		return POT;
	}

	public void setPOT(String pot) {
		POT = pot;
	}

	public String getRATE() {
		return RATE;
	}

	public void setRATE(String rate) {
		RATE = rate;
	}

	public Long getRCVNO() {
		return RCVNO;
	}

	public void setRCVNO(Long rcvno) {
		RCVNO = rcvno;
	}

}
