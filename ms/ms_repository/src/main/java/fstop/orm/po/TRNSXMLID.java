package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class TRNSXMLID implements Serializable{

	private int API_TXN_NO;

	private String MTI;
	
	private String PCODE;
	
	public TRNSXMLID() {
	}

	/** full constructor */
	public TRNSXMLID(int apiTxnNo, String mti, String pcode) {
		this.API_TXN_NO = apiTxnNo;
		this.MTI = mti;
		this.PCODE = pcode;
	}

	public int getAPI_TXN_NO() {
		return API_TXN_NO;
	}

	public String getMTI() {
		return MTI;
	}

	public String getPCODE() {
		return PCODE;
	}

	public void setAPI_TXN_NO(int aPI_TXN_NO) {
		API_TXN_NO = aPI_TXN_NO;
	}

	public void setMTI(String mTI) {
		MTI = mTI;
	}

	public void setPCODE(String pCODE) {
		PCODE = pCODE;
	}
	
	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof TRNSXMLID))
			return false;
		TRNSXMLID castOther = (TRNSXMLID) other;

		return ((this.getMTI() == castOther.getMTI()) || (this.getMTI() != null && castOther.getMTI() != null && 
				 this.getMTI().equals(castOther.getMTI()))) &&
				 ((this.getPCODE() == castOther.getPCODE()) || (this.getPCODE() != null && castOther.getPCODE() != null && 
				 this.getPCODE().equals(castOther.getPCODE())));
	}

	public int hashCode() {
		int result = 17;
		result = 37 * result + (getMTI() == null ? 0 : this.getMTI().hashCode());
		result = 37 * result + (getPCODE() == null ? 0 : this.getPCODE().hashCode());
		return result;
	}
}
