package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;

@Entity
@Table(name = "TXNCSSSLOG")
public class TXNCSSSLOG implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long LOGID;
	
	private String CUSIDN = ""; 

	private String ADOPID = "";

	private String ANSWER = "";

	private String NEXTBTN = "";

	private String LASTDATE = "";

	private String LASTTIME = "";

	private String CHANNEL = "";
	
	private String MSG = "";

	
	public Long getLOGID() {
		return LOGID;
	}

	public void setLOGID(Long logid) {
		LOGID = logid;
	}

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cusidn) {
		CUSIDN = cusidn;
	}

	public String getADOPID() {
		return ADOPID;
	}

	public void setADOPID(String adopid) {
		ADOPID = adopid;
	}

	public String getANSWER() {
		return ANSWER;
	}

	public void setANSWER(String answer) {
		ANSWER = answer;
	}

	public String getNEXTBTN() {
		return NEXTBTN;
	}

	public void setNEXTBTN(String nextbtn) {
		NEXTBTN = nextbtn;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

	public String getCHANNEL() {
		return CHANNEL;
	}

	public void setCHANNEL(String channel) {
		CHANNEL = channel;
	}

	public String getMSG() {
		return MSG;
	}

	public void setMSG(String mSG) {
		MSG = mSG;
	}
}
