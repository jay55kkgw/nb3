package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Entity
@Table(name = "GOLDPRICE")
public class GOLDPRICE implements Serializable {

	@Id
	private String GOLDPRICEID;

	private String QDATE;

	private String QTIME;

	private String BPRICE;

	private String SPRICE;

	private String PRICE1;

	private String PRICE2;

	private String PRICE3;

	private String PRICE4;

	private String LASTDATE;

	private String LASTTIME;

	public String toString() {
		return new ToStringBuilder(this).append("goldpriceid", getGOLDPRICEID())
		.toString();
	}

	public String getGOLDPRICEID() {
		return GOLDPRICEID;
	}

	public void setGOLDPRICEID(String goldpriceid) {
		log.debug("goldpriceid = " + goldpriceid);
		GOLDPRICEID = goldpriceid;
	}

	public String getQDATE() {
		return QDATE;
	}

	public void setQDATE(String qdate) {
		log.debug("qdate = " + qdate);
		QDATE = qdate;
	}

	public String getQTIME() {
		return QTIME;
	}

	public void setQTIME(String qtime) {
		log.debug("qtime = " + qtime);
		QTIME = qtime;
	}

	public String getBPRICE() {
		return BPRICE;
	}

	public void setBPRICE(String bprice) {
		log.debug("bprice = " + bprice);
		BPRICE = bprice;
	}

	public String getSPRICE() {
		return SPRICE;
	}

	public void setSPRICE(String sprice) {
		log.debug("sprice = " + sprice);
		SPRICE = sprice;
	}

	public String getPRICE1() {
		return PRICE1;
	}

	public void setPRICE1(String price1) {
		log.debug("price1 = " + price1);
		PRICE1 = price1;
	}

	public String getPRICE2() {
		return PRICE2;
	}

	public void setPRICE2(String price2) {
		log.debug("price2 = " + price2);
		PRICE2 = price2;
	}

	public String getPRICE3() {
		return PRICE3;
	}

	public void setPRICE3(String price3) {
		log.debug("price3 = " + price3);
		PRICE3 = price3;
	}

	public String getPRICE4() {
		return PRICE4;
	}

	public void setPRICE4(String price4) {
		log.debug("price4 = " + price4);
		PRICE4 = price4;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lastdate) {
		log.debug("lastdate = " + lastdate);
		LASTDATE = lastdate;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lasttime) {
		log.debug("lasttime = " + lasttime);
		LASTTIME = lasttime;
	}

}
