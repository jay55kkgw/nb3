package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

//分行/ATM/證券據點檔
@Entity
@Table(name = "ADMAPPBRANCH")
public class ADMAPPBRANCH implements Serializable{

	private Integer BHID;		         //據點ID
	
	private String BHUSERID ="";		 //使用者代號
	
	private String BHUSERNAME ="";		 //使用者名稱
	
	private String BHTXDATE ="";		 //交易日期
	
	private String BHTXTIME ="";		 //交易時間
	
	private String BHADTYPE ="";		 //據點類型
	
	private String BHNAME ="";		     //據點名稱
	
	private String BHCOUNTY ="";		 //縣市別
	
	private String BHREGION ="";		 //行政區
	
	private String BHADDR ="";		     //地址
	
	private String BHLATITUDE ="";		 //緯度
	
	private String BHLONGITUDE ="";	     //經度
	
	private String BHTELCOUNTRY ="";	 //電話-國碼
	
	private String BHTELREGION ="";	     //電話-區碼
	
	private String BHTEL ="";		     //電話號碼
	
	private String BHPICADD ="";		 //據點照片路徑
	
	private String BHPICDATA;		     //據點照片
	
	private String BHSTIME ="";		     //開始營業時間
	
	private String BHETIME ="";		     //結束營業時間
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getBHID() {
		return BHID;
	}

	public void setBHID(Integer bhid) {
		BHID = bhid;
	}

	public String getBHUSERID() {
		return BHUSERID;
	}

	public void setBHUSERID(String bhuserid) {
		BHUSERID = bhuserid;
	}

	public String getBHUSERNAME() {
		return BHUSERNAME;
	}

	public void setBHUSERNAME(String bhusername) {
		BHUSERNAME = bhusername;
	}

	public String getBHTXDATE() {
		return BHTXDATE;
	}

	public void setBHTXDATE(String bhtxdate) {
		BHTXDATE = bhtxdate;
	}

	public String getBHTXTIME() {
		return BHTXTIME;
	}

	public void setBHTXTIME(String bhtxtime) {
		BHTXTIME = bhtxtime;
	}

	public String getBHADTYPE() {
		return BHADTYPE;
	}

	public void setBHADTYPE(String bhadtype) {
		BHADTYPE = bhadtype;
	}

	public String getBHNAME() {
		return BHNAME;
	}

	public void setBHNAME(String bhname) {
		BHNAME = bhname;
	}

	public String getBHCOUNTY() {
		return BHCOUNTY;
	}

	public void setBHCOUNTY(String bhcounty) {
		BHCOUNTY = bhcounty;
	}

	public String getBHREGION() {
		return BHREGION;
	}

	public void setBHREGION(String bhregion) {
		BHREGION = bhregion;
	}

	public String getBHADDR() {
		return BHADDR;
	}

	public void setBHADDR(String bhaddr) {
		BHADDR = bhaddr;
	}

	public String getBHLATITUDE() {
		return BHLATITUDE;
	}

	public void setBHLATITUDE(String latitude) {
		BHLATITUDE = latitude;
	}

	public String getBHLONGITUDE() {
		return BHLONGITUDE;
	}

	public void setBHLONGITUDE(String longitude) {
		BHLONGITUDE = longitude;
	}

	public String getBHTELCOUNTRY() {
		return BHTELCOUNTRY;
	}

	public void setBHTELCOUNTRY(String country) {
		BHTELCOUNTRY = country;
	}

	public String getBHTELREGION() {
		return BHTELREGION;
	}

	public void setBHTELREGION(String region) {
		BHTELREGION = region;
	}

	public String getBHTEL() {
		return BHTEL;
	}

	public void setBHTEL(String bhtel) {
		BHTEL = bhtel;
	}

	public String getBHPICADD() {
		return BHPICADD;
	}

	public void setBHPICADD(String bhpicadd) {
		BHPICADD = bhpicadd;
	}

	public String getBHPICDATA() {
		return BHPICDATA;
	}

	public void setBHPICDATA(String bhpicdata) {
		BHPICDATA = bhpicdata;
	}

	public String getBHSTIME() {
		return BHSTIME;
	}

	public void setBHSTIME(String bhstime) {
		BHSTIME = bhstime;
	}

	public String getBHETIME() {
		return BHETIME;
	}

	public void setBHETIME(String bhetime) {
		BHETIME = bhetime;
	}

}
