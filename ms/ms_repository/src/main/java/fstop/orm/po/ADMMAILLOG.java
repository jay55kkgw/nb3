package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "ADMMAILLOG")
@Data
public class ADMMAILLOG implements Serializable
{

	private static final long serialVersionUID = -8184360929933833253L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long ADMAILLOGID;

	private String ADBATCHNO = "";

	private String ADUSERID = "";

	private String ADUSERNAME = "";

	private String ADMAILACNO = ""; // 批次作業代號 varchar(10)

	private String ADACNO = "";

	private String ADSENDTYPE = "";

	private String ADSENDTIME = "";

	private String ADSENDSTATUS = "";

	private String ADMSUBJECT = "";

	@Column(nullable = false)
	private String ADMAILCONTENT = "";

}
