package fstop.orm.po;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "ITRN027")
public class ITRN027 implements Serializable {

	@Id 
	private String RECNO;

	private String HEADER;

	private String SEQ;

	private String BRHBDT;

	private String COUNT;
	
	private String TERM;
		
	private String ITR1;
	
	private String TITLE1;
	
	private String ITR2;
	
	private String TITLE2;
	
	private String ITR3;
	
	private String TITLE3;
	
	private String FILL;
	
	public String toString() {
		return new ToStringBuilder(this).append("recno", getRECNO())
				.toString();
	}

	public String getRECNO() {
		return RECNO;
	}

	public void setRECNO(String recno) {
		RECNO = recno;
	}

	public String getHEADER() {
		return HEADER;
	}

	public void setHEADER(String header) {
		HEADER = header;
	}

	public String getSEQ() {
		return SEQ;
	}

	public void setSEQ(String seq) {
		SEQ = seq;
	}

	public String getBRHBDT() {
		return BRHBDT;
	}

	public void setBRHBDT(String brhbdt) {
		BRHBDT = brhbdt;
	}

	public String getCOUNT() {
		return COUNT;
	}

	public void setCOUNT(String count) {
		COUNT = count;
	}

	public String getTERM() {
		return TERM;
	}

	public void setTERM(String term) {
		TERM = term;
	}

	public String getITR1() {
		return ITR1;
	}

	public void setITR1(String itr1) {
		ITR1 = itr1;
	}

	public String getTITLE1() {
		return TITLE1;
	}

	public void setTITLE1(String title1) {
		TITLE1 = title1;
	}

	public String getITR2() {
		return ITR2;
	}

	public void setITR2(String itr2) {
		ITR2 = itr2;
	}

	public String getTITLE2() {
		return TITLE2;
	}

	public void setTITLE2(String title2) {
		TITLE2 = title2;
	}

	public String getITR3() {
		return ITR3;
	}

	public void setITR3(String itr3) {
		ITR3 = itr3;
	}

	public String getTITLE3() {
		return TITLE3;
	}

	public void setTITLE3(String title3) {
		TITLE3 = title3;
	}

	public String getFILL() {
		return FILL;
	}

	public void setFILL(String fill) {
		FILL = fill;
	}

}
