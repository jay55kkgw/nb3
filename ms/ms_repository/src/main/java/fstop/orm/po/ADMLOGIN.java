package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "ADMLOGIN")
@Data
public class ADMLOGIN implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4524439676076582336L;
	@Id
	private String ADUSERID = "";		// 使用者統編ID
	private String TOKENID = "";		// 使用者識別
	private String ADUSERTYPE = "";		// 操作者類別
	private String ADUSERIP = "";		// 登入 IP-Address
	private String LOGINOUT = "";		// 登入狀態
	private String LOGINTIME = "";		// 本次登入日期時間
	private String LOGOUTTIME = "";		// 本次正常登出日期時間
	private String FORCELOGOUTTIME = "";// 本次強迫登出日期時間
	private String LASTLOGINTIME = "";	// 上次登入日期時間
	private String LASTADUSERIP = "";	// 上次登入IP
	private String LOGINTYPE = "";		// 登入來源
	private String DPUSERNAME = "";		// 使用者姓名
	private String MMACOD = "";			// MMA輕鬆理財註記
	private String XMLCOD = "";			// 憑證代碼
	private String BRTHDY = "";			// 出生年月日
	private String TELNUM = "";			// 電話號碼
	private String STAFF = "";			// 是否為行員
	private String MBSTAT = "";			// 行動銀行目前狀態
	private String MBOPNDT = "";		// 行動銀行啟用/關閉日期
	private String MBOPNTM = "";		// 行動銀行啟用/關閉時間
	private String IDNCOD = "";			// 1 個人模式  2 企業模式
	private String APATR = "";			// 申請網銀約定業務權限功能
	private String HKCODE = "";			// 香港網銀是否需轉置註記

}
