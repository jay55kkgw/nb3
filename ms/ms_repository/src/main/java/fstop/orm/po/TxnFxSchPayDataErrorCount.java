package fstop.orm.po;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
public class TxnFxSchPayDataErrorCount {
	@Id
	private String FXEXCODE;
	private String FXEXCODEMSG;
	private String TIMES;
}
