package fstop.orm.po;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

import lombok.Data;

@Embeddable
@Data
public class ADMHOTOP_PK implements Serializable {

	private static final long serialVersionUID = 2626783181586822751L;
	/**
	 * 
	 */
	private String ADUSERID;
	private String ADOPID;
	
	
}
