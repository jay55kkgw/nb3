package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TRNSDATA")
public class TRNSDATA implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int API_TXN_NO;
	
	private String TRNSNO;
	
	private String ACCESS;
	
	private String BILL;
	
	private String ACTION;
	
	private String TXN_DATETIME;

	public int getAPI_TXN_NO() {
		return API_TXN_NO;
	}

	public void setAPI_TXN_NO(int aPI_TXN_NO) {
		API_TXN_NO = aPI_TXN_NO;
	}

	public String getTRNSNO() {
		return TRNSNO;
	}

	public void setTRNSNO(String tRNSNO) {
		TRNSNO = tRNSNO;
	}

	public String getACCESS() {
		return ACCESS;
	}

	public void setACCESS(String aCCESS) {
		ACCESS = aCCESS;
	}

	public String getBILL() {
		return BILL;
	}

	public void setBILL(String bILL) {
		BILL = bILL;
	}

	public String getACTION() {
		return ACTION;
	}

	public void setACTION(String aCTION) {
		ACTION = aCTION;
	}

	public String getTXN_DATETIME() {
		return TXN_DATETIME;
	}

	public void setTXN_DATETIME(String tXN_DATETIME) {
		TXN_DATETIME = tXN_DATETIME;
	}
}
