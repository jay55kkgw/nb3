package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

import lombok.Data;

@Entity
@Table(name = "ADMHOTOP")
@Data
public class ADMHOTOP implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5136011858396566291L;

	@Id
	private ADMHOTOP_PK pks;
	
	String LASTDT;

}
