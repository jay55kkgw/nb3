package fstop.orm.po;

import java.io.Serializable;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import fstop.util.JSONUtils;

@Entity
@Table(name = "TXNLOG")
public class TXNLOG implements Serializable {
	@Id
	private String ADTXNO = "";   //交易編號	varchar	24	PK		交易時間(年月日時分秒)14位+操作者代號10位

	private String ADUSERID = "";  //操作者代號	varchar	10

	private String ADOPID = "";  //操作功能ID	varchar	10

	private String FGTXWAY = "";  //交易機制	char	1			傳入參數集內有FGTXWAY參數時才寫入 0.交易密碼  1.IKey電子簽章  2.動態密碼

	private String ADTXACNO = "";  //交易帳號/卡號(轉出或設定)	varchar	16			交易XML有定義才寫入

	private String ADTXAMT = "";  //轉出交易金額	varchar	16			交易XML有定義才寫入

	private String ADCURRENCY = "";  // 轉出幣別	char	3			交易XML有定義才寫入

	private String ADSVBH = "";  // 轉入銀行代碼	char	3			交易XML有定義才寫入

	private String ADAGREEF = "";  // 轉入帳號是約定或非約定帳號註記	char	1 交易XML有定義才寫入 0.非約定 1.約定

	private String ADREQTYPE = "";  // REQUEST的類型	char	1 傳入參數集內有ADREQTYPE參數時才寫入 A.為AJAX交易

	private String ADUSERIP = "";  // 操作者IP位址	varchar	20

	private String ADCONTENT = "";  // 操作記錄內容	varchar	3000

	private String ADEXCODE = "";  // 交易錯誤代碼	varchar	10

	private String LASTDATE = ""; // 最後修改日	char	8

	private String LASTTIME = "";  // 最後修改時間	char	6
	
	private String ADTXID = "";  //指出哪一個電文有誤

	private String ADGUID = "";

	private String LOGINTYPE = "";//判斷NB:網銀，MB:行動
	
	private String PSTIMEDIFF = "";//發送電文到回傳電文的時間
	
	public String getADTXID() {
		return ADTXID;
	}

	public void setADTXID(String adtxid) {
		ADTXID = adtxid;
	}

	public String getADGUID() {
		return ADGUID;
	}

	public void setADGUID(String adguid) {
		ADGUID = adguid;
	}

	public String getADAGREEF() {
		return ADAGREEF;
	}

	public void setADAGREEF(String adagreef) {
		ADAGREEF = adagreef;
	}

	public String getADCONTENT() {
		
		Map<String, String> paramsMap = JSONUtils.json2map(ADCONTENT);
		paramsMap.remove("N950PASSWORD");
		paramsMap.remove("CMPASSWORD");
		paramsMap.remove("CMPWD");		
				
		return JSONUtils.map2json(paramsMap);
	}

	public void setADCONTENT(String adcontent) {
		
		Map<String, String> paramsMap = JSONUtils.json2map(adcontent);
		paramsMap.remove("N950PASSWORD");
		paramsMap.remove("CMPASSWORD");
		paramsMap.remove("CMPWD");
		
		ADCONTENT = JSONUtils.map2json(paramsMap);
	}

	public String getADCURRENCY() {
		return ADCURRENCY;
	}

	public void setADCURRENCY(String adcurrency) {
		ADCURRENCY = adcurrency;
	}

	public String getADEXCODE() {
		return ADEXCODE;
	}

	public void setADEXCODE(String adexcode) {
		ADEXCODE = adexcode;
	}

	public String getADOPID() {
		return ADOPID;
	}

	public void setADOPID(String adopid) {
		ADOPID = adopid;
	}

	public String getADREQTYPE() {
		return ADREQTYPE;
	}

	public void setADREQTYPE(String adreqtype) {
		ADREQTYPE = adreqtype;
	}

	public String getADSVBH() {
		return ADSVBH;
	}

	public void setADSVBH(String adsvbh) {
		ADSVBH = adsvbh;
	}

	public String getADTXACNO() {
		return ADTXACNO;
	}

	public void setADTXACNO(String adtxacno) {
		ADTXACNO = adtxacno;
	}

	public String getADTXAMT() {
		return ADTXAMT;
	}

	public void setADTXAMT(String adtxamt) {
		ADTXAMT = adtxamt;
	}

	public String getADTXNO() {
		return ADTXNO;
	}

	public void setADTXNO(String adtxno) {
		ADTXNO = adtxno;
	}

	public String getADUSERID() {
		return ADUSERID;
	}

	public void setADUSERID(String aduserid) {
		ADUSERID = aduserid;
	}

	public String getADUSERIP() {
		return ADUSERIP;
	}

	public void setADUSERIP(String aduserip) {
		ADUSERIP = aduserip;
	}

	public String getFGTXWAY() {
		return FGTXWAY;
	}

	public void setFGTXWAY(String fgtxway) {
		FGTXWAY = fgtxway;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

	public String toString() {
		return new ToStringBuilder(this).append("adtxno", getADTXNO())
				.toString();
	}

	public String getLOGINTYPE() {
		return LOGINTYPE;
	}

	public void setLOGINTYPE(String logintype) {
		LOGINTYPE = logintype;
	}

	public String getPSTIMEDIFF() {
		return PSTIMEDIFF;
	}

	public void setPSTIMEDIFF(String pSTIMEDIFF) {
		PSTIMEDIFF = pSTIMEDIFF;
	}

}
