package fstop.orm.po;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "ADMCOUNTRY")
public class ADMCOUNTRY implements Serializable {

	@Id
	//@GeneratedValue(strategy = GenerationType.AUTO)
	private String ADCTRYCODE;

	private String ADCTRYNAME;

	private String ADCTRYENGNAME;

	private String LASTUSER;

	private String LASTDATE;

	private String LASTTIME;

//	private Set ADMCURRENCIES;

	public String toString() {
		return new ToStringBuilder(this).append("adctrycode", getADCTRYCODE())
				.toString();
	}

	public String getADCTRYCODE() {
		return ADCTRYCODE;
	}

	public void setADCTRYCODE(String adctrycode) {
		ADCTRYCODE = adctrycode;
	}

	public String getADCTRYENGNAME() {
		return ADCTRYENGNAME;
	}

	public void setADCTRYENGNAME(String adctryengname) {
		ADCTRYENGNAME = adctryengname;
	}

	public String getADCTRYNAME() {
		return ADCTRYNAME;
	}

	public void setADCTRYNAME(String adctryname) {
		ADCTRYNAME = adctryname;
	}

//	public Set getADMCURRENCIES() {
//		return ADMCURRENCIES;
//	}
//
//	public void setADMCURRENCIES(Set admcurrencies) {
//		ADMCURRENCIES = admcurrencies;
//	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

	public String getLASTUSER() {
		return LASTUSER;
	}

	public void setLASTUSER(String lastuser) {
		LASTUSER = lastuser;
	}

}
