package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "TXNFXSCHPAY")
@Data
public class TXNFXSCHPAY implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4451703626964325822L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	
	private Integer FXSCHID; // 外幣預約流水號

	private String ADOPID = ""; // 操作功能ID

	private String FXUSERID = ""; // 使用者ID

	private String FXTXTYPE = ""; // 單次/循環轉帳

	private String FXPERMTDATE = ""; // 每月轉帳日

	private String FXFDATE = ""; // 生效日

	private String FXTDATE = ""; // 截止日

	private String FXWDAC = ""; // 轉出帳號

	private String FXWDCURR = ""; // 轉出幣別

	private String FXWDAMT = ""; // 轉出金額

	private String FXSVBH = ""; // 轉入行庫代碼

	private String FXSVAC = ""; // 轉入帳號

	private String FXSVCURR = ""; // 轉入幣別

	private String FXSVAMT = ""; // 轉入金額

	private String FXTXMEMO = ""; // 備註

	private String FXTXMAILS = ""; // 發送Mail清單

	private String FXTXMAILMEMO = ""; // Mail備註

	private String FXTXCODE = ""; // 交易機制

	private String FXSDATE = ""; // 預約設定日期

	private String FXSTIME = ""; // 預約設定時間

	private String FXTXSTATUS = ""; // 預約狀態

	private String XMLCA = ""; // CA 識別碼

	private String XMLCN = ""; // 憑證ＣＮ

	private String MAC = ""; // MAC

	private String FXTXINFO = ""; // 預約時上行電文必要資訊

	private String LASTDATE = ""; // 最後異動日期

	private String LASTTIME = ""; // 最後異動時間

	private String FXSCHNO = ""; // 預約批號Guid(36位)

	private String LOGINTYPE = ""; // NB登入或MB登入

	private String MSADDR = "";// 所屬微服務

}
