package fstop.orm.po;

import lombok.Data;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TXNTWSCHPAY")
@Data
public class TXNTWSCHPAY implements Serializable {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -2864824137279713209L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	private Integer DPSCHID;//台幣預約流水號
	
	private String ADOPID = "";//操作功能ID
	private String DPUSERID = "";//使用者ID
	private String DPTXTYPE = "";//單次/循環轉帳
	private String DPPERMTDATE = "";//每月轉帳日
	private String DPFDATE = "";//生效日
	private String DPTDATE = "";//截止日
	private String DPWDAC = "";//轉出帳號
	private String DPSVBH = "";//轉入行庫代碼
	private String DPSVAC = "";//轉入帳號/繳費代號
	private String DPTXAMT = "";//轉帳金額
	private String DPTXMEMO = "";//備註
	private String DPTXMAILS = "";//發送Mail清單
	private String DPTXMAILMEMO = "";//Mail備註
	private String DPTXCODE = "";//交易機制
	private String DPSDATE = "";//預約設定日期
	private String DPSTIME = "";//預約設定時間
	private String DPTXSTATUS = "";//預約狀態
	private String XMLCA = "";//CA 識別碼
	private String XMLCN = "";//憑證ＣＮ
	private String MAC = "";//MAC
	private String LASTDATE = "";//最後異動日期
	private String LASTTIME = "";//最後異動時間
	private String DPSCHNO = "";//預約批號Guid(36位)
	private String LOGINTYPE = "";//NB登入或MB登入
	private String DPTXINFO = "";//預約時上行電文必要資訊
	private String MSADDR = "";//所屬微服務

//
//	public String getADOPID() {
//		return ADOPID;
//	}
//	public void setADOPID(String aDOPID) {
//		ADOPID = aDOPID;
//	}
//	public String getDPUSERID() {
//		return DPUSERID;
//	}
//	public void setDPUSERID(String dPUSERID) {
//		DPUSERID = dPUSERID;
//	}
//	public String getDPTXTYPE() {
//		return DPTXTYPE;
//	}
//	public void setDPTXTYPE(String dPTXTYPE) {
//		DPTXTYPE = dPTXTYPE;
//	}
//	public String getDPPERMTDATE() {
//		return DPPERMTDATE;
//	}
//	public void setDPPERMTDATE(String dPPERMTDATE) {
//		DPPERMTDATE = dPPERMTDATE;
//	}
//	public String getDPFDATE() {
//		return DPFDATE;
//	}
//	public void setDPFDATE(String dPFDATE) {
//		DPFDATE = dPFDATE;
//	}
//	public String getDPTDATE() {
//		return DPTDATE;
//	}
//	public void setDPTDATE(String dPTDATE) {
//		DPTDATE = dPTDATE;
//	}
//	public String getDPWDAC() {
//		return DPWDAC;
//	}
//	public void setDPWDAC(String dPWDAC) {
//		DPWDAC = dPWDAC;
//	}
//	public String getDPSVBH() {
//		return DPSVBH;
//	}
//	public void setDPSVBH(String dPSVBH) {
//		DPSVBH = dPSVBH;
//	}
//	public String getDPSVAC() {
//		return DPSVAC;
//	}
//	public void setDPSVAC(String dPSVAC) {
//		DPSVAC = dPSVAC;
//	}
//	public String getDPTXAMT() {
//		return DPTXAMT;
//	}
//	public void setDPTXAMT(String dPTXAMT) {
//		DPTXAMT = dPTXAMT;
//	}
//	public String getDPTXMEMO() {
//		return DPTXMEMO;
//	}
//	public void setDPTXMEMO(String dPTXMEMO) {
//		DPTXMEMO = dPTXMEMO;
//	}
//	public String getDPTXMAILS() {
//		return DPTXMAILS;
//	}
//	public void setDPTXMAILS(String dPTXMAILS) {
//		DPTXMAILS = dPTXMAILS;
//	}
//	public String getDPTXMAILMEMO() {
//		return DPTXMAILMEMO;
//	}
//	public void setDPTXMAILMEMO(String dPTXMAILMEMO) {
//		DPTXMAILMEMO = dPTXMAILMEMO;
//	}
//	public String getDPTXCODE() {
//		return DPTXCODE;
//	}
//	public void setDPTXCODE(String dPTXCODE) {
//		DPTXCODE = dPTXCODE;
//	}
//	public String getDPSDATE() {
//		return DPSDATE;
//	}
//	public void setDPSDATE(String dPSDATE) {
//		DPSDATE = dPSDATE;
//	}
//	public String getDPSTIME() {
//		return DPSTIME;
//	}
//	public void setDPSTIME(String dPSTIME) {
//		DPSTIME = dPSTIME;
//	}
//	public String getDPTXSTATUS() {
//		return DPTXSTATUS;
//	}
//	public void setDPTXSTATUS(String dPTXSTATUS) {
//		DPTXSTATUS = dPTXSTATUS;
//	}
//	public String getXMLCA() {
//		return XMLCA;
//	}
//	public void setXMLCA(String xMLCA) {
//		XMLCA = xMLCA;
//	}
//	public String getXMLCN() {
//		return XMLCN;
//	}
//	public void setXMLCN(String xMLCN) {
//		XMLCN = xMLCN;
//	}
//	public String getMAC() {
//		return MAC;
//	}
//	public void setMAC(String mAC) {
//		MAC = mAC;
//	}
//	public String getLASTDATE() {
//		return LASTDATE;
//	}
//	public void setLASTDATE(String lASTDATE) {
//		LASTDATE = lASTDATE;
//	}
//	public String getLASTTIME() {
//		return LASTTIME;
//	}
//	public void setLASTTIME(String lASTTIME) {
//		LASTTIME = lASTTIME;
//	}
//	public String getDPSCHNO() {
//		return DPSCHNO;
//	}
//	public void setDPSCHNO(String dPSCHNO) {
//		DPSCHNO = dPSCHNO;
//	}
//	public String getLOGINTYPE() {
//		return LOGINTYPE;
//	}
//	public void setLOGINTYPE(String lOGINTYPE) {
//		LOGINTYPE = lOGINTYPE;
//	}
//	public String getDPTXINFO() {
//		return DPTXINFO;
//	}
//	public void setDPTXINFO(String dPTXINFO) {
//		DPTXINFO = dPTXINFO;
//	}

	


}
