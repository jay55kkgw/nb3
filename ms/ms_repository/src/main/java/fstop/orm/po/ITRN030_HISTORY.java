package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

import lombok.Data;

@Entity
@Table(name = "ITRN030_HISTORY")
@Data
public class ITRN030_HISTORY implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3730899714980107653L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer HISTORYID;

	private String CUR;

	private String BUY;

	private String SELL;

	private String TYPE;

	private String DATE;
	
}
