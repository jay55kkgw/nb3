package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "TXNFXSCHEDULE")
public class TXNFXSCHEDULE implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long FXSCHID;

	private String ADOPID = "";
	
	private String FXUSERID = "";

	private String FXPERMTDATE = "";

	private String FXFDATE = "";

	private String FXTDATE = "";

	private String FXNEXTDATE = "";

	private String FXWDAC = "";

	private String FXSVAC = "";

	private String FXWDAMT = "";

	private String FXSVAMT = "";

	private String FXTXMEMO = "";

	private String FXTXMAILS = "";
	private String FXTXMAILMEMO = "";

	private String FXTXCODE = "";

	private Long FXTXINFO = 0l;

	private String FXSDATE = "";

	private String FXSTIME = "";

	private String FXTXSTATUS = "";

	private String FXSVCURR = "";

	private String FXWDCURR = "";
	
	private String FXSVBH = "";
	
	private String XMLCA = "";
	
	private String XMLCN = "";
	
	private String MAC = "";
	
	private String LASTDATE = "";
	
	private String LASTTIME = "";
	
	private String FXSCHNO = "";
	
	private String LOGINTYPE = ""; //NB:網銀 ， MB:行動 ，預設為NB
	
	
	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

	public String toString() {
		return new ToStringBuilder(this).append("fxschid", getFXSCHID())
				.toString();
	}

	public String getFXSVCURR() {
		return FXSVCURR;
	}

	public void setFXSVCURR(String fxsvcurr) {
		FXSVCURR = fxsvcurr;
	}

	public String getFXWDCURR() {
		return FXWDCURR;
	}

	public void setFXWDCURR(String fxwdcurr) {
		FXWDCURR = fxwdcurr;
	}

	public String getFXFDATE() {
		return FXFDATE;
	}

	public void setFXFDATE(String fxfdate) {
		FXFDATE = fxfdate;
	}

	public String getFXNEXTDATE() {
		return FXNEXTDATE;
	}

	public void setFXNEXTDATE(String fxnextdate) {
		FXNEXTDATE = fxnextdate;
	}

	public String getFXPERMTDATE() {
		return FXPERMTDATE;
	}

	public void setFXPERMTDATE(String fxpermtdate) {
		FXPERMTDATE = fxpermtdate;
	}

	public Long getFXSCHID() {
		return FXSCHID;
	}

	public void setFXSCHID(Long fxschid) {
		FXSCHID = fxschid;
	}

	public String getFXSDATE() {
		return FXSDATE;
	}

	public void setFXSDATE(String fxsdate) {
		FXSDATE = fxsdate;
	}

	public String getFXSTIME() {
		return FXSTIME;
	}

	public void setFXSTIME(String fxstime) {
		FXSTIME = fxstime;
	}

	public String getFXSVAC() {
		return FXSVAC;
	}

	public void setFXSVAC(String fxsvac) {
		FXSVAC = fxsvac;
	}

	public String getFXSVAMT() {
		return FXSVAMT;
	}

	public void setFXSVAMT(String fxsvamt) {
		FXSVAMT = fxsvamt;
	}

	public String getFXTDATE() {
		return FXTDATE;
	}

	public void setFXTDATE(String fxtdate) {
		FXTDATE = fxtdate;
	}

	public String getFXTXCODE() {
		return FXTXCODE;
	}

	public void setFXTXCODE(String fxtxcode) {
		FXTXCODE = fxtxcode;
	}

	public String getFXTXMAILS() {
		return FXTXMAILS;
	}

	public void setFXTXMAILS(String fxtxmails) {
		FXTXMAILS = fxtxmails;
	}

	public String getFXTXMEMO() {
		return FXTXMEMO;
	}

	public void setFXTXMEMO(String fxtxmemo) {
		FXTXMEMO = fxtxmemo;
	}

	public String getFXTXSTATUS() {
		return FXTXSTATUS;
	}

	public void setFXTXSTATUS(String fxtxstatus) {
		FXTXSTATUS = fxtxstatus;
	}

	public String getFXUSERID() {
		return FXUSERID;
	}

	public void setFXUSERID(String fxuserid) {
		FXUSERID = fxuserid;
	}

	public String getFXWDAC() {
		return FXWDAC;
	}

	public void setFXWDAC(String fxwdac) {
		FXWDAC = fxwdac;
	}

	public String getFXWDAMT() {
		return FXWDAMT;
	}

	public void setFXWDAMT(String fxwdamt) {
		FXWDAMT = fxwdamt;
	}

	public String getADOPID() {
		return ADOPID;
	}

	public void setADOPID(String adopid) {
		ADOPID = adopid;
	}

	public Long getFXTXINFO() {
		return FXTXINFO;
	}

	public void setFXTXINFO(Long fxtxinfo) {
		FXTXINFO = fxtxinfo;
	}

	public String getFXTXMAILMEMO() {
		return FXTXMAILMEMO;
	}

	public void setFXTXMAILMEMO(String fxtxmailmemo) {
		FXTXMAILMEMO = fxtxmailmemo;
	}

	public String getFXSVBH() {
		return FXSVBH;
	}

	public void setFXSVBH(String fxsvbh) {
		FXSVBH = fxsvbh;
	}

	public String getMAC() {
		return MAC;
	}

	public void setMAC(String mac) {
		MAC = mac;
	}

	public String getXMLCA() {
		return XMLCA;
	}

	public void setXMLCA(String xmlca) {
		XMLCA = xmlca;
	}

	public String getXMLCN() {
		return XMLCN;
	}

	public void setXMLCN(String xmlcn) {
		XMLCN = xmlcn;
	}

	public String getFXSCHNO() {
		return FXSCHNO;
	}

	public void setFXSCHNO(String fxschno) {
		FXSCHNO = fxschno;
	}

	public String getLOGINTYPE() {
		return LOGINTYPE;
	}

	public void setLOGINTYPE(String logintype) {
		LOGINTYPE = logintype;
	}

}
