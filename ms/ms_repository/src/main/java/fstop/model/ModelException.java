package fstop.model;

import fstop.exception.UncheckedException;

public class ModelException extends UncheckedException { 

	private static final long serialVersionUID = 7034496227113613816L;
	
	public ModelException(String msg) {
		super(msg);
	}
	public ModelException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
