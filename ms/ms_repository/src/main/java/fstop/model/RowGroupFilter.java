package fstop.model;

public interface RowGroupFilter {
	public String groupKey(Row row);
}
