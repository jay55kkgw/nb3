
package fstop.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Observable;
import java.util.Observer;

import fstop.util.CustomProperties;
import fstop.util.StrUtils;


/**
 * 透過檔案取得 name, value 所實際對上的中文描述
 * @author pierre
 *
 */

public class AdmoplogTranslator implements Observer  {
	
	private static AdmoplogTranslator instance = null;
	
	private CustomProperties realValue = new CustomProperties();
	
	public static AdmoplogTranslator getInstance() {
		if(instance == null) {
			synchronized (AdmoplogTranslator.class) {
				if(instance == null) {
					instance = new AdmoplogTranslator();
				}
			}
		}
		return instance;
	}
	
	private AdmoplogTranslator() {
	}
	
	public String transfer(String scope, String name) {
		String all = (StrUtils.trim(name));
		String scopeKey = (StrUtils.trim(scope) + "." + StrUtils.trim(name));
		if(realValue.containsKey(scopeKey)) {
			return (String)realValue.get(scopeKey);
		}
		else if(realValue.containsKey(all)) {
			return (String)realValue.get(all);
		}
		else {
			return "";
		}
	}
	
	public String transfer(String scope, String name, String value) {
		String all = (StrUtils.trim(name) + "." + StrUtils.trim(value)).toUpperCase();
		String scopeKey = (StrUtils.trim(scope) + "." + StrUtils.trim(name) + "." + StrUtils.trim(value)).toUpperCase();
		if(realValue.containsKey(scopeKey)) {
			return (String)realValue.get(scopeKey);
		}
		else if(realValue.containsKey(all)) {
			return (String)realValue.get(all);
		}
		else {
			return "";
		}
	}
	
	public void update(Observable o, Object arg) {
		//只對 arg 為 "admOpLogTranslatorPath:properties file path" 處理
		
		if(arg instanceof String) {
			String path = (String)arg;
			if(path != null && path.toUpperCase().startsWith("admOpLogTranslatorPath".toUpperCase())) {
				path = path.substring(path.indexOf(":") + 1);
				try {
					realValue.load(new InputStreamReader(new FileInputStream(new File(path)), "UTF8"));
				}
				catch(Exception e) {
					throw new fstop.model.ModelException("載入 admOpLogTranslator Properties 錯誤 !", e);
				}
			}
		}
	}

}
