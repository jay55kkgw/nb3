package fstop.model;

import java.util.List;
import java.util.Map;

public class MVHFieldTranslatorProxy implements MVH {

	private MVH mvh;
	private DefaultTranslator translator = DefaultTranslator.getInstance();
	
	public MVHFieldTranslatorProxy(MVH mvh) {
		this.mvh = mvh;
	}
	
	public void dump() {
		this.mvh.dump();
	}

	public String dumpToString() {
		return this.mvh.dumpToString();
	}

	public IMasterValuesHelper getTableByIndex(int index) {
		return this.mvh.getTableByIndex(index);
	}

	public IMasterValuesHelper getTableByName(String name) {
		return mvh.getTableByName(name);
	}

	public int getTableCount() {
		return mvh.getTableCount();
	}

	public List getTableKeys() {
		return mvh.getTableKeys();
	}

	public String getValueByFieldName(String fieldName) {
			return translator.transfer(mvh.getValueByFieldName("TXID"), fieldName, mvh.getValueByFieldName(fieldName));
	}

	public String getValueByFieldName(String fieldName, int index) {
		return translator.transfer(mvh.getValueByFieldName("TXID"), fieldName, mvh.getValueByFieldName(fieldName, index));
	}

	public int getValueOccurs(String fieldName) {
		return mvh.getValueOccurs(fieldName);
	}

	public boolean isOccurs(String fieldName) {
		return mvh.isOccurs(fieldName);
	}

	public IMasterValuesHelper translate() {
		return this;
	}

	public Map getInputQueryData() {
		return mvh.getInputQueryData();
	}
}
