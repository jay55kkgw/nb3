package fstop.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

import fstop.util.CustomProperties;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 透過檔案取得 name, value 所實際對上的中文描述
 * @author pierre
 *
 */
@Slf4j
public class B201Translator implements Observer  {

	private static B201Translator instance = null;

	private CustomProperties realValue = new CustomProperties();

	public static B201Translator getInstance() {
		if(instance == null) {
			synchronized (B201Translator.class) {
				if(instance == null) {
					instance = new B201Translator();
				}
			}
		}
		return instance;
	}

	private B201Translator() {
	}

	public String transfer(String scope, String name) {
		String all = (StrUtils.trim(name));
		String scopeKey = (StrUtils.trim(scope) + "." + StrUtils.trim(name));
		if(realValue.containsKey(scopeKey)) {
			return (String)realValue.get(scopeKey);
		}
		else if(realValue.containsKey(all)) {
			return (String)realValue.get(all);
		}
		else {
			return "";
		}
	}

	public String transfer(String scope, String name, String value) {
		String all = (StrUtils.trim(name) + "." + StrUtils.trim(value)).toUpperCase();
		String scopeKey = (StrUtils.trim(scope) + "." + StrUtils.trim(name) + "." + StrUtils.trim(value)).toUpperCase();
		if(realValue.containsKey(scopeKey)) {
			return (String)realValue.get(scopeKey);
		}
		else if(realValue.containsKey(all)) {
			return (String)realValue.get(all);
		}
		else {
			return "";
		}
	}

	public void update(Observable o, Object arg) {
		//只對 arg 為 "B201TranslatorPath:properties file path" 處理

		if(arg instanceof String) {
			String path = (String)arg;
			if(path != null && path.toUpperCase().startsWith("B201TranslatorPath".toUpperCase())) {
				path = path.substring(path.indexOf(":") + 1);
				try {
					realValue.load(new InputStreamReader(new FileInputStream(new File(path)), "UTF8"));
				}
				catch(Exception e) {
					throw new fstop.model.ModelException("載入 B201Translator Properties 錯誤 !", e);
				}
			}
		}
	}
	public String[] GetKeySet(String adopid) {

		int keyMax = Integer.parseInt((String)realValue.get(adopid+".MAX"));
		log.debug("keyMax==="+keyMax+"===");
		if(keyMax==0) return null;
		int iCount = 0;
		String rc[] = new String[keyMax];
		String key = adopid+".";
		Set keys = realValue.keySet();
		Iterator iterator = keys.iterator();
		while (iterator.hasNext())
		{
			String sKey =(String)iterator.next();
			if(sKey.startsWith(key) ) {
				if(!sKey.startsWith(key+"MAX") ) {
					log.debug("sKey==="+sKey+"==="+iCount);
					rc[iCount] = sKey;
					iCount++;
				}
			}
		}
		sort(rc,keyMax);
		return rc;
	}
	private void sort(String[] id,int keyMax)
	{
		log.debug("Start sort======");
		int max = keyMax;
		while (--max >= 0) {
			for (int j= 0;j<max;j++) {
				int jj = getInt(id[j]);
				int ii = getInt(id[j+1]);
				if (jj > ii) {
					String tmp = id[j];
					id[j] = id[j+1];
					id[j+1] = tmp;
				}
			}
		}
		//for(int i=0;i<keyMax;i++)
		//	log.debug("id["+i+"]==="+id[i]+"===");
	}

	private int getInt(String key)
	{
		int rc = 0;
		int dot1 = key.indexOf(".");
		//log.debug("dot1==="+dot1+"===");
		int dot2 = key.lastIndexOf(".");
		//log.debug("dot2==="+dot2+"===");
		String strInt = key.substring(dot1+1, dot2);
		//log.debug("strInt==="+strInt+"===");
		rc = Integer.parseInt(strInt);
		return rc;
	}
}
