package fstop.model;

import org.apache.log4j.Logger;

import java.util.*;
import java.util.Map.Entry;

public class RowsGroup {
	private Logger logger = Logger.getLogger(getClass());

	private Rows rows = null;
	private Map<GrpColumns, RowsSelecter> groups = null;
	private RowGroupFilter groupFilter = null;
	private String[] groupColumn;

	public RowsGroup(List<Row> _rows) {
		this(_rows, null);
	}

	public RowsGroup(List<Row> _rows, RowGroupFilter groupFilter) {
		rows = new Rows();
		for(Row r: _rows) {
			rows.addRow(r);
		}
		if(groupFilter != null)
			setGroupFilter(groupFilter);
	}

	public RowsGroup(Rows rows) {
		this(rows, null);
	}

	public RowsGroup(Rows rows, RowGroupFilter groupFilter) {
		this.rows = rows;
		if(groupFilter != null)
			setGroupFilter(groupFilter);
	}

	public void setGroupColumn(final String[] groupColumn) {
		this.groupColumn = groupColumn;
		setGroupFilter(new RowGroupFilter(){

			public String groupKey(Row row) {
				StringBuffer result = new StringBuffer();
				if(groupColumn != null) {
					for(int i=0; i < groupColumn.length; i++) {
						if(result.length() > 0) {
							result.append("_");
						}
						result.append(row.getValue(groupColumn[i]));
					}
				}
				return result.toString();
			}});
	}
	
	private void setGroupFilter(RowGroupFilter grouper) {
		if(grouper == null)
			new ModelException("傳入的 GroupFilter 不可為 Null.");
		this.groups = new LinkedHashMap<GrpColumns, RowsSelecter>();
		this.groupFilter = grouper;
		LinkedHashMap<String, List<Row>> gr = new LinkedHashMap();
		for(int i=0; i < rows.getSize(); i++) {
			String groupKey = grouper.groupKey(rows.getRow(i));
			if(!gr.containsKey(groupKey))
				gr.put(groupKey, new ArrayList<Row>());
			gr.get(groupKey).add(rows.getRow(i));
		}
		for(Entry<String, List<Row>> ent : gr.entrySet()) {
			Map<String, String> c = new HashMap();
			for(String s : this.groupColumn) {
				c.put(s, ent.getValue().get(0).getValue(s));
			}		
			this.groups.put(new GrpColumns(c, this.groupColumn), new RowsSelecter(new Rows(ent.getValue())));
		}
	}

	public Iterator<RowsSelecter> groupIterator() {
		return groups.values().iterator();
	}
	
	public Map<GrpColumns, RowsSelecter> getGroups() {
		return groups;
	}
	
	public Map<GrpColumns, Double> sum(String columnName) {
		Map<GrpColumns, Double> result = new LinkedHashMap();
		for(Entry<GrpColumns, RowsSelecter> ent : groups.entrySet()) {
			result.put(ent.getKey(), ent.getValue().sum(columnName));
		}
		return result;
	}
	
	public Map<GrpColumns, Integer> sumtoColumn(String columnName) {
		Map<GrpColumns, Integer> result = new LinkedHashMap();
		for(Entry<GrpColumns, RowsSelecter> ent : groups.entrySet()) {
			result.put(ent.getKey(), ent.getValue().sumtoColumn(columnName));
		}
		return result;
	}
	
	public Map<GrpColumns, Double> avg(String columnName) {
		Map<GrpColumns, Double> result = new LinkedHashMap();
		for(Entry<GrpColumns, RowsSelecter> ent : groups.entrySet()) {
			//logger.debug("key : " + ent.getKey().getValue("CRY"));
			result.put(ent.getKey(), ent.getValue().avg(columnName));
		}
		return result;
	}
	
}
