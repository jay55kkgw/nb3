package fstop.model;

public interface RowFilterCallback {
	public boolean accept(Row row);
}
