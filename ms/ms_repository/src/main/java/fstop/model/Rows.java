package fstop.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Rows implements Serializable {
	private List<Row> rows;  //colname, value
	
	public Rows() {
		this.rows = new ArrayList<Row>();
	}
	
	public Rows(List<Row> rows){
		this.rows = rows; 
	}
	
	public Set<String> getColumnNames() {
		if(rows != null && rows.size() > 0) {
			return rows.get(rows.size()-1).getColumnNames();
		}
		else {
			return new HashSet();
		}
	}
	
	public void addRow(Row row) {
		rows.add(row);
	}

	public void addRows(Rows addrows) {
		for(int i=0; addrows != null && i < addrows.getSize(); i++) {
			this.rows.add(addrows.getRow(i));
		}
	}
	
	public void removeRow(Row row) {
		rows.remove(row);
	}
	
	public Row getRow(int index) {
		return rows.get(index);
	}

	public int getSize() {
		return rows.size();
	}
	
	public boolean isIncludeColumn(String name) {
		return  getColumnNames().contains(name);
	}

	public List<Row> getRows() {
		return rows;
	}
	
	public void setValue(String columnName, String value) {
		for(Row r : rows) {
			r.setValue(columnName, value);
		}
	}

}
