package fstop.model;

import fstop.util.StrUtils;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class MVHUtils {

	/**
	 * 對指定的 occur key 做 group by 的動作, 並寫到 table 去
	 * @param telcommResult
	 * @param groupKey
	 */
	public static Map<String, IMasterValuesHelper> groupByToTables(MVHImpl telcommResult, String[] groupName) {
		Map<String, IMasterValuesHelper> result = new LinkedHashMap();
		try {
			RowsGroup rowsGroup = new RowsGroup(telcommResult.getOccurs());
			rowsGroup.setGroupColumn(groupName);
			
			Map<GrpColumns, RowsSelecter> groups = rowsGroup.getGroups();
			for(GrpColumns grpCol : groups.keySet()) {
				String grpName = grpCol.toString();
				
				Rows rows = new Rows(groups.get(grpCol).getSelectedRows());
				MVHImpl mvh = new MVHImpl(rows);
				
				for(String col : grpCol.getColumnNames()) {
					if(rows.getSize() > 0) {
						mvh.getFlatValues().put(col, rows.getRow(0).getValue(col));
					}
				}
				
				result.put(grpName, mvh);
			}
		}
		catch(Exception e) {
			throw new ModelException("MVHUtils.groupByToTables 發生錯誤 !", e);
		}
		return result;
	}
	
	public static void addTables(MVHImpl mater, Map<String, IMasterValuesHelper> tables) {
		for(Entry<String, IMasterValuesHelper> o : tables.entrySet()) {
			mater.addTable(o.getValue(), o.getKey());
		}
	}
	
	public static RowsGroup group(Rows ros, String[] groupName) {
		RowsGroup rowsGroup = new RowsGroup(ros);
		rowsGroup.setGroupColumn(groupName);
		return rowsGroup;
	}

	public static RowsGroup group(List<Row> ros, String[] groupName) {
		RowsGroup rowsGroup = new RowsGroup(ros);
		rowsGroup.setGroupColumn(groupName);
		return rowsGroup;
	}

	public static RowsSelecter select(Rows ros, RowFilterCallback rowFilter) {
		RowsSelecter rowsSelecter = new RowsSelecter(ros);
		rowsSelecter.setFilter(rowFilter);
		return rowsSelecter;
	}

	public static MVHImpl toIMasterValuesHelperImpl(List<Row> rowList) {
		Rows rows = new Rows(rowList);
		return new MVHImpl(rows);
	}

	public static Map<String, String> toFlatValues(GrpColumns col, String keyPrefix, int index) {
		Map<String, String> result = new HashMap();
		for(String colName : col.getColumnNames()) {
			result.put(StrUtils.trim(keyPrefix) + colName + "_" + index, col.getValue(colName) + "");
		}
		return result;
	}
	
	/**
	 * 對 MVH 裡的所有 Table 裡的某一個 occurs 欄位, 做加總
	 * @param mvh
	 * @param column
	 * @return
	 */
	public static Double sumTablesOccurColumn(MVHImpl mvh, String column) {
		Double result = 0d;
		
		for(int i = 0; i < mvh.getTableCount(); i++) {
			MVHImpl imv = (MVHImpl)mvh.getTableByIndex(i + 1);
			for(int x = 0; imv.isOccurs(column) && x < imv.getValueOccurs(column); x++) {
				
				try {
					String v = imv.getOccurs().getRow(x).getValue(column);
					result += new Double(v);
				}
				catch(Exception e) {}
			}
		}
		
		return result;
	}
	
	/**
	 * 對 MVH 裡的所有 Table 裡的某一個不是 occurs 的欄位, 做加總
	 * @param mvh
	 * @param column
	 * @return
	 */
	public static Double sumTablesFlatColumn(MVHImpl mvh, String column) {
		Double result = 0d;
		
		for(int i = 0; i < mvh.getTableCount(); i++) {
			MVHImpl imv = (MVHImpl)mvh.getTableByIndex(i + 1);
			String v = imv.getFlatValues().get(column);
			try {
				result += new Double(v);
			}
			catch(Exception e) {}
		}
		
		return result;
	}
	
	public static Long sumTablesOccurs(MVHImpl mvh) {
		Long result = 0L;
		
		for(int i = 0; i < mvh.getTableCount(); i++) {
			MVHImpl imv = (MVHImpl)mvh.getTableByIndex(i + 1);
			result += imv.getOccurs().getSize();
		}
		
		return result;
	}
	
	
}
