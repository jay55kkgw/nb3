package fstop.model;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.netbank.util.ESAPIUtil;

import fstop.core.BeanUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
@Slf4j
public class DBResult extends MVHImpl implements Serializable {
	
	public DBResult(String[] columns, List queryResult) {
		super();
		Map<String, Object> tempValues = new HashMap<String, Object>();
		Map<String, Integer> mIndexRec = new HashMap<String, Integer>();
		 
		if(queryResult != null) {
			
			for(Object o : queryResult) {
				for(String col : columns) {
					
					if(!mIndexRec.containsKey(col)) {
						mIndexRec.put(col, new Integer(0));
					}
					
					try {
						String beanValue = BeanUtils.getProperty(o, col);
						//無法轉換物件
						if(StrUtils.isNotEmpty(beanValue) && beanValue.startsWith("fstop.orm.po")) {
							
						}
						mIndexRec.put(col, mIndexRec.get(col) + 1);
						tempValues.put(col + "_" + mIndexRec.get(col), beanValue);
					} catch (IllegalAccessException e) {
						log.error("", e);
					} catch (InvocationTargetException e) {
						log.error("", e);
					} catch (NoSuchMethodException e) {
						log.error("", e);
					}
				}
				 
			}
		} 

		//對 Occurs 的部份做處理
		Set<String> colnames = mIndexRec.keySet();
		int max = 0;
		for(Integer index : mIndexRec.values()) {
			if(index > max)
				max = index;
		}
		for(int i=0; i < max; i++) {
			Map<String, String> r = new HashMap();
			for(String coln : colnames) {
				//將Hashmap 變成 "欄位名稱", 及 "值"
				String key = coln + "_" + (i+1);
				r.put(coln, (String)tempValues.get(key));
			}
			getOccurs().addRow(new Row(r));
		}		
		
	}
	public DBResult(List queryResult) {
		super();
		Map<String, Object> tempValues = new HashMap<String, Object>();
		Map<String, Integer> mIndexRec = new HashMap<String, Integer>();
		 
		if(queryResult != null) {
			
			for(Object o : queryResult) {
				
				try {
					Map<String, String> bean = BeanUtils.describe(o);

					for(String col : bean.keySet()) {
						
						if(!mIndexRec.containsKey(col)) {
							mIndexRec.put(col, new Integer(0));
						}
						
						// TODO TXNLOANAPPLY 有某些欄位是null值(VERSION, LOGINTYPE)，造成NullPointException
						// 為了讓程式能運作，先把null欄塞空字串
						// 待查明null原因後，可移除此if區塊
						if(bean.get(col) == null)
						{
							log.error(ESAPIUtil.vaildLog(col + " is Null"));
							bean.put(col, "");
						}
						
						String beanValue = bean.get(col).toString();
						mIndexRec.put(col, mIndexRec.get(col) + 1);
						tempValues.put(col + "_" + mIndexRec.get(col),
								beanValue);
						
					}
				} catch (IllegalAccessException e) {
					log.error("", e);
				} catch (InvocationTargetException e) {
					log.error("", e);
				} catch (NoSuchMethodException e) {
					log.error("", e);
				}				
				
			}
		} 

		//對 Occurs 的部份做處理
		Set<String> colnames = mIndexRec.keySet();
		int max = 0;
		for(Integer index : mIndexRec.values()) {
			if(index > max)
				max = index;
		}
		for(int i=0; i < max; i++) {
			Map<String, String> r = new HashMap();
			for(String coln : colnames) {
				//將Hashmap 變成 "欄位名稱", 及 "值"
				String key = coln + "_" + (i+1);
				r.put(coln, (String)tempValues.get(key));
			}
			getOccurs().addRow(new Row(r));
		}		
		
	}

}
