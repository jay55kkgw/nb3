package fstop.model;

import java.util.List;
import java.util.Map;

/**
 * @author Administrator
 *
 */
public interface IMasterValuesHelper {

    ///**
    // * 取得組成 IMasterValuesHelper 所有電文交易過程
    // * @return
    // */
    //public List getTelcommProcesses();

    /*
     * 由JSP上傳的所有 Parameter 資料
     */
    public Map getInputQueryData();

    /*
     * 取得 fieldName 所對應的內容
     */
    public String getValueByFieldName(String fieldName);

    /*
     * 若 fieldName 所對應的內容為多筆資料, 則回傳第 index 筆資料 , index 由 1 開始,
     * 若非多筆資料, 則會回傳 getValueByFieldName(String fieldName) 的資料
     */
    public String getValueByFieldName(String fieldName, int index);


    public int getTableCount();

    public List getTableKeys();

    public IMasterValuesHelper getTableByIndex(int index);

    public IMasterValuesHelper getTableByName(String name);


    /*
     * 取得某一個 field 的迴圈次數
     */
    public int getValueOccurs(String fieldName);

    /*
     * 回傳 filedName 所對應的內容是否為多筆資料
     */
    public boolean isOccurs(String fieldName);

    public IMasterValuesHelper translate();

    public void dump();
    public String dumpToString();
}
