package fstop.model;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

public class Row implements Serializable {
	Map<String, String> row;  //colname, value
	public Row(Map<String, String> row){
		this.row = row;
	}
	
	public Set<String> getColumnNames() {
		return row.keySet();
	}
	
	public String getValue(String colname) {
		return row.get(colname);
	}
	
	public void setValue(String columnName, String value) {
		row.put(columnName, value);
	}
	
	public Map<String, String> getValues() {
		return row;
	}	
}
