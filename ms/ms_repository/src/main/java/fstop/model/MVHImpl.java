package fstop.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.annotation.JsonIgnore;

import fstop.util.JSONUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 只要是 key 為 OCCURS_ 開頭的, 會被放到 occurs 裡去, 只有在 constructer 時才會動作
 * @author jimmy
 *
 */
@Slf4j
public class MVHImpl implements MVH, Serializable {

	@JsonIgnore
	private Logger logger = Logger.getLogger(MVHImpl.class);

	private Map flatValues = null;
	private Rows occurs = null;   //發生 occurs 的地方
	
	private HashMap<String, IMasterValuesHelper> tables_alias = new LinkedHashMap();
	private HashMap<String, IMasterValuesHelper> tables = new LinkedHashMap();

	private MVHFieldTranslatorProxy translator = new MVHFieldTranslatorProxy(this);
	
	public MVHImpl() {
		this(new HashMap());
	}	
	
	public MVHImpl(Map<String, String> flatValues) {
		this(flatValues, new Rows());
	}
	
	public MVHImpl(Rows occurs) {
		this(new HashMap(), occurs);
	}
	
	public MVHImpl(Map<String, String> flatValues, Rows occurs) {
		this.flatValues = flatValues;
		this.occurs = occurs;
		
		Set<String> colnames = new HashSet<String>();
		int max = 0;
		for(String key : flatValues.keySet()) {
			if(key.toUpperCase().startsWith("OCCURS_") && key.indexOf('_', 7) >= 0 ) {
				colnames.add(key.substring(7, key.indexOf('_', 7)));
				int index = new Integer(key.substring(key.indexOf('_', 7) + 1));
				if(index > max)
					max = index;
			}
		}
		
		for(int i=0; i < max; i++) {
			Map<String, String> r = new HashMap();
			for(String coln : colnames) {
				String key = "OCCURS_" + coln + "_" + (i+1);
				r.put(coln, flatValues.get(key));
				flatValues.remove(key);
			}
			occurs.addRow(new Row(r));
		}
	}
	
//	public void setFlatValues(Map result) {
//		this.flatValues = result;
//	}
	
	public Map<String, String> getFlatValues() {
		return this.flatValues;
	}
	
	public Rows getOccurs() {
		return this.occurs;
	}	
	
	public Rows getOccursByUID(String uidColumnName, String realUID) {

		Rows returnValues = new Rows();
		
		for (int i=0; i < occurs.getSize(); i++) {
			
			Row row = occurs.getRow(i);
			String telcommUID = row.getValue(uidColumnName);
									
			//若下行電文之UID欄位值 == 登入之UID值 => 加入回傳之結構中
			if (telcommUID.equals(realUID)) {				
				returnValues.addRow(row);
			}		
		}// end for i

		return returnValues;
	}	
	
	
	public void setInputQueryData(Map inputQueryData) {
		this.inputQueryData = inputQueryData;
	}

	public Map getInputQueryData() {
		return inputQueryData;
	}


	public String getValueByFieldName(String fieldName) {
		if("BOQUERYNEXT".equals(fieldName)) {
			Map h = new HashMap();
			h.putAll(getInputQueryData());
			return JSONUtils.map2json(h);
		}
		return StrUtils.trim((String)flatValues.get(fieldName));
	}

	public String getValueByFieldName(String fieldName, int index) {
		
		
		String result = "";
		if(occurs.isIncludeColumn(fieldName)) {
			try {
				result = StrUtils.trim(occurs.getRow(index -1).getValue(fieldName));
				return result;
			}
			catch(Exception e) {
				//在 rows 找不到
				log.error("getRow error>>{}", e);
			}
		}
		else if(occurs.getSize() >= index && occurs.getRow(index-1).getColumnNames().contains(fieldName)) {
			try {
				result = StrUtils.trim(occurs.getRow(index -1).getValue(fieldName));
				return result;
			}
			catch(Exception e) {
				//在 rows 找不到
				log.error("getRow error>>{}", e);
			}
		}
		else if(flatValues.containsKey(fieldName + "_1")) {
			result = (String)flatValues.get(fieldName + "_" + index);
		}
		else {
			result = (String)flatValues.get(fieldName);
		}
		return StrUtils.trim(result);
	}

	public int getValueOccurs(String fieldName) {
		if(occurs.isIncludeColumn(fieldName)) {
			return occurs.getSize();
		}
		else if(flatValues.containsKey(fieldName + "_1")) {
			int result = 2;
			while(flatValues.containsKey(fieldName + "_" + result)) {
				result++;
			}
			return result - 1;
		}
		else if(flatValues.containsKey(fieldName)) {
			return 1;
		}
		else {
			return 0;
		}
	}

	public boolean isOccurs(String fieldName) {
		return occurs.isIncludeColumn(fieldName) ||
			flatValues.containsKey(fieldName + "_1") ;
	}

	
	public void addTable(IMasterValuesHelper table) {
		tables_alias.put("table_" + (tables.size() + 1), table);
		tables.put("table_" + (tables.size() + 1), table);
	}

	/**
	 * 將目前這個 table 加成 tables.size() + 1 個 table, 並加上 alias name
	 * @param table
	 * @param alias
	 */
	public void addTable(IMasterValuesHelper table, String alias) {
		if(!tables.containsKey(alias)) {
			tables.put(alias, table);
			tables_alias.put("table_" + (tables_alias.size() + 1), table);
		}
		else {
			tables.put(alias, table);
		}
		
	}

	public IMasterValuesHelper getTableByIndex(int index) {
		return tables_alias.get("table_" + index);
	}

	public IMasterValuesHelper getTableByName(String name) {
		return tables.get(name);
	}

	public int getTableCount() {
		return tables.size();
	}

	public Vector getTableKeys() {
		return new Vector(tables.keySet());
	}

	
	public List<String> getOccurKeys() {
		ArrayList result = new ArrayList();
		result.addAll(occurs.getColumnNames());
		return result;
	}
	

	public String dumpToString() {
		StringBuffer result = new StringBuffer();
//		result.append(DumpUtils.dumpToString(flatValues)).append("\r\n");
//		result.append(DumpUtils.dumpToString(this.getOccurs())).append("\r\n");
		
		for(String tableName : (Vector<String>)this.getTableKeys()) {
			result.append("@@Table Result [ " + tableName+ " ]: ------------------\r\n");
			result.append(this.getTableByName(tableName).dumpToString()).append("\r\n");
		}		
		
		return result.toString();
	}
	
	public void dump() {
//		DumpUtils.dump(flatValues);
//		DumpUtils.dump(this.getOccurs());
		
		for(String tableName : (Vector<String>)this.getTableKeys()) {
			//logger.debug("@@Table Result [ " + tableName+ " ]: ------------------");
			this.getTableByName(tableName).dump();
		}
	}

	private Map inputQueryData = new HashMap();
	public IMasterValuesHelper translate() {
		return translator;
	}

}
