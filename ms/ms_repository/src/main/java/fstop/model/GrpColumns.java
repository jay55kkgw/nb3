package fstop.model;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.util.Map;
import java.util.Set;

public class GrpColumns {
	Map<String, String> values;  //colname, value
	String[] cols;
	public GrpColumns(Map<String, String> values, String[] cols){
		this.values = values;
		this.cols = cols;
	}
	
	public Set<String> getColumnNames() {
		return values.keySet();
	}
	
	public String getValue(String colname) {
		return values.get(colname);
	}
	
	public void setValue(String columnName, String value) {
		values.put(columnName, value);
	}
	
	public String toString() {
		StringBuffer result = new StringBuffer();
		for(String s : this.cols) {
			if(result.length() > 0)
				result.append("_");
			result.append(values.get(s));
		}
		return result.toString();
	}
	
	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if (!(other instanceof GrpColumns))
			return false;
		GrpColumns castOther = (GrpColumns) other;
		EqualsBuilder b = new EqualsBuilder();
		for(String col : cols) {
			b.append(this.getValue(col), castOther.getValue(col));
		}
		
		return b.isEquals();
	}

	public int hashCode() {
		HashCodeBuilder h = new HashCodeBuilder();
		for(String col : cols) {
			h.append(col);
		}
		
		return h.toHashCode();
	}
	
}
