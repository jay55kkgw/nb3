package fstop.model;

import fstop.util.CustomProperties;
import fstop.util.StrUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Observable;
import java.util.Observer;

import com.netbank.util.ESAPIUtil;

/**
 * 透過檔案取得 name, value 所實際對上的中文描述
 * @author jimmy
 *
 */
public class DefaultTranslator implements Observer {
	
	private static DefaultTranslator instance = null;
	
	private CustomProperties realValue = new CustomProperties();
	
	public static DefaultTranslator getInstance() {
		if(instance == null) {
			synchronized (DefaultTranslator.class) {
				if(instance == null) {
					instance = new DefaultTranslator();
				}
			}
		}
		return instance;
	}
	
	private DefaultTranslator() {
	}
	
	public String transfer(String scope, String name, String value) {
		
		scope = ESAPIUtil.validInput(scope, "GeneralString", true);
		name = ESAPIUtil.validInput(name, "GeneralString", true);
		value = ESAPIUtil.validInput(value, "GeneralString", true);
		
		String all = (StrUtils.trim(name) + "." + StrUtils.trim(value)).toUpperCase();
		String scopeKey = (StrUtils.trim(scope) + "." + StrUtils.trim(name) + "." + StrUtils.trim(value)).toUpperCase();
		String resultStr = "";
		
		if(realValue.containsKey(scopeKey)) {
			resultStr = (String)realValue.get(scopeKey);
			resultStr = ESAPIUtil.validInput(resultStr, "GeneralString", true);
		}
		else if(realValue.containsKey(all)) {
			resultStr = (String)realValue.get(all);
			resultStr = ESAPIUtil.validInput(resultStr, "GeneralString", true);
		}
		
		return resultStr;
	}
	
	public void update(Observable o, Object arg) {
		//只對 arg 為 "TranslatorPath:properties file path" 處理
		
		if(arg instanceof String) {
			String path = (String)arg;
			if(path != null && path.toUpperCase().startsWith("TranslatorPath".toUpperCase())) {
				path = path.substring(path.indexOf(":") + 1);
				try {
					realValue.load(new InputStreamReader(new FileInputStream(new File(path)), "UTF8"));
				}
				catch(Exception e) {
					throw new ModelException("載入 FieldTranslator Properties 錯誤 !", e);
				}
			}
		}
	}

}
