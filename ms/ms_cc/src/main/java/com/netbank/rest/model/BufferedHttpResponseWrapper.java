//package com.netbank.rest.model;
//
//import java.io.IOException;
//import java.io.PrintWriter;
//import java.io.StringWriter;
//import java.io.UnsupportedEncodingException;
//
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpServletResponseWrapper;
//
//import lombok.extern.slf4j.Slf4j;
//
///**
// * This class wraps a HttpServletResponse with a buffered output. You can use
// * this to forward or include a Servlet or JSP page and capture the output from
// * it.
// * 
// * Use getOutput to get the output which was written to the response. Only
// * buffers the Writer. Not the OutputStream !!
// * 
// * @author Joost den Boer
// */
//@Slf4j
//public class BufferedHttpResponseWrapper extends HttpServletResponseWrapper {
//
//	PrintWriter writer = null;
//
//	//ByteArrayOutputStream baos = null;
//	
//	StringWriter sw = new StringWriter();
//	
//	String encoding = "UTF-8";
//	
//	HttpServletResponse response;
//
//	/**
//	 * Constructor for BufferedHttpResponseWrapper. Create a new buffered Writer
//	 * 
//	 * @param response
//	 *            The response object to wrap
//	 */
//	public BufferedHttpResponseWrapper(HttpServletResponse response) {
//		super(response);
//		this.response = response;
//		//baos = new ByteArrayOutputStream();
//	}
//
//	/**
//	 * Return the buffered Writer
//	 * 
//	 * @see javax.servlet.ServletResponse#getWriter()
//	 */
////	public PrintWriter getWriter() throws IOException {
////		String encoding = this.getResponse().getCharacterEncoding();
////		if(encoding == null || encoding.length() == 0)
////			encoding = "UTF-8";
////		this.writer = new PrintWriter(sw);
////		return writer;
////	}
//
//	/**
//	 * Return the output written to the Writer. To get the output, the Writer
//	 * must be flushed and closed. The content is captured by the
//	 * ByteArrayOutputStream.
//	 * 
//	 * @return
//	 */
//	public String getOutput() {
//		writer.flush();
//		writer.close();
//		try {
//			String result = sw.toString();
//			sw.close();
//			return result;
//		}
//		catch(UnsupportedEncodingException e) {
//			log.error("getOutput error >> {}", e);
//		} catch (IOException e) {
//			log.error("getOutput error >> {}", e);
//		}
//		return null;
//	}
//}
