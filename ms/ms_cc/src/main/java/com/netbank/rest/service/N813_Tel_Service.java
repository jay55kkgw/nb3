package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netbank.rest.model.CommonPools;
import com.netbank.util.AcnoTypeEnum;
import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import lombok.extern.slf4j.Slf4j;

/**
 * 主要用來 1.丟入MVH前的前置處理，包含檢核; 2.後製MVH回覆的訊息;
 * 
 * @author Hugo
 *
 */
@Service
@Slf4j
public class N813_Tel_Service extends Common_Tel_Service {

	@Autowired
	private CommonPools commonPools;

	@Override
	public HashMap pre_Processing(Map request) {
		// TODO Auto-generated method stub
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) {
		MVH mvh = new MVHImpl();
		// call N813Pool
		try {
			log.trace(ESAPIUtil.vaildLog("CUSIDN N813 process >> {}"+ (String) request.get("CUSIDN")));
			mvh = commonPools.n813.getAcnoList((String) request.get("CUSIDN"));

		} catch (NullPointerException e) {
			log.error("process.error >> {}", e);
		} catch (IllegalArgumentException e) {
			log.error("", e);
		}catch (TopMessageException e) {
			log.error("process.error>>",e);
			MVHImpl m = new MVHImpl();
			m.getFlatValues().put("TOPMSG", e.getMsgcode());
			mvh = m;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error("", e);
		}
		return data_Retouch(mvh);
	}

	@Override
	public HashMap data_Retouch(MVH mvh) {
		MVHImpl mvhimpl = null;
		LinkedList<Row> tmplist = null;
		Boolean result = Boolean.FALSE;
		HashMap<String, Object> dataMap = new HashMap<String, Object>();
		HashMap<String, String> rowMap = new HashMap<String, String>();
		LinkedList<HashMap<String, String>> rowlist = null;
		try {
			mvhimpl = (MVHImpl) mvh;
			log.info("getFlatValues>>" + mvhimpl.getFlatValues());
			log.info("result.getInputQueryData()>>" + mvhimpl.getInputQueryData());
			log.info("result.getInputQueryData()>>" + mvhimpl.getOccurs().getRows());
			Iterator<Row> tmp = mvhimpl.getOccurs().getRows().iterator();
			tmplist = new LinkedList<Row>();
			rowlist = new LinkedList<HashMap<String, String>>();
			tmp.forEachRemaining(tmplist::add);

			// TD02需要卡號列表放入隱藏欄位故新增一個String
			String cardList = "";

			log.info("datalist()>>" + tmplist);
			if (tmplist.size() > 0) {
				for (Row row : tmplist) {
					log.info("getValues>>" + row.getValues());
					rowMap = new HashMap<String, String>();
					rowMap.putAll(row.getValues());
					rowlist.add(rowMap);
				}
				rowlist.removeIf(Map::isEmpty);// 移除空白 OR null的Map
				rowlist.removeIf(Map -> null == Map.get("CARDNUM") || "".equals(Map.get("CARDNUM")));// 移除CARDNUM==空白 OR
																										// null的Map
				if ("".equals(mvhimpl.getFlatValues().get("TOPMSG")) && null == mvhimpl.getFlatValues().get("TOPMSG")) {
					for (Map<String, String> cardNum : rowlist) {
						cardList += cardNum.get("CARDNUM") + ",";
					}
				}
				dataMap.putAll(mvhimpl.getFlatValues());
				dataMap.put("REC", rowlist);
			}else {
				dataMap.putAll(mvhimpl.getFlatValues());
				dataMap.put("REC", rowlist);
			}
			if (!"".equals(cardList)) {
				dataMap.put("CARDLIST", cardList.substring(0, cardList.length() - 1));
			}
			result = Boolean.TRUE;

		} catch (Exception e) {
			log.error("data_Retouch error >> {} ", e);
		} finally {
			setMsgCode(dataMap, result);
		}

		return dataMap;

	}

	@Override
	public void setMsgCode(Map dataMap, Boolean data_Retouch_result) {
		// TODO Auto-generated method stub
		super.setMsgCode(dataMap, data_Retouch_result);
	}

}
