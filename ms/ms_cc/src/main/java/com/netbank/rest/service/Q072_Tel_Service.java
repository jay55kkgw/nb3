package com.netbank.rest.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.netbank.util.StrUtils;

import fstop.model.MVH;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Ian
 *
 */
@Service
@Slf4j
public class Q072_Tel_Service extends Common_Tel_Service
{

	@Override
	public HashMap pre_Processing(Map request)
	{
		// TODO Auto-generated method stub
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request)
	{
		// TODO Auto-generated method stub
		return super.process(serviceID, request);
	}

	@Override
	public HashMap data_Retouch(MVH mvh)
	{
		// TODO Auto-generated method stub
		return super.data_Retouch(mvh);
	}

	@Override
	public void setMsgCode(Map dataMap, Boolean data_Retouch_result)
	{
		String val = "";
		
		try {
			log.trace("data_Retouch_result>>{}",data_Retouch_result);
			log.trace("dataMap>>{}",dataMap);
//			預設就先設定是錯誤的狀況
			dataMap.put("msgCode", "FE0003");
			
			if(!data_Retouch_result) {
				return;
			}
			
			String[] msgkey = {"occurMSG", "occurMSG1"};
			List<String> list = Arrays.asList(msgkey);
			for (String key : list) {
				if (dataMap.containsKey(key)) {
					val = (String) dataMap.get(key);
					log.trace("val>>{}",val);
					
					if ( "0000".equals(val) ) {
						
						dataMap.put("msgCode", "0");
						
					} else {
						dataMap.put("msgCode", dataMap.get(key));
					}
					break;
				} // if end
			} // for end
			
			//新增 根據訊息代碼取得訊息
			if(null == dataMap.get("msgCode") || "0".equals(dataMap.get("msgCode"))) {
				log.debug("dataMap.get(msgcode) >>>>>>> {}",dataMap.get("msgCode"));
			}else {
				String occurMSG1DESC = String.valueOf(dataMap.get("occurMSG1DESC")); // 跨行錯誤訊息
				if (StrUtils.isNotEmpty(occurMSG1DESC)) {
					dataMap.put("msgName", occurMSG1DESC);
				} else {
					dataMap.put("msgName", getMessageByMsgCode(String.valueOf(dataMap.get("msgCode"))));
				}
			}
			
			
		} catch (Exception e) {
			log.error("setMsgCode error >> {}",e);
			dataMap.put("msgCode", "FE0003");
		}
	}

}
