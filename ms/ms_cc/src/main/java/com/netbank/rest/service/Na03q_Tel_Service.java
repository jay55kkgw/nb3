package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import org.springframework.stereotype.Service;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import lombok.extern.slf4j.Slf4j;

/**
 * NA03Q 線上申請信用卡
 *
 */
@Service
@Slf4j
public class Na03q_Tel_Service extends Common_Tel_Service  {
	
	@Override
	public HashMap pre_Processing(Map request) {
		return super.pre_Processing(request);
	}
	
	@Override
	public HashMap process(String serviceID, Map request) {
		return super.process(serviceID, request);
	}

	@Override
	public HashMap data_Retouch(MVH mvh) {
		MVHImpl mvhimpl = null;
		LinkedList<Row> tmplist = null;
		Boolean result = Boolean.FALSE;
		HashMap<String,Object> dataMap = new HashMap<String,Object>();
		HashMap<String,String> rowMap = new HashMap<String,String>();
		LinkedList<HashMap<String,String>> rowlist = null;
    	try {
    		mvhimpl = (MVHImpl) mvh;
    	log.info("getFlatValues>>"+mvhimpl.getFlatValues());
    	log.info("result.getInputQueryData()>>"+mvhimpl.getInputQueryData());
    	log.info("result.getInputQueryData()>>"+mvhimpl.getOccurs().getRows());
			Iterator<Row> tmp = mvhimpl.getOccurs().getRows().iterator();
			tmplist = new LinkedList<Row>();
			rowlist = new LinkedList<HashMap<String,String>>();
			tmp.forEachRemaining(tmplist::add);
			
			log.info("datalist()>>"+tmplist);
			for(Row  row:tmplist) {
				log.info("getValues>>"+row.getValues());
				rowMap = new HashMap<String,String>();
				rowMap.putAll(row.getValues());
				rowlist.add(rowMap);
			}
			dataMap.putAll(mvhimpl.getFlatValues());
			dataMap.put("REC", rowlist);
			try {
				if(dataMap.get("occurMsg") != null) {
					String occurMsg = (String)dataMap.get("occurMsg");
					if(!occurMsg.equals("0000")) {
						dataMap.put("TOPMSG", occurMsg);
					}
				}
			}catch (Exception e) {
				log.error("get occurMsg error >> {} ",e);
			}
			result = Boolean.TRUE;
			
		} catch (Exception e) {
			log.error("data_Retouch error >> {}",e);
		}finally {
			setMsgCode(dataMap,result);
		}
		return dataMap;
	}
	
	@Override
	public void setMsgCode(Map dataMap, Boolean data_Retouch_result) {
		super.setMsgCode(dataMap, data_Retouch_result);
	}
}