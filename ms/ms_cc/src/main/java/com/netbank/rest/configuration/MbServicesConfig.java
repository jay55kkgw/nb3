package com.netbank.rest.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import fstop.service.mobile.impl.MB_N072;
import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class MbServicesConfig {
    @Bean
    protected MB_N072 mb_n072Service() throws Exception {
        return new MB_N072();
    }

}
