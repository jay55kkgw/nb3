package com.netbank.rest.model.pool;

import java.util.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.rest.model.NewOneCallback;
import com.netbank.rest.model.Pool;

import fstop.core.BeanUtils;
import fstop.exception.ToRuntimeException;
import fstop.model.DBResult;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.AdmBhContactDao;
import fstop.orm.dao.TxnFundCompanyDao;
import fstop.orm.dao.TxnFundDataDao;
import fstop.orm.po.TXNFUNDCOMPANY;
import fstop.orm.po.TXNFUNDDATA;
import fstop.services.mgn.B105;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import fstop.model.IMasterValuesHelper;
import topibs.utility.NumericUtil;
import fstop.orm.dao.AdmBranchDao;
import fstop.orm.po.ADMBRANCH;

@Component
public class FundUtils extends Pool {
	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	private TxnFundDataDao txnFundDataDao;

	@Autowired
	private TxnFundCompanyDao txnFundCompanyDao;

	@Autowired
	private AdmBranchDao admBranchDao;
	
//	@Required
//	public void setTxnFundDataDao(TxnFundDataDao txnFundDataDao) {
//		this.txnFundDataDao = txnFundDataDao;
//	}
//	
//	@Required
//	public void setTxnFundCompanyDao(TxnFundCompanyDao dao) {
//		this.txnFundCompanyDao = dao;
//	}
//	@Required
//	public void setAdmBranchDao(AdmBranchDao dao) {
//		this.admBranchDao = dao;
//	}
	
	/**
	 * 依照 Trancode 取得基金的資料
	 * @param trancode
	 * @return
	 */
	@Transactional
	public MVH getFundData(final String transcode) {
		//由 DB 來
		try {
			
			NewOneCallback newoneCallback = new NewOneCallback() {

				public Object getNewOne() {
					
					TXNFUNDDATA result;
					try {
						result = txnFundDataDao.findById(transcode);
					}
					catch (Exception e) {
						return new MVHImpl();
					}
					
					Map m = null;
					try {
						m = BeanUtils.describe(result);
					} catch (Exception e) {
						throw new ToRuntimeException(e.getMessage(), e);
					}

					MVHImpl mvh = new MVHImpl();
					mvh.getFlatValues().putAll(m);
					
					return mvh;
				}
			};
			
			return (MVH)getPooledObject("getFundData(" + transcode +")", newoneCallback);
		}
		catch(Exception e) {			
			logger.warn("無法取得基金資料, TRANSCODE (" + transcode + ").");
		}
		return new MVHImpl();
	}

	/**
	 * 查詢基金公司, 若 cmp_code 為空, 則回傳全部資料
	 * @param cmp_code
	 * @return
	 */
	@Transactional
	public MVH getFundCompany(final String cmp_code) {

		try {
			
			NewOneCallback newoneCallback = new NewOneCallback() {

				public Object getNewOne() {
					
					if (StrUtils.isNotEmpty(cmp_code)) {
						
						if (cmp_code.equals("C")) {
							// 取得全部
							List all = txnFundCompanyDao.findAllAndSort();
							return new DBResult(all);				
							
						} else if (cmp_code.equals("Y")) {
							// 取得國外基金公司資料
							List all = txnFundCompanyDao.findOverseaAndSort();
							return new DBResult(all);					
							
						} else {					
							TXNFUNDCOMPANY result;

							try {
								result = txnFundCompanyDao.findById(cmp_code);
							}
							catch (Exception e) {
								return new MVHImpl();
							}
							
							Map m = null;
							try {
								m = BeanUtils.describe(result);
							} catch (Exception e) {
								throw new ToRuntimeException(e.getMessage(), e);
							}

							MVHImpl mvh = new MVHImpl();
							mvh.getFlatValues().putAll(m);

							return mvh;					
						}	
					} else {
						// 取得全部
						List all = txnFundCompanyDao.findAll();
						return new DBResult(all);
					}
				}
			};
			
			return (MVH)getPooledObject("getFundCompany(" + cmp_code + ")", newoneCallback);
		} catch (Exception e) {
			logger.warn("無法取得基金公司資料, COMPANYCODE (" + cmp_code + ").", e);
		}
		return new MVHImpl();

	}

	/**
	 * 查詢基金公司其下的所有基金
	 * @param cmp_code
	 * @return
	 */
	@Transactional
	public MVH getFundDataByCompany(final String cmp_code) {

		try {
			NewOneCallback newoneCallback = new NewOneCallback() {

				public Object getNewOne() {

					List result = txnFundDataDao.findByCompany(cmp_code);
					return new DBResult(result);
				}
			};
			
			
			return (MVH)getPooledObject("getFundDataByCompany(" + cmp_code + ")", newoneCallback);
		} catch (Exception e) {
			logger.warn("無法取得基金公司其下的所有基金, COMPANYCODE (" + cmp_code + ").");
		}
		return new MVHImpl();

	}

	/**
	 * 查詢相同集團代碼註記之基金公司其下的所有基金
	 * 
	 * <集團代碼註記>
	 *  同一集團表示：
	 *  集團代碼不為空白且相同集團代碼，表示可以做轉換交易)
	 * 
	 * @param cmp_code
	 * @return
	 */
	@Transactional
	public MVH getFundDataByGroupMark(final String cmp_code) {

		try {
			NewOneCallback newoneCallback = new NewOneCallback() {

				public Object getNewOne() {

					TXNFUNDCOMPANY company;

					try {
						company = txnFundCompanyDao.findById(cmp_code);
					}
					catch (Exception e) {
						return new MVHImpl();
					}
					
					Map m = null;
					try {
						m = BeanUtils.describe(company);
					} catch (Exception e) {
						throw new ToRuntimeException(e.getMessage(), e);
					}
					
					String str_GroupMark = m.get("FUNDGROUPMARK").toString();					
					List result = null;
					
					if (str_GroupMark.trim().equals("")) {
						result = txnFundDataDao.findByCompany(cmp_code);	
					}
					else {
						result = txnFundDataDao.findByFundGroupMark(str_GroupMark);						
					}
					
					return new DBResult(result);
				}
			};
						
			return (MVH)getPooledObject("getFundDataByGroupMark(" + cmp_code + ")", newoneCallback);
			
		} catch (Exception e) {
			logger.warn("無法取得相同集團代碼註記之基金公司其下的所有基金, COMPANYCODE (" + cmp_code + ").");
		}
		
		return new MVHImpl();
	}
	
	/**
	 * 一般網銀基金業務營業時間檢查
	 * 
	 * @return
	 */	
	public boolean isFundBizTime() {

		B105 b105 = (B105)SpringBeanFactory.getBean("b105".toLowerCase());
		return b105.isFundBizTime();
	}		

	/**
	 * 將基金電文取得之[基金報酬揭露資訊],組成網頁Template顯示時所需的條文內容:
	 * 
	 *  LINK_FUND-HOUSE	X	2	基金公司代號	&A
	 *	LINK_FUND-CODE	X	2	基金代號	&B
	 *	LINK_FEE-01	9	3.3	基金申購手續費費率	&3
	 *	LINK_FEE-02	9	3.3	基金申購手續費費率-銀行分成	&4
	 *	LINK_FEE-03	9	2.4	基金轉換手續費費率	&5
	 *	LINK_FEE-04	9	2.4	基金轉換手續費費率-銀行分成	&6
	 *	LINK_FEE-05	9	2.4	基金經理費年率	&7
	 *	LINK_FEE-07	9	2.4	基金經理費年率-銀行分成	&8
	 *	LINK_SLS-01	9	3.0	銷售獎勵金(一:年度)	&9
	 *	LINK_SLS-02	X	12	銷售獎勵金(一: 期間)	&10
	 *	LINK_SLS-03	9	2.4	銷售獎勵金(一: 費率)	&11
	 *	LINK_SLS-04	9	2.4	銷售獎勵金(二: 費率)	&12
	 *	LINK_SLS-05	9	3.0	銷售獎勵金(三:年度)	&13
	 *	LINK_SLS-06	X	12	銷售獎勵金(三: 期間)	&14
	 *	LINK_SLS-07	9	4.0	銷售獎勵金(三:金額)	&15
	 *	LINK_TRN-01	9	3	贊助金,訓練費:年度	&16
	 *	LINK_TRN-02	9	9. 0	贊助金,訓練費:金額	&17
	 *	LINK_OTHER-04	9	3.0	其他報酬一: 行銷贊助年度 	&18
	 *	LINK_OTHER-05	9	9. 0	其他報酬一: 行銷贊助金額	&19
	 *	LINK_SLS-08	9	2.4	銷售獎勵金(總費率)	&20
	 *	LINK-BASE-01	9	 9 .0	範例試算之基礎金額	&21
	 *	LINK-RESULT-01	9	 6 .0	試算出之申購手續費-前收	&22
	 *	LINK-RESULT-02	9	 6 .0	試算出之申購手續費-銀行	&23
	 *	LINK-RESULT-03	9	 6 .0	試算出之轉換費	&24
	 *	LINK-RESULT-04	9	 6 .0	試算出之轉換費分成	&25
	 *	LINK-RESULT-05	9	 6 .0	試算出之經理費分成	&26
	 *	LINK-RESULT-06	9	 6 .0	試算出之銷售獎勵金	&27
	 *	LINK-FEE-BSHARE	X	1	後收型基金識別碼	&28
	 *	LINK-FH-NAME	X	22	基金公司名稱	&1
	 *  LINK-FUND-NAME	X	22	基金名稱	&2
	 *  
	 * @param helper 基金報酬揭露資訊
	 * @param str_TXID 交易代碼
	 * @return
	 */
	public static Map getFundFeeExposeInfo(IMasterValuesHelper helper, String str_TXID) {

		Map htl_TextResult = new Hashtable();
		
		String str_LINK_FUND_HOUSE = (String)helper.getValueByFieldName("LINK_FUND-HOUSE"); 
		String str_LINK_FUND_CODE = (String)helper.getValueByFieldName("LINK_FUND-CODE");   
		String str_LINK_FEE_01 = NumericUtil.formatNumberString((String)helper.getValueByFieldName("LINK_FEE-01"), 3);           
		String str_LINK_FEE_02 = NumericUtil.formatNumberString((String)helper.getValueByFieldName("LINK_FEE-02"), 3);           
		String str_LINK_FEE_03 = NumericUtil.formatNumberString((String)helper.getValueByFieldName("LINK_FEE-03"), 4);         
		String str_LINK_FEE_04 = NumericUtil.formatNumberString((String)helper.getValueByFieldName("LINK_FEE-04"), 4);
		String str_LINK_FEE_05 = NumericUtil.formatNumberString((String)helper.getValueByFieldName("LINK_FEE-05"), 4);
		String str_LINK_FEE_07 = NumericUtil.formatNumberString((String)helper.getValueByFieldName("LINK_FEE-07"), 4);
		String str_LINK_SLS_01 = NumericUtil.formatNumberString((String)helper.getValueByFieldName("LINK_SLS-01"), 0);		
		String str_LINK_SLS_02 = (String)helper.getValueByFieldName("LINK_SLS-02");
		String str_LINK_SLS_03 = NumericUtil.formatNumberString((String)helper.getValueByFieldName("LINK_SLS-03"), 4);
		String str_LINK_SLS_04 = NumericUtil.formatNumberString((String)helper.getValueByFieldName("LINK_SLS-04"), 4);
		String str_LINK_SLS_05 = NumericUtil.formatNumberString((String)helper.getValueByFieldName("LINK_SLS-05"), 0);
		String str_LINK_SLS_06 = (String)helper.getValueByFieldName("LINK_SLS-06");		
		String str_LINK_SLS_07 = NumericUtil.formatNumberString((String)helper.getValueByFieldName("LINK_SLS-07"), 0);
		String str_LINK_TRN_01 = NumericUtil.formatNumberString((String)helper.getValueByFieldName("LINK_TRN-01"), 0);
		String str_LINK_TRN_02 = NumericUtil.formatNumberString((String)helper.getValueByFieldName("LINK_TRN-02"), 0);
		String str_LINK_OTHER_04 = NumericUtil.formatNumberString((String)helper.getValueByFieldName("LINK_OTHER-04"), 0);
		String str_LINK_OTHER_05 = NumericUtil.formatNumberString((String)helper.getValueByFieldName("LINK_OTHER-05"), 0);		
		String str_LINK_SLS_08 = NumericUtil.formatNumberString((String)helper.getValueByFieldName("LINK_SLS-08"), 4);
		String str_LINK_BASE_01 = NumericUtil.formatNumberString((String)helper.getValueByFieldName("LINK-BASE-01"), 0);
		String str_LINK_RESULT_01 = NumericUtil.formatNumberString((String)helper.getValueByFieldName("LINK-RESULT-01"), 0);
		String str_LINK_RESULT_02 = NumericUtil.formatNumberString((String)helper.getValueByFieldName("LINK-RESULT-02"), 0);
		String str_LINK_RESULT_03 = NumericUtil.formatNumberString((String)helper.getValueByFieldName("LINK-RESULT-03"), 0);		
		String str_LINK_RESULT_04 = NumericUtil.formatNumberString((String)helper.getValueByFieldName("LINK-RESULT-04"), 0);
		String str_LINK_RESULT_05 = NumericUtil.formatNumberString((String)helper.getValueByFieldName("LINK-RESULT-05"), 0);
		String str_LINK_RESULT_06 = NumericUtil.formatNumberString((String)helper.getValueByFieldName("LINK-RESULT-06"), 0);
		String str_LINK_FEE_BSHARE = (String)helper.getValueByFieldName("LINK-FEE-BSHARE");
		String str_LINK_FUND_TYPE = (String)helper.getValueByFieldName("LINK-FUND-TYPE");				
		String str_LINK_FH_NAME = (String)helper.getValueByFieldName("LINK-FH-NAME");
		String str_LINK_FUND_NAME = (String)helper.getValueByFieldName("LINK-FUND-NAME");
		String str_LINK_DATE = (String)helper.getValueByFieldName("LINK-DATE");			
		
		htl_TextResult.put("LINK_FUND-HOUSE", str_LINK_FUND_HOUSE);
		htl_TextResult.put("LINK_FUND-CODE", str_LINK_FUND_CODE);
		htl_TextResult.put("LINK_FEE-01", str_LINK_FEE_01);
		htl_TextResult.put("LINK_FEE-02", str_LINK_FEE_02);
		htl_TextResult.put("LINK_FEE-03", str_LINK_FEE_03);
		htl_TextResult.put("LINK_FEE-04", str_LINK_FEE_04);
		htl_TextResult.put("LINK_FEE-05", str_LINK_FEE_05);
		htl_TextResult.put("LINK_FEE-07", str_LINK_FEE_07);
		htl_TextResult.put("LINK_SLS-01", str_LINK_SLS_01);
		htl_TextResult.put("LINK_SLS-02", str_LINK_SLS_02);
		htl_TextResult.put("LINK_SLS-03", str_LINK_SLS_03);
		htl_TextResult.put("LINK_SLS-04", str_LINK_SLS_04);
		htl_TextResult.put("LINK_SLS-05", str_LINK_SLS_05);
		htl_TextResult.put("LINK_SLS-06", str_LINK_SLS_06);
		htl_TextResult.put("LINK_SLS-07", str_LINK_SLS_07);
		htl_TextResult.put("LINK_TRN-01", str_LINK_TRN_01);
		htl_TextResult.put("LINK_TRN-02", str_LINK_TRN_02);
		htl_TextResult.put("LINK_OTHER-04", str_LINK_OTHER_04);
		htl_TextResult.put("LINK_OTHER-05", str_LINK_OTHER_05);
		htl_TextResult.put("LINK_SLS-08", str_LINK_SLS_08);
		htl_TextResult.put("LINK-BASE-01", str_LINK_BASE_01);
		htl_TextResult.put("LINK-RESULT-01", str_LINK_RESULT_01);
		htl_TextResult.put("LINK-RESULT-02", str_LINK_RESULT_02);
		htl_TextResult.put("LINK-RESULT-03", str_LINK_RESULT_03);
		htl_TextResult.put("LINK-RESULT-04", str_LINK_RESULT_04);
		htl_TextResult.put("LINK-RESULT-05", str_LINK_RESULT_05);
		htl_TextResult.put("LINK-RESULT-06", str_LINK_RESULT_06);
		htl_TextResult.put("LINK-FEE-BSHARE", str_LINK_FEE_BSHARE);
		htl_TextResult.put("LINK-FUND-TYPE", str_LINK_FUND_TYPE);
		htl_TextResult.put("LINK-FH-NAME", str_LINK_FH_NAME);
		htl_TextResult.put("LINK-FUND-NAME", str_LINK_FUND_NAME);
		htl_TextResult.put("LINK-DATE", str_LINK_DATE);
		
		//STR_1
		if (str_TXID.equals("C021") || str_TXID.equals("C032")) {			
			htl_TextResult.put("STR_1", "轉入");					
		}
		else {
			htl_TextResult.put("STR_1", "銷售");
		}
		
		//STR_2
		if (str_LINK_FEE_BSHARE.equals("1") && (str_TXID.equals("C016") || str_TXID.equals("C017") || str_TXID.equals("C031"))) {
			htl_TextResult.put("STR_2", "台端支付的基金申購手續費為 " + str_LINK_FEE_01 + "%，其中本行收取不多於" + str_LINK_FEE_02 + "%。");
		}
		else if (str_LINK_FEE_BSHARE.equals("2") && (str_TXID.equals("C016") || str_TXID.equals("C017") || str_TXID.equals("C031"))) {
			htl_TextResult.put("STR_2", "本基金手續費遞延至贖回時收取，申購時無需支付，本公司將自基金公司收取" + str_LINK_FEE_02 + " %）。");
		}
		else if (str_TXID.equals("C021") || str_TXID.equals("C032")) {
			htl_TextResult.put("STR_2", "台端支付的基金轉換手續費為" + str_LINK_FEE_03 + "%，其中本銀行收取不多於"+ str_LINK_FEE_04 +"%。");			
		}
		else {
			htl_TextResult.put("STR_2", "");
		}
				
		//STR_3
		StringBuffer sb = new StringBuffer();
        if (Double.parseDouble(str_LINK_SLS_03)>0) {
        	sb.append("<li>本行" + str_LINK_SLS_01 + "年" + str_LINK_SLS_02 + "精選基金活動期間，" + str_LINK_FH_NAME + "依本行銷售金額支付獎勵金不多於" + str_LINK_SLS_03 + " % 。</li>");
        }
        
        if (Double.parseDouble(str_LINK_SLS_04)>0) {
        	sb.append("<li>本基金募集期間，" + str_LINK_FH_NAME + "依本行銷售金額支付獎勵金不多於" + str_LINK_SLS_04 + " %。</li>");
        }
        
        if (Integer.parseInt((String)helper.getValueByFieldName("LINK_SLS-07"))>0) {
        	sb.append("<li>本行" + str_LINK_SLS_05 + "年" + str_LINK_SLS_06 + "精選基金活動期間，" + str_LINK_FH_NAME + "依本行定期定額之銷售金額，新開戶一筆且達基金公司約定之成功扣款次數支付獎勵金不多於新台幣" + str_LINK_SLS_07 + "元。</li>");
        }             
		
        if (Double.parseDouble(str_LINK_SLS_03)==0 && Double.parseDouble(str_LINK_SLS_04)==0 && Integer.parseInt((String)helper.getValueByFieldName("LINK_SLS-07"))==0) {
        	sb.append("</ol>「未收取」。<ol>");        	
        }
        
    	htl_TextResult.put("STR_3", sb.toString());
    	
		//STR_4
		if (Integer.parseInt((String)helper.getValueByFieldName("LINK_TRN-02")) >= 2000000) {
			htl_TextResult.put("STR_4", "本行" + str_LINK_TRN_01 + "年度銷售" + str_LINK_FH_NAME + "基金，預計可收取新台幣" + str_LINK_TRN_02 + "元之產品說明會及員工教育訓練贊助金。");
		}
		else if (Integer.parseInt((String)helper.getValueByFieldName("LINK_TRN-02")) < 2000000 && Integer.parseInt((String)helper.getValueByFieldName("LINK_TRN-02")) >= 1) {
			htl_TextResult.put("STR_4", "「未達2百萬元揭露門檻」。");
		}
		else if (Integer.parseInt((String)helper.getValueByFieldName("LINK_TRN-02")) == 0) {
			htl_TextResult.put("STR_4", "「未收取」。");
		}		
		else {
			htl_TextResult.put("STR_4", "");
		}		
    	    	    
		//STR_5
		if (Integer.parseInt((String)helper.getValueByFieldName("LINK_OTHER-05")) >= 1000000) {
			htl_TextResult.put("STR_5", "本銀行" + str_LINK_OTHER_04 + "年度銷售" + str_LINK_FH_NAME + "基金<br>"+ str_LINK_OTHER_04 +"年度行銷贊助、經銷費：本行全年自" + str_LINK_FH_NAME +"獲得行銷贊助及經銷費為新台幣"+str_LINK_OTHER_05+"元"); 
		}
		else if(Integer.parseInt((String)helper.getValueByFieldName("LINK_OTHER-05")) > 0 && Integer.parseInt((String)helper.getValueByFieldName("LINK_OTHER-05"))<1000000){
			htl_TextResult.put("STR_5", "未達1百萬元揭露門檻。");			
		}else {
			htl_TextResult.put("STR_5", "「未收取」。");
		}
		 
		//STR_6
		if (Integer.parseInt((String)helper.getValueByFieldName("LINK_TRN-02")) > 2000000) {
			htl_TextResult.put("STR_6", "，另本行" + str_LINK_TRN_01 + "年度銷售" + str_LINK_FH_NAME + "基金，該公司預計贊助產品說明會及員工教育訓練之金額合計為" + str_LINK_TRN_02 + "元");									
		}
		else {
			htl_TextResult.put("STR_6", "");
		}
		
		//STR_7
		if (str_LINK_FEE_BSHARE.equals("1")) {
			htl_TextResult.put("STR_7", "由　台端所支付之" + str_LINK_RESULT_01 + "元申購手續費中收取不多於" + str_LINK_RESULT_02 + "元 (" + str_LINK_BASE_01 + "＊" + str_LINK_FEE_02 + "%=" + str_LINK_RESULT_02 + "元)");
		}
		else if (str_LINK_FEE_BSHARE.equals("2")) {
			htl_TextResult.put("STR_7", "本基金手續費遞延至贖回時收取，申購時無需支付，本公司將自基金公司收取" + str_LINK_RESULT_02 + "元 (" + str_LINK_BASE_01 + "＊" + str_LINK_FEE_02 + "%=" + str_LINK_RESULT_02 + "元)");
		}
		else {
			htl_TextResult.put("STR_7", "");
		}		
		
		//STR_8
		if (Integer.parseInt((String)helper.getValueByFieldName("LINK_TRN-02")) > 2000000) {
			htl_TextResult.put("STR_8", "(3)年度產品說明會及員工教育訓練贊助金： " + str_LINK_TRN_02 + " 元");									
		}
		else {
			htl_TextResult.put("STR_8", "");
		}		
		
		//STR_9
		if (Integer.parseInt((String)helper.getValueByFieldName("LINK_OTHER-05")) >= 1000000) {
			htl_TextResult.put("STR_9", "行銷贊助：本行全年自" + str_LINK_FH_NAME + "獲得行銷贊助及經銷費為新台幣" + str_LINK_OTHER_05 + " 元");
		}		
		else {
			htl_TextResult.put("STR_9", "");			
		}		
				
		return htl_TextResult;
	}	
	/**
	 * 一般網銀分行聯絡方式資料檔擷取
	 * @param bhid
	 * @return
	 */	
	public MVH getAdmBhContact(String bhid) {

		MVHImpl result = new DBResult(admBranchDao.findByBhID(bhid));
			
		return result;
	}			
}
