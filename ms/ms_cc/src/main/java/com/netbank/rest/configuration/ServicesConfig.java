package com.netbank.rest.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import fstop.services.ServerConfig;
import fstop.services.TopMsgDBDetecter;
import fstop.services.TopMsgF003Detecter;
import fstop.services.TopMsgF007Detecter;
import fstop.services.TopMsgFXDetecter;
import fstop.services.TopMsgFundDetecter;
import fstop.services.TopMsgN320Detecter;
import fstop.services.TopMsgN950Detecter;
import fstop.services.impl.AIOEBM;
//import fstop.services.batch.GD11;
//import fstop.services.batch.ND10;
//import fstop.services.batch.ND101;
import fstop.services.impl.CK01;
import fstop.services.impl.CK01_1;
import fstop.services.impl.CallMobileCenter;
import fstop.services.impl.EloanApi;
import fstop.services.impl.N072;
import fstop.services.impl.N1010_CP;
import fstop.services.impl.N1010_RP;
import fstop.services.impl.N110;
import fstop.services.impl.N810;
import fstop.services.impl.N814;
import fstop.services.impl.N815;
import fstop.services.impl.N816A;
import fstop.services.impl.N816L;
import fstop.services.impl.N816L_1;
import fstop.services.impl.N821;
import fstop.services.impl.N822;
import fstop.services.impl.N931;
import fstop.services.impl.NA02Q;
import fstop.services.impl.NA03;
import fstop.services.impl.NA03Q;
import fstop.services.impl.NI02;
import fstop.services.impl.NI04;
import fstop.services.impl.Q072;
import fstop.services.impl.QR10;
import fstop.services.impl.SmsOtp;
import fstop.services.impl.TD01;
import fstop.services.impl.TD01_MAIL;
import fstop.services.impl.TD02;
import fstop.services.impl.XLSTXT;
//import fstop.services.impl.fx.F001Controller;
//import fstop.services.impl.fx.F002Controller;
//import fstop.services.impl.fx.F003Controller;
import fstop.services.mgn.B105;
import fstop.services.mgn.B106;
import fstop.services.mgn.NA20;
import fstop.va.VATransform;
import fstop.ws.eai.client.N701WebServiceClient;
import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class ServicesConfig {

    @Bean
    protected VATransform vatransform() throws Exception {
        return new VATransform();
    }
//    @Bean
//    protected CheckServerHealth checkserverhealth() throws Exception {
//    	return new CheckServerHealth();
//    }
    @Bean
    protected N701WebServiceClient n701WebServiceClient() throws Exception {
        return new N701WebServiceClient();
    }
	
    @Bean
    protected TopMsgDBDetecter dbErrDetect() throws Exception {
        return new TopMsgDBDetecter();
    }

    @Bean
    protected TopMsgN320Detecter n320ErrDetect() throws Exception {
        return new TopMsgN320Detecter();
    }

    @Bean
    protected TopMsgFXDetecter fxErrDetect() throws Exception {
        return new TopMsgFXDetecter();
    }

    @Bean
    protected TopMsgF003Detecter f003ErrDetect() throws Exception {
        return new TopMsgF003Detecter();
    }

    @Bean
    protected TopMsgF007Detecter f007ErrDetect() throws Exception {
        return new TopMsgF007Detecter();
    }

    @Bean
    protected TopMsgFundDetecter fundErrDetect() throws Exception {
        return new TopMsgFundDetecter();
    }
    @Bean
    protected TopMsgN950Detecter n950ErrDetect() throws Exception {
        return new TopMsgN950Detecter();
    }
    
    @Bean
    protected B105 b105Service() throws Exception {
        return new B105();
    }
    
    @Bean
    protected B106 b106Service() throws Exception {
        return new B106();
    }
    @Bean
    protected CallMobileCenter callmobilecenterService() throws Exception {
    	return new CallMobileCenter();
    }
    @Bean
    protected CK01 ck01Service() throws Exception {
        return new CK01();
    }
    
    @Bean
    protected CK01_1 ck01_1Service() throws Exception {
        return new CK01_1();
    }
    
    
    
    @Bean
    protected NA02Q na02qService() throws Exception {
        return new NA02Q();
    }
    
    @Bean
    protected NA03 na03Service() throws Exception {
        return new NA03();
    }
    
    @Bean
    protected NA03Q na03qService() throws Exception {
        return new NA03Q();
    }
    
    @Bean
    protected NA20 na20Service() throws Exception {
        return new NA20();
    }
    
    @Bean
    protected NI02 ni02Service() throws Exception {
        return new NI02();
    }
    
    @Bean
    protected NI04 ni04Service() throws Exception {
        return new NI04();
    }
    
    @Bean
    protected N1010_RP n1010_rpService() throws Exception {
        return new N1010_RP();
    }
    
    @Bean
    protected N1010_CP n1010_cpService() throws Exception {
        return new N1010_CP();
    }
    
    @Bean
    protected N110 n110Service() throws Exception {
        return new N110();
    }


    
    @Bean
    protected N810 n810Service() throws Exception {
        return new N810();
    }
    
    @Bean
    protected N814 n814Service() throws Exception {
        return new N814();
    }
    
    @Bean
    protected N816A n816aService() throws Exception {
        return new N816A();
    }
    
    @Bean
    protected N816L n816lService() throws Exception {
        return new N816L();
    }
    
    @Bean
    protected N816L_1 n816l_1Service() throws Exception {
        return new N816L_1();
    }
    
    @Bean
    protected N821 n821Service() throws Exception {
        return new N821();
    }
    
    @Bean
    protected N822 n822Service() throws Exception {
        return new N822();
    }
    
    @Bean
    protected N815 n815Service() throws Exception {
        return new N815();
    }
   
    @Bean
    protected TD01 td01Service() throws Exception {
        return new TD01();
    }
    
    @Bean
    protected TD02 td02Service() throws Exception {
        return new TD02();
    }
    
    @Bean
    protected N931 n931Service() throws Exception{
    	return new N931();
    }
    
    //<!-- 後台OTP狀態 -->
    @Bean
    protected ServerConfig serverConfig() throws Exception {
        return new ServerConfig();
    }

	@Bean
    protected N072 n072Service() throws Exception {
        return new N072();
    }
	
	@Bean
    protected Q072 q072Service() throws Exception {
        return new Q072();
    }
	
	@Bean
	protected QR10 qr10Service() throws Exception {
	        return new QR10();
	}
	    
	
    @Bean
    protected TD01_MAIL td01_mailService() throws Exception {
        return new TD01_MAIL();
    }
    
    @Bean
    protected XLSTXT xlstxtService() throws Exception {
        return new XLSTXT();
    }
    
    @Bean
    protected SmsOtp smsotpService() throws Exception {
    	return new SmsOtp();
    }
    
    @Bean
    protected AIOEBM aioedmService() throws Exception {
    	return new AIOEBM();
    }
    
    @Bean
    protected EloanApi eloanapiService() throws Exception {
    	return new EloanApi();
    }
}
