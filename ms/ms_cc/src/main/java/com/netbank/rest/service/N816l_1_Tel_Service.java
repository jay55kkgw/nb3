package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.netbank.util.ESAPIUtil;
import com.netbank.util.NumericUtil;
import com.netbank.util.StrUtils;

import fstop.model.IMasterValuesHelper;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.model.TelcommResult;
import lombok.extern.slf4j.Slf4j;

/**
 * 主要用來 1.丟入MVH前的前置處理，包含檢核; 2.後製MVH回覆的訊息;
 * 
 * @author
 *
 */
@Service
@Slf4j
public class N816l_1_Tel_Service extends Common_Tel_Service {
	@Override
	public HashMap pre_Processing(Map request) {
		// TODO Auto-generated method stub
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) {
		// TODO Auto-generated method stub
		return super.process(serviceID, request);
	}

	@Override
	public HashMap data_Retouch(MVH mvh) {
		MVHImpl mvhimpl = null;
		LinkedList<Row> tmplist = null;
		HashMap<String, Object> dataMap = new HashMap<String, Object>();
		HashMap<String, String> rowMap = new HashMap<String, String>();
		LinkedList<HashMap<String, String>> rowlist = null;
		Boolean result = Boolean.FALSE;
		try {
			mvhimpl = (MVHImpl) mvh;
			// process
			log.trace(ESAPIUtil.vaildLog("mvh >>" +  mvh.dumpToString()));
			if (null != mvhimpl.getTableByName("SUCCEED")) {
				MVHImpl c = (MVHImpl) mvhimpl.getTableByName("SUCCEED");
				Iterator<Row> tmp = c.getOccurs().getRows().iterator();

				tmplist = new LinkedList<Row>();
				rowlist = new LinkedList<HashMap<String, String>>();
				tmp.forEachRemaining(tmplist::add);

				for (Row row : tmplist) {
					rowMap = new HashMap<String, String>();
					rowMap.putAll(row.getValues());
					rowlist.add(rowMap);
				}
				dataMap.put("SUCCEED", rowlist);
				dataMap.put("SUCCEEDCOUNT", rowlist.size());
			} else {
				dataMap.put("SUCCEEDCOUNT", "0");
			}


			if (null != mvhimpl.getTableByName("FAILED")) {
				MVHImpl c = (MVHImpl) mvhimpl.getTableByName("FAILED");
				Iterator<Row> tmp = c.getOccurs().getRows().iterator();

				tmplist = new LinkedList<Row>();
				rowlist = new LinkedList<HashMap<String, String>>();
				tmp.forEachRemaining(tmplist::add);

				for (Row row : tmplist) {
					rowMap = new HashMap<String, String>();
					rowMap.putAll(row.getValues());
					rowlist.add(rowMap);
				}
				dataMap.put("FAILED", rowlist);
				dataMap.put("FAILEDCOUNT", rowlist.size());
			} else {
				dataMap.put("FAILEDCOUNT", "0");
			}

			if(null==mvh.getValueByFieldName("TOPMSG")||"".equals(mvh.getValueByFieldName("TOPMSG"))) {
				dataMap.put("TOPMSG", "");
			}else {
				dataMap.put("TOPMSG", mvh.getValueByFieldName("TOPMSG"));
			}
			
			result = Boolean.TRUE;

		} catch (Exception e) {
			
			log.error("data_Retouch error >> {} ", e);
		} finally {
			setMsgCode(dataMap, result);
		}
		return dataMap;
	}

	@Override
	public void setMsgCode(Map dataMap, Boolean data_Retouch_result) {
		// TODO Auto-generated method stub
		super.setMsgCode(dataMap, data_Retouch_result);
	}

}
