package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netbank.rest.model.CommonPools;
import com.netbank.util.CodeUtil;
import com.netbank.util.StrUtils;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.services.CommonService;
import fstop.util.SpringBeanFactory;
import fstop.ws.bean.BHIvrWebServiceObj;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class Bhvrws_Tel_Service extends Common_Tel_Service {

	@Autowired
	private CommonPools commonPools;
	@Override
	public HashMap pre_Processing(Map request) {
		// TODO Auto-generated method stub
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) {
		MVH mvh = null;
		try {
			pre_Processing(request);
			String rid = (String)request.get("rid");
			String rcustid = (String)request.get("rcustid");
			BHIvrWebServiceObj result = commonPools.bhvr.getBHIVRWSResult(rid, rcustid);
			if (result==null) {
				throw TopMessageException.create("Z605");
			}
			mvh = new MVHImpl();
			((MVHImpl) mvh).getFlatValues().putAll(CodeUtil.objectCovert(Map.class, result));
		} catch (TopMessageException e) {
			// TODO Auto-generated catch block
			log.error("process.error>>",e);
			MVHImpl m = new MVHImpl();
			m.getFlatValues().put("TOPMSG", e.getMsgcode());
			mvh = m;
		} catch (Exception e) {
			log.error("process.error>>", e);
			((MVHImpl) mvh).getFlatValues().put("TOPMSG", "Z605");
		}finally {
//			TODO trur 做data_Retouch false 做msgCode 回覆處理
			return data_Retouch(mvh);
		}
	}

	@Override
	public HashMap data_Retouch(MVH mvh) {
		MVHImpl mvhimpl = null;
		LinkedList<Row> tmplist = null;
		Boolean result = Boolean.FALSE;
		HashMap<String,Object> dataMap = new HashMap<String,Object>();
    	try {
    		mvhimpl = (MVHImpl) mvh;
			dataMap.putAll(mvhimpl.getFlatValues());
			if(!dataMap.containsKey("TOPMSG")) {
				dataMap.put("TOPMSG", dataMap.get("billresult").equals("Y") ? "0000" : "ZX99");
			}
			result = Boolean.TRUE;
			
		} catch (Exception e) {
			log.error("data_Retouch error >>{}",e);
		}finally {
			setMsgCode(dataMap,result);
		}
		return dataMap;
	}

	@Override
	public void setMsgCode(Map dataMap, Boolean data_Retouch_result) {
		// TODO Auto-generated method stub
		super.setMsgCode(dataMap, data_Retouch_result);
	}
}
