package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.netbank.util.DateUtils;
import com.netbank.util.NumericUtil;

import fstop.exception.TopMessageException;
import fstop.model.IMasterValuesHelper;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import lombok.extern.slf4j.Slf4j;

/**
 * 主要用來 1.丟入MVH前的前置處理，包含檢核; 2.後製MVH回覆的訊息;
 * 
 * @author Hugo
 *
 */
@Service
@Slf4j
public class Td01_Tel_Service extends Common_Tel_Service {
	@Override
	public HashMap pre_Processing(Map request) {
		// TODO Auto-generated method stub
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) {
		// TODO Auto-generated method stub
		return super.process(serviceID, request);
	}


	@Override
	public HashMap data_Retouch(MVH mvh) {
		MVHImpl mvhimpl = null;
		LinkedList<Row> tmplist = null;
		Boolean result = Boolean.FALSE;
		HashMap<String, Object> dataMap = new HashMap<String, Object>();
		HashMap<String, String> rowMap = new HashMap<String, String>();
		LinkedList<HashMap<String, String>> rowlist = null;
		HashMap<String, Object> tableMap = null;
		LinkedList<HashMap<String, Object>> tableList = null;
		try {
			mvhimpl = (MVHImpl) mvh;
			// log.info("getFlatValues>>"+mvhimpl.getFlatValues());
			// log.info("result.getInputQueryData()>>"+mvhimpl.getInputQueryData());
			// log.info("result.getTableKeys()>>"+mvhimpl.getTableKeys());
			// log.info("result.getTableByName(00162965425)>>"+mvhimpl.getTableByName("00162965425"));

			tableList = new LinkedList<HashMap<String, Object>>();
			for (Object key : mvhimpl.getTableKeys()) {
				tableMap = new HashMap<String, Object>();
				log.info("getTableKeys >> {}", (String) key);
				log.info("result.getInputQueryData()>> {}",
						((MVHImpl) mvhimpl.getTableByName((String) key)).getOccurs().getRows());
				Iterator<Row> tmp = ((MVHImpl) mvhimpl.getTableByName((String) key)).getOccurs().getRows().iterator();
				tmplist = new LinkedList<Row>();
				rowlist = new LinkedList<HashMap<String, String>>();
				tmp.forEachRemaining(tmplist::add);

				log.info("datalist()>>" + tmplist);
				if (tmplist.size() != 0) {
					for (Row row : tmplist) {
						log.info("getValues>>" + row.getValues());
						rowMap = new HashMap<String, String>();
						row.getValues().put("CARDTYPE",((MVHImpl)mvhimpl.getTableByName((String) key)).getFlatValues().get("CARDTYPE"));
						rowMap.putAll(row.getValues());
						rowlist.add(rowMap);
					}
					tableMap.putAll(((MVHImpl) mvhimpl.getTableByName((String) key)).getFlatValues());
					tableMap.put("TABLE", rowlist);
					// tableMap.put("ACN", (String)key);
					// tableMap.put("TOPMSG", (String)topmsg);
					// tableMap.put("MSGCOD", (String)msgcod);
					// super.setMsgCode(tableMap, Boolean.TRUE);
					tableList.add(tableMap);
				}
			}
			dataMap.putAll(mvhimpl.getFlatValues());
			dataMap.put("REC", tableList);
			if(null==dataMap.get("TOPMSG")) {
				dataMap.put("TOPMSG", "");
			}
			
			result = Boolean.TRUE;
		} catch (TopMessageException e) {
			// TODO Auto-generated catch block
			log.error("process.error>>",e);
			dataMap.put("TOPMSG", e.getMsgcode());
		} catch (Exception e) {
			log.error("process.error>>",e);
		} finally {
			setMsgCode(dataMap, result);
		}
		return dataMap;

	}

	@Override
	public void setMsgCode(Map dataMap, Boolean data_Retouch_result) {
		// TODO Auto-generated method stub
		super.setMsgCode(dataMap, data_Retouch_result);
	}

}
