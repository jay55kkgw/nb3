/**
 * 
 */
package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.netbank.util.StrUtils;

import fstop.model.MVH;
import fstop.util.SpringBeanFactory;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 功能說明 :Tel_Service介面
 *
 */
@Service
@Slf4j
public class REST_Service {

	public HashMap process(String serviceID,Map request) {
		
		Common_Tel_Service service = (Common_Tel_Service)SpringBeanFactory.getBean(
              String.format("%s_Tel_Service",
                      Optional.ofNullable(serviceID)
                          .filter(s -> StrUtils.isNotEmpty(s)).map(s -> s.toLowerCase())
                          .orElse(null)));
		
		return service.process(serviceID, request);
		
	}
}
