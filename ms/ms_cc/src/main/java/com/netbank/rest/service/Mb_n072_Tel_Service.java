package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netbank.rest.model.pool.N810Pool;
import com.netbank.rest.model.pool.N920Pool;
import com.netbank.rest.model.pool.UISelectListPool;
import com.netbank.util.AcnoTypeEnum;
import com.netbank.util.DateUtils;
import com.netbank.util.ESAPIUtil;
import com.netbank.util.NumericUtil;
import com.netbank.util.StrUtils;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.model.TelcommResult;
import fstop.orm.po.ADMMSGCODE;
import fstop.services.CommonService;
import fstop.services.TopMsgDBDetecter;
import fstop.util.SpringBeanFactory;
import lombok.extern.slf4j.Slf4j;

/**
 * 主要用來 1.丟入MVH前的前置處理，包含檢核; 2.後製MVH回覆的訊息;
 * 
 * @author Hugo
 *
 */
@Service
@Slf4j
public class Mb_n072_Tel_Service extends Common_Tel_Service {

	@Autowired
	private TopMsgDBDetecter errDetect;

	@Override
	public HashMap pre_Processing(Map request) {
		// TODO Auto-generated method stub
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) {
		// TODO Auto-generated method stub
		return super.process(serviceID, request);
	}

	@Override
	public HashMap data_Retouch(MVH mvh) {
		log.trace(ESAPIUtil.vaildLog("N072.data_Retouch.request : "+ mvh));

		// 判斷交易結果並回傳
		HashMap<Object, Object> resultMap = new HashMap<Object, Object>();
		resultMap = super.data_Retouch(mvh);

		try {
			// 失敗應為錯誤代碼，成功應為轉出帳號
			String topMsg = resultMap.get(TOPMSG).toString();
			log.trace(ESAPIUtil.vaildLog("N072.data_Retouch.topMsg: {}"+  topMsg));

			// TOPMSG是否為錯誤代碼
			ADMMSGCODE isMsgCode = errDetect.isError(topMsg, null);
			log.trace(ESAPIUtil.vaildLog("N072.data_Retouch.isMsgCode : " + isMsgCode));

			if (topMsg != null && isMsgCode != null) {
				// TOPMSG是錯誤代碼
				log.trace("N072.data_Retouch.isMsgCode...");
				//super.setMsgCode(resultMap, Boolean.TRUE);
			} else {
				log.trace("N072.data_Retouch.isNotMsgCode...");
				resultMap.replace("TOPMSG", "0000");
//				String outAcn = resultMap.get("OUTACN").toString();
//				log.trace("N072.data_Retouch.outAcn: {}", outAcn);
//
//				// TOPMSG是轉出帳號
//				if (topMsg.equals(outAcn)) {
//					// 交易成功
//					resultMap.replace("TOPMSG", "0000");
//					super.setMsgCode(resultMap, Boolean.TRUE);
//				}

			}
			super.setMsgCode(resultMap, Boolean.TRUE);
		} catch (Exception e) {
			log.error("data_Retouch error >> {}", e);
		}
		return resultMap;
	}

	@Override
	public void setMsgCode(Map dataMap, Boolean data_Retouch_result) {
		// TODO Auto-generated method stub
		super.setMsgCode(dataMap, data_Retouch_result);
	}
}