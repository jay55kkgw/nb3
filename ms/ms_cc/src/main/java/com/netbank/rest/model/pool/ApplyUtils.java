package com.netbank.rest.model.pool;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Transparency;
import java.awt.image.BufferedImage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.netbank.rest.model.Pool;

import fstop.orm.dao.TxnAcnApplyDao;
import fstop.orm.po.TXNACNAPPLY;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class ApplyUtils extends Pool {
//	private Logger logger = Logger.getLogger(getClass());
	
	@Autowired
	private TxnAcnApplyDao txnAcnApplyDao;
			
//	@Required
//	public void setTxnAcnApplyDao(TxnAcnApplyDao dao) {
//		this.txnAcnApplyDao = dao;
//	}
	/**
	 * 取得申請數位存款帳戶類別
	 * @param 
	 * @return
	 */
	public String getACNTYPEByCusidn(final String cusidn) {
		String result = txnAcnApplyDao.findByCUSIDN(cusidn);
		return result;
	}	
	/**
	 * 更新數位存款帳戶db申請資料
	 * @param 
	 * @return
	 */
	public int updaterec(final String cusidn,final String brhcod,final String PURPOSE,final String PREASON,final String BDEALING,final String BREASON,final String IP,final String EMPLOYER) {
		int rtn=txnAcnApplyDao.updateAcnApplyData(cusidn, brhcod, PURPOSE, PREASON, BDEALING, BREASON,IP,EMPLOYER);
		return rtn;
	}
	/**
	 * 取得申請數位存款帳戶姓名
	 * @param 
	 * @return
	 */
	public String getNAMEByCusidn(final String cusidn) {
		String result = txnAcnApplyDao.findNAMEByCUSIDN(cusidn);
		return result;
	}
	/**
	 * 取得申請數位存款帳戶
	 * @param 
	 * @return
	 */
	public String getACNByCusidn(final String cusidn) {
		String result = txnAcnApplyDao.findACNByCUSIDN(cusidn);
		return result;
	}
	/**
	 * 取得申請數位存款帳戶的服務分行
	 * @param 
	 * @return
	 */
	public String getBRHCODByCusidn(final String cusidn) {
		String result = txnAcnApplyDao.findBRHCODByCUSIDN(cusidn);
		return result;
	}
	/**
	 * 取得申請數位存款帳戶的開戶目的
	 * @param 
	 * @return
	 */
	public String getPURPOSEByCusidn(final String cusidn) {
		String result = txnAcnApplyDao.findPURPOSEByCUSIDN(cusidn);
		return result;
	}
	/**
	 * 取得申請數位存款帳戶的開戶目的_其他說明
	 * @param 
	 * @return
	 */
	public String getPREASONByCusidn(final String cusidn) {
		String result = txnAcnApplyDao.findPREASONByCUSIDN(cusidn);
		return result;
	}
	/**
	 * 取得申請數位存款帳戶的與本行已往來業務
	 * @param 
	 * @return
	 */
	public String getBDEALINGByCusidn(final String cusidn) {
		String result = txnAcnApplyDao.findBDEALINGByCUSIDN(cusidn);
		return result;
	}
	/**
	 * 取得申請數位存款帳戶的與本行已往來業務_其他說明
	 * @param 
	 * @return
	 */
	public String getBREASONByCusidn(final String cusidn) {
		String result = txnAcnApplyDao.findBREASONByCUSIDN(cusidn);
		return result;
	}
	/**
	 * 取得申請數位存款帳戶的任職機構
	 * @param 
	 * @return
	 */
	public String getEMPLOYERByCusidn(final String cusidn) {
		String result = txnAcnApplyDao.findEMPLOYERByCUSIDN(cusidn);
		return result;
	}
	/**
	 * 調整上傳的圖片尺寸
	 */
	public BufferedImage getScaledInstance(BufferedImage img, int limitedWidth, int limitedHeight, Object hint, boolean higherQuality) {
		
		int initialwidth;
		int initialheight;
		
		int targetWidth;
		int targetHeight;
		
		int finalWidth;
		int finalHeight;
		
		float rate;
		
		int type;
		BufferedImage ret = null;
		
		try {
			initialwidth = img.getWidth();
			initialheight = img.getHeight();
			type = (img.getTransparency() == Transparency.OPAQUE) ? BufferedImage.TYPE_INT_RGB
					: BufferedImage.TYPE_INT_ARGB;
			ret = (BufferedImage) img;
			
			
			//檢查使用者上傳的圖檔寬、高符不符合格式
			
			//寬高都符合規定
			if(initialheight <= limitedHeight && initialwidth <= limitedWidth){
				targetHeight = initialheight;
				targetWidth = initialwidth;
			}//寬符合規定&高不符合
			 else if(initialheight > limitedHeight && initialwidth <= limitedWidth){
				rate = (float)initialheight / limitedHeight;
				targetHeight = limitedHeight;
				targetWidth = (int)((float)initialwidth / rate);
			}//高符合規定&寬不符合
			 else if(initialheight <= limitedHeight && initialwidth > limitedWidth){
				rate = (float)initialwidth / limitedWidth;
				targetHeight = (int)((float)initialheight / rate);
				targetWidth = limitedWidth;
			}//寬高都不符合規定
			 else{
				//原圖高寬比=限制高寬比
				if((float)initialheight / initialwidth == (float)limitedHeight / limitedWidth){
					targetHeight = limitedHeight;
					targetWidth = limitedWidth;
				}//原圖高寬比>限制高寬比
				else if((float)initialheight / initialwidth > (float)limitedHeight / limitedWidth){
					rate = (float)initialheight / limitedHeight;
					targetHeight = limitedHeight;
					targetWidth = (int)((float)initialwidth / rate);
				}//原圖高寬比<限制高寬比
				else{
					rate = (float)initialwidth / limitedWidth;
					targetHeight = (int)((float)initialheight / rate);
					targetWidth = limitedWidth;
				}
			}
			
			//判斷是否需要高品質的縮圖
			if (higherQuality) {
				// Use multi-step technique: start with original size, then
				// scale down in multiple passes with drawImage()
				// until the target size is reached
				finalWidth = img.getWidth();
				finalHeight = img.getHeight();
			} else {
				// Use one-step technique: scale directly from original
				// size to target size with a single drawImage() call
				finalWidth = targetWidth;
				finalHeight = targetHeight;
			}
			do {
				if (higherQuality && finalWidth > targetWidth) {
					finalWidth /= 2;
					if (finalWidth < targetWidth) {
						finalWidth = targetWidth;
					}
				}
				if (higherQuality && finalHeight > targetHeight) {
					finalHeight /= 2;
					if (finalHeight < targetHeight) {
						finalHeight = targetHeight;
					}
				}
				
				BufferedImage tmp = new BufferedImage(finalWidth, finalHeight, type);
				Graphics2D g2 = tmp.createGraphics();
				g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, hint);
				g2.drawImage(ret, 0, 0, finalWidth, finalHeight, null);
				g2.dispose();
				ret = tmp;
			} while (finalWidth != targetWidth || finalHeight != targetHeight);
			
			
		} catch (Exception e) {
			log.error("getScaledInstance.error >> {}", e);
		}
		
		return ret;
		
	}
	/**
	 * 取得申請數位存款帳戶的LOGID
	 * @param 
	 * @return
	 */
	public String getLOGID(final String acn) {
		String result = txnAcnApplyDao.findLOGID(acn);
		return result;
	}	
	
	public TXNACNAPPLY getApplyDataByCUSIDN(final String cusidn) {
		TXNACNAPPLY result = txnAcnApplyDao.getApplyDataByCUSIDN(cusidn);
		return result;
	}
}
