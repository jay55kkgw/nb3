package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netbank.rest.model.CommonPools;
import com.netbank.util.ESAPIUtil;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import lombok.extern.slf4j.Slf4j;

/**
 * N812電文POOL
 */
@Service
@Slf4j
public class N812_Tel_Service extends Common_Tel_Service{

	@Autowired
	private CommonPools commonPools;

	@Override
	public HashMap pre_Processing(Map request){
		return super.pre_Processing(request);
	}
	@Override
	public HashMap process(String serviceID,Map request){
		String UID = (String)request.get("CUSIDN");
		log.debug(ESAPIUtil.vaildLog("UID={}"+UID));
		
		MVH mvh = new MVHImpl();
		mvh = commonPools.n812.getAcnoList(UID);
		
		return data_Retouch(mvh);
	}
	@Override
	public HashMap data_Retouch(MVH mvh){
		MVHImpl mvhimpl = null;
		LinkedList<Row> tmplist = null;
		Boolean result = Boolean.FALSE;
		HashMap<String,Object> dataMap = new HashMap<String,Object>();
		HashMap<String,String> rowMap = new HashMap<String,String>();
		LinkedList<HashMap<String,String>> rowlist = null;
    	try {
    		mvhimpl = (MVHImpl) mvh;
    		log.info("getFlatValues>>"+mvhimpl.getFlatValues());
    		log.info("result.getInputQueryData()>>"+mvhimpl.getInputQueryData());
    		log.info("result.getInputQueryData()>>"+mvhimpl.getOccurs().getRows());
			Iterator<Row> tmp = mvhimpl.getOccurs().getRows().iterator();
			tmplist = new LinkedList<Row>();
			rowlist = new LinkedList<HashMap<String,String>>();
			tmp.forEachRemaining(tmplist::add);
			
			log.info("datalist()>>"+tmplist);
			for(Row  row:tmplist) {
				log.info("getValues>>"+row.getValues());
				
				String CARDNUM = row.getValue("CARDNUM");
				log.debug("CARDNUM={}",row.getValues());
				if(!"".equals(CARDNUM)){
					rowMap = new HashMap<String,String>();
					rowMap.putAll(row.getValues());
					rowlist.add(rowMap);
				}
			}
			dataMap.putAll(mvhimpl.getFlatValues());
			dataMap.put("REC", rowlist);
			result = Boolean.TRUE;
			
		} catch (Exception e) {
			log.error("data_Retouch error >> {} ",e);
		}finally {
			setMsgCode(dataMap,result);
		}
		return dataMap;
	}
}
