package fstop.services.mgn;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.Map;

import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.core.BeanUtils;
import fstop.exception.ToRuntimeException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.AdmHolidayDao;
import fstop.orm.dao.SysParamDataDao;
import fstop.orm.po.ADMHOLIDAY;
import fstop.orm.po.SYSPARAMDATA;
import fstop.services.Auditable;
import fstop.services.CommonService;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class B105 extends CommonService implements Auditable  {

	@Autowired
	private SysParamDataDao sysParamDataDao;
	
	@Autowired
	private AdmHolidayDao admholidayDao;
	
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	@SuppressWarnings("unchecked")
	public MVH doAction(Map _params) { 
		Map<String, String> params = _params;
		boolean isExists = false;
		SYSPARAMDATA po = null;
		try {
			po = sysParamDataDao.findById("NBSYS");
			
			po.getADPK();
			isExists = true;
		}
		catch(ObjectNotFoundException e) {
			isExists = false;
		}
		
		if(!isExists) {
			po = new SYSPARAMDATA();
			po.setADPK("NBSYS");
		}
		
		String[] propertyName = {
				"ADFXSHH",
				"ADFXSMM",
				"ADFXSSS",
				"ADFXEHH",
				"ADFXEMM",
				"ADFXESS",
				"ADFDSHH",
				"ADFDSMM",
				"ADFDSSS",
				"ADFDEHH",
				"ADFDEMM",
				"ADFDESS",
				"ADGDSHH",
				"ADGDSMM",
				"ADGDSSS",
				"ADGDEHH",
				"ADGDEMM",
				"ADGDESS",
				"ADFX1SHH",
				"ADFX1SMM",
				"ADFX1SSS",
				"ADFXE1HH",
				"ADFXE1MM",
				"ADFXE1SS"				
		};
		
		Date d = new Date();
		params.put("LASTDATE", DateTimeUtils.format("yyyyMMdd", d));
		params.put("LASTTIME", DateTimeUtils.format("HHmmss", d));
		
		MVHImpl helper = new MVHImpl();
		if("UPDATE".equalsIgnoreCase(params.get("EXECUTEFUNCTION"))) {
			
			for(String p : propertyName) {
				try {
					BeanUtils.setProperty(po, p, StrUtils.trim(params.get(p)));
				} catch (IllegalAccessException e) {
					throw new ToRuntimeException("BeanUtils.setProperty (" + p + ") Error.", e);
				} catch (InvocationTargetException e) {
					throw new ToRuntimeException("BeanUtils.setProperty (" + p + ") Error.", e);
				}
			}
			
			po.setLASTDATE(params.get("LASTDATE"));
			po.setLASTTIME(params.get("LASTTIME"));

			helper.getFlatValues().putAll(_params);
			
			sysParamDataDao.save(po);
		}
		else {
			for(String p : propertyName) {
				String v = "";
				try {
					v = (String)BeanUtils.getProperty(po, p);
				} catch (IllegalAccessException e) {
					throw new ToRuntimeException("BeanUtils.getProperty (" + p + ") Error.", e);
				} catch (InvocationTargetException e) {
					throw new ToRuntimeException("BeanUtils.getProperty (" + p + ") Error.", e);
				} catch (NoSuchMethodException e) {
					throw new ToRuntimeException("BeanUtils.getProperty (" + p + ") Error.", e);
				}
				helper.getFlatValues().put(p, v);
			}
		}
		
		 
		helper.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		
		
		
		return helper;
	}

	/**
	 * 判斷外匯是否可執行即時交易
	 * @return
	 */	
	public boolean isFxBizTime() {

		Date d = new Date();
		String str_CurrentDate = DateTimeUtils.getDateShort(d);
		log.debug("str_CurrentDate={}",str_CurrentDate);
		String str_CurrentTime = DateTimeUtils.getTimeShort(d);
		log.debug("str_CurrentTime={}",str_CurrentTime);
		
		SYSPARAMDATA syPparam = new SYSPARAMDATA();		
		try{
			syPparam = sysParamDataDao.findById("NBSYS");

			String str_BizStart = syPparam.getADFXSHH() + syPparam.getADFXSMM() + syPparam.getADFXSSS();
			log.debug("str_BizStart={}",str_BizStart);
			String str_BizEnd = syPparam.getADFXEHH() + syPparam.getADFXEMM() + syPparam.getADFXESS();			
			log.debug("str_BizEnd={}",str_BizEnd);
			
//			ADMHOLIDAY holiday = null;
//			try {
//				holiday = (ADMHOLIDAY) admholidayDao.findById(str_CurrentDate);
//			}
//			catch (Exception e) {
//				holiday = null;					
//			}
//			log.debug("holiday={}",holiday);
//			
//			//若非假日 && 系統時間 in 營業時間內
//			if ((holiday == null) && (str_CurrentTime.compareTo(str_BizStart) > 0) && (str_CurrentTime.compareTo(str_BizEnd) < 0)) {				
//				log.debug("business date");
//				return true;
//			}	
//			//若為假日 or 系統時間 not in 營業時間內			
//			else {
//				log.debug("not business date");
//				return false;				
//			}
			
			//換一個寫法
			boolean isHoliday = false;
			
			isHoliday = admholidayDao.isAdmholiday(DateTimeUtils.parse("yyyyMMdd",str_CurrentDate));
			
			//若非假日 && 系統時間 in 營業時間內
			if ((isHoliday == false) && (str_CurrentTime.compareTo(str_BizStart) > 0) && (str_CurrentTime.compareTo(str_BizEnd) < 0)) {				
				log.debug("business date");
				return true;
			}	
			//若為假日 or 系統時間 not in 營業時間內			
			else {
				log.debug("not business date");
				return false;				
			}
			
		}	
		catch(Exception e) {	
			log.error("isFxBizTime error >> {}",e);
		}
		
		return false;
	}			
	/**
	 * 判斷外匯結購售是否可執行即時交易
	 * @return
	 */	
	public boolean isFxBizTime1() {

		Date d = new Date();
		String str_CurrentDate = DateTimeUtils.getDateShort(d);		
		String str_CurrentTime = DateTimeUtils.getTimeShort(d);
		
		SYSPARAMDATA syPparam = new SYSPARAMDATA();		
		try {			
			syPparam = sysParamDataDao.findById("NBSYS");

			String str_BizStart = syPparam.getADFX1SHH() + syPparam.getADFX1SMM() + syPparam.getADFX1SSS();
			String str_BizEnd = syPparam.getADFXE1HH() + syPparam.getADFXE1MM() + syPparam.getADFXE1SS();			
						
			ADMHOLIDAY holiday = null;
			try {
				holiday = (ADMHOLIDAY) admholidayDao.findById(str_CurrentDate);
			}
			catch (Exception e) {
				holiday = null;					
			}
			
			
			//若非假日 && 系統時間 in 營業時間內
			if ((holiday == null) && (str_CurrentTime.compareTo(str_BizStart) > 0) && (str_CurrentTime.compareTo(str_BizEnd) < 0)) {				
				return true;
			}	
			//若為假日 or 系統時間 not in 營業時間內			
			else {
				return false;				
			}
		}	
		catch(Exception e) {	
			log.error("B105.isFxBizTime1() error : "+ e.getMessage());
		}
		
		return false;
	}			

	/**
	 * 判斷基金是否可執行即時交易
	 * @return
	 */	
	public boolean isFundBizTime() {

		Date d = new Date();
		String str_CurrentDate = DateTimeUtils.getDateShort(d);
		String str_CurrentTime = DateTimeUtils.getTimeShort(d);
		
		SYSPARAMDATA syPparam = new SYSPARAMDATA();
		try {
			syPparam = sysParamDataDao.findById("NBSYS");

			String str_BizStart = syPparam.getADFDSHH() + syPparam.getADFDSMM() + syPparam.getADFDSSS();
			String str_BizEnd = syPparam.getADFDEHH() + syPparam.getADFDEMM() + syPparam.getADFDESS();			

			ADMHOLIDAY holiday = null;
			try {
				holiday = (ADMHOLIDAY) admholidayDao.findById(str_CurrentDate);
			}
			catch (Exception e) {
				holiday = null;					
			}
			
			
			//若非假日 && 系統時間 in 營業時間內
			if ((holiday == null) && (str_CurrentTime.compareTo(str_BizStart) > 0) && (str_CurrentTime.compareTo(str_BizEnd) < 0)) {				
				return true;
			}	
			//若為假日 or 系統時間 not in 營業時間內			
			else {
				return false;				
			}
		}	
		catch(Exception e) {	
			log.error("B105.isFundBizTime() error : "+ e.getMessage());
		}
		
		return false;
	}		

	/**
	 * 判斷黃金存摺是否可執行即時交易
	 * @return
	 */
	public boolean isGoldBizTime() {

		Date d = new Date();
		String str_CurrentDate = DateTimeUtils.getDateShort(d);
		String str_CurrentTime = DateTimeUtils.getTimeShort(d);
		
		SYSPARAMDATA syPparam = new SYSPARAMDATA();
		try {
			syPparam = sysParamDataDao.findById("NBSYS");

			String str_BizStart = syPparam.getADGDSHH() + syPparam.getADGDSMM() + syPparam.getADGDSSS();
			String str_BizEnd = syPparam.getADGDEHH() + syPparam.getADGDEMM() + syPparam.getADGDESS();			

			ADMHOLIDAY holiday = null;
			try {
				holiday = (ADMHOLIDAY) admholidayDao.findById(str_CurrentDate);
			}
			catch (Exception e) {
				holiday = null;					
			}
			
			
			//若非假日 && 系統時間 in 營業時間內
			if ((holiday == null) && (str_CurrentTime.compareTo(str_BizStart) > 0) && (str_CurrentTime.compareTo(str_BizEnd) < 0)) {				
				return true;
			}	
			//若為假日 or 系統時間 not in 營業時間內			
			else {
				return false;				
			}
		}	
		catch(Exception e) {	
			log.error("B105.isGoldBizTime() error : "+ e.getMessage());
		}
		
		return false;
	}		
	
	/**
	 * 判斷黃金存摺是否可執行預約交易
	 * @return
	 */	
	public boolean isGoldScheduleTime() {

		Date d = new Date();
		String str_CurrentDate = DateTimeUtils.getDateShort(d);
		String str_CurrentTime = DateTimeUtils.getTimeShort(d);
		
		SYSPARAMDATA syPparam = new SYSPARAMDATA();
		try {
			syPparam = sysParamDataDao.findById("NBSYS");

			int i_SchStartHour = Integer.parseInt(syPparam.getADGDSHH()) - 1;
			String str_SchStartHour = String.valueOf(i_SchStartHour);
			if (str_SchStartHour.length() < 2)
				str_SchStartHour = "0" + str_SchStartHour;
			
			int i_SchStartMin = Integer.parseInt(syPparam.getADGDSMM()) + 59;
			String str_SchStartMin = String.valueOf(i_SchStartMin);
			if (str_SchStartMin.length() < 2)
				str_SchStartMin = "0" + str_SchStartMin;
			
					
			String str_BizStart = str_SchStartHour + str_SchStartMin + syPparam.getADGDSSS();
			String str_BizEnd = syPparam.getADGDEHH() + syPparam.getADGDEMM() + syPparam.getADGDESS();			

			ADMHOLIDAY holiday = null;
			try {
				holiday = (ADMHOLIDAY) admholidayDao.findById(str_CurrentDate);
			}
			catch (Exception e) {
				holiday = null;					
			}
						
            //若為假日  
			if (holiday != null) {
				return true;				
			}
			//若非假日且系統時間不在預約交易時間內
			else if ((str_CurrentTime.compareTo(str_BizStart) >= 0) && (str_CurrentTime.compareTo(str_BizEnd) <= 0)) {				
				return false;
			}	
			//若非假日且系統時間在預約交易時間內			
			else {
				return true;				
			}
		}	
		catch(Exception e) {	
			log.error("B105.isGoldScheduleTime() error : "+ e.getMessage());
		}
		
		return false;
	}		
	public boolean isGoldHOLIDAY() {

		Date d = new Date();
		String str_CurrentDate = DateTimeUtils.getDateShort(d);
		
		try {
			ADMHOLIDAY holiday = null;
			try {
				holiday = (ADMHOLIDAY) admholidayDao.findById(str_CurrentDate);
			}
			catch (Exception e) {
				holiday = null;					
			}
						
            //若為假日  
			if (holiday != null) {
				return true;				
			}
			//若非假日			
			else {
				return false;				
			}
		}	
		catch(Exception e) {	
			log.error("B105.isGoldHOLIDAY() error : "+ e.getMessage());
		}
		
		return false;
	}
	
	/**
	 * 判斷借款還款是否可執行即時交易
	 * @return
	 */	
	public boolean isLoanBizTime() {

		Date d = new Date();
		String str_CurrentDate = DateTimeUtils.getDateShort(d);		
		String str_CurrentTime = DateTimeUtils.getTimeShort(d);
	
		try {			
			
			String str_BizStart ="090000";
			String str_BizEnd = "153000";			
			
			ADMHOLIDAY holiday = null;
			try {
				holiday = (ADMHOLIDAY) admholidayDao.findById(str_CurrentDate);
			}
			catch (Exception e) {
				holiday = null;					
			}
			
			
			//若非假日 && 系統時間 in 營業時間內
			if ((holiday == null) && (str_CurrentTime.compareTo(str_BizStart) > 0) && (str_CurrentTime.compareTo(str_BizEnd) < 0)) {				
				return true;
			}	
			//若為假日 or 系統時間 not in 營業時間內			
			else {
				return false;				
			}
		}	
		catch(Exception e) {	
			log.error("B105.isLoanBizTime() error : "+ e.getMessage());
		}
		
		return false;
	}			

		
	public boolean isHOLIDAY(String day) {		
		try {
			ADMHOLIDAY holiday = null;
			try {
				holiday = (ADMHOLIDAY) admholidayDao.findById(day);
			}
			catch (Exception e) {
				holiday = null;					
			}
						
            //若為假日  
			if (holiday != null) {
				return true;				
			}
			//若非假日			
			else {
				return false;				
			}
		}	
		catch(Exception e) {	
			log.error("B105.isHOLIDAY() error : "+ e.getMessage());
		}
		
		return false;
	}	
	public SYSPARAMDATA getSysParamData() {
		
		SYSPARAMDATA sysParam = sysParamDataDao.findById("NBSYS");
		
		return sysParam;
	}
}
