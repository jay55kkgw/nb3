package fstop.services.batch;

import java.net.InetAddress;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.TelcommResult;
import fstop.notifier.NotifyAngent;
import fstop.services.batch.annotation.Batch;
import fstop.sys.SeverHealthChk;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.IPUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 每日主機對時
 * @author Owner
 *
 */
@Slf4j
@RestController
@Batch(id="batch.CheckServerHealth", name = "每日主機對時", description = "每日主機對時")
public class CheckServerHealth extends DispatchBatchExecute implements BatchExecute {
//	private log log = log.getlog(getClass());
	
	@Autowired
    @Qualifier("nd08Telcomm")
	private TelCommExec nd08Telcomm;
	
	@Value("${batch.admin.mail:}")
	private String adminMail;
	
	public CheckServerHealth(){}
	
//	@Required
//	public void setNd08Telcomm(TelCommExec nd08Telcomm) {
//		this.nd08Telcomm = nd08Telcomm;
//	}
//
//	@Required
//	public void setAdminMail(String mail) {
//		this.adminMail = mail;
//	}
	
	/**
	 * args.get(0) 為等待最後的時間, 格式為 1000, 表示 10 點整
	 */
	@RequestMapping(value = "/batch/checkServerHealth", method = {RequestMethod.POST},
			produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	@Override
	public BatchResult execute(@RequestBody List<String> args) {

		if(args == null || args.size() == 0) {
			args = Arrays.asList( "1000" );
		}
		
		log.info(ESAPIUtil.vaildLog("args.get(0): " + args.get(0)));
		log.info(ESAPIUtil.vaildLog("Today's hh:mm->  " + args.get(0).substring(0, 2) + ":" + args.get(0).substring(2, 4)));
		
		int hour = new Integer(args.get(0).substring(0, 2));
		int mm = new Integer(args.get(0).substring(2, 4));
		
		Date endTime = today(hour, mm);
		
		Hashtable<String, String> params = new Hashtable();
		params.put("TXNIDT", DateTimeUtils.format("yyMMddHHmmss", new Date()));

		boolean isSuccess = false;

		Map<String, Object> data = new HashMap();
		
		if(IPUtils.isLocalTestIp())
			isSuccess = true;
		log.info("new Date().getTime() <= endTime.getTime() && !isSuccess : " + (new Date().getTime() <= endTime.getTime() && !isSuccess));
		while(new Date().getTime() <= endTime.getTime() && !isSuccess) {
	
			if("N".equals(SeverHealthChk.getADNBSTATUS())) {
				log.info("System Close ..... at " + DateTimeUtils.format("HH:mm:ss", new Date()));

				sendZ025Notice();   //通知管理人員
				sleepmin(5);
				continue;
			}
			
			
			boolean isZ014 = false;
			boolean isZ025 = false;
			boolean isUnknow = false;
			try {
				Date d = new Date();
				String cdate = DateTimeUtils.getCDateShort(d);
				String time = DateTimeUtils.getTimeShort(d);
				
				params.put("DATE", cdate);
				params.put("TIME", time);
				params.put("DATE1", cdate);
				params.put("TIME1", time);
				
				TelcommResult qresult = nd08Telcomm.query(params);
				isSuccess = true;
				break;
			}
			catch(TopMessageException e) {
				if("Z014".equalsIgnoreCase(e.getMsgcode())) {
					isZ014 = true;
				}
				else if("Z025".equalsIgnoreCase(e.getMsgcode())) {
					isZ025 = true;
				}
				else {
					isUnknow = true;
					log.info(e.getMsgcode() + " ..... at " + DateTimeUtils.format("HH:mm:ss", new Date()));
				}
			}
			catch(Exception e) {
				isUnknow = true;
				log.info(e.getMessage() + " ..... at " + DateTimeUtils.format("HH:mm:ss", new Date()));
				try {
					sendUnknowNotice(e.getMessage());   //通知管理人員
				}
				catch(Exception ex){}
				
				sleepmin(5);
				continue;
			}
			
			if(isZ014) {
				log.info("Z014 ..... at " + DateTimeUtils.format("HH:mm:ss", new Date()));
				
				sleepmin(5);				
				continue;
			}
			else if(isZ025) {
				log.info("Z025 ..... at " + DateTimeUtils.format("HH:mm:ss", new Date()));
				sendZ025Notice();   //通知管理人員
				
				sleepmin(5);
				continue;
			}
			else if(isUnknow) {
				
				sleepmin(5);
				continue;				
			}
		}
		
		BatchResult result = new BatchResult();
		result.setSuccess(isSuccess);
		result.setBatchName(this.getClass().getSimpleName());
		result.setData(data);
		
		return result;
	}

	private Date today(int hour, int mm) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, hour);
		cal.set(Calendar.MINUTE, mm);
		cal.set(Calendar.SECOND, 0);
		return cal.getTime();
	}
	
	private void sleepmin(int min) {
		try {
			Thread.sleep(1000L * 60 * min); // 五分鐘之後再重送
		}
		catch(Exception e) {}
		
	}
	
	public void sendZ025Notice() {
		Map params = new HashMap();
		params.put("MACHNAME", GetHostName());
		params.put("CURRENTTIME", DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", new Date()));
		params.put("MSG", "所有LU已離線.");
		
		log.info(" CheckServerHealth sendZ025Notice() Receivers == " + this.adminMail);
		
		NotifyAngent.sendNotice("BATCH_ERROR", params, this.adminMail.replace(";", ","),"batch");
	}
	
	public void sendHealthNotice() {
		Map params = new HashMap();
		params.put("MACHNAME", GetHostName());
		params.put("CURRENTTIME", DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", new Date()));
		params.put("MSG", "目前應用系統關閉中, 無法執行 BATCH, 五分鐘之後重試.");
		NotifyAngent.sendNotice("BATCH_ERROR", params, this.adminMail.replace(";", ","));
	}
	
	public void sendUnknowNotice(String unknowMsg) {
		Map params = new HashMap();
		params.put("MACHNAME", GetHostName());
		params.put("CURRENTTIME", DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", new Date()));
		params.put("MSG", "目前Batch發生問題. (" + unknowMsg + ")");
		NotifyAngent.sendNotice("BATCH_ERROR", params, this.adminMail.replace(";", ","));
	}
	
	public void sendUnknowNotice(String unknowMsg,String ProName) {
		Map params = new HashMap();
		params.put("MACHNAME", GetHostName());
		params.put("CURRENTTIME", DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", new Date()));
		params.put("MSG", "目前Batch發生問題. (" + unknowMsg + ")");
		NotifyAngent.sendNotice("BATCH_ERROR", params, this.adminMail.replace(";", ","),ProName);
	}
	
	public void sendUserNotice(String unknowMsg,String RecvMail,String ProName) {
		Map params = new HashMap();
		params.put("MACHNAME", GetHostName());
		params.put("CURRENTTIME", DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", new Date()));
		params.put("MSG", "目前Batch發生問題. (" + unknowMsg + ")");
		NotifyAngent.sendNotice("BATCH_ERROR", params, RecvMail.replace(";", ","),ProName);
	}
	
	public void sendZ300Notice(String unknowMsg) {
		Map params = new HashMap();
		params.put("MACHNAME", GetHostName());
		params.put("CURRENTTIME", DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", new Date()));
		params.put("MSG", "目前押解碼發生問題. (" + unknowMsg + ")");
		NotifyAngent.sendNotice("DES_ERROR", params, this.adminMail.replace(";", ","),"batch");
	}
	
	public String GetHostName()
	{
		String hostname=null;
		try
		{
			InetAddress addr = InetAddress.getLocalHost();
			hostname= addr.getHostName();
		}
		catch(Exception e)
		{
			log.error("GetHostName error : " + e);
			hostname="";
		}
		log.debug("HostName = "+ hostname);
		return(hostname);
	}
}
