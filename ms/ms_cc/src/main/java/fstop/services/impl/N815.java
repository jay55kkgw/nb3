package fstop.services.impl;

import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N815 extends CommonService  {

	@Autowired
	@Qualifier("n815Telcomm")
	private TelCommExec n815Telcomm;
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	@SuppressWarnings("unchecked")
	public MVH doAction(Map params) { 

		//查詢
		Date d = new Date();
		params.put("DATE", DateTimeUtils.getCDateShort(d));
		params.put("TIME", DateTimeUtils.getTimeShort(d));
		
		TelcommResult telcommResult = n815Telcomm.query(params);
		telcommResult.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));

		return telcommResult;
	}


}