package fstop.services.impl;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;


import fstop.exception.TopMessageException;
import fstop.model.EachRowCallback;
import fstop.model.IMasterValuesHelper;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.MVHUtils;
import fstop.model.Row;
import fstop.model.RowsSelecter;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class TD02 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("n832Telcomm")
	private TelCommExec n832Telcomm;
	
//	@Required
//	public void setN832Telcomm(TelCommExec telcomm) {
//		n832Telcomm = telcomm;
//	}
//	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 
		
		Date d = new Date();
		params.put("DATE", DateTimeUtils.getCDateShort(d));
		params.put("TIME", DateTimeUtils.getTimeShort(d));
		params.put("CUSIDN", params.get("UID"));

		String period = (String)params.get("FGPERIOD");
		String enddate = "";
		String stadate = "";
		String periodStr = "";
		
		if("CMCURMON".equals(period)) { //當月
			Calendar cal = Calendar.getInstance();
			Date dend = cal.getTime();

			cal.set(Calendar.DATE, 1);
			Date dstart = cal.getTime();
			
			stadate = DateTimeUtils.getCDateShort(dstart);
			enddate = DateTimeUtils.getCDateShort(dend);
			periodStr = DateTimeUtils.format("yyyy/MM/dd", dstart) + "~" + DateTimeUtils.format("yyyy/MM/dd", dend);
		} 
		else if("CMLASTMON".equals(period)) { //上月
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.DATE, 1);
			cal.add(Calendar.MONTH, -1);
			Date dstart = cal.getTime();
			
			cal.add(Calendar.MONTH, 1);
			cal.add(Calendar.DATE, -1);
			Date dend = cal.getTime();
			
			stadate = DateTimeUtils.getCDateShort(dstart);
			enddate = DateTimeUtils.getCDateShort(dend);			
			periodStr = DateTimeUtils.format("yyyy/MM/dd", dstart) + "~" + DateTimeUtils.format("yyyy/MM/dd", dend);
		} 
		else if("CMLAST2MON".equals(period)) { //上上月
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.DATE, 1);
			cal.add(Calendar.MONTH, -2);
			Date dstart = cal.getTime();
			
			cal.add(Calendar.MONTH, 1);
			cal.add(Calendar.DATE, -1);
			Date dend = cal.getTime();
			
			stadate = DateTimeUtils.getCDateShort(dstart);
			enddate = DateTimeUtils.getCDateShort(dend);			
			periodStr = DateTimeUtils.format("yyyy/MM/dd", dstart) + "~" + DateTimeUtils.format("yyyy/MM/dd", dend);		
		} 
		else if("CMPERIOD".equals(period)) { //指定日期
			stadate = (String)params.get("CMSDATE");
			enddate = (String)params.get("CMEDATE");
			stadate = StrUtils.trim(stadate).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
			enddate = StrUtils.trim(enddate).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
			
			if(stadate != null && stadate.length() == 8) {
				Date sd = DateTimeUtils.parse("yyyyMMdd", stadate);
				stadate = DateTimeUtils.getCDateShort(sd);
				periodStr = DateTimeUtils.format("yyyy/MM/dd", sd);
			}
			if(enddate != null && enddate.length() == 8) {
				Date sd = DateTimeUtils.parse("yyyyMMdd", enddate);
				enddate = DateTimeUtils.getCDateShort(sd);
				periodStr += "~" + DateTimeUtils.format("yyyy/MM/dd", sd);
			}
			else { //若未指定結束時間, 則使用今天日期
				enddate = DateTimeUtils.getCDateShort(new Date());
				periodStr += "~" + DateTimeUtils.format("yyyy/MM/dd", new Date());
			}
		}
				
		params.put("BEGIN_DATE", stadate);
		params.put("END_DATE", enddate);
		
		if ("TRUE".equals(params.get("OKOVNEXT"))) {
			params.put("QUERYNEXT", "");
		}	
		
		MVHImpl result = new MVHImpl();
		String str_CARDTYPE = (String)params.get("CARDTYPE");		

		//一般信用卡
		if (str_CARDTYPE.equals("0")) {
			
			params.put("CARDNUM", params.get("CUSIDN")); 
			TelcommResult helperN832 = n832Telcomm.query(params);
			//helperN832.getFlatValues().put("CARDNUM", "9999999999999999");
			
			//卡號 CARDNUM
			Map<String, IMasterValuesHelper> tables = MVHUtils.groupByToTables(helperN832, new String[]{"CARDNUM"});
			MVHUtils.addTables(helperN832, tables);

			result.addTable(helperN832, "CARDDATA1");
			//int totalcount = Integer.parseInt(helperN832.getValueByFieldName("REC_NO"));
			
			int totalcount = helperN832.getValueOccurs("TRNDATE");
			
			if (totalcount == 0)
				throw TopMessageException.create("ENRD");	
			
			helperN832.getFlatValues().put("REC_NO", Integer.toString(totalcount));
			
			//result.getFlatValues().put("TOTCNT", helperN832.getValueByFieldName("REC_NO"));					
			result.getFlatValues().put("TOTCNT", new DecimalFormat("0").format(totalcount));
			result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
			result.getFlatValues().put("CMPERIOD", periodStr);		

		}//end  #一般信用卡#
		
		//VISA金融卡
		else {
			
			//查全部卡號
			if ("ALL".equals(params.get("CARDNUM"))) {
								
				StringTokenizer stk = new StringTokenizer((String)params.get("CARDNUM_ALL"), ",", false);
				
				int totalcount = 0;
				double totalamt = 0;
				int i = 0;	
				
				while (stk.hasMoreTokens()) {
					
					String str_CardNum = stk.nextToken();

					if (str_CardNum.trim().equals(""))
						continue;
					
					params.put("CARDNUM", str_CardNum);
					
					TelcommResult helperN832 = null;
					try {
						helperN832 = n832Telcomm.query(params);
					}
					catch(TopMessageException e) {
						
						if (e.getMsgcode().equals("ENRD")) {
							continue;						
						}
						else
							throw e;
					}
					
					
					//if (helperN832.getValueByFieldName("REC_NO").equals("00"))				
					//totalcount += Integer.parseInt(helperN832.getValueByFieldName("REC_NO"));
					
					int i_RecNo = helperN832.getValueOccurs("TRNDATE");
					if (i_RecNo == 0)
						continue;

					totalcount += i_RecNo;
	    			
					
					//卡號 CARDNUM
					Map<String, IMasterValuesHelper> tables = MVHUtils.groupByToTables(helperN832, new String[]{"CARDNUM"});
					MVHUtils.addTables(helperN832, tables);

					helperN832.getFlatValues().put("CARDNUM", str_CardNum);	
					helperN832.getFlatValues().put("REC_NO", Integer.toString(i_RecNo));	
					
					result.addTable(helperN832, "CARDDATA"+ (i++));	
					
				}//end while
				
				if (totalcount == 0)
					throw TopMessageException.create("ENRD");						
				
				result.getFlatValues().put("TOTCNT", new DecimalFormat("0").format(totalcount));
				result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
				result.getFlatValues().put("CMPERIOD", periodStr);						
			}
			
			//查單一卡號
			else {						
				TelcommResult helperN832 = n832Telcomm.query(params);
				helperN832.getFlatValues().put("CARDNUM", (String)params.get("CARDNUM"));
				
				//卡號 CARDNUM
				Map<String, IMasterValuesHelper> tables = MVHUtils.groupByToTables(helperN832, new String[]{"CARDNUM"});
				MVHUtils.addTables(helperN832, tables);

				result.addTable(helperN832, "CARDDATA1");
				//int totalcount = Integer.parseInt(helperN832.getValueByFieldName("REC_NO"));
				
				int totalcount = helperN832.getValueOccurs("TRNDATE");
				
				if (totalcount == 0)
					throw TopMessageException.create("ENRD");	
				
				helperN832.getFlatValues().put("REC_NO", Integer.toString(totalcount));
				
				//result.getFlatValues().put("TOTCNT", helperN832.getValueByFieldName("REC_NO"));					
				result.getFlatValues().put("TOTCNT", new DecimalFormat("0").format(totalcount));
				result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
				result.getFlatValues().put("CMPERIOD", periodStr);				
			}																
		}//end  #VISA金融卡#		
	
		return result;	
	}

}
