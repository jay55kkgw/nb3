package fstop.services.impl;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.MVHUtils;
import fstop.model.RowsSelecter;
import fstop.model.TelcommResult;
import fstop.notifier.NotifyAngent;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class TD01_MAIL extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("td01Telcomm")
	private TelCommExec td01Telcomm;
	
	
//	@Required
//	public void setTd01Telcomm(TelCommExec telcomm) {
//		td01Telcomm = telcomm;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 

		
		TelcommResult helper = new TelcommResult(new Vector());
		
		//寄送 email , 並回傳結果
		// 使用 java mail 或 bill hunter

		//TODO Mail body -> params.get("RESULT")
		//TODO Mail account -> params.get("CMMAIL")
		
		boolean result=NotifyAngent.sendNotice("TD01", params, (String)params.get("CMMAIL"));
		
		helper.getFlatValues().put("CMMAIL", (String)params.get("CMMAIL"));
		helper.getFlatValues().put("CMMSG", "成功/失敗");
		helper.getFlatValues().put("RESULT", String.valueOf(result));
		helper.getFlatValues().put("CMQTIME", DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", new Date()));
		
		return helper;
	}


}
