package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class N814 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("n814Telcomm")
	private TelCommExec n814Telcomm;
	
//	@Required
//	public void setN814Telcomm(TelCommExec telcomm) {
//		n814Telcomm = telcomm;
//	}
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 

		//查詢
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);
		params.put("CUSIDN", params.get("CUSIDN"));
		params.put("FLAG", params.get("FLAG")); 
		log.debug(ESAPIUtil.vaildLog("N814 System Out test UID:"+params.get("CUSIDN")+" FLAG:"+params.get("FLAG")));
		/*** 驗證交易密碼 - 開始 ***/
		if (params.get("FGTXWAY") == null) {
			throw TopMessageException.create("S302", (String)params.get("ADOPID"));				
		}			
		else if("0".equals(params.get("FGTXWAY"))) {  //使用交易密碼
			String TransPassUpdate = (String)params.get("TRANSPASSUPDATE");
			String Phase2 = (String)params.get("PHASE2");
			TelCommExec n950CMTelcomm;
		
			if(StrUtils.isNotEmpty(TransPassUpdate))
			{
				if(TransPassUpdate.equals("1") || Phase2.equals("ON"))
				{
					n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n951CMTelcomm");
				}
				else
				{
					n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n950CMTelcomm");
				}
			}
			else
			{
				n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n950CMTelcomm");
			}
		
			// N950會發兩次所以需FGTXWAY=""，N950回應後再把FGTXWAY改成原值即可 2010.09.29
			String fgtxway = (String)params.get("FGTXWAY");
			params.put("FGTXWAY", "");
			MVHImpl n950Result = n950CMTelcomm.query(params);
		
			//若驗證失敗, 則會發動 TopMessageException
			params.put("FGTXWAY", fgtxway);					
		}
		/*** 驗證交易密碼 - 結束 ***/
		
		TelcommResult telcommResult = n814Telcomm.query(params);
		telcommResult.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		
		return telcommResult;
	}


}
