package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class N821 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("n821Telcomm")
	private TelCommExec n821Telcomm;
	
//	@Required
//	public void setN821Telcomm(TelCommExec telcomm) {
//		n821Telcomm = telcomm;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 
		
		Date d = new Date();
		params.put("DATE", DateTimeUtils.getCDateShort(d));
		params.put("TIME", DateTimeUtils.getTimeShort(d));

		TelcommResult telcommResult = n821Telcomm.query(params);
		telcommResult.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));

		return telcommResult;
	}


}
