package fstop.services.impl;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;


import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.MVHUtils;
import fstop.model.RowsSelecter;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class NA02Q extends CommonService  {
//	private Logger logger = Log.getLogger(getClass());

	@Autowired
	@Qualifier("n810Telcomm")
	private TelCommExec n810Telcomm;
	
//	@Required
//	public void setN810Telcomm(TelCommExec telcomm) {
//		n810Telcomm = telcomm;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 

		//查詢
		Date d = new Date();
		params.put("DATE", DateTimeUtils.getCDateShort(d));
		params.put("TIME", DateTimeUtils.getTimeShort(d));
		MVHImpl telcommResult = new MVHImpl();
		try {
			telcommResult = n810Telcomm.query(params);
			telcommResult.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
			telcommResult.getFlatValues().put("CMRECNUM", telcommResult.getValueOccurs("CARDNUM") + "");
		}catch (TopMessageException e) {
			log.warn("N810 Exception:"+e.getMsgcode());
			telcommResult.getFlatValues().put("MSGCOD", e.getMsgcode());
		}
		return telcommResult;
	}
}
