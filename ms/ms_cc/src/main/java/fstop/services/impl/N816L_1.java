package fstop.services.impl;

import java.util.Hashtable;
import java.util.Map;
import java.util.UUID;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.util.CodeUtil;
import com.netbank.util.DateUtils;
import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.JSONUtils;
import fstop.util.DateTimeUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import fstop.ws.fisc.client.*;
import lombok.extern.slf4j.Slf4j;
import fstop.orm.dao.TxnCreditLogDao;
import fstop.orm.dao.TxnLogDao;
import fstop.orm.po.TXNCREDITLOG;
import fstop.orm.po.TXNLOG;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class N816L_1 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());
	
	@Autowired
	private IFiscWebServiceClient fiscWSClient;
//  @Qualifier("FiscWebServiceClient")

//	@Required
//	public void setFiscWSClient(FiscWebServiceClient fiscWSClient) {
//		this.fiscWSClient = fiscWSClient;
//	}

	@Autowired
	private TxnCreditLogDao txnCreditLogDao;
	@Autowired
	private TxnLogDao txnLogDao;

//	@Required
//	public void setTxnCreditLogDao(TxnCreditLogDao txnCreditLogDao) {
//		this.txnCreditLogDao = txnCreditLogDao;
//	}

	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) {
		MVHImpl helperSucceed = new MVHImpl();
		MVHImpl helperFailed =  new MVHImpl();
		DateTime d1 = new DateTime();
		DateTime d2 = new DateTime();
		//驗證交易密碼
		try {
			d1 = new DateTime();
			log.debug("Start check password");
			if("0".equals(params.get("FGTXWAY"))) {  //使用交易密碼
				TelCommExec n950CMTelcomm;
				
				n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n951CMTelcomm");
				log.debug("Execute check password...");
				MVHImpl n950Result = n950CMTelcomm.query(params);
				//若驗證失敗, 則會發動 TopMessageException
			}
			log.debug("End check password (Succeed)");
			d2 = new DateTime();
		} catch (TopMessageException e) {
			TXNLOG po = new TXNLOG();
			po.setADTXNO(UUID.randomUUID().toString());
			po.setADUSERID((String) params.get("CUSIDN"));
			po.setADOPID("N816L");
			po.setFGTXWAY((String) params.get("FGTXWAY"));
			po.setADUSERIP((String) params.get("ADUSERIP"));
			po.setADCONTENT(CodeUtil.toJsonCustom(params));
			po.setADEXCODE(e.getMsgcode());
			po.setADGUID( (String) params.get("ADGUID") == null ? "" :  (String) params.get("ADGUID") );
			po.setLASTDATE(new DateTime().toString("yyyyMMdd"));
			po.setLASTTIME(new DateTime().toString("HHmmss"));
			po.setPSTIMEDIFF(DateUtils.getDiffTimeMills(d1, d2));
			po.setLOGINTYPE("NB");
			//log.trace("PO >> {}", CodeUtil.toJson(po));
			txnLogDao.save(po);
			
			throw e;
		} catch (Exception e) {
			TXNLOG po = new TXNLOG();
			po.setADTXNO(UUID.randomUUID().toString());
			po.setADUSERID((String) params.get("CUSIDN"));
			po.setADOPID("N816L");
			po.setFGTXWAY((String) params.get("FGTXWAY"));
			po.setADUSERIP((String) params.get("ADUSERIP"));
			po.setADCONTENT(CodeUtil.toJsonCustom(params));
			po.setADEXCODE("FE0003");
			po.setADGUID( (String) params.get("ADGUID") == null ? "" :  (String) params.get("ADGUID") );
			po.setLASTDATE(new DateTime().toString("yyyyMMdd"));
			po.setLASTTIME(new DateTime().toString("HHmmss"));
			po.setPSTIMEDIFF(DateUtils.getDiffTimeMills(d1, d2));
			po.setLOGINTYPE("NB");
			//log.trace("PO >> {}", CodeUtil.toJson(po));
			txnLogDao.save(po);
			throw e;
		}
		
		//---交易密碼驗證完畢，開始進行信用卡掛失作業---
		String arrayParamsStr = (String) params.get("ArrayParam");
		String[] arrayParams = arrayParamsStr.split(",");
		if (arrayParams != null) {
			for (String param : arrayParams) {
				d1 = new DateTime();
				String newparam = param.replace(".", ",");
				boolean isSucceed = false;
				log.debug("申請掛失do : " + newparam);
				Map cloneParams = params;
				Map jb = JSONUtils.json2map(newparam);
				cloneParams.putAll(jb);
				
				MVHImpl tr = new MVHImpl();
				
				//呼叫Systex Web Service (通知Fisc)
				Response response = null;
				try {
					log.debug("[WS] Start call WebService(fisc)...");
					response = fiscWSClient.processLost(cloneParams);
					log.debug(ESAPIUtil.vaildLog("[WS] Fisc ResponseCode:" + response.getResponseCode()));
			
					if ("00".equals(response.getProcessCode()) && 
							"00".equals(response.getResponseCode())) {
						isSucceed = true;
					} else {
						log.warn(String.format("[WS] Call WebService(fisc) ProcessCode: %1$s, ResponseCode: %2$s", 
								response.getProcessCode(), response.getResponseCode()));
					}					
					log.debug("[WS] End call WebService(fisc).");
				} catch (Throwable ex) {
					log.error("[WS] Call WebService(fisc) exception: " + ex);
				}
				tr.getFlatValues().putAll(jb);
				
				if (isSucceed) {
					tr.getFlatValues().put("STATUS", "掛失成功");
					helperSucceed.getOccurs().addRow(new Row(tr.getFlatValues()));
				} else {
					tr.getFlatValues().put("STATUS", "掛失失敗");
					helperFailed.getOccurs().addRow(new Row(tr.getFlatValues()));
				}
				d2 = new DateTime();
				
				//掛失交易記錄寫入資料表 TxnCreditLog
				Date txnDatetime = Calendar.getInstance().getTime();
				TXNCREDITLOG txnCreditLog = new TXNCREDITLOG();
				txnCreditLog.setCUSIDN((String) params.get("UID"));
				txnCreditLog.setCARDNUM(tr.getValueByFieldName("CARDNUM"));
				txnCreditLog.setTXNDATE(DateTimeUtils.getDateShort(txnDatetime));
				txnCreditLog.setTXNTIME(DateTimeUtils.getTimeShort(txnDatetime));
				txnCreditLog.setSTATUS(isSucceed?"Y":"N");
				if (response != null) {
					txnCreditLog.setFISC_AUTHCODE(response.getAuthorizationCode());
					txnCreditLog.setFISC_REFNO(response.getReferenceNumber());
					txnCreditLog.setFISC_RESPCODE(response.getResponseCode());
				}
				txnCreditLogDao.save(txnCreditLog);
				
				try {
					TXNLOG po = new TXNLOG();
					po.setADTXNO(UUID.randomUUID().toString());
					po.setADUSERID((String) params.get("CUSIDN"));
					po.setADOPID("N816L");
					po.setFGTXWAY((String) params.get("FGTXWAY"));
					po.setADUSERIP((String) params.get("ADUSERIP"));
					po.setADCONTENT(CodeUtil.toJsonCustom(cloneParams));
					po.setADEXCODE(isSucceed?"":"Z7" + response.getProcessCode());
					po.setADGUID( (String) params.get("ADGUID") == null ? "" :  (String) params.get("ADGUID") );
					po.setLASTDATE(new DateTime().toString("yyyyMMdd"));
					po.setLASTTIME(new DateTime().toString("HHmmss"));
					po.setPSTIMEDIFF(DateUtils.getDiffTimeMills(d1, d2));
					po.setLOGINTYPE("NB");
					txnLogDao.save(po);
				} catch (Exception e) {
					
				}
			}
		}

		MVHImpl helper = new MVHImpl();
		helper.addTable(helperSucceed, "SUCCEED");
		helper.addTable(helperFailed, "FAILED");

		return helper;
	}
	
}
