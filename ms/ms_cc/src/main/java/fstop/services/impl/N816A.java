package fstop.services.impl;

import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import fstop.ws.fisc.client.FiscWebServiceClient;
import fstop.ws.fisc.client.IFiscWebServiceClient;
import fstop.ws.fisc.client.Response;
import lombok.extern.slf4j.Slf4j;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class N816A extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("n816Telcomm")
	private TelCommExec n816Telcomm;
	
//	@Required
//	public void setN816Telcomm(TelCommExec telcomm) {
//		n816Telcomm = telcomm;
//	}

	@Autowired
	private IFiscWebServiceClient fiscWSClient;
//  @Qualifier("FiscWebServiceClient")
	
//	@Required
//	public void setFiscWSClient(FiscWebServiceClient fiscWSClient) {
//		this.fiscWSClient = fiscWSClient;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 
		boolean isN816Fail = true;
		MVHImpl helper = new MVHImpl();
		
		//驗證交易密碼
		log.debug("Start check password");
		if("0".equals(params.get("FGTXWAY"))) {  //使用交易密碼
			TelCommExec n950CMTelcomm;
			n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n951CMTelcomm");
			log.debug("Execute check password...");
			MVHImpl n950Result = n950CMTelcomm.query(params);
			//若驗證失敗, 則會發動 TopMessageException
		}
		log.debug("End check password (Succeed)");
		
		//---交易密碼驗證完畢，開始進行信用卡開卡作業---
		
		//檢核信用卡是否可線上開卡(N816)
		try {
			//設定檢核類別: 掛失 (01:開卡, 02:掛失)
			params.put("KIND", "01");
			MVHImpl chkResult = n816Telcomm.query(params);
			if(!"0000".equals(chkResult.getFlatValues().get("TOPMSG"))){
				helper.getFlatValues().put("STATUS", "Z700");	
			}
			if ("0000".equalsIgnoreCase(chkResult.getValueByFieldName("MSGCOD"))) {
				//當檢核成功時，呼叫Systex Web Service (通知Fisc)
				Response response = null;
				try {
					log.debug("[WS] Start call WebService(fisc) - ProcessActive");
					response = fiscWSClient.processActive(params);
					log.debug(ESAPIUtil.vaildLog("[WS] Fisc ResponseCode:" + response.getResponseCode()));
			
					if ("00".equals(response.getProcessCode()) && 
							"00".equals(response.getResponseCode())) {
						helper.getFlatValues().put("STATUS", "0");
					} else {
						if("04".equals(response.getResponseCode())) {
						   helper.getFlatValues().put("STATUS", "Z704");
						    isN816Fail = false;
						}
						else if("05".equals(response.getResponseCode())) {
							helper.getFlatValues().put("STATUS", "Z705");
							isN816Fail = false;
						}
						else if("06".equals(response.getResponseCode())) {
							helper.getFlatValues().put("STATUS", "Z706");
							isN816Fail = false;
						}
						else if("11".equals(response.getResponseCode())) {
							helper.getFlatValues().put("STATUS", "Z707");
							isN816Fail = false;
						}
						else if("12".equals(response.getResponseCode())) {
							helper.getFlatValues().put("STATUS", "Z712");
							isN816Fail = false;
						}
						else if("16".equals(response.getResponseCode())) {
							helper.getFlatValues().put("STATUS", "Z716");
							isN816Fail = false;
						}
						else if("57".equals(response.getResponseCode())) {
							helper.getFlatValues().put("STATUS", "Z757");
							isN816Fail = false;
						}
						
						log.warn(String.format("[WS] Call WebService(fisc) ProcessCode: %1$s, ResponseCode: %2$s", 
								response.getProcessCode(), response.getResponseCode()));
						
						if(null==response.getResponseCode()) {
							helper.getFlatValues().put("STATUS", "FE0012");
						}
					}
					log.debug("[WS] End call WebService(fisc).");
				} catch (Exception ex) {
					log.error("[WS] Call WebService(fisc) exception: " + ex);
				}
			} 
		} catch(TopMessageException ex) {
			if(isN816Fail) {
			  helper.getFlatValues().put("STATUS", "Z700");	
			}
				log.debug(ESAPIUtil.vaildLog(String.format("卡號: [%1$s]檢核失敗, MSGCOD:[%2$s]" + params.get("CARDNUM") + ex.getMsgcode())));
		}
		
		helper.getFlatValues().put("CARDNUM", (String) params.get("CARDNUM"));
		helper.getFlatValues().put("EXP_DATE", String.format("%1$s/%2$s", params.get("EXP_MON"), params.get("EXP_YEAR")));
		helper.getFlatValues().put("TOPMSG","");
		if(helper.getFlatValues().containsKey("STATUS") && !helper.getFlatValues().get("STATUS").equals("")) {
			helper.getFlatValues().put("TOPMSG",helper.getFlatValues().get("STATUS"));
		}
		
		return helper;
	}
}
