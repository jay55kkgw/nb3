package fstop.services.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

import com.netbank.util.ESAPIUtil;
import com.tbb.ebill.util.MessageCode;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.TrnsDataDao;
import fstop.orm.dao.TrnsPayDao;
import fstop.orm.dao.TrnsXmlDao;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.ws.ebill.EBPayUtil;
import fstop.ws.ebill.EbillBean;
import fstop.ws.ebill.bean.CCard;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Owner
 *
 *         檢查銷帳編號與統一編號是否一致
 */
@Slf4j
public class QR10 extends CommonService {

    @Value("${EBILL_WSDL}")
    String ebillwsdl;

    @Value("${EBILL_SRCID}")
    String ebillsrcid;

    @Value("${cert_path}")
    String cert_path;

    @Value("${soci}")
    String soci;

    @Value("${VA_CHANNEL}")
    String vaChannel;

    @Value("${EBILL_KEYID}")
    String ebillKeyID;

    @Value("${EBILL_connectionTimeout}")
    Integer ebill_connectionTimeout;

    @Value("${EBILL_receiveTimeout}")
    Integer ebill_receiveTimeout;

    @Autowired
    @Qualifier("qr10Telcomm")
    private TelCommExec qr10Telcomm;

    @Autowired
    TrnsDataDao trnsDataDao;

    @Autowired
    TrnsXmlDao trnsXmlDao;

    @Autowired
    TrnsPayDao trnsPayDao;

    /**
     * 
     * @param params {"CARDNUM":"銷帳編號" ,"CUSIDN":"統一編號","BankID":"3碼 ","trin_acn":"轉出帳號"}
     * @return
     */
    @SuppressWarnings("unchecked")
    public MVH doAction(Map params) {
        String BankID = params.get("BankID").toString();
        String trin_acn = params.get("trin_acn").toString();
        String amount = params.get("AMOUNT").toString(); // 應繳金額
        MVHImpl result = new MVHImpl();
        String FEE = "";
        String MSGCOD = "";
        params.put("ITEM", "1");// 項目
        params.put("CARDNUM", params.get("CARDNUM"));// 銷帳編號
        params.put("CUSIDN", params.get("CUSIDN"));// 統一編號
        try {
            result = qr10Telcomm.query(params); // 檢查銷帳編號是否與統一編號一致
            MSGCOD = result.getValueByFieldName("MSGCOD");
            result.getFlatValues().put("MSGCOD", MSGCOD);
        } catch (TopMessageException e) {
            log.warn("QR10 Telcomm Exception:" + e.getMessage());
            MSGCOD = e.getMsgcode();
            result.getFlatValues().put("MSGCOD", MSGCOD);
        }
        if (BankID.equals("050")) {
            if (trin_acn.length() > 11)
                trin_acn = trin_acn.substring(trin_acn.length() - 11);

            result.getFlatValues().put("FEE", "0");
            result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
            log.warn(ESAPIUtil.vaildLog("Q072.java BankID: " + BankID));
        } else { // 呼叫全國繳費網 朝立中台 ，取手續費
            
            String VrfyMacRslt = "";
            String TrnsCode = "";
            String Rcode = "";
            String SessionId = "";
            //組
            CCard billData = new CCard();
            billData.setAccess("QR10");
            billData.setAction("CheckReq");
            billData.setBill("CCard");
            billData.setFeeRefId("00059003");
            billData.setNoticeNo(trin_acn);// 銷帳編號
            billData.setTxnAmount(amount);// 應繳金額
            String uuid = UUID.randomUUID().toString();
            billData.setTrnsNo(uuid);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            String df = sdf.format(new Date());
            log.debug(df);
            billData.setTxnDateTime(df);
            billData.setSrcId(ebillsrcid);
            try {
              //存入交易TRNSDATA
                trnsDataDao.insertTrnsData(billData.getTrnsNo(), billData.getAccess(), billData.getBill(), billData.getAction(), billData.getTxnDateTime());
                // 從 TRNSDATA 取得 apitxnno
                int apitxnno = trnsDataDao.queryTrnsData(billData.getTrnsNo());
                billData.setApiTxnNo(EBPayUtil.padding(Integer.toString(apitxnno), 7, "L", "0"));
                // 產生 XML
                String sendEbillXmlMsg =EBPayUtil.preCreditCard_CheckReq(billData, vaChannel, ebillKeyID);
                log.debug("CreditCard sendEbillXmlMsg >> {}", sendEbillXmlMsg);
             // 存入TRNSXML
                trnsXmlDao.insertTrnsXml(apitxnno, "0900", "7150", df, sendEbillXmlMsg);
                // 存入TRNSXML
                // 送出電文
                String rcvEbillXmlMsg = EbillBean.queryReq(sendEbillXmlMsg, ebillwsdl, cert_path, soci, ebill_connectionTimeout, ebill_receiveTimeout);
                log.debug("CreditCard rcvEbillXmlMsg >> {}", rcvEbillXmlMsg);
                // 存入TRNSXML
                trnsXmlDao.insertTrnsXml(apitxnno, "0910", "7150", df, rcvEbillXmlMsg);
                // 解讀 XML
                EBPayUtil.CreditCard_CheckReq(billData, rcvEbillXmlMsg, vaChannel, ebillKeyID);

                CCard rcvBillData = null;
                if (billData != null) {
                    rcvBillData = billData;
                    log.debug(ESAPIUtil .vaildLog("Q010 rcvBillData  >>>>>>> {}" + rcvBillData.toString()));
                    VrfyMacRslt = rcvBillData.isVrfyMacRslt() == true ? "Y" : "N";
                    TrnsCode = rcvBillData.getTrnsCode() == null ? "" : rcvBillData.getTrnsCode();
                    Rcode = rcvBillData.getRcode() == null ? "" : rcvBillData.getRcode();
                    SessionId =
                            rcvBillData.getSessionId() == null ? "" : rcvBillData.getSessionId();
                    FEE = rcvBillData.getId() == null ? "" : rcvBillData.getId();
                    log.warn(ESAPIUtil.vaildLog("Q072.java BankID: " + BankID));
                    log.warn("Q072.java VrfyMacRslt: " + rcvBillData.isVrfyMacRslt()); // true mac成功
                    log.warn("Q072.java TrnsCode: " + rcvBillData.getTrnsCode()); // 0000 代表成功
                    log.warn("Q072.java TxnDateTime: " + rcvBillData.getTxnDateTime());
                    log.warn("Q072.java ApiTxnNo: " + rcvBillData.getApiTxnNo());
                    log.warn("Q072.java Rcode: " + rcvBillData.getRcode() + "("
                            + MessageCode.getCodeDesc(rcvBillData.getRcode()) + ")"); // 0001代表對方回應成功，反之則回應錯誤訊息
                    log.warn("Q072.java SessionId: " + rcvBillData.getSessionId()); // 成功會取得session
                                                                                    // id
                    log.warn("Q072.java Ic: " + rcvBillData.getIc());
                    log.warn("Q072.java Id: " + rcvBillData.getId()); // id+account的手續費
                    result.getFlatValues().put("CMQTIME", rcvBillData.getTxnDateTime());
                    result.getFlatValues().put("FEE", FEE);
                    result.getFlatValues().put("VrfyMacRslt", VrfyMacRslt);
                    result.getFlatValues().put("TrnsCode", TrnsCode);
                    result.getFlatValues().put("Rcode", Rcode);
                    result.getFlatValues().put("SessionId", SessionId);
                }
            } catch (Exception e) {
                log.warn("Q010 ebill Exception:" + e.getMessage(), e);
            }
        }
        return result;
    }
}
