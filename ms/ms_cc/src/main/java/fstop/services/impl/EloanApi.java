package fstop.services.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;

import com.netbank.rest.util.RESTUtil;
import com.netbank.util.CodeUtil;

import bank.comm.StringUtil;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.services.CommonService;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class EloanApi extends CommonService  {
	
	@Autowired
	RESTUtil restutil;
	
	@Value("${eloanApiTimeOut:120}")
	Integer eloanApiTimeOut;
	
	@Value("${elaonPath:/CXFService/query/console/CxfApi}")
	String elaonPath= "/CXFService/query/console/CxfApi";
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 
		String json = null;
		MVHImpl result = new MVHImpl();
		try {
			Map elaon_params = new HashMap<>();
			elaon_params.put("method", "caCaseonlinequery");
			elaon_params.put("cqitem", params.get("CUSIDN"));
			elaon_params.put("qtype", params.get("FGTXSTATUS"));
			elaon_params.put("aqitem", params.get("QUOTA"));
			json = restutil.send2elaonApi(elaon_params, elaonPath, eloanApiTimeOut, HttpMethod.POST);
			Pattern remove_tags = Pattern.compile("<.+?>");
		    Matcher m = remove_tags.matcher(json);
		    json = m.replaceAll("");
			Map reMap = CodeUtil.fromJson(json, Map.class);
			if(StringUtil.isNotEmpty((String) reMap.get("result"))) {
				result.getFlatValues().put("RESULT",(String)reMap.get("result"));
				result.getFlatValues().put("TOPMSG","");
			}else {
				result.getFlatValues().put("RESULT",(String)reMap.get("msg"));
				result.getFlatValues().put("TOPMSG", "ZX99");
			}
			
		}catch (Exception e) {
			result.getFlatValues().put("TOPMSG", "ZX99");
		}
		return result;
	}


}
