package fstop.services.impl;

import java.util.Date;
import java.util.LinkedList;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.netbank.rest.model.CommonPools;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.IMasterValuesHelper;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.MVHUtils;
import fstop.model.Row;
import fstop.services.CommonService;
import fstop.util.DateTimeUtils;
import fstop.ws.EAIWebServiceTemplate;
import fstop.ws.eai.bean.TD01RQ;
import fstop.ws.eai.bean.TD01RS;
import fstop.ws.eai.bean.TD01RS_Data;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class TD01 extends CommonService {
	// private Logger logger = Logger.getLogger(getClass());

	// @Autowired
	// @Qualifier("td01Telcomm")
	// private TelCommExec td01Telcomm;

	@Autowired
	private EAIWebServiceTemplate eaiwebservicetemplate;

	@Autowired
	private CommonPools commonPools;

	// @Required
	// public void setTd01Telcomm(TelCommExec telcomm) {
	// td01Telcomm = telcomm;
	// }

	/**
	 * 
	 * @param params
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public MVH doAction(Map params) {

		Date d = new Date();
		params.put("DATE", DateTimeUtils.getCDateShort(d));
		params.put("TIME", DateTimeUtils.getTimeShort(d));
		int recCount = 0;

		MVHImpl result = new MVHImpl();
		String CARDTYPE1 = (String) params.get("CARDTYPE1") == null ? "" : (String) params.get("CARDTYPE1");
		int i_Record = 0;
		IMasterValuesHelper n810 = commonPools.n810.getAcnoList((String) params.get("UID"));
		IMasterValuesHelper n813 = commonPools.n813.getAcnoList((String) params.get("UID"));

		// 查詢全部
		// {"CARDTYPE1":"0","CARDNUM":"","CUSIDN":"A123456814","LOGINTYPE":"NB"}
		if ("".equals(params.get("CARDNUM")) || "ALL".equals(params.get("CARDNUM"))) {
			// params.put("PROPERTY", "A");
			if (!CARDTYPE1.equals("1")) { // 一般卡
				// n810 = commonPools.n810.getAcnoList((String) params.get("UID"));
				i_Record = n810.getValueOccurs("VALUE");
			} else { // VISA金融卡\悠遊Debit卡
				// n810 = commonPools.n813.getAcnoList((String) params.get("UID"));
				i_Record = n813.getValueOccurs("VALUE");
			}

			log.info("i_Record >> {} ", i_Record);

			if (i_Record == 0) {
				throw TopMessageException.create("ENRD");
			}
			String str_CUSNAME = "";
			String str_CYCLE = "";

			for (int i = 0; i < i_Record; i++) {

				int totalcount = 0;
				double totalamt = 0;
				String cardNum = "";
				String cardType = "";
				int txncnt = 1;

				TD01RQ rq = new TD01RQ();
				// TD01RS rs = new TD01RS();
				LinkedList<TD01RS_Data> rsData = new LinkedList<TD01RS_Data>();

				if (!CARDTYPE1.equals("1")) { // 一般卡
					cardType = n810.getValueByFieldName("TYPENAME", i + 1);
					cardNum = n810.getValueByFieldName("CARDNUM", i + 1);
				} else { // VISA金融卡\悠遊Debit卡
					cardNum = n813.getValueByFieldName("CARDNUM", i + 1);
					if (cardNum.substring(0, 6).equals("469553"))
						cardType = "VISA金融卡";
					else if (cardNum.substring(0, 6).equals("533360"))
						cardType = "悠遊Debit卡";
					else
						cardType = "未知卡片種類";
				}
				// cardType = cardType.substring(0, cardType.indexOf("-"));
				log.info(ESAPIUtil.vaildLog("cardNum >> "+ cardNum));
				log.info(ESAPIUtil.vaildLog("cardType >> "+ cardType));
				if (cardNum.trim().equals(""))
					continue;

				params.put("CARDNUM", cardNum);

				TD01RS rs = new TD01RS();
				MVHImpl helperTD01 = new MVHImpl();
				try {
					// 改走EAI WEBSERVICE 2019/06/04 BEN
					// TelcommResult helperTD01 = td01Telcomm.query(params);
					params.put("MSGNAME", "TD01");
					params.put("TXNIDN", "TD01");
					params.put("TXNCNT", txncnt);
					TD01_USERDATA_INFO userData = new TD01_USERDATA_INFO();
					//
					String user_org_length = userData.getUSER_ORG();
					String user_type_length = userData.getUSER_TYPE();
					String user_cardnum_length = userData.getUSER_CARDNUM();
					String user_seq_length = userData.getUSER_SEQ();
					String filler_length = userData.getFILLER();
					String user_property = userData.getUSERPROPERTY();
					
					String userDataStr = StringUtils.leftPad("",Integer.parseInt(user_org_length),' ') + 
										 StringUtils.leftPad("",Integer.parseInt(user_type_length),' ') +
										 StringUtils.leftPad("",Integer.parseInt(user_cardnum_length),' ') +
										 StringUtils.leftPad("",Integer.parseInt(user_seq_length),' ') +
										 StringUtils.leftPad("",Integer.parseInt(filler_length),' ') +
										 user_property ;
					
					params.put("USERDATA", userDataStr);

					do {
						rs = getEaiRS(params);
						// 直接跳離迴圈的訊息
						// 如果不是OKLR(結束)或" "(還有資料), 則視為錯誤代碼
						if (!"OKLR".equals(rs.getMSGCOD()) && !"    ".equals(rs.getMSGCOD())) {
							throw TopMessageException.create(rs.getMSGCOD());
						}
						// 帶入USERDATA 及 TXNCNT+1
						params.put("USERDATA", rs.getUSERDATA());
						txncnt++;
						params.put("TXNCNT", txncnt);
						rsData = rs.getREC();
						// 抓取RECCNT做迴圈取資料
						for (int j = 0; j < Integer.parseInt(rs.getRECCNT()); j++) {
							Map<String, String> data = CodeUtil.objectCovert(Map.class, rsData.get(j));
							log.trace("data row >>{}", data);
							Row row = new Row(data);
							helperTD01.getOccurs().addRow(row);
							recCount++;
						}
						// 當TXNCNT !="9999"時代表還有資料 回打
					} while (!"9999".equals(rs.getTXNCNT()));

					// 卡號 CARDNUM
					Map<String, IMasterValuesHelper> tables = MVHUtils.groupByToTables(helperTD01,
							new String[] { "CARDNUM" });
					MVHUtils.addTables(helperTD01, tables);

					if (str_CUSNAME.equals("")) {
						str_CUSNAME = rs.getCUSNAME();
						str_CYCLE = rs.getCYCLE();
					}

					helperTD01.getFlatValues().put("CARDNUM", cardNum);
					helperTD01.getFlatValues().put("CARDTYPE", cardType);
					helperTD01.getFlatValues().put("CARDTYPE1", (String) params.get("CARDTYPE1"));

					result.addTable(helperTD01, "CARDDATA" + (i + 1));

				} catch (TopMessageException e) {
					/*
					 * if (e.getMsgcode().equals("EACC")) throw TopMessageException.create("ENRD");
					 */

					MVHImpl helper2 = new MVHImpl();
					helper2.getFlatValues().put("TOPMSG", e.getMsgcode());
					helper2.getFlatValues().put("ADMSGIN", e.getMsgin());
					helper2.getFlatValues().put("ADMSGOUT", e.getMsgout());
					result.addTable(helper2, "CARDDATA" + (i + 1));
				}

				// 跑完一個卡號 將userdata及txncnt變回""及1
				params.put("USERDATA", "");
				txncnt = 1;
			} // end for i
				// result.getFlatValues().put("TOTAMT", new
				// DecimalFormat("#0.00").format(totalamt));
			result.getFlatValues().put("TOTCNT", String.valueOf(recCount));
			result.getFlatValues().put("CUSNAME", str_CUSNAME);
			result.getFlatValues().put("CYCLE", str_CYCLE);

			if (recCount == 0)
				throw TopMessageException.create("ENRD");

		}
		// 查詢單筆
		else {
			TD01RQ rq = new TD01RQ();
			TD01RS rs = new TD01RS();
			LinkedList<TD01RS_Data> rsData = new LinkedList<TD01RS_Data>();
			String cardType = "";
			// params.put("PROPERTY", "");
			int txncnt = 1;
			// result = td01Telcomm.query(params);
			if (((String) params.get("CARDNUM")).substring(0, 6).equals("469553"))
				cardType = "VISA金融卡";
			else if (((String) params.get("CARDNUM")).substring(0, 6).equals("533360"))
				cardType = "悠遊Debit卡";
			else
				cardType = "未知卡片種類";

			log.debug(ESAPIUtil.vaildLog("CARDNUM >> {} " + (String) params.get("CARDNUM")));
			log.debug("CARDTYPE >> {} ", cardType);
			params.put("MSGNAME", "TD01");
			params.put("TXNIDN", "TD01");
			params.put("TXNCNT", txncnt);
			TD01_USERDATA_INFO userData = new TD01_USERDATA_INFO();
			//
			String user_org_length = userData.getUSER_ORG();
			String user_type_length = userData.getUSER_TYPE();
			String user_cardnum_length = userData.getUSER_CARDNUM();
			String user_seq_length = userData.getUSER_SEQ();
			String filler_length = userData.getFILLER();
			String user_property = userData.getUSERPROPERTY();
			
			String userDataStr = StringUtils.leftPad("",Integer.parseInt(user_org_length),' ') + 
								 StringUtils.leftPad("",Integer.parseInt(user_type_length),' ') +
								 StringUtils.leftPad("",Integer.parseInt(user_cardnum_length),' ') +
								 StringUtils.leftPad("",Integer.parseInt(user_seq_length),' ') +
								 StringUtils.leftPad("",Integer.parseInt(filler_length),' ') +
								 user_property ;
			
			params.put("USERDATA", userDataStr);

			MVHImpl helperTD01 = new MVHImpl();
			try {
				do {
					rs = getEaiRS(params);
					// 直接跳離迴圈的訊息
					// 如果不是OKLR(結束)或" "(還有資料), 則視為錯誤代碼
					if (!"OKLR".equals(rs.getMSGCOD()) && !"    ".equals(rs.getMSGCOD())) {
						throw TopMessageException.create(rs.getMSGCOD());
					}
					// 帶入USERDATA 及 TXNCNT+1
					params.put("USERDATA", rs.getUSERDATA());
					txncnt++;
					params.put("TXNCNT", txncnt);
					rsData = rs.getREC();
					// 抓取RECCNT做迴圈取資料
					for (int j = 0; j < Integer.parseInt(rs.getRECCNT()); j++) {
						Map<String, String> data = CodeUtil.objectCovert(Map.class, rsData.get(j));
						log.trace("data row >>{}", data);
						Row row = new Row(data);
						helperTD01.getOccurs().addRow(row);
						recCount++;
					}
					// 當TXNCNT !="9999"時代表還有資料 回打
				} while (!"9999".equals(rs.getTXNCNT()));

				result.getFlatValues().put("CUSNAME", rs.getCUSNAME());
				result.getFlatValues().put("CYCLE", rs.getCYCLE());
				result.getFlatValues().put("CURRATE", rs.getCURRATE());
				result.getFlatValues().put("PYMTACC", rs.getPYMTACC());
				result.getFlatValues().put("RECCNT", String.valueOf(recCount));
				result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
				// 卡號 CARDNUM
				Map<String, IMasterValuesHelper> tables = MVHUtils.groupByToTables(helperTD01,
						new String[] { "CARDNUM" });
				MVHUtils.addTables(helperTD01, tables);

				double[] totals = new double[] { 0d, 0d };

				if (recCount == 0)
					throw TopMessageException.create("ENRD");
				
				helperTD01.getFlatValues().put("CARDTYPE", cardType);
				result.getFlatValues().put("TOTCNT", String.valueOf(recCount));

				result.getFlatValues().put("CARDTYPE1", (String) params.get("CARDTYPE1"));

				result.addTable(helperTD01, "CARDDATA1");

			} catch (TopMessageException e) {
				/*
				 * if (e.getMsgcode().equals("EACC")) throw TopMessageException.create("ENRD");
				 */
				if (e.getMsgcode().equals("EACC")) {
					throw TopMessageException.create("ENRD");
				}else {
					throw e;
				}
//				MVHImpl helper2 = new MVHImpl();
//				helper2.getFlatValues().put("TOPMSG", e.getMsgcode());
//				helper2.getFlatValues().put("ADMSGIN", e.getMsgin());
//				helper2.getFlatValues().put("ADMSGOUT", e.getMsgout());
//				result.addTable(helper2, "CARDDATA1");

			}

			// do {
			//
			// params.put("USERDATA", rs.getUSERDATA());
			// rq = CodeUtil.objectCovert(TD01RQ.class, params);
			// rs = eaiwebservicetemplate.sendAndReceive(rq, TD01RS.class);
			//
			// if ("000".equals(rs.getRECCNT())) {
			// txncnt=1;
			// rs.setUSERDATA("");
			// continue;
			// } else {
			// for (TD01RS_Data eachRow : rs.getREC()) {
			// // 去除無意義資料
			// if (StrUtils.isEmpty(eachRow.getCRDNAME().trim()) &&
			// StrUtils.isEmpty(eachRow.getPURDATE().trim())
			// && StrUtils.isEmpty(eachRow.getPOSTDATE().trim()) &&
			// StrUtils.isEmpty(eachRow.getCURRENCY().trim())
			// && StrUtils.isEmpty(eachRow.getDESCTXT().trim())) {
			// // do nothing;
			// } else {
			// log.trace("eachRow >>{}",eachRow.toString());
			// rsData.add(eachRow);
			// recCount++;
			// }
			// }
			// txncnt++;
			// }
			// } while (!"9999".equals(rs.getTXNCNT()));

			// MVHImpl helperTD01 = new MVHImpl();
			// for (TD01RS_Data eachRow : rsData) {
			// Map<String, String> data = CodeUtil.objectCovert(Map.class, eachRow);
			// log.trace("data row >>{}" , data);
			// Row row = new Row(data);
			// helperTD01.getOccurs().addRow(row);
			//
			// }

		}

		return result;
	}

	private TD01RS getEaiRS(Map params) {
		TD01RQ rq = CodeUtil.objectCovert(TD01RQ.class, params);
		try {
			TD01RS rs = eaiwebservicetemplate.sendAndReceive(rq, TD01RS.class);
			return rs;
		} catch (TopMessageException e) {
			throw e;
		}

	}

	// public static void main(String[] args) {
	// TD01RS rs = new TD01RS();
	// LinkedList<TD01RS_Data> REC = new LinkedList();
	// List<String> MONEY = new LinkedList();
	// MONEY.add("00000060200");
	// MONEY.add("00000060200");
	// MONEY.add("00000060200");
	// MONEY.add("00000060200");
	// MONEY.add("00000060200");
	// MONEY.add("00000060200");
	// MONEY.add("00000070200");
	// MONEY.add("00000070200");
	// MONEY.add("00000070200");
	// MONEY.add("00000070200");
	// MONEY.add("00000070200");
	// MONEY.add("00000070200");
	// MONEY.add("00000070200-");
	// MONEY.add("00000070200-");
	// MONEY.add("00000070200-");
	// List<String> date = new LinkedList();
	// date.add("1040708");
	// date.add("1040709");
	// date.add("1040710");
	// date.add("1040711");
	// date.add("1040801");
	// date.add("1040802");
	// date.add("1040708");
	// date.add("1040709");
	// date.add("1040710");
	// date.add("1040711");
	// date.add("1040801");
	// date.add("1040802");
	// date.add("1040708");
	// date.add("1040709");
	// date.add("10407010");
	//
	// TD01RS_Data row = new TD01RS_Data();
	//
	// for (int i = 0; i < MONEY.size(); i++) {
	// row.setCRDNAME("王銀色 ");
	// row.setCURAMNT(MONEY.get(i));
	// row.setCURRENCY("TWD");
	// row.setDESCTXT("測試特店 ");
	// row.setPOSTDATE(date.get(i));
	// row.setPURDATE("1080108");
	// row.setSRCAMNT("00000000000");
	// REC.add(row);
	// }
	//
	// rs.setCARDNUM("5588665000001106");
	// rs.setCURRATE("12120");
	// rs.setCUSNAME("許丕鈞 ");
	// rs.setCYCLE("01");
	// rs.setDUEDATE("1040721");
	// rs.setFILLER01(" ");
	// rs.setPYMTACC("99999999999");
	// rs.setREC(REC);
	// rs.setRECCNT("018");
	// rs.setUSERDATA("10801081040711測試特店 ");
	//
	// // 將rs BEAN變成TelcomResult格式
	// int totalcount = 0;
	// double totalamt = 0;
	// String str_CUSNAME = "";
	// String str_CYCLE = "";
	// String cardNum = "";
	// String cardType = "";
	// MVHImpl helperTD01 = new MVHImpl();
	// MVHImpl result = new MVHImpl();
	// log.debug("RS!!!!!!" + rs.toString());
	// try {
	//
	// // 將rs BEAN變成TelcomResult格式
	//
	// LinkedList<TD01RS_Data> rec = rs.getREC();
	// Map<String, String> rsToMap = CodeUtil.objectCovert(Map.class, rs);
	// rsToMap.remove("REC");
	// for (String flatvsKey : rsToMap.keySet()) {
	// helperTD01.getFlatValues().put(flatvsKey, rsToMap.get(flatvsKey));
	// }
	//
	// if (str_CUSNAME.equals("")) {
	// str_CUSNAME = helperTD01.getValueByFieldName("CUSNAME");
	// str_CYCLE = helperTD01.getValueByFieldName("CYCLE");
	// }
	//
	// // 卡號 CARDNUM
	// Map<String, IMasterValuesHelper> tables =
	// MVHUtils.groupByToTables(helperTD01, new String[] { "CARDNUM" });
	// MVHUtils.addTables(helperTD01, tables);
	//
	// // helperTD01.getFlatValues().put("CARDNUM", cardNum);
	// // helperTD01.getFlatValues().put("CARDTYPE", (String)
	// params.get("CARDTYPE"));
	// // helperTD01.getFlatValues().put("CARDTYPE1", (String)
	// // params.get("CARDTYPE1"));
	//
	// result.addTable(helperTD01, "CARDDATA" + 1);
	// log.debug("RECCNT
	// ================================================================> "
	// + helperTD01.getValueByFieldName("RECCNT"));
	// // totalcount += Integer.parseInt(helperTD01.getValueByFieldName("RECCNT"));
	// // totalcount +=
	// Integer.parseInt(helperTD01.getValueByFieldName("__OCCURS"));
	//
	// // totalcount += helperTD01.getValueOccurs("CURAMNT");
	// // totalamt += totals[1];
	//
	// } catch (TopMessageException e) {
	// /*
	// * if (e.getMsgcode().equals("EACC")) throw
	// TopMessageException.create("ENRD");
	// */
	//
	// MVHImpl helper2 = new MVHImpl();
	// helper2.getFlatValues().put("TOPMSG", e.getMsgcode());
	// helper2.getFlatValues().put("ADMSGIN", e.getMsgin());
	// helper2.getFlatValues().put("ADMSGOUT", e.getMsgout());
	// result.addTable(helper2, "CARDDATA" + 1);
	// }
	//
	// // result.getFlatValues().put("TOTAMT", new
	// // DecimalFormat("#0.00").format(totalamt));
	// result.getFlatValues().put("TOTCNT", new
	// DecimalFormat("0").format(totalcount));
	// result.getFlatValues().put("CUSNAME", str_CUSNAME);
	// result.getFlatValues().put("CYCLE", str_CYCLE);
	//
	// // if (totalcount == 0)
	// // throw TopMessageException.create("ENRD");
	// }
	
}
