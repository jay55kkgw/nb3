package fstop.services.impl;

import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.model.TelcommResult;
import fstop.orm.dao.TxnCreditLogDao;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class N816L extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("n810Telcomm")
	private TelCommExec n810Telcomm;
	
//	@Required
//	public void setN810Telcomm(TelCommExec telcomm) {
//		n810Telcomm = telcomm;
//	}

	@Autowired
    @Qualifier("n816Telcomm")
	private TelCommExec n816Telcomm;
	
//	@Required
//	public void setN816Telcomm(TelCommExec telcomm) {
//		n816Telcomm = telcomm;
//	}

	@Autowired
	private TxnCreditLogDao txnCreditLogDao;

//	@Required
//	public void setTxnCreditLogDao(TxnCreditLogDao txnCreditLogDao) {
//		this.txnCreditLogDao = txnCreditLogDao;
//	}

	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 

		Date d = new Date();
		params.put("DATE", DateTimeUtils.getCDateShort(d));
		params.put("TIME", DateTimeUtils.getTimeShort(d));
		
		//查詢持有信用卡清單(N810)
		TelcommResult telcommResult = n810Telcomm.query(params);
		telcommResult.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		telcommResult.getFlatValues().put("CMRECNUM", telcommResult.getValueOccurs("CARDNUM") + "");
		
		//檢查信用卡筆數，逐一檢核信用卡是否可線上掛失(N816)
		if (StrUtils.isNotEmpty(telcommResult.getValueByFieldName("RECNUM"))) {
			boolean isHaveValidCard = false;
			int i_Count = Integer.parseInt(telcommResult.getValueByFieldName("RECNUM"));
			for (int i=0 ; i<i_Count ; i++) {
				Row row = telcommResult.getOccurs().getRow(i);
				//STATUS: 0.一般, 1.已掛失, 2.已註記
				row.setValue("STATUS", "0");
				
				//查詢DB是否已成功掛失(需排除)
				try {
					if (txnCreditLogDao.findSucceedLog(row.getValue("CARDNUM")).size() > 0) {
						//查有成功掛失記錄，註記卡號狀態，於頁面顯示時識別使用
						row.setValue("STATUS", "1");
						continue;
					}
				} catch (Exception e) {
					log.error(String.format("Error, 查詢信用卡卡號掛失記錄: [%1$s], Exception: %2$s", row.getValue("CARDNUM"), e));
				}
				
				//透過中心N816電文檢核
				try {
					params.put("CARDNUM", row.getValue("CARDNUM"));
					//設定檢核類別: 掛失 (01:開卡, 02:掛失)
					params.put("KIND", "02");
					MVHImpl chkResult = n816Telcomm.query(params);
					if ("0000".equalsIgnoreCase(chkResult.getValueByFieldName("MSGCOD"))) {
						//當檢核成功時，複製回傳的[卡片到期日]到該筆卡號中
						row.setValue("EXP_DATE", chkResult.getValueByFieldName("EXP_DATE"));
						row.setValue("EPC_FLAG", chkResult.getValueByFieldName("EPC_FLAG"));
						isHaveValidCard = true;
					} else {
						//檢核失敗，註記卡號狀態，於頁面顯示時識別使用
						row.setValue("STATUS", "2");
					}
				} catch(TopMessageException ex) {
					log.error(String.format("卡號: [%1$s]檢核失敗, MSGCOD:[%2$s], 註記卡號狀態", row.getValue("CARDNUM"), ex.getMsgcode()));
					//檢核失敗，註記卡號狀態，於頁面顯示時識別使用
					row.setValue("STATUS", "2");
				}
				log.debug(String.format("N816L 卡號[%1$s]: {STATUS:%2$s, EPC_FLAG:%3$s, EXP_DATE:%4$s}", 
						row.getValue("CARDNUM"), row.getValue("STATUS"), row.getValue("EPC_FLAG"), row.getValue("EXP_DATE")));
			}
			
			//暫時仍以N816L.jsp頁面顯示提醒訊息，不導向例外錯誤頁面
//			if (!isHaveValidCard) {
//				throw TopMessageException.create("Z037");
//			}
		}
		return telcommResult;
	}
}
