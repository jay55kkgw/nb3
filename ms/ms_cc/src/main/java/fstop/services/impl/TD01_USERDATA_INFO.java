package fstop.services.impl;

import lombok.Data;

@Data
public class TD01_USERDATA_INFO {
	private String USER_ORG ="3"; //length 3
	private String USER_TYPE = "3"; //length 3
	private String USER_CARDNUM = "16" ; //length 16
	private String USER_SEQ = "5"; //length 5
	private String FILLER = "22" ; //length 22
	private String USERPROPERTY = "A" ; //length 1 Default "A"
}
