package fstop.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.rest.model.CommonPools;
import fstop.model.IMasterValuesHelper;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.MVHUtils;
import fstop.model.TelcommResult;
import com.netbank.rest.model.pool.N922Pool;

import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.TXNUSER;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N931 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("n931Telcomm")
	private TelCommExec n931Telcomm;

	@Autowired
    @Qualifier("n930Telcomm")
	private TelCommExec n930Telcomm; //用戶電子郵箱位址變更

	@Autowired
    @Qualifier("c015Telcomm")
	private TelCommExec c015Telcomm;

	@Autowired
    @Qualifier("c113Telcomm")
	private TelCommExec c113Telcomm;
//	
//	private N922Pool n922Pool;

	@Autowired
	private TxnUserDao txnUserDao;

	@Autowired
	private CommonPools commonPools;
	
	@Value("${MSIP}")
	private String msIp;
	
//	@Required
//	public void setTxnUserDao(TxnUserDao txnUserDao) {
//		this.txnUserDao = txnUserDao;
//	}
//
//	@Required
//	public void setN931Telcomm(TelCommExec telcomm) {
//		n931Telcomm = telcomm;
//	}
//	
//	public void setN930Telcomm(TelCommExec telCommExec) {
//		this.n930Telcomm = telCommExec;
//	}
//	
//	@Required
//	public void setC015Telcomm(TelCommExec telcomm) {
//		c015Telcomm = telcomm;
//	}
//
//	@Required
//	public void setC113Telcomm(TelCommExec telcomm) {
//		c113Telcomm = telcomm;
//	}
//	
//	@Required
//	public void setN922Pool(N922Pool pool) {
//		n922Pool = pool;
//	}	
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) {
		
		log.debug(ESAPIUtil.vaildLog("N931 doAction params = "+CodeUtil.toJson(params)));
		String str_Flag = (String)params.get("APPLYFLAG");
		String str_TYPE1 = (String)params.get("TYPE1"); //申請網銀電子對帳單
		String str_TYPE2 = (String)params.get("TYPE2"); //申請信用卡
		String str_TYPE3 = (String)params.get("TYPE3"); //申請基金
		String str_TYPE4 = (String)params.get("TYPE4"); //取消網銀電子對帳單
		String str_TYPE5 = (String)params.get("TYPE5"); //取消信用卡
		String str_TYPE6 = (String)params.get("TYPE6"); //取消基金
		TelcommResult result = null;
		
		//申請電子交易對帳單
		if (str_Flag.equals("A")) {

			//網銀電子對帳單
			if (! str_TYPE1.equals("")) 
				result = (TelcommResult)doNB(params);
			
			//信用卡
			if (! str_TYPE2.equals("")) 
				result = (TelcommResult)doCC(params);			
			
			//基金
			if (! str_TYPE3.equals("")) 
				result = (TelcommResult)doFund(params);			

			/*** 呼叫 Bill Hunter Web Service 進行密碼申請/變更/重置 ***/
			commonPools.bh.changeBillPwd((String)params.get("Cust_id"),(String)params.get("Status"),"",	"",(String)params.get("Email"),DateTimeUtils.getDatetime(new Date()),msIp);	
		}
		//取消電子交易對帳單
		else if (str_Flag.equals("B")) {
			
			//網銀電子對帳單
			if (! str_TYPE4.equals("")) 
				result = (TelcommResult)doNB(params);
			
			//信用卡
			if (! str_TYPE5.equals("")) 
				result = (TelcommResult)doCC(params);			
			
			//基金
			if (! str_TYPE6.equals("")) 
				result = (TelcommResult)doFund(params);					
			
		}

		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		
		return result;
	}
	
	public MVH getTXNUSERBILL(String dpuserid)
	{//依傳入的使用者id查詢出該使用者電子對帳單的設定情況後回傳。FOR N1010_A1.jsp
		MVHImpl result = new MVHImpl();
		TXNUSER txnUser = txnUserDao.findById(dpuserid);
		result.getFlatValues().put("DPBILL", txnUser.getDPBILL());
		result.getFlatValues().put("DPCARDBILL", txnUser.getDPCARDBILL());
		result.getFlatValues().put("DPFUNDBILL", txnUser.getDPFUNDBILL());
		return result;
		
	}

	private MVH doNB(Map params) {
		
		params.put("DATE", DateTimeUtils.getCDateShort(new Date()));
		params.put("TIME", DateTimeUtils.format("HHmmss", new Date()));
		String str_Flag = (String)params.get("APPLYFLAG");
		
//		if (str_Flag.equals("A")) {
//			params.put("FLAG", "02");
//			TelcommResult n930Result = n930Telcomm.query(params);//先發N930更新用戶電子郵箱，如有錯誤會導到topException頁面去
//		}			
		TXNUSER user = txnUserDao.findById((String)params.get("UID"));
		Date d = new Date();
		String date = DateTimeUtils.getCDateShort(d);
		String time = DateTimeUtils.getTimeShort(d);

		if(str_Flag.equals("A") && (! "".equals((String)params.get("TYPE1")) && "".equals((String)params.get("TYPE2"))))
		{	
			user.setDPBILL("Y");
			params.put("ITEM", "10");						
		}else if(str_Flag.equals("A") && (! "".equals((String)params.get("TYPE1")) && ! "".equals((String)params.get("TYPE2"))))
		{	
			user.setDPBILL("Y");
			user.setDPCARDBILL("Y");
			user.setCARDBILLDATE(date + time);
			params.put("ITEM", "11");						
		}else if(str_Flag.equals("B") && (! "".equals((String)params.get("TYPE4")) && "".equals((String)params.get("TYPE5"))))
		{	
			user.setDPBILL("N");		
			params.put("ITEM", "10");						
		}else if(str_Flag.equals("B") && (! "".equals((String)params.get("TYPE4")) && ! "".equals((String)params.get("TYPE5"))))
		{	
			user.setDPBILL("N");
			user.setDPCARDBILL("N");
			user.setCARDBILLDATE(date + time);
			params.put("ITEM", "11");
		}
		TelcommResult telcommResult = n931Telcomm.query(params);
		
		telcommResult.getFlatValues().put("n931Telcomm", DateTimeUtils.getDatetime(new Date()));
			
		user.setBILLDATE(date + time);
		txnUserDao.save(user);

		return telcommResult;		
	}
	
	private MVH doCC(Map params) {
		
		TelCommExec n950CMTelcomm;
		
		
		n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n951CMTelcomm");
			
		MVHImpl n950Result = n950CMTelcomm.query(params);

		TXNUSER user = txnUserDao.findById((String)params.get("UID"));
		String str_Flag = (String)params.get("APPLYFLAG");
		
		if(str_Flag.equals("A") && (! "".equals((String)params.get("TYPE2")) && "".equals((String)params.get("TYPE1"))))
		{	
			user.setDPCARDBILL("Y");
			params.put("ITEM", "01");
		}
		else if(str_Flag.equals("B") && (! "".equals((String)params.get("TYPE5")) && "".equals((String)params.get("TYPE4"))))
		{	
			user.setDPCARDBILL("N");
			params.put("ITEM", "01");
		}
		params.put("DATE", DateTimeUtils.getCDateShort(new Date()));
		params.put("TIME", DateTimeUtils.format("HHmmss", new Date()));		
		TelcommResult telcommResult = n931Telcomm.query(params);
		
		telcommResult.getFlatValues().put("n931Telcomm", DateTimeUtils.getDatetime(new Date()));		
		Date d = new Date();
		String date = DateTimeUtils.getCDateShort(d);
		String time = DateTimeUtils.getTimeShort(d);
		user.setCARDBILLDATE(date + time);
		txnUserDao.save(user);
		
		return telcommResult;
	}
//	private MVH doCredit(Map params) {
//		
//		TelCommExec n950CMTelcomm;
//		
//		
//		n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n951CMTelcomm");
//		
//		MVHImpl n950Result = n950CMTelcomm.query(params);
//		
//		TXNUSER user = txnUserDao.findById((String)params.get("UID"));
//		String str_Flag = (String)params.get("APPLYFLAG");
//		
//		if(str_Flag.equals("A") && (! "".equals((String)params.get("TYPE2")) && "".equals((String)params.get("TYPE1"))))
//		{	
//			user.setDPCARDBILL("Y");
//			params.put("ITEM", "01");
//		}
//		else if(str_Flag.equals("B") && (! "".equals((String)params.get("TYPE5")) && "".equals((String)params.get("TYPE4"))))
//		{	
//			user.setDPCARDBILL("N");
//			params.put("ITEM", "01");
//		}
//		params.put("DATE", DateTimeUtils.getCDateShort(new Date()));
//		params.put("TIME", DateTimeUtils.format("HHmmss", new Date()));		
//		TelcommResult telcommResult = n931Telcomm.query(params);
//		
//		telcommResult.getFlatValues().put("n931Telcomm", DateTimeUtils.getDatetime(new Date()));		
//		Date d = new Date();
//		String date = DateTimeUtils.getCDateShort(d);
//		String time = DateTimeUtils.getTimeShort(d);
//		user.setCARDBILLDATE(date + time);
//		txnUserDao.save(user);
//		
//		return telcommResult;
//	}
	
	private MVH doFund(Map params) {
		
		TXNUSER user = txnUserDao.findById((String)params.get("UID"));
		String str_Flag = (String)params.get("APPLYFLAG");
		
		if(str_Flag.equals("A") && (! "".equals((String)params.get("TYPE3")))) {
			user.setDPFUNDBILL("Y");
			user.setFUNDBILLSENDMODE("2");				
		}	
		else if(str_Flag.equals("B") && (! "".equals((String)params.get("TYPE6")))) {			
			user.setDPFUNDBILL("N");
			user.setFUNDBILLSENDMODE((String)params.get("BILLSENDMODE"));				
		}
		
		Date d = new Date();
		String date = DateTimeUtils.getCDateShort(d);
		String time = DateTimeUtils.getTimeShort(d);
		user.setFUNDBILLDATE(date + time);	
		txnUserDao.save(user);
		
		//同步更新基金主機臨櫃申請狀態
		Map htl_Data = new HashMap();
		
		//20211119 update , 把LOGINTYPE放到htl_Data 內 C113打過去需要過PINNEW() , 會判斷LOGINTYPE看是走E2E還是MB_E2E , 沒帶會出Z300
		htl_Data.put("LOGINTYPE", StrUtils.isNotEmpty((String)params.get("LOGINTYPE"))?(String)params.get("LOGINTYPE"):"");
		
		htl_Data.put("CUSIDN", (String)params.get("UID"));
		htl_Data.put("BILLSENDMODE", user.getFUNDBILLSENDMODE());

		commonPools.n922.removeGetDataCache((String)params.get("UID"));
		MVH mvh = commonPools.n922.getData((String)params.get("UID"));
		String aplbrh = mvh.getValueByFieldName("APLBRH");
		htl_Data.put("BRHCOD", aplbrh);
			
		TelcommResult c015Result = c015Telcomm.query(params);

		htl_Data.put("ZIPCODE", c015Result.getValueByFieldName("ZIPCODE"));						
		htl_Data.put("HTELPHONE", c015Result.getValueByFieldName("HTELPHONE"));			
		htl_Data.put("OTELPHONE", c015Result.getValueByFieldName("OTELPHONE"));			
		htl_Data.put("MTELPHONE", c015Result.getValueByFieldName("MTELPHONE"));			
		htl_Data.put("EMAIL", (String)params.get("DPMYEMAIL"));			
		htl_Data.put("ADDRESS", c015Result.getValueByFieldName("ADDRESS"));			
		htl_Data.put("FGTXWAY", (String)params.get("FGTXWAY"));	
		htl_Data.put("PINNEW", (String)params.get("PINNEW"));
		
		TelcommResult result = c113Telcomm.query(htl_Data);
		
		
		return result;
	}	
}
