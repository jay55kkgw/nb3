package fstop.services.impl;

import java.security.Key;
import java.security.SecureRandom;
//import java.text.DecimalFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

import com.netbank.util.ESAPIUtil;

import bank.comm.ConvCode;

//import webBranch.utility.IMasterValuesHelper;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
//import fstop.model.MVHUtils;
//import fstop.model.RowsSelecter;
//import fstop.model.TelcommResult;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.TXNUSER;
import fstop.services.CommonService;
import fstop.services.batch.CheckServerHealth;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
//import fstop.util.SpringBeanFactory;
//import fstop.util.StrUtils;
import fstop.util.thread.WebServletUtils;
import fstop.va.VaCheck;
import fstop.ws.AcctVerifyErrorCodeMapping;
import fstop.ws.BacctVerifyReq;
import fstop.ws.BacctVerifyResp;
import fstop.ws.BvRequestUtils;
import fstop.ws.VerifyCodeUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class NA03Q extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("n810Telcomm")
	private TelCommExec n810Telcomm;

	@Autowired
	private TxnUserDao txnUserDao;

	@Value("${bv.uri}")
	private String defaultBaseUrlPass;
	@Value("${trustStorePath}")
	private String trustStorePath;
	@Value("${soci}")
	private String soci;
	@Value("${bvReadTimeout}")
	private Integer bvReadTimeout;
	
//	@Required
//	public void setN810Telcomm(TelCommExec telcomm) {
//		n810Telcomm = telcomm;
//	}
	
//	@Required
//	public void setN953Telcomm(TelCommExec telcomm) {
//		n953Telcomm = telcomm;
//	}
		
//	@Required
//	public void setTxnUserDao(TxnUserDao txnUserDao) {
//		this.txnUserDao = txnUserDao;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params) { 

		Map<String, String> params = _params;
		//查詢
		Date d = new Date();
		params.put("DATE", DateTimeUtils.getCDateShort(d));
		params.put("TIME", DateTimeUtils.getTimeShort(d));			
		log.info(ESAPIUtil.vaildLog("UID===>"+params.get("UID")));
		params.put("CUSIDN", params.get("UID"));		
		log.info(ESAPIUtil.vaildLog("CUSIDN===>"+params.get("CUSIDN")));
		log.info(ESAPIUtil.vaildLog("FGTXWAY===>"+params.get("FGTXWAY")));
		log.info(ESAPIUtil.vaildLog("CARDNUM1===>"+params.get("CARDNUM1")));
		log.info(ESAPIUtil.vaildLog("CARDNUM2===>"+params.get("CARDNUM2")));
		log.info(ESAPIUtil.vaildLog("CARDNUM3===>"+params.get("CARDNUM3")));
		log.info(ESAPIUtil.vaildLog("CARDNUM4===>"+params.get("CARDNUM4")));						
		log.info(ESAPIUtil.vaildLog("EXPIREDYEAR===>"+params.get("EXPIREDYEAR")));
		log.info(ESAPIUtil.vaildLog("EXPIREDDATEMONTHS===>"+params.get("EXPIREDDATEMONTHS")));
		log.info(ESAPIUtil.vaildLog("CPRIMBIRTHDAY params===>"+params.get("CPRIMBIRTHDAY")));
		log.info(ESAPIUtil.vaildLog("CPRIMBIRTHDAY _params===>"+_params.get("CPRIMBIRTHDAY")));
		
		String cusidn = params.get("CUSIDN");
		HttpServletRequest request = WebServletUtils.getRequest();
//		HttpSession session = request.getSession();
		String fgtxway = (String)params.get("FGTXWAY")==null? "" : (String)params.get("FGTXWAY");
		String cprimbirthday = (String)params.get("CPRIMBIRTHDAY")==null? "" : (String)params.get("CPRIMBIRTHDAY");
		log.info(ESAPIUtil.vaildLog("cprimbirthday params===>"+cprimbirthday));
		//params.put("FGTXWAY", fgtxway);
//		session.setAttribute("FGTXWAY", fgtxway);//交易機制
		
		// Trust Boundary Violation
		cprimbirthday = ESAPIUtil.validInput(cprimbirthday, "GeneralString", true); 
//		session.setAttribute("CPRIMBIRTHDAY", cprimbirthday);//出生日期
		
//		session.setAttribute("UID", cusidn);//UID
//		log.info("cprimbirthday session===>"+session.getAttribute("CPRIMBIRTHDAY"));	
		MVHImpl telcommResult = new MVHImpl();
		String MSGCODE = "";
		if(params.containsKey("pkcs7Sign") && "4".equals(params.get("FGTXWAY"))) {
			//壓簽章字串比對
			String jsondc = StrUtils.trim(params.get("jsondc").toString());
			String __mac = StrUtils.trim(params.get("pkcs7Sign").toString());
			params.put("__SIGN", __mac);			
			log.warn(ESAPIUtil.vaildLog("NA03Q.java JSONDC:"+jsondc));
			log.warn(ESAPIUtil.vaildLog("NA03Q.java pkcs7Sign:"+__mac));
			try {
				String CertInfo = VaCheck.doVAVerify(__mac, jsondc, "VARule1");//自然人憑證密文驗證
				int VerfyICSrtn = -1;
				if(CertInfo.length()==0) {//密文驗證失敗
					MSGCODE="Z089";
					CheckServerHealth checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
					checkserverhealth.sendZ300Notice("Z089");
					telcommResult.getFlatValues().put("occurMsg", MSGCODE);
					log.warn("NA03Q.java doVAVerify fail: cert information error");
				}
				else
				{
					try {
						VerfyICSrtn = VaCheck.doVerfyICS(CertInfo, cusidn, true);//內政部驗證簽章
						if(VerfyICSrtn!=0)
						{
							MSGCODE="Z089";
							CheckServerHealth checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
							checkserverhealth.sendZ300Notice("Z089");
							telcommResult.getFlatValues().put("occurMsg", MSGCODE);
							log.warn("NA03Q.java doVerfyICS fail");
						}
					}
					catch(Exception e){
						log.warn("NA03Q.java doVerfyICS fail Exception:"+e.getMessage());
					}
				}
			}
			catch(Exception e) {
				log.warn("NA03Q.java doVAVerify fail Exception:"+e.getMessage());
			}
			
		}
		
		
		try {
			
			telcommResult = n810Telcomm.query(params);
			telcommResult.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
			telcommResult.getFlatValues().put("CMRECNUM", telcommResult.getValueOccurs("CARDNUM") + "");
//			telcommResult.getFlatValues().put("occurMsg", "0000");			
			
		}catch (TopMessageException e) {
			log.warn("N810 Exception:"+e.getMsgcode());
			log.info("telcommResult===>"+telcommResult);
//			telcommResult.getFlatValues().put("occurMsg", e.getMsgcode());
//			throw new TopMessageException(e.getMsgcode(), "", "");
		}
		
		try{
			if(fgtxway.equals("3")){//它行信用卡
				
				BacctVerifyReq baReq = new BacctVerifyReq();//塞上行電文的物件
				String cardnum1 = (String)params.get("CARDNUM1")==null? "" : (String)params.get("CARDNUM1");//卡號前四碼
				String cardnum2 = (String)params.get("CARDNUM2")==null? "" : (String)params.get("CARDNUM2");//卡號前中四碼
				String cardnum3 = (String)params.get("CARDNUM3")==null? "" : (String)params.get("CARDNUM3");//卡號後中四碼
				String cardnum4 = (String)params.get("CARDNUM4")==null? "" : (String)params.get("CARDNUM4");//卡號後四碼
				String cardNumber = cardnum1+cardnum2+cardnum3+cardnum4;//卡號
				log.info(ESAPIUtil.vaildLog("cardNumber=========>"+cardNumber));
				String expiredyear = params.get("EXPIREDYEAR").substring(2);//卡片期效抓後兩位
				String expiredmonths = Integer.parseInt(params.get("EXPIREDDATEMONTHS")) < 10 ?"0"+params.get("EXPIREDDATEMONTHS"):params.get("EXPIREDDATEMONTHS");
				log.info(ESAPIUtil.vaildLog("expiredyear=========>"+expiredyear));
				log.info(ESAPIUtil.vaildLog("expiredmonths=========>"+expiredmonths));
				String expiredDate = expiredyear+expiredmonths;
				log.info(ESAPIUtil.vaildLog("expiredDate========>"+expiredDate));								
				baReq.setCardNumber(cardNumber);//卡號											
				baReq.setMti("0100");//固定放0100
				baReq.setProcessingCode("003001");//處理代碼，固定放003001
				baReq.setAmt("000000000000");//交易金額，固定放0
				baReq.setLocalDate(toDateString(d, "yyyyMMdd"));//交易日期
				baReq.setLocalTime(toDateString(d, "HHmmss"));//交易時間
				baReq.setPosEntryMode("812");//端末設備型態，固定放812
				baReq.setAcqBank("050");//收單行代碼，由財金FOCUS平台取得(提出需求者會提供)											
				baReq.setTerminalId("00000001");//端末代號，由財金FOCUS平台取得(提出需求者會提供)
				baReq.setMerchantId("050037934079001");//特店代號，由財金FOCUS平台取得(提出需求者會提供)
				
				//端末交易序號 traceNumber：亂數六位數字
				
				// Use of Cryptographically Weak PRNG
//				Random random = new Random();
//				String ranDomint ="";		
//				for(int i=0; i < 6;i++){
//					ranDomint += random.nextInt(10);
//				}
				SecureRandom securerandom = new SecureRandom();
				String ranDomint = "";		
				for(int i=0; i < 6;i++){
					ranDomint += securerandom.nextInt(10);
				}
				
				log.info("ranDomint====>"+ranDomint);				
				baReq.setTraceNumber(ranDomint);//端末交易序號，取六位亂數數字，數值要唯一值
				
				//卡片效期加密		
				byte[] tracetime = baReq.getLocalTime().getBytes();//交易時間
				String tracetimeHexString = ConvCode.toHexString(tracetime);//交易時間轉HexString
				log.info("tracetime====>"+new String(tracetime,"UTF-8"));
				log.info("tracetimeHexString====>"+tracetimeHexString);
				byte[] hexByteArray = hexStringToByteArray((tracetimeHexString+expiredDate));//交易時間+卡片效期 hex Change to byte array
				byte[] ckparamter1 = hexStringToByteArray("514D67038CBB62536557597D5E02591A");//驗證參數
				byte[] resultencode;
				log.info("ckparamter====>"+new String(ckparamter1,"UTF-8"));
				log.info(ESAPIUtil.vaildLog("hexByteArray====>"+new String(hexByteArray,"UTF-8")));
				byte[] key1;
		        if (ckparamter1.length == 16) {
		            key1 = new byte[24];
		            System.arraycopy(ckparamter1, 0, key1, 0, 16);
		            System.arraycopy(ckparamter1, 0, key1, 16, 8);
		        } else {
		            key1 = ckparamter1;
		        }
				resultencode = des3EncodeECB(key1,hexByteArray);
				log.info("resultencode==>"+toHexString(resultencode));			
				baReq.setExpiredDate(toHexString(resultencode));//卡片效期															
								
				//otherinfo加密						
				byte[] plainTextBytes1 = cusidn.getBytes();//身分證			
				String pwtoHex1 = ConvCode.toHexString(plainTextBytes1);//身分證轉成HexString
				String pwtoHex2 = ConvCode.toHexString(tracetime);//交易時間轉成HexString
				
				// Privacy Violation
//				log.info("pwdtoHex1==>"+pwdtoHex1);
//				log.info("pwdtoHex2==>"+pwdtoHex2);		
				
				byte[] idAddTime = hexStringToByteArray((pwtoHex1+pwtoHex2));//HexToByte(ID 10碼轉成hex string + 交易時間6碼轉成hex string)
				byte[] ckparameter = hexStringToByteArray("514D67038CBB62536557597D5E02591A");//驗證參數			
				byte[] result3DES;			
				byte[] key;
		        if (ckparameter.length == 16) {
		            key = new byte[24];
		            System.arraycopy(ckparameter, 0, key, 0, 16);
		            System.arraycopy(ckparameter, 0, key, 16, 8);
		        } else {
		            key = ckparameter;
		        }		
		        result3DES = des3EncodeECB(key,idAddTime);//3DES加密
		        Map<String, String> oMap = new LinkedHashMap();
		        oMap.put("tag90", toHexString(result3DES));
		        oMap.put("tag91", "03");
		        //補充資訊，須帶{"tag90":"value","tag91":"03"}的格式，tag90及tag91欄位值規則如下:
		        //tag90: hexString(3DES ECB (驗證參數，HexToByte(ID轉成hex string + 交易時間6碼轉成hex string)))，加密後為16bytes，再轉成hex string共為32digital 
		        //tag91:03(固定)
		        baReq.setOtherInfo(JSONUtils.map2json(oMap));//轉成JSON格式
		        		        
				//所有上行欄位用sha 256加密的值，上行欄位先看第一個字母，如果字母都不同順序照著英文字母排序，如第一個字母一樣就看第二個字母，以此類推，然後只加密必填的欄位
		        baReq.setVerifyCode(VerifyCodeUtils.genCode(baReq,"514D67038CBB62536557597D5E02591A"));
//		        Optional<BacctVerifyResp> oQrRes = BvRequestUtils.doRequest(baReq, defaultBaseUrlPass , bvReadTimeout);//打GateWay
		        Optional<BacctVerifyResp> oQrRes = BvRequestUtils.doRequest(baReq, defaultBaseUrlPass , trustStorePath , soci , bvReadTimeout);//打GateWay
				log.info("ResponseCode==>"+oQrRes.get().getResponseCode());
				
				BacctVerifyResp acctVerRes = null;
				if(oQrRes.isPresent()) {
					log.info("Res Object : \r\n" + JSONUtils.toJson(oQrRes.get()));

//					acctVerRes = oQrRes.get();
					if("0000".equals(oQrRes.get().getResponseCode())) {
//						isSuccess = true;

//						log.debug("secureData =====================");
//						log.debug("secureData : " + qrRes.getSecureData());
//						log.debug("secureData =====================");
//						qrBean.fromSecureData(qrRes.getSecureData());
						telcommResult.getFlatValues().put("occurMsg", "0000");
					}
					else {
						String errorMsg = AcctVerifyErrorCodeMapping.getErrorString(oQrRes.get().getResponseCode());
	                    log.error(String.format("ERROR: %s, MSG: %s",
	                            oQrRes.get().getResponseCode(),
	                            errorMsg));
//						isSuccess = false;
	                    throw TopMessageException.create(oQrRes.get().getResponseCode());
					}
				}
				else {
					throw TopMessageException.create("0012"); // 系統忙碌中，請稍候再試，謝謝！
				}
			}						
			
		}catch (TopMessageException e) {
			log.warn("Gateway TopMessageException:"+e.getMsgcode());			
//			logger.info("telcommResult===>"+telcommResult);
//			telcommResult.getFlatValues().put("MSGCOD", e.getMsgcode());
			log.info("Msgout===>"+e.getMsgout());
			telcommResult.getFlatValues().put("occurMsg", e.getMsgcode());
			//throw new TopMessageException(e.getMsgcode(), "", e.getMessage());//參數1:ERROR CODE 參數2:對內顯示訊息 參數3:對外顯示訊息			
		}
		catch(Exception e){
			log.error("身分驗證失敗.", e);
			telcommResult.getFlatValues().put("occurMsg", "ZX99");
		}
								
		return telcommResult;
	}
	//取得使用者email
	public MVH getTXNUSEREMAIL(String dpuserid)
	{
		MVHImpl result = new MVHImpl();
		List<TXNUSER> listtxnUser;
		listtxnUser = txnUserDao.findByUserId(dpuserid);
		log.info(ESAPIUtil.vaildLog("NA03Q listtxnUser====>"+listtxnUser));
		log.info("NA03Q listtxnUser size====>"+listtxnUser.size());
		//TXNUSER txnuser=null;					
		String dpmyemail = (listtxnUser == null || listtxnUser.size() ==0) ? "":listtxnUser.get(0).getDPMYEMAIL();	
		log.info(ESAPIUtil.vaildLog("NA03Q dpmyemail====>"+dpmyemail));
		result.getFlatValues().put("DPMYEMAIL", dpmyemail);
		return result;
		
	}
	//日期、時間格式化
	private static String toDateString(Date d, String pattern) {
		return DateTimeUtils.format(pattern, d);
	}
	
	//3DES ECB加密
	public static byte[] des3EncodeECB(byte[] key,byte[] data) throws Exception{
		
		Key deskey = null;
		DESedeKeySpec spec = new DESedeKeySpec(key);
		SecretKeyFactory keyfactory = SecretKeyFactory.getInstance("DESede");
		deskey = keyfactory.generateSecret(spec);		
		Cipher cipher = Cipher.getInstance("DESede/ECB/NoPadding");
		cipher.init(Cipher.ENCRYPT_MODE, deskey);
		
		byte[] bOut =cipher.doFinal(data);
		return bOut;				
	}
	//hexString to byte array
	public static byte[] hexStringToByteArray(String s) {
	    int len = s.length();
	    byte[] data = new byte[len / 2];
	    for (int i = 0; i < len; i += 2) {
	        data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
	                             + Character.digit(s.charAt(i+1), 16));
	    }
	    return data;
	}
	//change to HexString
	public static String toHexString(byte[] buf) {
		char[] hexChar = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
				'A', 'B', 'C', 'D', 'E', 'F' };

		StringBuffer strBuf = new StringBuffer(buf.length * 2);
		for (int i = 0; i < buf.length; i++) {
			strBuf.append(hexChar[(buf[i] & 0xf0) >>> 4]); // fill left with zero bits
			strBuf.append(hexChar[buf[i] & 0x0f]);
		}
		return strBuf.toString();
	}
}
