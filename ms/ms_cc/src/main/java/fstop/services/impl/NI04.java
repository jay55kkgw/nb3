package fstop.services.impl;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import fstop.model.IMasterValuesHelper;

import fstop.exception.TopMessageException;
import fstop.model.EachRowCallback;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.MVHUtils;
import fstop.model.Row;
import fstop.model.RowsSelecter;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import com.netbank.rest.model.CommonPools;
import bank.comm.StringUtil;

/**
 * 
 * @author Owner
 *
 */
@Transactional
public class NI04 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("ni04Telcomm")
	private TelCommExec ni04Telcomm;
	
//	@Required
//	public void setNi04Telcomm(TelCommExec telcomm) {
//		ni04Telcomm = telcomm;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 
		
		Date d = new Date();
		params.put("DATE", DateTimeUtils.getCDateShort(d));
		params.put("TIME", DateTimeUtils.getTimeShort(d));
		params.put("CUSIDN", params.get("UID"));
             

		//若為取消自動扣繳
		if (params.get("APPLYFLAG").equals("2")) {
			params.put("TSFACN", "99999999999");
			params.put("PYMT_FLAG", "03");
		}
		//若為申請/變更自動扣繳		
		else {
	  		params.put("PYMT_FLAG", params.get("PAYFLAG"));			
		}
		  		
		//FGTXWAY 為交易機制
		if("0".equals(params.get("FGTXWAY"))) {   // 交易密碼
			params.put("CERTACN", StrUtils.repeat(" ", 19));	//即時
		}
		else if("1".equals(params.get("FGTXWAY"))) {  //憑證
			params.put("CERTACN", StrUtils.repeat("1", 19));	//即時
		}
		else if("7".equals(params.get("FGTXWAY"))) {  //IDGATE
			params.put("CERTACN", StrUtils.repeat("C", 19));	//即時
		}
		
		TelcommResult telcommResult = ni04Telcomm.query(params);
		telcommResult.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		
		return telcommResult;
	}

}
