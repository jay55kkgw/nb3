package fstop.services.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

//TODO:JSPUtils的使用有待商確
import com.netbank.rest.model.JSPUtils;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.PotRateDao;
import fstop.orm.dao.RevolvingCreditApplyDao;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.SpringBeanFactory;
import fstop.ws.EAIWebServiceTemplate;
import fstop.ws.eai.bean.CK01RQ;
import fstop.ws.eai.bean.CK01RS;
import fstop.ws.eai.bean.CK01RS_Data;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class CK01 extends CommonService {
	// private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("ck01Telcomm")
	private TelCommExec ck01Telcomm;

	@Autowired
	private EAIWebServiceTemplate eaiwebservicetemplate;

	@Autowired
	private RevolvingCreditApplyDao revolvingCreditApplyDao;

	// @Required
	// public void setCk01Telcomm(TelCommExec ck01Telcomm) {
	// this.ck01Telcomm = ck01Telcomm;
	// }

	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) {

		log.debug("CK01 - doAction() Begin");

		// 2019/06/14 BEN 檢核通過 判斷是否當日重複申請
		Date txnDatetime = Calendar.getInstance().getTime();
		boolean checkapply = revolvingCreditApplyDao.checkapply((String) params.get("UID"),
				DateTimeUtils.getDateShort(txnDatetime));
		if (checkapply) {
			log.error("本日已申請過長期使用循環信用");
			throw TopMessageException.create("Z610");
		}

		MVHImpl helper = new MVHImpl();

		Date d = new Date();
		params.put("DATE", DateTimeUtils.getCDateShort(d));
		params.put("TIME", DateTimeUtils.getTimeShort(d));
		// 長循申請檢核(CK01)
		// TelcommResult telcommResult = ck01Telcomm.query(params);
		CK01RQ rq = new CK01RQ();
		CK01RS rs = new CK01RS();
		int txncnt = 1;
		params.put("MSGNAME", "CK01");
		params.put("TXNIDN", "CK01");
		params.put("TXNCNT", txncnt);
		
		do {
			rs= getEaiRS(params);
			
		}while(!"9999".equals(rs.getTXNCNT()));
		
//		rq = CodeUtil.objectCovert(CK01RQ.class, params);
//		rs = eaiwebservicetemplate.sendAndReceive(rq, CK01RS.class);
		if (!"OKLR".equals(rs.getMSGCOD())) {
			throw TopMessageException.create(rs.getMSGCOD());
		}

		/*
		 * 檢查是否符合申請條件 (需符合下列三項條件) 1. 活卡註記(BLOCK_FG): Y[有活卡] 2. 使用循環註記(CY_USD_FG):
		 * Y[有使用超過一年] 3. 遲繳註記(DELAY_PAY_FG): N[無延遲紀錄]
		 */
		if ("Y".equals(rs.getBLOCKFG()) && "Y".equals(rs.getCYUSDFG()) && "N".equals(rs.getDELAYPAYFG())) {
			LinkedList<CK01RS_Data> cardList = rs.getREC();
			Map<String, String> resultMap = CodeUtil.objectCovert(Map.class, rs);
			resultMap.remove("REC");
			// 去除多餘半形空格及全形空格
			for (String key : resultMap.keySet()) {
				resultMap.put(key, resultMap.get(key).replaceAll(" ", ""));
				resultMap.put(key, resultMap.get(key).replaceAll("　", ""));
			}
			StringBuilder sb = new StringBuilder();
			log.trace("cardList.size() >> {}" ,cardList.size());
			if (cardList.size() > 0) {
				for (int i = 0; i < cardList.size(); i++) {
					sb.append(cardList.get(i).getCARDNUM().trim());
					if ("".equals(cardList.get(i + 1).getCARDNUM().trim())) {
						break;
					} else {
						sb.append(",");
					}
				}
				// for(CK01RS_Data cardNum:cardList) {
				// if(StrUtils.isNotEmpty(cardNum.getCARDNUM().trim())) {
				// sb.append(cardNum.getCARDNUM()+",");
				// }else {
				// break;
				// }
				// }
				log.trace("CardNumList >>{}", sb.toString());
			}

			// String cardNums="";
			// if(sb.toString().length()>0) {
			// cardNums = (sb.substring(0,sb.length()-1)).toString();
			// }
			// 去掉最後一個逗點
			resultMap.put("CARDNUMS", sb.toString());
			log.debug("CARDNUMS>>{}", sb.toString());
			helper.getFlatValues().putAll(resultMap);
			PotRateDao potRateDao = (PotRateDao) SpringBeanFactory.getBean("potRateDao");
			String POT = String.valueOf(Integer.parseInt(resultMap.get("CYCLE"))) + resultMap.get("POT");
			log.warn("CK01.java POT:" + POT);
			String potRate = potRateDao.findByRate(POT);
			log.warn(ESAPIUtil.vaildLog("CK01.java RATE:" + potRate));
			POT = ((resultMap.get("POT")).equals("14") || resultMap.get("POT").equals("15"))
					? String.valueOf(Integer.parseInt(resultMap.get("CYCLE"))) + "13"
					: String.valueOf(Integer.parseInt(resultMap.get("CYCLE"))) + resultMap.get("POT");
			String potRate1 = potRateDao.findByRate(POT);
			if (potRate.length() == 0 || potRate1.length() == 0) {
				log.error("長循申請: 適用利率對照表錯誤，請檢查每日更新排程");
				throw TopMessageException.create("X003");
			} else {
				helper.getFlatValues().put("RATE", potRate);
				helper.getFlatValues().put("RATE1", potRate1);
			}
			log.warn("CK01.java adjust POT:" + POT);
			log.warn(ESAPIUtil.vaildLog("CK01.java RATE1:" + potRate1));
			// 姓名文字半型轉全型
			String cusname = resultMap.get("CUSNAME");
			cusname = JSPUtils.convertFullorHalf(cusname, 1);
			helper.getFlatValues().put("CUSNAME", cusname);
		} else {
			log.error(ESAPIUtil.vaildLog(String.format("長循申請: CUSTID[%1$s]檢核失敗, 不符申請條件", params.get("CUSIDN"))));
			throw TopMessageException.create("X018");
		}

		log.debug("CK01 - doAction() End");

		return helper;
	}
	
	private CK01RS getEaiRS(Map params) {
		CK01RQ rq = CodeUtil.objectCovert(CK01RQ.class, params);
		CK01RS rs = eaiwebservicetemplate.sendAndReceive(rq, CK01RS.class);
		return rs;
	}
}
