package fstop.services.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

import com.netbank.util.ESAPIUtil;
import com.netbank.util.NumericUtil;
import com.tbb.ebill.util.MessageCode;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.notifier.NotifyAngent;
import fstop.orm.dao.PayBillLogDao;
import fstop.orm.dao.TrnsDataDao;
import fstop.orm.dao.TrnsPayDao;
import fstop.orm.dao.TrnsXmlDao;
import fstop.orm.po.PAYBILLLOG;
import fstop.orm.po.TRNSPAY;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import fstop.ws.ebill.EBPayUtil;
import fstop.ws.ebill.EbillBean;
import fstop.ws.ebill.bean.CCard;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 繳納本行國際信用卡帳款FOR QRCODE
 * 
 * @author Owner
 *
 */
@Slf4j
public class Q072 extends CommonService {

    @Value("${EBILL_WSDL}")
    String ebillwsdl;

    @Value("${EBILL_SRCID}")
    String ebillsrcid;

    @Value("${cert_path}")
    String cert_path;

    @Value("${soci}")
    String soci;

    @Value("${VA_CHANNEL}")
    String vaChannel;

    @Value("${EBILL_KEYID}")
    String ebillKeyID;

    @Value("${EBILL_connectionTimeout}")
    Integer ebill_connectionTimeout;

    @Value("${EBILL_receiveTimeout}")
    Integer ebill_receiveTimeout;

    @Autowired
    @Qualifier("q072Telcomm")
    private TelCommExec q072Telcomm;

    @Autowired
    private PayBillLogDao payBillLogDao;

    @Autowired
    TrnsDataDao trnsDataDao;

    @Autowired
    TrnsXmlDao trnsXmlDao;

    @Autowired
    TrnsPayDao trnsPayDao;

    /**
     * 
     * 
     * @param params
     *        {"UID":"","CARDNUM":"信用卡卡號（銷帳編號16)","BankID":"銀行代碼(3)","ipayamt":"轉帳金額(9)","trin_acn":"帳號(11)","IP":""}
     * @return
     */
    @SuppressWarnings("unchecked")
    public MVH doAction(Map _params) {

        // 去空白
        Map<String, String> params = _params;
        for (Map.Entry<String, String> entry : params.entrySet()) {
            params.put(entry.getKey(),
                    StrUtils.isEmpty(entry.getValue()) ? "" : entry.getValue().trim());
        }
        String BankID = params.get("BankID").toString();
        MVHImpl result = new MVHImpl();
        String MSGCODE = "";
        String CARDNUM = params.get("CARDNUM").toString();

        PAYBILLLOG paybilllog = null;
        try {
            paybilllog = payBillLogDao.findByRec(CARDNUM, params.get("ipayamt").toString(), BankID,
                    params.get("trin_acn").toString(), params.get("IP").toString(),
                    DateTimeUtils.format("yyyyMMdd", new Date()));
        } catch (Exception e) {
            log.debug("{}", e, e);
        }
        String LOGTIME1 = "";
        String LOGTIME2 = "";
        int difftime = 0;
        if (paybilllog != null) {
            LOGTIME1 = paybilllog.getLOGTIME() == null ? DateTimeUtils.format("HHmmss", new Date())
                    : paybilllog.getLOGTIME();
            LOGTIME2 = DateTimeUtils.format("HHmmss", new Date());
            difftime = Integer.valueOf(LOGTIME2) - Integer.valueOf(LOGTIME1);
            log.warn(ESAPIUtil.vaildLog("Q072_1.java difftime1:" + difftime + " LOGTIME1:"
                    + LOGTIME1 + " LOGTIME2:" + LOGTIME2));
            if (difftime <= 10 && difftime > 0) {
                MSGCODE = "Z621";
                result.getFlatValues().put("occurMSG", MSGCODE);
            } else {
                result = doJob(params);
            }
        } else {
            result = doJob(params);
        }
        return result;
    }

    @SuppressWarnings("unused")
    private String getContent(String CARDNUM, String AMOUNT, String BANKID, String OUTACN,
            String FEE) {

        if (OUTACN.length() == 11) {
            OUTACN = OUTACN.substring(0, 3) + "-" + OUTACN.substring(3, 5) + "-"
                    + OUTACN.substring(5);
        }
        StringBuilder sb = new StringBuilder();
        sb.append("<table cellSpacing=0 cellPadding=0 width=600 border=0 frame=border >")
                .append("\n");
        sb.append("<tbody>").append("\n");
        sb.append("<tr><td align=left>親愛的客戶您好：</td></tr></br>").append("\n");
        sb.append("<tr><td align=left>感謝您使用臺灣企銀信用卡，您繳納信用卡款資訊：</td></tr></br>").append("\n");
        sb.append("<tr><td align=left>扣款帳號：").append(BANKID).append("-").append(OUTACN).append("</td></tr>").append("\n");
        sb.append("<tr><td align=left>銷帳編號：").append(CARDNUM).append("</td></tr>").append("\n");
        sb.append("<tr><td align=left>繳納金額：NTD ").append(AMOUNT).append("元</td></tr>").append("\n");
        sb.append("<tr><td align=left>手續費　：NTD ").append(FEE).append("元</td></tr>").append("\n");
        sb.append("<tr><td align=left><BR><BR>祝您事業順心，萬事如意!</td></tr>").append("\n");
        sb.append(
                "<tr><td align=left><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;臺灣企銀&nbsp;&nbsp;&nbsp;敬上</td></tr>")
                .append("\n");
        sb.append("</tbody>").append("\n");
        sb.append("</table>").append("\n");
        return sb.toString();
    }

    @SuppressWarnings("unused")
    private MVHImpl doJob(Map params) {
        String BankID = params.get("BankID").toString();
        String trin_acn = params.get("trin_acn").toString();
        MVHImpl result = new MVHImpl();
        String MSGCODE = "";
        String PAYSOURCE = "";
        String ACCOUNT = "";
        String CARDNUM = params.get("CARDNUM").toString();
        PAYBILLLOG paybilllog = new PAYBILLLOG();
        String LOGTIME1 = "";
        String LOGTIME2 = "";
        int difftime = 0;
        if (BankID.equals("050")) {
            if (trin_acn.length() > 11)
                trin_acn = trin_acn.substring(trin_acn.length() - 11);
            Date d = new Date();
            PAYSOURCE = "1";
            ACCOUNT = trin_acn;
            params.put("DATE", DateTimeUtils.getCDateShort(d));
            params.put("TIME", DateTimeUtils.getTimeShort(d));
            params.put("SYNC", "");
            params.put("PPSYNC", "");
            params.put("PINKEY", "");
            params.put("CERTACN", "");
            params.put("FLAG", "4");
            params.put("BNKRA", "050");
            params.put("XMLCA", "");
            params.put("XMLCN", "");
            params.put("PPSYNCN", "");
            params.put("PINNEW", "");
            params.put("CUSIDN", params.get("UID"));
            params.put("ACN", trin_acn);
            params.put("AMOUNT", params.get("ipayamt"));
            params.put("MAC", "");
            params.put("TRANSEQ", "");
            params.put("ISSUER", "");
            params.put("ACNNO", "");
            params.put("ICDTTM", "");
            params.put("ICSEQ", "");
            params.put("ICMEMO", "");
            params.put("TAC", "");
            params.put("TRMID", "");

            try {
                paybilllog = payBillLogDao.findByRec(CARDNUM, params.get("ipayamt").toString(),
                        BankID, params.get("trin_acn").toString(), params.get("IP").toString(),
                        DateTimeUtils.format("yyyyMMdd", new Date()));
                if (paybilllog != null) {
                    LOGTIME1 = paybilllog.getLOGTIME() == null
                            ? DateTimeUtils.format("HHmmss", new Date())
                            : paybilllog.getLOGTIME();
                    LOGTIME2 = DateTimeUtils.format("HHmmss", new Date());
                    difftime = Integer.valueOf(LOGTIME2) - Integer.valueOf(LOGTIME1);
                    log.info(ESAPIUtil.vaildLog("Q072_1.java difftime2:" + difftime + " LOGTIME1:"
                            + LOGTIME1 + " LOGTIME2:" + LOGTIME2));
                    if (difftime <= 10 && difftime > 0) {
                        MSGCODE = "Z621";
                        result.getFlatValues().put("occurMSG", MSGCODE);
                    } else {
                        result = q072Telcomm.query(params);
                        if (result.getValueByFieldName("OUTACN").trim().length() == 4) // Q072交易失敗
                        {
                            result.getFlatValues().put("occurMSG",
                                    result.getValueByFieldName("OUTACN").trim());
                            MSGCODE = result.getValueByFieldName("OUTACN").trim();
                        } else {
                            result.getFlatValues().put("occurMSG", "0000"); // Q072交易成功
                            MSGCODE = "0000";
                            // 繳款成功發送email通知使用者
                            if (params.get("CustEmail").toString().length() > 0) {
                                Hashtable<String, String> varmail1 = new Hashtable();
                                varmail1.put("SUB", "臺灣企銀信用卡款繳納成功通知");
                                varmail1.put("MAILTEXT",
                                        getContent(params.get("CARDNUM").toString(),
                                                params.get("AMOUNT").toString(), BankID, trin_acn,
                                                params.get("FEE").toString()));
                                if (!NotifyAngent.sendNotice("Q072", varmail1,
                                        (String) params.get("CustEmail"))) {
                                    log.warn(ESAPIUtil.vaildLog("繳納本行信用卡款發送Email失敗.(mail:"
                                            + (String) params.get("CustEmail") + ")"));
                                }
                            }
                        }
                        result.getFlatValues().put("CMQTIME",DateTimeUtils.getDatetime(new Date()));
                        params.put("ADEXCODE", MSGCODE);
                        log.warn("Q072_1.java MSGCODE:" + MSGCODE);
                    }
                } else {
                    result = q072Telcomm.query(params);
                    if (result.getValueByFieldName("OUTACN").trim().length() == 4) // Q072交易失敗
                    {
                        result.getFlatValues().put("occurMSG",
                                result.getValueByFieldName("OUTACN").trim());
                        MSGCODE = result.getValueByFieldName("OUTACN").trim();
                    } else {
                        result.getFlatValues().put("occurMSG", "0000"); // Q072交易成功
                        MSGCODE = "0000";
                        // 繳款成功發送email通知使用者
                        if (params.get("CustEmail").toString().length() > 0) {
                            Hashtable<String, String> varmail1 = new Hashtable();
                            varmail1.put("SUB", "臺灣企銀信用卡款繳納成功通知");
                            varmail1.put("MAILTEXT",
                                    getContent(params.get("CARDNUM").toString(),
                                            params.get("AMOUNT").toString(), BankID, trin_acn,
                                            params.get("FEE").toString()));
                            if (!NotifyAngent.sendNotice("Q072", varmail1,
                                    (String) params.get("CustEmail"))) {
                                log.warn(ESAPIUtil.vaildLog("繳納本行信用卡款發送Email失敗.(mail:"
                                        + (String) params.get("CustEmail") + ")"));
                            }
                        }
                    }
                    result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
                    params.put("ADEXCODE", MSGCODE);
                    log.warn("Q072_1.java MSGCODE:" + MSGCODE);
                }
            } catch (TopMessageException e) {
                log.warn("Q072_1.java TelComm Exception:" + e.getMessage());
                MSGCODE = e.getMessage();
                result.getFlatValues().put("occurMSG", e.getMessage());
                params.put("ADEXCODE", e.getMessage());
            }
        } else { // 呼叫全國繳費網 朝立中台
            // TXN_INFO
            PAYSOURCE = "2";
            ACCOUNT = params.get("trin_acn").toString();
            String alphaword = "";
            String[] ABCARRAY = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M",
                    "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
            for (int i = 0; i < 26; i++) {
                if (ABCARRAY[i].equals((params.get("UID").toString()).substring(0, 1))) {
                    log.warn(ESAPIUtil.vaildLog(
                            "ABCARRAY i:" + i + " value:" + ABCARRAY[i] + " UID first alpha:"
                                    + (params.get("UID").toString()).substring(0, 1)));
                    if (i + 1 < 10) {
                        alphaword = "0" + String.valueOf(i + 1);
                    } else {
                        alphaword = String.valueOf(i + 1);
                    }
                    break;
                }
            }
            CARDNUM = "500" + alphaword + (params.get("UID").toString()).substring(1);
            params.put("CARDNUM1", CARDNUM);

            CCard billData = new CCard();
            billData.setAccess("NB");
            billData.setAction("PayReq");
            billData.setBill("CreditCard");
            billData.setSessionId(params.get("SessionId").toString());
            billData.setTrnsCode(params.get("TrnsCode").toString());
            billData.setHc(params.get("FEE").toString());
            billData.setNoticeNo(params.get("CARDNUM1").toString());
            billData.setTxnAmount(params.get("ipayamt").toString());
            billData.setFeeRefId("00059003");
            // PAY_INFO
            billData.setPayType("ID");
            billData.setTraBankId(params.get("BankID").toString());
            billData.setTraAccount(params.get("trin_acn").toString());
            billData.setPayIdnBan(params.get("UID").toString());
            billData.setTerminalType("");
            billData.setTerminalId("");
            billData.setTerminalCheckId("");
            // UUID
            String uuid = UUID.randomUUID().toString();
            billData.setTrnsNo(uuid);
            //
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            String df = sdf.format(new Date());
            billData.setTxnDateTime(df);
            billData.setSrcId(ebillsrcid);
            try {
                log.debug("Q072 STEP4 EbillServiceAxisImpl BEGIN:"
                        + DateTimeUtils.getDatetime(new Date()));
                paybilllog = payBillLogDao.findByRec(CARDNUM, params.get("ipayamt").toString(),
                        BankID, params.get("trin_acn").toString(), params.get("IP").toString(),
                        DateTimeUtils.format("yyyyMMdd", new Date()));
                log.debug("Q072 STEP5 trnsDataDao insertTrnsData :"
                        + DateTimeUtils.getDatetime(new Date()));
                // 存入交易TRNSDATA
                trnsDataDao.insertTrnsData(billData.getTrnsNo(), billData.getAccess(),
                        billData.getBill(), billData.getAction(), billData.getTxnDateTime());
                // 從 TRNSDATA 取得 apitxnno
                int apitxnno = trnsDataDao.queryTrnsData(billData.getTrnsNo());
                billData.setApiTxnNo(EBPayUtil.padding(Integer.toString(apitxnno), 7, "L", "0"));
                log.debug("Q072 STEP6 TRNSPAY  :" + DateTimeUtils.getDatetime(new Date()));
                // 存入TRNSCHECK
                TRNSPAY trnsPay = new TRNSPAY();
                trnsPay.setTXN_DATETIME(billData.getTxnDateTime());
                trnsPay.setRCODE(billData.getRcode());
                trnsPay.setRCODE_DESC(billData.getRcodeDesc());
                trnsPay.setSRC_ID(billData.getSrcId());
                trnsPay.setFEE_REF_ID(billData.getFeeRefId());
                trnsPay.setSESSION_ID(billData.getSessionId());
                trnsPay.setHC(billData.getHc());
                trnsPay.setTXN_AMOUNT(billData.getTxnAmount());
                trnsPay.setSTAN(billData.getStan());
                trnsPay.setPAYTYPE(billData.getPayType());
                trnsPay.setTRABANKID(billData.getTraBankId());
                trnsPay.setTRAACCOUNT(billData.getTraAccount());
                trnsPay.setPAYIDNBAN(billData.getPayIdnBan());
                trnsPay.setTRNSCODE(billData.getTrnsCode());
                trnsPay.setAPI_TXN_NO(apitxnno);
                trnsPay.setMTI("0900");
                trnsPay.setPCODE("7150");
                log.debug("trnsPay {}", trnsPay.toString());
                trnsPayDao.save(trnsPay);
                // 產生 XML
                log.debug("Q072 STEP7 sendEbillXmlMsg  :" + DateTimeUtils.getDatetime(new Date()));
                String sendEbillXmlMsg =
                        EBPayUtil.preReqCreditCard_PayReq(billData, vaChannel, ebillKeyID);
                log.debug("pCreditCard sendEbillXmlMsg >> {} " + sendEbillXmlMsg);
                // 存入TRNSXML
                trnsXmlDao.insertTrnsXml(apitxnno, "0900", "7150", df, sendEbillXmlMsg);
                log.debug("preRep >> " + sendEbillXmlMsg);
                // 送出電文
                String rcvEbillXmlMsg = EbillBean.payReq(sendEbillXmlMsg, ebillwsdl, cert_path,
                        soci, ebill_connectionTimeout, ebill_receiveTimeout);
                log.debug("CreditCard rcvEbillXmlMsg >> {}" + rcvEbillXmlMsg);
                // 存入TRNSXML
                trnsXmlDao.insertTrnsXml(apitxnno, "0910", "7150", df, rcvEbillXmlMsg);
                log.debug("Q072 STEP8 CreditCard rcvEbillXmlMsg  >> {}" + rcvEbillXmlMsg);
                EBPayUtil.preRepCreditCard_PayReq(billData, rcvEbillXmlMsg, vaChannel, ebillKeyID);
                // 存入TRNSCHECK
                trnsPay = trnsPayDao.findById(apitxnno);
                if (trnsPay != null) {
                    if (billData.getTxnDateTime() != null)
                        trnsPay.setTXN_DATETIME(billData.getTxnDateTime());
                    if (billData.getRcode() != null)
                        trnsPay.setRCODE(billData.getRcode());
                    if (billData.getRcodeDesc() != null)
                        trnsPay.setRCODE_DESC(billData.getRcodeDesc());
                    if (billData.getSrcId() != null)
                        trnsPay.setSRC_ID(billData.getSrcId());
                    if (billData.getFeeRefId() != null)
                        trnsPay.setFEE_REF_ID(billData.getFeeRefId());
                    if (billData.getSessionId() != null)
                        trnsPay.setSESSION_ID(billData.getSessionId());
                    if (billData.getHc() != null)
                        trnsPay.setHC(billData.getHc());
                    if (billData.getTxnAmount() != null)
                        trnsPay.setTXN_AMOUNT(billData.getTxnAmount());
                    if (billData.getStan() != null)
                        trnsPay.setSTAN(billData.getStan());
                    if (billData.getPayType() != null)
                        trnsPay.setPAYTYPE(billData.getPayType());
                    if (billData.getTraBankId() != null)
                        trnsPay.setTRABANKID(billData.getTraBankId());
                    if (billData.getTraAccount() != null)
                        trnsPay.setTRAACCOUNT(billData.getTraAccount());
                    else
                        trnsPay.setTRAACCOUNT(CARDNUM);
                    if (billData.getPayIdnBan() != null)
                        trnsPay.setPAYIDNBAN(billData.getPayIdnBan());
                    if (billData.getTrnsCode() != null)
                        trnsPay.setTRNSCODE(billData.getTrnsCode());
                    trnsPay.setAPI_TXN_NO(apitxnno);
                    trnsPayDao.update(trnsPay);
                }

                CCard rcvBillData = null;
                if (billData != null) {
                    rcvBillData = billData;
                    log.debug("rcvBillData : " + rcvBillData.toString());
                    String VrfyMacRslt = rcvBillData.isVrfyMacRslt() == true ? "Y" : "N";
                    String TrnsCode = rcvBillData.getTrnsCode() == null ? "" : rcvBillData.getTrnsCode();
                    String Rcode = rcvBillData.getRcode() == null ? "" : rcvBillData.getRcode();
                    log.info("VrfyMacRslt: " + rcvBillData.isVrfyMacRslt());
                    log.info("TrnsCode: " + rcvBillData.getTrnsCode());
                    log.info("TxnDateTime: " + rcvBillData.getTxnDateTime());
                    log.info("ApiTxnNo: " + rcvBillData.getApiTxnNo());
                    log.info("Rcode: " + rcvBillData.getRcode() + "("
                            + MessageCode.getCodeDesc(rcvBillData.getRcode()) + ")");
                    log.info("SessionId: " + rcvBillData.getSessionId());
                    log.info("Stan: " + rcvBillData.getStan());
                    log.info("NoticeNo: " + rcvBillData.getNoticeNo());
                    log.info("TxnAmount: " + rcvBillData.getTxnAmount());
                    log.info("Q072 STEP10 EbillServiceAxisImpl END:"
                            + DateTimeUtils.getDatetime(new Date()));
                    // ebill交易失敗
                    if (!VrfyMacRslt.equals("Y") || !TrnsCode.equals("0000")
                            || !Rcode.equals("0001")) {
                        if (!VrfyMacRslt.equals("Y")) {
                            result.getFlatValues().put("occurMSG", "0302");
                            MSGCODE = "0302";
                        }
                        if (!TrnsCode.equals("0000") && Rcode.length() == 0) {
                            result.getFlatValues().put("occurMSG", "ZX99");
                            MSGCODE = "ZX99";
                        }
                        if (VrfyMacRslt.equals("Y") && !Rcode.equals("0001")) {
                            result.getFlatValues().put("occurMSG1", Rcode);
                            result.getFlatValues().put("occurMSG1DESC",
                                    MessageCode.getCodeDesc(Rcode));
                            MSGCODE = Rcode;
                        }
                        if (TrnsCode.equals("1002")) {
                            result.getFlatValues().put("occurMSG", "Z621");
                            MSGCODE = "Z621";
                        }
                    } else if (VrfyMacRslt.equals("Y") && TrnsCode.equals("0000")
                            && Rcode.equals("0001")) {
                        result.getFlatValues().put("occurMSG", "0000"); // ebill交易成功
                        MSGCODE = "0000";
                        // 繳款成功發送email通知使用者
                        if (params.get("CustEmail").toString().length() > 0) {
                            Hashtable<String, String> varmail1 = new Hashtable();
                            varmail1.put("SUB", "臺灣企銀信用卡款繳納成功通知");
                            varmail1.put("MAILTEXT",getContent(params.get("CARDNUM").toString(),params.get("ipayamt").toString(), BankID, trin_acn,params.get("FEE").toString()));
                            if (!NotifyAngent.sendNotice("Q072", varmail1,
                                    (String) params.get("CustEmail"))) {
                                log.warn(ESAPIUtil.vaildLog("繳納本行信用卡款發送Email失敗.(mail:"
                                        + (String) params.get("CustEmail") + ")"));
                            }
                        }
                    }
                }
                result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
            } catch (Exception e) {
                log.warn("Q072_1.java ebill Exception:" + e.getMessage());
                result.getFlatValues().put("occurMSG", "ZX99");
                MSGCODE = "ZX99";
            }
            log.warn("Q072_1.java ebill MSGCODE:" + MSGCODE);
            params.put("ADEXCODE", MSGCODE);
            result.getFlatValues().put("TOPMSG", MSGCODE);
        }
        PAYBILLLOG attrib = new PAYBILLLOG();
        attrib.setCUSIDN(params.get("UID").toString());
        attrib.setCARDNUM(params.get("CARDNUM").toString());
        attrib.setBANKID(BankID);
        attrib.setACCOUNT(ACCOUNT);
        attrib.setPAYSOURCE(PAYSOURCE);
        attrib.setPAYAMT(params.get("ipayamt").toString());
        attrib.setREMOTEIP(params.get("IP").toString());
        attrib.setADMCODE(MSGCODE);
        attrib.setLOGDATE(DateTimeUtils.format("yyyyMMdd", new Date()));
        attrib.setLOGTIME(DateTimeUtils.format("HHmmss", new Date()));
        log.debug("attrib {}", attrib.toString());
        payBillLogDao.save(attrib);

        return result;
    }
}
