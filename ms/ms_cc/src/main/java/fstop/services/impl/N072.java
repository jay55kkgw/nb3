package fstop.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.rest.comm.bean.DoIDGateCommand_In;
import com.netbank.rest.comm.controller.CommonIDGATEController;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import encryptUtil.EncryptUtil;
import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.SysDailySeqDao;
import fstop.orm.dao.TxnReqInfoDao;
import fstop.orm.dao.TxnTwRecordDao;
import fstop.orm.dao.TxnTwSchPayDao;
import fstop.orm.dao.TxnTwSchPayDataDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.ADMMAILLOG;
import fstop.orm.po.TXNTWRECORD;
import fstop.orm.po.TXNTWSCHPAY;
import fstop.services.BookingAware;
import fstop.services.CommonService;
import fstop.services.NoticeInfo;
import fstop.services.NoticeInfoGetter;
import fstop.services.OnLineTransferNotice;
import fstop.services.TelCommExecWrapper;
import fstop.services.TransferNotice;
import fstop.services.WriteLogInterface;
import fstop.telcomm.TelCommExec;
import fstop.telcomm.cmd.CommandUtils;
import fstop.util.DateTimeUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import topibs.utility.NumericUtil;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class N072 extends CommonService implements BookingAware, WriteLogInterface {
	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("n072Telcomm")
	private TelCommExec n072Telcomm;

	@Autowired
	@Qualifier("n072Telcomm_m")
	private TelCommExec n072Telcomm_m;

	@Autowired
	private TxnTwSchPayDao txntwschpaydao;

	@Autowired
	private TxnTwSchPayDataDao txntwschpaydatadao;
	
	@Autowired
	private TxnTwRecordDao txnTwRecordDao;
	
	@Autowired
	private SysDailySeqDao sysDailySeqDao;

	@Autowired
	private TxnReqInfoDao txnReqInfoDao;
	
	@Autowired
	private TxnUserDao txnUserDao;
	
	@Autowired
	CommonIDGATEController commonIDGATEController;

	// @Autowired
	// private TxnReqInfoDao txnReqInfoDao;
	// @Autowired
	// private TxnUserDao txnUserDao;

	/**
	 * 
	 * @param params
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public MVH doAction(Map params) {

		String trin_acn = params.get("ACN").toString();
		if (trin_acn.length() > 11)
			trin_acn = trin_acn.substring(trin_acn.length() - 11);
		params.put("ACN", trin_acn);

		// 晶片金融卡
		if ("2".equals(params.get("FGTXWAY").toString())) {
			Date d = new Date();
			String trin_transeq = "1234";
			String trin_issuer = "05000000";
			String trin_acnno = params.get("ACNNO").toString();
			String trin_icdttm = DateTimeUtils.format("yyyyMMddHHmmss", d);
			log.debug("ICDTTM ----------> " + trin_icdttm);
			String trin_iseqno = params.get("iSeqNo").toString();
			String trin_icmemo = "";
			String trin_tac_length = "000A";
			String trin_tac = params.get("TAC").toString();
			log.debug(ESAPIUtil.vaildLog("TAC ----------> " + trin_tac_length + trin_tac + "("
					+ (trin_tac_length.length() + trin_tac.length()) + ")"));
			String trin_trmid = params.get("TRMID").toString();

			params.put("TRANSEQ", trin_transeq);
			params.put("ISSUER", trin_issuer);
			params.put("ACNNO", trin_acnno);
			params.put("ICDTTM", trin_icdttm);
			params.put("ICSEQ", trin_iseqno);
			params.put("ICMEMO", trin_icmemo);
			params.put("TAC_Length", trin_tac_length);
			params.put("TAC", trin_tac);
			params.put("TRMID", trin_trmid);
		}

		// 即時or預約
		String fgtxdate = (String) params.get("FGTXDATE");

		if ("1".equals(fgtxdate)) { // 即時

			// TODO 20190328 發送電子郵件
			OnLineTransferNotice trNotice = new OnLineTransferNotice();

			// TODO 要寫入TxnTwRecord
			return this.online(params, trNotice);
		} else if ("2".equals(fgtxdate)) { // 預約,"2" 特定日

			if ("0".equals(params.get("FGTXWAY"))) { // 使用交易密碼, 做預約

				// 目前SSL使用N951
				TelCommExec n951CMTelcomm;

				n951CMTelcomm = (TelCommExec) SpringBeanFactory.getBean("n951CMTelcomm");

				MVHImpl n951Result = n951CMTelcomm.query(params);
				// 若驗證失敗, 則會發動 TopMessageException
			} else if ("2".equals(params.get("FGTXWAY"))) { // 使用晶片金融卡, 做預約

				TelCommExec n954Telcomm = (TelCommExec) SpringBeanFactory.getBean("n954Telcomm");
				MVHImpl n954Result = n954Telcomm.query(params);
				// 若驗證失敗, 則會發動 TopMessageException
			}

			return this.booking(params);
		}

		return new MVHImpl();
	}

	public Map<String, String> scheduleObjectMapping() {
		Map<String, String> map = new HashMap();

		map.put("DPUSERID", "UID");
		map.put("DPWDAC", "ACN"); // 轉出帳號
		// map.put("DPSVBH", ""); // 轉入分行
		map.put("DPSVAC", "CARDNUM"); // 轉入帳號, 使用原始輸入的來記錄
		map.put("DPTXAMT", "AMOUNT");
		map.put("DPTXMEMO", "CMTRMEMO");
		map.put("DPTXMAILS", "CMTRMAIL");
		map.put("DPTXMAILMEMO", "CMMAILMEMO"); // CMMAILMEMO 對應的欄位

		map.put("ADOPID", "ADOPID");

		map.put("DPTXCODE", "FGTXWAY");
		/*
		 * 
		 * txntwschedule.setDPUSERID(params.get("UID"));
		 * txntwschedule.setDPWDAC(params.get("ACN")); //轉出帳號
		 * 
		 * 
		 * txntwschedule.setDPSVBH(""); //轉入分行
		 * txntwschedule.setDPSVAC(params.get("CARDNUM")); //轉入帳號
		 * txntwschedule.setDPTXAMT(params.get("AMOUNT"));
		 * txntwschedule.setDPTXMEMO(params.get("CMTRMEMO"));
		 * txntwschedule.setDPTXMAILS(params.get("CMTRMAIL"));
		 * txntwschedule.setDPTXMAILMEMO(params.get("CMMAILMEMO")); //CMMAILMEMO 對應的欄位
		 * 
		 * txntwschedule.setADOPID(params.get("TXID"));
		 */

		return map;
	}

	@Transactional
	public MVHImpl booking(Map<String, String> params) {
		TXNTWSCHPAY txntwschpay = new TXNTWSCHPAY();

		// 產生 CERTACN update by Blair -------------------------------------------- Start
		String TxWay = params.get("FGTXWAY");
		String certACN = "";

		if ("0".equals(TxWay)) { // SSL
			while (certACN.length() < 19)
				certACN += "2";
		} else if ("1".equals(TxWay)) { // I-KEY
			while (certACN.length() < 19)
				certACN += "3";
		} else if ("2".equals(TxWay)) { // 晶片
			while (certACN.length() < 19)
				certACN += "5";
		} else if ("3".equals(TxWay)) { // OTP
			while (certACN.length() < 19)
				certACN += "7";
		} else if ("7".equals(TxWay)) {  //IDGATE 預約
			while (certACN.length() < 19)
				certACN += "D";
		}
		params.put("CERTACN", certACN);
		// 產生 CERTACN update by Blair -------------------------------------------- End
		
		
		// 操作功能ID
		txntwschpay.setADOPID(params.get("ADOPID"));
		// 使用者ID
		txntwschpay.setDPUSERID(params.get("CUSIDN"));
		// 轉出帳號
		txntwschpay.setDPWDAC(params.get("ACN"));
		// 轉入行庫代碼
		txntwschpay.setDPSVBH(params.get("BNKRA"));
		// 轉入帳號/繳費代號
		txntwschpay.setDPSVAC(params.get("CARDNUM"));
		// 轉帳金額
		txntwschpay.setDPTXAMT(params.get("AMOUNT"));
		// 備註
		txntwschpay.setDPTXMEMO(params.get("CMTRMEMO"));
		// 發送Mail清單
		txntwschpay.setDPTXMAILS(params.get("CMTRMAIL"));
		// Mail備註
		txntwschpay.setDPTXMAILMEMO(params.get("CMMAILMEMO"));
		// 交易機制
		txntwschpay.setDPTXCODE(TxWay);
		// 預約編號
//		String uuid = UUID.randomUUID().toString();
		// 取得預約編號(YYMMDD+三位流水號)
		String dptmpschno = txnUserDao.getDptmpschno(params.get("UID"));
		txntwschpay.setDPSCHNO(dptmpschno);
		// 預約狀態
		txntwschpay.setDPTXSTATUS("0");
		

		// 預設NB
		txntwschpay.setLOGINTYPE("NB");
		
		
		// 寫入日期
		Date d = new Date();
		String DATE = DateTimeUtils.format("yyyyMMdd", d);
		String TIME = DateTimeUtils.format("HHmmss", d);
		txntwschpay.setDPSDATE(DATE);
		txntwschpay.setDPSTIME(TIME);
		txntwschpay.setLASTDATE(DATE);
		txntwschpay.setLASTTIME(TIME);	
		
		// 預約某一日
		String cmdate = StrUtils.trim(params.get("CMTRDATE")).replaceAll("/", "");
		if (cmdate.length() == 0) {
			throw TopMessageException.create("ZX99");
		} else {
			txntwschpay.setDPTXTYPE("S");
			txntwschpay.setDPPERMTDATE("");
			txntwschpay.setDPFDATE(cmdate);
			txntwschpay.setDPTDATE(cmdate);
		}

		// I-KEY VA驗證
		if (params.containsKey("pkcs7Sign") && "1".equals(params.get("FGTXWAY"))) {
			List<String> commands = new ArrayList();
			commands.add("XMLCA()");
			commands.add("XMLCN()");

			CommandUtils.doBefore(commands, params);

			if (!StrUtils.trim(params.get("XMLCN")).toUpperCase()
					.startsWith(StrUtils.trim(params.get("UID")).toUpperCase())) {
				throw TopMessageException.create("Z089");
			}

			txntwschpay.setXMLCA(StrUtils.trim(params.get("XMLCA")));
			txntwschpay.setXMLCN(StrUtils.trim(params.get("XMLCN")));
		}
		
		// IDGATE驗證
		if ("7".equals(params.get("FGTXWAY"))) {
			DoIDGateCommand_In in = new DoIDGateCommand_In();
			in.setIN_DATAS(params);
			HashMap rtnMap = commonIDGATEController.doIDGateCommand(in);
			if (!(boolean) rtnMap.get("result")) {
				log.error("N072 BOOKING CHECK IDGateCommand() ERROR");
				switch ((String) rtnMap.get("errMsg")) {
				case "Z300":
					throw TopMessageException.create("Z300");
				case "FE0011":
					throw TopMessageException.create("FE0011");
				default:
					throw TopMessageException.create("ZX99");
				}
			}

		}
		
		// 預約時上行電文必要資訊 
//		因IKEY資料太長 先移除避免塞爆欄位
		params.remove("pkcs7Sign");
		params.remove("__SIGN");
		String sha1_Mac = "";
		String reqinfo_Str = "{}";
		log.trace(ESAPIUtil.vaildLog("txntwschpay.params>>>{}"+ CodeUtil.toJson(params)));
		reqinfo_Str =  CodeUtil.toJson(params);
		txntwschpay.setDPTXINFO(reqinfo_Str);
		// MAC
		sha1_Mac = EncryptUtil.toEncS1(reqinfo_Str, null).substring(0,4);
		txntwschpay.setMAC(sha1_Mac);
		txntwschpay.setMSADDR(TxnTwSchPayDao.MS_CC);
		

		log.trace(ESAPIUtil.vaildLog("txntwschpay>>>{}"+ CodeUtil.toJson(txntwschpay)));
		txntwschpaydao.save(txntwschpay);
		
//		String reqinfo_addTXTime ="";
//		String sha1_MacTime ="" ;
		
//		//改寫預約副檔mac 防止批次因DB時間竄改執行錯誤日期
//        List<TXNTWSCHPAYDATA> dataList = txntwschpaydatadao.findByDPSCHNO(dptmpschno);
//        for(TXNTWSCHPAYDATA data:dataList) {
//        	params.put("DPSCHTXDATE", data.getPks().getDPSCHTXDATE());
//        	reqinfo_addTXTime =  CodeUtil.toJson(params);
//        	sha1_MacTime = EncryptUtil.toEncS1(reqinfo_addTXTime, null).substring(0,4);
//        	data.setMAC(sha1_MacTime);
//        	txntwschpaydatadao.update(data);
//        }

		MVHImpl result = new MVHImpl();

		try {
			result.getFlatValues().put("CMTXTIME", DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", d));
			result.getFlatValues().put("TOPMSG", "0000");
			result.getFlatValues().put("MSGCOD", "0000");
		} catch (Exception e) {
			logger.error(e);
		}

		return result;
	}

	/**
	 * 信用卡款
	 * 
	 * @param params
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public MVHImpl online(final Map<String, String> params, TransferNotice trNotice) {
		Date d = new Date();
		params.put("DATE", DateTimeUtils.getCDateShort(d));
		params.put("TIME", DateTimeUtils.getTimeShort(d));
		log.debug(ESAPIUtil.vaildLog("N072.java online method params >>>>{}"+CodeUtil.toJson(params)));
		String f = params.get("FGTXDATE");
		if ("1".equals(f)) { // 即時

			// 產生 CERTACN update by Blair -------------------------------------------- Start
			String TxWay = params.get("FGTXWAY");
			String certACN = "";

			if ("0".equals(TxWay)) {
				while (certACN.length() < 19)
					certACN += " ";
			} else if ("1".equals(TxWay)) {
				while (certACN.length() < 19)
					certACN += "1";
			} else if ("2".equals(TxWay)) {
				while (certACN.length() < 19)
					certACN += "4";
			} else if ("3".equals(TxWay)) {
				while (certACN.length() < 19)
					certACN += "6";
			} else if ("7".equals(TxWay)) {  //IDGATE 即時
				while (certACN.length() < 19)
					certACN += "C";
			}
			params.put("CERTACN", certACN);
			// 產生 CERTACN update by Blair -------------------------------------------- End
		}

		TelCommExec doexec = n072Telcomm;
		if (StrUtils.trim((String) params.get("LOGINTYPE")).equals("MB")) {
			doexec = n072Telcomm_m;
		}
		final TelCommExecWrapper execwrapper = new TelCommExecWrapper(doexec);

		MVHImpl result = execwrapper.query(params);

		final TXNTWRECORD record = (TXNTWRECORD) this.writeLog(execwrapper, params, result);

		final TelCommExecWrapper execwrapperFinal = execwrapper;
		trNotice.sendNotice(new NoticeInfoGetter() {

			public NoticeInfo getNoticeInfo() {
				NoticeInfo result = new NoticeInfo(params.get("UID"), params.get("DPMYEMAIL"), params.get("CMTRMAIL"));

				if (StrUtils.trim((String) params.get("LOGINTYPE")).equals("MB"))
					result.setTemplateName("CREDITCARD_MB");
				else
					result.setTemplateName("CREDITCARD");
				
				//20190510 edit by hugo 表格異動 欄位已經移除
				//result.setDpretxstatus(record.getDPRETXSTATUS());
				result.setADTXNO(record.getADTXNO());
				result.setException(execwrapperFinal.getLastException());
				
				String datetime = record.getDPTXDATE() + record.getDPTXTIME();
				Date d = DateTimeUtils.parse("yyyyMMddHHmmss", datetime);
				String trantime = DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", d);
				result.getParams().put("#TXNAME", "繳納本行信用卡款");
				result.getParams().put("#TRANTIME", trantime);
				result.getParams().put("#SEQNO", record.getDPTXNO());

				String acn = record.getDPSVAC();
				try {

					acn = StrUtils.left(acn, 6) + StrUtils.repeat("*", acn.length() - 9) + StrUtils.right(acn, 3);
					// acn=StrUtils.hideid(acn);
					logger.debug("acn = " + acn);
				} catch (Exception e) {
					log.error("", e);
				}

				String outacn = record.getDPWDAC();
				try {
					// outacn = StrUtils.left(outacn, outacn.length() - 5) + "***" +
					StrUtils.right(outacn, 2);
					outacn = StrUtils.hideaccount(outacn);
					logger.debug("outacn = " + outacn);
				} catch (Exception e) {
				}

				// result.getParams().put("#ACN", acn);
				String str_Cusidn = (String) params.get("UID");
				str_Cusidn = StrUtils.left(str_Cusidn, str_Cusidn.length() - 7) + "****"
						+ StrUtils.right(str_Cusidn, 3);
				result.getParams().put("#CUSIDN", acn);
				result.getParams().put("#OUTACN", outacn);
				result.getParams().put("#OUTCURRENCY", "新台幣");
				String dptxamtfmt = NumericUtil.formatNumberString(record.getDPTXAMT(), 0);
				result.getParams().put("#AMT", dptxamtfmt);
				result.getParams().put("#MEMO", params.get("CMMAILMEMO"));

				ADMMAILLOG admmaillog = new ADMMAILLOG();
				admmaillog.setADACNO(record.getDPWDAC());

				String trans_status = (String) params.get("__TRANS_STATUS");
				if (StrUtils.isEmpty(trans_status)) {
					trans_status = "TWONLINE";
				} else if ("SEND".equals(trans_status)) {
					trans_status = "TWSCH";
				} else if ("RESEND".equals(trans_status)) {
					trans_status = "TWSCHRE";
				}

				admmaillog.setADBATCHNO(trans_status);
				admmaillog.setADMAILACNO(StrUtils.trim(params.get("CMTRMAIL")));
				admmaillog.setADUSERID(record.getDPUSERID());
				admmaillog.setADUSERNAME("");
				admmaillog.setADSENDTYPE("3");
				admmaillog.setADSENDTIME(datetime);
				admmaillog.setADSENDSTATUS("");

				result.setADMMAILLOG(admmaillog);

				return result;
			}

		});

		execwrapper.throwException();

		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));

		try {
			result.getFlatValues().put("CMTXTIME", DateTimeUtils.getCDateTime(result.getValueByFieldName("TRNDATE"),
					result.getValueByFieldName("TRNTIME")));
		} catch (Exception e) {
			logger.error(e);
		}

		if (result.getValueByFieldName("OUTACN").trim().length() == 4)
			throw TopMessageException.create(result.getValueByFieldName("OUTACN").trim());

		return result;
	}

	// TODO 更改TABLE欄位 要修
	public Object writeLog(TelCommExecWrapper execwrapper, Map<String, String> params, final MVHImpl result) {

		TXNTWRECORD record = execwrapper.createTxnTwRecord(sysDailySeqDao);

		String date = record.getLASTDATE();
		String time = record.getLASTTIME();

		if (result != null) {
			try {
				date = DateTimeUtils.format("yyyyMMdd",
						DateTimeUtils.getCDate2Date(result.getValueByFieldName("TRADATE")));
			} catch (Exception e) {
				log.error("", e);
			}
			try {
				time = result.getValueByFieldName("TRADAY");
			} catch (Exception e) {
				log.error("", e);
			}
		}else {
			log.warn("result is null...");
		}

		record.setDPUSERID(params.get("UID"));
		record.setADOPID(params.get("ADOPID"));
		record.setDPTXDATE(date);
		record.setDPTXTIME(time);
		record.setDPWDAC(params.get("ACN"));
		record.setDPSVBH("");
		record.setDPSVAC(params.get("CARDNUM"));
		record.setDPTXAMT(params.get("AMOUNT"));

		record.setDPTXMEMO(params.get("CMTRMEMO"));
		record.setDPTXMAILS(params.get("CMTRMAIL"));
		record.setDPTXMAILMEMO(params.get("CMMAILMEMO")); // CMMAILMEMO 對應的欄位

		record.setDPEFEE("0");
		record.setDPTXNO(""); // 跨行序號
		record.setDPTXCODE(params.get("FGTXWAY"));
		record.setLOGINTYPE(params.get("LOGINTYPE"));
		//移除pkcs7Sign 避免資料過長
		params.remove("pkcs7Sign");
		params.remove("__SIGN");
		record.setDPTITAINFO(CodeUtil.toJson(params));
		// record.setDPSCHNO(params.get("__SCHNO")==null?"" : params.get("__SCHNO"));

		// TODO 20190328 此table暫時不做 故註解 BEN
		// TXNREQINFO titainfo = null;
		// if(record.getDPTITAINFO() == 0) {
		// titainfo = new TXNREQINFO();
		// titainfo.setREQINFO(BookingUtils.requestInfo(params));
		// }
		//
		// txnTwRecordDao.writeTxnRecord(titainfo, record);
		txnTwRecordDao.save(record);
		return record;
	}
}
