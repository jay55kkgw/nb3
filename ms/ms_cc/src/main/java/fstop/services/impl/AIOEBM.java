package fstop.services.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.netbank.util.ResultCode;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.orm.dao.TxnUserDao;
import fstop.services.CommonService;
import fstop.ws.AIOWebServiceTemplate;
import fstop.ws.ebm.bean.GetBillInfoResponseData;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AIOEBM extends CommonService  {

	@Autowired
	private AIOWebServiceTemplate aioWebServiceTemplate;

	@Override
	public MVH doAction(Map params) {
		MVHImpl result = new MVHImpl();
		try {
			String type = (String)params.get("Type");
			if(type.equals("01")) {
				GetBillInfoResponseData resultQ = aioWebServiceTemplate.getBillInfo((String)params.get("cUkey"));
				
				result.getFlatValues().put("rtnCode",resultQ.getDATA().getRTN_MSG().getCRtnCode());
				result.getFlatValues().put("rtnDesc",resultQ.getDATA().getRTN_MSG().getCRtnDesc());
				result.getFlatValues().put("cEmail",resultQ.getDATA().getRTN_DATA().getCEmail());
				result.getFlatValues().put("cDate",resultQ.getDATA().getRTN_DATA().getCSDate());
			}else if(type.equals("02")) {
				GetBillInfoResponseData resultQ = aioWebServiceTemplate.SetBillSend((String)params.get("cUkey"),(String)params.get("cDate"));

				result.getFlatValues().put("rtnCode",resultQ.getDATA().getRTN_MSG().getCRtnCode());
				result.getFlatValues().put("rtnDesc",resultQ.getDATA().getRTN_MSG().getCRtnDesc());
			}
			result.getFlatValues().put("MSGCOD","0");
		}catch(Exception e) {
			result.getFlatValues().put("MSGCOD","FE0003");
		}
		return result;
	}
}
