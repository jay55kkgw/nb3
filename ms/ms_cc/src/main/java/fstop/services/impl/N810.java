package fstop.services.impl;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class N810 extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("n810Telcomm")
	private TelCommExec n810Telcomm;
//
//	@Required
//	public void setN810Telcomm(TelCommExec telcomm) {
//		n810Telcomm = telcomm;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public MVH doAction(Map params) {

		//查詢
		Date d = new Date();
		params.put("DATE", DateTimeUtils.getCDateShort(d));
		params.put("TIME", DateTimeUtils.getTimeShort(d));
		
		String str_StartTime = "182900";
		String str_EndTime = "183159";
		String str_CurrentTime = DateTimeUtils.getTimeShort(d);
		TelcommResult telcommResult = null ;
		if (Integer.parseInt(str_CurrentTime) >= Integer.parseInt(str_StartTime) && Integer.parseInt(str_CurrentTime)<= Integer.parseInt(str_EndTime)) {
				throw TopMessageException.create("ERDB", (String)params.get("ADOPID"));
		}
		else {
			telcommResult = n810Telcomm.query(params);
			telcommResult.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
			telcommResult.getFlatValues().put("CMRECNUM", telcommResult.getValueOccurs("CARDNUM") + "");
		}
		return telcommResult;
	}


}
