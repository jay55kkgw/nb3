package fstop.services.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.rest.comm.batch.CheckSystemStatus;

import fstop.model.IMasterValuesHelper;
import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.orm.dao.AdmBankDao;
import fstop.orm.po.ADMBANK;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CallMobileCenter extends CommonService {
	
	@Autowired
	CheckSystemStatus check;
	
	@Override
	public MVH doAction(Map params) {		
		TelcommResult helper = check.ConnectionCheck(params);
		return helper;
	}
}
