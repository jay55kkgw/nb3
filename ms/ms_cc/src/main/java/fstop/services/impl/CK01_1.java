package fstop.services.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import fstop.orm.dao.RevolvingCreditApplyDao;
import fstop.orm.po.REVOLVINGCREDITAPPLY;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class CK01_1 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	private RevolvingCreditApplyDao revolvingCreditApplyDao;
	
//	@Required
//	public void setRevolvingCreditApplyDao(
//			RevolvingCreditApplyDao revolvingCreditApplyDao) {
//		this.revolvingCreditApplyDao = revolvingCreditApplyDao;
//	}

	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 
		
		log.debug("CK01_1 - doAction() Begin");
		
		MVHImpl helper = new MVHImpl();
		
		// 驗證交易密碼
		log.debug("Start check password");
		if("0".equals(params.get("FGTXWAY"))) {  //使用交易密碼
			TelCommExec n950CMTelcomm;
			
					n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n951CMTelcomm");
			log.debug("Execute check password...");
			MVHImpl n950Result = n950CMTelcomm.query(params);
			//若驗證失敗, 則會發動 TopMessageException
			if(!"0000".equals(n950Result.getFlatValues().get("TOPMSG"))&&!"".equals(n950Result.getFlatValues().get("TOPMSG"))) {
				throw TopMessageException.create(n950Result.getFlatValues().get("TOPMSG"));
			}
		}
		log.debug("End check password (Succeed)");
		
		//---交易密碼驗證完畢，開始進行申請長循建檔作業---
		
		//申請長循表單資料寫入資料表 TxnCreditLog
		Date txnDatetime = Calendar.getInstance().getTime();
		REVOLVINGCREDITAPPLY applyForm = new REVOLVINGCREDITAPPLY();
		applyForm.setCUSIDN((String) params.get("UID")); //防資料竄改使用Session中的UID
		applyForm.setCUSNAME((String) params.get("CUSNAME"));
		applyForm.setHPHONE((String) params.get("HPHONE"));
		applyForm.setOPHONE((String) params.get("OPHONE"));
		applyForm.setMPFONE((String) params.get("MPFONE"));
		applyForm.setCARDNUM((String) params.get("CARDNUM"));
		applyForm.setCURRBAL((String) params.get("CURRBAL"));
		applyForm.setPOT((String) params.get("POT"));
		applyForm.setRATE((String) params.get("RATE"));
		applyForm.setCRLIMIT((String) params.get("CRLIMIT"));
		applyForm.setAMOUNT((String) params.get("AMOUNT"));
		applyForm.setPERIOD((String) params.get("PERIOD"));
		applyForm.setAPPLY_RATE((String) params.get("APPLY_RATE"));
		applyForm.setFIRST_AMOUNT((String) params.get("FIRST_AMOUNT"));
		applyForm.setFIRST_INTEREST((String) params.get("FIRST_INTEREST"));
		applyForm.setPERIOD_AMOUNT((String) params.get("PERIOD_AMOUNT"));
		applyForm.setAPPDATE(DateTimeUtils.getDateShort(txnDatetime));
		applyForm.setAPPTIME(DateTimeUtils.getTimeShort(txnDatetime));
		
		//----20170603修改，新增聯徵軌跡-------------------------------
		applyForm.setUSER_IP((String) params.get("USERIP"));	//使用者IP，使用session中的USERIP
		applyForm.setCHK_RESULT("1");	//身分驗證結果，1為"網銀登錄"
		applyForm.setMATTER_VER("10412");	//約定事項版本
		//-----------------------------------------------
		
		
		revolvingCreditApplyDao.save(applyForm); 
		
		helper.getFlatValues().put("CUSTID", (String) params.get("UID"));
		helper.getFlatValues().putAll(params);
		
		log.debug("CK01_1 - doAction() End");
		
		return helper;
	}
}
