package com.netbank.rest.service;

import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netbank.rest.model.CommonPools;
import com.netbank.util.AcnoTypeEnum;
import com.netbank.util.ESAPIUtil;
import com.netbank.util.StrUtils;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.notifier.NotifyAngent;
import fstop.services.CommonService;
import fstop.util.DateTimeUtils;
import fstop.util.SpringBeanFactory;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class N206_Tel_Service extends Common_Tel_Service {

	@Autowired
	private CommonPools commonPools;

	@Override
	public HashMap pre_Processing(Map request) {
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) {
		String cid = String.valueOf(request.get("CUSIDN"));
		String acn = String.valueOf(request.get("ACN"));
		
		log.debug("N206 CUSIDN = " + cid + ", ACN = " + acn);
		
		MVHImpl result = new MVHImpl();
		
		String occurMsg = "0000";

		String currentDate = DateTimeUtils.getDate();
		
		commonPools.n206.removeGetDataCache(cid);
		result = (MVHImpl)commonPools.n206.doAction(cid, acn) ;
		
		String N206MSG = result.getValueByFieldName("MSGCOD");
		
		log.debug("N206.java N206 MSGCOD:"+N206MSG);
		
		if (N206MSG.equals("0000")) {
			commonPools.n960.removeGetDataCache(cid);
			MVH n960Result = commonPools.n960.getData(cid);
			
			// 發送Email通知_線上開戶
			String mail = n960Result.getValueByFieldName("MAILADDR").trim();
			
			log.debug("send mail:" + mail );	
			Hashtable<String, String> varmail = new Hashtable();
			varmail.put("SUB", "臺灣企銀數位存款帳戶升級成功通知");
			varmail.put("MAILTEXT", getContent(acn, currentDate));
			if (!NotifyAngent.sendNotice("N206", varmail, mail)) {
				log.warn("線上開戶通知發送Email失敗.(mail:" + mail + ")");
			}
			log.debug("N206 mail :" +  varmail );	
		}

		
		return data_Retouch(result);
	}

	@Override
	public HashMap data_Retouch(MVH mvh) {
		// TODO Auto-generated method stub
		return super.data_Retouch(mvh);
	}

	@Override
	public void setMsgCode(Map dataMap, Boolean data_Retouch_result) {
		super.setMsgCode(dataMap, data_Retouch_result);
	}
	
	@SuppressWarnings("unused")
	private String getContent(String ACN, String uDate){

		String mkACN = "";
    	if(ACN.length()==11)
		{	
    		mkACN = ACN.substring(0,6)+"***"+ACN.substring(9);
    	}

		StringBuilder sb = new StringBuilder ();
		sb.append("<TABLE align=center cellSpacing=0 cellPadding=5 frame=border width='650'><TR><TD width='100%'>");
		sb.append("親愛的客戶您好: <br><br>");
		sb.append("您於 ").append(uDate).append(" 申請數位存款帳戶(帳號：").append(mkACN).append(")第三類升級為第一類，業經本<br>");
		sb.append("行審核通過並啟用，特此通知。<br><br>");
		
		sb.append("提醒您!升級後之數位存款帳戶晶片金融卡額度與一般帳戶之晶片金融卡額度相同； <br>");
		sb.append("另同一客戶之「數位存款帳戶」合計非約定轉帳金額，每筆限新臺幣5萬元整，每日<br>");
		sb.append("限新臺幣10萬元整，每月限新臺幣20萬元整。<br><br>");

		sb.append("若有任何問題，您可至本行官網智能客服查詢或撥電話至您所指定服務分行(聯絡電<br>");
		sb.append("話:(02 )25597171 )、本行客服專線(0800-01-7171)詢問，<br>");
		sb.append("我們將竭誠為您服務。<br>");
		sb.append("祝您事業順心，萬事如意! <br><br>");
		
		sb.append("</TD></TR></TABLE>");
		return sb.toString();
	}

}
