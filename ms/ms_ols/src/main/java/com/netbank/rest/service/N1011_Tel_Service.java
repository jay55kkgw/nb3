package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.model.MVH;
import fstop.orm.po.ADMMSGCODE;
import fstop.services.TopMsgDBDetecter;
import lombok.extern.slf4j.Slf4j;

/**
 * 主要用來
 * 1.丟入MVH前的前置處理，包含檢核;
 * 2.後製MVH回覆的訊息;
 * @author JeffChang
 *
 */
@Service
@Slf4j
public class N1011_Tel_Service extends Common_Tel_Service  {

	@Autowired
	private TopMsgDBDetecter errDetect;
	
	@Override
	public HashMap pre_Processing(Map request) {
		// TODO Auto-generated method stub
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) {
		// TODO Auto-generated method stub
		return super.process(serviceID, request);
	}

	@Override
	public HashMap data_Retouch(MVH mvh) {
		// TODO Auto-generated method stub
		return super.data_Retouch(mvh);
	}

	@Override
	public void setMsgCode(Map dataMap, Boolean data_Retouch_result) {
		log.trace("N1011.data_Retouch.data_Retouch_result : {}", data_Retouch_result);
		super.setMsgCode(dataMap, data_Retouch_result);
	}
	
}
