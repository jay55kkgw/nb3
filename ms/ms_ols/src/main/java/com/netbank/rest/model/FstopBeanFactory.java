package com.netbank.rest.model;

import java.lang.reflect.Method;

import org.springframework.beans.factory.NoSuchBeanDefinitionException;

import fstop.exception.UncheckedException;
import fstop.services.ServiceBindingException;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
 
public class FstopBeanFactory {
 
//	private static Logger getLogger() {
//		return Logger.getLogger(FstopBeanFactory.class);
//	}
	
	public static Object getBean(String beanName) {
		return SpringBeanFactory.getBean(beanName);
	}
	
	public static Object beanMethodInvoke(String beanName,  String methodName, Class[] parameters, Object[] arguments)
			throws Exception { 
		
		Object result = null;
		try {
			String bo = StrUtils.trim(beanName);

			Object bean = null;
			try {
				bean = SpringBeanFactory.getBean(bo);
			}
			catch(NoSuchBeanDefinitionException e) {
				throw new ServiceBindingException("找不到對應的 BO Service. [" + bo + "]", e);
			}

			Method myMethod = null;
			try {
				myMethod = bean.getClass().getMethod(methodName, parameters);
			}
			catch(Exception e) {
				throw new ServiceBindingException("BO Service 無對應的 Method (" + methodName + "). [" + bo + "]", e);
			}
			if(myMethod == null)
				throw new ServiceBindingException("BO Service 無對應的 Method (" + methodName + "). [" + bo + "]");

			result = (Object)myMethod.invoke(bean, arguments);	
			return result;
		}
		catch(java.lang.reflect.InvocationTargetException e) {
			if(e.getTargetException() instanceof UncheckedException) {
				throw (UncheckedException)e.getTargetException();
			}
			else {
				throw new ServiceBindingException("呼叫 Service 時有誤 !! \r\nbeanMethodInvoke Error [beanName: " + beanName + ", methodName: " + methodName + ", arguments: " + arguments + "]!!", e.getTargetException());
			}
		}
	}
	
}
