package com.netbank.rest.model.pool;

import java.util.Date;
import java.util.Hashtable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.hitrust.isecurity2_0.client.util.Log;
import com.netbank.rest.model.CommonPools;
import com.netbank.rest.model.NewOneCallback;
import com.netbank.rest.model.UserPool;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class N961Pool extends UserPool {

	@Autowired
    @Qualifier("n961Telcomm")
	private TelCommExec n961Telcomm;
	
	private NewOneCallback getNewOneCallback(final String uid) {
		NewOneCallback callback = new NewOneCallback() {

			public Object getNewOne() {
				log.debug("CUSIDN = " + uid);
				Hashtable params = new Hashtable();
				params.put("CUSIDN", uid);
				Date d = new Date();
				String df = DateTimeUtils.getCDateShort(d);
				String tf = DateTimeUtils.format("HHmmss", d);
				params.put("DATE", df);
				params.put("TIME", tf);
						
				TelcommResult mvh = n961Telcomm.query(params);
				
				return mvh;
			}
			
		};		
		
		return callback;
	}
	
	/**
	 * N961客戶數位帳號查詢
	 * @param idn  使用者的身份證號
	 * @return
	 */
	public MVH getData(final String idn) {
		final String uid = StrUtils.trim(idn).toUpperCase();
		if(uid.length() == 0)
			return new MVHImpl();
		
		return (MVH)getPooledObject(uid, "N961.getData", getNewOneCallback(uid));

	}
	
	public void removeGetDataCache(final String idn) {
		this.removePoolObject(idn, "N961.getData");
	}
	
}
