package com.netbank.rest.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import fstop.services.ServerConfig;
import fstop.services.TopMsgDBDetecter;
import fstop.services.TopMsgF003Detecter;
import fstop.services.TopMsgF007Detecter;
import fstop.services.TopMsgFXDetecter;
import fstop.services.TopMsgFundDetecter;
import fstop.services.TopMsgN320Detecter;
import fstop.services.TopMsgN950Detecter;
import fstop.services.batch.CheckServerHealth;
import fstop.services.impl.C900;
import fstop.services.impl.CallMobileCenter;
import fstop.services.impl.N100;
import fstop.services.impl.N1010_CP;
import fstop.services.impl.N1010_RP;
import fstop.services.impl.N1011;
import fstop.services.impl.N1012;
import fstop.services.impl.N1014;
import fstop.services.impl.N110;
import fstop.services.impl.N130;
import fstop.services.impl.N201;
import fstop.services.impl.N215A;
import fstop.services.impl.N215A_1;
import fstop.services.impl.N215D;
import fstop.services.impl.N215D_1;
import fstop.services.impl.N215FA;
import fstop.services.impl.N215FA_1;
import fstop.services.impl.N215FD;
import fstop.services.impl.N215FD_1;
import fstop.services.impl.N215FM;
import fstop.services.impl.N215FM_1;
import fstop.services.impl.N215M;
import fstop.services.impl.N215M_1;
import fstop.services.impl.N215Q;
import fstop.services.impl.N360;
import fstop.services.impl.N361;
import fstop.services.impl.N361_1;
import fstop.services.impl.N366;
import fstop.services.impl.N366_1;
import fstop.services.impl.N570;
import fstop.services.impl.N880;
import fstop.services.impl.N900;
import fstop.services.impl.N913;
import fstop.services.impl.N920;
import fstop.services.impl.NA30;
import fstop.services.impl.NA30_1;
import fstop.services.impl.NA30_AUTH;
import fstop.services.impl.NA60;
import fstop.services.impl.NA70;
import fstop.services.impl.NA71;
import fstop.services.impl.NA80;
import fstop.services.mgn.B105;
import fstop.services.mgn.B106;
import fstop.services.mgn.NA20;
import fstop.va.VATransform;
import fstop.ws.eai.client.N701WebServiceClient;
import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class ServicesConfig {

    @Bean
    protected VATransform vatransform() throws Exception {
        return new VATransform();
    }
    @Bean
    protected CheckServerHealth checkserverhealth() throws Exception {
    	return new CheckServerHealth();
    }
    @Bean
    protected N701WebServiceClient n701WebServiceClient() throws Exception {
        return new N701WebServiceClient();
    }
	
    @Bean
    protected TopMsgDBDetecter dbErrDetect() throws Exception {
        return new TopMsgDBDetecter();
    }

    @Bean
    protected TopMsgN320Detecter n320ErrDetect() throws Exception {
        return new TopMsgN320Detecter();
    }

    @Bean
    protected TopMsgFXDetecter fxErrDetect() throws Exception {
        return new TopMsgFXDetecter();
    }

    @Bean
    protected TopMsgF003Detecter f003ErrDetect() throws Exception {
        return new TopMsgF003Detecter();
    }

    @Bean
    protected TopMsgF007Detecter f007ErrDetect() throws Exception {
        return new TopMsgF007Detecter();
    }

    @Bean
    protected TopMsgFundDetecter fundErrDetect() throws Exception {
        return new TopMsgFundDetecter();
    }
    @Bean
    protected TopMsgN950Detecter n950ErrDetect() throws Exception {
        return new TopMsgN950Detecter();
    }
    
    @Bean
    protected B105 b105Service() throws Exception {
        return new B105();
    }
    
    @Bean
    protected B106 b106Service() throws Exception {
        return new B106();
    }
    @Bean
    protected CallMobileCenter callmobilecenterService() throws Exception {
    	return new CallMobileCenter();
    }
    @Bean
    protected C900 c900Service() throws Exception {
        return new C900();
    }
    
    
    @Bean
    protected N100 n100Service() throws Exception {
        return new N100();
    }
    
    @Bean
    protected N1011 n1011Service() throws Exception {
        return new N1011();
    }
    
    @Bean
    protected N1010_RP n1010_rpService() throws Exception {
        return new N1010_RP();
    }
    
    @Bean
    protected N1010_CP n1010_cpService() throws Exception {
        return new N1010_CP();
    }
    
    @Bean
    protected N1012 n1012Service() throws Exception {
        return new N1012();
    }
    
    @Bean
    protected N1014 n1014Service() throws Exception {
        return new N1014();
    }
    
  
    @Bean
    protected N110 n110Service() throws Exception {
        return new N110();
    }


    @Bean
    protected N130 n130Service() throws Exception {
        return new N130();
    }

    
    @Bean
    protected N201 n201Service() throws Exception {
        return new N201();
    }    

    @Bean
    protected N215A n215aService() throws Exception {
        return new N215A();
    }
    
    @Bean
    protected N215A_1 n215a_1Service() throws Exception {
        return new N215A_1();
    }
    
    @Bean
    protected N215D n215dService() throws Exception {
        return new N215D();
    }
    
    @Bean
    protected N215D_1 n215d_1Service() throws Exception {
        return new N215D_1();
    }
    
    @Bean
    protected N215FA n215faService() throws Exception {
        return new N215FA();
    }
    
    @Bean
    protected N215FA_1 n215fa_1Service() throws Exception {
        return new N215FA_1();
    }
    
    @Bean
    protected N215FD n215fdService() throws Exception {
        return new N215FD();
    }
    
    @Bean
    protected N215FD_1 n215fd_1Service() throws Exception {
        return new N215FD_1();
    }
    
    @Bean
    protected N215FM n215fmService() throws Exception {
        return new N215FM();
    }
    
    @Bean
    protected N215FM_1 n215fm_1Service() throws Exception {
        return new N215FM_1();
    }
    
    @Bean
    protected N215M n215mService() throws Exception {
        return new N215M();
    }
    
    @Bean
    protected N215M_1 n215m_1Service() throws Exception {
        return new N215M_1();
    }
    
    @Bean
    protected N215Q n215qService() throws Exception {
        return new N215Q();
    }
    
    
    @Bean
    protected N360 n360Service() throws Exception {
        return new N360();
    }
    
    @Bean
    protected N361 n361Service() throws Exception {
        return new N361();
    }
    
    @Bean
    protected N361_1 n361_1Service() throws Exception {
        return new N361_1();
    }
    
    @Bean
    protected N366 n366Service() throws Exception {
        return new N366();
    }
    
    @Bean
    protected N366_1 n366_1Service() throws Exception {
        return new N366_1();
    }
    
    @Bean
    protected N570 n570Service() throws Exception {
        return new N570();
    }
    
    @Bean
    protected N880 n880Service() throws Exception {
        return new N880();
    }
    
    @Bean
    protected N900 n900Service() throws Exception{
    	return new N900();
    }
    
    @Bean
    protected N913 n913Service() throws Exception{
    	return new N913();
    }
    
    @Bean
    protected N920 n920Service() throws Exception {
        return new N920();
    }
    
    @Bean
    protected NA20 na20Service() throws Exception {
        return new NA20();
    }
    
    @Bean
    protected NA30 na30Service() throws Exception {
        return new NA30();
    }
    
    @Bean
    protected NA30_1 na30_1Service() throws Exception {
        return new NA30_1();
    }
    
    @Bean
    protected NA30_AUTH na30_authService() throws Exception {
        return new NA30_AUTH();
    }
    
    @Bean
    protected NA60 na60Service() throws Exception {
        return new NA60();
    }
    
    @Bean
    protected NA70 na70Service() throws Exception {
        return new NA70();
    }
    
    @Bean
    protected NA71 na71Service() throws Exception {
        return new NA71();
    }
    
    @Bean
    protected NA80 na80Service() throws Exception {
        return new NA80();
    }
   
    //<!-- 後台OTP狀態 -->
    @Bean
    protected ServerConfig serverConfig() throws Exception {
        return new ServerConfig();
    }
  
}
