package com.netbank.rest.model.pool;

import java.util.Date;
import java.util.Hashtable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.rest.model.NewOneCallback;
import com.netbank.rest.model.UserPool;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;

@Component
public class N206Pool extends UserPool {

	@Autowired
    @Qualifier("n206Telcomm")
	private TelCommExec n206Telcomm;

	
	private NewOneCallback getNewOneCallback(final String uid, final String acn) {
		NewOneCallback callback = new NewOneCallback() {

			public Object getNewOne() {

				Hashtable params = new Hashtable();
				params.put("CUSIDN", uid);
				params.put("ACN", acn);
				Date d = new Date();
				String df = DateTimeUtils.getCDateShort(d);
				String tf = DateTimeUtils.format("HHmmss", d);
				params.put("DATE", df);
				params.put("TIME", tf);
				
				TelcommResult mvh = n206Telcomm.query(params);
				
				return mvh;
			}
			
		};		
		
		return callback;
	}
	
	/**
	 * N206數三轉數一帳號
	 * @param idn  使用者的身份證號
	 * @param acn 數三帳號
	 * @return
	 */
	public MVH doAction(final String idn, final String acn) {
		final String uid = StrUtils.trim(idn).toUpperCase();
		if(uid.length() == 0 || acn.length() == 0) {
			return new MVHImpl();			
		}
		
		return (MVH)getPooledObject(uid, "N206.getData", getNewOneCallback(uid, acn));		
	}
	
	public void removeGetDataCache(final String idn) {
		this.removePoolObject(idn, "N206.getData");
	}
	
}
