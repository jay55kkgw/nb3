package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import lombok.extern.slf4j.Slf4j;

/**
 * /**
 * 綜存定期性存款明細查詢
 *
 * @author Ian
 *
 */
@Service
@Slf4j
public class N100_Tel_Service extends Common_Tel_Service {
	@Override
	public HashMap pre_Processing(Map request) {
		return super.pre_Processing(request);
	}
	
	@Override
	public HashMap process(String serviceID ,Map request) {
		return super.process(serviceID, request);
	}
	
	@Override
	public HashMap data_Retouch(MVH mvh) {
		MVHImpl mvhimpl = null;
		mvhimpl = (MVHImpl) mvh;
		if(mvhimpl.getFlatValues() == null) {
			((MVHImpl)mvh).getFlatValues().put("TOPMSG", "");			
		}
		return super.data_Retouch(mvh);
	}
		
	@Override
	public void setMsgCode(Map dataMap, Boolean data_Retouch_result) {
		// TODO Auto-generated method stub
		super.setMsgCode(dataMap, data_Retouch_result);
	}


}
