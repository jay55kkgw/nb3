package fstop.services.impl;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Vector;

import org.bouncycastle.util.encoders.Hex;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.util.CodeUtil;

import bank.comm.FSTopResultSet;
import bank.comm.ResultSetImpl;
import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.model.ValueContainer;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.ws.EAIWebServiceTemplate;
import fstop.ws.eai.bean.C900RQ;
import fstop.ws.eai.bean.C900RS;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class C900 extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	private EAIWebServiceTemplate eaiwebservicetemplate;
	
	@Autowired
    @Qualifier("c900Telcomm")
	private TelCommExec c900Telcomm;
	
//	@Required
//	public void setC900Telcomm(TelCommExec c900Telcomm) {
//		this.c900Telcomm = c900Telcomm;
//	}
	
	@Override
	public MVH doAction(Map params) {
		int totavertcnt=0;
		Map totavert=new HashMap();
		Vector tmpvector=new Vector();
		Vector tmptotavect=new Vector();
		MVHImpl resultconv=null;
		MVHImpl resulTelcomm=null;
		TelcommResult result = null;
		log.debug("C900 doAction");
		FSTopResultSet fstopresult=null;
		
		C900RQ rq = null;
		try
		{
			rq = CodeUtil.objectCovert(C900RQ.class, params);
			C900RS rs = eaiwebservicetemplate.sendAndReceive(rq, C900RS.class);
			log.trace("EAI GET RS>>>>>{}",rs.toString());
			String tota = rs.getTOTA();
			
//			tota = tota.substring(12);
			tota = tota.substring(24);
			log.trace("tota substring>>>{}",tota);
			byte[] totabyte = null;
			try {
				//EAI改成傳HEX值過來，不是明碼
//				tota = org.apache.commons.codec.binary.Hex.encodeHexString(tota.getBytes("UTF-8"),true);
//				log.trace("tota HEXstring>>>{}",tota);
				totabyte = org.apache.commons.codec.binary.Hex.decodeHex(tota);
			} catch (Exception e) {
				log.error("DecoderException>>{}",e);
			}
	//		byte[] totabyte = tota.getBytes("");
			log.trace("totabyte >>>{}",totabyte);
			assamble(totabyte,totavert);
			
			log.trace("GET totavert>>>>>{}",totavert.toString());
			resulTelcomm = new MVHImpl();
			
			Object data = totavert.get("totahash");
			Map dataMap=CodeUtil.objectCovert(Map.class, data);
			for(Object eachData:dataMap.keySet()) {
				resulTelcomm.getFlatValues().put((String)eachData,(String)dataMap.get(eachData));
			}
			
//			resulTelcomm = c900Telcomm.query(params);
		}
		catch(TopMessageException err)
		{
			MVHImpl helper = new MVHImpl();
			log.debug("C900 doAction TopMessageException = Msgcode = " + err.getMsgcode());
			log.debug("C900 doAction TopMessageException = Msgin = " + err.getMsgin());
			log.debug("C900 doAction TopMessageException = getMsgout = " + err.getMsgout());
			helper.getFlatValues().put("TOPMSG", err.getMsgcode());
			helper.getFlatValues().put("MSGIN", err.getMsgin());
			helper.getFlatValues().put("MSGOUT", err.getMsgout());
			return helper;
		}

		log.debug("REQTYPE = " + resulTelcomm.getFlatValues().get("REQTYPE"));
		log.debug("C900 doAction exit");
		return resulTelcomm;
	}
	
	
	public static void assamble(byte[] totadata,Map totavert) {
		Vector[] elementVector = new Vector[31]; // msgField.size()
		ResultSetImpl fstopresult=new ResultSetImpl();
//		byte[] totabyte=new byte[totadata.length];
		MVHImpl result=null;
		Hashtable totahash=new Hashtable();
//		totabyte=totadata;
		int bytecnt=0;
		int len = 0;
/*
		byte[] bytetemp=new byte[10];
		System.arraycopy(totadata,bytecnt,bytetemp,0,10);
		totahash.put("CTS", new String(ConvCode.host2pc(bytetemp)));
		logger.debug("CTS = " + new String(ConvCode.host2pc(bytetemp)));
		bytecnt+=10;
*/
		try {
			// Message Type id 4
			len = 4;
			byte[] bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("MSGTYPE", new String(bytetemp,"UTF-8"));
			totahash.put("MSGTYPE", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("MSGTYPE = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			// Primary bit map 16
			len = 16;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("Primarybitmap", new String(bytetemp,"UTF-8"));
			totahash.put("Primarybitmap", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("Primarybitmap = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			// Data_length 2
			int varlen;
			len = 2;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("ACNDATALEN", new String(bytetemp,"UTF-8"));
			totahash.put("ACNDATALEN", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("ACNDATALEN = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
//			varlen=Integer.parseInt(new String(bytetemp,"UTF-8"));
			varlen=Integer.parseInt(new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//Primary account number 16
			len = 16;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("PRIACN", new String(bytetemp,"UTF-8"));
			totahash.put("PRIACN", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("PRIACN = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			// System trace audit number 6
			len = 6;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("STAN", new String(bytetemp,"UTF-8"));
			totahash.put("STAN", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("STAN = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			// Date and time, local transaction 12
			len = 6;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("DATE", new String(bytetemp,"UTF-8"));
			totahash.put("DATE", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("DATE = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			len = 6;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("TIME", new String(bytetemp,"UTF-8"));
			totahash.put("TIME", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("TIME = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			// Date, capture 4
			len = 4;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("DATE4", new String(bytetemp,"UTF-8"));
			totahash.put("DATE4", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("DATE4 = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			// Function Code 305 3
			len = 3;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("FUNCODE", new String(bytetemp,"UTF-8"));
			totahash.put("FUNCODE", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("FUNCODE = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			// Action Code 3
			len = 3;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("ACTCODE", new String(bytetemp,"UTF-8"));
//			totahash.put("TOPMSG", new String(bytetemp,"UTF-8"));
			totahash.put("ACTCODE", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			totahash.put("TOPMSG", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("ACTCODE = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
//			if(!(new String(bytetemp,"UTF-8").equals("300")))
			if(!(new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8").equals("300")))
			{
				// Additional Response 2
				len = 2;
				bytetemp=new byte[len];
				System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//				totahash.put("ADDRESLEN", new String(bytetemp,"UTF-8"));  // var length
				totahash.put("ADDRESLEN", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));  // var length
				log.debug("ADDRESLEN = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
//				varlen=Integer.parseInt(new String(bytetemp,"UTF-8"));
				varlen=Integer.parseInt(new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
				bytecnt+=len;
				
				// Data Element 3
				len = 3;
				bytetemp=new byte[len];
				System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//				totahash.put("DATELE", new String(bytetemp,"UTF-8"));
				totahash.put("DATELE", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
				log.debug("DATELE = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
//			totahash.put("TOPMSG", new String(bytetemp,"UTF-8"));
				bytecnt+=len;
				
				// Error message 30
				len = 30;
				bytetemp=new byte[len];
				System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//				totahash.put("ERRMSG", new String(bytetemp,"UTF-8"));
				totahash.put("ERRMSG", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
				log.debug("ERRMSG = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
				bytecnt+=(varlen-3);
				
				log.trace("TOTAL BYTECNT>>>>>{}",bytecnt);
				totahash.put("TOPMSG","Z009");
				totavert.put("totahash",totahash);
				return;
			}else {
				totahash.put("TOPMSG", "");
			}
			
			//Input Request 3
			len = 3;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("INPREQLEN", new String(bytetemp,"UTF-8"));
			totahash.put("INPREQLEN", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("INPREQLEN = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
//			varlen=Integer.parseInt(new String(bytetemp,"UTF-8"));
			varlen=Integer.parseInt(new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			// Transaction code 4
			len = 4;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("TRACODE", new String(bytetemp,"UTF-8"));
			totahash.put("TRACODE", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("TRACODE = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			// Service code (delivery channel)
			len = 8;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("SERVCODE", new String(bytetemp,"UTF-8"));
			totahash.put("SERVCODE", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("SERVCODE = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			// Terminal ID 8
			len = 8;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("TERMID", new String(bytetemp,"UTF-8"));
			totahash.put("TERMID", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("TERMID = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			// Request Type 4
			len = 4;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("REQTYPE", new String(bytetemp,"UTF-8"));
			totahash.put("REQTYPE", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("REQTYPE = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			// Number of transactions 2
			len = 2;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("NUMTRA", new String(bytetemp,"UTF-8"));
			totahash.put("NUMTRA", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("NUMTRA = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			// Sequence 3
			len = 3;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("SEQ", new String(bytetemp,"UTF-8"));
			totahash.put("SEQ", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("SEQ = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			// Name 30
			len = 30;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("NAME", new String(bytetemp,"UTF-8"));
			totahash.put("NAME", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("NAME = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			// Date of Birth 8
			len = 8;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("DATEBRI", new String(bytetemp,"UTF-8"));
			totahash.put("DATEBRI", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("DATEBRI = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			// ID number 15
			len = 15;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("IDNUM", new String(bytetemp,"UTF-8"));
			totahash.put("IDNUM", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("IDNUM = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			// Office phone 18
			len = 18;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("OFFPHONE", new String(bytetemp,"UTF-8"));
			totahash.put("OFFPHONE", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("OFFPHONE = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			// House phone 18
			len = 18;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("HOUPHONE", new String(bytetemp,"UTF-8"));
			totahash.put("HOUPHONE", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("HOUPHONE = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			// Mother maiden name 30
			len = 30;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("MOTMAINAME", new String(bytetemp,"UTF-8"));
			totahash.put("MOTMAINAME", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("MOTMAINAME = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			// Reason for reporting 1
			len = 1;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("REASON", new String(bytetemp,"UTF-8"));
			totahash.put("REASON", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("REASON = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			// New pin block 16
			len = 16;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("NEWPIN", new String(bytetemp,"UTF-8"));
			totahash.put("NEWPIN", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("NEWPIN = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			// Personal ID 10
			len = 10;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("PERID", new String(bytetemp,"UTF-8"));
			totahash.put("PERID", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("PERID = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			// Output Data length 3
			len = 3;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("OUTLEN", new String(bytetemp,"UTF-8")); // var length
			totahash.put("OUTLEN", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8")); // var length
			log.debug("OUTLEN = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			String temp = new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8");
			if(temp.equals("   "))
				temp="000";
			varlen=Integer.parseInt(temp);
			bytecnt+=len;
			
			// Customer ID 16
			len = 16;
			bytetemp=new byte[len];
//		System.arraycopy(totabyte,bytecnt,bytetemp,0,varlen-4-8-8-4-2-3-30-8-15-18-18-30-1-16-16);
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("CUSTID", new String(bytetemp,"UTF-8"));
			totahash.put("CUSTID", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("CUSTID = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			// Customer Name 42
//			len = 42;
			// Customer Name 30
			len = 30;
			bytetemp=new byte[len];
//		System.arraycopy(totabyte,bytecnt,bytetemp,0,varlen-4-8-8-4-2-3-30-8-15-18-18-30-1-16-16);
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("CUSTNM", new String(bytetemp,"UTF-8"));
			totahash.put("CUSTNM", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("CUSTNM = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			// BIRTH DATE 8
			len = 8;
			bytetemp=new byte[len];
//		System.arraycopy(totabyte,bytecnt,bytetemp,0,varlen-4-8-8-4-2-3-30-8-15-18-18-30-1-16-16);
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("BIRTHDAY", new String(bytetemp,"UTF-8"));
			totahash.put("BIRTHDAY", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("BIRTHDAY = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			// ENG Name 30
			len = 30;
			bytetemp=new byte[len];
//		System.arraycopy(totabyte,bytecnt,bytetemp,0,varlen-4-8-8-4-2-3-30-8-15-18-18-30-1-16-16);
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("ENDNM", new String(bytetemp,"UTF-8"));
			totahash.put("ENDNM", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("ENDNM = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			// CO-NAME 42
//			len = 42;
			// CO-NAME 30
			len = 30;
			bytetemp=new byte[len];
//		System.arraycopy(totabyte,bytecnt,bytetemp,0,varlen-4-8-8-4-2-3-30-8-15-18-18-30-1-16-16);
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("CONM", new String(bytetemp,"UTF-8"));
			totahash.put("CONM", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("CONM = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			// CO-Birth date 8
			len = 8;
			bytetemp=new byte[len];
//		System.arraycopy(totabyte,bytecnt,bytetemp,0,varlen-4-8-8-4-2-3-30-8-15-18-18-30-1-16-16);
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("COBIRTHDAY", new String(bytetemp,"UTF-8"));
			totahash.put("COBIRTHDAY", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("COBIRTHDAY = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			// ADDR 1 42
//			len = 42;
			// ADDR 1 30
			len = 30;
			bytetemp=new byte[len];
//		System.arraycopy(totabyte,bytecnt,bytetemp,0,varlen-4-8-8-4-2-3-30-8-15-18-18-30-1-16-16);
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("ADDR1", new String(bytetemp,"UTF-8"));
			totahash.put("ADDR1", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("ADDR1 = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			// ADDR 2 42
//			len = 42;
			// ADDR 2 30
			len = 30;
			bytetemp=new byte[len];
//		System.arraycopy(totabyte,bytecnt,bytetemp,0,varlen-4-8-8-4-2-3-30-8-15-18-18-30-1-16-16);
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("ADDR2", new String(bytetemp,"UTF-8"));
			totahash.put("ADDR2", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("ADDR2 = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			// CITY ADDR 1 42+42
//			len=84;
			// CITY ADDR 1 28+30
			len=58;
			bytetemp=new byte[len];
//		System.arraycopy(totabyte,bytecnt,bytetemp,0,varlen-4-8-8-4-2-3-30-8-15-18-18-30-1-16-16);
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("CITYADDR1", new String(bytetemp,"UTF-8"));
			totahash.put("CITYADDR1", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("CITYADDR1 = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			// CITY ADDR 2 28
//		bytetemp=new byte[28];
////		System.arraycopy(totabyte,bytecnt,bytetemp,0,varlen-4-8-8-4-2-3-30-8-15-18-18-30-1-16-16);
//		System.arraycopy(totadata,bytecnt,bytetemp,0,28);
////		totahash.put("PERID", new String(ConvCode.host2pc(bytetemp)));
//		totahash.put("CITYADDR2", new String(bytetemp,"UTF-8"));
//		log.debug("CITYADDR2 = " + new String(bytetemp,"UTF-8"));
//		bytecnt+=28;
			
			// OFFICE  ADDR1 42
//			len=42;
			// OFFICE  ADDR1 30
			len=30;
			bytetemp=new byte[len];
//		System.arraycopy(totabyte,bytecnt,bytetemp,0,varlen-4-8-8-4-2-3-30-8-15-18-18-30-1-16-16);
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("OFFICEADDR1", new String(bytetemp,"UTF-8"));
			totahash.put("OFFICEADDR1", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("OFFICEADDR1 = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			// OFFICE  ADDR2 42
//			len=42;
			// OFFICE  ADDR2 30
			len=30;
			bytetemp=new byte[len];
//		System.arraycopy(totabyte,bytecnt,bytetemp,0,varlen-4-8-8-4-2-3-30-8-15-18-18-30-1-16-16);
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("OFFICEADDR2", new String(bytetemp,"UTF-8"));
			totahash.put("OFFICEADDR2", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("OFFICEADDR2 = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			// Home Phone 17
			len = 17;
			bytetemp=new byte[len];
//		System.arraycopy(totabyte,bytecnt,bytetemp,0,varlen-4-8-8-4-2-3-30-8-15-18-18-30-1-16-16);
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("HOMEPHONE", new String(bytetemp,"UTF-8"));
			totahash.put("HOMEPHONE", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("HOMEPHONE = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			// Office Phone 18
			len = 18;
			bytetemp=new byte[len];
//		System.arraycopy(totabyte,bytecnt,bytetemp,0,varlen-4-8-8-4-2-3-30-8-15-18-18-30-1-16-16);
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("OFFICPHONE", new String(bytetemp,"UTF-8"));
			totahash.put("OFFICPHONE", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("OFFICPHONE = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			// Mobile Phone 26
			len = 26;
			bytetemp=new byte[len];
//		System.arraycopy(totabyte,bytecnt,bytetemp,0,varlen-4-8-8-4-2-3-30-8-15-18-18-30-1-16-16);
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("MOBILEPHONE", new String(bytetemp,"UTF-8"));
			totahash.put("MOBILEPHONE", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("MOBILEPHONE = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			// e-mail   26
			len = 26;
			bytetemp=new byte[len];
//		System.arraycopy(totabyte,bytecnt,bytetemp,0,varlen-4-8-8-4-2-3-30-8-15-18-18-30-1-16-16);
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("EMAIL", new String(bytetemp,"UTF-8"));
			totahash.put("EMAIL", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("EMAIL = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			// Corporate Name 33
//			len = 33;
			// Corporate Name 30
			len = 30;
			bytetemp=new byte[len];
//		System.arraycopy(totabyte,bytecnt,bytetemp,0,varlen-4-8-8-4-2-3-30-8-15-18-18-30-1-16-16);
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("CORPNM", new String(bytetemp,"UTF-8"));
			totahash.put("CORPNM", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("CORPNM = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//  Customer Position 1 2
			len = 2;
			bytetemp=new byte[len];
//		System.arraycopy(totabyte,bytecnt,bytetemp,0,varlen-4-8-8-4-2-3-30-8-15-18-18-30-1-16-16);
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("CUSTPOSIT1", new String(bytetemp,"UTF-8"));
			totahash.put("CUSTPOSIT1", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("CUSTPOSIT1 = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//  Customer Position 2 13
//			len = 13;
			//  Customer Position 2 12
			len = 12;
			bytetemp=new byte[len];
//		System.arraycopy(totabyte,bytecnt,bytetemp,0,varlen-4-8-8-4-2-3-30-8-15-18-18-30-1-16-16);
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("CUSTPOSIT2", new String(bytetemp,"UTF-8"));
			totahash.put("CUSTPOSIT2", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("CUSTPOSIT2 = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//增加
			//  Contact 1 Name 30
			len = 30;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("CONTACTNAME1", new String(bytetemp,"UTF-8"));
			totahash.put("CONTACTNAME1", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("CONTACTNAME1 = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//  Contact 1 Relation 18
			len = 18;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("CONTACTREL1", new String(bytetemp,"UTF-8"));
			totahash.put("CONTACTREL1", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("CONTACTREL1 = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//  Contact 1 Phone 18
			len = 18;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("CONTACTPHONE1", new String(bytetemp,"UTF-8"));
			totahash.put("CONTACTPHONE1", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("CONTACTPHONE1 = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//  Contact 2 Name 30
			len = 30;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("CONTACTNAME2", new String(bytetemp,"UTF-8"));
			totahash.put("CONTACTNAME2", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("CONTACTNAME2 = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//  Contact 2 Relation 18
			len = 18;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("CONTACTREL2", new String(bytetemp,"UTF-8"));
			totahash.put("CONTACTREL2", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("CONTACTREL2 = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//  Contact 2 Phone 18
			len = 18;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("CONTACTPHONE2", new String(bytetemp,"UTF-8"));
			totahash.put("CONTACTPHONE2", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("CONTACTPHONE2 = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//  Cardholder Account 16
			len = 16;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("CARDHOLDER", new String(bytetemp,"UTF-8"));
			totahash.put("CARDHOLDER", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("CARDHOLDER = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//  CARD Mail Type 16
			len = 16;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("CARDMAILTYPE", new String(bytetemp,"UTF-8"));
			totahash.put("CARDMAILTYPE", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("CARDMAILTYPE = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//  Cycle Date 2
			len = 2;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("CYCLEDATE", new String(bytetemp,"UTF-8"));
			totahash.put("CYCLEDATE", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("CYCLEDATE = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//  OFFICER Code 3
			len = 3;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("OFFICERCODE", new String(bytetemp,"UTF-8"));
			totahash.put("OFFICERCODE", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("OFFICERCODE = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//  Block Code 1
			len = 1;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("BLOCKCODE", new String(bytetemp,"UTF-8"));
			totahash.put("BLOCKCODE", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("BLOCKCODE = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//  Card Expire Date 4
			len = 4;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("CARDEXPIREDATE", new String(bytetemp,"UTF-8"));
			totahash.put("CARDEXPIREDATE", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("CARDEXPIREDATE = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//  Make CARD Date 8
			len = 8;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("MAKECARDDATE", new String(bytetemp,"UTF-8"));
			totahash.put("MAKECARDDATE", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("MAKECARDDATE = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//  Credit Limit 13
			len = 13;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("CREDITLIMIT", new String(bytetemp,"UTF-8"));
			totahash.put("CDLIMIT", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("CDLIMIT = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//  Census Tract 7
			len = 7;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("CENSUSTRACT", new String(bytetemp,"UTF-8"));
			totahash.put("CENSUSTRACT", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("CENSUSTRACT = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//  Origin Limit 13
			len = 13;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("ORIGINLIMIT", new String(bytetemp,"UTF-8"));
			totahash.put("ORIGINLIMIT", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("ORIGINLIMIT = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//  Origin Limit Date 8
			len = 8;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("ORIGINLIMITDT", new String(bytetemp,"UTF-8"));
			totahash.put("ORIGINLIMITDT", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("ORIGINLIMITDT = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//  Current Balance 13
			len = 13;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("CURBAL", new String(bytetemp,"UTF-8"));
			totahash.put("CURBAL", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("CURBAL = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//  Current Balance Change Date 8
			len = 8;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("CURBALCHDT", new String(bytetemp,"UTF-8"));
			totahash.put("CURBALCHDT", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("CURBALCHDT = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//  Payment Type 1
			len = 1;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("PAYTYPE", new String(bytetemp,"UTF-8"));
			totahash.put("PAYTYPE", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("PAYTYPE = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//  Customer Type 1
			len = 1;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("CUSTTYPE", new String(bytetemp,"UTF-8"));
			totahash.put("CUSTTYPE", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("CUSTTYPE = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//  Auto Pay Account 16
			len = 16;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("AUTOPAYACC", new String(bytetemp,"UTF-8"));
			totahash.put("AUTOPAYACC", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("AUTOPAYACC = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//  Auto Pay Limit Ind 2
			len = 2;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("AUTOPAYLIMIT", new String(bytetemp,"UTF-8"));
			totahash.put("AUTOPAYLIMIT", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("AUTOPAYLIMIT = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//  Branch 5
			len = 5;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("BRANCH", new String(bytetemp,"UTF-8"));
			totahash.put("BRANCH", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("BRANCH = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//  Base Rate 7
			len = 7;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("BASERATE", new String(bytetemp,"UTF-8"));
			totahash.put("BASERATE", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("BASERATE = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//  Base ADJ 7
			len = 8;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("BASEADJ", new String(bytetemp,"UTF-8"));
			totahash.put("BASEADJ", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("BASEADJ = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//  Collection Account 13
			len = 13;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("COLLACC", new String(bytetemp,"UTF-8"));
			totahash.put("COLLACC", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("COLLACC = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//  Collection Flag 1
			len = 1;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("COLLFLAG", new String(bytetemp,"UTF-8"));
			totahash.put("COLLFLAG", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("COLLFLAG = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//  01-06 PROFILE 6
			len = 6;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("_0106PROFILE", new String(bytetemp,"UTF-8"));
			totahash.put("_0106PROFILE", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("_0106PROFILE = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//  07-12 PROFILE 6
			len = 6;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("_0712PROFILE", new String(bytetemp,"UTF-8"));
			totahash.put("_0712PROFILE", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("_0712PROFILE = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//  POT Code 2
			len = 2;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("POTCODE", new String(bytetemp,"UTF-8"));
			totahash.put("POTCODE", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("POTCODE = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//  POT Code Date 4
			len = 4;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("POTCODEDT", new String(bytetemp,"UTF-8"));
			totahash.put("POTCODEDT", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("POTCODEDT = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//  Last Maint Date 8
			len = 8;
			bytetemp=new byte[8];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("LASTMAINTDT", new String(bytetemp,"UTF-8"));
			totahash.put("LASTMAINTDT", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("LASTMAINTDT = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//  Memo 1 9
			len = 9;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("MEMO1", new String(bytetemp,"UTF-8"));
			totahash.put("MEMO1", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("MEMO1 = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//  Memo 2 7
			len = 7;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("MEMO2", new String(bytetemp,"UTF-8"));
			totahash.put("MEMO2", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("MEMO2 = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//  Memo 3 10
			len = 10;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("MEMO3", new String(bytetemp,"UTF-8"));
			totahash.put("MEMO3", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("MEMO3 = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//  Memo 4 26
			len = 26;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("MEMO4", new String(bytetemp,"UTF-8"));
			totahash.put("MEMO4", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("MEMO4 = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//  Memo 5 55
			len = 55;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("MEMO5", new String(bytetemp,"UTF-8"));
			totahash.put("MEMO5", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("MEMO5 = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//  SMSA 4
			len = 4;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("SMSA", new String(bytetemp,"UTF-8"));
			totahash.put("SMSA", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("SMSA = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			bytecnt+=len;
			
			//  Output Data 2 3
			len = 3;
			bytetemp=new byte[len];
			System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//			totahash.put("OUTDATA2", new String(bytetemp,"UTF-8"));
			totahash.put("OUTDATA2", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("OUTDATA2 = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
//			int outlen2=Integer.parseInt(new String(bytetemp,"UTF-8"));
			int outlen2=Integer.parseInt(new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
			log.debug("outlen2 = " + outlen2);
			bytecnt+=len;
			if(outlen2 > 0) {
				//  CURR DUE 13
				len = 13;
				bytetemp=new byte[len];
				System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//				totahash.put("CURRDUE", new String(bytetemp,"UTF-8"));
				totahash.put("CURRDUE", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
				log.debug("CURRDUE = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
				bytecnt+=len;
				
				//  PAST DUE 13
				len = 13;
				bytetemp=new byte[len];
				System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//				totahash.put("PASTDUE", new String(bytetemp,"UTF-8"));
				totahash.put("PASTDUE", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
				log.debug("PASTDUE = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
				bytecnt+=len;
				
				//  30DAYS DELQ 13
				len = 13;
				bytetemp=new byte[len];
				System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//				totahash.put("_30DAYSDELQ", new String(bytetemp,"UTF-8"));
				totahash.put("_30DAYSDELQ", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
				log.debug("_30DAYSDELQ = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
				bytecnt+=len;
				
				//  60DAYS DELQ 13
				len = 13;
				bytetemp=new byte[len];
				System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//				totahash.put("_60DAYSDELQ", new String(bytetemp,"UTF-8"));
				totahash.put("_60DAYSDELQ", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
				log.debug("_60DAYSDELQ = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
				bytecnt+=len;
				
				//  90DAYS DELQ 13
				len = 13;
				bytetemp=new byte[len];
				System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//				totahash.put("_90DAYSDELQ", new String(bytetemp,"UTF-8"));
				totahash.put("_90DAYSDELQ", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
				log.debug("_90DAYSDELQ = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
				bytecnt+=len;
				
				//  120DAYS DELQ 13
				len = 13;
				bytetemp=new byte[len];
				System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//				totahash.put("_120DAYSDELQ", new String(bytetemp,"UTF-8"));
				totahash.put("_120DAYSDELQ", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
				log.debug("_120DAYSDELQ = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
				bytecnt+=len;
				
				//  150DAYS DELQ 13
				len = 13;
				bytetemp=new byte[len];
				System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//				totahash.put("_150DAYSDELQ", new String(bytetemp,"UTF-8"));
				totahash.put("_150DAYSDELQ", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
				log.debug("_150DAYSDELQ = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
				bytecnt+=len;
				
				//  180DAYS DELQ 13
				len = 13;
				bytetemp=new byte[len];
				System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//				totahash.put("_180DAYSDELQ", new String(bytetemp,"UTF-8"));
				totahash.put("_180DAYSDELQ", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
				log.debug("_180DAYSDELQ = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
				bytecnt+=len;
				
				//  210DAYS DELQ 13
				len = 13;
				bytetemp=new byte[len];
				System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//				totahash.put("_210DAYSDELQ", new String(bytetemp,"UTF-8"));
				totahash.put("_210DAYSDELQ", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
				log.debug("_210DAYSDELQ = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
				bytecnt+=len;
				
				//  WAIVE ANNUAL FEE 1
				len = 1;
				bytetemp=new byte[len];
				System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//				totahash.put("WAIVEANNULFEE", new String(bytetemp,"UTF-8"));
				totahash.put("WAIVEANNULFEE", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
				log.debug("WAIVEANNULFEE = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
				bytecnt+=len;
				
				//  CO_HOME_PHONE 18
				len = 18;
				bytetemp=new byte[len];
				System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//				totahash.put("COHOMEPHONE", new String(bytetemp,"UTF-8"));
				totahash.put("COHOMEPHONE", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
				log.debug("COHOMEPHONE = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
				bytecnt+=len;
				
				//  DTE-OPENED 8
				len = 8;
				bytetemp=new byte[len];
				System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//				totahash.put("DTEOPENED", new String(bytetemp,"UTF-8"));
				totahash.put("DTEOPENED", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
				log.debug("DTEOPENED = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
				bytecnt+=len;
				
				//  EU_CUSTOMER_CLASS 2
				len = 2;
				bytetemp=new byte[len];
				System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//				totahash.put("EUCUSTOMERCLASS", new String(bytetemp,"UTF-8"));
				totahash.put("EUCUSTOMERCLASS", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
				log.debug("EUCUSTOMERCLASS = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
				bytecnt+=len;
				
				//  EU-BANK-ACCT-IND 1
				len = 1;
				bytetemp=new byte[len];
				System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//				totahash.put("EUBANKACCTIND", new String(bytetemp,"UTF-8"));
				totahash.put("EUBANKACCTIND", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
				log.debug("EUBANKACCTIND = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
				bytecnt+=len;
				
				//  ZIP_CODE 9
				len = 9;
				bytetemp=new byte[len];
				System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//				totahash.put("ZIPCODE", new String(bytetemp,"UTF-8"));
				totahash.put("ZIPCODE", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
				log.debug("ZIPCODE = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
				bytecnt+=len;
				
				//  CSH_ADV_LIM 13
				len = 13;
				bytetemp=new byte[len];
				System.arraycopy(totadata,bytecnt,bytetemp,0,len);
//				totahash.put("CSHADVLIM", new String(bytetemp,"UTF-8"));
				totahash.put("CSHADVLIM", new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
				log.debug("CSHADVLIM = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
				bytecnt+=len;
			}
			
//		totahash.put("TOPMSG",totahash.get("DATELE"));
//		log.debug("TOPMSG = " + totahash.get("DATELE"));
			
			log.trace("TOTAL BYTECNT>>>>>{}",bytecnt);
			totavert.put("totahash",totahash);
		}
		catch(Exception e) {
			log.error("assamble error >> {}",e);
		}

//		fstopresult.setElement(totavert);
//		return (FSTopResultSet)fstopresult;
	}
	
	final ValueContainer<TelcommResult> helperValue = new ValueContainer<TelcommResult>(); 
	final ValueContainer<String> occursFieldName = new ValueContainer<String>(); 
	
	MVHImpl FSTopResultSetToMVHImpl(Vector fstopresult)
	{
		MVHImpl result=new MVHImpl();
		TelcommResult tresult = new TelcommResult(fstopresult, occursFieldName.getValue());
		helperValue.setValue(tresult);
		
		

		return helperValue.getValue();
	}
	
//	public static void main(String[] args) {
//		Map totavert=new HashMap();
//		Vector tmpvector=new Vector();
//		Vector tmptotavect=new Vector();
////		String tota = "31333134343033303831303030323031303031383136343933383235303030303732383131333230353330353038303632383137353630363038303933303533303031373543434348494d534e4220202020202020202020204e4557203030313731202020202020202020202020202020202020202020202020202020202020303030303030303020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020204131323334353638313438363641313233343536383134202020202020e59182efbc8aefbc8a2020202020202020202020202020202020202020202030323038313934394c55202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020203030303030303030e9ab98e5a19ae5b882e5898de98eaee58d80efbc8aefbc8ae9878cefbc8aefbc8ae8a197efbc91e38080efbc90efbc99e8999fefbc93e6a893e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e38080e3808030372d373136332a2a2a2020202020202020202020202020202020202020202020202030393631333539363833202020202020202020202020202020202020202020202020202020202020202020202020202020202020e6ada3e58583e4bc81e6a5ade8a18c2020202020202020202020202020202020204231e8b2a0e8b2ace4baba2020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202034393338323530303030373238313133313130313033323031313031303630373037303038433034313732303033323031322b303030303038303030303030303030303030302b30303030303830303030303031373034313939362b303030303038303030303030313730343139393630313030303030303130313330383637383320203030363931313231323030302b30303030303030303030303030303030303030303033324242424242424242424231333132323030343036323031352020202020202020202020202020202020202020202020202020413132333435363831343038303831393639394752443935313720202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020303030323136392b3030303030303130353930302b3030303030303130353830302b3030303030303130303030302b3030303030303030303030302b3030303030303030303030302b3030303030303030303030302b3030303030303030303030302b3030303030303030303030302b30303030303030303030303031202020202020202020202020202020202020313730343139393630304e3830363830363830362b303030303031363030303030";
////		String tota = "F1C3D4C9F6F2404040404040F1F3F1F4F4F0F3F0F8F1F0F0F0F2F0F1F0F0F1F8F1F6F4F9F3F8F2F5F0F0F0F0F7F2F8F1F1F3F1F4F3F9F3F7F0F8F1F1F2F5F1F4F3F9F3F7F0F8F1F0F3F0F5F3F0F0F1F7F5C3C3C3C8C9D4E2D5C24040404040404040404040D5C5E6404040F1F7F1404040404040404040404040404040404040404040404040404040404040F0F0F0F0F0F0F0F04040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040C1F1F2F3F4F5F6F8F1F4F8F6F6C1F1F2F3F4F5F6F8F1F44040404040400E4EC742D642D64040404040404040404040404040404040404040400F40F0F2F0F8F1F9F4F9D6E2D2D3C9E2C860D5C1D4C54040404040404040404040404040404040400E42F042F15679569C40404040404040404040404040404040404040400FF0F0F0F0F0F0F0F00E42E742E74D6B524965D756B346B846B842E742E746B85AF842F140400F0E42E742E75D6042F360BB40404040404042E742E740404040404040400F0E42E742E742E742E742E742E740404040404040404040404040404040404040404040404040404040404040404040404040404040404040400F0E42E742E740404040404040404040404042E742E740404040404040400F0E42E742E740404040404040404040404042E742E740404040404040400FF9F960F9F9F9F9F9F9F9F9404040404040F8F860F8F8F8F8F8F8F8F840404040404040F6F6F6F6F6F6F6F6F6F64040404040404040404040404040404040404040404040404040404040404040404040404040404040400E4D7F4C984DB95C664E880F404040404040404040404040404040404040C2F10E53D742E742E70F40404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040F4F9F3F8F2F5F0F0F0F0F7F2F8F1F1F3F1F1F0F1F0F3F2F0F1F1F0F1F0F6F0F7F0F7F0F0F8C3F0F4F1F7F2F0F0F3F2F0F1F24EF0F0F0F0F0F8F0F0F0F0F0F0F0F0F0F0F0F0F04EF0F0F0F0F0F8F0F0F0F0F0F0F1F7F0F4F1F9F9F64EF0F0F0F0F0F8F0F0F0F0F0F0F1F7F0F4F1F9F9F6F0F1F0F0F0F0F0F0F1F0F1F3F0F8F6F7F8F34040F0F0F6F9F1F1F2F1F2F0F0F04EF0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F5F4F3F2C2C2C2C2C2C2C2C2F1F3F1F2F1F5F0F8F0F9F2F0F1F54040404040404040404040404040404040404040404040404040C1F1F2F3F4F5F6F8F1F4F0F8F0F8F1F9F6F9404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040F0F0F0F2F1F6F94EF0F0F0F0F0F0F1F0F6F1F0F04EF0F0F0F0F0F0F1F0F6F1F0F04EF0F0F0F0F0F0F1F0F5F9F0F04EF0F0F0F0F0F0F1F0F5F8F0F04EF0F0F0F0F0F0F1F0F0F0F0F04EF0F0F0F0F0F0F0F0F0F0F0F04EF0F0F0F0F0F0F0F0F0F0F0F04EF0F0F0F0F0F0F0F0F0F0F0F04EF0F0F0F0F0F0F0F0F0F0F0F0F1F1F1F1F1F1F1F1F140404040404040404040F1F7F0F4F1F9F9F6F0F0D5F8F0F6F8F0F6F8F0F64EF0F0F0F0F0F1F6F0F0F0F0F0";
//		String tota = "F1C3D4C9F6F2404040404040F1F3F1F4F4F0F3F0F8F1F0F0F0F2F0F1F0F0F1F8F1F6F4F9F0F7F2F3F0F0F0F1F2F2F6F1F0F8F1F5F5F6F0F5F0F8F1F1F2F5F1F5F5F6F0F5F0F8F1F0F3F0F5F3F0F0F1F7F5C3C3C3C8C9D4E2D5C24040404040404040404040D5C5E6404040F1F7F1404040404040404040404040404040404040404040404040404040404040F0F0F0F0F0F0F0F04040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040E6F1F0F0F0F8F9F6F5F5F8F6F6E6F1F0F0F0F8F9F6F5F54040404040400E58C142D642D60F40404040404040404040404040404040404040404040F1F2F0F3F1F9F6F0C5D5C7D3C9E2C860D5C1D4C54040404040404040404040404040404040400E42F042F95679569C0F4040404040404040404040404040404040404040F0F0F0F0F0F0F0F00E42E742E74D6B4C6B4DF756B3506F4D6D42E742E74C5552F342F152940F0E42E742E74F4D42F95D6042F460BB404042E742E740404040404040400F0E42E742E74D6B4C6B4DF756B3506F4D6D51495D9C4C5552F342F142E742E742F24F4D42F95D6042F460BB42E742E7404040404040404040400F0E42E742E740404040404040404040404042E742E740404040404040400F0E42E742E740404040404040404040404042E742E740404040404040400FF9F960F9F9F9F9F9F9F9F9404040404040F8F860F8F8F8F8F8F8F8F840404040404040F6F6F6F6F6F6F6F6F6F64040404040404040404040404040404040404040404040404040404040404040404040404040404040400E4D5A68A34DB95FCF566D4DB14C49547F0F404040404040404040404040C2F90E566D42E742E756F00F4040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040F4F9F0F7F2F3F0F0F0F1F2F2F6F1F0F8F1F1F0F1F0F2F2F0F1F1F0F1F0F2F2F4F0F1F0F0F840F0F3F1F7F2F0F0F2F2F0F1F24EF0F0F0F0F1F5F0F0F0F0F0F0F0F0F0F0F0F0F04EF0F0F0F0F1F5F0F0F0F0F0F0F1F9F0F3F1F9F9F64EF0F0F0F0F1F5F0F0F0F0F0F0F1F9F0F3F1F9F9F6F0F0F0F0F0F0F0F0F1F0F1F3F0F0F4F8F6F84040F0F0F1F1F0F1F5F0F0F0F0F04EF0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0E9E9E9E9C2F1F1F1F1F1F1F1F1F4F1F2F1F5F0F8F0F9F2F0F1F54040404040404040404040404040404040404040404040404040E6F1F0F0F0F8F9F6F5F5F1F2F0F3F1F9F6F0404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040404040F0F0F0F2F1F6F94EF0F0F0F0F0F0F0F0F0F0F0F04EF0F0F0F0F0F0F0F0F0F0F0F04EF0F0F0F0F0F0F0F0F0F0F0F04EF0F0F0F0F0F0F0F0F0F0F0F04EF0F0F0F0F0F0F0F0F0F0F0F04EF0F0F0F0F0F0F0F0F0F0F0F04EF0F0F0F0F0F0F0F0F0F0F0F04EF0F0F0F0F0F0F0F0F0F0F0F04EF0F0F0F0F0F0F0F0F0F0F0F0F1F1F1F1F1F1F1F1F140404040404040404040F1F9F0F3F1F9F9F6F0F0D5F1F0F6F1F0F64040404EF0F0F0F0F0F3F0F0F0F0F0F0";
//		byte[] totabyte=null;
//		byte[] utf8byte=null;
//		try {
//			tota = tota.substring(24);
////			totabyte = tota.getBytes("UTF-8");
////			tota = org.apache.commons.codec.binary.Hex.encodeHexString(tota.getBytes("UTF-8"));
//			log.debug("tota>>{}",tota);
////			totabyte = Hex.decode(tota) ;
//			totabyte = org.apache.commons.codec.binary.Hex.decodeHex(tota);
//			utf8byte = new String(totabyte, "IBM937").getBytes("UTF-8");
//			log.debug("utf8 string>>>{}",new String(utf8byte,"UTF-8"));
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		log.trace("totabyte >>>{}",totabyte);
//		int len =0;
//		int bytecnt =0;
//		Hashtable totahash=new Hashtable();
//		
//		
////		len = 4;
////		byte[] bytetemp=new byte[len];
////		System.arraycopy(totabyte,bytecnt,bytetemp,0,len);
////		try {
////			log.debug("MSGTYPE = " + new String(new String(bytetemp, "IBM937").getBytes("UTF-8"),"UTF-8"));
////		} catch (UnsupportedEncodingException e) {
////			// TODO Auto-generated catch block
////			e.printStackTrace();
////		}
//		
//		assamble(totabyte,totavert);
//		log.trace("GET totavert>>>>>{}",totavert.toString());
//	}
}
