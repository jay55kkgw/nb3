package fstop.services.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import lombok.extern.slf4j.Slf4j;

/**
 * 結清銷戶申請(台幣存款帳戶)
 * @author Owner
 *
 */
@Slf4j
public class N366 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("n365Telcomm")
	private TelCommExec n365Telcomm;
	
//	@Required
//	public void setN365Telcomm(TelCommExec telcomm) {
//		n365Telcomm = telcomm;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 
		// 查詢
		TelcommResult telcommResult = n365Telcomm.query(params);
		return telcommResult;
	}
	
}
