package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.util.ESAPIUtil;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 結清銷戶申請(台幣存款帳戶)
 * @author Owner
 *
 */
@Slf4j
public class N366_1 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("n365Telcomm")
	private TelCommExec n365Telcomm;
	@Autowired
	@Qualifier("n366Telcomm")
	private TelCommExec n366Telcomm;
	
//	@Required
//	public void setN365Telcomm(TelCommExec telcomm) {
//		n365Telcomm = telcomm;
//	}
//	@Required
//	public void setN366Telcomm(TelCommExec telcomm) {
//		n366Telcomm = telcomm;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params) { 
		final Map<String, String> params = _params;
		String fgtxway = params.get("FGTXWAY").toString();
		
		// 查詢
		log.debug(ESAPIUtil.vaildLog("CUSIDN ===> {}"+ params.get("UID")));
		log.debug(ESAPIUtil.vaildLog("ACN ===> {}"+ params.get("ACN")));
		log.debug(ESAPIUtil.vaildLog("INACN ===> {}"+ params.get("INACN")));
		
		log.debug(ESAPIUtil.vaildLog("DPIBAL ===> {}"+ params.get("DPIBAL")));
		log.debug(ESAPIUtil.vaildLog("DPIBALS ===> {}"+ params.get("DPIBALS")));
		log.debug(ESAPIUtil.vaildLog("DPIINT ===> {}"+ params.get("DPIINT")));
		log.debug(ESAPIUtil.vaildLog("DPIINTS ===> {}"+ params.get("DPIINTS")));
		log.debug(ESAPIUtil.vaildLog("ODFINT ===> {}"+ params.get("ODFINT")));
		log.debug(ESAPIUtil.vaildLog("ODFINTS ===> {}"+ params.get("ODFINTS")));
		log.debug(ESAPIUtil.vaildLog("AMTTAX ===> {}"+ params.get("AMTTAX")));
		log.debug(ESAPIUtil.vaildLog("AMTTAXS ===> {}"+ params.get("AMTTAXS")));
		log.debug(ESAPIUtil.vaildLog("AMTNHI ===> {}"+ params.get("AMTNHI")));
		log.debug(ESAPIUtil.vaildLog("AMTNHIS ===> {}"+ params.get("AMTNHIS")));
		log.debug(ESAPIUtil.vaildLog("AMTCLS ===> {}"+ params.get("AMTCLS")));
		log.debug(ESAPIUtil.vaildLog("AMTCLSS ===> {}"+ params.get("AMTCLSS")));
	
		// 結清金額為0，則轉入帳號為空
		if (params.get("AMTCLS").toString().equals("0000000000000")){
			params.put("INACN", "");
		}
		
//		TelcommResult n365telcommResult = n365Telcomm.query(params);		
		
		//params.put("CUSIDN", params.get("UID"));
		//params.put("ACN", n365telcommResult.getValueByFieldName("ACN"));
		
//		params.put("DPIBAL", NumericUtil.formatNumberString(n365telcommResult.getValueByFieldName("DPIBAL"),2));
//		log.debug("DPIBAL===>" +  NumericUtil.formatNumberString(n365telcommResult.getValueByFieldName("DPIBAL"),2));
//		params.put("DPIINT", NumericUtil.formatNumberString(n365telcommResult.getValueByFieldName("DPIINT"),0));
//		log.debug("DPIINT===>" + NumericUtil.formatNumberString(n365telcommResult.getValueByFieldName("DPIINT"),0));
//		params.put("ODFINT", NumericUtil.formatNumberString(n365telcommResult.getValueByFieldName("ODFINT"),0));
//		log.debug("ODFINT===>" + NumericUtil.formatNumberString(n365telcommResult.getValueByFieldName("ODFINT"),0));
//		params.put("AMTTAX", NumericUtil.formatNumberString(n365telcommResult.getValueByFieldName("AMTTAX"),0));
//		log.debug("AMTTAX===>" + NumericUtil.formatNumberString(n365telcommResult.getValueByFieldName("AMTTAX"),0));
//		params.put("AMTNHI", NumericUtil.formatNumberString(n365telcommResult.getValueByFieldName("AMTNHI"),0));
//		log.debug("AMTNHI===>" + NumericUtil.formatNumberString(n365telcommResult.getValueByFieldName("AMTNHI"),0));
//		params.put("AMTCLS", NumericUtil.formatNumberString(n365telcommResult.getValueByFieldName("AMTCLS"),2));
//		log.debug("AMTCLS===>" + NumericUtil.formatNumberString(n365telcommResult.getValueByFieldName("AMTCLS"),2));
//		params.put("INACN", params.get("INACN"));
//		log.debug("INACN===>" + params.get("INACN"));

		
		params.put("BNKRA", "050");   // 行庫別
		if (fgtxway.equals("1"))
		{   //電子簽章
			params.put("KIND", "1");
		}
		else if (fgtxway.equals("2"))
		{   //晶片金融卡			
			params.put("KIND", "2");						
			params.put("CHIP_ACN", params.get("CHIP_ACN").trim());
			params.put("ICSEQ", params.get("ICSEQ"));
		} else if (fgtxway.equals("3")) {
			
		} else {
		  params.put("KIND", "4");	
		}
		
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);	
		
		TelcommResult telcommResult = n366Telcomm.query(params);
		
		return telcommResult;
	}
	
}
