package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
@Transactional
public class N1011 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());
	
	@Autowired
    @Qualifier("n1011Telcomm")
	private TelCommExec n1011Telcomm;
	
//	@Required
//	public void setN1011Telcomm(TelCommExec telcomm) {
//		n1011Telcomm = telcomm;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 
		params.put("DATE", DateTimeUtils.getCDateShort(new Date()));
		params.put("TIME", DateTimeUtils.format("HHmmss", new Date()));
		TelcommResult result = n1011Telcomm.query(params);
		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));

		return result;
	}


}
