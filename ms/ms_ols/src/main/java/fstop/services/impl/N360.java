package fstop.services.impl;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;
import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class N360 extends CommonService  {

	@Autowired
    @Qualifier("n360Telcomm")
	private TelCommExec n360Telcomm;
	
//	@Required
//	public void setN360Telcomm(TelCommExec telcomm) {
//		n360Telcomm = telcomm;
//	}	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params) {
		Map<String, String> params = _params;
		//查詢
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);
		params.put("CUSIDN", (String)params.get("UID"));
		MVHImpl telcommResult = new MVHImpl();
		try {
			telcommResult = n360Telcomm.query(params);
		} catch (TopMessageException e) {
			throw e;
		}
	    return telcommResult;
	}
}
