package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class N1014 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("n1014Telcomm")
	private TelCommExec n1014Telcomm;
	
//	@Required
//	public void setN1014Telcomm(TelCommExec telcomm) {
//		n1014Telcomm = telcomm;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 
		params.put("DATE", DateTimeUtils.getCDateShort(new Date()));
		params.put("TIME", DateTimeUtils.format("HHmmss", new Date()));
		TelcommResult result = n1014Telcomm.query(params);
		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));

		return result;
	}
	/*
	   function countFee(){ 
		  	var fee =0;
		  	var date = DateDiff();
		  	if(date >30)
		  	    fee = 100;
		  	else
		  	    fee = 50;
		  	var num = parseInt(main.COPY.value);
		  	if(num >1)
		  	    fee = fee + (num-1)*20;
		  	    
		  	if(main.TAKE_TYPE[1].checked)
		  	{
		  		main.MAILADDR.disabled=false;
		  		main.DEBITACN.disabled=false;
		  		 fee += 50;
		        }
		        else
		        {
		        	main.MAILADDR.disabled=true;
		        	main.DEBITACN.disabled=true;
		        }
		                	 
		        main.FEE.value = fee;
		        main.FEAMT.value = fee;
		        
		  }
*/
}
