package fstop.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.DBResult;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.orm.dao.TxnFxScheduleDao;
import fstop.orm.dao.TxnTrAccSetDao;
import fstop.orm.dao.TxnTwScheduleDao;
import fstop.orm.po.TXNFXSCHEDULE;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.telcomm.cmd.CommandUtils;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class N215FD_1 extends CommonService  {
	@Autowired
	private TxnTrAccSetDao txnTrAccSetDao;	
	@Autowired
	@Qualifier("n215Telcomm")
	private TelCommExec n215Telcomm;
	@Autowired
	@Qualifier("f037Telcomm")
	private TelCommExec f037Telcomm;
	@Autowired
	private TxnTwScheduleDao txnTwScheduleDao;
	@Autowired
	private TxnFxScheduleDao txnFxScheduleDao;
	
//	@Required
//	public void setN215Telcomm(TelCommExec telcomm) {
//		n215Telcomm = telcomm;
//	}
//	@Required
//	public void setF037Telcomm(TelCommExec telcomm) {
//		f037Telcomm = telcomm;
//	}	
//	@Required
//	public void setTxnTrAccSetDao(TxnTrAccSetDao dao) {
//		this.txnTrAccSetDao = dao;
//	}
//	@Required
//	public void setTxnTwScheduleDao(TxnTwScheduleDao txnTwScheduleDao) {
//		this.txnTwScheduleDao = txnTwScheduleDao;
//	}	
//	@Required
//	public void setTxnFxScheduleDao(TxnFxScheduleDao txnFxScheduleDao) {
//		this.txnFxScheduleDao = txnFxScheduleDao;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params) { 
		Map<String, String> params = _params;
		//查詢
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);
		params.put("CUSIDN", (String)params.get("UID"));
		params.put("KIND", (String)params.get("FGTXWAY"));	
		params.put("TYPE", (String)params.get("TYPE"));	
		params.put("COUNT", (String)params.get("COUNT"));
		params.put("ACNINFO", (String)params.get("ACNINFO"));
		params.put("BNKRA", "050");
		if(params.containsKey("pkcs7Sign") && "1".equals(params.get("FGTXWAY"))) {
			List<String> commands = new ArrayList();
			commands.add("XMLCA()");
			commands.add("XMLCN()");
			
			CommandUtils.doBefore(commands, params);
			
			if(!StrUtils.trim(params.get("XMLCN")).toUpperCase().startsWith(StrUtils.trim(params.get("UID")).toUpperCase())) {
				throw TopMessageException.create("Z089");
			}
		}
		int COUNT=Integer.parseInt(params.get("COUNT"));
		int COUNT1=Integer.parseInt(params.get("COUNT1"));
		String SelectedRecord=(String)params.get("SelectedRecord");
		String SelectedRecord2=(String)params.get("SelectedRecord2");
		String[] BANK = new String[COUNT];
		String[] ACN = new String[COUNT];
		int position1=0;
		int position2=0;
		String[] CUSTID=new String[COUNT1];
		String[] BENACC=new String[COUNT1];
		String[] CCY=new String[COUNT1];
		List<TXNFXSCHEDULE> qresult1 = null;
		List<TXNFXSCHEDULE> qresult2 = null;
		log.debug("N215FD_1.java COUNT:"+COUNT+" SelectedRecord.length:"+SelectedRecord.length());
		log.debug("N215FD_1.java COUNT1:"+COUNT1+" SelectedRecord2.length:"+SelectedRecord2.length());
		for(int i=0;i<COUNT;i++)
		{				
			position2=position1+19;			
			BANK[i]= (SelectedRecord.substring(position1,position2)).substring(0,3);
			ACN[i]=	(SelectedRecord.substring(position1,position2)).substring(3).trim();
			position1=position2;				
		}
		log.debug(ESAPIUtil.vaildLog("N215FD System Out test UID:"+params.get("UID")+ " SelectedRecord:"+SelectedRecord));		
		log.debug(ESAPIUtil.vaildLog("N215FD System Out test UID:"+params.get("UID")+ " SelectedRecord2:"+SelectedRecord2));		
		TelcommResult telcommResult=null;
		if(COUNT>0)
		{	
			telcommResult = n215Telcomm.query(params);		
			if(telcommResult.getValueByFieldName("MSGCOD").equals("0000") || telcommResult.getValueByFieldName("MSGCOD").equals("    "))
			{
				for(int i=0;i<COUNT;i++)
				{
					if(txnTrAccSetDao.findDptrdacnoIsExists((String)params.get("UID"),BANK[i], ACN[i]))
					{
						//TODO:txnTrAccSetDao 沒有 removeById，暫時先註解
//						txnTrAccSetDao.removeById(new Integer((String)params.get("DPACCSETID"+i)));
					}
					try{   //取消該帳號預約交易資料
						qresult1 = txnFxScheduleDao.findScheduleBySVAC((String)params.get("UID"),BANK[i], ACN[i]);
						log.debug("N215FD_1.java qresult1 size:"+qresult1.size()+"===");
						MVHImpl helper =  new DBResult(qresult1);
						int COUNT2=helper.getValueOccurs("DPSCHID");
						log.debug("N215FD_1.java helper COUNT1:"+COUNT1);
						for(int j=0;j<COUNT2;j++)
						{
							log.debug("UPDATE DB DPSCHID:"+helper.getValueByFieldName("DPSCHID", j+1));
							updateDB(helper.getValueByFieldName("DPSCHID", j+1));
						}
					}catch(Exception e){;}				
				}
			}
			//telcommResult.getFlatValues().put("SelectedRecord", (String)params.get("SelectedRecord1"));
		    //telcommResult.getFlatValues().put("COUNT", String.valueOf(COUNT));
		}
		if(COUNT1>0)
		{
			
			position1=0;
			position2=0;
			for(int i=0;i<COUNT1;i++)
			{				
				position2=position1+48;			
				CUSTID[i]= (SelectedRecord2.substring(position1,position2)).substring(0,11);
				BENACC[i]=	(SelectedRecord2.substring(position1,position2)).substring(11,45);
				CCY[i]=	(SelectedRecord2.substring(position1,position2)).substring(45);
				position1=position2;				
				params.put("CUSTID", CUSTID[i]);
				params.put("BENACC", BENACC[i]);
				params.put("CCY", CCY[i]);			
				telcommResult = f037Telcomm.query(params);
				if(telcommResult.getValueByFieldName("MSGCOD").equals("0000") || telcommResult.getValueByFieldName("MSGCOD").equals("    "))
				{

						try{   //取消該帳號預約交易資料
							qresult2 = txnFxScheduleDao.findScheduleBySVAC(CUSTID[i],CCY[i], BENACC[i]);
							log.debug("N215FD_1.java qresult2 size:"+qresult2.size()+"===");
							MVHImpl helper =  new DBResult(qresult2);
							int COUNT2=helper.getValueOccurs("FXSCHID");
							log.debug("N215FD_1.java helper COUNT2:"+COUNT2);
							for(int j=0;j<COUNT2;j++)
							{
								log.debug("UPDATE DB FXSCHID:"+helper.getValueByFieldName("FXSCHID", j+1));
								updateDB(helper.getValueByFieldName("FXSCHID", j+1));
							}
						}catch(Exception e){;}				
				}	
			}			
			//telcommResult.getFlatValues().put("SelectedRecord1", (String)params.get("SelectedRecord3"));
			//telcommResult.getFlatValues().put("COUNT1", String.valueOf(COUNT1));
		}
	    return telcommResult;
	}
	private void updateDB(String schid)
	{
		log.debug("schid==="+schid+"===");
		if(schid != null)
		{
			//txnTwScheduleDao.updateStatusByTwSchID(schid,"3");
			txnFxScheduleDao.updateStatusByFxSchID(schid, "3");
		}
	}	
}
