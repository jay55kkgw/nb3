package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.netbank.rest.model.CommonPools;
import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.telcomm.cmd.BHPWD;
import fstop.util.DateTimeUtils;
import fstop.util.SpringBeanFactory;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class N1010_CP extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());	

	@Autowired
	private CommonPools commonPools;
	
	@Value("${MSIP}")
	private String msIp;
	/**
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 

		/*** 驗證交易密碼 - 開始 ***/
		if (params.get("FGTXWAY") == null) {
			throw TopMessageException.create("S302", (String)params.get("ADOPID"));				
		}
		else if("0".equals(params.get("FGTXWAY"))) {  //使用交易密碼
			
			//目前SSL使用N951
			TelCommExec n951CMTelcomm;
			
			n951CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n951CMTelcomm");
			
			MVHImpl n951Result = n951CMTelcomm.query(params);
			//若驗證失敗, 則會發動 TopMessageException
			
		}		
		/*** 驗證交易密碼 - 結束 ***/
		
		/*** 呼叫 Bill Hunter Web Service 進行密碼變更/重置 ***/
		// 密碼透過 TripleDES 加密
		
		 BHPWD bhpw=null;
		 bhpw = new BHPWD();
		 bhpw.setParams(params);
		 bhpw.setDefine((String)params.get("Oldpwd"));
		 String OKFAIL=(String)bhpw.doCommand();
		 String DES3_Oldpw = (String)bhpw.getParams().get("BHPWD");
		 log.debug("Oldpwd");
		 log.debug("OKFAIL = " + OKFAIL);
		
		// Privacy Violation
		log.debug(ESAPIUtil.vaildLog("DES3_Oldpw = " + DES3_Oldpw));
		
		
		bhpw.setParams(params);
		bhpw.setDefine((String)params.get("Newpwd"));
		OKFAIL=(String)bhpw.doCommand();
		String DES3_Newpw = (String)bhpw.getParams().get("BHPWD");
		log.debug("Newpwd");
		log.debug("OKFAIL = " + OKFAIL);
		
		// Privacy Violation
		log.debug(ESAPIUtil.vaildLog("DES3_Newpw = " + DES3_Newpw));
		
		
		
		params.put("Oldpwd", DES3_Oldpw);
		params.put("Newpwd", DES3_Newpw);		
		
		
		String str_ReturnCode = commonPools.bh.changeBillPwd(
										(String)params.get("Cust_id"),
										(String)params.get("Status"),
										//DES3_Oldpw,
										//DES3_Newpw,
										(String)params.get("Oldpwd"),
										(String)params.get("Newpwd"),
										(String)params.get("Email"),
										DateTimeUtils.getDatetime(new Date()),
										msIp);
										//"192.168.1.5");
		
		MVHImpl helper = new MVHImpl();
		helper.getFlatValues().put("BHCPResult", str_ReturnCode);
		
		return helper;
	}
	

}
