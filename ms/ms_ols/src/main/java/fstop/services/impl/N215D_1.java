package fstop.services.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.DBResult;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.orm.dao.TxnTrAccSetDao;
import fstop.orm.dao.TxnTwSchPayDao;
import fstop.orm.dao.TxnTwSchPayDataDao;
import fstop.orm.dao.TxnTwScheduleDao;
import fstop.orm.po.TXNADDRESSBOOK;
import fstop.orm.po.TXNTRACCSET;
import fstop.orm.po.TXNTWSCHEDULE;
import fstop.orm.po.TXNTWSCHPAY;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.telcomm.cmd.CommandUtils;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class N215D_1 extends CommonService  {
	@Autowired
	private TxnTrAccSetDao txnTrAccSetDao;	
	@Autowired
	@Qualifier("n215Telcomm")
	private TelCommExec n215Telcomm;
	@Autowired
	private TxnTwSchPayDao txnTwSchPayDao;
	@Autowired
	private TxnTwSchPayDataDao txnTwSchPayDataDao;
//	@Autowired
//	private TxnTwScheduleDao txnTwScheduleDao;
	
//	@Required
//	public void setN215Telcomm(TelCommExec telcomm) {
//		n215Telcomm = telcomm;
//	}
//	@Required
//	public void setTxnTrAccSetDao(TxnTrAccSetDao dao) {
//		this.txnTrAccSetDao = dao;
//	}
//	@Required
//	public void setTxnTwScheduleDao(TxnTwScheduleDao txnTwScheduleDao) {
//		this.txnTwScheduleDao = txnTwScheduleDao;
//	}	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params) { 
		Map<String, String> params = _params;
		//查詢
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);
		params.put("CUSIDN", (String)params.get("UID"));
		if("7".equals((String)params.get("FGTXWAY"))) {
			params.put("KIND", "4");	
		}else {
			params.put("KIND", (String)params.get("FGTXWAY"));	
		}
		params.put("TYPE", (String)params.get("TYPE"));	
		params.put("COUNT", (String)params.get("COUNT"));
		params.put("ACNINFO", (String)params.get("ACNINFO"));
		params.put("BNKRA", "050");
		if(params.containsKey("pkcs7Sign") && "1".equals(params.get("FGTXWAY"))) {
			List<String> commands = new ArrayList();
			commands.add("XMLCA()");
			commands.add("XMLCN()");
			
			CommandUtils.doBefore(commands, params);
			
			if(!StrUtils.trim(params.get("XMLCN")).toUpperCase().startsWith(StrUtils.trim(params.get("UID")).toUpperCase())) {
				throw TopMessageException.create("Z089");
			}
		}
		int COUNT=Integer.parseInt(params.get("COUNT"));
		String SelectedRecord=(String)params.get("SelectedRecord");
		String[] BANK = new String[COUNT];
		String[] ACN = new String[COUNT];
		int position1=0;
		int position2=0;
		log.debug("N215D_1.java COUNT:"+COUNT+" SelectedRecord.length:"+SelectedRecord.length());
		for(int i=0;i<COUNT;i++)
		{				
			position2=position1+19;			
			BANK[i]= (SelectedRecord.substring(position1,position2)).substring(0,3);
			ACN[i]=	(SelectedRecord.substring(position1,position2)).substring(3).trim();
			position1=position2;				
		}
		log.debug(ESAPIUtil.vaildLog("N215 System Out test UID:"+params.get("UID")+ " SelectedRecord:"+SelectedRecord));		
		TelcommResult telcommResult = n215Telcomm.query(params);
		//DB_Table邏輯部分修改5/21
		int qresult1 = 0;
		String qresult2 = null;
	    if(telcommResult.getValueByFieldName("MSGCOD").equals("0000") || telcommResult.getValueByFieldName("MSGCOD").equals("    "))
	    {
			for(int i=0;i<COUNT;i++)
			{	
				qresult1 = txnTrAccSetDao.getDpaccsetid((String)params.get("UID"),BANK[i], ACN[i]);
				//刪除資料庫轉入帳號的資料
				if(qresult1 != 0)
				{
					//TODO: txnTrAccSetDao 沒有 removeById，暫時先註解
//					txnTrAccSetDao.removeById(new Integer((String)params.get("DPACCSETID"+i)));					
					TXNTRACCSET newTXNTRACCSET = new TXNTRACCSET();
					newTXNTRACCSET.setDPACCSETID(qresult1);
					txnTrAccSetDao.delete(newTXNTRACCSET);
					log.debug("刪除資料約定帳號");
				}
				try{   
					//取消該帳號預約交易資料
					qresult2 = txnTwSchPayDataDao.getDpschno((String)params.get("UID"),BANK[i], ACN[i]);
					if(qresult2 != null && qresult2 != "") {
						updateDB(qresult2);
						log.debug(ESAPIUtil.vaildLog("更新成功{}"+qresult2));
					}else {
						log.debug(ESAPIUtil.vaildLog("無該預約資料{}"+qresult2));
					}
//					log.debug("N215D_1.java qresult1 size:"+qresult1.size()+"===");
//					MVHImpl helper =  new DBResult(qresult1);
//					int COUNT1=helper.getValueOccurs("DPSCHID");
//					log.debug("N215D_1.java helper COUNT1:"+COUNT1);
//					for(int j=0;j<COUNT1;j++)
//					{
//						log.debug("UPDATE DB DPSCHID:"+helper.getValueByFieldName("DPSCHID", j+1));
//						updateDB(helper.getValueByFieldName("DPSCHID", j+1));
//					}
				}catch(Exception e){;}				
			}
	    }
	    telcommResult.getFlatValues().put("SelectedRecord", (String)params.get("SelectedRecord1"));
	    telcommResult.getFlatValues().put("COUNT", String.valueOf(COUNT));
	    return telcommResult;
	}
	private void updateDB(String twschid)
	{
		// CGI Stored XSS
//		log.debug("twschid==="+twschid+"===");
		
		if(twschid != null)
		{
			txnTwSchPayDao.updataDptxstatus(twschid,"0");
		}
	}	
}
