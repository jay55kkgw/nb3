package fstop.services.impl.fx;

import fstop.telcomm.TelCommExec;


public interface FxTelCommStep extends TelCommExec {
	
	public Exception getLastException();
	
	public boolean isNeedDoF011WinFlagN();
	
	public boolean isSkip();
	
}
