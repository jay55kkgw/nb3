package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.util.ESAPIUtil;

import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
@Transactional
public class N215Q extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("n933Telcomm")
	private TelCommExec n933Telcomm;
	
//	@Required
//	public void setN933Telcomm(TelCommExec telcomm) {
//		n933Telcomm = telcomm;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 

		//查詢
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);
		params.put("CUSIDN", params.get("UID"));
		params.put("TYPE", "03"); //查詢全部
		params.put("TRNCOD", "Q");
		log.debug(ESAPIUtil.vaildLog("N933 System Out test UID:"+params.get("UID")));
		
		TelcommResult telcommResult = n933Telcomm.query(params);
		telcommResult.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		telcommResult.getFlatValues().put("CMRECNUM", telcommResult.getValueOccurs("ACN") + "");
		
		return telcommResult;
	}


}
