package fstop.services.impl;

import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;

import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.notifier.NotifyAngent;
import fstop.orm.dao.TxnCardLogDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.TXNCARDLOG;
import fstop.orm.po.TXNUSER;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class NA30_1 extends CommonService {

//	private Logger logger = Logger.getLogger(NA30_1.class);

	@Autowired
    @Qualifier("na30Telcomm")
	private TelCommExec na30Telcomm;

	@Autowired
	private TxnCardLogDao txnCardLogDao;

	@Autowired
    @Qualifier("na30VerifyTelcomm")
	private TelCommExec na30VerifyTelcomm;

	@Autowired
	private TxnUserDao txnUserDao;

//	@Required
//	public void setTxnUserDao(TxnUserDao txnUserDao) {
//		this.txnUserDao = txnUserDao;
//	}
//
//	@Required
//	public void setNa30VerifyTelcomm(TelCommExec na30VerifyTelcomm) {
//		this.na30VerifyTelcomm = na30VerifyTelcomm;
//	}
//
//	@Required
//	public void setTxnCardLogDao(TxnCardLogDao txnCardLogDao) {
//		this.txnCardLogDao = txnCardLogDao;
//	}
//
//	@Required
//	public void setNa30Telcomm(TelCommExec na30Telcomm) {
//		this.na30Telcomm = na30Telcomm;
//	}

	@Override
	public MVH doAction(Map params) {

		MVHImpl result = new MVHImpl();
		Date d = new Date();
		
		String occurMsg = "0000";

		String inputbirthyy  = (String)params.get("CCBIRTHDATEYY");
		String inputbirthmm  = (String)params.get("CCBIRTHDATEMM");
		String inputbirthdd  = (String)params.get("CCBIRTHDATEDD");

		String inputbirthday = "";
		String outputbirthday = "";

		if (inputbirthyy != null && inputbirthmm != null && inputbirthdd != null){
			inputbirthday = inputbirthyy.trim() + inputbirthmm.trim() + inputbirthdd.trim();
			Date sd = DateTimeUtils.parse("yyyyMMdd", inputbirthday);
			inputbirthday = DateTimeUtils.getCDateShort(sd);
			outputbirthday = inputbirthyy+"/"+inputbirthmm+"/"+inputbirthdd;
		}

		params.put("BIRTHDAY", inputbirthday);
		params.put("TYPE", "02");
		params.put("CUSIDN", (String)params.get("CUSIDN"));//20190621 _CUSIDN改成CUSIDN
		params.put("ICSEQ", (String)params.get("icSeq"));
		log.warn(ESAPIUtil.vaildLog("NA30_1.java TYPE:02 params.put ICSEQ:"+(String)params.get("icSeq")));
		//已使用信用卡申請網銀客戶，再次使用金融卡申請網銀。
		if("0000".equals(occurMsg) && "Y".equals((String)params.get("cflg"))){
			Hashtable<String, String> _params = new Hashtable<String, String>();

			//電文 TYPE:01 身份驗證
			String seq = "00000000" + String.valueOf(Integer.parseInt((String)params.get("icSeq")) - 1);
			seq = seq.substring(seq.length() - 8);

			_params.put("CUSIDN", (String)params.get("CUSIDN"));//20190621 _CUSIDN改成CUSIDN
			_params.put("TYPE", "01");
			_params.put("CARDNUM", (String)params.get("CARDNUM"));
			_params.put("BIRTHDAY", (String)params.get("BIRTHDAY"));
			_params.put("MOBILE", (String)params.get("MOBILE"));
			_params.put("MAIL", (String)params.get("MAIL"));
			_params.put("ICSEQ", seq);
			log.warn("NA30_1.java TYPE:01 _params.put ICSEQ:"+seq);
			try {
				result = na30VerifyTelcomm.query(_params);
			} catch (TopMessageException e) {
				occurMsg = e.getMsgcode();
			}

			//電文 TYPE:03 確認申請(不帶使用者名稱、簽入密碼、交易密碼)
			if("0000".equals(occurMsg)) {
				_params.put("TYPE", "03");
				_params.put("ICSEQ", (String)params.get("ICSEQ"));
				log.warn(ESAPIUtil.vaildLog("NA30_1.java TYPE:03 _params.put ICSEQ:"+(String)params.get("ICSEQ")));

				try {
					result = na30VerifyTelcomm.query(_params);
				} catch (TopMessageException e) {
					occurMsg = e.getMsgcode();
				}				
			}			
		}

		//電文 TYPE:02 確認申請
		if("0000".equals(occurMsg) && !"Y".equals((String)params.get("cflg"))){
			try {
				result = na30Telcomm.query(params);
			} catch (TopMessageException e) {
				occurMsg = e.getMsgcode();
			}	
		}

		//信用卡、金融卡申請網銀，如果使用者不在時建立一個使用者
		if("0000".equals(occurMsg)){
			TXNUSER user = txnUserDao.chkRecord((String)params.get("CUSIDN"));//20190621 _CUSIDN改成CUSIDN
			
			//更新TXNUSER
			user.setASKERRTIMES(new Integer(0));
			user.setCCARDFLAG("2");
			user.setDPMYEMAIL((String)params.get("MAIL"));
			user.setEMAILDATE(DateTimeUtils.getCDateShort(d) + DateTimeUtils.getTimeShort(d));

			String notify = user.getDPNOTIFY();
			if("Y".equals(params.get("NOTIFY_ACTIVE"))){
				if("".equals(StrUtils.trim(notify))){
					notify = notify + "15";
				}else{
					notify = notify + ",15";
				}
			}
			user.setDPNOTIFY(notify);
			txnUserDao.save(user);
		}

		//記錄申請成功log
		if("0000".equals(occurMsg)){
			TXNCARDLOG cardlog = new TXNCARDLOG();		
			cardlog.setLOGDATE(DateTimeUtils.format("yyyyMMdd", d));
			cardlog.setLOGTIME(DateTimeUtils.format("HHmmss", d));
			cardlog.setDPSUERID((String)params.get("CUSIDN"));//20190621 _CUSIDN改成CUSIDN
			cardlog.setCARDTYPE("1");
			cardlog.setCRADNO((String)params.get("CARDNUM"));
			cardlog.setOPERATETYPE("Y".equals((String)params.get("cflg"))? "03" : "02");
			cardlog.setREMOTEIP((String)params.get("USERIP"));
			cardlog.setSTATUS("Y");
			cardlog.setADMCODE("");
			txnCardLogDao.save(cardlog);
		}

		//申請成功發送Email通知申請者
		if("0000".equals(occurMsg)){
			Hashtable<String, String> varmail = new Hashtable();
			varmail.put("SUB","晶片金融卡申請網路銀行成功通知");
			varmail.put("MAILTEXT", getContent(""));
			if(!NotifyAngent.sendNotice("NA30",varmail,(String)params.get("MAIL"))){
				log.debug(ESAPIUtil.vaildLog("晶片金融卡申請網路銀行發送Email失敗.(mail:"+(String)params.get("MAIL")+")"));
			}
		}

		//錯誤訊息往後帶
		result.getFlatValues().put("occurMsg", occurMsg);
		
		//交易時間
		result.getFlatValues().put("CCTXTIME", DateTimeUtils.getDatetime(new Date()));

		//出生年月日
		result.getFlatValues().put("CCBIRTHDATE", outputbirthday);

		//將頁面訊息帶回result
		Iterator it = params.entrySet().iterator();
		while (it.hasNext()){
			Map.Entry entry = (Map.Entry) it.next();
			result.getFlatValues().put((String)entry.getKey(), (String)entry.getValue());
		}

		return result;
	}

	@SuppressWarnings("unused")
	private String getContent(String name){
		StringBuilder sb = new StringBuilder ();
		sb.append("<table cellSpacing=0 cellPadding=0 width=600 border=0 frame=border >").append("\n");
		sb.append("<tbody>").append("\n");
		sb.append("<tr><td align=left>").append(name).append("親愛的客戶，您好：</td></tr></br>").append("\n");
		sb.append("<tr><td align=left>感謝您申請臺灣企銀網路銀行服務！即日起，您將可以更快速、更安全的透過本行網路銀行查詢您相關帳務資訊，歡迎立即登入網路銀行使用。</td></tr>").append("\n");
		sb.append("<tr><td align=rigth>臺灣企銀   敬啟</td></tr>").append("\n");
		sb.append("</tbody>").append("\n");
		sb.append("</table>").append("\n");
		return sb.toString();
	}

}
