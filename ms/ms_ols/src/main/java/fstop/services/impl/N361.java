package fstop.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

import com.netbank.rest.model.CommonPools;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.orm.dao.TxnCusInvAttrHistDao;
import fstop.orm.dao.TxnCusInvAttribDao;
import fstop.orm.dao.TxnFundApplyDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.TXNCUSINVATTRHIST;
import fstop.orm.po.TXNCUSINVATTRIB;
import fstop.orm.po.TXNFUNDAPPLY;
import fstop.orm.po.TXNUSER;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.telcomm.cmd.CommandUtils;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 基金線上預約開戶作業
 * 
 * @author Owner
 *
 */
@Slf4j
public class N361 extends CommonService {
	// private Logger logger = Logger.getLogger(getClass());

	@Value("${answerHax:N}")
	String answerHax;
	@Autowired
	@Qualifier("n361Telcomm")
	private TelCommExec n361Telcomm;
	@Autowired
	@Qualifier("n322Telcomm")
	private TelCommExec n322Telcomm;
	@Autowired
	private TxnCusInvAttribDao txnCusInvAttribDao;
	@Autowired
	private TxnUserDao txnUserDao;
	@Autowired
	@Qualifier("n930Telcomm")
	private TelCommExec n930Telcomm; // 用戶電子郵箱位址變更
	@Autowired
	private TxnCusInvAttrHistDao txnCusInvAttrHistDao;
	@Autowired
	private CommonPools commonPools;
	@Autowired
	private TxnFundApplyDao txnFundApplyDao;

	@Value("${kyc_verion_natural_person:11012}")
	String kyc_verion_n;

	@Value("${kyc_verion_juristic_person:11012}")
	String kyc_verion_j;

	@Override
	public MVH doAction(Map _params) {
		final Map<String, String> params = _params;

		MVHImpl result = new MVHImpl();
		String occurMsg = "";
		String uid = params.get("UID").toString();
		String fgtxway = params.get("FGTXWAY").toString();
		String BRHCOD = "";
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);

		/*** 修改記錄答案電文20180912 -開始 ***/
		String nAnswer[][] = new String[15][3];
		for (int i = 1; i <= nAnswer.length; i++) {
			int sc = 0;
			int sc2 = 0;

			if ((uid.length() != 10) && i == 15) {// 法人(14題)與自然人(15題)KYC問卷題目數量不同，需先判斷客戶身分
				nAnswer[i - 1][1] = "0";
				nAnswer[i - 1][2] = "0";
			} else {
				nAnswer[i - 1][0] = (String) params.get("FDQ" + i);

				for (int j = 1; j <= nAnswer[i - 1][0].length(); j++) {

					if (nAnswer[i - 1][0].substring(j - 1, j).equals("A")) {
						sc += 8;
					} else if (nAnswer[i - 1][0].substring(j - 1, j).equals("B")) {
						sc += 4;
					} else if (nAnswer[i - 1][0].substring(j - 1, j).equals("C")) {
						sc += 2;
					} else if (nAnswer[i - 1][0].substring(j - 1, j).equals("D")) {
						sc += 1;
					}
					if (nAnswer[i - 1][0].substring(j - 1, j).equals("E")) {
						sc2 += 8;
					} else if (nAnswer[i - 1][0].substring(j - 1, j).equals("F")) {
						sc2 += 4;
					} else if (nAnswer[i - 1][0].substring(j - 1, j).equals("G")) {
						sc2 += 2;
					} else if (nAnswer[i - 1][0].substring(j - 1, j).equals("H")) {
						sc2 += 1;
					}
				}
				nAnswer[i - 1][1] = String.valueOf(sc);
				nAnswer[i - 1][2] = String.valueOf(sc2);
			}
			log.warn("N361 ----------> " + i + " " + nAnswer[i - 1][0] + ",sc: " + sc);
			log.warn("N361 ----------> " + i + " " + nAnswer[i - 1][1] + ",sc2: " + sc2);
		}

		String[] HEX = new String[15];
		String allanswer = "";

		for (int k = 0; k < HEX.length; k++) {
			HEX[k] = Integer.toHexString(Integer.parseInt(nAnswer[k][1])).toUpperCase()
					+ Integer.toHexString(Integer.parseInt(nAnswer[k][2])).toUpperCase();
			log.warn("allanswer ----------> " + k + ":" + HEX[k]);
			allanswer += HEX[k];
		}

		log.info("allanswer ----------> " + allanswer);
		/*** 修改記錄答案電文20180918 -結束 ***/

		params.put("DATA", params.get("FDINVTYPE"));
		params.put("DATE", df);
		params.put("TIME", tf);
		params.put("UPD_FLG", (String) params.get("UPD_FLG"));
		params.put("DEGREE", (String) params.get("DEGREE"));
		params.put("CAREER", (String) params.get("CAREER"));
		params.put("SALARY", (String) params.get("SALARY"));

		// 將KYC版本號變成環境變數 , 寫在fund或common內
		params.put("EDITON", (uid.length() == 10) ? kyc_verion_n : kyc_verion_j);

		if (answerHax.equals("Y")) {
			params.put("ANSWER", allanswer); // 修改答案傳遞電文
		} else {
			params.put("ANSWER", (String) params.get("ANSWER")); // 修改答案傳遞電文-hex // 先不做hex-20200527,by hugo
		}
		params.put("MARK1", (String) params.get("FDMARK1"));
		params.put("BNKRA", "050"); // 行庫別
		if (params.containsKey("pkcs7Sign") && "1".equals(fgtxway)) {
			List<String> commands = new ArrayList();
			commands.add("XMLCA()");
			commands.add("XMLCN()");

			CommandUtils.doBefore(commands, params);

			if (!StrUtils.trim(params.get("XMLCN")).toUpperCase()
					.startsWith(StrUtils.trim(params.get("UID")).toUpperCase())) {
				throw TopMessageException.create("Z089");
			}
		}
		if (fgtxway.equals("1")) { // 電子簽章
			params.put("KIND", "1");
		} else { // 晶片金融卡
			params.put("KIND", "2");
			params.put("CHIP_ACN", params.get("CHIP_ACN").trim());
			params.put("ICSEQ", params.get("ICSEQ"));
		}

		params.put("CUSIDN", uid);
		params.put("SCORE", params.get("FDSCORE"));
		params.put("IP", params.get("ADUSERIP"));

		try {
			params.put("E_MAIL_ADR", (String) params.get("DPMYEMAIL"));
			params.put("FLAG", "02"); // 更新需用02打中心更心
			log.info(ESAPIUtil.vaildLog("N361 Step1 ---N930 params: " + params));

			String dateTime = DateTimeUtils.getCDateShort(d) + DateTimeUtils.getTimeShort(d);
			result = n930Telcomm.query(params);
			occurMsg = result.getValueByFieldName("MSGCOD").trim();
			log.info("Step1 >> n930telcomm msgcod: {}", occurMsg);
			// result.getFlatValues().put("occurMsg", occurMsg);
			if (occurMsg.equals("0000") || occurMsg.equals("") || occurMsg.equals("R002")) {
				TXNUSER txnUser = txnUserDao.findById(uid);
				txnUser.setDPMYEMAIL((String) params.get("DPMYEMAIL"));
				txnUser.setEMAILDATE(dateTime);
				txnUser.setDPFUNDBILL("Y");
				txnUser.setFUNDBILLDATE(dateTime);
				txnUserDao.save(txnUser);
				log.info("N361 TXNUSER Saved");

			} else {
				throw TopMessageException.create(occurMsg);
			}

			log.info(ESAPIUtil.vaildLog("N361 Step2-1 --- N322 params:" + params));
			result = n322Telcomm.query(params);
			occurMsg = result.getValueByFieldName("MSGCOD").trim();
			log.info("Step2 >>  n322telcomm msgcod: {}", occurMsg);
			result.getFlatValues().put("occurMsg", occurMsg);
			if (occurMsg.equals("0000") || occurMsg.equals("")) {
				txnUserDao = (TxnUserDao) SpringBeanFactory.getBean("txnUserDao");
				List<TXNUSER> listtxnUser;
				listtxnUser = txnUserDao.findByUserId((String) params.get("UID"));
				if (listtxnUser == null || listtxnUser.size() == 0) {
					log.error(ESAPIUtil.vaildLog("User ID not found = {}" + params.get("UID")));
				} else {
					TXNUSER txnuser = listtxnUser.get(0);
					BRHCOD = StrUtils.trim(txnuser.getADBRANCHID());
					if (BRHCOD.length() == 0) {
						log.info(ESAPIUtil.vaildLog("N361 Step2-2 ---n927 params: " + params));
						TelcommResult n927 = (TelcommResult) commonPools.n927.getData((String) params.get("UID"));
						BRHCOD = n927.getValueByFieldName("APLBRH");
						log.info("N361 Step2-2 --- N927 msgCode: {}", n927.getValueByFieldName("msgCode"));
						log.info(ESAPIUtil.vaildLog("N927 BRHCOD: {}" + BRHCOD));
					}
				}

				TXNCUSINVATTRIB attrib = new TXNCUSINVATTRIB();
				attrib.setFDUSERID((String) params.get("UID"));
				attrib.setFDQ1((String) params.get("FDQ1"));
				attrib.setFDQ2((String) params.get("FDQ2"));
				attrib.setFDQ3((String) params.get("FDQ3"));
				attrib.setFDQ4((String) params.get("FDQ4"));
				attrib.setFDQ5((String) params.get("FDQ5"));
				attrib.setFDSCORE((String) params.get("FDSCORE"));
				attrib.setFDINVTYPE((String) params.get("FDINVTYPE"));
				attrib.setLASTUSER(BRHCOD); // 簽約行
				attrib.setLASTDATE(DateTimeUtils.format("yyyyMMdd", new Date()));
				attrib.setLASTTIME(DateTimeUtils.format("HHmmss", new Date()));
				attrib.setFDQ6((String) params.get("FDQ6"));
				attrib.setFDQ7((String) params.get("FDQ7"));
				attrib.setFDQ8((String) params.get("FDQ8"));
				attrib.setFDQ9((String) params.get("FDQ9"));
				attrib.setFDQ10((String) params.get("FDQ10"));
				attrib.setFDQ11((String) params.get("FDQ11"));
				attrib.setFDQ12((String) params.get("FDQ12"));
				attrib.setFDQ13((String) params.get("FDQ13"));
				attrib.setFDQ14((String) params.get("FDQ14"));
				if (((String) params.get("UID")).length() == 10) {
					attrib.setFDQ15((String) params.get("FDQ15"));
					attrib.setMARK1((String) params.get("FDMARK1"));
				} else {
					attrib.setFDQ15("");
					attrib.setMARK1("");
				}
				attrib.setADUSERIP((String) params.get("ADUSERIP"));

				log.info(ESAPIUtil.vaildLog("N361 Step2-3 ---TXNCUSINVATTRIB"));
				// 微服務修改，原本只有 save，如果 TXNCUSINVATTRIB 已存在資料會因為 PK 重複出錯，新增 update
				TXNCUSINVATTRIB record = txnCusInvAttribDao.findById((String) params.get("UID"));
				log.info(ESAPIUtil.vaildLog("N361 TxnCusInvAttrib record >> {}" + CodeUtil.toJson(record)));
				if (record == null) {
					txnCusInvAttribDao.save(attrib);
					log.info("N361 TxnCusInvAttrib Saved");
				} else {
					txnCusInvAttribDao.update(attrib);
					log.info("N361 TxnCusInvAttrib Updated");
				}

				log.info(ESAPIUtil.vaildLog("N361 Step2-4 ---TXNCUSINVATTRHIST"));
				TXNCUSINVATTRHIST updatePo = txnCusInvAttrHistDao
						.findById(Long.parseLong((String) params.get("FDHISTID")));
				updatePo.setAGREE("Y");
				txnCusInvAttrHistDao.update(updatePo);
				log.info("N361 TXNCUSINVATTRHIST Saved");

				String ACN_SSV = "";
				String ACN_FUD = "";
				params.put("ICSEQ", params.get("ICSEQ1"));
				ACN_SSV = (String) params.get("ACN_SSV");
				ACN_FUD = (String) params.get("ACN_FUD");
				if (ACN_SSV.length() == 0) {
					ACN_SSV = "00000000000";
					params.put("ACN_SSV", ACN_SSV);
				}
				if (ACN_FUD.length() == 0) {
					ACN_FUD = "00000000000";
					params.put("ACN_FUD", ACN_FUD);
				}

				log.info(ESAPIUtil.vaildLog("N361 Step3 --- N361 params:" + params));

				result = n361Telcomm.query(params);
				occurMsg = result.getValueByFieldName("MSGCOD").trim();
				result.getFlatValues().put("occurMsg", occurMsg);
				log.info("N361 Step3  --- n361telcomm msgcod: {}", occurMsg);
				if (occurMsg.equals("0000") || occurMsg.equals("")) {
					
				} else {
					throw TopMessageException.create(occurMsg);
				}

				TXNFUNDAPPLY txnfunapplyPo = new TXNFUNDAPPLY(); // 20210127 信託部需求，基金預約開戶資料存在前端資料庫並留存ip及交易時間
				txnfunapplyPo.setUSERID(uid);
				txnfunapplyPo.setACN_SSV(ACN_SSV);
				txnfunapplyPo.setACN_FUD(ACN_FUD);
				txnfunapplyPo.setBILLMTH((String) params.get("BILLMTH"));
				txnfunapplyPo.setKIND((String) params.get("KIND"));
				txnfunapplyPo.setMSGCODE(occurMsg);
				txnfunapplyPo.setIP((String) params.get("ADUSERIP"));
				txnfunapplyPo.setLASTDATE(DateTimeUtils.format("yyyyMMdd", new Date()));
				txnfunapplyPo.setLASTTIME(DateTimeUtils.format("HHmmss", new Date()));
				txnFundApplyDao.save(txnfunapplyPo);
			}else {
				log.error("N322 error , msgcode : {}" , occurMsg );
				throw TopMessageException.create(occurMsg);
			}
		} catch (Exception e) {
			log.error("N361.java unCatch Error: {}",e);
		}
		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		result.getFlatValues().put("BRHCOD", BRHCOD);
		return result;
	}
}
