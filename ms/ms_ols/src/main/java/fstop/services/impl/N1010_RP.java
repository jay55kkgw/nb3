package fstop.services.impl;

import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.rest.model.CommonPools;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
//import webBranch.utility.CommonPools;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class N1010_RP extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());	

	@Autowired
	private CommonPools commonPools;
	
	@Value("${MSIP}")
	private String msIp;
	
	/**
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 

		/*** 驗證交易密碼 - 開始 ***/
		if (params.get("FGTXWAY") == null) {
			throw TopMessageException.create("S302", (String)params.get("ADOPID"));				
		}
		else if("0".equals(params.get("FGTXWAY"))) {  //使用交易密碼
			
			//目前SSL使用N951
			TelCommExec n951CMTelcomm;
			
			n951CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n951CMTelcomm");
			
			MVHImpl n951Result = n951CMTelcomm.query(params);
			//若驗證失敗, 則會發動 TopMessageException
		}		
		/*** 驗證交易密碼 - 結束 ***/
		
		/*** 呼叫 Bill Hunter Web Service 進行密碼變更/重置 ***/
		String str_ReturnCode = commonPools.bh.changeBillPwd(
										(String)params.get("Cust_id"),
										(String)params.get("Status"),
										"",
										"",
										(String)params.get("Email"),
										DateTimeUtils.getDatetime(new Date()),
										msIp);
										//"172.22.11.23");
		
		MVHImpl helper = new MVHImpl();
		helper.getFlatValues().put("BHRPResult", str_ReturnCode);
		
		return helper;
	}
}
