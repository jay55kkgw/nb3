package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.util.ESAPIUtil;

import fstop.exception.ToRuntimeException;
import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
//import fstop.orm.dao.TxnReqInfoDao;
//import fstop.orm.dao.TxnTwRecordDao;
//import fstop.services.CommonService;
import fstop.services.DispatchService;
import fstop.services.ServiceBindingException;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 行動銀行啟用／關閉
 * @author Owner
 *
 */
@Slf4j
public class N913 extends DispatchService {
//	private Logger logger = Logger.getLogger(getClass());

//	@Autowired
//	private CommonService n913;

	@Autowired
    @Qualifier("na20Telcomm")
	private TelCommExec na20Telcomm;

	@Autowired
    @Qualifier("n913Telcomm")
	private TelCommExec n913Telcomm;

	@Autowired
    @Qualifier("n951Telcomm")
	private TelCommExec n951Telcomm;

//	@Autowired
//	private TxnTwRecordDao txnTwRecordDao;
//
//	@Autowired
//	private TxnReqInfoDao txnReqInfoDao;
	
//	@Required
//	public void setTxnReqInfoDao(TxnReqInfoDao txnReqInfoDao) {
//		this.txnReqInfoDao = txnReqInfoDao;
//	}
//	
//	@Required
//	public void setN913(CommonService service) {
//		n913 = service;
//	}
//	
//	@Required
//	public void setNa20Telcomm(TelCommExec telcomm) {
//		na20Telcomm = telcomm;
//	}
//	@Required
//	public void setN913Telcomm(TelCommExec telcomm) {
//		n913Telcomm = telcomm;
//	}
//	
//	@Required
//	public void setN951Telcomm(TelCommExec telcomm) {
//		this.n951Telcomm = telcomm;
//	}
//	
//	@Required
//	public void setTxnTwRecordDao(TxnTwRecordDao txnTwRecordDao) {
//		this.txnTwRecordDao = txnTwRecordDao;
//	}
	
	@Override
	public String getActionMethod(Map<String, String> params) {
		String txid = StrUtils.trim(params.get("TXID"));
		log.debug(ESAPIUtil.vaildLog("XXXX txid: " + txid));
		if("N913".equals(txid))
			return "doNA20";
		else if("N913_CHK".equals(txid))
			return "doN913CHK";
		else if("N913_1".equals(txid))
			return "doN913";
		else 
			throw new ServiceBindingException("不支援的 TXID [" + txid+"].");
	}
	
	@SuppressWarnings("unchecked")
	public MVH doNA20(Map params) {  
		log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Do NA20");

		Date d = new Date();
		String CurrentDate = DateTimeUtils.format("yyyyMMdd", d);
		int CurrentYear = Integer.valueOf(CurrentDate.substring(0, 4)) - 1911;
		params.put("DATE", CurrentYear + CurrentDate.substring(4, 8));
		params.put("TIME", DateTimeUtils.format("HHmmss", d));
		
		TelcommResult result = na20Telcomm.query(params);
		
		return result;
	}

	@SuppressWarnings("unchecked")
	public MVH doN913CHK(Map params) { 
		log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Do N913CHK");
		
		MVHImpl result = new MVHImpl();
		
		try
		{
			log.info("N950PASSWORD ===================================================> " + params.get("N950PASSWORD"));
			MVHImpl CheckResult = n951Telcomm.query(params);
			boolean PassCheckPass = false;
			
			PassCheckPass = CheckResult.getValueByFieldName("MSGCOD", 1).equals("0000");
			if(!PassCheckPass)PassCheckPass = CheckResult.getValueByFieldName("MSGCOD", 1).equals("");
			
			if(PassCheckPass)
			{
				result = (MVHImpl)doN913(params);
			}
			else
			{
				throw TopMessageException.create(CheckResult.getValueByFieldName("MSGCOD", 1));
			}
		}
		catch (TopMessageException Tex)
		{
			throw Tex;
		}
		catch (Exception e)
		{
			throw new ToRuntimeException(e.getMessage(), e);
		}
		
		return result;
	}

	@SuppressWarnings("unchecked")
	public MVH doN913(Map params) { 
		log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Do N913");

		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);

		log.info("MB_Switch ===================================================> " + params.get("MB_Switch"));
		log.info("MB_Ori ===================================================> " + params.get("OriStatus"));
		String CurrentDate = DateTimeUtils.format("yyyyMMdd", d);
		int CurrentYear = Integer.valueOf(CurrentDate.substring(0, 4)) - 1911;
		params.put("DATE", CurrentYear + CurrentDate.substring(4, 8));
		params.put("TIME", DateTimeUtils.format("HHmmss", d));
		
		String FinalKey = "A".equals((String)params.get("MB_Switch")) ? "1" : "2";
		
		params.put("OPTION", FinalKey);
		
		MVHImpl result = new MVHImpl();
		
		result = n913Telcomm.query(params);
		
		return result;
	}

}
