package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Transactional
public class N880 extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("n880Telcomm")
	private TelCommExec n880Telcomm;

//	@Autowired
//	private TxnTwScheduleDao txnTwScheduleDao;

//	@Autowired
//	private TxnFxScheduleDao txnFxScheduleDao;
	 
//	@Required
//	public void setTxnTwScheduleDao(TxnTwScheduleDao txnTwScheduleDao) {
//		this.txnTwScheduleDao = txnTwScheduleDao;
//	}
//
//	@Required
//	public void setTxnFxScheduleDao(TxnFxScheduleDao txnFxScheduleDao) {
//		this.txnFxScheduleDao = txnFxScheduleDao;
//	}
//
//	@Required
//	public void setN880Telcomm(TelCommExec telcomm) {
//		n880Telcomm = telcomm;
//	}
	
	@Override	
	public MVH doAction(Map params) {
//		log.debug("NEW_EMAIL==@");

		MVH helper = new MVHImpl();
		//DB讀取部分因為Table更動改至NB3，只保留電文部分
//		String method = StrUtils.trim((String)params.get("EXECUTEFUNCTION"));
//		log.debug("n880-EXECUTEFUNCTION====="+method+"===");
//		if(method.equals("QUERY"))
//			helper = querySchedule(params);
//		if(method.equals("N880"))
		helper = doN880(params);
		return helper;
	}
	
//	private MVH querySchedule(Map params)
//	{
//		String actype = (String)params.get("ACTYPE");
//		String cusidn = (String)params.get("CUSIDN");
//		String acn    = "";
//		String bnkcod = "";
//		if(actype.equals("1")) //發n921查到的資料
//		{	 
//			acn = (String)params.get("ACN");
//			bnkcod = (String)params.get("BNKCOD"); //銀行代碼
//		}	
//		else 
//		{
//			acn = (String)params.get("BENACC");//外幣跨行匯款約定轉入帳號
//			bnkcod = (String)params.get("CCY");  //轉入幣別
//		}
//		TelcommResult helper = new TelcommResult(new Vector());
//		int num = 0;
//		final Map<String, MVH> results = new LinkedHashMap();		
//		
//		List<TXNTWSCHEDULE> qresult1 = null;
//		List<TXNTWSCHEDULE> qresult2 = null;
//		try{
//			qresult1 = txnTwScheduleDao.findScheduleTodayBySVAC(cusidn,bnkcod, acn);
//			log.debug("Fuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu1"+qresult1.size()+"===");
//		}catch(Exception e){;}
//		results.put("txntwscheduletodaydao", new DBResult(qresult1));
//		try{
//			qresult2 = txnTwScheduleDao.findScheduleBySVAC(cusidn,bnkcod, acn);
//			log.debug("Fuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu2"+qresult2.size()+"===");
//		}catch(Exception e){;}
//		results.put("txntwscheduledao", new DBResult(qresult2));
//		
//		List<TXNFXSCHEDULE> qresult3 = null;
//		List<TXNFXSCHEDULE> qresult4 = null;
//		try{
//			qresult3 = txnFxScheduleDao.findScheduleTodayBySVAC(cusidn,bnkcod,acn);
//			log.debug("Fuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu3"+qresult3.size()+"===");
//		}catch(Exception e){;}
//		results.put("txnfxscheduletodaydao", new DBResult(qresult3));
//		try{
//			qresult4 = txnFxScheduleDao.findScheduleBySVAC(cusidn,bnkcod,acn);
//			log.debug("Fuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu4"+qresult4.size()+"===");
//		}catch(Exception e){;}
//		results.put("txnfxscheduledao", new DBResult(qresult4));
//		
//		helper.addTable(results.get("txntwscheduletodaydao"), "txntwscheduledaoTable1");
//		if(qresult1 != null)
//		{
//			log.debug("qresult1.size()==="+qresult1.size());
//			num += qresult1.size();
//		}
//		helper.addTable(results.get("txntwscheduledao"), "txntwscheduledaoTable2");
//		if(qresult2 != null)
//		{
//			log.debug("qresult2.size()==="+qresult2.size());
//			num += qresult2.size();
//		}
//		helper.addTable(results.get("txnfxscheduletodaydao"), "txnfxscheduledaoTable1");
//		if(qresult3 != null)
//		{
//			log.debug("qresult3.size()==="+qresult3.size());
//			num += qresult3.size();
//		}
//		helper.addTable(results.get("txnfxscheduledao"), "txnfxscheduledaoTable2");
//		if(qresult4 != null)
//		{
//			log.debug("qresult4.size()==="+qresult4.size());
//			num += qresult4.size();
//		}
//		
//		helper.getFlatValues().put("QTIME", DateTimeUtils.getDatetime(new Date()));
//		helper.getFlatValues().put("COUNT", num + "");
//		
//		return helper;
//	}
	
	private MVH doN880(Map params)
	{
		MVHImpl helper = new MVHImpl();
		
		//TODO: Map無法直接clone
//		Hashtable cloneParams = (Hashtable)params.clone();
		Map cloneParams = params;
		
		//即時交易選擇憑證，這裡要塞CERTACN成19個1
		if("1".equals((String)cloneParams.get("FGTXWAY"))) {
			cloneParams.put("CERTACN", StrUtils.repeat("1", 19));
		//即時交易選擇IDGATE，這裡要塞CERTACN成19個C
		}else if ("7".equals((String) cloneParams.get("FGTXWAY"))) {
			cloneParams.put("CERTACN", StrUtils.repeat("C", 19));
		}
		
		String acn = (String)cloneParams.get("ACN");
		
		while(acn.length() < 16)acn = "0" + acn;
		
		cloneParams.put("ACN", acn);
		
		try {
			helper = n880Telcomm.query(cloneParams);
			helper.getFlatValues().put("STATUS", "交易成功");
//			updateDB((String)cloneParams.get("TWSCHID"),(String)cloneParams.get("TWCOUNT"),(String)cloneParams.get("FXSCHID"),(String)cloneParams.get("FXCOUNT"));
		}
		catch(TopMessageException e) {
			helper = new MVHImpl();
			helper.getFlatValues().put("STATUS", "交易失敗-"+e.getMsgcode());
			throw e;
		}
		helper.getFlatValues().put("CMQTIME",DateTimeUtils.getDatetime(new Date()));

		return helper;
		
		/*String[] arrayParams = (String[])params.get("ArrayParam");
		if(arrayParams != null) {
			for(String p : arrayParams) {
				logger.debug("取消約定轉入帳號 do : " + p); //{}
				Hashtable cloneParams = (Hashtable)params.clone();
				Map jb = JSONUtils.json2map(p);
				cloneParams.putAll(jb);
				//cloneParams.put("DATE", DateTimeUtils.getCDateShort(new Date()));
				//cloneParams.put("TIME", DateTimeUtils.format("HHmmss", new Date()));
				MVHImpl tr = null;
				try {
					tr = n880Telcomm.query(cloneParams);
					tr.getFlatValues().put("STATUS", "交易成功");
				}
				catch(TopMessageException e) {
					tr = new MVHImpl();
					tr.getFlatValues().put("STATUS", "交易失敗-"+e.getMsgcode());
				}
				tr.getFlatValues().putAll(jb);
				helper.getOccurs().addRow(new Row(tr.getFlatValues()));
			}
		}
		
		
		helper.getFlatValues().put("CMQTIME",
				DateTimeUtils.getDatetime(new Date()));
		
		
		
		return helper;*/
	}
//	private void updateDB(String twschid ,String twcount,String fxschid,String fxcount)
//	{
//		log.debug("twschid==="+twschid+"===");
//		log.debug("twcount==="+twcount+"===");
//		log.debug("fxschid==="+fxschid+"===");
//		log.debug("fxcount==="+fxcount+"===");
//		String tmpStr = "";
//		int twno = 0;
//		if(twcount != null)
//		{
//			twno = Integer.parseInt(twcount);
//			log.debug("twno==="+twno+"===");
//			for(int i=0;i<twno;i++)
//			{
//				int twsubNo = twschid.indexOf(",");
//				if(twsubNo == -1) break;
//				String twsubStr = twschid.substring( 0, twsubNo );
//				txnTwScheduleDao.updateStatusByTwSchID(twsubStr,"3");
//				tmpStr = twschid.substring(twsubNo + 1);
//				twschid = tmpStr;
//			}
//		}
//		
//		int fxno =0;
//		if(fxcount != null)
//		{
//			fxno = Integer.parseInt(fxcount);
//			log.debug("fxno==="+fxno+"===");
//			for(int i=0;i<fxno;i++)
//			{
//				int fxsubNo = fxschid.indexOf(",");
//				if(fxsubNo == -1) break;
//				String fxsubStr = fxschid.substring( 0, fxsubNo );
//				txnFxScheduleDao.updateStatusByFxSchID(fxsubStr, "3");
//				tmpStr = fxschid.substring(fxsubNo + 1);
//				fxschid = tmpStr;
//			}
//		}
//	}
}
