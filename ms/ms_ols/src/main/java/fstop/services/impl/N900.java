package fstop.services.impl;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.orm.dao.TxnReqInfoDao;
import fstop.services.CommonService;
import fstop.services.ServiceBindingException;
import fstop.services.TelCommExecWrapper;
import fstop.telcomm.TelCommExec;
import fstop.telcomm.cmd.CommandUtils;
import fstop.util.StrUtils;
import fstop.ws.EAIWebServiceTemplate;
import lombok.extern.slf4j.Slf4j;

/**
 * 變更信用卡通訊地址／電話
 * @author Owner
 *
 */
@Slf4j
public class N900 extends CommonService {
//	public class N900 extends DispatchService {
//	private Logger logger = Logger.getLogger(getClass());

//	@Autowired
//	private CommonService n900;

	@Autowired
    @Qualifier("n810Telcomm")
	private TelCommExec n810Telcomm;

	@Autowired
    @Qualifier("n813Telcomm")
	private TelCommExec n813Telcomm;

	@Autowired
    @Qualifier("c900Telcomm")
	private TelCommExec c900Telcomm;

	@Autowired
    @Qualifier("n900ExecTelcomm")
	private TelCommExec n900ExecTelcomm;

//	@Autowired
//	private TxnTwRecordDao txnTwRecordDao;

	@Autowired
	private TxnReqInfoDao txnReqInfoDao;
	
	@Autowired
	private C900 c900;
	
	@Autowired
	private EAIWebServiceTemplate eaiwebservicetemplate;
	
//	@Required
//	public void setTxnReqInfoDao(TxnReqInfoDao txnReqInfoDao) {
//		this.txnReqInfoDao = txnReqInfoDao;
//	}
//	
//	@Required
//	public void setN900(CommonService service) {
//		n900 = service;
//	}
//	
//	@Required
//	public void setN810Telcomm(TelCommExec telcomm) {
//		n810Telcomm = telcomm;
//	}
//	
//	@Required
//	public void setN813Telcomm(TelCommExec telcomm) {
//		n813Telcomm = telcomm;
//	}
//	
//	@Required
//	public void setC900Telcomm(TelCommExec telcomm) {
//		c900Telcomm = telcomm;
//	}
//	
//	@Required
//	public void setN900ExecTelcomm(TelCommExec telcomm) {
//		n900ExecTelcomm = telcomm;
//	}
//	
//	@Required
//	public void setTxnTwRecordDao(TxnTwRecordDao txnTwRecordDao) {
//		this.txnTwRecordDao = txnTwRecordDao;
//	}
	
//	@Override
//	public String getActionMethod(Map<String, String> params) {
//		String txid = StrUtils.trim(params.get("TXID"));
//		log.debug("XXXX txid: " + txid);
//		if("N900_1".equals(txid))
//			return "doN900_1";
//		else if("N900_2".equals(txid))
//			return "doN900_2";
//		else 
//			throw new ServiceBindingException("不支援的 TXID [" + txid+"].");
//	}
	
	
	@Override
	public MVH doAction(Map params) {
		// TODO Auto-generated method stub
		String txid = StrUtils.trim((String)params.get("TXID"));
		log.debug(ESAPIUtil.vaildLog("XXXX txid: " + txid));
		if("N900_1".equals(txid))
			return doN900_1(params);
		else if("N900_2".equals(txid))
			return doN900_2(params);
		else 
			throw new ServiceBindingException("不支援的 TXID [" + txid+"].");
		
		
		
//		return null;
	}
	
	
	@SuppressWarnings("unchecked")
	public MVH doN900_1(Map params) { 
		log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Do N900_1");

		Map _params = new Hashtable();
		
		try
		{
		TelcommResult N810result = n810Telcomm.query(params);
		//為取得錯誤訊息，新增此判斷程式
		if(!"".equals(N810result.getFlatValues().get("TOPMSG"))) {
			log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Do n813Telcomm");
			
			TelcommResult N813result = n813Telcomm.query(params);
			if(!"".equals(N813result.getFlatValues().get("TOPMSG"))) {
				log.debug(ESAPIUtil.vaildLog("TOPMSG>>>>> "+N813result.getFlatValues().get("TOPMSG")));
				return N813result;
			}
			int N813RecordCount = N813result.getValueOccurs("CARDNUM");
			if(N813RecordCount > 0)
			{
				String strCardNum = "";
				
				for(int i = 0; i < N813RecordCount; i++)
				{
					String _CardNum = N813result.getOccurs().getRow(i).getValue("CARDNUM");
					
					if(_CardNum.length() > 0)
					{
						log.debug("_CardNum ==========================================> " + _CardNum);
						if("1".equals(_CardNum.substring(13,14)))
						{
							strCardNum = _CardNum;
							break;
						}
					}
				}

				params.put("PRIACN", strCardNum);
				_params.put("PRIACN", strCardNum);
				_params.put("PERID", params.get("PERID"));
			}
		}
		else {
			int N810RecordCount = N810result.getValueOccurs("CARDTYPE");
		
		//if(N810RecordCount > 0)
		//{
			String strCardType = "";
			String strCardNum = "";
			
			for(int i = 0; i < N810RecordCount; i++)
			{
				String _CardType = N810result.getOccurs().getRow(i).getValue("CARDTYPE");
				String _CardNum = N810result.getOccurs().getRow(i).getValue("CARDNUM");
				
				if(_CardType.length() > 0 && _CardNum.length() > 0)
				{
					log.debug("_CardType ==========================================> " + _CardType);
					log.debug("_CardNum ==========================================> " + _CardNum);
					if(!"6".equals(_CardType.substring(0,1)) && "1".equals(_CardNum.substring(13,14)))
					{
						strCardType = _CardType;
						strCardNum = _CardNum;
						break;
					}
				}
			}

			params.put("PRIACN", strCardNum);
			_params.put("PRIACN", strCardNum);
			_params.put("PERID", params.get("PERID"));
		}
			/*
		}
		else
		{
			log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Do n813Telcomm");
			
			TelcommResult N813result = n813Telcomm.query(params);
			
			int N813RecordCount = N813result.getValueOccurs("CARDTYPE");
			
			if(N813RecordCount > 0)
			{
				String strCardType = "";
				String strCardNum = "";
				
				for(int i = 0; i < N813RecordCount; i++)
				{
					String _CardType = N813result.getOccurs().getRow(i).getValue("CARDTYPE");
					String _CardNum = N813result.getOccurs().getRow(i).getValue("CARDNUM");
					
					if(_CardType.length() > 0 && _CardNum.length() > 0)
					{
						log.debug("_CardType ==========================================> " + _CardType);
						log.debug("_CardNum ==========================================> " + _CardNum);
						if(!"6".equals(_CardType.substring(0,1)) && "1".equals(_CardNum.substring(13,14)))
						{
							strCardType = _CardType;
							strCardNum = _CardNum;
							break;
						}
					}
				}

				_params.put("PRIACN", strCardNum);
				_params.put("PERID", params.get("PERID"));
			}
		}*/
		}
		catch(Exception n810ex)
		{
			log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Do n813Telcomm");
			
			TelcommResult N813result = n813Telcomm.query(params);
			
			int N813RecordCount = N813result.getValueOccurs("CARDNUM");
			
			if(N813RecordCount > 0)
			{
				String strCardNum = "";
				
				for(int i = 0; i < N813RecordCount; i++)
				{
					String _CardNum = N813result.getOccurs().getRow(i).getValue("CARDNUM");
					
					if(_CardNum.length() > 0)
					{
						log.debug("_CardNum ==========================================> " + _CardNum);
						if("1".equals(_CardNum.substring(13,14)))
						{
							strCardNum = _CardNum;
							break;
						}
					}
				}

				params.put("PRIACN", strCardNum);
				_params.put("PRIACN", strCardNum);
				_params.put("PERID", params.get("PERID"));
			}
		}
		
//		TelcommResult result = c900Telcomm.query(_params);

		//若卡號為空，要補16個9
		if(params.get("PRIACN").equals(null) || params.get("PRIACN").equals("")) {
			params.put("PRIACN", "9999999999999999");
		}
		
		MVHImpl resulTelcomm = (MVHImpl) c900.doAction(params);
//		MVHImpl resulTelcomm =null;
		return resulTelcomm;
	}

	@SuppressWarnings("unchecked")
	public MVH doN900_2(Map<String, String> params) { 
		log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Do N900_2");
//		TXNTWSCHEDULE txntwschedule = new TXNTWSCHEDULE();//無此table

		MVHImpl result = new MVHImpl();
		
		String strZIP = params.get("newZIP").toString();
		String strADDRESS = params.get("newADDR").toString();
		String strHOMEPHONE = params.get("newHOMEPHONE").toString();
		String strOFFICEPHONE = params.get("newOFFICEPHONE").toString();
		String strMOBILEPHONE = params.get("newMOBILEPHONE").toString();
		String strKIND = params.get("FGTXWAY").toString();
		String strCHIP_ACN = params.get("ACNNO").toString();
		
		params.put("POSTCOD", strZIP);
		params.put("ADDRESS", strADDRESS);
		params.put("HOME_TEL", strHOMEPHONE);
		params.put("OFFICE_TEL", strOFFICEPHONE);
		params.put("MOBILE_TEL", strMOBILEPHONE);
		if(strKIND.equals("7"))params.put("KIND", "4");//新增蓋特交易機制
		else {params.put("KIND", strKIND);}
		params.put("CHIP_ACN", strCHIP_ACN);
			
		final TelCommExecWrapper execwrapper = new TelCommExecWrapper(n900ExecTelcomm);
		result = execwrapper.query(params);
		//result = n900ExecTelcomm.query(params);
		
		if(params.containsKey("pkcs7Sign") && "1".equals(params.get("FGTXWAY"))) {
			List<String> commands = new ArrayList();
			commands.add("XMLCA()");
			commands.add("XMLCN()");
			
			CommandUtils.doBefore(commands, params);
			
			if(!StrUtils.trim(params.get("XMLCN")).toUpperCase().startsWith(StrUtils.trim(params.get("UID")).toUpperCase())) {
				throw TopMessageException.create("Z089");
			}
			//無此table
//			txntwschedule.setXMLCA(StrUtils.trim(params.get("XMLCA")));
//			txntwschedule.setXMLCN(StrUtils.trim(params.get("XMLCN")));
		}
		
		//無此table
//		TXNREQINFO titainfo = new TXNREQINFO();
//		titainfo.setREQINFO(BookingUtils.requestInfo(params));
//		txnReqInfoDao.save(titainfo);
		/*
		MVHImpl result =  new MVHImpl();
		
		try {
			result.getFlatValues();
		}
		catch(Exception e){
			logger.error(e);			
		}
		*/

		return result;
	}

	
}
