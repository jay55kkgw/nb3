package fstop.services.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.rest.model.CommonPools;
import com.netbank.util.ESAPIUtil;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.TXNUSER;
import fstop.services.DispatchService;
import fstop.services.ServiceBindingException;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 變更線上更改通訊地址、電話
 * @author Owner
 *
 */
@Slf4j
public class N100 extends DispatchService {
//	private Logger logger = Logger.getLogger(getClass());
	
//	private CommonService n100;

	@Autowired
    @Qualifier("n200Telcomm")
	private TelCommExec n200Telcomm;

	@Autowired
    @Qualifier("n100ExecTelcomm")
	private TelCommExec n100ExecTelcomm;
	
	@Autowired
    @Qualifier("c113Telcomm")
	private TelCommExec c113Telcomm;
	
	@Autowired
	private CommonPools commonPools;
	
	@Autowired
	private TxnUserDao txnUserDao;
	
//	private TxnTwRecordDao txnTwRecordDao;
//	
//	private TxnReqInfoDao txnReqInfoDao;
	
//	@Required
//	public void setTxnReqInfoDao(TxnReqInfoDao txnReqInfoDao) {
//		this.txnReqInfoDao = txnReqInfoDao;
//	}
//	
//	@Required
//	public void setN100(CommonService service) {
//		n100 = service;
//	}
//	
//	@Required
//	public void setN200Telcomm(TelCommExec telcomm) {
//		n200Telcomm = telcomm;
//	}
//	@Required
//	public void setN100ExecTelcomm(TelCommExec telcomm) {
//		n100ExecTelcomm = telcomm;
//	}
//	
//	@Required
//	public void setTxnTwRecordDao(TxnTwRecordDao txnTwRecordDao) {
//		this.txnTwRecordDao = txnTwRecordDao;
//	}
	
	@Override
	public String getActionMethod(Map<String, String> params) {
		String txid = StrUtils.trim(params.get("TXID"));
		log.debug(ESAPIUtil.vaildLog("XXXX txid: " + txid));
		if("N100_1".equals(txid))
			return "doN100_1";
		else if("N100_2".equals(txid))
			return "doN100_2";
		else 
			throw new ServiceBindingException("不支援的 TXID [" + txid+"].");
	}
	@SuppressWarnings("unchecked")
	public MVH doN100_1(Map params) { 
		log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Do N100_1");
		
		TelcommResult result = n200Telcomm.query(params);
		
		return result;
	}

	@SuppressWarnings("unchecked")
	public MVH doN100_2(Map params) { 
		log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Do N100_2");
		//final Hashtable<String, String> params = _params;
		log.debug("Create Date =======================================================> Start");
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);
		String strUpdateData = params.get("SelectedRecord").toString();
		log.debug("SelectedRecord ============================================= " + strUpdateData);
		String[] arrUpdateData = strUpdateData.split("\\|");
		String strBaseData = params.get("ModifyData").toString();		
		//=========idgate判斷
		if("7".equals(((String)params.get("FGTXWAY")).trim())) {
			params.put("KIND","4");
		}else {
			params.put("KIND", (String)params.get("FGTXWAY"));
		}
		//=========
		int ICSEQ_NO = 0;
		log.debug("FGTXWAY ============================================= '" + params.get("FGTXWAY") + "'");
		log.debug("iSeqNo ============================================= " + params.get("iSeqNo"));
		log.debug("FGTXWAY == 2 ============================================= " + ("2".equals(((String)params.get("FGTXWAY")).trim())));
		if("2".equals(((String)params.get("FGTXWAY")).trim()))ICSEQ_NO = Integer.parseInt(params.get("iSeqNo").toString());
		
		MVHImpl result = new MVHImpl();
		Hashtable<String, String> HashResult = new Hashtable();

		String TotalResult = "Success";
		String ProcessList = "";

		//異動全行往來資料迴圈移除只取一筆資料打一次電文
//		for(int i = 0; i < arrUpdateData.length; i++)
//		{
			log.debug("ICSEQ_NO > 0 ============================================= " + (ICSEQ_NO > 0));
			String strICSEQ_NO = ICSEQ_NO > 0 ? String.format("%1$08d", new Object[]{ICSEQ_NO}) : "";
			log.debug("strICSEQ_NO ============================================= " + strICSEQ_NO);
			
//			String[] arrOriData = arrUpdateData[i].split(",", -1);
			String[] arrOriData = arrUpdateData[0].split(",", -1);
			String[] arrNewData = strBaseData.split(",", -1);
			String[] arrBRHCOD = arrOriData[0].split("_", -1);
			
			String newPOSTCOD2 = arrNewData[0].trim();
			String newCTTADR = arrNewData[1].trim();
			String newARACOD = arrNewData[2].trim();
			String newTELNUM = arrNewData[3].trim();
			String newARACOD2 = arrNewData[4].trim();
			String newTELNUM2 = arrNewData[5].trim();
			newTELNUM2 = newTELNUM2.trim().length() > 1 && !"#".equals(newTELNUM2.trim()) ? newTELNUM2 : "";
			String newMOBTEL = arrNewData[6].trim();
			log.debug("BRHCOD =============================================> " + arrBRHCOD[0]);
			
			params.put("BRHCOD", arrBRHCOD[0]);
			params.put("POSTCOD2", newPOSTCOD2);
			params.put("CTTADR", newCTTADR);
			params.put("ARACOD", newARACOD);
			params.put("TELNUM", newTELNUM);
			params.put("ARACOD2", newARACOD2);
			params.put("TELNUM2", newTELNUM2);
			params.put("MOBTEL", newMOBTEL);
			params.put("ICSEQ", strICSEQ_NO);
			params.put("BNKRA", "050");
			//final TelCommExecWrapper execwrapper = new TelCommExecWrapper(n100ExecTelcomm);
			//MVHImpl InnerResult = execwrapper.query(params);
			result = n100ExecTelcomm.query(params);
			//log.debug(InnerResult.dumpToString());
			//log.debug("RSPCOD query result ==================================================> " + InnerResult.getFlatValues().get("RSPCOD"));
			
			if(result.getValueByFieldName("RSPCOD").length() == 0)
			{
				if(ProcessList.length() > 0)ProcessList += ",";
				ProcessList += result.getValueByFieldName("RSPCOD");

				try
				{
					if(result.getValueByFieldName("RSPCOD").length() > 0)TotalResult = "Fail";
				}
				catch(Exception UpdateResultEx)
				{
					TotalResult = "Fail";
				}
			
				log.debug("Result is null =============================================> " + (result == null ? "Yes" : "No"));
			}
//			ICSEQ_NO++;
//		}
		
		//HashResult.put("Status", TotalResult);
		log.debug("TotalResult =============================================> " + TotalResult);
		result.getFlatValues().put("FinalResult", TotalResult);
		result.getFlatValues().put("ProcessList", ProcessList);
		
		
		
		//20210201 同步2.5 >> 同步更新基金主機臨櫃申請狀態
		
		TXNUSER txnuser = new TXNUSER();
		List<TXNUSER> listtxnUser = txnUserDao.findByUserId((String)params.get("CUSIDN"));
		if(listtxnUser==null || listtxnUser.size()==0)
		{
			log.error(ESAPIUtil.vaildLog("User ID not found = {}"+ params.get("UID")));
		}
		else
		{
			 txnuser=listtxnUser.get(0);
		}
		
		Map c113param = new HashMap();
		c113param.put("FGTXWAY", (String)params.get("FGTXWAY"));	
		c113param.put("PINNEW", (String)params.get("PINNEW"));
		c113param.put("CUSIDN", (String)params.get("CUSIDN"));
		c113param.put("BILLSENDMODE", txnuser.getFUNDBILLSENDMODE());
		
		commonPools.n927.removeGetDataCache((String)params.get("CUSIDN"));
		TelcommResult n927 = (TelcommResult)commonPools.n927.getData((String)params.get("CUSIDN"));				
		String aplbrh = n927.getValueByFieldName("APLBRH");
		c113param.put("BRHCOD", aplbrh);
			
		String HTELPHONE = (String)params.get("ARACOD2")+"-"+(String)params.get("TELNUM2");
		HTELPHONE = HTELPHONE.length()>16 ? HTELPHONE.substring(0,16) : HTELPHONE;
		String OTELPHONE = (String)params.get("ARACOD")+"-"+(String)params.get("TELNUM");
		OTELPHONE = OTELPHONE.length()>16 ? OTELPHONE.substring(0,16) : OTELPHONE;
		c113param.put("ZIPCODE", (String)params.get("POSTCOD2"));						
		c113param.put("HTELPHONE", HTELPHONE);			
		c113param.put("OTELPHONE", OTELPHONE);			
		c113param.put("MTELPHONE", (String)params.get("MOBTEL"));			
		c113param.put("EMAIL", (String)params.get("EMAIL"));			
		c113param.put("ADDRESS", (String)params.get("CTTADR"));
		c113param.put("IP", (String)params.get("IP"));
		
		try {
			TelcommResult result113 = c113Telcomm.query(c113param);
			log.warn("N100_C113_tel MSGCOD:"+result113.getValueByFieldName("RESPCOD"));
		}catch(Exception e) {
			log.warn("N100_C113_tel Exception:"+e.getMessage());
		}
		
		
		return result;
	}
}
