package fstop.services.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;


import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 網路銀行交易密碼線上解鎖
 * @author Owner
 *
 */
@Slf4j
public class NA71 extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("na70Telcomm")
	private TelCommExec na70Telcomm;
	
//	@Required
//	public void setNa70Telcomm(TelCommExec telcomm) {
//		na70Telcomm = telcomm;
//	}
	
	/**
	 * 網路銀行交易密碼線上解鎖
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params) {
		final Map<String, String> params = _params;
		
		MVHImpl result = new MVHImpl();
		
		String uid = params.get("UID").toString();
		String fgtxway = params.get("FGTXWAY").toString();
		
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		Integer icseq = 0;
		params.put("DATE", df);
		params.put("TIME", tf);
		
		if (fgtxway.equals("1"))
		{   //電子簽章
			params.put("KIND", "1");
		}
		else if(fgtxway.equals("7")) {
			params.put("KIND", "3");
		}
		else
		{   //晶片金融卡			
			params.put("KIND", "2");			
			
			params.put("CHIP_ACN", params.get("CHIP_ACN").trim());
			params.put("ICSEQ", params.get("ICSEQ"));
		}
		
		params.put("CUSIDN", uid);
		params.put("TYPE", "2");  //2.	簽入密碼解鎖
		
		result = na70Telcomm.query(params);
		
		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));		
		
		return result;
	}
}
