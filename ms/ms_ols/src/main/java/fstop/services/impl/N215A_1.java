package fstop.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.TelcommResult;
import fstop.orm.dao.TxnTrAccSetDao;
import fstop.orm.po.TXNTRACCSET;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.telcomm.cmd.CommandUtils;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class N215A_1 extends CommonService  {
	@Autowired
	private TxnTrAccSetDao txnTrAccSetDao;	
	@Autowired
	@Qualifier("n215Telcomm")
	private TelCommExec n215Telcomm;
	
//	@Required
//	public void setN215Telcomm(TelCommExec telcomm) {
//		n215Telcomm = telcomm;
//	}
//	@Required
//	public void setTxnTrAccSetDao(TxnTrAccSetDao dao) {
//		this.txnTrAccSetDao = dao;
//	}	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params) { 
		Map<String, String> params = _params;
		//申請
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);
		params.put("CUSIDN", (String)params.get("UID"));
		
		if("7".equals((String)params.get("FGTXWAY"))) {
			params.put("KIND", "4");	
		}else {
			params.put("KIND", (String)params.get("FGTXWAY"));	
		}
		
		params.put("TYPE", (String)params.get("TYPE"));	
		params.put("COUNT", (String)params.get("COUNT"));
		params.put("ACNINFO", (String)params.get("ACNINFO"));
		params.put("BNKRA", "050");
		if(params.containsKey("pkcs7Sign") && "1".equals(params.get("FGTXWAY"))) {
			List<String> commands = new ArrayList();
			commands.add("XMLCA()");
			commands.add("XMLCN()");
			
			CommandUtils.doBefore(commands, params);
			
			if(!StrUtils.trim(params.get("XMLCN")).toUpperCase().startsWith(StrUtils.trim(params.get("UID")).toUpperCase())) {
				throw TopMessageException.create("Z089");
			}
		}
		int COUNT=Integer.parseInt(params.get("COUNT"));
		String SelectedRecord=(String)params.get("SelectedRecord");
		String SelectedRecord1=(String)params.get("SelectedRecord1");
		String[] BANK = new String[COUNT];
		String[] ACN = new String[COUNT];
		int position1=0;
		int position2=0;
		for(int i=0;i<COUNT;i++)
		{								
			position2=position1+19;			
			BANK[i]= (SelectedRecord.substring(position1,position2)).substring(0,3);
			ACN[i]=	(SelectedRecord.substring(position1,position2)).substring(3).trim();
			position1=position2;				
		}
		log.debug("N215A_1.java COUNT:"+COUNT+ " ACNINFO length:"+params.get("ACNINFO").toString().length());
		log.debug(ESAPIUtil.vaildLog("N215A_1.java ACNINFO:"+params.get("ACNINFO").toString()));
		TelcommResult telcommResult = n215Telcomm.query(params);
		//Table修改過
//	    if(telcommResult.getValueByFieldName("MSGCOD").equals("0000") || telcommResult.getValueByFieldName("MSGCOD").equals("    "))
//	    {
//			for(int i=0;i<COUNT;i++)
//			{
//				if(txnTrAccSetDao.findDptrdacnoIsExists((String)params.get("UID"),BANK[i], ACN[i]))
//				{
//					List<TXNTRACCSET> r = txnTrAccSetDao.findByDPGONAME((String)params.get("UID"),BANK[i], ACN[i]);
//					TXNTRACCSET r1 = r.get(0);            
//					int DPACCSETID=r1.getDPACCSETID();					
//					log.debug("N215A_1.java UID BANK ACN:"+(String)params.get("UID")+" "+BANK[i]+" "+ACN[i]);
//					log.debug("N215A_1.java check txntraccset DPACCSETID"+i+":"+DPACCSETID);
//					TXNTRACCSET txntraccset = txnTrAccSetDao.findById(DPACCSETID);
//					txntraccset.setDPUSERID((String)params.get("UID"));
//					txntraccset.setDPGONAME((String)params.get("DPACGONAME"+i));
//					txntraccset.setDPTRACNO("1");
//					txntraccset.setDPTRIBANK(BANK[i]);
//					txntraccset.setDPTRDACNO(ACN[i]);			
//					d= new Date();
//					txntraccset.setLASTDATE(DateTimeUtils.format("yyyyMMdd", d));
//					txntraccset.setLASTTIME(DateTimeUtils.format("HHmmss", d));
//					txnTrAccSetDao.save(txntraccset);
//				}
//				else
//				{
//					TXNTRACCSET txntraccset = new TXNTRACCSET();
//					txntraccset.setDPUSERID((String)params.get("UID"));
//					txntraccset.setDPGONAME((String)params.get("DPACGONAME"+i));
//					txntraccset.setDPTRACNO("1");
//					txntraccset.setDPTRIBANK(BANK[i]);
//					txntraccset.setDPTRDACNO(ACN[i]);				
//					d = new Date();
//					txntraccset.setLASTDATE(DateTimeUtils.format("yyyyMMdd", d));
//					txntraccset.setLASTTIME(DateTimeUtils.format("HHmmss", d));
//					txnTrAccSetDao.save(txntraccset);				
//				}				
//			}
//	    }
	    telcommResult.getFlatValues().put("SelectedRecord1", SelectedRecord1);
	    telcommResult.getFlatValues().put("COUNT", String.valueOf(COUNT));
	    return telcommResult;
	}
}
