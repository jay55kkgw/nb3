package fstop.services.impl;

import java.util.Date;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 金融卡線上申請/取消轉帳功能
 * @author Owner
 *
 */
@Slf4j
public class NA80 extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
    @Qualifier("na80Telcomm")
	private TelCommExec na80Telcomm;
	
//	@Required
//	public void setNa80Telcomm(TelCommExec telcomm) {
//		na80Telcomm = telcomm;
//	}
	
	/**
	 * 金融卡線上申請/取消轉帳功能
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params) {
		final Map<String, String> params = _params;
		
		MVHImpl result = new MVHImpl();
		
		String uid = params.get("UID").toString();
		
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		
		params.put("DATE", df);
		params.put("TIME", tf);		
		params.put("ACN", params.get("ACN").trim());
		params.put("ICSEQ", params.get("ICSEQ"));		
		params.put("CUSIDN", uid);
		params.put("TYPE", params.get("TYPE"));  //01:申請   02:取消
		
		result = na80Telcomm.query(params);
		
		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));		
		
		return result;
	}
}
