package fstop.services.impl;

import java.io.File;
import java.util.*;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;
import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.services.CommonService;
import fstop.services.batch.CheckServerHealth;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
//
//import fstop.util.FileUtils;
//TODO:FtpBase64的使用有待商確
//import fstop.util.FtpBase64;
import fstop.util.StrUtils;
import fstop.ws.AMLWebServiceTemplate;
import fstop.ws.aml.bean.AML001RQ;
import fstop.ws.aml.bean.AML001RQ.SearchNamesSoap;
import fstop.ws.aml.bean.AML001RQ.SearchNamesSoap.Branch;
import fstop.ws.aml.bean.AML001RQ.SearchNamesSoap.DateOfBirth;
import fstop.ws.aml.bean.AML001RQ.SearchNamesSoap.Names;
import fstop.ws.aml.client.SearchResponseSoap;
import lombok.extern.slf4j.Slf4j;
//import fstop.util.RuntimeExec;
import fstop.notifier.NotifyAngent;
import fstop.orm.dao.TxnLogDao;

import com.netbank.rest.model.JSPUtils;
import com.netbank.util.ESAPIUtil;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class N201 extends CommonService  {
	@Autowired
    @Qualifier("n201Telcomm")
	private TelCommExec n201Telcomm;
	@Autowired
    @Qualifier("n202Telcomm")
	private TelCommExec n202Telcomm;
	@Autowired
	private TxnLogDao txnLogDao;
//	private Logger logger = Logger.getLogger(N201.class);
	private Map ordersetting;

	@Autowired
	private AMLWebServiceTemplate amlWebServiceTemplate;
//	@Required
//	public void setN201Telcomm(TelCommExec telcomm) {
//		n201Telcomm = telcomm;
//	}	
//	@Required
//	public void setN202Telcomm(TelCommExec telcomm) {
//		n202Telcomm = telcomm;
//	}
//	
	public void setOrdersetting(Map ordersetting) {
		this.ordersetting = ordersetting;
	}	
//	
//	@Required
//	public void setTxnLogDao(TxnLogDao dao) {
//		this.txnLogDao = dao;
//	}
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params) {
		//logger.debug("N201.java come in");
		Map<String, String> params = _params;
		//查詢
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);
		String NAME=JSPUtils.convertFullorHalf((String)params.get("NAME"), 1);
		String PMTADR=JSPUtils.convertFullorHalf((String)params.get("PMTADR"), 1);
		String CTTADR=JSPUtils.convertFullorHalf((String)params.get("CTTADR"), 1);
		params.put("IBHPWD", params.get("IBHPW"));
		params.put("CUSIDN", (String)params.get("UID"));
		params.put("NAME", NAME);
		params.put("PMTADR", PMTADR); 
		params.put("CTTADR", CTTADR);
		params.put("BRHCOD", (String)params.get("BRHCOD"));
		String occurMsg="";
		MVHImpl telcommResult = new MVHImpl();
		String HAS_RISK   = (String) params.get("HAS_RISK")==null ? "" : (String) params.get("HAS_RISK");
		try {
			_params.put("ADOPID", "N201");
			HAS_RISK = amlWebServiceTemplate.callAML(_params) ? "Y": "N";
			log.debug("HAS_RISK >> {}", HAS_RISK);
			params.put("HAS_RISK", HAS_RISK);
		}catch(Exception e){
			log.error(e.toString());
		}
		String ACN ="";
		String DUEDT="";
//		String pathname = (String)ordersetting.get("pathname");				
//		String uploadpath = (String) params.get("UPLOADPATH");
//		if(!StrUtils.trim(pathname).endsWith("/"))
//			pathname = StrUtils.trim(pathname) + "/";
//		if(!StrUtils.trim(uploadpath).endsWith("/"))
//			uploadpath = StrUtils.trim(uploadpath) + "/";
//		String FILE1 = (String) params.get("FILE1");
//		String FILE2 = (String) params.get("FILE2");
//		String FILE3 = (String) params.get("FILE3");				
//		String FILE4 = (String) params.get("FILE4");				

		// 20210331 調整流程若為起家金帳戶不得線上申請晶片金融卡 則不呼叫N202取得帳號 在呼叫N201時也無須傳入 ACN,判斷STAEDYN =Y
		String STAEDYN = (String)params.get("STAEDYN");
		String STAEDSQ = StringUtils.isNotBlank((String)params.get("STAEDSQ")) ? fillSTAEDSQforN201Telc((String)params.get("STAEDSQ")) : fillSTAEDSQforN201Telc(StringUtils.EMPTY);
		log.info("STAEDYN >> {} , STAEDSQ >> {}", STAEDYN, STAEDSQ);
		try {
			if(StringUtils.isBlank(STAEDYN) || StringUtils.equalsAnyIgnoreCase(STAEDYN , "N")){
				log.info("Start Call N202...");
				telcommResult = n202Telcomm.query(params);
				occurMsg=telcommResult.getValueByFieldName("MSGCOD");
				telcommResult.getFlatValues().put("occurMsg", occurMsg);
				params.put("STAEDYN","N");
				params.put("STAEDSQ","00000");
			} else if(StringUtils.isNotBlank(STAEDYN) && StringUtils.equalsAnyIgnoreCase(STAEDYN , "Y")) {
				params.put("STAEDYN",STAEDYN);
				params.put("STAEDSQ",STAEDSQ);
				params.put("ODFTYN","N"); //防止由頁面串改資料 強制改為不申請質借
			}
		    if(occurMsg.equals("0000") || occurMsg.equals("    ") || StringUtils.equalsAnyIgnoreCase(STAEDYN, "Y"))
		    {
				ACN = telcommResult.getValueByFieldName("AXACN");
		    	if(StringUtils.isNotBlank(ACN))params.put("ACN",ACN); // 20210331 調整流程若為起家金帳戶不得線上申請晶片金融卡 則不呼叫N202取得帳號 在呼叫N201時也無須傳入 ACN,判斷STAEDYN =Y
		    	params.put("DEGREE",((String) params.get("DEGREE")).equals("#") ? "" : ((String) params.get("DEGREE")));
		    	params.put("MARRY", ((String) params.get("MARRY")).equals("#") ? "" : ((String) params.get("MARRY")));
		    	log.warn("N202 get ACN:"+ACN);
		    	log.warn(ESAPIUtil.vaildLog("N201 UP params:"+params)); 
		    	try{
			    	telcommResult = n201Telcomm.query(params);
			    	occurMsg=telcommResult.getValueByFieldName("MSGCOD");
					telcommResult.getFlatValues().put("occurMsg", occurMsg);
			    	log.warn("N201 get DUEDT:"+telcommResult.getValueByFieldName("DUEDT"));			 
			    	if(occurMsg.equals("0000") || occurMsg.equals("    "))
			    	{
			    		//發送Email通知
			    		String mail  = (String)params.get("MAILADDR");
			    		DUEDT = telcommResult.getValueByFieldName("DUEDT");
			    		Hashtable<String, String> varmail = new Hashtable();
			    		varmail.put("SUB","完成線上預約開戶通知");
			    		varmail.put("MAILTEXT", getContent(DUEDT));
						if(!NotifyAngent.sendNotice("N201",varmail,mail)){
							log.warn(ESAPIUtil.vaildLog("線上預約開戶通知發送Email失敗.(mail:"+mail+")"));
						}
						//TODO:將FTP作業移至web service
//						//FTP作業
//						if(FILE1.length()>0)
//							ftpFileToETABS1(pathname + ((String)params.get("UID")).toUpperCase()+"_01"+(FILE1.substring(FILE1.lastIndexOf(".jpg"), FILE1.length())).toLowerCase(),uploadpath + FILE1);
//							//ftpFileToETABS(uploadpath + FILE1,uploadpath +((String)params.get("UID")).toUpperCase()+"_01"+(FILE1.substring(FILE1.indexOf("."), FILE1.length())).toLowerCase());
//						if(FILE2.length()>0)
//							ftpFileToETABS1(pathname + ((String)params.get("UID")).toUpperCase()+"_02"+(FILE2.substring(FILE2.lastIndexOf(".jpg"), FILE2.length())).toLowerCase(),uploadpath + FILE2);							
//							//ftpFileToETABS(uploadpath + FILE2,uploadpath +((String)params.get("UID")).toUpperCase()+"_02"+(FILE2.substring(FILE2.indexOf("."), FILE2.length())).toLowerCase());
//						if(FILE3.length()>0)
//							ftpFileToETABS1(pathname + ((String)params.get("UID")).toUpperCase()+"_03"+(FILE3.substring(FILE3.lastIndexOf(".jpg"), FILE3.length())).toLowerCase(),uploadpath + FILE3);							
//							//ftpFileToETABS(uploadpath + FILE3,uploadpath +((String)params.get("UID")).toUpperCase()+"_03"+(FILE3.substring(FILE3.indexOf("."), FILE3.length())).toLowerCase());
//						if(FILE4.length()>0)
//							ftpFileToETABS1(pathname + ((String)params.get("UID")).toUpperCase()+"_04"+(FILE4.substring(FILE4.lastIndexOf(".jpg"), FILE4.length())).toLowerCase(),uploadpath + FILE4);							
//							//ftpFileToETABS(uploadpath + FILE4,uploadpath +((String)params.get("UID")).toUpperCase()+"_04"+(FILE4.substring(FILE4.indexOf("."), FILE4.length())).toLowerCase());
					    telcommResult.getFlatValues().put("ACN", ACN);
					    telcommResult.getFlatValues().put("DUEDT", DUEDT);					
			    	}		    		
		    	}catch (TopMessageException e) {
					occurMsg = e.getMsgcode();
					telcommResult.getFlatValues().put("occurMsg", occurMsg);
					telcommResult.getFlatValues().put("TOPMSG", occurMsg);
					//TODO:將FTP作業移至web service
//					if(removefile(uploadpath + FILE1)) log.debug("remove pic1 successful");
//					else log.warn("remove pic1 fail");
//					if(removefile(uploadpath + FILE2)) log.debug("remove pic2 successful");
//					else log.warn("remove pic2 fail");
//					if(removefile(uploadpath + FILE3)) log.debug("remove pic3 successful");					
//					else log.warn("remove pic3 fail");
//					if(removefile(uploadpath + FILE4)) log.debug("remove pic4 successful");					
//					else log.warn("remove pic4 fail");
				}	
		    }
		} catch (TopMessageException e) {
			occurMsg = e.getMsgcode();
			telcommResult.getFlatValues().put("occurMsg", occurMsg);
			//TODO:將FTP作業移至web service
//			if(removefile(uploadpath + FILE1)) log.debug("remove pic1 successful");
//			else log.warn("remove pic1 fail");
//			if(removefile(uploadpath + FILE2)) log.debug("remove pic2 successful");
//			else log.warn("remove pic2 fail");
//			if(removefile(uploadpath + FILE3)) log.debug("remove pic3 successful");					
//			else log.warn("remove pic3 fail");
//			if(removefile(uploadpath + FILE4)) log.debug("remove pic4 successful");					
//			else log.warn("remove pic4 fail");
		}
	    return telcommResult;
	}

	@SuppressWarnings("unused")
	private String getContent(String DUEDT){

		String dt = DUEDT.substring(0,3)+"/"+DUEDT.substring(3,5)+"/"+DUEDT.substring(5,7);
		Calendar c1 = Calendar.getInstance();
		c1.set(Integer.parseInt(DUEDT.substring(0,3))+1911,Integer.parseInt(DUEDT.substring(3,5))-1,Integer.parseInt(DUEDT.substring(5,7)));
		int day = c1.get(Calendar.DAY_OF_WEEK);
		String dayofweek="";
	    switch (day) {
	          	case 1:  dayofweek = "日";
	                     break;
	            case 2:  dayofweek = "一";
	                     break;
	            case 3:  dayofweek = "二";
	                     break;
	            case 4:  dayofweek = "三";
	                     break;
	            case 5:  dayofweek = "四";
	                     break;
	            case 6:  dayofweek = "五";
	                     break;
	            case 7:  dayofweek = "六";
	                     break;
	            default: dayofweek = "";
	                     break;
	    }	
		StringBuilder sb = new StringBuilder ();
		sb.append("<table cellSpacing=0 cellPadding=0 width=600 border=0 frame=border >").append("\n");
		sb.append("<tbody>").append("\n");
		sb.append("<tr><td align=left>親愛的客戶您好：</td></tr></br>").append("\n");
		sb.append("<tr><td align=left>恭喜您完成本行『預約開戶服務』，請親自攜帶印鑑、身分證、第二證件(如健保卡、</td></tr>").append("\n");
		sb.append("<tr><td align=left>駕照等)及開戶起存額壹仟元於營業時間至指定分行辦理開戶，為節省您開戶作業時間</td></tr>").append("\n");
		sb.append("<tr><td align=left>，建議您可列印本預約成功頁面。提醒您!若於<font color=red><b>『").append(dt).append("(星期").append(dayofweek).append(")』</b></font>前未來辦理開戶</td></tr>").append("\n");
		sb.append("<tr><td align=left>，本行將取消您的預約開戶資料，請您重新辦理。</td></tr>").append("\n\n");
		sb.append("<tr><td align=left>提醒您『本行備有專屬起家金悠遊Debit卡、請至往來分行服務台申辦。』</td></tr>").append("\n\n");
		sb.append("<tr><td align=left><BR><BR>祝您，事業順心，萬事如意!</td></tr>").append("\n");
		sb.append("<tr><td align=left><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;臺灣企銀&nbsp;&nbsp;&nbsp;敬上</td></tr>").append("\n");
		sb.append("</tbody>").append("\n");
		sb.append("</table>").append("\n");
		return sb.toString();
	}
	public boolean removefile(String localPath) {
		try
		{
			File DelFile = new File(localPath);
			DelFile.setWritable(true,true);
			DelFile.setReadable(true, true);
			DelFile.setExecutable(true,true);
			DelFile.delete();
			log.warn("刪除圖檔成功:"+localPath);
			return(true);
		}
		catch(Exception RemoveFileEx)
		{
			log.warn("刪除圖檔失敗"+localPath);
			return(false);
		}		
	}
	/*
	public boolean ftpFileToETABS(String localPath,String newfilename) {
			//boolean isSuccess = false;
			logger.info("@FTP@ " + " " + localPath );
			String tmp = "/TBB/nnb/applyfile";
					try
					{
						File DelFile = new File(localPath);
						File NewFile = new File(newfilename);
						if(DelFile.renameTo(NewFile))
							logger.debug(localPath+" rename to file:"+NewFile.getName()+" successful");
						else
							logger.debug(localPath+" rename to file:"+NewFile.getName()+" fail");							
						FileUtils.copyFileTo(NewFile, tmp);
				    	String localIP = IPUtils.getLocalIp();
				    	RuntimeExec runtimeExec=null;
				    	if(localIP.equals("10.16.22.17"))
				    		runtimeExec=new RuntimeExec(tmp,"chown -R apt003:apgrp "+tmp+"/");
				    	else
				    		runtimeExec=new RuntimeExec(tmp,"chown -R dcp001:dcpgrp "+tmp+"/");				    		
				    	if(runtimeExec.getcommstat()!=0)
						{
							logger.debug("chown  exec command error");
						}						
						NewFile.delete();
					}
					catch(Exception RemoveFileEx)
					{
						logger.debug(localPath+" rename to file:"+newfilename+" fail "+RemoveFileEx.getMessage());							
					}	
					return(true);				
	}
	*/
	public boolean ftpFileToETABS1(String eloanPath, String localPath) {
		//TODO:因FtpBase64使用有待商確，故先行註解
//		logger.info("@FTP@ " + " " + localPath + " " + eloanPath);
//    	String localIP = IPUtils.getLocalIp();
//		String ip = (String)ordersetting.get("ftp.ip");
//		String tmp = "/TBB/nnb/applyfile";
//		FtpBase64 ftpBase64 = new FtpBase64(ip);
//		int rc=ftpBase64.upload(eloanPath, localPath);
//		if(rc!=0)
//		{
//			log.warn("FTP 失敗(ETABS 主機一次) FILE："+eloanPath);
//			CheckServerHealth checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
//			checkserverhealth.sendUnknowNotice("FTP 失敗(ETABS 主機一次) FILE："+eloanPath);
//			rc=ftpBase64.upload(eloanPath, localPath);
//			if(rc!=0)
//			{	
//				log.warn("FTP 失敗(ETABS 主機二次) FILE："+eloanPath);
//				checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
//				checkserverhealth.sendUnknowNotice("FTP 失敗(ETABS 主機二次) FILE："+eloanPath);
//				return(false);
//			}
//			else
//			{	
//				try
//				{
//					File DelFile = new File(localPath);
//					FileUtils.copyFileTo(DelFile, tmp);
//				    RuntimeExec runtimeExec=null;
//				    if(localIP.equals("10.16.22.17"))
//				    	runtimeExec=new RuntimeExec(tmp,"chown -R apt003:apgrp "+tmp+"/");
//				    else
//				    	runtimeExec=new RuntimeExec(tmp,"chown -R dcp001:dcpgrp "+tmp+"/");				    		
//				    if(runtimeExec.getcommstat()!=0)
//					{
//						logger.warn("chown  exec command error");
//					}																			
//					DelFile.delete();
//					logger.warn("刪除圖檔成功:"+localPath);
//				}
//				catch(Exception e)
//				{
//					logger.warn("複製或刪除圖檔Path:"+localPath);
//					logger.warn("複製或刪除圖檔Fail:"+e.toString());
//				}								
//				return(true);				
//			}
//		}
//		else
//		{
//			try
//			{
//				File DelFile = new File(localPath);
//				FileUtils.copyFileTo(DelFile, tmp);
//			    RuntimeExec runtimeExec=null;
//			    if(localIP.equals("10.16.22.17"))
//			    	runtimeExec=new RuntimeExec(tmp,"chown -R apt003:apgrp "+tmp+"/");
//			    else
//			    	runtimeExec=new RuntimeExec(tmp,"chown -R dcp001:dcpgrp "+tmp+"/");				    		
//			    if(runtimeExec.getcommstat()!=0)
//				{
//					logger.warn("chown  exec command error");
//				}												
//				DelFile.delete();
//				logger.warn("刪除圖檔成功:"+localPath);
//			}
//			catch(Exception e)
//			{
//				logger.warn("複製或刪除圖檔Path:"+localPath);
//				logger.warn("複製或刪除圖檔Fail:"+e.toString());
//			}					
//		}
		return true;
	}	
	public boolean getAmlmsg(Map reqParam) {
		try{
			String name = (String)reqParam.get("NAME");
			String BRTHDY = (String)reqParam.get("BIRTHDAY");
			int yearnum = Integer.parseInt(BRTHDY.substring(0,3))+1911;
			String year = String.valueOf(yearnum);
			String month = BRTHDY.substring(3,5);
			String day = BRTHDY.substring(5,7);
			
			
			AML001RQ rq  = new AML001RQ();
			SearchNamesSoap  searchNamesSoap = new SearchNamesSoap();
			Names names = new Names();
			LinkedList list = new LinkedList<String>();
			list.add(name);
			names.setName(list);
			Branch branch = new Branch();
			branch.setBranchId((String)reqParam.get("BRHCOD"));
			branch.setBusinessUnit("H57");
			branch.setSourceSystem("NNB-W100089655");
			DateOfBirth dateOfBirth = new DateOfBirth();
			dateOfBirth.setYear(year);
			dateOfBirth.setMonth(month);
			dateOfBirth.setDay(day);
			searchNamesSoap.setNames(names);
			searchNamesSoap.setBranch(branch);
			searchNamesSoap.setDateOfBirth(dateOfBirth);
			rq.setMSGNAME("AML001");
			rq.setAPP("XML");
			rq.setCLIENTIP("::1");
			rq.setTYPE("01");
			rq.setSearchNamesSoap(searchNamesSoap);
			
			SearchResponseSoap rs = amlWebServiceTemplate.sendAndReceive (rq);
			return rs.isHit();
		}catch(Exception e) {
			log.debug(e.toString());
		}
		return true;
	}

	private String fillSTAEDSQforN201Telc (String seq) {
		StringBuilder fillStr = new StringBuilder();

		for (int i = 0; i < 5-seq.length(); i++) {
			fillStr.insert(i,"0");
		}
		return fillStr.append(seq).toString();

	}
}
