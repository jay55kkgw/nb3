package fstop.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;


import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.TxnTrAccSetDao;
import fstop.orm.po.TXNTRACCSET;
import fstop.services.CommonService;
//import fstop.telcomm.TelCommExec;
import fstop.telcomm.cmd.CommandUtils;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class N215FM_1 extends CommonService  {
	@Autowired
	private TxnTrAccSetDao txnTrAccSetDao;	
	//private TelCommExec n215Telcomm;
/*	
	@Required
	public void setN215Telcomm(TelCommExec telcomm) {
		n215Telcomm = telcomm;
	}
*/	
//	@Required
//	public void setTxnTrAccSetDao(TxnTrAccSetDao dao) {
//		this.txnTrAccSetDao = dao;
//	}	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params) { 
		Map<String, String> params = _params;
		//查詢
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);
		params.put("CUSIDN", (String)params.get("UID"));
		//params.put("KIND", (String)params.get("FGTXWAY"));	
		//params.put("TYPE", "02");	
		params.put("COUNT", (String)params.get("COUNT"));
		params.put("ACNINFO", (String)params.get("ACNINFO"));
		params.put("BNKRA", "050");
		if(params.containsKey("pkcs7Sign") && "1".equals(params.get("FGTXWAY"))) {
			List<String> commands = new ArrayList();
			commands.add("XMLCA()");
			commands.add("XMLCN()");
			
			CommandUtils.doBefore(commands, params);
			
			if(!StrUtils.trim(params.get("XMLCN")).toUpperCase().startsWith(StrUtils.trim(params.get("UID")).toUpperCase())) {
				throw TopMessageException.create("Z089");
			}
		}
		int COUNT=Integer.parseInt(params.get("COUNT"));
		String SelectedRecord=(String)params.get("SelectedRecord");
		//String SelectedRecord1=(String)params.get("SelectedRecord1");
		log.debug("N215FM_1.java COUNT:"+COUNT+" SelectedRecord.length:"+SelectedRecord.length());
		String BANK= SelectedRecord.substring(0,3);
		String ACN=SelectedRecord.substring(3).trim();
		//log.debug("N215 Step1:Delete UID:"+params.get("UID") + " SelectedRecord:"+SelectedRecord);		
		//TelcommResult telcommResult = null;
		//TelcommResult telcommResult = n215Telcomm.query(params);
	    //if(telcommResult.getValueByFieldName("MSGCOD").equals("0000") || telcommResult.getValueByFieldName("MSGCOD").equals("    "))
	    //{
			//for(int i=0;i<COUNT;i++)
			//{
				/*
				if(txnTrAccSetDao.findDptrdacnoIsExists((String)params.get("UID"),BANK[i], ACN[i]))
				{
					txnTrAccSetDao.removeById(new Integer((String)params.get("DPACCSETID1")));
					TXNTRACCSET txntraccset = new TXNTRACCSET();
					txntraccset.setDPUSERID((String)params.get("UID"));
					txntraccset.setDPGONAME((String)params.get("DPACGONAME2"));
					txntraccset.setDPTRACNO("2");
					txntraccset.setDPTRIBANK(SelectedRecord1.substring(0,3));
					txntraccset.setDPTRDACNO(SelectedRecord1.substring(3).trim());				
					d = new Date();
					txntraccset.setLASTDATE(DateTimeUtils.format("yyyyMMdd", d));
					txntraccset.setLASTTIME(DateTimeUtils.format("HHmmss", d));
					txnTrAccSetDao.save(txntraccset);									
				}
				*/
				if(txnTrAccSetDao.findDptrdacnoIsExists((String)params.get("UID"),BANK, ACN))
				{
					TXNTRACCSET txntraccset = txnTrAccSetDao.findById(new Integer((String)params.get("DPACCSETID1")));
					txntraccset.setDPUSERID((String)params.get("UID"));
					txntraccset.setDPGONAME((String)params.get("DPACGONAME2"));
					txntraccset.setDPTRACNO("1");
					txntraccset.setDPTRIBANK(BANK);
					txntraccset.setDPTRDACNO(ACN);			
					d= new Date();
					txntraccset.setLASTDATE(DateTimeUtils.format("yyyyMMdd", d));
					txntraccset.setLASTTIME(DateTimeUtils.format("HHmmss", d));
					txnTrAccSetDao.save(txntraccset);
				}
				else
				{
					TXNTRACCSET txntraccset = new TXNTRACCSET();
					txntraccset.setDPUSERID((String)params.get("UID"));
					txntraccset.setDPGONAME((String)params.get("DPACGONAME2"));
					txntraccset.setDPTRACNO("1");
					txntraccset.setDPTRIBANK(BANK);
					txntraccset.setDPTRDACNO(ACN);				
					d = new Date();
					txntraccset.setLASTDATE(DateTimeUtils.format("yyyyMMdd", d));
					txntraccset.setLASTTIME(DateTimeUtils.format("HHmmss", d));
					txnTrAccSetDao.save(txntraccset);				
				}
			//log.debug("N215 Step2:Insert UID:"+params.get("UID") + " SelectedRecord1:"+SelectedRecord1);
			//params.put("TYPE", "01");	
			//params.put("ACNINFO", (String)params.get("SelectedRecord1"));
			//telcommResult = n215Telcomm.query(params);
	    //}
		//log.debug("N215M_1.java COUNT:"+COUNT+" SelectedRecord1.length:"+SelectedRecord1.length());	    
	    //telcommResult.getFlatValues().put("SelectedRecord", SelectedRecord);
	    //telcommResult.getFlatValues().put("COUNT", String.valueOf(COUNT));
		MVHImpl result = new MVHImpl();
		result.getFlatValues().put("SelectedRecord", SelectedRecord);
		result.getFlatValues().put("COUNT", String.valueOf(COUNT));
	    return result;
	}
}
