package fstop.services.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.TXNUSER;
import fstop.services.CommonService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class NA30_AUTH extends CommonService {

//	private Logger logger = Logger.getLogger(NA30_AUTH.class);

	@Autowired
	private TxnUserDao txnUserDao;

//	@Required
//	public void setTxnUserDao(TxnUserDao txnUserDao) {
//		this.txnUserDao = txnUserDao;
//	}
	
	@Override
	public MVH doAction(Map params) {

		MVHImpl result = new MVHImpl();

		String ccardflag = "";
		
		try {
			TXNUSER user = txnUserDao.findById((String)params.get("idn"));
			ccardflag = user.getCCARDFLAG();
		} catch (Exception e) {
			log.error("doAction error >> {}",e);
		}
		
		result.getFlatValues().put("ccardflag", ccardflag);
		return result;
	}


}
