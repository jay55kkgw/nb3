package fstop.services.batch;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.util.ESAPIUtil;

import fstop.orm.dao.AdmHolidayDao;
import fstop.orm.dao.AdmMsgCodeDao;
import fstop.orm.dao.TxnFxRecordDao;
import fstop.orm.po.ADMHOLIDAY;
import fstop.orm.po.TXNFXRECORD;
import fstop.telcomm.TelCommExec;
import fstop.util.CustomProperties;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import fstop.util.PathSetting;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 外幣關帳
 *
 */
@Slf4j
public class F021  implements BatchExecute {
//	private Logger logger = Logger.getLogger("fstop_txnfx");

	private CustomProperties realValue = new CustomProperties();

	private DecimalFormat fmt = new DecimalFormat("#0");

	@Autowired
	private TxnFxRecordDao txnFxRecordDao;

	@Autowired
	private PathSetting pathSetting;

	@Autowired
	@Qualifier("f021Telcomm")
	private TelCommExec f021Telcomm;   //F021 電文

	@Autowired
	private AdmHolidayDao admholidayDao;
	
//	@Required
//	public void setAdmHolidayDao(AdmHolidayDao admholidayDao) {
//		this.admholidayDao = admholidayDao;
//	}
//	
//	public void setF021Telcomm(TelCommExec telcomm) {
//		f021Telcomm = telcomm;
//	}
//
//	@Required
//	public void setTxnFxRecordDao(TxnFxRecordDao txnFxRecordDao) {
//		this.txnFxRecordDao = txnFxRecordDao;
//	}
//
//	@Required
//	public void setPathSetting(PathSetting pathSetting) {
//		this.pathSetting = pathSetting;
//	}

	public F021(){}

	class CloseRecord {
		public String stan = "";
		public String custacc = "";
		public String benacc = "";
	}

	/**
	 * args[0] method name
	 */
	public BatchResult execute(String[] args) {

		log.info("開始執行外匯關帳" + (DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", new Date())));

		//檢查今日是否為假日
		ADMHOLIDAY holiday = null;
		try {
			Date d = new Date();
			String today = DateTimeUtils.getCDateShort(d);
			log.info("[F021] today = " + today);
			
			String holidaytoday = DateTimeUtils.getDateShort(d);
			log.info("[F021] holidaytoday = " + holidaytoday);
			
			holiday = (ADMHOLIDAY)admholidayDao.findById(holidaytoday);
		}
		catch (Exception e) {
			holiday = null;					
		}		

		if (holiday != null)
		{
			Map<String, Object> data = new HashMap();

			data.put("COUNTER", new BatchCounter());
			data.put("__TOPMSG", "ZXXX");
			data.put("__TOPMSGTEXT", "非營業日．");
			
			log.info("[F021] __TOPMSGTEXT = " + "非營業日．");
			
			BatchResult batchresult = new BatchResult();
			batchresult.setSuccess(false);
			batchresult.setBatchName(getClass().getSimpleName());
			batchresult.setData(JSONUtils.toJson(data));

			return batchresult;
		}		
		
		Hashtable SelChannel=new Hashtable();
		// 解析參數 2016/08/21
		for(int i=0;i<args.length;i++)
		{
			log.debug("args[" + i + "] = " + args[i]);
			int index=args[i].trim().indexOf("-");
			if(index>=0)
			{
				String aa=args[i].trim().substring(index+1);
				String[] params=aa.split("=");
				if(params.length==2)
				{
					log.debug("key = " + params[0]);
					log.debug("value = " + params[1]);
					SelChannel.put(params[0], params[1]);
				}
				else
				{
					Map<String, Object> data = new HashMap();

					data.put("COUNTER", new BatchCounter());
					data.put("__TOPMSG", "ZXXY");
					data.put("__TOPMSGTEXT", "參數錯誤(f021 -FILLER=A)．");
					
					log.info("[F021] __TOPMSGTEXT = " + "參數錯誤(f021 -FILLER=A)．");
					
					BatchResult batchresult = new BatchResult();
					batchresult.setSuccess(false);
					batchresult.setBatchName(getClass().getSimpleName());
					batchresult.setData(JSONUtils.toJson(data));

					return batchresult;
				}
			}
		}
		
		if(SelChannel.size()<=0)
			SelChannel.put("FILLER", "A");
		// 解析參數 END
		
		Map<String, Object> data = new HashMap();

		try {			
			// 20160822 兩個function 增加使用A或B通道參數
			if (args.length > 0 && args[0].equals("U"))
				executeUpdate(args[1],SelChannel); //ADTXNO
			else	
				executeMain(data,SelChannel);
		}
		catch(Exception e) {
			data.put("執行結果 :", "發生錯誤 .");
		}

		log.info("結束執行外匯關帳" + (DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", new Date())));

		BatchCounter counter = new BatchCounter();
		data.put("COUNTER", counter);

		BatchResult batchresult = new BatchResult();
		batchresult.setSuccess(true);
		batchresult.setBatchName(getClass().getSimpleName());
		batchresult.setData(JSONUtils.toJson(data));

		return batchresult;
	}
	
	//將解析出來的SelChannel 加到params Hhashtable裡面
	private boolean AddParams(Map params,Map SelChannel)
	{
		if(SelChannel==null || SelChannel.isEmpty()==true)
			return false;
		params.putAll(SelChannel);
		for(Object key : params.keySet())
		{
			log.debug(ESAPIUtil.vaildLog("params(" + key + params.get((String)key) + ")"));
		}
		return true;
	}

	private void executeUpdate(String adtxno,Map SelChannel) {

		//外匯關帳
		Map<String, Object> data = new HashMap();
		
		List<CloseRecord> selfbankRecord = new ArrayList();
		List<CloseRecord> crossbankRecord = new ArrayList();


		String lastdate = DateTimeUtils.format("yyyyMMdd", new Date());

		TXNFXRECORD rec = txnFxRecordDao.findById(adtxno);
		
		/*
		 * 2010/10/12 新增
		 * 
		 * 判斷 F001 前置電文是否發送成功; 如果不成功,則不發送 F021 關帳通知(TXFLAG="U")
		 */
		if (rec.getFXMSGSEQNO().indexOf("1") == -1) {			
			return;			
		}		
		
		/*
			自行關帳, PCODE 要放 1000
			(轉入轉出帳號 前三碼皆為 893
			  或 轉入轉出帳號 前三碼皆不為 893)

			他行關帳, PCODE 要放 2000
		 */
		String str_ADOPID = null;

		String inAcc = StrUtils.trim(rec.getFXSVAC()); //轉入帳號
		String outAcc = StrUtils.trim(rec.getFXWDAC()); //轉出帳號
		str_ADOPID = rec.getADOPID();
				
		CloseRecord r = new CloseRecord();
		
		if (str_ADOPID.equals("F003"))
			r.custacc = ""; //轉出帳號
		else
			r.custacc = outAcc; //轉出帳號				
		
		if (str_ADOPID.equals("F002"))
			r.benacc = "";      //轉入帳號(匯出匯款) 
		else
			r.benacc = inAcc;   //轉入帳號
				
		r.stan = rec.getFXMSGCONTENT();				
				
		if ( isSelf(inAcc, outAcc) && 
				(! str_ADOPID.equals("F002") && ! str_ADOPID.equals("F003")) ) {  //是否為自行

			//自行
			selfbankRecord.add(r);
		}
		else {									
			//他行
			crossbankRecord.add(r);
		}
		
		boolean isSelfSuccess = false;
		boolean isSelfSendTelcomm = false;
		boolean isCrossSuccess = false;
		boolean isCrossSendTelcomm = false;
		int i_RetryTimes = 0;
		
		while (true) {
			
			//他行關帳
			try {				
				//if (! isCrossSendTelcomm && crossbankRecord.size() > 0) {
				if (! isCrossSuccess && crossbankRecord.size() > 0) {
					
					try {
						Hashtable params = new Hashtable();

						String occur = toTelcommContent(crossbankRecord);
						
						params.put("CNT", crossbankRecord.size() + "");
						params.put("F021DATA", occur);
						params.put("PCODE", "2000");
						params.put("TXFLAG", "U");
						AddParams(params,SelChannel);           // 20160821  配合FX GATEWAY	修改固定走A通道
						// params.put("FILLER", "A");  // 20160712 配合FX GATEWAY	修改固定走A通道

						isCrossSendTelcomm = true;
						f021Telcomm.query(params);

						isCrossSuccess = true;
					} catch(Exception e) {
						isCrossSuccess = false;
						String errMsg = "他行關帳發生錯誤(" + (isCrossSendTelcomm ? "已發送 F021 電文" : "未發送 F021電文") + ") ." + e.getMessage();
						log.error(errMsg, e);
						data.put("他行關帳錯誤訊息 :", errMsg);
					}

					if(isCrossSuccess) {
						log.info("外匯他行已關帳.");
						data.put("他行關帳執行結果 :", "成功");
					}
					else {
						log.info("外匯他行有問題.");
						data.put("他行關帳執行結果 :", "失敗");
					}										
					
				}//end if (! isCrossSendTelcomm)			
				
				else {
					//isCrossSendTelcomm = true;     //ignore	
					
					isCrossSuccess = true;     //ignore
				}				
			}
			finally {}		
			
			//自行關帳
			try {
				//if (! isSelfSendTelcomm && selfbankRecord.size() > 0) {
				if (! isSelfSuccess && selfbankRecord.size() > 0) {				
					
					try {
						Hashtable params = new Hashtable();

						String occur = toTelcommContent(selfbankRecord);
							
						params.put("CNT", selfbankRecord.size() + "");
						params.put("F021DATA", occur);
						params.put("PCODE", "1000");
						params.put("TXFLAG", "U");
						AddParams(params,SelChannel);          // 20160821  配合FX GATEWAY	修改固定走A通道
						// params.put("FILLER", "A"); // 20160712 配合FX GATEWAY	修改固定走A通道
						isSelfSendTelcomm = true;
						f021Telcomm.query(params);

						isSelfSuccess = true;
					} catch(Exception e) {
						isSelfSuccess = false;
						String errMsg = "自行關帳發生錯誤(" + (isSelfSendTelcomm ? "已發送 F021 電文" : "未發送 F021電文") + ") ." + e.getMessage();
						log.error(errMsg, e);
						data.put("自行關帳錯誤訊息 :", errMsg);
					}

					if(isSelfSuccess) {
						log.info("外匯自行已關帳.");
						data.put("自行關帳執行結果 :", "成功");
					}
					else {
						log.info("外匯自行有問題.");
						data.put("自行關帳執行結果 :", "失敗");												
					}						
					
				}//end if (! isSelfSendTelcomm)
				
				else {
					//isSelfSendTelcomm = true;     //ignore
					
					isSelfSuccess = true;     //ignore
				}
			}
			finally {}			
			
			try {
				//if (isSelfSendTelcomm == true && isCrossSendTelcomm == true) {
				if (isSelfSuccess == true && isCrossSuccess == true) {				
					break;
				}
				else {									
					if (i_RetryTimes >= 10)
						break;
					else {
						Thread.sleep(60000); //關帳通知有異常時,等候 1 分鐘再繼續發送
						
						i_RetryTimes++;											
					}																
				}
			}
			catch (Exception e) {
				String errMsg = "F021 -> Thread.sleep() 時發生錯誤 : " + e.getMessage();
				log.error(errMsg, e);						
			}			
			
		}//end while (true)		
	}
	
	private void executeMain(Map<String, Object> data, Map SelChannel) {

		//外匯關帳

		List<CloseRecord> selfbankRecord = new ArrayList();
		List<CloseRecord> crossbankRecord = new ArrayList();


		String lastdate = DateTimeUtils.format("yyyyMMdd", new Date());
		List<TXNFXRECORD> fxOnlineResult = txnFxRecordDao.findOnlineResendStatus(lastdate, new String[]{"5"});
		List<TXNFXRECORD> fxScheduleResult = txnFxRecordDao.findScheduleResendStatus(lastdate, new String[]{"5"});

		
		/****** 過濾掉可自動重送,但不可人工重送之交易資料 START ******/		
		AdmMsgCodeDao admMsgCodeDao = (AdmMsgCodeDao) SpringBeanFactory.getBean("admMsgCodeDao");
		
		for (int i=0; i<fxOnlineResult.size(); i++) {
			TXNFXRECORD rec = (TXNFXRECORD)fxOnlineResult.get(i);
						
			String str_ResendFlag = admMsgCodeDao.findFXRESEND(rec.getFXEXCODE());					
			if (str_ResendFlag.equals("N") || 
					(!rec.getADOPID().equals("F001") && !rec.getADOPID().equals("F002") && !rec.getADOPID().equals("F003")) ) {
				fxOnlineResult.remove(i);
				i--;
			}	
		}		

		for (int i=0; i<fxScheduleResult.size(); i++) {
			TXNFXRECORD rec = (TXNFXRECORD)fxScheduleResult.get(i);
						
			String str_ResendFlag = admMsgCodeDao.findFXRESEND(rec.getFXEXCODE());					
			if (str_ResendFlag.equals("N") || 
					(!rec.getADOPID().equals("F001") && !rec.getADOPID().equals("F002")) ) {
				fxScheduleResult.remove(i);
				i--;
			}	
		}				
		/****** 過濾掉可自動重送,但不可人工重送之交易資料 END ******/		
		
		
		/*
			自行關帳, PCODE 要放 1000
			(轉入轉出帳號 前三碼皆為 893
			  或 轉入轉出帳號 前三碼皆不為 893)

			他行關帳, PCODE 要放 2000
		 */
		String str_ADOPID = null;
		List[] rAry = new List[] {
				fxOnlineResult, fxScheduleResult};
		
		for(List<TXNFXRECORD> recList : rAry) {
			for(TXNFXRECORD rec : recList) {
				String inAcc = StrUtils.trim(rec.getFXSVAC()); //轉入帳號
				String outAcc = StrUtils.trim(rec.getFXWDAC()); //轉出帳號
				str_ADOPID = rec.getADOPID();
				
				if ( !str_ADOPID.equals("F001") && !str_ADOPID.equals("F002") && !str_ADOPID.equals("F003") )
					continue;
								
				CloseRecord r = new CloseRecord();
				
				if (str_ADOPID.equals("F003"))
					r.custacc = ""; //轉出帳號
				else
					r.custacc = outAcc; //轉出帳號
				
				if (str_ADOPID.equals("F002"))
					r.benacc = "";      //轉入帳號(匯出匯款) 
				else
					r.benacc = inAcc;   //轉入帳號
				
				r.stan = rec.getFXMSGCONTENT();				
								
				if( isSelf(inAcc, outAcc) && 
						(! str_ADOPID.equals("F002") && ! str_ADOPID.equals("F003")) ) {  //是否為自行
					
					//自行
					selfbankRecord.add(r);
				}
				else {									
					//他行
					crossbankRecord.add(r);
				}

			}

		}
		
		boolean isSelfSuccess = false;
		boolean isSelfSendTelcomm = false;
		boolean isCrossSuccess = false;
		boolean isCrossSendTelcomm = false;
		int i_RetryTimes = 0;
		
		while (true) {
			
			//他行關帳
			try {				
				//if (! isCrossSendTelcomm) {
				if (! isCrossSuccess) {
					
					try {
						Hashtable params = new Hashtable();

						String occur = toTelcommContent(crossbankRecord);
						
						params.put("CNT", crossbankRecord.size() + "");
						params.put("F021DATA", occur);
						params.put("PCODE", "2000");
						params.put("TXFLAG", "A");
						AddParams(params,SelChannel);          // 20160821  配合FX GATEWAY	修改固定走A通道
						// params.put("FILLER", "A"); // 20160712 配合FX GATEWAY	修改固定走A通道

						isCrossSendTelcomm = true;
						f021Telcomm.query(params);

						isCrossSuccess = true;
					} catch(Exception e) {
						isCrossSuccess = false;
						String errMsg = "他行關帳發生錯誤(" + (isCrossSendTelcomm ? "已發送 F021 電文" : "未發送 F021電文") + ") ." + e.getMessage();
						log.error(errMsg, e);
						data.put("他行關帳錯誤訊息 :", errMsg);
					}

					if(isCrossSuccess) {
						log.info("外匯他行已關帳.");
						data.put("他行關帳執行結果 :", "成功");
					}
					else {
						log.info("外匯他行有問題.");
						data.put("他行關帳執行結果 :", "失敗");
					}										
					
				}//end if (! isCrossSendTelcomm)										
			}
			finally {}			
			
			//自行關帳
			try {
				//if (! isSelfSendTelcomm) {
				if (! isSelfSuccess) {
					
					try {
						Hashtable params = new Hashtable();

						String occur = toTelcommContent(selfbankRecord);
							
						params.put("CNT", selfbankRecord.size() + "");
						params.put("F021DATA", occur);
						params.put("PCODE", "1000");
						params.put("TXFLAG", "A");
						AddParams(params,SelChannel);          // 20160821  配合FX GATEWAY	修改固定走A通道
						// params.put("FILLER", "A"); // 20160712 配合FX GATEWAY	修改固定走A通道
						
						isSelfSendTelcomm = true;
						f021Telcomm.query(params);

						isSelfSuccess = true;
					} catch(Exception e) {
						isSelfSuccess = false;
						String errMsg = "自行關帳發生錯誤(" + (isSelfSendTelcomm ? "已發送 F021 電文" : "未發送 F021電文") + ") ." + e.getMessage();
						log.error(errMsg, e);
						data.put("自行關帳錯誤訊息 :", errMsg);
					}

					if(isSelfSuccess) {
						log.info("外匯自行已關帳.");
						data.put("自行關帳執行結果 :", "成功");
					}
					else {
						log.info("外匯自行有問題.");
						data.put("自行關帳執行結果 :", "失敗");												
					}						
					
				}//end if (! isSelfSendTelcomm)
			}
			finally {}									
			
			try {
				//if (isSelfSendTelcomm == true && isCrossSendTelcomm == true) {
				if (isSelfSuccess == true && isCrossSuccess == true) {
					break;
				}
				else {					
					if (i_RetryTimes >= 20)
						break;
					else {
						Thread.sleep(60000); //關帳通知有異常時,等候 1 分鐘再繼續發送
						
						i_RetryTimes++;											
					}	
				}
			}
			catch (Exception e) {
				String errMsg = "F021 -> Thread.sleep() 時發生錯誤 : " + e.getMessage();
				log.error(errMsg, e);						
			}			
			
		}//end while (true)		
	}

	//是否為自行
	private boolean isSelf(String inAcc, String outAcc) {
		return (inAcc.startsWith("893") && outAcc.startsWith("893")) 
		        || (!inAcc.startsWith("893") && !outAcc.startsWith("893"));
	}


	private String toTelcommContent(List<CloseRecord> records) {
		StringBuffer result = new StringBuffer();
		String stan = null;
		
		for(CloseRecord r : records) {
			//負號, 表示往左靠
			//STAN SEQNO CUSTACC BENACC
			stan = r.stan.trim();
			
			String s;
			
			if (stan.equals("")) {
				s = String.format("%s%s%-11s%-11s", StrUtils.repeat("0", 8), 
		                          StrUtils.repeat("0", 5), r.custacc, r.benacc);
			}
			else {
				s = String.format("%-8s%s%-11s%-11s", stan, StrUtils.repeat("0", 5), r.custacc, r.benacc);				
			}
			
			result.append(s);
		}

		return result.toString();
	}


}
