package fstop.services.batch;

public interface BatchExecute {
	public BatchResult execute(String[] params);
}
