package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.netbank.util.DateUtils;
import com.netbank.util.NumericUtil;
import com.netbank.util.StrUtils;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.model.TelcommResult;
import lombok.extern.slf4j.Slf4j;

/**
 * 主要用來
 * 1.丟入MVH前的前置處理，包含檢核;
 * 2.後製MVH回覆的訊息;
 * @author Eric
 *
 */
@Service
@Slf4j
public class N552_Tel_Service extends Common_Tel_Service{
	@Override
	public HashMap pre_Processing(Map request) {
		// TODO Auto-generated method stub
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) {
		// TODO Auto-generated method stub
		return super.process(serviceID, request);
	}

	@Override
	public HashMap<String, Object> data_Retouch(MVH mvh) {
		MVHImpl mvhimpl = null;
		LinkedList<Row> tmplist = null;
		Boolean result = Boolean.FALSE;
		HashMap<String, Object> dataMap = new HashMap<String, Object>();
		HashMap<String, String> rowMap = new HashMap<String, String>();
		LinkedList<HashMap<String, String>> rowlist = null;
		log.info("myn552","yes");
		try {
			mvhimpl = (MVHImpl) mvh;
			log.info("getFlatValues>>" + mvhimpl.getFlatValues());
			log.info("result.getInputQueryData()>>" + mvhimpl.getInputQueryData());
			log.info("result.getInputQueryData()>>" + mvhimpl.getOccurs().getRows());
			
			Iterator<Row> tmp = ((TelcommResult)mvhimpl.getTableByName("DPFMNM1")).getOccurs().getRows().iterator();//TW
			//設定下一層
			//Iterator<Row> tmpB1 = ((TelcommResult)mvhimpl.getTableByName("tmp")).getOccurs().getRows().iterator();//TW
			tmplist = new LinkedList<Row>();
			rowlist = new LinkedList<HashMap<String, String>>();
			tmp.forEachRemaining(tmplist::add);
			//tmpB1.
		//log.info("datalist()>>" + tmplist);
			for (Row row : tmplist) {
				log.info("getValues>>" + row.getValues());
				rowMap = new HashMap<String, String>();
//				//row.setValue("ACN", StrUtils.hideaccount(row.getValue("ACN")));
//				row.setValue("TRNINT", NumericUtil.fmtAmount((row.getValue("TRNINT").trim()),2));
//				row.setValue("TRNPAL", NumericUtil.fmtAmount((row.getValue("TRNPAL").trim()),2));
//				//BAL很特殊先被轉成xxx.xx 轉回來變成xxxxxx
//				row.setValue("BAL", NumericUtil.fmtAmount((row.getValue("BAL").replace(".", "")),2));
//				row.setValue("CURIPD", DateUtils.addSlash2(row.getValue("CURIPD")));
//				row.setValue("DATTRN", DateUtils.addSlash2(row.getValue("DATTRN")));
				rowMap.putAll(row.getValues());
				rowlist.add(rowMap);
			}
			dataMap.putAll(mvhimpl.getFlatValues());
			dataMap.put("REC", rowlist);
//			rowlist.removeIf(Map::isEmpty);// 移除空白 OR null的Map
//			rowlist.removeIf(Map -> null == Map.get("ACN") || "".equals(Map.get("ACN")));// 移除ACN==空白 OR null的Map
			result = Boolean.TRUE;

		} catch (Exception e) {
			log.error("data_Retouch error >> {}",e);
		} finally {
			if (result) {
				dataMap.put("msgCode", "0");
			} else {
				dataMap.put("msgCode", "FE0003");
			}
		}
		return dataMap;
	}

}
