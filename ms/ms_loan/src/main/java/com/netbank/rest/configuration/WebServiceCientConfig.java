package com.netbank.rest.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import fstop.ws.AMLWebServiceTemplate;
import fstop.ws.BHWebServiceTemplate;
import fstop.ws.EAIWebServiceTemplate;
import fstop.ws.fisc.client.FiscWebServiceClient;
import fstop.ws.fisc.client.IFiscWebServiceClient;
import lombok.extern.slf4j.Slf4j;

/**
 * 呼叫其他系統的WEB SERVICE 的 client
 */
@Configuration
@Slf4j
public class WebServiceCientConfig {

    @Bean
    protected BHWebServiceTemplate BHWebServiceTemplate() throws Exception {
        return new BHWebServiceTemplate();
    }
    
    @Bean
    protected EAIWebServiceTemplate EAIWebServiceTemplate() throws Exception {
    	return new EAIWebServiceTemplate();
    }
    
    @Bean
    protected IFiscWebServiceClient FiscWebServiceClient() throws Exception {
        return new FiscWebServiceClient();
    }
    @Bean
    protected AMLWebServiceTemplate AMLWebServiceTemplate() throws Exception {
    	return new AMLWebServiceTemplate();
    }
}
