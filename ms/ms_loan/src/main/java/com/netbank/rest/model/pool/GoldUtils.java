package com.netbank.rest.model.pool;

import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.netbank.rest.model.UserPool;

import fstop.orm.dao.TxnGdRecordDao;
import fstop.orm.po.TXNGDRECORD;

@Component
public class GoldUtils  extends UserPool {

	@Autowired
	private TxnGdRecordDao txnGdRecordDao;
	
//	@Required
//	public void setTxnGdRecordDao(TxnGdRecordDao dao) {
//		this.txnGdRecordDao = dao;
//	}
	
	/**
	 * 將黃金存摺交易單據之網頁內容寫入 TXNGDRECORD
	 * 
	 * @param adtxno
	 * @param gdCertContent
	 */
	public void updateGDCERT(String adtxno, String gdCertContent)
	{
		TXNGDRECORD gd = null;

		try {
			gd = txnGdRecordDao.findById(adtxno);
		}
		catch(ObjectNotFoundException e) {
			gd = null;
		}
		
		if(gd != null)
		{
			gd.setGDCERT(gdCertContent); //交易單據網頁內容
		}

		txnGdRecordDao.save(gd);
	}	
	
	/**
	 * 查詢 TXNGDRECORD 黃金存摺交易單據之網頁內容 
	 * 
	 * @param adtxno
	 */
	public String getGDCERT(String adtxno)
	{
		String GDCERT = "";
		TXNGDRECORD gd = null;

		try {
			gd = txnGdRecordDao.findById(adtxno);
		}
		catch(ObjectNotFoundException e) {
			gd = null;
		}
		
		if(gd != null)
		{
			GDCERT = gd.getGDCERT(); //交易單據網頁內容
		}

		return GDCERT;
	}		

}
