package com.netbank.rest.model.pool;

import java.util.Date;
import java.util.Hashtable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Component;

import com.netbank.rest.model.NewOneCallback;
import com.netbank.rest.model.UserPool;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;

import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;

@Component
public class N107Pool extends UserPool {

	@Autowired
	@Qualifier("n107Telcomm")
	private TelCommExec n107Telcomm;
	
//	@Required
//	public void setN107Telcomm(TelCommExec telcomm) {
//		n107Telcomm = telcomm;
//	}

	private NewOneCallback getNewOneCallback(final String uid) {
		NewOneCallback callback = new NewOneCallback() {

			public Object getNewOne() {

				Hashtable params = new Hashtable();
				params.put("CUSIDN", uid);
				Date d = new Date();
				String df = DateTimeUtils.getCDateShort(d);
				String tf = DateTimeUtils.format("HHmmss", d);
				params.put("DATE", df);
				params.put("TIME", tf);
				params.put("SYNC","");
				params.put("PINKEY","");
				params.put("PPSYNCN","");
				params.put("PINNEW","");
				params.put("CUSIDN", uid);
				params.put("TYPE", "R");
				
				TelcommResult mvh = n107Telcomm.query(params);
				
				return mvh;
			}
			
		};		
		
		return callback;
	}
	
	/**
	 * N107 利息扣繳憑單無紙化作業查詢
	 * @param uid  使用者的身份證號
	 * @return
	 */
	public MVH getData(final String idn) {
		final String uid = StrUtils.trim(idn).toUpperCase();
		if(uid.length() == 0)
			return new MVHImpl();
		
		return (MVH)getPooledObject(uid, "N107.getData", getNewOneCallback(uid));
		//return (MVH)getNewOneCallback(uid).getNewOne();

	}
	
	public void removeGetDataCache(final String idn) {
		this.removePoolObject(idn, "N107.getData");
	}
	
}
