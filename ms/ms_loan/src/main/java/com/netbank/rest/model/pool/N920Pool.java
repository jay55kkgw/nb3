package com.netbank.rest.model.pool;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.netbank.rest.model.NewOneCallback;
import com.netbank.rest.model.UserPool;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.model.RowFilterCallback;
import fstop.model.Rows;
import fstop.model.RowsSelecter;
import fstop.model.TelcommResult;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import fstop.util.StrUtils;

@Component
public class N920Pool extends UserPool {
	
	@Autowired
	@Qualifier("n920Telcomm")
	private TelCommExec n920Telcomm;
	
	//private AdmBranchDao admBranchDao;
	
//	@Required
//	public void setAdmBranchDao(AdmBranchDao admBranchDao) {
//		this.admBranchDao = admBranchDao;
//	}

//	@Required
//	public void setN920Telcomm(TelCommExec telcomm) {
//		n920Telcomm = telcomm;
//	}

	public void init(String uid){
		getAcnoList(uid, new String[]{}, "");		
	}

	private NewOneCallback getNewOneCallback(final String uid) {
		NewOneCallback callback = new NewOneCallback() {

			public Object getNewOne() {

				Hashtable params = new Hashtable();
				params.put("CUSIDN", uid);
				params.put("MMACOD", "M");
				Date d = new Date();
				String df = DateTimeUtils.getCDateShort(d);
				String tf = DateTimeUtils.format("HHmmss", d);
				params.put("DATE", df);
				params.put("TIME", tf);
				
				TelcommResult mvh = n920Telcomm.query(params);
				
				return mvh;
			}
			
		};		
		
		return callback;
	}
	
	
	/**
	 * 
	 * @param uid  使用者的身份證號
	 * @param acnotype  帳戶屬性
	 * @param trflag  轉出帳號註記
	 * @return
	 */
	public MVH getAcnoList(final String idn, final String[] acnotype, final String trflag) {
		final String uid = StrUtils.trim(idn).toUpperCase();
		if(uid.length() == 0)
			return new MVHImpl();
		
//		MVHImpl mvh = (MVHImpl)getPooledObject(uid, "getAcnoList", getNewOneCallback(uid));
//		MVHImpl mvh = new MVHImpl();
		Hashtable params = new Hashtable();
		params.put("CUSIDN", uid);
		params.put("MMACOD", "M");
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		params.put("DATE", df);
		params.put("TIME", tf);
		TelcommResult mvh = n920Telcomm.query(params);
		Rows rows  = mvh.getOccurs();
		
		final Set<String> actypes = new HashSet();
		if(acnotype != null) {
			for(String act : acnotype) {
				if(StrUtils.isNotEmpty(act))
					actypes.add(StrUtils.right("00" + StrUtils.trim(act), 2));
			}
		}
		
		RowsSelecter rowSelecter = new RowsSelecter(rows);
		rowSelecter.setFilter(new RowFilterCallback() {

			public boolean accept(Row row) {
				/*
				try {
					ADMBRANCH branch = admBranchDao.getByACN(row.getValue("ACN"));
					
					row.setValue("BRANID", branch.getADBRANCHID());
					row.setValue("BRANCHNAME", branch.getADBRANCHNAME());					
				}
				catch (Exception e) {
					row.setValue("BRANID", row.getValue("ACN").substring(0, 3));
					row.setValue("BRANCHNAME", "");
				}
				*/
				boolean result = true;
				if((acnotype == null || acnotype.length == 0) && StrUtils.isEmpty(trflag))
					return true;
				if(acnotype != null ) {
					result = (result && actypes.contains(StrUtils.trim(row.getValue("ATTRBUE"))));
				}
				if(StrUtils.isNotEmpty(trflag)) {
					result = (result && StrUtils.trim(trflag).equalsIgnoreCase(row.getValue("TRFLAG")));
				}
				
				return result;
			}
		});
		MVHImpl result = new MVHImpl(mvh.getFlatValues(), new Rows(rowSelecter.getSelectedRows()));

		return result;
	}
	
	/**
		屬性代號	帳號科目簡碼								中文名稱
		01		01、05									支存
		02		12、60、61、62、64						活存
		03		06、07、08、
				10、11、13、14、15、16、17、18、19、20
				36、37、38、39
				46、47、48、49
				63、65、66、67、68、69					特殊活儲（證券戶、台電分戶）
		04		50、52、54								外匯活存、外匯定存、外匯備償專戶
		05		51										外匯定存
		06		21、22、23、24、25、26、27、28、29
				71、72、73、74、75、76、77、78、79、80、81	定存
		07		12、60、61、62、64						整合型智慧理財帳戶
		08		97										中央登錄債券帳號
        09		98										黃金存摺帳號
	 * @param idn
	 * @param attr
	 * @return
	 */
	public MVH getAcnoListByAttr(final String idn, final String[] attrs) {
		
		Set<String> types = new HashSet();
		for(String attr : attrs) {
			String[] kattr = new String[] {"-1"};
			if("01".equals(attr))
				kattr = new String[] {"0", "05"};
			if("02".equals(attr))
				kattr = new String[] {"12", "60", "61", "62", "64"};
			if("03".equals(attr))
				kattr = new String[] {
					"06", "07", "08"
					,"10", "11", "13", "14", "15", "16", "17", "18", "19", "20"
					,"36", "37", "38", "39"
					,"46", "47", "48", "49"
					,"63", "65", "66", "67", "68", "69"};
			if("04".equals(attr))
				kattr = new String[] {"50", "52", "54"};
			if("05".equals(attr))
				kattr = new String[] {"51"};
			if("06".equals(attr))
				kattr = new String[] {
					"21", "22", "23", "24", "25", "26", "27", "28", "29"
					,"71", "72", "73", "74", "75", "76", "77", "78", "79", "80", "81"
			};
			if("07".equals(attr))
				kattr = new String[] {"12", "60", "61", "62", "64"};
			
			if("08".equals(attr))
				kattr = new String[] {"97"};
			if("09".equals(attr))
				kattr = new String[] {"98"};			
			types.addAll(Arrays.asList(kattr));
		}
		
		String[] kattr = new String[types.size()];
		types.toArray(kattr);
		return getAcnoList(idn, kattr, "");
	}
	
	/**
	 * 將 身份證字號 為KEY 放置在POOL 的 N920 資料刪除
	 * @param idn
	 */
	public void removeFromPool(String idn) {
		super.removePoolObject(idn);
	}
	
}
