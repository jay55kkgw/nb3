package com.netbank.rest.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import fstop.services.ServerConfig;
import fstop.services.TopMsgDBDetecter;
import fstop.services.TopMsgF003Detecter;
import fstop.services.TopMsgF007Detecter;
import fstop.services.TopMsgFXDetecter;
import fstop.services.TopMsgFundDetecter;
import fstop.services.TopMsgN320Detecter;
import fstop.services.TopMsgN950Detecter;
import fstop.services.impl.CallMobileCenter;
import fstop.services.impl.Eloan;
import fstop.services.impl.N014;
import fstop.services.impl.N016;
import fstop.services.impl.N105;
import fstop.services.impl.N110;
import fstop.services.impl.N3003;
import fstop.services.impl.N320;
import fstop.services.impl.NA01;
import fstop.services.impl.NA011;
import fstop.services.impl.SmsOtp;
import fstop.services.impl.SmsOtpWithphone;
import fstop.services.impl.XLSTXT;
import fstop.services.mgn.B105;
import fstop.services.mgn.B106;
import fstop.services.mgn.NA20;
import fstop.va.VATransform;
import fstop.ws.eai.client.N701WebServiceClient;
import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class ServicesConfig {

    @Bean
    protected VATransform vatransform() throws Exception {
        return new VATransform();
    }
//    @Bean
//    protected CheckServerHealth checkserverhealth() throws Exception {
//    	return new CheckServerHealth();
//    }
    @Bean
    protected N701WebServiceClient n701WebServiceClient() throws Exception {
        return new N701WebServiceClient();
    }
	
    @Bean
    protected TopMsgDBDetecter dbErrDetect() throws Exception {
        return new TopMsgDBDetecter();
    }

    @Bean
    protected TopMsgN320Detecter n320ErrDetect() throws Exception {
        return new TopMsgN320Detecter();
    }

    @Bean
    protected TopMsgFXDetecter fxErrDetect() throws Exception {
        return new TopMsgFXDetecter();
    }

    @Bean
    protected TopMsgF003Detecter f003ErrDetect() throws Exception {
        return new TopMsgF003Detecter();
    }

    @Bean
    protected TopMsgF007Detecter f007ErrDetect() throws Exception {
        return new TopMsgF007Detecter();
    }

    @Bean
    protected TopMsgFundDetecter fundErrDetect() throws Exception {
        return new TopMsgFundDetecter();
    }
    @Bean
    protected TopMsgN950Detecter n950ErrDetect() throws Exception {
        return new TopMsgN950Detecter();
    }
    
    @Bean
    protected B105 b105Service() throws Exception {
        return new B105();
    }
    
    @Bean
    protected B106 b106Service() throws Exception {
        return new B106();
    }
    @Bean
    protected CallMobileCenter callmobilecenterService() throws Exception {
    	return new CallMobileCenter();
    }
    @Bean
    protected NA01 na01Service() throws Exception {
        return new NA01();
    }
    
    @Bean
    protected NA20 na20Service() throws Exception {
        return new NA20();
    }
    
    @Bean
    protected NA011 na011Service() throws Exception {
        return new NA011();
    }
    
    @Bean
    protected N105 n105Service() throws Exception {
        return new N105();
    }
    
    @Bean
    protected N110 n110Service() throws Exception {
        return new N110();
    }

    @Bean
    protected N3003 n3003Service() throws Exception {
        return new N3003();
    }
    
    @Bean
    protected N016 n016Service() throws Exception {
        return new N016();
    }
    
    @Bean
    protected N014 n014Service() throws Exception {
        return new N014();
    }
    
    @Bean
    protected N320 n320Service() throws Exception {
        return new N320();
    }
    
    //<!-- 後台OTP狀態 -->
    @Bean
    protected ServerConfig serverConfig() throws Exception {
        return new ServerConfig();
    }
	
    @Bean
    protected XLSTXT xlstxtService() throws Exception {
        return new XLSTXT();
    }
    
    @Bean
    protected SmsOtp smsotpService() throws Exception {
    	return new SmsOtp();
    }
    
    @Bean
    protected SmsOtpWithphone smsotpwithphoneService() throws Exception {
    	return new SmsOtpWithphone();
    }
    
    @Bean
    protected Eloan eloanService() throws Exception {
        return new Eloan();
    }
}
