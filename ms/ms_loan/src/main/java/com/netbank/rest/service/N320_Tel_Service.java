package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.netbank.util.DateUtils;
import com.netbank.util.NumericUtil;
import com.netbank.util.StrUtils;

import fstop.model.GrpColumns;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.MVHUtils;
import fstop.model.Row;
import fstop.model.RowsGroup;
import fstop.model.TelcommResult;
import lombok.extern.slf4j.Slf4j;

/**
 * 主要用來 1.丟入MVH前的前置處理，包含檢核; 2.後製MVH回覆的訊息;
 * 
 * @author Hugo
 *
 */
@Service
@Slf4j
public class N320_Tel_Service extends Common_Tel_Service {
	@Override
	public HashMap pre_Processing(Map request) {
		// TODO Auto-generated method stub
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) {
		// TODO Auto-generated method stub
		return super.process(serviceID, request);
	}

	// @Override
	// public HashMap data_Retouch(MVH mvh) {
	// // TODO Auto-generated method stub
	// return super.data_Retouch(mvh);
	// }
	@Override
	public void setMsgCode(Map dataMap, Boolean data_Retouch_result) {
		// TODO Auto-generated method stub
		super.setMsgCode(dataMap, data_Retouch_result);
	}

	@Override
	public HashMap data_Retouch(MVH mvh) {
		MVHImpl mvhimpl = null;
		MVHImpl mvhimpl2 = null;
		LinkedList<Row> tmplist = null;
		LinkedList<Row> tmplist2 = null;
		Boolean result = Boolean.FALSE;
		HashMap<String, Object> dataMap = new HashMap<String, Object>();
		HashMap<String, String> rowMap = new HashMap<String, String>();
		LinkedList<HashMap<String, String>> rowlist = null;
		LinkedList<HashMap<String, String>> rowlist2 = null;
		
		LinkedList<HashMap<String, String>> cryrowlist = new LinkedList<HashMap<String, String>>();
		log.info("myn320", "yes");
		try {
			mvhimpl = (MVHImpl) mvh;
			log.info("getFlatValues>>" + mvhimpl.getFlatValues());
			log.info("result.getInputQueryData()>>" + mvhimpl.getInputQueryData());
			
			
			//TW DATA process
			if (null!=mvhimpl.getTableByName("DPFMNM1")) {
				Iterator<Row> tmp = ((TelcommResult) mvhimpl.getTableByName("DPFMNM1")).getOccurs().getRows()
						.iterator();// TW
				Map<String, String> flt320 = ((TelcommResult) mvhimpl.getTableByName("DPFMNM1")).getFlatValues();
				for (String key : flt320.keySet()) {
					dataMap.put(key + "_320", flt320.get(key));
				}
				tmplist = new LinkedList<Row>();
				rowlist = new LinkedList<HashMap<String, String>>();
				tmp.forEachRemaining(tmplist::add);

				for (Row row : tmplist) {
					if (!row.getValue("ITRUSSN").isEmpty()) {
						row.setValue("ITRUS", row.getValue("ITRUSSN"));
					}
					if (!row.getValue("SEQ").isEmpty()) {
						row.setValue("SEQ2", String.valueOf(Integer.parseInt(row.getValue("SEQ"))));
					}
					if (!row.getValue("RAT").isEmpty()) {
						row.setValue("RAT", NumericUtil.addDot(row.getValue("RAT").trim(), 3));
					}

					// MVHImpl TwTotalAMT = new MVHImpl();
					// TwTotalAMT.getOccurs().addRows(((TelcommResult)mvhimpl.getTableByName("DPFMNM1")).getOccurs());
					log.trace("getValues>>" + row.getValues());
					rowMap = new HashMap<String, String>();
					// String[] groupBy = new String[]{"AMTAPY"};
					// RowsGroup grp = MVHUtils.group(TwTotalAMT.getOccurs(), groupBy);
					// sum = grp.sum("AMTAPY");

					rowMap.putAll(row.getValues());
					rowlist.add(rowMap);
				}
				rowlist.removeIf(Map::isEmpty);// 移除空白 OR null的Map
				rowlist.removeIf(Map -> null == Map.get("ACN") || "".equals(Map.get("ACN")));// 移除ACN==空白 OR null的Map
				
				
				dataMap.put("TW", rowlist);
				int count = rowlist.size();
				dataMap.put("TWNUM", count);
			}else {
				dataMap.put("TOPMSG_320", "NoCall");
			}

			
			//FX DATA process
			if (null!=mvhimpl.getTableByName("DPFMNM2")) {
				Iterator<Row> tmp2 = ((TelcommResult) mvhimpl.getTableByName("DPFMNM2")).getOccurs().getRows()
						.iterator();// FX

				Map<String, String> flt552 = ((TelcommResult) mvhimpl.getTableByName("DPFMNM2")).getFlatValues();

				int i =1;
				while(i<flt552.size()) {
					HashMap<String, String> cryMap = new HashMap<String, String>();
					if(StrUtils.isNotEmpty(flt552.get("AMTLNCCY_" + i))) {
					cryMap.put("AMTLNCCY", flt552.get("AMTLNCCY_" + i));
					cryMap.put("FXTOTAMT", flt552.get("FXTOTAMT_" + i));
					cryMap.put("FXTOTNTDBAL", flt552.get("FXTOTNTDBAL_" + i));
					cryMap.put("NTDBALLNCCY", flt552.get("NTDBALLNCCY_" + i));
					
					flt552.remove("AMTLNCCY_" + i);
					flt552.remove("FXTOTAMT_" + i);
					flt552.remove("FXTOTNTDBAL_" + i);
					flt552.remove("NTDBALLNCCY_" + i);
					
					cryrowlist.add(cryMap);
					i++;
					}else {
						i++;
					}
				}
				
				
				for (String key : flt552.keySet()) {
					dataMap.put(key + "_552", flt552.get(key));
				}
				tmplist2 = new LinkedList<Row>();

				rowlist2 = new LinkedList<HashMap<String, String>>();

				tmp2.forEachRemaining(tmplist2::add);
				log.info("datalist()>>" + tmplist);

				for (Row row : tmplist2) {
					log.info("getValues>>" + row.getValues());
					rowMap = new HashMap<String, String>();
					// MVHImpl FxTotalAMT = new MVHImpl();
					// String[] groupBy = new String[]{"BAL"};
					// RowsGroup grp = MVHUtils.group(FxTotalAMT.getOccurs(), groupBy);
					// sum2 = grp.sum("BAL");
					rowMap.putAll(row.getValues());
					rowlist2.add(rowMap);
				}
				log.trace("FX ROWLIST >>{}",rowlist2);
				rowlist2.removeIf(Map::isEmpty);// 移除空白 OR null的Map
				//rowlist2.removeIf(Map -> null == Map.get("LNACCNO") || "".equals(Map.get("LNACCNO")));// 移除ACN==空白 OR
																										// null的Map
				dataMap.put("FX", rowlist2);
				dataMap.put("CRY", cryrowlist);
				int count = rowlist2.size();
				dataMap.put("FXNUM", count);
			}else {
				dataMap.put("TOPMSG_552", "NoCall");
			}

			// if(key.equals("AMTLNCCY_"+i)) {
			// cryMap.put(key, flt552.get(key));
			// cryMap.put("FXTOTAMT"+i,flt552.get("FXTOTAMT"+i));
			// }
			// cryrowlist.add(cryMap);
			// dataMap.put(key+"_552", flt320.get(key));
			// i++;

			// mvhimpl2.getFlatValues().put("TOTALBAL",
			// NumericUtil.fmtAmount(mvhimpl2.getFlatValues().get("TOTALBAL"), 2));

			int count ;

			if(null!=((TelcommResult) mvhimpl.getTableByName("DPFMNM1"))&&null!=((TelcommResult) mvhimpl.getTableByName("DPFMNM2"))) {
				count = rowlist.size()+rowlist2.size();
				dataMap.put("CMRECNUM", count);
			}else if(null!=((TelcommResult) mvhimpl.getTableByName("DPFMNM1"))) {
				count = rowlist.size();
				dataMap.put("CMRECNUM", count);
			}else if(null!=((TelcommResult) mvhimpl.getTableByName("DPFMNM2"))) {
				count = rowlist2.size();
				dataMap.put("CMRECNUM", count);
			}
			
			
			

			dataMap.put("TOPMSG", "");
			result = Boolean.TRUE;

		} catch (Exception e) {
			log.error("data_Retouch error >> {}",e);
		} finally {
			if (result) {
				dataMap.put("msgCode", "0");
			} else {
				dataMap.put("msgCode", "FE0003");
			}
		}
		return dataMap;
	}
}