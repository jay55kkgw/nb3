package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.netbank.util.DateUtils;
import com.netbank.util.NumericUtil;
import com.netbank.util.StrUtils;

import fstop.model.IMasterValuesHelper;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.model.TelcommResult;
import lombok.extern.slf4j.Slf4j;

/**
 * 主要用來
 * 1.丟入MVH前的前置處理，包含檢核;
 * 2.後製MVH回覆的訊息;
 * @author Hugo
 *
 */
@Service
@Slf4j
public class N016_Tel_Service extends Common_Tel_Service {

	@Override
	public HashMap pre_Processing(Map request) {
		// TODO Auto-generated method stub
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) {
		// TODO Auto-generated method stub
		return super.process(serviceID, request);
	}

	
	@Override
	public void setMsgCode(Map dataMap, Boolean data_Retouch_result) {
		// TODO Auto-generated method stub
		super.setMsgCode(dataMap, data_Retouch_result);
	}

	@Override
	public HashMap data_Retouch(MVH mvh) {
		MVHImpl mvhimpl = null;
		LinkedList<Row> tmplist = null;
		LinkedList<Row> tmplist2 = null;
		Boolean result = Boolean.FALSE;
		HashMap<String, Object> dataMap = new HashMap<String, Object>();
		HashMap<String, String> rowMap = new HashMap<String, String>();
		LinkedList<HashMap<String, String>> rowlist = null;
		LinkedList<HashMap<String, String>> rowlist2 = null;
		log.info("myn016","yes");
		try {
			mvhimpl = (MVHImpl) mvh;
			log.info("getFlatValues>>" + mvhimpl.getFlatValues());
			log.info("result.getInputQueryData()>>" + mvhimpl.getInputQueryData());
			
			Iterator<Row> tmp = ((TelcommResult)mvhimpl.getTableByName("DPFMNM1")).getOccurs().getRows().iterator();//TW
			Iterator<Row> tmp2 = ((TelcommResult)mvhimpl.getTableByName("DPFMNM2")).getOccurs().getRows().iterator();//FX
			tmplist = new LinkedList<Row>();
			tmplist2 = new LinkedList<Row>();
			rowlist = new LinkedList<HashMap<String, String>>();
			rowlist2 = new LinkedList<HashMap<String, String>>();
			tmp.forEachRemaining(tmplist::add);
			tmp2.forEachRemaining(tmplist2::add);
			log.info("datalist()>>" + tmplist);
			
			//TW 016 flatVs
			Map<String, String> flt016 = ((TelcommResult)mvhimpl.getTableByName("DPFMNM1")).getFlatValues();
			for(String key:flt016.keySet()) {
				dataMap.put(key+"_016", flt016.get(key));
			}
			//TW
			for (Row row : tmplist) {
				log.info("getValues>>" + row.getValues());
				rowMap = new HashMap<String, String>();
				row.setValue("ACN", StrUtils.hideaccount(row.getValue("ACN")));
				row.setValue("TRNINT", NumericUtil.fmtAmount((row.getValue("TRNINT").trim()),2));
				row.setValue("TRNPAL", NumericUtil.fmtAmount((row.getValue("TRNPAL").trim()),2));
				//BAL很特殊先被轉成xxx.xx 轉回來變成xxxxxx
				row.setValue("BAL", NumericUtil.fmtAmount((row.getValue("BAL").replace(".", "")),2));
				row.setValue("CURIPD", DateUtils.addSlash2(row.getValue("CURIPD")));
				row.setValue("DATTRN", DateUtils.addSlash2(row.getValue("DATTRN")));
				rowMap.putAll(row.getValues());
				rowlist.add(rowMap);
			}
			
			//FX 554 flatVs
			Map<String, String> flt554 = ((TelcommResult)mvhimpl.getTableByName("DPFMNM2")).getFlatValues();
			for(String key:flt554.keySet()) {
				dataMap.put(key+"_554", flt554.get(key));
			}
			//FX
			for (Row row : tmplist2) {
				log.info("getValues>>" + row.getValues());
				rowMap = new HashMap<String, String>();
				row.setValue("LNACCNO", StrUtils.hideaccount(row.getValue("LNACCNO")));
				row.setValue("CAPCOS", NumericUtil.fmtAmount(row.getValue("CAPCOS"),2));
				row.setValue("INTCOS", NumericUtil.fmtAmount(row.getValue("INTCOS"),2));
				row.setValue("LNUPDATE", row.getValue("LNUPDATE").replace("-", "/"));
				row.setValue("CUSPYDT", row.getValue("CUSPYDT").replace("-", "/"));
				rowMap.putAll(row.getValues());
				rowlist2.add(rowMap);
			}
			
			
			/*將 兩個table的TOPMSG 拿出來放到外面,讓外面能夠讀取判斷各自的顯示欄位
			 *最外圈的TOPMSG判斷讓他過才能導頁
			 * */
			dataMap.put("TOPMSG", "");
			dataMap.putAll(mvhimpl.getFlatValues());
			rowlist.removeIf(Map::isEmpty);// 移除空白 OR null的Map
			rowlist.removeIf(Map -> null == Map.get("ACN") || "".equals(Map.get("ACN")));
			rowlist2.removeIf(Map::isEmpty);// 移除空白 OR null的Map
			//rowlist2.removeIf(Map -> null == Map.get("ACN") || "".equals(Map.get("ACN")));
			dataMap.put("TW", rowlist);
			dataMap.put("FX", rowlist2);
			result = Boolean.TRUE;

		} catch (Exception e) {
			log.error("data_Retouch error >> {}",e);
		} finally {
			setMsgCode(dataMap, result);
		}
		return dataMap;
	}
}