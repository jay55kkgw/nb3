package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.netbank.util.DateUtils;
import com.netbank.util.NumericUtil;
import com.netbank.util.StrUtils;

import fstop.model.IMasterValuesHelper;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.model.TelcommResult;
import lombok.extern.slf4j.Slf4j;

/**
 * 主要用來
 * 1.丟入MVH前的前置處理，包含檢核;
 * 2.後製MVH回覆的訊息;
 * @author Hugo
 *
 */
@Service
@Slf4j
public class N014_Tel_Service extends Common_Tel_Service {

	@Override
	public HashMap pre_Processing(Map request) {
		// TODO Auto-generated method stub
		return super.pre_Processing(request);
	}

	@Override
	public HashMap process(String serviceID, Map request) {
		// TODO Auto-generated method stub
		return super.process(serviceID, request);
	}

//	@Override
//	public HashMap data_Retouch(MVH mvh) {
//		// TODO Auto-generated method stub
//		return super.data_Retouch(mvh);
//	}
//	
//	@Override
//	public HashMap process(String serviceID, Map request) {
//		// TODO Auto-generated method stub
////		return super.process(serviceID, request);
//		CommonService service = (CommonService)SpringBeanFactory.getBean(
//				String.format("%sService",
//						Optional.ofNullable(serviceID)
//						.filter(s -> StrUtils.isNotEmpty(s)).map(s -> s.toLowerCase())
//						.orElse(null)));
//		MVH mvh =service.doAction(request);
//		
//		return data_Retouch(mvh);
//	}
	
	@Override
	public void setMsgCode(Map dataMap, Boolean data_Retouch_result) {
		// TODO Auto-generated method stub
		super.setMsgCode(dataMap, data_Retouch_result);
	}
	
	
	@Override
	public HashMap data_Retouch(MVH mvh) {
		MVHImpl mvhimpl = null;
		LinkedList<Row> tmplist = null;
		LinkedList<Row> tmplist2 = null;
		Boolean result = Boolean.FALSE;
		HashMap<String, Object> dataMap = new HashMap<String, Object>();
		HashMap<String, String> rowMap = new HashMap<String, String>();
		LinkedList<HashMap<String, String>> rowlist = null;
		LinkedList<HashMap<String, String>> rowlist2 = null;
		log.info("myn014","yes");
		try {
			mvhimpl = (MVHImpl) mvh;
			log.info("getFlatValues>>" + mvhimpl.getFlatValues());
			log.info("result.getInputQueryData()>>" + mvhimpl.getInputQueryData());
			
			Iterator<Row> tmp = ((TelcommResult)mvhimpl.getTableByName("DPFMNM1")).getOccurs().getRows().iterator();//TW
			Iterator<Row> tmp2 = ((TelcommResult)mvhimpl.getTableByName("DPFMNM2")).getOccurs().getRows().iterator();//FX
			tmplist = new LinkedList<Row>();
			tmplist2 = new LinkedList<Row>();
			rowlist = new LinkedList<HashMap<String, String>>();
			rowlist2 = new LinkedList<HashMap<String, String>>();
			tmp.forEachRemaining(tmplist::add);
			tmp2.forEachRemaining(tmplist2::add);
			
			//TW 014 flatVs
			Map<String, String> flt014 = ((TelcommResult)mvhimpl.getTableByName("DPFMNM1")).getFlatValues();
			for(String key:flt014.keySet()) {
				dataMap.put(key+"_014", flt014.get(key));
			}
			//TW
			for (Row row : tmplist) {
				if(!row.getValue("SEQ").isEmpty()) {
					row.setValue("SEQ2",String.valueOf(Integer.parseInt(row.getValue("SEQ"))) );
				}
				log.info("getValues>>" + row.getValues());
				rowMap = new HashMap<String, String>();
				for (String key : row.getValues().keySet()) {
					if(!(row.getValues().get(key)).isEmpty()) {
						switch(key) {
							case "RAT":
								//利率加小數點
								row.setValue("RAT", NumericUtil.addDot(row.getValue("RAT"), 3));
								break;
							case "ACN":
								//帳號遮罩
								row.setValue("ACN", StrUtils.hideaccount(row.getValue("ACN")));
								break;
							case "AMTAPY":
								//金額類轉型
								row.setValue("AMTAPY",NumericUtil.fmtAmount((row.getValue("AMTAPY").trim()),2));
								break;
							case "BAL":
								//金額類轉型
								row.setValue("BAL",NumericUtil.fmtAmount((row.getValue("BAL")),2));
								break;
							case "AMTORLN":
								//金額類轉型
								row.setValue("AMTORLN",NumericUtil.fmtAmount(NumericUtil.addDot(row.getValue("AMTORLN"),2),2));
								break;
							case "DDT":
								row.setValue("DDT", (row.getValue("DDT")).trim());
								break;
							case "SEQ":
								
								break;
						}
						
					}
				}
				rowMap.putAll(row.getValues());
				//修改 Serializable Class Containing Sensitive Data
				rowMap.put("ITRUS", rowMap.get("ITRUSSN"));
				rowlist.add(rowMap);
			}
			
			//FX 554 flatVs
			Map<String, String> flt553 = ((TelcommResult)mvhimpl.getTableByName("DPFMNM2")).getFlatValues();
			for(String key:flt553.keySet()) {
				dataMap.put(key+"_553", flt553.get(key));
				log.debug(key+"_553");
			}
			//FX
			for (Row row : tmplist2) {
				log.info("getValues>>" + row.getValues());
				rowMap = new HashMap<String, String>();
				for (String key : row.getValues().keySet()) {
					if(!(row.getValues().get(key)).isEmpty()) {
						switch(key) {
							case "LNUPDATE":
								//原來的日期格式 Ex：2018-01-01 要轉成 2018/01/01
								row.setValue("LNUPDATE",(row.getValue("LNUPDATE").replace("-", "/")));
								break;
							case "NXTINTD":
								//原來的日期格式 Ex：2018-01-01 要轉成 2018/01/01
								row.setValue("NXTINTD",(row.getValue("NXTINTD").replace("-", "/")));
								break;
							case "CUSPYDT":
								//原來的日期格式 Ex：2018-01-01 要轉成 2018/01/01
								row.setValue("CUSPYDT",(row.getValue("CUSPYDT").replace("-", "/")));
								break;
							case "LNACCNO":
								//帳號遮罩
								row.setValue("LNACCNO", StrUtils.hideaccount(row.getValue("LNACCNO")));
								break;
							case "LNCOS":
								row.setValue("LNCOS",NumericUtil.fmtAmount((row.getValue("LNCOS")),2));
								break;
							case "PRNDAMT":
								row.setValue("PRNDAMT",NumericUtil.fmtAmount((row.getValue("PRNDAMT")),2));
								break;
								
						}
					}
				}
				
				rowMap.putAll(row.getValues());
				rowlist2.add(rowMap);
			}
			
			/*將 兩個table的TOPMSG 拿出來放到外面,讓外面能夠讀取判斷各自的顯示欄位
			 *最外圈的TOPMSG判斷讓他過才能導頁
			 * */
			dataMap.putAll(mvhimpl.getFlatValues());
			rowlist.removeIf(Map::isEmpty);// 移除空白 OR null的Map
			rowlist.removeIf(Map -> null == Map.get("ACN") || "".equals(Map.get("ACN")));
			rowlist2.removeIf(Map::isEmpty);// 移除空白 OR null的Map
			//rowlist2.removeIf(Map -> null == Map.get("LNREFNO") || "".equals(Map.get("LNREFNO")));
			dataMap.put("TW", rowlist);
			dataMap.put("FX", rowlist2);
			int count = rowlist.size()+rowlist2.size();
			dataMap.put("CMRECNUM", count);
			dataMap.put("TOPMSG", "");
			result = Boolean.TRUE;

		} catch (Exception e) {
			log.error("data_Retouch error >> {}",e);
		} finally {
			setMsgCode(dataMap, result);
		}
		return dataMap;
	}

	
	
	
	
	
}