package com.netbank.rest.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

//import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.Row;
import fstop.model.Rows;
import lombok.extern.slf4j.Slf4j;

/**
 * NA011 小額信貸申請資料查詢
 *
 */
@Service
@Slf4j
public class Na011_Tel_Service extends Common_Tel_Service  {
	// 因為實作NA011.java時的版本需要連eloan，所以將eloan ip設定環境變數
	// 後來改成正式套的NA011.java的版本不需連eloan，所以註解掉eloan相關屬性
//	@Value("${TBBeLoan.ip}")
//	String ip = "";
	
	@Override
	public HashMap pre_Processing(Map request) {
		return super.pre_Processing(request);
	}
	
	@Override
	public HashMap process(String serviceID, Map request) {
		// eloan ip 讀環境變數
//		request.put("TBBeLoanIp", ip);
//		log.trace("TBBeLoanIp >> {}", ip);
		return super.process(serviceID, request);
	}

	@Override
	public HashMap data_Retouch(MVH mvh) 
	{
		try
		{
			setQueryResultToRows(mvh);
			
			renameColumn(mvh);
		}
		catch (Exception e)
		{
			log.error("Na011_Tel_Service data_Retouch error", e);
		}
		return super.data_Retouch(mvh);
	}
	
	@Override
	public void setMsgCode(Map dataMap, Boolean data_Retouch_result) {
		super.setMsgCode(dataMap, data_Retouch_result);
	}
	
	
	/**
	 * 為了解決 DB 有查詢到資料，REC 內卻沒值的問題
	 * 原因是 NA011.java 內並沒將資料庫查詢結果放入 Row，導致 CommonService.data_Retouch 的 REC 為空
	 * trace NA011 發現查詢結果，以 key DPFMNM1 儲存在 MVHImpl 內的 HashMap
	 * 為了將查詢結果放入放入REC欄位，在此處手動把 DPFMNM1 的資料加到 Rows 中
	 * 便可以在 data_Retouch 將查詢結果放入 REC
	 * 
	 * @param mvh
	 * @return
	 */
	private void setQueryResultToRows(MVH mvh) throws Exception
	{
		try
		{
			MVHImpl result = (MVHImpl) mvh;
			MVHImpl tempMvh = new MVHImpl();
			if(mvh.getTableByName("DPFMNM1") instanceof MVHImpl)
			{
				tempMvh = (MVHImpl) mvh.getTableByName("DPFMNM1");
				Rows occurs = tempMvh.getOccurs();
				result.getOccurs().addRows(occurs);
			}
		}
		catch (Exception e)
		{
			log.error("Na011_Tel_Service.setQueryResultToRows error", e);
			throw new Exception();
		}
	}
	
	
	/**
	 * 將class欄位改名為Clz
	 * 
	 * @param occurs
	 * @return
	 */
	private void renameColumn(MVH mvh) throws Exception
	{
		try
		{
			Rows occurs = ((MVHImpl) mvh).getOccurs();
			for(int i = 0; i < occurs.getSize(); i++)
			{
				Row rows = occurs.getRow(i);
				Set<String> column = rows.getColumnNames();
				Map<String, String> row = rows.getValues();
				if(column.contains("class"))
				{
					row.put("Clz", row.get("class"));
					row.remove("class");
				}
			}
		}
		catch (Exception e)
		{
			log.error("Na011_Tel_Service.renameColumn error", e);
			throw new Exception();
		}
	}
	
}
