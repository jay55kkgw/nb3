package fstop.services.batch;

import fstop.exception.TopMessageException;
import fstop.exception.UncheckedException;
import fstop.services.ServiceBindingException;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

@Slf4j
public class DispatchBatchExecute {


    public BatchResult invoke(String methodName, List<String> paramList) {

        try {
            return (BatchResult) getClass().getMethod(methodName, new Class[]{List.class})
                    .invoke(this, new Object[]{paramList});
        } catch (TopMessageException e) {
            throw e;
        } catch (UncheckedException e) {
            throw e;
        } catch (InvocationTargetException e) {
            if (e.getTargetException() instanceof TopMessageException)
                throw (TopMessageException) e.getTargetException();

            throw new ServiceBindingException("DispatchService 執行錯誤 !, [" + methodName + "]", e.getTargetException());

        } catch (Exception e) {
            log.error("DispatchService 執行有誤 !!", e);
            throw new ServiceBindingException("無法連結對應的 method, [" + methodName + "]", e);
        }
    }
}
