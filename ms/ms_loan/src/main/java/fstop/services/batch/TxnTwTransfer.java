package fstop.services.batch;


import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netbank.util.ESAPIUtil;

import encryptUtil.EncryptUtil;
import fstop.aop.advice.LoggerHelper;
import fstop.exception.TopMessageException;
import fstop.exception.UncheckedException;
import fstop.model.MVHImpl;
import fstop.orm.dao.AdmMsgCodeDao;
import fstop.orm.dao.TxnTwSchPayDao;
import fstop.orm.dao.TxnTwSchPayDataDao;
import fstop.orm.po.TXNTWSCHPAY;
import fstop.orm.po.TXNTWSCHPAYDATA;
import fstop.orm.po.TXNTWSCHPAYDATA_PK;
import fstop.services.BookingAware;
import fstop.services.TransferNotice;
import fstop.services.batch.annotation.Batch;
import fstop.services.impl.SmsOtp;
import fstop.util.DateTimeUtils;
import fstop.util.JSONUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 台幣預約轉帳
 *
 * @author Owner
 */
@Slf4j
@RestController
@Batch(id="batch.TxnTwTransfer", name = "台幣預約交易一扣", description = "台幣預約交易一扣")
public class TxnTwTransfer extends DispatchBatchExecute implements BatchExecute {

	private DecimalFormat fmt = new DecimalFormat("#0");


	@Autowired
	ScheduleTransferNotice scheduleTransferNotice;

	@Autowired
	TxnTwSchPayDataDao txnTwSchPayDataDao; // 預約資料

	@Autowired
	TxnTwSchPayDao txnTwSchPayDao; // 預約主檔

	@Autowired
	AdmMsgCodeDao admMsgCodeDao;

	@Autowired
	SmsOtp smsOtp;

	// // 預約轉帳需通知客戶錯誤代表清單
	// private String transRecode[] =
	// {"3303","E024","E033","E062","E063","E064","Z011","Z008","Z007","Z033","Z031","Z030","Z001","Z002",
	// "Z003","Z012","Z016","Z019","Z020","Z024","Z025","Z026","Z029","Z022","Z032","Z034","ZX99","Z010"};

	// 預約轉帳需通知客戶錯誤代號清單
	/**
	 * private String transRecode[] =
	 * {"3303","E024","E033","E062","E063","E064","Z011","Z008","Z007","Z033","Z031","Z030","Z001","Z002",
	 * "Z003","Z012","Z016","Z019","Z020","Z024","Z025","Z026","Z029","Z022","Z032","Z034","ZX99","Z010","E038"};
	 * 20200110將清單註解 保留 E033,E062,E063,E064,E038 20200420加入E185
	 */
//	private String transRecode[] = { "E033", "E062", "E063", "E064", "E038", "E185" };
	
	//20201021 更新 移除E038 增加E006 E024 
	private String transRecode[] = { "E033", "E062", "E063", "E064","E185","E006","E024" };

    @RequestMapping(value = "/batch/txntwtransfer", method = {RequestMethod.POST},
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @Override
    public BatchResult execute(@RequestBody List<String> args) {
    	List<String> safeArgs = ESAPIUtil.validStrList(args);
    	
        log.info(ESAPIUtil.vaildLog("safeArgs: {}"+ JSONUtils.toJson(safeArgs)));

        //  第一個為這個 bean 的 method Name
        String methodName = safeArgs.get(0);
        List paramList = new ArrayList();
        paramList.addAll(safeArgs);
        paramList.remove(0);
        log.info(ESAPIUtil.vaildLog("methodName: {}, param list: " + JSONUtils.toJson(paramList)+methodName));
        return invoke(methodName, paramList);
    }

    /**
     * dpschno 為執行的預約編號
     * DPSCHTXDATE 預約日期
     *
     * @return
     */
    public BatchResult executeone(List<String> args) {
    	log.debug("args>>{}",args);
        String DPSCHNO = args.get(0);
        String DPSCHTXDATE = args.get(1);
        String DPUSERID = args.get(2);
        String SEQTRN = args.get(3);
        
        
        boolean sendResult = false;
        try {

            sendResult = send(DPSCHNO, DPSCHTXDATE,DPUSERID ,SEQTRN );
        } catch (Exception e) {
            log.error("執行 Batch 有誤 !", e);
        }

        Map<String, Object> data = new HashMap();

        data.put("COUNTER", new BatchCounter());

        BatchResult result = new BatchResult();
        result.setSuccess(sendResult);
        result.setBatchName("TxnTwTransfer_executeone");
        result.setData(data);

        return result;
    }


    private boolean send(String DPSCHNO, String DPSCHTXDATE , String DPUSERID , String SEQTRN) {
        Date startTime = new Date();
        log.info("開始執行, DPSCHNO: " + DPSCHNO + ", DPSCHTXDATE: " + DPSCHTXDATE + "," + DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", startTime));

        TXNTWSCHPAYDATA_PK pk = new TXNTWSCHPAYDATA_PK();
        pk.setDPSCHNO(DPSCHNO);
        pk.setDPSCHTXDATE(DPSCHTXDATE);
        pk.setDPUSERID(DPUSERID);
        TXNTWSCHPAYDATA s = txnTwSchPayDataDao.findById(pk);

        String adopid = s.getADOPID();

        //20201221更新  將狀態改成3 , 以防出問題時二扣再度call起
        updateStatus(s, "3" , "");

        Map<String, String> params = new HashMap<String, String>();

        Map info = new HashMap();
        Optional<List<TXNTWSCHPAY>> osch = null;
		TXNTWSCHPAY schdata = null;
		String reqinfo = "";
		String reqinfo_addTXTime = "";
		String digest ="";
		String mac ="";
		Boolean checkResult = true;
		try {
			osch = txnTwSchPayDao.findSCHPAY(s.getPks().getDPSCHNO(), s.getADOPID(), s.getPks().getDPUSERID());
			int size = osch.map(List::size).orElse(0);
			int position = 1;
			int getPosition = 0;
			switch (size) {
				case 0:
					log.error("未搜尋到對應的 TXNTWSCHPAY, DPSCHNO: " + s.getPks().getDPSCHNO());
					return false;
				case 1:
					schdata = osch.get().get(0);
					reqinfo = schdata.getDPTXINFO();
					reqinfo_addTXTime = reqinfo + s.getPks().getDPSCHTXDATE();
					digest = EncryptUtil.toEncS1(reqinfo_addTXTime, "UTF-8").substring(0, 4).toUpperCase();
					mac = s.getMAC();
					
					log.info(ESAPIUtil.vaildLog("APmac :" + digest));
					log.info(ESAPIUtil.vaildLog("DBmac :" + mac));
					checkResult = digest.equalsIgnoreCase(mac);
					getPosition = 0 ;
					break;
				default :
					log.warn("Find "+ size +" Rows Has Same DPSCHNO : "+ s.getPks().getDPSCHNO() 
							 + " , DPUSERID : "+ s.getPks().getDPUSERID() +" , ADOPID : "+ s.getADOPID());
					for(TXNTWSCHPAY eachData:osch.get()) {
						log.info("Check "+ position + " DATA's MAC");
						reqinfo = eachData.getDPTXINFO();
						reqinfo_addTXTime = reqinfo + s.getPks().getDPSCHTXDATE();
						digest = EncryptUtil.toEncS1(reqinfo_addTXTime, "UTF-8").substring(0, 4).toUpperCase();
						mac = s.getMAC();
						
						log.info(ESAPIUtil.vaildLog("APmac :" + digest));
						log.info(ESAPIUtil.vaildLog("DBmac :" + mac));
						checkResult = digest.equalsIgnoreCase(mac);
						if(true==checkResult) {
							getPosition=position-1;
							schdata=osch.get().get(getPosition);
							break;
						}
						position++;
					}
					break;
			}
			
			
			if(!checkResult) {
				log.error("MAC檢核不相同(DPSCHNO: " + s.getPks().getDPSCHNO() + " DPSCHTXDATE:"
						+ s.getPks().getDPSCHTXDATE() + " DPUSERID:" + s.getPks().getDPUSERID() + ")");
				updateStatus(s, "1", "MAC_FAIL");
				return false;
			}
			
			Map mt = JSONUtils.json2map(reqinfo);
			
			if (!mt.containsKey("LOGINTYPE")) {
				mt.put("LOGINTYPE", osch.get().get(getPosition).getLOGINTYPE());
			}
			
			// 加上交易序號
			mt.put("SEQTRN", s.getADTXNO());

			info.putAll(mt);
			log.info(ESAPIUtil.vaildLog("預約執行參數: " + reqinfo));

//            if (osch.isPresent()) {
//                schdata = osch.get();
//                String reqinfo = schdata.getDPTXINFO();
//                //reqinfo+DPSCHTXDATE => MAC
//                
//                reqinfo_addTXTime = reqinfo+s.getPks().getDPSCHTXDATE();
//                
//               	String digest = EncryptUtil.toEncS1(reqinfo_addTXTime, "UTF-8").substring(0,4);
//                String mac = s.getMAC();
//                
//                log.info(ESAPIUtil.vaildLog("APmac :" + digest));
//				log.info(ESAPIUtil.vaildLog("DBmac :" + mac));
//                
//                // check MAC
//				if (!mac.equalsIgnoreCase(digest)) {
//					log.error("MAC檢核不相同(DPSCHNO: " + s.getPks().getDPSCHNO() + " DPSCHTXDATE:"
//							+ s.getPks().getDPSCHTXDATE() + " DPUSERID:" + s.getPks().getDPUSERID() + ")");
//					updateStatus(s, "1", "MAC_FAIL");
//					
//					return false;
//				}
//
//                Map mt = JSONUtils.json2map(reqinfo);
//                
//                //TODO:加上交易序號 KEY的值還不確定 要等電文更新後才能確定
//                mt.put("SEQTRN",s.getADTXNO());
//                
//                if(!mt.containsKey("LOGINTYPE")) {
//                	mt.put("LOGINTYPE", osch.get().getLOGINTYPE());
//                }
//                
//                info.putAll(mt);
//                log.info(ESAPIUtil.vaildLog("預約執行參數: " + reqinfo ));
//            } else {
//                log.info("未搜尋到對應的 TXNTWSCHPAY, DPSCHNO: " + s.getPks().getDPSCHNO());
//            }
        } catch (Exception e) {
        	updateStatus(s, "1" , "Z011");
            throw new RuntimeException("組合上傳的 tita 錯誤.", e);
        }

        params.putAll(info);

        if ("0".equals(params.get("FGTXWAY"))) {  //交易密碼
            params.put("PINKEY", StrUtils.repeat("F2", 8));
//            if(params.containsKey("__PINNEW") ) {
//                params.put("PINNEW", params.get("__PINNEW"));
//            }
        } else if ("1".equals(params.get("FGTXWAY"))) {  //ikey
            params.put("XMLCA", s.getXMLCA());
            params.put("XMLCN", s.getXMLCN());
        }


        params.put("CUSIDN", s.getPks().getDPUSERID());
        params.put("ADOPID", s.getADOPID());
        params.put("TXID", s.getADOPID());
//20190827 by hugo 避免CERTACN 經過MAC2.java 時會被覆蓋 此參數必帶
        params.put("__SCHID", fmt.format(schdata.getDPSCHID()));
//        params.put("__SCHID", fmt.format(s.getDPSCHID()));
//        params.put("__REQID", fmt.format(s.getDPTXINFO()));
        params.put("__TRANS_STATUS", "SEND");  //@@@@@
        //TODO:並不是要當TXNRECORD的PK 故註解
        //params.put("__TRANS_RESEND_ADTXNO", s.getADTXNO());
//
        params.put("__SCHNO", s.getPks().getDPSCHNO());

        params.put("ADREQTYPE", "");   //寫入 TXNLOG 時, 空值 表示為即時, 需計入 ADMMONTHREPORT 中

        //log.debug("params: " + JSONUtils.toJson(params));

        //控制 TXNLOG
        try {
            params.put("ADREQTYPE", "B");
            params.put("UID", StrUtils.trim((String) params.get("CUSIDN")));

//            Map mtmp = JSONUtils.json2map(LoggerHelper.current().getRequestData());
//            mtmp.put("ADREQTYPE", "B");
//            mtmp.put("UID", StrUtils.trim((String) params.get("CUSIDN")));
//            params.putAll(mtmp);
            params.put("#TXNLOG", "TRUE");
            if ("NB".equals(params.get("LOGINTYPE"))) {
				paramMatchNB(params);
				// log.info("NB params >> "+params);
			} else if ("MB".equals(params.get("LOGINTYPE"))) {
				paramMatchMB(params);
				// log.info("MB params >> "+params);
			}
        } catch (Exception e) {
        }
        try {
            LoggerHelper.current().setUid(StrUtils.trim((String) params.get("CUSIDN")));
            LoggerHelper.current().setAdopid(StrUtils.trim((String) params.get("ADOPID")));
            LoggerHelper.current().setRequestData(JSONUtils.map2json(params));
        } catch (Exception e) {
        }
        //*****

        //20201221更新  將狀態改成3 , 以防出問題時二扣再度call起
        updateStatus(s, "3" , "");

        boolean isSuccess = false;
        try {
            isSuccess = reproduceTransaction(adopid, params, scheduleTransferNotice,s);
            LoggerHelper.clear();
        } catch (Exception e) {
            log.error("發生錯誤.", e);
        }

        if (isSuccess) {
            updateStatus(s, "0", "");
        } else {
            //已在rereproduceTransaction save過  移除
        }

        Date endTime = new Date();
        log.info(ESAPIUtil.vaildLog("執行結果: " + params.get("__SCHID") + ", " + (isSuccess ? "成功" : "失敗")
                + ", " + DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", endTime)));

        return isSuccess;
    }

    /*
0成功
1失敗
2其他(處理中) ‘’未執行 (預設)
     */
    private void updateStatus(TXNTWSCHPAYDATA s, String processCode , String excode) {
        txnTwSchPayDataDao.updateStatus(s, processCode ,excode);
    }

    /**
     * 重新發送預約交易, 使用 service bean
     *
     * @return
     */
    private boolean reproduceTransaction(String adopid, Map params, TransferNotice trNotice ,TXNTWSCHPAYDATA s) {
        boolean result = false;
        BookingAware service = null;
        Date now = new Date();
        String date ="";
        String time = "";
        String tita = "";
        String tota = "";
        String fee = "000";
        
        try {
            service = (BookingAware) SpringBeanFactory.getBean(adopid.toLowerCase() + "Service");

            log.info(ESAPIUtil.vaildLog("reproduceTransaction params: " +
                    JSONUtils.toJson(params)));
            
//            String p = (String)params.get("__PINNEW");
//            log.info("__PINNEW: " + p);
//            log.info("  PINNEW: " + params.get("PINNEW"));
//            params.remove("__PINNEW");
//            params.put("PINNEW", p);
//            log.info("  PINNEW2: " + params.get("PINNEW"));
//            log.info("  params: " + params);
            
            tita = JSONUtils.toJson(params);
            MVHImpl mvh = service.online(params, trNotice);
            log.info(ESAPIUtil.vaildLog("service.online: " + JSONUtils.toJson(mvh)));
            tota = JSONUtils.toJson(mvh.getFlatValues());
            if (StrUtils.isNotEmpty(mvh.getFlatValues().get("FEE"))) {
				fee = mvh.getFlatValues().get("FEE");
			}
			s.setDPEFEE(fee);
            result = true;
        } catch (ClassCastException e) {
            log.error("對應的BEAN不為BookingAware.", e);
        } catch (NoSuchBeanDefinitionException e) {
            log.error("沒有對應的 BEAN.", e);
        } catch (Exception e) {
            //log.error("台幣轉帳失敗. (DPSCHID: " + params.get("__DPSCHID") + ")", e);
            log.error(ESAPIUtil.vaildLog("台幣轉帳失敗. (DPSCHID: " + params.get("__SCHID") + "), " + e.getMessage()));
            //控制 TXNLOG
            try {
            	s.setDPEFEE(fee);
                Throwable e_root = getRootCause(e);
                if (e_root instanceof TopMessageException) {
                	String errorCode = StrUtils.trim(((TopMessageException) e_root).getMsgcode());
                	String autoResend = admMsgCodeDao.findAUTORESEND(errorCode);
                	//如果能自動重送,狀態為2(處理中),不能則為1(失敗)
                	if("Y".equalsIgnoreCase(autoResend)) {
                		s.setDPEXCODE(errorCode);
						s.setDPTXSTATUS("2");
						// updateStatus(s, "2", errorCode);
                	}else if("N".equalsIgnoreCase(autoResend)){
                		s.setDPEXCODE(errorCode);
						s.setDPTXSTATUS("1");
						// updateStatus(s, "1", errorCode);
                	}else if("".equalsIgnoreCase(autoResend)) { //資料庫找不到此錯誤代碼 先用FE0001
                		log.info("Can't find this error in Database !!!!");
                		s.setDPEXCODE("FE0001");
						s.setDPTXSTATUS("1");
						// updateStatus(s, "1", "FE0001");
                	}
                    LoggerHelper.current().setExceptionCode(errorCode);
                  //start Update
					for(int i =0; i <transRecode.length;i++) {
						if(e.getMessage().equals(transRecode[i])) {
							
							params.put("FuncType", "2");
							MVHImpl smsotp = (MVHImpl)smsOtp.doAction(params);
						}		
					}
					//end
                } else {
                	s.setDPEXCODE("ZX99");
					s.setDPTXSTATUS("1");
                	//將錯誤訊息寫進tota(?
					tota = e_root.getMessage();
                    LoggerHelper.current().setExceptionCode("ZX99");
                }
            } catch (Exception ex) {
            }
            //*** 控制TXNLOG
            result = false;
        }finally {
        	
        	s.setDPTITAINFO(tita);
        	s.setDPTOTAINFO(tota);
        	s.setDPTXDATE(DateTimeUtils.format("yyyyMMdd",new Date()));
        	s.setDPTXTIME(DateTimeUtils.format("HHmmss",new Date()));
        	txnTwSchPayDataDao.update(s);
        	
        }
        
        return result;
    }


    private Throwable getRootCause(Throwable cause) {
        if (cause instanceof UncheckedException) {
            Throwable rootCause = ((UncheckedException) cause).getRootCause();
            Throwable result = null;
            result = (rootCause != null ? rootCause : cause);

            return result;
        } else {
            return (cause.getCause() != null ? cause.getCause() : cause);
        }
    }
    
    private void paramMatchMB(Map<String, String> params) {
		Set<String> keyList = params.keySet();
		for (String key : keyList) {
			if (key.startsWith("#") && !"#TXNLOG".equals(key)) {
				// #ADTXACNO=ACN
				String valueKey = params.get(key);
				if (null != params.get(valueKey)) {
					params.put(key, params.get(valueKey));
				}
			}
		}
	}

	private void paramMatchNB(Map<String, String> params) {
		params.put("#ADTXACNO", params.get("ACN_OUT"));
		params.put("#ADTXAMT", params.get("PALPAY"));
		params.put("#ADSVBH", params.get("ADSVBH"));
		params.put("#ADAGREEF", params.get("ADAGREEF"));
		params.put("#ADCURRENCY", "TWD");
	}
    
}

