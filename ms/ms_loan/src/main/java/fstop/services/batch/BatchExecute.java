package fstop.services.batch;

import java.util.List;

public interface BatchExecute {
	public BatchResult execute(List<String> params);
}
