package fstop.services.impl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.netbank.rest.model.JSPUtils;

import fstop.core.BeanUtils;
import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.TxnLoanApplyDao;
import fstop.orm.po.TXNLOANAPPLY;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.telcomm.cmd.CommandUtils;
import fstop.util.DateTimeUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;

import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;
/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class NA01 extends CommonService  {
//	private Logger logger = Logger.getLogger(getClass());
	
	String NA01_PATH = "/TBB/nnb/applyfile";

	@Autowired
	private TxnLoanApplyDao txnLoanApplyDao;
	
	private Map loansetting;
	
//	@Required
//	public void setTxnLoanApplyDao(TxnLoanApplyDao txnLoanApplyDao) {
//		this.txnLoanApplyDao = txnLoanApplyDao;
//	}
//	public void setLoansetting(Map loansetting) {
//		this.loansetting = loansetting;
//	}
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map _params) { 
		
		MVHImpl result = new MVHImpl();
		Map<String, String> params = _params;
		
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		String tf = DateTimeUtils.format("HHmmss", d);
		String uploadpath = (String) params.get("UPLOADPATH");
		params.put("DATE", df);
		params.put("TIME", tf);	
		
		log.info("start to check CMSSL");
		if("0".equals(params.get("FGTXWAY"))) {  //使用交易密碼, 做預約
//			String TransPassUpdate = (String)params.get("TRANSPASSUPDATE");
//			String Phase2 = (String)params.get("PHASE2");
			TelCommExec n950CMTelcomm;
//			if(StrUtils.isNotEmpty(TransPassUpdate))
//			{
//				if(TransPassUpdate.equals("1") || Phase2.equals("ON"))
//				{
					n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n951CMTelcomm");
//				}
//				else
//				{
//					n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n950CMTelcomm");
//				}
//			}
//			else
//			{
//				n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n950CMTelcomm");
//			}
			MVHImpl n950Result = n950CMTelcomm.query(params);
			//若驗證失敗, 則會發動 TopMessageException
		}		
		log.info("finish to check CMSSL");
		
		String FILE1 = (String) params.get("FILE1");
		String FILE11 = (String) params.get("FILE11");
		String FILE2 = (String) params.get("FILE2");				
		String FILE3 = (String) params.get("FILE3");				
		String fgtxway = params.get("FGTXWAY").toString();
		if(params.containsKey("pkcs7Sign") && "1".equals(fgtxway)) {
			List<String> commands = new ArrayList();
			commands.add("XMLCA()");
			commands.add("XMLCN()");
			
			CommandUtils.doBefore(commands, params);
			
			if(!StrUtils.trim(params.get("XMLCN")).toUpperCase().startsWith(StrUtils.trim(params.get("UID")).toUpperCase())) {
				if(removefile(uploadpath + FILE1)) log.warn("remove pic1 successful");
				else log.warn("remove pic1 fail");				
				if(removefile(uploadpath + FILE11)) log.warn("remove pic11 successful");
				else log.warn("remove pic11 fail");
				if(removefile(uploadpath + FILE2)) log.warn("remove pic2 successful");					
				else log.warn("remove pic2 fail");
				if(removefile(uploadpath + FILE3)) log.warn("remove pic3 successful");					
				else log.warn("remove pic3 fail");				
				throw TopMessageException.create("Z089");
			}
		}			
		String SEQNO=String.valueOf(txnLoanApplyDao.countLoanRecord(d)+1);
		if(SEQNO.length()<2)
			SEQNO="0"+SEQNO;
		String RCVNO="LOAN"+DateTimeUtils.format("yyyyMMdd", new Date())+SEQNO;
		params.put("RCVNO", RCVNO);
		String APPNAME=JSPUtils.convertFullorHalf((String)params.get("APPNAME"), 1);
		params.put("APPNAME", APPNAME);
		String ADDR1=JSPUtils.convertFullorHalf((String)params.get("ADDR1"), 1);
		params.put("ADDR1", ADDR1);
		String ADDR2=JSPUtils.convertFullorHalf((String)params.get("ADDR2"), 1);
		params.put("ADDR2", ADDR2);	
		String CAREERNAME=JSPUtils.convertFullorHalf((String)params.get("CAREERNAME"), 1);
		params.put("CAREERNAME", CAREERNAME);
		String ADDR4=JSPUtils.convertFullorHalf((String)params.get("ADDR4"), 1);
		params.put("ADDR4", ADDR4);
		String PRECARNAME=JSPUtils.convertFullorHalf((String)params.get("PRECARNAME"), 1);
		params.put("PRECARNAME", PRECARNAME);
		String RELNAME1=JSPUtils.convertFullorHalf((String)params.get("RELNAME1"), 1);
		params.put("RELNAME1", RELNAME1);
		String RELNAME2=JSPUtils.convertFullorHalf((String)params.get("RELNAME2"), 1);
		params.put("RELNAME2", RELNAME2);
		String RELNAME3=JSPUtils.convertFullorHalf((String)params.get("RELNAME3"), 1);
		params.put("RELNAME3", RELNAME3);
		String RELNAME4=JSPUtils.convertFullorHalf((String)params.get("RELNAME4"), 1);
		params.put("RELNAME4", RELNAME4);
		String OTHCONM1=JSPUtils.convertFullorHalf((String)params.get("OTHCONM1"), 1);
		params.put("OTHCONM1", OTHCONM1);
		String MOTHCONM1=JSPUtils.convertFullorHalf((String)params.get("MOTHCONM1"), 1);
		params.put("MOTHCONM1", MOTHCONM1);
		TXNLOANAPPLY attrib = new TXNLOANAPPLY();
		log.debug(ESAPIUtil.vaildLog("RCVNO:"+RCVNO));
		attrib.setRCVNO(RCVNO);
		log.debug(ESAPIUtil.vaildLog("APPKIND:"+(String)params.get("APPKIND")));
		attrib.setAPPKIND((String)params.get("APPKIND"));
		log.debug(ESAPIUtil.vaildLog("APPAMT:"+(String)params.get("APPAMT")));
		attrib.setAPPAMT((String)params.get("APPAMT"));
		log.debug(ESAPIUtil.vaildLog("PURPOSE:"+(String)params.get("PURPOSE")));
		attrib.setPURPOSE((String)params.get("PURPOSE"));
		log.debug(ESAPIUtil.vaildLog("PERIODS:"+(String)params.get("PERIODS")));		
		attrib.setPERIODS((String)params.get("PERIODS"));
		log.debug(ESAPIUtil.vaildLog("BANKID:"+(String)params.get("BANKID")));		
		attrib.setBANKID((String)params.get("BANKID"));
		log.debug(ESAPIUtil.vaildLog("BANKNA:"+(String)params.get("BANKNA")));		
		attrib.setBANKNA((String)params.get("BANKNA"));
		log.debug(ESAPIUtil.vaildLog("SEX:"+(String)params.get("SEX")));		
		attrib.setSEX((String)params.get("SEX"));
		log.debug(ESAPIUtil.vaildLog("APPNAME:"+(String)params.get("APPNAME")));		
		attrib.setAPPNAME(APPNAME);    	
		log.debug(ESAPIUtil.vaildLog("APPIDNO:"+(String)params.get("APPIDNO")));		
		attrib.setAPPIDNO((String) params.get("APPIDNO"));
		log.debug(ESAPIUtil.vaildLog("BRTHDT:"+(String)params.get("BRTHDT")));		
		attrib.setBRTHDT((String) params.get("BRTHDT"));
		log.debug(ESAPIUtil.vaildLog("EDUS:"+(String)params.get("EDUS")));		
		attrib.setEDUS((String) params.get("EDUS"));
		log.debug(ESAPIUtil.vaildLog("MARITAL:"+(String)params.get("MARITAL")));		
		attrib.setMARITAL((String) params.get("MARITAL"));
		log.debug(ESAPIUtil.vaildLog("CHILDNO:"+(String)params.get("CHILDNO")));		
		attrib.setCHILDNO((String) params.get("CHILDNO"));
		log.debug(ESAPIUtil.vaildLog("CPHONE:"+(String)params.get("CPHONE")));		
		attrib.setCPHONE((String) params.get("CPHONE"));
		log.debug(ESAPIUtil.vaildLog("PHONE_01:"+(String)params.get("PHONE_01")));		
		attrib.setPHONE_01((String) params.get("PHONE_01"));
		log.debug(ESAPIUtil.vaildLog("PHONE_02:"+(String)params.get("PHONE_02")));		
		attrib.setPHONE_02((String) params.get("PHONE_02"));
		log.debug(ESAPIUtil.vaildLog("PHONE_03:"+(String)params.get("PHONE_03")));		
		attrib.setPHONE_03((String) params.get("PHONE_03"));
		log.debug(ESAPIUtil.vaildLog("PHONE_11:"+(String)params.get("PHONE_11")));		
		attrib.setPHONE_11((String) params.get("PHONE_11"));
		log.debug(ESAPIUtil.vaildLog("PHONE_12:"+(String)params.get("PHONE_12")));		
		attrib.setPHONE_12((String) params.get("PHONE_12"));
		log.debug(ESAPIUtil.vaildLog("HOUSE:"+(String)params.get("HOUSE")));		
		attrib.setHOUSE((String) params.get("HOUSE"));
		log.debug(ESAPIUtil.vaildLog("MONEY11:"+(String)params.get("MONEY11")));		
		attrib.setMONEY11((String) params.get("MONEY11"));
		log.debug(ESAPIUtil.vaildLog("MONEY12:"+(String)params.get("MONEY12")));		
		attrib.setMONEY12((String) params.get("MONEY12"));
		log.debug(ESAPIUtil.vaildLog("ZIP1:"+(String)params.get("ZIP1")));		
		attrib.setZIP1((String) params.get("ZIP1"));
		log.debug(ESAPIUtil.vaildLog("CITY1:"+(String)params.get("CITY1")));		
		attrib.setCITY1((String) params.get("CITY1"));
		log.debug(ESAPIUtil.vaildLog("ZONE1:"+(String)params.get("ZONE1")));		
		attrib.setZONE1((String) params.get("ZONE1"));
		log.debug(ESAPIUtil.vaildLog("ADDR1:"+(String)params.get("ADDR1")));		
		attrib.setADDR1(ADDR1);
		log.debug(ESAPIUtil.vaildLog("ZIP2:"+(String)params.get("ZIP2")));		
		attrib.setZIP2((String) params.get("ZIP2"));
		log.debug(ESAPIUtil.vaildLog("CITY2:"+(String)params.get("CITY2")));		
		attrib.setCITY2((String) params.get("CITY2"));
		log.debug(ESAPIUtil.vaildLog("ZONE2:"+(String)params.get("ZONE2")));		
		attrib.setZONE2((String) params.get("ZONE2"));
		log.debug(ESAPIUtil.vaildLog("ADDR2:"+(String)params.get("ADDR2")));		
		attrib.setADDR2(ADDR2);	
		attrib.setZIP3("");
		attrib.setCITY3("");
		attrib.setZONE3("");
		attrib.setADDR3("");				
		log.debug(ESAPIUtil.vaildLog("EMAIL:"+(String)params.get("EMAIL")));		
		attrib.setEMAIL((String) params.get("EMAIL"));
		log.debug(ESAPIUtil.vaildLog("CAREERNAME:"+(String)params.get("CAREERNAME")));		
		attrib.setCAREERNAME(CAREERNAME);
		log.debug(ESAPIUtil.vaildLog("CAREERIDNO:"+(String)params.get("CAREERIDNO")));		
		attrib.setCAREERIDNO((String) params.get("CAREERIDNO"));
		log.debug(ESAPIUtil.vaildLog("ZIP4:"+(String)params.get("ZIP4")));		
		attrib.setZIP4((String) params.get("ZIP4"));
		log.debug(ESAPIUtil.vaildLog("CITY4:"+(String)params.get("CITY4")));		
		attrib.setCITY4((String) params.get("CITY4"));
		log.debug(ESAPIUtil.vaildLog("ZONE4:"+(String)params.get("ZONE4")));		
		attrib.setZONE4((String) params.get("ZONE4"));
		log.debug(ESAPIUtil.vaildLog("ADDR4:"+(String)params.get("ADDR4")));		
		attrib.setADDR4(ADDR4);
		log.debug(ESAPIUtil.vaildLog("PHONE_41:"+(String)params.get("PHONE_41")));		
		attrib.setPHONE_41((String) params.get("PHONE_41"));
		log.debug(ESAPIUtil.vaildLog("PHONE_42:"+(String)params.get("PHONE_42")));		
		attrib.setPHONE_42((String) params.get("PHONE_42"));
		log.debug(ESAPIUtil.vaildLog("PHONE_43:"+(String)params.get("PHONE_43")));		
		attrib.setPHONE_43((String) params.get("PHONE_43"));
		log.debug(ESAPIUtil.vaildLog("OFFICEY:"+(String)params.get("OFFICEY")));		
		attrib.setOFFICEY((String) params.get("OFFICEY"));
		log.debug(ESAPIUtil.vaildLog("OFFICEM:"+(String)params.get("OFFICEM")));		
		attrib.setOFFICEM((String) params.get("OFFICEM"));
		log.debug(ESAPIUtil.vaildLog("PROFNO:"+(String)params.get("PROFNO")));		
		attrib.setPROFNO((String) params.get("PROFNO"));
		log.debug(ESAPIUtil.vaildLog("INCOME:"+(String)params.get("INCOME")));		
		attrib.setINCOME((String) params.get("INCOME"));
		log.debug(ESAPIUtil.vaildLog("PRECARNAME:"+(String)params.get("PRECARNAME")));		
		attrib.setPRECARNAME(PRECARNAME);
		log.debug(ESAPIUtil.vaildLog("PRETKY:"+(String)params.get("PRETKY")));		
		attrib.setPRETKY((String) params.get("PRETKY"));
		log.debug(ESAPIUtil.vaildLog("PRETKM:"+(String)params.get("PRETKM")));		
		attrib.setPRETKM((String) params.get("PRETKM"));
		log.debug(ESAPIUtil.vaildLog("CKS1:"+(String)params.get("CKS1")));		
		attrib.setCKS1((String) params.get("CKS1"));
		log.debug(ESAPIUtil.vaildLog("RELTYPE1:"+(String)params.get("RELTYPE1")));		
		attrib.setRELTYPE1((String) params.get("RELTYPE1"));
		log.debug(ESAPIUtil.vaildLog("RELNAME1:"+(String)params.get("RELNAME1")));		
		attrib.setRELNAME1(RELNAME1);
		log.debug(ESAPIUtil.vaildLog("RELID1:"+(String)params.get("RELID1")));		
		attrib.setRELID1((String) params.get("RELID1"));
		log.debug(ESAPIUtil.vaildLog("CKS2:"+(String)params.get("CKS2")));		
		attrib.setCKS2((String) params.get("CKS2"));
		log.debug(ESAPIUtil.vaildLog("RELTYPE2:"+(String)params.get("RELTYPE2")));		
		attrib.setRELTYPE2((String) params.get("RELTYPE2"));
		log.debug(ESAPIUtil.vaildLog("RELNAME2:"+(String)params.get("RELNAME2")));		
		attrib.setRELNAME2(RELNAME2);
		log.debug(ESAPIUtil.vaildLog("RELID2:"+(String)params.get("RELID2")));		
		attrib.setRELID2((String) params.get("RELID2"));
		log.debug(ESAPIUtil.vaildLog("CKS3:"+(String)params.get("CKS3")));		
		attrib.setCKS3((String) params.get("CKS3"));
		log.debug(ESAPIUtil.vaildLog("RELTYPE3:"+(String)params.get("RELTYPE3")));		
		attrib.setRELTYPE3((String) params.get("RELTYPE3"));
		log.debug(ESAPIUtil.vaildLog("RELNAME3:"+(String)params.get("RELNAME3")));		
		attrib.setRELNAME3(RELNAME3);
		log.debug(ESAPIUtil.vaildLog("RELID3:"+(String)params.get("RELID3")));		
		attrib.setRELID3((String) params.get("RELID3"));
		log.debug(ESAPIUtil.vaildLog("CKS4:"+(String)params.get("CKS4")));		
		attrib.setCKS4((String) params.get("CKS4"));
		log.debug(ESAPIUtil.vaildLog("RELTYPE4:"+(String)params.get("RELTYPE4")));		
		attrib.setRELTYPE4((String) params.get("RELTYPE4"));
		log.debug(ESAPIUtil.vaildLog("RELNAME4:"+(String)params.get("RELNAME4")));		
		attrib.setRELNAME4(RELNAME4);
		log.debug(ESAPIUtil.vaildLog("RELID4:"+(String)params.get("RELID4")));		
		attrib.setRELID4((String) params.get("RELID4"));
		log.debug(ESAPIUtil.vaildLog("CKP1:"+(String)params.get("CKP1")));		
		attrib.setCKP1((String) params.get("CKP1"));
		log.debug(ESAPIUtil.vaildLog("OTHCONM1:"+(String)params.get("OTHCONM1")));		
		attrib.setOTHCONM1(OTHCONM1);
		log.debug(ESAPIUtil.vaildLog("OTHCOID1:"+(String)params.get("OTHCOID1")));		
		attrib.setOTHCOID1((String) params.get("OTHCOID1"));
		log.debug(ESAPIUtil.vaildLog("CKM1:"+(String)params.get("CKM1")));		
		attrib.setCKM1((String) params.get("CKM1"));
		log.debug(ESAPIUtil.vaildLog("MOTHCONM1:"+(String)params.get("MOTHCONM1")));		
		attrib.setMOTHCONM1(MOTHCONM1);
		log.debug(ESAPIUtil.vaildLog("MOTHCOID1:"+(String)params.get("MOTHCOID1")));		
		attrib.setMOTHCOID1((String) params.get("MOTHCOID1"));
		log.debug(ESAPIUtil.vaildLog("PAYSOURCE:"+(String)params.get("PAYSOURCE")));		
		attrib.setPAYSOURCE((String) params.get("PAYSOURCE"));
		log.debug(ESAPIUtil.vaildLog("ECERT:"+(String)params.get("ECERT")));		
		attrib.setECERT((String) params.get("ECERT"));
		log.debug(ESAPIUtil.vaildLog("FILE1:"+(String)params.get("FILE1")));		
		attrib.setFILE1((String) params.get("FILE1"));
		log.debug(ESAPIUtil.vaildLog("FILE11:"+(String)params.get("FILE11")));		
		attrib.setFILE11((String) params.get("FILE11"));
		log.debug(ESAPIUtil.vaildLog("FILE2:"+(String)params.get("FILE2")));		
		attrib.setFILE2((String) params.get("FILE2"));
		log.debug(ESAPIUtil.vaildLog("FILE3:"+(String)params.get("FILE3")));		
		attrib.setFILE3((String) params.get("FILE3"));
		attrib.setLASTDATE(DateTimeUtils.format("yyyyMMdd", new Date()));
		attrib.setLASTTIME(DateTimeUtils.format("HHmmss", new Date()));
		log.debug(ESAPIUtil.vaildLog("STATUS:"+(String)params.get("STATUS")));		
		attrib.setSTATUS((String) params.get("STATUS"));
		log.debug(ESAPIUtil.vaildLog("VERSION:"+(String)params.get("VERSION")));		
		attrib.setVERSION((String) params.get("VERSION"));
		log.debug(ESAPIUtil.vaildLog("IP:"+(String)params.get("IP")));		
		attrib.setIP((String) params.get("IP"));
		attrib.setLOGINTYPE("0");
		
		txnLoanApplyDao.save(attrib);
		describePoToParams(result, attrib);
		genfile(RCVNO,uploadpath,(String) params.get("FILE1"),(String) params.get("FILE11"),(String) params.get("FILE2"),(String) params.get("FILE3"));
		
		return result;		
	}
	public void genfile(String rcvno,String uploadpath,String FILE1,String FILE11,String FILE2,String FILE3) {
		
		log.info("@NA01@ genfile");
		
		List<TXNLOANAPPLY> result = txnLoanApplyDao.findByRCVNO(rcvno);
		//long d = new Date().getTime();
		//String tmp = "/TBB/nnb/applyfile";
		String tmp = NA01_PATH;
		String pathname = (String)loansetting.get("pathname");
		String FILE_TXT = rcvno+".TXT";
		File fnew = new File(tmp, FILE_TXT);
		fnew.setWritable(true,true);
		fnew.setReadable(true, true);
		fnew.setExecutable(true,true);
		String[] fields = new String[]{
				"APPKIND", 
				"APPAMT", 
				"PURPOSE", 
				"PERIODS",
				"BANKID",
				"BANKNA",
				"SEX",
				"APPNAME",
				"APPIDNO",
				"BRTHDT",
				"EDUS",
				"MARITAL",
				"CHILDNO",
				"CPHONE",
				"PHONE_01",
				"PHONE_02",
				"PHONE_03",
				"PHONE_11",
				"PHONE_12",
				"HOUSE",
				"MONEY11",
				"MONEY12",
				"ZIP1",
				"CITY1",
				"ZONE1",
				"ADDR1",
				"ZIP2",
				"CITY2",
				"ZONE2",
				"ADDR2",
				"ZIP3",
				"CITY3",
				"ZONE3",
				"ADDR3",
				"EMAIL",
				"CAREERNAME",
				"CAREERIDNO",
				"ZIP4",
				"CITY4",
				"ZONE4",
				"ADDR4",
				"PHONE_41",
				"PHONE_42",
				"PHONE_43",
				"OFFICEY",
				"OFFICEM",
				"PROFNO",
				"INCOME",
				"PRECARNAME",
				"PRETKY",
				"PRETKM",
				"CKS1",
				"RELTYPE1",
				"RELNAME1",
				"RELID1",
				"CKS2",
				"RELTYPE2",
				"RELNAME2",
				"RELID2",
				"CKS3",
				"RELTYPE3",
				"RELNAME3",
				"RELID3",
				"CKS4",
				"RELTYPE4",
				"RELNAME4",
				"RELID4",
				"CKP1",
				"OTHCONM1",
				"OTHCOID1",
				"CKM1",
				"MOTHCONM1",
				"MOTHCOID1",
				"ECERT",
				"PAYSOURCE",
				"VERSION",
				"IP"
		};
		
		//List<String> advImgs = new ArrayList();
		
		BufferedWriter w = null;
		OutputStreamWriter o = null;
		int cnt=0;
		try {
			o = new OutputStreamWriter(new FileOutputStream(fnew), "MS950");
			w = new BufferedWriter(o);
			for(TXNLOANAPPLY ad : result) {
				//int fc = 0;
				for(String f : fields) {
					String v = "";
					try {
						Object obj = PropertyUtils.getProperty(ad, f);
						v = obj.toString();
					}
					catch(Exception e){}
					w.write("<"+f+">"+StrUtils.trim(v));
					w.write("\n");
				}
				if(FILE1.length()>0)
					cnt++;
				if(FILE11.length()>0)
					cnt++;
				if(FILE2.length()>0)
					cnt++;
				if(FILE3.length()>0)
					cnt++;	
				w.write("<CNT>"+cnt);
				w.write("\n");
			}
		}
		catch(Exception e) {
			log.error("無法寫入 "+FILE_TXT, e);
		}
		finally {
			if(w != null) {
				try {
					w.close();
				}
				catch(Exception e){}
			}
			if(o != null) {
				try {
					o.close();
				}
				catch(Exception e){}
			}
		}
		if(!StrUtils.trim(pathname).endsWith("/"))
			pathname = StrUtils.trim(pathname) + "/";
		
		ftpFileToELOAN(pathname + FILE_TXT, fnew.getAbsolutePath());
		
		if(!StrUtils.trim(uploadpath).endsWith("/"))
			uploadpath = StrUtils.trim(uploadpath) + "/";
		ftpFileToELOAN(pathname + FILE1, uploadpath + FILE1);
		ftpFileToELOAN(pathname + FILE11, uploadpath + FILE11);
		ftpFileToELOAN(pathname + FILE2, uploadpath + FILE2);
		if(FILE3.length()>0)
			ftpFileToELOAN(pathname + FILE3, uploadpath + FILE3);

		
	}
	
	public boolean ftpFileToELOAN(String eloanPath, String localPath) {
			//boolean isSuccess = false;
//			log.info("@FTP@ " + " " + localPath + " " + eloanPath);
//	    	String localIP = IPUtils.getLocalIp();
//			String ip = (String)loansetting.get("ftp.ip");
//			FtpBase64 ftpBase64 = new FtpBase64(ip);
//			int rc=ftpBase64.upload(eloanPath, localPath);
//			if(rc!=0)
//			{
//				logger.debug("FTP 失敗(ELOAN 主機一次) FILE："+eloanPath);
//				CheckServerHealth checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
//				checkserverhealth.sendUnknowNotice("FTP 失敗(ELOAN 主機一次) FILE："+eloanPath);
//				rc=ftpBase64.upload(eloanPath, localPath);
//				if(rc!=0)
//				{	
//					logger.debug("FTP 失敗(ELOAN 主機二次) FILE："+eloanPath);
//					checkserverhealth = (CheckServerHealth)SpringBeanFactory.getBean("checkserverhealth");
//					checkserverhealth.sendUnknowNotice("FTP 失敗(ELOAN 主機二次) FILE："+eloanPath);
//					return(false);
//				}
//				else
//				{	
//					try
//					{
//						File DelFile = new File(localPath);
//						if(localPath.indexOf(".TXT")<0)
//						{	
//							FileUtils.copyFileTo(DelFile, NA01_PATH);
//					    	RuntimeExec runtimeExec=null;
//					    	if(localIP.equals("10.16.22.17"))
//					    		runtimeExec=new RuntimeExec(NA01_PATH,"chown -R apt003:apgrp "+NA01_PATH+"/");
//					    	else
//					    		runtimeExec=new RuntimeExec(NA01_PATH,"chown -R dcp001:dcpgrp "+NA01_PATH+"/");				    		
//					    	if(runtimeExec.getcommstat()!=0)
//							{
//								logger.debug("chown  exec command error");
//							}																			
//							DelFile.delete();
//							logger.debug("刪除圖檔成功:"+localPath);
//						}	
//					}
//					catch(Exception e)
//					{
//						logger.debug("複製或刪除圖檔Path:"+localPath);
//						logger.debug("複製或刪除圖檔Fail:"+e.toString());
//					}								
//					return(true);				
//				}
//			}
//			else
//			{
//				try
//				{
//					File DelFile = new File(localPath);
//					if(localPath.indexOf(".TXT")<0)
//					{	
//						FileUtils.copyFileTo(DelFile, NA01_PATH);
//				    	RuntimeExec runtimeExec=null;
//				    	if(localIP.equals("10.16.22.17"))
//				    		runtimeExec=new RuntimeExec(NA01_PATH,"chown -R apt003:apgrp "+NA01_PATH+"/");
//				    	else
//				    		runtimeExec=new RuntimeExec(NA01_PATH,"chown -R dcp001:dcpgrp "+NA01_PATH+"/");				    		
//				    	if(runtimeExec.getcommstat()!=0)
//						{
//							logger.debug("chown  exec command error");
//						}												
//						DelFile.delete();
//						logger.debug("刪除圖檔成功:"+localPath);
//					}	
//				}
//				catch(Exception e)
//				{
//					logger.debug("複製或刪除圖檔Path:"+localPath);
//					logger.debug("複製或刪除圖檔Fail:"+e.toString());
//				}					
//			}
		return true;
	}
	private void describePoToParams(MVHImpl result, TXNLOANAPPLY po) {
		try {
			result.getFlatValues().putAll(BeanUtils.describe(po));
		} catch (IllegalAccessException e) {
			log.error("NA01 TXNLOANAPPLY BeanUtils.describe Error.", e);
		} catch (InvocationTargetException e) {
			log.error("NA01 TXNLOANAPPLY BeanUtils.describe Error.", e);
		} catch (NoSuchMethodException e) {
			log.error("NA01 TXNLOANAPPLY BeanUtils.describe Error.", e);
		}
	}	
	public boolean removefile(String localPath) {
		try
		{
			File DelFile = new File(localPath);
			DelFile.setWritable(true,true);
			DelFile.setReadable(true, true);
			DelFile.setExecutable(true,true);
			DelFile.delete();
			log.warn("刪除圖檔成功:"+localPath);
			return(true);
		}
		catch(Exception RemoveFileEx)
		{
			log.warn("刪除圖檔失敗"+localPath);
			return(false);
		}		
	}	
}
