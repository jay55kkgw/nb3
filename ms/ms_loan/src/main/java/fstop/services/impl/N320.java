package fstop.services.impl;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.exception.TopMessageException;
import fstop.model.GrpColumns;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.MVHUtils;
import fstop.model.RowsSelecter;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N320 extends CommonService {
	@Autowired
	@Qualifier("n320Telcomm")
	private TelCommExec n320Telcomm;
	@Autowired
	@Qualifier("n552Telcomm")
	private TelCommExec n552Telcomm;

	// @Required
	// public void setN320Telcomm(TelCommExec telcomm) {
	// n320Telcomm = telcomm;
	// }
	//
	// @Required
	// public void setN552Telcomm(TelCommExec telcomm) {
	// n552Telcomm = telcomm;
	// }
	/**
	 * 
	 * @param params
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public MVH doAction(Map params) {
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);

		params.put("QDATE", df);

		MVHImpl result = new MVHImpl();

		int totalcount = 0;

		if ("GO".equals(params.get("N320GO"))) {
			log.trace("CALL N320");
			try {
				params.put("USERDATA_X50",params.get("USERDATA_X50_320"));
				TelcommResult helperN320 = n320Telcomm.query(params);
				result.addTable(helperN320, "DPFMNM1");
				totalcount += helperN320.getValueOccurs("ACN");
				
				RowsSelecter rowSelecter = new RowsSelecter(helperN320.getOccurs());
				DecimalFormat fmt = new DecimalFormat("0.00");
				Double amtfdp = (double) rowSelecter.sum("BAL");
				;
				helperN320.getFlatValues().put("TOTALBAL", fmt.format(amtfdp));
			} catch (TopMessageException e) {
				MVHImpl helper = new MVHImpl();
				helper.getFlatValues().put("TOPMSG", e.getMsgcode());
				helper.getFlatValues().put("ADMSGIN", e.getMsgin());
				helper.getFlatValues().put("ADMSGOUT", e.getMsgout());
				result.addTable(helper, "DPFMNM1");
			}
		}

		if ("GO".equals(params.get("N552GO"))) {
			log.trace("CALL N552");
			try {
				params.put("USERDATA_X50",params.get("USERDATA_X50_552"));
				TelcommResult helperN553 = n552Telcomm.query(params);
				result.addTable(helperN553, "DPFMNM2");
				totalcount += helperN553.getValueOccurs("LNACCNO");
				try {
					String[] groupBy = new String[] { "LNCCY" };
					Map<GrpColumns, Double> sum = MVHUtils.group(helperN553.getOccurs(), groupBy).sum("LNCOS");
					int ind = 1;
					for (Entry<GrpColumns, Double> ent : sum.entrySet()) {
						GrpColumns grpCols = ent.getKey();

						Map<String, String> m = MVHUtils.toFlatValues(grpCols, "AMT", ind);
						helperN553.getFlatValues().putAll(m);
						helperN553.getFlatValues().put("FXTOTAMT_" + ind, ent.getValue() + "");
						ind++;
					}
				} finally {
				}
				try {
					String[] groupBy = new String[] { "LNCCY" };
					Map<GrpColumns, Double> sum = MVHUtils.group(helperN553.getOccurs(), groupBy).sum("NTDBAL");
					int ind = 1;
					for (Entry<GrpColumns, Double> ent : sum.entrySet()) {
						GrpColumns grpCols = ent.getKey();

						Map<String, String> m = MVHUtils.toFlatValues(grpCols, "NTDBAL", ind);
						helperN553.getFlatValues().putAll(m);
						helperN553.getFlatValues().put("FXTOTNTDBAL_" + ind, ent.getValue() + "");
						ind++;
					}
				} finally {
				}
			} catch (TopMessageException e) {
				MVHImpl helper = new MVHImpl();
				helper.getFlatValues().put("TOPMSG", e.getMsgcode());
				helper.getFlatValues().put("ADMSGIN", e.getMsgin());
				helper.getFlatValues().put("ADMSGOUT", e.getMsgout());
				result.addTable(helper, "DPFMNM2");
			}
		}

		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));

		result.getFlatValues().put("CMRECNUM", totalcount + "");
		
		result.getFlatValues().put("N552GO", (String)params.get("N552GO"));
		result.getFlatValues().put("N320GO", (String)params.get("N320GO"));

		//log.trace("result>>" + result.dumpToString());
		return result;

	}
}
