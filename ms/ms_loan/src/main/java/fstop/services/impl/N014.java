package fstop.services.impl;

import java.util.Date;
import java.util.Map;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N014 extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("n553Telcomm")
	private TelCommExec n553Telcomm;
	
	@Autowired
	@Qualifier("n014Telcomm")
	private TelCommExec n014Telcomm;

//	@Required
//	public void setN014Telcomm(TelCommExec telcomm) {
//		n014Telcomm = telcomm;
//	}
//
//	@Required
//	public void setN553Telcomm(TelCommExec telcomm) {
//		n553Telcomm = telcomm;
//	}

	public MVH doAction(Map params) {
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		//String tf = DateTimeUtils.format("HHmmss", d);
		params.put("CURDATE", df);
		//params.put("TIME", tf);

		params.put("QDATE", df);
		
		MVHImpl result = new MVHImpl();
		
		int totalcount = 0;
		try {
			TelcommResult helperN014 = n014Telcomm.query(params);
			result.addTable(helperN014, "DPFMNM1");
			totalcount += helperN014.getValueOccurs("ACN");
		}
		catch(TopMessageException e) {
			MVHImpl helper = new MVHImpl();
			helper.getFlatValues().put("TOPMSG", e.getMsgcode());
			helper.getFlatValues().put("ADMSGIN", e.getMsgin());
			helper.getFlatValues().put("ADMSGOUT", e.getMsgout());
			result.addTable(helper, "DPFMNM1");		
		}
		
		try {
			TelcommResult helperN553 = n553Telcomm.query(params);
			result.addTable(helperN553, "DPFMNM2");
			totalcount += helperN553.getValueOccurs("LNACCNO");
		}
		catch(TopMessageException e) {
			MVHImpl helper = new MVHImpl();
			helper.getFlatValues().put("TOPMSG", e.getMsgcode());
			helper.getFlatValues().put("ADMSGIN", e.getMsgin());
			helper.getFlatValues().put("ADMSGOUT", e.getMsgout());
			result.addTable(helper, "DPFMNM2");		
		}
		
		result.getFlatValues().put("CMQTIME",
				DateTimeUtils.getDatetime(new Date()));
		result.getFlatValues().put("CMRECNUM", totalcount + "");
		
		return result;

	}
}
