package fstop.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.netbank.rest.model.CommonPools;
import com.netbank.rest.model.JSPUtils;
import com.netbank.util.CodeUtil;
import com.netbank.util.ESAPIUtil;

import encryptUtil.EncryptUtil;
import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.SysDailySeqDao;
import fstop.orm.dao.TxnReqInfoDao;
import fstop.orm.dao.TxnTwRecordDao;
import fstop.orm.dao.TxnTwSchPayDao;
import fstop.orm.dao.TxnTwSchPayDataDao;
import fstop.orm.dao.TxnTwScheduleDao;
import fstop.orm.dao.TxnUserDao;
import fstop.orm.po.ADMMAILLOG;
import fstop.orm.po.TXNREQINFO;
import fstop.orm.po.TXNTWRECORD;
import fstop.orm.po.TXNTWSCHPAY;
import fstop.orm.po.TXNTWSCHPAYDATA;
import fstop.services.BookingAware;
import fstop.services.CommonService;
import fstop.services.NoticeInfo;
import fstop.services.NoticeInfoGetter;
import fstop.services.OnLineTransferNotice;
import fstop.services.TelCommExecWrapper;
import fstop.services.TransferNotice;
import fstop.services.WriteLogInterface;
import fstop.services.mgn.B105;
import fstop.telcomm.TelCommExec;
import fstop.telcomm.cmd.CommandUtils;
import fstop.util.DateTimeUtils;
import fstop.util.SpringBeanFactory;
import fstop.util.StrUtils;
import lombok.extern.slf4j.Slf4j;
import topibs.utility.NumericUtil;

/**
 * 
 * @author Owner
 *
 */
@Slf4j
public class N3003 extends CommonService implements BookingAware, WriteLogInterface {
//	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	@Qualifier("n3003Telcomm")
	private TelCommExec n3003Telcomm;

	@Autowired
	private TxnTwRecordDao txnTwRecordDao;

	@Autowired
	private TxnTwSchPayDao txntwschpaydao;
	
	@Autowired
	private TxnTwSchPayDataDao txntwschpaydatadao;
	
	@Autowired
	private TxnTwScheduleDao txnTwScheduleDao;

	@Autowired
	private TxnReqInfoDao txnReqInfoDao;

	@Autowired
	private TxnUserDao txnUserDao;
	
	@Autowired
	private CommonPools commonPools;
	
	@Autowired
	private SysDailySeqDao sysDailySeqDao;
	
//	@Required
//	public void setTxnUserDao(TxnUserDao txnUserDao) {
//		this.txnUserDao = txnUserDao;
//	}
//	
//	@Required
//	public void setTxnReqInfoDao(TxnReqInfoDao txnReqInfoDao) {
//		this.txnReqInfoDao = txnReqInfoDao;
//	}
//
//	
//	@Required
//	public void setN3003Telcomm(TelCommExec telcomm) {
//		n3003Telcomm = telcomm;
//	}
//	
//	
//	@Required
//	public void setTxnTwRecordDao(TxnTwRecordDao txnTwRecordDao) {
//		this.txnTwRecordDao = txnTwRecordDao;
//	}
//
//	@Required
//	public void setTxnTwScheduleDao(TxnTwScheduleDao dao) {
//		this.txnTwScheduleDao = dao;
//	}	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVH doAction(Map params) { 	
		
		//String trin_acn = params.get("ACN").toString();
		//if(trin_acn.length() > 11)trin_acn = trin_acn.substring(trin_acn.length() - 11);
		//params.put("ACN", trin_acn);

		
		log.debug(ESAPIUtil.vaildLog("====CURDATE====" + params.get("CURDATE")));
		log.debug(ESAPIUtil.vaildLog("====CUSIDN====" + params.get("CUSIDN")));
		log.debug(ESAPIUtil.vaildLog("====ACN_OUT====" + params.get("ACN_OUT")));
		log.debug(ESAPIUtil.vaildLog("====ACN====" + params.get("ACN")));
		log.debug(ESAPIUtil.vaildLog("====SEQ====" + params.get("SEQ")));
		log.debug(ESAPIUtil.vaildLog("====PALPAY====" + params.get("PALPAY")));
		log.debug(ESAPIUtil.vaildLog("====PINNEW====" + params.get("PINNEW")));
		
		log.debug(ESAPIUtil.vaildLog("====CHKPAGE====" + params.get("CHKPAGE")));
		String str_CHKPAGE = params.get("CHKPAGE")==null?"":params.get("CHKPAGE").toString();
		if (str_CHKPAGE.equals("3003_2")){
		    //從3003_2來的 	提前還款
			log.debug(ESAPIUtil.vaildLog("MM FROM 3003_2 " + params.get("CHKPAGE")));
			params.put("PAY_MARK", " ");
		}else if (str_CHKPAGE.equals("30031_2")){
		    //從30031_2來的	結清
			log.debug("MM FROM 30031_2 " );
			params.put("PAY_MARK", "A");
		}else{
			
		}
		log.debug(ESAPIUtil.vaildLog("====PAY_MARK====" + params.get("PAY_MARK")  + "="));
		params.put("CHKPAGE", "");
	
		String fgtxdate = (String)params.get("FGTXDATE");
		log.debug(ESAPIUtil.vaildLog("N3003 FGTXDATE:"+fgtxdate));
		
		/*** 驗證交易密碼 - 開始 ***/		
		if("0".equals(params.get("FGTXWAY"))) {  //使用交易密碼
	
//			String TransPassUpdate = (String)params.get("TRANSPASSUPDATE");
//			String Phase2 = (String)params.get("PHASE2");
			TelCommExec n950CMTelcomm;
		
//			if(StrUtils.isNotEmpty(TransPassUpdate))
//			{
//				if(TransPassUpdate.equals("1") || Phase2.equals("ON"))
//				{
					n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n951CMTelcomm");
//				}
//				else
//				{
//					n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n950CMTelcomm");
//				}
//			}
//			else
//			{
//				n950CMTelcomm = (TelCommExec)SpringBeanFactory.getBean("n950CMTelcomm");
//			}
		
			// N950會發兩次所以需FGTXWAY=""，N950回應後再把FGTXWAY改成原值即可 2010.09.29
			String fgtxway = (String)params.get("FGTXWAY");
			params.put("FGTXWAY", "");
			MVHImpl n950Result = n950CMTelcomm.query(params);
		
			//若驗證失敗, 則會發動 TopMessageException
			params.put("FGTXWAY", fgtxway);					
		}
		/*** 驗證交易密碼 - 結束 ***/	
		
		
		
		
		
		if("1".equals(fgtxdate)) {  //即時
			B105 b105 = commonPools.b105;
//			B105 b105 = (B105)SpringBeanFactory.getBean("b105".toLowerCase());
			String StrPAYDATE = StrUtils.trim((String)params.get("PAYDATE")).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
			if(b105.isHOLIDAY(StrPAYDATE)){
	        	throw TopMessageException.create("Z036");
	        }
	        else {
			OnLineTransferNotice trNotice = new OnLineTransferNotice();
			return online(params, trNotice);
	        }
		}
		
		else if("2".equals(fgtxdate)) {  //預約,"2" 特定日,   "3" 預約　固定每月的 某一天	
			
			return booking(params);
		}
		
		return new MVHImpl();
	}
	
	public Map<String, String> scheduleObjectMapping() {
		Map<String, String> map = new HashMap();
		
		map.put("DPUSERID", "UID");
		map.put("DPWDAC", "ACN_OUT"); // 轉出帳號
		//map.put("DPSVBH", ""); // 轉入分行
		map.put("DPSVAC", "ACN"); // 轉入帳號, 使用原始輸入的來記錄
		map.put("DPTXAMT", "PALPAY");
		map.put("DPTXMEMO", "CMTRMEMO");
		
		map.put("FGTXWAY", "FGTXWAY");
		
		map.put("DPTXMAILS", "CMTRMAIL");
		map.put("DPTXMAILMEMO", "CMMAILMEMO"); // CMMAILMEMO 對應的欄位

		map.put("ADOPID", "ADOPID");

		map.put("DPTXCODE","FGTXWAY");

		
		
		return map;
	}

	@Override
	public MVHImpl booking(Map<String, String> params) {
		log.trace(ESAPIUtil.vaildLog("3003.booking.params: {}"+CodeUtil.toJson(params)));
		TXNTWSCHPAY txntwschpay = new TXNTWSCHPAY();
		
		// CERTACN
		String TxWay = params.get("FGTXWAY");
		String certACN = "";
		String sha1_Mac = "";
		String reqinfo_Str = "{}";
		
		if("0".equals(TxWay)){
			while(certACN.length() < 19)certACN += "2";
		}
		else if("1".equals(TxWay)){
			while(certACN.length() < 19)certACN += "3";
		}
		else if("2".equals(TxWay)){
			while(certACN.length() < 19)certACN += "5";
		}
		else if("3".equals(TxWay)){
			while(certACN.length() < 19)certACN += "7";
		}
		params.put("CERTACN", certACN);
		
		
		// 操作功能ID
		txntwschpay.setADOPID(params.get("ADOPID"));
		// 使用者ID
		txntwschpay.setDPUSERID(params.get("CUSIDN"));
		// 轉出帳號
		txntwschpay.setDPWDAC(params.get("ACN_OUT"));
		// 轉入行庫代碼
		txntwschpay.setDPSVBH("050");
		// 轉入帳號/繳費代號
		txntwschpay.setDPSVAC(params.get("ACN"));
		// 轉帳金額
		txntwschpay.setDPTXAMT(params.get("PALPAY"));
		// 備註
		txntwschpay.setDPTXMEMO(params.get("CMTRMEMO")==null?"" : params.get("CMTRMEMO"));
		// 發送Mail清單
		txntwschpay.setDPTXMAILS(params.get("CMTRMAIL")==null?"" : params.get("CMTRMAIL"));
		// Mail備註
		txntwschpay.setDPTXMAILMEMO(params.get("CMMAILMEMO")==null?"" : params.get("CMMAILMEMO"));
		// 交易機制
		txntwschpay.setDPTXCODE(TxWay);
		

		// 更新SCHCOUNT(每日預約計數)
		int scnt = txnUserDao.incSchcount((String)params.get("UID"));

		// 預約編號(YYMMDD+三位流水號) 改用UUID
		String dptmpschno = "";
//		String ymd = DateTimeUtils.format("yyMMdd", new Date());
//		dptmpschno = ymd + StrUtils.right("000" + scnt, 3) + "HH";
		dptmpschno = txnUserDao.getDptmpschno((String)params.get("UID"));
		// 賽入預約編號
//		String uuid = UUID.randomUUID().toString();
		txntwschpay.setDPSCHNO(dptmpschno);
		// 預約狀態
		txntwschpay.setDPTXSTATUS("0");
		
		// TODO DPTXINFO放不下這兩個欄位，先拿掉，若預約交易執行時需要再看怎麼存
		params.remove("pkcs7Sign");
		params.remove("jsondc");
		reqinfo_Str =  CodeUtil.toJson(params);
		sha1_Mac = EncryptUtil.toEncS1(reqinfo_Str, "UTF-8").substring(0,4);
		// 預約時上行電文必要資訊
		txntwschpay.setMAC(sha1_Mac);
		txntwschpay.setDPTXINFO(reqinfo_Str);
		txntwschpay.setMSADDR(TxnTwSchPayDao.MS_LOAN);
		// 系統別
		txntwschpay.setLOGINTYPE(params.get("LOGINTYPE"));

		// 寫入日期
		Date d = new Date();
		txntwschpay.setDPSDATE(DateTimeUtils.format("yyyyMMdd", d));
		txntwschpay.setDPSTIME(DateTimeUtils.format("HHmmss", d));

		txntwschpay.setLASTDATE(txntwschpay.getDPSDATE());
		txntwschpay.setLASTTIME(txntwschpay.getDPSTIME());

		B105 b105 = commonPools.b105;
//	B105 b105 = (B105)SpringBeanFactory.getBean("b105".toLowerCase());
		String cmdate = StrUtils.trim(params.get("PAYDATE")).replaceAll("/", "");
		log.debug("PAYDATE = " + cmdate);		
		
	    if(cmdate.length()==0){
	        throw TopMessageException.create("ZX99");
	    }else if(b105.isHOLIDAY(cmdate)){
	    	throw TopMessageException.create("Z036");
	    }
	    else {
			txntwschpay.setDPTXTYPE("S");
	    	txntwschpay.setDPPERMTDATE("");
	    	txntwschpay.setDPFDATE(cmdate);
	    	txntwschpay.setDPTDATE(cmdate);       
	    }
		
//		String f = params.get("FGTXDATE");
//		if ("2".equals(f)) { // 預約某一日
//			params.put("CMDATE", StrUtils.trim(params.get("CMDATE")).replaceAll("/", ""));
//			if (params.get("CMDATE").length() == 0) {
//				throw TopMessageException.create("ZX99");
//			} else {
//				txntwschpay.setDPTXTYPE("S");
//				txntwschpay.setDPPERMTDATE("");
//				txntwschpay.setDPFDATE(params.get("CMDATE"));
//				txntwschpay.setDPTDATE(params.get("CMDATE"));
//			}
//		} else if ("3".equals(f)) {
//			params.put("CMSDATE", StrUtils.trim(params.get("CMSDATE")).replaceAll("/", ""));
//			params.put("CMEDATE", StrUtils.trim(params.get("CMEDATE")).replaceAll("/", ""));
//			if (params.get("CMDD").length() == 0 || params.get("CMSDATE").length() == 0
//					|| params.get("CMEDATE").length() == 0) {
//				throw TopMessageException.create("ZX99");
//			} else {
//				txntwschpay.setDPTXTYPE("C");
//				txntwschpay.setDPPERMTDATE(params.get("CMDD"));
//				txntwschpay.setDPFDATE(params.get("CMSDATE"));
//				txntwschpay.setDPTDATE(params.get("CMEDATE"));
//			}
//		}
		
		if (params.containsKey("pkcs7Sign") && "1".equals(params.get("FGTXWAY"))) {
			List<String> commands = new ArrayList();
			commands.add("XMLCA()");
			commands.add("XMLCN()");

			CommandUtils.doBefore(commands, params);

			if (!StrUtils.trim(params.get("XMLCN")).toUpperCase()
					.startsWith(StrUtils.trim(params.get("UID")).toUpperCase())) {
				throw TopMessageException.create("Z089");
			}

			txntwschpay.setXMLCA(StrUtils.trim(params.get("XMLCA")));
			txntwschpay.setXMLCN(StrUtils.trim(params.get("XMLCN")));
		}
		
		log.trace(ESAPIUtil.vaildLog("txntwschpay: {}"+CodeUtil.toJson(txntwschpay)));
		txntwschpaydao.save(txntwschpay);
		
//		String reqinfo_addTXTime ="";
//		String sha1_MacTime ="" ;
//		
//		//改寫預約副檔mac 防止批次因DB時間竄改執行錯誤日期
//        List<TXNTWSCHPAYDATA> dataList = txntwschpaydatadao.findByDPSCHNO(dptmpschno);
//        for(TXNTWSCHPAYDATA data:dataList) {
//        	params.put("DPSCHTXDATE", data.getPks().getDPSCHTXDATE());
//        	reqinfo_addTXTime =  CodeUtil.toJson(params);
//        	sha1_MacTime = EncryptUtil.toEncS1(reqinfo_addTXTime, null).substring(0,4);
//        	data.setMAC(sha1_MacTime);
//        	txntwschpaydatadao.update(data);
//        }
		
		MVHImpl result =  new MVHImpl();
		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		
		try {
			result.getFlatValues().put("CMTXTIME", DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", d));
			result.getFlatValues().put("TOPMSG", "0000");
			result.getFlatValues().put("MSGCOD", "0000");
		}
		catch(Exception e){
			log.error("",e);
		}
		
		return result;
	}
	
//	@Override
//	public MVHImpl booking(Map<String, String> params) {
//		TXNTWSCHPAY txntwschedule = new TXNTWSCHPAY();
//		
//		//產生 CERTACN update by Blair -------------------------------------------- Start
//		String TxWay = params.get("FGTXWAY");
//		String certACN = "";
//
//		if("0".equals(TxWay)){
//			while(certACN.length() < 19)certACN += "2";
//		}
//		else if("1".equals(TxWay)){
//			while(certACN.length() < 19)certACN += "3";
//		}
//		else if("2".equals(TxWay)){
//			while(certACN.length() < 19)certACN += "5";
//		}
//		else if("3".equals(TxWay)){
//			while(certACN.length() < 19)certACN += "7";
//		}
//
//		params.put("CERTACN", certACN);
//		//產生 CERTACN update by Blair -------------------------------------------- End
//		
//		log.info("***************************************預約編號***************************************************");
//		//更新SCHCOUNT(每日預約計數)
//		int scnt = txnUserDao.incSchcount((String)params.get("UID"));
//		
//		//預約編號(YYMMDD+三位流水號)
//		String dptmpschno = "";
//		String ymd = DateTimeUtils.format("yyMMdd", new Date());
//		dptmpschno = ymd + StrUtils.right("000" + scnt, 3); 
//		
//		//賽入預約編號
//		txntwschedule.setDPSCHNO(dptmpschno);
//		log.info("***************************************預約編號***************************************************");
//
//		 //預約某一日
//		//getBean -> commonPools
//		B105 b105 = commonPools.b105;
////		B105 b105 = (B105)SpringBeanFactory.getBean("b105".toLowerCase());
//		String cmdate = StrUtils.trim(params.get("PAYDATE")).replaceAll("/", "");
//		log.debug("PAYDATE = " + cmdate);		
//		
//        if(cmdate.length()==0){
//            throw TopMessageException.create("ZX99");
//        }else if(b105.isHOLIDAY(cmdate)){
//        	throw TopMessageException.create("Z036");
//        }
//        else {
//        	txntwschedule.setDPPERMTDATE("");
//    		txntwschedule.setDPFDATE(cmdate);
//    		txntwschedule.setDPTDATE(cmdate);       
//        }
//		
//		
//		
//		params.put("ADREQTYPE", "S");   //寫入 TXNLOG 時, S 表示為預約
//
//		Map<String, String> mapping = scheduleObjectMapping();
//		for(String key : mapping.keySet()) {
//			String v = mapping.get(key);
//			try {
//				log.debug("key =" + key + "	get(v)=" +params.get(v)  );
//				BeanUtils.setProperty(txntwschedule, key, params.get(v));
//			} catch (IllegalAccessException e) {
//				throw new ToRuntimeException("", e);
//			} catch (InvocationTargetException e) {
//				throw new ToRuntimeException("", e);
//			}
//		}
//
//
//
//		if(params.containsKey("pkcs7Sign") && "1".equals(params.get("FGTXWAY"))) {
//			List<String> commands = new ArrayList();
//			commands.add("XMLCA()");
//			commands.add("XMLCN()");
//			
//			CommandUtils.doBefore(commands, params);
//			
//			if(!StrUtils.trim(params.get("XMLCN")).toUpperCase().startsWith(StrUtils.trim(params.get("UID")).toUpperCase())) {
//				throw TopMessageException.create("Z089");
//			}
//
//			txntwschedule.setXMLCA(StrUtils.trim(params.get("XMLCA")));
//			txntwschedule.setXMLCN(StrUtils.trim(params.get("XMLCN")));
//		}
//
//		txntwschedule.setDPTXINFO(titainfo.getREQID());
//		String sha1 = DigestUtils.sha1(titainfo.getREQINFO()).substring(0, 4);
//		txntwschedule.setMAC(sha1);
//
//		Date d = new Date();
//		txntwschedule.setDPSDATE(DateTimeUtils.format("yyyyMMdd", d));
//		txntwschedule.setDPSTIME(DateTimeUtils.format("HHmmss", d));
//		
//		txntwschedule.setDPTXSTATUS("0");
//		txntwschedule.setLASTDATE(txntwschedule.getDPSDATE());
//		txntwschedule.setLASTTIME(txntwschedule.getDPSTIME());
//
//		
//		log.debug("booking == ADOPID " + txntwschedule.getADOPID() );
//		log.debug("booking == DPFDATE " + txntwschedule.getDPFDATE() );
//		log.debug("booking == DPNEXTDATE " + txntwschedule.getDPNEXTDATE() );
//		log.debug("booking == DPPERMTDATE " + txntwschedule.getDPPERMTDATE() );
//		log.debug("booking == DPSCHNO " + txntwschedule.getDPSCHNO() );
//		log.debug("booking == DPSDATE " + txntwschedule.getDPSDATE() );
//		log.debug("booking == DPSTIME " + txntwschedule.getDPSTIME() );
//		log.debug("booking == DPSVAC " + txntwschedule.getDPSVAC() );
//		log.debug("booking == DPSVBH " + txntwschedule.getDPSVBH() );
//		log.debug("booking == DPTDATE " + txntwschedule.getDPTDATE() );
//		log.debug("booking == DPTXAMT " + txntwschedule.getDPTXAMT() );
//		log.debug("booking == DPTXCODE " + txntwschedule.getDPTXCODE() );
//		log.debug("booking == DPTXINFO " + txntwschedule.getDPTXINFO() );
//		String str_DPTXMAILMEMO = txntwschedule.getDPTXMAILMEMO()==null?"":txntwschedule.getDPTXMAILMEMO();
//		String str_DPTXMAILS = txntwschedule.getDPTXMAILS()==null?"":txntwschedule.getDPTXMAILS();
//		txntwschedule.setDPTXMAILMEMO(str_DPTXMAILMEMO);
//		txntwschedule.setDPTXMAILS(str_DPTXMAILS);
//		log.debug("booking == DPTXMAILMEMO " + str_DPTXMAILMEMO );
//		log.debug("booking == DPTXMAILS " + str_DPTXMAILS );
//		log.debug("booking == DPTXMEMO " + txntwschedule.getDPTXMEMO() );
//		log.debug("booking == DPTXSTATUS " + txntwschedule.getDPTXSTATUS() );
//		log.debug("booking == DPUSERID " + txntwschedule.getDPUSERID() );
//		log.debug("booking == DPWDAC " + txntwschedule.getDPWDAC() );
//		log.debug("booking == LASTDATE " + txntwschedule.getLASTDATE() );
//		log.debug("booking == LASTTIME " + txntwschedule.getLASTTIME() );
//		log.debug("booking == LOGINTYPE " + txntwschedule.getLOGINTYPE() );
//		log.debug("booking == MAC " + txntwschedule.getMAC() );
//		log.debug("booking == XMLCA " + txntwschedule.getXMLCA() );
//		log.debug("booking == XMLCN " + txntwschedule.getXMLCN() );
//		
//		txnTwScheduleDao.save(txntwschedule);
//		log.debug("booking == 15" );
//		
//		MVHImpl result =  new MVHImpl();
//		
//		try {
//			result.getFlatValues().put("CMTXTIME", DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", d));
//		}
//		catch(Exception e){
//			log.error(e.toString());			
//		}
//		
//		return result;
//	}
	
	
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public MVHImpl online(final Map<String, String> params, TransferNotice trNotice) { 
		Date d = new Date();
		//params.put("CURDATE", DateTimeUtils.getCDateShort(d));
		//params.put("DDATE", DateTimeUtils.getCDateShort(d));
        
		log.debug("DateTimeUtils.getCDateShort(d) = " + DateTimeUtils.getCDateShort(d));

		String _PAYDATE = StrUtils.trim(params.get("PAYDATE")).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
		params.put("CURDATE", DateTimeUtils.getCDateShort(DateTimeUtils.parse("yyyyMMdd", _PAYDATE)));  //轉成民國年

		String _DATADATE = StrUtils.trim(params.get("DATADATE")).replaceAll("/", "");   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
		params.put("DDATE", DateTimeUtils.getCDateShort(DateTimeUtils.parse("yyyyMMdd", _DATADATE)));  //轉成民國年
	
		params.put("CUSIDN", params.get("CUSIDN"));
		//TelcommResult result = null;	
        
		
		
		log.debug(ESAPIUtil.vaildLog("FGTXWAY online1 = " + params.get("FGTXWAY")));
		
		
		
		
		
		
		/*** Execute n3003Telcomm ***/
		//result = n3003Telcomm.query(params);
		
		TelCommExec doexec = n3003Telcomm;		 
		final TelCommExecWrapper execwrapper = new TelCommExecWrapper(doexec);
		MVHImpl result = execwrapper.query(params);
		final TXNTWRECORD record = (TXNTWRECORD)writeLog(execwrapper, params, result);		
		
		final TelCommExecWrapper execwrapperFinal = execwrapper;
		
		trNotice.sendNotice(new NoticeInfoGetter() {

			public NoticeInfo getNoticeInfo() {
				log.debug("UID = " + params.get("UID")+ "DPMYEMAIL = " + params.get("DPMYEMAIL")+"CMTRMAIL = " + params.get("CMTRMAIL") );
				
				NoticeInfo result = new NoticeInfo(params.get("UID"), params.get("DPMYEMAIL"), params.get("CMTRMAIL"));
							
				if(StrUtils.trim((String)params.get("LOGINTYPE")).equals("MB"))
					//result.setTemplateName("CREDITCARD_MB");
					result.setTemplateName("PAYLOAN");
					else
					result.setTemplateName("PAYLOAN");
				
				
//				result.setDpretxstatus(record.getDPRETXSTATUS());
				result.setADTXNO(record.getADTXNO());
					
				result.setException(execwrapperFinal.getLastException());
				String datetime = record.getDPTXDATE() + record.getDPTXTIME();

				Date d = DateTimeUtils.parse("yyyyMMddHHmmss", datetime);
				String trantime = DateTimeUtils.format("yyyy/MM/dd HH:mm:ss", d);
	
				//result.getParams().put("#SEQ", params.get("SEQ"));
                String str_PAY_MARK = params.get("PAY_MARK");
				log.debug("str_PAY_MARK=" + str_PAY_MARK + "=");

				if (str_PAY_MARK.equals("A")){				
				    result.getParams().put("#TXNAME", "貸款提前結清");					
				}else {
				    result.getParams().put("#TXNAME", "貸款提前償還本金");					
				}
				    result.getParams().put("#TRANTIME", trantime);
				result.getParams().put("#SEQNO", record.getDPTXNO());
				
				String acn = record.getDPSVAC();
				log.debug("record.getDPSVAC() =" + record.getDPSVAC());
				try {

					//acn = StrUtils.left(acn, 6) + StrUtils.repeat("*", acn.length() - 9) + StrUtils.right(acn, 3);
					acn=JSPUtils.hideid(acn);
					log.debug("acn = " + acn);
				}
				catch(Exception e){}
				
				String outacn = record.getDPWDAC();
				log.debug("record.getDPWDAC() =" + record.getDPWDAC());
				try {
					//outacn = StrUtils.left(outacn, outacn.length() - 5) + "***" + StrUtils.right(outacn, 2);

					outacn=JSPUtils.hideaccount(outacn);
					log.debug("outacn = " + outacn);
				}
				catch(Exception e){}
				

				result.getParams().put("#ACN_OUT", outacn);
				
				result.getParams().put("#ACN", acn);
				result.getParams().put("#SEQ", params.get("SEQ"));
				String dptxamtfmt = NumericUtil.formatNumberString(record.getDPTXAMT(), 0);
				result.getParams().put("#PALPAY", dptxamtfmt);
				result.getParams().put("#MEMO", params.get("CMMAILMEMO"));
				
				ADMMAILLOG admmaillog = new ADMMAILLOG();
				admmaillog.setADACNO(record.getDPWDAC());
				
				String trans_status = (String)params.get("__TRANS_STATUS");
				if(StrUtils.isEmpty(trans_status)) {
					trans_status = "TWONLINE";
				}
				else if("SEND".equals(trans_status)) {
					trans_status = "TWSCH";
				}
				else if("RESEND".equals(trans_status)) {
					trans_status = "TWSCHRE";
				}
				
				admmaillog.setADBATCHNO(trans_status);
				admmaillog.setADMAILACNO(StrUtils.trim(params.get("CMTRMAIL")));
				admmaillog.setADUSERID(record.getDPUSERID());
				admmaillog.setADUSERNAME("");
				admmaillog.setADSENDTYPE("3");
				admmaillog.setADSENDTIME(datetime);
				admmaillog.setADSENDSTATUS("");
				
				result.setADMMAILLOG(admmaillog);
				
				return result;
			}
			
		});

		
		execwrapper.throwException();
		
		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		
		try {
			result.getFlatValues().put("CMTXTIME", 
					DateTimeUtils.getCDateTime(
							result.getValueByFieldName("TRNDATE"),
							result.getValueByFieldName("TRNTIME")));
		}
		catch(Exception e){
			log.error(e.toString());			
		}
		
		//if(result.getValueByFieldName("OUTACN").trim().length() == 4)
		//	throw TopMessageException.create(result.getValueByFieldName("OUTACN").trim());
		
		return result;
	}

	@Override
	public Object writeLog(TelCommExecWrapper execwrapper, Map<String, String> params, final MVHImpl result) {
		
		TXNTWRECORD record = execwrapper.createTxnTwRecord(sysDailySeqDao);
		
		String date = record.getLASTDATE();
		String time = record.getLASTTIME();
		
		if(result != null) {
			try {
				date = DateTimeUtils.format("yyyyMMdd", DateTimeUtils.getCDate2Date(result.getValueByFieldName("CURDATE")));
			}
			catch(Exception e){}
				
		}

		
		record.setDPUSERID(params.get("UID"));
		record.setADOPID(params.get("ADOPID"));
		record.setDPTXDATE(date);
		record.setDPTXTIME(time);
		record.setDPWDAC(params.get("ACN_OUT"));
		record.setDPSVBH("");
		record.setDPSVAC(params.get("ACN"));
		record.setDPTXAMT(params.get("PALPAY"));
		record.setDPTXMEMO(params.get("CMTRMEMO")==null?"" : params.get("CMTRMEMO"));
		record.setDPTXMAILS(params.get("CMTRMAIL")==null?"" : params.get("CMTRMAIL"));
		record.setDPTXMAILMEMO(params.get("CMMAILMEMO")==null?"" : params.get("CMMAILMEMO"));  //CMMAILMEMO 對應的欄位
		record.setDPEFEE("0");
		record.setDPTXNO("");  //跨行序號
		record.setDPTXCODE(params.get("FGTXWAY"));
//		record.setDPSCHNO(params.get("__SCHNO")==null?"" : params.get("__SCHNO"));
		
		TXNREQINFO titainfo = null;
//		if(record.getDPTITAINFO() == 0) {
//			titainfo = new TXNREQINFO();
//			titainfo.setREQINFO(BookingUtils.requestInfo(params));
//		}
		
		
		txnTwRecordDao.writeTxnRecord(titainfo, record);
	
		return record;
	}
}
