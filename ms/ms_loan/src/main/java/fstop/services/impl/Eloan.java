package fstop.services.impl;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.URL;
import java.security.cert.CertificateException;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.security.cert.X509Certificate;

import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.services.CommonService;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Value;

import com.google.api.client.json.Json;
import com.netbank.util.ESAPIUtil;


/*
 * 信用卡補上傳資料(身份證件/財力證明)-呼叫eln確認是否需要補件及取得補件類型與電話、補件號碼
 * 作者:曾勁順
 * 日期:110.06.10
 * 
 * */
@Slf4j
public class Eloan extends CommonService {

	@Value("${eln.uri}")
	private String elnurl;

	@Override
	public MVH doAction(Map _params) {
		
		log.info("===MS ELOAN.java start===");
		
		MVHImpl helper = new MVHImpl();
		Map<String, String> params = _params;
		
		/************************************/
		
		log.debug("RCVNO==>"+params.get("RCVNO"));
		log.debug("FNAME==>"+params.get("FNAME"));
		log.debug("CPRIMBIRTHDAY==>"+params.get("CPRIMBIRTHDAY"));

		String outputStr = "";
		if(params.get("FNAME") == null) {
			log.debug("=======Eloan part1=======");
			outputStr = "method=caOnlineSupCaseQuery&qtype=1&qitem="+(String)params.get("CUSIDN")+"&bdate="+(String)params.get("CPRIMBIRTHDAY");
		}else {
			log.debug("=======Eloan part2=======");
			outputStr = "method=caOnlineSupCaseQuery&qtype=2&rcvno="+(String)params.get("RCVNO")+"&fname="+(String)params.get("FNAME");			
		}
				
						   

		try {
		// 建立SSLContext物件，並使用我們指定的信任管理器初始化
			TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				public void checkClientTrusted(X509Certificate[] certs,	String authType) {
				}

				public void checkServerTrusted(X509Certificate[] certs,	String authType) {
				}

				public void checkClientTrusted(java.security.cert.X509Certificate[] arg0, String arg1) throws CertificateException {
					// TODO 自動產生方法 Stub
					
				}

				public void checkServerTrusted(java.security.cert.X509Certificate[] arg0, String arg1) throws CertificateException {
					// TODO 自動產生方法 Stub
					
				}
			} };
			SSLContext sslContext = SSLContext.getInstance("SSL");
			sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
			// 從上述SSLContext物件中得到SSLSocketFactory物件
			SSLSocketFactory ssf = sslContext.getSocketFactory();
			URL url = new URL(elnurl);
			HttpsURLConnection httpUrlConn = (HttpsURLConnection) url.openConnection();
			httpUrlConn.setSSLSocketFactory(ssf);
			httpUrlConn.setDoOutput(true);
			httpUrlConn.setDoInput(true);
			httpUrlConn.setUseCaches(false);
			httpUrlConn.setRequestMethod("POST");// 設定請求方式（GET/POST）
			
			// 當有資料需要提交時
			OutputStream outputStream = httpUrlConn.getOutputStream();
			outputStream.write(outputStr.getBytes("UTF-8"));				// 注意編碼格式，防止中文亂碼
			outputStream.flush();
			
			int responseCode = httpUrlConn.getResponseCode();
			
	        log.debug("POST request to URL: " + elnurl);
	        log.debug("POST outputStr     : " + outputStr);
	        log.debug("Response Code      : " + responseCode);
			
			// 將返回的輸入流轉換成字串
			InputStream inputStream = httpUrlConn.getInputStream();
			InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "utf-8");
			BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
			String str = null;
			StringBuffer strbuffer = new StringBuffer();
			while ((str = bufferedReader.readLine()) != null) {
				strbuffer.append(str);
			}

			// 釋放資源
			bufferedReader.close();
			inputStreamReader.close();
			inputStream.close();
			outputStream.close();
			httpUrlConn.disconnect();
			
			/*
			String json = strbuffer.toString();
			JSONObject jsonObject1 = JSONObject.fromObject(json);
			JSONArray jsonArray = (JSONArray) jsonObject1.get("data");
			JSONObject jsonObject2 = JSONObject.fromObject(jsonArray.get(0));
			log.debug("jsonObject2==>"+jsonObject2);
			*/
			
			

			
			
			
			String json = strbuffer.toString();
			/*
			JSONObject jsonObject1 = JSONObject.fromObject(json);
			JSONArray jsonArray = (JSONArray) jsonObject1.get("data");
			log.debug("size==>"+jsonArray.size());
			JSONObject jsonObject2 = new JSONObject();
			
			List<Map<String, String>> resultListMap = null;
			resultListMap = new ArrayList<Map<String, String>>();
			for(int i = 0; i < jsonArray.size(); i++){
				Map<String, String> map = new HashMap<String, String>();
				jsonObject2 = JSONObject.fromObject(jsonArray.get(i));
				
				if("E".equals((String)jsonObject2.get("YNFLG"))) {
					//ENRD
					log.debug("YNFLG==>"+jsonObject2.get("YNFLG"));
					helper.getFlatValues().put("YNFLG",String.valueOf(jsonObject2.get("YNFLG")));
					helper.getFlatValues().put("ERRCODE", "ENRD");	//查無資料 ENRD
				}else {
					//需要補件(Y)(PFLG：1缺身分證 2缺財力證明 3兩者都缺)
					map.put("PFLG", (String)jsonObject2.get("PFLG"));
					log.debug("PFLG==>"+(String)jsonObject2.get("PFLG"));
					map.put("MOBILE", (String)jsonObject2.get("MOBILE"));
					log.debug("MOBILE==>"+(String)jsonObject2.get("MOBILE"));
					map.put("RCVDATE", (String)jsonObject2.get("RCVDATE"));
					log.debug("RCVDATE==>"+(String)jsonObject2.get("RCVDATE"));
					map.put("RCVNO", (String)jsonObject2.get("RCVNO"));
					log.debug("RCVNO==>"+(String)jsonObject2.get("RCVNO"));
					map.put("DOCSTATUS", (String)jsonObject2.get("DOCSTATUS"));
					log.debug("DOCSTATUS==>"+(String)jsonObject2.get("DOCSTATUS"));
					map.put("CENCARD", (String)jsonObject2.get("CENCARD"));
					log.debug("CENCARD==>"+(String)jsonObject2.get("CENCARD"));
					map.put("YNFLG", (String)jsonObject2.get("YNFLG"));
					log.debug("YNFLG==>"+(String)jsonObject2.get("YNFLG"));
					map.put("CUSTID", (String)jsonObject2.get("CUSTID"));
					log.debug("CUSTID==>"+(String)jsonObject2.get("CUSTID"));
					
					resultListMap.add(map);
					//需要補件(Y)(PFLG：4超過當日補件次數 5需補件，但缺少電話)
					switch(String.valueOf(jsonObject2.get("PFLG"))) {
						case "4":
							helper.getFlatValues().put("ERRCODE", "FE0015");	//4超過當日補件次數
							break;
						case "5":
							helper.getFlatValues().put("ERRCODE", "FE0014");	//5需補件，但缺少電話
							break;
						default:
							// 新世代網路銀行修改，為了讓呼叫的系統能判別回應結果是否正確，加入 MSGCOD
							helper.getFlatValues().put("MSGCOD", "0000");
							break;
					}
				}
				
				
			}
			*/
			
			helper.getFlatValues().put("data", json);
			helper.getFlatValues().put("MSGCOD", "0000");
			
		} catch (ConnectException ce) {
			log.error("Weixin server connection timed out.");
		} catch (Exception e) {
			log.error("https request error:{}", e);
		}

		
		/************************************/
		
		log.info("===MS ELOAN.java end===");		
		return helper;

	}
	

//	/*
//	 * 截取標籤body中的文字
//	 * */
//	public String getBody(String val) {
//        String start = "<body>";
//        String end = "</body>";
//        int s = val.indexOf(start) + start.length();
//        int e = val.indexOf(end);
//      return val.substring(s, e);
//	}

}
