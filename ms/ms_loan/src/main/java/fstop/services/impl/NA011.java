package fstop.services.impl;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.netbank.util.ESAPIUtil;

import fstop.exception.TopMessageException;
import fstop.model.DBResult;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.orm.dao.TxnLoanApplyDao;
import fstop.orm.po.TXNLOANAPPLY;
import fstop.services.CommonService;
import fstop.util.DateTimeUtils;
import fstop.util.IPUtils;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONArray;

@Slf4j
public class NA011 extends CommonService {

	@Autowired
	private TxnLoanApplyDao txnLoanApplyDao;
	

	@Override
	public MVH doAction(Map params) {
		log.info("===NA011.java in===");
		
		MVHImpl helper = new MVHImpl();		
		int cmrecnum = 0;
		String uid="";
		uid = (String) params.get("UID");
		log.trace(ESAPIUtil.vaildLog("NA011 uid >> {}"+ uid));
		List<TXNLOANAPPLY> r=null;
		List<TXNLOANAPPLY> getRcvno =null;
		try {
			
			r = txnLoanApplyDao.findByCUSID(uid);
			log.trace(ESAPIUtil.vaildLog("NA011 txnLoanApplyDao.findByCUSID >> {}"+ r));
			MVHImpl db = new DBResult(r);
			helper.addTable(db, "DPFMNM1");
			cmrecnum += r.size();			
			//start Update 20180910	當案件已核准，需打eloan服務詢問該客戶account是否有值		
//			HttpServletRequest request= WebServletUtils.getRequest();
//			request.setAttribute("txnloanlist", r);
			sun.misc.BASE64Encoder encoder= new sun.misc.BASE64Encoder();//base64加密			
			String act =encoder.encode("query".getBytes());//分為query:查詢 unlock:離開頁面時通知eloan解lock check:完成對保
			String key = "";//LOAN2018032801(測試用的案件編號)
			SecureRandom secrandom = new SecureRandom(); //取六位亂數
			String randomval="";
			String localIP = IPUtils.getLocalIp();//取得IP位址
			getRcvno =txnLoanApplyDao.getRcvno(uid);//取得客戶已核准申請案件 
			log.trace(ESAPIUtil.vaildLog("NA011 txnLoanApplyDao.getRcvno >> {}"+ getRcvno));
//			String [] accArr = new String[getRcvno.size()];
			String defaultBaseUrl ="";//連結服務url
			String rcvno ="";
			String docflow ="";
			String account ="";
			String othkey ="";
			String appldate ="";
			String project="";
			String appramt="";
			String limitm="";
			String costnote="";
			String ratenote="";
			String rtnway ="";			
			for(int i =0; i< getRcvno.size();i++ ) {				
				rcvno = getRcvno.get(i).getRCVNO();
				log.info(ESAPIUtil.vaildLog("NA011.java rcvno---->"+rcvno));
				key = encoder.encode(rcvno.getBytes());//由DB取出案件編號
				for(int j=0;j<6;j++)  		   
				{  		    		   
					randomval +=secrandom.nextInt(10);
				}
				log.info("NA011.java randomval---->"+randomval);
				
				// 新世代網路銀行修改，為了讓新個網可以連到 eloan 服務，將 localIP 改為讀環境變數 env
				localIP = String.valueOf(params.get("TBBeLoanIp"));
				
				if(localIP.equals("10.16.22.17")){//測試環境
					defaultBaseUrl = "http://10.16.22.155/TBBeLoan/scrt/outter/Sc2EBankWeb.jsp?act="+act+"&key="+key+"&ss="+randomval;
				}else{							  //營運環境	
					//defaultBaseUrl = "https://10.16.21.68:11122/ZoneParkFees/ParkfeesServlet";
				}
				log.info("NA011.java act before:"+act);
				log.info(ESAPIUtil.vaildLog("NA011.java key before:"+key));			
				log.info(ESAPIUtil.vaildLog("NA011.java defaultBaseUrl:"+defaultBaseUrl));
				URL obj = new URL(defaultBaseUrl);
				HttpURLConnection con = (HttpURLConnection) obj.openConnection();
				con.setRequestMethod("POST");
				con.setRequestProperty("Content-Type", "application/xml");
				con.setDoOutput(true);
				DataOutputStream wr = new DataOutputStream(con.getOutputStream());        
				wr.flush();
				wr.close();
				
				int responseCode = con.getResponseCode();
				log.info("NA011.java responseCode:" + responseCode);
				
				StringBuffer response = new StringBuffer();
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(),"UTF-8"));
				String inputLine;
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				
				log.info(ESAPIUtil.vaildLog("NA011.java response Json:" + response.toString()));	        
				String json = response.toString();
				log.info(ESAPIUtil.vaildLog("NA011.java json:" + json));
				JSONArray jsonArray = JSONArray.fromObject(json);
				docflow  = jsonArray.getString(5);//案件狀態
				account  = jsonArray.getString(61);//撥款帳號
				othkey   = jsonArray.getString(1);//案件編號				
				appldate = jsonArray.getString(63);
				project  = jsonArray.getString(65);
				appramt  = jsonArray.getString(67);
				limitm   = jsonArray.getString(69);
				costnote = jsonArray.getString(71);
				ratenote = jsonArray.getString(73);
				rtnway   = jsonArray.getString(75);
				log.info(ESAPIUtil.vaildLog("NA011.java docflow===>"+docflow));
				log.info(ESAPIUtil.vaildLog("NA011.java account===>"+account));
				log.info(ESAPIUtil.vaildLog("NA011.java appldate===>"+appldate));
				log.info(ESAPIUtil.vaildLog("NA011.java project===>"+project));
				log.info(ESAPIUtil.vaildLog("NA011.java appramt===>"+appramt));
				log.info(ESAPIUtil.vaildLog("NA011.java limitm===>"+limitm));
				log.info(ESAPIUtil.vaildLog("NA011.java costnote===>"+costnote));
				log.info(ESAPIUtil.vaildLog("NA011.java ratenote===>"+ratenote));
				log.info(ESAPIUtil.vaildLog("NA011.java rtnway===>"+rtnway));
		        //String docflow = (String)jsonArray.getString(5);//案件狀態
				randomval ="";//reset空白
			}			
			//案件狀態 
	        // 0:尚無資料 1:經辦編制中   2:待主管放行 3:待對保(這個才做事) -1:不知道怎麼錯的 -7:尚未核定  
			//-6:非核准  -8:無法鎖定文件 -9:超過30天不給對保(或核定日期有誤) 9:線上對保完成
			helper.getFlatValues().put("DOCFLOW",docflow );//DOCFLOW   案件狀態
			helper.getFlatValues().put("ACCOUNT",account);//空值:為未撥貸 有值:已撥貸
			helper.getFlatValues().put("OTHKEY",othkey);//案件編號(eloan給，之後頁面帶的以此為主)
			helper.getFlatValues().put("APPLDATE",appldate);//核准日期(民國年)
			helper.getFlatValues().put("PROJECT",project);//授信項目
			helper.getFlatValues().put("APPRAMT",appramt);//授信金額
			helper.getFlatValues().put("LIMITM",limitm);//授信期限
			helper.getFlatValues().put("COSTNOTE",costnote);//利率
			helper.getFlatValues().put("RATENOTE",ratenote);//費率
			helper.getFlatValues().put("RTNWAY",rtnway);//撥貸及償還辦法
			
			// 新世代網路銀行修改，為了讓呼叫的系統能判別回應結果是否正確，加入 MSGCOD
			helper.getFlatValues().put("MSGCOD", "0000");
			
	        //end
			log.info("===NA011.java end===");
		}
		catch (TopMessageException e) {
			log.info("TopMessageException error:"+e.getMsgcode());
			helper.getFlatValues().put("ERRCODE", e.getMsgcode());			
		}catch(Exception e){
			log.info("ERROR:"+e.getMessage());
			helper.getFlatValues().put("ERRCODE", "ZX99");
		}
		finally {}		
		helper.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		helper.getFlatValues().put("CMRECNUM", cmrecnum + "");
		
		return helper;

	}

}
