package fstop.services.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;

import fstop.exception.TopMessageException;
import fstop.model.MVH;
import fstop.model.MVHImpl;
import fstop.model.TelcommResult;
import fstop.services.CommonService;
import fstop.telcomm.TelCommExec;
import fstop.util.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class N016 extends CommonService {
//	private Logger logger = Logger.getLogger(getClass());
	
	
	@Autowired
	@Qualifier("n016Telcomm")
	private TelCommExec n016Telcomm;
	
	@Autowired
	@Qualifier("n554Telcomm")
	private TelCommExec n554Telcomm;
	
//	@Required	public void setN016Telcomm(TelCommExec telcomm) {
//		n016Telcomm = telcomm;
//	}
//
//	@Required	public void setN554Telcomm(TelCommExec telcomm) {
//		n554Telcomm = telcomm;
//	}


	/**
	 * 
	 * @param params
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public MVH doAction(Map params) {
		Date d = new Date();
		String df = DateTimeUtils.getCDateShort(d);
		//String tf = DateTimeUtils.format("HHmmss", d);
		params.put("QDATE", df);
		//params.put("TIME", tf);
		
		String period = (String)params.get("PERIOD");
		String enddate = "";
		String stadate = "";
		String periodStr = "";
		if("01".equals(period)) {
			Calendar cal = Calendar.getInstance();
			 
			Date dend = cal.getTime();
			 
			cal.add(Calendar.DATE, -30);
			Date dstart = cal.getTime();
			stadate = DateTimeUtils.getCDateShort(dstart);
			enddate = DateTimeUtils.getCDateShort(dend);
			periodStr = DateTimeUtils.format("yyyy/MM/dd", dstart) + "~" + DateTimeUtils.format("yyyy/MM/dd", dend);
		} else if("02".equals(period)) {
			Calendar cal = Calendar.getInstance();
			Date dend = cal.getTime();
			cal.add(Calendar.MONTH, -2);
			Date dstart = cal.getTime();
			stadate = DateTimeUtils.getCDateShort(dstart);
			enddate = DateTimeUtils.getCDateShort(dend);			
			periodStr = DateTimeUtils.format("yyyy/MM/dd", dstart) + "~" + DateTimeUtils.format("yyyy/MM/dd", dend);
		} else if("03".equals(period)) {
			Calendar cal = Calendar.getInstance();
			Date dend = cal.getTime();
			cal.add(Calendar.MONTH, -3);
			Date dstart = cal.getTime();
			stadate = DateTimeUtils.getCDateShort(dstart);
			enddate = DateTimeUtils.getCDateShort(dend);			
			periodStr = DateTimeUtils.format("yyyy/MM/dd", dstart) + "~" + DateTimeUtils.format("yyyy/MM/dd", dend);
		} else if("04".equals(period)) {
			Calendar cal = Calendar.getInstance();
			Date dend = cal.getTime();
			cal.add(Calendar.MONTH, -4);
			Date dstart = cal.getTime();
			stadate = DateTimeUtils.getCDateShort(dstart);
			enddate = DateTimeUtils.getCDateShort(dend);			
			periodStr = DateTimeUtils.format("yyyy/MM/dd", dstart) + "~" + DateTimeUtils.format("yyyy/MM/dd", dend);
		} else if("05".equals(period)) {
			Calendar cal = Calendar.getInstance();
			Date dend = cal.getTime();
			cal.add(Calendar.MONTH, -5);
			Date dstart = cal.getTime();
			stadate = DateTimeUtils.getCDateShort(dstart);
			enddate = DateTimeUtils.getCDateShort(dend);			
			periodStr = DateTimeUtils.format("yyyy/MM/dd", dstart) + "~" + DateTimeUtils.format("yyyy/MM/dd", dend);
		} else if("06".equals(period)) {
			Calendar cal = Calendar.getInstance();
			Date dend = cal.getTime();
			cal.add(Calendar.MONTH, -6);
			Date dstart = cal.getTime();
			stadate = DateTimeUtils.getCDateShort(dstart);
			enddate = DateTimeUtils.getCDateShort(dend);			
			periodStr = DateTimeUtils.format("yyyy/MM/dd", dstart) + "~" + DateTimeUtils.format("yyyy/MM/dd", dend);
		} else if("07".equals(period)) {
			Calendar cal = Calendar.getInstance();
			Date dend = cal.getTime();
			cal.add(Calendar.MONTH, -7);
			Date dstart = cal.getTime();
			stadate = DateTimeUtils.getCDateShort(dstart);
			enddate = DateTimeUtils.getCDateShort(dend);			
			periodStr = DateTimeUtils.format("yyyy/MM/dd", dstart) + "~" + DateTimeUtils.format("yyyy/MM/dd", dend);
		} else if("08".equals(period)) {
			Calendar cal = Calendar.getInstance();
			Date dend = cal.getTime();
			cal.add(Calendar.MONTH, -8);
			Date dstart = cal.getTime();
			stadate = DateTimeUtils.getCDateShort(dstart);
			enddate = DateTimeUtils.getCDateShort(dend);			
			periodStr = DateTimeUtils.format("yyyy/MM/dd", dstart) + "~" + DateTimeUtils.format("yyyy/MM/dd", dend);
		} else if("09".equals(period)) {
			Calendar cal = Calendar.getInstance();
			Date dend = cal.getTime();
			cal.add(Calendar.MONTH, -9);
			Date dstart = cal.getTime();
			stadate = DateTimeUtils.getCDateShort(dstart);
			enddate = DateTimeUtils.getCDateShort(dend);			
			periodStr = DateTimeUtils.format("yyyy/MM/dd", dstart) + "~" + DateTimeUtils.format("yyyy/MM/dd", dend);
		} else if("10".equals(period)) {
			Calendar cal = Calendar.getInstance();
			Date dend = cal.getTime();
			cal.add(Calendar.MONTH, -10);
			Date dstart = cal.getTime();
			stadate = DateTimeUtils.getCDateShort(dstart);
			enddate = DateTimeUtils.getCDateShort(dend);			
			periodStr = DateTimeUtils.format("yyyy/MM/dd", dstart) + "~" + DateTimeUtils.format("yyyy/MM/dd", dend);
		} else if("11".equals(period)) {
			Calendar cal = Calendar.getInstance();
			Date dend = cal.getTime();
			cal.add(Calendar.MONTH, -11);
			Date dstart = cal.getTime();
			stadate = DateTimeUtils.getCDateShort(dstart);
			enddate = DateTimeUtils.getCDateShort(dend);			
			periodStr = DateTimeUtils.format("yyyy/MM/dd", dstart) + "~" + DateTimeUtils.format("yyyy/MM/dd", dend);
		} else if("12".equals(period)) {
			Calendar cal = Calendar.getInstance();
			Date dend = cal.getTime();
			cal.add(Calendar.MONTH, -12);
			Date dstart = cal.getTime();
			stadate = DateTimeUtils.getCDateShort(dstart);
			enddate = DateTimeUtils.getCDateShort(dend);			
			periodStr = DateTimeUtils.format("yyyy/MM/dd", dstart) + "~" + DateTimeUtils.format("yyyy/MM/dd", dend);
		}
		params.put("STADATE", stadate);
		params.put("ENDDATE", enddate);
		

		MVHImpl result = new MVHImpl();
		int totalcount = 0;
		try {
			TelcommResult helper_n016 = n016Telcomm.query(params);
			result.addTable(helper_n016, "DPFMNM1");
			totalcount += helper_n016.getValueOccurs("ACN");
		}
		catch(TopMessageException e) {
			MVHImpl helper = new MVHImpl();
			helper.getFlatValues().put("TOPMSG", e.getMsgcode());
			helper.getFlatValues().put("ADMSGIN", e.getMsgin());
			helper.getFlatValues().put("ADMSGOUT", e.getMsgout());
			result.addTable(helper, "DPFMNM1");			
		}
		
		try {
			TelcommResult helper_n554 = n554Telcomm.query(params);
			result.addTable(helper_n554, "DPFMNM2");
			totalcount += helper_n554.getValueOccurs("LNACCNO");
		}
		catch(TopMessageException e) {
			MVHImpl helper = new MVHImpl();
			helper.getFlatValues().put("TOPMSG", e.getMsgcode());
			helper.getFlatValues().put("ADMSGIN", e.getMsgin());
			helper.getFlatValues().put("ADMSGOUT", e.getMsgout());
			result.addTable(helper, "DPFMNM2");			
		}
		
		result.getFlatValues().put("CMQTIME", DateTimeUtils.getDatetime(new Date()));
		result.getFlatValues().put("CMPERIOD", periodStr);
		result.getFlatValues().put("CMRECNUM", totalcount + "");
		
		return result;
		
	}
}
