RHEL7.1 + WAS8.5.5 + DB2-9

開發環境準備

  maven 安裝 (optional)
    3.3.9 可以順利打包
	3.5 無法順利 compile
	
  eclipse 安裝
  
  ==
  安裝 wlp
    https://developer.ibm.com/wasdev/downloads/download-latest-stable-websphere-liberty-runtime/
  測試 wlp 啟動
    .\bin\server start
  測試 wlp 停止	
    .\bin\server stop	
  檢查 wlp 啟動狀態
    wlp\usr\servers\defaultServer\logs  
	
  安裝 wlp admin console	
	.\bin\installUtility install adminCenter-1.0
  安裝 wlp javaee 功能
    .\bin\installUtility install javaee-7.0

  修改 wlp defaultServer 設定
    wlp\usr\servers\defaultServer\server.xml
	啟用 feature (javaee-7.0, adminCenter-1.0, webProfile-7.0)
	設定 default key store 密碼
	設定 管理者密碼與權限
	設定 remoteFileAccess
	
  ?設定 wlp defaultServer 額外設定	
	wlp\usr\servers\defaultServer\dropins\custom-cfg.xml
	設定 invokeFlushAfterService="false"
	
  測試 wlp 啟動
    .\bin\server start
  測試 wlp 停止	
    .\bin\server stop	
  檢查 wlp 啟動狀態
    wlp\usr\servers\defaultServer\logs  
	
  安裝 wlp eclipse plugin
    eclipse marketplace > search > "IBM Liberty Developer Tools" 
  
  ==
  
  eclipse was plugin 安裝
  eclipse maven 設定
  eclipse maven ee 設定  
  >開發版 was 安裝  
  eclipse workspace 建立
 
開發結構準備

  結構建立 
    create-ee-prj.sh
      nnb-ear, nnb (web, core, service, repository, integration, old-core, old-telcom)

  專案匯入
    import > maven > existing maven project 
    
  >maven pom 設定
    spring mvc
	hibernate
	old source  
  
  專案相依性設定
  
  ear 與 模組的 Deployment Assembly 對映
    web/
	lib/

開發程序
  nnb 為父專案，其餘子專案位於 nnb 目錄下
  nnb 父專案 pom 加入要使用的子模組
  子專案的 pom 不用加 groupId, version
  
  子專案若有相互參考，必需在 pom 的 dependencies 中加入被引用專案的設定
  共用的組件(jar) 引用設定在 nnb 父專案的 pom 
  非共用的組件引用設定在個別的專案 pom 

  為了方便在 WAS 中開發 web 專案 pom 有設定 outputDirectory, testOutputDirectory
  會出現 Broken single-root rule: Only one <wb-resource> element with a deploy path of "/" is allowed for a web project or an EAR project
  請暫時忽略
  
  當確定全部 MANIFEST.MF 解析沒有問題，設定 wlp server.xml <logging hideMessage="SRVE9967W"/> 可以關閉警告訊息
	
  模組的 spring 相關設定放置於 /META-INF/
    
  有兩個 spring 設定會 scan package，請勿重複 package，以避免錯誤
    spring-mvc.xml
	spring-repo.xml
  

  !! 電文模組
  
  
  
  note : HibernateGenericDao 由 3.x 版 轉 5.x 版 
         CriteriaImpl 已改為 hibernate 內部功能，不建議引用 
		 HibernateDaoSupport getCurrentSession() 改為 getSessionFactory().getCurrentSession()
		 HibernateGenericDao 增加 getSession()
  note : spring LocalSessionFactoryBean hibernate 4 之後不支援 configurationClass
  note : hibernate.cfg.xml http://hibernate.sourceforge.net/hibernate-configuration-3.0.dtd 應該為 http://www.hibernate.org/dtd/hibernate-configuration
  note : 模組有需要 spring 載入的 properties ，請在 spring-properties.xml 中設定
	
	
	
Maven 開發版 轉 RAD開發版
	