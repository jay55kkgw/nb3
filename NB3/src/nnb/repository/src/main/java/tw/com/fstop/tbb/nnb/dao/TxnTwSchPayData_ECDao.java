package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import fstop.orm.po.TXNTWSCHPAYDATA_EC;
import tw.com.fstop.spring.dao.BaseHibernateDao;

@Component
public class TxnTwSchPayData_ECDao extends BaseHibernateDao<TXNTWSCHPAYDATA_EC, Serializable> {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	/**
	 * 清除使用者資料
	 * @param dpuserid 使用者統編
	 */
	public void deleteByUserId(String dpuserid) {
		
		String queryString = " DELETE fstop.orm.po.TXNTWSCHPAYDATA_EC WHERE DPUSERID = :dpuserid ";
		
		try {
			Query query = getSession().createQuery(queryString);
			query.setParameter("dpuserid", dpuserid).executeUpdate();
			
		} catch (Exception e) {
			log.error("",e);
		}
	}
}