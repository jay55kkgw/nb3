
/*
 * Copyright (c) 2017, FSTOP, Inc. All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tw.com.fstop.tbb.nnb.util;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.owasp.esapi.ESAPI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


/**
 * ESAPI utility functions.
 * 
 *
 * @since 1.0.1
 */
public class ESAPIUtil{
	 private final static Logger log = LoggerFactory.getLogger(ESAPIUtil.class);
	 static Gson gs = new Gson();
	 
	 public static String toJson(Object obj) {
			return gs.toJson(obj);
		}

	    /**
	     * URL encode.
	     * 
	     * @param url - url to encode
	     * @return valid url
	     */
	    public static String encodeURL(String url){
	        try{
	            url = ESAPI.encoder().encodeForURL(url);
	        }
	        catch(Exception e){
	        	log.error("ESAPIUtil error={}",e);
	        }
	        return url;
	    }

	    /**
	     * Javascript encode.
	     * 
	     * @param js - js to encode
	     * @return valid js
	     */
	    public static String encodeJS(String js){
	        try{
	            js = ESAPI.encoder().encodeForJavaScript(js);
	        }
	        catch(Exception e){
	        	log.error("ESAPIUtil error={}",e);
	        }
	        return js;
	    }

	    /**
	     * Html encode.
	     * 
	     * @param html - html to encode
	     * @return valid html
	     */
	    public static String encodeHTML(String html){
	        try{
	            html = ESAPI.encoder().encodeForHTML(html);
	        }
	        catch(Exception e){
	        	log.error("ESAPIUtil error={}",e);
	        }
	        return html;
	    }

	    /**
	     * Html attribute encode.
	     * 
	     * @param htmlAttr - html attribute to encode
	     * @return valid html attribute
	     */
	    public static String encodeHTMLAttr(String htmlAttr){
	        try{
	            htmlAttr = ESAPI.encoder().encodeForHTMLAttribute(htmlAttr);
	        }
	        catch(Exception e){
	        	log.error("ESAPIUtil error={}",e);
	        }
	        return htmlAttr;
	    }

	    /**
	     * 使用 validation.properties 中設定的驗證規則來檢核.
	     * 
	     * @param input - input data to validate
	     * @param validateType - validation type：
	     *        SafeString, Email, IPAddress, URL, CreditCard, SSN
	     * @param allowBlank - is allow blank string
	     * @return valid input string
	     */
	    public static String validInput(String input,String validateType,boolean allowBlank){
	        try{
	        	if(input != null && !"".equals(input)){
	        		input = ESAPI.validator().getValidInput("validInput",input,validateType,input.length(),allowBlank);
	        	}
	        }
	        catch(Exception e){
	            log.error("ESAPIUtil error={}",e);
	        }
	        return input;
	    }

	    /**
		 * Log Forging 漏洞校驗
		 * @param log
		 * @return
		 */
		/*public static String vaildLog(String message)
		{
//			Gson gson = new Gson();
//	        String jsonData = gson.toJson(message).replace("\"","");
//	        String clean  = jsonData.replace( '\n' ,  '_' ).replace( '\r' , '_' )
//	          .replace( '\t' , '_' );
//	          //clean = ESAPI.encoder().encodeForHTML( clean );
//	        return clean;
			List<String> list = new ArrayList<String>();
			list.add("%0d");
			list.add("\r");
			list.add("%0a");
			list.add("\n");
			String encode = Normalizer.normalize(message, Normalizer.Form.NFKC);
			for (int i = 0; i < list.size(); i++)
			{
				encode = encode.replace(list.get(i), "");
			}
			return encode;
		}*/
		/**
		 * Log Forging 漏洞校驗2
		 * @param log
		 * @return
		 */
//		public static String vaildLog(String message) {
//			try
//			{
//			    message = message.replaceAll( "\n" ,  "_" ).replaceAll( "\r" , "_" )
//			      .replaceAll( "\t" , "_" );
////			    message = ESAPI.encoder().encodeForHTML( message );
//			}
//			catch (Exception e)
//			{
//				log.error("ESAPIUtil vaildLog={}", e);
//			}
//		    return message;
//		}
		
		public static String vaildLog(String message) {
			try
			{
				  Pattern p = Pattern.compile("\\n\\r\\t");
				  message = p.matcher(message).replaceAll("" );
			}
			catch (Exception e)
			{
				log.error("ESAPIUtil vaildLog={}", e);
			}
		    return message;
		}
		
		
//	    /**
//	     * Log Forging 弱掃處理
//	     * @param message
//	     * @return
//	     */
//	    public static String vaildLog(String message) {
//	    	String clean = null;
//			try
//			{
//		        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
//		        String jsonData = gson.toJson(message).replace("\"","");
//		        clean  = jsonData.replace( '\n' ,  '_' ).replace( '\r' , '_' )
//		          .replace( '\t' , '_' );
//			}
//			catch (Exception e)
//			{
//				log.error("ESAPIUtil vaildLog={}", e);
//			}
//	        return clean;
//	    }
	    
	    /**
	     * 檢驗檔案內容.
	     * 
	     * @param file - File to validate.
	     * @param maxLength - max file size
	     * @param allowNull - is allow null
	     * @return valid File
	     */
	    /*public static File fileValidator(File file,int maxLength,boolean allowNull){
	        FileInputStream is = null;
	        FileOutputStream os = null;
	        byte [] content = null;
	        try{
				file.setWritable(true,true);
				file.setReadable(true,true);
				file.setExecutable(true,true);
	            is = new FileInputStream(file);
	            byte[] data = new byte[is.available()];
	            int pos = 0;
	            while(true){
	                int amt = is.read(data,pos,data.length - pos);
	                if(amt <= 0){
	                    break;
	                }
	                pos += amt;
	                int avail = is.available();
	                if(avail > data.length - pos){
	                    byte[] newData = new byte[(pos + avail)];
	                    System.arraycopy(data,0,newData,0,pos);
	                    data = newData;
	                }
	            }            
	            
	            is.close();
	            
	            content = ESAPI.validator().getValidFileContent("validFileContent",data,maxLength,allowNull);
	            
	            os = new FileOutputStream(file,false); 
	            os.write(content);
	            os.flush();
	            os.close();
	            
	        }
	        catch(Exception e){
	        	log.error("ESAPIUtil error={}",e);
	        }
	        finally{
	            close(is);
	            close(os);
	        }
	        return file;
	    }*/

	    /**
	     * Close closeable object.
	     * @param closeable closeable object
	     */
	    private static void close(Closeable closeable){
	        if(closeable != null){
	            try{
	                closeable.close();
	            }
	            catch(IOException e){
	            
	            }
	        }
	    }
	    
	    /**
	     * 檢核輸入 byte array.
	     * 
	     * @param byteArray - byte array to validate
	     * @param allowBlank - is allow blank
	     * @return valid byte array
	     */
	    public static byte[] validateByteArray(byte[] byteArray,boolean allowBlank){
	        try{
	            byteArray = ESAPI.encoder().decodeFromBase64(ESAPI.encoder().encodeForBase64(byteArray,allowBlank));
	        }
	        catch(Exception e){
	        	log.error("ESAPIUtil error={}",e);
	        }
	        return byteArray;
	    }

		/*
		 * 驗證輸入值 input-->輸入值 validateType-->驗證的類型
		 * allowBlank-->輸入值可為空值或null
		 */
		public static boolean isValidInput(String input,String validateType,boolean allowBlank){
			boolean result = true;
			try{
				if(input != null && !"".equals(input)){
					result = ESAPI.validator().isValidInput("isValidInput",input,validateType,input.length(),allowBlank);
				}
			}
			catch(Exception e){
				log.error("ESAPIUtil error={}",e);
			}
			return result;
		}
		
		// 解決 Potential O Reflected XSS All Clients
		public static Map<String, Object> validMap(Map<String,Object> reqParam){
			Map<String,Object> reMap = null;
			try{
				reMap = new HashMap<String,Object>();
				for(Map.Entry<String,Object> entry : reqParam.entrySet()){
					String enValue = String.valueOf(entry.getValue());
					boolean validResult = ESAPIUtil.isValidInput(enValue,"GeneralString",true);
					if(validResult){
						reMap.put(entry.getKey(),entry.getValue());
					}
				}
			}
			catch(Exception e){
				log.error("ESAPIUtil error={}",e);
			}
			return reMap;
		}
		
		// 解決 Potential O Reflected XSS All Clients
		public static Map<String,String> validStrMap(Map<String,String> reqParam){
			Map<String,String> reMap = null;
			try{
				reMap = new HashMap<String,String>();
				for (Map.Entry<String,String> entry : reqParam.entrySet()){
					String enValue = String.valueOf(entry.getValue());
					boolean validResult = ESAPIUtil.isValidInput(enValue,"GeneralString",true);
					if(validResult){
						reMap.put(entry.getKey(),entry.getValue());
					}
					else{
						String val = ESAPIUtil.validInput(entry.getValue(),"GeneralString",true);
						reMap.put(entry.getKey(),val);
					}
				}
			}
			catch(Exception e){
				log.error("ESAPIUtil error={}",e);
			}
			return reMap;
		}
		
		// 解決 Stored XSS
			public static <T> List<Object> validList(List<T> results){
				List<Object> resultList = new LinkedList<Object>();
				try{
//					log.trace("ESAPI.validList.srcList: {}", results);
					// 依序取出需驗證的List內之物件
					for(Object obj : results) {
						boolean result = false;
						// 將物件toJson後做ESAPI驗證
						String json = new Gson().toJson(obj);
						String[] strArrays = StrUtils.splitStringByLength(json, 200);
						
//						log.trace("ESAPI.validList.strArrays: {}", strArrays.toString());
						
						// 對toJson後的物件做ESAPI驗證
						for(String str : strArrays) {
							str = str.replace("\n", "");
							str = str.replace("\r", "");
							str = str.replace("\r\n", "");
							str = str.replace("%", "");
							str = str.replace(".", "");
							str = str.replace("\\r\\n", "");
							str = str.replace("\\", "");
							result = ESAPIUtil.isValidInput(str, "GeneralString", true);
							// 驗證失敗跳出迴圈
							if(result==false) {
								break;
							}
						}
						// 驗證失敗做下一個
						if(result==false) {
							continue;
						}
						// 驗證通過放入resultList
						if(result) {
							resultList.add(obj);
						}
					}
//					log.trace("ESAPI.validList.resultList: {}", resultList);
				}
				catch(Exception e){
					log.error("ESAPIUtil error={}",e);
				}
				return resultList;
			}
}