package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import fstop.orm.po.ADMADS;
import fstop.orm.po.ADMANN;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.DateUtil;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Component
public class AdmAdsDao extends BaseHibernateDao<ADMADS,Serializable>{
	Logger log = LoggerFactory.getLogger(this.getClass());

	public List<ADMADS> getAll() {
		List qresult = null;
		try
		{
			String queryString = " FROM fstop.orm.po.ADMADS";
			Query query = getSession().createQuery(queryString);
			// Stored XSS
			qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
//			qresult = ESAPIUtil.validList(qresult); // 花太多時間在滾LIST迴圈先註解

		}
		catch (Exception e)
		{
			log.error("", e);
		}
		return qresult;
		
	}
	
	public List<ADMADS> getByType(String type) {
		List qresult = null;
		try
		{
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT * FROM ADMADS ");
			sb.append("WHERE TYPE = '" + type + "'");
			sb.append("AND '" + DateUtil.getCurentDateTime("yyyyMMddHHmmss") + "' BETWEEN CONCAT(STARTDATE, STARTTIME) AND CONCAT(ENDDATE, ENDTIME) ");
			sb.append("ORDER BY SORTORDER, CONCAT(STARTDATE, STARTTIME) DESC");
			String queryString = sb.toString();
			Query query = getSession().createNativeQuery(queryString, ADMADS.class);
			// Stored XSS
			qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
//			qresult = ESAPIUtil.validList(qresult); // 花太多時間在滾LIST迴圈先註解

		}
		catch (Exception e)
		{
			log.error("", e);
		}
		return qresult;
		
	}
	
	public List<String> getTitleByType(String type) {
		List qresult = null;
		try
		{
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT TITLE, URL, STARTDATE, ENDDATE, TARGETTYPE, ID, CONTENT FROM ADMADS ");
			sb.append("WHERE TYPE = '" + type + "'");
			sb.append("AND '" + DateUtil.getCurentDateTime("yyyyMMddHHmmss") + "' BETWEEN CONCAT(STARTDATE, STARTTIME) AND CONCAT(ENDDATE, ENDTIME) ");
			sb.append("ORDER BY SORTORDER, CONCAT(STARTDATE, STARTTIME) DESC");
			String queryString = sb.toString();
			Query query = getSession().createNativeQuery(queryString);
			// Stored XSS
			qresult = query.getResultList();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			qresult = ESAPIUtil.validList(qresult); // 花太多時間在滾LIST迴圈先註解
			
		}
		catch (Exception e)
		{
			log.error("", e);
		}
		return qresult;
		
	}
	
	/**
	 *  取得不同廣告類型最後一筆編輯的時間
	 */
	public List<String> getLastDateTime(String type) {
		List qresult = null;
		try
		{
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT LASTDATE, LASTTIME ");
			sb.append("FROM ADMADS ");
			sb.append("WHERE TYPE = '" + type + "' ");
			sb.append("ORDER BY LASTDATE DESC, LASTTIME DESC ");
			sb.append("FETCH FIRST 1 ROW ONLY ");

			String queryString = sb.toString();
			Query query = getSession().createNativeQuery(queryString);
			
			// Stored XSS
			qresult = query.getResultList();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			qresult = ESAPIUtil.validList(qresult); // 花太多時間在滾LIST迴圈先註解
			
		}
		catch (Exception e)
		{
			log.error("", e);
		}
		return qresult;
		
	}
	
	/**
	 *  取得是否有今天生肖或到期的資料
	 */
	public List<String> getStartEnd(String type) {
		List qresult = null;
		try
		{
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT STARTDATE, ENDDATE ");
			sb.append("FROM ADMADS ");
			sb.append("WHERE TYPE = '" + type + "' ");
			sb.append("AND (STARTDATE = '" + DateUtil.getDate("") + "' OR ENDDATE = '" + DateUtil.getDate("") + "') ");

			String queryString = sb.toString();
			Query query = getSession().createNativeQuery(queryString);
			
			// Stored XSS
			qresult = query.getResultList();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			qresult = ESAPIUtil.validList(qresult); // 花太多時間在滾LIST迴圈先註解
			
		}
		catch (Exception e)
		{
			log.error("", e);
		}
		return qresult;
		
	}
	
	public ADMADS getById(String id){
		ADMADS result = null;
		try
		{
			String queryString = " FROM fstop.orm.po.ADMADS WHERE ID = :id ";
			Query<ADMADS> query = getSession().createQuery(queryString);
			query.setParameter("id", id);
			result = query.uniqueResult();
			
		} catch (Exception e) {
			log.error("getbank error >> {}",e);
		}
		return result;
	}
	
	public ADMADS getOrder(String num){
		ADMADS result = null;
		List qresult = new ArrayList(); 
		log.debug(ESAPIUtil.vaildLog("ADS>>>"+num));
		try
		{
			String queryString = " FROM fstop.orm.po.ADMADS WHERE TYPE = 'B' AND '" + DateUtil.getCurentDateTime("yyyyMMddHHmmss") + "' BETWEEN CONCAT(STARTDATE, STARTTIME) AND CONCAT(ENDDATE, ENDTIME) ORDER BY SORTORDER, CONCAT(STARTDATE, STARTTIME) DESC";
			Query<ADMADS> query = getSession().createQuery(queryString);
			query.setMaxResults(1);
			query.setFirstResult(Integer.parseInt(num)-1);
			qresult.add(query.uniqueResult());
			qresult = ESAPIUtil.validList(qresult);
			result = (ADMADS)qresult.get(0);
			
//			result = query.uniqueResult();
			
		} catch (Exception e) {
			log.error("getbank error >> {}",e);
		}
		return result;
	}
	
}
