package tw.com.fstop.tbb.nnb.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.ADMHOTOP;
import fstop.orm.po.ADMHOTOP_PK;
import fstop.orm.po.ADMKEYVALUE;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.DateUtil;

@Transactional
@Component
public class AdmHotopDao extends BaseHibernateDao<ADMHOTOP, String> {

	Logger log = LoggerFactory.getLogger(this.getClass());
	
	public List<ADMHOTOP> getByADUSERID(String userid) {
		List result = null;
		try {
			String queryString = " FROM ADMHOTOP WHERE ADUSERID= :userid";
			Query<ADMKEYVALUE> query = getSession().createQuery(queryString);
			query.setParameter("userid", userid);
			result = query.list();
		}
		catch(Exception e) {
//			e.printStackTrace();
			log.error("AdmKeyValueDao error >> {}",e);
		}
		return result;
	}
	

	public List<ADMHOTOP> getByID(String userid, String adopid) {
		List result = null;
		try {
			String queryString = " FROM ADMHOTOP WHERE ADUSERID= :userid AND ADOPID= :adopid";
			Query<ADMKEYVALUE> query = getSession().createQuery(queryString);
			query.setParameter("userid", userid);
			query.setParameter("adopid", adopid);
			result = query.list();
		}
		catch(Exception e) {
//			e.printStackTrace();
			log.error("AdmKeyValueDao error >> {}",e);
		}
		return result;
	}
	
	public List<Map> findLast5LimitData(String CUSIDN){
		List result=null;
		try {
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT TX.ADOPID, TX.ADUSERID, Max(TX.LASTDT) as LASTDT, Max(SYS.ADOPNAME) as ADOPNAME, Max(SYS.ADOPENGNAME) as ADOPENGNAME, Max(SYS.ADOPCHSNAME) as ADOPCHSNAME, Max(SYS.URL) as URL ");
			sb.append("from ADMHOTOP as TX ,NB3SYSOP AS SYS WHERE TX.ADOPID=SYS.ADOPID AND TX.ADUSERID='" + CUSIDN + "' ");
			sb.append("GROUP BY TX.ADOPID, TX.ADUSERID ORDER BY LASTDT LIMIT 5");
			String queryString = sb.toString();
			NativeQuery query = getSession().createNativeQuery(queryString);
			List<Object[]> resulttmp = query.list();
			result = new ArrayList<Map<String,String>>();
			for(Object[] row : resulttmp) {
				Map<String,String> rowObj = new HashMap<String,String>();
				rowObj.put("ADOPID", (String)row[0]);
				rowObj.put("ADUSERID", (String)row[1]);
				rowObj.put("LASTDT", (String)row[2]);
				rowObj.put("ADOPNAME", (String)row[3]);
				rowObj.put("ADOPENGNAME", (String)row[4]);
				rowObj.put("ADOPCHSNAME", (String)row[5]);
				rowObj.put("URL", (String)row[6]);
				result.add(rowObj);
			}
		}catch(Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("cancelAcn error >> {}", e);
		}
		
		return result;
	}
	
	public void saveADMHOTOP(String userid, String adopid) {

		try {
			ADMHOTOP_PK pk = new ADMHOTOP_PK();
			pk.setADOPID(adopid);
			pk.setADUSERID(userid);
			ADMHOTOP data = new ADMHOTOP();
			List<ADMHOTOP> querylist = getByID(userid,adopid);
			if(querylist != null && querylist.size() > 0) {
				data = querylist.get(0);
			}else {
				data.setId(pk);
			}
			data.setLASTDT(DateUtil.getDate("") + DateUtil.getTheTime(""));
			super.save(data);
		}
		catch(Exception e) {
//			e.printStackTrace();
			log.error("AdmKeyValueDao error >> {}",e);
		}
		
	}
	public void deleteOutOfLimitFive(String userid) {

		StringBuffer sb = new StringBuffer();
		sb.append("DELETE ADMHOTOP WHERE ADUSERID='" + userid +"' AND ADOPID NOT IN ");
		sb.append("(SELECT ADOPID FROM ADMHOTOP WHERE ADUSERID='" + userid + "' ORDER BY LASTDT DESC LIMIT 5)");
		try {
			String queryString = sb.toString();
			NativeQuery query = getSession().createNativeQuery(queryString);
			query.executeUpdate();
		} catch (Exception e) {
			log.error("", e);
		}
	}
		
}
