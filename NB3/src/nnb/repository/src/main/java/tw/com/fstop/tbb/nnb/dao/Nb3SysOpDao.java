package tw.com.fstop.tbb.nnb.dao;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.NB3SYSOP;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Transactional
@Component
public class Nb3SysOpDao extends BaseHibernateDao<NB3SYSOP, Serializable> {
	private Logger log = LoggerFactory.getLogger(this.getClass());

	public String findAdopid(String ADOPID) {
		NB3SYSOP po = null;
		String result = null;
		try{
			String sql = "FROM fstop.orm.po.NB3SYSOP WHERE ADOPID = :adopid";
			Query<NB3SYSOP> query = getSession().createQuery(sql);
			query.setParameter("adopid",ADOPID);
			po = query.uniqueResult();
			result = po.getADOPNAME();
		}catch (Exception e){
			log.error(ESAPIUtil.vaildLog("找不到交易類型的中文名稱 >> " + ADOPID));
			result = "";
		}
		
		return result;
	}
	public String findAdopidTW(String ADOPID) {
		NB3SYSOP po = null;
		String result = null;
		try{
			String sql = "FROM fstop.orm.po.NB3SYSOP WHERE ADOPID = :adopid";
			Query<NB3SYSOP> query = getSession().createQuery(sql);
			query.setParameter("adopid",ADOPID);
			po = query.uniqueResult();
			result = po.getADOPNAME();
		}catch (Exception e){
			log.error(ESAPIUtil.vaildLog("找不到交易類型的中文名稱 >> " + ADOPID));
			result = "";
		}
		
		return result;
	}
	
	public String findAdopidCN(String ADOPID) {
		NB3SYSOP po = null;
		String result = null;
		try{
			String sql = "FROM fstop.orm.po.NB3SYSOP WHERE ADOPID = :adopid";
			Query<NB3SYSOP> query = getSession().createQuery(sql);
			query.setParameter("adopid",ADOPID);
			po = query.uniqueResult();
			result = po.getADOPCHSNAME();
		}catch (Exception e){
			log.error(ESAPIUtil.vaildLog("找不到交易類型的中文名稱 >> " + ADOPID));
			result = "";
		}
		
		return result;
	}
	
	public String findAdopidEN(String ADOPID) {
		NB3SYSOP po = null;
		String result = null;
		try{
			String sql = "FROM fstop.orm.po.NB3SYSOP WHERE ADOPID = :adopid";
			Query<NB3SYSOP> query = getSession().createQuery(sql);
			query.setParameter("adopid",ADOPID);
			po = query.uniqueResult();
			result = po.getADOPENGNAME();
		}catch (Exception e){
			log.error(ESAPIUtil.vaildLog("找不到交易類型的中文名稱 >> " + ADOPID));
			result = "";
		}
		
		return result;
	}

//	@SuppressWarnings( { "unused", "unchecked" })
//	public List<SYSOP> getOfflineList() {
//		
//		//String ResultStr = "";
//		String sql = "from SYSOP where ADOPALIVE = 'N' ";
//		
//		Query query = getQuery(sql);
//		
//		List<SYSOP> ResultList = query.list();
//		
//		/*
//		if(ResultList.size() > 0)
//		{
//			for(int i = 0; i < ResultList.size(); i++)
//			{
//				if(ResultStr.length() > 0)ResultStr += ",";
//				ResultStr += ResultList.get(i).getADOPID();
//			}
//		}
//		*/
//		
//		return ResultList;
//	}
//
//	@SuppressWarnings( { "unused", "unchecked" })
//	public String updateStatus(String Items, String StatusCode) {
//		String UpdateResult = "";
//		
//		try
//		{
//			SQLQuery updateQueryStr=this.getSession().createSQLQuery("UPDATE SYSOP SET ADOPALIVE = '" + StatusCode + "' WHERE ADOPID IN (" + Items + ")");
//			System.out.println("QueryStr =================================> " + updateQueryStr.toString());
//			//updateQueryStr.setString(0, StatusCode);
//			//updateQueryStr.setString(1, Items);
//			
//			int updateRecords = updateQueryStr.executeUpdate();
//			
//			UpdateResult = "Success," + StatusCode + "," + updateRecords;
//		}
//		catch(Exception UpdateSysOpEx)
//		{
//			UpdateResult = UpdateSysOpEx.getMessage();
//		}
//		
//		return UpdateResult;
//	}
	
	
	public List<String> getUrlList() {
		List<String> qresult = null;
		try {
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT URL FROM NB3SYSOP ");
			sb.append("WHERE URL IS NOT NULL AND URL <> '' AND URL <> '/' ");
			String queryString = sb.toString();
			Query query = getSession().createNativeQuery(queryString);
			// Stored XSS
			qresult = query.getResultList();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}

		} catch (Exception e) {
			log.error("", e);
		}
		return qresult;
	}
	
	public List getAdopidList() {
		List qresult = null;
		try {
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT ADOPID, URL, ADOPAUTHTYPE, ADOPALIVE, ISANONYMOUS FROM NB3SYSOP ");
			sb.append("WHERE URL IS NOT NULL AND URL <> '' AND URL <> '/' ");
			String queryString = sb.toString();
			Query query = getSession().createNativeQuery(queryString);
			// Stored XSS
			qresult = query.getResultList();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			qresult = ESAPIUtil.validList(qresult); // 花太多時間在滾LIST迴圈先註解 
		} catch (Exception e) {
			log.error("", e);
		}
		return qresult;
	}
	
	public List<Map> findMyOpData(String cusidn){
		List result=null;
		try {
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT ADOPID, ADOPNAME, ADOPENGNAME, ADOPCHSNAME, URL ");
			sb.append("FROM NB3SYSOP ");
			sb.append("WHERE ADOPID IN ( ");
			sb.append(" SELECT ADOPID FROM ADMMYOP ");
			sb.append(" WHERE ADUSERID = '" + cusidn + "' ");
			sb.append(" ORDER BY LASTDT ");
			sb.append(" FETCH FIRST 10 ROWS ONLY ");
			sb.append(")");
			String queryString = sb.toString();
			NativeQuery query = getSession().createNativeQuery(queryString);
			List<Object[]> resulttmp = query.list();
			result = new ArrayList<Map<String,String>>();
			for(Object[] row : resulttmp) {
				Map<String,String> rowObj = new HashMap<String,String>();
				rowObj.put("ADOPID", (String)row[0]);
				rowObj.put("ADOPNAME", (String)row[1]);
				rowObj.put("ADOPENGNAME", (String)row[2]);
				rowObj.put("ADOPCHSNAME", (String)row[3]);
				rowObj.put("URL", (String)row[4]);
				result.add(rowObj);
			}
			
		}catch(Exception e) {
			log.error("{}", e);
		}
		
		return result;
	}
	
} 
