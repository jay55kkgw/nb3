package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.SYSPARAM;
import fstop.orm.po.TXNFXSCHPAYDATA;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Transactional
@Component
public class SysParamDao extends BaseHibernateDao<SYSPARAM, Serializable> {
	Logger log = LoggerFactory.getLogger(this.getClass());

	/**
	 * 以key查詢sysparam vlaue
	 */
	@Cacheable(value="SysParamCache",key="'paramName'",unless="#paramName == 'TxnCsssLogMailStartDate' || #paramName == 'TxnCsssLogMailEndDate' || #paramName == 'isOpenCsssLog'")
	public String getSysParam(String paramName) {
		String result = "";

		log.trace(ESAPIUtil.vaildLog("fgtxstatus>>>{}" + paramName));
		String queryString = " FROM fstop.orm.po.SYSPARAM WHERE ADPARAMNAME = :adparamname";
		try {
			Query<SYSPARAM> query = getSession().createQuery(queryString);
			query.setParameter("adparamname", paramName);
			if (query.list().size() > 0) {
				result = query.list().get(0).getADPARAMVALUE();
			}
			log.debug("TXNFXSCHPAYDATADao result size >> {}", query.list().size());
		} catch (Exception e) {
			log.error("getSysParam error >> {}", e);
		}
		return result;
	}
}