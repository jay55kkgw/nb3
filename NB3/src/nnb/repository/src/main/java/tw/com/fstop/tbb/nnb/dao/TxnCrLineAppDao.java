package tw.com.fstop.tbb.nnb.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import fstop.orm.po.TXNCARDAPPLY;
import fstop.orm.po.TXNCRLINEAPP;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Component
public class TxnCrLineAppDao extends BaseHibernateDao<TXNCRLINEAPP, String> {
	private Logger log = LoggerFactory.getLogger(this.getClass());

	
	/**
	 * 查詢客戶是否申請過
	 * @return
	 */
	public List<TXNCRLINEAPP> findByAll(String cusidn, String cardnum)
	{
		List<TXNCRLINEAPP> result = new ArrayList<>();
		String sql = " FROM fstop.orm.po.TXNCRLINEAPP WHERE CPRIMID = :CPRIMID and CARDNUM = :CARDNUM";
		Query<TXNCRLINEAPP> query = getSession().createQuery(sql);
		query.setParameter("CPRIMID", cusidn);
		query.setParameter("CARDNUM", cardnum);
		result = query.list();
		return result;
	}
	
}