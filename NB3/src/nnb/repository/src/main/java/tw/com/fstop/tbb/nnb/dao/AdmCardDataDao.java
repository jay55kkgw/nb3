package tw.com.fstop.tbb.nnb.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import fstop.orm.po.ADMCARDDATA;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;


@Component
public class AdmCardDataDao extends BaseHibernateDao<ADMCARDDATA, String> {
	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	@SuppressWarnings("unchecked")
	public List<ADMCARDDATA> getNormalCard(){
		String sql = "FROM fstop.orm.po.ADMCARDDATA WHERE CARDATTR='C' ORDER BY CARDNO DESC";
		Query<ADMCARDDATA> query = getSession().createQuery(sql);
		
		List<ADMCARDDATA> list = query.list();
		log.debug(ESAPIUtil.vaildLog("list="+list));
		
		return list;
	}
	/**
	 * 查詢銀色之愛信用卡資料
	 */
	@SuppressWarnings("unchecked")
	public List<ADMCARDDATA> getNormalSilverCard(String cardno1,String cardno2){
		String sql = "FROM fstop.orm.po.ADMCARDDATA WHERE CARDNO IN(:cardno1,:cardno2)";
		Query<ADMCARDDATA> query = getSession().createQuery(sql);
		query.setParameter("cardno1",cardno1);
		query.setParameter("cardno2",cardno2);
		
		List<ADMCARDDATA> list = query.list();
		log.debug(ESAPIUtil.vaildLog("list="+list));
		
		return list;
	}
	
	/**
	 * 查詢CARDNAME By CARDNO
	 * @return
	 */
	public String findCARDNAME(String cardno) 
	{
		String sql = "FROM fstop.orm.po.ADMCARDDATA WHERE CARDNO = :CARDNO";
		Query<ADMCARDDATA> query = getSession().createQuery(sql);
		query.setParameter("CARDNO", cardno);
		//修改 Potential Stored XSS
		List<ADMCARDDATA> list = query.list();
		if (null == list)
		{
			list = new ArrayList();
		}
		list = ESAPIUtil.validList((List)list);

		String result = "";
		if(list.size() > 0)
		{
			ADMCARDDATA po = list.get(0);
			result = po.getCARDNAME();
		}
		return result;
	}	
	
	
	/**
	 * 查詢CARDMEMO By CARDNO
	 * @return
	 */
	public String findCARDMEMO(String cardno) 
	{
		String sql = "FROM fstop.orm.po.ADMCARDDATA WHERE CARDNO = :CARDNO";
		Query<ADMCARDDATA> query = getSession().createQuery(sql);
		query.setParameter("CARDNO", cardno);
		//修改 Potential Stored XSS
		List<ADMCARDDATA> list = query.list();
		if (null == list)
		{
			list = new ArrayList();
		}
		list = ESAPIUtil.validList((List)list);
		String result = "";
		if(list.size() > 0)
		{
			ADMCARDDATA po = list.get(0);
			result = po.getCARDMEMO();
		}
		return result;
	}
	
	
//	public List<ADMCARDDATA> getCardDATA(String cardno) {
//		return find("FROM ADMCARDDATA WHERE CARDNO = ? ORDER BY CARDNO",cardno);
//	}
	/*
	 * 取得申請過的卡片之外的其他卡片
	 * 20180919
	 * DB增加群組欄位，以達成選卡時按照群組排序
	 * 20190327
	 * */
	@SuppressWarnings("unchecked")
	public List<Map<String,String>> getOtherCardGroupDATA(Set<String> set_CardType) {
		
		List<Map<String,String>> result = new ArrayList<Map<String,String>>();
		String query="";
		List<ADMCARDDATA> all = null;
		
		String gpoder = "";
		String isNotIn = "";
		Iterator<String> iterator = set_CardType.iterator();
		
		String CardType="";
		while (iterator.hasNext()) {
			CardType = iterator.next();		    
		    logger.info(" iterator CardType:"+CardType);
			if(CardType.equals("916") || CardType.equals("917") || CardType.equals("919") || CardType.equals("920")) {//銀色之愛卡
				gpoder +="'01',";
				isNotIn=" NOT ";
			}
			if(CardType.equals("902")) {//Master鈦金商旅卡
				gpoder +="'02',";
				isNotIn=" NOT ";
			}
//			if(CardType.equals("906") || CardType.equals("801") || CardType.equals("304") || CardType.equals("204")) {//VISA故宮卡
			if(CardType.equals("580")) {//藝FUN悠遊御璽卡
				gpoder +="'03',";
				isNotIn=" NOT ";
			}
			if(CardType.equals("914") || CardType.equals("915") || CardType.equals("413") || CardType.equals("415")) {//北港朝天宮認同卡
				gpoder +="'04',";
				isNotIn=" NOT ";
			}
			if(CardType.equals("911") || CardType.equals("411")) {//一卡通
				gpoder +="'05',";
				isNotIn=" NOT ";
			}
			if(CardType.equals("903") || CardType.equals("407")) {//悠遊卡
				gpoder +="'06',";
				isNotIn=" NOT ";
			}
			if(CardType.equals("905") || CardType.equals("409")) {//宜蘭悠遊卡
				gpoder +="'07',";
				isNotIn=" NOT ";
			}
			if(CardType.equals("200") || CardType.equals("300") || CardType.equals("800")) {//VISA銀行卡
				gpoder +="'08',";
				isNotIn=" NOT ";
			}		
			if(CardType.equals("500") || CardType.equals("400")) {//MASTERCARD銀行卡
				gpoder +="'09',";
				isNotIn=" NOT ";
			}
			if(CardType.equals("918")) {//ADD 20190401 發行保基金111關懷認同卡 
				gpoder +="'10',";
				isNotIn=" NOT ";
			}
			if(CardType.equals("923") || CardType.equals("924")) {//ADD 20200720 獅友會公益卡
				gpoder +="'11',";
				isNotIn=" NOT ";
			}
			if(CardType.equals("0")){//全撈所有信用卡
				gpoder +="'01','02','03','04','05','06','07','08','09','10','11',";
				isNotIn="";
			}
		}
		gpoder = gpoder.substring(0, gpoder.length()-1);//減1是因為最後是一個逗號
		query = " FROM fstop.orm.po.ADMCARDDATA WHERE CARDATTR='C' AND CARDTYPE NOT IN('408','410','901','508','904','910','902','916','917') " +
				" AND GPODER "+isNotIn+" IN("+gpoder+") "+
				" ORDER BY GPODER , CARDNO DESC";
		
		log.trace("query>>>{}",query);
		all = getSession().createQuery(query).getResultList();
		
		for(int j=0;j<all.size();j++) {
			Map<String,String> rm = new HashMap<String,String>();
			rm.put("CARDNO", all.get(j).getCARDNO());							
			rm.put("CARDNAME", all.get(j).getCARDNAME());			
			rm.put("GPODER", all.get(j).getGPODER());			
			result.add(rm);
		}
		if(isNotIn.replace(" ", "").equals("NOT")) {
			Map<String,String> card = new HashMap<String,String>();
			String Ncard = gpoder.replace("'", "");
			log.trace("Ncard>>>>>>"+Ncard);
			card.put("Ncard", Ncard);
			result.add(card);
		}

		return result;
	}
	
	/**
	 * 查詢CARDNO By CARDTYPE
	 * @return
	 */
	public String findCARDNO(String cardtype) 
	{
		String sql = "FROM fstop.orm.po.ADMCARDDATA WHERE CARDTYPE = :CARDTYPE";
		Query<ADMCARDDATA> query = getSession().createQuery(sql);
		query.setParameter("CARDTYPE", cardtype);
		//修改 Potential Stored XSS
		List<ADMCARDDATA> list = query.list();
		if (null == list)
		{
			list = new ArrayList();
		}
		list = ESAPIUtil.validList((List)list);
		String result = "";
		if(list.size() > 0)
		{
			ADMCARDDATA po = list.get(0);
			result = po.getCARDNO();
		}
		return result;
	}
}