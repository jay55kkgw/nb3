package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import fstop.orm.po.TXNTRACCSET;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.DateUtil;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Transactional
@Component
public class TxnTrAccSetDao extends BaseHibernateDao<TXNTRACCSET, Serializable> {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	/**
	 * 清除使用者資料
	 * @param dpuserid 使用者統編
	 */
	public void deleteByUserId(String dpuserid) {
		
		String queryString = " DELETE fstop.orm.po.TXNTRACCSET WHERE DPUSERID = :dpuserid ";
		
		try {
			Query query = getSession().createQuery(queryString);
			query.setParameter("dpuserid", dpuserid).executeUpdate();
			
		} catch (Exception e) {
			log.error("",e);
		}
	}
	
	public List<TXNTRACCSET> getByUid(String cusidn) {
		List result = null;
//		注意:舊語法已不能使用  要使用fstop.orm.po.xxx  PO 要加@Entity
		String queryString = " FROM fstop.orm.po.TXNTRACCSET WHERE DPUSERID = :cusidn ORDER BY DPTRIBANK, DPTRACNO";
//		String queryString = " FROM TXNADDRESSBOOK WHERE DPUSERID = :dpuserid ";
		try {
			log.trace(ESAPIUtil.vaildLog("cusidn >> " + cusidn));
			Query<TXNTRACCSET> query = getSession().createQuery(queryString);
			query.setParameter("cusidn", cusidn);
			result = query.list();
			if(result==null) {
				result = new ArrayList();
			}
			result = ESAPIUtil.validList(result);
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("TxnTrAccSetDao error >> {}",e);
		}
		
		return result;
		
	}
	
	/**
	 * 	取得銀行代號and好記名稱
	 * 
	 * @param cusidn 使用者帳號
	 * @param ACN 轉入帳號
	 * @return
	 */
	public TXNTRACCSET findByACN(String CUSIDN,String ACN) {
		TXNTRACCSET result = null;
		String queryString = "from fstop.orm.po.TXNTRACCSET where DPUSERID = :CUSIDN AND DPTRDACNO = :ACN";
		try {
			Query<TXNTRACCSET> query = getSession().createQuery(queryString);
			query.setParameter("CUSIDN", CUSIDN);
			query.setParameter("ACN", ACN);			
			
			result = query.list().get(0);
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("findByACN error >> {}",e);
			result = null;
		}
		
		return result;
		
	}
	
	public boolean findDptrdacnoIsExists(String idn,String DPTRIBANK, String DPTRDACNO) {
		Query query = this.getSession().createQuery("SELECT count(DPTRDACNO) FROM  fstop.orm.po.TXNTRACCSET WHERE DPUSERID=?0 and DPTRIBANK = ?1 and DPTRDACNO = ?2");
		query.setParameter(0, idn);
		query.setParameter(1, DPTRIBANK);
		query.setParameter(2, DPTRDACNO);
		
		return ((Long)query.list().iterator().next()) > 0;
	}
	public List<TXNTRACCSET> findByDPGONAME(String DPUSERID,String DPTRIBANK,String DPTRDACNO) {
		List<TXNTRACCSET> result = null;
//		注意:舊語法已不能使用  要使用fstop.orm.po.xxx  PO 要加@Entity
		String queryString = "from fstop.orm.po.TXNTRACCSET where DPUSERID = :dpuserid AND DPTRIBANK = :dptribank AND DPTRDACNO = :dptrdacno ORDER BY DPTRIBANK, DPTRACNO";
//		String queryString = " FROM TXNADDRESSBOOK WHERE DPUSERID = :dpuserid ";
		try {
			Query<TXNTRACCSET> query = getSession().createQuery(queryString);
			query.setParameter("dpuserid", DPUSERID);
			query.setParameter("dptribank", DPTRIBANK);
			query.setParameter("dptrdacno", DPTRDACNO);
			
			
			result = query.list();
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("findByDPGONAME error >> {}",e);
		}
		
		return result;
		
	}
	
	/**
	 * 
	 * @param dpuserid 統編
	 * @param dpgoname 好記名稱
	 * @param dptracno 帳號種類
	 * @param dptrdacno 帳號
	 * @param dptribank 銀行代碼
	 * @return
	 */
	public TXNTRACCSET findByInput(String dpuserid , String dpgoname ,String dptracno,String dptrdacno,String dptribank) {
		TXNTRACCSET result = null;
//		注意:舊語法已不能使用  要使用fstop.orm.po.xxx  PO 要加@Entity
		String queryString = "from fstop.orm.po.TXNTRACCSET where DPUSERID = :dpuserid AND DPTRIBANK = :dptribank AND DPTRDACNO = :dptrdacno AND DPTRACNO = :dptracno  AND DPTRIBANK = :dptribank ORDER BY DPTRIBANK, DPTRACNO";
//		String queryString = " FROM TXNADDRESSBOOK WHERE DPUSERID = :dpuserid ";
		try {
			Query<TXNTRACCSET> query = getSession().createQuery(queryString);
			query.setParameter("dpuserid", dpuserid);
			query.setParameter("dpgoname", dpgoname);
			query.setParameter("dptracno", dptracno);
			query.setParameter("dptrdacno", dptrdacno);
			query.setParameter("dptribank", dptribank);
			
			result = query.uniqueResult();
		} catch (Exception e) {
			log.error("findByInput.error >> {}",e);
		}
		
		return result;
		
	}
	/**
	 * 
	 * @param dpuserid
	 * @param dptracno
	 * @param dptrdacno 搭配 dptribank 為一個唯一值
	 * @param dptribank
	 * @return
	 */
	public TXNTRACCSET findByInput(String dpuserid ,String dptracno,String dptrdacno,String dptribank) {
		TXNTRACCSET result = null;
//		注意:舊語法已不能使用  要使用fstop.orm.po.xxx  PO 要加@Entity
		String queryString = "from fstop.orm.po.TXNTRACCSET where DPUSERID = :dpuserid AND DPTRIBANK = :dptribank AND DPTRDACNO = :dptrdacno AND DPTRACNO = :dptracno  AND DPTRIBANK = :dptribank ORDER BY DPTRIBANK, DPTRACNO";
//		String queryString = " FROM TXNADDRESSBOOK WHERE DPUSERID = :dpuserid ";
		try {
			Query<TXNTRACCSET> query = getSession().createQuery(queryString);
			query.setParameter("dpuserid", dpuserid);
			query.setParameter("dptracno", dptracno);
			query.setParameter("dptrdacno", dptrdacno);
			query.setParameter("dptribank", dptribank);
			
			result = query.uniqueResult();
		} catch (Exception e) {
			log.error("findByInput.error >> {}",e);
		}
		
		return result;
		
	}
	
	/**
	 * 
	 * @param dpuserid
	 * @param dptrdacno
	 * @param dptribank
	 * @return
	 */
	public TXNTRACCSET findByInput(String dpuserid ,String dptrdacno,String dptribank) {
		TXNTRACCSET result = null;
		String queryString = "from fstop.orm.po.TXNTRACCSET where DPUSERID = :dpuserid AND DPTRIBANK = :dptribank AND DPTRDACNO = :dptrdacno   AND DPTRIBANK = :dptribank ORDER BY DPTRIBANK, DPTRACNO";
		try {
			Query<TXNTRACCSET> query = getSession().createQuery(queryString);
			query.setParameter("dpuserid", dpuserid);
			query.setParameter("dptrdacno", dptrdacno);
			query.setParameter("dptribank", dptribank);
			
			result = query.uniqueResult();
		} catch (Exception e) {
			log.error("findByInput.error >> {}",e);
		}
		
		return result;
		
	}
	
	/**
	 * 取得好記名稱
	 * 
	 * @param cusidn 使用者帳號
	 * @param bank 分行代碼
	 * @param ACN 轉入帳號
	 * @return
	 */
	public String getDpgoName(String cusidn,String acn,String bank) 
	{
		String result = "";
		TXNTRACCSET po = null;
		try
		{
			String sql = "FROM fstop.orm.po.TXNTRACCSET where DPUSERID = :cusidn AND DPTRDACNO = :acn AND DPTRIBANK = :bank";
			Query<TXNTRACCSET> query = getSession().createQuery(sql);
			query.setParameter("cusidn", cusidn);
			query.setParameter("acn", acn);
			query.setParameter("bank", bank);
			po = query.uniqueResult();
			result = po.getDPGONAME();
			if(result == null) {
				result = "";
			}
		}
		catch (Exception e)
		{
			log.error(ESAPIUtil.vaildLog("找不到好記名稱({})."+ cusidn+"/"+acn+"/"+bank+ e));
			result = "";
		}
		return result;
	}
	public TXNTRACCSET getDpgoNameII(String cusidn,String acn,String bank) 
	{
		String result = "";
		TXNTRACCSET po = null;
		try
		{
			String sql = "FROM fstop.orm.po.TXNTRACCSET where DPUSERID =:cusidn AND DPTRDACNO =:acn AND DPTRIBANK =:bank";
			Query<TXNTRACCSET> query = getSession().createQuery(sql);
			query.setParameter("cusidn", cusidn);
			query.setParameter("acn", acn);
			query.setParameter("bank", bank);
			po = query.uniqueResult();
		}
		catch (Exception e)
		{
			log.error(ESAPIUtil.vaildLog("getDpgoNameII.ERROR({})."+ cusidn+"/"+acn+"/"+bank+ e));
			result = "";
		}
		return po;
	}
	
	/**
	 * 取得好記名稱
	 * 
	 * @param cusidn 使用者帳號
	 * @param bank 分行代碼
	 * @param ACN 轉入帳號
	 * @return
	 */
	public String updateDpgoName(String cusidn,String acn,String bnkcod,String dpgoname) 
	{
		log.debug("TXNTRACCSET_Dao >> updateDpgoName");
		String result = "";
		TXNTRACCSET po = null;
		bnkcod = bnkcod.substring(0, 3);
		log.trace(ESAPIUtil.vaildLog("bnkcod>>>{}"+ bnkcod));
		try{
			String sql = "FROM fstop.orm.po.TXNTRACCSET where DPUSERID = :cusidn AND DPTRDACNO = :acn AND DPTRIBANK = :bank";
			Query<TXNTRACCSET> query = getSession().createQuery(sql);
			query.setParameter("cusidn", cusidn);
			query.setParameter("acn", acn);
			query.setParameter("bank",bnkcod);
			po = query.uniqueResult();
			
			po.setDPGONAME(dpgoname);
			//更新最後時間
			Date d = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			SimpleDateFormat hms = new SimpleDateFormat("hhmmss");
			String today = sdf.format(d);
			String hours = hms.format(d);
			po.setLASTDATE(today);
			po.setLASTTIME(hours);
			update(po);
			result = "update";
		}
		catch (Exception e)
		{
			log.error(ESAPIUtil.vaildLog("好記名稱更新失敗({})."+ acn + e));
			result = "";
		}
		return result;
	}
	
	/**
	 * 約定並更新好記名稱
	 * 
	 * @param cusidn 使用者帳號
	 * @param bank 分行代碼
	 * @param ACN 轉入帳號
	 * @return
	 */
	public String updateDpgoname(String cusidn,String acn,String bnkcod,String dpgoname) 
	{
		log.debug("TXNTRACCSET_Dao >> updateDpgoName");
		String result = "";
		TXNTRACCSET po = null;
		bnkcod = bnkcod.substring(0, 3);
		log.trace("bnkcod>>>{}",bnkcod);
		try{
			String sql = "FROM fstop.orm.po.TXNTRACCSET where DPUSERID = :cusidn AND DPTRDACNO = :acn AND DPTRIBANK = :bank";
			Query<TXNTRACCSET> query = getSession().createQuery(sql);
			query.setParameter("cusidn", cusidn);
			query.setParameter("acn", acn);
			query.setParameter("bank",bnkcod);
			po = query.uniqueResult();			
			if(dpgoname != null && !"".equals(dpgoname)) {
				po.setDPGONAME(dpgoname);				
			}
			po.setDPTRACNO("1");
			//更新最後時間
			Date d = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			SimpleDateFormat hms = new SimpleDateFormat("hhmmss");
			String today = sdf.format(d);
			String hours = hms.format(d);
			po.setLASTDATE(today);
			po.setLASTTIME(hours);
			update(po);
			result = "update";
		}
		catch (Exception e)
		{
			log.error("好記名稱更新失敗({}).", acn, e);
			result = "";
		}
		return result;
	}

	/**
	 * 約定並更新好記名稱
	 * 
	 * @param cusidn 使用者帳號
	 * @param bank 分行代碼
	 * @param ACN 轉入帳號
	 * @return
	 */
	public String createDpgoname(String cusidn,String acn,String bnkcod,String dpgoname) 
	{
		log.debug("TXNTRACCSET_Dao >> updateDpgoName");
		String result = "";
		TXNTRACCSET po = null;
		bnkcod = bnkcod.substring(0, 3);
		log.trace("bnkcod>>>{}",bnkcod);
		try{

			TXNTRACCSET txntraccset = new TXNTRACCSET();
			txntraccset.setDPUSERID(cusidn);
			txntraccset.setDPGONAME(dpgoname);
			txntraccset.setDPTRACNO("1");
			txntraccset.setDPTRIBANK(bnkcod);
			txntraccset.setDPTRDACNO(acn);
			txntraccset.setLASTDATE(DateUtil.getDate(""));
			txntraccset.setLASTTIME(DateUtil.getTheTime(""));
			save(txntraccset);				
			result = "create";
		}
		catch (Exception e)
		{
			log.error("好記名稱更新失敗({}).", acn, e);
			result = "";
		}
		return result;
	}
	
	/**
	 * 	取消約定
	 * 
	 * @param cusidn 使用者帳號
	 * @param bank 分行代碼
	 * @param ACN 轉入帳號
	 * @return
	 */
	public String cancelAcn(String cusidn,String acn,String bnkcod) 
	{
		log.debug("TXNTRACCSET_Dao >> cancelAcn");
		String result = "";
		TXNTRACCSET po = null;
		bnkcod = bnkcod.substring(0, 3);
		log.trace(ESAPIUtil.vaildLog("bnkcod>>>{}"+ bnkcod));
		try{
			String sql = "FROM fstop.orm.po.TXNTRACCSET where DPUSERID = :cusidn AND DPTRDACNO = :acn AND DPTRIBANK = :bank";
			Query<TXNTRACCSET> query = getSession().createQuery(sql);
			query.setParameter("cusidn", cusidn);
			query.setParameter("acn", acn);
			query.setParameter("bank",bnkcod);
			po = query.uniqueResult();			

			po.setDPTRACNO("0");
			//更新最後時間
			Date d = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			SimpleDateFormat hms = new SimpleDateFormat("hhmmss");
			String today = sdf.format(d);
			String hours = hms.format(d);
			po.setLASTDATE(today);
			po.setLASTTIME(hours);
			update(po);
			result = "update";
		}
		catch (Exception e)
		{
			log.trace(ESAPIUtil.vaildLog("取消轉入帳號失敗({})."+ acn + e));
			result = "";
		}
		return result;
	}
}
