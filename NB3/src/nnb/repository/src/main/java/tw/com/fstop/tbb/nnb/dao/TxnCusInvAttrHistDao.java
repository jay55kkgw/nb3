package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.TXNCUSINVATTRHIST;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;
 
@Transactional
@Component
public class TxnCusInvAttrHistDao extends BaseHibernateDao<TXNCUSINVATTRHIST,Serializable>{
	Logger log = LoggerFactory.getLogger(this.getClass());

	/**
	 * 清除使用者資料
	 * @param dpuserid 使用者統編
	 */
	public void deleteByUserId(String fduserid) {
		
		String queryString = " DELETE fstop.orm.po.TXNCUSINVATTRHIST WHERE FDUSERID = :fduserid ";
		
		try {
			Query query = getSession().createQuery(queryString);
			query.setParameter("fduserid", fduserid).executeUpdate();
			
		} catch (Exception e) {
			log.error("",e);
		}
	}
	
	/**
	 * 查詢客戶當日KYC已填寫次數
	 */
	@SuppressWarnings("unchecked")
	public List<TXNCUSINVATTRHIST> findUserKycCount(String fundUserId,String lastDate){
		List qresult = null;
		String sql = "from fstop.orm.po.TXNCUSINVATTRHIST where FDUSERID = :fundUserId and LASTDATE = :lastDate order by LASTDATE ";
		
		try{
			Query query = getSession().createQuery(sql);
			query.setParameter("fundUserId",fundUserId);
			query.setParameter("lastDate",lastDate);
			qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			qresult = ESAPIUtil.validList(qresult);
		}
		catch(Exception e){
//			e.printStackTrace();
			log.error("findUserKycCount error >> {}",e);
		}
		return qresult;
	}
	
	public List<TXNCUSINVATTRHIST> findByDPUserID(String fundUserId) {
		List<TXNCUSINVATTRHIST> result = null;
//		注意:舊語法已不能使用  要使用fstop.orm.po.xxx  PO 要加@Entity
		String queryString = "FROM fstop.orm.po.TXNCUSINVATTRHIST WHERE FDUSERID = :fundUserId";
//		String queryString = " FROM TXNADDRESSBOOK WHERE DPUSERID = :dpuserid ";
		try {
			log.trace("fundUserId >> {}",fundUserId);
			Query<TXNCUSINVATTRHIST> query = getSession().createQuery(queryString);
			query.setParameter("fundUserId", fundUserId);
			result = query.list();
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();			
			log.error("findByDPUserID error >> {}",e);
		}
		
		return result;
		
	}

	public int saveLimit3Time(TXNCUSINVATTRHIST po) {
		int result = -1;
		String queryString = "INSERT INTO TXNCUSINVATTRHIST " + 
				"(FDUSERID,FDQ1,FDQ2,FDQ3,FDQ4,FDQ5,FDSCORE,FDINVTYPE,LASTUSER,LASTDATE,LASTTIME,FDQ6,FDQ7,FDQ8,FDQ9,FDQ10,FDQ11,FDQ12,FDQ13,FDQ14,FDQ15,MARK1,ADUSERIP,AGREE) " +
				"select :FDUSERID,:FDQ1,:FDQ2,:FDQ3,:FDQ4,:FDQ5,:FDSCORE,:FDINVTYPE,:LASTUSER,:LASTDATE,:LASTTIME,:FDQ6,:FDQ7,:FDQ8,:FDQ9,:FDQ10,:FDQ11,:FDQ12,:FDQ13,:FDQ14,:FDQ15,:MARK1,:ADUSERIP,:AGREE FROM sysibm.dual " +
				"WHERE (SELECT COUNT(FDHISTID) FROM TXNCUSINVATTRHIST WHERE FDUSERID =:FDUSERID AND LASTDATE =:LASTDATE) <3";
		try {
			Query query = getSession().createNativeQuery(queryString);
			query.setParameter("FDUSERID", po.getFDUSERID());
			query.setParameter("FDQ1", po.getFDQ1());
			query.setParameter("FDQ2", po.getFDQ2());
			query.setParameter("FDQ3", po.getFDQ3());
			query.setParameter("FDQ4", po.getFDQ4());
			query.setParameter("FDQ5", po.getFDQ5());
			query.setParameter("FDSCORE", po.getFDSCORE());
			query.setParameter("FDINVTYPE", po.getFDINVTYPE());
			query.setParameter("LASTUSER", po.getLASTUSER());
			query.setParameter("LASTDATE", po.getLASTDATE());
			query.setParameter("LASTTIME", po.getLASTTIME());
			query.setParameter("FDQ6", po.getFDQ6());
			query.setParameter("FDQ7", po.getFDQ7());
			query.setParameter("FDQ8", po.getFDQ8());
			query.setParameter("FDQ9", po.getFDQ9());
			query.setParameter("FDQ10", po.getFDQ10());
			query.setParameter("FDQ11", po.getFDQ11());
			query.setParameter("FDQ12", po.getFDQ12());
			query.setParameter("FDQ13", po.getFDQ13());
			query.setParameter("FDQ14", po.getFDQ14());
			query.setParameter("FDQ15", po.getFDQ15());
			query.setParameter("MARK1", po.getMARK1());
			query.setParameter("ADUSERIP", po.getADUSERIP());
			query.setParameter("AGREE", po.getAGREE());
			result = query.executeUpdate();
		} catch (Exception e) {
			log.error("",e);
		}
		return result;
		
	}
	
	//20200226註解 ( 如要上KYC功能修正再打開 )
	public List<TXNCUSINVATTRHIST> findByIDGetLastestData(String fundUserId) {
		List result = null;
//		注意:舊語法已不能使用  要使用fstop.orm.po.xxx  PO 要加@Entity
		String queryString = "FROM fstop.orm.po.TXNCUSINVATTRHIST WHERE FDUSERID = :fundUserId ORDER BY FDHISTID DESC";
//		String queryString = " FROM TXNADDRESSBOOK WHERE DPUSERID = :dpuserid ";
		try {
			log.trace(ESAPIUtil.vaildLog("fundUserId >> " + fundUserId));
			Query<TXNCUSINVATTRHIST> query = getSession().createQuery(queryString);
			query.setParameter("fundUserId", fundUserId);
			result = ESAPIUtil.validList(query.list());
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();			
			log.error("findByDPUserID error >> {}",e);
		}
		
		return result;
		
	}

	public List<TXNCUSINVATTRHIST> findByIDGetLastTimeData(String fundUserId, String lastdate, String lasttime) {
		List<TXNCUSINVATTRHIST> result = null;
//		注意:舊語法已不能使用  要使用fstop.orm.po.xxx  PO 要加@Entity
		String queryString = "FROM fstop.orm.po.TXNCUSINVATTRHIST WHERE FDUSERID = :fundUserId and LASTDATE = :lastdate and LASTTIME = :lasttime ORDER BY FDHISTID DESC";
		try {
			log.trace(ESAPIUtil.vaildLog("fundUserId >> " + fundUserId));
			Query<TXNCUSINVATTRHIST> query = getSession().createQuery(queryString);
			query.setParameter("fundUserId", fundUserId);
			query.setParameter("lastdate", lastdate);
			query.setParameter("lasttime", lasttime);
			result = query.list();
		} catch (Exception e) {
	
			log.error("findByIDGetLastTimeData error >> {}",e);
		}
		
		return result;
		
	}
}