package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import fstop.orm.po.TXNACNAPPLY;
import fstop.orm.po.TXNUSER;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Transactional
@Component
public class TxnAcnApplyDao  extends BaseHibernateDao<TXNACNAPPLY, Serializable> {
	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	public String findACNByCUSIDN(String cusidn) {
		String queryString="FROM fstop.orm.po.TXNACNAPPLY WHERE CUSIDN = :cusidn AND ACNTYPE IS NOT NULL ORDER BY LASTDATE DESC, LASTTIME DESC";
		Query<TXNACNAPPLY> query = getSession().createQuery(queryString);
		query.setParameter("cusidn", cusidn);
		List<TXNACNAPPLY> qresult = query.getResultList();
		return qresult.get(0).getACN();
	}		
	public String findBRHCODByCUSIDN(String cusidn) {
		String queryString="FROM fstop.orm.po.TXNACNAPPLY WHERE CUSIDN = :cusidn AND ACNTYPE IS NOT NULL ORDER BY LASTDATE DESC, LASTTIME DESC";
		Query<TXNACNAPPLY> query = getSession().createQuery(queryString);
		query.setParameter("cusidn", cusidn);
		List<TXNACNAPPLY> qresult = query.getResultList();
		return qresult.get(0).getBRHCOD();
	}	
	public String findACNTYPEByCUSIDN(String cusidn) {
		String queryString="FROM fstop.orm.po.TXNACNAPPLY WHERE CUSIDN = :cusidn AND ACNTYPE IS NOT NULL ORDER BY LASTDATE DESC, LASTTIME DESC";
		Query<TXNACNAPPLY> query = getSession().createQuery(queryString);
		query.setParameter("cusidn", cusidn);
		List<TXNACNAPPLY> qresult = query.getResultList();
		return qresult.get(0).getACNTYPE();
	}
	public TXNACNAPPLY getApplyDataByCUSIDN(String cusidn) {
		String queryString="FROM fstop.orm.po.TXNACNAPPLY WHERE CUSIDN = :cusidn AND ACNTYPE IS NOT NULL ORDER BY LASTDATE DESC, LASTTIME DESC";
		Query<TXNACNAPPLY> query = getSession().createQuery(queryString);
		query.setParameter("cusidn", cusidn);
		List<TXNACNAPPLY> qresult = query.getResultList();
		return qresult.get(0);
	}
	
	public List<TXNACNAPPLY> findByCUSIDN(String cusidn)
	{
		List qresult = null;
		String queryString = " FROM fstop.orm.po.TXNACNAPPLY WHERE CUSIDN = :cusidn ";
		// query.setMaxResults(100);
		try
		{
			log.trace(ESAPIUtil.vaildLog("dpuserid {}" + cusidn));
			Query query = getSession().createQuery(queryString);
			query.setParameter("cusidn", cusidn);
			// Stored XSS
			qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			qresult = ESAPIUtil.validList(qresult);
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("findByCUSIDN error >> {}",e);
		}

		return qresult;
	}
	
	public List<TXNACNAPPLY> findByCUSIDNOrderByDateAndTimeDESC(String cusidn)
	{
		List qresult = null;
		String queryString = " FROM fstop.orm.po.TXNACNAPPLY WHERE CUSIDN = :cusidn AND ACNTYPE IS NOT NULL ORDER BY LASTDATE DESC,LASTTIME DESC";
		// query.setMaxResults(100);
		try
		{
			log.trace(ESAPIUtil.vaildLog("dpuserid {}" + cusidn));
			Query query = getSession().createQuery(queryString);
			query.setParameter("cusidn", cusidn);
			// Stored XSS
			qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			qresult = ESAPIUtil.validList(qresult);
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("findByCUSIDN error >> {}",e);
		}

		return qresult;
	}
}
