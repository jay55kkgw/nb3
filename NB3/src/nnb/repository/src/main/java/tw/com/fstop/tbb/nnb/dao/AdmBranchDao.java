package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.ADMBANK;
import fstop.orm.po.ADMBRANCH;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;
 
@Transactional
@Component
public class AdmBranchDao extends BaseHibernateDao<ADMBRANCH, Serializable> {
	Logger log = LoggerFactory.getLogger(this.getClass());

//	@Transactional(propagation = Propagation.NOT_SUPPORTED)
//	public ADMBRANCH getByACN(String acn) {
//		return findById(acn.substring(0, 3));
//	}
	
	public List<ADMBRANCH> findByBhID(String bhid) {
		List result = null;
		try {
			String queryString = " FROM fstop.orm.po.ADMBRANCH WHERE ADBRANCHID = :bhid ";
			Query<ADMBRANCH> query = getSession().createQuery(queryString);
			query.setParameter("bhid", bhid);
			// Stored XSS
			result = query.list();
			if (null == result)
			{
				result = new ArrayList();
			}
			result = ESAPIUtil.validList(result); // 花太多時間在滾LIST迴圈先註解
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("findByBhID error >> {}",e);
		}
		return result;
	}
	
	public List<ADMBRANCH> getAll() {
		List result = null;
		try {
			log.debug("AdmBranchDao >> getAll");
			String queryString = " FROM fstop.orm.po.ADMBRANCH ORDER BY ADBRANCHID";
			Query<ADMBRANCH> query = getSession().createQuery(queryString);
			result = query.list();
			log.debug("AdmBranchDao result size >> {}", result.size());
			// Stored XSS
			result = query.getResultList();
			if (null == result)
			{
				result = new ArrayList();
			}
			result = ESAPIUtil.validList(result); // 花太多時間在滾LIST迴圈先註解
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("getAll error >> {}",e);
		}
		return result;
	}
	

	/**
	 * 查詢郵遞區號前三碼一樣的分行
	 * 
	 * @return
	 */
	public List<ADMBRANCH> findZipBh(String ZIPCODE) {
		List<ADMBRANCH> result = null;
		try {
			String queryString = " FROM fstop.orm.po.ADMBRANCH WHERE POSTCODE = :ZIPCODE ";
			Query<ADMBRANCH> query = getSession().createQuery(queryString);
			query.setParameter("ZIPCODE", ZIPCODE);
			result = query.list();
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("findZipBh error >> {}",e);
		}
		return result;
	}
	
	/**
	 * 查詢地址前三碼一樣的分行
	 * 
	 * @return
	 */
	public List<ADMBRANCH> findCityBh(String CITY) {
		List<ADMBRANCH> result = null;
		try {
			String queryString = " FROM fstop.orm.po.ADMBRANCH WHERE ADDRESS LIKE CONCAT(:CITY,'%')";
			Query<ADMBRANCH> query = getSession().createQuery(queryString);
			query.setParameter("CITY", CITY);
			result = query.list();
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("findZipBh error >> {}",e);
		}
		return result;
	}
	
	
//	public boolean isBranchMail(String branchCode) {
//		return ((Long) find(
//				"SELECT COUNT(*) FROM ADMBRANCH where ADBRANCHID = ? ",
//				branchCode).iterator().next()) != 0;	
//	}
}
