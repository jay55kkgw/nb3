package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import fstop.orm.po.ADMREMITMENU;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.StrUtils;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Component
public class AdmRemitMenuDao extends BaseHibernateDao<ADMREMITMENU, Serializable> {

	Logger log = LoggerFactory.getLogger(this.getClass());

	/**
	 * 依據傳入參數取得 AdmRemitMenu table符合之資料, 若全部傳入值皆為 ""時,表帶出所有大類(ADLKINDID)資料
	 * 
	 * @return
	 */
	public List<ADMREMITMENU> getRemitMenu(String ADRMTTYPE, String ADLKINDID, String ADMKINDID, String ADRMTID,
			String custype) {
		List<ADMREMITMENU> result = null;

		// 組 WHERE
		StringBuffer qp = new StringBuffer();
		Map<String, String> valuesMap = new LinkedHashMap<String, String>();
		if (StrUtils.isNotEmpty(ADLKINDID)) {
			if (qp.length() > 0)
				qp.append(" and ");
			qp.append("ADLKINDID = :ADLKINDID");
			valuesMap.put("ADLKINDID", ADLKINDID);
		}

		if (StrUtils.isNotEmpty(ADMKINDID)) {
			if (qp.length() > 0)
				qp.append(" and ");
			qp.append("ADMKINDID = :ADMKINDID");
			valuesMap.put("ADMKINDID", ADMKINDID);
		} else {
			qp.append(" and ");
			qp.append(" ADMKINDID <> '00' ");
		}

		if (StrUtils.isNotEmpty(ADRMTID)) {
			if (qp.length() > 0)
				qp.append(" and ");

			if ("*".equals(ADRMTID))
				qp.append(" ADRMTID <> '' ");
			else {
				qp.append("ADRMTID = :ADRMTID");
				valuesMap.put("ADRMTID", ADRMTID);
			}
		} else {
			qp.append(" and ");
			qp.append(" ADRMTID = '' ");
		}

		if (StrUtils.isNotEmpty(ADRMTTYPE)) {
			if (qp.length() > 0)
				qp.append(" and ");
			qp.append("ADRMTTYPE = :ADRMTTYPE");
			valuesMap.put("ADRMTTYPE", ADRMTTYPE);
		}

		if (StrUtils.isNotEmpty(custype)) {
			if (qp.length() > 0)
				qp.append(" and ");

			if (custype.equals("1") || custype.equals("2"))
				custype = "1";
			else if (custype.equals("3"))
				custype = "2";
			else
				custype = "3";

			qp.append(" ADRMTEETYPE" + custype + " = 'Y' ");
		}
		// 執行查詢
		try {
			String queryString = " FROM fstop.orm.po.ADMREMITMENU WHERE " + qp;
			log.debug("queryString " + queryString);
			Query<ADMREMITMENU> query = getSession().createQuery(queryString);
			for (String key : valuesMap.keySet()) {
				query.setParameter(key, valuesMap.get(key));
			}
			result = query.list();
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("getRemitMenu error >> {}",e);
		}

		return result;

	}

	/**
	 * 依據傳入參數取得 AdmRemitMenu table 符合之資料 (管理端選單維護使用)
	 * 
	 * @return
	 */
	public List<ADMREMITMENU> getAdminRemitMenu(String ADRMTTYPE, String ADLKINDID, String ADMKINDID, String ADRMTID,
			String custype) {
		List<ADMREMITMENU> result = null;
		// 組 WHERE
		StringBuffer qp = new StringBuffer();
		Map<String, String> valuesMap = new HashMap<>();
		if (StrUtils.isNotEmpty(ADLKINDID)) {
			if (qp.length() > 0)
				qp.append(" and ");
			qp.append(" ADLKINDID = : ADLKINDID");
			valuesMap.put("ADLKINDID", ADLKINDID);
		}

		if (StrUtils.isNotEmpty(ADMKINDID)) {
			if (qp.length() > 0)
				qp.append(" and ");
			qp.append(" (ADMKINDID = '00' or ADMKINDID = :ADMKINDID) ");
			valuesMap.put("ADMKINDID", ADMKINDID);
		}

		if (StrUtils.isNotEmpty(ADRMTTYPE)) {
			if (qp.length() > 0)
				qp.append(" and ");
			qp.append(" ADRMTTYPE = :ADRMTTYPE");
			valuesMap.put("ADRMTTYPE", ADRMTTYPE);
		}

		if (StrUtils.isNotEmpty(custype)) {
			if (qp.length() > 0)
				qp.append(" and ");

			if (custype.equals("1") || custype.equals("2"))
				custype = "1";
			else if (custype.equals("3"))
				custype = "2";
			else
				custype = "3";

			qp.append(" ADRMTEETYPE" + custype + " = 'Y' ");
		}

		// 執行查詢
		try {
			log.debug("SQL : FROM fstop.orm.po.ADMREMITMENU WHERE " + qp);
			String queryString = " FROM fstop.orm.po.ADMREMITMENU WHERE " + qp;
			Query<ADMREMITMENU> query = getSession().createQuery(queryString);
			for (String key : valuesMap.keySet()) {
				query.setParameter(key, valuesMap.get(key));
			}
			result = query.list();
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("getAdminRemitMenu error >> {}",e);
		}
		return result;
	}

	public List<ADMREMITMENU> getAllAdlKind(String adrmttype) {
		List result = null;
		try {
			Query query = getSession().createQuery("SELECT DISTINCT ADLKINDID, ADMKINDID, ADRMTITEM, ADRMTDESC "
					+ " FROM ADMREMITMENU WHERE ADRMTTYPE = :adrmttype and ADRMTID = '' and ADMKINDID = '00' ORDER BY ADLKINDID ");
			query.setParameter("adrmttype", adrmttype);
			List lresult = query.getResultList();
			for (Object o : lresult) {
				Object[] or = (Object[]) o;
				Map<String, String> rowdata = new HashMap();
				rowdata.put("ADLKINDID", or[0].toString());
				rowdata.put("ADMKINDID", or[1].toString());
				rowdata.put("ADRMTITEM", or[2].toString());
				rowdata.put("ADRMTDESC", or[3].toString());
				log.debug(ESAPIUtil.vaildLog("Object type = " + o.getClass().getSimpleName()));
				List row = new ArrayList<>();
				result.add(row);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("getAllAdlKind error >> {}",e);
		}
		return result;
	}

	/**
	 * 
	 * @param admrmitmenuMap
	 * @return
	 */
	public List<ADMREMITMENU> findAttrsEquals(Map<String, String> admrmitmenuMap) {

		List<ADMREMITMENU> result = null;
		try {
			String[] fields = { "ADRMTID", "ADRMTITEM", "ADRMTDESC", "ADCHKMK", "ADRMTEETYPE1", "ADRMTEETYPE2",
					"ADRMTEETYPE3" };
			String where = StrUtils.implode(" = : and ", fields);
			String queryString = " FROM fstop.orm.po.FROM ADMREMITMENU WHERE 1=1  " + where;
			Query<ADMREMITMENU> query = getSession().createQuery(queryString);
			for (String key : admrmitmenuMap.keySet()) {
				query.setParameter(key, admrmitmenuMap.get(key));
			}
			result = query.list();
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("findAttrsEquals error >> {}",e);
		}

		return result;
	}

	/**
	 * 就查詢全部資料, 依ADLKINDID 、ADMKINDID、 ADRMTID 順序做排序
	 * 
	 * @return
	 */
	public List<ADMREMITMENU> findAllAndSort(String ADRMTTYPE) {

		List<ADMREMITMENU> result = null;
		try {
			String queryString = " FROM fstop.orm.po.ADMREMITMENU WHERE ADRMTTYPE = :ADRMTTYPE ORDER BY ADRMTTYPE, ADLKINDID, ADMKINDID, ADRMTID";
			Query<ADMREMITMENU> query = getSession().createQuery(queryString);
			query.setParameter("ADRMTTYPE", ADRMTTYPE);
			result = query.list();
		} catch (Exception e) {
//			e.printStackTrace();
			log.error("findAllAndSort error >> {}",e);
		}
		return result;
	}

	/**
	 * 
	 * @param rmttype
	 * @param rmtid
	 * @return
	 */
	public ADMREMITMENU findUniqueByRmtid(String rmttype, String rmtid) {
		ADMREMITMENU po = null;
		try {
			String queryString = " FROM fstop.orm.po.ADMREMITMENU WHERE ADRMTTYPE = :rmttype and ADRMTID = :rmtid  ";
			Query<ADMREMITMENU> query = getSession().createQuery(queryString);
			query.setParameter("rmttype", rmttype);
			query.setParameter("rmtid", rmtid);
			po = query.uniqueResult();
		} catch (Exception e) {
//			e.printStackTrace();
			log.error("findUniqueByRmtid error >> {}",e);
		}
		return po;
	}
}
