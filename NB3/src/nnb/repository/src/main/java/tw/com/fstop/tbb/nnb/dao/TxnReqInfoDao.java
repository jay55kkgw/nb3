package tw.com.fstop.tbb.nnb.dao;


import java.io.Serializable;
import java.util.List;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.ADMBRANCH;
import fstop.orm.po.TXNREQINFO;
import tw.com.fstop.spring.dao.BaseHibernateDao;

@Component
public class TxnReqInfoDao extends BaseHibernateDao<TXNREQINFO, Serializable>{
	private Logger log = LoggerFactory.getLogger(getClass());

	public TXNREQINFO getReqInfo(Long id) {
		log.trace("ID>>{}",id);
		TXNREQINFO result = null;
		try {
			String queryString = " FROM fstop.orm.po.TXNREQINFO WHERE REQID = :reqid ";
			Query<TXNREQINFO> query = getSession().createQuery(queryString);
			query.setParameter("reqid", id);
			result = query.getSingleResult();
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("TxnReqInfoDao error >> {}",e);
		}
		return result;
	}
}
