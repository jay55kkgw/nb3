package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.ADMCOUNTRY;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Transactional
@Component
public class AdmCountryDao extends BaseHibernateDao<ADMCOUNTRY,Serializable>{
	Logger log = LoggerFactory.getLogger(this.getClass());

	/**
	 * 用PK取得該ENTITY
	 */
	public ADMCOUNTRY getByPK(String PK){
		log.debug("PK={}",PK);
		
		if ("NTD".equals(PK)){
			PK = "TWD";
		}
		
		ADMCOUNTRY po = null;
		try{
			po = get(ADMCOUNTRY.class,PK);
		}
		catch(Exception e){
			log.error("",e);
		}
		return po;
	}
	/**
	 * 覆寫原本的方法
	 * 取得所有國家名稱
	 */
	public List<ADMCOUNTRY> getAllCountry()
	{
		List qresult = null;
		try
		{
			String queryString = " FROM fstop.orm.po.ADMCOUNTRY ORDER BY ADCTRYNAME";
			Query query = getSession().createQuery(queryString);
			// Stored XSS
			qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			qresult = ESAPIUtil.validList(qresult);

		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("getAllCountry error >> {}",e);
		}
		return qresult;
		
	}
	
	/**
	 * 取得所有國家英文縮寫
	 * @return
	 */
	public List<ADMCOUNTRY> getAllCountryAbb()
	{
		List qresult = null;
		try
		{
			String queryString = " FROM fstop.orm.po.ADMCOUNTRY ORDER BY ADCTRYCODE";
			Query query = getSession().createQuery(queryString);
			// Stored XSS
			qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			qresult = ESAPIUtil.validList(qresult);
			
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("getAllCountryAbb error >> {}",e);
		}
		return qresult;
		
	}
	
	/**
	 * 取得指定國家名稱
	 * @return
	 */
	public ADMCOUNTRY getCountry(String country)
	{
		ADMCOUNTRY qresult = null;
		try
		{
			String queryString = " FROM fstop.orm.po.ADMCOUNTRY WHERE ADCTRYCODE = :adctrycode";
			Query query = getSession().createQuery(queryString);
			query.setParameter("adctrycode", country);
			// Stored XSS
			qresult = (ADMCOUNTRY) query.uniqueResult();
			
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			log.error("getCountry error >> {}",e);
		}
		return qresult;
		
	}
	
	
}