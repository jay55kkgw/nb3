package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import fstop.orm.po.TXNFUNDCOMPANY;
import fstop.orm.po.TXNFUNDDATA;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;
 
@Component
public class TxnFundCompanyDao extends BaseHibernateDao<TXNFUNDCOMPANY,Serializable>{
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	public void deleteAll() {
		this.getSession().createQuery("DELETE FROM TXNFUNDCOMPANY").executeUpdate();
	}
	
	//前收國內
	public List<TXNFUNDCOMPANY> findBeforeAllAndSort() {
		
		List qresult = null;
		StringBuffer sb = new StringBuffer();
		sb.append("select  *  from TXNFUNDCOMPANY WHERE FUNDHOUSEMARK='Y' and COMPANYCODE in ( select distinct( SUBSTR(TRANSCODE,1,2) ) from TXNFUNDDATA where FUSMON ='000' ) ORDER BY COUNTRYTYPE DESC , COMPANYCODE");
		try {
			NativeQuery<TXNFUNDDATA> query = getSession().createNativeQuery(sb.toString()).addEntity(TXNFUNDCOMPANY.class);
			// Stored XSS
			qresult = query.getResultList();
			if (null == qresult) {
				qresult = new ArrayList<>();
			}
		}catch (Exception e) {
			log.error("findBeforeAllAndSort error >> {}",e);
		}
		return ESAPIUtil.validList(qresult);
//		return find("FROM TXNFUNDCOMPANY WHERE FUNDHOUSEMARK='Y' and ORDER BY COUNTRYTYPE DESC ");
	}
	
	//前收國外
	public List<TXNFUNDCOMPANY> findBeforeOverseaAndSort() {
		List qresult = null;
		StringBuffer sb = new StringBuffer();
		sb.append("select  *  from TXNFUNDCOMPANY WHERE FUNDHOUSEMARK='Y' and COUNTRYTYPE<> 'C' and COMPANYCODE in ( select distinct( SUBSTR(TRANSCODE,1,2) ) from TXNFUNDDATA where FUSMON ='000' ) ORDER BY COUNTRYTYPE DESC , COMPANYCODE ");
		NativeQuery<TXNFUNDDATA> query = getSession().createNativeQuery(sb.toString()).addEntity(TXNFUNDCOMPANY.class);
		// Stored XSS
		qresult = query.getResultList();
		if (null == qresult) {
			qresult = new ArrayList<String>();
		}
		return ESAPIUtil.validList(qresult);
//		return find("FROM TXNFUNDCOMPANY WHERE FUNDHOUSEMARK='Y' and COUNTRYTYPE<> 'C' ORDER BY COUNTRYTYPE DESC ");
	}
	
	//後收國內
	public List<TXNFUNDCOMPANY> findAfterAllAndSort() {
		List qresult = null;
		StringBuffer sb = new StringBuffer();
		sb.append("select  *  from TXNFUNDCOMPANY WHERE FUNDHOUSEMARK='Y' and COUNTRYTYPE <> 'B' and COMPANYCODE in ( select distinct( SUBSTR(TRANSCODE,1,2) ) from TXNFUNDDATA where FUSMON !='000' ) ORDER BY COUNTRYTYPE DESC , COMPANYCODE ");
		NativeQuery<TXNFUNDDATA> query = getSession().createNativeQuery(sb.toString()).addEntity(TXNFUNDCOMPANY.class);
		// Stored XSS
		qresult = query.getResultList();
		if (null == qresult) {
			qresult = new ArrayList<String>();
		}
		return ESAPIUtil.validList(qresult);
//		return find("FROM TXNFUNDCOMPANY WHERE FUNDHOUSEMARK='Y' ORDER BY COUNTRYTYPE DESC ");
	}
	
	//後收國外
	public List<TXNFUNDCOMPANY> findAfterOverseaAndSort() {
		List qresult = null;
		StringBuffer sb = new StringBuffer();
		sb.append("select  *  from TXNFUNDCOMPANY WHERE FUNDHOUSEMARK='Y' and COUNTRYTYPE<> 'C' and COMPANYCODE in ( select distinct( SUBSTR(TRANSCODE,1,2) ) from TXNFUNDDATA where FUSMON !='000' ) ORDER BY COUNTRYTYPE DESC , COMPANYCODE ");
		NativeQuery<TXNFUNDDATA> query = getSession().createNativeQuery(sb.toString()).addEntity(TXNFUNDCOMPANY.class);
		// Stored XSS
		qresult = query.getResultList();
		if (null == qresult) {
			qresult = new ArrayList<String>();
		}
		return ESAPIUtil.validList(qresult);
//		return find("FROM TXNFUNDCOMPANY WHERE FUNDHOUSEMARK='Y' and COUNTRYTYPE<> 'C' ORDER BY COUNTRYTYPE DESC ");
	}
	
	/**
	 * 用PK取得該ENTITY
	 */
	public TXNFUNDCOMPANY getByPK(String PK){
		log.debug(ESAPIUtil.vaildLog("PK=>>"+PK));
		
		TXNFUNDCOMPANY po = null;
		try{
			po = get(TXNFUNDCOMPANY.class,PK);
		}
		catch(Exception e){
			log.error("",e);
		}
		return po;
	}
}
