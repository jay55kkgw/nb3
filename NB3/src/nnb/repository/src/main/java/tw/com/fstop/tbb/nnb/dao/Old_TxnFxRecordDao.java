package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import fstop.orm.po.OLD_TXNFXRECORD;
import fstop.orm.po.TXNFXRECORD;
import tw.com.fstop.spring.dao.OLD_BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;
 
@Component
public class Old_TxnFxRecordDao extends OLD_BaseHibernateDao<OLD_TXNFXRECORD, Serializable> {
	protected Logger log = LoggerFactory.getLogger(this.getClass());
	
	public List<OLD_TXNFXRECORD> findByUserId(String fxuserid) {
		List qresult = null;
		String queryString = " FROM fstop.orm.po.OLD_TXNFXRECORD WHERE FXUSERID = :fxuserid ";

		try {
			log.trace("fxuserid {}", fxuserid);
			Query query = getSession().createQuery(queryString);
			query.setParameter("fxuserid", fxuserid);
			// Stored XSS
			qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			log.debug(ESAPIUtil.vaildLog("qresult>>"+qresult));
			qresult = ESAPIUtil.validList(qresult);
			
		} catch (Exception e) {
			log.error("{}", e);
		}

		return qresult;
	}
	
	public List<TXNFXRECORD> findById(String fxuserid) {
		List qresult = null;
//		String queryString = "SELECT A.ADTXNO, A.ADOPID, A.FXUSERID, A.FXTXDATE, A.FXTXTIME, A.FXWDAC, "
//				+ "A.FXWDCURR, A.FXWDAMT, A.FXSVBH, A.FXSVAC, A.FXSVCURR, A.FXSVAMT, A.FXTXMEMO, A.FXTXMAILS, "
//				+ "A.FXTXMAILMEMO, A.FXTXCODE, B.REQINFO FXTITAINFO, A.FXTOTAINFO, A.FXSCHID, A.FXEFEECCY, A.FXEFEE, "
//				+ "'' as FXTELFEE, '' as FXOURCHG, A.FXEXRATE, A.FXREMAIL, A.FXTXSTATUS, A.FXEXCODE, "
//				+ "A.FXMSGSEQNO, A.FXMSGCONTENT, A.LASTDATE, A.LASTTIME, A.FXCERT, 'NB3' AS LOGINTYPE "
//				+ "FROM TXNFXRECORD A LEFT JOIN TXNREQINFO B ON A.FXTITAINFO = B.REQID "
//				+ "WHERE A.FXUSERID = '" + fxuserid + "' ";
		String queryString = "SELECT ADTXNO, ADOPID, FXUSERID, FXTXDATE, FXTXTIME, FXWDAC, "
				+ "FXWDCURR, FXWDAMT, FXSVBH, FXSVAC, FXSVCURR, FXSVAMT, FXTXMEMO, FXTXMAILS, "
				+ "FXTXMAILMEMO, FXTXCODE, FXTITAINFO, FXTOTAINFO, FXSCHID, FXEFEECCY, FXEFEE, "
				+ "'' as FXTELFEE, '' as FXOURCHG, FXEXRATE, FXREMAIL, FXTXSTATUS, FXEXCODE, "
				+ "FXMSGSEQNO, FXMSGCONTENT, LASTDATE, LASTTIME, FXCERT, 'NB' AS LOGINTYPE "
				+ "FROM TXNFXRECORD "
				+ "WHERE FXUSERID = '" + fxuserid + "' ";
		try {
			log.trace("fxuserid {}", fxuserid);
			Query query = getSession().createNativeQuery(queryString, TXNFXRECORD.class);
			// Stored XSS
			qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			log.debug(ESAPIUtil.vaildLog("qresult>>"+qresult));
			qresult = ESAPIUtil.validList(qresult);
			
		} catch (Exception e) {
			log.error("{}", e);
		}

		return qresult;
	}
}
