package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.IDGATE_TXN_STATUS;
import tw.com.fstop.spring.dao.BaseHibernateDao;

@Transactional
@Component
public class Idgate_Txn_StatusDao extends BaseHibernateDao<IDGATE_TXN_STATUS, Serializable>{
	protected Logger log = LoggerFactory.getLogger(this.getClass());
	
	public IDGATE_TXN_STATUS findByPK(IDGATE_TXN_STATUS po){
		//TODO 判斷時間區間
		String queryString = " FROM fstop.orm.po.IDGATE_TXN_STATUS WHERE SESSIONID = :sessionid AND IDGATEID = :idgateid AND TXNID = :txnid";
		IDGATE_TXN_STATUS qresult = null;
		try {
			Query<IDGATE_TXN_STATUS> query = getSession().createQuery(queryString);
			query.setParameter("sessionid", po.getPks().getSESSIONID());
			query.setParameter("idgateid", po.getPks().getIDGATEID());
			query.setParameter("txnid", po.getPks().getTXNID());
			qresult = query.uniqueResult();
		
		} catch (Exception e) {
			log.error("",e);
		}	
		return qresult;
	}
	
}
