package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.SYSPARAMDATA;
import tw.com.fstop.spring.dao.BaseHibernateDao;

@Transactional
@Component
public class SysParamDataDao extends BaseHibernateDao<SYSPARAMDATA,Serializable>{
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	/**
	 * 用PK取得該ENTITY
	 */
	public SYSPARAMDATA getByPK(String PK){
		log.debug("PK={}",PK);
		
		SYSPARAMDATA po = null;
		try{
			po = get(SYSPARAMDATA.class,PK);
			
			//查無資料
			if(po == null){
				po = new SYSPARAMDATA();
			}
		}
		catch(Exception e){
			log.error("",e);
		}
		return po;
	}
}