package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.ADMLOGINMB;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Transactional
@Component
public class AdmLoginMbDao extends BaseHibernateDao<ADMLOGINMB, Serializable> {
	protected Logger log = LoggerFactory.getLogger(this.getClass());

	public List<ADMLOGINMB> findByUserId(String aduserid) {
		List result = null;
		String queryString = " FROM fstop.orm.po.ADMLOGINMB WHERE ADUSERID = :aduserid ";
		try {
			log.debug(ESAPIUtil.vaildLog("AdmLoginMbDao.findByUserId.aduserid: {}"+ aduserid));
			Query<ADMLOGINMB> query = getSession().createQuery(queryString);
			query.setParameter("aduserid", aduserid);
			result = query.list();
			if (null == result)
			{
				result = new ArrayList();
			}
			result = ESAPIUtil.validList(result);
		} catch (Exception e) {
			log.error("",e);
		}
		return result;
	}
	
	
}