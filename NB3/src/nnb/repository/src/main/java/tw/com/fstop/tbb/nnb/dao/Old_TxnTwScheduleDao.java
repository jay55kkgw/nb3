package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import fstop.orm.po.OLD_TXNTWSCHEDULE;
import fstop.orm.po.TXNTWSCHPAY;
import tw.com.fstop.spring.dao.OLD_BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;
 
@Component
public class Old_TxnTwScheduleDao extends OLD_BaseHibernateDao<OLD_TXNTWSCHEDULE, Serializable> {
	protected Logger log = LoggerFactory.getLogger(this.getClass());
	
	public List<OLD_TXNTWSCHEDULE> findByUserId(String dpuserid) {
		List qresult = null;
		String queryString = " FROM fstop.orm.po.OLD_TXNTWSCHEDULE WHERE DPUSERID = :dpuserid ";

		try {
			log.trace(ESAPIUtil.vaildLog("dpuserid {}"+dpuserid));
			Query query = getSession().createQuery(queryString);
			query.setParameter("dpuserid", dpuserid);
			// Stored XSS
			qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			log.debug(ESAPIUtil.vaildLog("qresult>>"+qresult));
			qresult = ESAPIUtil.validList(qresult);
			
		} catch (Exception e) {
			log.error("{}", e);
		}

		return qresult;
	}
	
	public List<TXNTWSCHPAY> findById(String aduserid) {
		List qresult = null;
		String queryString = "SELECT A.DPSCHID, A.ADOPID, A.DPUSERID, "
				+ "CASE WHEN A.DPPERMTDATE IS NOT NULL AND A.DPPERMTDATE <> '' THEN 'C' ELSE 'S' END DPTXTYPE, "
				+ "A.DPPERMTDATE, A.DPFDATE, A.DPTDATE, A.DPWDAC, A.DPSVBH, A.DPSVAC, A.DPTXAMT, A.DPTXMEMO, A.DPTXMAILS, A.DPTXMAILMEMO, "
				+ "A.DPTXCODE, B.REQINFO DPTXINFO, A.DPSDATE, A.DPSTIME, A.DPTXSTATUS, A.XMLCA, A.XMLCN, A.MAC, A.LASTDATE, A.LASTTIME, A.DPSCHNO, A.LOGINTYPE, "
				+ "'' as MSADDR "
				+ "FROM TXNTWSCHEDULE A LEFT JOIN TXNREQINFO B ON A.DPTXINFO = B.REQID "
				+ "WHERE A.DPUSERID = '" + aduserid + "' AND ( DPTXSTATUS = '0' OR DPTXSTATUS = '3' OR DPTXSTATUS = '9' ) ";
		try {
			log.trace("aduserid {}", aduserid);
			Query query = getSession().createNativeQuery(queryString, TXNTWSCHPAY.class);
			// Stored XSS
			qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			log.debug(ESAPIUtil.vaildLog("qresult>>"+qresult));
			qresult = ESAPIUtil.validList(qresult);
			
		} catch (Exception e) {
			log.error("{}", e);
		}

		return qresult;
	}
	
	public boolean updateByPK(Long dpschid, String dptxstatus) {
		boolean result = false;
		String queryString = " UPDATE fstop.orm.po.OLD_TXNTWSCHEDULE C SET C.DPTXSTATUS = :dptxstatus WHERE C.DPSCHID = :dpschid ";

		try {
			log.trace("updateByPK.dpschid: {}", dpschid);
			log.trace("updateByPK.dptxstatus: {}", dptxstatus);
			
			Query query = getSession().createQuery(queryString);
			query.setParameter("dptxstatus", dptxstatus);
			query.setParameter("dpschid", dpschid);
			query.executeUpdate();
			
			result = true;
			
		} catch (Exception e) {
			log.error("{}", e);
		}

		return result;
	}
	
}
