package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.query.Query;

//import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.TXNCHECKACC;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.DateUtil;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Transactional
@Component
public class TxnCheckAccDao extends BaseHibernateDao<TXNCHECKACC, Serializable> {
	protected Logger log = LoggerFactory.getLogger(this.getClass());
	
	
	public List<TXNCHECKACC> findByAdtxno(String adtxno) {
		List result = null;
		try {
	
			String queryString = " FROM fstop.orm.po.TXNCHECKACC WHERE ADTXNO = :adtxno ";
	
			log.trace(ESAPIUtil.vaildLog("adtxno >> " + adtxno));
			Query<TXNCHECKACC> query = getSession().createQuery(queryString);
			query.setParameter("adtxno", adtxno);
	
			result = query.getResultList();
			if (null == result) {
				return  new ArrayList<TXNCHECKACC>();
			} 
			result = ESAPIUtil.validList(result);
		
		} catch (Exception e) {
			log.error("findByAdtxno error >> {}",e);
			return  new ArrayList<TXNCHECKACC>();
		} 
		return result;
	}
	
	public TXNCHECKACC newCheckRecord(String adtxno) {
		
		TXNCHECKACC txn = new TXNCHECKACC();
		txn.setADTXNO(adtxno);
		txn.setSTATUS("");
		txn.setRCODE("");
		txn.setRPT("");
		txn.setCREATEDATE(DateUtil.getTWDate(""));
		txn.setCREATETIME(DateUtil.getTheTime(""));
		txn.setLASTDATE("");
		txn.setLASTTIME("");
		
		save(txn);
		return txn;
	}
	
	public TXNCHECKACC getTxnCheckAcc(String adtxno) {
		TXNCHECKACC txn = null;

		List<TXNCHECKACC> txns = findByAdtxno(adtxno);
		if (txns.size() > 0) {
			txn = txns.get(0);
		}

		return txn;
	}
	
	public void updateReply(String adtxno, String status, String rcode) {
		String updateStr = "UPDATE fstop.orm.po.TXNCHECKACC SET STATUS =:status, RCODE = :rcode, LASTDATE = :lastdate, LASTTIME = :lasttime WHERE ADTXNO  = :adtxno ";
		Query<TXNCHECKACC> query  = getSession().createQuery(updateStr);
		query.setParameter("status", status);
		query.setParameter("rcode", rcode);
		query.setParameter("lastdate", DateUtil.getTWDate(""));
		query.setParameter("lasttime", DateUtil.getTheTime(""));
		query.setParameter("adtxno", adtxno);
		
		int result = query.executeUpdate();
		log.trace(ESAPIUtil.vaildLog("TxnCheckAcc.updateReply >> " + (result>0 ? "Success" : "Fail")));
	}

}
