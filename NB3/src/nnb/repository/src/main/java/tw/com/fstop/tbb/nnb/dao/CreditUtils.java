package tw.com.fstop.tbb.nnb.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CreditUtils {
	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private AdmCardDataDao admCardDataDao;
	
	@Autowired
	private TxnCardApplyDao txnCardApplyDao;
	
	/**
	 * 取得卡片名稱，依據CARDNO
	 * @param 
	 * @return
	 */
	public String getCardNameByCardno(final String cardno) {
		String result = admCardDataDao.findCARDNAME(cardno);
		return result;
	}		

	/**
	 * 查詢一般卡全部資料
	 * @param 
	 * @return
	 */
//	public MVH getNormalCard() {
//
//		try {
//			
//			NewOneCallback newoneCallback = new NewOneCallback() {
//
//				public Object getNewOne() {										
//						// 取得全部
//						List all = admCardDataDao.getNormalCard();
//						return new DBResult(all);
//				}
//			};
//			
//			return (MVH)getPooledObject("getNormalCard()", newoneCallback);
//		} catch (Exception e) {
//			logger.warn("無法取得卡別資料.", e);
//		}
//		return new MVHImpl();
//	}
		
	/**
	 * 取得卡片註記
	 * @param 
	 * @return
	 */
	public String getCardMemoByCardno(final String cardno) {
		String result = admCardDataDao.findCARDMEMO(cardno);
		return result;
	}	
	
	/**
	 * 依據舊戶已申請過的卡片，取得不含已申請過卡片的群組
	 * @param 
	 * @return
	 */
//	public MVH getOtherCardGroup(final String cardtype) {
//		logger.info("CreditUtil.java cardtype==>"+cardtype);		
//		MVHImpl result = null;
//		Rows rows = admCardDataDao.getOtherCardGroupDATA(cardtype);		
//		Rows newrows=new Rows();
//		for(int i =0; i < rows.getSize();i++){			
//			newrows.addRow(rows.getRow(i));
//		}		
//		result = new MVHImpl(newrows);
//		
//		return result;	
//	}
	
	/**
	 * 舊戶已申請過的卡片取得歸戶行
	 * @param 
	 * @return
	 */
//	public MVH getBranchNo(final String uid) {
//		logger.info("CreditUtil.java uid==>"+uid);		
//		MVHImpl result = null;
//		Rows rows = txnCardApplyDao.findBranchNo(uid);		
//		Rows newrows=new Rows();
//		for(int i =0; i < rows.getSize();i++){			
//			newrows.addRow(rows.getRow(i));
//		}		
//		result = new MVHImpl(newrows);
//		
//		return result;	
//	}
}
