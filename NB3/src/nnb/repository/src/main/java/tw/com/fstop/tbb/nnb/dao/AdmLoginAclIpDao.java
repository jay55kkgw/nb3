package tw.com.fstop.tbb.nnb.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.query.Query;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.ADMLOGINACLIP;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Transactional
@Component
public class AdmLoginAclIpDao extends BaseHibernateDao<ADMLOGINACLIP,String> {
	protected Logger log = Logger.getLogger(getClass());
	public List<ADMLOGINACLIP> getAll()
	{
		List qresult = null;
		try
		{
			String queryString = " FROM fstop.orm.po.ADMLOGINACLIP";
			Query query = getSession().createQuery(queryString);
			// Stored XSS
			qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			qresult = ESAPIUtil.validList(qresult);

		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			//修改Information Exposure Through an Error Message
//			e.printStackTrace();
			log.error("AdmBankDao error >> {}",e);
		}
		return qresult;
		
	}
}
