package tw.com.fstop.tbb.nnb.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.TXNLOANAPPLY;
import java.text.SimpleDateFormat;
import tw.com.fstop.spring.dao.BaseHibernateDao;

@Transactional
@Component
public class TxnLoanApplyDao extends BaseHibernateDao<TXNLOANAPPLY, String> {
	private Logger log = LoggerFactory.getLogger(this.getClass());
	
//	@SuppressWarnings( { "unused", "unchecked" })
//	public List<TXNLOANAPPLY> findByLastDate(String lastDate) {
//		
//		String sql = "from fstop.orm.po.TXNLOANAPPLY where LASTDATE <= ? AND STATUS='0' order by APPIDNO ";
//		Query query = getQuery(sql, lastDate);
//		
//		return query.getResultList();
//	}
	
//	@SuppressWarnings( { "unused", "unchecked" })
//	public List<TXNLOANAPPLY> findByUID(String UID) {
//		
//		String sql = "from fstop.orm.po.TXNLOANAPPLY where APPIDNO = ? order by APPIDNO ";
//		
//		Query query = getQuery(sql, UID);		
//		
//		return query.getResultList();
//	}	
	
	/**
	 * 取得某一日的申請貸款筆數
	 * @param currentDate
	 * @return
	 */
	public int countLoanRecord(Date currentDate) {

		String due = new SimpleDateFormat("yyyyMMdd").format(currentDate);
		String sql = "from fstop.orm.po.TXNLOANAPPLY where LASTDATE = :LASTDATE order by APPIDNO ";
		
		Query query = getSession().createQuery(sql);
		query.setParameter("LASTDATE",due);
		List<TXNLOANAPPLY> result = query.getResultList();
		return result.size();
	}	
	/**
	 * 查詢某一筆案件編號
	 * @return
	 */
//	public List<TXNLOANAPPLY> findByRCVNO(String rcvno) {
//		return find("FROM fstop.orm.po.TXNLOANAPPLY WHERE RCVNO = ?", rcvno);
//	}
	
	
	/**
	 * 查詢客戶申請案件
	 * @return
	 */
	public List<TXNLOANAPPLY> findByCUSID(String CUSID) {
		String sql = "FROM fstop.orm.po.TXNLOANAPPLY WHERE APPIDNO = :CUSID";
		Query query = getSession().createQuery(sql);
		query.setParameter("CUSID", CUSID);
		List<TXNLOANAPPLY> result = query.getResultList();
		return result;
	}
	
	
	/**
	 * 變更客戶申請狀態 1:送件中 2:核准 3:不核准
	 * @return
	 */
//	@SuppressWarnings({ "unused", "unchecked" })
//	public TXNLOANAPPLY setStatus(String RCVNO,String STATUS) {
//		
//		//String status = "1";
//		int isUserExist = -1;
//		TXNLOANAPPLY loan = null;
//		try {
//			loan = findById(StrUtils.trim(RCVNO));
//			System.out.println("update RCVNO:" + loan.getRCVNO());
//			isUserExist = ((loan == null) ? 0 : 1);
//		}
//		catch(Exception e) {
//			isUserExist = 0;
//		}
//		if(isUserExist == 1 ) {
//			
//            loan.setSTATUS(STATUS);
//			save(loan);
//			return loan;
//		}
//
//		return null;
//	}
	
	/**
	 * 取得客戶已核准申請案件
	 * @param cusid
	 * @return
	 */
	public List<TXNLOANAPPLY> getRcvno(String cusid) {									
		String sql = "FROM fstop.orm.po.TXNLOANAPPLY WHERE APPIDNO = :APPIDNO AND STATUS ='2' order by APPIDNO";
		Query query = getSession().createQuery(sql);
		query.setParameter("APPIDNO", cusid);
		List<TXNLOANAPPLY> result = query.getResultList();
		return result;
	}
}