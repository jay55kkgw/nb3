package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.query.Query;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.TXNRECMAILLOGRELATION;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Transactional
@Component
public class TxnRecMailLogRelationDao extends BaseHibernateDao<TXNRECMAILLOGRELATION, Serializable>
{
	protected Logger log = Logger.getLogger(getClass());
	
	public List<TXNRECMAILLOGRELATION> findByTypeAndAdtxno(String type, String adtxno) {
		List qresult = null;
		String queryString = " FROM TXNRECMAILLOGRELATION WHERE REC_TYPE = :rec_type and ADTXNO = :adtxno  ";
		try
		{
			Query query = getSession().createQuery(queryString);
			query.setParameter("rec_type", type);
			query.setParameter("adtxno", adtxno);
			
			// Stored XSS
			qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			qresult = ESAPIUtil.validList(qresult);
		}
		catch (Exception e)
		{
			log.error("",e);
		}

		return qresult;
	}
}
