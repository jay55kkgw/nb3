package tw.com.fstop.tbb.nnb.dao;

import java.util.List;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.QUICKLOGINMAPPING;
import fstop.orm.po.QUICKLOGINMAPPING_PK;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Transactional
@Component
public class QuickLoginMappingDao<T> extends BaseHibernateDao<QUICKLOGINMAPPING,QUICKLOGINMAPPING_PK>{
	Logger log = LoggerFactory.getLogger(this.getClass());

	
	// Stored Boundary Violation
//	public List<QUICKLOGINMAPPING> getDataByCusidn(String cusidn){
//		List<QUICKLOGINMAPPING> qresult = null;
	public List<T> getDataByCusidn(String cusidn){
		List<T> qresult = null;
		try
		{
			String queryString = " FROM fstop.orm.po.QUICKLOGINMAPPING WHERE CUSIDN = :cusidn AND DEVICESTATUS IN ('0','1','2' )";
			Query query = getSession().createQuery(queryString);
			query.setParameter("cusidn", cusidn);
			// Stored XSS
			qresult = query.list();
			
			// 
			qresult = (List<T>) ESAPIUtil.validList(qresult);
			
		}
		catch (Exception e)
		{
			log.error("getDataByCusidn error >> {}",e);
		}
		return qresult;
		
		
	}
	
	
	public QUICKLOGINMAPPING getDataByInputData(String cusidn ,String idGateId , String deviceId){
		QUICKLOGINMAPPING po = null;
		try
		{
			String queryString = " FROM fstop.orm.po.QUICKLOGINMAPPING WHERE CUSIDN = :cusidn AND IDGATEID = :idGateId AND DEVICEID = :deviceId";
			Query query = getSession().createQuery(queryString);
			query.setParameter("cusidn", cusidn);
			query.setParameter("idGateId", idGateId);
			query.setParameter("deviceId", deviceId);
			po = (QUICKLOGINMAPPING) query.uniqueResult();
			
		}
		catch (Exception e)
		{
			log.error("getDataByInputData error >> {}",e);
		}
		return po;
	}
	
}