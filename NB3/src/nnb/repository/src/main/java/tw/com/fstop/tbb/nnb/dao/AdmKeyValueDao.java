package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.ADMHOLIDAY;
import fstop.orm.po.ADMKEYVALUE;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Transactional
@Component
public class AdmKeyValueDao extends BaseHibernateDao<ADMKEYVALUE, String> {
	
	Logger log = LoggerFactory.getLogger(this.getClass());
	public ADMKEYVALUE findByCategoryAndKey (String category, String key) {
		ADMKEYVALUE po = null;
		try {
			String queryString = " FROM fstop.orm.po.ADMKEYVALUE WHERE CATEGORY= :ca AND KEY= :ke ";
			Query<ADMKEYVALUE> query = getSession().createQuery(queryString);
			query.setParameter("ca", category);
			query.setParameter("ke", key);
			po=query.list().get(0);
		}
		catch(Exception e) {
//			e.printStackTrace();
			log.error("AdmKeyValueDao error >> {}",e);
		}
		return po;
	}
	
	public String translatorTW (String category, String key)
	{
		ADMKEYVALUE po = findByCategoryAndKey(category,key);
		String result = "";
		if(po != null)
			result = po.getVALUE();
		return result;
	}
	
	public String translatorCN (String category, String key)
	{
		ADMKEYVALUE po = findByCategoryAndKey(category,key);
		String result = "";
		if(po != null)
			result = po.getCHSVALUE();
		return result;
	}
	
	public String translatorEN (String category, String key)
	{
		ADMKEYVALUE po = findByCategoryAndKey(category,key);
		String result = "";
		if(po != null)
			result = po.getENGVALUE();
		return result;
	}
	
	
}