package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.PASSIP;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Transactional
@Component
public class PassIpDao extends BaseHibernateDao<PASSIP, Serializable> {
	protected Logger log = LoggerFactory.getLogger(this.getClass());

	public List<PASSIP> findByUserId(String aduserid) {
		List result = null;
		String queryString = " FROM fstop.orm.po.PASSIP WHERE ADBRANCHIP = :aduserid ";
		try {
			log.trace(ESAPIUtil.vaildLog("PassIpDao.findByUserId.aduserid: {}"+ aduserid));
			Query<PASSIP> query = getSession().createQuery(queryString);
			query.setParameter("aduserid", aduserid);
			result = query.list();
		} catch (Exception e) {
			log.error("",e);
		}
		return result;
	}
	
}