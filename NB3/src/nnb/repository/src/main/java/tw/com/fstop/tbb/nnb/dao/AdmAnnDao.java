package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import fstop.orm.po.ADMANN;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.DateUtil;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Component
public class AdmAnnDao extends BaseHibernateDao<ADMANN,Serializable>{
	private Logger log = LoggerFactory.getLogger(this.getClass());

	public List<ADMANN> getByUser(String cusidn) {
		List qresult = null;
		try
		{
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT * FROM ADMANN ");
			sb.append("WHERE (ALLUSER = '1' OR (ALLUSER = '0' AND USERLIST LIKE '%" + cusidn + "%') )");
			sb.append("AND '" + DateUtil.getCurentDateTime("yyyyMMddHHmmss") + "' >= CONCAT(STARTDATE, STARTTIME) AND '" + DateUtil.getCurentDateTime("yyyyMMddHHmmss") + "'<= CONCAT(ENDDATE, ENDTIME) ");
			//待資料庫新增欄位後
			sb.append(" AND (ANNCHANNEL = 'A' OR ANNCHANNEL = 'N') ");
			sb.append("ORDER BY TYPE DESC, SORTORDER, CONCAT(STARTDATE, STARTTIME) DESC");
			String queryString = sb.toString();
			Query query = getSession().createNativeQuery(queryString, ADMANN.class);
			// Stored XSS
			qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			qresult = ESAPIUtil.validList(qresult);

		}
		catch (Exception e)
		{
			log.error("", e);
		}
		return qresult;
		
	}
	
	public ADMANN getById(String id){
		ADMANN result = null;
		try
		{
			String queryString = " FROM fstop.orm.po.ADMANN WHERE ID = :id ";
			Query<ADMANN> query = getSession().createQuery(queryString);
			query.setParameter("id", id);
			result = query.uniqueResult();
			
		} catch (Exception e) {
			log.error("getbank error >> {}",e);
		}
		return result;
	}
	
	
	public List<ADMANN> getByAll() {
		List qresult = null;
		try
		{
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT * FROM ADMANN ");
			sb.append("WHERE ALLUSER = '1' ");
			sb.append("AND '" + DateUtil.getCurentDateTime("yyyyMMddHHmmss") + "' >= CONCAT(STARTDATE, STARTTIME) AND '" + DateUtil.getCurentDateTime("yyyyMMddHHmmss") + "' <= CONCAT(ENDDATE, ENDTIME) ");
			sb.append("ORDER BY SORTORDER, CONCAT(STARTDATE, STARTTIME) DESC");
			String queryString = sb.toString();
			Query query = getSession().createNativeQuery(queryString, ADMANN.class);
			// Stored XSS
			qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			qresult = ESAPIUtil.validList(qresult);
			
		}
		catch (Exception e)
		{
			log.error("", e);
		}
		return qresult;
		
	}
}
