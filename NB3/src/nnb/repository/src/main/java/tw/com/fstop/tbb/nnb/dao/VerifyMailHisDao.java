package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.VERIFYMAIL_HIS;
import tw.com.fstop.spring.dao.BaseHibernateDao;

@Transactional
@Component
public class VerifyMailHisDao extends BaseHibernateDao<VERIFYMAIL_HIS, Serializable>
{
	protected Logger log = LoggerFactory.getLogger(this.getClass());

	
}