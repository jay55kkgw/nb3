package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import fstop.orm.po.TXNFXSCHPAYDATA;
import fstop.orm.po.TXNFXSCHPAYDATA_PK;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;


@Component
public class TxnFxSchPayDataDao extends BaseHibernateDao<TXNFXSCHPAYDATA, Serializable> {
	private Logger log = LoggerFactory.getLogger(this.getClass());
		
	/**
	 * 清除使用者資料
	 * @param dpuserid 使用者統編
	 */
	public void deleteByUserId(String fxuserid) {
		
		String queryString = " DELETE fstop.orm.po.TXNFXSCHPAYDATA WHERE FXUSERID = :fxuserid ";
		
		try {
			Query query = getSession().createQuery(queryString);
			query.setParameter("fxuserid", fxuserid).executeUpdate();
			
		} catch (Exception e) {
			log.error("",e);
		}
	}
	
	//取得下次轉帳日(今天)
	public String getFxschtxToday(String cusidn,String fxschno) {
		List<TXNFXSCHPAYDATA> po = null;
		TXNFXSCHPAYDATA p_date = null;
		TXNFXSCHPAYDATA_PK p_key = null;
		String result = null;
		try {
			log.debug("TXNFXSCHPAYDATA_Dao >> getAll");
			//今天之後
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String today = sdf.format(date);
			log.trace("FXSCHTXDATE_today>>>{}",today);
			String queryString = "FROM fstop.orm.po.TXNFXSCHPAYDATA WHERE FXUSERID  = :cusidn AND "
					+ "FXSCHNO = :fxschno AND FXSCHTXDATE = :fxschtxdate";
			Query<TXNFXSCHPAYDATA> query = getSession().createQuery(queryString);
			query.setParameter("cusidn", cusidn);
			query.setParameter("fxschno", fxschno);
			query.setParameter("fxschtxdate", today);
			po = query.list();
			if(po.size() != 0) {
				p_date = po.get(0);
				p_key = p_date.getPks();
				result = p_key.getFXSCHTXDATE();				
			}else {
				result = "";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("getFxschtxToday error >> {}",e);
		}
		return result;
	}
	
	//取得下次轉帳日(今天之後)
	public String getFxschtxOther(String cusidn,String fxschno) {
		List<TXNFXSCHPAYDATA> po = null;
		TXNFXSCHPAYDATA p_date = null;
		TXNFXSCHPAYDATA_PK p_key = null;
		String result = null;
		try {
			log.debug("TXNFXSCHPAYDATA_Dao >> getAll");
			//今天之後
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String today = sdf.format(date);
			log.trace("FXSCHTXDATE_today>>>{}",today);
			String queryString = "FROM fstop.orm.po.TXNFXSCHPAYDATA WHERE FXUSERID  = :cusidn AND "
					+ "FXSCHNO = :fxschno AND FXSCHTXDATE > :fxschtxdate";
			Query<TXNFXSCHPAYDATA> query = getSession().createQuery(queryString);
			query.setParameter("cusidn", cusidn);
			query.setParameter("fxschno", fxschno);
			query.setParameter("fxschtxdate", today);
			po = query.list();
			if(po.size() != 0) {
				p_date = po.get(0);
				p_key = p_date.getPks();
				result = p_key.getFXSCHTXDATE();				
			}else {
				result = "";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("getFxschtxOther error >> {}",e);
		}
		return result;
	}
	
	//取得下次轉帳日
	public String getFxschtxdate(String cusidn,String fxschno) {
		List<TXNFXSCHPAYDATA> po = null;
		TXNFXSCHPAYDATA p_date = null;
		TXNFXSCHPAYDATA_PK p_key = null;
		String result = null;
		try {
			log.debug("TXNFXSCHPAYDATA_Dao >> getAll");
			//今天之後
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String today = sdf.format(date);
			log.trace("FXSCHTXDATE_today>>>{}",today);
			String queryString = "FROM fstop.orm.po.TXNFXSCHPAYDATA WHERE FXUSERID  = :cusidn AND "
					+ "FXSCHNO = :fxschno AND FXSCHTXDATE >= :fxschtxdate ORDER BY FXSCHTXDATE";
			Query<TXNFXSCHPAYDATA> query = getSession().createQuery(queryString);
			query.setParameter("cusidn", cusidn);
			query.setParameter("fxschno", fxschno);
			query.setParameter("fxschtxdate", today);
			po = query.list();
			if(po.size() != 0) {
				p_date = po.get(0);
				p_key = p_date.getPks();
				result = p_key.getFXSCHTXDATE();				
			}else {
				result = "";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("getFxschtxdate error >> {}",e);
		}
		return result;
	}
	
	public List<TXNFXSCHPAYDATA> getAll(String cusidn,  Map<String, String> reqParam) {
		List result = null;
		try {
			log.debug("TXNFXSCHPAYDATA_Dao >> getAll");
			String dpfdate = reqParam.get("CMSDATE");
			dpfdate = dpfdate.replace("/","");
			log.trace("dpfdate>>>{}",dpfdate);
			String dptdate = reqParam.get("CMEDATE");
			dptdate = dptdate.replace("/","");
			log.trace("dptdate>>>{}",dptdate);
//			String queryString = " FROM fstop.orm.po.TXNFXSCHPAYDATA s WHERE FXUSERID = :cusidn AND FXSCHTXDATE BETWEEN :dpfdate AND :dptdate"
//					+ " AND exists (SELECT FXSCHNO FROM fstop.orm.po.TXNFXSCHPAY where FXSCHNO = s.FXSCHNO and FXTXSTATUS <> '3')";
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT * FROM TXNFXSCHPAYDATA s WHERE s.FXUSERID  = '"+cusidn+"'");
			sb.append(" AND s.FXSCHTXDATE BETWEEN '"+dpfdate+"'"+ " AND '"+ dptdate+"'");
			sb.append(" AND exists (SELECT FXSCHNO FROM TXNFXSCHPAY where FXSCHNO = s.FXSCHNO and FXUSERID = s.FXUSERID and FXTXSTATUS <> '3')");
			String queryString = sb.toString();
			
			Query query = getSession().createNativeQuery(queryString, TXNFXSCHPAYDATA.class);
//			Query<TXNFXSCHPAYDATA> query = getSession().createQuery(queryString);
//			query.setParameter("cusidn", cusidn);
//			query.setParameter("dpfdate", dpfdate);
//			query.setParameter("dptdate", dptdate);
			result = query.list();
			if (null == result)
			{
				result = new ArrayList();
			}
			result = ESAPIUtil.validList(result); 
			log.debug("TXNFXSCHPAYDATADao result size >> {}", result.size());
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("getAll error >> {}",e);
		}
		return result;
	}
	
	/**
	 * 查詢DPTXSTATUS By FGTXSTATUS
	 * @return
	 */
	public List<TXNFXSCHPAYDATA> getByUid(String cusidn , Map<String, String> reqParam) {
		List<TXNFXSCHPAYDATA> result = new ArrayList();
		String fgtxstatus = reqParam.get("FGTXSTATUS");			
		log.trace(ESAPIUtil.vaildLog("fgtxstatus>>>{}"+fgtxstatus));
		String dpfdate = reqParam.get("CMSDATE");
		dpfdate = dpfdate.replace("/","");
		log.trace("dpfdate>>>{}",dpfdate);
		String dptdate = reqParam.get("CMEDATE");
		dptdate = dptdate.replace("/","");
		log.trace("dptdate>>>{}",dptdate);
		String queryString = " FROM fstop.orm.po.TXNFXSCHPAYDATA WHERE FXUSERID = :cusidn AND FXTXSTATUS = :fgtxstatus AND FXSCHTXDATE BETWEEN :dpfdate AND :dptdate";
		try {
			log.trace(ESAPIUtil.vaildLog("cusidn>>{}"+cusidn));
			log.trace(ESAPIUtil.vaildLog("FGTXSTATUS>>{}"+fgtxstatus));
			Query<TXNFXSCHPAYDATA> query = getSession().createQuery(queryString);
			query.setParameter("cusidn", cusidn);
			query.setParameter("fgtxstatus", fgtxstatus);
			query.setParameter("dpfdate", dpfdate);
			query.setParameter("dptdate", dptdate);
			result = query.list();
			log.debug("TXNFXSCHPAYDATADao result size >> {}", result.size());
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("getByUid error >> {}",e);
		}		
		return result;		
	}
	/**
	 * 查詢TXNFXSCHPAYDATA By FGTXSTATUS
	 * 
	 * @param flag is true ? queryall : status
	 * @return 
	 */
	public List<TXNFXSCHPAYDATA> getByUidOrAll(Map<String, String> reqParam, Boolean flag) {
		List result = null;
		String sdate = reqParam.get("sdate");
		String edate = reqParam.get("edate");
		String cusidn = reqParam.get("cusidn");
		String dptxstatus = reqParam.get("dptxstatus");
		StringBuilder queryString = new StringBuilder();		
		queryString.append(
				"FROM fstop.orm.po.TXNFXSCHPAYDATA WHERE FXUSERID = :cusidn AND FXSCHTXDATE BETWEEN :sdate AND :edate  ");

		try {
			Query<TXNFXSCHPAYDATA> query;
			if (flag == false) {
				queryString.append(" AND FXTXSTATUS = :dptxstatus");
				query = getSession().createQuery(queryString.toString());
				query.setParameter("dptxstatus", dptxstatus);
			} else {
				query = getSession().createQuery(queryString.toString());
			}

			query.setParameter("cusidn", cusidn);
			query.setParameter("sdate", sdate);
			query.setParameter("edate", edate);

			result = query.getResultList();
			if (null == result)
			{
				result = new ArrayList();
			}
			result = ESAPIUtil.validList(result);
		} catch (Exception e) {
			log.error("getByUid error >> {}", e);
		}
		return result;
	}
	
}