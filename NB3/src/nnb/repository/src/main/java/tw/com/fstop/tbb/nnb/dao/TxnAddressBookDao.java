package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import fstop.orm.po.TXNADDRESSBOOK;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;
 
@Component
public class TxnAddressBookDao extends BaseHibernateDao<TXNADDRESSBOOK, Serializable> {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	
	
	public TXNADDRESSBOOK findByInput(String dpuserid , String dpgoname , String dpabmail ) {
		TXNADDRESSBOOK po = null;
		String queryString = " FROM fstop.orm.po.TXNADDRESSBOOK WHERE DPUSERID = :dpuserid AND DPGONAME= :dpgoname AND DPABMAIL = :dpabmail";
		try {
//			log.debug("dpuserid>>{} , dpgoname>>{} , dpabmail>>{}", dpuserid , dpgoname , dpabmail);
			Query query = getSession().createQuery(queryString);
			query.setParameter("dpuserid", dpuserid);
			query.setParameter("dpgoname", dpgoname);
			query.setParameter("dpabmail", dpabmail);
			po =(TXNADDRESSBOOK) query.uniqueResult();
			
		} catch (Exception e) {
			log.error("{}", e);
		}
		
		return po;
	}
	
	
	/**
	 * 清除使用者資料
	 * @param dpuserid 使用者統編
	 */
	public void deleteByUserId(String dpuserid) {
		
		String queryString = " DELETE fstop.orm.po.TXNADDRESSBOOK WHERE DPUSERID = :dpuserid ";
		
		try {
			Query query = getSession().createQuery(queryString);
			query.setParameter("dpuserid", dpuserid).executeUpdate();
			
		} catch (Exception e) {
			log.error("",e);
		}
	}
	
	public List<TXNADDRESSBOOK> findByDPUserID(String dpuserid) {
		List result = null;
//		注意:舊語法已不能使用  要使用fstop.orm.po.xxx  PO 要加@Entity
		String queryString = "FROM fstop.orm.po.TXNADDRESSBOOK WHERE DPUSERID = :dpuserid";
//		String queryString = " FROM TXNADDRESSBOOK WHERE DPUSERID = :dpuserid ";
		try {
			log.trace(ESAPIUtil.vaildLog("dpuserid {}"+dpuserid));
			Query<TXNADDRESSBOOK> query = getSession().createQuery(queryString);
			query.setParameter("dpuserid", dpuserid);
			result = query.list();
			if(result == null) {
				result = new ArrayList();
			}
			result = ESAPIUtil.validList(result);
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("findByDPUserID error >> {}",e);
		}
		
		return result;
		
	}
	public void remove(TXNADDRESSBOOK obj)
	{
		super.remove(obj);
	}
//	public void save(TXNADDRESSBOOK obj) {
//		super.saveORupdate(obj);
//	}
}
