package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import fstop.orm.po.TXNTWRECORD;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Transactional
@Component
public class TxnTwRecordDao extends BaseHibernateDao<TXNTWRECORD, Serializable> {
	private Logger log = LoggerFactory.getLogger(getClass());
	
	/**
	 * 清除使用者資料
	 * @param dpuserid 使用者統編
	 */
	public void deleteByUserId(String dpuserid) {
		
		String queryString = " DELETE fstop.orm.po.TXNTWRECORD WHERE DPUSERID = :dpuserid ";
		
		try {
			Query query = getSession().createQuery(queryString);
			query.setParameter("dpuserid", dpuserid).executeUpdate();
			
		} catch (Exception e) {
			log.error("",e);
		}
	}

	/**
	 * 起訖時間內交易紀錄 帳號為空查詢全部交易紀錄，不空則查詢帳號下資料
	 * 
	 * @return
	 */
	public List<TXNTWRECORD> findByDateRange(String cusidn, Map<String, String> reqParam) {
		log.trace(ESAPIUtil.vaildLog("findByDateRange >> " + ESAPIUtil.toJson(reqParam)));
		List<TXNTWRECORD> result = null;
		String acn = reqParam.get("ACN");
		log.trace(ESAPIUtil.vaildLog("acn >> " + acn));
		String cmsdate = reqParam.get("CMSDATE").replace("/", "");
		log.trace("cmsdate>>{}", cmsdate);
		String cmedate = reqParam.get("CMEDATE").replace("/", "");
		log.trace("cmedate>>{}", cmedate);

		StringBuffer queryString = new StringBuffer();
		queryString.append(
				"from fstop.orm.po.TXNTWRECORD where DPUSERID = :cusidn and DPTXDATE >= :cmsdate and DPTXDATE <= :cmedate and DPTXSTATUS = 0 ");

		if (acn.equals("AllAcn")) {
			Query<TXNTWRECORD> query = getSession().createQuery(queryString.toString());
			query.setParameter("cusidn", cusidn);
			query.setParameter("cmsdate", cmsdate);
			query.setParameter("cmedate", cmedate);
			result = query.list();
		} else {
			queryString.append("AND DPWDAC = :acn ");
			Query<TXNTWRECORD> query = getSession().createQuery(queryString.toString());
			query.setParameter("cusidn", cusidn);
			query.setParameter("cmsdate", cmsdate);
			query.setParameter("cmedate", cmedate);
			query.setParameter("acn", acn);
			result = query.list();
		}
		log.trace(ESAPIUtil.vaildLog("QueryResult >> " + ESAPIUtil.toJson(result)));
		return result;
	}

//更新重發次數
	public String updataDpremail(String cusidn, String adtxno) {
		TXNTWRECORD po = null;
		String result = null;
		try {
			log.debug("TXNTWRECORD_Dao >> updataDpremail");
			String queryString = " FROM fstop.orm.po.TXNTWRECORD WHERE DPUSERID = :cusidn AND ADTXNO = :adtxno";
			Query<TXNTWRECORD> query = getSession().createQuery(queryString);
			query.setParameter("cusidn", cusidn);
			query.setParameter("adtxno", adtxno);
			po = query.uniqueResult();
			String dpremail = po.getDPREMAIL();
			int dpreMailCount = 0;
			try {
				dpreMailCount = Integer.parseInt(dpremail);
				dpreMailCount = dpreMailCount + 1;
				log.trace("dpreMailCount>>{}", dpreMailCount);
			} catch (NumberFormatException e) {
				log.trace("{}", "參數非數字");
				;
			}
			dpremail = String.valueOf(dpreMailCount);
			po.setDPREMAIL(dpremail);
			update(po);
			result = dpremail;
		} catch (Exception e) {
//			e.printStackTrace();
			log.error("取得 Request Info 有誤.", e);
			result = "";
		}
		return result;
	}

	// 檢查今日是否做過交易
	public String isTodayHasUse(String cusidn, String adopid, String DPWDAC, String DPSVAC, String DPTXAMT,
			String dptxdate) {
		String result = "";
		try {
			log.debug(ESAPIUtil
					.vaildLog("isTodayHasUse param >> " + cusidn + adopid + DPWDAC + DPSVAC + DPTXAMT + dptxdate));
			String queryString = " FROM fstop.orm.po.TXNTWRECORD WHERE DPTXAMT = :DPTXAMT AND DPWDAC = :DPWDAC "
					+ "AND DPSVAC = :DPSVAC AND DPTXDATE = :dptxdate AND DPEXCODE = :msgCode "
					+ "AND DPUSERID = :cusidn AND ADOPID = :adopid";
			Query<TXNTWRECORD> query = getSession().createQuery(queryString);
			query.setParameter("cusidn", cusidn);
			query.setParameter("adopid", adopid);
			query.setParameter("dptxdate", dptxdate);
			query.setParameter("DPWDAC", DPWDAC);
			query.setParameter("DPSVAC", DPSVAC);
			query.setParameter("DPTXAMT", DPTXAMT);
			query.setParameter("msgCode", "");
			List<TXNTWRECORD> po = query.getResultList();
			if (po.size() > 0)
				result = po.get(0).getDPTXTIME();
		} catch (Exception e) {
//			e.printStackTrace();
			log.error(e.getMessage());
			result = "";
		}
		return result;
	}

//	@SuppressWarnings( { "unused", "unchecked" })
//	public List<TXNTWRECORD> findSchduleResultByDateRange(String uid, String startDt, String endDt, String status) {
//		List result = new ArrayList();
//		//DPRETXNO = ''表示只取同一交易的最後一筆
//		if(StrUtils.isEmpty(status)) { //全部
//			try {
//				result = findScheduleAllStatusByDateRange(uid, startDt, endDt);
//			}
//			finally {}
//		}
//		else if (status.equals("0")){ //成功
//			try {
//				String qhql = "from TXNTWRECORD where DPUSERID = ? and DPTXDATE >= ? and DPTXDATE <= ? and DPSCHID > 0 and DPTXSTATUS = ? and DPRETXNO = '' ";
//				Query query = getQuery(qhql, uid, startDt, endDt, status);
//				result = query.list();
//			}
//			finally{}
//		}
//		else if (status.equals("1")){ //失敗
//			try {
//				String qhql = "from TXNTWRECORD where DPUSERID = ? and DPTXDATE >= ? and DPTXDATE <= ? and DPSCHID > 0 and DPTXSTATUS = ? and DPRETXNO = '' and DPRETXSTATUS <> '1' and DPRETXSTATUS <> '2' and DPRETXSTATUS <> '5'";
//				Query query = getQuery(qhql, uid, startDt, endDt, status);
//				result = query.list();
//			}
//			finally{}
//		}
//		else if (status.equals("2")){ //處理中
//			try {
//				result = findSchedulePendingByDateRange(uid, startDt, endDt, status);
//			}
//			finally{}
//		}
//		return result;
//	}
//	
//	/**
//	 * 找出指定使用者某一期間內的處理中預約交易(如包括查詢日則會加上查詢日尚未執行的預約到期交易)
//	 * @param uid
//	 * @param startDt
//	 * @param endDt
//	 * @param status
//	 * @return
//	 */
//	public List<TXNTWRECORD> findSchedulePendingByDateRange(String uid, String startDt, String endDt, String status) {
//		List result = null;
//		
//		Date today = new Date();
//		String tyyyymmdd = DateTimeUtils.format("yyyyMMdd", today);
//		int iToday = Integer.parseInt(tyyyymmdd);
//		int iStartDate = Integer.parseInt(startDt);
//		int iEndDate = Integer.parseInt(endDt);
//		
//		//判斷查詢期間是否包含今日
//		if (iToday >= iStartDate && iToday <= iEndDate)
//		{//查詢期間有包含今日，要多查TXNTWSCHEDULE檔今日未執行的交易
//			String sql = "" +
//			"select * from TXNTWRECORD where DPUSERID = ? and DPTXDATE >= ? and DPTXDATE <= ? and DPSCHID > 0 and DPTXSTATUS = '1' and DPRETXNO = '' and (DPRETXSTATUS = '1' or DPRETXSTATUS = '2' or DPRETXSTATUS = '5') " + 
//			"union all " + 
//			getSqlForRecordDueTo(today, uid) +
//			"order by DPSCHNO ";
//			
//			System.out.println("@@@　TxnTwRecordDao.findSchedulePendingByDateRange() SQL　== " + sql);
//			System.out.flush();
//			
//			SQLQuery query = getSession().createSQLQuery(sql).addEntity("record", TXNTWRECORD.class);
//			query.setParameter(0, uid);
//			query.setParameter(1, startDt);
//			query.setParameter(2, endDt);
//
//			result = query.list();
//		}
//		else
//		{//查詢期間沒有包含今日，只查TXNTWRECORD檔
//			String qhql = "from TXNTWRECORD where DPUSERID = ? and DPTXDATE >= ? and DPTXDATE <= ? and DPSCHID > 0 and DPTXSTATUS = '1' and DPRETXNO = '' and (DPRETXSTATUS = '1' or DPRETXSTATUS = '2' or DPRETXSTATUS = '5')";
//			Query query = getQuery(qhql, uid, startDt, endDt, status);
//			result = query.list();
//		}
//		
//		return result;
//	}
//	
//	/**
//	 * 找出指定使用者某一期間內的所有已執行預約交易(如包括查詢日則會加上查詢日尚未執行的預約到期交易)
//	 * @param uid
//	 * @param startDt
//	 * @param endDt
//	 * @return
//	 */
//	public List<TXNTWRECORD> findScheduleAllStatusByDateRange(String uid, String startDt, String endDt) {
//		List result = null;
//		
//		Date today = new Date();
//		String tyyyymmdd = DateTimeUtils.format("yyyyMMdd", today);
//		int iToday = Integer.parseInt(tyyyymmdd);
//		int iStartDate = Integer.parseInt(startDt);
//		int iEndDate = Integer.parseInt(endDt);
//		
//		//判斷查詢期間是否包含今日
//		if (iToday >= iStartDate && iToday <= iEndDate)
//		{//查詢期間有包含今日，要多查TXNTWSCHEDULE檔今日未執行的交易
//			String sql = "" +
//			"select * from TXNTWRECORD where DPUSERID = ? and DPTXDATE >= ? and DPTXDATE <= ? and DPSCHID > 0 and DPRETXNO = '' " + 
//			"union all " + 
//			getSqlForRecordDueTo(today, uid) +
//			"order by DPSCHNO ";
//			
//			System.out.println("@@@　TxnTwRecordDao.findScheduleAllStatusByDateRange() SQL　== " + sql);
//			System.out.flush();
//			
//			SQLQuery query = getSession().createSQLQuery(sql).addEntity("record", TXNTWRECORD.class);
//			query.setParameter(0, uid);
//			query.setParameter(1, startDt);
//			query.setParameter(2, endDt);
//
//			result = query.list();
//		}
//		else
//		{//查詢期間沒有包含今日，只查TXNTWRECORD檔
//			String qhql = "from TXNTWRECORD where DPUSERID = ? and DPTXDATE >= ? and DPTXDATE <= ? and DPSCHID > 0 and DPRETXNO = '' ";
//			Query query = getQuery(qhql, uid, startDt, endDt);
//			result = query.list();
//		}
//		
//		return result;
//	}
//	
//	/**
//	 * 返回指定使用者某一日到期的尚未發送之預約交易SQL字串
//	 * @param currentDate
//	 * @param uid
//	 * @return
//	 */
//	private String getSqlForRecordDueTo(Date currentDate, String uid)
//	{
//		String due = DateTimeUtils.format("yyyyMMdd", currentDate);
//		
//		String sql = "" +
//		"select X.DPSCHNO as ADTXNO, X.ADOPID, X.DPUSERID, '" + due + "' as DPTXDATE, '000000' as DPTXTIME, X.DPWDAC, X.DPSVBH, X.DPSVAC, X.DPTXAMT, X.DPTXMEMO, X.DPTXMAILS, X.DPTXMAILMEMO, X.DPSCHID, '0' as DPEFEE, '' as PCSEQ, '' as DPTXNO, X.DPTXCODE, 1 as DPTITAINFO, '' as DPTOTAINFO, '0' as DPREMAIL, '' as DPTXSTATUS, '' as DPEXCODE, '' as DPRETXNO, '0' as DPRETXSTATUS, '" + due + "' as LASTDATE, '000000' as LASTTIME, X.DPSCHNO " +
//		"from ( " +
//		" SELECT distinct sch.* FROM                                                                                                  " +
//		" 	TXNTWSCHEDULE sch , (" + TxnTwScheduleDao.getSqlForScheduleExcecNextDate(currentDate, uid)+ ") nextexec " +
//		" 	where		                                                                                                         " +
//		" 			nextexec.DPSCHID = sch.DPSCHID and " +
//		"           nextexec.nextexecdate is not null and nextexec.nextexecdate = '" + due + "' " +		
//		"           and ( ( DPTXSTATUS = '0' and  LASTDATE <= '" + due + "' )   " +
//		"					 or ( DPTXSTATUS = '1' and  LASTDATE = '" + due + "'))  " + //不含狀態2是因有可能Record檔已有執行過此筆的資料，避免重複
//		") X ";
//		
//		return sql;
//	}
//
//	/**
//	 * 找出某一日的轉帳到期, 第二次轉帳 => 來源為預約檔(DPTXSTATUS = '1' and LASTDATE = 今天)
//	 * @param currentDate
//	 * @return
//	 */
//	public List<TXNTWSCHEDULE> findScheduleDueToReSend_S(Date currentDate) {
//		String due = DateTimeUtils.format("yyyyMMdd", currentDate);
//		Calendar cal = Calendar.getInstance();
//		cal.setTime(currentDate);
//		int yyyy = cal.get(Calendar.YEAR);
//		int mm = cal.get(Calendar.MONTH) + 1;
//		int dd = cal.get(Calendar.DATE);
//
//		String sql = "" +
//		" SELECT distinct {sch.*} FROM                                                                                                  " +
//		" 	TXNTWSCHEDULE sch , (" + TxnTwScheduleDao.getSqlForScheduleExcecNextDate(currentDate)+ ") nextexec " +
//		" 	where		                                                                                                         " +
//		" 			nextexec.DPSCHID = sch.DPSCHID and " +
//		"           nextexec.nextexecdate is not null and nextexec.nextexecdate = ? " +		
//		"           and  (sch.DPTXSTATUS = '1' and  sch.LASTDATE = ?) and sch.LOGINTYPE <> ? ";		
//		
//		
//		System.out.println("@@@　TxnTwRecordDao.findScheduleDueToReSend() SQL　== " + sql);
//		System.out.flush();
//		
//		
//		SQLQuery query = getSession().createSQLQuery(sql).addEntity("sch", TXNTWSCHEDULE.class);
//		
//		query.setParameter(0, due);
//		query.setParameter(1, due);
//		query.setParameter(2, "MB");
//
//		return query.list();
//	}
//
//	/**
//	 * 找出某一日的轉帳到期, 第二次轉帳 => 來源為轉帳紀錄檔(DPTXSTATUS = '0' or DPTXSTATUS = '5')
//	 * @param currentDate
//	 * @return
//	 */
//	public List<TXNTWRECORD> findScheduleDueToReSend_R(Date currentDate) {
//		String due = DateTimeUtils.format("yyyyMMdd", currentDate);
//		Calendar cal = Calendar.getInstance();
//		cal.setTime(currentDate);
//		int yyyy = cal.get(Calendar.YEAR);
//		int mm = cal.get(Calendar.MONTH) + 1;
//		int dd = cal.get(Calendar.DATE);
//
//		String sql = "" +
//		" SELECT distinct {rec.*} FROM                                                                                                  " +
//		" 	TXNTWSCHEDULE sch , (" + TxnTwScheduleDao.getSqlForScheduleExcecNextDate(currentDate)+ ") nextexec, TXNTWRECORD rec  " +
//		" 	where		                                                                                                         " +
//		" 			nextexec.DPSCHID = rec.DPSCHID and " +
//		"           nextexec.nextexecdate is not null and nextexec.nextexecdate = ?                         " +		
//		"           and (  (sch.DPTXSTATUS = '0' or sch.DPTXSTATUS = '5') and  sch.LASTDATE = ? and       " +
//		"                  rec.DPRETXSTATUS = '5' and rec.LASTDATE = ? and rec.DPRETXNO = '' " +		
//		"               ) and sch.LOGINTYPE <> ? " ;
//		
//		
//		
//		System.out.println("@@@　TxnTwRecordDao.findScheduleDueToReSend() SQL　== " + sql);
//		System.out.flush();
//		
//		
//		SQLQuery query = getSession().createSQLQuery(sql).addEntity("rec", TXNTWRECORD.class);
//
//		query.setParameter(0, due);
//		query.setParameter(1, due);
//		query.setParameter(2, due);
//		query.setParameter(3, "MB");
//
//		return query.list();
//	}
//	
//	@Transactional(propagation = Propagation.REQUIRES_NEW)
//	public void writeTxnRecord(TXNREQINFO info ,TXNTWRECORD record) {
//		if(record != null)
//			log.debug("WRITE NEW TXNTWRECORD ADTXNO:" + record.getADTXNO());
//
//		BOLifeMoniter.setValue("__TXNRECORD", record);
//		if(info != null) {
//			BOLifeMoniter.setValue("__TITAINFO", info);
//			save(info);
//			record.setDPTITAINFO(info.getREQID());
//		}
//		save( record);
//	}
//
//	@Transactional(propagation = Propagation.REQUIRES_NEW)
//	public void rewriteTxnRecord(TXNTWRECORD record) {
//		BOLifeMoniter.setValue("__TXNRECORD", record);
//		save( record);
//	}
//
//	public List<TXNTWRECORD> findResendStatus(String lastdate, String[] retxstatus) {
//		String restatusCondition = "";
//		if(retxstatus == null || retxstatus.length == 0)
//			return new ArrayList();
//		else if(retxstatus.length == 1)  {
//			if("*".equals(retxstatus[0]))
//				restatusCondition = "DPRETXSTATUS <> '0' ";
//			else
//				restatusCondition = "DPRETXSTATUS = '" + retxstatus[0] + "' ";
//		}
//		else if(retxstatus.length > 1)
//			restatusCondition = "DPRETXSTATUS in ('" + StrUtils.implode("','", retxstatus) + "') ";
//
//
//		Query query = getQuery("FROM TXNTWRECORD " +
//				" WHERE DPSCHID > 0 and DPTXSTATUS = '1' and  " +  restatusCondition +
//				" and LASTDATE = ? " +      //and DPRETXNO = '' " +
//				" ORDER BY LASTDATE DESC, LASTTIME DESC ", lastdate);
//
//		return query.list();
//	}
//
//	/**
//	 * 找到重送之後寫的那筆 TXNTWRECORD
//	 * @param adtxno
//	 * @return
//	 */
//	public TXNTWRECORD findResendRecord(String adtxno) {
//		return this.findUniqueBy("DPRETXNO", adtxno);
//	}
//
//	public void updateStatus(TXNTWRECORD record, String status) {
//		log.debug("update TwRecord status: " + status);
//		try {
//			Date d = new Date();
//			record.setLASTDATE(DateTimeUtils.format("yyyyMMdd", d));
//			record.setLASTTIME(DateTimeUtils.format("HHmmss", d));
//			record.setDPRETXSTATUS(status);
//
//			save(record);
//		}
//		catch(Exception e) {
//
//		}
//		finally {
//
//		}
//	}
//	/**
//	 * 取得某一日的預約交易結果總筆數
//	 * @param currentDate
//	 * @return
//	 */
//	public int countTWRecordTot(Date currentDate) {
//		String due = DateTimeUtils.format("yyyyMMdd", currentDate);
//
//		String sql = "" +
//		"select rec.* from TxnTwRecord rec where rec.DPSCHID>0 and rec.DPTXDATE= ? ";
//
//		SQLQuery query = getSession().createSQLQuery(sql).addEntity("rec", TXNTWRECORD.class);
//		query.setParameter(0, due);
//		return query.list().size();
//	}
//	/**
//	 * 取得某一日的預約交易結果成功筆數
//	 * @param currentDate
//	 * @return
//	 */
//	public int countTWRecordSuc(Date currentDate) {
//		String due = DateTimeUtils.format("yyyyMMdd", currentDate);
//
//		String sql = "" +
//		"select rec.* from TxnTwRecord rec where rec.DPSCHID>0 and rec.DPTXDATE= ?  and rec.DPEXCODE=''";
//
//		SQLQuery query = getSession().createSQLQuery(sql).addEntity("rec", TXNTWRECORD.class);
//		query.setParameter(0, due);
//		return query.list().size();
//	}
//	/**
//	 * 取得某一日的預約交易結果系統面失敗筆數
//	 * @param currentDate
//	 * @return
//	 */
//	public int countTWRecordFailS(Date currentDate) {
//		String due = DateTimeUtils.format("yyyyMMdd", currentDate);
//
//		String sql = "" +
//		"select rec.* from TxnTwRecord rec where rec.DPSCHID>0 and rec.DPTXDATE= ?  and rec.DPEXCODE like 'Z%'";
//
//		SQLQuery query = getSession().createSQLQuery(sql).addEntity("rec", TXNTWRECORD.class);
//		query.setParameter(0, due);
//		return query.list().size();
//	}
//	/**
//	 * 取得某一日的預約交易結果客戶面操作失敗筆數
//	 * @param currentDate
//	 * @return
//	 */
//	public int countTWRecordFailC(Date currentDate) {
//		String due = DateTimeUtils.format("yyyyMMdd", currentDate);
//
//		String sql = "" +
//		"select rec.* from TxnTwRecord rec where rec.DPSCHID>0 and rec.DPTXDATE= ?  and rec.DPEXCODE like 'E%'";
//
//		SQLQuery query = getSession().createSQLQuery(sql).addEntity("rec", TXNTWRECORD.class);
//		query.setParameter(0, due);
//		return query.list().size();
//	}			
	/**
	 * 起訖時間內交易紀錄，附加電文當條件 電文為空查詢全部交易紀錄，不空則查詢帳號下資料
	 * 
	 * @return
	 */
	public List<TXNTWRECORD> findByDateRange(Map<String, String> reqParam) {
		List result = null;
		try {
			log.trace(ESAPIUtil.vaildLog("findByDateRange>>{}"+ reqParam));
			String cusidn = reqParam.get("cusidn");
			log.trace(ESAPIUtil.vaildLog("cusidn>>{}"+ cusidn));
			String adopid = reqParam.get("adopid");
			log.trace(ESAPIUtil.vaildLog("adopid>>{}"+ adopid));
			String sdate = reqParam.get("sdate").replace("/", "");
			log.trace(ESAPIUtil.vaildLog("sdate>>{}"+ sdate));
			String edate = reqParam.get("edate").replace("/", "");
			log.trace(ESAPIUtil.vaildLog("edate>>{}"+ edate));

			StringBuffer queryString = new StringBuffer();
			queryString.append(
					"from fstop.orm.po.TXNTWRECORD where DPUSERID = :cusidn and DPTXDATE >= :sdate and DPTXDATE <= :edate");

			if (adopid == null || adopid.trim().length() == 0) {
				Query<TXNTWRECORD> query = getSession().createQuery(queryString.toString());
				query.setParameter("cusidn", cusidn);
				query.setParameter("sdate", sdate);
				query.setParameter("edate", edate);
				result = query.getResultList();
			} else {
				queryString.append(" AND ADOPID = :adopid ");
				Query<TXNTWRECORD> query = getSession().createQuery(queryString.toString());
				query.setParameter("cusidn", cusidn);
				query.setParameter("sdate", sdate);
				query.setParameter("edate", edate);
				query.setParameter("adopid", adopid);
				result = query.getResultList();
			}
			if (null == result) {
				result = new ArrayList();
			}
			result = ESAPIUtil.validList(result);
			log.trace(ESAPIUtil.vaildLog("QueryResult>>{}" + result));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("cancelAcn error >> {}", e);
		}

		return result;
	}

	
	
	/**
	 * 取得臺幣交易編號清單 by user
	 * 
	 * @return
	 */
	public List<String> getAdtxNoByUser(String cusidn) {
		List<String> qresult = null;
		try {
			
			StringBuffer bfs = new StringBuffer();
			bfs.append(" SELECT ADTXNO FROM TXNTWRECORD ");
			bfs.append(" WHERE DPUSERID = '" + cusidn + "' ");

			Query query = getSession().createNativeQuery(bfs.toString());
			// Stored XSS
			qresult = query.getResultList();
			if (null == qresult) {
				qresult = new ArrayList<String>();
			}
			// qresult = ESAPIUtil.validList(qresult); // 花太多時間在滾LIST迴圈先註解

		} catch (Exception e) {
			log.error("", e);
		}
		return qresult;

	}
	
	
}
