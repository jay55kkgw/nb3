package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.query.Query;
import org.springframework.stereotype.Component;

import fstop.orm.po.ADMMYREMITMENU;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Component
public class AdmMyRemitMenuDao extends BaseHibernateDao<ADMMYREMITMENU, Serializable>
{
	protected Logger log = Logger.getLogger(getClass());

	/**
	 * 找到某人的的 RemitMenu
	 * 
	 * @param uid
	 * @return
	 */
	public List<ADMMYREMITMENU> findByUid(String uid)
	{
		List qresult = null;
		try
		{
			// 注意:舊語法已不能使用 要使用fstop.orm.po.xxx PO 要加@Entity
			String queryString = " FROM fstop.orm.po.ADMMYREMITMENU WHERE ADUID = :uid ";
			Query query = getSession().createQuery(queryString);
			query.setParameter("uid", uid);
			// Stored XSS
			qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			qresult = ESAPIUtil.validList(qresult);
		}
		catch (Exception e)
		{
//			e.printStackTrace();
			log.error("findByUid error >> {}",e);
		}
		return qresult;

	}

	public ADMMYREMITMENU findByUidRmtid(String uid, String rmtid, String rmttype)
	{
		ADMMYREMITMENU po = null;
		try
		{
			String queryString = "FROM fstop.orm.po.ADMMYREMITMENU WHERE ADUID = :uid and ADRMTID = :rmtid and ADRMTTYPE = :rmttype  ";
			Query<ADMMYREMITMENU> query = getSession().createQuery(queryString);
			query.setParameter("uid", uid);
			query.setParameter("rmtid", rmtid);
			query.setParameter("rmttype", rmttype);
			po = query.uniqueResult();
		}
		catch (Exception e)
		{
//			e.printStackTrace();
			log.error("findByUidRmtid error >> {}",e);
		}
		return po;
	}

	public void removeByUidRmtid(String uid, String rmtid, String rmttype)
	{
		ADMMYREMITMENU po = null;
		try
		{
			po = findByUidRmtid(uid, rmtid, rmttype);
			if (po != null)
			{
				remove(po);
			}
		}
		catch (Exception e)
		{
//			e.printStackTrace();
			log.error("removeByUidRmtid error >> {}",e);
		}

	}

}
