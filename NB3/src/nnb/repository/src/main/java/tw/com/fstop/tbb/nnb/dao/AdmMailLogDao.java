package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.ADMMAILLOG;
import tw.com.fstop.spring.dao.BaseHibernateDao;


	
@Transactional
@Component
public class AdmMailLogDao extends BaseHibernateDao<ADMMAILLOG, Serializable>
{
	protected Logger log = Logger.getLogger(getClass());

	@Autowired
	AdmMailContentDao admMailContentDao;

	@Autowired
	TxnRecMailLogRelationDao txnRecMailLogRelationDao;

	/**
	 * 清除使用者資料
	 * @param dpuserid 使用者統編
	 */
	public void deleteByUserId(String aduserid) {
		List<String> qresult = null;
		try {
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT A.RELID, B.ADMAILLOGID ");
			sb.append("FROM TXNRECMAILLOGRELATION A LEFT JOIN ADMMAILLOG B ON A.ADMAILLOGID = B.ADMAILLOGID ");
			sb.append("WHERE B.ADUSERID = :aduserid");
			String queryString = sb.toString();
			Query query = getSession().createNativeQuery(queryString);
			query.setParameter("aduserid", aduserid);
			// Stored XSS
			qresult = query.getResultList();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			
			// 逐筆清除資料
			if (qresult != null) {
				for (Object o : qresult) {
					Object[] or = (Object[]) o;
					Map<String, String> rowdata = new HashMap();
					
					// 清除使用者資料
					deleteByFK(Long.valueOf(or[0].toString()), Long.valueOf(or[1].toString()));
				}
			}
		}
		catch (Exception e)
		{
			log.error("", e);
		}
			
	}
	/**
	 * 清除使用者資料
	 * @param relid
	 * @param admaillogid
	 */
	public void deleteByFK(Long relid, Long admaillogid) {
		
		String queryString1 = " DELETE fstop.orm.po.TXNRECMAILLOGRELATION WHERE RELID = :relid ";
		String queryString2 = " DELETE fstop.orm.po.ADMMAILLOG WHERE ADMAILLOGID = :admaillogid ";
		
		try {
			Query query1 = getSession().createQuery(queryString1);
			query1.setParameter("relid", relid).executeUpdate();
			Query query2 = getSession().createQuery(queryString2);
			query2.setParameter("admaillogid", admaillogid).executeUpdate();
			
		} catch (Exception e) {
			log.error("",e);
		}
	}
	
	public List<ADMMAILLOG> findByDateRange(String  aduserid, String adsendstatus, String startDateTime, String endDatetime) {
		List qresult = null;
		String queryString = "";
		try
		{
			if("".equals(adsendstatus)) {
				//修正SQL 將有INDEX的欄位往前移
				queryString = "FROM ADMMAILLOG WHERE ADSENDTIME >= :startDateTime and ADSENDTIME <= :endDatetime and ADUSERID= :aduserid";
				Query query = getSession().createQuery(queryString);
				query.setParameter("aduserid", aduserid);
				query.setParameter("startDateTime", startDateTime);
				query.setParameter("endDatetime", endDatetime);
				qresult = query.getResultList();

			}
			else {
				//修正SQL 將有INDEX的欄位往前移
				queryString = "FROM ADMMAILLOG WHERE ADSENDTIME >= :startDateTime and ADSENDTIME <= :endDatetime and ADUSERID = :aduserid and ADSENDSTATUS = :adsendstatus ";
				Query query = getSession().createQuery(queryString);
				query.setParameter("aduserid", aduserid);
				query.setParameter("startDateTime", startDateTime);
				query.setParameter("endDatetime", endDatetime);
				query.setParameter("adsendstatus", adsendstatus);
				qresult = query.getResultList();
			}
		}
		catch (Exception e)
		{
			log.error("",e);
		}
		
		return qresult;
	}
	
}
