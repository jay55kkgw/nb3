package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import fstop.orm.po.CITIES;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Component
public class CityDataDao extends BaseHibernateDao<CITIES, Serializable>{
	
	Logger log = LoggerFactory.getLogger(this.getClass());

	@SuppressWarnings("unchecked")
	public List<CITIES> getCities() {
		List qresult = null;
		String queryString = "FROM fstop.orm.po.CITIES";
		try
		{
			Query<CITIES> query = getSession().createQuery(queryString);
			// Stored XSS
			qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList<CITIES>();
			}
			qresult = ESAPIUtil.validList(qresult);
		}
		catch (Exception e)
		{
			log.error("getCities error >> {}",e);
		}

			return qresult;
	}
}
