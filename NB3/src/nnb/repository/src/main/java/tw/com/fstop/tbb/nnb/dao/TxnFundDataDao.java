package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import fstop.orm.po.TXNFUNDDATA;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Component
public class TxnFundDataDao extends BaseHibernateDao<TXNFUNDDATA, Serializable>
{
	Logger log = LoggerFactory.getLogger(this.getClass());

	public List<TXNFUNDDATA> findByCompany(String fee_type , String cmp_code, Boolean regular_purchase)
	{
		List qresult = null;
		String queryString = "";
			log.debug(ESAPIUtil.vaildLog("cmp_code=>>"+ cmp_code));
			try
			{
				String FUNDT = "";
				if (regular_purchase) {
					FUNDT = " and FUNDT != '1' ";
				}
				log.debug("regular_purchase = " + regular_purchase);
				log.debug("FUNDT = " + FUNDT);
				
				if("A".equals(fee_type)) {
					queryString = " FROM fstop.orm.po.TXNFUNDDATA WHERE SUBSTRING(TRANSCODE,1,2) = :cmp_code and FUSMON !='000'" + FUNDT + "order by TRANSCODE ";
				}else {
					queryString = " FROM fstop.orm.po.TXNFUNDDATA WHERE SUBSTRING(TRANSCODE,1,2) = :cmp_code and FUSMON ='000'" + FUNDT + "order by TRANSCODE ";
				}
				
				Query query = getSession().createQuery(queryString);
				query.setParameter("cmp_code",cmp_code);
				// Stored XSS
				qresult = query.list();
				if (null == qresult)
				{
					qresult = new ArrayList();
				}
				qresult = ESAPIUtil.validList(qresult);
			}
			catch (Exception e)
			{
//				e.printStackTrace();
				log.error("findByCompany error >> {}",e);
			}

			return qresult;
	}

	public List<TXNFUNDDATA> findByFundGroupMark(String fee_type , String group_mark)
	{
		List result = null;
		String sql = "";
		if("A".equals(fee_type)){
			sql = "SELECT a.* FROM TXNFUNDDATA a, TXNFUNDCOMPANY b WHERE b.FUNDGROUPMARK = ? "
					+ " and SUBSTR(a.TRANSCODE, 1, 2) = b.COMPANYCODE and a.FUSMON != '000' order by a.TRANSCODE";
		}else {
			sql = "SELECT a.* FROM TXNFUNDDATA a, TXNFUNDCOMPANY b WHERE b.FUNDGROUPMARK = ? "
					+ " and SUBSTR(a.TRANSCODE, 1, 2) = b.COMPANYCODE and a.FUSMON ='000' order by a.TRANSCODE";
		}
		try
		{
			log.debug(ESAPIUtil.vaildLog("group_mark={}"+ group_mark));
			NativeQuery<TXNFUNDDATA> query = getSession().createNativeQuery(sql).addEntity("a", TXNFUNDDATA.class);
			query.setParameter(1, group_mark);
			result = query.list();
			// Stored XSS
			result = query.list();
			if (null == result)
			{
				result = new ArrayList();
			}
			result = ESAPIUtil.validList(result); // 花太多時間在滾LIST迴圈先註解
		}
		catch (Exception e)
		{
//			e.printStackTrace();
			log.error("findByFundGroupMark error >> {}",e);
		}
		return result;
	}

	/**
	 * 用PK取得該ENTITY
	 */
	public TXNFUNDDATA getByPK(String PK)
	{
		log.debug(ESAPIUtil.vaildLog("TXNFUNDDATA PK={}"+ PK));

		TXNFUNDDATA po = null;
		try
		{
			po = get(TXNFUNDDATA.class, PK);
		}
		catch (Exception e)
		{
			log.error("TXNFUNDDATA error", e);
		}
		return po;
	}
}