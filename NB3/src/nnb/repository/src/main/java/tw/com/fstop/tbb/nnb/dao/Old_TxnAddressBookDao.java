package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import fstop.orm.po.OLD_TXNADDRESSBOOK;
import tw.com.fstop.spring.dao.OLD_BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;
 
@Component
public class Old_TxnAddressBookDao extends OLD_BaseHibernateDao<OLD_TXNADDRESSBOOK, Serializable> {
	protected Logger log = LoggerFactory.getLogger(this.getClass());
	
	public List<OLD_TXNADDRESSBOOK> findByUserId(String dpuserid) {
		List qresult = null;
		String queryString = " FROM fstop.orm.po.OLD_TXNADDRESSBOOK WHERE DPUSERID = :dpuserid ";

		try {
			log.trace("dpuserid {}", dpuserid);
			Query query = getSession().createQuery(queryString);
			query.setParameter("dpuserid", dpuserid);
			// Stored XSS
			qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			log.debug(ESAPIUtil.vaildLog("qresult>>"+qresult));
			qresult = ESAPIUtil.validList(qresult);
			
		} catch (Exception e) {
			log.error("{}", e);
		}

		return qresult;
	}
	
	
	
	public OLD_TXNADDRESSBOOK findByInput(String dpuserid , String dpgoname , String dpabmail ) {
		OLD_TXNADDRESSBOOK po = null;
		String queryString = " FROM fstop.orm.po.OLD_TXNADDRESSBOOK WHERE DPUSERID = :dpuserid AND DPGONAME= :dpgoname AND DPABMAIL = :dpabmail";
		try {
			log.debug("dpuserid>>{} , dpgoname>>{} , dpabmail>>{}", dpuserid , dpgoname , dpabmail);
			Query query = getSession().createQuery(queryString);
			query.setParameter("dpuserid", dpuserid);
			query.setParameter("dpgoname", dpgoname);
			query.setParameter("dpabmail", dpabmail);
			po =(OLD_TXNADDRESSBOOK) query.uniqueResult();
			
		} catch (Exception e) {
			log.error("{}", e);
		}
		
		return po;
	}
}
