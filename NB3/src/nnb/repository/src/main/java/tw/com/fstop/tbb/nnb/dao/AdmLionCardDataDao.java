package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.ADMLIONCARDDATA;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Transactional
@Component
public class AdmLionCardDataDao extends BaseHibernateDao<ADMLIONCARDDATA,Serializable>{
	Logger log = LoggerFactory.getLogger(this.getClass());

	
	/**
	 * 取得獅友會分會
	 */
	public List<Map<String, String>> getLionBranch(String zone)
	{
		List<Map<String, String>> resultListMap = null;
		try
		{ 
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT TEAM FROM ADMLIONCARDDATA ");
			sb.append("WHERE ZONE = ?");
			sb.append("ORDER BY ADMID");
			String queryString = sb.toString();
			Query query = getSession().createNativeQuery(queryString);
			query.setParameter(1, zone);
			// Stored XSS
			List<Object> qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			qresult = ESAPIUtil.validList(qresult);
			
			resultListMap = new ArrayList<Map<String, String>>();
			for (Object o : qresult)
			{
				Map<String, String> map = new HashMap<String, String>();
				map.put("TEAM", (String) o);

				resultListMap.add(map);
			}
			log.debug(ESAPIUtil.vaildLog("resultListMap="+ resultListMap));
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("getAllCountry error >> {}",e);
		}
		return resultListMap;
		
	}
	
	/**
	 * 取得獅友會分區
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, String>> getLionData()
	{
		List<Map<String, String>> resultListMap = null;
		try
		{
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT DISTINCT ZONE FROM ADMLIONCARDDATA");
			String queryString = sb.toString();
			Query query = getSession().createNativeQuery(queryString);
			// List<String> ADMZIPDATAList = ESAPIUtil.validList(query.list());

			// Stored XSS
			List<Object> qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			qresult = ESAPIUtil.validList(qresult);

			resultListMap = new ArrayList<Map<String, String>>();
			for (Object o : qresult)
			{
				Map<String, String> map = new HashMap<String, String>();
				map.put("ZONE", (String) o);

				resultListMap.add(map);
			}
			log.debug(ESAPIUtil.vaildLog("resultListMap="+ resultListMap));
		}
		catch (Exception e)
		{
//			e.printStackTrace();
			log.error("getLionData error >> {}",e);
		}
		return resultListMap;
	}
}