package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import fstop.orm.po.OLD_TXNCUSINVATTRHIST;
import tw.com.fstop.spring.dao.OLD_BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;
 
@Component
public class Old_TxnCusInvAttrHistDao extends OLD_BaseHibernateDao<OLD_TXNCUSINVATTRHIST, Serializable> {
	protected Logger log = LoggerFactory.getLogger(this.getClass());
	
	public List<OLD_TXNCUSINVATTRHIST> findByUserId(String fduserid) {
		List qresult = null;
		String queryString = " FROM fstop.orm.po.OLD_TXNCUSINVATTRHIST WHERE FDUSERID = :fduserid AND LASTDATE = :lastDate ";

		try {
			log.trace("findByUserId.fduserid {}", fduserid);
			
			String lastDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
			log.trace("findByUserId.lastDate {}", lastDate);
			
			Query query = getSession().createQuery(queryString);
			query.setParameter("fduserid", fduserid);
			query.setParameter("lastDate", lastDate);
			// Stored XSS
			qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			log.debug(ESAPIUtil.vaildLog("qresult>>"+qresult));
			qresult = ESAPIUtil.validList(qresult);
			
		} catch (Exception e) {
			log.error("{}", e);
		}

		return qresult;
	}
}
