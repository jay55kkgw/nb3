package tw.com.fstop.tbb.nnb.dao;
import java.io.Serializable;
import java.util.List;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import fstop.orm.po.GOLDPRICE;
import tw.com.fstop.spring.dao.BaseHibernateDao;

@Component
public class GoldPriceDao extends BaseHibernateDao<GOLDPRICE, Serializable> {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	
	@SuppressWarnings("unchecked")
	public List<GOLDPRICE> findByQDATE(String qdate) {

		List<GOLDPRICE> result = null;
		try {
			System.out.println("q>>"+qdate);
			String queryString = " FROM fstop.orm.po.GOLDPRICE WHERE QDATE =:qdate ORDER BY LASTTIME DESC";
			Query<GOLDPRICE> query = getSession().createQuery(queryString);
			query.setParameter("qdate", qdate);
			result = query.list();
			
		} catch (Exception e) {
//			e.printStackTrace();
			log.error("GoldPriceDao error >> {}",e);
		}
		return result;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<GOLDPRICE> getAll() {
		List<GOLDPRICE> result = null;
		try {
			log.debug("GOLDPRICEDao >> getAll");
			String queryString = " FROM fstop.orm.po.GOLDPRICE ORDER BY GOLDPRICEID";
			Query<GOLDPRICE> query = getSession().createQuery(queryString);
			result = query.list();
			log.debug("GoldPriceDao result size >> {}", result.size());
		} catch (Exception e) {
//			e.printStackTrace();
			log.error("getAll error >> {}",e);
		}
		return result;
	}
	public List<GOLDPRICE> getRecord(String querytype,String from,String to) {
		List<GOLDPRICE> rc = null;
		
		if(querytype.equals("TODAYLAST")){
			rc = getTODAYLastRecord(from);
		}
		if(querytype.equals("TODAY")){
			rc = getTODAYRecord(from);
		}
		if(querytype.equals("SOMEDAY")){
			rc = getSOMEDAYRecord(from);
		}
		if(querytype.equals("LASTDAY")){
			rc = getLASTDAYRecord();
		}
		if(querytype.equals("LASTMON")){
			rc = getLASTMONRecord(from,to);
		}
		if(querytype.equals("LASTSIXMON")){
			rc = getLASTSIXMONRecord(from,to);
		}
		if(querytype.equals("LASTYEAR")){
			rc = getLASTYEARRecord(from,to);
		}
		if(querytype.equals("PERIOD")){
			rc = getPERIODRecord(from,to);
		}
		return rc;
	}
	@SuppressWarnings("unchecked")
	public List<GOLDPRICE> getTODAYLastRecord(String datefrom){
		List<GOLDPRICE> result = null;
		try{
			log.debug("getTODAYLastRecord");
			String queryString = "FROM fstop.orm.po.GOLDPRICE WHERE GOLDPRICEID IN(SELECT MAX(GOLDPRICEID) FROM GOLDPRICE WHERE QDATE = :QDATE)";
			Query<GOLDPRICE> query = getSession().createQuery(queryString);
			query.setParameter("QDATE",datefrom);
			result = query.list();
			log.debug("result.size()={}",result.size());
		}
		catch(Exception e){
//			e.printStackTrace();
			log.error("getTODAYLastRecord error >> {}",e);
		}
		return result;
	}
	@SuppressWarnings("unchecked")
	public List<GOLDPRICE> getTODAYRecord(String datefrom){
		List<GOLDPRICE> result = null;
		try{
			log.debug("getTODAYRecord");
			String queryString = "FROM fstop.orm.po.GOLDPRICE WHERE QDATE = :QDATE";
			Query<GOLDPRICE> query = getSession().createQuery(queryString);
			query.setParameter("QDATE",datefrom);
			result = query.list();
			log.debug("result.size()={}",result.size());
		}
		catch(Exception e){
//			e.printStackTrace();
			log.error("getTODAYRecord error >> {}",e);
		}
		return result;
	}
	@SuppressWarnings("unchecked")
	public List<GOLDPRICE> getSOMEDAYRecord(String datefrom){
		List<GOLDPRICE> result = null;
		try{
			log.debug("getSOMEDAYRecord");
			String queryString = "FROM fstop.orm.po.GOLDPRICE WHERE QDATE = :QDATE";
			Query<GOLDPRICE> query = getSession().createQuery(queryString);
			query.setParameter("QDATE",datefrom);
			result = query.list();
			log.debug("result.size()={}",result.size());
		}
		catch(Exception e){
//			e.printStackTrace();
			log.error("getSOMEDAYRecord error >> {}",e);
		}
		return result;
	}
	@SuppressWarnings("unchecked")
	public List<GOLDPRICE> getLASTDAYRecord(){
		List<GOLDPRICE> result = null;
		try{
			log.debug("getLASTDAYRecord");
			String queryString = "FROM fstop.orm.po.GOLDPRICE WHERE QDATE IN(SELECT MAX(QDATE) FROM GOLDPRICE)";
			Query<GOLDPRICE> query = getSession().createQuery(queryString);
			result = query.list();
			log.debug("result.size()={}",result.size());
		}
		catch(Exception e){
//			e.printStackTrace();
			log.error("getLASTDAYRecord error >> {}",e);
		}
		return result;	
	}
	
	@SuppressWarnings("unchecked")
	public List<GOLDPRICE> getLASTMONRecord(String datefrom,String dateto){
		List<GOLDPRICE> result = null;
		try{
			log.debug("getLASTMONRecord");
			String queryString = "FROM fstop.orm.po.GOLDPRICE WHERE GOLDPRICEID IN(SELECT  MAX(GOLDPRICEID)  FROM GOLDPRICE GROUP BY QDATE) AND QDATE >= :FROM AND QDATE <= :TO ORDER BY QDATE";
			Query<GOLDPRICE> query = getSession().createQuery(queryString);
			query.setParameter("FROM",datefrom);
			query.setParameter("TO",dateto);
			result = query.list();
			log.debug("result.size()={}",result.size());
		}
		catch(Exception e){
//			e.printStackTrace();
			log.error("getLASTMONRecord error >> {}",e);
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<GOLDPRICE> getLASTSIXMONRecord(String datefrom,String dateto){
		List<GOLDPRICE> result = null;
		try{
			log.debug("getLASTSIXMONRecord");
			String queryString = "FROM fstop.orm.po.GOLDPRICE WHERE GOLDPRICEID IN(SELECT MAX(GOLDPRICEID) FROM GOLDPRICE GROUP BY QDATE) AND QDATE >= :FROM and QDATE <= :TO ORDER BY QDATE";
			Query<GOLDPRICE> query = getSession().createQuery(queryString);
			query.setParameter("FROM",datefrom);
			query.setParameter("TO",dateto);
			result = query.list();
			log.debug("result.size()={}",result.size());
		}
		catch(Exception e){
//			e.printStackTrace();
			log.error("getLASTSIXMONRecord error >> {}",e);
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<GOLDPRICE> getLASTYEARRecord(String datefrom,String dateto){
		List<GOLDPRICE> result = null;
		try{
			log.debug("getLASTYEARRecord");
			String queryString = "FROM fstop.orm.po.GOLDPRICE WHERE GOLDPRICEID IN(SELECT MAX(GOLDPRICEID) FROM GOLDPRICE GROUP BY QDATE) AND QDATE >= :FROM and QDATE <= :TO ORDER BY QDATE";
			Query<GOLDPRICE> query = getSession().createQuery(queryString);
			query.setParameter("FROM",datefrom);
			query.setParameter("TO",dateto);
			result = query.list();
			log.debug("result.size()={}",result.size());
		}
		catch(Exception e){
//			e.printStackTrace();
			log.error("getLASTYEARRecord error >> {}",e);
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<GOLDPRICE> getPERIODRecord(String datefrom,String dateto) {
		List<GOLDPRICE> result = null;
		try{
			log.debug("getLASTYEARRecord");
			String queryString = "FROM fstop.orm.po.GOLDPRICE WHERE GOLDPRICEID IN(SELECT MAX(GOLDPRICEID) FROM GOLDPRICE GROUP BY QDATE) AND QDATE >= :FROM and QDATE <= :TO ORDER BY QDATE";
			Query<GOLDPRICE> query = getSession().createQuery(queryString);
			query.setParameter("FROM",datefrom);
			query.setParameter("TO",dateto);
			result = query.list();
			log.debug("result.size()={}",result.size());
		}
		catch(Exception e){
//			e.printStackTrace();
			log.error("getPERIODRecord error >> {}",e);
		}
		return result;
	}
}
