package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.TRNS_NNB_CUSDATA;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Transactional
@Component
public class Trns_nnb_CusDataDao extends BaseHibernateDao<TRNS_NNB_CUSDATA, Serializable> {
	protected Logger log = LoggerFactory.getLogger(this.getClass());

	public List<TRNS_NNB_CUSDATA> findByUserId(String cusidn) {
		List<TRNS_NNB_CUSDATA> result = null;
		String queryString = " FROM fstop.orm.po.TRNS_NNB_CUSDATA WHERE CUSIDN = :cusidn ";
		try {
			log.debug(ESAPIUtil.vaildLog("Trns_nnb_CusDataDao.findByUserId.cusidn: {}"+ cusidn));
			Query<TRNS_NNB_CUSDATA> query = getSession().createQuery(queryString);
			query.setParameter("cusidn", cusidn);
			result = query.list();
		} catch (Exception e) {
			log.error("",e);
		}
		return result;
	}
	
	/**
	 * 清除使用者資料
	 * @param dpuserid 使用者統編
	 */
	public void deleteByUserId(String cusidn) {
		
		String queryString = " DELETE fstop.orm.po.TRNS_NNB_CUSDATA WHERE CUSIDN = :cusidn ";
		
		try {
			Query query = getSession().createQuery(queryString);
			query.setParameter("cusidn", cusidn).executeUpdate();
			
		} catch (Exception e) {
			log.error("",e);
		}
	}
	
	/**
	 * 更新使用者資料轉檔進度
	 * @param dpuserid 使用者統編
	 */
	public int updateByCol(String id, String col, String vue) {
	    String updateSql = "UPDATE Trns_nnb_CusData SET " + col + "='" + vue +  "' WHERE CUSIDN = '" + id + "'";
	    Query query = getSession().createNativeQuery(updateSql);
	    return query.executeUpdate();
	}

	public boolean checkCusNB3(String cusidn) {
		boolean result = false;
		
		try {
			StringBuffer queryString = new StringBuffer();
			queryString.append(" SELECT * FROM TRNS_NNB_CUSDATA ");
			queryString.append(" WHERE CUSIDN = '" + cusidn + "' ");
			queryString.append(" AND TXNUSER = '0' ");
			queryString.append(" AND TXNTWRECORD = '0' ");
			queryString.append(" AND TXNTWSCHPAY = '0' ");
			queryString.append(" AND TXNFXRECORD = '0' ");
			queryString.append(" AND TXNFXSCHPAY = '0' ");
			queryString.append(" AND TXNGDRECORD = '0' ");
			queryString.append(" AND TXNTRACCSET = '0' ");
//			queryString.append(" AND ADMMAILLOG = '0' "); // ADMMAILLOG--電郵記錄檔--20200316 不即時同步，改用批次
			queryString.append(" AND TXNADDRESSBOOK = '0' ");
			queryString.append(" AND TXNCUSINVATTRIB = '0' ");
			queryString.append(" AND TXNCUSINVATTRHIST = '0' ");
			queryString.append(" AND TXNPHONETOKEN = '0' ");
			
			Query query = getSession().createNativeQuery(queryString.toString());
			List list = query.getResultList();
			
			result = list.isEmpty() ? false : true;
			
		} catch (Exception e) {
			log.error("",e);
		}
		
		return result;
	}
	
	public boolean checkCusData(String cusidn) {
		boolean result = false;
		
		try {
			StringBuffer queryString = new StringBuffer();
			queryString.append(" SELECT * FROM TRNS_NNB_CUSDATA ");
			queryString.append(" WHERE CUSIDN = '" + cusidn + "' ");
			queryString.append(" AND TXNUSER = '0' ");
			queryString.append(" AND TXNTWRECORD = '0' ");
			queryString.append(" AND TXNTWSCHPAY = '0' ");
			queryString.append(" AND TXNFXRECORD = '0' ");
			queryString.append(" AND TXNFXSCHPAY = '0' ");
			queryString.append(" AND TXNGDRECORD = '0' ");
			queryString.append(" AND TXNTRACCSET = '0' ");
//			queryString.append(" AND ADMMAILLOG = '0' "); // ADMMAILLOG--電郵記錄檔--20200316 不即時同步，改用批次
			queryString.append(" AND TXNADDRESSBOOK = '0' ");
			queryString.append(" AND TXNCUSINVATTRIB = '0' ");
			queryString.append(" AND TXNCUSINVATTRHIST = '0' ");
			queryString.append(" AND TXNPHONETOKEN = '0' ");
			queryString.append(" AND NB3USER = '0' ");
			
			Query query = getSession().createNativeQuery(queryString.toString());
			List list = query.getResultList();
			
			result = list.isEmpty() ? false : true;
			
		} catch (Exception e) {
			log.error("",e);
		}
		
		return result;
	}
	
}