package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.NativeQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import fstop.orm.po.TXNBONDDATA;
import fstop.orm.po.TXNFUNDCOMPANY;
import fstop.orm.po.TXNFUNDDATA;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Component
public class TxnBondDataDao extends BaseHibernateDao<TXNBONDDATA, Serializable> {
	Logger log = LoggerFactory.getLogger(this.getClass());

	// 取得BondData ( 非專業投資人用 , 需判斷DBUONLY 及 OBUONLY 及 客戶投資屬性 )
	public List<TXNBONDDATA> getBondDataByFDINVTYPE(String FDINVTYPE) {
		List qresult = null;
		StringBuffer sb = new StringBuffer();
		NativeQuery<TXNBONDDATA> query = null;
		try {
			switch (FDINVTYPE) {
			// 積極 全部可購買 (RR1~RR5) , DBUONLY 及 OBUONLY 為空
			case "1":
				sb.append("SELECT * FROM TXNBONDDATA WHERE RISK !='' and DBUONLY !='Y' and OBUONLY !='Y' ");
				log.debug("getBondDataByFDINVTYPE SQL >> {}",sb.toString());
				query = getSession().createNativeQuery(sb.toString()).addEntity(TXNBONDDATA.class);
				break;
			// 穩健 (RR1~RR4) , DBUONLY 及 OBUONLY 為空
			case "2":
				sb.append("SELECT * FROM TXNBONDDATA WHERE RISK !='RR5' and RISK !='' and DBUONLY !='Y' and OBUONLY !='Y' ");
				log.debug("getBondDataByFDINVTYPE SQL >> {}",sb.toString());
				query = getSession().createNativeQuery(sb.toString()).addEntity(TXNBONDDATA.class);
				break;
			// 保守 (RR1~RR2) , DBUONLY 及 OBUONLY 為空
			case "3":
				sb.append("SELECT * FROM TXNBONDDATA WHERE RISK in ('RR1','RR2') and RISK !='' and DBUONLY !='Y' and OBUONLY !='Y' ");
				log.debug("getBondDataByFDINVTYPE SQL >> {}",sb.toString());
				query = getSession().createNativeQuery(sb.toString()).addEntity(TXNBONDDATA.class);
				break;
			}
			
			// Stored XSS
			qresult = query.getResultList();
			if (null == qresult) {
				qresult = new ArrayList<>();
			}
			
		} catch (Exception e) {
			log.error("TxnBondDataDao getBondDataByFDINVTYPE error >> {}", e);
		}
		return ESAPIUtil.validList(qresult);
	}

	// 取得全部FundData (專業投資人用)
	public List<TXNBONDDATA> getAllBondData() {
		List qresult = null;
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT * FROM TXNBONDDATA where RISK !='' ");
		try {
			NativeQuery<TXNBONDDATA> query = getSession().createNativeQuery(sb.toString()).addEntity(TXNBONDDATA.class);
			// Stored XSS
			qresult = query.getResultList();
			if (null == qresult) {
				qresult = new ArrayList<>();
			}
		} catch (Exception e) {
			log.error("TxnBondDataDao getAllBondData error >> {}", e);
		}
		return ESAPIUtil.validList(qresult);
		// return find("FROM TXNFUNDCOMPANY WHERE FUNDHOUSEMARK='Y' and ORDER BY
		// COUNTRYTYPE DESC ");
	}
	
	public TXNBONDDATA getBondDateById(String BONDCODE) {
		List qresult = null;
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT * FROM TXNBONDDATA where BONDCODE = ? ");
		try {
			NativeQuery<TXNBONDDATA> query = getSession().createNativeQuery(sb.toString()).addEntity(TXNBONDDATA.class);
			query.setParameter(1, BONDCODE);
			
			qresult = query.getResultList();
			
			if (null == qresult) {
				qresult = new ArrayList<>();
			}
		
		}catch (Exception e) {
			log.error("TxnBondDataDao getBondDateById error >> {}", e);
		}
		
		return (TXNBONDDATA) ESAPIUtil.validList(qresult).get(0);
	}
}
