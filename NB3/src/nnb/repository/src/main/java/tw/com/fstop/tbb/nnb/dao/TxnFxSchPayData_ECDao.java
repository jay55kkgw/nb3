package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import fstop.orm.po.TXNFXSCHPAYDATA_EC;
import tw.com.fstop.spring.dao.BaseHibernateDao;

@Component
public class TxnFxSchPayData_ECDao extends BaseHibernateDao<TXNFXSCHPAYDATA_EC, Serializable> {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	/**
	 * 清除使用者資料
	 * @param dpuserid 使用者統編
	 */
	public void deleteByUserId(String fxuserid) {
		
		String queryString = " DELETE fstop.orm.po.TXNFXSCHPAYDATA_EC WHERE FXUSERID = :fxuserid ";
		
		try {
			Query query = getSession().createQuery(queryString);
			query.setParameter("fxuserid", fxuserid).executeUpdate();
			
		} catch (Exception e) {
			log.error("",e);
		}
	}
}