package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import fstop.orm.po.TXNTWSCHPAYDATA;
import fstop.orm.po.TXNTWSCHPAYDATA_PK;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;


@Component
public class TxnTwSchPayDataDao extends BaseHibernateDao<TXNTWSCHPAYDATA, Serializable> {
	private Logger log = LoggerFactory.getLogger(this.getClass());

	/**
	 * 清除使用者資料
	 * @param dpuserid 使用者統編
	 */
	public void deleteByUserId(String dpuserid) {
		
		String queryString = " DELETE fstop.orm.po.TXNTWSCHPAYDATA WHERE DPUSERID = :dpuserid ";
		
		try {
			Query query = getSession().createQuery(queryString);
			query.setParameter("dpuserid", dpuserid).executeUpdate();
			
		} catch (Exception e) {
			log.error("",e);
		}
	}	
	
	public List<TXNTWSCHPAYDATA> getAll(String cusidn,  Map<String, String> reqParam) {
		List result = null;
		try {
			log.debug("TXNTWSCHPAYDATA_Dao >> getAll");
			String dpfdate = reqParam.get("CMSDATE");
			dpfdate = dpfdate.replace("/","");
			log.trace("dpfdate>>>{}",dpfdate);
			String dptdate = reqParam.get("CMEDATE");
			dptdate = dptdate.replace("/","");
			log.trace("dptdate>>>{}",dptdate);
//			String queryString = "SELECT * FROM fstop.orm.po.TXNTWSCHPAYDATA s WHERE DPUSERID  = :cusidn AND DPSCHTXDATE BETWEEN :dpfdate AND :dptdate"
//					+ " AND exists (SELECT DPSCHNO FROM fstop.orm.po.TXNTWSCHPAY where DPSCHNO = s.DPSCHNO and DPTXSTATUS <> '3')";
//			Query<TXNTWSCHPAYDATA> query = getSession().createQuery(queryString);
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT * FROM TXNTWSCHPAYDATA s WHERE s.DPUSERID  = '"+cusidn+"'");
			sb.append(" AND s.DPSCHTXDATE BETWEEN '"+dpfdate+"'"+ " AND '"+ dptdate+"'");
			sb.append(" AND exists (SELECT DPSCHNO FROM TXNTWSCHPAY where DPSCHNO = s.DPSCHNO and DPUSERID = s.DPUSERID and DPTXSTATUS <> '3')");
			String queryString = sb.toString();
			
			Query query = getSession().createNativeQuery(queryString, TXNTWSCHPAYDATA.class);
//			query.setParameter("cusidn", cusidn);
//			query.setParameter("dpfdate", dpfdate);
//			query.setParameter("dptdate", dptdate);
//			result = query.getResultList();
			result = query.list();
			if (null == result)
			{
				result = new ArrayList();
			}
			result = ESAPIUtil.validList(result); 
			log.debug("TXNTWSCHPAYDATADao result size >> {}", result.size());
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("getAll error >> {}",e);
		}
		return result;
	}
	
	/**
	 * 查詢DPTXSTATUS By FGTXSTATUS
	 * @return
	 */
	public List<TXNTWSCHPAYDATA> getByUid(String cusidn , Map<String, String> reqParam) {
		List result = new ArrayList();
		String fgtxstatus = reqParam.get("FGTXSTATUS");			
		log.trace(ESAPIUtil.vaildLog("fgtxstatus >> " + fgtxstatus));
		String dpfdate = reqParam.get("CMSDATE");
		dpfdate = dpfdate.replace("/","");
		log.trace("dpfdate>>>{}",dpfdate);
		String dptdate = reqParam.get("CMEDATE");
		dptdate = dptdate.replace("/","");
		log.trace("dptdate>>>{}",dptdate);
		String queryString = " FROM fstop.orm.po.TXNTWSCHPAYDATA WHERE DPUSERID = :cusidn AND DPTXSTATUS = :fgtxstatus AND DPSCHTXDATE BETWEEN :dpfdate AND :dptdate";
		try {
			log.trace(ESAPIUtil.vaildLog("cusidn>>{}" + cusidn));
			log.trace(ESAPIUtil.vaildLog("FGTXSTATUS >> " + fgtxstatus));
			Query<TXNTWSCHPAYDATA> query = getSession().createQuery(queryString);
			query.setParameter("cusidn", cusidn);
			query.setParameter("fgtxstatus", fgtxstatus);
			query.setParameter("dpfdate", dpfdate);
			query.setParameter("dptdate", dptdate);
			// Stored XSS
			result = query.getResultList();
			if (null == result)
			{
				result = new ArrayList();
			}
			result = ESAPIUtil.validList(result); // 花太多時間在滾LIST迴圈先註解
			log.debug("TXNTWSCHPAYDATADao result size >> {}", result.size());
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("getByUid error >> {}",e);
		}		
		return result;		
	}
	
	//取得下次轉帳日(今天)
	public String getDpschtxToday(String cusidn,String dpschno) {
		List<TXNTWSCHPAYDATA> po = null;
		TXNTWSCHPAYDATA p_date = null;
		TXNTWSCHPAYDATA_PK p_key = null;
		String result = null;
		try {
			log.debug("TXNTWSCHPAYDATA_Dao >> getAll");
			//今天之後
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String today = sdf.format(date);
			log.trace("DPSCHTXDATE_today>>>{}",today);
			String queryString = "FROM fstop.orm.po.TXNTWSCHPAYDATA WHERE DPUSERID  = :cusidn AND "
					+ "DPSCHNO = :dpschno AND DPSCHTXDATE = :dpschtxdate";
			Query<TXNTWSCHPAYDATA> query = getSession().createQuery(queryString);
			query.setParameter("cusidn", cusidn);
			query.setParameter("dpschno", dpschno);
			query.setParameter("dpschtxdate", today);
			po = query.list();
			if(po.size() != 0) {
				p_date = po.get(0);
				p_key = p_date.getPks();
				result = p_key.getDPSCHTXDATE();				
			}else {
				result = "";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("getDpschtxToday error >> {}",e);
		}
		return result;
	}
	
	//取得下次轉帳日(今天之後)
	public String getDpschtxOther(String cusidn,String dpschno) {
		List<TXNTWSCHPAYDATA> po = null;
		TXNTWSCHPAYDATA p_date = null;
		TXNTWSCHPAYDATA_PK p_key = null;
		String result = null;
		try {
			log.debug("TXNTWSCHPAYDATA_Dao >> getAll");
			//今天之後
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String today = sdf.format(date);
			log.trace("DPSCHTXDATE_today>>>{}",today);
			String queryString = "FROM fstop.orm.po.TXNTWSCHPAYDATA WHERE DPUSERID  = :cusidn AND "
					+ "DPSCHNO = :dpschno AND DPSCHTXDATE > :dpschtxdate";
			Query<TXNTWSCHPAYDATA> query = getSession().createQuery(queryString);
			query.setParameter("cusidn", cusidn);
			query.setParameter("dpschno", dpschno);
			query.setParameter("dpschtxdate", today);
			po = query.list();
			if(po.size() != 0) {
				p_date = po.get(0);
				p_key = p_date.getPks();
				result = p_key.getDPSCHTXDATE();				
			}else {
				result = "";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("getDpschtxOther error >> {}",e);
		}
		return result;
	}
	
	//取得下次轉帳日
	public String getDpschtxdate(String cusidn,String dpschno) {
		List<TXNTWSCHPAYDATA> po = null;
		TXNTWSCHPAYDATA p_date = null;
		TXNTWSCHPAYDATA_PK p_key = null;
		String result = null;
		try {
			log.debug("TXNTWSCHPAYDATA_Dao >> getAll");
			//今天之後
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String today = sdf.format(date);
			log.trace("DPSCHTXDATE_today>>>{}",today);
			String queryString = "FROM fstop.orm.po.TXNTWSCHPAYDATA WHERE DPUSERID  = :cusidn AND "
					+ "DPSCHNO = :dpschno AND DPSCHTXDATE >= :dpschtxdate ORDER BY DPSCHTXDATE";
			Query<TXNTWSCHPAYDATA> query = getSession().createQuery(queryString);
			query.setParameter("cusidn", cusidn);
			query.setParameter("dpschno", dpschno);
			query.setParameter("dpschtxdate", today);
			po = query.list();
			if(po.size() != 0) {
				p_date = po.get(0);
				p_key = p_date.getPks();
				result = p_key.getDPSCHTXDATE();				
			}else {
				result = "";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("getDpschtxdate error >> {}",e);
		}
		
		return result;
	}
	
	/**
	 * 查詢DPTXSTATUS By not FGTXSTATUS
	 * @return
	 */
	public List<TXNTWSCHPAYDATA> getByUid(Map<String, String> reqParam,String cusidn) {
		List result = new ArrayList();

		String dpfdate = reqParam.get("CMSDATE");
		dpfdate = dpfdate.replace("/","");
		log.trace("dpfdate>>>{}",dpfdate);
		String dptdate = reqParam.get("CMEDATE");
		dptdate = dptdate.replace("/","");
		log.trace("dptdate>>>{}",dptdate);
		String queryString = " FROM fstop.orm.po.TXNTWSCHPAYDATA WHERE DPUSERID = :cusidn AND DPSCHTXDATE BETWEEN :dpfdate AND :dptdate";
		try {
			log.trace(ESAPIUtil.vaildLog("cusidn>>{}" + cusidn));
			Query<TXNTWSCHPAYDATA> query = getSession().createQuery(queryString);
			query.setParameter("cusidn", cusidn);
			query.setParameter("dpfdate", dpfdate);
			query.setParameter("dptdate", dptdate);
			result = query.getResultList();
			if(null == result) {
				result = new ArrayList();
			}
			result = ESAPIUtil.validList(result);
			log.debug("TXNTWSCHPAYDATADao result size >> {}", result.size());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			log.error("getByUid error >> {}",e);
		}		
		return result;		
	}


}