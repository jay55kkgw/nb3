package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.ADMCURRENCY;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Transactional
@Component
public class AdmCurrencyDao extends BaseHibernateDao<ADMCURRENCY,Serializable>{
	Logger log = LoggerFactory.getLogger(this.getClass());

	/**
	 * 用PK取得該ENTITY
	 */
	public ADMCURRENCY getByPK(String PK){
		log.debug(ESAPIUtil.vaildLog("PK= >>" + PK));
		
		if ("NTD".equals(PK)){
			PK = "TWD";
		}
		
		ADMCURRENCY po = null;
		try{
			po = get(ADMCURRENCY.class,PK);
		}
		catch(Exception e){
			log.error("",e);
		}
		return po;
	}
	/**
	 * 覆寫原本的方法
	 */
	public List<ADMCURRENCY> getAll()
	{
		List qresult = null;
		try
		{
			String queryString = " FROM fstop.orm.po.ADMCURRENCY ORDER BY ADCCYNO";
			Query query = getSession().createQuery(queryString);
			// Stored XSS
			qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			qresult = ESAPIUtil.validList(qresult);

		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("getAll error >> {}",e);
		}
		return qresult;
		
	}
	
	
	
	
}