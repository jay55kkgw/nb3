package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import fstop.orm.po.OLD_TXNCUSINVATTRIB;
import tw.com.fstop.spring.dao.OLD_BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;
 
@Component
public class Old_TxnCusInvAttribDao extends OLD_BaseHibernateDao<OLD_TXNCUSINVATTRIB, Serializable> {
	protected Logger log = LoggerFactory.getLogger(this.getClass());
	
	public List<OLD_TXNCUSINVATTRIB> findByUserId(String fduserid) {
		List qresult = null;
		String queryString = " FROM fstop.orm.po.OLD_TXNCUSINVATTRIB WHERE FDUSERID = :fduserid ";

		try {
			log.trace("fduserid {}", fduserid);
			Query query = getSession().createQuery(queryString);
			query.setParameter("fduserid", fduserid);
			// Stored XSS
			qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			log.debug(ESAPIUtil.vaildLog("qresult>>"+qresult));
			qresult = ESAPIUtil.validList(qresult);
			
		} catch (Exception e) {
			log.error("{}", e);
		}

		return qresult;
	}
}
