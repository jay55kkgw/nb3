package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import fstop.orm.po.ADMBANK;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Component
public class AdmBankDao extends BaseHibernateDao<ADMBANK, Serializable>
{
	Logger log = LoggerFactory.getLogger(this.getClass());

	public List<ADMBANK> getAll()
	{
		List qresult = null;
		try
		{
			String queryString = " FROM fstop.orm.po.ADMBANK ORDER BY ADBANKID";
			Query query = getSession().createQuery(queryString);
			// Stored XSS
			qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			qresult = ESAPIUtil.validList(qresult);

		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			//修改Information Exposure Through an Error Message
//			e.printStackTrace();
			log.error("AdmBankDao error >> {}",e);
		}
		return qresult;
		
	}

	public ADMBANK getbank(String adbankid)
	{
		ADMBANK result = null;
		try
		{
			String queryString = " FROM fstop.orm.po.ADMBANK WHERE ADBANKID = :adbankid ";
			Query<ADMBANK> query = getSession().createQuery(queryString);
			query.setParameter("adbankid", adbankid);
			result = query.uniqueResult();
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getbank error >> {}",e);
		}
		return result;
	}

	/**
	 * 取得銀行的中文名稱
	 * 
	 * @param bankcod 分行代碼
	 * @return
	 */
	public String getBankName(String bankcod)
	{
		String result = bankcod;
		ADMBANK po = null;
		try
		{
			String sql = "FROM fstop.orm.po.ADMBANK WHERE ADBANKID = :adbankid ";
			Query<ADMBANK> query = getSession().createQuery(sql);
			query.setParameter("adbankid", bankcod);
			po = query.uniqueResult();
			result = po.getADBANKNAME();
		}
		catch (Exception e)
		{
			log.error(ESAPIUtil.vaildLog("找不到分行的中文名稱 >> " + bankcod + e));
			result = "";
		}
		return result;
	}
	
	public String getBankEnName(String bankcod)
	{
		String result = bankcod;
		ADMBANK po = null;
		try
		{
			String sql = "FROM fstop.orm.po.ADMBANK WHERE ADBANKID = :adbankid ";
			Query<ADMBANK> query = getSession().createQuery(sql);
			query.setParameter("adbankid", bankcod);
			po = query.uniqueResult();
			result = po.getADBANKENGNAME();
		}
		catch (Exception e)
		{
			log.error(ESAPIUtil.vaildLog("找不到分行的中文名稱 >> " + bankcod + e));
			result = "";
		}
		return result;
	}
	public String getBankChName(String bankcod)
	{
		String result = bankcod;
		ADMBANK po = null;
		try
		{
			String sql = "FROM fstop.orm.po.ADMBANK WHERE ADBANKID = :adbankid ";
			Query<ADMBANK> query = getSession().createQuery(sql);
			query.setParameter("adbankid", bankcod);
			po = query.uniqueResult();
			result = po.getADBANKCHSNAME();
		}
		catch (Exception e)
		{
			log.error(ESAPIUtil.vaildLog("找不到分行的中文名稱 >> " + bankcod + e));
			result = "";
		}
		return result;
	}
}