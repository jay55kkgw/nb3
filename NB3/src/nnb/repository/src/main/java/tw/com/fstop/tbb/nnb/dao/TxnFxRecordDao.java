package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.TXNFXRECORD;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;
@Transactional
@Component
public class TxnFxRecordDao  extends BaseHibernateDao<TXNFXRECORD, Serializable>{
	private Logger log = LoggerFactory.getLogger(getClass());
	
	/**
	 * 清除使用者資料
	 * @param dpuserid 使用者統編
	 */
	public void deleteByUserId(String fxuserid) {
		
		String queryString = " DELETE fstop.orm.po.TXNFXRECORD WHERE FXUSERID = :fxuserid ";
		
		try {
			Query query = getSession().createQuery(queryString);
			query.setParameter("fxuserid", fxuserid).executeUpdate();
			
		} catch (Exception e) {
			log.error("",e);
		}
	}
	
	/*用起訖日查詢交易紀錄
	 * 帳號為空，查詢全部；帳號不空，得帳號資料
	 * 
	 * 
	 * */
	public List<TXNFXRECORD> findByDateRange(String uid, String startDt, String endDt, String acn) {
		logger.trace("!!!!!!!!!!TXNFXRECORD Query START!!!!!!!!!!!!!");
		String commonQuery = "FROM fstop.orm.po.TXNFXRECORD where  FXUSERID = :UID  and FXTXDATE >= :STARTDT and FXTXDATE <= :ENDDT and FXTXSTATUS = '0'";
		String cmsdate = startDt.replace("/", "");
		log.trace("cmsdate>>{}",cmsdate);
		String cmedate = endDt.replace("/", "");
		log.trace("cmedate>>{}",cmedate);
		List<TXNFXRECORD> result=null;
		if (acn.equals("")) {
			Query<TXNFXRECORD> query=getSession().createQuery(commonQuery);
			query.setParameter("UID",uid);
			query.setParameter("STARTDT",cmsdate);
			query.setParameter("ENDDT",cmedate);
			result=query.list();
		}
		else {
			commonQuery=commonQuery + "and FXWDAC = :ACN ";
			Query<TXNFXRECORD> query=getSession().createQuery(commonQuery);
			query.setParameter("UID",uid);
			query.setParameter("STARTDT",cmsdate);
			query.setParameter("ENDDT",cmedate);
			query.setParameter("ACN",acn);
			result=query.list();
		}
		return result;
	}
	//更新重發次數
	public String updataDpremail(String cusidn ,String adtxno) {
		TXNFXRECORD po = null;
		String result = null;
		try {
			log.debug("TXNFXRECORD >> updataDpremail");
			log.debug(ESAPIUtil.vaildLog("CUSIDN >> "+cusidn));
			log.debug(ESAPIUtil.vaildLog("ADTXNO >> "+adtxno));
			String queryString = " FROM fstop.orm.po.TXNFXRECORD WHERE FXUSERID = :cusidn AND ADTXNO = :adtxno";
			Query<TXNFXRECORD> query = getSession().createQuery(queryString);
			query.setParameter("cusidn", cusidn);
			query.setParameter("adtxno", adtxno);
			po = query.uniqueResult();			
			String FXremail = po.getFXREMAIL();
			int FXreMailCount = 0;
			try{
				FXreMailCount = Integer.parseInt(FXremail);
			   FXreMailCount = FXreMailCount + 1;
			   log.trace("FXreMailCount>>{}",FXreMailCount);
			}catch(NumberFormatException e){
			   log.trace("{}","參數非數字");;
			}
			FXremail = String.valueOf(FXreMailCount);
			po.setFXREMAIL(FXremail);
			update(po);
			result = FXremail;
		} catch (Exception e) {
//			e.printStackTrace();
			log.error("取得 Request Info 有誤.", e);
			result = "";
		}
		return result;
	}
	public TXNFXRECORD findByADTXNO(String adtxno) {
		logger.trace("!!!!!!!!!!TXNFXRECORD Query START!!!!!!!!!!!!!");
		String commonQuery = "FROM fstop.orm.po.TXNFXRECORD where  ADTXNO = :adtxno";
		TXNFXRECORD result=null;
		List resultList = new ArrayList();
		try {
			Query<TXNFXRECORD> query=getSession().createQuery(commonQuery);
			query.setParameter("adtxno",adtxno);
			List<TXNFXRECORD> list = query.list();
			if (null == list)
			{
				list = new ArrayList();
			}
			list = ESAPIUtil.validList((List)list);
			if(list.size() > 0)
			{
				result = list.get(0);
			}
			resultList.add(result);
			// Stored XSS
			result = (TXNFXRECORD)resultList.get(0);
		} catch (Exception e) {
			log.error("取得 Request Info 有誤.{}", e);
		}
		
		
		return result;
	}
//
//	@SuppressWarnings( { "unused", "unchecked" })
//	public List<TXNFXRECORD> findSchduleResultByDateRange(String uid, String startDt, String endDt, String status) {
//		List result = new ArrayList();
//		//FXRETXNO = ''表示只取同一交易的最後一筆
//		if(StrUtils.isEmpty(status)) { //全部
//			try {
//				result = findScheduleAllStatusByDateRange(uid, startDt, endDt);
//			}
//			finally {}
//		}
//		else if (status.equals("0")){ //成功
//			try {
//				String qhql = "from TXNFXRECORD where FXUSERID = ? and FXTXDATE >= ? and FXTXDATE <= ? and FXSCHID > 0 and FXTXSTATUS = ? and FXRETXNO = ''";
//				Query query = getQuery(qhql, uid, startDt, endDt, status);
//				result = query.list();
//			}
//			finally{}
//		}
//		else if (status.equals("1")){ //失敗
//			try {
//				String qhql = "from TXNFXRECORD where FXUSERID = ? and FXTXDATE >= ? and FXTXDATE <= ? and FXSCHID > 0 and FXTXSTATUS = ? and FXRETXNO = '' and FXRETXSTATUS <> '1' and FXRETXSTATUS <> '2' and FXRETXSTATUS <> '5'";
//				Query query = getQuery(qhql, uid, startDt, endDt, status);
//				result = query.list();
//			}
//			finally{}
//		}
//		else if (status.equals("2")){ //處理中
//			try {
//				result = findSchedulePendingByDateRange(uid, startDt, endDt, status);
//			}
//			finally{}
//		}
//		return result;
//	}
//	
//	/**
//	 * 找出指定使用者某一期間內的處理中預約交易(如包括查詢日則會加上查詢日尚未執行的預約到期交易)
//	 * @param uid
//	 * @param startDt
//	 * @param endDt
//	 * @param status
//	 * @return
//	 */
//	public List<TXNFXRECORD> findSchedulePendingByDateRange(String uid, String startDt, String endDt, String status) {
//		List result = null;
//		
//		Date today = new Date();
//		String tyyyymmdd = DateTimeUtils.format("yyyyMMdd", today);
//		int iToday = Integer.parseInt(tyyyymmdd);
//		int iStartDate = Integer.parseInt(startDt);
//		int iEndDate = Integer.parseInt(endDt);
//		
//		//判斷查詢期間是否包含今日
//		if (iToday >= iStartDate && iToday <= iEndDate)
//		{//查詢期間有包含今日，要多查TXNFXSCHEDULE檔今日未執行的交易
//			String sql = "" +
//			"select * from TXNFXRECORD where FXUSERID = ? and FXTXDATE >= ? and FXTXDATE <= ? and FXSCHID > 0 and FXTXSTATUS = '1' and FXRETXNO = '' and (FXRETXSTATUS = '1' or FXRETXSTATUS = '2' or FXRETXSTATUS = '5') " + 
//			"union all " + 
//			getSqlForRecordDueTo(today, uid) +
//			"order by FXSCHNO ";
//			
//			System.out.println("@@@　TxnFxRecordDao.findSchedulePendingByDateRange() SQL　== " + sql);
//			System.out.flush();
//			
//			SQLQuery query = getSession().createSQLQuery(sql).addEntity("record", TXNFXRECORD.class);
//			query.setParameter(0, uid);
//			query.setParameter(1, startDt);
//			query.setParameter(2, endDt);
//
//			result = query.list();
//		}
//		else
//		{//查詢期間沒有包含今日，只查TXNFXRECORD檔
//			String qhql = "from TXNFXRECORD where FXUSERID = ? and FXTXDATE >= ? and FXTXDATE <= ? and FXSCHID > 0 and FXTXSTATUS = '1' and FXRETXNO = '' and (FXRETXSTATUS = '1' or FXRETXSTATUS = '2' or FXRETXSTATUS = '5')";
//			Query query = getQuery(qhql, uid, startDt, endDt, status);
//			result = query.list();
//		}
//		
//		return result;
//	}
//	
//	/**
//	 * 找出指定使用者某一期間內的所有已執行預約交易(如包括查詢日則會加上查詢日尚未執行的預約到期交易)
//	 * @param uid
//	 * @param startDt
//	 * @param endDt
//	 * @return
//	 */
//	public List<TXNFXRECORD> findScheduleAllStatusByDateRange(String uid, String startDt, String endDt) {
//		List result = null;
//		
//		Date today = new Date();
//		String tyyyymmdd = DateTimeUtils.format("yyyyMMdd", today);
//		int iToday = Integer.parseInt(tyyyymmdd);
//		int iStartDate = Integer.parseInt(startDt);
//		int iEndDate = Integer.parseInt(endDt);
//		
//		//判斷查詢期間是否包含今日
//		if (iToday >= iStartDate && iToday <= iEndDate)
//		{//查詢期間有包含今日，要多查TXNTWSCHEDULE檔今日未執行的交易
//			String sql = "" +
//			"select * from TXNFXRECORD where FXUSERID = ? and FXTXDATE >= ? and FXTXDATE <= ? and FXSCHID > 0 and FXRETXNO = '' " + 
//			"union all " + 
//			getSqlForRecordDueTo(today, uid) +
//			"order by FXSCHNO ";
//			
//			System.out.println("@@@　TXNFXRECORDDao.findScheduleAllStatusByDateRange() SQL　== " + sql);
//			System.out.flush();
//			
//			SQLQuery query = getSession().createSQLQuery(sql).addEntity("record", TXNFXRECORD.class);
//			query.setParameter(0, uid);
//			query.setParameter(1, startDt);
//			query.setParameter(2, endDt);
//
//			result = query.list();
//		}
//		else
//		{//查詢期間沒有包含今日，只查TXNFXRECORD檔
//			String qhql = "from TXNFXRECORD where FXUSERID = ? and FXTXDATE >= ? and FXTXDATE <= ? and FXSCHID > 0 and FXRETXNO = '' ";
//			Query query = getQuery(qhql, uid, startDt, endDt);
//			result = query.list();
//		}
//		
//		return result;
//	}
//	
//	/**
//	 * 返回指定使用者某一日到期的尚未發送之預約交易SQL字串
//	 * @param currentDate
//	 * @param uid
//	 * @return
//	 */
//	private String getSqlForRecordDueTo(Date currentDate, String uid)
//	{
//		String due = DateTimeUtils.format("yyyyMMdd", currentDate);
//		
//		String sql = "" +
//		"select X.FXSCHNO as ADTXNO, X.ADOPID, X.FXUSERID, '" + due + "' as FXTXDATE, '000000' as FXTXTIME, X.FXWDAC, X.FXSVBH, X.FXSVAC, X.FXWDCURR, X.FXWDAMT, X.FXSVCURR, X.FXSVAMT, X.FXTXMEMO, X.FXTXMAILS, X.FXTXMAILMEMO, X.FXTXCODE, 1 as FXTITAINFO, '' as FXTOTAINFO, X.FXSCHID, '' as FXEFEECCY, '' as FXEFEE, '' as FXEXRATE, '0' as FXREMAIL, '' as FXTXSTATUS, '' as FXEXCODE, '' as FXRETXNO, '0' as FXRETXSTATUS, '' as FXMSGSEQNO, '' as FXMSGCONTENT, '" + due + "' as LASTDATE, '000000' as LASTTIME, '' as FXCERT, X.FXSCHNO " +
//		"from ( " +
//		" SELECT distinct sch.* FROM                                                                                                  " +
//		" 	TXNFXSCHEDULE sch , (" + TxnFxScheduleDao.getSqlForScheduleExcecNextDate(currentDate, uid)+ ") nextexec " +
//		" 	where		                                                                                                         " +
//		" 			nextexec.FXSCHID = sch.FXSCHID and " +
//		"           nextexec.nextexecdate is not null and nextexec.nextexecdate = '" + due + "' " +		
//		"           and ( ( FXTXSTATUS = '0' and  LASTDATE <= '" + due + "' )   " +
//		"					 or ( FXTXSTATUS = '1' and  LASTDATE = '" + due + "'))  " + //不含狀態2是因有可能Record檔已有執行過此筆的資料，避免重複
//		") X ";
//		
//		return sql;
//	}
//
//	/**
//	 * 找出某一日的轉帳到期, 第二次轉帳 => 來源為預約檔(FXTXSTATUS = '1' and LASTDATE = 今天)
//	 * @param currentDate
//	 * @return
//	 */
//	public List<TXNFXSCHEDULE> findScheduleDueToReSend_S(Date currentDate) {
//		String due = DateTimeUtils.format("yyyyMMdd", currentDate);
//		Calendar cal = Calendar.getInstance();
//		cal.setTime(currentDate);
//		int yyyy = cal.get(Calendar.YEAR);
//		int mm = cal.get(Calendar.MONTH) + 1;
//		int dd = cal.get(Calendar.DATE);
//
//		String sql = 
//		"   SELECT distinct {sch.*} FROM                                                                                                  " +
//		" 	TXNFXSCHEDULE sch , (" + TxnFxScheduleDao.getSqlForScheduleExcecNextDate(new Date()) + ") nextexec  " +
//		" 	where	sch.FXSCHID = nextexec.FXSCHID and	" +
//		"           nextexec.nextexecdate is not null and nextexec.nextexecdate = ?                         " +
//		"           and ( sch.FXTXSTATUS = '1' and  sch.LASTDATE = ? ) and sch.LOGINTYPE <> ? ";
//		
//		
//		SQLQuery query = getSession().createSQLQuery(sql).addEntity("sch", TXNFXSCHEDULE.class);
//
//		
//
//		String sql2 = 
//			"   SELECT distinct {sch.*} FROM                                                                                                  " +
//			" 	TXNFXSCHEDULE sch , (" + TxnFxScheduleDao.getSqlForScheduleExcecNextDate(new Date()) + ") nextexec  " +
//			" 	where	sch.FXSCHID = nextexec.FXSCHID and	" +
//			"           nextexec.nextexecdate is not null and nextexec.nextexecdate = '" + due + "' " +
//			"           and ( sch.FXTXSTATUS = '1' and  sch.LASTDATE = '" + due + "' ) ";
//		
//		
//System.out.println("@@@ TxnFxRecordDao.findScheduleDueToReSend() SQL is == "+sql2);
//
//
//		query.setParameter(0, due);
//		query.setParameter(1, due);
//		query.setParameter(2, "MB");
//
//System.out.println("@@@ TxnFxRecordDao.findScheduleDueToReSend() query.list() == "+query.list());
//		
//		return query.list();
//	}
//
//	/**
//	 * 找出某一日的轉帳到期, 第二次轉帳 => 來源為轉帳紀錄檔(FXTXSTATUS = '0' or FXTXSTATUS = '5')
//	 * @param currentDate
//	 * @return
//	 */
//	public List<TXNFXRECORD> findScheduleDueToReSend_R(Date currentDate) {
//		String due = DateTimeUtils.format("yyyyMMdd", currentDate);
//		Calendar cal = Calendar.getInstance();
//		cal.setTime(currentDate);
//		int yyyy = cal.get(Calendar.YEAR);
//		int mm = cal.get(Calendar.MONTH) + 1;
//		int dd = cal.get(Calendar.DATE);
//
//		String sql = "SELECT {fxRec.*} FROM TXNFXRECORD fxRec, " +
//		" ( SELECT distinct rec.ADTXNO as ADTXNO_1 FROM                                                                                                  " +
//		" 	TXNFXSCHEDULE sch , (" + TxnFxScheduleDao.getSqlForScheduleExcecNextDate(new Date()) + ") nextexec, TXNFXRECORD rec  " +
//		" 	where	sch.FXSCHID = nextexec.FXSCHID and	                                                                                                         " +
//		" 			nextexec.FXSCHID = rec.FXSCHID and " +
//		"           nextexec.nextexecdate is not null and nextexec.nextexecdate = ?                         " +
//		"           and (  (sch.FXTXSTATUS = '0' or sch.FXTXSTATUS = '5' ) and  sch.LASTDATE = ? and       " +
//		"                   rec.FXRETXSTATUS = '5' and rec.LASTDATE = ? and rec.FXRETXNO = '' and sch.LOGINTYPE <> ? " +
//		"               ) " +
//		" ) t WHERE fxRec.ADTXNO = t.ADTXNO_1 and fxRec.FXRETXNO = '' " ;	
//		
//		SQLQuery query = getSession().createSQLQuery(sql).addEntity("fxRec", TXNFXRECORD.class);
//
//		
//
//		String sql2 = "SELECT {fxRec.*} FROM TXNFXRECORD fxRec, " +
//		" ( SELECT distinct rec.ADTXNO as ADTXNO_1 FROM                                                                                                  " +
//		" 	TXNFXSCHEDULE sch , (" + TxnFxScheduleDao.getSqlForScheduleExcecNextDate(new Date()) + ") nextexec, TXNFXRECORD rec  " +
//		" 	where	sch.FXSCHID = nextexec.FXSCHID and	                                                                                                         " +
//		" 			nextexec.FXSCHID = rec.FXSCHID and " +
//		"           nextexec.nextexecdate is not null and nextexec.nextexecdate = '" + due + "'                         " +
//		"           and (    (sch.FXTXSTATUS = '0' or sch.FXTXSTATUS = '5' ) and  sch.LASTDATE = '" + due + "' and       " +
//		"                     rec.FXRETXSTATUS = '5' and rec.LASTDATE = '" + due + "' and rec.FXRETXNO = '' " +
//		"               ) " +
//		" ) t WHERE fxRec.ADTXNO = t.ADTXNO_1" ;
//		
//		
//System.out.println("@@@ TxnFxRecordDao.findScheduleDueToReSend() SQL is == "+sql2);
//
//
//		query.setParameter(0, due);
//		query.setParameter(1, due);
//		query.setParameter(2, due);
//		query.setParameter(3, "MB");
//
//System.out.println("@@@ TxnFxRecordDao.findScheduleDueToReSend() query.list() == "+query.list());
//		
//		return query.list();
//	}
//	
//	@Transactional(propagation = Propagation.REQUIRES_NEW)
//	public void writeTxnRecord(TXNREQINFO info ,TXNFXRECORD record) {
//		BOLifeMoniter.setValue("__TXNRECORD", record);
//		if(info != null) {
//			BOLifeMoniter.setValue("__TITAINFO", info);
//			save(info);
//			record.setFXTITAINFO(info.getREQID());
//		}
//		save( record);
//
//	}
//
//	@Transactional(propagation = Propagation.REQUIRES_NEW)
//	public void rewriteTxnRecord(TXNFXRECORD record) {
//		BOLifeMoniter.setValue("__TXNRECORD", record);
//		save( record);
//	}
//
//	public List<TXNFXRECORD> findOnlineResendStatus(String lastdate, String[] retxstatus) {
//		return findResendStatus("online", lastdate, retxstatus);
//	}
//	public List<TXNFXRECORD> findScheduleResendStatus(String lastdate, String[] retxstatus) {
//		return findResendStatus("schedule", lastdate, retxstatus);
//	}
//	public List<TXNFXRECORD> findResendStatus(String func, String lastdate, String[] retxstatus) {
//		String restatusCondition = "";
//		if(retxstatus == null || retxstatus.length == 0)
//			return new ArrayList();
//		else if(retxstatus.length == 1)  {
//			if("*".equals(retxstatus[0]))
//				restatusCondition = "FXRETXSTATUS <> '0' ";
//			else
//				restatusCondition = "FXRETXSTATUS = '" + retxstatus[0] + "' ";
//		}
//		else if(retxstatus.length > 1)
//			restatusCondition = "FXRETXSTATUS in ('" + StrUtils.implode("','", retxstatus) + "') ";
//
//		String schidCondition = "";
//		if("online".equalsIgnoreCase(func))
//			schidCondition = " FXSCHID = 0 ";
//		else if("schedule".equals(func))
//			schidCondition = " FXSCHID > 0 ";
//
//		/*
//		Query query = getQuery("FROM TXNFXRECORD " +
//				" WHERE " + schidCondition + " and FXTXSTATUS = '1' and  " + restatusCondition +
//				" and LASTDATE = ? and FXRETXNO = '' " +
//				" ORDER BY LASTDATE DESC, LASTTIME DESC ", lastdate);
//		*/
//
//		Query query = getQuery("FROM TXNFXRECORD " +
//				" WHERE " + schidCondition + " and FXTXSTATUS = '1' and  " + restatusCondition +
//				" and LASTDATE = ? " +   //and FXRETXNO = '' " +
//				" ORDER BY LASTDATE DESC, LASTTIME DESC ", lastdate);
//
//System.out.println("@@@ B402 SQL == "+
//		"FROM TXNFXRECORD " +
//		" WHERE " + schidCondition + " and FXTXSTATUS = '1' and  " + restatusCondition +
//		" and LASTDATE = '" + lastdate + "' " +    // and FXRETXNO = '' " +
//		" ORDER BY LASTDATE DESC, LASTTIME DESC ");
//
//		return query.list();
//	}
//
//	/**
//	 * 找到重送之後寫的那筆 TXNFXRECORD
//	 * @param adtxno
//	 * @return
//	 */
//	public TXNFXRECORD findResendRecord(String adtxno) {
//		return this.findUniqueBy("FXRETXNO", adtxno);
//	}
//
//	public void updateStatus(TXNFXRECORD record, String status) {
//		try {
//			Date d = new Date();
//			record.setLASTDATE(DateTimeUtils.format("yyyyMMdd", d));
//			record.setLASTTIME(DateTimeUtils.format("HHmmss", d));
//			record.setFXRETXSTATUS(status);
//
//			save(record);
//		}
//		catch(Exception e) {
//
//		}
//		finally {
//
//		}
//	}
//
	/**
	 * 供<線上解款資料清單>查詢 ==> 是否有列為"待重送"之匯入匯款資料
	 * 
	 * @param refNo
	 * @return
	 */
	public boolean findByRefNo(String refNo) {
		TXNFXRECORD po = null;
		boolean falseFlae = false; 
		try {
			String queryString = " TXNFXRECORD where FXTXMEMO= :refNo and FXRETXSTATUS='5'  ";
			Query<TXNFXRECORD> query = getSession().createQuery(queryString);
			query.setParameter("refNo", refNo);
			po = query.uniqueResult();
			
			if (po==null) {
				falseFlae = false;
			}
			else {
				falseFlae = true;
			}
			
		}catch (Exception e) {
			log.debug("findByRefNo Exception>>>>>>>>>>>>>" ,e);
		}
		
		return falseFlae;
	}	
	/**
	 * 用日期查詢外幣轉帳結果 
	 *  
	 * @param
	 * @return
	 */
	public List<TXNFXRECORD> findAllByDateRange(Map<String, String> reqParam, Boolean flag) {
		List result = null;
		Query<TXNFXRECORD> query;
		String sdate = reqParam.get("sdate");
		String edate = reqParam.get("edate");
		String cusidn = reqParam.get("cusidn");
		String adopid = reqParam.get("adopid");
		StringBuilder queryString = new StringBuilder();
		queryString.append(
				" FROM fstop.orm.po.TXNFXRECORD WHERE FXUSERID = :cusidn  AND fxtxdate BETWEEN :sdate AND :edate");
		try {			
			if (flag == false) {
				queryString.append("  AND ADOPID = :adopid");
				query = getSession().createQuery(queryString.toString());
				query.setParameter("adopid", adopid);
			} else {
				query = getSession().createQuery(queryString.toString());
			}
			query.setParameter("cusidn", cusidn);
			query.setParameter("sdate", sdate);
			query.setParameter("edate", edate);

			result = query.getResultList();
			if (null == result)		
			{
				result = new ArrayList();
			}
			log.debug("TXNFXSCHPAYDao result size >> {}", result.size());
			result = ESAPIUtil.validList(result);
		} catch (Exception e) {
			log.error("getByUid error >> {}", e);
		}
		return result;
	}
	
	
	/**
	 * 取得外幣交易編號清單 by user
	 * 
	 * @return
	 */
	public List<String> getAdtxNoByUser(String cusidn) {
		List<String> qresult = null;
		try {
			
			StringBuffer bfs = new StringBuffer();
			bfs.append(" SELECT ADTXNO FROM TXNFXRECORD ");
			bfs.append(" WHERE FXUSERID = '" + cusidn + "' ");

			Query query = getSession().createNativeQuery(bfs.toString());
			// Stored XSS
			qresult = query.getResultList();
			if (null == qresult) {
				qresult = new ArrayList<String>();
			}
			// qresult = ESAPIUtil.validList(qresult); // 花太多時間在滾LIST迴圈先註解

		} catch (Exception e) {
			log.error("", e);
		}
		return qresult;

	}
	
	
}
