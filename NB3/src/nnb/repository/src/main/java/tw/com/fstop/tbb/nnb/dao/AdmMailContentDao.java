package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.ADMMAILCONTENT;
import tw.com.fstop.spring.dao.BaseHibernateDao;


@Transactional
@Component
public class AdmMailContentDao extends BaseHibernateDao<ADMMAILCONTENT, Serializable>
{
	protected Logger logger = Logger.getLogger(getClass());
	
	
}
