package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import fstop.orm.po.OLD_TXNRECMAILLOGRELATION;
import fstop.orm.po.TXNRECMAILLOGRELATION;
import tw.com.fstop.spring.dao.OLD_BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;
 
@Component
public class Old_TxnRecMailLogRelationDao extends OLD_BaseHibernateDao<OLD_TXNRECMAILLOGRELATION, Serializable> {
	protected Logger log = LoggerFactory.getLogger(this.getClass());
	
	public List<OLD_TXNRECMAILLOGRELATION> findByUserId(String adtxno) {
		List qresult = null;
		String queryString = " FROM fstop.orm.po.OLD_TXNRECMAILLOGRELATION WHERE ADTXNO = :adtxno ";

		try {
			log.trace("adtxno {}", adtxno);
			Query query = getSession().createQuery(queryString);
			query.setParameter("adtxno", adtxno);
			// Stored XSS
			qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			log.debug(ESAPIUtil.vaildLog("qresult>>"+qresult));
			qresult = ESAPIUtil.validList(qresult);
			
		} catch (Exception e) {
			log.error("{}", e);
		}

		return qresult;
	}
	
	public List<TXNRECMAILLOGRELATION> findByMailId(Long admaillogid) {
		List qresult = null;
		String queryString = "SELECT RELID, REC_TYPE, ADTXNO, ADMAILLOGID, LASTDATE, LASTTIME, '" + admaillogid + "' AS CUSIDN FROM TXNRECMAILLOGRELATION "
				+ "WHERE ADMAILLOGID = " + admaillogid;
		try {
			log.trace("admaillogid {}", admaillogid);
			Query query = getSession().createNativeQuery(queryString, TXNRECMAILLOGRELATION.class);
			// Stored XSS
			qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			log.debug(ESAPIUtil.vaildLog("qresult>>"+qresult));
			qresult = ESAPIUtil.validList(qresult);
			
		} catch (Exception e) {
			log.error("{}", e);
		}

		return qresult;
	}
	
}
