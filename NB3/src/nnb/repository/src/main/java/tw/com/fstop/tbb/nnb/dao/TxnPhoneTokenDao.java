package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.TXNPHONETOKEN;
import fstop.orm.po.TXNTWRECORD;
import fstop.orm.po.TXNTWSCHPAY;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Transactional
@Component
public class TxnPhoneTokenDao extends BaseHibernateDao<TXNPHONETOKEN, Serializable> {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	/**
	 * 清除使用者資料
	 * @param dpuserid 使用者統編
	 */
	public void deleteByUserId(String dpuserid) {
		
		String queryString = " DELETE fstop.orm.po.TXNPHONETOKEN WHERE DPUSERID = :dpuserid ";
		
		try {
			Query query = getSession().createQuery(queryString);
			query.setParameter("dpuserid", dpuserid).executeUpdate();
			
		} catch (Exception e) {
			log.error("",e);
		}
	}
	/**
	 * 萬用查詢
	 * 
	 * 
	 * */
	public List<TXNPHONETOKEN> getByEqual(Map<String,String> map){
		List result = null;
		try {
			StringBuffer queryString = new StringBuffer();
			queryString.append("FROM fstop.orm.po.TXNPHONETOKEN WHERE 1=1 ");
			//取得PO所有欄位
			Field[] fs = TXNPHONETOKEN.class.getDeclaredFields();
			List<String> list = new ArrayList<String>();
 			for(Field f : fs) {
 				if(!("serialVersionUID".equals(f.getName()))) {
 					if(map.get(f.getName().toLowerCase()) != null) {
 						//將拿出來的欄位當作查詢條件
 						queryString.append(" AND "+ f.getName()+"=:"+f.getName().toLowerCase());
 						list.add(f.getName());
 					}
 				}
 			}
 			Query<TXNPHONETOKEN> query = getSession().createQuery(queryString.toString());
 			//將條件的值代入
 			for(String str:list) {
 				query.setParameter(str.toLowerCase(), map.get(str.toLowerCase()));
 			}

 			result = query.getResultList();
			if (null == result)
			{
				result = new ArrayList<TXNPHONETOKEN>();
			}
			result = ESAPIUtil.validList(result); 
			log.debug("TXNPHONETOKENDao result size >> {}", result.size());
		}catch(Exception e){
			log.error("getByDpuserid error >> {}", e);
		}
		return result;
	}

	/**
	 * 以統編為條件進行查詢
	 * 
	 * @return
	 */
	public List<TXNPHONETOKEN> getByDpuserid(String dpuserid) {
		List result = null;
		String cusidn = dpuserid;
		try {
			log.debug(ESAPIUtil.vaildLog("TXNPHONETOKEN_Dao >> getByDpuserid>{}"+ dpuserid));
			log.trace(ESAPIUtil.vaildLog("dpuserid>>>{}" + dpuserid));
			String queryString = "FROM fstop.orm.po.TXNPHONETOKEN WHERE 1=1 AND DPUSERID =:cusidn ";
			Query<TXNPHONETOKEN> query = getSession().createQuery(queryString);
			query.setParameter("cusidn",cusidn);
			result = query.getResultList();
			if (null == result)
			{
				result = new ArrayList<TXNPHONETOKEN>();
			}
			result = ESAPIUtil.validList(result); 
			log.debug("TXNPHONETOKENDao result size >> {}", result.size());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("getByDpuserid error >> {}", e);
		}
		return result;
	}
	/**
	 * 以phonetoken為條件進行查詢
	 * 
	 * @return
	 */
	public List<TXNPHONETOKEN> getByPhonetoken(String phonetoken) {
		List result = null;
		try {
			log.debug(ESAPIUtil.vaildLog("TXNPHONETOKEN_Dao >> getByPhonetoken>{}"+ phonetoken));
			log.trace(ESAPIUtil.vaildLog("dpuserid>>>{}" + phonetoken));
			String queryString = "FROM fstop.orm.po.TXNPHONETOKEN WHERE 1=1 AND PHONETOKEN =:phonetoken ";
			Query<TXNPHONETOKEN> query = getSession().createQuery(queryString);
			query.setParameter("phonetoken",phonetoken);
			result = query.getResultList();
			if (null == result)
			{
				result = new ArrayList<TXNPHONETOKEN>();
			}
			result = ESAPIUtil.validList(result); 
			log.debug("TXNPHONETOKENDao result size >> {}", result.size());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("getByDpuserid error >> {}", e);
		}
		return result;
	}


}