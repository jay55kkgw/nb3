package tw.com.fstop.tbb.nnb.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import fstop.orm.po.TXNCARDAPPLY;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Component
public class TxnCardApplyDao extends BaseHibernateDao<TXNCARDAPPLY, String> {
	private Logger log = LoggerFactory.getLogger(this.getClass());

//	@SuppressWarnings( { "unused", "unchecked" })
//	public List<TXNCARDAPPLY> findByLastDate(String lastDate) {
//		
//		String sql = "from TXNCARDAPPLY where LASTDATE <= ? AND STATUS='0' order by RCVNO ";
//		
//		Query query = getQuery(sql, lastDate);
//		
//		return query.getResultList();
//	}
//	@SuppressWarnings( { "unused", "unchecked" })
//	public List<TXNCARDAPPLY> findByLastDate1(String lastDate) {
//		
//		String sql = "from TXNCARDAPPLY where LASTDATE = ? order by RCVNO ";
//		
//		Query query = getQuery(sql, lastDate);		
//		
//		return query.getResultList();
//	}	
//	@SuppressWarnings( { "unused", "unchecked" })
//	public List<TXNCARDAPPLY> findByUID(String UID) {
//		
//		String sql = "from TXNCARDAPPLY where CPRIMID = ? order by CPRIMID ";
//		
//		Query query = getQuery(sql, UID);		
//		
//		return query.getResultList();
//	}	
	/**
	 * 取得某一日的申請信用卡筆數
	 * @param currentDate
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public int countCreditRecord(Date currentDate) {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		
		String due = sdf.format(currentDate);
		log.debug("due={}",due);
		String sql = "from fstop.orm.po.TXNCARDAPPLY where LASTDATE = :LASTDATE order by CPRIMID ";
		
		Query<TXNCARDAPPLY> query = getSession().createQuery(sql);
		query.setParameter("LASTDATE",due);
		List<TXNCARDAPPLY> result = query.list();
		
		return result.size();
	}	
	
	/**
	 * Avoid Privacy Violation rename from countCreditRecord
	 * 取得某一日的申請信用卡筆數
	 * @param currentDate
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public int countCdRecord(Date currentDate) {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		
		String due = sdf.format(currentDate);
		log.debug("due={}",due);
		String sql = "from fstop.orm.po.TXNCARDAPPLY where LASTDATE = :LASTDATE order by CPRIMID ";
		
		Query<TXNCARDAPPLY> query = getSession().createQuery(sql);
		query.setParameter("LASTDATE",due);
		List<TXNCARDAPPLY> result = query.list();
		
		return result.size();
	}	
	
	/**
	 * 查詢某一筆案件編號
	 * @return
	 */
	public List<TXNCARDAPPLY> findByRCVNO(String rcvno) 
	{
		List<TXNCARDAPPLY> result = new ArrayList<>();
		String sql = " FROM fstop.orm.po.TXNCARDAPPLY WHERE RCVNO = :RCVNO";
		Query<TXNCARDAPPLY> query = getSession().createQuery(sql);
		query.setParameter("RCVNO", rcvno);
		result = query.list();
		return result;
	}
	
	
	/**
	 * 查詢客戶申請案件
	 * @return
	 */
	public List<TXNCARDAPPLY> findByCUSID(String cusidn) 
	{
		List result = new ArrayList<>();
		String sql = " FROM fstop.orm.po.TXNCARDAPPLY WHERE CPRIMID = :CPRIMID ORDER BY RCVNO";
		Query<TXNCARDAPPLY> query = getSession().createQuery(sql);
		query.setParameter("CPRIMID", cusidn);
		//修改 Potential Stored XSS
		result = query.list();
		if (null == result)
		{
			result = new ArrayList();
		}
		result = ESAPIUtil.validList(result);
		return result;
	}
	
	
	/**
	 * 變更客戶申請狀態 1:審核中 2:補件 3:不核准 4:核准
	 * @return
	 */
//	@SuppressWarnings({ "unused", "unchecked" })
//	public TXNCARDAPPLY setStatus(String RCVNO,String STATUS) {
//		
//		//String status = "1";
//		int isUserExist = -1;
//		TXNCARDAPPLY credit = null;
//		try {
//			credit = findById(StrUtils.trim(RCVNO));
//			System.out.println("update RCVNO:" + credit.getRCVNO());
//			isUserExist = ((credit == null) ? 0 : 1);
//		}
//		catch(Exception e) {
//			isUserExist = 0;
//		}
//		if(isUserExist == 1 ) {
//			
//			credit.setSTATUS(STATUS);
//			save(credit);
//			return credit;
//		}
//
//		return null;
//	}
//	@SuppressWarnings( { "unused", "unchecked" })
//	public List<String> findByLastDateRange(String startDate, String endDate) {
//		
//		String sql = "SELECT distinct LASTDATE from TXNCARDAPPLY where LASTDATE >= ? and LASTDATE <= ? order by LASTDATE ";
//		Query query = getQuery(sql, startDate, endDate);			
//		
//		return query.getResultList();
//	}
	
//	public long getTxnCount(boolean isSuccess) {
//		List<String> params = new ArrayList();
//		String sdate = DateTimeUtils.getPrevMonthFirstDay();
//
//		String stime = "000000";
//		String edate = DateTimeUtils.getPrevMonthLastDay();
//
//		String etime = "235959";
//		params.add(sdate);
//		params.add(stime);
//		params.add(edate);
//		params.add(etime);
//
//		String[] values = new String[params.size()];
//		params.toArray(values);
//
//		String SQL = "SELECT COUNT(*) FROM TXNCARDAPPLY WHERE LASTDATE >= ? AND LASTTIME >= ? AND LASTDATE <= ? AND LASTTIME <= ?";
//		if (isSuccess) {
//			SQL = SQL + " AND STATUS = '4' ";
//		}
//		return ((Long) find(SQL, values).get(0)).longValue();
//	}
	/*
	 * ADD 20180927
	 * 舊戶取得申請信用卡的歸戶行
	 * 
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String,String>> findBranchNo(String UID) {
		
		List<Map<String,String>> result = new ArrayList<Map<String,String>>();

		String sql = "from fstop.orm.po.TXNCARDAPPLY where CPRIMID = :CPRIMID AND BRNO !='' order by CPRIMID ";
		Query<TXNCARDAPPLY> query = getSession().createQuery(sql);
		query.setParameter("CPRIMID",UID);
		List<TXNCARDAPPLY> TXNCARDAPPLYList = query.list();
		
		Map<String,String> map;
		String BRNO;
		for(int x=0;x<TXNCARDAPPLYList.size();x++){
			BRNO = TXNCARDAPPLYList.get(x).getBRNO();
			log.debug(ESAPIUtil.vaildLog("BRNO="+BRNO));
			
			map = new HashMap<String,String>();
			map.put("BRNO",BRNO);
			
			result.add(map);
		}
		return result;
	}
}