package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import fstop.orm.po.OLD_TXNLOG;
import tw.com.fstop.spring.dao.OLD_BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;
 
@Component
public class Old_TxnLogDao extends OLD_BaseHibernateDao<OLD_TXNLOG, Serializable> {
	protected Logger log = LoggerFactory.getLogger(this.getClass());
	
	public List<OLD_TXNLOG> findByUserId(String aduserid) {
		List qresult = null;
		String queryString = " FROM fstop.orm.po.OLD_TXNLOG WHERE ADUSERID = :aduserid ";

		try {
			log.trace("aduserid {}", aduserid);
			Query query = getSession().createQuery(queryString);
			query.setParameter("aduserid", aduserid);
			// Stored XSS
			qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			log.debug(ESAPIUtil.vaildLog("qresult>>"+qresult));
			qresult = ESAPIUtil.validList(qresult);
			
		} catch (Exception e) {
			log.error("{}", e);
		}

		return qresult;
	}
}
