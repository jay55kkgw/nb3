package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import fstop.orm.po.OLD_TXNPHONETOKEN;
import fstop.orm.po.OLD_TXNTWRECORD;
import tw.com.fstop.spring.dao.OLD_BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Component
public class Old_TxnPhoneTokenDao extends OLD_BaseHibernateDao<OLD_TXNPHONETOKEN, Serializable>{
	protected Logger log = LoggerFactory.getLogger(this.getClass());
	
	public List<OLD_TXNPHONETOKEN> findByUserId(String dpuserid) {
		List qresult = null;
		String queryString = " FROM fstop.orm.po.OLD_TXNPHONETOKEN WHERE DPUSERID = :dpuserid ";

		try {
			log.trace("dpuserid: {}", dpuserid);
			Query query = getSession().createQuery(queryString);
			query.setParameter("dpuserid", dpuserid);
			// Stored XSS
			qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			log.debug(ESAPIUtil.vaildLog("qresult>>"+qresult));
			qresult = ESAPIUtil.validList(qresult);
			
		} catch (Exception e) {
			log.error("{}", e);
		}

		return qresult;
	}

}