package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import fstop.orm.po.ADMMAILLOG;
import fstop.orm.po.OLD_ADMMAILLOG;
import tw.com.fstop.spring.dao.OLD_BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;
 
@Component
public class Old_AdmMailLogDao extends OLD_BaseHibernateDao<OLD_ADMMAILLOG, Serializable> {
	protected Logger log = LoggerFactory.getLogger(this.getClass());
	
	public List<OLD_ADMMAILLOG> findByUserId(String aduserid) {
		List qresult = null;
		String queryString = " FROM fstop.orm.po.OLD_ADMMAILLOG WHERE ADUSERID = :aduserid ";

		try {
			log.trace("aduserid {}", aduserid);
			Query query = getSession().createQuery(queryString);
			query.setParameter("aduserid", aduserid);
			// Stored XSS
			qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			log.debug(ESAPIUtil.vaildLog("qresult>>"+qresult));
			qresult = ESAPIUtil.validList(qresult);
			
		} catch (Exception e) {
			log.error("{}", e);
		}

		return qresult;
	}
	
	public List<ADMMAILLOG> findMailLogById(String aduserid) {
		List qresult = null;
		String queryString = "SELECT A.ADMAILLOGID, A.ADBATCHNO, A.ADUSERID, "
				+ "A.ADUSERNAME , A.ADMAILACNO, A.ADACNO, A.ADSENDTIME, A.ADSENDSTATUS, "
				+ "A.ADSENDTYPE, B.ADSUBJECT ADMSUBJECT, B.ADMAILCONTENT "
				+ "FROM ADMMAILLOG A LEFT JOIN ADMMAILCONTENT B ON A.ADMAILID=B.ADMAILID "
				+ "WHERE 1=0";
		// 20200316 不即時同步，改用批次
//				+ "WHERE  A.ADUSERID = '" + aduserid + "' ";
		try {
			log.trace("aduserid {}", aduserid);
			Query query = getSession().createNativeQuery(queryString, ADMMAILLOG.class);
			// Stored XSS
			qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			log.debug(ESAPIUtil.vaildLog("qresult>>"+qresult));
			qresult = ESAPIUtil.validList(qresult);
			
		} catch (Exception e) {
			log.error("{}", e);
		}

		return qresult;
	}
	
}
