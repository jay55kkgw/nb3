package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.TXNRISKUSR;
import tw.com.fstop.spring.dao.BaseHibernateDao;

@Transactional
@Component
public class TxnRiskUsrDao extends BaseHibernateDao<TXNRISKUSR, Serializable>{
	private Logger log = LoggerFactory.getLogger(this.getClass());
	
public String getRISKLOG(String cusidn) {
	List result=null;
	try {
	StringBuffer queryString = new StringBuffer();
	queryString.append("SELECT RISKLOG FROM fstop.orm.po.TXNRISKUSR WHERE USERID = :cusidn");
	Query<TXNRISKUSR> query = getSession().createQuery(queryString.toString());
	query.setParameter("cusidn",cusidn);
	result = query.getResultList();
	}catch(Exception e) {
		// TODO Auto-generated catch block
		// e.printStackTrace();
		log.error("cancelAcn error >> {}", e);
	}
	if (null != result && result.size() > 0) {
		return result.get(0).toString();
	} else {
		return "Y";
	}
}

//	@Transactional(propagation = Propagation.REQUIRES_NEW)
//	public int updateRISKLOG(String RISKLOG, String USERID) {
//		Date d = new Date();
//		String date = DateTimeUtils.getCDateShort(d);
//		String time = DateTimeUtils.getTimeShort(d);
//		
//		SQLQuery add1=this.getSession().createSQLQuery("UPDATE TXNRISKUSR SET RISKLOG = ? , LASTDATE = ?, LASTTIME= ? WHERE USERID = ?");
//
//		add1.setString(0, RISKLOG);
//		add1.setString(1, date); 
//		add1.setString(2, time);
//		add1.setString(3, USERID);
//		int r2 = add1.executeUpdate();
//		return(r2);
//	}
}
