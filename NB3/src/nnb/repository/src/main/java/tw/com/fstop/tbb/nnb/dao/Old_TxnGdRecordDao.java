package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import fstop.orm.po.OLD_TXNGDRECORD;
import tw.com.fstop.spring.dao.OLD_BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;
 
@Component
public class Old_TxnGdRecordDao extends OLD_BaseHibernateDao<OLD_TXNGDRECORD, Serializable> {
	protected Logger log = LoggerFactory.getLogger(this.getClass());
	
	public List<OLD_TXNGDRECORD> findByUserId(String gduserid) {
		List qresult = null;
		String queryString = " FROM fstop.orm.po.OLD_TXNGDRECORD WHERE GDUSERID = :gduserid ";

		try {
			log.trace("gduserid {}", gduserid);
			Query query = getSession().createQuery(queryString);
			query.setParameter("gduserid", gduserid);
			// Stored XSS
			qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			log.debug(ESAPIUtil.vaildLog("qresult>>"+qresult));
			qresult = ESAPIUtil.validList(qresult);
			
		} catch (Exception e) {
			log.error("{}", e);
		}

		return qresult;
	}
}
