package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import fstop.orm.po.TXNTWSCHPAY;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Transactional
@Component
public class TxnTwSchPayDao extends BaseHibernateDao<TXNTWSCHPAY, Serializable> {
	private Logger log = LoggerFactory.getLogger(this.getClass());

	/**
	 * 清除使用者資料
	 * @param dpuserid 使用者統編
	 */
	public void deleteByUserId(String dpuserid) {
		
		String queryString = " DELETE fstop.orm.po.TXNTWSCHPAY WHERE DPUSERID = :dpuserid ";
		
		try {
			Query query = getSession().createQuery(queryString);
			query.setParameter("dpuserid", dpuserid).executeUpdate();
			
		} catch (Exception e) {
			log.error("",e);
		}
	}
	
	/**
	 * 取狀態的欄位(ALL)
	 * 
	 * @return
	 */
	public List<TXNTWSCHPAY> getDptxstatus(String cusidn) {
		List result = null;
		try {
			log.debug(ESAPIUtil.vaildLog("TXNTWSCHPAY_Dao >> getDptxstatus>{}"+ cusidn));
			Date d = new Date();
			DateFormat dateformat;
			dateformat = new SimpleDateFormat("yyyyMMdd");
			String date = dateformat.format(d);
			log.trace("today>>>{}", dateformat.format(d));

			String queryString = " FROM fstop.orm.po.TXNTWSCHPAY "
					+ "WHERE DPUSERID = :cusidn AND (DPTXSTATUS = '0' OR DPTXSTATUS = '3') AND DPTDATE >= :date ORDER BY DPFDATE";
			Query<TXNTWSCHPAY> query = getSession().createQuery(queryString);
			query.setParameter("cusidn", cusidn);
			query.setParameter("date", date);
			result = query.getResultList();
			if (null == result)
			{
				result = new ArrayList();
			}
			result = ESAPIUtil.validList(result); 
			log.debug("TXNTWSCHPAYDao result size >> {}", result.size());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("getDptxstatus error >> {}", e);
		}
		return result;
	}
	
	/**
	 * 取狀態的欄位(ALL)
	 * 
	 * @return
	 */
	public List<TXNTWSCHPAY> getDptxstatus_2rd(String cusidn) {
		List result = null;
		try {
			log.debug(ESAPIUtil.vaildLog("TXNTWSCHPAY_Dao >> getDptxstatus>{}"+ cusidn));
			Date d = new Date();
			DateFormat dateformat;
			dateformat = new SimpleDateFormat("yyyyMMdd");
			String date = dateformat.format(d);
			log.trace("today>>>{}", dateformat.format(d));

			String queryString = " FROM fstop.orm.po.TXNTWSCHPAY "
					+ "WHERE DPUSERID = :cusidn AND DPTXSTATUS = '0' AND DPTDATE >= :date ORDER BY DPFDATE";
			Query<TXNTWSCHPAY> query = getSession().createQuery(queryString);
			query.setParameter("cusidn", cusidn);
			query.setParameter("date", date);
			result = query.getResultList();
			if (null == result)
			{
				result = new ArrayList();
			}
			result = ESAPIUtil.validList(result); 
			log.debug("TXNTWSCHPAYDao result size >> {}", result.size());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("getDptxstatus error >> {}", e);
		}
		return result;
	}

	
	
	/**
	 * 取狀態的欄位(轉入帳號)
	 * 
	 * @return
	 */
	public List<TXNTWSCHPAY> getAcnDptxstatus(String cusidn, String dpsvac) {
		List result = null;
		try {
			log.debug("TXNTWSCHPAY_Dao >> getDptxstatus>{}", cusidn);
			String queryString = " FROM fstop.orm.po.TXNTWSCHPAY "
					+ "WHERE DPUSERID = :cusidn AND DPSVAC = :dpsvac AND (DPTXSTATUS = '0' OR DPTXSTATUS = '1') ORDER BY DPSVAC";
			Query<TXNTWSCHPAY> query = getSession().createQuery(queryString);
			query.setParameter("cusidn", cusidn);
			query.setParameter("dpsvac", dpsvac);
			result = query.getResultList();
			if(null == result) {
				result=new ArrayList();
			}
			result = ESAPIUtil.validList(result); 
			log.debug("TXNTWSCHPAYDao result size >> {}", result.size());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("getAcnDptxstatus error >> {}", e);
		}
		return result;
	}

	/**
	 * 取消轉入帳號相關預約
	 * 
	 * @return
	 */
	public List<TXNTWSCHPAY> cancelAcn(String cusidn, String dpsvac, String bank) {
		List<TXNTWSCHPAY> result = null;
		try {
			log.debug("TXNTWSCHPAY_Dao >> getAcnCancel>>{}", cusidn);
			String queryString = " FROM fstop.orm.po.TXNTWSCHPAY "
					+ "WHERE DPUSERID = :cusidn AND DPSVAC = :dpsvac AND DPSVBH = :dpsvbh AND (DPTXSTATUS = '0' OR DPTXSTATUS = '1')";
			Query<TXNTWSCHPAY> query = getSession().createQuery(queryString);
			query.setParameter("cusidn", cusidn);
			query.setParameter("dpsvac", dpsvac);
			query.setParameter("dpsvbh", bank);
			result = query.list();

			for (int i = 0; i < result.size(); i++) {
				TXNTWSCHPAY dptxstatus = result.get(i);
				dptxstatus.setDPTXSTATUS("3");
				update(dptxstatus);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("cancelAcn error >> {}", e);
		}
		return result;
	}

	/**
	 * 取狀態的欄位(更新狀態)
	 * 
	 * @return
	 */
	public TXNTWSCHPAY updataDptxstatus(String dpschid) {
		TXNTWSCHPAY result = null;
		try {
			log.debug("TXNTWSCHPAY_Dao >> updataDptxstatus");
			String queryString = " FROM fstop.orm.po.TXNTWSCHPAY WHERE DPSCHID = :dpschid";
			Query<TXNTWSCHPAY> query = getSession().createQuery(queryString);
			query.setParameter("dpschid", Integer.parseInt(dpschid));
			result = query.uniqueResult();

			result.setDPTXSTATUS("3");
			update(result);

		} catch (Exception e) {
			// e.printStackTrace();
			log.error("取得 Request Info 有誤.", e);
		}
		return result;
	}

	/**
	 * 取得行事曆台幣預約日期
	 * 
	 * @return
	 */
	public List<String> getByUser(String cusidn) {
		List<String> qresult = null;
		try {
//			String sql = "SELECT DPSCHTXDATE,DPWDAC,DPSVBH,DPSVAC,DPTXAMT,DPTXMEMO,ADOPNAME,ADOPENGNAME,ADOPCHSNAME,ADBRANCHID,ADBRANCHNAME,ADBRANENGNAME,ADBRANCHSNAME,DPTXSTATUS FROM\r\n"
//					+ "(SELECT * FROM\r\n" + "(SELECT * FROM  (\r\n" + "SELECT DPSCHNO FROM TXNTWSCHPAY \r\n"
//					+ "WHERE  DPUSERID = '" + cusidn + "' AND DPTXSTATUS = '0' \r\n" + ") A \r\n" + "INNER JOIN ( \r\n"
//					+ "SELECT * FROM TXNTWSCHPAYDATA WHERE DPUSERID = '" + cusidn + "' \r\n" + ") B  \r\n"
//					+ "ON A.DPSCHNO = B. DPSCHNO) X , \r\n"
//					+ "(SELECT ADOPID,ADOPNAME,ADOPENGNAME,ADOPCHSNAME FROM NB3SYSOP)  Y\r\n"
//					+ "WHERE X.ADOPID = Y.ADOPID) R,\r\n"
//					+ "(SELECT ADBRANCHID,ADBRANCHNAME,ADBRANENGNAME,ADBRANCHSNAME FROM ADMBRANCH)  T\r\n"
//					+ "WHERE R.DPSVBH = T.ADBRANCHID";
			
			StringBuffer bfs = new StringBuffer();
			bfs.append(" SELECT DPSCHTXDATE,DPWDAC,DPSVBH,DPSVAC,DPTXAMT,DPTXMEMO,ADOPNAME,ADOPENGNAME,ADOPCHSNAME,ADBANKID,ADBANKNAME,ADBANKENGNAME,ADBANKCHSNAME,DPTXSTATUS ");
			bfs.append(" FROM ( ");
			bfs.append(" SELECT * FROM ");
			bfs.append(" ( SELECT * FROM ( SELECT DPSCHNO FROM TXNTWSCHPAY WHERE DPUSERID = '" + cusidn + "' AND DPTXSTATUS = '0') A ");
			bfs.append(" INNER JOIN ");
			bfs.append(" ( SELECT * FROM TXNTWSCHPAYDATA WHERE DPUSERID = '" + cusidn + "') B ");
			bfs.append(" ON A.DPSCHNO = B.DPSCHNO ");
			bfs.append(" ) X ");
			bfs.append(" LEFT JOIN ");
			bfs.append(" ( SELECT ADOPID,ADOPNAME,ADOPENGNAME,ADOPCHSNAME FROM NB3SYSOP ) Y ");
			bfs.append(" ON X.ADOPID = Y.ADOPID ");
			bfs.append(" ) R ");
			bfs.append(" LEFT JOIN ");
			bfs.append(" ( SELECT ADBANKID,ADBANKNAME,ADBANKENGNAME,ADBANKCHSNAME FROM ADMBANK ) T ");
			bfs.append(" ON R.DPSVBH = T.ADBANKID");

			Query query = getSession().createNativeQuery(bfs.toString());
			// Stored XSS
			qresult = query.getResultList();
			if (null == qresult) {
				qresult = new ArrayList<String>();
			}
			// qresult = ESAPIUtil.validList(qresult); // 花太多時間在滾LIST迴圈先註解

		} catch (Exception e) {
			log.error("", e);
		}
		return qresult;

	}

	/**
	 * 取消轉入帳號相關預約，用UUID抓取
	 * @return
	 */
	public TXNTWSCHPAY cancelAcn(Integer dpschid,String dpuserid) {
		List result = null;
		TXNTWSCHPAY dptxstatus = null;
		try {
			log.debug("TXNTWSCHPAY_Dao >> getAcnCancel>>{}",dpschid);
			String queryString = " FROM fstop.orm.po.TXNTWSCHPAY "
					+ "WHERE DPSCHID = :dpschid AND DPUSERID= :dpuserid ";
			Query<TXNTWSCHPAY> query = getSession().createQuery(queryString);
			query.setParameter("dpschid",dpschid);
			query.setParameter("dpuserid",dpuserid);
	
			result = query.getResultList();
			if(null == result) {
				result= new ArrayList();
			}
			result = ESAPIUtil.validList(result); 
			dptxstatus = (TXNTWSCHPAY) result.get(0);
			dptxstatus.setDPTXSTATUS("3");
			update(dptxstatus);
			log.debug("TXNTWSCHPAYDao result size >> {}", result.size());
		} catch (RuntimeException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("cancelAcn error >> {}", e);
			throw new RuntimeException();
		}
		return dptxstatus;
	}
	
	
	/**
	 * 取得臺幣預約編號清單 by user
	 * 
	 * @return
	 */
	public List<String> getDpschNoByUser(String cusidn) {
		List<String> qresult = null;
		try {
			
			StringBuffer bfs = new StringBuffer();
			bfs.append(" SELECT DPSCHNO FROM TXNTWSCHPAY ");
			bfs.append(" WHERE DPUSERID = '" + cusidn + "' ");

			Query query = getSession().createNativeQuery(bfs.toString());
			// Stored XSS
			qresult = query.getResultList();
			if (null == qresult) {
				qresult = new ArrayList<String>();
			}
			// qresult = ESAPIUtil.validList(qresult); // 花太多時間在滾LIST迴圈先註解

		} catch (Exception e) {
			log.error("", e);
		}
		return qresult;

	}
	
	
}