package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.exception.LockAcquisitionException;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.TXNCARDAPPLY;
import fstop.orm.po.TXNUSER;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Transactional
@Component
public class TxnUserDao extends BaseHibernateDao<TXNUSER, Serializable>
{
	protected Logger log = LoggerFactory.getLogger(this.getClass());

	/**
	 * 清除使用者資料
	 * @param dpuserid 使用者統編
	 */
	public void deleteByUserId(String dpuserid) {
		
		String queryString = " DELETE fstop.orm.po.TXNUSER WHERE DPSUERID = :dpuserid ";
		
		try {
			Query query = getSession().createQuery(queryString);
			query.setParameter("dpuserid", dpuserid).executeUpdate();
			
		} catch (Exception e) {
			log.error("",e);
		}
	}
	
	public List<TXNUSER> findByUserId(String dpuserid)
	{
		List qresult = null;
		String queryString = " FROM fstop.orm.po.TXNUSER WHERE DPSUERID = :dpuserid ";
		// query.setMaxResults(100);
		try
		{
			log.trace(ESAPIUtil.vaildLog("dpuserid >> " + dpuserid));
			Query query = getSession().createQuery(queryString);
			query.setParameter("dpuserid", dpuserid);
			// Stored XSS
			qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			qresult = ESAPIUtil.validList(qresult);
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("findByUserId error >> {}",e);
		}

		return qresult;
	}

	@Override
	public List<TXNUSER> getAll()
	{
		List<TXNUSER> result = null;
		// 注意:舊語法已不能使用 要使用fstop.orm.po.xxx PO 要加@Entity
		String queryString = " FROM fstop.orm.po.TXNUSER ORDER BY DPSUERID";
		// String queryString = " FROM TXNADDRESSBOOK WHERE DPUSERID = :dpuserid ";
		try
		{
			Query<TXNUSER> query = getSession().createQuery(queryString);
			result = query.list();
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("getAll error >> {}",e);
		}

		return result;
		// return find("FROM TXNUSER ORDER BY DPSUERID");
	}

	public List<TXNUSER> getEmailUser()
	{
		List<TXNUSER> result = null;
		// 注意:舊語法已不能使用 要使用fstop.orm.po.xxx PO 要加@Entity
		String queryString = " FROM fstop.orm.po.TXNUSER WHERE DPMYEMAIL <> '' ORDER BY DPSUERID ";

		// String queryString = " FROM TXNADDRESSBOOK WHERE DPUSERID = :dpuserid ";
		try
		{
			Query<TXNUSER> query = getSession().createQuery(queryString);
			result = query.list();
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("getEmailUser error >> {}",e);
		}

		return result;
		// return find("FROM TXNUSER WHERE DPMYEMAIL <> '' ORDER BY DPSUERID");
	}

	public List<TXNUSER> getBillUser()
	{
		List<TXNUSER> result = null;
		// 注意:舊語法已不能使用 要使用fstop.orm.po.xxx PO 要加@Entity
		String queryString = " FROM fstop.orm.po.TXNUSER WHERE DPMYEMAIL <> '' AND (DPBILL <> '' OR DPFUNDBILL <> '' OR DPCARDBILL <> '') ORDER BY DPSUERID";
		// String queryString = " FROM TXNADDRESSBOOK WHERE DPUSERID = :dpuserid ";
		try
		{
			Query<TXNUSER> query = getSession().createQuery(queryString);
			result = query.list();
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("getBillUser error >> {}",e);
		}

		return result;
		// return find("FROM TXNUSER WHERE DPMYEMAIL <> '' AND (DPBILL <> '' OR DPFUNDBILL <> '' OR DPCARDBILL <> '')
		// ORDER BY DPSUERID");
	}

	public List<TXNUSER> getAllFundBillUser()
	{
		List<TXNUSER> result = null;
		// 注意:舊語法已不能使用 要使用fstop.orm.po.xxx PO 要加@Entity
		String queryString = " FROM fstop.orm.po.TXNUSER WHERE DPFUNDBILL <> '' and FUNDBILLDATE <> '' and LENGTH(DPMYEMAIL) > 0 ORDER BY DPSUERID";
		// String queryString = " FROM TXNADDRESSBOOK WHERE DPUSERID = :dpuserid ";
		try
		{
			Query<TXNUSER> query = getSession().createQuery(queryString);
			result = query.list();
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("getAllFundBillUser error >> {}",e);
		}

		return result;
		// return find("FROM TXNUSER WHERE DPFUNDBILL <> '' and FUNDBILLDATE <> '' and LENGTH(DPMYEMAIL) > 0 ORDER BY
		// DPSUERID");
	}

	public List<TXNUSER> getAllFundStopNotifyUser()
	{
		List<TXNUSER> result = null;
		// 注意:舊語法已不能使用 要使用fstop.orm.po.xxx PO 要加@Entity
		String queryString = " FROM fstop.orm.po.TXNUSER WHERE DPNOTIFY like '%14%' and LENGTH(DPMYEMAIL) > 0 ORDER BY DPSUERID";
		try
		{
			Query<TXNUSER> query = getSession().createQuery(queryString);
			result = query.list();
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("getAllFundStopNotifyUser error >> {}",e);
		}

		return result;
		// return find("FROM TXNUSER WHERE DPNOTIFY like '%14%' and LENGTH(DPMYEMAIL) > 0 ORDER BY DPSUERID");
	}

	public List<TXNUSER> getAllBillUserLast2Day(String tomorrow, String yesterday)
	{
		/*
		 * return find("FROM TXNUSER WHERE ((BILLDATE < '" + tomorrow + "' and BILLDATE >= '" + yesterday + "') " +
		 * " or (FUNDBILLDATE < '" + tomorrow + "' and FUNDBILLDATE >= '" + yesterday + "') " + " or (CARDBILLDATE < '"
		 * + tomorrow + "' and CARDBILLDATE >= '" + yesterday + "')) " + " ORDER BY DPSUERID");
		 */
		String subyesterday = yesterday.substring(0, 7);
		List<TXNUSER> result = null;
		// 注意:舊語法已不能使用 要使用fstop.orm.po.xxx PO 要加@Entity
		String queryString = " FROM fstop.orm.po.TXNUSER WHERE ((BILLDATE like '" + subyesterday + "%') "
				+ " or (FUNDBILLDATE like '" + subyesterday + "%') " + " or (CARDBILLDATE like '" + subyesterday
				+ "%')) " + " ORDER BY DPSUERID";
		// String queryString = " FROM TXNADDRESSBOOK WHERE DPUSERID = :dpuserid ";
		try
		{
			Query<TXNUSER> query = getSession().createQuery(queryString);
			result = query.list();
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("getAllBillUserLast2Day error >> {}",e);
		}

		return result;
		// return find("FROM TXNUSER WHERE ((BILLDATE like '" + subyesterday + "%') " +
		// " or (FUNDBILLDATE like '" + subyesterday + "%') " +
		// " or (CARDBILLDATE like '" + subyesterday + "%')) " +
		// " ORDER BY DPSUERID");
	}

	@SuppressWarnings({ "unused", "unchecked" })
	public boolean checkFundAgreeVersion(String version, String uid)
	{
		TXNUSER user = get(TXNUSER.class, uid);

		if (version.equals(user.getFUNDAGREEVERSION()))
			return true;
		else return false;
	}

	public TXNUSER getNotifyById(String cusidn)
	{
		TXNUSER result = null;
		String queryString = "FROM fstop.orm.po.TXNUSER WHERE DPSUERID = :cusidn ";
		// String queryString = " FROM TXNADDRESSBOOK WHERE DPUSERID = :dpuserid ";
		try
		{
			log.trace(ESAPIUtil.vaildLog("cusidn {}"+ cusidn));
			Query<TXNUSER> query = getSession().createQuery(queryString);
			query.setParameter("cusidn", cusidn);
			List<TXNUSER> list = query.list();
			if (null == list)
			{
				list = new ArrayList();
			}
			list = ESAPIUtil.validList((List)list);
			if(list.size() > 0)
			{
				result = list.get(0);
			}
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("getNotifyById error >> {}",e);
		}

		return result;

	}

	public TXNUSER chkRecord(String DPSUERID, String DPUSERNAME)
	{
		String dfNotify = "16,17,18,19";// 黃金存摺相關的要預設啟用通知
		TXNUSER user = null;
		int isUserExist = -1;
		try
		{
			user = get(TXNUSER.class, DPSUERID);
//			log.trace("chkRecord.DPSUERID: {}", user.getDPSUERID());
			log.trace("chkRecord.user: {}", user);
			isUserExist = ((user == null) ? 0 : 1);
		}
		catch (Exception e)
		{
			isUserExist = 0;
		}
		
		if (isUserExist == 0)
		{
			TXNUSER u = new TXNUSER();
			u.setDPSUERID(DPSUERID);
			u.setSCHCOUNT(new Integer(0));
			u.setASKERRTIMES(new Integer(0));
			u.setDPUSERNAME(DPUSERNAME);
			
			// 20200401--bugtracker no.11 新增需求: 2.5轉至3.0及新開戶使用者初次登入設定，通知服務預設皆通知
//			u.setDPNOTIFY(dfNotify);// 黃金存摺相關的要預設啟用通知
			u.setDPNOTIFY("1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21");
			
			save(u);
			log.trace("chkRecord.0.DPSUERID: {}", u.getDPSUERID());
			return u;
		}
		else if (isUserExist == 1)
		{
			user.setDPUSERNAME(DPUSERNAME);
			save(user);
			log.trace("chkRecord.1.DPSUERID: {}", user.getDPSUERID());
			return user;
		}

		return null;
	}
	
	public TXNUSER loginChk(String DPSUERID, String MAILADDR)
	{
		String dfNotify = "16,17,18,19";// 黃金存摺相關的要預設啟用通知
		TXNUSER user = null;
		int isUserExist = -1;
		try
		{
			user = get(TXNUSER.class, DPSUERID);
//			log.trace("loginChk.DPSUERID: {}", user.getDPSUERID()); 
			log.trace(ESAPIUtil.vaildLog("loginChk.user: {}"+ESAPIUtil.toJson(user)));
			isUserExist = ((user == null) ? 0 : 1);
		}
		catch (Exception e)
		{
			isUserExist = 0;
		}
		
		if (isUserExist == 0)
		{
			TXNUSER u = new TXNUSER();
			u.setDPSUERID(DPSUERID);
			u.setSCHCOUNT(new Integer(0));
			u.setASKERRTIMES(new Integer(0));
			u.setDPMYEMAIL(MAILADDR);
			
			// 20200401--bugtracker no.11 新增需求: 2.5轉至3.0及新開戶使用者初次登入設定，通知服務預設皆通知
//			u.setDPNOTIFY(dfNotify);// 黃金存摺相關的要預設啟用通知
			u.setDPNOTIFY("1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21");
			
			save(u);
			log.trace("loginChk.0.DPSUERID: {}", u.getDPSUERID());
			return u;
		}
		else if (isUserExist == 1)
		{
			user.setDPMYEMAIL(MAILADDR);
			save(user);
			log.trace("loginChk.1.DPSUERID: {}", user.getDPSUERID());
			return user;
		}

		return null;
	}

	/**
	 * 信用卡、金融卡申請網銀，如果使用者不在時建立一個使用者。
	 * 
	 * @param DPSUERID
	 * @return
	 */
	@SuppressWarnings({ "unused", "unchecked" })
	public TXNUSER chkRecord(String DPSUERID)
	{

		TXNUSER user = null;
		int isUserExist = -1;
		try
		{
			// user = super.get(StrUtils.trim(DPSUERID));
			List<TXNUSER> txnusers = findByUserId(DPSUERID);
			if (txnusers.isEmpty())
			{
				return null;
			}
			else
			{
				user = txnusers.get(0);
			}
			// System.out.println("" + user.getDPSUERID());
			isUserExist = ((user == null) ? 0 : 1);
		}
		catch (Exception e)
		{
			isUserExist = 0;
		}
		if (isUserExist == 0)
		{
			TXNUSER u = new TXNUSER();
			u.setDPSUERID(DPSUERID);
			u.setSCHCOUNT(new Integer(0));
			u.setASKERRTIMES(new Integer(0));
			u.setCCARDFLAG("0");
			
			// 20200401--bugtracker no.11 新增需求: 2.5轉至3.0及新開戶使用者初次登入設定，通知服務預設皆通知
			u.setDPNOTIFY("1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21");

			save(u);
			return u;
		}
		else if (isUserExist == 1)
		{
			return user;
		}

		return null;
	}

	public List<String> getAllUserEmailNotEmpty()
	{

		List<String> result = null;
		// 注意:舊語法已不能使用 要使用fstop.orm.po.xxx PO 要加@Entity
		String queryString = "SELECT DPMYEMAIL FROM TXNUSER  WHERE LENGTH(DPMYEMAIL) > 0 ORDER BY DPSUERID";
		// Query query = getQuery("SELECT DPMYEMAIL FROM TXNUSER WHERE LENGTH(DPMYEMAIL) > 0 ORDER BY DPSUERID");
		//
		// return query.list();
		try
		{
			Query<String> query = getSession().createQuery(queryString);
			result = query.list();
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("getAllUserEmailNotEmpty error >> {}",e);
		}

		return result;
	}

	public List<String> getAllUserEmailNotEmpty1()
	{
		List<String> result = null;
		// 注意:舊語法已不能使用 要使用fstop.orm.po.xxx PO 要加@Entity
		String queryString = "SELECT DPSUERID FROM TXNUSER  WHERE LENGTH(DPMYEMAIL) > 0 ORDER BY DPSUERID";
		// Query query = getQuery("SELECT DPMYEMAIL FROM TXNUSER WHERE LENGTH(DPMYEMAIL) > 0 ORDER BY DPSUERID");
		//
		// return query.list();
		try
		{
			Query<String> query = getSession().createQuery(queryString);
			result = query.list();
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("getAllUserEmailNotEmpty1 error >> {}",e);
		}

		return result;

		// Query query = getQuery("SELECT DPSUERID FROM TXNUSER WHERE LENGTH(DPMYEMAIL) > 0 ORDER BY DPSUERID");
		//
		// return query.list();
	}

	public String getAllUserEmailNotEmpty2(String dpuserid)
	{
		String result = null;
		String queryString = " FROM fstop.orm.po.TXNUSER WHERE DPUSERID = :dpuserid ";
		// query.setMaxResults(100);
		try
		{
			log.trace("dpuserid {}", dpuserid);
			Query<TXNUSER> query = getSession().createQuery(queryString);
			query.setParameter("dpuserid", dpuserid);
			result = query.list().get(0).toString();
			// Query query = getQuery("SELECT DPMYEMAIL FROM TXNUSER WHERE DPSUERID = ?");
			// query.setParameter(0, cusidn);
			// List qresult = query.list();
			// return qresult.get(0).toString();
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("getAllUserEmailNotEmpty2 error >> {}",e);
		}

		return result;
	}

	public List<String> getAllUserEmailApplied()
	{
		List<String> result = null;
		// 注意:舊語法已不能使用 要使用fstop.orm.po.xxx PO 要加@Entity
		String queryString = "SELECT DPMYEMAIL FROM TXNUSER  WHERE LENGTH(DPMYEMAIL) > 0 and DPNOTIFY like '%15%' ORDER BY DPSUERID";
		// Query query = getQuery("SELECT DPMYEMAIL FROM TXNUSER WHERE LENGTH(DPMYEMAIL) > 0 and DPNOTIFY like '%15%'
		// ORDER BY DPSUERID");
		//
		// return query.list();
		try
		{
			Query<String> query = getSession().createQuery(queryString);
			result = query.list();
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("getAllUserEmailApplied error >> {}",e);
		}

		return result;

	}
	
	public boolean checkEmailDup(String cusidn, String email) {
//		"SELECT COUNT(*) FROM TXNUSER  WHERE DPSUERID <> ? AND DPMYEMAIL = ?", cusidn, email
		// 注意:舊語法已不能使用 要使用fstop.orm.po.xxx PO 要加@Entity
		String queryString = "FROM fstop.orm.po.TXNUSER WHERE DPSUERID <> :cusidn AND DPMYEMAIL = :email";
		Query<TXNUSER> query = getSession().createQuery(queryString);
		query.setParameter("cusidn", cusidn);
		query.setParameter("email", email);
		
		if (query.getResultList().size() > 0) {
			return true;
		} else {
			return false;
		}
	}		

	@Override
	public <T> T get(Class<T> entityClass, Serializable id) throws DataAccessException
	{
		// TODO Auto-generated method stub
		return super.get(entityClass, id);
	}

	// @Transactional(propagation = Propagation.REQUIRES_NEW)
	// public void updateSchcount(String DPSUERID) {
	//
	// SQLQuery add1=this.getSession().createSQLQuery("UPDATE TXNUSER SET SCHCOUNT = SCHCOUNT + 1 WHERE DPSUERID = ?");
	//
	// add1.setString(0, DPSUERID);
	//
	// int r2 = add1.executeUpdate();
	// }
	//
	// @Transactional(propagation = Propagation.REQUIRES_NEW)
	// public int incSchcount(String DPSUERID) {
	//
	// //判斷是否換日
	// SQLQuery add0 = getSession().createSQLQuery("SELECT SCHCNTDATE FROM TXNUSER WHERE DPSUERID = ?");
	// add0.setString(0, DPSUERID);
	// List result0 = add0.list();
	//
	// String schcntdate = (String) result0.get(0) == null ? "" : (String) result0.get(0); //最後拿編號日期
	// String sysdate = DateTimeUtils.format("yyyyMMdd", new Date());
	//
	// SQLQuery add1 = null;
	//
	// if(!schcntdate.equals(sysdate)){
	// add1= getSession().createSQLQuery("UPDATE TXNUSER SET SCHCOUNT = 1, SCHCNTDATE = ? WHERE DPSUERID = ?");
	// add1.setString(0, sysdate);
	// add1.setString(1, DPSUERID);
	// } else {
	// add1= getSession().createSQLQuery("UPDATE TXNUSER SET SCHCOUNT = SCHCOUNT + 1 WHERE DPSUERID = ?");
	// add1.setString(0, DPSUERID);
	// }
	//
	// int r2 = add1.executeUpdate();
	//
	// SQLQuery add2= getSession().createSQLQuery("SELECT SCHCOUNT FROM TXNUSER WHERE DPSUERID = ? ");
	// add2.setString(0, DPSUERID);
	// add2.addScalar( "SCHCOUNT", Hibernate.INTEGER);
	// List result = add2.list();
	//
	// return (Integer)result.get(0);
	// }
	//
	// @Transactional(propagation = Propagation.REQUIRES_NEW)
	// public int updateAdBranchID(String DPSUERID,String ADBRANCHID) {
	//
	// SQLQuery add1=this.getSession().createSQLQuery("UPDATE TXNUSER SET ADBRANCHID = ? WHERE DPSUERID = ?");
	//
	// add1.setString(0, ADBRANCHID);
	// add1.setString(1, DPSUERID);
	//
	// int r2 = add1.executeUpdate();
	// return(r2);
	// }
	//
	// @Transactional(propagation = Propagation.REQUIRES_NEW)
	// public int updateFundAgreeVersion(String DPSUERID, String VERSION) {
	//
	// SQLQuery add1=this.getSession().createSQLQuery("UPDATE TXNUSER SET FUNDAGREEVERSION = ? WHERE DPSUERID = ?");
	//
	// add1.setString(0, VERSION);
	// add1.setString(1, DPSUERID);
	//
	// int r2 = add1.executeUpdate();
	// return(r2);
	// }
	//
	// /**
	// * 信用卡、金融卡申請網銀，如果使用者不在時建立一個使用者。
	// * @param DPSUERID
	// * @return
	// */
	// @SuppressWarnings({ "unused", "unchecked" })
	// public TXNUSER chkRecord(String DPSUERID) {
	//
	// TXNUSER user = null;
	// int isUserExist = -1;
	// try {
	// user = super.get(StrUtils.trim(DPSUERID));
	//// System.out.println("" + user.getDPSUERID());
	// isUserExist = ((user == null) ? 0 : 1);
	// }
	// catch(Exception e) {
	// isUserExist = 0;
	// }
	// if(isUserExist == 0 ) {
	// TXNUSER u = new TXNUSER();
	// u.setDPSUERID(DPSUERID);
	// u.setSCHCOUNT(new Integer(0));
	// u.setASKERRTIMES(new Integer(0));
	// u.setCCARDFLAG("0");
	//
	// save(u);
	// return u;
	// }
	// else if(isUserExist == 1) {
	// return user;
	// }
	//
	// return null;
	// }
	// @Transactional(propagation = Propagation.REQUIRES_NEW)
	// public int updateDPMYEMAIL(String DPSUERID, String EMAIL) {
	// Date d = new Date();
	// String date = DateTimeUtils.getCDateShort(d);
	// String time = DateTimeUtils.getTimeShort(d);
	//
	// SQLQuery add1=this.getSession().createSQLQuery("UPDATE TXNUSER SET DPMYEMAIL = ? ,EMAILDATE = ? WHERE DPSUERID =
	// ?");
	//
	// add1.setString(0, EMAIL);
	// add1.setString(1, date + time);
	// add1.setString(2, DPSUERID);
	// int r2 = add1.executeUpdate();
	// return(r2);
	// }
	

	@Override
	public void saveOrUpdate(Object entity) throws DataAccessException {
		try {
			// TODO Auto-generated method stub
			super.saveOrUpdate(entity);
			log.info("TxnUser finish saveOrUpdate >> {}", entity);
		}catch(Throwable e) {
			log.error("TxnUser Throwable >> {}, {}", entity, e.toString());
			throw e;
		}
	}
	@Override
	public void update(Object entity) throws DataAccessException {
		try {
			// TODO Auto-generated method stub
			super.update(entity);
			log.info("TxnUser finish update >> {}", entity);
		}catch(Throwable e) {
			log.error("TxnUser Throwable >> {}, {}", entity, e.toString());
			throw e;
		}
	}
	@Override
	public Serializable save(Object entity) throws DataAccessException {
		try {
			// TODO Auto-generated method stub
			Serializable result = super.save(entity);
			log.info("TxnUser finish save >> {}", entity);
			return result;
		}catch(Throwable e) {
			log.error("TxnUser Throwable >> {}, {}", entity, e.toString());
			throw e;
		}
	}
}