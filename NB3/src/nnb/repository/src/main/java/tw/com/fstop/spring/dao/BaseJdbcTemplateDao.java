
/*
 * Copyright (c) 2017, FSTOP, Inc. All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tw.com.fstop.spring.dao;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

/**
 * Base class for jdbc template dao.
 * Inject NamedParameterJdbcTemplate is preferred.  
 * When injected by datasource, this class will automatically initialize NamedParameterJdbcTemplate object. 
 *
 * @since 1.0.1
 */
public class BaseJdbcTemplateDao
{
    
    @Resource
    NamedParameterJdbcTemplate dbi;
        
    DataSource dataSource;
    
    public void setDataSource(DataSource dataSource) 
    {
        this.dataSource = dataSource;
        this.dbi = new NamedParameterJdbcTemplate(dataSource);
    }

    public DataSource getDataSource()
    {
        return dataSource;
    }

    public void setDbi(NamedParameterJdbcTemplate jdbc)
    {
        this.dbi = jdbc;
    }

    public NamedParameterJdbcTemplate getDbi()
    {
        return dbi;
    }

}
