package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import fstop.orm.po.OLD_TXNTRACCSET;
import tw.com.fstop.spring.dao.OLD_BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;
 
@Component
public class Old_TxnTrAccSetDao extends OLD_BaseHibernateDao<OLD_TXNTRACCSET, Serializable> {
	protected Logger log = LoggerFactory.getLogger(this.getClass());
	
	public List<OLD_TXNTRACCSET> findById(String dpuserid) {
		List qresult = null;
		
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT DPUSERID, DPTRIBANK, DPTRDACNO, MAX(DPACCSETID) DPACCSETID, MAX(DPGONAME) DPGONAME, MAX(DPTRACNO) DPTRACNO, MAX(LASTDATE) LASTDATE, MAX(LASTTIME) LASTTIME, MAX(DPPHOTOID) DPPHOTOID ");
		sb.append("FROM TXNTRACCSET ");
		sb.append("WHERE DPUSERID ='" + dpuserid + "' ");
		sb.append("GROUP BY DPUSERID, DPTRIBANK, DPTRDACNO ");
		try {
			Query query = getSession().createNativeQuery(sb.toString(), OLD_TXNTRACCSET.class);
			// Stored XSS
			qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			log.debug(ESAPIUtil.vaildLog("qresult>>"+qresult));
			qresult = ESAPIUtil.validList(qresult);
			
		} catch (Exception e) {
			log.error("{}", e);
		}

		return qresult;
	}
	
	public List<OLD_TXNTRACCSET> findByUserId(String dpuserid) {
		List qresult = null;
		String queryString = " FROM fstop.orm.po.OLD_TXNTRACCSET WHERE DPUSERID = :dpuserid ";

		try {
			log.trace("dpuserid {}", dpuserid);
			Query query = getSession().createQuery(queryString);
			query.setParameter("dpuserid", dpuserid);
			// Stored XSS
			qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			log.debug(ESAPIUtil.vaildLog("qresult>>"+qresult));
			qresult = ESAPIUtil.validList(qresult);
			
		} catch (Exception e) {
			log.error("{}", e);
		}

		return qresult;
	}
	
	
	public boolean findDptrdacnoIsExists(String idn,String DPTRIBANK, String DPTRDACNO) {
		Query query = this.getSession().createQuery("SELECT count(DPTRDACNO) FROM  fstop.orm.po.TXNTRACCSET WHERE DPUSERID=?0 and DPTRIBANK = ?1 and DPTRDACNO = ?2");
		query.setParameter(0, idn);
		query.setParameter(1, DPTRIBANK);
		query.setParameter(2, DPTRDACNO);
		
		return ((Long)query.list().iterator().next()) > 0;
	}
	
	
	public OLD_TXNTRACCSET getDpgoNameII(String cusidn,String acn,String bank) 
	{
		String result = "";
		OLD_TXNTRACCSET po = null;
		try
		{
			String sql = "FROM fstop.orm.po.OLD_TXNTRACCSET where DPUSERID =:cusidn AND DPTRDACNO =:acn AND DPTRIBANK =:bank";
			Query<OLD_TXNTRACCSET> query = getSession().createQuery(sql);
			query.setParameter("cusidn", cusidn);
			query.setParameter("acn", acn);
			query.setParameter("bank", bank);
			po = query.uniqueResult();
		}
		catch (Exception e)
		{
			log.error(ESAPIUtil.vaildLog("Old_TxnTrAccSetDao.getDpgoNameII.ERROR({})."+ cusidn+"/"+acn+"/"+bank+ e));
			result = "";
		}
		return po;
	}
}
