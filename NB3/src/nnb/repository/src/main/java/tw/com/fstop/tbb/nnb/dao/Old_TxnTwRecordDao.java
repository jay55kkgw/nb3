package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import fstop.orm.po.OLD_TXNTWRECORD;
import fstop.orm.po.TXNTWRECORD;
import tw.com.fstop.spring.dao.OLD_BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;
 
@Component
public class Old_TxnTwRecordDao extends OLD_BaseHibernateDao<OLD_TXNTWRECORD, Serializable> {
	protected Logger log = LoggerFactory.getLogger(this.getClass());
	
	public List<OLD_TXNTWRECORD> findByUserId(String dpuserid) {
		List qresult = null;
		String queryString = " FROM fstop.orm.po.OLD_TXNTWRECORD WHERE DPUSERID = :dpuserid ";

		try {
			log.trace("dpuserid {}", dpuserid);
			Query query = getSession().createQuery(queryString);
			query.setParameter("dpuserid", dpuserid);
			// Stored XSS
			qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			log.debug(ESAPIUtil.vaildLog("qresult>>"+qresult));
			qresult = ESAPIUtil.validList(qresult);
			
		} catch (Exception e) {
			log.error("{}", e);
		}

		return qresult;
	}
	
	
	public List<TXNTWRECORD> findById(String aduserid) {
		List qresult = null;
		String queryString = "SELECT ADTXNO, ADOPID, DPUSERID, DPTXDATE, DPTXTIME, DPWDAC, DPSVBH, DPSVAC, DPTXAMT, DPTXMEMO, DPTXMAILS, DPTXMAILMEMO, "
				+ "DPSCHID, DPEFEE, PCSEQ, DPTXNO, DPTXCODE, DPTITAINFO, DPTOTAINFO, DPREMAIL, DPTXSTATUS, DPEXCODE, LASTDATE, LASTTIME, 'NB' AS LOGINTYPE "
				+ "FROM TXNTWRECORD "
				+ "WHERE DPUSERID = '" + aduserid + "' ";
		try {
			log.trace("aduserid {}", aduserid);
			Query query = getSession().createNativeQuery(queryString, TXNTWRECORD.class);
			// Stored XSS
			qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			log.debug(ESAPIUtil.vaildLog("qresult>>"+qresult));
			qresult = ESAPIUtil.validList(qresult);
			
		} catch (Exception e) {
			log.error("{}", e);
		}

		return qresult;
	}
}
