package tw.com.fstop.tbb.nnb.dao;


import java.math.BigInteger;
import java.util.List;

import fstop.orm.po.ITRCOUNT;
import tw.com.fstop.spring.dao.BaseHibernateDao;
//import tw.com.fstop.tbb.nnb.po.ITRCOUNT2;

//public class ItrCountDao2 extends BaseHibernateDao<ITRCOUNT2, String> {
public class ItrCountDao2 extends BaseHibernateDao<ITRCOUNT, String> {

	public String getCountSum()
	{
		String sql = 
				//"SELECT sum(int(char('COUNT')))  from ITRCOUNT";
				"select sum (cast (trim(COUNT) as integer)) from ITRCOUNT";
		
		List list = this.getBySql(sql);
		BigInteger sum = (BigInteger)list.get(0);
		return sum.toString();
	}
	
	//TODO 要查明原本不用，但後來卻要加上的原因
	public void save(ITRCOUNT obj)
	{
		getSession().save(obj);
	}

}
