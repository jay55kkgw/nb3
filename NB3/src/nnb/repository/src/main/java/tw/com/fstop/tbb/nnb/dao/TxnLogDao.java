package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.TXNFUNDDATA;
import fstop.orm.po.TXNLOG;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Transactional
@Component
public class TxnLogDao extends BaseHibernateDao<TXNLOG, Serializable>{
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	/**
	 * 起訖時間內交易紀錄，附加電文當條件
	 * 電文為空查詢全部交易紀錄，不空則查詢帳號下資料
	 * @return
	 */
	public List<TXNLOG> findByDateRange(Map<String, String> reqParam) {
		List result=null;
		try {
			log.trace(ESAPIUtil.vaildLog("findByDateRange>>{}"+ reqParam));
			String cusidn = reqParam.get("cusidn");
			log.trace(ESAPIUtil.vaildLog("cusidn>>{}"+cusidn));
			String adopid = reqParam.get("adopid");
			log.trace(ESAPIUtil.vaildLog("adopid>>{}"+adopid));
			String sdate = reqParam.get("sdate").replace("/", "");
			log.trace(ESAPIUtil.vaildLog("sdate>>{}"+sdate));
			String edate = reqParam.get("edate").replace("/", "");
			log.trace(ESAPIUtil.vaildLog("edate>>{}"+edate));
			
			StringBuffer queryString = new StringBuffer();
			queryString.append("from fstop.orm.po.TXNLOG where ADUSERID = :cusidn and LASTDATE >= :sdate and LASTDATE <= :edate");
			
			if (adopid == null ||adopid.trim().length() == 0){
				Query<TXNLOG> query = getSession().createQuery(queryString.toString());
				query.setParameter("cusidn",cusidn);
				query.setParameter("sdate",sdate);
				query.setParameter("edate",edate);
				result = query.getResultList();
			}else{
				queryString.append(" AND ADOPID = :adopid ");
				Query<TXNLOG> query = getSession().createQuery(queryString.toString());
				query.setParameter("cusidn",cusidn);
				query.setParameter("sdate",sdate);
				query.setParameter("edate",edate);
				query.setParameter("adopid",adopid.trim());
				result = query.getResultList();
			}
			if(null == result) {
				result = new ArrayList();
			}
			result = ESAPIUtil.validList(result);
			
			log.trace(ESAPIUtil.vaildLog("QueryResult>>"+result));
		}catch(Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("cancelAcn error >> {}", e);
		}
		
		return result;
	}
	
	public List<Map> findLast5LimitData(String CUSIDN){
		List result=null;
		try {
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT TX.ADOPID, TX.ADUSERID, Max(TX.LASTDATE) as LASTDATE, Max(TX.LASTTIME) as LASTTIME, Max(SYS.ADOPNAME) as ADOPNAME, Max(SYS.ADOPENGNAME) as ADOPENGNAME, Max(SYS.ADOPCHSNAME) as ADOPCHSNAME, Max(SYS.URL) as URL ");
			sb.append("from TXNLOG as TX ,NB3SYSOP AS SYS WHERE TX.ADOPID=SYS.ADOPID AND TX.ADUSERID='" + CUSIDN + "' ");
			sb.append("GROUP BY TX.ADOPID, TX.ADUSERID ORDER BY LASTDATE DESC, LASTTIME DESC LIMIT 5");
			String queryString = sb.toString();
			NativeQuery query = getSession().createNativeQuery(queryString);
			List<Object[]> resulttmp = query.list();
			result = new ArrayList<Map<String,String>>();
			for(Object[] row : resulttmp) {
				Map<String,String> rowObj = new HashMap<String,String>();
				rowObj.put("ADOPID", (String)row[0]);
				rowObj.put("ADUSERID", (String)row[1]);
				rowObj.put("LASTDATE", (String)row[2]);
				rowObj.put("LASTTIME", (String)row[3]);
				rowObj.put("ADOPNAME", (String)row[4]);
				rowObj.put("ADOPENGNAME", (String)row[5]);
				rowObj.put("ADOPCHSNAME", (String)row[6]);
				rowObj.put("URL", (String)row[7]);
				result.add(rowObj);
			}
		}catch(Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("cancelAcn error >> {}", e);
		}
		
		return result;
	}

	@Override
	public Serializable save(Object entity) throws DataAccessException {
		// TODO Auto-generated method stub
		return super.save(entity);
	}

	@Override
	public Serializable save(String entityName, Object entity) throws DataAccessException {
		// TODO Auto-generated method stub
		return super.save(entityName, entity);
	}

	@Override
	public void saveOrUpdate(Object entity) throws DataAccessException {
		// TODO Auto-generated method stub
		super.saveOrUpdate(entity);
	}

	@Override
	public void saveOrUpdate(String entityName, Object entity) throws DataAccessException {
		// TODO Auto-generated method stub
		super.saveOrUpdate(entityName, entity);
	}
	
	@Override
	public void update(Object entity) throws DataAccessException {
		// TODO Auto-generated method stub
		super.update(entity);
	}

	@Override
	public Object get(String entityName, Serializable id) throws DataAccessException {
		// TODO Auto-generated method stub
		return super.get(entityName, id);
	}

	@Override
	public <T> T get(Class<T> entityClass, Serializable id) throws DataAccessException {
		// TODO Auto-generated method stub
		return super.get(entityClass, id);
	}

	@Override
	public List<TXNLOG> getAll(){
		return super.getAll();
	}
	@Override
	@SuppressWarnings("rawtypes")
	public List getBySql(String sql){
		return super.getBySql(sql);
	}
	@Override
	public List<TXNLOG> getAllOrderByCol(String col){
		return super.getAllOrderByCol(col);
	}
}
