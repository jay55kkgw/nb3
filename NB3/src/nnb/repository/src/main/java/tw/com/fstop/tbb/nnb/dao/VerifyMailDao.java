package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.VERIFYMAIL;
import tw.com.fstop.spring.dao.BaseHibernateDao;

@Transactional
@Component
public class VerifyMailDao extends BaseHibernateDao<VERIFYMAIL, Serializable>
{
	protected Logger log = LoggerFactory.getLogger(this.getClass());
	
	public VERIFYMAIL findByENC_URL(String enc_url)
	{
		VERIFYMAIL qresult = null;
		String queryString = " FROM fstop.orm.po.VERIFYMAIL WHERE ENC_URL = :enc_url";
		try
		{
			Query query = getSession().createQuery(queryString);
			query.setParameter("enc_url", enc_url);
			qresult = (VERIFYMAIL) query.list().get(0);
		}
		catch (Exception e)
		{
			log.error("findByENC_URL error >> {}",e);
		}

		return qresult;
	}
	
	public VERIFYMAIL findActiveMailById(String cusidn)
	{
		VERIFYMAIL qresult = null;
		String queryString = " FROM fstop.orm.po.VERIFYMAIL WHERE CUSIDN = :cusidn AND STATUS ='0' ";
		try
		{
			Query query = getSession().createQuery(queryString);
			query.setParameter("cusidn", cusidn);
			qresult = (VERIFYMAIL) query.list().get(0);
		}
		catch (Exception e)
		{
			log.error("findActiveMailById error >> {}",e);
		}

		return qresult;
	}
	
}