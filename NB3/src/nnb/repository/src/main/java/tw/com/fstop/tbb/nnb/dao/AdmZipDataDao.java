package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.ADMZIPDATA;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Transactional
@Component
public class AdmZipDataDao extends BaseHibernateDao<ADMZIPDATA, Serializable>
{
	private Logger log = LoggerFactory.getLogger(this.getClass());

	/**
	 * 查詢City
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, String>> findCity()
	{
		List<Map<String, String>> resultListMap = null;
		try
		{
			String queryString = "SELECT CITY FROM ADMZIPDATA GROUP BY CITY";
			Query query = getSession().createQuery(queryString);
			// List<String> ADMZIPDATAList = ESAPIUtil.validList(query.list());

			// Stored XSS
			List<Object> qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			qresult = ESAPIUtil.validList(qresult);

			resultListMap = new ArrayList<Map<String, String>>();
			for (Object o : qresult)
			{
				Map<String, String> map = new HashMap<String, String>();
				map.put("CITY", (String) o);

				resultListMap.add(map);
			}
			log.debug(ESAPIUtil.vaildLog("resultListMap="+ resultListMap));
		}
		catch (Exception e)
		{
//			e.printStackTrace();
			log.error("findCity error >> {}",e);
		}
		return resultListMap;
	}

	/**
	 * 查詢Area
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, String>> findArea(String city)
	{
		List<Map<String, String>> resultListMap = null;
		try
		{
			String queryString = "SELECT AREA FROM ADMZIPDATA WHERE CITY = :CITY GROUP BY AREA";
			Query query = getSession().createQuery(queryString);
			query.setParameter("CITY", city);

			// List<String> ADMZIPDATAList = ESAPIUtil.validList(query.list());

			// Stored XSS
			List<Object> qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			qresult = ESAPIUtil.validList(qresult);

			resultListMap = new ArrayList<Map<String, String>>();

			for (Object o : qresult)
			{
				Map<String, String> map = new HashMap<String, String>();
				map.put("AREA", (String) o);

				resultListMap.add(map);
			}
			log.debug(ESAPIUtil.vaildLog("resultListMap >> " + ESAPIUtil.toJson(resultListMap)));
		}
		catch (Exception e)
		{
//			e.printStackTrace();
			log.error("findArea error >> {}",e);
		}
		return resultListMap;
	}

	/**
	 * 查詢ZIPCODE
	 */
	@SuppressWarnings("unchecked")
	public String findZipBycityarea(String city, String area)
	{
		String ZIPCODE = "";

		String queryString = "SELECT ZIPCODE FROM ADMZIPDATA WHERE CITY = :CITY AND AREA = :AREA GROUP BY ZIPCODE";
		Query query = getSession().createQuery(queryString);
		query.setParameter("CITY", city);
		query.setParameter("AREA", area);
		// List<String> ADMZIPDATAList = ESAPIUtil.validList(query.list());

		// Stored XSS
		List<Object> qresult = query.list();
		if (null == qresult)
		{
			qresult = new ArrayList();
		}
		qresult = ESAPIUtil.validList(qresult);

		if (qresult.size() > 0)
		{
			ZIPCODE = (String) qresult.get(0);
		}
		log.debug(ESAPIUtil.vaildLog("ZIPCODE >> " + ZIPCODE));

		return ZIPCODE;
	}
}