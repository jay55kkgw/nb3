package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import fstop.orm.po.TXNCSSSLOG;
import fstop.orm.po.TXNFXSCHPAYDATA;
import tw.com.fstop.spring.dao.BaseHibernateDao;

@Transactional
@Component
public class TxnCsssLogDao extends BaseHibernateDao<TXNCSSSLOG, Serializable> {
	private Logger log = LoggerFactory.getLogger(this.getClass());

	/**
	 * 在今年填寫過客戶滿意度問卷調查 或是 選擇三次的下次再填 回true 否則回false
	 * 
	 * @param cusidn
	 *            身分證
	 * @param type
	 *            1為判斷填寫 3為判斷是否三次回下次再填
	 * @return Boolean
	 */
	public Boolean isFill(String cusidn, String year, String type) {
		Boolean result = false;
		String queryString = "";
		int count = 0;
		queryString = "FROM fstop.orm.po.TXNCSSSLOG WHERE CUSIDN = :cusidn AND NEXTBTN = :type AND LASTDATE BETWEEN :startYear AND :endYear";

		Query query = getSession().createQuery(queryString);
		query.setParameter("cusidn", cusidn);
		query.setParameter("type", type);
		query.setParameter("startYear", year+"0000");
		query.setParameter("endYear", year+"9999");
		if (query.getResultList() == null ||query.getResultList().size()==0 ) {
			return result;
		}
		count = query.getResultList().size();
		if (("N".equals(type) && count > 0) || ("Y".equals(type) && count >= 3)) {
			result = true;
		}

		return result;
	}
}