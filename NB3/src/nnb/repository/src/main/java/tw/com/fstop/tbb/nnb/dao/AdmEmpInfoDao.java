package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.ADMBANK;
import fstop.orm.po.ADMBHCONTACT;
import fstop.orm.po.ADMEMPINFO;
import fstop.orm.po.TXNUSER;
import tw.com.fstop.spring.dao.BaseHibernateDao;
 
@Transactional
@Component
public class AdmEmpInfoDao  extends BaseHibernateDao<ADMEMPINFO, Serializable> {
	Logger log = LoggerFactory.getLogger(this.getClass());
	public boolean isEmpCUSID(String CUSID) {
		List<ADMEMPINFO> list = null;
		boolean result = false;
//		long count = (Long) (find(
//				"SELECT COUNT(*) FROM ADMEMPINFO where CUSID = ? ", CUSID)).get(0);
//		if (count == 0) {
//			return false;
//		} else {
//			return true;
//		}
		try
		{
			String queryString = " FROM ADMEMPINFO where CUSID = :CUSID and LDATE='' ";
			Query<ADMEMPINFO> query = getSession().createQuery(queryString);
			query.setParameter("CUSID", CUSID);
			list = query.list();
			if(list.size() > 0)
				result = true;
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("AdmEmpInfoDao error >> {}",e);
		}
		return result;
	}
	
	public boolean isEmployeeAndEmpMail(String cusidn, String email) {
//		long count = (Long) (find(
//				"SELECT COUNT(*) FROM ADMEMPINFO where LDATE = '' and CUSID = ? and EMPMAIL = ?", cusidn, empIdMail)).get(0);
		String queryString = " FROM ADMEMPINFO WHERE LDATE = '' AND CUSID = :cusidn AND EMPMAIL = :email ";
		Query<TXNUSER> query = getSession().createQuery(queryString);
		query.setParameter("cusidn", cusidn);
		query.setParameter("email", email);
		
		if (query.getResultList().size() > 0) {
			return true;
		} else {
			return false;
		}
	}	
}
