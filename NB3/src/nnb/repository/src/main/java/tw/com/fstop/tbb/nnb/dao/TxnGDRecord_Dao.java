package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import fstop.orm.po.TXNGDRECORD;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Transactional
@Component
public class TxnGDRecord_Dao extends BaseHibernateDao<TXNGDRECORD, Serializable> {
	private Logger log = LoggerFactory.getLogger(getClass());

	/**
	 * 用日期查詢黃金轉帳結果 *
	 * 
	 * @param
	 * @return
	 */
	
	public List<TXNGDRECORD> findAllByDateRange(Map<String, String> reqParam, Boolean flag) {
		List result = null;
		Query<TXNGDRECORD> query;
		String sdate = reqParam.get("sdate");
		String edate = reqParam.get("edate");
		String gduserid = reqParam.get("cusidn");
		String adopid = reqParam.get("adopid");
		StringBuilder queryString = new StringBuilder();
		queryString.append(
				" FROM fstop.orm.po.TXNGDRECORD WHERE GDUSERID = :gduserid AND GDTXDATE BETWEEN :sdate AND :edate");
		try {			
			if (flag == false) {
				queryString.append("  AND ADOPID = :adopid");
				query = getSession().createQuery(queryString.toString());
				query.setParameter("adopid", adopid);
			} else {
				query = getSession().createQuery(queryString.toString());
			}

			query.setParameter("gduserid", gduserid);
			query.setParameter("sdate", sdate);
			query.setParameter("edate", edate);
			
			result = query.getResultList();
			if (null == result)		
			{
				result = new ArrayList();
			}
			log.debug("TXNFXSCHPAYDao result size >> {}", result.size());
			result = ESAPIUtil.validList(result);
		} catch (Exception e) {
			log.error("getByUid error >> {}", e);
		}
		return result;
	}
}
