package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.ADMLOGIN;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Transactional
@Component
public class AdmLoginDao extends BaseHibernateDao<ADMLOGIN, Serializable> {
	protected Logger log = LoggerFactory.getLogger(this.getClass());

	public List<ADMLOGIN> findByUserId(String aduserid) {
		List result = null;
		String queryString = " FROM fstop.orm.po.ADMLOGIN WHERE ADUSERID = :aduserid ";
		try {
			log.debug(ESAPIUtil.vaildLog("AdmLoginDao.findByUserId.aduserid: {}"+ aduserid));
			Query<ADMLOGIN> query = getSession().createQuery(queryString);
			query.setParameter("aduserid", aduserid);
			result = query.list();
			if (null == result)
			{
				result = new ArrayList();
			}
			result = ESAPIUtil.validList(result);
		} catch (Exception e) {
			log.error("",e);
		}
		return result;
	}
	
	public List<ADMLOGIN> findByIp(String cusidn, String ip) {
		List result = null;
		String queryString = " FROM fstop.orm.po.ADMLOGIN WHERE ADUSERID <> :cusidn AND ADUSERIP = :aduserip AND LOGINOUT = '0' ";
		try {
			log.debug(ESAPIUtil.vaildLog("AdmLoginDao.findByUserId.aduserip: {}"+ ip));
			Query<ADMLOGIN> query = getSession().createQuery(queryString);
			query.setParameter("aduserip", ip);
			query.setParameter("cusidn", cusidn);
			result = query.list();
		} catch (Exception e) {
			log.error("",e);
		}
		return result;
	}
	
}