package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.TXNCUSINVATTRIB;
import tw.com.fstop.spring.dao.BaseHibernateDao;
 
@Transactional
@Component
public class TxnGdRecordDao extends BaseHibernateDao<TXNCUSINVATTRIB,Serializable>{
	Logger log = LoggerFactory.getLogger(this.getClass());

	/**
	 * 清除使用者資料
	 * @param dpuserid 使用者統編
	 */
	public void deleteByUserId(String gduserid) {
		
		String queryString = " DELETE fstop.orm.po.TXNGDRECORD WHERE GDUSERID = :gduserid ";
		
		try {
			Query query = getSession().createQuery(queryString);
			query.setParameter("gduserid", gduserid).executeUpdate();
			
		} catch (Exception e) {
			log.error("",e);
		}
	}
	
}