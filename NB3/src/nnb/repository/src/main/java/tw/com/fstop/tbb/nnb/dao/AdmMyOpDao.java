package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.query.Query;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.ADMMYOP;
import fstop.orm.po.ADMMYOP_PK;
import tw.com.fstop.spring.dao.BaseHibernateDao;

	
@Transactional
@Component
public class AdmMyOpDao extends BaseHibernateDao<ADMMYOP, Serializable>
{
	protected Logger log = Logger.getLogger(getClass());

	
	public List<String> getMyOpList(String cusidn) {
		List<String> qresult = null;
		try {
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT ADOPID FROM ADMMYOP ");
			sb.append("WHERE ADUSERID = :cusidn ");
			String queryString = sb.toString();
			Query query = getSession().createNativeQuery(queryString);
			query.setParameter("cusidn", cusidn);
			// Stored XSS
			qresult = query.getResultList();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}

		} catch (Exception e) {
			log.error("", e);
		}
		return qresult;
	}
	
	
	/**
	 * 刪除我的最愛功能
	 * @param dpuserid 使用者統編
	 */
	public void deleteById(String cusidn, String adopid) {
		
		String queryString = " DELETE fstop.orm.po.ADMMYOP WHERE ADUSERID = :cusidn AND ADOPID = :adopid";
		
		try {
			Query query = getSession().createQuery(queryString);
			query.setParameter("cusidn", cusidn);
			query.setParameter("adopid", adopid);
			query.executeUpdate();
			
		} catch (Exception e) {
			log.error("", e);
		}
	}
	
}
