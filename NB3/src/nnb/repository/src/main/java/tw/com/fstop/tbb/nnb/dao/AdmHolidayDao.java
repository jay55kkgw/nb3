package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import fstop.orm.po.ADMHOLIDAY;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.DateUtil;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Transactional
@Component
public class AdmHolidayDao extends BaseHibernateDao<ADMHOLIDAY, Serializable>
{
	Logger log = LoggerFactory.getLogger(this.getClass());

	/**
	 * 判斷該日是否為營業日
	 * 
	 * @param uid
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public ADMHOLIDAY findADREMARK()
	{

		String due = DateUtil.getCurentDateTime("yyyyMMdd");
		this.logger.debug("due  ===>" + due);
		ADMHOLIDAY po = null;
		try
		{
			String queryString = " FROM fstop.orm.po.ADMHOLIDAY WHERE ADHOLIDAYID= :ADHOLIDAYID ";
			Query<ADMHOLIDAY> query = getSession().createQuery(queryString);
			query.setParameter("ADHOLIDAYID", due);
			po = query.uniqueResult();
		}
		catch (Exception e)
		{
//			e.printStackTrace();
			log.error("AdmHolidayDao error >> {}",e);
		}
		return po;

	}

	@SuppressWarnings("unchecked")
	public boolean isAdmholiday()
	{

		String due = DateUtil.getCurentDateTime("yyyyMMdd");
		this.logger.debug("due  ===>" + due);
		boolean falseFlag = false;
		try
		{
			String queryString = " FROM fstop.orm.po.ADMHOLIDAY WHERE ADHOLIDAYID = :ADHOLIDAYID ";
			Query<ADMHOLIDAY> query = getSession().createQuery(queryString);
			query.setParameter("ADHOLIDAYID", due);
			List qresult = query.getResultList();
			this.logger.debug(ESAPIUtil.vaildLog("qresult  ===>" + qresult));	
			if(!qresult.isEmpty()){
				falseFlag =  true;
			}else{
				falseFlag =  false;
			}
		}
		catch (Exception e)
		{
//			e.printStackTrace();
			log.error("isAdmholiday error >> {}",e);
		}
		return falseFlag;
	}
	
	/**
	 * 判斷是否為假日
	 * @param date
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public boolean isAdmholiday(Date date)
	{
		
		String due = DateUtil.format("yyyyMMdd", date);
		this.logger.debug("due  ===>" + due);
		boolean falseFlag = false;
		try
		{
			String queryString = " FROM fstop.orm.po.ADMHOLIDAY WHERE ADHOLIDAYID = :ADHOLIDAYID ";
			Query<ADMHOLIDAY> query = getSession().createQuery(queryString);
			query.setParameter("ADHOLIDAYID", due);
			List qresult = query.getResultList();
			this.logger.debug(ESAPIUtil.vaildLog("qresult  ===>" + qresult));	
			if(!qresult.isEmpty()){
				falseFlag =  true;
			}else{
				falseFlag =  false;
			}
		}
		catch (Exception e)
		{
//			e.printStackTrace();
			log.error("isAdmholiday error >> {}",e);
		}
		return falseFlag;
	}

	/**
	 * get ADMHOLIDAY po，查無資料回空物件，須自行檢查物件內 primeKey，有值才代表查詢到資料
	 * 
	 * @param primeKey 日期格式yyyyMMdd
	 * @return
	 */
	public ADMHOLIDAY getByPK(String primeKey) 
	{
		ADMHOLIDAY po = null;
		try
		{
			po = get(ADMHOLIDAY.class, primeKey);
			
			// 查無資料
			if(po == null)
			{
				po = new ADMHOLIDAY();
			}
			log.trace("PK >>> {}", primeKey);
			log.trace("getByPK >>> " + BeanUtils.describe(po));
		}
		catch (Exception e)
		{
			log.error("getByPK error. primeKey >> {}", primeKey, e);
		}
		return po;
	}
	
}
