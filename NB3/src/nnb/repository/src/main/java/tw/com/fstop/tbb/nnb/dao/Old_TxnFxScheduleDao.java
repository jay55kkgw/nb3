package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import fstop.orm.po.OLD_TXNFXSCHEDULE;
import fstop.orm.po.TXNFXSCHPAY;
import tw.com.fstop.spring.dao.OLD_BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;
 
@Component
public class Old_TxnFxScheduleDao extends OLD_BaseHibernateDao<OLD_TXNFXSCHEDULE, Serializable> {
	protected Logger log = LoggerFactory.getLogger(this.getClass());
	
	public List<OLD_TXNFXSCHEDULE> findByUserId(String fxuserid) {
		List qresult = null;
		String queryString = " FROM fstop.orm.po.OLD_TXNFXSCHEDULE WHERE FXUSERID = :fxuserid ";

		try {
			log.trace(ESAPIUtil.vaildLog("fxuserid {}"+ fxuserid));
			Query query = getSession().createQuery(queryString);
			query.setParameter("fxuserid", fxuserid);
			// Stored XSS
			qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			log.debug(ESAPIUtil.vaildLog("qresult>>"+qresult));
			qresult = ESAPIUtil.validList(qresult);
			
		} catch (Exception e) {
			log.error("{}", e);
		}

		return qresult;
	}
	
	public List<TXNFXSCHPAY> findById(String fxuserid) {
		List qresult = null;
		String queryString = "SELECT A.FXSCHID, A.ADOPID, A.FXUSERID, A.FXPERMTDATE, "
				+ "CASE WHEN A.FXPERMTDATE IS NOT NULL AND A.FXPERMTDATE <> '' THEN 'C' ELSE 'S' END FXTXTYPE, "
				+ "A.FXFDATE, A.FXTDATE, A.FXWDAC, A.FXWDCURR, A.FXWDAMT, A.FXSVBH, A.FXSVAC, A.FXSVCURR, A.FXSVAMT, "
				+ "A.FXTXMEMO, A.FXTXMAILS, A.FXTXMAILMEMO, A.FXTXCODE, A.FXSDATE, A.FXSTIME, A.FXTXSTATUS, A.XMLCA, A.XMLCN, A.MAC, "
				+ "B.REQINFO FXTXINFO, A.LASTDATE, A.LASTTIME, A.FXSCHNO, A.LOGINTYPE, '' AS MSADDR "
				+ "FROM TXNFXSCHEDULE A LEFT JOIN TXNREQINFO B ON A.FXTXINFO = B.REQID "
				+ "WHERE A.FXUSERID = '" + fxuserid + "' AND ( FXTXSTATUS = '0' OR FXTXSTATUS = '3' OR FXTXSTATUS = '9' ) ";
		try {
			log.trace("fxuserid {}", fxuserid);
			Query query = getSession().createNativeQuery(queryString, TXNFXSCHPAY.class);
			// Stored XSS
			qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			log.debug(ESAPIUtil.vaildLog("qresult>>"+qresult));
			qresult = ESAPIUtil.validList(qresult);
			
		} catch (Exception e) {
			log.error("{}", e);
		}

		return qresult;
	}
	
	public boolean updateByPK(Long fxschid, String fxtxstatus) {
		boolean result = false;
		String queryString = " UPDATE fstop.orm.po.OLD_TXNFXSCHEDULE C SET C.FXTXSTATUS = :fxtxstatus WHERE C.FXSCHID = :fxschid ";

		try {
			log.trace("updateByPK.fxschid: {}", fxschid);
			log.trace("updateByPK.fxtxstatus: {}", fxtxstatus);
			
			Query query = getSession().createQuery(queryString);
			query.setParameter("fxtxstatus", fxtxstatus);
			query.setParameter("fxschid", fxschid);
			query.executeUpdate();
			
			result = true;
			
		} catch (Exception e) {
			log.error("{}", e);
		}

		return result;
	}
}
