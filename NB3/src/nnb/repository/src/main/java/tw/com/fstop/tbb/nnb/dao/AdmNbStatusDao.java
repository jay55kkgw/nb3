package tw.com.fstop.tbb.nnb.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.query.Query;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.ADMNBSTATUS;
import tw.com.fstop.spring.dao.BaseHibernateDao;

@Transactional
@Component
public class AdmNbStatusDao extends BaseHibernateDao<ADMNBSTATUS,String> {
	protected Logger log = Logger.getLogger(getClass());
	
	@SuppressWarnings("unchecked")
	public ADMNBSTATUS findByID(String Servicename) {

		List<ADMNBSTATUS> result = null;
		try {
			log.trace("Servicename>>"+Servicename);
			String queryString = " FROM ADMNBSTATUS WHERE ADNBSTATUSID =:Servicename";
			Query<ADMNBSTATUS> query = getSession().createQuery(queryString);
			query.setParameter("Servicename", Servicename);
			result = query.list();
			
		} catch (Exception e) {
//			e.printStackTrace();
			log.error("AdmNbStatusDao error >> {}",e);
		}
		return result.get(0);
	}
	
	
	public boolean getTest() {
		boolean result = false;
		try
		{
			List qresult = null;
			String queryString = " FROM ADMNBSTATUS";
			Query query = getSession().createQuery(queryString);
			query.setMaxResults(1); // 一筆即可
			
			// Stored XSS
			qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			
			if (qresult != null && !qresult.isEmpty()) {
				result = true;
			}
		}
		catch (Exception e)
		{
			log.error("", e);
		}
		return result;
		
	}
	
}
