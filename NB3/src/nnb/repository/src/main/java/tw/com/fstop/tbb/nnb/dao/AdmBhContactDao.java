package tw.com.fstop.tbb.nnb.dao;

import java.util.List;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import fstop.orm.po.ADMBHCONTACT;
import tw.com.fstop.spring.dao.BaseHibernateDao;

@Component
public class AdmBhContactDao extends BaseHibernateDao<ADMBHCONTACT, Integer>
{
	Logger log = LoggerFactory.getLogger(this.getClass());

	public List<ADMBHCONTACT> findByBhID(String bhid)
	{
		List<ADMBHCONTACT> result = null;
		try
		{
			String queryString = " FROM fstop.orm.po.ADMBHCONTACT WHERE ADBRANCHID = :bhid ";
			Query<ADMBHCONTACT> query = getSession().createQuery(queryString);
			query.setParameter("bhid", bhid);
			result = query.list();
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("AdmBhContactDao error >> {}",e);
		}
		return result;
	}

	/**
	 * 取得台企銀行的物件
	 * 
	 * @param bankcod 
	 * @param adbranchid 分行代碼
	 * @return
	 */
	public ADMBHCONTACT findByAdbranchid(String adbranchid)
	{
		ADMBHCONTACT po = new ADMBHCONTACT();
		try
		{
			String sql = "FROM fstop.orm.po.ADMBHCONTACT WHERE ADBRANCHID = :adbranchid ";
			Query<ADMBHCONTACT> query = getSession().createQuery(sql);
			query.setParameter("adbranchid", adbranchid);
			po = query.uniqueResult();
		}
		catch (Exception e)
		{
			log.error("找不到分行的名稱({}).", adbranchid, e);
		}
		return po;
	}
}
