package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.ONLINE_APPOINTMENT_TXNLOG;
import fstop.orm.po.TXNTRACCSET;
import fstop.orm.po.TXNUSER;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Transactional
@Component
public class OnlineAppointmentTxnlogDao extends BaseHibernateDao<ONLINE_APPOINTMENT_TXNLOG, Serializable>{
	protected Logger log = LoggerFactory.getLogger(this.getClass());
	
	public ONLINE_APPOINTMENT_TXNLOG findByPK(ONLINE_APPOINTMENT_TXNLOG po){
		String queryString = " FROM fstop.orm.po.ONLINE_APPOINTMENT_TXNLOG WHERE TXNDATE = :txndate AND STAN = :stan AND SRCID = :srcid AND KEYID = :keyid";
		ONLINE_APPOINTMENT_TXNLOG qresult = null;
		try {
			Query<ONLINE_APPOINTMENT_TXNLOG> query = getSession().createQuery(queryString);
			query.setParameter("txndate", po.getPks().getTXNDATE());
			query.setParameter("stan", po.getPks().getSTAN());
			query.setParameter("srcid", po.getPks().getSRCID());
			query.setParameter("keyid", po.getPks().getKEYID());
			qresult = query.uniqueResult();
		
		} catch (Exception e) {
			log.error("",e);
		}	
		return qresult;
	}
	
}
