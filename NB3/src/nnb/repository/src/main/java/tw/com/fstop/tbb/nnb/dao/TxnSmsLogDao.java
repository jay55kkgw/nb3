package tw.com.fstop.tbb.nnb.dao;

import java.util.List;

import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.TXNSMSLOG;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;


@Transactional
@Component
public class TxnSmsLogDao extends BaseHibernateDao<TXNSMSLOG, String> {
	protected Logger logger = Logger.getLogger(getClass());
	

	
	public int findTotalCountByInput(String _ID , String _Date ,String adopid) {
		logger.warn(ESAPIUtil.vaildLog("TxnSmsLogDao findTotalCount ADUSERID:"+_ID +" ADOPID:"+adopid+" LASTDATE:"+_Date));	
		String sql = "SELECT COUNT(*) FROM TXNSMSLOG WHERE ADUSERID=:_ID and LASTDATE=:_Date and STATUSCODE in('1','2','3') and ADOPID=:_adopid";
		Query query = getSession().createQuery(sql);
		query.setParameter("_ID", _ID);
		query.setParameter("_Date", _Date);
		query.setParameter("_adopid", adopid);	
		List result = query.getResultList();
		logger.warn("LEO"+result.get(0));
		if(null==result.get(0))return 0;
		else {
			//result.get(0)的type是long
			return Integer.parseInt(result.get(0).toString());
		}
	}	
	
	
	
}
