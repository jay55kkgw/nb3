package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import fstop.orm.po.TXNFXSCHPAY;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Transactional
@Component
public class TxnFxSchPayDao extends BaseHibernateDao<TXNFXSCHPAY, Serializable> {
	private Logger log = LoggerFactory.getLogger(this.getClass());

	/**
	 * 清除使用者資料
	 * @param dpuserid 使用者統編
	 */
	public void deleteByUserId(String fxuserid) {
		
		String queryString = " DELETE fstop.orm.po.TXNFXSCHPAY WHERE FXUSERID = :fxuserid ";
		
		try {
			Query query = getSession().createQuery(queryString);
			query.setParameter("fxuserid", fxuserid).executeUpdate();
			
		} catch (Exception e) {
			log.error("",e);
		}
	}
	
	/**
	 *	取狀態的欄位(ALL)
	 * @return
	 */
	public List<TXNFXSCHPAY> getFxtxstatus(String cusidn) {
		List<TXNFXSCHPAY> result = null;
		try {
			log.debug("TXNFXSCHPAY_Dao >> getFxtxstatus>{}",cusidn);
			Date d = new Date();
			DateFormat dateformat;
			dateformat = new SimpleDateFormat("yyyyMMdd");
			String date = dateformat.format(d);
			log.trace("today>>>{}",dateformat.format(d));
			
			String queryString = " FROM fstop.orm.po.TXNFXSCHPAY "+
						"WHERE FXUSERID = :cusidn AND (FXTXSTATUS = '0' OR FXTXSTATUS = '3') AND FXTDATE >= :date ORDER BY FXFDATE";
			Query<TXNFXSCHPAY> query = getSession().createQuery(queryString);
			query.setParameter("cusidn", cusidn);
			query.setParameter("date", date);
			result = query.list();
			log.debug("TXNFXSCHPAYDao result size >> {}", result.size());
		} catch (Exception e) {
//			e.printStackTrace();
			log.error("取得 TXNFXSCHPAY Request Info 有誤.", e);
		}
		return result;
	}
	
	/**
	 *	取狀態的欄位(轉入帳號)
	 * @return
	 */
	public List<TXNFXSCHPAY> getAcnFxtxstatus(String cusidn , String fxsvac) {
		List<TXNFXSCHPAY> result = null;
		try {
			log.debug("TXNFXSCHPAY_Dao >> getFxtxstatus>{}",cusidn);
			String queryString = " FROM fstop.orm.po.TXNFXSCHPAY "+
						"WHERE FXUSERID = :cusidn AND FXSVAC = :fxsvac AND (FXTXSTATUS = '0' OR FXTXSTATUS = '1') ORDER BY FXSVAC";
			Query<TXNFXSCHPAY> query = getSession().createQuery(queryString);
			query.setParameter("cusidn", cusidn);
			query.setParameter("fxsvac", fxsvac);
			result = query.list();
			log.debug("TXNFXSCHPAYDao result size >> {}", result.size());
		} catch (Exception e) {
//			e.printStackTrace();
			log.error("取得 TXNFXSCHPAY Request Info 有誤.", e);
		}
		return result;
	}	
	
	/**
	 *	取消轉入帳號相關預約
	 * @return
	 */
	public List<TXNFXSCHPAY> cancelAcn(String cusidn , String dpsvac ,String bank) {
		List<TXNFXSCHPAY> result = null;
		try {
			log.debug("TXNFXSCHPAY_Dao >> getAcnCancel>>{}",cusidn);
			String queryString = " FROM fstop.orm.po.TXNFXSCHPAY "+
						"WHERE DPUSERID = :cusidn AND FXSVAC = :dpsvac AND FXSVBH = :dpsvbh AND (FXTXSTATUS = '0' OR FXTXSTATUS = '1')";
			Query<TXNFXSCHPAY> query = getSession().createQuery(queryString);
			query.setParameter("cusidn", cusidn);
			query.setParameter("dpsvac", dpsvac);
			query.setParameter("dpsvbh", bank);
			result = query.list();
			
			for(int i = 0;i<result.size();i++) {
				TXNFXSCHPAY fxstatus = result.get(i);
				fxstatus.setFXTXSTATUS("3");
				update(fxstatus);
			}
			
			
			log.debug("TXNFXSCHPAYDao result size >> {}", result.size());
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("cancelAcn error >> {}",e);
		}
		return result;
	}
	
	/**
	 *	取狀態的欄位(更新狀態)
	 * @return
	 */
	public TXNFXSCHPAY updataFxtxstatus(String fxschid) {
		TXNFXSCHPAY result = null;
		try {
			log.debug("TXNFXSCHPAY_Dao >> updataDptxstatus");
			String queryString = " FROM fstop.orm.po.TXNFXSCHPAY WHERE FXSCHID = :fxschid";
			Query<TXNFXSCHPAY> query = getSession().createQuery(queryString);
			query.setParameter("fxschid", Integer.parseInt(fxschid));
			result = query.uniqueResult();
			
			result.setFXTXSTATUS("3");
			update(result);
			
		} catch (Exception e) {
//			e.printStackTrace();
			log.error("取得 TXNFXSCHPAY Request Info 有誤.", e);
		}
		return result;
	}

	/**
	 *	取得行事曆外幣預約日期
	 * @return
	 */
	public List<String> getByUser(String cusidn) {
		List qresult = null;
		try
		{
			String sql = "SELECT FXSCHTXDATE,FXWDAC,FXSVBH,FXSVAC,FXWDCURR,FXWDAMT,FXSVCURR,FXSVAMT,FXTXMEMO,ADOPNAME,ADOPENGNAME,ADOPCHSNAME,ADBRANCHID,ADBRANCHNAME,ADBRANENGNAME,ADBRANCHSNAME,FXTXSTATUS FROM\r\n" + 
					"(SELECT * FROM\r\n" + 
					"(SELECT * FROM  (\r\n" + 
					"SELECT FXSCHNO FROM TXNFXSCHPAY \r\n" + 
					"WHERE  FXUSERID = '"+cusidn+"' AND FXTXSTATUS = '0' \r\n" + 
					") A \r\n" + 
					"INNER JOIN ( \r\n" + 
					"SELECT * FROM TXNFXSCHPAYDATA WHERE FXUSERID = '"+cusidn+"' \r\n" + 
					") B  \r\n" + 
					"ON A.FXSCHNO = B. FXSCHNO) X , \r\n" + 
					"(SELECT ADOPID,ADOPNAME,ADOPENGNAME,ADOPCHSNAME FROM NB3SYSOP)  Y\r\n" + 
					"WHERE X.ADOPID = Y.ADOPID) R,\r\n" + 
					"(SELECT ADBRANCHID,ADBRANCHNAME,ADBRANENGNAME,ADBRANCHSNAME FROM ADMBRANCH)  T\r\n" + 
					"WHERE R.FXSVBH = T.ADBRANCHID";
					
			
			Query query = getSession().createNativeQuery(sql);
			
			// Stored XSS
			qresult = query.getResultList();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			qresult = ESAPIUtil.validList(qresult); // 花太多時間在滾LIST迴圈先註解

		}
		catch (Exception e)
		{
			log.error("", e);
		}
		return qresult;
		
	}
	/**
	 *	依照狀態回傳外幣資料
	 * @return
	 */
	public List<TXNFXSCHPAY> getallFxtxstatus(String cusidn) {
		List result = null;		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String today = formatter.format(new Date()).replace("-", "");
		log.debug("today>>>{}",today);
		String queryString = " FROM fstop.orm.po.TXNFXSCHPAY WHERE FXUSERID = :cusidn AND FXFDATE >=  '"+ today+"' AND FXTXSTATUS != '3'" ;
		try {						
			Query<TXNFXSCHPAY> query = getSession().createQuery(queryString);
			query.setParameter("cusidn", cusidn);			
			result = query.getResultList();
			if (null == result)
			{
				result = new ArrayList();
			}
			result = ESAPIUtil.validList(result);
		} catch (Exception e) {
			log.error("E>>>>{}", e);
		}
		return result;
	}	
	/**
	 *	依照流水號 電文 統編取消外幣預約
	 * @return
	 */
	public void cancelByCusidn(String fxuserid ,String fxschid) {	
		String queryString = " update fstop.orm.po.TXNFXSCHPAY SET fxtxstatus='3' WHERE FXSCHID = :fxschid and FXUSERID = :fxuserid ";
		try {			
			Query query = getSession().createQuery(queryString);
			query.setParameter("fxschid", Integer.valueOf(fxschid));
			query.setParameter("fxuserid", fxuserid);
			query.executeUpdate();			
		} catch (Exception e) {
//			e.printStackTrace();
			log.error("e>>>{}", e);
		}		
	}
	
	/**
	 * 清除使用者資料
	 * @param dpuserid 使用者統編
	 */
	public Boolean fxschnoIsNull(String fxschno) {
		List result = null;		
		String queryString = " FROM fstop.orm.po.TXNFXSCHPAY WHERE FXSCHNO=:fxschno";
		
		try {
			Query query = getSession().createQuery(queryString);
			query.setParameter("fxschno", fxschno);
			result = query.getResultList();
			if (result.size()==0)			
				return false;
			
			else return true;
		
		} catch (Exception e) {
			log.error("",e);
		}
		return null;
	}
	
	
	
	/**
	 * 取得外幣預約編號清單 by user
	 * 
	 * @return
	 */
	public List<String> getFxschNoByUser(String cusidn) {
		List<String> qresult = null;
		try {
			
			StringBuffer bfs = new StringBuffer();
			bfs.append(" SELECT FXSCHNO FROM TXNFXSCHPAY ");
			bfs.append(" WHERE FXUSERID = '" + cusidn + "' ");

			Query query = getSession().createNativeQuery(bfs.toString());
			// Stored XSS
			qresult = query.getResultList();
			if (null == qresult) {
				qresult = new ArrayList<String>();
			}
			// qresult = ESAPIUtil.validList(qresult); // 花太多時間在滾LIST迴圈先註解

		} catch (Exception e) {
			log.error("", e);
		}
		return qresult;

	}
	
	
}