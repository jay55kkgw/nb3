package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.OLD_TXNUSER;
import tw.com.fstop.spring.dao.OLD_BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Component
public class Old_TxnUserDao extends OLD_BaseHibernateDao<OLD_TXNUSER, Serializable>
{
	protected Logger log = LoggerFactory.getLogger(this.getClass());

	
	
	public List<OLD_TXNUSER> findByUserId(String dpuserid)
	{
		List qresult = null;
		String queryString = " FROM fstop.orm.po.OLD_TXNUSER WHERE DPSUERID = :dpuserid ";
		// query.setMaxResults(100);
		try
		{
			log.trace(ESAPIUtil.vaildLog("dpuserid {}"+ dpuserid));
			Query query = getSession().createQuery(queryString);
			query.setParameter("dpuserid", dpuserid);
			// Stored XSS
			qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			log.debug(ESAPIUtil.vaildLog("qresult>>{}"+qresult));
			qresult = ESAPIUtil.validList(qresult);
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			//e.printStackTrace();
			log.error("findByUserId ERROR>>", e);
		}

		return qresult;
	}

	@Override
	public List<OLD_TXNUSER> getAll()
	{
		List<OLD_TXNUSER> result = null;
		// 注意:舊語法已不能使用 要使用fstop.orm.po.xxx PO 要加@Entity
		String queryString = " FROM fstop.orm.po.OLD_TXNUSER ORDER BY DPSUERID";
		// String queryString = " FROM TXNADDRESSBOOK WHERE DPUSERID = :dpuserid ";
		try
		{
			Query<OLD_TXNUSER> query = getSession().createQuery(queryString);
			result = query.list();
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			//e.printStackTrace();
			log.error("getAll ERROR>>", e);
		}

		return result;
		// return find("FROM OLD_TXNUSER ORDER BY DPSUERID");
	}

	public List<OLD_TXNUSER> getEmailUser()
	{
		List<OLD_TXNUSER> result = null;
		// 注意:舊語法已不能使用 要使用fstop.orm.po.xxx PO 要加@Entity
		String queryString = " FROM fstop.orm.po.OLD_TXNUSER WHERE DPMYEMAIL <> '' ORDER BY DPSUERID ";

		// String queryString = " FROM TXNADDRESSBOOK WHERE DPUSERID = :dpuserid ";
		try
		{
			Query<OLD_TXNUSER> query = getSession().createQuery(queryString);
			result = query.list();
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			//e.printStackTrace();
			log.error("getEmailUser ERROR>>", e);
		}

		return result;
		// return find("FROM OLD_TXNUSER WHERE DPMYEMAIL <> '' ORDER BY DPSUERID");
	}

	public List<OLD_TXNUSER> getBillUser()
	{
		List<OLD_TXNUSER> result = null;
		// 注意:舊語法已不能使用 要使用fstop.orm.po.xxx PO 要加@Entity
		String queryString = " FROM fstop.orm.po.OLD_TXNUSER WHERE DPMYEMAIL <> '' AND (DPBILL <> '' OR DPFUNDBILL <> '' OR DPCARDBILL <> '') ORDER BY DPSUERID";
		// String queryString = " FROM TXNADDRESSBOOK WHERE DPUSERID = :dpuserid ";
		try
		{
			Query<OLD_TXNUSER> query = getSession().createQuery(queryString);
			result = query.list();
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			//e.printStackTrace();
			log.error("getBillUser ERROR>>", e);
		}

		return result;
		// return find("FROM OLD_TXNUSER WHERE DPMYEMAIL <> '' AND (DPBILL <> '' OR DPFUNDBILL <> '' OR DPCARDBILL <> '')
		// ORDER BY DPSUERID");
	}

	public List<OLD_TXNUSER> getAllFundBillUser()
	{
		List<OLD_TXNUSER> result = null;
		// 注意:舊語法已不能使用 要使用fstop.orm.po.xxx PO 要加@Entity
		String queryString = " FROM fstop.orm.po.OLD_TXNUSER WHERE DPFUNDBILL <> '' and FUNDBILLDATE <> '' and LENGTH(DPMYEMAIL) > 0 ORDER BY DPSUERID";
		// String queryString = " FROM TXNADDRESSBOOK WHERE DPUSERID = :dpuserid ";
		try
		{
			Query<OLD_TXNUSER> query = getSession().createQuery(queryString);
			result = query.list();
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			//e.printStackTrace();
			log.error("getAllFundBillUser ERROR>>", e);
		}

		return result;
		// return find("FROM OLD_TXNUSER WHERE DPFUNDBILL <> '' and FUNDBILLDATE <> '' and LENGTH(DPMYEMAIL) > 0 ORDER BY
		// DPSUERID");
	}

	public List<OLD_TXNUSER> getAllFundStopNotifyUser()
	{
		List<OLD_TXNUSER> result = null;
		// 注意:舊語法已不能使用 要使用fstop.orm.po.xxx PO 要加@Entity
		String queryString = " FROM fstop.orm.po.OLD_TXNUSER WHERE DPNOTIFY like '%14%' and LENGTH(DPMYEMAIL) > 0 ORDER BY DPSUERID";
		try
		{
			Query<OLD_TXNUSER> query = getSession().createQuery(queryString);
			result = query.list();
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			//e.printStackTrace();
			log.error("getAllFundStopNotifyUser ERROR>>", e);
		}

		return result;
		// return find("FROM OLD_TXNUSER WHERE DPNOTIFY like '%14%' and LENGTH(DPMYEMAIL) > 0 ORDER BY DPSUERID");
	}

	public List<OLD_TXNUSER> getAllBillUserLast2Day(String tomorrow, String yesterday)
	{
		/*
		 * return find("FROM OLD_TXNUSER WHERE ((BILLDATE < '" + tomorrow + "' and BILLDATE >= '" + yesterday + "') " +
		 * " or (FUNDBILLDATE < '" + tomorrow + "' and FUNDBILLDATE >= '" + yesterday + "') " + " or (CARDBILLDATE < '"
		 * + tomorrow + "' and CARDBILLDATE >= '" + yesterday + "')) " + " ORDER BY DPSUERID");
		 */
		String subyesterday = yesterday.substring(0, 7);
		List<OLD_TXNUSER> result = null;
		// 注意:舊語法已不能使用 要使用fstop.orm.po.xxx PO 要加@Entity
		String queryString = " FROM fstop.orm.po.OLD_TXNUSER WHERE ((BILLDATE like '" + subyesterday + "%') "
				+ " or (FUNDBILLDATE like '" + subyesterday + "%') " + " or (CARDBILLDATE like '" + subyesterday
				+ "%')) " + " ORDER BY DPSUERID";
		// String queryString = " FROM TXNADDRESSBOOK WHERE DPUSERID = :dpuserid ";
		try
		{
			Query<OLD_TXNUSER> query = getSession().createQuery(queryString);
			result = query.list();
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			//e.printStackTrace();
			log.error("getAllBillUserLast2Day ERROR>>", e);
		}

		return result;
		// return find("FROM OLD_TXNUSER WHERE ((BILLDATE like '" + subyesterday + "%') " +
		// " or (FUNDBILLDATE like '" + subyesterday + "%') " +
		// " or (CARDBILLDATE like '" + subyesterday + "%')) " +
		// " ORDER BY DPSUERID");
	}

	@SuppressWarnings({ "unused", "unchecked" })
	public boolean checkFundAgreeVersion(String version, String uid)
	{
		OLD_TXNUSER user = get(OLD_TXNUSER.class, uid);

		if (version.equals(user.getFUNDAGREEVERSION()))
			return true;
		else return false;
	}

	public OLD_TXNUSER getNotifyById(String cusidn)
	{
		OLD_TXNUSER result = null;
		String queryString = "FROM fstop.orm.po.OLD_TXNUSER WHERE DPSUERID = :cusidn ";
		// String queryString = " FROM TXNADDRESSBOOK WHERE DPUSERID = :dpuserid ";
		try
		{
			log.trace("cusidn {}", cusidn);
			Query<OLD_TXNUSER> query = getSession().createQuery(queryString);
			query.setParameter("cusidn", cusidn);

			result = query.getSingleResult();
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			//e.printStackTrace();
			log.error("getNotifyById ERROR>>", e);
		}

		return result;

	}

	public OLD_TXNUSER chkRecord(String DPSUERID, String DPUSERNAME)
	{
		String dfNotify = "16,17,18,19";// 黃金存摺相關的要預設啟用通知
		OLD_TXNUSER user = null;
		int isUserExist = -1;
		try
		{
			user = get(OLD_TXNUSER.class, DPSUERID);
//			log.trace("chkRecord.DPSUERID: {}", user.getDPSUERID());
			log.trace("chkRecord.user: {}", user);
			isUserExist = ((user == null) ? 0 : 1);
		}
		catch (Exception e)
		{
			isUserExist = 0;
		}
		
		if (isUserExist == 0)
		{
			OLD_TXNUSER u = new OLD_TXNUSER();
			u.setDPSUERID(DPSUERID);
			u.setSCHCOUNT(new Integer(0));
			u.setASKERRTIMES(new Integer(0));
			u.setDPUSERNAME(DPUSERNAME);
			u.setDPNOTIFY(dfNotify);// 黃金存摺相關的要預設啟用通知
			save(u);
			log.trace("chkRecord.0.DPSUERID: {}", u.getDPSUERID());
			return u;
		}
		else if (isUserExist == 1)
		{
			user.setDPUSERNAME(DPUSERNAME);
			save(user);
			log.trace("chkRecord.1.DPSUERID: {}", user.getDPSUERID());
			return user;
		}

		return null;
	}

	/**
	 * 信用卡、金融卡申請網銀，如果使用者不在時建立一個使用者。
	 * 
	 * @param DPSUERID
	 * @return
	 */
	@SuppressWarnings({ "unused", "unchecked" })
	public OLD_TXNUSER chkRecord(String DPSUERID)
	{

		OLD_TXNUSER user = null;
		int isUserExist = -1;
		try
		{
			// user = super.get(StrUtils.trim(DPSUERID));
			List<OLD_TXNUSER> OLD_TXNUSERs = findByUserId(DPSUERID);
			if (OLD_TXNUSERs.isEmpty())
			{
				return null;
			}
			else
			{
				user = OLD_TXNUSERs.get(0);
			}
			// System.out.println("" + user.getDPSUERID());
			isUserExist = ((user == null) ? 0 : 1);
		}
		catch (Exception e)
		{
			isUserExist = 0;
		}
		if (isUserExist == 0)
		{
			OLD_TXNUSER u = new OLD_TXNUSER();
			u.setDPSUERID(DPSUERID);
			u.setSCHCOUNT(new Integer(0));
			u.setASKERRTIMES(new Integer(0));
			u.setCCARDFLAG("0");

			save(u);
			return u;
		}
		else if (isUserExist == 1)
		{
			return user;
		}

		return null;
	}

	public List<String> getAllUserEmailNotEmpty()
	{

		List<String> result = null;
		// 注意:舊語法已不能使用 要使用fstop.orm.po.xxx PO 要加@Entity
		String queryString = "SELECT DPMYEMAIL FROM OLD_TXNUSER  WHERE LENGTH(DPMYEMAIL) > 0 ORDER BY DPSUERID";
		// Query query = getQuery("SELECT DPMYEMAIL FROM OLD_TXNUSER WHERE LENGTH(DPMYEMAIL) > 0 ORDER BY DPSUERID");
		//
		// return query.list();
		try
		{
			Query<String> query = getSession().createQuery(queryString);
			result = query.list();
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			//e.printStackTrace();
			log.error("getAllUserEmailNotEmpty ERROR>>", e);
		}

		return result;
	}

	public List<String> getAllUserEmailNotEmpty1()
	{
		List<String> result = null;
		// 注意:舊語法已不能使用 要使用fstop.orm.po.xxx PO 要加@Entity
		String queryString = "SELECT DPSUERID FROM OLD_TXNUSER  WHERE LENGTH(DPMYEMAIL) > 0 ORDER BY DPSUERID";
		// Query query = getQuery("SELECT DPMYEMAIL FROM OLD_TXNUSER WHERE LENGTH(DPMYEMAIL) > 0 ORDER BY DPSUERID");
		//
		// return query.list();
		try
		{
			Query<String> query = getSession().createQuery(queryString);
			result = query.list();
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			//e.printStackTrace();
			log.error("getAllUserEmailNotEmpty1 ERROR>>", e);
		}

		return result;

		// Query query = getQuery("SELECT DPSUERID FROM OLD_TXNUSER WHERE LENGTH(DPMYEMAIL) > 0 ORDER BY DPSUERID");
		//
		// return query.list();
	}

	public String getAllUserEmailNotEmpty2(String dpuserid)
	{
		String result = null;
		String queryString = " FROM fstop.orm.po.OLD_TXNUSER WHERE DPUSERID = :dpuserid ";
		// query.setMaxResults(100);
		try
		{
			log.trace("dpuserid {}", dpuserid);
			Query<OLD_TXNUSER> query = getSession().createQuery(queryString);
			query.setParameter("dpuserid", dpuserid);
			result = query.list().get(0).toString();
			// Query query = getQuery("SELECT DPMYEMAIL FROM OLD_TXNUSER WHERE DPSUERID = ?");
			// query.setParameter(0, cusidn);
			// List qresult = query.list();
			// return qresult.get(0).toString();
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			//e.printStackTrace();
			log.error("getAllUserEmailNotEmpty2 ERROR>>", e);
		}

		return result;
	}

	public List<String> getAllUserEmailApplied()
	{
		List<String> result = null;
		// 注意:舊語法已不能使用 要使用fstop.orm.po.xxx PO 要加@Entity
		String queryString = "SELECT DPMYEMAIL FROM TXNUSER  WHERE LENGTH(DPMYEMAIL) > 0 and DPNOTIFY like '%15%' ORDER BY DPSUERID";
		// Query query = getQuery("SELECT DPMYEMAIL FROM TXNUSER WHERE LENGTH(DPMYEMAIL) > 0 and DPNOTIFY like '%15%'
		// ORDER BY DPSUERID");
		//
		// return query.list();
		try
		{
			Query<String> query = getSession().createQuery(queryString);
			result = query.list();
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			//e.printStackTrace();
			log.error("getAllUserEmailApplied ERROR>>", e);
		}

		return result;

	}

	@Override
	public <T> T get(Class<T> entityClass, Serializable id) throws DataAccessException
	{
		// TODO Auto-generated method stub
		return super.get(entityClass, id);
	}

	// @Transactional(propagation = Propagation.REQUIRES_NEW)
	// public void updateSchcount(String DPSUERID) {
	//
	// SQLQuery add1=this.getSession().createSQLQuery("UPDATE TXNUSER SET SCHCOUNT = SCHCOUNT + 1 WHERE DPSUERID = ?");
	//
	// add1.setString(0, DPSUERID);
	//
	// int r2 = add1.executeUpdate();
	// }
	//
	// @Transactional(propagation = Propagation.REQUIRES_NEW)
	// public int incSchcount(String DPSUERID) {
	//
	// //判斷是否換日
	// SQLQuery add0 = getSession().createSQLQuery("SELECT SCHCNTDATE FROM TXNUSER WHERE DPSUERID = ?");
	// add0.setString(0, DPSUERID);
	// List result0 = add0.list();
	//
	// String schcntdate = (String) result0.get(0) == null ? "" : (String) result0.get(0); //最後拿編號日期
	// String sysdate = DateTimeUtils.format("yyyyMMdd", new Date());
	//
	// SQLQuery add1 = null;
	//
	// if(!schcntdate.equals(sysdate)){
	// add1= getSession().createSQLQuery("UPDATE TXNUSER SET SCHCOUNT = 1, SCHCNTDATE = ? WHERE DPSUERID = ?");
	// add1.setString(0, sysdate);
	// add1.setString(1, DPSUERID);
	// } else {
	// add1= getSession().createSQLQuery("UPDATE TXNUSER SET SCHCOUNT = SCHCOUNT + 1 WHERE DPSUERID = ?");
	// add1.setString(0, DPSUERID);
	// }
	//
	// int r2 = add1.executeUpdate();
	//
	// SQLQuery add2= getSession().createSQLQuery("SELECT SCHCOUNT FROM TXNUSER WHERE DPSUERID = ? ");
	// add2.setString(0, DPSUERID);
	// add2.addScalar( "SCHCOUNT", Hibernate.INTEGER);
	// List result = add2.list();
	//
	// return (Integer)result.get(0);
	// }
	//
	// @Transactional(propagation = Propagation.REQUIRES_NEW)
	// public int updateAdBranchID(String DPSUERID,String ADBRANCHID) {
	//
	// SQLQuery add1=this.getSession().createSQLQuery("UPDATE TXNUSER SET ADBRANCHID = ? WHERE DPSUERID = ?");
	//
	// add1.setString(0, ADBRANCHID);
	// add1.setString(1, DPSUERID);
	//
	// int r2 = add1.executeUpdate();
	// return(r2);
	// }
	//
	// @Transactional(propagation = Propagation.REQUIRES_NEW)
	// public int updateFundAgreeVersion(String DPSUERID, String VERSION) {
	//
	// SQLQuery add1=this.getSession().createSQLQuery("UPDATE TXNUSER SET FUNDAGREEVERSION = ? WHERE DPSUERID = ?");
	//
	// add1.setString(0, VERSION);
	// add1.setString(1, DPSUERID);
	//
	// int r2 = add1.executeUpdate();
	// return(r2);
	// }
	//
	// /**
	// * 信用卡、金融卡申請網銀，如果使用者不在時建立一個使用者。
	// * @param DPSUERID
	// * @return
	// */
	// @SuppressWarnings({ "unused", "unchecked" })
	// public TXNUSER chkRecord(String DPSUERID) {
	//
	// TXNUSER user = null;
	// int isUserExist = -1;
	// try {
	// user = super.get(StrUtils.trim(DPSUERID));
	//// System.out.println("" + user.getDPSUERID());
	// isUserExist = ((user == null) ? 0 : 1);
	// }
	// catch(Exception e) {
	// isUserExist = 0;
	// }
	// if(isUserExist == 0 ) {
	// TXNUSER u = new TXNUSER();
	// u.setDPSUERID(DPSUERID);
	// u.setSCHCOUNT(new Integer(0));
	// u.setASKERRTIMES(new Integer(0));
	// u.setCCARDFLAG("0");
	//
	// save(u);
	// return u;
	// }
	// else if(isUserExist == 1) {
	// return user;
	// }
	//
	// return null;
	// }
	// @Transactional(propagation = Propagation.REQUIRES_NEW)
	// public int updateDPMYEMAIL(String DPSUERID, String EMAIL) {
	// Date d = new Date();
	// String date = DateTimeUtils.getCDateShort(d);
	// String time = DateTimeUtils.getTimeShort(d);
	//
	// SQLQuery add1=this.getSession().createSQLQuery("UPDATE TXNUSER SET DPMYEMAIL = ? ,EMAILDATE = ? WHERE DPSUERID =
	// ?");
	//
	// add1.setString(0, EMAIL);
	// add1.setString(1, date + time);
	// add1.setString(2, DPSUERID);
	// int r2 = add1.executeUpdate();
	// return(r2);
	// }
}