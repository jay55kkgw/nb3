package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import fstop.orm.po.ADMBANK;
import fstop.orm.po.ADMCOUNTRY;
import fstop.orm.po.ADMMSGCODE;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Component
public class AdmMsgCodeDao extends BaseHibernateDao<ADMMSGCODE,Serializable> {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	/**
	 * 確認是否有該訊息
	 * @param msgCode
	 * @return
	 */
	public Boolean hasMsg(String msgCode ,String locale) {
		Boolean hasMsg = Boolean.FALSE;
		Locale currentLocale = LocaleContextHolder.getLocale();
		log.debug("errorMsg.locale >> {}" , currentLocale);
		try {
			ADMMSGCODE po = get(ADMMSGCODE.class, msgCode);
			if(po!=null) {
				hasMsg = Boolean.TRUE;
			}
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("hasMsg error >> {}",e);
		}
		log.trace("hasMsg>>{}",hasMsg);
		return hasMsg;
		
	}
	
	/*
	 * BEN 取得錯誤訊息from DB temp 
	 */
	public String errorMsg(String msgCode) {
		String errorMsg = "";
		Locale currentLocale = LocaleContextHolder.getLocale();
		log.debug("errorMsg.locale >> {}" , currentLocale);
		String locale = currentLocale.toString();
		try {
			log.trace("msgCode >> {}" , msgCode);
			ADMMSGCODE po = get(ADMMSGCODE.class,msgCode);
			switch (locale) {
				case "en":
					errorMsg = po.getADMSGOUTENG();
					break;
				case "zh_TW":
					errorMsg = po.getADMSGOUT();
					break;
				case "zh_CN":
					errorMsg = po.getADMSGOUTCHS();
					break;
			}
			//errorMsg = po.getADMSGOUT();
		} catch (DataAccessException e) {
			log.error("errorMsg error", e);
		}
		return errorMsg;
		
	}
	/*
	 * BEN 取得錯誤訊息from DB temp 
	 */
	public String getCodeMsg(String msgCode) {
		String errorMsg = "";
		Locale currentLocale = LocaleContextHolder.getLocale();
		log.debug("errorMsg.locale >> {}" , currentLocale);
		try {
			log.trace(ESAPIUtil.vaildLog("msgCode >> " + msgCode));
			ADMMSGCODE po = get(ADMMSGCODE.class,msgCode);
			String locale = currentLocale.toString();
			switch (locale) {
				case "en":
					errorMsg = po.getADMSGOUTENG();
					break;
				case "zh_TW":
					errorMsg = po.getADMSGOUT();
					break;
				case "zh_CN":
					errorMsg = po.getADMSGOUTCHS();
					break;
			}
			//errorMsg = po.getADMSGIN();
		} catch (DataAccessException e) {
			log.error("errorMsg error", e);
		} catch (Throwable ex){
			log.warn(ESAPIUtil.vaildLog("找不到錯誤代碼!!"+msgCode));
		}
		return errorMsg;
	}
	
	/**
	 * 錯誤代碼頁使用
	 */
	public List<ADMMSGCODE> getMsg() {
		List qresult = null;
		try {
			//String queryString = " FROM fstop.orm.po.ADMMSGCODE ORDER BY ADMCODE";
			String queryString = " FROM fstop.orm.po.ADMMSGCODE WHERE substr(ADMCODE,1,1) in ('0','1','2','3','4','5','6','7','8','9') ORDER BY ADMCODE";
			Query query = getSession().createQuery(queryString);
			//log.trace("query {}",query);
			qresult = query.list();
			//log.trace("qresult {}",qresult);
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			qresult = ESAPIUtil.validList(qresult);
			//qresult = (List) qresult.stream().filter( e -> Character.isDigit(((ADMMSGCODE)e).getADMCODE().charAt(0))).collect(Collectors.toList());
		} catch (DataAccessException e) {
			log.error("errorMsg error", e);
		}
		return qresult;
	}
	
	
}
