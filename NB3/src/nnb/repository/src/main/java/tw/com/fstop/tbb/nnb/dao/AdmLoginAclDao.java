package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.ADMLOGINACL;
import tw.com.fstop.spring.dao.BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Transactional
@Component
public class AdmLoginAclDao extends BaseHibernateDao<ADMLOGINACL, Serializable> {
	protected Logger log = LoggerFactory.getLogger(this.getClass());

	public List<ADMLOGINACL> findByUserId(String aduserid) {
		List result = null;
		String queryString = " FROM fstop.orm.po.ADMLOGINACL WHERE ADUSERID = :aduserid ";
		try {
			log.trace(ESAPIUtil.vaildLog("AdmLoginAclDao.findByUserId.aduserid: {}"+ aduserid));
			Query<ADMLOGINACL> query = getSession().createQuery(queryString);
			query.setParameter("aduserid", aduserid);
			result = query.list();
		} catch (Exception e) {
			log.error("",e);
		}
		return result;
	}
	
}