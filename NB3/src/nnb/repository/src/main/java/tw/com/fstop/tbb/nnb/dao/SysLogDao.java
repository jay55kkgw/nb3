package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.SYSLOG;
import tw.com.fstop.spring.dao.BaseHibernateDao;

@Transactional
@Component
public class SysLogDao extends BaseHibernateDao<SYSLOG, Serializable> {

}
