package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fstop.orm.po.SYSDAILYSEQ;
import tw.com.fstop.spring.dao.BaseHibernateDao;

@Transactional
@Component
public class SysDailySeqDao extends BaseHibernateDao<SYSDAILYSEQ, Serializable> {
	Logger log = LoggerFactory.getLogger(this.getClass());

	/**
	 * 取財金金融帳戶檢核交易序號<BR>
	 */
	public String getFistATMSEQ() {
		
		String atmseq =  String.format("%07d",dailySeq("N857"));
		log.debug("getFistATMSEQ >>>{}", atmseq);
		
		return atmseq;
	}

	/**
	 * 取得 送中心主機所需的序號
	 * 
	 * @param appid
	 * @return
	 */
	public int dailySeq(String appid) {
		log.debug("dailySeq.start ...");
		Query<SYSDAILYSEQ> query = getSession().createQuery("FROM fstop.orm.po.SYSDAILYSEQ WHERE ADSEQID = :ADSEQID ");
		query.setParameter("ADSEQID", appid);
		List<SYSDAILYSEQ> obj = query.getResultList();
		log.debug("r >>{}", obj.size() );
		
		int vseq = 1;
		
		if (obj.size() == 0) {
			SYSDAILYSEQ seq = new SYSDAILYSEQ();
			seq.setADSEQID(appid);
			seq.setADSEQMEMO("");
			seq.setADSEQ(1);
			save(seq);

		} else {
			vseq = ((SYSDAILYSEQ) obj.get(0)).getADSEQ() +1;
			log.debug("dailySeq.executeUpdate ...");
			Query<SYSDAILYSEQ> add1 = getSession().createQuery("UPDATE fstop.orm.po.SYSDAILYSEQ SET ADSEQ=:adseq WHERE ADSEQID = :adseqid ");
			add1.setParameter("adseq", vseq);
			add1.setParameter("adseqid", appid);
			int r1 = add1.executeUpdate();
		}
		log.debug("dailySeq.end ...");
		
		return vseq;
		
	}
	
}
