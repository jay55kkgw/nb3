package tw.com.fstop.spring.dao;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Resource;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.GenericTypeResolver;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Transactional(
        readOnly = false,
        rollbackFor = {Throwable.class}
)
public class BaseHibernateDao<T, PK extends Serializable> extends HibernateTemplate{
	
	@Resource
	@Qualifier("nb3_sessionFactory")
	private SessionFactory sessionFactory;
	
	private Class<T> daoType;

	@SuppressWarnings("unchecked")
	public Class<T> getDaoType()
	{
		if (daoType == null)
		{
			Class<T> [] t = (Class<T>[]) GenericTypeResolver.resolveTypeArguments(getClass(), BaseHibernateDao.class);
			daoType = t[0];
		}
		return daoType;
	}
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public Session getSession()
	{
		return sessionFactory.getCurrentSession();
	}
		
	public void remove(T obj)
	{
		getSession().delete(obj);
	}
	
	
	
	@Override
	public Serializable save(Object entity) throws DataAccessException {
		// TODO Auto-generated method stub
		return super.save(entity);
	}

	@Override
	public Serializable save(String entityName, Object entity) throws DataAccessException {
		// TODO Auto-generated method stub
		return super.save(entityName, entity);
	}

	@Override
	public void saveOrUpdate(Object entity) throws DataAccessException {
		// TODO Auto-generated method stub
		super.saveOrUpdate(entity);
	}

	@Override
	public void saveOrUpdate(String entityName, Object entity) throws DataAccessException {
		// TODO Auto-generated method stub
		super.saveOrUpdate(entityName, entity);
	}

	public List<T> getAll()
	{
		Class<T> t = getDaoType();
		CriteriaBuilder builder = getSession().getCriteriaBuilder();		 
		CriteriaQuery<T> criteria = builder.createQuery(t);
		Root<T> root = criteria.from(t);
		criteria.select(root);
		//Query query = getSession().createQuery(criteria);
		//List<T> list = query.getResultList();
		List<T> list = (List<T>) ESAPIUtil.validList(getSession().createQuery(criteria).getResultList());
		return list;
	}


	@SuppressWarnings("rawtypes")
	public List getBySql(String sql)
	{
	    Query query = getSession().createNativeQuery(sql);
	    return query.getResultList();
	}	
	
	
	public List<T> getAllOrderByCol(String col)
	{
		// Dao Class
		Class<T> t = getDaoType();
		// Define the CriteriaQuery
		CriteriaBuilder builder = getSession().getCriteriaBuilder();		 
		CriteriaQuery<T> criteria = builder.createQuery(t);
		Root<T> root = criteria.from(t);
		criteria.orderBy((builder.asc(root.get(col))));

		// Execute query with pagination
		List<T> list = (List<T>) ESAPIUtil.validList(getSession().createQuery(criteria).getResultList());
		
		return list;
	}

	@Override
	public void update(Object entity) throws DataAccessException {
		// TODO Auto-generated method stub
		super.update(entity);
	}

	@Override
	public Object get(String entityName, Serializable id) throws DataAccessException {
		// TODO Auto-generated method stub
		return super.get(entityName, id);
	}

	@Override
	public <T> T get(Class<T> entityClass, Serializable id) throws DataAccessException {
		// TODO Auto-generated method stub
		return super.get(entityClass, id);
	}
	
	
	
}
