package tw.com.fstop.tbb.nnb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import fstop.orm.po.OLD_NB3USER;
import tw.com.fstop.spring.dao.OLD_BaseHibernateDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Component
public class Old_Nb3UserDao extends OLD_BaseHibernateDao<OLD_NB3USER, Serializable>
{
	protected Logger log = LoggerFactory.getLogger(this.getClass());
	
	public List<OLD_NB3USER> findByUserId(String nb3userid)
	{
		List qresult = null;
		String queryString = " FROM fstop.orm.po.OLD_NB3USER WHERE NB3USERID = :nb3userid ";
		// query.setMaxResults(100);
		try
		{
			log.trace(ESAPIUtil.vaildLog("nb3userid {}"+ nb3userid));
			Query query = getSession().createQuery(queryString);
			query.setParameter("nb3userid", nb3userid);
			// Stored XSS
			qresult = query.list();
			if (null == qresult)
			{
				qresult = new ArrayList();
			}
			log.debug(ESAPIUtil.vaildLog("qresult>>{}"+qresult));
			qresult = ESAPIUtil.validList(qresult);
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			//e.printStackTrace();
			log.error("findByUserId ERROR>>", e);
		}

		return qresult;
	}

}