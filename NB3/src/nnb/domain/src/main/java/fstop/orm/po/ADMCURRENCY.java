package fstop.orm.po;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "ADMCURRENCY")
public class ADMCURRENCY implements Serializable {

	@Id
	private String ADCURRENCY;//幣別代號(英文)

	private String ADCCYNO = "";

	private String ADCCYNAME = "";//中文
	
	private String BUY01 = "";
	
	private String SEL01 = "";
	
	private String BOOKRATE = "";
	
	private String MSGCODE = "";

	private String LASTUSER = "";

	private String LASTDATE = "";

	private String LASTTIME = "";
	
	private String ADCCYENGNAME ="";//英文
	
	private String ADCCYCHSNAME ="";//簡體

//	private ADMCOUNTRY ADMCOUNTRY;
//
//	private Set TXNFXRECORDSBYFXSVCURR;
//
//	private Set TXNFXRECORDSBYFXWDCURR;
//
//	private Set TXNFXSCHEDULESBYFXSVCURR;
//
//	private Set TXNFXSCHEDULESBYFXWDCURR;

	public String getADCURRENCY() {
		return ADCURRENCY;
	}

	public void setADCURRENCY(String adcurrency) {
		ADCURRENCY = adcurrency;
	}

	public String getADCCYNO() {
		return ADCCYNO;
	}

	public void setADCCYNO(String adccyno) {
		ADCCYNO = adccyno;
	}

	public String getADCCYNAME() {
		return ADCCYNAME;
	}

	public void setADCCYNAME(String adccyname) {
		ADCCYNAME = adccyname;
	}

	public String getBUY01() {
		return(BUY01);
	}
	
	public void setBUY01(String buy01) {
		BUY01 = buy01;
	}
	
	public String getSEL01() {
		return(SEL01);
	}
	
	public void setSEL01(String sel01) {
		SEL01 = sel01;
	}
	
	public String getBOOKRATE() {
		return(BOOKRATE);
	}
	
	public void setBOOKRATE(String bookrate) {
		BOOKRATE = bookrate;
	}

	public String getMSGCODE() {
		return(MSGCODE);
	}
	
	public void setMSGCODE(String msgcode) {
		MSGCODE = msgcode;
	}

	public String getLASTUSER() {
		return LASTUSER;
	}

	public void setLASTUSER(String lastuser) {
		LASTUSER = lastuser;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}
	
	public String getADCCYENGNAME() {
		return ADCCYENGNAME;
	}

	public String getADCCYCHSNAME() {
		return ADCCYCHSNAME;
	}

	public void setADCCYENGNAME(String aDCCYENGNAME) {
		ADCCYENGNAME = aDCCYENGNAME;
	}

	public void setADCCYCHSNAME(String aDCCYCHSNAME) {
		ADCCYCHSNAME = aDCCYCHSNAME;
	}

	@Override
	public String toString()
	{
		return "ADMCURRENCY [ADCURRENCY=" + ADCURRENCY + ", ADCCYNO=" + ADCCYNO + ", ADCCYNAME=" + ADCCYNAME
				+ ", BUY01=" + BUY01 + ", SEL01=" + SEL01 + ", BOOKRATE=" + BOOKRATE + ", MSGCODE=" + MSGCODE
				+ ", LASTUSER=" + LASTUSER + ", LASTDATE=" + LASTDATE + ", LASTTIME=" + LASTTIME + "]";
	}


//	public ADMCOUNTRY getADMCOUNTRY() {
//		return ADMCOUNTRY;
//	}
//
//	public void setADMCOUNTRY(ADMCOUNTRY admcountry) {
//		ADMCOUNTRY = admcountry;
//	}


//	public Set getTXNFXRECORDSBYFXSVCURR() {
//		return TXNFXRECORDSBYFXSVCURR;
//	}
//
//	public void setTXNFXRECORDSBYFXSVCURR(Set txnfxrecordsbyfxsvcurr) {
//		TXNFXRECORDSBYFXSVCURR = txnfxrecordsbyfxsvcurr;
//	}
//
//	public Set getTXNFXRECORDSBYFXWDCURR() {
//		return TXNFXRECORDSBYFXWDCURR;
//	}
//
//	public void setTXNFXRECORDSBYFXWDCURR(Set txnfxrecordsbyfxwdcurr) {
//		TXNFXRECORDSBYFXWDCURR = txnfxrecordsbyfxwdcurr;
//	}
//
//	public Set getTXNFXSCHEDULESBYFXSVCURR() {
//		return TXNFXSCHEDULESBYFXSVCURR;
//	}
//
//	public void setTXNFXSCHEDULESBYFXSVCURR(Set txnfxschedulesbyfxsvcurr) {
//		TXNFXSCHEDULESBYFXSVCURR = txnfxschedulesbyfxsvcurr;
//	}
//
//	public Set getTXNFXSCHEDULESBYFXWDCURR() {
//		return TXNFXSCHEDULESBYFXWDCURR;
//	}
//
//	public void setTXNFXSCHEDULESBYFXWDCURR(Set txnfxschedulesbyfxwdcurr) {
//		TXNFXSCHEDULESBYFXWDCURR = txnfxschedulesbyfxwdcurr;
//	}

}
