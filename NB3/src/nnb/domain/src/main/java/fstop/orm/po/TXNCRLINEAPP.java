package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="fstop.orm.po.TXNCRLINEAPP")
@Table(name = "TXNCRLINEAPP")
public class TXNCRLINEAPP implements Serializable {

	@Id
	private String CPRIMDATE = "";			//申請日期
	private String CPRIMTIME = "";			//申請時間
	private String CPRIMID = "";			//身分證字號
	private String CPRIMCHNAME = "";		//姓名
	private String CARDNUM = "";			//信用卡卡號
	private String OCRLINE = "";			//目前額度
	private String NCRLINE = "";			//申請調升額度
	private String CRLINETPE = "";			//申請調升期間類別
	private String CRLINEBEGDATE = "";		//申請調升起日
	private String CRLINEENDDATE = "";		//申請調升迄日
	private String UPFILES = "";			//上傳財力件數
	private String CPRIMCELLULANO = "";		//行動電話
	private String IP = "";					//IP
	private String CHANTPE = "";			//申請通路
	private String VERSION1 = "";			//個人資料運用告知同意聲明書-版本別
	private String VERSION2 = "";			//個人網路銀行暨行動銀行服務條款-版本別
	private String CHKID = "";				//身份驗證結果
	private String LASTDATE = "";			//最後變更日期
	private String LASTTIME = "";			//最後變更時間
	private String UPDATER = "";			//更新人員
	
	
	public String getCPRIMDATE() {
		return CPRIMDATE;
	}
	public void setCPRIMDATE(String cPRIMDATE) {
		CPRIMDATE = cPRIMDATE;
	}
	public String getCPRIMTIME() {
		return CPRIMTIME;
	}
	public void setCPRIMTIME(String cPRIMTIME) {
		CPRIMTIME = cPRIMTIME;
	}
	public String getCPRIMID() {
		return CPRIMID;
	}
	public void setCPRIMID(String cPRIMID) {
		CPRIMID = cPRIMID;
	}
	public String getCPRIMCHNAME() {
		return CPRIMCHNAME;
	}
	public void setCPRIMCHNAME(String cPRIMCHNAME) {
		CPRIMCHNAME = cPRIMCHNAME;
	}
	public String getCARDNUM() {
		return CARDNUM;
	}
	public void setCARDNUM(String cARDNUM) {
		CARDNUM = cARDNUM;
	}
	public String getOCRLINE() {
		return OCRLINE;
	}
	public void setOCRLINE(String oCRLINE) {
		OCRLINE = oCRLINE;
	}
	public String getNCRLINE() {
		return NCRLINE;
	}
	public void setNCRLINE(String nCRLINE) {
		NCRLINE = nCRLINE;
	}
	public String getCRLINETPE() {
		return CRLINETPE;
	}
	public void setCRLINETPE(String cRLINETPE) {
		CRLINETPE = cRLINETPE;
	}
	public String getCRLINEBEGDATE() {
		return CRLINEBEGDATE;
	}
	public void setCRLINEBEGDATE(String cRLINEBEGDATE) {
		CRLINEBEGDATE = cRLINEBEGDATE;
	}
	public String getCRLINEENDDATE() {
		return CRLINEENDDATE;
	}
	public void setCRLINEENDDATE(String cRLINEENDDATE) {
		CRLINEENDDATE = cRLINEENDDATE;
	}
	public String getUPFILES() {
		return UPFILES;
	}
	public void setUPFILES(String uPFILES) {
		UPFILES = uPFILES;
	}
	public String getCPRIMCELLULANO() {
		return CPRIMCELLULANO;
	}
	public void setCPRIMCELLULANO(String cPRIMCELLULANO) {
		CPRIMCELLULANO = cPRIMCELLULANO;
	}
	public String getIP() {
		return IP;
	}
	public void setIP(String iP) {
		IP = iP;
	}
	public String getCHANTPE() {
		return CHANTPE;
	}
	public void setCHANTPE(String cHANTPE) {
		CHANTPE = cHANTPE;
	}
	public String getVERSION1() {
		return VERSION1;
	}
	public void setVERSION1(String vERSION1) {
		VERSION1 = vERSION1;
	}
	public String getVERSION2() {
		return VERSION2;
	}
	public void setVERSION2(String vERSION2) {
		VERSION2 = vERSION2;
	}
	public String getCHKID() {
		return CHKID;
	}
	public void setCHKID(String cHKID) {
		CHKID = cHKID;
	}
	public String getLASTDATE() {
		return LASTDATE;
	}
	public void setLASTDATE(String lASTDATE) {
		LASTDATE = lASTDATE;
	}
	public String getLASTTIME() {
		return LASTTIME;
	}
	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}
	public String getUPDATER() {
		return UPDATER;
	}
	public void setUPDATER(String uPDATER) {
		UPDATER = uPDATER;
	}
}