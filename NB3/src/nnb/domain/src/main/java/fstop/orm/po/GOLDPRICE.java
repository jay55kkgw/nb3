package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity(name="fstop.orm.po.GOLDPRICE")
@Table(name = "GOLDPRICE")
public class GOLDPRICE implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8877022068336599762L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String GOLDPRICEID;

	private String QDATE;

	private String QTIME;

	private String BPRICE;

	private String SPRICE;

	private String PRICE1;

	private String PRICE2;

	private String PRICE3;

	private String PRICE4;

	private String LASTDATE;

	private String LASTTIME;

	public String toString() {
		return new ToStringBuilder(this).append("goldpriceid", getGOLDPRICEID())
		.toString();
	}

	public String getGOLDPRICEID() {
		return GOLDPRICEID;
	}

	public void setGOLDPRICEID(String goldpriceid) {
		System.out.println("goldpriceid = " + goldpriceid);
		GOLDPRICEID = goldpriceid;
	}

	public String getQDATE() {
		return QDATE;
	}

	public void setQDATE(String qdate) {
		System.out.println("qdate = " + qdate);
		QDATE = qdate;
	}

	public String getQTIME() {
		return QTIME;
	}

	public void setQTIME(String qtime) {
		System.out.println("qtime = " + qtime);
		QTIME = qtime;
	}

	public String getBPRICE() {
		return BPRICE;
	}

	public void setBPRICE(String bprice) {
		System.out.println("bprice = " + bprice);
		BPRICE = bprice;
	}

	public String getSPRICE() {
		return SPRICE;
	}

	public void setSPRICE(String sprice) {
		System.out.println("sprice = " + sprice);
		SPRICE = sprice;
	}

	public String getPRICE1() {
		return PRICE1;
	}

	public void setPRICE1(String price1) {
		System.out.println("price1 = " + price1);
		PRICE1 = price1;
	}

	public String getPRICE2() {
		return PRICE2;
	}

	public void setPRICE2(String price2) {
		System.out.println("price2 = " + price2);
		PRICE2 = price2;
	}

	public String getPRICE3() {
		return PRICE3;
	}

	public void setPRICE3(String price3) {
		System.out.println("price3 = " + price3);
		PRICE3 = price3;
	}

	public String getPRICE4() {
		return PRICE4;
	}

	public void setPRICE4(String price4) {
		System.out.println("price4 = " + price4);
		PRICE4 = price4;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lastdate) {
		System.out.println("lastdate = " + lastdate);
		LASTDATE = lastdate;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lasttime) {
		System.out.println("lasttime = " + lasttime);
		LASTTIME = lasttime;
	}

}
