package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="fstop.orm.po.OLD_TXNADDRESSBOOK")
@Table(name = "TXNADDRESSBOOK")
public class OLD_TXNADDRESSBOOK implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 4147166971760046470L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer DPADDBKID;

	private String DPUSERID;

	private String DPGONAME;

	private String DPABMAIL;

	private String LASTDATE;

	private String LASTTIME;

	public Integer getDPADDBKID() {
		return DPADDBKID;
	}

	public void setDPADDBKID(Integer dPADDBKID) {
		DPADDBKID = dPADDBKID;
	}

	public String getDPUSERID() {
		return DPUSERID;
	}

	public void setDPUSERID(String dPUSERID) {
		DPUSERID = dPUSERID;
	}

	public String getDPGONAME() {
		return DPGONAME;
	}

	public void setDPGONAME(String dPGONAME) {
		DPGONAME = dPGONAME;
	}

	public String getDPABMAIL() {
		return DPABMAIL;
	}

	public void setDPABMAIL(String dPABMAIL) {
		DPABMAIL = dPABMAIL;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lASTDATE) {
		LASTDATE = lASTDATE;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}
	
	

}
