package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class ONLINE_APPOINTMENT_TXNLOG_PK implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7374212399011924081L;
	private String TXNDATE = "";//交易日期
	private String STAN = "";//平台交易序號
	private String SRCID = "";//參加單位代號
	private String KEYID = "";//機碼代號
	public String getTXNDATE() {
		return TXNDATE;
	}
	public void setTXNDATE(String tXNDATE) {
		TXNDATE = tXNDATE;
	}
	public String getSTAN() {
		return STAN;
	}
	public void setSTAN(String sTAN) {
		STAN = sTAN;
	}
	public String getSRCID() {
		return SRCID;
	}
	public void setSRCID(String sRCID) {
		SRCID = sRCID;
	}
	public String getKEYID() {
		return KEYID;
	}
	public void setKEYID(String kEYID) {
		KEYID = kEYID;
	}

}
