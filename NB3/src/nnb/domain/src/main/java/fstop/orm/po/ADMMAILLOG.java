package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ADMMAILLOG")
public class ADMMAILLOG implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8200455498999490778L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long ADMAILLOGID;

	private String ADBATCHNO = "";

	private String ADUSERID = "";

	private String ADUSERNAME = "";

	private String ADMAILACNO = "";  //批次作業代號 varchar(10)

	private String ADACNO = "";

	private String ADSENDTYPE = "";
	
	private String ADSENDTIME = "";

	private String ADSENDSTATUS = "";
	
	private String ADMSUBJECT = "";
	
	private String ADMAILCONTENT = "";

	public Long getADMAILLOGID() {
		return ADMAILLOGID;
	}

	public void setADMAILLOGID(Long aDMAILLOGID) {
		ADMAILLOGID = aDMAILLOGID;
	}

	public String getADBATCHNO() {
		return ADBATCHNO;
	}

	public void setADBATCHNO(String aDBATCHNO) {
		ADBATCHNO = aDBATCHNO;
	}

	public String getADUSERID() {
		return ADUSERID;
	}

	public void setADUSERID(String aDUSERID) {
		ADUSERID = aDUSERID;
	}

	public String getADUSERNAME() {
		return ADUSERNAME;
	}

	public void setADUSERNAME(String aDUSERNAME) {
		ADUSERNAME = aDUSERNAME;
	}

	public String getADMAILACNO() {
		return ADMAILACNO;
	}

	public void setADMAILACNO(String aDMAILACNO) {
		ADMAILACNO = aDMAILACNO;
	}

	public String getADACNO() {
		return ADACNO;
	}

	public void setADACNO(String aDACNO) {
		ADACNO = aDACNO;
	}

	public String getADSENDTYPE() {
		return ADSENDTYPE;
	}

	public void setADSENDTYPE(String aDSENDTYPE) {
		ADSENDTYPE = aDSENDTYPE;
	}

	public String getADSENDTIME() {
		return ADSENDTIME;
	}

	public void setADSENDTIME(String aDSENDTIME) {
		ADSENDTIME = aDSENDTIME;
	}

	public String getADSENDSTATUS() {
		return ADSENDSTATUS;
	}

	public void setADSENDSTATUS(String aDSENDSTATUS) {
		ADSENDSTATUS = aDSENDSTATUS;
	}

	public String getADMSUBJECT() {
		return ADMSUBJECT;
	}

	public void setADMSUBJECT(String aDMSUBJECT) {
		ADMSUBJECT = aDMSUBJECT;
	}

	public String getADMAILCONTENT() {
		return ADMAILCONTENT;
	}

	public void setADMAILCONTENT(String aDMAILCONTENT) {
		ADMAILCONTENT = aDMAILCONTENT;
	}


}
