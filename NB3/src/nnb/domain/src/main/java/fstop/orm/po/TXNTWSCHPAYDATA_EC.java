package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="fstop.orm.po.TXNTWSCHPAYDATA_EC")
@Table(name = "TXNTWSCHPAYDATA_EC")
public class TXNTWSCHPAYDATA_EC implements Serializable
{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1038467817020565457L;

	@Id
	private TXNTWSCHPAYDATA_EC_PK pks;// 複合組鍵
	
	private String ADOPID = "";
	private String DPWDAC = "";
	private String DPSVBH = "";
	private String DPSVAC = "";
	private String DPTXAMT = "";
	private String DPTXMEMO = "";
	private String DPTXMAILS = "";
	private String DPTXMAILMEMO = "";
	private String DPTXCODE = "";
	private String XMLCA = "";
	private String XMLCN = "";
	private String MAC = "";
	private String DPTXDATE = "";
	private String DPTXTIME = "";
	private String DPSEQ = "";
	private String ADTXNO = "";
	private String DPTOTAINFO = "";
	private String DPSTANNO = "";
	private String DPEFEE = "";
	private String DPTXSTATUS = "";
	private String DPEXCODE = "";
	private String DPRESEND = "";
	private String MSADDR = "";
	private String LASTDATE = "";
	private String LASTTIME = "";
	private String DPTITAINFO = "";
	
	
	public TXNTWSCHPAYDATA_EC_PK getPks() {
		return pks;
	}
	public void setPks(TXNTWSCHPAYDATA_EC_PK pks) {
		this.pks = pks;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getDPWDAC() {
		return DPWDAC;
	}
	public void setDPWDAC(String dPWDAC) {
		DPWDAC = dPWDAC;
	}
	public String getDPSVBH() {
		return DPSVBH;
	}
	public void setDPSVBH(String dPSVBH) {
		DPSVBH = dPSVBH;
	}
	public String getDPSVAC() {
		return DPSVAC;
	}
	public void setDPSVAC(String dPSVAC) {
		DPSVAC = dPSVAC;
	}
	public String getDPTXAMT() {
		return DPTXAMT;
	}
	public void setDPTXAMT(String dPTXAMT) {
		DPTXAMT = dPTXAMT;
	}
	public String getDPTXMEMO() {
		return DPTXMEMO;
	}
	public void setDPTXMEMO(String dPTXMEMO) {
		DPTXMEMO = dPTXMEMO;
	}
	public String getDPTXMAILS() {
		return DPTXMAILS;
	}
	public void setDPTXMAILS(String dPTXMAILS) {
		DPTXMAILS = dPTXMAILS;
	}
	public String getDPTXMAILMEMO() {
		return DPTXMAILMEMO;
	}
	public void setDPTXMAILMEMO(String dPTXMAILMEMO) {
		DPTXMAILMEMO = dPTXMAILMEMO;
	}
	public String getDPTXCODE() {
		return DPTXCODE;
	}
	public void setDPTXCODE(String dPTXCODE) {
		DPTXCODE = dPTXCODE;
	}
	public String getXMLCA() {
		return XMLCA;
	}
	public void setXMLCA(String xMLCA) {
		XMLCA = xMLCA;
	}
	public String getXMLCN() {
		return XMLCN;
	}
	public void setXMLCN(String xMLCN) {
		XMLCN = xMLCN;
	}
	public String getMAC() {
		return MAC;
	}
	public void setMAC(String mAC) {
		MAC = mAC;
	}
	public String getDPTXDATE() {
		return DPTXDATE;
	}
	public void setDPTXDATE(String dPTXDATE) {
		DPTXDATE = dPTXDATE;
	}
	public String getDPTXTIME() {
		return DPTXTIME;
	}
	public void setDPTXTIME(String dPTXTIME) {
		DPTXTIME = dPTXTIME;
	}
	public String getDPSEQ() {
		return DPSEQ;
	}
	public void setDPSEQ(String dPSEQ) {
		DPSEQ = dPSEQ;
	}
	public String getADTXNO() {
		return ADTXNO;
	}
	public void setADTXNO(String aDTXNO) {
		ADTXNO = aDTXNO;
	}
	public String getDPTOTAINFO() {
		return DPTOTAINFO;
	}
	public void setDPTOTAINFO(String dPTOTAINFO) {
		DPTOTAINFO = dPTOTAINFO;
	}
	public String getDPSTANNO() {
		return DPSTANNO;
	}
	public void setDPSTANNO(String dPSTANNO) {
		DPSTANNO = dPSTANNO;
	}
	public String getDPEFEE() {
		return DPEFEE;
	}
	public void setDPEFEE(String dPEFEE) {
		DPEFEE = dPEFEE;
	}
	public String getDPTXSTATUS() {
		return DPTXSTATUS;
	}
	public void setDPTXSTATUS(String dPTXSTATUS) {
		DPTXSTATUS = dPTXSTATUS;
	}
	public String getDPEXCODE() {
		return DPEXCODE;
	}
	public void setDPEXCODE(String dPEXCODE) {
		DPEXCODE = dPEXCODE;
	}
	public String getDPRESEND() {
		return DPRESEND;
	}
	public void setDPRESEND(String dPRESEND) {
		DPRESEND = dPRESEND;
	}
	public String getMSADDR() {
		return MSADDR;
	}
	public void setMSADDR(String mSADDR) {
		MSADDR = mSADDR;
	}
	public String getLASTDATE() {
		return LASTDATE;
	}
	public void setLASTDATE(String lASTDATE) {
		LASTDATE = lASTDATE;
	}
	public String getLASTTIME() {
		return LASTTIME;
	}
	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}
	public String getDPTITAINFO() {
		return DPTITAINFO;
	}
	public void setDPTITAINFO(String dPTITAINFO) {
		DPTITAINFO = dPTITAINFO;
	}

	
}
