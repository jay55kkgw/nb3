package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="fstop.orm.po.OLD_TXNTWRECORD")
@Table(name = "TXNTWRECORD")
public class OLD_TXNTWRECORD implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -2697992783203257003L;

	@Id
	private String ADTXNO;

	private String ADOPID = "";

	private String DPUSERID = "";

	private String DPTXDATE = "";

	private String DPTXTIME = "";

	private String DPWDAC = "";

	private String DPSVBH = "";
	
	private String PCSEQ = "";

	private String DPSVAC = "";

	private String DPTXAMT = "";

	private String DPTXMEMO = "";

	private String DPTXMAILS = "";
	
	private String DPTXMAILMEMO = "";

	private Long DPSCHID = 0L;

	private String DPEFEE = "";

	private String DPTXNO = "";   //跨行序號

	private String DPTXCODE = "";   //交易機制

	private Long DPTITAINFO = 0L;

	private String DPTOTAINFO = "";

	private String DPREMAIL = "";

	private String DPTXSTATUS = "";

	private String DPEXCODE = "";

	private String DPRETXNO = "";
	
	private String DPRETXSTATUS = "";

	private String LASTDATE = "";

	private String LASTTIME = "";
	
	private String DPSCHNO = "";

	public String getADTXNO() {
		return ADTXNO;
	}

	public void setADTXNO(String aDTXNO) {
		ADTXNO = aDTXNO;
	}

	public String getADOPID() {
		return ADOPID;
	}

	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}

	public String getDPUSERID() {
		return DPUSERID;
	}

	public void setDPUSERID(String dPUSERID) {
		DPUSERID = dPUSERID;
	}

	public String getDPTXDATE() {
		return DPTXDATE;
	}

	public void setDPTXDATE(String dPTXDATE) {
		DPTXDATE = dPTXDATE;
	}

	public String getDPTXTIME() {
		return DPTXTIME;
	}

	public void setDPTXTIME(String dPTXTIME) {
		DPTXTIME = dPTXTIME;
	}

	public String getDPWDAC() {
		return DPWDAC;
	}

	public void setDPWDAC(String dPWDAC) {
		DPWDAC = dPWDAC;
	}

	public String getDPSVBH() {
		return DPSVBH;
	}

	public void setDPSVBH(String dPSVBH) {
		DPSVBH = dPSVBH;
	}

	public String getPCSEQ() {
		return PCSEQ;
	}

	public void setPCSEQ(String pCSEQ) {
		PCSEQ = pCSEQ;
	}

	public String getDPSVAC() {
		return DPSVAC;
	}

	public void setDPSVAC(String dPSVAC) {
		DPSVAC = dPSVAC;
	}

	public String getDPTXAMT() {
		return DPTXAMT;
	}

	public void setDPTXAMT(String dPTXAMT) {
		DPTXAMT = dPTXAMT;
	}

	public String getDPTXMEMO() {
		return DPTXMEMO;
	}

	public void setDPTXMEMO(String dPTXMEMO) {
		DPTXMEMO = dPTXMEMO;
	}

	public String getDPTXMAILS() {
		return DPTXMAILS;
	}

	public void setDPTXMAILS(String dPTXMAILS) {
		DPTXMAILS = dPTXMAILS;
	}

	public String getDPTXMAILMEMO() {
		return DPTXMAILMEMO;
	}

	public void setDPTXMAILMEMO(String dPTXMAILMEMO) {
		DPTXMAILMEMO = dPTXMAILMEMO;
	}

	public Long getDPSCHID() {
		return DPSCHID;
	}

	public void setDPSCHID(Long dPSCHID) {
		DPSCHID = dPSCHID;
	}

	public String getDPEFEE() {
		return DPEFEE;
	}

	public void setDPEFEE(String dPEFEE) {
		DPEFEE = dPEFEE;
	}

	public String getDPTXNO() {
		return DPTXNO;
	}

	public void setDPTXNO(String dPTXNO) {
		DPTXNO = dPTXNO;
	}

	public String getDPTXCODE() {
		return DPTXCODE;
	}

	public void setDPTXCODE(String dPTXCODE) {
		DPTXCODE = dPTXCODE;
	}

	public Long getDPTITAINFO() {
		return DPTITAINFO;
	}

	public void setDPTITAINFO(Long dPTITAINFO) {
		DPTITAINFO = dPTITAINFO;
	}

	public String getDPTOTAINFO() {
		return DPTOTAINFO;
	}

	public void setDPTOTAINFO(String dPTOTAINFO) {
		DPTOTAINFO = dPTOTAINFO;
	}

	public String getDPREMAIL() {
		return DPREMAIL;
	}

	public void setDPREMAIL(String dPREMAIL) {
		DPREMAIL = dPREMAIL;
	}

	public String getDPTXSTATUS() {
		return DPTXSTATUS;
	}

	public void setDPTXSTATUS(String dPTXSTATUS) {
		DPTXSTATUS = dPTXSTATUS;
	}

	public String getDPEXCODE() {
		return DPEXCODE;
	}

	public void setDPEXCODE(String dPEXCODE) {
		DPEXCODE = dPEXCODE;
	}

	public String getDPRETXNO() {
		return DPRETXNO;
	}

	public void setDPRETXNO(String dPRETXNO) {
		DPRETXNO = dPRETXNO;
	}

	public String getDPRETXSTATUS() {
		return DPRETXSTATUS;
	}

	public void setDPRETXSTATUS(String dPRETXSTATUS) {
		DPRETXSTATUS = dPRETXSTATUS;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lASTDATE) {
		LASTDATE = lASTDATE;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}

	public String getDPSCHNO() {
		return DPSCHNO;
	}

	public void setDPSCHNO(String dPSCHNO) {
		DPSCHNO = dPSCHNO;
	}
	
	

}
