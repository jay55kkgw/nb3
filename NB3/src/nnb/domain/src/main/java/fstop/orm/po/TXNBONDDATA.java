package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity(name="fstop.orm.po.TXNBONDDATA")
@Table(name = "TXNBONDDATA")
public class TXNBONDDATA implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6198168685639709106L;
	
	
	@Id
	private String BONDCODE;

	private String BONDNAME;

	private String BONDCRY;

	private String DBUONLY;

	private String OBUONLY;

	private String RISK;

	private String MINBPRICE;

	private String PROGRESSIVEBPRICE;
	
	private String MINSPRICE;
	
	private String PROGRESSIVESPRICE;
	
	@Transient
	private String BONDCRY_SHOW;
	
	@Transient
	private String MINBPRICE_FMT;
	
	@Transient
	private String PROGRESSIVEBPRICE_FMT;
	
	@Transient
	private String MINSPRICE_FMT;
	
	@Transient
	private String PROGRESSIVESPRICE_FMT;

	public String getBONDCODE() {
		return BONDCODE;
	}

	public void setBONDCODE(String bONDCODE) {
		BONDCODE = bONDCODE;
	}

	public String getBONDNAME() {
		return BONDNAME;
	}

	public void setBONDNAME(String bONDNAME) {
		BONDNAME = bONDNAME;
	}

	public String getBONDCRY() {
		return BONDCRY;
	}

	public void setBONDCRY(String bONDCRY) {
		BONDCRY = bONDCRY;
	}

	public String getDBUONLY() {
		return DBUONLY;
	}

	public void setDBUONLY(String dBUONLY) {
		DBUONLY = dBUONLY;
	}

	public String getOBUONLY() {
		return OBUONLY;
	}

	public void setOBUONLY(String oBUONLY) {
		OBUONLY = oBUONLY;
	}

	public String getRISK() {
		return RISK;
	}

	public void setRISK(String rISK) {
		RISK = rISK;
	}

	public String getMINBPRICE() {
		return MINBPRICE;
	}

	public void setMINBPRICE(String mINBPRICE) {
		MINBPRICE = mINBPRICE;
	}

	public String getPROGRESSIVEBPRICE() {
		return PROGRESSIVEBPRICE;
	}

	public void setPROGRESSIVEBPRICE(String pROGRESSIVEBPRICE) {
		PROGRESSIVEBPRICE = pROGRESSIVEBPRICE;
	}

	public String getMINSPRICE() {
		return MINSPRICE;
	}

	public void setMINSPRICE(String mINSPRICE) {
		MINSPRICE = mINSPRICE;
	}

	public String getPROGRESSIVESPRICE() {
		return PROGRESSIVESPRICE;
	}

	public void setPROGRESSIVESPRICE(String pROGRESSIVESPRICE) {
		PROGRESSIVESPRICE = pROGRESSIVESPRICE;
	}

	public String getBONDCRY_SHOW() {
		return BONDCRY_SHOW;
	}

	public void setBONDCRY_SHOW(String bONDCRY_SHOW) {
		BONDCRY_SHOW = bONDCRY_SHOW;
	}

	public String getMINBPRICE_FMT() {
		return MINBPRICE_FMT;
	}

	public void setMINBPRICE_FMT(String mINBPRICE_FMT) {
		MINBPRICE_FMT = mINBPRICE_FMT;
	}

	public String getPROGRESSIVEBPRICE_FMT() {
		return PROGRESSIVEBPRICE_FMT;
	}

	public void setPROGRESSIVEBPRICE_FMT(String pROGRESSIVEBPRICE_FMT) {
		PROGRESSIVEBPRICE_FMT = pROGRESSIVEBPRICE_FMT;
	}

	public String getMINSPRICE_FMT() {
		return MINSPRICE_FMT;
	}

	public void setMINSPRICE_FMT(String mINSPRICE_FMT) {
		MINSPRICE_FMT = mINSPRICE_FMT;
	}

	public String getPROGRESSIVESPRICE_FMT() {
		return PROGRESSIVESPRICE_FMT;
	}

	public void setPROGRESSIVESPRICE_FMT(String pROGRESSIVESPRICE_FMT) {
		PROGRESSIVESPRICE_FMT = pROGRESSIVESPRICE_FMT;
	}
	
}
