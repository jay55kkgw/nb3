package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "TXNFUNDTRACELOG")
public class TXNFUNDTRACELOG implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long ADFDTRACEID;

	private String ADTXNO = "";

	private String FDTXTIME = "";

	private String FDUSERID = "";

	private String FDTXLOG = "";

	private String FDNOTICETYPE = "";

	private String FDAGREEFLAG = "";
	
	private String FDPUBLICTYPE = "";
	
	private String FDTXSTATUS = "";

	private String ADGUID = "";
	
	public String getADGUID() {
		return ADGUID;
	}

	public void setADGUID(String adguid) {
		ADGUID = adguid;
	}

	public String getFDTXSTATUS() {
		return FDTXSTATUS;
	}

	public void setFDTXSTATUS(String fdtxstatus) {
		FDTXSTATUS = fdtxstatus;
	}

	public String getFDPUBLICTYPE() {
		return FDPUBLICTYPE;
	}

	public void setFDPUBLICTYPE(String fdpublictype) {
		FDPUBLICTYPE = fdpublictype;
	}

	public String toString() {
		return new ToStringBuilder(this)
				.append("adfdtraceid", getADFDTRACEID()).toString();
	}

	public Long getADFDTRACEID() {
		return ADFDTRACEID;
	}

	public void setADFDTRACEID(Long adfdtraceid) {
		ADFDTRACEID = adfdtraceid;
	}

	public String getADTXNO() {
		return ADTXNO;
	}

	public void setADTXNO(String adtxno) {
		ADTXNO = adtxno;
	}

	public String getFDAGREEFLAG() {
		return FDAGREEFLAG;
	}

	public void setFDAGREEFLAG(String fdagreeflag) {
		FDAGREEFLAG = fdagreeflag;
	}

	public String getFDNOTICETYPE() {
		return FDNOTICETYPE;
	}

	public void setFDNOTICETYPE(String fdnoticetype) {
		FDNOTICETYPE = fdnoticetype;
	}

	public String getFDTXLOG() {
		return FDTXLOG;
	}

	public void setFDTXLOG(String fdtxlog) {
		FDTXLOG = fdtxlog;
	}

	public String getFDTXTIME() {
		return FDTXTIME;
	}

	public void setFDTXTIME(String fdtxtime) {
		FDTXTIME = fdtxtime;
	}

	public String getFDUSERID() {
		return FDUSERID;
	}

	public void setFDUSERID(String fduserid) {
		FDUSERID = fduserid;
	}

}
