package fstop.orm.po;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "ITRINTERVAL")
public class ITRINTERVAL implements Serializable {

	@Id
	private String HEADER;

	private String TABLENAME;

	private String INTERVAL;

	private String DATE;

	private String TIME;
	
	public String toString() {
		return new ToStringBuilder(this).append("recno", getHEADER())
				.toString();
	}

	public String getHEADER() {
		return HEADER;
	}

	public void setHEADER(String header) {
		HEADER = header;
	}

	public String getTABLENAME() {
		return TABLENAME;
	}

	public void setTABLENAME(String tablename) {
		TABLENAME = tablename;
	}

	public String getINTERVAL() {
		return INTERVAL;
	}

	public void setINTERVAL(Integer interval) {
		INTERVAL = interval.toString();
	}

	public String getDATE() {
		return DATE;
	}

	public void setDATE(String date) {
		DATE = date;
	}

	public String getTIME() {
		return TIME;
	}

	public void setTIME(String time) {
		TIME = time;
	}

}
