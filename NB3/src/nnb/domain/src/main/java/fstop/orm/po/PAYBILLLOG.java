package fstop.orm.po;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PAYBILLLOG")
public class PAYBILLLOG implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long LOGID;

	private String CUSIDN = "";

	private String CARDNUM = "";

	private String PAYSOURCE = "";

	private String PAYAMT = "";

	private String BANKID = "";

	private String ACCOUNT = "";

	private String ADMCODE = "";

	private String REMOTEIP = "";

	private String LOGDATE = "";

	private String LOGTIME = "";

	public String getADMCODE() {
		return ADMCODE;
	}

	public void setADMCODE(String admcode) {
		ADMCODE = admcode;
	}

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cusidn) {
		CUSIDN = cusidn;
	}

	public String getCARDNUM() {
		return CARDNUM;
	}

	public void setCARDNUM(String cardnum) {
		CARDNUM = cardnum;
	}

	public String getPAYSOURCE() {
		return PAYSOURCE;
	}

	public void setPAYSOURCE(String paysource) {
		PAYSOURCE = paysource;
	}

	public String getLOGDATE() {
		return LOGDATE;
	}

	public void setLOGDATE(String logdate) {
		LOGDATE = logdate;
	}

	public Long getLOGID() {
		return LOGID;
	}

	public void setLOGID(Long logid) {
		LOGID = logid;
	}

	public String getLOGTIME() {
		return LOGTIME;
	}

	public void setLOGTIME(String logtime) {
		LOGTIME = logtime;
	}

	public String getPAYAMT() {
		return PAYAMT;
	}

	public void setPAYAMT(String payamt) {
		PAYAMT = payamt;
	}

	public String getREMOTEIP() {
		return REMOTEIP;
	}

	public void setREMOTEIP(String remoteip) {
		REMOTEIP = remoteip;
	}

	public String getBANKID() {
		return BANKID;
	}

	public void setBANKID(String bankid) {
		BANKID = bankid;
	}

	public String getACCOUNT() {
		return ACCOUNT;
	}

	public void setACCOUNT(String account) {
		ACCOUNT = account;
	}


}
