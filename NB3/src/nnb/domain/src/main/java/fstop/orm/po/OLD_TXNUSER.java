package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="fstop.orm.po.OLD_TXNUSER")
@Table(name = "TXNUSER")
public class OLD_TXNUSER implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -3021386132767262450L;

	@Id
	private String DPSUERID;

	private String DPUSERNAME = "";

	private String DPBILL  = "";

	private String DPMENU  = "";

	private String DPOVERVIEW  = "";

	private String DPMYEMAIL  = "";

	private String DPNOTIFY  = "";

	private String DPFUNDBILL  = "";

	private String DPCARDBILL  = "";

	private String LASTDATE  = "";

	private String LASTTIME  = "";

	private String BILLDATE = "";

	private String FUNDBILLDATE = "";

	private String CARDBILLDATE = "";

	private String EMAILDATE = "";

	private Integer SCHCOUNT;

	private String SCHCNTDATE = "";

	private String ADBRANCHID = "";

	private String FUNDAGREEVERSION = "";

	private String FUNDBILLSENDMODE = "";

	private Integer ASKERRTIMES;

	private String CCARDFLAG = "";
	
	private String RESETTIME = "";

	private String FXREMITFLAG = "";
	
	private String EINPHONE = "";
	
	private String CARDNO = "";
	
	private String SHOWCARDNOFLAG = "";
	
	private String FREQACN = "";
	
	private String FREQCARD = "";

	public String getDPSUERID() {
		return DPSUERID;
	}

	public void setDPSUERID(String dPSUERID) {
		DPSUERID = dPSUERID;
	}

	public String getDPUSERNAME() {
		return DPUSERNAME;
	}

	public void setDPUSERNAME(String dPUSERNAME) {
		DPUSERNAME = dPUSERNAME;
	}

	public String getDPBILL() {
		return DPBILL;
	}

	public void setDPBILL(String dPBILL) {
		DPBILL = dPBILL;
	}

	public String getDPMENU() {
		return DPMENU;
	}

	public void setDPMENU(String dPMENU) {
		DPMENU = dPMENU;
	}

	public String getDPOVERVIEW() {
		return DPOVERVIEW;
	}

	public void setDPOVERVIEW(String dPOVERVIEW) {
		DPOVERVIEW = dPOVERVIEW;
	}

	public String getDPMYEMAIL() {
		return DPMYEMAIL;
	}

	public void setDPMYEMAIL(String dPMYEMAIL) {
		DPMYEMAIL = dPMYEMAIL;
	}

	public String getDPNOTIFY() {
		return DPNOTIFY;
	}

	public void setDPNOTIFY(String dPNOTIFY) {
		DPNOTIFY = dPNOTIFY;
	}

	public String getDPFUNDBILL() {
		return DPFUNDBILL;
	}

	public void setDPFUNDBILL(String dPFUNDBILL) {
		DPFUNDBILL = dPFUNDBILL;
	}

	public String getDPCARDBILL() {
		return DPCARDBILL;
	}

	public void setDPCARDBILL(String dPCARDBILL) {
		DPCARDBILL = dPCARDBILL;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lASTDATE) {
		LASTDATE = lASTDATE;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}

	public String getBILLDATE() {
		return BILLDATE;
	}

	public void setBILLDATE(String bILLDATE) {
		BILLDATE = bILLDATE;
	}

	public String getFUNDBILLDATE() {
		return FUNDBILLDATE;
	}

	public void setFUNDBILLDATE(String fUNDBILLDATE) {
		FUNDBILLDATE = fUNDBILLDATE;
	}

	public String getCARDBILLDATE() {
		return CARDBILLDATE;
	}

	public void setCARDBILLDATE(String cARDBILLDATE) {
		CARDBILLDATE = cARDBILLDATE;
	}

	public String getEMAILDATE() {
		return EMAILDATE;
	}

	public void setEMAILDATE(String eMAILDATE) {
		EMAILDATE = eMAILDATE;
	}

	public Integer getSCHCOUNT() {
		return SCHCOUNT;
	}

	public void setSCHCOUNT(Integer sCHCOUNT) {
		SCHCOUNT = sCHCOUNT;
	}

	public String getSCHCNTDATE() {
		return SCHCNTDATE;
	}

	public void setSCHCNTDATE(String sCHCNTDATE) {
		SCHCNTDATE = sCHCNTDATE;
	}

	public String getADBRANCHID() {
		return ADBRANCHID;
	}

	public void setADBRANCHID(String aDBRANCHID) {
		ADBRANCHID = aDBRANCHID;
	}

	public String getFUNDAGREEVERSION() {
		return FUNDAGREEVERSION;
	}

	public void setFUNDAGREEVERSION(String fUNDAGREEVERSION) {
		FUNDAGREEVERSION = fUNDAGREEVERSION;
	}

	public String getFUNDBILLSENDMODE() {
		return FUNDBILLSENDMODE;
	}

	public void setFUNDBILLSENDMODE(String fUNDBILLSENDMODE) {
		FUNDBILLSENDMODE = fUNDBILLSENDMODE;
	}

	public Integer getASKERRTIMES() {
		return ASKERRTIMES;
	}

	public void setASKERRTIMES(Integer aSKERRTIMES) {
		ASKERRTIMES = aSKERRTIMES;
	}

	public String getCCARDFLAG() {
		return CCARDFLAG;
	}

	public void setCCARDFLAG(String cCARDFLAG) {
		CCARDFLAG = cCARDFLAG;
	}

	public String getRESETTIME() {
		return RESETTIME;
	}

	public void setRESETTIME(String rESETTIME) {
		RESETTIME = rESETTIME;
	}

	public String getFXREMITFLAG() {
		return FXREMITFLAG;
	}

	public void setFXREMITFLAG(String fXREMITFLAG) {
		FXREMITFLAG = fXREMITFLAG;
	}

	public String getEINPHONE() {
		return EINPHONE;
	}

	public void setEINPHONE(String eINPHONE) {
		EINPHONE = eINPHONE;
	}

	public String getCARDNO() {
		return CARDNO;
	}

	public void setCARDNO(String cARDNO) {
		CARDNO = cARDNO;
	}

	public String getSHOWCARDNOFLAG() {
		return SHOWCARDNOFLAG;
	}

	public void setSHOWCARDNOFLAG(String sHOWCARDNOFLAG) {
		SHOWCARDNOFLAG = sHOWCARDNOFLAG;
	}

	public String getFREQACN() {
		return FREQACN;
	}

	public void setFREQACN(String fREQACN) {
		FREQACN = fREQACN;
	}

	public String getFREQCARD() {
		return FREQCARD;
	}

	public void setFREQCARD(String fREQCARD) {
		FREQCARD = fREQCARD;
	}
	
	
}
