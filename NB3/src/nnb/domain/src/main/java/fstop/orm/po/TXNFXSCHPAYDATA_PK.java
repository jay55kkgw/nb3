package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class TXNFXSCHPAYDATA_PK implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -236990212418942830L;
	
	
	private String FXSCHTXDATE = "";	//預約批號
	private String FXSCHNO = "";		//預約轉帳日
	private String FXUSERID="";
	public String getFXSCHTXDATE() {
		return FXSCHTXDATE;
	}
	public void setFXSCHTXDATE(String fXSCHTXDATE) {
		FXSCHTXDATE = fXSCHTXDATE;
	}
	public String getFXSCHNO() {
		return FXSCHNO;
	}
	public void setFXSCHNO(String fXSCHNO) {
		FXSCHNO = fXSCHNO;
	}
	public String getUSERID() {
		return FXUSERID;
	}
	public void setUSERID(String uSERID) {
		FXUSERID = uSERID;
	}
	
	

}
