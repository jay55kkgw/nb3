package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "IDGATE_TXN_STATUS")
public class IDGATE_TXN_STATUS  implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7735653341773285393L;

	@Id
	private IDGATE_TXN_STATUS_PK pks;

	private String TXNCONTENT;
	private String TXNSTATUS;
	private String LASTDATE;
	private String LASTTIME;
	private String ADOPID;
	public IDGATE_TXN_STATUS_PK getPks() {
		return pks;
	}
	public void setPks(IDGATE_TXN_STATUS_PK pks) {
		this.pks = pks;
	}
	public String getTXNCONTENT() {
		return TXNCONTENT;
	}
	public void setTXNCONTENT(String tXNCONTENT) {
		TXNCONTENT = tXNCONTENT;
	}
	public String getTXNSTATUS() {
		return TXNSTATUS;
	}
	public void setTXNSTATUS(String tXNSTATUS) {
		TXNSTATUS = tXNSTATUS;
	}
	public String getLASTDATE() {
		return LASTDATE;
	}
	public void setLASTDATE(String lASTDATE) {
		LASTDATE = lASTDATE;
	}
	public String getLASTTIME() {
		return LASTTIME;
	}
	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
}
