package fstop.orm.po;

import java.io.Serializable;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity(name="fstop.orm.po.TXNPHONETOKEN")
@Table(name = "TXNPHONETOKEN")
public class TXNPHONETOKEN implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7074866990725914678L;

	@Id
	@Column(name="DPUSERID") 
	private String DPUSERID;//統編

	private String PHONETOKEN;

	private String PHONETYPE;//手機OS型別，A(Android)或I(Iphone)
	
	private String NOTIFYAD;
	
	private String NOTIFYTRANS;

	private String LASTDATE;//Y或N

	private String LASTTIME;//Y或N

	public String toString() {
		return new ToStringBuilder(this).append("phonetoken", getPHONETOKEN())
				.toString();
	}

	public String getDPUSERID() {
		return DPUSERID;
	}

	public void setDPUSERID(String dpuserid) {
		DPUSERID = dpuserid;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

	public String getPHONETOKEN() {
		return PHONETOKEN;
	}

	public void setPHONETOKEN(String phonetoken) {
		PHONETOKEN = phonetoken;
	}

	public String getPHONETYPE() {
		return PHONETYPE;
	}

	public void setPHONETYPE(String phonetype) {
		PHONETYPE = phonetype;
	}

	public String getNOTIFYAD() {
		return NOTIFYAD;
	}

	public void setNOTIFYAD(String notifyad) {
		NOTIFYAD = notifyad;
	}

	public String getNOTIFYTRANS() {
		return NOTIFYTRANS;
	}

	public void setNOTIFYTRANS(String notifytrans) {
		NOTIFYTRANS = notifytrans;
	}
	
}
