package fstop.orm.po;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity(name="fstop.orm.po.ADMBRANCH")
@Table(name = "ADMBRANCH")
public class ADMBRANCH implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1956623886815183253L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String ADBRANCHID;

	private String ADBRANCHNAME;

	private String ADBRANENGNAME;
	
	private String ADBRANCHSNAME;

	private String LASTUSER;

	private String LASTDATE;

	private String LASTTIME;

	private String POSTCODE;

	private String ADDRESS;
	
	private String TELNUM;
	
	private String BANKCODE;
	
	private String ADDRESSENG;
	
	private String ADDRESSCHS;
	
	public String toString() {
		
		return new ToStringBuilder(this).append("admbranchid", getADBRANCHID())
				.toString();
	}
	
	public String getADBRANCHID() {
		return ADBRANCHID;
	}

	public void setADBRANCHID(String adbranchid) {
		ADBRANCHID = adbranchid;
	}

	public String getADBRANCHNAME() {
		return ADBRANCHNAME;
	}

	public void setADBRANCHNAME(String adbranchname) {
		ADBRANCHNAME = adbranchname;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

	public String getLASTUSER() {
		return LASTUSER;
	}

	public void setLASTUSER(String lastuser) {
		LASTUSER = lastuser;
	}
	public String getPOSTCODE() {
		return POSTCODE;
	}

	public void setPOSTCODE(String postcode) {
		POSTCODE = postcode;
	}
	public String getADDRESS() {
		return ADDRESS;
	}

	public void setADDRESS(String address) {
		ADDRESS = address;
	}
	public String getTELNUM() {
		return TELNUM;
	}

	public void setTELNUM(String telnum) {
		TELNUM = telnum;
	}

	public String getBANKCODE() {
		return BANKCODE;
	}

	public void setBANKCODE(String bankcode) {
		BANKCODE = bankcode;
	}	
	
	public String getADBRANENGNAME() {
		return ADBRANENGNAME;
	}

	public void setADBRANENGNAME(String aDBRANENGNAME) {
		ADBRANENGNAME = aDBRANENGNAME;
	}

	public String getADBRANCHSNAME() {
		return ADBRANCHSNAME;
	}

	public void setADBRANCHSNAME(String aDBRANCHSNAME) {
		ADBRANCHSNAME = aDBRANCHSNAME;
	}
	public String getADDRESSENG() {
		return ADDRESSENG;
	}

	public void setADDRESSENG(String aDDRESSENG) {
		ADDRESSENG = aDDRESSENG;
	}

	public String getADDRESSCHS() {
		return ADDRESSCHS;
	}

	public void setADDRESSCHS(String aDDRESSCHS) {
		ADDRESSCHS = aDDRESSCHS;
	}

}
