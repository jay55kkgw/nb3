package fstop.orm.po;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "ITRN030")
public class ITRN030 implements Serializable {

	@Id
	private String RECNO;

	private String HEADER;

	private String SEQ;

	private String DATE;

	private String TIME;

	private String OFCRY;
	
	private String CRY;
	
	private String BUYITR;
	
	private String SELITR;
	
	private String TERM;

	private String FILL;
	
	public String toString() {
		return new ToStringBuilder(this).append("recno", getRECNO())
				.toString();
	}

	public String getRECNO() {
		return RECNO;
	}

	public void setRECNO(String recno) {
		RECNO = recno;
	}

	public String getHEADER() {
		return HEADER;
	}

	public void setHEADER(String header) {
		HEADER = header;
	}

	public String getSEQ() {
		return SEQ;
	}

	public void setSEQ(String seq) {
		SEQ = seq;
	}

	public String getDATE() {
		return DATE;
	}

	public void setDATE(String date) {
		DATE = date;
	}

	public String getTIME() {
		return TIME;
	}

	public void setTIME(String time) {
		TIME = time;
	}

	public String getOFCRY() {
		return OFCRY;
	}

	public void setOFCRY(String ofcry) {
		OFCRY = ofcry;
	}

	public String getCRY() {
		return CRY;
	}

	public void setCRY(String cry) {
		CRY = cry;
	}

	public String getBUYITR() {
		return BUYITR;
	}

	public void setBUYITR(String buyitr) {
		BUYITR = buyitr;
	}

	public String getSELITR() {
		return SELITR;
	}

	public void setSELITR(String selitr) {
		SELITR = selitr;
	}

	public String getTERM() {
		return TERM;
	}

	public void setTERM(String term) {
		TERM = term;
	}

	public String getFILL() {
		return FILL;
	}

	public void setFILL(String fill) {
		FILL = fill;
	}

}
