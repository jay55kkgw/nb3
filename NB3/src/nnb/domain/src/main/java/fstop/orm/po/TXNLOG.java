package fstop.orm.po;

import java.io.Serializable;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.google.gson.Gson;

//TODO need to change dependency
//import fstop.util.JSONUtils;

@Entity(name="fstop.orm.po.TXNLOG")
@Table(name = "TXNLOG")
public class TXNLOG implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7303114678256533967L;

	@Id
	private String ADTXNO = "";

	private String ADUSERID = "";

	private String ADOPID = "";

	private String FGTXWAY = "";

	private String ADTXACNO = "";

	private String ADTXAMT = "";

	private String ADCURRENCY = "";

	private String ADSVBH = "";

	private String ADAGREEF = "";

	private String ADREQTYPE = "";

	private String ADUSERIP = "";

	private String ADCONTENT = "";

	private String ADEXCODE = "";

	private String LASTDATE = "";

	private String LASTTIME = "";
	
	private String ADTXID = "";  //指出哪一個電文有誤
	
	private String ADGUID = ""; //追蹤編號
	
	private String LOGINTYPE = "NB";//判斷NB:網銀，MB:行動
	
	private String PSTIMEDIFF = "";//NB3 TO ms 時間差
	
	
	public String getADTXID() {
		return ADTXID;
	}

	public void setADTXID(String adtxid) {
		ADTXID = adtxid;
	}

	public String getADGUID() {
		return ADGUID;
	}

	public void setADGUID(String adguid) {
		ADGUID = adguid;
	}

	public String getADAGREEF() {
		return ADAGREEF;
	}

	public void setADAGREEF(String adagreef) {
		ADAGREEF = adagreef;
	}

	public String getADCONTENT() {

//TODO need to change dependency		
//		Map<String, String> paramsMap = JSONUtils.json2map(ADCONTENT);
//		paramsMap.remove("N950PASSWORD");
//		paramsMap.remove("CMPASSWORD");
//		paramsMap.remove("CMPWD");		
//				
//		return JSONUtils.map2json(paramsMap);
		return ADCONTENT;
	}

	public void setADCONTENT(String adcontent) {

//TODO need to change dependency				
//		Map<String, String> paramsMap = JSONUtils.json2map(adcontent);
//		paramsMap.remove("N950PASSWORD");
//		paramsMap.remove("CMPASSWORD");
//		paramsMap.remove("CMPWD");
//		
//		ADCONTENT = JSONUtils.map2json(paramsMap);
		ADCONTENT = adcontent;
	}

	@Override
	public String toString() {
		return "TXNLOG [ADTXNO=" + ADTXNO + ", ADUSERID=" + ADUSERID + ", ADOPID=" + ADOPID + ", FGTXWAY=" + FGTXWAY
				+ ", ADTXACNO=" + ADTXACNO + ", ADTXAMT=" + ADTXAMT + ", ADCURRENCY=" + ADCURRENCY + ", ADSVBH="
				+ ADSVBH + ", ADAGREEF=" + ADAGREEF + ", ADREQTYPE=" + ADREQTYPE + ", ADUSERIP=" + ADUSERIP
				+ ", ADCONTENT=" + ADCONTENT + ", ADEXCODE=" + ADEXCODE + ", LASTDATE=" + LASTDATE + ", LASTTIME="
				+ LASTTIME + ", ADTXID=" + ADTXID + ", ADGUID=" + ADGUID + ", LOGINTYPE=" + LOGINTYPE + "]";
	}

	public String getADCURRENCY() {
		return ADCURRENCY;
	}

	public void setADCURRENCY(String adcurrency) {
		ADCURRENCY = adcurrency;
	}

	public String getADEXCODE() {
		return ADEXCODE;
	}

	public void setADEXCODE(String adexcode) {
		ADEXCODE = adexcode;
	}

	public String getADOPID() {
		return ADOPID;
	}

	public void setADOPID(String adopid) {
		ADOPID = adopid;
	}

	public String getADREQTYPE() {
		return ADREQTYPE;
	}

	public void setADREQTYPE(String adreqtype) {
		ADREQTYPE = adreqtype;
	}

	public String getADSVBH() {
		return ADSVBH;
	}

	public void setADSVBH(String adsvbh) {
		ADSVBH = adsvbh;
	}

	public String getADTXACNO() {
		return ADTXACNO;
	}

	public void setADTXACNO(String adtxacno) {
		ADTXACNO = adtxacno;
	}

	public String getADTXAMT() {
		return ADTXAMT;
	}

	public void setADTXAMT(String adtxamt) {
		ADTXAMT = adtxamt;
	}

	public String getADTXNO() {
		return ADTXNO;
	}

	public void setADTXNO(String adtxno) {
		ADTXNO = adtxno;
	}

	public String getADUSERID() {
		return ADUSERID;
	}

	public void setADUSERID(String aduserid) {
		ADUSERID = aduserid;
	}

	public String getADUSERIP() {
		return ADUSERIP;
	}

	public void setADUSERIP(String aduserip) {
		ADUSERIP = aduserip;
	}

	public String getFGTXWAY() {
		return FGTXWAY;
	}

	public void setFGTXWAY(String fgtxway) {
		FGTXWAY = fgtxway;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

//	public String toString() {
//		return new ToStringBuilder(this).append("adtxno", getADTXNO())
//				.toString();
//	}

	public String getLOGINTYPE() {
		return LOGINTYPE;
	}

	public void setLOGINTYPE(String logintype) {
		LOGINTYPE = logintype;
	}

	public String getPSTIMEDIFF() {
		return PSTIMEDIFF;
	}

	public void setPSTIMEDIFF(String pSTIMEDIFF) {
		PSTIMEDIFF = pSTIMEDIFF;
	}


	
	
	
}
