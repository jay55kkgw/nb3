package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "SYSDAILYSEQ")
public class SYSDAILYSEQ implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8007134015164124370L;

	@Id
	private String ADSEQID;

	private Integer ADSEQ;

	private String ADSEQMEMO;

	public Integer getADSEQ() {
		return ADSEQ;
	}

	public void setADSEQ(Integer adseq) {
		ADSEQ = adseq;
	}

	public String getADSEQID() {
		return ADSEQID;
	}

	public void setADSEQID(String adseqid) {
		ADSEQID = adseqid;
	}

	public String getADSEQMEMO() {
		return ADSEQMEMO;
	}

	public void setADSEQMEMO(String adseqmemo) {
		ADSEQMEMO = adseqmemo;
	}

	public String toString() {
		return new ToStringBuilder(this).append("adseqid", getADSEQID())
				.toString();
	}
}
