package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;


/**
 * 封測白名單
 */
@Entity(name="fstop.orm.po.ADMLOGINACLIP")
@Table(name = "ADMLOGINACLIP")
public class ADMLOGINACLIP implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -4568170350335329484L;

	@Id
	private String IP = "";
	
	private String LASTUSER = "";
	private String LASTDATE = "";
	private String LASTTIME = "";
	
	public String getIP() {
		return IP;
	}
	public void setIP(String iP) {
		IP = iP;
	}
	public String getLASTUSER() {
		return LASTUSER;
	}
	public void setLASTUSER(String lASTUSER) {
		LASTUSER = lASTUSER;
	}
	public String getLASTDATE() {
		return LASTDATE;
	}
	public void setLASTDATE(String lASTDATE) {
		LASTDATE = lASTDATE;
	}
	public String getLASTTIME() {
		return LASTTIME;
	}
	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}
}
