package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity(name="fstop.orm.po.TXNADDRESSBOOK")
@Table(name = "TXNADDRESSBOOK")
public class TXNADDRESSBOOK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7079998359571967209L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer DPADDBKID;

	private String DPUSERID;

	private String DPGONAME;

	private String DPABMAIL;

	private String LASTDATE;

	private String LASTTIME;

	public String toString() {
		return new ToStringBuilder(this).append("dpaddbkid", getDPADDBKID())
				.toString();
	}

	public String getDPABMAIL() {
		return DPABMAIL;
	}

	public void setDPABMAIL(String dpabmail) {
		DPABMAIL = dpabmail;
	}

	public Integer getDPADDBKID() {
		return DPADDBKID;
	}

	public void setDPADDBKID(Integer dpaddbkid) {
		DPADDBKID = dpaddbkid;
	}

	public String getDPGONAME() {
		return DPGONAME;
	}

	public void setDPGONAME(String dpgoname) {
		DPGONAME = dpgoname;
	}

	public String getDPUSERID() {
		return DPUSERID;
	}

	public void setDPUSERID(String dpuserid) {
		DPUSERID = dpuserid;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

}
