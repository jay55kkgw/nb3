package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity(name="fstop.orm.po.NB3SYSOP")
@Table(name = "NB3SYSOP")
public class NB3SYSOP implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8905384155005175428L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String DPACCSETID;

	private String ADOPID;

	private String ADOPNAME;

	private String ADOPENGNAME;

	private String ADOPCHSNAME;
	
	private String ADOPMEMO;

	private String ADOPALIVE;

	private String LASTUSER;

	private String LASTDATE;

	private String LASTTIME;
	
	private String ADSEQ;
	
	private String ADOPGROUPID;
	
	private String ISANONYMOUS;
	
	private String URL;
	
	private String ISPOPUP;
	
	private String ADOPAUTHTYPE;

	public String getADOPAUTHTYPE() {
		return ADOPAUTHTYPE;
	}

	public void setADOPAUTHTYPE(String aDOPAUTHTYPE) {
		ADOPAUTHTYPE = aDOPAUTHTYPE;
	}

	public String getDPACCSETID() {
		return DPACCSETID;
	}

	public void setDPACCSETID(String dPACCSETID) {
		DPACCSETID = dPACCSETID;
	}

	public String getADOPID() {
		return ADOPID;
	}

	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}

	public String getADOPNAME() {
		return ADOPNAME;
	}

	public void setADOPNAME(String aDOPNAME) {
		ADOPNAME = aDOPNAME;
	}

	public String getADOPENGNAME() {
		return ADOPENGNAME;
	}

	public void setADOPENGNAME(String aDOPENGNAME) {
		ADOPENGNAME = aDOPENGNAME;
	}

	public String getADOPCHSNAME() {
		return ADOPCHSNAME;
	}

	public void setADOPCHSNAME(String aDOPCHSNAME) {
		ADOPCHSNAME = aDOPCHSNAME;
	}

	public String getADOPMEMO() {
		return ADOPMEMO;
	}

	public void setADOPMEMO(String aDOPMEMO) {
		ADOPMEMO = aDOPMEMO;
	}

	public String getADOPALIVE() {
		return ADOPALIVE;
	}

	public void setADOPALIVE(String aDOPALIVE) {
		ADOPALIVE = aDOPALIVE;
	}

	public String getLASTUSER() {
		return LASTUSER;
	}

	public void setLASTUSER(String lASTUSER) {
		LASTUSER = lASTUSER;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lASTDATE) {
		LASTDATE = lASTDATE;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}

	public String getADSEQ() {
		return ADSEQ;
	}

	public void setADSEQ(String aDSEQ) {
		ADSEQ = aDSEQ;
	}

	public String getADOPGROUPID() {
		return ADOPGROUPID;
	}

	public void setADOPGROUPID(String aDOPGROUPID) {
		ADOPGROUPID = aDOPGROUPID;
	}

	public String getISANONYMOUS() {
		return ISANONYMOUS;
	}

	public void setISANONYMOUS(String iSANONYMOUS) {
		ISANONYMOUS = iSANONYMOUS;
	}

	public String getURL() {
		return URL;
	}

	public void setURL(String uRL) {
		URL = uRL;
	}

	public String getISPOPUP() {
		return ISPOPUP;
	}

	public void setISPOPUP(String iSPOPUP) {
		ISPOPUP = iSPOPUP;
	}

	
}
