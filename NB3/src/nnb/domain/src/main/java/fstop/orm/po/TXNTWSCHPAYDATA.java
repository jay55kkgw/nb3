package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="fstop.orm.po.TXNTWSCHPAYDATA")
@Table(name = "TXNTWSCHPAYDATA")
public class TXNTWSCHPAYDATA implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5522013503555802925L;

	@Id
	private TXNTWSCHPAYDATA_PK pks;//複合組鍵

	//移至PK
	//private String DPUSERID = "";//使用者ID
	private String ADOPID = "";//操作功能ID
	private String DPWDAC = "";//轉出帳號
	private String DPSVBH = "";//轉入行庫代碼
	private String DPSVAC = "";//轉入帳號/繳費代號
	private String DPTXAMT = "";//轉帳金額
	private String DPTXMEMO = "";//備註
	private String DPTXMAILS = "";//發送Mail清單
	private String DPTXMAILMEMO = "";//Mail備註
	private String DPTXCODE = "";//交易機制
	private String XMLCA = "";//CA 識別碼
	private String XMLCN = "";//憑證ＣＮ
	private String MAC = "";//MAC
	private String DPTXDATE = "";//實際轉帳日期
	private String DPTXTIME = "";//實際轉帳時間
	private String DPSEQ = "";//交易序號
	private String ADTXNO = "";//中心主機交易辨識序號，防主機二扣使用
	private String DPTITAINFO = "";//交易上行資訊
	private String DPTOTAINFO = "";//交易下行資訊
	private String DPSTANNO = "";//跨行序號
	private String DPEFEE = "";//手續費
	private String DPEXCODE = "";//主機回應碼/交易錯誤代碼
	private String LASTDATE = "";//最後異動日期
	private String LASTTIME = "";//最後異動時間
	private String DPRESEND = "";
	private String DPTXSTATUS = "";//交易執行狀態
	
	
	public TXNTWSCHPAYDATA_PK getPks() {
		return pks;
	}
	public void setPks(TXNTWSCHPAYDATA_PK pks) {
		this.pks = pks;
	}
	

	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getDPWDAC() {
		return DPWDAC;
	}
	public void setDPWDAC(String dPWDAC) {
		DPWDAC = dPWDAC;
	}
	public String getDPSVBH() {
		return DPSVBH;
	}
	public void setDPSVBH(String dPSVBH) {
		DPSVBH = dPSVBH;
	}
	public String getDPSVAC() {
		return DPSVAC;
	}
	public void setDPSVAC(String dPSVAC) {
		DPSVAC = dPSVAC;
	}
	public String getDPTXAMT() {
		return DPTXAMT;
	}
	public void setDPTXAMT(String dPTXAMT) {
		DPTXAMT = dPTXAMT;
	}
	public String getDPTXMEMO() {
		return DPTXMEMO;
	}
	public void setDPTXMEMO(String dPTXMEMO) {
		DPTXMEMO = dPTXMEMO;
	}
	public String getDPTXMAILS() {
		return DPTXMAILS;
	}
	public void setDPTXMAILS(String dPTXMAILS) {
		DPTXMAILS = dPTXMAILS;
	}
	public String getDPTXMAILMEMO() {
		return DPTXMAILMEMO;
	}
	public void setDPTXMAILMEMO(String dPTXMAILMEMO) {
		DPTXMAILMEMO = dPTXMAILMEMO;
	}
	public String getDPTXCODE() {
		return DPTXCODE;
	}
	public void setDPTXCODE(String dPTXCODE) {
		DPTXCODE = dPTXCODE;
	}
	public String getXMLCA() {
		return XMLCA;
	}
	public void setXMLCA(String xMLCA) {
		XMLCA = xMLCA;
	}
	public String getXMLCN() {
		return XMLCN;
	}
	public void setXMLCN(String xMLCN) {
		XMLCN = xMLCN;
	}
	public String getMAC() {
		return MAC;
	}
	public void setMAC(String mAC) {
		MAC = mAC;
	}
	public String getDPTXDATE() {
		return DPTXDATE;
	}
	public void setDPTXDATE(String dPTXDATE) {
		DPTXDATE = dPTXDATE;
	}
	public String getDPTXTIME() {
		return DPTXTIME;
	}
	public void setDPTXTIME(String dPTXTIME) {
		DPTXTIME = dPTXTIME;
	}
	public String getDPSEQ() {
		return DPSEQ;
	}
	public void setDPSEQ(String dPSEQ) {
		DPSEQ = dPSEQ;
	}
	public String getADTXNO() {
		return ADTXNO;
	}
	public void setADTXNO(String aDTXNO) {
		ADTXNO = aDTXNO;
	}
	public String getDPTITAINFO() {
		return DPTITAINFO;
	}
	public void setDPTITAINFO(String dPTITAINFO) {
		DPTITAINFO = dPTITAINFO;
	}
	public String getDPTOTAINFO() {
		return DPTOTAINFO;
	}
	public void setDPTOTAINFO(String dPTOTAINFO) {
		DPTOTAINFO = dPTOTAINFO;
	}
	public String getDPSTANNO() {
		return DPSTANNO;
	}
	public void setDPSTANNO(String dPSTANNO) {
		DPSTANNO = dPSTANNO;
	}
	public String getDPEFEE() {
		return DPEFEE;
	}
	public void setDPEFEE(String dPEFEE) {
		DPEFEE = dPEFEE;
	}
	public String getDPEXCODE() {
		return DPEXCODE;
	}
	public void setDPEXCODE(String dPEXCODE) {
		DPEXCODE = dPEXCODE;
	}
	public String getLASTDATE() {
		return LASTDATE;
	}
	public void setLASTDATE(String lASTDATE) {
		LASTDATE = lASTDATE;
	}
	public String getLASTTIME() {
		return LASTTIME;
	}
	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}
	public String getDPRESEND() {
		return DPRESEND;
	}
	public void setDPRESEND(String dPRESEND) {
		DPRESEND = dPRESEND;
	}
	public String getDPTXSTATUS() {
		return DPTXSTATUS;
	}
	public void setDPTXSTATUS(String dPTXSTATUS) {
		DPTXSTATUS = dPTXSTATUS;
	}
	
	

}
