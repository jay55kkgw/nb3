package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class ADMHOTOP_PK implements Serializable {

	private String ADUSERID;
	
	private String ADOPID;

	public String getADUSERID() {
		return ADUSERID;
	}

	public void setADUSERID(String aDUSERID) {
		ADUSERID = aDUSERID;
	}

	public String getADOPID() {
		return ADOPID;
	}

	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	
}
