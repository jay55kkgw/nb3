package fstop.orm.po;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "QUICKLOGINMAPPING")
public class QUICKLOGINMAPPING implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5236075994281599923L;

	@Id
	private QUICKLOGINMAPPING_PK pks;

	private String DEVICEID;
	private String DEVICENAME;
	private String DEVICETYPE;
	private String DEVICESTATUS;
	private String VERIFYTYPE;
	private String ERRORCNT;
	private String ISDEFAULT;
	private String LASTDATE;
	private String LASTTIME;
	private String DEVICEBRAND;
	
	
	public String getDEVICEBRAND() {
		return DEVICEBRAND;
	}
	public void setDEVICEBRAND(String dEVICEBRAND) {
		DEVICEBRAND = dEVICEBRAND;
	}
	public QUICKLOGINMAPPING_PK getPks() {
		return pks;
	}
	public void setPks(QUICKLOGINMAPPING_PK pks) {
		this.pks = pks;
	}
	public String getDEVICEID() {
		return DEVICEID;
	}
	public void setDEVICEID(String dEVICEID) {
		DEVICEID = dEVICEID;
	}
	public String getDEVICENAME() {
		return DEVICENAME;
	}
	public void setDEVICENAME(String dEVICENAME) {
		DEVICENAME = dEVICENAME;
	}
	public String getDEVICETYPE() {
		return DEVICETYPE;
	}
	public void setDEVICETYPE(String dEVICETYPE) {
		DEVICETYPE = dEVICETYPE;
	}
	public String getDEVICESTATUS() {
		return DEVICESTATUS;
	}
	public void setDEVICESTATUS(String dEVICESTATUS) {
		DEVICESTATUS = dEVICESTATUS;
	}
	public String getVERIFYTYPE() {
		return VERIFYTYPE;
	}
	public void setVERIFYTYPE(String vERIFYTYPE) {
		VERIFYTYPE = vERIFYTYPE;
	}
	public String getERRORCNT() {
		return ERRORCNT;
	}
	public void setERRORCNT(String eRRORCNT) {
		ERRORCNT = eRRORCNT;
	}
	public String getISDEFAULT() {
		return ISDEFAULT;
	}
	public void setISDEFAULT(String iSDEFAULT) {
		ISDEFAULT = iSDEFAULT;
	}
	public String getLASTDATE() {
		return LASTDATE;
	}
	public void setLASTDATE(String lASTDATE) {
		LASTDATE = lASTDATE;
	}
	public String getLASTTIME() {
		return LASTTIME;
	}
	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}


	
}
