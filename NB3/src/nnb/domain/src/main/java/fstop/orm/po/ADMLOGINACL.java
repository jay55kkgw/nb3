package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="fstop.orm.po.ADMLOGINACL")
@Table(name="ADMLOGINACL")
public class ADMLOGINACL implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6108013033874186126L;
	
	@Id
	private String ADUSERID = "";
	private String LASTUSER = "";
	private String LASTDATE = "";
	private String LASTTIME = "";
	private String NOTEDATE = "";
	
	
	public String getADUSERID() {
		return ADUSERID;
	}
	public void setADUSERID(String aDUSERID) {
		ADUSERID = aDUSERID;
	}
	public String getLASTUSER() {
		return LASTUSER;
	}
	public void setLASTUSER(String lASTUSER) {
		LASTUSER = lASTUSER;
	}
	public String getLASTDATE() {
		return LASTDATE;
	}
	public void setLASTDATE(String lASTDATE) {
		LASTDATE = lASTDATE;
	}
	public String getLASTTIME() {
		return LASTTIME;
	}
	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}
	public String getNOTEDATE() {
		return NOTEDATE;
	}
	public void setNOTEDATE(String nOTEDATE) {
		NOTEDATE = nOTEDATE;
	}

}
