package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="fstop.orm.po.OLD_TXNGDRECORD")
@Table(name = "TXNGDRECORD")
public class OLD_TXNGDRECORD implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7932639865048188386L;

	@Id
	private String ADTXNO;

	private String ADOPID = "";

	private String GDUSERID = "";

	private String GDTXDATE = "";

	private String GDTXTIME = "";

	private String GDWDAC = "";

	private String GDSVBH = "";

	private String GDSVAC = "";

	private String GDTXAMT = "";

	private String GDWEIGHT = "";
	
	private String GDPRICE = "";
	
	private String GDDISCOUNT = "";
	
	private String GDFEE1 = "";
	
	private String GDFEE2 = "";	

	private String GDTXMAILS = "";				

	private String GDTXCODE = "";   //交易機制

	private Long GDTITAINFO = 0L;

	private String GDTOTAINFO = "";

	private String GDREMAIL = "";

	private String GDTXSTATUS = "";

	private String GDEXCODE = "";

	private String GDRETXNO = "";
	
	private String GDRETXSTATUS = "";

	private String LASTDATE = "";

	private String LASTTIME = "";
		
	private String GDCERT = "";

	public String getADTXNO() {
		return ADTXNO;
	}

	public void setADTXNO(String aDTXNO) {
		ADTXNO = aDTXNO;
	}

	public String getADOPID() {
		return ADOPID;
	}

	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}

	public String getGDUSERID() {
		return GDUSERID;
	}

	public void setGDUSERID(String gDUSERID) {
		GDUSERID = gDUSERID;
	}

	public String getGDTXDATE() {
		return GDTXDATE;
	}

	public void setGDTXDATE(String gDTXDATE) {
		GDTXDATE = gDTXDATE;
	}

	public String getGDTXTIME() {
		return GDTXTIME;
	}

	public void setGDTXTIME(String gDTXTIME) {
		GDTXTIME = gDTXTIME;
	}

	public String getGDWDAC() {
		return GDWDAC;
	}

	public void setGDWDAC(String gDWDAC) {
		GDWDAC = gDWDAC;
	}

	public String getGDSVBH() {
		return GDSVBH;
	}

	public void setGDSVBH(String gDSVBH) {
		GDSVBH = gDSVBH;
	}

	public String getGDSVAC() {
		return GDSVAC;
	}

	public void setGDSVAC(String gDSVAC) {
		GDSVAC = gDSVAC;
	}

	public String getGDTXAMT() {
		return GDTXAMT;
	}

	public void setGDTXAMT(String gDTXAMT) {
		GDTXAMT = gDTXAMT;
	}

	public String getGDWEIGHT() {
		return GDWEIGHT;
	}

	public void setGDWEIGHT(String gDWEIGHT) {
		GDWEIGHT = gDWEIGHT;
	}

	public String getGDPRICE() {
		return GDPRICE;
	}

	public void setGDPRICE(String gDPRICE) {
		GDPRICE = gDPRICE;
	}

	public String getGDDISCOUNT() {
		return GDDISCOUNT;
	}

	public void setGDDISCOUNT(String gDDISCOUNT) {
		GDDISCOUNT = gDDISCOUNT;
	}

	public String getGDFEE1() {
		return GDFEE1;
	}

	public void setGDFEE1(String gDFEE1) {
		GDFEE1 = gDFEE1;
	}

	public String getGDFEE2() {
		return GDFEE2;
	}

	public void setGDFEE2(String gDFEE2) {
		GDFEE2 = gDFEE2;
	}

	public String getGDTXMAILS() {
		return GDTXMAILS;
	}

	public void setGDTXMAILS(String gDTXMAILS) {
		GDTXMAILS = gDTXMAILS;
	}

	public String getGDTXCODE() {
		return GDTXCODE;
	}

	public void setGDTXCODE(String gDTXCODE) {
		GDTXCODE = gDTXCODE;
	}

	public Long getGDTITAINFO() {
		return GDTITAINFO;
	}

	public void setGDTITAINFO(Long gDTITAINFO) {
		GDTITAINFO = gDTITAINFO;
	}

	public String getGDTOTAINFO() {
		return GDTOTAINFO;
	}

	public void setGDTOTAINFO(String gDTOTAINFO) {
		GDTOTAINFO = gDTOTAINFO;
	}

	public String getGDREMAIL() {
		return GDREMAIL;
	}

	public void setGDREMAIL(String gDREMAIL) {
		GDREMAIL = gDREMAIL;
	}

	public String getGDTXSTATUS() {
		return GDTXSTATUS;
	}

	public void setGDTXSTATUS(String gDTXSTATUS) {
		GDTXSTATUS = gDTXSTATUS;
	}

	public String getGDEXCODE() {
		return GDEXCODE;
	}

	public void setGDEXCODE(String gDEXCODE) {
		GDEXCODE = gDEXCODE;
	}

	public String getGDRETXNO() {
		return GDRETXNO;
	}

	public void setGDRETXNO(String gDRETXNO) {
		GDRETXNO = gDRETXNO;
	}

	public String getGDRETXSTATUS() {
		return GDRETXSTATUS;
	}

	public void setGDRETXSTATUS(String gDRETXSTATUS) {
		GDRETXSTATUS = gDRETXSTATUS;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lASTDATE) {
		LASTDATE = lASTDATE;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}

	public String getGDCERT() {
		return GDCERT;
	}

	public void setGDCERT(String gDCERT) {
		GDCERT = gDCERT;
	}
	
	

}
