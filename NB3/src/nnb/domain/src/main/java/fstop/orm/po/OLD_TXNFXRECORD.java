package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="fstop.orm.po.OLD_TXNFXRECORD")
@Table(name = "TXNFXRECORD")
public class OLD_TXNFXRECORD implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8419310763109401593L;

	@Id
	private String ADTXNO;

	private String ADOPID = "";

	private String FXUSERID = "";

	private String FXTXDATE = "";

	private String FXTXTIME = "";

	private String FXWDAC = "";

	private String FXSVBH = "";

	private String FXSVAC = "";

	private String FXWDCURR = "";

	private String FXWDAMT = "";

	private String FXSVCURR = "";

	private String FXSVAMT = "";

	private String FXTXMEMO = "";

	private String FXTXMAILS = "";

	private String FXTXMAILMEMO = "";

	private String FXTXCODE = "";

	private Long FXTITAINFO = 0L;
	private String FXTOTAINFO = "";

	private Long FXSCHID = 0L;

	private String FXEFEECCY = "";	
	private String FXEFEE = "";

	private String FXEXRATE = "";

	@Column(nullable = false)
	private String FXCERT = "";

	private String FXREMAIL = "";

	private String FXTXSTATUS = "";

	private String FXEXCODE = "";

	private String FXRETXNO = "";

	private String FXMSGSEQNO = "";

	private String FXMSGCONTENT = "";   //STAN

	private String FXRETXSTATUS = "";

	private String LASTDATE = "";

	private String LASTTIME = "";

	private String FXSCHNO = "";

	public String getADTXNO() {
		return ADTXNO;
	}

	public void setADTXNO(String aDTXNO) {
		ADTXNO = aDTXNO;
	}

	public String getADOPID() {
		return ADOPID;
	}

	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}

	public String getFXUSERID() {
		return FXUSERID;
	}

	public void setFXUSERID(String fXUSERID) {
		FXUSERID = fXUSERID;
	}

	public String getFXTXDATE() {
		return FXTXDATE;
	}

	public void setFXTXDATE(String fXTXDATE) {
		FXTXDATE = fXTXDATE;
	}

	public String getFXTXTIME() {
		return FXTXTIME;
	}

	public void setFXTXTIME(String fXTXTIME) {
		FXTXTIME = fXTXTIME;
	}

	public String getFXWDAC() {
		return FXWDAC;
	}

	public void setFXWDAC(String fXWDAC) {
		FXWDAC = fXWDAC;
	}

	public String getFXSVBH() {
		return FXSVBH;
	}

	public void setFXSVBH(String fXSVBH) {
		FXSVBH = fXSVBH;
	}

	public String getFXSVAC() {
		return FXSVAC;
	}

	public void setFXSVAC(String fXSVAC) {
		FXSVAC = fXSVAC;
	}

	public String getFXWDCURR() {
		return FXWDCURR;
	}

	public void setFXWDCURR(String fXWDCURR) {
		FXWDCURR = fXWDCURR;
	}

	public String getFXWDAMT() {
		return FXWDAMT;
	}

	public void setFXWDAMT(String fXWDAMT) {
		FXWDAMT = fXWDAMT;
	}

	public String getFXSVCURR() {
		return FXSVCURR;
	}

	public void setFXSVCURR(String fXSVCURR) {
		FXSVCURR = fXSVCURR;
	}

	public String getFXSVAMT() {
		return FXSVAMT;
	}

	public void setFXSVAMT(String fXSVAMT) {
		FXSVAMT = fXSVAMT;
	}

	public String getFXTXMEMO() {
		return FXTXMEMO;
	}

	public void setFXTXMEMO(String fXTXMEMO) {
		FXTXMEMO = fXTXMEMO;
	}

	public String getFXTXMAILS() {
		return FXTXMAILS;
	}

	public void setFXTXMAILS(String fXTXMAILS) {
		FXTXMAILS = fXTXMAILS;
	}

	public String getFXTXMAILMEMO() {
		return FXTXMAILMEMO;
	}

	public void setFXTXMAILMEMO(String fXTXMAILMEMO) {
		FXTXMAILMEMO = fXTXMAILMEMO;
	}

	public String getFXTXCODE() {
		return FXTXCODE;
	}

	public void setFXTXCODE(String fXTXCODE) {
		FXTXCODE = fXTXCODE;
	}

	public Long getFXTITAINFO() {
		return FXTITAINFO;
	}

	public void setFXTITAINFO(Long fXTITAINFO) {
		FXTITAINFO = fXTITAINFO;
	}

	public String getFXTOTAINFO() {
		return FXTOTAINFO;
	}

	public void setFXTOTAINFO(String fXTOTAINFO) {
		FXTOTAINFO = fXTOTAINFO;
	}

	public Long getFXSCHID() {
		return FXSCHID;
	}

	public void setFXSCHID(Long fXSCHID) {
		FXSCHID = fXSCHID;
	}

	public String getFXEFEECCY() {
		return FXEFEECCY;
	}

	public void setFXEFEECCY(String fXEFEECCY) {
		FXEFEECCY = fXEFEECCY;
	}

	public String getFXEFEE() {
		return FXEFEE;
	}

	public void setFXEFEE(String fXEFEE) {
		FXEFEE = fXEFEE;
	}

	public String getFXEXRATE() {
		return FXEXRATE;
	}

	public void setFXEXRATE(String fXEXRATE) {
		FXEXRATE = fXEXRATE;
	}

	public String getFXCERT() {
		return FXCERT;
	}

	public void setFXCERT(String fXCERT) {
		FXCERT = fXCERT;
	}

	public String getFXREMAIL() {
		return FXREMAIL;
	}

	public void setFXREMAIL(String fXREMAIL) {
		FXREMAIL = fXREMAIL;
	}

	public String getFXTXSTATUS() {
		return FXTXSTATUS;
	}

	public void setFXTXSTATUS(String fXTXSTATUS) {
		FXTXSTATUS = fXTXSTATUS;
	}

	public String getFXEXCODE() {
		return FXEXCODE;
	}

	public void setFXEXCODE(String fXEXCODE) {
		FXEXCODE = fXEXCODE;
	}

	public String getFXRETXNO() {
		return FXRETXNO;
	}

	public void setFXRETXNO(String fXRETXNO) {
		FXRETXNO = fXRETXNO;
	}

	public String getFXMSGSEQNO() {
		return FXMSGSEQNO;
	}

	public void setFXMSGSEQNO(String fXMSGSEQNO) {
		FXMSGSEQNO = fXMSGSEQNO;
	}

	public String getFXMSGCONTENT() {
		return FXMSGCONTENT;
	}

	public void setFXMSGCONTENT(String fXMSGCONTENT) {
		FXMSGCONTENT = fXMSGCONTENT;
	}

	public String getFXRETXSTATUS() {
		return FXRETXSTATUS;
	}

	public void setFXRETXSTATUS(String fXRETXSTATUS) {
		FXRETXSTATUS = fXRETXSTATUS;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lASTDATE) {
		LASTDATE = lASTDATE;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}

	public String getFXSCHNO() {
		return FXSCHNO;
	}

	public void setFXSCHNO(String fXSCHNO) {
		FXSCHNO = fXSCHNO;
	}
	
	

}
