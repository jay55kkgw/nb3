package fstop.orm.po;

import java.io.Serializable;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

//TODO need to change dependency
//import fstop.util.JSONUtils;

@Entity(name="fstop.orm.po.TXNREQINFO")
@Table(name = "TXNREQINFO")
public class TXNREQINFO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1009384459958098523L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long REQID;

	private String REQINFO;

	public Long getREQID() {
		return REQID;
	}

	public void setREQID(Long reqid) {
		REQID = reqid;
	}

	public String toString() {
		return new ToStringBuilder(this).append("reqid", getREQID())
				.toString();
	}

	public String getREQINFO() {

//TODO need to change dependency		
//		Map<String, String> paramsMap = JSONUtils.json2map(REQINFO);
//		paramsMap.remove("N950PASSWORD");
//		paramsMap.remove("CMPASSWORD");
//		paramsMap.remove("CMPWD");		
//				
//		return JSONUtils.map2json(paramsMap);
		return REQINFO;
	}

	public void setREQINFO(String reqinfo) {

//TODO need to change dependency		
//		Map<String, String> paramsMap = JSONUtils.json2map(reqinfo);
//		paramsMap.remove("N950PASSWORD");
//		paramsMap.remove("CMPASSWORD");
//		paramsMap.remove("CMPWD");
//
//		REQINFO = JSONUtils.map2json(paramsMap);
		REQINFO = null;
	}

}
