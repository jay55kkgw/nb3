package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="fstop.orm.po.OLD_TXNFXSCHEDULE")
@Table(name = "TXNFXSCHEDULE")
public class OLD_TXNFXSCHEDULE implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7407687522724294091L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long FXSCHID;

	private String ADOPID = "";
	
	private String FXUSERID = "";

	private String FXPERMTDATE = "";

	private String FXFDATE = "";

	private String FXTDATE = "";

	private String FXNEXTDATE = "";

	private String FXWDAC = "";

	private String FXSVAC = "";

	private String FXWDAMT = "";

	private String FXSVAMT = "";

	private String FXTXMEMO = "";

	private String FXTXMAILS = "";
	
	private String FXTXMAILMEMO = "";

	private String FXTXCODE = "";

	private Long FXTXINFO = 0l;

	private String FXSDATE = "";

	private String FXSTIME = "";

	private String FXTXSTATUS = "";

	private String FXSVCURR = "";

	private String FXWDCURR = "";
	
	private String FXSVBH = "";
	
	private String XMLCA = "";
	
	private String XMLCN = "";
	
	private String MAC = "";
	
	private String LASTDATE = "";
	
	private String LASTTIME = "";
	
	private String FXSCHNO = "";
	
	private String LOGINTYPE = ""; //NB:網銀 ， MB:行動 ，預設為NB

	public Long getFXSCHID() {
		return FXSCHID;
	}

	public void setFXSCHID(Long fXSCHID) {
		FXSCHID = fXSCHID;
	}

	public String getADOPID() {
		return ADOPID;
	}

	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}

	public String getFXUSERID() {
		return FXUSERID;
	}

	public void setFXUSERID(String fXUSERID) {
		FXUSERID = fXUSERID;
	}

	public String getFXPERMTDATE() {
		return FXPERMTDATE;
	}

	public void setFXPERMTDATE(String fXPERMTDATE) {
		FXPERMTDATE = fXPERMTDATE;
	}

	public String getFXFDATE() {
		return FXFDATE;
	}

	public void setFXFDATE(String fXFDATE) {
		FXFDATE = fXFDATE;
	}

	public String getFXTDATE() {
		return FXTDATE;
	}

	public void setFXTDATE(String fXTDATE) {
		FXTDATE = fXTDATE;
	}

	public String getFXNEXTDATE() {
		return FXNEXTDATE;
	}

	public void setFXNEXTDATE(String fXNEXTDATE) {
		FXNEXTDATE = fXNEXTDATE;
	}

	public String getFXWDAC() {
		return FXWDAC;
	}

	public void setFXWDAC(String fXWDAC) {
		FXWDAC = fXWDAC;
	}

	public String getFXSVAC() {
		return FXSVAC;
	}

	public void setFXSVAC(String fXSVAC) {
		FXSVAC = fXSVAC;
	}

	public String getFXWDAMT() {
		return FXWDAMT;
	}

	public void setFXWDAMT(String fXWDAMT) {
		FXWDAMT = fXWDAMT;
	}

	public String getFXSVAMT() {
		return FXSVAMT;
	}

	public void setFXSVAMT(String fXSVAMT) {
		FXSVAMT = fXSVAMT;
	}

	public String getFXTXMEMO() {
		return FXTXMEMO;
	}

	public void setFXTXMEMO(String fXTXMEMO) {
		FXTXMEMO = fXTXMEMO;
	}

	public String getFXTXMAILS() {
		return FXTXMAILS;
	}

	public void setFXTXMAILS(String fXTXMAILS) {
		FXTXMAILS = fXTXMAILS;
	}

	public String getFXTXMAILMEMO() {
		return FXTXMAILMEMO;
	}

	public void setFXTXMAILMEMO(String fXTXMAILMEMO) {
		FXTXMAILMEMO = fXTXMAILMEMO;
	}

	public String getFXTXCODE() {
		return FXTXCODE;
	}

	public void setFXTXCODE(String fXTXCODE) {
		FXTXCODE = fXTXCODE;
	}

	public Long getFXTXINFO() {
		return FXTXINFO;
	}

	public void setFXTXINFO(Long fXTXINFO) {
		FXTXINFO = fXTXINFO;
	}

	public String getFXSDATE() {
		return FXSDATE;
	}

	public void setFXSDATE(String fXSDATE) {
		FXSDATE = fXSDATE;
	}

	public String getFXSTIME() {
		return FXSTIME;
	}

	public void setFXSTIME(String fXSTIME) {
		FXSTIME = fXSTIME;
	}

	public String getFXTXSTATUS() {
		return FXTXSTATUS;
	}

	public void setFXTXSTATUS(String fXTXSTATUS) {
		FXTXSTATUS = fXTXSTATUS;
	}

	public String getFXSVCURR() {
		return FXSVCURR;
	}

	public void setFXSVCURR(String fXSVCURR) {
		FXSVCURR = fXSVCURR;
	}

	public String getFXWDCURR() {
		return FXWDCURR;
	}

	public void setFXWDCURR(String fXWDCURR) {
		FXWDCURR = fXWDCURR;
	}

	public String getFXSVBH() {
		return FXSVBH;
	}

	public void setFXSVBH(String fXSVBH) {
		FXSVBH = fXSVBH;
	}

	public String getXMLCA() {
		return XMLCA;
	}

	public void setXMLCA(String xMLCA) {
		XMLCA = xMLCA;
	}

	public String getXMLCN() {
		return XMLCN;
	}

	public void setXMLCN(String xMLCN) {
		XMLCN = xMLCN;
	}

	public String getMAC() {
		return MAC;
	}

	public void setMAC(String mAC) {
		MAC = mAC;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lASTDATE) {
		LASTDATE = lASTDATE;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}

	public String getFXSCHNO() {
		return FXSCHNO;
	}

	public void setFXSCHNO(String fXSCHNO) {
		FXSCHNO = fXSCHNO;
	}

	public String getLOGINTYPE() {
		return LOGINTYPE;
	}

	public void setLOGINTYPE(String lOGINTYPE) {
		LOGINTYPE = lOGINTYPE;
	}


}
