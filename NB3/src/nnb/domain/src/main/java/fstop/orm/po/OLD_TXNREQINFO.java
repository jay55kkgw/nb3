package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="fstop.orm.po.OLD_TXNREQINFO")
@Table(name = "TXNREQINFO")
public class OLD_TXNREQINFO implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -4402544719476889906L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long REQID;

	private String REQINFO;

	public Long getREQID() {
		return REQID;
	}

	public void setREQID(Long rEQID) {
		REQID = rEQID;
	}

	public String getREQINFO() {
		return REQINFO;
	}

	public void setREQINFO(String rEQINFO) {
		REQINFO = rEQINFO;
	}
	
	

}
