package fstop.orm.po;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "ADMLIONCARDDATA")
public class ADMLIONCARDDATA implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String ADMID;
	
	private String ZONE;

	private String TEAM;

	public String getADMID() {
		return ADMID;
	}

	public void setADMID(String aDMID) {
		ADMID = aDMID;
	}

	public String getZONE() {
		return ZONE;
	}

	public void setZONE(String zone) {
		ZONE = zone;
	}

	public String getTEAM() {
		return TEAM;
	}

	public void setTEAM(String team) {
		TEAM = team;
	}

}
