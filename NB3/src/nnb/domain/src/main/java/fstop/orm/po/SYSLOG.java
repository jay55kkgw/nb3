package fstop.orm.po;

import java.io.Serializable;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

//TODO need to change dependency
//import fstop.util.JSONUtils;

@Entity
@Table(name = "SYSLOG")
public class SYSLOG implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3005408490237994676L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long SYSLOGID;

	private String ADUSERID;

	private String ADFDATE;

	private String ADFTIME;

	private String ADTDATE;

	private String ADTTIME;

	private Integer ADSEQ;	

	private String ADTXNO = "";

	private String ADPAGENM = "";

	private String ADOPID = "";

	private String ADBONM = "";

	private String ADMETHODNM = "";

	private String ADUSERIP = "";

	private String ADLOGTYPE = "";
	
	private String ADCONTENT = "";

	private String ADEXCODE = "";

	private String ADLOGGING = "";

	private String ADTXACNO = "";

	private String ADTXAMT = "";

	private String ADTXTYPE = "";
	
	private String ADCURRENCY = "";
	
	private String FGTXWAY = "";
	
	private String ADTXID = "";  //指出哪一個電文有誤
	
	private String ADGUID = "";
	
	private String LOGINTYPE = "NB"; //NB:網銀 ， MB:行動 ，預設為NB

	@Override
	public String toString() {
		return "SYSLOG [SYSLOGID=" + SYSLOGID + ", ADUSERID=" + ADUSERID + ", ADFDATE=" + ADFDATE + ", ADFTIME="
				+ ADFTIME + ", ADTDATE=" + ADTDATE + ", ADTTIME=" + ADTTIME + ", ADSEQ=" + ADSEQ + ", ADTXNO=" + ADTXNO
				+ ", ADPAGENM=" + ADPAGENM + ", ADOPID=" + ADOPID + ", ADBONM=" + ADBONM + ", ADMETHODNM=" + ADMETHODNM
				+ ", ADUSERIP=" + ADUSERIP + ", ADLOGTYPE=" + ADLOGTYPE + ", ADCONTENT=" + ADCONTENT + ", ADEXCODE="
				+ ADEXCODE + ", ADLOGGING=" + ADLOGGING + ", ADTXACNO=" + ADTXACNO + ", ADTXAMT=" + ADTXAMT
				+ ", ADTXTYPE=" + ADTXTYPE + ", ADCURRENCY=" + ADCURRENCY + ", FGTXWAY=" + FGTXWAY + ", ADTXID="
				+ ADTXID + ", ADGUID=" + ADGUID + ", LOGINTYPE=" + LOGINTYPE + "]";
	}

	public Long getSYSLOGID() {
		return SYSLOGID;
	}

	public void setSYSLOGID(Long sYSLOGID) {
		SYSLOGID = sYSLOGID;
	}

	public String getADUSERID() {
		return ADUSERID;
	}

	public void setADUSERID(String aDUSERID) {
		ADUSERID = aDUSERID;
	}

	public String getADFDATE() {
		return ADFDATE;
	}

	public void setADFDATE(String aDFDATE) {
		ADFDATE = aDFDATE;
	}

	public String getADFTIME() {
		return ADFTIME;
	}

	public void setADFTIME(String aDFTIME) {
		ADFTIME = aDFTIME;
	}

	public String getADTDATE() {
		return ADTDATE;
	}

	public void setADTDATE(String aDTDATE) {
		ADTDATE = aDTDATE;
	}

	public String getADTTIME() {
		return ADTTIME;
	}

	public void setADTTIME(String aDTTIME) {
		ADTTIME = aDTTIME;
	}

	public Integer getADSEQ() {
		return ADSEQ;
	}

	public void setADSEQ(Integer aDSEQ) {
		ADSEQ = aDSEQ;
	}

	public String getADTXNO() {
		return ADTXNO;
	}

	public void setADTXNO(String aDTXNO) {
		ADTXNO = aDTXNO;
	}

	public String getADPAGENM() {
		return ADPAGENM;
	}

	public void setADPAGENM(String aDPAGENM) {
		ADPAGENM = aDPAGENM;
	}

	public String getADOPID() {
		return ADOPID;
	}

	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}

	public String getADBONM() {
		return ADBONM;
	}

	public void setADBONM(String aDBONM) {
		ADBONM = aDBONM;
	}

	public String getADMETHODNM() {
		return ADMETHODNM;
	}

	public void setADMETHODNM(String aDMETHODNM) {
		ADMETHODNM = aDMETHODNM;
	}

	public String getADUSERIP() {
		return ADUSERIP;
	}

	public void setADUSERIP(String aDUSERIP) {
		ADUSERIP = aDUSERIP;
	}

	public String getADLOGTYPE() {
		return ADLOGTYPE;
	}

	public void setADLOGTYPE(String aDLOGTYPE) {
		ADLOGTYPE = aDLOGTYPE;
	}

	public String getADCONTENT() {
		return ADCONTENT;
	}

	public void setADCONTENT(String aDCONTENT) {
		ADCONTENT = aDCONTENT;
	}

	public String getADEXCODE() {
		return ADEXCODE;
	}

	public void setADEXCODE(String aDEXCODE) {
		ADEXCODE = aDEXCODE;
	}

	public String getADLOGGING() {
		return ADLOGGING;
	}

	public void setADLOGGING(String aDLOGGING) {
		ADLOGGING = aDLOGGING;
	}

	public String getADTXACNO() {
		return ADTXACNO;
	}

	public void setADTXACNO(String aDTXACNO) {
		ADTXACNO = aDTXACNO;
	}

	public String getADTXAMT() {
		return ADTXAMT;
	}

	public void setADTXAMT(String aDTXAMT) {
		ADTXAMT = aDTXAMT;
	}

	public String getADTXTYPE() {
		return ADTXTYPE;
	}

	public void setADTXTYPE(String aDTXTYPE) {
		ADTXTYPE = aDTXTYPE;
	}

	public String getADCURRENCY() {
		return ADCURRENCY;
	}

	public void setADCURRENCY(String aDCURRENCY) {
		ADCURRENCY = aDCURRENCY;
	}

	public String getFGTXWAY() {
		return FGTXWAY;
	}

	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}

	public String getADTXID() {
		return ADTXID;
	}

	public void setADTXID(String aDTXID) {
		ADTXID = aDTXID;
	}

	public String getADGUID() {
		return ADGUID;
	}

	public void setADGUID(String aDGUID) {
		ADGUID = aDGUID;
	}

	public String getLOGINTYPE() {
		return LOGINTYPE;
	}

	public void setLOGINTYPE(String lOGINTYPE) {
		LOGINTYPE = lOGINTYPE;
	}
	
	
	
	
}
