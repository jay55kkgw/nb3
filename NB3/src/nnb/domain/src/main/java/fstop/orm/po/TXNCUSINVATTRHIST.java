package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity(name="fstop.orm.po.TXNCUSINVATTRHIST")
@Table(name = "TXNCUSINVATTRHIST")
public class TXNCUSINVATTRHIST implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5144113354161183763L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long FDHISTID;
	private String FDUSERID;
	private String FDQ1;
	private String FDQ2;
	private String FDQ3;
	private String FDQ4;
	private String FDQ5;
	private String FDSCORE;
	private String FDINVTYPE;
	private String LASTUSER;
	private String LASTDATE;
	private String LASTTIME;
	private String FDQ6;
	private String FDQ7;
	private String FDQ8;
	private String FDQ9;
	private String FDQ10;
	private String FDQ11;
	private String FDQ12;
	private String FDQ13;
	private String FDQ14;
	private String FDQ15;
	private String MARK1;
	//20200226註解 ( 如要上KYC功能修正再打開 )
	private String ADUSERIP;
	private String AGREE;
	
	public Long getFDHISTID() {
		return FDHISTID;
	}
	public void setFDHISTID(Long fDHISTID) {
		FDHISTID = fDHISTID;
	}
	public String getFDUSERID() {
		return FDUSERID;
	}
	public void setFDUSERID(String fDUSERID) {
		FDUSERID = fDUSERID;
	}
	public String getFDQ1() {
		return FDQ1;
	}
	public void setFDQ1(String fDQ1) {
		FDQ1 = fDQ1;
	}
	public String getFDQ2() {
		return FDQ2;
	}
	public void setFDQ2(String fDQ2) {
		FDQ2 = fDQ2;
	}
	public String getFDQ3() {
		return FDQ3;
	}
	public void setFDQ3(String fDQ3) {
		FDQ3 = fDQ3;
	}
	public String getFDQ4() {
		return FDQ4;
	}
	public void setFDQ4(String fDQ4) {
		FDQ4 = fDQ4;
	}
	public String getFDQ5() {
		return FDQ5;
	}
	public void setFDQ5(String fDQ5) {
		FDQ5 = fDQ5;
	}
	public String getFDSCORE() {
		return FDSCORE;
	}
	public void setFDSCORE(String fDSCORE) {
		FDSCORE = fDSCORE;
	}
	public String getFDINVTYPE() {
		return FDINVTYPE;
	}
	public void setFDINVTYPE(String fDINVTYPE) {
		FDINVTYPE = fDINVTYPE;
	}
	public String getLASTUSER() {
		return LASTUSER;
	}
	public void setLASTUSER(String lASTUSER) {
		LASTUSER = lASTUSER;
	}
	public String getLASTDATE() {
		return LASTDATE;
	}
	public void setLASTDATE(String lASTDATE) {
		LASTDATE = lASTDATE;
	}
	public String getLASTTIME() {
		return LASTTIME;
	}
	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}
	public String getFDQ6() {
		return FDQ6;
	}
	public void setFDQ6(String fDQ6) {
		FDQ6 = fDQ6;
	}
	public String getFDQ7() {
		return FDQ7;
	}
	public void setFDQ7(String fDQ7) {
		FDQ7 = fDQ7;
	}
	public String getFDQ8() {
		return FDQ8;
	}
	public void setFDQ8(String fDQ8) {
		FDQ8 = fDQ8;
	}
	public String getFDQ9() {
		return FDQ9;
	}
	public void setFDQ9(String fDQ9) {
		FDQ9 = fDQ9;
	}
	public String getFDQ10() {
		return FDQ10;
	}
	public void setFDQ10(String fDQ10) {
		FDQ10 = fDQ10;
	}
	public String getFDQ11() {
		return FDQ11;
	}
	public void setFDQ11(String fDQ11) {
		FDQ11 = fDQ11;
	}
	public String getFDQ12() {
		return FDQ12;
	}
	public void setFDQ12(String fDQ12) {
		FDQ12 = fDQ12;
	}
	public String getFDQ13() {
		return FDQ13;
	}
	public void setFDQ13(String fDQ13) {
		FDQ13 = fDQ13;
	}
	public String getFDQ14() {
		return FDQ14;
	}
	public void setFDQ14(String fDQ14) {
		FDQ14 = fDQ14;
	}
	public String getFDQ15() {
		return FDQ15;
	}
	public void setFDQ15(String fDQ15) {
		FDQ15 = fDQ15;
	}
	public String getMARK1() {
		return MARK1;
	}
	public void setMARK1(String mARK1) {
		MARK1 = mARK1;
	}
	public String getADUSERIP() {
		return ADUSERIP;
	}
	public void setADUSERIP(String aDUSERIP) {
		ADUSERIP = aDUSERIP;
	}
	public String getAGREE() {
		return AGREE;
	}
	public void setAGREE(String aGREE) {
		AGREE = aGREE;
	}
}
