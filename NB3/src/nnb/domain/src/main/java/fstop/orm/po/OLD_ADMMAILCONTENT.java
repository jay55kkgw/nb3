package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="fstop.orm.po.OLD_ADMMAILCONTENT")
@Table(name = "ADMMAILCONTENT")
public class OLD_ADMMAILCONTENT implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8411639849031108225L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long ADMAILID;

	private String ADSUBJECT = "";

	@Column(nullable = false)
	private String ADMAILCONTENT = "";

	public Long getADMAILID() {
		return ADMAILID;
	}

	public void setADMAILID(Long aDMAILID) {
		ADMAILID = aDMAILID;
	}

	public String getADSUBJECT() {
		return ADSUBJECT;
	}

	public void setADSUBJECT(String aDSUBJECT) {
		ADSUBJECT = aDSUBJECT;
	}

	public String getADMAILCONTENT() {
		return ADMAILCONTENT;
	}

	public void setADMAILCONTENT(String aDMAILCONTENT) {
		ADMAILCONTENT = aDMAILCONTENT;
	} 


}
