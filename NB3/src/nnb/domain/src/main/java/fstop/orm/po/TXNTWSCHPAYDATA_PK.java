package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class TXNTWSCHPAYDATA_PK implements Serializable {
		

	/**
	 * 
	 */
	private static final long serialVersionUID = 1914490891589621321L;

	private String DPSCHNO = "";	//預約批號Guid(36位)
	
	private String DPSCHTXDATE = "";//預約轉帳日
	
	private String DPUSERID = "";//統編

	public String getDPSCHNO() {
		return DPSCHNO;
	}

	public void setDPSCHNO(String dPSCHNO) {
		DPSCHNO = dPSCHNO;
	}

	public String getDPSCHTXDATE() {
		return DPSCHTXDATE;
	}

	public void setDPSCHTXDATE(String dPSCHTXDATE) {
		DPSCHTXDATE = dPSCHTXDATE;
	}

	public String getDPUSERID() {
		return DPUSERID;
	}

	public void setDPUSERID(String dPUSERID) {
		DPUSERID = dPUSERID;
	}

	
	public TXNTWSCHPAYDATA_PK() {
	}


	public TXNTWSCHPAYDATA_PK(String dPSCHNO, String dPSCHTXDATE , String dPUSERID) {
		super();
		DPSCHNO = dPSCHNO;
		DPSCHTXDATE = dPSCHTXDATE;
		DPUSERID = dPUSERID;
	}
	
	


}
