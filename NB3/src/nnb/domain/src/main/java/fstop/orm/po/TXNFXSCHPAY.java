package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="fstop.orm.po.TXNFXSCHPAY")
@Table(name = "TXNFXSCHPAY")
public class TXNFXSCHPAY implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -126693951490854880L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer FXSCHID;			// 外幣預約流水號
	
	private String ADOPID = "";			// 操作功能ID
	private String FXUSERID = "";		// 使用者ID
	private String FXTXTYPE = "";		// 單次/循環轉帳
	private String FXPERMTDATE = "";	// 每月轉帳日
	private String FXFDATE = "";		// 生效日
	private String FXTDATE = "";		// 截止日
	private String FXWDAC = "";			// 轉出帳號
	private String FXWDCURR = "";		// 轉出幣別
	private String FXWDAMT = "";		// 轉出金額
	private String FXSVBH = "";			// 轉入行庫代碼
	private String FXSVAC = "";			// 轉入帳號
	private String FXSVCURR = "";		// 轉入幣別
	private String FXSVAMT = "";		// 轉入金額
	private String FXTXMEMO = "";		// 備註
	private String FXTXMAILS = "";		// 發送Mail清單
	private String FXTXMAILMEMO = "";	// Mail備註
	private String FXTXCODE = "";		// 交易機制
	private String FXSDATE = "";		// 預約設定日期
	private String FXSTIME = "";		// 預約設定時間
	private String FXTXSTATUS = "";		// 預約狀態
	private String XMLCA = "";			// CA 識別碼
	private String XMLCN = "";			// 憑證ＣＮ
	private String MAC = "";			// MAC
	private String FXTXINFO = "";		// 預約時上行電文必要資訊
	private String LASTDATE = "";		// 最後異動日期
	private String LASTTIME = "";		// 最後異動時間
	private String FXSCHNO = "";		// 預約批號Guid(36位)
	private String LOGINTYPE = "";		// NB登入或MB登入
	private String MSADDR= "";			//交易所屬微服務
	
	public String getMSADDR() {
		return MSADDR;
	}
	public void setMSADDR(String mSADDR) {
		MSADDR = mSADDR;
	}
	public Integer getFXSCHID() {
		return FXSCHID;
	}
	public void setFXSCHID(Integer fXSCHID) {
		FXSCHID = fXSCHID;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getFXUSERID() {
		return FXUSERID;
	}
	public void setFXUSERID(String fXUSERID) {
		FXUSERID = fXUSERID;
	}
	public String getFXTXTYPE() {
		return FXTXTYPE;
	}
	public void setFXTXTYPE(String fXTXTYPE) {
		FXTXTYPE = fXTXTYPE;
	}
	public String getFXPERMTDATE() {
		return FXPERMTDATE;
	}
	public void setFXPERMTDATE(String fXPERMTDATE) {
		FXPERMTDATE = fXPERMTDATE;
	}
	public String getFXFDATE() {
		return FXFDATE;
	}
	public void setFXFDATE(String fXFDATE) {
		FXFDATE = fXFDATE;
	}
	public String getFXTDATE() {
		return FXTDATE;
	}
	public void setFXTDATE(String fXTDATE) {
		FXTDATE = fXTDATE;
	}
	public String getFXWDAC() {
		return FXWDAC;
	}
	public void setFXWDAC(String fXWDAC) {
		FXWDAC = fXWDAC;
	}
	public String getFXWDCURR() {
		return FXWDCURR;
	}
	public void setFXWDCURR(String fXWDCURR) {
		FXWDCURR = fXWDCURR;
	}
	public String getFXWDAMT() {
		return FXWDAMT;
	}
	public void setFXWDAMT(String fXWDAMT) {
		FXWDAMT = fXWDAMT;
	}
	public String getFXSVBH() {
		return FXSVBH;
	}
	public void setFXSVBH(String fXSVBH) {
		FXSVBH = fXSVBH;
	}
	public String getFXSVAC() {
		return FXSVAC;
	}
	public void setFXSVAC(String fXSVAC) {
		FXSVAC = fXSVAC;
	}
	public String getFXSVCURR() {
		return FXSVCURR;
	}
	public void setFXSVCURR(String fXSVCURR) {
		FXSVCURR = fXSVCURR;
	}
	public String getFXSVAMT() {
		return FXSVAMT;
	}
	public void setFXSVAMT(String fXSVAMT) {
		FXSVAMT = fXSVAMT;
	}
	public String getFXTXMEMO() {
		return FXTXMEMO;
	}
	public void setFXTXMEMO(String fXTXMEMO) {
		FXTXMEMO = fXTXMEMO;
	}
	public String getFXTXMAILS() {
		return FXTXMAILS;
	}
	public void setFXTXMAILS(String fXTXMAILS) {
		FXTXMAILS = fXTXMAILS;
	}
	public String getFXTXMAILMEMO() {
		return FXTXMAILMEMO;
	}
	public void setFXTXMAILMEMO(String fXTXMAILMEMO) {
		FXTXMAILMEMO = fXTXMAILMEMO;
	}
	public String getFXTXCODE() {
		return FXTXCODE;
	}
	public void setFXTXCODE(String fXTXCODE) {
		FXTXCODE = fXTXCODE;
	}
	public String getFXSDATE() {
		return FXSDATE;
	}
	public void setFXSDATE(String fXSDATE) {
		FXSDATE = fXSDATE;
	}
	public String getFXSTIME() {
		return FXSTIME;
	}
	public void setFXSTIME(String fXSTIME) {
		FXSTIME = fXSTIME;
	}
	public String getFXTXSTATUS() {
		return FXTXSTATUS;
	}
	public void setFXTXSTATUS(String fXTXSTATUS) {
		FXTXSTATUS = fXTXSTATUS;
	}
	public String getXMLCA() {
		return XMLCA;
	}
	public void setXMLCA(String xMLCA) {
		XMLCA = xMLCA;
	}
	public String getXMLCN() {
		return XMLCN;
	}
	public void setXMLCN(String xMLCN) {
		XMLCN = xMLCN;
	}
	public String getMAC() {
		return MAC;
	}
	public void setMAC(String mAC) {
		MAC = mAC;
	}
	public String getFXTXINFO() {
		return FXTXINFO;
	}
	public void setFXTXINFO(String fXTXINFO) {
		FXTXINFO = fXTXINFO;
	}
	public String getLASTDATE() {
		return LASTDATE;
	}
	public void setLASTDATE(String lASTDATE) {
		LASTDATE = lASTDATE;
	}
	public String getLASTTIME() {
		return LASTTIME;
	}
	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}
	public String getFXSCHNO() {
		return FXSCHNO;
	}
	public void setFXSCHNO(String fXSCHNO) {
		FXSCHNO = fXSCHNO;
	}
	public String getLOGINTYPE() {
		return LOGINTYPE;
	}
	public void setLOGINTYPE(String lOGINTYPE) {
		LOGINTYPE = lOGINTYPE;
	}
	
	
	
}
