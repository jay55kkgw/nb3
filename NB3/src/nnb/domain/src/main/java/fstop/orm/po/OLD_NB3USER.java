package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="fstop.orm.po.OLD_NB3USER")
@Table(name = "NB3USER")
public class OLD_NB3USER implements Serializable {
	
	//一定要有 @Id，否則啟動伺服器時會發生錯誤
	@Id
	//@GeneratedValue(strategy = GenerationType.AUTO)
	private String NB3USERID;

	private String NB3STATUS;

	private String LASTDATE;

	private String LASTTIME;

	
	
	public String getNB3USERID() {
		return NB3USERID;
	}

	public void setNB3USERID(String nB3USERID) {
		NB3USERID = nB3USERID;
	}

	public String getNB3STATUS() {
		return NB3STATUS;
	}

	public void setNB3STATUS(String nB3STATUS) {
		NB3STATUS = nB3STATUS;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

}
