package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ONLINE_APPOINTMENT_TXNLOG")
public class ONLINE_APPOINTMENT_TXNLOG implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8423151768462421079L;
	@Id
	private ONLINE_APPOINTMENT_TXNLOG_PK pks;//複合組鍵
	
	private String TXNDATETIME="";
	private String BILLERBAN="";
	private String BUSTYPE="";
	private String BILLERID="";
	private String PAYMENTTYPE="";
	
	private String FEEID="";
	private String CUSTIDNBAN="";
	private String CUSTBILLERACNT="";
	private String CUSTBANKACNT="";
	private String DIVDATA="";
	
	private String ICV="";
	private String MAC="";
	private String RCODE="";
	private String ORIGTXNDATETIME="";
	private String ORIGSTAN="";
	
	private String LASTDATE="";
	private String LASTTIME="";
	private String RESULTDATA="";
	
	public ONLINE_APPOINTMENT_TXNLOG_PK getPks() {
		return pks;
	}
	public void setPks(ONLINE_APPOINTMENT_TXNLOG_PK pks) {
		this.pks = pks;
	}
	public String getTXNDATETIME() {
		return TXNDATETIME;
	}
	public void setTXNDATETIME(String tXNDATETIME) {
		TXNDATETIME = tXNDATETIME;
	}
	public String getBILLERBAN() {
		return BILLERBAN;
	}
	public void setBILLERBAN(String bILLERBAN) {
		BILLERBAN = bILLERBAN;
	}
	public String getBUSTYPE() {
		return BUSTYPE;
	}
	public void setBUSTYPE(String bUSTYPE) {
		BUSTYPE = bUSTYPE;
	}
	public String getBILLERID() {
		return BILLERID;
	}
	public void setBILLERID(String bILLERID) {
		BILLERID = bILLERID;
	}
	public String getPAYMENTTYPE() {
		return PAYMENTTYPE;
	}
	public void setPAYMENTTYPE(String pAYMENTTYPE) {
		PAYMENTTYPE = pAYMENTTYPE;
	}
	public String getFEEID() {
		return FEEID;
	}
	public void setFEEID(String fEEID) {
		FEEID = fEEID;
	}
	public String getCUSTIDNBAN() {
		return CUSTIDNBAN;
	}
	public void setCUSTIDNBAN(String cUSTIDNBAN) {
		CUSTIDNBAN = cUSTIDNBAN;
	}
	public String getCUSTBILLERACNT() {
		return CUSTBILLERACNT;
	}
	public void setCUSTBILLERACNT(String cUSTBILLERACNT) {
		CUSTBILLERACNT = cUSTBILLERACNT;
	}
	public String getCUSTBANKACNT() {
		return CUSTBANKACNT;
	}
	public void setCUSTBANKACNT(String cUSTBANKACNT) {
		CUSTBANKACNT = cUSTBANKACNT;
	}
	public String getDIVDATA() {
		return DIVDATA;
	}
	public void setDIVDATA(String dIVDATA) {
		DIVDATA = dIVDATA;
	}
	public String getICV() {
		return ICV;
	}
	public void setICV(String iCV) {
		ICV = iCV;
	}
	public String getMAC() {
		return MAC;
	}
	public void setMAC(String mAC) {
		MAC = mAC;
	}
	public String getRCODE() {
		return RCODE;
	}
	public void setRCODE(String rCODE) {
		RCODE = rCODE;
	}
	public String getORIGTXNDATETIME() {
		return ORIGTXNDATETIME;
	}
	public void setORIGTXNDATETIME(String oRIGTXNDATETIME) {
		ORIGTXNDATETIME = oRIGTXNDATETIME;
	}
	public String getORIGSTAN() {
		return ORIGSTAN;
	}
	public void setORIGSTAN(String oRIGSTAN) {
		ORIGSTAN = oRIGSTAN;
	}
	public String getLASTDATE() {
		return LASTDATE;
	}
	public void setLASTDATE(String lASTDATE) {
		LASTDATE = lASTDATE;
	}
	public String getLASTTIME() {
		return LASTTIME;
	}
	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}
	public String getRESULTDATA() {
		return RESULTDATA;
	}
	public void setRESULTDATA(String rESULTDATA) {
		RESULTDATA = rESULTDATA;
	}
	
}
