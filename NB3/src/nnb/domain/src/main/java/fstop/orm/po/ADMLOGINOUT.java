package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="fstop.orm.po.ADMLOGINOUT")
@Table(name = "ADMLOGINOUT")
public class ADMLOGINOUT implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -377371791265291430L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long ADLOGINOUTID;

	private String ADUSERTYPE = "";

	private String ADUSERID = "";

	private String ADUSERIP = "";

	private String CMYYYYMM = "";

	private String CMDD = "";

	private String CMTIME = "";

	private String LOGINOUT = "";     //0. 登入, 1. 登出
	
	private String LOGINTYPE = ""; //NB:網銀 ， MB:行動 ，預設為NB
	
	private String TOKENID = "";
	
	private String ADEXCODE = "";

	public Long getADLOGINOUTID() {
		return ADLOGINOUTID;
	}

	public void setADLOGINOUTID(Long adloginoutid) {
		ADLOGINOUTID = adloginoutid;
	}

	public String getADUSERID() {
		return ADUSERID;
	}

	public void setADUSERID(String aduserid) {
		ADUSERID = aduserid;
	}

	public String getADUSERIP() {
		return ADUSERIP;
	}

	public void setADUSERIP(String aduserip) {
		ADUSERIP = aduserip;
	}

	public String getADUSERTYPE() {
		return ADUSERTYPE;
	}

	public void setADUSERTYPE(String adusertype) {
		ADUSERTYPE = adusertype;
	}

	public String getCMDD() {
		return CMDD;
	}

	public void setCMDD(String cmdd) {
		CMDD = cmdd;
	}

	public String getCMTIME() {
		return CMTIME;
	}

	public void setCMTIME(String cmtime) {
		CMTIME = cmtime;
	}

	public String getCMYYYYMM() {
		return CMYYYYMM;
	}

	public void setCMYYYYMM(String cmyyyymm) {
		CMYYYYMM = cmyyyymm;
	}

	public String getLOGINOUT() {
		return LOGINOUT;
	}

	public void setLOGINOUT(String loginout) {
		LOGINOUT = loginout;
	}

	public String getLOGINTYPE() {
		return LOGINTYPE;
	}

	public void setLOGINTYPE(String logintype) {
		LOGINTYPE = logintype;
	}

	public String getTOKENID() {
		return TOKENID;
	}

	public void setTOKENID(String tOKENID) {
		TOKENID = tOKENID;
	}

	public String getADEXCODE() {
		return ADEXCODE;
	}

	public void setADEXCODE(String aDEXCODE) {
		ADEXCODE = aDEXCODE;
	}

}
