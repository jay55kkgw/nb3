package fstop.orm.po;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "ITRCOUNT")
public class ITRCOUNT implements Serializable {

	@Id
	private String HEADER;

	private String DATE;

	private String TIME;

	private String COUNT;
	
	
	public String getCOUNT() {
		return COUNT;
	}

	public void setCOUNT(String count) {
		COUNT = count;
	}

	public String getDATE() {
		return DATE;
	}

	public void setDATE(String date) {
		DATE = date;
	}

	public String getHEADER() {
		return HEADER;
	}

	public void setHEADER(String header) {
		HEADER = header;
	}

	public String getTIME() {
		return TIME;
	}

	public void setTIME(String time) {
		TIME = time;
	}	

}
