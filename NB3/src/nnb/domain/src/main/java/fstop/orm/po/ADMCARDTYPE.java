package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "ADMCARDTYPE")
public class ADMCARDTYPE implements Serializable {

	@Id
	//@GeneratedValue(strategy = GenerationType.AUTO)
	private String ADTYPEID;

	private String ADTYPENAME;

	private String ADTYPEDESC;

	private String LASTUSER = "";

	private String LASTDATE = "";

	private String LASTTIME = "";

	public String toString() {
		return new ToStringBuilder(this).append("adtypeid", getADTYPEID())
				.toString();
	}

	public String getADTYPEDESC() {
		return ADTYPEDESC;
	}

	public void setADTYPEDESC(String adtypedesc) {
		ADTYPEDESC = adtypedesc;
	}

	public String getADTYPEID() {
		return ADTYPEID;
	}

	public void setADTYPEID(String adtypeid) {
		ADTYPEID = adtypeid;
	}

	public String getADTYPENAME() {
		return ADTYPENAME;
	}

	public void setADTYPENAME(String adtypename) {
		ADTYPENAME = adtypename;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

	public String getLASTUSER() {
		return LASTUSER;
	}

	public void setLASTUSER(String lastuser) {
		LASTUSER = lastuser;
	}

}
