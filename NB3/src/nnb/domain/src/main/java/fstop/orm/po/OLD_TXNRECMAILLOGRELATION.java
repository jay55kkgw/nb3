package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="fstop.orm.po.OLD_TXNRECMAILLOGRELATION")
@Table(name = "TXNRECMAILLOGRELATION")
public class OLD_TXNRECMAILLOGRELATION implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 6993796311736179084L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long RELID;

	private String REC_TYPE = ""; // FX or TW

	private String ADTXNO = "";

	private Long ADMAILLOGID;

	private String LASTDATE = "";

	private String LASTTIME = "";

	public Long getRELID() {
		return RELID;
	}

	public void setRELID(Long rELID) {
		RELID = rELID;
	}

	public String getREC_TYPE() {
		return REC_TYPE;
	}

	public void setREC_TYPE(String rEC_TYPE) {
		REC_TYPE = rEC_TYPE;
	}

	public String getADTXNO() {
		return ADTXNO;
	}

	public void setADTXNO(String aDTXNO) {
		ADTXNO = aDTXNO;
	}

	public Long getADMAILLOGID() {
		return ADMAILLOGID;
	}

	public void setADMAILLOGID(Long aDMAILLOGID) {
		ADMAILLOGID = aDMAILLOGID;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lASTDATE) {
		LASTDATE = lASTDATE;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}
	
	
}
