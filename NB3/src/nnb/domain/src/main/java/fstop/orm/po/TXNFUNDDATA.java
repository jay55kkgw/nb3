package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity(name="fstop.orm.po.TXNFUNDDATA")
@Table(name = "TXNFUNDDATA")
public class TXNFUNDDATA implements Serializable {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 3034357752038068583L;

	@Id
	private String TRANSCODE;

	private String TRANSCRY;

	private String FUNDLNAME;

	private String FUNDSNAME;

	private String COUNTRYTYPE;

	private String FUNDMARK;

	private String RISK;

	private String PERIODVAR;
	
	private String FUS98E;
	
	private String FUSMON;
	
	private String CBAMT;
	
	private String YBAMT;
	
	private String CBTAMT;
	
	private String YBTAMT;

	private String FUNDT;
	
	private String C30AMT;
	
	private String C30RAMT;
	
	private String Y30AMT;
	
	private String Y30RAMT;
	
	private String C302AMT;
	
	private String C302RAMT;
	
	private String Y302AMT;
	
	private String Y302RAMT;
	
	public String toString() {
		return new ToStringBuilder(this).append("transcode", getTRANSCODE())
				.toString();
	}

	public String getCOUNTRYTYPE() {
		return COUNTRYTYPE;
	}

	public void setCOUNTRYTYPE(String countrytype) {
		COUNTRYTYPE = countrytype;
	}

	public String getFUNDLNAME() {
		return FUNDLNAME;
	}

	public void setFUNDLNAME(String fundlname) {
		FUNDLNAME = fundlname;
	}

	public String getFUNDMARK() {
		return FUNDMARK;
	}

	public void setFUNDMARK(String fundmark) {
		FUNDMARK = fundmark;
	}

	public String getFUNDSNAME() {
		return FUNDSNAME;
	}

	public void setFUNDSNAME(String fundsname) {
		FUNDSNAME = fundsname;
	}

	public String getRISK() {
		return RISK;
	}

	public void setRISK(String risk) {
		RISK = risk;
	}

	public String getTRANSCODE() {
		return TRANSCODE;
	}

	public void setTRANSCODE(String transcode) {
		TRANSCODE = transcode;
	}

	public String getTRANSCRY() {
		return TRANSCRY;
	}

	public void setTRANSCRY(String transcry) {
		TRANSCRY = transcry;
	}
	
	public String getPERIODVAR() {
		return PERIODVAR;
	}

	public void setPERIODVAR(String periodvar) {
		PERIODVAR = periodvar;
	}

	
	public String getFUS98E() {
		return FUS98E;
	}

	public void setFUS98E(String fus98e) {
		FUS98E = fus98e;
	}

	public String getFUSMON() {
		return FUSMON;
	}

	public void setFUSMON(String fUSMON) {
		FUSMON = fUSMON;
	}

	public String getCBAMT() {
		return CBAMT;
	}

	public void setCBAMT(String cBAMT) {
		CBAMT = cBAMT;
	}

	public String getYBAMT() {
		return YBAMT;
	}

	public void setYBAMT(String yBAMT) {
		YBAMT = yBAMT;
	}

	public String getCBTAMT() {
		return CBTAMT;
	}

	public void setCBTAMT(String cBTAMT) {
		CBTAMT = cBTAMT;
	}

	public String getYBTAMT() {
		return YBTAMT;
	}

	public void setYBTAMT(String yBTAMT) {
		YBTAMT = yBTAMT;
	}
	
	public String getFUNDT() {
		return FUNDT;
	}

	public void setFUNDT(String fUNDT) {
		FUNDT = fUNDT;
	}
	
	public String getC30AMT() {
		return C30AMT;
	}

	public void setC30AMT(String c30AMT) {
		C30AMT = c30AMT;
	}

	public String getC30RAMT() {
		return C30RAMT;
	}

	public void setC30RAMT(String c30RAMT) {
		C30RAMT = c30RAMT;
	}

	public String getY30AMT() {
		return Y30AMT;
	}

	public void setY30AMT(String y30AMT) {
		Y30AMT = y30AMT;
	}

	public String getY30RAMT() {
		return Y30RAMT;
	}

	public void setY30RAMT(String y30RAMT) {
		Y30RAMT = y30RAMT;
	}

	public String getC302AMT() {
		return C302AMT;
	}

	public void setC302AMT(String c302AMT) {
		C302AMT = c302AMT;
	}

	public String getC302RAMT() {
		return C302RAMT;
	}

	public void setC302RAMT(String c302RAMT) {
		C302RAMT = c302RAMT;
	}

	public String getY302AMT() {
		return Y302AMT;
	}

	public void setY302AMT(String y302AMT) {
		Y302AMT = y302AMT;
	}

	public String getY302RAMT() {
		return Y302RAMT;
	}

	public void setY302RAMT(String y302RAMT) {
		Y302RAMT = y302RAMT;
	}
}
