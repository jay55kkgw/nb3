package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="fstop.orm.po.ADMLOGIN")
@Table(name = "ADMLOGIN")
public class ADMLOGIN implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5035448429590992861L;
	
	@Id
	private String ADUSERID = "";		// 使用者統編ID
	private String TOKENID = "";		// 使用者識別
	private String ADUSERTYPE = "";		// 操作者類別
	private String ADUSERIP = "";		// 登入 IP-Address
	private String LOGINOUT = "";		// 登入狀態
	private String LOGINTIME = "";		// 本次登入日期時間
	private String LOGOUTTIME = "";		// 本次正常登出日期時間
	private String FORCELOGOUTTIME = "";// 本次強迫登出日期時間
	private String LASTLOGINTIME = "";	// 上次登入日期時間
	private String LASTADUSERIP = "";	// 上次登入IP
	private String LOGINTYPE = "";		// 登入來源
	private String DPUSERNAME = "";		// 使用者姓名
	private String MMACOD = "";			// MMA輕鬆理財註記
	private String XMLCOD = "";			// 憑證代碼
	private String BRTHDY = "";			// 出生年月日
	private String TELNUM = "";			// 電話號碼
	private String STAFF = "";			// 是否為行員
	private String MBSTAT = "";			// 行動銀行目前狀態
	private String MBOPNDT = "";		// 行動銀行啟用/關閉日期
	private String MBOPNTM = "";		// 行動銀行啟用/關閉時間
	private String IDNCOD = "";			// 1 個人模式  2 企業模式
	private String APATR = "";			// 申請網銀約定業務權限功能
	private String HKCODE = "";			// 香港網銀是否需轉置註記
	private String MOBTEL = "";			// 行動電話
	
	
	public String getADUSERID() {
		return ADUSERID;
	}
	public void setADUSERID(String aDUSERID) {
		ADUSERID = aDUSERID;
	}
	public String getTOKENID() {
		return TOKENID;
	}
	public void setTOKENID(String tOKENID) {
		TOKENID = tOKENID;
	}
	public String getADUSERTYPE() {
		return ADUSERTYPE;
	}
	public void setADUSERTYPE(String aDUSERTYPE) {
		ADUSERTYPE = aDUSERTYPE;
	}
	public String getADUSERIP() {
		return ADUSERIP;
	}
	public void setADUSERIP(String aDUSERIP) {
		ADUSERIP = aDUSERIP;
	}
	public String getLOGINOUT() {
		return LOGINOUT;
	}
	public void setLOGINOUT(String lOGINOUT) {
		LOGINOUT = lOGINOUT;
	}
	public String getLOGINTIME() {
		return LOGINTIME;
	}
	public void setLOGINTIME(String lOGINTIME) {
		LOGINTIME = lOGINTIME;
	}
	public String getLOGOUTTIME() {
		return LOGOUTTIME;
	}
	public void setLOGOUTTIME(String lOGOUTTIME) {
		LOGOUTTIME = lOGOUTTIME;
	}
	public String getFORCELOGOUTTIME() {
		return FORCELOGOUTTIME;
	}
	public void setFORCELOGOUTTIME(String fORCELOGOUTTIME) {
		FORCELOGOUTTIME = fORCELOGOUTTIME;
	}
	public String getLASTLOGINTIME() {
		return LASTLOGINTIME;
	}
	public void setLASTLOGINTIME(String lASTLOGINTIME) {
		LASTLOGINTIME = lASTLOGINTIME;
	}
	public String getLASTADUSERIP() {
		return LASTADUSERIP;
	}
	public void setLASTADUSERIP(String lASTADUSERIP) {
		LASTADUSERIP = lASTADUSERIP;
	}
	public String getLOGINTYPE() {
		return LOGINTYPE;
	}
	public void setLOGINTYPE(String lOGINTYPE) {
		LOGINTYPE = lOGINTYPE;
	}
	public String getDPUSERNAME() {
		return DPUSERNAME;
	}
	public void setDPUSERNAME(String dPUSERNAME) {
		DPUSERNAME = dPUSERNAME;
	}
	public String getMMACOD() {
		return MMACOD;
	}
	public void setMMACOD(String mMACOD) {
		MMACOD = mMACOD;
	}
	public String getXMLCOD() {
		return XMLCOD;
	}
	public void setXMLCOD(String xMLCOD) {
		XMLCOD = xMLCOD;
	}
	public String getBRTHDY() {
		return BRTHDY;
	}
	public void setBRTHDY(String bRTHDY) {
		BRTHDY = bRTHDY;
	}
	public String getTELNUM() {
		return TELNUM;
	}
	public void setTELNUM(String tELNUM) {
		TELNUM = tELNUM;
	}
	public String getSTAFF() {
		return STAFF;
	}
	public void setSTAFF(String sTAFF) {
		STAFF = sTAFF;
	}
	public String getMBSTAT() {
		return MBSTAT;
	}
	public void setMBSTAT(String mBSTAT) {
		MBSTAT = mBSTAT;
	}
	public String getMBOPNDT() {
		return MBOPNDT;
	}
	public void setMBOPNDT(String mBOPNDT) {
		MBOPNDT = mBOPNDT;
	}
	public String getMBOPNTM() {
		return MBOPNTM;
	}
	public void setMBOPNTM(String mBOPNTM) {
		MBOPNTM = mBOPNTM;
	}
	public String getIDNCOD() {
		return IDNCOD;
	}
	public void setIDNCOD(String iDNCOD) {
		IDNCOD = iDNCOD;
	}
	public String getAPATR() {
		return APATR;
	}
	public void setAPATR(String aPATR) {
		APATR = aPATR;
	}
	public String getHKCODE() {
		return HKCODE;
	}
	public void setHKCODE(String hKCODE) {
		HKCODE = hKCODE;
	}
	public String getMOBTEL() {
		return MOBTEL;
	}
	public void setMOBTEL(String mOBTEL) {
		MOBTEL = mOBTEL;
	}
	

}
