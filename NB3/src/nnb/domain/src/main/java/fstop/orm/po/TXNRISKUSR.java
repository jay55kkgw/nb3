package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="fstop.orm.po.TXNRISKUSR")
@Table(name = "TXNRISKUSR")
public class TXNRISKUSR implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6836013293723101885L;
	
	@Id
	private String USERID;
	private String RISKLOG = "";
	private String LASTDATE  = "";
	private String LASTTIME  = "";
	
	public String getUSERID() {
		return USERID;
	}

	public void setUSERID(String uSERID) {
		USERID = uSERID;
	}

	public String getRISKLOG() {
		return RISKLOG;
	}

	public void setRISKLOG(String rISKLOG) {
		RISKLOG = rISKLOG;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lASTDATE) {
		LASTDATE = lASTDATE;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}


}
