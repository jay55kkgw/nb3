package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "ADMROLEAUTH")
public class ADMROLEAUTH implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer ADAUTHID;

	private String ADROLENO;

	private String ADSTAFFNO;

	private String ADAUTH;

	private String LASTUSER = "";

	private String LASTDATE = "";

	private String LASTTIME = "";

	public String toString() {
		return new ToStringBuilder(this).append("adauthid", getADAUTHID())
				.toString();
	}

	public String getADAUTH() {
		return ADAUTH;
	}

	public void setADAUTH(String adauth) {
		ADAUTH = adauth;
	}

	public Integer getADAUTHID() {
		return ADAUTHID;
	}

	public void setADAUTHID(Integer adauthid) {
		ADAUTHID = adauthid;
	}

	public String getADROLENO() {
		return ADROLENO;
	}

	public void setADROLENO(String adroleno) {
		ADROLENO = adroleno;
	}

	public String getADSTAFFNO() {
		return ADSTAFFNO;
	}

	public void setADSTAFFNO(String adstaffno) {
		ADSTAFFNO = adstaffno;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

	public String getLASTUSER() {
		return LASTUSER;
	}

	public void setLASTUSER(String lastuser) {
		LASTUSER = lastuser;
	}

}
