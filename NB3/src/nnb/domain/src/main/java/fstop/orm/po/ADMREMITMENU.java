package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity(name = "fstop.orm.po.ADMREMITMENU")
@Table(name = "ADMREMITMENU")
public class ADMREMITMENU implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2745652845448542495L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long ADSEQNO;

	private String ADRMTTYPE = "";

	private String ADRMTID = "";

	private String ADLKINDID = "";// 分類編號

	private String ADMKINDID = "";

	private String ADRMTITEM = "";// 項目(中)

	private String ADRMTENGITEM = "";// 項目(英)

	private String ADRMTCHSITEM = "";// 項目(簡)

	private String ADRMTDESC = "";// 說明(中)

	private String ADRMTENGDESC = "";// 說明(英)

	private String ADRMTCHSDESC = "";// 說明(簡)

	private String ADCHKMK = "";

	private String ADRMTEETYPE1 = "";

	private String ADRMTEETYPE2 = "";

	private String ADRMTEETYPE3 = "";

	private String LASTUSER = "";

	private String LASTDATE = "";

	private String LASTTIME = "";

	public Long getADSEQNO()
	{
		return ADSEQNO;
	}

	public void setADSEQNO(Long aDSEQNO)
	{
		ADSEQNO = aDSEQNO;
	}

	public String getADRMTTYPE()
	{
		return ADRMTTYPE;
	}

	public void setADRMTTYPE(String aDRMTTYPE)
	{
		ADRMTTYPE = aDRMTTYPE;
	}

	public String getADRMTID()
	{
		return ADRMTID;
	}

	public void setADRMTID(String aDRMTID)
	{
		ADRMTID = aDRMTID;
	}

	public String getADLKINDID()
	{
		return ADLKINDID;
	}

	public void setADLKINDID(String aDLKINDID)
	{
		ADLKINDID = aDLKINDID;
	}

	public String getADMKINDID()
	{
		return ADMKINDID;
	}

	public void setADMKINDID(String aDMKINDID)
	{
		ADMKINDID = aDMKINDID;
	}

	public String getADRMTITEM()
	{
		return ADRMTITEM;
	}

	public void setADRMTITEM(String aDRMTITEM)
	{
		ADRMTITEM = aDRMTITEM;
	}

	public String getADRMTENGITEM()
	{
		return ADRMTENGITEM;
	}

	public void setADRMTENGITEM(String aDRMTENGITEM)
	{
		ADRMTENGITEM = aDRMTENGITEM;
	}

	public String getADRMTCHSITEM()
	{
		return ADRMTCHSITEM;
	}

	public void setADRMTCHSITEM(String aDRMTCHSITEM)
	{
		ADRMTCHSITEM = aDRMTCHSITEM;
	}

	public String getADRMTDESC()
	{
		return ADRMTDESC;
	}

	public void setADRMTDESC(String aDRMTDESC)
	{
		ADRMTDESC = aDRMTDESC;
	}

	public String getADRMTENGDESC()
	{
		return ADRMTENGDESC;
	}

	public void setADRMTENGDESC(String aDRMTENGDESC)
	{
		ADRMTENGDESC = aDRMTENGDESC;
	}

	public String getADRMTCHSDESC()
	{
		return ADRMTCHSDESC;
	}

	public void setADRMTCHSDESC(String aDRMTCHSDESC)
	{
		ADRMTCHSDESC = aDRMTCHSDESC;
	}

	public String getADCHKMK()
	{
		return ADCHKMK;
	}

	public void setADCHKMK(String aDCHKMK)
	{
		ADCHKMK = aDCHKMK;
	}

	public String getADRMTEETYPE1()
	{
		return ADRMTEETYPE1;
	}

	public void setADRMTEETYPE1(String aDRMTEETYPE1)
	{
		ADRMTEETYPE1 = aDRMTEETYPE1;
	}

	public String getADRMTEETYPE2()
	{
		return ADRMTEETYPE2;
	}

	public void setADRMTEETYPE2(String aDRMTEETYPE2)
	{
		ADRMTEETYPE2 = aDRMTEETYPE2;
	}

	public String getADRMTEETYPE3()
	{
		return ADRMTEETYPE3;
	}

	public void setADRMTEETYPE3(String aDRMTEETYPE3)
	{
		ADRMTEETYPE3 = aDRMTEETYPE3;
	}

	public String getLASTUSER()
	{
		return LASTUSER;
	}

	public void setLASTUSER(String lASTUSER)
	{
		LASTUSER = lASTUSER;
	}

	public String getLASTDATE()
	{
		return LASTDATE;
	}

	public void setLASTDATE(String lASTDATE)
	{
		LASTDATE = lASTDATE;
	}

	public String getLASTTIME()
	{
		return LASTTIME;
	}

	public void setLASTTIME(String lASTTIME)
	{
		LASTTIME = lASTTIME;
	}

}
