package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;

@Entity
@Table(name = "TXNACNAPPLY")
public class TXNACNAPPLY implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long LOGID;
	
	private String CUSIDN = "";

	private String BRHCOD = "";

	private String NAME = "";

	private String BIRTHDAY = "";

	private String POSTCOD1 = "";

	private String PMTADR = "";

	private String POSTCOD2 = "";

	private String CTTADR = "";

	private String HOMEZIP = "";

	private String HOMETEL = "";

	private String BUSZIP = "";

	private String BUSTEL = "";

	private String CELPHONE = "";

	private String ARACOD3 = "";

	private String FAX = "";

	private String MAILADDR = "";

	private String MARRY = "";

	private String CHILD = "";

	private String DEGREE = "";

	private String CAREER1 = "";

	private String CAREER2 = "";

	private String SALARY = "";

	private String TRFLAG = "";

	private String EBILLFLAG = "";
	
	private String PURPOSE = "";

	private String PREASON = "";

	private String BDEALING = "";

	private String BREASON = "";

	private String ACN = "";

	private String N203MSGCOD = "";

	private String NA30MSGCOD = "";

	private String FILE1 = "";

	private String FILE2 = "";

	private String FILE3 = "";

	private String FILE4 = "";

	private String IP = "";

	private String LASTDATE = "";

	private String LASTTIME = "";
	
	private String CARDAPPLY = "";
	
	private String NAATAPPLY = "";
	
	private String CChargeApply = "";
	
	private String CROSSAPPLY = "";
	
	private String ACNTYPE = "";


	public Long getLOGID() {
		return LOGID;
	}

	public void setLOGID(Long logid) {
		LOGID = logid;
	}

	public String getBRHCOD() {
		return BRHCOD;
	}

	public void setBRHCOD(String brhcod) {
		BRHCOD = brhcod;
	}

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cusidn) {
		CUSIDN = cusidn;
	}

	public String getNAME() {
		return NAME;
	}

	public void setNAME(String name) {
		NAME = name;
	}

	public String getBIRTHDAY() {
		return BIRTHDAY;
	}

	public void setBIRTHDAY(String birthday) {
		BIRTHDAY = birthday;
	}

	public String getPOSTCOD1() {
		return POSTCOD1;
	}

	public void setPOSTCOD1(String postcod1) {
		POSTCOD1 = postcod1;
	}

	public String getPMTADR() {
		return PMTADR;
	}

	public void setPMTADR(String pmtadr) {
		PMTADR = pmtadr;
	}

	public String getPOSTCOD2() {
		return POSTCOD2;
	}

	public void setPOSTCOD2(String postcod2) {
		POSTCOD2 = postcod2;
	}

	public String getCTTADR() {
		return CTTADR;
	}
	public void setCTTADR(String cttadr) {
		CTTADR = cttadr;
	}
	public String getHOMEZIP() {
		return HOMEZIP;
	}
	public void setHOMEZIP(String homezip) {
		HOMEZIP = homezip;
	}
	public String getHOMETEL() {
		return HOMETEL;
	}
	public void setHOMETEL(String hometel) {
		HOMETEL = hometel;
	}
	public String getBUSZIP() {
		return BUSZIP;
	}
	public void setBUSZIP(String buszip) {
		BUSZIP = buszip;
	}
	public String getBUSTEL() {
		return BUSTEL;
	}
	public void setBUSTEL(String bustel) {
		BUSTEL = bustel;
	}
	public String getCELPHONE() {
		return CELPHONE;
	}
	public void setCELPHONE(String celphone) {
		CELPHONE = celphone;
	}
	public String getARACOD3() {
		return ARACOD3;
	}
	public void setARACOD3(String aracod3) {
		ARACOD3 = aracod3;
	}
	public String getFAX() {
		return FAX;
	}
	public void setFAX(String fax) {
		FAX = fax;
	}
	public String getMAILADDR() {
		return MAILADDR;
	}
	public void setMAILADDR(String mailaddr) {
		MAILADDR = mailaddr;
	}
	public String getMARRY() {
		return MARRY;
	}
	public void setMARRY(String marry) {
		MARRY = marry;
	}
	public String getCHILD() {
		return CHILD;
	}
	public void setCHILD(String child) {
		CHILD = child;
	}
	public String getDEGREE() {
		return DEGREE;
	}
	public void setDEGREE(String degree) {
		DEGREE = degree;
	}
	public String getCAREER1() {
		return CAREER1;
	}
	public void setCAREER1(String career1) {
		CAREER1 = career1;
	}
	public String getCAREER2() {
		return CAREER2;
	}
	public void setCAREER2(String career2) {
		CAREER2 = career2;
	}
	public String getSALARY() {
		return SALARY;
	}
	public void setSALARY(String salary) {
		SALARY = salary;
	}
	public String getTRFLAG() {
		return TRFLAG;
	}
	public void setTRFLAG(String trflag) {
		TRFLAG = trflag;
	}
	public String getEBILLFLAG() {
		return EBILLFLAG;
	}
	public void setEBILLFLAG(String ebillflag) {
		EBILLFLAG = ebillflag;
	}
	public String getPURPOSE() {
		return PURPOSE;
	}
	public void setPURPOSE(String purpose) {
		PURPOSE = purpose;
	}
	public String getPREASON() {
		return PREASON;
	}
	public void setPREASON(String preason) {
		PREASON = preason;
	}
	public String getBDEALING() {
		return BDEALING;
	}
	public void setBDEALING(String bdealing) {
		BDEALING = bdealing;
	}
	public String getBREASON() {
		return BREASON;
	}
	public void setBREASON(String breason) {
		BREASON = breason;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String acn) {
		ACN = acn;
	}
	public String getN203MSGCOD() {
		return N203MSGCOD;
	}
	public void setN203MSGCOD(String n203msgcod) {
		N203MSGCOD = n203msgcod;
	}
	public String getNA30MSGCOD() {
		return NA30MSGCOD;
	}
	public void setNA30MSGCOD(String na30msgcod) {
		NA30MSGCOD = na30msgcod;
	}
	public String getFILE1() {
		return FILE1;
	}
	public void setFILE1(String file1) {
		FILE1 = file1;
	}
	public String getFILE2() {
		return FILE2;
	}
	public void setFILE2(String file2) {
		FILE2 = file2;
	}
	public String getFILE3() {
		return FILE3;
	}
	public void setFILE3(String file3) {
		FILE3 = file3;
	}
	public String getFILE4() {
		return FILE4;
	}
	public void setFILE4(String file4) {
		FILE4 = file4;
	}
	public String getIP() {
		return IP;
	}
	public void setIP(String ip) {
		IP = ip;
	}
	public String getLASTDATE() {
		return LASTDATE;
	}
	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}
	public String getLASTTIME() {
		return LASTTIME;
	}
	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}
	public String getCARDAPPLY() {
		return CARDAPPLY;
	}
	public void setCARDAPPLY(String cardapply) {
		CARDAPPLY = cardapply;
	}
	public String getNAATAPPLY() {
		return NAATAPPLY;
	}
	public void setNAATAPPLY(String naatapply) {
		NAATAPPLY = naatapply;
	}
	public String getCChargeApply() {
		return CChargeApply;
	}
	public void setCChargeApply(String cchargeapply) {
		CChargeApply = cchargeapply;
	}
	public String getCROSSAPPLY() {
		return CROSSAPPLY;
	}
	public void setCROSSAPPLY(String crossapply) {
		CROSSAPPLY = crossapply;
	}
	public String getACNTYPE() {
		return ACNTYPE;
	}
	public void setACNTYPE(String acntype) {
		ACNTYPE = acntype;
	}	
}
