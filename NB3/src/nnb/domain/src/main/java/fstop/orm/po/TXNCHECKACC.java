package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TXNCHECKACC")
public class TXNCHECKACC implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8283446478147932106L;
	@Id
	private String ADTXNO;
	private String STATUS;
	private String RCODE;
	private String RPT;
	private String CREATEDATE;
	private String CREATETIME;
	private String LASTDATE;
	private String LASTTIME;
	public String getADTXNO() {
		return ADTXNO;
	}
	public void setADTXNO(String aDTXNO) {
		ADTXNO = aDTXNO;
	}
	public String getSTATUS() {
		return STATUS;
	}
	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}
	public String getRCODE() {
		return RCODE;
	}
	public void setRCODE(String rCODE) {
		RCODE = rCODE;
	}
	public String getRPT() {
		return RPT;
	}
	public void setRPT(String rPT) {
		RPT = rPT;
	}
	public String getCREATEDATE() {
		return CREATEDATE;
	}
	public void setCREATEDATE(String cREATEDATE) {
		CREATEDATE = cREATEDATE;
	}
	public String getCREATETIME() {
		return CREATETIME;
	}
	public void setCREATETIME(String cREATETIME) {
		CREATETIME = cREATETIME;
	}
	public String getLASTDATE() {
		return LASTDATE;
	}
	public void setLASTDATE(String lASTDATE) {
		LASTDATE = lASTDATE;
	}
	public String getLASTTIME() {
		return LASTTIME;
	}
	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}
	
	@Override
	public String toString() {
		return "TXNCHECKACC [ADTXNO=" + ADTXNO + ", STATUS=" + STATUS + ", RCODE=" + RCODE + ", RPT=" + RPT
				+ ", CREATEDATE=" + CREATEDATE + ", CREATETIME=" + CREATETIME + ", LASTDATE=" + LASTDATE + ", LASTTIME="
				+ LASTTIME + "]";
	}
 
}
