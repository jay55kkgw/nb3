package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "ADMADS")
public class ADMADS implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1795453561063361131L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String ID = "";
	
	private String OID = "";
	
	private String TITLE = "";
	
	private String CONTENT = "";
	
	private String TYPE = "";
	
	private String STARTDATE = "";
	
	private String STARTTIME = "";
	
	private String ENDDATE = "";
	
	private String ENDTIME = "";
	
	private String FILENAMEL = "";
	
	private String FILENAMEM = "";
	
	private String FILENAMES = "";

//	@Lob
//	@Basic(fetch = FetchType.LAZY) 
//	@Column(name="FILECONTENTL", columnDefinition="BLOB")
	private byte[] FILECONTENTL;
	
//	@Lob
//	@Basic(fetch = FetchType.LAZY) 
//	@Column(name="FILECONTENTM", columnDefinition="BLOB")
	private byte[] FILECONTENTM;
	
//	@Lob
//	@Basic(fetch = FetchType.LAZY) 
//	@Column(name="FILECONTENTS", columnDefinition="BLOB")
	private byte[] FILECONTENTS;
	
	private String URL = "";
	
	private Integer SORTORDER;
	
	private String LASTUSER = "";
	
	private String LASTDATE = "";
	
	private String LASTTIME = "";
	
	private String TARGETTYPE = "";
	
	private byte[] TARGETCONTENT ;
	
	private String TARGETFILENAME = "";
	
	@Transient
	private String showText = "";

	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}

	public String getOID() {
		return OID;
	}

	public void setOID(String oID) {
		OID = oID;
	}

	public String getTITLE() {
		return TITLE;
	}

	public void setTITLE(String tITLE) {
		TITLE = tITLE;
	}

	public String getCONTENT() {
		return CONTENT;
	}

	public void setCONTENT(String cONTENT) {
		CONTENT = cONTENT;
	}

	public String getTYPE() {
		return TYPE;
	}

	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}

	public String getSTARTDATE() {
		return STARTDATE;
	}

	public void setSTARTDATE(String sTARTDATE) {
		STARTDATE = sTARTDATE;
	}

	public String getSTARTTIME() {
		return STARTTIME;
	}

	public void setSTARTTIME(String sTARTTIME) {
		STARTTIME = sTARTTIME;
	}

	public String getENDDATE() {
		return ENDDATE;
	}

	public void setENDDATE(String eNDDATE) {
		ENDDATE = eNDDATE;
	}

	public String getENDTIME() {
		return ENDTIME;
	}

	public void setENDTIME(String eNDTIME) {
		ENDTIME = eNDTIME;
	}

	public String getFILENAMEL() {
		return FILENAMEL;
	}

	public void setFILENAMEL(String fILENAMEL) {
		FILENAMEL = fILENAMEL;
	}

	public String getFILENAMEM() {
		return FILENAMEM;
	}

	public void setFILENAMEM(String fILENAMEM) {
		FILENAMEM = fILENAMEM;
	}

	public String getFILENAMES() {
		return FILENAMES;
	}

	public void setFILENAMES(String fILENAMES) {
		FILENAMES = fILENAMES;
	}

	public byte[] getFILECONTENTL() {
		return FILECONTENTL;
	}

	public void setFILECONTENTL(byte[] fILECONTENTL) {
		FILECONTENTL = fILECONTENTL;
	}

	public byte[] getFILECONTENTM() {
		return FILECONTENTM;
	}

	public void setFILECONTENTM(byte[] fILECONTENTM) {
		FILECONTENTM = fILECONTENTM;
	}

	public byte[] getFILECONTENTS() {
		return FILECONTENTS;
	}

	public void setFILECONTENTS(byte[] fILECONTENTS) {
		FILECONTENTS = fILECONTENTS;
	}

	public String getURL() {
		return URL;
	}

	public void setURL(String uRL) {
		URL = uRL;
	}

	public Integer getSORTORDER() {
		return SORTORDER;
	}

	public void setSORTORDER(Integer sORTORDER) {
		SORTORDER = sORTORDER;
	}

	public String getLASTUSER() {
		return LASTUSER;
	}

	public void setLASTUSER(String lASTUSER) {
		LASTUSER = lASTUSER;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lASTDATE) {
		LASTDATE = lASTDATE;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}

	public String getTARGETTYPE() {
		return TARGETTYPE;
	}

	public void setTARGETTYPE(String tARGETTYPE) {
		TARGETTYPE = tARGETTYPE;
	}

	public byte[] getTARGETCONTENT() {
		return TARGETCONTENT;
	}

	public void setTARGETCONTENT(byte[] tARGETCONTENT) {
		TARGETCONTENT = tARGETCONTENT;
	}

	public String getTARGETFILENAME() {
		return TARGETFILENAME;
	}

	public void setTARGETFILENAME(String tARGETFILENAME) {
		TARGETFILENAME = tARGETFILENAME;
	}

	public String getShowText() {
		return showText;
	}

	public void setShowText(String showText) {
		this.showText = showText;
	}


	
}
