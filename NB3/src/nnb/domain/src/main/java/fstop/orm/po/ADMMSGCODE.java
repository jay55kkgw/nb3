package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity(name="fstop.orm.po.ADMMSGCODE")
@Table(name = "ADMMSGCODE")
public class ADMMSGCODE implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1126375775064371258L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String ADMCODE;

	private String ADMRESEND = "";

	private String ADMEXCE = "";   //需列入異常事件通知否

	private String ADMSGIN = "";

	private String ADMSGOUT = "";

	private String LASTUSER = "";

	private String LASTDATE = "";

	private String LASTTIME = "";

	private String ADMRESENDFX = "";

	private String ADMAUTOSEND = "";
	
	private String ADMAUTOSENDFX = "";
	
	private String ADMSGOUTENG = "";
	
	private String ADMSGOUTCHS = "";
	
	
	@Override
	public String toString() {
		return "ADMMSGCODE [ADMCODE=" + ADMCODE + ", ADMRESEND=" + ADMRESEND + ", ADMEXCE=" + ADMEXCE + ", ADMSGIN="
				+ ADMSGIN + ", ADMSGOUT=" + ADMSGOUT + ", LASTUSER=" + LASTUSER + ", LASTDATE=" + LASTDATE
				+ ", LASTTIME=" + LASTTIME + ", ADMRESENDFX=" + ADMRESENDFX + ", ADMAUTOSEND=" + ADMAUTOSEND
				+ ", ADMAUTOSENDFX=" + ADMAUTOSENDFX + ", ADMSGOUTENG=" + ADMSGOUTENG + ", ADMSGOUTCHS=" + ADMSGOUTCHS
				+ "]";
	}

	public String getADMCODE() {
		return ADMCODE;
	}

	public void setADMCODE(String admcode) {
		ADMCODE = admcode;
	}

	public String getADMEXCE() {
		return ADMEXCE;
	}

	public void setADMEXCE(String admexce) {
		ADMEXCE = admexce;
	}

	public String getADMRESEND() {
		return ADMRESEND;
	}

	public void setADMRESEND(String admresend) {
		ADMRESEND = admresend;
	}

	public String getADMSGIN() {
		return ADMSGIN;
	}

	public void setADMSGIN(String admsgin) {
		ADMSGIN = admsgin;
	}

	public String getADMSGOUT() {
		return ADMSGOUT;
	}

	public void setADMSGOUT(String admsgout) {
		ADMSGOUT = admsgout;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

	public String getLASTUSER() {
		return LASTUSER;
	}

	public void setLASTUSER(String lastuser) {
		LASTUSER = lastuser;
	}

	public String getADMRESENDFX() {
		return ADMRESENDFX;
	}

	public void setADMRESENDFX(String admresendfx) {
		ADMRESENDFX = admresendfx;
	}
	
	public String getADMAUTOSEND() {
		return ADMAUTOSEND;
	}

	public void setADMAUTOSEND(String admautosend) {
		ADMAUTOSEND = admautosend;
	}
	
	public String getADMAUTOSENDFX() {
		return ADMAUTOSENDFX;
	}

	public void setADMAUTOSENDFX(String admautosendfx) {
		ADMAUTOSENDFX = admautosendfx;
	}

	public String getADMSGOUTENG() {
		return ADMSGOUTENG;
	}

	public String getADMSGOUTCHS() {
		return ADMSGOUTCHS;
	}

	public void setADMSGOUTENG(String aDMSGOUTENG) {
		ADMSGOUTENG = aDMSGOUTENG;
	}

	public void setADMSGOUTCHS(String aDMSGOUTCHS) {
		ADMSGOUTCHS = aDMSGOUTCHS;
	}	
	
	
}
