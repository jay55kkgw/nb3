package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "SCRECORD")
public class SCRECORD implements Serializable {

	@Id
	//@GeneratedValue(strategy = GenerationType.AUTO)
	private String USERID;

	private String SCTN;

	private String SCVersion;

	private String Agree; 

	private String AgreeDate;


	public String getUSERID() {
		return USERID; 
	}

	public void setUSERID(String userid) {
		USERID = userid; 
	}

	public String getSCTN() {
		return SCTN;
	}

	public void setSCTN(String sctn) {
		SCTN = sctn;
	}

	public String getSCVersion() {
		return SCVersion;
	}

	public void setSCVersion(String scversion) {
		SCVersion = scversion;
	}

	public String getAgree() {
		return Agree;
	}

	public void setAgree(String agree) {
		Agree = agree;
	}

	public String getAgreeDate() {
		return AgreeDate;
	}

	public void setAgreeDate(String agreedate) {
		AgreeDate = agreedate;
	}
}
