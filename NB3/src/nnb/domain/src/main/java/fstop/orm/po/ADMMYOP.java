package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ADMMYOP")
public class ADMMYOP implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1549288861267779928L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private ADMMYOP_PK ADMMYOP_PK;	// PK
	private String LASTDT; 		// 日期
	
	public ADMMYOP_PK getADMMYOP_PK() {
		return ADMMYOP_PK;
	}
	public void setADMMYOP_PK(ADMMYOP_PK aDMMYOP_PK) {
		ADMMYOP_PK = aDMMYOP_PK;
	}
	public String getLASTDT() {
		return LASTDT;
	}
	public void setLASTDT(String lASTDT) {
		LASTDT = lASTDT;
	}
	
	
	public ADMMYOP() {
		super();
	}
	public ADMMYOP(String aDUSERID, String aDOPID, String lASTDT) {
		super();
		ADMMYOP_PK = new ADMMYOP_PK(aDUSERID, aDOPID);
		LASTDT = lASTDT;
	}
	
	
	
	
}
