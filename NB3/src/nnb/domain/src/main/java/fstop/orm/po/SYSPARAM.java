package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SYSPARAM")
public class SYSPARAM implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1795639964319517445L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String ADPARAMID;

	private String ADPARAMNAME;

	private String ADPARAMVALUE;

	private String ADPARAMMEMO;
	
	public String getADPARAMID() {
		return ADPARAMID;
	}

	public void setADPARAMID(String adparamid) {
		ADPARAMID = adparamid;
	}

	public String getADPARAMMEMO() {
		return ADPARAMMEMO;
	}

	public void setADPARAMMEMO(String adparammemo) {
		ADPARAMMEMO = adparammemo;
	}

	public String getADPARAMNAME() {
		return ADPARAMNAME;
	}

	public void setADPARAMNAME(String adparamname) {
		ADPARAMNAME = adparamname;
	}

	public String getADPARAMVALUE() {
		return ADPARAMVALUE;
	}

	public void setADPARAMVALUE(String adparamvalue) {
		ADPARAMVALUE = adparamvalue;
	}

}
