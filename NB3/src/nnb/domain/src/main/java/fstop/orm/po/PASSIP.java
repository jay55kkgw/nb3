package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="PASSIP")
public class PASSIP implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9218627636708329286L;
	
	@Id
	private PASSIP_PK pks;//複合組鍵
	
	private String LASTUSER = "";
	private String LASTDATE = "";
	private String LASTTIME = "";
	
	public PASSIP_PK getPks() {
		return pks;
	}
	public void setPks(PASSIP_PK pks) {
		this.pks = pks;
	}
	public String getLASTUSER() {
		return LASTUSER;
	}
	public void setLASTUSER(String lASTUSER) {
		LASTUSER = lASTUSER;
	}
	public String getLASTDATE() {
		return LASTDATE;
	}
	public void setLASTDATE(String lASTDATE) {
		LASTDATE = lASTDATE;
	}
	public String getLASTTIME() {
		return LASTTIME;
	}
	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}
	

}
