package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "SYSOPGROUP")
public class SYSOPGROUP implements Serializable {

	@Id
	//@GeneratedValue(strategy = GenerationType.AUTO)
	private String ADOPGROUPID;

	private String ADOPGROUP;

	private String ADOPID;

	private String ADSEQ;

	private String ADISPARENT;

	private String LASTUSER;

	private String LASTDATE;

	private String LASTTIME;

	public String toString() {
		return new ToStringBuilder(this).append("adopgroupid", getADOPGROUPID())
				.toString();
	}

	public String getADISPARENT() {
		return ADISPARENT;
	}

	public void setADISPARENT(String adisparent) {
		ADISPARENT = adisparent;
	}

	public String getADOPGROUP() {
		return ADOPGROUP;
	}

	public void setADOPGROUP(String adopgroup) {
		ADOPGROUP = adopgroup;
	}

	public String getADOPGROUPID() {
		return ADOPGROUPID;
	}

	public void setADOPGROUPID(String adopgroupid) {
		ADOPGROUPID = adopgroupid;
	}

	public String getADOPID() {
		return ADOPID;
	}

	public void setADOPID(String adopid) {
		ADOPID = adopid;
	}

	public String getADSEQ() {
		return ADSEQ;
	}

	public void setADSEQ(String adseq) {
		ADSEQ = adseq;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

	public String getLASTUSER() {
		return LASTUSER;
	}

	public void setLASTUSER(String lastuser) {
		LASTUSER = lastuser;
	}

}
