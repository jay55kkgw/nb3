package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="fstop.orm.po.TXNFXSCHPAYDATA_EC")
@Table(name = "TXNFXSCHPAYDATA_EC")
public class TXNFXSCHPAYDATA_EC implements Serializable
{


	/**
	 * 
	 */
	private static final long serialVersionUID = 7237969876010065538L;

	@Id
	private TXNFXSCHPAYDATA_EC_PK pks;// 複合組鍵
	
	private String ADOPID = "";

	private String FXWDAC = "";

	private String FXWDCURR = "";
	
	private String FXWDAMT = "";
	
	private String FXSVBH = "";
	
	private String FXSVAC = "";
	
	private String FXSVCURR = "";
	
	private String FXSVAMT = "";
	
	private String FXTXMEMO = "";
	
	private String FXTXMAILS = "";
	
	private String FXTXMAILMEMO = "";
	
	private String FXTXCODE = "";
	
	private String XMLCA = "";
	
	private String XMLCN = "";
	
	private String MAC = "";
	
	private String FXTXDATE = "";
	
	private String FXTXTIME = "";
	
	private String PCSEQ = "";
	
	private String ADTXNO = "";
	
	private String FXTOTAINFO = "";
	
	private String FXEFEECCY = "";
	
	private String FXEFEE = "";
	
	private String FXTELFEE = "";
	
	private String FXOURCHG = "";
	
	private String FXEXRATE = "";
	
	private String FXTXSTATUS = "";
	
	private String FXEXCODE = "";
	
	private String FXRESEND = "";
	
	private String FXMSGSEQNO = "";
	
	private String FXCERT = "";
	
	private String MSADDR = "";
	
	private String LASTDATE = "";
	
	private String LASTTIME = "";
	
	private String FXTITAINFO = "";

	public TXNFXSCHPAYDATA_EC_PK getPks() {
		return pks;
	}

	public void setPks(TXNFXSCHPAYDATA_EC_PK pks) {
		this.pks = pks;
	}

	public String getADOPID() {
		return ADOPID;
	}

	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}

	public String getFXWDAC() {
		return FXWDAC;
	}

	public void setFXWDAC(String fXWDAC) {
		FXWDAC = fXWDAC;
	}

	public String getFXWDCURR() {
		return FXWDCURR;
	}

	public void setFXWDCURR(String fXWDCURR) {
		FXWDCURR = fXWDCURR;
	}

	public String getFXWDAMT() {
		return FXWDAMT;
	}

	public void setFXWDAMT(String fXWDAMT) {
		FXWDAMT = fXWDAMT;
	}

	public String getFXSVBH() {
		return FXSVBH;
	}

	public void setFXSVBH(String fXSVBH) {
		FXSVBH = fXSVBH;
	}

	public String getFXSVAC() {
		return FXSVAC;
	}

	public void setFXSVAC(String fXSVAC) {
		FXSVAC = fXSVAC;
	}

	public String getFXSVCURR() {
		return FXSVCURR;
	}

	public void setFXSVCURR(String fXSVCURR) {
		FXSVCURR = fXSVCURR;
	}

	public String getFXSVAMT() {
		return FXSVAMT;
	}

	public void setFXSVAMT(String fXSVAMT) {
		FXSVAMT = fXSVAMT;
	}

	public String getFXTXMEMO() {
		return FXTXMEMO;
	}

	public void setFXTXMEMO(String fXTXMEMO) {
		FXTXMEMO = fXTXMEMO;
	}

	public String getFXTXMAILS() {
		return FXTXMAILS;
	}

	public void setFXTXMAILS(String fXTXMAILS) {
		FXTXMAILS = fXTXMAILS;
	}

	public String getFXTXMAILMEMO() {
		return FXTXMAILMEMO;
	}

	public void setFXTXMAILMEMO(String fXTXMAILMEMO) {
		FXTXMAILMEMO = fXTXMAILMEMO;
	}

	public String getFXTXCODE() {
		return FXTXCODE;
	}

	public void setFXTXCODE(String fXTXCODE) {
		FXTXCODE = fXTXCODE;
	}

	public String getXMLCA() {
		return XMLCA;
	}

	public void setXMLCA(String xMLCA) {
		XMLCA = xMLCA;
	}

	public String getXMLCN() {
		return XMLCN;
	}

	public void setXMLCN(String xMLCN) {
		XMLCN = xMLCN;
	}

	public String getMAC() {
		return MAC;
	}

	public void setMAC(String mAC) {
		MAC = mAC;
	}

	public String getFXTXDATE() {
		return FXTXDATE;
	}

	public void setFXTXDATE(String fXTXDATE) {
		FXTXDATE = fXTXDATE;
	}

	public String getFXTXTIME() {
		return FXTXTIME;
	}

	public void setFXTXTIME(String fXTXTIME) {
		FXTXTIME = fXTXTIME;
	}

	public String getPCSEQ() {
		return PCSEQ;
	}

	public void setPCSEQ(String pCSEQ) {
		PCSEQ = pCSEQ;
	}

	public String getADTXNO() {
		return ADTXNO;
	}

	public void setADTXNO(String aDTXNO) {
		ADTXNO = aDTXNO;
	}

	public String getFXTOTAINFO() {
		return FXTOTAINFO;
	}

	public void setFXTOTAINFO(String fXTOTAINFO) {
		FXTOTAINFO = fXTOTAINFO;
	}

	public String getFXEFEECCY() {
		return FXEFEECCY;
	}

	public void setFXEFEECCY(String fXEFEECCY) {
		FXEFEECCY = fXEFEECCY;
	}

	public String getFXEFEE() {
		return FXEFEE;
	}

	public void setFXEFEE(String fXEFEE) {
		FXEFEE = fXEFEE;
	}

	public String getFXTELFEE() {
		return FXTELFEE;
	}

	public void setFXTELFEE(String fXTELFEE) {
		FXTELFEE = fXTELFEE;
	}

	public String getFXOURCHG() {
		return FXOURCHG;
	}

	public void setFXOURCHG(String fXOURCHG) {
		FXOURCHG = fXOURCHG;
	}

	public String getFXEXRATE() {
		return FXEXRATE;
	}

	public void setFXEXRATE(String fXEXRATE) {
		FXEXRATE = fXEXRATE;
	}

	public String getFXTXSTATUS() {
		return FXTXSTATUS;
	}

	public void setFXTXSTATUS(String fXTXSTATUS) {
		FXTXSTATUS = fXTXSTATUS;
	}

	public String getFXEXCODE() {
		return FXEXCODE;
	}

	public void setFXEXCODE(String fXEXCODE) {
		FXEXCODE = fXEXCODE;
	}

	public String getFXRESEND() {
		return FXRESEND;
	}

	public void setFXRESEND(String fXRESEND) {
		FXRESEND = fXRESEND;
	}

	public String getFXMSGSEQNO() {
		return FXMSGSEQNO;
	}

	public void setFXMSGSEQNO(String fXMSGSEQNO) {
		FXMSGSEQNO = fXMSGSEQNO;
	}

	public String getFXCERT() {
		return FXCERT;
	}

	public void setFXCERT(String fXCERT) {
		FXCERT = fXCERT;
	}

	public String getMSADDR() {
		return MSADDR;
	}

	public void setMSADDR(String mSADDR) {
		MSADDR = mSADDR;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lASTDATE) {
		LASTDATE = lASTDATE;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}

	public String getFXTITAINFO() {
		return FXTITAINFO;
	}

	public void setFXTITAINFO(String fXTITAINFO) {
		FXTITAINFO = fXTITAINFO;
	}
	
	
	
}
