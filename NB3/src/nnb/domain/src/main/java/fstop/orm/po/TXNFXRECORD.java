package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity(name="fstop.orm.po.TXNFXRECORD")
@Table(name = "TXNFXRECORD")
public class TXNFXRECORD implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2493191345708691801L;	
	@Id	
	private String ADTXNO;//交易編號

	private String ADOPID = "";//操作功能

	private String FXUSERID = "";//使用者ID

	private String FXTXDATE = "";//實際轉帳日期

	private String FXTXTIME = "";//實際轉帳時間

	private String FXWDAC = "";//轉出帳號
	
	private String FXWDCURR = "";//轉出幣別

	private String FXWDAMT = "";//轉出金額

	private String FXSVBH = "";//轉入行庫代碼

	private String FXSVAC = "";//轉入帳號

	private String FXSVCURR = "";//轉入幣別

	private String FXSVAMT = "";//轉入金額

	private String FXTXMEMO = "";//備註*

	private String FXTXMAILS = "";//發送MAIL清單*

	private String FXTXMAILMEMO = "";//MAIL備註*

	private String FXTXCODE = "";//交易機制*

	private String FXTITAINFO = "";//交易上行
	private String FXTOTAINFO = "";//交易下行*

	private Long FXSCHID = 0L;//預約交易ID*
	
	private String FXEFEECCY = "";//手續費幣別*
	
	private String FXEFEE = "";//手續費*
	
	private String FXTELFEE="";//郵電費*
	
	private String FXOURCHG="";//郵電費*

	private String FXEXRATE = "";//匯率
	
	private String FXCERT = "";//交易單據

	private String FXREMAIL = "";//重發MAIL次數

	private String FXTXSTATUS = "";//交易執行狀況

	private String FXEXCODE = "";//交易錯誤代碼*

	private String FXMSGSEQNO = "";//電文傳送狀態

	private String FXMSGCONTENT = "";//電文傳送內容

	private String LASTDATE = "";//最後異動日期

	private String LASTTIME = "";//最後異動時間
	
	private String LOGINTYPE="";//NB或是MB登入
	
//	private String GDCERT="";

//	public String getGDCERT() {
//		return GDCERT;
//	}
//
//	public void setGDCERT(String gDCERT) {
//		GDCERT = gDCERT;
//	}

	public String getADTXNO() {
		return ADTXNO;
	}

	public void setADTXNO(String aDTXNO) {
		ADTXNO = aDTXNO;
	}

	public String getADOPID() {
		return ADOPID;
	}

	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}

	public String getFXUSERID() {
		return FXUSERID;
	}

	public void setFXUSERID(String fXUSERID) {
		FXUSERID = fXUSERID;
	}

	public String getFXTXDATE() {
		return FXTXDATE;
	}

	public void setFXTXDATE(String fXTXDATE) {
		FXTXDATE = fXTXDATE;
	}

	public String getFXTXTIME() {
		return FXTXTIME;
	}

	public void setFXTXTIME(String fXTXTIME) {
		FXTXTIME = fXTXTIME;
	}

	public String getFXWDAC() {
		return FXWDAC;
	}

	public void setFXWDAC(String fXWDAC) {
		FXWDAC = fXWDAC;
	}

	public String getFXWDCURR() {
		return FXWDCURR;
	}

	public void setFXWDCURR(String fXWDCURR) {
		FXWDCURR = fXWDCURR;
	}

	public String getFXWDAMT() {
		return FXWDAMT;
	}

	public void setFXWDAMT(String fXWDAMT) {
		FXWDAMT = fXWDAMT;
	}

	public String getFXSVBH() {
		return FXSVBH;
	}

	public void setFXSVBH(String fXSVBH) {
		FXSVBH = fXSVBH;
	}

	public String getFXSVAC() {
		return FXSVAC;
	}

	public void setFXSVAC(String fXSVAC) {
		FXSVAC = fXSVAC;
	}

	public String getFXSVCURR() {
		return FXSVCURR;
	}

	public void setFXSVCURR(String fXSVCURR) {
		FXSVCURR = fXSVCURR;
	}

	public String getFXSVAMT() {
		return FXSVAMT;
	}

	public void setFXSVAMT(String fXSVAMT) {
		FXSVAMT = fXSVAMT;
	}

	public String getFXTXMEMO() {
		return FXTXMEMO;
	}

	public void setFXTXMEMO(String fXTXMEMO) {
		FXTXMEMO = fXTXMEMO;
	}

	public String getFXTXMAILS() {
		return FXTXMAILS;
	}

	public void setFXTXMAILS(String fXTXMAILS) {
		FXTXMAILS = fXTXMAILS;
	}

	public String getFXTXMAILMEMO() {
		return FXTXMAILMEMO;
	}

	public void setFXTXMAILMEMO(String fXTXMAILMEMO) {
		FXTXMAILMEMO = fXTXMAILMEMO;
	}

	public String getFXTXCODE() {
		return FXTXCODE;
	}

	public void setFXTXCODE(String fXTXCODE) {
		FXTXCODE = fXTXCODE;
	}

	public String getFXTITAINFO() {
		return FXTITAINFO;
	}

	public void setFXTITAINFO(String fXTITAINFO) {
		FXTITAINFO = fXTITAINFO;
	}

	public String getFXTOTAINFO() {
		return FXTOTAINFO;
	}

	public void setFXTOTAINFO(String fXTOTAINFO) {
		FXTOTAINFO = fXTOTAINFO;
	}

	public Long getFXSCHID() {
		return FXSCHID;
	}

	public void setFXSCHID(Long fXSCHID) {
		FXSCHID = fXSCHID;
	}

	public String getFXEFEECCY() {
		return FXEFEECCY;
	}

	public void setFXEFEECCY(String fXEFEECCY) {
		FXEFEECCY = fXEFEECCY;
	}

	public String getFXEFEE() {
		return FXEFEE;
	}

	public void setFXEFEE(String fXEFEE) {
		FXEFEE = fXEFEE;
	}

	public String getFXTELFEE() {
		return FXTELFEE;
	}

	public void setFXTELFEE(String fXTELFEE) {
		FXTELFEE = fXTELFEE;
	}

	public String getFXOURCHG() {
		return FXOURCHG;
	}

	public void setFXOURCHG(String fXOURCHG) {
		FXOURCHG = fXOURCHG;
	}

	public String getFXEXRATE() {
		return FXEXRATE;
	}

	public void setFXEXRATE(String fXEXRATE) {
		FXEXRATE = fXEXRATE;
	}

	public String getFXCERT() {
		return FXCERT;
	}

	public void setFXCERT(String fXCERT) {
		FXCERT = fXCERT;
	}

	public String getFXREMAIL() {
		return FXREMAIL;
	}

	public void setFXREMAIL(String fXREMAIL) {
		FXREMAIL = fXREMAIL;
	}

	public String getFXTXSTATUS() {
		return FXTXSTATUS;
	}

	public void setFXTXSTATUS(String fXTXSTATUS) {
		FXTXSTATUS = fXTXSTATUS;
	}

	public String getFXEXCODE() {
		return FXEXCODE;
	}

	public void setFXEXCODE(String fXEXCODE) {
		FXEXCODE = fXEXCODE;
	}

	public String getFXMSGSEQNO() {
		return FXMSGSEQNO;
	}

	public void setFXMSGSEQNO(String fXMSGSEQNO) {
		FXMSGSEQNO = fXMSGSEQNO;
	}

	public String getFXMSGCONTENT() {
		return FXMSGCONTENT;
	}

	public void setFXMSGCONTENT(String fXMSGCONTENT) {
		FXMSGCONTENT = fXMSGCONTENT;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lASTDATE) {
		LASTDATE = lASTDATE;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}

	public String getLOGINTYPE() {
		return LOGINTYPE;
	}

	public void setLOGINTYPE(String lOGINTYPE) {
		LOGINTYPE = lOGINTYPE;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
