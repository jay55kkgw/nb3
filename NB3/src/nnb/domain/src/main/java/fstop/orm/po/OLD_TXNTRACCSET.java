package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="fstop.orm.po.OLD_TXNTRACCSET")
@Table(name = "TXNTRACCSET")
public class OLD_TXNTRACCSET implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -139759686350140527L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer DPACCSETID;

	private String DPUSERID;

	private String DPGONAME;

	private String DPTRACNO; 

	private String DPTRDACNO;

	private String LASTDATE;

	private String LASTTIME;

	private String DPTRIBANK;

	public Integer getDPACCSETID() {
		return DPACCSETID;
	}

	public void setDPACCSETID(Integer dPACCSETID) {
		DPACCSETID = dPACCSETID;
	}

	public String getDPUSERID() {
		return DPUSERID;
	}

	public void setDPUSERID(String dPUSERID) {
		DPUSERID = dPUSERID;
	}

	public String getDPGONAME() {
		return DPGONAME;
	}

	public void setDPGONAME(String dPGONAME) {
		DPGONAME = dPGONAME;
	}

	public String getDPTRACNO() {
		return DPTRACNO;
	}

	public void setDPTRACNO(String dPTRACNO) {
		DPTRACNO = dPTRACNO;
	}

	public String getDPTRDACNO() {
		return DPTRDACNO;
	}

	public void setDPTRDACNO(String dPTRDACNO) {
		DPTRDACNO = dPTRDACNO;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lASTDATE) {
		LASTDATE = lASTDATE;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}

	public String getDPTRIBANK() {
		return DPTRIBANK;
	}

	public void setDPTRIBANK(String dPTRIBANK) {
		DPTRIBANK = dPTRIBANK;
	}


}
