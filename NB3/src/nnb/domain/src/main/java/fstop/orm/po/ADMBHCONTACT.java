package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * 台企分行檔
 * 
 * @author Ian
 *
 */
@Entity(name = "fstop.orm.po.ADMBHCONTACT")
@Table(name = "ADMBHCONTACT")
public class ADMBHCONTACT implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5032029873617724995L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer ADBHCONTID;

	private String ADBRANCHID;//代號

	private String ADCONTACTIID;

	private String ADCONTACTNAME;

	private String ADCONTACTEMAIL;

	private String ADCONTACTTEL;// 電話

	private String LASTUSER;

	private String LASTDATE;

	private String LASTTIME;

	private String ADBRANCHNAME;// 分行中文

	private String ADBRANENGNAME;// 分行英文

	private String ADBRANCHSNAME;// 分行簡中

	@Override
	public String toString()
	{
		return "ADMBHCONTACT [ADBHCONTID=" + ADBHCONTID + ", ADBRANCHID=" + ADBRANCHID + ", ADCONTACTIID="
				+ ADCONTACTIID + ", ADCONTACTNAME=" + ADCONTACTNAME + ", ADCONTACTEMAIL=" + ADCONTACTEMAIL
				+ ", ADCONTACTTEL=" + ADCONTACTTEL + ", LASTUSER=" + LASTUSER + ", LASTDATE=" + LASTDATE + ", LASTTIME="
				+ LASTTIME + ", ADBRANCHNAME=" + ADBRANCHNAME + ", ADBRANENGNAME=" + ADBRANENGNAME + ", ADBRANCHSNAME="
				+ ADBRANCHSNAME + "]";
	}

	public Integer getADBHCONTID()
	{
		return ADBHCONTID;
	}

	public void setADBHCONTID(Integer aDBHCONTID)
	{
		ADBHCONTID = aDBHCONTID;
	}

	public String getADBRANCHID()
	{
		return ADBRANCHID;
	}

	public void setADBRANCHID(String aDBRANCHID)
	{
		ADBRANCHID = aDBRANCHID;
	}

	public String getADCONTACTIID()
	{
		return ADCONTACTIID;
	}

	public void setADCONTACTIID(String aDCONTACTIID)
	{
		ADCONTACTIID = aDCONTACTIID;
	}

	public String getADCONTACTNAME()
	{
		return ADCONTACTNAME;
	}

	public void setADCONTACTNAME(String aDCONTACTNAME)
	{
		ADCONTACTNAME = aDCONTACTNAME;
	}

	public String getADCONTACTEMAIL()
	{
		return ADCONTACTEMAIL;
	}

	public void setADCONTACTEMAIL(String aDCONTACTEMAIL)
	{
		ADCONTACTEMAIL = aDCONTACTEMAIL;
	}

	public String getADCONTACTTEL()
	{
		return ADCONTACTTEL;
	}

	public void setADCONTACTTEL(String aDCONTACTTEL)
	{
		ADCONTACTTEL = aDCONTACTTEL;
	}

	public String getLASTUSER()
	{
		return LASTUSER;
	}

	public void setLASTUSER(String lASTUSER)
	{
		LASTUSER = lASTUSER;
	}

	public String getLASTDATE()
	{
		return LASTDATE;
	}

	public void setLASTDATE(String lASTDATE)
	{
		LASTDATE = lASTDATE;
	}

	public String getLASTTIME()
	{
		return LASTTIME;
	}

	public void setLASTTIME(String lASTTIME)
	{
		LASTTIME = lASTTIME;
	}

	public String getADBRANCHNAME()
	{
		return ADBRANCHNAME;
	}

	public void setADBRANCHNAME(String aDBRANCHNAME)
	{
		ADBRANCHNAME = aDBRANCHNAME;
	}

	public String getADBRANENGNAME()
	{
		return ADBRANENGNAME;
	}

	public void setADBRANENGNAME(String aDBRANENGNAME)
	{
		ADBRANENGNAME = aDBRANENGNAME;
	}

	public String getADBRANCHSNAME()
	{
		return ADBRANCHSNAME;
	}

	public void setADBRANCHSNAME(String aDBRANCHSNAME)
	{
		ADBRANCHSNAME = aDBRANCHSNAME;
	}

}
