package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
/*
 * 2019/12/5 vincenthuang 修改 GDTITAINFO 為 String 
 * 之前long當作查詢碼 到查詢表查
 * */
@Entity
@Table(name = "TXNGDRECORD")
public class TXNGDRECORD implements Serializable {

	@Id
	private String ADTXNO;

	private String ADOPID = "";

	private String GDUSERID = "";

	private String GDTXDATE = "";

	private String GDTXTIME = "";

	private String GDWDAC = "";

	private String GDSVBH = "";

	private String GDSVAC = "";

	private String GDTXAMT = "";

	private String GDWEIGHT = "";
	
	private String GDPRICE = "";
	
	private String GDDISCOUNT = "";
	
	private String GDFEE1 = "";
	
	private String GDFEE2 = "";	

	private String GDTXMAILS = "";				

	private String GDTXCODE = "";   //交易機制

	private String GDTITAINFO = "";

	private String GDTOTAINFO = "";

	private String GDREMAIL = "";

	private String GDTXSTATUS = "";

	private String GDEXCODE = "";

	private String GDRETXNO = "";
	
	private String GDRETXSTATUS = "";

	private String LASTDATE = "";

	private String LASTTIME = "";
		//無法確認的欄位
//	private String GDCERT = "";
	
	public String getGDRETXSTATUS() {
		return GDRETXSTATUS;
	}

	public void setGDRETXSTATUS(String GDretxstatus) {
		GDRETXSTATUS = GDretxstatus;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

	public String getGDWEIGHT() {
		return GDWEIGHT;
	}

	public void setGDWEIGHT(String gdweight) {
		GDWEIGHT = gdweight;
	}

	public String getGDPRICE() {
		return GDPRICE;
	}

	public void setGDPRICE(String GDprice) {
		GDPRICE = GDprice;
	}
	
	public String getGDDISCOUNT() {
		return GDDISCOUNT;
	}

	public void setGDDISCOUNT(String GDdiscount) {
		GDDISCOUNT = GDdiscount;
	}
	
	

	public String getGDTITAINFO() {
		return GDTITAINFO;
	}

	public void setGDTITAINFO(String gDTITAINFO) {
		GDTITAINFO = gDTITAINFO;
	}

	public String getGDTOTAINFO() {
		return GDTOTAINFO;
	}

	public void setGDTOTAINFO(String GDtotainfo) {
		GDTOTAINFO = GDtotainfo;
	}

	public String getADTXNO() {
		return ADTXNO;
	}

	public void setADTXNO(String adtxno) {
		ADTXNO = adtxno;
	}

	public String getGDFEE1() {
		return GDFEE1;
	}

	public void setGDFEE1(String GDfee1) {
		GDFEE1 = GDfee1;
	}

	public String getGDFEE2() {
		return GDFEE2;
	}

	public void setGDFEE2(String GDfee2) {
		GDFEE2 = GDfee2;
	}
	
	public String getGDEXCODE() {
		return GDEXCODE;
	}

	public void setGDEXCODE(String GDexcode) {
		GDEXCODE = GDexcode;
	}

	public String getGDREMAIL() {
		return GDREMAIL;
	}

	public void setGDREMAIL(String GDremail) {
		GDREMAIL = GDremail;
	}

	public String getGDRETXNO() {
		return GDRETXNO;
	}

	public void setGDRETXNO(String GDretxno) {
		GDRETXNO = GDretxno;
	}

	public String getGDSVAC() {
		return GDSVAC;
	}

	public void setGDSVAC(String GDsvac) {
		GDSVAC = GDsvac;
	}

	public String getGDSVBH() {
		return GDSVBH;
	}

	public void setGDSVBH(String GDsvbh) {
		GDSVBH = GDsvbh;
	}

	public String getGDTXAMT() {
		return GDTXAMT;
	}

	public void setGDTXAMT(String GDtxamt) {
		GDTXAMT = GDtxamt;
	}

	public String getGDTXCODE() {
		return GDTXCODE;
	}

	public void setGDTXCODE(String GDtxcode) {
		GDTXCODE = GDtxcode;
	}

	public String getGDTXDATE() {
		return GDTXDATE;
	}

	public void setGDTXDATE(String GDtxdate) {
		GDTXDATE = GDtxdate;
	}

	public String getGDTXMAILS() {
		return GDTXMAILS;
	}

	public void setGDTXMAILS(String GDtxmails) {
		GDTXMAILS = GDtxmails;
	}

	public String getGDTXSTATUS() {
		return GDTXSTATUS;
	}

	public void setGDTXSTATUS(String GDtxstatus) {
		GDTXSTATUS = GDtxstatus;
	}

	public String getGDTXTIME() {
		return GDTXTIME;
	}

	public void setGDTXTIME(String GDtxtime) {
		GDTXTIME = GDtxtime;
	}

	public String getGDUSERID() {
		return GDUSERID;
	}

	public void setGDUSERID(String GDuserid) {
		GDUSERID = GDuserid;
	}

	public String getGDWDAC() {
		return GDWDAC;
	}

	public void setGDWDAC(String GDwdac) {
		GDWDAC = GDwdac;
	}

	public String getADOPID() {
		return ADOPID;
	}

	public void setADOPID(String adopid) {
		ADOPID = adopid;
	}

//	public String getGDCERT() {
//		return GDCERT;
//	}
//
//	public void setGDCERT(String GDcert) {
//		GDCERT = GDcert;
//	}
	
}
