package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;

@Entity
@Table(name = "POTRATE")
public class POTRATE implements Serializable {

	@Id
	private String POT;

	private String POTRATE;


	public String getPOT() {
		return POT;
	}

	public void setPOT(String pot) {
		POT = pot;
	}

	public String getPOTRATE() {
		return POTRATE;
	}

	public void setPOTRATE(String potrate) {
		POTRATE = potrate;
	}
}
