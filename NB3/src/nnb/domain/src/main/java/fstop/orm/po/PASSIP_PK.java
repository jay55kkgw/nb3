package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class PASSIP_PK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -356340662460748979L;
	
	
	private String ADBRANCHID = "";
	private String ADBRANCHIP = "";
	
	
	public String getADBRANCHID() {
		return ADBRANCHID;
	}
	public void setADBRANCHID(String aDBRANCHID) {
		ADBRANCHID = aDBRANCHID;
	}
	public String getADBRANCHIP() {
		return ADBRANCHIP;
	}
	public void setADBRANCHIP(String aDBRANCHIP) {
		ADBRANCHIP = aDBRANCHIP;
	}
	

	
	


}
