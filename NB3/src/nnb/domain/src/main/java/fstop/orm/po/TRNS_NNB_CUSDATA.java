package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TRNS_NNB_CUSDATA")
public class TRNS_NNB_CUSDATA implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -8554025058969289747L;

	@Id
	private String CUSIDN = "";

	private String TXNUSER = "";
	
	private String TXNTWRECORD = "";
	
	private String TXNTWSCHPAY = "";
	
	private String TXNFXRECORD = "";
	
	private String TXNFXSCHPAY = "";
	
	private String TXNGDRECORD = "";
	
	private String TXNTRACCSET = "";
	
	private String ADMMAILLOG = "";
	
	private String TXNADDRESSBOOK = "";
	
	private String TXNCUSINVATTRIB = "";
	
	private String TXNCUSINVATTRHIST = "";

	private String NB3USER = "";
	
	private String TXNPHONETOKEN = "";

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}

	public String getTXNUSER() {
		return TXNUSER;
	}

	public void setTXNUSER(String tXNUSER) {
		TXNUSER = tXNUSER;
	}

	public String getTXNTWRECORD() {
		return TXNTWRECORD;
	}

	public void setTXNTWRECORD(String tXNTWRECORD) {
		TXNTWRECORD = tXNTWRECORD;
	}

	public String getTXNTWSCHPAY() {
		return TXNTWSCHPAY;
	}

	public void setTXNTWSCHPAY(String tXNTWSCHPAY) {
		TXNTWSCHPAY = tXNTWSCHPAY;
	}

	public String getTXNFXRECORD() {
		return TXNFXRECORD;
	}

	public void setTXNFXRECORD(String tXNFXRECORD) {
		TXNFXRECORD = tXNFXRECORD;
	}

	public String getTXNFXSCHPAY() {
		return TXNFXSCHPAY;
	}

	public void setTXNFXSCHPAY(String tXNFXSCHPAY) {
		TXNFXSCHPAY = tXNFXSCHPAY;
	}

	public String getTXNGDRECORD() {
		return TXNGDRECORD;
	}

	public void setTXNGDRECORD(String tXNGDRECORD) {
		TXNGDRECORD = tXNGDRECORD;
	}

	public String getTXNTRACCSET() {
		return TXNTRACCSET;
	}

	public void setTXNTRACCSET(String tXNTRACCSET) {
		TXNTRACCSET = tXNTRACCSET;
	}

	public String getADMMAILLOG() {
		return ADMMAILLOG;
	}

	public void setADMMAILLOG(String aDMMAILLOG) {
		ADMMAILLOG = aDMMAILLOG;
	}

	public String getTXNADDRESSBOOK() {
		return TXNADDRESSBOOK;
	}

	public void setTXNADDRESSBOOK(String tXNADDRESSBOOK) {
		TXNADDRESSBOOK = tXNADDRESSBOOK;
	}

	public String getTXNCUSINVATTRIB() {
		return TXNCUSINVATTRIB;
	}

	public void setTXNCUSINVATTRIB(String tXNCUSINVATTRIB) {
		TXNCUSINVATTRIB = tXNCUSINVATTRIB;
	}

	public String getTXNCUSINVATTRHIST() {
		return TXNCUSINVATTRHIST;
	}

	public void setTXNCUSINVATTRHIST(String tXNCUSINVATTRHIST) {
		TXNCUSINVATTRHIST = tXNCUSINVATTRHIST;
	}
	
	public String getNB3USER() {
		return NB3USER;
	}

	public void setNB3USER(String nB3USER) {
		NB3USER = nB3USER;
	}

	public String getTXNPHONETOKEN() {
		return TXNPHONETOKEN;
	}

	public void setTXNPHONETOKEN(String tXNPHONETOKEN) {
		TXNPHONETOKEN = tXNPHONETOKEN;
	}

	public boolean checkCus() {
		return checkVal(TXNUSER) 
				&& checkVal(TXNTWRECORD)
				&& checkVal(TXNTWSCHPAY) 
				&& checkVal(TXNFXRECORD) 
				&& checkVal(TXNFXSCHPAY) 
				&& checkVal(TXNGDRECORD) 
				&& checkVal(TXNTRACCSET)
				// ADMMAILLOG--電郵記錄檔--20200316 不即時同步，改用批次
//				&& checkVal(ADMMAILLOG)
				&& checkVal(TXNADDRESSBOOK) 
				&& checkVal(TXNCUSINVATTRIB) 
				&& checkVal(TXNCUSINVATTRHIST)
				&& checkVal(NB3USER)
				&& checkVal(TXNPHONETOKEN)
				;
	}
	
	public boolean checkCusNB3() {
		return checkVal(TXNUSER) 
				&& checkVal(TXNTWRECORD)
				&& checkVal(TXNTWSCHPAY) 
				&& checkVal(TXNFXRECORD) 
				&& checkVal(TXNFXSCHPAY) 
				&& checkVal(TXNGDRECORD) 
				&& checkVal(TXNTRACCSET)
				// ADMMAILLOG--電郵記錄檔--20200316 不即時同步，改用批次
//				&& checkVal(ADMMAILLOG)
				&& checkVal(TXNADDRESSBOOK) 
				&& checkVal(TXNCUSINVATTRIB) 
				&& checkVal(TXNCUSINVATTRHIST)
				&& checkVal(TXNPHONETOKEN)
				;
	}
	
	private boolean checkVal(String col) {
		return "0".equals(col);
	}
	
	
}
