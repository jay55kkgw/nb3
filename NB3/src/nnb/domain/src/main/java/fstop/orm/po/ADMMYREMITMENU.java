package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity(name="fstop.orm.po.ADMMYREMITMENU")
@Table(name = "ADMMYREMITMENU")
public class ADMMYREMITMENU implements Serializable { 

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long ADSEQNO;

	private String ADUID = "";
	private String ADRMTTYPE = "";	
	private String ADRMTID = "";

	private String LASTUSER = "";
	private String LASTDATE = "";
	private String LASTTIME = "";


	public String toString() {
		return new ToStringBuilder(this).append("adseqno", getADSEQNO())
				.toString();
	}


	public String getADRMTID() {
		return ADRMTID;
	}


	public void setADRMTID(String adrmtid) {
		ADRMTID = adrmtid;
	}


	public Long getADSEQNO() {
		return ADSEQNO;
	}


	public void setADSEQNO(Long adseqno) {
		ADSEQNO = adseqno;
	}


	public String getADUID() {
		return ADUID;
	}


	public void setADUID(String aduid) {
		ADUID = aduid;
	}

	public String getADRMTTYPE() {
		return ADRMTTYPE;
	}


	public void setADRMTTYPE(String adrmttype) {
		ADRMTTYPE = adrmttype;
	}
	
	public String getLASTDATE() {
		return LASTDATE;
	}


	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}


	public String getLASTTIME() {
		return LASTTIME;
	}


	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}


	public String getLASTUSER() {
		return LASTUSER;
	}


	public void setLASTUSER(String lastuser) {
		LASTUSER = lastuser;
	}



}
