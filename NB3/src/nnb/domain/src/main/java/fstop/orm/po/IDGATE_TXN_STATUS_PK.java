package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Embeddable;


@Embeddable
public class IDGATE_TXN_STATUS_PK  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5491873221952721452L;
	
	private String SESSIONID;
	private String IDGATEID;
	private String TXNID;
	public String getSESSIONID() {
		return SESSIONID;
	}
	public void setSESSIONID(String sESSIONID) {
		SESSIONID = sESSIONID;
	}
	public String getIDGATEID() {
		return IDGATEID;
	}
	public void setIDGATEID(String iDGATEID) {
		IDGATEID = iDGATEID;
	}
	public String getTXNID() {
		return TXNID;
	}
	public void setTXNID(String tXNID) {
		TXNID = tXNID;
	}
}
