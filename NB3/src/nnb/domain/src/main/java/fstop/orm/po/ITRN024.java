package fstop.orm.po;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "ITRN024")
public class ITRN024 implements Serializable {

	@Id
	private String RECNO;

	private String HEADER;

	private String SEQ;

	private String DATE;

	private String TIME;

	private String COUNT;
	
	private String COLOR;
	
	private String CRYNAME;
	
	private String CRY;
	
	private String ITR1;
	
	private String ITR2;
	
	private String FILL;
	
	public String toString() {
		return new ToStringBuilder(this).append("recno", getRECNO())
				.toString();
	}

	public String getRECNO() {
		return RECNO;
	}

	public void setRECNO(String recno) {
		RECNO = recno;
	}

	public String getHEADER() {
		return HEADER;
	}

	public void setHEADER(String header) {
		HEADER = header;
	}

	public String getSEQ() {
		return SEQ;
	}

	public void setSEQ(String seq) {
		SEQ = seq;
	}

	public String getDATE() {
		return DATE;
	}

	public void setDATE(String date) {
		DATE = date;
	}

	public String getTIME() {
		return TIME;
	}

	public void setTIME(String time) {
		TIME = time;
	}

	public String getCOUNT() {
		return COUNT;
	}

	public void setCOUNT(String count) {
		COUNT = count;
	}

	public String getCOLOR() {
		return COLOR;
	}

	public void setCOLOR(String color) {
		COLOR = color;
	}

	public String getCRYNAME() {
		return CRYNAME;
	}

	public void setCRYNAME(String cryname) {
		CRYNAME = cryname;
	}

	public String getCRY() {
		return CRY;
	}

	public void setCRY(String cry) {
		CRY = cry;
	}

	public String getITR1() {
		return ITR1;
	}

	public void setITR1(String itr1) {
		ITR1 = itr1;
	}

	public String getITR2() {
		return ITR2;
	}

	public void setITR2(String itr2) {
		ITR2 = itr2;
	}

	public String getFILL() {
		return FILL;
	}

	public void setFILL(String fill) {
		FILL = fill;
	}

}
