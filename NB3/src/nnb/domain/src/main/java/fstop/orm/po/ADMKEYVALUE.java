package fstop.orm.po;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity(name="fstop.orm.po.ADMKEYVALUE")
@Table(name = "ADMKEYVALUE")
public class ADMKEYVALUE implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	private String ADKEYVALUEID;

	private String CATEGORY;
	
	private String KEY;

	private String VALUE;
	
	private String ENGVALUE;
	
	private String CHSVALUE;

	private String LASTUSER;

	private String LASTDATE;

	private String LASTTIME;

	public String getENGVALUE() {
		return ENGVALUE;
	}

	public void setENGVALUE(String eNGVALUE) {
		ENGVALUE = eNGVALUE;
	}

	public String getCHSVALUE() {
		return CHSVALUE;
	}

	public void setCHSVALUE(String cHSVALUE) {
		CHSVALUE = cHSVALUE;
	}
	public String getADKEYVALUEID() {
		return ADKEYVALUEID;
	}

	public void setADKEYVALUEID(String adkeyvalueid) {
		ADKEYVALUEID = adkeyvalueid;
	}

	public String getCATEGORY() {
		return CATEGORY;
	}

	public void setCATEGORY(String category) {
		CATEGORY = category;
	}

	public String getKEY() {
		return KEY;
	}

	public void setKEY(String key) {
		KEY = key;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

	public String getLASTUSER() {
		return LASTUSER;
	}

	public void setLASTUSER(String lastuser) {
		LASTUSER = lastuser;
	}

	public String getVALUE() {
		return VALUE;
	}

	public void setVALUE(String value) {
		VALUE = value;
	}

}
