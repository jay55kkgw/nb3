package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class ADMMYOP_PK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7131777467457041067L;
	
	private String ADUSERID; 	// 使用者名稱	
	private String ADOPID; 		// 功能代號
	
	public String getADUSERID() {
		return ADUSERID;
	}
	public void setADUSERID(String aDUSERID) {
		ADUSERID = aDUSERID;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	
	public ADMMYOP_PK(String aDUSERID, String aDOPID) {
		super();
		ADUSERID = aDUSERID;
		ADOPID = aDOPID;
	}
	
	
}
