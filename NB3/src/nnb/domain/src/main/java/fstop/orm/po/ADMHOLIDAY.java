package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity(name="fstop.orm.po.ADMHOLIDAY")
@Table(name = "ADMHOLIDAY")
public class ADMHOLIDAY implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7143291581229263677L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)	
	private String ADHOLIDAYID;

	private String ADREMARK;

	private String LASTUSER = "";

	private String LASTDATE = "";

	private String LASTTIME = "";

	public String toString() {
		return new ToStringBuilder(this)
				.append("adholidayid", getADHOLIDAYID()).toString();
	}

	public String getADHOLIDAYID() {
		return ADHOLIDAYID;
	}

	public void setADHOLIDAYID(String adholidayid) {
		ADHOLIDAYID = adholidayid;
	}

	public String getADREMARK() {
		return ADREMARK;
	}

	public void setADREMARK(String adremark) {
		ADREMARK = adremark;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

	public String getLASTUSER() {
		return LASTUSER;
	}

	public void setLASTUSER(String lastuser) {
		LASTUSER = lastuser;
	}

}
