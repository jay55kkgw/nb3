package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="fstop.orm.po.CITIES")
@Table(name = "CITIES")
public class CITIES implements Serializable{

	/**
	 * 台灣身分證縣市
	 */
	private static final long serialVersionUID = 7117430904611300143L;

	@Id
	private String CITYID = "";
	
	private String CITYCODE = "";
	
	private String CITYNAME = "";
	
	
	public String getCITYID() {
		return CITYID;
	}

	public void setCITYID(String cITYID) {
		CITYID = cITYID;
	}
	
	public String getCITYCODE() {
		return CITYCODE;
	}

	public void setCITYCODE(String cITYCODE) {
		CITYCODE = cITYCODE;
	}
	
	public String getCITYNAME() {
		return CITYNAME;
	}

	public void setCITYNAME(String cITYNAME) {
		CITYNAME = cITYNAME;
	}
}
