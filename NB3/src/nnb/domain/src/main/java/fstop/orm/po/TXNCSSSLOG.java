package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/*
 * 顧客滿意度調查記錄檔(Customer Service Satisfaction Survey Log)
 * */

@Entity(name="fstop.orm.po.TXNCSSSLOG")
@Table(name = "TXNCSSSLOG")
public class TXNCSSSLOG implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6356514297164822355L;

	//流水號
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String LOGID = "";
	
	/*身分證字號 
	 * varchar:10
	 * null:no
	 * */
	private String CUSIDN = "";
	
	/*功能代號 
	 *varchar:10 
	 *null:no
	 * */
	private String ADOPID = "";
	
	/*滿意度答案
	 *  5：非常滿意
		4：滿意
		3：普通
		2：再加強
		1：非常不滿意
	 *  varchar:1
	 *  null:yes
	 *  */ 
	private String ANSWER = "";
	
	/*
	 * 下次再填註記 ，Y：代表按了下次再填
	 * varchar:1
	 * null:yes
	 * */
	private String NEXTBTN = "";
	
	/*
	 * 填寫日期 ，格式：YYYYMMDD
	 * varchar:8
	 * null:no
	 * */
	private String LASTDATE = "";
	
	/*
	 * 填寫時間，格式：hhmmss
	 * varchar:6
	 * null:no
	 * */
	private String LASTTIME = "";
	
	/*  通路別
	 *  NB3：新世代網銀
		NB：舊網銀
		MB：舊行動
		MB3：新世代行動銀行
	 * varchar:6
	 * null:no
	 * */
	private String CHANNEL = "";
	
	//留言 最大長度300
	private String MSG = "";
	
	public String getLOGID() {
		return LOGID;
	}

	public String getMSG() {
		return MSG;
	}

	public void setMSG(String mSG) {
		MSG = mSG;
	}

	public void setLOGID(String lOGID) {
		LOGID = lOGID;
	}

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}

	public String getADOPID() {
		return ADOPID;
	}

	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}

	public String getANSWER() {
		return ANSWER;
	}

	public void setANSWER(String aNSWER) {
		ANSWER = aNSWER;
	}

	public String getNEXTBTN() {
		return NEXTBTN;
	}

	public void setNEXTBTN(String nEXTBTN) {
		NEXTBTN = nEXTBTN;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lASTDATE) {
		LASTDATE = lASTDATE;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}

	public String getCHANNEL() {
		return CHANNEL;
	}

	public void setCHANNEL(String channel) {
		CHANNEL = channel;
	}
	
}
