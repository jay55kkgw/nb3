package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "TXNFUNDCOMPANY")
public class TXNFUNDCOMPANY implements Serializable {

	@Id
	private String COMPANYCODE;

	private String COUNTRYTYPE;

	private String COMPANYLNAME;

	private String COMPANYSNAME;

	private String FUNDHOUSEMARK;

	private String FUNDGROUPMARK;
	
	public String toString() {
		return new ToStringBuilder(this)
				.append("companycode", getCOMPANYCODE()).toString();
	}

	public String getCOMPANYCODE() {
		return COMPANYCODE;
	}

	public void setCOMPANYCODE(String companycode) {
		COMPANYCODE = companycode;
	}

	public String getCOMPANYLNAME() {
		return COMPANYLNAME;
	}

	public void setCOMPANYLNAME(String companylname) {
		COMPANYLNAME = companylname;
	}

	public String getCOMPANYSNAME() {
		return COMPANYSNAME;
	}

	public void setCOMPANYSNAME(String companysname) {
		COMPANYSNAME = companysname;
	}

	public String getCOUNTRYTYPE() {
		return COUNTRYTYPE;
	}

	public void setCOUNTRYTYPE(String countrytype) {
		COUNTRYTYPE = countrytype;
	}

	public String getFUNDHOUSEMARK() {
		return FUNDHOUSEMARK;
	}

	public void setFUNDHOUSEMARK(String fundhousemark) {
		FUNDHOUSEMARK = fundhousemark;
	}

	public String getFUNDGROUPMARK() {
		return FUNDGROUPMARK;
	}

	public void setFUNDGROUPMARK(String fundgroupmark) {
		FUNDGROUPMARK = fundgroupmark;
	}	
	
}
