package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "TXNRECMAILLOGRELATION")
public class TXNRECMAILLOGRELATION implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7842332220597241239L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long RELID;

	private String REC_TYPE = ""; // FX or TW

	private String ADTXNO = "";

	private Long ADMAILLOGID;

	private String LASTDATE = "";

	private String LASTTIME = "";
	
	private String CUSIDN = "";

	public Long getADMAILLOGID() {
		return ADMAILLOGID;
	}

	public void setADMAILLOGID(Long admaillogid) {
		ADMAILLOGID = admaillogid;
	}

	public String getADTXNO() {
		return ADTXNO;
	}

	public void setADTXNO(String adtxno) {
		ADTXNO = adtxno;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

	public String getREC_TYPE() {
		return REC_TYPE;
	}

	public void setREC_TYPE(String rec_type) {
		REC_TYPE = rec_type;
	}

	public Long getRELID() {
		return RELID;
	}

	public void setRELID(Long relid) {
		RELID = relid;
	}

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}

	public String toString() {
		return new ToStringBuilder(this)
				.append("relid", getADMAILLOGID()).toString();
	}


}
