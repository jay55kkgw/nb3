package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "SYSBATCH")
public class SYSBATCH implements Serializable {

	@Id
	//@GeneratedValue(strategy = GenerationType.AUTO)
	private String ADBATID;

	private String ADBATRUN;

	public String toString() {
		return new ToStringBuilder(this).append("adbatid", getADBATID())
				.toString();
	}

	public String getADBATID() {
		return ADBATID;
	}

	public void setADBATID(String adbatid) {
		ADBATID = adbatid;
	}

	public String getADBATRUN() {
		return ADBATRUN;
	}

	public void setADBATRUN(String adbatrun) {
		ADBATRUN = adbatrun;
	}

}
