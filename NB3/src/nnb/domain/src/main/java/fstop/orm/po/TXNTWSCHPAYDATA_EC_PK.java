package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class TXNTWSCHPAYDATA_EC_PK implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3800510413815657298L;
	

	private String DPSCHNO = "";
	private String DPUSERID = "";
	private String DPSCHTXDATE = "";
	
	
	public String getDPSCHNO() {
		return DPSCHNO;
	}
	public void setDPSCHNO(String dPSCHNO) {
		DPSCHNO = dPSCHNO;
	}
	public String getDPUSERID() {
		return DPUSERID;
	}
	public void setDPUSERID(String dPUSERID) {
		DPUSERID = dPUSERID;
	}
	public String getDPSCHTXDATE() {
		return DPSCHTXDATE;
	}
	public void setDPSCHTXDATE(String dPSCHTXDATE) {
		DPSCHTXDATE = dPSCHTXDATE;
	}
	
	
}
