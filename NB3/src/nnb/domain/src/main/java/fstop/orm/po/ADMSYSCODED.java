package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "ADMSYSCODED")
public class ADMSYSCODED implements Serializable{

	private String CODEKIND;	//代碼種類
	private String CODEID;	//代碼
	private String CODEPARENT;	//父代碼
	private String CODENAME;	//代碼內容
	private String CODESNAME;	//代碼簡稱
	private String ISENABLE;	//可使用
	private String SORTORDER;	//排序
	private String MEMO;	//備註
	@Id
	public String getCODEKIND() {
		return CODEKIND;
	}
	public void setCODEKIND(String codekind) {
		CODEKIND = codekind;
	}
	@Id
	public String getCODEID() {
		return CODEID;
	}
	public void setCODEID(String codeid) {
		CODEID = codeid;
	}
	public String getCODEPARENT() {
		return CODEPARENT;
	}
	public void setCODEPARENT(String codeparent) {
		CODEPARENT = codeparent;
	}
	public String getCODENAME() {
		return CODENAME;
	}
	public void setCODENAME(String codename) {
		CODENAME = codename;
	}
	public String getCODESNAME() {
		return CODESNAME;
	}
	public void setCODESNAME(String codesname) {
		CODESNAME = codesname;
	}
	public String getISENABLE() {
		return ISENABLE;
	}
	public void setISENABLE(String isenable) {
		ISENABLE = isenable;
	}
	public String getSORTORDER() {
		return SORTORDER;
	}
	public void setSORTORDER(String sortorder) {
		SORTORDER = sortorder;
	}
	public String getMEMO() {
		return MEMO;
	}
	public void setMEMO(String memo) {
		MEMO = memo;
	}
}
