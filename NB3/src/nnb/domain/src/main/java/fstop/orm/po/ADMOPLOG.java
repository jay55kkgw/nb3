package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "ADMOPLOG")
public class ADMOPLOG implements Serializable {

	@Id
	private String ADTXNO;

	private String ADUSERID = "";

	private String ADUSERIP = "";

	private String ADTXDATE = "";

	private String ADTXTIME = "";
	
	private String ADTXITEM = "";

	private String ADLOGGING = "";

	private String ADOPID = "";

	private String ADOPITEM = "";

	private String ADBCHCON = "";

	private String ADACHCON = "";

	private String ADEXCODE = "";

	public String toString() {
		return new ToStringBuilder(this).append("adtxno", getADTXNO())
				.toString();
	}

	public String getADACHCON() {
		return ADACHCON;
	}

	public void setADACHCON(String adachcon) {
		ADACHCON = adachcon;
	}

	public String getADBCHCON() {
		return ADBCHCON;
	}

	public void setADBCHCON(String adbchcon) {
		ADBCHCON = adbchcon;
	}

	public String getADEXCODE() {
		return ADEXCODE;
	}

	public void setADEXCODE(String adexcode) {
		ADEXCODE = adexcode;
	}

	public String getADOPID() {
		return ADOPID;
	}

	public void setADOPID(String adopid) {
		ADOPID = adopid;
	}

	public String getADOPITEM() {
		return ADOPITEM;
	}

	public void setADOPITEM(String adopitem) {
		ADOPITEM = adopitem;
	}

	public String getADTXDATE() {
		return ADTXDATE;
	}

	public void setADTXDATE(String adtxdate) {
		ADTXDATE = adtxdate;
	}

	public String getADTXNO() {
		return ADTXNO;
	}

	public void setADTXNO(String adtxno) {
		ADTXNO = adtxno;
	}

	public String getADTXTIME() {
		return ADTXTIME;
	}

	public void setADTXTIME(String adtxtime) {
		ADTXTIME = adtxtime;
	}

	public String getADUSERID() {
		return ADUSERID;
	}

	public void setADUSERID(String aduserid) {
		ADUSERID = aduserid;
	}

	public String getADUSERIP() {
		return ADUSERIP;
	}

	public void setADUSERIP(String aduserip) {
		ADUSERIP = aduserip;
	}

	public String getADLOGGING() {
		return ADLOGGING;
	}

	public void setADLOGGING(String adlogging) {
		ADLOGGING = adlogging;
	}

	public String getADTXITEM() {
		return ADTXITEM;
	}

	public void setADTXITEM(String adtxitem) {
		ADTXITEM = adtxitem;
	}

}
