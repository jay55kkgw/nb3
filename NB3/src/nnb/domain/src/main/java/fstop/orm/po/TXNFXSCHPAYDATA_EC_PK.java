package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class TXNFXSCHPAYDATA_EC_PK implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2283575442001524539L;
	
	private String FXSCHNO = "";
	private String FXUSERID = "";
	private String FXSCHTXDATE ="";
	
	
	public String getFXSCHNO() {
		return FXSCHNO;
	}
	public void setFXSCHNO(String fXSCHNO) {
		FXSCHNO = fXSCHNO;
	}
	public String getFXUSERID() {
		return FXUSERID;
	}
	public void setFXUSERID(String fXUSERID) {
		FXUSERID = fXUSERID;
	}
	public String getFXSCHTXDATE() {
		return FXSCHTXDATE;
	}
	public void setFXSCHTXDATE(String fXSCHTXDATE) {
		FXSCHTXDATE = fXSCHTXDATE;
	}
	
	
	
}
