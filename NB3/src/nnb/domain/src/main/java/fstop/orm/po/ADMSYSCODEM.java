package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "ADMSYSCODEM")
public class ADMSYSCODEM implements Serializable{

	private String CODEKIND;	//代碼種類
	private String CODEPARENT;	//父代碼種類
	private String CODENAME	;//代碼內容
	private String CODESNAME;	//代碼簡稱
	private String ISVISIBLE;	//是否顯示
	private String CODESYSID;	//系統別
	private String EDITFLAG;	//可維護
	@Id
	public String getCODEKIND() {
		return CODEKIND;
	}
	public void setCODEKIND(String codekind) {
		CODEKIND = codekind;
	}
	public String getCODEPARENT() {
		return CODEPARENT;
	}
	public void setCODEPARENT(String codeparent) {
		CODEPARENT = codeparent;
	}
	public String getCODENAME() {
		return CODENAME;
	}
	public void setCODENAME(String codename) {
		CODENAME = codename;
	}
	public String getCODESNAME() {
		return CODESNAME;
	}
	public void setCODESNAME(String codesname) {
		CODESNAME = codesname;
	}
	public String getISVISIBLE() {
		return ISVISIBLE;
	}
	public void setISVISIBLE(String isvisible) {
		ISVISIBLE = isvisible;
	}
	public String getCODESYSID() {
		return CODESYSID;
	}
	public void setCODESYSID(String codesysid) {
		CODESYSID = codesysid;
	}
	public String getEDITFLAG() {
		return EDITFLAG;
	}
	public void setEDITFLAG(String editflag) {
		EDITFLAG = editflag;
	}
	
	

}
