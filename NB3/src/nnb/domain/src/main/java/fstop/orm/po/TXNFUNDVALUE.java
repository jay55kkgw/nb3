package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "TXNFUNDVALUE")
public class TXNFUNDVALUE implements Serializable {

	@Id
	private String TRANSCODE;

	private String REFUNDAMT;

	private String TRADEDATE;

	private String FUNDREMARK;

	public String toString() {
		return new ToStringBuilder(this).append("transcode", getTRANSCODE())
				.toString();
	}

	public String getFUNDREMARK() {
		return FUNDREMARK;
	}

	public void setFUNDREMARK(String fundremark) {
		FUNDREMARK = fundremark;
	}

	public String getREFUNDAMT() {
		return REFUNDAMT;
	}

	public void setREFUNDAMT(String refundamt) {
		REFUNDAMT = refundamt;
	}

	public String getTRADEDATE() {
		return TRADEDATE;
	}

	public void setTRADEDATE(String tradedate) {
		TRADEDATE = tradedate;
	}

	public String getTRANSCODE() {
		return TRANSCODE;
	}

	public void setTRANSCODE(String transcode) {
		TRANSCODE = transcode;
	}

}
