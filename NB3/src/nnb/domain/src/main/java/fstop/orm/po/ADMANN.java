package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name = "ADMANN")
public class ADMANN implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5624617231448798859L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String ID = "";
	
	private String OID = "";
	private String ALLUSER = "";
	
	@Lob 
	@Basic(fetch = FetchType.LAZY) 
	@Column(name="USERLIST", columnDefinition="CLOB", nullable=true)
	private String USERLIST = "";

	private String FILENAME = "";
	private String TYPE = "";
	private Integer SORTORDER;
	private String STARTDATE = "";
	private String STARTTIME = "";
	private String ENDDATE = "";
	private String ENDTIME = "";
	private String LASTUSER = "";
	private String LASTDATE = "";
	private String LASTTIME = "";

	@Lob 
	@Basic(fetch = FetchType.LAZY) 
	@Column(name="CONTENT", columnDefinition="CLOB", nullable=true)
	private String CONTENT = "";
	
	private String URL = "";
	private String CONTENTTYPE = "";
	private String SRCFILENAME = "";
	
	@Lob 
	@Basic(fetch = FetchType.LAZY) 
	@Column(name="SRCCONTENT", columnDefinition="BLOB", nullable=true)
	private byte[] SRCCONTENT;
	
	private String TITLE = "";
	
	

	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}

	public String getOID() {
		return OID;
	}

	public void setOID(String oID) {
		OID = oID;
	}

	public String getALLUSER() {
		return ALLUSER;
	}

	public void setALLUSER(String aLLUSER) {
		ALLUSER = aLLUSER;
	}

	public String getUSERLIST() {
		return USERLIST;
	}

	public void setUSERLIST(String uSERLIST) {
		USERLIST = uSERLIST;
	}

	public String getFILENAME() {
		return FILENAME;
	}

	public void setFILENAME(String fILENAME) {
		FILENAME = fILENAME;
	}

	public String getTYPE() {
		return TYPE;
	}

	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}

	public Integer getSORTORDER() {
		return SORTORDER;
	}

	public void setSORTORDER(Integer sORTORDER) {
		SORTORDER = sORTORDER;
	}

	public String getSTARTDATE() {
		return STARTDATE;
	}

	public void setSTARTDATE(String sTARTDATE) {
		STARTDATE = sTARTDATE;
	}

	public String getSTARTTIME() {
		return STARTTIME;
	}

	public void setSTARTTIME(String sTARTTIME) {
		STARTTIME = sTARTTIME;
	}

	public String getENDDATE() {
		return ENDDATE;
	}

	public void setENDDATE(String eNDDATE) {
		ENDDATE = eNDDATE;
	}

	public String getENDTIME() {
		return ENDTIME;
	}

	public void setENDTIME(String eNDTIME) {
		ENDTIME = eNDTIME;
	}

	public String getLASTUSER() {
		return LASTUSER;
	}

	public void setLASTUSER(String lASTUSER) {
		LASTUSER = lASTUSER;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lASTDATE) {
		LASTDATE = lASTDATE;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}

	public String getCONTENT() {
		return CONTENT;
	}

	public void setCONTENT(String cONTENT) {
		CONTENT = cONTENT;
	}

	public String getURL() {
		return URL;
	}

	public void setURL(String uRL) {
		URL = uRL;
	}

	public String getCONTENTTYPE() {
		return CONTENTTYPE;
	}

	public void setCONTENTTYPE(String cONTENTTYPE) {
		CONTENTTYPE = cONTENTTYPE;
	}

	public String getSRCFILENAME() {
		return SRCFILENAME;
	}

	public void setSRCFILENAME(String sRCFILENAME) {
		SRCFILENAME = sRCFILENAME;
	}

	public byte[] getSRCCONTENT() {
		return SRCCONTENT;
	}

	public void setSRCCONTENT(byte[] sRCCONTENT) {
		SRCCONTENT = sRCCONTENT;
	}

	public String getTITLE() {
		return TITLE;
	}

	public void setTITLE(String tITLE) {
		TITLE = tITLE;
	}
	
	
	
}
