package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ADMMONTHREPORT")
public class ADMMONTHREPORT implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long ADREPORTID; // 報表資料ID

	private String CMYYYYMM; // 西元年月

	private String ADOPID; // 操作功能ID

	private String ADUSERTYPE; // 用戶類別

	private String ADAGREEF; // 約定/非約定

	private String ADBKFLAG; // 自行/跨行

	private String ADLEVEL; // 金額級距筆數

	private String ADTXSTATUS; // 交易狀態

	private String ADTXCODE; // 交易機制

	private String ADCOUNT; // 筆數

	private Long ADAMOUNT; // 台幣金額
	
	private String LOGINTYPE = ""; //NB:網銀 ， MB:行動 ，預設為NB


	public String getADAGREEF() {
		return ADAGREEF;
	}

	public void setADAGREEF(String adagreef) {
		ADAGREEF = adagreef;
	}

	public Long getADAMOUNT() {
		return ADAMOUNT;
	}

	public void setADAMOUNT(Long adamount) {
		ADAMOUNT = adamount;
	}

	public String getADBKFLAG() {
		return ADBKFLAG;
	}

	public void setADBKFLAG(String adbkflag) {
		ADBKFLAG = adbkflag;
	}

	public String getADCOUNT() {
		return ADCOUNT;
	}

	public void setADCOUNT(String adcount) {
		ADCOUNT = adcount;
	}

	public String getADLEVEL() {
		return ADLEVEL;
	}

	public void setADLEVEL(String adlevel) {
		ADLEVEL = adlevel;
	}

	public String getADOPID() {
		return ADOPID;
	}

	public void setADOPID(String adopid) {
		ADOPID = adopid;
	}

	public Long getADREPORTID() {
		return ADREPORTID;
	}

	public void setADREPORTID(Long adreportid) {
		ADREPORTID = adreportid;
	}

	public String getADTXCODE() {
		return ADTXCODE;
	}

	public void setADTXCODE(String adtxcode) {
		ADTXCODE = adtxcode;
	}

	public String getADTXSTATUS() {
		return ADTXSTATUS;
	}

	public void setADTXSTATUS(String adtxstatus) {
		ADTXSTATUS = adtxstatus;
	}

	public String getADUSERTYPE() {
		return ADUSERTYPE;
	}

	public void setADUSERTYPE(String adusertype) {
		ADUSERTYPE = adusertype;
	}

	public String getCMYYYYMM() {
		return CMYYYYMM;
	}

	public void setCMYYYYMM(String cmyyyymm) {
		CMYYYYMM = cmyyyymm;
	}

	public String getLOGINTYPE() {
		return LOGINTYPE;
	}
	
	public void setLOGINTYPE(String logintype) {
		LOGINTYPE = logintype;
	}
}
