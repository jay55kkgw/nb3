package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "ADMMAILCONTENT")
public class ADMMAILCONTENT implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long ADMAILID;

	private String ADSUBJECT = "";

	@Column(nullable = false)
	private String ADMAILCONTENT = ""; 
 
	public String getADMAILCONTENT() {
		return ADMAILCONTENT;
	} 

	public void setADMAILCONTENT(String admailcontent) {
		ADMAILCONTENT = admailcontent;
	}




	public Long getADMAILID() {
		return ADMAILID;
	}




	public void setADMAILID(Long admailid) {
		ADMAILID = admailid;
	}




	public String getADSUBJECT() {
		return ADSUBJECT;
	}




	public void setADSUBJECT(String adsubject) {
		ADSUBJECT = adsubject;
	}




	public String toString() {
		return new ToStringBuilder(this)
				.append("admailid", getADMAILID()).toString();
	}
 
}
