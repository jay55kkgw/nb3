package fstop.orm.po;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "ADMCUSTTELNO")
public class ADMCUSTTELNO implements Serializable {

	@Id
	private String ADMCUSTID;

	private String ADMTELNO;
	
	private String ADMDATE;
	
	private String ADMTIME;

	public String toString() {
		return new ToStringBuilder(this).append("admcustid", getADMCUSTID())
				.toString();
	}

	public String getADMCUSTID() {
		return ADMCUSTID;
	}

	public void setADMCUSTID(String admcustid) {
		ADMCUSTID = admcustid;
	}

	public String getADMTELNO() {
		return ADMTELNO;
	}

	public void setADMTELNO(String admtelno) {
		ADMTELNO = admtelno;
	}

	public String getADMDATE() {
		return ADMDATE;
	}

	public void setADMDATE(String admdate) {
		ADMDATE = admdate;
	}

	public String getADMTIME() {
		return ADMTIME;
	}

	public void setADMTIME(String admtime) {
		ADMTIME = admtime;
	}

}
