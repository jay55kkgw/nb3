package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="fstop.orm.po.TXNFXSCHPAYDATA")
@Table(name = "TXNFXSCHPAYDATA")
public class TXNFXSCHPAYDATA implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7032373023952252161L;

	@Id
	private TXNFXSCHPAYDATA_PK pks;//複合組鍵	
	
	private String ADOPID = "";//操作功能ID
	private String FXWDAC = "";//轉出帳號
	private String FXWDCURR = "";//轉出幣別
	private String FXWDAMT = "";//轉出金額
	private String FXSVBH = "";//轉入行庫代碼
	private String FXSVAC = "";//轉入帳號/繳費代號
	private String FXSVCURR = "";//轉入幣別
	private String FXSVAMT = "";//轉入金額
	private String FXTXMEMO = "";//備註
	private String FXTXMAILS = "";//發送Mail清單
	private String FXTXMAILMEMO = "";//Mail備註
	private String FXTXCODE = "";//交易機制
	private String XMLCA = "";//CA 識別碼
	private String XMLCN = "";//憑證ＣＮ
	private String MAC = "";
	private String FXTXDATE = "";//實際轉帳日期
	private String FXTXTIME = "";//實際轉帳時間
	private String PCSEQ = "";//交易序號
	private String ADTXNO = "";
	private String FXTITAINFO = "";
	private String FXTOTAINFO = "";
	private String FXEFEE = "";
	private String FXTELFEE = "";
	private String FXEXRATE = "";
	private String FXTXSTATUS = "";
	private String FXEXCODE = "";
	private String FXRESEND = "";
	private String FXMSGSEQNO = "";
	private String FXCERT = "";
	private String LASTDATE = "";//最後異動日期
	private String LASTTIME = "";//最後異動時間
	private String FXOURCHG = "";//國外費用
	private String FXEFEECCY="";
	private String MSADDR="";
	
	public TXNFXSCHPAYDATA_PK getPks() {
		return pks;
	}
	public void setPks(TXNFXSCHPAYDATA_PK pks) {
		this.pks = pks;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getFXWDAC() {
		return FXWDAC;
	}
	public void setFXWDAC(String fXWDAC) {
		FXWDAC = fXWDAC;
	}
	public String getFXWDCURR() {
		return FXWDCURR;
	}
	public void setFXWDCURR(String fXWDCURR) {
		FXWDCURR = fXWDCURR;
	}
	public String getFXWDAMT() {
		return FXWDAMT;
	}
	public void setFXWDAMT(String fXWDAMT) {
		FXWDAMT = fXWDAMT;
	}
	public String getFXSVBH() {
		return FXSVBH;
	}
	public void setFXSVBH(String fXSVBH) {
		FXSVBH = fXSVBH;
	}
	public String getFXSVAC() {
		return FXSVAC;
	}
	public void setFXSVAC(String fXSVAC) {
		FXSVAC = fXSVAC;
	}
	public String getFXSVCURR() {
		return FXSVCURR;
	}
	public void setFXSVCURR(String fXSVCURR) {
		FXSVCURR = fXSVCURR;
	}
	public String getFXSVAMT() {
		return FXSVAMT;
	}
	public void setFXSVAMT(String fXSVAMT) {
		FXSVAMT = fXSVAMT;
	}
	public String getFXTXMEMO() {
		return FXTXMEMO;
	}
	public void setFXTXMEMO(String fXTXMEMO) {
		FXTXMEMO = fXTXMEMO;
	}
	public String getFXTXMAILS() {
		return FXTXMAILS;
	}
	public void setFXTXMAILS(String fXTXMAILS) {
		FXTXMAILS = fXTXMAILS;
	}
	public String getFXTXMAILMEMO() {
		return FXTXMAILMEMO;
	}
	public void setFXTXMAILMEMO(String fXTXMAILMEMO) {
		FXTXMAILMEMO = fXTXMAILMEMO;
	}
	public String getFXTXCODE() {
		return FXTXCODE;
	}
	public void setFXTXCODE(String fXTXCODE) {
		FXTXCODE = fXTXCODE;
	}
	public String getXMLCA() {
		return XMLCA;
	}
	public void setXMLCA(String xMLCA) {
		XMLCA = xMLCA;
	}
	public String getXMLCN() {
		return XMLCN;
	}
	public void setXMLCN(String xMLCN) {
		XMLCN = xMLCN;
	}
	public String getMAC() {
		return MAC;
	}
	public void setMAC(String mAC) {
		MAC = mAC;
	}
	public String getFXTXDATE() {
		return FXTXDATE;
	}
	public void setFXTXDATE(String fXTXDATE) {
		FXTXDATE = fXTXDATE;
	}
	public String getFXTXTIME() {
		return FXTXTIME;
	}
	public void setFXTXTIME(String fXTXTIME) {
		FXTXTIME = fXTXTIME;
	}
	public String getPCSEQ() {
		return PCSEQ;
	}
	public void setPCSEQ(String pCSEQ) {
		PCSEQ = pCSEQ;
	}
	public String getADTXNO() {
		return ADTXNO;
	}
	public void setADTXNO(String aDTXNO) {
		ADTXNO = aDTXNO;
	}
	public String getFXTITAINFO() {
		return FXTITAINFO;
	}
	public void setFXTITAINFO(String fXTITAINFO) {
		FXTITAINFO = fXTITAINFO;
	}
	public String getFXTOTAINFO() {
		return FXTOTAINFO;
	}
	public void setFXTOTAINFO(String fXTOTAINFO) {
		FXTOTAINFO = fXTOTAINFO;
	}
	public String getFXEFEE() {
		return FXEFEE;
	}
	public void setFXEFEE(String fXEFEE) {
		FXEFEE = fXEFEE;
	}
	public String getFXTELFEE() {
		return FXTELFEE;
	}
	public void setFXTELFEE(String fXTELFEE) {
		FXTELFEE = fXTELFEE;
	}
	public String getFXEXRATE() {
		return FXEXRATE;
	}
	public void setFXEXRATE(String fXEXRATE) {
		FXEXRATE = fXEXRATE;
	}
	public String getFXTXSTATUS() {
		return FXTXSTATUS;
	}
	public void setFXTXSTATUS(String fXTXSTATUS) {
		FXTXSTATUS = fXTXSTATUS;
	}
	public String getFXEXCODE() {
		return FXEXCODE;
	}
	public void setFXEXCODE(String fXEXCODE) {
		FXEXCODE = fXEXCODE;
	}
	public String getFXRESEND() {
		return FXRESEND;
	}
	public void setFXRESEND(String fXRESEND) {
		FXRESEND = fXRESEND;
	}
	public String getFXMSGSEQNO() {
		return FXMSGSEQNO;
	}
	public void setFXMSGSEQNO(String fXMSGSEQNO) {
		FXMSGSEQNO = fXMSGSEQNO;
	}
	public String getFXCERT() {
		return FXCERT;
	}
	public void setFXCERT(String fXCERT) {
		FXCERT = fXCERT;
	}
	public String getLASTDATE() {
		return LASTDATE;
	}
	public void setLASTDATE(String lASTDATE) {
		LASTDATE = lASTDATE;
	}
	public String getLASTTIME() {
		return LASTTIME;
	}
	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}
	public String getFXOURCHG() {
		return FXOURCHG;
	}
	public void setFXOURCHG(String fXOURCHG) {
		FXOURCHG = fXOURCHG;
	}
	public String getFXEFEECCY() {
		return FXEFEECCY;
	}
	public void setFXEFEECCY(String fXEFEECCY) {
		FXEFEECCY = fXEFEECCY;
	}
	public String getMSADDR() {
		return MSADDR;
	}
	public void setMSADDR(String mSADDR) {
		MSADDR = mSADDR;
	}



}
