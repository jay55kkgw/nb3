package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="fstop.orm.po.TXNTWRECORD")
@Table(name = "TXNTWRECORD")
public class TXNTWRECORD implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 3037018130739966657L;

	@Id
	private String ADTXNO; //交易編號
	// 轉帳即時(或預約發動待處理時)產生, YYYYMMDDhh(PCSEQ)長度15

	private String ADOPID = ""; // 操作功能ID

	private String DPUSERID = ""; //使用者ID

	private String DPTXDATE = ""; //實際轉帳日期

	private String DPTXTIME = ""; //實際轉帳時間

	private String DPWDAC = ""; //轉出帳號

	private String DPSVBH = ""; //轉入行庫代碼

	private String DPSVAC = ""; // 轉入帳號/繳費代號

	private String DPTXAMT = ""; //轉帳金額

	private String DPTXMEMO = ""; // 備註*

	private String DPTXMAILS = ""; //發送 mail 清單*
	
	private String DPTXMAILMEMO = "";  // Mail 備註*

	private Long DPSCHID = 0L; // 預約流水號*
	// 1. 即時交易時此欄位預設為0
	// 2. 預約交易發動時需填入 ref: TxnTwSchPay.DPSCHID

	private String DPEFEE = ""; // 手續費*

	private String PCSEQ = ""; //交易序號*
	
	private String DPTXNO = "";   //跨行序號*

	private String DPTXCODE = "";   //交易機制
	// 0 交易密碼, 1 電子簽章, 2 晶片金融卡, 3 動態密碼OTP(行動網銀MB使用)

	private String DPTITAINFO = "";  //交易上行資訊
//	private Long DPTITAINFO = 0L;

	private String DPTOTAINFO = ""; //交易下上行資訊*

	private String DPREMAIL = ""; //重發 mail 次數*

	private String DPTXSTATUS = ""; //交易執行狀態 0 成功, 1 失敗, 2 處理中

	private String DPEXCODE = "";  // 交易錯誤代碼*

//	private String DPRETXNO = "";
	
//	private String DPRETXSTATUS = "";

	private String LASTDATE = "";  // 最後異動日期

	private String LASTTIME = "";  //最後異動時間
	
//	private String DPSCHNO = "";
	private String LOGINTYPE = "";  // NB 登入或 MB 登入
	// (NB 預設) (MB 行動) (QRCODE QR)

	public String getADTXNO() {
		return ADTXNO;
	}

	public void setADTXNO(String aDTXNO) {
		ADTXNO = aDTXNO;
	}

	public String getADOPID() {
		return ADOPID;
	}

	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}

	public String getDPUSERID() {
		return DPUSERID;
	}

	public void setDPUSERID(String dPUSERID) {
		DPUSERID = dPUSERID;
	}

	public String getDPTXDATE() {
		return DPTXDATE;
	}

	public void setDPTXDATE(String dPTXDATE) {
		DPTXDATE = dPTXDATE;
	}

	public String getDPTXTIME() {
		return DPTXTIME;
	}

	public void setDPTXTIME(String dPTXTIME) {
		DPTXTIME = dPTXTIME;
	}

	public String getDPWDAC() {
		return DPWDAC;
	}

	public void setDPWDAC(String dPWDAC) {
		DPWDAC = dPWDAC;
	}

	public String getDPSVBH() {
		return DPSVBH;
	}

	public void setDPSVBH(String dPSVBH) {
		DPSVBH = dPSVBH;
	}

	public String getDPSVAC() {
		return DPSVAC;
	}

	public void setDPSVAC(String dPSVAC) {
		DPSVAC = dPSVAC;
	}

	public String getDPTXAMT() {
		return DPTXAMT;
	}

	public void setDPTXAMT(String dPTXAMT) {
		DPTXAMT = dPTXAMT;
	}

	public String getDPTXMEMO() {
		return DPTXMEMO;
	}

	public void setDPTXMEMO(String dPTXMEMO) {
		DPTXMEMO = dPTXMEMO;
	}

	public String getDPTXMAILS() {
		return DPTXMAILS;
	}

	public void setDPTXMAILS(String dPTXMAILS) {
		DPTXMAILS = dPTXMAILS;
	}

	public String getDPTXMAILMEMO() {
		return DPTXMAILMEMO;
	}

	public void setDPTXMAILMEMO(String dPTXMAILMEMO) {
		DPTXMAILMEMO = dPTXMAILMEMO;
	}

	public Long getDPSCHID() {
		return DPSCHID;
	}

	public void setDPSCHID(Long dPSCHID) {
		DPSCHID = dPSCHID;
	}

	public String getDPEFEE() {
		return DPEFEE;
	}

	public void setDPEFEE(String dPEFEE) {
		DPEFEE = dPEFEE;
	}

	public String getPCSEQ() {
		return PCSEQ;
	}

	public void setPCSEQ(String pCSEQ) {
		PCSEQ = pCSEQ;
	}

	public String getDPTXNO() {
		return DPTXNO;
	}

	public void setDPTXNO(String dPTXNO) {
		DPTXNO = dPTXNO;
	}

	public String getDPTXCODE() {
		return DPTXCODE;
	}

	public void setDPTXCODE(String dPTXCODE) {
		DPTXCODE = dPTXCODE;
	}

	public String getDPTITAINFO() {
		return DPTITAINFO;
	}

	public void setDPTITAINFO(String dPTITAINFO) {
		DPTITAINFO = dPTITAINFO;
	}

	public String getDPTOTAINFO() {
		return DPTOTAINFO;
	}

	public void setDPTOTAINFO(String dPTOTAINFO) {
		DPTOTAINFO = dPTOTAINFO;
	}

	public String getDPREMAIL() {
		return DPREMAIL;
	}

	public void setDPREMAIL(String dPREMAIL) {
		DPREMAIL = dPREMAIL;
	}

	public String getDPTXSTATUS() {
		return DPTXSTATUS;
	}

	public void setDPTXSTATUS(String dPTXSTATUS) {
		DPTXSTATUS = dPTXSTATUS;
	}

	public String getDPEXCODE() {
		return DPEXCODE;
	}

	public void setDPEXCODE(String dPEXCODE) {
		DPEXCODE = dPEXCODE;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lASTDATE) {
		LASTDATE = lASTDATE;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}

	public String getLOGINTYPE() {
		return LOGINTYPE;
	}

	public void setLOGINTYPE(String lOGINTYPE) {
		LOGINTYPE = lOGINTYPE;
	}
	
	
}
