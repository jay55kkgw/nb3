package fstop.orm.po;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Embeddable
public class QUICKLOGINMAPPING_PK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7805853347354812595L;
	private String CUSIDN;
	private String IDGATEID;
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getIDGATEID() {
		return IDGATEID;
	}
	public void setIDGATEID(String iDGATEID) {
		IDGATEID = iDGATEID;
	}
	
	
	
}
