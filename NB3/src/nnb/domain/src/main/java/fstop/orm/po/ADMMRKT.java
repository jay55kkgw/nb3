package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "ADMMRKT")
public class ADMMRKT implements Serializable{

	private Integer MTID; 		// 行銷ID	
	private String MTUSERID; 	// 使用者代號	
	private String MTUSERNAME; 	// 使用者名稱	
	private String MTTXDATE; 	// 交易日期	
	private String MTTXTIME; 	// 交易時間	
	private String MTSDATE; 	// 行銷上版起日	
	private String MTSTIME; 	// 行銷上版起時	
	private String MTEDATE;		// 行銷上版迄日	
	private String MTETIME; 	// 行銷上版迄時	
	private String MTADTYPE; 	// 行銷類別	
	private String MTWEIGHT; 	// 權重	
	private String MTADHLK;		// 行銷標題	
	private String MTADCON; 	// 行銷內容-文字敘述	
	private String MTPICTYPE; 	// 圖片位置	
	private String MTRUNSHOW; 	// 跑馬燈顯示
	private String MTBULLETIN; 	// 強迫公告	
	private String MTPICADD; 	// 行銷圖片路徑	
	private String MTPICDATA; 	// 行銷圖片	
	private String MTADSTAT; 	// 行銷狀態	
	private String MTCHRES; 	// 審核結果	
	private String MTSUPNO; 	// 審核主管代號	
	private String MTSUPNAME;	// 審核主管名	
	private String MTSUPDATE;	// 審核日期	
	private String MTSUPTIME;	// 審核時間	
	private String MTBTNNAME;	// 按鈕名稱	
	private String MTADDRESS;	// 超連結網址	
	private String MTMARQUEEPICADD; // 跑馬燈圖片	
	private String MTMARQUEEPIC;// 跑馬燈圖片
	private String MTNF;		// 是否要推播
	private String MTNFSTAT;	// 推播狀態
	private String MTNFDATE;	// 推播日期
	private String MTNFTIME;	// 推播時間
	private String MTCF;		// 廣告訊息
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getMTID() {
		return MTID;
	}
	public void setMTID(Integer mtid) {
		MTID = mtid;
	}
	public String getMTUSERID() {
		return MTUSERID;
	}
	public void setMTUSERID(String mtuserid) {
		MTUSERID = mtuserid;
	}
	public String getMTUSERNAME() {
		return MTUSERNAME;
	}
	public void setMTUSERNAME(String mtusername) {
		MTUSERNAME = mtusername;
	}
	public String getMTTXDATE() {
		return MTTXDATE;
	}
	public void setMTTXDATE(String mttxdate) {
		MTTXDATE = mttxdate;
	}
	public String getMTTXTIME() {
		return MTTXTIME;
	}
	public void setMTTXTIME(String mttxtime) {
		MTTXTIME = mttxtime;
	}
	public String getMTSDATE() {
		return MTSDATE;
	}
	public void setMTSDATE(String mtsdate) {
		MTSDATE = mtsdate;
	}
	public String getMTSTIME() {
		return MTSTIME;
	}
	public void setMTSTIME(String mtstime) {
		MTSTIME = mtstime;
	}
	public String getMTEDATE() {
		return MTEDATE;
	}
	public void setMTEDATE(String mtedate) {
		MTEDATE = mtedate;
	}
	public String getMTETIME() {
		return MTETIME;
	}
	public void setMTETIME(String mtetime) {
		MTETIME = mtetime;
	}
	public String getMTADTYPE() {
		return MTADTYPE;
	}
	public void setMTADTYPE(String mtadtype) {
		MTADTYPE = mtadtype;
	}
	public String getMTWEIGHT() {
		return MTWEIGHT;
	}
	public void setMTWEIGHT(String mtweight) {
		MTWEIGHT = mtweight;
	}
	public String getMTADHLK() {
		return MTADHLK;
	}
	public void setMTADHLK(String mtadhlk) {
		MTADHLK = mtadhlk;
	}
	public String getMTADCON() {
		return MTADCON;
	}
	public void setMTADCON(String mtadcon) {
		MTADCON = mtadcon;
	}
	public String getMTPICTYPE() {
		return MTPICTYPE;
	}
	public void setMTPICTYPE(String mtpictype) {
		MTPICTYPE = mtpictype;
	}
	public String getMTRUNSHOW() {
		return MTRUNSHOW;
	}
	public void setMTRUNSHOW(String mtrunshow) {
		MTRUNSHOW = mtrunshow;
	}
	public String getMTBULLETIN() {
		return MTBULLETIN;
	}
	public void setMTBULLETIN(String mtbulletin) {
		MTBULLETIN = mtbulletin;
	}
	public String getMTPICADD() {
		return MTPICADD;
	}
	public void setMTPICADD(String mtpicadd) {
		MTPICADD = mtpicadd;
	}
	public String getMTPICDATA() {
		return MTPICDATA;
	}
	public void setMTPICDATA(String mtpicdata) {
		MTPICDATA = mtpicdata;
	}
	public String getMTADSTAT() {
		return MTADSTAT;
	}
	public void setMTADSTAT(String mtadstat) {
		MTADSTAT = mtadstat;
	}
	public String getMTCHRES() {
		return MTCHRES;
	}
	public void setMTCHRES(String mtchres) {
		MTCHRES = mtchres;
	}
	public String getMTSUPNO() {
		return MTSUPNO;
	}
	public void setMTSUPNO(String mtsupno) {
		MTSUPNO = mtsupno;
	}
	public String getMTSUPNAME() {
		return MTSUPNAME;
	}
	public void setMTSUPNAME(String mtsupname) {
		MTSUPNAME = mtsupname;
	}
	public String getMTSUPDATE() {
		return MTSUPDATE;
	}
	public void setMTSUPDATE(String mtsupdate) {
		MTSUPDATE = mtsupdate;
	}
	public String getMTSUPTIME() {
		return MTSUPTIME;
	}
	public void setMTSUPTIME(String mtsuptime) {
		MTSUPTIME = mtsuptime;
	}

	public String getMTBTNNAME() {
		return this.MTBTNNAME;
	}
	public void setMTBTNNAME(String mtbtnname) {
		this.MTBTNNAME = mtbtnname;
	}
	
	public String getMTADDRESS() {
		return this.MTADDRESS;
	}
	public void setMTADDRESS(String mtaddress) {
		this.MTADDRESS = mtaddress;
	}
	
	public String getMTMARQUEEPICADD() {
		return MTMARQUEEPICADD;
	}
	public void setMTMARQUEEPICADD(String mtmarqueepicadd) {
		MTMARQUEEPICADD = mtmarqueepicadd;
	}
	public String getMTMARQUEEPIC() {
		return this.MTMARQUEEPIC;
	}
	public void setMTMARQUEEPIC(String mtmarqueepic) {
		this.MTMARQUEEPIC = mtmarqueepic;
	}
	public String getMTNF() {
		return this.MTNF;
	}
	public void setMTNF(String MTNF) {
		this.MTNF = MTNF;
	}
	public String getMTNFSTAT() {
		return this.MTNFSTAT;
	}
	public void setMTNFSTAT(String MTNFSTAT) {
		this.MTNFSTAT = MTNFSTAT;
	}
	public String getMTNFDATE() {
		return this.MTNFDATE;
	}
	public void setMTNFDATE(String MTNFDATE) {
		this.MTNFDATE = MTNFDATE;
	}
	public String getMTNFTIME() {
		return this.MTNFTIME;
	}
	public void setMTNFTIME(String MTNFTIME) {
		this.MTNFTIME = MTNFTIME;
	}
	public String getMTCF() {
		return MTCF;
	}
	public void setMTCF(String mtcf) {
		MTCF = mtcf;
	}	
}
