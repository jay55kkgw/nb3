package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="fstop.orm.po.OLD_TXNTWSCHEDULE")
@Table(name = "TXNTWSCHEDULE")
public class OLD_TXNTWSCHEDULE implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -1816042250223539804L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long DPSCHID;

	private String ADOPID = "";
	private String DPUSERID = "";
	private String DPPERMTDATE = "";
	private String DPFDATE = "";
	private String DPTDATE = "";
	private String DPNEXTDATE = "";
	private String DPWDAC = "";
	private String DPSVBH = "";
	private String DPSVAC = "";
	private String DPTXAMT = "";
	private String DPTXMEMO = "";
	private String DPTXMAILS = "";
	private String DPTXMAILMEMO = "";
	private String DPTXCODE = "";
	private Long DPTXINFO = 0l;
	private String DPSDATE = "";
	private String DPSTIME = "";
	private String DPTXSTATUS = "";
	private String XMLCA = "";
	private String XMLCN = "";
	private String MAC = "";
	private String LASTDATE = "";
	private String LASTTIME = "";
	private String DPSCHNO = "";
	private String LOGINTYPE = ""; //NB:網銀 ， MB:行動 ，預設為NB
	public Long getDPSCHID() {
		return DPSCHID;
	}
	public void setDPSCHID(Long dPSCHID) {
		DPSCHID = dPSCHID;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getDPUSERID() {
		return DPUSERID;
	}
	public void setDPUSERID(String dPUSERID) {
		DPUSERID = dPUSERID;
	}
	public String getDPPERMTDATE() {
		return DPPERMTDATE;
	}
	public void setDPPERMTDATE(String dPPERMTDATE) {
		DPPERMTDATE = dPPERMTDATE;
	}
	public String getDPFDATE() {
		return DPFDATE;
	}
	public void setDPFDATE(String dPFDATE) {
		DPFDATE = dPFDATE;
	}
	public String getDPTDATE() {
		return DPTDATE;
	}
	public void setDPTDATE(String dPTDATE) {
		DPTDATE = dPTDATE;
	}
	public String getDPNEXTDATE() {
		return DPNEXTDATE;
	}
	public void setDPNEXTDATE(String dPNEXTDATE) {
		DPNEXTDATE = dPNEXTDATE;
	}
	public String getDPWDAC() {
		return DPWDAC;
	}
	public void setDPWDAC(String dPWDAC) {
		DPWDAC = dPWDAC;
	}
	public String getDPSVBH() {
		return DPSVBH;
	}
	public void setDPSVBH(String dPSVBH) {
		DPSVBH = dPSVBH;
	}
	public String getDPSVAC() {
		return DPSVAC;
	}
	public void setDPSVAC(String dPSVAC) {
		DPSVAC = dPSVAC;
	}
	public String getDPTXAMT() {
		return DPTXAMT;
	}
	public void setDPTXAMT(String dPTXAMT) {
		DPTXAMT = dPTXAMT;
	}
	public String getDPTXMEMO() {
		return DPTXMEMO;
	}
	public void setDPTXMEMO(String dPTXMEMO) {
		DPTXMEMO = dPTXMEMO;
	}
	public String getDPTXMAILS() {
		return DPTXMAILS;
	}
	public void setDPTXMAILS(String dPTXMAILS) {
		DPTXMAILS = dPTXMAILS;
	}
	public String getDPTXMAILMEMO() {
		return DPTXMAILMEMO;
	}
	public void setDPTXMAILMEMO(String dPTXMAILMEMO) {
		DPTXMAILMEMO = dPTXMAILMEMO;
	}
	public String getDPTXCODE() {
		return DPTXCODE;
	}
	public void setDPTXCODE(String dPTXCODE) {
		DPTXCODE = dPTXCODE;
	}
	public Long getDPTXINFO() {
		return DPTXINFO;
	}
	public void setDPTXINFO(Long dPTXINFO) {
		DPTXINFO = dPTXINFO;
	}
	public String getDPSDATE() {
		return DPSDATE;
	}
	public void setDPSDATE(String dPSDATE) {
		DPSDATE = dPSDATE;
	}
	public String getDPSTIME() {
		return DPSTIME;
	}
	public void setDPSTIME(String dPSTIME) {
		DPSTIME = dPSTIME;
	}
	public String getDPTXSTATUS() {
		return DPTXSTATUS;
	}
	public void setDPTXSTATUS(String dPTXSTATUS) {
		DPTXSTATUS = dPTXSTATUS;
	}
	public String getXMLCA() {
		return XMLCA;
	}
	public void setXMLCA(String xMLCA) {
		XMLCA = xMLCA;
	}
	public String getXMLCN() {
		return XMLCN;
	}
	public void setXMLCN(String xMLCN) {
		XMLCN = xMLCN;
	}
	public String getMAC() {
		return MAC;
	}
	public void setMAC(String mAC) {
		MAC = mAC;
	}
	public String getLASTDATE() {
		return LASTDATE;
	}
	public void setLASTDATE(String lASTDATE) {
		LASTDATE = lASTDATE;
	}
	public String getLASTTIME() {
		return LASTTIME;
	}
	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}
	public String getDPSCHNO() {
		return DPSCHNO;
	}
	public void setDPSCHNO(String dPSCHNO) {
		DPSCHNO = dPSCHNO;
	}
	public String getLOGINTYPE() {
		return LOGINTYPE;
	}
	public void setLOGINTYPE(String lOGINTYPE) {
		LOGINTYPE = lOGINTYPE;
	}
	
	
}
