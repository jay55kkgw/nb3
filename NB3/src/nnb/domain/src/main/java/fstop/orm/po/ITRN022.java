package fstop.orm.po;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "ITRN022")
public class ITRN022 implements Serializable {

	@Id
	private String RECNO;

	private String HEADER;

	private String SEQ;

	private String DATE;

	private String TIME;

	private String COUNT;
	
	private String COLOR;
	
	private String CRYNAME;
	
	private String CRY;
	
	private String ITR1;
	
	private String ITR2;
	
	private String ITR3;
	
	private String ITR4;
	
	private String ITR5;
	
	private String ITR6;
	
	private String ITR7;
	
	private String ITR8;
	
	private String ITR9;
	
	private String ITR10;
	
	private String ITR11;
	
	private String ITR12;
	
	private String ITR13;
	
	private String ITR14;
	
	private String ITR15;
	
	private String ITR16;
	
	private String ITR17;
	
	private String ITR18;
	
	private String ITR19;
	
	private String ITR20;
	
	private String FILL;
	
	public String toString() {
		return new ToStringBuilder(this).append("recno", getRECNO())
				.toString();
	}

	public String getRECNO() {
		return RECNO;
	}

	public void setRECNO(String recno) {
		RECNO = recno;
	}

	public String getHEADER() {
		return HEADER;
	}

	public void setHEADER(String header) {
		HEADER = header;
	}

	public String getSEQ() {
		return SEQ;
	}

	public void setSEQ(String seq) {
		SEQ = seq;
	}

	public String getDATE() {
		return DATE;
	}

	public void setDATE(String date) {
		DATE = date;
	}

	public String getTIME() {
		return TIME;
	}

	public void setTIME(String time) {
		TIME = time;
	}

	public String getCOUNT() {
		return COUNT;
	}

	public void setCOUNT(String count) {
		COUNT = count;
	}

	public String getCOLOR() {
		return COLOR;
	}

	public void setCOLOR(String color) {
		COLOR = color;
	}

	public String getCRYNAME() {
		return CRYNAME;
	}

	public void setCRYNAME(String cryname) {
		CRYNAME = cryname;
	}

	public String getCRY() {
		return CRY;
	}

	public void setCRY(String cry) {
		CRY = cry;
	}

	public String getITR1() {
		return ITR1;
	}

	public void setITR1(String itr1) {
		ITR1 = itr1;
	}

	public String getITR2() {
		return ITR2;
	}

	public void setITR2(String itr2) {
		ITR2 = itr2;
	}

	public String getITR3() {
		return ITR3;
	}

	public void setITR3(String itr3) {
		ITR3 = itr3;
	}

	public String getITR4() {
		return ITR4;
	}

	public void setITR4(String itr4) {
		ITR4 = itr4;
	}

	public String getITR5() {
		return ITR5;
	}

	public void setITR5(String itr5) {
		ITR5 = itr5;
	}

	public String getITR6() {
		return ITR6;
	}

	public void setITR6(String itr6) {
		ITR6 = itr6;
	}

	public String getITR7() {
		return ITR7;
	}

	public void setITR7(String itr7) {
		ITR7 = itr7;
	}

	public String getITR8() {
		return ITR8;
	}

	public void setITR8(String itr8) {
		ITR8 = itr8;
	}

	public String getITR9() {
		return ITR9;
	}

	public void setITR9(String itr9) {
		ITR9 = itr9;
	}

	public String getITR10() {
		return ITR10;
	}

	public void setITR10(String itr10) {
		ITR10 = itr10;
	}

	public String getITR11() {
		return ITR11;
	}

	public void setITR11(String itr11) {
		ITR11 = itr11;
	}

	public String getITR12() {
		return ITR12;
	}

	public void setITR12(String itr12) {
		ITR12 = itr12;
	}

	public String getITR13() {
		return ITR13;
	}

	public void setITR13(String itr13) {
		ITR13 = itr13;
	}

	public String getITR14() {
		return ITR14;
	}

	public void setITR14(String itr14) {
		ITR14 = itr14;
	}

	public String getITR15() {
		return ITR15;
	}

	public void setITR15(String itr15) {
		ITR15 = itr15;
	}

	public String getITR16() {
		return ITR16;
	}

	public void setITR16(String itr16) {
		ITR16 = itr16;
	}

	public String getITR17() {
		return ITR17;
	}

	public void setITR17(String itr17) {
		ITR17 = itr17;
	}

	public String getITR18() {
		return ITR18;
	}

	public void setITR18(String itr18) {
		ITR18 = itr18;
	}

	public String getITR19() {
		return ITR19;
	}

	public void setITR19(String itr19) {
		ITR19 = itr19;
	}

	public String getITR20() {
		return ITR20;
	}

	public void setITR20(String itr20) {
		ITR20 = itr20;
	}

	public String getFILL() {
		return FILL;
	}

	public void setFILL(String fill) {
		FILL = fill;
	}

}
