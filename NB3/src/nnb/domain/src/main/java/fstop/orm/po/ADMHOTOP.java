package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "ADMHOTOP")
public class ADMHOTOP implements Serializable {

	@Id
	private ADMHOTOP_PK id;//複合組鍵
	
	private String LASTDT;

	public ADMHOTOP_PK getId() {
		return id;
	}

	public void setId(ADMHOTOP_PK id) {
		this.id = id;
	}

	public String getLASTDT() {
		return LASTDT;
	}

	public void setLASTDT(String lASTDT) {
		LASTDT = lASTDT;
	}

}
