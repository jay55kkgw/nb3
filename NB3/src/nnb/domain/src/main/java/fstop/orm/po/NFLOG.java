package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "NFLOG")
public class NFLOG implements Serializable {
	
	//一定要有 @Id，否則啟動伺服器時會發生錯誤
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String NFLOGID;

	private String NFUSERID;

	private String NFACCOUNT;

	private String NFPHONETOKEN;

	private String NFPHONETYPE;
	
	private String NFTOPIC;
	
	private String NFMESSAGE;
	
	private String NFSENDTIME;
	
	private String NFSENDTYPE;
	
	private String NFSTATUS;	

	public String getNFACCOUNT() {
		return NFACCOUNT;
	}

	public void setNFACCOUNT(String nfaccount) {
		NFACCOUNT = nfaccount;
	}

	public String getNFLOGID() {
		return NFLOGID;
	}

	public void setNFLOGID(String nflogid) {
		NFLOGID = nflogid;
	}

	public String getNFPHONETOKEN() {
		return NFPHONETOKEN;
	}

	public void setNFPHONETOKEN(String nfphonetoken) {
		NFPHONETOKEN = nfphonetoken;
	}

	public String getNFPHONETYPE() {
		return NFPHONETYPE;
	}

	public void setNFPHONETYPE(String nfphonetype) {
		NFPHONETYPE = nfphonetype;
	}

	public String getNFSENDTIME() {
		return NFSENDTIME;
	}

	public void setNFSENDTIME(String nfsendtime) {
		NFSENDTIME = nfsendtime;
	}

	public String getNFSENDTYPE() {
		return NFSENDTYPE;
	}

	public void setNFSENDTYPE(String nfsendtype) {
		NFSENDTYPE = nfsendtype;
	}

	public String getNFSTATUS() {
		return NFSTATUS;
	}

	public void setNFSTATUS(String nfstatus) {
		NFSTATUS = nfstatus;
	}

	public String getNFTOPIC() {
		return NFTOPIC;
	}

	public void setNFTOPIC(String nftopic) {
		NFTOPIC = nftopic;
	}

	public String getNFUSERID() {
		return NFUSERID;
	}

	public void setNFUSERID(String nfuserid) {
		NFUSERID = nfuserid;
	}

	public String getNFMESSAGE() {
		return NFMESSAGE;
	}

	public void setNFMESSAGE(String nfmessage) {
		NFMESSAGE = nfmessage;
	}	
}
