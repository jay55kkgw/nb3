package fstop.orm.po;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity(name="fstop.orm.po.TXNTRACCSET")
@Table(name = "TXNTRACCSET")
public class TXNTRACCSET implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6671243027536060479L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer DPACCSETID;

	private String DPUSERID;

	private String DPGONAME;

	private String DPTRACNO; 

	private String DPTRDACNO;

	private String LASTDATE;

	private String LASTTIME;

	private String DPTRIBANK;

	public String toString() {
		return new ToStringBuilder(this).append("dpaccsetid", getDPACCSETID())
				.toString();
	}

	public String getDPTRIBANK() {
		return DPTRIBANK;
	}

	public void setDPTRIBANK(String admbank) {
		DPTRIBANK = admbank;
	}

	public Integer getDPACCSETID() {
		return DPACCSETID;
	}

	public void setDPACCSETID(Integer dpaccsetid) {
		DPACCSETID = dpaccsetid;
	}

	public String getDPGONAME() {
		return DPGONAME;
	}

	public void setDPGONAME(String dpgoname) {
		DPGONAME = dpgoname;
	}

	public String getDPTRACNO() {
		return DPTRACNO;
	}

	public void setDPTRACNO(String dptracno) {
		DPTRACNO = dptracno;
	}

	public String getDPTRDACNO() {
		return DPTRDACNO;
	}

	public void setDPTRDACNO(String dptrdacno) {
		DPTRDACNO = dptrdacno;
	}

	public String getDPUSERID() {
		return DPUSERID;
	}

	public void setDPUSERID(String dpuserid) {
		DPUSERID = dpuserid;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lastdate) {
		LASTDATE = lastdate;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lasttime) {
		LASTTIME = lasttime;
	}

}
