package fstop.ws;

public interface WsFxRemitMenu{

	public CmFxRemitOut getRemitMenuL(CmRemitLIn params);
	
	public CmFxRemitOut getRemitMenuM(CmRemitMIn params);
	
	public CmFxRemitOut getRemitMenuS(CmRemitSIn params);
	
	public CmMyRemitOut getMyRemitMenu(CmMyRemitIn params);
	
	public CmEditRemitOut editMyRemitMenu(CmEditRemitIn params);
	
	public WsFxRemitTypeOut getRemitType(WsFxRemitTypeIn params);
}