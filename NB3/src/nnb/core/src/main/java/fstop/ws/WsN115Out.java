package fstop.ws;

public class WsN115Out {
	
	private String _MsgCode;
	
	private String _MsgName;
	
	private String _CMQTIME;
	
	private String _ACN;
	
	private String _TOTBAL;
	
	private String _AVLBAL;
	
	private String _SNTCNT;
	
	private String _SNTAMT;
	
	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String MsgCode) {
		this._MsgCode = MsgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String MsgName) {
		this._MsgName = MsgName;
	}

	public String getCMQTIME() {
		return _CMQTIME;
	}

	public void setCMQTIME(String CMQTIME) {
		this._CMQTIME = CMQTIME;
	}

	public String getACN() {
		return _ACN;
	}

	public void setACN(String ACN) {
		this._ACN = ACN;
	}
	
	public String getTOTBAL() {
		return _TOTBAL;
	}

	public void setTOTBAL(String TOTBAL) {
		this._TOTBAL = TOTBAL;
	}
	
	public String getAVLBAL() {
		return _AVLBAL;
	}

	public void setAVLBAL(String AVLBAL) {
		this._AVLBAL = AVLBAL;
	}		
	
	public String getSNTCNT() {
		return _SNTCNT;
	}

	public void setSNTCNT(String SNTCNT) {
		this._SNTCNT = SNTCNT;
	}
	
	public String getSNTAMT() {
		return _SNTAMT;
	}

	public void setSNTAMT(String SNTAMT) {
		this._SNTAMT = SNTAMT;
	}
}
