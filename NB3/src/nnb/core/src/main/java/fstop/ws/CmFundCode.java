package fstop.ws;

public class CmFundCode {

	private String _SessionId;
	
	private String _N950PASSWORD;
	
	private String _QryData;
	
	private String _InputData;
	
	private String _InputData1;
	
	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String SessionId) {
		this._SessionId = SessionId;
	}

	public String getN950PASSWORD() {
		return _N950PASSWORD;
	}

	public void setN950PASSWORD(String N950PASSWORD) {
		this._N950PASSWORD = N950PASSWORD;
	}
	
	public String getQryData(){
		return _QryData;
	}
	
	public void setQryData(String EBIDIC_HP){
		this._QryData = EBIDIC_HP;
	}

	public String getInputData() {
		return _InputData;
	}

	public void setInputData(String inputData) {
		this._InputData = inputData;
	}

	public String getInputData1() {
		return _InputData1;
	}

	public void setInputData1(String inputData1) {
		this._InputData1 = inputData1;
	}
	
}
