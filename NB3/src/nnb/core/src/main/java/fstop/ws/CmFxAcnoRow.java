package fstop.ws;

public class CmFxAcnoRow {
	
	private String _ACN;//帳號

	private String _BNKCOD;//銀行代碼

	private String _AGREE;//約定否 0：非約定 1：約定

	private String _VALUE;//帳號值

	private String _TEXT;//常用帳號說明
	
	private String _DPGONAME;
	
	public String getACN() {
		return _ACN;
	}
	public void setACN(String acn) {
		this._ACN = acn;
	}

	public String getBNKCOD() {
		return _BNKCOD;
	}
	public void setBNKCOD(String bnkcod) {
		this._BNKCOD = bnkcod;
	}

	public String getAGREE() {
		return _AGREE;
	}
	public void setAGREE(String agree) {
		this._AGREE = agree;
	}

	public String getVALUE() {
		return _VALUE;
	}
	public void setVALUE(String value) {
		this._VALUE = value;
	}

	public String getTEXT() {
		return _TEXT;
	}
	public void setTEXT(String text) {
		this._TEXT = text;
	}
	public String getDPGONAME() {
		return _DPGONAME;
	}
	public void setDPGONAME(String DPGONAME) {
		this._DPGONAME = DPGONAME;
	}

	
}
