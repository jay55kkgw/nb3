package fstop.ws;

public interface WsN520{
	
	public CmList getAcnoList(CmUser params);
	
	public WsN520Out action(WsN520In params);	
}