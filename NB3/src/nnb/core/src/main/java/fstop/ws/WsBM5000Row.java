package fstop.ws;

public class WsBM5000Row {

	private String _ADTYPE;

	private String _STTYPE;

	private String _STADHLK;

	private String _STADCON;

	private String _STADNOTE;

	private String _STADINFO;

	private String _STPICDATA;
	
	private String _STTEL;

	public String getADTYPE() {
		return _ADTYPE;
	}

	public void setADTYPE(String ADTYPE) {
		this._ADTYPE = ADTYPE;
	}

	public String getSTTYPE() {
		return _STTYPE;
	}

	public void setSTTYPE(String STTYPE) {
		this._STTYPE = STTYPE;
	}

	public String getSTADHLK() {
		return _STADHLK;
	}

	public void setSTADHLK(String STADHLK) {
		this._STADHLK = STADHLK;
	}

	public String getSTADCON() {
		return _STADCON;
	}

	public void setSTADCON(String STADCON) {
		this._STADCON = STADCON;
	}

	public String getSTADNOTE() {
		return _STADNOTE;
	}

	public void setSTADNOTE(String STADNOTE) {
		this._STADNOTE = STADNOTE;
	}

	public String getSTADINFO() {
		return _STADINFO;
	}

	public void setSTADINFO(String STADINFO) {
		this._STADINFO = STADINFO;
	}

	public String getSTPICDATA() {
		return _STPICDATA;
	}

	public void setSTPICDATA(String STPICDATA) {
		this._STPICDATA = STPICDATA;
	}

	public String getSTTEL() {
		return _STTEL;
	}

	public void setSTTEL(String STTEL) {
		this._STTEL = STTEL;
	}
}
