package fstop.ws;

public class WsN922TwoOut {

	private String _MsgCode;	//訊息代碼
	private String _MsgName;	//訊息名稱
	private String _APLBRH;		//簽約行
	private String _CUTTYPE;	//客戶性質別	
	private String _ACN1;		//新台幣扣帳帳號
	private String _ACN2;		//外幣扣帳帳號
	private String _NAME;		//姓名
	private String _FDINVTYPE;	//投資屬性
	private String _GETLTD;		//投資屬性資料日期
	private String _ACN3;		//新台幣入帳帳號
	private String _ACN4;		//外幣入帳帳號
	private String _ICCOD;		//是否約定信用卡
	private String _DEGREE;		//學歷
	private String _CAREER;		//職業
	private String _SALARY;		//年收入
	private String _BRTHDY;		//出生日期
	private String _MARK1;		//重大傷病註記
	private String _RISK7;		//專業投資人屬性
	private String _MARK3;		//一年內信託交易>=5筆註記

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgcode) {
		this._MsgCode = msgcode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgname) {
		this._MsgName = msgname;
	}

	public String getAPLBRH() {
		return _APLBRH;
	}

	public void setAPLBRH(String aplbrh) {
		this._APLBRH = aplbrh;
	}

	public String getCUTTYPE() {
		return _CUTTYPE;
	}

	public void setCUTTYPE(String cuttype) {
		this._CUTTYPE = cuttype;
	}

	public String getACN1() {
		return _ACN1;
	}

	public void setACN1(String acn1) {
		this._ACN1 = acn1;
	}

	public String getACN2() {
		return _ACN2;
	}

	public void setACN2(String acn2) {
		this._ACN2 = acn2;
	}

	public String getNAME() {
		return _NAME;
	}

	public void setNAME(String name) {
		this._NAME = name;
	}

	public String getFDINVTYPE() {
		return _FDINVTYPE;
	}

	public void setFDINVTYPE(String fdinvtype) {
		this._FDINVTYPE = fdinvtype;
	}

	public String getGETLTD() {
		return _GETLTD;
	}

	public void setGETLTD(String getltd) {
		this._GETLTD = getltd;
	}

	public String getACN3() {
		return _ACN3;
	}

	public void setACN3(String acn3) {
		this._ACN3 = acn3;
	}

	public String getACN4() {
		return _ACN4;
	}

	public void setACN4(String acn4) {
		this._ACN4 = acn4;
	}

	public String getICCOD() {
		return _ICCOD;
	}

	public void setICCOD(String iccod) {
		this._ICCOD = iccod;
	}

	public String getDEGREE() {
		return _DEGREE;
	}

	public void setDEGREE(String DEGREE) {
		this._DEGREE = DEGREE;
	}
	
	public String getCAREER() {
		return _CAREER;
	}

	public void setCAREER(String CAREER) {
		this._CAREER = CAREER;
	}
	
	public String getSALARY() {
		return _SALARY;
	}

	public void setSALARY(String SALARY) {
		this._SALARY = SALARY;
	}
	
	public String getBRTHDY() {
		return _BRTHDY;
	}

	public void setBRTHDY(String BRTHDY) {
		this._BRTHDY = BRTHDY;
	}
	
	public String getMARK1() {
		return _MARK1;
	}

	public void setMARK1(String MARK1) {
		this._MARK1 = MARK1;
	}
	
	public String getRISK7() {
		return _RISK7;
	}

	public void setRISK7(String RISK7) {
		this._RISK7 = RISK7;
	}
	
	public String getMARK3() {
		return _MARK3;
	}

	public void setMARK3(String MARK3) {
		this._MARK3 = MARK3;
	}
}
