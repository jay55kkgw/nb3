package fstop.ws;

public class WsN070AOut {
	
	
	private String _MsgCode;//	訊息代碼
	
	private String _MsgName;//	訊息名稱
	
	private String _CMTXIME;//	交易時間
	
	private String _OUTACN;//	轉出帳號
	
	private String _INTSACN;//	銷帳編號
	
	private String _AMOUNT;//	繳款金額
	
	private String _O_TOTBAL;//	轉出帳號帳上餘額
	
	private String _O_AVLBAL;//	轉出帳號可用餘額
	
	//private String _CMTRMEMO;//	交易備註
	
	private String _CMDATE;//	轉帳日期
	
	public String getMsgCode() {
		return _MsgCode;
	}
	public void setMsgCode(String msgcode) {
		this._MsgCode = msgcode;
	}

	public String getMsgName() {
		return _MsgName;
	}
	public void setMsgName(String msgname) {
		this._MsgName = msgname;
	}

	public String getCMTXIME() {
		return _CMTXIME;
	}
	public void setCMTXIME(String cmtxime) {
		this._CMTXIME = cmtxime;
	}

	public String getOUTACN() {
		return _OUTACN;
	}
	public void setOUTACN(String outacn) {
		this._OUTACN = outacn;
	}

	public String getINTSACN() {
		return _INTSACN;
	}
	public void setINTSACN(String intsacn) {
		this._INTSACN = intsacn;
	}

	public String getAMOUNT() {
		return _AMOUNT;
	}
	public void setAMOUNT(String amount) {
		this._AMOUNT = amount;
	}

	public String getO_TOTBAL() {
		return _O_TOTBAL;
	}
	public void setO_TOTBAL(String o_totbal) {
		this._O_TOTBAL = o_totbal;
	}

	public String getO_AVLBAL() {
		return _O_AVLBAL;
	}
	public void setO_AVLBAL(String o_avlbal) {
		this._O_AVLBAL = o_avlbal;
	}

//	public String getCMTRMEMO() {
//		return _CMTRMEMO;
//	}
//	public void setCMTRMEMO(String cmtrmemo) {
//		this._CMTRMEMO = cmtrmemo;
//	}

	public String getCMDATE() {
		return _CMDATE;
	}
	public void setCMDATE(String cmdate) {
		this._CMDATE = cmdate;
	}




}
