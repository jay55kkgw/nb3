package fstop.ws;

public class WsN530Row {

	private String ACN;//	帳號
	private String DEPTYPE;//	存款種類
	private String FDPNO;//	存單號碼
	private String CUID;//	幣別
	private String BALANCE;//	存單金額
	private String ITR;//	利率(%)
	private String INTMTH;//	計息方式
	private String DPISDT;//	起存日
	private String DUEDAT;//	到期日
	private String TSFACN;//	利息轉入帳號
	private String ILAZLFTM;//	自動轉期已轉期數
	private String AUTXFTM;//	自動轉期未轉期數
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getDEPTYPE() {
		return DEPTYPE;
	}
	public void setDEPTYPE(String dEPTYPE) {
		DEPTYPE = dEPTYPE;
	}
	public String getFDPNO() {
		return FDPNO;
	}
	public void setFDPNO(String fDPNO) {
		FDPNO = fDPNO;
	}
	public String getCUID() {
		return CUID;
	}
	public void setCUID(String cUID) {
		CUID = cUID;
	}
	public String getBALANCE() {
		return BALANCE;
	}
	public void setBALANCE(String bALANCE) {
		BALANCE = bALANCE;
	}
	public String getITR() {
		return ITR;
	}
	public void setITR(String iTR) {
		ITR = iTR;
	}
	public String getINTMTH() {
		return INTMTH;
	}
	public void setINTMTH(String iNTMTH) {
		INTMTH = iNTMTH;
	}
	public String getDPISDT() {
		return DPISDT;
	}
	public void setDPISDT(String dPISDT) {
		DPISDT = dPISDT;
	}
	public String getDUEDAT() {
		return DUEDAT;
	}
	public void setDUEDAT(String dUEDAT) {
		DUEDAT = dUEDAT;
	}
	public String getTSFACN() {
		return TSFACN;
	}
	public void setTSFACN(String tSFACN) {
		TSFACN = tSFACN;
	}
	public String getILAZLFTM() {
		return ILAZLFTM;
	}
	public void setILAZLFTM(String iLAZLFTM) {
		ILAZLFTM = iLAZLFTM;
	}
	public String getAUTXFTM() {
		return AUTXFTM;
	}
	public void setAUTXFTM(String aUTXFTM) {
		AUTXFTM = aUTXFTM;
	}

	
}
