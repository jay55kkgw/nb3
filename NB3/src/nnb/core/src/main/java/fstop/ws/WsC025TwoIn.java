package fstop.ws;

public class WsC025TwoIn {

	private String _SessionId;		//身份識別
	private String _N950PASSWORD;	//交易密碼
	private String _TRANSCODE;		//基金代碼
	private String _COUNTRYTYPE;	//信託業務別
	private String _AMT3;			//申購金額
	private String _HTELPHONE;		//扣帳帳號
	private String _STOP;			//停損
	private String _YIELD;			//停利	
	private String _PAYTYPE;		//扣款方式
	private String _FUNDACN;		//入帳帳號
	private String _ADTXNO;			//TraceLog NO
	private String _FDPUBLICTYPE;	//公開說明書交付方式
	private String _PAYDAY1;		//扣款日期1
	private String _PAYDAY2;		//扣款日期2
	private String _PAYDAY3;		//扣款日期3
	private String _PAYDAY4;		//扣款日期4
	private String _PAYDAY5;		//扣款日期5
	private String _PAYDAY6;		//扣款日期6
	private String _PAYDAY7;		//扣款日期7
	private String _PAYDAY8;		//扣款日期8
	private String _PAYDAY9;		//扣款日期9
	private String _MIP;            //定期不定額註記

	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionid) {
		this._SessionId = sessionid;
	}

	public String getN950PASSWORD() {
		return _N950PASSWORD;
	}

	public void setN950PASSWORD(String n950password) {
		this._N950PASSWORD = n950password;
	}

	public String getTRANSCODE() {
		return _TRANSCODE;
	}

	public void setTRANSCODE(String transcode) {
		this._TRANSCODE = transcode;
	}

	public String getCOUNTRYTYPE() {
		return _COUNTRYTYPE;
	}

	public void setCOUNTRYTYPE(String countrytype) {
		this._COUNTRYTYPE = countrytype;
	}

	public String getAMT3() {
		return _AMT3;
	}

	public void setAMT3(String amt3) {
		this._AMT3 = amt3;
	}

	public String getHTELPHONE() {
		return _HTELPHONE;
	}

	public void setHTELPHONE(String htelphone) {
		this._HTELPHONE = htelphone;
	}

	public String getSTOP() {
		return _STOP;
	}

	public void setSTOP(String stop) {
		this._STOP = stop;
	}

	public String getYIELD() {
		return _YIELD;
	}

	public void setYIELD(String yield) {
		this._YIELD = yield;
	}

	public String getPAYTYPE() {
		return _PAYTYPE;
	}

	public void setPAYTYPE(String paytype) {
		this._PAYTYPE = paytype;
	}

	public String getFUNDACN() {
		return _FUNDACN;
	}

	public void setFUNDACN(String fundacn) {
		this._FUNDACN = fundacn;
	}

	public String getADTXNO() {
		return _ADTXNO;
	}

	public void setADTXNO(String adtxno) {
		this._ADTXNO = adtxno;
	}

	public String getFDPUBLICTYPE() {
		return _FDPUBLICTYPE;
	}

	public void setFDPUBLICTYPE(String fdpublictype) {
		this._FDPUBLICTYPE = fdpublictype;
	}

	public String getPAYDAY1() {
		return _PAYDAY1;
	}

	public void setPAYDAY1(String payday1) {
		this._PAYDAY1 = payday1;
	}

	public String getPAYDAY2() {
		return _PAYDAY2;
	}

	public void setPAYDAY2(String payday2) {
		this._PAYDAY2 = payday2;
	}

	public String getPAYDAY3() {
		return _PAYDAY3;
	}

	public void setPAYDAY3(String payday3) {
		this._PAYDAY3 = payday3;
	}

	public String getPAYDAY4() {
		return _PAYDAY4;
	}

	public void setPAYDAY4(String payday4) {
		this._PAYDAY4 = payday4;
	}
	
	public String getPAYDAY5() {
		return _PAYDAY5;
	}

	public void setPAYDAY5(String payday5) {
		this._PAYDAY5 = payday5;
	}
	
	public String getPAYDAY6() {
		return _PAYDAY6;
	}

	public void setPAYDAY6(String payday6) {
		this._PAYDAY6 = payday6;
	}
	
	public String getPAYDAY7() {
		return _PAYDAY7;
	}

	public void setPAYDAY7(String payday7) {
		this._PAYDAY7 = payday7;
	}
	
	public String getPAYDAY8() {
		return _PAYDAY8;
	}

	public void setPAYDAY8(String payday8) {
		this._PAYDAY8 = payday8;
	}
	
	public String getPAYDAY9() {
		return _PAYDAY9;
	}

	public void setPAYDAY9(String payday9) {
		this._PAYDAY9 = payday9;
	}

	public String getMIP() {
		return _MIP;
	}

	public void setMIP(String mip) {
		_MIP = mip;
	}
	
}
