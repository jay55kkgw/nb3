package fstop.ws;

public class WsN022Out {
	private String _MsgCode;

	private String _MsgName;

	private String _DATE;

	private String _TIME;

	private WsN022Row1[] _Table1;

	private WsN022Row2[] _Table2;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getDATE() {
		return _DATE;
	}

	public void setDATE(String DATE) {
		this._DATE = DATE;
	}

	public String getTIME() {
		return _TIME;
	}

	public void setTIME(String TIME) {
		this._TIME = TIME;
	}

	public WsN022Row1[] getTable1() {
		return _Table1;
	}

	public void setTable1(WsN022Row1[] table1) {
		this._Table1 = table1;
	}

	public WsN022Row2[] getTable2() {
		return _Table2;
	}

	public void setTable2(WsN022Row2[] table2) {
		this._Table2 = table2;
	}

}
