package fstop.ws;

public class CmNotifyInfo {

	private String _MsgCode;

	private String _MsgName;

	private String _SENDME;

	private String _DPMYMAIL;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getSENDME() {
		return _SENDME;
	}

	public void setSENDME(String sendme) {
		this._SENDME = sendme;
	}

	public String getDPMYMAIL() {
		return _DPMYMAIL;
	}

	public void setDPMYMAIL(String dpmymail) {
		this._DPMYMAIL = dpmymail;
	}

}
