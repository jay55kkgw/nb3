package fstop.ws;

public class WsBM30000MergeContent {
	
private String _MsgCode;
	
	private String _MsgName;
	
	private WsBM3000MergeRow2[] _Table;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public WsBM3000MergeRow2[] getTable() {
		return _Table;
	}

	public void setTable(WsBM3000MergeRow2[] table) {
		this._Table = table;
	}


}
