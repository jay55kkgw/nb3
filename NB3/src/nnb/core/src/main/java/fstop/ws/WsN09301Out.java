package fstop.ws;

public class WsN09301Out {

	private String _MsgCode;

	private String _MsgName;

	private String _CMQTIME;

	private String _SVACN;

	private String _ACN;

	private String _AMT_06;
	
	private String _AMT_16;
	
	private String _AMT_26;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getCMQTIME() {
		return _CMQTIME;
	}

	public void setCMQTIME(String CMQTIME) {
		this._CMQTIME = CMQTIME;
	}

	public String getSVACN() {
		return _SVACN;
	}

	public void setSVACN(String SVACN) {
		this._SVACN = SVACN;
	}

	public String getACN() {
		return _ACN;
	}

	public void setACN(String ACN) {
		this._ACN = ACN;
	}
	
	public String getAMT_06() {
		return _AMT_06;
	}

	public void setAMT_06(String AMT_06) {
		this._AMT_06 = AMT_06;
	}

	public String getAMT_16() {
		return _AMT_16;
	}

	public void setAMT_16(String AMT_16) {
		this._AMT_16 = AMT_16;
	}
	
	public String getAMT_26() {
		return _AMT_26;
	}

	public void setAMT_26(String AMT_26) {
		this._AMT_26 = AMT_26;
	}
}
