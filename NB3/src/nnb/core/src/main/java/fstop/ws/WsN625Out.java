package fstop.ws;

public class WsN625Out {
	
	private String _MsgCode;
	
	private String _MsgName;
	
	private String _CMQTIME;

	private String _QUERYNEXT;
	
	private String _BOQUERYNEXT;

	private WsN625Row[] _Table;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		_MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		_MsgName = msgName;
	}

	public String getCMQTIME() {
		return _CMQTIME;
	}

	public void setCMQTIME(String cmqtime) {
		_CMQTIME = cmqtime;
	}

	public String getQUERYNEXT() {
		return _QUERYNEXT;
	}

	public void setQUERYNEXT(String querynext) {
		_QUERYNEXT = querynext;
	}

	public String getBOQUERYNEXT() {
		return _BOQUERYNEXT;
	}

	public void setBOQUERYNEXT(String boquerynext) {
		_BOQUERYNEXT = boquerynext;
	}

	public WsN625Row[] getTable() {
		return _Table;
	}

	public void setTable(WsN625Row[] table) {
		_Table = table;
	}
		
}
