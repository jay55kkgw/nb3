package fstop.ws;

public class WsN215In {

	private String _SessionId;

	private String _TXTOKEN;// 防止重送代碼

	private String _OTPKEY;// OTP動態密碼
	
	private String _CAPTCHA;// 圖形驗證碼

	private String _TYPE;

	private String _COUNT;

	//private String _ACNINFO;

	private String _BNKCOD;
	
	private String _BNKNAME;
	
	private String _TSFACN;

	private String _DPACCSETID;
	
	private String _DPACGONAME;
	
	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		this._SessionId = sessionId;
	}

	public String getTXTOKEN() {
		return _TXTOKEN;
	}

	public void setTXTOKEN(String txtoken) {
		this._TXTOKEN = txtoken;
	}

	public String getOTPKEY() {
		return _OTPKEY;
	}

	public void setOTPKEY(String otpkey) {
		this._OTPKEY = otpkey;
	}
	
	public String getCAPTCHA() {
		return _CAPTCHA;
	}

	public void setCAPTCHA(String captcha) {
		this._CAPTCHA = captcha;
	}
	
	public String getTYPE() {
		return _TYPE;
	}

	public void setTYPE(String type) {
		this._TYPE = type;
	}

	public String getCOUNT() {
		return _COUNT;
	}

	public void setCOUNT(String count) {
		this._COUNT = count;
	}

	public String getBNKCOD() {
		return _BNKCOD;
	}

	public void setBNKCOD(String bnkcod) {
		this._BNKCOD = bnkcod;
	}

	public String getBNKNAME() {
		return _BNKNAME;
	}

	public void setBNKNAME(String bnkname) {
		this._BNKNAME = bnkname;
	}

	
	public String getTSFACN() {
		return _TSFACN;
	}

	public void setTSFACN(String tsfacn) {
		this._TSFACN = tsfacn;
	}
	
	public String getDPACCSETID() {
		return _DPACCSETID;
	}

	public void setDPACCSETID(String dpaccsetid) {
		this._DPACCSETID = dpaccsetid;
	}
	
	
	public String getDPACGONAME() {
		return _DPACGONAME;
	}

	public void setDPACGONAME(String dpacgoname) {
		this._DPACGONAME = dpacgoname;
	}
	
}
