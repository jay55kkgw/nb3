package fstop.ws;

public class WsN130Out {
	
	private String _MsgCode; //訊息代碼
	
	private String _MsgName; //訊息名稱	
	
	private String _CMQTIME;//查詢時間
	
	private String _QUERYNEXT;//	繼續查詢key1

	private String _BOQUERYNEXT;//  繼續查詢Key2

	private WsN130Row[] _Table; //表格結果


	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		_MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		_MsgName = msgName;
	}

	public String getCMQTIME() {
		return _CMQTIME;
	}

	public void setCMQTIME(String cmqtime) {
		_CMQTIME = cmqtime;
	}

	public String getQUERYNEXT() {
		return _QUERYNEXT;
	}

	public void setQUERYNEXT(String querynext) {
		_QUERYNEXT = querynext;
	}

	public String getBOQUERYNEXT() {
		return _BOQUERYNEXT;
	}

	public void setBOQUERYNEXT(String boquerynext) {
		_BOQUERYNEXT = boquerynext;
	}

	public WsN130Row[] getTable() {
		return _Table;
	}

	public void setTable(WsN130Row[] table) {
		_Table = table;
	}
	
}
