package fstop.ws;

public class WsFundAgreeOut {

	private String _MsgCode;// 訊息代碼
	private String _MsgName;// 訊息名稱
	private String _OutData;// 查詢條件

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getOutData() {
		return _OutData;
	}

	public void setOutData(String outData) {
		this._OutData = outData;
	}

}
