package fstop.ws;

public class WsN079Out {

	private String _MsgCode;	//訊息代碼	
	private String _MsgName;	//訊息名稱	
	private String _CMQTIME;	//交易時間
	private String _FDPACN;		//帳號
	private String _FDPTYPE;	//存款種類
	private String _FDPNUM;		//存單號碼
	private String _AMT;		//存單金額	
	private String _INTMTH;		//計息方式	
	private String _DPISDT;		//起存日
	private String _DUEDAT;		//到期日			
	private String _INTPAY;		//利息	
	private String _TAX;		//所得稅	
	private String _INTRCV;		//透支息
	private String _PAIAFTX;	//稅後本息
	private String _NHITAX;		//健保費
	private String _CMTRMEMO;	//交易備註
	
	public String getMsgCode() {
		return _MsgCode;
	}
	public void setMsgCode(String msgcode) {
		this._MsgCode = msgcode;
	}

	public String getMsgName() {
		return _MsgName;
	}
	public void setMsgName(String msgname) {
		this._MsgName = msgname;
	}

	public String getCMQTIME() {
		return _CMQTIME;
	}
	public void setCMQTIME(String CMQTIME) {
		this._CMQTIME = CMQTIME;
	}

	public String getFDPACN() {
		return _FDPACN;
	}
	public void setFDPACN(String FDPACN) {
		this._FDPACN = FDPACN;
	}
	
	public String getFDPTYPE() {
		return _FDPTYPE;
	}
	public void setFDPTYPE(String FDPTYPE) {
		this._FDPTYPE = FDPTYPE;
	}
	
	public String getFDPNUM() {
		return _FDPNUM;
	}
	public void setFDPNUM(String FDPNUM) {
		this._FDPNUM = FDPNUM;
	}
	
	public String getAMT() {
		return _AMT;
	}
	public void setAMT(String AMT) {
		this._AMT = AMT;
	}
	
	public String getINTMTH() {
		return _INTMTH;
	}
	public void setINTMTH(String INTMTH) {
		this._INTMTH = INTMTH;
	}		

	public String getDPISDT() {
		return _DPISDT;
	}
	public void setDPISDT(String DPISDT) {
		this._DPISDT = DPISDT;
	}	

	public String getDUEDAT() {
		return _DUEDAT;
	}
	public void setDUEDAT(String DUEDAT) {
		this._DUEDAT = DUEDAT;
	}	
	
	public String getTAX() {
		return _TAX;
	}
	public void setTAX(String TAX) {
		this._TAX = TAX;
	}

	public String getINTPAY() {
		return _INTPAY;
	}
	public void setINTPAY(String INTPAY) {
		this._INTPAY = INTPAY;
	}		

	public String getINTRCV() {
		return _INTRCV;
	}
	public void setINTRCV(String INTRCV) {
		this._INTRCV = INTRCV;
	}
	
	public String getPAIAFTX() {
		return _PAIAFTX;
	}
	public void setPAIAFTX(String PAIAFTX) {
		this._PAIAFTX = PAIAFTX;
	}

	public String getNHITAX() {
		return _NHITAX;
	}
	public void setNHITAX(String NHITAX) {
		this._NHITAX = NHITAX;
	}

	public String getCMTRMEMO() {
		return _CMTRMEMO;
	}
	public void setCMTRMEMO(String CMTRMEMO) {
		this._CMTRMEMO = CMTRMEMO;
	}
}

