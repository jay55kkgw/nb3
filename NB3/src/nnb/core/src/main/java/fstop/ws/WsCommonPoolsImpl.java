package fstop.ws;

public class WsCommonPoolsImpl implements WsCommonPools{

	@Override
	public CmAcno getAgreeAcnoList(CmUser params){
		return null;
	}

	@Override
	public CmBank getBankList(CmUser params){
		return null;
	}

	@Override
	public CmNotifyInfo getNotifyInfo(CmUser params){
		return null;
	}

	@Override
	public CmTxToken getTxToken(CmUser params){
		return null;
	}

	@Override
	public CmCrdAcno getAgreeCreditAcnoList(CmUser params){
		return null;
	}

	@Override
	public CmCode getCodeList(CmUserQry params){
		return null;
	}

	@Override
	public CmFxCryOut getCRYList(CmFxCryIn params){
		return null;
	}

	@Override
	public CmFxAcno getAgreeFxAcnoList(CmUser params){
		return null;
	}

	@Override
	public CmBhContactOut getAdmBhContact(CmBhContactIn params){
		return null;
	}

	@Override
	public CmFxCryOut getComCcyList(CmComCcyIn params){
		return null;
	}

	@Override
	public CmCRY getCRY(CmUserQry params){
		return null;
	}

	@Override
	public CmMsg checkGoldBizTimeStatus(CmUserQry params){
		return null;
	}

	@Override
	public CmMsgData checkFxBizTimeStatus(CmUser params){
		return null;
	}

	@Override
	public CmMsgData getSystemDateTime(CmUser params){
		return null;
	}

	@Override
	public CmFxDepCryOut getFxDepCRYList(CmFxCryIn params){
		return null;
	}

	@Override
	public CmAcno getCertAgreeAcnoList(CmUser params){
		return null;
	}
}