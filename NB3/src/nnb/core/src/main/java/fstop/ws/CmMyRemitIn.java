package fstop.ws;

public class CmMyRemitIn {
	
	private String _SessionId;

	private String _UID;
	
	public String getSessionId() {
		return _SessionId;
	}
	public void setSessionId(String sessionid) {
		this._SessionId = sessionid;
	}

	public String getUID() {
		return _UID;
	}
	public void setUID(String uid) {
		this._UID = uid;
	}
}
