package fstop.ws;

public class WsF001SOut {

	private String _MsgCode;

	private String _MsgName;

	private String _CMQTIME;

	public String getMsgCode() {
		return _MsgCode;
	}
	public void setMsgCode(String msgcode) {
		this._MsgCode = msgcode;
	}

	public String getMsgName() {
		return _MsgName;
	}
	public void setMsgName(String msgname) {
		this._MsgName = msgname;
	}

	public String getCMQTIME() {
		return _CMQTIME;
	}
	public void setCMQTIME(String cmqtime) {
		this._CMQTIME = cmqtime;
	}

}
