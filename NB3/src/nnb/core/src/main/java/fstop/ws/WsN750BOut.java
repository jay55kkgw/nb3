package fstop.ws;

public class WsN750BOut {

	private String _MsgCode	;//訊息代碼
	
	private String _MsgName;//訊息名稱
	
	private String _CMTXIME;//	交易時間
	
	private String _OUTACN;//	轉出帳號
	
	private String _BARCODE1;//	條碼一
	
	private String _BARCODE2;//	條碼二
	
	private String _BARCODE3;//	條碼三
	
	private String _WAT_NO;//	水號
	
	private String _AMOUNT;//	繳款金額
	
	private String _TOTBAL;//	轉出帳號帳上餘額
	
	private String _AVLBAL;//	轉出帳號可用餘額
	
	//private String _CMTRMEMO;//	交易備註
	
	private String _CMDATE;//	轉帳日期
	
	public String getMsgCode() {
		return _MsgCode;
	}
	public void setMsgCode(String msgcode) {
		this._MsgCode = msgcode;
	}

	public String getMsgName() {
		return _MsgName;
	}
	public void setMsgName(String msgname) {
		this._MsgName = msgname;
	}

	public String getCMTXIME() {
		return _CMTXIME;
	}
	public void setCMTXIME(String cmtxime) {
		this._CMTXIME = cmtxime;
	}

	public String getOUTACN() {
		return _OUTACN;
	}
	public void setOUTACN(String outacn) {
		this._OUTACN = outacn;
	}

	public String getBARCODE1() {
		return _BARCODE1;
	}
	public void setBARCODE1(String barcode1) {
		this._BARCODE1 = barcode1;
	}

	public String getBARCODE2() {
		return _BARCODE2;
	}
	public void setBARCODE2(String barcode2) {
		this._BARCODE2 = barcode2;
	}

	public String getBARCODE3() {
		return _BARCODE3;
	}
	public void setBARCODE3(String barcode3) {
		this._BARCODE3 = barcode3;
	}

	public String getWAT_NO() {
		return _WAT_NO;
	}
	public void setWAT_NO(String wat_no) {
		this._WAT_NO = wat_no;
	}

	public String getAMOUNT() {
		return _AMOUNT;
	}
	public void setAMOUNT(String amount) {
		this._AMOUNT = amount;
	}

	public String getTOTBAL() {
		return _TOTBAL;
	}
	public void setTOTBAL(String totbal) {
		this._TOTBAL = totbal;
	}

	public String getAVLBAL() {
		return _AVLBAL;
	}
	public void setAVLBAL(String avlbal) {
		this._AVLBAL = avlbal;
	}

//	public String getCMTRMEMO() {
//		return _CMTRMEMO;
//	}
//	public void setCMTRMEMO(String cmtrmemo) {
//		this._CMTRMEMO = cmtrmemo;
//	}

	public String getCMDATE() {
		return _CMDATE;
	}
	public void setCMDATE(String cmdate) {
		this._CMDATE = cmdate;
	}



}
