package fstop.ws;

public class WsN015Row {

	private String _CHKDATE;

	private String _CHKNUM;

	private String _AMTACC;

	public String getCHKDATE() {
		return _CHKDATE;
	}

	public void setCHKDATE(String chkdate) {
		this._CHKDATE = chkdate;
	}

	public String getCHKNUM() {
		return _CHKNUM;
	}

	public void setCHKNUM(String chknum) {
		this._CHKNUM = chknum;
	}

	public String getAMTACC() {
		return _AMTACC;
	}

	public void setAMTACC(String amtacc) {
		this._AMTACC = amtacc;
	}

}
