package fstop.ws;

public class WsC014In {

	private String _SessionId;	
	private String _N950PASSWORD; //交易密碼	
	private String _CREDITNO;     //信託帳號	
	private String _TRANSCODE;    //基金代碼
	private String _TRADEDATE;    //交易日期
	private String _FUNDTYPE;     //交易類別
	private String _INTRANSCODE;  //轉入基金代碼

	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionid) {
		this._SessionId = sessionid;
	}

	public String getN950PASSWORD() {
		return _N950PASSWORD;
	}

	public void setN950PASSWORD(String N950PASSWORD) {
		this._N950PASSWORD = N950PASSWORD;
	}

	public String getCREDITNO() {
		return _CREDITNO;
	}

	public void setCREDITNO(String CREDITNO) {
		this._CREDITNO = CREDITNO;
	}

	public String getTRANSCODE() {
		return _TRANSCODE;
	}

	public void setTRANSCODE(String TRANSCODE) {
		this._TRANSCODE = TRANSCODE;
	}
	
	public String getTRADEDATE() {
		return _TRADEDATE;
	}

	public void setTRADEDATE(String TRADEDATE) {
		this._TRADEDATE = TRADEDATE;
	}
	
	public String getFUNDTYPE() {
		return _FUNDTYPE;
	}

	public void setFUNDTYPE(String FUNDTYPE) {
		this._FUNDTYPE = FUNDTYPE;
	}
	
	public String getINTRANSCODE() {
		return _INTRANSCODE;
	}

	public void setINTRANSCODE(String INTRANSCODE) {
		this._INTRANSCODE = INTRANSCODE;
	}
}
