package fstop.ws;

public class WsC021QryOut {

	private String _MsgCode;// 訊息代碼
	private String _MsgName;// 訊息名稱
	private String _FDINVTYPE;// 投資屬性
	private String _GETLTD;// 投資屬性資料日期
	private WsC021QryRow[] _Table;// 表格結果

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getFDINVTYPE() {
		return _FDINVTYPE;
	}

	public void setFDINVTYPE(String fdinvtype) {
		this._FDINVTYPE = fdinvtype;
	}

	public String getGETLTD() {
		return _GETLTD;
	}

	public void setGETLTD(String getltd) {
		this._GETLTD = getltd;
	}

	public WsC021QryRow[] getTable() {
		return _Table;
	}

	public void setTable(WsC021QryRow[] table) {
		this._Table = table;
	}

}
