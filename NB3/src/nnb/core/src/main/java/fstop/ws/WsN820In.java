package fstop.ws;

public class WsN820In {
	
	private String _SessionId;
	
	private String _N950PASSWORD;
	
	private String _CARDTYPE;
	
	private String _FGPERIOD;
	
	private String _card_type;
	
	private String _bcardflag;
	
	private String _Bill_No;

	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		_SessionId = sessionId;
	}

	public String getN950PASSWORD() {
		return _N950PASSWORD;
	}

	public void setN950PASSWORD(String n950password) {
		_N950PASSWORD = n950password;
	}

	public String getCARDTYPE() {
		return _CARDTYPE;
	}

	public void setCARDTYPE(String cardtype) {
		_CARDTYPE = cardtype;
	}

	public String getFGPERIOD() {
		return _FGPERIOD;
	}

	public void setFGPERIOD(String fgperiod) {
		_FGPERIOD = fgperiod;
	}

	public String getcard_type() {
		return _card_type;
	}

	public void setcard_type(String card_type) {
		this._card_type = card_type;
	}

	public String getbcardflag() {
		return _bcardflag;
	}

	public void setbcardflag(String bcardflag) {
		this._bcardflag = bcardflag;
	}

	public String getBill_No() {
		return _Bill_No;
	}

	public void setBill_No(String bill_No) {
		_Bill_No = bill_No;
	}	
	
}
