package fstop.ws;

public class CmTxToken {

	private String _MsgCode;

	private String _MsgName;

	private String _TXTOKEN;

	private String _CAPTCHA;
	
	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getTXTOKEN() {
		return _TXTOKEN;
	}

	public void setTXTOKEN(String txtoken) {
		this._TXTOKEN = txtoken;
	}

	public String getCAPTCHA() {
		return _CAPTCHA;
	}

	public void setCAPTCHA(String captcha) {
		this._CAPTCHA = captcha;
	}

}
