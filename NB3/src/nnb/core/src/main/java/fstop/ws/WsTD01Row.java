package fstop.ws;

public class WsTD01Row {
	
	private String _CARDNUM;
	
	private String _CARDTYPE;
	
	private String _PURDATE;
	
	private String _POSTDATE;
	
	private String _DESCTXT;
	
	private String _CRDNAME;
	
	private String _CURRENCY;
	
	private String _SRCAMNT;
	
	private String _CURAMNT;

	public String getCARDNUM() {
		return _CARDNUM;
	}

	public void setCARDNUM(String cardnum) {
		_CARDNUM = cardnum;
	}

	public String getCARDTYPE() {
		return _CARDTYPE;
	}

	public void setCARDTYPE(String cardtype) {
		_CARDTYPE = cardtype;
	}

	public String getPURDATE() {
		return _PURDATE;
	}

	public void setPURDATE(String purdate) {
		_PURDATE = purdate;
	}

	public String getPOSTDATE() {
		return _POSTDATE;
	}

	public void setPOSTDATE(String postdate) {
		_POSTDATE = postdate;
	}

	public String getDESCTXT() {
		return _DESCTXT;
	}

	public void setDESCTXT(String desctxt) {
		_DESCTXT = desctxt;
	}

	public String getCRDNAME() {
		return _CRDNAME;
	}

	public void setCRDNAME(String crdname) {
		_CRDNAME = crdname;
	}

	public String getCURRENCY() {
		return _CURRENCY;
	}

	public void setCURRENCY(String currency) {
		_CURRENCY = currency;
	}

	public String getSRCAMNT() {
		return _SRCAMNT;
	}

	public void setSRCAMNT(String srcamnt) {
		_SRCAMNT = srcamnt;
	}

	public String getCURAMNT() {
		return _CURAMNT;
	}

	public void setCURAMNT(String curamnt) {
		_CURAMNT = curamnt;
	}
		
}
