package fstop.ws;

import java.io.Serializable;

public class WsCN32In implements Serializable {
	private static final long serialVersionUID = -5387513192373327720L;
	private String sessionId;	//SessionId
	private String PINBLOCK;
	private String CUSIDN;
	//private String BNKCOD;
	private String CN;
	private String ACN;

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}


	public String getPINBLOCK() {
		return PINBLOCK;
	}

	public void setPINBLOCK(String PINBLOCK) {
		this.PINBLOCK = PINBLOCK;
	}

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String CUSIDN) {
		this.CUSIDN = CUSIDN;
	}

//	public String getBNKCOD() {
//		return BNKCOD;
//	}
//
//	public void setBNKCOD(String BNKCOD) {
//		this.BNKCOD = BNKCOD;
//	}

	public String getACN() {
		return ACN;
	}

	public void setACN(String ACN) {
		this.ACN = ACN;
	}


	public String getCN() {
		return CN;
	}

	public void setCN(String CN) {
		this.CN = CN;
	}
}
