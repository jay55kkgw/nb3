package fstop.ws;

public class WsGD11HSIn {
	
	private String _SessionId;//	身份識別
	
	private String _N950PASSWORD;//	交易密碼
	
	private String _QUERYTYPE;//	查詢項目
	
	private String _CMSDATE;//	日期(起)
	
	private String _CMEDATE	;//日期(迄)

	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		this._SessionId = sessionId;
	}

	public String getN950PASSWORD() {
		return _N950PASSWORD;
	}

	public void setN950PASSWORD(String n950password) {
		this._N950PASSWORD = n950password;
	}

	public String getQUERYTYPE() {
		return _QUERYTYPE;
	}

	public void setQUERYTYPE(String querytype) {
		this._QUERYTYPE = querytype;
	}

	public String getCMSDATE() {
		return _CMSDATE;
	}

	public void setCMSDATE(String cmsdate) {
		this._CMSDATE = cmsdate;
	}

	public String getCMEDATE() {
		return _CMEDATE;
	}

	public void setCMEDATE(String cmedate) {
		this._CMEDATE = cmedate;
	}

	

}
