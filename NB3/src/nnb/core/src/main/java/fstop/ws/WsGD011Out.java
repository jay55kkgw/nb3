package fstop.ws;

public class WsGD011Out {

	private String _MsgCode;

	private String _MsgName;

	private String _DATE;

	private String _TIME;

	private String _BPRICE;

	private String _SPRICE;

	private String _PRICE1;

	private String _PRICE2;

	private String _PRICE3;

	private String _PRICE4;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getDATE() {
		return _DATE;
	}

	public void setDATE(String DATE) {
		this._DATE = DATE;
	}

	public String getTIME() {
		return _TIME;
	}

	public void setTIME(String TIME) {
		this._TIME = TIME;
	}

	public String getBPRICE() {
		return _BPRICE;
	}

	public void setBPRICE(String BPRICE) {
		this._BPRICE = BPRICE;
	}

	public String getSPRICE() {
		return _SPRICE;
	}

	public void setSPRICE(String SPRICE) {
		this._SPRICE = SPRICE;
	}

	public String getPRICE1() {
		return _PRICE1;
	}

	public void setPRICE1(String PRICE1) {
		this._PRICE1 = PRICE1;
	}

	public String getPRICE2() {
		return _PRICE2;
	}

	public void setPRICE2(String PRICE2) {
		this._PRICE2 = PRICE2;
	}

	public String getPRICE3() {
		return _PRICE3;
	}

	public void setPRICE3(String PRICE3) {
		this._PRICE3 = PRICE3;
	}

	public String getPRICE4() {
		return _PRICE4;
	}

	public void setPRICE4(String PRICE4) {
		this._PRICE4 = PRICE4;
	}

}
