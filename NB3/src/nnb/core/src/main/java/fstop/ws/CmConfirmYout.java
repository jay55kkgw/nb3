package fstop.ws;

public class CmConfirmYout {

	private String _MsgCode;//	訊息代碼
	
	private String _MsgName	;//訊息名稱
	
	private String _ADTXNO;//	交易編號

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getADTXNO() {
		return _ADTXNO;
	}

	public void setADTXNO(String adtxno) {
		this._ADTXNO = adtxno;
	}

}
