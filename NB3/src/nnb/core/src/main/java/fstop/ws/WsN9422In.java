package fstop.ws;

public class WsN9422In {
	
	private String _SessionId;
	
	private String _OLDUID;
	
	private String _NEWUID;
	
	private String _RENEWUID;

	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		_SessionId = sessionId;
	}

	public String getOLDUID() {
		return _OLDUID;
	}

	public void setOLDUID(String olduid) {
		_OLDUID = olduid;
	}

	public String getNEWUID() {
		return _NEWUID;
	}

	public void setNEWUID(String newuid) {
		_NEWUID = newuid;
	}

	public String getRENEWUID() {
		return _RENEWUID;
	}

	public void setRENEWUID(String renewuid) {
		_RENEWUID = renewuid;
	}
	
}
