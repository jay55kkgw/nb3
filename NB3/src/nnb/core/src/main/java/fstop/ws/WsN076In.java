package fstop.ws;

public class WsN076In {

	private String SessionId;//	身份識別
	private String TXTOKEN;//	防止重送代碼
	private String FGTXWAY;//	密碼類別
	private String CMPASSWORD;//	交易密碼
	private String PINNEW;//	交易密碼SHA1值
	private String OTPKEY;//	OTP動態密碼
	private String OUTACN;//	轉出帳號
	private String FDPACN;//	存單帳號
	private String AMOUNT;//	轉帳金額
	private String FDPNUM;//	存單號碼
	private String CMTRMEMO;//	交易備註
	private String CMTRMAIL;//	Email信箱
	private String CMMAILMEMO;//	Email摘要內容
	private String CAPTCHA;//	圖形驗證碼
	
	//no PINNEW
	@Override
	public String toString()
	{
		return "WsN076In [SessionId=" + SessionId + ", TXTOKEN=" + TXTOKEN + ", FGTXWAY=" + FGTXWAY + ", OTPKEY="
				+ OTPKEY + ", OUTACN=" + OUTACN + ", FDPACN=" + FDPACN + ", AMOUNT=" + AMOUNT + ", FDPNUM=" + FDPNUM
				+ ", CMTRMEMO=" + CMTRMEMO + ", CMTRMAIL=" + CMTRMAIL + ", CMMAILMEMO=" + CMMAILMEMO + ", CAPTCHA="
				+ CAPTCHA + "]";
	}
	public String getSessionId() {
		return SessionId;
	}
	public void setSessionId(String sessionId) {
		SessionId = sessionId;
	}
	public String getTXTOKEN() {
		return TXTOKEN;
	}
	public void setTXTOKEN(String tXTOKEN) {
		TXTOKEN = tXTOKEN;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getCMPASSWORD() {
		return CMPASSWORD;
	}
	public void setCMPASSWORD(String cMPASSWORD) {
		CMPASSWORD = cMPASSWORD;
	}
	public String getPINNEW() {
		return PINNEW;
	}
	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}
	public String getOTPKEY() {
		return OTPKEY;
	}
	public void setOTPKEY(String oTPKEY) {
		OTPKEY = oTPKEY;
	}
	public String getOUTACN() {
		return OUTACN;
	}
	public void setOUTACN(String oUTACN) {
		OUTACN = oUTACN;
	}
	public String getFDPACN() {
		return FDPACN;
	}
	public void setFDPACN(String fDPACN) {
		FDPACN = fDPACN;
	}
	public String getAMOUNT() {
		return AMOUNT;
	}
	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}
	public String getFDPNUM() {
		return FDPNUM;
	}
	public void setFDPNUM(String fDPNUM) {
		FDPNUM = fDPNUM;
	}
	public String getCMTRMEMO() {
		return CMTRMEMO;
	}
	public void setCMTRMEMO(String cMTRMEMO) {
		CMTRMEMO = cMTRMEMO;
	}
	public String getCMTRMAIL() {
		return CMTRMAIL;
	}
	public void setCMTRMAIL(String cMTRMAIL) {
		CMTRMAIL = cMTRMAIL;
	}
	public String getCMMAILMEMO() {
		return CMMAILMEMO;
	}
	public void setCMMAILMEMO(String cMMAILMEMO) {
		CMMAILMEMO = cMMAILMEMO;
	}
	public String getCAPTCHA() {
		return CAPTCHA;
	}
	public void setCAPTCHA(String cAPTCHA) {
		CAPTCHA = cAPTCHA;
	}
	
	

}
