package fstop.ws;

public class WsC117Out {

	private String _MsgCode;	  //訊息代碼
	private String _MsgName;	  //訊息名稱
	private String _CMQTIME;	  //交易時間
	private String _CUSIDN;	  	  //身分證字號/統一編號
	private String _CREDITNO;	  //信託帳號
	private String _FUNDLNAME;	  //基金名稱
	private String _AC202; 		  //類別
	private String _FUNDAMT; 	  //信託金額
	private String _FUNDCUR; 	  //信託金額幣別
	private String _PAYAMT;		  //扣款金額
	private String _PAYCUR;		  //扣款金額幣別
	private String _NSTOPPROF;	  //停利點
	private String _NSTOPLOSS;	  //停損點

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getCMQTIME() {
		return _CMQTIME;
	}

	public void setCMQTIME(String CMQTIME) {
		this._CMQTIME = CMQTIME;
	}
	
	public String getCUSIDN() {
		return _CUSIDN;
	}

	public void setCUSIDN(String CUSIDN) {
		this._CUSIDN = CUSIDN;
	}
	
	public String getCREDITNO() {
		return _CREDITNO;
	}

	public void setCREDITNO(String CREDITNO) {
		this._CREDITNO = CREDITNO;
	}
	
	public String getFUNDLNAME() {
		return _FUNDLNAME;
	}

	public void setFUNDLNAME(String FUNDLNAME) {
		this._FUNDLNAME = FUNDLNAME;
	}
	
	public String getAC202() {
		return _AC202;
	}

	public void setAC202(String AC202) {
		this._AC202 = AC202;
	}
	
	public String getFUNDAMT() {
		return _FUNDAMT;
	}

	public void setFUNDAMT(String FUNDAMT) {
		this._FUNDAMT = FUNDAMT;
	}
	
	public String getFUNDCUR() {
		return _FUNDCUR;
	}

	public void setFUNDCUR(String FUNDCUR) {
		this._FUNDCUR = FUNDCUR;
	}
	
	public String getPAYAMT() {
		return _PAYAMT;
	}

	public void setPAYAMT(String PAYAMT) {
		this._PAYAMT = PAYAMT;
	}
			
	public String getPAYCUR() {
		return _PAYCUR;
	}

	public void setPAYCUR(String PAYCUR) {
		this._PAYCUR = PAYCUR;
	}
	
	public String getNSTOPPROF() {
		return _NSTOPPROF;
	}

	public void setNSTOPPROF(String NSTOPPROF) {
		this._NSTOPPROF = NSTOPPROF;
	}
	
	public String getNSTOPLOSS() {
		return _NSTOPLOSS;
	}

	public void setNSTOPLOSS(String NSTOPLOSS) {
		this._NSTOPLOSS = NSTOPLOSS;
	}
}
