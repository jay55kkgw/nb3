package fstop.ws;

public class CmBankRow {

	private String _ADBANKID;
	
	private String _ADBANKNAME;

	public String getADBANKID() {
		return _ADBANKID;
	}

	public void setADBANKID(String adbankid) {
		this._ADBANKID = adbankid;
	}

	public String getADBANKNAME() {
		return _ADBANKNAME;
	}

	public void setADBANKNAME(String adbankname) {
		this._ADBANKNAME = adbankname;
	}
	
	
}
