package fstop.ws;

public class WsN816LIn {

	// 身份證別
	private String _SessionId;
	
	// 防止重送代碼
	private String _TXTOKEN;

	// 圖形驗證碼
	private String _CAPTCHA;	

	// 密碼類別
	private String _FGTXWAY;

	// 交易密碼
	private String _CMPASSWORD;

	// 交易密碼 SHA1
	private String _PINNEW;

	// 信用卡別
	private String _TYPENAME;

	// 信用卡號
	private String _CARDNUM;

	// 卡片效期
	private String _EXP_DATE;
	
	// 電子票證種類 (1：一般信用卡, 2：悠遊卡, 3：一卡通)
	private String _EPC_FLAG;

	
	
	
	
	@Override
	public String toString() {
		return "WsN816LIn [_SessionId=" + _SessionId + ", _TXTOKEN=" + _TXTOKEN + ", _CAPTCHA=" + _CAPTCHA
				+ ", _FGTXWAY=" + _FGTXWAY +  ", _TYPENAME="
				+ _TYPENAME + ", _CARDNUM=" + _CARDNUM + ", _EXP_DATE=" + _EXP_DATE + ", _EPC_FLAG=" + _EPC_FLAG + "]";
	}

	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		this._SessionId = sessionId;
	}

	public void setTXTOKEN(String TXTOKEN) {
		this._TXTOKEN = TXTOKEN;
	}

	public String getTXTOKEN() {
		return _TXTOKEN;
	}

	public String getCAPTCHA() {
		return _CAPTCHA;
	}

	public void setCAPTCHA(String CAPTCHA) {
		this._CAPTCHA = CAPTCHA;
	}
	
	public String getFGTXWAY() {
		return _FGTXWAY;
	}

	public void setFGTXWAY(String fgtxway) {
		this._FGTXWAY = fgtxway;
	}

	public String getCMPASSWORD() {
		return _CMPASSWORD;
	}

	public void setCMPASSWORD(String CMPASSWORD) {
		this._CMPASSWORD = CMPASSWORD;
	}

	public String getPINNEW() {
		return _PINNEW;
	}

	public void setPINNEW(String pinnew) {
		this._PINNEW = pinnew;
	}

	public String getTYPENAME() {
		return _TYPENAME;
	}

	public void setTYPENAME(String typename) {
		_TYPENAME = typename;
	}

	public String getCARDNUM() {
		return _CARDNUM;
	}

	public void setCARDNUM(String cardnum) {
		_CARDNUM = cardnum;
	}

	public String getEXP_DATE() {
		return _EXP_DATE;
	}

	public void setEXP_DATE(String exp_date) {
		_EXP_DATE = exp_date;
	}		
		
	public String getEPC_FLAG() {
		return _EPC_FLAG;
	}

	public void setEPC_FLAG(String epc_flag) {
		_EPC_FLAG = epc_flag;
	}

}
