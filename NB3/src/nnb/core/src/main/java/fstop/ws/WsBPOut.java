package fstop.ws;

public class WsBPOut {
	
	private String _MsgCode;	//訊息代碼
	private String _MsgName;	//訊息名稱
	private String _CMTXIME;	//交易時間
	private String _OUTACN;		//轉出帳號
	private String _INTSACN;	//銷帳編號
	private String _AMOUNT;		//繳費金額
	private String _O_TOTBAL;	//轉出帳號帳上餘額
	private String _O_AVLBAL;	//轉出帳號可用餘額
	private String _CMDATE;		//轉帳日期
	public String getMsgCode() {
		return _MsgCode;
	}
	public void setMsgCode(String msgCode) {
		_MsgCode = msgCode;
	}
	public String getMsgName() {
		return _MsgName;
	}
	public void setMsgName(String msgName) {
		_MsgName = msgName;
	}
	public String getCMTXIME() {
		return _CMTXIME;
	}
	public void setCMTXIME(String CMTXIME) {
		_CMTXIME = CMTXIME;
	}
	public String getOUTACN() {
		return _OUTACN;
	}
	public void setOUTACN(String OUTACN) {
		_OUTACN = OUTACN;
	}
	public String getINTSACN() {
		return _INTSACN;
	}
	public void setINTSACN(String INTSACN) {
		_INTSACN = INTSACN;
	}
	public String getAMOUNT() {
		return _AMOUNT;
	}
	public void setAMOUNT(String amount) {
		_AMOUNT = amount;
	}
	public String getO_TOTBAL() {
		return _O_TOTBAL;
	}
	public void setO_TOTBAL(String O_TOTBAL) {
		_O_TOTBAL = O_TOTBAL;
	}
	public String getO_AVLBAL() {
		return _O_AVLBAL;
	}
	public void setO_AVLBAL(String O_AVLBAL) {
		_O_AVLBAL = O_AVLBAL;
	}		
	public String getCMDATE() {
		return _CMDATE;
	}
	public void setCMDATE(String CMDATE) {
		_CMDATE = CMDATE;
	}
}
