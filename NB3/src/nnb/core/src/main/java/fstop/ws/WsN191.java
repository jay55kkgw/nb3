package fstop.ws;

public interface WsN191{

	public CmList getAcnoList(CmUser params);
	
	public WsN191Out action(WsN191In params);
}