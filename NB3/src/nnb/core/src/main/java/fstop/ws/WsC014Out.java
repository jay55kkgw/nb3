package fstop.ws;

public class WsC014Out {

	private String _MsgCode;			// 訊息代碼
	private String _MsgName;			// 訊息名稱	
	private String _DATE;				// 日期	
	private String _CUSIDN;				// 身分證號/統一編號
	private String _FUNDLNAME;			// 基金名稱
	private String _CREDITNO;			// 信託帳號
	private String _CRY;				// 幣別	
	private String _TRANSCRY;			// 基金計價幣別
	private String _TRADEDATE;			// 日期
	private String _FUNDFLAG;			// 是否未分配
	private String _INFUNDLNAME;		// 轉入基金名稱
	private String _FUNDAMT;			// 信託金額
	private String _PRICE1;				// 價格	
	private String _PRICE2;				// 轉入價格
	private String _EXRATE;				// 匯率
	private String _EXAMT;				// 外幣金額
	private String _UNIT;				// 單位數
	private String _OKCOUNT;			// 扣款成功次數
	private String _TXUNIT;				// 轉入單位數	
	private String _DTXUNIT;			// 除權後信託單位數
	private String _AMT1;				// 轉出金額
	private String _AMT3;				// 申購金額
	private String _FCA2;				// 手續費
	private String _AMT4;				// 每單位分配金額
	private String _AMT5;				// 代扣稅款	
	private String _AMT6;				// 單位數乘碼
	private String _INTRANSCRY;			// 轉入計價幣別
	private String _AMT7;				// 給付淨額
	private String _TRUSTCURRENCY;		// 轉入信託幣別
	private String _SHORTTRADEFEE;		// 短線費用
	private String _NHITAX;				// 代扣健保費

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getDATE() {
		return _DATE;
	}

	public void setDATE(String DATE) {
		this._DATE = DATE;
	}
	
	public String getCUSIDN() {
		return _CUSIDN;
	}

	public void setCUSIDN(String CUSIDN) {
		this._CUSIDN = CUSIDN;
	}
	
	public String getFUNDLNAME() {
		return _FUNDLNAME;
	}

	public void setFUNDLNAME(String FUNDLNAME) {
		this._FUNDLNAME = FUNDLNAME;
	}
	
	public String getCREDITNO() {
		return _CREDITNO;
	}

	public void setCREDITNO(String CREDITNO) {
		this._CREDITNO = CREDITNO;
	}
	
	public String getCRY() {
		return _CRY;
	}

	public void setCRY(String CRY) {
		this._CRY = CRY;
	}
	
	public String getTRANSCRY() {
		return _TRANSCRY;
	}

	public void setTRANSCRY(String TRANSCRY) {
		this._TRANSCRY = TRANSCRY;
	}

	public String getTRADEDATE() {
		return _TRADEDATE;
	}

	public void setTRADEDATE(String TRADEDATE) {
		this._TRADEDATE = TRADEDATE;
	}
	
	public String getFUNDFLAG() {
		return _FUNDFLAG;
	}

	public void setFUNDFLAG(String FUNDFLAG) {
		this._FUNDFLAG = FUNDFLAG;
	}
	
	public String getINFUNDLNAME() {
		return _INFUNDLNAME;
	}

	public void setINFUNDLNAME(String INFUNDLNAME) {
		this._INFUNDLNAME = INFUNDLNAME;
	}
	
	public String getFUNDAMT() {
		return _FUNDAMT;
	}

	public void setFUNDAMT(String FUNDAMT) {
		this._FUNDAMT = FUNDAMT;
	}
	
	public String getPRICE1() {
		return _PRICE1;
	}

	public void setPRICE1(String PRICE1) {
		this._PRICE1 = PRICE1;
	}
	
	public String getPRICE2() {
		return _PRICE2;
	}

	public void setPRICE2(String PRICE2) {
		this._PRICE2 = PRICE2;
	}
	
	public String getEXRATE() {
		return _EXRATE;
	}

	public void setEXRATE(String EXRATE) {
		this._EXRATE = EXRATE;
	}
	
	public String getEXAMT() {
		return _EXAMT;
	}

	public void setEXAMT(String EXAMT) {
		this._EXAMT = EXAMT;
	}
	
	public String getUNIT() {
		return _UNIT;
	}

	public void setUNIT(String UNIT) {
		this._UNIT = UNIT;
	}
	
	public String getOKCOUNT() {
		return _OKCOUNT;
	}

	public void setOKCOUNT(String OKCOUNT) {
		this._OKCOUNT = OKCOUNT;
	}
	
	public String getTXUNIT() {
		return _TXUNIT;
	}

	public void setTXUNIT(String TXUNIT) {
		this._TXUNIT = TXUNIT;
	}
	
	public String getDTXUNIT() {
		return _DTXUNIT;
	}

	public void setDTXUNIT(String DTXUNIT) {
		this._DTXUNIT = DTXUNIT;
	}
	
	public String getAMT1() {
		return _AMT1;
	}

	public void setAMT1(String AMT1) {
		this._AMT1 = AMT1;
	}
	
	public String getAMT3() {
		return _AMT3;
	}

	public void setAMT3(String AMT3) {
		this._AMT3 = AMT3;
	}
	
	public String getFCA2() {
		return _FCA2;
	}

	public void setFCA2(String _FCA2) {
		this._FCA2 = _FCA2;
	}
	
	public String getAMT4() {
		return _AMT4;
	}

	public void setAMT4(String AMT4) {
		this._AMT4 = AMT4;
	}
	
	public String getAMT5() {
		return _AMT5;
	}

	public void setAMT5(String AMT5) {
		this._AMT5 = AMT5;
	}
	
	public String getAMT6() {
		return _AMT6;
	}

	public void setAMT6(String AMT6) {
		this._AMT6 = AMT6;
	}
	
	public String getINTRANSCRY() {
		return _INTRANSCRY;
	}

	public void setINTRANSCRY(String INTRANSCRY) {
		this._INTRANSCRY = INTRANSCRY;
	}
	
	public String getAMT7() {
		return _AMT7;
	}

	public void setAMT7(String AMT7) {
		this._AMT7 = AMT7;
	}
	
	public String getTRUSTCURRENCY() {
		return _TRUSTCURRENCY;
	}

	public void setTRUSTCURRENCY(String TRUSTCURRENCY) {
		this._TRUSTCURRENCY = TRUSTCURRENCY;
	}
	
	public String getSHORTTRADEFEE() {
		return _SHORTTRADEFEE;
	}

	public void setSHORTTRADEFEE(String SHORTTRADEFEE) {
		this._SHORTTRADEFEE = SHORTTRADEFEE;
	}
	
	public String getNHITAX() {
		return _NHITAX;
	}

	public void setNHITAX(String NHITAX) {
		this._NHITAX = NHITAX;
	}
}
