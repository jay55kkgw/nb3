package fstop.ws;

public class WsBPDataRow {

	private String _ORGNAME;	//收款單位名稱
	private String _BILLNAME;	//單位名稱
	private String _CANCELNO;	//銷帳編號
	private String _AMOUNT;	//繳費金額
	public String getORGNAME() {
		return _ORGNAME;
	}
	public void setORGNAME(String orgname) {
		_ORGNAME = orgname;
	}
	public String getBILLNAME() {
		return _BILLNAME;
	}
	public void setBILLNAME(String billname) {
		_BILLNAME = billname;
	}
	public String getCANCELNO() {
		return _CANCELNO;
	}
	public void setCANCELNO(String cancelno) {
		_CANCELNO = cancelno;
	}
	public String getAMOUNT() {
		return _AMOUNT;
	}
	public void setAMOUNT(String amount) {
		_AMOUNT = amount;
	}

	
}
