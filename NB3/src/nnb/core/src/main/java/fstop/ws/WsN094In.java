package fstop.ws;

public class WsN094In {

	private String _SessionId;

	private String _TXTOKEN;

	private String _FGTXWAY;

	private String _CMPASSWORD;

	private String _PINNEW;

	private String _OTPKEY;

	private String _SVACN;

	private String _ACN;

	private String _TRNAMT;

	private String _CAPTCHA;
	
	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		this._SessionId = sessionId;
	}

	public void setTXTOKEN(String TXTOKEN) {
		this._TXTOKEN = TXTOKEN;
	}

	public String getTXTOKEN() {
		return _TXTOKEN;
	}

	public String getACN() {
		return _ACN;
	}

	public void setACN(String ACN) {
		this._ACN = ACN;
	}

	public String getFGTXWAY() {
		return _FGTXWAY;
	}

	public void setFGTXWAY(String fgtxway) {
		this._FGTXWAY = fgtxway;
	}

	public String getCMPASSWORD() {
		return _CMPASSWORD;
	}

	public void setCMPASSWORD(String CMPASSWORD) {
		this._CMPASSWORD = CMPASSWORD;
	}

	public String getPINNEW() {
		return _PINNEW;
	}

	public void setPINNEW(String pinnew) {
		this._PINNEW = pinnew;
	}

	public String getOTPKEY() {
		return _OTPKEY;
	}

	public void setOTPKEY(String otpkey) {
		this._OTPKEY = otpkey;
	}

	public String getSVACN() {
		return _SVACN;
	}

	public void setSVACN(String SVACN) {
		this._SVACN = SVACN;
	}

	public String getTRNAMT() {
		return _TRNAMT;
	}

	public void setTRNAMT(String TRNAMT) {
		this._TRNAMT = TRNAMT;
	}

	public String getCAPTCHA() {
		return _CAPTCHA;
	}

	public void setCAPTCHA(String CAPTCHA) {
		this._CAPTCHA = CAPTCHA;
	}

}
