package fstop.ws;

import java.util.Locale;

public class WsN072In {

	private String _SessionId;

	private String _TXTOKEN;

	private String _FGTXWAY;

	private String _CMPWD;

	private String _PINNEW;

	private String _OTPKEY;

	private String _FLAG;

	private String _ACN;

	private String _CARDNUM1;

	private String _CARDNUM2;

	private String _AMOUNT;

	private String _CMTRMEMO;

	private String _CMTRMAIL;

	private String _CMMAILMEMO;

	private String _CAPTCHA;
	

	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		this._SessionId = sessionId;
	}

	public void setTXTOKEN(String TXTOKEN) {
		this._TXTOKEN = TXTOKEN;
	}

	public String getTXTOKEN() {
		return _TXTOKEN;
	}

	public String getACN() {
		return _ACN;
	}

	public void setACN(String ACN) {
		this._ACN = ACN;
	}

	public String getCARDNUM1() {
		return _CARDNUM1.toUpperCase(Locale.ENGLISH);
	}

	public void setCARDNUM1(String CARDNUM1) {
		this._CARDNUM1 = CARDNUM1;
	}

	public String getCARDNUM2() {
		return _CARDNUM2.toUpperCase(Locale.ENGLISH);
	}

	public void setCARDNUM2(String CARDNUM2) {
		this._CARDNUM2 = CARDNUM2;
	}

	public String getFGTXWAY() {
		return _FGTXWAY;
	}

	public void setFGTXWAY(String fgtxway) {
		this._FGTXWAY = fgtxway;
	}

	public String getCMPWD() {
		return _CMPWD;
	}

	public void setCMPWD(String CMPWD) {
		this._CMPWD = CMPWD;
	}

	public String getPINNEW() {
		return _PINNEW;
	}

	public void setPINNEW(String pinnew) {
		this._PINNEW = pinnew;
	}

	public String getOTPKEY() {
		return _OTPKEY;
	}

	public void setOTPKEY(String otpkey) {
		this._OTPKEY = otpkey;
	}

	public String getFLAG() {
		return _FLAG;
	}

	public void setFLAG(String FLAG) {
		this._FLAG = FLAG;
	}

	public String getCMTRMEMO() {
		return _CMTRMEMO;
	}

	public void setCMTRMEMO(String cmtrmemo) {
		this._CMTRMEMO = cmtrmemo;
	}

	public String getCMTRMAIL() {
		return _CMTRMAIL;
	}

	public void setCMTRMAIL(String cmtrmail) {
		this._CMTRMAIL = cmtrmail;
	}

	public String getCMMAILMEMO() {
		return _CMMAILMEMO;
	}

	public void setCMMAILMEMO(String cmmailmemo) {
		this._CMMAILMEMO = cmmailmemo;
	}

	public String getAMOUNT() {
		return _AMOUNT;
	}

	public void setAMOUNT(String amount) {
		this._AMOUNT = amount;
	}

	public String getCAPTCHA() {
		return _CAPTCHA;
	}

	public void setCAPTCHA(String captcha) {
		this._CAPTCHA = captcha;
	}


}
