package fstop.ws;

public class WsC114Out {

	private String _MsgCode;

	private String _MsgName;

	private String _CMQTIME;

	private String _CREDITNO;

	private String _FUNDLNAME;

	private String _BILLSENDMODE;

	private String _FUNDCUR;

	private String _FUNDAMT;

	private String _UNIT;

	private String _RESTOREDAY;

	private String _FUNDACN;

	private String _STOPNOTICE;  //是否小額終止的通知

	
	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgcode) {
		this._MsgCode = msgcode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgname) {
		this._MsgName = msgname;
	}

	public String getCMQTIME() {
		return _CMQTIME;
	}

	public void setCMQTIME(String cmqtime) {
		this._CMQTIME = cmqtime;
	}

	public String getCREDITNO() {
		return _CREDITNO;
	}

	public void setCREDITNO(String creditno) {
		this._CREDITNO = creditno;
	}

	public String getFUNDLNAME() {
		return _FUNDLNAME;
	}

	public void setFUNDLNAME(String fundlname) {
		this._FUNDLNAME = fundlname;
	}

	public String getBILLSENDMODE() {
		return _BILLSENDMODE;
	}

	public void setBILLSENDMODE(String billsendmode) {
		this._BILLSENDMODE = billsendmode;
	}

	public String getFUNDCUR() {
		return _FUNDCUR;
	}

	public void setFUNDCUR(String fundcur) {
		this._FUNDCUR = fundcur;
	}

	public String getFUNDAMT() {
		return _FUNDAMT;
	}

	public void setFUNDAMT(String fundamt) {
		this._FUNDAMT = fundamt;
	}

	public String getUNIT() {
		return _UNIT;
	}

	public void setUNIT(String unit) {
		this._UNIT = unit;
	}

	public String getRESTOREDAY() {
		return _RESTOREDAY;
	}

	public void setRESTOREDAY(String restoreday) {
		this._RESTOREDAY = restoreday;
	}

	public String getFUNDACN() {
		return _FUNDACN;
	}

	public void setFUNDACN(String fundacn) {
		this._FUNDACN = fundacn;
	}

	public String getSTOPNOTICE() {
		return _STOPNOTICE;
	}

	public void setSTOPNOTICE(String stopnotice) {
		this._STOPNOTICE = stopnotice;
	}
}
