package fstop.ws;

public class WsN374In {

	private String _SessionId;// 身份識別
	private String _N950PASSWORD;// 交易密碼
	private String _TXTOKEN;// 防止重送代碼
	private String _FGTXWAY;// 密碼類別
	private String _CMPASSWORD;// 交易密碼
	private String _PINNEW;// 交易密碼SHA1值
	private String _OTPKEY;// OTP動態密碼
	private String _CAPTCHA;// 圖形驗證碼
	private String _CUSIDN;// 統一編號
	private String _CREDITNO;// 信託帳號
	private String _TRANSCODE;// 基金代碼
	private String _FUNDAMT;// 信託金額
	private String _TRADEDATE;// 生效日期
	private String _UNIT;// 單位數
	private String _BILLSENDMODE;// 贖回方式
	private String _FUNDACN;// 入帳帳號
	private String _CRY;// 信託幣別
	
	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String SessionId) {
		_SessionId = SessionId;
	}

	public String getN950PASSWORD() {
		return _N950PASSWORD;
	}

	public void setN950PASSWORD(String N950PASSWORD) {
		_N950PASSWORD = N950PASSWORD;
	}

	public String getTXTOKEN() {
		return _TXTOKEN;
	}

	public void setTXTOKEN(String TXTOKEN) {
		_TXTOKEN = TXTOKEN;
	}

	public String getFGTXWAY() {
		return _FGTXWAY;
	}

	public void setFGTXWAY(String FGTXWAY) {
		_FGTXWAY = FGTXWAY;
	}

	public String getCMPASSWORD() {
		return _CMPASSWORD;
	}

	public void setCMPASSWORD(String CMPASSWORD) {
		_CMPASSWORD = CMPASSWORD;
	}

	public String getPINNEW() {
		return _PINNEW;
	}

	public void setPINNEW(String PINNEW) {
		_PINNEW = PINNEW;
	}

	public String getOTPKEY() {
		return _OTPKEY;
	}

	public void setOTPKEY(String OTPKEY) {
		_OTPKEY = OTPKEY;
	}

	public String getCAPTCHA() {
		return _CAPTCHA;
	}

	public void setCAPTCHA(String CAPTCHA) {
		_CAPTCHA = CAPTCHA;
	}

	public String getCUSIDN() {
		return _CUSIDN;
	}

	public void setCUSIDN(String CUSIDN) {
		_CUSIDN = CUSIDN;
	}

	public String getCREDITNO() {
		return _CREDITNO;
	}

	public void setCREDITNO(String CREDITNO) {
		_CREDITNO = CREDITNO;
	}

	public String getTRANSCODE() {
		return _TRANSCODE;
	}

	public void setTRANSCODE(String TRANSCODE) {
		_TRANSCODE = TRANSCODE;
	}

	public String getFUNDAMT() {
		return _FUNDAMT;
	}

	public void setFUNDAMT(String FUNDAMT) {
		_FUNDAMT = FUNDAMT;
	}

	public String getUNIT() {
		return _UNIT;
	}

	public void setUNIT(String UNIT) {
		_UNIT = UNIT;
	}

	public String getBILLSENDMODE() {
		return _BILLSENDMODE;
	}

	public void setBILLSENDMODE(String BILLSENDMODE) {
		_BILLSENDMODE = BILLSENDMODE;
	}

	public String getFUNDACN() {
		return _FUNDACN;
	}

	public void setFUNDACN(String FUNDACN) {
		_FUNDACN = FUNDACN;
	}

	public String getTRADEDATE() {
		return _TRADEDATE;
	}

	public void setTRADEDATE(String tradedate) {
		_TRADEDATE = tradedate;
	}

	public String getCRY() {
		return _CRY;
	}

	public void setCRY(String CRY) {
		_CRY = CRY;
	}
}
