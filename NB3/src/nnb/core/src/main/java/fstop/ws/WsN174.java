package fstop.ws;

public interface WsN174{

	public CmFxAcno getFxDepAgreeAcnoList(CmUser params);//取得外幣轉帳之約定/常用非約定帳號
	
	public WsN174Out action(WsN174In params);//執行外幣定存
}