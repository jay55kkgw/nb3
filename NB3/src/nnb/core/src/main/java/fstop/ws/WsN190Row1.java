package fstop.ws;

public class WsN190Row1 {

	private String _ACN;//	帳號
	
	private String _TSFBAL;//	可用餘額(公克)
	
	private String _GDBAL;//	帳戶餘額(公克)
	
	private String _MKBAL;//	參考市值(元)

	private String _AVGCOST;//	參考平均成本(元)
	
	public String getACN() {
		return _ACN;
	}

	public void setACN(String acn) {
		this._ACN = acn;
	}

	public String getTSFBAL() {
		return _TSFBAL;
	}

	public void setTSFBAL(String tsfbal) {
		this._TSFBAL = tsfbal;
	}

	public String getGDBAL() {
		return _GDBAL;
	}

	public void setGDBAL(String gdbal) {
		this._GDBAL = gdbal;
	}

	public String getMKBAL() {
		return _MKBAL;
	}

	public void setMKBAL(String mkbal) {
		this._MKBAL = mkbal;
	}

	public String getAVGCOST() {
		return _AVGCOST;
	}

	public void setAVGCOST(String avgcost) {
		this._AVGCOST = avgcost;
	}
}
