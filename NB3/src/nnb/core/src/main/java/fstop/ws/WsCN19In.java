package fstop.ws;

import java.io.Serializable;

public class WsCN19In implements Serializable {
	private static final long serialVersionUID = 5611101113710760427L;
	private String sessionId;	//SessionId
	private String PPSYNC;
	private String PINNEW;
	private String CUSIDN;
	private String TRANSEQ;
	private String ISSUER;
	private String ACNNO;
	private String ICDTTM;
	private String ICSEQ;
	private String ICMEMO;
	private String TAC;
	private String TRMID;

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getPPSYNC() {
		return PPSYNC;
	}

	public void setPPSYNC(String pPSYNC) {
		PPSYNC = pPSYNC;
	}

	public String getPINNEW() {
		return PINNEW;
	}

	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}

	public String getTRANSEQ() {
		return TRANSEQ;
	}

	public void setTRANSEQ(String tRANSEQ) {
		TRANSEQ = tRANSEQ;
	}

	public String getISSUER() {
		return ISSUER;
	}

	public void setISSUER(String iSSUER) {
		ISSUER = iSSUER;
	}

	public String getACNNO() {
		return ACNNO;
	}

	public void setACNNO(String aCNNO) {
		ACNNO = aCNNO;
	}

	public String getICDTTM() {
		return ICDTTM;
	}

	public void setICDTTM(String iCDTTM) {
		ICDTTM = iCDTTM;
	}

	public String getICSEQ() {
		return ICSEQ;
	}

	public void setICSEQ(String iCSEQ) {
		ICSEQ = iCSEQ;
	}

	public String getICMEMO() {
		return ICMEMO;
	}

	public void setICMEMO(String iCMEMO) {
		ICMEMO = iCMEMO;
	}

	public String getTAC() {
		return TAC;
	}

	public void setTAC(String tAC) {
		TAC = tAC;
	}

	public String getTRMID() {
		return TRMID;
	}

	public void setTRMID(String tRMID) {
		TRMID = tRMID;
	} 
	
}
