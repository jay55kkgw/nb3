package fstop.ws;

/**
 * @author user
 * 
 */
public class WsC116Row {

	private String _CREDITNO;	//信託帳號
	private String _PAYTAG;		//扣款標的代號
	private String _FUNDLNAME;	//扣款標的名稱
	private String _AC202;		//類別
	private String _AC202NO;	//類別代號
	private String _FUNDCUR;	//信託金額幣別(英文)
	private String _FUNDCURNAME;//信託金額幣別(中文)
	private String _FUNDAMT;	//信託金額
	private String _PAYCUR;		//每次定額申購金額＋不定額基準申購金額幣別(英文)
	private String _PAYCURNAME;	//每次定額申購金額＋不定額基準申購金額幣別(中文)
	private String _PAYAMT;		//每次定額申購金額＋不定額基準申購金額
	private String _STOPPROF;	//停利點
	private String _STOPLOSS;	//停損點
	private String _MIP;		//定期不定額註記
	
	public String getCREDITNO() {
		return _CREDITNO;
	}

	public void setCREDITNO(String CREDITNO) {
		this._CREDITNO = CREDITNO;
	}
	
	public String getPAYTAG() {
		return _PAYTAG;
	}

	public void setPAYTAG(String PAYTAG) {
		this._PAYTAG = PAYTAG;
	}

	public String getFUNDLNAME() {
		return _FUNDLNAME;
	}

	public void setFUNDLNAME(String FUNDLNAME) {
		this._FUNDLNAME = FUNDLNAME;
	}

	public String getAC202() {
		return _AC202;
	}

	public void setAC202(String AC202) {
		this._AC202 = AC202;
	}

	public String getAC202NO() {
		return _AC202NO;
	}

	public void setAC202NO(String AC202NO) {
		this._AC202NO = AC202NO;
	}
	
	public String getFUNDCUR() {
		return _FUNDCUR;
	}

	public void setFUNDCUR(String FUNDCUR) {
		this._FUNDCUR = FUNDCUR;
	}

	public String getFUNDCURNAME() {
		return _FUNDCURNAME;
	}

	public void setFUNDCURNAME(String FUNDCURNAME) {
		this._FUNDCURNAME = FUNDCURNAME;
	}
	
	public String getFUNDAMT() {
		return _FUNDAMT;
	}

	public void setFUNDAMT(String FUNDAMT) {
		this._FUNDAMT = FUNDAMT;
	}

	public String getPAYCUR() {
		return _PAYCUR;
	}

	public void setPAYCUR(String PAYCUR) {
		this._PAYCUR = PAYCUR;
	}

	public String getPAYCURNAME() {
		return _PAYCURNAME;
	}

	public void setPAYCURNAME(String PAYCURNAME) {
		this._PAYCURNAME = PAYCURNAME;
	}
	
	public String getPAYAMT() {
		return _PAYAMT;
	}

	public void setPAYAMT(String PAYAMT) {
		this._PAYAMT = PAYAMT;
	}

	public String getSTOPPROF() {
		return _STOPPROF;
	}

	public void setSTOPPROF(String STOPPROF) {
		this._STOPPROF = STOPPROF;
	}
	
	public String getSTOPLOSS() {
		return _STOPLOSS;
	}

	public void setSTOPLOSS(String STOPLOSS) {
		this._STOPLOSS = STOPLOSS;
	}
	
	public String getMIP() {
		return _MIP;
	}

	public void setMIP(String MIP) {
		this._MIP = MIP;
	}
}
