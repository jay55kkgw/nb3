package fstop.ws;

public class WsN175Out {

	private String MsgCode;//	訊息代碼
	private String MsgName;//	訊息名稱
	private String AMT;//		存單金額
	private String CUID;//		幣別
	private String DPISDT;//	存起日
	private String DUEDAT;//	到期日
	private String FDPACN;//	帳號
	private String FDPNUM;//	存單號碼
	private String INT;//		利息
	private String INTMTH;//	計息方式
	private String TAX;//		所得稅
	private String PAIAFTX;//	稅後本息
	private String NHITAX;//	健保費
	private String CMTRMEMO;//	交易備註
	private String CMQTIME;//	交易時間
	public String getMsgCode() {
		return MsgCode;
	}
	public void setMsgCode(String msgCode) {
		MsgCode = msgCode;
	}
	public String getMsgName() {
		return MsgName;
	}
	public void setMsgName(String msgName) {
		MsgName = msgName;
	}
	public String getAMT() {
		return AMT;
	}
	public void setAMT(String aMT) {
		AMT = aMT;
	}
	public String getCUID() {
		return CUID;
	}
	public void setCUID(String cUID) {
		CUID = cUID;
	}
	public String getDPISDT() {
		return DPISDT;
	}
	public void setDPISDT(String dPISDT) {
		DPISDT = dPISDT;
	}
	public String getDUEDAT() {
		return DUEDAT;
	}
	public void setDUEDAT(String dUEDAT) {
		DUEDAT = dUEDAT;
	}
	public String getFDPACN() {
		return FDPACN;
	}
	public void setFDPACN(String fDPACN) {
		FDPACN = fDPACN;
	}
	public String getFDPNUM() {
		return FDPNUM;
	}
	public void setFDPNUM(String fDPNUM) {
		FDPNUM = fDPNUM;
	}
	public String getINT() {
		return INT;
	}
	public void setINT(String iNT) {
		INT = iNT;
	}
	public String getINTMTH() {
		return INTMTH;
	}
	public void setINTMTH(String iNTMTH) {
		INTMTH = iNTMTH;
	}
	public String getTAX() {
		return TAX;
	}
	public void setTAX(String tAX) {
		TAX = tAX;
	}
	public String getPAIAFTX() {
		return PAIAFTX;
	}
	public void setPAIAFTX(String pAIAFTX) {
		PAIAFTX = pAIAFTX;
	}
	public String getNHITAX() {
		return NHITAX;
	}
	public void setNHITAX(String nHITAX) {
		NHITAX = nHITAX;
	}
	public String getCMTRMEMO() {
		return CMTRMEMO;
	}
	public void setCMTRMEMO(String cMTRMEMO) {
		CMTRMEMO = cMTRMEMO;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}

	
}
