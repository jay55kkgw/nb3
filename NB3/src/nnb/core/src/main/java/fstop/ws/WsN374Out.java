package fstop.ws;

public class WsN374Out {

	private String _MsgCode;// 訊息代碼
	private String _MsgName;// 訊息名稱
	private String _CMQTIME;// 統一編號
	private String _CREDITNO;// 信託號碼
	private String _UNIT;// 轉出單位數
	private String _FUNDACN;// 轉出信託金額幣別
	private String _RESTOREDAY;// 轉出信託金額
	private String _FUNDCUR;// 申購金額
	private String _ADCCYNAME;// 基金公司外加手續費
	private String _FUNDAMT;// 補數手續費
	private String _STOPNOTICE;  //是否小額終止的通知

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		_MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		_MsgName = msgName;
	}

	public String getCMQTIME() {
		return _CMQTIME;
	}

	public void setCMQTIME(String CMQTIME) {
		_CMQTIME = CMQTIME;
	}

	public String getCREDITNO() {
		return _CREDITNO;
	}

	public void setCREDITNO(String creditno) {
		_CREDITNO = creditno;
	}

	public String getUNIT() {
		return _UNIT;
	}

	public void setUNIT(String unit) {
		_UNIT = unit;
	}

	public String getFUNDACN() {
		return _FUNDACN;
	}

	public void setFUNDACN(String FUNDACN) {
		_FUNDACN = FUNDACN;
	}

	public String getRESTOREDAY() {
		return _RESTOREDAY;
	}

	public void setRESTOREDAY(String RESTOREDAY) {
		_RESTOREDAY = RESTOREDAY;
	}

	public String getFUNDCUR() {
		return _FUNDCUR;
	}

	public void setFUNDCUR(String FUNDCUR) {
		_FUNDCUR = FUNDCUR;
	}

	public String getADCCYNAME() {
		return _ADCCYNAME;
	}

	public void setADCCYNAME(String ADCCYNAME) {
		_ADCCYNAME = ADCCYNAME;
	}

	public String getFUNDAMT() {
		return _FUNDAMT;
	}

	public void setFUNDAMT(String FUNDAMT) {
		_FUNDAMT = FUNDAMT;
	}

	public String getSTOPNOTICE() {
		return _STOPNOTICE;
	}

	public void setSTOPNOTICE(String stopnotice) {
		this._STOPNOTICE = stopnotice;
	}
	
}
