package fstop.ws;

public class WsN373In {

	private String _SessionId;// 身份識別
	private String _N950PASSWORD;// 交易密碼
	private String _TXTOKEN;// 防止重送代碼
	private String _FGTXWAY;// 密碼類別
	private String _CMPASSWORD;// 交易密碼
	private String _PINNEW;// 交易密碼SHA1值
	private String _OTPKEY;// OTP動態密碼
	private String _CAPTCHA;// 圖形驗證碼
	private String _BILLSENDMODE;// 轉換方式
	private String _CUSIDN;// 統一編號
	private String _CREDITNO;// 信託帳號
	private String _TRANSCODE;// 基金代碼
	private String _FUNDAMT;// 信託金額
	private String _TRADEDATE;// 生效日期
	private String _INTRANSCODE;// 轉入基金代碼
	private String _UNIT;// 轉出單位數
	private String _FCA1;// 補收手續費
	private String _AMT3;// 申購金額
	private String _FCA2;// 手續費
	private String _AMT5;// 扣款金額
	private String _CRY;// 幣別代碼
	private String _SSLTXNO;// SSL交易序號

	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		this._SessionId = sessionId;
	}

	public String getN950PASSWORD() {
		return _N950PASSWORD;
	}

	public void setN950PASSWORD(String n950password) {
		this._N950PASSWORD = n950password;
	}

	public String getTXTOKEN() {
		return _TXTOKEN;
	}

	public void setTXTOKEN(String TXTOKEN) {
		this._TXTOKEN = TXTOKEN;
	}

	public String getFGTXWAY() {
		return _FGTXWAY;
	}

	public void setFGTXWAY(String FGTXWAY) {
		this._FGTXWAY = FGTXWAY;
	}

	public String getCMPASSWORD() {
		return _CMPASSWORD;
	}

	public void setCMPASSWORD(String CMPASSWORD) {
		this._CMPASSWORD = CMPASSWORD;
	}

	public String getPINNEW() {
		return _PINNEW;
	}

	public void setPINNEW(String PINNEW) {
		this._PINNEW = PINNEW;
	}

	public String getOTPKEY() {
		return _OTPKEY;
	}

	public void setOTPKEY(String OTPKEY) {
		this._OTPKEY = OTPKEY;
	}

	public String getCAPTCHA() {
		return _CAPTCHA;
	}

	public void setCAPTCHA(String CAPTCHA) {
		this._CAPTCHA = CAPTCHA;
	}

	public String getBILLSENDMODE() {
		return _BILLSENDMODE;
	}

	public void setBILLSENDMODE(String BILLSENDMODE) {
		this._BILLSENDMODE = BILLSENDMODE;
	}

	public String getCUSIDN() {
		return _CUSIDN;
	}

	public void setCUSIDN(String CUSIDN) {
		this._CUSIDN = CUSIDN;
	}

	public String getCREDITNO() {
		return _CREDITNO;
	}

	public void setCREDITNO(String CREDITNO) {
		this._CREDITNO = CREDITNO;
	}
	
	public String getTRANSCODE() {
		return _TRANSCODE;
	}

	public void setTRANSCODE(String TRANSCODE) {
		this._TRANSCODE = TRANSCODE;
	}
	
	public String getFUNDAMT() {
		return _FUNDAMT;
	}

	public void setFUNDAMT(String FUNDAMT) {
		this._FUNDAMT = FUNDAMT;
	}
	
	public String getTRADEDATE() {
		return _TRADEDATE;
	}

	public void setTRADEDATE(String TRADEDATE) {
		this._TRADEDATE = TRADEDATE;
	}
	
	public String getINTRANSCODE() {
		return _INTRANSCODE;
	}

	public void setINTRANSCODE(String INTRANSCODE) {
		this._INTRANSCODE = INTRANSCODE;
	}
	
	public String getUNIT() {
		return _UNIT;
	}

	public void setUNIT(String UNIT) {
		this._UNIT = UNIT;
	}
	
	public String getFCA1() {
		return _FCA1;
	}

	public void setFCA1(String FCA1) {
		this._FCA1 = FCA1;
	}
	
	public String getAMT3() {
		return _AMT3;
	}

	public void setAMT3(String AMT3) {
		this._AMT3 = AMT3;
	}
	
	public String getFCA2() {
		return _FCA2;
	}

	public void setFCA2(String FCA2) {
		this._FCA2 = FCA2;
	}
	
	public String getAMT5() {
		return _AMT5;
	}

	public void setAMT5(String AMT5) {
		this._AMT5 = AMT5;
	}
	
	public String getCRY() {
		return _CRY;
	}

	public void setCRY(String CRY) {
		this._CRY = CRY;
	}
	
	public String getSSLTXNO() {
		return _SSLTXNO;
	}

	public void setSSLTXNO(String SSLTXNO) {
		this._SSLTXNO = SSLTXNO;
	}
}
