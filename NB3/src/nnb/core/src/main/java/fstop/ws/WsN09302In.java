package fstop.ws;

public class WsN09302In {

	private String _SessionId;
	private String _TXTOKEN;
	private String _FGTXWAY;
	private String _CMPASSWORD;
	private String _PINNEW;
	private String _OTPKEY;
	private String _SVACN;
	private String _ACN;
	private String _DATE_06;
	private String _AMT_06_N;
	private String _PAYSTATUS_06;
	private String _DATE_16;
	private String _AMT_16_N;
	private String _PAYSTATUS_16;
	private String _DATE_26;
	private String _AMT_26_N;
	private String _PAYSTATUS_26;
	private String _CAPTCHA;
	
	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		this._SessionId = sessionId;
	}
	public String getTXTOKEN() {
		return _TXTOKEN;
	}

	public void setTXTOKEN(String TXTOKEN) {
		this._TXTOKEN = TXTOKEN;
	}

	public String getFGTXWAY() {
		return _FGTXWAY;
	}

	public void setFGTXWAY(String FGTXWAY) {
		this._FGTXWAY = FGTXWAY;
	}

	public String getCMPASSWORD() {
		return _CMPASSWORD;
	}

	public void setCMPASSWORD(String CMPASSWORD) {
		this._CMPASSWORD = CMPASSWORD;
	}

	public String getPINNEW() {
		return _PINNEW;
	}

	public void setPINNEW(String PINNEW) {
		this._PINNEW = PINNEW;
	}

	public String getOTPKEY() {
		return _OTPKEY;
	}

	public void setOTPKEY(String OTPKEY) {
		this._OTPKEY = OTPKEY;
	}

	public String getSVACN() {
		return _SVACN;
	}

	public void setSVACN(String SVACN) {
		this._SVACN = SVACN;
	}

	public String getACN() {
		return _ACN;
	}

	public void setACN(String ACN) {
		this._ACN = ACN;
	}

	public String getDATE_06() {
		return _DATE_06;
	}

	public void setDATE_06(String DATE_06) {
		this._DATE_06 = DATE_06;
	}

	public String getAMT_06_N() {
		return _AMT_06_N;
	}

	public void setAMT_06_N(String AMT_06_N) {
		this._AMT_06_N = AMT_06_N;
	}

	public String getPAYSTATUS_06() {
		return _PAYSTATUS_06;
	}

	public void setPAYSTATUS_06(String PAYSTATUS_06) {
		this._PAYSTATUS_06 = PAYSTATUS_06;
	}

	public String getDATE_16() {
		return _DATE_16;
	}

	public void setDATE_16(String DATE_16) {
		this._DATE_16 = DATE_16;
	}

	public String getAMT_16_N() {
		return _AMT_16_N;
	}

	public void setAMT_16_N(String AMT_16_N) {
		this._AMT_16_N = AMT_16_N;
	}

	public String getPAYSTATUS_16() {
		return _PAYSTATUS_16;
	}

	public void setPAYSTATUS_16(String PAYSTATUS_16) {
		this._PAYSTATUS_16 = PAYSTATUS_16;
	}

	public String getDATE_26() {
		return _DATE_26;
	}

	public void setDATE_26(String DATE_26) {
		this._DATE_26 = DATE_26;
	}

	public String getAMT_26_N() {
		return _AMT_26_N;
	}

	public void setAMT_26_N(String AMT_26_N) {
		this._AMT_26_N = AMT_26_N;
	}

	public String getPAYSTATUS_26() {
		return _PAYSTATUS_26;
	}

	public void setPAYSTATUS_26(String PAYSTATUS_26) {
		this._PAYSTATUS_26 = PAYSTATUS_26;
	}

	public String getCAPTCHA() {
		return _CAPTCHA;
	}

	public void setCAPTCHA(String captcha) {
		this._CAPTCHA = captcha;
	}

}
