package fstop.ws;

public class WsFxRemitTypeIn {

	private String _SessionId;// 身份識別
	private String _CUSTACC;// 轉出帳號
	private String _PAYCCY;// 轉出幣別
	private String _INACNO;// 轉入帳號
	private String _REMITCY;// 轉入幣別

	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		this._SessionId = sessionId;
	}

	public String getCUSTACC() {
		return _CUSTACC;
	}

	public void setCUSTACC(String custacc) {
		this._CUSTACC = custacc;
	}

	public String getPAYCCY() {
		return _PAYCCY;
	}

	public void setPAYCCY(String payccy) {
		this._PAYCCY = payccy;
	}

	public String getINACNO() {
		return _INACNO;
	}

	public void setINACNO(String inacno) {
		this._INACNO = inacno;
	}

	public String getREMITCY() {
		return _REMITCY;
	}

	public void setREMITCY(String remitcy) {
		this._REMITCY = remitcy;
	}

}
