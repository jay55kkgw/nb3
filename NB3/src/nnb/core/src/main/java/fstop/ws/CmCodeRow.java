package fstop.ws;

public class CmCodeRow {

	private String _CODEID;

	private String _CODENAME;
	
	public String getCODEID() {
		return _CODEID;
	}

	public void setCODEID(String CODEID) {
		this._CODEID = CODEID;
	}

	public String getCODENAME() {
		return _CODENAME;
	}

	public void setCODENAME(String CODENAME) {
		this._CODENAME = CODENAME;
	}

}
