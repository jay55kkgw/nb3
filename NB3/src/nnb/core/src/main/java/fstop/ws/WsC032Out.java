package fstop.ws;

public class WsC032Out {

	private String _MsgCode;// 訊息代碼
	private String _MsgName;// 訊息名稱
	private String _CUSIDN;// 統一編號
	private String _CUSNAME;// 姓名
	private String _CREDITNO;// 信託號碼
	private String _TRANSCODE;// 基金代碼
	private String _INTRANSCODE;// 轉入基金
	private String _BILLSENDMODE;// 轉換方式
	private String _UNIT;// 轉出單位數
	private String _FUNCUR;// 轉出信託金額幣別
	private String _FUNDAMT;// 轉出信託金額
	private String _AMT3;// 申購金額
	private String _FCA2;// 基金公司外加手續費
	private String _FCA1;// 補數手續費
	private String _ACN1;// 扣款帳號
	private String _AMT5;// 扣款金額
	private String _TRADEDATE;// 生效日期
	private String _SSLTXNO;// SSL交易序號
	private String _STR_1;// 轉入/銷售
	private String _STR_2;// 申購手續費分成(%)(依　台端申購金額)的說明
	private String _STR_3;// 銷售獎勵金(%)(依銷售金額/定期定額開戶數)的說明
	private String _STR_4;// 贊助或提供產品說明會及員工教育訓練的說明
	private String _STR_5;// (三)其他報酬後的文字
	private String _STR_6;// 贊助金,訓練費:金額>5000000的說明
	private String _STR_7;// 本行每年收取之通路報酬的第1點
	private String _STR_9;// 其他報酬一: 行銷贊助金額>= 1000000的說明
	private String _LINK_FH_NAME;// 基金公司名稱
	private String _LINK_FEE_01;// 基金申購手續費費率
	private String _LINK_FEE_02;// 基金申購手續費費率_銀行分成
	private String _LINK_FEE_05;// 基金經理費年率
	private String _LINK_FEE_07;// 基金經理費年率_銀行分成
	private String _LINK_SLS_08;// 銷售獎勵金(總費率)
	private String _LINK_BASE_01;// 範例試算之基礎金額
	private String _LINK_RESULT_05;// 試算出之經理費分成
	private String _LINK_RESULT_06;// 試算出之銷售獎勵金
	private String _CRYNAME;//幣別名稱
	private String _LINK_FUND_NAME;//幣別名稱
	
	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		_MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		_MsgName = msgName;
	}

	public String getCUSIDN() {
		return _CUSIDN;
	}

	public void setCUSIDN(String cusidn) {
		_CUSIDN = cusidn;
	}

	public String getCUSNAME() {
		return _CUSNAME;
	}

	public void setCUSNAME(String cusname) {
		_CUSNAME = cusname;
	}

	public String getCREDITNO() {
		return _CREDITNO;
	}

	public void setCREDITNO(String creditno) {
		_CREDITNO = creditno;
	}

	public String getTRANSCODE() {
		return _TRANSCODE;
	}

	public void setTRANSCODE(String transcode) {
		_TRANSCODE = transcode;
	}

	public String getINTRANSCODE() {
		return _INTRANSCODE;
	}

	public void setINTRANSCODE(String intranscode) {
		_INTRANSCODE = intranscode;
	}

	public String getBILLSENDMODE() {
		return _BILLSENDMODE;
	}

	public void setBILLSENDMODE(String billsendmode) {
		_BILLSENDMODE = billsendmode;
	}

	public String getUNIT() {
		return _UNIT;
	}

	public void setUNIT(String unit) {
		_UNIT = unit;
	}

	public String getFUNCUR() {
		return _FUNCUR;
	}

	public void setFUNCUR(String funcur) {
		_FUNCUR = funcur;
	}

	public String getFUNDAMT() {
		return _FUNDAMT;
	}

	public void setFUNDAMT(String fundamt) {
		_FUNDAMT = fundamt;
	}

	public String getAMT3() {
		return _AMT3;
	}

	public void setAMT3(String amt3) {
		_AMT3 = amt3;
	}

	public String getFCA2() {
		return _FCA2;
	}

	public void setFCA2(String fca2) {
		_FCA2 = fca2;
	}

	public String getFCA1() {
		return _FCA1;
	}

	public void setFCA1(String fca1) {
		_FCA1 = fca1;
	}

	public String getACN1() {
		return _ACN1;
	}

	public void setACN1(String acn1) {
		_ACN1 = acn1;
	}

	public String getAMT5() {
		return _AMT5;
	}

	public void setAMT5(String amt5) {
		_AMT5 = amt5;
	}

	public String getTRADEDATE() {
		return _TRADEDATE;
	}

	public void setTRADEDATE(String tradedate) {
		_TRADEDATE = tradedate;
	}

	public String getSSLTXNO() {
		return _SSLTXNO;
	}

	public void setSSLTXNO(String ssltxno) {
		_SSLTXNO = ssltxno;
	}

	public String getSTR_1() {
		return _STR_1;
	}

	public void setSTR_1(String str_1) {
		_STR_1 = str_1;
	}

	public String getSTR_2() {
		return _STR_2;
	}

	public void setSTR_2(String str_2) {
		_STR_2 = str_2;
	}

	public String getSTR_3() {
		return _STR_3;
	}

	public void setSTR_3(String str_3) {
		_STR_3 = str_3;
	}

	public String getSTR_4() {
		return _STR_4;
	}

	public void setSTR_4(String str_4) {
		_STR_4 = str_4;
	}

	public String getSTR_5() {
		return _STR_5;
	}

	public void setSTR_5(String str_5) {
		_STR_5 = str_5;
	}

	public String getSTR_6() {
		return _STR_6;
	}

	public void setSTR_6(String str_6) {
		_STR_6 = str_6;
	}

	public String getSTR_7() {
		return _STR_7;
	}

	public void setSTR_7(String str_7) {
		_STR_7 = str_7;
	}

	public String getSTR_9() {
		return _STR_9;
	}

	public void setSTR_9(String str_9) {
		_STR_9 = str_9;
	}

	public String getLINK_FH_NAME() {
		return _LINK_FH_NAME;
	}

	public void setLINK_FH_NAME(String link_fh_name) {
		_LINK_FH_NAME = link_fh_name;
	}

	public String getLINK_FEE_01() {
		return _LINK_FEE_01;
	}

	public void setLINK_FEE_01(String link_fee_01) {
		_LINK_FEE_01 = link_fee_01;
	}

	public String getLINK_FEE_02() {
		return _LINK_FEE_02;
	}

	public void setLINK_FEE_02(String link_fee_02) {
		_LINK_FEE_02 = link_fee_02;
	}

	public String getLINK_FEE_05() {
		return _LINK_FEE_05;
	}

	public void setLINK_FEE_05(String link_fee_05) {
		_LINK_FEE_05 = link_fee_05;
	}

	public String getLINK_FEE_07() {
		return _LINK_FEE_07;
	}

	public void setLINK_FEE_07(String link_fee_07) {
		_LINK_FEE_07 = link_fee_07;
	}

	public String getLINK_SLS_08() {
		return _LINK_SLS_08;
	}

	public void setLINK_SLS_08(String link_sls_08) {
		_LINK_SLS_08 = link_sls_08;
	}

	public String getLINK_BASE_01() {
		return _LINK_BASE_01;
	}

	public void setLINK_BASE_01(String link_base_01) {
		_LINK_BASE_01 = link_base_01;
	}

	public String getLINK_RESULT_05() {
		return _LINK_RESULT_05;
	}

	public void setLINK_RESULT_05(String link_result_05) {
		_LINK_RESULT_05 = link_result_05;
	}

	public String getLINK_RESULT_06() {
		return _LINK_RESULT_06;
	}

	public void setLINK_RESULT_06(String link_result_06) {
		_LINK_RESULT_06 = link_result_06;
	}

	public String getCRYNAME() {
		return _CRYNAME;
	}

	public void setCRYNAME(String cryname) {
		_CRYNAME = cryname;
	}

	public String getLINK_FUND_NAME() {
		return _LINK_FUND_NAME;
	}

	public void setLINK_FUND_NAME(String LINK_FUND_NAME) {
		_LINK_FUND_NAME = LINK_FUND_NAME;
	}
}
