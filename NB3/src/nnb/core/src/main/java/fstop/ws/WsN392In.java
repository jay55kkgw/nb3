package fstop.ws;

public class WsN392In {

	private String _SessionId;// 身份識別
	private String _TXTOKEN;// 防止重送代碼
	private String _CMPASSWORD;// 交易密碼
	private String _PINNEW;// 交易密碼SHA1值
	private String _CAPTCHA;// 圖形驗證碼
	private String _TRANSCODE;// 基金代碼
	private String _TRADEDATE;// 預約日期
	private String _AMT3;// 申購金額
	private String _FCA2;// 手續費
	private String _OUTACN;// 轉出帳號
	private String _CRY;// 幣別代碼
	private String _AMT5;// 扣款金額
	private String _FCAFEE;// 手續費率
	private String _SSLTXNO;// SSL交易序號

	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		this._SessionId = sessionId;
	}

	public String getTXTOKEN() {
		return _TXTOKEN;
	}

	public void setTXTOKEN(String txtoken) {
		this._TXTOKEN = txtoken;
	}

	public String getCMPASSWORD() {
		return _CMPASSWORD;
	}

	public void setCMPASSWORD(String cmpassword) {
		this._CMPASSWORD = cmpassword;
	}

	public String getPINNEW() {
		return _PINNEW;
	}

	public void setPINNEW(String pinnew) {
		this._PINNEW = pinnew;
	}

	public String getCAPTCHA() {
		return _CAPTCHA;
	}

	public void setCAPTCHA(String captcha) {
		this._CAPTCHA = captcha;
	}

	public String getTRANSCODE() {
		return _TRANSCODE;
	}

	public void setTRANSCODE(String transcode) {
		this._TRANSCODE = transcode;
	}

	public String getTRADEDATE() {
		return _TRADEDATE;
	}

	public void setTRADEDATE(String tradedate) {
		this._TRADEDATE = tradedate;
	}

	public String getAMT3() {
		return _AMT3;
	}

	public void setAMT3(String amt3) {
		this._AMT3 = amt3;
	}

	public String getFCA2() {
		return _FCA2;
	}

	public void setFCA2(String fca2) {
		this._FCA2 = fca2;
	}

	public String getOUTACN() {
		return _OUTACN;
	}

	public void setOUTACN(String outacn) {
		this._OUTACN = outacn;
	}

	public String getCRY() {
		return _CRY;
	}

	public void setCRY(String cry) {
		this._CRY = cry;
	}

	public String getAMT5() {
		return _AMT5;
	}

	public void setAMT5(String amt5) {
		this._AMT5 = amt5;
	}

	public String getFCAFEE() {
		return _FCAFEE;
	}

	public void setFCAFEE(String fcafee) {
		this._FCAFEE = fcafee;
	}

	public String getSSLTXNO() {
		return _SSLTXNO;
	}

	public void setSSLTXNO(String ssltxno) {
		this._SSLTXNO = ssltxno;
	}


}
