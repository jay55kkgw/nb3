package fstop.ws;

public interface WsLogin{
	
	public WsLoginOut action(WsLoginIn params);
	
	public WsLoginOut actionNB3(WsLoginIn params);
	
	public CmMsg refresh(CmUser params);

	public WsLoginOut login(WsLoginStatus params);
}

