package fstop.ws;

public class WsC012Row2 {

	private String _SUBCRY;// 信託金額幣別

	private String _SUBTOTAMT;// 信託金額

	private String _SUBREGCRY;// 參考市值幣別

	private String _SUBREFVALUE;// 參考市值

	private String _SUBLCYRAT;// 台幣報酬率

	private String _SUBFCYRAT;// 外幣報酬率

	public String getSUBCRY() {
		return _SUBCRY;
	}

	public void setSUBCRY(String subcry) {
		this._SUBCRY = subcry;
	}

	public String getSUBTOTAMT() {
		return _SUBTOTAMT;
	}

	public void setSUBTOTAMT(String subtotamt) {
		this._SUBTOTAMT = subtotamt;
	}

	public String getSUBREGCRY() {
		return _SUBREGCRY;
	}

	public void setSUBREGCRY(String subregcry) {
		this._SUBREGCRY = subregcry;
	}

	public String getSUBREFVALUE() {
		return _SUBREFVALUE;
	}

	public void setSUBREFVALUE(String subrefvalue) {
		this._SUBREFVALUE = subrefvalue;
	}

	public String getSUBLCYRAT() {
		return _SUBLCYRAT;
	}

	public void setSUBLCYRAT(String sublcyrat) {
		this._SUBLCYRAT = sublcyrat;
	}

	public String getSUBFCYRAT() {
		return _SUBFCYRAT;
	}

	public void setSUBFCYRAT(String subfcyrat) {
		this._SUBFCYRAT = subfcyrat;
	}

}
