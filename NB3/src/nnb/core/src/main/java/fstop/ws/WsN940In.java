package fstop.ws;

public class WsN940In {
	
	private String _SessionId;
	
	private String _OLDPIN;
	
	private String _NEWPIN;
	
	private String _RENEWPIN;
	
	private String _NEWPASSSHA1;
	
	private String _OLDPASSSHA1;

	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		_SessionId = sessionId;
	}

	public String getOLDPIN() {
		return _OLDPIN;
	}

	public void setOLDPIN(String oldpin) {
		_OLDPIN = oldpin;
	}

	public String getNEWPIN() {
		return _NEWPIN;
	}

	public void setNEWPIN(String newpin) {
		_NEWPIN = newpin;
	}

	public String getRENEWPIN() {
		return _RENEWPIN;
	}

	public void setRENEWPIN(String renewpin) {
		_RENEWPIN = renewpin;
	}

	public String getNEWPASSSHA1() {
		return _NEWPASSSHA1;
	}

	public void setNEWPASSSHA1(String newpasssha1) {
		_NEWPASSSHA1 = newpasssha1;
	}

	public String getOLDPASSSHA1() {
		return _OLDPASSSHA1;
	}

	public void setOLDPASSSHA1(String oldpasssha1) {
		_OLDPASSSHA1 = oldpasssha1;
	}
}
