package fstop.ws;

public class WsInvestAttrInSet {

	private String _SessionId;// 身份識別

	private String[] _List;// 問卷答案清單
	
	private String _TXTOKEN;

	private String _FGTXWAY;

	private String _CMPASSWORD;

	private String _PINNEW;

	private String _OTPKEY;
	
	
	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		this._SessionId = sessionId;
	}

	public String[] getList() {
		return _List;
	}

	public void setList(String[] list) {
		this._List = list;
	}

	public String getTXTOKEN() {
		return _TXTOKEN;
	}

	public void setTXTOKEN(String TXTOKEN) {
		this._TXTOKEN = TXTOKEN;
	}
	
	public String getFGTXWAY() {
		return _FGTXWAY;
	}

	public void setFGTXWAY(String fgtxway) {
		this._FGTXWAY = fgtxway;
	}
	
	public String getCMPASSWORD() {
		return _CMPASSWORD;
	}

	public void setCMPASSWORD(String cmpassword) {
		this._CMPASSWORD = cmpassword;
	}

	public String getPINNEW() {
		return _PINNEW;
	}

	public void setPINNEW(String pinnew) {
		this._PINNEW = pinnew;
	}

	public String getOTPKEY() {
		return _OTPKEY;
	}

	public void setOTPKEY(String otpkey) {
		this._OTPKEY = otpkey;
	}
	
	
}
