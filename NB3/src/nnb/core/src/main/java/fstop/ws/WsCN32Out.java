package fstop.ws;

import java.io.Serializable;

public class WsCN32Out implements Serializable {
	private static final long serialVersionUID = -2665681557275260021L;
	private String msgCode;
	private String msgName;
	private String changeDate;
	
	public String getMsgCode() {
		return msgCode;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public String getMsgName() {
		return msgName;
	}

	public void setMsgName(String msgName) {
		this.msgName = msgName;
	}

	public String getChangeDate() {
		return changeDate;
	}

	public void setChangeDate(String changeDate) {
		this.changeDate = changeDate;
	}
}