package fstop.ws;

public class WsN015Out {

	private String _MsgCode;

	private String _MsgName;

	private String _CMQTIME;

	private String _ACN;

	private String _SNTCNT;

	private String _SNTAMT;

	private String _TOTBAL;

	private String _AVLBAL;
	
	private String _QUERYNEXT;

	private String _BOQUERYNEXT;

	private WsN015Row[] _Table;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getCMQTIME() {
		return _CMQTIME;
	}

	public void setCMQTIME(String cmqtime) {
		this._CMQTIME = cmqtime;
	}

	public String getACN() {
		return _ACN;
	}

	public void setACN(String acn) {
		this._ACN = acn;
	}

	public String getSNTCNT() {
		return _SNTCNT;
	}

	public void setSNTCNT(String sntcnt) {
		this._SNTCNT = sntcnt;
	}

	public String getSNTAMT() {
		return _SNTAMT;
	}

	public void setSNTAMT(String sntamt) {
		this._SNTAMT = sntamt;
	}

	public String getTOTBAL() {
		return _TOTBAL;
	}

	public void setTOTBAL(String totbal) {
		this._TOTBAL = totbal;
	}

	public String getQUERYNEXT() {
		return _QUERYNEXT;
	}

	public void setQUERYNEXT(String querynext) {
		this._QUERYNEXT = querynext;
	}

	public String getBOQUERYNEXT() {
		return _BOQUERYNEXT;
	}

	public void setBOQUERYNEXT(String boquerynext) {
		this._BOQUERYNEXT = boquerynext;
	}

	public WsN015Row[] getTable() {
		return _Table;
	}

	public void setTable(WsN015Row[] Table) {
		this._Table = Table;
	}

	public String getAVLBAL() {
		return _AVLBAL;
	}

	public void setAVLBAL(String avlbal) {
		this._AVLBAL = avlbal;
	}

}
