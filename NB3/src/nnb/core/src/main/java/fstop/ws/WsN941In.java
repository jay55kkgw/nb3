package fstop.ws;

public class WsN941In {
	
	private String _SessionId;
	
	private String _OLDPIN;
	
	private String _NEWPIN;
	
	private String _RENEWPIN;
	
	private String _NEWTRANSPASSSHA1;
	
	private String _OLDTRANSPASSSHA1;

	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		_SessionId = sessionId;
	}

	public String getOLDPIN() {
		return _OLDPIN;
	}

	public void setOLDPIN(String oldpin) {
		_OLDPIN = oldpin;
	}

	public String getNEWPIN() {
		return _NEWPIN;
	}

	public void setNEWPIN(String newpin) {
		_NEWPIN = newpin;
	}

	public String getRENEWPIN() {
		return _RENEWPIN;
	}

	public void setRENEWPIN(String renewpin) {
		_RENEWPIN = renewpin;
	}
	
	public String getNEWTRANSPASSSHA1() {
		return _NEWTRANSPASSSHA1;
	}

	public void setNEWTRANSPASSSHA1(String newtranspasssha1) {
		_NEWTRANSPASSSHA1 = newtranspasssha1;
	}
	
	public String getOLDTRANSPASSSHA1() {
		return _OLDTRANSPASSSHA1;
	}

	public void setOLDTRANSPASSSHA1(String oldtranspasssha1) {
		_OLDTRANSPASSSHA1 = oldtranspasssha1;
	}
}
