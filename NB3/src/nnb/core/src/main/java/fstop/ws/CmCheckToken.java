package fstop.ws;

public class CmCheckToken {

	private String _MsgCode;	// 訊息代碼

	private String _MsgName;	// 訊息名稱

	private String _STATUS;		// 狀態 1:與既有的相同 0:與既有的不同

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getSTATUS() {
		return _STATUS;
	}

	public void setSTATUS(String status) {
		this._STATUS = status;
	}

}