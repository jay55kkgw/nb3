package fstop.ws;

public class WsN384Row {

	private String _TRADEDATE;// 預約日期
	private String _CREDITNO;// 信託帳號
	private String _TRANSCODE;// 基金代碼
	private String _FUNDLNAME;// 基金名稱
	private String _BILLSENDMODE;// 轉換方式
	private String _UNIT;// 贖回單位
	private String _CRY;// 贖回信託幣別代碼
	private String _ADCCYNAME;// 贖回信託幣別名稱
	private String _FUNDAMT;// 贖回信託金額
	private String _FUNDACN;// 入帳帳號
	private String _SSLTXNO;// SSL交易序號

	public String getTRADEDATE() {
		return _TRADEDATE;
	}

	public void setTRADEDATE(String tradedate) {
		this._TRADEDATE = tradedate;
	}

	public String getCREDITNO() {
		return _CREDITNO;
	}

	public void setCREDITNO(String creditno) {
		this._CREDITNO = creditno;
	}

	public String getTRANSCODE() {
		return _TRANSCODE;
	}

	public void setTRANSCODE(String transcode) {
		this._TRANSCODE = transcode;
	}

	public String getFUNDLNAME() {
		return _FUNDLNAME;
	}

	public void setFUNDLNAME(String fundlname) {
		this._FUNDLNAME = fundlname;
	}

	public String getBILLSENDMODE() {
		return _BILLSENDMODE;
	}

	public void setBILLSENDMODE(String billsendmode) {
		this._BILLSENDMODE = billsendmode;
	}

	public String getUNIT() {
		return _UNIT;
	}

	public void setUNIT(String unit) {
		this._UNIT = unit;
	}

	public String getCRY() {
		return _CRY;
	}

	public void setCRY(String cry) {
		this._CRY = cry;
	}

	public String getADCCYNAME() {
		return _ADCCYNAME;
	}

	public void setADCCYNAME(String adccyname) {
		this._ADCCYNAME = adccyname;
	}

	public String getFUNDAMT() {
		return _FUNDAMT;
	}

	public void setFUNDAMT(String fundamt) {
		this._FUNDAMT = fundamt;
	}

	public String getFUNDACN() {
		return _FUNDACN;
	}

	public void setFUNDACN(String fundacn) {
		this._FUNDACN = fundacn;
	}

	public String getSSLTXNO() {
		return _SSLTXNO;
	}

	public void setSSLTXNO(String ssltxno) {
		this._SSLTXNO = ssltxno;
	}

}
