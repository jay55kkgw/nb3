package fstop.ws;

public class WsBM2000Out {
	
	private String _MsgCode;
	
	private String _MsgName;
	
	private String _ADOS;
	
	private String _ADMODEL;
	
	private String _ADVERNO;
	
	private String _ADVERDATE;
	
	private String _ADSTATUS;
	
	private String _ADMSG;
	
	private String _ADURL;
		
	public String getADMODEL() {
		return _ADMODEL;
	}

	public void setADMODEL(String ADMODEL) {
		this._ADMODEL = ADMODEL;
	}
	
	public String getADVERNO() {
		return _ADVERNO;
	}

	public void setADVERNO(String ADVERNO) {
		this._ADVERNO = ADVERNO;
	}
	
	public String getADVERDATE() {
		return _ADVERDATE;
	}

	public void setADVERDATE(String ADVERDATE) {
		this._ADVERDATE = ADVERDATE;
	}
	
	public String getADSTATUS() {
		return _ADSTATUS;
	}

	public void setADSTATUS(String ADSTATUS) {
		this._ADSTATUS = ADSTATUS;
	}
	
	public String getADMSG() {
		return _ADMSG;
	}

	public void setADMSG(String ADMSG) {
		this._ADMSG = ADMSG;
	}
	
	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String MsgCode) {
		this._MsgCode = MsgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String MsgName) {
		this._MsgName = MsgName;
	}

	public String getADOS() {
		return _ADOS;
	}

	public void setADOS(String ADOS) {
		this._ADOS = ADOS;
	}
	public String getADURL() {
		return _ADURL;
	}

	public void setADURL(String ADURL) {
		this._ADURL = ADURL;
	}
}
