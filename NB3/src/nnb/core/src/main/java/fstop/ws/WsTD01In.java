package fstop.ws;

public class WsTD01In {
	
	private String _SessionId;
	// 信用卡類型(一般卡、VISA金融卡)
	private String _CARDTYPE1;
	// 卡號
	private String _CARDNUM;

	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		_SessionId = sessionId;
	}

	public String getCARDTYPE1() {
		return _CARDTYPE1;
	}

	public void setCARDTYPE1(String cardtype1) {
		_CARDTYPE1 = cardtype1;
	}

	public String getCARDNUM() {
		return _CARDNUM;
	}

	public void setCARDNUM(String cardnum) {
		_CARDNUM = cardnum;
	}
		
}
