package fstop.ws;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class WsCN35Out implements Serializable {
	private static final long serialVersionUID = -8613249540913820996L;
	private String msgCode;
	private String msgName; 

	private List<WsCN35Row> rows = new ArrayList<WsCN35Row>();
	
	public String getMsgCode() {
		return msgCode;
	}
	
	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public String getMsgName() {
		return msgName;
	}

	public void setMsgName(String msgName) {
		this.msgName = msgName;
	}

	public List<WsCN35Row> getRows() {
		return rows;
	}

	public void setRows(List<WsCN35Row> rows) {
		this.rows = rows;
	}
}