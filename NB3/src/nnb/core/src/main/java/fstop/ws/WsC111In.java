package fstop.ws;

public class WsC111In {

	private String _SessionId;     //身份識別
	private String _N950PASSWORD;  //交易密碼
	private String _CREDITNO;      //信託帳號	
	private String _TRANSCODE;     //扣款標的代號
//	private String _OPAYCUR;	   //原申購金額幣別	
//	private String _OPAYAMT;	   //原申購金額
//	private String _OPAYDAY1;      //原日期1	
//	private String _OPAYDAY2;      //原日期2
//	private String _OPAYDAY3;      //原日期3		
//	private String _OPAYDAY4;      //原日期4	
//	private String _OPAYDAY5;      //原日期5	
//	private String _OPAYDAY6;      //原日期6
//	private String _OPAYDAY7;      //原日期7
//	private String _OPAYDAY8;      //原日期8
//	private String _OPAYDAY9;      //原日期9
//	private String _MIP;           //類別	

	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionid) {
		this._SessionId = sessionid;
	}

	public String getN950PASSWORD() {
		return _N950PASSWORD;
	}

	public void setN950PASSWORD(String n950password) {
		this._N950PASSWORD = n950password;
	}

	public String getCREDITNO() {
		return _CREDITNO;
	}

	public void setCREDITNO(String CREDITNO) {
		this._CREDITNO = CREDITNO;
	}
	
	public String getTRANSCODE() {
		return _TRANSCODE;
	}

	public void setTRANSCODE(String TRANSCODE) {
		this._TRANSCODE = TRANSCODE;
	}
//
//	public String getOPAYCUR() {
//		return _OPAYCUR;
//	}
//
//	public void setOPAYCUR(String OPAYCUR) {
//		this._OPAYCUR = OPAYCUR;
//	}
//
//	public String getOPAYAMT() {
//		return _OPAYAMT;
//	}
//
//	public void setOPAYAMT(String OPAYAMT) {
//		this._OPAYAMT = OPAYAMT;
//	}
//
//	public String getOPAYDAY1() {
//		return _OPAYDAY1;
//	}
//
//	public void setOPAYDAY1(String OPAYDAY1) {
//		this._OPAYDAY1 = OPAYDAY1;
//	}
//
//	public String getOPAYDAY2() {
//		return _OPAYDAY2;
//	}
//
//	public void setOPAYDAY2(String OPAYDAY2) {
//		this._OPAYDAY2 = OPAYDAY2;
//	}
//	
//	public String getOPAYDAY3() {
//		return _OPAYDAY3;
//	}
//
//	public void setOPAYDAY3(String OPAYDAY3) {
//		this._OPAYDAY3 = OPAYDAY3;
//	}
//
//	public String getOPAYDAY4() {
//		return _OPAYDAY4;
//	}
//
//	public void setOPAYDAY4(String OPAYDAY4) {
//		this._OPAYDAY4 = OPAYDAY4;
//	}
//
//	public String getOPAYDAY5() {
//		return _OPAYDAY5;
//	}
//
//	public void setOPAYDAY5(String OPAYDAY5) {
//		this._OPAYDAY5 = OPAYDAY5;
//	}
//
//	public String getOPAYDAY6() {
//		return _OPAYDAY6;
//	}
//
//	public void setOPAYDAY6(String OPAYDAY6) {
//		this._OPAYDAY6 = OPAYDAY6;
//	}
//
//	public String getOPAYDAY7() {
//		return _OPAYDAY7;
//	}
//
//	public void setOPAYDAY7(String OPAYDAY7) {
//		this._OPAYDAY7 = OPAYDAY7;
//	}
//	
//	public String getOPAYDAY8() {
//		return _OPAYDAY8;
//	}
//
//	public void setOPAYDAY8(String OPAYDAY8) {
//		this._OPAYDAY8 = OPAYDAY8;
//	}
//	
//	public String getOPAYDAY9() {
//		return _OPAYDAY9;
//	}
//
//	public void setOPAYDAY9(String OPAYDAY9) {
//		this._OPAYDAY9 = OPAYDAY9;
//	}
//	
//	public String getMIP() {
//		return _MIP;
//	}
//
//	public void setMIP(String MIP) {
//		this._MIP = MIP;
//	}
	
}
