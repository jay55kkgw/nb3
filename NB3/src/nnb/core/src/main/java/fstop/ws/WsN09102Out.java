package fstop.ws;

public class WsN09102Out {

	private String _MsgCode;

	private String _MsgName;

	private String _CMQTIME;

	private String _SVACN;

	private String _ACN;

	private String _TRNGD;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getCMQTIME() {
		return _CMQTIME;
	}

	public void setCMQTIME(String CMQTIME) {
		this._CMQTIME = CMQTIME;
	}

	public String getSVACN() {
		return _SVACN;
	}

	public void setSVACN(String SVACN) {
		this._SVACN = SVACN;
	}

	public String getACN() {
		return _ACN;
	}

	public void setACN(String ACN) {
		this._ACN = ACN;
	}

	public String getTRNGD() {
		return _TRNGD;
	}

	public void setTRNGD(String TRNGD) {
		this._TRNGD = TRNGD;
	}

}
