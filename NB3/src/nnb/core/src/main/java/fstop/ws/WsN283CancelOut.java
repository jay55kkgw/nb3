package fstop.ws;

public class WsN283CancelOut {

	private String _MsgCode;

	private String _MsgName;

	private String _CMTXTIME;

	private String _SCHID;
	
	public String getMsgCode() {
		return _MsgCode;
	}
	public void setMsgCode(String msgcode) {
		this._MsgCode = msgcode;
	}

	public String getMsgName() {
		return _MsgName;
	}
	public void setMsgName(String msgname) {
		this._MsgName = msgname;
	}

	public String getCMTXTIME() {
		return _CMTXTIME;
	}
	public void setCMTXTIME(String cmtxtime) {
		this._CMTXTIME = cmtxtime;
	}

	public String getSCHID() {
		return _SCHID;
	}
	public void setSCHID(String schid) {
		this._SCHID = schid;
	}
}
