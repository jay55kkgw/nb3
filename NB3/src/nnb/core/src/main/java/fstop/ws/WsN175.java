package fstop.ws;

public interface WsN175{

	//外匯定存查詢
	public WsN531Out getList(CmUserQry prarams);
	//外匯定存解約前置電文資料
	public WsN175BeforOut beforN175(WsN175BeforIn prarams);
	//執行外匯定存解約
	public WsN175Out actionN175(WsN175In prarams);
	//執行外幣定存解約(預約)
	public WsN175SOut actionN175S(WsN175SIn prarams);
}