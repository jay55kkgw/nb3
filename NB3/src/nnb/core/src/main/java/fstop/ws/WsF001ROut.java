package fstop.ws;

public class WsF001ROut {

	private String _MsgCode;

	private String _MsgName;

	private String _CMQTIME;

	private String _BGROENO;

	private String _RATE;

	private String _CURAMT;

	private String _ATRAMT;

	public String getMsgCode() {
		return _MsgCode;
	}
	public void setMsgCode(String msgcode) {
		this._MsgCode = msgcode;
	}

	public String getMsgName() {
		return _MsgName;
	}
	public void setMsgName(String msgname) {
		this._MsgName = msgname;
	}

	public String getCMQTIME() {
		return _CMQTIME;
	}
	public void setCMQTIME(String cmqtime) {
		this._CMQTIME = cmqtime;
	}

	public String getBGROENO() {
		return _BGROENO;
	}
	public void setBGROENO(String bgroeno) {
		this._BGROENO = bgroeno;
	}

	public String getRATE() {
		return _RATE;
	}
	public void setRATE(String rate) {
		this._RATE = rate;
	}

	public String getCURAMT() {
		return _CURAMT;
	}
	public void setCURAMT(String curamt) {
		this._CURAMT = curamt;
	}

	public String getATRAMT() {
		return _ATRAMT;
	}
	public void setATRAMT(String atramt) {
		this._ATRAMT = atramt;
	}
}
