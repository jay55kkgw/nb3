package fstop.ws;

/**
 * @author user
 * 
 */
public class WsC033Row {

	private String _CREDITNO;// 基金名稱

	private String _TRANSCODE;// 信託帳號

	private String _FUNDLNAME;// 首次申購日

	private String _CRY;// 信託幣別

	private String _ADCCYNAME;// 信託金額

	private String _FUNDAMT;// 類別

	private String _UNIT;// 單位數

	private String _TRADEDATE;// 參考淨值
	
	private String _FUNDACN;// 入帳帳號
	
	public String getCREDITNO() {
		return _CREDITNO;
	}

	public void setCREDITNO(String CREDITNO) {
		this._CREDITNO = CREDITNO;
	}
	
	public String getTRANSCODE() {
		return _TRANSCODE;
	}

	public void setTRANSCODE(String TRANSCODE) {
		this._TRANSCODE = TRANSCODE;
	}

	public String getFUNDLNAME() {
		return _FUNDLNAME;
	}

	public void setFUNDLNAME(String FUNDLNAME) {
		this._FUNDLNAME = FUNDLNAME;
	}

	public String getCRY() {
		return _CRY;
	}

	public void setCRY(String cry) {
		this._CRY = cry;
	}

	public String getADCCYNAME() {
		return _ADCCYNAME;
	}

	public void setADCCYNAME(String ADCCYNAME) {
		this._ADCCYNAME = ADCCYNAME;
	}

	public String getFUNDAMT() {
		return _FUNDAMT;
	}

	public void setFUNDAMT(String FUNDAMT) {
		this._FUNDAMT = FUNDAMT;
	}

	public String getUNIT() {
		return _UNIT;
	}

	public void setUNIT(String UNIT) {
		this._UNIT = UNIT;
	}

	public String getTRADEDATE() {
		return _TRADEDATE;
	}

	public void setTRADEDATE(String TRADEDATE) {
		this._TRADEDATE = TRADEDATE;
	}

	public String getFUNDACN() {
		return _FUNDACN;
	}

	public void setFUNDACN(String FUNDACN) {
		this._FUNDACN = FUNDACN;
	}
}
