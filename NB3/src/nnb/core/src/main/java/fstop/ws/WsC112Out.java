package fstop.ws;

public class WsC112Out {
 
	private String _MsgCode;	  //訊息代碼
	private String _MsgName;	  //訊息名稱
	private String _CREDITNO;	  //信託帳號
	private String _FUNDLNAME;	  //扣款標的名稱
	private String _MIP;		  //類別
	private String _ALTERITEM;	  //變更項目
	private String _OPAYAMTTITLE; //原申購金額TITLE
	private String _NPAYAMTTITLE; //變更後申購金額TITLE
	private String _PAYAMT;		  //扣款金額
	private String _PAYCUR;		  //扣款金額幣別
	private String _PAYDAY;	      //變更後日期
	private String _ALTERTYPE;	  //變更後扣款狀態

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getCREDITNO() {
		return _CREDITNO;
	}

	public void setCREDITNO(String CREDITNO) {
		this._CREDITNO = CREDITNO;
	}
	
	public String getFUNDLNAME() {
		return _FUNDLNAME;
	}

	public void setFUNDLNAME(String FUNDLNAME) {
		this._FUNDLNAME = FUNDLNAME;
	}
	
	public String getMIP() {
		return _MIP;
	}

	public void setMIP(String MIP) {
		this._MIP = MIP;
	}
	
	public String getALTERITEM() {
		return _ALTERITEM;
	}

	public void setALTERITEM(String ALTERITEM) {
		this._ALTERITEM = ALTERITEM;
	}
	
	public String getOPAYAMTTITLE() {
		return _OPAYAMTTITLE;
	}

	public void setOPAYAMTTITLE(String OPAYAMTTITLE) {
		this._OPAYAMTTITLE = OPAYAMTTITLE;
	}
	
	public String getNPAYAMTTITLE() {
		return _NPAYAMTTITLE;
	}

	public void setNPAYAMTTITLE(String NPAYAMTTITLE) {
		this._NPAYAMTTITLE = NPAYAMTTITLE;
	}
	
	public String getPAYAMT() {
		return _PAYAMT;
	}

	public void setPAYAMT(String PAYAMT) {
		this._PAYAMT = PAYAMT;
	}
			
	public String getPAYCUR() {
		return _PAYCUR;
	}

	public void setPAYCUR(String PAYCUR) {
		this._PAYCUR = PAYCUR;
	}
	
	public String getPAYDAY() {
		return _PAYDAY;
	}

	public void setPAYDAY(String PAYDAY) {
		this._PAYDAY = PAYDAY;
	}
	
	public String getALTERTYPE() {
		return _ALTERTYPE;
	}

	public void setALTERTYPE(String ALTERTYPE) {
		this._ALTERTYPE = ALTERTYPE;
	}
}
