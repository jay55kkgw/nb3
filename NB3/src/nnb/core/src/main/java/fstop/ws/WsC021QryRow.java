package fstop.ws;

public class WsC021QryRow {

	private String _CREDITNO;// 信託帳號
	private String _TRANSCODE;// 基金代碼
	private String _CRYCODE;// 信託金額幣別代碼
	private String _CRYNAME;// 信託金額幣別名稱
	private String _FUNDAMT;// 信託金額
	private String _UNIT;// 單位數
	private String _SHORTTUNIT;// 已達短線之贖回單位數
	private String _FUNDLNAME;//	基金名稱

	public String getCREDITNO() {
		return _CREDITNO;
	}

	public void setCREDITNO(String creditno) {
		this._CREDITNO = creditno;
	}

	public String getTRANSCODE() {
		return _TRANSCODE;
	}

	public void setTRANSCODE(String transcode) {
		this._TRANSCODE = transcode;
	}

	public String getCRYCODE() {
		return _CRYCODE;
	}

	public void setCRYCODE(String cry) {
		this._CRYCODE = cry;
	}

	public String getCRYNAME() {
		return _CRYNAME;
	}

	public void setCRYNAME(String cry) {
		this._CRYNAME = cry;
	}

	public String getFUNDAMT() {
		return _FUNDAMT;
	}

	public void setFUNDAMT(String fundamt) {
		this._FUNDAMT = fundamt;
	}

	public String getUNIT() {
		return _UNIT;
	}

	public void setUNIT(String unit) {
		this._UNIT = unit;
	}

	public String getSHORTTUNIT() {
		return _SHORTTUNIT;
	}

	public void setSHORTTUNIT(String shorttunit) {
		this._SHORTTUNIT = shorttunit;
	}

	public String getFUNDLNAME() {
		return _FUNDLNAME;
	}

	public void setFUNDLNAME(String fundlname) {
		this._FUNDLNAME = fundlname;
	}

}
