package fstop.ws;

public class WsBM8000Out {

	private String _MsgCode;

	private String _MsgName;

	private WsBM8000Row[] _Table;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public WsBM8000Row[] getTable() {
		return _Table;
	}

	public void setTable(WsBM8000Row[] table) {
		this._Table = table;
	}

}
