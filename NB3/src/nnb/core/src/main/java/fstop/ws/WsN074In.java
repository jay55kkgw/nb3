package fstop.ws;

public class WsN074In {
	
	private String _SessionId;       //身份識別
	private String _N950PASSWORD;	 //交易密碼
	private String _TXTOKEN;		 //防止重送代碼
	private String _CAPTCHA;		 //圖形驗證碼
	private String _FGTXWAY;		 //交易機制
	private String _CMPASSWORD;		 //交易密碼
	private String _PINNEW;		 	 //交易密碼SHA1值
	private String _OTPKEY;			 //OTP動態密碼
	private String _FGTXDATE;		 //轉帳日期類型
	private String _CMDATE;			 //轉帳日期
	private String _CMDATE1;		 //指定到期日
	private String _FGSVACNO;		 //約定否
	private String _ACNNO;		 	 //轉出帳號
	private String _DPAGACNO;		 //轉入帳號
	private String _INTSACN;		 //轉入帳號
	private String _AMOUNT;		 	 //轉帳金額
	private String _FDPACC;		 	 //存款種類
	private String _TERM;		 	 //存單期別
	private String _FGTDPERIOD;		 //存款期別或指定到期日
	private String _INTMTH;		 	 //計息方式
	private String _TXTIMES;		 //到期轉期次數
	private String _CODE;		 	 //到期轉期
	private String _FGSVTYPE;		 //轉存方式
	private String _CMTRMEMO;		 //交易備註
	private String _CMTRMAIL;		 //Email信箱
	private String _CMMAILMEMO;		 //Email摘要內容
	
	
	/**
	 * no _N950PASSWORD AND NO _CMPASSWORD AND _NO PINNEW
	 */
	@Override
	public String toString()
	{
		return "WsN074In [_SessionId=" + _SessionId + ", _TXTOKEN=" + _TXTOKEN + ", _CAPTCHA=" + _CAPTCHA
				+ ", _FGTXWAY=" + _FGTXWAY + ", _OTPKEY=" + _OTPKEY + ", _FGTXDATE=" + _FGTXDATE + ", _CMDATE="
				+ _CMDATE + ", _CMDATE1=" + _CMDATE1 + ", _FGSVACNO=" + _FGSVACNO + ", _ACNNO=" + _ACNNO
				+ ", _DPAGACNO=" + _DPAGACNO + ", _INTSACN=" + _INTSACN + ", _AMOUNT=" + _AMOUNT + ", _FDPACC="
				+ _FDPACC + ", _TERM=" + _TERM + ", _FGTDPERIOD=" + _FGTDPERIOD + ", _INTMTH=" + _INTMTH + ", _TXTIMES="
				+ _TXTIMES + ", _CODE=" + _CODE + ", _FGSVTYPE=" + _FGSVTYPE + ", _CMTRMEMO=" + _CMTRMEMO
				+ ", _CMTRMAIL=" + _CMTRMAIL + ", _CMMAILMEMO=" + _CMMAILMEMO + "]";
	}
	
	
	public String getSessionId() {
		return _SessionId;
	}
	public void setSessionId(String sessionid) {
		this._SessionId = sessionid;
	}

	public String getN950PASSWORD() {
		return _N950PASSWORD;
	}
	public void setN950PASSWORD(String N950PASSWORD) {
		this._N950PASSWORD = N950PASSWORD;
	}
	
	public String getTXTOKEN() {
		return _TXTOKEN;
	}
	public void setTXTOKEN(String txtoken) {
		this._TXTOKEN = txtoken;
	}

	public String getCAPTCHA() {
		return _CAPTCHA;
	}
	public void setCAPTCHA(String CAPTCHA) {
		this._CAPTCHA = CAPTCHA;
	}
	
	public String getFGTXWAY() {
		return _FGTXWAY;
	}
	public void setFGTXWAY(String FGTXWAY) {
		this._FGTXWAY = FGTXWAY;
	}

	public String getCMPASSWORD() {
		return _CMPASSWORD;
	}
	public void setCMPASSWORD(String CMPASSWORD) {
		this._CMPASSWORD = CMPASSWORD;
	}

	public String getPINNEW() {
		return _PINNEW;
	}
	public void setPINNEW(String PINNEW) {
		this._PINNEW = PINNEW;
	}

	public String getOTPKEY() {
		return _OTPKEY;
	}
	public void setOTPKEY(String OTPKEY) {
		this._OTPKEY = OTPKEY;
	}

	public String getFGTXDATE() {
		return _FGTXDATE;
	}
	public void setFGTXDATE(String FGTXDATE) {
		this._FGTXDATE = FGTXDATE;
	}
	
	public String getCMDATE() {
		return _CMDATE;
	}
	public void setCMDATE(String CMDATE) {
		this._CMDATE = CMDATE;
	}
	
	public String getCMDATE1() {
		return _CMDATE1;
	}
	public void setCMDATE1(String CMDATE1) {
		this._CMDATE1 = CMDATE1;
	}
	
	public String getFGSVACNO() {
		return _FGSVACNO;
	}
	public void setFGSVACNO(String FGSVACNO) {
		this._FGSVACNO = FGSVACNO;
	}

	public String getACNNO() {
		return _ACNNO;
	}
	public void setACNNO(String ACNNO) {
		this._ACNNO = ACNNO;
	}

	public String getDPAGACNO() {
		return _DPAGACNO;
	}
	public void setDPAGACNO(String DPAGACNO) {
		this._DPAGACNO = DPAGACNO;
	}

	public String getINTSACN() {
		return _INTSACN;
	}
	public void setINTSACN(String INTSACN) {
		this._INTSACN = INTSACN;
	}
	
	public String getAMOUNT() {
		return _AMOUNT;
	}
	public void setAMOUNT(String AMOUNT) {
		this._AMOUNT = AMOUNT;
	}

	public String getFDPACC() {
		return _FDPACC;
	}
	public void setFDPACC(String FDPACC) {
		this._FDPACC = FDPACC;
	}

	public String getTERM() {
		return _TERM;
	}
	public void setTERM(String TERM) {
		this._TERM = TERM;
	}
	
	public String getFGTDPERIOD() {
		return _FGTDPERIOD;
	}
	public void setFGTDPERIOD(String FGTDPERIOD) {
		this._FGTDPERIOD = FGTDPERIOD;
	}

	public String getINTMTH() {
		return _INTMTH;
	}
	public void setINTMTH(String INTMTH) {
		this._INTMTH = INTMTH;
	}
	
	public String getTXTIMES() {
		return _TXTIMES;
	}
	public void setTXTIMES(String TXTIMES) {
		this._TXTIMES = TXTIMES;
	}
	
	public String getCODE() {
		return _CODE;
	}
	public void setCODE(String CODE) {
		this._CODE = CODE;
	}
	
	public String getFGSVTYPE() {
		return _FGSVTYPE;
	}
	public void setFGSVTYPE(String FGSVTYPE) {
		this._FGSVTYPE = FGSVTYPE;
	}
	
	public String getCMTRMEMO() {
		return _CMTRMEMO;
	}
	public void setCMTRMEMO(String CMTRMEMO) {
		this._CMTRMEMO = CMTRMEMO;
	}
	
	public String getCMTRMAIL() {
		return _CMTRMAIL;
	}
	public void setCMTRMAIL(String CMTRMAIL) {
		this._CMTRMAIL = CMTRMAIL;
	}
	
	public String getCMMAILMEMO() {
		return _CMMAILMEMO;
	}
	public void setCMMAILMEMO(String CMMAILMEMO) {
		this._CMMAILMEMO = CMMAILMEMO;
	}
}
