package fstop.ws;

public class WsC016In {

	private String _SessionId;

	private String _N950PASSWORD;

	private String _TRANSCODE;

	private String _COUNTRYTYPE;

	private String _AMT3;

	private String _HTELPHONE;

	private String _STOP;

	private String _YIELD;

	private String _FDPUBLICTYPE;// 公開說明書交付方式

	private String _ADTXNO;// 交易編號

	private String _TYPE;//扣款方式
	
	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionid) {
		this._SessionId = sessionid;
	}

	public String getN950PASSWORD() {
		return _N950PASSWORD;
	}

	public void setN950PASSWORD(String n950password) {
		this._N950PASSWORD = n950password;
	}

	public String getTRANSCODE() {
		return _TRANSCODE;
	}

	public void setTRANSCODE(String transcode) {
		this._TRANSCODE = transcode;
	}

	public String getCOUNTRYTYPE() {
		return _COUNTRYTYPE;
	}

	public void setCOUNTRYTYPE(String countrytype) {
		this._COUNTRYTYPE = countrytype;
	}

	public String getAMT3() {
		return _AMT3;
	}

	public void setAMT3(String amt3) {
		this._AMT3 = amt3;
	}

	public String getHTELPHONE() {
		return _HTELPHONE;
	}

	public void setHTELPHONE(String htelphone) {
		this._HTELPHONE = htelphone;
	}

	public String getSTOP() {
		return _STOP;
	}

	public void setSTOP(String stop) {
		this._STOP = stop;
	}

	public String getYIELD() {
		return _YIELD;
	}

	public void setYIELD(String yield) {
		this._YIELD = yield;
	}

	public String getFDPUBLICTYPE() {
		return _FDPUBLICTYPE;
	}

	public void setFDPUBLICTYPE(String fdpublictype) {
		this._FDPUBLICTYPE = fdpublictype;
	}

	public String getADTXNO() {
		return _ADTXNO;
	}

	public void setADTXNO(String adtxno) {
		this._ADTXNO = adtxno;
	}

	public String getTYPE() {
		return _TYPE;
	}

	public void setTYPE(String type) {
		this._TYPE = type;
	}

}
