package fstop.ws;

public interface WsN076{
	
	public CmFDP getFDPList(CmUserQry params);
	
	public WsN076Out action(WsN076In params);
}