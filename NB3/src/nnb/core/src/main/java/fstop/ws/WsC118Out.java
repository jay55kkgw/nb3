package fstop.ws;

public class WsC118Out {
	
	private String MsgCode;	//訊息代碼
	private String MsgName;	//訊息名稱
	private String CHANGEKIND;	//變更種類
	private String TRANSCODE_RecordCount;	//資料總數
	private String CUSIDN;	//身分證字號/統一編號
	private String CUSNAME;	//姓名
	private WsC118Row[] Table;//	表格結果
	public String getMsgCode() {
		return MsgCode;
	}
	public void setMsgCode(String msgCode) {
		MsgCode = msgCode;
	}
	public String getMsgName() {
		return MsgName;
	}
	public void setMsgName(String msgName) {
		MsgName = msgName;
	}
	public String getCHANGEKIND() {
		return CHANGEKIND;
	}
	public void setCHANGEKIND(String changekind) {
		CHANGEKIND = changekind;
	}
	public String getTRANSCODE_RecordCount() {
		return TRANSCODE_RecordCount;
	}
	public void setTRANSCODE_RecordCount(String recordCount) {
		TRANSCODE_RecordCount = recordCount;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cusidn) {
		CUSIDN = cusidn;
	}
	public String getCUSNAME() {
		return CUSNAME;
	}
	public void setCUSNAME(String cusname) {
		CUSNAME = cusname;
	}
	public WsC118Row[] getTable() {
		return Table;
	}
	public void setTable(WsC118Row[] table) {
		Table = table;
	}

	

}
