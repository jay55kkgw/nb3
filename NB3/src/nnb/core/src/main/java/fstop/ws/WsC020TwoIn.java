package fstop.ws;

public class WsC020TwoIn {

	private String _SessionId;     //身份識別
	private String _TXTOKEN;	   //防止重送代碼
	private String _CAPTCHA;	   //圖形驗證碼
	private String _FGTXWAY;	   //密碼類別
	private String _CMPASSWORD;	   //交易密碼
	private String _PINNEW;		   //交易密碼SHA1值
	private String _CUSIDN;		   //身分證字號/統一編號
	private String _CUSNAME;	   //姓名
	private String _FDINVTYPE;	   //客戶投資屬性
	private String _MIP;	   	   //是否為定期不定額	
	private String _COUNTRYTYPE;   //扣款方式	
	private String _HTELPHONE;	   //扣帳帳號	
	private String _AMT3;		   //每次申購金額
	private String _PAYDAY1;	   //扣款日期1
	private String _PAYDAY2;	   //扣款日期2
	private String _PAYDAY3;	   //扣款日期3
	private String _PAYDAY4;	   //扣款日期4
	private String _PAYDAY5;	   //扣款日期5
	private String _PAYDAY6;	   //扣款日期6
	private String _PAYDAY7;	   //扣款日期7
	private String _PAYDAY8;	   //扣款日期8
	private String _PAYDAY9;	   //扣款日期9
	private String _YIELD;		   //停利通知設定
	private String _STOP;		   //停損通知設定
	private String _PAYTYPE;	   //扣帳類別
	private String _FUNDACN;	   //入帳帳號
//	private String _SSLTXNO;	   //SSL交易序號
	private String _TRADEDATE;	   //申請日期
	private String _FCA2;		   //手續費
	private String _FCAFEE;		   //手續費率
	private String _AMT5;		   //每次扣款金額
	private String _DBDATE;		   //首次扣款日
	private String _TRANSCODE;	   //基金代碼
	private String _FUDCUR;	       //扣款幣別

	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionid) {
		this._SessionId = sessionid;
	}

	public String getTXTOKEN() {
		return _TXTOKEN;
	}

	public void setTXTOKEN(String txtoken) {
		this._TXTOKEN = txtoken;
	}

	public String getCAPTCHA() {
		return _CAPTCHA;
	}

	public void setCAPTCHA(String captcha) {
		this._CAPTCHA = captcha;
	}
	
	public String getFGTXWAY() {
		return _FGTXWAY;
	}

	public void setFGTXWAY(String fgtxway) {
		this._FGTXWAY = fgtxway;
	}

	public String getCMPASSWORD() {
		return _CMPASSWORD;
	}

	public void setCMPASSWORD(String cmpassword) {
		this._CMPASSWORD = cmpassword;
	}

	public String getPINNEW() {
		return _PINNEW;
	}

	public void setPINNEW(String pinnew) {
		this._PINNEW = pinnew;
	}

	public String getCUSIDN() {
		return _CUSIDN;
	}

	public void setCUSIDN(String CUSIDN) {
		this._CUSIDN = CUSIDN;
	}
	
	public String getCUSNAME() {
		return _CUSNAME;
	}

	public void setCUSNAME(String CUSNAME) {
		this._CUSNAME = CUSNAME;
	}
	
	public String getFDINVTYPE() {
		return _FDINVTYPE;
	}

	public void setFDINVTYPE(String fdinvtype) {
		this._FDINVTYPE = fdinvtype;
	}

	public String getMIP() {
		return _MIP;
	}

	public void setMIP(String MIP) {
		this._MIP = MIP;
	}
	
	public String getCOUNTRYTYPE() {
		return _COUNTRYTYPE;
	}

	public void setCOUNTRYTYPE(String countrytype) {
		this._COUNTRYTYPE = countrytype;
	}	

	public String getHTELPHONE() {
		return _HTELPHONE;
	}

	public void setHTELPHONE(String htelphone) {
		this._HTELPHONE = htelphone;
	}	

	public String getAMT3() {
		return _AMT3;
	}

	public void setAMT3(String amt3) {
		this._AMT3 = amt3;
	}

	public String getPAYDAY1() {
		return _PAYDAY1;
	}

	public void setPAYDAY1(String payday1) {
		this._PAYDAY1 = payday1;
	}

	public String getPAYDAY2() {
		return _PAYDAY2;
	}

	public void setPAYDAY2(String payday2) {
		this._PAYDAY2 = payday2;
	}

	public String getPAYDAY3() {
		return _PAYDAY3;
	}

	public void setPAYDAY3(String payday3) {
		this._PAYDAY3 = payday3;
	}

	public String getPAYDAY4() {
		return _PAYDAY4;
	}

	public void setPAYDAY4(String PAYDAY4) {
		this._PAYDAY4 = PAYDAY4;
	}
	
	public String getPAYDAY5() {
		return _PAYDAY5;
	}

	public void setPAYDAY5(String PAYDAY5) {
		this._PAYDAY5 = PAYDAY5;
	}
	
	public String getPAYDAY6() {
		return _PAYDAY6;
	}

	public void setPAYDAY6(String PAYDAY6) {
		this._PAYDAY6 = PAYDAY6;
	}
	
	public String getPAYDAY7() {
		return _PAYDAY7;
	}

	public void setPAYDAY7(String PAYDAY7) {
		this._PAYDAY7 = PAYDAY7;
	}
	
	public String getPAYDAY8() {
		return _PAYDAY8;
	}

	public void setPAYDAY8(String PAYDAY8) {
		this._PAYDAY8 = PAYDAY8;
	}
	
	public String getPAYDAY9() {
		return _PAYDAY9;
	}

	public void setPAYDAY9(String PAYDAY9) {
		this._PAYDAY9 = PAYDAY9;
	}
	
	public String getYIELD() {
		return _YIELD;
	}

	public void setYIELD(String yield) {
		this._YIELD = yield;
	}

	public String getSTOP() {
		return _STOP;
	}

	public void setSTOP(String stop) {
		this._STOP = stop;
	}

	public String getPAYTYPE() {
		return _PAYTYPE;
	}

	public void setPAYTYPE(String paytype) {
		this._PAYTYPE = paytype;
	}

	public String getFUNDACN() {
		return _FUNDACN;
	}

	public void setFUNDACN(String fundacn) {
		this._FUNDACN = fundacn;
	}
	
//	public String getSSLTXNO() {
//		return _SSLTXNO;
//	}
//
//	public void setSSLTXNO(String ssltxno) {
//		this._SSLTXNO = ssltxno;
//	}
	public String getTRADEDATE() {
		return _TRADEDATE;
	}

	public void setTRADEDATE(String tradedate) {
		this._TRADEDATE = tradedate;
	}

	public String getFCA2() {
		return _FCA2;
	}

	public void setFCA2(String fca2) {
		this._FCA2 = fca2;
	}

	public String getFCAFEE() {
		return _FCAFEE;
	}

	public void setFCAFEE(String fcafee) {
		this._FCAFEE = fcafee;
	}

	public String getAMT5() {
		return _AMT5;
	}

	public void setAMT5(String AMT5) {
		this._AMT5 = AMT5;
	}
	
	public String getDBDATE() {
		return _DBDATE;
	}

	public void setDBDATE(String dbdate) {
		this._DBDATE = dbdate;
	}

	public String getTRANSCODE() {
		return _TRANSCODE;
	}

	public void setTRANSCODE(String transcode) {
		this._TRANSCODE = transcode;
	}
	
	public String getFUDCUR() {
		return _FUDCUR;
	}

	public void setFUDCUR(String fudcur) {
		this._FUDCUR = fudcur;
	}
}
