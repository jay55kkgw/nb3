package fstop.ws;

public class WsN930eaRow {

	private String _DPABMAIL;

	private String _DPGONAME;

//	private String _DPUSERID;

	public String getDPABMAIL() {
		return _DPABMAIL;
	}

	public void setDPABMAIL(String dpabmail) {
		this._DPABMAIL = dpabmail;
	}

	public String getDPGONAME() {
		return _DPGONAME;
	}

	public void setDPGONAME(String dpgoname) {
		this._DPGONAME = dpgoname;
	}

//	public String getDPUSERID() {
//		return _DPUSERID;
//	}
//
//	public void setDPUSERID(String dpuserid) {
//		this._DPUSERID = dpuserid;
//	}

}
