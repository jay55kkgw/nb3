package fstop.ws;

public class WsC012TwoOut {
	
	private String _MsgCode	;//訊息代碼
	
	private String _MsgName;//	訊息名稱
	
	private String _TRANSCODE_RecordCount;//	資料總數
	
	private String _CMQTIME	;//查詢時間
	
	private String _CUSNAME;//	姓名
	
	private WsC012TwoRow1[] _Table1;//	表格結果
	
	private WsC012TwoRow2[] _Table2;//	表格結果
	
	private WsC012TwoRow3[] _Table3;//	表格結果
	
	private WsC012TwoRow4[] _Table4;//	表格結果

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getTRANSCODE_RecordCount() {
		return _TRANSCODE_RecordCount;
	}

	public void setTRANSCODE_RecordCount(String recordCount) {
		this._TRANSCODE_RecordCount = recordCount;
	}

	public String getCMQTIME() {
		return _CMQTIME;
	}

	public void setCMQTIME(String cmqtime) {
		this._CMQTIME = cmqtime;
	}

	public String getCUSNAME() {
		return _CUSNAME;
	}

	public void setCUSNAME(String cusname) {
		this._CUSNAME = cusname;
	}

	public WsC012TwoRow1[] getTable1() {
		return _Table1;
	}

	public void setTable1(WsC012TwoRow1[] table1) {
		this._Table1 = table1;
	}

	public WsC012TwoRow2[] getTable2() {
		return _Table2;
	}

	public void setTable2(WsC012TwoRow2[] table2) {
		this._Table2 = table2;
	}

	public WsC012TwoRow3[] getTable3() {
		return _Table3;
	}

	public void setTable3(WsC012TwoRow3[] table3) {
		this._Table3 = table3;
	}

	public WsC012TwoRow4[] getTable4() {
		return _Table4;
	}

	public void setTable4(WsC012TwoRow4[] table4) {
		this._Table4 = table4;
	}


}
