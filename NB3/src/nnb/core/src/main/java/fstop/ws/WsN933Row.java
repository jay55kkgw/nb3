package fstop.ws;

public class WsN933Row {
	private String _BNKCOD;
	private String _BNKNAME;
	private String _ACN;
	private String _DPACCSETID;
	private String _DPGONAME;
	private String _TRAFLAG;
    

	public String getBNKCOD() {
		return _BNKCOD;
	}

	public void setBNKCOD(String bnkcod) {
		_BNKCOD = bnkcod;
	}

	public String getBNKNAME() {
		return _BNKNAME;
	}

	public void setBNKNAME(String bnkname) {
		_BNKNAME = bnkname;
	}
	
	public String getACN() {
		return _ACN;
	}

	public void setACN(String acn) {
		_ACN = acn;
	}

	public String getDPACCSETID() {
		return _DPACCSETID;
	}

	public void setDPACCSETID(String dpaccsetid) {
		_DPACCSETID = dpaccsetid;
	}
	
	public String getDPGONAME() {
		return _DPGONAME;
	}

	public void setDPGONAME(String dpgoname) {
		_DPGONAME = dpgoname;
	}
	
	public String getTRAFLAG() {
		return _TRAFLAG;
	}

	public void setTRAFLAG(String traflag) {
		_TRAFLAG = traflag;
	}
		
}
