package fstop.ws;

public interface WsN920{
	
	public CmList getCheckAcnoList(CmUser params);
	
	public CmList getOutAcnoList(CmUser params);
	
	public CmList getAcnoList(CmUser params);
	
	public CmList getGoldAcnoList(CmUser params);
	
	public CmFxList getFxOutAcnoList(CmUser params);
	
	public CmFxDepList getFxDepOutAcnoList(CmUser params);
}