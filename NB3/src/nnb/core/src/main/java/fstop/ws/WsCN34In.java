package fstop.ws;

import java.io.Serializable;

public class WsCN34In implements Serializable {
	private static final long serialVersionUID = -1196574609058268087L;
	private String sessionId;	//SessionId
	private String CUSIDN;
	private String BNKCOD;
	private String ACN;
	private String AMOUNT;
	private String CN;

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String CUSIDN) {
		this.CUSIDN = CUSIDN;
	}

	public String getBNKCOD() {
		return BNKCOD;
	}

	public void setBNKCOD(String BNKCOD) {
		this.BNKCOD = BNKCOD;
	}

	public String getACN() {
		return ACN;
	}

	public void setACN(String ACN) {
		this.ACN = ACN;
	}

	public String getAMOUNT() {
		return AMOUNT;
	}

	public void setAMOUNT(String AMOUNT) {
		this.AMOUNT = AMOUNT;
	}

	public String getCN() {
		return CN;
	}

	public void setCN(String CN) {
		this.CN = CN;
	}

}
