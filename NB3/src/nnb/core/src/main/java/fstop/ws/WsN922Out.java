package fstop.ws;

public class WsN922Out {

	private String _MsgCode;

	private String _MsgName;

	private String _APLBRH;

	private String _CUTTYPE;

	private String _ACN1;

	private String _ACN2;

	private String _NAME;

	private String _FDINVTYPE;

	private String _GETLTD;

	private String _ACN3;

	private String _ACN4;

	private String _ICCOD;// 是否約定信用卡

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgcode) {
		this._MsgCode = msgcode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgname) {
		this._MsgName = msgname;
	}

	public String getAPLBRH() {
		return _APLBRH;
	}

	public void setAPLBRH(String aplbrh) {
		this._APLBRH = aplbrh;
	}

	public String getCUTTYPE() {
		return _CUTTYPE;
	}

	public void setCUTTYPE(String cuttype) {
		this._CUTTYPE = cuttype;
	}

	public String getACN1() {
		return _ACN1;
	}

	public void setACN1(String acn1) {
		this._ACN1 = acn1;
	}

	public String getACN2() {
		return _ACN2;
	}

	public void setACN2(String acn2) {
		this._ACN2 = acn2;
	}

	public String getNAME() {
		return _NAME;
	}

	public void setNAME(String name) {
		this._NAME = name;
	}

	public String getFDINVTYPE() {
		return _FDINVTYPE;
	}

	public void setFDINVTYPE(String fdinvtype) {
		this._FDINVTYPE = fdinvtype;
	}

	public String getGETLTD() {
		return _GETLTD;
	}

	public void setGETLTD(String getltd) {
		this._GETLTD = getltd;
	}

	public String getACN3() {
		return _ACN3;
	}

	public void setACN3(String acn3) {
		this._ACN3 = acn3;
	}

	public String getACN4() {
		return _ACN4;
	}

	public void setACN4(String acn4) {
		this._ACN4 = acn4;
	}

	public String getICCOD() {
		return _ICCOD;
	}

	public void setICCOD(String iccod) {
		this._ICCOD = iccod;
	}

}
