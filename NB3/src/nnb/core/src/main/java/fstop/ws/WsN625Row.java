package fstop.ws;

import tw.com.fstop.util.DateUtil;

public class WsN625Row {
	
	private String _ACN;
	
	private String _LTD;
	
	private String _MEMO_C;
	
	private String _DPWDAMT;
	
	private String _DPSVAMT;
	
	private String _DPIBAL;
	
	private String _CHKNUM;
	
	private String _DATA1;
	
	private String _ORNBRH;
	
	private String _TIME;

//	private String _TOTAL_DPWDAMT;
	
//	private String _TOTAL_DPSVAMT;
	
	public String getACN() {
		return _ACN;
	}

	public void setACN(String acn) {
		_ACN = acn;
	}

	public String getLTD() {
		return _LTD;
	}

	public void setLTD(String ltd) {
		_LTD = ltd;
	}

	public String getMEMO_C() {
		return _MEMO_C;
	}

	public void setMEMO_C(String memo_c) {
		_MEMO_C = memo_c;
	}

	public String getDPWDAMT() {
		return _DPWDAMT;
	}

	public void setDPWDAMT(String dpwdamt) {
		_DPWDAMT = dpwdamt;
	}

	public String getDPSVAMT() {
		return _DPSVAMT;
	}

	public void setDPSVAMT(String dpsvamt) {
		_DPSVAMT = dpsvamt;
	}

	public String getDPIBAL() {
		return _DPIBAL;
	}

	public void setDPIBAL(String dpibal) {
		_DPIBAL = dpibal;
	}

	public String getCHKNUM() {
		return _CHKNUM;
	}

	public void setCHKNUM(String chknum) {
		_CHKNUM = chknum;
	}

	public String getDATA1() {
		return _DATA1;
	}

	public void setDATA1(String data1) {
		_DATA1 = data1;
	}

	public String getORNBRH() {
		return _ORNBRH;
	}

	public void setORNBRH(String ornbrh) {
		_ORNBRH = ornbrh;
	}

	public String getTIME() {		
		return _TIME;
	}

	public void setTIME(String time) {		
		_TIME = time;
	}
	
/*	public String getTOTALDPWDAMT() {
		return _TOTAL_DPWDAMT;
	}

	public void setTOTALDPWDAMT(String totaldpwdamt) {
		_TOTAL_DPWDAMT = totaldpwdamt;
	}
	
	public String getTOTALDPSVAMT() {
		return _TOTAL_DPSVAMT;
	}

	public void setTOTALDPSVAMT(String totaldpsvamt) {
		_TOTAL_DPSVAMT = totaldpsvamt;
	}
*/	
}
