package fstop.ws;

import java.io.Serializable;

public class WsCN35Row implements Serializable {
	private static final long serialVersionUID = -5523440697726472426L;
	private String BNKCOD;
	private String BNKNAME;
	private String ACN;
	private String APLDATE;
	private String APLTIME;
	private String SEQNO;
	private String VALDATE;
	private String VALTIME;
	private String AMOUNT;
	
	public String getBNKCOD() {
		return BNKCOD;
	}
	public void setBNKCOD(String bNKCOD) {
		BNKCOD = bNKCOD;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getAPLDATE() {
		return APLDATE;
	}
	public void setAPLDATE(String aPLDATE) {
		APLDATE = aPLDATE;
	}
	public String getAPLTIME() {
		return APLTIME;
	}
	public void setAPLTIME(String aPLTIME) {
		APLTIME = aPLTIME;
	}
	public String getSEQNO() {
		return SEQNO;
	}
	public void setSEQNO(String sEQNO) {
		SEQNO = sEQNO;
	}
	public String getVALDATE() {
		return VALDATE;
	}
	public void setVALDATE(String vALDATE) {
		VALDATE = vALDATE;
	}
	public String getVALTIME() {
		return VALTIME;
	}
	public void setVALTIME(String vALTIME) {
		VALTIME = vALTIME;
	}
	public String getAMOUNT() {
		return AMOUNT;
	}
	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}

	public String getBNKNAME() {
		return BNKNAME;
	}

	public void setBNKNAME(String BNKNAME) {
		this.BNKNAME = BNKNAME;
	}
}
