package fstop.ws;

public class WsN816LRow {
	
	// 信用卡別
	private String _TYPENAME;
	
	// 信用卡號
	private String _CARDNUM;
	
	// 持卡人姓名
	private String _CR_NAME;
	
	// 卡片註記
	private String _PT_FLG;
	
	// 卡片效期
	private String _EXP_DATE;
	
	// 電子票證種類 (1：一般信用卡, 2：悠遊卡, 3：一卡通)
	private String _EPC_FLAG;

	public String getTYPENAME() {
		return _TYPENAME;
	}

	public void setTYPENAME(String typename) {
		_TYPENAME = typename;
	}

	public String getCARDNUM() {
		return _CARDNUM;
	}

	public void setCARDNUM(String cardnum) {
		_CARDNUM = cardnum;
	}

	public String getCR_NAME() {
		return _CR_NAME;
	}

	public void setCR_NAME(String cr_name) {
		_CR_NAME = cr_name;
	}

	public String getPT_FLG() {
		return _PT_FLG;
	}

	public void setPT_FLG(String pt_flg) {
		_PT_FLG = pt_flg;
	}

	public String getEXP_DATE() {
		return _EXP_DATE;
	}

	public void setEXP_DATE(String exp_date) {
		_EXP_DATE = exp_date;
	}		
		
	public String getEPC_FLAG() {
		return _EPC_FLAG;
	}

	public void setEPC_FLAG(String epc_flag) {
		_EPC_FLAG = epc_flag;
	}

}
