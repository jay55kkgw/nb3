package fstop.ws;

public class CmGDAcnoInfo {

	private String _MsgCode;

	private String _MsgName;

	private String _TSFBAL;

	private String _GDBAL;

	private String _FEEAMT1;

	private String _FEEAMT2;

	private String _SVACN;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getTSFBAL() {
		return _TSFBAL;
	}

	public void setTSFBAL(String TSFBAL) {
		this._TSFBAL = TSFBAL;
	}

	public String getGDBAL() {
		return _GDBAL;
	}

	public void setGDBAL(String GDBAL) {
		this._GDBAL = GDBAL;
	}

	public String getFEEAMT1() {
		return _FEEAMT1;
	}

	public void setFEEAMT1(String FEEAMT1) {
		this._FEEAMT1 = FEEAMT1;
	}

	public String getFEEAMT2() {
		return _FEEAMT2;
	}

	public void setFEEAMT2(String FEEAMT2) {
		this._FEEAMT2 = FEEAMT2;
	}

	public String getSVACN() {
		return _SVACN;
	}

	public void setSVACN(String SVACN) {
		this._SVACN = SVACN;
	}

}
