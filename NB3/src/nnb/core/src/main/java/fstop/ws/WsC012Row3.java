package fstop.ws;

public class WsC012Row3 {

	private String _UNALLOTAMTCRY;// 未分配金額幣別
	private String _UNALLOTAMT;// 未分配金額

	public String getUNALLOTAMTCRY() {
		return _UNALLOTAMTCRY;
	}

	public void setUNALLOTAMTCRY(String unallotamtcry) {
		this._UNALLOTAMTCRY = unallotamtcry;
	}

	public String getUNALLOTAMT() {
		return _UNALLOTAMT;
	}

	public void setUNALLOTAMT(String unallotamt) {
		this._UNALLOTAMT = unallotamt;
	}

}
