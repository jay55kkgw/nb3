package fstop.ws;

public class WsC032QryOut {

	private String _MsgCode;// 訊息代碼
	private String _MsgName;// 訊息名稱
	private String _CUSIDN;// 身分證字號/統一編號
	private String _Name;// 姓名
	private String _DataCount;// 總計筆數
	private String _FDINVTYPE;// 投資屬性
	private WsC032QryRow[] _Table;//	表格結果
	
	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		_MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		_MsgName = msgName;
	}

	public String getCUSIDN() {
		return _CUSIDN;
	}

	public void setCUSIDN(String cusidn) {
		_CUSIDN = cusidn;
	}

	public String getName() {
		return _Name;
	}

	public void setName(String Name) {
		_Name = Name;
	}

	public String getDataCount() {
		return _DataCount;
	}

	public void setDataCount(String DataCount) {
		_DataCount = DataCount;
	}

	public String getFDINVTYPE() {
		return _FDINVTYPE;
	}

	public void setFDINVTYPE(String FDINVTYPE) {
		_FDINVTYPE = FDINVTYPE;
	}

	public WsC032QryRow[] getTable() {
		return _Table;
	}

	public void setTable(WsC032QryRow[] table) {
		this._Table = table;
	}
	
}
