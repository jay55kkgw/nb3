package fstop.ws;

import java.io.Serializable;

public class WsCN31Out implements Serializable {
	private static final long serialVersionUID = 5786210524023515961L;
	private String msgCode;
	private String msgName; 
	private String CN;
	private String changeDate;

	public String getMsgCode() {
		return msgCode;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public String getMsgName() {
		return msgName;
	}

	public void setMsgName(String msgName) {
		this.msgName = msgName;
	}

	public String getCN() {
		return CN;
	}

	public void setCN(String CN) {
		this.CN = CN;
	}

	public String getChangeDate() {
		return changeDate;
	}

	public void setChangeDate(String changeDate) {
		this.changeDate = changeDate;
	}
}