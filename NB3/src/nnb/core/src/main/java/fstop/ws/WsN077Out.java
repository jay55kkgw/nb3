package fstop.ws;

public class WsN077Out {

	private String _MsgCode;	//訊息代碼	
	private String _MsgName;	//訊息名稱	
	private String _CMQTIME;	//交易時間
	private String _FDPACN;		//存單帳號
	private String _FDPNUM;		//存單號碼
	private String _AMOUNT;		//存單金額		
	private String _TYPE;		//存款種類
	private String _TERM;		//存款期別		
	private String _INTMTH;		//計息方式	
	private String _FGRENCNT;	//轉期次數
	private String _DUEDAT;		//轉存方式
	
	public String getMsgCode() {
		return _MsgCode;
	}
	public void setMsgCode(String msgcode) {
		this._MsgCode = msgcode;
	}

	public String getMsgName() {
		return _MsgName;
	}
	public void setMsgName(String msgname) {
		this._MsgName = msgname;
	}

	public String getCMQTIME() {
		return _CMQTIME;
	}
	public void setCMQTIME(String CMQTIME) {
		this._CMQTIME = CMQTIME;
	}

	public String getFDPACN() {
		return _FDPACN;
	}
	public void setFDPACN(String FDPACN) {
		this._FDPACN = FDPACN;
	}
	
	public String getFDPNUM() {
		return _FDPNUM;
	}
	public void setFDPNUM(String FDPNUM) {
		this._FDPNUM = FDPNUM;
	}

	public String getTERM() {
		return _TERM;
	}
	public void setTERM(String TERM) {
		this._TERM = TERM;
	}

	public String getTYPE() {
		return _TYPE;
	}
	public void setTYPE(String TYPE) {
		this._TYPE = TYPE;
	}
	
	public String getAMOUNT() {
		return _AMOUNT;
	}
	public void setAMOUNT(String amount) {
		this._AMOUNT = amount;
	}

	public String getINTMTH() {
		return _INTMTH;
	}
	public void setINTMTH(String INTMTH) {
		this._INTMTH = INTMTH;
	}

	public String getFGRENCNT() {
		return _FGRENCNT;
	}
	public void setFGRENCNT(String FGRENCNT) {
		this._FGRENCNT = FGRENCNT;
	}

	public String getDUEDAT() {
		return _DUEDAT;
	}
	public void setDUEDAT(String DUEDAT) {
		this._DUEDAT = DUEDAT;
	}

}

