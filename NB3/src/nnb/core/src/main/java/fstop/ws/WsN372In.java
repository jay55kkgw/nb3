package fstop.ws;

public class WsN372In {

	private String _SessionId;// 身份識別
	private String _TXTOKEN;// 防止重送代碼
	private String _FGTXWAY;// 密碼類別
	private String _CMPASSWORD;// 交易密碼
	private String _PINNEW;// 交易密碼SHA1值
	private String _OTPKEY;// OTP動態密碼
	private String _CAPTCHA;// 圖形驗證碼
	private String _TYPE;// 查詢類別
	private String _TRANSCODE;// 基金代碼
	private String _COUNTRYTYPE;// 信託業務別

	private String _TRADEDATE;// 生效日期
	private String _AMT3;// 申購金額
	private String _FCA2;// 手續費
	private String _ACN1;// 新台幣扣帳帳號
	private String _ACN2;// 外幣扣帳帳號
	private String _INTSACN;// 轉入帳號
	private String _AMT5;// 扣款金額
	private String _FCAFEE;// 手續費率
	private String _SSLTXNO;// SSL交易序號
	private String _PAYDAY1;// 扣款日期1
	private String _PAYDAY2;// 扣款日期2
	private String _PAYDAY3;// 扣款日期3
	private String _CRY1;// 幣別
	private String _YIELD;// 停利通知設定
	private String _STOP;// 停損通知設定

	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		this._SessionId = sessionId;
	}

	public String getTXTOKEN() {
		return _TXTOKEN;
	}

	public void setTXTOKEN(String txtoken) {
		this._TXTOKEN = txtoken;
	}

	public String getFGTXWAY() {
		return _FGTXWAY;
	}

	public void setFGTXWAY(String fgtxway) {
		this._FGTXWAY = fgtxway;
	}

	public String getCMPASSWORD() {
		return _CMPASSWORD;
	}

	public void setCMPASSWORD(String cmpassword) {
		this._CMPASSWORD = cmpassword;
	}

	public String getPINNEW() {
		return _PINNEW;
	}

	public void setPINNEW(String pinnew) {
		this._PINNEW = pinnew;
	}

	public String getOTPKEY() {
		return _OTPKEY;
	}

	public void setOTPKEY(String otpkey) {
		this._OTPKEY = otpkey;
	}

	public String getCAPTCHA() {
		return _CAPTCHA;
	}

	public void setCAPTCHA(String captcha) {
		this._CAPTCHA = captcha;
	}

	public String getTYPE() {
		return _TYPE;
	}

	public void setTYPE(String type) {
		this._TYPE = type;
	}

	public String getTRANSCODE() {
		return _TRANSCODE;
	}

	public void setTRANSCODE(String transcode) {
		this._TRANSCODE = transcode;
	}

	public String getCOUNTRYTYPE() {
		return _COUNTRYTYPE;
	}

	public void setCOUNTRYTYPE(String countrytype) {
		this._COUNTRYTYPE = countrytype;
	}

	public String getTRADEDATE() {
		return _TRADEDATE;
	}

	public void setTRADEDATE(String tradedate) {
		this._TRADEDATE = tradedate;
	}

	public String getAMT3() {
		return _AMT3;
	}

	public void setAMT3(String amt3) {
		this._AMT3 = amt3;
	}

	public String getFCA2() {
		return _FCA2;
	}

	public void setFCA2(String fca2) {
		this._FCA2 = fca2;
	}

	public String getACN1() {
		return _ACN1;
	}

	public void setACN1(String acn1) {
		this._ACN1 = acn1;
	}

	public String getACN2() {
		return _ACN2;
	}

	public void setACN2(String acn2) {
		this._ACN2 = acn2;
	}

	public String getINTSACN() {
		return _INTSACN;
	}

	public void setINTSACN(String intsacn) {
		this._INTSACN = intsacn;
	}

	public String getAMT5() {
		return _AMT5;
	}

	public void setAMT5(String amt5) {
		this._AMT5 = amt5;
	}

	public String getFCAFEE() {
		return _FCAFEE;
	}

	public void setFCAFEE(String fcafee) {
		this._FCAFEE = fcafee;
	}

	public String getSSLTXNO() {
		return _SSLTXNO;
	}

	public void setSSLTXNO(String ssltxno) {
		this._SSLTXNO = ssltxno;
	}

	public String getPAYDAY1() {
		return _PAYDAY1;
	}

	public void setPAYDAY1(String payday1) {
		this._PAYDAY1 = payday1;
	}

	public String getPAYDAY2() {
		return _PAYDAY2;
	}

	public void setPAYDAY2(String payday2) {
		this._PAYDAY2 = payday2;
	}

	public String getPAYDAY3() {
		return _PAYDAY3;
	}

	public void setPAYDAY3(String payday3) {
		this._PAYDAY3 = payday3;
	}

	public String getCRY1() {
		return _CRY1;
	}

	public void setCRY1(String cry1) {
		this._CRY1 = cry1;
	}

	public String getYIELD() {
		return _YIELD;
	}

	public void setYIELD(String yield) {
		this._YIELD = yield;
	}

	public String getSTOP() {
		return _STOP;
	}

	public void setSTOP(String stop) {
		this._STOP = stop;
	}

}
