package fstop.ws;

public class CmComCcyIn {
	
	private String _SessionId;

	private String _ACNO;

	private String _PAYCCY;

	private String _PAYCCYNAME;
	
	public String getSessionId() {
		return _SessionId;
	}
	public void setSessionId(String sessionid) {
		this._SessionId = sessionid;
	}

	public String getACNO() {
		return _ACNO;
	}
	public void setACNO(String acno) {
		this._ACNO = acno;
	}

	public String getPAYCCY() {
		return _PAYCCY;
	}
	public void setPAYCCY(String payccy) {
		this._PAYCCY = payccy;
	}

	public String getPAYCCYNAME() {
		return _PAYCCYNAME;
	}
	public void setPAYCCYNAME(String payccyname) {
		this._PAYCCYNAME = payccyname;
	}
}
