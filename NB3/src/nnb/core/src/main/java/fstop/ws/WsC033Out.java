package fstop.ws;

public class WsC033Out {

	private String _MsgCode;// 訊息代碼
	private String _MsgName;// 訊息名稱
	private String _CUSIDN;// 統一編號
	private String _Name;// 姓名
	private String _DataCount;// 信託號碼
	private WsC033Row[] _Table;//	表格結果
	
	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		_MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		_MsgName = msgName;
	}

	public String getCUSIDN() {
		return _CUSIDN;
	}

	public void setCUSIDN(String cusidn) {
		_CUSIDN = cusidn;
	}

	public String getName() {
		return _Name;
	}

	public void setName(String Name) {
		_Name = Name;
	}

	public String getDataCount() {
		return _DataCount;
	}

	public void setDataCount(String DataCount) {
		_DataCount = DataCount;
	}

	public WsC033Row[] getTable() {
		return _Table;
	}

	public void setTable(WsC033Row[] table) {
		this._Table = table;
	}
	
}
