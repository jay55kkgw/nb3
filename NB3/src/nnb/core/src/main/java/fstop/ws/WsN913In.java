package fstop.ws;

public class WsN913In {

	private String _SessionId;

	private String _N950PASSWORD;

	private String _OPTION;

	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		this._SessionId = sessionId;
	}

	public String getN950PASSWORD() {
		return _N950PASSWORD;
	}

	public void setN950PASSWORD(String n950password) {
		this._N950PASSWORD = n950password;
	}

	public String getOPTION() {
		return _OPTION;
	}

	public void setOPTION(String option) {
		this._OPTION = option;
	}

}
