package fstop.ws;

public class WsN022Row1 {

	private String _COLOR;

	private String _CRY;

	private String _CRYNAME;
	
	private String _RECNO;

	private String _ITR1;

	private String _ITR2;

	private String _ITR3;

	private String _ITR4;

	private String _ITR5;

	private String _ITR6;

	private String _ITR7;

	private String _ITR8;

	private String _ITR9;

	private String _ITR10;

	private String _ITR11;

	private String _ITR12;

	private String _ITR13;

	private String _ITR14;

	private String _ITR15;

	private String _ITR16;

	private String _ITR17;

	private String _ITR18;

	private String _ITR19;

	private String _ITR20;

	public String getCOLOR() {
		return _COLOR;
	}

	public void setCOLOR(String color) {
		this._COLOR = color;
	}

	public String getCRY() {
		return _CRY;
	}

	public void setCRY(String cry) {
		this._CRY = cry;
	}

	public String getCRYNAME() {
		return _CRYNAME;
	}

	public void setCRYNAME(String cryname) {
		this._CRYNAME = cryname;
	}

	public String getRECNO() {
		return _RECNO;
	}

	public void setRECNO(String recno) {
		this._RECNO = recno;
	}
	public String getITR1() {
		return _ITR1;
	}

	public void setITR1(String itr1) {
		this._ITR1 = itr1;
	}

	public String getITR2() {
		return _ITR2;
	}

	public void setITR2(String itr2) {
		this._ITR2 = itr2;
	}

	public String getITR3() {
		return _ITR3;
	}

	public void setITR3(String itr3) {
		this._ITR3 = itr3;
	}

	public String getITR4() {
		return _ITR4;
	}

	public void setITR4(String itr4) {
		this._ITR4 = itr4;
	}

	public String getITR5() {
		return _ITR5;
	}

	public void setITR5(String itr5) {
		this._ITR5 = itr5;
	}

	public String getITR6() {
		return _ITR6;
	}

	public void setITR6(String itr6) {
		this._ITR6 = itr6;
	}

	public String getITR7() {
		return _ITR7;
	}

	public void setITR7(String itr7) {
		this._ITR7 = itr7;
	}

	public String getITR8() {
		return _ITR8;
	}

	public void setITR8(String itr8) {
		this._ITR8 = itr8;
	}

	public String getITR9() {
		return _ITR9;
	}

	public void setITR9(String itr9) {
		this._ITR9 = itr9;
	}

	public String getITR10() {
		return _ITR10;
	}

	public void setITR10(String itr10) {
		this._ITR10 = itr10;
	}

	public String getITR11() {
		return _ITR11;
	}

	public void setITR11(String itr11) {
		this._ITR11 = itr11;
	}

	public String getITR12() {
		return _ITR12;
	}

	public void setITR12(String itr12) {
		this._ITR12 = itr12;
	}

	public String getITR13() {
		return _ITR13;
	}

	public void setITR13(String itr13) {
		this._ITR13 = itr13;
	}

	public String getITR14() {
		return _ITR14;
	}

	public void setITR14(String itr14) {
		this._ITR14 = itr14;
	}

	public String getITR15() {
		return _ITR15;
	}

	public void setITR15(String itr15) {
		this._ITR15 = itr15;
	}

	public String getITR16() {
		return _ITR16;
	}

	public void setITR16(String itr16) {
		this._ITR16 = itr16;
	}

	public String getITR17() {
		return _ITR17;
	}

	public void setITR17(String itr17) {
		this._ITR17 = itr17;
	}

	public String getITR18() {
		return _ITR18;
	}

	public void setITR18(String itr18) {
		this._ITR18 = itr18;
	}

	public String getITR19() {
		return _ITR19;
	}

	public void setITR19(String itr19) {
		this._ITR19 = itr19;
	}

	public String getITR20() {
		return _ITR20;
	}

	public void setITR20(String itr20) {
		this._ITR20 = itr20;
	}

}
