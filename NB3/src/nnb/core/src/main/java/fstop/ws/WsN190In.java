package fstop.ws;

public class WsN190In {

	private String _SessionId;//	身份識別
	
	private String _N950PASSWORD;//	交易密碼

	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		this._SessionId = sessionId;
	}

	public String getN950PASSWORD() {
		return _N950PASSWORD;
	}

	public void setN950PASSWORD(String n950password) {
		this._N950PASSWORD = n950password;
	}
	
	

}
