package fstop.ws;

public class WsN191In {

	private String _SessionId;//	身份識別
	
	private String _N950PASSWORD;//	交易密碼
	
	private String _ACN;//	帳號
	
	private String _FGPERIOD;//	查詢期間
	
	private String _CMSDATE;//	日期(起)
	
	private String _CMEDATE;//	日期(迄)
	
	private String _QUERYNEXT;//	繼續查詢key1
	
	private String _BOQUERYNEXT;//	繼續查詢Key2


	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		this._SessionId = sessionId;
	}

	public String getN950PASSWORD() {
		return _N950PASSWORD;
	}

	public void setN950PASSWORD(String n950password) {
		this._N950PASSWORD = n950password;
	}

	public String getACN() {
		return _ACN;
	}

	public void setACN(String acn) {
		this._ACN = acn;
	}

	public String getFGPERIOD() {
		return _FGPERIOD;
	}

	public void setFGPERIOD(String fgperiod) {
		this._FGPERIOD = fgperiod;
	}

	public String getCMSDATE() {
		return _CMSDATE;
	}

	public void setCMSDATE(String cmsdate) {
		this._CMSDATE = cmsdate;
	}

	public String getCMEDATE() {
		return _CMEDATE;
	}

	public void setCMEDATE(String cmedate) {
		this._CMEDATE = cmedate;
	}

	public String getQUERYNEXT() {
		return _QUERYNEXT;
	}

	public void setQUERYNEXT(String querynext) {
		this._QUERYNEXT = querynext;
	}

	public String getBOQUERYNEXT() {
		return _BOQUERYNEXT;
	}

	public void setBOQUERYNEXT(String boquerynext) {
		this._BOQUERYNEXT = boquerynext;
	}
	
	

}
