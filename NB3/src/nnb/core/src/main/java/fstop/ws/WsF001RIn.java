package fstop.ws;

public class WsF001RIn {

	private String _SessionId;

	private String _TXTOKEN;

	private String _FGINACNO;

	private String _INACNO1;

	private String _INACNO2;

	private String _FGTRDATE;

	private String _CUSTACC;

	private String _PAYCCY;

	private String _REMITCY;

	private String _PAYREMIT;

	private String _CURAMT;

	public String getSessionId() {
		return _SessionId;
	}
	public void setSessionId(String sessionid) {
		this._SessionId = sessionid;
	}

	public String getTXTOKEN() {
		return _TXTOKEN;
	}
	public void setTXTOKEN(String txtoken) {
		this._TXTOKEN = txtoken;
	}

	public String getFGINACNO() {
		return _FGINACNO;
	}
	public void setFGINACNO(String fginacno) {
		this._FGINACNO = fginacno;
	}

	public String getINACNO1() {
		return _INACNO1;
	}
	public void setINACNO1(String inacno1) {
		this._INACNO1 = inacno1;
	}

	public String getINACNO2() {
		return _INACNO2;
	}
	public void setINACNO2(String inacno2) {
		this._INACNO2 = inacno2;
	}

	public String getFGTRDATE() {
		return _FGTRDATE;
	}
	public void setFGTRDATE(String fgtrdate) {
		this._FGTRDATE = fgtrdate;
	}

	public String getCUSTACC() {
		return _CUSTACC;
	}
	public void setCUSTACC(String custacc) {
		this._CUSTACC = custacc;
	}

	public String getPAYCCY() {
		return _PAYCCY;
	}
	public void setPAYCCY(String payccy) {
		this._PAYCCY = payccy;
	}

	public String getREMITCY() {
		return _REMITCY;
	}
	public void setREMITCY(String remitcy) {
		this._REMITCY = remitcy;
	}

	public String getPAYREMIT() {
		return _PAYREMIT;
	}
	public void setPAYREMIT(String payremit) {
		this._PAYREMIT = payremit;
	}

	public String getCURAMT() {
		return _CURAMT;
	}
	public void setCURAMT(String curamt) {
		this._CURAMT = curamt;
	}

}
