package fstop.ws;

public class WsN015In {

	private String _SessionId;

	private String _N950PASSWORD;

	private String _ACN;

	private String _QUERYNEXT;

	private String _BOQUERYNEXT;

	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		this._SessionId = sessionId;
	}

	public void setN950PASSWORD(String N950PASSWORD) {
		this._N950PASSWORD = N950PASSWORD;
	}

	public String getN950PASSWORD() {
		return _N950PASSWORD;
	}

	public String getACN() {
		return _ACN;
	}

	public void setACN(String acn) {
		this._ACN = acn;
	}

	public String getQUERYNEXT() {
		return _QUERYNEXT;
	}

	public void setQUERYNEXT(String querynext) {
		this._QUERYNEXT = querynext;
	}

	public String getBOQUERYNEXT() {
		return _BOQUERYNEXT;
	}

	public void setBOQUERYNEXT(String boquerynext) {
		this._BOQUERYNEXT = boquerynext;
	}

}
