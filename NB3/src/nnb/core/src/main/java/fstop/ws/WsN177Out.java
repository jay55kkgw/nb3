package fstop.ws;

public class WsN177Out {

	private String MsgCode;//	訊息代碼
	private String MsgName;//	訊息名稱
	private String CMQTIME;//	交易時間
	private String FYACN;//	存單帳號
	private String FDPNUM;//	存單號碼
	private String AMTFDP;//	存單金額
	private String CRY;//	幣別
	private String DEPTYPE;//	存款種類
	private String INTMTH;//	計息方式
	private String AUTXFTM;//	轉期次數
	private String CODE;//	轉存方式
	private String FYTSFAN;//利息轉入帳號
	public String getMsgCode() {
		return MsgCode;
	}
	public void setMsgCode(String msgCode) {
		MsgCode = msgCode;
	}
	public String getMsgName() {
		return MsgName;
	}
	public void setMsgName(String msgName) {
		MsgName = msgName;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getFYACN() {
		return FYACN;
	}
	public void setFYACN(String fYACN) {
		FYACN = fYACN;
	}
	public String getFDPNUM() {
		return FDPNUM;
	}
	public void setFDPNUM(String fDPNUM) {
		FDPNUM = fDPNUM;
	}
	public String getAMTFDP() {
		return AMTFDP;
	}
	public void setAMTFDP(String aMTFDP) {
		AMTFDP = aMTFDP;
	}
	public String getCRY() {
		return CRY;
	}
	public void setCRY(String cRY) {
		CRY = cRY;
	}
	public String getDEPTYPE() {
		return DEPTYPE;
	}
	public void setDEPTYPE(String dEPTYPE) {
		DEPTYPE = dEPTYPE;
	}
	public String getINTMTH() {
		return INTMTH;
	}
	public void setINTMTH(String iNTMTH) {
		INTMTH = iNTMTH;
	}
	public String getAUTXFTM() {
		return AUTXFTM;
	}
	public void setAUTXFTM(String aUTXFTM) {
		AUTXFTM = aUTXFTM;
	}
	public String getCODE() {
		return CODE;
	}
	public void setCODE(String cODE) {
		CODE = cODE;
	}
	public String getFYTSFAN() {
		return FYTSFAN;
	}
	public void setFYTSFAN(String fytsfan) {
		FYTSFAN = fytsfan;
	}

	
}
