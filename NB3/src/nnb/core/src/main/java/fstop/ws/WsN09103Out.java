package fstop.ws;

public class WsN09103Out {

	private String _MsgCode;

	private String _MsgName;

	private String _APPTYPE;

	private String _APPDATE;

	private String _SVACN;
	
	private String _ACN;

	private String _TRNGD;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getAPPTYPE() {
		return _APPTYPE;
	}

	public void setAPPTYPE(String APPTYPE) {
		this._APPTYPE = APPTYPE;
	}

	public String getAPPDATE() {
		return _APPDATE;
	}

	public void setAPPDATE(String APPDATE) {
		this._APPDATE = APPDATE;
	}

	public String getSVACN() {
		return _SVACN;
	}

	public void setSVACN(String SVACN) {
		this._SVACN = SVACN;
	}

	public String getACN() {
		return _ACN;
	}

	public void setACN(String ACN) {
		this._ACN = ACN;
	}
	
	public String getTRNGD() {
		return _TRNGD;
	}

	public void setTRNGD(String TRNGD) {
		this._TRNGD = TRNGD;
	}

}
