package fstop.ws;

public class WsN283FXRow {

	private String _FXSCHNO;

	private String _FXPERMTDATE;

	private String _FXFTDATE;

	private String _FXNEXTDATE;

	private String _FXWDAC;

	private String _FXWDCURR;

	private String _FXWDAMT;

	private String _FXSVAC;

	private String _FXSVCURR;

	private String _FXSVAMT;

	private String _TXTYPE;

	private String _FXTXMEMO;

	private String _TXWAY;

	private String _FXTXCODE;

	private String _FXSCHID;
	
	public String getFXSCHNO() {
		return _FXSCHNO;
	}
	public void setFXSCHNO(String fxschno) {
		this._FXSCHNO = fxschno;
	}

	public String getFXPERMTDATE() {
		return _FXPERMTDATE;
	}
	public void setFXPERMTDATE(String fxpermtdate) {
		this._FXPERMTDATE = fxpermtdate;
	}

	public String getFXFTDATE() {
		return _FXFTDATE;
	}
	public void setFXFTDATE(String fxftdate) {
		this._FXFTDATE = fxftdate;
	}

	public String getFXNEXTDATE() {
		return _FXNEXTDATE;
	}
	public void setFXNEXTDATE(String fxnextdate) {
		this._FXNEXTDATE = fxnextdate;
	}

	public String getFXWDAC() {
		return _FXWDAC;
	}
	public void setFXWDAC(String fxwdac) {
		this._FXWDAC = fxwdac;
	}

	public String getFXWDCURR() {
		return _FXWDCURR;
	}
	public void setFXWDCURR(String fxwdcurr) {
		this._FXWDCURR = fxwdcurr;
	}

	public String getFXWDAMT() {
		return _FXWDAMT;
	}
	public void setFXWDAMT(String fxwdamt) {
		this._FXWDAMT = fxwdamt;
	}

	public String getFXSVAC() {
		return _FXSVAC;
	}
	public void setFXSVAC(String fxsvac) {
		this._FXSVAC = fxsvac;
	}

	public String getFXSVCURR() {
		return _FXSVCURR;
	}
	public void setFXSVCURR(String fxsvcurr) {
		this._FXSVCURR = fxsvcurr;
	}

	public String getFXSVAMT() {
		return _FXSVAMT;
	}
	public void setFXSVAMT(String fxsvamt) {
		this._FXSVAMT = fxsvamt;
	}

	public String getTXTYPE() {
		return _TXTYPE;
	}
	public void setTXTYPE(String txtype) {
		this._TXTYPE = txtype;
	}

	public String getFXTXMEMO() {
		return _FXTXMEMO;
	}
	public void setFXTXMEMO(String fxtxmemo) {
		this._FXTXMEMO = fxtxmemo;
	}

	public String getTXWAY() {
		return _TXWAY;
	}
	public void setTXWAY(String txway) {
		this._TXWAY = txway;
	}

	public String getFXTXCODE() {
		return _FXTXCODE;
	}
	public void setFXTXCODE(String fxtxcode) {
		this._FXTXCODE = fxtxcode;
	}

	public String getFXSCHID() {
		return _FXSCHID;
	}
	public void setFXSCHID(String fxschid) {
		this._FXSCHID = fxschid;
	}

}
