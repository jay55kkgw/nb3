package fstop.ws;

import java.io.Serializable;

public class WsCN19Out implements Serializable {
	private static final long serialVersionUID = 1772059889123524796L;
	private String msgCode;
	private String msgName;

	private String CN;

	private String DATAPL;
	private String DATTIM;
	public String getMsgCode() {
		return msgCode;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public String getMsgName() {
		return msgName;
	}

	public void setMsgName(String msgName) {
		this.msgName = msgName;
	} 

	public String getCN() {
		return CN;
	}

	public void setCN(String CN) {
		this.CN = CN;
	}

	public String getDATAPL() {
		return DATAPL;
	}

	public void setDATAPL(String DATAPL) {
		this.DATAPL = DATAPL;
	}

	public String getDATTIM() {
		return DATTIM;
	}

	public void setDATTIM(String DATTIM) {
		this.DATTIM = DATTIM;
	}
}