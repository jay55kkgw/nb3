package fstop.ws;

public class CmBhContactIn {
	
	private String _SessionId;

	private String _BhID;
	
	public String getSessionId() {
		return _SessionId;
	}
	public void setSessionId(String sessionid) {
		this._SessionId = sessionid;
	}

	public String getBhID() {
		return _BhID;
	}
	public void setBhID(String bhid) {
		this._BhID = bhid;
	}
}
