package fstop.ws;

public class WsGD11LDIn {

	private String _SessionId;// 身份識別

	private String _N950PASSWORD;// 交易密碼

	private String _DATE;// 查詢日期

	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		this._SessionId = sessionId;
	}

	public String getN950PASSWORD() {
		return _N950PASSWORD;
	}

	public void setN950PASSWORD(String n950password) {
		this._N950PASSWORD = n950password;
	}

	public String getDATE() {
		return _DATE;
	}

	public void setDATE(String date) {
		this._DATE = date;
	}
	
	

}
