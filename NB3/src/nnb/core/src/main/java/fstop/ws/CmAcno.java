package fstop.ws;

public class CmAcno {
	
	private String _MsgCode;
	
	private String _MsgName;
	
	private CmAcnoRow[] _Table;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public CmAcnoRow[] getTable() {
		return _Table;
	}

	public void setTable(CmAcnoRow[] table) {
		this._Table = table;
	}
	
	
}
