package fstop.ws;

public class WsInvestAttrIn {

	private String _SessionId;// 身份識別

	private String[] _List;// 問卷答案清單
	
	private String _N950PASSWORD;// 交易密碼

	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		this._SessionId = sessionId;
	}

	public String[] getList() {
		return _List;
	}

	public void setList(String[] list) {
		this._List = list;
	}

	public String getN950PASSWORD() {
		return _N950PASSWORD;
	}

	public void setN950PASSWORD(String n950password) {
		this._N950PASSWORD = n950password;
	}
}
