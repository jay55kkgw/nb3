package fstop.ws;

public class WsN816AIn {

	// 身份證別
	private String _SessionId;

	// 防止重送代碼
	private String _TXTOKEN;

	// 圖形驗證碼
	private String _CAPTCHA;	
	
	// 密碼類別
	private String _FGTXWAY;

	// 交易密碼
	private String _CMPASSWORD;

	// 交易密碼 SHA1
	private String _PINNEW;

	// 信用卡號
	private String _CARDNUM;

	// 卡片效期
	private String _EXP_DATE;

	// 開卡密碼(生日)
	private String _ACT_PASS;
	
	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		this._SessionId = sessionId;
	}

	public void setTXTOKEN(String TXTOKEN) {
		this._TXTOKEN = TXTOKEN;
	}

	public String getTXTOKEN() {
		return _TXTOKEN;
	}

	public String getCAPTCHA() {
		return _CAPTCHA;
	}

	public void setCAPTCHA(String CAPTCHA) {
		this._CAPTCHA = CAPTCHA;
	}
	public String getFGTXWAY() {
		return _FGTXWAY;
	}

	public void setFGTXWAY(String fgtxway) {
		this._FGTXWAY = fgtxway;
	}

	public String getCMPASSWORD() {
		return _CMPASSWORD;
	}

	public void setCMPASSWORD(String CMPASSWORD) {
		this._CMPASSWORD = CMPASSWORD;
	}

	public String getPINNEW() {
		return _PINNEW;
	}

	public void setPINNEW(String pinnew) {
		this._PINNEW = pinnew;
	}

	public String getCARDNUM() {
		return _CARDNUM;
	}

	public void setCARDNUM(String cardnum) {
		this._CARDNUM = cardnum;
	}

	public String getEXP_DATE() {
		return _EXP_DATE;
	}

	public void setEXP_DATE(String exp_date) {
		this._EXP_DATE = exp_date;
	}

	public String getACT_PASS() {
		return _ACT_PASS;
	}

	public void setACT_PASS(String act_pass) {
		this._ACT_PASS = act_pass;
	}

}
