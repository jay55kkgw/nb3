package fstop.ws;

public class WsN820Row {
	
	private String _RDETAIL20R1;
	
	private String _RDETAIL20R2C1;
	
	private String _RDETAIL20R2C3;
	
	private String _RDETAIL20R2C5;
	
	private String _RDETAIL20R2C7;
	
	private String _RDETAIL20R2C8;
	
	private String _RDETAIL20R2C5NAME;
	
	public String getRDETAIL20R1() {
		return _RDETAIL20R1;
	}

	public void setRDETAIL20R1(String RDETAIL20R1) {
		_RDETAIL20R1 = RDETAIL20R1;
	}

	public String getRDETAIL20R2C1() {
		return _RDETAIL20R2C1;
	}

	public void setRDETAIL20R2C1(String RDETAIL20R2C1) {
		_RDETAIL20R2C1 = RDETAIL20R2C1;
	}

	public String getRDETAIL20R2C3() {
		return _RDETAIL20R2C3;
	}

	public void setRDETAIL20R2C3(String RDETAIL20R2C3) {
		_RDETAIL20R2C3 = RDETAIL20R2C3;
	}

	public String getRDETAIL20R2C5() {
		return _RDETAIL20R2C5;
	}

	public void setRDETAIL20R2C5(String RDETAIL20R2C5) {
		_RDETAIL20R2C5 = RDETAIL20R2C5;
	}

	public String getRDETAIL20R2C7() {
		return _RDETAIL20R2C7;
	}

	public void setRDETAIL20R2C7(String RDETAIL20R2C7) {
		_RDETAIL20R2C7 = RDETAIL20R2C7;
	}

	public String getRDETAIL20R2C8() {
		return _RDETAIL20R2C8;
	}

	public void setRDETAIL20R2C8(String RDETAIL20R2C8) {
		_RDETAIL20R2C8 = RDETAIL20R2C8;
	}

	public String getRDETAIL20R2C5NAME() {
		return _RDETAIL20R2C5NAME;
	}

	public void setRDETAIL20R2C5NAME(String RDETAIL20R2C5NAME) {
		_RDETAIL20R2C5NAME = RDETAIL20R2C5NAME;
	}
	
}
