package fstop.ws;

public class WsBM8000In {

	private String _SessionId;
	private String _CrmId;
	
	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionid) {
		this._SessionId = sessionid;
	}

	public String getCrmId() {
		return _CrmId;
	}

	public void setCrmId(String crmId) {
		_CrmId = crmId;
	}

	

}
