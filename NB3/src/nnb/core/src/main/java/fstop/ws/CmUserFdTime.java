package fstop.ws;

public class CmUserFdTime {

	private String _SessionId;//	身份識別
	private String _isTraKind;//	交易類別
	public String getSessionId() {
		return _SessionId;
	}
	public void setSessionId(String sessionId) {
		this._SessionId = sessionId;
	}
	public String getisTraKind() {
		return _isTraKind;
	}
	public void setisTraKind(String traKind) {
		this._isTraKind = traKind;
	}

}
