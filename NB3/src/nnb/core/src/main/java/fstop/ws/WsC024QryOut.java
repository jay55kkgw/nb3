package fstop.ws;

public class WsC024QryOut {

	private String _MsgCode;//
	
	private String _MsgName;//
	
	private WsC024QryRow[] _Table;//

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public WsC024QryRow[] getTable() {
		return _Table;
	}

	public void setTable(WsC024QryRow[] table) {
		this._Table = table;
	}
	

}
