package fstop.ws;

public class WsBM2000In {

	private String _ADOS;

	private String _ADMODEL;

	private String _ADVERNO;

	public String getADOS() {
		return _ADOS;
	}

	public void setADOS(String ADOS) {
		this._ADOS = ADOS;
	}

	public void setADMODEL(String ADMODEL) {
		this._ADMODEL = ADMODEL;
	}

	public String getADMODEL() {
		return _ADMODEL;
	}

	public String getADVERNO() {
		return _ADVERNO;
	}

	public void setADVERNO(String ADVERNO) {
		this._ADVERNO = ADVERNO;
	}

}
