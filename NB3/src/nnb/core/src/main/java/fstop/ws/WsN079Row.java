package fstop.ws;

public class WsN079Row {
	
	private String _ACN;		//帳號
	private String _TYPE;		//存款種類	
	private String _FDPNUM;		//存單號碼
	private String _AMTFDP;		//存單金額
	private String _ITR;		//利率
	private String _INTMTH;		//計息方式
	private String _DPISDT;		//起存日
	private String _DUEDAT;		//到期日
	private String _ILAZLFTM;	//自動轉期已轉次數
	private String _AUTXFTM;	//自動轉期未轉次數
	private String _STATUS;		//狀態
	private String _REMARK;		//備註
	
	public String getACN() {
		return _ACN;
	}

	public void setACN(String ACN) {
		this._ACN = ACN;
	}

	public String getTYPE() {
		return _TYPE;
	}

	public void setTYPE(String TYPE) {
		this._TYPE = TYPE;
	}

	public String getFDPNUM() {
		return _FDPNUM;
	}

	public void setFDPNUM(String FDPNUM) {
		this._FDPNUM = FDPNUM;
	}

	public String getAMTFDP() {
		return _AMTFDP;
	}

	public void setAMTFDP(String AMTFDP) {
		this._AMTFDP = AMTFDP;
	}

	public String getITR() {
		return _ITR;
	}

	public void setITR(String ITR) {
		this._ITR = ITR;
	}

	public String getINTMTH() {
		return _INTMTH;
	}

	public void setINTMTH(String INTMTH) {
		this._INTMTH = INTMTH;
	}
	
	public String getDPISDT() {
		return _DPISDT;
	}

	public void setDPISDT(String DPISDT) {
		this._DPISDT = DPISDT;
	}
	
	public String getDUEDAT() {
		return _DUEDAT;
	}

	public void setDUEDAT(String DUEDAT) {
		this._DUEDAT = DUEDAT;
	}
	
	public String getILAZLFTM() {
		return _ILAZLFTM;
	}

	public void setILAZLFTM(String ILAZLFTM) {
		this._ILAZLFTM = ILAZLFTM;
	}
	
	public String getAUTXFTM() {
		return _AUTXFTM;
	}

	public void setAUTXFTM(String AUTXFTM) {
		this._AUTXFTM = AUTXFTM;
	}
	
	public String getSTATUS() {
		return _STATUS;
	}

	public void setSTATUS(String STATUS) {
		this._STATUS = STATUS;
	}
	public String getREMARK() {
		return _REMARK;
	}

	public void setREMARK(String remark) {
		this._REMARK = remark;
	}
	
}

