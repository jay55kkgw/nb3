package fstop.ws;

public class WsC020In {

	private String _SessionId;

	private String _TXTOKEN;

	private String _FGTXWAY;

	private String _CMPASSWORD;

	private String _PINNEW;

	private String _OTPKEY;

	private String _FDINVTYPE;

	private String _COUNTRYTYPE;

	private String _TRANSCODE;

	private String _HTELPHONE;

	private String _CAPTCHA;

	private String _AMT3;

	private String _PAYDAY1;

	private String _PAYDAY2;

	private String _PAYDAY3;

	private String _YIELD;

	private String _STOP;

	private String _PAYTYPE;

	private String _FUNDACN;
	
	private String _SSLTXNO;
	
	private String _TRADEDATE;
	
	private String _FCA2;
	
	private String _FCAFEE;
	
	private String _DBDATE;// 首次扣日期

	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionid) {
		this._SessionId = sessionid;
	}

	public String getTXTOKEN() {
		return _TXTOKEN;
	}

	public void setTXTOKEN(String txtoken) {
		this._TXTOKEN = txtoken;
	}

	public String getFGTXWAY() {
		return _FGTXWAY;
	}

	public void setFGTXWAY(String fgtxway) {
		this._FGTXWAY = fgtxway;
	}

	public String getCMPASSWORD() {
		return _CMPASSWORD;
	}

	public void setCMPASSWORD(String cmpassword) {
		this._CMPASSWORD = cmpassword;
	}

	public String getPINNEW() {
		return _PINNEW;
	}

	public void setPINNEW(String pinnew) {
		this._PINNEW = pinnew;
	}

	public String getOTPKEY() {
		return _OTPKEY;
	}

	public void setOTPKEY(String otpkey) {
		this._OTPKEY = otpkey;
	}

	public String getFDINVTYPE() {
		return _FDINVTYPE;
	}

	public void setFDINVTYPE(String fdinvtype) {
		this._FDINVTYPE = fdinvtype;
	}

	public String getCOUNTRYTYPE() {
		return _COUNTRYTYPE;
	}

	public void setCOUNTRYTYPE(String countrytype) {
		this._COUNTRYTYPE = countrytype;
	}

	public String getTRANSCODE() {
		return _TRANSCODE;
	}

	public void setTRANSCODE(String transcode) {
		this._TRANSCODE = transcode;
	}

	public String getHTELPHONE() {
		return _HTELPHONE;
	}

	public void setHTELPHONE(String htelphone) {
		this._HTELPHONE = htelphone;
	}

	public String getCAPTCHA() {
		return _CAPTCHA;
	}

	public void setCAPTCHA(String captcha) {
		this._CAPTCHA = captcha;
	}

	public String getAMT3() {
		return _AMT3;
	}

	public void setAMT3(String amt3) {
		this._AMT3 = amt3;
	}

	public String getPAYDAY1() {
		return _PAYDAY1;
	}

	public void setPAYDAY1(String payday1) {
		this._PAYDAY1 = payday1;
	}

	public String getPAYDAY2() {
		return _PAYDAY2;
	}

	public void setPAYDAY2(String payday2) {
		this._PAYDAY2 = payday2;
	}

	public String getPAYDAY3() {
		return _PAYDAY3;
	}

	public void setPAYDAY3(String payday3) {
		this._PAYDAY3 = payday3;
	}

	public String getYIELD() {
		return _YIELD;
	}

	public void setYIELD(String yield) {
		this._YIELD = yield;
	}

	public String getSTOP() {
		return _STOP;
	}

	public void setSTOP(String stop) {
		this._STOP = stop;
	}

	public String getPAYTYPE() {
		return _PAYTYPE;
	}

	public void setPAYTYPE(String paytype) {
		this._PAYTYPE = paytype;
	}

	public String getFUNDACN() {
		return _FUNDACN;
	}

	public void setFUNDACN(String fundacn) {
		this._FUNDACN = fundacn;
	}
	
	public String getSSLTXNO() {
		return _SSLTXNO;
	}

	public void setSSLTXNO(String ssltxno) {
		this._SSLTXNO = ssltxno;
	}
	public String getTRADEDATE() {
		return _TRADEDATE;
	}

	public void setTRADEDATE(String tradedate) {
		this._TRADEDATE = tradedate;
	}

	public String getFCA2() {
		return _FCA2;
	}

	public void setFCA2(String fca2) {
		this._FCA2 = fca2;
	}

	public String getFCAFEE() {
		return _FCAFEE;
	}

	public void setFCAFEE(String fcafee) {
		this._FCAFEE = fcafee;
	}

	public String getDBDATE() {
		return _DBDATE;
	}

	public void setDBDATE(String dbdate) {
		this._DBDATE = dbdate;
	}

}
