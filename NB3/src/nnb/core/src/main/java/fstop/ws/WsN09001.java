package fstop.ws;

public interface WsN09001{

	public WsN09001Out getPrice(WsN09001SS params); 
	
	public WsN09001Out action (WsN09001In params);
}