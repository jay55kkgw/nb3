package fstop.ws;

public class WsN900In {
	
	private String _SessionId;
	
	private String _newZIP;
	
	private String _newADDR;
	
	private String _newHOMEPHONE;
	
	private String _newOFFICEPHONE;
	
	private String _newOFFICEEXT;
	
	private String _newMOBILEPHONE;
	
	private String _TXTOKEN;
	
	private String _OTPKEY;
	
	private String _CAPTCHA;
	
	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		_SessionId = sessionId;
	}

	public String getnewZIP() {
		return _newZIP;
	}

	public void setnewZIP(String newZIP) {
		_newZIP = newZIP;
	}

	public String getnewADDR() {
		return _newADDR;
	}

	public void setnewADDR(String newADDR) {
		_newADDR = newADDR;
	}

	public String getnewHOMEPHONE() {
		return _newHOMEPHONE;
	}

	public void setnewHOMEPHONE(String newHOMEPHONE) {
		_newHOMEPHONE = newHOMEPHONE;
	}

	public String getnewOFFICEPHONE() {
		return _newOFFICEPHONE;
	}

	public void setnewOFFICEPHONE(String newOFFICEPHONE) {
		this._newOFFICEPHONE = newOFFICEPHONE;
	}

	public String getnewOFFICEEXT() {
		return _newOFFICEEXT;
	}

	public void setnewOFFICEEXT(String newOFFICEEXT) {
		this._newOFFICEEXT = newOFFICEEXT;
	}

	public String getnewMOBILEPHONE() {
		return _newMOBILEPHONE;
	}

	public void setnewMOBILEPHONE(String newMOBILEPHONE) {
		_newMOBILEPHONE = newMOBILEPHONE;
	}	
	
	public String getTXTOKEN() {
		return _TXTOKEN;
	}

	public void setTXTOKEN(String txtoken) {
		_TXTOKEN = txtoken;
	}
	
	public String getOTPKEY() {
		return _OTPKEY;
	}

	public void setOTPKEY(String otpkey) {
		_OTPKEY = otpkey;
	}
	
	public String getCAPTCHA() {
		return _CAPTCHA;
	}

	public void setCAPTCHA(String captcha) {
		_CAPTCHA = captcha;
	}
	
}
