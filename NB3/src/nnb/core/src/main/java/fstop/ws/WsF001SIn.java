package fstop.ws;

public class WsF001SIn {

	private String _SessionId;

	private String _TXTOKEN;

	private String _FGTXWAY;

	private String _CMPASSWORD;

	private String _PINNEW;

	private String _OTPKEY;

	private String _ADOPID;

	private String _FGINACNO;

	private String _INACNO1;

	private String _INACNO2;

	private String _FGTRDATE;

	private String _CMTRDATE;

	private String _CMSDATE;

	private String _CMEDATE;

	private String _CMPERIOD;

	private String _CUSTACC;

	private String _PAYCCY;

	private String _REMITCY;

	private String _PAYREMIT;

	private String _CURAMT;

	private String _CUSIDN;

	private String _UID;

	private String _COMMCCY;

	private String _COMMACC;

	private String _PAYDATE;

	private String _NAME;

	private String _MEMO1;

	private String _CMTRMEMO;

	private String _CMTRMAIL;

	private String _CMMAILMEMO;
	
	private String _SRCFUND;
	
	private String _BENTYPE;

	private String _CAPTCHA;

	public String getSessionId() {
		return _SessionId;
	}
	public void setSessionId(String sessionid) {
		this._SessionId = sessionid;
	}

	public String getTXTOKEN() {
		return _TXTOKEN;
	}
	public void setTXTOKEN(String txtoken) {
		this._TXTOKEN = txtoken;
	}

	public String getFGTXWAY() {
		return _FGTXWAY;
	}
	public void setFGTXWAY(String fgtxway) {
		this._FGTXWAY = fgtxway;
	}

	public String getCMPASSWORD() {
		return _CMPASSWORD;
	}
	public void setCMPASSWORD(String cmpassword) {
		this._CMPASSWORD = cmpassword;
	}

	public String getPINNEW() {
		return _PINNEW;
	}
	public void setPINNEW(String pinnew) {
		this._PINNEW = pinnew;
	}

	public String getOTPKEY() {
		return _OTPKEY;
	}
	public void setOTPKEY(String otpkey) {
		this._OTPKEY = otpkey;
	}

	public String getADOPID() {
		return _ADOPID;
	}
	public void setADOPID(String adopid) {
		this._ADOPID = adopid;
	}

	public String getFGINACNO() {
		return _FGINACNO;
	}
	public void setFGINACNO(String fginacno) {
		this._FGINACNO = fginacno;
	}

	public String getINACNO1() {
		return _INACNO1;
	}
	public void setINACNO1(String inacno1) {
		this._INACNO1 = inacno1;
	}

	public String getINACNO2() {
		return _INACNO2;
	}
	public void setINACNO2(String inacno2) {
		this._INACNO2 = inacno2;
	}

	public String getFGTRDATE() {
		return _FGTRDATE;
	}
	public void setFGTRDATE(String fgtrdate) {
		this._FGTRDATE = fgtrdate;
	}

	public String getCMTRDATE() {
		return _CMTRDATE;
	}
	public void setCMTRDATE(String cmtrdate) {
		this._CMTRDATE = cmtrdate;
	}

	public String getCMSDATE() {
		return _CMSDATE;
	}
	public void setCMSDATE(String cmsdate) {
		this._CMSDATE = cmsdate;
	}

	public String getCMEDATE() {
		return _CMEDATE;
	}
	public void setCMEDATE(String cmedate) {
		this._CMEDATE = cmedate;
	}

	public String getCMPERIOD() {
		return _CMPERIOD;
	}
	public void setCMPERIOD(String cmperiod) {
		this._CMPERIOD = cmperiod;
	}

	public String getCUSTACC() {
		return _CUSTACC;
	}
	public void setCUSTACC(String custacc) {
		this._CUSTACC = custacc;
	}

	public String getPAYCCY() {
		return _PAYCCY;
	}
	public void setPAYCCY(String payccy) {
		this._PAYCCY = payccy;
	}

	public String getREMITCY() {
		return _REMITCY;
	}
	public void setREMITCY(String remitcy) {
		this._REMITCY = remitcy;
	}

	public String getPAYREMIT() {
		return _PAYREMIT;
	}
	public void setPAYREMIT(String payremit) {
		this._PAYREMIT = payremit;
	}

	public String getCURAMT() {
		return _CURAMT;
	}
	public void setCURAMT(String curamt) {
		this._CURAMT = curamt;
	}

	public String getCUSIDN() {
		return _CUSIDN;
	}
	public void setCUSIDN(String cusidn) {
		this._CUSIDN = cusidn;
	}

	public String getUID() {
		return _UID;
	}
	public void setUID(String uid) {
		this._UID = uid;
	}

	public String getCOMMCCY() {
		return _COMMCCY;
	}
	public void setCOMMCCY(String commccy) {
		this._COMMCCY = commccy;
	}

	public String getCOMMACC() {
		return _COMMACC;
	}
	public void setCOMMACC(String commacc) {
		this._COMMACC = commacc;
	}

	public String getPAYDATE() {
		return _PAYDATE;
	}
	public void setPAYDATE(String paydate) {
		this._PAYDATE = paydate;
	}

	public String getNAME() {
		return _NAME;
	}
	public void setNAME(String name) {
		this._NAME = name;
	}

	public String getMEMO1() {
		return _MEMO1;
	}
	public void setMEMO1(String memo1) {
		this._MEMO1 = memo1;
	}

	public String getCMTRMEMO() {
		return _CMTRMEMO;
	}
	public void setCMTRMEMO(String cmtrmemo) {
		this._CMTRMEMO = cmtrmemo;
	}

	public String getCMTRMAIL() {
		return _CMTRMAIL;
	}
	public void setCMTRMAIL(String cmtrmail) {
		this._CMTRMAIL = cmtrmail;
	}

	public String getCMMAILMEMO() {
		return _CMMAILMEMO;
	}
	public void setCMMAILMEMO(String cmmailmemo) {
		this._CMMAILMEMO = cmmailmemo;
	}
	
	public String getSRCFUND() {
		return _SRCFUND;
	}
	public void setSRCFUND(String srcfund) {
		this._SRCFUND = srcfund;
	}
	
	public String getBENTYPE() {
		return _BENTYPE;
	}
	public void setBENTYPE(String bentype) {
		this._BENTYPE = bentype;
	}

	public String getCAPTCHA() {
		return _CAPTCHA;
	}
	public void setCAPTCHA(String captcha) {
		this._CAPTCHA = captcha;
	}

}
