package fstop.ws;

public class WsN320Row {
	
	private String _ACN;
	
	private String _SEQ;
	
	private String _BAL;
	
	private String _ITR;
	
	private String _DATFSLN;
	
	private String _DDT;
	
	private String _AWT;
	
	private String _DATITPY;

	private String _AMTORLN;

	public String getACN() {
		return _ACN;
	}

	public void setACN(String acn) {
		_ACN = acn;
	}

	public String getSEQ() {
		return _SEQ;
	}

	public void setSEQ(String seq) {
		_SEQ = seq;
	}

	public String getBAL() {
		return _BAL;
	}

	public void setBAL(String bal) {
		_BAL = bal;
	}

	public String getITR() {
		return _ITR;
	}

	public void setITR(String itr) {
		_ITR = itr;
	}

	public String getDATFSLN() {
		return _DATFSLN;
	}

	public void setDATFSLN(String datfsln) {
		_DATFSLN = datfsln;
	}

	public String getDDT() {
		return _DDT;
	}

	public void setDDT(String ddt) {
		_DDT = ddt;
	}

	public String getAWT() {
		return _AWT;
	}

	public void setAWT(String awt) {
		_AWT = awt;
	}

	public String getDATITPY() {
		return _DATITPY;
	}

	public void setDATITPY(String datitpy) {
		_DATITPY = datitpy;
	}
	
	public String getAMTORLN() {
		return _AMTORLN;
	}

	public void setAMTORLN(String amtorln) {
		_AMTORLN = amtorln;
	}
	
}
