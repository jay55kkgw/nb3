package fstop.ws;

public class GDFeeInfo {

	private String _MsgCode;

	private String _MsgName;

	private String _ACN;

	private String _TRNAMT;

	private PeriodFeeRow[] _Table;

	private String[] _List;
	
	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getACN() {
		return _ACN;
	}

	public void setACN(String ACN) {
		this._ACN = ACN;
	}

	public PeriodFeeRow[] getTable() {
		return _Table;
	}

	public void setTable(PeriodFeeRow[] table) {
		this._Table = table;
	}

	public String getTRNAMT() {
		return _TRNAMT;
	}

	public void setTRNAMT(String TRNAMT) {
		this._TRNAMT = TRNAMT;
	}

	public String[] getList() {
		return _List;
	}

	public void setList(String[] List) {
		this._List = List;
	}

}
