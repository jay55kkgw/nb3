package fstop.ws;

public class WsFxRemitTypeOut {

	private String _MsgCode;// 訊息代碼
	private String _MsgName;// 訊息名稱
	private String _TYPE;// 匯款用途性質
	private String _ADRMTID;//匯款分類ID

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getTYPE() {
		return _TYPE;
	}

	public void setTYPE(String type) {
		this._TYPE = type;
	}

	public String getADRMTID() {
		return _ADRMTID;
	}

	public void setADRMTID(String adrmtid) {
		this._ADRMTID = adrmtid;
	}

	
}
