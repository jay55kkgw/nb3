package fstop.ws;

import java.io.Serializable;

public class WsCN28In implements Serializable {
	private static final long serialVersionUID = 3389424799674299790L;
	private String sessionId;	//SessionId
	private String CUSIDN;
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}

}
