package fstop.ws;

public class WsN076Out {

	private String MsgCode;//	訊息代碼
	private String MsgName;//	訊息名稱
	private String CMTXTIME;//	交易時間
	private String OUTACN;//	轉出帳號
	private String FDPACN;//	存單帳號
	private String FDPNUM;//	存單號碼
	private String AMOUNT;//	轉帳金額
	private String TERMS;//	已存入總期數
	private String TOTAMT;//	已存入總金額
	private String RETERMS;//	欠繳期數
	private String CMTRMEMO;//	交易備註
	private String TOTBAL;//	轉出帳號帳戶餘額
	private String 	AVLBAL;//	轉出帳號可用餘額
	public String getMsgCode() {
		return MsgCode;
	}
	public void setMsgCode(String msgCode) {
		MsgCode = msgCode;
	}
	public String getMsgName() {
		return MsgName;
	}
	public void setMsgName(String msgName) {
		MsgName = msgName;
	}
	public String getCMTXTIME() {
		return CMTXTIME;
	}
	public void setCMTXTIME(String cMTXTIME) {
		CMTXTIME = cMTXTIME;
	}
	public String getOUTACN() {
		return OUTACN;
	}
	public void setOUTACN(String oUTACN) {
		OUTACN = oUTACN;
	}
	public String getFDPACN() {
		return FDPACN;
	}
	public void setFDPACN(String fDPACN) {
		FDPACN = fDPACN;
	}
	public String getFDPNUM() {
		return FDPNUM;
	}
	public void setFDPNUM(String fDPNUM) {
		FDPNUM = fDPNUM;
	}
	public String getAMOUNT() {
		return AMOUNT;
	}
	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}
	public String getTERMS() {
		return TERMS;
	}
	public void setTERMS(String tERMS) {
		TERMS = tERMS;
	}
	public String getTOTAMT() {
		return TOTAMT;
	}
	public void setTOTAMT(String tOTAMT) {
		TOTAMT = tOTAMT;
	}
	public String getRETERMS() {
		return RETERMS;
	}
	public void setRETERMS(String rETERMS) {
		RETERMS = rETERMS;
	}
	public String getCMTRMEMO() {
		return CMTRMEMO;
	}
	public void setCMTRMEMO(String cMTRMEMO) {
		CMTRMEMO = cMTRMEMO;
	}
	public String getTOTBAL() {
		return TOTBAL;
	}
	public void setTOTBAL(String tOTBAL) {
		TOTBAL = tOTBAL;
	}
	public String getAVLBAL() {
		return AVLBAL;
	}
	public void setAVLBAL(String aVLBAL) {
		AVLBAL = aVLBAL;
	}

	
}
