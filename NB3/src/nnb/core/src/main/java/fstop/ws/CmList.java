package fstop.ws;

public class CmList {
	
	private String _MsgCode;
	
	private String _MsgName;
	
	private String[] _List;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		_MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		_MsgName = msgName;
	}

	public String[] getList() {
		return _List;
	}

	public void setList(String[] list) {
		_List = list;
	}
	
}
