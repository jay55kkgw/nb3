package fstop.ws;

public class WsN940N941In {
	
	private String _SessionId;
	
	private String _OLDPIN;
	
	private String _NEWPIN;
	
	private String _RENEWPIN;
	
	private String _OLDTRANPIN;
	
	private String _NEWTRANPIN;
	
	private String _RENEWTRANPIN;

	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		_SessionId = sessionId;
	}

	public String getOLDPIN() {
		return _OLDPIN;
	}

	public void setOLDPIN(String oldpin) {
		_OLDPIN = oldpin;
	}

	public String getNEWPIN() {
		return _NEWPIN;
	}

	public void setNEWPIN(String newpin) {
		_NEWPIN = newpin;
	}

	public String getRENEWPIN() {
		return _RENEWPIN;
	}

	public void setRENEWPIN(String renewpin) {
		_RENEWPIN = renewpin;
	}

	public String getOLDTRANPIN() {
		return _OLDTRANPIN;
	}

	public void setOLDTRANPIN(String OLDTRANPIN) {
		_OLDTRANPIN = OLDTRANPIN;
	}

	public String getNEWTRANPIN() {
		return _NEWTRANPIN;
	}

	public void setNEWTRANPIN(String NEWTRANPIN) {
		_NEWTRANPIN = NEWTRANPIN;
	}
	
	public String getRENEWTRANPIN() {
		return _RENEWTRANPIN;
	}

	public void setRENEWTRANPIN(String RENEWTRANPIN) {
		_RENEWTRANPIN = RENEWTRANPIN;
	}
}
