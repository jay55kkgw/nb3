package fstop.ws;

public class WsN092Out {

	private String _MsgCode;

	private String _MsgName;

	private String _str_SystemDate;

	private String _i_TotalCount;

	private WsN092Row[] _Table;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String MsgCode) {
		this._MsgCode = MsgCode;
	}

	public void setMsgName(String MsgName) {
		this._MsgName = MsgName;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public String getstr_SystemDate() {
		return _str_SystemDate;
	}

	public void setstr_SystemDate(String str_SystemDate) {
		this._str_SystemDate = str_SystemDate;
	}

	public String geti_TotalCount() {
		return _i_TotalCount;
	}

	public void seti_TotalCount(String i_TotalCount) {
		this._i_TotalCount = i_TotalCount;
	}

	public WsN092Row[] getTable() {
		return _Table;
	}

	public void setTable(WsN092Row[] Table) {
		this._Table = Table;
	}

}
