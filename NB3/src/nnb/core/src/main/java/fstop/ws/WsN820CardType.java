package fstop.ws;

public class WsN820CardType {
	
	private String _MsgCode;
	
	private String _MsgName;
	
	private String _SHOWCARD0;
	
	private String _SHOWCARD1;
	
	private String _SHOWCARD2;
	
	private String _N810MsgCode;
	
	private String _N810MsgName;
	
	private String _N813MsgCode;
	
	private String _N813MsgName;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		_MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		_MsgName = msgName;
	}

	public String getSHOWCARD0() {
		return _SHOWCARD0;
	}

	public void setSHOWCARD0(String showcard0) {
		_SHOWCARD0 = showcard0;
	}

	public String getSHOWCARD1() {
		return _SHOWCARD1;
	}

	public void setSHOWCARD1(String showcard1) {
		_SHOWCARD1 = showcard1;
	}

	public String getSHOWCARD2() {
		return _SHOWCARD2;
	}

	public void setSHOWCARD2(String showcard2) {
		_SHOWCARD2 = showcard2;
	}

	public String getN810MsgCode() {
		return _N810MsgCode;
	}

	public void setN810MsgCode(String msgCode) {
		_N810MsgCode = msgCode;
	}

	public String getN810MsgName() {
		return _N810MsgName;
	}

	public void setN810MsgName(String msgName) {
		_N810MsgName = msgName;
	}

	public String getN813MsgCode() {
		return _N813MsgCode;
	}

	public void setN813MsgCode(String msgCode) {
		_N813MsgCode = msgCode;
	}

	public String getN813MsgName() {
		return _N813MsgName;
	}

	public void setN813MsgName(String msgName) {
		_N813MsgName = msgName;
	}
	
}
