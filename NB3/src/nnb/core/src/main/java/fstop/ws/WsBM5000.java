package fstop.ws;

public interface WsBM5000{

	public WsBM5000Out action(WsBM5000In params);
	
	public WsBM5000Eout getStoreTypeList(WsBM5000EIn params);
}
