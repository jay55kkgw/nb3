package fstop.ws;

public class WsN323In {

	private String _SessionId;
	
	private String _CMPASSWORD;
	
	private String _PINNEW;
	
	private String _CAPTCHA;// 圖形驗證碼
	
	private String _TYPE;

	private String _DEGREE;

	private String _MARK1;
	
	
	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		this._SessionId = sessionId;
	}

	public String getCMPASSWORD() {
		return _CMPASSWORD;
	}

	public void setCMPASSWORD(String cmpassword) {
		this._CMPASSWORD = cmpassword;
	}
	
	public String getPINNEW() {
		return _PINNEW;
	}

	public void setPINNEW(String pinnew) {
		this._PINNEW = pinnew;
	}
	
	public String getCAPTCHA() {
		return _CAPTCHA;
	}

	public void setCAPTCHA(String captcha) {
		this._CAPTCHA = captcha;
	}
	
	public String getTYPE() {
		return _TYPE;
	}

	public void setTYPE(String type) {
		this._TYPE = type;
	}

	public String getDEGREE() {
		return _DEGREE;
	}

	public void setDEGREE(String degree) {
		this._DEGREE = degree;
	}

	public String getMARK1() {
		return _MARK1;
	}

	public void setMARK1(String mark1) {
		this._MARK1 = mark1;
	}

	
}
