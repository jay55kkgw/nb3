package fstop.ws;

public class WsN816AOut {

	// 訊息代碼
	private String _MsgCode;

	// 訊息名稱
	private String _MsgName;

	// 系統時間
	private String _SYSTIME;

	// 信用卡號
	private String _CARDNUM;

	// 卡片效期
	private String _EXP_DATE;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getSYSTIME() {
		return _SYSTIME;
	}

	public void setSYSTIME(String systime) {
		_SYSTIME = systime;
	}

	public String getCARDNUM() {
		return _CARDNUM;
	}

	public void setCARDNUM(String cardnum) {
		_CARDNUM = cardnum;
	}

	public String getEXP_DATE() {
		return _EXP_DATE;
	}

	public void setEXP_DATE(String exp_date) {
		_EXP_DATE = exp_date;
	}

}
