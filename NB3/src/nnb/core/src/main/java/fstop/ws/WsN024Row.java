package fstop.ws;

public class WsN024Row {

	private String _COLOR;

	private String _CRYNAME;

	private String _CRY;

	private String _ITR1;

	private String _ITR2;

	public String getCOLOR() {
		return _COLOR;
	}

	public void setCOLOR(String color) {
		this._COLOR = color;
	}

	public String getCRYNAME() {
		return _CRYNAME;
	}

	public void setCRYNAME(String cryname) {
		this._CRYNAME = cryname;
	}

	public String getCRY() {
		return _CRY;
	}

	public void setCRY(String cry) {
		this._CRY = cry;
	}

	public String getITR1() {
		return _ITR1;
	}

	public void setITR1(String itr1) {
		this._ITR1 = itr1;
	}

	public String getITR2() {
		return _ITR2;
	}

	public void setITR2(String itr2) {
		this._ITR2 = itr2;
	}

}
