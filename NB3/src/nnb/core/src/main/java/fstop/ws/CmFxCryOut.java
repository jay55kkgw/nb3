package fstop.ws;

public class CmFxCryOut {
	
	private String _MsgCode;

	private String _MsgName;

	private CmFxCryOutRow[] _Table;
	
	public String getMsgCode() {
		return _MsgCode;
	}
	public void setMsgCode(String msgcode) {
		this._MsgCode = msgcode;
	}

	public String getMsgName() {
		return _MsgName;
	}
	public void setMsgName(String msgname) {
		this._MsgName = msgname;
	}

	public CmFxCryOutRow[] getTable() {
		return _Table;
	}
	public void setTable(CmFxCryOutRow[] table) {
		this._Table = table;
	}

}
