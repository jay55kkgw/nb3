package fstop.ws;

public class WsBM3000MergeRow2 {
	
	private String _DPADID;//	行銷ID
	private String _DPADHLK;//	行銷標題
	private String _DPADCON;//	行銷內容
	private String _DPPICTYPE;//	明細圖片位置
	private String _DPPICPATH;//	行銷圖片
	private String _DPBTNNAME;//	按鈕名稱
	private String _DPADRESS;//	超連結網址
	private String _DPSDATE;//	活動期間起日
	private String _DPEDATE;//	活動期間迄日

	
	public String getDPADID() {
		return _DPADID;
	}
	public void setDPADID(String _dpadid) {
		_DPADID = _dpadid;
	}
	public String getDPADHLK() {
		return _DPADHLK;
	}
	public void setDPADHLK(String _dpadhlk) {
		_DPADHLK = _dpadhlk;
	}
	public String getDPADCON() {
		return _DPADCON;
	}
	public void setDPADCON(String _dpadcon) {
		_DPADCON = _dpadcon;
	}
	public String getDPPICTYPE() {
		return _DPPICTYPE;
	}
	public void setDPPICTYPE(String _dppictype) {
		_DPPICTYPE = _dppictype;
	}
	public String getDPPICPATH() {
		return _DPPICPATH;
	}
	public void setDPPICPATH(String _dppicpath) {
		_DPPICPATH = _dppicpath;
	}
	public String getDPBTNNAME() {
		return this._DPBTNNAME;
	}
	public void setDPBTNNAME(String dpbtname) {
		this._DPBTNNAME = dpbtname;
	}
	public String getDPADRESS() {
		return this._DPADRESS;
	}
	public void setDPADRESS(String dpadress) {
		this._DPADRESS = dpadress;
	}
	public String getDPSDATE() {
		return _DPSDATE;
	}
	public void setDPSDATE(String _dpsdate) {
		_DPSDATE = _dpsdate;
	}
	public String getDPEDATE() {
		return _DPEDATE;
	}
	public void setDPEDATE(String _dpedate) {
		_DPEDATE = _dpedate;
	}

	
	

}
