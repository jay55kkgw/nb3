package fstop.ws;

public class WsN110Out {
	
	private String _MsgCode;
	
	private String _MsgName;
	
	private String _CMQTIME;
	
	private String _TOTAL_ADPIBAL;
	
	private String _TOTAL_BDPIBAL;
	
	private String _TOTAL_CLR;
	
	private WsN110Row[] _Table;
	
	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String MsgCode) {
		this._MsgCode = MsgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String MsgName) {
		this._MsgName = MsgName;
	}

	public String getCMQTIME() {
		return _CMQTIME;
	}

	public void setCMQTIME(String CMQTIME) {
		this._CMQTIME = CMQTIME;
	}

	public String getTOTAL_ADPIBAL() {
		return _TOTAL_ADPIBAL;
	}

	public void setTOTAL_ADPIBAL(String TOTAL_ADPIBAL) {
		this._TOTAL_ADPIBAL = TOTAL_ADPIBAL;
	}
	
	public String getTOTAL_BDPIBAL() {
		return _TOTAL_BDPIBAL;
	}

	public void setTOTAL_BDPIBAL(String TOTAL_BDPIBAL) {
		this._TOTAL_BDPIBAL = TOTAL_BDPIBAL;
	}
	
	public String getTOTAL_CLR() {
		return _TOTAL_CLR;
	}

	public void setTOTAL_CLR(String TOTAL_CLR) {
		this._TOTAL_CLR = TOTAL_CLR;
	}		
	
	public WsN110Row[] getTable() {
		return _Table;
	}

	public void setTable(WsN110Row[] Table) {
		this._Table = Table;
	}
}
