package fstop.ws;

public class WsGD11TLRow1 {
	
	private String _GDTYPE1;//	黃金存摺規格
	
	private String _BPRICE1	;//賣出價格
	
	private String _SPRICE1	;//買進價格
	
	private String _PRICE1	;//轉換100公克補繳價款
	
	private String _GDTYPE2	;//黃金存摺規格
	
	private String _BPRICE2	;//賣出價格
	
	private String _SPRICE2	;//買進價格
	
	private String _PRICE2	;//轉換250公克補繳價款
	
	private String _GDTYPE3	;//黃金存摺規格
	
	private String _BPRICE3	;//賣出價格
	
	private String _SPRICE3	;//買進價格
	
	private String _PRICE3	;//轉換500公克補繳價款
	
	private String _GDTYPE4	;//黃金存摺規格
	
	private String _BPRICE4	;//賣出價格
	
	private String _SPRICE4	;//買進價格
	
	private String _PRICE4	;//轉換1000公克補繳價款

	public String getGDTYPE1() {
		return _GDTYPE1;
	}

	public void setGDTYPE1(String gdtype1) {
		this._GDTYPE1 = gdtype1;
	}

	public String getBPRICE1() {
		return _BPRICE1;
	}

	public void setBPRICE1(String bprice1) {
		this._BPRICE1 = bprice1;
	}

	public String getSPRICE1() {
		return _SPRICE1;
	}

	public void setSPRICE1(String sprice1) {
		this._SPRICE1 = sprice1;
	}

	public String getPRICE1() {
		return _PRICE1;
	}

	public void setPRICE1(String price1) {
		this._PRICE1 = price1;
	}

	public String getGDTYPE2() {
		return _GDTYPE2;
	}

	public void setGDTYPE2(String gdtype2) {
		this._GDTYPE2 = gdtype2;
	}

	public String getBPRICE2() {
		return _BPRICE2;
	}

	public void setBPRICE2(String bprice2) {
		this._BPRICE2 = bprice2;
	}

	public String getSPRICE2() {
		return _SPRICE2;
	}

	public void setSPRICE2(String sprice2) {
		this._SPRICE2 = sprice2;
	}

	public String getPRICE2() {
		return _PRICE2;
	}

	public void setPRICE2(String price2) {
		this._PRICE2 = price2;
	}

	public String getGDTYPE3() {
		return _GDTYPE3;
	}

	public void setGDTYPE3(String gdtype3) {
		this._GDTYPE3 = gdtype3;
	}

	public String getBPRICE3() {
		return _BPRICE3;
	}

	public void setBPRICE3(String bprice3) {
		this._BPRICE3 = bprice3;
	}

	public String getSPRICE3() {
		return _SPRICE3;
	}

	public void setSPRICE3(String sprice3) {
		this._SPRICE3 = sprice3;
	}

	public String getPRICE3() {
		return _PRICE3;
	}

	public void setPRICE3(String price3) {
		this._PRICE3 = price3;
	}

	public String getGDTYPE4() {
		return _GDTYPE4;
	}

	public void setGDTYPE4(String gdtype4) {
		this._GDTYPE4 = gdtype4;
	}

	public String getBPRICE4() {
		return _BPRICE4;
	}

	public void setBPRICE4(String bprice4) {
		this._BPRICE4 = bprice4;
	}

	public String getSPRICE4() {
		return _SPRICE4;
	}

	public void setSPRICE4(String sprice4) {
		this._SPRICE4 = sprice4;
	}

	public String getPRICE4() {
		return _PRICE4;
	}

	public void setPRICE4(String price4) {
		this._PRICE4 = price4;
	}

	

}
