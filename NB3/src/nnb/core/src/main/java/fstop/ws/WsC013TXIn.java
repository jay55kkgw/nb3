package fstop.ws;

public class WsC013TXIn {

	private String _SessionId;
	
	private String _N950PASSWORD;//交易密碼
	
	private String _BEGDATE;     //起日
	
	private String _ENDDATE;     //迄日

	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionid) {
		this._SessionId = sessionid;
	}

	public String getN950PASSWORD() {
		return _N950PASSWORD;
	}

	public void setN950PASSWORD(String N950PASSWORD) {
		this._N950PASSWORD = N950PASSWORD;
	}

	public String getBEGDATE() {
		return _BEGDATE;
	}

	public void setBEGDATE(String BEGDATE) {
		this._BEGDATE = BEGDATE;
	}

	public String getENDDATE() {
		return _ENDDATE;
	}

	public void setENDDATE(String ENDDATE) {
		this._ENDDATE = ENDDATE;
	}
}
