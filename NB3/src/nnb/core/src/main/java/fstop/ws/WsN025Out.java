package fstop.ws;

public class WsN025Out {
	private String _MsgCode;

	private String _MsgName;

	private String _DATE;

	private String _TIME;

	private WsN025Row[] _Table;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getDATE() {
		return _DATE;
	}

	public void setDATE(String DATE) {
		this._DATE = DATE;
	}

	public String getTIME() {
		return _TIME;
	}

	public void setTIME(String TIME) {
		this._TIME = TIME;
	}

	public WsN025Row[] getTable() {
		return _Table;
	}

	public void setTable(WsN025Row[] table) {
		this._Table = table;
	}

}
