package fstop.ws;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.httpclient.params.HttpClientParams;
//import org.codehaus.xfire.MessageContext;
//import org.codehaus.xfire.client.Client;
//import org.codehaus.xfire.client.XFireProxyFactory;
//import org.codehaus.xfire.handler.AbstractHandler;
//import org.codehaus.xfire.service.Service;
//import org.codehaus.xfire.service.binding.ObjectServiceFactory;
//import org.codehaus.xfire.soap.SoapConstants;
//import org.codehaus.xfire.transport.http.CommonsHttpMessageSender;
//import org.jdom.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tw.com.fstop.util.EncUtil;
import tw.com.fstop.util.SpringBeanFactory;

public class WebServiceUtil{
	static Logger log = LoggerFactory.getLogger(WebServiceUtil.class);
	
//	LOCAL
//	static String __WSURL = "http://localhost:8080/TBBMP/services/" ;
//	kuso 乙太IP
//	static String __WSURL = "http://192.168.43.170:8080/TBBMP/services/" ;
//	kuso 台企無線IP
//	static String __WSURL = "http://192.168.50.118:8080/TBBMP/services/" ;
//	景毅 台企無線IP
//	static String __WSURL = "http://192.168.43.18:8080/TBBMP/services/" ;
	
//	開發套無線IP(Zhiru)
//	static String __WSURL = "http://192.168.43.208:8080/TBBMP/services/" ;
//	開發套無線IP(kusoyalo)
//	static String __WSURL = "http://192.168.43.119:8080/TBBMP/services/" ;
//	開發套MB5無線IP
//	static String __WSURL = "http://192.168.50.69:8080/TBBMP/services/" ;
	
//	台企開發套
//	static String __WSURL = "http://10.16.22.17/TBBNBAppsWeb_MB5/services/" ;
//	static String __WSURL = SpringBeanFactory.getBean("WSURL");
//	台企測試套
//	static String __WSURL = "http://10.16.22.17/TBBNBAppsWeb_MB2/services/" ;
	
	/*
	 * 呼叫WEBSERVICE
	 * WSInterFace-CLIENT的WS介面
	 * macPO-如果要押MAC的話，要代SET完屬性的PO
	 */
//	public static <T> T getWebService(Class<?> WSInterFace,Object macPO){
//		log.debug("WSInterFace=" + WSInterFace);
//		log.debug("macPO=" + macPO);
//		String WSURL = __WSURL + WSInterFace.getSimpleName();
//		
//		return getWebService(WSInterFace,WSURL,macPO);
//	}
	
	/*
	 * 呼叫WEBSERVICE
	 * WSInterFace-CLIENT的WS介面
	 */
//	public static <T> T getWebService(Class<?> WSInterFace){
//		log.debug("WSInterFace=" + WSInterFace);
//		String WSURL = __WSURL + WSInterFace.getSimpleName();
//		
//		return getWebService(WSInterFace,WSURL,null);
//	}
	
	/*
	 * WEBSERVICE
	 * SOAP HEADER加欄位
	 */
//	public static class ClientHeader extends AbstractHandler{
//		private String deviceID;
//		private String timeStamp;
//		private String TAC;
//		private String MAC;
//		
//		public ClientHeader(String deviceID,String timeStamp,String TAC,String MAC){
//			this.deviceID = deviceID;
//			this.timeStamp = timeStamp;
//			this.TAC = TAC;
//			this.MAC = MAC;
//		}
//		
//		@Override
//		public void invoke(MessageContext context){
//			Element element = new Element("header"); 
//			context.getOutMessage().setHeader(element); 
//			
//			Element deviceIdElement = new Element("DeviceID");
//			Element timestampElement = new Element("TimeStamp");
//			Element TACElement = new Element("TAC");
//			
//			deviceIdElement.addContent(deviceID); 
//			timestampElement.addContent(timeStamp);
//			TACElement.addContent(TAC);
//			
//			element.addContent(deviceIdElement);
//			element.addContent(timestampElement);
//			element.addContent(TACElement);
//			if(MAC != null){
//				Element MACElement = new Element("MAC");
//				MACElement.addContent(MAC);
//				element.addContent(MACElement);
//			}
//		}
//	}
	/*
	 * 產生SOAP BODY的XML字串
	 */
//	public static String generateSOAPBody(Map<String,String> valueMap){
//		String SOAPBody = "";
//		String template = "<ns1:action xmlns:ns1=\"http://xfire.codehaus.org\"><ns1:in0>%s</ns1:in0></ns1:action>";
//		
//		StringBuilder stringBuilder = new StringBuilder();
//		
//		SortedSet<String> sortedSet = new TreeSet<String>(valueMap.keySet());
//		Iterator<String> iterator = sortedSet.iterator();
//
//		while(iterator.hasNext()){  
//			String key = iterator.next();  
//			String value = valueMap.get(key);  
//			log.debug("key=" + key +"，value=" + value + "。");
//			
//			if(!"class".equals(key)){
//				if(value == null){
//					stringBuilder.append("<" + key + " xmlns=\"http://ws.fstop\" xsi:nil=\"true\">");
//					stringBuilder.append("");
//					stringBuilder.append("</" + key + ">");
//				}
//				else{
//					stringBuilder.append("<" + key + " xmlns=\"http://ws.fstop\">");
//					stringBuilder.append(value);
//					stringBuilder.append("</" + key + ">");
//				}
//			}
//		}
//		log.debug("stringBuilder.toString()=" + stringBuilder.toString());
//		
//		SOAPBody = String.format(template,stringBuilder.toString());
//		log.debug("SOAPBody=" + SOAPBody);
//		
//		return SOAPBody;
//	}
	
	/*
	 * 呼叫WEBSERVICE
	 * WSInterFace-CLIENT的WS介面
	 * WSURL-WS的URL
	 * macPO-如果要押MAC的話，要代SET完屬性的PO
	 */
//	@SuppressWarnings("unchecked")
//	public static <T> T getWebService(Class<?> WSInterFace,String WSURL,Object macPO){
//		try{
//			log.debug("WSInterFace=" + WSInterFace);
//			log.debug("WSURL=" + WSURL);
//			log.debug("macPO=" + macPO);
//			
//			Map<String,String> properties = new HashMap<String,String>();
//	        properties.put(ObjectServiceFactory.STYLE,SoapConstants.STYLE_WRAPPED);
//			properties.put(ObjectServiceFactory.USE,SoapConstants.USE_LITERAL);
//			log.debug("properties=" + properties);
//			
//			Service serviceModel = new ObjectServiceFactory().create(WSInterFace,null,"http://xfire.codehaus.org",properties);
//			log.debug("serviceModel.getSimpleName()=" + serviceModel.getSimpleName());
//			
//			XFireProxyFactory xfireProxyFactory = new XFireProxyFactory();
//			Object result = xfireProxyFactory.create(serviceModel,WSURL);
//			log.debug("result=" + result);
//			
//			Client client = Client.getInstance(result);
//			HttpClientParams httpClientParams = new HttpClientParams();
//	        //回應逾時
//			httpClientParams.setIntParameter(HttpClientParams.SO_TIMEOUT,60000);
//			client.setProperty(CommonsHttpMessageSender.HTTP_CLIENT_PARAMS,httpClientParams);
//			client.setProperty(CommonsHttpMessageSender.DISABLE_KEEP_ALIVE,"true");
//			client.setProperty(CommonsHttpMessageSender.DISABLE_EXPECT_CONTINUE,"true");
//			
//			String deviceID = "TBBNNNB";
//			
//			String timeStamp = Long.valueOf(new Date().getTime()).toString() + "00000";
//			log.debug("timeStamp=" + timeStamp);
//			
//			String TAC = EncUtil.encryptEBC(deviceID + timeStamp,deviceID.substring(0,3) + timeStamp.substring(8,18) + deviceID.substring(0,3));
//			log.debug("TAC=" + TAC);
//			
//			if(TAC == null){
//				return null;
//			}
//			
//			String MAC = null;
//			if(macPO != null){
//				Map<String,String> valueMap = BeanUtils.describe(macPO);
//				log.debug("valueMap=" + valueMap);
//				
//				MAC = generateSOAPBody(valueMap);
//				log.debug("MAC=" + MAC);
//				
//				MAC = EncUtil.MD5(MAC,"UTF-8");
//				log.debug("MAC=" + MAC);
//				
//				if(MAC == null){
//					return null;
//				}
//			}
//			
//			client.addOutHandler(new ClientHeader(deviceID,timeStamp,TAC,MAC));
//
//			log.debug("client=" + client);
//			
//			return (T)result;
//		}
//		catch(Exception e){
//			log.error("",e);
//			e.printStackTrace();
//			return null;
//		}
//	}
}