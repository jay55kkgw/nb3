package fstop.ws;

public class WsN09001Out {

	private String _MsgCode;

	private String _MsgName;

	private String _CMQTIME;

	private String _SVACN;

	private String _ACN;

	private String _TRNGD;

	private String _PRICE;

	private String _DISPRICE;

	private String _PERDIS;

	private String _TRNFEE;

	private String _TRNAMT;

	private String _DISAMT;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getCMQTIME() {
		return _CMQTIME;
	}

	public void setCMQTIME(String cmqtime) {
		this._CMQTIME = cmqtime;
	}

	public String getSVACN() {
		return _SVACN;
	}

	public void setSVACN(String SVACN) {
		this._SVACN = SVACN;
	}

	public String getACN() {
		return _ACN;
	}

	public void setACN(String ACN) {
		this._ACN = ACN;
	}

	public String getTRNGD() {
		return _TRNGD;
	}

	public void setTRNGD(String TRNGD) {
		this._TRNGD = TRNGD;
	}

	public String getPRICE() {
		return _PRICE;
	}

	public void setPRICE(String PRICE) {
		this._PRICE = PRICE;
	}

	public String getDISPRICE() {
		return _DISPRICE;
	}

	public void setDISPRICE(String DISPRICE) {
		this._DISPRICE = DISPRICE;
	}

	public String getPERDIS() {
		return _PERDIS;
	}

	public void setPERDIS(String PERDIS) {
		this._PERDIS = PERDIS;
	}

	public String getTRNFEE() {
		return _TRNFEE;
	}

	public void setTRNFEE(String TRNFEE) {
		this._TRNFEE = TRNFEE;
	}

	public String getTRNAMT() {
		return _TRNAMT;
	}

	public void setTRNAMT(String TRNAMT) {
		this._TRNAMT = TRNAMT;
	}

	public String getDISAMT() {
		return _DISAMT;
	}

	public void setDISAMT(String disamt) {
		this._DISAMT = disamt;
	}

}
