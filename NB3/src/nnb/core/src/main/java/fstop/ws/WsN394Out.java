package fstop.ws;

public class WsN394Out {

	private String _MsgCode;// 訊息代碼
	private String _MsgName;// 訊息名稱
	private String _CMQTIME;// 交易時間
	private String _TRADEDATE;// 預約日期
	private String _CREDITNO;// 信託帳號
	private String _UNIT;// 贖回單位
	private String _CRY;// 贖回信託幣別代碼
	private String _ADCCYNAME;// 贖回信託幣別名稱
	private String _FUNDAMT;// 贖回信託金額
	private String _FUNDACN;// 入帳帳號
	private String _FDTXTYPE;// 交易種類

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getCMQTIME() {
		return _CMQTIME;
	}

	public void setCMQTIME(String cmqtime) {
		this._CMQTIME = cmqtime;
	}

	public String getTRADEDATE() {
		return _TRADEDATE;
	}

	public void setTRADEDATE(String tradedate) {
		this._TRADEDATE = tradedate;
	}

	public String getCREDITNO() {
		return _CREDITNO;
	}

	public void setCREDITNO(String creditno) {
		this._CREDITNO = creditno;
	}

	public String getUNIT() {
		return _UNIT;
	}

	public void setUNIT(String unit) {
		this._UNIT = unit;
	}

	public String getCRY() {
		return _CRY;
	}

	public void setCRY(String cry) {
		this._CRY = cry;
	}

	public String getADCCYNAME() {
		return _ADCCYNAME;
	}

	public void setADCCYNAME(String adccyname) {
		this._ADCCYNAME = adccyname;
	}

	public String getFUNDAMT() {
		return _FUNDAMT;
	}

	public void setFUNDAMT(String fundamt) {
		this._FUNDAMT = fundamt;
	}

	public String getFUNDACN() {
		return _FUNDACN;
	}

	public void setFUNDACN(String fundacn) {
		this._FUNDACN = fundacn;
	}

	public String getFDTXTYPE() {
		return _FDTXTYPE;
	}

	public void setFDTXTYPE(String fdtxtype) {
		this._FDTXTYPE = fdtxtype;
	}

}
