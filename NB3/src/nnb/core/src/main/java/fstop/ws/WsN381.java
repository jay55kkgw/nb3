package fstop.ws;

public interface WsN381{

	public WsN382Out getN382 (CmUser params);
	
    public 	WsN392Out actionN392 (WsN392In params);
    
    public WsN383Out getN383 (CmUser params);
    
    public WsN393Out actionN393 (WsN393In params);
    
    public WsN384Out getN384(CmUser params);
    
    public WsN394Out actionN394 (WsN394In params);
}