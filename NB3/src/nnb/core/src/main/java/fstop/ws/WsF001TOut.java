package fstop.ws;

public class WsF001TOut {

	private String _MsgCode;

	private String _MsgName;

	private String _CMQTIME;

	private String _CURAMT;

	private String _AVAAMT;

	public String getMsgCode() {
		return _MsgCode;
	}
	public void setMsgCode(String msgcode) {
		this._MsgCode = msgcode;
	}

	public String getMsgName() {
		return _MsgName;
	}
	public void setMsgName(String msgname) {
		this._MsgName = msgname;
	}

	public String getCMQTIME() {
		return _CMQTIME;
	}
	public void setCMQTIME(String cmqtime) {
		this._CMQTIME = cmqtime;
	}

	public String getCURAMT() {
		return _CURAMT;
	}
	public void setCURAMT(String curamt) {
		this._CURAMT = curamt;
	}

	public String getAVAAMT() {
		return _AVAAMT;
	}
	public void setAVAAMT(String avaamt) {
		this._AVAAMT = avaamt;
	}
}
