package fstop.ws;

public class WsN930EOut {

	private String _MsgCode;

	private String _MsgName;

	private String _DPMYEMAIL;

	private String _DPNOTIFY1;

	private String _DPNOTIFY2;

	private String _DPNOTIFY3;

	private String _DPNOTIFY4;

	private String _DPNOTIFY5;

	private String _DPNOTIFY12;

	private String _DPNOTIFY10;

	private String _DPNOTIFY13;
	
	private String _DPNOTIFY6;
	
	private String _DPNOTIFY7;
	
	private String _DPNOTIFY8;
	
	private String _DPNOTIFY14;
	
	private String _DPNOTIFY9;
	
	private String _DPNOTIFY11;
	
	private String _DPNOTIFY15;

	private String _DPNOTIFY16;
	
	private String _DPNOTIFY17;
	
	private String _DPNOTIFY18;
	
	private String _DPNOTIFY19;

	private String _DPNOTIFY20;

	private String _DPNOTIFY21;

	
	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getDPMYEMAIL() {
		return _DPMYEMAIL;
	}

	public void setDPMYEMAIL(String DPMYEMAIL) {
		this._DPMYEMAIL = DPMYEMAIL;
	}

	public String getDPNOTIFY1() {
		return _DPNOTIFY1;
	}

	public void setDPNOTIFY1(String DPNOTIFY1) {
		this._DPNOTIFY1 = DPNOTIFY1;
	}

	public String getDPNOTIFY2() {
		return _DPNOTIFY2;
	}

	public void setDPNOTIFY2(String DPNOTIFY2) {
		this._DPNOTIFY2 = DPNOTIFY2;
	}

	public String getDPNOTIFY3() {
		return _DPNOTIFY3;
	}

	public void setDPNOTIFY3(String DPNOTIFY3) {
		this._DPNOTIFY3 = DPNOTIFY3;
	}

	public String getDPNOTIFY4() {
		return _DPNOTIFY4;
	}

	public void setDPNOTIFY4(String DPNOTIFY4) {
		this._DPNOTIFY4 = DPNOTIFY4;
	}

	public String getDPNOTIFY5() {
		return _DPNOTIFY5;
	}

	public void setDPNOTIFY5(String DPNOTIFY5) {
		this._DPNOTIFY5 = DPNOTIFY5;
	}

	public String getDPNOTIFY12() {
		return _DPNOTIFY12;
	}

	public void setDPNOTIFY12(String DPNOTIFY12) {
		this._DPNOTIFY12 = DPNOTIFY12;
	}

	public String getDPNOTIFY10() {
		return _DPNOTIFY10;
	}

	public void setDPNOTIFY10(String DPNOTIFY10) {
		this._DPNOTIFY10 = DPNOTIFY10;
	}

	public String getDPNOTIFY13() {
		return _DPNOTIFY13;
	}

	public void setDPNOTIFY13(String DPNOTIFY13) {
		this._DPNOTIFY13 = DPNOTIFY13;
	}
	
	public String getDPNOTIFY6() {
		return _DPNOTIFY6;
	}

	public void setDPNOTIFY6(String DPNOTIFY6) {
		this._DPNOTIFY6 = DPNOTIFY6;
	}
	
	public String getDPNOTIFY7() {
		return _DPNOTIFY7;
	}

	public void setDPNOTIFY7(String DPNOTIFY7) {
		this._DPNOTIFY7 = DPNOTIFY7;
	}
	
	public String getDPNOTIFY8() {
		return _DPNOTIFY8;
	}

	public void setDPNOTIFY8(String DPNOTIFY8) {
		this._DPNOTIFY8 = DPNOTIFY8;
	}
	
	public String getDPNOTIFY14() {
		return _DPNOTIFY14;
	}

	public void setDPNOTIFY14(String DPNOTIFY14) {
		this._DPNOTIFY14 = DPNOTIFY14;
	}
	
	public String getDPNOTIFY9() {
		return _DPNOTIFY9;
	}

	public void setDPNOTIFY9(String DPNOTIFY9) {
		this._DPNOTIFY9 = DPNOTIFY9;
	}
	
	public String getDPNOTIFY11() {
		return _DPNOTIFY11;
	}

	public void setDPNOTIFY11(String DPNOTIFY11) {
		this._DPNOTIFY11 = DPNOTIFY11;
	}
	
	public String getDPNOTIFY15() {
		return _DPNOTIFY15;
	}

	public void setDPNOTIFY15(String DPNOTIFY15) {
		this._DPNOTIFY15 = DPNOTIFY15;
	}
	
	public String getDPNOTIFY16() {
		return _DPNOTIFY16;
	}

	public void setDPNOTIFY16(String DPNOTIFY16) {
		this._DPNOTIFY16 = DPNOTIFY16;
	}
	
	public String getDPNOTIFY17() {
		return _DPNOTIFY17;
	}

	public void setDPNOTIFY17(String DPNOTIFY17) {
		this._DPNOTIFY17 = DPNOTIFY17;
	}
	
	public String getDPNOTIFY18() {
		return _DPNOTIFY18;
	}

	public void setDPNOTIFY18(String DPNOTIFY18) {
		this._DPNOTIFY18 = DPNOTIFY18;
	}
	
	public String getDPNOTIFY19() {
		return _DPNOTIFY19;
	}

	public void setDPNOTIFY19(String DPNOTIFY19) {
		this._DPNOTIFY19 = DPNOTIFY19;
	}
	
	public String getDPNOTIFY20() {
		return _DPNOTIFY20;
	}

	public void setDPNOTIFY20(String DPNOTIFY20) {
		this._DPNOTIFY20 = DPNOTIFY20;
	}
	
	public String getDPNOTIFY21() {
		return _DPNOTIFY21;
	}

	public void setDPNOTIFY21(String DPNOTIFY21) {
		this._DPNOTIFY21 = DPNOTIFY21;
	}
}
