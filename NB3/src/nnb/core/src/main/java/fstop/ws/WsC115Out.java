package fstop.ws;

public class WsC115Out {
 
	private String _MsgCode;	//訊息代碼
	private String _MsgName;	//訊息名稱
	private String _CMQTIME;	//查詢時間
	private String _COUNSUM;	//資料總數
	private String _NAME;		//姓名
	private WsC115Row[] _Table;	//表格結果

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getCMQTIME() {
		return _CMQTIME;
	}

	public void setCMQTIME(String CMQTIME) {
		this._CMQTIME = CMQTIME;
	}

	public String getCOUNSUM() {
		return _COUNSUM;
	}

	public void setCOUNSUM(String COUNSUM) {
		this._COUNSUM = COUNSUM;
	}
	
	public String getNAME() {
		return _NAME;
	}

	public void setNAME(String NAME) {
		this._NAME = NAME;
	}
	
	public WsC115Row[] getTable() {
		return _Table;
	}

	public void setTable(WsC115Row[] table) {
		this._Table = table;
	}

}
