package fstop.ws;

public class WsN373Out {

	private String _MsgCode;// 訊息代碼
	private String _MsgName;// 訊息名稱
	private String _CMQTIME;// 統一編號
	private String _CUSIDN;// 身份證號
	private String _NAME;// 姓名
	private String _CREDITNO;// 信託帳號
	private String _TRANSCODE;// 基金代碼
	private String _FUNDLNAME;// 基金名稱
	private String _INTRANSCODE;// 轉入基金
	private String _INFUNDLNAME;// 轉入基金名稱
	private String _BILLSENDMODE;// 轉換方式
	private String _UNIT;// 轉出單位數
	private String _CRY;// 轉出信託金額幣別代碼
	private String _ADCCYNAME;// 轉出信託金額幣別名稱
	private String _FUNDAMT;//轉出信託金額
	private String _AMT3;// 轉換手續費
	private String _FCA2;// 基金公司外加手續費
	private String _FCA1;// 補收手續費
	private String _AMT5;// 扣款金額
	private String _TRADEDATE;// 生效日期
	private String _OUTACN;// 扣款帳號	
	
	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getCMQTIME() {
		return _CMQTIME;
	}

	public void setCMQTIME(String CMQTIME) {
		this._CMQTIME = CMQTIME;
	}

	public String getCUSIDN() {
		return _CUSIDN;
	}

	public void setCUSIDN(String CUSIDN) {
		this._CUSIDN = CUSIDN;
	}

	public String getNAME() {
		return _NAME;
	}

	public void setNAME(String NAME) {
		this._NAME = NAME;
	}

	public String getCREDITNO() {
		return _CREDITNO;
	}

	public void setCREDITNO(String CREDITNO) {
		this._CREDITNO = CREDITNO;
	}

	public String getTRANSCODE() {
		return _TRANSCODE;
	}

	public void setTRANSCODE(String TRANSCODE) {
		this._TRANSCODE = TRANSCODE;
	}

	public String getFUNDLNAME() {
		return _FUNDLNAME;
	}

	public void setFUNDLNAME(String FUNDLNAME) {
		this._FUNDLNAME = FUNDLNAME;
	}

	public String getINTRANSCODE() {
		return _INTRANSCODE;
	}

	public void setINTRANSCODE(String INTRANSCODE) {
		this._INTRANSCODE = INTRANSCODE;
	}

	public String getINFUNDLNAME() {
		return _INFUNDLNAME;
	}

	public void setINFUNDLNAME(String INFUNDLNAME) {
		this._INFUNDLNAME = INFUNDLNAME;
	}

	public String getBILLSENDMODE() {
		return _BILLSENDMODE;
	}

	public void setBILLSENDMODE(String BILLSENDMODE) {
		this._BILLSENDMODE = BILLSENDMODE;
	}

	public String getUNIT() {
		return _UNIT;
	}

	public void setUNIT(String UNIT) {
		this._UNIT = UNIT;
	}

	public String getCRY() {
		return _CRY;
	}

	public void setCRY(String CRY) {
		this._CRY = CRY;
	}

	public String getADCCYNAME() {
		return _ADCCYNAME;
	}

	public void setADCCYNAME(String ADCCYNAME) {
		this._ADCCYNAME = ADCCYNAME;
	}

	public String getFUNDAMT() {
		return _FUNDAMT;
	}

	public void setFUNDAMT(String FUNDAMT) {
		this._FUNDAMT = FUNDAMT;
	}

	public String getAMT3() {
		return _AMT3;
	}

	public void setAMT3(String AMT3) {
		this._AMT3 = AMT3;
	}

	public String getFCA2() {
		return _FCA2;
	}

	public void setFCA2(String FCA2) {
		this._FCA2 = FCA2;
	}

	public String getFCA1() {
		return _FCA1;
	}

	public void setFCA1(String FCA1) {
		this._FCA1 = FCA1;
	}

	public String getAMT5() {
		return _AMT5;
	}

	public void setAMT5(String AMT5) {
		this._AMT5 = AMT5;
	}

	public String getTRADEDATE() {
		return _TRADEDATE;
	}

	public void setTRADEDATE(String TRADEDATE) {
		this._TRADEDATE = TRADEDATE;
	}

	public String getOUTACN() {
		return _OUTACN;
	}

	public void setOUTACN(String OUTACN) {
		this._OUTACN = OUTACN;
	}

}
