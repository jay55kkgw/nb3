package fstop.ws;

public class CmMsgData {
	
	private String _MsgCode;//	訊息代碼;
	
	private String _MsgName;//訊息名稱;
	
	private String _QryData	;//版本正確;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getQryData() {
		return _QryData;
	}

	public void setQryData(String qryData) {
		this._QryData = qryData;
	}


}
