package fstop.ws;

public interface WsN177{
    
	//外匯定存自動轉期申請變更交易
    public WsN177Out action(WsN177In params);
    //外匯定存約定/常用帳號查詢
    public CmFxAcno getACNList(CmUserQry params);
}