package fstop.ws;

public interface WsPhoneToken{
	
	public CmMsg sendMessage(CmPhoneTokenRow params);
	
	public CmMsg action(CmPhoneTokenRow params);
	
	public CmCheckToken checkToken(CmPhoneTokenRow params);
	
	public CmMsg notifySetting(CmNotifySetting params);
	
	public CmNotifySettingOut queryNotifySetting(CmPhoneTokenRow params);
}