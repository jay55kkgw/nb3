package fstop.ws;

public class CmAcnoRow {
	
	private String _ACN;
	
	private String _BNKCOD;
	
	private String _AGREE;
	
	private String _DPGONAME;
	
	private String _DPPHOTOID;
	
	private String _VALUE;
	
	private String _TEXT;

	public String getACN() {
		return _ACN;
	}

	public void setACN(String acn) {
		this._ACN = acn;
	}

	public String getBNKCOD() {
		return _BNKCOD;
	}

	public void setBNKCOD(String bnkcod) {
		this._BNKCOD = bnkcod;
	}

	public String getTEXT() {
		return _TEXT;
	}

	public void setTEXT(String text) {
		this._TEXT = text;
	}

	public String getAGREE() {
		return _AGREE;
	}

	public void setAGREE(String agree) {
		this._AGREE = agree;
	}

	public String getDPGONAME() {
		return _DPGONAME;
	}

	public void setDPGONAME(String dpgoname) {
		this._DPGONAME = dpgoname;
	}

	public String getDPPHOTOID() {
		return _DPPHOTOID;
	}

	public void setDPPHOTOID(String dpphotoid) {
		this._DPPHOTOID = dpphotoid;
	}

	public String getVALUE() {
		return _VALUE;
	}

	public void setVALUE(String value) {
		this._VALUE = value;
	}
	
}
