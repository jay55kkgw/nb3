package fstop.ws;

public class WsN100In {

	private String _SessionId;

	private String _POSTCOD2;

	private String _CTTADR;

	private String _ARACOD;

	private String _TELNUM;

	private String _TELEXT;

	private String _ARACOD2;

	private String _TELNUM2;

	private String _MOBTEL;

	private String _TXTOKEN;

	private String _OTPKEY;

	private String _CAPTCHA;
	
//	private String _ModifyBaseStr;
	
//	private String _N950PASSWORD;

	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		this._SessionId = sessionId;
	}

	public String getPOSTCOD2() {
		return _POSTCOD2;
	}

	public void setPOSTCOD2(String postcod2) {
		this._POSTCOD2 = postcod2;
	}

	public String getCTTADR() {
		return _CTTADR;
	}

	public void setCTTADR(String cttadr) {
		this._CTTADR = cttadr;
	}

	public String getARACOD() {
		return _ARACOD;
	}

	public void setARACOD(String aracod) {
		this._ARACOD = aracod;
	}

	public String getTELNUM() {
		return _TELNUM;
	}

	public void setTELNUM(String telnum) {
		this._TELNUM = telnum;
	}

	public String getTELEXT() {
		return _TELEXT;
	}

	public void setTELEXT(String telext) {
		this._TELEXT = telext;
	}

	public String getARACOD2() {
		return _ARACOD2;
	}

	public void setARACOD2(String aracod2) {
		this._ARACOD2 = aracod2;
	}

	public String getTELNUM2() {
		return _TELNUM2;
	}

	public void setTELNUM2(String telnum2) {
		this._TELNUM2 = telnum2;
	}

	public String getMOBTEL() {
		return _MOBTEL;
	}

	public void setMOBTEL(String mobtel) {
		this._MOBTEL = mobtel;
	}

	public String getTXTOKEN() {
		return _TXTOKEN;
	}

	public void setTXTOKEN(String txtoken) {
		this._TXTOKEN = txtoken;
	}

	public String getOTPKEY() {
		return _OTPKEY;
	}

	public void setOTPKEY(String otpkey) {
		this._OTPKEY = otpkey;
	}

	public String getCAPTCHA() {
		return _CAPTCHA;
	}

	public void setCAPTCHA(String captcha) {
		this._CAPTCHA = captcha;
	}

//	public String getModifyBaseStr() {
//		return _ModifyBaseStr;
//	}
//
//	public void setModifyBaseStr(String modifyBaseStr) {
//		this._ModifyBaseStr = modifyBaseStr;
//	}

//	public String getN950PASSWORD() {
//		return _N950PASSWORD;
//	}
//
//	public void setN950PASSWORD(String N950PASSWORD) {
//		this._N950PASSWORD = N950PASSWORD;
//	}

}
