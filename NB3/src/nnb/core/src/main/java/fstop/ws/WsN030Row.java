package fstop.ws;

public class WsN030Row {

	private String _CRY;
	
	private String _CRYNAME;

	private String _BUYITR;

	private String _SELITR;

	private String _TERM;

	private String _RECNO;

	public String getCRY() {
		return _CRY;
	}

	public void setCRY(String CRY) {
		this._CRY = CRY;
	}

	public String getCRYNAME() {
		return _CRYNAME;
	}

	public void setCRYNAME(String CRYNAME) {
		this._CRYNAME = CRYNAME;
	}
	
	public String getBUYITR() {
		return _BUYITR;
	}

	public void setBUYITR(String BUYITR) {
		this._BUYITR = BUYITR;
	}

	public String getSELITR() {
		return _SELITR;
	}

	public void setSELITR(String SELITR) {
		this._SELITR = SELITR;
	}

	public String getTERM() {
		return _TERM;
	}

	public void setTERM(String TERM) {
		this._TERM = TERM;
	}

	public String getRECNO() {
		return _RECNO;
	}

	public void setRECNO(String RECNO) {
		this._RECNO = RECNO;
	}

}
