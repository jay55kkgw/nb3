package fstop.ws;

public class CmCrdAcno {

	private String _MsgCode;
	
	private String _MsgName;
	
	private CmCrdAcnoRow[] _Table;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public CmCrdAcnoRow[] getTable() {
		return _Table;
	}

	public void setTable(CmCrdAcnoRow[] table) {
		this._Table = table;
	}
	
	
}
