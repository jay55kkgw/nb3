package fstop.ws;

public class WsC013Out {

	private String _MsgCode;// 訊息代碼
	private String _MsgName;// 訊息名稱
	private String _CREDITNO;// 信託編號
	private WsC013Row[] _Table;// 表格結果

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getCREDITNO() {
		return _CREDITNO;
	}

	public void setCREDITNO(String creditno) {
		this._CREDITNO = creditno;
	}

	public WsC013Row[] getTable() {
		return _Table;
	}

	public void setTable(WsC013Row[] table) {
		this._Table = table;
	}

}
