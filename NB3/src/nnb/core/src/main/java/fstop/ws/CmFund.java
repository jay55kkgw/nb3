package fstop.ws;

public class CmFund {
	
    private String _MsgCode	;//訊息代碼
	
	private String _MsgName	;//訊息名稱
	
	private CmFundRow[] _Table;//	表格結果

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public CmFundRow[] getTable() {
		return _Table;
	}

	public void setTable(CmFundRow[] table) {
		this._Table = table;
	}

}
