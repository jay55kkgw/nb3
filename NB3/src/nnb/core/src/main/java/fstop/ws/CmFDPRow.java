package fstop.ws;

public class CmFDPRow {

	private String FDPACN;//	存單帳號
	private String FDPNUM;//	存單號碼
	private String AMTFDP;// 存單金額
	private String TEXT;// 好記 網頁顯示的存單帳號 EX:050-臺灣企銀-FDPACN
	public String getFDPACN() {
		return FDPACN;
	}
	public void setFDPACN(String fDPACN) {
		FDPACN = fDPACN;
	}
	public String getFDPNUM() {
		return FDPNUM;
	}
	public void setFDPNUM(String fDPNUM) {
		FDPNUM = fDPNUM;
	}
	public String getAMTFDP() {
		return AMTFDP;
	}
	public void setAMTFDP(String amtfdp) {
		AMTFDP = amtfdp;
	}
	public String getTEXT() {
		return TEXT;
	}
	public void setTEXT(String text) {
		TEXT = text;
	}
	
	

}
