package fstop.ws;

public class WsN283TWRow {

	private String _DPSCHNO;

	private String _DPPERMTDATE;

	private String _DPFTDATE;

	private String _DPNEXTDATE;

	private String _DPWDAC;

	private String _DPSVAC;

	private String _DPTXAMT;

	private String _TXTYPE;

	private String _DPTXMEMO;

	private String _TXWAY;

	private String _DPTXCODE;

	private String _DPSCHID;
	
	public String getDPSCHNO() {
		return _DPSCHNO;
	}
	public void setDPSCHNO(String dpschno) {
		this._DPSCHNO = dpschno;
	}

	public String getDPPERMTDATE() {
		return _DPPERMTDATE;
	}
	public void setDPPERMTDATE(String dppermtdate) {
		this._DPPERMTDATE = dppermtdate;
	}

	public String getDPFTDATE() {
		return _DPFTDATE;
	}
	public void setDPFTDATE(String dpftdate) {
		this._DPFTDATE = dpftdate;
	}

	public String getDPNEXTDATE() {
		return _DPNEXTDATE;
	}
	public void setDPNEXTDATE(String dpnextdate) {
		this._DPNEXTDATE = dpnextdate;
	}

	public String getDPWDAC() {
		return _DPWDAC;
	}
	public void setDPWDAC(String dpwdac) {
		this._DPWDAC = dpwdac;
	}

	public String getDPSVAC() {
		return _DPSVAC;
	}
	public void setDPSVAC(String dpsvac) {
		this._DPSVAC = dpsvac;
	}

	public String getDPTXAMT() {
		return _DPTXAMT;
	}
	public void setDPTXAMT(String dptxamt) {
		this._DPTXAMT = dptxamt;
	}

	public String getTXTYPE() {
		return _TXTYPE;
	}
	public void setTXTYPE(String txtype) {
		this._TXTYPE = txtype;
	}

	public String getDPTXMEMO() {
		return _DPTXMEMO;
	}
	public void setDPTXMEMO(String dptxmemo) {
		this._DPTXMEMO = dptxmemo;
	}

	public String getTXWAY() {
		return _TXWAY;
	}
	public void setTXWAY(String txway) {
		this._TXWAY = txway;
	}

	public String getDPTXCODE() {
		return _DPTXCODE;
	}
	public void setDPTXCODE(String dptxcode) {
		this._DPTXCODE = dptxcode;
	}

	public String getDPSCHID() {
		return _DPSCHID;
	}
	public void setDPSCHID(String dpschid) {
		this._DPSCHID = dpschid;
	}

}
