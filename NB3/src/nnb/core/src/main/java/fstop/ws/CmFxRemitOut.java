package fstop.ws;

public class CmFxRemitOut {
	
	private String _MsgCode;

	private String _MsgName;
	
	private String _ADRMTTYPE;

	private CmFxRemitOutRow[] _Table;
	
	public String getMsgCode() {
		return _MsgCode;
	}
	public void setMsgCode(String msgcode) {
		this._MsgCode = msgcode;
	}

	public String getMsgName() {
		return _MsgName;
	}
	public void setMsgName(String msgname) {
		this._MsgName = msgname;
	}

	public String getADRMTTYPE() {
		return _ADRMTTYPE;
	}
	public void setADRMTTYPE(String adrmttype) {
		this._ADRMTTYPE = adrmttype;
	}
	public CmFxRemitOutRow[] getTable() {
		return _Table;
	}
	public void setTable(CmFxRemitOutRow[] table) {
		this._Table = table;
	}

}
