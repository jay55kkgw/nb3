package fstop.ws;

public class CmMyRemitOut {
	
	private String _MsgCode;

	private String _MsgName;

	private CmMyRemitOutRow[] _Table;
	
	public String getMsgCode() {
		return _MsgCode;
	}
	public void setMsgCode(String msgcode) {
		this._MsgCode = msgcode;
	}

	public String getMsgName() {
		return _MsgName;
	}
	public void setMsgName(String msgname) {
		this._MsgName = msgname;
	}

	public CmMyRemitOutRow[] getTable() {
		return _Table;
	}
	public void setTable(CmMyRemitOutRow[] table) {
		this._Table = table;
	}
}
