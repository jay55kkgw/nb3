package fstop.ws;

public class WsN997In {

	private String _SessionId;
	
	private String _DPGONAME;
	
	private String _DPPHOTOID;
	
	private String _DPTRACNO;
	
	private String _DPTRIBANK;
	
	private String _DPTRDACNO;
	
	@Override
	public String toString()
	{
		return "WsN997In [_SessionId=" + _SessionId + ", _DPGONAME=" + _DPGONAME + ", _DPPHOTOID=" + _DPPHOTOID
				+ ", _DPTRACNO=" + _DPTRACNO + ", _DPTRIBANK=" + _DPTRIBANK + ", _DPTRDACNO=" + _DPTRDACNO + "]";
	}

	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		this._SessionId = sessionId;
	}

	public String getDPGONAME() {
		return _DPGONAME;
	}

	public void setDPGONAME(String dpgoname) {
		this._DPGONAME = dpgoname;
	}

	public String getDPPHOTOID() {
		return _DPPHOTOID;
	}

	public void setDPPHOTOID(String dpphotoid) {
		this._DPPHOTOID = dpphotoid;
	}

	public String getDPTRACNO() {
		return _DPTRACNO;
	}

	public void setDPTRACNO(String dptracno) {
		this._DPTRACNO = dptracno;
	}

	public String getDPTRIBANK() {
		return _DPTRIBANK;
	}

	public void setDPTRIBANK(String dptribank) {
		this._DPTRIBANK = dptribank;
	}

	public String getDPTRDACNO() {
		return _DPTRDACNO;
	}

	public void setDPTRDACNO(String dptrdacno) {
		this._DPTRDACNO = dptrdacno;
	}

}
