package fstop.ws;

public class WsN077In {
	
	private String _SessionId;       //身份識別
	private String _N950PASSWORD;	 //交易密碼
	private String _TXTOKEN;		 //防止重送代碼
	private String _CAPTCHA;		 //圖形驗證碼
	private String _FGTXWAY;		 //交易機制
	private String _CMPASSWORD;		 //交易密碼
	private String _PINNEW;		 	 //交易密碼SHA1值
	private String _OTPKEY;			 //OTP動態密碼
	private String _FDPACN;		 	 //存單帳號
	private String _FDPNUM;			 //存單號碼
	private String _AMT;		 	 //存單金額
	private String _FDPTYPE;		 //存款種類	
	private String _INTMTH;		 	 //計息方式
	private String _FGRENCNT;		 //轉期次數	
	private String _FGSVTYPE;		 //轉存方式
	private String _TSFACN;		 	 //利息轉入帳號	
	private String _DPRENCNT;        //轉期次數區間
	
	
	
	
	@Override
	public String toString()
	{
		return "WsN077In [_SessionId=" + _SessionId + ", _TXTOKEN=" + _TXTOKEN + ", _CAPTCHA=" + _CAPTCHA
				+ ", _FGTXWAY=" + _FGTXWAY + ", _OTPKEY=" + _OTPKEY + ", _FDPACN=" + _FDPACN + ", _FDPNUM=" + _FDPNUM
				+ ", _AMT=" + _AMT + ", _FDPTYPE=" + _FDPTYPE + ", _INTMTH=" + _INTMTH + ", _FGRENCNT=" + _FGRENCNT
				+ ", _FGSVTYPE=" + _FGSVTYPE + ", _TSFACN=" + _TSFACN + ", _DPRENCNT=" + _DPRENCNT + "]";
	}
	public String getSessionId() {
		return _SessionId;
	}
	public void setSessionId(String sessionid) {
		this._SessionId = sessionid;
	}

	public String getN950PASSWORD() {
		return _N950PASSWORD;
	}
	public void setN950PASSWORD(String N950PASSWORD) {
		this._N950PASSWORD = N950PASSWORD;
	}
	
	public String getTXTOKEN() {
		return _TXTOKEN;
	}
	public void setTXTOKEN(String txtoken) {
		this._TXTOKEN = txtoken;
	}

	public String getCAPTCHA() {
		return _CAPTCHA;
	}
	public void setCAPTCHA(String CAPTCHA) {
		this._CAPTCHA = CAPTCHA;
	}
	
	public String getFGTXWAY() {
		return _FGTXWAY;
	}
	public void setFGTXWAY(String FGTXWAY) {
		this._FGTXWAY = FGTXWAY;
	}

	public String getCMPASSWORD() {
		return _CMPASSWORD;
	}
	public void setCMPASSWORD(String CMPASSWORD) {
		this._CMPASSWORD = CMPASSWORD;
	}

	public String getPINNEW() {
		return _PINNEW;
	}
	public void setPINNEW(String PINNEW) {
		this._PINNEW = PINNEW;
	}

	public String getOTPKEY() {
		return _OTPKEY;
	}
	public void setOTPKEY(String OTPKEY) {
		this._OTPKEY = OTPKEY;
	}

	public String getFDPACN() {
		return _FDPACN;
	}
	public void setFDPACN(String FDPACN) {
		this._FDPACN = FDPACN;
	}
	
	public String getFDPNUM() {
		return _FDPNUM;
	}
	public void setFDPNUM(String FDPNUM) {
		this._FDPNUM = FDPNUM;
	}

	public String getAMT() {
		return _AMT;
	}
	public void setAMT(String AMT) {
		this._AMT = AMT;
	}

	public String getFDPTYPE() {
		return _FDPTYPE;
	}
	public void setFDPTYPE(String FDPTYPE) {
		this._FDPTYPE = FDPTYPE;
	}

	public String getINTMTH() {
		return _INTMTH;
	}
	public void setINTMTH(String INTMTH) {
		this._INTMTH = INTMTH;
	}
	
	public String getFGRENCNT() {
		return _FGRENCNT;
	}
	public void setFGRENCNT(String FGRENCNT) {
		this._FGRENCNT = FGRENCNT;
	}
	
	public String getFGSVTYPE() {
		return _FGSVTYPE;
	}
	public void setFGSVTYPE(String FGSVTYPE) {
		this._FGSVTYPE = FGSVTYPE;
	}
	
	public String getTSFACN() {
		return _TSFACN;
	}
	public void setTSFACN(String TSFACN) {
		this._TSFACN = TSFACN;
	}
	public String getDPRENCNT() {
		return _DPRENCNT;
	}
	public void setDPRENCNT(String dprencnt) {
		this._DPRENCNT = dprencnt;
	}

}
