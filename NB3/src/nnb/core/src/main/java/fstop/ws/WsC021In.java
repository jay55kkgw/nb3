package fstop.ws;

public class WsC021In {

	private String _SessionId;// 身份識別

	private String _N950PASSWORD;// 交易密碼

	private String _TXTOKEN;// 防止重送代碼

	private String _FGTXWAY;// 密碼類別

	private String _CMPASSWORD;// 交易密碼

	private String _PINNEW;// 交易密碼SHA1值

	private String _OTPKEY;// OTP動態密碼

	private String _BILLSENDMODE;// 轉換方式

	private String _CUSIDN;// 統一編號

	private String _CREDITNO;// 信託帳號

	private String _TRANSCODE;// 基金代碼

	private String _FUNDAMT;// 信託金額

	private String _TRADEDATE;// 生效日期

	private String _INTRANSCODE;// 轉入基金代碼

	private String _UNIT;// 轉出單位數

	private String _FCA1;// 補收手續費

	private String _AMT3;// 申購金額

	private String _FCA2;// 手續費

	private String _AMT5;// 扣款金額

	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		this._SessionId = sessionId;
	}

	public String getN950PASSWORD() {
		return _N950PASSWORD;
	}

	public void setN950PASSWORD(String n950password) {
		this._N950PASSWORD = n950password;
	}

	public String getTXTOKEN() {
		return _TXTOKEN;
	}

	public void setTXTOKEN(String txtoken) {
		this._TXTOKEN = txtoken;
	}

	public String getFGTXWAY() {
		return _FGTXWAY;
	}

	public void setFGTXWAY(String fgtxway) {
		this._FGTXWAY = fgtxway;
	}

	public String getCMPASSWORD() {
		return _CMPASSWORD;
	}

	public void setCMPASSWORD(String cmpassword) {
		this._CMPASSWORD = cmpassword;
	}

	public String getPINNEW() {
		return _PINNEW;
	}

	public void setPINNEW(String pinnew) {
		this._PINNEW = pinnew;
	}

	public String getOTPKEY() {
		return _OTPKEY;
	}

	public void setOTPKEY(String otpkey) {
		this._OTPKEY = otpkey;
	}

	public String getBILLSENDMODE() {
		return _BILLSENDMODE;
	}

	public void setBILLSENDMODE(String billsendmode) {
		this._BILLSENDMODE = billsendmode;
	}

	public String getCUSIDN() {
		return _CUSIDN;
	}

	public void setCUSIDN(String cusidn) {
		this._CUSIDN = cusidn;
	}

	public String getCREDITNO() {
		return _CREDITNO;
	}

	public void setCREDITNO(String creditno) {
		this._CREDITNO = creditno;
	}

	public String getTRANSCODE() {
		return _TRANSCODE;
	}

	public void setTRANSCODE(String transcode) {
		this._TRANSCODE = transcode;
	}

	public String getFUNDAMT() {
		return _FUNDAMT;
	}

	public void setFUNDAMT(String fundamt) {
		this._FUNDAMT = fundamt;
	}

	public String getTRADEDATE() {
		return _TRADEDATE;
	}

	public void setTRADEDATE(String tradedate) {
		this._TRADEDATE = tradedate;
	}

	public String getINTRANSCODE() {
		return _INTRANSCODE;
	}

	public void setINTRANSCODE(String intranscode) {
		this._INTRANSCODE = intranscode;
	}

	public String getUNIT() {
		return _UNIT;
	}

	public void setUNIT(String unit) {
		this._UNIT = unit;
	}

	public String getFCA1() {
		return _FCA1;
	}

	public void setFCA1(String fca1) {
		this._FCA1 = fca1;
	}

	public String getAMT3() {
		return _AMT3;
	}

	public void setAMT3(String amt3) {
		this._AMT3 = amt3;
	}

	public String getFCA2() {
		return _FCA2;
	}

	public void setFCA2(String fca2) {
		this._FCA2 = fca2;
	}

	public String getAMT5() {
		return _AMT5;
	}

	public void setAMT5(String amt5) {
		this._AMT5 = amt5;
	}

}
