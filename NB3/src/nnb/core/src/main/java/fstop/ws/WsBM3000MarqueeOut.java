package fstop.ws;

public class WsBM3000MarqueeOut {

	
	private String _MsgCode;
	
	private String _MsgName;
	
	private WsBM3000MarqueeRow[] _Table;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public WsBM3000MarqueeRow[] getTable() {
		return _Table;
	}

	public void setTable(WsBM3000MarqueeRow[] table) {
		this._Table = table;
	}	
}
