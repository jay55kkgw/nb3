package fstop.ws;

public class CmFxDepList {

	
	private String MsgCode;//	訊息代碼	
	private String MsgName;//	訊息名稱	
	private String NAME;//	付款人名稱	
	private String VIPNUMF;//	外幣匯款人VIP註記	
	private String[] List;//	字串陣列	
	public String getMsgCode() {
		return MsgCode;
	}
	public void setMsgCode(String msgCode) {
		MsgCode = msgCode;
	}
	public String getMsgName() {
		return MsgName;
	}
	public void setMsgName(String msgName) {
		MsgName = msgName;
	}
	public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	public String getVIPNUMF() {
		return VIPNUMF;
	}
	public void setVIPNUMF(String vIPNUMF) {
		VIPNUMF = vIPNUMF;
	}
	public String[] getList() {
		return List;
	}
	public void setList(String[] list) {
		List = list;
	}

	
}
