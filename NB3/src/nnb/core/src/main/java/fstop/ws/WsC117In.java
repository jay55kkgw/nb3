package fstop.ws;

public class WsC117In {

	private String _SessionId;     //身份識別
	private String _N950PASSWORD;  //交易密碼
	private String _CUSIDN;        //身分證字號/統一編號
	private String _TXTOKEN;       //防止重送代碼
	private String _CAPTCHA;       //圖形驗證碼
	private String _FGTXWAY;	   //交易機制
	private String _CMPASSWORD;	   //交易密碼
	private String _PINNEW;	   	   //交易密碼SHA1值
	private String _CREDITNO;      //信託帳號	
	private String _PAYTAG;        //基金代號		
	private String _FUNDAMT;	   //信託金額
	private String _FUNDCUR;       //信託金額幣別	
	private String _PAYAMT;        //扣款金額	
	private String _PAYCUR;        //扣款幣別	
	private String _STOPPROF;      //變更前停利
	private String _STOPLOSS;      //變更前停損
	private String _NSTOPLOSS;     //變更後停損
	private String _NSTOPPROF;     //變更後停利
	private String _AC202;         //類別
	private String _MIP;           //定期不定額註記

	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionid) {
		this._SessionId = sessionid;
	}

	public String getN950PASSWORD() {
		return _N950PASSWORD;
	}

	public void setN950PASSWORD(String n950password) {
		this._N950PASSWORD = n950password;
	}

	public String getCUSIDN() {
		return _CUSIDN;
	}

	public void setCUSIDN(String CUSIDN) {
		this._CUSIDN = CUSIDN;
	}
	
	public String getTXTOKEN() {
		return _TXTOKEN;
	}

	public void setTXTOKEN(String txtoken) {
		this._TXTOKEN = txtoken;
	}

	public String getCAPTCHA() {
		return _CAPTCHA;
	}

	public void setCAPTCHA(String CAPTCHA) {
		this._CAPTCHA = CAPTCHA;
	}
	
	public String getFGTXWAY() {
		return _FGTXWAY;
	}

	public void setFGTXWAY(String fgtxway) {
		this._FGTXWAY = fgtxway;
	}

	public String getCMPASSWORD() {
		return _CMPASSWORD;
	}

	public void setCMPASSWORD(String CMPASSWORD) {
		this._CMPASSWORD = CMPASSWORD;
	}
	
	public String getPINNEW() {
		return _PINNEW;
	}

	public void setPINNEW(String PINNEW) {
		this._PINNEW = PINNEW;
	}
	
	public String getCREDITNO() {
		return _CREDITNO;
	}
	
	public void setCREDITNO(String CREDITNO) {
		this._CREDITNO = CREDITNO;
	}
	
	public String getPAYTAG() {
		return _PAYTAG;
	}

	public void setPAYTAG(String PAYTAG) {
		this._PAYTAG = PAYTAG;
	}
	
	public String getFUNDAMT() {
		return _FUNDAMT;
	}

	public void setFUNDAMT(String FUNDAMT) {
		this._FUNDAMT = FUNDAMT;
	}

	public String getFUNDCUR() {
		return _FUNDCUR;
	}

	public void setFUNDCUR(String FUNDCUR) {
		this._FUNDCUR = FUNDCUR;
	}

	public String getPAYAMT() {
		return _PAYAMT;
	}

	public void setPAYAMT(String PAYAMT) {
		this._PAYAMT = PAYAMT;
	}

	public String getPAYCUR() {
		return _PAYCUR;
	}

	public void setPAYCUR(String PAYCUR) {
		this._PAYCUR = PAYCUR;
	}
	
	public String getSTOPPROF() {
		return _STOPPROF;
	}

	public void setSTOPPROF(String STOPPROF) {
		this._STOPPROF = STOPPROF;
	}

	public String getSTOPLOSS() {
		return _STOPLOSS;
	}

	public void setSTOPLOSS(String STOPLOSS) {
		this._STOPLOSS = STOPLOSS;
	}

	public String getNSTOPLOSS() {
		return _NSTOPLOSS;
	}

	public void setNSTOPLOSS(String NSTOPLOSS) {
		this._NSTOPLOSS = NSTOPLOSS;
	}

	public String getNSTOPPROF() {
		return _NSTOPPROF;
	}

	public void setNSTOPPROF(String NSTOPPROF) {
		this._NSTOPPROF = NSTOPPROF;
	}
	
	public String getAC202() {
		return _AC202;
	}
	
	public void setAC202(String AC202) {
		this._AC202 = AC202;
	}

	public String getMIP() {
		return _MIP;
	}
	
	public void setMIP(String MIP) {
		this._MIP = MIP;
	}
}
