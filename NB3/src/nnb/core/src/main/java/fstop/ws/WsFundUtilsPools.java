package fstop.ws;

public interface WsFundUtilsPools{

	public CmFundCom getFundCompany(CmUserQry params);

	public CmFund getFundDataByCompanyMip(CmUserFundMip params);

	public CmFundData getFundData(CmUserQry params);

	public CmConfirmYout doConfirmY(CmUserQry params);

	public CmMsg doConfirmN(CmUserQry params);

	public CmMsgData getKYCDATE(CmUser params);

	public CmMsgData checkFundType(CmUser params);

	public CmMsgData isFundBizTime(CmUser params);

	public CmMsgData checkRisk(CmUserQry params);

	public CmApMsg updateFundAgreeVersion(CmUser params);

	public CmFund getFundDataByGroupMark(CmUserFund params);
	
	public WsN812Out getCardACN(CmUser params);
	
	public CmMsg isFundRsvTime(CmUserFdTime params);
	
	public CmFund getFundDataByCompany(CmUserFund params); 
	
	public CmFund getFundDataByCompany(CmUserFundMip params); 
}