package fstop.ws;

public class CmFundComRow {
	
	private String _COUNTRYTYPE	;//國內/國外
	
	private String _COMPANYCODE;//	基金公司代碼
	
	private String _COMPANYSNAME;//	基金公司名稱

	public String getCOUNTRYTYPE() {
		return _COUNTRYTYPE;
	}

	public void setCOUNTRYTYPE(String countrytype) {
		this._COUNTRYTYPE = countrytype;
	}

	public String getCOMPANYCODE() {
		return _COMPANYCODE;
	}

	public void setCOMPANYCODE(String companycode) {
		this._COMPANYCODE = companycode;
	}

	public String getCOMPANYSNAME() {
		return _COMPANYSNAME;
	}

	public void setCOMPANYSNAME(String companysname) {
		this._COMPANYSNAME = companysname;
	}


	
}
