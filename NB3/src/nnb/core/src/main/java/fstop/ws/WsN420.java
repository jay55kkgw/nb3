package fstop.ws;

public interface WsN420{

	public WsN420Out getN420List(CmUserQry params);
	
	//	臺幣定存單到期續存明細
	public WsN420Out getN078List(CmUserQry prarams);
	
	public WsN420Out getN077List(CmUserQry params);
}