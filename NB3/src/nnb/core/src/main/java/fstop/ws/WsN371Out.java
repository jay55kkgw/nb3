package fstop.ws;

public class WsN371Out {

	private String _MsgCode;// 訊息代碼

	private String _MsgName;// 訊息名稱

	private String _CMQTIME;// 交易時間

	private String _CUSIDN;// 身份證號

	private String _NAME;// 姓名

	private String _CREDITNO;// 信託帳號

	private String _TRANSCODE;// 基金代碼

	private String _FUNDLNAME;// 基金名稱

	private String _INTRANSCODE;// 轉入基金

	private String _INFUNDLNAME;// 轉入基金名稱

	private String _BILLSENDMODE;// 轉換方式

	private String _UNIT;// 轉出單位數

	private String _FUNCUR;// 轉出信託金額幣別

	private String _FUNDAMT;// 轉出信託金額

	private String _AMT3;// 轉換手續費

	private String _FCA2;// 基金公司外加手續費

	private String _FCA1;// 補收手續費

	private String _AMT5;// 扣款金額

	private String _TRADEDATE;// 生效日期

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgcode) {
		this._MsgCode = msgcode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgname) {
		this._MsgName = msgname;
	}

	public String getCMQTIME() {
		return _CMQTIME;
	}

	public void setCMQTIME(String cmqtime) {
		this._CMQTIME = cmqtime;
	}

	public String getCUSIDN() {
		return _CUSIDN;
	}

	public void setCUSIDN(String cusidn) {
		this._CUSIDN = cusidn;
	}

	public String getNAME() {
		return _NAME;
	}

	public void setNAME(String name) {
		this._NAME = name;
	}

	public String getCREDITNO() {
		return _CREDITNO;
	}

	public void setCREDITNO(String creditno) {
		this._CREDITNO = creditno;
	}

	public String getTRANSCODE() {
		return _TRANSCODE;
	}

	public void setTRANSCODE(String transcode) {
		this._TRANSCODE = transcode;
	}

	public String getFUNDLNAME() {
		return _FUNDLNAME;
	}

	public void setFUNDLNAME(String fundlname) {
		this._FUNDLNAME = fundlname;
	}

	public String getINTRANSCODE() {
		return _INTRANSCODE;
	}

	public void setINTRANSCODE(String intranscode) {
		this._INTRANSCODE = intranscode;
	}

	public String getINFUNDLNAME() {
		return _INFUNDLNAME;
	}

	public void setINFUNDLNAME(String infundlname) {
		this._INFUNDLNAME = infundlname;
	}

	public String getBILLSENDMODE() {
		return _BILLSENDMODE;
	}

	public void setBILLSENDMODE(String billsendmode) {
		this._BILLSENDMODE = billsendmode;
	}

	public String getUNIT() {
		return _UNIT;
	}

	public void setUNIT(String unit) {
		this._UNIT = unit;
	}

	public String getFUNCUR() {
		return _FUNCUR;
	}

	public void setFUNCUR(String funcur) {
		this._FUNCUR = funcur;
	}

	public String getFUNDAMT() {
		return _FUNDAMT;
	}

	public void setFUNDAMT(String fundamt) {
		this._FUNDAMT = fundamt;
	}

	public String getAMT3() {
		return _AMT3;
	}

	public void setAMT3(String amt3) {
		this._AMT3 = amt3;
	}

	public String getFCA2() {
		return _FCA2;
	}

	public void setFCA2(String fca2) {
		this._FCA2 = fca2;
	}

	public String getFCA1() {
		return _FCA1;
	}

	public void setFCA1(String fca1) {
		this._FCA1 = fca1;
	}

	public String getAMT5() {
		return _AMT5;
	}

	public void setAMT5(String amt5) {
		this._AMT5 = amt5;
	}

	public String getTRADEDATE() {
		return _TRADEDATE;
	}

	public void setTRADEDATE(String tradedate) {
		this._TRADEDATE = tradedate;
	}

}
