package fstop.ws;

public interface WsN283TW{
	
	public WsN283TWOut action(CmUser params);

	public WsN283CancelOut cancel(WsN283CancelIn params);
}