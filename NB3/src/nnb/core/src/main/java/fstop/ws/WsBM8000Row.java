package fstop.ws;

public class WsBM8000Row {

	private String _LISTNAME;	//行銷主題

	private String _LISTURL;	//行銷網址
	
	private String _CRMID;			//行銷編號

	public String getCRMID() {
		return _CRMID;
	}

	public void setCRMID(String crmid) {
		_CRMID = crmid;
	}

	public String getLISTNAME() {
		return _LISTNAME;
	}

	public void setLISTNAME(String listname) {
		_LISTNAME = listname;
	}

	public String getLISTURL() {
		return _LISTURL;
	}

	public void setLISTURL(String listurl) {
		_LISTURL = listurl;
	}	
}
