package fstop.ws;

public class WsN178beforIn {

	private String SessionId;//	身份識別
	private String FYACN;//		存單帳號
	private String FDPNUM;//	存單號碼
	private String AMTFDP;//	存單金額
	private String CRY;//		幣別
	private String TYPE1;//		續存方式
	private String FYTSFAN;//	轉入帳號
	private String INTMTH;//	計息方式
	
	@Override
	public String toString()
	{
		return "WsN178beforIn [SessionId=" + SessionId + ", FYACN=" + FYACN + ", FDPNUM=" + FDPNUM + ", AMTFDP="
				+ AMTFDP + ", CRY=" + CRY + ", TYPE1=" + TYPE1 + ", FYTSFAN=" + FYTSFAN + ", INTMTH=" + INTMTH + "]";
	}
	public String getSessionId() {
		return SessionId;
	}
	public void setSessionId(String sessionId) {
		SessionId = sessionId;
	}
	public String getFYACN() {
		return FYACN;
	}
	public void setFYACN(String fyacn) {
		FYACN = fyacn;
	}
	public String getFDPNUM() {
		return FDPNUM;
	}
	public void setFDPNUM(String fdpnum) {
		FDPNUM = fdpnum;
	}
	public String getAMTFDP() {
		return AMTFDP;
	}
	public void setAMTFDP(String amtfdp) {
		AMTFDP = amtfdp;
	}
	public String getCRY() {
		return CRY;
	}
	public void setCRY(String cry) {
		CRY = cry;
	}
	public String getTYPE1() {
		return TYPE1;
	}
	public void setTYPE1(String type1) {
		TYPE1 = type1;
	}
	public String getFYTSFAN() {
		return FYTSFAN;
	}
	public void setFYTSFAN(String fytsfan) {
		FYTSFAN = fytsfan;
	}
	public String getINTMTH() {
		return INTMTH;
	}
	public void setINTMTH(String intmth) {
		INTMTH = intmth;
	}

}
