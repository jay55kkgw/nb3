package fstop.ws;

public class WsN175BeforIn {

	private String SessionId;//	身份識別
	private String FGTRDATE;//	即時/預約記號
	private String ACN;//		帳號
	private String FDPNUM;//	存單號碼
	private String CUID;//		幣別
	private String AMT;//		存單金額
	private String INTMTH;//	計息方式
	private String DPISDT;//	存起日
	private String DUEDAT;//	到期日
	private String CMTRMEMO;//	交易備註
	private String CMTRMAIL;//	Email信箱
	private String CMMAILMEMO;//Email摘要內容
	
	
	
	@Override
	public String toString()
	{
		return "WsN175BeforIn [SessionId=" + SessionId + ", FGTRDATE=" + FGTRDATE + ", ACN=" + ACN + ", FDPNUM="
				+ FDPNUM + ", CUID=" + CUID + ", AMT=" + AMT + ", INTMTH=" + INTMTH + ", DPISDT=" + DPISDT + ", DUEDAT="
				+ DUEDAT + ", CMTRMEMO=" + CMTRMEMO + ", CMTRMAIL=" + CMTRMAIL + ", CMMAILMEMO=" + CMMAILMEMO + "]";
	}
	public String getSessionId() {
		return SessionId;
	}
	public void setSessionId(String sessionId) {
		SessionId = sessionId;
	}
	public String getFGTRDATE() {
		return FGTRDATE;
	}
	public void setFGTRDATE(String fgtrdate) {
		FGTRDATE = fgtrdate;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String acn) {
		ACN = acn;
	}
	public String getFDPNUM() {
		return FDPNUM;
	}
	public void setFDPNUM(String fdpnum) {
		FDPNUM = fdpnum;
	}
	public String getCUID() {
		return CUID;
	}
	public void setCUID(String cuid) {
		CUID = cuid;
	}
	public String getAMT() {
		return AMT;
	}
	public void setAMT(String amt) {
		AMT = amt;
	}
	public String getINTMTH() {
		return INTMTH;
	}
	public void setINTMTH(String intmth) {
		INTMTH = intmth;
	}
	public String getDPISDT() {
		return DPISDT;
	}
	public void setDPISDT(String dpisdt) {
		DPISDT = dpisdt;
	}
	public String getDUEDAT() {
		return DUEDAT;
	}
	public void setDUEDAT(String duedat) {
		DUEDAT = duedat;
	}
	public String getCMTRMEMO() {
		return CMTRMEMO;
	}
	public void setCMTRMEMO(String cmtrmemo) {
		CMTRMEMO = cmtrmemo;
	}
	public String getCMTRMAIL() {
		return CMTRMAIL;
	}
	public void setCMTRMAIL(String cmtrmail) {
		CMTRMAIL = cmtrmail;
	}
	public String getCMMAILMEMO() {
		return CMMAILMEMO;
	}
	public void setCMMAILMEMO(String cmmailmemo) {
		CMMAILMEMO = cmmailmemo;
	}
	
	
	
}
