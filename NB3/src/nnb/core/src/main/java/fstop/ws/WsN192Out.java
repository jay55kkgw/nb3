package fstop.ws;

public class WsN192Out {

	private String _MsgCode;

	private String _MsgName;

	private String _ACN;
	
	private String _CMPERIOD;
	
	private String _i_RecNum;

	private WsN192Row[] _Table;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String MsgCode) {
		this._MsgCode = MsgCode;
	}

	public void setMsgName(String MsgName) {
		this._MsgName = MsgName;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public String getACN() {
		return _ACN;
	}

	public void setACN(String ACN) {
		this._ACN = ACN;
	}
	
	public String getCMPERIOD() {
		return _CMPERIOD;
	}

	public void setCMPERIOD(String CMPERIOD) {
		this._CMPERIOD = CMPERIOD;
	}
	
	public String geti_RecNum() {
		return _i_RecNum;
	}

	public void seti_RecNum(String i_RecNum) {
		this._i_RecNum = i_RecNum;
	}

	public WsN192Row[] getTable() {
		return _Table;
	}

	public void setTable(WsN192Row[] Table) {
		this._Table = Table;
	}

}
