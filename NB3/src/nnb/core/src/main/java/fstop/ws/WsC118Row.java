package fstop.ws;

public class WsC118Row {
	
	private String CREDITNO;//	信託帳號
	private String PAYTAG;//	基金代碼
	private String FUNDLNAME;//	基金代碼名稱
	private String AC202;//	信託型態
	private String AC202NAME;//	信託型態
	private String FUNDCUR;//	信託幣別
	private String FUNDCURNAME;//信託金額幣別(中文)
	private String FUNDAMT;//	信託金額
	private String ACUCOUNT;//	單位數
	private String REFVALUE;//	參考淨值
	private String FXRATE;//	參考匯率
	private String AMT;//	參考現值
	private String NAMT;//	未分配金額
	private String RTNC;//	基準報酬率正負
	private String RTNRATE;//	基準報酬率
	private String STOPPROF;//	原自動贖回停利設定
	private String STOPLOSS;//	原自動贖回停損設定
	private String MIP;//定期不定額註記
	public String getCREDITNO() {
		return CREDITNO;
	}
	public void setCREDITNO(String creditno) {
		CREDITNO = creditno;
	}
	public String getPAYTAG() {
		return PAYTAG;
	}
	public void setPAYTAG(String paytag) {
		PAYTAG = paytag;
	}
	public String getFUNDLNAME() {
		return FUNDLNAME;
	}
	public void setFUNDLNAME(String fundlname) {
		FUNDLNAME = fundlname;
	}
	public String getAC202() {
		return AC202;
	}
	public void setAC202(String ac202) {
		AC202 = ac202;
	}
	
	public String getAC202NAME() {
		return AC202NAME;
	}
	public void setAC202NAME(String ac202name) {
		AC202NAME = ac202name;
	}
	public String getFUNDCUR() {
		return FUNDCUR;
	}
	public void setFUNDCUR(String fundcur) {
		FUNDCUR = fundcur;
	}
	
	public String getFUNDCURNAME() {
		return FUNDCURNAME;
	}
	public void setFUNDCURNAME(String fundcurname) {
		FUNDCURNAME = fundcurname;
	}
	public String getFUNDAMT() {
		return FUNDAMT;
	}
	public void setFUNDAMT(String fundamt) {
		FUNDAMT = fundamt;
	}
	public String getACUCOUNT() {
		return ACUCOUNT;
	}
	public void setACUCOUNT(String acucount) {
		ACUCOUNT = acucount;
	}
	public String getREFVALUE() {
		return REFVALUE;
	}
	public void setREFVALUE(String refvalue) {
		REFVALUE = refvalue;
	}
	public String getFXRATE() {
		return FXRATE;
	}
	public void setFXRATE(String fxrate) {
		FXRATE = fxrate;
	}
	public String getAMT() {
		return AMT;
	}
	public void setAMT(String amt) {
		AMT = amt;
	}
	public String getNAMT() {
		return NAMT;
	}
	public void setNAMT(String namt) {
		NAMT = namt;
	}
	public String getRTNC() {
		return RTNC;
	}
	public void setRTNC(String rtnc) {
		RTNC = rtnc;
	}
	public String getRTNRATE() {
		return RTNRATE;
	}
	public void setRTNRATE(String rtnrate) {
		RTNRATE = rtnrate;
	}
	public String getSTOPPROF() {
		return STOPPROF;
	}
	public void setSTOPPROF(String stopprof) {
		STOPPROF = stopprof;
	}
	public String getSTOPLOSS() {
		return STOPLOSS;
	}
	public void setSTOPLOSS(String stoploss) {
		STOPLOSS = stoploss;
	}
	public String getMIP() {
		return MIP;
	}
	public void setMIP(String mip) {
		MIP = mip;
	}
	

	

}
