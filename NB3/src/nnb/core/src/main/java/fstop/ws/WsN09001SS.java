package fstop.ws;

public class WsN09001SS {

	private String _SessionId;
	
	private String _N950PASSWORD;

	private String _SVACN;

	private String _ACN;

	private String _TRNGD;

	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		this._SessionId = sessionId;
	}

	public String getN950PASSWORD() {
		return _N950PASSWORD;
	}

	public void setN950PASSWORD(String n950password) {
		_N950PASSWORD = n950password;
	}
	
	public void setSVACN(String SVACN) {
		this._SVACN = SVACN;
	}

	public String getSVACN() {
		return _SVACN;
	}

	public String getACN() {
		return _ACN;
	}

	public void setACN(String ACN) {
		this._ACN = ACN;
	}

	public String getTRNGD() {
		return _TRNGD;
	}

	public void setTRNGD(String TRNGD) {
		this._TRNGD = TRNGD;
	}

}
