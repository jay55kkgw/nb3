package fstop.ws;

public class WsC020Out {

	private String _MsgCode;

	private String _MsgName;

	private String _CMQTIME;

	private String _FDINVTYPE;

	private String _TYPE;

	private String _FUNDLNAME;

	private String _RISK;

	private String _CUR;

	private String _AMT3;

	private String _FCAFEE;

	private String _FCA2;

	private String _AMT5;

	private String _FUNDACN;

	private String _TRADEDATE;

	private String _PAYDAY1;

	private String _PAYDAY2;

	private String _PAYDAY3;

	private String _DBDATE;

	private String _YIELD;

	private String _STOP;

	private String _CUSIDN;

	private String _NAME;

	public String getCMQTIME() {
		return _CMQTIME;
	}

	public void setCMQTIME(String cmqtime) {
		this._CMQTIME = cmqtime;
	}

	public String getFDINVTYPE() {
		return _FDINVTYPE;
	}

	public void setFDINVTYPE(String fdinvtype) {
		this._FDINVTYPE = fdinvtype;
	}

	public String getTYPE() {
		return _TYPE;
	}

	public void setTYPE(String type) {
		this._TYPE = type;
	}

	public String getFUNDLNAME() {
		return _FUNDLNAME;
	}

	public void setFUNDLNAME(String fundlname) {
		this._FUNDLNAME = fundlname;
	}

	public String getRISK() {
		return _RISK;
	}

	public void setRISK(String risk) {
		this._RISK = risk;
	}

	public String getCUR() {
		return _CUR;
	}

	public void setCUR(String cur) {
		this._CUR = cur;
	}

	public String getAMT3() {
		return _AMT3;
	}

	public void setAMT3(String amt3) {
		this._AMT3 = amt3;
	}

	public String getFCAFEE() {
		return _FCAFEE;
	}

	public void setFCAFEE(String fcafee) {
		this._FCAFEE = fcafee;
	}

	public String getFCA2() {
		return _FCA2;
	}

	public void setFCA2(String fca2) {
		this._FCA2 = fca2;
	}

	public String getAMT5() {
		return _AMT5;
	}

	public void setAMT5(String amt5) {
		this._AMT5 = amt5;
	}

	public String getFUNDACN() {
		return _FUNDACN;
	}

	public void setFUNDACN(String fundacn) {
		this._FUNDACN = fundacn;
	}

	public String getTRADEDATE() {
		return _TRADEDATE;
	}

	public void setTRADEDATE(String tradedate) {
		this._TRADEDATE = tradedate;
	}

	public String getPAYDAY1() {
		return _PAYDAY1;
	}

	public void setPAYDAY1(String payday1) {
		this._PAYDAY1 = payday1;
	}

	public String getPAYDAY2() {
		return _PAYDAY2;
	}

	public void setPAYDAY2(String payday2) {
		this._PAYDAY2 = payday2;
	}

	public String getPAYDAY3() {
		return _PAYDAY3;
	}

	public void setPAYDAY3(String payday3) {
		this._PAYDAY3 = payday3;
	}

	public String getDBDATE() {
		return _DBDATE;
	}

	public void setDBDATE(String dbdate) {
		this._DBDATE = dbdate;
	}

	public String getYIELD() {
		return _YIELD;
	}

	public void setYIELD(String yield) {
		this._YIELD = yield;
	}

	public String getSTOP() {
		return _STOP;
	}

	public void setSTOP(String stop) {
		this._STOP = stop;
	}

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getCUSIDN() {
		return _CUSIDN;
	}

	public void setCUSIDN(String cusidn) {
		this._CUSIDN = cusidn;
	}

	public String getNAME() {
		return _NAME;
	}

	public void setNAME(String name) {
		this._NAME = name;
	}

}
