package fstop.ws;

public class WsGD11HSOut {
	
	private String _MsgCode;//	訊息代碼
	
	private String _MsgName	;//訊息名稱
	
	private String _SYSDATE	;//系統日期
	
	private String _SYSTIME	;//系統時間
	
	private WsGD11HSRow1[] _Table1;//	表格結果

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getSYSDATE() {
		return _SYSDATE;
	}

	public void setSYSDATE(String sysdate) {
		this._SYSDATE = sysdate;
	}

	public String getSYSTIME() {
		return _SYSTIME;
	}

	public void setSYSTIME(String systime) {
		this._SYSTIME = systime;
	}

	public WsGD11HSRow1[] getTable1() {
		return _Table1;
	}

	public void setTable1(WsGD11HSRow1[] table1) {
		this._Table1 = table1;
	}
	
	
	
	


}
