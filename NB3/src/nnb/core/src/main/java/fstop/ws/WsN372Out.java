package fstop.ws;

public class WsN372Out {

	private String _MsgCode;// 訊息代碼
	private String _MsgName;// 訊息名稱
	private String _CMQTIME;// 交易時間
	private String _FDINVTYPE;// 客戶投資屬性
	private String _TYPE;// 扣款方式
	private String _FUNDLNAME;// 基金名稱
	private String _RISK;// 商品風險等級
	private String _CRY1;// 幣別代碼
	private String _ADCCYNAME;// 幣別名稱
	private String _AMT3;// 申購金額
	private String _FCAFEE;// 手續費率
	private String _FCA2;// 手續費
	private String _AMT5;// 扣款金額
	private String _OUTACN;// 扣帳帳號
	private String _TRADEDATE;// 生效日期
	private String _YIELD;// 停利通知設定
	private String _STOP;// 停損通知設定
	private String _CUSIDN;// 身分字號/統一編號
	private String _NAME;// 姓名

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getCMQTIME() {
		return _CMQTIME;
	}

	public void setCMQTIME(String cmqtime) {
		this._CMQTIME = cmqtime;
	}

	public String getFDINVTYPE() {
		return _FDINVTYPE;
	}

	public void setFDINVTYPE(String fdinvtype) {
		this._FDINVTYPE = fdinvtype;
	}

	public String getTYPE() {
		return _TYPE;
	}

	public void setTYPE(String type) {
		this._TYPE = type;
	}

	public String getFUNDLNAME() {
		return _FUNDLNAME;
	}

	public void setFUNDLNAME(String fundlname) {
		this._FUNDLNAME = fundlname;
	}

	public String getRISK() {
		return _RISK;
	}

	public void setRISK(String risk) {
		this._RISK = risk;
	}

	public String getCRY1() {
		return _CRY1;
	}

	public void setCRY1(String cry1) {
		this._CRY1 = cry1;
	}

	public String getADCCYNAME() {
		return _ADCCYNAME;
	}

	public void setADCCYNAME(String adccyname) {
		this._ADCCYNAME = adccyname;
	}

	public String getAMT3() {
		return _AMT3;
	}

	public void setAMT3(String amt3) {
		this._AMT3 = amt3;
	}

	public String getFCAFEE() {
		return _FCAFEE;
	}

	public void setFCAFEE(String fcafee) {
		this._FCAFEE = fcafee;
	}

	public String getFCA2() {
		return _FCA2;
	}

	public void setFCA2(String fca2) {
		this._FCA2 = fca2;
	}

	public String getAMT5() {
		return _AMT5;
	}

	public void setAMT5(String amt5) {
		this._AMT5 = amt5;
	}

	public String getOUTACN() {
		return _OUTACN;
	}

	public void setOUTACN(String outacn) {
		this._OUTACN = outacn;
	}

	public String getTRADEDATE() {
		return _TRADEDATE;
	}

	public void setTRADEDATE(String tradedate) {
		this._TRADEDATE = tradedate;
	}

	public String getYIELD() {
		return _YIELD;
	}

	public void setYIELD(String yield) {
		this._YIELD = yield;
	}

	public String getSTOP() {
		return _STOP;
	}

	public void setSTOP(String stop) {
		this._STOP = stop;
	}

	public String getCUSIDN() {
		return _CUSIDN;
	}

	public void setCUSIDN(String cusidn) {
		this._CUSIDN = cusidn;
	}

	public String getNAME() {
		return _NAME;
	}

	public void setNAME(String name) {
		this._NAME = name;
	}

}
