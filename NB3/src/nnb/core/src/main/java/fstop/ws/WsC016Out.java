package fstop.ws;

public class WsC016Out {

	private String _MsgCode;

	private String _MsgName;

	private String _CUSIDN;

	private String _CUSNAME;

	private String _TRADEDATE;

	private String _AMT3;

	private String _FCA2;

	private String _HTELPHONE;

	private String _AMT5;

	private String _FCAFEE;

	private String _FUDCUR;

	private String _STOP;

	private String _YIELD;

	private String _LINK_FUND_NAME;

	private String _OTELPHONE;

	private String _SSLTXNO;

	private String _PAYDAY1;

	private String _PAYDAY2;

	private String _PAYDAY3;

	private String _STR_1;

	private String _STR_2;

	private String _STR_3;

	private String _STR_4;

	private String _STR_5;

	private String _STR_6;

	private String _STR_7;

	private String _STR_9;

	private String _LINK_FH_NAME;

	private String _LINK_FEE_01;

	private String _LINK_FEE_02;

	private String _LINK_FEE_05;

	private String _LINK_FEE_07;

	private String _LINK_SLS_08;

	private String _LINK_BASE_01;

	private String _LINK_RESULT_05;

	private String _LINK_RESULT_06;
	
	private String _DBDATE;//	首次扣日期
	
	private String _CRYNAME;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgcode) {
		this._MsgCode = msgcode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgname) {
		this._MsgName = msgname;
	}

	public String getCUSIDN() {
		return _CUSIDN;
	}

	public void setCUSIDN(String cusidn) {
		this._CUSIDN = cusidn;
	}

	public String getCUSNAME() {
		return _CUSNAME;
	}

	public void setCUSNAME(String cusname) {
		this._CUSNAME = cusname;
	}

	public String getTRADEDATE() {
		return _TRADEDATE;
	}

	public void setTRADEDATE(String tradedate) {
		this._TRADEDATE = tradedate;
	}

	public String getAMT3() {
		return _AMT3;
	}

	public void setAMT3(String amt3) {
		this._AMT3 = amt3;
	}

	public String getFCA2() {
		return _FCA2;
	}

	public void setFCA2(String fca2) {
		this._FCA2 = fca2;
	}

	public String getHTELPHONE() {
		return _HTELPHONE;
	}

	public void setHTELPHONE(String htelphone) {
		this._HTELPHONE = htelphone;
	}

	public String getAMT5() {
		return _AMT5;
	}

	public void setAMT5(String amt5) {
		this._AMT5 = amt5;
	}

	public String getFCAFEE() {
		return _FCAFEE;
	}

	public void setFCAFEE(String fcafee) {
		this._FCAFEE = fcafee;
	}

	public String getFUDCUR() {
		return _FUDCUR;
	}

	public void setFUDCUR(String fudcur) {
		this._FUDCUR = fudcur;
	}

	public String getSTOP() {
		return _STOP;
	}

	public void setSTOP(String stop) {
		this._STOP = stop;
	}

	public String getYIELD() {
		return _YIELD;
	}

	public void setYIELD(String yield) {
		this._YIELD = yield;
	}

	public String getLINK_FUND_NAME() {
		return _LINK_FUND_NAME;
	}

	public void setLINK_FUND_NAME(String link_fund_name) {
		this._LINK_FUND_NAME = link_fund_name;
	}

	public String getOTELPHONE() {
		return _OTELPHONE;
	}

	public void setOTELPHONE(String otelphone) {
		this._OTELPHONE = otelphone;
	}

	public String getSSLTXNO() {
		return _SSLTXNO;
	}

	public void setSSLTXNO(String ssltxno) {
		this._SSLTXNO = ssltxno;
	}

	public String getPAYDAY1() {
		return _PAYDAY1;
	}

	public void setPAYDAY1(String payday1) {
		this._PAYDAY1 = payday1;
	}

	public String getPAYDAY2() {
		return _PAYDAY2;
	}

	public void setPAYDAY2(String payday2) {
		this._PAYDAY2 = payday2;
	}

	public String getPAYDAY3() {
		return _PAYDAY3;
	}

	public void setPAYDAY3(String payday3) {
		this._PAYDAY3 = payday3;
	}

	public String getSTR_1() {
		return _STR_1;
	}

	public void setSTR_1(String str_1) {
		this._STR_1 = str_1;
	}

	public String getSTR_2() {
		return _STR_2;
	}

	public void setSTR_2(String str_2) {
		this._STR_2 = str_2;
	}

	public String getSTR_3() {
		return _STR_3;
	}

	public void setSTR_3(String str_3) {
		this._STR_3 = str_3;
	}

	public String getSTR_4() {
		return _STR_4;
	}

	public void setSTR_4(String str_4) {
		this._STR_4 = str_4;
	}

	public String getSTR_5() {
		return _STR_5;
	}

	public void setSTR_5(String str_5) {
		this._STR_5 = str_5;
	}

	public String getSTR_6() {
		return _STR_6;
	}

	public void setSTR_6(String str_6) {
		this._STR_6 = str_6;
	}

	public String getSTR_7() {
		return _STR_7;
	}

	public void setSTR_7(String str_7) {
		this._STR_7 = str_7;
	}

	public String getSTR_9() {
		return _STR_9;
	}

	public void setSTR_9(String str_9) {
		this._STR_9 = str_9;
	}

	public String getLINK_FH_NAME() {
		return _LINK_FH_NAME;
	}

	public void setLINK_FH_NAME(String link_fh_name) {
		this._LINK_FH_NAME = link_fh_name;
	}

	public String getLINK_FEE_01() {
		return _LINK_FEE_01;
	}

	public void setLINK_FEE_01(String link_fee_01) {
		this._LINK_FEE_01 = link_fee_01;
	}

	public String getLINK_FEE_02() {
		return _LINK_FEE_02;
	}

	public void setLINK_FEE_02(String link_fee_02) {
		this._LINK_FEE_02 = link_fee_02;
	}

	public String getLINK_FEE_05() {
		return _LINK_FEE_05;
	}

	public void setLINK_FEE_05(String link_fee_05) {
		this._LINK_FEE_05 = link_fee_05;
	}

	public String getLINK_FEE_07() {
		return _LINK_FEE_07;
	}

	public void setLINK_FEE_07(String link_fee_07) {
		this._LINK_FEE_07 = link_fee_07;
	}

	public String getLINK_SLS_08() {
		return _LINK_SLS_08;
	}

	public void setLINK_SLS_08(String link_sls_08) {
		this._LINK_SLS_08 = link_sls_08;
	}

	public String getLINK_BASE_01() {
		return _LINK_BASE_01;
	}

	public void setLINK_BASE_01(String link_base_01) {
		this._LINK_BASE_01 = link_base_01;
	}

	public String getLINK_RESULT_05() {
		return _LINK_RESULT_05;
	}

	public void setLINK_RESULT_05(String link_result_05) {
		this._LINK_RESULT_05 = link_result_05;
	}

	public String getLINK_RESULT_06() {
		return _LINK_RESULT_06;
	}

	public void setLINK_RESULT_06(String link_result_06) {
		this._LINK_RESULT_06 = link_result_06;
	}

	public String getDBDATE() {
		return _DBDATE;
	}

	public void setDBDATE(String dbdate) {
		this._DBDATE = dbdate;
	}

	public String getCRYNAME() {
		return _CRYNAME;
	}

	public void setCRYNAME(String cryname) {
		this._CRYNAME = cryname;
	}

}
