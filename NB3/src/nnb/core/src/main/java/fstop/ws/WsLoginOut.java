package fstop.ws;

public class WsLoginOut {
	
	private String _MsgCode;
	
	private String _MsgName;
	
	private String _SessionId;
	
	private String _LOGINDT;
	
	private String _LOGINTM;
	
	private String _XMLCOD;
	
	private String _DPUSERNAME;
	
	private String _DPMYEMAIL;
		
	private String _AUTHORITY;//業務權限
	
	private String _MBSTAT;
	
	private String _DIGCODE;

	private String TWFSTS;
	
	private String QRCODE_AGREE;

	private String NOCARD_CONSENT;  //無卡提款註記

	private String NOCARD; //是否申請無卡提款密碼

	private String MRKCODE;


	private String cardNo; //手機條碼
	private String einPhone; //電子發票手機號碼
	private String showCardNoFlag; //是否自動顯示手機條碼


	public String getMRKCODE() {
		return MRKCODE;
	}

	public void setMRKCODE(String MRKCODE) {
		this.MRKCODE = MRKCODE;
	}

	public String getAUTHORITY() {
		return _AUTHORITY;
	}

	public void setAUTHORITY(String AUTHORITY) {
		this._AUTHORITY = AUTHORITY;
	}
	
	public String getLOGINDT() {
		return _LOGINDT;
	}

	public void setLOGINDT(String LOGINDT) {
		this._LOGINDT = LOGINDT;
	}
	
	public String getLOGINTM() {
		return _LOGINTM;
	}

	public void setLOGINTM(String LOGINTM) {
		this._LOGINTM = LOGINTM;
	}
	
	public String getXMLCOD() {
		return _XMLCOD;
	}

	public void setXMLCOD(String XMLCOD) {
		this._XMLCOD = XMLCOD;
	}
	
	public String getDPUSERNAME() {
		return _DPUSERNAME;
	}

	public void setDPUSERNAME(String DPUSERNAME) {
		this._DPUSERNAME = DPUSERNAME;
	}
	
	public String getDPMYEMAIL() {
		return _DPMYEMAIL;
	}

	public void setDPMYEMAIL(String DPMYEMAIL) {
		this._DPMYEMAIL = DPMYEMAIL;
	}
	
	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String MsgCode) {
		this._MsgCode = MsgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String MsgName) {
		this._MsgName = MsgName;
	}

	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String SessionId) {
		this._SessionId = SessionId;
	}
	
	public String getMBSTAT() {
		return _MBSTAT;
	}

	public void setMBSTAT(String mbstat) {
		this._MBSTAT = mbstat;
	}
	
	public String getDIGCODE() {
		return _DIGCODE;
	}

	public void setDIGCODE(String digcode) {
		this._DIGCODE = digcode;
	}

	public String getTWFSTS() {
		return TWFSTS;
	}

	public void setTWFSTS(String tWFSTS) {
		TWFSTS = tWFSTS;
	}

	public String getQRCODE_AGREE() {
		return QRCODE_AGREE;
	}

	public void setQRCODE_AGREE(String qRCODE_AGREE) {
		QRCODE_AGREE = qRCODE_AGREE;
	}

	public String getNOCARD_CONSENT() {
		return NOCARD_CONSENT;
	}

	public void setNOCARD_CONSENT(String NOCARD_CONSENT) {
		this.NOCARD_CONSENT = NOCARD_CONSENT;
	}

	public String getNOCARD() {
		return NOCARD;
	}

	public void setNOCARD(String NOCARD) {
		this.NOCARD = NOCARD;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getEinPhone() {
		return einPhone;
	}

	public void setEinPhone(String einPhone) {
		this.einPhone = einPhone;
	}

	public String getShowCardNoFlag() {
		return showCardNoFlag;
	}

	public void setShowCardNoFlag(String showCardNoFlag) {
		this.showCardNoFlag = showCardNoFlag;
	}
}
