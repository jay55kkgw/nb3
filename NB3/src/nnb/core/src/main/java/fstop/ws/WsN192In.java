package fstop.ws;

public class WsN192In {

	private String _SessionId;

	private String _ACN;

	private String _FGPERIOD;

	private String _CMSDATE;

	private String _CMEDATE;
	
	private String _N950PASSWORD;

	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		this._SessionId = sessionId;
	}

	public void setACN(String ACN) {
		this._ACN = ACN;
	}

	public String getACN() {
		return _ACN;
	}

	public String getFGPERIOD() {
		return _FGPERIOD;
	}

	public void setFGPERIOD(String FGPERIOD) {
		this._FGPERIOD = FGPERIOD;
	}

	public String getCMSDATE() {
		return _CMSDATE;
	}

	public void setCMSDATE(String CMSDATE) {
		this._CMSDATE = CMSDATE;
	}

	public String getCMEDATE() {
		return _CMEDATE;
	}

	public void setCMEDATE(String CMEDATE) {
		this._CMEDATE = CMEDATE;
	}

	public String getN950PASSWORD() {
		return _N950PASSWORD;
	}

	public void setN950PASSWORD(String n950password) {
		_N950PASSWORD = n950password;
	}
}
