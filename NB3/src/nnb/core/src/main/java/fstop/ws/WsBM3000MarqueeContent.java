package fstop.ws;

public class WsBM3000MarqueeContent {

	
	private String _MsgCode;	
	private String _MsgName;	
	private String _DPADID;	
	private String _DPADTYPE;	
	private String _DPADHLK;	
	private String _DPADCON;	
	private String _DPPICTYPE;	
	private String _DPRUNYN;	
	private String _DPNOTE;	
	private String _DPPICPATH;	
	private String _DPBTNNAME;
	private String _DPADDRESS;
	private String _DPMARQUEEPIC;
	private String _DPSDATE;
	private String _DPEDATE;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getDPADID() {
		return _DPADID;
	}

	public void setDPADID(String dpadId) {
		this._DPADID = dpadId;
	}

	public String getDPADTYPE() {
		return _DPADTYPE;
	}

	public void setDPADTYPE(String dpadType) {
		this._DPADTYPE = dpadType;
	}

	public String getDPADHLK() {
		return _DPADHLK;
	}

	public void setDPADHLK(String dpadHlk) {
		this._DPADHLK = dpadHlk;
	}

	public String getDPADCON() {
		return _DPADCON;
	}

	public void setDPADCON(String dpadCon) {
		this._DPADCON = dpadCon;
	}

	public String getDPPICTYPE() {
		return _DPPICTYPE;
	}

	public void setDPPICTYPE(String dpPicType) {
		this._DPPICTYPE = dpPicType;
	}

	public String getDPRUNYN() {
		return _DPRUNYN;
	}

	public void setDPRUNYN(String dpRunYn) {
		this._DPRUNYN = dpRunYn;
	}

	public String getDPNOTE() {
		return _DPNOTE;
	}

	public void setDPNOTE(String dpNote) {
		this._DPNOTE = dpNote;
	}

	public String getDPPICPATH() {
		return _DPPICPATH;
	}

	public void setDPPICPATH(String dpPicPath) {
		this._DPPICPATH = dpPicPath;
	}
	
	public String getDPBTNNAME() {
		return _DPBTNNAME;
	}

	public void setDPBTNNAME(String dpBtnname) {
		this._DPBTNNAME = dpBtnname;
	}
	
	public String getDPADDRESS() {
		return _DPADDRESS;
	}

	public void setDPADDRESS(String dpAddress) {
		this._DPADDRESS = dpAddress;
	}
	
	public String getDPMARQUEEPIC() {
		return this._DPMARQUEEPIC;
	}

	public void setDPMARQUEEPIC(String dpmarqueepic) {
		this._DPMARQUEEPIC = dpmarqueepic;
	}
	
	public String getDPSDATE() {
		return this._DPSDATE;
	}

	public void setDPSDATE(String dpsdate) {
		this._DPSDATE = dpsdate;
	}
	
	public String getDPEDATE() {
		return this._DPEDATE;
	}

	public void setDPEDATE(String dpedate) {
		this._DPEDATE = dpedate;
	}
}
