package fstop.ws;

public interface WsBM8000{
	
	public WsBM8000Out queryCRMData(CmUser params);
	
	public WsBM8000Result updateAllReadCount(CmUser params);
	
	public WsBM8000Result updateReadCount(WsBM8000In params);
	
	public WsBM8000Result updateJoinFlag(WsBM8000In params);
	
	public WsBM8000Result updateNoRemind(WsBM8000In params);
}
