package fstop.ws;

public class WsTD01CardType {
	
	private String _MsgCode;
	
	private String _MsgName;
	// 顯示一般卡
	private String _SHOWCARD0;
	// 顯示VISA金融卡
	private String _SHOWCARD1;
	// N810錯誤訊息代碼
	private String _N810MsgCode;
	// N810錯誤訊息名稱
	private String _N810MsgName;
	// N813錯誤訊息代碼
	private String _N813MsgCode;
	// N813錯誤訊息名稱
	private String _N813MsgName;
	/**
	 * 持有的信用卡List
	 * ex:_List=[4695530002180105, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null]
	 */
	private String[] _List;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		_MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		_MsgName = msgName;
	}

	public String getSHOWCARD0() {
		return _SHOWCARD0;
	}

	public void setSHOWCARD0(String showcard0) {
		_SHOWCARD0 = showcard0;
	}

	public String getSHOWCARD1() {
		return _SHOWCARD1;
	}

	public void setSHOWCARD1(String showcard1) {
		_SHOWCARD1 = showcard1;
	}

	public String getN810MsgCode() {
		return _N810MsgCode;
	}

	public void setN810MsgCode(String msgCode) {
		_N810MsgCode = msgCode;
	}

	public String getN810MsgName() {
		return _N810MsgName;
	}

	public void setN810MsgName(String msgName) {
		_N810MsgName = msgName;
	}

	public String getN813MsgCode() {
		return _N813MsgCode;
	}

	public void setN813MsgCode(String msgCode) {
		_N813MsgCode = msgCode;
	}

	public String getN813MsgName() {
		return _N813MsgName;
	}

	public void setN813MsgName(String msgName) {
		_N813MsgName = msgName;
	}

	public String[] getList() {
		return _List;
	}

	public void setList(String[] list) {
		_List = list;
	}
	
}
