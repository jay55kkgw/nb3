package fstop.ws;

public interface WsN094{
	
	public GDFeeInfo getGDFeeInfoByACN(CmUserACN params);
	
	public WsN094Out action(WsN094In params);
}