package fstop.ws;

public class WsBM3000MergeOut {

	
	private String _MsgCode;
	
	private String _MsgName;
	
	private WsBM3000MergeRow[] _Table;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public WsBM3000MergeRow[] getTable() {
		return _Table;
	}

	public void setTable(WsBM3000MergeRow[] table) {
		this._Table = table;
	}
	
	
}
