package fstop.ws;

public class WsN932Out {

	private String _MsgCode;

	private String _MsgName;

	private WsN932Row[] _Table;



	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public WsN932Row[] getTable() {
		return _Table;
	}

	public void setTable(WsN932Row[] table) {
		_Table = table;
	}

}
