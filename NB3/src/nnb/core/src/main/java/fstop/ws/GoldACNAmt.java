package fstop.ws;

public class GoldACNAmt {

	private String _MsgCode;
	private String _MsgName;
	private String _SVACN;
	private String _AMT_06;
	private String _AMT_16;
	private String _AMT_26;
	private String _AMT_06_DESC;
	private String _AMT_16_DESC;
	private String _AMT_26_DESC;
	private String _FLAG_06;
	private String _FLAG_16;
	private String _FLAG_26;
	private String _FLAG_06_DESC;
	private String _FLAG_16_DESC;
	private String _FLAG_26_DESC;
	private String _CHA_06;
	private String _CHA_16;
	private String _CHA_26;
	
	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String MsgCode) {
		this._MsgCode = MsgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String MsgName) {
		this._MsgName = MsgName;
	}

	public String getSVACN() {
		return _SVACN;
	}

	public void setSVACN(String SVACN) {
		this._SVACN = SVACN;
	}

	public String getAMT_06() {
		return _AMT_06;
	}

	public void setAMT_06(String AMT_06) {
		this._AMT_06 = AMT_06;
	}

	public String getAMT_16() {
		return _AMT_16;
	}

	public void setAMT_16(String AMT_16) {
		this._AMT_16 = AMT_16;
	}

	public String getAMT_26() {
		return _AMT_26;
	}

	public void setAMT_26(String AMT_26) {
		this._AMT_26 = AMT_26;
	}

	public String getAMT_06_DESC() {
		return _AMT_06_DESC;
	}

	public void setAMT_06_DESC(String AMT_06_DESC) {
		this._AMT_06_DESC = AMT_06_DESC;
	}

	public String getAMT_16_DESC() {
		return _AMT_16_DESC;
	}

	public void setAMT_16_DESC(String AMT_16_DESC) {
		this._AMT_16_DESC = AMT_16_DESC;
	}

	public String getAMT_26_DESC() {
		return _AMT_26_DESC;
	}

	public void setAMT_26_DESC(String AMT_26_DESC) {
		this._AMT_26_DESC = AMT_26_DESC;
	}

	public String getFLAG_06() {
		return _FLAG_06;
	}

	public void setFLAG_06(String FLAG_06) {
		this._FLAG_06 = FLAG_06;
	}

	public String getFLAG_16() {
		return _FLAG_16;
	}

	public void setFLAG_16(String FLAG_16) {
		this._FLAG_16 = FLAG_16;
	}

	public String getFLAG_26() {
		return _FLAG_26;
	}

	public void setFLAG_26(String FLAG_26) {
		this._FLAG_26 = FLAG_26;
	}

	public String getFLAG_06_DESC() {
		return _FLAG_06_DESC;
	}

	public void setFLAG_06_DESC(String FLAG_06_DESC) {
		this._FLAG_06_DESC = FLAG_06_DESC;
	}

	public String getFLAG_16_DESC() {
		return _FLAG_16_DESC;
	}

	public void setFLAG_16_DESC(String FLAG_16_DESC) {
		this._FLAG_16_DESC = FLAG_16_DESC;
	}

	public String getFLAG_26_DESC() {
		return _FLAG_26_DESC;
	}

	public void setFLAG_26_DESC(String FLAG_26_DESC) {
		this._FLAG_26_DESC = FLAG_26_DESC;
	}

	public String getCHA_06() {
		return _CHA_06;
	}

	public void setCHA_06(String CHA_06) {
		this._CHA_06 = CHA_06;
	}

	public String getCHA_16() {
		return _CHA_16;
	}

	public void setCHA_16(String CHA_16) {
		this._CHA_16 = CHA_16;
	}

	public String getCHA_26() {
		return _CHA_26;
	}

	public void setCHA_26(String CHA_26) {
		this._CHA_26 = CHA_26;
	}

}
