package fstop.ws;

public class CmUserQry {

	private String _SessionId;
	
	private String _N950PASSWORD;
	
	private String _QryData;		
	
	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String SessionId) {
		this._SessionId = SessionId;
	}

	public String getN950PASSWORD() {
		return _N950PASSWORD;
	}

	public void setN950PASSWORD(String N950PASSWORD) {
		this._N950PASSWORD = N950PASSWORD;
	}
	
	public String getQryData(){
		return _QryData;
	}
	
	public void setQryData(String EBIDIC_HP){
		_QryData = EBIDIC_HP;
	}
	
}
