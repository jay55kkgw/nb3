package fstop.ws;

public class WsN190Out {

	private String _MsgCode;//	訊息代碼
	
	private String _MsgName;//	訊息名稱
	
	private String _CMQTIME;//查詢時間
	
	private String _CMRECNUM;//筆數
	
	private WsN190Row1[] _Table1;//	表格結果

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public WsN190Row1[] getTable1() {
		return _Table1;
	}

	public void setTable1(WsN190Row1[] table1) {
		this._Table1 = table1;
	}

	public String getCMQTIME() {
		return _CMQTIME;
	}

	public void setCMQTIME(String cmqtime) {
		this._CMQTIME = cmqtime;
	}

	public String getCMRECNUM() {
		return _CMRECNUM;
	}

	public void setCMRECNUM(String cmrecnum) {
		this._CMRECNUM = cmrecnum;
	}
	
	

}
