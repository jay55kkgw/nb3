package fstop.ws;

public class WsC024QryRow {

	private String _CREDITNO;

	private String _FUNDLNAME;

	private String _CRYCODE;

	private String _CRYNAME;

	private String _FUNDAMT;

	private String _UNIT;
	
	private String _TRANSCODE;
	
	private String _SHORTTUNIT;
	
	private String _TRADEDATE;
	
	private String _FUNDACN;

	public String getCREDITNO() {
		return _CREDITNO;
	}

	public void setCREDITNO(String creditno) {
		this._CREDITNO = creditno;
	}

	public String getFUNDLNAME() {
		return _FUNDLNAME;
	}

	public void setFUNDLNAME(String fundlname) {
		this._FUNDLNAME = fundlname;
	}

	public String getCRYCODE() {
		return _CRYCODE;
	}

	public void setCRYCODE(String cry) {
		this._CRYCODE = cry;
	}

	public String getCRYNAME() {
		return _CRYNAME;
	}

	public void setCRYNAME(String cry) {
		this._CRYNAME = cry;
	}

	public String getFUNDAMT() {
		return _FUNDAMT;
	}

	public void setFUNDAMT(String fundamt) {
		this._FUNDAMT = fundamt;
	}

	public String getUNIT() {
		return _UNIT;
	}

	public void setUNIT(String unit) {
		this._UNIT = unit;
	}

	public String getTRANSCODE() {
		return _TRANSCODE;
	}

	public void setTRANSCODE(String TRANSCODE) {
		this._TRANSCODE = TRANSCODE;
	}
	
	public String getSHORTTUNIT() {
		return _SHORTTUNIT;
	}

	public void setSHORTTUNIT(String SHORTTUNIT) {
		this._SHORTTUNIT = SHORTTUNIT;
	}
	
	public String getTRADEDATE() {
		return _TRADEDATE;
	}

	public void setTRADEDATE(String TRADEDATE) {
		this._TRADEDATE = TRADEDATE;
	}
	
	public String getFUNDACN() {
		return _FUNDACN;
	}

	public void setFUNDACN(String FUNDACN) {
		this._FUNDACN = FUNDACN;
	}
}
