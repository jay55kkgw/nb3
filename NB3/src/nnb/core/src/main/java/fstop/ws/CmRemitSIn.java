package fstop.ws;

public class CmRemitSIn {
	
	private String _SessionId;

	private String _ADLKINDID;

	private String _ADMKINDID;

	private String _UID;
	
	private String _OutAcn;

	private String _OutCcy;

	private String _InAcn;

	private String _InCcy;
	
	public String getSessionId() {
		return _SessionId;
	}
	public void setSessionId(String sessionid) {
		this._SessionId = sessionid;
	}

	public String getADLKINDID() {
		return _ADLKINDID;
	}
	public void setADLKINDID(String adlkindid) {
		this._ADLKINDID = adlkindid;
	}

	public String getADMKINDID() {
		return _ADMKINDID;
	}
	public void setADMKINDID(String admkindid) {
		this._ADMKINDID = admkindid;
	}

	public String getUID() {
		return _UID;
	}
	public void setUID(String uid) {
		this._UID = uid;
	}

	public String getOutAcn() {
		return _OutAcn;
	}
	public void setOutAcn(String outacn) {
		this._OutAcn = outacn;
	}

	public String getOutCcy() {
		return _OutCcy;
	}
	public void setOutCcy(String outccy) {
		this._OutCcy = outccy;
	}

	public String getInAcn() {
		return _InAcn;
	}
	public void setInAcn(String inacn) {
		this._InAcn = inacn;
	}

	public String getInCcy() {
		return _InCcy;
	}
	public void setInCcy(String inccy) {
		this._InCcy = inccy;
	}
}
