package fstop.ws;

public class WsC119Out {

	private String MsgCode;//	訊息代碼
	private String MsgName;//	訊息名稱
	private String CMQTIME;//	交易時間
	private String CUSIDN;//	身分證字號/統一編號
	private String CREDITNO;//	信託帳號
	private String PAYTAG;//	基金代碼
	private String FUNDLNAME;//	基金代碼名稱
	private String AC202;//	信託型態
	private String FUNDAMT;//	信託金額
	private String FUNDCUR;//	信託幣別
	private String NSTOPPROF;//	停利點
	private String NSTOPLOSS;//	停損點
	private String NOOPT;//	不設定註記
	private String MIP;//	定期不定額註記
	public String getMsgCode() {
		return MsgCode;
	}
	public void setMsgCode(String msgCode) {
		MsgCode = msgCode;
	}
	public String getMsgName() {
		return MsgName;
	}
	public void setMsgName(String msgName) {
		MsgName = msgName;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cmqtime) {
		CMQTIME = cmqtime;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cusidn) {
		CUSIDN = cusidn;
	}
	public String getCREDITNO() {
		return CREDITNO;
	}
	public void setCREDITNO(String creditno) {
		CREDITNO = creditno;
	}
	public String getPAYTAG() {
		return PAYTAG;
	}
	public void setPAYTAG(String paytag) {
		PAYTAG = paytag;
	}
	public String getFUNDLNAME() {
		return FUNDLNAME;
	}
	public void setFUNDLNAME(String fundlname) {
		FUNDLNAME = fundlname;
	}
	public String getAC202() {
		return AC202;
	}
	public void setAC202(String ac202) {
		AC202 = ac202;
	}
	public String getFUNDAMT() {
		return FUNDAMT;
	}
	public void setFUNDAMT(String fundamt) {
		FUNDAMT = fundamt;
	}
	public String getFUNDCUR() {
		return FUNDCUR;
	}
	public void setFUNDCUR(String fundcur) {
		FUNDCUR = fundcur;
	}
	public String getNSTOPPROF() {
		return NSTOPPROF;
	}
	public void setNSTOPPROF(String nstopprof) {
		NSTOPPROF = nstopprof;
	}
	public String getNSTOPLOSS() {
		return NSTOPLOSS;
	}
	public void setNSTOPLOSS(String nstoploss) {
		NSTOPLOSS = nstoploss;
	}
	public String getNOOPT() {
		return NOOPT;
	}
	public void setNOOPT(String noopt) {
		NOOPT = noopt;
	}
	public String getMIP() {
		return MIP;
	}
	public void setMIP(String mip) {
		MIP = mip;
	}
	
	

}
