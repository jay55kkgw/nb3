package fstop.ws;

public class WsN070TwoOut {

	private String _MsgCode;

	private String _MsgName;

	private String _CMQTIME;

	private String _STAN;

	private String _PRIZETIMES;

	private String _OUTACN;

	private String _INTSACN;

	private String _AMOUNT;

	private String _FEE;

	private String _HEADER;

	private String _INPCST;

	private String _O_TOTBAL;

	private String _O_AVLBAL;

	private String _I_TOTBAL;

	private String _I_AVLBAL;
	
//	private String _FGTXDATE;
	
//	private String _CMTRMEMO;
	  
	private String _SCHDATE;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getCMQTIME() {
		return _CMQTIME;
	}

	public void setCMQTIME(String cmqtime) {
		this._CMQTIME = cmqtime;
	}

	public String getSTAN() {
		return _STAN;
	}

	public void setSTAN(String stan) {
		this._STAN = stan;
	}

	public String getPRIZETIMES() {
		return _PRIZETIMES;
	}

	public void setPRIZETIMES(String prizetimes) {
		this._PRIZETIMES = prizetimes;
	}

	public String getOUTACN() {
		return _OUTACN;
	}

	public void setOUTACN(String outacn) {
		this._OUTACN = outacn;
	}

	public String getINTSACN() {
		return _INTSACN;
	}

	public void setINTSACN(String intsacn) {
		this._INTSACN = intsacn;
	}

	public String getAMOUNT() {
		return _AMOUNT;
	}

	public void setAMOUNT(String amount) {
		this._AMOUNT = amount;
	}

	public String getFEE() {
		return _FEE;
	}

	public void setFEE(String fee) {
		this._FEE = fee;
	}

	public String getHEADER() {
		return _HEADER;
	}

	public void setHEADER(String header) {
		this._HEADER = header;
	}

	public String getINPCST() {
		return _INPCST;
	}

	public void setINPCST(String inpcst) {
		this._INPCST = inpcst;
	}

	public String getO_TOTBAL() {
		return _O_TOTBAL;
	}

	public void setO_TOTBAL(String ototbal) {
		this._O_TOTBAL = ototbal;
	}

	public String getO_AVLBAL() {
		return _O_AVLBAL;
	}

	public void setO_AVLBAL(String oavlbal) {
		this._O_AVLBAL = oavlbal;
	}

	public String getI_TOTBAL() {
		return _I_TOTBAL;
	}

	public void setI_TOTBAL(String itotbal) {
		this._I_TOTBAL = itotbal;
	}

	public String getI_AVLBAL() {
		return _I_AVLBAL;
	}

	public void setI_AVLBAL(String iavlbal) {
		this._I_AVLBAL = iavlbal;
	}

//	public String getFGTXDATE() {
//		return _FGTXDATE;
//	}
//
//	public void setFGTXDATE(String fgtxdate) {
//		this._FGTXDATE = fgtxdate;
//	}

//	public String getCMTRMEMO() {
//		return _CMTRMEMO;
//	}
//
//	public void setCMTRMEMO(String cmtrmemo) {
//		this._CMTRMEMO = cmtrmemo;
//	}

	public String getSCHDATE() {
		return _SCHDATE;
	}

	public void setSCHDATE(String schdate) {
		this._SCHDATE = schdate;
	}
		
}
