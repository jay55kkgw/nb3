package fstop.ws;

import java.io.Serializable;
import java.util.List;

public class WsCN33Out implements Serializable {
	private static final long serialVersionUID = -4599119983240089341L;
	private String msgCode;
	private String msgName;
	private String CUSIDN;
	private String CNT;
	private List<WsCN33Row> rows;
	

	public String getMsgCode() {
		return msgCode;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public String getMsgName() {
		return msgName;
	}

	public void setMsgName(String msgName) {
		this.msgName = msgName;
	}

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}

	public String getCNT() {
		return CNT;
	}

	public void setCNT(String cNT) {
		CNT = cNT;
	}

	public List<WsCN33Row> getRows() {
		return rows;
	}

	public void setRows(List<WsCN33Row> rows) {
		this.rows = rows;
	}

}