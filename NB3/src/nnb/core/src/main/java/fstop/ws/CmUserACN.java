package fstop.ws;

public class CmUserACN {
	
	private String _SessionId;
	
	private String _N950PASSWORD;
	
	private String _SVACN;
		
	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		_SessionId = sessionId;
	}

	public String getN950PASSWORD() {
		return _N950PASSWORD;
	}

	public void setN950PASSWORD(String n950password) {
		_N950PASSWORD = n950password;
	}

	public String getSVACN() {
		return _SVACN;
	}

	public void setSVACN(String svacn) {
		_SVACN = svacn;
	}
}
