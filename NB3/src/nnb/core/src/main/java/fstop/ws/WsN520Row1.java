package fstop.ws;

public class WsN520Row1 {

	private String _TXDATE;
	
	private String _MEMO;
	
	private String _FXPAYAMT;
	
	private String _FXRECAMT;
	
	private String _BALANCE;
	
	private String _DATA12;
	
	private String _TEXT;
	
	private String _TRNTIME;
	
	private String _CUID;

	public String getTXDATE() {
		return _TXDATE;
	}
	public void setTXDATE(String txdate) {
		this._TXDATE = txdate;
	}

	public String getMEMO() {
		return _MEMO;
	}
	public void setMEMO(String memo) {
		this._MEMO = memo;
	}

	public String getFXPAYAMT() {
		return _FXPAYAMT;
	}
	public void setFXPAYAMT(String fxpayamt) {
		this._FXPAYAMT = fxpayamt;
	}

	public String getFXRECAMT() {
		return _FXRECAMT;
	}
	public void setFXRECAMT(String fxrecamt) {
		this._FXRECAMT = fxrecamt;
	}

	public String getBALANCE() {
		return _BALANCE;
	}
	public void setBALANCE(String balance) {
		this._BALANCE = balance;
	}

	public String getDATA12() {
		return _DATA12;
	}
	public void setDATA12(String data12) {
		this._DATA12 = data12;
	}

	public String getTEXT() {
		return _TEXT;
	}
	public void setTEXT(String text) {
		this._TEXT = text;
	}
	
	public String getTRNTIME() {
		return _TRNTIME;
	}
	public void setTRNTIME(String trntime) {
		this._TRNTIME = trntime;
	}
	
	public String getCUID() {
		return _CUID;
	}
	public void setCUID(String cuid) {
		this._CUID = cuid;
	}
	
}
