package fstop.ws;

public interface WsN09302{
	
	public GoldACNAmt getGoldACNAmt(CmUserACN params);
	
	public WsN09302Out  action(WsN09302In params);
}