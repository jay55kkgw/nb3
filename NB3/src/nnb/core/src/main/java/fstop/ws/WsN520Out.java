package fstop.ws;

public class WsN520Out {

	private String _MsgCode;
	
	private String _MsgName;
	
	private WsN520Row1[] _Table1;

	public String getMsgCode() {
		return _MsgCode;
	}
	public void setMsgCode(String msgcode) {
		this._MsgCode = msgcode;
	}

	public String getMsgName() {
		return _MsgName;
	}
	public void setMsgName(String msgname) {
		this._MsgName = msgname;
	}

	public WsN520Row1[] getTable1() {
		return _Table1;
	}
	public void setTable1(WsN520Row1[] table1) {
		this._Table1 = table1;
	}

}
