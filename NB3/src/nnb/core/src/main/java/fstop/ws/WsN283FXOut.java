package fstop.ws;

public class WsN283FXOut {

	private String _MsgCode;

	private String _MsgName;

	private String _CMQTIME;

	private String _CMRECNUMFX;

	private WsN283FXRow[] _Table;
	
	public String getMsgCode() {
		return _MsgCode;
	}
	public void setMsgCode(String msgcode) {
		this._MsgCode = msgcode;
	}

	public String getMsgName() {
		return _MsgName;
	}
	public void setMsgName(String msgname) {
		this._MsgName = msgname;
	}

	public String getCMQTIME() {
		return _CMQTIME;
	}
	public void setCMQTIME(String cmqtime) {
		this._CMQTIME = cmqtime;
	}

	public String getCMRECNUMFX() {
		return _CMRECNUMFX;
	}
	public void setCMRECNUMFX(String cmrecnumfx) {
		this._CMRECNUMFX = cmrecnumfx;
	}

	public WsN283FXRow[] getTable() {
		return _Table;
	}
	public void setTable(WsN283FXRow[] table) {
		this._Table = table;
	}
}
