package fstop.ws;

public class WsN09002SS {

	private String _SessionId;

	private String _SVACN;

	private String _ACN;

	private String _TSFBAL_H;
	
	private String _GDBAL_H;
	
	private String _TRNGD;
	
	private String _SELLFLAG;
	
	private String _PINNEW;

	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		this._SessionId = sessionId;
	}

	public void setSVACN(String SVACN) {
		this._SVACN = SVACN;
	}

	public String getSVACN() {
		return _SVACN;
	}

	public String getACN() {
		return _ACN;
	}

	public void setACN(String ACN) {
		this._ACN = ACN;
	}

	public String getTSFBAL_H() {
		return _TSFBAL_H;
	}

	public void setTSFBAL_H(String TSFBAL_H) {
		this._TSFBAL_H = TSFBAL_H;
	}
	
	public String getGDBAL_H() {
		return _GDBAL_H;
	}

	public void setGDBAL_H(String GDBAL_H) {
		this._GDBAL_H = GDBAL_H;
	}
	
	public String getTRNGD() {
		return _TRNGD;
	}

	public void setTRNGD(String TRNGD) {
		this._TRNGD = TRNGD;
	}

	public String getSELLFLAG() {
		return _SELLFLAG;
	}

	public void setSELLFLAG(String SELLFLAG) {
		this._SELLFLAG = SELLFLAG;
	}
	
	public String getPINNEW() {
		return _PINNEW;
	}

	public void setPINNEW(String pinnew) {
		this._PINNEW = pinnew;
	}
}
