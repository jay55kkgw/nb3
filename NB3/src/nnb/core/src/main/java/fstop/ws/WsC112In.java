package fstop.ws;

public class WsC112In {

	private String _SessionId;     //身份識別
	private String _N950PASSWORD;  //交易密碼
	private String _TXTOKEN;       //防止重送代碼
	private String _CAPTCHA;       //圖形驗證碼
	private String _FGTXWAY;	   //交易機制
	private String _CMPASSWORD;	   //交易密碼
	private String _PINNEW;	   	   //交易密碼SHA1值
	private String _ALTERTYPE1;	   //是否變更金額項目
	private String _ALTERTYPE2;    //是否變更日期項目	
	private String _ALTERTYPE3;    //是否變更扣款狀態項目
	private String _PAYAMT;        //扣款金額	
	private String _PAYCUR;        //扣款幣別
	private String _PAYDAY4;       //扣款日期4
	private String _PAYDAY5;       //扣款日期5
	private String _PAYDAY6;       //扣款日期6 
	private String _PAYDAY1;       //扣款日期1
	private String _PAYDAY2;       //扣款日期2
	private String _PAYDAY3;       //扣款日期3
	private String _PAYDAY7;       //扣款日期7
	private String _PAYDAY8;       //扣款日期8
	private String _PAYDAY9;       //扣款日期9
	private String _ALTERTYPE;     //扣款狀態
	private String _CREDITNO;      //信託帳號	
	private String _PAYTAG;        //扣款標的代號
	private String _FUNDLNAME;     //扣款標的名稱	
	private String _MIP;           //類別

	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionid) {
		this._SessionId = sessionid;
	}

	public String getN950PASSWORD() {
		return _N950PASSWORD;
	}

	public void setN950PASSWORD(String n950password) {
		this._N950PASSWORD = n950password;
	}

	public String getTXTOKEN() {
		return _TXTOKEN;
	}

	public void setTXTOKEN(String txtoken) {
		this._TXTOKEN = txtoken;
	}

	public String getCAPTCHA() {
		return _CAPTCHA;
	}

	public void setCAPTCHA(String CAPTCHA) {
		this._CAPTCHA = CAPTCHA;
	}
	
	public String getFGTXWAY() {
		return _FGTXWAY;
	}

	public void setFGTXWAY(String fgtxway) {
		this._FGTXWAY = fgtxway;
	}

	public String getCMPASSWORD() {
		return _CMPASSWORD;
	}

	public void setCMPASSWORD(String CMPASSWORD) {
		this._CMPASSWORD = CMPASSWORD;
	}
	
	public String getPINNEW() {
		return _PINNEW;
	}

	public void setPINNEW(String PINNEW) {
		this._PINNEW = PINNEW;
	}
	
	public String getALTERTYPE1() {
		return _ALTERTYPE1;
	}

	public void setALTERTYPE1(String ALTERTYPE1) {
		this._ALTERTYPE1 = ALTERTYPE1;
	}

	public String getALTERTYPE2() {
		return _ALTERTYPE2;
	}

	public void setALTERTYPE2(String ALTERTYPE2) {
		this._ALTERTYPE2 = ALTERTYPE2;
	}

	public String getALTERTYPE3() {
		return _ALTERTYPE3;
	}

	public void setALTERTYPE3(String ALTERTYPE3) {
		this._ALTERTYPE3 = ALTERTYPE3;
	}
	
	public String getPAYAMT() {
		return _PAYAMT;
	}

	public void setPAYAMT(String PAYAMT) {
		this._PAYAMT = PAYAMT;
	}

	public String getPAYCUR() {
		return _PAYCUR;
	}

	public void setPAYCUR(String PAYCUR) {
		this._PAYCUR = PAYCUR;
	}
	
	public String getPAYDAY4() {
		return _PAYDAY4;
	}

	public void setPAYDAY4(String PAYDAY4) {
		this._PAYDAY4 = PAYDAY4;
	}

	public String getPAYDAY5() {
		return _PAYDAY5;
	}

	public void setPAYDAY5(String PAYDAY5) {
		this._PAYDAY5 = PAYDAY5;
	}

	public String getPAYDAY6() {
		return _PAYDAY6;
	}

	public void setPAYDAY6(String PAYDAY6) {
		this._PAYDAY6 = PAYDAY6;
	}

	public String getPAYDAY1() {
		return _PAYDAY1;
	}

	public void setPAYDAY1(String PAYDAY1) {
		this._PAYDAY1 = PAYDAY1;
	}

	public String getPAYDAY2() {
		return _PAYDAY2;
	}

	public void setPAYDAY2(String PAYDAY2) {
		this._PAYDAY2 = PAYDAY2;
	}

	public String getPAYDAY3() {
		return _PAYDAY3;
	}

	public void setPAYDAY3(String PAYDAY3) {
		this._PAYDAY3 = PAYDAY3;
	}

	public String getPAYDAY7() {
		return _PAYDAY7;
	}

	public void setPAYDAY7(String PAYDAY7) {
		this._PAYDAY7 = PAYDAY7;
	}
	
	public String getPAYDAY8() {
		return _PAYDAY8;
	}

	public void setPAYDAY8(String PAYDAY8) {
		this._PAYDAY8 = PAYDAY8;
	}
	
	public String getPAYDAY9() {
		return _PAYDAY9;
	}

	public void setPAYDAY9(String PAYDAY9) {
		this._PAYDAY9 = PAYDAY9;
	}
	
	public String getALTERTYPE() {
		return _ALTERTYPE;
	}

	public void setALTERTYPE(String ALTERTYPE) {
		this._ALTERTYPE= ALTERTYPE;
	}
	
	public String getCREDITNO() {
		return _CREDITNO;
	}

	public void setCREDITNO(String CREDITNO) {
		this._CREDITNO = CREDITNO;
	}

	public String getPAYTAG() {
		return _PAYTAG;
	}

	public void setPAYTAG(String PAYTAG) {
		this._PAYTAG = PAYTAG;
	}
	
	public String getFUNDLNAME() {
		return _FUNDLNAME;
	}

	public void setFUNDLNAME(String FUNDLNAME) {
		this._FUNDLNAME = FUNDLNAME;
	}

	public String getMIP() {
		return _MIP;
	}

	public void setMIP(String MIP) {
		this._MIP = MIP;
	}
}
