package fstop.ws;

public class WsN510Row3 {

	private String _BALSUBCUR;
	
	private String _BALSUB;
	
	private String _BALSUBAMTRECNUM;

	public String getBALSUBCUR() {
		return _BALSUBCUR;
	}

	public void setBALSUBCUR(String balsubcur) {
		this._BALSUBCUR = balsubcur;
	}

	public String getBALSUB() {
		return _BALSUB;
	}

	public void setBALSUB(String balsub) {
		this._BALSUB = balsub;
	}
	
	public String getBALSUBAMTRECNUM() {
		return _BALSUBAMTRECNUM;
	}

	public void setBALSUBAMTRECNUM(String balsubamtrecnum) {
		this._BALSUBAMTRECNUM = balsubamtrecnum;
	}
	
}
