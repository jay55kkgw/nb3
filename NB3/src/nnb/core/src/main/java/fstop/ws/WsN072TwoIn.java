package fstop.ws;

import java.util.Locale;

public class WsN072TwoIn {

	private String _SessionId;// 身份識別

	private String _TXTOKEN;// 防止重送代碼

	private String _FGTXWAY;// 密碼類別

	private String _CMPWD;// 交易密碼

	private String _PINNEW;// 交易密碼SHA1值

	private String _OTPKEY;// OTP動態密碼

	private String _FLAG;// 約定否

	private String _ACN;// 轉出帳號

	private String _CARDNUM1;// 轉入帳號

	private String _CARDNUM2;// 轉入帳號

	private String _AMOUNT;// 轉帳金額

	private String _CMTRMEMO;// 交易備註

	private String _CMTRMAIL;// Email信箱

	private String _CMMAILMEMO;// Email摘要內容

	private String _CAPTCHA;// 圖形驗證碼
	
	//二階增加
	private String _FGTXDATE;// 轉帳日期類型

	private String _CMTRDATE;// 預約日期
	

	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		this._SessionId = sessionId;
	}

	public void setTXTOKEN(String TXTOKEN) {
		this._TXTOKEN = TXTOKEN;
	}

	public String getTXTOKEN() {
		return _TXTOKEN;
	}

	public String getACN() {
		return _ACN;
	}

	public void setACN(String ACN) {
		this._ACN = ACN;
	}

	public String getCARDNUM1() {
		return _CARDNUM1.toUpperCase(Locale.ENGLISH);
	}

	public void setCARDNUM1(String CARDNUM1) {
		this._CARDNUM1 = CARDNUM1;
	}

	public String getCARDNUM2() {
		return _CARDNUM2.toUpperCase(Locale.ENGLISH);
	}

	public void setCARDNUM2(String CARDNUM2) {
		this._CARDNUM2 = CARDNUM2;
	}

	public String getFGTXWAY() {
		return _FGTXWAY;
	}

	public void setFGTXWAY(String fgtxway) {
		this._FGTXWAY = fgtxway;
	}

	public String getCMPWD() {
		return _CMPWD;
	}

	public void setCMPWD(String CMPWD) {
		this._CMPWD = CMPWD;
	}

	public String getPINNEW() {
		return _PINNEW;
	}

	public void setPINNEW(String pinnew) {
		this._PINNEW = pinnew;
	}

	public String getOTPKEY() {
		return _OTPKEY;
	}

	public void setOTPKEY(String otpkey) {
		this._OTPKEY = otpkey;
	}

	public String getFLAG() {
		return _FLAG;
	}

	public void setFLAG(String FLAG) {
		this._FLAG = FLAG;
	}

	public String getCMTRMEMO() {
		return _CMTRMEMO;
	}

	public void setCMTRMEMO(String cmtrmemo) {
		this._CMTRMEMO = cmtrmemo;
	}

	public String getCMTRMAIL() {
		return _CMTRMAIL;
	}

	public void setCMTRMAIL(String cmtrmail) {
		this._CMTRMAIL = cmtrmail;
	}

	public String getCMMAILMEMO() {
		return _CMMAILMEMO;
	}

	public void setCMMAILMEMO(String cmmailmemo) {
		this._CMMAILMEMO = cmmailmemo;
	}

	public String getAMOUNT() {
		return _AMOUNT;
	}

	public void setAMOUNT(String amount) {
		this._AMOUNT = amount;
	}

	public String getCAPTCHA() {
		return _CAPTCHA;
	}

	public void setCAPTCHA(String captcha) {
		this._CAPTCHA = captcha;
	}

	public String getFGTXDATE() {
		return _FGTXDATE;
	}
	public void setFGTXDATE(String fgtxdate) {
		this._FGTXDATE = fgtxdate;
	}

	public String getCMTRDATE() {
		return _CMTRDATE;
	}
	public void setCMTRDATE(String cmtrdate) {
		this._CMTRDATE = cmtrdate;
	}

}
