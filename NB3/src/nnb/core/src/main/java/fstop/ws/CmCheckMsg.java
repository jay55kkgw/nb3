package fstop.ws;

public class CmCheckMsg {
	
	private String _MsgCode;
	
	private String _MsgName;
	
	private String _CheckResult;
	
	private String _ALERTTYPE;
	
	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		_MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		_MsgName = msgName;
	}

	public String getCheckResult() {
		return _CheckResult;
	}

	public void setCheckResult(String checkResult) {
		_CheckResult = checkResult;
	}
	
	public String getALERTTYPE() {
		return _ALERTTYPE;
	}

	public void setALERTTYPE(String ALERTTYPE) {
		_ALERTTYPE = ALERTTYPE;
	}
}

