package fstop.ws;

public class WsN930eaOut {

	private String _MsgCode;
	
	private String _MsgName;
	
	private WsN930eaRow[] _Table;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public WsN930eaRow[] getTable() {
		return _Table;
	}

	public void setTable(WsN930eaRow[] table) {
		this._Table = table;
	}
	
	
}
