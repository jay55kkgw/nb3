package fstop.ws;

public class WsN094Out {

	private String _MsgCode;

	private String _MsgName;

	private String _CMTXTIME;

	private String _SVACN;

	private String _ACN;

	private String _O_TOTBAL;

	private String _O_AVLBAL;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getCMTXTIME() {
		return _CMTXTIME;
	}

	public void setCMTXTIME(String CMTXTIME) {
		this._CMTXTIME = CMTXTIME;
	}

	public String getSVACN() {
		return _SVACN;
	}

	public void setSVACN(String SVACN) {
		this._SVACN = SVACN;
	}

	public String getACN() {
		return _ACN;
	}

	public void setACN(String ACN) {
		this._ACN = ACN;
	}

	public String getO_TOTBAL() {
		return _O_TOTBAL;
	}

	public void setO_TOTBAL(String O_TOTBAL) {
		this._O_TOTBAL = O_TOTBAL;
	}

	public String getO_AVLBAL() {
		return _O_AVLBAL;
	}

	public void setO_AVLBAL(String O_AVLBAL) {
		this._O_AVLBAL = O_AVLBAL;
	}

}
