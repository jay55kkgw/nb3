package fstop.ws;

public class CmEditRemitIn {
	
	private String _SessionId;

	private String _UID;

	private String _TYPE;

	private String _ADRMTID;

	private String _ADRMTTYPE;
	
	public String getSessionId() {
		return _SessionId;
	}
	public void setSessionId(String sessionid) {
		this._SessionId = sessionid;
	}

	public String getUID() {
		return _UID;
	}
	public void setUID(String uid) {
		this._UID = uid;
	}

	public String getTYPE() {
		return _TYPE;
	}
	public void setTYPE(String type) {
		this._TYPE = type;
	}

	public String getADRMTID() {
		return _ADRMTID;
	}
	public void setADRMTID(String adrmtid) {
		this._ADRMTID = adrmtid;
	}

	public String getADRMTTYPE() {
		return _ADRMTTYPE;
	}
	public void setADRMTTYPE(String adrmttype) {
		this._ADRMTTYPE = adrmttype;
	}
}
