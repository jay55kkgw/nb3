package fstop.ws;

public class WsN816LOut {

	// 訊息代碼
	private String _MsgCode;

	// 訊息名稱
	private String _MsgName;

	// 系統時間
	private String _SYSTIME;

	// 信用卡別
	private String _TYPENAME;

	// 信用卡號
	private String _CARDNUM;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getSYSTIME() {
		return _SYSTIME;
	}

	public void setSYSTIME(String systime) {
		this._SYSTIME = systime;
	}

	public String getTYPENAME() {
		return _TYPENAME;
	}

	public void setTYPENAME(String typename) {
		_TYPENAME = typename;
	}

	public String getCARDNUM() {
		return _CARDNUM;
	}

	public void setCARDNUM(String cardnum) {
		_CARDNUM = cardnum;
	}

}
