package fstop.ws;

public class WsC115Row {

	private String _CONDAY;		//申請日期
	private String _PAYTAG;		//扣款標的代號	
	private String _FUNDLNAME;	//扣款標的名稱
	private String _MIP;		//類別
	private String _PAYCUR;		//每次定額申購金額＋不定額基準申購金額幣別
	private String _OPAYAMT;	//每次定額申購金額＋不定額基準申購金額
	private String _DBTTYPE;	//扣款方式
	private String _PAYACNO;	//扣款帳號/卡號
	private String _PAYDAY;		//扣款日
	private String _OPAYDAY1;	//原扣款日1
	private String _OPAYDAY2;	//原扣款日2
	private String _OPAYDAY3;	//原扣款日3
	private String _OPAYDAY4;	//原扣款日4
	private String _OPAYDAY5;	//原扣款日5
	private String _OPAYDAY6;	//原扣款日6
	private String _OPAYDAY7;	//原扣款日7
	private String _OPAYDAY8;	//原扣款日8
	private String _OPAYDAY9;	//原扣款日9
	private String _FSTDAY;		//首次申購日
	private String _STOPBEGDAY;	//暫停扣款起日
	private String _STOPENDDAY;	//暫停扣款迄日
	private String _STPDAY;		//終止扣款日
	private String _REMARK1;	//扣款約定
	private String _REMARK2;	//憑證
	private String _CREDITNO;	//信託帳號	

	public String getCONDAY() {
		return _CONDAY;
	}

	public void setCONDAY(String CONDAY) {
		this._CONDAY = CONDAY;
	}

	public String getPAYTAG() {
		return _PAYTAG;
	}

	public void setPAYTAG(String PAYTAG) {
		this._PAYTAG = PAYTAG;
	}

	public String getFUNDLNAME() {
		return _FUNDLNAME;
	}

	public void setFUNDLNAME(String FUNDLNAME) {
		this._FUNDLNAME = FUNDLNAME;
	}

	public String getMIP() {
		return _MIP;
	}

	public void setMIP(String MIP) {
		this._MIP = MIP;
	}

	public String getPAYCUR() {
		return _PAYCUR;
	}

	public void setPAYCUR(String PAYCUR) {
		this._PAYCUR = PAYCUR;
	}

	public String getOPAYAMT() {
		return _OPAYAMT;
	}

	public void setOPAYAMT(String OPAYAMT) {
		this._OPAYAMT = OPAYAMT;
	}

	public String getDBTTYPE() {
		return _DBTTYPE;
	}

	public void setDBTTYPE(String DBTTYPE) {
		this._DBTTYPE = DBTTYPE;
	}

	public String getPAYACNO() {
		return _PAYACNO;
	}

	public void setPAYACNO(String PAYACNO) {
		this._PAYACNO = PAYACNO;
	}

	public String getPAYDAY() {
		return _PAYDAY;
	}

	public void setPAYDAY(String PAYDAY) {
		this._PAYDAY = PAYDAY;
	}

	public String getOPAYDAY1() {
		return _OPAYDAY1;
	}

	public void setOPAYDAY1(String OPAYDAY1) {
		this._OPAYDAY1 = OPAYDAY1;
	}
	
	public String getOPAYDAY2() {
		return _OPAYDAY2;
	}

	public void setOPAYDAY2(String OPAYDAY2) {
		this._OPAYDAY2 = OPAYDAY2;
	}
	
	public String getOPAYDAY3() {
		return _OPAYDAY3;
	}

	public void setOPAYDAY3(String OPAYDAY3) {
		this._OPAYDAY3 = OPAYDAY3;
	}
	
	public String getOPAYDAY4() {
		return _OPAYDAY4;
	}

	public void setOPAYDAY4(String OPAYDAY4) {
		this._OPAYDAY4 = OPAYDAY4;
	}
	
	public String getOPAYDAY5() {
		return _OPAYDAY5;
	}

	public void setOPAYDAY5(String OPAYDAY5) {
		this._OPAYDAY5 = OPAYDAY5;
	}
	
	public String getOPAYDAY6() {
		return _OPAYDAY6;
	}

	public void setOPAYDAY6(String OPAYDAY6) {
		this._OPAYDAY6 = OPAYDAY6;
	}
	
	public String getOPAYDAY7() {
		return _OPAYDAY7;
	}

	public void setOPAYDAY7(String OPAYDAY7) {
		this._OPAYDAY7 = OPAYDAY7;
	}
	
	public String getOPAYDAY8() {
		return _OPAYDAY8;
	}

	public void setOPAYDAY8(String OPAYDAY8) {
		this._OPAYDAY8 = OPAYDAY8;
	}
	
	public String getOPAYDAY9() {
		return _OPAYDAY9;
	}

	public void setOPAYDAY9(String OPAYDAY9) {
		this._OPAYDAY9 = OPAYDAY9;
	}
	
	public String getFSTDAY() {
		return _FSTDAY;
	}

	public void setFSTDAY(String FSTDAY) {
		this._FSTDAY = FSTDAY;
	}

	public String getSTOPBEGDAY() {
		return _STOPBEGDAY;
	}

	public void setSTOPBEGDAY(String STOPBEGDAY) {
		this._STOPBEGDAY = STOPBEGDAY;
	}

	public String getSTOPENDDAY() {
		return _STOPENDDAY;
	}

	public void setSTOPENDDAY(String STOPENDDAY) {
		this._STOPENDDAY = STOPENDDAY;
	}

	public String getSTPDAY() {
		return _STPDAY;
	}

	public void setSTPDAY(String STPDAY) {
		this._STPDAY = STPDAY;
	}

	public String getREMARK1() {
		return _REMARK1;
	}

	public void setREMARK1(String REMARK1) {
		this._REMARK1 = REMARK1;
	}

	public String getREMARK2() {
		return _REMARK2;
	}

	public void setREMARK2(String REMARK2) {
		this._REMARK2 = REMARK2;
	}

	public String getCREDITNO() {
		return _CREDITNO;
	}

	public void setCREDITNO(String CREDITNO) {
		this._CREDITNO = CREDITNO;
	}
	
}
