package fstop.ws;

public interface WsN079{
	
	public CmcancelList queryCancelList(CmUser params);
	
	public cancelDataOut getCancelData(cancelDataIn params);
	
	public WsN079Out action(WsN079In params);
}