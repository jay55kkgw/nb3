package fstop.ws;

public class WsC013TXRow {
	
	private String _TRADEDATE;			  //交易日期
	private String _CREDITNO;   		  //信託號碼
	private String _TRANSCODE;			  //基金代碼
	private String _FUNDLNAME;			  //基金名稱
	private String _TR106;				  //投資方式
	private String _FUNDTYPE;			  //交易類別
	private String _ADCCYNAME;			  //信託/除息金額幣別
	private String _FUNDAMT;			  //信託/除息金額
	private String _UNIT;				  //單位數
	private String _PRICE1;				  //淨值
	private String _EXRATE;				  //匯率			
	private String _INTRANSCODE; 		  //轉入基金代碼
	private String _INTRANSCODEFUNDLNAME; //轉入基金名稱
	private String _TXUNIT;				  //轉入單位數	
	private String _PRICE2;				  //轉入價格
	private String _INTRANSCRYADCCYNAME;  //轉入價格(幣別)
		
	public String getTRADEDATE() {
		return _TRADEDATE;
	}

	public void setTRADEDATE(String TRADEDATE) {
		this._TRADEDATE = TRADEDATE;
	}

	public String getCREDITNO() {
		return _CREDITNO;
	}

	public void setCREDITNO(String CREDITNO) {
		this._CREDITNO = CREDITNO;
	}
	
	public String getTRANSCODE() {
		return _TRANSCODE;
	}

	public void setTRANSCODE(String TRANSCODE) {
		this._TRANSCODE = TRANSCODE;
	}
	
	public String getFUNDLNAME() {
		return _FUNDLNAME;
	}

	public void setFUNDLNAME(String FUNDLNAME) {
		this._FUNDLNAME = FUNDLNAME;
	}
		
	public String getTR106() {
		return _TR106;
	}

	public void setTR106(String TR106) {
		this._TR106 = TR106;
	}

	public String getFUNDTYPE() {
		return _FUNDTYPE;
	}

	public void setFUNDTYPE(String FUNDTYPE) {
		this._FUNDTYPE = FUNDTYPE;
	}

	public String getADCCYNAME() {
		return _ADCCYNAME;
	}

	public void setADCCYNAME(String ADCCYNAME) {
		this._ADCCYNAME = ADCCYNAME;
	}

	public String getFUNDAMT() {
		return _FUNDAMT;
	}

	public void setFUNDAMT(String FUNDAMT) {
		this._FUNDAMT = FUNDAMT;
	}

	public String getUNIT() {
		return _UNIT;
	}

	public void setUNIT(String UNIT) {
		this._UNIT = UNIT;
	}

	public String getPRICE1() {
		return _PRICE1;
	}

	public void setPRICE1(String PRICE1) {
		this._PRICE1 = PRICE1;
	}

	public String getEXRATE() {
		return _EXRATE;
	}

	public void setEXRATE(String EXRATE) {
		this._EXRATE = EXRATE;
	}

	public String getINTRANSCODE() {
		return _INTRANSCODE;
	}

	public void setINTRANSCODE(String INTRANSCODE) {
		this._INTRANSCODE = INTRANSCODE;
	}
	
	public String getINTRANSCODEFUNDLNAME() {
		return _INTRANSCODEFUNDLNAME;
	}

	public void setINTRANSCODEFUNDLNAME(String INTRANSCODEFUNDLNAME) {
		this._INTRANSCODEFUNDLNAME = INTRANSCODEFUNDLNAME;
	}

	public String getTXUNIT() {
		return _TXUNIT;
	}

	public void setTXUNIT(String TXUNIT) {
		this._TXUNIT = TXUNIT;
	}

	public String getPRICE2() {
		return _PRICE2;
	}

	public void setPRICE2(String price2) {
		this._PRICE2 = price2;
	}
	
	public String getINTRANSCRYADCCYNAME() {
		return _INTRANSCRYADCCYNAME;
	}

	public void setINTRANSCRYADCCYNAME(String INTRANSCRYADCCYNAME) {
		this._INTRANSCRYADCCYNAME = INTRANSCRYADCCYNAME;
	}

}
