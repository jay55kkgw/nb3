package fstop.ws;

public class WsN177In {

	private String SessionId;//	身份識別
	private String TXTOKEN;//	防止重送代碼
	private String FGTXWAY;//	密碼類別
	private String PINNEW;//	交易密碼SHA1值
	private String OTPKEY;//	OTP動態密碼
	private String CAPTCHA;//	圖形驗證碼
	private String FYACN;//	存單帳號
	private String FDPNUM;//	存單號碼
	private String AMTFDP;//	存單金額
	private String CRY;//	幣別
	private String FGAUTXFTM;//	轉期次數註記
	private String CODE;//	轉存方式
	private String FYTSFAN;//	轉入帳號
	private String DEPTYPE;//	存款種類
	private String INTMTH;//	計息方式
	private String AUTXFTM;//	轉期次數
	private String CMPASSWORD;
	
	//no CMPASSWORD && PINNEW
	@Override
	public String toString()
	{
		return "WsN177In [SessionId=" + SessionId + ", TXTOKEN=" + TXTOKEN + ", FGTXWAY=" + FGTXWAY + ", OTPKEY="
				+ OTPKEY + ", CAPTCHA=" + CAPTCHA + ", FYACN=" + FYACN + ", FDPNUM=" + FDPNUM + ", AMTFDP=" + AMTFDP
				+ ", CRY=" + CRY + ", FGAUTXFTM=" + FGAUTXFTM + ", CODE=" + CODE + ", FYTSFAN=" + FYTSFAN + ", DEPTYPE="
				+ DEPTYPE + ", INTMTH=" + INTMTH + ", AUTXFTM=" + AUTXFTM + "]";
	}
	public String getSessionId() {
		return SessionId;
	}
	public void setSessionId(String sessionId) {
		SessionId = sessionId;
	}
	public String getTXTOKEN() {
		return TXTOKEN;
	}
	public void setTXTOKEN(String tXTOKEN) {
		TXTOKEN = tXTOKEN;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getPINNEW() {
		return PINNEW;
	}
	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}
	public String getOTPKEY() {
		return OTPKEY;
	}
	public void setOTPKEY(String oTPKEY) {
		OTPKEY = oTPKEY;
	}
	public String getCAPTCHA() {
		return CAPTCHA;
	}
	public void setCAPTCHA(String cAPTCHA) {
		CAPTCHA = cAPTCHA;
	}
	public String getFYACN() {
		return FYACN;
	}
	public void setFYACN(String fYACN) {
		FYACN = fYACN;
	}
	public String getFDPNUM() {
		return FDPNUM;
	}
	public void setFDPNUM(String fDPNUM) {
		FDPNUM = fDPNUM;
	}
	public String getAMTFDP() {
		return AMTFDP;
	}
	public void setAMTFDP(String aMTFDP) {
		AMTFDP = aMTFDP;
	}
	public String getCRY() {
		return CRY;
	}
	public void setCRY(String cRY) {
		CRY = cRY;
	}
	public String getFGAUTXFTM() {
		return FGAUTXFTM;
	}
	public void setFGAUTXFTM(String fGAUTXFTM) {
		FGAUTXFTM = fGAUTXFTM;
	}
	public String getCODE() {
		return CODE;
	}
	public void setCODE(String cODE) {
		CODE = cODE;
	}
	public String getFYTSFAN() {
		return FYTSFAN;
	}
	public void setFYTSFAN(String fYTSFAN) {
		FYTSFAN = fYTSFAN;
	}
	public String getDEPTYPE() {
		return DEPTYPE;
	}
	public void setDEPTYPE(String dEPTYPE) {
		DEPTYPE = dEPTYPE;
	}
	public String getINTMTH() {
		return INTMTH;
	}
	public void setINTMTH(String iNTMTH) {
		INTMTH = iNTMTH;
	}
	public String getAUTXFTM() {
		return AUTXFTM;
	}
	public void setAUTXFTM(String aUTXFTM) {
		AUTXFTM = aUTXFTM;
	}
	public String getCMPASSWORD() {
		return CMPASSWORD;
	}
	public void setCMPASSWORD(String cmpassword) {
		CMPASSWORD = cmpassword;
	}

	
}
