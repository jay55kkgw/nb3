package fstop.ws;

public interface WsN926{

	public CmList getSVAcnoList(CmUser params);
	
	public CmList getGDAcnoListBySVACN(CmUserACN params);
	
	public CmList getGoldAcnoList(CmUser params);
}