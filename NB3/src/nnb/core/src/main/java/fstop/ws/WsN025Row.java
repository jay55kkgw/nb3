package fstop.ws;

public class WsN025Row {

	private String _COLOR;

	private String _ACC;

	private String _ITR1;

	private String _ITR2;

	private String _MARK;

	public String getCOLOR() {
		return _COLOR;
	}

	public void setCOLOR(String color) {
		this._COLOR = color;
	}

	public String getACC() {
		return _ACC;
	}

	public void setACC(String acc) {
		this._ACC = acc;
	}

	public String getITR1() {
		return _ITR1;
	}

	public void setITR1(String itr1) {
		this._ITR1 = itr1;
	}

	public String getITR2() {
		return _ITR2;
	}

	public void setITR2(String itr2) {
		this._ITR2 = itr2;
	}

	public String getMARK() {
		return _MARK;
	}

	public void setMARK(String MARK) {
		this._MARK = MARK;
	}

}
