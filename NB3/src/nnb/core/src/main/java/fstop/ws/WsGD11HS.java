package fstop.ws;

public interface WsGD11HS{
	
	public WsGD11HSOut action(WsGD11HSIn params);
	
	public WsGD11LDOut actionLD(WsGD11LDIn params);
}