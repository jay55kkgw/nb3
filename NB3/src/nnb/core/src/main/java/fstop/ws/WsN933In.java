package fstop.ws;

public class WsN933In {
	
	private String _SessionId;
	
	private String _N950PASSWORD;
	
	private String _TYPE;
	
	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		_SessionId = sessionId;
	}

	public String getN950PASSWORD() {
		return _N950PASSWORD;
	}

	public void setN950PASSWORD(String n950password) {
		_N950PASSWORD = n950password;
	}

	public String getTYPE() {
		return _TYPE;
	}

	public void setTYPE(String type) {
		_TYPE = type;
	}
	
}
