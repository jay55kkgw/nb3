package fstop.ws;

public class WsN820Out {
	
	private String _MsgCode;
	
	private String _MsgName;
	// 本期應繳金額
	private String _R12C1;
	// 最低應繳金額
	private String _R12C2;
	// 結帳日
	private String _R12C3;
	// 繳款期限
	private String _R12C4;
	/**
	 * WsN820Row[]
	 * @param RDETAIL20R1 卡別
	 * @param RDETAIL20R2C1 消費日期
	 * @param RDETAIL20R2C3 消費說明
	 * @param RDETAIL20R2C10 臺幣金額
	 * @param RDETAIL20R2C9 外幣金額
	 * @param RDETAIL20R2C7 幣別
	 * @param RDETAIL20R2C7NAME 幣別名稱
	 * @return
	 */
	private WsN820Row[] _Table;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		_MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		_MsgName = msgName;
	}

	public String getR12C1() {
		return _R12C1;
	}

	public void setR12C1(String R12C1) {
		_R12C1 = R12C1;
	}
	
	public String getR12C2() {
		return _R12C2;
	}

	public void setR12C2(String R12C2) {
		_R12C2 = R12C2;
	}
	
	public String getR12C3() {
		return _R12C3;
	}

	public void setR12C3(String R12C3) {
		_R12C3 = R12C3;
	}
	
	public String getR12C4() {
		return _R12C4;
	}

	public void setR12C4(String R12C4) {
		_R12C4 = R12C4;
	}
	
	public WsN820Row[] getTable() {
		return _Table;
	}

	public void setTable(WsN820Row[] table) {
		_Table = table;
	}
	
}
