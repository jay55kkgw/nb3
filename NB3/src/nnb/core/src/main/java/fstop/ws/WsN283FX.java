package fstop.ws;

public interface WsN283FX{
	
	public WsN283FXOut action(CmUser params);

	public WsN283CancelOut cancel(WsN283CancelIn params);
}