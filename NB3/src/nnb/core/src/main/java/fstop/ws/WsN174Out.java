package fstop.ws;

public class WsN174Out {

	private String MsgCode;//	訊息代碼
	private String MsgName;//	訊息名稱
	private String CMQTIME;//	交易時間
	private String FYTSFAN;//	轉出帳號
	private String TSFACN;//	轉入帳號
	private String FDPNUM;//	存單號碼
	private String OUTCRY;//	轉帳幣別
	private String AMOUNT;//	轉帳金額
	private String TYPCOD;//	存款期別
	private String SDT;//		起存日
	private String DUEDAT;//	到期日
	private String INTMTH;//	計息方式
	private String ITR;//		利率
	private String CODE;//		轉存方式
	private String AUTXFTM;//	到期轉存
	private String CMTRMEMO;//	交易備註
	private String TOTBAL;//	轉帳帳號帳戶餘額
	private String AVLBAL;//	轉帳帳號可用餘額
	private String TRDATE;//	轉帳日期
	public String getMsgCode() {
		return MsgCode;
	}
	public void setMsgCode(String msgCode) {
		MsgCode = msgCode;
	}
	public String getMsgName() {
		return MsgName;
	}
	public void setMsgName(String msgName) {
		MsgName = msgName;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getFYTSFAN() {
		return FYTSFAN;
	}
	public void setFYTSFAN(String fYTSFAN) {
		FYTSFAN = fYTSFAN;
	}
	public String getTSFACN() {
		return TSFACN;
	}
	public void setTSFACN(String tSFACN) {
		TSFACN = tSFACN;
	}
	public String getFDPNUM() {
		return FDPNUM;
	}
	public void setFDPNUM(String fDPNUM) {
		FDPNUM = fDPNUM;
	}
	public String getOUTCRY() {
		return OUTCRY;
	}
	public void setOUTCRY(String oUTCRY) {
		OUTCRY = oUTCRY;
	}
	public String getAMOUNT() {
		return AMOUNT;
	}
	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}
	public String getTYPCOD() {
		return TYPCOD;
	}
	public void setTYPCOD(String tYPCOD) {
		TYPCOD = tYPCOD;
	}
	public String getSDT() {
		return SDT;
	}
	public void setSDT(String sDT) {
		SDT = sDT;
	}
	public String getDUEDAT() {
		return DUEDAT;
	}
	public void setDUEDAT(String dUEDAT) {
		DUEDAT = dUEDAT;
	}
	public String getINTMTH() {
		return INTMTH;
	}
	public void setINTMTH(String iNTMTH) {
		INTMTH = iNTMTH;
	}
	public String getITR() {
		return ITR;
	}
	public void setITR(String iTR) {
		ITR = iTR;
	}
	public String getCODE() {
		return CODE;
	}
	public void setCODE(String cODE) {
		CODE = cODE;
	}
	public String getAUTXFTM() {
		return AUTXFTM;
	}
	public void setAUTXFTM(String aUTXFTM) {
		AUTXFTM = aUTXFTM;
	}
	public String getCMTRMEMO() {
		return CMTRMEMO;
	}
	public void setCMTRMEMO(String cMTRMEMO) {
		CMTRMEMO = cMTRMEMO;
	}
	public String getTOTBAL() {
		return TOTBAL;
	}
	public void setTOTBAL(String tOTBAL) {
		TOTBAL = tOTBAL;
	}
	public String getAVLBAL() {
		return AVLBAL;
	}
	public void setAVLBAL(String aVLBAL) {
		AVLBAL = aVLBAL;
	}
	public String getTRDATE() {
		return TRDATE;
	}
	public void setTRDATE(String trdate) {
		TRDATE = trdate;
	}

	
}
