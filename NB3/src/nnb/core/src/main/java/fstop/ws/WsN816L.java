package fstop.ws;

public interface WsN816L{

	public WsN816LCardList getCardList(CmUser params); 
	
	public WsN816LOut action(WsN816LIn params);
}