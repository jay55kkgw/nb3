package fstop.ws;

public interface WsN070A{
	
	public WsN070AOut action(WsN070AIn params);
	
	public WsN070AOut actionCert(WsN070ATwoIn params);
}