package fstop.ws;

public class WsN750BIn {

	private String _SessionId;//	身份識別
	
	private String _TXTOKEN	;//防止重送代碼
	
	private String _OTPKEY	;//OTP動態密碼
	
	private String _OUTACN;//	轉出帳號
	
	private String _BARCODE1;//	條碼一
	
	private String _BARCODE2;//	條碼二
	
	private String _BARCODE3;//	條碼三
	
	private String _WAT_NO	;//水號
	
	private String _AMOUNT	;//應繳總金額
	
	private String _CMTRMEMO;//	交易備註
	
	private String _CMTRMAIL;//	Email信箱
	
	private String _CMMAILMEMO;//	Email摘要內容
	
	private String _CAPTCHA	;//圖形驗證碼
	
	private String _FGTXDATE;//	轉帳日期類型
	
	private String _CMDATE	;//預約日期

	public String getSessionId() {
		return _SessionId;
	}
	public void setSessionId(String sessionid) {
		this._SessionId = sessionid;
	}

	public String getTXTOKEN() {
		return _TXTOKEN;
	}
	public void setTXTOKEN(String txtoken) {
		this._TXTOKEN = txtoken;
	}

	public String getOTPKEY() {
		return _OTPKEY;
	}
	public void setOTPKEY(String otpkey) {
		this._OTPKEY = otpkey;
	}

	public String getOUTACN() {
		return _OUTACN;
	}
	public void setOUTACN(String outacn) {
		this._OUTACN = outacn;
	}

	public String getBARCODE1() {
		return _BARCODE1;
	}
	public void setBARCODE1(String barcode1) {
		this._BARCODE1 = barcode1;
	}

	public String getBARCODE2() {
		return _BARCODE2;
	}
	public void setBARCODE2(String barcode2) {
		this._BARCODE2 = barcode2;
	}

	public String getBARCODE3() {
		return _BARCODE3;
	}
	public void setBARCODE3(String barcode3) {
		this._BARCODE3 = barcode3;
	}

	public String getWAT_NO() {
		return _WAT_NO;
	}
	public void setWAT_NO(String wat_no) {
		this._WAT_NO = wat_no;
	}

	public String getAMOUNT() {
		return _AMOUNT;
	}
	public void setAMOUNT(String amount) {
		this._AMOUNT = amount;
	}

	public String getCMTRMEMO() {
		return _CMTRMEMO;
	}
	public void setCMTRMEMO(String cmtrmemo) {
		this._CMTRMEMO = cmtrmemo;
	}

	public String getCMTRMAIL() {
		return _CMTRMAIL;
	}
	public void setCMTRMAIL(String cmtrmail) {
		this._CMTRMAIL = cmtrmail;
	}

	public String getCMMAILMEMO() {
		return _CMMAILMEMO;
	}
	public void setCMMAILMEMO(String cmmailmemo) {
		this._CMMAILMEMO = cmmailmemo;
	}

	public String getCAPTCHA() {
		return _CAPTCHA;
	}
	public void setCAPTCHA(String captcha) {
		this._CAPTCHA = captcha;
	}

	public String getFGTXDATE() {
		return _FGTXDATE;
	}
	public void setFGTXDATE(String fgtxdate) {
		this._FGTXDATE = fgtxdate;
	}

	public String getCMDATE() {
		return _CMDATE;
	}
	public void setCMDATE(String cmdate) {
		this._CMDATE = cmdate;
	}


}
