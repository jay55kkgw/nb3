package fstop.ws;

import java.util.Arrays;

public class CmFxDepCryOut {

	private String _MsgCode;

	private String _MsgName;

	private CmFxDepCryOutRow[] _Table;
	

	@Override
	public String toString()
	{
		return "CmFxDepCryOut [_MsgCode=" + _MsgCode + ", _MsgName=" + _MsgName + ", _Table=" + Arrays.toString(_Table)
				+ "]";
	}

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String MsgCode) {
		this._MsgCode = MsgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String MsgName) {
		this._MsgName = MsgName;
	}

	public CmFxDepCryOutRow[] getTable() {
		return _Table;
	}

	public void setTable(CmFxDepCryOutRow[] Table) {
		this._Table = Table;
	}
	
	

}
