package fstop.ws;

public class WsN531Row {
	
	private String ACN;//		帳號
	private String FDPNO;//		存單號碼
	private String CUID;//		幣別
	private String BALANCE;//	存單金額
	private String ITR;//		利率(%)
	private String INTMTH;//	計息方式
	private String DPISDT;//	起存日
	private String DUEDAT;//	到期日
	private String ILAZLFTM;//	已轉期數
	private String AUTXFTM;//	未轉期數
	private String EVTMARK;//	狀態
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getFDPNO() {
		return FDPNO;
	}
	public void setFDPNO(String fDPNO) {
		FDPNO = fDPNO;
	}
	public String getCUID() {
		return CUID;
	}
	public void setCUID(String cUID) {
		CUID = cUID;
	}
	public String getBALANCE() {
		return BALANCE;
	}
	public void setBALANCE(String bALANCE) {
		BALANCE = bALANCE;
	}
	public String getITR() {
		return ITR;
	}
	public void setITR(String iTR) {
		ITR = iTR;
	}
	public String getINTMTH() {
		return INTMTH;
	}
	public void setINTMTH(String iNTMTH) {
		INTMTH = iNTMTH;
	}
	public String getDPISDT() {
		return DPISDT;
	}
	public void setDPISDT(String dPISDT) {
		DPISDT = dPISDT;
	}
	public String getDUEDAT() {
		return DUEDAT;
	}
	public void setDUEDAT(String dUEDAT) {
		DUEDAT = dUEDAT;
	}
	public String getILAZLFTM() {
		return ILAZLFTM;
	}
	public void setILAZLFTM(String iLAZLFTM) {
		ILAZLFTM = iLAZLFTM;
	}
	public String getAUTXFTM() {
		return AUTXFTM;
	}
	public void setAUTXFTM(String aUTXFTM) {
		AUTXFTM = aUTXFTM;
	}
	public String getEVTMARK() {
		return EVTMARK;
	}
	public void setEVTMARK(String eVTMARK) {
		EVTMARK = eVTMARK;
	}
	
	


}
