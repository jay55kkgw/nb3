package fstop.ws;

public class WsC032In {

	private String _SessionId;// 身份識別
	private String _N950PASSWORD;// 交易密碼
	private String _ADTXNO;// 交易編號
	private String _TRANSCODE;// 基金代碼
	private String _CREDITNO;// 信託號碼
	private String _INTRANSCODE;// 轉入基金代碼
	private String _UNIT;// 單位數
	private String _BILLSENDMODE;// 信託型態
	private String _FUNDAMT;// 信託金額
	private String _SHORTTUNIT;// 已達短線之贖回單位數

	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		this._SessionId = sessionId;
	}

	public String getN950PASSWORD() {
		return _N950PASSWORD;
	}

	public void setN950PASSWORD(String n950password) {
		this._N950PASSWORD = n950password;
	}

	public String getADTXNO() {
		return _ADTXNO;
	}

	public void setADTXNO(String adtxno) {
		this._ADTXNO = adtxno;
	}

	public String getTRANSCODE() {
		return _TRANSCODE;
	}

	public void setTRANSCODE(String transcode) {
		this._TRANSCODE = transcode;
	}

	public String getCREDITNO() {
		return _CREDITNO;
	}

	public void setCREDITNO(String creditno) {
		this._CREDITNO = creditno;
	}

	public String getINTRANSCODE() {
		return _INTRANSCODE;
	}

	public void setINTRANSCODE(String intranscode) {
		this._INTRANSCODE = intranscode;
	}

	public String getUNIT() {
		return _UNIT;
	}

	public void setUNIT(String unit) {
		_UNIT = unit;
	}

	public String getBILLSENDMODE() {
		return _BILLSENDMODE;
	}

	public void setBILLSENDMODE(String billsendmode) {
		_BILLSENDMODE = billsendmode;
	}

	public String getFUNDAMT() {
		return _FUNDAMT;
	}

	public void setFUNDAMT(String fundamt) {
		this._FUNDAMT = fundamt;
	}

	public String getSHORTTUNIT() {
		return _SHORTTUNIT;
	}

	public void setSHORTTUNIT(String shorttunit) {
		this._SHORTTUNIT = shorttunit;
	}

}
