package fstop.ws;

public class WsN078In {

	private String SessionId;//	身份識別
	private String TXTOKEN;//	防止重送代碼
	private String FGTXWAY;//	密碼類別
	private String PINNEW;//	交易密碼SHA1值
	private String OTPKEY;//	OTP動態密碼
	private String CAPTCHA;//	圖形驗證碼
	private String FDPACN;//	存單帳號
	private String FDPNUM;//	存單號碼
	private String INTMTH;//	計息方式
	private String FGSVTYPE;//	續存方式
	private String DPSVACNO;//	轉帳帳號
	private String CUSIDN;//	使用者身分證
	private String DUEDAT;//	到期日
	private String DPISDT;//起存日
	private String ITR;//利率(%)
	private String AMTFDP;// 存單金額
	private String TYPE;// 存款種類
	private String CMPASSWORD;//交易密碼
	
	// no PINNEW no CMPASSWORD
	@Override
	public String toString()
	{
		return "WsN078In [SessionId=" + SessionId + ", TXTOKEN=" + TXTOKEN + ", FGTXWAY=" + FGTXWAY + 
				", CAPTCHA=" + CAPTCHA + ", FDPACN=" + FDPACN + ", FDPNUM=" + FDPNUM
				+ ", INTMTH=" + INTMTH + ", FGSVTYPE=" + FGSVTYPE + ", DPSVACNO=" + DPSVACNO + ", CUSIDN=" + CUSIDN
				+ ", DUEDAT=" + DUEDAT + ", DPISDT=" + DPISDT + ", ITR=" + ITR + ", AMTFDP=" + AMTFDP + ", TYPE=" + TYPE
				+ "]";
	}
	public String getSessionId() {
		return SessionId;
	}
	public void setSessionId(String sessionId) {
		SessionId = sessionId;
	}
	public String getTXTOKEN() {
		return TXTOKEN;
	}
	public void setTXTOKEN(String tXTOKEN) {
		TXTOKEN = tXTOKEN;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getPINNEW() {
		return PINNEW;
	}
	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}
	public String getOTPKEY() {
		return OTPKEY;
	}
	public void setOTPKEY(String oTPKEY) {
		OTPKEY = oTPKEY;
	}
	public String getCAPTCHA() {
		return CAPTCHA;
	}
	public void setCAPTCHA(String cAPTCHA) {
		CAPTCHA = cAPTCHA;
	}
	public String getFDPACN() {
		return FDPACN;
	}
	public void setFDPACN(String fDPACN) {
		FDPACN = fDPACN;
	}
	public String getFDPNUM() {
		return FDPNUM;
	}
	public void setFDPNUM(String fDPNUM) {
		FDPNUM = fDPNUM;
	}
	public String getINTMTH() {
		return INTMTH;
	}
	public void setINTMTH(String iNTMTH) {
		INTMTH = iNTMTH;
	}
	public String getFGSVTYPE() {
		return FGSVTYPE;
	}
	public void setFGSVTYPE(String fGSVTYPE) {
		FGSVTYPE = fGSVTYPE;
	}
	public String getDPSVACNO() {
		return DPSVACNO;
	}
	public void setDPSVACNO(String dPSVACNO) {
		DPSVACNO = dPSVACNO;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getDUEDAT() {
		return DUEDAT;
	}
	public void setDUEDAT(String dUEDAT) {
		DUEDAT = dUEDAT;
	}
	public String getDPISDT() {
		return DPISDT;
	}
	public void setDPISDT(String dpisdt) {
		DPISDT = dpisdt;
	}
	public String getITR() {
		return ITR;
	}
	public void setITR(String itr) {
		ITR = itr;
	}
	public String getAMTFDP() {
		return AMTFDP;
	}
	public void setAMTFDP(String amtfdp) {
		AMTFDP = amtfdp;
	}
	public String getTYPE() {
		return TYPE;
	}
	public void setTYPE(String type) {
		TYPE = type;
	}
	public String getCMPASSWORD() {
		return CMPASSWORD;
	}
	public void setCMPASSWORD(String cmpassword) {
		CMPASSWORD = cmpassword;
	}
}
