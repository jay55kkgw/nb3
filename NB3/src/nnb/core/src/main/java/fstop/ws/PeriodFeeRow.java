package fstop.ws;

public class PeriodFeeRow {

	private String _TSFACN;

	private String _FEELTD;

	private String _QTAMT;

	private String _FEEAMT;

	private String _SOURCE;

	public String getTSFACN() {
		return _TSFACN;
	}

	public void setTSFACN(String TSFACN) {
		this._TSFACN = TSFACN;
	}

	public String getFEELTD() {
		return _FEELTD;
	}

	public void setFEELTD(String FEELTD) {
		this._FEELTD = FEELTD;
	}

	public String getQTAMT() {
		return _QTAMT;
	}

	public void setQTAMT(String QTAMT) {
		this._QTAMT = QTAMT;
	}

	public String getFEEAMT() {
		return _FEEAMT;
	}

	public void setFEEAMT(String FEEAMT) {
		this._FEEAMT = FEEAMT;
	}

	public String getSOURCE() {
		return _SOURCE;
	}

	public void setSOURCE(String SOURCE) {
		this._SOURCE = SOURCE;
	}

}
