package fstop.ws;

public class WsGD11LDRow {

	private String _QTIME;// 時間HHMMSS
	
	private String _BPRICE;// 賣出價格
	
	private String _SPRICE;// 買進價格

	public String getQTIME() {
		return _QTIME;
	}

	public void setQTIME(String qtime) {
		this._QTIME = qtime;
	}

	public String getBPRICE() {
		return _BPRICE;
	}

	public void setBPRICE(String bprice) {
		this._BPRICE = bprice;
	}

	public String getSPRICE() {
		return _SPRICE;
	}

	public void setSPRICE(String sprice) {
		this._SPRICE = sprice;
	}

	
}
