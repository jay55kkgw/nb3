package fstop.ws;

public interface WsInvestAttr{
	
	public WsInvestQAOut getQuestionList(CmUser params);
	
	public WsInvestAttrOut action(WsInvestAttrInSet params);

	public WsInvestAttrResult sendQuestionResult(WsInvestAttrIn params);
}