package fstop.ws;

public class WsN930EIn {

	private String _SessionId;
	private String _NEWEMAIL;
	private String _DPNOTIFY1;
	private String _DPNOTIFY2;
	private String _DPNOTIFY3;
	private String _DPNOTIFY4;
	private String _DPNOTIFY5;
	private String _DPNOTIFY12;
	private String _DPNOTIFY10;
	private String _DPNOTIFY13;
	private String _DPNOTIFY6;
	private String _DPNOTIFY7;
	private String _DPNOTIFY8;
	private String _DPNOTIFY14;
	private String _DPNOTIFY9;
	private String _DPNOTIFY11;
	private String _DPNOTIFY15;
	private String _DPNOTIFY16;
	private String _DPNOTIFY17;
	private String _DPNOTIFY18;
	private String _DPNOTIFY19;
	private String _DPNOTIFY20;
	private String _DPNOTIFY21;

	private String _TXTOKEN;
	private String _OTPKEY;
	private String _CAPTCHA;

	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		this._SessionId = sessionId;
	}

	public String getNEWEMAIL() {
		return _NEWEMAIL;
	}

	public void setNEWEMAIL(String newemail) {
		this._NEWEMAIL = newemail;
	}

	public String getDPNOTIFY1() {
		return _DPNOTIFY1;
	}

	public void setDPNOTIFY1(String dpnotify1) {
		this._DPNOTIFY1 = dpnotify1;
	}

	public String getDPNOTIFY2() {
		return _DPNOTIFY2;
	}

	public void setDPNOTIFY2(String dpnotify2) {
		this._DPNOTIFY2 = dpnotify2;
	}

	public String getDPNOTIFY3() {
		return _DPNOTIFY3;
	}

	public void setDPNOTIFY3(String dpnotify3) {
		this._DPNOTIFY3 = dpnotify3;
	}

	public String getDPNOTIFY4() {
		return _DPNOTIFY4;
	}

	public void setDPNOTIFY4(String dpnotify4) {
		this._DPNOTIFY4 = dpnotify4;
	}

	public String getDPNOTIFY5() {
		return _DPNOTIFY5;
	}

	public void setDPNOTIFY5(String dpnotify5) {
		this._DPNOTIFY5 = dpnotify5;
	}

	public String getDPNOTIFY12() {
		return _DPNOTIFY12;
	}

	public void setDPNOTIFY12(String dpnotify12) {
		this._DPNOTIFY12 = dpnotify12;
	}

	public String getDPNOTIFY10() {
		return _DPNOTIFY10;
	}

	public void setDPNOTIFY10(String dpnotify10) {
		this._DPNOTIFY10 = dpnotify10;
	}

	public String getDPNOTIFY13() {
		return _DPNOTIFY13;
	}

	public void setDPNOTIFY13(String dpnotify13) {
		this._DPNOTIFY13 = dpnotify13;
	}

	public String getDPNOTIFY6() {
		return _DPNOTIFY6;
	}

	public void setDPNOTIFY6(String dpnotify6) {
		this._DPNOTIFY6 = dpnotify6;
	}

	public String getDPNOTIFY7() {
		return _DPNOTIFY7;
	}

	public void setDPNOTIFY7(String dpnotify7) {
		this._DPNOTIFY7 = dpnotify7;
	}

	public String getDPNOTIFY8() {
		return _DPNOTIFY8;
	}

	public void setDPNOTIFY8(String dpnotify8) {
		this._DPNOTIFY8 = dpnotify8;
	}

	public String getDPNOTIFY14() {
		return _DPNOTIFY14;
	}

	public void setDPNOTIFY14(String dpnotify14) {
		this._DPNOTIFY14 = dpnotify14;
	}

	public String getDPNOTIFY9() {
		return _DPNOTIFY9;
	}

	public void setDPNOTIFY9(String dpnotify9) {
		this._DPNOTIFY9 = dpnotify9;
	}

	public String getDPNOTIFY11() {
		return _DPNOTIFY11;
	}

	public void setDPNOTIFY11(String dpnotify11) {
		this._DPNOTIFY11 = dpnotify11;
	}

	public String getDPNOTIFY15() {
		return _DPNOTIFY15;
	}

	public void setDPNOTIFY15(String dpnotify15) {
		this._DPNOTIFY15 = dpnotify15;
	}

	public String getDPNOTIFY16() {
		return _DPNOTIFY16;
	}

	public void setDPNOTIFY16(String dpnotify16) {
		this._DPNOTIFY16 = dpnotify16;
	}

	public String getDPNOTIFY17() {
		return _DPNOTIFY17;
	}

	public void setDPNOTIFY17(String dpnotify17) {
		this._DPNOTIFY17 = dpnotify17;
	}

	public String getDPNOTIFY18() {
		return _DPNOTIFY18;
	}

	public void setDPNOTIFY18(String dpnotify18) {
		this._DPNOTIFY18 = dpnotify18;
	}

	public String getDPNOTIFY19() {
		return _DPNOTIFY19;
	}

	public void setDPNOTIFY19(String dpnotify19) {
		this._DPNOTIFY19 = dpnotify19;
	}

	public String getDPNOTIFY20() {
		return _DPNOTIFY20;
	}

	public void setDPNOTIFY20(String dpnotify20) {
		this._DPNOTIFY20 = dpnotify20;
	}

	
	public String getDPNOTIFY21() {
		return _DPNOTIFY21;
	}

	public void setDPNOTIFY21(String dpnotify21) {
		this._DPNOTIFY21 = dpnotify21;
	}
	
	
	public String getTXTOKEN() {
		return _TXTOKEN;
	}

	public void setTXTOKEN(String txtoken) {
		this._TXTOKEN = txtoken;
	}

	public String getOTPKEY() {
		return _OTPKEY;
	}

	public void setOTPKEY(String otpkey) {
		this._OTPKEY = otpkey;
	}

	public String getCAPTCHA() {
		return _CAPTCHA;
	}

	public void setCAPTCHA(String captcha) {
		this._CAPTCHA = captcha;
	}

}
