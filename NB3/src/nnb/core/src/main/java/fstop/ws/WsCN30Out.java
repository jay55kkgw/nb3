package fstop.ws;

import java.io.Serializable;

public class WsCN30Out implements Serializable {
	private static final long serialVersionUID = 7816188986746352341L;
	private String msgCode;
	private String msgName; 
	
	public String getMsgCode() {
		return msgCode;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public String getMsgName() {
		return msgName;
	}

	public void setMsgName(String msgName) {
		this.msgName = msgName;
	}


}