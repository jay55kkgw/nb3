package fstop.ws;

public class CmFxList {
	
	private String _MsgCode;
	
	private String _MsgName;
	
	private String _NAME;
	
	private String[] _List;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		_MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		_MsgName = msgName;
	}
	
	public String getNAME() {
		return _NAME;
	}

	public void setNAME(String name) {
		_NAME = name;
	}

	public String[] getList() {
		return _List;
	}

	public void setList(String[] list) {
		_List = list;
	}
	
}
