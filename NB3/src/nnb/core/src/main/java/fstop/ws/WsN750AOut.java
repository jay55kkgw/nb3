package fstop.ws;

public class WsN750AOut {

	private String _MsgCode;// 訊息代碼

	private String _MsgName;// 訊息名稱

	private String _CMTXIME;// 交易時間

	private String _OUTACN;// 轉出帳號

	private String _CMDATE1;// 代收期限

	private String _WAT_NO;// 銷帳編號

	private String _CHKCOD;// 查核碼

	private String _AMOUNT;// 繳款金額

	private String _TOTBAL;// 轉出帳號帳上餘額

	private String _AVLBAL;// 轉出帳號可用餘額

	//private String _CMTRMEMO;// 交易備註

	private String _CMDATE;// 轉帳日期

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgcode) {
		this._MsgCode = msgcode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgname) {
		this._MsgName = msgname;
	}

	public String getCMTXIME() {
		return _CMTXIME;
	}

	public void setCMTXIME(String cmtxime) {
		this._CMTXIME = cmtxime;
	}

	public String getOUTACN() {
		return _OUTACN;
	}

	public void setOUTACN(String outacn) {
		this._OUTACN = outacn;
	}

	public String getCMDATE1() {
		return _CMDATE1;
	}

	public void setCMDATE1(String cmdate1) {
		this._CMDATE1 = cmdate1;
	}

	public String getWAT_NO() {
		return _WAT_NO;
	}

	public void setWAT_NO(String wat_no) {
		this._WAT_NO = wat_no;
	}

	public String getCHKCOD() {
		return _CHKCOD;
	}

	public void setCHKCOD(String chkcod) {
		this._CHKCOD = chkcod;
	}

	public String getAMOUNT() {
		return _AMOUNT;
	}

	public void setAMOUNT(String amount) {
		this._AMOUNT = amount;
	}

	public String getTOTBAL() {
		return _TOTBAL;
	}

	public void setTOTBAL(String totbal) {
		this._TOTBAL = totbal;
	}

	public String getAVLBAL() {
		return _AVLBAL;
	}

	public void setAVLBAL(String avlbal) {
		this._AVLBAL = avlbal;
	}

//	public String getCMTRMEMO() {
//		return _CMTRMEMO;
//	}
//
//	public void setCMTRMEMO(String cmtrmemo) {
//		this._CMTRMEMO = cmtrmemo;
//	}

	public String getCMDATE() {
		return _CMDATE;
	}

	public void setCMDATE(String cmdate) {
		this._CMDATE = cmdate;
	}

}
