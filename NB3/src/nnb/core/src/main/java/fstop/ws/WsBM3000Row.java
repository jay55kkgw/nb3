package fstop.ws;

public class WsBM3000Row {

	private String _DPADID;

	private String _DPADTYPE;

	private String _DPADHLK;

	private String _DPADCON;

	private String _DPPICTYPE;

	private String _DPRUNYN;

	private String _DPNOTE;

	private String _DPPICPATH;

	public String getDPADID() {
		return _DPADID;
	}

	public void setDPADID(String dpadid) {
		this._DPADID = dpadid;
	}

	public String getDPADTYPE() {
		return _DPADTYPE;
	}

	public void setDPADTYPE(String dpadtype) {
		this._DPADTYPE = dpadtype;
	}

	public String getDPADHLK() {
		return _DPADHLK;
	}

	public void setDPADHLK(String dpadhlk) {
		this._DPADHLK = dpadhlk;
	}

	public String getDPADCON() {
		return _DPADCON;
	}

	public void setDPADCON(String dpadcon) {
		this._DPADCON = dpadcon;
	}

	public String getDPPICTYPE() {
		return _DPPICTYPE;
	}

	public void setDPPICTYPE(String dppictype) {
		this._DPPICTYPE = dppictype;
	}

	public String getDPRUNYN() {
		return _DPRUNYN;
	}

	public void setDPRUNYN(String dprunyn) {
		this._DPRUNYN = dprunyn;
	}

	public String getDPNOTE() {
		return _DPNOTE;
	}

	public void setDPNOTE(String dpnote) {
		this._DPNOTE = dpnote;
	}

	public String getDPPICPATH() {
		return _DPPICPATH;
	}

	public void setDPPICPATH(String dppicpath) {
		this._DPPICPATH = dppicpath;
	}

}
