package fstop.ws;

public interface WsCommonPools{
	
	public CmAcno getAgreeAcnoList(CmUser params);

	public CmBank getBankList(CmUser params);

	public CmNotifyInfo getNotifyInfo(CmUser params);

	public CmTxToken getTxToken(CmUser params);
	
	public CmCrdAcno getAgreeCreditAcnoList(CmUser params);
	
	public CmCode getCodeList(CmUserQry params);
	
	public CmFxCryOut getCRYList(CmFxCryIn params);
	
	public CmFxAcno getAgreeFxAcnoList(CmUser params);//取得外幣轉帳之約定/常用非約定帳號
	
	public CmBhContactOut getAdmBhContact(CmBhContactIn params);
	
	public CmFxCryOut getComCcyList(CmComCcyIn params);
	
	public CmCRY getCRY (CmUserQry params);
	
	public CmMsg  checkGoldBizTimeStatus (CmUserQry params);
	
	public CmMsgData  checkFxBizTimeStatus (CmUser params);
	
	public CmMsgData getSystemDateTime (CmUser params);
	
	public CmFxDepCryOut getFxDepCRYList(CmFxCryIn params);//取得外匯定存幣別
	
	public CmAcno getCertAgreeAcnoList(CmUser params);
}