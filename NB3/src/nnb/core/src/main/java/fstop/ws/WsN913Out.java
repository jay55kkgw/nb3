package fstop.ws;

public class WsN913Out {

	private String _MsgCode;
	
	private String _MsgName;
	
	private String _MBOPNDT;
	
	private String _MBOPNTM;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getMBOPNDT() {
		return _MBOPNDT;
	}

	public void setMBOPNDT(String mbopndt) {
		this._MBOPNDT = mbopndt;
	}

	public String getMBOPNTM() {
		return _MBOPNTM;
	}

	public void setMBOPNTM(String mbopntm) {
		this._MBOPNTM = mbopntm;
	}
	
	
}
