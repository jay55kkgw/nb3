package fstop.ws;

public class WsInvestAttrResult {

	private String _MsgCode;// 訊息代碼
	private String _MsgName;// 訊息名稱
	private String[] _List;// 問卷答案清單
	private String[] _DiffResult;// 差異清單


	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String[] getList() {
		return _List;
	}

	public void setList(String[] list) {
		this._List = list;
	}
	
	public String[] getDiffResult() {
		return _DiffResult;
	}

	public void setDiffResult(String[] diffresult) {
		this._DiffResult = diffresult;
	}

}
