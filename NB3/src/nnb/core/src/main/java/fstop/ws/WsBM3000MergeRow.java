package fstop.ws;

public class WsBM3000MergeRow {

	private String _DPADID;		  //行銷ID
	private String _DPADTYPE;	  //行銷類別
	private String _DPADHLK;	  //行銷標題
	private String _DPADCON;	  //行銷內容
	private String _DPPICTYPE;	  //圖片位置
	private String _DPRUNYN;	  //跑馬燈顯示
	private String _DPNOTE;		  //強迫公告
	private String _DPPICPATH;	  //行銷圖片
	private String _DPBTNNAME;	  //按鈕名稱
	private String _DPADRESS;	  //超連結網址
	private String _DPMARQUEEPIC; //跑馬燈圖片
	private String _DPSDATE;	  //活動期間起日
	private String _DPEDATE;	  //活動期間迄日
	

	public String getDPADID() {
		return _DPADID;
	}

	public void setDPADID(String dpadid) {
		this._DPADID = dpadid;
	}

	public String getDPADTYPE() {
		return _DPADTYPE;
	}

	public void setDPADTYPE(String dpadtype) {
		this._DPADTYPE = dpadtype;
	}

	public String getDPADHLK() {
		return _DPADHLK;
	}

	public void setDPADHLK(String dpadhlk) {
		this._DPADHLK = dpadhlk;
	}

	public String getDPADCON() {
		return _DPADCON;
	}

	public void setDPADCON(String dpadcon) {
		this._DPADCON = dpadcon;
	}

	public String getDPPICTYPE() {
		return _DPPICTYPE;
	}

	public void setDPPICTYPE(String dppictype) {
		this._DPPICTYPE = dppictype;
	}

	public String getDPRUNYN() {
		return _DPRUNYN;
	}

	public void setDPRUNYN(String dprunyn) {
		this._DPRUNYN = dprunyn;
	}

	public String getDPNOTE() {
		return _DPNOTE;
	}

	public void setDPNOTE(String dpnote) {
		this._DPNOTE = dpnote;
	}

	public String getDPPICPATH() {
		return _DPPICPATH;
	}

	public void setDPPICPATH(String dppicpath) {
		this._DPPICPATH = dppicpath;
	}
	
	public String getDPBTNNAME() {
		return _DPBTNNAME;
	}

	public void setDPBTNNAME(String DPBTNNAME) {
		this._DPBTNNAME = DPBTNNAME;
	}
	
	public String getDPADRESS() {
		return _DPADRESS;
	}

	public void setDPADRESS(String DPADRESS) {
		this._DPADRESS = DPADRESS;
	}

	public String getDPMARQUEEPIC() {
		return _DPMARQUEEPIC;
	}

	public void setDPMARQUEEPIC(String DPMARQUEEPIC) {
		this._DPMARQUEEPIC = DPMARQUEEPIC;
	}
	public String getDPSDATE() {
		return _DPSDATE;
	}

	public void setDPSDATE(String DPSDATE) {
		this._DPSDATE = DPSDATE;
	}
	public String getDPEDATE() {
		return _DPEDATE;
	}

	public void setDPEDATE(String DPEDATE) {
		this._DPEDATE = DPEDATE;
	}
}
