package fstop.ws;

public class CmcancelList {

	private String _MsgCode;	  //訊息代碼
	private String _MsgName; 	  //訊息名稱		
	private String _CMQTIME;	  //查詢時間
	private String _RecordCount;  //資料總數
	private WsN079Row[] _Table;   //表格結果
	
	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String MsgCode) {
		this._MsgCode = MsgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String MsgName) {
		this._MsgName = MsgName;
	}
	
	public String getCMQTIME(){
		return _CMQTIME;
	}
	
	public void setCMQTIME(String CMQTIME){
		this._CMQTIME = CMQTIME;
	}
	
	public String getRecordCount(){
		return _RecordCount;
	}
	
	public void setRecordCount(String RecordCount){
		this._RecordCount = RecordCount;
	}
	
	public WsN079Row[] getTable() {
		return _Table;
	}

	public void setTable(WsN079Row[] Table) {
		this._Table = Table;
	}
}
