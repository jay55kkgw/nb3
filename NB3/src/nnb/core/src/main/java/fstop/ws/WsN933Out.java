package fstop.ws;

public class WsN933Out {

	private String _MsgCode;

	private String _MsgName;

	private WsN933Row[] _Table;



	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public WsN933Row[] getTable() {
		return _Table;
	}

	public void setTable(WsN933Row[] table) {
		_Table = table;
	}

}
