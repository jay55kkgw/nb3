package fstop.ws;

public class WsBPDataOut {
	
	private String _MsgCode;	//訊息代碼
	private String _MsgName;	//訊息名稱
	private String _CMQTIME;	//查詢時間
	private String _COUNT;	//資料總數
	private String _CUSTNAME;	//使用者姓名
	private WsBPDataRow[] _Table;	//表格結果
	
	public String getMsgCode() {
		return _MsgCode;
	}
	public void setMsgCode(String msgCode) {
		_MsgCode = msgCode;
	}
	public String getMsgName() {
		return _MsgName;
	}
	public void setMsgName(String msgName) {
		_MsgName = msgName;
	}
	public String getCMQTIME() {
		return _CMQTIME;
	}
	public void setCMQTIME(String cmqtime) {
		_CMQTIME = cmqtime;
	}
	public String getCOUNT() {
		return _COUNT;
	}
	public void setCOUNT(String count) {
		_COUNT = count;
	}
	public String getCUSTNAME() {
		return _CUSTNAME;
	}
	public void setCUSTNAME(String custname) {
		_CUSTNAME = custname;
	}
	public WsBPDataRow[] getTable() {
		return _Table;
	}
	public void setTable(WsBPDataRow[] table) {
		_Table = table;
	}

	

}
