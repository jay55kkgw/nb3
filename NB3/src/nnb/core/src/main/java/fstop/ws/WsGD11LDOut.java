package fstop.ws;

public class WsGD11LDOut {

	public String _MsgCode;// 訊息代碼

	public String _MsgName;// 訊息名稱

	private String _SYSDATE;// 系統日期

	private String _SYSTIME;// 系統時間

	public WsGD11LDRow[] _Table1;// 表格結果

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public WsGD11LDRow[] getTable1() {
		return _Table1;
	}

	public void setTable1(WsGD11LDRow[] table1) {
		this._Table1 = table1;
	}

	public String getSYSDATE() {
		return _SYSDATE;
	}

	public void setSYSDATE(String sysdate) {
		this._SYSDATE = sysdate;
	}

	public String getSYSTIME() {
		return _SYSTIME;
	}

	public void setSYSTIME(String systime) {
		this._SYSTIME = systime;
	}

}
