package fstop.ws;

public class CmCRY {

	private String _MsgCode;// 訊息代碼

	private String _MsgName;// 訊息名稱

	private String _ADCURRENCY;// 幣別代碼

	private String _ADCCYNAME;// 幣別中文名稱

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getADCURRENCY() {
		return _ADCURRENCY;
	}

	public void setADCURRENCY(String adcurrency) {
		this._ADCURRENCY = adcurrency;
	}

	public String getADCCYNAME() {
		return _ADCCYNAME;
	}

	public void setADCCYNAME(String adccyname) {
		this._ADCCYNAME = adccyname;
	}

}
