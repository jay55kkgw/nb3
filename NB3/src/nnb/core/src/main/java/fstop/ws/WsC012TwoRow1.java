package fstop.ws;

/**
 * @author user
 * 
 */
public class WsC012TwoRow1 {

	private String _FUNDLNAME;// 基金名稱

	private String _CREDITNO;// 信託帳號

	private String _AC225;// 首次申購日

	private String _CRY;// 信託幣別

	private String _TOTAMT;// 信託金額

	private String _AC202;// 類別

	private String _ACUCOUNT;// 單位數

	private String _REFUNDAMT;// 參考淨值

	private String _NET01;// 淨值日期

	private String _FXRATE;// 參考匯率

	private String _REFVALUE;// 參考市值

	private String _LCYRAT;// 台幣報酬率

	private String _FCYRAT;// 外幣報酬率

	private String _STOP;// 停損設定

	private String _YIELD;// 停利設定

	private String _DIFAMT;// 損益金額

	private String _REFMARK;// 憑證

	private String _TYPE;// 快速選項
	
	private String _TRANSCODE;//基金代碼

	public String getFUNDLNAME() {
		return _FUNDLNAME;
	}

	public void setFUNDLNAME(String fundlname) {
		this._FUNDLNAME = fundlname;
	}

	public String getCREDITNO() {
		return _CREDITNO;
	}

	public void setCREDITNO(String creditno) {
		this._CREDITNO = creditno;
	}

	public String getAC225() {
		return _AC225;
	}

	public void setAC225(String ac225) {
		this._AC225 = ac225;
	}

	public String getCRY() {
		return _CRY;
	}

	public void setCRY(String cry) {
		this._CRY = cry;
	}

	public String getTOTAMT() {
		return _TOTAMT;
	}

	public void setTOTAMT(String totamt) {
		this._TOTAMT = totamt;
	}

	public String getAC202() {
		return _AC202;
	}

	public void setAC202(String ac202) {
		this._AC202 = ac202;
	}

	public String getACUCOUNT() {
		return _ACUCOUNT;
	}

	public void setACUCOUNT(String acucount) {
		this._ACUCOUNT = acucount;
	}

	public String getREFUNDAMT() {
		return _REFUNDAMT;
	}

	public void setREFUNDAMT(String refundamt) {
		this._REFUNDAMT = refundamt;
	}

	public String getNET01() {
		return _NET01;
	}

	public void setNET01(String net01) {
		this._NET01 = net01;
	}

	public String getFXRATE() {
		return _FXRATE;
	}

	public void setFXRATE(String fxrate) {
		this._FXRATE = fxrate;
	}

	public String getREFVALUE() {
		return _REFVALUE;
	}

	public void setREFVALUE(String refvalue) {
		this._REFVALUE = refvalue;
	}

	public String getLCYRAT() {
		return _LCYRAT;
	}

	public void setLCYRAT(String lcyrat) {
		this._LCYRAT = lcyrat;
	}

	public String getFCYRAT() {
		return _FCYRAT;
	}

	public void setFCYRAT(String fcyrat) {
		this._FCYRAT = fcyrat;
	}

	public String getSTOP() {
		return _STOP;
	}

	public void setSTOP(String stop) {
		this._STOP = stop;
	}

	public String getYIELD() {
		return _YIELD;
	}

	public void setYIELD(String yield) {
		this._YIELD = yield;
	}

	public String getDIFAMT() {
		return _DIFAMT;
	}

	public void setDIFAMT(String difamt) {
		this._DIFAMT = difamt;
	}

	public String getREFMARK() {
		return _REFMARK;
	}

	public void setREFMARK(String refmark) {
		this._REFMARK = refmark;
	}

	public String getTYPE() {
		return _TYPE;
	}

	public void setTYPE(String type) {
		this._TYPE = type;
	}

	public String getTRANSCODE() {
		return _TRANSCODE;
	}

	public void setTRANSCODE(String transcode) {
		this._TRANSCODE = transcode;
	}

}
