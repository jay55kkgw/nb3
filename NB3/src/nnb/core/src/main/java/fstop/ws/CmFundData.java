package fstop.ws;

public class CmFundData {

	private String _MsgCode	;//訊息代碼
	private String _MsgName;//	訊息名稱
	private String _TRANSCRY;//	基金計價幣別
	private String _FUNDLNAME;//	基金全稱
	private String _COUNTRYTYPE;//	國內或國外
	private String _FUNDMARK;//	是否交易
	private String _RISK;//	基金風險等級設定
	private String _FUS98E;//高收益債券註記 
	
	public String getMsgCode() {
		return _MsgCode;
	}
	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}
	public String getMsgName() {
		return _MsgName;
	}
	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}
	public String getTRANSCRY() {
		return _TRANSCRY;
	}
	public void setTRANSCRY(String transcry) {
		this._TRANSCRY = transcry;
	}
	public String getFUNDLNAME() {
		return _FUNDLNAME;
	}
	public void setFUNDLNAME(String fundlname) {
		this._FUNDLNAME = fundlname;
	}
	public String getCOUNTRYTYPE() {
		return _COUNTRYTYPE;
	}
	public void setCOUNTRYTYPE(String countrytype) {
		this._COUNTRYTYPE = countrytype;
	}
	public String getFUNDMARK() {
		return _FUNDMARK;
	}
	public void setFUNDMARK(String fundmark) {
		this._FUNDMARK = fundmark;
	}
	public String getRISK() {
		return _RISK;
	}
	public void setRISK(String risk) {
		this._RISK = risk;
	}
	
	public String getFUS98E() {
		return _FUS98E;
	}
	public void setFUS98E(String fus98e) {
		this._FUS98E = fus98e;
	}

}
