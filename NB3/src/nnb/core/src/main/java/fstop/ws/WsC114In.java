package fstop.ws;

public class WsC114In {

	private String _SessionId;

	private String _N950PASSWORD;

	private String _TXTOKEN;

	private String _FGTXWAY;

	private String _CMPASSWORD;

	private String _PINNEW;

	private String _OTPKEY;

	private String _CUSIDN;

	private String _CREDITNO;

	private String _TRANSCODE;

	private String _FUNDAMT;

	private String _TRADEDATE;

	private String _UNIT;

	private String _BILLSENDMODE;

	private String _FUNDACN;
	
	private String _SHORTTUNIT;
	
	private String _CRYCODE;

	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionid) {
		this._SessionId = sessionid;
	}

	public String getN950PASSWORD() {
		return _N950PASSWORD;
	}

	public void setN950PASSWORD(String n950password) {
		this._N950PASSWORD = n950password;
	}

	public String getTXTOKEN() {
		return _TXTOKEN;
	}

	public void setTXTOKEN(String txtoken) {
		this._TXTOKEN = txtoken;
	}

	public String getFGTXWAY() {
		return _FGTXWAY;
	}

	public void setFGTXWAY(String fgtxway) {
		this._FGTXWAY = fgtxway;
	}

	public String getCMPASSWORD() {
		return _CMPASSWORD;
	}

	public void setCMPASSWORD(String cmpassword) {
		this._CMPASSWORD = cmpassword;
	}

	public String getPINNEW() {
		return _PINNEW;
	}

	public void setPINNEW(String pinnew) {
		this._PINNEW = pinnew;
	}

	public String getOTPKEY() {
		return _OTPKEY;
	}

	public void setOTPKEY(String otpkey) {
		this._OTPKEY = otpkey;
	}

	public String getCUSIDN() {
		return _CUSIDN;
	}

	public void setCUSIDN(String cusidn) {
		this._CUSIDN = cusidn;
	}

	public String getCREDITNO() {
		return _CREDITNO;
	}

	public void setCREDITNO(String creditno) {
		this._CREDITNO = creditno;
	}

	public String getTRANSCODE() {
		return _TRANSCODE;
	}

	public void setTRANSCODE(String transcode) {
		this._TRANSCODE = transcode;
	}

	public String getFUNDAMT() {
		return _FUNDAMT;
	}

	public void setFUNDAMT(String fundamt) {
		this._FUNDAMT = fundamt;
	}

	public String getTRADEDATE() {
		return _TRADEDATE;
	}

	public void setTRADEDATE(String tradedate) {
		this._TRADEDATE = tradedate;
	}

	public String getUNIT() {
		return _UNIT;
	}

	public void setUNIT(String unit) {
		this._UNIT = unit;
	}

	public String getBILLSENDMODE() {
		return _BILLSENDMODE;
	}

	public void setBILLSENDMODE(String billsendmode) {
		this._BILLSENDMODE = billsendmode;
	}

	public String getFUNDACN() {
		return _FUNDACN;
	}

	public void setFUNDACN(String fundacn) {
		this._FUNDACN = fundacn;
	}

	public String getSHORTTUNIT() {
		return _SHORTTUNIT;
	}

	public void setSHORTTUNIT(String shorttunit) {
		this._SHORTTUNIT = shorttunit;
	}

	public String getCRYCODE() {
		return _CRYCODE;
	}

	public void setCRYCODE(String crycode) {
		this._CRYCODE = crycode;
	}

}
