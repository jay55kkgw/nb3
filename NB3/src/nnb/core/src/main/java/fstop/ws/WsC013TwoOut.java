package fstop.ws;

public class WsC013TwoOut {

	private String _MsgCode;		//訊息代碼
	private String _MsgName;		//訊息名稱
	private String _QUERYTIME;		//查詢時間
	private String _CMRECNUM;		//資料總數
	private String _CUSIDN;			//身分證號/統一編號
	private String _NAME;			//姓名
	private String _CREDITNO;		//信託帳號
	private WsC013TwoRow[] _Table;	//表格結果

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getQUERYTIME() {
		return _QUERYTIME;
	}

	public void setQUERYTIME(String QUERYTIME) {
		this._QUERYTIME = QUERYTIME;
	}

	public String getCMRECNUM() {
		return _CMRECNUM;
	}

	public void setCMRECNUM(String CMRECNUM) {
		this._CMRECNUM = CMRECNUM;
	}
	
	public String getCUSIDN() {
		return _CUSIDN;
	}

	public void setCUSIDN(String CUSIDN) {
		this._CUSIDN = CUSIDN;
	}
	
	public String getNAME() {
		return _NAME;
	}

	public void setNAME(String NAME) {
		this._NAME = NAME;
	}
	
	public String getCREDITNO() {
		return _CREDITNO;
	}

	public void setCREDITNO(String CREDITNO) {
		this._CREDITNO = CREDITNO;
	}
	
	public WsC013TwoRow[] getTable() {
		return _Table;
	}

	public void setTable(WsC013TwoRow[] table) {
		this._Table = table;
	}

}
