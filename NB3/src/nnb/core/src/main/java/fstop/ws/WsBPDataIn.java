package fstop.ws;

public class WsBPDataIn {
	
	private String _SessionId;  //身份識別
	private String _IDENTITY;	//身分證字號
	private String _CANCELNO;	//銷帳編號
	private String _FLAG;		//查詢條件
	private String _TYPE;		//交易查詢
	
	public String getSessionId() {
		return _SessionId;
	}
	public void setSessionId(String SessionId) {
		_SessionId = SessionId;
	}
	public String getIDENTITY() {
		return _IDENTITY;
	}
	public void setIDENTITY(String identity) {
		_IDENTITY = identity;
	}
	public String getCANCELNO() {
		return _CANCELNO;
	}
	public void setCANCELNO(String cancelno) {
		_CANCELNO = cancelno;
	}
	public String getFLAG() {
		return _FLAG;
	}
	public void setFLAG(String flag) {
		_FLAG = flag;
	}
	public String getTYPE() {
		return _TYPE;
	}
	public void setTYPE(String type) {
		_TYPE = type;
	}
}
