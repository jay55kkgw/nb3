package fstop.ws;

public class WsC116Out {

	private String _MsgCode;				//訊息代碼
	private String _MsgName;				//訊息名稱
	private String _CHANGEKIND;				//變更種類
	private String _TRANSCODE_RecordCount;	//資料總數
	private String _CUSIDN;					//身分證字號/統一編號
	private String _CUSNAME;				//姓名
	private WsC116Row[] _Table;				//表格結果
	
	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		_MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		_MsgName = msgName;
	}

	public String getCHANGEKIND() {
		return _CHANGEKIND;
	}

	public void setCHANGEKIND(String CHANGEKIND) {
		_CHANGEKIND = CHANGEKIND;
	}
	
	public String getTRANSCODE_RecordCount() {
		return _TRANSCODE_RecordCount;
	}

	public void setTRANSCODE_RecordCount(String TRANSCODE_RecordCount) {
		_TRANSCODE_RecordCount = TRANSCODE_RecordCount;
	}
	
	public String getCUSIDN() {
		return _CUSIDN;
	}

	public void setCUSIDN(String cusidn) {
		_CUSIDN = cusidn;
	}
	
	public String getCUSNAME() {
		return _CUSNAME;
	}

	public void setCUSNAME(String CUSNAME) {
		_CUSNAME = CUSNAME;
	}

	public WsC116Row[] getTable() {
		return _Table;
	}

	public void setTable(WsC116Row[] table) {
		this._Table = table;
	}
	
}
