package fstop.ws;

public interface WsN190{

	public CmList getAcnoList(CmUser params);
	
	public WsN190Out action(WsN190In params);
	
	public CmGDAcnoInfo getGDAcnoInfoByACN(CmUserACN params);
}