package fstop.ws;

public class WsC022Out {

	private String _MsgCode;

	private String _MsgName;

	private String _CUSIDN;

	private String _CUSNAME;

	private String _CREDITNO;

	private String _TRANSCODE;

	private String _INTRANSCODE;

	private String _BILLSENDMODE;

	private String _UNIT;

	private String _FUNCUR;

	private String _FUNDAMT;

	private String _AMT3;

	private String _FCA2;

	private String _FCA1;

	private String _ACN1;

	private String _AMT5;

	private String _TRADEDATE;

	private String _SSLTXNO;

	private String _STR_1;

	private String _STR_2;

	private String _STR_3;

	private String _STR_4;

	private String _STR_5;

	private String _STR_6;

	private String _STR_7;

	private String _STR_9;

	private String _LINK_FH_NAME;

	private String _LINK_FEE_01;

	private String _LINK_FEE_02;

	private String _LINK_FEE_05;

	private String _LINK_FEE_07;

	private String _LINK_SLS_08;

	private String _LINK_BASE_01;

	private String _LINK_RESULT_05;

	private String _LINK_RESULT_06;
	
	private String _LINK_FUND_NAME;
	
	private String _CRYNAME;

	public String getMsgCode() {
		return _MsgCode;
	}
	public void setMsgCode(String msgcode) {
		this._MsgCode = msgcode;
	}

	public String getMsgName() {
		return _MsgName;
	}
	public void setMsgName(String msgname) {
		this._MsgName = msgname;
	}

	public String getCUSIDN() {
		return _CUSIDN;
	}
	public void setCUSIDN(String cusidn) {
		this._CUSIDN = cusidn;
	}

	public String getCUSNAME() {
		return _CUSNAME;
	}
	public void setCUSNAME(String cusname) {
		this._CUSNAME = cusname;
	}

	public String getCREDITNO() {
		return _CREDITNO;
	}
	public void setCREDITNO(String creditno) {
		this._CREDITNO = creditno;
	}

	public String getTRANSCODE() {
		return _TRANSCODE;
	}
	public void setTRANSCODE(String transcode) {
		this._TRANSCODE = transcode;
	}

	public String getINTRANSCODE() {
		return _INTRANSCODE;
	}
	public void setINTRANSCODE(String intranscode) {
		this._INTRANSCODE = intranscode;
	}

	public String getBILLSENDMODE() {
		return _BILLSENDMODE;
	}
	public void setBILLSENDMODE(String billsendmode) {
		this._BILLSENDMODE = billsendmode;
	}

	public String getUNIT() {
		return _UNIT;
	}
	public void setUNIT(String unit) {
		this._UNIT = unit;
	}

	public String getFUNCUR() {
		return _FUNCUR;
	}
	public void setFUNCUR(String funcur) {
		this._FUNCUR = funcur;
	}

	public String getFUNDAMT() {
		return _FUNDAMT;
	}
	public void setFUNDAMT(String fundamt) {
		this._FUNDAMT = fundamt;
	}

	public String getAMT3() {
		return _AMT3;
	}
	public void setAMT3(String amt3) {
		this._AMT3 = amt3;
	}

	public String getFCA2() {
		return _FCA2;
	}
	public void setFCA2(String fca2) {
		this._FCA2 = fca2;
	}

	public String getFCA1() {
		return _FCA1;
	}
	public void setFCA1(String fca1) {
		this._FCA1 = fca1;
	}

	public String getACN1() {
		return _ACN1;
	}
	public void setACN1(String acn1) {
		this._ACN1 = acn1;
	}

	public String getAMT5() {
		return _AMT5;
	}
	public void setAMT5(String amt5) {
		this._AMT5 = amt5;
	}

	public String getTRADEDATE() {
		return _TRADEDATE;
	}
	public void setTRADEDATE(String tradedate) {
		this._TRADEDATE = tradedate;
	}

	public String getSSLTXNO() {
		return _SSLTXNO;
	}
	public void setSSLTXNO(String ssltxno) {
		this._SSLTXNO = ssltxno;
	}

	public String getSTR_1() {
		return _STR_1;
	}
	public void setSTR_1(String str_1) {
		this._STR_1 = str_1;
	}

	public String getSTR_2() {
		return _STR_2;
	}
	public void setSTR_2(String str_2) {
		this._STR_2 = str_2;
	}

	public String getSTR_3() {
		return _STR_3;
	}
	public void setSTR_3(String str_3) {
		this._STR_3 = str_3;
	}

	public String getSTR_4() {
		return _STR_4;
	}
	public void setSTR_4(String str_4) {
		this._STR_4 = str_4;
	}

	public String getSTR_5() {
		return _STR_5;
	}
	public void setSTR_5(String str_5) {
		this._STR_5 = str_5;
	}

	public String getSTR_6() {
		return _STR_6;
	}
	public void setSTR_6(String str_6) {
		this._STR_6 = str_6;
	}

	public String getSTR_7() {
		return _STR_7;
	}
	public void setSTR_7(String str_7) {
		this._STR_7 = str_7;
	}

	public String getSTR_9() {
		return _STR_9;
	}
	public void setSTR_9(String str_9) {
		this._STR_9 = str_9;
	}

	public String getLINK_FH_NAME() {
		return _LINK_FH_NAME;
	}
	public void setLINK_FH_NAME(String link_fh_name) {
		this._LINK_FH_NAME = link_fh_name;
	}

	public String getLINK_FEE_01() {
		return _LINK_FEE_01;
	}
	public void setLINK_FEE_01(String link_fee_01) {
		this._LINK_FEE_01 = link_fee_01;
	}

	public String getLINK_FEE_02() {
		return _LINK_FEE_02;
	}
	public void setLINK_FEE_02(String link_fee_02) {
		this._LINK_FEE_02 = link_fee_02;
	}

	public String getLINK_FEE_05() {
		return _LINK_FEE_05;
	}
	public void setLINK_FEE_05(String link_fee_05) {
		this._LINK_FEE_05 = link_fee_05;
	}

	public String getLINK_FEE_07() {
		return _LINK_FEE_07;
	}
	public void setLINK_FEE_07(String link_fee_07) {
		this._LINK_FEE_07 = link_fee_07;
	}

	public String getLINK_SLS_08() {
		return _LINK_SLS_08;
	}
	public void setLINK_SLS_08(String link_sls_08) {
		this._LINK_SLS_08 = link_sls_08;
	}

	public String getLINK_BASE_01() {
		return _LINK_BASE_01;
	}
	public void setLINK_BASE_01(String link_base_01) {
		this._LINK_BASE_01 = link_base_01;
	}

	public String getLINK_RESULT_05() {
		return _LINK_RESULT_05;
	}
	public void setLINK_RESULT_05(String link_result_05) {
		this._LINK_RESULT_05 = link_result_05;
	}

	public String getLINK_RESULT_06() {
		return _LINK_RESULT_06;
	}
	public void setLINK_RESULT_06(String link_result_06) {
		this._LINK_RESULT_06 = link_result_06;
	}
	public String getLINK_FUND_NAME() {
		return _LINK_FUND_NAME;
	}
	public void setLINK_FUND_NAME(String link_fund_name) {
		this._LINK_FUND_NAME = link_fund_name;
	}
	public String getCRYNAME() {
		return _CRYNAME;
	}

	public void setCRYNAME(String cryname) {
		this._CRYNAME = cryname;
	}

}
