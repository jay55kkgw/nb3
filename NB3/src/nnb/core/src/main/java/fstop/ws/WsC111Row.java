package fstop.ws;

public class WsC111Row {
 
	private String _CREDITNO;		//信託帳號		
	private String _PAYTAG;			//扣款標的代號	
	private String _FUNDLNAME;		//扣款標的名稱
	private String _MIP;			//類別
	private String _DAMT;			//扣款方式
	private String _PAYCUR;			//每次定額申購金額＋不定額基準申購金額幣別
	private String _PAYAMT;			//每次定額申購金額＋不定額基準申購金額
	private String _PAYDAY;			//扣款日	
	private String _STOPBEGDAY;		//暫停扣款起日
	private String _STOPENDDAY;		//暫停扣款迄日
	private String _BEFORETYPEFLAG;	//原扣款狀態
	private String _TYPEFLAG;		//扣款狀態
	private String _PAYDAY1;		//扣款日期1
	private String _PAYDAY2;		//扣款日期2
	private String _PAYDAY3;		//扣款日期3
	private String _PAYDAY4;		//扣款日期4
	private String _PAYDAY5;		//扣款日期5
	private String _PAYDAY6;		//扣款日期6
	private String _PAYDAY7;		//扣款日期7
	private String _PAYDAY8;		//扣款日期8
	private String _PAYDAY9;		//扣款日期9

	public String getCREDITNO() {
		return _CREDITNO;
	}

	public void setCREDITNO(String CREDITNO) {
		this._CREDITNO = CREDITNO;
	}
	
	public String getPAYTAG() {
		return _PAYTAG;
	}

	public void setPAYTAG(String PAYTAG) {
		this._PAYTAG = PAYTAG;
	}

	public String getFUNDLNAME() {
		return _FUNDLNAME;
	}

	public void setFUNDLNAME(String FUNDLNAME) {
		this._FUNDLNAME = FUNDLNAME;
	}

	public String getMIP() {
		return _MIP;
	}

	public void setMIP(String MIP) {
		this._MIP = MIP;
	}

	public String getDAMT() {
		return _DAMT;
	}

	public void setDAMT(String DAMT) {
		this._DAMT = DAMT;
	}
	
	public String getPAYCUR() {
		return _PAYCUR;
	}

	public void setPAYCUR(String PAYCUR) {
		this._PAYCUR = PAYCUR;
	}

	public String getPAYAMT() {
		return _PAYAMT;
	}

	public void setPAYAMT(String PAYAMT) {
		this._PAYAMT = PAYAMT;
	}

	public String getPAYDAY() {
		return _PAYDAY;
	}

	public void setPAYDAY(String PAYDAY) {
		this._PAYDAY = PAYDAY;
	}
	
	public String getSTOPBEGDAY() {
		return _STOPBEGDAY;
	}

	public void setSTOPBEGDAY(String STOPBEGDAY) {
		this._STOPBEGDAY = STOPBEGDAY;
	}

	public String getSTOPENDDAY() {
		return _STOPENDDAY;
	}

	public void setSTOPENDDAY(String STOPENDDAY) {
		this._STOPENDDAY = STOPENDDAY;
	}
	
	public String getBEFORETYPEFLAG() {
		return _BEFORETYPEFLAG;
	}

	public void setBEFORETYPEFLAG(String BEFORETYPEFLAG) {
		this._BEFORETYPEFLAG = BEFORETYPEFLAG;
	}
	
	public String getTYPEFLAG() {
		return _TYPEFLAG;
	}

	public void setTYPEFLAG(String TYPEFLAG) {
		this._TYPEFLAG = TYPEFLAG;
	}
	
	public String getPAYDAY1() {
		return _PAYDAY1;
	}

	public void setPAYDAY1(String PAYDAY1) {
		this._PAYDAY1 = PAYDAY1;
	}
	
	public String getPAYDAY2() {
		return _PAYDAY2;
	}

	public void setPAYDAY2(String PAYDAY2) {
		this._PAYDAY2 = PAYDAY2;
	}
	
	public String getPAYDAY3() {
		return _PAYDAY3;
	}

	public void setPAYDAY3(String PAYDAY3) {
		this._PAYDAY3 = PAYDAY3;
	}
	
	public String getPAYDAY4() {
		return _PAYDAY4;
	}

	public void setPAYDAY4(String PAYDAY4) {
		this._PAYDAY4 = PAYDAY4;
	}
	
	public String getPAYDAY5() {
		return _PAYDAY5;
	}

	public void setPAYDAY5(String PAYDAY5) {
		this._PAYDAY5 = PAYDAY5;
	}
	
	public String getPAYDAY6() {
		return _PAYDAY6;
	}

	public void setPAYDAY6(String PAYDAY6) {
		this._PAYDAY6 = PAYDAY6;
	}
	
	public String getPAYDAY7() {
		return _PAYDAY7;
	}

	public void setPAYDAY7(String PAYDAY7) {
		this._PAYDAY7 = PAYDAY7;
	}
	
	public String getPAYDAY8() {
		return _PAYDAY8;
	}

	public void setPAYDAY8(String PAYDAY8) {
		this._PAYDAY8 = PAYDAY8;
	}
	
	public String getPAYDAY9() {
		return _PAYDAY9;
	}

	public void setPAYDAY9(String PAYDAY9) {
		this._PAYDAY9 = PAYDAY9;
	}
}
