package fstop.ws;

public class WsN078Out {

	private String MsgCode;//	訊息代碼
	private String MsgName;//	訊息名稱
	private String 	CMTXTIME;//	交易時間
	private String 	FDPACN;//	存單帳號
	private String FDPNUM;//	存單號碼
	private String AMOUNT;//	存單金額
	private String FDPACC;//	存款種類
	private String TERM;//	存款期別
	private String SDT;//	起存日
	private String 	DUEDAT;//	到期日
	private String ITR;//	利率
	private String INTMTH;//	計息方式
	private String FGSVTYPE;//	續存方式
	private String FDPAMT;//	原存單金額
	private String PYDINT;//	原存單利息
	private String GRSTAX;//	代扣利息所得稅
	private String NHITAX;//	健保費
	private String TSFACN;//    轉入帳號
	
	
	@Override
	public String toString()
	{
		return "WsN078Out [MsgCode=" + MsgCode + ", MsgName=" + MsgName + ", CMTXTIME=" + CMTXTIME + ", FDPACN="
				+ FDPACN + ", FDPNUM=" + FDPNUM + ", AMOUNT=" + AMOUNT + ", FDPACC=" + FDPACC + ", TERM=" + TERM
				+ ", SDT=" + SDT + ", DUEDAT=" + DUEDAT + ", ITR=" + ITR + ", INTMTH=" + INTMTH + ", FGSVTYPE="
				+ FGSVTYPE + ", FDPAMT=" + FDPAMT + ", PYDINT=" + PYDINT + ", GRSTAX=" + GRSTAX + ", NHITAX=" + NHITAX
				+ ", TSFACN=" + TSFACN + "]";
	}
	public String getMsgCode() {
		return MsgCode;
	}
	public void setMsgCode(String msgCode) {
		MsgCode = msgCode;
	}
	public String getMsgName() {
		return MsgName;
	}
	public void setMsgName(String msgName) {
		MsgName = msgName;
	}
	public String getCMTXTIME() {
		return CMTXTIME;
	}
	public void setCMTXTIME(String cMTXTIME) {
		CMTXTIME = cMTXTIME;
	}
	public String getFDPACN() {
		return FDPACN;
	}
	public void setFDPACN(String fDPACN) {
		FDPACN = fDPACN;
	}
	public String getFDPNUM() {
		return FDPNUM;
	}
	public void setFDPNUM(String fDPNUM) {
		FDPNUM = fDPNUM;
	}
	public String getAMOUNT() {
		return AMOUNT;
	}
	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}
	public String getFDPACC() {
		return FDPACC;
	}
	public void setFDPACC(String fDPACC) {
		FDPACC = fDPACC;
	}
	public String getTERM() {
		return TERM;
	}
	public void setTERM(String tERM) {
		TERM = tERM;
	}
	public String getSDT() {
		return SDT;
	}
	public void setSDT(String sDT) {
		SDT = sDT;
	}
	public String getDUEDAT() {
		return DUEDAT;
	}
	public void setDUEDAT(String dUEDAT) {
		DUEDAT = dUEDAT;
	}
	public String getITR() {
		return ITR;
	}
	public void setITR(String iTR) {
		ITR = iTR;
	}
	public String getINTMTH() {
		return INTMTH;
	}
	public void setINTMTH(String iNTMTH) {
		INTMTH = iNTMTH;
	}
	public String getFGSVTYPE() {
		return FGSVTYPE;
	}
	public void setFGSVTYPE(String fGSVTYPE) {
		FGSVTYPE = fGSVTYPE;
	}
	public String getFDPAMT() {
		return FDPAMT;
	}
	public void setFDPAMT(String fDPAMT) {
		FDPAMT = fDPAMT;
	}
	public String getPYDINT() {
		return PYDINT;
	}
	public void setPYDINT(String pYDINT) {
		PYDINT = pYDINT;
	}
	public String getGRSTAX() {
		return GRSTAX;
	}
	public void setGRSTAX(String gRSTAX) {
		GRSTAX = gRSTAX;
	}
	public String getNHITAX() {
		return NHITAX;
	}
	public void setNHITAX(String nHITAX) {
		NHITAX = nHITAX;
	}
	public String getTSFACN() {
		return TSFACN;
	}
	public void setTSFACN(String tsfacn) {
		TSFACN = tsfacn;
	}

	
}
