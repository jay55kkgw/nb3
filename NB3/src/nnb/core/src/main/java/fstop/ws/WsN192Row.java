package fstop.ws;

public class WsN192Row {

	private String _LTD;
	private String _TIME;
	private String _SOURCE;
	private String _BTSFACN;
	private String _ATSFACN;
	private String _BFEEAMT;
	private String _AFEEAMT;
	private String _BQTAMT1;
	private String _AQTAMT1;
	private String _BSTATUS1;
	private String _ASTATUS1;
	private String _BQTAMT2;
	private String _AQTAMT2;
	private String _BSTATUS2;
	private String _ASTATUS2;
	private String _BQTAMT3;
	private String _AQTAMT3;
	private String _BSTATUS3;
	private String _ASTATUS3;
	
	public String getLTD() {
		return _LTD;
	}

	public void setLTD(String LTD) {
		this._LTD = LTD;
	}

	public String getTIME() {
		return _TIME;
	}

	public void setTIME(String TIME) {
		this._TIME = TIME;
	}

	public String getSOURCE() {
		return _SOURCE;
	}

	public void setSOURCE(String SOURCE) {
		this._SOURCE = SOURCE;
	}

	public String getBTSFACN() {
		return _BTSFACN;
	}

	public void setBTSFACN(String BTSFACN) {
		this._BTSFACN = BTSFACN;
	}

	public String getATSFACN() {
		return _ATSFACN;
	}

	public void setATSFACN(String ATSFACN) {
		this._ATSFACN = ATSFACN;
	}

	public String getBFEEAMT() {
		return _BFEEAMT;
	}

	public void setBFEEAMT(String BFEEAMT) {
		this._BFEEAMT = BFEEAMT;
	}

	public String getAFEEAMT() {
		return _AFEEAMT;
	}

	public void setAFEEAMT(String AFEEAMT) {
		this._AFEEAMT = AFEEAMT;
	}

	public String getBQTAMT1() {
		return _BQTAMT1;
	}

	public void setBQTAMT1(String BQTAMT1) {
		this._BQTAMT1 = BQTAMT1;
	}

	public String getAQTAMT1() {
		return _AQTAMT1;
	}

	public void setAQTAMT1(String AQTAMT1) {
		this._AQTAMT1 = AQTAMT1;
	}

	public String getBSTATUS1() {
		return _BSTATUS1;
	}

	public void setBSTATUS1(String BSTATUS1) {
		this._BSTATUS1 = BSTATUS1;
	}

	public String getASTATUS1() {
		return _ASTATUS1;
	}

	public void setASTATUS1(String ASTATUS1) {
		this._ASTATUS1 = ASTATUS1;
	}

	public String getBQTAMT2() {
		return _BQTAMT2;
	}

	public void setBQTAMT2(String BQTAMT2) {
		this._BQTAMT2 = BQTAMT2;
	}

	public String getAQTAMT2() {
		return _AQTAMT2;
	}

	public void setAQTAMT2(String AQTAMT2) {
		this._AQTAMT2 = AQTAMT2;
	}

	public String getBSTATUS2() {
		return _BSTATUS2;
	}

	public void setBSTATUS2(String BSTATUS2) {
		this._BSTATUS2 = BSTATUS2;
	}

	public String getASTATUS2() {
		return _ASTATUS2;
	}

	public void setASTATUS2(String ASTATUS2) {
		this._ASTATUS2 = ASTATUS2;
	}

	public String getBQTAMT3() {
		return _BQTAMT3;
	}

	public void setBQTAMT3(String BQTAMT3) {
		this._BQTAMT3 = BQTAMT3;
	}

	public String getAQTAMT3() {
		return _AQTAMT3;
	}

	public void setAQTAMT3(String AQTAMT3) {
		this._AQTAMT3 = AQTAMT3;
	}

	public String getBSTATUS3() {
		return _BSTATUS3;
	}

	public void setBSTATUS3(String BSTATUS3) {
		this._BSTATUS3 = BSTATUS3;
	}

	public String getASTATUS3() {
		return _ASTATUS3;
	}

	public void setASTATUS3(String ASTATUS3) {
		this._ASTATUS3 = ASTATUS3;
	}

}
