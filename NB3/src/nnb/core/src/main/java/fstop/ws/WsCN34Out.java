package fstop.ws;

import java.io.Serializable;

public class WsCN34Out implements Serializable {
	private static final long serialVersionUID = -8472954511057921293L;
	private String msgCode;
	private String msgName; 
	private String BNKCOD;
	private String BNKNAME;
	private String ACN;
	private String SEQNO;
	private String APPLYDATE;  //申請日期(西元年)
	private String VALDATE;  //密碼有效日期(西元年)
	private String VALTIME;  //密碼有效時間
	private String AMOUNT;   //提款金額
	
	public String getMsgCode() {
		return msgCode;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public String getMsgName() {
		return msgName;
	}

	public void setMsgName(String msgName) {
		this.msgName = msgName;
	}


	public String getBNKCOD() {
		return BNKCOD;
	}

	public void setBNKCOD(String BNKCOD) {
		this.BNKCOD = BNKCOD;
	}

	public String getACN() {
		return ACN;
	}

	public void setACN(String ACN) {
		this.ACN = ACN;
	}

	public String getSEQNO() {
		return SEQNO;
	}

	public void setSEQNO(String SEQNO) {
		this.SEQNO = SEQNO;
	}

	public String getVALDATE() {
		return VALDATE;
	}

	public void setVALDATE(String VALDATE) {
		this.VALDATE = VALDATE;
	}

	public String getVALTIME() {
		return VALTIME;
	}

	public void setVALTIME(String VALTIME) {
		this.VALTIME = VALTIME;
	}

	public String getAMOUNT() {
		return AMOUNT;
	}

	public void setAMOUNT(String AMOUNT) {
		this.AMOUNT = AMOUNT;
	}

	public String getAPPLYDATE() {
		return APPLYDATE;
	}

	public void setAPPLYDATE(String APPLYDATE) {
		this.APPLYDATE = APPLYDATE;
	}

	public String getBNKNAME() {
		return BNKNAME;
	}

	public void setBNKNAME(String BNKNAME) {
		this.BNKNAME = BNKNAME;
	}
}