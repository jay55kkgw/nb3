package fstop.ws;

public class WsBM5000Eout {

	private String _MsgCode;

	private String _MsgName;

	private WsBM5000ERow[] _Table;	

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public WsBM5000ERow[] getTable() {
		return _Table;
	}

	public void setTable(WsBM5000ERow[] Table) {
		this._Table = Table;
	}
	
}
