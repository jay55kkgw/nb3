package fstop.ws;

public class WsN383Row {

	private String _TRADEDATE;// 預約日期
	private String _CREDITNO;// 信託帳號
	private String _TRANSCODE;// 轉出基金代碼
	private String _FUNDLNAME;// 轉出基金名稱
	private String _BILLSENDMODE;// 轉換方式
	private String _UNIT;// 轉出單位
	private String _CRY;// 轉出信託幣別代碼
	private String _ADCCYNAME;// 轉出信託幣別名稱
	private String _FUNDAMT;// 轉出信託金額
	private String _INTRANSCODE;// 轉入基金代碼
	private String _INFUNDLNAME;// 轉入基金名稱
	private String _AMT3;// 轉換手續費
	private String _FCA2;// 基金公司外加手續費
	private String _FCA1;// 補數手續費
	private String _AMT5;// 扣款金額
	private String _OUTACN;// 扣款帳號
	private String _SSLTXNO;// SSL交易序號

	public String getTRADEDATE() {
		return _TRADEDATE;
	}

	public void setTRADEDATE(String tradedate) {
		this._TRADEDATE = tradedate;
	}

	public String getCREDITNO() {
		return _CREDITNO;
	}

	public void setCREDITNO(String creditno) {
		this._CREDITNO = creditno;
	}

	public String getTRANSCODE() {
		return _TRANSCODE;
	}

	public void setTRANSCODE(String transcode) {
		this._TRANSCODE = transcode;
	}

	public String getFUNDLNAME() {
		return _FUNDLNAME;
	}

	public void setFUNDLNAME(String fundlname) {
		this._FUNDLNAME = fundlname;
	}

	public String getBILLSENDMODE() {
		return _BILLSENDMODE;
	}

	public void setBILLSENDMODE(String billsendmode) {
		this._BILLSENDMODE = billsendmode;
	}

	public String getUNIT() {
		return _UNIT;
	}

	public void setUNIT(String unit) {
		this._UNIT = unit;
	}

	public String getCRY() {
		return _CRY;
	}

	public void setCRY(String cry) {
		this._CRY = cry;
	}

	public String getADCCYNAME() {
		return _ADCCYNAME;
	}

	public void setADCCYNAME(String adccyname) {
		this._ADCCYNAME = adccyname;
	}

	public String getFUNDAMT() {
		return _FUNDAMT;
	}

	public void setFUNDAMT(String fundamt) {
		this._FUNDAMT = fundamt;
	}

	public String getINTRANSCODE() {
		return _INTRANSCODE;
	}

	public void setINTRANSCODE(String intranscode) {
		this._INTRANSCODE = intranscode;
	}

	public String getINFUNDLNAME() {
		return _INFUNDLNAME;
	}

	public void setINFUNDLNAME(String infundlname) {
		this._INFUNDLNAME = infundlname;
	}

	public String getAMT3() {
		return _AMT3;
	}

	public void setAMT3(String amt3) {
		this._AMT3 = amt3;
	}

	public String getFCA2() {
		return _FCA2;
	}

	public void setFCA2(String fca2) {
		this._FCA2 = fca2;
	}

	public String getFCA1() {
		return _FCA1;
	}

	public void setFCA1(String fca1) {
		this._FCA1 = fca1;
	}

	public String getAMT5() {
		return _AMT5;
	}

	public void setAMT5(String amt5) {
		this._AMT5 = amt5;
	}

	public String getOUTACN() {
		return _OUTACN;
	}

	public void setOUTACN(String outacn) {
		this._OUTACN = outacn;
	}

	public String getSSLTXNO() {
		return _SSLTXNO;
	}

	public void setSSLTXNO(String ssltxno) {
		this._SSLTXNO = ssltxno;
	}

}
