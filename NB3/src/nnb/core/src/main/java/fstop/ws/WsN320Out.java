package fstop.ws;

public class WsN320Out {
	
	private String _MsgCode;
	
	private String _MsgName;
	
	private String _CMQTIME;

	private String _MSGFLG;

	private WsN320Row[] _Table;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		_MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		_MsgName = msgName;
	}

	public String getCMQTIME() {
		return _CMQTIME;
	}

	public void setCMQTIME(String cmqtime) {
		_CMQTIME = cmqtime;
	}

	public String getMSGFLG() {
		return _MSGFLG;
	}

	public void setMSGFLG(String msgflg) {
		_MSGFLG = msgflg;
	}

	public WsN320Row[] getTable() {
		return _Table;
	}

	public void setTable(WsN320Row[] table) {
		_Table = table;
	}
		
}
