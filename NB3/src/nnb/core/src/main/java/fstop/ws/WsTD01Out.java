package fstop.ws;

public class WsTD01Out {
	
	private String _MsgCode;
	
	private String _MsgName;
	
	private String _CUSNAME;

	private String _CYCLE;
		
	private WsTD01Row[] _Table;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		_MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		_MsgName = msgName;
	}

	public String getCUSNAME() {
		return _CUSNAME;
	}

	public void setCUSNAME(String cusname) {
		_CUSNAME = cusname;
	}

	public String getCYCLE() {
		return _CYCLE;
	}

	public void setCYCLE(String cycle) {
		_CYCLE = cycle;
	}

	public WsTD01Row[] getTable() {
		return _Table;
	}

	public void setTable(WsTD01Row[] table) {
		_Table = table;
	}
	
}
