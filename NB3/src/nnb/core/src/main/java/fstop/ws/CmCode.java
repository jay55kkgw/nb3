package fstop.ws;

public class CmCode {

private String _MsgCode;
	
	private String _MsgName;
	
	private CmCodeRow[] _Table;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public CmCodeRow[] getTable() {
		return _Table;
	}

	public void setTable(CmCodeRow[] table) {
		this._Table = table;
	}
	
}
