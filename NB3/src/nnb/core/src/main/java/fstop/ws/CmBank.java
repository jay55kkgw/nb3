package fstop.ws;

public class CmBank {

	private String _MsgCode;

	private String _MsgName;

	private CmBankRow[] _Table;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public CmBankRow[] getTable() {
		return _Table;
	}

	public void setTable(CmBankRow[] table) {
		this._Table = table;
	}

}
