package fstop.ws;

public class WsN510Row1 {

	private String _ACN;

	private String _CRY;
	  
	private String _AVAILABLE;
	  
	private String _BALANCE;

	public String getACN() {
		return _ACN;
	}

	public void setACN(String acn) {
		this._ACN = acn;
	}

	public String getCRY() {
		return _CRY;
	}

	public void setCRY(String cry) {
		this._CRY = cry;
	}

	public String getAVAILABLE() {
		return _AVAILABLE;
	}

	public void setAVAILABLE(String available) {
		this._AVAILABLE = available;
	}

	public String getBALANCE() {
		return _BALANCE;
	}

	public void setBALANCE(String balance) {
		this._BALANCE = balance;
	}
	
}
