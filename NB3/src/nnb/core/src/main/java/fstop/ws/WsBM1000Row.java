package fstop.ws;

public class WsBM1000Row {

	private String _ADLOID;

	private String _ADLOTYPE;

	private String _ADLONAME;

	private String _ADCITY;

	private String _ADCITYName;
	
	private String _ADAREA;
	
	private String _ADADDR;
	
	private String _ADLATITUDE;

	private String _ADLONGITUDE;

	private String _ADPHONE1;

	private String _ADPHONE2;

	private String _ADPHONE3;

	private String _ADPHOTO;
	
	private String _ADSTART;
	
	private String _ADEND;

	public String getADLOID() {
		return _ADLOID;
	}

	public void setADLOID(String adloid) {
		_ADLOID = adloid;
	}

	public String getADLOTYPE() {
		return _ADLOTYPE;
	}

	public void setADLOTYPE(String adlotype) {
		_ADLOTYPE = adlotype;
	}

	public String getADLONAME() {
		return _ADLONAME;
	}

	public void setADLONAME(String adloname) {
		_ADLONAME = adloname;
	}

	public String getADCITY() {
		return _ADCITY;
	}

	public void setADCITY(String adcity) {
		_ADCITY = adcity;
	}

	public String getADCITYName() {
		return _ADCITYName;
	}

	public void setADCITYName(String adcityname) {
		_ADCITYName = adcityname;
	}
	
	public String getADAREA() {
		return _ADAREA;
	}

	public void setADAREA(String adarea) {
		_ADAREA = adarea;
	}

	public String getADADDR() {
		return _ADADDR;
	}

	public void setADADDR(String adaddr) {
		_ADADDR = adaddr;
	}

	public String getADLATITUDE() {
		return _ADLATITUDE;
	}

	public void setADLATITUDE(String adlatitude) {
		_ADLATITUDE = adlatitude;
	}

	public String getADLONGITUDE() {
		return _ADLONGITUDE;
	}

	public void setADLONGITUDE(String adlongitude) {
		_ADLONGITUDE = adlongitude;
	}

	public String getADPHONE1() {
		return _ADPHONE1;
	}

	public void setADPHONE1(String adphone1) {
		_ADPHONE1 = adphone1;
	}

	public String getADPHONE2() {
		return _ADPHONE2;
	}

	public void setADPHONE2(String adphone2) {
		_ADPHONE2 = adphone2;
	}

	public String getADPHONE3() {
		return _ADPHONE3;
	}

	public void setADPHONE3(String adphone3) {
		_ADPHONE3 = adphone3;
	}

	public String getADPHOTO() {
		return _ADPHOTO;
	}

	public void setADPHOTO(String adphoto) {
		_ADPHOTO = adphoto;
	}

	public String getADSTART() {
		return _ADSTART;
	}

	public void setADSTART(String adstart) {
		_ADSTART = adstart;
	}

	public String getADEND() {
		return _ADEND;
	}

	public void setADEND(String adend) {
		_ADEND = adend;
	}

	
}
