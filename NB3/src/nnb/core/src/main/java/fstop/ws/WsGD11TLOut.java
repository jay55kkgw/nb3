package fstop.ws;

public class WsGD11TLOut {

	private String _MsgCode;// 訊息代碼

	private String _MsgName;// 訊息名稱

	private String _SYSDATE;// 系統日期

	private String _SYSTIME;// 系統時間

	private String _QDATE;// 掛牌日期

	private String _QTIME;// 掛牌時間

	private String _BPRICE;// 賣出價格

	private String _SPRICE;// 買進價格
	
	private String _PRICE1;// 轉換100公克補繳價款

	private String _PRICE2;// 轉換250公克補繳價款

	private String _PRICE3;// 轉換500公克補繳價款

	private String _PRICE4;// 轉換1000公克補繳價款

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getSYSDATE() {
		return _SYSDATE;
	}

	public void setSYSDATE(String sysdate) {
		this._SYSDATE = sysdate;
	}

	public String getSYSTIME() {
		return _SYSTIME;
	}

	public void setSYSTIME(String systime) {
		this._SYSTIME = systime;
	}

	public String getQDATE() {
		return _QDATE;
	}

	public void setQDATE(String qdate) {
		this._QDATE = qdate;
	}

	public String getQTIME() {
		return _QTIME;
	}

	public void setQTIME(String qtime) {
		this._QTIME = qtime;
	}

	public String getBPRICE() {
		return _BPRICE;
	}

	public void setBPRICE(String bprice) {
		this._BPRICE = bprice;
	}

	public String getSPRICE() {
		return _SPRICE;
	}

	public void setSPRICE(String sprice) {
		this._SPRICE = sprice;
	}

	public String getPRICE1() {
		return _PRICE1;
	}

	public void setPRICE1(String price1) {
		this._PRICE1 = price1;
	}
	
	public String getPRICE2() {
		return _PRICE2;
	}

	public void setPRICE2(String price2) {
		this._PRICE2 = price2;
	}

	public String getPRICE3() {
		return _PRICE3;
	}

	public void setPRICE3(String price3) {
		this._PRICE3 = price3;
	}

	public String getPRICE4() {
		return _PRICE4;
	}

	public void setPRICE4(String price4) {
		this._PRICE4 = price4;
	}

}
