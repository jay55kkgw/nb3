package fstop.ws;

public class WsN178Out {

	private String MsgCode;//	訊息代碼
	private String MsgName;//	訊息名稱
	private String CMQTIME;//	交易時間
	private String 	FYACN;//	存單帳號
	private String FDPNUM;//	存單號碼
	private String 	AMTTSF;//	存單金額
	private String CRYNAME;//	幣別
	private String DEPTYPE;//	存款種類
	private String TERM;//	存款期別數值
	private String TYPCOD;//	期別種類
	private String DPISDT_N;//	起存日
	private String DUEDAT_N;//	到期日
	private String ITR;//	利率
	private String INTMTH;//	計息方式
	private String FYTSFAN;//	利息轉入帳號
	private String AMTFDP;//	原存單金額
	private String INT;//	原存單利息
	private String FYTAX;//	代扣利息所得稅
	private String NHITAX;//	健保費
	private String STR_TERM;//	存款期別
	public String getMsgCode() {
		return MsgCode;
	}
	public void setMsgCode(String msgCode) {
		MsgCode = msgCode;
	}
	public String getMsgName() {
		return MsgName;
	}
	public void setMsgName(String msgName) {
		MsgName = msgName;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getFYACN() {
		return FYACN;
	}
	public void setFYACN(String fYACN) {
		FYACN = fYACN;
	}
	public String getFDPNUM() {
		return FDPNUM;
	}
	public void setFDPNUM(String fDPNUM) {
		FDPNUM = fDPNUM;
	}
	public String getAMTTSF() {
		return AMTTSF;
	}
	public void setAMTTSF(String aMTTSF) {
		AMTTSF = aMTTSF;
	}
	public String getCRYNAME() {
		return CRYNAME;
	}
	public void setCRYNAME(String cRYNAME) {
		CRYNAME = cRYNAME;
	}
	public String getDEPTYPE() {
		return DEPTYPE;
	}
	public void setDEPTYPE(String dEPTYPE) {
		DEPTYPE = dEPTYPE;
	}
	public String getTERM() {
		return TERM;
	}
	public void setTERM(String tERM) {
		TERM = tERM;
	}
	public String getTYPCOD() {
		return TYPCOD;
	}
	public void setTYPCOD(String tYPCOD) {
		TYPCOD = tYPCOD;
	}
	public String getDPISDT_N() {
		return DPISDT_N;
	}
	public void setDPISDT_N(String dPISDT_N) {
		DPISDT_N = dPISDT_N;
	}
	public String getDUEDAT_N() {
		return DUEDAT_N;
	}
	public void setDUEDAT_N(String dUEDAT_N) {
		DUEDAT_N = dUEDAT_N;
	}
	public String getITR() {
		return ITR;
	}
	public void setITR(String iTR) {
		ITR = iTR;
	}
	public String getINTMTH() {
		return INTMTH;
	}
	public void setINTMTH(String iNTMTH) {
		INTMTH = iNTMTH;
	}
	public String getFYTSFAN() {
		return FYTSFAN;
	}
	public void setFYTSFAN(String fYTSFAN) {
		FYTSFAN = fYTSFAN;
	}
	public String getAMTFDP() {
		return AMTFDP;
	}
	public void setAMTFDP(String aMTFDP) {
		AMTFDP = aMTFDP;
	}
	public String getINT() {
		return INT;
	}
	public void setINT(String iNT) {
		INT = iNT;
	}
	public String getFYTAX() {
		return FYTAX;
	}
	public void setFYTAX(String fYTAX) {
		FYTAX = fYTAX;
	}
	public String getNHITAX() {
		return NHITAX;
	}
	public void setNHITAX(String nHITAX) {
		NHITAX = nHITAX;
	}
	public String getSTR_TERM() {
		return STR_TERM;
	}
	public void setSTR_TERM(String str_term) {
		STR_TERM = str_term;
	}
	
	

}
