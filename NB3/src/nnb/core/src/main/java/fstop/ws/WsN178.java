package fstop.ws;

public interface WsN178{

	// 取得轉入帳號
	public CmFxAcno getACNList(CmUserQry params);
	//外匯定存單到期明細查詢  執行定存單到期續存前置電文
	public WsN178beforOut beforN178(WsN178beforIn params);
	// 執行外匯定存單到期續存
	public WsN178Out actionN178(WsN178In params);
}