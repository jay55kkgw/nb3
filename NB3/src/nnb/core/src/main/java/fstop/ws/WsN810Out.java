package fstop.ws;

public class WsN810Out {
	
	private String _MsgCode;
	
	private String _MsgName;
	// 查詢時間
	private String _CMQTIME;
	// 信用卡卡別
	private String _CARDTYPE;
	// 信用額度
	private String _CRLIMIT;
	// 預借現金額度
	private String _CSHLIMIT;
	// 目前已使用額度
	private String _ACCBAL;
	// 已動用預借現金
	private String _CSHBAL;
	// 本期循環信用利率
	private String _INT_RATE;
	// 自動扣繳帳號
	private String _PAYMT_ACCT;
	// 扣繳方式
	private String _DBCODE;
	// 應繳總金額
	private String _CURR_BAL;
	// 本期最低金額
	private String _TOTL_DUE;
	// 結帳日
	private String _STMT_DAY;
	// 繳款截止日
	private String _STOP_DAY;
	/*
	 * ex:_Table=[{_TYPENAME=宜蘭大學認同卡鈦金卡, _CARDNUM=5242634994008103, _CR_NAME=王小明, _PT_FLG=1}]}
	 * 信用卡卡別 : _TYPENAME
	 * 信用卡卡號 : _CARDNUM
	 * 持有人名稱 : _CR_NAME
	 * 信用卡附卡 : _PT_FLG
	 */
	private WsN810Row[] _Table;

	
	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		_MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		_MsgName = msgName;
	}

	public String getCMQTIME() {
		return _CMQTIME;
	}

	public void setCMQTIME(String cmqtime) {
		_CMQTIME = cmqtime;
	}

	public String getCARDTYPE() {
		return _CARDTYPE;
	}

	public void setCARDTYPE(String cardtype) {
		_CARDTYPE = cardtype;
	}

	public String getCRLIMIT() {
		return _CRLIMIT;
	}

	public void setCRLIMIT(String crlimit) {
		_CRLIMIT = crlimit;
	}

	public String getCSHLIMIT() {
		return _CSHLIMIT;
	}

	public void setCSHLIMIT(String cshlimit) {
		_CSHLIMIT = cshlimit;
	}

	public String getACCBAL() {
		return _ACCBAL;
	}

	public void setACCBAL(String accbal) {
		_ACCBAL = accbal;
	}

	public String getCSHBAL() {
		return _CSHBAL;
	}

	public void setCSHBAL(String cshbal) {
		_CSHBAL = cshbal;
	}

	public String getINT_RATE() {
		return _INT_RATE;
	}

	public void setINT_RATE(String int_rate) {
		_INT_RATE = int_rate;
	}

	public String getPAYMT_ACCT() {
		return _PAYMT_ACCT;
	}

	public void setPAYMT_ACCT(String paymt_acct) {
		_PAYMT_ACCT = paymt_acct;
	}

	public String getDBCODE() {
		return _DBCODE;
	}

	public void setDBCODE(String dbcode) {
		_DBCODE = dbcode;
	}

	public String getCURR_BAL() {
		return _CURR_BAL;
	}

	public void setCURR_BAL(String curr_bal) {
		_CURR_BAL = curr_bal;
	}

	public String getTOTL_DUE() {
		return _TOTL_DUE;
	}

	public void setTOTL_DUE(String totl_due) {
		_TOTL_DUE = totl_due;
	}

	public String getSTMT_DAY() {
		return _STMT_DAY;
	}

	public void setSTMT_DAY(String stmt_day) {
		_STMT_DAY = stmt_day;
	}

	public String getSTOP_DAY() {
		return _STOP_DAY;
	}

	public void setSTOP_DAY(String stop_day) {
		_STOP_DAY = stop_day;
	}

	public WsN810Row[] getTable() {
		return _Table;
	}

	public void setTable(WsN810Row[] table) {
		_Table = table;
	}
	
}
