package fstop.ws;

public class WsBM5000In {

	private String _ADTYPE;
	
	private String _STTYPE;

	public String getADTYPE() {
		return _ADTYPE;
	}

	public void setADTYPE(String ADTYPE) {
		_ADTYPE = ADTYPE;
	}
	
	public String getSTTYPE() {
		return _STTYPE;
	}

	public void setSTTYPE(String STTYPE) {
		_STTYPE = STTYPE;
	}
}
