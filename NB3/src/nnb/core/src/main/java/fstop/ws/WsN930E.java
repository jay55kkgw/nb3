package fstop.ws;

public interface WsN930E{

	public WsN930EOut getEmailNotify(CmUser params);
	
	public CmMsg action(WsN930EIn params);
}