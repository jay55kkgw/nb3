package fstop.ws;

public interface WsTD01{
	
	public WsTD01CardType getCardType(CmUser params);
	
	public WsTD01Out action(WsTD01In params);
}