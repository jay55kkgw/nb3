package fstop.ws;

public class WsN072Out {

	private String _MsgCode;

	private String _MsgName;

	private String _CMQTIME;

	private String _OUTACN;

	private String _CARDNUM;

	private String _AMOUNT;

	private String _O_TOTBAL;

	private String _O_AVLBAL;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getCMQTIME() {
		return _CMQTIME;
	}

	public void setCMQTIME(String cmqtime) {
		this._CMQTIME = cmqtime;
	}

	public String getOUTACN() {
		return _OUTACN;
	}

	public void setOUTACN(String outacn) {
		this._OUTACN = outacn;
	}

	public String getCARDNUM() {
		return _CARDNUM;
	}

	public void setCARDNUM(String CARDNUM) {
		this._CARDNUM = CARDNUM;
	}

	public String getAMOUNT() {
		return _AMOUNT;
	}

	public void setAMOUNT(String amount) {
		this._AMOUNT = amount;
	}

	public String getO_TOTBAL() {
		return _O_TOTBAL;
	}

	public void setO_TOTBAL(String ototbal) {
		this._O_TOTBAL = ototbal;
	}

	public String getO_AVLBAL() {
		return _O_AVLBAL;
	}

	public void setO_AVLBAL(String oavlbal) {
		this._O_AVLBAL = oavlbal;
	}

}
