package fstop.ws;

public class WsN100Out {

	private String _MsgCode;

	private String _MsgName;

	private String _BRHCOD;

	private String _POSTCOD2;

	private String _CTTADR;

	private String _ARACOD;

	private String _TELNUM;

	private String _ARACOD2;

	private String _TELNUM2;

	private String _MOBTEL;



	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getBRHCOD() {
		return _BRHCOD;
	}

	public void setBRHCOD(String brhcod) {
		this._BRHCOD = brhcod;
	}

	public String getPOSTCOD2() {
		return _POSTCOD2;
	}

	public void setPOSTCOD2(String postcod2) {
		this._POSTCOD2 = postcod2;
	}

	public String getCTTADR() {
		return _CTTADR;
	}

	public void setCTTADR(String cttadr) {
		this._CTTADR = cttadr;
	}

	public String getARACOD() {
		return _ARACOD;
	}

	public void setARACOD(String aracod) {
		this._ARACOD = aracod;
	}

	public String getTELNUM() {
		return _TELNUM;
	}

	public void setTELNUM(String telnum) {
		this._TELNUM = telnum;
	}

	public String getARACOD2() {
		return _ARACOD2;
	}

	public void setARACOD2(String aracod2) {
		this._ARACOD2 = aracod2;
	}

	public String getTELNUM2() {
		return _TELNUM2;
	}

	public void setTELNUM2(String telnum2) {
		this._TELNUM2 = telnum2;
	}

	public String getMOBTEL() {
		return _MOBTEL;
	}

	public void setMOBTEL(String mobtel) {
		this._MOBTEL = mobtel;
	}

}
