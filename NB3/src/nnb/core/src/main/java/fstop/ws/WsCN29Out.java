package fstop.ws;

import java.io.Serializable;

public class WsCN29Out implements Serializable {
	private static final long serialVersionUID = 8321755128207198017L;
	private String msgCode;
	private String msgName;
	private String ENABLE;
	public String getMsgCode() {
		return msgCode;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public String getMsgName() {
		return msgName;
	}

	public void setMsgName(String msgName) {
		this.msgName = msgName;
	} 

	public String getENABLE() {
		return ENABLE;
	}

	public void setENABLE(String ENABLE) {
		this.ENABLE = ENABLE;
	}
}