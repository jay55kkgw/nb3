package fstop.ws;

public class CmFDP {

	private String MsgCode;//	訊息代碼
	private String MsgName;//	訊息名稱
	private CmFDPRow[] Table;//	資料表
	public String getMsgCode() {
		return MsgCode;
	}
	public void setMsgCode(String msgCode) {
		MsgCode = msgCode;
	}
	public String getMsgName() {
		return MsgName;
	}
	public void setMsgName(String msgName) {
		MsgName = msgName;
	}
	public CmFDPRow[] getTable() {
		return Table;
	}
	public void setTable(CmFDPRow[] table) {
		Table = table;
	}

	
}
