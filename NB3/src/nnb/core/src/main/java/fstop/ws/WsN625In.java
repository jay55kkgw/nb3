package fstop.ws;

public class WsN625In {

	private String _SessionId;

	private String _N950PASSWORD;

	private String _ACN;

	private String _FGPERIOD;

	private String _CMSDATE;

	private String _CMEDATE;

	private String _QUERYNEXT;

	private String _BOQUERYNEXT;

	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		_SessionId = sessionId;
	}

	public String getN950PASSWORD() {
		return _N950PASSWORD;
	}

	public void setN950PASSWORD(String n950password) {
		_N950PASSWORD = n950password;
	}

	public String getACN() {
		return _ACN;
	}

	public void setACN(String acn) {
		_ACN = acn;
	}

	public String getFGPERIOD() {
		return _FGPERIOD;
	}

	public void setFGPERIOD(String fgperiod) {
		_FGPERIOD = fgperiod;
	}

	public String getCMSDATE() {
		return _CMSDATE;
	}

	public void setCMSDATE(String cmsdate) {
		_CMSDATE = cmsdate;
	}

	public String getCMEDATE() {
		return _CMEDATE;
	}

	public void setCMEDATE(String cmedate) {
		_CMEDATE = cmedate;
	}

	public String getQUERYNEXT() {
		return _QUERYNEXT;
	}

	public void setQUERYNEXT(String querynext) {
		_QUERYNEXT = querynext;
	}

	public String getBOQUERYNEXT() {
		return _BOQUERYNEXT;
	}

	public void setBOQUERYNEXT(String boquerynext) {
		_BOQUERYNEXT = boquerynext;
	}

}
