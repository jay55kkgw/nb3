package fstop.ws;

public class CmFxCryIn {
	
	private String _SessionId;

	private String _ACNO;
	
	public String getSessionId() {
		return _SessionId;
	}
	public void setSessionId(String sessionid) {
		this._SessionId = sessionid;
	}

	public String getACNO() {
		return _ACNO;
	}
	public void setACNO(String acno) {
		this._ACNO = acno;
	}

}
