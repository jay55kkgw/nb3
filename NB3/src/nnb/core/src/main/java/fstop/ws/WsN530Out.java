package fstop.ws;

public class WsN530Out {

	private String MsgCode;//	訊息代碼
	private String MsgName;//	訊息名稱
	private String CMQTIME;//	查詢時間
	private String Count;//	資料總數
	private String[] TOTAMT;//	總計資料
	private String[] AMTCUID;
	private String[] TOTCOUNT;
	private WsN530Row[] Table;//	表格結果
	public String getMsgCode() {
		return MsgCode;
	}
	public void setMsgCode(String msgCode) {
		MsgCode = msgCode;
	}
	public String getMsgName() {
		return MsgName;
	}
	public void setMsgName(String msgName) {
		MsgName = msgName;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getCount() {
		return Count;
	}
	public void setCount(String count) {
		Count = count;
	}
	public WsN530Row[] getTable() {
		return Table;
	}
	public void setTable(WsN530Row[] table) {
		Table = table;
	}
	public String[] getTOTAMT() {
		return TOTAMT;
	}
	public void setTOTAMT(String[] totamt) {
		TOTAMT = totamt;
	}
	public String[] getAMTCUID() {
		return AMTCUID;
	}
	public void setAMTCUID(String[] amtcuid) {
		AMTCUID = amtcuid;
	}
	public String[] getTOTCOUNT() {
		return TOTCOUNT;
	}
	public void setTOTCOUNT(String[] totcount) {
		TOTCOUNT = totcount;
	}


	
}
