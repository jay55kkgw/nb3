package fstop.ws;

public class CmFxAcno {
	
	private String _MsgCode;

	private String _MsgName;

	private CmFxAcnoRow[] _Table;
	
	public String getMsgCode() {
		return _MsgCode;
	}
	public void setMsgCode(String msgcode) {
		this._MsgCode = msgcode;
	}

	public String getMsgName() {
		return _MsgName;
	}
	public void setMsgName(String msgname) {
		this._MsgName = msgname;
	}

	public CmFxAcnoRow[] getTable() {
		return _Table;
	}
	public void setTable(CmFxAcnoRow[] table) {
		this._Table = table;
	}

}
