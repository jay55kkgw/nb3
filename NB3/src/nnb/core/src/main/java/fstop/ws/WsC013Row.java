package fstop.ws;

public class WsC013Row {

	private String _FUNDLNAME;// 基金名稱
	private String _TRADEDATE;// 交易日期
	private String _TR106;// 投資方式
	private String _FUNDTYPE;// 交易類別
	private String _ADCCYNAME;// 信託/除息金額幣別
	private String _FUNDAMT;// 信託/除息金額
	private String _UNIT;// 單位數
	private String _PRICE1;// 淨值
	private String _EXRATE;// 匯率
	private String _FEECRYADCCYNAME;// 相關費用合計(幣別)
	private String _FEEAMT;// 相關費用合計
	private String _PAYACN;// 存款帳號/
	private String _INTRANSCODEFUNDLNAME;// 轉入基金
	private String _TXUNIT;// 轉入單位數
	private String _INTRANSCRYADCCYNAME;// 轉入價格(幣別)
	private String _PRICE2;// 轉入價格

	public String getFUNDLNAME() {
		return _FUNDLNAME;
	}

	public void setFUNDLNAME(String fundlname) {
		this._FUNDLNAME = fundlname;
	}

	public String getTRADEDATE() {
		return _TRADEDATE;
	}

	public void setTRADEDATE(String tradedate) {
		this._TRADEDATE = tradedate;
	}

	public String getTR106() {
		return _TR106;
	}

	public void setTR106(String tr106) {
		this._TR106 = tr106;
	}

	public String getFUNDTYPE() {
		return _FUNDTYPE;
	}

	public void setFUNDTYPE(String fundtype) {
		this._FUNDTYPE = fundtype;
	}

	public String getADCCYNAME() {
		return _ADCCYNAME;
	}

	public void setADCCYNAME(String adccyname) {
		this._ADCCYNAME = adccyname;
	}

	public String getFUNDAMT() {
		return _FUNDAMT;
	}

	public void setFUNDAMT(String fundamt) {
		this._FUNDAMT = fundamt;
	}

	public String getUNIT() {
		return _UNIT;
	}

	public void setUNIT(String unit) {
		this._UNIT = unit;
	}

	public String getPRICE1() {
		return _PRICE1;
	}

	public void setPRICE1(String price1) {
		this._PRICE1 = price1;
	}

	public String getEXRATE() {
		return _EXRATE;
	}

	public void setEXRATE(String exrate) {
		this._EXRATE = exrate;
	}

	public String getFEECRYADCCYNAME() {
		return _FEECRYADCCYNAME;
	}

	public void setFEECRYADCCYNAME(String feecryadccyname) {
		this._FEECRYADCCYNAME = feecryadccyname;
	}

	public String getFEEAMT() {
		return _FEEAMT;
	}

	public void setFEEAMT(String feeamt) {
		this._FEEAMT = feeamt;
	}

	public String getPAYACN() {
		return _PAYACN;
	}

	public void setPAYACN(String payacn) {
		this._PAYACN = payacn;
	}

	public String getINTRANSCODEFUNDLNAME() {
		return _INTRANSCODEFUNDLNAME;
	}

	public void setINTRANSCODEFUNDLNAME(String intranscodefundlname) {
		this._INTRANSCODEFUNDLNAME = intranscodefundlname;
	}

	public String getTXUNIT() {
		return _TXUNIT;
	}

	public void setTXUNIT(String txunit) {
		this._TXUNIT = txunit;
	}

	public String getINTRANSCRYADCCYNAME() {
		return _INTRANSCRYADCCYNAME;
	}

	public void setINTRANSCRYADCCYNAME(String intranscryadccyname) {
		this._INTRANSCRYADCCYNAME = intranscryadccyname;
	}

	public String getPRICE2() {
		return _PRICE2;
	}

	public void setPRICE2(String price2) {
		this._PRICE2 = price2;
	}

}
