package fstop.ws;

public class CmNotifySettingOut {
	
	private String MsgCode;

	private String MsgName;
	
	private String NOTIFYAD;
	
	private String NOTIFYTRANS;

	
	public String getMsgCode() {
		return MsgCode;
	}

	public void setMsgCode(String msgCode) {
		MsgCode = msgCode;
	}

	public String getMsgName() {
		return MsgName;
	}

	public void setMsgName(String msgName) {
		MsgName = msgName;
	}

	public String getNOTIFYAD() {
		return NOTIFYAD;
	}

	public void setNOTIFYAD(String notifyad) {
		NOTIFYAD = notifyad;
	}

	public String getNOTIFYTRANS() {
		return NOTIFYTRANS;
	}

	public void setNOTIFYTRANS(String notifytrans) {
		NOTIFYTRANS = notifytrans;
	}	

}
