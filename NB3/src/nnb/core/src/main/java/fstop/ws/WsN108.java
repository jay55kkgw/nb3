package fstop.ws;

public interface WsN108{

	public CmList getAcnList(CmUser param);
	
	public WsN108Out action(WsN108In param);
}