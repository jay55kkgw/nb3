package fstop.ws;

public interface WsBM3000Marquee{

	public WsBM3000MarqueeOut action();

	public WsBM3000MarqueeContent getContent(WsBM3000MarqueeIn marqueeIn);
}
