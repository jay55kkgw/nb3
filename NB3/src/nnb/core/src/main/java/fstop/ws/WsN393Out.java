package fstop.ws;

public class WsN393Out {

	private String _MsgCode;// 訊息代碼
	private String _MsgName;// 訊息名稱
	private String _CMQTIME;// 交易時間
	private String _TRADEDATE;// 預約日期
	private String _CRY;// 轉出信託幣別代碼
	private String _ADCCYNAME;// 轉出信託幣別名稱
	private String _CREDITNO;// 信託帳號
	private String _UNIT;// 轉出單位
	private String _FUNDAMT;// 轉出信託金額
	private String _AMT3;// 轉換手續費
	private String _FCA2;// 基金公司外加手續費
	private String _FCA1;// 補數手續費
	private String _AMT5;// 扣款金額
	private String _OUTACN;// 扣款帳號
	private String _FDTXTYPE;// 交易種類

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getCMQTIME() {
		return _CMQTIME;
	}

	public void setCMQTIME(String cmqtime) {
		this._CMQTIME = cmqtime;
	}

	public String getTRADEDATE() {
		return _TRADEDATE;
	}

	public void setTRADEDATE(String tradedate) {
		this._TRADEDATE = tradedate;
	}

	public String getCRY() {
		return _CRY;
	}

	public void setCRY(String cry) {
		this._CRY = cry;
	}

	public String getADCCYNAME() {
		return _ADCCYNAME;
	}

	public void setADCCYNAME(String adccyname) {
		this._ADCCYNAME = adccyname;
	}

	public String getCREDITNO() {
		return _CREDITNO;
	}

	public void setCREDITNO(String creditno) {
		this._CREDITNO = creditno;
	}

	public String getUNIT() {
		return _UNIT;
	}

	public void setUNIT(String unit) {
		this._UNIT = unit;
	}

	public String getFUNDAMT() {
		return _FUNDAMT;
	}

	public void setFUNDAMT(String fundamt) {
		this._FUNDAMT = fundamt;
	}

	public String getAMT3() {
		return _AMT3;
	}

	public void setAMT3(String amt3) {
		this._AMT3 = amt3;
	}

	public String getFCA2() {
		return _FCA2;
	}

	public void setFCA2(String fca2) {
		this._FCA2 = fca2;
	}

	public String getFCA1() {
		return _FCA1;
	}

	public void setFCA1(String fca1) {
		this._FCA1 = fca1;
	}

	public String getAMT5() {
		return _AMT5;
	}

	public void setAMT5(String amt5) {
		this._AMT5 = amt5;
	}

	public String getOUTACN() {
		return _OUTACN;
	}

	public void setOUTACN(String outacn) {
		this._OUTACN = outacn;
	}

	public String getFDTXTYPE() {
		return _FDTXTYPE;
	}

	public void setFDTXTYPE(String fdtxtype) {
		this._FDTXTYPE = fdtxtype;
	}

}
