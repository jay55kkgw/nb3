package fstop.ws;

public class WsC119In {
	
	private String SessionId;//	身份識別
	private String CUSIDN;//	身分證字號/統一編號
	private String TXTOKEN;//	防止重送代碼
	private String FGTXWAY;//	密碼類別
	private String CMPASSWORD;//	交易密碼
	private String PINNEW;//	交易密碼SHA1值
	private String CREDITNO;//	信託號碼
	private String PAYTAG;//	基金代號
	private String FUNDAMT;//	信託金額
	private String FUNDCUR	;//信託幣別
	private String STOPPROF;//	原自動贖回停利設定
	private String STOPLOSS;//	原自動贖回停損設定
	private String NSTOPLOSS;//	新自動贖回停損設定
	private String NSTOPPROF;//	新自動贖回停利設定
	private String NOOPT;//	不設定註記
	private String AC202;//	信託業務別
	private String CAPTCHA;//圖形驗證碼
	private String MIP;//定期不定額註記
	public String getSessionId() {
		return SessionId;
	}
	public void setSessionId(String sessionId) {
		SessionId = sessionId;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cusidn) {
		CUSIDN = cusidn;
	}
	public String getTXTOKEN() {
		return TXTOKEN;
	}
	public void setTXTOKEN(String txtoken) {
		TXTOKEN = txtoken;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fgtxway) {
		FGTXWAY = fgtxway;
	}
	public String getCMPASSWORD() {
		return CMPASSWORD;
	}
	public void setCMPASSWORD(String cmpassword) {
		CMPASSWORD = cmpassword;
	}
	public String getPINNEW() {
		return PINNEW;
	}
	public void setPINNEW(String pinnew) {
		PINNEW = pinnew;
	}
	public String getCREDITNO() {
		return CREDITNO;
	}
	public void setCREDITNO(String creditno) {
		CREDITNO = creditno;
	}
	public String getPAYTAG() {
		return PAYTAG;
	}
	public void setPAYTAG(String paytag) {
		PAYTAG = paytag;
	}
	public String getFUNDAMT() {
		return FUNDAMT;
	}
	public void setFUNDAMT(String fundamt) {
		FUNDAMT = fundamt;
	}
	public String getFUNDCUR() {
		return FUNDCUR;
	}
	public void setFUNDCUR(String fundcur) {
		FUNDCUR = fundcur;
	}
	public String getSTOPPROF() {
		return STOPPROF;
	}
	public void setSTOPPROF(String stopprof) {
		STOPPROF = stopprof;
	}
	public String getSTOPLOSS() {
		return STOPLOSS;
	}
	public void setSTOPLOSS(String stoploss) {
		STOPLOSS = stoploss;
	}
	public String getNSTOPLOSS() {
		return NSTOPLOSS;
	}
	public void setNSTOPLOSS(String nstoploss) {
		NSTOPLOSS = nstoploss;
	}
	public String getNSTOPPROF() {
		return NSTOPPROF;
	}
	public void setNSTOPPROF(String nstopprof) {
		NSTOPPROF = nstopprof;
	}
	public String getNOOPT() {
		return NOOPT;
	}
	public void setNOOPT(String noopt) {
		NOOPT = noopt;
	}
	public String getAC202() {
		return AC202;
	}
	public void setAC202(String ac202) {
		AC202 = ac202;
	}
	public String getCAPTCHA() {
		return CAPTCHA;
	}
	public void setCAPTCHA(String captcha) {
		CAPTCHA = captcha;
	}
	public String getMIP() {
		return MIP;
	}
	public void setMIP(String mip) {
		MIP = mip;
	}

	

}
