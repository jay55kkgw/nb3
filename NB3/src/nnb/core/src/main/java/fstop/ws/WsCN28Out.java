package fstop.ws;

import java.io.Serializable;
import java.util.List;

public class WsCN28Out implements Serializable {
	private static final long serialVersionUID = -2259395603131282619L;
	private String msgCode;
	private String msgName; 
	private String CUSIDN; 
	private List<String> ACNS;
	public String getMsgCode() {
		return msgCode;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public String getMsgName() {
		return msgName;
	}

	public void setMsgName(String msgName) {
		this.msgName = msgName;
	}



	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String CUSIDN) {
		this.CUSIDN = CUSIDN;
	}

	public List<String> getACNS() {
		return ACNS;
	}

	public void setACNS(List<String> aCNS) {
		ACNS = aCNS;
	}

}