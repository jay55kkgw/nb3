package fstop.ws;

public class WsBM6000Out {

	private String _MsgCode;

	private String _MsgName;

	private String _ADNBSTATU;	

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getADNBSTATU() {
		return _ADNBSTATU;
	}

	public void setADNBSTATU(String ADNBSTATU) {
		this._ADNBSTATU = ADNBSTATU;
	}
	
}
