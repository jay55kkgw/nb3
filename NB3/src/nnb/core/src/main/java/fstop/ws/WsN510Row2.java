package fstop.ws;

public class WsN510Row2 {

	private String _AVBLESUBCRY;
	
	private String _AVBLESUB;
	
	private String _AVASUBAMTRECNUM;

	public String getAVBLESUBCRY() {
		return _AVBLESUBCRY;
	}

	public void setAVBLESUBCRY(String avblesubcry) {
		this._AVBLESUBCRY = avblesubcry;
	}

	public String getAVBLESUB() {
		return _AVBLESUB;
	}

	public void setAVBLESUB(String avblesub) {
		this._AVBLESUB = avblesub;
	}
	
	public String getAVASUBAMTRECNUM() {
		return _AVASUBAMTRECNUM;
	}

	public void setAVASUBAMTRECNUM(String avasubamtrecnum) {
		this._AVASUBAMTRECNUM = avasubamtrecnum;
	}
}
