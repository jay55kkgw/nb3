package fstop.ws;

public class WsN014Out {

	private String _MsgCode;

	private String _MsgName;

	private String _CMQTIME;

	private String _MSGFLG;

	private WsN014Row[] _table;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getCMQTIME() {
		return _CMQTIME;
	}

	public void setCMQTIME(String cmqtime) {
		this._CMQTIME = cmqtime;
	}

	public String getMSGFLG() {
		return _MSGFLG;
	}

	public void setMSGFLG(String msgflg) {
		this._MSGFLG = msgflg;
	}

	public WsN014Row[] gettable() {
		return _table;
	}

	public void settable(WsN014Row[] table) {
		this._table = table;
	}

}
