package fstop.ws;

public class WsN200Row {

	private String _BRHCOD;

	private String _POSTCOD2;

	private String _CTTADR;

	private String _ARACOD;

	private String _TELNUM;

	private String _ARACOD2;

	private String _TELNUM2;

	private String _MOBTEL;

	public String getBRHCOD() {
		return _BRHCOD;
	}

	public void setBRHCOD(String BRHCOD) {
		this._BRHCOD = BRHCOD;
	}

	public String getPOSTCOD2() {
		return _POSTCOD2;
	}

	public void setPOSTCOD2(String POSTCOD2) {
		this._POSTCOD2 = POSTCOD2;
	}

	public String getCTTADR() {
		return _CTTADR;
	}

	public void setCTTADR(String CTTADR) {
		this._CTTADR = CTTADR;
	}

	public String getARACOD() {
		return _ARACOD;
	}

	public void setARACOD(String ARACOD) {
		this._ARACOD = ARACOD;
	}

	public String getTELNUM() {
		return _TELNUM;
	}

	public void setTELNUM(String TELNUM) {
		this._TELNUM = TELNUM;
	}

	public String getARACOD2() {
		return _ARACOD2;
	}

	public void setARACOD2(String ARACOD2) {
		this._ARACOD2 = ARACOD2;
	}

	public String getTELNUM2() {
		return _TELNUM2;
	}

	public void setTELNUM2(String TELNUM2) {
		this._TELNUM2 = TELNUM2;
	}

	public String getMOBTEL() {
		return _MOBTEL;
	}

	public void setMOBTEL(String MOBTEL) {
		this._MOBTEL = MOBTEL;
	}

}
