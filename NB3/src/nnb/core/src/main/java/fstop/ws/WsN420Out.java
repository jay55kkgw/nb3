package fstop.ws;

public class WsN420Out {

	private String MsgCode;//	訊息代碼
	private String MsgName;//	訊息名稱
	private String 	CMQTIME;//	查詢時間
	private String Count;//	資料總數
	private String 	TOTAMT;//	總計金額
	private WsN420Row[] Table;//	表格結果
	public String getMsgCode() {
		return MsgCode;
	}
	public void setMsgCode(String msgCode) {
		MsgCode = msgCode;
	}
	public String getMsgName() {
		return MsgName;
	}
	public void setMsgName(String msgName) {
		MsgName = msgName;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getCount() {
		return Count;
	}
	public void setCount(String count) {
		Count = count;
	}
	public String getTOTAMT() {
		return TOTAMT;
	}
	public void setTOTAMT(String tOTAMT) {
		TOTAMT = tOTAMT;
	}
	public WsN420Row[] getTable() {
		return Table;
	}
	public void setTable(WsN420Row[] table) {
		Table = table;
	}

	
}
