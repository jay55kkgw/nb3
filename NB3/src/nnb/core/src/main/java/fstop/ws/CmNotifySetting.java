package fstop.ws;

public class CmNotifySetting {
	
	private String SessionId;
	
	private String DPUSERID;
	
	private String NOTIFYAD;
	
	private String NOTIFYTRANS;

	public String getDPUSERID() {
		return DPUSERID;
	}

	public void setDPUSERID(String dpuserid) {
		DPUSERID = dpuserid;
	}

	public String getNOTIFYAD() {
		return NOTIFYAD;
	}

	public void setNOTIFYAD(String notifyad) {
		NOTIFYAD = notifyad;
	}

	public String getNOTIFYTRANS() {
		return NOTIFYTRANS;
	}

	public void setNOTIFYTRANS(String notifytrans) {
		NOTIFYTRANS = notifytrans;
	}

	public String getSessionId() {
		return SessionId;
	}

	public void setSessionId(String sessionId) {
		SessionId = sessionId;
	}

}
