package fstop.ws;

public class WsLoginStatus {
	
	private String _CUSIDN;
	
	private String _USERID;
	
	private String _N950PASSWORD;
	
	private String _EBCDICHashPass;
	
	private String _ASCIIHashPass;
	
	private String _APPPOS;
	
	public String getCUSIDN() {
		return _CUSIDN;
	}

	public void setCUSIDN(String CUSIDN) {
		this._CUSIDN = CUSIDN;
	}

	public String getUSERID() {
		return _USERID;
	}

	public void setUSERID(String USERID) {
		this._USERID = USERID;
	}

	public String getN950PASSWORD() {
		return _N950PASSWORD;
	}

	public void setN950PASSWORD(String N950PASSWORD) {
		this._N950PASSWORD = N950PASSWORD;
	}
	
	public String getEBCDICHashPass(){
		return _EBCDICHashPass;
	}
	
	public void setEBCDICHashPass(String EBIDIC_HP){
		_EBCDICHashPass = EBIDIC_HP;
	}
	
	public String getASCIIHashPass(){
		return _ASCIIHashPass;
	}
	
	public void setASCIIHashPass(String ASCII_HP){
		_ASCIIHashPass = ASCII_HP;
	}
	
	public String getAPPPOS(){
		return _APPPOS;
	}
	
	public void setAPPPOS(String APPPOS){
		_APPPOS = APPPOS;
	}
}
