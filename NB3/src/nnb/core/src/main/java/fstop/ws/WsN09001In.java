package fstop.ws;

import java.util.Locale;

public class WsN09001In {

	private String _SessionId;

	private String _TXTOKEN;

	private String _FGTXWAY;

	private String _CMPASSWORD;

	private String _PINNEW;

	private String _OTPKEY;

	private String _SVACN;

	private String _ACN;

	private String _TRNGD;

	private String _PRICE;

	private String _DISPRICE;

	private String _PERDIS;

	private String _TRNFEE;

	private String _TRNAMT;

	private String _DISAMT;
	
	private String _CAPTCHA;	
	
	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		this._SessionId = sessionId;
	}

	public void setTXTOKEN(String TXTOKEN) {
		this._TXTOKEN = TXTOKEN;
	}

	public String getTXTOKEN() {
		return _TXTOKEN;
	}

	public String getACN() {
		return _ACN;
	}

	public void setACN(String ACN) {
		this._ACN = ACN;
	}

	public String getTRNGD() {
		return _TRNGD.toUpperCase(Locale.ENGLISH);
	}

	public void setTRNGD(String TRNGD) {
		this._TRNGD = TRNGD;
	}

	public String getPRICE() {
		return _PRICE.toUpperCase(Locale.ENGLISH);
	}

	public void setPRICE(String PRICE) {
		this._PRICE = PRICE;
	}

	public String getFGTXWAY() {
		return _FGTXWAY;
	}

	public void setFGTXWAY(String fgtxway) {
		this._FGTXWAY = fgtxway;
	}

	public String getCMPASSWORD() {
		return _CMPASSWORD;
	}

	public void setCMPASSWORD(String CMPASSWORD) {
		this._CMPASSWORD = CMPASSWORD;
	}

	public String getPINNEW() {
		return _PINNEW;
	}

	public void setPINNEW(String pinnew) {
		this._PINNEW = pinnew;
	}

	public String getOTPKEY() {
		return _OTPKEY;
	}

	public void setOTPKEY(String otpkey) {
		this._OTPKEY = otpkey;
	}

	public String getSVACN() {
		return _SVACN;
	}

	public void setSVACN(String SVACN) {
		this._SVACN = SVACN;
	}

	public String getPERDIS() {
		return _PERDIS;
	}

	public void setPERDIS(String PERDIS) {
		this._PERDIS = PERDIS;
	}

	public String getTRNFEE() {
		return _TRNFEE;
	}

	public void setTRNFEE(String TRNFEE) {
		this._TRNFEE = TRNFEE;
	}

	public String getTRNAMT() {
		return _TRNAMT;
	}

	public void setTRNAMT(String TRNAMT) {
		this._TRNAMT = TRNAMT;
	}

	public String getDISPRICE() {
		return _DISPRICE;
	}

	public void setDISPRICE(String DISPRICE) {
		this._DISPRICE = DISPRICE;
	}

	public String getDISAMT() {
		return _DISAMT;
	}

	public void setDISAMT(String DISAMT) {
		this._DISAMT = DISAMT;
	}

	public String getCAPTCHA() {
		return _CAPTCHA;
	}

	public void setCAPTCHA(String CAPTCHA) {
		this._CAPTCHA = CAPTCHA;
	}
}
