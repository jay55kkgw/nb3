package fstop.ws;

public interface WsN530{

	public WsN530Out getN530List(CmUserQry params);
	
	public WsN530Out getN177List(CmUserQry params);
	
	public WsN530Out getN178List(CmUserQry params);
}