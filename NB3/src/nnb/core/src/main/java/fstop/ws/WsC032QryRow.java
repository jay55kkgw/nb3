package fstop.ws;

/**
 * @author user
 * 
 */
public class WsC032QryRow {

	private String _CREDITNO;// 信託帳號

	private String _TRANSCODE;// 基金代碼

	private String _FUNDLNAME;// 基金名稱

	private String _CRY;// 信託金額幣別

	private String _ADCCYNAME;// 幣別名稱

	private String _FUNDAMT;// 信託金額

	private String _UNIT;// 單位數

	private String _SHORTTUNIT;// 已達短線之贖回單位數
	
	public String getCREDITNO() {
		return _CREDITNO;
	}

	public void setCREDITNO(String CREDITNO) {
		this._CREDITNO = CREDITNO;
	}
	
	public String getTRANSCODE() {
		return _TRANSCODE;
	}

	public void setTRANSCODE(String TRANSCODE) {
		this._TRANSCODE = TRANSCODE;
	}

	public String getFUNDLNAME() {
		return _FUNDLNAME;
	}

	public void setFUNDLNAME(String FUNDLNAME) {
		this._FUNDLNAME = FUNDLNAME;
	}

	public String getCRY() {
		return _CRY;
	}

	public void setCRY(String cry) {
		this._CRY = cry;
	}

	public String getADCCYNAME() {
		return _ADCCYNAME;
	}

	public void setADCCYNAME(String ADCCYNAME) {
		this._ADCCYNAME = ADCCYNAME;
	}

	public String getFUNDAMT() {
		return _FUNDAMT;
	}

	public void setFUNDAMT(String FUNDAMT) {
		this._FUNDAMT = FUNDAMT;
	}

	public String getUNIT() {
		return _UNIT;
	}

	public void setUNIT(String UNIT) {
		this._UNIT = UNIT;
	}

	public String getSHORTTUNIT() {
		return _SHORTTUNIT;
	}

	public void setSHORTTUNIT(String SHORTTUNIT) {
		this._SHORTTUNIT = SHORTTUNIT;
	}

}
