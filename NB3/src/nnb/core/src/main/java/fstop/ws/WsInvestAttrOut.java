package fstop.ws;

public class WsInvestAttrOut {

	private String _MsgCode;// 訊息代碼
	private String _MsgName;// 訊息名稱
	private String _KYCTYPE;// KYC型態
	private String _FDSCORE;// 總分
	private String _RISK;// 投資屬性

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getKYCTYPE() {
		return _KYCTYPE;
	}

	public void setKYCTYPE(String kyctype) {
		this._KYCTYPE = kyctype;
	}

	public String getFDSCORE() {
		return _FDSCORE;
	}

	public void setFDSCORE(String fdscore) {
		this._FDSCORE = fdscore;
	}

	public String getRISK() {
		return _RISK;
	}

	public void setRISK(String risk) {
		this._RISK = risk;
	}

}
