package fstop.ws;

public class CmBhContactOut {
	
	private String _MsgCode;

	private String _MsgName;

	private CmBhContactOutRow[] _Table;
	
	public String getMsgCode() {
		return _MsgCode;
	}
	public void setMsgCode(String msgcode) {
		this._MsgCode = msgcode;
	}

	public String getMsgName() {
		return _MsgName;
	}
	public void setMsgName(String msgname) {
		this._MsgName = msgname;
	}

	public CmBhContactOutRow[] getTable() {
		return _Table;
	}
	public void setTable(CmBhContactOutRow[] table) {
		this._Table = table;
	}
}
