package fstop.ws;

public interface WsBM3000Merge{

	public WsBM3000MergeOut action();
	
	public WsBM30000MergeContent getContent();
}