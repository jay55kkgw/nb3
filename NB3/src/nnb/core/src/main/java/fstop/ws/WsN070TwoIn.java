package fstop.ws;

public class WsN070TwoIn {

	private String _SessionId;
	//防止重複交易代碼 來自getTxToken
	private String _TXTOKEN;
//	0:交易密碼  1:電子簽章(載具i-key)或 2: 晶片金融卡， 3:otp
	private String _FGTXWAY;
	//交易密碼(明)
	private String _CMPASSWORD;
	//加密後的交易密碼
	private String _PINNEW;

	private String _OTPKEY;

	//轉入帳號種類 1=約定或常用 2=非約定
	private String _FGSVACNO;
	//轉出帳號
	private String _ACN;
	//轉入帳號(約定或常用)
	private String _DPAGACNO;
	//轉入銀行代碼
	private String _DPBHNO;
	//轉入帳號 非約定
	private String _DPACNO;
	//轉帳金額
	private String _AMOUNT;

	private String _INPCST;

	private String _CMTRMEMO;

	private String _CMTRMAIL;

	private String _CMMAILMEMO;
	//防止重複交易代碼 來自getTxToken
	private String _CAPTCHA;
//	轉帳日期類型  1：即時   2：預約單筆  3：預約定期
	private String _FGTXDATE;
//	預約單筆 日期 FGTXDATE=2 時必填
	private String _CMDATE;
//預約定期每月的第幾日 FGTXDATE=3 時必填
	private String _CMDD;
//預約定期 起始時間 FGTXDATE=3 時必填
	private String _CMSDATE;
//預約定期 結束時間 FGTXDATE=3 時必填
	private String _CMEDATE;

	
	
	
	
	

	@Override
	public String toString() {
		return "WsN070TwoIn [_SessionId=" + _SessionId + ", _TXTOKEN=" + _TXTOKEN + ", _FGTXWAY=" + _FGTXWAY
				+  ", _OTPKEY=" + _OTPKEY + ", _FGSVACNO="
				+ _FGSVACNO + ", _ACN=" + _ACN + ", _DPAGACNO=" + _DPAGACNO + ", _DPBHNO=" + _DPBHNO + ", _DPACNO="
				+ _DPACNO + ", _AMOUNT=" + _AMOUNT + ", _INPCST=" + _INPCST + ", _CMTRMEMO=" + _CMTRMEMO
				+ ", _CMTRMAIL=" + _CMTRMAIL + ", _CMMAILMEMO=" + _CMMAILMEMO + ", _CAPTCHA=" + _CAPTCHA
				+ ", _FGTXDATE=" + _FGTXDATE + ", _CMDATE=" + _CMDATE + ", _CMDD=" + _CMDD + ", _CMSDATE=" + _CMSDATE
				+ ", _CMEDATE=" + _CMEDATE + "]";
	}

	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		this._SessionId = sessionId;
	}

	public void setTXTOKEN(String TXTOKEN) {
		this._TXTOKEN = TXTOKEN;
	}

	public String getTXTOKEN() {
		return _TXTOKEN;
	}

	public String getACN() {
		return _ACN;
	}

	public void setACN(String acn) {
		this._ACN = acn;
	}

	public String getDPAGACNO() {
		return _DPAGACNO;
	}

	public void setDPAGACNO(String DPAGACNO) {
		this._DPAGACNO = DPAGACNO;
	}

	public String getDPBHNO() {
		return _DPBHNO;
	}

	public void setDPBHNO(String DPBHNO) {
		this._DPBHNO = DPBHNO;
	}

	public String getFGTXWAY() {
		return _FGTXWAY;
	}

	public void setFGTXWAY(String fgtxway) {
		this._FGTXWAY = fgtxway;
	}

	public String getCMPASSWORD() {
		return _CMPASSWORD;
	}

	public void setCMPASSWORD(String cmpassword) {
		this._CMPASSWORD = cmpassword;
	}

	public String getPINNEW() {
		return _PINNEW;
	}

	public void setPINNEW(String pinnew) {
		this._PINNEW = pinnew;
	}

	public String getOTPKEY() {
		return _OTPKEY;
	}

	public void setOTPKEY(String otpkey) {
		this._OTPKEY = otpkey;
	}

	public String getFGSVACNO() {
		return _FGSVACNO;
	}

	public void setFGSVACNO(String fgsvacno) {
		this._FGSVACNO = fgsvacno;
	}

	public String getDPACNO() {
		return _DPACNO;
	}

	public void setDPACNO(String dpacno) {
		this._DPACNO = dpacno;
	}

	public String getINPCST() {
		return _INPCST;
	}

	public void setINPCST(String inpcst) {
		this._INPCST = inpcst;
	}

	public String getCMTRMEMO() {
		return _CMTRMEMO;
	}

	public void setCMTRMEMO(String cmtrmemo) {
		this._CMTRMEMO = cmtrmemo;
	}

	public String getCMTRMAIL() {
		return _CMTRMAIL;
	}

	public void setCMTRMAIL(String cmtrmail) {
		this._CMTRMAIL = cmtrmail;
	}

	public String getCMMAILMEMO() {
		return _CMMAILMEMO;
	}

	public void setCMMAILMEMO(String cmmailmemo) {
		this._CMMAILMEMO = cmmailmemo;
	}

	public String getAMOUNT() {
		return _AMOUNT;
	}

	public void setAMOUNT(String amount) {
		this._AMOUNT = amount;
	}

	public String getCAPTCHA() {
		return _CAPTCHA;
	}

	public void setCAPTCHA(String captcha) {
		this._CAPTCHA = captcha;
	}
	
	public String getFGTXDATE() {
		return _FGTXDATE;
	}

	public void setFGTXDATE(String fgtxdate) {
		this._FGTXDATE = fgtxdate;
	}

	public String getCMDATE() {
		return _CMDATE;
	}

	public void setCMDATE(String cmdate) {
		this._CMDATE = cmdate;
	}

	public String getCMDD() {
		return _CMDD;
	}

	public void setCMDD(String cmdd) {
		this._CMDD = cmdd;
	}

	public String getCMSDATE() {
		return _CMSDATE;
	}

	public void setCMSDATE(String cmsdate) {
		this._CMSDATE = cmsdate;
	}

	public String getCMEDATE() {
		return _CMEDATE;
	}

	public void setCMEDATE(String cmedate) {
		this._CMEDATE = cmedate;
	}

}
