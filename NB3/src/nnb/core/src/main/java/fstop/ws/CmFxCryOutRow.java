package fstop.ws;

public class CmFxCryOutRow {
	
	private String _ADCURRENCY;

	private String _ADCCYNAME;
	
	public String getADCURRENCY() {
		return _ADCURRENCY;
	}
	public void setADCURRENCY(String adcurrency) {
		this._ADCURRENCY = adcurrency;
	}

	public String getADCCYNAME() {
		return _ADCCYNAME;
	}
	public void setADCCYNAME(String adccyname) {
		this._ADCCYNAME = adccyname;
	}
}
