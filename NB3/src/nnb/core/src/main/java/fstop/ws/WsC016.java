package fstop.ws;

public interface WsC016{

	public WsC016Out actionC016(WsC016In params);

	public WsC025Out actionC025(WsC025In params);

	public WsC022Out actionC022(WsC022In params);

	public WsC031Out actionC031(WsC031In params);

	public WsC032Out actionC032(WsC032In params);
}
