package fstop.ws;

public class CmFxRemitOutRow {
	
	private String _ADKINDID;

	private String _ADRMTITEM;

	private String _ADRMTDESC;
	
	public String getADKINDID() {
		return _ADKINDID;
	}
	public void setADKINDID(String adkindid) {
		this._ADKINDID = adkindid;
	}

	public String getADRMTITEM() {
		return _ADRMTITEM;
	}
	public void setADRMTITEM(String adrmtitem) {
		this._ADRMTITEM = adrmtitem;
	}

	public String getADRMTDESC() {
		return _ADRMTDESC;
	}
	public void setADRMTDESC(String adrmtdesc) {
		this._ADRMTDESC = adrmtdesc;
	}
}
