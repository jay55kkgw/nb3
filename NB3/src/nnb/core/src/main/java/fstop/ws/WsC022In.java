package fstop.ws;

public class WsC022In {

	private String _SessionId;

	private String _N950PASSWORD;

	private String _TRANSCODE;

	private String _CREDITNO;

	private String _INTRANSCODE;

	private String _UNIT;

	private String _BILLSENDMODE;

	private String _FUNDAMT;

	private String _SHORTTUNIT;

	private String _ADTXNO;// 交易編號

	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionid) {
		this._SessionId = sessionid;
	}

	public String getN950PASSWORD() {
		return _N950PASSWORD;
	}

	public void setN950PASSWORD(String n950password) {
		this._N950PASSWORD = n950password;
	}

	public String getTRANSCODE() {
		return _TRANSCODE;
	}

	public void setTRANSCODE(String transcode) {
		this._TRANSCODE = transcode;
	}

	public String getCREDITNO() {
		return _CREDITNO;
	}

	public void setCREDITNO(String creditno) {
		this._CREDITNO = creditno;
	}

	public String getINTRANSCODE() {
		return _INTRANSCODE;
	}

	public void setINTRANSCODE(String intranscode) {
		this._INTRANSCODE = intranscode;
	}

	public String getUNIT() {
		return _UNIT;
	}

	public void setUNIT(String unit) {
		this._UNIT = unit;
	}

	public String getBILLSENDMODE() {
		return _BILLSENDMODE;
	}

	public void setBILLSENDMODE(String billsendmode) {
		this._BILLSENDMODE = billsendmode;
	}

	public String getFUNDAMT() {
		return _FUNDAMT;
	}

	public void setFUNDAMT(String fundamt) {
		this._FUNDAMT = fundamt;
	}

	public String getSHORTTUNIT() {
		return _SHORTTUNIT;
	}

	public void setSHORTTUNIT(String shorttunit) {
		this._SHORTTUNIT = shorttunit;
	}

	public String getADTXNO() {
		return _ADTXNO;
	}

	public void setADTXNO(String adtxno) {
		this._ADTXNO = adtxno;
	}

}
