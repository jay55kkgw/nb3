package fstop.ws;

public class WsN014Row {

	private String _ACN;

	private String _SEQ;

	private String _BAL;

	private String _DATITPY;

	private String _AMTAPY;

	private String _DDT;

	private String _AMTORLN;

	
	public String getACN() {
		return _ACN;
	}

	public void setACN(String acn) {
		this._ACN = acn;
	}

	public String getSEQ() {
		return _SEQ;
	}

	public void setSEQ(String seq) {
		this._SEQ = seq;
	}

	public String getBAL() {
		return _BAL;
	}

	public void setBAL(String bal) {
		this._BAL = bal;
	}

	public String getDATITPY() {
		return _DATITPY;
	}

	public void setDATITPY(String datitpy) {
		this._DATITPY = datitpy;
	}

	public String getAMTAPY() {
		return _AMTAPY;
	}

	public void setAMTAPY(String amtapy) {
		this._AMTAPY = amtapy;
	}

	public String getDDT() {
		return _DDT;
	}

	public void setDDT(String ddt) {
		this._DDT = ddt;
	}

	public String getAMTORLN() {
		return _AMTORLN;
	}

	public void setAMTORLN(String amtorln) {
		this._AMTORLN = amtorln;
	}	
	
}
