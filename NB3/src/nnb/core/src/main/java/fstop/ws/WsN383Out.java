package fstop.ws;

public class WsN383Out {

	private String _MsgCode;// 訊息代碼
	private String _MsgName;// 訊息名稱
	private String _DataCount;// 總計筆數
	private String _Name;// 姓名
	private String _FDTXTYPE;// 交易種類
	private WsN383Row[] _Table;// 表格結果

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getDataCount() {
		return _DataCount;
	}

	public void setDataCount(String dataCount) {
		this._DataCount = dataCount;
	}

	public String getName() {
		return _Name;
	}

	public void setName(String name) {
		this._Name = name;
	}

	public String getFDTXTYPE() {
		return _FDTXTYPE;
	}

	public void setFDTXTYPE(String fdtxtype) {
		this._FDTXTYPE = fdtxtype;
	}

	public WsN383Row[] getTable() {
		return _Table;
	}

	public void setTable(WsN383Row[] table) {
		this._Table = table;
	}

}
