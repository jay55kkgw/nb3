package fstop.ws;

public class CmFundRow {
	
	private String _TRANSCODE;//	基金代碼
	
	private String _FUNDLNAME;//	基金名稱
	
	private String _COUNTRYTYPE;//	國內或國外

	public String getTRANSCODE() {
		return _TRANSCODE;
	}

	public void setTRANSCODE(String transcode) {
		this._TRANSCODE = transcode;
	}

	public String getFUNDLNAME() {
		return _FUNDLNAME;
	}

	public void setFUNDLNAME(String fundlname) {
		this._FUNDLNAME = fundlname;
	}

	public String getCOUNTRYTYPE() {
		return _COUNTRYTYPE;
	}

	public void setCOUNTRYTYPE(String countrytype) {
		this._COUNTRYTYPE = countrytype;
	}
	
	


}
