package fstop.ws;

public class WsBM4000Row {

	private String _DPANNID;

	private String _DPANNTYPE;
	
	private String _DPANNHLK;
	
	private String _DPANNCON;
	
	private String _DPNOTE;

	public String getDPANNID() {
		return _DPANNID;
	}

	public void setDPANNID(String dpannid) {
		this._DPANNID = dpannid;
	}

	public String getDPANNTYPE() {
		return _DPANNTYPE;
	}

	public void setDPANNTYPE(String dpanntype) {
		this._DPANNTYPE = dpanntype;
	}

	public String getDPANNHLK() {
		return _DPANNHLK;
	}

	public void setDPANNHLK(String dpannhlk) {
		this._DPANNHLK = dpannhlk;
	}

	public String getDPANNCON() {
		return _DPANNCON;
	}

	public void setDPANNCON(String dpanncon) {
		this._DPANNCON = dpanncon;
	}

	public String getDPNOTE() {
		return _DPNOTE;
	}

	public void setDPNOTE(String dpnote) {
		this._DPNOTE = dpnote;
	}
	
}
