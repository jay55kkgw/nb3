package fstop.ws;

public class WsN130In {
	
	private String _SessionId;
	
	private String _N950PASSWORD;//交易密碼
	
	private String _ACN;//帳號
	
	private String _FGPERIOD;//查詢時間
	
	private String _CMSDATE;//日期(起)
	
	private String _CMEDATE;//日期(迄)
	
	private String _QUERYNEXT;//繼續查詢key1
	
	private String _BOQUERYNEXT;//繼續查詢Key2

	//no pswd
	@Override
	public String toString()
	{
		return "WsN130In [_SessionId=" + _SessionId + ", _ACN =" + _ACN
				+ ", _FGPERIOD=" + _FGPERIOD + ", _CMSDATE=" + _CMSDATE + ", _CMEDATE=" + _CMEDATE + ", _QUERYNEXT="
				+ _QUERYNEXT + ", _BOQUERYNEXT=" + _BOQUERYNEXT + "]";
	}

	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		_SessionId = sessionId;
	}

	public String getN950PASSWORD() {
		return _N950PASSWORD;
	}

	public void setN950PASSWORD(String n950password) {
		_N950PASSWORD =n950password;
	}

	public String getACN() {
		return _ACN;
	}

	public void setACN(String acn) {
		_ACN = acn;
	}

	public String getFGPERIOD() {
		return _FGPERIOD;
	}

	public void setFGPERIOD(String fgperiod) {
		_FGPERIOD = fgperiod;
	}

	public String getCMSDATE() {
		return _CMSDATE;
	}

	public void setCMSDATE(String cmsdate) {
		_CMSDATE = cmsdate;
	}

	public String getCMEDATE() {
		return _CMEDATE;
	}

	public void setCMEDATE(String cmedate) {
		_CMEDATE =cmedate;
	}

	public String getQUERYNEXT() {
		return _QUERYNEXT;
	}

	public void setQUERYNEXT(String querynext) {
		_QUERYNEXT =querynext;
	}

	public String getBOQUERYNEXT() {
		return _BOQUERYNEXT;
	}

	public void setBOQUERYNEXT(String boquerynext) {
		_BOQUERYNEXT =boquerynext;
	}
}
