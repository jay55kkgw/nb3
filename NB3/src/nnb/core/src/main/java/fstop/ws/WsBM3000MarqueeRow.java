package fstop.ws;

public class WsBM3000MarqueeRow {

	private String _DPADID;

	private String _DPADHLK;

	public String getDPADID() {
		return _DPADID;
	}

	public void setDPADID(String dpadid) {
		this._DPADID = dpadid;
	}

	public String getDPADHLK() {
		return _DPADHLK;
	}

	public void setDPADHLK(String dpadhlk) {
		this._DPADHLK = dpadhlk;
	}
}
