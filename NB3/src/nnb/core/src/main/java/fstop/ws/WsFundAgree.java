package fstop.ws;

public interface WsFundAgree{

	public WsFundAgreeOut checkFundAgreeVersion (CmUser params);
	
	public WsFundAgreeOut getFundAgree (CmUser params);
	
	public CmMsg updateFundAgreeVersion (CmUser params);
}
