package fstop.ws;

public class WsN283TWOut {

	private String _MsgCode;

	private String _MsgName;

	private String _CMQTIME;

	private String _CMRECNUMTW;

	private WsN283TWRow[] _Table;
	
	public String getMsgCode() {
		return _MsgCode;
	}
	public void setMsgCode(String msgcode) {
		this._MsgCode = msgcode;
	}

	public String getMsgName() {
		return _MsgName;
	}
	public void setMsgName(String msgname) {
		this._MsgName = msgname;
	}

	public String getCMQTIME() {
		return _CMQTIME;
	}
	public void setCMQTIME(String cmqtime) {
		this._CMQTIME = cmqtime;
	}

	public String getCMRECNUMTW() {
		return _CMRECNUMTW;
	}
	public void setCMRECNUMTW(String cmrecnumtw) {
		this._CMRECNUMTW = cmrecnumtw;
	}

	public WsN283TWRow[] getTable() {
		return _Table;
	}
	public void setTable(WsN283TWRow[] table) {
		this._Table = table;
	}
}
