package fstop.ws;

import java.io.Serializable;

public class WsCN30In implements Serializable {
	private static final long serialVersionUID = 2360525587712667272L;

	private String sessionId;	//SessionId

	private String CUSIDN;

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String CUSIDN) {
		this.CUSIDN = CUSIDN;
	}
}
