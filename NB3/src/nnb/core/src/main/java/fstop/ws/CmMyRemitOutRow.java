package fstop.ws;

public class CmMyRemitOutRow {
	
	private String _ADRMTID;

	private String _ADRMTTYPE;

	private String _ADRMTITEM;

	private String _ADRMTDESC;
	
	public String getADRMTID() {
		return _ADRMTID;
	}
	public void setADRMTID(String adrmtid) {
		this._ADRMTID = adrmtid;
	}

	public String getADRMTTYPE() {
		return _ADRMTTYPE;
	}
	public void setADRMTTYPE(String adrmttype) {
		this._ADRMTTYPE = adrmttype;
	}

	public String getADRMTITEM() {
		return _ADRMTITEM;
	}
	public void setADRMTITEM(String adrmtitem) {
		this._ADRMTITEM = adrmtitem;
	}

	public String getADRMTDESC() {
		return _ADRMTDESC;
	}
	public void setADRMTDESC(String adrmtdesc) {
		this._ADRMTDESC = adrmtdesc;
	}

}
