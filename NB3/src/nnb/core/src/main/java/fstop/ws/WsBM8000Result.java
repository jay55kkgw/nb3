package fstop.ws;

public class WsBM8000Result {
	
	private String _MsgCode;
	
	private String _MsgName;
	
	private String _Result;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		_MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		_MsgName = msgName;
	}

	public String getResult() {
		return _Result;
	}

	public void setResult(String result) {
		_Result = result;
	}
	
}
