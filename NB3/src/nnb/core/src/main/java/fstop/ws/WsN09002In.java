package fstop.ws;

import java.util.Locale;

public class WsN09002In {

	private String _SessionId;

	private String _TXTOKEN;

	private String _FGTXWAY;

	private String _CMPASSWORD;

	private String _PINNEW;

	private String _OTPKEY;

	private String _SVACN;

	private String _ACN;

	private String _TSFBAL_H;

	private String _GDBAL_H;

	private String _TRNGD;

	private String _SELLFLAG;

	private String _PRICE;

	private String _TRNAMT;

	private String _FEEAMT1;
	
//	private String _FEEAMT2;
	
	private String _TRNAMT2;
	
	private String _CAPTCHA;
	
	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		this._SessionId = sessionId;
	}

	public void setTXTOKEN(String TXTOKEN) {
		this._TXTOKEN = TXTOKEN;
	}

	public String getTXTOKEN() {
		return _TXTOKEN;
	}

	public String getACN() {
		return _ACN;
	}

	public void setACN(String ACN) {
		this._ACN = ACN;
	}

	public String getTSFBAL_H() {
		return _TSFBAL_H.toUpperCase(Locale.ENGLISH);
	}

	public void setTSFBAL_H(String TSFBAL_H) {
		this._TSFBAL_H = TSFBAL_H;
	}

	public String getGDBAL_H() {
		return _GDBAL_H.toUpperCase(Locale.ENGLISH);
	}

	public void setGDBAL_H(String GDBAL_H) {
		this._GDBAL_H = GDBAL_H;
	}

	public String getFGTXWAY() {
		return _FGTXWAY;
	}

	public void setFGTXWAY(String fgtxway) {
		this._FGTXWAY = fgtxway;
	}

	public String getCMPASSWORD() {
		return _CMPASSWORD;
	}

	public void setCMPASSWORD(String CMPASSWORD) {
		this._CMPASSWORD = CMPASSWORD;
	}

	public String getPINNEW() {
		return _PINNEW;
	}

	public void setPINNEW(String pinnew) {
		this._PINNEW = pinnew;
	}

	public String getOTPKEY() {
		return _OTPKEY;
	}

	public void setOTPKEY(String otpkey) {
		this._OTPKEY = otpkey;
	}

	public String getSVACN() {
		return _SVACN;
	}

	public void setSVACN(String SVACN) {
		this._SVACN = SVACN;
	}

	public String getSELLFLAG() {
		return _SELLFLAG;
	}

	public void setSELLFLAG(String SELLFLAG) {
		this._SELLFLAG = SELLFLAG;
	}

	public String getPRICE() {
		return _PRICE;
	}

	public void setPRICE(String PRICE) {
		this._PRICE = PRICE;
	}

	public String getTRNAMT() {
		return _TRNAMT;
	}

	public void setTRNAMT(String TRNAMT) {
		this._TRNAMT = TRNAMT;
	}

	public String getTRNGD() {
		return _TRNGD;
	}

	public void setTRNGD(String TRNGD) {
		this._TRNGD = TRNGD;
	}

	public String getFEEAMT1() {
		return _FEEAMT1;
	}

	public void setFEEAMT1(String FEEAMT1) {
		this._FEEAMT1 = FEEAMT1;
	}

//	public String getFEEAMT2() {
//		return _FEEAMT2;
//	}
//
//	public void setFEEAMT2(String FEEAMT2) {
//		this._FEEAMT2 = FEEAMT2;
//	}
	
	public String getTRNAMT2() {
		return _TRNAMT2;
	}

	public void setTRNAMT2(String TRNAMT2) {
		this._TRNAMT2 = TRNAMT2;
	}
	
	public String getCAPTCHA() {
		return _CAPTCHA;
	}

	public void setCAPTCHA(String CAPTCHA) {
		this._CAPTCHA = CAPTCHA;
	}
}
