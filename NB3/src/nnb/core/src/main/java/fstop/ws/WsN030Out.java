package fstop.ws;

public class WsN030Out {
	private String _MsgCode;

	private String _MsgName;

	private String _DATE;

	private String _TIME;
	
	private String _BOARDDATE;
	
	private String _BOARDTIME;

	private WsN030Row[] _Table;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getDATE() {
		return _DATE;
	}

	public void setDATE(String DATE) {
		this._DATE = DATE;
	}

	public String getBOARDTIME() {
		return _BOARDTIME;
	}

	public void setBOARDTIME(String BOARDTIME) {
		this._BOARDTIME = BOARDTIME;
	}

	public String getBOARDDATE() {
		return _BOARDDATE;
	}

	public void setBOARDDATE(String BOARDDATE) {
		this._BOARDDATE = BOARDDATE;
	}
	
	public String getTIME() {
		return _TIME;
	}

	public void setTIME(String TIME) {
		this._TIME = TIME;
	}
	
	public WsN030Row[] getTable() {
		return _Table;
	}

	public void setTable(WsN030Row[] table) {
		this._Table = table;
	}

}
