package fstop.ws;

public class WsN070ATwoIn {

	private String _SessionId;// 身份識別

	private String _TXTOKEN;// 防止重送代碼

	private String _OTPKEY;// OTP動態密碼

	private String _ACN;// 轉出帳號

	private String _TSFACN;// 銷帳編號

	private String _AMOUNT;// 轉帳金額

	private String _CMTRMEMO;// 交易備註

	private String _CMTRMAIL;// Email信箱

	private String _CMMAILMEMO;// Email摘要內容

	private String _CAPTCHA;// 圖形驗證碼

	private String _FGTXDATE;// 轉帳日期類型

	private String _CMDATE;// 預約日期

	private String _TYPE;// 交易查詢
	
	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionid) {
		this._SessionId = sessionid;
	}

	public String getTXTOKEN() {
		return _TXTOKEN;
	}

	public void setTXTOKEN(String txtoken) {
		this._TXTOKEN = txtoken;
	}

	public String getOTPKEY() {
		return _OTPKEY;
	}

	public void setOTPKEY(String otpkey) {
		this._OTPKEY = otpkey;
	}

	public String getACN() {
		return _ACN;
	}

	public void setACN(String acn) {
		this._ACN = acn;
	}

	public String getTSFACN() {
		return _TSFACN;
	}

	public void setTSFACN(String tsfacn) {
		this._TSFACN = tsfacn;
	}

	public String getAMOUNT() {
		return _AMOUNT;
	}

	public void setAMOUNT(String amount) {
		this._AMOUNT = amount;
	}

	public String getCMTRMEMO() {
		return _CMTRMEMO;
	}

	public void setCMTRMEMO(String cmtrmemo) {
		this._CMTRMEMO = cmtrmemo;
	}

	public String getCMTRMAIL() {
		return _CMTRMAIL;
	}

	public void setCMTRMAIL(String cmtrmail) {
		this._CMTRMAIL = cmtrmail;
	}

	public String getCMMAILMEMO() {
		return _CMMAILMEMO;
	}

	public void setCMMAILMEMO(String cmmailmemo) {
		this._CMMAILMEMO = cmmailmemo;
	}

	public String getCAPTCHA() {
		return _CAPTCHA;
	}

	public void setCAPTCHA(String captcha) {
		this._CAPTCHA = captcha;
	}

	public String getFGTXDATE() {
		return _FGTXDATE;
	}

	public void setFGTXDATE(String fgtxdate) {
		this._FGTXDATE = fgtxdate;
	}

	public String getCMDATE() {
		return _CMDATE;
	}

	public void setCMDATE(String cmdate) {
		this._CMDATE = cmdate;
	}

	public String getTYPE() {
		return this._TYPE;
	}

	public void setTYPE(String type) {
		this._TYPE = type;
	}
}
