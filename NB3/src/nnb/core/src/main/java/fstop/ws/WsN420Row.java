package fstop.ws;

public class WsN420Row {

	private String ACN;//	帳號
	private String TYPE;//	存單種類
	private String FDPNUM;//	存單號碼
	private String 	AMTFDP;//	存單金額
	private String 	ITR;//	利率(%)
	private String DPINTTYPE;//	計息方式
	private String DPISDT;//	起存日
	private String DUEDAT;//	到期日
	private String TSFACN;//	利息轉入帳號
	private String ILAZLFTM;//	自動轉期已轉期數
	private String AUTXFTM;//	自動轉期未轉期數
	private String REMARK;//	備註
	private String TYPE1;//轉存方式
	private String TYPE2;//申請不轉期
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getTYPE() {
		return TYPE;
	}
	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}
	public String getFDPNUM() {
		return FDPNUM;
	}
	public void setFDPNUM(String fDPNUM) {
		FDPNUM = fDPNUM;
	}
	public String getAMTFDP() {
		return AMTFDP;
	}
	public void setAMTFDP(String aMTFDP) {
		AMTFDP = aMTFDP;
	}
	public String getITR() {
		return ITR;
	}
	public void setITR(String iTR) {
		ITR = iTR;
	}
	public String getDPINTTYPE() {
		return DPINTTYPE;
	}
	public void setDPINTTYPE(String dPINTTYPE) {
		DPINTTYPE = dPINTTYPE;
	}
	public String getDPISDT() {
		return DPISDT;
	}
	public void setDPISDT(String dPISDT) {
		DPISDT = dPISDT;
	}
	public String getDUEDAT() {
		return DUEDAT;
	}
	public void setDUEDAT(String dUEDAT) {
		DUEDAT = dUEDAT;
	}
	public String getTSFACN() {
		return TSFACN;
	}
	public void setTSFACN(String tSFACN) {
		TSFACN = tSFACN;
	}
	public String getILAZLFTM() {
		return ILAZLFTM;
	}
	public void setILAZLFTM(String iLAZLFTM) {
		ILAZLFTM = iLAZLFTM;
	}
	public String getAUTXFTM() {
		return AUTXFTM;
	}
	public void setAUTXFTM(String aUTXFTM) {
		AUTXFTM = aUTXFTM;
	}

	public String getTYPE1() {
		return TYPE1;
	}
	public void setTYPE1(String type1) {
		TYPE1 = type1;
	}
	public String getTYPE2() {
		return TYPE2;
	}
	public void setTYPE2(String type2) {
		TYPE2 = type2;
	}
	public String getREMARK() {
		return REMARK;
	}
	public void setREMARK(String remark) {
		REMARK = remark;
	}
	
	

}
