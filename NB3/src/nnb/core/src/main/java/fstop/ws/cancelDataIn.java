package fstop.ws;

public class cancelDataIn {
	
	private String _SessionId;       //身份識別
	private String _N950PASSWORD;	 //交易密碼	
	private String _FGTXWAY;		 //交易機制
	private String _CMPASSWORD;		 //交易密碼
	private String _PINNEW;		 	 //交易密碼SHA1值	
	private String _FDPACN;		 	 //帳號
	private String _FDPTYPE;		 //存款種類
	private String _FDPNUM;		 	 //存單號碼
	private String _AMT;		 	 //存單金額
	private String _INTMTH;		 	 //計息方式
	private String _DPISDT;		 	 //起存日
	private String _DUEDAT;		 	 //到期日	
	private String _ITR;		 	 //利率
	
	// no _N950PASSWORD _CMPASSWORD 
	@Override
	public String toString()
	{
		return "cancelDataIn [_SessionId=" + _SessionId + ", _FGTXWAY=" + _FGTXWAY + ", _FDPACN=" + _FDPACN
				+ ", _FDPTYPE=" + _FDPTYPE + ", _FDPNUM=" + _FDPNUM + ", _AMT=" + _AMT + ", _INTMTH=" + _INTMTH
				+ ", _DPISDT=" + _DPISDT + ", _DUEDAT=" + _DUEDAT + ", _ITR=" + _ITR + "]";
	}
	public String getSessionId() {
		return _SessionId;
	}
	public void setSessionId(String sessionid) {
		this._SessionId = sessionid;
	}

	public String getN950PASSWORD() {
		return _N950PASSWORD;
	}
	public void setN950PASSWORD(String N950PASSWORD) {
		this._N950PASSWORD = N950PASSWORD;
	}
		
	public String getFGTXWAY() {
		return _FGTXWAY;
	}
	public void setFGTXWAY(String FGTXWAY) {
		this._FGTXWAY = FGTXWAY;
	}

	public String getCMPASSWORD() {
		return _CMPASSWORD;
	}
	public void setCMPASSWORD(String CMPASSWORD) {
		this._CMPASSWORD = CMPASSWORD;
	}

	public String getPINNEW() {
		return _PINNEW;
	}
	public void setPINNEW(String PINNEW) {
		this._PINNEW = PINNEW;
	}

	public String getFDPACN() {
		return _FDPACN;
	}
	public void setFDPACN(String FDPACN) {
		this._FDPACN = FDPACN;
	}
	
	public String getFDPTYPE() {
		return _FDPTYPE;
	}
	public void setFDPTYPE(String FDPTYPE) {
		this._FDPTYPE = FDPTYPE;
	}

	public String getFDPNUM() {
		return _FDPNUM;
	}
	public void setFDPNUM(String FDPNUM) {
		this._FDPNUM = FDPNUM;
	}

	public String getAMT() {
		return _AMT;
	}
	public void setAMT(String AMT) {
		this._AMT = AMT;
	}

	public String getINTMTH() {
		return _INTMTH;
	}
	public void setINTMTH(String INTMTH) {
		this._INTMTH = INTMTH;
	}

	public String getDPISDT() {
		return _DPISDT;
	}
	public void setDPISDT(String DPISDT) {
		this._DPISDT = DPISDT;
	}
	
	public String getDUEDAT() {
		return _DUEDAT;
	}
	public void setDUEDAT(String DUEDAT) {
		this._DUEDAT = DUEDAT;
	}

	public String getITR() {
		return _ITR;
	}
	public void setITR(String ITR) {
		this._ITR = ITR;
	}

}
