package fstop.ws;

public class WsN812Row {
	
	private String _CARDNUM;
	
	private String _PT_FLG;
	
	private String _TYPENAME;

	public String getCARDNUM() {
		return _CARDNUM;
	}

	public void setCARDNUM(String cardnum) {
		this._CARDNUM = cardnum;
	}

	public String getPT_FLG() {
		return _PT_FLG;
	}

	public void setPT_FLG(String pt_flg) {
		this._PT_FLG = pt_flg;
	}

	public String getTYPENAME() {
		return _TYPENAME;
	}

	public void setTYPENAME(String typename) {
		this._TYPENAME = typename;
	}
	
	

}
