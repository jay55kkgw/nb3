package fstop.ws;

public class WsN074Out {

	private String _MsgCode;	//訊息代碼	
	private String _MsgName;	//訊息名稱	
	private String _CMTXTIME;	//交易時間
	private String _CMDATE;		//轉帳日期
	private String _OUTACN;		//轉出帳號
	private String _INTSACN;	//轉入帳號
	private String _AMOUNTCUR;	//轉帳金額幣別
	private String _AMOUNT;		//轉帳金額
	private String _FDPNUM;		//存單號碼
	private String _FDPACC;		//存單種類	
	private String _SDT;		//起存日
	private String _DUEDATE;	//到期日	
	private String _INTMTH;		//計息方式	
	private String _ITR;		//利率
	private String _TXTIMES;	//到期轉期
	private String _FGSVTYPE;	//轉存方式
	private String _CMTRMEMO;	//交易備註
	private String _O_TOTBAL;	//轉出帳號帳戶餘額
	private String _O_AVLBAL;	//轉出帳號可用餘額
	
	
	
	
	@Override
	public String toString()
	{
		return "WsN074Out [_MsgCode=" + _MsgCode + ", _MsgName=" + _MsgName + ", _CMTXTIME=" + _CMTXTIME + ", _CMDATE="
				+ _CMDATE + ", _OUTACN=" + _OUTACN + ", _INTSACN=" + _INTSACN + ", _AMOUNTCUR=" + _AMOUNTCUR
				+ ", _AMOUNT=" + _AMOUNT + ", _FDPNUM=" + _FDPNUM + ", _FDPACC=" + _FDPACC + ", _SDT=" + _SDT
				+ ", _DUEDATE=" + _DUEDATE + ", _INTMTH=" + _INTMTH + ", _ITR=" + _ITR + ", _TXTIMES=" + _TXTIMES
				+ ", _FGSVTYPE=" + _FGSVTYPE + ", _CMTRMEMO=" + _CMTRMEMO + ", _O_TOTBAL=" + _O_TOTBAL + ", _O_AVLBAL="
				+ _O_AVLBAL + "]";
	}
	public String getMsgCode() {
		return _MsgCode;
	}
	public void setMsgCode(String msgcode) {
		this._MsgCode = msgcode;
	}

	public String getMsgName() {
		return _MsgName;
	}
	public void setMsgName(String msgname) {
		this._MsgName = msgname;
	}

	public String getCMTXTIME() {
		return _CMTXTIME;
	}
	public void setCMTXTIME(String CMTXTIME) {
		this._CMTXTIME = CMTXTIME;
	}

	public String getCMDATE() {
		return _CMDATE;
	}
	public void setCMDATE(String CMDATE) {
		this._CMDATE = CMDATE;
	}
	
	public String getOUTACN() {
		return _OUTACN;
	}
	public void setOUTACN(String outacn) {
		this._OUTACN = outacn;
	}

	public String getINTSACN() {
		return _INTSACN;
	}
	public void setINTSACN(String INTSACN) {
		this._INTSACN = INTSACN;
	}

	public String getFDPNUM() {
		return _FDPNUM;
	}
	public void setFDPNUM(String FDPNUM) {
		this._FDPNUM = FDPNUM;
	}

	public String getFDPACC() {
		return _FDPACC;
	}
	public void setFDPACC(String FDPACC) {
		this._FDPACC = FDPACC;
	}

	public String getAMOUNTCUR() {
		return _AMOUNTCUR;
	}
	public void setAMOUNTCUR(String AMOUNTCUR) {
		this._AMOUNTCUR = AMOUNTCUR;
	}
	
	public String getAMOUNT() {
		return _AMOUNT;
	}
	public void setAMOUNT(String amount) {
		this._AMOUNT = amount;
	}

	public String getSDT() {
		return _SDT;
	}
	public void setSDT(String SDT) {
		this._SDT = SDT;
	}

	public String getDUEDATE() {
		return _DUEDATE;
	}
	public void setDUEDATE(String DUEDATE) {
		this._DUEDATE = DUEDATE;
	}

	public String getINTMTH() {
		return _INTMTH;
	}
	public void setINTMTH(String INTMTH) {
		this._INTMTH = INTMTH;
	}

	public String getITR() {
		return _ITR;
	}
	public void setITR(String ITR) {
		this._ITR = ITR;
	}

	public String getTXTIMES() {
		return _TXTIMES;
	}
	public void setTXTIMES(String TXTIMES) {
		this._TXTIMES = TXTIMES;
	}

	public String getFGSVTYPE() {
		return _FGSVTYPE;
	}
	public void setFGSVTYPE(String FGSVTYPE) {
		this._FGSVTYPE = FGSVTYPE;
	}
	
	public String getCMTRMEMO() {
		return _CMTRMEMO;
	}
	public void setCMTRMEMO(String CMTRMEMO) {
		this._CMTRMEMO = CMTRMEMO;
	}
	
	public String getO_TOTBAL() {
		return _O_TOTBAL;
	}
	public void setO_TOTBAL(String O_TOTBAL) {
		this._O_TOTBAL = O_TOTBAL;
	}
	
	public String getO_AVLBAL() {
		return _O_AVLBAL;
	}
	public void setO_AVLBAL(String O_AVLBAL) {
		this._O_AVLBAL = O_AVLBAL;
	}
}

