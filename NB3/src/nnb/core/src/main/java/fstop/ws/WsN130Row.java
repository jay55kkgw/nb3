package fstop.ws;

public class WsN130Row {
	
	private String _ACN;//帳號
	
	private String _LSTLTD;//	異動日期
	
	private String _MEMO_C;//	摘要
	
	private String _DPWDAMT;//	支出
	
	private String _DPSVAMT;//	存入
	
	private String _BAL;//餘額
	
	private String _CHKNUM;//票據號碼
	
	private String _DATA16;//資料內容
	
	private String _TRNBRH;//收付行
	
	private String _TRNTIME;//交易時間
	

	public String getACN() {
		return _ACN;
	}

	public void setACN(String acn) {
		_ACN = acn;
	}

	public String getLSTLTD() {
		return _LSTLTD;
	}

	public void setLSTLTD(String lstltd) {
		_LSTLTD = lstltd;
	}

	public String getMEMO_C() {
		return _MEMO_C;
	}

	public void setMEMO_C(String memo_c) {
		_MEMO_C = memo_c;
	}

	public String getDPWDAMT() {
		return _DPWDAMT;
	}

	public void setDPWDAMT(String dpwdamt) {
		_DPWDAMT = dpwdamt;
	}

	public String getDPSVAMT() {
		return _DPSVAMT;
	}

	public void setDPSVAMT(String dpsvamt) {
		_DPSVAMT = dpsvamt;
	}

	public String getBAL() {
		return _BAL;
	}

	public void setBAL(String bal) {
		_BAL = bal;
	}

	public String getCHKNUM() {
		return _CHKNUM;
	}

	public void setCHKNUM(String chknum) {
		_CHKNUM = chknum;
	}

	public String getDATA16() {
		return _DATA16;
	}

	public void setDATA16(String data16) {
		_DATA16 = data16;
	}

	public String getTRNBRH() {
		return _TRNBRH;
	}

	public void setTRNBRH(String trnbrh) {
		_TRNBRH = trnbrh;
	}

	public String getTRNTIME() {
		return _TRNTIME;
	}

	public void setTRNTIME(String trntime) {
		_TRNTIME = trntime;
	}

}
