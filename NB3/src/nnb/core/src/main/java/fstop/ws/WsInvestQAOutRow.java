package fstop.ws;

public class WsInvestQAOutRow {

	private String _QCONTEXT;//	題目內容
	private String _QA;//	答案選項
	private String _QB;//	答案選項
	private String _QC;//	答案選項
	private String _QD;//	答案選項
	private String _QE;//;	答案選項
	private String _QF;//;	答案選項
	private String _ISRESEL	;//是否複選
	public String getQCONTEXT() {
		return _QCONTEXT;
	}
	public void setQCONTEXT(String qcontext) {
		this._QCONTEXT = qcontext;
	}
	public String getQA() {
		return _QA;
	}
	public void setQA(String qa) {
		this._QA = qa;
	}
	public String getQB() {
		return _QB;
	}
	public void setQB(String qb) {
		this._QB = qb;
	}
	public String getQC() {
		return _QC;
	}
	public void setQC(String qc) {
		this._QC = qc;
	}
	public String getQD() {
		return _QD;
	}
	public void setQD(String qd) {
		this._QD = qd;
	}
	public String getQE() {
		return _QE;
	}
	public void setQE(String qe) {
		this._QE = qe;
	}
	public String getQF() {
		return _QF;
	}
	public void setQF(String qf) {
		this._QF = qf;
	}
	public String getISRESEL() {
		return _ISRESEL;
	}
	public void setISRESEL(String isresel) {
		this._ISRESEL = isresel;
	}
	

}
