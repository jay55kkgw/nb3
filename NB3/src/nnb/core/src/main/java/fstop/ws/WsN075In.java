package fstop.ws;

public class WsN075In {
	
	private String _SessionId;

	private String _TXTOKEN;

	private String _OTPKEY;

	private String _OUTACN;

	private String _CMDATE1;

	private String _ELENUM;

	private String _CHKNUM;

	private String _AMOUNT;

	private String _CMTRMEMO;

	private String _CMTRMAIL;

	private String _CMMAILMEMO;

	private String _CAPTCHA;

	private String _FGTXDATE;

	private String _CMDATE;

	public String getSessionId() {
		return _SessionId;
	}
	public void setSessionId(String sessionid) {
		this._SessionId = sessionid;
	}

	public String getTXTOKEN() {
		return _TXTOKEN;
	}
	public void setTXTOKEN(String txtoken) {
		this._TXTOKEN = txtoken;
	}

	public String getOTPKEY() {
		return _OTPKEY;
	}
	public void setOTPKEY(String otpkey) {
		this._OTPKEY = otpkey;
	}

	public String getOUTACN() {
		return _OUTACN;
	}
	public void setOUTACN(String outacn) {
		this._OUTACN = outacn;
	}

	public String getCMDATE1() {
		return _CMDATE1;
	}
	public void setCMDATE1(String cmdate1) {
		this._CMDATE1 = cmdate1;
	}

	public String getELENUM() {
		return _ELENUM;
	}
	public void setELENUM(String elenum) {
		this._ELENUM = elenum;
	}

	public String getCHKNUM() {
		return _CHKNUM;
	}
	public void setCHKNUM(String chknum) {
		this._CHKNUM = chknum;
	}

	public String getAMOUNT() {
		return _AMOUNT;
	}
	public void setAMOUNT(String amount) {
		this._AMOUNT = amount;
	}

	public String getCMTRMEMO() {
		return _CMTRMEMO;
	}
	public void setCMTRMEMO(String cmtrmemo) {
		this._CMTRMEMO = cmtrmemo;
	}

	public String getCMTRMAIL() {
		return _CMTRMAIL;
	}
	public void setCMTRMAIL(String cmtrmail) {
		this._CMTRMAIL = cmtrmail;
	}

	public String getCMMAILMEMO() {
		return _CMMAILMEMO;
	}
	public void setCMMAILMEMO(String cmmailmemo) {
		this._CMMAILMEMO = cmmailmemo;
	}

	public String getCAPTCHA() {
		return _CAPTCHA;
	}
	public void setCAPTCHA(String captcha) {
		this._CAPTCHA = captcha;
	}

	public String getFGTXDATE() {
		return _FGTXDATE;
	}
	public void setFGTXDATE(String fgtxdate) {
		this._FGTXDATE = fgtxdate;
	}

	public String getCMDATE() {
		return _CMDATE;
	}
	public void setCMDATE(String cmdate) {
		this._CMDATE = cmdate;
	}

}
