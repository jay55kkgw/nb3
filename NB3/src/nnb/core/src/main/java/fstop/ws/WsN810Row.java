package fstop.ws;

public class WsN810Row {
	
	private String _TYPENAME;
	
	private String _CARDNUM;
	
	private String _CR_NAME;
	
	private String _PT_FLG;

	public String getTYPENAME() {
		return _TYPENAME;
	}

	public void setTYPENAME(String typename) {
		_TYPENAME = typename;
	}

	public String getCARDNUM() {
		return _CARDNUM;
	}

	public void setCARDNUM(String cardnum) {
		_CARDNUM = cardnum;
	}

	public String getCR_NAME() {
		return _CR_NAME;
	}

	public void setCR_NAME(String cr_name) {
		_CR_NAME = cr_name;
	}

	public String getPT_FLG() {
		return _PT_FLG;
	}

	public void setPT_FLG(String pt_flg) {
		_PT_FLG = pt_flg;
	}		
		
}
