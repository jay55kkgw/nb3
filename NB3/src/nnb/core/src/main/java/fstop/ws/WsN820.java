package fstop.ws;

public interface WsN820{
	
	public WsN820CardType getCardType(CmUser params);
	
	public WsN820Out action(CmUser params);
}