package fstop.ws;

import java.io.Serializable;

public class WsCN31In implements Serializable {
	private static final long serialVersionUID = -6522892258784350218L;
	private String sessionId;	//SessionId
	private String PINBLOCK;
	private String CUSIDN;
	private String CN;

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}


	public String getPINBLOCK() {
		return PINBLOCK;
	}

	public void setPINBLOCK(String pINBLOCK) {
		PINBLOCK = pINBLOCK;
	}

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}

	public String getCN() {
		return CN;
	}

	public void setCN(String cN) {
		CN = cN;
	}

}
