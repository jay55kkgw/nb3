package fstop.ws;

public class WsN174In {

	private String SessionId;//	身份識別
	private String TXTOKEN;//	防止重送代碼
	private String FGTXWAY;//	密碼類別
	private String CMPASSWORD;//交易密碼
	private String PINNEW;//	交易密碼SHA1值
	private String OTPKEY;//	OTP動態密碼
	private String FGINACNO;//	約定/非約定記號
	private String INACNO1;//	約定轉入帳號
	private String FGTRDATE;//	即時/預約記號
	private String CMTRDATE;//	預約單筆轉帳日期
	private String CMSDATE;//	預約週期起日
	private String CMEDATE;//	預約週期迄日
	private String CMPERIOD;//	預約每月轉帳日期
	private String FYTSFAN;//	轉出帳號
	private String OUTCRY;//	轉帳幣別
	private String AMOUNT;//	轉帳金額(整數位)
	private String AMOUNT_DIG;//轉帳金額(小數位)
	private String BGROENO;//	議價編號
	private String CMTRMEMO;//	交易備註
	private String CMTRMAIL	;//	Email信箱
	private String CMMAILMEMO;//Email摘要內容
	private String TYPCOD;//	存款期別
	private String INTMTH;//	計息方式
	private String CODE;//		轉存方式
	private String AUTXFTM;//	到期轉期日
	private String CAPTCHA;//	圖形驗證碼
	private String TRDATE;//	交易日期
	
	//no CMPASSWORD && PINNEW
	@Override
	public String toString()
	{
		return "WsN174In [SessionId=" + SessionId + ", TXTOKEN=" + TXTOKEN + ", FGTXWAY=" + FGTXWAY + ", OTPKEY="
				+ OTPKEY + ", FGINACNO=" + FGINACNO + ", INACNO1=" + INACNO1 + ", FGTRDATE=" + FGTRDATE + ", CMTRDATE="
				+ CMTRDATE + ", CMSDATE=" + CMSDATE + ", CMEDATE=" + CMEDATE + ", CMPERIOD=" + CMPERIOD + ", FYTSFAN="
				+ FYTSFAN + ", OUTCRY=" + OUTCRY + ", AMOUNT=" + AMOUNT + ", AMOUNT_DIG=" + AMOUNT_DIG + ", BGROENO="
				+ BGROENO + ", CMTRMEMO=" + CMTRMEMO + ", CMTRMAIL=" + CMTRMAIL + ", CMMAILMEMO=" + CMMAILMEMO
				+ ", TYPCOD=" + TYPCOD + ", INTMTH=" + INTMTH + ", CODE=" + CODE + ", AUTXFTM=" + AUTXFTM + ", CAPTCHA="
				+ CAPTCHA + ", TRDATE=" + TRDATE + "]";
	}
	public String getSessionId() {
		return SessionId;
	}
	public void setSessionId(String sessionId) {
		SessionId = sessionId;
	}
	public String getTXTOKEN() {
		return TXTOKEN;
	}
	public void setTXTOKEN(String tXTOKEN) {
		TXTOKEN = tXTOKEN;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getCMPASSWORD() {
		return CMPASSWORD;
	}
	public void setCMPASSWORD(String cMPASSWORD) {
		CMPASSWORD = cMPASSWORD;
	}
	public String getPINNEW() {
		return PINNEW;
	}
	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}
	public String getOTPKEY() {
		return OTPKEY;
	}
	public void setOTPKEY(String oTPKEY) {
		OTPKEY = oTPKEY;
	}
	public String getFGINACNO() {
		return FGINACNO;
	}
	public void setFGINACNO(String fGINACNO) {
		FGINACNO = fGINACNO;
	}
	public String getINACNO1() {
		return INACNO1;
	}
	public void setINACNO1(String iNACNO1) {
		INACNO1 = iNACNO1;
	}
	public String getFGTRDATE() {
		return FGTRDATE;
	}
	public void setFGTRDATE(String fGTRDATE) {
		FGTRDATE = fGTRDATE;
	}
	public String getCMTRDATE() {
		return CMTRDATE;
	}
	public void setCMTRDATE(String cMTRDATE) {
		CMTRDATE = cMTRDATE;
	}
	public String getCMSDATE() {
		return CMSDATE;
	}
	public void setCMSDATE(String cMSDATE) {
		CMSDATE = cMSDATE;
	}
	public String getCMEDATE() {
		return CMEDATE;
	}
	public void setCMEDATE(String cMEDATE) {
		CMEDATE = cMEDATE;
	}
	public String getCMPERIOD() {
		return CMPERIOD;
	}
	public void setCMPERIOD(String cMPERIOD) {
		CMPERIOD = cMPERIOD;
	}
	public String getFYTSFAN() {
		return FYTSFAN;
	}
	public void setFYTSFAN(String fYTSFAN) {
		FYTSFAN = fYTSFAN;
	}
	public String getOUTCRY() {
		return OUTCRY;
	}
	public void setOUTCRY(String oUTCRY) {
		OUTCRY = oUTCRY;
	}
	public String getAMOUNT() {
		return AMOUNT;
	}
	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}
	public String getAMOUNT_DIG() {
		return this.AMOUNT_DIG;
	}
	public void setAMOUNT_DIG(String aMOUNT_DIG) {
		this.AMOUNT_DIG = aMOUNT_DIG;
	}	
	public String getBGROENO() {
		return BGROENO;
	}
	public void setBGROENO(String bGROENO) {
		BGROENO = bGROENO;
	}
	public String getCMTRMEMO() {
		return CMTRMEMO;
	}
	public void setCMTRMEMO(String cMTRMEMO) {
		CMTRMEMO = cMTRMEMO;
	}
	public String getCMTRMAIL() {
		return CMTRMAIL;
	}
	public void setCMTRMAIL(String cMTRMAIL) {
		CMTRMAIL = cMTRMAIL;
	}
	public String getCMMAILMEMO() {
		return CMMAILMEMO;
	}
	public void setCMMAILMEMO(String cMMAILMEMO) {
		CMMAILMEMO = cMMAILMEMO;
	}
	public String getTYPCOD() {
		return TYPCOD;
	}
	public void setTYPCOD(String tYPCOD) {
		TYPCOD = tYPCOD;
	}
	public String getINTMTH() {
		return INTMTH;
	}
	public void setINTMTH(String iNTMTH) {
		INTMTH = iNTMTH;
	}
	public String getCODE() {
		return CODE;
	}
	public void setCODE(String cODE) {
		CODE = cODE;
	}
	public String getAUTXFTM() {
		return AUTXFTM;
	}
	public void setAUTXFTM(String aUTXFTM) {
		AUTXFTM = aUTXFTM;
	}
	public String getCAPTCHA() {
		return CAPTCHA;
	}
	public void setCAPTCHA(String cAPTCHA) {
		CAPTCHA = cAPTCHA;
	}
	public String getTRDATE() {
		return TRDATE;
	}
	public void setTRDATE(String trdate) {
		TRDATE = trdate;
	}

	
}
