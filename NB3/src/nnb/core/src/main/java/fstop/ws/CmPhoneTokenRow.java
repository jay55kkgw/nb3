package fstop.ws;

public class CmPhoneTokenRow {
	
	private String SessionId;
	
	private String DPUSERID;
	
	private String PHONETOKEN;
	
	private String PHONETYPE;

	public String getDPUSERID() {
		return DPUSERID;
	}

	public void setDPUSERID(String dpuserid) {
		DPUSERID = dpuserid;
	}

	public String getPHONETOKEN() {
		return PHONETOKEN;
	}

	public void setPHONETOKEN(String phonetoken) {
		PHONETOKEN = phonetoken;
	}

	public String getPHONETYPE() {
		return PHONETYPE;
	}

	public void setPHONETYPE(String phonetype) {
		PHONETYPE = phonetype;
	}

	public String getSessionId() {
		return SessionId;
	}

	public void setSessionId(String sessionId) {
		SessionId = sessionId;
	}

}
