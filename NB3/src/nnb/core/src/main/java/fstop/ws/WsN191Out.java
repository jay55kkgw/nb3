package fstop.ws;

public class WsN191Out {
	
	private String _MsgCode	;//訊息代碼
	
	private String _MsgName;//	訊息名稱
	
	private String _CMQTIME	;//查詢時間
	
	private String _CMPERIOD;//查詢期間
	
	private String _CMRECNUM;//筆數
	
	private String _QUERYNEXT;//	繼續查詢key1
	
	private String _BOQUERYNEXT	;//繼續查詢Key2
	
	private WsN191Row[] _Table;//	表格結果

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getCMQTIME() {
		return _CMQTIME;
	}

	public void setCMQTIME(String cmqtime) {
		this._CMQTIME = cmqtime;
	}

	public String getQUERYNEXT() {
		return _QUERYNEXT;
	}

	public void setQUERYNEXT(String querynext) {
		this._QUERYNEXT = querynext;
	}

	public String getBOQUERYNEXT() {
		return _BOQUERYNEXT;
	}

	public void setBOQUERYNEXT(String boquerynext) {
		this._BOQUERYNEXT = boquerynext;
	}

	public WsN191Row[] getTable() {
		return _Table;
	}

	public void setTable(WsN191Row[] table) {
		this._Table = table;
	}

	public String getCMPERIOD() {
		return _CMPERIOD;
	}

	public void setCMPERIOD(String cmperiod) {
		this._CMPERIOD = cmperiod;
	}

	public String getCMRECNUM() {
		return _CMRECNUM;
	}

	public void setCMRECNUM(String cmrecnum) {
		this._CMRECNUM = cmrecnum;
	}

	
	

}
