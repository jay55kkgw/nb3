package fstop.ws;

public class WsBPIn {
	
	private String _SessionId;	//SessionId
	private String _TXTOKEN;	//防止重送代碼
	private String _OTPKEY;		//OTP動態密碼
	private String _CAPTCHA;	//圖形驗證碼
	private String _FGTXDATE;	//轉帳日期類型
	private String _CMDATE;		//預約日期
	private String _CANCELNO;	//銷帳編號
	private String _AMOUNT;		//繳費金額
	private String _ACN;		//轉出帳號
	private String _CMTRMEMO;	//交易備註
	private String _CMTRMAIL;	//Email信箱
	private String _CMMAILMEMO;	//Email摘要內容
	private String _TYPE;		//交易查詢
	private String CN;
	
	public String getSessionId() {
		return _SessionId;
	}
	public void setSessionId(String sessionid) {
		this._SessionId = sessionid;
	}
	public String getTXTOKEN() {
		return _TXTOKEN;
	}
	public void setTXTOKEN(String txtoken) {
		_TXTOKEN = txtoken;
	}
	public String getOTPKEY() {
		return _OTPKEY;
	}
	public void setOTPKEY(String otpkey) {
		_OTPKEY = otpkey;
	}
	public String getCAPTCHA() {
		return _CAPTCHA;
	}
	public void setCAPTCHA(String captcha) {
		_CAPTCHA = captcha;
	}
	public String getFGTXDATE() {
		return _FGTXDATE;
	}
	public void setFGTXDATE(String fgtxdate) {
		_FGTXDATE = fgtxdate;
	}
	public String getCMDATE() {
		return _CMDATE;
	}
	public void setCMDATE(String cmdate) {
		_CMDATE = cmdate;
	}
	public String getCANCELNO() {
		return _CANCELNO;
	}
	public void setCANCELNO(String cancelno) {
		_CANCELNO = cancelno;
	}
	public String getAMOUNT() {
		return _AMOUNT;
	}
	public void setAMOUNT(String amount) {
		_AMOUNT = amount;
	}
	public String getACN() {
		return _ACN;
	}
	public void setACN(String acn) {
		_ACN = acn;
	}	
	public String getCMTRMEMO() {
		return _CMTRMEMO;
	}
	public void setCMTRMEMO(String cmtrmemo) {
		_CMTRMEMO = cmtrmemo;
	}
	public String getCMTRMAIL() {
		return _CMTRMAIL;
	}
	public void setCMTRMAIL(String cmtrmail) {
		_CMTRMAIL = cmtrmail;
	}
	public String getCMMAILMEMO() {
		return _CMMAILMEMO;
	}
	public void setCMMAILMEMO(String cmmailmemo) {
		_CMMAILMEMO = cmmailmemo;
	}		
	public String getTYPE() {
		return _TYPE;
	}
	public void setTYPE(String type) {
		_TYPE = type;
	}
	public String getCN() {
		return CN;
	}
	public void setCN(String cN) {
		CN = cN;
	}
	
	
}
