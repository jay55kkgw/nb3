package fstop.ws;

public class WsN092Row {

	private String _APPTYPE;
	
	private String _APPDATE;

	private String _APPTIME;

	private String _ACN;

	private String _ACN2;

	private String _TRNGD;
	
	private String _STATUS;
	
	private String _STATUSDESC;

	public String getAPPTYPE() {
		return _APPTYPE;
	}

	public void setAPPTYPE(String APPTYPE) {
		this._APPTYPE = APPTYPE;
	}

	public String getAPPDATE() {
		return _APPDATE;
	}

	public void setAPPDATE(String APPDATE) {
		this._APPDATE = APPDATE;
	}
	
	public String getAPPTIME() {
		return _APPTIME;
	}

	public void setAPPTIME(String APPTIME) {
		this._APPTIME = APPTIME;
	}

	public String getACN() {
		return _ACN;
	}

	public void setACN(String ACN) {
		this._ACN = ACN;
	}

	public String getACN2() {
		return _ACN2;
	}

	public void setACN2(String ACN2) {
		this._ACN2 = ACN2;
	}

	public String getTRNGD() {
		return _TRNGD;
	}

	public void setTRNGD(String TRNGD) {
		this._TRNGD = TRNGD;
	}

	public String getSTATUS() {
		return _STATUS;
	}

	public void setSTATUS(String STATUS) {
		this._STATUS = STATUS;
	}
	
	public String getSTATUSDESC() {
		return _STATUSDESC;
	}

	public void setSTATUSDESC(String STATUSDESC) {
		this._STATUSDESC = STATUSDESC;
	}
}
