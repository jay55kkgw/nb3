package fstop.ws;

public class WsN816LCardList {

	// 訊息代碼
	private String _MsgCode;

	// 訊息名稱
	private String _MsgName;

	// 信用卡數量
	private String _RECNUM;

	private WsN816LRow[] _Table;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getRECNUM() {
		return _RECNUM;
	}

	public void setRECNUM(String recnum) {
		_RECNUM = recnum;
	}

	public WsN816LRow[] getTable() {
		return _Table;
	}

	public void setTable(WsN816LRow[] table) {
		_Table = table;
	}

}
