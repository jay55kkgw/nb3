package fstop.ws;

public class WsN021Row {

	private String _COLOR;

	private String _ACC;

	private String _AMTTYPE;

	private String _ITRFIXED;
	
	private String _ITRCHANGE;
	
	public String getCOLOR() {
		return _COLOR;
	}

	public void setCOLOR(String color) {
		this._COLOR = color;
	}

	public String getACC() {
		return _ACC;
	}

	public void setACC(String acc) {
		this._ACC = acc;
	}

	public String getAMTTYPE() {
		return _AMTTYPE;
	}

	public void setAMTTYPE(String AMTTYPE) {
		this._AMTTYPE = AMTTYPE;
	}

	public String getITRFIXED() {
		return _ITRFIXED;
	}

	public void setITRFIXED(String ITRFIXED) {
		this._ITRFIXED = ITRFIXED;
	}

	public String getITRCHANGE() {
		return _ITRCHANGE;
	}

	public void setITRCHANGE(String ITRCHANGE) {
		this._ITRCHANGE = ITRCHANGE;
	}

}
