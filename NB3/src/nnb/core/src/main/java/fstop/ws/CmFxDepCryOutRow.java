package fstop.ws;

public class CmFxDepCryOutRow {
	
	private String _ADCURRENCY;

	private String _ADCCYNAME;
	
	private String _ADCCYNO;

	public String getADCURRENCY() {
		return _ADCURRENCY;
	}

	public void setADCURRENCY(String ADCURRENCY) {
		this._ADCURRENCY = ADCURRENCY;
	}

	public String getADCCYNAME() {
		return _ADCCYNAME;
	}

	public void setADCCYNAME(String ADCCYNAME) {
		this._ADCCYNAME = ADCCYNAME;
	}

	public String getADCCYNO() {
		return _ADCCYNO;
	}

	public void setADCCYNO(String ADCCYNO) {
		this._ADCCYNO = ADCCYNO;
	}
	
	
}
