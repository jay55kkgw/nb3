package fstop.ws;

public class WsGD11HSRow1 {

	private String _QDATE;// 日期(掛牌日期)

	private String _GDTYPE1;// 黃金存摺規格

	private String _BPRICE1;// 賣出價格

	private String _SPRICE1;// 買進價格

	public String getQDATE() {
		return _QDATE;
	}

	public void setQDATE(String qdate) {
		this._QDATE = qdate;
	}

	public String getGDTYPE1() {
		return _GDTYPE1;
	}

	public void setGDTYPE1(String gdtype1) {
		this._GDTYPE1 = gdtype1;
	}

	public String getBPRICE1() {
		return _BPRICE1;
	}

	public void setBPRICE1(String bprice1) {
		this._BPRICE1 = bprice1;
	}

	public String getSPRICE1() {
		return _SPRICE1;
	}

	public void setSPRICE1(String sprice1) {
		this._SPRICE1 = sprice1;
	}

}
