package fstop.ws;

public class WsN510Out {

	private String _MsgCode;

	private String _MsgName;

	private WsN510Row1[] _Table1;
	
	private WsN510Row2[] _Table2;
	
	private WsN510Row3[] _Table3;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public WsN510Row1[] getTable1() {
		return _Table1;
	}

	public void setTable1(WsN510Row1[] Table1) {
		this._Table1 = Table1;
	}

	public WsN510Row2[] getTable2() {
		return _Table2;
	}

	public void setTable2(WsN510Row2[] Table2) {
		this._Table2 = Table2;
	}
	
	public WsN510Row3[] getTable3() {
		return _Table3;
	}

	public void setTable3(WsN510Row3[] Table3) {
		this._Table3 = Table3;
	}
}
