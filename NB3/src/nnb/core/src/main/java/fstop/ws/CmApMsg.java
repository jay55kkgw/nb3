package fstop.ws;

public class CmApMsg {

	private String _MsgCode;// 訊息代碼

	private String _MsgName;// 訊息名稱

	private String _FUNDAGREEFLAG;// 確認否

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getFUNDAGREEFLAG() {
		return _FUNDAGREEFLAG;
	}

	public void setFUNDAGREEFLAG(String fundagreeflag) {
		this._FUNDAGREEFLAG = fundagreeflag;
	}

}
