package fstop.ws;

public class WsN175SOut {

	private String MsgCode;//	訊息代碼
	private String MsgName;//	訊息名稱
	private String AMT;//		存單金額
	private String CUID;//		幣別
	private String DPISDT;//	存起日
	private String DUEDAT;//	到期日
	private String ACN;//		帳號
	private String FDPNUM;//	存單號碼
	private String INTMTH;//	計息方式
	private String CMTRMEMO	;//	交易備註
	private String TRDATE;//	轉帳日期
	private String CMQTIME;//	交易時間
	
	public String getMsgCode() {
		return MsgCode;
	}
	public void setMsgCode(String msgCode) {
		MsgCode = msgCode;
	}
	public String getMsgName() {
		return MsgName;
	}
	public void setMsgName(String msgName) {
		MsgName = msgName;
	}
	public String getAMT() {
		return AMT;
	}
	public void setAMT(String aMT) {
		AMT = aMT;
	}
	public String getCUID() {
		return CUID;
	}
	public void setCUID(String cUID) {
		CUID = cUID;
	}
	public String getDPISDT() {
		return DPISDT;
	}
	public void setDPISDT(String dPISDT) {
		DPISDT = dPISDT;
	}
	public String getDUEDAT() {
		return DUEDAT;
	}
	public void setDUEDAT(String dUEDAT) {
		DUEDAT = dUEDAT;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getFDPNUM() {
		return FDPNUM;
	}
	public void setFDPNUM(String fDPNUM) {
		FDPNUM = fDPNUM;
	}
	public String getINTMTH() {
		return INTMTH;
	}
	public void setINTMTH(String iNTMTH) {
		INTMTH = iNTMTH;
	}
	public String getCMTRMEMO() {
		return CMTRMEMO;
	}
	public void setCMTRMEMO(String cMTRMEMO) {
		CMTRMEMO = cMTRMEMO;
	}
	public String getTRDATE() {
		return TRDATE;
	}
	public void setTRDATE(String tRDATE) {
		TRDATE = tRDATE;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}

	
}
