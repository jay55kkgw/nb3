package fstop.ws;

public class WsN090In {

	private String _SessionId;

	private String _N950PASSWORD;

	private String _ADOPID;
	
	
	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionId) {
		this._SessionId = sessionId;
	}

	public String getN950PASSWORD() {
		return _N950PASSWORD;
	}

	public void setN950PASSWORD(String N950PASSWORD) {
		this._N950PASSWORD = N950PASSWORD;
	}

	public String getADOPID() {
		return _ADOPID;
	}

	public void setADOPID(String ADOPID) {
		this._ADOPID = ADOPID;
	}
}
