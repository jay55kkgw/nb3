package fstop.ws;

public class WsN110Row {
	
	private String _ACN;
	
	private String _ADPIBAL;
	
	private String _BDPIBAL;
	
	private String _CLR;
	
	public String getACN() {
		return _ACN;
	}

	public void setACN(String ACN) {
		this._ACN = ACN;
	}

	public String getADPIBAL() {
		return _ADPIBAL;
	}

	public void setADPIBAL(String ADPIBAL) {
		this._ADPIBAL = ADPIBAL;
	}

	public String getBDPIBAL() {
		return _BDPIBAL;
	}

	public void setBDPIBAL(String BDPIBAL) {
		this._BDPIBAL = BDPIBAL;
	}
	
	public String getCLR() {
		return _CLR;
	}

	public void setCLR(String CLR) {
		this._CLR = CLR;
	}
}
