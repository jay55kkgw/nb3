package fstop.ws;

public class WsN531Out {

	private String MsgCode;//	訊息代碼
	private String MsgName;//	訊息名稱
	private String CMQTIME;//	查詢時間
	private String Count;//	資料總數
	private WsN531Row[] Table;//	表格結果
	public String getMsgCode() {
		return MsgCode;
	}
	public void setMsgCode(String msgCode) {
		MsgCode = msgCode;
	}
	public String getMsgName() {
		return MsgName;
	}
	public void setMsgName(String msgName) {
		MsgName = msgName;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getCount() {
		return Count;
	}
	public void setCount(String count) {
		Count = count;
	}
	public WsN531Row[] getTable() {
		return Table;
	}
	public void setTable(WsN531Row[] table) {
		Table = table;
	}

	
}
