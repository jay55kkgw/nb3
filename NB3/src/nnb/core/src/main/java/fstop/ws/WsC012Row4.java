package fstop.ws;

public class WsC012Row4 {

	private String _TOTTWDAMT;// 約當台幣信託金額

	private String _TOTREFVALUE;// 總參考市值

	private String _TOTLCYRAT;// 台幣報酬率

	public String getTOTTWDAMT() {
		return _TOTTWDAMT;
	}

	public void setTOTTWDAMT(String tottwdamt) {
		this._TOTTWDAMT = tottwdamt;
	}

	public String getTOTREFVALUE() {
		return _TOTREFVALUE;
	}

	public void setTOTREFVALUE(String totrefvalue) {
		this._TOTREFVALUE = totrefvalue;
	}

	public String getTOTLCYRAT() {
		return _TOTLCYRAT;
	}

	public void setTOTLCYRAT(String totlcyrat) {
		this._TOTLCYRAT = totlcyrat;
	}

}
