package fstop.ws;

public class WsN09002Out {

	private String _MsgCode;

	private String _MsgName;

	private String _CMQTIME;

	private String _SVACN;

	private String _ACN;

	private String _TRNGD;

	private String _PRICE;

	private String _TRNAMT;

	private String _FEEAMT1;

//	private String _FEEAMT2;
	
	private String _TRNAMT2;

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}

	public String getCMQTIME() {
		return _CMQTIME;
	}

	public void setCMQTIME(String cmqtime) {
		this._CMQTIME = cmqtime;
	}

	public String getSVACN() {
		return _SVACN;
	}

	public void setSVACN(String SVACN) {
		this._SVACN = SVACN;
	}

	public String getACN() {
		return _ACN;
	}

	public void setACN(String ACN) {
		this._ACN = ACN;
	}

	public String getTRNGD() {
		return _TRNGD;
	}

	public void setTRNGD(String TRNGD) {
		this._TRNGD = TRNGD;
	}

	public String getPRICE() {
		return _PRICE;
	}

	public void setPRICE(String PRICE) {
		this._PRICE = PRICE;
	}

	public String getFEEAMT1() {
		return _FEEAMT1;
	}

	public void setFEEAMT1(String FEEAMT1) {
		this._FEEAMT1 = FEEAMT1;
	}

	public String getTRNAMT() {
		return _TRNAMT;
	}

	public void setTRNAMT(String TRNAMT) {
		this._TRNAMT = TRNAMT;
	}

//	public String getFEEAMT2() {
//		return _FEEAMT2;
//	}
//
//	public void setFEEAMT2(String FEEAMT2) {
//		this._FEEAMT2 = FEEAMT2;
//	}

	public String getTRNAMT2() {
		return _TRNAMT2;
	}

	public void setTRNAMT2(String TRNAMT2) {
		this._TRNAMT2 = TRNAMT2;
	}
}
