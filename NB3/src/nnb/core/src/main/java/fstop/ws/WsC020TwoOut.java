package fstop.ws;

public class WsC020TwoOut {

	private String _MsgCode;     //訊息代碼
	private String _MsgName;	 //訊息名稱
	private String _CMQTIME;	 //交易時間
	private String _FDINVTYPE;	 //客戶投資屬性
	private String _MIP;		 //是否為定期不定額
	private String _INVTYPE;	 //投資方式
	private String _TYPE;		 //扣款方式
	private String _FUNDLNAME;   //基金名稱
	private String _RISK;		 //商品風險等級
	private String _CUSIDN;		 //身分字號/統一編號
	private String _NAME;		 //姓名
	private String _CUR;		 //申購金額幣別		
	private String _AMT3;		 //申購金額
	private String _FCAFEE;		 //手續費率
	private String _FCACUR;		 //手續費幣別	
	private String _FCA2;		 //手續費
	private String _AMTCUR;		 //每次扣款金額幣別
	private String _AMT5;		 //每次扣款金額
	private String _FUNDACN;	 //扣帳帳號/卡號
	private String _TRADEDATE;	 //申請日期
	private String _PAYDAY1;	 //扣款日期1
	private String _PAYDAY2;	 //扣款日期2
	private String _PAYDAY3;	 //扣款日期3
	private String _PAYDAY4;	 //扣款日期4
	private String _PAYDAY5;	 //扣款日期5
	private String _PAYDAY6;	 //扣款日期6
	private String _PAYDAY7;	 //扣款日期7
	private String _PAYDAY8;	 //扣款日期8
	private String _PAYDAY9;	 //扣款日期9
	private String _DBDATE;	 	 //首次扣款日
	private String _YIELD;		 //停利通知設定
	private String _STOP;		 //停損通知設定
	

	public String getCMQTIME() {
		return _CMQTIME;
	}

	public void setCMQTIME(String cmqtime) {
		this._CMQTIME = cmqtime;
	}

	public String getFDINVTYPE() {
		return _FDINVTYPE;
	}

	public void setFDINVTYPE(String fdinvtype) {
		this._FDINVTYPE = fdinvtype;
	}

	public String getMIP() {
		return _MIP;
	}

	public void setMIP(String MIP) {
		this._MIP = MIP;
	}
	
	public String getINVTYPE() {
		return _INVTYPE;
	}

	public void setINVTYPE(String INVTYPE) {
		this._INVTYPE = INVTYPE;
	}
	
	public String getTYPE() {
		return _TYPE;
	}

	public void setTYPE(String type) {
		this._TYPE = type;
	}

	public String getFUNDLNAME() {
		return _FUNDLNAME;
	}

	public void setFUNDLNAME(String FUNDLNAME) {
		this._FUNDLNAME = FUNDLNAME;
	}
	
	public String getRISK() {
		return _RISK;
	}

	public void setRISK(String RISK) {
		this._RISK = RISK;
	}
	
	public String getCUSIDN() {
		return _CUSIDN;
	}

	public void setCUSIDN(String cusidn) {
		this._CUSIDN = cusidn;
	}

	public String getNAME() {
		return _NAME;
	}

	public void setNAME(String name) {
		this._NAME = name;
	}
	
	public String getCUR() {
		return _CUR;
	}

	public void setCUR(String cur) {
		this._CUR = cur;
	}
	
	public String getAMT3() {
		return _AMT3;
	}

	public void setAMT3(String amt3) {
		this._AMT3 = amt3;
	}

	public String getFCAFEE() {
		return _FCAFEE;
	}

	public void setFCAFEE(String fcafee) {
		this._FCAFEE = fcafee;
	}

	public String getFCACUR() {
		return _FCACUR;
	}

	public void setFCACUR(String FCACUR) {
		this._FCACUR = FCACUR;
	}
	
	public String getFCA2() {
		return _FCA2;
	}

	public void setFCA2(String fca2) {
		this._FCA2 = fca2;
	}

	public String getAMTCUR() {
		return _AMTCUR;
	}

	public void setAMTCUR(String AMTCUR) {
		this._AMTCUR = AMTCUR;
	}
	
	public String getAMT5() {
		return _AMT5;
	}

	public void setAMT5(String amt5) {
		this._AMT5 = amt5;
	}

	public String getFUNDACN() {
		return _FUNDACN;
	}

	public void setFUNDACN(String fundacn) {
		this._FUNDACN = fundacn;
	}

	public String getTRADEDATE() {
		return _TRADEDATE;
	}

	public void setTRADEDATE(String tradedate) {
		this._TRADEDATE = tradedate;
	}

	public String getPAYDAY1() {
		return _PAYDAY1;
	}

	public void setPAYDAY1(String payday1) {
		this._PAYDAY1 = payday1;
	}

	public String getPAYDAY2() {
		return _PAYDAY2;
	}

	public void setPAYDAY2(String payday2) {
		this._PAYDAY2 = payday2;
	}

	public String getPAYDAY3() {
		return _PAYDAY3;
	}

	public void setPAYDAY3(String payday3) {
		this._PAYDAY3 = payday3;
	}

	public String getPAYDAY4() {
		return _PAYDAY4;
	}

	public void setPAYDAY4(String PAYDAY4) {
		this._PAYDAY4 = PAYDAY4;
	}
	
	public String getPAYDAY5() {
		return _PAYDAY5;
	}

	public void setPAYDAY5(String PAYDAY5) {
		this._PAYDAY5 = PAYDAY5;
	}
	
	public String getPAYDAY6() {
		return _PAYDAY6;
	}

	public void setPAYDAY6(String PAYDAY6) {
		this._PAYDAY6 = PAYDAY6;
	}	
	
	public String getPAYDAY7() {
		return _PAYDAY7;
	}

	public void setPAYDAY7(String PAYDAY7) {
		this._PAYDAY7 = PAYDAY7;
	}
	
	public String getPAYDAY8() {
		return _PAYDAY8;
	}

	public void setPAYDAY8(String PAYDAY8) {
		this._PAYDAY8 = PAYDAY8;
	}
	
	public String getPAYDAY9() {
		return _PAYDAY9;
	}

	public void setPAYDAY9(String PAYDAY9) {
		this._PAYDAY9 = PAYDAY9;
	}
	
	public String getDBDATE() {
		return _DBDATE;
	}

	public void setDBDATE(String dbdate) {
		this._DBDATE = dbdate;
	}

	public String getYIELD() {
		return _YIELD;
	}

	public void setYIELD(String yield) {
		this._YIELD = yield;
	}

	public String getSTOP() {
		return _STOP;
	}

	public void setSTOP(String stop) {
		this._STOP = stop;
	}

	public String getMsgCode() {
		return _MsgCode;
	}

	public void setMsgCode(String msgCode) {
		this._MsgCode = msgCode;
	}

	public String getMsgName() {
		return _MsgName;
	}

	public void setMsgName(String msgName) {
		this._MsgName = msgName;
	}	

}
