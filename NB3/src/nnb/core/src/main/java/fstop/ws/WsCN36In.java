package fstop.ws;

import java.io.Serializable;

public class WsCN36In implements Serializable {
	private static final long serialVersionUID = 3341188339994012391L;
	private String sessionId;	//SessionId
	private String CUSIDN;
	private String BNKCOD;
	private String ACN;
	private String SEQNO;
	private String VALDATE;
	private String VALTIME;
	private String AMOUNT;
	private String CN;

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String CUSIDN) {
		this.CUSIDN = CUSIDN;
	}

	public String getBNKCOD() {
		return BNKCOD;
	}

	public void setBNKCOD(String BNKCOD) {
		this.BNKCOD = BNKCOD;
	}

	public String getACN() {
		return ACN;
	}

	public void setACN(String ACN) {
		this.ACN = ACN;
	}

	public String getSEQNO() {
		return SEQNO;
	}

	public void setSEQNO(String SEQNO) {
		this.SEQNO = SEQNO;
	}

	public String getVALDATE() {
		return VALDATE;
	}

	public void setVALDATE(String VALDATE) {
		this.VALDATE = VALDATE;
	}

	public String getVALTIME() {
		return VALTIME;
	}

	public void setVALTIME(String VALTIME) {
		this.VALTIME = VALTIME;
	}

	public String getAMOUNT() {
		return AMOUNT;
	}

	public void setAMOUNT(String AMOUNT) {
		this.AMOUNT = AMOUNT;
	}

	public String getCN() {
		return CN;
	}

	public void setCN(String CN) {
		this.CN = CN;
	}
}
