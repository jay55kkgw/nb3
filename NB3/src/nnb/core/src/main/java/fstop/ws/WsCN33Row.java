package fstop.ws;

import java.io.Serializable;

public class WsCN33Row implements Serializable {
	private static final long serialVersionUID = -7571494084310786912L;
	private String BNKCOD;
	private String ACN;
	private String DPIBAL;
	private String TILING;
	
	public String getBNKCOD() {
		return BNKCOD;
	}

	public void setBNKCOD(String BNKCOD) {
		this.BNKCOD = BNKCOD;
	}

	public String getACN() {
		return ACN;
	}

	public void setACN(String ACN) {
		this.ACN = ACN;
	}

	public String getDPIBAL() {
		return DPIBAL;
	}

	public void setDPIBAL(String DPIBAL) {
		this.DPIBAL = DPIBAL;
	}

	public String getTILING() {
		return TILING;
	}

	public void setTILING(String tILING) {
		TILING = tILING;
	}
	
	
}