package fstop.ws;

public class WsN079In {
	
	private String _SessionId;       //身份識別
	private String _N950PASSWORD;	 //交易密碼
	private String _TXTOKEN;		 //防止重送代碼
	private String _CAPTCHA;		 //圖形驗證碼
	private String _FGTXWAY;		 //交易機制
	private String _CMPASSWORD;		 //交易密碼
	private String _PINNEW;		 	 //交易密碼SHA1值
	private String _OTPKEY;			 //OTP動態密碼
	private String _FDPACN;		 	 //帳號
	private String _FDPTYPE;		 //存款種類
	private String _FDPNUM;		 	 //存單號碼
	private String _AMT;		 	 //存單金額
	private String _INTMTH;		 	 //計息方式
	private String _DPISDT;		 	 //起存日
	private String _DUEDAT;		 	 //到期日
	private String _INTPAY;		 	 //利息
	private String _ITR;		 	 //利率
	private String _TAX;		 	 //所得稅
	private String _INTRCV;		 	 //透支息	
	private String _PAIAFTX;		 //稅後本息
	private String _NHITAX;		 	 //健保費
	private String _CMTRMEMO;		 //交易備註
	private String _CMTRMAIL;		 //Email信箱
	private String _CMMAILMEMO;		 //Email摘要內容
	
	// no _N950PASSWORD , _CMPASSWORD ,_PINNEW
	@Override
	public String toString()
	{
		return "WsN079In [_SessionId=" + _SessionId + ", _TXTOKEN=" + _TXTOKEN + ", _CAPTCHA=" + _CAPTCHA
				+ ", _FGTXWAY=" + _FGTXWAY + ", _OTPKEY=" + _OTPKEY + ", _FDPACN=" + _FDPACN + ", _FDPTYPE=" + _FDPTYPE
				+ ", _FDPNUM=" + _FDPNUM + ", _AMT=" + _AMT + ", _INTMTH=" + _INTMTH + ", _DPISDT=" + _DPISDT
				+ ", _DUEDAT=" + _DUEDAT + ", _INTPAY=" + _INTPAY + ", _ITR=" + _ITR + ", _TAX=" + _TAX + ", _INTRCV="
				+ _INTRCV + ", _PAIAFTX=" + _PAIAFTX + ", _NHITAX=" + _NHITAX + ", _CMTRMEMO=" + _CMTRMEMO
				+ ", _CMTRMAIL=" + _CMTRMAIL + ", _CMMAILMEMO=" + _CMMAILMEMO + "]";
	}
	
	public String getSessionId() {
		return _SessionId;
	}

	public void setSessionId(String sessionid) {
		this._SessionId = sessionid;
	}

	public String getN950PASSWORD() {
		return _N950PASSWORD;
	}
	public void setN950PASSWORD(String N950PASSWORD) {
		this._N950PASSWORD = N950PASSWORD;
	}
	
	public String getTXTOKEN() {
		return _TXTOKEN;
	}
	public void setTXTOKEN(String txtoken) {
		this._TXTOKEN = txtoken;
	}

	public String getCAPTCHA() {
		return _CAPTCHA;
	}
	public void setCAPTCHA(String CAPTCHA) {
		this._CAPTCHA = CAPTCHA;
	}
	
	public String getFGTXWAY() {
		return _FGTXWAY;
	}
	public void setFGTXWAY(String FGTXWAY) {
		this._FGTXWAY = FGTXWAY;
	}

	public String getCMPASSWORD() {
		return _CMPASSWORD;
	}
	public void setCMPASSWORD(String CMPASSWORD) {
		this._CMPASSWORD = CMPASSWORD;
	}

	public String getPINNEW() {
		return _PINNEW;
	}
	public void setPINNEW(String PINNEW) {
		this._PINNEW = PINNEW;
	}

	public String getOTPKEY() {
		return _OTPKEY;
	}
	public void setOTPKEY(String OTPKEY) {
		this._OTPKEY = OTPKEY;
	}

	public String getFDPACN() {
		return _FDPACN;
	}
	public void setFDPACN(String FDPACN) {
		this._FDPACN = FDPACN;
	}
	
	public String getFDPTYPE() {
		return _FDPTYPE;
	}
	public void setFDPTYPE(String FDPTYPE) {
		this._FDPTYPE = FDPTYPE;
	}

	public String getFDPNUM() {
		return _FDPNUM;
	}
	public void setFDPNUM(String FDPNUM) {
		this._FDPNUM = FDPNUM;
	}

	public String getAMT() {
		return _AMT;
	}
	public void setAMT(String AMT) {
		this._AMT = AMT;
	}

	public String getINTMTH() {
		return _INTMTH;
	}
	public void setINTMTH(String INTMTH) {
		this._INTMTH = INTMTH;
	}

	public String getDPISDT() {
		return _DPISDT;
	}
	public void setDPISDT(String DPISDT) {
		this._DPISDT = DPISDT;
	}
	
	public String getDUEDAT() {
		return _DUEDAT;
	}
	public void setDUEDAT(String DUEDAT) {
		this._DUEDAT = DUEDAT;
	}

	public String getINTPAY() {
		return _INTPAY;
	}
	public void setINTPAY(String INTPAY) {
		this._INTPAY = INTPAY;
	}

	public String getITR() {
		return _ITR;
	}
	public void setITR(String ITR) {
		this._ITR = ITR;
	}
	
	public String getTAX() {
		return _TAX;
	}
	public void setTAX(String TAX) {
		this._TAX = TAX;
	}
	
	public String getINTRCV() {
		return _INTRCV;
	}
	public void setINTRCV(String INTRCV) {
		this._INTRCV = INTRCV;
	}

	public String getPAIAFTX() {
		return _PAIAFTX;
	}
	public void setPAIAFTX(String PAIAFTX) {
		this._PAIAFTX = PAIAFTX;
	}
	
	public String getNHITAX() {
		return _NHITAX;
	}
	public void setNHITAX(String NHITAX) {
		this._NHITAX = NHITAX;
	}
		
	public String getCMTRMEMO() {
		return _CMTRMEMO;
	}
	public void setCMTRMEMO(String CMTRMEMO) {
		this._CMTRMEMO = CMTRMEMO;
	}
	
	public String getCMTRMAIL() {
		return _CMTRMAIL;
	}
	public void setCMTRMAIL(String CMTRMAIL) {
		this._CMTRMAIL = CMTRMAIL;
	}
	
	public String getCMMAILMEMO() {
		return _CMMAILMEMO;
	}
	public void setCMMAILMEMO(String CMMAILMEMO) {
		this._CMMAILMEMO = CMMAILMEMO;
	}
}
