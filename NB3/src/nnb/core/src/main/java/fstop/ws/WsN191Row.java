package fstop.ws;

public class WsN191Row {

	private String _ACN;//	帳號
	
	private String _LSTLTD;//	異動日期
	
	private String _MEMO;//	摘要
	
	private String _DPWDAMT;//	支出
	
	private String _DPSVAMT;//	存入
	
	private String _GDBAL;//	結存
	
	private String _PRICE;//	單價(元)
	
	private String _TRNAMT;//交易金額(元)
	
	private String _TIME;//	異動時間

	public String getACN() {
		return _ACN;
	}

	public void setACN(String acn) {
		this._ACN = acn;
	}

	public String getLSTLTD() {
		return _LSTLTD;
	}

	public void setLSTLTD(String lstltd) {
		this._LSTLTD = lstltd;
	}

	public String getMEMO() {
		return _MEMO;
	}

	public void setMEMO(String memo) {
		this._MEMO = memo;
	}

	public String getDPWDAMT() {
		return _DPWDAMT;
	}

	public void setDPWDAMT(String dpwdamt) {
		this._DPWDAMT = dpwdamt;
	}

	public String getDPSVAMT() {
		return _DPSVAMT;
	}

	public void setDPSVAMT(String dpsvamt) {
		this._DPSVAMT = dpsvamt;
	}

	public String getGDBAL() {
		return _GDBAL;
	}

	public void setGDBAL(String gdbal) {
		this._GDBAL = gdbal;
	}

	public String getPRICE() {
		return _PRICE;
	}

	public void setPRICE(String price) {
		this._PRICE = price;
	}

	public String getTRNAMT() {
		return _TRNAMT;
	}

	public void setTRNAMT(String trnamt) {
		this._TRNAMT = trnamt;
	}

	public String getTIME() {
		return _TIME;
	}

	public void setTIME(String time) {
		this._TIME = time;
	}

	
}
