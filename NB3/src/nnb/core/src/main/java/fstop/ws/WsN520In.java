package fstop.ws;

public class WsN520In {

	private String _SessionId;

	private String _N950PASSWORD;
	
	private String _ACN;
	
	private String _FGPERIOD;
	
	private String _CMSDATE;
	
	private String _CMEDATE;

	public String getSessionId() {
		return _SessionId;
	}
	public void setSessionId(String sessionid) {
		this._SessionId = sessionid;
	}

	public String getN950PASSWORD() {
		return _N950PASSWORD;
	}
	public void setN950PASSWORD(String n950password) {
		this._N950PASSWORD = n950password;
	}

	public String getACN() {
		return _ACN;
	}
	public void setACN(String acn) {
		this._ACN = acn;
	}

	public String getFGPERIOD() {
		return _FGPERIOD;
	}
	public void setFGPERIOD(String fgperiod) {
		this._FGPERIOD = fgperiod;
	}

	public String getCMSDATE() {
		return _CMSDATE;
	}
	public void setCMSDATE(String cmsdate) {
		this._CMSDATE = cmsdate;
	}

	public String getCMEDATE() {
		return _CMEDATE;
	}
	public void setCMEDATE(String cmedate) {
		this._CMEDATE = cmedate;
	}

}
