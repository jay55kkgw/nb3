
/*
 * Copyright (c) 2017, FSTOP, Inc. All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tw.com.fstop.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


/**
 * Bean reflection utility functions.
 * 
 *
 * @since 1.0.1
 */
public final class BeanUtil
{
    private BeanUtil() {}
    
    static Method [] getMethods(Class<?> clazz)
    {
        return clazz.getMethods();
    }

    static List<Method> getMethods(Class<?> clazz, int modifier)
    {
        List<Method> list = new ArrayList<Method>();
        Method [] methods = clazz.getMethods();
        
        for(Method method : methods)
        {
            int m = method.getModifiers();
            if ((modifier & m) != 0)
            {
                list.add(method);
            }
        }        
        return list;
    }
    
    static Field [] getFields(Class<?> clazz)
    {        
        return clazz.getFields(); //This only returns public fields.
    }
    
    static Field [] getPublicFields(Class<?> clazz)
    {
        return getFields(clazz);
    }
    
    static Field [] getDeclaredFields(Class<?> clazz)
    {
        return clazz.getDeclaredFields();
    }
    
    static String decapitalize(String string)
    {
        if (string == null || string.isEmpty())
        {
            return string;
        }
        char c[] = string.toCharArray();
        c[0] = Character.toLowerCase(c[0]);
        return new String(c);
    }

    static String capitalize(String string)
    {
        if (string == null || string.isEmpty())
        {
            return string;
        }
        char c[] = string.toCharArray();
        c[0] = Character.toUpperCase(c[0]);
        return new String(c);
    }
    
    /**
     * Get getters of class.
     * 
     * @param clazz - class to access
     * @return list of getters
     */
    public static List<Method> getGetters(Class<?> clazz)
    {
        List<Method> ret = new ArrayList<Method>();
        List<Method> ma = getMethods(clazz, Modifier.PUBLIC);
        for (Method mf : ma)
        {
            String name = mf.getName();
            if(name.startsWith("get") || name.startsWith("is"))
            {
                ret.add(mf);
            }            
        }
        return ret;
    }
    
    /**
     * Get property names of class.
     * 
     * @param clazz - class to access
     * @return list of properties
     */
    public static List<String> getPropertyNames(Class<?> clazz)
    {
        List<String> ret = new ArrayList<String>();
        List<Method> ml = getGetters(clazz);
        
        String prop = null;
        for(Method mf : ml)
        {
            String name = mf.getName();
            
            if(name.startsWith("get"))
            {                
                prop = name.substring(3);
                if (prop.length() == 0)
                {
                    prop = name;
                }
                
            }
            else if(name.startsWith("is"))
            {
                prop = name.substring(2);
                if (prop.length() == 0)
                {
                    prop = name;
                }
            }
            prop = decapitalize(prop);
            if (!ret.contains(prop))
            {
                ret.add(prop);                
            }
        }
        
        return ret;
    }  //getPropertyNames
    
    /**
     * Get class setters.
     * 
     * @param clazz - class to access
     * @return list of setters
     */
    public static List<Method> getSetters(Class<?> clazz)
    {
        List<Method> ma = getMethods(clazz, Modifier.PUBLIC);
        List<Method> ret = new ArrayList<Method>();
        for (Method mf : ma)
        {
            String name = mf.getName();
            if(name.startsWith("set"))
            {
                ret.add(mf);
            }            
        }
        return ret;
    }
    
    /**
     * Get class method names.
     * 
     * @param clazz - class to access
     * @return list of method name
     */
    public static List<String> getMethodNames(Class<?> clazz)
    {
        List<String> ret = new ArrayList<String>();
        Method[] methods = clazz.getMethods();
        for (Method method : methods)
        {
            String name=method.getName();
            ret.add(name);
        }
        return ret;
    }
    
    /**
     * Find match method by reflection.
     * 
     * @param clazz class to find
     * @param methodName method name to find
     * @param parameterTypes method parameter types
     * @return search result
     * @throws NoSuchMethodException throws if not found
     */
    public static Method findMethod(Class<?> clazz, String methodName, Class<?>... parameterTypes)
            throws NoSuchMethodException
    {

        // First try the trivial approach. This works usually, but not always.
        try
        {
            return clazz.getMethod(methodName, parameterTypes);
        }
        catch (NoSuchMethodException ex)
        {
        }

        // Then loop through all available methods, checking them one by one.
        for (Method method : clazz.getMethods())
        {

            String name = method.getName();
            if (!methodName.equals(name))
            { // The method must have right name.
                continue;
            }

            Class<?>[] acceptedParameterTypes = method.getParameterTypes();
            if (acceptedParameterTypes.length != parameterTypes.length)
            { // Must have right number of parameters.
                continue;
            }

            boolean match = true;
            for (int i = 0; i < acceptedParameterTypes.length; i++)
            { // All parameters must be right type.
                if (null != parameterTypes[i] && !acceptedParameterTypes[i].isAssignableFrom(parameterTypes[i]))
                {
                    match = false;
                    break;
                }
                if (null == parameterTypes[i] && acceptedParameterTypes[i].isPrimitive())
                { // Accept null except for primitive fields.
                    match = false;
                    break;
                }
            }

            if (match)
            {
                return method;
            }

        }

        // None of our trials was successful!
        throw new NoSuchMethodException();
    }

    /**
     * Invoke setters when data conversion is possible.
     * Since normal setter function just accept one input parameter, 
     * so we just check one accept parameter here.
     * 
     * @param object Object to invoke
     * @param fieldName field name to invoke setter
     * @param fieldValue field value to set
     * @throws NoSuchMethodException throws if not found
     */
    public static void invokeConvertableSetter(Object object, String fieldName, Object fieldValue)
            throws NoSuchMethodException
    {

        Class<?> clazz = object.getClass();
        
        //field name to setter method name, for example : "name" -> "setName"
        String methodName = "set" + capitalize(fieldName);
        
        //First try the trivial approach. This works usually, but not always.
        Class<?> [] parameterTypes = new Class[] { fieldValue.getClass() };
        try
        {
            Method method = clazz.getMethod(methodName, parameterTypes); 
            method.invoke(object, fieldValue);  
            return;
        }
        catch (NoSuchMethodException ex)
        {
        }
        catch (IllegalAccessException e)
        {
            throw new NoSuchMethodException(); 
        }
        catch (IllegalArgumentException e)
        {
            throw new NoSuchMethodException();
        }
        catch (InvocationTargetException e)
        {
            throw new NoSuchMethodException();
        }

        //Then loop through all available methods, checking them one by one.
        for (Method method : clazz.getMethods())
        {

            String name = method.getName();
            if (!methodName.equals(name))
            { 
                //The method must have right name.
                continue;
            }

            Class<?>[] acceptedParameterTypes = method.getParameterTypes();
            if (acceptedParameterTypes.length != parameterTypes.length)
            { 
                //Must have right number of parameters.
                continue;
            }

            for (int i = 0; i < acceptedParameterTypes.length; i++)
            {   
                //All parameters must be right type. Here we just check 1 input parameter for setter. 
                try
                {
                    //System.out.println(parameterTypes[i].getName());
                    //System.out.println(acceptedParameterTypes[i].getName());
                    if (parameterTypes[i] == acceptedParameterTypes[i])
                    {                        
                        method.invoke(object, fieldValue);
                        return;
                    }
                    else
                    {
                        Object obj = ObjectConvertUtil.convert(fieldValue, acceptedParameterTypes[i]);
                        method.invoke(object, obj);
                        return;
                    }                    
                }
                catch(Exception ex)
                {    
                }                
            }
        }
        //None of our trials was successful!
        throw new NoSuchMethodException();
    }
        
    /**
     * Invoke object setter by input field name and field value.
     * 
     * @param object target object to invoke
     * @param fieldName invoke field name
     * @param fieldValue invoke value
     * @throws IllegalStateException throws if field value is not matched
     */
    public static void invokeSetter(Object object, String fieldName, Object fieldValue) throws IllegalStateException
    {
        Class<?> clazz = object.getClass();
        try
        {
            String methodName = "set" + capitalize(fieldName);
            // Class<?>... parameterTypes
            // clazz.getMethod(methodName, parameterTypes);
            Method method = clazz.getMethod(methodName, new Class[]
                { fieldValue.getClass() });
            method.invoke(object, fieldValue);
            return;
        }
        catch (Exception e)
        {
            throw new IllegalStateException(e);
        }
    }
        
    
    /**
     * Direct set value to object attribute/field.
     * Note: this function bypass setter functions.
     * 
     * @param object object to set value.
     * @param fieldName field name of object
     * @param fieldValue value to set
     * @return true/false
     */
    public static boolean setField(Object object, String fieldName, Object fieldValue)
    {
        Class<?> clazz = object.getClass();
        try
        {
            Field field = clazz.getDeclaredField(fieldName);
            field.setAccessible(true);
            if (field.getType().isPrimitive())
            {
                field.set(object, fieldValue);
            }
            else
            {
                field.set(object, ObjectConvertUtil.convert(fieldValue, field.getType()));
            }
            return true;
        }
        catch (NoSuchFieldException e)
        {
            clazz = clazz.getSuperclass();
        }
        catch (Exception e)
        {
            throw new IllegalStateException(e);
        }
        return false;
    }
        
    /**
     * Get object field value by reflection.
     * 
     * @param object - object to access
     * @param fieldName - field name of object to get value
     * @return field value or null if access failed
     */
    @SuppressWarnings("unchecked")
    public static <V> V getValue(Object object, String fieldName)
    {
        Class<?> clazz = object.getClass();
        while (clazz != null)
        {
            try
            {
                Field field = clazz.getDeclaredField(fieldName);
                field.setAccessible(true);
                return (V) field.get(object);
            }
            catch (NoSuchFieldException e)
            {
                clazz = clazz.getSuperclass();
            }
            catch (Exception e)
            {
                throw new IllegalStateException(e);
            }
        }
        return null;
    }
    
    /**
     * Get generic parameter type.
     * 
     * @param clazz - class to get parameter type.
     * @return actual parameter type 
     */
    @SuppressWarnings("unchecked")
    public static <T> Class<T> getTypeParameterClass(Class<T> clazz)
    {
        Type type = clazz.getGenericSuperclass();
        ParameterizedType paramType = (ParameterizedType) type;
        return (Class<T>) paramType.getActualTypeArguments()[0];
    }
    
    /**
     * Create new instance of class by specified class.
     * 
     * @param clazz - class to instantiate
     * @return instance of class
     * @throws InstantiationException if instantiate fail
     * @throws IllegalAccessException if access class fail
     */
    public static <T> T newInstance(Class<T> clazz) throws InstantiationException, IllegalAccessException
    {
        return clazz.newInstance();
    }

    /**
     * Get class instance by class name.
     * 
     * @param className - class name 
     * @return instance of class
     * @throws ClassNotFoundException if class not found
     */
    @SuppressWarnings("unchecked")
    public static <T> T getClassObject(String className) throws ClassNotFoundException
    {
        return (T) Class.forName(className);       
    }

    /**
     * Create new instance of specified class.
     * 
     * @param className - class to create new instance
     * @return instance of class
     * @throws ClassNotFoundException if class not found
     * @throws InstantiationException if instantiate fail
     * @throws IllegalAccessException if access class fail
     */
    public static <T> T newInstance(String className) throws ClassNotFoundException, InstantiationException, IllegalAccessException
    {
        Class<T> c = getClassObject(className);
        return newInstance(c);
    }
    
    /**
     * Invoke object setter by input field name and field value.
     * 
     * @param object target object to invoke
     * @param fieldName invoke field name
     * @param fieldValue invoke value
     * @throws IllegalStateException throws if field value is not matched
     */
    
    /**
     * Invoke object method by input method name and parameters.
     * 
     * @param object - target object to invoke 
     * @param methodName - invoke method name
     * @param param - method parameters
     * @return invoke result
     * @throws NoSuchMethodException if method not found
     * @throws InvocationTargetException 
     */
    @SuppressWarnings("unchecked")
    public static <T> T invokeMethod(Object object, String methodName, Object ... param) throws NoSuchMethodException, InvocationTargetException
    {
        Object ret = null;
        Class<?> clazz = object.getClass();
        
        try
        {
            // Class<?>... parameterTypes
            // clazz.getMethod(methodName, parameterTypes);
            Method method = null;
            if (param == null || param.length == 0)
            {
                method = clazz.getMethod(methodName, new Class[] {});  
            }
            else
            {
                Class<?> [] parameterTypes = new Class [param.length];
                for(int i=0; i < param.length; i++)
                {
                    parameterTypes[i] = param.getClass();
                }
                method = clazz.getMethod(methodName, parameterTypes);   
            }
            
            ret = method.invoke(object, param);
            return (T) ret;
        }
        catch (NoSuchMethodException ex)
        {
        }
        catch (IllegalAccessException e)
        {
            throw new NoSuchMethodException(); 
        }
        catch (IllegalArgumentException e)
        {
            throw new NoSuchMethodException();
        }
        catch (InvocationTargetException e)
        {
            throw e;
        }
        
        // get each parameter types
        Class<?> [] parameterTypes = new Class [param.length];
        for(int i=0; i < param.length; i++)
        {
            parameterTypes[i] = param.getClass();
        }
                   
        //Then loop through all available methods, checking them one by one.
        for (Method method : clazz.getMethods())
        {

            String name = method.getName();
            if (!methodName.equals(name))
            { 
                //The method must have right name.
                continue;
            }

            Class<?>[] acceptedParameterTypes = method.getParameterTypes();
            if (acceptedParameterTypes.length != parameterTypes.length)
            { 
                //Must have right number of parameters.
                continue;
            }

            //All parameters must be right type. 
            boolean isAllMatch = false;
            boolean isAssignable = false;
            for (int i = 0; i < acceptedParameterTypes.length; i++)
            {
                isAssignable = acceptedParameterTypes[i].getClass().isAssignableFrom(parameterTypes[i].getClass());
                if (parameterTypes[i] == acceptedParameterTypes[i] || isAssignable)
                {
                    isAllMatch = true;
                }
                else
                {
                    isAllMatch = false;
                }
            }
            
            if (isAllMatch)
            {
                try
                {
                    ret = method.invoke(object, param);
                    return (T) ret;
                }
                catch (IllegalAccessException e)
                {
                    throw new NoSuchMethodException();
                }
                catch (IllegalArgumentException e)
                {
                    throw new NoSuchMethodException();
                }
                catch (InvocationTargetException e)
                {                    
                    throw e;
                }   
            }
        }  // for each method
        
        //None of our trials was successful!
        throw new NoSuchMethodException();
    }  //invokeMethod
    
    
    
    
}
