package tw.com.fstop.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OriginalUtil {
	private static Logger log = LoggerFactory.getLogger(DownloadUtil.class);

	/**
	 * 原N998判斷是否寄信通知的邏輯
	 * 
	 * @param chkNo
	 *            輸入檢核字串
	 * @param DBStr
	 *            TXNUSER.DPNOTIFY
	 * @return
	 */
	public static boolean chkContains(String chkNo, String DBStr){
		int count = 0;
		int chNo  = 0;
		String str = "";
		while(true){
			chNo = DBStr.indexOf(',',count);
			if(chNo == -1)
			{
				str = DBStr.substring(count,DBStr.length());
				if(str.equals(chkNo)) return true;
				else
         			 break;
			}
			str = DBStr.substring(count,chNo);
			if(str.equals(chkNo)) return true;
			count = chNo+1;
		}
		return false;
	}
	
}