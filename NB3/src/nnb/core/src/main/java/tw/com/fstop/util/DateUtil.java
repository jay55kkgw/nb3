package tw.com.fstop.util;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DateUtil {
	static Logger log = LoggerFactory.getLogger(DateUtil.class);
	
	public static int INTERCONVERSION = 1;
	public static int NOT_INTERCONVERSION = 2;

	/**
	 * 取得民國年
	 * 
	 * @return
	 */
	public static String getCurrentTWYear() {
		Calendar cal = Calendar.getInstance();
		String year = String.valueOf(cal.get(Calendar.YEAR) - 1911);
		return year;
	}

	
	
	/**
	 * 取得目前民國年月日(YYYMMDD)
	 * 
	 * @return
	 */
	public static String getTWDate(String sign) {
		if (StrUtil.isEmpty(sign)) {
			sign = "";
		}
		Calendar cal = Calendar.getInstance();
		String theDate = null;
		String month = String.valueOf((cal.get(Calendar.MONTH) + 1) / 10) + ""
				+ String.valueOf((cal.get(Calendar.MONTH) + 1) % 10);
		String day = String.valueOf(cal.get(Calendar.DAY_OF_MONTH) / 10) + ""
				+ String.valueOf(cal.get(Calendar.DAY_OF_MONTH) % 10);
		theDate = getCurrentTWYear() + sign + month + sign + day;
		return theDate;
	}

	/**
	 * 格式化目前日期
	 * 
	 * @param pattern(轉換的格式)
	 *            範例  yyyyMMdd >>>>20190525
	 * @return
	 */
	public static String getCurentDateTime(String pattern) {
		pattern = StrUtil.isNotEmpty(pattern) ? pattern : "yyyyMMddHHmmss";
		SimpleDateFormat sdFormat = new SimpleDateFormat(pattern);
		Date current = new Date();
		String d = sdFormat.format(current);
		return d;
	}

	/**
	 * 取得目前時間(HHMMSS)
	 * 
	 * @param sign
	 * @return
	 */
	public static String getTheTime(String sign) {
		if (StrUtil.isEmpty(sign)) {
			sign = "";
		}
		Calendar cal = Calendar.getInstance();
		String theTime = null;
		String theHours = String.valueOf(cal.get(Calendar.HOUR_OF_DAY) / 10) + ""
				+ String.valueOf(cal.get(Calendar.HOUR_OF_DAY) % 10);
		String theMinutes = String.valueOf(cal.get(Calendar.MINUTE) / 10) + ""
				+ String.valueOf(cal.get(Calendar.MINUTE) % 10);
		String theSeconds = String.valueOf(cal.get(Calendar.SECOND) / 10) + ""
				+ String.valueOf(cal.get(Calendar.SECOND) % 10);
		theTime = theHours + sign + theMinutes + sign + theSeconds;
		return theTime;
	}

	public static String getDate(String sign) {
		if (StrUtil.isEmpty(sign)) {
			sign = "";
		}
		Calendar cal = Calendar.getInstance();
		String theDate = null;
		String month = String.valueOf((cal.get(Calendar.MONTH) + 1) / 10) + ""
				+ String.valueOf((cal.get(Calendar.MONTH) + 1) % 10);
		String day = String.valueOf(cal.get(Calendar.DAY_OF_MONTH) / 10) + ""
				+ String.valueOf(cal.get(Calendar.DAY_OF_MONTH) % 10);
		theDate = cal.get(Calendar.YEAR) + sign + month + sign + day;
		// System.out.println("theDate>>"+theDate);
		return theDate;
	}

	/**
	 * 取得下個日期
	 * 
	 * @param retPattern
	 *            輸入回傳的格式 ex yyyy/MM/dd =2016/07/16
	 * @return
	 */
	public static String getNextDate(String retPattern) {
		String date = "";
		try {
			date = new DateTime().plusDays(1).toString(retPattern);
		} catch (Exception e) {
			log.error("getNextDate error >> {}",e);
		}
		log.debug("getNextDate  {}" ,date);
		return date;
	}

	/**
	 * 取得下N天日期
	 * 
	 * @param retPattern
	 * @param num
	 * @return
	 */
	public static String getNext_N_Date(String retPattern, Integer num) {
		String  date = "";
		try {
			date = new DateTime().plusDays(num).toString(retPattern);
		} catch (Exception e) {
			log.error("getNext_N_Date error >> {}",e); 
		}
		log.debug("next date>> {}" , date);
		return date;
	}

	/**
	 * 取得2個日期間的天數
	 * 
	 * @param firstdate
	 * @param seconddate
	 * @return
	 */
	public static int getDiffDate(String firstdate, String seconddate, String pattern) {
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		Calendar calendar = Calendar.getInstance();
		Calendar calendar2 = Calendar.getInstance();
		long d1 = 0;
		long d2 = 0;
		BigDecimal diffdays = new BigDecimal(0);
		try {
			// System.out.println("firstdate>>"+firstdate+"seconddate>>"+seconddate);
			calendar.setTime(sdf.parse(firstdate));
			calendar2.setTime(sdf.parse(seconddate));
			d1 = calendar.getTimeInMillis();
			d2 = calendar2.getTimeInMillis();
			diffdays = new BigDecimal((d1 - d2) / (1000 * 60 * 60 * 24)).abs();
		} catch (ParseException e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getDiffDate error >> {}",e);
		}
		return diffdays.intValue() + 1;

	}

	/**
	 * 取得2個日期間的天數
	 * 
	 * @param firstdate
	 * @param seconddate
	 * @return
	 */
	public static int getDiffTimeStamp(String firstdate, String seconddate) {
		// System.out.println("firstdate>>"+firstdate);
		// System.out.println("seconddate>>"+seconddate);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar calendar = Calendar.getInstance();
		Calendar calendar2 = Calendar.getInstance();
		long d1 = 0;
		long d2 = 0;
		BigDecimal diffdays = new BigDecimal(0);
		try {
			// System.out.println("firstdate>>"+firstdate+"seconddate>>"+seconddate);
			calendar.setTime(sdf.parse(firstdate));
			calendar2.setTime(sdf.parse(seconddate));
			d1 = calendar.getTimeInMillis();
			d2 = calendar2.getTimeInMillis();
			// diffdays = new BigDecimal((d1-d2)/(1000*60*60*24)).abs();
			diffdays = new BigDecimal((d1 - d2) / (1000)).abs();
		} catch (ParseException e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getDiffTimeStamp error >> {}",e);
		}
		return diffdays.intValue() + 1;

	}
	
	/**
	 * 取得時間差:單位毫秒
	 * @param d1
	 * @param d2
	 * @return
	 */
	public static String getDiffTimeMills(DateTime d1 ,DateTime d2) {
		String ret ="";
		try {
			Long l = d1.getMillis() - d2.getMillis();
			BigDecimal bd = new BigDecimal(l).divide(new BigDecimal(1000)).abs().setScale(3);
			ret = bd.toString();
		} catch (Exception e) {
			log.error("{}",e);
		}
		return ret;
	}
	
	
	/**
	 * 預約:取得次日之日期
	 * 
	 * @param firstdate
	 * @param seconddate
	 * @return
	 */
	public static String getNextDay() {
		String str_SystemDate = "";
		
		try {
			Calendar cal = Calendar.getInstance();  
			cal.set(Calendar.DATE, cal.get(Calendar.DATE) + 1); 
			Date nextDay = cal.getTime();
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
			str_SystemDate = sdf.format(nextDay);
			
		} catch (Exception e) {
			log.error("", e);
		}
		log.debug("DateUtil.getNextDay: {}", str_SystemDate);
		return str_SystemDate;
	}
	
	/**
	 * 預約:取得次日起後一年之日期
	 * 
	 * @param firstdate
	 * @param seconddate
	 * @return
	 */
	public static String getNextYearDay() {
		String sNextYearDay = "";
		
		try {
			Calendar cal = Calendar.getInstance();  
			cal.set(Calendar.DATE, cal.get(Calendar.DATE) + 1); 
			cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) + 1);
			Date nextYearDay = cal.getTime();
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
			
			sNextYearDay = sdf.format(nextYearDay);
			
		} catch (Exception e) {
			log.error("", e);
		}
		log.debug("DateUtil.getNextYearDay: {}", sNextYearDay);
		return sNextYearDay;
	}
	
	/**
	 * 預約:取得次日起後二年之日期
	 * 
	 * @param firstdate
	 * @param seconddate
	 * @return
	 */
	public static String getNextTwoYearsDay() {
		String sNextYearDay = "";
		
		try {
			Calendar cal = Calendar.getInstance();  
			cal.set(Calendar.DATE, cal.get(Calendar.DATE) + 1); 
			cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) + 2);
			Date nextTwoYearsDay = cal.getTime();
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
			
			sNextYearDay = sdf.format(nextTwoYearsDay);
			
		} catch (Exception e) {
			log.error("", e);
		}
		log.debug("DateUtil.getNextYearsDay: {}", sNextYearDay);
		return sNextYearDay;
	}

	/**
	 * 西元年 民國年互轉
	 * 
	 * @param type(1(INTERCONVERSION)=西元民國互轉;2(NOT_INTERCONVERSION)=不轉)
	 * @param AD
	 * @param beforeFormat(轉換前的格式)
	 * @param afterFormat(轉換後的格式)
	 *            範例 convertDate("2013-05-08 21:10:10 ","yyyy-MM-dd
	 *            HH:mm:ss","yyyyMMdd HH:mm:ss") convertDate("0102-05-08
	 *            21:20:10","yyyy-MM-dd HH:mm:ss","yyyyMMdd HH:mm:ss"
	 * @return
	 */
	public static String convertDate(int type, String AD, String beforeFormat, String afterFormat) {// 轉年月格式
		if (StrUtil.isEmpty(AD))
			return "";
		SimpleDateFormat df4 = new SimpleDateFormat(beforeFormat);
		SimpleDateFormat df2 = new SimpleDateFormat(afterFormat);
		Calendar cal = Calendar.getInstance();
		String tmp = "";
		try {
			cal.setTime(df4.parse(AD));
			if (type == INTERCONVERSION) {
				if (cal.get(Calendar.YEAR) > 1492) {
					if (cal.get(Calendar.YEAR) - 1911 < 100) {
						tmp = "0";
					}
					cal.add(Calendar.YEAR, -1911);
				} else {
					cal.add(Calendar.YEAR, +1911);
				}
			}
			return tmp + df2.format(cal.getTime());
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getNextYearDay error >> {}",e);
			return null;
		}
	}

	/**
	 * 西元年 民國年互轉
	 * 
	 * @param type(1(INTERCONVERSION)=西元民國互轉;2(NOT_INTERCONVERSION)=不轉)
	 * @param AD
	 * @param beforeFormat(轉換前的格式)
	 * @param afterFormat(轉換後的格式)
	 *            範例 convertDate("2013-05-08 21:10:10 ","yyyy-MM-dd
	 *            HH:mm:ss","yyyyMMdd HH:mm:ss") convertDate("0102-05-08
	 *            21:20:10","yyyy-MM-dd HH:mm:ss","yyyyMMdd HH:mm:ss"
	 * @return
	 */
	public static String convertDateNP0(int type, String AD, String beforeFormat, String afterFormat) {// 轉年月格式
		if (StrUtil.isEmpty(AD))
			return "";
		SimpleDateFormat df4 = new SimpleDateFormat(beforeFormat);
		SimpleDateFormat df2 = new SimpleDateFormat(afterFormat);
		Calendar cal = Calendar.getInstance();
		try {
			cal.setTime(df4.parse(AD));
			if (type == INTERCONVERSION) {
				if (cal.get(Calendar.YEAR) > 1492) {
					cal.add(Calendar.YEAR, -1911);
				} else {
					cal.add(Calendar.YEAR, +1911);
				}
			}
			return df2.format(cal.getTime());
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getNextYearDay error >> {}",e);
			return null;
		}
	}
	/**
	 * 此方法會因為作業系統語系不同而產生相對應的字串 輸入日期回傳星期幾
	 * 
	 * @param dateString
	 * @return
	 * @throws ParseException
	 */
	public static String date2DayofWeek(String dateString, String pattern) throws ParseException {
		pattern = StrUtil.isEmpty(pattern) ? "yyyy-MM-dd" : pattern;
		// SimpleDateFormat dateStringFormat = new SimpleDateFormat( "yyyy-MM-dd" );
		SimpleDateFormat dateStringFormat = new SimpleDateFormat(pattern);
		Date date = dateStringFormat.parse(dateString);
		// SimpleDateFormat date2DayFormat = new SimpleDateFormat( "u" );
		SimpleDateFormat date2DayFormat = new SimpleDateFormat("E");
		// System.out.println(""+date2DayFormat.format( date ));
		return date2DayFormat.format(date);
	}

	public static enum Week {
		Mon, Tue, Wed, Thu, Fri, Sat, Sun
	}

	/**
	 * 
	 * @param dateString
	 * @param pattern
	 * @return
	 * @throws ParseException
	 */
	public static String date2DayofWeekII(String dateString, String pattern) throws ParseException {
		String ret = "";
		String tmp = "";
		pattern = StrUtil.isEmpty(pattern) ? "yyyy-MM-dd" : pattern;
		// SimpleDateFormat dateStringFormat = new SimpleDateFormat( "yyyy-MM-dd" );
		SimpleDateFormat dateStringFormat = new SimpleDateFormat(pattern);
		Date date = dateStringFormat.parse(dateString);
		// GyMdkHmsSEDFwWahKzZ
		// SimpleDateFormat date2DayFormat = new SimpleDateFormat( "u" );
		SimpleDateFormat date2DayFormat = new SimpleDateFormat("E");
		// System.out.println(""+date2DayFormat.format( date ));
		// System.out.println("isWindows>>"+OSValidator.isWindows());
		if (!OSValidator.isWindows()) {
			tmp = date2DayFormat.format(date);
			Week week = Week.valueOf(tmp);
			// System.out.println("week"+week);
			switch (week) {
			case Mon:
				ret = "星期一";
				break;
			case Tue:
				ret = "星期二";
				break;
			case Wed:
				ret = "星期三";
				break;
			case Thu:
				ret = "星期四";
				break;
			case Fri:
				ret = "星期五";
				break;
			case Sat:
				ret = "星期六";
				break;
			case Sun:
				ret = "星期日";
				// System.out.println("this is sun");
				break;

			default:
				ret = "";
				break;
			}
		} else {
			ret = date2DayFormat.format(date);
		}
		// System.out.println("ret"+ret);
		return ret;
	}

	/**
	 * 
	 * <p>
	 * 傳回格式化(HH:MM:SS)時間字串
	 * </p>
	 * 
	 * @return a_str_time:未格式化之時間字串
	 * @see old-core\DisplayUtil.java
	 */
	public static String formatTime(String a_str_time) {
		try {
			if (a_str_time == null || a_str_time.equals(""))
				return "";

			// 若傳入字串大於六位，後面無條件捨去
			return a_str_time.substring(0, 2) + ":" + a_str_time.substring(2, 4) + ":" + a_str_time.substring(4, 6);
		} catch (Exception exc) {
			return "時間格式錯誤";
		}
	}

	/**
	 * <p>
	 * 得到本月的第一天
	 * </p>
	 * 
	 * @return 回傳的格式 yyyy/MM/dd EX : 2018/09/01
	 * @see old-core\DateTimeUtils.java
	 */
	public static String getMonthFirstDay() {
		SimpleDateFormat formatSlash = new SimpleDateFormat("yyyy/MM/dd");
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
		return formatSlash.format(calendar.getTime());
	}

	/**
	 * <p>
	 * 得到上個月的第一天
	 * </p>
	 * 
	 * @return 回傳的格式 yyyy/MM/dd EX : 2018/08/01
	 * @see old-core\DateTimeUtils.java
	 */
	public static String getPrevMonthFirstDay() {
		SimpleDateFormat formatSlash = new SimpleDateFormat("yyyy/MM/dd");
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1);
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
		return formatSlash.format(calendar.getTime());
	}

	/**
	 * 得到上個月的最後一天
	 * 
	 * @return 回傳的格式 yyyy/MM/dd EX :2018/08/31
	 * @see old-core\DateTimeUtils.java
	 */
	public static String getPrevMonthLastDay() {
		SimpleDateFormat formatSlash = new SimpleDateFormat("yyyy/MM/dd");
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1);
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		return formatSlash.format(calendar.getTime());
	}

	/**
	 * <p>
	 * 處理日期區間的方法
	 * </p>
	 * 
	 * @param reqParam
	 * @return Map<String, String> returnParam.put("fgperiod", fgperiod);
	 *         returnParam.put("cmsdate", cmsdate); returnParam.put("cmedate",
	 *         cmedate);
	 * @see TBBNBAppsJava\fstop\services\impl\N130.java
	 */
	public static Map<String, String> periodProcessing(Map<String, String> reqParam) {
		String fgperiod = reqParam.get("FGPERIOD");// 查詢時間
		String cmsdate = reqParam.get("CMSDATE"); // 日期(起)
		String cmedate = reqParam.get("CMEDATE"); // 日期(迄)
		SimpleDateFormat dtD = new SimpleDateFormat("yyyy/MM/dd");

		if ("CMTODAY".equals(fgperiod)) { // 今日
			cmsdate = dtD.format(new Date());
			cmedate = cmsdate;
		} else if ("CMCURMON".equals(fgperiod)) { // 本月
			cmsdate = DateUtil.getMonthFirstDay();// 取得本月的第一天
			cmedate = dtD.format(new Date());// Date format
		} else if ("CMLASTMON".equals(fgperiod)) { // 上月
			cmsdate = DateUtil.getPrevMonthFirstDay();// 取得上個月的第一天
			cmedate = DateUtil.getPrevMonthLastDay();// 取得上個月的最後一天
		} else if ("CMNEARMON".equals(fgperiod)) { // 最近一個月
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.MONTH, -1);
			Date dstart = cal.getTime();
			Date dend = new Date();
			// Date format
			cmsdate = dtD.format(dstart);
			cmedate = dtD.format(dend);
		} else if ("CMLASTWEEK".equals(fgperiod)) { // 最近一星期
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -7);
			Date dstart = cal.getTime();
			Date dend = new Date();
			// Date format
			cmsdate = dtD.format(dstart);
			cmedate = dtD.format(dend);
		} else if ("CMPERIOD".equals(fgperiod)) { // 指定日期

		}
		reqParam.put("fgperiod", fgperiod);
		reqParam.put("cmsdate", cmsdate);
		reqParam.put("cmedate", cmedate);
		return reqParam;
	}

	/**
	 * 取得明天日期，如果當前時間太晚(23:50:00~23:59:59)則為後天
	 * 
	 * @return
	 */
	public static String getTomorrowOrAfterTomorrowDate() {
		String resultDate = "-1";
		try {
			String dn = DateUtil.getNext_N_Date("yyyy/MM/dd", 2);
			String dt = DateUtil.getNextDate("yyyy/MM/dd");
			Integer dsint = Integer.parseInt(DateUtil.getTheTime(""));
			Integer s_start = 235000; // 23:50:00
			Integer s_end = 235959; // 23:59:59
			if (dsint >= s_start && dsint <= s_end) {
				resultDate = dn;
			} else {
				resultDate = dt;
			}
		} catch (Exception e) {
			log.error("getTomorrowOrAfterTomorrowDate error >> {}",e);
		}
		return resultDate;
	}

	/**
	 * 將民國年的字串轉換為西元年日期格式
	 * 
	 * @param String
	 *            reqDate 欲轉換的日期字串 yyyMMdd 1080306
	 * @return Date yyyyMMdd 20190306
	 */
	public static Date twconvertDate(String reqDate) {
		// 民國年日期格式
		String TWFORMAT = "yyyMMdd";
		// 西元年日期格式
		String WESFORMAT = "yyyyMMdd";
		// 將民國年的字串轉換為西元年字串格式
		String resDate = DateUtil.convertDate(1, reqDate, TWFORMAT, WESFORMAT);
		// 設定日期格式
		DateFormat sdf = new SimpleDateFormat(WESFORMAT);
		// 進行轉換
		Date date = new Date();
		try {
			date = sdf.parse(resDate);
		} catch (ParseException e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("twconvertDate error >> {}",e);
		}
		return date;
	}

	/**
	 * Ben增加
	 * 
	 */
	static SimpleDateFormat dtF = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	static SimpleDateFormat dtD = new SimpleDateFormat("yyyyMMdd");
	static SimpleDateFormat dtT = new SimpleDateFormat("HHmmss");
	static Map<String, SimpleDateFormat> pool = new HashMap();

	public static String format(String pattern, Date d) {
		SimpleDateFormat sdf = getDateFormatFromPool(pattern);
		return sdf.format(d);
	}

	private static SimpleDateFormat getDateFormatFromPool(String pattern) {
		SimpleDateFormat sdf = null;
		if (!pool.containsKey(pattern)) {
			sdf = new SimpleDateFormat(pattern);
			pool.put(pattern, sdf);
		} else {
			sdf = pool.get(pattern);
		}
		return sdf;
	}

	
	
	
	/**
	 * 將 1080201 格式的日期, 變成 108/02/01
	 */
	public static String addSlash2(String dateString) {
		StringBuffer stb_FormattedDate = new StringBuffer();

		try {
			if (dateString.indexOf("/") != -1)
				return dateString;

			if (dateString.length() == 6) {
				String year = dateString.substring(0, 2);
				String month = dateString.substring(2, 4);
				String day = dateString.substring(4);

				stb_FormattedDate.append("0");
				stb_FormattedDate.append(year);
				stb_FormattedDate.append("/");
				stb_FormattedDate.append(month);
				stb_FormattedDate.append("/");
				stb_FormattedDate.append(day);
			} else if (dateString.length() == 7) {
				String year = dateString.substring(0, 3);
				String month = dateString.substring(3, 5);
				String day = dateString.substring(5);

				stb_FormattedDate.append(year);
				stb_FormattedDate.append("/");
				stb_FormattedDate.append(month);
				stb_FormattedDate.append("/");
				stb_FormattedDate.append(day);
			} else if (dateString.length() == 8) {
				String year = null;

				if ("0".equals(dateString.substring(0, 1)))
					year = dateString.substring(1, 4);
				else
					year = dateString.substring(0, 4);

				String month = dateString.substring(4, 6);
				String day = dateString.substring(6);

				stb_FormattedDate.append(year);
				stb_FormattedDate.append("/");
				stb_FormattedDate.append(month);
				stb_FormattedDate.append("/");
				stb_FormattedDate.append(day);
			}
		} catch (Exception exc) {
			// Message.debug(exc);
			// throw new TopException("00001", "DateUtil.formatDate()");

			stb_FormattedDate.delete(0, stb_FormattedDate.length());
			stb_FormattedDate.append("日期格式錯誤:" + dateString);
		}

		return stb_FormattedDate.toString();
	}
	
	/**
	 *  將日期加以格式化, 例: 097/10/10, 2008/10/10
	 */
	public static String formatDate(String a_str_ADDate)
	{
		StringBuffer stb_FormattedDate = new StringBuffer();
		
		try
		{			
			if (a_str_ADDate.indexOf("/") != -1)
				return a_str_ADDate;
			
			if (a_str_ADDate.length() == 6)
			{
				String year = a_str_ADDate.substring(0, 2);
				String month = a_str_ADDate.substring(2, 4);
				String day = a_str_ADDate.substring(4);

				stb_FormattedDate.append("0");
				stb_FormattedDate.append(year);
				stb_FormattedDate.append("/");
				stb_FormattedDate.append(month);
				stb_FormattedDate.append("/");
				stb_FormattedDate.append(day);		
			}
			else if (a_str_ADDate.length() == 7)
			{
				String year = a_str_ADDate.substring(0, 3);
				String month = a_str_ADDate.substring(3, 5);
				String day = a_str_ADDate.substring(5);
				
				stb_FormattedDate.append(year);
				stb_FormattedDate.append("/");
				stb_FormattedDate.append(month);
				stb_FormattedDate.append("/");
				stb_FormattedDate.append(day);		
			}
			else if (a_str_ADDate.length() == 8)
			{
				String year = null;
				
				if ("0".equals(a_str_ADDate.substring(0,1)))
					year = a_str_ADDate.substring(1, 4);										
				else
					year = a_str_ADDate.substring(0, 4);					
				
				String month = a_str_ADDate.substring(4, 6);
				String day = a_str_ADDate.substring(6);
				
				stb_FormattedDate.append(year);
				stb_FormattedDate.append("/");
				stb_FormattedDate.append(month);
				stb_FormattedDate.append("/");
				stb_FormattedDate.append(day);	
			}
		}
		catch (Exception exc)
		{
			//Message.debug(exc);
			//throw new TopException("00001", "DateUtil.formatDate()");
			
			stb_FormattedDate.delete(0, stb_FormattedDate.length());
			stb_FormattedDate.append("日期格式錯誤:"+a_str_ADDate);
		}    	
				
		return stb_FormattedDate.toString();
	}
	
	/**
	 *  比對兩日期大小
	 */
	public static boolean time2compare(String time1, String time2, String sdfType) {
		boolean result = true;
		try {
			log.debug("time2compare.time1: {}", time1);
			log.debug("time2compare.time2: {}", time2);
			
			SimpleDateFormat sdf = new SimpleDateFormat(sdfType);
			Date a = sdf.parse(time1);  
			Date b = sdf.parse(time2);
			
			// Date類的一個方法,如果a早於b返回true,否則返回false
			result = a.before(b);
			log.debug("time2compare.result: {}", result);
			
		} catch (Exception e) {
			log.error("", e);
		}
		return result;
    }
	

	public static void main(String args[]) throws Exception {
		 System.out.println(time2compare("0000000 000000", "0000000 000001", "yyyyMMdd HHmmss"));
		
		// System.out.println(getMonthFirstDay());
		// System.out.println(getPrevMonthFirstDay());
		// System.out.println(getPrevMonthLastDay());
//		System.out.println();
//		getNextDate("MMdd");
//		getNextDate("yyyyMMdd");
//
//		getNext_N_Date("yyyy/MM/dd", 2);
//		System.out.println(getTheTime(""));
//		System.out.println(getDate(""));
		// date2DayofWeek("20161016" , "yyyyMMdd");
		// date2DayofWeekII("20161016" , "yyyyMMdd")
	}
	
	public static boolean checkDate(String oStartDate, String oEndDate, int range) {
		boolean result = false;
//		String today = getCurentDateTime("yyyy/MM/dd");
		SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date current = new Date();
		String d = sdFormat.format(current);
		int t = Integer.parseInt(d.toString().replaceAll("/", ""));
		int s = Integer.parseInt(oStartDate.replaceAll("/", ""));
		int e = Integer.parseInt(oEndDate.replaceAll("/", ""));
		try {
			String sYear = oStartDate.substring(0, 4);
			String sMonth = oStartDate.substring(5, 7);
			String sDay = oStartDate.substring(8, 10);
			
			String eYear = oEndDate.substring(0, 4);
			String eMonth = oEndDate.substring(5, 7);
			String eDay = oEndDate.substring(8, 10);
			if(s > t || e > t || s > e) {
				return false;
			}
			if(Integer.parseInt(eYear)-Integer.parseInt(sYear) == 0) {
				if(Integer.parseInt(eMonth)-Integer.parseInt(sMonth) > range) {
					return false;
				}
				else {
					result = true;
				}
			}
			else if(Integer.parseInt(eYear)-Integer.parseInt(sYear) > 0){
				int month = 0;
				int i = Integer.parseInt(eYear)-Integer.parseInt(sYear);
				if(Integer.parseInt(eMonth) >= Integer.parseInt(sMonth)) {
					month = Integer.parseInt(eMonth) - Integer.parseInt(sMonth);
				}
				else {
					month = -(Integer.parseInt(sMonth) - Integer.parseInt(eMonth));
				}
				month = month + (i * 12);
				if(month > range) {
					return false;
				}
				else {
					result = true;
				}
			}
		}
		catch (Exception ex) {
			log.error(""+ex);
		}
		
		
		return result;
	}
	
}
