/*
 * Copyright (c) 2017, FSTOP, Inc. All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tw.com.fstop.authc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 使用 LDAP 方式進行 AD 認證.
 * 認證時，同一個 AD 的帳號，所使用的 principal 可能會不相同，必需注意。
 * 
 * <pre>
 * dn
 * distinguished name
 * This refers to the name that uniquely identifies an entry in the directory.
 * 
 * dc
 * domain component
 * This refers to each component of the domain. For example www.google.com would be written as DC=www,DC=google,DC=com
 * 
 * ou
 * organizational unit
 * This refers to the organizational unit (or sometimes the user group) that the user is part of. If the user is part of more than one group, you may specify as such, e.g., OU= Lawyer,OU= Judge.
 * 
 * cn
 * common name
 * This refers to the individual object (person's name; meeting room; recipe name; job title; etc.) for whom/which you are querying.
 * 
 * </pre>
 *
 */
public class AuthenticationAdImpl
{
    //Log4j
    //private final static Logger log = Logger.getLogger(BaseJdbcDao.class.getName());
    //Slf4j
    private final static Logger log = LoggerFactory.getLogger(AuthenticationAdImpl.class);
    
    
	private static Hashtable<String, String> env = new Hashtable<String, String>();

	//172.17.200.1  or 172.17.200.2 or 172.17.200.3 or 192.168.8.110
	private String serverIP = "172.17.200.2";
	private String serverPort = "389";
	private String authenticationType = "simple";
	private String contextFactory = "com.sun.jndi.ldap.LdapCtxFactory";
	private String authenticationDC = "cn=Users,dc=xxx,dc=com";
	private String principal = "@xxx.com";
	
	/**
	 * Constructor
	 */
	public AuthenticationAdImpl()
	{
	    
	}
	
	/**
	 * Constructor with basic fields initialize.
	 * 
	 * @param ip AD server ip address
	 * @param port AD server port
	 * @param domain AD authenticate domain in "ou=xxx,cn=xxx,dc=xxx" form 
	 * @param principal AD authenticate principal
	 */
	public AuthenticationAdImpl(String ip, String port, String domain, String principal)
	{
	    this.serverIP = ip;
	    this.serverPort = port;
	    this.authenticationDC = domain;
	    this.principal = principal;
	}
	
	/**
	 * 使用 LDAP 方式與 AD 認證
	 * @param id		帳號
	 * @param password	密碼
	 * @return			目前傳回 null 表示失敗，若 AP 需要更詳細的回傳資訊
	 *                  需進行傳回字串的定義與加工
	 */
	@SuppressWarnings("unused")
    public String connect(String id, String password)
	{
		String ret = null;
	    String userDN = null;
	    String adDepartment;
	    String memberof;
	    DirContext ldapContext;
	    
	    /*
	       525  user not found
	       52e  invalid credentials
	       530  not permitted to logon at this time
	       531  not permitted to logon at this workstation
	       532  password expired
	       533  account disabled
	       701  account expired
	       773  user must reset password
	       775  user account locked      
	    */  

		try
		{
			LdapContext dirContext = null;
			env.put(Context.PROVIDER_URL, "ldap://" + getServerIP() + ":" + getServerPort());
		    
			//System.out.println(id + getPrincipal());
			//env.put(Context.SECURITY_PRINCIPAL, id + getPrincipal());   //OK
			env.put(Context.SECURITY_PRINCIPAL, id );   //OK
		    //env.put(Context.SECURITY_PRINCIPAL, "cn=" + id + "," + getAuthenticationDC()); //OK 表示知道 DC 組成		    
		    env.put(Context.SECURITY_CREDENTIALS, password);
		    //System.out.println("password=" + password);
		    env.put(Context.SECURITY_AUTHENTICATION, getAuthenticationType());
		    env.put(Context.INITIAL_CONTEXT_FACTORY, getContextFactory());
		    dirContext = new InitialLdapContext(env, null);
		    
		    System.out.println("authenticate successfully");
/*		    
		    SearchControls controls = new SearchControls();
		    controls.setReturningObjFlag(false);
		    controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		    String filter = "(sAMAccountName=" + id + ")";  //OK
		    NamingEnumeration answer = dirContext.search(getAuthenticationDC(), filter, controls);
		    ret = "";
		    while (answer.hasMore()) 
		    {
		        userDN = ((NameClassPair) answer.nextElement()).getName();
		        System.out.println("取得帳號為:" + id + "，userDN==>"  + userDN + "\n");
		        ret += userDN + ";";
		    }
*/			    	    
		    log.debug("authenticat success");
		    ret = "OK";
		}
	    catch(javax.naming.AuthenticationException e) 
	    {
	        log.error("authenticat exception=", e);
	        return ret;
	    }
	    catch (javax.naming.CommunicationException e)
	    {
	        log.error("authenticat exception=", e);
	        return ret;
	    }
	    catch (NamingException e) 
	    {
	        log.error("authenticat exception=", e);
	        return ret;
	    }
	    catch (Exception e) 
	    {
	        log.error("authenticat exception=", e);
	        return ret;
	    }
		
		return ret;
	}

	public String getServerIP() {
		return serverIP;
	}

	public void setServerIP(String serverIP) {
		this.serverIP = serverIP;
	}

	public String getServerPort() {
		return serverPort;
	}

	public void setServerPort(String serverPort) {
		this.serverPort = serverPort;
	}

	public String getAuthenticationType() {
		return authenticationType;
	}

	public void setAuthenticationType(String authenticationType) {
		this.authenticationType = authenticationType;
	}

	public String getContextFactory() {
		return contextFactory;
	}

	public void setContextFactory(String contextFactory) {
		this.contextFactory = contextFactory;
	}

	public String getAuthenticationDC() {
		return authenticationDC;
	}

	public void setAuthenticationDC(String authenticationDC) {
		this.authenticationDC = authenticationDC;
	}
	
	public String getPrincipal() {
		return principal;
	}

	public void setPrincipal(String principal) {
		this.principal = principal;
	}


	
}
