
/*
 * Copyright (c) 2017, FSTOP, Inc. All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tw.com.fstop.common;

/**
 * Home brew Precondition check. 
 * Runtime exceptions are used.
 * <pre>
 * Supports JDK 5+.
 * Google Guava library should be used if JDK version 7 above can be used.
 * These class functions are NOT for business logic check! 
 * 請勿使用本類別的功能進行業務邏輯檢核!
 * </pre>
 *
 * @since 1.0.1
 */
public final class Preconditions
{
    /**
     * Ensures the truth of an expression involving one or more parameters to the calling method.
     * 
     * @param expression a boolean expression
     * @param errorMessage the exception message to use if the check fails; 
     *                      will be converted to a string using String.valueOf(Object)
     * @throws IllegalArgumentException if expression is false                      
     */
    public static void checkArgument(boolean expression, Object errorMessage) throws IllegalArgumentException
    {
        if(!expression) 
        {
            throw new IllegalArgumentException(String.valueOf(errorMessage));
        }
    }

    /**
     * Ensures the truth of an expression involving one or more parameters to the calling method.
     * 
     * @param expression a boolean expression
     * @throws IllegalArgumentException if expression is false  
     */
    public static void checkArgument(boolean expression) throws IllegalArgumentException
    {
        checkArgument(expression, "");
    }
    
    /**
     * Ensures that an object reference passed as a parameter to the calling method is not null.
     * 
     * @param reference an object reference
     * @param errorMessage the exception message to use if the check fails; 
     *                      will be converted to a string using String.valueOf(Object)
     * @return the non-null reference that was validated
     * @throws NullPointerException if reference is null                    
     */
    public static <T> T checkNotNull(T reference, Object errorMessage) throws NullPointerException
    {
        if (reference == null)
        {
            throw new NullPointerException(String.valueOf(errorMessage));
        }
        return reference;
    }

    /**
     * Ensures that an object reference passed as a parameter to the calling method is not null.
     * 
     * @param reference an object reference
     * @return the non-null reference that was validated
     * @throws NullPointerException if reference is null  
     */
    public static <T> T checkNotNull(T reference) throws NullPointerException
    {
        return checkNotNull(reference, "");
    }
    
    /**
     * Ensures the truth of an expression involving the state of the calling instance, 
     * but not involving any parameters to the calling method.
     * 
     * @param expression a boolean expression
     * @param errorMessage the exception message to use if the check fails; 
     *                      will be converted to a string using String.valueOf(Object)
     * @throws IllegalStateException if expression is false
     */
    public static void checkState(boolean expression, Object errorMessage) throws IllegalStateException
    {
        if(!expression) 
        {
            throw new IllegalStateException(String.valueOf(errorMessage));
        }       
    }
    
    public static void checkState(boolean expression) throws IllegalStateException
    {
        checkState(expression, "");   
    }
    
    
}
