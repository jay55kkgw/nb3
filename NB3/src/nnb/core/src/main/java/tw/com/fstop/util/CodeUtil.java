package tw.com.fstop.util;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.imageio.ImageIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.lang.NonNull;

import com.google.gson.Gson;

import tw.com.fstop.common.BaseResult;

public class CodeUtil {
	static Logger log = LoggerFactory.getLogger(CodeUtil.class);
	static Gson gs = new Gson();
	static int HIDELENGHTSTAR = 5;// 倒數5
	static int HIDELENGHTEND = 3;// 倒數3
	public static Boolean IS_AP1_LIVE = Boolean.TRUE;
	public static Boolean IS_AP2_LIVE = Boolean.TRUE;
	public static String d4regex = "(([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3})(((0[13578]|1[02])(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)(0[1-9]|[12][0-9]|30))|(02(0[1-9]|[1][0-9]|2[0-8]))))|((([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00))-02-29)";
	private static final String MAC_NAME = "HmacSHA256";
	public static final String ENCODING = "UTF-8";

	public static String toJson(Object obj) {
		return gs.toJson(obj);
	}

	/**
	 * 轉json前移除敏感資料只接受Map<String,String>
	 * 
	 * @param map
	 * @return
	 */
	public static String toJsonCustom(Map<String, String> map) {
		String result = "";
		try {
			Map<String, String> newmap = new HashMap<String, String>();
			newmap.putAll(map);
			String s = "N950PASSWORD,CMPASSWORD,CMPWD,ConfirmNewpwd,Newpw,Newpwd,Newpwd_SHOW,Oldpwd,Oldpwd_SHOW,Oldpw,pinnew,DPNTXPD,DPNSTXPD,DPTXPD,DPNSIPD,DPNSSIPD,USERNAME,DPSIPD,OLDUID,NEWUID";
			List<String> list = Arrays.asList(s.split(","));
			for (String key : newmap.keySet()) {
				if (list.contains(key)) {
					newmap.get(key).length();
					newmap.put(key, mask(newmap.get(key), 0, newmap.get(key).length(), '*'));
				}
			}

			result = gs.toJson(newmap);

		} catch (Exception e) {
			log.error("toJsonCustom.error>>", e);
		}
		return result;
	}

	public static <T> T fromJson(String json, Class<T> to) {
		return gs.fromJson(json, to);
	}

	public static void convert2BaseResult(BaseResult bs, Object obj) {
		convert2BaseResult(bs, obj, null, null);
	}

	public static void convert2BaseResult(BaseResult bs, Object obj, Boolean showLog) {
		if (showLog = true)
			convert2BaseResultNoLog(bs, obj, null, null);
		else
			convert2BaseResult(bs, obj, null, null);
	}

	public static <T> T objectCovert(Class<T> to, Object from) {
		String json = null;
		if (from == null) {
			log.error("  from Object is null");
			return null;
		}
		json = gs.toJson(from);
		T t = gs.fromJson(json, to);

		return t;
	}

	public static void convert2BaseResult(BaseResult bs, Object obj, @NonNull String msgCode, @NonNull String message) {
		// Gson gs = new Gson();
		String json = null;
		try {
			if (bs == null) {
				log.warn("警告 BaseResult is null...");
			}
			if (StrUtil.isNotEmpty(msgCode)) {
				bs.setMsgCode(msgCode);
			}
			if (StrUtil.isNotEmpty(message)) {
				bs.setMessage(message);
			}

			json = gs.toJson(obj);
			log.trace(ESAPIUtil.vaildLog("convert2Baseresult.json>> {}" + json));
			log.trace(ESAPIUtil
					.vaildLog("convert2Baseresult.jsontoMap>> {}" + CodeUtil.toJson(gs.fromJson(json, Map.class))));
			bs.setTBB_WS_Result(gs.fromJson(json, Map.class));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("convert2BaseResult.error>>", e);
		}
	}

	public static void convert2BaseResultNoLog(BaseResult bs, Object obj, @NonNull String msgCode,
			@NonNull String message) {
		// Gson gs = new Gson();
		String json = null;
		try {
			log.trace("convert2Baseresult...");
			if (bs == null) {
				log.warn("警告 BaseResult is null...");
			}
			if (StrUtil.isNotEmpty(msgCode)) {
				bs.setMsgCode(msgCode);
			}
			if (StrUtil.isNotEmpty(message)) {
				bs.setMessage(message);
			}

			json = gs.toJson(obj);
			// log.trace("convert2Baseresult.json>> {}", json);
			// log.trace("convert2Baseresult.jsontoMap>> {}", gs.fromJson(json, Map.class));
			bs.setTBB_WS_Result(gs.fromJson(json, Map.class));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("convert2BaseResult.error>>", e);
		}
	}

	/*
	 * 物件轉BYTE
	 */
	public byte[] objectToByte(Object object) {
		byte[] result = null;
		ObjectOutputStream objectOutputStream = null;

		try {
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
			objectOutputStream.writeObject(object);
			result = byteArrayOutputStream.toByteArray();
		} catch (Exception e) {
			// e.printStackTrace();
			log.error(String.valueOf(e));
		}
		if (objectOutputStream != null) {
			try {
				objectOutputStream.close();
			} catch (Exception e) {
				// e.printStackTrace();
				log.error(String.valueOf(e));
			}
		}
		return result;
	}

	/*
	 * BYTE轉物件
	 */
	public Object byteToObject(byte[] byteArray) {
		Object result = null;
		ByteArrayInputStream byteArrayInputStream = null;
		ObjectInputStream objectInputStream = null;

		try {
			byteArrayInputStream = new ByteArrayInputStream(byteArray);
			objectInputStream = new ObjectInputStream(byteArrayInputStream);
			result = objectInputStream.readObject();
		} catch (Exception e) {
			// e.printStackTrace();
			log.error(String.valueOf(e));
		}
		if (objectInputStream != null) {
			try {
				objectInputStream.close();
			} catch (Exception e) {
				// e.printStackTrace();
				log.error(String.valueOf(e));
			}
		}
		if (byteArrayInputStream != null) {
			try {
				byteArrayInputStream.close();
			} catch (Exception e) {
				// e.printStackTrace();
				log.error(String.valueOf(e));
			}
		}
		return result;
	}

	/**
	 * 字串遮罩
	 * 
	 * @param text
	 * @param start
	 * @param length
	 * @param maskSymbol
	 * @return
	 */
	public static String mask(String text, int start, int length, char maskSymbol) {
		if (text == null || text.isEmpty()) {
			return "";
		}
		if (start < 0) {
			start = 0;
		}
		if (length < 1) {
			return text;
		}

		StringBuilder sb = new StringBuilder();
		char[] cc = text.toCharArray();
		for (int i = 0; i < cc.length; i++) {
			if (i >= start && i < (start + length)) {
				sb.append(maskSymbol);
			} else {
				sb.append(cc[i]);
			}
		}
		return sb.toString();
	}

	/**
	 * 字串遮罩從後面開始
	 * 
	 * @param replaceStr
	 *            遮罩字串
	 * @return
	 */
	public static String strReplaceByMask(String replaceStr) {
		int startIndex = replaceStr.length() - HIDELENGHTSTAR;// 開始
		int endIndex = replaceStr.length() + 1 - HIDELENGHTEND;// 結束
		String replaceSymbol = "*";// 替換字元
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < replaceStr.length(); i++) {
			char number = replaceStr.charAt(i);
			if (i >= startIndex && i < endIndex) {
				stringBuilder.append(replaceSymbol);
			} else {
				stringBuilder.append(number);
			}
		}
		return String.valueOf(stringBuilder);
	}

	/*
	 * HEX轉成BYTE
	 */
	public static byte[] hexStringToByteArray(String string) {
		int length = string.length();
		byte[] data = new byte[length / 2];
		for (int i = 0; i < length; i += 2) {
			data[i / 2] = (byte) ((Character.digit(string.charAt(i), 16) << 4)
					+ Character.digit(string.charAt(i + 1), 16));
		}
		return data;
	}

	/**
	 * 在JAVA中生成UUID字符串的有效方法（UUID.randomUUID（）。的toString（），不帶破折號）
	 * 
	 * @return
	 */
	public static String generateUniqueId() {
		String uuid = UUID.randomUUID().toString().replace("-", "");
		return uuid;
	}

	/**
	 * 遮蔽帳號：遮7、8、9碼(含信託帳號)
	 * 
	 * @author Owner
	 * 
	 */
	public static String hideaccount(String originalStr) {
		String header = "";
		String middle = "";
		String tailer = "";
		String newstr = "";
		if (originalStr.length() > 0) {
			if (originalStr.length() == 9) {
				header = originalStr.substring(0, 6);
				middle = "***";
				tailer = "";
			} else if (originalStr.length() > 9) {
				header = originalStr.substring(0, 6);
				middle = "***";
				tailer = originalStr.substring(9);
			}
		}
		newstr = header + middle + tailer;
		return newstr;
	}

	/**
	 * 遮蔽信用卡號：遮第14及第15碼
	 * 
	 * @author Owner
	 * 
	 */
	public static String hidecardnum(String originalStr) {
		String header = "";
		String middle = "";
		String tailer = "";
		String newstr = "";
		if (originalStr.length() > 0) {
			if (originalStr.length() >= 16) {
				header = originalStr.substring(0, originalStr.length() - 3);
				middle = "**";
				tailer = originalStr.substring(originalStr.length() - 1);
			}
		}
		newstr = header + middle + tailer;
		return newstr;
	}

	/**
	 * 遮蔽姓名：二個字遮第二個字，超過二個字保留頭尾，中間遮掉
	 * 
	 * @author Owner
	 * 
	 */
	public static String hideusername(String originalStr) {
		String header = "";
		String middle = "";
		String tailer = "";
		String newstr = "";
		String originalStr_1 = convertFullorHalf(originalStr.replace((char) 12288, ' ').trim(), 1);
		if (originalStr_1.length() > 0) {
			if (originalStr_1.length() == 2) {
				header = originalStr_1.substring(0, 1);
				middle = "０";
				newstr = "";
			} else if (originalStr_1.length() == 3) {
				header = originalStr_1.substring(0, 1);
				middle = "０";
				tailer = originalStr_1.substring(2, 3);
			} else {
				header = originalStr_1.substring(0, 1);
				for (int i = 0; i < originalStr_1.length() - 2; i++) {
					middle = middle + "０";
				}
				tailer = originalStr_1.substring(originalStr_1.length());
			}
		}
		newstr = header + middle + tailer;
		return newstr;
	}

	// 轉換全型半型 option 0: toHalf, 1:toFull
	public static String convertFullorHalf(String originalStr, int option) {
		String[] asciiTable = { " ", "!", "\"", "#", "$", "%", "&", "'", "(", ")", "*", "+", ",", "-", ".", "/", "0",
				"1", "2", "3", "4", "5", "6", "7", "8", "9", ":", ";", "<", "=", ">", "?", "@", "A", "B", "C", "D", "E",
				"F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
				"[", "\\", "]", "^", "_", "`", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n",
				"o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "{", "|", "}", "~" };
		String[] big5Table = { "　", "！", "”", "＃", "＄", "％", "＆", "’", "（", "）", "＊", "＋", "，", "－", "\u2027", "／", "０",
				"１", "２", "３", "４", "５", "６", "７", "８", "９", "：", "；", "＜", "＝", "＞", "？", "＠", "Ａ", "Ｂ", "Ｃ", "Ｄ", "Ｅ",
				"Ｆ", "Ｇ", "Ｈ", "Ｉ", "Ｊ", "Ｋ", "Ｌ", "Ｍ", "Ｎ", "Ｏ", "Ｐ", "Ｑ", "Ｒ", "Ｓ", "Ｔ", "Ｕ", "Ｖ", "Ｗ", "Ｘ", "Ｙ", "Ｚ",
				"〔", "\uFE68", "〕", "︿", "＿", "\uFF40", "ａ", "ｂ", "ｃ", "ｄ", "ｅ", "ｆ", "ｇ", "ｈ", "ｉ", "ｊ", "ｋ", "ｌ", "ｍ",
				"ｎ", "ｏ", "ｐ", "ｑ", "ｒ", "ｓ", "ｔ", "ｕ", "ｖ", "ｗ", "ｘ", "ｙ", "ｚ", "｛", "｜", "｝", "\uFF5E" };

		if (asciiTable.length == big5Table.length) {
			// System.out.println("對照表長度正確！ 長度為： " + asciiTable.length);
			// 開始轉換
			if (originalStr == null || "".equalsIgnoreCase(originalStr)) {
				return "";
			}
			for (int i = 0; i < asciiTable.length; i++) {
				// 印出對照表
				// System.out.println((i + 1) + ": " + asciiTable[i] + "\t" + big5Table[i]);
				// 開始轉換
				if (option == 0)// to Half
				{
					originalStr = originalStr.replace(big5Table[i].charAt(0), asciiTable[i].charAt(0));// 不可以用replaceAll，會因regular
																										// expression出錯
				}
				if (option == 1)// to Full
				{
					originalStr = originalStr.replace(asciiTable[i].charAt(0), big5Table[i].charAt(0));// 不可以用replaceAll，會因regular
																										// expression出錯
				}
			}
			return originalStr;
		} else {
			// System.out.println("對照表長度不正確！ asciiTable長度為： " + asciiTable.length);
			// System.out.println("對照表長度不正確！ big5Table長度為： " + big5Table.length);
			return originalStr;
		}
	}

	/**
	 * 根據模板靜態頁面ToString
	 * 
	 * @param filePath
	 *            路徑/模板名稱 <br>
	 *            模板中要替換的key要用"##" + key + "##"
	 * 
	 * @param valuesMap傳入要替換的map
	 * @return
	 */
	public static String HtmlToString(String filePath, Map<String, Object> valuesMap) {
		String str = "";
		InputStream input = null;
		InputStreamReader inputReader = null;
		try {
			log.debug("HtmlToString filePath >>>>>>>>{}", filePath);
			log.debug("HtmlToString input >>>>>>>>{}", valuesMap);
			String tempStr = "";
			Resource resource = new ClassPathResource(filePath);
			// File file = resource.getFile();
			input = resource.getInputStream();
			inputReader = new InputStreamReader(input, "UTF-8");
			BufferedReader br = new BufferedReader(inputReader);
			while ((tempStr = br.readLine()) != null) {
				str = str + tempStr;
			}
			// 傳入要替換的map
			log.debug("valuesMap >>>>>>>> {}", valuesMap);
			// 替換掉模塊中的地方
			for (String key : valuesMap.keySet()) {
				if (valuesMap.get(key) != null) {
					str = str.replace("##" + key + "##", String.valueOf(valuesMap.get(key)));
				} else {
					str = str.replace(key, "");
				}
			}
			input.close();
			inputReader.close();
		} catch (IOException e) {
			log.error("JspToHtmlFile :", e);
			return str;
		} finally {
			// ** 無論如何都必須釋放連接.
			try {
				if (input != null) {
					input.close();
				}
				if (inputReader != null) {
					inputReader.close();
				}
			} catch (IOException e) {
				log.error("JspToHtmlFile finally:" + e, e);
			}
		}
		log.debug(ESAPIUtil.vaildLog("HtmlToString out >> " + str));
		return str;
	}

	// 獲取on docker的機器IP
	public static String getIpAddress() throws UnknownHostException {
		InetAddress address = InetAddress.getLocalHost();
		String sys_ip = address.getHostAddress();
		return sys_ip;
	}

	/**
	 * 將輸入的map字串轉為map物件
	 *
	 * @param str
	 *            EX : String value = "{ACN=01077258228, TYPE=1, FDPNUM=0009205}";
	 * @return EX : Map {FDPNUM=0009205, ACN=01077258228, TYPE=1}
	 */
	public Map<String, String> StringConvertMap(String str) {
		Map<String, String> map = new HashMap<>();
		try {
			str = str.substring(1, str.length() - 1); // remove curly brackets
			String[] keyValuePairs = str.split(","); // split the string to creat key-value pairs
			for (String pair : keyValuePairs) // iterate over the pairs
			{
				String[] entry = pair.split("="); // split the pairs to get key and value
				if (entry.length > 1) {
					map.put(entry[0].trim(), entry[1].trim()); // add them to the hashmap and trim whitespaces
				}
			}
		} catch (Exception e) {
			log.error("Exception: {}  and map:{}", e, map);
		}
		return map;
	}

	/**
	 * 取得檔案類型
	 *
	 * @param BufferedImage
	 *            : 資料源
	 * @param path
	 *            : 圖檔儲存路徑
	 */
	public static String getFileType(String fileName) {
		String type = "";
		try {
			log.trace(ESAPIUtil.vaildLog("getFileType.fileName: " + fileName));
			int lastIndexOf = fileName.lastIndexOf(".");
			if (lastIndexOf != -1) {
				type = fileName.substring(lastIndexOf + 1);
			}
			log.trace(ESAPIUtil.vaildLog("getFileType.type: " + type));
		} catch (Exception e) {
			log.error("", e);
		}
		return type;
	}

	/**
	 * 將輸入的BufferedImage轉成File
	 *
	 * @param BufferedImage
	 *            : 資料源
	 * @param path
	 *            : 圖檔儲存路徑
	 */
	public static void bufferedimageConvertImage(BufferedImage bufferedImage, String type, String path) {
		// BufferedImage bufferedImage;
		try {
			// read image file
			// bufferedImage = ImageIO.read(file);

			// create a blank, RGB, same width and height, and a white background
			BufferedImage newBufferedImage = new BufferedImage(bufferedImage.getWidth(), bufferedImage.getHeight(),
					BufferedImage.TYPE_INT_RGB);
			newBufferedImage.createGraphics().drawImage(bufferedImage, 0, 0, Color.WHITE, null);

			// write to jpeg file
			ImageIO.write(newBufferedImage, type, new File(path));

			log.debug("CodeUtil.bufferedimageConvertImage Done!");

		} catch (IOException e) {
			log.error("", e);
		}
	}

	/**
	 * convert a byte[] to a BufferedImage
	 *
	 * @param BufferedImage
	 *            : 資料源
	 * @param path
	 *            : 圖檔儲存路徑
	 */
	public static BufferedImage createImageFromBytes(byte[] imageData) {
		ByteArrayInputStream bais = new ByteArrayInputStream(imageData);
		try {
			return ImageIO.read(bais);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 驗證日期格式
	 * 
	 * @param regex
	 *            正則表示式，未輸入會使用d4regex 只驗證yyyymmdd
	 * @param orginal
	 *            日期
	 * @return
	 */
	public static boolean isMatch(String regex, String orginal) {
		if (StrUtil.isEmpty(regex)) {
			regex = d4regex;
		}

		if (StrUtil.isEmpty(orginal)) {
			return false;
		}
		Pattern pattern = Pattern.compile(regex);
		Matcher isNum = pattern.matcher(orginal);
		return isNum.matches();
		// return orginal.matches(regex);
	}

	public static byte[] hmacSHA1Encrypt(String encryptText, byte[] encryptKey) throws Exception {
		// byte[] data= encryptKey.getBytes(ENCODING);
		SecretKey secretKey = new SecretKeySpec(encryptKey, MAC_NAME);
		Mac mac = Mac.getInstance(MAC_NAME);

		mac.init(secretKey);

		byte[] text = encryptText.getBytes(ENCODING);
		byte[] b = mac.doFinal(text);
		return b;
		// return mac.doFinal(text);
	}

	private static byte[] sign(String data, PrivateKey priKey) throws Exception {
		Signature rsa = Signature.getInstance("SHA256withRSA");
		rsa.initSign(priKey);
		rsa.update(data.getBytes("UTF-8"));
		return rsa.sign();
	}

	// 生成隨機數字和字母,
	public static String getStringRandom(int length) {

		String val = "";
		SecureRandom random = new SecureRandom();

		// 參數length，表示生成幾位隨機數
		for (int i = 0; i < length; i++) {

			String charOrNum = random.nextInt(2) % 2 == 0 ? "char" : "num";
			// 輸出字母還是數字
			if ("char".equalsIgnoreCase(charOrNum)) {
				// 輸出是大寫字母還是小寫字母
				int temp = random.nextInt(2) % 2 == 0 ? 65 : 97;
				val += (char) (random.nextInt(26) + temp);
			} else if ("num".equalsIgnoreCase(charOrNum)) {
				val += String.valueOf(random.nextInt(10));
			}
		}
		return val;
	}

	/*
	 * 
	 * 
	 * 
	 */
	public static String aes_decode(Map<String,String> reqMap) throws NoSuchAlgorithmException, UnsupportedEncodingException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
		String decodeString = "";

		KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        // 設定特定的隨機數種子，
        // 如隨機數種子不變，就會得到相同的隨機數，
        // 這裡相當於每次都使用同樣的金鑰
        SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
        secureRandom.setSeed(reqMap.get("seedForRandom").getBytes("UTF-8"));
        
        keyGenerator.init(256, secureRandom);
        SecretKey secretKey = keyGenerator.generateKey();
        SecretKey AES_secretKey = new SecretKeySpec(secretKey.getEncoded(), "AES");
        // 取得 AES 加解密器
        Cipher cipher = Cipher.getInstance("AES");
        
        
        cipher.init(Cipher.DECRYPT_MODE, AES_secretKey);
		byte[] decryptedText_byteArray = cipher.doFinal(Base64.getDecoder().decode(URLDecoder.decode(reqMap.get("url"),"UTF-8")));
		decodeString = new String(decryptedText_byteArray, "UTF-8");

		return decodeString;

	}
	
	// @前留第一碼跟最後
	public static String emailMask(String originalStr) {
		StringBuffer newstr = new StringBuffer();
		String[] spilt = originalStr.split("@");
		String part1 = spilt[0];
		part1 = mask(part1,1,part1.length()-2,'*');
		newstr.append(part1);
		newstr.append("@");
		newstr.append(spilt[1]);
		return newstr.toString();
	}
}
