
/*
 * Copyright (c) 2017, FSTOP, Inc. All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tw.com.fstop.dao;

/**
 * Generic interface for DAO.
 * 
 * @param <T> DAO type
 * @param <K> DAO key type
 *
 * @since 1.0.1
 */
public interface GenericDao<T, K>
{
    public int create(T obj);
    public T read(K key);
    public int update(T obj);
    public int delete(K key);
    
}
