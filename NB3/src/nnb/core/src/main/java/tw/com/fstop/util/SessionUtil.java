package tw.com.fstop.util;

import java.util.Map;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.ui.Model;

public class SessionUtil {
	/**
	 * 定義session key 避免key重複
	 */
//	使用者是否為第一次登入新網銀
	public static final String FIRSTLOGIN = "firstlogin";
//	登入後取得IIUD
	public static final String TOKENID = "tokenid";
//	登入後取得Timeout時間
	public static final String TIMEOUT = "timeout";
//	因應部分電文需要不須重新key密碼 ，所以存session ，加解密Base64
	public static final String PD = "pd";
//	因應部分電文需要身分字號查詢帳號
	public static final String CUSIDN = "cusidn";
//	未登入之線上申請功能--線上開立基金戶
	public static final String CUSIDN_N361 = "cusidn_n361";
//	使用者預設mail
	public static final String DPMYEMAIL = "dpmyemail";
//	業務權限 ALL : 一般網銀臨櫃申請使用者 ATM : 金融卡申請網銀使用者,ATMT : 金融卡申請網銀使用者(線上申請交易功能), CRD : 信用卡申請網銀使用者
	public static final String AUTHORITY ="authority";
//	台幣轉帳資料
	public static final String TRANSFER_DATA = "transfer_data";
//	登入用驗證碼
	public static final String KAPTCHA_SESSION_KEY = "kaptcha_session_key";
//	交易用驗證碼
	public static final String TRANS_KAPTCHA_SESSION_KEY = "trans_kaptcha_session_key";
//	台幣轉帳confirm token
	public static final String TRANSFER_CONFIRM_TOKEN = "transfer_confirm_token";
//	台幣轉帳result token
	public static final String TRANSFER_RESULT_TOKEN = "transfer_result_token";
//	台幣轉帳完成 token
	public static final String TRANSFER_RESULT_FINSH_TOKEN = "transfer_result_finsh_token";
//  使用者代號
	public static final String USERID = "userid";
//	使用者姓名
	public static final String DPUSERNAME = "dpusername";
//	使用者姓名--首頁更新顯示於menu
	public static final String SUSERNAME = "susername";
//  登入時間	
	public static final String LOGINTIME = "login_time";
//  轉入臺幣綜存定存完成 Token	
	public static final String NT_DEPOSIT_TRANSFER_FINSH_TOKEN = "nt_deposit_transfer_finsh_token";
//  台幣定存解約完成 Token	
	public static final String NT_TDEPOSIT_CANCEL_FINSH_TOKEN = "nt_deposit_cancel_finsh_token";
//  臺幣定存自動轉期申請/變更完成 Token	
	public static final String NT_RENEWAL_APPLY_FINSH_TOKEN = "nt_renewal_apply_finsh_token";
//	臺幣定存單到期續存完成 Token	
	public static final String NT_ORDER_RENEWAL_FINSH_TOKEN = "nt_order_renewal_finsh_token";
//  臺幣零存整付按月繳存完成  Token	
	public static final String NT_INSTALLMENT_SAVING_FINSH_TOKEN = "nt_installment_saving_finsh_token";
//  轉入外匯綜存定存完成 Token	
	public static final String FCY_DEPOSIT_TRANSFER_FINSH_TOKEN = "fcy_deposit_transfer_finsh_token";
//  外匯定存解約完成 Token	
	public static final String FCY_TDEPOSIT_CANCEL_FINSH_TOKEN = "fcy_deposit_cancel_finsh_token";
//  外匯定存自動轉期申請/變更完成 Token	
	public static final String FCY_RENEWAL_APPLY_FINSH_TOKEN = "fcy_renewal_apply_finsh_token";
//	外匯定存單到期續存完成 Token	
	public static final String FCY_ORDER_RENEWAL_FINSH_TOKEN = "fcy_order_renewal_finsh_token";
//	下載EXCEL或TXT會用到的資料
	public static final String DOWNLOAD_DATALISTMAP_DATA = "download_datalistmap_data";
//	列印結果頁會用到的資料
	public static final String PRINT_DATALISTMAP_DATA = "print_datalistmap_data";
//	各種BHO
	//臺幣轉帳
	public static final String TRANSFER_BHOLOCATION = "transfer_bholocation";
	public static final String TRANSFER_TRANSINACCOUNT = "transfer_transinaccount";
	//繳納本行信用卡款
	public static final String CREDITCARDPAY_BOHLOCATION = "creditcardpay_bholocation";
	public static final String CREDITCARDPAY__TRANSINACCOUNT = "creditcardpay_transinaccount";
	//	轉入臺幣綜存定存BHO
	public static final String DEPOSIT_TRANSFER_BHOLOCATION = "deposit_transfer_bholocation";
	public static final String DEPOSIT_TRANSFER_TRANSINACCOUNT = "deposit_transfer_transinaccount";
	//	臺幣零存整付按月繳存BHO
	public static final String INSTALLMENT_SAVING_BHOLOCATION = "installment_saving_bholocation";
	public static final String INSTALLMENT_SAVING__TRANSINACCOUNT = "installment_saving_transinaccount";
	// step1切換語系的暫存資料
	public static final String STEP1_LOCALE_DATA = "step1_locale_data";
	// step2切換語系的暫存資料
	public static final String STEP2_LOCALE_DATA = "step2_locale_data";
	// step3切換語系的暫存資料
	public static final String STEP3_LOCALE_DATA = "step3_locale_data";
	// step4切換語系的暫存資料
	public static final String STEP4_LOCALE_DATA = "step4_locale_data";
	// step4_2切換語系的暫存資料
	public static final String STEP4_2_LOCALE_DATA = "step4_2_locale_data";
	// step4_3切換語系的暫存資料
	public static final String STEP4_3_LOCALE_DATA = "step4_3_locale_data";
//	台幣轉帳 確認頁切換語系的暫存資料
	public static final String CONFIRM_LOCALE_DATA = "confirm_locale_data";
//	台幣轉帳 結果頁切換語系的暫存資料
	public static final String RESULT_LOCALE_DATA = "result_locale_data";
	//交易單據
	public static final String FXCERTPREVIEW_DATA ="fxcertpreview_data";
	
//	N911 判斷是否為IKEY使用者欄位
	public static final String LOGINDT = "logindt";
//	N911 判斷是否為IKEY使用者欄位
	public static final String LOGINTM = "logintm";
//	N911 憑證代碼
	public static final String XMLCOD = "xmlcod";
//	N911 Client端IP
	public static final String USERIP = "userip";
//	N911 行動銀行啟用狀態
	public static final String MBSTAT = "mbstat";
//	N911 行動銀行上次啟用/信用日期
	public static final String MBOPNDT = "mbopndt";
//	N911 行動銀行上次啟用/信用時間
	public static final String MBOPNTM = "mbopntm";
//	N911 是否為行員
	public static final String STAFF = "staff";
//	N911 全球金融網是否轉置
	public static final String HKCODE = "hkcode";
//	是否是IKEY使用者
	public static final String ISIKEYUSER = "isikeyuser";
	
//  回上一頁 資料
	public static final String BACK_DATA = "back_data";
	
//	KYC問卷日期
	public static final String KYCDATE = "kycdate";
//	弱勢族群
	public static final String WEAK = "weak";
//	KYC是否PASS
	public static final String KYC = "kyc";
//	黃金存摺網路交易申請暨約定書同意的SIGNAL
	public static final String GOLDAGREEFLAG = "goldagreeflag";
//	基金餘額及損益查詢的總參考市值
	public static final String C012TOTAL = "c012total";
//  線上申請貸款種類
	public static final String LOANTYPE = "loantype";
//	線上申請Flag
	public static final String ONLINEAPPLY = "onlineApply";
//	線上信託開戶Flag
	public static final String N361 = "n361";
	public static final String N361_1 = "n361_1";
	public static final String N361_2 = "n361_2";
	public static final String N361_3 = "n361_3";
	public static final String N361_4 = "n361_4";
//	線上信託開戶 使用者填寫的問卷資料
	public static final String N361_KYC = "n361_kyc";
//	線上信託開戶 使用者確認的問卷資料
	public static final String N361_KYC_CONFIRM = "n361_kyc_confirm";
//	線上信託開戶 約定事項資料	
	public static final String N361_5 = "n361_5";
//	線上預約開立存款戶資料	
	public static final String N201_CONFIRM_DATA = "n201_confirm_data";
//	線上預約開立存款戶資料	
	public static final String N201_CONFIRM_DATA_2 = "n201_confirm_data_2";
//	線上預約開立存款戶資料	
	public static final String N201_CONFIRM_DATA_3 = "n201_confirm_data_3";
//	線上申請資料	
	public static final String ONLINE_APPLY_DATA = "online_apply_data";
	// N361 JSONDC參數
	public static final String N361_JSONDC = "n361_jsondc";
	//黃金定期定額條款同意FLAG
	public static final String GOLDAGREEFLAG_PERIOD = "goldagreeflag_period";
	//外匯條款同意FLAG
	public static final String FXAGREEFLAG = "fxagreeflag";
	// 線上申請新臺幣存款帳戶結清銷戶
	public static final String CLOSING_TW_ACCOUNT = "closing_tw_account";
	// menu
	public static final String MENU = "menu";
	// AOP 記錄TXN_LOG 用
	public static final String ADGUID = "adguid";
	// A106 使用者基本資料填寫
	public static final String USERDATA = "userdata";
	// 每次登入只寄信一次
	public static final String SENDMAIL = "sendmail";
	// 功能代碼--快速選單(我的最愛)
	public static final String ADOPID = "adopid";
	
	//	是否打開憑證註冊中心連結
	public static final String ISCERTIFICATIONREGISTERY = "isCertificationRegistery";
	
	// 外匯匯款編號是否顯示標誌
	public static final String NULFLAG_FX = "nulflag_fx";

	
	// N911回傳手機號碼
	public static final String MOBTEL = "mobtel";
	
	//全國性繳費平台申請
	public static final String EBILLAPPLY_DATA = "ebillapply_data";
	public static final String CUSIDN_EBILLAPPLY = "cusidn_ebillapply";
	
	// 使用者帳號清單--為了避免被竄改先存Session，交易前驗證
	public static final String ACN_LIST = "acn_list";
	// 使用者帳號清單--為了避免被竄改先存Session，交易前驗證
	public static final String FEE_ACN_LIST = "fee_acn_list";
	// 對帳單退件類別
	public static final String REMAILID = "remailid";
	
	public static final String IDGATE_TRANSDATA = "idagate_transdata";
	
	public static final String UPDATE_IMAGE_DATA = "update_image_data";

	public static final String OTP_SECCURE_TOKEN = "otp_seccure_token";
	public static final String OUTSIDE_SOURCE = "outside_source";
	
	public static final String SAL01 = "SAL01";
	
	public static final String SAL03 = "SAL03";
	public static void test() {
		addAttribute(null, null, null);
	}
	
//	未登入之線上申請功能--
	public static final String CUSIDN_ONLINE_APPLY = "cusidn_online_apply";
//	未登入之線上申請功能--信用卡補上傳資料
	public static final String UPLOAD_CREDITCARD_IDENTITY_P2_DATA = "upload_creditcard_identity_p2_data";
//	未登入之線上申請功能--信用卡補上傳資料-YNFLG
	public static final String YNFLG = "ynflg";
//	未登入之線上申請功能--信用卡補上傳資料-RCVNO-案件編號
	public static final String RCVNO = "rcvno";
//	未登入之線上申請功能--信用卡補上傳資料-PFLG-補件類型
	public static final String PFLG = "pflg";
//	未登入之線上申請功能--信用卡補上傳資料-MOBILE-行動電話
	public static final String MOBILE = "mobile";	
	//未登入之線上申請功能--信用卡補上傳資料-DCCSTA-補件類型
	public static final String DCCSTA = "dccsta";
	//線上申請信用卡，取得為舊戶的註記
	public static final String OLDCARDOWNERCHK = "oldcardownerchk";	
	//線上申請信用卡，是否通過身分驗證註記
	public static final String PASSAUTH = "PASSAUTH";
	
	//EMAIL驗證信URL
	public static final String VERIFYURL = "verifyurl";
	//EMAIL是否重覆
	public static final String DOUBLEMAIL = "doublemail";
	//EMAIL ALERT SHOW
	public static final String EMAILALERT = "emailalert";
	
	public static final String KYC_INSERT_HIST_PASS = "kyc_insert_hist_pass";
	public static final String KYC_INSERT_HIST_PASS_FINSH = "kyc_insert_hist_pass_finsh";
	
	public static void addAttribute(@NonNull Model model ,@NonNull String attributeName, @Nullable Object attributeValue) {
		model.addAttribute(attributeName,attributeValue);
//		TODO 日後要跟REDIS 或 infinispan  整合
	}
	
	
	public static void removeAttribute(@NonNull Model model ,@NonNull String attributeName, @Nullable Object attributeValue) {
		Map<String, Object> sessionMap = null;
		sessionMap = model.asMap();
		sessionMap.remove(attributeName);
//		TODO 日後要跟REDIS 或 infinispan  整合
	}
	
	// TODO hugo要做清除session的API，目前暫時在Controller用SpringMVC的status.setComplete
	public static void removeAll(@NonNull Model model) {
		Map<String, Object> sessionMap = null;
		sessionMap = model.asMap();
		sessionMap.clear();
		model.addAllAttributes(sessionMap);
//		TODO 日後要跟REDIS 或 infinispan  整合
	}
	
	public static Object getAttribute(@NonNull Model model ,@NonNull String attributeName, @Nullable Object attributeValue) {
		Map<String, Object> sessionMap = null;
		Object obj = null;
		sessionMap = model.asMap();
//		System.out.println("sessionMap>>"+sessionMap);
		obj = sessionMap.get(attributeName);
//		TODO 日後要跟REDIS 或 infinispan  整合
		return obj;
	}
	
	
	
	
	public static void main(String [] args) {
		test();
	}
}
