package tw.com.fstop.common;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import esccm.escslf;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.I18nUtil;
import tw.com.fstop.util.StrUtil;

/**
 * 提供共用的處理結果封裝
 * 
 *
 */
public class BaseResult implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -769063866588206848L;
	
	Logger log = LoggerFactory.getLogger(this.getClass());
	public static  String ERROR = "error"; 
	private String msgCode = ResultCode.SYS_ERROR;
	private String message = ResultCode.SYS_ERROR_MESSAGE;
	private String next = null;
	private String previous = null;
	private Boolean result = Boolean.FALSE;
	private Object data = null;
	
	public BaseResult() {
		this.msgCode = msgCode;
		setSYSMessage(msgCode);
	}
	public String getMsgCode() {
		return msgCode;
	}
	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
	
	
	public String getNext() {
		return next;
	}
	public void setNext(String next) {
		this.next = next;
	}
	
	
	public String getPrevious() {
		return previous;
	}
	public void setPrevious(String previous) {
		this.previous = previous;
	}
	public Boolean getResult() {
		return result;
	}
	public void setResult(Boolean result) {
		this.result = result;
	}
	public void setErrorMessage(String msgCode, String message) {
		this.msgCode = msgCode;
		this.message = message;
	}
	
	public Object getData() {
		return data;
	}
	
	public void setData(Object data) {
		this.data = data;
	}
	
	@SuppressWarnings("unchecked")
	public void addData(String key, Object value) {
		if(this.data == null){
			this.data = new HashMap<String, Object>();
		}
		
		((Map<String, Object>) this.data).put(key, value);
	}
	@SuppressWarnings("unchecked")
	public void addAllData(Map value) {
		if(this.data == null){
			this.data = new HashMap<String, Object>();
		}
		
		((Map<String, Object>) this.data).putAll(value);
	}
	
	public void setMessage(String msgCode ,String message ) {
		this.message = message;
		this.msgCode = msgCode;
	}
	
	
	
	public void reset() {
		this.msgCode = ResultCode.SYS_ERROR;
		this.message = ResultCode.SYS_ERROR_MESSAGE;
		this.next = null;
		this.previous = null;
		this.result = Boolean.FALSE;
		this.data = null;
	}
	
	public void setSYSMessage(String msgCode)  {
		
//		setMessage(msgCode,I18nUtil.getMsg(msgCode));
		try {
//			setMessage(msgCode,new String(I18nUtil.getMsg(msgCode).getBytes("UTF-8") ,"UTF-8") );
			setMessage(msgCode,I18nUtil.getMsg(msgCode ,null,null) );
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			//Avoid Information Exposure Through an Error Message
			log.error("setSYSMessage Error {}",e);
		}
	}
	
	
	
	public void setTBB_WS_Result(Object obj) {
		Map<String,Object> map = null; 
		String  __message ,__msgCode ;
		Boolean __result = Boolean.FALSE;
		try {
			if(obj!=null) {
				map = (Map<String, Object>) obj;
				
				__msgCode = map.containsKey("_MsgCode")  ? map.get("_MsgCode").toString():msgCode;
				__message = map.containsKey("_MsgName")  ? map.get("_MsgName").toString():message;
				
				__msgCode = map.containsKey("msgCode")  ? map.get("msgCode").toString():__msgCode;
				__message = map.containsKey("msgName")  ? map.get("msgName").toString():__message;
				
				__msgCode = map.containsKey("MsgCode")  ? map.get("MsgCode").toString():__msgCode;
				__message = map.containsKey("MsgName")  ? map.get("MsgName").toString():__message;
//				針對ws的rest 的客製化
				__message = map.containsKey("message")  ? map.get("message").toString():__message;
				__result = StrUtil.isNotEmpty(__msgCode) && __msgCode.equals("0") ? Boolean.TRUE : Boolean.FALSE ;
				
				log.info(ESAPIUtil.vaildLog("__msgCode>>" + __msgCode + ", __message>>{}"));
				setMsgCode(__msgCode);
				
//				20190501 edit by hugo 因為ms_tw等開始會回傳 FE00XX的msgCode 
//				20190828 edit by hugo 因為FEXXXX也放資料庫不抓設定檔了故都走setMessage
//				if( __msgCode.length()>=6  && __msgCode.startsWith("FE")) {
//					setSYSMessage(__msgCode);
//				}else {
					setMessage(__message);
//				}
					
				setData(map);
				setResult(__result);
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			// Avoid Information Exposure Through an Error Message
			//String eMsg = ESAPIUtil.vaildLog("setTBB_WS_Result Error"+e.getMessage());
			String eMsg1 = "setTBB_WS_Result Error";
			String eMsg2 = e.getMessage();
			String eMsg3 = eMsg1 + eMsg2;
			
			eMsg3 = ESAPIUtil.vaildLog(eMsg3);
			escslf.escslf1(this.getClass(), eMsg3);
			//log.error(eMsg3);
		}
	}
}
