package tw.com.fstop.util;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * 下載檔案的共用程式
 */
public class DownloadUtil{
	private static Logger log = LoggerFactory.getLogger(DownloadUtil.class);
	
	//取出這一列的所有$字號，封裝進Variable物件
	public static List<Variable> getVariables(String inputLine,String languageTag){
		//zh_TW是繁中；en是英文；zh_CN是簡中
		log.debug("languageTag={}",languageTag);
		
		List<Variable> variableList = new ArrayList<Variable>();
        
		List<String> variableKeysList = new ArrayList<String>();
		String leftSignal = "${";
		String rightSignal = "}";
		
		getVariableKeys(variableKeysList,leftSignal,rightSignal,inputLine);
		log.debug("variableKeysList={}",variableKeysList);
		
		for(String key : variableKeysList){
			log.debug(ESAPIUtil.vaildLog("key >> " + key));
			Variable variable = new Variable();
			
			String[] attributes = key.split(",");
			variable.setVariableName(attributes[0].trim());
			
			if("zh-TW".equals(languageTag)){
				variable.setVariableLength(Integer.valueOf(attributes[1].trim().replace("[","").replace("]","").split("/")[0]));
			}
			else if("en".equals(languageTag)){
				variable.setVariableLength(Integer.valueOf(attributes[1].trim().replace("[","").replace("]","").split("/")[1]));
			}
			else{
				variable.setVariableLength(Integer.valueOf(attributes[1].trim().replace("[","").replace("]","").split("/")[2]));
			}
			
			String variableType = attributes[2].trim();
			variable.setVariableType(variableType);
			
			//金額的特殊補位
			if("C".equals(variableType)){
				variable.setRightZeroLength(Integer.valueOf(attributes[3].trim()));
			}
			//先把,和.拿掉，再做金額的特殊補位
			else if("D".equals(variableType)){
				variable.setRightZeroLength(Integer.valueOf(attributes[3].trim()));
			}
			variableList.add(variable);
		}
		log.debug("variableList={}",variableList);
		
		return variableList;
	}
	//封裝$字號變數的物件
	public static class Variable{
        private String variableName;
        private int variableLength;
        private String variableType;
        private int rightZeroLength;
        
		public String getVariableName(){
			return variableName;
		}
		public void setVariableName(String variableName){
			this.variableName = variableName;
		}
		public int getVariableLength(){
			return variableLength;
		}
		public void setVariableLength(int variableLength){
			this.variableLength = variableLength;
		}
		public String getVariableType(){
			return variableType;
		}
		public void setVariableType(String variableType){
			this.variableType = variableType;
		}
		public int getRightZeroLength(){
			return rightZeroLength;
		}
		public void setRightZeroLength(int rightZeroLength){
			this.rightZeroLength = rightZeroLength;
		}
	}
	//使用Variable物件及ROWDATA來組成這一列的資料
	public static String getResultLine(List<Variable> variableList,Map<String,?> dataMap){
		String resultLine = "";
		DecimalFormat decimalFormat = new DecimalFormat();
		
		for(int x=0;x<variableList.size();x++){
			String variableName = variableList.get(x).getVariableName();
			log.debug("variableName={}",variableName);
			int variableLength = variableList.get(x).getVariableLength();
			log.debug("variableLength={}",variableLength);
			String variableType = variableList.get(x).getVariableType();
			log.debug("variableType={}",variableType);
			int rightZeroLength = variableList.get(x).getRightZeroLength();
			log.debug("rightZeroLength={}",rightZeroLength);
			
			String dataMapValue = (String)dataMap.get(variableName);
			log.debug("dataMapValue={}",dataMapValue);
			
			//有對應的值
			if(dataMapValue != null){
				//取得中文字的個數
				int chineseNum = getChineseNum(dataMapValue);
				log.debug("chineseNum={}",chineseNum);
				
				int minusValue = variableLength - chineseNum;
				log.debug("minusValue={}",minusValue);
				
				//如果是0的話，%0s或%-0s會錯誤
				if(minusValue > 0){
					String formatValue;
					
					//左靠右補空白
					if("A".equals(variableType)){
						resultLine += String.format("%-" + minusValue + "s",dataMapValue);
					}
					//右靠左補空白
					else if("B".equals(variableType)){
						resultLine += String.format("%" + minusValue + "s",dataMapValue);
					}
					//金額的特殊補位
					else if("C".equals(variableType)){
						resultLine += oldStyleFormatAmount(dataMapValue,variableLength,rightZeroLength);
					}
					//先把,和.拿掉，再做金額的特殊補位
					else if("D".equals(variableType)){
						formatValue = dataMapValue.replaceAll(",","").replaceAll("\\.","");
						log.debug("formatValue={}",formatValue);
						
						resultLine += oldStyleFormatAmount(formatValue,variableLength,rightZeroLength);
					}
					//金額千分位
					else{
						formatValue = decimalFormat.format(Double.valueOf(dataMapValue));
						log.debug("formatValue={}",formatValue);
						
						resultLine += String.format("%" + variableLength + "s",formatValue);
					}
				}
				else{
					resultLine += dataMapValue;
				}
			}
			//沒有對應的值
			else{
				//補滿空白
				for(int y=0;y<variableLength;y++){
					resultLine += " ";
				}
			}
		}
		log.debug("resultLine={}",resultLine);
		
		return resultLine;
	}
	//計算中文字的個數
	public static int getChineseNum(String name){
		int chineseNum = 0;
		
		try{
			chineseNum = name.getBytes("BIG5").length - name.length();
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			log.error("getChineseNum error >> {}",e);
			log.error(String.valueOf(e));
		}
		return chineseNum;
	}
	//金額的特殊補位
	public static String oldStyleFormatAmount(String amount,int totalLength,int rightZeroLength){
		log.debug("amount={}",amount);
		log.debug("totalLength={}",totalLength);
		log.debug("rightZeroLength={}",rightZeroLength);
		
		String result = amount;
		
		int amountLength = amount.length();
		log.debug("amountLength={}",amountLength);
		
		if(amountLength + rightZeroLength <= totalLength){
			String rightZero = "";
			for(int x=0;x<rightZeroLength;x++){
				rightZero += "0";
			}
			log.debug("rightZero={}",rightZero);
			
			result += rightZero;
			log.debug("result={}",result);
			
			for(int x=result.length();x<totalLength;x++){
				result = "0" + result;
			}
			log.debug("result={}",result);
		}
		return result;
	}
	//取得多個變數的KEY
	public static void getVariableKeys(List<String> variableKeysList,String leftSignal,String rightSignal,String text){
		log.debug(ESAPIUtil.vaildLog("text >> " + text)); 
		
		if(text.length() > leftSignal.length() + rightSignal.length()){
			if(text.indexOf(leftSignal) > -1){
				int leftPosition = text.indexOf(leftSignal);
				log.debug("leftPosition={}",leftPosition);
				
				int rightPosition = text.indexOf(rightSignal);
				log.debug("rightPosition={}",rightPosition);
				
				while(leftPosition > rightPosition){
					text = text.substring(rightPosition + 1,text.length());
					log.debug(ESAPIUtil.vaildLog("text >> " + text)); 
					
					leftPosition = text.indexOf(leftSignal);
					log.debug("leftPosition={}",leftPosition);
					
					rightPosition = text.indexOf(rightSignal);
					log.debug("rightPosition={}",rightPosition);
				}
				
				String key = text.substring(leftPosition + leftSignal.length(),rightPosition);
				log.debug(ESAPIUtil.vaildLog("key >> " + key)); 
				variableKeysList.add(key);
				
				String restText = text.substring(rightPosition + rightSignal.length(),text.length());
				log.debug(ESAPIUtil.vaildLog("restText >> " + restText)); 
				
				getVariableKeys(variableKeysList,leftSignal,rightSignal,restText);
			}
		}
	}
	//將頁面上JQUERY序列化的表單內容字串放進MAP
	public static Map<String,String> serializeStringToMap(String serializeString){
		Map<String,String> resultMap = new HashMap<String,String>();
		
		String[] pairs = serializeString.split("&");
		
		String[] keyValue;
		String key;
		String value;
		for(String string : pairs){
			keyValue = string.split("=");
			log.debug("keyValue.length={}",keyValue.length);
			
			key = keyValue[0];
			log.debug(ESAPIUtil.vaildLog("key >> " + key));
			if(keyValue.length > 1){
				value = keyValue[1];
			}
			else{
				value = "";
			}
			log.debug(ESAPIUtil.vaildLog("value >>> " + value));
			
			resultMap.put(key,value);
		}
		log.debug(ESAPIUtil.vaildLog("resultMap >> " + CodeUtil.toJson(resultMap)));
		return resultMap;
	}
}