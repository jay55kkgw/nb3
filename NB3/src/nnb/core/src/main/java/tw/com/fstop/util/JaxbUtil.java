//因原碼問題暫時註解
///*
// * Copyright (c) 2017, FSTOP, Inc. All Rights Reserved.
// *
// * You may not use this file except in compliance with the License.
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
// */
//
//package tw.com.fstop.util;
//
//import java.io.ByteArrayInputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.StringReader;
//import java.io.StringWriter;
//
//import javax.xml.bind.JAXBContext;
//import javax.xml.bind.Marshaller;
//import javax.xml.bind.Unmarshaller;
//import javax.xml.parsers.DocumentBuilder;
//import javax.xml.parsers.DocumentBuilderFactory;
//import javax.xml.parsers.ParserConfigurationException;
//import javax.xml.transform.Transformer;
//import javax.xml.transform.TransformerConfigurationException;
//import javax.xml.transform.TransformerException;
//import javax.xml.transform.TransformerFactory;
//import javax.xml.transform.dom.DOMSource;
//import javax.xml.transform.stream.StreamResult;
//
//import org.w3c.dom.Document;
//import org.xml.sax.InputSource;
//import org.xml.sax.SAXException;
//
///**
// * JAXB utility class.
// * Converts xml to object and vice versa.
// * 
// *
// * @since 1.0.1
// */
//public class JaxbUtil
//{
//    static boolean escCdataChart = false;
//    static String xmlEncoding = "UTF-8";
//    
//    /**
//     * Convert object to xml.
//     * Supports polymorphism.
//     * Note that implement classes must be submitted to JAXBContext.newInstance one way or the other.
//     * 
//     * @param obj object to convert to xml
//     * @param isFragment if convert result contain xml header
//     * @param clazz implement classes
//     * @return xml
//     */
//    public static String ObjToXml(Object obj, boolean isFragment, Class<?> ... clazz)
//    {
//        try
//        {
//            JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
//            Marshaller marshaller = jaxbContext.createMarshaller();
//            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
//
//            // 指定輸出 XML 編碼
//            if (xmlEncoding != null && !xmlEncoding.isEmpty())
//            {
//                marshaller.setProperty(Marshaller.JAXB_ENCODING, xmlEncoding);
//            }
//
//            // 不產生 <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
//            if (isFragment == true)
//            {
//                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
//            }
//
//            /*
//             * Unicode code points in the following ranges are valid in XML 1.0
//             * documents:[1]
//             * U+0009, U+000A, U+000D: these are the only C0 controls accepted
//             * in XML 1.0;
//             * U+0020–U+D7FF, U+E000–U+FFFD: this excludes some (not all)
//             * non-characters in the BMP (all surrogates, U+FFFE and U+FFFF are
//             * forbidden);
//             * U+10000–U+10FFFF: this includes all code points in supplementary
//             * planes, including non-characters.
//             */
//            // escape(char[] ch, int start, int length, boolean isAttVal, Writer
//            // out)
//            // 將 <![CDATA[]]> 的 "<" 及 ">" 保留，不轉換成 &lt; 及 &gt;
//
//            // import com.sun.xml.bind.marshaller.CharacterEscapeHandler; ->  CharacterEscapeHandler
//            
//            // 轉換 CDATA
//            // if (escCdataChart == true) {
//            // marshaller.setProperty(CharacterEscapeHandler.class.getName(),
//            // new CharacterEscapeHandler() {
//            // @Override
//            // public void escape(char[] ac, int i, int j, boolean flag, Writer
//            // writer) throws IOException {
//            // //System.out.println(new String(ac));
//            // //System.out.println("&#" + Integer.toString(ac[i]) + ";");
//            // writer.write(ac, i, j);
//            // }
//            // });
//            // }
//
//            StringWriter stringWriter = new StringWriter();
//            marshaller.marshal(obj, stringWriter);
//            return stringWriter.toString();
//
//        }
//        catch (Exception e)
//        {
//            System.out.println(e.toString());
//        }
//        return null;
//    }
//    
//    
//    public static String ObjToXml(Class<?> clazz, Object obj, boolean isFragment)
//    {
//        try
//        {
//            JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
//            Marshaller marshaller = jaxbContext.createMarshaller();
//            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
//
//            // 指定輸出 XML 編碼
//            if (xmlEncoding != null && !xmlEncoding.isEmpty())
//            {
//                marshaller.setProperty(Marshaller.JAXB_ENCODING, xmlEncoding);
//            }
//
//            // 不產生 <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
//            if (isFragment == true)
//            {
//                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
//            }
//
//            /*
//             * Unicode code points in the following ranges are valid in XML 1.0
//             * documents:[1]
//             * U+0009, U+000A, U+000D: these are the only C0 controls accepted
//             * in XML 1.0;
//             * U+0020–U+D7FF, U+E000–U+FFFD: this excludes some (not all)
//             * non-characters in the BMP (all surrogates, U+FFFE and U+FFFF are
//             * forbidden);
//             * U+10000–U+10FFFF: this includes all code points in supplementary
//             * planes, including non-characters.
//             */
//            // escape(char[] ch, int start, int length, boolean isAttVal, Writer
//            // out)
//            // 將 <![CDATA[]]> 的 "<" 及 ">" 保留，不轉換成 &lt; 及 &gt;
//
//            // import com.sun.xml.bind.marshaller.CharacterEscapeHandler; ->  CharacterEscapeHandler
//            
//            // 轉換 CDATA
//            // if (escCdataChart == true) {
//            // marshaller.setProperty(CharacterEscapeHandler.class.getName(),
//            // new CharacterEscapeHandler() {
//            // @Override
//            // public void escape(char[] ac, int i, int j, boolean flag, Writer
//            // writer) throws IOException {
//            // //System.out.println(new String(ac));
//            // //System.out.println("&#" + Integer.toString(ac[i]) + ";");
//            // writer.write(ac, i, j);
//            // }
//            // });
//            // }
//
//            StringWriter stringWriter = new StringWriter();
//            marshaller.marshal(obj, stringWriter);
//            return stringWriter.toString();
//
//        }
//        catch (Exception e)
//        {
//            System.out.println(e.toString());
//        }
//        return null;
//    }
//
//    public static <T> T XmlToObj(Class<T> clazz, String xml)
//    {
//        try
//        {
//            JAXBContext context = JAXBContext.newInstance(clazz);
//            Unmarshaller um = context.createUnmarshaller();
//            //return clazz.cast(um.unmarshal(new StringReader(xml)));            
//            //return clazz.cast(um.unmarshal(new StringReader(parseDocument(xml))));
//            return clazz.cast(um.unmarshal(getDocument(xml)));
//
//        }
//        catch (Exception e)
//        {
//            System.out.println(e.toString());
//        }
//        return null;
//    }
//    
//    /**
//     * Convert xml to object.
//     * Supports polymorphism.
//     * Note that implement classes must be submitted to JAXBContext.newInstance one way or the other.
//     * 
//     * @param clazz primary class
//     * @param xml xml string
//     * @param clz implement classes
//     * @return object of primary class
//     */
//    public static <T> T XmlToObj(Class<T> clazz, String xml, Class<?> ... clz)
//    {
//        try
//        {
//            JAXBContext context = JAXBContext.newInstance(clz);
//            Unmarshaller um = context.createUnmarshaller();
//            //return clazz.cast(um.unmarshal(new StringReader(xml)));
//            //return clazz.cast(um.unmarshal(new StringReader(parseDocument(xml))));
//            return clazz.cast(um.unmarshal(getDocument(xml)));
//
//        }
//        catch (Exception e)
//        {
//            System.out.println(e.toString());
//        }
//        return null;
//    }
//
//    static String parseDocument(String xml)
//    {
//        String ret = "";
//        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
//        DocumentBuilder db;
//        Document document = null;
//        try
//        {
//            dbf.setExpandEntityReferences(false);
//            //NamespaceAware 預設為 false，會造成 javax.xml.bind.UnmarshalException: unexpected element URI:"" 錯誤
//            dbf.setNamespaceAware(true);
//            db = dbf.newDocumentBuilder();
//            
//            //fail
//            //document = db.parse(xml); 
//
//            //OK
//            //InputStream stream = new ByteArrayInputStream(xml.getBytes(xmlEncoding));
//            //document = db.parse(stream);
//            
//            //OK
//            document = db.parse(new InputSource(new StringReader(xml)));
//            document.setXmlStandalone(true);
//            
//            DOMSource domSource = new DOMSource(document);
//            StringWriter writer = new StringWriter();
//            StreamResult result = new StreamResult(writer);
//            TransformerFactory tf = TransformerFactory.newInstance();
//            Transformer transformer = tf.newTransformer();
//            transformer.transform(domSource, result);
//            //System.out.println(writer.toString());
//            ret = writer.toString();
//            
//        }
//        catch (ParserConfigurationException e1)
//        {
//            System.out.println(e1.toString());
//        }
//        catch (SAXException e1)
//        {
//            System.out.println(e1.toString());
//        }
//        catch (IOException e1)
//        {
//            System.out.println(e1.toString());
//        }
//        catch (TransformerConfigurationException e)
//        {
//            System.out.println(e.toString());
//        }
//        catch (TransformerException e)
//        {
//            System.out.println(e.toString());
//        }
//        
//        return ret;
//    }
//    
//    static Document getDocument(String xml)
//    {
//        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
//        DocumentBuilder db;
//        Document document = null;
//        try
//        {
//            dbf.setExpandEntityReferences(false);
//            //NamespaceAware 預設為 false，會造成 javax.xml.bind.UnmarshalException: unexpected element URI:"" 錯誤
//            dbf.setNamespaceAware(true);
//            db = dbf.newDocumentBuilder();
//            
//            //fail
//            //document = db.parse(xml); 
//
//            //OK
//            //InputStream stream = new ByteArrayInputStream(xml.getBytes(xmlEncoding));
//            //document = db.parse(stream);
//            
//            //OK
//            document = db.parse(new InputSource(new StringReader(xml)));
//
////驗證用            
////            document.setXmlStandalone(true);            
////            DOMSource domSource = new DOMSource(document);
////            StringWriter writer = new StringWriter();
////            StreamResult result = new StreamResult(writer);
////            TransformerFactory tf = TransformerFactory.newInstance();
////            Transformer transformer = tf.newTransformer();                       
////            transformer.transform(domSource, result);
////            System.out.println(writer.toString());
//            
//        }
//        catch (ParserConfigurationException e1)
//        {
//            System.out.println(e1.toString());
//        }
//        catch (SAXException e1)
//        {
//            System.out.println(e1.toString());
//        }
//        catch (IOException e1)
//        {
//            System.out.println(e1.toString());
//        }
////        catch (TransformerConfigurationException e)
////        {
////            System.out.println(e.toString());
////        }
////        catch (TransformerException e)
////        {
////            System.out.println(e.toString());
////        }
//        
//        return document;
//    }
//
//    
//}
//
///*
// * Polymorphism Memo 
//https://javaee.github.io/jaxb-v2/doc/user-guide/ch03.html
//
//@XmlJavaTypeAdapter(AbstractFooImpl.Adapter.class)
//interface IFoo {
//  ...
//}
//abstract class AbstractFooImpl implements IFoo {
//  ...
//
//  static class Adapter extends XmlAdapter<AbstractFooImpl,IFoo> {
//    IFoo unmarshal(AbstractFooImpl v) { return v; }
//    AbstractFooImpl marshal(IFoo v) { return (AbstractFooImpl)v; }
//  }
//}
//
//class SomeFooImpl extends AbstractFooImpl {
//  @XmlAttribute String name;
//  ...
//}
//
//class AnotherFooImpl extends AbstractFooImpl {
//  @XmlAttribute int id;
//  ...
//}
//
//class Somewhere {
//  public IFoo lhs;
//  public IFoo rhs;
//} 
// 
//<somewhere>
//  <lhs xsi:type="someFooImpl" name="...">
//  </lhs>
//  <rhs xsi:type="anotherFooImpl" id="3" />
//</somewhere> 
// 
// */
//
///* 
//  Generate xsi:type by JAXBElement & QName
//        JAXBElement<ServiceBody> element = new JAXBElement<ServiceBody>(
//                new QName("http://www.w3.org/2001/XMLSchema-instance", "FXNACQ02SvcRqType"), ServiceBody.class, body);
//        xml = JaxbUtil.ObjToXml(GenericEnvlope.class, element, true);
// 
// */
//
