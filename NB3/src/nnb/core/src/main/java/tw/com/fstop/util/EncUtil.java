package tw.com.fstop.util;

import java.security.MessageDigest;
import java.security.Security;
import java.util.Arrays;
import java.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class EncUtil {
	private static Logger logger = Logger.getLogger(EncUtil.class);
	private static final boolean hexcase = true;
	public static String SHA1_TYPE_EBCDIC = "IBM937";
	
	/**
	 * 
	 * @param str
	 * @param type  default ASCII
	 * @return String Hex
	 */
	//修改 Reversible One Way Hash
//	public static String toSHA1(String str ,String type) {
//		byte[] data = null;
//		try {
//			if(StrUtil.isEmpty(str)) {
//				return null;
//			}
//			
//			if(StrUtil.isEmpty(type)) {
//				data = str.getBytes();
//			}else{
//				data = str.getBytes(type);
//			}
//			
////			switch (type) {
////			case "IBM937" :
////				data = str.getBytes(SHA1_TYPE_EBCDIC);
////				break;
////			default:
////				data = str.getBytes();
////				break;
////			}
//			str = DigestUtils.shaHex(data);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			str = null;
//		}
//		
//		return str;
//		
//	}
	/**
	 * @param source
	 * @param key
	 * @return EBCString
	 */
    public static String encryptEBC(String source,String key){
    	String encryptString;
    	
    	try{
	    	Security.addProvider(new BouncyCastleProvider());
	    	
	    	if(StrUtil.isEmpty(key)){
	    		logger.error("key為空值");
	    		return null;
	    	}
	    	if(key.length() != 16){
	    		logger.error("key長度錯誤");
	            return null;
	        }
	    	
	    	byte[] compBefore = key.getBytes();
	        byte[] compAfter = Arrays.copyOf(compBefore,16);
	        
	        SKSpecMethod skspecmethod = new SKSpecMethod(compAfter,"AES");
	        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS7Padding");
	        
	        byte[] bytesSource = source.getBytes("UTF-8");
	        cipher.init(Cipher.ENCRYPT_MODE,skspecmethod);
	        byte[] encryptByte = cipher.doFinal(bytesSource);
	        
	        encryptString = Base64.getEncoder().encodeToString(encryptByte);
	        logger.debug("encryptString=" + encryptString);
    	}
    	catch(Exception e){
    		logger.error(e);
			//e.printStackTrace();
			return null;
    	}
    	
        return encryptString;
    }
    // Use of Hard coded Cryptographic Key
	public static class SKSpecMethod extends SecretKeySpec{
		private static final long serialVersionUID = -2875828002477265500L;
		
		public SKSpecMethod(byte[] key,String algorithm){
			super(key,algorithm);
		}
	}
    
    /**
	 * @param source
	 * @param encoding
	 * @return MD5String
	 */
	//修改 Use of Broken or Risky Cryptographic Algorithm
//	public static String MD5(String source,String encoding){
//		String MD5String = null;
//		
//		try{
//			MessageDigest messageDigest = MessageDigest.getInstance("MD5");
//
//			byte[] sourceByte = source.getBytes(encoding);
//			
//			messageDigest.update(sourceByte);
//			
//			byte[] byteArray = messageDigest.digest();
//			
//			MD5String = byteToHexString(byteArray);
//		}
//		catch(Exception e){
//			logger.error(e);
//			e.printStackTrace();
//		}
//		return MD5String;
//	}
	public static String byteToHexString(byte[] byteArray){
		char[] hexChar = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};

		StringBuffer stringBuffer = new StringBuffer(byteArray.length * 2);
		for(int i=0;i<byteArray.length;i++){
			stringBuffer.append(hexChar[(byteArray[i] & 0xf0) >>> 4]);
			stringBuffer.append(hexChar[byteArray[i] & 0x0f]);
		}
		return stringBuffer.toString();
	}
}