
/*
 * Copyright (c) 2017, FSTOP, Inc. All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tw.com.fstop.util;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.lang.Nullable;

/**
 * I18n Utility functions.
 * Default resource bundle name define is BUNDLE_NAME.
 * 
 * <pre>
 * 地區資訊 = 語言編碼（Language code）+ 地區編碼（Country code） 
 * 語言編碼 例如 zh
 * 地區編碼 例如 TW
 * </pre>
 *
 * @since 1.0.1
 */
public class I18nUtil
{
	static Logger log = LoggerFactory.getLogger(I18nUtil.class);
    //default resource bundle name
    public static final String BUNDLE_NAME = "messages";

    /**
     * Get resource bundle with specified locale.
     * 
     * @param baseName - resource bundle name
     * @param locale - specified locale
     * @return resource bundle
     */
    public static ResourceBundle getResource(String baseName, Locale locale)
    {
        return ResourceBundle.getBundle(baseName, locale);
    }
    
    /**
     * Get resoruce bundle with default locale.
     * 
     * @param baseName - resource bundle name
     * @return resource bundle
     */
    public static ResourceBundle getResource(String baseName)
    {
        return ResourceBundle.getBundle(baseName);
    }
    
    /**
     * Get resource bundle message with parameters.
     * 
     * @param res - resource bundle
     * @param key - resource bundle key
     * @param params - resource bundle parameters
     * @return message string
     */
    public static String getString(ResourceBundle res, String key, Object ...params)
    {
        return MessageFormat.format(res.getString(key), params);
    }
    
    /**
     * Get resource bundle message (string).
     * 
     * @param key - resource bundle key
     * @return resource string
     */
    public static String getMsg(String key)
    {
        String ret = "";
        try
        {
            ret = getResource(BUNDLE_NAME).getString(key);
        }
        catch(MissingResourceException e)
        {
            
        }        
        return ret;
    }

    public static String getMsg(String key, String def)
    {
        String ret = "";
        try
        {
            ret = getResource(BUNDLE_NAME).getString(key);
        }
        catch(MissingResourceException e)
        {
            ret = def;
        }        
        return ret;
    }
    
    public static String getMsg(Integer key)
    {
    	return getMsg(Integer.toString(key));
    }
    
    /**
     * 解決i18n設定檔 messages_*.properties 採用 utf-8編碼後，中文亂碼問題
     * @param key
     * @param def
     * @param args ex: messages_*.properties 設定   key = test {0},{1}
     * Object[] 如果是{"a" ,"b"} 其輸出結果就是會  test a,b
     * @return
     */
    public static String getMsg(String key, @Nullable String def ,@Nullable Object[] args)
    {
    	String ret = "";
    	try
    	{
    		def = StrUtil.isEmpty(def)?"":def;
    		ResourceBundleMessageSource rbm = SpringBeanFactory.getBean("messageSource");
            Locale currentLocale = LocaleContextHolder.getLocale();
        	ret = rbm.getMessage(key, args, currentLocale);
    	}
    	catch(Exception e)
    	{
    		log.error("",e);
    	}finally {
    		ret = StrUtil.isEmpty(ret)? def:ret;
    	}        
    	return ret;
    }
    //----
    
    String baseName;
    ResourceBundle res;
    Locale locale;
    
    public I18nUtil() 
    {
        baseName = BUNDLE_NAME;
    }
    
    /**
     * New instance with resource bundle name.
     * 
     * @param baseName - resource bundle name
     */
    public I18nUtil(String baseName)
    {
        this.baseName = baseName;
        res = ResourceBundle.getBundle(baseName);
    }
    
    /**
     * New instance with resource bundle name, language and country.
     * 
     * @param baseName - resource bundle name
     * @param language - language code
     * @param country - country code
     */
    public I18nUtil(String baseName, String language, String country)
    {
        this.baseName = baseName;
        this.locale = new Locale(language, country);
        res = ResourceBundle.getBundle(baseName, locale);        
    }
    
    /**
     * New instance with resource bundle name and locale.
     * 
     * @param baseName - resource bundle name
     * @param locale - locale
     */
    public I18nUtil(String baseName, Locale locale)
    {
        this.baseName = baseName;
        this.locale = locale;
        res = ResourceBundle.getBundle(baseName, locale);
    }
    
    /**
     * Get resource string.
     * 
     * @param key - resource key
     * @return resource string
     */
    public String getString(String key)
    {
        return res.getString(key);
    }
    
    /**
     * Get resource string with default value.
     * 
     * @param key - resource key
     * @param def - default value
     * @return resource string
     */
    public String getString(String key, String def)
    {
        String ret = "";
        try
        {
            ret = res.getString(key);
        }
        catch(MissingResourceException e)
        {
            ret = def;
        }
        
        return ret;
    }
    
    /**
     * Get resource string with parameters.
     * 
     * @param key - resource key
     * @param params - parameters
     * @return formated string
     */
    public String getString(String key, Object ... params)
    {
        return MessageFormat.format(res.getString(key), params);
    }
    
    /**
     * Get resource string with default value and parameters.
     * 
     * <pre>
     * Resource example:
     * item.added = User {0} added new item {1} to group {2}.
     * </pre>
     * 
     * @param key - resource key
     * @param def - default value
     * @param params - parameters
     * @return formated string
     */
    public String getString(String key, String def, Object ... params)
    {
        String ret = "";
        String str = "";
        try
        {
            str = res.getString(key);
            ret = MessageFormat.format(str, params);
        }
        catch(MissingResourceException e)
        {
            str = def;
            ret = MessageFormat.format(str, params);
        }
        
        return ret;
    }

    public String getBaseName()
    {
        return baseName;
    }

    public void setBaseName(String baseName)
    {
        this.baseName = baseName;
    }

    public ResourceBundle getRes()
    {
        return res;
    }

    public void setRes(ResourceBundle res)
    {
        this.res = res;
    }

    public Locale getLocale()
    {
        return locale;
    }

    public void setLocale(Locale locale)
    {
        this.locale = locale;
    }
    
    
}
