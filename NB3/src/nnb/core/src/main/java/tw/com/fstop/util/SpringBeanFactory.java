package tw.com.fstop.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class SpringBeanFactory implements  ApplicationContextAware   {
	static Logger log = LoggerFactory.getLogger(SpringBeanFactory.class);
	private static ApplicationContext ctx;
	public static String XML = "classpath*:/spring-root-context.xml";
	@Override
	public void setApplicationContext(ApplicationContext arg) throws BeansException {
		// TODO Auto-generated method stub
		this.ctx = arg;
	}

	public static ApplicationContext getApplicationContext() {  
        return ctx;  
    }  
	/**
	 * 
	 * @param name
	 * @return
	 * @throws BeansException
	 */
    public static <T> T getBean(String name) throws BeansException {  
    	
    	if(ctx == null) {
    		log.warn("警告 ctx is null  ");
    		getlocalCtx();
    	}
    	
        return (T) ctx.getBean(name);  
    }
	
    
    public static void getlocalCtx() {
    	ctx = new FileSystemXmlApplicationContext(XML);
    	log.warn("getlocalCtx.ctx is null...");
    }
    
    
    /**
     * 通過class取得bean
     * 
     * @param clazz
     * @return
     */
    public static <T> T getBean(Class<T> clazz) {
    	if(ctx == null) {
    		log.warn("警告 ctx is null  ");
    		getlocalCtx();
    	}
        return getApplicationContext().getBean(clazz);
    }
	
}
