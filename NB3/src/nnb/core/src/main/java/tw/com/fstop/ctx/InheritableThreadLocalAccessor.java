
/*
 * Copyright (c) 2017, FSTOP, Inc. All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tw.com.fstop.ctx;

/**
 * Context accessor by using thread local storage.
 * Default access to InheritableThreadLocal object. 
 * 
 * <pre>
 * Note:
 * ThreadLocal instances are typically private static fields in classes that wish to associate state with a thread.
 * Provide inheritance of values from parent thread to child thread: 
 * when a child thread is created, the child receives initial values for all inheritable thread-local 
 * variables for which the parent has values.
 * 
 * Inner classes have an implicit pointer to the surrounding class, so it will have a class loader leak anyway 
 * when the application is undeployed, since both the inner and the enclosing class will be in the application 
 * class loader (and still alive in the thread pool)
 * 
 * 操作方法雒然是物件層級的，但是實際上呼叫時會作用在 thread object.
 * 
 * 如果ThreadLocal存儲的是不變（Immutable）的物件，如String，對於主線程設置的值，子線程可以通過get函數獲取，
 * 但子線程調用set函數設置新值後，對主線程沒有影響，對其它子線程也沒有影響，只對自己可見；
 * 如果主線程還沒有獲取(get)或者設置(set)過ThreadLocal變量，而子線程先獲取(get)或者設置(set)了ThreadLocal變量，
 * 那麼這個份值只屬於那個子線程，對主線程和其它子線程都不可見.
 * 
 * 如果ThreadLocal存儲的是可變（Mutable）的物件，如StringBuffer，對於主線程設置的值，子線程可以通過get函數獲取，
 * 但子線程調用set函數設置新值後，對主線程沒有影響，對其它子線程也沒有影響，只對自己可見，但如果子線程先get獲取再修改物件的屬性，
 * 那麼這個修改對主線程和其它子線程是可見的，即他們還是共享一個引用；如果主線程還沒有獲取(get)或者設置(set)過ThreadLocal變量，
 * 而子線程先獲取(get)或者設置(set)了ThreadLocal變量，那麼這份值只屬於那個子線程，對主線程和其它子線程都不可見.
 * 
 * 所以子線程只能通過修改可變（Mutable）對像對主線程才是可見的，即才能將修改傳遞給主線程，但這不是一種好的實踐，不建議使用，
 * 為了保護線程的安全性，一般建議只傳遞不可變（Immutable）物件，即沒有狀態的物件。
 * 
 * 使用 ThreadLocal在 thread 之間共用物件容易讓物件使用後不被釋放，而造成 memory leak。
 * 
 * ThreadLocal 若使用於有 thread pool 機制的情形，必需要保證將 thread local 中的物件移除，否則會造成 memory leak 
 * (java.lang.OutOfMemoryError: PermGen space ).
 * 
 * </pre>
 * Thread 之間若要共用物件，請使用 java.util.concurrent.Exchanger
 *
 * @since 1.0.0
 */
public class InheritableThreadLocalAccessor
{

    private static final InheritableThreadLocal<Object> inheritableThreadLocal = new InheritableThreadLocal<Object>();
    
    /**
     * Get object from inheritable thread local.
     * 
     * @return  object in threadlocal
     */
    public static Object getInheritable()
    {
        return inheritableThreadLocal.get();
    }
    
    /**
     * Set object to inheritable thread local.
     * @param value object
     */
    public static void setInheritable(Object value)
    {
        inheritableThreadLocal.set(value);
    }
    
    /**
     * Remove object from inheritable thread local.
     * This function should be called when object is no longer be used to prevent memory leak.  
     */
    public static void removeInheritable()
    {
        inheritableThreadLocal.remove();
    }
    


}
