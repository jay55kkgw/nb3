package tw.com.fstop.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;

import org.aspectj.lang.reflect.CatchClauseSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NumericUtil {
	private static Logger log = LoggerFactory.getLogger(NumericUtil.class);

	/**
	 * 數字字串加上千分位及希望的小數位數
	 * 
	 * @param text
	 *            輸入字串
	 * @param digital
	 *            小數幾位 若電文回傳為三位數,請至少輸入3以上以防誤差
	 * @return
	 */
	public static String fmtAmount(String text, int digits) {
		// 若text為null轉成0
		DecimalFormat df = null;
		BigDecimal number = null;
		try {
			text = StrUtil.isEmpty(text) ? "0" : text;
			df = null;
			String zeros = "";
			// 若text有帶","先移掉
			if (text.contains(",")) {
				text = text.replaceAll(",", "");
			}
			// 景毅有遇到數字尾帶負號的，多個判斷式
			if (text.substring(text.length() - 1).equals("-")) {
				text = text.substring(0, text.length() - 1);
			}
			// 遇到數字尾帶正號的，多個判斷式
			if (text.substring(text.length() - 1).equals("+")) {
				text = text.substring(0, text.length() - 1);
			}
						
			if (digits != 0) {
				if (text.indexOf(".") > 0) {

					for (int i = 0; i < digits; i++) {
						zeros += "0";
					}
					df = new DecimalFormat("###,##0." + zeros);
				} else {
					for (int i = 0; i < digits; i++) {
						zeros += "0";
					}
					df = new DecimalFormat("###,##0." + zeros);
				}
			} else {
				df = new DecimalFormat("###,##0");
			}

			number = new BigDecimal(text);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.trace(ESAPIUtil.vaildLog("wrong text >> " + text)); 
		}
		return df.format(number);
	}
	public static String addDot(String text,int digits) {
		String result="";
		
		// 景毅有遇到數字尾帶負號的，多個判斷式
		if (text.substring(text.length() - 1).equals("-")) {
			text = text.substring(0, text.length() - 1);
		}
		// 遇到數字尾帶正號的，多個判斷式
		if (text.substring(text.length() - 1).equals("+")) {
			text = text.substring(0, text.length() - 1);
		}
		
		result = text.substring(0, text.length()-digits)+"."+text.substring((text.length()-digits), text.length());
		return result;
	}
	
	
	/**
	 * 簡易的字串數字反格式化
	 * ex:500,000.00 >> 500000
	 * @param text
	 * @return
	 */
	public static String unfmtAmount(String text ) {
		// 若text為null轉成0
		DecimalFormat df = null;
		BigDecimal number = null;
		try {
			text = StrUtil.isEmpty(text) ? "0" : text;
			if (text.contains(",")) {
				text = text.replaceAll(",", "");
			}
//			df = new DecimalFormat("###,##0");
			number = new BigDecimal(text).setScale(0);
			log.debug("number>>{}",number.toString());
			System.out.println("number>>{}"+number.toString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.trace(ESAPIUtil.vaildLog("wrong text >> " + text));
		}
		return number.toString();
	}

	/**
	 * Added by Richard Yang on 2004-01-09
	 * @param a_str_expr 表示待格式化的数值字符串
	 * @param a_i_decimal 表示格式化后的数值字符串中小数点后面的位数
	 * @return
	 * @author Modified by Richard Yang on 2004-01-11
	 */
	public static String formatNumberString(String a_str_expr, int a_i_decimal) {
		String str_nocomma = "";
		String str_intpart = "";
		String str_decpart = "";
		String str_result = "";
		StringBuffer str_buffer = new StringBuffer();
		double d_amt = 0;
		double d_temp = 0;
		boolean b_negative = false;
		try {
			if (a_str_expr.equals("")) {
				return "";
			} else {
				try {
					str_nocomma = deleteComma(a_str_expr);
					// 判斷正負號
					if (str_nocomma.indexOf("+") != -1) {
						int sign = str_nocomma.indexOf("+");
						if (sign == 0) {
							str_nocomma = str_nocomma.substring(1);
						} else {
							str_nocomma = str_nocomma.substring(0, str_nocomma.length() - 1);
						}
					} else if (str_nocomma.indexOf("-") != -1) {
						int sign = str_nocomma.indexOf("-");
						if (sign == 0) {
							str_nocomma = str_nocomma.substring(1);
						} else {
							str_nocomma = str_nocomma.substring(0, str_nocomma.length() - 1);
						}
						b_negative = true;
					}
				} catch (Exception e) {
					//Avoid Information Exposure Through an Error Message
					log.error("formatNumberString error >> {}",e);
				}
			}
			d_amt = Double.parseDouble(str_nocomma);
			if (a_i_decimal != 0 && str_nocomma.indexOf(".") == -1) {
				d_temp = Math.pow(10, a_i_decimal);
				// d_amt = Math.round( d_amt * d_temp ) / d_temp;
				d_amt /= d_temp;
			} else if (a_i_decimal != 0 && str_nocomma.indexOf(".") != -1) {
				// do nothing
			} else {
				d_amt = Math.round(d_amt);
			}
			java.math.BigDecimal big = new java.math.BigDecimal(d_amt);
			java.math.BigDecimal one = new java.math.BigDecimal(1.0);
			str_nocomma = (big.divide(one, a_i_decimal, java.math.BigDecimal.ROUND_HALF_UP)).toString();
			if (str_nocomma.indexOf(".") != -1) {
				str_intpart = str_nocomma.substring(0, str_nocomma.indexOf("."));
				str_decpart = str_nocomma.substring(str_nocomma.indexOf(".") + 1);
			} else {
				str_intpart = str_nocomma;
				if (a_i_decimal != 0) {
					str_decpart = new Double(Math.pow(10, (-1) * a_i_decimal)).toString();
				} else {
					str_decpart = "";
				}
			}
			if (str_intpart.length() % 3 == 0) {
				for (int i = 0; i < (str_intpart.length() / 3); i++) {
					str_buffer.append(str_intpart.substring(i * 3, i * 3 + 3));
					str_buffer.append(",");
				}
				str_result = (str_buffer.toString()).substring(0, str_buffer.toString().length() - 1);
			} else if (str_intpart.length() % 3 == 1) {
				str_buffer.append(str_intpart.substring(0, 1));
				str_buffer.append(",");
				for (int i = 0; i < Math.floor((str_intpart.length() / 3)); i++) {
					str_buffer.append(str_intpart.substring(i * 3 + 1, i * 3 + 4));
					str_buffer.append(",");
				}
				str_result = (str_buffer.toString()).substring(0, str_buffer.toString().length() - 1);
			} else if (str_intpart.length() % 3 == 2) {
				str_buffer.append(str_intpart.substring(0, 2));
				str_buffer.append(",");
				for (int i = 0; i < Math.floor((str_intpart.length() / 3)); i++) {
					str_buffer.append(str_intpart.substring(i * 3 + 2, i * 3 + 5));
					str_buffer.append(",");
				}
				str_result = (str_buffer.toString()).substring(0, str_buffer.toString().length() - 1);
			}
			if (a_i_decimal != 0) {
				str_result = str_result + "." + str_decpart;
			} else {
				str_result = str_result + str_decpart;
			}
			if (b_negative) {
				str_result = "-" + str_result;
			}
		} catch (Exception exc) {
			str_result = "數值格式錯誤:" + a_str_expr;
		}
		return str_result;
	}
	
	/**
	 * 
	 * @param a_str_expr 表示待格式化的数值字符串
	 * @param a_i_decimal 表示格式化后的数值字符串中小数点后面的位数
	 * @param a_str_ccy  傳入幣別
	 * @return
	 */
	public static String formatNumberStringByCcy(String a_str_expr, int a_i_decimal, String a_str_ccy)
	{
	     String str_nocomma = "";
	     String str_intpart = "";
	     String str_decpart = "";
	     String str_result = "";
	     StringBuffer str_buffer = new StringBuffer();	     
	     double d_amt = 0;
	     double d_temp = 0;	      	
	     boolean b_negative = false;
	     
	  try 
	  {         	     
	     if(a_str_expr.equals(""))
	     {
	         return "";
	     }
	     else
	     {	
	         try {
	        	 if (a_str_ccy.equals("TWD") || a_str_ccy.equals("JPY")) {
	        		 a_i_decimal = 0;
	        	 }
	             str_nocomma = deleteComma(a_str_expr);
	     	     //判斷正負號
	     	     if (str_nocomma.indexOf("+") != -1)
	     	     {
	     	    	 int sign = str_nocomma.indexOf("+");
	     	    	 
	     	    	 if (sign == 0) {
	     	    		 str_nocomma = str_nocomma.substring(1);
	     	    	 }
	     	    	 else {
	     	    		 str_nocomma = str_nocomma.substring(0, str_nocomma.length()-1);
	     	    	 }
	     	     }
	     	     else if (str_nocomma.indexOf("-") != -1)
	     	     {
	     	    	 int sign = str_nocomma.indexOf("-");
	     	    	 if (sign == 0) {
	     	    		 str_nocomma = str_nocomma.substring(1);
	     	    	 }
	     	    	 else {
	     	    		 str_nocomma = str_nocomma.substring(0, str_nocomma.length()-1); 		
	     	    	 }
	  	    		 b_negative = true;
	     	     }	     
	     	  
	         }catch(Exception e)
	         {
	        	//Avoid Information Exposure Through an Error Message
	        	//e.printStackTrace();
	        	log.error("formatNumberStringByCcy error >> {}",e);
	         }
	     }	       
	     d_amt = Double.parseDouble(str_nocomma);	     
	     
	     if (a_i_decimal != 0 && str_nocomma.indexOf(".") == -1)
	     {          
	         d_temp = Math.pow(10, a_i_decimal);
	         //d_amt = Math.round( d_amt * d_temp ) / d_temp;  
	         d_amt /= d_temp;
	     }
	     else if (a_i_decimal != 0 && str_nocomma.indexOf(".") != -1)
	     {
	     	 //do nothing        
	     }	   
	     else
	     {
	    	 d_amt = Math.round( d_amt );		    	 
	     }
	 
	     java.math.BigDecimal big = new java.math.BigDecimal(d_amt);
	     java.math.BigDecimal one = new java.math.BigDecimal(1.0);	     
	     str_nocomma = ( big.divide(one, a_i_decimal, java.math.BigDecimal.ROUND_HALF_UP) ).toString();	      
	     	
	     if(str_nocomma.indexOf(".") != -1){
	         str_intpart = str_nocomma.substring(0, str_nocomma.indexOf("."));
	         str_decpart = str_nocomma.substring(str_nocomma.indexOf(".") + 1);	         	        
	     }else
	     {
	         str_intpart = str_nocomma;
	         
	         if(a_i_decimal !=0){
	             str_decpart = new Double( Math.pow(10, (-1) * a_i_decimal)).toString();
	         }else 
	         {
	             str_decpart = "";	
	         }     
	     }
	    
	     if(str_intpart.length()%3 ==0)
	     {
	       	 for(int i=0;i<(str_intpart.length()/3);i++)
	       	 {
	       		  str_buffer.append(str_intpart.substring(i*3, i*3 + 3));
	       		  str_buffer.append(",");
	       	 }
	        	
	       	 str_result = (str_buffer.toString()).substring(0, str_buffer.toString().length()-1);	        	
	     }else  if(str_intpart.length()%3 ==1)
	     {
	       	  str_buffer.append(str_intpart.substring(0,1));
	          str_buffer.append(",");
	                	
	          for(int i=0;i<Math.floor((str_intpart.length()/3));i++)
	          {
	        	   str_buffer.append(str_intpart.substring(i*3+1, i*3 + 4));
	        	   str_buffer.append(",");
	          }
	        	
	          str_result = (str_buffer.toString()).substring(0, str_buffer.toString().length()-1);	        	
	     }else  if(str_intpart.length()%3 ==2)
	     {
	          str_buffer.append(str_intpart.substring(0,2));
	          str_buffer.append(",");
	                	
	          for(int i=0;i<Math.floor((str_intpart.length()/3));i++)
	          {
	        	   str_buffer.append(str_intpart.substring(i*3+2, i*3 + 5));
	        	   str_buffer.append(",");
	          }
	        	
	          str_result = (str_buffer.toString()).substring(0, str_buffer.toString().length()-1);	        	
	     }        
        
         if(a_i_decimal !=0)
         {
             str_result = str_result + "." + str_decpart;
         }else
         {
             str_result = str_result + str_decpart;	
         }           
	    
	     if(b_negative)
	     {
	     	str_result = "-" + str_result;  
	     }	     	
	  }  
	  catch (Exception exc)
	  {
		 str_result = "數值格式錯誤:" + a_str_expr;		   
	  }
	   
	   return str_result;		
	}	

	/**
	 * 去掉撇節符號。例如：123,456.00 -> 123456.00
	 * 
	 * @param a_str_String
	 * @return
	 * @throws Exception
	 */
	public static String deleteComma(String a_str_String) throws Exception {
		int i;
		StringBuffer sb = new StringBuffer();
		String s = a_str_String;
		try {
			if (s == null) {
				return null;
			}
			// 去掉撇節符號。例如：123,456.00 -> 123456.00
			for (i = s.indexOf(","); i > 0;) {
				sb.append(s.substring(0, i));
				s = s.substring(i + 1);
				i = s.indexOf(",");
			}
			sb.append(s);
			return sb.toString();
		} catch (Exception exc) {
			throw new Exception("00001");
		}
	}
	
	

	public static String formatDouble(double a_d_UnFormated, int a_i_fractionLen) throws Exception
	{
		try
		{
			
			String str_format = "";
			for (int i =0 ; i< a_i_fractionLen ; i++)
			{
			  str_format += "0";
			}

			if (a_i_fractionLen==0)
			{
			  str_format = "#0";
			} else {
			  str_format = "#0." + str_format;
			}
			DecimalFormat obj_DecimalFormat = new DecimalFormat(str_format);
			String str_Formated = obj_DecimalFormat.format(a_d_UnFormated);
			return str_Formated;
		}
		catch (Exception exc)
		{
			throw new Exception("00001");
		}
	}
	
	
	
	public static void main(String[] args) {
//		System.out.println();
//		String a =addDot("05000000",6);
//		System.out.println(fmtAmount(a,6));
		
//		String s = "50000000";
//		unfmtAmount(s);
		
//		String s = "000000000000100";
//		System.out.println(formatNumberString(s,2));
//		System.out.println(formatNumberString(s,8));
	}
}