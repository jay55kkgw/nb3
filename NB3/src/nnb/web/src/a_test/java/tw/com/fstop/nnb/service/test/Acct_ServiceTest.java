package tw.com.fstop.nnb.service.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import tw.com.fstop.nnb.service.Acct_Service;

public class Acct_ServiceTest {

	Acct_Service as = new Acct_Service();
	
	@Test
	public void getFormatedNumTest_100000()
	{
		String num = "100000";
		String expected = "1,000.00";
		
		String actual = as.getFormatedAmount(num);
		
		assertEquals(expected, actual);
	}
	
	@Test
	public void getFormatedNumTest_Positive()
	{
		String num = "+100000";
		String expected = "1,000.00";
		
		String actual = as.getFormatedAmount(num);
		
		assertEquals(expected, actual);
	}
	
	@Test
	public void getFormatedNumTest_100789()
	{
		String num = "100789";
		String expected = "1,007.89";
		
		String actual = as.getFormatedAmount(num);
		
		assertEquals(expected, actual);
	}
	
	
	@Test
	public void getFormatedNumTest_Negative()
	{
		String num = "-100000";
		String expected = "-1,000.00";
		
		String actual = as.getFormatedAmount(num);
		
		assertEquals(expected, actual);
	}
	
	@Test
	public void getFormatedNumTest_tailNegative()
	{
		String num = "100000-";
		String expected = "-1,000.00";
		
		String actual = as.getFormatedAmount(num);
		
		assertEquals(expected, actual);
	}
	
	
	@Test
	public void getFormatedAmountTest_inputHas2Scale_return2Scale()
	{
		String num = "456021.40";
		String expected = "456,021.40";
		
		String actual = as.getFormatedAmount(num);
		
		assertEquals(expected, actual);
	}
	
	@Test
	public void getFormatedAmountTest_inputHas4Scale_returnInputScale()
	{
		String num = "456021.8640";
		String expected = "456,021.8640";
		
		String actual = as.getFormatedAmount(num);
		
		assertEquals(expected, actual);
	}
	
	
	@Test
	public void getFormatedRateTest()
	{
		String rate = "00123";
		String expected = "0.123";
		
		String actual = as.getFormatedRate(rate);
		
		assertEquals(expected, actual);
	}
	
	@Test
	public void getFormatedRateTest_tailZero()
	{
		String rate = "01090";
		String expected = "1.090";
		
		String actual = as.getFormatedRate(rate);
		
		assertEquals(expected, actual);
	}
	
	@Test
	public void getFormatedRateTest_scale3()
	{
		String rate = "01200";
		String expected = "1.200";
		
		String actual = as.getFormatedRate(rate);
		
		assertEquals(expected, actual);
	}
}
