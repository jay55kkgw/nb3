package tw.com.fstop.tbb;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.RestTemplate;

public class RESTfulTest {

	
	
	public static RestTemplate setHttpConfigI() {
		Integer connectTimeout = 5*1000;
		Integer socketTimeout = 10*1000;

		RequestConfig requestConfig;
		CloseableHttpClient httpClient;
		
		
		requestConfig = RequestConfig.custom()
	              .setConnectTimeout(connectTimeout)
	              .setSocketTimeout(socketTimeout)
	              .build();
	     httpClient = HttpClientBuilder.create()
	              .setDefaultRequestConfig(requestConfig)
	              .build();
		
	     HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
		 requestFactory.setHttpClient(httpClient);
		 requestFactory.setBufferRequestBody(false);
		 requestFactory.setConnectTimeout(6*1000);
		 requestFactory.setConnectionRequestTimeout(6*1000);
		 requestFactory.setReadTimeout(6*1000);
//		 ClientHttpRequestFactory  Factory = requestFactory;
	     
	     RestTemplate restTemplate = new RestTemplate();
	     restTemplate.setRequestFactory(requestFactory);
//	     restTemplate.setRequestFactory(Factory);
	     return restTemplate;
	}
	
	
	public static RestTemplate setHttpConfig2() {

		SSLContext sslContext = null;
		RestTemplate restTemplate = null;
		TrustStrategy acceptingTrustStrategy = new TrustStrategy() {

			@Override
			public boolean isTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
				// TODO Auto-generated method stub
				// return false;
				System.out.println("X509Certificate >>{} , String {}"+ arg0 +",arg1>>"+arg1);
				return true;
			}
		};

		try {
			sslContext = org.apache.http.ssl.SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy)
					.build();

//			SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);
			 SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext ,
			 new String[]{"TLSv1.2"},null,
			// SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext ,
			// new String[]{"TLSv1","TLSv1.1", "TLSv1.2"},null,
			 SSLConnectionSocketFactory.STRICT_HOSTNAME_VERIFIER);

			CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(csf).build();
			HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();

			requestFactory.setHttpClient(httpClient);

			restTemplate = new RestTemplate(requestFactory);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//Avoid Information Exposure Through an Error Message
			//log.error("setHttpConfig2 error >> {}",e);
			System.err.println(e);
		}
		return restTemplate;
	}

	@ResponseStatus
	public static void sendTest() {
//		String url = "https://nnb.tbb.com.tw/TBBNBAppsWeb_MB/services/";
//		String url = "https://www.programcreek.com/java-api-examples/index.php?api=org.springframework.http.client.HttpComponentsClientHttpRequestFactory";
//		String url = "https://nnb.tbb.com.tw/TBBNBAppsWeb_MB/services/BatchWebService?wsdl";
		String url = "https://ebank.tbb.com.tw/";
//		String url = "https://nnb.tbb.com.tw";
		String result = null;
		String request = null;
		RestTemplate restTemplate = null;
		// GET 使用getForObject
		// result = restTemplate.getForObject(REST_TW_URL, objclass, params);
		try {
			System.out.println("url>>" + url);
			// setHttpConfig();
//			restTemplate = new RestTemplate();
//			result = restTemplate.getForObject(url, String.class);
//			restTemplate = setHttpConfig2();
			restTemplate = setHttpConfigI();
			result = restTemplate.postForObject(url, request,  String.class);
//			result = restTemplate.getForObject(url, String.class);
			System.out.println("result>>{}" + result);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			System.err.println(e);
		}

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		sendTest();
	}

}
