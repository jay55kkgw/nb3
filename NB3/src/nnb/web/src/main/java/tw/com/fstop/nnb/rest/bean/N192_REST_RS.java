package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N192_REST_RS extends BaseRestBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2909697943363162947L;
	
	private String REC_NO;
	private String USERDATA;
	private String CMPERIOD;
	private String __OCCURS;
	LinkedList<N192_REST_RSDATA> REC;
	
	public String getCMPERIOD() {
		return CMPERIOD;
	}
	public void setCMPERIOD(String cMPERIOD) {
		CMPERIOD = cMPERIOD;
	}
	public String get__OCCURS() {
		return __OCCURS;
	}
	public void set__OCCURS(String __OCCURS) {
		this.__OCCURS = __OCCURS;
	}
	public String getREC_NO() {
		return REC_NO;
	}
	public void setREC_NO(String rEC_NO) {
		REC_NO = rEC_NO;
	}
	public String getUSERDATA() {
		return USERDATA;
	}
	public void setUSERDATA(String uSERDATA) {
		USERDATA = uSERDATA;
	}
	public LinkedList<N192_REST_RSDATA> getREC() {
		return REC;
	}
	public void setREC(LinkedList<N192_REST_RSDATA> rEC) {
		REC = rEC;
	}

}
