package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class EloanApi_REST_RQ extends BaseRestBean_CC implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1884299917800110163L;
	
	private String CUSIDN;//統一編號
	private String QUOTA;//申請信用額度
	private String FGTXSTATUS;//申請期間
	
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getQUOTA() {
		return QUOTA;
	}
	public void setQUOTA(String qUOTA) {
		QUOTA = qUOTA;
	}
	public String getFGTXSTATUS() {
		return FGTXSTATUS;
	}
	public void setFGTXSTATUS(String fGTXSTATUS) {
		FGTXSTATUS = fGTXSTATUS;
	}
}
