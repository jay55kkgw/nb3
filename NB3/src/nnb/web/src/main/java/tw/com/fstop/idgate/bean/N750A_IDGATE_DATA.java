package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class N750A_IDGATE_DATA implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = 229152612181606122L;
	
	//0：非約定，1：約定
	@SerializedName(value = "FLAG")
	private String FLAG;
	
	//轉出帳號
	@SerializedName(value = "OUTACN")
	private String OUTACN;
	
	//代收截止日
	@SerializedName(value = "PAYDUE")
	private String PAYDUE;
	
	//銷帳編號
	@SerializedName(value = "WAT_NO")
	private String WAT_NO;
	
	//查核碼
	@SerializedName(value = "CHKCOD")
	private String CHKCOD;
	
	//繳款金額 
	@SerializedName(value = "AMOUNT")
	private String AMOUNT;
	
	public String getFLAG() {
		return FLAG;
	}

	public void setFLAG(String fLAG) {
		FLAG = fLAG;
	}

	public String getOUTACN() {
		return OUTACN;
	}

	public void setOUTACN(String oUTACN) {
		OUTACN = oUTACN;
	}

	public String getPAYDUE() {
		return PAYDUE;
	}

	public void setPAYDUE(String pAYDUE) {
		PAYDUE = pAYDUE;
	}

	public String getWAT_NO() {
		return WAT_NO;
	}

	public void setWAT_NO(String wAT_NO) {
		WAT_NO = wAT_NO;
	}

	public String getCHKCOD() {
		return CHKCOD;
	}

	public void setCHKCOD(String cHKCOD) {
		CHKCOD = cHKCOD;
	}

	public String getAMOUNT() {
		return AMOUNT;
	}

	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}

	
	
}
