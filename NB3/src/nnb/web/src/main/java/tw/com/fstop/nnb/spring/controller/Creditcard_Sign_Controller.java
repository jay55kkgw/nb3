package tw.com.fstop.nnb.spring.controller;

import java.util.Base64;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.service.Card_Sign_Service;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.WebUtil;

@SessionAttributes({ SessionUtil.CUSIDN, SessionUtil.PD,SessionUtil.DOWNLOAD_DATALISTMAP_DATA,
	SessionUtil.PRINT_DATALISTMAP_DATA,SessionUtil.RESULT_LOCALE_DATA, SessionUtil.CONFIRM_LOCALE_DATA, 
	SessionUtil.RESULT_LOCALE_DATA,SessionUtil.CUSIDN, SessionUtil.TRANSFER_CONFIRM_TOKEN, SessionUtil.TRANSFER_RESULT_TOKEN, 
	SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, SessionUtil.TRANSFER_DATA, SessionUtil.STEP1_LOCALE_DATA, SessionUtil.DPUSERNAME })

@Controller
@RequestMapping(value = "/CREDIT/SIGN")
public class Creditcard_Sign_Controller {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	Card_Sign_Service card_sign_service;
	
	/**
	 * 線上簽署信用卡來電分期專案申請書約款
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/card_sign")
	public String card_sign(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/creditcard_sign/card_sign";
//		清除切換語系時暫存的資料
		SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
		SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
		return target;
	}
	
	
	/**
	 * 簽署信用卡來電分期專案申請書約款(申請)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/card_sign_apply_confirm")
	public String card_sign_apply_confirm(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{		
		String target = "/error";
		log.trace("card_sign_apply_confirm start");
		BaseResult bs = new BaseResult();
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {
		
			
			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = card_sign_service.getTxToken();
			log.trace("card_sign_apply_confirm.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			if(bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			}
			// 登入時的身分證字號
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			okMap.put("TXTOKEN", ((Map<String,String>) bs.getData()).get("TXTOKEN"));
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("card_sign_apply_confirm error >> {}",e);
		} finally {
			target = "creditcard_sign/card_sign_apply_confirm";
			model.addAttribute("result_data", okMap);
		}
		return target;
	}
	

	/**
	 * 簽署信用卡來電分期專案申請書約款(取消)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/card_sign_cancle_confirm")
	public String card_sign_cancle_confirm(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{		
		String target = "/error";
		log.trace("card_sign_cancle_confirm start");
		BaseResult bs = new BaseResult();
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {
            
			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = card_sign_service.getTxToken();
			log.trace("card_sign_cancle_confirm.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			if(bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			}
			// 登入時的身分證字號
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			okMap.put("TXTOKEN", ((Map<String,String>) bs.getData()).get("TXTOKEN"));
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("card_sign_cancle_confirm error >> {}",e);
		} finally {
			target = "creditcard_sign/card_sign_cancle_confirm";
			model.addAttribute("result_data", okMap);
		}
		return target;
	}


	/**
	 * 簽署信用卡來電分期專案申請書約款(申請)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/card_sign_apply_result")
	public String card_sign_apply_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{		
		String target = "/error";
		log.trace("card_sign_apply_confirm_result start");
		BaseResult bs = null;
		Map<String, String> okMap = null;
		try {
			//okMap = reqParam;
			log.trace(ESAPIUtil.vaildLog("reqParam >> {}"+ reqParam));
			okMap = ESAPIUtil.validStrMap(reqParam);
			log.trace(ESAPIUtil.vaildLog("okMap >> {}"+okMap));
			boolean hasLocale = okMap.containsKey("locale");
			bs = new BaseResult();
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				okMap = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null);
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				// 登入時的身分證字號
				log.trace("card_sign_apply_result.validate TXTOKEN...");
				log.trace("card_sign_apply_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("card_sign_apply_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("card_sign_apply_result.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("card_sign_apply_result TXTOKEN 不正確，TXTOKEN>>{}"+okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						!okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("card_sign_apply_result.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("card_sign_apply_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}" + okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				log.debug(ESAPIUtil.vaildLog("reqParam >> {}"+CodeUtil.toJson(reqParam)));
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
				String name = (String) SessionUtil.getAttribute(model, SessionUtil.DPUSERNAME, null);
				bs = card_sign_service.card_sign_result(cusidn, reqParam, name);
				bs.addData("CUSIDN", cusidn);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, okMap);
				
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("card_sign_apply_result error >> {}",e);
		} finally {
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TXTOKEN"));
			if(bs!=null && bs.getResult()) {
				target = "creditcard_sign/card_sign_apply_result";
				List<Map<String, Object>> dataListMap = ((List<Map<String, Object>>) ((Map<String, Object>) bs.getData()).get("REC"));
				log.debug("dataListMap={}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
				model.addAttribute("result_data", bs);
				model.addAttribute("input_data", okMap);
			}else {
				bs.setPrevious("/CREDIT/SIGN/card_sign");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	

	/**
	 * 簽署信用卡來電分期專案申請書約款(申請)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/card_sign_cancle_result")
	public String card_sign_cancle_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{		
		String target = "/error";
		log.trace("card_sign_apply_confirm_result start");
		BaseResult bs = null;
		Map<String, String> okMap = null;
		try {
// 			解決Trust Boundary Violation
			okMap = ESAPIUtil.validStrMap(reqParam);
			boolean hasLocale = okMap.containsKey("locale");
			bs = new BaseResult();
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				okMap = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null);
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				// 登入時的身分證字號
				log.trace("card_sign_cancle_result.validate TXTOKEN...");
				log.trace("card_sign_cancle_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("card_sign_cancle_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("card_sign_cancle_result.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("card_sign_cancle_result TXTOKEN 不正確，TXTOKEN>>{}" + okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						!okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("card_sign_cancle_result.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("card_sign_cancle_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}" + okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				log.debug(ESAPIUtil.vaildLog("reqParam >> {}"+CodeUtil.toJson(reqParam)));
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
				String name = (String) SessionUtil.getAttribute(model, SessionUtil.DPUSERNAME, null);
				bs = card_sign_service.card_sign_result(cusidn, reqParam, name);
				bs.addData("CUSIDN", cusidn);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, okMap);
				
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("card_sign_cancle_result error >> {}",e);
		} finally {
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TXTOKEN"));
			if(bs!=null && bs.getResult()) {
				target = "creditcard_sign/card_sign_cancle_result";
				List<Map<String, Object>> dataListMap = ((List<Map<String, Object>>) ((Map<String, Object>) bs.getData()).get("REC"));
				log.debug("dataListMap={}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
				model.addAttribute("result_data", bs);
				model.addAttribute("input_data", okMap);
			}else {
				bs.setPrevious("/CREDIT/SIGN/card_sign");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * 線上簽署信用卡來電分期專案申請書約款
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/card_sign_menu")
	public String card_sign_menu(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/creditcard_sign/card_sign_menu";
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		boolean hasLocale = okMap.containsKey("locale");
		if (hasLocale) {
			okMap = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
		}else {
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, okMap);
		}
		model.addAttribute("input_data", okMap);
		return target;
	}
}
