package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class TD02_REST_RQ extends BaseRestBean_CC implements Serializable {

	
	private static final long serialVersionUID = 8019822747111651798L;
	/**
	 * 
	 */
	
	
	String UID ;
	String CARDTYPE ;
	String FGPERIOD ;
	String CMSDATE ;
	String CMEDATE ;
	String CARDNUM_ALL;
	
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	public String getCARDTYPE() {
		return CARDTYPE;
	}
	public void setCARDTYPE(String cARDTYPE) {
		CARDTYPE = cARDTYPE;
	}
	public String getFGPERIOD() {
		return FGPERIOD;
	}
	public void setFGPERIOD(String fGPERIOD) {
		FGPERIOD = fGPERIOD;
	}
	public String getCMSDATE() {
		return CMSDATE;
	}
	public void setCMSDATE(String cMSDATE) {
		CMSDATE = cMSDATE;
	}
	public String getCMEDATE() {
		return CMEDATE;
	}
	public void setCMEDATE(String cMEDATE) {
		CMEDATE = cMEDATE;
	}
	public String getCARDNUM_ALL() {
		return CARDNUM_ALL;
	}
	public void setCARDNUM_ALL(String cARDNUM_ALL) {
		CARDNUM_ALL = cARDNUM_ALL;
	}
	
}
