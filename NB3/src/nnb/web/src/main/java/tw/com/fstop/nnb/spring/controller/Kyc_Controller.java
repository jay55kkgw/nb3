package tw.com.fstop.nnb.spring.controller;

import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.service.Bond_Purchase_Service;
import tw.com.fstop.nnb.service.Kyc_Service;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.TxnBondDataDao;
import tw.com.fstop.tbb.nnb.util.DateUtil;
import tw.com.fstop.util.DateTimeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.StrUtils;
import tw.com.fstop.web.util.WebUtil;


/**
 * KYC Controller
 */
@SessionAttributes({ SessionUtil.CUSIDN, SessionUtil.KYCDATE, SessionUtil.WEAK, SessionUtil.XMLCOD,
		SessionUtil.DPMYEMAIL, SessionUtil.KYC, SessionUtil.STEP1_LOCALE_DATA, SessionUtil.STEP2_LOCALE_DATA,
		SessionUtil.STEP3_LOCALE_DATA, SessionUtil.CONFIRM_LOCALE_DATA, SessionUtil.IDGATE_TRANSDATA, SessionUtil.RESULT_LOCALE_DATA,
		SessionUtil.USERIP })
@Controller
@RequestMapping(value = "/KYC")
public class Kyc_Controller {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	I18n i18n;
	
	@Autowired
	private Kyc_Service kyc_Service;
	
	/**
	 * 
	 */
	@RequestMapping(value = "/kyc_personal")
	public String kyc_personal(HttpServletRequest request,@RequestParam Map<String,String> requestParam,Model model){
		String target = "";
		return target;
		
	}
	/**
	 * 
	 * @param request
	 * @param requestParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/kyc_bussiness")
	public String kyc_bussiness(HttpServletRequest request,@RequestParam Map<String,String> requestParam,Model model){
		String target = "";
		return target;
		
	}
	

}
