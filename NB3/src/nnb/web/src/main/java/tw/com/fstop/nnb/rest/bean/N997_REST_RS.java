package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N997_REST_RS extends BaseRestBean implements Serializable{
	private static final long serialVersionUID = 1874898449756769229L;
	
	private LinkedList<N997_REST_RSDATA> REC;

	public LinkedList<N997_REST_RSDATA> getREC(){
		return REC;
	}
	public void setREC(LinkedList<N997_REST_RSDATA> rEC){
		REC = rEC;
	}
}
