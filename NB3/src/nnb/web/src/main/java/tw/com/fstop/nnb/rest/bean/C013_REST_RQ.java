package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * C013電文RQ
 */
public class C013_REST_RQ extends BaseRestBean_FUND implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8458947184749077614L;
	private String CUSIDN;		// 身份證號
	private String BEGDATE;		// 查詢起日
	private String ENDDATE;		// 查詢迄日
	
	// 歷史交易明細查詢使用
	private String CDNO;	// 信託號碼
	
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getBEGDATE() {
		return BEGDATE;
	}
	public void setBEGDATE(String bEGDATE) {
		BEGDATE = bEGDATE;
	}
	public String getENDDATE() {
		return ENDDATE;
	}
	public void setENDDATE(String eNDDATE) {
		ENDDATE = eNDDATE;
	}
	public String getCDNO() {
		return CDNO;
	}
	public void setCDNO(String cDNO) {
		CDNO = cDNO;
	}
	
}