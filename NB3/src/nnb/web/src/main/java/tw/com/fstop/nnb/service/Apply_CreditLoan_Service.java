package tw.com.fstop.nnb.service;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.SafeFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.multipart.MultipartFile;

import fstop.orm.po.ADMBHCONTACT;
import fstop.orm.po.TXNLOANAPPLY;
import tbb.cxf.client.TbbSc;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.AdmBhContactDao;
import tw.com.fstop.tbb.nnb.dao.AdmZipDataDao;
import tw.com.fstop.tbb.nnb.dao.TxnLoanApplyDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.web.util.FTPUtil;
import tw.com.fstop.web.util.WebUtil;

@Service
public class Apply_CreditLoan_Service extends Base_Service
{
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private TxnLoanApplyDao txnLoanApplyDao;
	@Autowired
	private AdmBhContactDao admBhContactDao;
	@Autowired
	private AdmZipDataDao admZipDataDao;
	@Autowired
	ServletContext context; 
	@Autowired
	private I18n i18n;
	
	
	/**
	 * NA011 小額信貸申請資料查詢service
	 * 
	 * @param cusidn
	 * @return
	 */
	public BaseResult applyCreditLoanQuery(String cusidn)
	{
		BaseResult bs = new BaseResult();
		try
		{
			// query db
			bs = na011_doAction(cusidn);
			
			if(bs.getResult())
			{
				Map<String, Object> bsData = (Map<String, Object>) bs.getData();
				List<TXNLOANAPPLY> applyCases = (List<TXNLOANAPPLY>) bsData.get("REC");
				List<Map<String, Object>> rec = new ArrayList<>();
				log.debug("APPLYCASES DATA >>{}",applyCases);
				for(TXNLOANAPPLY po : applyCases)
				{
					String json = CodeUtil.toJson(po);
					log.debug("JSON DATA >>{}",json);
					Map<String, Object> row = CodeUtil.fromJson(json, Map.class);
					row.put("APPKIND", getAPPKIND(row.get("APPKIND").toString()));
					row.put("STATUS", getSTATUS(row.get("STATUS").toString()));
					rec.add(row);
				}
				bs.addData("REC", rec);
			}
		}
		catch (Exception e)
		{
			log.error("applyCreditLoanQuery error", e);
		}
		return bs;
	}
	
	
	/**
	 * NA011 小額信貸申請資料查詢doAction，此method是將舊bean邏輯搬運過來修改
	 * 舊bean以舊網銀正式版為基礎，而不是以Jimmy的版本，所以不需要連eLoan
	 * 
	 * @param cusidn
	 * @return
	 */
	public BaseResult na011_doAction(String cusidn)
	{
		BaseResult bs = new BaseResult();
		
		TbbSc sc = null;
		//STEP 1 CONNECT
		try {
			sc = getConnect();
			int s;
			try {
				s = sc.getConnect();
				List<TXNLOANAPPLY> r = txnLoanApplyDao.findByCUSID(cusidn);
				for(TXNLOANAPPLY ta : r) {
					String RCVNO = ta.getRCVNO();
					List list = sc.doQuery(RCVNO);
					if(list!=null && list.size()>0){
						Map map = (Map) list.get(0);
						String scRrst = sc.escapeNull(map.get("SCRRST"));
						String scFlow = sc.escapeNull(map.get("SCFLOW"));
						Map<String,Object> hm = new HashMap<String,Object>();
						hm.put("scRrst", scRrst);
						hm.put("scFlow", scFlow);
						ta.setSTATUS(CodeUtil.toJson(hm));
						txnLoanApplyDao.update(ta);
					}
				}
			} catch (Exception e) {
				log.trace("connect_Exception >> " + e);
				s= -1;
			}	//當傳回1才可服務 01:00~03:30我們server會停下來
			log.trace("connect_state >> " + s);
			if(s!=1) {
				throw new Exception();
			}
		}catch(Exception e){
			log.error("na011_doAction ERROR>>",e);
		}
		
		//STEP 2 query
		try 
		{
			List<TXNLOANAPPLY> r = txnLoanApplyDao.findByCUSID(cusidn);
			log.debug(ESAPIUtil.vaildLog("TXNLOANAPPLY　DATA >> " + r));
			if(r != null)
			{
				if(r.size() >= 0)
				{
					int cmrecnum = r.size();
					String CMQTIME = DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss");
					
					bs.addData("REC", r);
					bs.addData("CMQTIME", CMQTIME);
					bs.addData("CMRECNUM", String.valueOf(cmrecnum));
					bs.setResult(true);
					bs.setMessage("0000", i18n.getMsg("LB.X1805"));//查詢成功
				}
			}
		}
		catch (Exception e)
		{
			log.error("na011_doAction error", e);
		}
		return bs;
		
	}
	
	
	/**
	 * 貸款種類 TODO i18n
	 * 
	 * @param appkind
	 * @return
	 */
	public String getAPPKIND(String appkind)
	{
		String result = appkind;
		Map m = new HashMap();
		//卓越圓夢
		m.put("B", "LB.D0546");
		//e網輕鬆貸
		m.put("E", "LB.D0547");
		
		if(m.containsKey(appkind)) 
		{
			result = m.get(appkind).toString();
		}
		return result;
	}
	
	
	/**
	 * 審核狀況 TODO i18n
	 * 
	 * @param status
	 * @return
	 */
	public String getSTATUS(String status)
	{
		String result = "LB.X2269";
		
		try {
			Map<String,String> hm = new HashMap<String,String>();
			hm  = CodeUtil.fromJson(status, hm.getClass());
			String scFlow = hm.get("scFlow");
			String scRrst = hm.get("scRrst");
			if(scFlow.startsWith("C")) {
				//不核准
				result = "LB.X1808";
			}else if(!scFlow.equals("9")) {
				//送件中
				result = "LB.X1806";
			}else {
				if(scRrst.equals("3")) {
					//核准
					result = "LB.X1807";
				}else {
					//不核准
					result = "LB.X1808";
				}
			}
		}catch (Exception e)
		{
			log.error("getSTATUS error", e);
		}

		return result;
	}
	
	/**
	 * 準備前往申請小額信貸輸入頁P6的資料
	 */
	@SuppressWarnings("unchecked")
	public void prepareApplyCreditloanP6Data(Map<String,String> requestParam,BaseResult baseResult){
		Map<String,Object>tempbs=(Map<String, Object>) baseResult.getData();
		
		String APPKIND = requestParam.get("LOANTYPE");
		log.debug(ESAPIUtil.vaildLog("APPKIND >> " + APPKIND)); 
		tempbs.put("APPKIND",APPKIND);
		
		String APPString = "";
		if(APPKIND.equals("B") || APPKIND.equals("F")){
			APPString="（"+i18n.getMsg("LB.D0563_1")+"）";
		}
		else{
			APPString="（"+i18n.getMsg("LB.D0563")+"）";
		}
		log.debug("APPString={}",APPString);
		tempbs.put("APPString",APPString);
	
		List<Map<String,String>> ADMBHCONTACTList = getBhContact();
		
		if(!ADMBHCONTACTList.isEmpty()){
			tempbs.put("ADMBHCONTACTList",ADMBHCONTACTList);
			//model.addAttribute("ADMBHCONTACTList",ADMBHCONTACTList);
		}
		
//		Map<String,Object> bsData = (Map<String,Object>)baseResult.getData();
		
		String USERNAME = (String)tempbs.get("NAME");
		log.debug(ESAPIUtil.vaildLog("USERNAME >> " + USERNAME));
//		requestParam.put("USERNAME",USERNAME);
		tempbs.put("USERNAME",USERNAME);
		
		String hiddenUSERNAME = WebUtil.hideusername1Convert(USERNAME);
		tempbs.put("hiddenUSERNAME",hiddenUSERNAME);
		
		String BRTHDY = (String)tempbs.get("BRTHDY");
		log.trace(ESAPIUtil.vaildLog("BRTHDY >> " + BRTHDY));
//		requestParam.put("BRTHDY",BRTHDY);
		
		String BIRTHY = "";
		String BIRTHM = "";
		String BIRTHD = "";
		
		if(BRTHDY.length() >= 7){
			BIRTHY = BRTHDY.substring(0,3);
			BIRTHM = BRTHDY.substring(3,5);
			BIRTHD = BRTHDY.substring(5,7);
		}
		log.debug(ESAPIUtil.vaildLog("BIRTHY >> " + BIRTHY));
		log.debug(ESAPIUtil.vaildLog("BIRTHM >> " + BIRTHM));
		log.debug(ESAPIUtil.vaildLog("BIRTHD >> " + BIRTHD));
		tempbs.put("BIRTHY",BIRTHY);
		tempbs.put("BIRTHM",BIRTHM);
		tempbs.put("BIRTHD",BIRTHD);
		
		String POSTCOD1 = (String)tempbs.get("POSTCOD1");
		log.debug(ESAPIUtil.vaildLog("POSTCOD1 >> " + POSTCOD1));
		
		if(POSTCOD1.length() < 3){
			POSTCOD1 = "";
		}
		else{
			POSTCOD1 = POSTCOD1.substring(0,3);
		}
		log.debug(ESAPIUtil.vaildLog("POSTCOD1 >> " + POSTCOD1));
		tempbs.put("POSTCOD1",POSTCOD1);
		
		String PMTADR = (String)tempbs.get("PMTADR");
		log.debug(ESAPIUtil.vaildLog("PMTADR >> " + PMTADR));
		tempbs.put("PMTADR",PMTADR);
		
		String PMTADR1 = "";
		if(PMTADR.length() > 6){
			PMTADR1 = PMTADR.substring(6);
		}
		else{
			PMTADR1 = "";
		}
		log.debug(ESAPIUtil.vaildLog("PMTADR1 >> " + PMTADR1));
		tempbs.put("PMTADR1",PMTADR1);
		
		String POSTCOD2 = (String)tempbs.get("POSTCOD2");
		log.trace(ESAPIUtil.vaildLog("POSTCOD2 >> " + POSTCOD2));
		
		if(POSTCOD2.length() < 3){
			POSTCOD2 = "";
		}
		else{
			POSTCOD2 = POSTCOD2.substring(0,3);	
		}
		log.debug(ESAPIUtil.vaildLog("POSTCOD2 >> " + POSTCOD2));
		tempbs.put("POSTCOD2",POSTCOD2);
		
		String CTTADR = (String)tempbs.get("CTTADR") == null ? "" : (String)tempbs.get("CTTADR");
		log.debug(ESAPIUtil.vaildLog("CTTADR >> " + CTTADR));
		tempbs.put("CTTADR",CTTADR);
		
		String CTTADR1 = "";
		
		if(CTTADR.length() > 6){
			CTTADR1 = CTTADR.substring(6);
		}
		else{
			CTTADR1 = "";
		}
		
		tempbs.put("CTTADR1",CTTADR1);
		
		String TEL11 = (String)tempbs.get("ARACOD");  
		String TEL12 = (String)tempbs.get("TELNUM");
		String TEL13 = "";
		
		
		if(TEL12.indexOf("#") > -1){
			TEL13 = TEL12.substring(TEL12.indexOf("#") + 1);
			TEL12 = TEL12.substring(0,TEL12.indexOf("#"));
		}
		
		tempbs.put("TEL11",TEL11);
		tempbs.put("TEL12",TEL12);
		tempbs.put("TEL13",TEL13);
		
//		String MOBTEL = (String)tempbs.get("MOBTEL");
//		log.debug("MOBTEL={}",MOBTEL);
//		requestParam.put("MOBTEL",MOBTEL);
		
		List<Map<String,String>> cityList = getCityList();
		
		if(!cityList.isEmpty()){
			tempbs.put("cityList",cityList);
		}
		
		List<Map<String,String>> areaList;
		if(CTTADR.length() >= 3){
			areaList = getAreaList(CTTADR.substring(0,3));
		}
	    else{
	    	areaList = getAreaList("");
	    }
		if(!areaList.isEmpty()){
			tempbs.put("CTTADRList",areaList);
		}
		
		if(PMTADR.length() >= 3){
			areaList = getAreaList(PMTADR.substring(0,3));
		}
	    else{
	    	areaList = getAreaList("");
	    }
		if(!areaList.isEmpty()){
			tempbs.put("PMTADRList",areaList);
		}
		
		log.debug(ESAPIUtil.vaildLog("tempbs="+tempbs));
		//model.addAttribute("requestParam",requestParam);
	}
	/**
	 * 準備前往申請小額信貸輸入頁P7的資料
	 */
	public void prepareApplyCreditloanP7Data(Map<String,String> requestParam,BaseResult bs){
		try{
			Map<String,Object>TempBsData=(Map<String, Object>) bs.getData();
			String MARITAL = requestParam.get("MARITAL");
			log.debug(ESAPIUtil.vaildLog("MARITAL >> " + MARITAL)); 
			String MARITALNA = "";
			if(MARITAL.equals("1")){
				MARITALNA = "LB.D0595";//已婚
			}
			if(MARITAL.equals("2")){
				MARITALNA = "LB.D0596";//未婚
			}
			if(MARITAL.equals("3")){
				MARITALNA = "LB.D0572";//其他
			}
			log.debug("MARITALNA={}",MARITALNA);
			TempBsData.put("MARITALNA",MARITALNA);
			
			String APPKIND = requestParam.get("APPKIND");
			log.debug(ESAPIUtil.vaildLog("APPKIND >> " + APPKIND)); 
			
			String APPKINDNAME = "";
			if("B".equals(APPKIND)){
				APPKINDNAME = "LB.D0546";
			}
			if("E".equals(APPKIND)){
				APPKINDNAME = "LB.D0547";
			}
			log.debug("APPKINDNAME={}",APPKINDNAME);
			TempBsData.put("APPKINDNAME",APPKINDNAME);
			
			String PURPOSE = requestParam.get("PURPOSE");
			log.debug(ESAPIUtil.vaildLog("PURPOSE >> " + PURPOSE)); 
			String PURPOSENAME = "";
			if("B".equals(PURPOSE)){
				PURPOSENAME = "LB.D0565";//房屋修繕
			}
			if("D".equals(PURPOSE)){
				PURPOSENAME = "LB.D0566";//旅遊
			}
			if("E".equals(PURPOSE)){
				PURPOSENAME = "LB.D0567";//購置耐久消費財
			}
			if("G".equals(PURPOSE)){
				PURPOSENAME = "LB.D0568";//購買汽車
			}
			if("I".equals(PURPOSE)){
				PURPOSENAME = "LB.D0569";//進修
			}
			if("J".equals(PURPOSE)){
				PURPOSENAME = "LB.D0570";//結婚
			}
			if("K".equals(PURPOSE)){
				PURPOSENAME = "LB.D0571";//子女教育基金
			}
			if("Z".equals(PURPOSE)){
				PURPOSENAME = "LB.D0572";//其他
			}
			log.debug("PURPOSENAME={}",PURPOSENAME);
			TempBsData.put("PURPOSENAME",PURPOSENAME);
			
			String SEX = requestParam.get("SEX");
			log.debug(ESAPIUtil.vaildLog("SEX >> " + SEX)); 
			
			String SEXNAME = "";
			if("1".equals(SEX)){
				SEXNAME = "LB.D0579";//男
			}
			if("2".equals(SEX)){
				SEXNAME = "LB.D0580";//女
			}
			log.debug("SEXNAME={}",SEXNAME);
			TempBsData.put("SEXNAME",SEXNAME);
			
			String EDUS = requestParam.get("EDUS");

			log.debug(ESAPIUtil.vaildLog("EDUS >> " + EDUS)); 
			String EDUSNAME = "";
			if("1".equals(EDUS)){
				EDUSNAME = "LB.D0588";//博士
			}
			if("2".equals(EDUS)){
				EDUSNAME = "LB.D0589";//碩士
			}
			if("3".equals(EDUS)){
				EDUSNAME = "LB.D0590";//大學
			}
			if("4".equals(EDUS)){
				EDUSNAME = "LB.D0591";//專科
			}
			if("5".equals(EDUS)){
				EDUSNAME = "LB.D0592";//高中職
			}
			if("9".equals(EDUS)){
				EDUSNAME = "LB.D0572";//其他
			}
			log.debug("EDUSNAME={}",EDUSNAME);
			TempBsData.put("EDUSNAME",EDUSNAME);
			
			String HOUSE = requestParam.get("HOUSE");
			log.debug(ESAPIUtil.vaildLog("HOUSE >> " + HOUSE)); 
			String HOUSENAME = "";
			if("1".equals(HOUSE)){
				HOUSENAME = "LB.D0604";//自有或配偶持有無設定抵押權或設押本行
			}
			if("2".equals(HOUSE)){
				HOUSENAME = "LB.D0605";//自有或配偶持有已設定他行抵押權
			}
			if("3".equals(HOUSE)){
				HOUSENAME = "LB.D0606";//直系親屬持有
			}
			if("4".equals(HOUSE)){
				HOUSENAME = "LB.D0068";//租賃或宿舍
			}
			if("5".equals(HOUSE)){
				HOUSENAME = "LB.D0572";//其他
			}
			log.debug("HOUSENAME={}",HOUSENAME);
			TempBsData.put("HOUSENAME",HOUSENAME);
			
			String PROFNO = requestParam.get("PROFNO");
			log.debug(ESAPIUtil.vaildLog("PROFNO >> " + PROFNO)); 
			
			String PROFNONAME = "";
			if("01".equals(PROFNO)){PROFNONAME = "民意代表";}
			if("02".equals(PROFNO)){PROFNONAME = "公司負責人／董事／總經理／股東";}
			if("03".equals(PROFNO)){PROFNONAME = "主管";}
			if("04".equals(PROFNO)){PROFNONAME = "一般職員";}
			if("05".equals(PROFNO)){PROFNONAME = "業務人員";}
			if("06".equals(PROFNO)){PROFNONAME = "公營事業職工";}
			if("07".equals(PROFNO)){PROFNONAME = "文化藝術工作者";}
			if("08".equals(PROFNO)){PROFNONAME = "家庭主婦";}
			if("09".equals(PROFNO)){PROFNONAME = "廚師";}
			if("10".equals(PROFNO)){PROFNONAME = "臨時人員";}
			if("11".equals(PROFNO)){PROFNONAME = "退休人員";}
			if("12".equals(PROFNO)){PROFNONAME = "學生";}
			if("13".equals(PROFNO)){PROFNONAME = "無業";}
			if("14".equals(PROFNO)){PROFNONAME = "自營登記之司機";}
			if("15".equals(PROFNO)){PROFNONAME = "有營登或稅籍之自營業主";}
			if("16".equals(PROFNO)){PROFNONAME = "無營登或稅籍之自營業主";}
			if("17".equals(PROFNO)){PROFNONAME = "作業（技術或操作）員";}
			if("18".equals(PROFNO)){PROFNONAME = "工程師";}
			if("19".equals(PROFNO)){PROFNONAME = "財務人員";}
			if("20".equals(PROFNO)){PROFNONAME = "駕駛（司機）";}
			if("21".equals(PROFNO)){PROFNONAME = "警衛";}
			if("22".equals(PROFNO)){PROFNONAME = "工友";}
			if("23".equals(PROFNO)){PROFNONAME = "技工";}
			if("24".equals(PROFNO)){PROFNONAME = "約聘人員";}
			if("25".equals(PROFNO)){PROFNONAME = "教師";}
			if("26".equals(PROFNO)){PROFNONAME = "律師";}
			if("27".equals(PROFNO)){PROFNONAME = "醫師";}
			if("28".equals(PROFNO)){PROFNONAME = "會計師";}
			if("29".equals(PROFNO)){PROFNONAME = "護理人員";}
			if("30".equals(PROFNO)){PROFNONAME = "專業人員";}
			if("31".equals(PROFNO)){PROFNONAME = "保全人員";}
			
			log.debug("PROFNONAME={}",PROFNONAME);
			TempBsData.put("PROFNONAME",PROFNONAME);
			
			if(requestParam.get("PHONE_03") == null){
				TempBsData.put("PHONE_03","");
			}
			if(requestParam.get("CHILDNO") == null){
				TempBsData.put("CHILDNO","");
			}
			if(requestParam.get("MONEY11") == null){
				TempBsData.put("MONEY11","");
			}
			if(requestParam.get("MONEY12") == null){
				TempBsData.put("MONEY12","");
			}
			if(requestParam.get("CKS1") == null){
				TempBsData.put("CKS1","");
			}
			if("#".equals(requestParam.get("RELTYPE1"))){
				TempBsData.put("RELTYPE1","");
			}
			if(requestParam.get("RELNAME1") == null){
				TempBsData.put("RELNAME1","");
			}
			if(requestParam.get("RELID1") == null){
				TempBsData.put("RELID1","");
			}
			if(requestParam.get("CKS2") == null){
				TempBsData.put("CKS2","");
			}
			if("#".equals(requestParam.get("RELTYPE2"))){
				TempBsData.put("RELTYPE2","");
			}
			if(requestParam.get("RELNAME2") == null){
				TempBsData.put("RELNAME2","");
			}
			if(requestParam.get("RELID2") == null){
				TempBsData.put("RELID2","");
			}
			if(requestParam.get("CKS3") == null){
				TempBsData.put("CKS3","");
			}
			if("#".equals(requestParam.get("RELTYPE3"))){
				TempBsData.put("RELTYPE3","");
			}
			if(requestParam.get("RELNAME3") == null){
				TempBsData.put("RELNAME3","");
			}
			if(requestParam.get("RELID3") == null){
				TempBsData.put("RELID3","");
			}
			if(requestParam.get("CKS4") == null){
				TempBsData.put("CKS4","");
			}
			if("#".equals(requestParam.get("RELTYPE4"))){
				TempBsData.put("RELTYPE4","");
			}
			if(requestParam.get("RELNAME4") == null){
				TempBsData.put("RELNAME4","");
			}
			if(requestParam.get("RELID4") == null){
				TempBsData.put("RELID4","");
			}
			if(requestParam.get("CKP1") == null){
				TempBsData.put("CKP1","");
			}
			if(requestParam.get("OTHCONM1") == null){
				TempBsData.put("OTHCONM1","");
			}
			if(requestParam.get("OTHCOID1") == null){
				TempBsData.put("OTHCOID1","");
			}
			if(requestParam.get("CKM1") == null){
				TempBsData.put("CKM1","");
			}
			if(requestParam.get("MOTHCONM1") == null){
				TempBsData.put("MOTHCONM1","");
			}
			if(requestParam.get("MOTHCOID1") == null){
				TempBsData.put("MOTHCOID1","");
			}
			if(requestParam.get("PRECARNAME") == null){
				TempBsData.put("PRECARNAME","");
			}
			if(requestParam.get("PRETKY") == null){
				TempBsData.put("PRETKY","");
			}
			if(requestParam.get("PRETKM") == null){
				TempBsData.put("PRETKM","");
			}
			if(requestParam.get("PHONE_43") == null){
				TempBsData.put("PHONE_43","");
			}
			bs.addAllData(requestParam);
			if(bs.getData()!=null)bs.setResult(Boolean.TRUE);
		}
		catch(Exception e){
			//e.printStackTrace();
			log.error("prepareApplyCreditloanP7Data ERROR>>",e);
		}
	}
	
	public String FormValidation(Map<String,String> requestParam,BaseResult bs,MultipartFile picFile1,MultipartFile picFile11,MultipartFile picFile2,MultipartFile picFile3) {
		String target="/error";
		//檢核圖示
		Map<String,Object> returnMap = new HashMap<String,Object>();
		//圖示允許的類型
		String[] allowImageType = {"image/jpeg","image/bmp","image/png","image/x-png","image/gif","image/jpg","image/pjpeg"};
		//圖示允許的長寬
		int allowImageWidth = 640;
		int allowImageHeight = 480;
		//圖示允許的檔案大小
		int allowImageSize = 3300 * 1024;//3300KB
		String contentType;
		String originalFilename;
		
		String errorMessage = "";
		
		//先把值KEEP起來
//		model.addAttribute("requestParam",reqParam);
		bs.addAllData(requestParam);
		Map<String,Object>TempBs=(Map<String, Object>) bs.getData();
		
		Date nowDate = new Date();
		String SEQNO = String.valueOf(txnLoanApplyDao.countLoanRecord(nowDate) + 1);
		if(SEQNO.length() < 2){
			SEQNO = "0" + SEQNO;
		}
		log.debug("SEQNO={}",SEQNO);
		
		String RCVNO = "LOAN" + new SimpleDateFormat("yyyyMMdd").format(nowDate) + SEQNO;
		log.debug("RCVNO={}",RCVNO);
		TempBs.put("RCVNO",RCVNO);
		
		TempBs.put("LASTDATE",new SimpleDateFormat("yyyyMMdd").format(nowDate));
		TempBs.put("LASTTIME",new SimpleDateFormat("HHmmss").format(nowDate));
		
		if(!picFile1.isEmpty()){
			contentType = picFile1.getContentType();
			log.debug(ESAPIUtil.vaildLog("picFile1 contentType >> " + contentType));
			originalFilename = picFile1.getOriginalFilename();
			log.debug(ESAPIUtil.vaildLog("picFile1 originalFilename >> " + originalFilename));
			returnMap = checkImage(picFile1,originalFilename,contentType,allowImageType,allowImageWidth,allowImageHeight,allowImageSize);
			requestParam.put("FILE1","1" + RCVNO + ".jpg");
		}
		//檢核不通過
		if("FALSE".equals(returnMap.get("result"))){
			errorMessage = (String)returnMap.get("message");
			log.debug(ESAPIUtil.vaildLog("errorMessage >> " + errorMessage));
			bs.addData("errorMessage","身分證正面圖檔" + errorMessage);	
			bs.setResult(Boolean.FALSE);
			target = "/apply_credit/apply_creditloan_p7";
			return target;
		}
		picFile1 = (MultipartFile)returnMap.get("resultImage");
		
		if(!picFile11.isEmpty()){
			contentType = picFile11.getContentType();
			log.debug(ESAPIUtil.vaildLog("picFile11 contentType >> " + contentType));
			originalFilename = picFile11.getOriginalFilename();
			log.debug(ESAPIUtil.vaildLog("picFile11 originalFilename >> " + originalFilename));
			returnMap = checkImage(picFile11,originalFilename,contentType,allowImageType,allowImageWidth,allowImageHeight,allowImageSize);
			requestParam.put("FILE11","11" + RCVNO + ".jpg");
		}
		//檢核不通過
		if("FALSE".equals(returnMap.get("result"))){
			errorMessage = (String)returnMap.get("message");
			log.debug(ESAPIUtil.vaildLog("errorMessage >> " + errorMessage));
			bs.addData("errorMessage","身分證反面圖檔" + errorMessage);	
			bs.setResult(Boolean.FALSE);
			target = "/apply_credit/apply_creditloan_p7";
			
			return target;
		}
		picFile11 = (MultipartFile)returnMap.get("resultImage");
		
		if(!picFile2.isEmpty()){
			contentType = picFile2.getContentType();
			log.debug(ESAPIUtil.vaildLog("picFile2 contentType >> " + contentType));
			originalFilename = picFile2.getOriginalFilename();
			log.debug(ESAPIUtil.vaildLog("picFile2 originalFilename >> " + originalFilename));
			returnMap = checkImage(picFile2,originalFilename,contentType,allowImageType,allowImageWidth,allowImageHeight,allowImageSize);
			requestParam.put("FILE2","2" + RCVNO + ".jpg");
		}
		//檢核不通過
		if("FALSE".equals(returnMap.get("result"))){
			errorMessage = (String)returnMap.get("message");
			log.debug(ESAPIUtil.vaildLog("errorMessage >> " + errorMessage));
			bs.addData("errorMessage","財力證明圖檔" + errorMessage);
			bs.setResult(Boolean.FALSE);
			target = "/apply_credit/apply_creditloan_p7";
			
			return target;
		}
		picFile2 = (MultipartFile)returnMap.get("resultImage");
		
		if(!picFile3.isEmpty()){
			contentType = picFile3.getContentType();
			log.debug(ESAPIUtil.vaildLog("picFile3 contentType >> " + contentType));
			originalFilename = picFile3.getOriginalFilename();
			log.debug(ESAPIUtil.vaildLog("picFile3 originalFilename >> " + originalFilename));
			returnMap = checkImage(picFile3,originalFilename,contentType,allowImageType,allowImageWidth,allowImageHeight,allowImageSize);
			requestParam.put("FILE3","3" + RCVNO + ".jpg");
		}
		//檢核不通過
		if("FALSE".equals(returnMap.get("result"))){
			errorMessage = (String)returnMap.get("message");
			log.debug(ESAPIUtil.vaildLog("errorMessage >> " + errorMessage));
			bs.addData("errorMessage","年資資料圖檔" + errorMessage);	
			bs.setResult(Boolean.FALSE);
			target = "/apply_credit/apply_creditloan_p7";	
			return target;
		}
		picFile3 = (MultipartFile)returnMap.get("resultImage");
		return target;
	}
	
	/**
	 * 準備前往申請小額信貸輸入頁P8的資料
	 */
	@SuppressWarnings("unchecked")
	public int prepareApplyCreditloanP8Data(Map<String,String> requestParam,MultipartFile picFile1,MultipartFile picFile11,MultipartFile picFile2,MultipartFile picFile3,BaseResult bs){
		BufferedReader bufferedReader = null;

		Map<String,Object> returnMap = new HashMap<String,Object>();
		//圖示允許的類型
		String[] allowImageType = {"image/jpeg","image/bmp","image/png","image/x-png","image/gif","image/jpg","image/pjpeg"};
		//圖示允許的長寬
		int allowImageWidth = 640;
		int allowImageHeight = 480;
		//圖示允許的檔案大小
		int allowImageSize = 3300 * 1024;//3300KB
		String contentType;
		String originalFilename;
		//圖片暫存位置
		String separ = File.separator;
		String uploadPath = context.getRealPath("") + separ + "com" + separ + "updateTmp" + separ;
		Date nowDate = new Date();
		String SEQNO = String.valueOf(txnLoanApplyDao.countLoanRecord(nowDate) + 1);
		TbbSc sc = null;
		if(SEQNO.length() < 2){
			SEQNO = "0" + SEQNO;
		}
		log.debug("SEQNO={}",SEQNO);
		String RCVNO = "LOAN" + new SimpleDateFormat("yyyyMMdd").format(nowDate) + SEQNO;
		log.debug("RCVNO={}",RCVNO);
		requestParam.put("LASTDATE",new SimpleDateFormat("yyyyMMdd").format(nowDate));
		requestParam.put("LASTTIME",new SimpleDateFormat("HHmmss").format(nowDate));
		requestParam.put("RCVNO",RCVNO);
		TXNLOANAPPLY attrib = new TXNLOANAPPLY();
		attrib = CodeUtil.objectCovert(TXNLOANAPPLY.class,requestParam);
		attrib.setAPPNAME(CodeUtil.convertFullorHalf(attrib.getAPPNAME(),1));
		attrib.setADDR1(CodeUtil.convertFullorHalf(attrib.getADDR1(),1));
		attrib.setADDR2(CodeUtil.convertFullorHalf(attrib.getADDR2(),1));
		attrib.setCAREERNAME(CodeUtil.convertFullorHalf(attrib.getCAREERNAME(),1));
		attrib.setADDR4(CodeUtil.convertFullorHalf(attrib.getADDR4(),1));
		attrib.setPRECARNAME(CodeUtil.convertFullorHalf(attrib.getPRECARNAME(),1));
		attrib.setRELNAME1(CodeUtil.convertFullorHalf(attrib.getRELNAME1(),1));
		attrib.setRELNAME2(CodeUtil.convertFullorHalf(attrib.getRELNAME2(),1));
		attrib.setRELNAME3(CodeUtil.convertFullorHalf(attrib.getRELNAME3(),1));
		attrib.setRELNAME4(CodeUtil.convertFullorHalf(attrib.getRELNAME4(),1));
		attrib.setOTHCONM1(CodeUtil.convertFullorHalf(attrib.getOTHCONM1(),1));
		attrib.setMOTHCONM1(CodeUtil.convertFullorHalf(attrib.getMOTHCONM1(),1));
		attrib.setZIP3("");
		attrib.setCITY3("");
		attrib.setZONE3("");
		attrib.setADDR3("");
		attrib.setLOGINTYPE("0");
		//STEP 1 DB SAVE
		try {
		txnLoanApplyDao.save(attrib);
		}catch(Exception e){
			//e.printStackTrace();
			log.error("prepareApplyCreditloanP8Data ERROR>>",e);
			return 1;//DB ERROR
		}
		//STEP 2 CONNECT
		try {
			sc = getConnect();
			int s;
			try {
				s = sc.getConnect();
			} catch (Exception e) {
				log.trace("connect_Exception >> " + e);
				s= -1;
			}	//當傳回1才可服務 01:00~03:30我們server會停下來
			log.info("connect_state >> " + s);
			if(s!=1) {
				throw new Exception();
			}
		}catch(Exception e){
			log.error("prepareApplyCreditloanP8Data ERROR>>",e);
			return 2;//connect ERROR
		}
		//STEP 3 UPLOADIMAGE
		int txtCnt = 0;
		if(picFile1.isEmpty())
			return 3;//圖片為空
		if(!UploadImage(picFile1,uploadPath,RCVNO+"_1.jpg"))
			return 4;//圖片上傳失敗
		txtCnt +=1;
		if(picFile11.isEmpty())
			return 3;//圖片為空
		if(!UploadImage(picFile11,uploadPath,RCVNO+"_2.jpg"))
			return 4;//圖片上傳失敗
		txtCnt +=1;
		if(picFile2.isEmpty())
			return 3;//圖片為空
		if(!UploadImage(picFile2,uploadPath,RCVNO+"_3.jpg"))
			return 4;//圖片上傳失敗
		txtCnt +=1;
		if(picFile3.isEmpty())
			return 3;//圖片為空
		if(!UploadImage(picFile3,uploadPath,RCVNO+"_4.jpg"))
			return 4;//圖片上傳失敗
		txtCnt +=1;
		
		//STEP 4  產TXT
		Map<String,String> newParamMap = new HashMap<String,String>();
		newParamMap = CodeUtil.objectCovert(newParamMap.getClass(),attrib);
        log.debug(ESAPIUtil.vaildLog("newParamMap >> " + newParamMap));
        newParamMap.putAll(requestParam);
        log.debug(ESAPIUtil.vaildLog("newParamMap >> " + newParamMap));
		//將所有newparamMap裡面的null值換成空值
		Set<String> set = newParamMap.keySet();
		Iterator<String> iterator = set.iterator();
		while(iterator.hasNext()){
				String key = iterator.next();
				log.debug(ESAPIUtil.vaildLog("key >> " + key));
				String value = newParamMap.get(key);
				log.debug(ESAPIUtil.vaildLog("value >> " + value));
				if(value == null){
					newParamMap.put(key,"");
				}
		}
		newParamMap.put("CNT",String.valueOf(txtCnt));
		String templatePath = "/uploadTemplate/MicroCreditLoans.txt";
		log.debug("templatePath={}",templatePath);
		Resource resource = new ClassPathResource(templatePath);
		try {
//		File PF = resource.getFile();
			if(resource.exists()){
				InputStream inputStream = new BufferedInputStream(resource.getInputStream());
				bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"MS950"));
				StringBuilder stringBuilder = new StringBuilder();
				String line;
				line = bufferedReader.readLine();
				while(line != null){
					log.debug(ESAPIUtil.vaildLog("line >> " + line)); 
					ESAPIUtil.validInput(line,"GeneralString",true);					
					String tagName = line.replace("<","").replace(">","");
					log.debug(ESAPIUtil.vaildLog("tagName >> " + tagName));
					String value = newParamMap.get(tagName);
					log.debug(ESAPIUtil.vaildLog("value >> " + value));					
					if(value != null && !"".equals(value)){
						line = line + value;
					}
					stringBuilder.append(line).append("\n");
					line = bufferedReader.readLine();
				}
				log.debug(ESAPIUtil.vaildLog("stringBuilder.toString()= " + stringBuilder.toString())); 

				String validatePath = ESAPIUtil.vaildPathTraversal2(uploadPath,RCVNO + ".txt");
				log.trace(validatePath);
				File pf = new SafeFile(validatePath);
				pf.setWritable(true, true);
				pf.setReadable(true, true);
				pf.setExecutable(true, true);
//				Path p = pf.toPath();
//				Files.write(p, stringBuilder.toString().getBytes("UTF-8"));

				BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(pf));
			    byte[] strToBytes = stringBuilder.toString().getBytes("MS950");
			    outputStream.write(strToBytes);
			 
			    outputStream.close();
			}
		}catch(Exception e){
			return 5;//產TXT失敗
		}
		//STEP 5 上傳
		String In[] = null;			
		try {
			In = sc.caseInEBNK(uploadPath, RCVNO + ".txt", RCVNO, RCVNO+"_1.jpg", RCVNO+"_2.jpg", RCVNO+"_3.jpg", RCVNO+"_4.jpg");
		} catch (Exception e) {
			log.error("upload error >>",e);
			return 6;//上傳失敗
		} //進件作業
		//STEP 6 查詢進件狀態
		List list = sc.doQuery(RCVNO);
		if(list!=null && list.size()>0){
			Map map = (Map) list.get(0);
			
			String scDate = sc.escapeNull(map.get("SCDATE"));
			String minDate = sc.escapeNull(map.get("MINDATE")); //核定30天內方可進行對保 scDate.compareTo(minDate)>=0
			int scCheck = scDate.compareTo(minDate);
			String scFlow = sc.escapeNull(map.get("SCFLOW"));
			String scRrst = sc.escapeNull(map.get("SCRRST"));

			//String scFlow = (String) map.get("SCFLOW");
			String bbi = sc.escapeNull(map.get("BBI")); //補件次數 >0 N次審核中
			String bbd = sc.escapeNull(map.get("BBD"));
			String bbc = sc.escapeNull(map.get("BBC"));

			
		//108.10.30 新增	START
			String custnm = sc.escapeNull(map.get("CUSTNM"));  //客戶名稱
			String rcvdate = sc.escapeNull(map.get("RCVDATE"));  //申請日期(網銀上傳日)
			String product = sc.escapeNull(map.get("PRODUCT")); //授信項目
			String appramt = sc.escapeNull(map.get("APPRAMT")); //核定金額(元)
			String limitm = sc.escapeNull(map.get("LIMITM")); //授信期限(月)
			String costrate = sc.escapeNull(map.get("COSTNOTE")); //系統作業費
			String ratenate = sc.escapeNull(map.get("RATENOTE")); //年利率
			String rtnway = sc.escapeNull(map.get("RTNWAY")); //償還方式
			String othnote = sc.escapeNull(map.get("OTHNOTE"));  //其他條件

			log.trace("客戶名稱="+custnm);
			log.trace("申請日期(網銀上傳日)="+rcvdate);
			log.trace("授信項目="+product);
			log.trace("核定金額(元)="+appramt);
			log.trace("授信期限(月)="+limitm);
			log.trace("系統作業費="+costrate);
			log.trace("年利率="+ratenate);
			log.trace("償還方式="+rtnway);
			log.trace("其他條件="+othnote);
		//108.10.30 新增	END				
			log.trace("minDate="+minDate);
			log.trace("scDate="+scDate); //結案日期 scFlow=9 才有值
			log.trace("scCheck="+scCheck);
			log.trace("scFlow="+scFlow); 
		/* 2-收到通知，9-結案(參考scRrst)
		0:進件登錄
		1:主管覆核
		2:待簽收
		3:待鍵機
		4:徵信作業
		5:授信作業
		6:初階主管作業
		7:放行主管作業
		8:AML資料審核
		9:結案
		C1 C2 C3:撤件
		*/
			log.trace("scRrst="+scRrst);
		/* 1-待補件 B-補件完成待確認
		* scFlow=9 下 scRrst=3 核准，其餘駁回(A or 4 or 13)
		0:進件中
		6:不予承作(分行駁回)
		1:待補件
		2:撤件申請中
		8:AML審查
		A:AML結案
		B:完成補件
		3:核准
		4:駁回
		13:未補件結案
		14:分行撤件
		*/
			log.trace("bbi="+bbi); //>0 二次審核中
			log.trace("bbd="+bbd); //最近一次要求補件日期
			log.trace("bbc="+bbc); //最近一次要求補件內容
		  //成功??
		   return 0;//success
		}else {
			return 7; //主機查不到
		}
		
		
	}
	/**
	 * 申請小額信貸，客戶基本資料查詢
	 */
	public BaseResult getN960Data(Map<String, String> reqParam){
		BaseResult bs = null;
		try{
			bs = N960_REST_UPDATE_CUSNAME(reqParam);
		}
		catch(Exception e){
			//e.printStackTrace();
			log.error("getN960Data ERROR>>",e);
		}
		return bs;
	}
	/**
	 * 申請小額信貸，取得分行資料
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String,String>> getBhContact(){
		List<Map<String,String>> resultListMap = null;
		try{
			List<ADMBHCONTACT> ADMBHCONTACTList = admBhContactDao.getAll();
			
			log.debug(ESAPIUtil.vaildLog("ADMBHCONTACTList="+ADMBHCONTACTList));
			
			resultListMap = new ArrayList<Map<String,String>>();
			
			for(ADMBHCONTACT po : ADMBHCONTACTList){
				Map<String,String> map = BeanUtils.describe(po);
				
				resultListMap.add(map);
			}
		}
		catch(Exception e){
			//e.printStackTrace();
			log.error("getBhContact ERROR>>",e);
		}
		
		return resultListMap;
	}
	/**
	 * 取得縣市別資料
	 */
	public List<Map<String,String>> getCityList(){
		List<Map<String,String>> cityList = admZipDataDao.findCity();
		
		log.debug(ESAPIUtil.vaildLog("cityList=" + cityList));
		
		return cityList;
	}
	/**
	 * 取得市區鄉鎮資料
	 */
	public List<Map<String,String>> getAreaList(String city){
		List<Map<String,String>> areaList = admZipDataDao.findArea(city);
		log.debug(ESAPIUtil.vaildLog("areaList >> " + CodeUtil.toJson(areaList)));
		return areaList;
	}
	/**
	 * 取得郵遞區號資料
	 */
	public String getZipCodeByCityArea(String city,String area){
		String zipCode = admZipDataDao.findZipBycityarea(city,area);
		log.debug(ESAPIUtil.vaildLog("zipCode >> " + zipCode));
		return zipCode;
	}
	/**
	 * 圖片檢核
	 * */
	public Map<String,Object> checkImage(MultipartFile file,String fileName,String fileContentType,String[] allowImageType,int allowImageWidth,int allowImageHeight,int allowImageSize){
		Map<String,Object> returnMap = new HashMap<String,Object>();
		returnMap.put("result","FALSE");
		returnMap.put("message","");
		try{
			//判斷格式
			for(int x=0;x<allowImageType.length;x++){
				if(allowImageType[x].equals(fileContentType)){
					break;
				}
				if(x == allowImageType.length - 1){
					returnMap.put("message",appendMessage((String)returnMap.get("message"),"圖示格式不符"));
				}
			}
			//判斷大小
			if(file.getSize() > allowImageSize){
				returnMap.put("message",appendMessage((String)returnMap.get("message"),"圖示檔案過大，不能超過" + allowImageSize / 1024 + "KB"));
			}
			//判斷檔名是否過長
			if(fileName.length() > 300){
				returnMap.put("message",appendMessage((String)returnMap.get("message"),"圖示檔名過長"));
			}
			//判斷副檔名是否過長
			if(FilenameUtils.getExtension(fileName).length() > 10){
				returnMap.put("message",appendMessage((String)returnMap.get("message"),"圖示副檔名過長"));
			}
			
			log.debug("allowImageWidth={}",allowImageWidth);
			log.debug("allowImageHeight={}",allowImageHeight);
			
			if(file.getSize() <= allowImageSize){
				BufferedImage bufferedImage = ImageIO.read(file.getInputStream());
				//檢查檔案尺寸，不合格式就幫使用者改變尺寸
				BufferedImage bufferedResizedImage = getScaledInstance(bufferedImage,640,480,RenderingHints.VALUE_INTERPOLATION_BICUBIC,true);
		
				int width = bufferedResizedImage.getWidth();
				int height = bufferedResizedImage.getHeight();
				
				log.debug("imageWidth={}",width);
				log.debug("imageHeight={}",height);
				
				ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
				ImageIO.write(bufferedImage,"jpg",byteArrayOutputStream);
				byteArrayOutputStream.flush();
	
				file = new MultipartImage(byteArrayOutputStream.toByteArray(),file.getName() + ".jpg",file.getOriginalFilename(),file.getContentType(),byteArrayOutputStream.size());
				
				returnMap.put("resultImage",file);
			}
			//檢核通過
			if("".equals(returnMap.get("message"))){
				returnMap.put("result","TRUE");
			}
		}
		catch(Exception e){
			log.error("",e);
			returnMap.put("message",appendMessage((String)returnMap.get("message"),String.valueOf(e.getMessage())));
		}
		return returnMap;
	}
	/**
	 * 調整上傳的圖片尺寸
	 */
	public BufferedImage getScaledInstance(BufferedImage image,Integer limitedWidth,Integer limitedHeight,Object hint,boolean higherQuality){
		int initialwidth;
		int initialheight;
		
		int targetWidth;
		int targetHeight;
		
		int finalWidth;
		int finalHeight;
		
		float rate;
		
		int type;
		BufferedImage returnImage = null;
		
		try{
			initialwidth = image.getWidth();
			initialheight = image.getHeight();
			type = (image.getTransparency() == Transparency.OPAQUE) ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
			returnImage = (BufferedImage)image;
			
			//檢查使用者上傳的圖檔寬、高符不符合格式
			
			//寬高都符合規定
			if(initialheight <= limitedHeight && initialwidth <= limitedWidth){
				targetHeight = initialheight;
				targetWidth = initialwidth;
			}
			//寬符合規定，高不符合
			else if(initialheight > limitedHeight && initialwidth <= limitedWidth){
				rate = (float)initialheight / limitedHeight;
				targetHeight = limitedHeight;
				targetWidth = (int)((float)initialwidth / rate);
			}
			//高符合規定，寬不符合
			else if(initialheight <= limitedHeight && initialwidth > limitedWidth){
				rate = (float)initialwidth / limitedWidth;
				targetHeight = (int)((float)initialheight / rate);
				targetWidth = limitedWidth;
			}
			//寬高都不符合規定
			else{
				//原圖高寬比=限制高寬比
				if((float)initialheight / initialwidth == (float)limitedHeight / limitedWidth){
					targetHeight = limitedHeight;
					targetWidth = limitedWidth;
				}
				//原圖高寬比>限制高寬比
				else if((float)initialheight / initialwidth > (float)limitedHeight / limitedWidth){
					rate = (float)initialheight / limitedHeight;
					targetHeight = limitedHeight;
					targetWidth = (int)((float)initialwidth / rate);
				}
				//原圖高寬比<限制高寬比
				else{
					rate = (float)initialwidth / limitedWidth;
					targetHeight = (int)((float)initialheight / rate);
					targetWidth = limitedWidth;
				}
			}
			
			//判斷是否需要高品質的縮圖
			if(higherQuality) {
				finalWidth = image.getWidth();
				finalHeight = image.getHeight();
			}
			else{
				finalWidth = targetWidth;
				finalHeight = targetHeight;
			}
			do{
				if(higherQuality && finalWidth > targetWidth){
					finalWidth /= 2;
					if(finalWidth < targetWidth){
						finalWidth = targetWidth;
					}
				}
				if(higherQuality && finalHeight > targetHeight){
					finalHeight /= 2;
					if(finalHeight < targetHeight){
						finalHeight = targetHeight;
					}
				}
				
				BufferedImage tmp = new BufferedImage(finalWidth,finalHeight,type);
				Graphics2D g2 = tmp.createGraphics();
				g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION,hint);
				g2.drawImage(returnImage,0,0,finalWidth,finalHeight,null);
				g2.dispose();
				returnImage = tmp;
			}
			while(finalWidth != targetWidth || finalHeight != targetHeight);
		}
		catch(Exception e){
			//e.printStackTrace();
			log.error("",e);
		}
		return returnImage;
	}
	/**
	 * 將message相加
	 */
	public String appendMessage(String message,String text){
		if("".equals(message)){
			message = text;
		}
		else{
			message = message + "，" + text;
		}
		return message;
	}
	
	/**
	 * 建立連線
	 * @throws Exception 
	 */
	@SuppressWarnings("unchecked")
	public TbbSc getConnect() throws Exception{
		String myIP = SpringBeanFactory.getBean("eloan_ip");
		String client = SpringBeanFactory.getBean("eloan_client");
		log.debug("ELOAN myIP>>{}",myIP);
		log.debug("ELOAN client>>{}",client);
		TbbSc sc = new TbbSc(myIP);
		sc.getClient(client);//T測試 D開發 P營運 
		return sc;
	}
	
	@SuppressWarnings("unchecked")
	public Boolean UploadImage(MultipartFile picFile,String rootPath,String fileName) {
		//圖示允許的檔案大小
		int allowImageSize = 3300 * 1024;//3300KB
		try {
			if(picFile.getSize() <= allowImageSize) {
				BufferedImage bufferedImage;
				bufferedImage = ImageIO.read(picFile.getInputStream());
				BufferedImage bufferedResizedImage = getScaledInstance(bufferedImage,640,480,RenderingHints.VALUE_INTERPOLATION_BICUBIC,true);
		//		OutputStream os = Files.newOutputStream(Paths.get(fileName), new OpenOption[] { StandardOpenOption.WRITE, StandardOpenOption.CREATE });
		
				String validatePath = ESAPIUtil.vaildPathTraversal2(rootPath,fileName);
				File outputfile = new SafeFile(validatePath);
				outputfile.setWritable(true, true);
				outputfile.setReadable(true, true);
				outputfile.setExecutable(true, true);
				Boolean result = ImageIO.write(bufferedResizedImage, "jpg", outputfile);
		//		os.flush();
		//		os.close();
			   return result;
			}
		}catch(Exception e){
			return false;
		}
		return false;
	}
	public class MultipartImage implements MultipartFile{
		private byte[] bytes;
		private InputStream inputStream;
		private String name;
		private String originalFilename;
		private String contentType;
		private boolean isEmpty;
		private long size;

		public MultipartImage(byte[] bytes,String name,String originalFilename,String contentType,long size){
		    this.bytes = bytes;
		    
		    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
		    this.inputStream = byteArrayInputStream;
		    
		    this.name = name;
		    this.originalFilename = originalFilename;
		    this.contentType = contentType;
		    this.size = size;
		    this.isEmpty = false;
		}
		@Override
		public String getName(){
		    return name;
		}
		@Override
		public String getOriginalFilename(){
		    return originalFilename;
		}
		@Override
		public String getContentType(){
		    return contentType;
		}
		@Override
		public boolean isEmpty(){
		    return isEmpty;
		}
		@Override
		public long getSize(){
		    return size;
		}
		@Override
		public byte[] getBytes() throws IOException{
		    return bytes;
		}
		@Override
		public InputStream getInputStream() throws IOException{
		    return inputStream;
		}
		@Override
		public void transferTo(File dest) throws IOException,IllegalStateException{
			
		}
	}
}