package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tw.com.fstop.util.NumericUtil;

public class N366_REST_RS extends BaseRestBean implements Serializable {
	
	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	private static final long serialVersionUID = -2137326113925896396L;
	private String MSGCOD;			// 訊息代號
	private String CUSIDN;			// 身份證號
	private String ACN;				// 欲結清帳號
	private String DPIBAL;			// 存款餘額
	private String DPIBALS;			// 存款餘額正負號 
	private String DPIINT;			// 存款息
	private String DPIINTS;			// 存款息正負號 
	private String ODFINT;			// 透支息
	private String ODFINTS;			// 透支息正負號
	private String AMTTAX;			// 存款息稅額
	private String AMTTAXS;			// 存款息稅額正負號
	private String AMTNHI;			// 二代健保費
	private String AMTNHIS;			// 二代健保費正負號
	private String AMTCLS;			// 結清淨額
	private String AMTCLSS;			// 結清淨額正負號
	
	
	/**
	 * 過濾正號
	 * 
	 * @param symbol
	 * @return
	 */
	private String filterPlus(String symbol)
	{
		String result = "-".equals(symbol) ?  "-" : "";
		return result;
	}
	
	/**
	 * 結合負號和餘額
	 * 
	 * @param sign
	 * @param balance
	 * @return
	 */
	private String combineSignBalance(String sign, String balance)
	{
		String result = filterPlus(sign) + balance;
		return result;
	}
	
	public String getMSGCOD() {
		return MSGCOD;
	}
	
	public void setMSGCOD(String mSGCOD) {
		MSGCOD = mSGCOD;
	}

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}

	public String getACN() {
		return ACN;
	}

	public void setACN(String aCN) {
		ACN = aCN;
	}

	public String getDPIBAL() {
		return DPIBAL;
	}
	
	/**
	 * 0000001000000 > 10,000.00，如果有負號 > -10,000.00
	 * @return
	 */
	public String getDPIBAL_F() 
	{
		String result = DPIBAL;
		try
		{
			BigDecimal b = new BigDecimal(new BigInteger(DPIBAL), 2);
			String bal = NumericUtil.fmtAmount(b.toPlainString(), 2);
			result = combineSignBalance(DPIBALS, bal);
		}
		catch (Exception e)
		{
			log.error("getDPIBAL_F error. param >> {}", DPIBAL, e);
		}
		return result;
	}

	public void setDPIBAL(String dPIBAL) {
		DPIBAL = dPIBAL;
	}

	public String getDPIBALS() {
		return DPIBALS;
	}

	public void setDPIBALS(String dPIBALS) {
		DPIBALS = dPIBALS;
	}

	public String getDPIINT() {
		return DPIINT;
	}

	/**
	 * 000003151 > 3,151，如果有負號 > -3,151
	 * @return
	 */
	public String getDPIINT_F() 
	{
		String result = DPIINT;
		try
		{
			BigDecimal b = new BigDecimal(DPIINT);
			String bal = NumericUtil.fmtAmount(b.toPlainString(), 0);
			result = combineSignBalance(DPIINTS, bal);
		}
		catch (Exception e)
		{
			log.error("getDPIINT_F error. param >> {}", DPIINT, e);
		}
		return result;
	}

	
	public void setDPIINT(String dPIINT) {
		DPIINT = dPIINT;
	}

	public String getDPIINTS() {
		return DPIINTS;
	}

	public void setDPIINTS(String dPIINTS) {
		DPIINTS = dPIINTS;
	}

	public String getODFINT() {
		return ODFINT;
	}
	
	/**
	 * 000000000 > 0，如果有負號 > -0
	 * @return
	 */
	public String getODFINT_F() 
	{
		String result = ODFINT;
		try
		{
			BigDecimal b = new BigDecimal(ODFINT);
			String bal = NumericUtil.fmtAmount(b.toPlainString(), 0);
			result = combineSignBalance(ODFINTS, bal);
		}
		catch (Exception e)
		{
			log.error("getODFINT_F error. param >> {}", ODFINT, e);
		}
		return result;
	}

	public void setODFINT(String oDFINT) {
		ODFINT = oDFINT;
	}

	public String getODFINTS() {
		return ODFINTS;
	}

	public void setODFINTS(String oDFINTS) {
		ODFINTS = oDFINTS;
	}

	public String getAMTTAX() {
		return AMTTAX;
	}
	
	/**
	 * 000000000 > 0，如果有負號 > -0
	 * @return
	 */
	public String getAMTTAX_F() 
	{
		String result = AMTTAX;
		try
		{
			BigDecimal b = new BigDecimal(AMTTAX);
			String bal = NumericUtil.fmtAmount(b.toPlainString(), 0);
			result = combineSignBalance(AMTTAXS, bal);
		}
		catch (Exception e)
		{
			log.error("getAMTTAX_F error. param >> {}", AMTTAX, e);
		}
		return result;
	}

	public void setAMTTAX(String aMTTAX) {
		AMTTAX = aMTTAX;
	}

	public String getAMTTAXS() {
		return AMTTAXS;
	}

	public void setAMTTAXS(String aMTTAXS) {
		AMTTAXS = aMTTAXS;
	}

	public String getAMTNHI() {
		return AMTNHI;
	}

	/**
	 * 000000000 > 0，如果有負號 > -0
	 * @return
	 */
	public String getAMTNHI_F() 
	{
		String result = AMTNHI;
		try
		{
			BigDecimal b = new BigDecimal(AMTNHI);
			String bal = NumericUtil.fmtAmount(b.toPlainString(), 0);
			result = combineSignBalance(AMTNHIS, bal);
		}
		catch (Exception e)
		{
			log.error("getAMTNHI_F error. param >> {}", AMTNHI, e);
		}
		return result;
	}
	
	public void setAMTNHI(String aMTNHI) {
		AMTNHI = aMTNHI;
	}

	public String getAMTNHIS() {
		return AMTNHIS;
	}

	public void setAMTNHIS(String aMTNHIS) {
		AMTNHIS = aMTNHIS;
	}

	public String getAMTCLS() {
		return AMTCLS;
	}
	
	/**
	 * 0000001315100 > 13,151.00，如果有負號 > -13,151.00
	 * @return
	 */
	public String getAMTCLS_F() 
	{
		String result = AMTCLS;
		try
		{
			BigDecimal b = new BigDecimal(new BigInteger(AMTCLS), 2);
			String bal = NumericUtil.fmtAmount(b.toPlainString(), 2);
			result = combineSignBalance(AMTCLSS, bal);
		}
		catch (Exception e)
		{
			log.error("getAMTCLS_F error. param >> {}", AMTCLS, e);
		}
		return result;
	}

	public void setAMTCLS(String aMTCLS) {
		AMTCLS = aMTCLS;
	}

	public String getAMTCLSS() {
		return AMTCLSS;
	}

	public void setAMTCLSS(String aMTCLSS) {
		AMTCLSS = aMTCLSS;
	}
	
}