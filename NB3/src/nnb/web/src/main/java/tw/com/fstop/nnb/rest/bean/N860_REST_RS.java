package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N860_REST_RS extends BaseRestBean implements Serializable {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 8179096943452336269L;
	private String VALCNT;						//
	private String REMCNT;						//託收票據總張
    private String REMAMT;						//託收票據總金額
    private String CMQTIME;						//查詢時間
    private String TYPE;						//查詢類別 
    private String TYPE_STR;						//查詢類別 
    private String VALAMT;						//
    private String CMPERIOD;					//查詢時間
	private LinkedList<N860_REST_RSDATA> REC;
	
	private String SEQ;							//序號
	private String ITEM;						//查詢項目
	private String ACN;							//帳號
	private String STADATE;						//開始日期
	private String ENDDATE;						//結束日期
	private String CNT;							//筆數
	private String CMRECNUM;					//查詢筆數
	private String USERDATA_X50;				//暫存空間區
	private String __OCCURS;
	private String QUERYNEXT;
	private String DONEACNOS;
	private String COLLBRH;
	
	
    public String getTYPE_STR() {
		return TYPE_STR;
	}
	public void setTYPE_STR(String tYPE_STR) {
		TYPE_STR = tYPE_STR;
	}
	public String getQUERYNEXT() {
		return QUERYNEXT;
	}
	public void setQUERYNEXT(String qUERYNEXT) {
		QUERYNEXT = qUERYNEXT;
	}
	public String getDONEACNOS() {
		return DONEACNOS;
	}
	public void setDONEACNOS(String dONEACNOS) {
		DONEACNOS = dONEACNOS;
	}
	public String getCOLLBRH() {
		return COLLBRH;
	}
	public void setCOLLBRH(String cOLLBRH) {
		COLLBRH = cOLLBRH;
	}
	public String getSEQ() {
		return SEQ;
	}
	public void setSEQ(String sEQ) {
		SEQ = sEQ;
	}
	public String getITEM() {
		return ITEM;
	}
	public void setITEM(String iTEM) {
		ITEM = iTEM;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getSTADATE() {
		return STADATE;
	}
	public void setSTADATE(String sTADATE) {
		STADATE = sTADATE;
	}
	public String getENDDATE() {
		return ENDDATE;
	}
	public void setENDDATE(String eNDDATE) {
		ENDDATE = eNDDATE;
	}
	public String getCNT() {
		return CNT;
	}
	public void setCNT(String cNT) {
		CNT = cNT;
	}
	public String getCMRECNUM() {
		return CMRECNUM;
	}
	public void setCMRECNUM(String cMRECNUM) {
		CMRECNUM = cMRECNUM;
	}
	public String getUSERDATA_X50() {
		return USERDATA_X50;
	}
	public void setUSERDATA_X50(String uSERDATA_X50) {
		USERDATA_X50 = uSERDATA_X50;
	}
	public String get__OCCURS() {
		return __OCCURS;
	}
	public void set__OCCURS(String __OCCURS) {
		this.__OCCURS = __OCCURS;
	}
	public String getVALCNT() {
		return VALCNT;
	}
	public void setVALCNT(String vALCNT) {
		VALCNT = vALCNT;
	}
	public String getREMCNT() {
		return REMCNT;
	}
	public void setREMCNT(String rEMCNT) {
		REMCNT = rEMCNT;
	}
	public String getREMAMT() {
		return REMAMT;
	}
	public void setREMAMT(String rEMAMT) {
		REMAMT = rEMAMT;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getTYPE() {
		return TYPE;
	}
	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}
	public String getVALAMT() {
		return VALAMT;
	}
	public void setVALAMT(String vALAMT) {
		VALAMT = vALAMT;
	}
	public String getCMPERIOD() {
		return CMPERIOD;
	}
	public void setCMPERIOD(String cMPERIOD) {
		CMPERIOD = cMPERIOD;
	}
	public LinkedList<N860_REST_RSDATA> getREC() {
		return REC;
	}
	public void setREC(LinkedList<N860_REST_RSDATA> rEC) {
		REC = rEC;
	}	
}
