package tw.com.fstop.nnb.spring.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.service.TxnGDRecord_Service;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;
import tw.com.fstop.util.CodeUtil;
@RestController
@RequestMapping(value = "/MB/TXNGDRECORD")
public class TxnGDRecord_Controller {

	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	TxnGDRecord_Service txngdrecord_Service;

	/*
	 * 查詢外幣帳結果
	 */
	@PostMapping(value = "/query", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody BaseResult query(HttpServletRequest request, HttpServletResponse response,
			@RequestBody Map<String, String> reqParam, Model model) {		
		log.info(ESAPIUtil.vaildLog("reqParam>>{}"+CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		try {
			Map<String,String> okmap = ESAPIUtil.validStrMap(reqParam);
			bs = txngdrecord_Service.query(okmap);			
		} catch (Throwable e) {
			log.error("Throwable>>{}", e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", "查詢失敗");
		}
		return bs;
	}
	/*
	 * 新增外幣轉帳結果
	 */
	@PostMapping(value = "/add", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody BaseResult add(HttpServletRequest request, HttpServletResponse response,
			@RequestBody Map<String, String> reqParam, Model model) {		
		log.info(ESAPIUtil.vaildLog("reqParam>>{}"+CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		try {
			Map<String,String> okmap = ESAPIUtil.validStrMap(reqParam);
			bs = txngdrecord_Service.add(okmap);		
		} catch (Throwable e) {
			log.error("Throwable>>{}", e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", "新增失敗");
		}
		return bs;
	}	
}
