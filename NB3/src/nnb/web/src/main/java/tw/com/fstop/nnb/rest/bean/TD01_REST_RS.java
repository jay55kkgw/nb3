package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

public class TD01_REST_RS extends BaseRestBean implements Serializable {

	private static final long serialVersionUID = 9018187800581094918L;
	/**
	 * 
	 */
	 String TOTCNT;
	 String CUSNAME;
	 String CYCLE;
	 
	 private LinkedList<TD01_REST_RSDATA> REC;

	public String getTOTCNT() {
		return TOTCNT;
	}

	public String getCUSNAME() {
		return CUSNAME;
	}

	public String getCYCLE() {
		return CYCLE;
	}


	public void setTOTCNT(String tOTCNT) {
		TOTCNT = tOTCNT;
	}

	public void setCUSNAME(String cUSNAME) {
		CUSNAME = cUSNAME;
	}

	public void setCYCLE(String cYCLE) {
		CYCLE = cYCLE;
	}

	public LinkedList<TD01_REST_RSDATA> getREC() {
		return REC;
	}

	public void setREC(LinkedList<TD01_REST_RSDATA> rEC) {
		REC = rEC;
	}
	
}

