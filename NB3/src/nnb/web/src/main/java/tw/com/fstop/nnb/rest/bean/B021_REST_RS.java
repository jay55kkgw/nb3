package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class B021_REST_RS extends BaseRestBean implements Serializable{
	

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4383671524541527975L;
	
	private String CMRECNUM;	//總筆數
	private String MSGTYPE;		//Message Type
	private String CUSIDN;		//統一編號
	private String EMAILMSG;	//Email重複訊息代號2組
	private String CMQTIME;		//查詢時間
	private String RESPCOD;		//Response Code
	
	private LinkedList<B021_REST_RSDATA> REC;
	
	public LinkedList<B021_REST_RSDATA> getREC() {
		return REC;
	}
	public void setREC(LinkedList<B021_REST_RSDATA> rEC) {
		REC = rEC;
	}
	public String getCMRECNUM() {
		return CMRECNUM;
	}
	public void setCMRECNUM(String cMRECNUM) {
		CMRECNUM = cMRECNUM;
	}
	public String getMSGTYPE() {
		return MSGTYPE;
	}
	public void setMSGTYPE(String mSGTYPE) {
		MSGTYPE = mSGTYPE;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getEMAILMSG() {
		return EMAILMSG;
	}
	public void setEMAILMSG(String eMAILMSG) {
		EMAILMSG = eMAILMSG;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getRESPCOD() {
		return RESPCOD;
	}
	public void setRESPCOD(String rESPCOD) {
		RESPCOD = rESPCOD;
	}
}
