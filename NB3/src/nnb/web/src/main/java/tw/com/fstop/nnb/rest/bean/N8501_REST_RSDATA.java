package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N8501_REST_RSDATA implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7679961151740217323L;
	
	private String AMTICHD;
	private String CLR;
	private String HEADER;
	private String TIME;
	private String ACN;
	private String BALANCE;
	private String DATE;
	private String STATUS;
	private String OFFSET;
	private String TOPMSG;
	private String __OCCURS;
	private String CUSIDN;
	public String getAMTICHD() {
		return AMTICHD;
	}
	public void setAMTICHD(String aMTICHD) {
		AMTICHD = aMTICHD;
	}
	public String getCLR() {
		return CLR;
	}
	public void setCLR(String cLR) {
		CLR = cLR;
	}
	public String getHEADER() {
		return HEADER;
	}
	public void setHEADER(String hEADER) {
		HEADER = hEADER;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getBALANCE() {
		return BALANCE;
	}
	public void setBALANCE(String bALANCE) {
		BALANCE = bALANCE;
	}
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getSTATUS() {
		return STATUS;
	}
	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}
	public String getOFFSET() {
		return OFFSET;
	}
	public void setOFFSET(String oFFSET) {
		OFFSET = oFFSET;
	}
	public String getTOPMSG() {
		return TOPMSG;
	}
	public void setTOPMSG(String tOPMSG) {
		TOPMSG = tOPMSG;
	}
	public String get__OCCURS() {
		return __OCCURS;
	}
	public void set__OCCURS(String __OCCURS) {
		this.__OCCURS = __OCCURS;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	
	
	
	
	
}
