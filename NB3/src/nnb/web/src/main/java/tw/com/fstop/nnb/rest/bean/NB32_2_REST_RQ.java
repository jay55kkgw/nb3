package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class NB32_2_REST_RQ  extends BaseRestBean_OLA implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2599345183918376113L;
	private String ACN;
	private String CUSIDN;
	private String ADOPID;
	private String FGTXWAY;
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	
}
