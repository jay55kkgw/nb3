package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N564_REST_RS extends BaseRestBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3961328299192394492L;
	
	private String CMQTIME;
	private String LENGTH;
	private String CMRECNUM;
	private String CMPERIOD;
	private LinkedList<N564_REST_RSDATA> REC;
	private LinkedList<N564_REST_RSDATA_CRY> CRY;
	
	public String getCMQTIME() {
		return CMQTIME;
	}
	public String getLENGTH() {
		return LENGTH;
	}
	public String getCMRECNUM() {
		return CMRECNUM;
	}
	public String getCMPERIOD() {
		return CMPERIOD;
	}
	public LinkedList<N564_REST_RSDATA> getREC() {
		return REC;
	}
	public LinkedList<N564_REST_RSDATA_CRY> getCRY() {
		return CRY;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public void setLENGTH(String lENGTH) {
		LENGTH = lENGTH;
	}
	public void setCMRECNUM(String cMRECNUM) {
		CMRECNUM = cMRECNUM;
	}
	public void setCMPERIOD(String cMPERIOD) {
		CMPERIOD = cMPERIOD;
	}
	public void setREC(LinkedList<N564_REST_RSDATA> rEC) {
		REC = rEC;
	}
	public void setCRY(LinkedList<N564_REST_RSDATA_CRY> cRY) {
		CRY = cRY;
	}

}
