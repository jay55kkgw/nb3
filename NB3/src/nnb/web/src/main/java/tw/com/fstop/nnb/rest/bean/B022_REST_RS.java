package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class B022_REST_RS  extends BaseRestBean implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4152166581071669117L;
	
	private String DATE;//回應日期(民國年)YYYMMDD
	private String TIME;//回應時間HHMMSS
	private String O01;//商品代號
	private String O02;//商品全名
	private String O03;//客戶投資屬性(1~4)
	private String O04;
	private String O05;//幣別
	private String O06;//委買面額
	private String O07;//委託買價
	private String O08;//前手息(+/-)
	private String O09;//前手息
	private String O10;//手續費率
	private String O11;//預估手續費
	
	private String CMQTIME;//交易時間
	
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getO01() {
		return O01;
	}
	public void setO01(String o01) {
		O01 = o01;
	}
	public String getO02() {
		return O02;
	}
	public void setO02(String o02) {
		O02 = o02;
	}
	public String getO03() {
		return O03;
	}
	public void setO03(String o03) {
		O03 = o03;
	}
	public String getO04() {
		return O04;
	}
	public void setO04(String o04) {
		O04 = o04;
	}
	public String getO05() {
		return O05;
	}
	public void setO05(String o05) {
		O05 = o05;
	}
	public String getO06() {
		return O06;
	}
	public void setO06(String o06) {
		O06 = o06;
	}
	public String getO07() {
		return O07;
	}
	public void setO07(String o07) {
		O07 = o07;
	}
	public String getO08() {
		return O08;
	}
	public void setO08(String o08) {
		O08 = o08;
	}
	public String getO09() {
		return O09;
	}
	public void setO09(String o09) {
		O09 = o09;
	}
	public String getO10() {
		return O10;
	}
	public void setO10(String o10) {
		O10 = o10;
	}
	public String getO11() {
		return O11;
	}
	public void setO11(String o11) {
		O11 = o11;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
}
