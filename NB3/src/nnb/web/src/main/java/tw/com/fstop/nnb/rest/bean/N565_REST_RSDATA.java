package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N565_REST_RSDATA implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9092591777823427161L;
	
	//附言
	String RMEMO1;
	String RMEMO2;
	String RMEMO3;
	String RMEMO4;
	
	String RREMITAM;	//匯入金額
	String RREMBREF; 	//匯款銀行參考號碼
	String RBANC;		//帳號
	String RORDNAME;	//匯款人
	String RRTNDATE;	//實際解款日期
	String FEXRATE;		//匯率
	String RORDBAD1;	//匯款銀行名稱
	String RVALDATE;	//結案日期
	String RREFNO;		//匯入匯款編號
	String PORDCUS1;	//匯款客戶名稱
	String RREMITCY;	//匯入幣別
	String RSTATE;		//狀態
	String CUSIDN;		//統一編號
	String RADATE;		//通知日
	String RORDBANK;	//匯款銀行SWIFT CODE
	String RCNADATE;	//退匯日期
	
	public String getRMEMO1() {
		return RMEMO1;
	}
	public void setRMEMO1(String rMEMO1) {
		RMEMO1 = rMEMO1;
	}
	public String getRMEMO2() {
		return RMEMO2;
	}
	public void setRMEMO2(String rMEMO2) {
		RMEMO2 = rMEMO2;
	}
	public String getRMEMO3() {
		return RMEMO3;
	}
	public void setRMEMO3(String rMEMO3) {
		RMEMO3 = rMEMO3;
	}
	public String getRMEMO4() {
		return RMEMO4;
	}
	public void setRMEMO4(String rMEMO4) {
		RMEMO4 = rMEMO4;
	}
	public String getRREMITAM() {
		return RREMITAM;
	}
	public void setRREMITAM(String rREMITAM) {
		RREMITAM = rREMITAM;
	}
	public String getRREMBREF() {
		return RREMBREF;
	}
	public void setRREMBREF(String rREMBREF) {
		RREMBREF = rREMBREF;
	}
	public String getRBANC() {
		return RBANC;
	}
	public void setRBANC(String rBANC) {
		RBANC = rBANC;
	}
	public String getRORDNAME() {
		return RORDNAME;
	}
	public void setRORDNAME(String rORDNAME) {
		RORDNAME = rORDNAME;
	}
	public String getRRTNDATE() {
		return RRTNDATE;
	}
	public void setRRTNDATE(String rRTNDATE) {
		RRTNDATE = rRTNDATE;
	}
	public String getFEXRATE() {
		return FEXRATE;
	}
	public void setFEXRATE(String fEXRATE) {
		FEXRATE = fEXRATE;
	}
	public String getRORDBAD1() {
		return RORDBAD1;
	}
	public void setRORDBAD1(String rORDBAD1) {
		RORDBAD1 = rORDBAD1;
	}
	public String getRVALDATE() {
		return RVALDATE;
	}
	public void setRVALDATE(String rVALDATE) {
		RVALDATE = rVALDATE;
	}
	public String getRREFNO() {
		return RREFNO;
	}
	public void setRREFNO(String rREFNO) {
		RREFNO = rREFNO;
	}
	public String getPORDCUS1() {
		return PORDCUS1;
	}
	public void setPORDCUS1(String pORDCUS1) {
		PORDCUS1 = pORDCUS1;
	}
	public String getRREMITCY() {
		return RREMITCY;
	}
	public void setRREMITCY(String rREMITCY) {
		RREMITCY = rREMITCY;
	}
	public String getRSTATE() {
		return RSTATE;
	}
	public void setRSTATE(String rSTATE) {
		RSTATE = rSTATE;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getRADATE() {
		return RADATE;
	}
	public void setRADATE(String rADATE) {
		RADATE = rADATE;
	}
	public String getRORDBANK() {
		return RORDBANK;
	}
	public void setRORDBANK(String rORDBANK) {
		RORDBANK = rORDBANK;
	}
	public String getRCNADATE() {
		return RCNADATE;
	}
	public void setRCNADATE(String rCNADATE) {
		RCNADATE = rCNADATE;
	}
	
}
