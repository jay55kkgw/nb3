package tw.com.fstop.nnb.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import com.google.gson.Gson;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.po.TXNCUSINVATTRHIST;
import fstop.orm.po.TXNCUSINVATTRIB;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.tbb.nnb.dao.TxnCusInvAttribDao;
import tw.com.fstop.tbb.nnb.dao.TxnCusInvAttrHistDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.StrUtil;

@Service
public class TxnCusInvAttrService {
	private Logger log = LoggerFactory.getLogger(this.getClass().getName());
	
	@Autowired
	private TxnCusInvAttrHistDao txnCusInvAttrHistDao;
	
	@Autowired
	private TxnCusInvAttribDao  txnCusInvAttrDao;
	
	
	/**
	 * 查詢歷史紀錄
	 * @param reqParam
	 * @return
	 */
	public BaseResult queryhis(Map<String,String> reqParam) {
		BaseResult bs = new BaseResult();
		try {
			String cusidn = reqParam.get("cusidn");			
			String lastdate = reqParam.get("lastdate");	
			if(StrUtil.isEmpty(cusidn)) {
				bs.setResult(Boolean.FALSE);
				bs.setMessage("1", "cusidn不可為空白");
				return bs;
			}
			
			if(!CodeUtil.isMatch(null, lastdate)) {
				bs.setResult(Boolean.FALSE);
				bs.setMessage("1", "日期格式不正確:yyyymmdd");
				return bs;
			}
			
			List<TXNCUSINVATTRHIST> list  = txnCusInvAttrHistDao.findUserKycCount(cusidn ,lastdate );
			bs.reset();			
			bs.setData(list);
			bs.setResult(Boolean.TRUE);
			bs.setMessage("0", "查詢成功");
		} catch (Exception e) {
			log.error("{}",e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", "查詢異常");
		}
		return bs;
	}
	
	
	
	public BaseResult query(Map<String,String> reqParam) {
		BaseResult bs = new BaseResult();
		try {
			String cusidn = reqParam.get("cusidn");			
			TXNCUSINVATTRIB po = txnCusInvAttrDao.get(TXNCUSINVATTRIB.class ,cusidn);
			log.info(ESAPIUtil.vaildLog("CusInvAttr >> {}"+ po));
			bs.reset();			
			bs.setData(po);
			bs.setResult(Boolean.TRUE);
			bs.setMessage("0", "查詢成功");
		} catch (Exception e) {
			log.error("{}",e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", "查詢異常");
		}
		return bs;
	}
	
	
	/**
	 * 把客戶資料更新或新增TXNCUSINVATTRIB
	 * 及新增一筆資料到TXNCUSINVATTRHIST(歷史紀錄)
	 */
	@Transactional
	public BaseResult saveData2InvAttrAndInvAttrHist(Map<String,String> reqParam) {
		reqParam = ESAPIUtil.validStrMap(reqParam);
		TXNCUSINVATTRIB po = new TXNCUSINVATTRIB();
		TXNCUSINVATTRHIST poHist = new TXNCUSINVATTRHIST();
		BaseResult bs = new BaseResult();
		try {
			log.trace(ESAPIUtil.vaildLog("reqParam>>{}"+CodeUtil.toJson(reqParam)));
			Map<String,String> map = reqParam.entrySet().parallelStream().collect(Collectors.toMap(entry -> entry.getKey().toUpperCase(), Map.Entry::getValue));
			log.info(ESAPIUtil.vaildLog("map>>{}"+CodeUtil.toJson(map)));
			String fd = map.get("FDHISTID");
			
			map.remove("FDHISTID");
			po = CodeUtil.objectCovert(TXNCUSINVATTRIB.class, map);
			poHist = CodeUtil.objectCovert(TXNCUSINVATTRHIST.class, map);
			map.put("FDHISTID", fd);
			poHist.setLASTDATE(new DateTime().toString("yyyyMMdd"));
			poHist.setLASTTIME(new DateTime().toString("HHmmss"));
			po.setLASTDATE(new DateTime().toString("yyyyMMdd"));
			po.setLASTTIME(new DateTime().toString("HHmmss"));
			log.debug(ESAPIUtil.vaildLog("TXNCUSINVATTRHIST>>{}"+CodeUtil.toJson(poHist.toString()))); 
			log.debug(ESAPIUtil.vaildLog("TXNCUSINVATTRIB>>{}"+CodeUtil.toJson(po.toString())));
			//user 是否同意投資屬性結果
			String AGREE = map.get("AGREE");

			if(StrUtil.isNotEmpty(fd)){
				poHist=txnCusInvAttrHistDao.get(TXNCUSINVATTRHIST.class, Long.valueOf(fd));
				poHist.setAGREE(AGREE);
			}
			txnCusInvAttrHistDao.saveOrUpdate(poHist);
			if("Y".equals(AGREE)){
				txnCusInvAttrHistDao.saveOrUpdate(po);
			}
			//是否確認
			String FDHISTID="";
			if(StrUtil.isEmpty(map.get("FDHISTID"))){
				List<TXNCUSINVATTRHIST> list = txnCusInvAttrHistDao.findByIDGetLastTimeData(poHist.getFDUSERID(), poHist.getLASTDATE(), poHist.getLASTTIME());
				if(list.size()>0){
					poHist = list.get(0);
					FDHISTID = new String().valueOf(poHist.getFDHISTID());
				}
				log.debug("new fdhistid="+FDHISTID);
			}else{
				FDHISTID = map.get("FDHISTID");
			}
			Gson gson = new Gson();
			Map reMap = new HashMap<String, String>();
			reMap.put("fdhistid", FDHISTID);
			
			bs.reset();
			bs.setData(reMap);
			bs.setResult(Boolean.TRUE);
			bs.setMessage("0", "修改成功");
		} catch (Exception e) {
			log.error("{}",e.getMessage(), e);
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", "修改異常");
		}
		return bs;
	}
}