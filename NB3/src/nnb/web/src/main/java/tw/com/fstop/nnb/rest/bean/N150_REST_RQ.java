package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * 輕鬆理財存摺明細查詢
 * 
 *
 */
public class N150_REST_RQ extends BaseRestBean_TW implements Serializable
{

	private static final long serialVersionUID = -1923766616001251442L;

	private String ACN; // 帳號

	private String CUSIDN; // 統一編號

	private String CMSDATE; // 開始日期

	private String CMEDATE; // 結束日期

	private String QUERYNEXT;// 繼續查詢用暫存空間區

	public String getACN()
	{
		return ACN;
	}

	public void setACN(String aCN)
	{
		ACN = aCN;
	}

	public String getCUSIDN()
	{
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN)
	{
		CUSIDN = cUSIDN;
	}

	public String getCMSDATE()
	{
		return CMSDATE;
	}

	public void setCMSDATE(String cMSDATE)
	{
		CMSDATE = cMSDATE;
	}

	public String getCMEDATE()
	{
		return CMEDATE;
	}

	public void setCMEDATE(String cMEDATE)
	{
		CMEDATE = cMEDATE;
	}

	public String getQUERYNEXT()
	{
		return QUERYNEXT;
	}

	public void setQUERYNEXT(String qUERYNEXT)
	{
		QUERYNEXT = qUERYNEXT;
	}

}
