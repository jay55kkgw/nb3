package tw.com.fstop.nnb.service;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;

import fstop.orm.po.ADMNBSTATUS;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.rest.bean.Monitor_Status;
import tw.com.fstop.tbb.nnb.dao.AdmNbStatusDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.web.util.ConfingManager;

@Service
public class Monitor_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private AdmNbStatusDao admnbstatusdao;

	@Transactional(value = "nnb_transactionManager")
	public List<ADMNBSTATUS> getAll() {
		List<ADMNBSTATUS> list_serverstatus = admnbstatusdao.getAll();
		for (ADMNBSTATUS po : list_serverstatus) {
			try {
				log.trace(ESAPIUtil.vaildLog("list_admbank: " + BeanUtils.describe(po)));

			} catch (Exception e) {
				// Avoid Information Exposure Through an Error Message
				// e.printStackTrace();
				log.error("getAll error >> {}", e);
			}
		}
		return list_serverstatus;
	}

	public BaseResult SingleChecking(Map<String, String> reqParam, BaseResult bs) {

		// initialize
		String channel = null;
		String serviceName = reqParam.get("service");
		BaseResult tempbs = new BaseResult();
		ADMNBSTATUS serviceBoottimeData = null;
		Monitor_Status service = new Monitor_Status();
		service.initialStatus();
		String NB3DBstatus = "not connect";
		bs.setResult(true);
		// selectchannel and check
		if (serviceName.equals("TMRA"))
			channel = SpringBeanFactory.getBean("tmra_uri");
		else if (serviceName.equals("MS_TW"))
			channel = ConfingManager.MS_TW;
		else if (serviceName.equals("MS_PS"))
			channel = ConfingManager.MS_PS;
		else if (serviceName.equals("MS_PAY"))
			channel = ConfingManager.MS_PAY;
		else if (serviceName.equals("MS_OLS"))
			channel = ConfingManager.MS_OLS;
		else if (serviceName.equals("MS_OLA"))
			channel = ConfingManager.MS_OLA;
		else if (serviceName.equals("MS_LOAN"))
			channel = ConfingManager.MS_LOAN;
		else if (serviceName.equals("MS_GOLD"))
			channel = ConfingManager.MS_GOLD;
		else if (serviceName.equals("MS_FX"))
			channel = ConfingManager.MS_FX;
		else if (serviceName.equals("MS_FUND"))
			channel = ConfingManager.MS_FUND;
		else if (serviceName.equals("MS_CC"))
			channel = ConfingManager.MS_CC;
		goCheck(service, channel, serviceName, tempbs);

		// get boottime from db
		try {
			if (serviceName.equals("TMRA"))
				serviceBoottimeData = admnbstatusdao.findByID("TMRA");
			else
				serviceBoottimeData = admnbstatusdao.findByID(serviceName.replace("MS_", ""));
			if (serviceBoottimeData != null) {
				String date = null;
				String time = null;
				String tmp_servertime = null;
				date = serviceBoottimeData.getLASTDATE();
				time = serviceBoottimeData.getLASTTIME();
				tmp_servertime = DateUtil.convertDate(2, (date + " " + time), "yyyyMMdd HHmmss", "yyyy/MM/dd HH:mm:ss");
				service.setBOOT_TIME(tmp_servertime);
			}
			NB3DBstatus = "true";
		} catch (Exception e) {
			log.error("monitor error DB connect >> {}", e);
		}
		// setdata
		bs.addData("turnDB", NB3DBstatus);
		bs.addData("Service_Status", service);
		log.trace("Monitor Service >>" + CodeUtil.toJson(bs));

		return bs;
	}

	/**
	 * call nd08 to check MobileCenter_status
	 * 
	 * @return true or false
	 */
	public BaseResult chkMobileCenter() {
		// initialize
		BaseResult bs = null;
		BaseResult bs_result = new BaseResult();
		String channel = null;

		Monitor_Status ms_tw = new Monitor_Status();
		Monitor_Status ms_ps = new Monitor_Status();
		Monitor_Status ms_pay = new Monitor_Status();
		Monitor_Status ms_ols = new Monitor_Status();
		Monitor_Status ms_ola = new Monitor_Status();
		Monitor_Status ms_loan = new Monitor_Status();
		Monitor_Status ms_gold = new Monitor_Status();
		Monitor_Status ms_fx = new Monitor_Status();
		Monitor_Status ms_fund = new Monitor_Status();
		Monitor_Status ms_cc = new Monitor_Status();
		Monitor_Status tmra = new Monitor_Status();
		tmra.initialStatus();
		String turnDB = "not connect";
		List<ADMNBSTATUS> list_serverstatus = null;

		initialALL(ms_tw, ms_ps, ms_pay, ms_ols, ms_ola, ms_loan, ms_gold, ms_fx, ms_fund, ms_cc);
		bs_result = new BaseResult();
		bs_result.setResult(true);

		// DO ms_service connection check
		/*
		 * goCheck(Monitor_Status service,String ms_Channel,String
		 * servicename,BaseResult bs);
		 */
		goCheck(ms_tw, ConfingManager.MS_TW, "MS_TW", bs);
		goCheck(ms_ps, ConfingManager.MS_PS, "MS_PS", bs);
		goCheck(ms_pay, ConfingManager.MS_PAY, "MS_PAY", bs);
		goCheck(ms_ols, ConfingManager.MS_OLS, "MS_OLS", bs);
		goCheck(ms_ola, ConfingManager.MS_OLA, "MS_OLA", bs);

		goCheck(ms_loan, ConfingManager.MS_LOAN, "MS_LOAN", bs);
		goCheck(ms_gold, ConfingManager.MS_GOLD, "MS_GOLD", bs);
		goCheck(ms_fx, ConfingManager.MS_FX, "MS_FX", bs);
		goCheck(ms_fund, ConfingManager.MS_FUND, "MS_FUND", bs);
		goCheck(ms_cc, ConfingManager.MS_CC, "MS_CC", bs);

		// NB3_DB連線狀態
		try {
			list_serverstatus = getAll();
			log.trace(ESAPIUtil.vaildLog("DB>>" + list_serverstatus));
			// 若能成功取回資料則為連線狀態
			if (list_serverstatus != null && !list_serverstatus.isEmpty()) {
				turnDB = "true";
			}
		} catch (Exception e) {
			log.error("monitor error DB connect >> {}", e);
		}
		// set ms_serveice boot time from DB
		try {
			if (list_serverstatus != null && !list_serverstatus.isEmpty()) {
				for (ADMNBSTATUS serverdata : list_serverstatus) {
					String date = null;
					String time = null;
					String tmp_servertime = null;
					date = serverdata.getLASTDATE();
					time = serverdata.getLASTTIME();
					tmp_servertime = DateUtil.convertDate(2, (date + " " + time), "yyyyMMdd HHmmss",
							"yyyy/MM/dd HH:mm:ss");
					if (serverdata.getADNBSTATUSID().trim().equals("TW"))
						ms_tw.setBOOT_TIME(tmp_servertime);
					else if (serverdata.getADNBSTATUSID().trim().equals("PS"))
						ms_ps.setBOOT_TIME(tmp_servertime);
					else if (serverdata.getADNBSTATUSID().trim().equals("PAY"))
						ms_pay.setBOOT_TIME(tmp_servertime);
					else if (serverdata.getADNBSTATUSID().trim().equals("OLS"))
						ms_ols.setBOOT_TIME(tmp_servertime);
					else if (serverdata.getADNBSTATUSID().trim().equals("OLA"))
						ms_ola.setBOOT_TIME(tmp_servertime);

					else if (serverdata.getADNBSTATUSID().trim().equals("LOAN"))
						ms_loan.setBOOT_TIME(tmp_servertime);
					else if (serverdata.getADNBSTATUSID().trim().equals("GOLD"))
						ms_gold.setBOOT_TIME(tmp_servertime);
					else if (serverdata.getADNBSTATUSID().trim().equals("FX"))
						ms_fx.setBOOT_TIME(tmp_servertime);
					else if (serverdata.getADNBSTATUSID().trim().equals("FUND"))
						ms_fund.setBOOT_TIME(tmp_servertime);
					else if (serverdata.getADNBSTATUSID().trim().equals("CC"))
						ms_cc.setBOOT_TIME(tmp_servertime);
					else if (serverdata.getADNBSTATUSID().trim().equals("TMRA"))
						tmra.setBOOT_TIME(tmp_servertime);
				}
			}
		} catch (Exception e) {
			log.error("monitor error DB connect >> {}", e);
		}
		// Setting value
		bs_result.addData("turnDB", turnDB);
		setBsdata(bs_result, ms_tw, ms_ps, ms_pay, ms_ols, ms_ola, ms_loan, ms_gold, ms_fx, ms_fund, ms_cc);
		bs_result.addData("TMRA_status", tmra);
		log.trace("Monitor Service >>" + CodeUtil.toJson(bs_result));
		return bs_result;
	}

	public void initialALL(Monitor_Status tw, Monitor_Status ps, Monitor_Status pay, Monitor_Status ols,
			Monitor_Status ola, Monitor_Status loan, Monitor_Status gold, Monitor_Status fx, Monitor_Status fund,
			Monitor_Status cc) {
		tw.initialStatus();
		ps.initialStatus();
		pay.initialStatus();
		ols.initialStatus();
		ola.initialStatus();
		loan.initialStatus();
		gold.initialStatus();
		fx.initialStatus();
		fund.initialStatus();
		cc.initialStatus();
	}

	public void goCheck(Monitor_Status service, String ms_Channel, String servicename, BaseResult bs) {
		try {
			String channel = null;
			// service健康狀態
			service.addLog("[[ " + servicename + " ]]");
			sendPost(getchannel(ms_Channel), service);
			// 電文和DB連線狀態
			if (service.getSERVER_STATUS().equals("healthy") && !servicename.equals("TMRA")) {
				// channel="http://192.168.50.191:8888/ms_tw/com/CallMobileCenter";
				channel = getchannel(ms_Channel) + "CallMobileCenter";
				log.trace(ESAPIUtil.vaildLog("channel: " + channel));
				bs = getMobileCenterStatus(channel);
				log.trace("BS >> " + CodeUtil.toJson(bs));
				try {
					if (((Map<String, String>) bs.getData()).get("TELconnect") != null) {
						String SERVICE_flag = ((Map<String, String>) bs.getData()).get("TELconnect");
						service.setTEL_STATUS(SERVICE_flag);
						service.addLog("		TelLog >>" + ((Map<String, String>) bs.getData()).get("TEL_LOG"));
					}
				}catch(Exception tmpe) {
					service.setTEL_STATUS("false");
					service.addLog("		TelLog >>" + tmpe.getMessage());
				}
				try {
					if (((Map<String, String>) bs.getData()).get("DBconnect") != null) {
						String SERVICE_DBconnect = ((Map<String, String>) bs.getData()).get("DBconnect");
						service.setDB_STATUS(SERVICE_DBconnect);
						service.addLog("		DbLog >>" + ((Map<String, String>) bs.getData()).get("DB_LOG"));
					}
				}catch(Exception tmpe) {
					service.setDB_STATUS("false");
					service.addLog("		DbLog >>" + tmpe.getMessage());
				}
			}
		} catch (Exception e) {
			log.error(ESAPIUtil.vaildLog("chkMobileCenter " + servicename + " connect error >> "+ e));
			service.addLog("NB3 ERROR:" + e);
		}

	}

	public void setBsdata(BaseResult bs, Monitor_Status tw, Monitor_Status ps, Monitor_Status pay, Monitor_Status ols,
			Monitor_Status ola, Monitor_Status loan, Monitor_Status gold, Monitor_Status fx, Monitor_Status fund,
			Monitor_Status cc) {
		bs.addData("TW_Status", tw);
		bs.addData("PS_Status", ps);
		bs.addData("PAY_Status", pay);
		bs.addData("OLS_Status", ols);
		bs.addData("OLA_Status", ola);
		bs.addData("LOAN_Status", loan);
		bs.addData("GOLD_Status", gold);
		bs.addData("FX_Status", fx);
		bs.addData("FUND_Status", fund);
		bs.addData("CC_Status", cc);
	}

	public String getchannel(String ms_Channel) {

		if (ms_Channel.endsWith("/") == false) {
			ms_Channel = ms_Channel + "/";
		}
		return ms_Channel;
	}

	// HTTP POST请求
	private void sendPost(String channel, Monitor_Status server) throws Exception {
		log.trace("channel >>" + channel);
		String urlParameters = "[]";
		byte[] postData = urlParameters.getBytes();
		int postDataLength = postData.length;
		/*
		 * http://192.168.50.191:8888/ms_tw/com/ to
		 * http://192.168.50.191:8888/ms_tw/batch/com/echo
		 */
		String checkurl = channel.replace("/com/", "/batch/com/echo");
		StringBuilder sb = new StringBuilder();
		try {
			URL obj = new URL(checkurl);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
			con.setRequestProperty("Content-Length", Integer.toString(postDataLength));
			con.setDoOutput(true);
			con.setConnectTimeout(5 * 1000);
			con.setReadTimeout(60 * 1000);
			con.setDoInput(true);

			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.write(postData);
			wr.flush();
			wr.close();

			int responseCode = con.getResponseCode();

			log.debug("\nSending 'POST' request to URL : " + checkurl);
			log.debug("Response Code : " + responseCode);

			BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));

			String line;

			while ((line = br.readLine()) != null) {
				sb.append(line + "\n");
			}

			br.close();
			Gson gson = new Gson();
			log.debug("json>>>>>" + sb.toString());
			Map map = gson.fromJson(sb.toString(), Map.class);
			log.debug("gson>>>>>" + CodeUtil.toJson(map));
			/*
			 * "batchName": "echo", "isSuccess": true
			 */
			if ((boolean) map.get("isSuccess")) {
				server.setSERVER_STATUS("healthy");
			} else {
				server.setSERVER_STATUS("dead");
			}
		} catch (Exception e) {
			server.addLog("SERVER STATUS >>" + e);
		}
	}
	
	/**
	 * 確認資料庫連線狀態
	 * @param cusidn 統編/身分證
	 * 
	 * @return
	 */
	public boolean testDB() {
		boolean result = false;
		try {
			result = admnbstatusdao.getTest();
			
		} catch (Exception e) {
			log.error("",e);
		}
		return result;
		
	}
}
