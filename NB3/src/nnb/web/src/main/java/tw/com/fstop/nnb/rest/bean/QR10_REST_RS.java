package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * 
 * @author Ian
 *
 */
public class QR10_REST_RS extends BaseRestBean_CC implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6339402081074818914L;

	private String FEE;// 手續費

	private String CMQTIME;//

	/// 跨行回傳參數
	private String VrfyMacRslt;//

	private String TrnsCode;//

	private String Rcode;//

	private String SessionId;//

	public String getFEE()
	{
		return FEE;
	}

	public void setFEE(String fEE)
	{
		FEE = fEE;
	}

	public String getCMQTIME()
	{
		return CMQTIME;
	}

	public void setCMQTIME(String cMQTIME)
	{
		CMQTIME = cMQTIME;
	}

	public String getVrfyMacRslt()
	{
		return VrfyMacRslt;
	}

	public void setVrfyMacRslt(String vrfyMacRslt)
	{
		VrfyMacRslt = vrfyMacRslt;
	}

	public String getTrnsCode()
	{
		return TrnsCode;
	}

	public void setTrnsCode(String trnsCode)
	{
		TrnsCode = trnsCode;
	}

	public String getRcode()
	{
		return Rcode;
	}

	public void setRcode(String rcode)
	{
		Rcode = rcode;
	}

	public String getSessionId()
	{
		return SessionId;
	}

	public void setSessionId(String sessionId)
	{
		SessionId = sessionId;
	}

}
