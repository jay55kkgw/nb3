package tw.com.fstop.nnb.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import fstop.orm.po.ADMCURRENCY;
import fstop.orm.po.TXNBONDDATA;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.AdmCurrencyDao;
import tw.com.fstop.tbb.nnb.dao.AdmHolidayDao;
import tw.com.fstop.tbb.nnb.dao.SysParamDataDao;
import tw.com.fstop.tbb.nnb.dao.TxnBondDataDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;
import tw.com.fstop.tbb.nnb.util.StrUtils;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.WebUtil;

/**
 * 海外債券特別約定事項同意書 Service
 */
@Service
public class Bond_Deal_Service extends Base_Service{
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	
	
	@Autowired
	private I18n i18n;
	
}
