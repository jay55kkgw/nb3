package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class ELOAN_REST_RQ extends BaseRestBean_LOAN implements Serializable {

	private static final long serialVersionUID = 8859733676256041186L;
	
	private String CUSIDN;
	
	private String RCVNO;
	
	private String FNAME;
	
	private String CPRIMBIRTHDAY;

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}

	public String getRCVNO() {
		return RCVNO;
	}

	public void setRCVNO(String rCVNO) {
		RCVNO = rCVNO;
	}

	public String getFNAME() {
		return FNAME;
	}

	public void setFNAME(String fNAME) {
		FNAME = fNAME;
	}

	public String getCPRIMBIRTHDAY() {
		return CPRIMBIRTHDAY;
	}

	public void setCPRIMBIRTHDAY(String cPRIMBIRTHDAY) {
		CPRIMBIRTHDAY = cPRIMBIRTHDAY;
	}

	

}
