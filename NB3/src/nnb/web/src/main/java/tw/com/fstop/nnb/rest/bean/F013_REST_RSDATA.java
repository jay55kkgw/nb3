package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * 
 * 功能說明 :一般網銀外幣匯出匯款受款人約定檔擷取
 *
 */
public class F013_REST_RSDATA implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4731137087876579715L;

	private String FORMAT_NAME;// FORMAT-NAME

	private String PGMID;// 程式名稱

	private String ETY_LVL;// ENTRY LEVEL

	private String BRH_CODE;// 分行代碼 x(4)

	private String HOST_TIME;// HOST時間HHMMSS

	private String STATUS;// 狀態

	private String DEPT;// 業務別

	private String CUSTID;// 客戶統編

	private String REFNO;// 匯入匯款編號

	private String TXDATE;// 建檔日期

	private String VALDATE;// 生效日

	private String TXCCY;// 匯入幣別

	private String TXAMT;// 匯入金額

	private String RETCCY;// 解款幣別

	private String BENACC;// 解款帳號

	private String TXBRH;// 承作行代碼

	private String ACBRH;// 實績行代碼

	private String ORDNAME;// 匯款人名稱

	private String CNTRY;// 國別

	private String ORDAD1;// 匯款人住址1

	private String ORDAD2;// 匯款人住址2

	private String ORDAD3;

	private String BENAD1;

	private String BENAD2;

	private String BENAD3;

	private String RCVBANK;

	private String RCVAD1;// 地址

	private String RCVAD2;

	private String RCVAD3;

	private String DETCHG;

	private String ORDMEMO1;

	private String ORDMEMO2;

	private String ORDMEMO3;

	private String ORDMEMO4;

	private String ORDACC;

	private String BENNAME;

	public String getFORMAT_NAME()
	{
		return FORMAT_NAME;
	}

	public void setFORMAT_NAME(String fORMAT_NAME)
	{
		FORMAT_NAME = fORMAT_NAME;
	}

	public String getPGMID()
	{
		return PGMID;
	}

	public void setPGMID(String pGMID)
	{
		PGMID = pGMID;
	}

	public String getETY_LVL()
	{
		return ETY_LVL;
	}

	public void setETY_LVL(String eTY_LVL)
	{
		ETY_LVL = eTY_LVL;
	}

	public String getBRH_CODE()
	{
		return BRH_CODE;
	}

	public void setBRH_CODE(String bRH_CODE)
	{
		BRH_CODE = bRH_CODE;
	}

	public String getHOST_TIME()
	{
		return HOST_TIME;
	}

	public void setHOST_TIME(String hOST_TIME)
	{
		HOST_TIME = hOST_TIME;
	}

	public String getSTATUS()
	{
		return STATUS;
	}

	public void setSTATUS(String sTATUS)
	{
		STATUS = sTATUS;
	}

	public String getDEPT()
	{
		return DEPT;
	}

	public void setDEPT(String dEPT)
	{
		DEPT = dEPT;
	}

	public String getCUSTID()
	{
		return CUSTID;
	}

	public void setCUSTID(String cUSTID)
	{
		CUSTID = cUSTID;
	}

	public String getREFNO()
	{
		return REFNO;
	}

	public void setREFNO(String rEFNO)
	{
		REFNO = rEFNO;
	}

	public String getTXDATE()
	{
		return TXDATE;
	}

	public void setTXDATE(String tXDATE)
	{
		TXDATE = tXDATE;
	}

	public String getVALDATE()
	{
		return VALDATE;
	}

	public void setVALDATE(String vALDATE)
	{
		VALDATE = vALDATE;
	}

	public String getTXCCY()
	{
		return TXCCY;
	}

	public void setTXCCY(String tXCCY)
	{
		TXCCY = tXCCY;
	}

	public String getTXAMT()
	{
		return TXAMT;
	}

	public void setTXAMT(String tXAMT)
	{
		TXAMT = tXAMT;
	}

	public String getRETCCY()
	{
		return RETCCY;
	}

	public void setRETCCY(String rETCCY)
	{
		RETCCY = rETCCY;
	}

	public String getBENACC()
	{
		return BENACC;
	}

	public void setBENACC(String bENACC)
	{
		BENACC = bENACC;
	}

	public String getTXBRH()
	{
		return TXBRH;
	}

	public void setTXBRH(String tXBRH)
	{
		TXBRH = tXBRH;
	}

	public String getACBRH()
	{
		return ACBRH;
	}

	public void setACBRH(String aCBRH)
	{
		ACBRH = aCBRH;
	}

	public String getORDNAME()
	{
		return ORDNAME;
	}

	public void setORDNAME(String oRDNAME)
	{
		ORDNAME = oRDNAME;
	}

	public String getCNTRY()
	{
		return CNTRY;
	}

	public void setCNTRY(String cNTRY)
	{
		CNTRY = cNTRY;
	}

	public String getORDAD1()
	{
		return ORDAD1;
	}

	public void setORDAD1(String oRDAD1)
	{
		ORDAD1 = oRDAD1;
	}

	public String getORDAD2()
	{
		return ORDAD2;
	}

	public void setORDAD2(String oRDAD2)
	{
		ORDAD2 = oRDAD2;
	}

	public String getORDAD3()
	{
		return ORDAD3;
	}

	public void setORDAD3(String oRDAD3)
	{
		ORDAD3 = oRDAD3;
	}

	public String getBENAD1()
	{
		return BENAD1;
	}

	public void setBENAD1(String bENAD1)
	{
		BENAD1 = bENAD1;
	}

	public String getBENAD2()
	{
		return BENAD2;
	}

	public void setBENAD2(String bENAD2)
	{
		BENAD2 = bENAD2;
	}

	public String getBENAD3()
	{
		return BENAD3;
	}

	public void setBENAD3(String bENAD3)
	{
		BENAD3 = bENAD3;
	}

	public String getRCVBANK()
	{
		return RCVBANK;
	}

	public void setRCVBANK(String rCVBANK)
	{
		RCVBANK = rCVBANK;
	}

	public String getRCVAD1()
	{
		return RCVAD1;
	}

	public void setRCVAD1(String rCVAD1)
	{
		RCVAD1 = rCVAD1;
	}

	public String getRCVAD2()
	{
		return RCVAD2;
	}

	public void setRCVAD2(String rCVAD2)
	{
		RCVAD2 = rCVAD2;
	}

	public String getRCVAD3()
	{
		return RCVAD3;
	}

	public void setRCVAD3(String rCVAD3)
	{
		RCVAD3 = rCVAD3;
	}

	public String getDETCHG()
	{
		return DETCHG;
	}

	public void setDETCHG(String dETCHG)
	{
		DETCHG = dETCHG;
	}

	public String getORDMEMO1()
	{
		return ORDMEMO1;
	}

	public void setORDMEMO1(String oRDMEMO1)
	{
		ORDMEMO1 = oRDMEMO1;
	}

	public String getORDMEMO2()
	{
		return ORDMEMO2;
	}

	public void setORDMEMO2(String oRDMEMO2)
	{
		ORDMEMO2 = oRDMEMO2;
	}

	public String getORDMEMO3()
	{
		return ORDMEMO3;
	}

	public void setORDMEMO3(String oRDMEMO3)
	{
		ORDMEMO3 = oRDMEMO3;
	}

	public String getORDMEMO4()
	{
		return ORDMEMO4;
	}

	public void setORDMEMO4(String oRDMEMO4)
	{
		ORDMEMO4 = oRDMEMO4;
	}

	public String getORDACC()
	{
		return ORDACC;
	}

	public void setORDACC(String oRDACC)
	{
		ORDACC = oRDACC;
	}

	public String getBENNAME()
	{
		return BENNAME;
	}

	public void setBENNAME(String bENNAME)
	{
		BENNAME = bENNAME;
	}


}
