package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * N374電文RS
 */
public class N374_REST_RS extends BaseRestBean implements Serializable{
	private static final long serialVersionUID = 6791228050721094802L;
	
	private String DATE;
	private String TIME;
	private String CUSIDN;
	private String CUSNAME;
	private String TRANSCODE;
	private String CDNO;
	private String RESTOREDAY;
	private String BILLSENDMODE;
	private String UNIT;
	private String FUNDACN;
	private String BANKID;
	private String BANKNAME;
	private String FUNDAMT;
	private String FUNDCUR;
	private String SSLTXNO;
	private String SHORTTRADE;
	private String SHORTTUNIT;
	private String STOPFLAG;
	private String TRNDATE;
	private String TRNTIME;
	private String TYPE;
	private String TRADEDATE;
	private String CMQTIME;
	
	
	public String getDATE(){
		return DATE;
	}
	public void setDATE(String dATE){
		DATE = dATE;
	}
	public String getTIME(){
		return TIME;
	}
	public void setTIME(String tIME){
		TIME = tIME;
	}
	public String getCUSIDN(){
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN){
		CUSIDN = cUSIDN;
	}
	
	public String getCUSNAME(){
		return CUSNAME;
	}
	public void setCUSNAME(String cUSNAME){
		CUSNAME = cUSNAME;
	}
	public String getTRNDATE(){
		return TRNDATE;
	}
	public void setTRNDATE(String tRNDATE){
		TRNDATE = tRNDATE;
	}
	public String getTRNTIME(){
		return TRNTIME;
	}
	public void setTRNTIME(String tRNTIME){
		TRNTIME = tRNTIME;
	}
	public String getTYPE(){
		return TYPE;
	}
	public void setTYPE(String tYPE){
		TYPE = tYPE;
	}
	public String getTRANSCODE(){
		return TRANSCODE;
	}
	public void setTRANSCODE(String tRANSCODE){
		TRANSCODE = tRANSCODE;
	}
	public String getRESTOREDAY(){
		return RESTOREDAY;
	}
	public void setRESTOREDAY(String rESTOREDAY){
		RESTOREDAY = rESTOREDAY;
	}
	public String getTRADEDATE(){
		return TRADEDATE;
	}
	public void setTRADEDATE(String tRADEDATE){
		TRADEDATE = tRADEDATE;
	}
	public String getUNIT(){
		return UNIT;
	}
	public void setUNIT(String uNIT){
		UNIT = uNIT;
	}
	public String getFUNDACN(){
		return FUNDACN;
	}
	public void setFUNDACN(String fUNDACN){
		FUNDACN = fUNDACN;
	}
	public String getBANKID(){
		return BANKID;
	}
	public void setBANKID(String bANKID){
		BANKID = bANKID;
	}
	public String getBANKNAME(){
		return BANKNAME;
	}
	public void setBANKNAME(String bANKNAME){
		BANKNAME = bANKNAME;
	}
	public String getFUNDCUR(){
		return FUNDCUR;
	}
	public void setFUNDCUR(String fUNDCUR){
		FUNDCUR = fUNDCUR;
	}
	public String getSHORTTRADE(){
		return SHORTTRADE;
	}
	public void setSHORTTRADE(String sHORTTRADE){
		SHORTTRADE = sHORTTRADE;
	}
	public String getSHORTTUNIT(){
		return SHORTTUNIT;
	}
	public void setSHORTTUNIT(String sHORTTUNIT){
		SHORTTUNIT = sHORTTUNIT;
	}
	public String getSTOPFLAG(){
		return STOPFLAG;
	}
	public void setSTOPFLAG(String sTOPFLAG){
		STOPFLAG = sTOPFLAG;
	}
	public String getBILLSENDMODE(){
		return BILLSENDMODE;
	}
	public void setBILLSENDMODE(String bILLSENDMODE){
		BILLSENDMODE = bILLSENDMODE;
	}
	public String getSSLTXNO(){
		return SSLTXNO;
	}
	public void setSSLTXNO(String sSLTXNO){
		SSLTXNO = sSLTXNO;
	}
	public String getFUNDAMT(){
		return FUNDAMT;
	}
	public void setFUNDAMT(String fUNDAMT){
		FUNDAMT = fUNDAMT;
	}
	public String getCMQTIME(){
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME){
		CMQTIME = cMQTIME;
	}
	public String getCDNO() {
		return CDNO;
	}
	public void setCDNO(String cDNO) {
		CDNO = cDNO;
	}
}