package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class NB32_1_REST_RQ extends BaseRestBean_OLA implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5049179417317026856L;
	private String CUSIDN;
	private String ACN;
	private String BRHCOD;
	private String FGTXWAY;
	private String UID;
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getBRHCOD() {
		return BRHCOD;
	}
	public void setBRHCOD(String bRHCOD) {
		BRHCOD = bRHCOD;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}

}
