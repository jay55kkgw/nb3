package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class B106_REST_RS extends BaseRestBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5074582395210043776L;

	private String ADLEADBYTE;
	private String ADMAILDOMAIN;
	private String CMQTIME;
	private String KYCDATE;
	
	public String getADLEADBYTE() {
		return ADLEADBYTE;
	}
	public void setADLEADBYTE(String aDLEADBYTE) {
		ADLEADBYTE = aDLEADBYTE;
	}
	public String getADMAILDOMAIN() {
		return ADMAILDOMAIN;
	}
	public void setADMAILDOMAIN(String aDMAILDOMAIN) {
		ADMAILDOMAIN = aDMAILDOMAIN;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getKYCDATE() {
		return KYCDATE;
	}
	public void setKYCDATE(String kYCDATE) {
		KYCDATE = kYCDATE;
	}
}
