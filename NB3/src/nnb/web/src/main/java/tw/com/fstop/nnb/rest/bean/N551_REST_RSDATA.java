package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N551_REST_RSDATA implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6584902266710328709L;

	String RORIGAMT;	//開狀金額
	String RMARK;		//備註
	String REXPDATE;	//信用狀到期日
	String RLCOS;		//信用狀餘額
	String ROPENDAT;	//開狀日期
	String RLCCCY;		//幣別
	String RLCNO;		//信用狀號碼
	String RBENNAME;	//受益人
	String RSHPDATE;	//最後裝船日
	
	public String getRORIGAMT() {
		return RORIGAMT;
	}
	public void setRORIGAMT(String rORIGAMT) {
		RORIGAMT = rORIGAMT;
	}
	public String getRMARK() {
		return RMARK;
	}
	public void setRMARK(String rMARK) {
		RMARK = rMARK;
	}
	public String getREXPDATE() {
		return REXPDATE;
	}
	public void setREXPDATE(String rEXPDATE) {
		REXPDATE = rEXPDATE;
	}
	public String getRLCOS() {
		return RLCOS;
	}
	public void setRLCOS(String rLCOS) {
		RLCOS = rLCOS;
	}
	public String getROPENDAT() {
		return ROPENDAT;
	}
	public void setROPENDAT(String rOPENDAT) {
		ROPENDAT = rOPENDAT;
	}
	public String getRLCCCY() {
		return RLCCCY;
	}
	public void setRLCCCY(String rLCCCY) {
		RLCCCY = rLCCCY;
	}
	public String getRLCNO() {
		return RLCNO;
	}
	public void setRLCNO(String rLCNO) {
		RLCNO = rLCNO;
	}
	public String getRBENNAME() {
		return RBENNAME;
	}
	public void setRBENNAME(String rBENNAME) {
		RBENNAME = rBENNAME;
	}
	public String getRSHPDATE() {
		return RSHPDATE;
	}
	public void setRSHPDATE(String rSHPDATE) {
		RSHPDATE = rSHPDATE;
	}
	
}
