package tw.com.fstop.web.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Enumeration;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tw.com.fstop.nnb.service.FindFileChange;
import tw.com.fstop.nnb.service.Login_out_Service;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.util.StrUtils;
import tw.com.fstop.web.util.WebUtil;

public class RoleAuthFilter implements Filter {

	Logger log = LoggerFactory.getLogger(this.getClass());
	
	private FilterConfig filterConfig;
	
	// 忽略網址的列表
	private ArrayList<String> urlList;
	
	private Login_out_Service login_out_service;
	
    @Override
    public void init(FilterConfig filterConfig) throws ServletException{
    	log.trace("RoleAuthFilter.init()");
    	
    	this.filterConfig = filterConfig;
    			
    	// 設定白名單
		String urls = filterConfig.getInitParameter("noCheck");
		StringTokenizer token = new StringTokenizer(urls, "\n\t +,");
		
		// 忽略網址的列表
		urlList = new ArrayList<String>();
		while (token.hasMoreTokens()) {
			urlList.add(token.nextToken());
		}
		
    }
	
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException{
    	log.trace("RoleAuthFilter...");
    	
    	HttpServletRequest req  = (HttpServletRequest) request ;
    	HttpServletResponse resp = (HttpServletResponse)response;
    	HttpSession session = req.getSession();
		try {
			// Missing X Frame Options
			resp.setHeader("X-Frame-Options", "SAMEORIGIN");
			
			/*** 防止網頁cache機制 ***/
//			resp.setHeader("Pragma", "no-cache");
//			resp.setHeader("Cache-Control", "no-cache");
//			resp.setDateHeader("Expires", 0);
			// 改為no-store
			resp.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
			resp.setHeader("Pragma", "no-cache"); // HTTP 1.0.
			resp.setHeader("Strict-Transport-Security", "max-age=31536000");
			resp.setDateHeader("Expires", 0); // Proxies.
			
			// request.uri, e.x: /nnb/login
			String funcUri = req.getRequestURI();
			// contextPath, e.x: /nnb
			String contextPath = req.getContextPath();
			// servletPath, e.x: /login
//			String pathInfo = req.getPathInfo(); // getPathInfo在較新的Server上會為null，故改用getRequestURI去掉getContextPath
			String pathInfo = funcUri.indexOf(contextPath)==0 ? funcUri.replaceFirst(contextPath, "") : funcUri;
			
			log.trace(ESAPIUtil.vaildLog("RoleAuthFilter.funcUri: " + funcUri)); 
			log.trace("RoleAuthFilter.contextPath: " + contextPath);
			log.trace(ESAPIUtil.vaildLog("RoleAuthFilter.pathInfo: " + pathInfo)); 
			
			// 白名單
			log.trace("RoleAuthFilter.noCheck.urlList: " + urlList);
			
			// uri=contextPath，不管有沒有sessionId都導引到登入頁，e.x.:.../nnb、.../nb3
			if( contextPath.equals(funcUri) || (contextPath+"/").equals(funcUri) ) {
				log.trace(contextPath + "...");
				String redirectUrl = req.getContextPath() + "/login";
				resp.sendRedirect(redirectUrl);
				return;
			}
			
			String isE2E = SpringBeanFactory.getBean("isE2E")!=null
					? SpringBeanFactory.getBean("isE2E").toString() : "N";
					req.setAttribute("isE2E", isE2E);
			
					
			// openIdgate 此FLAG 決定是否開啟IDGATE交易 ,  Y=開 , N=關 || 相關檔案 : idgateAlert.jsp , idgateConfirmAlert.jsp , RoleAuthFilter.java
			// 預設關閉
			if(StrUtils.isNotEmpty(SpringBeanFactory.getBean("openIdgate"))) {
				request.setAttribute("openIdgate", SpringBeanFactory.getBean("openIdgate"));
				log.info("OpenIdgate : {} " , SpringBeanFactory.getBean("openIdgate").toString());	
			}else {
				request.setAttribute("openIdgate", "N");
				log.info("OpenIdgate : N ");	
			}		
			
			// 此FLAG 決定是否驗證 isSessionID ，建議:只本機開發時異動 時依照需要再改成N
			String isSessionID = SpringBeanFactory.getBean("isSessionID")!=null
									? SpringBeanFactory.getBean("isSessionID").toString() : "Y";
			log.trace("isSessionID: {}", isSessionID);
			if("N".equals(isSessionID) ) {
				log.trace("NB3 IN LOCAL");
				chain.doFilter(request, response);
				return;
			}
			
			
			log.trace("checkWhiteList...");
			String[] whitelist = { 
					"/css/", "/js/", "/img/", "/fonts/", "/component/", "/FISC/InterBank/",
					"/ONLINE/APPLY/", "/DIGITAL/ACCOUNT/", "/ONLINE/print", "/download", "/CREDIT/APPLY/",
					"/term/", "/public/", "/updateTmp/", "/upload/", "/CAPCODE/", "/RESET/",
					"/COMPONENT/", "/MONITOR", "/login", "/keyboard", "/EXTERNAL/API/", 
					"/webservice", "/MB", "/errorCloss", "/SEND/MAIL/" , "/batch/com/", "/getAnn/", "/directDownload",
					"/CUSTOMER/SERVICE", "/PORTAL","/EBILL/PAY","/APPLY/CREDIT/getAreaListAjax","/RATE/QUERY/","/GOLD/OUT/PASSBOOK",
					"/clearMenuList","/clearAdmNbStatu",
					"/getAds/","/MB3/",
					"/EBILL/APPLY/","/ebillApply/","/IBPDToBank/API/query","/IDGATE/CHECK/","/VERIFYMAIL/"
			};
			FindFileChange findFileChange= SpringBeanFactory.getBean("findFileChange");
			request.setAttribute("jscssDate", findFileChange.initdate.toString("yyyyMMddHH"));
			log.debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@whitelist >>>>>>>> {}",whitelist);
			if(pathInfo.startsWith("/IBPDToBank/API/query")) {
		        log.trace("/IBPDToBank/API/query");
		        Enumeration<String> requestHeader = req.getHeaderNames();
		        log.trace("------- header -------");
		        while(requestHeader.hasMoreElements()){
		            String headerKey=requestHeader.nextElement().toString();
		            log.trace("headerKey="+headerKey+";value="+req.getHeader(headerKey));
		        }
			}
			
			// 白名單檢查
			if( !checkWhiteList(pathInfo, whitelist) && !urlList.contains(pathInfo) ) {
				log.trace("CHECK ROLE AUTH...");
				
				
				// 不在白名單驗證使用者tokenid
				String tokenid = "";
				tokenid = String.valueOf(session.getAttribute(SessionUtil.TOKENID));
				log.trace("RoleAuthFilter.tokenid: " + tokenid);
				if( "".equals(tokenid) || "null".equals(tokenid) ) {
					// 沒通過驗證
					log.trace("RoleAuthFilter.req.getContextPath(): " + req.getContextPath());
					String redirectUrl = req.getContextPath() + "/login";
					log.trace("RoleAuthFilter.resp.url: " + redirectUrl);
					resp.sendRedirect(redirectUrl);
					return;
					
				}
				
				// 驗證使用者在資料庫是否為登入狀態
				String cusidn = new String(Base64.getDecoder().decode(String.valueOf(session.getAttribute(SessionUtil.CUSIDN))), "utf-8");
				log.trace("RoleAuthFilter.cusidn: " + cusidn);
				
				login_out_service = login_out_service==null ? SpringBeanFactory.getBean("login_out_Service") : login_out_service;
				
				boolean isLoginStatus = login_out_service.isLoginStatus(cusidn);
				log.trace("RoleAuthFilter.isLoginStatus: " + isLoginStatus);
				
				
				// 20201218--已從其他位置登入邏輯修正:原本用IP，改成若Session有TOKENID，則比對資料庫是否吻合，若不吻合代表已從其他位置登入
				// 驗證使用者在資料庫TOKENID是否吻合
				boolean isLoginToken = login_out_service.isLoginToken(cusidn, tokenid);
				
//				String reqIP = login_out_service.loginIP(cusidn);
//				log.trace(ESAPIUtil.vaildLog("RoleAuthFilter.reqIP: " + reqIP));
				
				// 若是登出狀態或IP不同則導回登入頁重新登入
//				if( !isLoginStatus || !reqIP.equals(WebUtil.getIpAddr(req))) {
				// 20201209--IP是否與登入時不同的驗證邏輯，於企業戶多個IP或行動網路浮動IP會有問題所以拔掉
				// 20201218--不驗證IP會發生可以從多個位置同時登入NB3，故加上驗證TOKENID在Session與DB.ADMLOGIN是否相符
				if ( !isLoginStatus || !isLoginToken ) {
					log.warn(ESAPIUtil.vaildLog("RoleAuthFilter.reLogin: cusidn=" + cusidn));
					// 測試環境不踢退
					if(!isSessionID.equals("T")) {
						String redirectUrl = req.getContextPath() + "/reLogin";
						resp.sendRedirect(redirectUrl);
						return;
					}
				}
					
			}
			
			// 通過此Filter
			chain.doFilter(request, response);
			
		} catch (Exception e) {
			log.error("",e);
		}
    }
    
    // 檢查是否是白名單
    public boolean checkWhiteList(String pathInfo, String[] strArr) {
    	for(String str : strArr) {
    		if(pathInfo.startsWith(str)) {
    			return true;
    		}
    	}
    	return false;
    	
    }
    
	public void destroy() {
		this.filterConfig = null;
	}
	
}
