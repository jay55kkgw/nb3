package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class B017_REST_RS  extends BaseRestBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2746893464588061851L;
	
	private String AGREE;
	private String ACCOUNT;
	private String EMAILMSG;
	
	public String getAGREE() {
		return AGREE;
	}
	public void setAGREE(String aGREE) {
		AGREE = aGREE;
	}
	public String getACCOUNT() {
		return ACCOUNT;
	}
	public void setACCOUNT(String aCCOUNT) {
		ACCOUNT = aCCOUNT;
	}
	public String getEMAILMSG() {
		return EMAILMSG;
	}
	public void setEMAILMSG(String eMAILMSG) {
		EMAILMSG = eMAILMSG;
	}
	
	
	
}
