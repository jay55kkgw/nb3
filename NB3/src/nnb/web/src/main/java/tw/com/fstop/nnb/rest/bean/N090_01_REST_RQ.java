package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N090_01_REST_RQ extends BaseRestBean_GOLD implements Serializable {

	
	/**
	 * 黃金買進RQ
	 */
	private static final long serialVersionUID = 1392497182026606436L;
	private String ADOPID = "N09001";
	private String DATE;
	private String TIME;
	private String CERTACN;
	private String FLAG;
	private String XMLCA;
	private String XMLCN;
	private String PINNEW;
	private String CUSIDN;
	private String TRNSRC;
	private String TRNTYP;
	private String TRNCOD;
	private String TRNBDT;
	private String ACN;
	private String SVACN;
	private String TRNGD;
	private String PRICE;
	private String DISPRICE;
	private String PERDIS;
	private String TRNFEE_SIGN;
	private String TRNFEE;
	private String TRNAMT_SIGN;
	private String TRNAMT;
	private String DISAMT_SIGN;
	private String DISAMT;
	private String UID;
	private String FGTXWAY;
	private String DPMYEMAIL;
	private String pkcs7Sign;
	private String jsondc;
	//IDGATE
	private String sessionID;
	private String idgateID;
	private String txnID;
	
	public String getSessionID() {
		return sessionID;
	}
	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}
	public String getIdgateID() {
		return idgateID;
	}
	public void setIdgateID(String idgateID) {
		this.idgateID = idgateID;
	}
	public String getTxnID() {
		return txnID;
	}
	public void setTxnID(String txnID) {
		this.txnID = txnID;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getCERTACN() {
		return CERTACN;
	}
	public void setCERTACN(String cERTACN) {
		CERTACN = cERTACN;
	}
	public String getFLAG() {
		return FLAG;
	}
	public void setFLAG(String fLAG) {
		FLAG = fLAG;
	}
	public String getXMLCA() {
		return XMLCA;
	}
	public void setXMLCA(String xMLCA) {
		XMLCA = xMLCA;
	}
	public String getXMLCN() {
		return XMLCN;
	}
	public void setXMLCN(String xMLCN) {
		XMLCN = xMLCN;
	}
	public String getPINNEW() {
		return PINNEW;
	}
	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getTRNSRC() {
		return TRNSRC;
	}
	public void setTRNSRC(String tRNSRC) {
		TRNSRC = tRNSRC;
	}
	public String getTRNTYP() {
		return TRNTYP;
	}
	public void setTRNTYP(String tRNTYP) {
		TRNTYP = tRNTYP;
	}
	public String getTRNCOD() {
		return TRNCOD;
	}
	public void setTRNCOD(String tRNCOD) {
		TRNCOD = tRNCOD;
	}
	public String getTRNBDT() {
		return TRNBDT;
	}
	public void setTRNBDT(String tRNBDT) {
		TRNBDT = tRNBDT;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getSVACN() {
		return SVACN;
	}
	public void setSVACN(String sVACN) {
		SVACN = sVACN;
	}
	public String getTRNGD() {
		return TRNGD;
	}
	public void setTRNGD(String tRNGD) {
		TRNGD = tRNGD;
	}
	public String getPRICE() {
		return PRICE;
	}
	public void setPRICE(String pRICE) {
		PRICE = pRICE;
	}
	public String getDISPRICE() {
		return DISPRICE;
	}
	public void setDISPRICE(String dISPRICE) {
		DISPRICE = dISPRICE;
	}
	public String getPERDIS() {
		return PERDIS;
	}
	public void setPERDIS(String pERDIS) {
		PERDIS = pERDIS;
	}
	public String getTRNFEE_SIGN() {
		return TRNFEE_SIGN;
	}
	public void setTRNFEE_SIGN(String tRNFEE_SIGN) {
		TRNFEE_SIGN = tRNFEE_SIGN;
	}
	public String getTRNFEE() {
		return TRNFEE;
	}
	public void setTRNFEE(String tRNFEE) {
		TRNFEE = tRNFEE;
	}
	public String getTRNAMT_SIGN() {
		return TRNAMT_SIGN;
	}
	public void setTRNAMT_SIGN(String tRNAMT_SIGN) {
		TRNAMT_SIGN = tRNAMT_SIGN;
	}
	public String getTRNAMT() {
		return TRNAMT;
	}
	public void setTRNAMT(String tRNAMT) {
		TRNAMT = tRNAMT;
	}
	public String getDISAMT_SIGN() {
		return DISAMT_SIGN;
	}
	public void setDISAMT_SIGN(String dISAMT_SIGN) {
		DISAMT_SIGN = dISAMT_SIGN;
	}
	public String getDISAMT() {
		return DISAMT;
	}
	public void setDISAMT(String dISAMT) {
		DISAMT = dISAMT;
	}
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getDPMYEMAIL() {
		return DPMYEMAIL;
	}
	public void setDPMYEMAIL(String dPMYEMAIL) {
		DPMYEMAIL = dPMYEMAIL;
	}
	public String getPkcs7Sign() {
		return pkcs7Sign;
	}
	public void setPkcs7Sign(String pkcs7Sign) {
		this.pkcs7Sign = pkcs7Sign;
	}
	public String getJsondc() {
		return jsondc;
	}
	public void setJsondc(String jsondc) {
		this.jsondc = jsondc;
	}
}
