package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;


public class N580_REST_RQ extends BaseRestBean_OLA implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6444267764265911932L;

	private String CUSIDN;//統一編號
	private String ICSEQ;
	private String pkcs7Sign;
	private String TRMID;
	private String ACN;
	private String jsondc;
	private String CHIP_ACN;
	private String FGTXWAY;
	private String ADOPID;
	private String UID;
	private String ISSUER;
	private String TAC;
	private String iSeqNo;
	private String OUTACN;
	private String ACNNO;
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getICSEQ() {
		return ICSEQ;
	}
	public void setICSEQ(String iCSEQ) {
		ICSEQ = iCSEQ;
	}
	public String getPkcs7Sign() {
		return pkcs7Sign;
	}
	public void setPkcs7Sign(String pkcs7Sign) {
		this.pkcs7Sign = pkcs7Sign;
	}
	public String getTRMID() {
		return TRMID;
	}
	public void setTRMID(String tRMID) {
		TRMID = tRMID;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getJsondc() {
		return jsondc;
	}
	public void setJsondc(String jsondc) {
		this.jsondc = jsondc;
	}
	public String getCHIP_ACN() {
		return CHIP_ACN;
	}
	public void setCHIP_ACN(String cHIP_ACN) {
		CHIP_ACN = cHIP_ACN;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	public String getISSUER() {
		return ISSUER;
	}
	public void setISSUER(String iSSUER) {
		ISSUER = iSSUER;
	}
	public String getTAC() {
		return TAC;
	}
	public void setTAC(String tAC) {
		TAC = tAC;
	}
	public String getiSeqNo() {
		return iSeqNo;
	}
	public void setiSeqNo(String iSeqNo) {
		this.iSeqNo = iSeqNo;
	}
	public String getOUTACN() {
		return OUTACN;
	}
	public void setOUTACN(String oUTACN) {
		OUTACN = oUTACN;
	}
	public String getACNNO() {
		return ACNNO;
	}
	public void setACNNO(String aCNNO) {
		ACNNO = aCNNO;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}