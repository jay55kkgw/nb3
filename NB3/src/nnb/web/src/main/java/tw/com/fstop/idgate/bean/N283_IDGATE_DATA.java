package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class N283_IDGATE_DATA implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8685795346500185074L;

	@SerializedName(value = "TXTYPE")
	private String TXTYPE;

	@SerializedName(value = "DPSCHNO")
	private String DPSCHNO;

	@SerializedName(value = "DPNEXTDATE")
	private String DPNEXTDATE;
	
	@SerializedName(value = "DPWDAC")
	private String DPWDAC;
	
	@SerializedName(value = "DPSVBH")
	private String DPSVBH;
	
	@SerializedName(value = "DPSVAC")
	private String DPSVAC;
	
	@SerializedName(value = "DPTXAMT")
	private String DPTXAMT;

	public String getTXTYPE() {
		return TXTYPE;
	}

	public void setTXTYPE(String tXTYPE) {
		TXTYPE = tXTYPE;
	}

	public String getDPSCHNO() {
		return DPSCHNO;
	}

	public void setDPSCHNO(String dPSCHNO) {
		DPSCHNO = dPSCHNO;
	}

	public String getDPNEXTDATE() {
		return DPNEXTDATE;
	}

	public void setDPNEXTDATE(String dPNEXTDATE) {
		DPNEXTDATE = dPNEXTDATE;
	}

	public String getDPWDAC() {
		return DPWDAC;
	}

	public void setDPWDAC(String dPWDAC) {
		DPWDAC = dPWDAC;
	}

	public String getDPSVBH() {
		return DPSVBH;
	}

	public void setDPSVBH(String dPSVBH) {
		DPSVBH = dPSVBH;
	}

	public String getDPSVAC() {
		return DPSVAC;
	}

	public void setDPSVAC(String dPSVAC) {
		DPSVAC = dPSVAC;
	}

	public String getDPTXAMT() {
		return DPTXAMT;
	}

	public void setDPTXAMT(String dPTXAMT) {
		DPTXAMT = dPTXAMT;
	}
}
