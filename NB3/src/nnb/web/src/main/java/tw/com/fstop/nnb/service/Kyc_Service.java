package tw.com.fstop.nnb.service;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.po.SYSPARAMDATA;
import tw.com.fstop.tbb.nnb.dao.SysParamDataDao;
@Service
public class Kyc_Service extends Base_Service{
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	
	@Autowired
	private SysParamDataDao sysParamDataDao;
	
	/**
	 * 取得問卷版本日期
	 */
	public String getKYCDate(){
		String KYCDate = null;
		
		SYSPARAMDATA po = null; 
		try{
			po = sysParamDataDao.getByPK("NBSYS");
			
			KYCDate = po.getKYCDATE().trim();
		}
		catch(Exception e){
			log.error("getKYCDate error >> {}",e);
		}
		return KYCDate;
	}
	
	/**
	 * 算出time1跟time2間隔幾日 
	 */	
	public long getRangeDay(String time1,String time2){
		long rangeDay = 0;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		try{
			Date date1 = sdf.parse(time1);
			Date date2 = sdf.parse(time2);
			rangeDay = date1.getTime() - date2.getTime();
			rangeDay = rangeDay / 1000 / 60 / 60 / 24;
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getRangeDay error >> {}",e);
		}
		return rangeDay;
	}

}
