package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N392_REST_RS extends BaseRestBean implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1177301659522134329L;
	
	private String CMQTIME;
	private String TRANSCODE;
	private String TRADEDATE;
	private String AMT3;
	private String FCA2;
	private String OUTACN;
	private String AMT5;
	private String FCAFEE;
	private String SSLTXNO;
	private String CRY;
	
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getTRANSCODE() {
		return TRANSCODE;
	}
	public void setTRANSCODE(String tRANSCODE) {
		TRANSCODE = tRANSCODE;
	}
	public String getTRADEDATE() {
		return TRADEDATE;
	}
	public void setTRADEDATE(String tRADEDATE) {
		TRADEDATE = tRADEDATE;
	}
	public String getAMT3() {
		return AMT3;
	}
	public void setAMT3(String aMT3) {
		AMT3 = aMT3;
	}
	public String getFCA2() {
		return FCA2;
	}
	public void setFCA2(String fCA2) {
		FCA2 = fCA2;
	}
	public String getOUTACN() {
		return OUTACN;
	}
	public void setOUTACN(String oUTACN) {
		OUTACN = oUTACN;
	}
	public String getAMT5() {
		return AMT5;
	}
	public void setAMT5(String aMT5) {
		AMT5 = aMT5;
	}
	public String getFCAFEE() {
		return FCAFEE;
	}
	public void setFCAFEE(String fCAFEE) {
		FCAFEE = fCAFEE;
	}
	public String getSSLTXNO() {
		return SSLTXNO;
	}
	public void setSSLTXNO(String sSLTXNO) {
		SSLTXNO = sSLTXNO;
	}
	public String getCRY() {
		return CRY;
	}
	public void setCRY(String cRY) {
		CRY = cRY;
	}
}
