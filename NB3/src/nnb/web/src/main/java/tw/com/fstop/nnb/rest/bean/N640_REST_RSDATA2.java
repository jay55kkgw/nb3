package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N640_REST_RSDATA2 extends BaseRestBean implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7727320883072431123L;

	private String FILLER_X4;//備註

	private String LSTLTD;// 最後異動日

	private String ACN;// 帳號

	private String MEMO;// 摘要

	private String CODDBCR;

	private String DPDCCODE;// 借貸別

	private String AMTTRN;// 交易金額

	private String BAL;// 餘額

	private String CHKNUM;

	private String DATA16;// 補充資料

	private String TRNBRH;// 收付行

	public String getFILLER_X4()
	{
		return FILLER_X4;
	}

	public void setFILLER_X4(String fILLER_X4)
	{
		FILLER_X4 = fILLER_X4;
	}

	public String getLSTLTD()
	{
		return LSTLTD;
	}

	public void setLSTLTD(String lSTLTD)
	{
		LSTLTD = lSTLTD;
	}

	public String getACN()
	{
		return ACN;
	}

	public void setACN(String aCN)
	{
		ACN = aCN;
	}

	public String getMEMO()
	{
		return MEMO;
	}

	public void setMEMO(String mEMO)
	{
		MEMO = mEMO;
	}

	public String getCODDBCR()
	{
		return CODDBCR;
	}

	public void setCODDBCR(String cODDBCR)
	{
		CODDBCR = cODDBCR;
	}

	public String getDPDCCODE()
	{
		return DPDCCODE;
	}

	public void setDPDCCODE(String dPDCCODE)
	{
		DPDCCODE = dPDCCODE;
	}

	public String getAMTTRN()
	{
		return AMTTRN;
	}

	public void setAMTTRN(String aMTTRN)
	{
		AMTTRN = aMTTRN;
	}

	public String getBAL()
	{
		return BAL;
	}

	public void setBAL(String bAL)
	{
		BAL = bAL;
	}

	public String getCHKNUM()
	{
		return CHKNUM;
	}

	public void setCHKNUM(String cHKNUM)
	{
		CHKNUM = cHKNUM;
	}

	public String getDATA16()
	{
		return DATA16;
	}

	public void setDATA16(String dATA16)
	{
		DATA16 = dATA16;
	}

	public String getTRNBRH()
	{
		return TRNBRH;
	}

	public void setTRNBRH(String tRNBRH)
	{
		TRNBRH = tRNBRH;
	}

}
