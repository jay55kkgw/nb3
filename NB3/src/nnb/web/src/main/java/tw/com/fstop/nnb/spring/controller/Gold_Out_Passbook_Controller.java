package tw.com.fstop.nnb.spring.controller;

import java.net.URLDecoder;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.custom.annotation.ISTXNLOG;
import tw.com.fstop.nnb.service.Gold_Passbook_Service;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.DownloadUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.web.util.WebUtil;

@SessionAttributes({ SessionUtil.DOWNLOAD_DATALISTMAP_DATA,SessionUtil.PRINT_DATALISTMAP_DATA})
@Controller
@RequestMapping(value = "/GOLD/OUT/PASSBOOK")
public class Gold_Out_Passbook_Controller {

	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private Gold_Passbook_Service gold_passbook_Service;

	// 黃金存摺當日價格查詢
	@ISTXNLOG(value="GD11TL")
	@RequestMapping(value = "/day_price_query")
	public String day_price_query_get_gold_list(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {

		String target = "/online_apply/ErrorWithoutMenu";
		BaseResult bs = null;
		String systime="";
		int isGH=2;
		log.trace("day_price_query_get_gold_list");
		try {
			bs = gold_passbook_Service.getGoldList();
			systime = gold_passbook_Service.getSystemtime();
			isGH = gold_passbook_Service.isGoldHOLIDAY();
//			log.debug("branch_list size >> {}", bs.size());
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("day_price_query_get_gold_list error >> {}",e);
		}finally {
			if (bs != null && bs.getResult()) {
				log.debug("gold_list size >> {}", bs.getData());
			    Map<String,Object> dataMap = (Map<String, Object>) bs.getData();
				Map<String, Object> dataListMap = (Map<String, Object>)dataMap.get("REC");
				if(dataListMap.size() > 0) {
					List<Map<String,String>> dataList = (List<Map<String,String>>)dataListMap.get("data");
					dataMap.put("SIZE",dataList.size());
					log.debug("datalistmapp: >> {}", dataListMap);
					log.debug("dataList: >> {}", dataList);
					SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataList);
				}
				else {
					dataMap.put("SIZE",dataListMap.size());
				}
				target = "/gold_out/day_price_query";
				log.debug("gold_list size >> {}", bs.getData());
				log.debug("holiday_flag >> {}", isGH);
				log.debug("system_time >> {}", systime);
				model.addAttribute("gold_list", bs);
				model.addAttribute("system_time", systime);
				model.addAttribute("holiday_flag", isGH);
				model.addAttribute("gold_listJSON", CodeUtil.toJson(bs));
			}
			else{
				//新增回上一頁的路徑
				bs.setPrevious("/login");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		//String type = reqParam.get("type");
		return target;		
	}



	// 黃金存摺歷史價格查詢(GD11HS)
	@RequestMapping(value = "/history_price_query")
	public String history_price_query(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/online_apply/ErrorWithoutMenu";
		try {
		} catch (Exception e) {
			log.error("history_price_query error >> {}",e);
		} finally {
			target = "/gold_out/history_price_query";
		}
		model.addAttribute("time_now", DateUtil.getDate("/") + " " + DateUtil.getTheTime(":"));
		return target;
	}

	// 黃金存摺歷史價格查詢(GD11PER、GD11LD)
	@RequestMapping(value = "/history_price_query_result")
	public String history_price_query_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/online_apply/ErrorWithoutMenu";
		BaseResult bs = null;
		log.trace("history_price_query_result>>");
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {
			bs = new BaseResult();
			bs = gold_passbook_Service.history_price_query_result(okMap);
			bs.addData("QUERYTYPE", reqParam.get("QUERYTYPE"));
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("history_price_query_result error >> {}",e);
		} finally {
			if (bs != null && bs.getResult())
			{
				if( ((String)((Map<String, Object>)bs.getData()).get("QUERYTYPE")).equals("LASTDAY") ) {
					target = "/gold_out/history_price_query_result_LD";
				}else {
					target = "/gold_out/history_price_query_result_PER";
				}
				List<Map<String, Object>> dataListMap = ((List<Map<String, Object>>) ((Map<String, Object>) bs.getData()).get("REC"));
				log.debug("dataListMap={}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, dataListMap);
				model.addAttribute("result_data", bs);
			}
			else
			{
				//新增回上一頁的路徑
				bs.setPrevious("/GOLD/OUT/PASSBOOK/history_price_query");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// 託收票據明細輸入頁直接下載
	@RequestMapping(value = "/history_price_query_ajaxDirectDownload")
	public String history_price_query_directDownload(HttpServletRequest request,HttpServletResponse response, @RequestBody(required = false) String serializeString, Model model) {
		log.trace("history_price_query_ajaxDirectDownload");
		String target = "/online_apply/ErrorWithoutMenu";
		BaseResult bs = null;

		try {
			bs = new BaseResult();
			
			// 序列化傳入值
			Map<String, String> formMap = DownloadUtil.serializeStringToMap(serializeString);
			log.debug(ESAPIUtil.vaildLog("formMap={}" + formMap));
			for(Entry<String,String> entry : formMap.entrySet()){
			formMap.put(entry.getKey(),URLDecoder.decode(entry.getValue(),"UTF-8"));
			}
			bs =gold_passbook_Service.history_price_query_directDownload(formMap);

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("history_price_query_directDownload error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "forward:/directDownload";
				Map<String, Object> dataMap = (Map) bs.getData();
				log.trace("dataMap={}", dataMap);
				List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("REC");
				log.trace("rowListMap={}", rowListMap);
				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, rowListMap);
				Map<String, Object> parameterMap = (Map<String,Object>)dataMap.get("parameterMap");
				request.setAttribute("parameterMap", parameterMap);
			} else {
				bs.setPrevious("/GOLD/OUT/PASSBOOK/history_price_query");
				model.addAttribute(BaseResult.ERROR, bs);

			}
		}
		return target;
	}
}
