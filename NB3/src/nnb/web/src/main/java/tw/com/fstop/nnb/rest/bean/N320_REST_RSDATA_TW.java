package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N320_REST_RSDATA_TW extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7445173263967145749L;

	private String ACN;
	private String SEQ;
	private String SEQ2;
	private String AMTORLN;
	private String BAL;
	private String ITR;
	private String DATFSLN;
	private String DDT;
	private String AWT;
	private String RAT;
	private String DATITPY;
	private String ITRLTD;
	private String ITRUS;
	private String PAYTYPE;
	private String AMTAPY;
	
	public String getAMTAPY() {
		return AMTAPY;
	}
	public void setAMTAPY(String aMTAPY) {
		AMTAPY = aMTAPY;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getSEQ() {
		return SEQ;
	}
	public void setSEQ(String sEQ) {
		SEQ = sEQ;
	}
	public String getAMTORLN() {
		return AMTORLN;
	}
	public void setAMTORLN(String aMTORLN) {
		AMTORLN = aMTORLN;
	}
	public String getBAL() {
		return BAL;
	}
	public void setBAL(String bAL) {
		BAL = bAL;
	}
	public String getITR() {
		return ITR;
	}
	public void setITR(String iTR) {
		ITR = iTR;
	}
	public String getDATFSLN() {
		return DATFSLN;
	}
	public void setDATFSLN(String dATFSLN) {
		DATFSLN = dATFSLN;
	}
	public String getDDT() {
		return DDT;
	}
	public void setDDT(String dDT) {
		DDT = dDT;
	}
	public String getAWT() {
		return AWT;
	}
	public void setAWT(String aWT) {
		AWT = aWT;
	}
	public String getDATITPY() {
		return DATITPY;
	}
	public void setDATITPY(String dATITPY) {
		DATITPY = dATITPY;
	}
	public String getSEQ2() {
		return SEQ2;
	}
	public void setSEQ2(String sEQ2) {
		SEQ2 = sEQ2;
	}
	public String getRAT() {
		return RAT;
	}
	public void setRAT(String rAT) {
		RAT = rAT;
	}
	public String getITRLTD() {
		return ITRLTD;
	}
	public void setITRLTD(String iTRLTD) {
		ITRLTD = iTRLTD;
	}
	public String getITRUS() {
		return ITRUS;
	}
	public void setITRUS(String iTRUS) {
		ITRUS = iTRUS;
	}
	public String getPAYTYPE() {
		return PAYTYPE;
	}
	public void setPAYTYPE(String pAYTYPE) {
		PAYTYPE = pAYTYPE;
	}
	
}
