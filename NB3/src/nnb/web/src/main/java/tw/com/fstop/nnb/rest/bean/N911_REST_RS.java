package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N911_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2650061838340800048L;

	private String OFFSET;		// 空白
	private String HEADER;		// HEADER
	private String MSGCOD;		// 訊息代碼
	private String MMACOD;		// MMA註記
	private String XMLCOD;		// 憑證代碼
	private String BRTHDY;		// 出生年月日
	private String TELNUM;		// 電話號碼
	private String LOGINDT;		// 上次登入日期
	private String LOGINTM;		// 上次登入時間
	private String RESERVE;		// 保留
	private String COUNT;		// 筆數
	private String SPACE;		// 空白_SPACE(5)
	private String WINDATA;		// 空白_WINDATA(5)
	private String AREA;		// 地區別_AREA(8)
	private String CHGYON;		// 地區別是否做過異動
	private String STAFF;		// 是否為行員
	private String MBLINDT;		// 上次登入日期 (MB)
	private String MBLINTM;		// 上次登入時間 (MB)
	private String MBSTAT;		// 行動銀行目前狀態
	private String MBOPNDT;		// 行動銀行啟用/關閉日期
	private String MBOPNTM;		// 行動銀行啟用/關閉時間
	private String HKCODE;		// 香港網銀是否需轉置註記
	private String DIGCODE;		// 數位帳戶到期通知
	private String MAILADDR;	// 電子郵件地址
	private String TWFSTS;		// 雙因素憑證狀態
	private String NOCARD;		// 是否申請無卡提款密碼
	private String QCKLGIN;		// 是否申請快速登入
	private String QCKTRAN;		// 是否申請快速交易
	private String SMALLPAY;	// 是否申請小額轉帳交易
	private String SMALLCNT;	// 小額當日已轉帳次數
	private String QLGIDT;		// 快登啟用 / 關閉日期
	private String QTRNDT;		// 快交啟用 / 關閉日期
	private String QSMLDT;		// 小額啟用 / 關閉日期
	private String QKLINDT;		// 上次登入日期 ( 快登 )
	private String QKLINTM;		// 上次登入時間 ( 快登 )
	private String BIOME;		// 生物辨識
	private String MOBTEL;		// 行動電話
	private String REMAILID;	// 對帳單退件類別
	private String OPCODE;		// 使用模式
	private String USERID1;		// 企網管理者1
	private String USERID2;		// 企網管理者2
	
	public String getOFFSET() {
		return OFFSET;
	}
	public void setOFFSET(String oFFSET) {
		OFFSET = oFFSET;
	}
	public String getHEADER() {
		return HEADER;
	}
	public void setHEADER(String hEADER) {
		HEADER = hEADER;
	}
	public String getMSGCOD() {
		return MSGCOD;
	}
	public void setMSGCOD(String mSGCOD) {
		MSGCOD = mSGCOD;
	}
	public String getMMACOD() {
		return MMACOD;
	}
	public void setMMACOD(String mMACOD) {
		MMACOD = mMACOD;
	}
	public String getXMLCOD() {
		return XMLCOD;
	}
	public void setXMLCOD(String xMLCOD) {
		XMLCOD = xMLCOD;
	}
	public String getBRTHDY() {
		return BRTHDY;
	}
	public void setBRTHDY(String bRTHDY) {
		BRTHDY = bRTHDY;
	}
	public String getTELNUM() {
		return TELNUM;
	}
	public void setTELNUM(String tELNUM) {
		TELNUM = tELNUM;
	}
	public String getLOGINDT() {
		return LOGINDT;
	}
	public void setLOGINDT(String lOGINDT) {
		LOGINDT = lOGINDT;
	}
	public String getLOGINTM() {
		return LOGINTM;
	}
	public void setLOGINTM(String lOGINTM) {
		LOGINTM = lOGINTM;
	}
	public String getRESERVE() {
		return RESERVE;
	}
	public void setRESERVE(String rESERVE) {
		RESERVE = rESERVE;
	}
	public String getCOUNT() {
		return COUNT;
	}
	public void setCOUNT(String cOUNT) {
		COUNT = cOUNT;
	}
	public String getSPACE() {
		return SPACE;
	}
	public void setSPACE(String sPACE) {
		SPACE = sPACE;
	}
	public String getWINDATA() {
		return WINDATA;
	}
	public void setWINDATA(String wINDATA) {
		WINDATA = wINDATA;
	}
	public String getAREA() {
		return AREA;
	}
	public void setAREA(String aREA) {
		AREA = aREA;
	}
	public String getCHGYON() {
		return CHGYON;
	}
	public void setCHGYON(String cHGYON) {
		CHGYON = cHGYON;
	}
	public String getSTAFF() {
		return STAFF;
	}
	public void setSTAFF(String sTAFF) {
		STAFF = sTAFF;
	}
	public String getMBLINDT() {
		return MBLINDT;
	}
	public void setMBLINDT(String mBLINDT) {
		MBLINDT = mBLINDT;
	}
	public String getMBLINTM() {
		return MBLINTM;
	}
	public void setMBLINTM(String mBLINTM) {
		MBLINTM = mBLINTM;
	}
	public String getMBSTAT() {
		return MBSTAT;
	}
	public void setMBSTAT(String mBSTAT) {
		MBSTAT = mBSTAT;
	}
	public String getMBOPNDT() {
		return MBOPNDT;
	}
	public void setMBOPNDT(String mBOPNDT) {
		MBOPNDT = mBOPNDT;
	}
	public String getMBOPNTM() {
		return MBOPNTM;
	}
	public void setMBOPNTM(String mBOPNTM) {
		MBOPNTM = mBOPNTM;
	}
	public String getHKCODE() {
		return HKCODE;
	}
	public void setHKCODE(String hKCODE) {
		HKCODE = hKCODE;
	}
	public String getDIGCODE() {
		return DIGCODE;
	}
	public void setDIGCODE(String dIGCODE) {
		DIGCODE = dIGCODE;
	}
	public String getMAILADDR() {
		return MAILADDR;
	}
	public void setMAILADDR(String mAILADDR) {
		MAILADDR = mAILADDR;
	}
	public String getTWFSTS() {
		return TWFSTS;
	}
	public void setTWFSTS(String tWFSTS) {
		TWFSTS = tWFSTS;
	}
	public String getNOCARD() {
		return NOCARD;
	}
	public void setNOCARD(String nOCARD) {
		NOCARD = nOCARD;
	}
	public String getQCKLGIN() {
		return QCKLGIN;
	}
	public void setQCKLGIN(String qCKLGIN) {
		QCKLGIN = qCKLGIN;
	}
	public String getQCKTRAN() {
		return QCKTRAN;
	}
	public void setQCKTRAN(String qCKTRAN) {
		QCKTRAN = qCKTRAN;
	}
	public String getSMALLPAY() {
		return SMALLPAY;
	}
	public void setSMALLPAY(String sMALLPAY) {
		SMALLPAY = sMALLPAY;
	}
	public String getSMALLCNT() {
		return SMALLCNT;
	}
	public void setSMALLCNT(String sMALLCNT) {
		SMALLCNT = sMALLCNT;
	}
	public String getQLGIDT() {
		return QLGIDT;
	}
	public void setQLGIDT(String qLGIDT) {
		QLGIDT = qLGIDT;
	}
	public String getQTRNDT() {
		return QTRNDT;
	}
	public void setQTRNDT(String qTRNDT) {
		QTRNDT = qTRNDT;
	}
	public String getQSMLDT() {
		return QSMLDT;
	}
	public void setQSMLDT(String qSMLDT) {
		QSMLDT = qSMLDT;
	}
	public String getQKLINDT() {
		return QKLINDT;
	}
	public void setQKLINDT(String qKLINDT) {
		QKLINDT = qKLINDT;
	}
	public String getQKLINTM() {
		return QKLINTM;
	}
	public void setQKLINTM(String qKLINTM) {
		QKLINTM = qKLINTM;
	}
	public String getBIOME() {
		return BIOME;
	}
	public void setBIOME(String bIOME) {
		BIOME = bIOME;
	}
	public String getMOBTEL() {
		return MOBTEL;
	}
	public void setMOBTEL(String mOBTEL) {
		MOBTEL = mOBTEL;
	}
	public String getREMAILID() {
		return REMAILID;
	}
	public void setREMAILID(String rEMAILID) {
		REMAILID = rEMAILID;
	}
	public String getOPCODE() {
		return OPCODE;
	}
	public void setOPCODE(String oPCODE) {
		OPCODE = oPCODE;
	}
	public String getUSERID1() {
		return USERID1;
	}
	public void setUSERID1(String uSERID1) {
		USERID1 = uSERID1;
	}
	public String getUSERID2() {
		return USERID2;
	}
	public void setUSERID2(String uSERID2) {
		USERID2 = uSERID2;
	}

}