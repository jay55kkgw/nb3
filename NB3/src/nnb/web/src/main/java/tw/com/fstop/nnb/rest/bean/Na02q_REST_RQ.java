package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * NA02Q電文RQ
 */
public class Na02q_REST_RQ extends BaseRestBean_CC implements Serializable{
	private static final long serialVersionUID = 6868861910004819383L;
	
	private String UID;
	private String CUSIDN;
	
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
}