package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.Map;

/**
 * 手機門號_綁定查詢
 * 
 * @author Vincenthuang
 *
 */
public class MtpBindQry_REST_RQ extends BaseRestBean_QR implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 39717691064748449L;
	
	//=========其餘參數===========
	private String ADOPID = "MtpBindQry";//紀錄TXNLOG用
	
	
	//=========API參數===========
	private String mobilephone;			//手機門號
	private String userid;				//統一編號

	
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getMobilephone() {
		return mobilephone;
	}
	public void setMobilephone(String mobilephone) {
		this.mobilephone = mobilephone;
	}

	
	
}
