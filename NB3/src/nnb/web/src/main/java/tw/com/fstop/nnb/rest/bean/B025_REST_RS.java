package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class B025_REST_RS  extends BaseRestBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3575475805767366732L;
	
	private String DATE;//回應日期(民國年)YYYMMDD
	private String TIME;//回應時間HHMMSS
	private String CUSIDN;
	private String RECDATE;
	private String CMQTIME; 
	
	
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getRECDATE() {
		return RECDATE;
	}
	public void setRECDATE(String rECDATE) {
		RECDATE = rECDATE;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
}
