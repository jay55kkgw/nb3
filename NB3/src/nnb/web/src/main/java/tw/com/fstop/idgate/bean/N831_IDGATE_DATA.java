package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class N831_IDGATE_DATA implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3750893419815329920L;

	@SerializedName(value = "CARDNUM")
	private String CARDNUM;
	
	@SerializedName(value = "CUSNUM")
	private String CUSNUM;

	@SerializedName(value = "ADOPID")
	private String ADOPID;
	
	@SerializedName(value = "TYPE")
	private String TYPE;
	
	@SerializedName(value = "ITMNUM")
	private String ITMNUM;

	public String getCARDNUM() {
		return CARDNUM;
	}

	public void setCARDNUM(String cARDNUM) {
		CARDNUM = cARDNUM;
	}

	public String getCUSNUM() {
		return CUSNUM;
	}

	public void setCUSNUM(String cUSNUM) {
		CUSNUM = cUSNUM;
	}

	public String getADOPID() {
		return ADOPID;
	}

	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}

	public String getTYPE() {
		return TYPE;
	}

	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}

	public String getITMNUM() {
		return ITMNUM;
	}

	public void setITMNUM(String iTMNUM) {
		ITMNUM = iTMNUM;
	}
	
}
