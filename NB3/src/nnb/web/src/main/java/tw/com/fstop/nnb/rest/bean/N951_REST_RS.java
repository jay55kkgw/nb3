package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N951_REST_RS extends BaseRestBean implements Serializable {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 9212618584365333820L;

    String MSGCOD;
    
	//rowdata用
	LinkedList<N951_REST_RSDATA> REC;

	public String getMSGCOD() {
		return MSGCOD;
	}


	public void setMSGCOD(String mSGCOD) {
		MSGCOD = mSGCOD;
	}


	public LinkedList<N951_REST_RSDATA> getREC() {
		return REC;
	}


	public void setREC(LinkedList<N951_REST_RSDATA> rEC) {
		REC = rEC;
	}
}
