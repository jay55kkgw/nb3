package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N857_REST_RQ extends BaseRestBean_OLA implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String DATE;
	private String TIME;
	private String ATTIBK;
	private String ATTIAC;
	private String ATDENO;
	private String ATMOBI;
	private String ATMSEQ;
	private String BIRTH;

	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getATTIBK() {
		return ATTIBK;
	}
	public void setATTIBK(String aTTIBK) {
		ATTIBK = aTTIBK;
	}
	public String getATTIAC() {
		return ATTIAC;
	}
	public void setATTIAC(String aTTIAC) {
		ATTIAC = aTTIAC;
	}
	public String getATDENO() {
		return ATDENO;
	}
	public void setATDENO(String aTDENO) {
		ATDENO = aTDENO;
	}
	public String getATMOBI() {
		return ATMOBI;
	}
	public void setATMOBI(String aTMOBI) {
		ATMOBI = aTMOBI;
	}
	public String getATMSEQ() {
		return ATMSEQ;
	}
	public void setATMSEQ(String aTMSEQ) {
		ATMSEQ = aTMSEQ;
	}
	public String getBIRTH() {
		return BIRTH;
	}
	public void setBIRTH(String bIRTH) {
		BIRTH = bIRTH;
	} 
	
}
