package tw.com.fstop.nnb.spring.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.service.TxnFxSchPayData_Service;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;

@RestController
@RequestMapping(value = "/MB/TXNFXSCHPAYDATA")
public class TxnFxSchPayData_Controller {

	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	TxnFxSchPayData_Service txnfxschpaydata_Service;

	/*
	 * 外幣預約結果查詢
	 */
	@PostMapping(value = "/query", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody BaseResult query(HttpServletRequest request, HttpServletResponse response,
			@RequestBody Map<String, String> reqParam, Model model) {
	    Map<String,String> okMap = ESAPIUtil.validStrMap(reqParam);
		log.info(ESAPIUtil.vaildLog("reqParam>>{}"+ okMap));
		BaseResult bs = new BaseResult();
		try {
			bs = txnfxschpaydata_Service.query(okMap);
			log.trace(ESAPIUtil.vaildLog("bs.getData>>{}" + bs.getData()));
		} catch (Throwable e) {
			log.error("Throwable>>{}", e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", "查詢失敗");
		}
		return bs;
	}
}
