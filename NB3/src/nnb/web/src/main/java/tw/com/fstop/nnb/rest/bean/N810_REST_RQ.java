package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N810_REST_RQ extends BaseRestBean_CC implements Serializable {

	
	private static final long serialVersionUID = 8019822747111651798L;
	/**
	 * 
	 */
	
	
	String	CUSIDN	;
	String	CALLFROM;
	String  ADOPID;
	
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getCALLFROM() {
		return CALLFROM;
	}
	public void setCALLFROM(String cALLFROM) {
		CALLFROM = cALLFROM;
	}
	
}
