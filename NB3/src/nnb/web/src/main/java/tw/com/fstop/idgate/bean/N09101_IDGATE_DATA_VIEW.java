package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;

public class N09101_IDGATE_DATA_VIEW  extends Base_IDGATE_DATA_VIEW implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3313399258035269929L;

	
	@SerializedName(value = "TRANTITLE")
	private String TRANTITLE;

	@SerializedName(value = "TRANNAME")
	private String TRANNAME;
	
	@SerializedName(value = "SVACN")
	private String SVACN;

	@SerializedName(value = "ACN")
	private String ACN;
	
	@SerializedName(value = "TRNGD")
	private String TRNGD;

	@Override
	public Map<String, String> coverMap(){
		Map<String, String> result = new LinkedHashMap<String, String>();
		result.put("交易名稱", this.TRANTITLE);
		result.put("交易類型", this.TRANNAME);
		result.put("買進公克數", this.TRNGD + "公克");
		result.put("臺幣轉出帳號", this.SVACN);
		result.put("黃金轉入帳號", this.ACN);
		return result;
	}
	

	public String getTRANTITLE() {
		return TRANTITLE;
	}


	public void setTRANTITLE(String tRANTITLE) {
		TRANTITLE = tRANTITLE;
	}


	public String getTRANNAME() {
		return TRANNAME;
	}

	public void setTRANNAME(String tRANNAME) {
		TRANNAME = tRANNAME;
	}

	public String getSVACN() {
		return SVACN;
	}

	public void setSVACN(String sVACN) {
		SVACN = sVACN;
	}

	public String getACN() {
		return ACN;
	}

	public void setACN(String aCN) {
		ACN = aCN;
	}

	public String getTRNGD() {
		return TRNGD;
	}

	public void setTRNGD(String tRNGD) {
		TRNGD = tRNGD;
	}
	
}
