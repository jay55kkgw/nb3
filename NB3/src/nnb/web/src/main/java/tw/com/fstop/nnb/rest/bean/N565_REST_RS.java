package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N565_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5976806177887178981L;

	String CMQTIME;					//查詢時間
	String CMRECNUM;				//查詢筆數
	String CMPERIOD;				//查詢期間
    String __OCCURS;
    String REC_NO;					//筆數
    String USERDATA;				//本次未完資料KEY值
    String ABEND;					//結束代碼

    String QUERYNEXT;
    
    String AMTRREMITCY_1;
    String AMTRREMITCY_2;
    String AMTRREMITCY_3;
    String AMTRREMITCY_4;
    String AMTRREMITCY_5;
    String AMTRREMITCY_6;
    String AMTRREMITCY_7;
    String AMTRREMITCY_8;
    
    String FXSUBAMTRECNUM_1;
    String FXSUBAMTRECNUM_2;
    String FXSUBAMTRECNUM_3;
    String FXSUBAMTRECNUM_4;
    String FXSUBAMTRECNUM_5;
    String FXSUBAMTRECNUM_6;
    String FXSUBAMTRECNUM_7;
    String FXSUBAMTRECNUM_8;
    
    String FXSUBAMT_1;
    String FXSUBAMT_2;
    String FXSUBAMT_3;
    String FXSUBAMT_4;
    String FXSUBAMT_5;
    String FXSUBAMT_6;
    String FXSUBAMT_7;
    String FXSUBAMT_8;
    
	LinkedList<N565_REST_RSDATA> REC;
	
    public String getQUERYNEXT() {
		return QUERYNEXT;
	}
	public void setQUERYNEXT(String qUERYNEXT) {
		QUERYNEXT = qUERYNEXT;
	}
	public String getABEND() {
		return ABEND;
	}
	public void setABEND(String aBEND) {
		ABEND = aBEND;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getCMRECNUM() {
		return CMRECNUM;
	}
	public void setCMRECNUM(String cMRECNUM) {
		CMRECNUM = cMRECNUM;
	}
	public String getCMPERIOD() {
		return CMPERIOD;
	}
	public void setCMPERIOD(String cMPERIOD) {
		CMPERIOD = cMPERIOD;
	}
	public String get__OCCURS() {
		return __OCCURS;
	}
	public void set__OCCURS(String __OCCURS) {
		this.__OCCURS = __OCCURS;
	}
	public String getREC_NO() {
		return REC_NO;
	}
	public void setREC_NO(String rEC_NO) {
		REC_NO = rEC_NO;
	}
	public String getUSERDATA() {
		return USERDATA;
	}
	public void setUSERDATA(String uSERDATA) {
		USERDATA = uSERDATA;
	}
	public LinkedList<N565_REST_RSDATA> getREC() {
		return REC;
	}
	public void setREC(LinkedList<N565_REST_RSDATA> rEC) {
		REC = rEC;
	}
    
}
