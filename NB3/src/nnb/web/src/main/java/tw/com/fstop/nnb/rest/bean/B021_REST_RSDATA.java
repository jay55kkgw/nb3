package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class B021_REST_RSDATA extends BaseRestBean implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5598992973739489572L;
	
	private String O01;	//信託帳號
	private String O02;	//商品代號
	private String O03;	//商品全名
	private String O04;	//計價幣別
	private String O06;	//信託本金
	private String O07;	//庫存面額
	private String O08;	//贖回價格(為0時表示無價格)
	private String O09;	//部贖註記(Y.可部贖/''不可部贖)
	public String getO01() {
		return O01;
	}
	public void setO01(String o01) {
		O01 = o01;
	}
	public String getO02() {
		return O02;
	}
	public void setO02(String o02) {
		O02 = o02;
	}
	public String getO03() {
		return O03.trim();
	}
	public void setO03(String o03) {
		O03 = o03.trim();
	}
	public String getO04() {
		return O04;
	}
	public void setO04(String o04) {
		O04 = o04;
	}
	public String getO06() {
		return O06;
	}
	public void setO06(String o06) {
		O06 = o06;
	}
	public String getO07() {
		return O07;
	}
	public void setO07(String o07) {
		O07 = o07;
	}
	public String getO08() {
		return O08;
	}
	public void setO08(String o08) {
		O08 = o08;
	}
	public String getO09() {
		return O09;
	}
	public void setO09(String o09) {
		O09 = o09;
	}
}
