package tw.com.fstop.nnb.spring.controller;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Map;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;

/*
 * BHO的控制器
 */
@SessionAttributes({SessionUtil.TRANSFER_BHOLOCATION,SessionUtil.TRANSFER_TRANSINACCOUNT,SessionUtil.CREDITCARDPAY_BOHLOCATION,SessionUtil.CREDITCARDPAY__TRANSINACCOUNT,SessionUtil.DEPOSIT_TRANSFER_BHOLOCATION,SessionUtil.DEPOSIT_TRANSFER_TRANSINACCOUNT,SessionUtil.INSTALLMENT_SAVING_BHOLOCATION,SessionUtil.INSTALLMENT_SAVING__TRANSINACCOUNT})
@Controller
public class BHOController{
	private Logger log = LoggerFactory.getLogger(this.getClass().getName());
	
	@Autowired
	private I18n i18n;
	
	@RequestMapping("/getBHO")
	public void getBHO(@RequestParam Map<String,String> requestParam,HttpServletResponse response,Model model){
		log.debug("IN getBHO");

		try{
			// 解決Trust Boundary Violation 

			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam); 
			
			String transferInAccount = okMap.get("transferInAccount");
			log.debug(ESAPIUtil.vaildLog("transferInAccount >> " + transferInAccount));
			String functionName = okMap.get("functionName");
			log.debug(ESAPIUtil.vaildLog("functionName >> " + functionName));
			
		    Vector<String> selectedDigits = new Vector<String>();
		    int[] selectedDigitsArray = new int[3];  
			SecureRandom secureRandom = new SecureRandom();
		    int nextInt = 0;
		    
			//取得須認證之位數(不得重複)
			for(int i=0;i<3;i++){
				while(true){
					nextInt = secureRandom.nextInt(transferInAccount.length());
					
					if(!selectedDigits.contains(Integer.toString(nextInt))){
						selectedDigits.add(Integer.toString(nextInt));
						selectedDigitsArray[i] = nextInt;
						break;	
					}	
				}		
			}
			Arrays.sort(selectedDigitsArray);
			
			int digit1 = selectedDigitsArray[0];
			int digit2 = selectedDigitsArray[1];
			int digit3 = selectedDigitsArray[2];
			
			// BHO三碼
			String aBHO = functionName + "_bholocation";
			log.debug(ESAPIUtil.vaildLog("getBHO.aBHO >> " + aBHO));
			
			String bBHO = transferToHexLocation(digit1) + transferToHexLocation(digit2) + transferToHexLocation(digit3);
			log.debug(ESAPIUtil.vaildLog("getBHO.bBHO >> " + bBHO));
			
			// BHO_標誌位置
			SessionUtil.addAttribute(model, aBHO, bBHO);
			log.debug(ESAPIUtil.vaildLog("getBHO.sBHO. " + aBHO + ": " + SessionUtil.getAttribute(model, aBHO, null)));
			
			// 轉入帳號
			SessionUtil.addAttribute(model, functionName + "_transinaccount", transferInAccount);
			
		    // 生成新圖片
		    int width = 26;
		    int height = 26;
		    
		    BufferedImage bufferedImage = new BufferedImage(width * transferInAccount.length(),height,BufferedImage.TYPE_INT_RGB);
		    
		    for(int i=0;i<transferInAccount.length();i++){    	
		    	int[] imageArray = createImgArray(width,height,transferInAccount.substring(i,i+1),i,selectedDigits);
		    	bufferedImage.setRGB(i * width,0,width,height,imageArray,0,width);
		    }
			response.setContentType("image/jpeg");
			ImageIO.write(bufferedImage,"jpeg",response.getOutputStream());
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getBHO error >> {}",e);
		}
	}
	@RequestMapping(value="/checkBHO",produces={"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult checkBHO(HttpServletRequest request,HttpServletResponse response,@RequestParam Map<String,String> requestParam,Model model){
		log.debug("IN checkBHO");
		
		BaseResult baseResult = new BaseResult();
		baseResult.setResult(Boolean.FALSE);
		baseResult.setMessage(i18n.getMsg("LB.BHO_note"));
		try{
			String keyInBHO = requestParam.get("keyInBHO");
			log.info(ESAPIUtil.vaildLog("keyInBHO >> " + keyInBHO));
			
			String functionName = requestParam.get("functionName");
			log.debug(ESAPIUtil.vaildLog("functionName >> " + functionName));
			
			// BHO_標誌位置
			String BHOLocation = (String)SessionUtil.getAttribute(model,functionName + "_bholocation",null);
			log.info(ESAPIUtil.vaildLog("BHOLocation >> " + BHOLocation));
			
			// 轉入帳號
			String transferInAccount = (String)SessionUtil.getAttribute(model,functionName + "_transinaccount",null);
			log.info(ESAPIUtil.vaildLog("transferInAccount >> " + transferInAccount));
			
			int first = transferToIntPosition(BHOLocation.substring(0,1));
			int second = transferToIntPosition(BHOLocation.substring(1,2));
			int third = transferToIntPosition(BHOLocation.substring(2,3));
			
			// 可能會有英文，所以用大寫來比
			if ( keyInBHO.substring(0,1).toUpperCase().equals(transferInAccount.substring(first,first + 1).toUpperCase()) 
					&& keyInBHO.substring(1,2).toUpperCase().equals(transferInAccount.substring(second,second + 1).toUpperCase()) 
					&& keyInBHO.substring(2,3).toUpperCase().equals(transferInAccount.substring(third,third + 1).toUpperCase()))
			{
				baseResult.setResult(Boolean.TRUE);
				baseResult.setMessage("BHO~"+i18n.getMsg("LB.X1759"));
				
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("checkBHO error >> {}",e);
		}
		return baseResult;
	}
	//設定各組數字的背景與字體顏色
	private int[] createImgArray(int width,int height,String acnoDigitValue,int acnoCurrentIndex,Vector<String> selectedDigits){
		BufferedImage image = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
		Graphics g = image.getGraphics();

		//以下填充背景顏色
		if(selectedDigits.contains(Integer.toString(acnoCurrentIndex))){
			g.setColor(Color.yellow);
		}
		else{
			g.setColor(Color.lightGray);
		}
		g.fillRect(0,0,width,height);

		//設置字體顏色
		g.setColor(Color.BLUE);
		Font font = new Font("Tempus Sans ITC",Font.BOLD,20);
		g.setFont(font);
		g.drawString(acnoDigitValue,5,20);

		//圖像生效
		g.dispose();

		int[] imageArray = new int[width * height];
		imageArray = image.getRGB(0,0,width,height,imageArray,0,width);

		return imageArray;
	}
	//轉成16進位的位置
	private String transferToHexLocation(int location){
		String result = "";
		
		if(location <= 9){
			result = String.valueOf(location);
		}
		else{
			switch(location){
			case 10:
				result = "A";
				break;
			case 11:
				result = "B";
				break;
			case 12:
				result = "C";
				break;
			case 13:
				result = "D";
				break;
			case 14:
				result = "E";
				break;
			case 15:
				result = "F";
				break;
			case 16:
				result = "F";
				break;
			}
		}
		return result;
	}
	//16進位的位置轉成數字
	private int transferToIntPosition(String position){
		int result = 0;
		
		if("A".equals(position) || "B".equals(position) || "C".equals(position) || "D".equals(position) || "E".equals(position) || "F".equals(position)){
			if("A".equals(position)){
				result= 10;
			}
			else if("B".equals(position)){
				result= 11;
			}
			else if("C".equals(position)){
				result= 12;
			}
			else if("D".equals(position)){
				result= 13;
			}
			else if("E".equals(position)){
				result= 14;
			}
			else{
				result= 15;
			}
		}
		else{
			result = Integer.parseInt(position);
		}
		return result;		
	}
}