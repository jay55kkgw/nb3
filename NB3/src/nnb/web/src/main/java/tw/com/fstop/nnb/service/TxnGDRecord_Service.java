package tw.com.fstop.nnb.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.TemporalAccessor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.po.TXNGDRECORD;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.tbb.nnb.dao.TxnGDRecord_Dao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.web.util.StrUtils;

@Service
public class TxnGDRecord_Service extends Base_Service {
	private Logger log = LoggerFactory.getLogger(this.getClass().getName());

	@Autowired
	TxnGDRecord_Dao txngdrecord_dao;

	/**
	 * 查詢黃金交易結果
	 * 
	 * @param 
	 * @return BaseResult
	 */
	public BaseResult query(Map<String, String> reqParam) {
		BaseResult bs = new BaseResult();	
		Boolean flag = false;
		String sdate = reqParam.get("sdate");
		String edate = reqParam.get("edate");
		try {
			String cusidn = reqParam.get("cusidn");
			//驗證統編為空
			if(cusidn==null) {
				bs.setMessage("cusidn不得null");
				throw new Exception();
			}
			//驗證日期為空則改六個月內
			if (StrUtils.isNull(sdate) || StrUtils.isNull(edate)) {
				LocalDateTime now = LocalDateTime.now();
				DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE;
				formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
				reqParam.put("edate", now.format(formatter));
				reqParam.put("sdate", now.plusMonths(-6).format(formatter));
			}else {
			// 檢查日期是否正常
			DateTimeFormatter strToDateFormatter = DateTimeFormatter.ofPattern("yyyyMMdd");
			TemporalAccessor sdate_c = LocalDate.parse(reqParam.get("sdate"), strToDateFormatter);
			TemporalAccessor edate_c = LocalDate.parse(reqParam.get("edate"), strToDateFormatter);
			}	
			//驗證ADOPID為空則把Flag改true 表查全部
			if(StrUtils.isNull(reqParam.get("adopid"))) {
				flag = true;
			}
			//驗證起始日不得大於截止日
			if (Integer.parseInt(reqParam.get("sdate")) > Integer.parseInt(reqParam.get("edate")) || cusidn.isEmpty()) {
				bs.setMessage("期間啟日不得大於期間迄日");
				throw new Exception();
			}
			List<TXNGDRECORD> list  = txngdrecord_dao.findAllByDateRange(reqParam,flag);
			
			bs.setData(list);
			bs.setResult(Boolean.TRUE);
			bs.setMessage("0", "查詢成功");
		} catch (DateTimeParseException e) {
			log.error(ESAPIUtil.vaildLog("sdate="+sdate+",edate="+edate));
			bs.setResult(Boolean.FALSE);
			bs.setMsgCode("2");
			bs.setMessage("日期輸入錯誤");
		} catch (Exception e) {
			log.error("error>>>{}"+e.getMessage());
			bs.setResult(Boolean.FALSE);
			bs.setMsgCode("1");
			if (bs.getMessage() == null) {
				bs.setMessage("查詢失敗");
			}
		}
		return bs;
	}

	/**
	 * 新增黃金交易結果
	 * 
	 * @param
	 * @return BaseResult
	 */
	public BaseResult add(Map<String, String> reqParam1) {		
		BaseResult bs = new BaseResult();
		Map<String, String> reqmap = new HashMap<String, String>();
		Map<String, String> reqParam=new HashMap<String, String>();
		List<String> list = new ArrayList<String>();
		List<String> list2= new ArrayList<String>();
		
		//排除可能給null的欄位 之後如果null給空字串 
		List<String> list3 = new ArrayList<String>(); 
		list3.add("gdexcode");
		list3.add("gdtxamt");
		list3.add("gdprice");
		list3.add("gddiscount");
		list3.add("gdfee1");
		list3.add("gdfee2");
		list3.add("gdtxmails");
		list3.add("gdremail");
		list3.add("gdtitainfo");
		list3.add("gdtotainfo");
		list3.add("gdretxno");
		list3.add("gdcert");
		list3.add("gdweight");
		
		try {	
		
			//把JAP的欄位抓出來轉小寫塞進list
			list = StrUtils.getBeanName(TXNGDRECORD.class);
			for(String str: reqParam1.keySet()) {
				if(list.contains(str.toUpperCase())) {
					reqParam.put(str,reqParam1.get(str));
				}
			}
			for (String str : list) {
				list2.add(str.toLowerCase());				
			}				
			//移除可能給null的欄位 之後另外處理 
			for(String str: list3) {
				list2.remove(str);
			}
			
			//判斷剩下的欄位是否有值(不能空字串 空白字串 null)
			for(String key : list2) {
				if(StrUtils.isNull(reqParam.get(key))) {
					bs.setMessage(key+"不得空值 空字串 Null");
					throw new Exception();
				}
			}
			//另外處理的資料如果null改為空白字串，避免資料庫丟出Exception
			for(String str: list3) {
				if(reqParam.get(str)==null || reqParam.get(str)=="") {
					reqParam.put(str, "");
				}
			}						
			//配合POJO轉大寫作新增
			for (Object obj : reqParam.keySet()) {
				reqmap.put(obj.toString().toUpperCase(), reqParam.get(obj.toString()));
			}	
			
			TXNGDRECORD txngdrecord= CodeUtil.objectCovert(TXNGDRECORD.class, reqmap);
			txngdrecord_dao.save(txngdrecord);
			bs.setResult(Boolean.TRUE);
			bs.setMessage("0", "新增成功");
		} catch (Exception e) {
			log.error("error>>>{}"+e.getMessage());
			bs.setResult(Boolean.FALSE);
			bs.setMsgCode("1");
			if (bs.getMessage() == null) {
				bs.setMessage("新增失敗");
			}
		}
		return bs;
	}
	
}
