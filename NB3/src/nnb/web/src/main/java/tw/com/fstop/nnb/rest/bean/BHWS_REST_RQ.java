package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class BHWS_REST_RQ extends BaseRestBean_CC implements Serializable {

	
	private static final long serialVersionUID = 8019822747111651798L;
	/**
	 * 
	 */
	
	
	private String CUSIDN;
	private String FGPERIOD;
	private String CARDTYPE;
	private String BILL_NO;
	private String USEREMAIL;
	
	private String QUERYTYPE;

	public String getCUSIDN() {
		return CUSIDN;
	}

	public String getFGPERIOD() {
		return FGPERIOD;
	}

	public String getCARDTYPE() {
		return CARDTYPE;
	}

	public String getBILL_NO() {
		return BILL_NO;
	}

	public String getQUERYTYPE() {
		return QUERYTYPE;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}

	public void setFGPERIOD(String fGPERIOD) {
		FGPERIOD = fGPERIOD;
	}

	public void setCARDTYPE(String cARDTYPE) {
		CARDTYPE = cARDTYPE;
	}

	public void setBILL_NO(String bILL_NO) {
		BILL_NO = bILL_NO;
	}

	public void setQUERYTYPE(String qUERYTYPE) {
		QUERYTYPE = qUERYTYPE;
	}

	public String getUSEREMAIL() {
		return USEREMAIL;
	}

	public void setUSEREMAIL(String uSEREMAIL) {
		USEREMAIL = uSEREMAIL;
	}
}
