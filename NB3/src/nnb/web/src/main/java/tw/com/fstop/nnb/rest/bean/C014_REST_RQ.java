package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class C014_REST_RQ extends BaseRestBean_FUND implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7171574783568313593L;
	private String CUSIDN;			// 身份證號
	private String TRANSCODE;		// 基金代碼
	private String CDNO;		// 信託號碼
	private String TRADEDATE;		// 生效日期
	private String FUNDTYPE;		// 交易種類
	private String INTRANSCODE;		// 轉入基金代碼
	
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getTRANSCODE() {
		return TRANSCODE;
	}
	public void setTRANSCODE(String tRANSCODE) {
		TRANSCODE = tRANSCODE;
	}
	public String getTRADEDATE() {
		return TRADEDATE;
	}
	public void setTRADEDATE(String tRADEDATE) {
		TRADEDATE = tRADEDATE;
	}
	public String getFUNDTYPE() {
		return FUNDTYPE;
	}
	public void setFUNDTYPE(String fUNDTYPE) {
		FUNDTYPE = fUNDTYPE;
	}
	public String getINTRANSCODE() {
		return INTRANSCODE;
	}
	public void setINTRANSCODE(String iNTRANSCODE) {
		INTRANSCODE = iNTRANSCODE;
	}
	public String getCDNO() {
		return CDNO;
	}
	public void setCDNO(String cDNO) {
		CDNO = cDNO;
	}
	
}