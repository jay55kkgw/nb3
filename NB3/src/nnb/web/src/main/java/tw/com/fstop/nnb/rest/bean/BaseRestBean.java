package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.web.util.ConfingManager;

public class BaseRestBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7558266676532828276L;
	
	private String LOGINTYPE ="NB";
	private String TRXCOD;
	private String FILL01;
	private String TXID;
	private String VERSION;
	private String msgCode;
	private String msgName;
	private String TOPMSG;
	private String isTxnlLog;
	

	

	
	
	public String getIsTxnlLog() {
		return isTxnlLog;
	}
	public void setIsTxnlLog(String isTxnlLog) {
		this.isTxnlLog = isTxnlLog;
	}
	public String getLOGINTYPE() {
		return LOGINTYPE;
	}
	public void setLOGINTYPE(String lOGINTYPE) {
		LOGINTYPE = lOGINTYPE;
	}
	public String getTRXCOD() {
		return TRXCOD;
	}
	public void setTRXCOD(String tRXCOD) {
		TRXCOD = tRXCOD;
	}
	public String getFILL01() {
		return FILL01;
	}
	public void setFILL01(String fILL01) {
		FILL01 = fILL01;
	}
	public String getTXID() {
		return TXID;
	}
	public void setTXID(String tXID) {
		TXID = tXID;
	}
	public String getVERSION() {
		return VERSION;
	}
	public void setVERSION(String vERSION) {
		VERSION = vERSION;
	}
	
	public String getMsgCode() {
		return msgCode;
	}
	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}
	public String getMsgName() {
		return msgName;
	}
	public void setMsgName(String msgName) {
		this.msgName = msgName;
	}	
	public String getTOPMSG() {
		return TOPMSG;
	}
	public void setTOPMSG(String tOPMSG) {
		TOPMSG = tOPMSG;
	}
	
	
	
	
	

}
