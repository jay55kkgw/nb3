package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N920_REST_RSDATA implements Serializable {

	private static final long serialVersionUID = 5325764787100029115L;

	private String TRFLAG; //轉出帳號註記

	private String ATTRBUE;//屬性

	private String ACN;//帳號

	public String getTRFLAG() {
		return TRFLAG;
	}

	public void setTRFLAG(String tRFLAG) {
		TRFLAG = tRFLAG;
	}

	public String getATTRBUE() {
		return ATTRBUE;
	}

	public void setATTRBUE(String aTTRBUE) {
		ATTRBUE = aTTRBUE;
	}

	public String getACN() {
		return ACN;
	}

	public void setACN(String aCN) {
		ACN = aCN;
	}

}
