package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * 信用卡查詢RQ
 */
public class N812_REST_RQ extends BaseRestBean_CC implements Serializable{
	private static final long serialVersionUID = -1646749640736846151L;

	private String CUSIDN;

	public String getCUSIDN(){
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN){
		CUSIDN = cUSIDN;
	}
}