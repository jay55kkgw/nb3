package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * 
 * 功能說明 :F003_外匯匯入匯款線上解款
 *
 */
public class F003T_REST_RQ extends BaseRestBean_FX implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4342681606770114457L;

	
	private String ADOPID;		// 電文代號

	private String iSeqNo; // iSeqNo

	private String pkcs7Sign;// IKEY

	private String jsondc;// IKEY

	private String PINNEW;// 網路銀行密碼（新）SSL 用

	private String BGROENO; // 人工議價編號

	private String BENACC; // 轉入帳號

	private String BENNAME;// 收款人名稱

	private String BENTYPE; // 受款人身分別

	private String CNTRY;// 交易國別

	private String CURAMT; // 議價金額 9(13)V9(2)

	private String CUSIDN; // 付款人統一編號

	private String CUSTID;// 收款人統編

	private String CUSTYPE; // 付款人身份別

	private String ORDNAME;// 付款人名稱

	private String ORDACC;// //轉出帳號

	private String REFNO;//// 匯入匯款交易編號

	private String RETCCY;// 轉入幣別(解款幣別)

	private String REMITAMT;// 轉入金額(匯入金額) 

	private String SRCFUND; // 匯款分類編號

	private String TXCCY;/// 轉出幣別(匯入幣別)

	private String TXAMT;//// 轉出金額(匯入金額)

	private String TXDATE;// 建檔日期

	private String VALDATE;// 生效日

	private String CMTRANPAGE;

	private String PGMID;// 程式名稱

	private String ETY_LVL;// ENTRY LEVEL

	private String BRH_CODE;// 分行代碼 x(4)

	private String HOST_TIME;// HOST時間HHMMSS

	private String DEPT;// 業務別

	private String TXBRH;// 承作行代碼

	private String ACBRH;// 實績行代碼

	private String ORDAD1;// 匯款人住址1

	private String ORDAD2;// 匯款人住址2

	private String ORDAD3;

	private String BENAD1;

	private String BENAD2;

	private String BENAD3;

	private String RCVBANK;

	private String RCVAD1;// 地址

	private String RCVAD2;

	private String RCVAD3;

	private String DETCHG;

	private String ORDMEMO1;

	private String ORDMEMO2;

	private String ORDMEMO3;

	private String ORDMEMO4;

	//IDGATE
	private String sessionID;
	private String idgateID;
	private String txnID;
	
	public String getADOPID()	{
		return ADOPID;
	}

	public void setADOPID(String aDOPID)
	{
		ADOPID = aDOPID;
	}

	public String getiSeqNo()
	{
		return iSeqNo;
	}

	public void setiSeqNo(String iSeqNo)
	{
		this.iSeqNo = iSeqNo;
	}

	public String getPkcs7Sign()
	{
		return pkcs7Sign;
	}

	public void setPkcs7Sign(String pkcs7Sign)
	{
		this.pkcs7Sign = pkcs7Sign;
	}

	public String getJsondc()
	{
		return jsondc;
	}

	public void setJsondc(String jsondc)
	{
		this.jsondc = jsondc;
	}

	public String getPINNEW()
	{
		return PINNEW;
	}

	public void setPINNEW(String pINNEW)
	{
		PINNEW = pINNEW;
	}

	public String getCUSIDN()
	{
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN)
	{
		CUSIDN = cUSIDN;
	}

	public String getCUSTID()
	{
		return CUSTID;
	}

	public void setCUSTID(String cUSTID)
	{
		CUSTID = cUSTID;
	}

	public String getBGROENO()
	{
		return BGROENO;
	}

	public void setBGROENO(String bGROENO)
	{
		BGROENO = bGROENO;
	}

	public String getBENACC()
	{
		return BENACC;
	}

	public void setBENACC(String bENACC)
	{
		BENACC = bENACC;
	}

	public String getORDNAME()
	{
		return ORDNAME;
	}

	public void setORDNAME(String oRDNAME)
	{
		ORDNAME = oRDNAME;
	}

	public String getBENNAME()
	{
		return BENNAME;
	}

	public void setBENNAME(String bENNAME)
	{
		BENNAME = bENNAME;
	}

	public String getCUSTYPE()
	{
		return CUSTYPE;
	}

	public void setCUSTYPE(String cUSTYPE)
	{
		CUSTYPE = cUSTYPE;
	}

	public String getBENTYPE()
	{
		return BENTYPE;
	}

	public void setBENTYPE(String bENTYPE)
	{
		BENTYPE = bENTYPE;
	}

	public String getTXCCY()
	{
		return TXCCY;
	}

	public void setTXCCY(String tXCCY)
	{
		TXCCY = tXCCY;
	}

	public String getRETCCY()
	{
		return RETCCY;
	}

	public void setRETCCY(String rETCCY)
	{
		RETCCY = rETCCY;
	}

	public String getTXAMT()
	{
		return TXAMT;
	}

	public void setTXAMT(String tXAMT)
	{
		TXAMT = tXAMT;
	}

	public String getREFNO()
	{
		return REFNO;
	}

	public void setREFNO(String rEFNO)
	{
		REFNO = rEFNO;
	}

	public String getORDACC()
	{
		return ORDACC;
	}

	public void setORDACC(String oRDACC)
	{
		ORDACC = oRDACC;
	}

	public String getCNTRY()
	{
		return CNTRY;
	}

	public void setCNTRY(String cNTRY)
	{
		CNTRY = cNTRY;
	}

	public String getTXDATE()
	{
		return TXDATE;
	}

	public void setTXDATE(String tXDATE)
	{
		TXDATE = tXDATE;
	}

	public String getVALDATE()
	{
		return VALDATE;
	}

	public void setVALDATE(String vALDATE)
	{
		VALDATE = vALDATE;
	}

	public String getSRCFUND()
	{
		return SRCFUND;
	}

	public void setSRCFUND(String sRCFUND)
	{
		SRCFUND = sRCFUND;
	}

	public String getCURAMT()
	{
		return CURAMT;
	}

	public void setCURAMT(String cURAMT)
	{
		CURAMT = cURAMT;
	}

	public String getREMITAMT()
	{
		return REMITAMT;
	}

	public void setREMITAMT(String rEMITAMT)
	{
		REMITAMT = rEMITAMT;
	}

	public String getCMTRANPAGE()
	{
		return CMTRANPAGE;
	}

	public void setCMTRANPAGE(String cMTRANPAGE)
	{
		CMTRANPAGE = cMTRANPAGE;
	}

	public String getPGMID()
	{
		return PGMID;
	}

	public void setPGMID(String pGMID)
	{
		PGMID = pGMID;
	}

	public String getETY_LVL()
	{
		return ETY_LVL;
	}

	public void setETY_LVL(String eTY_LVL)
	{
		ETY_LVL = eTY_LVL;
	}

	public String getBRH_CODE()
	{
		return BRH_CODE;
	}

	public void setBRH_CODE(String bRH_CODE)
	{
		BRH_CODE = bRH_CODE;
	}

	public String getHOST_TIME()
	{
		return HOST_TIME;
	}

	public void setHOST_TIME(String hOST_TIME)
	{
		HOST_TIME = hOST_TIME;
	}

	public String getDEPT()
	{
		return DEPT;
	}

	public void setDEPT(String dEPT)
	{
		DEPT = dEPT;
	}

	public String getTXBRH()
	{
		return TXBRH;
	}

	public void setTXBRH(String tXBRH)
	{
		TXBRH = tXBRH;
	}

	public String getACBRH()
	{
		return ACBRH;
	}

	public void setACBRH(String aCBRH)
	{
		ACBRH = aCBRH;
	}

	public String getORDAD1()
	{
		return ORDAD1;
	}

	public void setORDAD1(String oRDAD1)
	{
		ORDAD1 = oRDAD1;
	}

	public String getORDAD2()
	{
		return ORDAD2;
	}

	public void setORDAD2(String oRDAD2)
	{
		ORDAD2 = oRDAD2;
	}

	public String getORDAD3()
	{
		return ORDAD3;
	}

	public void setORDAD3(String oRDAD3)
	{
		ORDAD3 = oRDAD3;
	}

	public String getBENAD1()
	{
		return BENAD1;
	}

	public void setBENAD1(String bENAD1)
	{
		BENAD1 = bENAD1;
	}

	public String getBENAD2()
	{
		return BENAD2;
	}

	public void setBENAD2(String bENAD2)
	{
		BENAD2 = bENAD2;
	}

	public String getBENAD3()
	{
		return BENAD3;
	}

	public void setBENAD3(String bENAD3)
	{
		BENAD3 = bENAD3;
	}

	public String getRCVBANK()
	{
		return RCVBANK;
	}

	public void setRCVBANK(String rCVBANK)
	{
		RCVBANK = rCVBANK;
	}

	public String getRCVAD1()
	{
		return RCVAD1;
	}

	public void setRCVAD1(String rCVAD1)
	{
		RCVAD1 = rCVAD1;
	}

	public String getRCVAD2()
	{
		return RCVAD2;
	}

	public void setRCVAD2(String rCVAD2)
	{
		RCVAD2 = rCVAD2;
	}

	public String getRCVAD3()
	{
		return RCVAD3;
	}

	public void setRCVAD3(String rCVAD3)
	{
		RCVAD3 = rCVAD3;
	}

	public String getDETCHG()
	{
		return DETCHG;
	}

	public void setDETCHG(String dETCHG)
	{
		DETCHG = dETCHG;
	}

	public String getORDMEMO1()
	{
		return ORDMEMO1;
	}

	public void setORDMEMO1(String oRDMEMO1)
	{
		ORDMEMO1 = oRDMEMO1;
	}

	public String getORDMEMO2()
	{
		return ORDMEMO2;
	}

	public void setORDMEMO2(String oRDMEMO2)
	{
		ORDMEMO2 = oRDMEMO2;
	}

	public String getORDMEMO3()
	{
		return ORDMEMO3;
	}

	public void setORDMEMO3(String oRDMEMO3)
	{
		ORDMEMO3 = oRDMEMO3;
	}

	public String getORDMEMO4()
	{
		return ORDMEMO4;
	}

	public void setORDMEMO4(String oRDMEMO4)
	{
		ORDMEMO4 = oRDMEMO4;
	}

	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public String getIdgateID() {
		return idgateID;
	}

	public void setIdgateID(String idgateID) {
		this.idgateID = idgateID;
	}

	public String getTxnID() {
		return txnID;
	}

	public void setTxnID(String txnID) {
		this.txnID = txnID;
	}

}
