package tw.com.fstop.nnb.service;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import com.codahale.metrics.MetricRegistryListener.Base;

import fstop.orm.po.ADMMSGCODE;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.common.ResultCode;
import tw.com.fstop.idgate.bean.N960_BRHACC_REST_RQ;
import tw.com.fstop.idgate.bean.N960_BRHACC_REST_RS;
import tw.com.fstop.nnb.rest.bean.*;
import tw.com.fstop.tbb.nnb.dao.AdmMsgCodeDao;
import tw.com.fstop.tbb.nnb.util.StrUtils;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.ConfingManager;
import tw.com.fstop.web.util.RESTUtil;
import tw.com.fstop.web.util.RESTfulClientUtil;

@Service
public class Base_Service {
    Logger log = LoggerFactory.getLogger(this.getClass());
    // KEY:登入
    public static final String LOGIN = "login";
    // KEY:臺幣存款帳號
    public static final String ACNO = "acno";
    // KEY:支存帳號
    public static final String CHECK_ACNO = "check_acno";
    // KEY:臺幣轉出帳號
    public static final String OUT_ACNO = "out_acno";
    // KEY:臺幣轉入帳號
    public static final String IN_ACNO = "in_acno";
    // KEY:黃金存摺帳號
    public static final String GOLD_ACNO = "gold_acno";
    // KEY:外幣轉出帳號/手續費帳號
    public static final String FX_OUT_ACNO = "fx_out_acno";
    // KEY:外幣定存轉出帳號
    public static final String FX_OUT_DEP_ACNO = "fx_out_dep_acno";
    // KEY:線上申請外匯存款帳戶結清銷戶
    public static final String CLOSING_FCY_ACCOUNT = "closing_fcy_account";
    // KEY:線上申請外匯存款帳戶結清銷戶
    public static final String CLOSING_FCY_ACCOUNT_IN = "closing_fcy_account_in";
    // KEY:線上申請外匯存款帳戶結清銷戶轉入台幣帳號
    public static final String CLOSING_FCY_ACCOUNT_AGREEACNOS = "closing_fcy_account_agreeacnos";
    // KEY:線上申請外匯存款帳戶結清銷戶轉入外幣
    public static final String CLOSING_FCY_ACCOUNT_ACNOS1 = "closing_fcy_account_acnos1";
    // KEY:輕鬆理財戶帳號
    public static final String MANAGEMENT = "management";
    // KEY:N420電文的n420
    public static final String N420_ACNO = "n420_acno";
    // KEY:N420電文的n078
    public static final String N078_ACNO = "n078_acno";
    // KEY:N420電文的n077
    public static final String N077_ACNO = "n077_acno";
    // KEY:N283電文的call
    public static final String N283_ACNO = "n283_acno";
    // KEY:N320電文的call
    public static final String N320_ACNO = "n320_acno";
    // KEY:N997電文的call
    public static final String N997_ACNO = "n997_acno";
    // KEY:N283電文的取消
    public static final String N283_CANCEL = "n283_cancel";
    // KEY:N530電文的n530
    public static final String N530_ACNO = "n530_acno";
    // KEY:N530電文的n177
    public static final String N177_ACNO = "n177_acno";
    // KEY:N530電文的n178
    public static final String N178_ACNO = "n178_acno";
    // KEY:N076 得到清單
    public static final String N076QUERYLLIST = "N076";
    // KEY:N076 發送交易電文
    public static final String WSN076ACTION = "N076_1";
    // KEY:N079 得到解約清單
    public static final String WSN079QUERYCANCELLIST = "N079";
    // KEY:N079 得到解約資料電文
    public static final String WSN079GETCANCELDATA = "N079_1";
    // KEY:N079 發送解約交易電文
    public static final String WSN079ACTION = "N079_2";
    // KEY:N108電文的call
    public static final String N108_ACNO = "n108_acno";
    // KEY:N108電文的取消
    public static final String N108_CANCEL = "n108_cancel";
    // KEY:N520電文的acn
    public static final String N520_ACNO = "n520_acno";
    // KEY:N520電文的call
    public static final String N520_CALL = "n520_call";
    // KEY:N175 外匯定存查詢
    public static final String WSN175GTILIST = "getList";
    // KEY:N175 外匯定存解約前置電文資料
    public static final String WSN175BEFOROUT = "beforN175";
    // KEY:N175 執行外匯定存解約
    public static final String WSN175ACTION = "actionN175";
    // KEY:N175 執行外幣定存解約(預約)
    public static final String WSN175ACTIONS = "actionN175S";
    // KEY:N178 得到外匯定存單到期清單
    public static final String WSN178GETACNLIST = "getACNList";
    // KEY:N178 得到外匯定存單到期資料
    public static final String WSN178BEFORNOUT = "beforN178";
    // KEY:N178 發送外匯定存單到期
    public static final String WSN178ACTION = "actionN178";
    // KEY:N565 外匯匯入匯款查詢
    public static final String INWARD_REMITTANC = "INWARD_REMITTANC";
    // KEY:N870 中央登錄債券餘額
    public static final String BOND_BOLANCE = "BOND_BOLANCE";
    // KEY:N650 輕鬆理財自動申購基金明細
    public static final String PURCHASE_FUND_DETAILS = "PURCHASE_FUND_DETAILS";
    // KEY:綜合存款帳戶
    public static final String COLLATER = "collater";
    // KEY:常用帳戶設定
    public static final String ACCSET = "accset";
    // KEY:N921 外幣定存到期或解約轉入帳號
    public static final String FX_DEPOSIT = "FX_DEPOSIT";
    // KEY:N920 外幣定存到期或解約轉入帳號_N920
    public static final String FX_DEPOSIT_N920 = "FX_DEPOSIT_N920";
    // KEY:N921 外匯結購售/轉帳
    public static final String FX_TRANSFER = "FX_TRANSFER";
    // KEY:晶片卡轉帳交易帳號設定
    public static final String TRANSFER_RECORD = "TRANSFER_RECORD";
    // 取得臺幣定存轉帳之約定/常用非約定帳號 臺幣定存使用
    public static final String CERTAGREEACNOLIST = "CERTAGREEACNOLIST";
    // 轉入外匯綜存定存的約定轉入帳號
    public static final String FX_DEPOSIT_AGREEACNOLIST = "FX_DEPOSIT_AGREEACNOLIST";
    // 黃金存摺帳號
    public static final String GOLD_RESERVATION_QOERY_N920 = "GOLD_RESERVATION_QOERY_N920";
    // 黃金存摺帳號
    public static final String GOLD_RESERVATION_QOERY = "GOLD_RESERVATION_QOERY";
    // N920 外幣活期性存款帳戶
    public static final String FX_DEMAND_DEPOSIT_ACCOUNT = "FX_DEMAND_DEPOSIT_ACCOUNT";
    // 期貨帳號
    public static final String FUTURES_DEPOSIT_ACNO = "futures_deposit_acno";
    // 其他費用轉入帳號
    public static final String OTHER_FEE_ACNO = "other_fee_acno";
    // 新臺幣存款帳戶結清銷戶
    public static final String CLOSING_TW_ACCOUNT = "closing_tw_account";
    // 存款餘額申請帳號
    public static final String APPLYACNO = "applyacno";
    // 手續費扣帳帳號
    public static final String FEEACNO = "feeacno";
    // 全國繳線上約定平台帳戶
    public static final String EBILLAPPLYACNO = "ebillapplyacno";
    @Autowired
    private AdmMsgCodeDao admMsgCodeDao;
    
    @Autowired public RESTUtil restutil;
    
    @Autowired
    public DaoService daoService;
//    
//    public BaseResult N110_REST_TEST(String cusidn, String acn, String ms_Channel) {
//        log.trace("N110_REST_TEST");
//        log.trace("cusidn>>{}", cusidn);
//        BaseResult bs = null;
//        N110_REST_RQ rq = null;
//        N110_REST_RS rs = null;
//        try {
//            bs = new BaseResult();
//            // call REST API
//            rq = new N110_REST_RQ();
//            // if(StrUtil.isNotEmpty(ms_Channel)) {
//            // rq.setMs_Channel(ms_Channel);
//            // }
//            rq.setCUSIDN(cusidn);
//            rq.setACN(acn);
//            rs = restutil.send(rq, N110_REST_RS.class, rq.getAPI_Name(rq.getClass(), ms_Channel));
//            // rs = restutil.send(rq, N110_REST_RS.class,
//            // rq.getAPI_Name(rq.getClass()));
//            log.trace("N110_REST_RS>>{}", rs);
//            if (rs != null) {
//                CodeUtil.convert2BaseResult(bs, rs);
//                
//            } else {
//                log.error("N110_REST_RS is null");
//                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
//                
//            }
//        } catch (Exception e) {
//            log.error("", e);
//            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
//            
//        }
//
//        this.getMessageByMsgCode(bs);
//        return bs;
//    }
    
    

    /**
     * AML驗證
     * 
     * @param NAME
     * @param BIRTHDY
     * @return TRUE/FALSE
     */
    public BaseResult AML_REST(Map<String, String> reqParam, String ms_Channel) {
        log.trace("AML_REST");
        BaseResult bs = null;
        AML_REST_RQ rq = null;
        AML_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new AML_REST_RQ();
            reqParam.put("ADOPIDAML", reqParam.get("ADOPID"));
            rq = CodeUtil.objectCovert(AML_REST_RQ.class, reqParam);
            rs = restutil.send(rq, AML_REST_RS.class, rq.getAPI_Name(rq.getClass(), ms_Channel));
            log.trace(ESAPIUtil.vaildLog("AML_REST_RS>>{}"+rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("AML_REST is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 基本資料查詢
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */
    public BaseResult A106_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("A106_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        A106_REST_RQ rq = null;
        A106_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new A106_REST_RQ();
            rq = CodeUtil.objectCovert(A106_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);
            rq.setPARAM1(cusidn);
            rs = restutil.send(rq, A106_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("A106_REST_RS>>{}"+rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("A106_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }
    
    /**
     * 海外債券餘額及損益查詢(結果頁)
     * 
     * @param cusidn
     * @param reqParam
     * @param ms_Channel TODO
     * @return
     */
    public BaseResult B012_REST(String cusidn, Map<String, String> reqParam, String ms_Channel) {
        log.trace("B012_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        B012_REST_RQ rq = null;
        B012_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new B012_REST_RQ();
            rq.setCUSIDN(cusidn);
            if(reqParam.containsKey("isTxnlLog"))
            	rq.setIsTxnlLog(reqParam.get("isTxnlLog"));
            rs = restutil.send(rq, B012_REST_RS.class, rq.getAPI_Name(rq.getClass(), ms_Channel));
            // rs = restutil.send(rq, B012_REST_RS.class,
            // rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("B012_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("B012_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 海外債券交易查詢
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */
    public BaseResult B013_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("B013_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        B013_REST_RQ rq = null;
        B013_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new B013_REST_RQ();
            rq = CodeUtil.objectCovert(B013_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);
            rs = restutil.send(rq, B013_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("B013_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("B013_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 海外債券配息查詢
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */
    public BaseResult B014_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("B014_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        B014_REST_RQ rq = null;
        B014_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new B014_REST_RQ();
            rq = CodeUtil.objectCovert(B014_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);
            rs = restutil.send(rq, B014_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("B014_REST_RS>>{}"+rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("B014_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 海外債券歷史查詢
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */
    public BaseResult B015_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("B015_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        B015_REST_RQ rq = null;
        B015_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new B015_REST_RQ();
            rq = CodeUtil.objectCovert(B015_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);
            rs = restutil.send(rq, B015_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("B015_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("B015_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 變更電話/通訊地址
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */
    public BaseResult N100_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N100_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N100_REST_RQ rq = null;
        N100_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N100_REST_RQ();
            rq = CodeUtil.objectCovert(N100_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);
            rs = restutil.send(rq, N100_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N100_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N100_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 海外債券餘額及損益查詢(結果頁)
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */
    public BaseResult B106_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("B106_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        B106_REST_RQ rq = null;
        B106_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new B106_REST_RQ();
            rq = CodeUtil.objectCovert(B106_REST_RQ.class, reqParam);
            rs = restutil.send(rq, B106_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("B106_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("B106_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * C111定期投資約定變更
     * 
     * @param reqParam
     * @return
     */
    public BaseResult C111_REST(Map<String, String> reqParam) {
        log.trace("C111_REST Start~~~~");
        BaseResult bs = null;
        C111_REST_RQ rq = null;
        C111_REST_RS rs = null;

        try {
            bs = new BaseResult();
            // 組上行
            rq = new C111_REST_RQ();
            rq = CodeUtil.objectCovert(C111_REST_RQ.class, reqParam);
            // 取得回應
            rs = restutil.send(rq, C111_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("C111_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("C111_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("C111_REST_RS error ", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * C116停損停利通知設定
     * 
     * @param reqParam
     * @return
     */
    public BaseResult C116_REST(Map<String, String> reqParam) {
        log.trace("C116_REST Start~~~~");
        BaseResult bs = null;
        C116_REST_RQ rq = null;
        C116_REST_RS rs = null;

        try {
            bs = new BaseResult();
            // 組上行
            rq = new C116_REST_RQ();
            rq = CodeUtil.objectCovert(C116_REST_RQ.class, reqParam);
            // 取得回應
            rs = restutil.send(rq, C116_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("C116_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("C116_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("C116_REST_RS error ", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * C117停損停利通知設定
     * 
     * @param reqParam
     * @return
     */
    public BaseResult C117_REST(Map<String, String> reqParam) {
        log.trace("C117_REST Start~~~~");
        BaseResult bs = null;
        C117_REST_RQ rq = null;
        C117_REST_RS rs = null;

        try {
            bs = new BaseResult();
            // 組上行
            rq = new C117_REST_RQ();
            rq = CodeUtil.objectCovert(C117_REST_RQ.class, reqParam);
            // 取得回應
            rs = restutil.send(rq, C117_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("C117_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("C117_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("C117_REST_RS error ", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * C118SMART FUND自動贖回設定
     * 
     * @param reqParam
     * @return
     */
    public BaseResult C118_REST(Map<String, String> reqParam) {
        log.trace("C118_REST Start~~~~");
        BaseResult bs = null;
        C118_REST_RQ rq = null;
        C118_REST_RS rs = null;

        try {
            bs = new BaseResult();
            // 組上行
            rq = new C118_REST_RQ();
            rq = CodeUtil.objectCovert(C118_REST_RQ.class, reqParam);
            // 取得回應
            rs = restutil.send(rq, C118_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("C118_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("C118_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("C118_REST_RS error ", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * C118SMART FUND自動贖回設定
     * 
     * @param reqParam
     * @return
     */
    public BaseResult C119_REST(Map<String, String> reqParam) {
        log.trace("C119_REST Start~~~~");
        BaseResult bs = null;
        C119_REST_RQ rq = null;
        C119_REST_RS rs = null;

        try {
            bs = new BaseResult();
            // 組上行
            rq = new C119_REST_RQ();
            rq = CodeUtil.objectCovert(C119_REST_RQ.class, reqParam);
            // 取得回應
            rs = restutil.send(rq, C119_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("C119_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("C119_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("C119_REST_RS error ", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * F001T外匯結購售/轉帳 即時
     * 
     * @param sessionId
     * @return
     */
    public BaseResult F001T_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("F001T_REST");
        BaseResult bs = null;
        F001T_REST_RQ rq = null;
        F001T_REST_RS rs = null;

        try {
            bs = new BaseResult();

            rq = new F001T_REST_RQ();
            rq = CodeUtil.objectCovert(F001T_REST_RQ.class, reqParam);
            rq.setADTXACNO(rq.getCUSTACC()); 	// 轉出帳號  ( TABLE此欄位原是放轉入帳號，舊網銀外匯轉帳卻放轉出帳號，比照舊網銀 )
            rq.setADSVBH("050"); 				// 轉入服務銀行 
            rq.setADREQTYPE(""); 				// 預約>>台幣:S，外幣:B ，其他就給空字串 
//            rq.setFGTXWAY(""); 				// 交易機制 ex:0=SSL ，1=IKEY 
//            rq.setADTXAMT(""); 				// 轉出金額
//            rq.setADCURRENCY(""); 			// 轉出幣別 
            rq.setADAGREEF("1"); 				// 轉入帳號約定或非約定註記>>0:非約定，1:約定 ( 外匯轉帳目前規定不能做非約定，故皆為1 )

            rs = restutil.send(rq, F001T_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("F001T_REST_RS>>{}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("F001T_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }

        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * F001R外匯結購售/轉帳 即時
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */
    public BaseResult F001R_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("F001R_REST");
        BaseResult bs = null;
        F001R_REST_RQ rq = null;
        F001R_REST_RS rs = null;

        try {
            bs = new BaseResult();

            rq = new F001R_REST_RQ();
            rq = CodeUtil.objectCovert(F001R_REST_RQ.class, reqParam);
            rq.setADTXACNO(rq.getCUSTACC()); 	// 轉出帳號  ( TABLE此欄位原是放轉入帳號，舊網銀外匯轉帳卻放轉出帳號，比照舊網銀 )
            rq.setADSVBH("050"); 				// 轉入服務銀行 
            rq.setADREQTYPE(""); 				// 預約>>台幣:S，外幣:B ，其他就給空字串 
//            rq.setFGTXWAY(""); 				// 交易機制 ex:0=SSL ，1=IKEY 
//            rq.setADTXAMT(""); 				// 轉出金額
//            rq.setADCURRENCY(""); 			// 轉出幣別 
            rq.setADAGREEF("1"); 				// 轉入帳號約定或非約定註記>>0:非約定，1:約定 ( 外匯轉帳目前規定不能做非約定，故皆為1 )
            
            rs = restutil.send(rq, F001R_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("F001R_REST_RS>>{}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("F001R_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }

        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * F001R外匯結購售/轉帳 即時
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */
    public BaseResult F001S_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("F001S_REST");
        BaseResult bs = null;
        F001S_REST_RQ rq = null;
        F001S_REST_RS rs = null;

        try {
            bs = new BaseResult();

            rq = new F001S_REST_RQ();
            rq = CodeUtil.objectCovert(F001S_REST_RQ.class, reqParam);
            rq.setADTXACNO(rq.getCUSTACC()); 	// 轉出帳號  ( TABLE此欄位原是放轉入帳號，舊網銀外匯轉帳卻放轉出帳號，比照舊網銀 )
            rq.setADSVBH("050"); 				// 轉入服務銀行 
            rq.setADREQTYPE("S"); 				// 預約:S，批次:B 
            rq.setADAGREEF("1"); 				// 轉入帳號約定或非約定註記>>0:非約定，1:約定 ( 外匯轉帳目前規定不能做非約定，故皆為1 )
            
            rs = restutil.send(rq, F001S_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("F001S_REST_RS>>{}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("F001S_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }

        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 外匯匯出匯款-即時
     * 
     * F002R_REST
     * 
     * @param reqParam
     * 
     */
    public BaseResult F002R_REST(Map<String, String> reqParam) {
        log.trace("F002R_REST_RS start ~~");
        BaseResult bs = null;
        F002R_REST_RQ rq = null;
        F002R_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new F002R_REST_RQ();
            rq = CodeUtil.objectCovert(F002R_REST_RQ.class, reqParam);
            rs = restutil.send(rq, F002R_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("F002R_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("F002R_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("F002R_REST_RS Exception", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 外匯匯出匯款-即時
     * 
     * @param reqParam
     * @return
     */
    public BaseResult F002T_REST(Map<String, String> reqParam) {
        log.trace("F002T_REST start ~~");
        BaseResult bs = null;
        F002T_REST_RQ rq = null;
        F002T_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new F002T_REST_RQ();
            rq = CodeUtil.objectCovert(F002T_REST_RQ.class, reqParam);
            rq.setADOPID("F002");
            rs = restutil.send(rq, F002T_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("F002T_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("F002T_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("F002T_REST_RS Exception", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 外匯匯出匯款-預約
     * 
     * F002S_REST
     * 
     * @param reqParam
     * 
     */
    public BaseResult F002S_REST(Map<String, String> reqParam) {
        log.trace("F002S_REST start ~~");
        BaseResult bs = null;
        F002S_REST_RQ rq = null;
        F002S_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new F002S_REST_RQ();
            rq = CodeUtil.objectCovert(F002S_REST_RQ.class, reqParam);
            rq.setADOPID("F002");
            rq.setADREQTYPE("S");
            rs = restutil.send(rq, F002S_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("F002S_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("F002S_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("F002S_REST_RS Exception", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 外匯匯入匯款線上解款清單
     * 
     * F013_REST
     * 
     * @param cusidn
     * @return
     */
    public BaseResult F013_REST(String cusidn) {
        log.trace("F013_REST START!!!");
        BaseResult bs = null;
        F013_REST_RQ rq = null;
        F013_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new F013_REST_RQ();
            rq.setCUSIDN(cusidn);
            rs = restutil.send(rq, F013_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("F013_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("F013_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 功能說明 :線上申請外匯存款帳戶結清銷戶結清資料
     * 
     * F015_REST
     * 
     * @param cusidn
     * @return
     */
    public BaseResult F015_REST(Map<String, String> reqParam) {
        log.trace("F015_REST START!!!");
        BaseResult bs = null;
        F015_REST_RQ rq = null;
        F015_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new F015_REST_RQ();
            rq = CodeUtil.objectCovert(F015_REST_RQ.class, reqParam);
            rs = restutil.send(rq, F015_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("F015_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("F015_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 線上活存幣別結清
     * 
     * F017_REST
     * 
     * @param reqParam
     * 
     */
    public BaseResult F017_REST(Map<String, String> reqParam) {
        log.trace("F017_REST start ~~");
        BaseResult bs = null;
        F017_REST_RQ rq = null;
        F017_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new F017_REST_RQ();
            rq = CodeUtil.objectCovert(F017_REST_RQ.class, reqParam);
            rs = restutil.send(rq, F017_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("F017_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("F017_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("F017_REST_RS Exception", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 線上活存幣別結清
     * 
     * F017R_REST
     * 
     * @param reqParam
     * 
     */
    public BaseResult F017R_REST(Map<String, String> reqParam) {
        log.trace("F017R_REST start ~~");
        BaseResult bs = null;
        F017R_REST_RQ rq = null;
        F017R_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new F017R_REST_RQ();
            rq = CodeUtil.objectCovert(F017R_REST_RQ.class, reqParam);
            rs = restutil.send(rq, F017R_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("F017R_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("F017R_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("F017R_REST_RS Exception", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 線上活存幣別結清
     * 
     * F017T_REST
     * 
     * @param reqParam
     * 
     */
    public BaseResult F017T_REST(Map<String, String> reqParam) {
        log.trace("F017T_REST start ~~");
        BaseResult bs = null;
        F017T_REST_RQ rq = null;
        F017T_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new F017T_REST_RQ();
            rq = CodeUtil.objectCovert(F017T_REST_RQ.class, reqParam);
            rs = restutil.send(rq, F017T_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("F017T_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("F017T_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("F017T_REST_RS Exception", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 外匯匯入匯款線上解款
     * 
     * F003R_REST
     * 
     * @param reqParam
     * 
     */
    public BaseResult F003R_REST(Map<String, String> reqParam) {
        log.trace("F003R_REST start ~~");
        BaseResult bs = null;
        F003R_REST_RQ rq = null;
        F003R_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new F003R_REST_RQ();
            rq = CodeUtil.objectCovert(F003R_REST_RQ.class, reqParam);
            rs = restutil.send(rq, F003R_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("F003R_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("F003R_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("F003R_REST_RS Exception", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 外匯匯入匯款線上解款
     * 
     * F003T_REST
     * 
     * @param reqParam
     * 
     */
    public BaseResult F003T_REST(Map<String, String> reqParam) {
        log.trace("F003T_REST start ~~");
        BaseResult bs = null;
        F003T_REST_RQ rq = null;
        F003T_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new F003T_REST_RQ();
            rq = CodeUtil.objectCovert(F003T_REST_RQ.class, reqParam);
            try {
            	String txamt = NumericUtil.formatNumberString(reqParam.get("TXAMT"), 2);
            	log.info(ESAPIUtil.vaildLog("txamt >> " + txamt));
                rq.setADTXAMT(txamt);
            }catch(Exception e){
            	log.error(e.toString());
            }
            rs = restutil.send(rq, F003T_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("F003T_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("F003T_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("F003T_REST_RS Exception", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 一般網銀外幣匯出匯款受款人約定檔擷取
     * 
     * F031_REST
     * 
     * @param cusidn
     * @return
     */
    public BaseResult F031_REST(String cusidn) {
        log.trace("F031_REST");
        BaseResult bs = null;
        F031_REST_RQ rq = null;
        F031_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new F031_REST_RQ();
            rq.setCUSIDN(cusidn);
            rs = restutil.send(rq, F031_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("F031_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("F031_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 匯入匯款線上解款申請及註銷
     * 
     * F035_REST
     * 
     * @param reqParamMap
     * 
     */
    public BaseResult F035_REST(Map<String, String> reqParam) {
        log.trace("F035_REST start ~~");
        BaseResult bs = null;
        F035_REST_RQ rq = null;
        F035_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new F035_REST_RQ();
            rq = CodeUtil.objectCovert(F035_REST_RQ.class, reqParam);
            rs = restutil.send(rq, F035_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("F035_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("F035_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 一般網銀外幣匯出匯款受款人約定檔擷取
     * 
     * F037_REST
     * 
     * @param cusidn
     * @return
     */
    public BaseResult F037_REST(String cusidn, String benacc, String ccy, Map<String, String> reqParam) {
        log.trace("F037_REST");
        BaseResult bs = null;
        F037_REST_RQ rq = null;
        F037_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new F037_REST_RQ();
            rq = CodeUtil.objectCovert(F037_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);
            rq.setBENACC(benacc);
            rq.setCCY(ccy);
            rs = restutil.send(rq, F037_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("F037_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("F037_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 黃金存摺歷史價格查詢
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */
    public BaseResult GD11HS_REST(Map<String, String> reqParam) {
        log.trace("GD11HS_REST");
        BaseResult bs = null;
        GD011_REST_RQ rq = null;
        GD011_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new GD011_REST_RQ();
            rq = CodeUtil.objectCovert(GD011_REST_RQ.class, reqParam);
            rq.setADOPID("GD11HS");
            rs = restutil.send(rq, GD011_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("GD011_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("GD011_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 
     * N074 轉入臺幣綜存定存
     * 
     * @param reqParam
     * @return
     */
    public BaseResult N074_REST(Map<String, String> reqParam) {
        log.trace("N074_REST start");
        BaseResult bs = null;
        N074_REST_RQ rq = null;
        N074_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = CodeUtil.objectCovert(N074_REST_RQ.class, reqParam);
            rs = restutil.send(rq, N074_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N074_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("N074_REST", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     *
     * N076 臺幣零存整付按月繳存
     * 
     * @param type
     * @param reqMap
     * @return
     */
    public BaseResult N076_REST(String type, Map<String, String> reqMap) {

        BaseResult bs = new BaseResult();
        N076_REST_RQ rq = null;
        try {
            // 建立傳入參數
            rq = CodeUtil.objectCovert(N076_REST_RQ.class, reqMap);
            rq.setTXID(type);
            log.trace(ESAPIUtil.vaildLog("toString >> " + rq.toString()));
            switch (type) {
            case N076QUERYLLIST:
                log.trace("N076_REST start~~~~~!!!");
                // 得到回應結果
                N420_REST_RS n420_REST_RS = restutil.send(rq, N420_REST_RS.class,
                        rq.getAPI_Name(rq.getClass()));
                if (n420_REST_RS != null) {
                    CodeUtil.convert2BaseResult(bs, n420_REST_RS);
                } else {
                    log.error("N076_REST is null");
                    bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                    
                }
                break;
            //
            case WSN076ACTION:
                log.trace("N076_REST start~~~~~!!!");
                N076_REST_RS n076_REST_RS = restutil.send(rq, N076_REST_RS.class,
                        rq.getAPI_Name(rq.getClass()));
                if (n076_REST_RS != null) {
                    CodeUtil.convert2BaseResult(bs, n076_REST_RS);
                } else {
                    log.error("N076_REST is null");
                    bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                    
                }
                // 得到回應結果
                break;
            default:
                log.warn("call_N076 type is not maping type is {}", type);
                break;
            }
        } catch (Exception e) {
            log.error("N079_REST action", e);
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N077 臺幣定存自動轉期申請/變更
     * 
     * @param reqParam
     * @return
     */
    public BaseResult N077_REST(Map<String, String> reqParam) {
        log.trace("N077_REST start");
        BaseResult bs = null;
        N077_REST_RQ rq = null;
        N077_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = CodeUtil.objectCovert(N077_REST_RQ.class, reqParam);
            log.trace(ESAPIUtil.vaildLog("toString >> " + rq.toString()));
            rs = restutil.send(rq, N077_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N077_REST>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N077_REST is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("N077_REST", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N078
     * <p>
     * 臺幣定存單到期續存
     * 
     * @param reqParam
     * @return
     */
    public BaseResult N078_REST(Map<String, String> reqParam) {
        BaseResult bs = new BaseResult();
        N078_REST_RQ rq = null;
        N078_REST_RS rs = null;
        try {
            // 建立傳入資料
            rq = CodeUtil.objectCovert(N078_REST_RQ.class, reqParam);
            log.trace(ESAPIUtil.vaildLog("call_N078 >> " + rq.toString()));
            // 得到回應結果
            rs = restutil.send(rq, N078_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("call_N078>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N077_REST is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            //e.printStackTrace();
        	//Avoid Information Exposure Through an Error Message
            log.error("call_N078 Error >> {}", e);
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N079_REST
     * <p>
     * getCancelData : 得到解約資料
     * <p>
     * action : 發送解約交易
     * 
     * @param type
     *                 </P>
     *                 getCancelData type : N079_1
     *                 </P>
     *                 WSN079ACTION : N079_2
     * @param reqParam
     * @return
     */
    public BaseResult N079_REST(String type, Map<String, String> reqMap,Boolean txnlog) {
        BaseResult bs = new BaseResult();
        N079_REST_RQ rq = null;
        try {
            // 建立傳入參數
            rq = CodeUtil.objectCovert(N079_REST_RQ.class, reqMap);
            rq.setTXID(type);
            if(!txnlog) {
            	rq.setADOPID("N079_C");
            }
            log.trace(ESAPIUtil.vaildLog("toString >> " + rq.toString()));
            switch (type) {
            // 得到解約資料
            case WSN079GETCANCELDATA:
                log.trace("call_N079 WSN079GETCANCELDATA start~~~~~!!!");
                N079_REST_RS n079_REST_RS = restutil.send(rq, N079_REST_RS.class,
                        rq.getAPI_Name(rq.getClass()));
                if (n079_REST_RS != null) {
                    CodeUtil.convert2BaseResult(bs, n079_REST_RS);
                } else {
                    log.error("WSN079GETCANCELDATA is null");
                    bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                    
                }
                break;
            // 發送解約交易
            case WSN079ACTION:
                log.trace("call_N079 WSN079ACTION start~~~~~!!!");
                N079_REST_RS n079_REST_RS_1 = restutil.send(rq, N079_REST_RS.class,
                        rq.getAPI_Name(rq.getClass()));
                if (n079_REST_RS_1 != null) {
                    CodeUtil.convert2BaseResult(bs, n079_REST_RS_1);
                } else {
                    log.error("WSN079GETCANCELDATA is null");
                    bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                    
                }
                // 得到回應結果
                break;
            default:
                log.warn("call_N079 type is not maping type is {}", type);
                break;
            }
        } catch (Exception e) {
            log.error("N079_REST action", e);
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 臺幣餘額查詢 查詢所有帳戶，ACN帶 null 查詢單一帳戶餘額，ACN帶帳號
     * 
     * @param cusidn
     * @param acn
     * @return
     * @deprecated Use {@link #N110_REST(String,String,String)} instead
     */
    public BaseResult N110_REST(String cusidn, String acn) {
        return N110_REST(cusidn, acn, null);
    }

    /**
     * 臺幣餘額查詢 查詢所有帳戶，ACN帶 null 查詢單一帳戶餘額，ACN帶帳號
     * 
     * @param cusidn
     * @param acn
     * @param ms_Channel TODO
     * @return
     */
    public BaseResult N110_REST(String cusidn, String acn, String ms_Channel) {
        log.trace("N110_REST");
        log.trace(ESAPIUtil.vaildLog("cusidn>>{}"+ cusidn));
        BaseResult bs = null;
        N110_REST_RQ rq = null;
        N110_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N110_REST_RQ();
            // if(StrUtil.isNotEmpty(ms_Channel)) {
            // rq.setMs_Channel(ms_Channel);
            // }
            rq.setCUSIDN(cusidn);
            rq.setACN(acn);
//            rs = restutil.send(rq, N110_REST_RS.class, rq.getAPI_Name(rq.getClass(), ms_Channel));
            rs = restutil.send(rq, N110_REST_RS.class, rq.getAPI_Name(rq.getClass(), ms_Channel));
            log.trace(ESAPIUtil.vaildLog("N110_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
                
            } else {
                log.error("N110_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 預約取消
     * 
     * @param cusidn
     * @param acn
     * @return
     */
    public BaseResult N091_03_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N091_03_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N091_03_REST_RQ rq = null;
        N091_03_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N091_03_REST_RQ();
            rq = CodeUtil.objectCovert(N091_03_REST_RQ.class, reqParam);
            rq.setUID(cusidn);
            rs = restutil.send(rq, N091_03_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N091_03_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N091_03_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 預約查詢
     * 
     * @param cusidn
     * @param acn
     * @return
     */
    public BaseResult N092_REST(String cusidn, String acn) {
        log.trace("N092_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N092_REST_RQ rq = null;
        N092_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N092_REST_RQ();
            rq.setCUSIDN(cusidn);
            rq.setACN(acn);
            rs = restutil.send(rq, N092_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N092_REST_RS>>{}"+rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N092_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 預約查詢
     * 
     * @param cusidn
     * @param acn
     * @return
     */
    public BaseResult N094_REST(String cusidn, Map<String, String> reqParam, String adopid) {
        log.trace("N094_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N094_REST_RQ rq = null;
        N094_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N094_REST_RQ();
            rq = CodeUtil.objectCovert(N094_REST_RQ.class, reqParam);
            rq.setUID(cusidn);
            rq.setADSVBH("050");
            rq.setADAGREEF("0");
            rq.setADOPID(adopid);
            rs = restutil.send(rq, N094_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N094_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N094_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 
     * <p>
     * 臺幣活期性存款
     * </p>
     * 
     * @param acn
     * @param reqParam 前端輸入變數
     * @return
     */
    public BaseResult N130_REST(String cusidn, String acn, Map<String, String> reqParam) {

        log.trace("N130_REST start");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N130_REST_RQ rq = null;
        N130_REST_RS rs = null;
        try {
            reqParam = DateUtil.periodProcessing(reqParam);
            String fgperiod = reqParam.get("FGPERIOD");// 查詢區間
            String cmsdate = reqParam.get("CMSDATE"); // 日期(起)
            String cmedate = reqParam.get("CMEDATE"); // 日期(迄)
            log.debug(ESAPIUtil.vaildLog("cmsdate >> " + cmsdate)); 
            log.debug(ESAPIUtil.vaildLog("cmedate >> " + cmedate)); 
            bs = new BaseResult();
            // call REST API
            rq = new N130_REST_RQ();
            rq.setACN(acn);
            rq.setCUSIDN(cusidn);
            rq.setCMSDATE(cmsdate);
            rq.setCMEDATE(cmedate);
            rq.setFGPERIOD(fgperiod);
            rs = restutil.send(rq, N130_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N130_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N130_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 
     * @param cusidn
     * @param acn
     * @param reqParam
     * @return
     */
    public BaseResult N150_REST(Map<String, String> reqParam) {
        log.trace("N150_REST start");
        BaseResult bs = new BaseResult();
        N150_REST_RQ rq = null;
        N150_REST_RS rs = null;
        try {
            rq = CodeUtil.objectCovert(N150_REST_RQ.class, reqParam);
            rs = restutil.send(rq, N150_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N150_REST>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N150_REST is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N175 外幣綜存定存解約
     * 
     * @param type
     * @param reqMap
     * @return
     */
    public BaseResult N175_REST(String type, Map<String, String> reqMap) {
        BaseResult bs = new BaseResult();
        try {
            log.debug(ESAPIUtil.vaildLog("N175_REST params >> {}"+ CodeUtil.toJson(reqMap)));
            String cusidn = reqMap.get("CUSIDN");
            if (StrUtil.isEmpty(cusidn)) {
                log.error(ESAPIUtil.vaildLog("未帶 CUSIDN 參數 >> {}" + cusidn));
                throw new NullPointerException("未帶 CUSIDN 參數");
            } else {
                // 多帶UID，與CUSIDN相同
                reqMap.put("UID", cusidn);
            }

            String apiName = "";

            switch (type) {
            // 外匯定存解約前置電文資料
            case WSN175BEFOROUT:
                log.debug("N175_1 START");
                N175_REST_RQ n175_1_rq = new N175_REST_RQ();
                N175_REST_RS n175_1_rs = null;

                // 建立參數
                n175_1_rq = CodeUtil.objectCovert(N175_REST_RQ.class, reqMap);
                n175_1_rq.setTXID("N175_1"); // 交易型態
                n175_1_rq.setFGTXWAY("0"); // 交易機制，查詢帶0

                apiName = n175_1_rq.getAPI_Name(n175_1_rq.getClass());
                // 得到回應電文
                n175_1_rs = restutil.send(n175_1_rq, N175_REST_RS.class, apiName);
                log.debug(ESAPIUtil.vaildLog("N175_1_RS >> {}"+ CodeUtil.toJson(n175_1_rs)));

                if (n175_1_rs != null) {
                    CodeUtil.convert2BaseResult(bs, n175_1_rs);
                } else {
                    log.error("N175_1_REST is null");
                    bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                    
                }
                break;

            // 發送解約交易
            case WSN175ACTION:
                log.debug("N175_2 START");
                N175_REST_RQ n175_2_rq = new N175_REST_RQ();
                N175_REST_RS n175_2_rs = null;

                // 建立參數
                n175_2_rq = CodeUtil.objectCovert(N175_REST_RQ.class, reqMap);
                n175_2_rq.setTXID("N175_2");

                apiName = n175_2_rq.getAPI_Name(n175_2_rq.getClass());
                // 得到回應電文
                n175_2_rs = restutil.send(n175_2_rq, N175_REST_RS.class, apiName);
                log.debug(ESAPIUtil.vaildLog("N175_2_RS >> {}"+ CodeUtil.toJson(n175_2_rs)));

                if (n175_2_rs != null) {
                    CodeUtil.convert2BaseResult(bs, n175_2_rs);
                } else {
                    log.error("N175_2_REST is null");
                    bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                    
                }
                break;

            // 發送解約交易(預約)
            case WSN175ACTIONS:
                log.debug("N175_2S START");
                N175_REST_RQ n175_2s_rq = new N175_REST_RQ();
                N175_REST_RS n175_2s_rs = null;

                // 建立參數
                n175_2s_rq = CodeUtil.objectCovert(N175_REST_RQ.class, reqMap);
                n175_2s_rq.setTXID("N175_2S");
                log.debug(ESAPIUtil.vaildLog("N175_2S_RQ >> {}"+ CodeUtil.toJson(n175_2s_rq)));

                apiName = n175_2s_rq.getAPI_Name(n175_2s_rq.getClass());
                // 得到回應結果，ms_tw會寫入一筆預約檔
                n175_2s_rs = restutil.send(n175_2s_rq, N175_REST_RS.class, apiName);
                log.debug(ESAPIUtil.vaildLog("N175_2S_RS >> {}"+ CodeUtil.toJson(n175_2s_rs)));

                if (n175_2s_rs != null) {
                    String topmsg = n175_2s_rs.getTOPMSG();
                    String msgName = n175_2s_rs.getMsgName();
                    String cmqtime = DateUtil.getCurentDateTime("yyyy/MM/dd hh:MM:ss");
                    reqMap.put("CMQTIME", cmqtime); // 資料時間

                    // 寫入預約檔失敗
                    if ("ZX99".equals(topmsg)) {
                        log.warn(ESAPIUtil.vaildLog("寫入預約檔失敗 TOPMSG >> {}"+ topmsg));
                    } else if ("0".equals(topmsg)) {
                        // 寫入預約檔成功
                        // 將前端帶來資料放入bs回傳
                        n175_2s_rs = CodeUtil.objectCovert(N175_REST_RS.class, reqMap);
                    }
                    CodeUtil.convert2BaseResult(bs, n175_2s_rs, topmsg, msgName);
                } else {
                    log.error("N175_2S_REST is null");
                    bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                    
                }
                break;

            default:
                log.warn("N175_REST type is not maping, type is {}", type);
                break;
            }
            log.debug("N175_REST_RS BaseResult >> {}", CodeUtil.toJson(bs.getData()));
        } catch (Exception e) {
            log.error("N175 REST error", e);
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 外匯定存自動轉期申請/變更，查詢及交易
     * 
     * @param txid
     * @param reqParam
     * @return
     */
    public BaseResult N177_REST(String txid, Map<String, String> reqParam) {
        log.debug("N177_REST start");
        log.debug(ESAPIUtil.vaildLog("N177_REST TXID >> {}"+ txid));
        log.debug(ESAPIUtil.vaildLog("N177_REST reqParam >> {}"+ CodeUtil.toJson(reqParam)));
        reqParam.put("TXID", txid);

        BaseResult bs = new BaseResult();
        try {
            String cusidn = reqParam.get("CUSIDN");
            String uid = cusidn;
            reqParam.put("UID", uid);
            log.debug(ESAPIUtil.vaildLog("N177_REST CUSIDN >> {}"+cusidn));

            N530_REST_RQ n530rq = null;
            N530_REST_RS n530rs = null;
            N177_REST_RQ n177rq = new N177_REST_RQ();
            N177_REST_RS n177rs = null;
            String apiName = n177rq.getAPI_Name(n177rq.getClass());
            switch (txid) {
            case "N177":
                // 查詢
                n530rq = CodeUtil.objectCovert(N530_REST_RQ.class, reqParam);
                // call N177_Tel_Service, not N530_Tel_Service
                n530rs = restutil.send(n530rq, N530_REST_RS.class, apiName);
                log.debug(ESAPIUtil.vaildLog("N177_REST 查詢 >> {}"+ CodeUtil.toJson(n530rs)));

                if (n530rs == null) {
                    log.error("N177_REST_RS is null");
                    bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                    
                } else {
                    CodeUtil.convert2BaseResult(bs, n530rs);
                }
                break;
            case "N177_1":
                // 交易
                n177rq = CodeUtil.objectCovert(N177_REST_RQ.class, reqParam);
                n177rs = restutil.send(n177rq, N177_REST_RS.class, apiName);
                log.debug(ESAPIUtil.vaildLog("N177_REST 交易 >> {}"+CodeUtil.toJson(n177rs)));

                if (n177rs == null) {
                    log.error("N177_REST_RS is null");
                    bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                    
                } else {
                    CodeUtil.convert2BaseResult(bs, n177rs);
                }
                break;
            default:
                log.warn("N177_REST TXID is not maping, txid is {}", txid);
                break;
            }
            log.debug("N177_REST_RS BaseResult >> {}", CodeUtil.toJson(bs.getData()));
        } catch (Exception e) {
            log.error("N177_REST Error", e);
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N178外匯定存單到期續存
     * 
     * @param txid
     * @param reqParam
     * @return
     */
    public BaseResult N178_REST(String txid, Map<String, String> reqParam) {
        log.debug("N178_REST start");
        log.debug(ESAPIUtil.vaildLog("N178_REST TXID >> {}"+ txid));
        log.debug(ESAPIUtil.vaildLog("N178_REST reqParam >> {}"+ CodeUtil.toJson(reqParam)));
        reqParam.put("TXID", txid);

        BaseResult bs = new BaseResult();

        try {
            String cusidn = reqParam.get("CUSIDN");
            if (StrUtil.isEmpty(cusidn)) {
                log.error(ESAPIUtil.vaildLog("未帶 CUSIDN 參數 >> {}"+ cusidn));
                throw new NullPointerException("未帶 CUSIDN 參數");
            } else {
                // 多帶UID，與CUSIDN相同
                reqParam.put("UID", cusidn);
            }

            N530_REST_RQ n530rq = null;
            N530_REST_RS n530rs = null;
            N178_REST_RQ n178rq = new N178_REST_RQ();
            N178_REST_RS n178rs = null;
            String apiName = n178rq.getAPI_Name(n178rq.getClass());
            switch (txid) {
            case "N178":
                // 查詢清單N530
                n530rq = CodeUtil.objectCovert(N530_REST_RQ.class, reqParam);

                // call N178_Tel_Service, not N530_Tel_Service
                n530rs = restutil.send(n530rq, N530_REST_RS.class, apiName);
                log.debug(ESAPIUtil.vaildLog("N178_REST 查詢清單 >> {}"+ CodeUtil.toJson(n530rs)));

                if (n530rs == null) {
                    log.error("N178_REST_RS is null");
                    bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                    
                } else {
                    CodeUtil.convert2BaseResult(bs, n530rs);
                }

                break;
            case "N178_1":
                // 取得確認頁資料
                n178rq = CodeUtil.objectCovert(N178_REST_RQ.class, reqParam);
                n178rq.setFGTXWAY("0");

                // call N178_Tel_Service
                n178rs = restutil.send(n178rq, N178_REST_RS.class, apiName);
                log.debug(ESAPIUtil.vaildLog("N178_REST 取得確認頁資料 >> {}"+CodeUtil.toJson(n178rs)));

                if (n178rs == null) {
                    log.error("N178_REST_RS is null");
                    bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                    
                } else {
                    CodeUtil.convert2BaseResult(bs, n178rs);
                }

                break;
            case "N178_2":
                // 交易
                n178rq = CodeUtil.objectCovert(N178_REST_RQ.class, reqParam);
                // call N178_Tel_Service
                n178rs = restutil.send(n178rq, N178_REST_RS.class, apiName);
                log.debug(ESAPIUtil.vaildLog("N178_REST 交易 >> {}"+ CodeUtil.toJson(n178rs)));

                if (n178rs == null) {
                    log.error("N178_REST_RS is null");
                    bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                    
                } else {
                    CodeUtil.convert2BaseResult(bs, n178rs);
                }

                break;
            default:
                log.warn("N178_REST TXID is not maping, txid is {}", txid);
                break;
            }
            log.debug("N178_REST_RS BaseResult >> {}", CodeUtil.toJson(bs.getData()));
        } catch (Exception e) {
            log.error("N178_REST Error", e);
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N190黃金存摺-餘額查詢
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */
    public BaseResult N190_REST(Map<String, String> reqParam) {
        log.trace("N190_REST");
        log.trace(ESAPIUtil.vaildLog("cusidn>>{}"+ reqParam.get("CUSIDN")));
        BaseResult bs = null;
        N190_REST_RQ rq = null;
        N190_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N190_REST_RQ();
            rq = CodeUtil.objectCovert(N190_REST_RQ.class, reqParam);
            rs = restutil.send(rq, N190_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N190_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
                bs.setResult(Boolean.TRUE);
            } else {
                log.error("N190_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;

    }

    /**
     * N191黃金存摺-餘額查詢
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */
    public BaseResult N191_REST(String cusidn, String acn, Map<String, String> reqParam) {

        log.trace("N191_REST start");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N191_REST_RQ rq = null;
        N191_REST_RS rs = null;
        try {
            reqParam = DateUtil.periodProcessing(reqParam);
            String stadate = reqParam.get("STADATE"); // 日期(起)
            String enddate = reqParam.get("ENDDATE"); // 日期(迄)
            log.debug(ESAPIUtil.vaildLog("stadate {} "+ stadate));
            log.debug(ESAPIUtil.vaildLog("enddate {} "+ enddate));
            bs = new BaseResult();
            // call REST API
            rq = new N191_REST_RQ();
            rq = CodeUtil.objectCovert(N191_REST_RQ.class, reqParam);
            rq.setACN(acn);
            rq.setCUSIDN(cusidn);
            rs = restutil.send(rq, N191_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N191_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N191_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 線上約定轉入帳號申請
     * 
     * @param cusidn
     * @return
     */

    public BaseResult N215A_REST(String cusidn) {
        log.trace("N215A_REST>>{}", cusidn);
        BaseResult bs = null;
        N215A_REST_RQ rq = null;
        N215A_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N215A_REST_RQ();
            rq.setUID(cusidn);
            rs = restutil.send(rq, N215A_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.trace(ESAPIUtil.vaildLog("N215A_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N215A_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 線上約定轉入帳號註銷_申請
     * 
     * @param cusidn
     * @return
     */

    public BaseResult N215A_1_REST(String cusidn, String SelectedRecord, String COUNT, String FGTXWAY, Map<String, String> reqParam) {
        log.trace("N215A_1_REST>>{}", cusidn);
        BaseResult bs = null;
        N215A_1_REST_RQ rq = null;
        N215A_1_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N215A_1_REST_RQ();
            rq = CodeUtil.objectCovert(N215A_1_REST_RQ.class, reqParam);
            rq.setUID(cusidn);
            rq.setCOUNT(COUNT);
            rq.setTYPE("01");
            rq.setSelectedRecord(SelectedRecord);
            rq.setACNINFO(SelectedRecord);
            rq.setADOPID("N215");
            rq.setTXID("N215A");
            rq.setFGTXWAY(FGTXWAY);
            rs = restutil.send(rq, N215A_1_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.trace(ESAPIUtil.vaildLog("N215A_1_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N215A_1_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 線上約定轉入帳號註銷
     * 
     * @param cusidn
     * @return
     */

    public BaseResult N215D_REST(String cusidn) {
        log.trace("N215D_REST>>{}", cusidn);
        BaseResult bs = null;
        N215D_REST_RQ rq = null;
        N215D_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N215D_REST_RQ();
            rq.setUID(cusidn);
            rs = restutil.send(rq, N215D_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.trace(ESAPIUtil.vaildLog("N215D_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N215D_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 線上約定轉入帳號註銷_結果
     * 
     * @param cusidn
     * @return
     */

    public BaseResult N215D_1_REST(String cusidn, String SelectedRecord, String COUNT, Map<String, String> reqParam) {
        log.trace("N215D_1_REST>>{}", cusidn);
        BaseResult bs = null;
        N215D_1_REST_RQ rq = null;
        N215D_1_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N215D_1_REST_RQ();
            rq = CodeUtil.objectCovert(N215D_1_REST_RQ.class, reqParam);
            rq.setUID(cusidn);
            rq.setCOUNT(COUNT);
            rq.setTYPE("02");
            rq.setACNINFO(SelectedRecord);
            rq.setSelectedRecord(SelectedRecord);
            rq.setADOPID("N215");
            rq.setTXID("N215D");
            rs = restutil.send(rq, N215D_1_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.trace(ESAPIUtil.vaildLog("N215D_1_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N215D_1_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 線上約定轉入帳號好記名稱
     * 
     * @param cusidn
     * @return
     */

    public BaseResult N215M_REST(String cusidn) {
        log.trace("N215M_REST>>{}", cusidn);
        BaseResult bs = null;
        N215M_REST_RQ rq = null;
        N215M_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N215M_REST_RQ();
            rq.setUID(cusidn);
            rq.setADOPID("N215M");
            rq.setTXID("N215M");
            rs = restutil.send(rq, N215M_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.trace(ESAPIUtil.vaildLog("N215M_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N215M_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }
    /**
     * 線上約定轉入帳號好記名稱
     * 
     * @param cusidn
     * @return
     */

    public BaseResult N215M_1_REST(String cusidn,Map<String,String> reqParam) {
        log.trace("N215M_1_REST>>{}", cusidn);
        BaseResult bs = null;
        N215M_1_REST_RQ rq = null;
        N215M_1_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N215M_1_REST_RQ();
            rq = CodeUtil.objectCovert(N215M_1_REST_RQ.class, reqParam);
            rq.setUID(cusidn);
            rs = restutil.send(rq, N215M_1_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.trace(ESAPIUtil.vaildLog("N215M_1_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N215M_1_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }
    /**
     * 借款明細查詢
     * 
     * @param cusidn
     * @return
     */

    public BaseResult N320_REST(Map<String, String> reqParam) {
        log.trace("N320_REST");
        log.trace(ESAPIUtil.vaildLog("cusidn>>{}"+ reqParam.get("CUSIDN")));
        BaseResult bs = null;
        N320_REST_RQ rq = null;
        N320_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N320_REST_RQ();
            rq = CodeUtil.objectCovert(N320_REST_RQ.class, reqParam);

            rs = restutil.send(rq, N320_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.trace(ESAPIUtil.vaildLog("N320_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N320_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }
    public BaseResult N320_NoTxn_REST(Map<String, String> reqParam) {
        log.trace("N320_REST");
        log.trace(ESAPIUtil.vaildLog("cusidn>>{}"+ reqParam.get("CUSIDN")));
        BaseResult bs = null;
        N320_REST_RQ rq = null;
        N320_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N320_REST_RQ();
            rq = CodeUtil.objectCovert(N320_REST_RQ.class, reqParam);
            rq.setADOPID("N320_NoTxn");
            rs = restutil.send(rq, N320_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.trace(ESAPIUtil.vaildLog("N320_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N320_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 境外信託商品推介
     * 
     * @param reqParam
     * @return
     */
    public BaseResult N323_REST(Map<String, String> reqParam) {
        log.trace("N323_REST");
        log.trace(ESAPIUtil.vaildLog("cusidn>>{}"+ reqParam.get("UID")));
        BaseResult bs = null;
        N323_REST_RQ rq = null;
        N323_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new N323_REST_RQ();
            rq = CodeUtil.objectCovert(N323_REST_RQ.class, reqParam);

            rs = restutil.send(rq, N323_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.trace(ESAPIUtil.vaildLog("N323_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N323REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    public BaseResult N360_REST(String cusidn) {
        log.debug(ESAPIUtil.vaildLog("CUSIDN >> {}"+ cusidn));
        BaseResult bs = new BaseResult();
        N360_REST_RQ rq = new N360_REST_RQ();
        N360_REST_RS rs = null;
        try {
            rq.setCUSIDN(cusidn);
            rs = restutil.send(rq, N360_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.debug(ESAPIUtil.vaildLog("N360_REST_RS >> {}"+ CodeUtil.toJson(rs)));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
                // bs.addData("RS", rs);
            } else {
                log.error("N360_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("N360_REST error", e);
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 線上預約開立基金戶(線上信託開戶)
     * 
     * @param cusidn
     * @return
     */
    public BaseResult N361_REST(N361_REST_RQ rq) {
        log.debug(ESAPIUtil.vaildLog("N361_REST params >> {}"+ CodeUtil.toJson(rq)));
        BaseResult bs = new BaseResult();
        N361_REST_RS rs = null;
        try {
            rs = restutil.send(rq, N361_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.debug(ESAPIUtil.vaildLog("N361_REST_RS >> {}"+ CodeUtil.toJson(rs)));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N361_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("N361_REST error", e);
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 線上申請新臺幣存款帳戶結清銷戶-查詢結清帳號餘額
     * 
     * @param rq
     * @return
     */
    public BaseResult N366_REST(N366_REST_RQ rq) {
        log.debug(ESAPIUtil.vaildLog("N366_REST params >> " + CodeUtil.toJson(rq))); 
        BaseResult bs = new BaseResult();
        N366_REST_RS rs = null;
        try {
            rs = restutil.send(rq, N366_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.debug(ESAPIUtil.vaildLog("N366_REST_RS >> {}"+ CodeUtil.toJson(rs)));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
                bs.addData("RS", rs);
            } else {
                log.error("N366_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("N366_REST error", e);
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 線上申請新臺幣存款帳戶結清銷戶-交易電文
     * 
     * @param rq
     * @return
     */
    public BaseResult N366_1_REST(N366_1_REST_RQ rq) {
        log.debug("N366_1_REST params >> {}", CodeUtil.toJson(rq));
        BaseResult bs = new BaseResult();
        N366_1_REST_RS rs = null;
        try {
            rs = restutil.send(rq, N366_1_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.debug(ESAPIUtil.vaildLog("N366_1_REST_RS >> {}"+CodeUtil.toJson(rs)));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
                bs.addData("RS", rs);
            } else {
                log.error("N366_1_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("N366_1_REST error", e);
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 境外信託商品推介 查詢
     * 
     * @param reqParam
     * @return
     */
    public BaseResult N3231_REST(String cusidn) {
        log.trace("N3231_REST");
        BaseResult bs = null;
        N3231_REST_RQ rq = null;
        N323_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new N3231_REST_RQ();
            rq.setUID(cusidn);

            rs = restutil.send(rq, N323_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.trace(ESAPIUtil.vaildLog("N3231_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N3231REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 台幣定存明細查詢
     * 
     * @param cusidn 身份證號
     * @param acn    帳號
     * @return
     */

    public BaseResult N420_REST(String cusidn) {
    	return N420_REST(cusidn, null);
    }
    public BaseResult N420_REST(String cusidn, String isTxnlLog) {
        log.trace("N420_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N420_REST_RQ rq = null;
        N420_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N420_REST_RQ();
            rq.setCUSIDN(cusidn);
            rq.setACN("");
            rq.setOKOVNEXT("TRUE");
            if(isTxnlLog != null)
            	rq.setIsTxnlLog(isTxnlLog);
            rs = restutil.send(rq, N420_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N420_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N420_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }    
    /**
     * 台幣定存明細查詢
     * 
     * @param cusidn 身份證號
     * @param acn    帳號
     * @return
     */
    public BaseResult N420_NoTxn_REST(String cusidn) {
        log.trace("N420_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N420_REST_RQ rq = null;
        N420_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N420_REST_RQ();
            rq.setCUSIDN(cusidn);
            rq.setACN("");
            rq.setADOPID("N420_NoTxn");
            rq.setOKOVNEXT("TRUE");
            rs = restutil.send(rq, N420_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N420_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N420_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 台幣定存明細查詢
     * 
     * @param cusidn 身份證號
     * @param acn    帳號
     * @return
     */
    public BaseResult N421_REST(String cusidn) {
        log.trace("N421_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N421_REST_RQ rq = null;
        N421_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N421_REST_RQ();
            rq.setCUSIDN(cusidn);
            // 20210602 新增 若客戶資料超過300筆 , 用此參數找出全部資料 , 沒超過的話結果不變
            rq.setOKOVNEXT("TRUE");
            rs = restutil.send(rq, N421_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N421_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N421_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * n520外匯活期存款交易明細查詢
     * 
     * @param cusidn
     * @param acn
     * @param reqParam
     * @return
     */
    public BaseResult N520_REST(String cusidn, String acn, Map<String, String> reqParam) {
        log.trace("N520_REST start");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = new BaseResult();
        N520_REST_RQ rq = null;
        N520_REST_RS rs = null;
        try {
            reqParam = DateUtil.periodProcessing(reqParam);
            String fgperiod = reqParam.get("FGPERIOD");// 查詢區間
            String cmsdate = reqParam.get("CMSDATE"); // 日期(起)
            String cmedate = reqParam.get("CMEDATE"); // 日期(迄)
            log.debug(ESAPIUtil.vaildLog("cmsdate {} "+ cmsdate));
            log.debug(ESAPIUtil.vaildLog("cmedate {} "+ cmedate));
            // call REST API
            rq = new N520_REST_RQ();
            rq.setACN(acn);
            rq.setCUSIDN(cusidn);
            rq.setCMSDATE(cmsdate);
            rq.setCMEDATE(cmedate);
            rq.setFGPERIOD(fgperiod);
            rs = restutil.send(rq, N520_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N520_REST>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N520_REST is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * n531外匯綜存定存查詢
     * 
     * @param cusidn
     * @return
     */
    public BaseResult N531_REST(String cusidn) {
        log.trace("N531_REST start");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = new BaseResult();
        N531_REST_RQ rq = null;
        N531_REST_RS rs = null;
        try {
            // call REST API
            rq = new N531_REST_RQ();
            rq.setCUSIDN(cusidn);
            // call REST API
            rs = restutil.send(rq, N531_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N531_REST>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
                log.trace("Baseresult>>" + bs);
            } else {
                log.error("N531_REST is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 
     * @param cusidn
     * @param acn
     * @param reqParam
     * @return
     */
    public BaseResult N550_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N550_REST start");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = new BaseResult();
        N550_REST_RQ rq = null;
        N550_REST_RS rs = null;
        try {
            String okind = reqParam.get("QKIND");
            String fdate = reqParam.get("FDATE");
            String tdate = reqParam.get("TDATE");
            String lcno = reqParam.get("LCNO");
            String refno = reqParam.get("REFNO");

            // call REST API
            rq = new N550_REST_RQ();
            rq.setCUSIDN(cusidn);
            rq.setQKIND(okind);
            rq.setFDATE(fdate);
            rq.setTDATE(tdate);
            rq.setLCNO(lcno);
            rq.setREFNO(refno);

            rs = restutil.send(rq, N550_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N550_REST>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N550_REST is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    public BaseResult N551_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N551_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N551_REST_RQ rq = null;
        N551_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new N551_REST_RQ();
            rq.setCUSIDN(cusidn);
            log.trace(ESAPIUtil.vaildLog("CMSDATE>>{}" + reqParam.get("CMSDATE")));
            rq.setFDATE(reqParam.get("CMSDATE"));
            log.trace(ESAPIUtil.vaildLog("CMEDATE>>{}" + reqParam.get("CMEDATE")));
            rq.setTDATE(reqParam.get("CMEDATE"));
            rq.setLCNO(reqParam.get("LCNO"));
            rq.setUSERDATA_X50(reqParam.get("USERDATA_X50"));
            rq.setOKOVNEXT(reqParam.get("OKOVNEXT"));
            rq.setADOPID("N551");
            rq.setTXID("N551");

            rs = restutil.send(rq, N551_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.trace(ESAPIUtil.vaildLog("N551_REST_RS>>{}"+rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N551_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    public BaseResult N555_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N555_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N555_REST_RQ rq = null;
        N555_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new N555_REST_RQ();
            rq.setCUSIDN(cusidn);
            log.trace(ESAPIUtil.vaildLog("LCNO>>{}"+reqParam.get("LCNO")));
            rq.setLCNO(reqParam.get("LCNO"));
            rq.setUSERDATA_X50(reqParam.get("USERDATA_X50_import_detail"));
            rq.setADOPID("N555");
            rq.setTXID("N555");
            rs = restutil.send(rq, N555_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.trace(ESAPIUtil.vaildLog("N555_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N555_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    public BaseResult N556_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N556_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N556_REST_RQ rq = null;
        N556_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new N556_REST_RQ();
            rq.setCUSIDN(cusidn);
            log.trace(ESAPIUtil.vaildLog("LCNO>>{}" + reqParam.get("LCNO")));
            rq.setLCNO(reqParam.get("LCNO"));
            rq.setADOPID("N556");
            rq.setTXID("N556");

            rs = restutil.send(rq, N556_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.trace(ESAPIUtil.vaildLog("N556_REST_RS>>{}"+rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N556_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    public BaseResult N557_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N557_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N557_REST_RQ rq = null;
        N557_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new N557_REST_RQ();
            rq.setCUSIDN(cusidn);
            log.trace(ESAPIUtil.vaildLog("CMSDATE>>{}"+reqParam.get("CMSDATE")));
            rq.setFDATE(reqParam.get("CMSDATE"));
            log.trace(ESAPIUtil.vaildLog("CMEDATE>>{}"+ reqParam.get("CMEDATE")));
            rq.setTDATE(reqParam.get("CMEDATE"));
            rq.setLCNO(reqParam.get("LCNO"));
            rq.setUSERDATA_X50(reqParam.get("USERDATA_X50"));
            rq.setOKOVNEXT(reqParam.get("OKOVNEXT"));
            rq.setADOPID("N557");
            rq.setTXID("N557");

            rs = restutil.send(rq, N557_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.trace(ESAPIUtil.vaildLog("N557_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N557_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 
     * @param cusidn
     * @param acn
     * @param reqParam
     * @return
     */
    public BaseResult N558_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N558_REST start");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = new BaseResult();
        N558_REST_RQ rq = null;
        N558_REST_RS rs = null;
        try {
            String fdate = reqParam.get("FDATE");
            String tdate = reqParam.get("TDATE");
            String lcno = reqParam.get("LCNO");

            // call REST API
            rq = new N558_REST_RQ();
            rq.setCUSIDN(cusidn);
            rq.setFDATE(fdate);
            rq.setTDATE(tdate);
            rq.setLCNO(lcno);
            rq.setUSERDATA(reqParam.get("USERDATA"));

            rs = restutil.send(rq, N558_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N558_REST>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N558_REST is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    public BaseResult N564_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N564_REST start");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = new BaseResult();
        N564_REST_RQ rq = null;
        N564_REST_RS rs = null;
        try {
            String fdate = reqParam.get("CMSDATE");
            String tdate = reqParam.get("CMEDATE");
            String lcno = reqParam.get("LCNO");
            String refno = reqParam.get("REFNO");
            String accno = reqParam.get("ACN");
            // call REST API
            rq = new N564_REST_RQ();
            rq.setACCNO(accno);
            rq.setCUSIDN(cusidn);
            rq.setLCNO(lcno);
            rq.setREFNO(refno);
            rq.setCMSDATE(fdate);
            rq.setCMEDATE(tdate);

            rs = restutil.send(rq, N564_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N564_REST>>{}"+rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N564_REST is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    public BaseResult N565_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N565_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N565_REST_RQ rq = null;
        N565_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N565_REST_RQ();

            rq.setCUSIDN(cusidn);
            rq.setACCTNO(reqParam.get("ACN").equals("ALL") ? "" : reqParam.get("ACN"));
            log.trace(ESAPIUtil.vaildLog("CMSDATE>>{}"+reqParam.get("CMSDATE")));
            rq.setCMSDATE(reqParam.get("CMSDATE"));
            log.trace(ESAPIUtil.vaildLog("CMEDATE>>{}"+reqParam.get("CMEDATE")));
            rq.setCMEDATE(reqParam.get("CMEDATE"));
            rq.setUSERDATA(reqParam.get("USERDATA"));
            rq.setOKOVNEXT(reqParam.get("OKOVNEXT"));

            rs = restutil.send(rq, N565_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.trace(ESAPIUtil.vaildLog("N565_REST_RS>>{}"+rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N565_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N566匯出匯款查詢
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */
    public BaseResult N566_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N566_REST start");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = new BaseResult();
        N566_REST_RQ rq = null;
        N566_REST_RS rs = null;
        try {
            String fdate = reqParam.get("FDATE");
            String tdate = reqParam.get("TDATE");
            String cmsdate = reqParam.get("CMSDATE"); // 日期(起)
            String cmedate = reqParam.get("CMEDATE"); // 日期(迄)
            // call REST API
            rq = new N566_REST_RQ();
            rq.setCUSIDN(cusidn);
            rq.setFDATE(fdate);
            rq.setTDATE(tdate);
            rq.setCMSDATE(cmsdate);
            rq.setCMEDATE(cmedate);
            rs = restutil.send(rq, N566_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N566_REST>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N566_REST is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N567光票託收查詢
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */

    public BaseResult N567_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N567_REST start");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = new BaseResult();
        N567_REST_RQ rq = null;
        N567_REST_RS rs = null;
        try {
            String cmsdate = reqParam.get("CMSDATE"); // 日期(起)
            String cmedate = reqParam.get("CMEDATE"); // 日期(迄)
            String trnsrc = reqParam.get("TRNSRC");
            String userdata = reqParam.get("USERDATA");// 本次未完資料KEY值
            // call REST API
            rq = new N567_REST_RQ();
            rq.setCUSIDN(cusidn);
            rq.setCMSDATE(cmsdate);
            rq.setCMEDATE(cmedate);
            rq.setTRNSRC(trnsrc);
            rq.setUSERDATA(userdata);
            rs = restutil.send(rq, N567_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N567_REST>>{}"+rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N567_REST is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N640輕鬆理財證券交割明細
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */
    public BaseResult N640_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N640_REST start");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = new BaseResult();
        N640_REST_RQ rq = null;
        N640_REST_RS rs = null;
        try {
            // call REST API
            rq = new N640_REST_RQ();
            rq = CodeUtil.objectCovert(N640_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);
            rq.setTXID("N640");
            log.trace(ESAPIUtil.vaildLog("N640_REST_RQ >> " + rq));
            rs = restutil.send(rq, N640_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N640_REST_RS >> " + rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
                log.error("N640_REST is not null");
                log.trace("bs.getResult>> {}", bs.getResult());
            } else {
                log.error("N640_REST is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    public BaseResult N650_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N650_REST start");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = new BaseResult();
        N650_REST_RQ rq = null;
        N650_REST_RS rs = null;
        try {
            // call REST API
            rq = new N650_REST_RQ();
            rq.setCUSIDN(cusidn);
            rq.setACN(reqParam.get("ACN").equals("ALL") ? "" : reqParam.get("ACN"));
            rq.setUSERDATA_X50(reqParam.get("USERDATA_X50"));
            rs = restutil.send(rq, N650_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N650_REST>>{}"+rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N650_REST is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    public BaseResult N860_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N860_REST start");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = new BaseResult();
        N860_REST_RQ rq = null;
        N860_REST_RS rs = null;
        try {
            String acn = reqParam.get("ACN").equals("All") ? "" : reqParam.get("ACN");
            String cmsdate2 = reqParam.get("CMSDATE2"); // 日期(起)
            String cmedate2 = reqParam.get("CMEDATE2"); // 日期(迄)
            String cmsdate3 = reqParam.get("CMSDATE3"); // 日期(起)
            String cmedate3 = reqParam.get("CMEDATE3"); // 日期(迄)
            String cmsdate4 = reqParam.get("CMSDATE4"); // 日期(起)
            String cmedate4 = reqParam.get("CMEDATE4"); // 日期(迄)
            String fgBrhcod = reqParam.get("FGBRHCOD");
            String dpbhno = reqParam.get("DPBHNO");
            String fgType = reqParam.get("FGTYPE");
            String fgPeriod = reqParam.get("FGPERIOD");

            // call REST API
            rq = new N860_REST_RQ();
            rq.setCUSIDN(cusidn);
            rq.setACN(acn);
            rq.setCMEDATE2(cmedate2);
            rq.setCMEDATE3(cmedate3);
            rq.setCMEDATE4(cmedate4);
            rq.setCMSDATE2(cmsdate2);
            rq.setCMSDATE3(cmsdate3);
            rq.setCMSDATE4(cmsdate4);
            rq.setFGBRHCOD(fgBrhcod);
            rq.setDPBHNO(dpbhno);
            rq.setFGTYPE(fgType);
            rq.setFGPERIOD(fgPeriod);
            rs = restutil.send(rq, N860_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N860_REST_RS >>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N860_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 根據傳入Type查詢帳號
     * 
     * @param cusidn
     * @param type
     * @return
     */
    public BaseResult N920_REST(String cusidn, String type) {
    	List<String> ehcacheTypeWhiltList = new ArrayList<String>();
    	//使用Ehcache之白名單
    	ehcacheTypeWhiltList.addAll(Arrays.asList(ACNO,OUT_ACNO,IN_ACNO,GOLD_ACNO,FX_OUT_ACNO,COLLATER,GOLD_RESERVATION_QOERY_N920));
    	if(ehcacheTypeWhiltList.indexOf(type) != -1) {
	        BaseResult bs = null;
	        try {
	        	bs = new BaseResult();
	        	BaseResult bsTmp = daoService.N920_REST_Ehcache(cusidn, type);
	        	bs.addAllData((Map<String,Object>)bsTmp.getData());
	        	bs.setMessage(bsTmp.getMsgCode(), bsTmp.getMessage());
	        	bs.setResult(bsTmp.getResult());
	        	bs.setPrevious(bsTmp.getPrevious());
	        	bs.setNext(bsTmp.getNext());
	        	log.debug(CodeUtil.toJson(bs.getData()));
	        } catch (Exception e) {
	            log.error("", e);
	            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
	        }
	        this.getMessageByMsgCode(bs);
	        return bs;
        }else {
        	return N920_REST_No_Ehcache(cusidn,type);
        }
    }
    /**
     * 根據傳入Type查詢帳號
     * 
     * @param cusidn
     * @param type
     * @return
     */
    public BaseResult N920_REST_No_Ehcache(String cusidn, String type) {
        log.debug("N920_REST_No_Ehcache start");
        log.trace(ESAPIUtil.vaildLog("cusidn>>" + cusidn));
        log.trace(ESAPIUtil.vaildLog("type>>" + type));
        log.debug("is in Cache");
        BaseResult bs = null;
        N920_REST_RQ rq = null;
        N920_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new N920_REST_RQ();
            // call REST API
            rq.setCUSIDN(cusidn);
            rq.setTYPE(type.toUpperCase());
            rs = restutil.send(rq, N920_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.debug(ESAPIUtil.vaildLog("N920_REST_RS >> {}"+ CodeUtil.toJson(rs)));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N920_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }
    
    /**
     * 根據傳入Type查詢帳號
     * 
     * @param cusidn
     * @param type
     * @return
     */
    public BaseResult N921_REST(String cusidn, String type) {
    	List<String> ehcacheTypeWhiltList = new ArrayList<String>();
    	//使用Ehcache之白名單
    	ehcacheTypeWhiltList.addAll(Arrays.asList(ACNO,OUT_ACNO,IN_ACNO,GOLD_ACNO,FX_OUT_ACNO,COLLATER,GOLD_RESERVATION_QOERY_N920));
    	if(ehcacheTypeWhiltList.indexOf(type) != -1) {
	        BaseResult bs = null;
	        try {
	        	bs = new BaseResult();
	        	BaseResult bsTmp = daoService.N921_REST_Ehcache(cusidn, type);
	        	bs.addAllData((Map<String,Object>)bsTmp.getData());
	        	bs.setMessage(bsTmp.getMsgCode(), bsTmp.getMessage());
	        	bs.setResult(bsTmp.getResult());
	        	bs.setPrevious(bsTmp.getPrevious());
	        	bs.setNext(bsTmp.getNext());
	        	log.debug(CodeUtil.toJson(bs.getData()));
	        } catch (Exception e) {
	            log.error("", e);
	            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
	            
	        }
	        this.getMessageByMsgCode(bs);
	        return bs;
        }else {
        	return N921_REST_No_Ehcache(cusidn,type);
        }
    }
    /**
     * 根據傳入Type查詢帳號
     * 
     * @param cusidn
     * @param type
     * @return
     */
    public BaseResult N921_REST_No_Ehcache(String cusidn, String type) {
        log.trace("N921_REST_No_Ehcache start");
        log.trace(ESAPIUtil.vaildLog("cusidn>>" + cusidn));
        log.trace(ESAPIUtil.vaildLog("type>>" + type));
        BaseResult bs = null;
        N921_REST_RQ rq = null;
        N921_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new N921_REST_RQ();
            // call REST API
            rq.setCUSIDN(cusidn);
            rq.setTYPE(type);
            rs = restutil.send(rq, N921_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N921_REST_RS >>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N921_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 
     * @param cusidn
     * @return
     */
    public BaseResult N924_REST(String cusidn) {
        log.trace("N924_REST start");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N924_REST_RQ rq = null;
        N924_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new N924_REST_RQ();
            // call REST API
            rq.setCUSIDN(cusidn);
            rs = restutil.send(rq, N924_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N924_REST_RS >>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N924_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 帳號查詢
     * 
     * @param cusidn
     * @param type
     * @return
     */
    public BaseResult N926_REST(String cusidn, String type) {
        log.trace("N926_REST start");
        log.trace(ESAPIUtil.vaildLog("cusidn>>{}"+ cusidn));
        log.trace("type>>{}", type);
        BaseResult bs = null;
        N926_REST_RQ rq = null;
        N926_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new N926_REST_RQ();
            // call REST API
            rq.setCUSIDN(cusidn);
            rq.setTYPE(type.toUpperCase());
            rs = restutil.send(rq, N926_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N926_REST_RS >>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N926_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N015
     * 
     * @param cusidn
     * @param acn    帳號
     * @return
     */
    public BaseResult call_N015_REST(String cusidn, String acn, String USERDATA_X50) {

        log.trace("call_N015_REST>>");
        BaseResult bs = null;
        N015_REST_RQ rq = null;
        N015_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N015_REST_RQ();
            rq.setCUSIDN(cusidn);
            rq.setACN(acn);
            rq.setUSERDATA_X50(USERDATA_X50);
            rs = restutil.send(rq, N015_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N015_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N015_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }


    /**
     * 
     * @param cusidn
     * @param type 帳號
     * @param reqParam
     * @return
     */
    public BaseResult N105_REST(String cusidn, String type, Map<String, String> reqParam) {

        log.trace("call_N105_REST>>");
        BaseResult bs = null;
        N105_REST_RQ rq = null;
        N105_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N105_REST_RQ();
            rq = CodeUtil.objectCovert(N105_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);
            rq.setTYPE(type);
            rs = restutil.send(rq, N105_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N105_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N105_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N192黃金存摺-定期定額查詢
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */
    public BaseResult N192_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N192_REST start");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = new BaseResult();
        N192_REST_RQ rq = null;
        N192_REST_RS rs = null;
        try {
            // call REST API
            rq = new N192_REST_RQ();
            rq = CodeUtil.objectCovert(N192_REST_RQ.class, reqParam);
            rq.setTXID("N192");
            log.trace(ESAPIUtil.vaildLog("N192_REST_RQ>>{}"+ rq));
            rs = restutil.send(rq, N192_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N192_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
                log.trace("bs.getResult>> {}", bs.getResult());
            } else {
                log.error("N192_REST is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N510外幣餘額查詢
     * 
     * @param sessionId
     * @return
     * @throws UnsupportedEncodingException
     */
    public BaseResult N510_REST(String cusidn, String acn) {
    	return N510_REST(cusidn, acn, null);
    }
    public BaseResult N510_REST(String cusidn, String acn, String isTxnlLog) {
        log.trace("N510_REST_No_Ehcache");
        BaseResult bs = null;
        N510_REST_RQ rq = null;
        N510_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N510_REST_RQ();
            rq.setCUSIDN(cusidn);
            rq.setACN(acn);
            if(isTxnlLog != null)
            	rq.setIsTxnlLog(isTxnlLog);
            rs = restutil.send(rq, N510_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N510_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }
    /**
     * N510外幣餘額查詢
     * 
     * @param sessionId
     * @return
     * @throws UnsupportedEncodingException
     */
    public BaseResult N510_NoTxn_REST(String cusidn, String acn) {
        log.trace("N510_REST_No_Ehcache");
        BaseResult bs = null;
        N510_REST_RQ rq = null;
        N510_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N510_REST_RQ();
            rq.setCUSIDN(cusidn);
            rq.setACN(acn);
            rq.setADOPID("N510_NoTxn");
            rs = restutil.send(rq, N510_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N510_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }
    /**
     * 外幣餘額查詢
     * 
     * @param cusidn
     * @return
     */

    public BaseResult N530_REST(String cusidn, String acn) {
    	return N530_REST(cusidn, acn, null);
    }
    public BaseResult N530_REST(String cusidn, String acn, String isTxnlLog) {
        log.trace("N530_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N530_REST_RQ rq = null;
        N530_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N530_REST_RQ();
            rq.setCUSIDN(cusidn);
            rq.setACN(acn);
            if(isTxnlLog != null)
            	rq.setIsTxnlLog(isTxnlLog);
            rs = restutil.send(rq, N530_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N530_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N530_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }
    /**
     * 外幣餘額查詢
     * 
     * @param cusidn
     * @return
     */
    public BaseResult N530_NoTxn_REST(String cusidn, String acn) {
        log.trace("N530_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N530_REST_RQ rq = null;
        N530_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N530_REST_RQ();
            rq.setCUSIDN(cusidn);
            rq.setACN(acn);
            rq.setADOPID("N530_NoTxn");
            rs = restutil.send(rq, N530_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N530_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N530_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }
    /**
     * 支存已兌現票據明細
     * 
     * @param cusidn
     * @return
     */

    public BaseResult N625_REST(String cusidn, String acn, Map<String, String> reqParam) {
        log.trace("N625_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N625_REST_RQ rq = null;
        N625_REST_RS rs = null;
        // 取得傳入資料
        reqParam = DateUtil.periodProcessing(reqParam);
        String fgperiod = reqParam.get("FGPERIOD");// 查詢時間
        String cmsdate = reqParam.get("CMSDATE"); // 日期(起)
        String cmedate = reqParam.get("CMEDATE"); // 日期(迄)
        String USERDATA_X50 = reqParam.get("USERDATA_X50"); // 日期(迄)
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N625_REST_RQ();
            rq.setCUSIDN(cusidn);
            rq.setACN(acn);
            rq.setFGPERIOD(fgperiod);
            rq.setCMSDATE(cmsdate);
            rq.setCMEDATE(cmedate);
            rq.setUSERDATA_X50(USERDATA_X50);
            rs = restutil.send(rq, N625_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N625_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N625_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N174
     * <p>
     * 轉入外匯綜存定存
     * 
     * @param reqParam
     * @return
     */
    @SuppressWarnings("static-access")
    public BaseResult N174_REST(Map<String, String> requestParam) {
        log.debug(ESAPIUtil.vaildLog("requestParam={}" + CodeUtil.toJson(requestParam)));

        BaseResult bs = null;
        N174_REST_RQ rq = null;
        N174_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = CodeUtil.objectCovert(N174_REST_RQ.class, requestParam);

            rs = restutil.send(rq, N174_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.debug(ESAPIUtil.vaildLog("rs={}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N174_REST is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }


    /**
     * 薪轉戶優惠次數查詢
     * 
     * @param cusidn
     * @return
     */
    public BaseResult N861_REST(String cusidn) {
        log.trace("N861_REST");
        log.trace(ESAPIUtil.vaildLog("cusidn>>{}"+ cusidn));
        BaseResult bs = null;
        N861_REST_RQ rq = null;
        N861_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N861_REST_RQ();
      
            rq.setCUSIDN(cusidn);
         
            rs = restutil.send(rq, N861_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N861_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
                
            } else {
                log.error("N861_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }


    /**
     * 中央登錄債券餘額
     * 
     * @param cusidn   統一編號
     * @param reqParam
     * @return
     */
    public BaseResult N870_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N870_REST");
        log.trace(ESAPIUtil.vaildLog("cusidn>>{}"+CodeUtil.toJson(cusidn)));
        BaseResult bs = null;
        N870_REST_RQ rq = null;
        N870_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N870_REST_RQ();
            rq.setCUSIDN(cusidn);
            rq.setACN1(reqParam.get("ACN1").equals("ALL") ? "" : reqParam.get("ACN1"));
            rq.setUSERDATA_X50(reqParam.get("USERDATA_X50"));
            if(reqParam.containsKey("isTxnlLog"))
            	rq.setIsTxnlLog(reqParam.get("isTxnlLog"));
            rs = restutil.send(rq, N870_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N870_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N870_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N997 REST
     */
    @SuppressWarnings("static-access")
    public BaseResult N997_REST(Map<String, String> reqParam) {
        log.trace(ESAPIUtil.vaildLog("reqParam={}"+ CodeUtil.toJson(reqParam)));
        BaseResult bs = null;
        N997_REST_RQ rq = null;
        N997_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new N997_REST_RQ();
            rq.setEXECUTEFUNCTION(reqParam.get("EXECUTEFUNCTION"));
            rq.setDPUSERID(reqParam.get("DPUSERID"));
            rq.setDPGONAME(reqParam.get("DPGONAME"));
            rq.setDPTRACNO(reqParam.get("DPTRACNO"));
            rq.setDPTRIBANK(reqParam.get("DPTRIBANK"));
            rq.setDPTRDACNO(reqParam.get("DPTRDACNO"));

            rs = restutil.send(rq, N997_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N997_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N997_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 基金下單歸戶帳號查詢
     */
    @SuppressWarnings("static-access")
    public BaseResult N922_REST(String UID) {
        log.debug(ESAPIUtil.vaildLog("UID={}"+UID));

        BaseResult bs = null;
        N922_REST_RQ rq = null;
        N922_REST_RS rs = null;

        try {
            bs = new BaseResult();
            rq = new N922_REST_RQ();
            rq.setCUSIDN(UID);

            rs = restutil.send(rq, N922_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.debug(ESAPIUtil.vaildLog("rs={}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N922_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 投資屬性查詢
     */
    @SuppressWarnings("static-access")
    public BaseResult N927_REST(String UID) {
        log.debug(ESAPIUtil.vaildLog("UID={}"+ UID));

        BaseResult bs = null;
        N927_REST_RQ rq = null;
        N927_REST_RS rs = null;

        try {
            bs = new BaseResult();
            rq = new N927_REST_RQ();
            rq.setCUSIDN(UID);

            rs = restutil.send(rq, N927_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.debug(ESAPIUtil.vaildLog("rs={}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N927_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N960申請小額信貸，客戶基本資料查詢
     */
    @SuppressWarnings("static-access")
    public BaseResult N960_REST(Map<String, String> reqParam) {
        log.debug(ESAPIUtil.vaildLog("reqParam >> " + CodeUtil.toJson(reqParam)));
        String cusidn = "";
        if (reqParam.containsKey("CUSIDN")) {
            cusidn = reqParam.get("CUSIDN");
        } else if (reqParam.containsKey("cusidn")) {
            cusidn = reqParam.get("cusidn");
        }
        log.debug(ESAPIUtil.vaildLog("UID >> " + cusidn));
        BaseResult bs = null;
        N960_REST_RQ rq = null;
        N960_REST_RS rs = null;

        try {
            bs = new BaseResult();
            rq = new N960_REST_RQ();
            rq.setCUSIDN(cusidn);

            if (reqParam.containsKey("NCHECKNB")) {
                rq.setNCHECKNB(reqParam.get("NCHECKNB"));
            }

            rs = restutil.send(rq, N960_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.debug(ESAPIUtil.vaildLog("rs={}"+ CodeUtil.toJson(rs)));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N960_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }
    
    //更新原住民 2019/09/02
    public BaseResult N960_REST_UPDATE_CUSNAME(Map<String, String> reqParam) {
    	 BaseResult bs = null;
    	  try {
              bs = new BaseResult();
              bs = N960_REST(reqParam);
            //如有原住民姓名,以原住民姓名優先顯示
  			if(StrUtil.isNotEmpty(((Map<String,String>)bs.getData()).get("CUSNAME"))) {
  				((Map<String,String>)bs.getData()).put("NAME",((Map<String,String>)bs.getData()).get("CUSNAME"));
  			}
    	  }catch (Exception e) {
              log.error("", e);
    	  }
    	  this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N960 客戶基本資料查詢
     * 
     * @param reqParam
     * @return
     */
    public BaseResult N960_BRHACC_REST(Map<String, String> reqParam) {
        BaseResult bs = new BaseResult();
        N960_BRHACC_REST_RQ rq = null;
        N960_BRHACC_REST_RS rs = null;
        try {
            rq = new N960_BRHACC_REST_RQ();
            rq = CodeUtil.objectCovert(N960_BRHACC_REST_RQ.class, reqParam);
            rs = restutil.send(rq, N960_BRHACC_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N934_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N960_BRHACC_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            //e.printStackTrace();
        	//Avoid Information Exposure Through an Error Message
        	log.error("call_N960 Error >> {}", e);
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }
    
    /**
     * N934 線上登錄/取消匯款通知作業
     * 
     * @param reqParam
     * @return
     */
    public BaseResult N934_REST(Map<String, String> reqParam) {
        BaseResult bs = new BaseResult();
        N934_REST_RQ rq = null;
        N934_REST_RS rs = null;
        try {
            rq = new N934_REST_RQ();
            rq = CodeUtil.objectCovert(N934_REST_RQ.class, reqParam);
            rs = restutil.send(rq, N934_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N934_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N934_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            //e.printStackTrace();
        	//Avoid Information Exposure Through an Error Message
        	log.error("call_N934 Error >> {}", e);
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * n935 匯入匯款設定通知查詢
     * 
     * @param reqParam
     * @return
     */
    public BaseResult N935_REST(Map<String, String> reqParam) {
        BaseResult bs = new BaseResult();
        log.trace("inward_notice_setting");
        N935_REST_RQ rq = null;
        N935_REST_RS rs = null;
        try {
            rq = new N935_REST_RQ();
            rq = CodeUtil.objectCovert(N935_REST_RQ.class, reqParam);
            rs = restutil.send(rq, N935_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N935_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N935_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            //e.printStackTrace();
        	log.error("call_N935 Error >> {}", e);
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * n911網路通行碼檢核
     * 
     * @param sessionId
     * @return
     */
    public BaseResult N911_REST(String cusidn, String userid, String pinnew) {
        log.trace("N911_REST");
        BaseResult bs = null;
        N911_REST_RQ rq = null;
        N911_REST_RS rs = null;

        try {
            bs = new BaseResult();

            rq = new N911_REST_RQ();
            rq.setCUSIDN(cusidn);
            rq.setUSERID(userid);
            rq.setPINNEW(pinnew);

            rs = restutil.send(rq, N911_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N911_REST_RS>>{}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N911_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }

        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * n912業務權限及約定功能
     * 
     * @param sessionId
     * @return
     */
    public BaseResult N912_REST(String cusidn) {
        log.trace("N912_REST");
        BaseResult bs = null;
        N912_REST_RQ rq = null;
        N912_REST_RS rs = null;

        try {
            bs = new BaseResult();

            rq = new N912_REST_RQ();
            rq.setCUSIDN(cusidn);

            rs = restutil.send(rq, N912_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N912_REST_RS>>{}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N912_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }

        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * n944簽入密碼變更
     * 
     * @param sessionId
     * @return
     */
    public BaseResult N944_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N944_REST");
        BaseResult bs = null;
        N944_REST_RQ rq = null;
        N944_REST_RS rs = null;

        // 取得傳入資料
        String pinOld = reqParam.get("PINOLD");
        String pinNew = reqParam.get("PINNEW");

        try {
            bs = new BaseResult();

            rq = new N944_REST_RQ();
            rq.setCUSIDN(cusidn);
            rq.setPINOLD(pinOld);
            rq.setPINNEW(pinNew);
            rq.setADOPID("N940_S1");

            rs = restutil.send(rq, N944_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N944_REST_RS>>{}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N944_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }

        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * n945交易密碼變更
     * 
     * @param sessionId
     * @return
     */
    public BaseResult N945_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N945_REST");
        BaseResult bs = null;
        N945_REST_RQ rq = null;
        N945_REST_RS rs = null;

        // 取得傳入資料
        String pinOld = reqParam.get("PINOLD");
        String pinNew = reqParam.get("PINNEW");

        try {
            bs = new BaseResult();

            rq = new N945_REST_RQ();
            rq.setCUSIDN(cusidn);
            rq.setPINOLD(pinOld);
            rq.setPINNEW(pinNew);
            rq.setADOPID("N940_T1");;

            rs = restutil.send(rq, N945_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N945_REST_RS>>{}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N945_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }

        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * n948客戶沿用舊密碼
     * 
     * @param sessionId
     * @return
     */
    public BaseResult N948_REST(String cusidn) {
        log.trace("N948_REST");
        BaseResult bs = null;
        N948_REST_RQ rq = null;
        N948_REST_RS rs = null;

        try {
            bs = new BaseResult();

            rq = new N948_REST_RQ();
            rq.setCUSIDN(cusidn);

            rs = restutil.send(rq, N948_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N948_REST_RS>>{}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N948_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }

        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 使用者名稱變更
     * 
     * @param cusidn 統一編號
     * @return
     */
    public BaseResult N942_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N942_REST");
        BaseResult bs = null;
        N942_REST_RQ rq = null;
        N942_REST_RS rs = null;

        // 取得傳入資料
        String olduid = reqParam.get("OLDUID");
        String newuid = reqParam.get("NEWUID");
        String fgtway = reqParam.get("FGTXWAY");

        try {
            bs = new BaseResult();
            rq = new N942_REST_RQ();
            rq.setCUSIDN(cusidn);
            rq.setOLDUID(olduid);
            rq.setNEWUID(newuid);
            rq.setADOPID("N9422");
            rq.setFGTXWAY(fgtway);
            rs = restutil.send(rq, N942_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N942_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
                bs.addData("NEWUID", newuid);
            } else {
                log.error("N942_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }

        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 使用者名稱變更 && 使用者SSL密碼檢查
     * 
     * @param cusidn 統一編號
     * @return
     */
    public BaseResult N9422_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N9422_REST");
        BaseResult bs = null;
        try {
            bs = new BaseResult();
            bs = N951_REST(cusidn, reqParam);
            // Map<String, Object> bsData = (Map<String, Object>)bs.getData();

            if (bs.getMsgCode().equals("0")) {
                bs = N942_REST(cusidn, reqParam);
            } else {
                log.trace(bs.getMsgCode());
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 使用者SSL密碼檢查
     * 
     * @param cusidn 統一編號
     * @return
     */
    public BaseResult N951_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N951_REST");
        BaseResult bs = null;
        N951_REST_RQ rq = null;
        N951_REST_RS rs = null;

        // password 交易密碼(SSL)
        String pinnew = reqParam.get("PINNEW");
        try {
            bs = new BaseResult();
            rq = new N951_REST_RQ();
            rq.setCUSIDN(cusidn);
            rq.setUID(cusidn);
            rq.setPINNEW(pinnew);
            rq.setCMPW(pinnew);
            rs = restutil.send(rq, N951_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N951_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N951_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }

        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 查詢持卡人身份證資料
     * 
     * @param cusidn 統一編號
     * @return
     */
    public BaseResult N953_REST(String acn) {
        log.trace("N953_REST");
        BaseResult bs = null;
        N953_REST_RQ rq = null;
        N953_REST_RS rs = null;

        try {
            bs = new BaseResult();
            rq = new N953_REST_RQ();
            rq.setACN(acn);
            ;
            rs = restutil.send(rq, N953_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N953_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N953_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }

        } catch (Exception e) {
            log.error("{}", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N1012 空白票據申請
     * 
     * @param reqParam
     * @return
     */
    public BaseResult N1012_REST(Map<String, String> reqParam) {
        BaseResult bs = new BaseResult();
        N1012_REST_RQ rq = null;
        N1012_REST_RS rs = null;
        try {
            rq = new N1012_REST_RQ();
            rq = CodeUtil.objectCovert(N1012_REST_RQ.class, reqParam);
            rs = restutil.send(rq, N1012_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N1012_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N1012_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            //e.printStackTrace();
        	log.error("call_N1012 Error >> {}", e);
            //log.error("", e);
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 取得防止重送代碼
     * 
     * @param sessionID
     * @return
     * @throws UnsupportedEncodingException
     */
    public BaseResult getTxToken() {
        BaseResult bs = null;
        try {
            bs = new BaseResult();
            String uuid = CodeUtil.generateUniqueId();
            bs.addData("TXTOKEN", uuid);
            bs.setResult(true);
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 台幣轉帳:可做預約，測試中需要Mac
     * 
     * @param reqParam
     * @return
     * @throws UnsupportedEncodingException
     */
    public BaseResult N070_REST(Map<String, String> reqParam) {
        log.trace("N070_REST");
        BaseResult bs = null;
        N070_REST_RQ rq = null;
        N070_REST_RS rs = null;

        try {
            bs = new BaseResult();
            rq = new N070_REST_RQ();
            rq = CodeUtil.objectCovert(N070_REST_RQ.class, reqParam);
//          TXNLOG  轉入帳號選擇非約定要做得特別處理
            if(StrUtil.isNotEmpty(reqParam.get("FGSVACNO")) && "2".equals(reqParam.get("FGSVACNO"))) {
            	rq.setADAGREEF("0");
            	if(StrUtil.isNotEmpty(reqParam.get("DPBHNO")) ) {
            		rq.setADSVBH(reqParam.get("DPBHNO"));
            	}
            }
            
            
            rs = restutil.send(rq, N070_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N070_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N070_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }

        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }
    

    /**
     * 台幣轉帳:可做預約，測試中需要Mac
     * 
     * @param reqParam
     * @return
     * @throws UnsupportedEncodingException
     */
    public BaseResult N070C_REST(Map<String, String> reqParam) {
        log.trace("N070C_REST");
        BaseResult bs = null;
        N070C_REST_RQ rq = null;
        N070C_REST_RS rs = null;

        try {
            bs = new BaseResult();
            rq = new N070C_REST_RQ();
            rq = CodeUtil.objectCovert(N070C_REST_RQ.class, reqParam);
//          TXNLOG  轉入帳號選擇非約定要做得特別處理
            if(StrUtil.isNotEmpty(reqParam.get("FGSVACNO")) && "2".equals(reqParam.get("FGSVACNO"))) {
            	rq.setADAGREEF("0");
            	if(StrUtil.isNotEmpty(reqParam.get("DPBHNO")) ) {
            		rq.setADSVBH(reqParam.get("DPBHNO"));
            	}
            }
            
            rs = restutil.send(rq, N070C_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N070C_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N070C_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 20190730 hugo 目前未使用 ，先註解 根據傳入Type查詢帳號
     * 
     * @param cusidn
     * @param type
     * @return2
     */
    // public BaseResult Cusidn_REST(String cusidn, String type) {
    // log.trace("Cusidn_REST start");
    // log.trace("cusidn>>{}", cusidn);
    // log.trace("type>>{}",type);
    // BaseResult bs = null;
    // Cusidn_REST_RQ rq = null;
    // Cusidn_REST_RS rs = null;
    // try {
    // bs = new BaseResult();
    // rq = new Cusidn_REST_RQ();
    // // call REST API
    // rq.setCUSIDN(cusidn);
    // rq.setTYPE(type.toUpperCase());
    // rs = restutil.send(rq, Cusidn_REST_RS.class,
    // rq.getAPI_Name(rq.getClass()));
    // log.trace("Cusidn_REST_RS >>{}", rs);
    // if (rs != null) {
    // CodeUtil.convert2BaseResult(bs, rs);
    // } else {
    // log.error("Cusidn_REST_RS is null");
    // bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
    // 
    // }
    // } catch (Exception e) {
    // log.error("", e);
    // bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
    // 
    // }
    // this.getMessageByMsgCode(bs);
//        return bs;
    // }

    /**
     * 約定轉入帳號查詢
     * 
     * @param sessionID
     * @param type      = "01" "02" "03"
     * @return
     */
    public BaseResult N215Q_REST(String cusidn) {
        log.trace("call_N215Q");
        N215Q_REST_RQ rq = null;
        N215Q_REST_RS rs = null;
        BaseResult bs = null;
        try {
            bs = new BaseResult();
            rq = new N215Q_REST_RQ();
            // call REST API
            rq.setCUSIDN(cusidn);
            rq.setUID(cusidn);
            rs = restutil.send(rq, N215Q_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N215Q_REST_RS >>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N215Q_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
               return bs;
    }

    public BaseResult N1011_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("call_N1011");
        N1011_REST_RQ rq = null;
        N1011_REST_RS rs = null;
        BaseResult bs = null;
        try {
            bs = new BaseResult();
            rq = new N1011_REST_RQ();
            String pinnew = reqParam.get("PINNEW");
            // call REST API
            rq = CodeUtil.objectCovert(N1011_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);
            rq.setPINNEW(pinnew);
            rs = restutil.send(rq, N1011_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N1011_REST_RS >>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N1011_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    public BaseResult N1014_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("call_N1014");
        N1014_REST_RQ rq = null;
        N1014_REST_RS rs = null;
        BaseResult bs = null;
        try {
            bs = new BaseResult();
            rq = new N1014_REST_RQ();
            // call REST API
            rq = CodeUtil.objectCovert(N1014_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);
            rs = restutil.send(rq, N1014_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N1014_REST_RS >>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N1014_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 根據傳入帳號去取得帳號餘額等資料
     * 
     * @param sessionId
     * @param accno
     * @return
     * @deprecated Use {@link #findOenByN110(String,String,String)} instead
     */
    public BaseResult findOenByN110(String cusidn, String acno) {
        return findOenByN110(cusidn, acno, null);
    }

    /**
     * 根據傳入帳號去取得帳號餘額等資料
     * 
     * @param ms_Channel
     * @param sessionId
     * @param accno
     * @return
     */
    public BaseResult findOenByN110(String cusidn, String acno, String ms_Channel) {
        log.trace(ESAPIUtil.vaildLog("findOenByN110 >> " + acno));
        List<Map<String, Object>> list = null;
        BaseResult bs = null;
        try {
            bs = new BaseResult();
            bs = N110_REST(cusidn, acno, ms_Channel);

            if (bs.getResult()) {
                // list =
                // CodeUtil.fromJson(((Map<String,Object>)bs.getData()).get("_Table").toString(),
                // List.class);
                list = (List<Map<String, Object>>) ((Map<String, Object>) bs.getData()).get("REC");
            }
            log.trace("list>>{}", list);
            if (list == null || list.isEmpty()) {
                throw new Exception("findOenByN110.list is null ");
            }

            for (Map<String, Object> map : list) {
                if (map.containsValue(acno)) {
                    log.trace("map>>{}", map);
                    bs.addData("accno_data", map);
                }
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 下期借款本息查詢
     * 
     * @param cusidn
     * @return
     * @throws UnsupportedEncodingException
     */
    public BaseResult call_N014_REST(Map<String, String> reqParam) {
        log.trace("N014_REST");
        log.debug(ESAPIUtil.vaildLog("reqParam >> " + CodeUtil.toJson(reqParam)));
        BaseResult bs = null;
        N014_REST_RQ rq = null;
        N014_REST_RS rs = null;
        String cusidn = reqParam.get("CUSIDN");
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N014_REST_RQ();
            rq.setCUSIDN(cusidn);
            rs = restutil.send(rq, N014_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N014_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            	//修改 Serializable Class Containing Sensitive Data
                Map<String,Object> callData = (Map<String,Object>)bs.getData();
                for(Map<String,String> callRow : (List<Map<String,String>>)callData.get("TW")) {
                	callRow.put("ITRUSSN", callRow.get("ITRUS"));
                }
            } else {
                log.error("N014_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;

    }

    public BaseResult call_N930EE_REST(Map<String, String> reqParam) {
        BaseResult bs = null;
        log.trace("N930EE_REST");
        N930EE_REST_RQ rq = null;
        N930EE_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N930EE_REST_RQ();

            rq.setFGTXWAY(reqParam.get("FGTXWAY"));
            rq.setDPUSERID(reqParam.get("DPUSERID"));
            rq.setEXECUTEFUNCTION(reqParam.get("EXECUTEFUNCTION"));
            rq.setNEW_EMAIL(reqParam.get("NEW_EMAIL"));
            rq.setPINNEW(reqParam.get("PINNEW"));
            rq.setCUSIDN(reqParam.get("DPUSERID"));
            rq.setFLAG(reqParam.get("FLAG"));
            rq.setCARDAP(reqParam.get("CARDAP"));
            rq.setIP(reqParam.get("IP"));
            rq.setSessionID(reqParam.get("sessionID"));
            rq.setIdgateID(reqParam.get("idgateID"));
            rq.setTxnID(reqParam.get("txnID"));
            rs = restutil.send(rq, N930EE_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N930EE_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N930EE_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;

    }

    /**
     * 根據傳入幣別去取得外幣帳號餘額等資料
     * 
     * @param cusidn
     * @param accno
     * @param cry
     * @return
     */
    public BaseResult findOenByN510(String cusidn, String acno, String cry) {
        log.trace("findOenByN510");
        log.trace(ESAPIUtil.vaildLog("findOenByN510.cusidn>>{}"+ cusidn));
        log.trace(ESAPIUtil.vaildLog("findOenByN510.acno >>{}"+ acno));
        log.trace(ESAPIUtil.vaildLog("findOenByN510.cry >>{}" +cry));
        List<Map<String, Object>> list = null;
        BaseResult bs = null;
        try {
            bs = new BaseResult();
            bs = N510_REST(cusidn, acno ,"N");

            if (bs.getResult()) {
                list = (List<Map<String, Object>>) ((Map<String, Object>) bs.getData()).get("ACNInfo");
            }
            log.trace("list>>{}", list);
            
            if (list == null || list.isEmpty()) {
                throw new Exception("findOenByN510.list is null ");
            }
            
            List<Map<String, Object>> filteredList = new ArrayList<>();
            
            // 撈全部外幣
            if("FX".equals(cry)) {
            	for (Map<String, Object> map : list) {
                    if (map.containsValue(acno) && !("TWD".equals(map.get("CUID")))) {
                        log.trace("map>>{}", map);
                        filteredList.add(map);
                        log.trace("findOenByN510 bs.addData >>>> {}", bs.getData());
                    }
                }
                bs.addData("accno_data", filteredList);
            } else {	// 撈特定幣別
            	for (Map<String, Object> map : list) {
                    if (map.containsValue(acno) && map.containsValue(cry)) {
                        log.trace("findOenByN510 bs.addData >>>> {}", bs.getData());
                        bs.addData("accno_data", map);
                    }
                }
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 信用卡總覽
     * 
     * @param cusidn
     * @return
     */
    public BaseResult N810_REST(Map<String, String> reqParam) {
        log.trace("N810_REST");
        log.trace(ESAPIUtil.vaildLog("cusidn >> " + reqParam.get("CUSIDN")));
        BaseResult bs = null;
        N810_REST_RQ rq = null;
        N810_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N810_REST_RQ();
            rq.setCUSIDN(reqParam.get("CUSIDN"));
            rq.setADOPID(reqParam.get("ADOPID"));
            if(reqParam.containsKey("isTxnlLog"))
            	rq.setIsTxnlLog(reqParam.get("isTxnlLog"));
            if (StrUtil.isNotEmpty(reqParam.get("CF"))) {
                rq.setCALLFROM(reqParam.get("CF"));
            }
            rs = restutil.send(rq, N810_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N810_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N810_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }
    /**
     * 信用卡總覽
     * 
     * @param cusidn
     * @return
     */
    public BaseResult N810_NoTxn_REST(Map<String, String> reqParam) {
        log.trace("N810_REST");
        log.trace(ESAPIUtil.vaildLog("cusidn >> " + reqParam.get("CUSIDN")));
        BaseResult bs = null;
        N810_REST_RQ rq = null;
        N810_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N810_REST_RQ();
            rq.setCUSIDN(reqParam.get("CUSIDN"));
            rq.setADOPID(reqParam.get("ADOPID"));
            rq.setADOPID("N810_NoTxn");
            if (StrUtil.isNotEmpty(reqParam.get("CF"))) {
                rq.setCALLFROM(reqParam.get("CF"));
            }
            rs = restutil.send(rq, N810_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N810_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N810_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }
    /**
     * N997常用帳號設定
     * 
     * @param cusidn
     * @return
     * @throws UnsupportedEncodingException
     */
    // public BaseResult N997_REST(String cusidn ,String acn)
    // {
    // log.trace("N997_REST");
    // BaseResult bs = null ;
    // N510_REST_RQ rq = null;
    // N510_REST_RS rs = null;
    // try {
    // bs = new BaseResult();
    // //call REST API
    // rq = new N997_REST_RQ();
    // rq.setCUSIDN(cusidn);
    // rq.setACN(acn);
    // rs = restutil.send(rq, N997_REST_RS.class,
    // rq.getAPI_Name(rq.getClass()));
    // if(rs != null) {
    // CodeUtil.convert2BaseResult(bs, rs);
    // }else {
    // log.error("N997_REST_RS is null");
    // bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
    // 
    // }
    // } catch (Exception e) {
    // log.error("",e);
    // bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
    // 
    // }
    // this.getMessageByMsgCode(bs);
//        return bs;
    // }

    /**
     * REST 信用卡未出帳單明細查詢_結果頁
     * 
     * @param sessionId
     * @param sessionpw
     * @return
     */
    public BaseResult TD01_REST(Map<String, String> reqParam) {
        // 電文TD01_1
        log.trace("TD01_REST...");

        BaseResult bs = null;
        TD01_REST_RQ rq = null;
        TD01_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new TD01_REST_RQ();
            rq = CodeUtil.objectCovert(TD01_REST_RQ.class, reqParam);
            rs = restutil.send(rq, TD01_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("TD01_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("TD01_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("{}", e);
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * REST 信用卡未出帳單明細查詢_EAIL頁
     * 
     * @param sessionId
     * @param sessionpw
     * @return
     */
    public BaseResult TD01_MAIL_REST(Map<String, String> reqParam) {
        // 電文TD01_1
        log.trace("TD01_MAIL_REST...");

        BaseResult bs = null;
        TD01_MAIL_REST_RQ rq = null;
        TD01_MAIL_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new TD01_MAIL_REST_RQ();
            rq = CodeUtil.objectCovert(TD01_MAIL_REST_RQ.class, reqParam);
            rs = restutil.send(rq, TD01_MAIL_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("TD01_MAIL_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("TD01_MAIL_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("{}", e);
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * REST 已繳款本息查詢_結果頁
     * 
     * @param cusidn
     * @param period
     * @return
     */
    public BaseResult N016_REST(String cusidn, String period) {
        // 電文N016
        log.trace("N016_REST...");

        BaseResult bs = null;
        N016_REST_RQ rq = null;
        N016_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N016_REST_RQ();
            rq.setCUSIDN(cusidn);
            rq.setPERIOD(period);
            rs = restutil.send(rq, N016_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N016_REST_RS>>"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N016_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("{}", e);
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 基金贖回查詢
     */
    @SuppressWarnings("static-access")
    public BaseResult C024_REST(String cusidn) {
        log.debug("cusidn={}", cusidn);

        BaseResult bs = null;
        C024_REST_RQ rq = null;
        C024_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new C024_REST_RQ();
            rq.setCUSIDN(cusidn);

            rs = restutil.send(rq, C024_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.debug(ESAPIUtil.vaildLog("rs={}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("C024_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 基金餘額及損益查詢
     */
    public BaseResult C012_REST(String cusidn) {
    	return C012_REST(cusidn, null);
    }
    @SuppressWarnings("static-access")
    public BaseResult C012_REST(String cusidn, String isTxnlLog) {
        log.debug("cusidn={}", cusidn);

        BaseResult bs = null;
        C012_REST_RQ rq = null;
        C012_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new C012_REST_RQ();
            rq.setCUSIDN(cusidn);
            if(isTxnlLog != null)
            	rq.setIsTxnlLog(isTxnlLog);

            rs = restutil.send(rq, C012_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.debug(ESAPIUtil.vaildLog("rs={}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("C012_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }
    /**
     * 基金餘額及損益查詢
     */
    @SuppressWarnings("static-access")
    public BaseResult C012_NoTxn_REST(String cusidn) {
        log.debug("cusidn={}", cusidn);

        BaseResult bs = null;
        C012_REST_RQ rq = null;
        C012_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new C012_REST_RQ();
            rq.setCUSIDN(cusidn);
            rq.setADOPID("C012_NoTxn");
            rs = restutil.send(rq, C012_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.debug(ESAPIUtil.vaildLog("rs={}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("C012_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 基金交易資料查詢
     * 
     * @param cusidn
     * @param begData 查詢起日 yyyMMdd
     * @param endDate 查詢迄日 yyyMMdd
     * @return
     */
    public BaseResult C013_REST(C013_REST_RQ rq) {
        log.debug(ESAPIUtil.vaildLog("C013_REST_RQ params >>"+ CodeUtil.toJson(rq)));

        BaseResult bs = new BaseResult();
        C013_REST_RS rs = null;
        try {

            rs = restutil.send(rq, C013_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.debug(ESAPIUtil.vaildLog("C013_REST_RS >> {}"+ CodeUtil.toJson(rs)));

            if (rs == null) {
                log.error("C013_REST_RS is null");
                throw new NullPointerException();
            }

            CodeUtil.convert2BaseResult(bs, rs);
            bs.addData("RS", rs);

            // 取得使用者名稱
            BaseResult n927 = N927_REST(rq.getCUSIDN());
            if (n927.getResult()) {
                Map<String, String> bsData = (Map<String, String>) n927.getData();
                String name = bsData.get("NAME");
                //跟N927的CUSNAME原住民姓名沒有關係
                rs.setCUSNAME(name);
            } else {
                log.warn("N927 取得使用者名稱失敗");
            }
        } catch (Exception e) {
            log.error("C013_REST Error", e);
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * C013 進一步查詢 單筆交易明細
     * 
     * @param rq
     * @return
     */
    public BaseResult C014_REST(C014_REST_RQ rq) {
        log.debug(ESAPIUtil.vaildLog("C014_REST_RQ params >>"+CodeUtil.toJson(rq)));

        BaseResult bs = new BaseResult();
        C014_REST_RS rs = null;
        try {
            rs = restutil.send(rq, C014_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.debug(ESAPIUtil.vaildLog("C014_REST_RS >> {}"+ CodeUtil.toJson(rs)));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
                bs.addData("RS", rs);
            } else {
                log.error("C014_REST_RS is null");
            }
        } catch (Exception e) {
            log.error("C014_REST Error", e);
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 基金轉換交易查詢
     */
    @SuppressWarnings("static-access")
    public BaseResult C021_REST(String cusidn) {
        log.debug("cusidn={}", cusidn);

        BaseResult bs = null;
        C021_REST_RQ rq = null;
        C021_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new C021_REST_RQ();
            rq.setCUSIDN(cusidn);

            rs = restutil.send(rq, C021_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.debug(ESAPIUtil.vaildLog("rs={}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("C021_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 執行個人KYC變更
     */
    @SuppressWarnings("static-access")
    public BaseResult InvAttribS_REST(Map<String, String> requestParam) {
    	log.debug(ESAPIUtil.vaildLog("requestParam={}"+CodeUtil.toJson(requestParam)));

        BaseResult bs = null;
        InvAttrib_S_REST_RQ rq = null;
        InvAttrib_S_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new InvAttrib_S_REST_RQ();
            rq.setADOPID("InvestAttr");
            rq.setCUSIDN(requestParam.get("CUSIDN"));
            rq.setFGTXWAY(requestParam.get("FGTXWAY"));
            rq.setPINNEW(requestParam.get("PINNEW"));
            rq.setRTC(requestParam.get("RTC"));
            rq.setUPD_FLG(requestParam.get("UPD_FLG"));
            rq.setDEGREE(requestParam.get("DEGREE"));
            rq.setCAREER(requestParam.get("CAREER"));
            rq.setSALARY(requestParam.get("SALARY"));
            rq.setEDITON(requestParam.get("EDITON"));
            rq.setANSWER(requestParam.get("ANSWER"));
            rq.setFDMARK1(requestParam.get("FDMARK1"));
            rq.setFDQ1(requestParam.get("FDQ1"));
            rq.setFDQ2(requestParam.get("FDQ2"));
            rq.setFDQ3(requestParam.get("FDQ3"));
            rq.setFDQ4(requestParam.get("FDQ4"));
            rq.setFDQ5(requestParam.get("FDQ5"));
            rq.setFDQ6(requestParam.get("FDQ6"));
            rq.setFDQ7(requestParam.get("FDQ7"));
            rq.setFDQ8(requestParam.get("FDQ8"));
            rq.setFDQ9(requestParam.get("FDQ9"));
            rq.setFDQ10(requestParam.get("FDQ10"));
            rq.setFDQ11(requestParam.get("FDQ11"));
            rq.setFDQ12(requestParam.get("FDQ12"));
            rq.setFDQ13(requestParam.get("FDQ13"));
            rq.setFDQ14(requestParam.get("FDQ14"));
            rq.setFDQ15(requestParam.get("FDQ15"));
            rq.setFDSCORE(requestParam.get("FDSCORE"));
            rq.setFDINVTYPE(requestParam.get("FDINVTYPE"));
            rq.setiSeqNo(requestParam.get("iSeqNo"));
            rq.setISSUER(requestParam.get("ISSUER"));
            rq.setACNNO(requestParam.get("ACNNO"));
            rq.setTRMID(requestParam.get("TRMID"));
            rq.setICSEQ(requestParam.get("ICSEQ"));
            rq.setTAC(requestParam.get("TAC"));
            rq.setPkcs7Sign(requestParam.get("pkcs7Sign"));
            rq.setJsondc(requestParam.get("jsondc"));
            rq.setTYPE(requestParam.get("TYPE"));
            rq.setADUSERIP(requestParam.get("ADUSERIP"));
            rq.setAGREE(requestParam.get("AGREE"));
            rq.setDEVNAME(requestParam.get("DEVNAME"));
            rq.setPCMAC(requestParam.get("PCMAC"));
            rq.setFDHISTID(requestParam.get("FDHISTID"));
            rq.setSessionID(requestParam.get("sessionID"));
            rq.setIdgateID(requestParam.get("idgateID"));
            rq.setTxnID(requestParam.get("txnID"));
            rs = restutil.send(rq, InvAttrib_S_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.debug(ESAPIUtil.vaildLog("rs={}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("InvAttribS_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 申購交易查詢，手續費檢核
     */
    @SuppressWarnings("static-access")
    public BaseResult C016Public_REST(Map<String, String> requestParam) {
        log.debug(ESAPIUtil.vaildLog("requestParam={}"+CodeUtil.toJson(requestParam)));

        BaseResult bs = null;
        C016_Public_REST_RQ rq = null;
        C016_Public_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new C016_Public_REST_RQ();
            rq.setCUSIDN(requestParam.get("CUSIDN"));
            rq.setTRANSCODE(requestParam.get("TRANSCODE"));
            rq.setCOUNTRYTYPE(requestParam.get("COUNTRYTYPE"));
            rq.setAMT3(requestParam.get("AMT3"));
            rq.setBILLSENDMODE(requestParam.get("BILLSENDMODE"));
            rq.setBRHCOD(requestParam.get("BRHCOD"));
            rq.setCUTTYPE(requestParam.get("CUTTYPE"));
            rq.setHTELPHONE(requestParam.get("HTELPHONE"));
            rq.setSTOP(requestParam.get("STOP"));
            rq.setYIELD(requestParam.get("YIELD"));
            rq.setFDAGREEFLAG(requestParam.get("FDAGREEFLAG"));
            rq.setFDNOTICETYPE(requestParam.get("FDNOTICETYPE"));
            rq.setFDPUBLICTYPE(requestParam.get("FDPUBLICTYPE"));
            rq.setFUNDLNAME(requestParam.get("FUNDLNAME"));
            rq.setPAYDAY1(requestParam.get("PAYDAY1"));
            rq.setPAYDAY2(requestParam.get("PAYDAY2"));
            rq.setPAYDAY3(requestParam.get("PAYDAY3"));
            rq.setPAYDAY4(requestParam.get("PAYDAY4"));
            rq.setPAYDAY5(requestParam.get("PAYDAY5"));
            rq.setPAYDAY6(requestParam.get("PAYDAY6"));
            rq.setPAYDAY7(requestParam.get("PAYDAY7"));
            rq.setPAYDAY8(requestParam.get("PAYDAY8"));
            rq.setPAYDAY9(requestParam.get("PAYDAY9"));
            
            rs = restutil.send(rq, C016_Public_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.debug(ESAPIUtil.vaildLog("rs={}"+ rs));
            
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("C016Public_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 預約申購交易查詢，手續費檢核
     */
    @SuppressWarnings("static-access")
    public BaseResult C031Public_REST(Map<String, String> requestParam) {
        log.debug(ESAPIUtil.vaildLog("requestParam={}"+CodeUtil.toJson(requestParam)));

        BaseResult bs = null;
        C031_Public_REST_RQ rq = null;
        C031_Public_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new C031_Public_REST_RQ();
            rq.setCUSIDN(requestParam.get("CUSIDN"));
            rq.setTRANSCODE(requestParam.get("TRANSCODE"));
            rq.setCOUNTRYTYPE(requestParam.get("COUNTRYTYPE"));
            rq.setAMT3(requestParam.get("AMT3"));
            rq.setBILLSENDMODE(requestParam.get("BILLSENDMODE"));
            rq.setBRHCOD(requestParam.get("BRHCOD"));
            rq.setCUTTYPE(requestParam.get("CUTTYPE"));
            rq.setHTELPHONE(requestParam.get("HTELPHONE"));
            rq.setSTOP(requestParam.get("STOP"));
            rq.setYIELD(requestParam.get("YIELD"));
            rq.setFDAGREEFLAG(requestParam.get("FDAGREEFLAG"));
            rq.setFDNOTICETYPE(requestParam.get("FDNOTICETYPE"));
            rq.setFDPUBLICTYPE(requestParam.get("FDPUBLICTYPE"));
            rq.setFUNDLNAME(requestParam.get("FUNDLNAME"));
            rq.setSALESNO(requestParam.get("SALESNO"));
            rq.setSLSNO(requestParam.get("SLSNO"));
            rq.setKYCNO(requestParam.get("KYCNO"));

            rs = restutil.send(rq, C031_Public_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.debug(ESAPIUtil.vaildLog("rs={}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("C031Public_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 定期申購交易查詢，手續費檢核
     */
    @SuppressWarnings("static-access")
    public BaseResult C017Public_REST(Map<String, String> requestParam) {
        log.debug(ESAPIUtil.vaildLog("requestParam={}"+ requestParam));

        BaseResult bs = null;
        C017_Public_REST_RQ rq = null;
        C017_Public_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new C017_Public_REST_RQ();
            rq.setCUSIDN(requestParam.get("CUSIDN"));
            rq.setTRANSCODE(requestParam.get("TRANSCODE"));
            rq.setCOUNTRYTYPE(requestParam.get("COUNTRYTYPE"));
            rq.setAMT3(requestParam.get("AMT3"));
            rq.setFUNDACN(requestParam.get("FUNDACN"));
            rq.setPAYTYPE(requestParam.get("PAYTYPE"));
            rq.setBILLSENDMODE(requestParam.get("BILLSENDMODE"));
            rq.setPAYDAY1(requestParam.get("PAYDAY1"));
            rq.setPAYDAY2(requestParam.get("PAYDAY2"));
            rq.setPAYDAY3(requestParam.get("PAYDAY3"));
            rq.setBRHCOD(requestParam.get("BRHCOD"));
            rq.setCUTTYPE(requestParam.get("CUTTYPE"));
            rq.setHTELPHONE(requestParam.get("HTELPHONE"));
            rq.setSTOP(requestParam.get("STOP"));
            rq.setYIELD(requestParam.get("YIELD"));
            rq.setPAYDAY4(requestParam.get("PAYDAY4"));
            rq.setPAYDAY5(requestParam.get("PAYDAY5"));
            rq.setPAYDAY6(requestParam.get("PAYDAY6"));
            rq.setMIP(requestParam.get("MIP"));
            rq.setPAYDAY7(requestParam.get("PAYDAY7"));
            rq.setPAYDAY8(requestParam.get("PAYDAY8"));
            rq.setPAYDAY9(requestParam.get("PAYDAY9"));
            rq.setFDAGREEFLAG(requestParam.get("FDAGREEFLAG"));
            rq.setFDNOTICETYPE(requestParam.get("FDNOTICETYPE"));
            rq.setFDPUBLICTYPE(requestParam.get("FDPUBLICTYPE"));
            rq.setFUNDLNAME(requestParam.get("FUNDLNAME"));

            rs = restutil.send(rq, C017_Public_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.debug(ESAPIUtil.vaildLog("rs={}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("C017Public_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 轉換交易查詢，手續費檢核
     */
    @SuppressWarnings("static-access")
    public BaseResult C022Public_REST(Map<String, String> requestParam) {
    	log.debug(ESAPIUtil.vaildLog("requestParam={}"+CodeUtil.toJson(requestParam)));

        BaseResult bs = null;
        C022_Public_REST_RQ rq = null;
        C022_Public_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new C022_Public_REST_RQ();
            rq.setCUSIDN(requestParam.get("CUSIDN"));
            rq.setINTRANSCODE(requestParam.get("INTRANSCODE"));
            rq.setFUNDAMT(requestParam.get("FUNDAMTNoComma"));
            rq.setUNIT(requestParam.get("UNITNoDot"));
            rq.setBILLSENDMODE(requestParam.get("BILLSENDMODE"));
            rq.setSHORTTUNIT(requestParam.get("SHORTTUNIT"));
            rq.setSHORTTRADE(requestParam.get("SHORTTRADE"));
            rq.setTRANSCODE(requestParam.get("TRANSCODE"));
            rq.setCDNO(requestParam.get("CDNO"));
            rq.setFDAGREEFLAG(requestParam.get("FDAGREEFLAG"));
            rq.setFDNOTICETYPE(requestParam.get("FDNOTICETYPE"));
            rq.setFDPUBLICTYPE(requestParam.get("FDPUBLICTYPE"));

            rs = restutil.send(rq, C022_Public_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.debug(ESAPIUtil.vaildLog("rs={}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("C022Public_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 預約轉換交易查詢，手續費檢核
     */
    @SuppressWarnings("static-access")
    public BaseResult C032Public_REST(Map<String, String> requestParam) {
        log.debug(ESAPIUtil.vaildLog("requestParam={}"+CodeUtil.toJson(requestParam)));

        BaseResult bs = null;
        C032_Public_REST_RQ rq = null;
        C032_Public_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new C032_Public_REST_RQ();
            rq.setCUSIDN(requestParam.get("CUSIDN"));
            rq.setINTRANSCODE(requestParam.get("INTRANSCODE"));
            rq.setFUNDAMT(requestParam.get("FUNDAMTNoComma"));
            rq.setUNIT(requestParam.get("UNITNoDot"));
            rq.setBILLSENDMODE(requestParam.get("BILLSENDMODE"));
            rq.setSHORTTUNIT(requestParam.get("SHORTTUNIT"));
            rq.setSHORTTRADE(requestParam.get("SHORTTRADE"));
            rq.setTRANSCODE(requestParam.get("TRANSCODE"));
            rq.setCDNO(requestParam.get("CDNO"));
            rq.setFDAGREEFLAG(requestParam.get("FDAGREEFLAG"));
            rq.setFDNOTICETYPE(requestParam.get("FDNOTICETYPE"));
            rq.setFDPUBLICTYPE(requestParam.get("FDPUBLICTYPE"));
            rq.setFUNDLNAME(requestParam.get("FUNDLNAME"));
            

            rs = restutil.send(rq, C032_Public_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.debug(ESAPIUtil.vaildLog("rs={}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("C032Public_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 基金申購交易
     */
    @SuppressWarnings("static-access")
    public BaseResult N370_REST(Map<String, String> requestParam) {
        log.debug(ESAPIUtil.vaildLog("requestParam=>>"+ requestParam));

        BaseResult bs = null;
        N370_REST_RQ rq = null;
        N370_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new N370_REST_RQ();
            rq.setCUSIDN(requestParam.get("CUSIDN"));
            rq.setFGTXWAY(requestParam.get("FGTXWAY"));
            rq.setPINNEW(requestParam.get("PINNEW"));
            rq.setTYPE(requestParam.get("TYPE"));
            rq.setTRANSCODE(requestParam.get("TRANSCODE"));
            rq.setCOUNTRYTYPE(requestParam.get("COUNTRYTYPE"));
            rq.setTRADEDATE(requestParam.get("TRADEDATE"));
            rq.setAMT3(requestParam.get("AMT3"));
            rq.setFCA2(requestParam.get("FCA2"));
            rq.setAMT5(requestParam.get("AMT5"));
            rq.setOUTACN(requestParam.get("OUTACN"));
            rq.setINTSACN(requestParam.get("INTSACN"));
            rq.setBILLSENDMODE(requestParam.get("BILLSENDMODE"));
            rq.setFCAFEE(requestParam.get("FCAFEE"));
            rq.setSSLTXNO(requestParam.get("SSLTXNO"));
            rq.setPAYDAY1(requestParam.get("PAYDAY1"));
            rq.setPAYDAY2(requestParam.get("PAYDAY2"));
            rq.setPAYDAY3(requestParam.get("PAYDAY3"));
            rq.setBRHCOD(requestParam.get("BRHCOD"));
            rq.setCUTTYPE(requestParam.get("CUTTYPE"));
            rq.setCRY1(requestParam.get("CRY1"));
            rq.setHTELPHONE(requestParam.get("HTELPHONE"));
            rq.setDBDATE(requestParam.get("DBDATE"));
            rq.setSALESNO(requestParam.get("SALESNO"));
            rq.setYIELD(requestParam.get("YIELD"));
            rq.setSTOP(requestParam.get("STOP"));
            rq.setPAYDAY4(requestParam.get("PAYDAY4"));
            rq.setPAYDAY5(requestParam.get("PAYDAY5"));
            rq.setPAYDAY6(requestParam.get("PAYDAY6"));
            rq.setMIP(requestParam.get("MIP"));
            rq.setPRO(requestParam.get("PRO"));
            rq.setNUM(requestParam.get("NUM"));
            rq.setIP(requestParam.get("IP"));
            String KYCDATE = requestParam.get("KYCDATE");
            if (KYCDATE != null) {
                rq.setKYCDATE(KYCDATE);
            }
            String WEAK = requestParam.get("WEAK");
            if (WEAK != null) {
                rq.setWEAK(WEAK);
            }
            rq.setRSKATT(requestParam.get("RSKATT"));
            rq.setRRSK(requestParam.get("RRSK"));
            rq.setPAYDAY7(requestParam.get("PAYDAY7"));
            rq.setPAYDAY8(requestParam.get("PAYDAY8"));
            rq.setPAYDAY9(requestParam.get("PAYDAY9"));
            rq.setFDAGREEFLAG(requestParam.get("FDAGREEFLAG"));
            rq.setFDNOTICETYPE(requestParam.get("FDNOTICETYPE"));
            rq.setFDPUBLICTYPE(requestParam.get("FDPUBLICTYPE"));
            rq.setDPMYEMAIL(requestParam.get("DPMYEMAIL"));
            rq.setCMTRMAIL(requestParam.get("DPMYEMAIL"));
            rq.setSLSNO(requestParam.get("SLSNO"));
            rq.setKYCNO(requestParam.get("KYCNO"));
            rq.setOFLAG(requestParam.get("OFLAG"));
            
            //for txnlog
            rq.setADSVBH("050");
            
            //20210706新增 將基金前收後收帶到後面修改信件值
            rq.setFEE_TYPE(requestParam.get("FEE_TYPE"));

            rs = restutil.send(rq, N370_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.debug(ESAPIUtil.vaildLog("rs={}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N370_REST is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 基金預約申購交易
     */
    @SuppressWarnings("static-access")
    public BaseResult N372_REST(Map<String, String> requestParam) {
        log.debug(ESAPIUtil.vaildLog("requestParam={}"+CodeUtil.toJson(requestParam)));

        BaseResult bs = null;
        N372_REST_RQ rq = null;
        N372_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new N372_REST_RQ();
            rq.setCUSIDN(requestParam.get("CUSIDN"));
            rq.setFGTXWAY(requestParam.get("FGTXWAY"));
            rq.setPINNEW(requestParam.get("PINNEW"));
            rq.setTYPE(requestParam.get("TYPE"));
            rq.setTRANSCODE(requestParam.get("TRANSCODE"));
            rq.setCOUNTRYTYPE(requestParam.get("COUNTRYTYPE"));
            rq.setTRADEDATE(requestParam.get("TRADEDATE"));
            rq.setAMT3(requestParam.get("AMT3"));
            rq.setFCA2(requestParam.get("FCA2"));
            rq.setAMT5(requestParam.get("AMT5"));
            rq.setOUTACN(requestParam.get("OUTACN"));
            rq.setINTSACN(requestParam.get("INTSACN"));
            rq.setBILLSENDMODE(requestParam.get("BILLSENDMODE"));
            rq.setFCAFEE(requestParam.get("FCAFEE"));
            rq.setSSLTXNO(requestParam.get("SSLTXNO"));
            rq.setPAYDAY1(requestParam.get("PAYDAY1"));
            rq.setPAYDAY2(requestParam.get("PAYDAY2"));
            rq.setPAYDAY3(requestParam.get("PAYDAY3"));
            rq.setBRHCOD(requestParam.get("BRHCOD"));
            rq.setCUTTYPE(requestParam.get("CUTTYPE"));
            rq.setCRY1(requestParam.get("CRY1"));
            rq.setYIELD(requestParam.get("YIELD"));
            rq.setSTOP(requestParam.get("STOP"));
            rq.setIP(requestParam.get("IP"));
            rq.setADREQTYPE("S");
            String KYCDATE = requestParam.get("KYCDATE");
            if (KYCDATE != null) {
                rq.setKYCDATE(KYCDATE);
            }
            String WEAK = requestParam.get("WEAK");
            if (WEAK != null) {
                rq.setWEAK(WEAK);
            }
            rq.setRSKATT(requestParam.get("RSKATT"));
            rq.setRRSK(requestParam.get("RRSK"));
            rq.setFDAGREEFLAG(requestParam.get("FDAGREEFLAG"));
            rq.setFDNOTICETYPE(requestParam.get("FDNOTICETYPE"));
            rq.setFDPUBLICTYPE(requestParam.get("FDPUBLICTYPE"));
            rq.setDPMYEMAIL(requestParam.get("DPMYEMAIL"));
            rq.setCMTRMAIL(requestParam.get("DPMYEMAIL"));
            rq.setNUM(requestParam.get("NUM"));
            rq.setXFLAG(requestParam.get("XFLAG"));
            rq.setREPID(requestParam.get("REPID"));
            rq.setPEMAIL(requestParam.get("PEMAIL"));
            rq.setSLSNO(requestParam.get("SLSNO"));
            rq.setOFLAG(requestParam.get("OFLAG"));
         
            //for txnlog
            rq.setADSVBH("050");

            rs = restutil.send(rq, N372_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.debug(ESAPIUtil.vaildLog("rs={}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N372_REST is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 基金轉換交易
     */
    @SuppressWarnings("static-access")
    public BaseResult N371_REST(Map<String, String> requestParam) {
    	log.debug(ESAPIUtil.vaildLog("requestParam={}"+CodeUtil.toJson(requestParam)));

        BaseResult bs = null;
        N371_REST_RQ rq = null;
        N371_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new N371_REST_RQ();
            rq.setCUSIDN(requestParam.get("CUSIDN"));
            rq.setFGTXWAY(requestParam.get("FGTXWAY"));
            rq.setPINNEW(requestParam.get("PINNEW"));
            rq.setTYPE(requestParam.get("TYPE"));
            rq.setTRANSCODE(requestParam.get("TRANSCODE"));
            rq.setCDNO(requestParam.get("CDNO"));
            rq.setTRADEDATE(requestParam.get("TRADEDATE"));
            rq.setINTRANSCODE(requestParam.get("INTRANSCODE"));
            rq.setUNIT(requestParam.get("UNIT"));
            rq.setFCA1(requestParam.get("FCA1"));
            rq.setAMT3(requestParam.get("AMT3"));
            rq.setFCA2(requestParam.get("FCA2"));
            rq.setAMT5(requestParam.get("AMT5"));
            rq.setOUTACN(requestParam.get("OUTACN"));
            rq.setINTSACN(requestParam.get("INTSACN"));
            rq.setBILLSENDMODE(requestParam.get("BILLSENDMODE"));
            rq.setSSLTXNO(requestParam.get("SSLTXNO"));
            rq.setFUNDAMT(requestParam.get("FUNDAMT"));
            rq.setSHORTTRADE(requestParam.get("SHORTTRADE"));
            rq.setSHORTTUNIT(requestParam.get("SHORTTUNIT"));
            rq.setPRO(requestParam.get("PRO"));
            rq.setKYCDATE(requestParam.get("KYCDATE"));
            rq.setWEAK(requestParam.get("WEAK"));
            rq.setRSKATT(requestParam.get("RSKATT"));
            rq.setRRSK(requestParam.get("RRSK"));
            rq.setFDAGREEFLAG(requestParam.get("FDAGREEFLAG"));
            rq.setFDNOTICETYPE(requestParam.get("FDNOTICETYPE"));
            rq.setFDPUBLICTYPE(requestParam.get("FDPUBLICTYPE"));
            rq.setDPMYEMAIL(requestParam.get("DPMYEMAIL"));
            rq.setCMTRMAIL(requestParam.get("DPMYEMAIL"));
            rq.setIP(requestParam.get("IP"));
            //for txnlog
            rq.setADSVBH("050");
            rq.setADCURRENCY("TWD");

            rs = restutil.send(rq, N371_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.debug(ESAPIUtil.vaildLog("rs={}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N371_REST is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 基金預約轉換交易
     */
    @SuppressWarnings("static-access")
    public BaseResult N373_REST(Map<String, String> requestParam) {
    	log.debug(ESAPIUtil.vaildLog("requestParam={}"+CodeUtil.toJson(requestParam)));

        BaseResult bs = null;
        N373_REST_RQ rq = null;
        N373_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new N373_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), requestParam);
            //for txnlog
            rq.setADSVBH("050");
            rq.setADCURRENCY("TWD");

            rs = restutil.send(rq, N373_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.debug(ESAPIUtil.vaildLog("rs={}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N373_REST is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 基金預約贖回交易
     */
    @SuppressWarnings("static-access")
    public BaseResult N374_REST(Map<String, String> requestParam) {
        log.debug(ESAPIUtil.vaildLog("requestParam={}"+ CodeUtil.toJson(requestParam)));

        BaseResult bs = null;
        N374_REST_RQ rq = null;
        N374_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new N374_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), requestParam);
            //for txnlog
            rq.setADSVBH("050");

            rq.setFUNDAMT(requestParam.get("FUNDAMTNoComma") + ".00");
            rq.setUNIT(requestParam.get("UNITOriginal"));

            rs = restutil.send(rq, N374_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.debug(ESAPIUtil.vaildLog("rs={}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N374_REST is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 基金贖回交易
     */
    @SuppressWarnings("static-access")
    public BaseResult C114_REST(Map<String, String> requestParam) {
        log.debug(ESAPIUtil.vaildLog("requestParam={}"+ requestParam));

        BaseResult bs = null;
        C114_REST_RQ rq = null;
        C114_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new C114_REST_RQ();
            rq.setCUSIDN(requestParam.get("CUSIDN"));
            rq.setFGTXWAY(requestParam.get("FGTXWAY"));
            rq.setPINNEW(requestParam.get("PINNEW"));
            rq.setTRANSCODE(requestParam.get("TRANSCODE"));
            rq.setCDNO(requestParam.get("CDNO"));
            rq.setRESTOREDAY(requestParam.get("RESTOREDAY"));
            rq.setBILLSENDMODE(requestParam.get("BILLSENDMODE"));
            rq.setUNIT(requestParam.get("UNITOriginal"));
            rq.setFUNDACN(requestParam.get("FUNDACN"));
            rq.setBANKID(requestParam.get("BANKID"));
            rq.setFUNDAMT(requestParam.get("FUNDAMTNoComma"));
            rq.setSHORTTRADE(requestParam.get("SHORTTRADE"));
            rq.setSHORTTUNIT(requestParam.get("SHORTTUNIT"));
            rq.setDPMYEMAIL(requestParam.get("DPMYEMAIL"));
            rq.setCMTRMAIL(requestParam.get("CMTRMAIL"));
            rq.setIP(requestParam.get("IP"));
            //for txnlog
            rq.setADSVBH("050");
            rq.setCRY(requestParam.get("CRY"));

            rs = restutil.send(rq, C114_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.debug(ESAPIUtil.vaildLog("rs={}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("C114_REST is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 基金贖回終止扣款
     */
    @SuppressWarnings("static-access")
    public BaseResult C112_REST(Map<String, String> requestParam) {
        log.debug(ESAPIUtil.vaildLog("requestParam={}"+ CodeUtil.toJson(requestParam)));

        BaseResult bs = null;
        C112_REST_RQ rq = null;
        C112_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // 組上行
            rq = new C112_REST_RQ();
            rq = CodeUtil.objectCovert(C112_REST_RQ.class, requestParam);
            // 取得回應
            rs = restutil.send(rq, C112_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.debug(ESAPIUtil.vaildLog("rs={}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("C112_REST is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 基金定期申購交易
     */
    @SuppressWarnings("static-access")
    public BaseResult C020_REST(Map<String, String> requestParam) {
        log.debug(ESAPIUtil.vaildLog("requestParam={}"+CodeUtil.toJson(requestParam)));

        BaseResult bs = null;
        C020_REST_RQ rq = null;
        C020_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new C020_REST_RQ();
            rq.setCUSIDN(requestParam.get("CUSIDN"));
            rq.setFGTXWAY(requestParam.get("FGTXWAY"));
            rq.setPINNEW(requestParam.get("PINNEW"));
            rq.setTYPE(requestParam.get("TYPE"));
            rq.setTRANSCODE(requestParam.get("TRANSCODE"));
            rq.setCOUNTRYTYPE(requestParam.get("COUNTRYTYPE"));
            rq.setTRADEDATE(requestParam.get("TRADEDATE"));
            rq.setAMT3(requestParam.get("AMT3"));
            rq.setFCA2(requestParam.get("FCA2"));
            rq.setAMT5(requestParam.get("AMT5"));
            rq.setPAYTYPE(requestParam.get("PAYTYPE"));
            rq.setFUNDACN(requestParam.get("FUNDACN"));
            rq.setOUTACN(requestParam.get("OUTACN"));
            rq.setINTSACN(requestParam.get("INTSACN"));
            rq.setBILLSENDMODE(requestParam.get("BILLSENDMODE"));
            rq.setFCAFEE(requestParam.get("FCAFEE"));
            rq.setSSLTXNO(requestParam.get("SSLTXNO"));
            rq.setPAYDAY1(requestParam.get("PAYDAY1"));
            rq.setPAYDAY2(requestParam.get("PAYDAY2"));
            rq.setPAYDAY3(requestParam.get("PAYDAY3"));
            rq.setBRHCOD(requestParam.get("BRHCOD"));
            rq.setCUTTYPE(requestParam.get("CUTTYPE"));
            rq.setCRY1(requestParam.get("CRY1"));
            rq.setHTELPHONE(requestParam.get("HTELPHONE"));
            rq.setDBDATE(requestParam.get("DBDATE"));
            rq.setSALESNO(requestParam.get("SALESNO"));
            rq.setYIELD(requestParam.get("YIELD"));
            rq.setSTOP(requestParam.get("STOP"));
            rq.setPAYDAY4(requestParam.get("PAYDAY4"));
            rq.setPAYDAY5(requestParam.get("PAYDAY5"));
            rq.setPAYDAY6(requestParam.get("PAYDAY6"));
            rq.setMIP(requestParam.get("MIP"));
            rq.setPRO(requestParam.get("PRO"));
            rq.setNUM(requestParam.get("NUM"));
            String KYCDATE = requestParam.get("KYCDATE");
            if (KYCDATE != null) {
                rq.setKYCDATE(KYCDATE);
            }
            String WEAK = requestParam.get("WEAK");
            if (WEAK != null) {
                rq.setWEAK(WEAK);
            }
            rq.setRSKATT(requestParam.get("RSKATT"));
            rq.setRRSK(requestParam.get("RRSK"));
            rq.setPAYDAY7(requestParam.get("PAYDAY7"));
            rq.setPAYDAY8(requestParam.get("PAYDAY8"));
            rq.setPAYDAY9(requestParam.get("PAYDAY9"));
            rq.setFDAGREEFLAG(requestParam.get("FDAGREEFLAG"));
            rq.setFDNOTICETYPE(requestParam.get("FDNOTICETYPE"));
            rq.setFDPUBLICTYPE(requestParam.get("FDPUBLICTYPE"));
            rq.setDPMYEMAIL(requestParam.get("DPMYEMAIL"));
            rq.setCMTRMAIL(requestParam.get("CMTRMAIL"));
            rq.setIP(requestParam.get("IP"));
            rq.setSLSNO(requestParam.get("SLSNO"));
            rq.setKYCNO(requestParam.get("KYCNO"));
            //for txnlog
            rq.setADSVBH("050");

            rs = restutil.send(rq, C020_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.debug(ESAPIUtil.vaildLog("rs={}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("C020_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 更改基金EMAIL
     */
    @SuppressWarnings("static-access")
    public BaseResult FundEmail_REST(Map<String, String> requestParam) {
    	log.debug(ESAPIUtil.vaildLog("requestParam={}"+CodeUtil.toJson(requestParam)));

        BaseResult bs = null;
        FundEmail_REST_RQ rq = null;
        FundEmail_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new FundEmail_REST_RQ();
            rq.setCUSIDN(requestParam.get("CUSIDN"));
            rq.setDPUSERID(requestParam.get("CUSIDN"));
            rq.setNEW_EMAIL(requestParam.get("NEW_EMAIL"));

            rs = restutil.send(rq, FundEmail_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.debug(ESAPIUtil.vaildLog("rs={}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("FundEmail_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 信用卡查詢
     */
    @SuppressWarnings("static-access")
    public BaseResult N812_REST(String UID) {
        log.debug("UID={}", UID);

        BaseResult bs = null;
        N812_REST_RQ rq = null;
        N812_REST_RS rs = null;

        try {
            bs = new BaseResult();
            rq = new N812_REST_RQ();
            rq.setCUSIDN(UID);

            rs = restutil.send(rq, N812_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.debug(ESAPIUtil.vaildLog("rs={}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N812_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 海外債券餘額及損益查詢(結果頁)
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */
    public BaseResult N570_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N570_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N570_REST_RQ rq = null;
        N570_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N570_REST_RQ();
            rq = CodeUtil.objectCovert(N570_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);
            rs = restutil.send(rq, N570_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N570_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N570_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 虛擬帳號入帳明細
     * 
     * @param cusidn
     * @return
     */

    public BaseResult N615_REST(String cusidn, String acn, Map<String, String> reqParam) {
        log.trace("N615_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N615_REST_RQ rq = null;
        N615_REST_RS rs = null;
        try {
            String stadate = reqParam.get("CMSDATE"); // 日期(起)
            String enddate = reqParam.get("CMEDATE"); // 日期(迄)
            bs = new BaseResult();
            // call REST API
            rq = new N615_REST_RQ();
            rq.setCUSIDN(cusidn);
            rq.setACN(acn);
            rq.setCMSDATE(stadate);
            rq.setCMEDATE(enddate);
            rs = restutil.send(rq, N615_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N615_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N615_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N072繳納信用卡
     * 
     * @param reqParam
     * @return
     */
    public BaseResult N072_REST(Map<String, String> reqParam) {

        log.trace("N072");
        log.trace(ESAPIUtil.vaildLog("cusidn>>{}" + reqParam.get("CUSIDN")));
        BaseResult bs = null;
        N072_REST_RQ rq = null;
        N072_REST_RS rs = null;
        try {

            bs = new BaseResult();
            // call REST API
            rq = new N072_REST_RQ();

            rq = CodeUtil.objectCovert(N072_REST_RQ.class, reqParam);

            rs = restutil.send(rq, N072_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("rs >>{}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("rs is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N073繳納期貨保證金(限本行)
     * 
     * @param sessionId
     * @return
     */
    public BaseResult N073_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N073_REST");
        BaseResult bs = null;
        N073_REST_RQ rq = null;
        N073_REST_RS rs = null;

        try {
            bs = new BaseResult();

            rq = new N073_REST_RQ();
            rq = CodeUtil.objectCovert(N073_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);
            rq.setACN(reqParam.get("OUTACN"));
            rq.setADCURRENCY("TWD");

            rs = restutil.send(rq, N073_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N073_REST_RS>>{}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N073_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }

        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N070A學雜費
     * 
     * @param reqParam
     * @return
     * @throws UnsupportedEncodingException
     */
    public BaseResult N070A_REST(Map<String, String> reqParam, String cusidn) {
        log.trace("N070A_REST");
        log.trace(ESAPIUtil.vaildLog("N070A_REST.reqParam: {}"+CodeUtil.toJson(reqParam)));
        BaseResult bs = null;
        N070A_REST_RQ rq = null;
        N070A_REST_RS rs = null;

        try {
            bs = new BaseResult();
            rq = new N070A_REST_RQ();
            rq = CodeUtil.objectCovert(N070A_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);
            rq.setACN(reqParam.get("OUTACN"));
            rq.setADCURRENCY("TWD");
            rq.setADSVBH("050");
            rq.setADAGREEF("0");

            rs = restutil.send(rq, N070A_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N070A_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N070A_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }

        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N070B其他費用
     * 
     * @param reqParam
     * @return
     * @throws UnsupportedEncodingException
     */
    public BaseResult N070B_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N070B_REST");
        log.trace(ESAPIUtil.vaildLog("N070B_REST.reqParam: {}"+CodeUtil.toJson(reqParam)));
        BaseResult bs = null;
        N070B_REST_RQ rq = null;
        N070B_REST_RS rs = null;

        try {
            bs = new BaseResult();
            rq = new N070B_REST_RQ();
            rq = CodeUtil.objectCovert(N070B_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);
            rq.setACN(reqParam.get("OUTACN"));
            rq.setADCURRENCY("TWD");

            rs = restutil.send(rq, N070B_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N070B_REST_RS>>{}"+ CodeUtil.toJson(rs)));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N070B_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }

        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N171繳稅
     * 
     * @param sessionId
     * @return
     */
    public BaseResult N171_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N171_REST");
        BaseResult bs = null;
        N171_REST_RQ rq = null;
        N171_REST_RS rs = null;

        try {
            bs = new BaseResult();

            rq = new N171_REST_RQ();
            rq = CodeUtil.objectCovert(N171_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);
            rq.setADSVBH("BANKIND");
            rq.setADAGREEF("0");

            rs = restutil.send(rq, N171_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N171_REST_RS>>{}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N171_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }

        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N075繳電費
     * 
     * @param sessionId
     * @return
     */
    public BaseResult N075_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N075_REST");
        BaseResult bs = null;
        N075_REST_RQ rq = null;
        N075_REST_RS rs = null;

        try {
            bs = new BaseResult();

            rq = new N075_REST_RQ();
            rq = CodeUtil.objectCovert(N075_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);
            rq.setADCURRENCY("TWD");
            rq.setADSVBH("050");
            rq.setADAGREEF("0");

            rs = restutil.send(rq, N075_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N075_REST_RS>>{}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N075_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }

        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N750A臺灣省水費
     * 
     * @param sessionId
     * @return
     */
    public BaseResult N750A_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N750A_REST");
        BaseResult bs = null;
        N750A_REST_RQ rq = null;
        N750A_REST_RS rs = null;

        try {
            bs = new BaseResult();

            rq = new N750A_REST_RQ();
            rq = CodeUtil.objectCovert(N750A_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);
            rq.setADCURRENCY("TWD");
            rq.setADSVBH("050");
            rq.setADAGREEF("0");

            rs = restutil.send(rq, N750A_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N750A_REST_RS>>{}"+rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N750A_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }

        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N750B臺灣市水費
     * 
     * @param sessionId
     * @return
     */
    public BaseResult N750B_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N750B_REST");
        BaseResult bs = null;
        N750B_REST_RQ rq = null;
        N750B_REST_RS rs = null;

        try {
            bs = new BaseResult();

            rq = new N750B_REST_RQ();
            rq = CodeUtil.objectCovert(N750B_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);
            rq.setADCURRENCY("TWD");
            rq.setADSVBH("050");
            rq.setADAGREEF("0");

            rs = restutil.send(rq, N750B_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N750B_REST_RS>>{}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N750B_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }

        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N750C勞保費
     * 
     * @param sessionId
     * @return
     */
    public BaseResult N750C_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N750C_REST");
        BaseResult bs = null;
        N750C_REST_RQ rq = null;
        N750C_REST_RS rs = null;

        try {
            bs = new BaseResult();

            rq = new N750C_REST_RQ();
            rq = CodeUtil.objectCovert(N750C_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);
            rq.setADCURRENCY("TWD");
            rq.setADSVBH("050");
            rq.setADAGREEF("0");
            
            rs = restutil.send(rq, N750C_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N750C_REST_RS>>{}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N750C_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }

        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N750E健保費/補充保險費
     * 
     * @param sessionId
     * @return
     */
    public BaseResult N750E_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N750E_REST");
        BaseResult bs = null;
        N750E_REST_RQ rq = null;
        N750E_REST_RS rs = null;

        try {
            bs = new BaseResult();

            rq = new N750E_REST_RQ();
            rq = CodeUtil.objectCovert(N750E_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);
            rq.setADCURRENCY("TWD");
            rq.setADSVBH("050");
            rq.setADAGREEF("0");
            
            rs = restutil.send(rq, N750E_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N750E_REST_RS>>{}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N750E_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }

        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N750F國民年金保險費
     * 
     * @param sessionId
     * @return
     */
    public BaseResult N750F_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N750F_REST");
        BaseResult bs = null;
        N750F_REST_RQ rq = null;
        N750F_REST_RS rs = null;

        try {
            bs = new BaseResult();

            rq = new N750F_REST_RQ();
            rq = CodeUtil.objectCovert(N750F_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);
            rq.setADCURRENCY("TWD");
            rq.setADSVBH("050");
            rq.setADAGREEF("0");
            
            rs = restutil.send(rq, N750F_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N750F_REST_RS>>{}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N750F_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }

        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N750H新制勞工退休金
     * 
     * @param sessionId
     * @return
     */
    public BaseResult N750H_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N750H_REST");
        BaseResult bs = null;
        N750H_REST_RQ rq = null;
        N750H_REST_RS rs = null;

        try {
            bs = new BaseResult();

            rq = new N750H_REST_RQ();
            rq = CodeUtil.objectCovert(N750H_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);
            rq.setADCURRENCY("TWD");
            rq.setADSVBH("050");
            rq.setADAGREEF("0");
            
            rs = restutil.send(rq, N750H_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N750H_REST_RS>>{}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N750H_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }

        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N750G欣欣瓦斯費
     * 
     * @param sessionId
     * @return
     */
    public BaseResult N750G_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N750G_REST");
        BaseResult bs = null;
        N750G_REST_RQ rq = null;
        N750G_REST_RS rs = null;

        try {
            bs = new BaseResult();

            rq = new N750G_REST_RQ();
            rq = CodeUtil.objectCovert(N750G_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);
            rq.setADCURRENCY("TWD");
            rq.setADSVBH("050");
            rq.setADAGREEF("0");
            
            rs = restutil.send(rq, N750G_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N750G_REST_RS>>{}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N750G_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }

        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 小額信貸申請資料查詢
     * 
     * @param cusidn
     * @return
     */
    public BaseResult NA011_REST(String cusidn) {
        log.trace("NA011_REST");
        log.trace("uid >> {}", cusidn);
        BaseResult bs = new BaseResult();
        NA011_REST_RQ rq = null;
        NA011_REST_RS rs = null;
        try {
            rq = new NA011_REST_RQ();
            rq.setUID(cusidn);
            rq.setCUSIDN(cusidn);
            String apiName = rq.getAPI_Name(rq.getClass());
            rs = restutil.send(rq, NA011_REST_RS.class, apiName);

            if (rs == null) {
                log.error("NA011_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            } else {
                log.trace(ESAPIUtil.vaildLog("NA011_REST_RS >>" + rs));
                CodeUtil.convert2BaseResult(bs, rs);
            }
        } catch (Exception e) {
            log.error("NA011_REST error", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    public BaseResult N108_REST(Map<String, String> reqParam) {
        log.trace("N108_REST");
        BaseResult bs = new BaseResult();
        N108_REST_RQ rq = null;
        N108_REST_RS rs = null;
        try {
            bs = new BaseResult();

            rq = new N108_REST_RQ();
            rq = CodeUtil.objectCovert(N108_REST_RQ.class, reqParam);
            rs = restutil.send(rq, N108_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N108_REST_RS>>{}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N108_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }

        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 新台幣存單掛失
     * 
     * @param cusidn
     * @param acn
     * @param reqParam
     * @return
     */

    public BaseResult N841_REST(String cusidn, String acn, Map<String, String> reqParam) {
        log.trace("N841_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N841_REST_RQ rq = null;
        N841_REST_RS rs = null;
        try {
            String txid = reqParam.get("TXID");
            log.trace(ESAPIUtil.vaildLog("TXID>>{}"+ txid));
            String adopid = reqParam.get("ADOPID");
            log.trace(ESAPIUtil.vaildLog("ADOPID>>{}"+ adopid));
            String trancode = reqParam.get("trancode");
            bs = new BaseResult();
            // call REST API
            rq = new N841_REST_RQ();
            rq.setCUSIDN(cusidn);
            rq.setACN(acn);
            rq.setTXID(txid);
            rq.setTrancode(trancode);
            log.trace("TESTTTTTT>>{}", rq.getAPI_Name(rq.getClass()));
            rs = restutil.send(rq, N841_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N841_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N841_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 新台幣存單掛失結果頁
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */

    public BaseResult N8503_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N8503_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N8503_REST_RQ rq = null;
        N8503_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N8503_REST_RQ();
            rq = CodeUtil.objectCovert(N8503_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);

            rs = restutil.send(rq, N8503_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N8503_REST_RS>>{}"+ rs.getREC()));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N8503_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 外幣存單掛失
     * 
     * @param cusidn
     * @param acn
     * @param reqParam
     * @return
     */

    public BaseResult N842_REST(String cusidn, String acn, Map<String, String> reqParam) {
        log.trace("N842_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N842_REST_RQ rq = null;
        N842_REST_RS rs = null;
        try {
            String txid = reqParam.get("TXID");
            log.trace(ESAPIUtil.vaildLog("TXID>>{}"+ txid));
            String adopid = reqParam.get("ADOPID");
            log.trace(ESAPIUtil.vaildLog("ADOPID>>{}"+ adopid));
            String trancode = reqParam.get("trancode");
            bs = new BaseResult();
            // call REST API
            rq = new N842_REST_RQ();
            rq.setCUSIDN(cusidn);
            rq.setACN(acn);
            rq.setTXID(txid);
            rq.setTrancode(trancode);
            log.trace("TESTTTTTT>>{}", rq.getAPI_Name(rq.getClass()));
            rs = restutil.send(rq, N842_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N842_REST_RS>>{}" + rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N842_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 外幣存單掛失結果頁
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */

    public BaseResult N8505_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N8505_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N8505_REST_RQ rq = null;
        N8505_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N8505_REST_RQ();
            rq = CodeUtil.objectCovert(N8505_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);

            rs = restutil.send(rq, N8505_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N8505_REST_RS>>{}"+ rs.getREC()));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N8505_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 網銀線上約定平台
     * 
     * F013_REST
     * 
     * @param cusidn
     * @return
     */
    public BaseResult N855_REST( Map<String, String> reqParam) {
        log.trace("N855_REST START!!!");
        BaseResult bs = null;
        N855_REST_RQ rq = null;
        N855_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N855_REST_RQ();
            rq = CodeUtil.objectCovert(N855_REST_RQ.class, reqParam);
            rs = restutil.send(rq, N855_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N855_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N855_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 網銀線上約定平台
     * 
     * F013_REST
     * 
     * @param cusidn
     * @return
     */
    public BaseResult N856_REST( Map<String, Object> reqParam) {
        log.trace("N856_REST START!!!");
        BaseResult bs = null;
        N856_REST_RQ rq = null;
        N856_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N856_REST_RQ();
            rq = CodeUtil.objectCovert(N856_REST_RQ.class, reqParam);
            rs = restutil.send(rq, N856_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N856_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N856_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 網銀線上約定平台
     * 
     * F013_REST
     * 
     * @param cusidn
     * @return
     */
    public BaseResult N855decrypt_REST( Map<String, String> reqParam) {
        log.trace("N855decrypt_REST START!!!");
        BaseResult bs = null;
        N855decrypt_REST_RQ rq = null;
        N855decrypt_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N855decrypt_REST_RQ();
            rq = CodeUtil.objectCovert(N855decrypt_REST_RQ.class, reqParam);
            rs = restutil.send(rq, N855decrypt_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N855decrypt_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N855decrypt_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }
    /**
     * 網銀線上約定平台
     * 
     * F013_REST
     * 
     * @param cusidn
     * @return
     */
    public BaseResult N855checkmac_REST( Map<String, Object> reqParam) {
        log.trace("N855checkmac_REST START!!!");
        BaseResult bs = null;
        N855checkmac_REST_RQ rq = null;
        N855checkmac_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N855checkmac_REST_RQ();
            rq = CodeUtil.objectCovert(N855checkmac_REST_RQ.class, reqParam);
            rs = restutil.send(rq, N855checkmac_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N855checkmac_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N855checkmac_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }
    /**
     * 網銀線上約定平台
     * 
     * N855CancelData_REST
     * 
     * @param cusidn
     * @return
     */
    public BaseResult N855canceldata_REST( Map<String, String> reqParam) {
        log.trace("N855canceldata_REST START!!!");
        BaseResult bs = null;
        N855canceldata_REST_RQ rq = null;
        N855canceldata_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N855canceldata_REST_RQ();
            rq = CodeUtil.objectCovert(N855canceldata_REST_RQ.class, reqParam);
            rs = restutil.send(rq, N855canceldata_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N855canceldata_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N855canceldata_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }    
    
    /**
     * 新台幣存摺掛失
     * 
     * @param cusidn
     * @param acn
     * @param reqParam
     * @return
     */

    public BaseResult N840_REST(String cusidn, String acn, Map<String, String> reqParam) {
        log.trace("N840_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N840_REST_RQ rq = null;
        N840_REST_RS rs = null;
        try {
            String txid = reqParam.get("TXID");
            log.trace(ESAPIUtil.vaildLog("TXID>>{}"+ txid));
            String adopid = reqParam.get("ADOPID");
            log.trace(ESAPIUtil.vaildLog("ADOPID>>{}"+ adopid));
            String trancode = reqParam.get("trancode");
            bs = new BaseResult();
            // call REST API
            rq = new N840_REST_RQ();
            rq.setCUSIDN(cusidn);
            rq.setACN(acn);
            rq.setTXID(txid);
            rq.setTrancode(trancode);
            log.trace("TESTTTTTT>>{}", rq.getAPI_Name(rq.getClass()));
            rs = restutil.send(rq, N840_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N840_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N840_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 新台幣存摺掛失結果頁
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */

    public BaseResult N8501_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N8501_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N8501_REST_RQ rq = null;
        N8501_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N8501_REST_RQ();
            rq = CodeUtil.objectCovert(N8501_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);

            rs = restutil.send(rq, N8501_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N8501_REST_RS>>{}"+rs.getREC()));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N8501_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 外幣存褶掛失
     * 
     * @param cusidn
     * @param acn
     * @param reqParam
     * @return
     */

    public BaseResult N843_REST(String cusidn, String acn, Map<String, String> reqParam) {
        log.trace("N843_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N843_REST_RQ rq = null;
        N843_REST_RS rs = null;
        try {
            String txid = reqParam.get("TXID");
            log.trace(ESAPIUtil.vaildLog("TXID>>{}"+ txid));
            String adopid = reqParam.get("ADOPID");
            log.trace(ESAPIUtil.vaildLog("ADOPID>>{}"+ adopid));
            String trancode = reqParam.get("trancode");
            bs = new BaseResult();
            // call REST API
            rq = new N843_REST_RQ();
            rq.setCUSIDN(cusidn);
            rq.setACN(acn);
            rq.setTXID(txid);
            rq.setTrancode(trancode);
            log.trace("TESTTTTTT>>{}", rq.getAPI_Name(rq.getClass()));
            rs = restutil.send(rq, N843_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N843_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N843_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 外幣存褶掛失結果頁
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */

    public BaseResult N8507_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N8507_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N8507_REST_RQ rq = null;
        N8507_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N8507_REST_RQ();
            rq = CodeUtil.objectCovert(N8507_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);

            rs = restutil.send(rq, N8507_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N8507_REST_RS>>{}"+rs.getREC()));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N8507_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 黃金存褶掛失
     * 
     * @param cusidn
     * @param acn
     * @param reqParam
     * @return
     */

    public BaseResult N845_REST(String cusidn, String acn, Map<String, String> reqParam) {
        log.trace("N845_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N845_REST_RQ rq = null;
        N845_REST_RS rs = null;
        try {
        	String txid = reqParam.get("TXID");
            log.trace(ESAPIUtil.vaildLog("TXID>>{}"+ txid));
            String adopid = reqParam.get("ADOPID");
            log.trace(ESAPIUtil.vaildLog("ADOPID>>{}"+ adopid));
            String trancode = reqParam.get("trancode");
            bs = new BaseResult();
            // call REST API
            rq = new N845_REST_RQ();
            rq.setCUSIDN(cusidn);
            rq.setACN(acn);
            rq.setTXID(txid);
            rq.setTrancode(trancode);
            log.trace("TESTTTTTT>>{}", rq.getAPI_Name(rq.getClass()));
            rs = restutil.send(rq, N845_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N845_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N845_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N880
     * 
     * @param cusidn
     * @param acn    帳號
     * @return
     */
    public BaseResult N880_REST(String cusidn, String twAcn, Map<String, String> reqParam) {

        log.trace("N880_REST>>");
        BaseResult bs = null;
        N880_REST_RQ rq = null;
        N880_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N880_REST_RQ();
            rq = CodeUtil.objectCovert(N880_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);
            rq.setACN(twAcn);
            rs = restutil.send(rq, N880_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N880_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N880_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 黃金存褶掛失
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */

    public BaseResult N850A_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N850A_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N850A_REST_RQ rq = null;
        N850A_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N850A_REST_RQ();
            rq = CodeUtil.objectCovert(N850A_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);

            rs = restutil.send(rq, N850A_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N850A_REST_RS>>{}"+ rs.getREC()));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N850A_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * "VISA金融卡卡號查詢"
     * 
     * @param cusidn
     * @return
     */
    public BaseResult N813_REST(Map<String, String> reqParam) {
        log.trace("N813_REST");
        log.trace(ESAPIUtil.vaildLog("cusidn>>{}"+ reqParam.get("CUSIDN")));
        BaseResult bs = null;
        N813_REST_RQ rq = null;
        N813_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N813_REST_RQ();
            rq.setCUSIDN(reqParam.get("CUSIDN"));
            rs = restutil.send(rq, N813_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N813_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N813_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * "數位存款帳戶補申請晶片金融卡"
     * 
     * @param cusidn
     * @return
     */
    public BaseResult NB31_REST(Map<String, String> reqParam) {
        log.trace("NB31_REST_RQ");
        log.trace(ESAPIUtil.vaildLog("cusidn>>{}"+ reqParam.get("CUSIDN")));
        BaseResult bs = null;
        NB31_REST_RQ rq = null;
        NB31_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new NB31_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rq.setUID(reqParam.get("CUSIDN"));
            rs = restutil.send(rq, NB31_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("NB31_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("NB31_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * "數位存款帳戶補申請晶片金融卡"
     * 
     * @param cusidn
     * @return
     */
    public BaseResult NB31_1_REST(Map<String, String> reqParam) {
        log.trace("NB31_1_REST");
        log.trace(ESAPIUtil.vaildLog("cusidn>>{}" + reqParam.get("CUSIDN")));
        BaseResult bs = null;
        NB31_1_REST_RQ rq = null;
        NB31_1_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new NB31_1_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rs = restutil.send(rq, NB31_1_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("NB31_1_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("NB31_1_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 數位存款帳戶晶片金融卡確認領用申請及變更密碼Step1
     * 
     * @param cusidn
     * @return
     */
    public BaseResult NB32_1_REST(Map<String, String> reqParam) {
        log.trace("NB32_1_REST_RQ");
        BaseResult bs = null;
        NB32_1_REST_RQ rq = null;
        NB32_1_REST_RS rs = null;
        try {
            log.trace(ESAPIUtil.vaildLog("NB32_1_REST_RS.reqParam: {}"+CodeUtil.toJson(reqParam)));
            bs = new BaseResult();
            rq = new NB32_1_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rq.setUID(reqParam.get("CUSIDN"));
            rs = restutil.send(rq, NB32_1_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("NB32_1_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N813_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * "數位存款帳戶晶片金融卡確認領用申請及變更密碼Step2"
     * 
     * @param cusidn
     * @return
     */
    public BaseResult NB32_2_REST(Map<String, String> reqParam) {
        log.trace("NB32_2_REST_RQ");
        log.trace(ESAPIUtil.vaildLog("cusidn>>{}"+reqParam.get("CUSIDN")));
        BaseResult bs = null;
        NB32_2_REST_RQ rq = null;
        NB32_2_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new NB32_2_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rs = restutil.send(rq, NB32_2_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("NB32_2_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("NB32_2_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * "TD02_REST繳款紀錄查詢"
     * 
     * @param cusidn
     * @return
     */
    public BaseResult TD02_REST(Map<String, String> reqParam) {
        log.trace("TD02_REST");
        log.trace(ESAPIUtil.vaildLog("cusidn>>{}" + reqParam.get("CUSIDN")));
        BaseResult bs = null;
        TD02_REST_RQ rq = null;
        TD02_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new TD02_REST_RQ();
            rq = CodeUtil.objectCovert(TD02_REST_RQ.class, reqParam);
            rs = restutil.send(rq, TD02_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("TD02_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("TD02_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * "CN19_REST申請隨護神盾"
     * 
     * @param reqParam
     * @return
     */
    public BaseResult CN19_REST(Map<String, String> reqParam) {
        log.trace("CN19_REST");
        log.trace(ESAPIUtil.vaildLog("cusidn>>{}"+ reqParam.get("CUSIDN")));
        BaseResult bs = null;
        CN19_REST_RQ rq = null;
        CN19_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new CN19_REST_RQ();
            log.trace("CN19_REST_RS>>{}", rs);
            rq = CodeUtil.objectCovert(CN19_REST_RQ.class, reqParam);
            rs = restutil.send(rq, CN19_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("CN19_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("CN19_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * "CN32_REST線上註銷無卡提款Step1"
     * 
     * @param reqParam
     * @return
     */
    public BaseResult CN32D_REST(Map<String, String> reqParam) {
        log.trace("CN32D_REST");
        log.trace(ESAPIUtil.vaildLog("cusidn>>{}"+ reqParam.get("CUSIDN")));
        BaseResult bs = null;
        CN32D_REST_RQ rq = null;
        CN32D_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new CN32D_REST_RQ();
            log.trace(ESAPIUtil.vaildLog("CN32D_REST_RQ>>{}"+ rq));
            rq = CodeUtil.objectCovert(CN32D_REST_RQ.class, reqParam);
            rs = restutil.send(rq, CN32D_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("CN32D_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("CN32D_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * "N580"
     * 
     * @param reqParam
     * @return
     */
    public BaseResult N580_REST(Map<String, String> reqParam) {
        log.trace("N580_REST");
        log.trace(ESAPIUtil.vaildLog("cusidn>>{}"+ reqParam.get("CUSIDN")));
        BaseResult bs = null;
        N580_REST_RQ rq = null;
        N580_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N580_REST_RQ();
            log.trace(ESAPIUtil.vaildLog("N580_REST_RQ>>{}"+rq));
            rq = CodeUtil.objectCovert(N580_REST_RQ.class, reqParam);
            rs = restutil.send(rq, N580_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N580_REST_RS>>{}"+rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N580_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }
    
    /**
     * "N580_1"
     * 
     * @param reqParam
     * @return
     */
    public BaseResult N580_1_REST(Map<String, String> reqParam) {
        log.trace("N580_1_REST");
        log.trace(ESAPIUtil.vaildLog("cusidn>>{}"+ reqParam.get("CUSIDN")));
        BaseResult bs = null;
        N580_1_REST_RQ rq = null;
        N580_1_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N580_1_REST_RQ();
            log.trace(ESAPIUtil.vaildLog("N580_1_REST_RQ>>{}"+ rq));
            rq = CodeUtil.objectCovert(N580_1_REST_RQ.class, reqParam);
            rs = restutil.send(rq, N580_1_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N580_1_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N580_1_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }
    
    /**
     * "CN32_1_REST線上註銷無卡提款Step2"
     * 
     * @param reqParam
     * @return
     */
    public BaseResult CN32D_1_REST(Map<String, String> reqParam) {
        log.trace("CN32D_1_REST");
        log.trace(ESAPIUtil.vaildLog("cusidn>>{}"+ reqParam.get("CUSIDN")));
        BaseResult bs = null;
        CN32D_1_REST_RQ rq = null;
        CN32D_1_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new CN32D_1_REST_RQ();
            rq.setCMPW(reqParam.get("CMPASSWORD"));
            log.trace(ESAPIUtil.vaildLog("CN32D_1_REST_RQ>>{}"+ rq));
            rq = CodeUtil.objectCovert(CN32D_1_REST_RQ.class, reqParam);
            rs = restutil.send(rq, CN32D_1_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("CN32D_1_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("CN32D_1_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 中央登錄債券明細
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */
    public BaseResult N871_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N871_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N871_REST_RQ rq = null;
        N871_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N871_REST_RQ();
            rq.setCUSIDN(cusidn);
            rq.setACN(reqParam.get("ACN").equals("") ? "" : reqParam.get("ACN"));
            rq.setCMEDATE(reqParam.get("CMEDATE"));
            rq.setCMSDATE(reqParam.get("CMSDATE"));
            rq.setUSERDATA_X50(reqParam.get("USERDATA_X50"));
            rs = restutil.send(rq, N871_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N871_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N871_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 金融卡掛失
     * 
     * @param cusidn
     * @param acn
     * @param reqParam
     * @return
     */

    public BaseResult N844_REST(String cusidn, String acn, Map<String, String> reqParam) {
        log.trace("N844_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N844_REST_RQ rq = null;
        N844_REST_RS rs = null;
        try {
        	String txid = reqParam.get("TXID");
            log.trace(ESAPIUtil.vaildLog("TXID>>{}"+ txid));
            String adopid = reqParam.get("ADOPID");
            log.trace(ESAPIUtil.vaildLog("ADOPID>>{}"+ adopid));
            String trancode = reqParam.get("trancode");
            bs = new BaseResult();
            // call REST API
            rq = new N844_REST_RQ();
            rq.setCUSIDN(cusidn);
            rq.setACN(acn);
            rq.setTXID(txid);
            rq.setADOPID("N844_1");
            rq.setTrancode(trancode);
            log.trace("TESTTTTTT>>{}", rq.getAPI_Name(rq.getClass()));
            rs = restutil.send(rq, N844_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N845_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N844_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 金融卡掛失結果頁
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */

    public BaseResult N8509_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N8509_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N8509_REST_RQ rq = null;
        N8509_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N8509_REST_RQ();
            rq = CodeUtil.objectCovert(N8509_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);

            rs = restutil.send(rq, N8509_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N8509_REST_RS>>{}"+ rs.getREC()));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N8509_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N559_REST 進口託收查詢
     * 
     * @param reqParam
     * @return
     */
    public BaseResult N559_REST(Map<String, String> reqParam) {
        log.trace("N559_REST");
        log.trace(ESAPIUtil.vaildLog("cusidn>>{}" +reqParam.get("CUSIDN")));
        BaseResult bs = null;
        N559_REST_RQ rq = null;
        N559_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N559_REST_RQ();
            log.trace(ESAPIUtil.vaildLog("N559_REST_RS>>{}"+ rs));
            rq = CodeUtil.objectCovert(N559_REST_RQ.class, reqParam);
            rs = restutil.send(rq, N559_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N559_REST_RS>>{}"+rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N559_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N560_REST 信用狀項下出口託收查詢
     * 
     * @param reqParam
     * @return
     */
    public BaseResult N560_REST(Map<String, String> reqParam) {
        log.trace("N560_REST");
        log.trace(ESAPIUtil.vaildLog("cusidn>>{}" +reqParam.get("CUSIDN")));
        BaseResult bs = null;
        N560_REST_RQ rq = null;
        N560_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N560_REST_RQ();
            log.trace(ESAPIUtil.vaildLog("N560_REST_RS>>{}"+ rs));
            rq = CodeUtil.objectCovert(N560_REST_RQ.class, reqParam);
            rs = restutil.send(rq, N560_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N560_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N560_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N563_REST D/A、D/P出口託收查詢
     * 
     * @param reqParam
     * @return
     */
    public BaseResult N563_REST(Map<String, String> reqParam) {
        log.trace("N563_REST");
        log.trace(ESAPIUtil.vaildLog("cusidn>>{}" + reqParam.get("CUSIDN")));
        BaseResult bs = null;
        N563_REST_RQ rq = null;
        N563_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N563_REST_RQ();
            log.trace(ESAPIUtil.vaildLog("CN19_REST_RS>>{}"+ rs));
            rq = CodeUtil.objectCovert(N563_REST_RQ.class, reqParam);
            rs = restutil.send(rq, N563_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N563_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N563_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 新台幣存單印鑑掛失結果頁
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */

    public BaseResult N8504_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N8504_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N8504_REST_RQ rq = null;
        N8504_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N8504_REST_RQ();
            rq = CodeUtil.objectCovert(N8504_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);

            rs = restutil.send(rq, N8504_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N8504_REST_RS>>{}"+ rs.getREC()));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N8504_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 外幣存單印鑑掛失結果頁
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */

    public BaseResult N8506_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N8506_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N8506_REST_RQ rq = null;
        N8506_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N8506_REST_RQ();
            rq = CodeUtil.objectCovert(N8506_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);

            rs = restutil.send(rq, N8506_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N8506_REST_RS>>{}"+ rs.getREC()));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N8506_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 新台幣存摺印鑑掛失結果頁
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */

    public BaseResult N8502_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N8502_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N8502_REST_RQ rq = null;
        N8502_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N8502_REST_RQ();
            rq = CodeUtil.objectCovert(N8502_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);

            rs = restutil.send(rq, N8502_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N8502_REST_RS>>{}"+rs.getREC()));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N8502_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 外幣存褶印鑑掛失結果頁
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */

    public BaseResult N8508_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N8508_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N8508_REST_RQ rq = null;
        N8508_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N8508_REST_RQ();
            rq = CodeUtil.objectCovert(N8508_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);

            rs = restutil.send(rq, N8508_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N8508_REST_RS>>{}"+ rs.getREC()));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N8508_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N115_REST
     * 
     * @param reqParam
     * @return
     */
    public BaseResult C115_REST(Map<String, String> reqParam) {
        log.trace("C115_REST");
        log.trace(ESAPIUtil.vaildLog("cusidn>>"+ reqParam.get("CUSIDN")));
        BaseResult bs = null;
        C115_REST_RQ rq = null;
        C115_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new C115_REST_RQ();
            rq = CodeUtil.objectCovert(C115_REST_RQ.class, reqParam);
            rs = restutil.send(rq, C115_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("C115_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("C115_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    public BaseResult N913_REST(String cusidn, Map<String, String> reqParam) {
        log.trace(ESAPIUtil.vaildLog("N913_REST reqParam >> {}"+CodeUtil.toJson(reqParam)));
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N913_REST_RQ rq = null;
        N913_REST_RS rs = null;
        try {
            String txid = "N913_CHK";
            String pinnew = reqParam.get("PINNEW");
            String oristatus = reqParam.get("OriStatus");
            String mb_switch = reqParam.get("MB_Switch");
            bs = new BaseResult();
            // call REST API
            rq = new N913_REST_RQ();

            rq.setCUSIDN(cusidn);
            rq.setMB_Switch(mb_switch);
            rq.setOriStatus(oristatus);
            rq.setPINNEW(pinnew);
            rq.setTXID(txid);
            rq.setUID(cusidn);

            rs = restutil.send(rq, N913_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N913_REST_RS>>{}" + rs.getMBOPNTM() + " MBOPNDT= " + rs.getMBOPNDT() + " LOTCNT= " + rs.getLOTCNT()));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N913_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 執行NA01
     */
    @SuppressWarnings("static-access")
    public BaseResult NA01_REST(Map<String, String> requestParam) {
    	log.debug(ESAPIUtil.vaildLog("requestParam={}"+CodeUtil.toJson(requestParam)));

        BaseResult bs = null;
        Na01_REST_RQ rq = null;
        Na01_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new Na01_REST_RQ();
            rq = CodeUtil.objectCovert(Na01_REST_RQ.class, requestParam);

            rs = restutil.send(rq, Na01_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.debug(ESAPIUtil.vaildLog("rs={}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("Na01_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 信用卡資料查詢
     */
    @SuppressWarnings("static-access")
    public BaseResult NA03Q_REST(Map<String, String> requestParam) {
        log.debug(ESAPIUtil.vaildLog("requestParam >> " + CodeUtil.toJson(requestParam)));
        BaseResult bs = null;
        Na03q_REST_RQ rq = null;
        Na03q_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new Na03q_REST_RQ();
            rq = CodeUtil.objectCovert(Na03q_REST_RQ.class, requestParam);

            rs = restutil.send(rq, Na03q_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.debug(ESAPIUtil.vaildLog("rs={}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("NA03Q_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 信用卡申請
     */
    @SuppressWarnings("static-access")
    public BaseResult NA03_REST(Map<String, String> requestParam) {
    	log.debug(ESAPIUtil.vaildLog("requestParam={}"+CodeUtil.toJson(requestParam)));

        BaseResult bs = null;
        Na03_REST_RQ rq = null;
        Na03_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new Na03_REST_RQ();
            rq = CodeUtil.objectCovert(Na03_REST_RQ.class, requestParam);

            rs = restutil.send(rq, Na03_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.debug(ESAPIUtil.vaildLog("rs={}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("NA03_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 信用卡資料查詢不用驗證版
     */
    @SuppressWarnings("static-access")
    public BaseResult NA02Q_REST(Map<String, String> requestParam) {
        log.debug(ESAPIUtil.vaildLog("requestParam >> " + CodeUtil.toJson(requestParam)));
        BaseResult bs = null;
        Na02q_REST_RQ rq = null;
        Na02q_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new Na02q_REST_RQ();
            rq = CodeUtil.objectCovert(Na02q_REST_RQ.class, requestParam);

            rs = restutil.send(rq, Na02q_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.debug(ESAPIUtil.vaildLog("rs={}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("NA02Q_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 信用卡款自動扣繳申請/取消
     * 
     * @param reqParam
     * @return
     */
    public BaseResult NI04_REST(Map<String, String> reqParam) {
        log.trace("NI04_REST");
        log.trace(ESAPIUtil.vaildLog("cusidn>>{}" + reqParam.get("CUSIDN")));
        log.trace(ESAPIUtil.vaildLog("NI04 reqParam >>{}"+ CodeUtil.toJson(reqParam)));
        BaseResult bs = null;
        NI04_REST_RQ rq = null;
        NI04_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new NI04_REST_RQ();
            rq = CodeUtil.objectCovert(NI04_REST_RQ.class, reqParam);
            rq.setADTXACNO(reqParam.get("TSFACN"));
            rs = restutil.send(rq, NI04_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("NI04_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("NI04_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * NI02預借現金密碼函補寄
     * 
     * @param reqParam
     * @return
     */
    public BaseResult NI02_REST(Map<String, String> reqParam) {
        log.trace("NI02_REST");
        log.trace(ESAPIUtil.vaildLog("cusidn >> " + reqParam.get("CUSIDN")));
        log.trace(ESAPIUtil.vaildLog("NI02 reqParam >> " + CodeUtil.toJson(reqParam)));
        BaseResult bs = null;
        NI02_REST_RQ rq = null;
        NI02_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new NI02_REST_RQ();
            rq = CodeUtil.objectCovert(NI02_REST_RQ.class, reqParam);
            rs = restutil.send(rq, NI02_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("NI02_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("NI02_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 晶片金融卡+讀卡機申請網路銀行
     */
    public BaseResult NA30_REST(Map<String, String> reqParam) {
        log.trace("NA30_REST");
        log.trace(ESAPIUtil.vaildLog("REST_reqParam >>{}"+ reqParam));
        BaseResult bs = null;
        Na30_REST_RQ rq = null;
        Na30_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new Na30_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rs = restutil.send(rq, Na30_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("NA30_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("NA30_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 晶片金融卡+讀卡機申請網路銀行
     */
    public BaseResult NA30_1_REST(Map<String, String> reqParam) {
        log.trace("Na30_1_REST");
        log.trace(ESAPIUtil.vaildLog("REST_reqParam >>{}"+ reqParam));
        BaseResult bs = null;
        Na30_1_REST_RQ rq = null;
        Na30_1_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new Na30_1_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rs = restutil.send(rq, Na30_1_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("Na30_1_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("Na30_1_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 線上申請黃金存摺帳戶
     */
    public BaseResult NA50_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("NA50_REST_RQ");
        log.trace(ESAPIUtil.vaildLog("cusidn>>{}"+ reqParam.get("CUSIDN")));
        BaseResult bs = null;
        NA50_REST_RQ rq = null;
        NA50_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new NA50_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rq.setUID(cusidn);
            rq.setADOPID("NA50");
            rs = restutil.send(rq, NA50_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("NA50_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("NA50_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 線上申請黃金存摺網路交易
     */
    public BaseResult NA60_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("NA60_REST_RQ");
        log.trace(ESAPIUtil.vaildLog("cusidn>>{}"+ reqParam.get("CUSIDN")));
        BaseResult bs = null;
        NA60_REST_RQ rq = null;
        NA60_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new NA60_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rq.setUID(cusidn);
            rq.setADOPID("NA60");
            rs = restutil.send(rq, NA60_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("NA60_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("NA60_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 金融卡線上申請/取消轉帳功能
     * 
     * @param rq
     * @return
     */
    public BaseResult NA80_REST(NA80_REST_RQ rq) {
        log.debug(ESAPIUtil.vaildLog("NA80_REST_RQ params >> {}"+ CodeUtil.toJson(rq)));

        BaseResult bs = new BaseResult();
        NA80_REST_RS rs = null;
        try {

            rs = restutil.send(rq, NA80_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.debug(ESAPIUtil.vaildLog("NA80_REST_RS >> {}"+ CodeUtil.toJson(rs)));

            if (rs == null) {
                log.error("NA80_REST_RS is null");
                throw new NullPointerException();
            }

            CodeUtil.convert2BaseResult(bs, rs);
            bs.addData("RS", rs);
        } catch (Exception e) {
            log.error("NA80_REST Error", e);
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 
     * @param reqParam
     * @return
     */
    public BaseResult C015_REST(Map<String, String> reqParam) {
        log.trace("C015_REST");
        log.trace(ESAPIUtil.vaildLog("cusidn>>{}" + reqParam.get("CUSIDN")));
        log.trace(ESAPIUtil.vaildLog("C015 reqParam >>{}" + CodeUtil.toJson(reqParam)));
        BaseResult bs = null;
        C015_REST_RQ rq = null;
        C015_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new C015_REST_RQ();
            rq = CodeUtil.objectCovert(C015_REST_RQ.class, reqParam);
            rs = restutil.send(rq, C015_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("C015_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("C015_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 
     * @param reqParam
     * @return
     */
    public BaseResult BHWS_REST(Map<String, String> reqParam) {
        log.trace("BHWS_REST");
        log.trace(ESAPIUtil.vaildLog("cusidn>>{}" + reqParam.get("CUSIDN")));
        log.trace(ESAPIUtil.vaildLog("BHWS reqParam >>{}" +  CodeUtil.toJson(reqParam)));
        BaseResult bs = null;
        BHWS_REST_RQ rq = null;
        BHWS_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new BHWS_REST_RQ();
            rq = CodeUtil.objectCovert(BHWS_REST_RQ.class, reqParam);
            rs = restutil.send(rq, BHWS_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("Bhwshtml_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("Bhwshtml_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }
    
    /**
     * 網路銀行簽入密碼／使用者代號線上解鎖
     */
    public BaseResult NA70_REST(String cusidn, Map<String, String> reqParam) {
    	log.trace(ESAPIUtil.vaildLog("NA71_REST reqParam >> {}"+CodeUtil.toJson(reqParam)));
        log.trace(ESAPIUtil.vaildLog("cusidn>>{}"+ cusidn));
        BaseResult bs = null;
        NA70_REST_RQ rq = null;
        NA70_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new NA70_REST_RQ();
            rq = CodeUtil.objectCovert(NA70_REST_RQ.class, reqParam);
            rs = restutil.send(rq, NA70_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("NA70_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    public BaseResult NA71_REST(String cusidn, Map<String, String> reqParam) {
    	log.trace(ESAPIUtil.vaildLog("NA71_REST reqParam >> {}"+CodeUtil.toJson(reqParam)));
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        NA71_REST_RQ rq = null;
        NA71_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new NA71_REST_RQ();
            rq = CodeUtil.objectCovert(NA71_REST_RQ.class, reqParam);
            rs = restutil.send(rq, NA71_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("NA71_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 其他費用代扣繳取消
     */
    public BaseResult N8330_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N8330_REST_RQ");
        BaseResult bs = null;
        N8330_REST_RQ rq = null;
        N8330_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new N8330_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rq.setCUSIDN(cusidn);
            rs = restutil.send(rq, N8330_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N8330_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N8330_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 其他費用代扣繳申請
     */
    public BaseResult N830_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N830_REST_RQ");
        log.trace(ESAPIUtil.vaildLog("cusidn>>{}"+ reqParam.get("CUSIDN")));
        BaseResult bs = null;
        N830_REST_RQ rq = null;
        N830_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new N830_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rq.setCUSIDN(cusidn);
            log.trace(ESAPIUtil.vaildLog("R1 >> {}"+reqParam.get("R1")));
        	rq.setADTXACNO(rq.getTSFACN());
            log.trace("ADTXACNO >> {}",rq.getADTXACNO());
            rs = restutil.send(rq, N830_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N830_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N830_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 其他費用代扣繳申請
     */
    public BaseResult N8301_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N8301_REST_RQ");
        log.trace(ESAPIUtil.vaildLog("cusidn>>{}"+ reqParam.get("CUSIDN")));
        BaseResult bs = null;
        N8301_REST_RQ rq = null;
        N8301_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new N8301_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rq.setCUSIDN(cusidn);
        	rq.setADTXACNO(rq.getTSFACN());
            log.trace("ADTXACNO >> {}",rq.getADTXACNO());
            rs = restutil.send(rq, N8301_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N8301_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N8301_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 其他費用代扣繳申請
     */
    public BaseResult N8302_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N8302_REST_RQ");
        log.trace(ESAPIUtil.vaildLog("cusidn>>{}"+ reqParam.get("CUSIDN")));
        BaseResult bs = null;
        N8302_REST_RQ rq = null;
        N8302_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new N8302_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rq.setCUSIDN(cusidn);
        	rq.setADTXACNO(rq.getTSFACN());
            log.trace("ADTXACNO >> {}",rq.getADTXACNO());
            rs = restutil.send(rq, N8302_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N8302_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N8302_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 其他費用代扣繳申請
     */
    public BaseResult N831_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N831_REST_RQ");
        log.trace(ESAPIUtil.vaildLog("cusidn>>{}"+ reqParam.get("CUSIDN")));
        BaseResult bs = null;
        N831_REST_RQ rq = null;
        N831_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new N831_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rq.setCUSIDN(cusidn);
        	rq.setADTXACNO(rq.getCARDNUM());
            log.trace("ADTXACNO >> {}",rq.getADTXACNO());
            rs = restutil.send(rq, N831_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N830_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N830_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 
     * @param reqParam
     * @return
     */
    public BaseResult N931_REST(Map<String, String> reqParam) {
        log.trace("N931_REST");
        log.trace(ESAPIUtil.vaildLog("cusidn>>{}" + reqParam.get("CUSIDN")));
        log.trace(ESAPIUtil.vaildLog("N931 reqParam >>{}" +  CodeUtil.toJson(reqParam)));
        BaseResult bs = null;
        N931_REST_RQ rq = null;
        N931_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N931_REST_RQ();
            rq = CodeUtil.objectCovert(N931_REST_RQ.class, reqParam);
            rs = restutil.send(rq, N931_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N931_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N931_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 
     * @param reqParam
     * @return
     */
    public BaseResult N1010RP_REST(Map<String, String> reqParam) {
        log.trace("N1010RP_REST");
        log.trace(ESAPIUtil.vaildLog("cusidn>>{}"+ reqParam.get("CUSIDN")));
        log.trace(ESAPIUtil.vaildLog("N1010RP_REST reqParam >>{}" +  CodeUtil.toJson(reqParam)));
        BaseResult bs = null;
        N1010_RP_REST_RQ rq = null;
        N1010_RP_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N1010_RP_REST_RQ();
            rq = CodeUtil.objectCovert(N1010_RP_REST_RQ.class, reqParam);
            rs = restutil.send(rq, N1010_RP_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N1010RP_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N1010RP_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 
     * @param reqParam
     * @return
     */
    public BaseResult N1010CP_REST(Map<String, String> reqParam) {
        log.trace("N1010CP_REST");
        log.trace(ESAPIUtil.vaildLog("cusidn>>{}" + reqParam.get("CUSIDN")));
        BaseResult bs = null;
        N1010_CP_REST_RQ rq = null;
        N1010_CP_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N1010_CP_REST_RQ();
            rq = CodeUtil.objectCovert(N1010_CP_REST_RQ.class, reqParam);
        	//修正Heap Inspection
            rq.setOldpw(reqParam.get("Oldpwd"));
            rq.setNewpw(reqParam.get("Newpwd"));
            rs = restutil.send(rq, N1010_CP_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N1010RP_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N1010CP_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 停車費
     */
    public BaseResult P018_REST() {
        log.trace("P018_REST_RQ");
        BaseResult bs = null;
        P018_REST_RQ rq = null;
        P018_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new P018_REST_RQ();
            rs = restutil.send(rq, P018_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("P018_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("P018_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 停車費
     */
    public BaseResult P001_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("P001_REST_RQ");
        BaseResult bs = null;
        P001_REST_RQ rq = null;
        P001_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new P001_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rq.setCustomerId(cusidn);
            rq.setCUSIDN(cusidn);
            rq.setADOPID("N614_1");
            rs = restutil.send(rq, P001_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("P001_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("P001_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 停車費
     */
    public BaseResult P002_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("P002_REST_RQ");
        BaseResult bs = null;
        P002_REST_RQ rq = null;
        P002_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new P002_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rq.setCustomerId(cusidn);
            rq.setCUSIDN(cusidn);
            rq.setADOPID("N614_2");
            rs = restutil.send(rq, P002_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("P002_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("P002_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 停車費
     */
    public BaseResult P003_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("P003_REST_RQ");
        BaseResult bs = null;
        P003_REST_RQ rq = null;
        P003_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new P003_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rq.setCustomerId(cusidn);
            rq.setADOPID("N614");
            rs = restutil.send(rq, P003_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("P003_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("P003_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 停車費
     */
    public BaseResult P004_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("P004_REST_RQ");
        BaseResult bs = null;
        P004_REST_RQ rq = null;
        P004_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new P004_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rq.setCustomerId(cusidn);
            rq.setADOPID("N614_3");
            rs = restutil.send(rq, P004_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("P004_REST_RS>>{}"+rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("P004_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 停車費
     */
    public BaseResult P005_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("P005_REST_RQ");
        BaseResult bs = null;
        P005_REST_RQ rq = null;
        P005_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new P005_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rq.setCustomerId(cusidn);
            rq.setCUSIDN(cusidn);
            rq.setADOPID("N614_2");
            rs = restutil.send(rq, P005_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("P005_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("P005_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 
     * @param reqParam
     * @return
     */
    public BaseResult N821_REST(Map<String, String> reqParam) {
        log.trace("N821_REST");
        log.trace(ESAPIUtil.vaildLog("cusidn >> " + reqParam.get("CUSIDN")));
        BaseResult bs = null;
        N821_REST_RQ rq = null;
        N821_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N821_REST_RQ();
            rq = CodeUtil.objectCovert(N821_REST_RQ.class, reqParam);
            rs = restutil.send(rq, N821_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N821_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N821_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 預約交易查詢/取消(申購資料頁)
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */

    public BaseResult N382_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N382_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N382_REST_RQ rq = null;
        N382_REST_RS rs = null;
        try {
            String adopid = reqParam.get("ADOPID");
            log.trace(ESAPIUtil.vaildLog("ADOPID>>{}"+ adopid));

            bs = new BaseResult();
            // call REST API
            rq = new N382_REST_RQ();
            rq = CodeUtil.objectCovert(N382_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);
            rs = restutil.send(rq, N382_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N382_REST_RS>>{}"+ rs.getREC()));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N382_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 預約交易查詢/取消(申購結果頁)
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */

    public BaseResult N392_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N392_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N392_REST_RQ rq = null;
        N392_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N392_REST_RQ();
            rq = CodeUtil.objectCovert(N392_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);
            rs = restutil.send(rq, N392_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N392_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N392_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 預約交易查詢/取消(轉換資料頁)
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */

    public BaseResult N383_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N383_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N383_REST_RQ rq = null;
        N383_REST_RS rs = null;
        try {
            String adopid = reqParam.get("ADOPID");
            log.trace(ESAPIUtil.vaildLog("ADOPID>>{}"+ adopid));

            bs = new BaseResult();
            // call REST API
            rq = new N383_REST_RQ();
            rq = CodeUtil.objectCovert(N383_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);
            rs = restutil.send(rq, N383_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N383_REST_RS>>{}"+ rs.getREC()));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N383_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 預約交易查詢/取消(轉換結果頁)
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */

    public BaseResult N393_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N393_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N393_REST_RQ rq = null;
        N393_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N393_REST_RQ();
            rq = CodeUtil.objectCovert(N393_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);
            rs = restutil.send(rq, N393_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N393_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N393_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 預約交易查詢/取消(贖回資料頁)
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */

    public BaseResult N384_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N384_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N384_REST_RQ rq = null;
        N384_REST_RS rs = null;
        try {
            String adopid = reqParam.get("ADOPID");
            log.trace(ESAPIUtil.vaildLog("ADOPID>>{}"+ adopid));

            bs = new BaseResult();
            // call REST API
            rq = new N384_REST_RQ();
            rq = CodeUtil.objectCovert(N384_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);
            rs = restutil.send(rq, N384_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N384_REST_RS>>{}"+rs.getREC()));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N384_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 預約交易查詢/取消(贖回結果頁)
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */

    public BaseResult N394_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N394_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N394_REST_RQ rq = null;
        N394_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N394_REST_RQ();
            rq = CodeUtil.objectCovert(N394_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);
            rs = restutil.send(rq, N394_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N394_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N394_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 預約交易查詢/取消(贖回資料頁)
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */

    public BaseResult N814_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N814_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N814_REST_RQ rq = null;
        N814_REST_RS rs = null;
        try {

            bs = new BaseResult();
            rq = new N814_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            if(reqParam.get("FLAG").equals("N")) {
            	rq.setADOPID("N8141");
            }else {
            	rq.setADOPID("N814");
            }
            rq.setCUSIDN(cusidn);
            rs = restutil.send(rq, N814_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N814_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }
    
    /**
     * 信用卡其他費用查詢
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */

    public BaseResult N815_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N815_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N815_REST_RQ rq = null;
        N815_REST_RS rs = null;
        try {

            bs = new BaseResult();
            rq = new N815_REST_RQ();
            rq.setCUSIDN(cusidn);
            rs = restutil.send(rq, N815_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N815_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }
    
    /**
     * 信用卡開卡結果
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */
    public BaseResult N816A_REST(Map<String, String> reqParam) {
        log.trace("N816A_REST");
        log.trace(ESAPIUtil.vaildLog("reqParam >> " + CodeUtil.toJson(reqParam)));
        BaseResult bs = null;
        N816A_REST_RQ rq = null;
        N816A_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N816A_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rs = restutil.send(rq, N816A_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N816A_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 信用卡掛失輸入
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */
    public BaseResult N816L_REST(String cusidn) {
        log.trace("N816L_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N816L_REST_RQ rq = null;
        N816L_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N816L_REST_RQ();
            rq.setCUSIDN(cusidn);
            rs = restutil.send(rq, N816L_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N816L_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 信用卡掛失結果
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */
    public BaseResult N816L_1_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N816L_1_REST");
        log.trace("cusidn>>{}", cusidn);
        log.trace(ESAPIUtil.vaildLog("ArrayParam >> " + reqParam.get("ArrayParam")));
        BaseResult bs = null;
        N816L_1_REST_RQ rq = null;
        N816L_1_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N816L_1_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rq.setCUSIDN(cusidn);
            rs = restutil.send(rq, N816L_1_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N816L_1_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 黃金即時回售
     */
    public BaseResult N090_02_REST(Map<String, String> reqParam) {
    	log.debug(ESAPIUtil.vaildLog("reqParam >> " + CodeUtil.toJson(reqParam)));

        BaseResult bs = null;
        N090_02_REST_RQ rq = null;
        N090_02_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new N090_02_REST_RQ();
            rq = CodeUtil.objectCovert(N090_02_REST_RQ.class, reqParam);

            rs = restutil.send(rq, N090_02_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.debug(ESAPIUtil.vaildLog("rs={}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N090_02_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 黃金預約回售
     */
    public BaseResult N091_02_REST(Map<String, String> reqParam) {
    	log.debug(ESAPIUtil.vaildLog("reqParam >> " + CodeUtil.toJson(reqParam)));

        BaseResult bs = null;
        N091_02_REST_RQ rq = null;
        N091_02_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new N091_02_REST_RQ();
            rq = CodeUtil.objectCovert(N091_02_REST_RQ.class, reqParam);

            rs = restutil.send(rq, N091_02_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.debug(ESAPIUtil.vaildLog("rs={}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N091_02_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 黃金即時買進
     */
    public BaseResult N090_01_REST(Map<String, String> reqParam) {
    	log.debug(ESAPIUtil.vaildLog("reqParam >> " + CodeUtil.toJson(reqParam)));

        BaseResult bs = null;
        N090_01_REST_RQ rq = null;
        N090_01_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new N090_01_REST_RQ();
            rq = CodeUtil.objectCovert(N090_01_REST_RQ.class, reqParam);

            rs = restutil.send(rq, N090_01_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.debug(ESAPIUtil.vaildLog("rs={}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N090_01_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 黃金買進(預約)
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */

    public BaseResult N091_01_REST(Map<String, String> reqParam) {
        log.trace("N091_01_REST");
        log.trace(ESAPIUtil.vaildLog("cusidn>>{}"+ reqParam.get("CUSIDN")));
        BaseResult bs = null;
        N091_01_REST_RQ rq = null;
        N091_01_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N091_01_REST_RQ();
            rq = CodeUtil.objectCovert(N091_01_REST_RQ.class, reqParam);
            rs = restutil.send(rq, N091_01_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N091_01_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N091_01_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N201 線上預約開立存款戶
     */
    public BaseResult N201_REST(Map<String, String> reqParam) {
        log.trace("N201_REST_RQ");
        BaseResult bs = null;
        N201_REST_RQ rq = null;
        N201_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new N201_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rs = restutil.send(rq, N201_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N201_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N201_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 黃金定期定額申購
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */

    public BaseResult N093_01_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N093_01_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N093_01_REST_RQ rq = null;
        N093_01_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N093_01_REST_RQ();
            rq = CodeUtil.objectCovert(N093_01_REST_RQ.class, reqParam);
            rq.setUID(cusidn);
            rs = restutil.send(rq, N093_01_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N093_01_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N093_01_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 黃金定期定額變更
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */

    public BaseResult N093_02_REST(String cusidn, Map<String, String> reqParam, String adopid) {
        log.trace(ESAPIUtil.vaildLog("N093_02_REST>>>{}"+ CodeUtil.toJson(reqParam)));
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N093_02_REST_RQ rq = null;
        N093_02_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N093_02_REST_RQ();
            rq = CodeUtil.objectCovert(N093_02_REST_RQ.class, reqParam);
            rq.setUID(cusidn);
            rq.setADSVBH("050");
            rq.setADAGREEF("0");
            rq.setADOPID(adopid);
            rs = restutil.send(rq, N093_02_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N093_02_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N093_02_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N203_VA
     */
    public BaseResult N203_VA_REST(Map<String, String> reqParam) {
        log.trace("N203_VA_REST_RQ");
        BaseResult bs = null;
        N203_VA_REST_RQ rq = null;
        N203_VA_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new N203_VA_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rs = restutil.send(rq, N203_VA_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N203_VA_REST_RS>>{}"+ CodeUtil.toJson(rs)));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N203_VA_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N10
     */
    public BaseResult N103_REST(String FGTXWAY, String cusidn) {
        log.trace("N103_REST_RQ");
        BaseResult bs = null;
        N103_REST_RQ rq = null;
        N103_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new N103_REST_RQ();
            rq.setCUSIDN(cusidn);
            rq.setFGTXWAY(FGTXWAY);
            rs = restutil.send(rq, N103_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N103_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N103_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * NB30
     */
    public BaseResult NB30_REST(Map<String, String> reqParam) {
        log.trace("NB30_REST_RQ");
        BaseResult bs = null;
        NB30_REST_RQ rq = null;
        NB30_REST_RS rs = null;
        try {
        	log.trace("reqParam >> {}", CodeUtil.toJson(reqParam));
            bs = new BaseResult();
            rq = new NB30_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
        	log.trace("rq >> {}", CodeUtil.toJson(rq));
            rs = restutil.send(rq, NB30_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("NB30_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("NB30_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }
    /**
     * N203
     */
    public BaseResult N203_REST(Map<String, String> reqParam) {
        log.trace("N203_REST_RQ");
        BaseResult bs = null;
        N203_REST_RQ rq = null;
        N203_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new N203_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rs = restutil.send(rq, N203_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N203_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N203_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N204
     */
    public BaseResult N2041_REST(Map<String, String> reqParam) {
        log.trace("N2041_REST_RQ");
        BaseResult bs = null;
        N2041_REST_RQ rq = null;
        N2041_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new N2041_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rs = restutil.send(rq, N2041_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N2041_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N2041_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N204
     */
    public BaseResult N2042_REST(Map<String, String> reqParam) {
        log.trace("N2042_REST_RQ");
        BaseResult bs = null;
        N2042_REST_RQ rq = null;
        N2042_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new N2042_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rs = restutil.send(rq, N2042_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N2042_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N2042_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N204
     */
    public BaseResult N204_REST(Map<String, String> reqParam) {
        log.trace("N204_REST_RQ");
        BaseResult bs = null;
        N204_REST_RQ rq = null;
        N204_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new N204_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rs = restutil.send(rq, N204_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N204_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N204_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N104
     */
    public BaseResult N104_REST(Map<String, String> reqParam) {
        log.trace("N104_REST_RQ");
        BaseResult bs = null;
        N104_REST_RQ rq = null;
        N104_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new N104_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rs = restutil.send(rq, N104_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N104_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N104_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N104
     */
    public BaseResult N104_1_REST(Map<String, String> reqParam) {
        log.trace("N104_1_REST_RQ");
        BaseResult bs = null;
        N104_1_REST_RQ rq = null;
        N104_1_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new N104_1_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rs = restutil.send(rq, N104_1_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N104_1_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N104_1_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N104
     */
    public BaseResult N104_2_REST(Map<String, String> reqParam) {
        log.trace("N104_2_REST_RQ");
        BaseResult bs = null;
        N104_2_REST_RQ rq = null;
        N104_2_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new N104_2_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rs = restutil.send(rq, N104_2_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N104_2_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N104_2_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 自然人憑證驗證
     */
    @SuppressWarnings("static-access")
    public BaseResult VACheck_REST(String AUTHCODE) {
        log.debug(ESAPIUtil.vaildLog("AUTHCODE >> " + AUTHCODE));

        BaseResult bs = null;
        Vacheck_REST_RQ rq = null;
        Vacheck_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new Vacheck_REST_RQ();
            rq.setAUTHCODE(AUTHCODE);

            rs = restutil.send(rq, Vacheck_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.debug(ESAPIUtil.vaildLog("rs >> " + CodeUtil.toJson(rs)));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("Vacheck_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * CK01 長期使用循環信用申請分期還款
     * 
     * @param reqParam
     * @return bs
     */
    public BaseResult CK01_REST_EAI(Map<String, String> reqParam) {
        log.trace("CK01_REST_EAI");
        BaseResult bs = null;
        CK01_REST_RQ rq = null;
        CK01_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new CK01_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rs = restutil.send(rq, CK01_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("CK01_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("CK01_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * CK01 長期使用循環信用申請分期還款 結果
     * 
     * @param reqParam
     * @return bs
     */
    public BaseResult CK01_EAI_RESULT(Map<String, String> reqParam) {
        log.trace("CK01_EAI_RESULT");
        BaseResult bs = null;
        CK01_1_REST_RQ rq = null;
        CK01_1_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new CK01_1_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rq.setADTXACNO(reqParam.get("CARDNUM"));
            rs = restutil.send(rq, CK01_1_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("CK01_1_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("CK01_1_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 變更信用卡帳單地址／電話
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */
    public BaseResult N900_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("N900_REST");
        log.trace("cusidn>>{}", cusidn);
        BaseResult bs = null;
        N900_REST_RQ rq = null;
        N900_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N900_REST_RQ();
            rq = CodeUtil.objectCovert(N900_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);
            rq.setUID(cusidn);
            rs = restutil.send(rq, N900_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N900_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N900_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N106
     * 
     * @param reqParam
     * @return bs
     */
    public BaseResult N106_REST(Map<String, String> reqParam) {
        log.trace("N106_REST");
        BaseResult bs = null;
        N106_REST_RQ rq = null;
        N106_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new N106_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rs = restutil.send(rq, N106_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N106_REST_RS>>{}"+rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N106_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N903EA
     * 
     * @param reqParam
     * @return bs
     */
    public BaseResult N930EA_REST(Map<String, String> reqParam) {
        log.trace("N106_REST");
        BaseResult bs = null;
        N930EA_REST_RQ rq = null;
        N930EA_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new N930EA_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rs = restutil.send(rq, N930EA_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N930EA_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N930EA_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * NA40使用臺灣企銀信用卡(step2)
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */

    public BaseResult NA40_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("NA40_REST");
        log.trace(ESAPIUtil.vaildLog("cusidn>>{}"+ cusidn));
        BaseResult bs = null;
        NA40_REST_RQ rq = null;
        NA40_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new NA40_REST_RQ();
            rq = CodeUtil.objectCovert(NA40_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);
            rs = restutil.send(rq, NA40_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("NA40_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("NA40_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * NA40使用臺灣企銀信用卡(結果)
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */

    public BaseResult NA40_1_REST(String cusidn, Map<String, String> reqParam) {
        log.trace("NA40_1_REST");
        log.trace(ESAPIUtil.vaildLog("cusidn>>{}"+ cusidn));
        BaseResult bs = null;
        NA40_1_REST_RQ rq = null;
        NA40_1_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new NA40_1_REST_RQ();
            rq = CodeUtil.objectCovert(NA40_1_REST_RQ.class, reqParam);
            rq.setCUSIDN(cusidn);
            rs = restutil.send(rq, NA40_1_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("NA40_1_REST_RS>>{}"+rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("NA40_1_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N660
     * 
     * @param reqParam
     * @return bs
     */
    public BaseResult N660_REST(Map<String, String> reqParam) {
        log.trace("N660_REST");
        BaseResult bs = null;
        N660_REST_RQ rq = null;
        N660_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new N660_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rs = restutil.send(rq, N660_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N660_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N660_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * N3003
     * 
     * @param reqParam
     * @return bs
     */
    public BaseResult N3003_REST(Map<String, String> reqParam, String cusidn, String adopid) {
        log.trace("N3003_REST");
        BaseResult bs = null;
        N3003_REST_RQ rq = null;
        N3003_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new N3003_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rq.setCUSIDN(cusidn);
            rq.setUID(cusidn);
            
            rq.setADOPID(adopid);
            rq.setADSVBH("050");
            rq.setADAGREEF("0");
            rs = restutil.send(rq, N3003_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N3003_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N3003_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 發送簡訊驗證碼OTP
     * 
     * @param reqParam
     * @return
     */
    public BaseResult SmsOtp_REST(Map<String, String> reqParam) {
        log.trace("Smsotp_REST");
        BaseResult bs = null;
        Smsotp_REST_RQ rq = null;
        Smsotp_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new Smsotp_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rs = restutil.send(rq, Smsotp_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("Smsotp_REST_RS>>{}"+rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("Smsotp_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

/**
     * 發送簡訊驗證碼OTP for MS_OLA
     * 
     * @param reqParam
     * @return
     */
    public BaseResult Send_Otp_REST(Map<String, String> reqParam) {
        log.trace("Smsotp_REST");
        BaseResult bs = null;
        OLA_OTP_REST_RQ rq = null;
        OLA_OTP_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new OLA_OTP_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rs = restutil.send(rq, OLA_OTP_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("Send_Otp_REST>>{}"+rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("OLA_OTP_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * NA721重設交易密碼
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */
    public BaseResult NA721_REST(Map<String, String> reqParam) {
        log.trace("NA721_REST");
        BaseResult bs = null;
        NA721_REST_RQ rq = null;
        NA721_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new NA721_REST_RQ();
            rq = CodeUtil.objectCovert(NA721_REST_RQ.class, reqParam);
            rs = restutil.send(rq, NA721_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("NA721_REST_RS>>{}"+rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("NA721_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * NA72變更使用名稱/簽入密碼
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */
    public BaseResult NA72_REST(Map<String, String> reqParam) {
        log.trace("NA72_REST");
        BaseResult bs = null;
        NA72_REST_RQ rq = null;
        NA72_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new NA72_REST_RQ();
            rq = CodeUtil.objectCovert(NA72_REST_RQ.class, reqParam);
            rs = restutil.send(rq, NA72_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("NA72_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("NA72_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 根據訊息代碼填入訊息
     * 
     * @param bs
     */
    public void getMessageByMsgCode(BaseResult bs) {
        //log.trace("bs>>{}", bs);
        String msg = "";
        String msgCode = "";
        if (bs != null) {
            msg = bs.getMessage();
            msgCode = bs.getMsgCode();
            log.trace("r>>{}", msg);
            log.trace("MsgCode>>{}", msgCode);
            if ("0".equals(bs.getMsgCode())) {
                log.trace("MsgCode is 0  return..");
                return;
            }
            
            msg = getMessageByMsgCode(bs.getMsgCode());
            bs.setMessage(msgCode, msg);
            log.trace("msg>>{}", bs.getMessage());
           
        }
    }

    /**
     * 根據訊息代碼取得訊息
     * 
     * @param msgCode
     * @return
     */
    public String getMessageByMsgCode(String msgCode) {
        String message = "";
        ADMMSGCODE po = null;
        try {
        	
        	Locale currentLocale = LocaleContextHolder.getLocale();
    		log.debug("errorMsg.locale >> {}" , currentLocale);
    		String locale = currentLocale.toString();
    		log.debug("errorMsg.locale2 >> {}" , locale);
            log.trace("admMsgCodeDao>>{}", admMsgCodeDao);
            if (admMsgCodeDao.hasMsg(msgCode, null)) {
                po = admMsgCodeDao.get(ADMMSGCODE.class, msgCode);
                //                message = po.getADMSGIN();
                
                switch (locale) {
				case "en":
					message = po.getADMSGOUTENG();
					break;
				case "zh_TW":
					message = po.getADMSGOUT();
					break;
				case "zh_CN":
					message = po.getADMSGOUTCHS();
					break;
                }
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            //e.printStackTrace();
        	//Avoid Information Exposure Through an Error Message
        	log.error("getMessageByMsgCode Error >> {}", e);
        }
        log.trace("message>>{}", message);
        return message;
    }
    /**
     * SENDADMAIL
     * 

     */
    public BaseResult SendAdMail_REST(String Subject,String Content,List<String> Receivers,String contentType) {
        log.trace("SendAdMail_REST");
        BaseResult bs = null;
        Sendadmail_REST_RQ rq = null;
        Sendadmail_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new Sendadmail_REST_RQ();
            rq.setSubject(Subject);
            rq.setContent(Content);
            rq.setReceivers(Receivers);
            rq.setContentType(contentType);
            rs = restutil.send(rq, Sendadmail_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("SendAdMail_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("SendAdMail_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }
    
    
    
    /**
     * 資產負債圖
     * 
     * @param cusidn
     * @param acn
     * @param ms_Channel
     * @return
     */
    public BaseResult A1000_REST(String cusidn, String acn, String ms_Channel) {
    	log.trace("A1000_REST");
    	log.trace("A1000_REST.cusidn: {}", cusidn);
    	BaseResult bs = null;
    	A1000_REST_RQ rq = null;
    	A1000_REST_RS rs = null;
    	try {
    		bs = new BaseResult();
    		
    		rq = new A1000_REST_RQ();
    		rq.setCust_ID(cusidn);
    		
    		rs = restutil.send(rq, A1000_REST_RS.class, rq.getAPI_Name(rq.getClass(), ms_Channel));
    		log.trace(ESAPIUtil.vaildLog("A1000_REST_RS: {}"+ rs));
    		
    		if (rs != null) {
    			CodeUtil.convert2BaseResult(bs, rs);
    			
    		} else {
    			log.error("A1000_REST_RS is null");
    			bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
    			
    		}
    	} catch (Exception e) {
    		log.error("", e);
    		bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
    		
    	}
    	
    	this.getMessageByMsgCode(bs);
    	return bs;
    }
    

    /**
     * EBPay1
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */
    public BaseResult EBPay1_REST(Map<String, String> reqParam) {
        log.trace("EBPay1_REST");
        BaseResult bs = null;
        EBPay1_REST_RQ rq = null;
        EBPay1_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new EBPay1_REST_RQ();
            rq = CodeUtil.objectCovert(EBPay1_REST_RQ.class, reqParam);
            rq.setFGTXWAY("3");
            rq.setADSVBH("050");
            rs = restutil.send(rq, EBPay1_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("EBPay1_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("EBPay1_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }
    /**
     * EBPay1_1
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */
    public BaseResult EBPay1_1_REST(Map<String, String> reqParam) {
        log.trace("EBPay1_1_REST");
        BaseResult bs = null;
        EBPay1_1_REST_RQ rq = null;
        EBPay1_1_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new EBPay1_1_REST_RQ();
            rq = CodeUtil.objectCovert(EBPay1_1_REST_RQ.class, reqParam);
            rq.setADOPID("EBPay1");
            rq.setFGTXWAY("3");
            rq.setADSVBH("050");
            rs = restutil.send(rq, EBPay1_1_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("EBPay1_1_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("EBPay1_1_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }
    public BaseResult GoldService_REST(Map<String, String> reqParam) 
	{
		log.info("GoldService_REST");
		BaseResult bs = null ;
		GoldService_REST_RQ rq = null;
		GoldService_REST_RS rs = null;
		try {
			bs =new  BaseResult();
			//		 call REST API
			rq = new GoldService_REST_RQ();
			rq = CodeUtil.objectCovert(GoldService_REST_RQ.class,reqParam);
//			rs = RESTfulClientUtil.send(rq, GoldService_REST_RS.class, rq.getAPI_Name(rq.getClass()));
			rs = RESTfulClientUtil.send(rq, GoldService_REST_RS.class, rq.getAPI_Name(rq.getClass() ,ConfingManager.MS_GOLD+"ws" ) );
			log.info("GoldService_REST_RS>>{}",rs);
			if(rs != null ) {
				CodeUtil.convert2BaseResult(bs, rs);
			}else {
				log.error("GoldService_REST_RS is null");
				bs.setSYSMessage(ResultCode.SYS_ERROR_TEL);
				this.getMessageByMsgCode(bs);
			}
		} catch (Exception e) {
			log.error("",e);
			bs.setSYSMessage(ResultCode.SYS_ERROR);
			this.getMessageByMsgCode(bs);
		}
		return  bs;
	}
    
    /**
     * QR10  檢查銷帳編號與統一編號是否一致
     * 
     * @param reqParam
     * @return
     */
    public BaseResult QR10_REST(Map<String, String> reqParam) {
        log.trace("QR10_REST start");
        BaseResult bs = null;
        QR10_REST_RQ rq = null;
        QR10_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = CodeUtil.objectCovert(QR10_REST_RQ.class, reqParam);
            log.trace(ESAPIUtil.vaildLog("toString >> " + rq.toString()));
            rs = restutil.send(rq, QR10_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("QR10_REST>>{}"+rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("QR10_REST is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("QR10_REST", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }
    
    
    /**
     * Q072  繳納本行國際信用卡帳款FOR QRCODE
     * 
     * @param reqParam
     * @return
     */
    public BaseResult Q072_REST(Map<String, String> reqParam) {
        log.trace("Q072_REST start");
        BaseResult bs = null;
        Q072_REST_RQ rq = null;
        Q072_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = CodeUtil.objectCovert(Q072_REST_RQ.class, reqParam);
            log.trace(ESAPIUtil.vaildLog("toString >> " + rq.toString()));
            rs = restutil.send(rq, Q072_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("Q072_REST>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("Q072_REST is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            }
        } catch (Exception e) {
            log.error("Q072_REST", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
        }
        
        if ("0".equals(bs.getMsgCode()) || StrUtils.isEmpty(bs.getMessage()) || "null".equals(bs.getMessage())) {
        	this.getMessageByMsgCode(bs);
        }
        
        return bs;
    }
    
    /**
     * RateQuery
     * 
     * @param reqParam
     * @return
     */
    public BaseResult RateQuery_REST(Map<String, String> reqParam) {
		log.trace("RateQuery_REST start");
		BaseResult bs = null;
		Ratequery_REST_RQ rq = null;
		try {
			bs = new BaseResult();
			rq = CodeUtil.objectCovert(Ratequery_REST_RQ.class, reqParam);
			log.trace(ESAPIUtil.vaildLog("toString >> " + rq.toString()));
			switch ((String) reqParam.get("QUERYTYPE")) {
			case "N021":
				Ratequery_N021_REST_RS n021rs = restutil.send(rq, Ratequery_N021_REST_RS.class,rq.getAPI_Name(rq.getClass()));
				if (n021rs != null) {
					CodeUtil.convert2BaseResult(bs, n021rs);
				} else {
					log.error("Ratequery_N021_REST_RS is null");
					bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
				}
				break;
			case "N022":
				Ratequery_N022_REST_RS n022rs = restutil.send(rq, Ratequery_N022_REST_RS.class,rq.getAPI_Name(rq.getClass()));
				if (n022rs != null) {
					CodeUtil.convert2BaseResult(bs, n022rs);
				} else {
					log.error("Ratequery_N022_REST_RS is null");
					bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
				}
				break;
			case "N023":
				Ratequery_N023_REST_RS n023rs = restutil.send(rq, Ratequery_N023_REST_RS.class,rq.getAPI_Name(rq.getClass()));
				if (n023rs != null) {
					CodeUtil.convert2BaseResult(bs, n023rs);
				} else {
					log.error("Ratequery_N023_REST_RS is null");
					bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
				}
				break;
			case "N024":
				Ratequery_N024_REST_RS n024rs = restutil.send(rq, Ratequery_N024_REST_RS.class,rq.getAPI_Name(rq.getClass()));
				if (n024rs != null) {
					CodeUtil.convert2BaseResult(bs, n024rs);
				} else {
					log.error("Ratequery_N024_REST_RS is null");
					bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
				}
				break;
			case "N025":
				Ratequery_N025_REST_RS n025rs = restutil.send(rq, Ratequery_N025_REST_RS.class,rq.getAPI_Name(rq.getClass()));
				if (n025rs != null) {
					CodeUtil.convert2BaseResult(bs, n025rs);
				} else {
					log.error("Ratequery_N025_REST_RS is null");
					bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
				}
				break;
			case "N026":
				Ratequery_N026_REST_RS n026rs = restutil.send(rq, Ratequery_N026_REST_RS.class,rq.getAPI_Name(rq.getClass()));
				if (n026rs != null) {
					CodeUtil.convert2BaseResult(bs, n026rs);
				} else {
					log.error("Ratequery_N026_REST_RS is null");
					bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
				}
				break;
			case "N027":
				Ratequery_N027_REST_RS n027rs = restutil.send(rq, Ratequery_N027_REST_RS.class,rq.getAPI_Name(rq.getClass()));
				if (n027rs != null) {
					CodeUtil.convert2BaseResult(bs, n027rs);
				} else {
					log.error("Ratequery_N027_REST_RS is null");
					bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
				}
				break;
			case "N030":
				Ratequery_N030_REST_RS n030rs = restutil.send(rq, Ratequery_N030_REST_RS.class,rq.getAPI_Name(rq.getClass()));
				if (n030rs != null) {
					CodeUtil.convert2BaseResult(bs, n030rs);
				} else {
					log.error("Ratequery_N030_REST_RS is null");
					bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
				}
				break;
			}
			

		} catch (Exception e) {
			log.error("RateQuery_REST", e);
			bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
		}
		this.getMessageByMsgCode(bs);
		return bs;

	}
    /**
     * n022 
     * 
     * @param reqParam
     * @return
     */
	
    public BaseResult getMobileCenterStatus(String channel) {
        
    	log.trace("N022_REST_RS start 3");
        BaseResult bs = null;
        N022_REST_RS rs = null;
        N022_REST_RQ rq=null;
        Map <String,String>temp=new HashMap<String,String>();
        try {
            bs = new BaseResult();
            rq = CodeUtil.objectCovert(N022_REST_RQ.class,temp);
            rs = restutil.send(rq, N022_REST_RS.class,channel);
            log.trace(ESAPIUtil.vaildLog("N022_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
//              if(bs.getMsgCode().equals("99"))bs.setResult(true);改做在MS
            } else {
                log.error("N022_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            }
        } catch (Exception e) {
            log.error("N022_REST", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }
    
    /**
     * 
     * 
     * @param reqParam
     * @return
     */
    public BaseResult reSend_REST(Map<String, String> reqParam) {
        log.trace("Sendmail_REST start");
        BaseResult bs = null;
        Sendmail_REST_RQ rq = null;
        Sendmail_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = CodeUtil.objectCovert(Sendmail_REST_RQ.class, reqParam);
            log.trace(ESAPIUtil.vaildLog("toString >> " + rq.toString()));
            rs = restutil.send(rq, Sendmail_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("Sendmail_REST>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("Sendmail_REST is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            }
        } catch (Exception e) {
            log.error("Sendmail_REST", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    public BaseResult Ikeycheck_REST(Map<String, String> reqParam) {
        log.trace("Ikeycheck_REST");
        BaseResult bs = null;
        Ikeycheck_REST_RQ rq = null;
        Ikeycheck_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new Ikeycheck_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rs = restutil.send(rq, Ikeycheck_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("Ikeycheck_REST>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("Ikeycheck_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }
    public BaseResult Aioedm_REST(Map<String, String> reqParam) {
        log.trace("Aioedm_REST");
        BaseResult bs = null;
        Aioedm_REST_RQ rq = null;
        Aioedm_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new Aioedm_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rs = restutil.send(rq, Aioedm_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("Aioedm_REST>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("Aioedm_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }
    
    /**
     * NP10--檢查行員才能使用行內IP
     * 
     * @param sessionId
     * @return
     */
    public BaseResult NP10_REST(String cusidn, String loginip) {
        log.trace("NP10_REST");
        BaseResult bs = null;
        NP10_REST_RQ rq = null;
        NP10_REST_RS rs = null;

        try {
            bs = new BaseResult();

            rq = new NP10_REST_RQ();
            rq.setCUSIDN(cusidn);
            rq.setIP(loginip);

            rs = restutil.send(rq, NP10_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("NP10_REST_RS>>{}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("NP10_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }

        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }
    
    
    /**
     * C011--檢查傳入之使用者及EMAIL在FUND主機EMAI是否重複
     * 
     * @param cusidn
     * @param email
     * @return
     */
    public BaseResult C011_REST(String cusidn, String email) {
        log.trace("C011_REST");
        BaseResult bs = null;
        C011_REST_RQ rq = null;
        C011_REST_RS rs = null;

        try {
            bs = new BaseResult();

            rq = new C011_REST_RQ();
            rq.setCUSIDN(cusidn);
            rq.setEMAIL(email);

            rs = restutil.send(rq, C011_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("C011_REST_RS>>{}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("C011_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }

        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }
        
    /**
     * N205數位存款帳戶存摺封面下載
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */
    public BaseResult N205_REST(String cusidn) {
        log.trace("N205_REST");
        BaseResult bs = null;
        N205_REST_RQ rq = null;
        N205_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N205_REST_RQ();
            rq.setCUSIDN(cusidn);
//            rq = CodeUtil.objectCovert(N205_REST_RQ.class, reqParam);
            rs = restutil.send(rq, N205_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N205_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N205_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }
    /*
     * IDgate身分
     * 
     * @param cusidn
     * @param email
     * @return
     */
    public BaseResult IdGateMappingQuery_REST(String cusidn) {
        log.trace("IdGateMappingQuery_REST");
        BaseResult bs = null;
        IdGateMappingQuery_REST_RQ rq = null;
        IdGateMappingQuery_REST_RS rs = null;

        try {
            bs = new BaseResult();

            rq = new IdGateMappingQuery_REST_RQ();
            rq.setCUSIDN(cusidn);

            rs = restutil.send(rq, IdGateMappingQuery_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("IdGateMappingQuery_REST_RS>>{}"+ CodeUtil.toJson(rs)));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("IdGateMappingQuery_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }
    
    /**
     * N822信用卡調額核身查詢
     * @param reqParam
     * @return
     */
    public BaseResult N822_REST(Map<String, String> reqParam) {
    	log.trace("N822_REST");
    	BaseResult bs = null;
    	N822_REST_RQ rq = null;
    	N822_REST_RS rs = null;
    	try {
    		bs = new BaseResult();
    		// call REST API
    		rq = new N822_REST_RQ();
            rq = CodeUtil.objectCovert(N822_REST_RQ.class, reqParam);
    		rs = restutil.send(rq, N822_REST_RS.class, rq.getAPI_Name(rq.getClass()));
    		log.trace(ESAPIUtil.vaildLog("N822_REST_RS>>{}"+ rs));
    		if (rs != null) {
    			CodeUtil.convert2BaseResult(bs, rs);
    		} else {
    			log.error("N822_REST_RS is null");
    			bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
    			
    		}
    	} catch (Exception e) {
    		log.error("", e);
    		bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
    		
    	}
    	this.getMessageByMsgCode(bs);
    	return bs;
    }
    
    /**
     * EloanApi
     * @param reqParam
     * @return
     */
    public BaseResult EloanApi_REST(Map<String, String> reqParam) {
    	log.trace("EloanApi_REST");
    	BaseResult bs = null;
    	EloanApi_REST_RQ rq = null;
    	EloanApi_REST_RS rs = null;
    	try {
    		bs = new BaseResult();
    		// call REST API
    		rq = new EloanApi_REST_RQ();
    		rq = CodeUtil.objectCovert(EloanApi_REST_RQ.class, reqParam);
    		rs = restutil.send(rq, EloanApi_REST_RS.class, rq.getAPI_Name(rq.getClass()));
    		log.trace(ESAPIUtil.vaildLog("EloanApi_REST_RS>>{}"+ rs));
    		if (rs != null) {
    			CodeUtil.convert2BaseResult(bs, rs);
    		} else {
    			log.error("EloanApi_REST_RS is null");
    			bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
    			
    		}
    	} catch (Exception e) {
    		log.error("", e);
    		bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
    		
    	}
    	this.getMessageByMsgCode(bs);
    	return bs;
    }
    /**
    * N857
    * @param attibk	查詢行庫別
    * @param attiac	查詢帳號
    * @param atdeno	客戶統編
    * @param atmobi	手機號碼
    * @param atmseq	交易序號
    * @return
    */
    public BaseResult N857_REST(String attibk, String attiac, String atdeno,String atmobi,String atmseq,String birth) {
        log.trace("N857_REST_RQ");
        BaseResult bs = null;
        N857_REST_RQ rq = null;
        N857_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new N857_REST_RQ();
            rq.setATTIBK(attibk);
            rq.setATTIAC(attiac);
            rq.setATDENO(atdeno);
            rq.setATMOBI(atmobi);
            rq.setATMSEQ(atmseq);
            rq.setBIRTH(birth);
            rs = restutil.send(rq, N857_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N857_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N857_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        if (bs.getMsgCode().equals("0000")) {
        	bs.setResult(Boolean.TRUE);
        }
        return bs;
    }	
    /*
     * IDgate啟動驗證
     * 
     * @param cusidn
     * @param email
     * @return
     */
    public BaseResult svCreate_Verify_Txn_REST(Map<String, Object> reqParam) {
        log.trace("svCreate_Verify_Txn_REST");
        BaseResult bs = null;
        svCreate_Verify_Txn_REST_RQ rq = null;
        svCreate_Verify_Txn_REST_RS rs = null;

        try {
            bs = new BaseResult();

            rq = new svCreate_Verify_Txn_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);

            rs = restutil.send(rq, svCreate_Verify_Txn_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("svCreate_Verify_Txn_REST_RS>>{}"+ rs));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("svCreate_Verify_Txn_REST is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }
    
    
    /**
     * IDgate NB3 to IDGateCommand
     * 
     * 
     * @param reqParam
     * @return
     */
    public Boolean doIDGateCommand_REST(Map reqParam,String ms_Channel) {
        log.info("doIDGateCommand_REST");
        DoIDGateCommand_REST_RQ rq = null;
        DoIDGateCommand_REST_RS rs = null;
        try {
            rq = new DoIDGateCommand_REST_RQ(ms_Channel);
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rs = restutil.send(rq, DoIDGateCommand_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("DoIDGateCommand_REST_RS>>{}"+ rs));
        } catch (Exception e) {
            log.error("doIDGateCommand_REST error >>{} ", e.getMessage());
        }
        
        return rs.getResult();
    }
     /**
     * IDgate取消
     * 
     * @param cusidn
     * @param email
     * @return
     */
    public BaseResult SvCancel_Txn_REST(Map<String, Object> reqParam) {
        log.trace("SvCancel_Txn_REST");
        BaseResult bs = null;
        svCancel_Txn_REST_RQ rq = null;
        svCancel_Txn_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new svCancel_Txn_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rs = restutil.send(rq, svCancel_Txn_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("SvCancel_Txn_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("SvCancel_Txn_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);                
            }

        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);           
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * 受託投資海外債券特別約定事項同意書查詢
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */
    public BaseResult B017_REST(Map<String, String> reqParam) {
        log.trace("B017_REST");
        log.trace("cusidn>>{}", reqParam.get("CUSIDN"));
        BaseResult bs = null;
        B017_REST_RQ rq = null;
        B017_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new B017_REST_RQ();
            rq = CodeUtil.objectCovert(B017_REST_RQ.class, reqParam);
            rs = restutil.send(rq, B017_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("B017_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("B017_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }
    
    /**
     * 海外債券價格查詢
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */
    public BaseResult B018_REST(Map<String, String> reqParam) {
        log.trace("B018_REST");
        log.trace("cusidn>>{}", reqParam.get("CUSIDN"));
        BaseResult bs = null;
        B018_REST_RQ rq = null;
        B018_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new B018_REST_RQ();
            rq = CodeUtil.objectCovert(B018_REST_RQ.class, reqParam);
            rs = restutil.send(rq, B018_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("B018_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("B018_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }
    
    /**
     * 海外債券申購交易(明細)
     * I01 類別(1:檢核 2:確認)
     * @param cusidn
     * @param reqParam
     * @return
     */
    public BaseResult B019_REST(Map<String, String> reqParam) {
        log.trace("B019_REST");
        log.trace("cusidn>>{}", reqParam.get("CUSIDN"));
        BaseResult bs = null;
        B019_REST_RQ rq = null;
        B019_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new B019_REST_RQ();
            rq = CodeUtil.objectCovert(B019_REST_RQ.class, reqParam);
            
            if("1".equals(rq.getI01())) {
            	rq.setIsTxnlLog("N");
            }
            
            rs = restutil.send(rq, B019_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("B019_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("B019_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }
    
    /**
     * 簽署受託投資海外債券特別約定事項同意書
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */
    public BaseResult B025_REST(Map<String, String> reqParam) {
        log.trace("B025_REST");
        BaseResult bs = null;
        B025_REST_RQ rq = null;
        B025_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new B025_REST_RQ();
            rq = CodeUtil.objectCovert(B025_REST_RQ.class, reqParam);
            rs = restutil.send(rq, B025_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("B025_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("B025_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }

    /**
     * B023
     * @param reqParam
     * @return
     */
    public BaseResult B023_REST(String cusidn, Map<String, String> reqParam) {
    	log.trace("B023_REST");
    	BaseResult bs = null;
    	B023_REST_RQ rq = null;
    	B023_REST_RS rs = null;
    	try {
    		bs = new BaseResult();
    		// call REST API
    		rq = new B023_REST_RQ();
    		rq = CodeUtil.objectCovert(B023_REST_RQ.class, reqParam);
    		rq.setCUSIDN(cusidn);
    		rs = restutil.send(rq, B023_REST_RS.class, rq.getAPI_Name(rq.getClass()));
    		log.trace(ESAPIUtil.vaildLog("B023_REST_RS>>{}"+ rs));
    		if (rs != null) {
    			CodeUtil.convert2BaseResult(bs, rs);
    		} else {
    			log.error("B023_REST_RS is null");
    			bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
    			
    		}
    	} catch (Exception e) {
    		log.error("", e);
    		bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
    		
    	}
    	this.getMessageByMsgCode(bs);
    	return bs;
    }
    
    /**
     * B021 海外債券贖回交易(列表)
     * @param reqParam
     * @return
     */
    public BaseResult B021_REST( Map<String, String> reqParam ) {
    	log.trace("B021_REST");
    	BaseResult bs = null;
    	B021_REST_RQ rq = null;
    	B021_REST_RS rs = null;
    	try {
    		bs = new BaseResult();
    		// call REST API
    		rq = new B021_REST_RQ();
    		rq = CodeUtil.objectCovert(B021_REST_RQ.class, reqParam);
    		rs = restutil.send(rq, B021_REST_RS.class, rq.getAPI_Name(rq.getClass()));
    		log.trace(ESAPIUtil.vaildLog("B021_REST_RS>>{}"+ rs));
    		if (rs != null) {
    			CodeUtil.convert2BaseResult(bs, rs);
    		} else {
    			log.error("B021_REST_RS is null");
    			bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
    			
    		}
    	} catch (Exception e) {
    		log.error("", e);
    		bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
    		
    	}
    	this.getMessageByMsgCode(bs);
    	return bs;
    }
    
    /**
     * B022 海外債券贖回交易(明細)
     * @param reqParam
     * @return
     */
    public BaseResult B022_REST( Map<String, String> reqParam ) {
    	log.trace("B022_REST");
    	BaseResult bs = null;
    	B022_REST_RQ rq = null;
    	B022_REST_RS rs = null;
    	try {
    		bs = new BaseResult();
    		// call REST API
    		rq = new B022_REST_RQ();
    		rq = CodeUtil.objectCovert(B022_REST_RQ.class, reqParam);
    		
    		if("1".equals(rq.getI01())) {
    			rq.setIsTxnlLog("N");
            }
    		
    		rs = restutil.send(rq, B022_REST_RS.class, rq.getAPI_Name(rq.getClass()));
    		log.trace(ESAPIUtil.vaildLog("B022_REST_RS>>{}"+ rs));
    		if (rs != null) {
    			CodeUtil.convert2BaseResult(bs, rs);
    		} else {
    			log.error("B022_REST_RS is null");
    			bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
    			
    		}
    	} catch (Exception e) {
    		log.error("", e);
    		bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
    		
    	}
    	this.getMessageByMsgCode(bs);
    	return bs;
    }
    
    public BaseResult N961_REST(String cusidn) {
    	
        log.debug(ESAPIUtil.vaildLog("UID >> " + cusidn));
        BaseResult bs = null;
        N961_REST_RQ rq = null;
        N961_REST_RS rs = null;

        try {
            bs = new BaseResult();
            rq = new N961_REST_RQ();
            rq.setCUSIDN(cusidn);
            
            rs = restutil.send(rq, N961_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.debug(ESAPIUtil.vaildLog("rs={}"+ CodeUtil.toJson(rs)));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N961_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }
    
    public BaseResult N206_REST(String cusidn, String acn) {
    	
        log.debug(ESAPIUtil.vaildLog("UID >> " + cusidn));
        BaseResult bs = null;
        N206_REST_RQ rq = null;
        N206_REST_RS rs = null;

        try {
            bs = new BaseResult();
            rq = new N206_REST_RQ();
            rq.setCUSIDN(cusidn);
            rq.setACN(acn);
            
            rs = restutil.send(rq, N206_REST_RS.class, rq.getAPI_Name(rq.getClass()));

            log.debug(ESAPIUtil.vaildLog("rs={}"+ CodeUtil.toJson(rs)));

            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs, rs.getMSGCOD(), rs.getMSGNAME());
            } else {
                log.error("N206_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);            
        }
        this.getMessageByMsgCode(bs);
        if (bs.getMsgCode().equals("0000") || bs.getMsgCode().equals("0")) {
        	bs.setResult(Boolean.TRUE);
        } else {
        	bs.setResult(Boolean.FALSE);
        }
        return bs;
    }
    
    /**
     * ELOAN驗證
     * @param NAME
     */
    public BaseResult ELOAN_REST(Map<String, String> reqParam) {
        log.trace("ELOAN_REST start");
        BaseResult bs = null;
        ELOAN_REST_RQ rq = null;
        ELOAN_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new ELOAN_REST_RQ();
            rq.setCUSIDN(reqParam.get("CUSIDN"));
            rq.setRCVNO(reqParam.get("RCVNO"));
            rq.setFNAME(reqParam.get("FNAME"));
            rq.setCPRIMBIRTHDAY(reqParam.get("CPRIMBIRTHDAY"));
            rs = restutil.send(rq, ELOAN_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            }
            log.trace(ESAPIUtil.vaildLog("ELOAN_REST_RS bs>>{}"+bs));
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
        }
        this.getMessageByMsgCode(bs);
        log.trace("ELOAN_REST end");
        return bs;
    }    
    
    
    /**
     * 發送簡訊驗證碼OTP Withphone
     * 
     * @param reqParam
     * @return
     */
    public BaseResult SmsOtpWithphone_REST(Map<String, String> reqParam) {
        log.trace("SmsotpWithphone_REST");
        BaseResult bs = null;
        SmsotpWithphone_REST_RQ rq = null;
        SmsotpWithphone_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new SmsotpWithphone_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rs = restutil.send(rq, SmsotpWithphone_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("Smsotp_REST_RS>>{}"+rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("SmsotpWithphone_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }        
    
    public BaseResult MtpUser_REST(Map<String, Object> reqParam) {
    	 log.trace("MtpUser Start");
         BaseResult bs = null;
         MtpUser_REST_RQ rq = null;
         MtpUser_REST_RS rs = null;
         try {
             bs = new BaseResult();
             rq = new MtpUser_REST_RQ();
             rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
             rs = restutil.send(rq, MtpUser_REST_RS.class, rq.getAPI_Name(rq.getClass()));
             log.trace(ESAPIUtil.vaildLog("MtpUser_REST_RQ_REST_RS>>{}"+ rs));
             if (rs != null) {
                 CodeUtil.convert2BaseResult(bs, rs);
             } else {
                 log.error("MtpUser_REST_RS is null");
                 bs.setMsgCode(ResultCode.SYS_ERROR_TEL);                
             }

         } catch (Exception e) {
             log.error("", e);
             bs.setMsgCode(ResultCode.SYS_ERROR_TEL);           
         }
         this.getMessageByMsgCode(bs);
         return bs;
    }
    
    public BaseResult MtpBindQry_REST(Map<String, String> reqParam) {
      	 log.trace("MtpBindQry_REST Start");
           BaseResult bs = null;
           MtpBindQry_REST_RQ rq = null;
           MtpBindQry_REST_RS rs = null;
           try {
               bs = new BaseResult();
               rq = new MtpBindQry_REST_RQ();
               rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
               rs = restutil.send(rq, MtpBindQry_REST_RS.class, rq.getAPI_Name(rq.getClass()));
               log.trace(ESAPIUtil.vaildLog("MtpBindQry_REST_RQ >>{}"+ rs));
               if (rs != null) {
                   CodeUtil.convert2BaseResult(bs, rs);
               } else {
                   log.error("MtpBindQry_REST_RS is null");
                   bs.setMsgCode(ResultCode.SYS_ERROR_TEL);                
               }

           } catch (Exception e) {
               log.error("", e);
               bs.setMsgCode(ResultCode.SYS_ERROR_TEL);           
           }
           this.getMessageByMsgCode(bs);
           return bs;
      }
    
    
    public BaseResult MtpQuery_REST(Map<String, String> reqParam) {
   	 log.trace("MTPQUERY Start");
        BaseResult bs = null;
        MtpQuery_REST_RQ rq = null;
        MtpQuery_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new MtpQuery_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rs = restutil.send(rq, MtpQuery_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("MtpQuery_REST_RQ >>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("MtpQuery_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);                
            }

        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);           
        }
        this.getMessageByMsgCode(bs);
        return bs;
   }
    
    
    
    
    public BaseResult MtpChangeAccount_REST(Map<String, Object> reqParam) {
      	 log.trace("MtpChangeAccount Start");
           BaseResult bs = null;
           MtpChangeAccount_REST_RQ rq = null;
           MtpChangeAccount_REST_RS rs = null;
           try {
               bs = new BaseResult();
               rq = new MtpChangeAccount_REST_RQ();
               rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
               rs = restutil.send(rq, MtpChangeAccount_REST_RS.class, rq.getAPI_Name(rq.getClass()));
               log.trace(ESAPIUtil.vaildLog("MtpChangeAccount_REST_RQ >>{}"+ rs));
               if (rs != null) {
                   CodeUtil.convert2BaseResult(bs, rs);
               } else {
                   log.error("MtpChangeAccount_REST_RS is null");
                   bs.setMsgCode(ResultCode.SYS_ERROR_TEL);                
               }
           } catch (Exception e) {
               log.error("", e);
               bs.setMsgCode(ResultCode.SYS_ERROR_TEL);           
           }
           this.getMessageByMsgCode(bs);
           return bs;
      }
    
    public BaseResult MtpUnbind_REST(Map<String, Object> reqParam) {
     	 log.trace("MtpChangeAccount Start");
          BaseResult bs = null;
          MtpUnbind_REST_RQ rq = null;
          MtpUnbind_REST_RS rs = null;
          try {
              bs = new BaseResult();
              rq = new MtpUnbind_REST_RQ();
              rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
              rs = restutil.send(rq, MtpUnbind_REST_RS.class, rq.getAPI_Name(rq.getClass()));
              log.trace(ESAPIUtil.vaildLog("MtpChangeAccount_REST_RQ >>{}"+ rs));
              if (rs != null) {
                  CodeUtil.convert2BaseResult(bs, rs);
              } else {
                  log.error("MtpChangeAccount_REST_RS is null");
                  bs.setMsgCode(ResultCode.SYS_ERROR_TEL);                
              }
          } catch (Exception e) {
              log.error("", e);
              bs.setMsgCode(ResultCode.SYS_ERROR_TEL);           
          }
          this.getMessageByMsgCode(bs);
          return bs;
     }
    
    public BaseResult MtpPreTransferInfoQry_REST(Map<String, String> reqParam) {
    	 log.trace("MtpPreTransferInfoQry_REST Start");
         BaseResult bs = null;
         MtpPreTransferInfoQry_REST_RQ rq = null;
         MtpPreTransferInfoQry_REST_RS rs = null;
         try {
             bs = new BaseResult();
             rq = new MtpPreTransferInfoQry_REST_RQ();
             rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
             rs = restutil.send(rq, MtpPreTransferInfoQry_REST_RS.class, rq.getAPI_Name(rq.getClass()));
             log.trace(ESAPIUtil.vaildLog("MtpPreTransferInfoQry_REST_RQ >>{}"+ rs));
             if (rs != null) {
                 CodeUtil.convert2BaseResult(bs, rs);
             } else {
                 log.error("MtpChangeAccount_REST_RS is null");
                 bs.setMsgCode(ResultCode.SYS_ERROR_TEL);                
             }
         } catch (Exception e) {
             log.error("", e);
             bs.setMsgCode(ResultCode.SYS_ERROR_TEL);           
         }
         this.getMessageByMsgCode(bs);
         return bs;
    } 
    
    public BaseResult MtpBankList_REST(Map<String, String> reqParam) {
    	
    	 log.trace("MtpChangeAccount Start");
         BaseResult bs = null;
         MtpBankList_REST_RQ rq = null;
         MtpBankList_REST_RS rs = null;
         try {
             bs = new BaseResult();
             rq = new MtpBankList_REST_RQ();
             rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
             rs = restutil.send(rq, MtpBankList_REST_RS.class, rq.getAPI_Name(rq.getClass()));
             log.trace(ESAPIUtil.vaildLog("MtpBankList_REST_RQ >>{}"+ rs));
             if (rs != null) {
                 CodeUtil.convert2BaseResult(bs, rs);
             } else {
                 log.error("MtpBankList_REST_RS is null");
                 bs.setMsgCode(ResultCode.SYS_ERROR_TEL);                
             }
         } catch (Exception e) {
             log.error("", e);
             bs.setMsgCode(ResultCode.SYS_ERROR_TEL);           
         }
         this.getMessageByMsgCode(bs);
         return bs;
    }
    /* N207 FATCA辨識查詢作業
     * 
     * @param reqParam
     * @return
     */
    public BaseResult N207_REST(Map<String, String> reqParam) {
        log.trace("N207_REST Start~~~~");
        BaseResult bs = null;
        N207_REST_RQ rq = null;
        N207_REST_RS rs = null;

        try {
            bs = new BaseResult();
            // 組上行
            rq = new N207_REST_RQ();
            rq = CodeUtil.objectCovert(N207_REST_RQ.class, reqParam);
            // 取得回應
            rs = restutil.send(rq, N207_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N207_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N207_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("N207_REST_RS error ", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }
        this.getMessageByMsgCode(bs);
        return bs;
    }
       
    /*
     * 
     */
    public BaseResult N930_TX_REST(Map<String, String> reqParam) {
        BaseResult bs = null;
        log.trace("N930EE_REST");
        N930EE_REST_RQ rq = null;
        N930EE_REST_RS rs = null;
        try {
            bs = new BaseResult();
            // call REST API
            rq = new N930EE_REST_RQ();
            rq = CodeUtil.objectCovert(rq.getClass(), reqParam);
            rs = restutil.send(rq, N930EE_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.trace(ESAPIUtil.vaildLog("N930EE_REST_RS>>{}"+ rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N930EE_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;

    }

}