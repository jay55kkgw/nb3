package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class NB30_REST_RQ extends BaseRestBean_OLA implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6802701423904201687L;
	private String CUSIDN;
	private String CHIP_ACN;
	private String ICSEQ;
	private String USERNAME;
	private String HLOGINPIN;
	private String HTRANSPIN;
	private String TRFLAG;
	private String ACNNO;
	private String MAILADDR;
	private String FGTXWAY;
	private String IP;
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getCHIP_ACN() {
		return CHIP_ACN;
	}
	public void setCHIP_ACN(String cHIP_ACN) {
		CHIP_ACN = cHIP_ACN;
	}
	public String getICSEQ() {
		return ICSEQ;
	}
	public void setICSEQ(String iCSEQ) {
		ICSEQ = iCSEQ;
	}
	public String getUSERNAME() {
		return USERNAME;
	}
	public void setUSERNAME(String uSERNAME) {
		USERNAME = uSERNAME;
	}
	public String getHLOGINPIN() {
		return HLOGINPIN;
	}
	public void setHLOGINPIN(String hLOGINPIN) {
		HLOGINPIN = hLOGINPIN;
	}
	public String getHTRANSPIN() {
		return HTRANSPIN;
	}
	public void setHTRANSPIN(String hTRANSPIN) {
		HTRANSPIN = hTRANSPIN;
	}
	public String getTRFLAG() {
		return TRFLAG;
	}
	public void setTRFLAG(String tRFLAG) {
		TRFLAG = tRFLAG;
	}
	public String getACNNO() {
		return ACNNO;
	}
	public void setACNNO(String aCNNO) {
		ACNNO = aCNNO;
	}
	public String getMAILADDR() {
		return MAILADDR;
	}
	public void setMAILADDR(String mAILADDR) {
		MAILADDR = mAILADDR;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getIP() {
		return IP;
	}
	public void setIP(String iP) {
		IP = iP;
	}
	
}
