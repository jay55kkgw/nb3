package tw.com.fstop.nnb.rest.bean;


import java.io.Serializable;


public class N580_1_REST_RS extends BaseRestBean implements Serializable {


	private static final long serialVersionUID = 202996937652061565L;
	private String OFFSET;//空白
	private String HEADER;//HEADER
	private String MSGCOD;//MSGCOD
	private String ACN;
	private String CUSIDN;
	private String occurMsg;
	private String DATE;
	private String TIME;
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getOccurMsg() {
		return occurMsg;
	}
	public void setOccurMsg(String occurMsg) {
		this.occurMsg = occurMsg;
	}
	public String getOFFSET() {
		return OFFSET;
	}
	public void setOFFSET(String oFFSET) {
		OFFSET = oFFSET;
	}
	public String getHEADER() {
		return HEADER;
	}
	public void setHEADER(String hEADER) {
		HEADER = hEADER;
	}
	public String getMSGCOD() {
		return MSGCOD;
	}
	public void setMSGCOD(String mSGCOD) {
		MSGCOD = mSGCOD;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}


}
