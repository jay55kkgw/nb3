package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N205_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7382163769927157129L;
	
	private String REC_NO;		// 筆數
	LinkedList<N205_REST_RSDATA> REC;
	
	
	public String getREC_NO() {
		return REC_NO;
	}
	public void setREC_NO(String rEC_NO) {
		REC_NO = rEC_NO;
	}
	public LinkedList<N205_REST_RSDATA> getREC() {
		return REC;
	}
	public void setREC(LinkedList<N205_REST_RSDATA> rEC) {
		REC = rEC;
	}
	
}