package tw.com.fstop.nnb.spring.controller;

import java.net.URLEncoder;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.google.gson.Gson;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.common.ResultCode;
import tw.com.fstop.idgate.bean.N072_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N072_IDGATE_DATA_VIEW;
import tw.com.fstop.nnb.service.Creditcard_Pay_Service;
import tw.com.fstop.nnb.service.DaoService;
import tw.com.fstop.nnb.service.IdGateService;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.ConfingManager;
import tw.com.fstop.web.util.WebUtil;

@SessionAttributes({ SessionUtil.PD, SessionUtil.CUSIDN, SessionUtil.DPMYEMAIL, SessionUtil.TRANSFER_DATA,
		SessionUtil.TRANSFER_CONFIRM_TOKEN, SessionUtil.TRANSFER_RESULT_TOKEN, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN,
		SessionUtil.BACK_DATA, SessionUtil.CONFIRM_LOCALE_DATA, SessionUtil.RESULT_LOCALE_DATA,SessionUtil.IDGATE_TRANSDATA })
@Controller
@RequestMapping(value = "/CREDIT/PAY")
public class Creditcard_Pay_Controller {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private Creditcard_Pay_Service creditcard_pay_service;
	@Autowired
	private DaoService daoservice;
	
	@Autowired		 
	IdGateService idgateservice;

	/**
	 * 要取得傳入帳號之”帳戶餘額“
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/getACNO_Data_aj", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody BaseResult getACNO_Data(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String str = "getACNO_Data";
		log.trace("getACNO_Data_aj >> {}", str);
		log.trace(ESAPIUtil.vaildLog("reqParam >>" + CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		try {
			// 登入時的身分證字號
			String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if (!"".equals(cusidn) && cusidn != null) {
				// 解密成明碼
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				String acno = reqParam.get("acno");
				bs = creditcard_pay_service.findOenByN110(cusidn, acno, ConfingManager.MS_CC);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getACNO_Data error >> {}",e);
		}
		return bs;
	}

	/**
	 * 取得繳納信用卡款之約定/常用非約定帳號之轉入帳號
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/getInAcno_aj", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody BaseResult getInACNO(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String str = "getInAcno_aj";
		log.trace("getInAcno_aj >> {}", str);
		log.trace(ESAPIUtil.vaildLog("reqParam >>" + CodeUtil.toJson(reqParam)));
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		BaseResult bs = new BaseResult();
		try {
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");
			String type = reqParam.get("type");
			bs = creditcard_pay_service.getInAcnoList(cusidn, type);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getInACNO error >> {}",e);
		}
		return bs;
	}

	/**
	 * 取得轉帳之轉出帳號
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/getOutAcno_aj", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody BaseResult getOutACNO(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String str = "getOutAcno_aj";
		log.trace("getOutAcno_aj >> {}", str);
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		// 要取得使用者相關帳號
		BaseResult bs = new BaseResult();
		try {
			// String sessionID = (String) SessionUtil.getAttribute(model,
			// SessionUtil.PORTAL_SESSION_ID, null);
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");
			String type = reqParam.get("type");
			bs = creditcard_pay_service.getOutAcnoList(cusidn, type);

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getOutACNO error >> {}",e);
		}
		return bs;

	}

	@RequestMapping(value = "/getBankList_aj", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody BaseResult getBankList(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String str = "getBankList_aj";
		log.trace("getBankList_aj >> {}", str);
		log.trace(ESAPIUtil.vaildLog("reqParam >>" + CodeUtil.toJson(reqParam)));
		// 要取得使用者相關帳號
		BaseResult bs = new BaseResult();
		try {
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");
			bs = daoservice.getDpBHNO_List(cusidn);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getBankList error >> {}",e);
		}
		return bs;

	}

	/**
	 * 進入N072-繳納本行信用卡款_輸入頁
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/payment")
	public String payment(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("creditcard_pay...");
		String target = "/error";
		BaseResult bs = null;
		BaseResult bs1 = null;
		Map<String, String> cleanMap=new HashMap<String, String>();
		try {
			bs = new BaseResult();
			bs1 = new BaseResult();
			
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			String getAcn = reqParam.get("Acn");
			log.trace(ESAPIUtil.vaildLog("GET ACN >>>{}"+getAcn));
			
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			
			// 登入時的身分證字號
			String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if (!"".equals(cusidn) && cusidn != null) {
				// 解密成明碼
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				log.trace("cusidn: " + cusidn);

				// Todo 轉出成功是否Email通知
				// boolean chk_result = pay_expense_service.chkContains(cusidn);
				// model.addAttribute("sendMe", chk_result);
				okMap.put("CUSIDN", cusidn);
			} else {
				log.error("session no cusidn!!!");
			}

			// 是否是回上一頁帶過來的
			if ("Y".equals(okMap.get("back"))) {
				model.addAttribute("bk_key", "Y");
			}else {
				SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, cleanMap);
			}

			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = creditcard_pay_service.getTxToken();
			log.trace("credit_pay.TXTOKEN: " + ((Map<String, String>) bs.getData()).get("TXTOKEN"));
			if (bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN,
						((Map<String, String>) bs.getData()).get("TXTOKEN"));
			}

			bs1 = creditcard_pay_service.payment_page(okMap);
			bs1.addData("CUSIDN",okMap.get("CUSIDN"));
			bs1.addData("Acn",getAcn);
			//第三階段說明
			String isProd = SpringBeanFactory.getBean("isProd");
			bs1.addData("isProd",isProd);
			
			 //IDGATE身分
	           String idgateUserFlag="N";		 
	           Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
	           try {		 
	        	   if(IdgateData==null) {
	        		   IdgateData = new HashMap<String, Object>();
	        	   }
	               BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
	               if(tmp.getResult()) {		 
	                   idgateUserFlag="Y";                  		 
	               }		 
	               tmp.addData("idgateUserFlag",idgateUserFlag);		 
	               IdgateData.putAll((Map<String, String>) tmp.getData());
	               SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	           }catch(Exception e) {		 
	               log.debug("idgateUserFlag error {}",e);		 
	           }		 
	           model.addAttribute("idgateUserFlag",idgateUserFlag);
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("payment error >> {}",e);
			bs.setSYSMessage(ResultCode.SYS_ERROR);
		} finally {
			if (bs1.getResult() && bs1.getResult()) {
				model.addAttribute("pay_information", bs1);
				target = "/creditcard/payment";

			} else {
				bs.setNext("/CREDIT/PAY/payment");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}

		log.trace("target >> {}", target);
		return target;
	}

	/**
	 * 進入N072-繳納本行信用卡款_確認頁
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/payment_confirm")
	public String payment_confirm(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("creditcard_pay_confirm...");
		String target = "/error";
		String jsondc = "";
		String previous = "/CREDIT/PAY/payment";
		String cusidn="";
		log.trace(ESAPIUtil.vaildLog("credit_pay_confirm_reqParam>>{}" + CodeUtil.toJson(reqParam)));

		BaseResult bs = new BaseResult();

		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);

		try {
			cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");
			//金額欄位避免前面欄位有0 ex:000123
			okMap.put("AMOUNT", String.valueOf(Integer.parseInt(okMap.get("AMOUNT"))));
			
			
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam), "UTF-8");
			log.trace(ESAPIUtil.vaildLog("creditcard_pay_confirm.jsondc: " + jsondc));

			okMap.put("jsondc", jsondc);
			log.trace(ESAPIUtil.vaildLog("creditcard_pay_confirm.okMap: {}" + CodeUtil.toJson(okMap)));

			// 回上一頁重新賦值
			if (okMap.containsKey("previous")) {
				Map<String, String> backMap = (Map<String, String>) SessionUtil.getAttribute(model,
						SessionUtil.BACK_DATA, null);
				okMap = backMap;
			}

			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				if (!bs.getResult()) {
					// TODO 交易結束後，CONFIRM_LOCALE_DATA 會被清空，造成在URL輸入確認頁會導向錯誤頁，原因待查
					log.trace("bs.getResult(): {}", bs.getResult());
					throw new Exception();
				} else {
					// session 資料放入 map 讓後續 model 和回上一頁取值
					okMap.putAll((Map<String, String>) bs.getData());
				}
			} else {
				// 驗證TOKEN 沒TOKEN會到錯誤頁，錯誤頁確認後返回輸入頁
				String token = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null);
				log.trace("creditcard_pay_confirm.token: {}", token);
				if (StrUtil.isNotEmpty(okMap.get("TOKEN")) && okMap.get("TOKEN").equals(token)) {
					bs.setResult(Boolean.TRUE);
				}
				log.trace("bs.getResult(): {}", bs.getResult());
				if (!bs.getResult()) {
					log.trace("throw new Exception()");
					throw new Exception();
				}
				
				// TOKEN驗證成功，資料處理後進入確認頁
				bs.reset();
				bs = creditcard_pay_service.payment_confirm(okMap);
				
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
				
				Map<String, String> dataMap = (Map<String,String>)bs.getData();
				//結果頁轉置先做 >> 為了 IDGATE_DATA 
				creditcard_pay_service.payment_Result_Data_Pre_Processing(dataMap);
				// IDGATE transdata            		 
	            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
	    		String adopid = "N072";
	    		String title ="";
	    		if("1".equals(okMap.get("FGTXDATE"))) {
	    			title = "您有一筆繳納本行信用卡費交易待確認，金額 "+dataMap.get("AMOUNT_SHOW");
	    		}else {
	    			title = "您有一筆預約繳納本行信用卡費交易待確認，金額 "+dataMap.get("AMOUNT_SHOW");
	    		}
	        	if(IdgateData != null) {
		            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
		            if(IdgateData.get("idgateUserFlag").equals("Y")) {
		            	dataMap.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
		            	dataMap.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
		            	dataMap.put("CUSIDN", cusidn);
		            	log.debug("IDGATE ADOPID >> {}" , adopid);
		            	log.debug("IDGATE TITLE >> {}" , title);
		            	log.debug("IDGATE VALUE >> {}" , dataMap);
						IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N072_IDGATE_DATA.class, N072_IDGATE_DATA_VIEW.class, dataMap));
		                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
		            }
	        	}
	            model.addAttribute("idgateAdopid", adopid);
	            model.addAttribute("idgateTitle", title);
				
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("payment_confirm error >> {}",e);
		} finally {
			if (bs.getResult()) {
				target = "/creditcard/payment_confirm";
				// 這邊還要調整 不能存物件，要存成JSNO字串
				model.addAttribute(SessionUtil.TRANSFER_DATA, bs);	
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, ((Map<String,Object>)bs.getData()).get("TXTOKEN"));
				
				// BHO
				String DPAGACNO = "";
				if ("1".equals(okMap.get("FLAG"))) { //已約定下拉值 已符合BHO controller需要的格式
					DPAGACNO = okMap.get("CARDNUM1");
				} else if ("2".equals(okMap.get("FLAG"))) { //繳納本行radio 需要組成BHO controller需要的格式
					DPAGACNO = "{\"ACN\":\"" + okMap.get("CARDNUM2") + "\"}";
				}else if ("0".equals(okMap.get("FLAG"))){ //非約定自輸入(CARDNUM2),需要組成BHO controller需要的格式  or 下拉值(CARDNUM3),已符合BHO controller需要的格式
					if(null!=okMap.get("CARDNUM2")&&!"".equals(okMap.get("CARDNUM2"))) {
						DPAGACNO = "{\"ACN\":\"" + okMap.get("CARDNUM2") + "\"}";
					}else {
						DPAGACNO = okMap.get("CARDNUM3");
					}
				}

				log.trace(ESAPIUtil.vaildLog("DPAGACNO={}" + DPAGACNO));
				// 將轉入帳號存起來
				Map<String, String> DPAGACNOMap = new Gson().fromJson(DPAGACNO, Map.class);
				String transferInAccount = DPAGACNOMap.get("ACN");
				log.trace(ESAPIUtil.vaildLog("transferInAccount={}"+ transferInAccount));
				model.addAttribute("transferInAccount", transferInAccount);
				// end BHO
				//SSL 交易機制開關
				if(cusidn.toUpperCase().equals(DPAGACNOMap.get("ACN").toUpperCase())) {
					model.addAttribute("SSLFLAG", "Y");
				}else model.addAttribute("SSLFLAG", "N");
				// 設定回上一頁的url,配合confirm頁面的js
				model.addAttribute("previous", "/CREDIT/PAY/payment");
				//測適用

				SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, okMap);
				

			} else {
				bs.setPrevious(previous);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}

		log.trace("target {}", target);
		return target;

	}

	/**
	 * 轉帳結果頁
	 * 
	 * @return
	 */
	@RequestMapping(value = "/payment_result")
	public String payment_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		String next = "/CREDIT/PAY/payment";
		log.trace(ESAPIUtil.vaildLog("reqParam {}" + CodeUtil.toJson(reqParam)));
		String jsondc ="";
		String pkcs7Sign ="";
		BaseResult bs = null;
		// 驗證 result token 如果存在 且相同表示F5 或i18n
		// 要CALL電文進行轉帳 注意要分預約非預約 約轉非約轉
		try {
			bs = new BaseResult();
			
			jsondc = reqParam.get("jsondc");
			pkcs7Sign = reqParam.get("pkcs7Sign");
			reqParam.remove("jsondc");
			reqParam.remove("pkcs7Sign");
			// 解決Trust Boundary Violation			
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			okMap.put("jsondc", jsondc);
			okMap.put("pkcs7Sign", pkcs7Sign);
			
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}

			if (!hasLocale) {
				log.trace("creditcard_pay_result.validate TXTOKEN...");
				log.trace("creditcard_pay_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("creditcard_pay_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("creditcard_pay_result.bs.step1 is successful...");
				}

				if (!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("creditcard_pay_result TXTOKEN 不正確，TXTOKEN>>{}"+okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						!okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("creditcard_pay_result.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("creditcard_pay_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				
				// 取得從輸入頁輸入存在session中的輸入資料
				bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
				Map<String, String> payment_data = (Map<String, String>) bs.getData();
				
				//bug fix 密碼錯誤回上一頁重打正確密碼，仍然錯誤，因為pin被覆蓋成舊的, TODO 查明transfer_data
				String tmpPw = okMap.get("PINNEW");
				okMap.putAll(payment_data);
				okMap.put("PINNEW", tmpPw);
				
				// 重置bs，進行交易
				bs.reset();
				
				okMap.put("CUSIDN", new String(
						Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
						"utf-8"));
				
				//IDgate驗證		 
                try {		 
                    if(okMap.get("FGTXWAY").equals("7")) {		 
        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N072_IDGATE");
                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));	 
                    }
                }catch(Exception e) {		 
                    log.error("IDGATE TRANS error>>{}",e);		 
                }  
				
				Map<String,Object>IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
				model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
				
				bs = creditcard_pay_service.payment_result(okMap);
				
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
			
			log.trace(ESAPIUtil.vaildLog("creditcard_pay_result bs >>{}" + bs.getData()));

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("payment_result error >> {}",e);
		} finally {
			// TODO 要清除必要的SESSION
			
			Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			// 交易成功要存之前的token 在Session 以利下次比對是否重複交易
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TXTOKEN"));
			if (bs.getResult()) {
				target = "/creditcard/payment_result";
				model.addAttribute("payment_result_data", bs);
				
			} else {
				// E004 是密碼錯誤 要讓使用者可以回上一步
				bs.setNext(next);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}

		log.trace("target >> {}", target);
		return target;
	}

	/**
	 * 要取得傳入帳號相關資料
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/addAcn_aj", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody BaseResult addAcn(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace(ESAPIUtil.vaildLog("addAcn_aj reqParam >> {} " + CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		try {
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");
			reqParam.put("CUSIDN", cusidn);
			reqParam.put("DPUSERID", cusidn);
			bs = daoservice.add_common_acount(reqParam);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("addAcn error >> {}",e);
		}

		return bs;

	}

	// /*回上一頁*/
	// @RequestMapping(value = "/paymentb" )
	// public String backpament(HttpServletRequest request, HttpServletResponse
	// response, @RequestParam Map<String, String> reqParam, Model model ) {
	//
	// String target = "/error";
	// String next = "/CREDIT/PAY/payment";
	// log.trace("reqParam>>{}",reqParam);
	// BaseResult bs = new BaseResult();
	// // 解決Trust Boundary Violation
	// Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
	// try {
	//
	// }catch(exp){
	//
	// }
	//
	//
	//
	// SessionUtil.addAttribute(model, SessionUtil.IS_BACK_KEY, "Y");
	// model.addAttribute("back_data",reqParam);
	//
	// return null;
	//
	// }
	//

}
