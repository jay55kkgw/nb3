package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * N372電文RQ
 */
public class N372_REST_RQ extends BaseRestBean_FUND implements Serializable{
	private static final long serialVersionUID = 6868861910004819383L;
	private String CUSIDN;
	private String FGTXWAY;
	private String PINNEW;
	private String TYPE;
	private String TRANSCODE;
	private String COUNTRYTYPE;
	private String TRADEDATE;
	private String AMT3;
	private String FCA2;
	private String OUTACN;
	private String INTSACN;
	private String AMT5;
	private String BILLSENDMODE;
	private String FCAFEE;
	private String SSLTXNO;
	private String PAYDAY1;
	private String PAYDAY2;
	private String PAYDAY3;
	private String BRHCOD;
	private String CUTTYPE;
	private String CRY1;
	private String STOP;
	private String YIELD;
	private String RSKATT;
	private String GETLTD;
	private String RRSK;
	private String RISK7;
	private String GETLTD7;
	private String KYCDATE;
	private String WEAK;
	private String FDAGREEFLAG;
	private String FDNOTICETYPE;
	private String FDPUBLICTYPE;
	private String DPMYEMAIL;
	private String CMTRMAIL;
	private String ADOPID = "C031";
	private String PEMAIL;
	private String IP;
	private String REPID;
	private String XFLAG;
	private String NUM;
	private String SLSNO;
	private String OFLAG;
	
	public String getPEMAIL() {
		return PEMAIL;
	}
	public void setPEMAIL(String pEMAIL) {
		PEMAIL = pEMAIL;
	}
	public String getIP() {
		return IP;
	}
	public void setIP(String iP) {
		IP = iP;
	}
	public String getREPID() {
		return REPID;
	}
	public void setREPID(String rEPID) {
		REPID = rEPID;
	}
	public String getXFLAG() {
		return XFLAG;
	}
	public void setXFLAG(String xFLAG) {
		XFLAG = xFLAG;
	}
	public String getNUM() {
		return NUM;
	}
	public void setNUM(String nUM) {
		NUM = nUM;
	}
	public String getSHWD() {
		return SHWD;
	}
	public void setSHWD(String sHWD) {
		SHWD = sHWD;
	}
	private String SHWD;
	
	public String getCUSIDN(){
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN){
		CUSIDN = cUSIDN;
	}
	public String getFGTXWAY(){
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY){
		FGTXWAY = fGTXWAY;
	}
	public String getPINNEW(){
		return PINNEW;
	}
	public void setPINNEW(String pINNEW){
		PINNEW = pINNEW;
	}
	public String getTYPE(){
		return TYPE;
	}
	public void setTYPE(String tYPE){
		TYPE = tYPE;
	}
	public String getTRANSCODE(){
		return TRANSCODE;
	}
	public void setTRANSCODE(String tRANSCODE){
		TRANSCODE = tRANSCODE;
	}
	public String getCOUNTRYTYPE(){
		return COUNTRYTYPE;
	}
	public void setCOUNTRYTYPE(String cOUNTRYTYPE){
		COUNTRYTYPE = cOUNTRYTYPE;
	}
	public String getTRADEDATE(){
		return TRADEDATE;
	}
	public void setTRADEDATE(String tRADEDATE){
		TRADEDATE = tRADEDATE;
	}
	public String getAMT3(){
		return AMT3;
	}
	public void setAMT3(String aMT3){
		AMT3 = aMT3;
	}
	public String getFCA2(){
		return FCA2;
	}
	public void setFCA2(String fCA2){
		FCA2 = fCA2;
	}
	public String getAMT5(){
		return AMT5;
	}
	public void setAMT5(String aMT5){
		AMT5 = aMT5;
	}
	public String getOUTACN(){
		return OUTACN;
	}
	public void setOUTACN(String oUTACN){
		OUTACN = oUTACN;
	}
	public String getINTSACN(){
		return INTSACN;
	}
	public void setINTSACN(String iNTSACN){
		INTSACN = iNTSACN;
	}
	public String getBILLSENDMODE(){
		return BILLSENDMODE;
	}
	public void setBILLSENDMODE(String bILLSENDMODE){
		BILLSENDMODE = bILLSENDMODE;
	}
	public String getFCAFEE(){
		return FCAFEE;
	}
	public void setFCAFEE(String fCAFEE){
		FCAFEE = fCAFEE;
	}
	public String getSSLTXNO(){
		return SSLTXNO;
	}
	public void setSSLTXNO(String sSLTXNO){
		SSLTXNO = sSLTXNO;
	}
	public String getPAYDAY1(){
		return PAYDAY1;
	}
	public void setPAYDAY1(String pAYDAY1){
		PAYDAY1 = pAYDAY1;
	}
	public String getPAYDAY2(){
		return PAYDAY2;
	}
	public void setPAYDAY2(String pAYDAY2){
		PAYDAY2 = pAYDAY2;
	}
	public String getPAYDAY3(){
		return PAYDAY3;
	}
	public void setPAYDAY3(String pAYDAY3){
		PAYDAY3 = pAYDAY3;
	}
	public String getBRHCOD(){
		return BRHCOD;
	}
	public void setBRHCOD(String bRHCOD){
		BRHCOD = bRHCOD;
	}
	public String getCUTTYPE(){
		return CUTTYPE;
	}
	public void setCUTTYPE(String cUTTYPE){
		CUTTYPE = cUTTYPE;
	}
	public String getCRY1(){
		return CRY1;
	}
	public void setCRY1(String cRY1){
		CRY1 = cRY1;
	}
	public String getSTOP(){
		return STOP;
	}
	public void setSTOP(String sTOP){
		STOP = sTOP;
	}
	public String getYIELD(){
		return YIELD;
	}
	public void setYIELD(String yIELD){
		YIELD = yIELD;
	}
	public String getKYCDATE(){
		return KYCDATE;
	}
	public void setKYCDATE(String kYCDATE){
		KYCDATE = kYCDATE;
	}
	public String getWEAK(){
		return WEAK;
	}
	public void setWEAK(String wEAK){
		WEAK = wEAK;
	}
	public String getRSKATT(){
		return RSKATT;
	}
	public void setRSKATT(String rSKATT){
		RSKATT = rSKATT;
	}
	public String getGETLTD(){
		return GETLTD;
	}
	public void setGETLTD(String gETLTD){
		GETLTD = gETLTD;
	}
	public String getRISK7(){
		return RISK7;
	}
	public void setRISK7(String rISK7){
		RISK7 = rISK7;
	}
	public String getGETLTD7(){
		return GETLTD7;
	}
	public void setGETLTD7(String gETLTD7){
		GETLTD7 = gETLTD7;
	}
	public String getRRSK(){
		return RRSK;
	}
	public void setRRSK(String rRSK){
		RRSK = rRSK;
	}
	public String getFDAGREEFLAG(){
		return FDAGREEFLAG;
	}
	public void setFDAGREEFLAG(String fDAGREEFLAG){
		FDAGREEFLAG = fDAGREEFLAG;
	}
	public String getFDNOTICETYPE(){
		return FDNOTICETYPE;
	}
	public void setFDNOTICETYPE(String fDNOTICETYPE){
		FDNOTICETYPE = fDNOTICETYPE;
	}
	public String getFDPUBLICTYPE(){
		return FDPUBLICTYPE;
	}
	public void setFDPUBLICTYPE(String fDPUBLICTYPE){
		FDPUBLICTYPE = fDPUBLICTYPE;
	}
	public String getDPMYEMAIL(){
		return DPMYEMAIL;
	}
	public void setDPMYEMAIL(String dPMYEMAIL){
		DPMYEMAIL = dPMYEMAIL;
	}
	public String getCMTRMAIL(){
		return CMTRMAIL;
	}
	public void setCMTRMAIL(String cMTRMAIL){
		CMTRMAIL = cMTRMAIL;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getSLSNO() {
		return SLSNO;
	}
	public void setSLSNO(String sLSNO) {
		SLSNO = sLSNO;
	}
	public String getOFLAG() {
		return OFLAG;
	}
	public void setOFLAG(String oFLAG) {
		OFLAG = oFLAG;
	}
}