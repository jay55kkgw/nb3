package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

public class A1000_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -451029782160434620L;

	private String code;			// 訊息代碼
	private String message;			// 訊息內容
	private String tid;				// 交易識別序號
	private String system;			// TSIP機器代號
	private String processorName;	// API閘道名稱
	private String Cust_ID;			// 客戶統編
	private String DW_Data_Dt;		// 入倉資料日期--西元年月日YYYY-MM-DD
	private String Sv_Bal_Amt;		// 臺幣存款餘額
	private String Syv_Bal_Amt;		// 外幣存款餘額折台幣
	private String Gd_Bal_Amt;		// 黃金存摺餘額折台幣--單位公克
	private String Ln_Bal_Amt;		// 臺幣放款餘額
	private String Fl_Bal_Amt;		// 外幣放款餘額折台幣
	private String Ic_Bal_Amt;		// 信用卡餘額
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getTid() {
		return tid;
	}
	public void setTid(String tid) {
		this.tid = tid;
	}
	public String getSystem() {
		return system;
	}
	public void setSystem(String system) {
		this.system = system;
	}
	public String getProcessorName() {
		return processorName;
	}
	public void setProcessorName(String processorName) {
		this.processorName = processorName;
	}
	public String getCust_ID() {
		return Cust_ID;
	}
	public void setCust_ID(String cust_ID) {
		Cust_ID = cust_ID;
	}
	public String getDW_Data_Dt() {
		return DW_Data_Dt;
	}
	public void setDW_Data_Dt(String dW_Data_Dt) {
		DW_Data_Dt = dW_Data_Dt;
	}
	public String getSv_Bal_Amt() {
		return Sv_Bal_Amt;
	}
	public void setSv_Bal_Amt(String sv_Bal_Amt) {
		Sv_Bal_Amt = sv_Bal_Amt;
	}
	public String getSyv_Bal_Amt() {
		return Syv_Bal_Amt;
	}
	public void setSyv_Bal_Amt(String syv_Bal_Amt) {
		Syv_Bal_Amt = syv_Bal_Amt;
	}
	public String getGd_Bal_Amt() {
		return Gd_Bal_Amt;
	}
	public void setGd_Bal_Amt(String gd_Bal_Amt) {
		Gd_Bal_Amt = gd_Bal_Amt;
	}
	public String getLn_Bal_Amt() {
		return Ln_Bal_Amt;
	}
	public void setLn_Bal_Amt(String ln_Bal_Amt) {
		Ln_Bal_Amt = ln_Bal_Amt;
	}
	public String getFl_Bal_Amt() {
		return Fl_Bal_Amt;
	}
	public void setFl_Bal_Amt(String fl_Bal_Amt) {
		Fl_Bal_Amt = fl_Bal_Amt;
	}
	public String getIc_Bal_Amt() {
		return Ic_Bal_Amt;
	}
	public void setIc_Bal_Amt(String ic_Bal_Amt) {
		Ic_Bal_Amt = ic_Bal_Amt;
	}
	
	
}
