package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N361_REST_RQ extends BaseRestBean_OLS implements Serializable {
	
	private static final long serialVersionUID = 7888860285947846963L;
	
	// 共用
	private String CUSIDN;		// 身份證號
	private String DATA;
	private String FGTXWAY;		// 交易機制
	private String PINNEW;
	private String RTC;
	// N322電文會用到
	private String TYPE;
	private String UPD_FLG;
	private String DEGREE;
	private String CAREER;
	private String SALARY;
	private String EDITON;
	private String ANSWER;
	private String FDMARK1;
	private String KIND;		// 交易機制 與N361共用
	private String BNKRA;
	private String XMLCA;
	private String XMLCN;
	private String FDQ1;
	private String FDQ2;
	private String FDQ3;
	private String FDQ4;
	private String FDQ5;
	private String FDQ6;
	private String FDQ7;
	private String FDQ8;
	private String FDQ9;
	private String FDQ10;
	private String FDQ11;
	private String FDQ12;
	private String FDQ13;
	private String FDQ14;
	private String FDQ15;
	private String FDSCORE;
	private String FDINVTYPE;
	
	// N361電文使用的欄位
	private String ACN_SSV;				// 台幣約轉帳號
	private String ACN_FUD;				// 外幣約轉帳號
	private String ICCOD;				// 信用卡授權扣帳
	private String CTDNUM;				// 推薦行員
	private String BILLMTH;				// 對帳單交付方式
	private String AMLCOD;			// AML告警
	
	// N930電文使用的欄位
	private String DPMYEMAIL;			// 電子郵箱
	
	// 交易機制相關欄位
	private String iSeqNo;				// iSeqNo
	private String ISSUER;
	private String ACNNO;				// N322 晶片卡帳號
	private String CHIP_ACN;			// N361 晶片卡帳號
	private String TRMID;
	private String ICSEQ;				// 晶片卡序號
	private String ICSEQ1;				// 晶片卡序號1
	private String TAC;
	private String pkcs7Sign;			// IKEY
	private String jsondc;				// IKEY
	
	// 電文沒使用但 ms_tw 需要的資料
	private String trancode = "N361";	// MAC2.java 押碼用
	
	private String AGREE;
	private String ADUSERIP;
	private String PCMAC;
	private String DEVNAME;
	private String FDHISTID;
	
	//FATCA及CRS個人客戶自我聲明的出生地縣市
	private String CITY;
	
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}

	public String getDATA() {
		return DATA;
	}

	public void setDATA(String dATA) {
		DATA = dATA;
	}

	public String getFGTXWAY() {
		return FGTXWAY;
	}

	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}

	public String getPINNEW() {
		return PINNEW;
	}

	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}

	public String getRTC() {
		return RTC;
	}

	public void setRTC(String rTC) {
		RTC = rTC;
	}

	public String getTYPE() {
		return TYPE;
	}

	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}

	public String getUPD_FLG() {
		return UPD_FLG;
	}

	public void setUPD_FLG(String uPD_FLG) {
		UPD_FLG = uPD_FLG;
	}

	public String getDEGREE() {
		return DEGREE;
	}

	public void setDEGREE(String dEGREE) {
		DEGREE = dEGREE;
	}

	public String getCAREER() {
		return CAREER;
	}

	public void setCAREER(String cAREER) {
		CAREER = cAREER;
	}

	public String getSALARY() {
		return SALARY;
	}

	public void setSALARY(String sALARY) {
		SALARY = sALARY;
	}

	public String getEDITON() {
		return EDITON;
	}

	public void setEDITON(String eDITON) {
		EDITON = eDITON;
	}

	public String getANSWER() {
		return ANSWER;
	}

	public void setANSWER(String aNSWER) {
		ANSWER = aNSWER;
	}

	public String getFDMARK1() {
		return FDMARK1;
	}

	public void setFDMARK1(String fDMARK1) {
		FDMARK1 = fDMARK1;
	}

	public String getKIND() {
		return KIND;
	}

	public void setKIND(String kIND) {
		KIND = kIND;
	}

	public String getBNKRA() {
		return BNKRA;
	}

	public void setBNKRA(String bNKRA) {
		BNKRA = bNKRA;
	}

	public String getXMLCA() {
		return XMLCA;
	}

	public void setXMLCA(String xMLCA) {
		XMLCA = xMLCA;
	}

	public String getXMLCN() {
		return XMLCN;
	}

	public void setXMLCN(String xMLCN) {
		XMLCN = xMLCN;
	}

	public String getFDQ1() {
		return FDQ1;
	}

	public void setFDQ1(String fDQ1) {
		FDQ1 = fDQ1;
	}

	public String getFDQ2() {
		return FDQ2;
	}

	public void setFDQ2(String fDQ2) {
		FDQ2 = fDQ2;
	}

	public String getFDQ3() {
		return FDQ3;
	}

	public void setFDQ3(String fDQ3) {
		FDQ3 = fDQ3;
	}

	public String getFDQ4() {
		return FDQ4;
	}

	public void setFDQ4(String fDQ4) {
		FDQ4 = fDQ4;
	}

	public String getFDQ5() {
		return FDQ5;
	}

	public void setFDQ5(String fDQ5) {
		FDQ5 = fDQ5;
	}

	public String getFDQ6() {
		return FDQ6;
	}

	public void setFDQ6(String fDQ6) {
		FDQ6 = fDQ6;
	}

	public String getFDQ7() {
		return FDQ7;
	}

	public void setFDQ7(String fDQ7) {
		FDQ7 = fDQ7;
	}

	public String getFDQ8() {
		return FDQ8;
	}

	public void setFDQ8(String fDQ8) {
		FDQ8 = fDQ8;
	}

	public String getFDQ9() {
		return FDQ9;
	}

	public void setFDQ9(String fDQ9) {
		FDQ9 = fDQ9;
	}

	public String getFDQ10() {
		return FDQ10;
	}

	public void setFDQ10(String fDQ10) {
		FDQ10 = fDQ10;
	}

	public String getFDQ11() {
		return FDQ11;
	}

	public void setFDQ11(String fDQ11) {
		FDQ11 = fDQ11;
	}

	public String getFDQ12() {
		return FDQ12;
	}

	public void setFDQ12(String fDQ12) {
		FDQ12 = fDQ12;
	}

	public String getFDQ13() {
		return FDQ13;
	}

	public void setFDQ13(String fDQ13) {
		FDQ13 = fDQ13;
	}

	public String getFDQ14() {
		return FDQ14;
	}

	public void setFDQ14(String fDQ14) {
		FDQ14 = fDQ14;
	}

	public String getFDQ15() {
		return FDQ15;
	}

	public void setFDQ15(String fDQ15) {
		FDQ15 = fDQ15;
	}

	public String getFDSCORE() {
		return FDSCORE;
	}

	public void setFDSCORE(String fDSCORE) {
		FDSCORE = fDSCORE;
	}

	public String getFDINVTYPE() {
		return FDINVTYPE;
	}

	public void setFDINVTYPE(String fDINVTYPE) {
		FDINVTYPE = fDINVTYPE;
	}

	public String getACN_SSV() {
		return ACN_SSV;
	}
	
	public String getACN_SSV_F() 
	{
		if("00000000000".equals(ACN_SSV))
		{
			return "無帳號";
		}
		return ACN_SSV;
	}

	public void setACN_SSV(String aCN_SSV) {
		ACN_SSV = aCN_SSV;
	}

	public String getACN_FUD() {
		return ACN_FUD;
	}
	
	public String getACN_FUD_F() 
	{
		if("00000000000".equals(ACN_FUD))
		{
			return "無帳號";
		}
		return ACN_FUD;
	}

	public void setACN_FUD(String aCN_FUD) {
		ACN_FUD = aCN_FUD;
	}

	public String getICCOD() {
		return ICCOD;
	}

	public void setICCOD(String iCCOD) {
		ICCOD = iCCOD;
	}

	public String getCTDNUM() {
		return CTDNUM;
	}

	public void setCTDNUM(String cTDNUM) {
		CTDNUM = cTDNUM;
	}

	public String getBILLMTH() {
		return BILLMTH;
	}

	public void setBILLMTH(String bILLMTH) {
		BILLMTH = bILLMTH;
	}

	public String getAMLCOD() {
		return AMLCOD;
	}

	public void setAMLCOD(String aMLCOD) {
		AMLCOD = aMLCOD;
	}

	public String getDPMYEMAIL() {
		return DPMYEMAIL;
	}

	public void setDPMYEMAIL(String dPMYEMAIL) {
		DPMYEMAIL = dPMYEMAIL;
	}

	public String getiSeqNo() {
		return iSeqNo;
	}

	public void setiSeqNo(String iSeqNo) {
		this.iSeqNo = iSeqNo;
	}

	public String getISSUER() {
		return ISSUER;
	}

	public void setISSUER(String iSSUER) {
		ISSUER = iSSUER;
	}

	public String getACNNO() {
		return ACNNO;
	}

	public void setACNNO(String aCNNO) {
		ACNNO = aCNNO;
	}
	
	public String getCHIP_ACN() {
		return CHIP_ACN;
	}

	public void setCHIP_ACN(String cHIP_ACN) {
		CHIP_ACN = cHIP_ACN;
	}

	public String getTRMID() {
		return TRMID;
	}

	public void setTRMID(String tRMID) {
		TRMID = tRMID;
	}

	public String getICSEQ() {
		return ICSEQ;
	}

	public void setICSEQ(String iCSEQ) {
		ICSEQ = iCSEQ;
	}

	public String getICSEQ1() {
		return ICSEQ1;
	}

	public void setICSEQ1(String iCSEQ1) {
		ICSEQ1 = iCSEQ1;
	}
	
	public String getTAC() {
		return TAC;
	}

	public void setTAC(String tAC) {
		TAC = tAC;
	}

	public String getPkcs7Sign() {
		return pkcs7Sign;
	}

	public void setPkcs7Sign(String pkcs7Sign) {
		this.pkcs7Sign = pkcs7Sign;
	}

	public String getJsondc() {
		return jsondc;
	}

	public void setJsondc(String jsondc) {
		this.jsondc = jsondc;
	}

	public String getTrancode() {
		return trancode;
	}

	public void setTrancode(String trancode) {
		this.trancode = trancode;
	}

	public String getAGREE() {
		return AGREE;
	}

	public void setAGREE(String aGREE) {
		AGREE = aGREE;
	}

	public String getADUSERIP() {
		return ADUSERIP;
	}

	public void setADUSERIP(String aDUSERIP) {
		ADUSERIP = aDUSERIP;
	}

	public String getPCMAC() {
		return PCMAC;
	}

	public void setPCMAC(String pCMAC) {
		PCMAC = pCMAC;
	}

	public String getDEVNAME() {
		return DEVNAME;
	}

	public void setDEVNAME(String dEVNAME) {
		DEVNAME = dEVNAME;
	}

	public String getFDHISTID() {
		return FDHISTID;
	}

	public void setFDHISTID(String fDHISTID) {
		FDHISTID = fDHISTID;
	}

	public String getCITY() {
		return CITY;
	}

	public void setCITY(String cITY) {
		CITY = cITY;
	}
}