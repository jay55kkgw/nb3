package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N421_REST_RQ extends BaseRestBean_TW implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8561989741852937624L;
	
	private String CUSIDN;
	
	private String OKOVNEXT;

	public String getCUSIDN()
	{
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN)
	{
		CUSIDN = cUSIDN;
	}

	public String getOKOVNEXT() {
		return OKOVNEXT;
	}

	public void setOKOVNEXT(String oKOVNEXT) {
		OKOVNEXT = oKOVNEXT;
	}
}
