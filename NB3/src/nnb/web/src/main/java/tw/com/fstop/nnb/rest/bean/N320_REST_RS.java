package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N320_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1874898449756769229L;
	
	
	
	//臺幣相關
	
	private String TOPMSG_320; //判斷表格顯示條件1 "OKLR"&&"OKOV" 正常 其餘錯誤代碼:異常
	private String MSGFLG_320; //判斷表格顯示條件2 00:正常 01:異常
	private String USERDATA_X50_320; //電文回打參數
	private String QUERYNEXT_320; //判斷是否需要回打
	private String TOTALBAL_320; //表格內臺幣總金額
	
	//外幣相關
	private String TOPMSG_552; //判斷表格顯示條件 "OKLR"&&"OKOV" 正常 其餘錯誤代碼:異常
	private String QUERYNEXT_552;//判斷是否需要回打
	private String USERDATA_X50_552;//電文回打參數
	
	private String N320GO;
	private String N552GO;
	private String CMRECNUM;
	
	//表格相關
	private LinkedList<N320_REST_RSDATA_TW> TW; //臺幣借款表格
	private LinkedList<N320_REST_RSDATA_FX> FX; //外幣借款表格
	private LinkedList<N320_REST_RSDATA_CRY> CRY; //幣別及金額表格
	
	
	public String getMSGFLG_320() {
		return MSGFLG_320;
	}
	public void setMSGFLG_320(String mSGFLG_320) {
		MSGFLG_320 = mSGFLG_320;
	}
	public String getTOPMSG_320() {
		return TOPMSG_320;
	}
	public void setTOPMSG_320(String tOPMSG_320) {
		TOPMSG_320 = tOPMSG_320;
	}
	public String getUSERDATA_X50_320() {
		return USERDATA_X50_320;
	}
	public void setUSERDATA_X50_320(String uSERDATA_X50_320) {
		USERDATA_X50_320 = uSERDATA_X50_320;
	}
	public String getQUERYNEXT_320() {
		return QUERYNEXT_320;
	}
	public void setQUERYNEXT_320(String qUERYNEXT_320) {
		QUERYNEXT_320 = qUERYNEXT_320;
	}
	public String getTOTALBAL_320() {
		return TOTALBAL_320;
	}
	public void setTOTALBAL_320(String tOTALBAL_320) {
		TOTALBAL_320 = tOTALBAL_320;
	}
	public String getTOPMSG_552() {
		return TOPMSG_552;
	}
	public void setTOPMSG_552(String tOPMSG_552) {
		TOPMSG_552 = tOPMSG_552;
	}
	public String getQUERYNEXT_552() {
		return QUERYNEXT_552;
	}
	public void setQUERYNEXT_552(String qUERYNEXT_552) {
		QUERYNEXT_552 = qUERYNEXT_552;
	}
	public String getUSERDATA_X50_552() {
		return USERDATA_X50_552;
	}
	public void setUSERDATA_X50_552(String uSERDATA_X50_552) {
		USERDATA_X50_552 = uSERDATA_X50_552;
	}
	public LinkedList<N320_REST_RSDATA_TW> getTW() {
		return TW;
	}
	public void setTW(LinkedList<N320_REST_RSDATA_TW> tW) {
		TW = tW;
	}
	public LinkedList<N320_REST_RSDATA_FX> getFX() {
		return FX;
	}
	public void setFX(LinkedList<N320_REST_RSDATA_FX> fX) {
		FX = fX;
	}
	public LinkedList<N320_REST_RSDATA_CRY> getCRY() {
		return CRY;
	}
	public void setCRY(LinkedList<N320_REST_RSDATA_CRY> cRY) {
		CRY = cRY;
	}
	public String getN320GO() {
		return N320GO;
	}
	public void setN320GO(String n320go) {
		N320GO = n320go;
	}
	public String getN552GO() {
		return N552GO;
	}
	public void setN552GO(String n552go) {
		N552GO = n552go;
	}
	public String getCMRECNUM() {
		return CMRECNUM;
	}
	public void setCMRECNUM(String cMRECNUM) {
		CMRECNUM = cMRECNUM;
	}
	
	
	
}
