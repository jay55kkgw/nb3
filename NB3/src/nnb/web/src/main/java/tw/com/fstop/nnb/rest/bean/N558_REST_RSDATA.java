package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;
/**
 * 
 * @author fstop
 *
 */
public class N558_REST_RSDATA implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8531484768832680416L;
	private String RLCAMT;
	private String RADVFIXR;
	private String RINTSTDT;
	private String RIBNO;
	private String RIBDATE;
	private String RLCNO;
	private String RBENNAME;
	private String RBILLAMT;
	private String RCFMDATE;
	private String RMARK;
	private String RINTDUDT;
	private String RBILLCCY;
	public String getRLCAMT() {
		return RLCAMT;
	}
	public void setRLCAMT(String rLCAMT) {
		RLCAMT = rLCAMT;
	}
	public String getRADVFIXR() {
		return RADVFIXR;
	}
	public void setRADVFIXR(String rADVFIXR) {
		RADVFIXR = rADVFIXR;
	}
	public String getRINTSTDT() {
		return RINTSTDT;
	}
	public void setRINTSTDT(String rINTSTDT) {
		RINTSTDT = rINTSTDT;
	}
	public String getRIBNO() {
		return RIBNO;
	}
	public void setRIBNO(String rIBNO) {
		RIBNO = rIBNO;
	}
	public String getRIBDATE() {
		return RIBDATE;
	}
	public void setRIBDATE(String rIBDATE) {
		RIBDATE = rIBDATE;
	}
	public String getRLCNO() {
		return RLCNO;
	}
	public void setRLCNO(String rLCNO) {
		RLCNO = rLCNO;
	}
	public String getRBENNAME() {
		return RBENNAME;
	}
	public void setRBENNAME(String rBENNAME) {
		RBENNAME = rBENNAME;
	}
	public String getRBILLAMT() {
		return RBILLAMT;
	}
	public void setRBILLAMT(String rBILLAMT) {
		RBILLAMT = rBILLAMT;
	}
	public String getRCFMDATE() {
		return RCFMDATE;
	}
	public void setRCFMDATE(String rCFMDATE) {
		RCFMDATE = rCFMDATE;
	}
	public String getRMARK() {
		return RMARK;
	}
	public void setRMARK(String rMARK) {
		RMARK = rMARK;
	}
	public String getRINTDUDT() {
		return RINTDUDT;
	}
	public void setRINTDUDT(String rINTDUDT) {
		RINTDUDT = rINTDUDT;
	}
	public String getRBILLCCY() {
		return RBILLCCY;
	}
	public void setRBILLCCY(String rBILLCCY) {
		RBILLCCY = rBILLCCY;
	}
	

}