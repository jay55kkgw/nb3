package tw.com.fstop.nnb.service;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import fstop.orm.po.ADMHOLIDAY;
import fstop.orm.po.SYSPARAMDATA;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.SysParamDataDao;
import tw.com.fstop.tbb.nnb.dao.AdmHolidayDao;
import tw.com.fstop.tbb.nnb.util.StrUtils;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.SessionUtil;


@Service
public class Gold_Transaction_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private SysParamDataDao sysParamDataDao;
	@Autowired
	private AdmHolidayDao admHolidayDao;
	@Autowired
	private Fund_Transfer_Service fund_transfer_service;
	@Autowired
	I18n i18n;
	
	public BaseResult pay_fail_fee(String cusidn, Map<String, String> reqParam) {
		log.trace("pay_fail_fee start");
		BaseResult bs = new BaseResult();
		BaseResult bsN920 = new BaseResult();
		BaseResult bsN926 = new BaseResult();
		try {
			bsN920 = N920_REST(cusidn, GOLD_RESERVATION_QOERY_N920);
			bsN926 = N926_REST(cusidn, GOLD_RESERVATION_QOERY);
			
			if(bsN920!=null && bsN920.getResult() && bsN926!=null && bsN926.getResult()) {
				Map<String , Object> callDataN920 = (Map) bsN920.getData();
				ArrayList<Map<String , String>> callTableN920 = (ArrayList<Map<String , String>>) callDataN920.get("REC");
				Map<String , Object> callDataN926 = (Map) bsN926.getData();
				ArrayList<Map<String , String>> callTableN926 = (ArrayList<Map<String , String>>) callDataN926.get("REC");
				

				for(int i = 0;i < callTableN926.size();i++) {
					if(callTableN926.get(i).get("ACN").length() != 11) {
						callTableN926.remove(i);
						i--;
					}
				}
				
				if(callTableN920.size() > 0) {
					if(callTableN926.size() <= 0){
						//您尚未申請黃金存摺網路交易，請洽往來分行辦理或線上立即申請本功能
						bsN920.addData("MsgDesc", i18n.getMsg("LB.X1776"));
						bsN920.addData("TxType", "NA60");
						bsN920.addData("Trancode", "NA60");
						bsN920.addData("ToGoFlag", "0");
						bsN920.setSYSMessage("0");
						bsN920.setResult(Boolean.TRUE);
					}else {
						bsN920.addData("ToGoFlag", "1");
					}
				}else {
					//您尚未申請黃金存摺帳戶，請洽往來分行辦理或線上立即申請。
					bsN920.addData("MsgDesc", i18n.getMsg("LB.X1772"));
					bsN920.addData("TxType", "NA50");
					bsN920.addData("Trancode", "NA50");
					bsN920.addData("ToGoFlag", "0");
					bsN920.setSYSMessage("0");
					bsN920.setResult(Boolean.TRUE);
					
				}
			}
		}catch(Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("pay_fail_fee error >> {}",e);
		}	
		return  bsN920;
	}
	
	public BaseResult pay_fail_fee_confirm(String cusidn, Map<String, String> reqParam) {
		log.trace("pay_fail_fee_confirm start");
		BaseResult bs = new BaseResult();
		try {
			bs = N094_REST(cusidn, reqParam, "N094_C");
			if(bs!=null && bs.getResult()) {
				Map<String , Object> callData = (Map) bs.getData();
				ArrayList<Map<String , String>> callTable = (ArrayList<Map<String , String>>) callData.get("REC");
				// 移除空入帳帳號
				for (int i = 0; i < callTable.size(); i++) {
					log.debug("RBANC >> {}", callTable.get(i).get("TSFACN"));
					if (callTable.get(i).get("TSFACN").equals("")) {
						callTable.remove(i);
						i--;
					}
				}
				for(Map<String , String> callRow:callTable) {
					callRow.put("QTAMT", NumericUtil.fmtAmount(callRow.get("QTAMT"), 0));
					callRow.put("FEEAMT", NumericUtil.fmtAmount(callRow.get("FEEAMT"), 0));
				}
				callData.put("TOKEN", reqParam.get("TOKEN"));
			}
		}catch(Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("pay_fail_fee_confirm error >> {}",e);
		}	
		return  bs;
	}
	
	public BaseResult pay_fail_fee_result(String cusidn, Map<String, String> reqParam) {
		log.trace("pay_fail_fee_result start");
		BaseResult bs = new BaseResult();
		try {
			bs = N094_REST(cusidn, reqParam, "N094");
			if(bs!=null && bs.getResult()) {
				Map<String , Object> callData = (Map) bs.getData();
				callData.put("TRNAMT", NumericUtil.fmtAmount((String)callData.get("TRNAMT"), 0));
				callData.put("O_TOTBAL", NumericUtil.fmtAmount((String)callData.get("O_TOTBAL"), 2));
				callData.put("O_AVLBAL", NumericUtil.fmtAmount((String)callData.get("O_AVLBAL"), 2));
			}
		}catch(Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("pay_fail_fee_result error >> {}",e);
		}	
		return  bs;
	}
	
	/**
	 * 取得黃金帳號
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	public List<Map<String, String>> getGoldAcn(String acn, String cusidn) {
		log.trace(ESAPIUtil.vaildLog("getGoldAcn...>>>" + acn));
		
		List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();
		BaseResult bs = new BaseResult();
		try {
			bs = N926_REST(cusidn, GOLD_RESERVATION_QOERY);
			if(bs!=null && bs.getResult()) {
				Map<String , Object> callDataN926 = (Map) bs.getData();
				ArrayList<Map<String , String>> callTableN926 = (ArrayList<Map<String , String>>) callDataN926.get("REC");
				log.trace("callTableN926.size>>>{}",callTableN926.size());
				//將電文回傳的帳號與頁面選取的帳號做比對,若不相同就移除
				for (int i = 0;i < callTableN926.size();i++) {
					if(!callTableN926.get(i).get("SVACN").equals(acn)) 
					{
						callTableN926.remove(i);
						i--;
					}
				}
				log.trace("callTableN926 data>>>{}",callTableN926);
				resultList = callTableN926;
			}
		} 
		catch (Exception e) {
			log.error("getGoldAcn.error: {}", e);
		}
		
		log.debug("resultList: {}", resultList);
		
		return resultList;
	}
	
	/**
	 * 檢核KYC
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	public BaseResult checkKYC(String cusidn, Map<String, String> reqParam, String kyc,Model model) {
		log.trace("checkKYC...>>>" + kyc);
		
		BaseResult bs = null;
		
		try {
			bs = new BaseResult();
			bs = N927_REST(cusidn);
			if(bs!=null && bs.getResult()) {
				Map<String,Object> callTable = (Map<String,Object>)bs.getData();
				String str_RISK = (String)callTable.get("RISK");   //投資屬性
				String GETLTD = (String)callTable.get("GETLTD");  //KYC問卷填寫日期
				String time1="";
				String time2="";
				int day = 0;
				if(!GETLTD.equals("")) {
					time1 = DateUtil.getDate("/");
					time2 = DateUtil.convertDate(1, GETLTD, "yyyMMdd", "yyyy/MM/dd");
					day = DateUtil.getDiffDate(time1,time2,"yyyy/MM/dd");
				}
				Hashtable b106Params = new Hashtable();
			    b106Params.put("TABLE","SysParamData");
			    b106Params.put("ADPK","NBSYS");
			    bs.reset();
			    bs = B106_REST(cusidn, b106Params);
			    callTable = (Map<String,Object>)bs.getData();
			    String KYCDATE = (String)callTable.get("KYCDATE") == null?"":(String)callTable.get("KYCDATE");
			    SessionUtil.addAttribute(model,SessionUtil.KYCDATE,KYCDATE);
			    
			    log.debug("@@@ UID/RISK="+cusidn+"/"+str_RISK+"/GETLTD="+GETLTD+"/time1="+time1+"/time2="+time2+"/DAY="+day+"/KYCDATE="+KYCDATE);
			    bs.reset();
				bs.addData("Gd_InvAttr", "0");
				if(StrUtils.isEmpty(kyc)) {   //因KYC提醒後，判斷是否繼續交易，有PASS就不會進來這
					if(str_RISK.equals("") || (!GETLTD.equals("") && day>=365) || (!KYCDATE.equals("") && Integer.parseInt(KYCDATE)>Integer.parseInt(GETLTD))){
						bs.addData("TXID", "NA50");
						if(day>=365) {   //滿一年
							bs.addData("ALERTTYPE","2");
						}else if(str_RISK.equals("")) {   //無投資屬性
							bs.addData("ALERTTYPE","3");
						}
						bs.addData("Gd_InvAttr", "1");
						bs.setSYSMessage("OKLR");
					}
				}
				bs.setResult(true);
			}
		} 
		catch (Exception e) {
			log.error("checkKYC.error: {}", e);
		}
		return bs;
	}
	/**
	 * N09002 黃金即時回售確認頁
	 */
	@SuppressWarnings("unchecked")
	public BaseResult gold_resale_confirm(Map<String,String> requestParam,Model model){
		BaseResult bs = null;
		try{
			bs = new BaseResult();
			bs = N090_02_REST(requestParam);
			if(bs != null && bs.getResult()){
				Map<String,Object> bsData = (Map<String,Object>)bs.getData();
				log.debug("bsData={}",bsData);
				
				String TRNGD = (String)bsData.get("TRNGD");
				log.debug("TRNGD={}",TRNGD);
				String TRNGDFormat = fund_transfer_service.formatNumberString(TRNGD,2);
				log.debug("TRNGDFormat={}",TRNGDFormat);
				bsData.put("TRNGDFormat",TRNGDFormat);
				
				String PRICE = (String)bsData.get("PRICE");
				log.debug("PRICE={}",PRICE);
				String PRICEFormat = fund_transfer_service.formatNumberString(PRICE,2);
				log.debug("PRICEFormat={}",PRICEFormat);
				bsData.put("PRICEFormat",PRICEFormat);
				
				String TRNAMT = (String)bsData.get("TRNAMT");
				log.debug("TRNAMT={}",TRNAMT);
				String TRNAMTFormat = fund_transfer_service.formatNumberString(TRNAMT,0);
				log.debug("TRNAMTFormat={}",TRNAMTFormat);
				bsData.put("TRNAMTFormat",TRNAMTFormat);
				
				String FEEAMT1 = (String)bsData.get("FEEAMT1");
				log.debug("FEEAMT1={}",FEEAMT1);
				String FEEAMT1Format = fund_transfer_service.formatNumberString(FEEAMT1,0);
				log.debug("FEEAMT1Format={}",FEEAMT1Format);
				bsData.put("FEEAMT1Format",FEEAMT1Format);
				
				String FEEAMT2 = (String)bsData.get("FEEAMT2");
				log.debug("FEEAMT2={}",FEEAMT2);
				String FEEAMT2Format = fund_transfer_service.formatNumberString(FEEAMT2,0);
				log.debug("FEEAMT2Format={}",FEEAMT2Format);
				bsData.put("FEEAMT2Format",FEEAMT2Format);
				
				String TRNAMT2 = (String)bsData.get("TRNAMT2");
				log.debug("TRNAMT2={}",TRNAMT2);
				String TRNAMT2Format = fund_transfer_service.formatNumberString(TRNAMT2,0);
				log.debug("TRNAMT2Format={}",TRNAMT2Format);
				bsData.put("TRNAMT2Format",TRNAMT2Format);
				
				model.addAttribute("bsData",bsData);
				
				//IKEY要使用的JSON:DC
			 	String jsondc = URLEncoder.encode(CodeUtil.toJson(requestParam),"UTF-8");
			 	log.debug(ESAPIUtil.vaildLog("jsondc={}"+jsondc));
			 	model.addAttribute("jsondc",jsondc);
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_resale_confirm error >> {}",e);
		}
		return bs;
	}
	/**
	 * N09002黃金即時回售結果頁
	 */
	@SuppressWarnings("unchecked")
	public BaseResult gold_resale_result(Map<String,String> requestParam,Model model){
		BaseResult bs = null;
		try{
			bs = new BaseResult();
			bs = N090_02_REST(requestParam);
			if(bs != null && bs.getResult()){
				Map<String,Object> bsData = (Map<String,Object>)bs.getData();
				log.debug("bsData={}",bsData);
				
				String TRNGD = (String)bsData.get("TRNGD");
				log.debug("TRNGD={}",TRNGD);
				String TRNGDFormat = fund_transfer_service.formatNumberString(TRNGD,2);
				log.debug("TRNGDFormat={}",TRNGDFormat);
				bsData.put("TRNGDFormat",TRNGDFormat);
				
				String PRICE = (String)bsData.get("PRICE");
				log.debug("PRICE={}",PRICE);
				String PRICEFormat = fund_transfer_service.formatNumberString(PRICE,2);
				log.debug("PRICEFormat={}",PRICEFormat);
				bsData.put("PRICEFormat",PRICEFormat);
				
				String TRNAMT = (String)bsData.get("TRNAMT");
				log.debug("TRNAMT={}",TRNAMT);
				String TRNAMTFormat = fund_transfer_service.formatNumberString(TRNAMT,0);
				log.debug("TRNAMTFormat={}",TRNAMTFormat);
				bsData.put("TRNAMTFormat",TRNAMTFormat);
				
				String FEEAMT1 = (String)bsData.get("FEEAMT1");
				log.debug("FEEAMT1={}",FEEAMT1);
				String FEEAMT1Format = fund_transfer_service.formatNumberString(FEEAMT1,0);
				log.debug("FEEAMT1Format={}",FEEAMT1Format);
				bsData.put("FEEAMT1Format",FEEAMT1Format);
				
				String FEEAMT2 = (String)bsData.get("FEEAMT2");
				log.debug("FEEAMT2={}",FEEAMT2);
				String FEEAMT2Format = fund_transfer_service.formatNumberString(FEEAMT2,0);
				log.debug("FEEAMT2Format={}",FEEAMT2Format);
				bsData.put("FEEAMT2Format",FEEAMT2Format);
				
				String TRNAMT2 = (String)bsData.get("TRNAMT2");
				log.debug("TRNAMT2={}",TRNAMT2);
				String TRNAMT2Format = fund_transfer_service.formatNumberString(TRNAMT2,0);
				log.debug("TRNAMT2Format={}",TRNAMT2Format);
				bsData.put("TRNAMT2Format",TRNAMT2Format);
				
				model.addAttribute("bsData",bsData);
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_resale_result error >> {}",e);
		}
		return bs;
	}
	/**
	 * N09102 黃金預約回售確認頁
	 */
	public void gold_reserve_resale_confirm(Map<String,String> requestParam,Model model){
		try{
			String TRNGD = requestParam.get("TRNGD");
			log.debug(ESAPIUtil.vaildLog("TRNGD={}"+TRNGD));
			
			String TRNGDFormat;
			if(TRNGD.indexOf(".") == -1){
				TRNGDFormat = TRNGD + ".00";
			}
			else{
				TRNGDFormat = TRNGD;
			}
			log.debug(ESAPIUtil.vaildLog("TRNGDFormat={}"+TRNGDFormat));
			requestParam.put("TRNGDFormat",TRNGDFormat);
			
			String FEEAMT1 = requestParam.get("FEEAMT1");
			log.debug(ESAPIUtil.vaildLog("FEEAMT1={}"+FEEAMT1));
			String FEEAMT1Format = fund_transfer_service.formatNumberString(FEEAMT1,0);
			log.debug(ESAPIUtil.vaildLog("FEEAMT1Format={}"+FEEAMT1Format));
			requestParam.put("FEEAMT1Format",FEEAMT1Format);
			
			String FEEAMT2 = requestParam.get("FEEAMT2");
			log.debug(ESAPIUtil.vaildLog("FEEAMT2={}"+FEEAMT2));
			String FEEAMT2Format = fund_transfer_service.formatNumberString(FEEAMT2,0);
			log.debug(ESAPIUtil.vaildLog("FEEAMT2Format={}"+FEEAMT2Format));
			requestParam.put("FEEAMT2Format",FEEAMT2Format);
			
			model.addAttribute("requestParam",requestParam);
			
			//IKEY要使用的JSON:DC
		 	String jsondc = URLEncoder.encode(CodeUtil.toJson(requestParam),"UTF-8");
		 	log.debug(ESAPIUtil.vaildLog("jsondc={}"+jsondc));
		 	model.addAttribute("jsondc",jsondc);
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_reserve_resale_confirm error >> {}",e);
		}
	}
	/**
	 * N09102黃金預約回售結果頁
	 */
	@SuppressWarnings("unchecked")
	public BaseResult gold_reserve_resale_result(Map<String,String> requestParam,Model model){
		BaseResult bs = null;
		try{
			bs = new BaseResult();
			bs = N091_02_REST(requestParam);
			if(bs != null && bs.getResult()){
				Map<String,Object> bsData = (Map<String,Object>)bs.getData();
				log.debug("bsData={}",bsData);
				
				String TRNGD = (String)bsData.get("TRNGD");
				log.debug("TRNGD={}",TRNGD);
				String TRNGDFormat = fund_transfer_service.formatNumberString(TRNGD,2);
				log.debug("TRNGDFormat={}",TRNGDFormat);
				bsData.put("TRNGDFormat",TRNGDFormat);
				
				String CMQTIME = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());
				log.debug("CMQTIME={}",CMQTIME);
				bsData.put("CMQTIME",CMQTIME);
				
				model.addAttribute("bsData",bsData);
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_reserve_resale_result error >> {}",e);
		}
		return bs;
	}
	/**
	 * N09001 黃金即時買進確認頁
	 */
	@SuppressWarnings("unchecked")
	public BaseResult gold_buy_confirm(Map<String,String> requestParam,Model model){
		BaseResult bs = null;
		try{
			bs = new BaseResult();
			bs = N090_01_REST(requestParam);
			if(bs != null && bs.getResult()){
				Map<String,Object> bsData = (Map<String,Object>)bs.getData();
				log.debug("bsData={}",bsData);
				
				String TRNGD = (String)bsData.get("TRNGD");
				log.debug("TRNGD={}",TRNGD);
				String TRNGDFormat = fund_transfer_service.formatNumberString(TRNGD,2);
				log.debug("TRNGDFormat={}",TRNGDFormat);
				bsData.put("TRNGDFormat",TRNGDFormat);
				
				String PRICE = (String)bsData.get("PRICE");
				log.debug("PRICE={}",PRICE);
				String PRICEFormat = fund_transfer_service.formatNumberString(PRICE,2);
				log.debug("PRICEFormat={}",PRICEFormat);
				bsData.put("PRICEFormat",PRICEFormat);
				
				String DISPRICE = (String)bsData.get("DISPRICE");
				log.debug("DISPRICE={}",DISPRICE);
				String DISPRICEFormat = fund_transfer_service.formatNumberString(DISPRICE,2);
				log.debug("DISPRICEFormat={}",DISPRICEFormat);
				bsData.put("DISPRICEFormat",DISPRICEFormat);
				
				String PERDIS = (String)bsData.get("PERDIS");
				log.debug("PERDIS={}",PERDIS);
				String PERDISFormat = fund_transfer_service.formatNumberString(PERDIS,2);
				log.debug("PERDISFormat={}",PERDISFormat);
				bsData.put("PERDISFormat",PERDISFormat);
				
				String TRNFEE = (String)bsData.get("TRNFEE");
				log.debug("TRNFEE={}",TRNFEE);
				String TRNFEEFormat = fund_transfer_service.formatNumberString(TRNFEE,0);
				log.debug("TRNFEEFormat={}",TRNFEEFormat);
				bsData.put("TRNFEEFormat",TRNFEEFormat);
				
				String TRNAMT = (String)bsData.get("TRNAMT");
				log.debug("TRNAMT={}",TRNAMT);
				String TRNAMTFormat = fund_transfer_service.formatNumberString(TRNAMT,0);
				log.debug("TRNAMTFormat={}",TRNAMTFormat);
				bsData.put("TRNAMTFormat",TRNAMTFormat);
				
				model.addAttribute("bsData",bsData);
				
				//IKEY要使用的JSON:DC
			 	String jsondc = URLEncoder.encode(CodeUtil.toJson(requestParam),"UTF-8");
			 	log.debug(ESAPIUtil.vaildLog("jsondc={}"+jsondc));
			 	model.addAttribute("jsondc",jsondc);
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_buy_confirm error >> {}",e);
		}
		return bs;
	}
	/**
	 * N09001黃金即時買進結果頁
	 */
	@SuppressWarnings("unchecked")
	public BaseResult gold_buy_result(Map<String,String> requestParam,Model model){
		BaseResult bs = null;
		try{
			bs = new BaseResult();
			bs = N090_01_REST(requestParam);
			if(bs != null && bs.getResult()){
				Map<String,Object> bsData = (Map<String,Object>)bs.getData();
				log.debug("bsData={}",bsData);
				
				String TRNGD = (String)bsData.get("TRNGD");
				log.debug("TRNGD={}",TRNGD);
				String TRNGDFormat = fund_transfer_service.formatNumberString(TRNGD,2);
				log.debug("TRNGDFormat={}",TRNGDFormat);
				bsData.put("TRNGDFormat",TRNGDFormat);
				
				String PRICE = (String)bsData.get("PRICE");
				log.debug("PRICE={}",PRICE);
				String PRICEFormat = fund_transfer_service.formatNumberString(PRICE,2);
				log.debug("PRICEFormat={}",PRICEFormat);
				bsData.put("PRICEFormat",PRICEFormat);
				
				String DISPRICE = (String)bsData.get("DISPRICE");
				log.debug("DISPRICE={}",DISPRICE);
				String DISPRICEFormat = fund_transfer_service.formatNumberString(DISPRICE,2);
				log.debug("DISPRICEFormat={}",DISPRICEFormat);
				bsData.put("DISPRICEFormat",DISPRICEFormat);
				
				String PERDIS = (String)bsData.get("PERDIS");
				log.debug("PERDIS={}",PERDIS);
				String PERDISFormat = fund_transfer_service.formatNumberString(PERDIS,2);
				log.debug("PERDISFormat={}",PERDISFormat);
				bsData.put("PERDISFormat",PERDISFormat);
				
				String TRNFEE = (String)bsData.get("TRNFEE");
				log.debug("TRNFEE={}",TRNFEE);
				String TRNFEEFormat = fund_transfer_service.formatNumberString(TRNFEE,0);
				log.debug("TRNFEEFormat={}",TRNFEEFormat);
				bsData.put("TRNFEEFormat",TRNFEEFormat);
				
				String TRNAMT = (String)bsData.get("TRNAMT");
				log.debug("TRNAMT={}",TRNAMT);
				String TRNAMTFormat = fund_transfer_service.formatNumberString(TRNAMT,0);
				log.debug("TRNAMTFormat={}",TRNAMTFormat);
				bsData.put("TRNAMTFormat",TRNAMTFormat);
				
				model.addAttribute("bsData",bsData);
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_buy_result error >> {}",e);
		}
		return bs;
	}
	/**
	 * N09101 預約黃金買進確認頁
	 */
	@SuppressWarnings("unchecked")
	public BaseResult gold_booking_confirm(Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			log.trace(ESAPIUtil.vaildLog("gold_booking_confirm.reqParam: {}"+ CodeUtil.toJson(reqParam)));
			bs = new BaseResult();

			// 放入TXTOKEN
			bs = getTxToken();
			if (bs.getResult()) {
				reqParam.put("TXTOKEN", ((Map<String, String>) bs.getData()).get("TXTOKEN"));
				bs.setData(reqParam);
				bs.setResult(Boolean.TRUE);
			}

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_booking_confirm error >> {}",e);
		}
		return bs;
	}
	
	/**
	 * N09101預約黃金買進結果頁
	 */
	@SuppressWarnings("unchecked")
	public BaseResult gold_booking_result(Map<String,String> requestParam){
		BaseResult bs = null;
		try{
			bs = new BaseResult();
			bs = N091_01_REST(requestParam);
			if(bs != null && bs.getResult()){
				Map<String,Object> bsData = (Map<String,Object>)bs.getData();
				log.debug("bsData={}",bsData);
				//轉值
				bsData.put("TRNGD", NumericUtil.formatNumberString((String)bsData.get("TRNGD"),2));
				bsData.put("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
				bsData.put("CUSIDN", requestParam.get("CUSIDN"));
				bs.setData(bsData);
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_booking_result error >> {}",e);
		}
		return bs;
	}
	
	
	/**
	 * 判斷黃金是否可以執行即時交易
	 * true可以
	 * false不可以
	 */
	public boolean isGoldTradeTime(){
		boolean result = false;
		
		Date date = new Date();
		String currentDate = new SimpleDateFormat("yyyyMMdd").format(date);
		log.debug("currentDate={}",currentDate);
		String currentTime = new SimpleDateFormat("HHmmss").format(date);
		log.debug("currentTime={}",currentTime);
		
		SYSPARAMDATA sysParamData = sysParamDataDao.getByPK("NBSYS");

		if(sysParamData != null && sysParamData.getADPK() != null){
			String tradeStart = sysParamData.getADGDSHH() + sysParamData.getADGDSMM() + sysParamData.getADGDSSS();
			log.debug("tradeStart={}",tradeStart);
			String tradeEnd = sysParamData.getADGDEHH() + sysParamData.getADGDEMM() + sysParamData.getADGDESS();			
			log.debug("tradeEnd={}",tradeEnd);
			
			ADMHOLIDAY admHoliday = admHolidayDao.getByPK(currentDate);
			log.debug("admHoliday={}",admHoliday);
			//非假日且在營業時間內
			if(admHoliday != null && admHoliday.getADHOLIDAYID() == null && currentTime.compareTo(tradeStart) > 0 && currentTime.compareTo(tradeEnd) < 0){
				result = true;
			}
		}
		return result;
	}
	/**
	 * 判斷黃金是否可以執行預約交易
	 * true可以
	 * false不可以
	 */
	public boolean isGoldScheduleTime(){
		boolean result = false;
		
		Date date = new Date();
		String currentDate = new SimpleDateFormat("yyyyMMdd").format(date);
		log.debug("currentDate={}",currentDate);
		String currentTime = new SimpleDateFormat("HHmmss").format(date);
		log.debug("currentTime={}",currentTime);
		
		SYSPARAMDATA sysParamData = sysParamDataDao.getByPK("NBSYS");

		if(sysParamData != null && sysParamData.getADPK() != null){
			int scheduleStartHourInt = Integer.parseInt(sysParamData.getADGDSHH()) - 1;
			String scheduleStartHour = String.valueOf(scheduleStartHourInt);
			if (scheduleStartHour.length() < 2){
				scheduleStartHour = "0" + scheduleStartHour;
			}
			log.debug("scheduleStartHour={}",scheduleStartHour);
			
			int scheduleStartMinInt = Integer.parseInt(sysParamData.getADGDSMM()) + 59;
			String scheduleStartMin = String.valueOf(scheduleStartMinInt);
			if (scheduleStartMin.length() < 2){
				scheduleStartMin = "0" + scheduleStartMin;
			}
			log.debug("scheduleStartMin={}",scheduleStartMin);
			
			String tradeStart = scheduleStartHour + scheduleStartMin + sysParamData.getADGDSSS();
			log.debug("tradeStart={}",tradeStart);
			String tradeEnd = sysParamData.getADGDEHH() + sysParamData.getADGDEMM() + sysParamData.getADGDESS();			
			log.debug("tradeEnd={}",tradeEnd);
			
			ADMHOLIDAY admHoliday = admHolidayDao.getByPK(currentDate);
			log.debug("admHoliday={}",admHoliday);
			
			//假日
			if(admHoliday != null && admHoliday.getADHOLIDAYID() != null){
				result = true;
			}
			//非假日且在預約時間內
			else if(admHoliday != null && admHoliday.getADHOLIDAYID() == null && (currentTime.compareTo(tradeStart) < 0 || currentTime.compareTo(tradeEnd) > 0)){
				result = true;
			}
		}
		return result;
	}
	/**
	 * 取得黃金存款網路交易帳號
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String,String>> getGoldTradeAccountList(String CUSIDN , String TYPE){
		List<Map<String,String>> goldTradeAccountList = new ArrayList<Map<String,String>>();
		
		//取得黃金存摺網路交易帳號
		BaseResult bs926 = N926_REST(CUSIDN,GOLD_RESERVATION_QOERY);
		
		
		if(bs926 != null && bs926.getResult()){
			Map<String,Object> bs926Data = (Map<String,Object>)bs926.getData();
			List<Map<String,String>> data = (List<Map<String,String>>)bs926Data.get("REC");
			List<Map<String, String>> rows = new ArrayList<>();
			List checkDouble = new ArrayList();
			int count = 0;
				for (Map<String, String> map : data) {
					if ("BUY".equals(TYPE)) { //買進的帳號選擇
						if (map.get("SVACN").length() == 11) {
							if(count>0) {
								if(!checkDouble.contains(map.get("SVACN"))) {
									checkDouble.add((String)map.get("SVACN"));
									rows.add(map);
									count++;
								}
							}else {
								checkDouble.add((String)map.get("SVACN"));
								rows.add(map);
								count++;
							}
						}
					}else { //回售的帳號選擇
						if (map.get("ACN").length() == 11) {
							if(count>0) {
								if(!checkDouble.contains(map.get("ACN"))) {
									checkDouble.add((String)map.get("ACN"));
									rows.add(map);
									count++;
								}
							}else {
								checkDouble.add((String)map.get("ACN"));
								rows.add(map);
								count++;
							}
						}
					}
				}
			log.debug("rows={}",rows);
			
			goldTradeAccountList = rows;
		}
		
		return goldTradeAccountList;
	}
}
