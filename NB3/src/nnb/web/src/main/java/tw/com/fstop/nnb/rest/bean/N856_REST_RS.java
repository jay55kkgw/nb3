package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N856_REST_RS extends BaseRestBean_TW implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1349162467760639989L;

	private String	SrcID ;
	private String	KeyID ;
	private String	DivData ;
	private String	ICV ;
	private String	MAC ;
	private String	TxnDatetime ;
	private String	STAN ;
	private String	RCode ;
	private String	CustBillerAcnt ;
	private String	CustBankAcnt ;
	
	public String getSrcID() {
		return SrcID;
	}
	public void setSrcID(String srcID) {
		SrcID = srcID;
	}
	public String getKeyID() {
		return KeyID;
	}
	public void setKeyID(String keyID) {
		KeyID = keyID;
	}
	public String getDivData() {
		return DivData;
	}
	public void setDivData(String divData) {
		DivData = divData;
	}
	public String getICV() {
		return ICV;
	}
	public void setICV(String iCV) {
		ICV = iCV;
	}
	public String getMAC() {
		return MAC;
	}
	public void setMAC(String mAC) {
		MAC = mAC;
	}
	public String getTxnDatetime() {
		return TxnDatetime;
	}
	public void setTxnDatetime(String txnDatetime) {
		TxnDatetime = txnDatetime;
	}
	public String getSTAN() {
		return STAN;
	}
	public void setSTAN(String sTAN) {
		STAN = sTAN;
	}
	public String getRCode() {
		return RCode;
	}
	public void setRCode(String rCode) {
		RCode = rCode;
	}
	public String getCustBillerAcnt() {
		return CustBillerAcnt;
	}
	public void setCustBillerAcnt(String custBillerAcnt) {
		CustBillerAcnt = custBillerAcnt;
	}
	public String getCustBankAcnt() {
		return CustBankAcnt;
	}
	public void setCustBankAcnt(String custBankAcnt) {
		CustBankAcnt = custBankAcnt;
	}
}
