package tw.com.fstop.nnb.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.po.ADMLOGINMB;
import tw.com.fstop.tbb.nnb.dao.AdmLoginMbDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.web.util.RESTUtil;


@Service
public class Mobile_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	 @Autowired
	 public RESTUtil restutil;
	
	@Autowired
	private AdmLoginMbDao admloginmbdao;
		
	/**
	 * 確認使用者MB3連線狀態--因為load balance問題改成直接檢核資料庫-getMB3StatusDB
	 * @param cusidn 統編/身分證
	 * 
	 * @return
	 */
//	public boolean getMB3Status(String cusidn) {
//		boolean result = true;
//		
//		try {
//			boolean auth = "Y".equals(SpringBeanFactory.getBean("mb3_auth")) ? true : false;
//			String url = SpringBeanFactory.getBean("mb3_url") + "checkLogin";
//			String cert_path = SpringBeanFactory.getBean("mb3_cert_path");
//			String soci = SpringBeanFactory.getBean("mb3_soci");
//			
//			HashMap<String, String> params = new HashMap<String, String>();
//			params.put("cusidn", cusidn);
//			
//			
//			Map retMap = restutil.query(params, url, cert_path, soci, auth);
//			log.debug(ESAPIUtil.vaildLog("getMB3Status.retMap: " + CodeUtil.toJson(retMap)));
//			// retMap.result: true -> MB3登出, false -> MB3登入 
//			result = "false".equals(String.valueOf(retMap.get("result"))) ? false : true;
//			
//		} catch (Exception e) {
//			log.error("",e);
//		}
//		
//		return result;
//	}
	
	/**
	 * 確認使用者MB3連線狀態
	 * @param cusidn 統編/身分證
	 * 
	 * @return
	 * true : MB3登入中
	 */
	public boolean getMB3StatusDB(String cusidn) {
		boolean result = false;
		List<ADMLOGINMB> admloginmbList = null;
		try {
			// 取得登入時資訊
			admloginmbList = admloginmbdao.findByUserId(cusidn);
			
			if(admloginmbList != null && !admloginmbList.isEmpty()) {
				for(ADMLOGINMB po : admloginmbList) {
					if (po != null && "0".equals(po.getLOGINOUT())) {
						result = true;
					}
				}
			}
			
		} catch (Exception e) {
			log.error("", e);
		}
		return result;
	}
	
	/**
	 * 剔退使用者MB3連線狀態--因為load balance問題改成直接修改資料庫-kickMB3DB
	 * @param cusidn 統編/身分證
	 * 
	 * @return
	 */
//	public void kickMB3(String cusidn) {
//		try {
//			boolean auth = "Y".equals(SpringBeanFactory.getBean("mb3_cert_path")) ? true :false;
//			String url = SpringBeanFactory.getBean("mb3_url") + "logoutForce";
//			String cert_path = SpringBeanFactory.getBean("mb3_cert_path");
//			String soci = SpringBeanFactory.getBean("mb3_soci");
//			
//			HashMap<String, String> params = new HashMap<String, String>();
//			params.put("cusidn", cusidn);
//			
//			Map retMap = restutil.query(params, url, cert_path, soci, auth);
//			log.debug(ESAPIUtil.vaildLog("kickMB3.retMap: " + CodeUtil.toJson(retMap)));
//			
//		} catch (Exception e) {
//			log.error("",e);
//		}
//		
//	}
	
	/**
	 * 讓MB3資料庫登入狀態改為登出
	 * @param cusidn 統編/身分證
	 * 
	 * @return
	 */
	public boolean kickMB3DB(String cusidn) {
		boolean result = false;
		List<ADMLOGINMB> admloginmbList = null;
		try {
			// 取得登入時資訊
			admloginmbList = admloginmbdao.findByUserId(cusidn);
			
			if(admloginmbList != null && !admloginmbList.isEmpty()) {
				for(ADMLOGINMB po : admloginmbList) {
					log.info("kickMB3DB.po.ori: {}", CodeUtil.toJson(po));
					po.setLOGINOUT("1"); // 登入狀態	0:登入 1:登出
					po.setFORCELOGOUTTIME(new DateTime(new Date()).toString("yyyyMMddHHmmss")); // 本次強迫登出日期時間
					po.setLASTLOGINTIME(po.getLOGINTIME()); // 上次登入日期時間
					po.setLASTADUSERIP(po.getADUSERIP()); // 上次登入IP

					log.info("kickMB3DB.po.final: {}", CodeUtil.toJson(po));
					admloginmbdao.update(po);
				}
			}
			result = true;
			
		} catch (Exception e) {
			log.error("", e);
		}
		return result;
	}
	
}
