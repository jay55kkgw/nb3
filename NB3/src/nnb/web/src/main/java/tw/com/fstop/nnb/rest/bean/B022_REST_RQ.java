package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class B022_REST_RQ extends BaseRestBean_FUND implements Serializable{
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5780706698186289963L;
	
	private String CUSIDN ;
	private String BRHCOD ;
	private String I01;//類別(1:檢核 2:確認)
	private String I02;//信託帳號
	private String I03;//商品代號
	private String I05;//幣別
	private String I06;//委賣面額
	private String I07;//委託賣價
	private String I08;//保管費
	private String I09;//前手息+/-
	private String I10;//前手息
	private String I11;//信託本金
	private String I12;//入帳帳號
	private String I13;//贖回方式(1.全贖2.部贖)
	private String IP;//IP位址
	
	private String ADOPID = "B022";
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getBRHCOD() {
		return BRHCOD;
	}
	public void setBRHCOD(String bRHCOD) {
		BRHCOD = bRHCOD;
	}
	public String getI01() {
		return I01;
	}
	public void setI01(String i01) {
		I01 = i01;
	}
	public String getI02() {
		return I02;
	}
	public void setI02(String i02) {
		I02 = i02;
	}
	public String getI03() {
		return I03;
	}
	public void setI03(String i03) {
		I03 = i03;
	}
	public String getI05() {
		return I05;
	}
	public void setI05(String i05) {
		I05 = i05;
	}
	public String getI06() {
		return I06;
	}
	public void setI06(String i06) {
		I06 = i06;
	}
	public String getI07() {
		return I07;
	}
	public void setI07(String i07) {
		I07 = i07;
	}
	public String getI08() {
		return I08;
	}
	public void setI08(String i08) {
		I08 = i08;
	}
	public String getI09() {
		return I09;
	}
	public void setI09(String i09) {
		I09 = i09;
	}
	public String getI10() {
		return I10;
	}
	public void setI10(String i10) {
		I10 = i10;
	}
	public String getI11() {
		return I11;
	}
	public void setI11(String i11) {
		I11 = i11;
	}
	public String getI12() {
		return I12;
	}
	public void setI12(String i12) {
		I12 = i12;
	}
	public String getI13() {
		return I13;
	}
	public void setI13(String i13) {
		I13 = i13;
	}
	public String getIP() {
		return IP;
	}
	public void setIP(String iP) {
		IP = iP;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	
}
