package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class NA71_REST_RS extends BaseRestBean implements Serializable 
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2797665857808999513L;
	
	private String RSPCOD;		// 回應代碼
	private String HEADER;
	// 電文沒回但ms_tw回應的欄位
	private String CMQTIME;		// 執行時間

	
	
	public String getCMQTIME() {
		return CMQTIME;
	}

	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getRSPCOD() {
		return RSPCOD;
	}

	public void setRSPCOD(String rSPCOD) {
		RSPCOD = rSPCOD;
	}

	public String getHEADER() {
		return HEADER;
	}

	public void setHEADER(String hEADER) {
		HEADER = hEADER;
	}
	
}
