package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class TD02_REST_RS extends BaseRestBean implements Serializable {

	
	private static final long serialVersionUID = -2984598977923484580L;
	/**
	 * 
	 */
	
	String TOTCNT;
	String CMQTIME;
	String CMPERIOD;
	LinkedList<TD02_REST_RSDATA> REC;
	
	
	
	public LinkedList<TD02_REST_RSDATA> getREC() {
		return REC;
	}
	public void setREC(LinkedList<TD02_REST_RSDATA> rEC) {
		REC = rEC;
	}
	public String getTOTCNT() {
		return TOTCNT;
	}
	public void setTOTCNT(String tOTCNT) {
		TOTCNT = tOTCNT;
	}
	public String getCMPERIOD() {
		return CMPERIOD;
	}
	public void setCMPERIOD(String cMPERIOD) {
		CMPERIOD = cMPERIOD;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	
	
	
}



	//空白
	//String OFFSET;
	//N810
	//String HEADER;
