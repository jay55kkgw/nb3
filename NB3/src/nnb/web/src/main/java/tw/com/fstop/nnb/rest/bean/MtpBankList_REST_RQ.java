package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.Map;

/**
 * 查詢參加單位
 * 
 * @author Vincenthuang
 *
 */
public class MtpBankList_REST_RQ extends BaseRestBean_QR implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 39717691064748449L;
	
	//=========其餘參數===========
	private String ADOPID =  "MtpBankList";	//紀錄TXNLOG用
	
	//=========API參數===========
	private String bankcode = "050";			//銀行代碼

	
	
	
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getBankcode() {
		return bankcode;
	}
	public void setBankcode(String bankcode) {
		this.bankcode = bankcode;
	}

	
	
}
