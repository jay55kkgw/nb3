package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N841_REST_RS extends BaseRestBean implements Serializable {

	

	/**
	 * 
	 */
	private static final long serialVersionUID = -2800878139167394383L;
	
	
	private String CMQTIME;
//	private String TOPMSG;
	private String ABEND;
	private String REC_NO;
	private String __OCCURS;
	private String USERDATA;
	private LinkedList<N841_REST_RSDATA> REC;
	
	
	
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
//	public String getTOPMSG() {
//		return TOPMSG;
//	}
//	public void setTOPMSG(String tOPMSG) {
//		TOPMSG = tOPMSG;
//	}
	public String getABEND() {
		return ABEND;
	}
	public void setABEND(String aBEND) {
		ABEND = aBEND;
	}
	public String getREC_NO() {
		return REC_NO;
	}
	public void setREC_NO(String rEC_NO) {
		REC_NO = rEC_NO;
	}
	public String get__OCCURS() {
		return __OCCURS;
	}
	public void set__OCCURS(String __OCCURS) {
		this.__OCCURS = __OCCURS;
	}
	public String getUSERDATA() {
		return USERDATA;
	}
	public void setUSERDATA(String uSERDATA) {
		USERDATA = uSERDATA;
	}
	public LinkedList<N841_REST_RSDATA> getREC() {
		return REC;
	}
	public void setREC(LinkedList<N841_REST_RSDATA> rEC) {
		REC = rEC;
	}
	
	

}
