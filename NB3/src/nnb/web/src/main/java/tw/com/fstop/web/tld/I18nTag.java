
/*
 * Copyright (c) 2017, FSTOP, Inc. All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tw.com.fstop.web.tld;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import tw.com.fstop.util.I18nUtil;


/**
 * I18n tag library.
 * 
 * <pre>
 * Usage:
 *   JSP:
 *     &lt;%@ taglib prefix=&quot;top&quot; uri=&quot;/WEB-INF/tld/i18n.tld&quot;%&gt;
 *     &lt;top:i18n key=&quot;userName&quot; def=&quot;page&quot;/&gt;
 * 
 * </pre>
 * 
 *
 * @since 1.0.1
 */
public class I18nTag extends SimpleTagSupport
{
    private String key;
    private String def;

    public String getKey()
    {
        return key;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public String getDef()
    {
        return def;
    }

    public void setDef(String def)
    {
        this.def = def;
    }

    @Override
    public void doTag() throws JspException, IOException
    {
        final JspWriter out = getJspContext().getOut();
        String word = I18nUtil.getMsg(key, def);
        out.print(word);
    }
}
