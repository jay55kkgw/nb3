package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N8508_REST_RS extends BaseRestBean implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3445565249062483323L;
	
	private String CMQTIME;
	private LinkedList<N8507_REST_RSDATA> REC;
	
	
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public LinkedList<N8507_REST_RSDATA> getREC() {
		return REC;
	}
	public void setREC(LinkedList<N8507_REST_RSDATA> rEC) {
		REC = rEC;
	}
	
	
	
	
	

}
