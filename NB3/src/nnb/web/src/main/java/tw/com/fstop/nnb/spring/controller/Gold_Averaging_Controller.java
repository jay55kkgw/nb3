package tw.com.fstop.nnb.spring.controller;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.idgate.bean.N09302_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N09302_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N09301_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N09301_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N830_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N830_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N831_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N831_IDGATE_DATA_VIEW;
import tw.com.fstop.nnb.service.Gold_Averaging_Service;
import tw.com.fstop.nnb.service.Gold_Transaction_Service;
import tw.com.fstop.nnb.service.IdGateService;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.util.DateUtil;
import tw.com.fstop.tbb.nnb.util.StrUtils;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.web.util.WebUtil;

/**
 * 黃金存摺-定期定額的Controller
 */
@SessionAttributes({SessionUtil.CUSIDN,SessionUtil.PRINT_DATALISTMAP_DATA,SessionUtil.RESULT_LOCALE_DATA,
	SessionUtil.GOLDAGREEFLAG_PERIOD,SessionUtil.CONFIRM_LOCALE_DATA,SessionUtil.TRANSFER_DATA,
	SessionUtil.STEP1_LOCALE_DATA,SessionUtil.IDGATE_TRANSDATA})
@Controller
@RequestMapping(value = "/GOLD/AVERAGING")
public class Gold_Averaging_Controller {
	Logger log = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private Gold_Averaging_Service gold_averaging_service;
	
	@Autowired
	private Gold_Transaction_Service gold_transaction_service;
	
	@Autowired
	I18n i18n;

	@Autowired		 
	IdGateService idgateservice;
	@RequestMapping(value = "/averaging_query")
	public String averaging_query(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("averaging_query start~~~");
		try {
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			log.debug("cusidn: {} ", cusidn);
			bs = gold_averaging_service.averaging_query(cusidn);
			bs.addData("TODAY",DateUtil.getDate("/"));
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("averaging_query error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				Map<String, Object> dataMap = (Map) bs.getData();
				ArrayList<Map<String, String>> row = (ArrayList<Map<String, String>>) dataMap.get("REC");
				int count = row.size();
				log.trace("row>>"+row.size());
				log.trace("count>>"+count);
				if(count==0) {
					target = "/gold/noaccount_1";
					Map<String,String>NCdata=new HashMap<String,String>();
					//TODO i18N
					//NCdata.put("NC_title","黃金定期定額約定查詢");
					NCdata.put("NC_title","LB.X1771");
					//NCdata.put("NC_msg","您尚未申請黃金存摺帳戶，請洽往來分行辦理或線上立即申請。");
					NCdata.put("NC_msg","LB.X1772");
					//NCdata.put("NC_forwardmsg","線上立即申請");
					NCdata.put("NC_forwardmsg","LB.X0954");
					NCdata.put("NC_forward","/GOLD/APPLY/gold_account_apply");
					bs.addData("NCdata", NCdata);
					log.trace("bsData>>{}",bs.getData());
					model.addAttribute("bsdata", bs);
				}
				else {
					target = "/gold/averaging_query";
					log.debug("averaging_query JSP display data >> "+CodeUtil.toJson(bs));
					model.addAttribute("averaging_query", bs);
				}
			} 
			else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	//定期定額結果頁
	@RequestMapping(value = "/averaging_query_result")
	public String averaging_query_r(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("averaging_query_result start~~~");
		try {
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			log.debug(ESAPIUtil.vaildLog("reqParam{}"+ reqParam));
			log.debug("cusidn" + cusidn);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
			}
			else{
				bs = gold_averaging_service.averaging_query_result(cusidn, reqParam);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("averaging_query_r error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/gold/averaging_query_result";
				log.debug("bs_get_data={}", bs.getData());
				List<Map<String, String>> dataListMap = ((List<Map<String, String>>) ((Map<String, Object>) bs.getData()).get("REC"));
				log.debug("dataListMap={}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
				model.addAttribute("averaging_query_result", bs);
			} else {
				bs.setPrevious("/GOLD/AVERAGING/averaging_query");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * 黃金定期定額(條款頁)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/gdperiod_article")
	public String gdperiod_article(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		BaseResult bs = null;
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		String urlACN = okMap.get("ACN");
		try {
			bs = new BaseResult();
		}catch (Exception e){
			
		}finally {
			if (bs != null && bs.getResult())
			{
				bs.addData("ACN", urlACN);
//				log.trace("ACN >>",urlACN);
				target = "/gold/gdperiod_article";
				model.addAttribute("gdperiod_article", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		
		return target;
	}
	
	/**
	 * N09301定期定額申購(輸入頁)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/averaging_purchase")
	public String averaging_purchase(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		BaseResult bs = null;
		BaseResult bs920 = null;
		BaseResult bs927 = null;
		BaseResult bs926 = null;
		//清除切換語系時暫存的資料
		SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		String urlACN = okMap.get("ACN");
		log.debug("averaging_purchase");
		try {
			bs = new BaseResult();
			bs920 = new BaseResult();
			bs927 = new BaseResult();
			bs926 = new BaseResult();			
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			log.trace("CUSIDN>>>{}", cusidn);
			//判斷是否同意過條款
			String agreeflag =(String) SessionUtil.getAttribute(model,SessionUtil.GOLDAGREEFLAG_PERIOD,null);
			if(StrUtils.isNotEmpty(reqParam.get("FLAG"))) {
				agreeflag = reqParam.get("FLAG");
				SessionUtil.addAttribute(model, SessionUtil.GOLDAGREEFLAG_PERIOD, agreeflag);
			}
			log.trace(ESAPIUtil.vaildLog("agree Flag>>>{}"+agreeflag));
			reqParam.put("agreeflag", agreeflag);
			
			if(StrUtils.isEmpty(agreeflag)) {
				//若未同意條款導條款頁及判斷n920 & n926
				bs920 = gold_averaging_service.N920_REST(cusidn, gold_averaging_service.GOLD_RESERVATION_QOERY_N920);
				if(bs920 != null && bs920.getResult()){
					Map<String,Object> bs920Data = (Map<String,Object>)bs920.getData();
					List<Map<String,String>> rows = (List<Map<String,String>>)bs920Data.get("REC");
					//沒有黃金存摺帳號
					if(rows.isEmpty()){
						BaseResult bsParm = new BaseResult();
						bsParm.addData("TXID", "averaging_purchase");
						bsParm.addData("MSGTYPE", "N920");
						SessionUtil.addAttribute(model,SessionUtil.STEP1_LOCALE_DATA,bsParm);
						target = "forward:/GOLD/TRANSACTION/gold_applyconfirm";
						return target;
					}
				}
				bs926 = gold_averaging_service.N926_REST(cusidn, gold_averaging_service.GOLD_RESERVATION_QOERY);
				if(bs926 != null && bs926.getResult()){
					Map<String,Object> bs926Data = (Map<String,Object>)bs926.getData();
					List<Map<String,String>> rows = (List<Map<String,String>>)bs926Data.get("REC");
				
					//沒有黃金存摺網路交易帳號
					if(rows.isEmpty()){
						BaseResult bsParm = new BaseResult();
						bsParm.addData("TXID", "averaging_purchase");
						bsParm.addData("MSGTYPE", "N926");
						SessionUtil.addAttribute(model,SessionUtil.STEP1_LOCALE_DATA,bsParm);
						target = "forward:/GOLD/TRANSACTION/gold_applyconfirm";
						return target;
					}
				}
				bs.addData("ADOPID", "N09301");
				bs.addData("ACN", urlACN);
				model.addAttribute("gdperiod_article", bs);
				target = "/gold/gdperiod_article";
				return target;
			}
//			
//			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				log.debug("averaging_purchase.cusidn >>{}", cusidn);
				try {
					String kyc = (String)SessionUtil.getAttribute(model,SessionUtil.KYCDATE,null);
					bs927 = gold_averaging_service.checkKYC(cusidn, reqParam, kyc, model);
					if(bs927 == null || !bs927.getResult()) {
						model.addAttribute(BaseResult.ERROR,bs927);
						return target;
					}
					else if(bs927 != null && bs927.getResult()) {
						Map<String, Object> dataMap = (Map<String, Object>) bs927.getData();
						if(((String)dataMap.get("Gd_InvAttr")).equals("N")) {
							model.addAttribute("TXID","N09301");
							target = "/gold/gd_inv_attr";
							return target;
						}
						
						//查N920
						bs920 = gold_averaging_service.N920_REST(cusidn, gold_averaging_service.GOLD_RESERVATION_QOERY_N920);
						log.trace("bs920 DATA>>>>>{}",bs920.getData());
						if(bs920 != null && bs920.getResult()) {
							Map<String, Object> bs920Data = (Map) bs920.getData();
							ArrayList<Map<String, String>> rows = (ArrayList<Map<String, String>>) bs920Data.get("REC");
							//檢查是否有黃金存摺帳號
							int acncount = 0;
							for (Map<String, String> row : rows) 
							{
								if(row.get("ACN").length() == 11) {
									acncount++;
								}
							}
							log.trace("bs920 ACN count>>>{}",acncount);
							if(acncount <= 0) {//沒有黃金存摺帳號
								BaseResult bsParm = new BaseResult();
								bsParm.addData("TXID", "averaging_purchase");
								bsParm.addData("MSGTYPE", "N920");
								SessionUtil.addAttribute(model,SessionUtil.STEP1_LOCALE_DATA,bsParm);
								target = "forward:/GOLD/TRANSACTION/gold_applyconfirm";
								return target;
							}
							bs = gold_averaging_service.averaging_purchase(cusidn, reqParam);
							if(bs != null && bs.getResult()) {
								Map<String, String> bsData = (Map) bs.getData();
								String getacn = String.valueOf(bsData.get("COUNT"));
								log.trace("SHOW COUNT>>>>>{}", getacn);
								if(StrUtils.isEmpty(getacn) || getacn.equals("0")) {
									//沒有黃金存摺網路交易帳號
									BaseResult bsParm = new BaseResult();
									bsParm.addData("TXID", "averaging_purchase");
									bsParm.addData("MSGTYPE", "N926");
									SessionUtil.addAttribute(model,SessionUtil.STEP1_LOCALE_DATA,bsParm);
									target = "forward:/GOLD/TRANSACTION/gold_applyconfirm";
									return target;
								}
							}
							
							log.trace("averaging_purchase bs>>>>>{}", bs.getData());
							SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
						}
					}
				}
				catch(Exception e) {
					//Avoid Information Exposure Through an Error Message
					//e.printStackTrace();
					log.error("averaging_purchase error >> {}",e);
				}
			}
           //IDGATE身分
           String idgateUserFlag="N";		 
           Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
           try {		 
        	   if(IdgateData==null) {
        		   IdgateData = new HashMap<String, Object>();
        	   }
               BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
               if(tmp.getResult()) {		 
                   idgateUserFlag="Y";                  		 
               }		 
               tmp.addData("idgateUserFlag",idgateUserFlag);		 
               IdgateData.putAll((Map<String, String>) tmp.getData());
               SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
           }catch(Exception e) {		 
               log.debug("idgateUserFlag error {}",e);		 
           }		 
           model.addAttribute("idgateUserFlag",idgateUserFlag);
		}
		catch(Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("averaging_purchase error >> {}",e);
		}finally {
			if (bs != null && bs.getResult())
			{
				bs.addData("ACN", urlACN);
				target = "/gold/averaging_purchase";
				model.addAttribute("averaging_purchase", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		
		return target;
	}
	
	/**
	 * N09301定期定額申購(確認頁)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/averaging_purchase_confirm")
	public String averaging_purchase_confirm(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		log.trace(ESAPIUtil.vaildLog("averaging_purchase_confirm >>>{}"+ CodeUtil.toJson(reqParam)));
		String target = "/error";
		BaseResult bs = null;
		
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			log.trace("CUSIDN>>>{}", cusidn);
			
			// IKEY要使用的JSON:DC
			String jsondc = null;
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("averaging_purchase_confirm.jsondc: {}" + jsondc));
			
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				log.debug("averaging_purchase_confirm.cusidn >>{}", cusidn);
				try {
					bs = gold_averaging_service.averaging_purchase_confirm(cusidn, reqParam);
					if(bs != null && bs.getResult()) {
						bs.addData("jsondc", jsondc);
						log.trace("GET BS DATA>>>{}", bs.getData());
						SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
					}
				}
				catch(Exception e) {
					//Avoid Information Exposure Through an Error Message
					//e.printStackTrace();
					log.error("averaging_purchase_confirm error >> {}",e);
				}
			}

			// IDGATE transdata            		 
            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
    		String adopid = "N09301";
    		String title = "您有一筆黃金定期定額申購待確認";
    		Map<String, String> result = new HashMap<String, String>();
    		Map<String, Object> bsData = (Map<String, Object>)bs.getData();
    		for(String key : bsData.keySet()) {
    			try {
    				result.put(key, (String)bsData.get(key));
    			}catch(Exception e){
    			}
    		}
        	if(IdgateData != null) {
	            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
	            if(IdgateData.get("idgateUserFlag").equals("Y")) {
	            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
	            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
	            	result.put("TRANTITLE", "黃金定期定額申購");
	            	result.put("TRANNAME", "申購");
	            	result.put("CUSIDN", cusidn);
	            	result.put("CUSIDN1", cusidn);
	            	result.put("TRNTYP", "11"); //定期定額
	            	result.put("TRNCOD", "01"); //申購
	    			if (! result.get("AMT_06").equals("")) {			
	    				result.put("DATE_06", "Y");				
	    			}
	    			else {
	    				result.put("DATE_06", "N");
	    				result.put("AMT_06", "0");
	    			}
	    			if (! result.get("AMT_16").equals("")) {			
	    				result.put("DATE_16", "Y");				
	    			}
	    			else {
	    				result.put("DATE_16", "N");
	    				result.put("AMT_16", "0");				
	    			}
	    			if (! result.get("AMT_26").equals("")) {			
	    				result.put("DATE_26", "Y");				
	    			}
	    			else {
	    				result.put("DATE_26", "N");
	    				result.put("AMT_26", "0");				
	    			}
					IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N09301_IDGATE_DATA.class, N09301_IDGATE_DATA_VIEW.class, result));

	                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	            }
        	}
            model.addAttribute("idgateAdopid", adopid);
            model.addAttribute("idgateTitle", title);
            
		}
		catch(Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("averaging_purchase_confirm error >> {}",e);
		}
		finally {
			if (bs != null && bs.getResult())
			{
				target = "/gold/averaging_purchase_confirm";
				model.addAttribute("averaging_purchase_confirm", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		
		return target;
	}
	
	/**
	 * N09301定期定額申購(結果頁)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/averaging_purchase_result")
	public String averaging_purchase_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		log.trace(ESAPIUtil.vaildLog("averaging_purchase_result >>>>>{}"+ CodeUtil.toJson(reqParam)));
		String target = "/error";
		BaseResult bs = null;
		
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			log.trace("CUSIDN>>>{}", cusidn);
			
//			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			Map<String, String> okMap = reqParam;
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				log.debug("averaging_purchase_result.cusidn >>{}", cusidn);
				try {
	                //IDgate驗證		 
	                try {		 
	                    if(reqParam.get("FGTXWAY").equals("7")) {		 

	        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
	        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N09301_IDGATE");
	        				reqParam.put("sessionID", (String)IdgateData.get("sessionid"));		 
	        				reqParam.put("txnID", (String)IdgateData.get("txnID"));		 
	        				reqParam.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
	                    }		 
	                }catch(Exception e) {		 
	                    log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
	                }  
					Map<String,Object>IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
					model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
					
					bs = gold_averaging_service.averaging_purchase_result(cusidn, reqParam);
					if(bs != null && bs.getResult()) {
						log.trace("GET BS DATA>>>{}", bs.getData());
						SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
					}
				}
				catch(Exception e) {
					//Avoid Information Exposure Through an Error Message
					//e.printStackTrace();
					log.error("averaging_purchase_result error >> {}",e);
				}
			}
		}
		catch(Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("averaging_purchase_result error >> {}",e);
		}
		finally {
			if (bs != null && bs.getResult())
			{
				target = "/gold/averaging_purchase_result";
				model.addAttribute("averaging_purchase_result", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		
		return target;
	}
	
	/**
	 * N09302定期定額變更(輸入頁)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/averaging_alter")
	public String averaging_alter(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		BaseResult bs = null;
		BaseResult bs920 = null;
		BaseResult bs927 = null;
		BaseResult bs926 = null;
		//清除切換語系時暫存的資料
		SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		String urlACN = okMap.get("ACN");
		log.debug("averaging_alter");
		try {
			bs = new BaseResult();
			bs920 = new BaseResult();
			bs927 = new BaseResult();
			bs926 = new BaseResult();			
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			log.trace("CUSIDN>>>{}", cusidn);
			
			//判斷是否同意過條款
			String agreeflag =(String) SessionUtil.getAttribute(model,SessionUtil.GOLDAGREEFLAG_PERIOD,null);
			if(StrUtils.isNotEmpty(reqParam.get("FLAG"))) {
				agreeflag = reqParam.get("FLAG");
				SessionUtil.addAttribute(model, SessionUtil.GOLDAGREEFLAG_PERIOD, agreeflag);
			}
			log.trace(ESAPIUtil.vaildLog("agree Flag>>>{}"+agreeflag));
			reqParam.put("agreeflag", agreeflag);
			
			if(StrUtils.isEmpty(agreeflag)) {
				//若未同意條款導條款頁及判斷n920 & n926
				bs920 = gold_averaging_service.N920_REST(cusidn, gold_averaging_service.GOLD_RESERVATION_QOERY_N920);
				if(bs920 != null && bs920.getResult()){
					Map<String,Object> bs920Data = (Map<String,Object>)bs920.getData();
					List<Map<String,String>> rows = (List<Map<String,String>>)bs920Data.get("REC");
					//沒有黃金存摺帳號
					if(rows.isEmpty()){
						BaseResult bsParm = new BaseResult();
						bsParm.addData("TXID", "averaging_alter");
						bsParm.addData("MSGTYPE", "N920");
						SessionUtil.addAttribute(model,SessionUtil.STEP1_LOCALE_DATA,bsParm);
						target = "forward:/GOLD/TRANSACTION/gold_applyconfirm";
						return target;
					}
				}
				bs926 = gold_averaging_service.N926_REST(cusidn, gold_averaging_service.GOLD_RESERVATION_QOERY);
				if(bs926 != null && bs926.getResult()){
					Map<String,Object> bs926Data = (Map<String,Object>)bs926.getData();
					List<Map<String,String>> rows = (List<Map<String,String>>)bs926Data.get("REC");
				
					//沒有黃金存摺網路交易帳號
					if(rows.isEmpty()){
						BaseResult bsParm = new BaseResult();
						bsParm.addData("TXID", "averaging_alter");
						bsParm.addData("MSGTYPE", "N926");
						SessionUtil.addAttribute(model,SessionUtil.STEP1_LOCALE_DATA,bsParm);
						target = "forward:/GOLD/TRANSACTION/gold_applyconfirm";
						return target;
					}
				}
				bs.addData("ADOPID", "N09302");
				bs.addData("ACN", urlACN);
				model.addAttribute("gdperiod_article", bs);
				target = "/gold/gdperiod_article";
				return target;
			}
			
			
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs920 = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				if( ! bs920.getResult())
				{
					log.trace("bs.getResult() >>{}",bs920.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				log.debug("averaging_alter.cusidn >>{}", cusidn);
				try {
					String kyc = (String)SessionUtil.getAttribute(model,SessionUtil.KYCDATE,null);
					bs927 = gold_averaging_service.checkKYC(cusidn, reqParam, kyc, model);
					if(bs927 == null || !bs927.getResult()) {
						model.addAttribute(BaseResult.ERROR,bs927);
						return target;
					}
					else if(bs927 != null && bs927.getResult()) {
						Map<String, Object> dataMap = (Map<String, Object>) bs927.getData();
						if(((String)dataMap.get("Gd_InvAttr")).equals("N")) {
							model.addAttribute("TXID","N09302");
							target = "/gold/gd_inv_attr";
							return target;
						}
						
						//查N920
						bs920 = gold_averaging_service.averaging_alter(cusidn, reqParam);
						log.trace("bs920 DATA>>>>>{}",bs920.getData());
						if(bs920 != null && bs920.getResult()) {
							Map<String, String> bs920Data = (Map) bs920.getData();
							String getacn = String.valueOf(bs920Data.get("COUNT"));
							log.trace("SHOW COUNT>>>>>{}", getacn);
							if(StrUtils.isEmpty(getacn) || getacn.equals("0")) {
								//沒有黃金存摺帳號
								BaseResult bsParm = new BaseResult();
								bsParm.addData("TXID", "averaging_alter");
								bsParm.addData("MSGTYPE", "N920");
								SessionUtil.addAttribute(model,SessionUtil.STEP1_LOCALE_DATA,bsParm);
								target = "forward:/GOLD/TRANSACTION/gold_applyconfirm";
								return target;
							}
							
							bs926 = gold_averaging_service.N926_REST(cusidn, gold_averaging_service.GOLD_RESERVATION_QOERY);
							if(bs926 != null && bs926.getResult()) {
								Map<String, Object> bs926Data = (Map) bs926.getData();
								ArrayList<Map<String, String>> bs926rows = (ArrayList<Map<String, String>>) bs926Data.get("REC");
								//檢查是否有黃金存摺網路交易帳號
								int oacncount = 0;
								for (Map<String, String> row : bs926rows) 
								{
									if(row.get("ACN").length() == 11) {
										oacncount++;
									}
								}
								log.trace("bs926 ACN count>>>{}",oacncount);
								if(oacncount <= 0) {
									//沒有黃金存摺網路交易帳號
									BaseResult bsParm = new BaseResult();
									bsParm.addData("TXID", "averaging_alter");
									bsParm.addData("MSGTYPE", "N926");
									SessionUtil.addAttribute(model,SessionUtil.STEP1_LOCALE_DATA,bsParm);
									target = "forward:/GOLD/TRANSACTION/gold_applyconfirm";
									return target;
								}
								
								log.trace("averaging_alter bs>>>>>{}", bs920.getData());
								SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs920);
							}
							
							//IDGATE身分
							String idgateUserFlag="N";		 
							Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
							try {		 
								if(IdgateData == null) {
									IdgateData = new HashMap<String, Object>();
								}
								BaseResult tmp = idgateservice.checkIdentity(cusidn);		 
								if(tmp.getResult()) {		 
									idgateUserFlag="Y";                  		 
								}		 
								tmp.addData("idgateUserFlag",idgateUserFlag);		 
								IdgateData.putAll((Map<String, String>) tmp.getData());
								log.debug("test get data>>>"+tmp.getData());
								SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
							}catch(Exception e) {		 
								log.debug("idgateUserFlag error {}",e);		 
							}		 
							model.addAttribute("idgateUserFlag",idgateUserFlag);
						}
					}
				}
				catch(Exception e) {
					//Avoid Information Exposure Through an Error Message
					//e.printStackTrace();
					log.error("averaging_alter error >> {}",e);
				}
			}
		}
		catch(Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("averaging_alter error >> {}",e);
		}finally {
			if (bs920 != null && bs920.getResult())
			{
				bs920.addData("alert_ACN", urlACN);
				target = "/gold/averaging_alter";
				model.addAttribute("averaging_alter", bs920);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs920);
			}
		}
		
		return target;
	}
	
	/**
	 * N09302定期定額變更(確認頁)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/averaging_alter_confirm")
	public String averaging_alter_confirm(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		log.trace(ESAPIUtil.vaildLog("GET reqParam>>>>>{}"+CodeUtil.toJson(reqParam)));
		String target = "/error";
		BaseResult bs = null;
		
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			log.trace("CUSIDN>>>{}", cusidn);
			
			// IKEY要使用的JSON:DC
			String jsondc = null;
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("averaging_alter_confirm.jsondc: {}" + jsondc));
			
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				log.debug("averaging_alter_confirm.cusidn >>{}", cusidn);
				try {
					bs = gold_averaging_service.averaging_alter_confirm(cusidn, reqParam);
					if(bs != null && bs.getResult()) {
						bs.addData("jsondc", jsondc);
						log.trace("GET BS DATA>>>{}", bs.getData());
						SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
						
						Map<String, String> result = new HashMap<String, String>();
						result.putAll((Map)bs.getData());
						log.debug("test get map data>>>"+result.toString());
						// IDGATE transdata            		 
			            Map<String,Object>IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
			    		String adopid = "N09302";
			    		String title = "您有一筆黃金定期定額變更待確認";
			        	if(IdgateData != null) {
				            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
				            if(IdgateData.get("idgateUserFlag").equals("Y")) {
				            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
				            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
								IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N09302_IDGATE_DATA.class, N09302_IDGATE_DATA_VIEW.class, result));
				                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
				            }
			        	}
			            model.addAttribute("idgateAdopid", adopid);
			            model.addAttribute("idgateTitle", title);
					}
				}
				catch(Exception e) {
					//Avoid Information Exposure Through an Error Message
					//e.printStackTrace();
					log.error("averaging_alter_confirm error >> {}",e);
				}
			}
		}
		catch(Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("averaging_alter_confirm error >> {}",e);
		}
		finally {
			if (bs != null && bs.getResult())
			{
				target = "/gold/averaging_alter_confirm";
				model.addAttribute("averaging_alter_confirm", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		
		return target;
	}
	
	/**
	 * N09302定期定額變更(結果頁)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/averaging_alter_result")
	public String averaging_alter_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		log.trace(ESAPIUtil.vaildLog("averaging_alter_result GET reqParam>>>>>{}"+CodeUtil.toJson(reqParam)));
		String target = "/error";
		BaseResult bs = null;
		
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			log.trace("CUSIDN>>>{}", cusidn);
			
//			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			Map<String, String> okMap = reqParam;
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				log.debug("averaging_alter_result.cusidn >>{}", cusidn);
				//IDgate驗證		 
                try {		 
                    if(okMap.get("FGTXWAY").equals("7")) {		 

        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N09302_IDGATE");
                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
                    }		 
                }catch(Exception e) {		 
                    log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
                }
				Map<String,Object>IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
				model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
				try {
					bs = gold_averaging_service.averaging_alter_result(cusidn, okMap);
					if(bs != null && bs.getResult()) {
						log.trace("GET BS DATA>>>{}", bs.getData());
						SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
					}
				}
				catch(Exception e) {
					//Avoid Information Exposure Through an Error Message
					//e.printStackTrace();
					log.error("averaging_alter_result error >> {}",e);
				}
				
			}
		}
		catch(Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("averaging_alter_result error >> {}",e);
		}
		finally {
			if (bs != null && bs.getResult())
			{
				target = "/gold/averaging_alter_result";
				model.addAttribute("averaging_alter_result", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		
		return target;
	}
	
	// 取得帳號資料Ajax
	@RequestMapping(value = "/get_data_ajax", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody List<Map<String, String>> goldacn_ajax(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("get_data_ajax...");
		SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, "");
		List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();
		try {
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			String acn = reqParam.get("ACN");
			// 帳號資料
			resultList = gold_averaging_service.get_acn_data(acn, cusidn);
			log.trace("get resultList>>>>>{}",resultList.toString());
			BaseResult bs = new BaseResult();
			for(Map<String, String> row : resultList) {
				if(StrUtils.isNotEmpty(row.get("MSGCOD"))) {
					bs.setMsgCode(row.get("MSGCOD"));
					log.trace("add error msg>>>{}",bs.getMsgCode());
					bs.setMessage(row.get("msgName"));
					log.trace("add error msg>>>{}",bs.getMessage());
					SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
					model.addAttribute(BaseResult.ERROR, bs);
				}
				else if(StrUtils.isEmpty(row.get("SVACN"))) {
					bs.setMessage(i18n.getMsg("LB.X1773"));
					log.trace("add error msg>>>{}",bs.getMessage());
					bs.setMsgCode("");
					bs.setNext("/GOLD/AVERAGING/averaging_purchase");
					bs.setPrevious("/INDEX/index");
					SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
				}
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("goldacn_ajax error >> {}",e);
		}
		return resultList;
	}
	
	/**
	 * ERROR
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/g_error")
	public String error(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/gold/g_error";
		BaseResult bs = new BaseResult();
		bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null));
		model.addAttribute(BaseResult.ERROR, bs);
		return target;
	}

}
