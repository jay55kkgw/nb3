package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class NA71_REST_RQ extends BaseRestBean_OLS implements Serializable 
{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -9116252449076481360L;
	
	private String DATE;
	private String TIME;
	private String CUSIDN;
	private String TYPE;
	private String KIND;
	private String BNKRA;
	private String XMLCA;
	private String XMLCN;
	private String CHIP_ACN;
	private String ICSEQ;
	private String UID;
	private String FGTXWAY;
	private String jsondc;
	private String pkcs7Sign;
	private String ADOPID;
	private String ISSUER;
	private String ACNNO;
	private String OUTACN;
	private String iSeqNo;
	private String TAC;
	private String TRMID;
	
	//IDGATE
	private String sessionID;
	private String idgateID;
	private String txnID;

	public String getDATE() {
		return DATE;
	}

	public void setDATE(String dATE) {
		DATE = dATE;
	}

	public String getTIME() {
		return TIME;
	}

	public void setTIME(String tIME) {
		TIME = tIME;
	}

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}

	public String getTYPE() {
		return TYPE;
	}

	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}

	public String getKIND() {
		return KIND;
	}

	public void setKIND(String kIND) {
		KIND = kIND;
	}

	public String getBNKRA() {
		return BNKRA;
	}

	public void setBNKRA(String bNKRA) {
		BNKRA = bNKRA;
	}

	public String getXMLCA() {
		return XMLCA;
	}

	public void setXMLCA(String xMLCA) {
		XMLCA = xMLCA;
	}

	public String getXMLCN() {
		return XMLCN;
	}

	public void setXMLCN(String xMLCN) {
		XMLCN = xMLCN;
	}

	public String getCHIP_ACN() {
		return CHIP_ACN;
	}

	public void setCHIP_ACN(String cHIP_ACN) {
		CHIP_ACN = cHIP_ACN;
	}

	public String getICSEQ() {
		return ICSEQ;
	}

	public void setICSEQ(String iCSEQ) {
		ICSEQ = iCSEQ;
	}

	public String getUID() {
		return UID;
	}

	public void setUID(String uID) {
		UID = uID;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}

	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getJsondc() {
		return jsondc;
	}

	public void setJsondc(String jsondc) {
		this.jsondc = jsondc;
	}
	public String getPkcs7Sign() {
		return pkcs7Sign;
	}

	public void setPkcs7Sign(String pkcs7Sign) {
		this.pkcs7Sign = pkcs7Sign;
	}

	public String getADOPID() {
		return ADOPID;
	}

	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}

	public String getISSUER() {
		return ISSUER;
	}

	public void setISSUER(String iSSUER) {
		ISSUER = iSSUER;
	}

	public String getACNNO() {
		return ACNNO;
	}

	public void setACNNO(String aCNNO) {
		ACNNO = aCNNO;
	}

	public String getOUTACN() {
		return OUTACN;
	}

	public void setOUTACN(String oUTACN) {
		OUTACN = oUTACN;
	}

	public String getiSeqNo() {
		return iSeqNo;
	}

	public void setiSeqNo(String iSeqNo) {
		this.iSeqNo = iSeqNo;
	}

	public String getTAC() {
		return TAC;
	}

	public void setTAC(String tAC) {
		TAC = tAC;
	}

	public String getTRMID() {
		return TRMID;
	}

	public void setTRMID(String tRMID) {
		TRMID = tRMID;
	}

	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public String getIdgateID() {
		return idgateID;
	}

	public void setIdgateID(String idgateID) {
		this.idgateID = idgateID;
	}

	public String getTxnID() {
		return txnID;
	}

	public void setTxnID(String txnID) {
		this.txnID = txnID;
	}
	
}
