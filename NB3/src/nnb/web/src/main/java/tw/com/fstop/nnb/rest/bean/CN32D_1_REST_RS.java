package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class CN32D_1_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6171149874933960686L;
	private String MSGCOD;
	private String KEYLEN;//無卡提款密碼長度
	private String CMPW;//無卡提款密碼
	private String CUSIDN;//統一編號
	private String BNKCOD;//無卡提款行庫別
	private String ACN;//無卡提款帳號
	private String TRFLAG;//交易項目
	private String DATE;
	private String TIME;
	private String occurMsg;
	public String getMSGCOD() {
		return MSGCOD;
	}
	public void setMSGCOD(String mSGCOD) {
		MSGCOD = mSGCOD;
	}
	public String getKEYLEN() {
		return KEYLEN;
	}
	public void setKEYLEN(String kEYLEN) {
		KEYLEN = kEYLEN;
	}
	public String getCMPW() {
		return CMPW;
	}
	public void setCMPW(String cMPW) {
		CMPW = cMPW;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getBNKCOD() {
		return BNKCOD;
	}
	public void setBNKCOD(String bNKCOD) {
		BNKCOD = bNKCOD;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getTRFLAG() {
		return TRFLAG;
	}
	public void setTRFLAG(String tRFLAG) {
		TRFLAG = tRFLAG;
	}
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getOccurMsg() {
		return occurMsg;
	}
	public void setOccurMsg(String occurMsg) {
		this.occurMsg = occurMsg;
	}
}
