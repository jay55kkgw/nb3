package tw.com.fstop.nnb.spring.controller;

import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.service.FindFileChange;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;

@RestController
@RequestMapping(value = "/batch/com")
public class Batch_Controller {

	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	FindFileChange findfilechange ;
	
	@Autowired
	private ServletContext context;
	
	@RequestMapping(value = "/findfilechange" , method = {RequestMethod.POST} , produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody BaseResult findfilechange(HttpServletRequest request, HttpServletResponse response, @RequestBody List<String> args, Model model) {
		log.trace(ESAPIUtil.vaildLog("args>>{}"+CodeUtil.toJson(args)));
		BaseResult bs = new BaseResult();
		try {
			bs = findfilechange.findChange(context.getContextPath());
		} catch (Throwable e) {
			log.error("Throwable>>{}",e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", "批次執行異常");
		}
		return bs;
		
	}
	@RequestMapping(value = "/isLive" , method = {RequestMethod.POST} , produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody BaseResult isLive(HttpServletRequest request, HttpServletResponse response, @RequestBody List<String> args, Model model) {
		log.trace(ESAPIUtil.vaildLog("args>>{}"+CodeUtil.toJson(args)));
		BaseResult bs = new BaseResult();
		try {
			
			if(args!=null && "AP1".equals(args.get(0)) && "false".equals(args.get(1)) ) {
				CodeUtil.IS_AP1_LIVE = Boolean.FALSE;
			}else {
				CodeUtil.IS_AP1_LIVE = Boolean.TRUE;
			}
			if(args!=null && "AP2".equals(args.get(0)) && "false".equals(args.get(1)) ) {
				CodeUtil.IS_AP2_LIVE = Boolean.FALSE;
			}else {
				CodeUtil.IS_AP2_LIVE = Boolean.TRUE;
			}
			bs.setResult(Boolean.TRUE);
			bs.setMsgCode("0");
			bs.setMessage("ok");
		} catch (Throwable e) {
			log.error("Throwable>>{}",e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", "批次執行異常");
		}
		return bs;
		
	}
	
	
	
	@RequestMapping(value = "/echo" , method = {RequestMethod.POST} , produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody BaseResult echo(HttpServletRequest request, HttpServletResponse response, @RequestBody List<String> args, Model model) {
		log.trace("reqParam>>{}" ,ESAPIUtil.vaildLog(CodeUtil.toJson(args)));
		BaseResult bs = new BaseResult();
		try {
			bs.setResult(Boolean.TRUE);
			
			// CodeUtil.convert2BaseResult(bs, obj);
			bs.setMessage("0", "ok");
		} catch (Exception e) {
			log.error(e.toString());
			bs.setResult(Boolean.FALSE);
		}
		return bs;
		
	}
	

	
	
}
