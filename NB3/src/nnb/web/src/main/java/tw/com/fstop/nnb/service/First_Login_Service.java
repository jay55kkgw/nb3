package tw.com.fstop.nnb.service;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.po.ADMLOGIN;
import fstop.orm.po.OLD_NB3USER;
import fstop.orm.po.OLD_TXNADDRESSBOOK;
import fstop.orm.po.OLD_TXNCUSINVATTRHIST;
import fstop.orm.po.OLD_TXNCUSINVATTRIB;
import fstop.orm.po.OLD_TXNFXSCHEDULE;
import fstop.orm.po.OLD_TXNGDRECORD;
import fstop.orm.po.OLD_TXNLOG;
import fstop.orm.po.OLD_TXNPHONETOKEN;
import fstop.orm.po.OLD_TXNTRACCSET;
import fstop.orm.po.OLD_TXNTWSCHEDULE;
import fstop.orm.po.OLD_TXNUSER;
import fstop.orm.po.TRNS_NNB_CUSDATA;
import fstop.orm.po.TXNADDRESSBOOK;
import fstop.orm.po.TXNCUSINVATTRHIST;
import fstop.orm.po.TXNCUSINVATTRIB;
import fstop.orm.po.TXNFXRECORD;
import fstop.orm.po.TXNFXSCHPAY;
import fstop.orm.po.TXNGDRECORD;
import fstop.orm.po.TXNPHONETOKEN;
import fstop.orm.po.TXNTRACCSET;
import fstop.orm.po.TXNTWRECORD;
import fstop.orm.po.TXNTWSCHPAY;
import fstop.orm.po.TXNUSER;
import tw.com.fstop.tbb.nnb.dao.AdmLoginDao;
import tw.com.fstop.tbb.nnb.dao.Old_Nb3UserDao;
import tw.com.fstop.tbb.nnb.dao.Old_TxnAddressBookDao;
import tw.com.fstop.tbb.nnb.dao.Old_TxnCusInvAttrHistDao;
import tw.com.fstop.tbb.nnb.dao.Old_TxnCusInvAttribDao;
import tw.com.fstop.tbb.nnb.dao.Old_TxnFxRecordDao;
import tw.com.fstop.tbb.nnb.dao.Old_TxnFxScheduleDao;
import tw.com.fstop.tbb.nnb.dao.Old_TxnGdRecordDao;
import tw.com.fstop.tbb.nnb.dao.Old_TxnLogDao;
import tw.com.fstop.tbb.nnb.dao.Old_TxnPhoneTokenDao;
import tw.com.fstop.tbb.nnb.dao.Old_TxnRecMailLogRelationDao;
import tw.com.fstop.tbb.nnb.dao.Old_TxnTrAccSetDao;
import tw.com.fstop.tbb.nnb.dao.Old_TxnTwRecordDao;
import tw.com.fstop.tbb.nnb.dao.Old_TxnTwScheduleDao;
import tw.com.fstop.tbb.nnb.dao.Old_TxnUserDao;
import tw.com.fstop.tbb.nnb.dao.Trns_nnb_CusDataDao;
import tw.com.fstop.tbb.nnb.dao.TxnAddressBookDao;
import tw.com.fstop.tbb.nnb.dao.TxnCusInvAttrHistDao;
import tw.com.fstop.tbb.nnb.dao.TxnCusInvAttribDao;
import tw.com.fstop.tbb.nnb.dao.TxnFxRecordDao;
import tw.com.fstop.tbb.nnb.dao.TxnFxSchPayDao;
import tw.com.fstop.tbb.nnb.dao.TxnFxSchPayDataDao;
import tw.com.fstop.tbb.nnb.dao.TxnFxSchPayData_ECDao;
import tw.com.fstop.tbb.nnb.dao.TxnGdRecordDao;
import tw.com.fstop.tbb.nnb.dao.TxnPhoneTokenDao;
import tw.com.fstop.tbb.nnb.dao.TxnRecMailLogRelationDao;
import tw.com.fstop.tbb.nnb.dao.TxnTrAccSetDao;
import tw.com.fstop.tbb.nnb.dao.TxnTwRecordDao;
import tw.com.fstop.tbb.nnb.dao.TxnTwSchPayDao;
import tw.com.fstop.tbb.nnb.dao.TxnTwSchPayDataDao;
import tw.com.fstop.tbb.nnb.dao.TxnTwSchPayData_ECDao;
import tw.com.fstop.tbb.nnb.dao.TxnUserDao;
import tw.com.fstop.tbb.nnb.util.DateUtil;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;
import tw.com.fstop.tbb.nnb.util.StrUtils;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.SpringBeanFactory;

@Service
public class First_Login_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private AdmLoginDao admlogindao;
	
	// 電郵記錄檔--20200316 不即時同步，改用批次
//	@Autowired
//	private Old_AdmMailContentDao old_admmailcontentdao;
//	@Autowired
//	private Old_AdmMailLogDao old_admmaillogdao;
	@Autowired
	private Old_TxnAddressBookDao old_txnaddressbookdao;
	@Autowired
	private Old_TxnCusInvAttribDao old_txncusinvattribdao;
	@Autowired
	private Old_TxnCusInvAttrHistDao old_txncusinvattrhistdao;
	@Autowired
	private Old_TxnFxRecordDao old_txnfxrecorddao;
	@Autowired
	private Old_TxnFxScheduleDao old_txnfxscheduledao;
	@Autowired
	private Old_TxnGdRecordDao old_txngdrecorddao;
	@Autowired
	private Old_TxnLogDao old_txnlogdao;
	// ADMMAILLOG--電郵記錄檔--20200316 不即時同步，改用批次
//	@Autowired
//	private Old_TxnRecMailLogRelationDao old_txnrecmaillogrelationdao;
//	@Autowired
//	private Old_TxnReqInfoDao old_txnreqinfodao;
	@Autowired
	private Old_TxnTrAccSetDao old_txntraccsetdao;
	@Autowired
	private Old_TxnTwRecordDao old_txntwrecorddao;
	@Autowired
	private Old_TxnTwScheduleDao old_txntwscheduledao;
	@Autowired
	private Old_TxnUserDao old_txnuserdao;
	@Autowired
	private Old_TxnPhoneTokenDao old_txnphonetokendao;
	@Autowired
	private Old_Nb3UserDao old_nb3userdao;
	
	// 電郵記錄檔--20200316 不即時同步，改用批次
//	@Autowired
//	private AdmMailLogDao admmaillogdao;
	@Autowired
	private TxnAddressBookDao txnaddressbookdao;
	@Autowired
	private TxnUserDao txnuserdao;
	@Autowired
	private TxnPhoneTokenDao txnphonetokendao;
	@Autowired
	private TxnTrAccSetDao txntraccsetdao;
	@Autowired
	private TxnCusInvAttribDao txncusinvattribdao;
	@Autowired
	private TxnCusInvAttrHistDao txncusinvattrhistdao;
	@Autowired
	private TxnFxRecordDao txnfxrecorddao;
	@Autowired
	private TxnFxSchPayDao txnfxschpaydao;
	@Autowired
	private TxnFxSchPayDataDao txnfxschpaydatadao;
	@Autowired
	private TxnGdRecordDao txngdrecorddao;
	@Autowired
	private TxnTwRecordDao txntwrecorddao;
	@Autowired
	private TxnTwSchPayDao txntwschpaydao;
	@Autowired
	private TxnTwSchPayDataDao txntwschpaydatadao;
	// ADMMAILLOG--電郵記錄檔--20200316 不即時同步，改用批次
//	@Autowired
//	private TxnRecMailLogRelationDao txnrecmaillogrelationdao;
	@Autowired
	private Trns_nnb_CusDataDao trns_nnb_cusdatadao;
	@Autowired
	private TxnTwSchPayData_ECDao txntwschpaydata_ecdao;
	@Autowired
	private TxnFxSchPayData_ECDao txnfxschpaydata_ecdao;
	
	
	/**
	 * 使用者是否曾經登入過新網銀
	 * @param cusidn 使用者統編
	 * @return
	 */
	public Boolean everLogin(String cusidn) {
		boolean result = false;
		List<ADMLOGIN> admloginList = null;
		try {
			log.debug(ESAPIUtil.vaildLog("First_Login_Service.everLogin.cusidn: {}"+ cusidn));
			
			// 取得登入時資訊
			admloginList = admlogindao.findByUserId(cusidn);
			// 曾經登入過
			if(admloginList!=null && !admloginList.isEmpty()) {
				result = true;
			}
		} catch (Exception e) {
			log.error("", e);
		}
		
		return result;
	}
	
	/**
	 * 紀錄使用者資料轉檔結果
	 * @param cusidn 使用者統編
	 * @return
	 */
	public TRNS_NNB_CUSDATA cusdataRecord(String cusidn) {
		TRNS_NNB_CUSDATA trns_nnb_cusdata = null;
		try {
			// 取得轉檔結果
			List<TRNS_NNB_CUSDATA> cusdataList = trns_nnb_cusdatadao.findByUserId(cusidn);
			if(cusdataList != null && !cusdataList.isEmpty()) {
				trns_nnb_cusdata = cusdataList.get(0);
			} else {
				trns_nnb_cusdata = new TRNS_NNB_CUSDATA();
			}
			
			trns_nnb_cusdata.setCUSIDN(cusidn);
			
		} catch (Exception e) {
			log.error("", e);
		}
		
		return trns_nnb_cusdata;
	}
	
	/**
	 * 使用者首次登入新個網資料轉檔結果
	 * @param cusidn 使用者統編
	 * @return
	 */
	public Boolean firstCheck(String cusidn) {
		boolean result = false;
		List<TRNS_NNB_CUSDATA> cusdataList = null;
		try {
			log.debug(ESAPIUtil.vaildLog("First_Login_Service.everLogin.cusidn: {}"+ cusidn));
			
			// 取得登入時資訊
			cusdataList = trns_nnb_cusdatadao.findByUserId(cusidn);
			// 曾經登入過
			if(cusdataList != null && !cusdataList.isEmpty() && cusdataList.get(0) != null) {
				TRNS_NNB_CUSDATA po = cusdataList.get(0);
				result = po.checkCus(); // 判斷之前登入時轉檔是否成功
			}
		} catch (Exception e) {
			log.error("", e);
		}
		
		return result;
	}
	
	/**
	 * 判斷是否於預約交易排程執行時間之間(07:00 a.m. ~ 10:00 a.m.)
	 * @param cusidn 使用者統編
	 * @return
	 */
	public Boolean nb3SchCheck(String cusidn) {
		boolean result = true;
		boolean tw_result = false;
		boolean fx_result = false;
		
		try {
			// 如果使用者沒有預約資料則視為白名單不驗證轉檔時間
			List<OLD_TXNTWSCHEDULE> old_txntwschedule_list = old_txntwscheduledao.findByUserId(cusidn);
			if(old_txntwschedule_list!=null) {
				// 舊網銀臺幣預約檔沒有預約資料
				if(old_txntwschedule_list.isEmpty()) {
					tw_result = true;
				}
			}
			List<OLD_TXNFXSCHEDULE> old_txnfxschedule_list = old_txnfxscheduledao.findByUserId(cusidn);
			if(old_txnfxschedule_list!=null) {
				// 舊網銀外幣預約檔沒有預約資料
				if(old_txnfxschedule_list.isEmpty()) {
					fx_result = true;
				}
			}
			if (tw_result && fx_result) {
				result = true;
				return result;
			}
			
			// 系統時間 in 預約排程執行時間
			String str_CurrentTime = DateUtil.getTheTime("");
			String str_BizStart ="070000";
			String str_BizEnd = "100000";			
			if ((str_CurrentTime.compareTo(str_BizStart) > 0) && (str_CurrentTime.compareTo(str_BizEnd) < 0)) {				
				result = false;
			}
		
		} catch (Exception e) {
			log.error("", e);
		}
		
		return result;
	}
	
	/**
	 * 判斷舊網銀是否有正在進行的預約交易排程
	 * @param cusidn 使用者統編
	 * @return
	 */
	public Boolean nnbSchCheck(String cusidn) {
		boolean result = false;
		try {
			log.debug(ESAPIUtil.vaildLog("First_Login_Service.everLogin.cusidn: {}"+ cusidn));
			
			// 舊網銀臺幣預約檔
			List<OLD_TXNTWSCHEDULE> old_txntwschedule_list = old_txntwscheduledao.findByUserId(cusidn);
			if(old_txntwschedule_list!=null) {
				// 沒有預約資料
				if(old_txntwschedule_list.isEmpty()) {
					result = true;
				} else {
					// 逐筆作判斷
					for(OLD_TXNTWSCHEDULE old_txntwschedule : old_txntwschedule_list) {
						if( !"1".equals(old_txntwschedule.getDPTXSTATUS()) && !"2".equals(old_txntwschedule.getDPTXSTATUS()) ) {
							// 沒有正在執行交易的預約資料
							result = true;
						}
					}
				}
			}
			
			// 舊網銀外幣預約檔
			List<OLD_TXNFXSCHEDULE> old_txnfxschedule_list = old_txnfxscheduledao.findByUserId(cusidn);
			if(old_txnfxschedule_list!=null) {
				// 沒有預約資料
				if(old_txnfxschedule_list.isEmpty()) {
					result = true;
				} else {
					// 逐筆作判斷
					for(OLD_TXNFXSCHEDULE old_txnfxschedule : old_txnfxschedule_list) {
						if( !"1".equals(old_txnfxschedule.getFXTXSTATUS()) && !"2".equals(old_txnfxschedule.getFXTXSTATUS()) ) {
							// 沒有正在執行交易的預約資料
							result = true;
						}
					}
				}
			}
			
		} catch (Exception e) {
			log.error("", e);
		}
		
		return result;
	}
	
	/**
	 * 避免前一次登入轉檔失敗的可能，轉檔前先清除原本的資料
	 * @param cusidn 使用者統編
	 * @return
	 */
	public Boolean firstClear(String cusidn) {
		boolean result = false;
		try {
			log.debug(ESAPIUtil.vaildLog("First_Login_Service.firstClear.cusidn: {}"+ cusidn));
			
			// 轉檔結果初始化
			trns_nnb_cusdatadao.deleteByUserId(cusidn);
			
			// TXNUSER--使用者設定檔
			txnuserdao.deleteByUserId(cusidn);
			// TXNTWRECORD--臺幣轉帳結果檔
//			txntwrecorddao.deleteByUserId(cusidn);
			// TXNTWSCHPAY--臺幣預約交易設定檔
//			txntwschpaydao.deleteByUserId(cusidn); // 20201217:副科決議不清預約資料
			// TXNTWSCHPAYDATA--臺幣預約交易設定檔
//			txntwschpaydatadao.deleteByUserId(cusidn); // 20201217:副科決議不清預約資料
			// TXNFXRECORD--外匯轉帳結果檔
//			txnfxrecorddao.deleteByUserId(cusidn);
			// TXNFXSCHPAY--外幣預約交易設定檔
//			txnfxschpaydao.deleteByUserId(cusidn); // 20201217:副科決議不清預約資料
			// TXNFXSCHPAYDATA--外幣預約交易設定檔
//			txnfxschpaydatadao.deleteByUserId(cusidn); // 20201217:副科決議不清預約資料
			// TXNGDRECORD--黃金交易結果檔
			txngdrecorddao.deleteByUserId(cusidn);
			// TXNTRACCSET--常用轉入帳號設定檔
			txntraccsetdao.deleteByUserId(cusidn);
			// TXNADDRESSBOOK--使用者通訊錄檔
			txnaddressbookdao.deleteByUserId(cusidn);
			// TXNCUSINVATTRIB--基金客戶投資屬性線上調查紀錄檔
			txncusinvattribdao.deleteByUserId(cusidn);
			// TXNCUSINVATTRHIST--基金客戶投資屬性線上調查紀錄歷史檔
			txncusinvattrhistdao.deleteByUserId(cusidn);
			// ADMMAILLOG--電郵記錄檔--20200316 不即時同步，改用批次
//			admmaillogdao.deleteByUserId(cusidn);
			// TXNPHONETOKEN--使用者手機推撥設定檔
			txnphonetokendao.deleteByUserId(cusidn);
			// TXNTWSCHPAYDATA_EC
//			txntwschpaydata_ecdao.deleteByUserId(cusidn); // 20201217:副科決議不清預約資料
			// TXNFXSCHPAYDATA_EC
//			txnfxschpaydata_ecdao.deleteByUserId(cusidn); // 20201217:副科決議不清預約資料
			
			result = true;
			
			log.debug("First_Login_Service.firstClear.finish!");
			
		} catch (Exception e) {
			log.error("", e);
		}
		
		return result;
	}
	
	
	/**
	 * ADMMAILLOG--舊網銀轉新網銀--20200316 不即時同步，改用批次
	 * @param cusidn 使用者統編
	 * @return
	 */
//	public boolean admmaillog_tx(String cusidn) {
//		boolean result = false;
//		try {
//			log.trace("First_Login_Service.admmaillog_tx.cusidn: {}", cusidn);
//			
//			// 取得使用者所有資料
//			List<ADMMAILLOG> admmaillog_list = admmaillog_q(cusidn);
//			
//			// 舊網銀有使用者資料
//			if(admmaillog_list != null && !admmaillog_list.isEmpty()) {
//				// 逐筆作轉檔
//				for(ADMMAILLOG admmaillog : admmaillog_list) {
//					Long old_ADMAILLOGID = admmaillog.getADMAILLOGID();
//					admmaillog.setADMAILLOGID(null);
//					admmaillogdao.saveOrUpdate(admmaillog);
//					
//					List<TXNRECMAILLOGRELATION> recList = old_txnrecmaillogrelationdao.findByMailId(old_ADMAILLOGID);
//					if(!recList.isEmpty()) {
//						TXNRECMAILLOGRELATION txnrecmaillogrelation = recList.get(0);
//						txnrecmaillogrelation.setRELID(null);
//						txnrecmaillogrelation.setADMAILLOGID(admmaillog.getADMAILLOGID());
//						txnrecmaillogrelation.setCUSIDN(cusidn);
//						txnrecmaillogrelationdao.save(txnrecmaillogrelation);
//					}
//				}
//			}
//			
//			result = true;
//			
//		} catch (Exception e) {
//			log.error("", e);
//		}
//		
//		return result;
//	}
	
	/**
	 * TXNPHONETOKEN--舊網銀轉新網銀
	 * @param cusidn 使用者統編
	 * @return
	 */
	public boolean txnphonetoken_tx(String cusidn) {
		boolean result = false;
		try {
			log.trace("First_Login_Service.txnphonetoken_tx.cusidn: {}", cusidn);
			
			// 取得使用者所有資料
			List<OLD_TXNPHONETOKEN> old_txnphonetoken_list = txnphonetoken_q(cusidn);
			
			// 舊網銀有使用者資料
			if(old_txnphonetoken_list!=null) {
				// 逐筆作轉檔
				for(OLD_TXNPHONETOKEN old_txnphonetoken : old_txnphonetoken_list) {
					TXNPHONETOKEN txnphonetoken = new TXNPHONETOKEN();
					txnphonetoken = CodeUtil.objectCovert(TXNPHONETOKEN.class, old_txnphonetoken);
					txnphonetokendao.saveOrUpdate(txnphonetoken);
				}
				result = true;
			}
			
		} catch (Exception e) {
			log.error("", e);
		}
		
		return result;
	}
	
	
	/**
	 * 查詢舊網銀ADMMAILLOG資料--20200316 不即時同步，改用批次
	 * @param admaillogid
	 * @return
	 */
//	public List<ADMMAILLOG> admmaillog_q(String admaillogid) {
//		List<ADMMAILLOG> admmaillog_list = null;
//		try {
//			log.trace("First_Login_Service.admmaillog_q.admaillogid: {}", admaillogid);
//			
//			// 取得使用者所有資料
//			admmaillog_list = old_admmaillogdao.findMailLogById(admaillogid);
//			
//		} catch (Exception e) {
//			log.error("", e);
//		}
//		
//		return admmaillog_list;
//	}
	
	/**
	 * 查詢舊網銀TXNPHONETOKEN資料
	 * @param dpuserid
	 * @return
	 */
	public List<OLD_TXNPHONETOKEN> txnphonetoken_q(String dpuserid) {
		List<OLD_TXNPHONETOKEN> txnphonetoken_list = null;
		try {
			log.trace("First_Login_Service.txnphonetoken_q.dpuserid: {}", dpuserid);
			
			// 取得使用者所有資料
			txnphonetoken_list = old_txnphonetokendao.findByUserId(dpuserid);
			
		} catch (Exception e) {
			log.error("", e);
		}
		
		return txnphonetoken_list;
	}
	
	
	/**
	 * TXNADDRESSBOOK--舊網銀轉新網銀
	 * @param cusidn 使用者統編
	 * @return
	 */
	public boolean txnaddressbook_tx(String cusidn) {
		boolean result = false;
		try {
			log.trace("First_Login_Service.txnaddressbook_tx.cusidn: {}", cusidn);
			
			// 取得使用者所有資料
			List<OLD_TXNADDRESSBOOK> old_txnaddressbook_list = txnaddressbook_q(cusidn);
			
			// 舊網銀有使用者資料
			if(old_txnaddressbook_list!=null) {
				// 逐筆作轉檔
				for(OLD_TXNADDRESSBOOK old_txnaddressbook : old_txnaddressbook_list) {
					TXNADDRESSBOOK txnaddressbook = new TXNADDRESSBOOK();
					txnaddressbook = CodeUtil.objectCovert(TXNADDRESSBOOK.class, old_txnaddressbook);
					txnaddressbook.setDPADDBKID(null);
					txnaddressbookdao.saveOrUpdate(txnaddressbook);
				}
				result = true;
			}
			
			
		} catch (Exception e) {
			log.error("", e);
		}
		
		return result;
	}
	
	/**
	 * 查詢舊網銀TXNADDRESSBOOK資料
	 * @param dpaddbkid
	 * @return
	 */
	public List<OLD_TXNADDRESSBOOK> txnaddressbook_q(String dpaddbkid) {
		List<OLD_TXNADDRESSBOOK> old_txnaddressbook_list = null;
		try {
			log.trace("First_Login_Service.txnaddressbook_q.dpaddbkid: {}", dpaddbkid);
			
			// 取得使用者所有資料
			old_txnaddressbook_list = old_txnaddressbookdao.findByUserId(dpaddbkid);
			
		} catch (Exception e) {
			log.error("", e);
		}
		
		return old_txnaddressbook_list;
	}
	
	/**
	 * TXNCUSINVATTRIB--舊網銀轉新網銀
	 * @param cusidn 使用者統編
	 * @return
	 */
	public boolean txncusinvattrhist_tx(String cusidn) {
		boolean result = false;
		try {
			log.trace("First_Login_Service.txncusinvattrhist_tx.cusidn: {}", cusidn);
			
			// 取得使用者所有資料
			List<OLD_TXNCUSINVATTRHIST> old_txncusinvattrhist_list = txncusinvattrhist_q(cusidn);
			
			// 舊網銀有使用者資料
			if(old_txncusinvattrhist_list!= null) {
				// 逐筆作轉檔
				for(OLD_TXNCUSINVATTRHIST old_txncusinvattrhist : old_txncusinvattrhist_list) {
					TXNCUSINVATTRHIST txncusinvattrhist = new TXNCUSINVATTRHIST();
					txncusinvattrhist = CodeUtil.objectCovert(TXNCUSINVATTRHIST.class, old_txncusinvattrhist);
					txncusinvattrhist.setFDHISTID(null);
					txncusinvattrhistdao.saveOrUpdate(txncusinvattrhist);
				}
				result = true;
			}
			
			
		} catch (Exception e) {
			log.error("", e);
		}
		
		return result;
	}
	
	/**
	 * 查詢舊網銀TXNCUSINVATTRIB資料
	 * @param fduserid
	 * @return
	 */
	public List<OLD_TXNCUSINVATTRHIST> txncusinvattrhist_q(String fduserid) {
		List<OLD_TXNCUSINVATTRHIST> txncusinvattrhist_list = null;
		try {
			log.trace("First_Login_Service.txncusinvattrhist_q.fduserid: {}", fduserid);
			
			// 取得使用者所有資料
			txncusinvattrhist_list = old_txncusinvattrhistdao.findByUserId(fduserid);
			
		} catch (Exception e) {
			log.error("", e);
		}
		
		return txncusinvattrhist_list;
	}
	
	/**
	 * TXNCUSINVATTRIB--舊網銀轉新網銀
	 * @param cusidn 使用者統編
	 * @return
	 */
	public boolean txncusinvattrib_tx(String cusidn) {
		boolean result = false;
		try {
			log.trace("First_Login_Service.txncusinvattrib_tx.cusidn: {}", cusidn);
			
			// 取得使用者所有資料
			List<OLD_TXNCUSINVATTRIB> old_txncusinvattrib_list = txncusinvattrib_q(cusidn);
			
			// 舊網銀有使用者資料
			if(old_txncusinvattrib_list!=null) {
				// 逐筆作轉檔
				for(OLD_TXNCUSINVATTRIB old_txncusinvattrib : old_txncusinvattrib_list) {
					TXNCUSINVATTRIB txncusinvattrib = new TXNCUSINVATTRIB();
					txncusinvattrib = CodeUtil.objectCovert(TXNCUSINVATTRIB.class, old_txncusinvattrib);
					txncusinvattribdao.saveOrUpdate(txncusinvattrib);
				}
				result = true;
			}
			
			
		} catch (Exception e) {
			log.error("", e);
		}
		
		return result;
	}
	
	/**
	 * 查詢舊網銀TXNCUSINVATTRIB資料
	 * @param fduserid
	 * @return
	 */
	public List<OLD_TXNCUSINVATTRIB> txncusinvattrib_q(String fduserid) {
		List<OLD_TXNCUSINVATTRIB> old_txncusinvattrib_list = null;
		try {
			log.trace("First_Login_Service.txncusinvattrib_q.fduserid: {}", fduserid);
			
			// 取得使用者所有資料
			old_txncusinvattrib_list = old_txncusinvattribdao.findByUserId(fduserid);
			
		} catch (Exception e) {
			log.error("", e);
		}
		
		return old_txncusinvattrib_list;
	}
	
	/**
	 * TXNFXRECORD--舊網銀轉新網銀
	 * @param cusidn 使用者統編
	 * @return
	 */
	public boolean txnfxrecord_tx(String cusidn) {
		boolean result = false;
		try {
			log.trace("First_Login_Service.txnfxrecord_tx.cusidn: {}", cusidn);
			
			// 取得使用者所有資料
			List<TXNFXRECORD> old_txnfxrecord_list = txnfxrecord_q(cusidn);
			
			// 舊網銀有使用者資料
			if(old_txnfxrecord_list!=null) {
				// 逐筆作轉檔
				for(TXNFXRECORD txnfxrecord : old_txnfxrecord_list) {
					// 20201217--不清資料，若有重複的ADTXNO就不INSERT
					List<String> adtxNoList = txnfxrecorddao.getAdtxNoByUser(cusidn);
					
					if (adtxNoList != null && !adtxNoList.isEmpty()) {
						log.info(ESAPIUtil.vaildLog("txnfxrecord_tx.adtxNoList: " + adtxNoList)); // NB3 交易編號清單
						
						String adtxno = txnfxrecord.getADTXNO(); // 2.5 交易編號清單
						log.info(ESAPIUtil.vaildLog("txnfxrecord_tx.adtxno: " + adtxno));
						
						// 如果有包含一樣的交易編號就跳過不INSERT
						if (adtxNoList.contains(adtxno)) {
							continue;
						}
					}
					
					txnfxrecorddao.saveOrUpdate(txnfxrecord);
				}
				result = true;
			}
			
		} catch (Exception e) {
			log.error("", e);
		}
		
		return result;
	}
	
	/**
	 * 查詢舊網銀TXNFXRECORD資料
	 * @param adtxno
	 * @return
	 */
	public List<TXNFXRECORD> txnfxrecord_q(String adtxno) {
		List<TXNFXRECORD> old_txnfxrecord_list = null;
		try {
			log.trace("First_Login_Service.txnfxrecord_q.adtxno: {}", adtxno);
			
			// 取得使用者所有資料
			old_txnfxrecord_list = old_txnfxrecorddao.findById(adtxno);
			
		} catch (Exception e) {
			log.error("", e);
		}
		
		return old_txnfxrecord_list;
	}
	
	
	/**
	 * TXNFXRECORD--舊網銀轉新網銀
	 * @param cusidn 使用者統編
	 * @return
	 */
	public boolean txnfxschedule_tx(String cusidn) {
		boolean result = false;
		try {
			log.trace("First_Login_Service.txnfxschedule_tx.cusidn: {}", cusidn);
			
			// 取得使用者所有資料
			List<TXNFXSCHPAY> old_txnfxschedule_list = txnfxschedule_q(cusidn);
			
			// 舊網銀有使用者資料
			if(old_txnfxschedule_list!=null) {
				// 逐筆作轉檔
				for(TXNFXSCHPAY txnfxschpay : old_txnfxschedule_list) {
					// 先更新舊網銀資料，未執行:0->9
					if ("0".equals(txnfxschpay.getFXTXSTATUS())) {
						old_txnfxscheduledao.updateByPK(Long.valueOf(txnfxschpay.getFXSCHID()), "9");
					}
					
					// 若前次轉檔有失敗，會撈到9要改成0
					if ("9".equals(txnfxschpay.getFXTXSTATUS())) {
						txnfxschpay.setFXTXSTATUS("0");
					}
					
					
					// 20201217--不清資料，若有重複的DPSCHNO就不INSERT
					List<String> fxschNoList = txnfxschpaydao.getFxschNoByUser(cusidn);
					
					if (fxschNoList != null && !fxschNoList.isEmpty()) {
						log.info(ESAPIUtil.vaildLog("txnfxschedule_tx.fxschNoList: " + fxschNoList)); // NB3 預約編號清單
						
						String fxschno = txnfxschpay.getFXSCHNO(); // 2.5 預約編號清單
						log.info(ESAPIUtil.vaildLog("txnfxschedule_tx.fxschno: " + fxschno));
						
						// 如果有包含一樣的預約編號就跳過不INSERT
						if (fxschNoList.contains(fxschno)) {
							continue;
						}
					}
					
					// 轉移資料至新網銀
					log.debug("txnfxschpay.FXSCHID.trace: {}", txnfxschpay.getFXSCHID());
					txnfxschpay.setFXSCHID(null);
					txnfxschpaydao.saveOrUpdate(txnfxschpay);
				}
				result = true;
			}
			
		} catch (Exception e) {
			log.error("", e);
		}
		
		return result;
	}
	
	/**
	 * NB3USER--舊網銀轉新網銀
	 * @param cusidn 使用者統編
	 * @return
	 */
	public boolean nb3user_tx(String cusidn) {
		boolean result = false;
		try {
			log.trace("First_Login_Service.nb3user_tx.cusidn: {}", cusidn);
			
			// 新增舊網銀使用者資料
			OLD_NB3USER old_nb3user = new OLD_NB3USER();
			old_nb3user.setNB3USERID(cusidn);
			old_nb3user.setNB3STATUS("");
			old_nb3user.setLASTDATE(DateUtil.getCurentDateTime("yyyyMMdd"));
			old_nb3user.setLASTTIME(DateUtil.getCurentDateTime("HHmmss"));
			
			old_nb3userdao.saveOrUpdate(old_nb3user);
			
			result = true;
			
		} catch (Exception e) {
			log.error("", e);
		}
		
		return result;
	}
	
	/**
	 * 查詢舊網銀TXNFXSCHEDULE資料
	 * @param fxschid
	 * @return
	 */
	public List<TXNFXSCHPAY> txnfxschedule_q(String fxschid) {
		List<TXNFXSCHPAY> old_txnfxschedule_list = null;
		try {
			log.trace("First_Login_Service.txnfxschedule_q.fxschid: {}", fxschid);
			
			// 取得使用者所有資料
			old_txnfxschedule_list = old_txnfxscheduledao.findById(fxschid);
			
		} catch (Exception e) {
			log.error("", e);
		}
		
		return old_txnfxschedule_list;
	}
	
	/**
	 * TXNGDRECORD--舊網銀轉新網銀
	 * @param cusidn 使用者統編
	 * @return
	 */
	public boolean txngdrecord_tx(String cusidn) {
		boolean result = false;
		try {
			log.trace("First_Login_Service.txngdrecord_tx.cusidn: {}", cusidn);
			
			// 取得使用者所有資料
			List<OLD_TXNGDRECORD> old_txngdrecord_list = txngdrecord_q(cusidn);
			
			// 舊網銀有使用者資料
			if(old_txngdrecord_list!=null) {
				// 逐筆作轉檔
				for(OLD_TXNGDRECORD old_txngdrecord : old_txngdrecord_list) {
					TXNGDRECORD txngdrecord = new TXNGDRECORD();
					txngdrecord = CodeUtil.objectCovert(TXNGDRECORD.class, old_txngdrecord);
					txngdrecorddao.saveOrUpdate(txngdrecord);
				}
				result = true;
			}
			
			
		} catch (Exception e) {
			log.error("", e);
		}
		
		return result;
	}
	
	/**
	 * 查詢舊網銀TXNGDRECORD資料
	 * @param adtxno
	 * @return
	 */
	public List<OLD_TXNGDRECORD> txngdrecord_q(String adtxno) {
		List<OLD_TXNGDRECORD> old_txngdrecord_list = null;
		try {
			log.trace("First_Login_Service.txngdrecord_q.adtxno: {}", adtxno);
			
			// 取得使用者所有資料
			old_txngdrecord_list = old_txngdrecorddao.findByUserId(adtxno);
			
		} catch (Exception e) {
			log.error("", e);
		}
		
		return old_txngdrecord_list;
	}
	
	
	/**
	 * 查詢舊網銀TXNLOG資料
	 * @param adtxno
	 * @return
	 */
	public List<OLD_TXNLOG> txnlog_q(String adtxno) {
		List<OLD_TXNLOG> old_txnlog_list = null;
		try {
			log.trace("First_Login_Service.txnlog_q.adtxno: {}", adtxno);
			
			// 取得使用者所有資料
			old_txnlog_list = old_txnlogdao.findByUserId(adtxno);
			
		} catch (Exception e) {
			log.error("", e);
		}
		
		return old_txnlog_list;
	}
	
	
	/**
	 * TXNTRACCSET--舊網銀轉新網銀
	 * @param cusidn 使用者統編
	 * @return
	 */
	public boolean txntraccset_tx(String cusidn) {
		boolean result = false;
		try {
			log.trace("First_Login_Service.txntraccset_tx.cusidn: {}", cusidn);
			
			// 取得使用者所有資料
			List<OLD_TXNTRACCSET> old_txnuser_list = txntraccset_q(cusidn);
			
			// 舊網銀有使用者資料
			if(old_txnuser_list!=null) {
				// 逐筆作轉檔
				for(OLD_TXNTRACCSET old_txnuser : old_txnuser_list) {
					TXNTRACCSET txntraccset = new TXNTRACCSET();
					txntraccset = CodeUtil.objectCovert(TXNTRACCSET.class, old_txnuser);
					txntraccset.setDPACCSETID(null);
					// Stored Log Forging
//					log.debug("First_Login_Service.txntraccset_tx.debug: {}", CodeUtil.toJson(txntraccset));
					txntraccsetdao.saveOrUpdate(txntraccset);
				}
				result = true;
			}
			
			
		} catch (Exception e) {
			log.error("", e);
		}
		
		return result;
	}
	
	
	/**
	 * 查詢舊網銀TXNTRACCSET資料
	 * @param reqid
	 * @return
	 */
	public List<OLD_TXNTRACCSET> txntraccset_q(String dpaccsetid) {
		List<OLD_TXNTRACCSET> old_txntraccset_list = null;
		try {
			log.trace("First_Login_Service.txntraccset_q.reqid: {}", dpaccsetid);
			
			// 取得使用者所有資料--20201210發現NB3的TXNTRACCSET多了限制複數欄位唯一性限制
//			old_txntraccset_list = old_txntraccsetdao.findByUserId(dpaccsetid);
			old_txntraccset_list = old_txntraccsetdao.findById(dpaccsetid);
			
		} catch (Exception e) {
			log.error("", e);
		}
		
		return old_txntraccset_list;
	}
	
	
	/**
	 * TXNTWRECORD--舊網銀轉新網銀
	 * @param cusidn 使用者統編
	 * @return
	 */
	public boolean txntwrecord_tx(String cusidn) {
		boolean result = false;
		try {
			log.trace("First_Login_Service.txntwrecord_tx.cusidn: {}", cusidn);
			
			// 取得使用者所有資料
			List<TXNTWRECORD> old_txntwrecord_list = txntwrecord_q(cusidn);
			
			// 舊網銀有使用者資料
			if(old_txntwrecord_list!=null) {
				// 逐筆作轉檔
				for(TXNTWRECORD txntwrecord : old_txntwrecord_list) {
					
					// 20201217--不清資料，若有重複的ADTXNO就不INSERT
					List<String> adtxNoList = txntwrecorddao.getAdtxNoByUser(cusidn);
					
					if (adtxNoList != null && !adtxNoList.isEmpty()) {
						log.info(ESAPIUtil.vaildLog("txntwrecord_tx.adtxNoList: " + adtxNoList)); // NB3 交易編號清單
						
						String adtxno = txntwrecord.getADTXNO(); // 2.5 交易編號清單
						log.info(ESAPIUtil.vaildLog("txntwrecord_tx.adtxno: " + adtxno));
						
						// 如果有包含一樣的交易編號就跳過不INSERT
						if (adtxNoList.contains(adtxno)) {
							continue;
						}
					}
					
					txntwrecorddao.saveOrUpdate(txntwrecord);
				}
				result = true;
			}
			
		} catch (Exception e) {
			log.error("", e);
		}
		
		return result;
	}
	
	/**
	 * 查詢舊網銀TXNTWRECORD資料
	 * @param adtxno
	 * @return
	 */
	public List<TXNTWRECORD> txntwrecord_q(String adtxno) {
		List<TXNTWRECORD> txntwrecord_list = null;
		try {
			log.trace("First_Login_Service.txntwrecord_q.adtxno: {}", adtxno);
			
			// 取得使用者所有資料
			txntwrecord_list = old_txntwrecorddao.findById(adtxno);
			
		} catch (Exception e) {
			log.error("", e);
		}
		
		return txntwrecord_list;
	}
	
	/**
	 * TXNTWSCHEDULE--舊網銀轉新網銀--TXNTWSCHPAY
	 * @param cusidn 使用者統編
	 * @return
	 */
	public boolean txntwschedule_tx(String cusidn) {
		boolean result = false;
		try {
			log.trace("First_Login_Service.txntwschedule_tx.cusidn: {}", cusidn);
			
			// MSADDR對照
			Map<String,String> adopMap = SpringBeanFactory.getBean("ADOPIDMSA");
			
			// 取得使用者所有資料
			List<TXNTWSCHPAY> txntwschpay_list = txntwschedule_q(cusidn);
			
			// 舊網銀有使用者資料
			if(txntwschpay_list!=null) {
				// 逐筆作轉檔
				for(TXNTWSCHPAY txntwschpay : txntwschpay_list) {
					// 先更新舊網銀資料，未執行:0->9
					if ("0".equals(txntwschpay.getDPTXSTATUS())) {
						old_txntwscheduledao.updateByPK(Long.valueOf(txntwschpay.getDPSCHID()), "9");
					}
					
					// 若前次轉檔有失敗，會撈到9要改成0
					if("9".equals(txntwschpay.getDPTXSTATUS())) {
						txntwschpay.setDPTXSTATUS("0");
					}
					
					
					// 20201217--不清資料，若有重複的DPSCHNO就不INSERT
					List<String> dpschNoList = txntwschpaydao.getDpschNoByUser(cusidn);
					
					if (dpschNoList != null && !dpschNoList.isEmpty()) {
						log.info(ESAPIUtil.vaildLog("txntwschedule_tx.dpschNoList: " + dpschNoList)); // NB3 預約編號清單
						
						String dpschno = txntwschpay.getDPSCHNO(); // 2.5 預約編號清單
						log.info(ESAPIUtil.vaildLog("txntwschedule_tx.dpschno: " + dpschno));
						
						// 如果有包含一樣的預約編號就跳過不INSERT
						if (dpschNoList.contains(dpschno)) {
							continue;
						}
					}

					// 轉移資料至新網銀
					txntwschpay.setDPSCHID(null);
					String msaddr = StrUtils.isNotEmpty(adopMap.get(txntwschpay.getADOPID())) ? adopMap.get(txntwschpay.getADOPID()) : "";
					txntwschpay.setMSADDR(msaddr);
					txntwschpaydao.saveOrUpdate(txntwschpay);
					
				}
				result = true;
			}
			
		} catch (Exception e) {
			log.error("", e);
		}
		
		return result;
	}
	
	/**
	 * 查詢舊網銀TXNTWSCHEDULE資料
	 * @param dpschid
	 * @return
	 */
	public List<TXNTWSCHPAY> txntwschedule_q(String dpschid) {
		List<TXNTWSCHPAY> txntwschpay_list = null;
		try {
			log.trace("First_Login_Service.txntwschedule_q.dpschid: {}", dpschid);
			
			// 取得使用者所有資料
			txntwschpay_list = old_txntwscheduledao.findById(dpschid);
			
		} catch (Exception e) {
			log.error("", e);
		}
		
		return txntwschpay_list;
	}
	
	
	/**
	 * TXNUSER--舊網銀轉新網銀
	 * @param cusidn 使用者統編
	 * @return
	 */
	public boolean txnuser_tx(String cusidn) {
		boolean result = false;
		try {
			log.trace(ESAPIUtil.vaildLog("First_Login_Service.txnuser_tx.cusidn: {}"+ cusidn));
			
			// 取得使用者所有資料
			List<OLD_TXNUSER> old_txnuser_list = old_txnuser_q(cusidn);
			
			// 舊網銀有使用者資料
			if(old_txnuser_list!=null) {
				// 逐筆作轉檔
				for(OLD_TXNUSER old_txnuser : old_txnuser_list) {
					TXNUSER txnuser = new TXNUSER();
					txnuser = CodeUtil.objectCovert(TXNUSER.class, old_txnuser);
					txnuser.setDPNOTIFY("1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21");
					txnuserdao.saveOrUpdate(txnuser);
				}
				// 紀錄使用者資料轉檔結果
				TRNS_NNB_CUSDATA po = cusdataRecord(cusidn);
				po.setTXNUSER("0");
				trns_nnb_cusdatadao.saveOrUpdate(po);
				
				result = true;
			}
			
		} catch (Exception e) {
			log.error("", e);
		}
		
		return result;
	}
	
	/**
	 * TXNUSER--20200401--bugtracker no.11 新增需求: 2.5轉至3.0及新開戶使用者初次登入設定，通知服務預設皆通知
	 * @param cusidn 使用者統編
	 * @return
	 */
	public boolean txnuser_first_setting(String cusidn) {
		boolean result = false;
		try {
			log.trace(ESAPIUtil.vaildLog("First_Login_Service.txnuser_first_setting.cusidn: {}"+ cusidn));
			
			// 取得使用者TXNUSER資料
			List<TXNUSER> txnuser_list = txnuserdao.findByUserId(cusidn);
			
			// TXNUSER沒有資料
			if( txnuser_list==null || txnuser_list.isEmpty() ) {
				// 20200401--bugtracker no.11 新增需求: 2.5轉至3.0及新開戶使用者初次登入設定，通知服務預設皆通知
				TXNUSER txnuser = new TXNUSER();
				txnuser.setDPSUERID(cusidn);
				txnuser.setDPNOTIFY("1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21");
				txnuserdao.saveOrUpdate(txnuser);
				
			} // TXNUSER有資料
			else {
				TXNUSER txnuser = txnuser_list.get(0);
				txnuser.setDPNOTIFY("1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21");
				txnuserdao.saveOrUpdate(txnuser);
			}
			
			result = true;
			
		} catch (Exception e) {
			log.error("", e);
		}
		
		return result;
	}
	
	/**
	 * 查詢舊網銀TXNUSER資料
	 * @param cusidn 使用者統編
	 * @return
	 */
	public List<OLD_TXNUSER> old_txnuser_q(String cusidn) {
		List<OLD_TXNUSER> old_txnuser_list = null;
		try {
			log.trace(ESAPIUtil.vaildLog("First_Login_Service.txnuser_q.cusidn: {}"+ cusidn));
			
			// 取得使用者所有資料
			old_txnuser_list = old_txnuserdao.findByUserId(cusidn);
			
		} catch (Exception e) {
			log.error("", e);
		}
		
		return old_txnuser_list;
	}
	
	
	
}
