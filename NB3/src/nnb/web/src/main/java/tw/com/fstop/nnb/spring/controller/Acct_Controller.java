package tw.com.fstop.nnb.spring.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.idgate.bean.N108_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N108_IDGATE_DATA_VIEW;
import tw.com.fstop.nnb.custom.annotation.ISTXNLOG;
import tw.com.fstop.nnb.service.Acct_Service;
import tw.com.fstop.nnb.service.IdGateService;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.DownloadUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.WebUtil;
import tw.com.fstop.nnb.aop.Dialog;

@SessionAttributes({ SessionUtil.PD, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, SessionUtil.PRINT_DATALISTMAP_DATA,
		SessionUtil.CONFIRM_LOCALE_DATA,SessionUtil.IDGATE_TRANSDATA, SessionUtil.RESULT_LOCALE_DATA, SessionUtil.CUSIDN,
		SessionUtil.TRANSFER_CONFIRM_TOKEN, SessionUtil.TRANSFER_RESULT_TOKEN, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN,
		SessionUtil.TRANSFER_DATA })
@Controller
@RequestMapping(value = "/NT/ACCT")
public class Acct_Controller
{
	Logger log = LoggerFactory.getLogger(this.getClass());

	// TODO 測試用
	// int cnt = 0;

	@Autowired
	private Acct_Service acct_service;
	
	@Autowired
	private I18n i18n;

	@Autowired		 
	IdGateService idgateservice;

	@ISTXNLOG(value="N110")
	@RequestMapping(value = "/balance_query")
	public String balance_query(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		String target = "/error";
		BaseResult bs = null;
		log.trace("balance_query");
		try
		{
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");

			log.debug("balance_query.cusidn >>{}", cusidn);
			bs = acct_service.balance_query(cusidn);
			log.trace(" call_N110 bs.getResult>> {}", bs.getResult());
			if (bs != null && bs.getResult())
			{
				Map<String, Object> bsData = (Map) bs.getData();
				List<Map<String, String>> rows = (List<Map<String, String>>) bsData.get("REC");
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, rows);
				model.addAttribute("CMQTIME", bsData.get("CMQTIME"));

				model.addAttribute("balance_query", bs);
			}

		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("balance_query error >> {}",e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = "/acct/balance_q";
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
		// return new ModelAndView("redirect:" + "https://tw.yahoo.com/");
	}

	// N420 定存明細
	@RequestMapping(value = "/time_deposit_details")
	public String time_deposit_details(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		String target = "/error";
		BaseResult bs = null;
		log.trace("time_deposit_details");
		try
		{
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			log.debug("time_deposit_details.cusidn >>{}", cusidn);
			bs = acct_service.time_deposit_details(cusidn);
			log.trace("bs.getResult>> {}", bs.getResult());

			if (bs != null && bs.getResult())
			{
				Map<String, Object> bsData = (Map) bs.getData();
				log.trace("dataMap={}", bsData);
				List<Map<String, String>> rows = (List<Map<String, String>>) bsData.get("REC");
				log.trace("rowListMap={}", rows);

				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, rows);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, rows);

				model.addAttribute("COUNT", bsData.get("CMRECNUM"));

				model.addAttribute("TOTAMT", bsData.get("TOTAMT"));

				model.addAttribute("TOTAL_DPWDAMT", bsData.get("TOTAL_DPWDAMT"));
				model.addAttribute("TOTAL_DPSVAMT", bsData.get("TOTAL_DPSVAMT"));
				model.addAttribute("LASTDPIBAL", bsData.get("LASTDPIBAL"));

				model.addAttribute("time_deposit_details", bs);
			}
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("time_deposit_details error >> {}",e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = "/acct/time_deposit_d";
			}
			else
			{
				//回首頁(無上一頁則回首頁) 
				bs.setPrevious("/INDEX/index");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// <p> 顯示臺幣活期性存款明細查詢頁</p>
	@RequestMapping(value = "/demand_deposit_detail")
	public String demand_deposit_detail(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("demand_deposit_detail Start~~~~~~~");
		String target = "/error";
		BaseResult bs = null;
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try
		{
			String getAcn = okMap.get("Acn");
			log.trace(ESAPIUtil.vaildLog("GET ACN >>> " + getAcn));
			bs = new BaseResult();
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			// Get session value by session Key
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			// Get put page value
			bs = acct_service.N920_REST(cusidn, Acct_Service.ACNO);
			bs.addData("TODAY", DateUtil.getCurentDateTime("yyyy/MM/dd"));
			bs.addData("Acn", getAcn);
			if(okMap.containsKey("n110_ACN"))
				bs.addData("n110_ACN", okMap.get("n110_ACN"));
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("demand_deposit_detail error >> {}",e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = "/acct/demand_deposit_detail";
				model.addAttribute("demand_deposit_detail", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// <p> 顯示臺幣活期性存款結果頁</p>
	@RequestMapping(value = "/demand_deposit_detail_result")
	@Dialog(ADOPID = "N130")
	public String demand_deposit_detail_result(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("demand_deposit_detail_query Start~~~~~~~");
		String target = "/error";
		BaseResult bs = new BaseResult();
		try
		{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
			}
			else
			{
				// Get session value by session Key
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
				bs = acct_service.getTwDepositDetail(cusidn, okMap);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("demand_deposit_detail_result error >> {}",e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = "/acct/demand_deposit_detail_result";
				List<Map<String, Object>> dataListMap = ((List<Map<String, Object>>) ((Map<String, Object>) bs.getData()).get("labelList"));
				log.debug("dataListMap={}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
				for (Map<String, Object> dataMap : dataListMap) {
					List<Map<String, String>> rows = (List<Map<String, String>>)dataMap.get("rowListMap");
					for(Map<String, String> row :rows) {
						if(row.get("CODDBCR").equals("C")) {
							row.put("DLDPWDAMT", NumericUtil.fmtAmount(row.get("DPSVAMT"), 2) + " ");
						}
					}
				}
				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, dataListMap);
				model.addAttribute("demand_deposit_detail_result", bs);
			}
			else
			{
				// 新增回上一頁的路徑
				bs.setPrevious("/NT/ACCT/demand_deposit_detail");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// <p> 臺幣活期性存款直接下載</p>
	@RequestMapping(value = "/demand_deposit_detail_directDownload")
	public String demand_deposit_detail_directDownload(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("demand_deposit_detail_directDownload start~~");
		log.trace(ESAPIUtil.vaildLog("reqParam >> " + CodeUtil.toJson(reqParam))); 
		String target = "/error";
		BaseResult bs = null;
		try
		{
			bs = new BaseResult();

			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			bs = acct_service.twDepositDetaildirectDownload(cusidn, okMap);
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("demand_deposit_detail_directDownload error >> {}",e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = "forward:/directDownload";
				Map<String, Object> dataMap = (Map) bs.getData();
				log.trace("dataMap={}", dataMap);
				List<Map<String, Object>> rowListMap = (List<Map<String, Object>>) dataMap.get("labelList");
				log.debug("rowListMap={}", rowListMap);
				
				for (Map<String, Object> rowlist : rowListMap) {
					List<Map<String, String>> rows = (List<Map<String, String>>)rowlist.get("rowListMap");
					for(Map<String, String> row :rows) {
						if(row.get("CODDBCR").equals("C")) {
							row.put("DLDPWDAMT", NumericUtil.fmtAmount(row.get("DPSVAMT"), 2) + " ");
						}
					}
				}
				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, rowListMap);
				Map<String, Object> parameterMap = (Map<String, Object>) dataMap.get("parameterMap");
				request.setAttribute("parameterMap", parameterMap);
			}
			else
			{
				bs.setPrevious("/NT/ACCT/demand_deposit_detail");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// N015支存不足明細輸入頁
	@RequestMapping(value = "/checking_insufficient")
	public String checking_insufficient(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("checking_insufficient>>");
		String target = "/error";
		BaseResult bs = null;

		// 清除切換語系時暫存的資料
		SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
		SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");

		try
		{
			String getAcn = reqParam.get("Acn");
			log.trace(ESAPIUtil.vaildLog("GET ACN >>> " + getAcn));
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			bs = acct_service.checking_insufficient(cusidn);
			bs.addData("Acn", getAcn);
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("checking_insufficient error >> {}",e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = "/acct/checking_insufficient";
				model.addAttribute("checking_insufficient", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);

			}
		}
		return target;
	}

	// N015支存不足明細結果頁
	@RequestMapping(value = "/checking_insufficient_details_result")
	public String checking_insufficient_details_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		String target = "/error";
		BaseResult bs = null;
		log.trace("checking_insufficient_result>>");
		try
		{
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			String acn = reqParam.get("ACN");
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult())
				{
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale)
			{
				bs.reset();
				log.trace("cusidn>>{}", cusidn);
				bs = acct_service.checking_insufficient_result(cusidn, okMap);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("checking_insufficient_details_result error >> {}",e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = "/acct/checking_insufficient_r";

				Map<String, Object> dataMap = (Map) bs.getData();
				log.trace("dataMap={}", dataMap);
				List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("REC");
				log.trace("rowListMap={}", rowListMap);

				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, rowListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, rowListMap);

				model.addAttribute("checking_insufficient", bs);
			}
			else
			{
				bs.setPrevious("/NT/ACCT/checking_insufficient");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// 支存不足票據明細直接下載
	@RequestMapping(value = "/checking_insufficient_directDownload")
	public String checking_insufficient_directDownload(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		String target = "/error";
		BaseResult bs = null;
		log.trace("checking_insufficient_directDownload");
		// 序列化傳入值
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		log.debug(ESAPIUtil.vaildLog("okMap >> " + CodeUtil.toJson(okMap))); 
		try
		{
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			log.trace("cusidn={}", cusidn);
			for (Entry<String, String> entry : okMap.entrySet())
			{
				okMap.put(entry.getKey(), URLDecoder.decode(entry.getValue(), "UTF-8"));
			}
			bs = acct_service.checking_insufficient_result(cusidn, okMap);
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("checking_insufficient_directDownload error >> {}",e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = "forward:/directDownload";

				Map<String, Object> dataMap = (Map) bs.getData();
				log.trace("dataMap={}", dataMap);
				List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("REC");
				log.trace("rowListMap={}", rowListMap);

				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, rowListMap);

				Map<String, Object> parameterMap = new HashMap<String, Object>();

				parameterMap.put("CMQTIME", dataMap.get("CMQTIME"));

				parameterMap.put("ACN", dataMap.get("ACN"));

				parameterMap.put("SNTCNT", dataMap.get("SNTCNT"));
				parameterMap.put("SNTAMT", dataMap.get("SNTAMT"));
				parameterMap.put("TOTBAL", dataMap.get("TOTBAL"));
				parameterMap.put("AVLBAL", dataMap.get("AVLBAL"));

				parameterMap.put("downloadFileName", okMap.get("downloadFileName"));
				parameterMap.put("downloadType", okMap.get("downloadType"));
				parameterMap.put("templatePath", okMap.get("templatePath"));

				String downloadType = okMap.get("downloadType");
				log.trace(ESAPIUtil.vaildLog("downloadType >> " + downloadType)); 
				// EXCEL和OLDEXCEL下載
				if ("EXCEL".equals(downloadType) || "OLDEXCEL".equals(downloadType))
				{
					parameterMap.put("headerRightEnd", okMap.get("headerRightEnd"));
					parameterMap.put("headerBottomEnd", okMap.get("headerBottomEnd"));
					parameterMap.put("rowStartIndex", okMap.get("rowStartIndex"));
					parameterMap.put("rowRightEnd", okMap.get("rowRightEnd"));
				}
				// TXT下載
				else
				{
					parameterMap.put("txtHeaderBottomEnd", okMap.get("txtHeaderBottomEnd"));
					parameterMap.put("txtHasRowData", okMap.get("txtHasRowData"));
				}
				request.setAttribute("parameterMap", parameterMap);
			}
			else
			{
				bs.setPrevious("/NT/ACCT/checking_insufficient");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// 支存已兌現票據明細輸入頁
	@RequestMapping(value = "/checking_cashed")
	public String checking_cashed(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("checking_cashed");
		String target = "/error";
		BaseResult bs = null;

		// 清除切換語系時暫存的資料
		SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
		SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");

		try
		{
			String getAcn = reqParam.get("Acn");
			log.trace(ESAPIUtil.vaildLog("GET ACN >>> " + getAcn));
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			log.trace("cusidn={}", cusidn);
			bs = acct_service.checking_cashed(cusidn);
			bs.addData("TODAY", DateUtil.getCurentDateTime("yyyy/MM/dd"));
			bs.addData("Acn", getAcn);
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("checking_cashed error >> {}",e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = "/acct/checking_cashed";
				model.addAttribute("checking_cashed", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);

			}
		}
		return target;
	}

	// 支存已兌現票據明細
	@RequestMapping(value = "/checking_cashed_result")
	public String checking_cashed_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		String target = "/error";
		BaseResult bs = null;
		log.trace("checking_cashed_result");
		try
		{
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			String acn = reqParam.get("ACN");
			log.trace("cusidn={}", cusidn);
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult())
				{
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale)
			{
				bs.reset();
				bs = acct_service.checking_cashed_result(cusidn, okMap);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
			bs.addData("ACN", acn);
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("checking_cashed_result error >> {}",e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = "/acct/checking_cashed_result";

				Map<String, Object> dataMap = (Map<String, Object>) bs.getData();
				log.trace("dataMap={}", dataMap);

				List<Map<String, Object>> labelListMap = (List<Map<String, Object>>) dataMap.get("labelList");
				log.trace("labelListMap={}", labelListMap);

				// 列印調整

				// 總筆數
				int TOTALCOUNT = 0;

				for (Map<String, Object> map : labelListMap)
				{
					List<Map<String, String>> rowListMap = (List<Map<String, String>>) map.get("rowListMap");
					TOTALCOUNT += rowListMap.size();

					// 總計支出金額
					int TOTAL_DPWDAMT = 0;
					// 總計收入金額
					int TOTAL_DPSVAMT = 0;
					// 總計餘額
					int LASTDPIBAL = 0;
					// 小數點位置
					int dotPosition = 0;
					// 轉金額千分位格式
					DecimalFormat df = new DecimalFormat();

					for (int x = 0; x < rowListMap.size(); x++)
					{
						Map<String, String> detailMap = rowListMap.get(x);

						// 支出
						String DPWDAMT = detailMap.get("DPWDAMT");
						if ("".equals(DPWDAMT) || DPWDAMT == null)
						{
							DPWDAMT = "0";
						}
						log.trace("DPWDAMT={}", DPWDAMT);
						dotPosition = DPWDAMT.indexOf(".");
						log.trace("dotPosition={}", dotPosition);
						if (dotPosition > -1)
						{
							DPWDAMT = DPWDAMT.substring(0, dotPosition);
							log.trace("DPWDAMT={}", DPWDAMT);
						}
						DPWDAMT = DPWDAMT.replace(",", "");
						log.trace("DPWDAMT={}", DPWDAMT);
						try
						{
							TOTAL_DPWDAMT += Integer.valueOf(DPWDAMT);
						}
						catch (Exception e)
						{
							log.error("", e);
						}
						detailMap.put("DPWDAMT", NumericUtil.fmtAmount(DPWDAMT, 2));

						// 收入
						String DPSVAMT = detailMap.get("DPSVAMT");
						if ("".equals(DPSVAMT) || DPSVAMT == null)
						{
							DPSVAMT = "0";
						}
						log.trace("DPSVAMT={}", DPSVAMT);
						dotPosition = DPWDAMT.indexOf(".");
						log.trace("dotPosition={}", dotPosition);
						if (dotPosition > -1)
						{
							DPSVAMT = DPSVAMT.substring(0, dotPosition);
							log.trace("DPSVAMT={}", DPSVAMT);
						}
						DPSVAMT = DPSVAMT.replace(",", "");
						log.trace("DPSVAMT={}", DPSVAMT);
						try
						{
							TOTAL_DPSVAMT += Integer.valueOf(DPSVAMT);
						}
						catch (Exception e)
						{
							log.error("", e);
						}
						detailMap.put("DPSDAMT", NumericUtil.fmtAmount(DPSVAMT, 2));

						// 餘額
						String DPIBAL = detailMap.get("DPIBAL");
						if ("".equals(DPIBAL))
						{
							DPIBAL = "0";
						}
						log.trace("DPIBAL={}", DPIBAL);
						dotPosition = DPIBAL.indexOf(".");
						log.trace("dotPosition={}", dotPosition);
						if (dotPosition > -1)
						{
							DPIBAL = DPIBAL.substring(0, dotPosition);
							log.trace("DPIBAL={}", DPIBAL);
						}
						DPIBAL = DPIBAL.replaceAll(",", "");
						log.trace("DPIBAL={}", DPIBAL);
						try
						{
							LASTDPIBAL = Integer.valueOf(DPIBAL);
						}
						catch (Exception e)
						{
							log.error("", e);
						}
						if (x == rowListMap.size() - 1)
						{
							map.put("TOTAL_DPWDAMT", df.format(TOTAL_DPWDAMT) + ".00");
							map.put("TOTAL_DPSVAMT", df.format(TOTAL_DPSVAMT) + ".00");
							map.put("LASTDPIBAL", df.format(LASTDPIBAL) + ".00");
						}
					}
				}
				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, labelListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, labelListMap);

				model.addAttribute("checking_cashed_result", bs);
			}
			else
			{
				bs.setPrevious("/NT/ACCT/checking_cashed");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// 支存已兌現票據明細直接下載
	@RequestMapping(value = "/checking_cashed_directDownload")
	public String checking_cashed_directDownload(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		String target = "/error";
		BaseResult bs = null;
		log.trace("checking_cashed_directDownload");
		// 序列化傳入參數
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		log.debug(ESAPIUtil.vaildLog("formMap >> " + CodeUtil.toJson(okMap))); 
		try
		{
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			log.trace("cusidn={}", cusidn);
			for (Entry<String, String> entry : okMap.entrySet())
			{
				okMap.put(entry.getKey(), URLDecoder.decode(entry.getValue(), "UTF-8"));
			}
			bs = acct_service.checkingCashedDirectDownload(cusidn, okMap);
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("checking_cashed_directDownload error >> {}",e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = "forward:/directDownload";
				Map<String, Object> dataMap = (Map) bs.getData();
				log.trace("dataMap={}", dataMap);
				List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("labelList");
				log.debug("rowListMap={}", rowListMap);
				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, rowListMap);
				Map<String, Object> parameterMap = (Map<String, Object>) dataMap.get("parameterMap");
				request.setAttribute("parameterMap", parameterMap);
			}
			else
			{
				bs.setPrevious("/NT/ACCT/checking_cashed");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * 取得綜合存款帳號
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/getCollateralAcn_aj", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody BaseResult getOutAcn(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("_result.getOutACNO...");
		BaseResult bs = new BaseResult();
		try
		{
			response.setHeader("Content-Security-Policy", "default-src 'none'; img-src 'self' data:;");
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			bs = acct_service.getCollateralAcn(cusidn);
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getOutAcn error >> {}",e);
		}

		return bs;

	}

	// 台幣綜存質借申請/取消輸入頁
	@RequestMapping(value = "/collateral")
	public String collateral(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("collateral>>");
		String target = "/error";
		BaseResult bs = null;

		try
		{
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");

			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = acct_service.getTxToken();
			log.trace("collateral.getTxToken: " + bs.getResult());
			log.trace("collateral.TXTOKEN: " + ((Map<String, String>) bs.getData()).get("TXTOKEN"));
			if (bs.getResult())
			{
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, ((Map<String, String>) bs.getData()).get("TXTOKEN"));
			}

			// 登入時的身分證字號
			String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if (!"".equals(cusidn) && cusidn != null)
			{
				// 解密成明碼
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				log.trace("cusidn: " + cusidn);

				bs.setResult(true);

				// 轉出成功是否Email通知
				// boolean chk_result = pay_expense_service.chkContains(cusidn);
				// model.addAttribute("sendMe", chk_result);

			}
			else
			{
				log.error("session no cusidn!!!");
			}
			//IDGATE身分
			String idgateUserFlag="N";		 
			Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
			try {		 
				if(IdgateData==null) {
					IdgateData = new HashMap<String, Object>();
				}
				BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
				if(tmp.getResult()) {		 
					idgateUserFlag="Y";                  		 
				}		 
				tmp.addData("idgateUserFlag",idgateUserFlag);		 
				IdgateData.putAll((Map<String, String>) tmp.getData());
				SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
			}catch(Exception e) {		 
				log.debug("idgateUserFlag error {}",e);		 
			}		 
			model.addAttribute("idgateUserFlag",idgateUserFlag);

		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("collateral error >> {}",e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = "/acct/collateral";
				model.addAttribute("collateral", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);

			}
		}
		return target;
	}

	// 台幣綜存質借申請/取消確認頁
	@RequestMapping(value = "/collateral_confirm")
	public String collateral_confirm(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("collateral_confirm>>");

		String target = "/error";
		String jsondc = "";
		BaseResult bs = new BaseResult();
		
		log.trace(ESAPIUtil.vaildLog("reqParam >> " + CodeUtil.toJson(reqParam))); 

		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);

		log.trace(ESAPIUtil.vaildLog("okMap >> " + CodeUtil.toJson(okMap))); 

		try
		{
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam), "UTF-8");
			log.trace(ESAPIUtil.vaildLog("collateral_confirm.jsondc >> " + jsondc)); 

			okMap.put("jsondc", jsondc);
			log.trace(ESAPIUtil.vaildLog("collateral_confirm.okMap >> " + CodeUtil.toJson(okMap))); 
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				if (!bs.getResult())
				{
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
				else
				{
					// session 資料放入 map 讓後續 model 和回上一頁取值
					okMap.putAll((Map<String, String>) bs.getData());
				}
			}
			else
			{
				// 驗證TOKEN 沒TOKEN會到錯誤頁，錯誤頁確認後返回輸入頁
				String token = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null);
				log.trace("collateral_confirm.token: {}", token);
				if (StrUtil.isNotEmpty(okMap.get("TOKEN")) && okMap.get("TOKEN").equals(token))
				{
					bs.setResult(Boolean.TRUE);
				}
				log.trace("bs.getResult(): {}", bs.getResult());
				if (!bs.getResult())
				{
					log.trace("throw new Exception()");
					throw new Exception();
				}
				// TOKEN驗證成功，資料處理後進入確認頁
				bs.reset();
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
				bs = acct_service.collateral_confirm(cusidn, okMap);
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
				Map<String, String> result = new HashMap<>();
				result.putAll((Map<String, String>) bs.getData());
				// result = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null);
				log.debug("session result >> {}", result);
				result.put("TOKEN", ((Map<String,String>) bs.getData()).get("TXTOKEN"));
				result.put("jsondc", jsondc); // IKEY要用不經過ESAPIUtil，會出錯
				log.trace(ESAPIUtil.vaildLog("other_electricity_fee_confirm.result: {}"+ result));
				
				// IDGATE transdata            		 
				Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
				String adopid = "N108";
				String title = "您有一筆質借功能取消待確認";
				if(IdgateData != null) {
					model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
					if(IdgateData.get("idgateUserFlag").equals("Y")) {
						result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
						result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
						IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N108_IDGATE_DATA.class, N108_IDGATE_DATA_VIEW.class, result));
						SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
					}
				}
				model.addAttribute("idgateAdopid", adopid);
				model.addAttribute("idgateTitle", title);
			}
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("collateral_confirm error >> {}",e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = "/acct/collateral_step1";
				model.addAttribute("collateral_confirm", bs);
				model.addAttribute(SessionUtil.TRANSFER_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, ((Map<String, Object>) bs.getData()).get("TXTOKEN"));
			}
			else
			{
				bs.setPrevious("/acct/collateral");
				model.addAttribute(BaseResult.ERROR, bs);

			}
		}
		return target;
	}

	// 台幣綜存質借申請/取消結果頁
	@RequestMapping(value = "/collateral_result")
	public String collateral_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{

		String target = "/error";
		String next = "/NT/ACCT/collateral";

		BaseResult bs = null;
		log.trace("collateral_result>>");
		try
		{
			// 初始化BaseResult
			bs = new BaseResult();
			// 取得CUSIDN明碼
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			reqParam.put("CUSIDN", cusidn);
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			//Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI

			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult())
				{
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale)
			{
				log.trace("collateral_result.validate TXTOKEN...");
				log.trace("collateral_result.TRANSFER_RESULT_TOKEN: "
						+ SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("collateral_result.TRANSFER_RESULT_FINSH_TOKEN: "
						+ SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));

				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if (StrUtil.isNotEmpty(okMap.get("TXTOKEN"))
						&& okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)))
				{
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("collateral_result.bs.step1 is successful...");
				}
				if (!bs.getResult())
				{
					log.error(ESAPIUtil.vaildLog("collateral_result TXTOKEN 不正確，TXTOKEN >> " + okMap.get("TXTOKEN"))); 
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if (StrUtil.isNotEmpty(okMap.get("TXTOKEN"))
						&& !okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)))
				{
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("collateral_result.bs.step2 is successful...");
				}
				if (!bs.getResult())
				{
					log.error(ESAPIUtil.vaildLog("collateral_result TXTOKEN 不正確，或重複交易，TXTOKEN >> " + okMap.get("TXTOKEN"))); 
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();

				// 取得從輸入頁輸入存在session中的輸入資料
				bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
				Map<String, String> transfer_data = (Map<String, String>) bs.getData();

				// 取得SESSION中的輸入資料，加入到確認頁的交易機制資料
				okMap.putAll(transfer_data);

				// 重置bs，進行交易
				bs.reset();
				//IDgate驗證		 
                try {		 
                    if(okMap.get("FGTXWAY").equals("7")) {		 
        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N108_IDGATE");
                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
                    }		 
                }catch(Exception e) {		 
                    log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
                } 
                
				Map<String,Object>IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
				model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
				
				bs = acct_service.collateral_result(okMap);

				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}

			model.addAttribute("transfer_result_data", bs);
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("collateral_result error >> {}",e);
		}
		finally
		{
			// TODO 要清除必要的SESSION

			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			//Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI

			// 交易成功要存之前的token 在Session 以利下次比對是否重複交易
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TXTOKEN"));
			if (bs.getResult())
			{
				target = "/acct/collateral_result";
			}
			else
			{
				bs.setNext(next);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// 虛擬帳號入帳明細輸入頁
	@RequestMapping(value = "/virtual_account_detail")
	public String virtual_account_detail(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{

		String target = "/error";
		BaseResult bs = null;

		// 清除切換語系時暫存的資料
		SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
		SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");

		try
		{
			bs = new BaseResult();
			bs.addData("TODAY", DateUtil.getCurentDateTime("yyyy/MM/dd"));
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("virtual_account_detail error >> {}",e);
		}
		finally
		{
			target = "/acct/virtual_account_detail";
			model.addAttribute("virtual_account_detail", bs);

		}
		return target;
	}

	// 虛擬帳號入帳明細結果頁
	@RequestMapping(value = "/virtual_account_detail_result")
	public String virtual_account_detail_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		String target = "/error";
		BaseResult bs = null;
		log.trace("virtual_account_detail_result");
		try
		{
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			log.trace(" call_N615 bs.getResult>> {}", bs.getResult());

			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult())
				{
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale)
			{
				bs.reset();
				log.debug("virtual_account_detail_result.cusidn >>{}", cusidn);
				bs = acct_service.virtual_account_detail_result(cusidn, okMap);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("virtual_account_detail_result error >> {}",e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = "/acct/virtual_account_detail_result";
				Map<String, Object> bsData = (Map) bs.getData();
				log.debug("bsData" + bsData);
				List<Map<String, String>> rows = (List<Map<String, String>>) bsData.get("REC");
				log.debug("rows" + rows);

				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, rows);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, rows);
				// model.addAttribute("CMQTIME", bsData.get("CMQTIME"));

				model.addAttribute("virtual_account_detail_result", bs);
			}
			else
			{
				bs.setPrevious("/NT/ACCT/virtual_account_detail");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// 虛擬帳號入帳明細直接下載
	@RequestMapping(value = "/virtual_account_detail_directDownload")
	public String virtual_account_detail_directDownload(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		String target = "/error";
		BaseResult bs = null;
		log.trace("virtual_account_detail_directDownload");
		// 序列化傳入參數
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		log.debug(ESAPIUtil.vaildLog("formMap >> " + CodeUtil.toJson(okMap))); 
		try
		{
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			log.trace("cusidn={}", cusidn);
			for (Entry<String, String> entry : okMap.entrySet())
			{
				okMap.put(entry.getKey(), URLDecoder.decode(entry.getValue(), "UTF-8"));
			}
			bs = acct_service.virtual_account_detail_result(cusidn, okMap);
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("virtual_account_detail_directDownload error >> {}",e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = "forward:/directDownload";
				Map<String, Object> dataMap = (Map) bs.getData();
				log.trace("dataMap={}", dataMap);
				List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("REC");
				log.trace("rowListMap={}", rowListMap);

				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, rowListMap);

				Map<String, Object> parameterMap = new HashMap<String, Object>();

				parameterMap.put("CMQTIME", dataMap.get("CMQTIME"));
				parameterMap.put("ACN", dataMap.get("ACN"));
				parameterMap.put("CMPERIOD", dataMap.get("CMPERIOD"));
				parameterMap.put("CMRECNUM", String.valueOf(rowListMap.size()));

				parameterMap.put("downloadFileName", okMap.get("downloadFileName"));
				parameterMap.put("downloadType", okMap.get("downloadType"));
				parameterMap.put("templatePath", okMap.get("templatePath"));

				String downloadType = okMap.get("downloadType");
				log.trace(ESAPIUtil.vaildLog("downloadType >> " + downloadType)); 

				// EXCEL和OLDEXCEL下載
				if ("EXCEL".equals(downloadType) || "OLDEXCEL".equals(downloadType))
				{
					parameterMap.put("headerRightEnd", okMap.get("headerRightEnd"));
					parameterMap.put("headerBottomEnd", okMap.get("headerBottomEnd"));
					parameterMap.put("rowStartIndex", okMap.get("rowStartIndex"));
					parameterMap.put("rowRightEnd", okMap.get("rowRightEnd"));
				}
				// TXT下載
				else
				{
					parameterMap.put("txtHeaderBottomEnd", okMap.get("txtHeaderBottomEnd"));
					parameterMap.put("txtHasRowData", okMap.get("txtHasRowData"));
				}
				request.setAttribute("parameterMap", parameterMap);
			}
			else
			{
				bs.setPrevious("/NT/ACCT/virtual_account_detail");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// 託收票據明細輸入頁
	@RequestMapping(value = "/collection_bills")
	public String collection_bills(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("collection_bills Start");
		String target = "/error";
		BaseResult bs = null;
		try
		{
			bs = new BaseResult();
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			// Get session value by session Key
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			// Get put page value
			bs = acct_service.N920_REST(cusidn, Acct_Service.ACNO);
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("collection_bills error >> {}",e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = "/acct/collection_bills";
				model.addAttribute("collection_bills", bs);
			}
			else
			{
				bs.setPrevious("/INDEX/index");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		model.addAttribute("time_now", DateUtil.getDate("/") + " " + DateUtil.getTheTime(":"));
		return target;
	}

	@RequestMapping(value = "/getAcno_aj", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody BaseResult getACNO(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		String str = "getOutAcno_aj";
		log.trace(ESAPIUtil.vaildLog("hi >> " + str)); 
		log.trace(ESAPIUtil.vaildLog("reqParam >> " + reqParam)); 
		// TODO 要取得使用者相關帳號
		BaseResult bs = new BaseResult();
		try
		{
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			bs = acct_service.getACNO_List(cusidn);

		}
		catch (UnsupportedEncodingException e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getACNO error >> {}",e);
		}
		// String type = reqParam.get("type");
		return bs;
	}

	@RequestMapping(value = "/collection_bills_get_branch_list")
	public String collection_bills_get_branch_list(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{

		String target = "/error";
		BaseResult bs = null;
		log.trace("collection_bills_get_branch_list");
		try
		{
			bs = acct_service.getBranchList();
			// log.debug("branch_list size >> {}", bs.size());
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("collection_bills_get_branch_list error >> {}",e);
		}
		finally
		{
			log.debug("branch_list size >> {}", bs.getData());
			target = "/acct/collection_bills_get_branch_list";
			model.addAttribute("collection_bills", bs);
		}
		// String type = reqParam.get("type");
		return target;
	}

	// 託收票據明細結果頁
	@RequestMapping(value = "/collection_bills_result")
	public String collection_bills_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		String target = "/error";
		BaseResult bs = null;
		log.trace("collection_bills_result");
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try
		{
			// TODO:i18n改語系資料保留尚未做
			bs = new BaseResult();
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult())
				{
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale)
			{
				bs.reset();
				bs = acct_service.collection_bills_result(cusidn, reqParam);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
			bs.setResult(true);
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("collection_bills_result error >> {}",e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = "/acct/collection_bills_result";
				log.debug("bs_get_data={}", bs.getData());
				List<Map<String, String>> dataListMap = ((List<Map<String, String>>) ((Map<String, Object>) bs.getData()).get("REC"));
				log.debug("dataListMap={}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
				log.trace("test" + bs);

				model.addAttribute("collection_bills_result", bs);

			}
			else
			{
				// 新增回上一頁的路徑
				bs.setPrevious("/NT/ACCT/BOND/collection_bills");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// 託收票據明細輸入頁直接下載
	@RequestMapping(value = "/collection_bills_ajaxDirectDownload")
	public String collection_bills_directDownload(HttpServletRequest request, HttpServletResponse response,
			@RequestBody(required = false) String serializeString, Model model)
	{
		log.trace("collection_bills_ajaxDirectDownload");
		String target = "/error";
		BaseResult bs = null;

		try
		{
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));

			log.debug("cusidn {} ", cusidn);
			
			// 序列化傳入值
			Map<String, String> formMap = DownloadUtil.serializeStringToMap(serializeString);
			
			//修改 Potential O Reflected XSS All Clients
			Map<String, String> okMap = ESAPIUtil.validStrMap(formMap);
			
			log.debug(ESAPIUtil.vaildLog("okMap >> " + CodeUtil.toJson(okMap)));
			for (Entry<String, String> entry : okMap.entrySet())
			{
				okMap.put(entry.getKey(), URLDecoder.decode(entry.getValue(), "UTF-8"));
			}
			bs = acct_service.CollectionBillsdirectDownload(cusidn, okMap);
			bs.setResult(true);

		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("collection_bills_directDownload error >> {}",e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = "forward:/directDownload";
				Map<String, Object> dataMap = (Map) bs.getData();
				log.trace("dataMap={}", dataMap);
				List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("REC");
				log.trace("rowListMap={}", rowListMap);
				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, rowListMap);
				Map<String, Object> parameterMap = (Map<String, Object>) dataMap.get("parameterMap");
				log.trace("parameterMap={}", parameterMap);
				request.setAttribute("parameterMap", parameterMap);
			}
			else
			{
				bs.setPrevious("/NT/ACCT/collection_bills");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * 數位存款帳戶存摺封面下載查詢頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/digital_deposit")
	public String digital_deposit(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		
		log.trace("digital_deposit");
		String target = "/error";
		BaseResult bs = null;

		// 清除切換語系時暫存的資料
		SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
		SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);

		try
		{
			boolean hasLocale = okMap.containsKey("locale");
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
			}
			else {
				bs = new BaseResult();
				bs = acct_service.digital_deposit(cusidn);
				log.debug("GET DATA = " + bs.getData());
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
			}
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("digital_deposit error >> {}",e);
		}
		finally
		{
			if(bs != null && bs.getResult()) 
			{
				target = "/acct/digital_deposit";
				model.addAttribute("digital_deposit", bs);
			} else {
				bs.setPrevious("/INDEX/index"); 
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * 數位存款帳戶存摺封面下載結果頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/digital_deposit_result")
	public String digital_deposit_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		
		log.trace("digital_deposit_result");
		String target = "/error";
		BaseResult bs = new BaseResult();
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		
		try
		{
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			String acn = okMap.get("ACN");
			if (hasLocale)
			{
				if (!bs.getResult())
				{
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale)
			{
				BaseResult bs1 = new BaseResult();
				bs1 = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				Map<String, Object> bsData = (Map) bs1.getData();
				log.debug("bsData" + bsData);
				
				bs = acct_service.digital_deposit_result(acn, bsData);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("digital_deposit error >> {}",e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = "/acct/digital_deposit_result";
				model.addAttribute("digital_deposit_result", bs);
			}
			else
			{
				bs.setPrevious("/NT/ACCT/digital_deposit");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * 帳戶明細查詢PDF下載
	 * @param requestParam
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping(value = "/downloadDemandDepositDetailPDF")
	public String downloadDemandDepositDetailPDF(@RequestParam Map<String, String> requestParam, HttpServletRequest request,
			HttpServletResponse response, Model model) {
		
		log.trace("downloadDemandDepositDetailPDF");
		String target = "/error";
		BaseResult bs = new BaseResult();
		Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
		List<Map<String, Object>> dataListMap = null;
		try
		{
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			
			if(okMap.get("downloadType").equals("direct")) {
				//pdf直接下載
				bs = acct_service.twDepositDetaildirectDownload(cusidn, okMap);
				if(bs != null && bs.getResult()) {
					Map<String, Object> addMap = (Map) bs.getData();
					String cmqtime = addMap.get("CMQTIME").toString();
					okMap.put("CMQTIME", cmqtime);
					String cmperiod = addMap.get("CMPERIOD").toString();
					okMap.put("CMPERIOD", cmperiod);
					String count = addMap.get("COUNT").toString();
					okMap.put("COUNT", count);
					
					dataListMap = ((List<Map<String, Object>>) ((Map<String, Object>) bs.getData()).get("labelList"));
					log.debug("dataListMap={}", dataListMap);
					SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
					for (Map<String, Object> dataMap : dataListMap) {
						List<Map<String, String>> rows = (List<Map<String, String>>)dataMap.get("rowListMap");
						for(Map<String, String> row :rows) {
							if(row.get("CODDBCR").equals("C")) {
								row.put("DLDPWDAMT", NumericUtil.fmtAmount(row.get("DPSVAMT"), 2) + " ");
							}
						}
					}
					SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, dataListMap);
				}
			}
			else {
				dataListMap = (List<Map<String,Object>>)SessionUtil.getAttribute(model,SessionUtil.DOWNLOAD_DATALISTMAP_DATA,null);
			}
			
			if(null != dataListMap && !dataListMap.isEmpty()) {
				acct_service.downloadDemandDepositDetailPDF(response, dataListMap, okMap);
				if(okMap.get("downloadType").equals("PDF")) {
					target = "forward:/acct/demand_deposit_detail_result";
				}
				else {
					target = "forward:/acct/demand_deposit_detail";
				}
			}
			else {
				log.error("DemandDepositDetail dataListMap null");
				bs.setPrevious("/NT/ACCT/demand_deposit_detail");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("digital_deposit error >> {}",e);
			bs.setPrevious("/NT/ACCT/demand_deposit_detail");
			model.addAttribute(BaseResult.ERROR, bs);
		}
		return target;
	}
	
	/**
	 * 數位存款帳戶存摺封面下載
	 * @param requestParam
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping(value = "/downloadDigitalDepositPDF")
	public String downloadDigitalDepositPDF(@RequestParam Map<String, String> requestParam, HttpServletRequest request,
			HttpServletResponse response, Model model) {
		
		log.trace("downloadDigitalDepositPDF");
		String target = "/error";
		BaseResult bs = new BaseResult();
		Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
		try
		{
			acct_service.downloadDigitalDepositPDF(okMap, response);
			target = "forward:/acct/digital_deposit_result";
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("digital_deposit error >> {}",e);
			bs.setPrevious("/NT/ACCT/digital_deposit");
			model.addAttribute(BaseResult.ERROR, bs);
		}
		return target;
	}
	
	@ISTXNLOG(value="N861")
	@RequestMapping(value = "/dep_salary")
	public String dep_salary(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		String target = "/error";
		BaseResult bs = null;
		log.trace("dep_salary");
		try
		{
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");

			log.debug("dep_salary_query.cusidn >>{}", cusidn);
			bs = acct_service.dep_salary_query(cusidn);
			log.trace(" call_N861 bs.getResult>> {}", bs.getResult());
			if (bs != null && bs.getResult())
			{
				Map<String, Object> bsData = (Map) bs.getData();
				String ABAVCT = (String) bsData.get("ABAVCT");
				String ABRECT = (String)bsData.get("ABRECT");
				String ABAVCTX = (String) bsData.get("ABAVCTX");
				String ABRECTX = (String) bsData.get("ABRECTX");
				String MEGECT = (String) bsData.get("MEGECT");
				String MEAVCT = (String) bsData.get("MEAVCT");
				String ITR = (String)bsData.get("ITR");

				bsData.put("ABAVCT", Integer.valueOf(ABAVCT));
				bsData.put("ABRECT", Integer.valueOf(ABRECT));
				bsData.put("ABAVCTX", Integer.valueOf(ABAVCTX));
				bsData.put("ABRECTX", Integer.valueOf(ABRECTX));
				bsData.put("MEGECT", Integer.valueOf(MEGECT));
				bsData.put("MEAVCT", Integer.valueOf(MEAVCT));
				log.debug("bsData={}", bsData);
				
				if(Double.parseDouble(ITR)>0){
					bsData.put("ITR", Double.parseDouble(ITR));
				}

				model.addAttribute("CMQTIME", bsData.get("CMQTIME"));

				model.addAttribute("dep_salary", bs);
			}

		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("dep_salary_query error >> {}",e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = "/acct/dep_salary";
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
		// return new ModelAndView("redirect:" + "https://tw.yahoo.com/");
	}
}
