
package tw.com.fstop.nnb.spring.controller;


import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import tw.com.fstop.nnb.custom.annotation.ISTXNLOG;
import tw.com.fstop.util.SessionUtil;


@Controller
@RequestMapping(value ="/FINANCIAL/TRIAL/NT")
public class Financial_Trial_NT_Controller {
	Logger log = LoggerFactory.getLogger(this.getClass());

	// 臺幣定期儲蓄存款試算_選擇頁
	@RequestMapping(value = "/deposit_trial")
		public String predesignated_account(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("deposit_trial-->");
		
		String target = "/financial_trial_nt/deposit_trial";			

		//清除切換語系時暫存的資料
		SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
		SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");		
				
		return target;
	}
	
	/**
	 * 存本取息
	 */
	@ISTXNLOG(value="A4021")
	@RequestMapping(value ="/deposit_interest")
	public String deposit_interest(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		return "/financial_trial_nt/deposit_interest";
	}
	
	/**
	 * 整存整付
	 */
	@ISTXNLOG(value="A4022")
	@RequestMapping(value ="/whole_deposit")
	public String whole_deposit(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		return "/financial_trial_nt/whole_deposit";
	}
	
	/**
	 * 零存整付
	 */
	@ISTXNLOG(value="A4023")
	@RequestMapping(value ="/scatter_deposit")
	public String scatter_deposit(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		return "/financial_trial_nt/scatter_deposit";
	} 
	
	
}
	 	