package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class N930EE_IDGATE_DATA implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -762951082705400880L;
	
	@SerializedName(value = "DPUSERID")
	private String DPUSERID;
	//動作
	@SerializedName(value = "EXECUTEFUNCTION")
	private String EXECUTEFUNCTION;

	@SerializedName(value = "NEW_EMAIL")
	private String NEW_EMAIL;

	public String getDPUSERID() {
		return DPUSERID;
	}

	public void setDPUSERID(String dPUSERID) {
		DPUSERID = dPUSERID;
	}

	public String getEXECUTEFUNCTION() {
		return EXECUTEFUNCTION;
	}

	public void setEXECUTEFUNCTION(String eXECUTEFUNCTION) {
		EXECUTEFUNCTION = eXECUTEFUNCTION;
	}

	public String getNEW_EMAIL() {
		return NEW_EMAIL;
	}

	public void setNEW_EMAIL(String nEW_EMAIL) {
		NEW_EMAIL = nEW_EMAIL;
	}
	
	
}
