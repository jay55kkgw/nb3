package tw.com.fstop.nnb.spring.controller;

import java.util.Base64;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import fstop.orm.po.TXNFUNDDATA;
import org.springframework.beans.factory.annotation.Autowired;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.service.Fund_Reserve_Redeem_Service;
import tw.com.fstop.nnb.service.Fund_Reserve_Transfer_Service;
import tw.com.fstop.nnb.service.Fund_Transfer_Service;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.web.util.WebUtil;

/**
 * 基金預約贖回交易的Controller
 */
@SessionAttributes({SessionUtil.CUSIDN,SessionUtil.KYCDATE,SessionUtil.WEAK,SessionUtil.XMLCOD,SessionUtil.DPMYEMAIL,SessionUtil.CONFIRM_LOCALE_DATA,SessionUtil.RESULT_LOCALE_DATA,SessionUtil.STEP1_LOCALE_DATA})
@Controller
@RequestMapping(value = "/FUND/RESERVE/REDEEM")
public class Fund_Reserve_Redeem_Controller{
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private Fund_Transfer_Service fund_transfer_service;
	@Autowired
	private Fund_Reserve_Redeem_Service fund_reserve_redeem_service;
	
	/**
	 * 前往基金預約贖回選擇頁
	 */
	@RequestMapping(value = "/fund_reserve_redeem_select")
	public String fund_reserve_redeem_select(HttpServletRequest request,@RequestParam Map<String,String> requestParam,Model model){
		String target = "/error";
		BaseResult bs=null;
		log.debug("fund_reserve_redeem_select");
		try{
			// 解決 Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
			log.debug(ESAPIUtil.vaildLog("REQPARAM >> {}"+CodeUtil.toJson(okMap)));
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			bs = new BaseResult();
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null));
				log.debug("LOCALE BSDATA >> {}",CodeUtil.toJson(bs));
			}else {
				//清空session
				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA,"");
				//do something
				bs = fund_reserve_redeem_service.prepareFundReserveRedeemSelectData(okMap);
				if(bs.getData()!=null)bs.setResult(Boolean.TRUE);
				//存入session
				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, bs);
				log.debug("BS DATA >>{}",CodeUtil.toJson(bs));
				log.debug("MODEL DATA >>{}",CodeUtil.toJson(model));
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("fund_reserve_redeem_select error >> {}",e);
		}finally {
			if(bs!=null && bs.getResult()) {
				model.addAttribute("bs",bs);
				target = "/fund/fund_reserve_redeem_select";
			}else {
				bs.setPrevious("/FUND/REDEEM/fund_redeem_data");
				model.addAttribute(BaseResult.ERROR,bs);
			}
		}
		return target;
	}
	/**
	 * 前往基金預約贖回確認頁
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/fund_reserve_redeem_confirm")
	public String fund_reserve_redeem_confirm(HttpServletRequest request,@RequestParam Map<String,String> requestParam,Model model){
		String target = "/error";
		BaseResult bs=null;
		log.debug("fund_reserve_redeem_confirm");
		try{			
			// 解決 Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
			log.debug(ESAPIUtil.vaildLog("REQPARAM >> {}"+CodeUtil.toJson(okMap)));
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			bs = new BaseResult();
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				log.debug("LOCALE BSDATA >> {}",CodeUtil.toJson(bs));
			}else {

				String cusidn = new String(Base64.getDecoder().decode((String)SessionUtil.getAttribute(model,SessionUtil.CUSIDN,null)),"UTF-8");
				String hiddenCUSIDN = WebUtil.hideID(cusidn);
				
				bs = fund_transfer_service.getN922Data(cusidn);
				
				if(bs != null && bs.getResult()){
					//清空session
					SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA,"");
					//do something
					bs.addData("hiddenCUSIDN",hiddenCUSIDN);	
					fund_reserve_redeem_service.prepareFundReserveRedeemConfirmData(okMap,bs);
					//存入session
					SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
					log.debug("BS DATA >>{}",CodeUtil.toJson(bs));
				}

			}	
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("fund_reserve_redeem_confirm error >> {}",e);
		}finally {
			if(bs!=null && bs.getResult()) {
				model.addAttribute("bs",bs);
				target = "/fund/fund_reserve_redeem_confirm";
			}else {
				bs.setPrevious("/FUND/REDEEM/fund_redeem_data");
				model.addAttribute(BaseResult.ERROR,bs);
			}
		}
		return target;
	}
	/**
	 * 前往基金預約贖回結果頁
	 */
	@RequestMapping(value = "/fund_reserve_redeem_result")
	public String fund_reserve_redeem_result(HttpServletRequest request,@RequestParam Map<String,String> requestParam,Model model){
		String target = "/error";
		BaseResult bs=null;
		log.debug("fund_reserve_redeem_result");
		try{
			// 解決 Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
			log.debug(ESAPIUtil.vaildLog("REQPARAM >> {}"+CodeUtil.toJson(okMap)));
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			bs = new BaseResult();
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				log.debug("LOCALE BSDATA >> {}",CodeUtil.toJson(bs));
			}else {
				//get session data
				String cusidn = new String(Base64.getDecoder().decode((String)SessionUtil.getAttribute(model,SessionUtil.CUSIDN,null)),"UTF-8");
				okMap.put("CUSIDN",cusidn);
				String hiddenCUSIDN = WebUtil.hideID(cusidn);
				//model.addAttribute("hiddenCUSIDN",hiddenCUSIDN);
				//寄件者信箱
				String DPMYEMAIL = (String)SessionUtil.getAttribute(model,SessionUtil.DPMYEMAIL,null);
				okMap.put("DPMYEMAIL",DPMYEMAIL);
				//收件者信箱
				okMap.put("CMTRMAIL",DPMYEMAIL);
				
				//清空session
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA,"");
				//do something
				//Warning: 內有new BaseResult
				String IP = WebUtil.getIpAddr(request);
				log.debug(ESAPIUtil.vaildLog("IP >> " + IP));
				okMap.put("IP",IP);
				bs = fund_reserve_redeem_service.processFundReserveRedeem(okMap);
				bs.addData("hiddenCUSIDN",hiddenCUSIDN);
				
				//20210322
				bs.addData("SKIP_P", "Y");
				
				//存入session
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				log.debug("BS DATA >>{}",CodeUtil.toJson(bs));
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("fund_reserve_redeem_result error >> {}",e);
		}finally {
			if(bs!=null && bs.getResult()) {
				model.addAttribute("bs",bs);
				target = "/fund/fund_reserve_redeem_result";
			}else {
				bs.setPrevious("/FUND/REDEEM/fund_redeem_data");
				model.addAttribute(BaseResult.ERROR,bs);
			}
		}
		return target;
	}
}