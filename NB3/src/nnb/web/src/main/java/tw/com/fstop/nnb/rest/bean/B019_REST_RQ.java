package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class B019_REST_RQ extends BaseRestBean_FUND implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6625697734094678791L;
	
	private String CUSIDN ;
	private String BRHCOD ;
	private String I01;//類別(1:檢核 2:確認)
	private String I02;//商品代號
	private String I03;//申購價格
	private String I04;//委買面額
	private String I05;//前手息(+/-)
	private String I06;//前手息
	private String I07;//手續費率
	private String I08;//預估手續費
	private String I09;//預估圈存金額
	private String I10;//扣款帳號
	private String I11;//轉介行員(中心)
	private String I12;//推薦行員(客戶)
	private String I13;//KYC行員(中心)
	private String IP;//IP位址
	private String I14;//投資人身份
	private String I15;//投資人風險等級
	private String I16;//風險預告書(Y/N)
	private String I17;//自主投資聲明書(Y/N)
	private String I18;//商品說明書(Y/N)
	private String I19;//高齡聲明書(Y/N)
	private String I20;//共用電子信箱聲明書(Y/N)
	
	private String ADOPID = "B019";
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getBRHCOD() {
		return BRHCOD;
	}
	public void setBRHCOD(String bRHCOD) {
		BRHCOD = bRHCOD;
	}
	public String getI01() {
		return I01;
	}
	public void setI01(String i01) {
		I01 = i01;
	}
	public String getI02() {
		return I02;
	}
	public void setI02(String i02) {
		I02 = i02;
	}
	public String getI03() {
		return I03;
	}
	public void setI03(String i03) {
		I03 = i03;
	}
	public String getI04() {
		return I04;
	}
	public void setI04(String i04) {
		I04 = i04;
	}
	public String getI05() {
		return I05;
	}
	public void setI05(String i05) {
		I05 = i05;
	}
	public String getI06() {
		return I06;
	}
	public void setI06(String i06) {
		I06 = i06;
	}
	public String getI07() {
		return I07;
	}
	public void setI07(String i07) {
		I07 = i07;
	}
	public String getI08() {
		return I08;
	}
	public void setI08(String i08) {
		I08 = i08;
	}
	public String getI09() {
		return I09;
	}
	public void setI09(String i09) {
		I09 = i09;
	}
	public String getI10() {
		return I10;
	}
	public void setI10(String i10) {
		I10 = i10;
	}
	public String getI11() {
		return I11;
	}
	public void setI11(String i11) {
		I11 = i11;
	}
	public String getI12() {
		return I12;
	}
	public void setI12(String i12) {
		I12 = i12;
	}
	public String getI13() {
		return I13;
	}
	public void setI13(String i13) {
		I13 = i13;
	}
	public String getIP() {
		return IP;
	}
	public void setIP(String iP) {
		IP = iP;
	}
	public String getI14() {
		return I14;
	}
	public void setI14(String i14) {
		I14 = i14;
	}
	public String getI15() {
		return I15;
	}
	public void setI15(String i15) {
		I15 = i15;
	}
	public String getI16() {
		return I16;
	}
	public void setI16(String i16) {
		I16 = i16;
	}
	public String getI17() {
		return I17;
	}
	public void setI17(String i17) {
		I17 = i17;
	}
	public String getI18() {
		return I18;
	}
	public void setI18(String i18) {
		I18 = i18;
	}
	public String getI19() {
		return I19;
	}
	public void setI19(String i19) {
		I19 = i19;
	}
	public String getI20() {
		return I20;
	}
	public void setI20(String i20) {
		I20 = i20;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
}
