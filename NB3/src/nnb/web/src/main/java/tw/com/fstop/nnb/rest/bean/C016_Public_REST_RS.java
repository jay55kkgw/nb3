package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * C016電文RS
 */
public class C016_Public_REST_RS extends BaseRestBean implements Serializable{
	private static final long serialVersionUID = 6791228050721094802L;
	
	private String DATE;
	private String TIME;
	private String CUSIDN;
	private String CUSNAME;
	private String TRANSCODE;
	private String COUNTRYTYPE;
	private String TRADEDATE;
	private String AMT3;
	private String FCA2;
	private String HTELPHONE;
	private String OTELPHONE;
	private String AMT5;
	private String BILLSENDMODE;
	private String FCAFEE;
	private String SSLTXNO;
	private String PAYDAY1;
	private String PAYDAY2;
	private String PAYDAY3;
	private String BRHCOD;
	private String CUTTYPE;
	private String FUDCUR;
	private String DBDATE;
	private String STOP;
	private String YIELD;
	private String LINK_FUND_HOUSE;
	private String LINK_FUND_CODE;
	private String LINK_FEE_01;
	private String LINK_FEE_02;
	private String LINK_FEE_03;
	private String LINK_FEE_04;
	private String LINK_FEE_05;
	private String LINK_FEE_07;
	private String LINK_SLS_01;
	private String LINK_SLS_02;
	private String LINK_SLS_03;
	private String LINK_SLS_04;
	private String LINK_SLS_05;
	private String LINK_SLS_06;
	private String LINK_SLS_07;
	private String LINK_TRN_01;
	private String LINK_TRN_02;
	private String LINK_OTHER_04;
	private String LINK_OTHER_05;
	private String LINK_SLS_08;
	private String LINK_BASE_01;
	private String LINK_RESULT_01;
	private String LINK_RESULT_02;
	private String LINK_RESULT_03;
	private String LINK_RESULT_04;
	private String LINK_RESULT_05;
	private String LINK_RESULT_06;
	private String LINK_FEE_BSHARE;
	private String LINK_FUND_TYPE;
	private String LINK_FH_NAME;
	private String LINK_FUND_NAME;
	private String LINK_DATE;
	private String MAC;
	private String PAYDAY4;
	private String PAYDAY5;
	private String PAYDAY6;
	private String MIP;
	private String PAYDAY7;
	private String PAYDAY8;
	private String PAYDAY9;
	private String CMQTIME;
	private String SHWD;//視窗註記 2020/07/02新增
	private String XFLAG;
	private String OFLAG; //加填"高齡聲明書"
	private String SAL01; //薪轉戶優惠額度
	private String SAL02; //薪轉戶優惠剩餘額度
	private String SAL03; //是否顯示薪轉戶視窗
	
	public String getDATE(){
		return DATE;
	}
	public void setDATE(String dATE){
		DATE = dATE;
	}
	public String getTIME(){
		return TIME;
	}
	public void setTIME(String tIME){
		TIME = tIME;
	}
	public String getCUSIDN(){
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN){
		CUSIDN = cUSIDN;
	}
	public String getCUSNAME(){
		return CUSNAME;
	}
	public void setCUSNAME(String cUSNAME){
		CUSNAME = cUSNAME;
	}
	public String getTRANSCODE(){
		return TRANSCODE;
	}
	public void setTRANSCODE(String tRANSCODE){
		TRANSCODE = tRANSCODE;
	}
	public String getCOUNTRYTYPE(){
		return COUNTRYTYPE;
	}
	public void setCOUNTRYTYPE(String cOUNTRYTYPE){
		COUNTRYTYPE = cOUNTRYTYPE;
	}
	public String getTRADEDATE(){
		return TRADEDATE;
	}
	public void setTRADEDATE(String tRADEDATE){
		TRADEDATE = tRADEDATE;
	}
	public String getAMT3(){
		return AMT3;
	}
	public void setAMT3(String aMT3){
		AMT3 = aMT3;
	}
	public String getFCA2(){
		return FCA2;
	}
	public void setFCA2(String fCA2){
		FCA2 = fCA2;
	}
	public String getHTELPHONE(){
		return HTELPHONE;
	}
	public void setHTELPHONE(String hTELPHONE){
		HTELPHONE = hTELPHONE;
	}
	public String getOTELPHONE(){
		return OTELPHONE;
	}
	public void setOTELPHONE(String oTELPHONE){
		OTELPHONE = oTELPHONE;
	}
	public String getAMT5(){
		return AMT5;
	}
	public void setAMT5(String aMT5){
		AMT5 = aMT5;
	}
	public String getBILLSENDMODE(){
		return BILLSENDMODE;
	}
	public void setBILLSENDMODE(String bILLSENDMODE){
		BILLSENDMODE = bILLSENDMODE;
	}
	public String getFCAFEE(){
		return FCAFEE;
	}
	public void setFCAFEE(String fCAFEE){
		FCAFEE = fCAFEE;
	}
	public String getSSLTXNO(){
		return SSLTXNO;
	}
	public void setSSLTXNO(String sSLTXNO){
		SSLTXNO = sSLTXNO;
	}
	public String getPAYDAY1(){
		return PAYDAY1;
	}
	public void setPAYDAY1(String pAYDAY1){
		PAYDAY1 = pAYDAY1;
	}
	public String getPAYDAY2(){
		return PAYDAY2;
	}
	public void setPAYDAY2(String pAYDAY2){
		PAYDAY2 = pAYDAY2;
	}
	public String getPAYDAY3(){
		return PAYDAY3;
	}
	public void setPAYDAY3(String pAYDAY3){
		PAYDAY3 = pAYDAY3;
	}
	public String getBRHCOD(){
		return BRHCOD;
	}
	public void setBRHCOD(String bRHCOD){
		BRHCOD = bRHCOD;
	}
	public String getCUTTYPE(){
		return CUTTYPE;
	}
	public void setCUTTYPE(String cUTTYPE){
		CUTTYPE = cUTTYPE;
	}
	public String getFUDCUR(){
		return FUDCUR;
	}
	public void setFUDCUR(String fUDCUR){
		FUDCUR = fUDCUR;
	}
	public String getDBDATE(){
		return DBDATE;
	}
	public void setDBDATE(String dBDATE){
		DBDATE = dBDATE;
	}
	public String getSTOP(){
		return STOP;
	}
	public void setSTOP(String sTOP){
		STOP = sTOP;
	}
	public String getYIELD(){
		return YIELD;
	}
	public void setYIELD(String yIELD){
		YIELD = yIELD;
	}
	public String getLINK_FUND_HOUSE(){
		return LINK_FUND_HOUSE;
	}
	public void setLINK_FUND_HOUSE(String lINK_FUND_HOUSE){
		LINK_FUND_HOUSE = lINK_FUND_HOUSE;
	}
	public String getLINK_FUND_CODE(){
		return LINK_FUND_CODE;
	}
	public void setLINK_FUND_CODE(String lINK_FUND_CODE){
		LINK_FUND_CODE = lINK_FUND_CODE;
	}
	public String getLINK_FEE_01(){
		return LINK_FEE_01;
	}
	public void setLINK_FEE_01(String lINK_FEE_01){
		LINK_FEE_01 = lINK_FEE_01;
	}
	public String getLINK_FEE_02(){
		return LINK_FEE_02;
	}
	public void setLINK_FEE_02(String lINK_FEE_02){
		LINK_FEE_02 = lINK_FEE_02;
	}
	public String getLINK_FEE_03(){
		return LINK_FEE_03;
	}
	public void setLINK_FEE_03(String lINK_FEE_03){
		LINK_FEE_03 = lINK_FEE_03;
	}
	public String getLINK_FEE_04(){
		return LINK_FEE_04;
	}
	public void setLINK_FEE_04(String lINK_FEE_04){
		LINK_FEE_04 = lINK_FEE_04;
	}
	public String getLINK_FEE_05(){
		return LINK_FEE_05;
	}
	public void setLINK_FEE_05(String lINK_FEE_05){
		LINK_FEE_05 = lINK_FEE_05;
	}
	public String getLINK_FEE_07(){
		return LINK_FEE_07;
	}
	public void setLINK_FEE_07(String lINK_FEE_07){
		LINK_FEE_07 = lINK_FEE_07;
	}
	public String getLINK_SLS_01(){
		return LINK_SLS_01;
	}
	public void setLINK_SLS_01(String lINK_SLS_01){
		LINK_SLS_01 = lINK_SLS_01;
	}
	public String getLINK_SLS_02(){
		return LINK_SLS_02;
	}
	public void setLINK_SLS_02(String lINK_SLS_02){
		LINK_SLS_02 = lINK_SLS_02;
	}
	public String getLINK_SLS_03(){
		return LINK_SLS_03;
	}
	public void setLINK_SLS_03(String lINK_SLS_03){
		LINK_SLS_03 = lINK_SLS_03;
	}
	public String getLINK_SLS_04(){
		return LINK_SLS_04;
	}
	public void setLINK_SLS_04(String lINK_SLS_04){
		LINK_SLS_04 = lINK_SLS_04;
	}
	public String getLINK_SLS_05(){
		return LINK_SLS_05;
	}
	public void setLINK_SLS_05(String lINK_SLS_05){
		LINK_SLS_05 = lINK_SLS_05;
	}
	public String getLINK_SLS_06(){
		return LINK_SLS_06;
	}
	public void setLINK_SLS_06(String lINK_SLS_06){
		LINK_SLS_06 = lINK_SLS_06;
	}
	public String getLINK_SLS_07(){
		return LINK_SLS_07;
	}
	public void setLINK_SLS_07(String lINK_SLS_07){
		LINK_SLS_07 = lINK_SLS_07;
	}
	public String getLINK_TRN_01(){
		return LINK_TRN_01;
	}
	public void setLINK_TRN_01(String lINK_TRN_01){
		LINK_TRN_01 = lINK_TRN_01;
	}
	public String getLINK_TRN_02(){
		return LINK_TRN_02;
	}
	public void setLINK_TRN_02(String lINK_TRN_02){
		LINK_TRN_02 = lINK_TRN_02;
	}
	public String getLINK_OTHER_04(){
		return LINK_OTHER_04;
	}
	public void setLINK_OTHER_04(String lINK_OTHER_04){
		LINK_OTHER_04 = lINK_OTHER_04;
	}
	public String getLINK_OTHER_05(){
		return LINK_OTHER_05;
	}
	public void setLINK_OTHER_05(String lINK_OTHER_05){
		LINK_OTHER_05 = lINK_OTHER_05;
	}
	public String getLINK_SLS_08(){
		return LINK_SLS_08;
	}
	public void setLINK_SLS_08(String lINK_SLS_08){
		LINK_SLS_08 = lINK_SLS_08;
	}
	public String getLINK_BASE_01(){
		return LINK_BASE_01;
	}
	public void setLINK_BASE_01(String lINK_BASE_01){
		LINK_BASE_01 = lINK_BASE_01;
	}
	public String getLINK_RESULT_01(){
		return LINK_RESULT_01;
	}
	public void setLINK_RESULT_01(String lINK_RESULT_01){
		LINK_RESULT_01 = lINK_RESULT_01;
	}
	public String getLINK_RESULT_02(){
		return LINK_RESULT_02;
	}
	public void setLINK_RESULT_02(String lINK_RESULT_02){
		LINK_RESULT_02 = lINK_RESULT_02;
	}
	public String getLINK_RESULT_03(){
		return LINK_RESULT_03;
	}
	public void setLINK_RESULT_03(String lINK_RESULT_03){
		LINK_RESULT_03 = lINK_RESULT_03;
	}
	public String getLINK_RESULT_04(){
		return LINK_RESULT_04;
	}
	public void setLINK_RESULT_04(String lINK_RESULT_04){
		LINK_RESULT_04 = lINK_RESULT_04;
	}
	public String getLINK_RESULT_05(){
		return LINK_RESULT_05;
	}
	public void setLINK_RESULT_05(String lINK_RESULT_05){
		LINK_RESULT_05 = lINK_RESULT_05;
	}
	public String getLINK_RESULT_06(){
		return LINK_RESULT_06;
	}
	public void setLINK_RESULT_06(String lINK_RESULT_06){
		LINK_RESULT_06 = lINK_RESULT_06;
	}
	public String getLINK_FEE_BSHARE(){
		return LINK_FEE_BSHARE;
	}
	public void setLINK_FEE_BSHARE(String lINK_FEE_BSHARE){
		LINK_FEE_BSHARE = lINK_FEE_BSHARE;
	}
	public String getLINK_FUND_TYPE(){
		return LINK_FUND_TYPE;
	}
	public void setLINK_FUND_TYPE(String lINK_FUND_TYPE){
		LINK_FUND_TYPE = lINK_FUND_TYPE;
	}
	public String getLINK_FH_NAME(){
		return LINK_FH_NAME;
	}
	public void setLINK_FH_NAME(String lINK_FH_NAME){
		LINK_FH_NAME = lINK_FH_NAME;
	}
	public String getLINK_FUND_NAME(){
		return LINK_FUND_NAME;
	}
	public void setLINK_FUND_NAME(String lINK_FUND_NAME){
		LINK_FUND_NAME = lINK_FUND_NAME;
	}
	public String getLINK_DATE(){
		return LINK_DATE;
	}
	public void setLINK_DATE(String lINK_DATE){
		LINK_DATE = lINK_DATE;
	}
	public String getMAC(){
		return MAC;
	}
	public void setMAC(String mAC){
		MAC = mAC;
	}
	public String getPAYDAY4(){
		return PAYDAY4;
	}
	public void setPAYDAY4(String pAYDAY4){
		PAYDAY4 = pAYDAY4;
	}
	public String getPAYDAY5(){
		return PAYDAY5;
	}
	public void setPAYDAY5(String pAYDAY5){
		PAYDAY5 = pAYDAY5;
	}
	public String getPAYDAY6(){
		return PAYDAY6;
	}
	public void setPAYDAY6(String pAYDAY6){
		PAYDAY6 = pAYDAY6;
	}
	public String getMIP(){
		return MIP;
	}
	public void setMIP(String mIP){
		MIP = mIP;
	}
	public String getPAYDAY7(){
		return PAYDAY7;
	}
	public void setPAYDAY7(String pAYDAY7){
		PAYDAY7 = pAYDAY7;
	}
	public String getPAYDAY8(){
		return PAYDAY8;
	}
	public void setPAYDAY8(String pAYDAY8){
		PAYDAY8 = pAYDAY8;
	}
	public String getPAYDAY9(){
		return PAYDAY9;
	}
	public void setPAYDAY9(String pAYDAY9){
		PAYDAY9 = pAYDAY9;
	}
	public String getCMQTIME(){
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME){
		CMQTIME = cMQTIME;
	}
	public String getSHWD() {
		return SHWD;
	}
	public void setSHWD(String sHWD) {
		SHWD = sHWD;
	}
	public String getXFLAG() {
		return XFLAG;
	}
	public void setXFLAG(String xFLAG) {
		XFLAG = xFLAG;
	}
	public String getOFLAG() {
		return OFLAG;
	}
	public void setOFLAG(String oFLAG) {
		OFLAG = oFLAG;
	}
	public String getSAL01() {
		return SAL01;
	}
	public void setSAL01(String sAL01) {
		SAL01 = sAL01;
	}
	public String getSAL02() {
		return SAL02;
	}
	public void setSAL02(String sAL02) {
		SAL02 = sAL02;
	}
	public String getSAL03() {
		return SAL03;
	}
	public void setSAL03(String sAL03) {
		SAL03 = sAL03;
	}
}