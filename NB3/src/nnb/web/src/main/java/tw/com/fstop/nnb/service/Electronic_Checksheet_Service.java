package tw.com.fstop.nnb.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.po.TXNUSER;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.TxnUserDao;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.util.StrUtil;

@Service
public class Electronic_Checksheet_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private I18n i18n;
	
	@Autowired
	private TxnUserDao txnuserdao;

	/**
	 * 檢查是否有申請電子帳單
	 * 
	 * @return
	 */
	public boolean isDPUser(String cusidn) {
		boolean isDPUser = false;

		// 取得TXNUSER用戶資料
		List<TXNUSER> txnuserList = txnuserdao.findByUserId(cusidn);
		if (!txnuserList.isEmpty()) {
			TXNUSER txnuser = txnuserList.get(0);
			// 是DPUSER
			if (txnuser.getDPBILL().equals("Y") || txnuser.getDPFUNDBILL().equals("Y")
					|| txnuser.getDPCARDBILL().equals("Y")) {
				isDPUser = true;
			}
		}
		return isDPUser;
	}

	/**
	 * 
	 * 
	 * @param custid
	 * @param
	 * @return
	 */
	public BaseResult apply_bill_input(Map<String, String> reqParam) {
		log.trace("apply_bill_input ...");
		log.trace(ESAPIUtil.vaildLog("custid: " + reqParam.get("CUSIDN")));
		
		BaseResult bs = null;
		BaseResult bsN922 = null;
		BaseResult bsN810 = null;
		BaseResult bsN930 = null;
		BaseResult bsC015 = null;
		boolean n922status = true;
		
		//JSP頁面判斷值
		String dpbill = "";
		String dpcardbill = "";
		String dpfundbill = "";

		boolean b_DisplayCard = true;
		boolean b_C015Applied = true;
		boolean b_ApplyNBFund = true;
		boolean b_DPBILL = true;
		try {
			bs = new BaseResult();
			bsN922 = new BaseResult();
			bsN810 = new BaseResult();
			bsN930 = new BaseResult();
			bsC015 = new BaseResult();
			// 取得TXNUSER用戶資料
			// 同於舊網銀code之CommonPools.n931.getTXNUSERBILL((String)session.getAttribute("UID"));
			List<TXNUSER> txnuserList = txnuserdao.findByUserId(reqParam.get("CUSIDN"));
			if (!txnuserList.isEmpty()) {
				TXNUSER txnuser = txnuserList.get(0);
				dpbill = txnuser.getDPBILL();
				dpcardbill = txnuser.getDPCARDBILL();
				dpfundbill = txnuser.getDPFUNDBILL();
			}

			// 電文N922 判斷有無申請網銀基金下單功能
			bsN922 = N922_REST(reqParam.get("CUSIDN"));
			if (!"".equals(((Map<String, String>) bsN922.getData()).get("TOPMSG"))) {
				b_ApplyNBFund = false;
				// 加此判斷 若是舊網銀n922出現exception情形 n922 = null
				n922status = false;
			} else {
				b_ApplyNBFund = true;
				n922status = true;
			}

			// 電文N810 判斷有無持有本行信用卡
			//Avoid TXNLOG
			reqParam.put("ADOPID", "__N810");
			bsN810 = N810_REST(reqParam);                                               //防止測試機服務未開
//			if ("E091".equals(((Map<String, String>) bsN810.getData()).get("msgCode"))||"E099".equals(((Map<String, String>) bsN810.getData()).get("msgCode"))) {
			if (!"".equals(((Map<String, String>) bsN810.getData()).get("TOPMSG"))) {
				b_DisplayCard = false;
			}

			// 判斷有無臨櫃申請網銀電子帳單
			Map n930reqParam = new HashMap();
			n930reqParam.put("DATE", DateUtil.getTWDate(""));
			n930reqParam.put("TIME", DateUtil.getTheTime(""));
			n930reqParam.put("FLAG", "01");
			n930reqParam.put("EXECUTEFUNCTION", "n930query");
			n930reqParam.put("CUSIDN", reqParam.get("CUSIDN"));
			n930reqParam.put("DPUSERID", reqParam.get("CUSIDN"));
			n930reqParam.put("IP", reqParam.get("IP"));

			bsN930 = call_N930EE_REST(n930reqParam);

			if ("0".equals(((Map<String, String>) bsN930.getData()).get("msgCode"))) {
				if ("Y".equals(((Map<String, String>) bsN930.getData()).get("E_BILL"))) {
					b_DPBILL = true;
				} else {
					b_DPBILL = false;
				}
			} else {
				b_DPBILL = false;
			}

			bsC015 = C015_REST(reqParam);
			// 若尚未 "臨櫃申請" 基金對帳單 (BILLSENDMODE <> "2")
			if ("0".equals(((Map<String, String>) bsC015.getData()).get("msgCode"))) {
				if (!"2".equals(((Map<String, String>) bsC015.getData()).get("BILLSENDMODE"))) {

					b_C015Applied = false;
					// 舊網銀 N1010.jsp將此段註解
					// htl_Data.put("BILLSENDMODE", "2");
					// htl_Data.put("BRHCOD", n922.getValueByFieldName("APLBRH"));
					// htl_Data.put("ZIPCODE", c015_Helper.getValueByFieldName("ZIPCODE"));
					// htl_Data.put("HTELPHONE", c015_Helper.getValueByFieldName("HTELPHONE"));
					// htl_Data.put("OTELPHONE", c015_Helper.getValueByFieldName("OTELPHONE"));
					// htl_Data.put("MTELPHONE", c015_Helper.getValueByFieldName("MTELPHONE"));
					// htl_Data.put("EMAIL", c015_Helper.getValueByFieldName("EMAIL"));
					// htl_Data.put("ADDRESS", c015_Helper.getValueByFieldName("ADDRESS"));
					// if(StrUtils.isNotEmpty((String)session.getAttribute("TRANSPASSUPDATE")))
					// {
					// htl_Result.put("PHASE1",(String)session.getAttribute("PHASE1"));
					// htl_Result.put("PHASE2",(String)session.getAttribute("PHASE2"));
					// htl_Result.put("TRANSPASSUPDATE",(String)session.getAttribute("TRANSPASSUPDATE"));
					// }
					//
					// c113.doAction(htl_Data);
				} else {
					b_C015Applied = true;

					// 若網銀與臨櫃申請基金對帳單之狀態不同,須同步更新 -> DB
					if ((n922status != false) && b_ApplyNBFund && (!"Y".equals(dpfundbill))) {
						if (!txnuserList.isEmpty()) {
							TXNUSER txnuser = txnuserList.get(0);
							txnuser.setDPFUNDBILL("Y");
							txnuserdao.saveOrUpdate(txnuser);
							
						}

					}
				}
			} else {
				b_C015Applied = false;
			}
			
			String Type1Flag="";
			String Type2Flag="";
			String Type3Flag="";
			String ApplyFlag="";
			String Type4Flag="";
			String Type5Flag="";
			String Type6Flag="";
			String CancelFlag="";
			
			Type1Flag = (b_DPBILL)?"disabled":"";
			Type2Flag = ("Y".equals(dpcardbill))?"disabled":"";
			Type3Flag = ("Y".equals(dpfundbill))?"disabled":"";
			if ("disabled".equals(Type1Flag) && "disabled".equals(Type2Flag) && "disabled".equals(Type3Flag)) {
				ApplyFlag = "disabled";
			}	 		
			Type4Flag = (!b_DPBILL)?"disabled":"";
			Type5Flag = (!"Y".equals(dpcardbill))?"disabled":"";
			Type6Flag = ((!"Y".equals(dpfundbill))&&(!b_C015Applied))?"disabled":"";
			if ("disabled".equals(Type4Flag) && "disabled".equals(Type5Flag) && "disabled".equals(Type6Flag)) {
				CancelFlag = "disabled";
			}
			
			
			//回傳頁面所需判斷值
			bs.addData("b_DPBILL", b_DPBILL);
			bs.addData("b_C015Applied", b_C015Applied);
			bs.addData("b_DisplayCard", b_DisplayCard);
			bs.addData("b_ApplyNBFund", b_ApplyNBFund);
			bs.addData("dpbill", dpbill );
			bs.addData("dpcardbill", dpcardbill);
			bs.addData("dpfundbill", dpfundbill);
			bs.addData("Type1Flag", Type1Flag);
			bs.addData("Type2Flag", Type2Flag);
			bs.addData("Type3Flag", Type3Flag);
			bs.addData("Type4Flag", Type4Flag);
			bs.addData("Type5Flag", Type5Flag);
			bs.addData("Type6Flag", Type6Flag);
			bs.addData("ApplyFlag", ApplyFlag);
			bs.addData("CancelFlag", CancelFlag);
			bs.setResult(true);

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("apply_bill_input error >> {}",e);
		}
		// 回傳處理結果
		return bs;
	}

	/**
	 * 
	 * 
	 * @param custid
	 * @param
	 * @return
	 */
	public BaseResult cancel_confirm(Map<String, String> reqParam) {
		log.trace("cancel_confirm ...");
		log.trace(ESAPIUtil.vaildLog("custid: " + reqParam.get("CUSIDN")));
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs.setData(reqParam);
			bs.setResult(true);
		}catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("cancel_confirm error >> {}",e);
		}
		return bs ;
	}
	
	
	/**
	 * 
	 * 
	 * @param custid
	 * @param
	 * @return
	 */
	public BaseResult apply_confirm(Map<String, String> reqParam) {
		log.trace("apply_confirm ...");
		log.trace(ESAPIUtil.vaildLog("custid: " + reqParam.get("CUSIDN")));
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs.setData(reqParam);
			bs.setResult(true);
		}catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("apply_confirm error >> {}",e);
		}
		return bs ;
	}
	
	/**
	 * 
	 * 
	 * @param custid
	 * @param
	 * @return
	 */
	public BaseResult cancel_result(Map<String, String> reqParam) {
		log.trace("cancel_result ...");
		log.trace(ESAPIUtil.vaildLog("custid: " + reqParam.get("CUSIDN")));
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			
			bs = N931_REST(reqParam);
		}catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("cancel_result error >> {}",e);
		}
		return bs ;
	}
	
	/**
	 * 
	 * 
	 * @param custid
	 * @param
	 * @return
	 */
	public BaseResult apply_result(Map<String, String> reqParam) {
		log.trace("apply_result ...");
		log.trace(ESAPIUtil.vaildLog("custid: " + reqParam.get("CUSIDN")));
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			
			bs = N931_REST(reqParam);
		}catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("apply_result error >> {}",e);
		}
		return bs ;
	}
	
	/**
	 * 
	 * 
	 * @param custid
	 * @param
	 * @return
	 */
	public BaseResult reset_result(Map<String, String> reqParam) {
		log.trace("reset_result ...");
		log.trace(ESAPIUtil.vaildLog("custid: " + reqParam.get("CUSIDN")));
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			
			bs = N1010RP_REST(reqParam);
			
			Map<String,String>mapN1010_RP = (Map<String, String>) bs.getData();
			
			if(null==mapN1010_RP.get("BHRPResult")) {
				bs.setData(mapN1010_RP);
				bs.setResult(false);
			}else {
				mapN1010_RP.put("BHRPResult", this.getBeanMapValue("N1010_RP", mapN1010_RP.get("BHRPResult")));
				bs.setData(mapN1010_RP);
				bs.setResult(true);
			}
		}catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("reset_result error >> {}",e);
		}
		return bs ;
	}
	
	/**
	 * 
	 * 
	 * @param custid
	 * @param
	 * @return
	 */
	public BaseResult alter_result(Map<String, String> reqParam) {
		log.trace("alter_result ...");
		log.trace(ESAPIUtil.vaildLog("custid: " + reqParam.get("CUSIDN")));
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = N1010CP_REST(reqParam);
			Map<String,String>mapN1010_CP = (Map<String, String>) bs.getData();
			if(null==mapN1010_CP.get("BHCPResult")) {
				bs.setData(mapN1010_CP);
				bs.setResult(false);
			}else {
				mapN1010_CP.put("BHCPResult", this.getBeanMapValue("N1010_RP", mapN1010_CP.get("BHCPResult")));
				bs.setData(mapN1010_CP);
				bs.setResult(true);
			}
		}catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("alter_result error >> {}",e);
		}
		return bs ;
	}
	
	
	public BaseResult elec_bill_resent_result(Map<String, String> reqParam) {
		log.trace("elec_bill_resent_result ...");
		log.trace(ESAPIUtil.vaildLog("custid: " + reqParam.get("CUSIDN")));
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = N660_REST(reqParam);
			String cusidn = reqParam.get("CUSIDN");
			String psString="";
			if(cusidn.length()==10) {
				psString=i18n.getMsg("LB.D0581");
			}else {
				psString=i18n.getMsg("LB.X2142");
			}
			bs.addData("psString", psString);
		}catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("elec_bill_resent_result error >> {}",e);
		}
		return bs ;
	}
	
	public BaseResult aio_bill_query(String cusidn) {
		log.trace("aio_bill_query ...");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			Map<String,String> reqParam = new HashMap<String,String>();
			reqParam.put("Type", "01");
			reqParam.put("cUkey", cusidn);
			bs = Aioedm_REST(reqParam);
			
			Map<String,Object> bsData = (Map<String,Object>)bs.getData();
			String[] dateSplit = ((String)bsData.get("cDate")).split(";");
			String[] eMailSplit = ((String)bsData.get("cEmail")).split(";");
			
			List<Map<String,String>> rec = new ArrayList<Map<String,String>>();
			for(int i = 0;i < dateSplit.length;i++) {
				Map<String,String> data = new HashMap<String,String>();
				data.put("cDate", dateSplit[i]);
				data.put("cEmail", eMailSplit[i]);
				rec.add(data);
			}
			bs.addData("REC", rec);
			bs.setMessage((String)bsData.get("rtnCode"), (String)bsData.get("rtnDesc"));
			if(!((String)bsData.get("rtnCode")).equals("0")){
				bs.setResult(false);
			}
		}catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("elec_bill_resent_result error >> {}",e);
		}
		return bs ;
	}
	public BaseResult aio_bill_resend(Map<String,String> reqParam) {
		log.trace("aio_bill_resend ...");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			reqParam.put("Type", "02");
			bs = Aioedm_REST(reqParam);
			
		}catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("elec_bill_resent_result error >> {}",e);
		}
		return bs ;
	}
	

	/**
	 * 輸入beanId && beanMapKey Get beanMapValue
	 * 
	 * @param beanId
	 * @param beanMapKey
	 * @see spring-arg.xml
	 *  @return get spring-arg.xml beanMapValue By beanId && beanMapKey, if any (or
	 *         empty String otherwise)
	 */
	public String getBeanMapValue(String beanId, String beanMapKey)
	{
		// 取得Bean中的Map
		Map<String, String> getTypeBean = SpringBeanFactory.getBean(beanId);
		// 取得Map中的Value
		String getTypeBeanValue = getTypeBean.get(beanMapKey);
		return getTypeBeanValue;
	}
}
