package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N650_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4974778999205998075L;

	String CMQTIME;						//查詢時間
	String CMRECNUM;					//查詢筆數
	String ABEND;						//結束代碼
	String REC_NO;						//筆數
	String __OCCURS;
	String USERDATA_X50;				//暫存空間區
	
	String QUERYNEXT;
	String DONEACNOS;
	LinkedList<N650_REST_RSDATA> REC;

	public String getQUERYNEXT() {
		return QUERYNEXT;
	}

	public void setQUERYNEXT(String qUERYNEXT) {
		QUERYNEXT = qUERYNEXT;
	}

	public String getDONEACNOS() {
		return DONEACNOS;
	}

	public void setDONEACNOS(String dONEACNOS) {
		DONEACNOS = dONEACNOS;
	}

	public String getCMQTIME() {
		return CMQTIME;
	}

	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}

	public String getCMRECNUM() {
		return CMRECNUM;
	}

	public void setCMRECNUM(String cMRECNUM) {
		CMRECNUM = cMRECNUM;
	}

	public String getABEND() {
		return ABEND;
	}

	public void setABEND(String aBEND) {
		ABEND = aBEND;
	}

	public String getREC_NO() {
		return REC_NO;
	}

	public void setREC_NO(String rEC_NO) {
		REC_NO = rEC_NO;
	}

	public String get__OCCURS() {
		return __OCCURS;
	}

	public void set__OCCURS(String __OCCURS) {
		this.__OCCURS = __OCCURS;
	}

	public String getUSERDATA_X50() {
		return USERDATA_X50;
	}

	public void setUSERDATA_X50(String uSERDATA_X50) {
		USERDATA_X50 = uSERDATA_X50;
	}

	public LinkedList<N650_REST_RSDATA> getREC() {
		return REC;
	}

	public void setREC(LinkedList<N650_REST_RSDATA> rEC) {
		REC = rEC;
	}
}
