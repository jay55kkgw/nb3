
package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N931_REST_RQ extends BaseRestBean_CC implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5051219122081175767L;
	
	
	//電文需要
	private String CUSIDN;//統一編號
	private String TYPE ; 
	private String ITEM ;
	private String PINNEW;
	private String E_MAIL_ADR;
	private String CARDAP;
	
	//ms_tw需要
	private String APPLYFLAG;
	private String TYPE1;
	private String TYPE2;
	private String TYPE3;
	private String TYPE4;
	private String TYPE5;
	private String TYPE6;
	private String BILLSENDMODE;
	private String FGTXWAY;
	private String DPMYEMAIL;
	
	//BillHunter 需要
	private String Cust_id;
	private String Status;
	private String Email;
	private String sys_ip;
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	public String getTYPE() {
		return TYPE;
	}
	public String getITEM() {
		return ITEM;
	}
	public String getAPPLYFLAG() {
		return APPLYFLAG;
	}
	public String getTYPE1() {
		return TYPE1;
	}
	public String getTYPE2() {
		return TYPE2;
	}
	public String getTYPE3() {
		return TYPE3;
	}
	public String getTYPE4() {
		return TYPE4;
	}
	public String getTYPE5() {
		return TYPE5;
	}
	public String getTYPE6() {
		return TYPE6;
	}
	public String getCust_id() {
		return Cust_id;
	}
	public String getStatus() {
		return Status;
	}
	public String getEmail() {
		return Email;
	}
	public String getSys_ip() {
		return sys_ip;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}
	public void setITEM(String iTEM) {
		ITEM = iTEM;
	}
	public void setAPPLYFLAG(String aPPLYFLAG) {
		APPLYFLAG = aPPLYFLAG;
	}
	public void setTYPE1(String tYPE1) {
		TYPE1 = tYPE1;
	}
	public void setTYPE2(String tYPE2) {
		TYPE2 = tYPE2;
	}
	public void setTYPE3(String tYPE3) {
		TYPE3 = tYPE3;
	}
	public void setTYPE4(String tYPE4) {
		TYPE4 = tYPE4;
	}
	public void setTYPE5(String tYPE5) {
		TYPE5 = tYPE5;
	}
	public void setTYPE6(String tYPE6) {
		TYPE6 = tYPE6;
	}
	public void setCust_id(String cust_id) {
		Cust_id = cust_id;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public void setSys_ip(String sys_ip) {
		this.sys_ip = sys_ip;
	}
	public String getPINNEW() {
		return PINNEW;
	}
	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}
	public String getBILLSENDMODE() {
		return BILLSENDMODE;
	}
	public void setBILLSENDMODE(String bILLSENDMODE) {
		BILLSENDMODE = bILLSENDMODE;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public String getDPMYEMAIL() {
		return DPMYEMAIL;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public void setDPMYEMAIL(String dPMYEMAIL) {
		DPMYEMAIL = dPMYEMAIL;
	}
	public String getE_MAIL_ADR() {
		return E_MAIL_ADR;
	}
	public void setE_MAIL_ADR(String e_MAIL_ADR) {
		E_MAIL_ADR = e_MAIL_ADR;
	}
	public String getCARDAP() {
		return CARDAP;
	}
	public void setCARDAP(String cARDAP) {
		CARDAP = cARDAP;
	}
}
