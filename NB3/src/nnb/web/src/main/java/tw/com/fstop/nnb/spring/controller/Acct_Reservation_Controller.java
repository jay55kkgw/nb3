package tw.com.fstop.nnb.spring.controller;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.idgate.bean.N283_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N283_IDGATE_DATA_VIEW;
import tw.com.fstop.nnb.custom.annotation.ACNVERIFY;
import tw.com.fstop.nnb.custom.annotation.ISTXNLOG;
import tw.com.fstop.nnb.service.Acct_Reservation_Service;
import tw.com.fstop.nnb.service.IdGateService;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.web.util.WebUtil;


@SessionAttributes({SessionUtil.PD ,SessionUtil.PRINT_DATALISTMAP_DATA,SessionUtil.DOWNLOAD_DATALISTMAP_DATA,SessionUtil.CONFIRM_LOCALE_DATA ,SessionUtil.RESULT_LOCALE_DATA,SessionUtil.CUSIDN,SessionUtil.IDGATE_TRANSDATA})
@Controller
@RequestMapping(value = "/NT/ACCT/RESERVATION")
public class Acct_Reservation_Controller {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private Acct_Reservation_Service acct_reservation_service;
	
	@Autowired
	IdGateService idgateservice;


	@ISTXNLOG(value="N283")
	@RequestMapping(value = "/reservation_detail")
	public String reservation_detail(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("reservation_detail>>");
		
//		清除切換語系時暫存的資料
		SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
		SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
		
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			log.debug("reservation_detail.cusidn >>{}", cusidn);
// 			解決Trust Boundary Violation
			Map<String, String> okMap = reqParam;
			bs = acct_reservation_service.reservation_detail(cusidn);
			
			String isProd = SpringBeanFactory.getBean("isProd");
			bs.addData("isProd",isProd);
			//IDGATE身分
			String idgateUserFlag="N";		 
			Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
			try {		 
				if(IdgateData==null) {
					IdgateData = new HashMap<String, Object>();
				}
				BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
				if(tmp.getResult()) {		 
					idgateUserFlag="Y";                  		 
				}		 
				tmp.addData("idgateUserFlag",idgateUserFlag);		 
				IdgateData.putAll((Map<String, String>) tmp.getData());
				SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
			}catch(Exception e) {		 
				log.debug("idgateUserFlag error {}",e);		 
			}		 
			model.addAttribute("idgateUserFlag",idgateUserFlag);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("reservation_detail error >> {}",e);
		}finally {
			if(bs!=null && bs.getResult()) {
				target = "/acct/reservation_detail";
				
				Map<String,Object> dataMap = (Map)bs.getData();
				log.trace("dataMap={}",dataMap);
				List<Map<String,String>> rowListMap = (List<Map<String,String>>)dataMap.get("REC");
				log.trace("rowListMap={}",rowListMap);

				SessionUtil.addAttribute(model,SessionUtil.PRINT_DATALISTMAP_DATA,rowListMap);
				
				model.addAttribute("reservation_detail", bs);
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
				bs.setPrevious("/INDEX/index");
			}
		}
		return target;
	}
	
	@RequestMapping(value = "/reservation_confirm")
	public String reservation_confirm(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace(ESAPIUtil.vaildLog("reservation_confirm >> " + CodeUtil.toJson(reqParam)));
		try {
			bs = new BaseResult();
			// 解決Trust Boundary Violation
			Map<String, String> okMap = reqParam;
			// IKEY要使用的JSON:DC
			String jsondc = null;
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam.get("ROWDATA")),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("reservation_confirm.jsondc >> " + jsondc));
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				bs = acct_reservation_service.reservation_confirm(okMap);
				bs.addData("jsondc", jsondc);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				
				log.trace("IDGATE DATA >>"+CodeUtil.toJson(bs.getData()));
				
				Map<String, Object> bsData = (Map) bs.getData();
				Map<String, String> result = (Map)bsData.get("dataSet");
				// IDGATE transdata
				Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
				String adopid = "N283";
	    		String title = "您有一筆臺幣預約交易查詢取消待確認，金額" + result.get("DPTXAMTS");
	    		if(IdgateData != null) {
	    			model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
	    			if(IdgateData.get("idgateUserFlag").equals("Y")) {
	    				result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
	    				result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
	    				IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N283_IDGATE_DATA.class, N283_IDGATE_DATA_VIEW.class, result));
	    				SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);
	    			}
	    		}
	    		 model.addAttribute("idgateAdopid", adopid);
	             model.addAttribute("idgateTitle", title);
			}			
		} catch (Exception e) {			
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("reservation_confirm error >> {}",e);
		}finally {
			if(bs!=null && bs.getResult()) {
				target = "/acct/reservation_confirm";
				model.addAttribute("reservation_confirm", bs);
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
				bs.setPrevious("/NT/ACCT/RESERVATION/reservation_detail");
			}
		}		
		return target;
	}
	
	@ISTXNLOG(value="N283_1")
	@RequestMapping(value = "/reservation_result")
	public String reservation_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
//		BaseResult bsMsgCode = null;
		log.trace("reservation_result>>");
		try {
			bs = new BaseResult();
//			bsMsgCode = new BaseResult();
			
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			log.debug("reservation_result_cusidn >>{}", cusidn);
			//解決長度過長問題
			reqParam.remove("jsondc");
			reqParam.remove("pkcs7Sign");
// 			解決Trust Boundary Violation			
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			log.trace(ESAPIUtil.vaildLog("reservation_result_okMap >> " + CodeUtil.toJson(okMap)));


//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale) 
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				//判斷是SSL/晶片金融卡 or i-key
				String dptxcode = okMap.get("DPTXCODE");
				if("1".equals(dptxcode)) {
					bs = acct_reservation_service.reservation_result(cusidn,okMap);				
				}else if("2".equals(dptxcode)){
					bs = acct_reservation_service.reservation_result(cusidn,okMap);
				}else if("7".equals(dptxcode)){
					Map inMap = new HashMap();
					Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
    				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N283_IDGATE");
					okMap.put("sessionID", (String)IdgateData.get("sessionid"));
					okMap.put("txnID", (String)IdgateData.get("txnID"));
					okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));
					inMap.put("IN_DATAS", okMap);
					Boolean doIdgate = idgateservice.doIDGateCommand(inMap, "ms_tw");
					if(doIdgate) {
						bs = acct_reservation_service.reservation_result(cusidn,okMap);
					}
					else {
						log.info("doIdgate false");
					}
				}else{
					bs = acct_reservation_service.N951_REST(cusidn,okMap);										
					if(bs.getMsgCode().equals("0")) {
						bs.reset();
						bs = acct_reservation_service.reservation_result(cusidn,okMap);				
					}else {
						log.trace(bs.getMsgCode());
					}
				}
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {			
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("reservation_result error >> {}",e);
		}finally {
			if(bs!=null && bs.getResult()) {
				target = "/acct/reservation_result";				
				model.addAttribute("reservation_result", bs);
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
				bs.setPrevious("/NT/ACCT/RESERVATION/reservation_detail");
			}
		}			
		return target;
	}
	
	// A3000-轉出記錄查詢
	@RequestMapping(value = "/transfer_record_query")
	public String transfer_record_query(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("transfer_record_query start~~~");
		try {
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			log.debug("cusidn: {} ", cusidn);
			bs = acct_reservation_service.transfer_record_query(cusidn);
			bs.addData("TODAY", DateUtil.getCurentDateTime("yyyy/MM/dd"));
			String isProd = SpringBeanFactory.getBean("isProd");
			bs.addData("isProd",isProd);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("transfer_record_query error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/acct/transfer_record_query";
				log.debug("transfer_record_query bs getData {}",bs.getData());
				model.addAttribute("transfer_record_query", bs);
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	// 轉出記錄查詢結果頁
	@ISTXNLOG(value="A3000")
	@ACNVERIFY(acn="ACN")
	@RequestMapping(value = "/transfer_record_query_result")
	public String transfer_record_query_result(HttpServletRequest request, HttpServletResponse response,
		@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.debug("transfer_record_query_result start~~~{}");
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			log.trace("cusidn{}" , cusidn);
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			//判斷查詢區間是否在限定範圍
			boolean ckDate = DateUtil.checkDate(okMap.get("CMSDATE"), okMap.get("CMEDATE"), 6);
			if(ckDate) {
				if (hasLocale) {
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
					if (!bs.getResult()) {
						log.trace("bs.getResult() >>{}", bs.getResult());
						throw new Exception();
					}
				}
				if (!hasLocale) {
					bs.reset();
					bs = acct_reservation_service.transfer_record_query_result(cusidn, okMap);
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				}
			}
			else {
				bs.setResult(false);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("transfer_record_query_result error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/acct/transfer_record_query_result";
				log.debug("bs_get_data={}", bs.getData());
				Map<String,Object> dataMap = (Map)bs.getData();
				log.trace("dataMap={}",dataMap);
				List<Map<String,String>> rowListMap = (List<Map<String,String>>)dataMap.get("REC");
				log.trace("rowListMap={}",rowListMap);

				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, rowListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, rowListMap);
				
				model.addAttribute("transfer_record_query_result", bs);
			} 
			else {
				//新增回上一頁的路徑
				bs.setPrevious("/NT/ACCT/RESERVATION/transfer_record_query");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	// A3000-轉出記錄查詢-mail重送
		@RequestMapping(value = "/A3000_mail_aj")
		public @ResponseBody BaseResult transfer_record_query_mail(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
			log.trace(ESAPIUtil.vaildLog("transfer_record_query mail start >> " + CodeUtil.toJson(reqParam)));
			// 處理結果
			BaseResult bs = null;
			try {
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
				// 外幣預約轉帳查詢
				bs = acct_reservation_service.transfer_record_query_mail(cusidn,reqParam);
			} catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("transfer_record_query_mail error >> {}",e);
			}
			return bs;
		}
		
//		A3000-轉出記錄查詢直接下載
//		@RequestMapping(value = "/transfer_record_query_ajaxDirectDownload")
//		public String transfer_record_query_ajaxDirectDownload(HttpServletRequest request,HttpServletResponse response,@RequestBody(required=false) String serializeString, Model model){
//			String target = "/error";
//			BaseResult bs = null;
//			log.trace("transfer_record_query_ajaxDirectDownload");
//			log.trace("serializeString={}",serializeString);
//			//序列化傳入值
//			Map<String,String> formMap = DownloadUtil.serializeStringToMap(serializeString);
//			log.debug("formMap={}",  formMap);
//			try{
//				bs = new BaseResult();
//				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
//				log.trace("cusidn={}",cusidn);
//				for(Entry<String,String> entry : formMap.entrySet()){
//					formMap.put(entry.getKey(),URLDecoder.decode(entry.getValue(),"UTF-8"));
//				}
//				bs = acct_reservation_service.transfer_record_query_result(cusidn, formMap);
//			}
//			catch(Exception e){
//				e.printStackTrace();
//				log.error("",e);
//			}
//			finally{
//				if(bs != null && bs.getResult()){
//					target = "forward:/ajaxDirectDownload";
//					
//					Map<String,Object> dataMap = (Map)bs.getData();
//					log.trace("dataMap={}",dataMap);
//					List<Map<String,String>> rowListMap = (List<Map<String,String>>)dataMap.get("REC");
//					log.trace("rowListMap={}",rowListMap);
//					
//					SessionUtil.addAttribute(model,SessionUtil.DOWNLOAD_DATALISTMAP_DATA,rowListMap);
//					
//					Map<String,Object> parameterMap = new HashMap<String,Object>();
//					
//					parameterMap.put("CMQTIME", dataMap.get("CMQTIME"));
//					parameterMap.put("CMPERIOD", dataMap.get("cmedate"));
//					parameterMap.put("COUNT", dataMap.get("COUNT"));
//					
//					parameterMap.put("downloadFileName",formMap.get("downloadFileName"));
//					parameterMap.put("downloadType",formMap.get("downloadType"));
//					parameterMap.put("templatePath",formMap.get("templatePath"));
//					
//					String downloadType = formMap.get("downloadType");
//					log.trace("downloadType={}",downloadType);
//					
//					//EXCEL和OLDEXCEL下載
//					if("EXCEL".equals(downloadType) || "OLDEXCEL".equals(downloadType)){
//						parameterMap.put("headerRightEnd",formMap.get("headerRightEnd"));
//						parameterMap.put("headerBottomEnd",formMap.get("headerBottomEnd"));
//						parameterMap.put("rowStartIndex",formMap.get("rowStartIndex"));
//						parameterMap.put("rowRightEnd",formMap.get("rowRightEnd"));
//					}
//					//TXT下載
//					else{
//						parameterMap.put("txtHeaderBottomEnd",formMap.get("txtHeaderBottomEnd"));
//						parameterMap.put("txtHasRowData",formMap.get("txtHasRowData"));
//					}
//					request.setAttribute("parameterMap",parameterMap);
//				}
//				else{
//					String errorCode = "";
//					String errorMessage = "";
//					
//					try{
//						errorCode = URLEncoder.encode(bs.getMsgCode(),"UTF-8");
//						errorMessage = URLEncoder.encode(bs.getMessage(),"UTF-8");
//					}
//					catch(Exception e){
//						e.printStackTrace();
//						log.error("",e);
//					}
//					response.addHeader("errorCode",errorCode);
//					response.addHeader("errorMessage",errorMessage);
//				}
//			}
//			return target;
//		}
		
		//A3000-轉出記錄查詢直接下載
		@RequestMapping(value = "/transfer_record_query_directDownload")
		public String transfer_record_query_directDownload(HttpServletRequest request,HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model){
			String target = "/error";
			BaseResult bs = null;
			log.trace("transfer_record_query_directDownload");
			//序列化傳入值
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			log.trace(ESAPIUtil.vaildLog("okMap >> " + CodeUtil.toJson(okMap)));
			try{
				bs = new BaseResult();
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
				log.trace("cusidn={}",cusidn);
				for(Entry<String,String> entry : okMap.entrySet()){
					okMap.put(entry.getKey(),URLDecoder.decode(entry.getValue(),"UTF-8"));
				}
				bs = acct_reservation_service.transfer_record_query_result(cusidn, okMap);
			}
			catch(Exception e){
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("transfer_record_query_directDownload error >> {}",e);
			}
			finally{
				if(bs != null && bs.getResult()){
					target = "forward:/directDownload";
					
					Map<String,Object> dataMap = (Map)bs.getData();
					log.trace("dataMap={}",dataMap);
					List<Map<String,String>> rowListMap = (List<Map<String,String>>)dataMap.get("REC");
					log.trace("rowListMap={}",rowListMap);
					
					SessionUtil.addAttribute(model,SessionUtil.DOWNLOAD_DATALISTMAP_DATA,rowListMap);
					
					Map<String,Object> parameterMap = new HashMap<String,Object>();
					
					parameterMap.put("CMQTIME", dataMap.get("CMQTIME"));
					parameterMap.put("CMPERIOD", dataMap.get("cmedate"));
					parameterMap.put("COUNT", dataMap.get("COUNT"));
					
					parameterMap.put("downloadFileName",okMap.get("downloadFileName"));
					parameterMap.put("downloadType",okMap.get("downloadType"));
					parameterMap.put("templatePath",okMap.get("templatePath"));
					
					String downloadType = okMap.get("downloadType");
					log.trace(ESAPIUtil.vaildLog("downloadType >> " + downloadType));
					//EXCEL和OLDEXCEL下載
					if("EXCEL".equals(downloadType) || "OLDEXCEL".equals(downloadType)){
						parameterMap.put("headerRightEnd",okMap.get("headerRightEnd"));
						parameterMap.put("headerBottomEnd",okMap.get("headerBottomEnd"));
						parameterMap.put("rowStartIndex",okMap.get("rowStartIndex"));
						parameterMap.put("rowRightEnd",okMap.get("rowRightEnd"));
					}
					//TXT下載
					else{
						parameterMap.put("txtHeaderBottomEnd",okMap.get("txtHeaderBottomEnd"));
						parameterMap.put("txtHasRowData",okMap.get("txtHasRowData"));
					}
					request.setAttribute("parameterMap",parameterMap);
				}
				else{
					bs.setPrevious("/NT/ACCT/RESERVATION/transfer_record_query");
					model.addAttribute(BaseResult.ERROR, bs);
				}
			}
			return target;
		}
		
//	預約交易結果查詢輸入頁
	@RequestMapping(value = "/reservation_trans")
	public String reservation_trans(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("reservation_trans");
		String target = "/error";
		BaseResult bs = null;
		
//		清除切換語系時暫存的資料
		SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
		SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
		
		try {
			bs = new BaseResult();
			bs.addData("TODAY", DateUtil.getCurentDateTime("yyyy/MM/dd"));
			bs.setResult(Boolean.TRUE);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("reservation_trans error >> {}",e);	
		}finally {
			if(bs!=null && bs.getResult()) {
				target = "/acct/reservation_trans";
				model.addAttribute("reservation_trans", bs);
			}else {
				model.addAttribute(BaseResult.ERROR, bs);				
			}
		}
		return target;
	}
	
	// 預約交易結果查詢結果頁
	@ISTXNLOG(value="N790")
	@RequestMapping(value = "/reservation_trans_result")
	public String reservation_trans_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace(ESAPIUtil.vaildLog("reservation_trans_result >> " + CodeUtil.toJson(reqParam)));
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
// 			解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			//判斷查詢區間是否在限定範圍
			boolean ckDate = DateUtil.checkDate(okMap.get("CMSDATE"), okMap.get("CMEDATE"), 6);
			if(ckDate) {
				if(hasLocale){
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
					if( ! bs.getResult())
					{
						log.trace("bs.getResult() >>{}",bs.getResult());
						throw new Exception();
					}
				}
				if( ! hasLocale){
					bs.reset();			
					log.trace("cusidn>>{}", cusidn);
					bs = acct_reservation_service.reservation_trans_result(cusidn, okMap);
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
					log.trace("bs.getData()>>{}",bs.getData());
				}
			}
			else {
				bs.setResult(false);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("reservation_trans_result error >> {}",e);
		}finally {
			if(bs!=null && bs.getResult()) {
				target = "/acct/reservation_trans_result";
				
				Map<String,Object> dataMap = (Map)bs.getData();
				log.trace("dataMap={}",dataMap);
				List<Map<String,String>> rowListMap = (List<Map<String,String>>)dataMap.get("REC");
				log.trace("rowListMap={}",rowListMap);

				SessionUtil.addAttribute(model,SessionUtil.DOWNLOAD_DATALISTMAP_DATA,rowListMap);
				SessionUtil.addAttribute(model,SessionUtil.PRINT_DATALISTMAP_DATA,rowListMap);
				
				model.addAttribute("reservation_trans_result", bs);
			}else {
				bs.setPrevious("/NT/ACCT/RESERVATION/reservation_trans");
				model.addAttribute(BaseResult.ERROR, bs);				
			}
		}
		return target;
	}
	
	
//	預約交易結果直接下載
//	@RequestMapping(value = "/reservation_trans_ajaxDirectDownload")
//	public String reservation_trans_ajaxDirectDownload(HttpServletRequest request,HttpServletResponse response,@RequestBody(required=false) String serializeString, Model model){
//		String target = "/error";
//		BaseResult bs = null;
//		log.trace("reservation_trans_ajaxDirectDownload");
//		log.trace("serializeString={}",serializeString);
//		//序列化傳入值
//		Map<String,String> formMap = DownloadUtil.serializeStringToMap(serializeString);
//		log.debug("formMap={}",  formMap);
//		try{
//			bs = new BaseResult();
//			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
//			log.trace("cusidn={}",cusidn);
//			for(Entry<String,String> entry : formMap.entrySet()){
//				formMap.put(entry.getKey(),URLDecoder.decode(entry.getValue(),"UTF-8"));
//			}
//			bs = acct_reservation_service.reservation_trans_result(cusidn,formMap);
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			log.error("",e);
//		}
//		finally{
//			if(bs != null && bs.getResult()){
//				target = "forward:/ajaxDirectDownload";
//				
//				Map<String,Object> dataMap = (Map)bs.getData();
//				log.trace("dataMap={}",dataMap);
//				List<Map<String,String>> rowListMap = (List<Map<String,String>>)dataMap.get("REC");
//				log.trace("rowListMap={}",rowListMap);
//				
//				SessionUtil.addAttribute(model,SessionUtil.DOWNLOAD_DATALISTMAP_DATA,rowListMap);
//				
//				Map<String,Object> parameterMap = new HashMap<String,Object>();
//				
//				parameterMap.put("CMQTIME", dataMap.get("CMQTIME"));
//				parameterMap.put("COUNT", dataMap.get("COUNT"));
//				parameterMap.put("cmedate", dataMap.get("cmedate"));
//				parameterMap.put("FGTXSTATUS", dataMap.get("FGTXSTATUS"));
//				
//				parameterMap.put("downloadFileName",formMap.get("downloadFileName"));
//				parameterMap.put("downloadType",formMap.get("downloadType"));
//				parameterMap.put("templatePath",formMap.get("templatePath"));
//				
//				String downloadType = formMap.get("downloadType");
//				log.trace("downloadType={}",downloadType);
//				
//				//EXCEL和OLDEXCEL下載
//				if("EXCEL".equals(downloadType) || "OLDEXCEL".equals(downloadType)){
//					parameterMap.put("headerRightEnd",formMap.get("headerRightEnd"));
//					parameterMap.put("headerBottomEnd",formMap.get("headerBottomEnd"));
//					parameterMap.put("rowStartIndex",formMap.get("rowStartIndex"));
//					parameterMap.put("rowRightEnd",formMap.get("rowRightEnd"));
//				}
//				//TXT下載
//				else{
//					parameterMap.put("txtHeaderBottomEnd",formMap.get("txtHeaderBottomEnd"));
//					parameterMap.put("txtHasRowData",formMap.get("txtHasRowData"));
//				}
//				request.setAttribute("parameterMap",parameterMap);
//			}
//			else{
//				String errorCode = "";
//				String errorMessage = "";
//				
//				try{
//					errorCode = URLEncoder.encode(bs.getMsgCode(),"UTF-8");
//					errorMessage = URLEncoder.encode(bs.getMessage(),"UTF-8");
//				}
//				catch(Exception e){
//					e.printStackTrace();
//					log.error("",e);
//				}
//				response.addHeader("errorCode",errorCode);
//				response.addHeader("errorMessage",errorMessage);
//			}
//		}
//		return target;
//	}
	
	//預約交易結果直接下載
	@RequestMapping(value = "/reservation_trans_directDownload")
	public String reservation_trans_directDownload(HttpServletRequest request,HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model){
		String target = "/error";
		BaseResult bs = null;
		log.trace("reservation_trans_directDownload");
		//序列化傳入值
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		log.debug(ESAPIUtil.vaildLog("okMap >> " + CodeUtil.toJson(okMap)));
		try{
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			log.trace("cusidn={}",cusidn);
			for(Entry<String,String> entry : okMap.entrySet()){
				okMap.put(entry.getKey(),URLDecoder.decode(entry.getValue(),"UTF-8"));
			}
			bs = acct_reservation_service.reservation_trans_result(cusidn,okMap);
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("reservation_trans_directDownload error >> {}",e);
		}
		finally{
			if(bs != null && bs.getResult()){
				target = "forward:/directDownload";
				
				Map<String,Object> dataMap = (Map)bs.getData();
				log.trace("dataMap={}",dataMap);
				List<Map<String,String>> rowListMap = (List<Map<String,String>>)dataMap.get("REC");
				log.trace("rowListMap={}",rowListMap);
				
				SessionUtil.addAttribute(model,SessionUtil.DOWNLOAD_DATALISTMAP_DATA,rowListMap);
				
				Map<String,Object> parameterMap = new HashMap<String,Object>();
				
				parameterMap.put("CMQTIME", dataMap.get("CMQTIME"));
				parameterMap.put("COUNT", dataMap.get("COUNT"));
				parameterMap.put("cmedate", dataMap.get("cmedate"));
				parameterMap.put("FGTXSTATUS", dataMap.get("FGTXSTATUS"));
				
				parameterMap.put("downloadFileName",okMap.get("downloadFileName"));
				parameterMap.put("downloadType",okMap.get("downloadType"));
				parameterMap.put("templatePath",okMap.get("templatePath"));
				
				String downloadType = okMap.get("downloadType");
				log.trace(ESAPIUtil.vaildLog("downloadType >> " + downloadType));
				//EXCEL和OLDEXCEL下載
				if("EXCEL".equals(downloadType) || "OLDEXCEL".equals(downloadType)){
					parameterMap.put("headerRightEnd",okMap.get("headerRightEnd"));
					parameterMap.put("headerBottomEnd",okMap.get("headerBottomEnd"));
					parameterMap.put("rowStartIndex",okMap.get("rowStartIndex"));
					parameterMap.put("rowRightEnd",okMap.get("rowRightEnd"));
				}
				//TXT下載
				else{
					parameterMap.put("txtHeaderBottomEnd",okMap.get("txtHeaderBottomEnd"));
					parameterMap.put("txtHasRowData",okMap.get("txtHasRowData"));
				}
				request.setAttribute("parameterMap",parameterMap);
			}
			else{
				bs.setPrevious("/NT/ACCT/RESERVATION/reservation_trans");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
}
