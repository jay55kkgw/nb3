package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class P018_REST_RSDATA extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8818382801665658285L;
	private String ZoneCode;
	private String ZoneName;
	private String Enabled;
	private String FILLER;
	
	public String getZoneCode() {
		return ZoneCode;
	}
	public void setZoneCode(String zoneCode) {
		ZoneCode = zoneCode;
	}
	public String getZoneName() {
		return ZoneName;
	}
	public void setZoneName(String zoneName) {
		ZoneName = zoneName;
	}
	public String getEnabled() {
		return Enabled;
	}
	public void setEnabled(String enabled) {
		Enabled = enabled;
	}
	public String getFILLER() {
		return FILLER;
	}
	public void setFILLER(String fILLER) {
		FILLER = fILLER;
	}
}
