package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N2041_REST_RQ extends BaseRestBean_OLA implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6684414607022255457L;
	private String CUSIDN;
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
}
