package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class TD01_REST_RSDATA2 extends BaseRestBean implements Serializable {
	
	private static final long serialVersionUID = 5150640688730965034L;
	/**
	 * 
	 */
	 private String CURRENCY;
	 private String CRDNAME;
	 private String POSTDATE;
	 private String SRCAMNT;
	 private String DESCTXT;
	 private String PURDATE;
	 private String CARDTYPE;
	 private String CURAMNT;
	 
	 
	public String getCURRENCY() {
		return CURRENCY;
	}
	public String getCRDNAME() {
		return CRDNAME;
	}
	public String getPOSTDATE() {
		return POSTDATE;
	}
	public String getSRCAMNT() {
		return SRCAMNT;
	}
	public String getDESCTXT() {
		return DESCTXT;
	}
	public String getPURDATE() {
		return PURDATE;
	}
	public String getCARDTYPE() {
		return CARDTYPE;
	}
	public String getCURAMNT() {
		return CURAMNT;
	}
	public void setCURRENCY(String cURRENCY) {
		CURRENCY = cURRENCY;
	}
	public void setCRDNAME(String cRDNAME) {
		CRDNAME = cRDNAME;
	}
	public void setPOSTDATE(String pOSTDATE) {
		POSTDATE = pOSTDATE;
	}
	public void setSRCAMNT(String sRCAMNT) {
		SRCAMNT = sRCAMNT;
	}
	public void setDESCTXT(String dESCTXT) {
		DESCTXT = dESCTXT;
	}
	public void setPURDATE(String pURDATE) {
		PURDATE = pURDATE;
	}
	public void setCARDTYPE(String cARDTYPE) {
		CARDTYPE = cARDTYPE;
	}
	public void setCURAMNT(String cURAMNT) {
		CURAMNT = cURAMNT;
	}
	 
}
