package tw.com.fstop.nnb.spring.controller;

import java.net.URLEncoder;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.idgate.bean.F003_IDGATE_DATA;
import tw.com.fstop.idgate.bean.F003_IDGATE_DATA_VIEW;import tw.com.fstop.idgate.bean.N830_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N830_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N831_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N831_IDGATE_DATA_VIEW;import tw.com.fstop.idgate.bean.F004_IDGATE_DATA;
import tw.com.fstop.idgate.bean.F004_IDGATE_DATA_VIEW;import tw.com.fstop.nnb.service.Fcy_Online_Service;
import tw.com.fstop.nnb.service.IdGateService;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.WebUtil;

/**
 * 
 * 功能說明 :外匯匯入匯款線上解款
 * 
 */
@SessionAttributes({ SessionUtil.CUSIDN, SessionUtil.DPMYEMAIL, SessionUtil.STEP1_LOCALE_DATA,SessionUtil.STEP2_LOCALE_DATA,SessionUtil.STEP3_LOCALE_DATA,
		
		SessionUtil.CONFIRM_LOCALE_DATA, SessionUtil.RESULT_LOCALE_DATA, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, SessionUtil.IDGATE_TRANSDATA,
		SessionUtil.TRANSFER_DATA, SessionUtil.FXAGREEFLAG, SessionUtil.PRINT_DATALISTMAP_DATA })
@Controller
@RequestMapping(value = "/FCY/ACCT/ONLINE")
public class Fcy_Online_Controller
{
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private Fcy_Online_Service fcy_Online_Service;

	@Autowired
	private I18n i18n;
	// 錯誤頁路徑
	private final String ERRORJSP = "/error";

	@Autowired		 
	IdGateService idgateservice;
	
	/**
	 * F004外匯匯入匯款線上解款申請/註銷進入頁!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/f_remittances_apply")
	public String f_remittances_apply(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("f_remittances_apply Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		try
		{
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			bs = fcy_Online_Service.fcy_Online_Service(cusidn);
			

           //IDGATE身分
           String idgateUserFlag="N";		 
           Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
           try {		 
        	   if(IdgateData==null) {
        		   IdgateData = new HashMap<String, Object>();
        	   }
               BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
               if(tmp.getResult()) {		 
                   idgateUserFlag="Y";                  		 
               }		 
               tmp.addData("idgateUserFlag",idgateUserFlag);		 
               IdgateData.putAll((Map<String, String>) tmp.getData());
               SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
           }catch(Exception e) {		 
               log.debug("idgateUserFlag error {}",e);		 
           }		 
           model.addAttribute("idgateUserFlag",idgateUserFlag);	           
		}
		catch (Exception e)
		{
			log.error("f_remittances_apply Error ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = "acct_fcy/f_remittances_apply";
				model.addAttribute("f_remittances_apply", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * F004外匯匯入匯款線上解款申請/註銷確認頁!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/f_remittances_apply_confirm")
	public String f_remittances_apply_confirm(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("f_remittances_apply_confirm Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		String jsondc = "";
		try{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam), "UTF-8");
			log.trace(ESAPIUtil.vaildLog("renewal_apply_confirm.jsondc: {}"+jsondc));
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
			}
			else
			{
				bs = fcy_Online_Service.f_remittances_apply_confirm(reqParam);
				// 防止重送代碼TxToken
				bs.addData("TXTOKEN", ((Map<String, String>) fcy_Online_Service.getTxToken().getData()).get("TXTOKEN"));
				// IKEY要使用的JSON:DC
				bs.addData("jsondc", jsondc);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
				
				// IDGATE transdata 
				Map<String,String> result = new HashMap();
				result.putAll((Map< String,String>) bs.getData());
	            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
	            String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
	    		String adopid = "F004";
	    		String TXTYPE = "(查無類別)";
	    		if("A".equals(result.get("TXTYPE")))
	    			TXTYPE = "申請匯入匯款線上解款";
	    		else
	    			TXTYPE = "取消匯入匯款線上解款";
	    		String title = "您有一筆 "+ TXTYPE + "待確認";
	        	if(IdgateData != null) {
		            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
		            if(IdgateData.get("idgateUserFlag").equals("Y")) {
		            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
		            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
		            	result.put("CUSIDN", cusidn);
						IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(F004_IDGATE_DATA.class, F004_IDGATE_DATA_VIEW.class, result));
		                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
		            }
	        	}
	            model.addAttribute("idgateAdopid", adopid);
	            model.addAttribute("idgateTitle", title);	            
			}
		}
		catch (Exception e)
		{
			log.error("f_remittances_apply_confirm Error ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = "acct_fcy/f_remittances_apply_confirm";
				model.addAttribute("f_remittances_apply_confirm", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * F004外匯匯入匯款線上解款申請/註銷結果頁!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/f_remittances_apply_result")
	public String f_remittances_apply_result(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("f_remittances_apply__result Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		String pageToken = "";
		try
		{

			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			//Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI

			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
			}
			else
			{
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
				// Check Session Token
				String SessionFinshToken = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null);
				pageToken = reqParam.get("TXTOKEN");
				BaseResult checkToken = fcy_Online_Service.validateToken(pageToken, SessionFinshToken);
				if (!checkToken.getResult())
				{
					bs = checkToken;
				}
				else
				{
					//IDgate驗證		 
					try {		 
						if(okMap.get("FGTXWAY").equals("7")) {		 
							Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
							Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("F004_IDGATE");
							okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
							okMap.put("txnID", (String)IdgateData.get("txnID"));		 
							okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
						}		 
					}catch(Exception e) {		 
						log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
					}  
					
					Map<String,Object>IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
					model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
					
					bs = fcy_Online_Service.f_remittances_apply_result(cusidn, okMap);
					// 將查過的資料放入session，待切換 locale 時讀取
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
					
				}
			}

		}
		catch (Exception e)
		{
			log.error("f_remittances_apply__result Error ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				// session add TxToken
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, pageToken);
				target = "acct_fcy/f_remittances_apply_result";
				model.addAttribute("f_remittances_apply_result", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * F003外匯匯入匯款線上解款說明通知頁!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/f_remittances_payments")
	public String f_remittances(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("f_remittances_payments Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		try
		{
			// get session value
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			String fxagreeflag = (String) SessionUtil.getAttribute(model, SessionUtil.FXAGREEFLAG, null);
			log.debug("fxagreeflag >>>>>{}", fxagreeflag);
			if (StrUtil.isEmpty(fxagreeflag))
			{
				target = "acct_fcy/f_remittances_payments";
			}
			else
			{
				target = "forward:/FCY/ACCT/ONLINE/f_remittances_payments_step1";
			}
		}
		catch (Exception e)
		{
			log.error("f_remittances_payments Error >>>> {} ", e);
		}
		return target;
	}

	/**
	 * F003外匯匯入匯款線上解款進入頁!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/f_remittances_payments_step1")
	public String f_remittances_step1(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("f_remittances_payments_step1 Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		Boolean falgFXREMITFLAG = Boolean.FALSE;
		try
		{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			// 將點選確認的值放入session
			String fxagreeflag = okMap.get("FXAGREEFLAG");
			SessionUtil.addAttribute(model, SessionUtil.FXAGREEFLAG, fxagreeflag);
			bs = fcy_Online_Service.f_remittances_payments_step1(cusidn);
			List<Map<String, Object>> dataListMap = ((List<Map<String, Object>>) ((Map<String, Object>) bs.getData()).get("REC"));
			if (!((Map<String, Object>) bs.getData()).get("FXREMITFLAG").equals("Y"))
			{
				falgFXREMITFLAG = Boolean.TRUE;
			}
			else if (dataListMap.size() <= 0)
			{
				bs.setResult(Boolean.FALSE);
				bs.setErrorMessage("ENRD", i18n.getMsg("LB.Check_no_data"));
			}

			log.debug("dataListMap={}", dataListMap);
			SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);

            //IDGATE身分
            String idgateUserFlag="N";		 
            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
            try {		 
        	    if(IdgateData==null) {
        		    IdgateData = new HashMap<String, Object>();
        	    }
                BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
                if(tmp.getResult()) {		 
                    idgateUserFlag="Y";                  		 
                }		 
                tmp.addData("idgateUserFlag",idgateUserFlag);		 
                IdgateData.putAll((Map<String, String>) tmp.getData());
                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
            }catch(Exception e) {		 
                log.debug("idgateUserFlag error {}",e);		 
            }		 
            model.addAttribute("idgateUserFlag",idgateUserFlag);
            
		}
		catch (Exception e)
		{
			log.error("f_remittances_payments_step1 Error >>>> {} ", e);
		}
		finally
		{
			if (falgFXREMITFLAG)
			{
				target = "acct_fcy/f_remittances_payments_error";
			}
			else if (bs != null && bs.getResult())
			{
				target = "acct_fcy/f_remittances_payments_step1";
				model.addAttribute("f_remittances_payments_step1", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}

		return target;
	}

	/**
	 * F003外匯匯入匯款線上解款第2頁!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/f_remittances_payments_step2")
	public String f_remittances_payments_step2(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("f_remittances_payments_step2 Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		try
		{
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP2_LOCALE_DATA, null));
			}
			else
			{
				bs = fcy_Online_Service.f_remittances_payments_step2(cusidn, reqParam);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.STEP2_LOCALE_DATA, bs);
			}
		}
		catch (Exception e)
		{
			log.error("f_remittances_payments_step2 Error >>>> {} ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = "acct_fcy/f_remittances_payments_step2";
				model.addAttribute("f_remittances_payments_step2", bs);
				// TODO 這邊還要調整 不能存物件，要存成JSNO字串
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * F003外匯匯入匯款線上解款第3頁!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/f_remittances_payments_step3")
	public String f_remittances_step3(@RequestParam Map<String, String> reqParam, Model model)
	{

		log.trace("f_remittances_payments_step2 Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		try
		{
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			BaseResult bsdata = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
			Map<String, String> transfer_data = (Map<String, String>) bsdata.getData();
			log.debug("f_remittances_payments_step3 transfer_data >>> {}", transfer_data);
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP3_LOCALE_DATA, null));
			}
			else
			{
				bs = fcy_Online_Service.f_remittances_payments_step3(cusidn, transfer_data, reqParam);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.STEP3_LOCALE_DATA, bs);
			}

		}
		catch (Exception e)
		{
			log.error("f_remittances_payments_step3 Error >>>> {} ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				// TODO 這邊還要調整 不能存物件，要存成JSNO字串
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
				target = "acct_fcy/f_remittances_payments_step3";
				model.addAttribute("f_remittances_payments_step3", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * F003外匯匯入匯款線上解款確認頁!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/f_remittances_payments_confirm")
	public String f_remittances_payments_confirm(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("f_remittances_payments_confirm Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		String jsondc = "";
		try
		{

			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam), "UTF-8");
			log.trace(ESAPIUtil.vaildLog("renewal_apply_confirm.jsondc: {}"+ jsondc));
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
			}
			else
			{
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
				BaseResult bsdata = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
				Map<String, String> transfer_data = (Map<String, String>) bsdata.getData();
				log.debug("f_remittances_payments_confirm transfer_data >>> {}", transfer_data);
				bs = fcy_Online_Service.f_remittances_payments_confirm(cusidn, transfer_data, reqParam);
				// 防止重送代碼TxToken
				bs.addData("TXTOKEN", ((Map<String, String>) fcy_Online_Service.getTxToken().getData()).get("TXTOKEN"));
				// IKEY要使用的JSON:DC
				bs.addData("jsondc", jsondc);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);

			}

			// IDGATE transdata            		 
            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
    		String adopid = "F003";
    		Map<String, String> result = new HashMap<String, String>();
    		Map<String, Object> bsData = (Map<String, Object>)bs.getData();
    		for(String key : bsData.keySet()) {
    			try {
    				result.put(key, (String)bsData.get(key));
    			}catch(Exception e){
    			}
    		}
    		String title = "您有一筆外匯匯入匯款線上解款待確認，金額 " + result.get("RETCCY") + " " + result.get("showTXAMT") + "元";
        	if(IdgateData != null) {
	            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
	            if(IdgateData.get("idgateUserFlag").equals("Y")) {
	            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
	            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
	            	result.put("TRANTITLE", "外匯匯入匯款線上解款");
	            	result.put("TRANNAME", "即時");
	            	result.put("BGROENO", (result.get("BGROENO") == null) ? "" : result.get("BGROENO"));
					IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(F003_IDGATE_DATA.class, F003_IDGATE_DATA_VIEW.class, result));
	                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	            }
        	}
            model.addAttribute("idgateAdopid", adopid);
            model.addAttribute("idgateTitle", title);
		}
		catch (Exception e)
		{
			log.error("f_remittances_payments_confirm Error >>>> {} ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				// TODO 這邊還要調整 不能存物件，要存成JSNO字串
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
				target = "acct_fcy/f_remittances_payments_confirm";
				model.addAttribute("f_remittances_payments_confirm", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}

		return target;
	}

	/**
	 * F003外匯匯入匯款線上解款結果頁!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/f_remittances_payments_result")
	public String f_remittances_payments_result(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("f_remittances_payments_result Start ~~~~~~~~~~~~~~~~");

		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		String pageToken = "";
		try
		{

			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			//Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
			}
			else
			{

				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
				// Check Session Token
				String SessionFinshToken = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null);
				pageToken = reqParam.get("TXTOKEN");
				BaseResult checkToken = fcy_Online_Service.validateToken(pageToken, SessionFinshToken);
				if (!checkToken.getResult())
				{
					bs = checkToken;
				}
				else
				{
					BaseResult bsdata = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
					Map<String, String> transfer_data = (Map<String, String>) bsdata.getData();
					log.debug("f_remittances_payments_result transfer_data >>> {}", transfer_data);

	                //IDgate驗證		 
	                try {		 
	                    if(reqParam.get("FGTXWAY").equals("7")) {		 

	        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
	        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("F003_IDGATE");
	        				reqParam.put("sessionID", (String)IdgateData.get("sessionid"));		 
	        				reqParam.put("txnID", (String)IdgateData.get("txnID"));		 
	        				reqParam.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
	                    }
	                }catch(Exception e) {		 
	                    log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
	                }  
					
					Map<String,Object>IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
					model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
	                
					bs = fcy_Online_Service.f_remittances_payments_result(cusidn, transfer_data, reqParam);
					// 將查過的資料放入session，待切換 locale 時讀取
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				}
			}
		}
		catch (Exception e)
		{
			log.error("f_remittances_payments_result Error >>>> {} ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				// session add TxToken
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, pageToken);
				target = "acct_fcy/f_remittances_payments_result";
				model.addAttribute("f_remittances_payments_result", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

}
