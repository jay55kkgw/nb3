package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;


public class N750A_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8806533305439577457L;

	//交易類別  1即時繳費/2預約交易
	@SerializedName(value = "FGTXDATE")
	private String FGTXDATE;
	
	//轉帳日期 yyyyMMdd
	@SerializedName(value = "transfer_date")
	private String transfer_date;
	
	//轉出帳號
	@SerializedName(value = "OUTACN")
	private String OUTACN;
	
	//代收截止日
	@SerializedName(value = "IDGATE_PAYDUE")
	private String IDGATE_PAYDUE;
	
	//銷帳編號
	@SerializedName(value = "WAT_NO")
	private String WAT_NO;
	
	//查核碼
	@SerializedName(value = "CHKCOD")
	private String CHKCOD;
	
	//繳款金額 
	@SerializedName(value = "IDGATE_AMOUNT")
	private String IDGATE_AMOUNT;
	
	@Override
	public Map<String, String> coverMap(){
		Map<String, String> result = new LinkedHashMap<String, String>();
		String fgtxdate = " ";
		if("1".equals(this.FGTXDATE))
			fgtxdate = "即時繳費";
		else if("2".equals(this.FGTXDATE))
			fgtxdate = "預約繳費";
		result.put("交易名稱", "繳費-臺灣自來水公司");
		result.put("交易類別", fgtxdate);
		result.put("轉帳日期", this.transfer_date);
		result.put("轉出帳號", this.OUTACN);
		result.put("代收截止日", this.IDGATE_PAYDUE);
		result.put("銷帳編號", this.WAT_NO);
		result.put("查核碼", this.CHKCOD);
		result.put("繳款金額 ", "新台幣"+this.IDGATE_AMOUNT+"元");
		return result;
	}
	
	public String getFGTXDATE() {
		return FGTXDATE;
	}
	public void setFGTXDATE(String fGTXDATE) {
		FGTXDATE = fGTXDATE;
	}
	public String getTransfer_date() {
		return transfer_date;
	}
	public void setTransfer_date(String transfer_date) {
		this.transfer_date = transfer_date;
	}
	public String getOUTACN() {
		return OUTACN;
	}
	public void setOUTACN(String oUTACN) {
		OUTACN = oUTACN;
	}
	
	public String getIDGATE_PAYDUE() {
		return IDGATE_PAYDUE;
	}

	public void setIDGATE_PAYDUE(String iDGATE_PAYDUE) {
		IDGATE_PAYDUE = iDGATE_PAYDUE;
	}

	public String getWAT_NO() {
		return WAT_NO;
	}
	public void setWAT_NO(String wAT_NO) {
		WAT_NO = wAT_NO;
	}
	public String getCHKCOD() {
		return CHKCOD;
	}
	public void setCHKCOD(String cHKCOD) {
		CHKCOD = cHKCOD;
	}

	public String getIDGATE_AMOUNT() {
		return IDGATE_AMOUNT;
	}

	public void setIDGATE_AMOUNT(String iDGATE_AMOUNT) {
		IDGATE_AMOUNT = iDGATE_AMOUNT;
	}

}
