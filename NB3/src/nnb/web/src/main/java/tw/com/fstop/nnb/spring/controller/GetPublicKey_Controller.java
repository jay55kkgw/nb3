package tw.com.fstop.nnb.spring.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.service.GetPublicKey_Service;
import tw.com.fstop.util.CodeUtil;

@RestController
@RequestMapping(value = "/MB/GETPK")
public class GetPublicKey_Controller {

	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	GetPublicKey_Service service;

	@RequestMapping(value = "/getPublicKey", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody BaseResult query(HttpServletRequest request, HttpServletResponse response,
			@RequestBody Map<String, String> reqParam, Model model) {
		log.info("getPublicKey");
		BaseResult bs = new BaseResult();
		try {
//			String publickey = "-----BEGIN RSA PUBLIC KEY-----MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqAIML0q1FfkkV6BUegBYljX8Afolt00/7bMB5pLulXzv/jz2ecfbwvxUGIQptp6y2Zavj7p26J1SYzSh65zcdORokkeuTw/EQxXSctJ+kMHbgW+bDyeuQ3Ild/MF6V77DuSy/kxO54vVu5aO/lhe091gtszuzTiRXkklcCb8DwQMC+uHw9aafO4L6Lot6n+AJPsw5ffuUHqnQb3nN50r/5S7sMi3k9x9e+419gG0SEM0m8gxTXmQjt4v5iNwkCl+VMXZEW7rp8Dn6EFgc73d+eA5OCQmu2RKQaijgQlWb7pX4RuZFRu45S4eEFBppxbhd7/oget6XskHeUHv0KZo6QIDAQAB-----END RSA PUBLIC KEY-----";
			
			bs.reset();
			bs = service.getPublickey_REST();
			if(bs.getResult()) {
				bs.setMessage("ok");
			}
			log.info("bs>>{}", CodeUtil.toJson(bs) );
			// CodeUtil.convert2BaseResult(bs, obj);
		} catch (Exception e) {
			log.error(e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", "getPublicKey fail");
		}
		return bs;

	}
	
	@RequestMapping(value = "/updatePublicKey", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody BaseResult updatePublicKey(HttpServletRequest request, HttpServletResponse response,
			@RequestBody Map<String, String> reqParam, Model model) {
		log.info("updatePublicKey");
		BaseResult bs = new BaseResult();
		try {
			
			bs.reset();
			bs = service.updatePublicKey();
			if(bs.getResult()) {
				bs.setMessage("ok");
			}
			log.info("bs>>{}", CodeUtil.toJson(bs) );
			// CodeUtil.convert2BaseResult(bs, obj);
		} catch (Exception e) {
			log.error(e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", "updatePublicKey fail");
		}
		return bs;
		
	}
	
 
}
