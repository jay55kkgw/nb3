package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N207_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4943161159214672801L;
	private String FATCA;

	public String getFATCA() {
		return FATCA;
	}

	public void setFATCA(String fATCA) {
		FATCA = fATCA;
	}
}
