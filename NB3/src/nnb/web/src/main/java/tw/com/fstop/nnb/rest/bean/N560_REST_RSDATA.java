package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N560_REST_RSDATA extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2496154579991130006L;

	private String RBILLAMT; //到單金額
	private String RBILLTYP; //匯票期間
	private String RREFNO; //託收編號
	private String RCMDATE1; //承兌日
	private String RCOUNTRY; //國家別
	private String RMARK; //備註
	private String RUPSTAT; //拒付狀態
	private String RLCNO; //信用狀號碼
	private String RNREMDAT; //託收日期
	private String RBILLCCY; //幣別
	private String RVALDATE; //結案日期
	
	public String getRBILLAMT() {
		return RBILLAMT;
	}
	public void setRBILLAMT(String rBILLAMT) {
		RBILLAMT = rBILLAMT;
	}
	public String getRBILLTYP() {
		return RBILLTYP;
	}
	public void setRBILLTYP(String rBILLTYP) {
		RBILLTYP = rBILLTYP;
	}
	public String getRREFNO() {
		return RREFNO;
	}
	public void setRREFNO(String rREFNO) {
		RREFNO = rREFNO;
	}
	public String getRCMDATE1() {
		return RCMDATE1;
	}
	public void setRCMDATE1(String rCMDATE1) {
		RCMDATE1 = rCMDATE1;
	}
	public String getRCOUNTRY() {
		return RCOUNTRY;
	}
	public void setRCOUNTRY(String rCOUNTRY) {
		RCOUNTRY = rCOUNTRY;
	}
	public String getRMARK() {
		return RMARK;
	}
	public void setRMARK(String rMARK) {
		RMARK = rMARK;
	}
	public String getRUPSTAT() {
		return RUPSTAT;
	}
	public void setRUPSTAT(String rUPSTAT) {
		RUPSTAT = rUPSTAT;
	}
	public String getRLCNO() {
		return RLCNO;
	}
	public void setRLCNO(String rLCNO) {
		RLCNO = rLCNO;
	}
	public String getRNREMDAT() {
		return RNREMDAT;
	}
	public void setRNREMDAT(String rNREMDAT) {
		RNREMDAT = rNREMDAT;
	}
	public String getRBILLCCY() {
		return RBILLCCY;
	}
	public void setRBILLCCY(String rBILLCCY) {
		RBILLCCY = rBILLCCY;
	}
	public String getRVALDATE() {
		return RVALDATE;
	}
	public void setRVALDATE(String rVALDATE) {
		RVALDATE = rVALDATE;
	}
	
}
