
/*
 * Copyright (c) 2017, FSTOP, Inc. All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tw.com.fstop.web.util;

import java.io.BufferedReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.IntrusionException;
import org.owasp.esapi.errors.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.AStar.TBConvert.WStrURLConvert;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

//import fstop.model.MVH;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.tbb.nnb.util.StrUtils;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SpringBeanFactory;



/**
 * Web utility functions for web application.
 * 
 *
 * @since 1.0.1
 */
public class WebUtil
{
	private static Logger log = LoggerFactory.getLogger(WebUtil.class);
	private static ObjectMapper jsonMapper;
	private static final String WORD_URL = SpringBeanFactory.getBean("WORD_URL");
	
	static
	{
		jsonMapper = new ObjectMapper();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        jsonMapper.setDateFormat(df);		
	}
	
	
    /**
     * Get client ip address.
     * 
     * @param request HttpServletRequest
     * @return client ip address
     */
    public static String getIpAddr(HttpServletRequest request) 
    {
        if (request == null)
        {
            return "unknown";
        }
        // getValidHeader解決Trust Boundary Violation
        
        String ip = getValidHeader(request,"x-forwarded-for");
        
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))
        {
        	ip = request.getRemoteAddr();
        }
        //String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))
        {
        	//ip = request.getHeader("Proxy-Client-IP");
        	ip = getValidHeader(request,"Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))
        {
        	//ip = request.getHeader("WL-Proxy-Client-IP");
        	ip = getValidHeader(request,"WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))
        {
        	ip = request.getRemoteAddr();
        }
        
//        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))
//        {
//        	//ip = request.getHeader("X-Forwarded-For");
//        	ip = getValidHeader(request,"X-Forwarded-For");
//        }
//        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))
//        {
//            //ip = request.getHeader("X-Real-IP");
//        	ip = getValidHeader(request,"X-Real-IP");
//        }
//        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))
//        {
//            //ip = request.getHeader("HTTP_CLIENT_IP");
//        	ip = getValidHeader(request,"HTTP_CLIENT_IP");
//        }
        
        // LOCAL測試用
//        ip = LocalUtil.checkIp(ip);
        
        // 20210604-by ss: 取得客戶端IP方式改為抓request header x-forwarded-for欄位最後一個ip。
//        ip = trimIp(ip);
        ip = remoteLastIp(ip);
        
        return ip;
    }
    
	
    /**
     * Get remote IP trim
     */
	public static String trimIp(String ip) {
		String result = "";
		if ( ip != null && ip.contains(",")) {
			ip = ip.replaceAll("\\s+","");
			result = ip.substring(0, ip.indexOf(","));
		} else {
			result = ip.replaceAll("\\s+","");
		}
		return result;
	}
	
    /**
     * Get remote IP Last
     */
	public static String remoteLastIp(String ip) {
		String result = "";
		if ( ip != null && ip.contains(",")) {
			ip = ip.replaceAll("\\s+","");
			String[] ipArray = ip.split(",");
			if (ipArray != null && ipArray.length!=0) {
				result = ipArray[ipArray.length-1]; 
			}
			
		} else {
			result = ip.replaceAll("\\s+",""); // removes all whitespaces and non-visible characters (e.g., tab, \n).
		}
		return result;
	}
	
    /**
     * Get user agent information.
     * 
     * @param request HttpServletRequest
     * @return user agent information
     */
    public static String getUserAgent(HttpServletRequest request) 
    {
        String ret = request.getHeader("User-Agent");        
        return ret;
    }
    
    /**
     * Get http client accept language.
     * zh-TW,zh;q=0.8,en-US;q=0.6,en;q=0.4
     * 
     * @param request HttpServletRequest
     * @return accept language
     */
    public static String getAcceptLanguage(HttpServletRequest request)
    {        
        String ret = request.getHeader("Accept-Language");       
        return ret;
    }
    
    /**
     * Get http client preferred locale.
     * 
     * <pre>
     * Convert 
     *   language + "_" + country + "_" + (variant + "_#" | "#") + script + "-" + extensions
     * To
     *   language + "_" + country
     * 
     * Examples:  
     * en -&gt; en
     * de_DE -&gt; de_DE
     * _GB -&gt; GB
     * en_US_WIN -&gt; en_US
     * de__POSIX -&gt; de
     * zh_CN_#Hans -&gt; zh_CN
     * zh_TW_#Hant-x-java -&gt; zh_TW
     * th_TH_TH_#u-nu-thai -&gt; th_TH
     *  
     * </pre>
     * @param request HttpServletRequest
     * @return client preferred locale
     */
    public static String getPreferredLocale(HttpServletRequest request)
    {
        ///Enumeration<Locale> locales = request.getLocales();
        //while (locales.hasMoreElements()) 
        //{
        //    Locale locale = (Locale) locales.nextElement();
        //    return locale.getDisplayLanguage();
        //}
        String ret = null;
        String lang = request.getLocale().toString();
        int idx = lang.indexOf("_", 3);
        
        if (idx > 0)
        {
            lang = lang.substring(0, idx);
        }

        //remove leading and ending "_"        
        if (lang.startsWith("_"))
        {
            lang = lang.substring(1);
        }
        
        if (lang.endsWith("_"))
        {
            idx = lang.indexOf("_");
            lang = lang.substring(0, idx);            
        }
        
        ret = lang;
        return ret;        
    }
    

    /**
     * Collects both GET and POST input parameters from http request.
     * 
     * 
     * @param request http servlet request
     * @return Map<String, Object>
     */
//    public static Map<String, Object> getParameterMap(HttpServletRequest request)
//    {
//        //將Request的參數全部放到userInput裡。
//        Map<String, Object> userInput = new HashMap<String, Object>();      
//        Enumeration<String> paramNames = request.getParameterNames();
//        
//        //取得一般 get 的 parameter
//        while (paramNames.hasMoreElements())  
//        {
//            String paramName = (String) paramNames.nextElement();
//
//            //對於多選的網頁元件，例如 checkbox 需再進一步檢查多選狀態
//            String[] paramValues =request.getParameterValues(paramName);
//            if (paramValues.length == 1) 
//            {
//                userInput.put(paramName, paramValues[0]);
//            } 
//            else 
//            {
//                //多選元件的值以 arraylist 放入 hashmap 中
//                List<String> list = new ArrayList<String>();
//                for(int i=0; i < paramValues.length; i++) 
//                {                   
//                    list.add(paramValues[i]);                    
//                }                
//                userInput.put(paramName, list);                
//            }
//        }       
//
//        //取得 post 的 parameter
//        try
//        {
//            //由 post data 中取得的資料為 key=value，必需加以處理
//            BufferedReader reader = request.getReader();
//            String line;
//            while ((line = reader.readLine()) != null)
//            {
//                String [] data = line.split("=");
//                if (data.length > 1)
//                {
//                    userInput.put(data[0], data[1]);                    
//                }
//            }            
//        }
//        catch(Exception e)
//        {            
//        }
//                
//        return userInput;        
//    }//getParameterMap

    public static Hashtable getParameterHashtable(HttpServletRequest request)
    {
        //將Request的參數全部放到userInput裡。
    	Hashtable userInput = new Hashtable();      
        Enumeration<String> paramNames = request.getParameterNames();
        
        //取得一般 get 的 parameter
        while (paramNames.hasMoreElements())  
        {
            String paramName = (String) paramNames.nextElement();

            //對於多選的網頁元件，例如 checkbox 需再進一步檢查多選狀態
            String[] paramValues =request.getParameterValues(paramName);
            if (paramValues.length == 1) 
            {
                userInput.put(paramName, paramValues[0]);
            } 
            else 
            {
                //多選元件的值以 arraylist 放入 hashmap 中
                List<String> list = new ArrayList<String>();
                for(int i=0; i < paramValues.length; i++) 
                {                   
                    list.add(paramValues[i]);                    
                }                
                userInput.put(paramName, list);                
            }
        }       

        //取得 post 的 parameter
        try
        {
            //由 post data 中取得的資料為 key=value，必需加以處理
            BufferedReader reader = request.getReader();
            String line;
            while ((line = reader.readLine()) != null)
            {
                String [] data = line.split("=");
                if (data.length > 1)
                {
                    userInput.put(data[0], data[1]);                    
                }
            }            
        }
        catch(Exception e)
        {            
        }
                
        return userInput;        
    }//getParameterHashtable
    
    public static String getJsonResult(BaseResult result) throws JsonProcessingException
    {
    	return jsonMapper.writeValueAsString(result);
    }
    
//    public static String getJsonResult(MVH result) throws JsonProcessingException
//    {
//    	return jsonMapper.writeValueAsString(result);
//    }
    
    /**
     * 轉型成 BaseResult，用於判斷 session 取出的資料物件是否為 BaseResult
     * 
     * @param data
     * @return
     */
	public static BaseResult toBs(Object data)
	{
		BaseResult bs = null;
		if (data instanceof BaseResult) 
		{
			bs = (BaseResult) data;
		}
		else
		{
			bs = new BaseResult();
		}
		return bs;
	}
	/**
	 * 身分證號統編遮末三碼
	 */ 
	public static String hideID(String ID){
		log.debug(ESAPIUtil.vaildLog("ID={}"+ID));
		String result = ID;
		
		if(ID.length() > 3){
			result = ID.substring(0,ID.length() - 3) + "***";
		}
		log.debug(ESAPIUtil.vaildLog("result={}"+result));
		
		return result;
	}
	
	 /**
	  * 遮蔽帳號：遮7、8、9碼(含信託帳號)
	  * @author Owner
	  *  
	  */ 
	 public static String hideaccount(String originalStr)
	 {
	  String header="";
	  String middle="";
	  String tailer="";
	  String newstr="";
	  if(originalStr.length()>0)
	  {
		  if(originalStr.length()==9)
		  {	  
			  header=originalStr.substring(0,6);
			  middle="***";
			  tailer="";	 	  
		  }else if(originalStr.length()>9)
		  {
			  header=originalStr.substring(0,6);
			  middle="***";
			  tailer=originalStr.substring(9);	  
		  }	  
	  }
	  newstr=header+middle+tailer;  
	  return newstr;
	 } 
	

	/**
	 *	台企銀新網銀 處理姓名遮照的方法
	 *	1. 先call 字霸 
	 *	2.再 call hideusername1 處理字霸轉換後的的值
	 * @param Source
	 * @return WURL
	 */
	public static String hideusername1Convert(String Source)
	{
		log.trace(ESAPIUtil.vaildLog("Source >> " + Source));
		String WURL = "";
		try
		{
//			20200921 add by hugo 目前都轉UTF-8 字霸已經不適用，難字已經在TMRA處理
			if(true) {
				int end = 0;
				if(StrUtils.isEmpty(Source)) {
					return Source;
				}
				Source = Source.replaceAll("\\s*", "").replaceAll("　", "");
				if (Source.length() == 2) {
					end = Source.length() - 1;
				}else {
					end = Source.length() - 2;
				}
//				1個字不處理 2個字遮最後一字，其餘只留前後中間遮罩
				Source = CodeUtil.mask(Source, 1, end, '０');
				return Source;
			}
			
			// 字體顏色為黑( #000000 )
			String Color = "#000000";
			// 字體大小 2 號字
			short FontSize = 2;
			// 不是斜體字
			boolean Italic = false;
			// 沒有加上底線
			boolean Underline = false;
			// 字體中間沒有一橫
			boolean Strike = false;
			// 不是粗體字
			boolean Bold = false;
			// 圖形所在 URL 目錄
			String URLPrefix = WORD_URL;
			// 設定圖形的額外參數 "align=middle border=0"
			String ImgProperties = "align=\"middle\" border=\"0\"";
			// 使用 Gif 圖形 ( 可為 Png )
			short PicType = WStrURLConvert.PIC_TYPE_GIF;
			// 使用細明體
			short FontFace = WStrURLConvert.FONT_FACE_MING;
			Source = Source.replaceAll("\\s*", "");
			WStrURLConvert wct = new WStrURLConvert();
			// 呼叫轉換
			wct.convert(Source, Color, FontFace, FontSize, Italic, Underline, Strike, Bold, PicType, URLPrefix, ImgProperties);
			// 取得轉換完結果
			WURL = WebUtil.hideusername1(wct.getResultURL());
		}
		catch (Exception e)
		{
			log.debug("Source >>>>>>>>>{}",Source);
			log.debug("WURL >>>>>>>>>{}",WURL);
			log.debug("hideusername1URL Exception",e);
			WURL = Source;
		}
		log.debug("WURL >>>>>>>>>{}",WURL);

		return WURL;
	}

	/**
	 * 配合字霸作特別處理 遮蔽姓名：二個字遮第二個字，超過二個字保留頭尾，中間遮掉
	 * 
	 * @author Owner
	 * 
	 */
	public static String hideusername1(String originalStr)
	{
		String header = "";
		String middle = "";
		String tailer = "";
		String newstr = "";
		int index1 = originalStr.indexOf("<img"), index2 = originalStr.lastIndexOf("<img"),
				index3 = originalStr.indexOf("</img>"), index4 = originalStr.lastIndexOf("</img"), count = 0;
		int len = ((index4 + 6) - index2);
		String indexstr = "";
		if (index1 >= 0)
		{
			count++;
			indexstr = String.valueOf(index1);
			while (index1 <= index2)
			{
				// System.out.println(index1);
				index1 += len;
				index1 = originalStr.indexOf("<img", index1);
				if (index1 != -1)
				{
					count++;
					indexstr += "," + index1;
				}
				else
				{
					break;
				}
			}
			log.debug("Find <img" + " : " + count + " times!");
		}
		else
		{
			log.debug("<img 找不到!");
		}
		int chinesenum = (originalStr.length() - (len * count)) + count;
		index1 = originalStr.indexOf("<img");
		index2 = originalStr.lastIndexOf("<img");
		index3 = originalStr.indexOf("</img>");
		index4 = originalStr.lastIndexOf("</img");
		int endlen = index1 + len;
		if (index1 == 0)
		{
			if (chinesenum == 2)
			{
				header = originalStr.substring(0, endlen);
				middle = "０";
				newstr = "";
			}
			else if (chinesenum == 3)
			{
				header = originalStr.substring(0, endlen);
				middle = "０";
				tailer = originalStr.substring(endlen + 1, endlen + 2);
			}
			else
			{
				header = originalStr.substring(0, endlen);
				for (int i = 0; i < chinesenum - 2; i++)
				{
					middle = middle + "０";
					endlen += 1;
				}
				tailer = originalStr.substring(endlen);
			}
			newstr = header + middle + tailer;
		}
		else if (index1 > 0)
		{
			if (chinesenum == 2)
			{
				header = originalStr.substring(0, 1);
				middle = "０";
				newstr = "";
			}
			else if (chinesenum == 3)
			{
				header = originalStr.substring(0, 1);
				if (index1 == 1)
				{
					middle = "０";
					tailer = originalStr.substring(endlen);
				}
				else
				{
					middle = "０";
					tailer = originalStr.substring(index1);
				}
			}
			else
			{
				header = originalStr.substring(0, 1);
				if (index2 == originalStr.length() - len)
				{
					for (int i = 0; i < chinesenum - 2; i++)
					{
						middle = middle + "０";
					}
					tailer = originalStr.substring(index2);
				}
				else
				{
					for (int i = 0; i < chinesenum - 2; i++)
					{
						middle = middle + "０";
					}
					tailer = originalStr.substring(originalStr.length() - 1);
				}
			}
			newstr = header + middle + tailer;
		}
		else
		{
			String originalStr_1 = convertFullorHalf(originalStr.replace((char) 12288, ' ').trim(), 1);
			if (originalStr_1.length() > 0)
			{
				if (originalStr_1.length() == 2)
				{
					header = originalStr_1.substring(0, 1);
					middle = "０";
					newstr = "";
				}
				else if (originalStr_1.length() == 3)
				{
					header = originalStr_1.substring(0, 1);
					middle = "０";
					tailer = originalStr_1.substring(2, 3);
				}
				else
				{
					header = originalStr_1.substring(0, 1);
					for (int i = 0; i < originalStr_1.length() - 2; i++)
					{
						middle = middle + "０";
					}
					tailer = originalStr_1.substring(originalStr_1.length());
				}
			}
			newstr = header + middle + tailer;
		}
		return newstr;
	}

	
//	/**
//	 * 姓名二個字遮第二個字，超過二個字保留頭尾，中間遮掉
//	 */
//	public static String hideUsername(String username){
//		log.debug("username={}",username);
//		
//		String header = "";
//		String middle = "";
//		String tailer = "";
//		String result = "";
//		String fullString = convertFullorHalf(username.replace((char)12288,' ').trim(),1);
//		
//		if(fullString.length() > 0){
//			if(fullString.length() == 2){
//				header = fullString.substring(0,1);
//				middle = "０";
//				result = "";
//			}
//			else if(fullString.length() == 3){
//				header = fullString.substring(0,1);
//				middle = "０";
//				tailer = fullString.substring(2,3);	  
//			}
//			else{
//				header = fullString.substring(0,1);
//				for(int i=0;i<fullString.length()-2;i++){
//					middle = middle + "０";
//				}	  
//				tailer = fullString.substring(fullString.length()-1);	 	  
//			}
//		}
//		result = header + middle + tailer;
//		log.debug("result={}",result);
//		
//		return result;
//	}
//	
	
	/**
	 * 轉換全型半型option 0 toHalf，1 toFull
	 */
	public static String convertFullorHalf(String originalString,int option){
		String[] asciiTable = {" ","!","\"","#","$","%","&","'","(",")","*","+",",","-",".","/",
				"0","1","2","3","4","5","6","7","8","9",":",";","<","=",">","?","@",
				"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z",
				"[","\\","]","^","_","`",
				"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z",
				"{","|","}","~"};
		String[] big5Table = {"　","！","”","＃","＄","％","＆","’","（","）","＊","＋","，","－","\u2027","／",
				"０","１","２","３","４","５","６","７","８","９","：","；","＜","＝","＞","？","＠",
				"Ａ","Ｂ","Ｃ","Ｄ","Ｅ","Ｆ","Ｇ","Ｈ","Ｉ","Ｊ","Ｋ","Ｌ","Ｍ","Ｎ","Ｏ","Ｐ","Ｑ","Ｒ","Ｓ","Ｔ","Ｕ","Ｖ","Ｗ","Ｘ","Ｙ","Ｚ",
				"〔","\uFE68","〕","︿","＿","\uFF40",
				"ａ","ｂ","ｃ","ｄ","ｅ","ｆ","ｇ","ｈ","ｉ","ｊ","ｋ","ｌ","ｍ","ｎ","ｏ","ｐ","ｑ","ｒ","ｓ","ｔ","ｕ","ｖ","ｗ","ｘ","ｙ","ｚ",
				"｛","｜","｝","\uFF5E"};

		if(asciiTable.length == big5Table.length){
			if(originalString == null || "".equalsIgnoreCase(originalString)){
				return "";
			}
			for(int i=0;i<asciiTable.length;i++){
				//開始轉換
				if(option == 0){
					//不可以用replaceAll，會因regular expression出錯
					originalString = originalString.replace(big5Table[i].charAt(0),asciiTable[i].charAt(0));
				}
				if(option == 1){
					//不可以用replaceAll，會因regular expression出錯
					originalString = originalString.replace(asciiTable[i].charAt(0), big5Table[i].charAt(0));
				}
			}
		}
		return originalString;
	}

	/**
	 * 遮蔽帳號：遮7、8、9碼(含信託帳號)
	 * @param account
	 * @return
	 */
	public static String hideAccount(String account){
		String header = "";
		String middle = "";
		String tailer = "";
		String result = "";
		
		if(account.length() > 0){
			if(account.length() == 9){
				header = account.substring(0,6);
				middle = "***";
				tailer = "";	 	  
			}
			else if(account.length() > 9){
				header = account.substring(0,6);
				middle = "***";
				tailer = account.substring(9);	  
			}	  
		}
		result = header + middle + tailer;
		log.debug(ESAPIUtil.vaildLog("result={}"+result));
	  
		return result;
	}
	/**
	 * 信用卡號遮14、15碼
	 */ 
	public static String hideCardNum(String cardNum){
		String header = "";
		String middle = "";
		String tailer = "";
		String result="";
		
		if(cardNum.length() > 0){
			if(cardNum.length() >= 16){
				header = cardNum.substring(0,cardNum.length() - 3);
				middle = "**";
				tailer = cardNum.substring(cardNum.length() - 1);
			}
		}
		result = header + middle + tailer;
		log.debug(ESAPIUtil.vaildLog("result >> " + result));
		return result;
	}
	/**
	 * 信用卡號遮1-12碼
	 */ 
	public static String hideCardNum2(String cardNum){
		String result="";
		result = StrUtils.repeat("*", 4) + " " + StrUtils.repeat("*", 4) + " " + StrUtils.repeat("*", 4) + " " + cardNum.substring(12);
		return result;
	}
	
	
	/**
	 * 解決Trust Boundary Violation
	 */ 
	public static String getValidHeader(HttpServletRequest request, String name) {

		String value = request.getHeader(name);
		if (value == null) {
			return null;
		}
		try {
			return ESAPI.validator().getValidInput("HTTP header value: " + value, value, "HTTPHeaderValue",
					value.length(), false);
		} catch (ValidationException e) {
			return null;
		} catch (IntrusionException e) {
			return null;
		}
	}
	
	
	
}