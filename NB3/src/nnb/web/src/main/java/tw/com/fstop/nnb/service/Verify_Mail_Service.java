package tw.com.fstop.nnb.service;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.po.VERIFYMAIL;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.VerifyMailDao;
import tw.com.fstop.tbb.nnb.dao.VerifyMailHisDao;
import tw.com.fstop.tbb.nnb.util.DateUtil;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.SpringBeanFactory;

@Service
public class Verify_Mail_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private VerifyMailDao verifyMailDao;
	
	@Autowired
	private I18n i18n;

	public BaseResult verify(Map<String, String> reqParam) {

		BaseResult bs = new BaseResult();

		try {
			String cusidn = "";
			String decode_String = "";

			// 先取資料
			VERIFYMAIL verifyData = verifyMailDao.findByENC_URL(reqParam.get("url"));

			if (null == verifyData) {
				bs.setMsgCode("ENRD");
				bs.setResult(false);
				getMessageByMsgCode(bs);
				return bs;
			}

			// 跟解碼後比對
			cusidn = verifyData.getCUSIDN();

			// 解碼
			Map<String, String> reqMap = new HashMap<String, String>();
			reqMap.put("url", reqParam.get("url"));
			reqMap.put("seedForRandom", SpringBeanFactory.getBean("VERIFY_MAIL_AES_KEY"));
			decode_String = CodeUtil.aes_decode(reqMap);

			log.info("decode_String :{}", decode_String);
			
			String dataCusidn = decode_String.substring(decode_String.length() - cusidn.length());

			// 身分證資料比對錯誤
			if (!cusidn.equals(dataCusidn)) {
				bs.setErrorMessage("ZX99", i18n.getMsg("LB.X2631"));
				bs.setResult(false);
				return bs;
			}
			
			//查看信件狀態
			String mailStatus = verifyData.getSTATUS();
			if(!"0".equals(mailStatus)) {
				bs.setErrorMessage("ZX99", i18n.getMsg("LB.X2632"));
				bs.setResult(false);
				return bs;
			}
			
			//比對驗證碼
			String customVERIFYCODE = reqParam.get("VERIFYCODE");
			
			String VERIFYCODE = new String(Base64.getDecoder().decode(verifyData.getVERIFYCODE()), "utf-8");
			
			if(!customVERIFYCODE.equals(VERIFYCODE)) {
				bs.setErrorMessage("ZX99", i18n.getMsg("LB.X2633"));
				bs.setResult(false);
				return bs;
			}
			
			//都過了要串N930參數 , 並把bs Result 設成 true
			bs = n930param(bs , verifyData , reqParam.get("IP"));

		} catch (NoSuchAlgorithmException | UnsupportedEncodingException | NoSuchPaddingException
				| IllegalBlockSizeException | BadPaddingException | InvalidKeyException e) {
			
			log.error("Verify_Mail_Service verify 解碼錯誤 > {}",e);
			bs.setErrorMessage("ZX99", "解碼錯誤");
			bs.setResult(false);
			return bs;
		
		} catch (Exception e) {
		
			log.error("Verify_Mail_Service verify 未知錯誤 > {}" ,e);
			bs.setErrorMessage("ZX99", "未知錯誤");
			bs.setResult(false);
			return bs;
		}
		return bs;
	}
	
	public BaseResult n930param(BaseResult bs , VERIFYMAIL data , String ip) {
		Map n930data = new HashMap<>();
		// N930EE會用到各種的 身分證字號呢
		n930data.put("DPUSERID", data.getCUSIDN());
		n930data.put("UID", data.getCUSIDN());
		n930data.put("CUSIDN", data.getCUSIDN());
		// N930EE會用到
		n930data.put("NEW_EMAIL", data.getNMAIL());
		n930data.put("EXECUTEFUNCTION", "UPDATE_TX");
		
		//N930電文會用到
		n930data.put("FLAG", "06"); //固定06
		n930data.put("E_MAIL_ADDR", data.getNMAIL());
		n930data.put("BRHCOD", data.getBRHCOD());
		n930data.put("TLRNUM", data.getTLRNUM());
		n930data.put("TRNNUM", data.getTRNNUM());
		
		n930data.put("IP", ip);
		
		
		bs.addAllData(n930data);
		bs.setResult(true);
		return bs;
	}
	
	public BaseResult emailAlertShow(String cusidn) {
		BaseResult bs = new BaseResult();
		try {
			VERIFYMAIL data = verifyMailDao.findActiveMailById(cusidn);
			Map dataMap = new HashMap<>();
			//只拿需要的 , 避免被頁面上竊取
			dataMap.put("TXDATE", DateUtil.addSlash(data.getTXDATE()));
			dataMap.put("EXPIREDATE", DateUtil.addSlash(data.getEXPIREDATE()));
			dataMap.put("EMAIL", CodeUtil.emailMask(data.getNMAIL()));
			
			bs.addAllData(dataMap);
			bs.setResult(true);
			
		}catch (Exception e) {
			log.error("emailAlertShow error " , e);
		}
		
		return bs;
	}
	
}
