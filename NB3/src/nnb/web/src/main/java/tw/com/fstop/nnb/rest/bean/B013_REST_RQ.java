package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class B013_REST_RQ extends BaseRestBean_FUND implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2367187476092389030L;
	//*為必要欄位
	//暫時移除CDNO欄位
	String CUSIDN;//*統一編號
	String BEGDATE;//*查詢起日
	String ENDDATE;//*查詢迄日
	String MAC;//MAC
	String RECNO;//讀取筆數
	String NEXT;//資料起始位置
	public String getMAC() {
		return MAC;
	}
	public void setMAC(String mAC) {
		MAC = mAC;
	}
	public String getRECNO() {
		return RECNO;
	}
	public void setRECNO(String rECNO) {
		RECNO = rECNO;
	}
	public String getNEXT() {
		return NEXT;
	}
	public void setNEXT(String nEXT) {
		NEXT = nEXT;
	}

	
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getBEGDATE() {
		return BEGDATE;
	}
	public void setBEGDATE(String bEGDATE) {
		BEGDATE = bEGDATE;
	}
	public String getENDDATE() {
		return ENDDATE;
	}
	public void setENDDATE(String eNDDATE) {
		ENDDATE = eNDDATE;
	}

	
}
