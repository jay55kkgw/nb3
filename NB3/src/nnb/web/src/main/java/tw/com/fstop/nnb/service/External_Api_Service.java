package tw.com.fstop.nnb.service;

import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.po.ADMLOGIN;
import tw.com.fstop.tbb.nnb.dao.AdmLoginDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;

@Service
public class External_Api_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private AdmLoginDao admlogindao;


	/**
	 * 驗證使用者 登入/登出 狀態
	 * @param cusidn 使用者統編
	 * @return
	 */
	public Boolean isLoginStatus(String cusidn) {
		boolean result = true;
		List<ADMLOGIN> admloginList = null;
		
		try {
			log.trace(ESAPIUtil.vaildLog("External_Api_Service.isLoginStatus.cusidn: {}"+ CodeUtil.toJson(cusidn)));
			
			String loginout = "";
			
			// 取得登入時資訊
			admloginList = admlogindao.findByUserId(cusidn);
			if(admloginList != null && !admloginList.isEmpty()) {
				for(ADMLOGIN po : admloginList) {
					loginout = po.getLOGINOUT();
				}
			}
			
			// 登入狀態	0:登入,1:登出,null:不曾登入
			result = "0".equals(loginout) ? false : true;
			
		} catch (Exception e) {
			log.error("", e);
		}
		
		return result;
	}

	
	/**
	 * 強迫登出--更改使用者 登入/登出 狀態檔 資訊
	 * @param cusidn 使用者統編
	 * @return
	 */
	public Boolean logoutForce(String cusidn) {
		boolean result = false;
		List<ADMLOGIN> admloginList = null;
		try {
			log.trace(ESAPIUtil.vaildLog("External_Api_Service.logoutForce.cusidn: " + CodeUtil.toJson(cusidn)));
			
			// 取得登入時資訊
			admloginList = admlogindao.findByUserId(cusidn);
			if(admloginList != null && !admloginList.isEmpty()) {
				for(ADMLOGIN po : admloginList) {
					po.setLOGINOUT("1"); // 登入狀態	0:登入 1:登出
					po.setFORCELOGOUTTIME(new DateTime(new Date()).toString("yyyyMMddHHmmss")); // 本次強迫登出日期時間
					po.setLASTLOGINTIME(po.getLOGINTIME());	// 上次登入日期時間
					po.setLASTADUSERIP(po.getADUSERIP()); // 上次登入IP
					log.trace("External_Api_Service.logoutForce.po: {}", CodeUtil.toJson(po));
					admlogindao.update(po);
				}
			}
			result = true;
		} catch (Exception e) {
			log.error("", e);
		}
		
		return result;
	}
	
	
	/**
	 * 查詢使用者是否曾經登入過
	 * @param cusidn 使用者統編
	 * @return
	 */
	public Boolean onceLogin(String cusidn) {
		boolean result = false;
		List<ADMLOGIN> admloginList = null;
		try {
			log.trace(ESAPIUtil.vaildLog("External_Api_Service.onceLogin.cusidn: " + cusidn));
			
			// 取得登入時資訊
			admloginList = admlogindao.findByUserId(cusidn);
			// 曾經登入過
			if(admloginList != null && !admloginList.isEmpty()) {
				result = true;
			}
		} catch (Exception e) {
			log.error("", e);
		}
		
		return result;
	}
	
}
