package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N934_REST_RQ extends BaseRestBean_PS implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5051219122081175767L;
	
	private String ADOPID;
	
	private String CUSIDN;//統一編號
	
	private String REC_NO;//筆數
	
	private String ACNINFO;//匯入匯款通知帳號資訊

	public String getADOPID() {
		return ADOPID;
	}

	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}

	public String getREC_NO() {
		return REC_NO;
	}

	public void setREC_NO(String rEC_NO) {
		REC_NO = rEC_NO;
	}

	public String getACNINFO() {
		return ACNINFO;
	}

	public void setACNINFO(String aCNINFO) {
		ACNINFO = aCNINFO;
	}

	
	
}
