package tw.com.fstop.nnb.ws.server.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.transform.Source;

import org.springframework.ws.server.endpoint.PayloadEndpoint;
//@XmlRootElement(namespace = "http://ws.fstop", name="GoldSell")
@XmlRootElement(namespace = "http://ws.fstop", name="informGoldValueResponse")
@XmlAccessorType(XmlAccessType.FIELD)
//@XmlType(name = "", propOrder = {
//		"out"
//})
public class InformGoldValueResponse  {
	@XmlElement(namespace = "http://ws.fstop")
	public GoldSell goldSell ;

	public GoldSell getGoldSell() {
		return goldSell;
	}

	public void setGoldSell(GoldSell goldSell) {
		this.goldSell = goldSell;
	}

	
	
	
}
