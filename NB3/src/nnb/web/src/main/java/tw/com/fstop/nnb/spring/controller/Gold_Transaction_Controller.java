package tw.com.fstop.nnb.spring.controller;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.google.gson.Gson;

import fstop.orm.po.GOLDPRICE;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.idgate.bean.N094_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N094_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N09001_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N09001_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N09002_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N09002_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N09101_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N09101_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N09102_IDGATE_DATA_VIEW;
import tw.com.fstop.nnb.service.Fund_Transfer_Service;
import tw.com.fstop.nnb.service.Gold_Passbook_Service;
import tw.com.fstop.nnb.service.Gold_Transaction_Service;
import tw.com.fstop.nnb.service.IdGateService;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.GoldPriceDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.WebUtil;

@SessionAttributes({ SessionUtil.PD, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, SessionUtil.PRINT_DATALISTMAP_DATA,
		SessionUtil.RESULT_LOCALE_DATA, SessionUtil.CONFIRM_LOCALE_DATA, SessionUtil.RESULT_LOCALE_DATA,
		SessionUtil.CUSIDN, SessionUtil.TRANSFER_CONFIRM_TOKEN, SessionUtil.TRANSFER_RESULT_TOKEN, SessionUtil.IDGATE_TRANSDATA,
		SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, SessionUtil.KYCDATE, SessionUtil.KYC, SessionUtil.GOLDAGREEFLAG,
		SessionUtil.XMLCOD, SessionUtil.DPMYEMAIL, SessionUtil.DPUSERNAME, SessionUtil.TRANSFER_DATA,SessionUtil.STEP2_LOCALE_DATA,SessionUtil.STEP1_LOCALE_DATA,SessionUtil.IDGATE_TRANSDATA })
@Controller
@RequestMapping(value = "/GOLD/TRANSACTION")
public class Gold_Transaction_Controller {

	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private Gold_Transaction_Service gold_transaction_service;
	@Autowired
	private Fund_Transfer_Service fund_transfer_service;
	@Autowired
	private Gold_Passbook_Service gold_passbook_service;
	@Autowired
	private GoldPriceDao goldPriceDao;
	
	@Autowired
	I18n i18n;
	@Autowired		 
	IdGateService idgateservice;
	// 繳納定期扣款失敗手續費(N094)
	@RequestMapping(value = "/pay_fail_fee")
	public String pay_fail_fee(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("pay_fail_fee>>");
		try {
			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = gold_transaction_service.getTxToken();
			log.trace("gold_account_apply.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			if(bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			}
			
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			
			bs = new BaseResult();
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			bs.reset();
			
			//IDGATE身分
			String idgateUserFlag="N";
			try {
				BaseResult tmp=idgateservice.checkIdentity(cusidn);
				if(tmp.getResult()) {
					idgateUserFlag="Y";					
				}
				tmp.addData("idgateUserFlag",idgateUserFlag);
				SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA,((Map<String, String>) tmp.getData()));
			}catch(Exception e) {
				log.debug("idgateUserFlag error {}",e);
			}
			log.debug("idgateUserFlag : " + idgateUserFlag);
			model.addAttribute("idgateUserFlag",idgateUserFlag);
			bs = gold_transaction_service.pay_fail_fee(cusidn, reqParam);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("pay_fail_fee error >> {}",e);
		} finally {
			if (bs != null && bs.getResult())
			{
				if( ((Map<String, Object>) bs.getData()).get("ToGoFlag").equals("1")) {
					target = "/gold/pay_fail_fee";
				}else {
					target = "/gold/gold_reservation_query_confirm";
				}
				List<Map<String, Object>> dataListMap = ((List<Map<String, Object>>) ((Map<String, Object>) bs.getData()).get("REC"));
				log.debug("dataListMap={}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
				model.addAttribute("result_data", bs);
			}
			else
			{
				bs.setPrevious("/INDEX/index");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	// 繳納定期扣款失敗手續費(N094)
	@RequestMapping(value = "/pay_fail_fee_confirm")
	public String pay_fail_fee_confirm(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		BaseResult bsN920 = null;
		String jsondc = "";
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		log.trace("pay_fail_fee_confirm>>");
		try {
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("pay_fail_fee_confirm.jsondc: " + jsondc));
			
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			
			bs = new BaseResult();
			bsN920 = new BaseResult();
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				// 驗證TOKEN 沒TOKEN會到錯誤頁，錯誤頁確認後返回輸入頁
				String token = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null);
				log.trace("gold_account_apply.token: {}", token);
				log.trace(ESAPIUtil.vaildLog("gold_account_apply.okMap.token: {}"+ okMap.get("TOKEN")));
				if(StrUtil.isNotEmpty(okMap.get("TOKEN"))  &&  okMap.get("TOKEN").equals(token)) {
					bs.setResult(Boolean.TRUE);
				}
				
				log.trace("bs.getResult(): {}",bs.getResult());
				if(!bs.getResult()) {
					log.trace("throw new Exception()");
					throw new Exception();
				}
				bs.reset();
				bs = gold_transaction_service.pay_fail_fee_confirm(cusidn, okMap);
				Map<String, String> data = (Map<String,String>)bs.getData();
				// IDGATE transdata            		 
	            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
	            if(IdgateData != null) {
		           model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
		           String adopid = "N094";
		    	   String title ="";
		    	   title = "您有一筆繳納定期扣款失敗手續費交易待確認，手續費合計金額" + data.get("TRNAMT");
		    	   model.addAttribute("idgateAdopid",adopid);
		    	   model.addAttribute("idgateTitle",title);
	            }
				
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				
			}
			bsN920 = gold_transaction_service.N920_REST(cusidn, gold_transaction_service.OUT_ACNO);
			bs.addData("jsondc", jsondc);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("pay_fail_fee_confirm error >> {}",e);
		} finally {
			if (bs != null && bs.getResult())
			{
				target = "/gold/pay_fail_fee_confirm";
				List<Map<String, Object>> dataListMap = ((List<Map<String, Object>>) ((Map<String, Object>) bs.getData()).get("REC"));
				log.debug("dataListMap={}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, okMap.get("TOKEN"));
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
				model.addAttribute("result_data", bs);
				model.addAttribute("n920_data", bsN920);
			}
			else
			{
				bs.setPrevious("/GOLD/TRANSACTION/pay_fail_fee");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	// 繳納定期扣款失敗手續費(N094)
	@RequestMapping(value = "/pay_fail_fee_result")
	public String pay_fail_fee_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		String jsondc = "";
		Map<String, String> okMap = null;
		
		
		log.trace("pay_fail_fee_result>>");
		try {
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			
			bs = new BaseResult();
			okMap = ESAPIUtil.validStrMap(reqParam);
			//okMap = reqParam;
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				log.trace("gold_account_apply.validate TXTOKEN...");
				log.trace("gold_account_apply.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("gold_account_apply.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
			
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
						okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("gold_account_apply.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("gold_account_apply TXTOKEN 不正確，TXTOKEN>>{}"+ okMap.get("TOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
						!okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("gold_account_apply.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("gold_account_apply TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				
				//IDgate驗證
				try {
					if(okMap.get("FGTXWAY").equals("7")) {
						Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
						Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N094_IDGATE");
						okMap.put("sessionID",(String)IdgateData.get("sessionid"));
						okMap.put("txnID",(String)IdgateData.get("txnID"));
						okMap.put("idgateID",(String)IdgateData.get("IDGATEID"));
						okMap.put("FGTXWAY","7");	
					}
				}catch(Exception e) {
					log.error("IDGATE ValidateFail>>{}",bs.getResult());
				}
				
				Map<String,Object>IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
				model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
				
				bs = gold_transaction_service.pay_fail_fee_result(cusidn, okMap);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				
				
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("pay_fail_fee_result error >> {}",e);
		} finally {
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TOKEN"));
			if (bs != null && bs.getResult())
			{
				target = "/gold/pay_fail_fee_result";
				List<Map<String, Object>> dataListMap = ((List<Map<String, Object>>) ((Map<String, Object>) bs.getData()).get("REC"));
				log.debug("dataListMap={}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
				model.addAttribute("result_data", bs);
			}
			else
			{
				bs.setPrevious("/GOLD/TRANSACTION/pay_fail_fee");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	// 黃金帳號Ajax
	@RequestMapping(value = "/goldacn_ajax", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody List<Map<String, String>> goldacn_ajax(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("goldacn_ajax...");
		List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();
		try {
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			String acn = reqParam.get("ACN");
			// 帳號資料
			resultList = gold_transaction_service.getGoldAcn(acn, cusidn);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("goldacn_ajax error >> {}",e);
		}
		return resultList;
	}
	/**
	 * N09002黃金回售第一步
	 */
	@SuppressWarnings({"static-access","unchecked"})
	@RequestMapping(value = "/gold_resale")
	public String gold_resale(HttpServletRequest request,@RequestParam Map<String,String> reqParam,Model model){
		String target = "/error";
		log.debug("gold_resale");
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		BaseResult bs = new BaseResult();
		
		try{
			//如果是從KYC來會帶入多餘的交易機制參數,故移除
			okMap.remove("jsondc");
			okMap.remove("pkcs7Sign");
			
			String reqACN = okMap.get("ACN");
			log.debug(ESAPIUtil.vaildLog("reqACN={}"+reqACN));
			if(reqACN == null){
				reqACN = "#";
			}
			log.debug(ESAPIUtil.vaildLog("reqACN={}"+reqACN));
			model.addAttribute("reqACN", reqACN);
			
			String cusidn = new String(Base64.getDecoder().decode((String)SessionUtil.getAttribute(model,SessionUtil.CUSIDN,null)));
			log.debug(ESAPIUtil.vaildLog("cusidn={}"+cusidn));

			//檢查是否有黃金存摺帳號
			BaseResult bs920 = gold_transaction_service.N920_REST(cusidn,gold_transaction_service.GOLD_RESERVATION_QOERY_N920);
			log.debug("bs920={}",bs920);
			
			if(bs920 != null && bs920.getResult()){
				Map<String,Object> bs920Data = (Map<String,Object>)bs920.getData();
				
				//使用者名稱存SESSION，黃金單據列印頁會顯示
				String NAME = (String)bs920Data.get("NAME");
				log.debug("NAME={}",NAME);
				SessionUtil.addAttribute(model,SessionUtil.DPUSERNAME,NAME);
				
				List<Map<String,String>> rows = (List<Map<String,String>>)bs920Data.get("REC");
				//沒有黃金存摺帳號
				if(rows.isEmpty()){
					log.trace("rows is Empty()");
				//假的
				//if(false){
				//if(true){
					BaseResult bsParm = new BaseResult();
					bsParm.addData("TXID", "gold_resale");
					bsParm.addData("MSGTYPE", "N920");
					SessionUtil.addAttribute(model,SessionUtil.STEP1_LOCALE_DATA,bsParm);
					log.trace("TXID>>gold_resale MSGTYPE>>N920");
					target = "forward:/GOLD/TRANSACTION/gold_applyconfirm";
					return target;
				}
			}
			
			//檢查是否有黃金存摺網路交易帳號
			BaseResult bs926 = gold_transaction_service.N926_REST(cusidn,gold_transaction_service.GOLD_RESERVATION_QOERY);
			if(bs926 != null && bs926.getResult()){
				Map<String,Object> bs926Data = (Map<String,Object>)bs926.getData();
				List<Map<String,String>> rows = (List<Map<String,String>>)bs926Data.get("REC");
			
				//沒有黃金存摺網路交易帳號
				if(rows.isEmpty()){
				//假的
				//if(false){
				//if(true){
					
					BaseResult bsParm = new BaseResult();
					bsParm.addData("TXID", "gold_resale");
					bsParm.addData("MSGTYPE", "N926");
					SessionUtil.addAttribute(model,SessionUtil.STEP1_LOCALE_DATA,bsParm);
					log.trace("TXID>>gold_resale MSGTYPE>>N926");
					target = "forward:/GOLD/TRANSACTION/gold_applyconfirm";
					return target;
				}
			}
			//判斷黃金存摺業務營業時間
			boolean isGoldTradeTime = gold_transaction_service.isGoldTradeTime();
			log.debug("isGoldTradeTime={}",isGoldTradeTime);
			
			//判斷黃金存摺是否可執行預約交易
			boolean isGoldScheduleTime = gold_transaction_service.isGoldScheduleTime();
			log.trace("isGoldScheduleTime={}",isGoldScheduleTime);
			
			//如果不能執行即時或預約交易的話，顯示錯誤
			if(isGoldTradeTime == false && isGoldScheduleTime == false){
				bs.setMessage("Z999",i18n.getMsg("LB.X1774"));
				model.addAttribute(BaseResult.ERROR,bs);
				return target;
			}
			
			//預約不判斷牌告
			if(!isGoldScheduleTime) {
				String twDate = DateUtil.getTWDate("");
				log.debug("twDate={}",twDate);
				
				List<GOLDPRICE> goldPriceList = goldPriceDao.getTODAYRecord(twDate);
				log.debug(ESAPIUtil.vaildLog("goldPriceList="+goldPriceList));
				
				if(goldPriceList.isEmpty()){
					//假的
					//if(false){
					bs.setMessage("Z410",i18n.getMsg("LB.X1775"));
					model.addAttribute(BaseResult.ERROR,bs);
					return target;
				}
			}
			
			String GOLDAGREEFLAG = (String)SessionUtil.getAttribute(model,SessionUtil.GOLDAGREEFLAG,null);
			log.debug(ESAPIUtil.vaildLog("GOLDAGREEFLAG={}"+GOLDAGREEFLAG));
			
			//檢查用戶是否已經同意約定條款
			if(GOLDAGREEFLAG != null){
				//黃金回售
		    	if(isGoldTradeTime == true){
				//假的
				//if(true){
				//if(false){
		    		target = "forward:/GOLD/TRANSACTION/gold_resale_select?ADOPID=N09002";
		    	}
				//預約黃金回售
		    	else if(isGoldScheduleTime == true){
				//假的
				//else{
		    		target = "forward:/GOLD/TRANSACTION/gold_reserve_resale_select?ADOPID=N09102";
		    	}
			}
			else{
				String ADOPID = "";
				//黃金回售
		    	if(isGoldTradeTime == true){
				//假的
				//if(true){
				//if(false){
		    		ADOPID = "N09002";
		    	}
		    	//預約黃金回售
		    	else if(isGoldScheduleTime == true){
		    	//假的
				//else{
		    		ADOPID = "N09102";
		    	}
		    	model.addAttribute("ADOPID",ADOPID);
		    	
				target = "gold/gold_term";
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_resale error >> {}",e);
		}
		return target;
	}
	/**
	 * N09002黃金即時回售選擇頁
	 */
	@RequestMapping(value = "/gold_resale_select")
	public String gold_resale_select(HttpServletRequest request,@RequestParam Map<String,String> reqParam,Model model){
		String target = "/error";
		
		log.debug("gold_resale_select");
		try{
			//如果是從KYC來會帶入多餘的交易機制參數,故移除
			reqParam.remove("jsondc");
			reqParam.remove("pkcs7Sign");
			// 解決Trust Boundary Violation 
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam); 
			//
			String cusidn = new String(Base64.getDecoder().decode((String)SessionUtil.getAttribute(model,SessionUtil.CUSIDN,null)),"UTF-8");
			log.debug("cusidn={}",cusidn);
			model.addAttribute("UID",cusidn);
			
			//先找出是否有GOLDAGREEFLAG的值
			String GOLDAGREEFLAG = okMap.get("GOLDAGREEFLAG");
			log.debug(ESAPIUtil.vaildLog("GOLDAGREEFLAG={}"+GOLDAGREEFLAG));
			
			//如果是Y的話就存在SESSION
			if("Y".equals(GOLDAGREEFLAG)){
				SessionUtil.addAttribute(model,SessionUtil.GOLDAGREEFLAG,GOLDAGREEFLAG);
			}
			
			String reqACN = okMap.get("ACN");
			log.debug(ESAPIUtil.vaildLog("reqACN={}"+reqACN));
			model.addAttribute("reqACN",reqACN);
			
			BaseResult n927BS = new BaseResult();
			n927BS = fund_transfer_service.getN927Data(cusidn);
			
			if(n927BS == null || !n927BS.getResult()){
				model.addAttribute(BaseResult.ERROR,n927BS);
				return target;
			}
			Map<String,Object> n927BSData = (Map<String,Object>)n927BS.getData();
			log.debug("n927BSData={}",n927BSData);
			
			String RISK = n927BSData.get("RISK") == null ? "" : (String)n927BSData.get("RISK");
			log.debug("RISK={}",RISK);
			
			//問卷填寫日期
			String GETLTD = n927BSData.get("GETLTD") == null ? "" : (String)n927BSData.get("GETLTD");
			log.debug("GETLTD={}",GETLTD);
			
			String time1 = "";
			String time2 = "";
			long day = 0;
			if(!GETLTD.equals("")){
				time1 = new SimpleDateFormat("yyyy/MM/dd").format(new Date());
				log.debug("time1={}",time1);
				String GETLTDString = Integer.valueOf(GETLTD.substring(0,3)) + 1911 + "";
				GETLTDString = GETLTDString + GETLTD.substring(3);
				log.debug("GETLTDString={}",GETLTDString);
				time2 = GETLTDString.substring(0,4) + "/" + GETLTDString.substring(4,6) + "/" + GETLTDString.substring(6,8);
				log.debug("time2={}",time2);
				
				//算出距離上次問卷填寫天數
				day = fund_transfer_service.getRangeDay(time1,time2);
			}
			
			String KYC = okMap.get("KYC");
			log.debug(ESAPIUtil.vaildLog("KYC={}"+KYC));
			
			if("PASS".equals(KYC)){
				SessionUtil.addAttribute(model,SessionUtil.KYC,KYC);
			}
			
			//找出是否有KYC PASS的值
			String KYCSession = (String)SessionUtil.getAttribute(model,SessionUtil.KYC,null);
			log.debug(ESAPIUtil.vaildLog("KYCSession={}"+KYCSession));
			
			//問卷版本日期
			String KYCDate = fund_transfer_service.getKYCDate();
			
			if(KYCDate == null){
				KYCDate = "";
			}
			
			if(KYCSession == null){
				if((RISK.equals("") || (!GETLTD.equals("") && day >= 365) || (!KYCDate.equals("") && Integer.parseInt(KYCDate) > Integer.parseInt(GETLTD)))){//自然人、法人版，無投資屬性或問卷填寫日期已滿一年或問卷版本日期大於問卷填寫日期	
				//假的
//				if(true){
					model.addAttribute("TXID","N09002");
					
					if(day >= 365){//滿一年
						model.addAttribute("ALERTTYPE","2");
					}
					else if(RISK.equals("")){//無投資屬性
					//假的
//					if(true){
						model.addAttribute("ALERTTYPE","1");
					}
					target = "gold/gd_inv_attr";
					return target;
		    	}
			}
			
			//取得黃金存摺網路交易帳號
			String type ="SELL";
			List<Map<String,String>> goldTradeAccountList = gold_transaction_service.getGoldTradeAccountList(cusidn,type);
			
			model.addAttribute("goldTradeAccountList",goldTradeAccountList);
			
			//現在時間
			String nowDate = new SimpleDateFormat("yyyy/MM/dd").format(new Date());
			log.debug("nowDate={}",nowDate);
			model.addAttribute("nowDate",nowDate);
			
			target = "gold/gold_resale_select";
			
           //IDGATE身分
           String idgateUserFlag="N";		 
           Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
           try {		 
        	   if(IdgateData==null) {
        		   IdgateData = new HashMap<String, Object>();
        	   }
               BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
               if(tmp.getResult()) {		 
                   idgateUserFlag="Y";                  		 
               }		 
               tmp.addData("idgateUserFlag",idgateUserFlag);		 
               IdgateData.putAll((Map<String, String>) tmp.getData());
               SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
           }catch(Exception e) {		 
               log.debug("idgateUserFlag error {}",e);		 
           }		 
           model.addAttribute("idgateUserFlag",idgateUserFlag);
           
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_resale_select error >> {}",e);
		}
		return target;
	}
	/**
	 * N09002 黃金即時回售確認頁
	 */
	@RequestMapping(value = "/gold_resale_confirm")
	public String gold_resale_confirm(HttpServletRequest request,@RequestParam Map<String,String> reqParam,Model model){
		String target = "/error";
		BaseResult bs = null;
		log.debug("gold_resale_confirm");
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try{
			
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				Map<String, String> confirm_locale_dataMap = (Map<String, String>) SessionUtil.getAttribute(model,
						SessionUtil.CONFIRM_LOCALE_DATA, null);
				for (String key : confirm_locale_dataMap.keySet()) {
					model.addAttribute(key, confirm_locale_dataMap.get(key));
				}
				target = "gold/gold_resale_confirm";
			} else {

				String cusidn = new String(
						Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
						"UTF-8");
				log.debug("cusidn={}", cusidn);
				okMap.put("CUSIDN", cusidn);
				okMap.put("UID", cusidn);

				String XMLCOD = (String) SessionUtil.getAttribute(model, SessionUtil.XMLCOD, null);
				log.debug("XMLCOD={}", XMLCOD);
				model.addAttribute("XMLCOD", XMLCOD);

				bs = gold_transaction_service.gold_resale_confirm(okMap, model);

				if (bs == null || !bs.getResult()) {
					model.addAttribute(BaseResult.ERROR, bs);
					SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, model);
					return target;
				}

				target = "gold/gold_resale_confirm";
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, model);
				
				// IDGATE transdata            		 
	            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
	    		String adopid = "N09002";
	    		String title = "您有一筆黃金回售待確認";
	    		Map<String, String> result = new HashMap<String, String>();
	    		Map<String, Object> bsData = (Map<String, Object>)bs.getData();
	    		for(String key : bsData.keySet()) {
	    			try {
	    				result.put(key, (String)bsData.get(key));
	    			}catch(Exception e){
	    			}
	    		}
	        	if(IdgateData != null) {
		            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
		            if(IdgateData.get("idgateUserFlag").equals("Y")) {
		            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
		            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
		            	result.put("TRANTITLE", "黃金回售");
		            	result.put("TRANNAME", "即時");
		            	result.put("TRNCOD", "02");
		            	
		    			String str_TRNAMT = (String)result.get("TRNAMT");
		    			String str_FEEAMT1 = (String)result.get("FEEAMT1");
		    			String str_FEEAMT2 = (String)result.get("FEEAMT2");
		    			String str_TRNAMT2 = (String)result.get("TRNAMT2");	
		    			if ((str_TRNAMT.indexOf("+") != -1) || (str_TRNAMT.indexOf("-") != -1)) {
		    				result.put("TRNAMT_SIGN", str_TRNAMT.substring(0,1).equals("+") ? " " : "-");
		    				result.put("TRNAMT", str_TRNAMT.substring(1));			
		    			}
		    			else {
		    				result.put("TRNAMT_SIGN", " ");			
		    			}
		    			if ((str_FEEAMT1.indexOf("+") != -1) || (str_FEEAMT1.indexOf("-") != -1)) {
		    				result.put("FEEAMT1_SIGN", str_FEEAMT1.substring(0,1).equals("+") ? " " : "-");
		    				result.put("FEEAMT1", str_FEEAMT1.substring(1));				
		    			}
		    			if ((str_FEEAMT2.indexOf("+") != -1) || (str_FEEAMT2.indexOf("-") != -1)) {
		    				result.put("FEEAMT2_SIGN", str_FEEAMT2.substring(0,1).equals("+") ? " " : "-");
		    				result.put("FEEAMT2", str_FEEAMT2.substring(1));				
		    			}
		    			if ((str_TRNAMT2.indexOf("+") != -1) || (str_TRNAMT2.indexOf("-") != -1)) {
		    				result.put("TRNAMT2_SIGN", str_TRNAMT2.substring(0,1).equals("+") ? " " : "-");
		    				result.put("TRNAMT2", str_TRNAMT2.substring(1));
		    			}
		    			String str_PRICE = (String)result.get("PRICE");
		    			try {
		    				result.put("PRICE", NumericUtil.formatDouble(Double.parseDouble(str_PRICE) / 100, 2));
		    			}
		    			catch (Exception e) {
		    				log.error("doAction error>>{}", e);
		    			}
		    			
						IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N09002_IDGATE_DATA.class, N09002_IDGATE_DATA_VIEW.class, result));

		                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
		            }
	        	}
	            model.addAttribute("idgateAdopid", adopid);
	            model.addAttribute("idgateTitle", title);
	            
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_resale_confirm error >> {}",e);
		}
		return target;
	}
	/**
	 * N09002 黃金即時回售結果頁
	 */
	@RequestMapping(value = "/gold_resale_result")
	public String gold_resale_result(HttpServletRequest request,@RequestParam Map<String,String> requestParam,Model model){
		String target = "/error";
		
		log.debug("gold_resale_result");
		String jsondc= requestParam.get("jsondc");
		String pkcs7Sign = requestParam.get("pkcs7Sign");
		requestParam.remove("jsondc");
		requestParam.remove("pkcs7Sign");
		Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
		okMap.put("jsondc", jsondc);
		okMap.put("pkcs7Sign", pkcs7Sign);
		try{
			
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				Map<String, String> result_locale_dataMap = (Map<String, String>) SessionUtil.getAttribute(model,
						SessionUtil.RESULT_LOCALE_DATA, null);
				for (String key : result_locale_dataMap.keySet()) {
					model.addAttribute(key, result_locale_dataMap.get(key));
				}
				target = "gold/gold_resale_result";
			} else {

				String cusidn = new String(
						Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
						"UTF-8");
				log.debug("cusidn={}", cusidn);
				okMap.put("CUSIDN", cusidn);
				okMap.put("UID", cusidn);

				// 寄件者信箱
				String DPMYEMAIL = (String) SessionUtil.getAttribute(model, SessionUtil.DPMYEMAIL, null);
				log.debug("DPMYEMAIL={}", DPMYEMAIL);
				okMap.put("DPMYEMAIL", DPMYEMAIL);

				// 使用者姓名，黃金單據列印頁會顯示
				String DPUSERNAME = (String) SessionUtil.getAttribute(model, SessionUtil.DPUSERNAME, null);
				log.debug("DPUSERNAME={}", DPUSERNAME);
				model.addAttribute("DPUSERNAME", DPUSERNAME);
				
                //IDgate驗證		 
                try {		 
                    if(okMap.get("FGTXWAY").equals("7")) {		 

        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N09002_IDGATE");
                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
                    }
                }catch(Exception e) {		 
                    log.error("IDGATE ValidateFail>>{}", e);		 
                }  
				Map<String,Object>IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
				model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));                

				BaseResult bs = gold_transaction_service.gold_resale_result(okMap, model);

				if (bs == null || !bs.getResult()) {
					model.addAttribute(BaseResult.ERROR, bs);
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, model);
					return target;
				}

				target = "gold/gold_resale_result";
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, model);
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_resale_result error >> {}",e);
		}
		return target;
	}
	/**
	 * N09102黃金預約回售選擇頁
	 */
	@RequestMapping(value = "/gold_reserve_resale_select")
	public String gold_reserve_resale_select(HttpServletRequest request,@RequestParam Map<String,String> reqParam,Model model){
		String target = "/error";
		
		log.debug("gold_reserve_resale_select");
		try{
			//如果是從KYC來會帶入多餘的交易機制參數,故移除
			reqParam.remove("jsondc");
			reqParam.remove("pkcs7Sign");
			// 解決Trust Boundary Violation 
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			
			String reqACN = okMap.get("ACN");
			log.debug(ESAPIUtil.vaildLog("reqACN={}"+reqACN));
			model.addAttribute("reqACN",reqACN);
			
			String cusidn = new String(Base64.getDecoder().decode((String)SessionUtil.getAttribute(model,SessionUtil.CUSIDN,null)),"UTF-8");
			log.debug(ESAPIUtil.vaildLog("cusidn={}"+cusidn));
			model.addAttribute("UID",cusidn);
			
			//先找出是否有GOLDAGREEFLAG的值
			String GOLDAGREEFLAG = reqParam.get("GOLDAGREEFLAG");
			log.debug(ESAPIUtil.vaildLog("GOLDAGREEFLAG={}"+GOLDAGREEFLAG));
			
			//如果是Y的話就存在SESSION
			if("Y".equals(GOLDAGREEFLAG)){
				SessionUtil.addAttribute(model,SessionUtil.GOLDAGREEFLAG,GOLDAGREEFLAG);
			}
			
			BaseResult n927BS = new BaseResult();
			n927BS = fund_transfer_service.getN927Data(cusidn);
			
			if(n927BS == null || !n927BS.getResult()){
				model.addAttribute(BaseResult.ERROR,n927BS);
				return target;
			}
			Map<String,Object> n927BSData = (Map<String,Object>)n927BS.getData();
			log.debug("n927BSData={}",n927BSData);
			
			String RISK = n927BSData.get("RISK") == null ? "" : (String)n927BSData.get("RISK");
			log.debug("RISK={}",RISK);
			
			//問卷填寫日期
			String GETLTD = n927BSData.get("GETLTD") == null ? "" : (String)n927BSData.get("GETLTD");
			log.debug("GETLTD={}",GETLTD);
			
			String time1 = "";
			String time2 = "";
			long day = 0;
			if(!GETLTD.equals("")){
				time1 = new SimpleDateFormat("yyyy/MM/dd").format(new Date());
				log.debug("time1={}",time1);
				String GETLTDString = Integer.valueOf(GETLTD.substring(0,3)) + 1911 + "";
				GETLTDString = GETLTDString + GETLTD.substring(3);
				log.debug("GETLTDString={}",GETLTDString);
				time2 = GETLTDString.substring(0,4) + "/" + GETLTDString.substring(4,6) + "/" + GETLTDString.substring(6,8);
				log.debug("time2={}",time2);
				
				//算出距離上次問卷填寫天數
				day = fund_transfer_service.getRangeDay(time1,time2);
			}
			
			String KYC = okMap.get("KYC");
			log.debug(ESAPIUtil.vaildLog("KYC={}"+KYC));
			
			if("PASS".equals(KYC)){
				SessionUtil.addAttribute(model,SessionUtil.KYC,KYC);
			}
			
			//找出是否有KYC PASS的值
			String KYCSession = (String)SessionUtil.getAttribute(model,SessionUtil.KYC,null);
			log.debug(ESAPIUtil.vaildLog("KYCSession={}"+KYCSession));
			
			//問卷版本日期
			String KYCDate = fund_transfer_service.getKYCDate();
			
			if(KYCDate == null){
				KYCDate = "";
			}
			
			if(KYCSession == null){
				if((RISK.equals("") || (!GETLTD.equals("") && day >= 365) || (!KYCDate.equals("") && Integer.parseInt(KYCDate) > Integer.parseInt(GETLTD)))){//自然人、法人版，無投資屬性或問卷填寫日期已滿一年或問卷版本日期大於問卷填寫日期	
				//假的
//				if(true){
					model.addAttribute("TXID","N09102");
					
					if(day >= 365){//滿一年
						model.addAttribute("ALERTTYPE","2");
					}
					else if(RISK.equals("")){//無投資屬性
					//假的
//					if(true){
						model.addAttribute("ALERTTYPE","1");
					}
					target = "gold/gd_inv_attr";
					return target;
		    	}
			}
			
			//取得黃金存摺網路交易帳號
			String type ="SELL";
			List<Map<String,String>> goldTradeAccountList = gold_transaction_service.getGoldTradeAccountList(cusidn,type);
			
			model.addAttribute("goldTradeAccountList",goldTradeAccountList);

            //IDGATE身分
            String idgateUserFlag="N";		 
            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
            try {		 
        	    if(IdgateData==null) {
        		    IdgateData = new HashMap<String, Object>();
        	    }
                BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
                if(tmp.getResult()) {		 
                    idgateUserFlag="Y";                  		 
                }		 
                tmp.addData("idgateUserFlag",idgateUserFlag);		 
                IdgateData.putAll((Map<String, String>) tmp.getData());
                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
            }catch(Exception e) {		 
                log.debug("idgateUserFlag error {}",e);		 
            }		 
            model.addAttribute("idgateUserFlag",idgateUserFlag);
			           
			target = "gold/gold_reserve_resale_select";
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_reserve_resale_select error >> {}",e);
		}
		return target;
	}
	/**
	 * N09102黃金預約回售選擇頁2
	 */
	@RequestMapping(value = "/gold_reserve_resale_select2")
	public String gold_reserve_resale_select2(HttpServletRequest request,@RequestParam Map<String,String> reqParam,Model model){
		String target = "/error";
		
		log.debug("gold_reserve_resale_select2");
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try{
			boolean hasLocale = okMap.containsKey("locale");

			if (hasLocale) {
				Map<String, String> step2_locale_dataMap = (Map<String, String>) SessionUtil.getAttribute(model,
						SessionUtil.STEP2_LOCALE_DATA, null);
				for (String key : step2_locale_dataMap.keySet()) {
					model.addAttribute(key, step2_locale_dataMap.get(key));
				}
				target = "gold/gold_reserve_resale_select2";

			} else {
				
				String FEEAMT1 = reqParam.get("FEEAMT1");
				log.debug(ESAPIUtil.vaildLog("FEEAMT1={}"+ FEEAMT1));
				String FEEAMT1Format = fund_transfer_service.formatNumberString(FEEAMT1, 0);
				log.debug(ESAPIUtil.vaildLog("FEEAMT1Format={}"+ FEEAMT1Format));
				okMap.put("FEEAMT1Format", FEEAMT1Format);

				model.addAttribute("requestParam", okMap);
				
				
				SessionUtil.addAttribute(model, SessionUtil.STEP2_LOCALE_DATA, model);
				target = "gold/gold_reserve_resale_select2";
				
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_reserve_resale_select2 error >> {}",e);
		}
		return target;
	}
	/**
	 * N09102 黃金預約回售確認頁
	 */
	@RequestMapping(value = "/gold_reserve_resale_confirm")
	public String gold_reserve_resale_confirm(HttpServletRequest request,@RequestParam Map<String,String> reqParam,Model model){
		String target = "/error";
		log.debug("gold_reserve_resale_confirm");
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try{
			boolean hasLocale = okMap.containsKey("locale");
			
			if (hasLocale) {
				Map<String, String> result_locale_dataMap = (Map<String, String>) SessionUtil.getAttribute(model,
						SessionUtil.CONFIRM_LOCALE_DATA, null);
				for (String key : result_locale_dataMap.keySet()) {
					model.addAttribute(key, result_locale_dataMap.get(key));
				}
				
			} else {
				String XMLCOD = (String) SessionUtil.getAttribute(model, SessionUtil.XMLCOD, null);
				log.debug("XMLCOD={}", XMLCOD);
				model.addAttribute("XMLCOD", XMLCOD);

				gold_transaction_service.gold_reserve_resale_confirm(okMap, model);
				
				
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, model);

				// IDGATE transdata            		 
	            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
	    		String adopid = "N09102";
	    		String title = "您有一筆預約黃金回售待確認";
	        	if(IdgateData != null) {
		            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
		            if(IdgateData.get("idgateUserFlag").equals("Y")) {
		            	okMap.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
		            	okMap.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
		            	okMap.put("TRANTITLE", "預約黃金回售");
		            	okMap.put("TRANNAME", "預約");
		            	okMap.put("TRNGD", okMap.get("TRNGDFormat"));
						IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N09101_IDGATE_DATA.class, N09102_IDGATE_DATA_VIEW.class, okMap));

		                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
		            }
	        	}
	            model.addAttribute("idgateAdopid", adopid);
	            model.addAttribute("idgateTitle", title);
	            
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_reserve_resale_confirm error >> {}",e);
		}finally {
			target = "gold/gold_reserve_resale_confirm";
		}
		return target;
	}
	/**
	 * N09102 黃金預約回售結果頁
	 */
	@RequestMapping(value = "/gold_reserve_resale_result")
	public String gold_reserve_resale_result(HttpServletRequest request,@RequestParam Map<String,String> requestParam,Model model){
		String target = "/error";
		
		log.debug("gold_reserve_resale_result");
		
		String jsondc= requestParam.get("jsondc");
		String pkcs7Sign = requestParam.get("pkcs7Sign");
		requestParam.remove("jsondc");
		requestParam.remove("pkcs7Sign");
		Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
		okMap.put("jsondc", jsondc);
		okMap.put("pkcs7Sign", pkcs7Sign);
		BaseResult bs = null;
		try{
			bs = new BaseResult();
			boolean hasLocale = okMap.containsKey("locale");

			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null));
				if (bs.getResult()) {
					Map<String, String> result_locale_dataMap = (Map<String, String>) SessionUtil.getAttribute(model,
							SessionUtil.RESULT_LOCALE_DATA, null);
					for (String key : result_locale_dataMap.keySet()) {
						model.addAttribute(key, result_locale_dataMap.get(key));
					}
					target = "gold/gold_reserve_resale_result";
					return target;
				}else {
					//讓bs繼續跑到錯誤頁
				}
			} else {
				String cusidn = new String(
						Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
						"UTF-8");
				log.debug("cusidn={}", cusidn);
				okMap.put("CUSIDN", cusidn);
				okMap.put("UID", cusidn);

				// 寄件者信箱
				String DPMYEMAIL = (String) SessionUtil.getAttribute(model, SessionUtil.DPMYEMAIL, null);
				log.debug("DPMYEMAIL={}", DPMYEMAIL);
				okMap.put("DPMYEMAIL", DPMYEMAIL);
				
                //IDgate驗證		 
                try {		 
                    if(okMap.get("FGTXWAY").equals("7")) {		 

        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N09102_IDGATE");
                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
                    }
                }catch(Exception e) {		 
                    log.error("IDGATE ValidateFail>>{}", e);		 
                }  
                
				Map<String,Object>IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
				model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));

				bs = gold_transaction_service.gold_reserve_resale_result(okMap, model);
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_reserve_resale_result error >> {}",e);
		}finally {
			if (bs.getResult()) {
				target = "gold/gold_reserve_resale_result";
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, model);
				
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
			
		}
		return target;
	}
	/**
	 * N09001黃金買進第一步
	 */
	@SuppressWarnings({"static-access","unchecked"})
	@RequestMapping(value = "/gold_buy")
	public String gold_buy(HttpServletRequest request,@RequestParam Map<String,String> reqParam,Model model){
		String target = "/error";
		log.debug("gold_buy");
		BaseResult bs = new BaseResult();
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try{
			//如果是從KYC來會帶入多餘的交易機制參數,故移除
			okMap.remove("jsondc");
			okMap.remove("pkcs7Sign");
			
			String reqACN = okMap.get("reqACN");
			log.debug(ESAPIUtil.vaildLog("reqACN={}"+reqACN));
			if(reqACN == null){
				reqACN = "";
			}
			log.debug(ESAPIUtil.vaildLog("reqACN"+ reqACN));
			model.addAttribute("reqACN",reqACN);
			
			String cusidn = new String(Base64.getDecoder().decode((String)SessionUtil.getAttribute(model,SessionUtil.CUSIDN,null)));
			log.debug(ESAPIUtil.vaildLog("cusidn={}"+cusidn));

			//檢查是否有黃金存摺帳號
			BaseResult bs920 = gold_transaction_service.N920_REST(cusidn,gold_transaction_service.GOLD_RESERVATION_QOERY_N920);
			log.debug("bs920={}",bs920);
			
			if(bs920 != null && bs920.getResult()){
				Map<String,Object> bs920Data = (Map<String,Object>)bs920.getData();
				
				//使用者名稱存SESSION，黃金單據列印頁會顯示
				String NAME = (String)bs920Data.get("NAME");
				log.debug("NAME={}",NAME);
				SessionUtil.addAttribute(model,SessionUtil.DPUSERNAME,NAME);
				
				List<Map<String,String>> rows = (List<Map<String,String>>)bs920Data.get("REC");
				//沒有黃金存摺帳號
				if(rows.isEmpty()){
				//假的
				//if(false){
				//if(true){
					BaseResult bsParm = new BaseResult();
					bsParm.addData("TXID", "gold_buy");
					bsParm.addData("MSGTYPE", "N920");
					SessionUtil.addAttribute(model,SessionUtil.STEP1_LOCALE_DATA,bsParm);
					log.trace("TXID>>gold_buy MSGTYPE>>N920");
					target = "forward:/GOLD/TRANSACTION/gold_applyconfirm";
					return target;
				}
			}
			
			//檢查是否有黃金存摺網路交易帳號
			BaseResult bs926 = gold_transaction_service.N926_REST(cusidn,gold_transaction_service.GOLD_RESERVATION_QOERY);
			if(bs926 != null && bs926.getResult()){
				Map<String,Object> bs926Data = (Map<String,Object>)bs926.getData();
				List<Map<String,String>> rows = (List<Map<String,String>>)bs926Data.get("REC");
			
				//沒有黃金存摺網路交易帳號
				if(rows.isEmpty()){
				//假的
				//if(false){
				//if(true){
					BaseResult bsParm = new BaseResult();
					bsParm.addData("TXID", "gold_buy");
					bsParm.addData("MSGTYPE", "N926");
					SessionUtil.addAttribute(model,SessionUtil.STEP1_LOCALE_DATA,bsParm);
					log.trace("TXID>>gold_buy MSGTYPE>>N926");
					target = "forward:/GOLD/TRANSACTION/gold_applyconfirm";
					return target;
				}
			}
			//判斷黃金存摺業務營業時間
			boolean isGoldTradeTime = gold_transaction_service.isGoldTradeTime();
			log.debug("isGoldTradeTime={}",isGoldTradeTime);
			
			//判斷黃金存摺是否可執行預約交易
			boolean isGoldScheduleTime = gold_transaction_service.isGoldScheduleTime();
			log.trace("isGoldScheduleTime={}",isGoldScheduleTime);
			
			//如果不能執行即時或預約交易的話，顯示錯誤
			if(isGoldTradeTime == false && isGoldScheduleTime == false){
				bs.setMessage("Z999",i18n.getMsg("LB.X1774"));
				model.addAttribute(BaseResult.ERROR,bs);
				return target;
			}
			
			//預約不判斷牌告
			if(!isGoldScheduleTime) {
				String twDate = DateUtil.getTWDate("");
				log.debug("twDate={}",twDate);
				
				List<GOLDPRICE> goldPriceList = goldPriceDao.getTODAYRecord(twDate);
				log.debug(ESAPIUtil.vaildLog("goldPriceList="+goldPriceList));
				
				if(goldPriceList.isEmpty()){
					//假的
					//if(false){
					bs.setMessage("Z410",i18n.getMsg("LB.X1775"));
					model.addAttribute(BaseResult.ERROR,bs);
					return target;
				}
			}
			
			String GOLDAGREEFLAG = (String)SessionUtil.getAttribute(model,SessionUtil.GOLDAGREEFLAG,null);
			log.debug(ESAPIUtil.vaildLog("GOLDAGREEFLAG={}"+GOLDAGREEFLAG));
			
			//檢查用戶是否已經同意約定條款
			if(GOLDAGREEFLAG != null){
				//黃金買進
		    	if(isGoldTradeTime == true){
				//假的
				//if(true){
		    		target = "forward:/GOLD/TRANSACTION/gold_buy_select?ADOPID=N09001&reqACN=" + reqACN;
		    	}
				//預約黃金買進
		    	else if(isGoldScheduleTime == true){
		    		// BEN
		    		target = "forward:/GOLD/TRANSACTION/gold_booking_select?ADOPID=N09101&reqACN=" + reqACN;
		    	}
			}
			else{
				String ADOPID = "";
				//黃金買進
		    	if(isGoldTradeTime == true){
				//假的
				//if(true){
		    		ADOPID = "N09001";
		    	}
		    	//預約黃金買進
		    	else if(isGoldScheduleTime == true){
		    		ADOPID = "N09101";
		    	}
		    	model.addAttribute("ADOPID",ADOPID);
		    	
				target = "gold/gold_term";
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_buy error >> {}",e);
		}
		return target;
	}
	/**
	 * N09001黃金交易缺東缺西頁
	 */
	@RequestMapping(value = "/gold_applyconfirm")
	public String gold_applyconfirm(HttpServletRequest request,@RequestParam Map<String,String> reqParam,Model model){
		String target = "/error";
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		log.debug("gold_applyconfirm");
		
		try{
			BaseResult bsParm = new BaseResult();
			bsParm = WebUtil.toBs(SessionUtil.getAttribute(model,SessionUtil.STEP1_LOCALE_DATA,null));
			Map<String, String> dataListMap = (Map<String, String>) bsParm.getData();
			String TXID =  dataListMap.get("TXID");
			String MSGTYPE = dataListMap.get("MSGTYPE");
			log.debug("MSGTYPE={}",MSGTYPE);
			
			String msgDesc = "";
			String txType = "";
			String trancode = "";
			
			if("N920".equals(MSGTYPE)){
				msgDesc = i18n.getMsg("LB.X1772");//您尚未申請黃金存摺帳戶，請洽往來分行辦理或線上立即申請。	
				//申請黃金存摺帳戶
				txType = "NA50";
				trancode = "GOLD/APPLY/gold_account_apply";
			}
			else if("N926".equals(MSGTYPE)){
				msgDesc = i18n.getMsg("LB.X1776");//您尚未申請黃金存摺網路交易，請洽往來分行辦理或線上立即申請本功能。
				//申請黃金存摺網路交易
				txType = "NA60";
				trancode = "GOLD/APPLY/gold_trading_apply";
			}
			else if("N093".equals(MSGTYPE)){
				msgDesc = i18n.getMsg("LB.X1773");//您尚未約定扣款帳號，請執行黃金定期定額申購。
				//黃金定期定額申購
				txType = "N09301";
				trancode = "GOLD/AVERAGING/averaging_purchase";		
			}
			okMap.put("msgDesc",msgDesc);
			okMap.put("txType",txType);
			okMap.put("trancode",trancode);
			
			log.debug("TXID={}",TXID);
			
			String title = "";
			if("gold_buy".equals(TXID)){
				//黃金買進
				title = "gold_buy";
			}
			else if("gold_resale".equals(TXID)){
				//黃金回售
				title = "gold_resale";
			}
			else if("averaging_purchase".equals(TXID)){
				//定期定額申購
				title = "averaging_purchase";
			}
			else if("averaging_alter".equals(TXID)) {
				title = "averaging_alter";
			}
			log.debug("title={}",title);
			
			okMap.put("title",title);
			
			model.addAttribute("reqParam",okMap);
			
			target = "gold/gold_applyconfirm";
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_applyconfirm error >> {}",e);
		}
		return target;
	}
	/**
	 * N09001黃金即時買進選擇頁
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/gold_buy_select")
	public String gold_buy_select(HttpServletRequest request,@RequestParam Map<String,String> reqParam,Model model){
		String target = "/error";
		
		log.debug("gold_buy_select");
		
		try{
			//若先經過KYC會將JSONDC及PKCS7SIGN帶入,用不到故移除
			reqParam.remove("jsondc");
			reqParam.remove("pkcs7Sign");
			// 解決Trust Boundary Violation 
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			//
			String cusidn = new String(Base64.getDecoder().decode((String)SessionUtil.getAttribute(model,SessionUtil.CUSIDN,null)),"UTF-8");
			log.debug("cusidn={}",cusidn);
			model.addAttribute("UID",cusidn);
			
			//先找出是否有GOLDAGREEFLAG的值
			String GOLDAGREEFLAG = okMap.get("GOLDAGREEFLAG");
			log.debug(ESAPIUtil.vaildLog("GOLDAGREEFLAG={}"+GOLDAGREEFLAG));
			
			//如果是Y的話就存在SESSION
			if("Y".equals(GOLDAGREEFLAG)){
				SessionUtil.addAttribute(model,SessionUtil.GOLDAGREEFLAG,GOLDAGREEFLAG);
			}
			BaseResult n927BS = new BaseResult();
			n927BS = fund_transfer_service.getN927Data(cusidn);
			
			if(n927BS == null || !n927BS.getResult()){
				model.addAttribute(BaseResult.ERROR,n927BS);
				return target;
			}
			Map<String,Object> n927BSData = (Map<String,Object>)n927BS.getData();
			log.debug("n927BSData={}",n927BSData);
			
			String RISK = n927BSData.get("RISK") == null ? "" : (String)n927BSData.get("RISK");
			log.debug("RISK={}",RISK);
			
			//問卷填寫日期
			String GETLTD = n927BSData.get("GETLTD") == null ? "" : (String)n927BSData.get("GETLTD");
			log.debug("GETLTD={}",GETLTD);
			
			String time1 = "";
			String time2 = "";
			long day = 0;
			if(!GETLTD.equals("")){
				time1 = new SimpleDateFormat("yyyy/MM/dd").format(new Date());
				log.debug("time1={}",time1);
				String GETLTDString = Integer.valueOf(GETLTD.substring(0,3)) + 1911 + "";
				GETLTDString = GETLTDString + GETLTD.substring(3);
				log.debug("GETLTDString={}",GETLTDString);
				time2 = GETLTDString.substring(0,4) + "/" + GETLTDString.substring(4,6) + "/" + GETLTDString.substring(6,8);
				log.debug("time2={}",time2);
				
				//算出距離上次問卷填寫天數
				day = fund_transfer_service.getRangeDay(time1,time2);
			}
			
			String KYC = okMap.get("KYC");
			log.debug(ESAPIUtil.vaildLog("KYC={}"+KYC));
			
			if("PASS".equals(KYC)){
				SessionUtil.addAttribute(model,SessionUtil.KYC,KYC);
			}
			
			//找出是否有KYC PASS的值
			String KYCSession = (String)SessionUtil.getAttribute(model,SessionUtil.KYC,null);
			log.debug(ESAPIUtil.vaildLog("KYCSession={}"+KYCSession));
			
			//問卷版本日期
			String KYCDate = fund_transfer_service.getKYCDate();
			
			if(KYCDate == null){
				KYCDate = "";
			}
			
			if(KYCSession == null){
				if((RISK.equals("") || (!GETLTD.equals("") && day >= 365) || (!KYCDate.equals("") && Integer.parseInt(KYCDate) > Integer.parseInt(GETLTD)))){//自然人、法人版，無投資屬性或問卷填寫日期已滿一年或問卷版本日期大於問卷填寫日期	
				//假的
//				if(true){
					model.addAttribute("TXID","N09001");
					
					if(day >= 365){//滿一年
						model.addAttribute("ALERTTYPE","2");
					}
					else if(RISK.equals("")){//無投資屬性
					//假的
//					if(true){
						model.addAttribute("ALERTTYPE","1");
					}
					target = "gold/gd_inv_attr";
					return target;
		    	}
			}
			
			//取得黃金存摺網路交易帳號(買進)
			String type = "BUY";
			List<Map<String,String>> goldTradeAccountList = gold_transaction_service.getGoldTradeAccountList(cusidn,type);
			
			model.addAttribute("goldTradeAccountList",goldTradeAccountList);
			
			//現在時間
			String nowDate = new SimpleDateFormat("yyyy/MM/dd").format(new Date());
			log.debug("nowDate={}",nowDate);
			model.addAttribute("nowDate",nowDate);

           //IDGATE身分
           String idgateUserFlag="N";		 
           Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
           try {		 
        	   if(IdgateData==null) {
        		   IdgateData = new HashMap<String, Object>();
        	   }
               BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
               if(tmp.getResult()) {		 
                   idgateUserFlag="Y";                  		 
               }		 
               tmp.addData("idgateUserFlag",idgateUserFlag);		 
               IdgateData.putAll((Map<String, String>) tmp.getData());
               SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
           }catch(Exception e) {		 
               log.debug("idgateUserFlag error {}",e);		 
           }		 
           model.addAttribute("idgateUserFlag",idgateUserFlag);
	           
		   target = "gold/gold_buy_select";
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_buy_select error >> {}",e);
		}
		return target;
	}
	/**
	 * N09001 黃金即時買進確認頁
	 */
	@RequestMapping(value = "/gold_buy_confirm")
	public String gold_buy_confirm(HttpServletRequest request,@RequestParam Map<String,String> reqParam,Model model){
		String target = "/error";
		BaseResult bs = null;
		log.debug("gold_buy_confirm");
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try{
			bs = new BaseResult();
			//新增TOKEN 避免重複交易
			bs = gold_transaction_service.getTxToken();
			if (bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN,((Map<String, String>) bs.getData()).get("TXTOKEN"));
			}
			bs.reset();
			
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null));
				if(bs.getResult()) {
					Map<String, String> confirm_locale_dataMap = (Map<String, String>) SessionUtil.getAttribute(model,
							SessionUtil.CONFIRM_LOCALE_DATA, null);
					for (String key : confirm_locale_dataMap.keySet()) {
						model.addAttribute(key, confirm_locale_dataMap.get(key));
					}
					target = "gold/gold_buy_confirm";
					return target;
				}else {
					//讓bs繼續跑到錯誤頁
				}
			} else {

				String cusidn = new String(
						Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
						"UTF-8");
				log.debug("cusidn={}", cusidn);
				reqParam.put("CUSIDN", cusidn);
				reqParam.put("UID", cusidn);

				String XMLCOD = (String) SessionUtil.getAttribute(model, SessionUtil.XMLCOD, null);
				log.debug("XMLCOD={}", XMLCOD);
				model.addAttribute("XMLCOD", XMLCOD);

				bs = gold_transaction_service.gold_buy_confirm(reqParam, model);
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
				
				// IDGATE transdata            		 
	            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
	    		String adopid = "N09001";
	    		String title = "您有一筆黃金申購待確認";
	    		Map<String, String> result = new HashMap<String, String>();
	    		Map<String, Object> bsData = (Map<String, Object>)bs.getData();
	    		for(String key : bsData.keySet()) {
	    			try {
	    				result.put(key, (String)bsData.get(key));
	    			}catch(Exception e){
	    			}
	    		}
	        	if(IdgateData != null) {
		            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
		            if(IdgateData.get("idgateUserFlag").equals("Y")) {
		            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
		            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
		            	result.put("TRANTITLE", "黃金申購");
		            	result.put("TRANNAME", "即時");
		            	result.put("TRNCOD", "02");
		            	String str_TRNFEE = (String)result.get("TRNFEE");
		    			String str_TRNAMT = (String)result.get("TRNAMT");
		    			String str_DISAMT = (String)result.get("DISAMT");						
		    			
		    			if ((str_TRNFEE.indexOf("+") != -1) || (str_TRNFEE.indexOf("-") != -1)) {
		    				result.put("TRNFEE_SIGN", str_TRNFEE.substring(0,1).equals("+") ? " " : "-");
		    				result.put("TRNFEE", str_TRNFEE.substring(1));				
		    			}
		    			if ((str_TRNAMT.indexOf("+") != -1) || (str_TRNAMT.indexOf("-") != -1)) {
		    				result.put("TRNAMT_SIGN", str_TRNAMT.substring(0,1).equals("+") ? " " : "-");
		    				result.put("TRNAMT", str_TRNAMT.substring(1));			
		    			}
		    			else {
		    				result.put("TRNAMT_SIGN", " ");			
		    			}
		    			if ((str_DISAMT.indexOf("+") != -1) || (str_DISAMT.indexOf("-") != -1)) {
		    				result.put("DISAMT_SIGN", str_DISAMT.substring(0,1).equals("+") ? " " : "-");
		    				result.put("DISAMT", str_DISAMT.substring(1));				
		    			}

		    			String str_PRICE = (String)result.get("PRICE");
		    			String str_DISPRICE = (String)result.get("DISPRICE");
		    			String str_PERDIS = (String)result.get("PERDIS");
		    			
		    			try {
		    				result.put("PRICE", NumericUtil.formatDouble(Double.parseDouble(str_PRICE) / 100, 2));
		    				result.put("DISPRICE", NumericUtil.formatDouble(Double.parseDouble(str_DISPRICE) / 100, 2));
		    				result.put("PERDIS", NumericUtil.formatDouble(Double.parseDouble(str_PERDIS) / 100, 2));				
		    			}
		    			catch (Exception e) {
		    				log.error("doAction error>>{}", e);
		    			}						
		    			
						IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N09001_IDGATE_DATA.class, N09001_IDGATE_DATA_VIEW.class, result));

		                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
		            }
	        	}
	            model.addAttribute("idgateAdopid", adopid);
	            model.addAttribute("idgateTitle", title);
	            
			}
			
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_buy_confirm error >> {}",e);
		}finally {
			if(bs.getResult()) {
				target = "gold/gold_buy_confirm";
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, model);
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	/**
	 * N09001 黃金即時買進結果頁
	 */
	@RequestMapping(value = "/gold_buy_result")
	public String gold_buy_result(HttpServletRequest request,@RequestParam Map<String,String> requestParam,Model model){
		String target = "/error";
		
		log.debug("gold_buy_result");
		String jsondc= requestParam.get("jsondc");
		String pkcs7Sign = requestParam.get("pkcs7Sign");
		requestParam.remove("jsondc");
		requestParam.remove("pkcs7Sign");
		Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
		okMap.put("jsondc", jsondc);
		okMap.put("pkcs7Sign", pkcs7Sign);
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			
			//1.要先驗證TOKEN的正確性
			if (StrUtil.isNotEmpty(okMap.get("TOKEN")) && okMap.get("TOKEN")
					.equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null))) {
				bs.setResult(Boolean.TRUE);
			}
			log.trace("bs.getResult() >>{}", bs.getResult());
			if (!bs.getResult()) {
				log.trace("throw new Exception()");
				throw new Exception();
			}
			bs.reset();
			// 2.要驗證token是否與完成交易的TOKEN重複
			if (StrUtil.isNotEmpty(okMap.get("TOKEN")) && !okMap.get("TOKEN")
					.equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null))) {
				bs.setResult(Boolean.TRUE);
			}
			if (!bs.getResult()) {
				log.error(ESAPIUtil
						.vaildLog("transfer_result TXTOKEN 不正確，或重複交易，TXTOKEN >> " + okMap.get("TXTOKEN")));
				throw new Exception();
			}
			bs.reset();
			
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null));
				if(bs.getResult()) {
					Map<String, String> result_locale_dataMap = (Map<String, String>) SessionUtil.getAttribute(model,
							SessionUtil.RESULT_LOCALE_DATA, null);
					for (String key : result_locale_dataMap.keySet()) {
						model.addAttribute(key, result_locale_dataMap.get(key));
					}
					target = "gold/gold_buy_result";
				}else {
					//讓bs繼續跑到錯誤頁
				}
				
			} else {
				String cusidn = new String(
						Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
						"UTF-8");
				log.debug("cusidn={}", cusidn);
				okMap.put("CUSIDN", cusidn);

				// 寄件者信箱
				String DPMYEMAIL = (String) SessionUtil.getAttribute(model, SessionUtil.DPMYEMAIL, null);
				log.debug("DPMYEMAIL={}", DPMYEMAIL);
				okMap.put("DPMYEMAIL", DPMYEMAIL);

				// KYC
				String KYC = (String) SessionUtil.getAttribute(model, SessionUtil.KYC, null);
				log.debug("KYC={}", KYC);
				model.addAttribute("KYC", KYC);

				// 使用者姓名，黃金單據列印頁會顯示
				String DPUSERNAME = (String) SessionUtil.getAttribute(model, SessionUtil.DPUSERNAME, null);
				log.debug("DPUSERNAME={}", DPUSERNAME);
				model.addAttribute("DPUSERNAME", DPUSERNAME);

                //IDgate驗證		 
                try {		 
                    if(okMap.get("FGTXWAY").equals("7")) {		 

        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N09001_IDGATE");
                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
                    }		 
                }catch(Exception e) {		 
                    log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
                }  
                
				Map<String,Object>IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
				model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
                
				bs = gold_transaction_service.gold_buy_result(okMap, model);
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_buy_result error >> {}",e);
		}finally {
			if(bs.getResult()) {
				// 交易成功要存之前的token 在Session 以利下次比對是否重複交易
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TOKEN"));
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, model);
				target = "gold/gold_buy_result";
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * N09101黃金預約買進選擇頁
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/gold_booking_select")
	public String gold_buy_select_booking(HttpServletRequest request,@RequestParam Map<String,String> reqParam,Model model){
		String target = "/error";
		BaseResult bs = null;
		log.debug("gold_booking_select");
		try{
			
			String cusidn = new String(Base64.getDecoder().decode((String)SessionUtil.getAttribute(model,SessionUtil.CUSIDN,null)),"UTF-8");
			log.debug("cusidn={}",cusidn);
			model.addAttribute("UID",cusidn);
			
			//先找出是否有GOLDAGREEFLAG的值
			String GOLDAGREEFLAG = reqParam.get("GOLDAGREEFLAG");
			log.debug(ESAPIUtil.vaildLog("GOLDAGREEFLAG={}"+GOLDAGREEFLAG));
			
			//如果是Y的話就存在SESSION
			if("Y".equals(GOLDAGREEFLAG)){
				SessionUtil.addAttribute(model,SessionUtil.GOLDAGREEFLAG,GOLDAGREEFLAG);
			}
			BaseResult n927BS = new BaseResult();
			n927BS = fund_transfer_service.getN927Data(cusidn);
			
			if(n927BS == null || !n927BS.getResult()){
				model.addAttribute(BaseResult.ERROR,n927BS);
				return target;
			}
			Map<String,Object> n927BSData = (Map<String,Object>)n927BS.getData();
			log.debug("n927BSData={}",n927BSData);
			
			String RISK = n927BSData.get("RISK") == null ? "" : (String)n927BSData.get("RISK");
			log.debug("RISK={}",RISK);
			
			//問卷填寫日期
			String GETLTD = n927BSData.get("GETLTD") == null ? "" : (String)n927BSData.get("GETLTD");
			log.debug("GETLTD={}",GETLTD);
			
			String time1 = "";
			String time2 = "";
			long day = 0;
			if(!GETLTD.equals("")){
				time1 = new SimpleDateFormat("yyyy/MM/dd").format(new Date());
				log.debug("time1={}",time1);
				String GETLTDString = Integer.valueOf(GETLTD.substring(0,3)) + 1911 + "";
				GETLTDString = GETLTDString + GETLTD.substring(3);
				log.debug("GETLTDString={}",GETLTDString);
				time2 = GETLTDString.substring(0,4) + "/" + GETLTDString.substring(4,6) + "/" + GETLTDString.substring(6,8);
				log.debug("time2={}",time2);
				
				//算出距離上次問卷填寫天數
				day = fund_transfer_service.getRangeDay(time1,time2);
			}
			
			String KYC = reqParam.get("KYC");
			log.debug(ESAPIUtil.vaildLog("KYC={}"+KYC));
			
			if("PASS".equals(KYC)){
				SessionUtil.addAttribute(model,SessionUtil.KYC,KYC);
			}
			
			//找出是否有KYC PASS的值
			String KYCSession = (String)SessionUtil.getAttribute(model,SessionUtil.KYC,null);
			log.debug("KYCSession={}",KYCSession);
			
			//問卷版本日期
			String KYCDate = fund_transfer_service.getKYCDate();
			
			if(KYCDate == null){
				KYCDate = "";
			}
			
			if(KYCSession == null){
				if((RISK.equals("") || (!GETLTD.equals("") && day >= 365) || (!KYCDate.equals("") && Integer.parseInt(KYCDate) > Integer.parseInt(GETLTD)))){//自然人、法人版，無投資屬性或問卷填寫日期已滿一年或問卷版本日期大於問卷填寫日期	
				//假的
//				if(true){
					model.addAttribute("TXID","N09101");
					
					if(day >= 365){//滿一年
						model.addAttribute("ALERTTYPE","2");
					}
					else if(RISK.equals("")){//無投資屬性
					//假的
//					if(true){
						model.addAttribute("ALERTTYPE","1");
					}
					target = "gold/gd_inv_attr";
					return target;
		    	}
			}
			
			//取得黃金存摺網路交易帳號
			String type = "BUY";
			List<Map<String,String>> goldTradeAccountList = gold_transaction_service.getGoldTradeAccountList(cusidn,type);
			
			model.addAttribute("goldTradeAccountList",goldTradeAccountList);
			
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			
			//如果是從KYC來會帶入多餘的交易機制參數,故移除
			reqParam.remove("jsondc");
			reqParam.remove("pkcs7Sign");
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);

			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = gold_transaction_service.getTxToken();
			log.trace("gold_booking_select.TXTOKEN: " + ((Map<String, String>) bs.getData()).get("TXTOKEN"));
			if (bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN,
						((Map<String, String>) bs.getData()).get("TXTOKEN"));
			}

			// session有無CUSIDN
//			if (!"".equals(cusidn) && cusidn != null) {
//				// 解密成明碼
//				cusidn = new String(Base64.getDecoder().decode(cusidn));
//				log.trace("cusidn: " + cusidn);
//
//			} else {
//				log.error("session no cusidn!!!");
//			}

           //IDGATE身分
           String idgateUserFlag="N";		 
           Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
           try {		 
        	   if(IdgateData==null) {
        		   IdgateData = new HashMap<String, Object>();
        	   }
               BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
               if(tmp.getResult()) {		 
                   idgateUserFlag="Y";                  		 
               }		 
               tmp.addData("idgateUserFlag",idgateUserFlag);		 
               IdgateData.putAll((Map<String, String>) tmp.getData());
               SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
           }catch(Exception e) {		 
               log.debug("idgateUserFlag error {}",e);		 
           }		 
           model.addAttribute("idgateUserFlag",idgateUserFlag);
		           
			
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_buy_select_booking error >> {}",e);
		}finally {
			if(bs!=null && bs.getResult()) {
				target = "/gold/gold_booking_select";
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
				
			}
		}
		return target;
	}
	/**
	 * N09001 黃金預約買進確認頁
	 */
	@RequestMapping(value = "/gold_booking_confirm")
	public String gold_booking_confirm(HttpServletRequest request,@RequestParam Map<String,String> reqParam,Model model){
//		String target = "/error";
//		BaseResult bs = null;
//		log.debug("gold_booking_confirm");
//		try{
//			String cusidn = new String(Base64.getDecoder().decode((String)SessionUtil.getAttribute(model,SessionUtil.CUSIDN,null)),"UTF-8");
//			log.debug("cusidn={}",cusidn);
//			reqParam.put("CUSIDN",cusidn);
//			reqParam.put("UID",cusidn);
//			
//			String XMLCOD = (String)SessionUtil.getAttribute(model,SessionUtil.XMLCOD,null);
//			log.debug("XMLCOD={}",XMLCOD);
//			model.addAttribute("XMLCOD",XMLCOD);
//			
//			bs = gold_transaction_service.gold_buy_confirm(reqParam,model);
//			
//			if(bs == null || !bs.getResult()){
//				model.addAttribute(BaseResult.ERROR,bs);
//				return target;
//			}
//			
//			target = "/gold/gold_booking_confirm";
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			log.error("",e);
//		}
//		return target;
		
		String target = "/error";
		String jsondc = "";
		BaseResult bs = new BaseResult();
		
		log.trace(ESAPIUtil.vaildLog("gold_booking_confirm.reqParam: {}"+ CodeUtil.toJson(reqParam)));
		
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		
		try {
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("gold_booking_confirm.jsondc: " + jsondc));
			
			okMap.put("jsondc", jsondc); // IKEY要用不經過ESAPIUtil，會出錯
			log.trace(ESAPIUtil.vaildLog("gold_booking_confirm.okMap: {}"+ CodeUtil.toJson(okMap)));
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale: {}"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				if (!bs.getResult()) {
					// TODO 交易結束後，CONFIRM_LOCALE_DATA 會被清空，造成在URL輸入確認頁會導向錯誤頁，原因待查
					log.trace("bs.getResult(): {}", bs.getResult());
					throw new Exception();
				} else {
					// session 資料放入 map 讓後續 model 和回上一頁取值
					okMap.putAll((Map<String, String>) bs.getData());
				}
			} else {
				// 驗證TOKEN 沒TOKEN會到錯誤頁，錯誤頁確認後返回輸入頁
				String token = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null);
				log.trace("gold_booking_confirm.token: {}", token);
				if (StrUtil.isNotEmpty(okMap.get("TOKEN")) && okMap.get("TOKEN").equals(token)) {
					bs.setResult(Boolean.TRUE);
				}
				log.trace("bs.getResult(): {}", bs.getResult());
				if (!bs.getResult()) {
					log.trace("throw new Exception()");
					throw new Exception();
				}

				// TOKEN驗證成功，資料處理後進入確認頁
				bs.reset();
				String cusidn = new String(
						Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
						"utf-8");
				bs = gold_transaction_service.gold_booking_confirm(okMap);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
				

				// IDGATE transdata            		 
	            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
	    		String adopid = "N09101";
	    		String title = "您有一筆預約黃金申購待確認";
	    		Map<String, String> result = new HashMap<String, String>();
	    		Map<String, Object> bsData = (Map<String, Object>)bs.getData();
	    		for(String key : bsData.keySet()) {
	    			try {
	    				result.put(key, (String)bsData.get(key));
	    			}catch(Exception e){
	    			}
	    		}
	        	if(IdgateData != null) {
		            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
		            if(IdgateData.get("idgateUserFlag").equals("Y")) {
		            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
		            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
		            	result.put("TRANTITLE", "預約黃金申購");
		            	result.put("TRANNAME", "預約");
						IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N09101_IDGATE_DATA.class, N09101_IDGATE_DATA_VIEW.class, result));

		                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
		            }
	        	}
	            model.addAttribute("idgateAdopid", adopid);
	            model.addAttribute("idgateTitle", title);
	            
			}

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_booking_confirm error >> {}",e);
		} finally {
			if (bs.getResult()) {
				target = "/gold/gold_booking_confirm";
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN,
						((Map<String, Object>) bs.getData()).get("TXTOKEN"));

			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	/**
	 * N09101 黃金預約買進結果頁
	 */
	@RequestMapping(value = "/gold_booking_result")
	public String gold_booking_result(HttpServletRequest request,@RequestParam Map<String,String> reqParam,Model model){
		String target = "/error";
		
		log.debug("gold_booking_result");
//		try{
//			String cusidn = new String(Base64.getDecoder().decode((String)SessionUtil.getAttribute(model,SessionUtil.CUSIDN,null)),"UTF-8");
//			log.debug("cusidn={}",cusidn);
//			requestParam.put("CUSIDN",cusidn);
//			
//			//寄件者信箱
//			String DPMYEMAIL = (String)SessionUtil.getAttribute(model,SessionUtil.DPMYEMAIL,null);
//			log.debug("DPMYEMAIL={}",DPMYEMAIL);
//			requestParam.put("DPMYEMAIL",DPMYEMAIL);
//			
//			//KYC
//			String KYC = (String)SessionUtil.getAttribute(model,SessionUtil.KYC,null);
//			log.debug("KYC={}",KYC);
//			model.addAttribute("KYC",KYC);
//			
//			//使用者姓名，黃金單據列印頁會顯示
//			String DPUSERNAME = (String)SessionUtil.getAttribute(model,SessionUtil.DPUSERNAME,null);
//			log.debug("DPUSERNAME={}",DPUSERNAME);
//			model.addAttribute("DPUSERNAME",DPUSERNAME);
//			
//			BaseResult bs = gold_transaction_service.gold_buy_result(requestParam,model);
//			
//			if(bs == null || !bs.getResult()){
//				model.addAttribute(BaseResult.ERROR,bs);
//				return target;
//			}
//			
//			target = "gold/gold_booking_result";
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			log.error("",e);
//		}
//		return target;
		
		// 解決Trust Boundary Violation
		String jsondc= reqParam.get("jsondc");
		String pkcs7Sign = reqParam.get("pkcs7Sign");
		reqParam.remove("jsondc");
		reqParam.remove("pkcs7Sign");
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		okMap.put("jsondc", jsondc);
		okMap.put("pkcs7Sign", pkcs7Sign);
		
		BaseResult bs = null;
		try {
			// 初始化BaseResult
			bs = new BaseResult();
			
			// 取得CUSIDN明碼
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			okMap.put("CUSIDN", cusidn);
		
			
			// 判斷LOCAL KEY是否有帶值，有帶表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			log.trace("gold_booking_result.locale: " + hasLocale);
			if(hasLocale){
				log.trace("gold_booking_result.hasLocale...");
				// 判斷從session取出的資料物件是否為BaseResult物件
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( !bs.getResult() ){
					log.trace("gold_booking_result.bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			// 判斷LOCAL KEY沒有帶值則驗證TXTOKEN，驗證通過才可做交易
			if(!hasLocale){
				log.trace("gold_booking_result.validate TXTOKEN...");
				log.trace("gold_booking_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("gold_booking_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("gold_booking_result.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("gold_booking_result TXTOKEN 不正確，TXTOKEN>>{}"+ okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						!okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("gold_booking_result.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("gold_booking_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				
				
				// 取得從輸入頁輸入存在session中的輸入資料
				bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
				Map<String,String> transfer_data = (Map<String, String>) bs.getData();
				
				// 取得SESSION中的輸入資料，加入到確認頁的交易機制資料
				okMap.putAll(transfer_data);
				
				// 重置bs，進行交易
				bs.reset();
				
                //IDgate驗證		 
                try {		 
                    if(okMap.get("FGTXWAY").equals("7")) {		 

        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N09101_IDGATE");
                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
                    }		 
                }catch(Exception e) {		 
                    log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
                }  
                
				Map<String,Object>IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
				model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
                
				bs = gold_transaction_service.gold_booking_result(okMap);
				
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_booking_result error >> {}",e);
		} finally {
			
			// 交易成功要存之前的token 在Session 以利下次比對是否重複交易
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TXTOKEN"));
			if(bs.getResult()) {
				target = "/gold/gold_booking_result";
				model.addAttribute("gold_booking_result_data", bs);
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	/**
	 * 用台幣轉出帳號取得黃金轉入帳號的AJAX
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	@RequestMapping(value="/getGoldTradeTWAccountListAjax",produces={"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult getGoldTradeTWAccountListAjax(@RequestParam Map<String,String> requestParam,Model model){
		log.debug("IN getGoldTradeTWAccountListAjax");
		
		BaseResult baseResult = new BaseResult();
		baseResult.setResult(Boolean.FALSE);
		
		try{
			String thisSVACN = requestParam.get("SVACN");
			log.debug(ESAPIUtil.vaildLog("thisSVACN={}"+thisSVACN));
			
			String cusidn = new String(Base64.getDecoder().decode((String)SessionUtil.getAttribute(model,SessionUtil.CUSIDN,null)),"UTF-8");
			log.debug("cusidn={}",cusidn);
			
			BaseResult bs926 = gold_transaction_service.N926_REST(cusidn,gold_transaction_service.GOLD_RESERVATION_QOERY);
			if(bs926 != null && bs926.getResult()){
				Map<String,Object> bs926Data = (Map<String,Object>)bs926.getData();
				List<Map<String,String>> rows = (List<Map<String,String>>)bs926Data.get("REC");
			
				//有黃金存摺網路交易帳號
				if(!rows.isEmpty()){
					List<Map<String,String>> dataListMap = new ArrayList<Map<String,String>>();
					
					for(int x=0;x<rows.size();x++){
						String SVACN = rows.get(x).get("SVACN");
						log.debug("SVACN={}",SVACN);
						
						if(thisSVACN.equals(SVACN)){
							Map<String,String> map = new HashMap<String,String>();
							
							String ACN = rows.get(x).get("ACN");
							log.debug("ACN={}",ACN);
							
							map.put("ACN",ACN);
							dataListMap.add(map);
						}
					}
					log.debug("dataListMap={}",dataListMap);
					
					baseResult.setResult(Boolean.TRUE);
					String dataString = new Gson().toJson(dataListMap,dataListMap.getClass());
					log.debug("dataString={}",dataString);
					
					baseResult.setData(dataString);
				}
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getGoldTradeTWAccountListAjax error >> {}",e);
		}
		return baseResult;
	}
	/**
	 * 用黃金轉出帳號取得台幣轉入帳號的AJAX
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	@RequestMapping(value="/getGoldTradeTWAccountListAjax2",produces={"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult getGoldTradeTWAccountListAjax2(@RequestParam Map<String,String> requestParam,Model model){
		log.debug("IN getGoldTradeTWAccountListAjax2");
		
		BaseResult baseResult = new BaseResult();
		baseResult.setResult(Boolean.FALSE);
		Map<String,String> resultMap = new HashMap<String,String>();
		
		try{
			String ACN = requestParam.get("ACN");
			log.debug(ESAPIUtil.vaildLog("ACN={}"+ACN));
			
			String cusidn = new String(Base64.getDecoder().decode((String)SessionUtil.getAttribute(model,SessionUtil.CUSIDN,null)),"UTF-8");
			log.debug("cusidn={}",cusidn);
			requestParam.put("CUSIDN",cusidn);
			
			BaseResult bs190 = gold_passbook_service.N190_REST(requestParam);
			
			if(bs190 != null && bs190.getResult()){
				Map<String,Object> bs190Data = (Map<String,Object>)bs190.getData();
				List<Map<String,String>> rows = (List<Map<String,String>>)bs190Data.get("REC");
			
				//取得黃金存摺帳號可用餘額
				for(int i=0;i<rows.size();i++){
					String innerACN = rows.get(i).get("ACN");
					log.debug("innerACN={}",innerACN);
					if(ACN.equals(innerACN)){
						resultMap.put("TSFBAL",rows.get(i).get("TSFBAL"));
						resultMap.put("GDBAL",rows.get(i).get("GDBAL"));
						resultMap.put("FEEAMT1",rows.get(i).get("FEEAMT1").substring(1));
						resultMap.put("FEEAMT2",rows.get(i).get("FEEAMT2").substring(1));
			
						break;
					}
				}
				
				BaseResult bs926 = gold_transaction_service.N926_REST(cusidn,gold_transaction_service.GOLD_RESERVATION_QOERY);
				if(bs926 != null && bs926.getResult()){
					Map<String,Object> bs926Data = (Map<String,Object>)bs926.getData();
					List<Map<String,String>> rows926 = (List<Map<String,String>>)bs926Data.get("REC");
				
					for(int i=0;i<rows926.size();i++){
						String innerACN = rows926.get(i).get("ACN");
						log.debug("innerACN={}",innerACN);
						if(ACN.equals(innerACN)){
							resultMap.put("SVACN",rows926.get(i).get("SVACN"));
							
							break;
						}
					}
					log.debug("resultMap={}",resultMap);
					
					baseResult.setResult(Boolean.TRUE);
					String dataString = new Gson().toJson(resultMap,resultMap.getClass());
					log.debug("dataString={}",dataString);
					
					baseResult.setData(dataString);
				}
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getGoldTradeTWAccountListAjax2 error >> {}",e);
		}
		return baseResult;
	}
	
	/**
	 * 繳納定期扣款失敗手續費 IDGATE
	 */
	@RequestMapping(value="/n094_idgate_set_value",produces={"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult n094_idgate_set_value(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String,String> requestParam,Model model){
		log.debug("IN n094_idgate_set_value");
		BaseResult baseResult = new BaseResult();
		baseResult.setResult(Boolean.FALSE);
		try{
			String TRNAMT = requestParam.get("TRNAMT");
			log.debug(ESAPIUtil.vaildLog("TRNAMT={}"+TRNAMT));
			
			String cusidn = new String(Base64.getDecoder().decode((String)SessionUtil.getAttribute(model,SessionUtil.CUSIDN,null)),"UTF-8");
			log.debug("cusidn={}",cusidn);
			requestParam.put("CUSIDN",cusidn);
			
			// IDGATE transdata            		 
            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
        	if(IdgateData != null) {
	            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
	            if(IdgateData.get("idgateUserFlag").equals("Y")) {
	            	requestParam.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
	            	requestParam.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
	            	
	            	requestParam.put("CUSIDN", cusidn);
	            	requestParam.put("ACN", requestParam.get("ACN"));
	            	requestParam.put("SVACN", requestParam.get("SVACN"));
	            	requestParam.put("TRNAMT", requestParam.get("TRNAMT"));
	            	
	            	log.debug("IDGATE VALUE >> {}" , requestParam);
	            	IdgateData.put(requestParam.get("ADOPID") + "_IDGATE", idgateservice.coverTxnData(N094_IDGATE_DATA.class, N094_IDGATE_DATA_VIEW.class, requestParam));
					 SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);
	            }
        	}
            baseResult.setResult(true);
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("n094_idgate_set_value error >> {}",e);
			baseResult.setResult(false);
		}
		
		return baseResult;
	}
	
}