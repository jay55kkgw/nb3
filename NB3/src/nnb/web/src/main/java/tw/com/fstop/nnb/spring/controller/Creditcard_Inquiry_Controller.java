package tw.com.fstop.nnb.spring.controller;

import java.util.Base64;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.custom.annotation.ISTXNLOG;
import tw.com.fstop.nnb.service.Creditcard_Inquiry_Service;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.web.util.WebUtil;

@SessionAttributes({ SessionUtil.PD, SessionUtil.DPMYEMAIL, SessionUtil.DOWNLOAD_DATALISTMAP_DATA,
		SessionUtil.BACK_DATA, SessionUtil.PRINT_DATALISTMAP_DATA, SessionUtil.RESULT_LOCALE_DATA,
		SessionUtil.CONFIRM_LOCALE_DATA, SessionUtil.CUSIDN, SessionUtil.DPMYEMAIL })
@Controller
@RequestMapping(value = "/CREDIT/INQUIRY")
public class Creditcard_Inquiry_Controller {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private Creditcard_Inquiry_Service creditcard_inquiry_service;

	// 信用卡總覽_查詢頁
	@RequestMapping(value = "/card_overview")
	public String overview(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("creditcard_controller_overview...");
		String target = "/error";
		// 處理結果
		BaseResult bs = null;

		try {
			// 查詢結果走RestService
			// Base64解碼
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");
			log.trace("card_overview_cusidn >>{} ", cusidn);
			reqParam.put("CUSIDN", cusidn);

			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			bs = creditcard_inquiry_service.overview_rest(okMap);

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("overview error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/creditcard/overview";
				model.addAttribute("card_overview", bs);
				List<Map<String, String>> dataListMap = ((List<Map<String, String>>) ((Map<String, Object>) bs
						.getData()).get("REC"));
				log.debug("dataListMap >> {}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
			} else {
				String previous = "/INDEX/index";
				bs.setPrevious(previous);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		log.trace("target >> {}", target);
		return target;
	}

	// 信用卡歷史帳單明細查詢_輸入頁
	@ISTXNLOG(value="N820")
	@RequestMapping(value = "/card_history")
	public String history(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("creditcard_controller_history...");
		String target = "/error";
		// 處理結果
		BaseResult bs = null;

		// 清除切換語系時暫存的資料
		SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
		SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
		try {
			// portal物件
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");

			reqParam.put("CUSIDN", cusidn);
			bs = creditcard_inquiry_service.history_p1(reqParam);

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("history error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/creditcard/history";
				bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
				model.addAttribute("card_history", bs);
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		log.trace("target >> {}", target);
		return target;
	}

//	// 信用卡歷史帳單明細查詢_查詢頁
//	@RequestMapping(value = "/card_history_step1")
//	public String history_step1(HttpServletRequest request, HttpServletResponse response,
//			@RequestParam Map<String, String> reqParam, Model model) {
//		log.trace("creditcard_controller_history_step1...");
//		log.trace("history_step1.reqParam >>{} ", reqParam);
//
//		String target = "/error";
//		BaseResult bs = null;
//		Map<String, String> okMap = null;
//		try {
//			if ("Y".equals(reqParam.get("back"))) {
//				reqParam = (Map<String, String>) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
//			}
//			// 解決Trust Boundary Violation
//			okMap = ESAPIUtil.validStrMap(reqParam);
//			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
//			boolean hasLocale = okMap.containsKey("locale");
//			if (hasLocale) {
//				log.trace("locale>>" + okMap.get("locale"));
//				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
//				if (!bs.getResult()) {
//					// TODO 交易結束後，CONFIRM_LOCALE_DATA 會被清空，造成在URL輸入確認頁會導向錯誤頁，原因待查
//					log.trace("bs.getResult() >>{}", bs.getResult());
//					throw new Exception();
//				} else {
//					// session 資料放入 map 讓後續 model 和回上一頁取值
//					okMap.putAll((Map<String, String>) bs.getData());
//				}
//			} else {
//				// 信用卡歷史帳單明細查詢
//				bs = creditcard_inquiry_service.history_step1(okMap);
//				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//			log.error("{}", e);
//		} finally {
//			if (bs != null && bs.getResult()) {
//
//				target = "/creditcard/history_step1";
//				model.addAttribute("card_history_step1", bs);
//				SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, okMap);
//			} else {
//				model.addAttribute(BaseResult.ERROR, bs);
//			}
//		}
//		log.trace("target >>{}", target);
//		return target;
//	}

	// 信用卡歷史帳單明細查詢_線上查詢帳單
	@RequestMapping(value = "/card_history_online")
	public String history_online(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("creditcard_controller_history_online...");

		String target = "/error";
		String previous = "/CREDIT/INQUIRY/card_history";
		BaseResult bs = null;
		String fgperiod = "";
		String way = "";

		try {
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");
			log.trace("card_history_cusidn >>{}", cusidn);
			reqParam.put("CUSIDN", cusidn);
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			} else {
				// 信用卡帳單明細查詢
				log.trace(ESAPIUtil.vaildLog("card_history_online_reqParam >>{}" +  CodeUtil.toJson(okMap)));
				bs = creditcard_inquiry_service.history_query(okMap);
				if(null!=okMap.get("FUNCTION")&&!"".equals(okMap.get("FUNCTION"))) {
					bs.addData("FUNCTION", okMap.get("FUNCTION"));
				}
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("history_online error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/creditcard/history_online";
				model.addAttribute("history_online", bs);
				List<Map<String, String>> dataListMap = ((List<Map<String, String>>) ((Map<String, Object>) bs
						.getData()).get("_Table"));
				log.debug("dataListMap >> {}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
			} else {
				bs.setPrevious(previous);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		log.trace("target >>{} ", target);
		return target;
	}

	@RequestMapping(value = "/card_history_online_aj", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody BaseResult card_history_online_aj(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String cusidn;
		BaseResult bs = null;
		try {
			cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");
			reqParam.put("CUSIDN", cusidn);
			bs = new BaseResult();
			bs = creditcard_inquiry_service.history_query(reqParam);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("card_history_online_aj error >> {}",e);
		}
		return bs;
	}

	// 信用卡歷史帳單明細查詢_EMAIL帳單
	@RequestMapping(value = "/card_history_email")
	public String history_email(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("creditcard_controller_history_email...");

		String target = "/error";
		BaseResult bs = null;
		try {
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");
			reqParam.put("CUSIDN", cusidn);
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			} else {
				// 信用卡歷史帳單明細查詢
				log.trace(ESAPIUtil.vaildLog("card_history_email_reqParam >>{}" + CodeUtil.toJson(okMap)));
				bs = creditcard_inquiry_service.history_query(okMap);
				bs.addData("CUSIDN", cusidn);
				bs.addData("CUSIDNLength", cusidn.length());
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("history_email error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/creditcard/history_email";
				model.addAttribute("history_email", bs);
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		log.trace("target >> {}", target);
		return target;
	}

	// 信用卡歷史帳單明細查詢_線上查詢繳款單
	@RequestMapping(value = "/card_history_onlinePay")
	public String history_onlinePay(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("creditcard_controller_history_onlinePay...");

		String target = "/error";
		BaseResult bs = null;
		try {
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");
			reqParam.put("CUSIDN", cusidn);
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);

			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			} else {
				// 信用卡歷史帳單明細查詢
				bs = creditcard_inquiry_service.history_query(okMap);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("history_onlinePay error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/creditcard/history_onlinePay";
				model.addAttribute("history_onlinePay", bs);
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		log.trace("target >>{} ", target);
		return target;
	}

	/**
	 * 信用卡未出帳單明細查詢_輸入頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/card_unbilled")
	public String unbilled_rest(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("creditcard_controller_unbilled...");

		String target = "/error";
		// 處理結果

		BaseResult bs = null;
		try {
			log.trace("card_unbilled_Controller...");
			
				
			// 清除切換語系時暫存的資料
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
				bs = new BaseResult();
				// 登入時的身分證字號
				String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
				// session有無CUSIDN
				if (!"".equals(cusidn) && null != cusidn) {
					// 解密成明碼
					cusidn = new String(Base64.getDecoder().decode(cusidn));
					log.trace("cusidn: " + cusidn);
					reqParam.put("CUSIDN", cusidn);
					// 解決Trust Boundary Violation
					Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
					
					bs = creditcard_inquiry_service.card_unbilled_p1(okMap);

				} else {
					log.error("session no cusidn!!!");
				}

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("unbilled_rest error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/creditcard/unbilled";
				model.addAttribute("card_unbilled", bs);
				// model.addAttribute("card_unbilled_n813", bs1);
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		log.trace("target >>{} ", target);
		return target;
	}

	/**
	 *  信用卡未出帳單明細查詢_結果頁 EAI WEBSERVICE路線
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/card_unbilled_result")
	public String unbilled_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("creditcard_controller_unbilled_result...");

		String target = "/error";
		String previous = "/CREDIT/INQUIRY/card_unbilled";
		// 處理結果
		BaseResult bs = null;
		String systime = "";
		// int totcnt = 0;
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {
			bs = new BaseResult();
			
			
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");
			okMap.put("CUSIDN", cusidn);
			log.trace(ESAPIUtil.vaildLog("card_unbilled_result_reqParam >>{} " + CodeUtil.toJson(okMap)));

			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			Boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			} else {
				bs = creditcard_inquiry_service.unbilled_result_rest(okMap);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("unbilled_result error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/creditcard/unbilled_result_rest";

				List<Map<String,String>> dataListMap = ((List<Map<String,String>>)((Map<String,Object>)bs.getData()).get("REC"));
				log.debug("dataListMap >> {}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
				model.addAttribute("card_unbilled_result", bs);
				log.trace("card_unbilled_result >>{}",bs );
				
			} else {
				bs.setPrevious(previous);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		log.trace("target >>{}", target);
		return target;
	}
	
		/**
		 *  信用卡未出帳單明細查詢_EMAIL頁 EAI WEBSERVICE路線
		 * @param request
		 * @param response
		 * @param reqParam
		 * @param model
		 * @return
		 */
		@RequestMapping(value = "/card_unbilled_email")
		public String unbilled_email(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
			log.trace("creditcard_controller_card_unbilled_email...");

			String target = "/error";
			String previous = "/CREDIT/INQUIRY/card_unbilled";
			// 處理結果
			BaseResult bs = null;
			Map<String, String> okMap = null;
			try {
				
				// portal物件
				// String sessionId = (String) SessionUtil.getAttribute(model,
				// log.trace("card_unbilled_result_sessionId >>{}", sessionId);
				String cusidn = new String(
						Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
						"utf-8");
				reqParam.put("CUSIDN", cusidn);
				log.trace(ESAPIUtil.vaildLog("card_unbilled_email_result_reqParam >>{} " +  CodeUtil.toJson(reqParam)));

				// 解決Trust Boundary Violation
				okMap = ESAPIUtil.validStrMap(reqParam);
				// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
				boolean hasLocale = okMap.containsKey("locale");
				if (hasLocale) {
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				} else {
					bs = creditcard_inquiry_service.get_Result_String(okMap);
					
					if(!bs.getResult()) {
						bs.setPrevious(previous);
						model.addAttribute(BaseResult.ERROR, bs);
						return target;
					}
					
					
					okMap.put("RESULT", ((Map<String,String>)bs.getData()).get("htmlString"));
					bs.reset();
					bs = creditcard_inquiry_service.send_mail(okMap);
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				}

			} catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("unbilled_email error >> {}",e);
			} finally {
				if (bs != null && bs.getResult()) {
					target = "/creditcard/unbilled_mail_result";

					model.addAttribute("card_unbilled_mail_result", bs);
					
				} else {
					bs.setPrevious(previous);
					model.addAttribute(BaseResult.ERROR, bs);
				}
			}
			log.trace("target >>{}", target);
			return target;
		}

	/**
	 * TD02 信用卡繳款紀錄查詢
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/card_paid_history")
	public String card_paid_history(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("card_paid_history_Controller...");
		String target = "/error";
		BaseResult bs = null;

		try {
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			bs = new BaseResult();
			// 登入時的身分證字號
			String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if (!"".equals(cusidn) && null != cusidn) {
				// 解密成明碼
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				log.trace("cusidn: " + cusidn);
				reqParam.put("CUSIDN", cusidn);
				reqParam.put("CF", "TD02");
				// 解決Trust Boundary Violation
				Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
				bs = creditcard_inquiry_service.card_paid_history_p1_info(okMap);

			} else {
				log.error("session no cusidn!!!");
			}

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("card_paid_history error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/creditcard/card_paid_history";
				model.addAttribute("card_paid_history", bs);
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * TD02 信用卡繳款紀錄查詢 結果頁
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/card_paid_history_result")
	public String card_paid_history_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("card_paid_history_result_Controller...");
		String target = "/error";
		String previous = "/CREDIT/INQUIRY/card_paid_history";
		BaseResult bs = null;
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		
		try {
			bs = new BaseResult();
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult()) {
					// TODO 交易結束後，CONFIRM_LOCALE_DATA 會被清空，造成在URL輸入確認頁會導向錯誤頁，原因待查
					log.trace("bs.getResult(): {}", bs.getResult());
					throw new Exception();
				} else {
					// session 資料放入 map 讓後續 model 和回上一頁取值
					okMap.putAll((Map<String, String>) bs.getData());
				}
			} else {
				// 登入時的身分證字號
				String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
				// session有無CUSIDN
				if (!"".equals(cusidn) && cusidn != null) {
					// 解密成明碼
					cusidn = new String(Base64.getDecoder().decode(cusidn));
					log.trace("cusidn: " + cusidn);
					okMap.put("CUSIDN", cusidn);
					okMap.put("UID", cusidn);

					bs = creditcard_inquiry_service.card_paid_history_result(okMap);
					
					// 將查過的資料放入session，待切換 locale 時讀取
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
					
				} else {
					log.error("session no cusidn!!!");
				}
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("card_paid_history_result error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/creditcard/card_paid_history_result";
				model.addAttribute("card_paid_history_result", bs);
				Map<String, Object> bsData = (Map) bs.getData();
				List<Map<String, String>> rows = (List<Map<String, String>>) bsData.get("REC");

				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, rows);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, rows);
			} else {
				bs.setPrevious(previous);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	
	
	
	
}
