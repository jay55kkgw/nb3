package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * InvAttribS電文RS
 */
public class InvAttrib_S_REST_RS extends BaseRestBean implements Serializable{
	private static final long serialVersionUID = 6791228050721094802L;
	
	private String MSGCOD;
	private String DATA;
	private String TYPE;
	private String CUTTYPE;//客戶性質別
	private String SALARY;//年收入
	private String BRTHDY;//出生年月日
	private String ACN4;
	private String ACN2;
	private String EMPNO;
	private String ACN3;
	private String ICCOD;
	private String ACN1;
	private String GETLTD;//問卷填寫日期
	private String MARK3;
	private String MARK1;//重大傷病證明
	private String FDINVTYPE;//投資屬性
	private String RISK;//風險
	private String RISK7;
	private String GETLTD7;
	private String APLBRH;// 網銀申請行
	private String NAME;
	private String CAREER;//職業
	private String DEGREE;//學歷
	
	//20210813新增 , 由電文回來的KYC分數
	private String SCORE;
	
	public String getMSGCOD(){
		return MSGCOD;
	}
	public void setMSGCOD(String mSGCOD){
		MSGCOD = mSGCOD;
	}
	public String getDATA(){
		return DATA;
	}
	public void setDATA(String dATA){
		DATA = dATA;
	}
	public String getTYPE(){
		return TYPE;
	}
	public void setTYPE(String tYPE){
		TYPE = tYPE;
	}
	public String getCUTTYPE(){
		return CUTTYPE;
	}
	public void setCUTTYPE(String cUTTYPE){
		CUTTYPE = cUTTYPE;
	}
	public String getSALARY(){
		return SALARY;
	}
	public void setSALARY(String sALARY){
		SALARY = sALARY;
	}
	public String getBRTHDY(){
		return BRTHDY;
	}
	public void setBRTHDY(String bRTHDY){
		BRTHDY = bRTHDY;
	}
	public String getACN4(){
		return ACN4;
	}
	public void setACN4(String aCN4){
		ACN4 = aCN4;
	}
	public String getACN2(){
		return ACN2;
	}
	public void setACN2(String aCN2){
		ACN2 = aCN2;
	}
	public String getEMPNO(){
		return EMPNO;
	}
	public void setEMPNO(String eMPNO){
		EMPNO = eMPNO;
	}
	public String getACN3(){
		return ACN3;
	}
	public void setACN3(String aCN3){
		ACN3 = aCN3;
	}
	public String getICCOD(){
		return ICCOD;
	}
	public void setICCOD(String iCCOD){
		ICCOD = iCCOD;
	}
	public String getACN1(){
		return ACN1;
	}
	public void setACN1(String aCN1){
		ACN1 = aCN1;
	}
	public String getGETLTD(){
		return GETLTD;
	}
	public void setGETLTD(String gETLTD){
		GETLTD = gETLTD;
	}
	public String getMARK3(){
		return MARK3;
	}
	public void setMARK3(String mARK3){
		MARK3 = mARK3;
	}
	public String getMARK1(){
		return MARK1;
	}
	public void setMARK1(String mARK1){
		MARK1 = mARK1;
	}
	public String getFDINVTYPE(){
		return FDINVTYPE;
	}
	public void setFDINVTYPE(String fDINVTYPE){
		FDINVTYPE = fDINVTYPE;
	}
	public String getRISK(){
		return RISK;
	}
	public void setRISK(String rISK){
		RISK = rISK;
	}
	public String getRISK7(){
		return RISK7;
	}
	public void setRISK7(String rISK7){
		RISK7 = rISK7;
	}
	public String getGETLTD7(){
		return GETLTD7;
	}
	public void setGETLTD7(String gETLTD7){
		GETLTD7 = gETLTD7;
	}
	public String getAPLBRH(){
		return APLBRH;
	}
	public void setAPLBRH(String aPLBRH){
		APLBRH = aPLBRH;
	}
	public String getNAME(){
		return NAME;
	}
	public void setNAME(String nAME){
		NAME = nAME;
	}
	public String getCAREER(){
		return CAREER;
	}
	public void setCAREER(String cAREER){
		CAREER = cAREER;
	}
	public String getDEGREE(){
		return DEGREE;
	}
	public void setDEGREE(String dEGREE){
		DEGREE = dEGREE;
	}
	public String getSCORE() {
		return SCORE;
	}
	public void setSCORE(String sCORE) {
		SCORE = sCORE;
	}
}