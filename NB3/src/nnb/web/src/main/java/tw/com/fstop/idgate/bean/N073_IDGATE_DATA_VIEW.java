package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;


public class N073_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5370773529503299761L;
	
	// 轉出帳號
	@SerializedName(value = "OUTACN")
	private String OUTACN;
	
	// 期貨帳號
	@SerializedName(value = "DPAGACNO_TEXT")
	private String DPAGACNO_TEXT;
	
	//轉帳金額 (需加上新臺幣..元)
	@SerializedName(value = "IDGATE_AMOUNT")
	private String IDGATE_AMOUNT;
	
	
	
	
	@Override
	public Map<String, String> coverMap(){
		Map<String, String> result = new LinkedHashMap<String, String>();
		result.put("交易名稱", "繳費-期貨保證金 ");
		result.put("交易類別", "即時");
		result.put("轉出帳號", this.OUTACN);
		result.put("期貨帳號", this.DPAGACNO_TEXT);
		result.put("轉帳金額", "新臺幣"+this.IDGATE_AMOUNT+"元");
		return result;
	}




	public String getOUTACN() {
		return OUTACN;
	}




	public void setOUTACN(String oUTACN) {
		OUTACN = oUTACN;
	}




	public String getDPAGACNO_TEXT() {
		return DPAGACNO_TEXT;
	}




	public void setDPAGACNO_TEXT(String dPAGACNO_TEXT) {
		DPAGACNO_TEXT = dPAGACNO_TEXT;
	}




	public String getIDGATE_AMOUNT() {
		return IDGATE_AMOUNT;
	}




	public void setIDGATE_AMOUNT(String iDGATE_AMOUNT) {
		IDGATE_AMOUNT = iDGATE_AMOUNT;
	}


}
