package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N104_REST_RQ extends BaseRestBean_OLA implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8878284952570279918L;
	private String CUSIDN;
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
}
