package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class N177_IDGATE_DATA implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5562556378398396786L;
	/**
	 * 
	 */

	@SerializedName(value = "FYACN")
	private String FYACN;//存單帳號
	@SerializedName(value = "FDPNUM")
	private String FDPNUM;//存單號碼
//	@SerializedName(value = "CRY") //在java會有mapping因此對應不到
//	private String CRY;//幣別 
	@SerializedName(value = "AMTFDP")
	private String AMTFDP;//存單金額 
	@SerializedName(value = "INTMTH")
	private String INTMTH;
	@SerializedName(value = "CODE")
	private String CODE;
	@SerializedName(value = "FYTSFAN")
	private String FYTSFAN;//轉入帳號
	@SerializedName(value = "FGTXDATE")
	private String FGTXDATE; // 即時或預約
	public String getFYACN() {
		return FYACN;
	}
	public void setFYACN(String fYACN) {
		FYACN = fYACN;
	}
	public String getFDPNUM() {
		return FDPNUM;
	}
	public void setFDPNUM(String fDPNUM) {
		FDPNUM = fDPNUM;
	}
//	public String getCRY() {
//		return CRY;
//	}
//	public void setCRY(String cRY) {
//		CRY = cRY;
//	}
	public String getAMTFDP() {
		return AMTFDP;
	}
	public void setAMTFDP(String aMTFDP) {
		AMTFDP = aMTFDP;
	}
	public String getINTMTH() {
		return INTMTH;
	}
	public void setINTMTH(String iNTMTH) {
		INTMTH = iNTMTH;
	}
	public String getCODE() {
		return CODE;
	}
	public void setCODE(String cODE) {
		CODE = cODE;
	}
	public String getFYTSFAN() {
		return FYTSFAN;
	}
	public void setFYTSFAN(String fYTSFAN) {
		FYTSFAN = fYTSFAN;
	}
	public String getFGTXDATE() {
		return FGTXDATE;
	}
	public void setFGTXDATE(String fGTXDATE) {
		FGTXDATE = fGTXDATE;
	}
	
}
