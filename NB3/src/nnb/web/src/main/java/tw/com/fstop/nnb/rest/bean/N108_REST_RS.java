package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N108_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1874898449756769229L;
	
	private String FDPACN;
	private String CMTXTIME;

	public String getFDPACN() {
		return FDPACN;
	}

	public void setFDPACN(String fDPACN) {
		FDPACN = fDPACN;
	}

	public String getCMTXTIME() {
		return CMTXTIME;
	}

	public void setCMTXTIME(String cMTXTIME) {
		CMTXTIME = cMTXTIME;
	}	  
}
