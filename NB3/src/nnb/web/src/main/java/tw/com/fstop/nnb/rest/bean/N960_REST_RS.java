package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N960_REST_RS extends BaseRestBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4976574500269140110L;
	/**
	 * 
	 */
	
	private String OFFSET;
	private String HEADER;
	private String MSGCOD;
	private String NAME;
	private String BRTHDY;
	private String POSTCOD1;
	private String PMTADR;
	private String POSTCOD2;
	private String CTTADR;
	private String ARACOD;
	private String TELNUM;
	private String ARACOD2;
	private String TELNUM2;
	private String MOBTEL;
	private String ARACOD3;
	private String FAX;
	private String COUNTRY;
	private String CONSENT;
	private String VERNUM;
	private String BRHCOD;
	private String REPIDN;
	private String CUSNAME;
	private String MAILADDR;
	
	public String getOFFSET() {
		return OFFSET;
	}
	public void setOFFSET(String oFFSET) {
		OFFSET = oFFSET;
	}
	public String getHEADER() {
		return HEADER;
	}
	public void setHEADER(String hEADER) {
		HEADER = hEADER;
	}
	public String getMSGCOD() {
		return MSGCOD;
	}
	public void setMSGCOD(String mSGCOD) {
		MSGCOD = mSGCOD;
	}
	public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	public String getBRTHDY() {
		return BRTHDY;
	}
	public void setBRTHDY(String bRTHDY) {
		BRTHDY = bRTHDY;
	}
	public String getPOSTCOD1() {
		return POSTCOD1;
	}
	public void setPOSTCOD1(String pOSTCOD1) {
		POSTCOD1 = pOSTCOD1;
	}
	public String getPMTADR() {
		return PMTADR;
	}
	public void setPMTADR(String pMTADR) {
		PMTADR = pMTADR;
	}
	public String getPOSTCOD2() {
		return POSTCOD2;
	}
	public void setPOSTCOD2(String pOSTCOD2) {
		POSTCOD2 = pOSTCOD2;
	}
	public String getCTTADR() {
		return CTTADR;
	}
	public void setCTTADR(String cTTADR) {
		CTTADR = cTTADR;
	}
	public String getARACOD() {
		return ARACOD;
	}
	public void setARACOD(String aRACOD) {
		ARACOD = aRACOD;
	}
	public String getTELNUM() {
		return TELNUM;
	}
	public void setTELNUM(String tELNUM) {
		TELNUM = tELNUM;
	}
	public String getARACOD2() {
		return ARACOD2;
	}
	public void setARACOD2(String aRACOD2) {
		ARACOD2 = aRACOD2;
	}
	public String getTELNUM2() {
		return TELNUM2;
	}
	public void setTELNUM2(String tELNUM2) {
		TELNUM2 = tELNUM2;
	}
	public String getMOBTEL() {
		return MOBTEL;
	}
	public void setMOBTEL(String mOBTEL) {
		MOBTEL = mOBTEL;
	}
	public String getARACOD3() {
		return ARACOD3;
	}
	public void setARACOD3(String aRACOD3) {
		ARACOD3 = aRACOD3;
	}
	public String getFAX() {
		return FAX;
	}
	public void setFAX(String fAX) {
		FAX = fAX;
	}
	public String getCOUNTRY() {
		return COUNTRY;
	}
	public void setCOUNTRY(String cOUNTRY) {
		COUNTRY = cOUNTRY;
	}
	public String getCONSENT() {
		return CONSENT;
	}
	public void setCONSENT(String cONSENT) {
		CONSENT = cONSENT;
	}
	public String getVERNUM() {
		return VERNUM;
	}
	public void setVERNUM(String vERNUM) {
		VERNUM = vERNUM;
	}
	public String getBRHCOD() {
		return BRHCOD;
	}
	public void setBRHCOD(String bRHCOD) {
		BRHCOD = bRHCOD;
	}
	public String getREPIDN() {
		return REPIDN;
	}
	public String getCUSNAME() {
		return CUSNAME;
	}
	public void setREPIDN(String rEPIDN) {
		REPIDN = rEPIDN;
	}
	public void setCUSNAME(String cUSNAME) {
		CUSNAME = cUSNAME;
	}
	public String getMAILADDR() {
		return MAILADDR;
	}
	public void setMAILADDR(String mAILADDR) {
		MAILADDR = mAILADDR;
	}
}
