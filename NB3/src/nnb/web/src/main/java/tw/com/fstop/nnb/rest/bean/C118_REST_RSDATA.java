package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;


/**
 * 
 * 功能說明 :SMART FUND自動贖回設定 回應 DATTA
 *
 */
public class C118_REST_RSDATA implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7359369179014285508L;

	private String CDNO;// 信託號碼

	private String PAYTAG;// 扣款標的

	private String FUNDAMT;// 信託金額

	private String FUNDCUR;// 贖回信託金額幣別

	private String ACUCOUNT;// 單位數

	private String REFVALUE1;// 參考淨值

	private String FXRATE;// 參考匯率

	private String AMT;// 參考現值

	private String NAMT;// 未分配金額

	private String RTNC;// 基準報酬率正負

	private String RTNRATE;// 基準報酬率

	private String STOPLOSS;// 原自動贖回停損設定

	private String STOPPROF;// 原自動贖回停利設定

	private String AC202;// 信託型態

	private String MIP;// 定期不定額註記

	public String getPAYTAG()
	{
		return PAYTAG;
	}

	public void setPAYTAG(String pAYTAG)
	{
		PAYTAG = pAYTAG;
	}

	public String getFUNDAMT()
	{
		return FUNDAMT;
	}

	public void setFUNDAMT(String fUNDAMT)
	{
		FUNDAMT = fUNDAMT;
	}

	public String getFUNDCUR()
	{
		return FUNDCUR;
	}

	public void setFUNDCUR(String fUNDCUR)
	{
		FUNDCUR = fUNDCUR;
	}

	public String getACUCOUNT()
	{
		return ACUCOUNT;
	}

	public void setACUCOUNT(String aCUCOUNT)
	{
		ACUCOUNT = aCUCOUNT;
	}

	public String getREFVALUE1()
	{
		return REFVALUE1;
	}

	public void setREFVALUE1(String rEFVALUE1)
	{
		REFVALUE1 = rEFVALUE1;
	}

	public String getFXRATE()
	{
		return FXRATE;
	}

	public void setFXRATE(String fXRATE)
	{
		FXRATE = fXRATE;
	}

	public String getAMT()
	{
		return AMT;
	}

	public void setAMT(String aMT)
	{
		AMT = aMT;
	}

	public String getNAMT()
	{
		return NAMT;
	}

	public void setNAMT(String nAMT)
	{
		NAMT = nAMT;
	}

	public String getRTNC()
	{
		return RTNC;
	}

	public void setRTNC(String rTNC)
	{
		RTNC = rTNC;
	}

	public String getRTNRATE()
	{
		return RTNRATE;
	}

	public void setRTNRATE(String rTNRATE)
	{
		RTNRATE = rTNRATE;
	}

	public String getSTOPLOSS()
	{
		return STOPLOSS;
	}

	public void setSTOPLOSS(String sTOPLOSS)
	{
		STOPLOSS = sTOPLOSS;
	}

	public String getSTOPPROF()
	{
		return STOPPROF;
	}

	public void setSTOPPROF(String sTOPPROF)
	{
		STOPPROF = sTOPPROF;
	}

	public String getAC202()
	{
		return AC202;
	}

	public void setAC202(String aC202)
	{
		AC202 = aC202;
	}

	public String getMIP()
	{
		return MIP;
	}

	public void setMIP(String mIP)
	{
		MIP = mIP;
	}

	public String getCDNO() {
		return CDNO;
	}

	public void setCDNO(String cDNO) {
		CDNO = cDNO;
	}
	

}
