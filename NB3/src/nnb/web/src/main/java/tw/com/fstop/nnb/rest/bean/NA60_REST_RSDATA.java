package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class NA60_REST_RSDATA  extends BaseRestBean implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2460999618855115199L;

	private String GDACN;
	private String SVACN;
	
	public String getGDACN() {
		return GDACN;
	}
	public void setGDACN(String gDACN) {
		GDACN = gDACN;
	}
	public String getSVACN() {
		return SVACN;
	}
	public void setSVACN(String sVACN) {
		SVACN = sVACN;
	}
}
