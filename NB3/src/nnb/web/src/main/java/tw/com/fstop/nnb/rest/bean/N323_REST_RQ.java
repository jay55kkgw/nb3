package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N323_REST_RQ extends BaseRestBean_FUND implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3212481384784720315L;
	
	String UID;
	String PINNEW;
	String DEGREE;
	String MARK1;
	String TYPE;
	String MARK3;
	String RISK7;
	String AGE;
	String FGTXWAY;
	
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	public String getPINNEW() {
		return PINNEW;
	}
	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}
	public String getDEGREE() {
		return DEGREE;
	}
	public void setDEGREE(String dEGREE) {
		DEGREE = dEGREE;
	}
	public String getMARK1() {
		return MARK1;
	}
	public void setMARK1(String mARK1) {
		MARK1 = mARK1;
	}
	public String getTYPE() {
		return TYPE;
	}
	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}
	public String getMARK3() {
		return MARK3;
	}
	public void setMARK3(String mARK3) {
		MARK3 = mARK3;
	}
	public String getRISK7() {
		return RISK7;
	}
	public void setRISK7(String rISK7) {
		RISK7 = rISK7;
	}
	public String getAGE() {
		return AGE;
	}
	public void setAGE(String aGE) {
		AGE = aGE;
	}
}
