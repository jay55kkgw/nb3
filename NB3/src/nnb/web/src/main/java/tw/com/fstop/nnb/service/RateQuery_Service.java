package tw.com.fstop.nnb.service;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.tbb.nnb.dao.Nb3SysOpDao;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.SpringBeanFactory;

@Service
public class RateQuery_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	Nb3SysOpDao nb3SysOpDao;

	/**
	 * Rate Query N021
	 * 
	 * @param custid
	 * @param
	 * @return
	 */
	public BaseResult query_n021(Map<String, String> reqParam) {
		log.trace("Rate Query N021 Service...");
		// 處理結果
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = RateQuery_REST(reqParam);
			dataProcessN021(bs);
		} catch (Exception e) {
			log.error("Rate Query N021 Service error >> {}", e);
		}
		// 回傳處理結果
		return bs;
	}

	/**
	 * Rate Query N022
	 * 
	 * @param custid
	 * @param
	 * @return
	 */
	public BaseResult query_n022(Map<String, String> reqParam) {
		log.trace("Rate Query N022 Service...");
		// 處理結果
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = RateQuery_REST(reqParam);
			colorMapping(bs);
			bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
		} catch (Exception e) {
			log.error("Rate Query N022 Service error >> {}", e);
		}
		// 回傳處理結果
		return bs;
	}

	/**
	 * Rate Query N023
	 * 
	 * @param custid
	 * @param
	 * @return
	 */
	public BaseResult query_n023(Map<String, String> reqParam) {
		log.trace("Rate Query N023 Service...");
		// 處理結果
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = RateQuery_REST(reqParam);
			colorMapping(bs);
			bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
		} catch (Exception e) {
			log.error("Rate Query N023 Service error >> {}", e);
		}
		// 回傳處理結果
		return bs;
	}

	/**
	 * Rate Query N024
	 * 
	 * @param custid
	 * @param
	 * @return
	 */
	public BaseResult query_n024(Map<String, String> reqParam) {
		log.trace("Rate Query N024 Service...");
		// 處理結果
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = RateQuery_REST(reqParam);
			colorMapping(bs);
			bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("Rate Query N024 Service error >> {}", e);
		}
		// 回傳處理結果
		return bs;
	}

	/**
	 * Rate Query N025
	 * 
	 * @param custid
	 * @param
	 * @return
	 */
	public BaseResult query_n025(Map<String, String> reqParam) {
		log.trace("Rate Query N025 Service...");
		// 處理結果
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = RateQuery_REST(reqParam);
			colorMapping(bs);
			bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("Rate Query N025 Service error >> {}", e);
		}
		// 回傳處理結果
		return bs;
	}

	/**
	 * Rate Query N026
	 * 
	 * @param custid
	 * @param
	 * @return
	 */
	public BaseResult query_n026(Map<String, String> reqParam) {
		log.trace("Rate Query N026 Service...");
		// 處理結果
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = RateQuery_REST(reqParam);
			colorMapping(bs);
		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("Rate Query N026 Service error >> {}", e);
		}
		// 回傳處理結果
		return bs;
	}

	/**
	 * Rate Query N027
	 * 
	 * @param custid
	 * @param
	 * @return
	 */
	public BaseResult query_n027(Map<String, String> reqParam) {
		log.trace("Rate Query N027 Service...");
		// 處理結果
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = RateQuery_REST(reqParam);
			dataProcessN027(bs);
			bs.addData("JSPTITLE", nb3SysOpDao.findAdopid(reqParam.get("QUERYTYPE")));
		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("Rate Query N027 Service error >> {}", e);
		}
		// 回傳處理結果
		return bs;
	}

	/**
	 * Rate Query N030
	 * 
	 * @param custid
	 * @param
	 * @return
	 */
	public BaseResult query_n030(Map<String, String> reqParam) {
		log.trace("Rate Query N030 Service...");
		// 處理結果
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = RateQuery_REST(reqParam);
			dataProcessN030(bs);
			bs.addData("CMQTIME", DateUtil.getTWDate("/") + " " + DateUtil.getTheTime(":").substring(0, 5));
		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("Rate Query N030 Service error >> {}", e);
		}
		// 回傳處理結果
		return bs;
	}

	public void colorMapping(BaseResult bs) {
		List<Map<String, String>> dataListMap = ((List<Map<String, String>>) ((Map<String, Object>) bs.getData())
				.get("REC"));
		bs.addData("RECSIZE", dataListMap.size());
		if (dataListMap.size() > 0) {
			for (Map<String, String> eachrow : dataListMap) {
				eachrow.put("COLOR", getBeanMapValue("RateQuery", "N021COLOR" + eachrow.get("COLOR")));
			}
		}
	}

	public void dataProcessN027(BaseResult bs) {
		List<Map<String, String>> dataListMap = ((List<Map<String, String>>) ((Map<String, Object>) bs.getData())
				.get("REC"));
		bs.addData("RECSIZE", dataListMap.size());
		if (dataListMap.size() > 0) {
			for (Map<String, String> eachrow : dataListMap) {
				eachrow.put("TERM", getBeanMapValue("RateQuery", "N027TERM" + eachrow.get("TERM")));
				int title1 = Integer.parseInt(eachrow.get("TITLE1"));
				int title2 = Integer.parseInt(eachrow.get("TITLE2"));
				int title3 = Integer.parseInt(eachrow.get("TITLE3"));
				int intamt = 0;
				float famt = 0;
				String amt1 = "";
				String amt2 = "";
				String amt3 = "";
				if (Float.parseFloat(String.valueOf(title1)) % 100 > 0) {
					famt = Float.parseFloat(String.valueOf(title1)) / 100;
					amt1 = "" + famt;
				} else {
					intamt = Integer.parseInt(String.valueOf(title1)) / 100;
					amt1 = "" + intamt;
				}
				if (Float.parseFloat(String.valueOf(title2)) % 100 > 0) {
					famt = Float.parseFloat(String.valueOf(title2)) / 100;
					amt2 = "" + famt;
				} else {
					intamt = Integer.parseInt(String.valueOf(title2)) / 100;
					amt2 = "" + intamt;
				}
				if (Float.parseFloat(String.valueOf(title3)) % 100 > 0) {
					famt = Float.parseFloat(String.valueOf(title3)) / 100;
					amt3 = "" + famt;
				} else {
					intamt = Integer.parseInt(String.valueOf(title3)) / 100;
					amt3 = "" + intamt;
				}

				eachrow.put("amt1", amt1);
				eachrow.put("amt2", amt2);
				eachrow.put("amt3", amt3);
			}
		}
	}

	public void dataProcessN030(BaseResult bs) {
		List<Map<String, String>> dataListMap = ((List<Map<String, String>>) ((Map<String, Object>) bs.getData())
				.get("REC"));
		bs.addData("RECSIZE", dataListMap.size());
		if (dataListMap.size() > 0) {
			for (Map<String, String> eachrow : dataListMap) {
				eachrow.put("TERM", getBeanMapValue("RateQuery", "N030TERM" + eachrow.get("TERM")));
				eachrow.put("CRYText", getBeanMapValue("RateQuery", "N030CRY" + eachrow.get("CRY")));
				eachrow.put("BUYITR", NumericUtil.formatNumberString(eachrow.get("BUYITR"), 4));
				eachrow.put("SELITR", NumericUtil.formatNumberString(eachrow.get("SELITR"), 4));
			}
		}
	}

	public void dataProcessN021(BaseResult bs) {
		boolean isAAColor = false;
		String yyyyDay = "";
		String yyymmdd = "";
		List<Map<String, String>> dataListMap = ((List<Map<String, String>>) ((Map<String, Object>) bs.getData())
				.get("REC"));
		List<Map<String, String>> dataListMapNormal = new LinkedList<Map<String, String>>();
		List<Map<String, String>> dataListMapAA = new LinkedList<Map<String, String>>();
		List<Map<String, String>> dataListMapAB = new LinkedList<Map<String, String>>();

		LinkedList listAA = new LinkedList<>();
		LinkedList<LinkedList<String>> lListAB = new LinkedList<>();

		// 資料分組
		if (dataListMap.size() > 0) {
			for (Map<String, String> each : dataListMap) {
				if ("AA".equals(each.get("COLOR"))) {
					dataListMapAA.add(each);
				} else if ("AB".equals(each.get("COLOR"))) {
					dataListMapAB.add(each);
				} else {
					each.put("COLOR", getBeanMapValue("RateQuery", "N021COLOR" + each.get("COLOR")));
					dataListMapNormal.add(each);
				}
			}
		}
		// AA處理 to List
		if (dataListMapAA.size() > 0) {
			for (Map<String, String> eachAA : dataListMapAA) {
				isAAColor = true;
				int aaCountTime = 10;
				String getVal = "";
				int intamt = 0;
				float famt = 0;
				String amt = "";
				for (int inaa = 0; inaa < aaCountTime; inaa++) {
					int qInt = inaa + 1;
					getVal = eachAA.get("ITR" + qInt);
					if (null == getVal || getVal.equals("") || getVal.equals("000000")) {
						break;
					}
					// if(null == getVal || getVal.equals("")) break;
					log.trace("getVal >> {} ", getVal);
					if (inaa == 0) {
						int yyyy = Integer.parseInt(getVal.substring(0, 3)) + 1911;
						String mm = getVal.substring(3, 5);
						String dd = getVal.substring(5, 7);
						yyyyDay = "" + yyyy + "/" + mm + "/" + dd;
						yyymmdd = getVal;
					} else {
						if (Float.parseFloat(getVal) % 100 > 0) {
							famt = Float.parseFloat(getVal) / 100;
							amt = "" + famt;
						} else {
							intamt = Integer.parseInt(getVal) / 100;
							amt = "" + intamt;
						}
						log.trace("amt >> {}", amt);
						listAA.add(amt);
					}
				}
			}
		}

		// AB處理
		if (dataListMapAB.size() > 0) {
			int countTime = listAA.size()+1;
			int abCount = dataListMapAB.size();
			String val = "";
			for (int i = 0; i < abCount; i++) {
				LinkedList listAB = new LinkedList<>();
				for (int j = 0; j < countTime; j++) {
					if (j == 0) {
						String itr1 = dataListMapAB.get(i).get("ITR" + (j + 1));
						if(itr1.equals("000")) break;
						listAB.add(getBeanMapValue("RateQuery",
								"N021TERM" + String.format("%03d", Integer.parseInt(itr1))));
					} else {
						val=dataListMapAB.get(i).get("ITR" + (j + 1));
						if ("0.000".equals(val)) {
							val="";
							listAB.add(val);
						}
						listAB.add(val);
					} 
				}
				lListAB.add(listAB);
			}
		}

		bs.addData("RECNormal", dataListMapNormal);
		bs.addData("ListAA", listAA);
		bs.addData("LListAB", lListAB);
		bs.addData("isAAColor", isAAColor);

		String today = DateUtil.getTWDate("");
		bs.addData("today", today);
		bs.addData("yyymmdd", yyymmdd);
		bs.addData("yyyyDay", yyyyDay);

		bs.addData("RECSIZE", dataListMapNormal.size());

	}

	/**
	 * 輸入beanId && beanMapKey Get beanMapValue
	 * 
	 * @param beanId
	 * @param beanMapKey
	 * @see spring-arg.xml
	 * @return get spring-arg.xml beanMapValue By beanId && beanMapKey, if any (or
	 *         empty String otherwise)
	 */
	public String getBeanMapValue(String beanId, String beanMapKey) {
		// 取得Bean中的Map
		Map<String, String> getTypeBean = SpringBeanFactory.getBean(beanId);
		// 取得Map中的Value
		String getTypeBeanValue = getTypeBean.get(beanMapKey);
		return getTypeBeanValue;
	}
}
