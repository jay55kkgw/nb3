package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class EBPay1_1_REST_RQ extends BaseRestBean_PAY implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6791952991679178273L;
	private String CMDATE1;//代收期限-民國年
	private String CMDATE2;//代收期限-財金(轉西元年)
	private String WAT_NO;
	private String CHKCOD;
	private String AMOUNT;
	private String CARDNUM;
	private String EXPDTA;
	private String CHECKNO;
	private String SessionId;
	private String FEE;
	private String TrnsCode;
	private String ADOPID;
	private String FGTXWAY;
	
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getCMDATE1() {
		return CMDATE1;
	}
	public void setCMDATE1(String cMDATE1) {
		CMDATE1 = cMDATE1;
	}
	public String getCMDATE2() {
		return CMDATE2;
	}
	public void setCMDATE2(String cMDATE2) {
		CMDATE2 = cMDATE2;
	}
	public String getWAT_NO() {
		return WAT_NO;
	}
	public void setWAT_NO(String wAT_NO) {
		WAT_NO = wAT_NO;
	}
	public String getCHKCOD() {
		return CHKCOD;
	}
	public void setCHKCOD(String cHKCOD) {
		CHKCOD = cHKCOD;
	}
	public String getAMOUNT() {
		return AMOUNT;
	}
	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}
	public String getCARDNUM() {
		return CARDNUM;
	}
	public void setCARDNUM(String cARDNUM) {
		CARDNUM = cARDNUM;
	}
	public String getEXPDTA() {
		return EXPDTA;
	}
	public void setEXPDTA(String eXPDTA) {
		EXPDTA = eXPDTA;
	}
	public String getCHECKNO() {
		return CHECKNO;
	}
	public void setCHECKNO(String cHECKNO) {
		CHECKNO = cHECKNO;
	}
	public String getSessionId() {
		return SessionId;
	}
	public void setSessionId(String sessionId) {
		SessionId = sessionId;
	}
	public String getFEE() {
		return FEE;
	}
	public void setFEE(String fEE) {
		FEE = fEE;
	}
	public String getTrnsCode() {
		return TrnsCode;
	}
	public void setTrnsCode(String trnsCode) {
		TrnsCode = trnsCode;
	}
}
