package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N320_REST_RSDATA_FX extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7445173263967145749L;

	private String LNACCNO;
	private String LNREFNO;
	private String LNCCY;
	private String LNCOS;
	private String LNINTST;
	private String LNDUEDT;
	private String NXTINTD;
	private String LNFIXR;
	private String LNUPDATE;
	private String TYPEA;
	private String RELREF;
	
	public String getLNACCNO() {
		return LNACCNO;
	}
	public void setLNACCNO(String lNACCNO) {
		LNACCNO = lNACCNO;
	}
	public String getLNREFNO() {
		return LNREFNO;
	}
	public void setLNREFNO(String lNREFNO) {
		LNREFNO = lNREFNO;
	}
	public String getLNCCY() {
		return LNCCY;
	}
	public void setLNCCY(String lNCCY) {
		LNCCY = lNCCY;
	}
	public String getLNCOS() {
		return LNCOS;
	}
	public void setLNCOS(String lNCOS) {
		LNCOS = lNCOS;
	}
	public String getLNINTST() {
		return LNINTST;
	}
	public void setLNINTST(String lNINTST) {
		LNINTST = lNINTST;
	}
	public String getLNDUEDT() {
		return LNDUEDT;
	}
	public void setLNDUEDT(String lNDUEDT) {
		LNDUEDT = lNDUEDT;
	}
	public String getNXTINTD() {
		return NXTINTD;
	}
	public void setNXTINTD(String nXTINTD) {
		NXTINTD = nXTINTD;
	}
	public String getLNFIXR() {
		return LNFIXR;
	}
	public void setLNFIXR(String lNFIXR) {
		LNFIXR = lNFIXR;
	}
	public String getLNUPDATE() {
		return LNUPDATE;
	}
	public void setLNUPDATE(String lNUPDATE) {
		LNUPDATE = lNUPDATE;
	}
	public String getTYPEA() {
		return TYPEA;
	}
	public void setTYPEA(String tYPEA) {
		TYPEA = tYPEA;
	}
	public String getRELREF() {
		return RELREF;
	}
	public void setRELREF(String rELREF) {
		RELREF = rELREF;
	}
}
