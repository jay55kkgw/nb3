package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * NA03電文RS
 */
public class Na03_REST_RS extends BaseRestBean implements Serializable{
	private static final long serialVersionUID = 6791228050721094802L;
	
	private String MSGCOD;

	public String getMSGCOD() {
		return MSGCOD;
	}
	public void setMSGCOD(String mSGCOD) {
		MSGCOD = mSGCOD;
	}
}