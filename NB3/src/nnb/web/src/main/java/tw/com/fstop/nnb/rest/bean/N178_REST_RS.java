package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N178_REST_RS extends BaseRestBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1038494851089760312L;
	private String FYACN;		// 存單帳號
	private String FDPNUM;		// 存單號碼
	private String INTMTH;		// 計息方式
	private String TYPE1;		// 續存方式
	private String FYTSFAN;		// 轉入帳號(外匯活存帳號)
	private String CRY;			// 幣別代號
	private String CRYNAME;		// 幣別名稱
	private String AMTTSF;		// 續轉存金額
	private String TYPCOD;		// 期別種類
	private String TERM;		// 期別
	private String DPISDT_O;	// 原存單起存日
	private String DUEDAT_O;	// 原存單到期日
	private String DPISDT_N;	// 新存單起存日
	private String DUEDAT_N;	// 新存單到期日
	private String ITR;			// 新存單利率
	private String AMTFDP;		// 原存單金額
	private String INT;			// 原存單利息
	private String FYTAX;		// 所得稅
	private String PYDINT;		// 已付利息
	private String PAIAFTX;		// 稅後本息
	private String TRNDATE;		// 交易日期 YYYMMDD
	private String TRNTIME;		// 交易時間 HHMMSS
	private String NHITAX;		// 健保費
	
	// 電文沒回但ms_tw回應的資料
	private String CMQTIME;		// 交易時間
	private String CMTXTIME;	// 交易日期時間 民國YYY/MM/DD HH:MM:SS
	
	public String getFYACN() {
		return FYACN;
	}
	public void setFYACN(String fYACN) {
		FYACN = fYACN;
	}
	public String getFDPNUM() {
		return FDPNUM;
	}
	public void setFDPNUM(String fDPNUM) {
		FDPNUM = fDPNUM;
	}
	public String getINTMTH() {
		return INTMTH;
	}
	public void setINTMTH(String iNTMTH) {
		INTMTH = iNTMTH;
	}
	public String getTYPE1() {
		return TYPE1;
	}
	public void setTYPE1(String tYPE1) {
		TYPE1 = tYPE1;
	}
	public String getFYTSFAN() {
		return FYTSFAN;
	}
	public void setFYTSFAN(String fYTSFAN) {
		FYTSFAN = fYTSFAN;
	}
	public String getCRY() {
		return CRY;
	}
	public void setCRY(String cRY) {
		CRY = cRY;
	}
	public String getCRYNAME() {
		return CRYNAME;
	}
	public void setCRYNAME(String cRYNAME) {
		CRYNAME = cRYNAME;
	}
	public String getAMTTSF() {
		return AMTTSF;
	}
	public void setAMTTSF(String aMTTSF) {
		AMTTSF = aMTTSF;
	}
	public String getTYPCOD() {
		return TYPCOD;
	}
	public void setTYPCOD(String tYPCOD) {
		TYPCOD = tYPCOD;
	}
	public String getTERM() {
		return TERM;
	}
	public void setTERM(String tERM) {
		TERM = tERM;
	}
	public String getDPISDT_O() {
		return DPISDT_O;
	}
	public void setDPISDT_O(String dPISDT_O) {
		DPISDT_O = dPISDT_O;
	}
	public String getDUEDAT_O() {
		return DUEDAT_O;
	}
	public void setDUEDAT_O(String dUEDAT_O) {
		DUEDAT_O = dUEDAT_O;
	}
	public String getDPISDT_N() {
		return DPISDT_N;
	}
	public void setDPISDT_N(String dPISDT_N) {
		DPISDT_N = dPISDT_N;
	}
	public String getDUEDAT_N() {
		return DUEDAT_N;
	}
	public void setDUEDAT_N(String dUEDAT_N) {
		DUEDAT_N = dUEDAT_N;
	}
	public String getITR() {
		return ITR;
	}
	public void setITR(String iTR) {
		ITR = iTR;
	}
	public String getAMTFDP() {
		return AMTFDP;
	}
	public void setAMTFDP(String aMTFDP) {
		AMTFDP = aMTFDP;
	}
	public String getINT() {
		return INT;
	}
	public void setINT(String iNT) {
		INT = iNT;
	}
	public String getFYTAX() {
		return FYTAX;
	}
	public void setFYTAX(String fYTAX) {
		FYTAX = fYTAX;
	}
	public String getPYDINT() {
		return PYDINT;
	}
	public void setPYDINT(String pYDINT) {
		PYDINT = pYDINT;
	}
	public String getPAIAFTX() {
		return PAIAFTX;
	}
	public void setPAIAFTX(String pAIAFTX) {
		PAIAFTX = pAIAFTX;
	}
	public String getTRNDATE() {
		return TRNDATE;
	}
	public void setTRNDATE(String tRNDATE) {
		TRNDATE = tRNDATE;
	}
	public String getTRNTIME() {
		return TRNTIME;
	}
	public void setTRNTIME(String tRNTIME) {
		TRNTIME = tRNTIME;
	}
	public String getNHITAX() {
		return NHITAX;
	}
	public void setNHITAX(String nHITAX) {
		NHITAX = nHITAX;
	}
	
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	
	public String getCMTXTIME() {
		return CMTXTIME;
	}
	public void setCMTXTIME(String cMTXTIME) {
		CMTXTIME = cMTXTIME;
	}
	
}
