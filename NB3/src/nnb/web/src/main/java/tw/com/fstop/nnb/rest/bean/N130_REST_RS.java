package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

/**
 * 活期性存款交易明細查詢
 * 
 * @author Ian
 *
 */
public class N130_REST_RS extends BaseRestBean implements Serializable {

	private static final long serialVersionUID = -8133182731073566739L;

	private String ABEND;
	
	private String USERDATA;
	
	private String CMQTIME;
	
	private String REC_NO;

	private String CMRECNUM;

	private String ENDDATE;

	private String CMPERIOD;

	private String __OCCURS;

	private String LSTIME;

	private String STADATE;

	private LinkedList<N130_REST_RSDATA> REC;

	public String getABEND() {
		return ABEND;
	}

	public void setABEND(String aBEND) {
		ABEND = aBEND;
	}

	public String getUSERDATA() {
		return USERDATA;
	}

	public void setUSERDATA(String uSERDATA) {
		USERDATA = uSERDATA;
	}

	public String getCMQTIME() {
		return CMQTIME;
	}

	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}

	public String getREC_NO() {
		return REC_NO;
	}

	public void setREC_NO(String rEC_NO) {
		REC_NO = rEC_NO;
	}

	public String getCMRECNUM() {
		return CMRECNUM;
	}

	public void setCMRECNUM(String cMRECNUM) {
		CMRECNUM = cMRECNUM;
	}

	public String getENDDATE() {
		return ENDDATE;
	}

	public void setENDDATE(String eNDDATE) {
		ENDDATE = eNDDATE;
	}

	public String getCMPERIOD() {
		return CMPERIOD;
	}

	public void setCMPERIOD(String cMPERIOD) {
		CMPERIOD = cMPERIOD;
	}

	public String get__OCCURS() {
		return __OCCURS;
	}

	public void set__OCCURS(String __OCCURS) {
		this.__OCCURS = __OCCURS;
	}

	public String getLSTIME() {
		return LSTIME;
	}

	public void setLSTIME(String lSTIME) {
		LSTIME = lSTIME;
	}

	public String getSTADATE() {
		return STADATE;
	}

	public void setSTADATE(String sTADATE) {
		STADATE = sTADATE;
	}

	public LinkedList<N130_REST_RSDATA> getREC() {
		return REC;
	}

	public void setREC(LinkedList<N130_REST_RSDATA> rEC) {
		REC = rEC;
	}

}
