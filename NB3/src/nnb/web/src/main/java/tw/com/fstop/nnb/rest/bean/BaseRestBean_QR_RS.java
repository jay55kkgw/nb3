package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.web.util.ConfingManager;

public class BaseRestBean_QR_RS extends BaseRestBean implements Serializable{

	//	代碼	說明
	//	O001	交易成功。
	//	O101	訊息檢核錯誤。
	//	O102	訊息長度不符。
	//	O441	門號失效或不存在
	//	O442	此門號已有綁定預設帳號(於原發卡行綁定預設時回覆)
	//	O443	此門號已綁定預設帳號於其他銀行
	//	O445	帳號尚未生效
	//	O451	申請帳戶為問題帳戶
	//	O486	客戶手機號碼與門號所綁定之代碼(Token)不相同
	//	O997	交易失敗。
	//	O998	驗證碼錯誤。
	//	O999	系統發生錯誤。
	private String RCode;

	public String getRCode() {
		return RCode;
	}

	public void setRCode(String rCode) {
		RCode = rCode;
	}

	
}
