package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N900_REST_RSDATA extends BaseRestBean implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7724215918825362442L;
		
	private String SMSA;//帳單遞送地址
	private String POSTCOD;//郵遞區號
	private String ADDRESS;//地址
	private String HOME_TEL;//住家電話
	private String OFFICE_TEL;//公司電話
	private String MOBILE_TEL;//手機號碼
	
	
	public String getSMSA() {
		return SMSA;
	}
	public void setSMSA(String sMSA) {
		SMSA = sMSA;
	}
	public String getPOSTCOD() {
		return POSTCOD;
	}
	public void setPOSTCOD(String pOSTCOD) {
		POSTCOD = pOSTCOD;
	}
	public String getADDRESS() {
		return ADDRESS;
	}
	public void setADDRESS(String aDDRESS) {
		ADDRESS = aDDRESS;
	}
	public String getHOME_TEL() {
		return HOME_TEL;
	}
	public void setHOME_TEL(String hOME_TEL) {
		HOME_TEL = hOME_TEL;
	}
	public String getOFFICE_TEL() {
		return OFFICE_TEL;
	}
	public void setOFFICE_TEL(String oFFICE_TEL) {
		OFFICE_TEL = oFFICE_TEL;
	}
	public String getMOBILE_TEL() {
		return MOBILE_TEL;
	}
	public void setMOBILE_TEL(String mOBILE_TEL) {
		MOBILE_TEL = mOBILE_TEL;
	}
}
