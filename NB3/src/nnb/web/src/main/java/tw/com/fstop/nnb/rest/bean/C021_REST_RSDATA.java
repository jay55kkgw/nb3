package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * C021電文RSDATA
 */
public class C021_REST_RSDATA implements Serializable{
	private static final long serialVersionUID = 7127169312136862734L;
	
	private String TRANSCODE;
	private String CDNO;
	private String CRY;
	private String FUNDAMT;
	private String UNIT;
	private String SHORTTRADE;
	private String SHORTTUNIT;
	private String FUSMON;
	
	public String getTRANSCODE(){
		return TRANSCODE;
	}
	public void setTRANSCODE(String tRANSCODE){
		TRANSCODE = tRANSCODE;
	}
	public String getCRY(){
		return CRY;
	}
	public void setCRY(String cRY){
		CRY = cRY;
	}
	public String getFUNDAMT(){
		return FUNDAMT;
	}
	public void setFUNDAMT(String fUNDAMT){
		FUNDAMT = fUNDAMT;
	}
	public String getUNIT(){
		return UNIT;
	}
	public void setUNIT(String uNIT){
		UNIT = uNIT;
	}
	public String getSHORTTRADE(){
		return SHORTTRADE;
	}
	public void setSHORTTRADE(String sHORTTRADE){
		SHORTTRADE = sHORTTRADE;
	}
	public String getSHORTTUNIT(){
		return SHORTTUNIT;
	}
	public void setSHORTTUNIT(String sHORTTUNIT){
		SHORTTUNIT = sHORTTUNIT;
	}
	public String getCDNO() {
		return CDNO;
	}
	public void setCDNO(String cDNO) {
		CDNO = cDNO;
	}
	public String getFUSMON() {
		return FUSMON;
	}
	public void setFUSMON(String fUSMON) {
		FUSMON = fUSMON;
	}
}