package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;

public class N322_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -1568671015374302095L;

	/**
	 * 
	 */

	@SerializedName(value = "UPD_FLG")
	private String UPD_FLG;// 異動註記

	@SerializedName(value = "Q2A")
	private String Q2A;// 變更前學歷
	
	@SerializedName(value = "Q3A")
	private String Q3A;// 變更前職業

	@SerializedName(value = "SRCFUND")
	private String SRCFUND;// 變更後職業

	@SerializedName(value = "Q4A")
	private String Q4A;// 變更前年收入

	@SerializedName(value = "newsalary")
	private String newsalary;// 變更後年收入

	@SerializedName(value = "OMARK1")
	private String OMARK1;// 變更前是否領有全民健康保險重大傷病證明

	@SerializedName(value = "Q1")
	private String Q1;// 年齡為何?A:未達40歲;B:40歲以上~未達55歲;C:55歲以上~未達70歲;D:70歲以上

	@SerializedName(value = "Q2")
	private String Q2;// 教育程度為何?A:博士;B:碩士;C:大學;D:專科;E:高中職;F:國中以下

	@SerializedName(value = "Q3")
	private String Q3;// 職業為何?A:金融相關行業（銀行、保險、證券等）;B:非金融相關行業;C:學生／家管／退休／其他

	@SerializedName(value = "Q4")
	private String Q4;// 年收入?A:未達50萬;B:50萬以上 - 未達70萬;C:70萬以上 - 未達100萬;D:100萬以上

	@SerializedName(value = "Q5")
	private String Q5;// 資金來源?A:薪資、租金、資本利得等;B:儲蓄所得;C:退休金;

	@SerializedName(value = "Q6")
	private String Q6;// 資產合計?A:未達50萬;B:50萬以上 - 未達70萬;C:70萬以上 - 未達100萬;D:100萬以上

	@SerializedName(value = "Q7")
	private String Q7;// 投資目的?A:投資理財;B:退休計劃;C:教育基金;D:清償債務

	@SerializedName(value = "Q8")
	private String Q8;// 何時開始提領部分投資金額?A:未達1年;B:1年以上～未達2年;C:2年以上～未達5年;D:5年以上

	@SerializedName(value = "Q9")
	private String Q9;// 期望平均年報酬率?A:－15％～30％;B:－8％～15％;C:0％～3％

	@SerializedName(value = "Q10")
	private String Q10;// 預計投資期限多長?A:未達1年;B:1年以上～未達2年;C:2年以上

	// START---Q11過去投資經驗
	@SerializedName(value = "Q111")
	private String Q111;// 曾經投資期貨、連動債、投資型保單、衍生性金融商品

	@SerializedName(value = "Q112")
	private String Q112;// 曾經投資國內外共同基金、債券、股票

	@SerializedName(value = "Q113")
	private String Q113;// 對於投資有心得，偏好自行決定投資策略

	@SerializedName(value = "Q114")
	private String Q114;// 沒經驗
	// END---Q11過去投資經驗

	@SerializedName(value = "Q12")
	private String Q12;// 從事理財投資時間?A:未達1年;B:1年以上～未達2年;C:2年以上～未達5年;D:5年以上
	// START---Q13對金融商品的認識
	@SerializedName(value = "Q131")
	private String Q131;// 對金融商品（如連動債等衍生性商品）了解

	@SerializedName(value = "Q132")
	private String Q132;// 對金融商品（如股票）了解

	@SerializedName(value = "Q133")
	private String Q133;// 對金融商品（如國內外共同基金）了解

	@SerializedName(value = "Q134")
	private String Q134;// 對金融商品不了解
	// END---Q13對金融商品的認識

	@SerializedName(value = "Q14")
	private String Q14;// 最接近的投資行為?A:無法忍受投資虧損;B:我偏好資本損失率低，可以獲取穩定收益;C:我願意承擔少量風險，以追求潛力稍高的報酬;D:我喜歡高風險高報酬，不在乎短期的波動

	@SerializedName(value = "Q15")
	private String Q15;// 何者風險最高?A:期貨或衍生性金融產品;B:國內外基金、股票或投資型保單;C:存款、定存

	@SerializedName(value = "MARK1")
	private String MARK1;// 是否領有重大傷病證明?Y:是;N:否

	@Override
	public Map<String, String> coverMap() {
		LinkedHashMap<String, String> result = new LinkedHashMap<String, String>();
        result.put("交易名稱", "投資屬性評估調查表－（自然人版）問卷結果更新");
        result.put("交易類型", "更新");
		return result;
		// 履歷異動
		// // 學歷
		// String UPD_FLG1 = this.UPD_FLG.substring(0, 1);
		// // 職業
		// String UPD_FLG2 = this.UPD_FLG.substring(1, 2);
		// // 年收入
		// String UPD_FLG3 = this.UPD_FLG.substring(2, 3);
		// // 重大傷殘
		// String MARK1 = this.MARK1;

		// if (!"".equals(UPD_FLG1) && !"".equals(UPD_FLG2) && !"".equals(UPD_FLG3) && !"".equals(MARK1)) {
		// 	result.put("個人資料異動項目", "");

		// 	if ("Y".equals(UPD_FLG1)) {
		// 		String DEGREE_DESC ="";
		// 		String NEWDEGREE_DESC="";
		// 		switch (Q2A) {
		// 			case "A":
		// 				DEGREE_DESC="博士";
		// 				break;
				
		// 			case "B":
		// 				DEGREE_DESC="碩士";
		// 				break;
				
		// 			case "C":
		// 				DEGREE_DESC="大學";
		// 				break;
				
		// 			case "D":
		// 				DEGREE_DESC="專科";
		// 				break;
				
		// 			case "E":
		// 				DEGREE_DESC="高中職";
		// 				break;
				
		// 			case "F":
		// 				DEGREE_DESC="國中以下";
		// 				break;
		// 		}

		// 		switch (Q2) {
		// 			case "A":
		// 				NEWDEGREE_DESC="博士";
		// 				break;
				
		// 			case "B":
		// 				NEWDEGREE_DESC="碩士";
		// 				break;
				
		// 			case "C":
		// 				NEWDEGREE_DESC="大學";
		// 				break;
				
		// 			case "D":
		// 				NEWDEGREE_DESC="專科";
		// 				break;
				
		// 			case "E":
		// 				NEWDEGREE_DESC="高中職";
		// 				break;
				
		// 			case "F":
		// 				NEWDEGREE_DESC="國中以下";
		// 				break;
		// 		}

		// 		result.put("異動前學歷", DEGREE_DESC);
		// 		result.put("異動後學歷", NEWDEGREE_DESC);
		// 	}
		// 	if ("Y".equals(UPD_FLG2)) {
		// 		String CAREERCHINESE=careerSwitch(Q3A);
		// 		String SRCFUNDCHINESE=careerSwitch(SRCFUND);
				
		// 		result.put("異動前職業", CAREERCHINESE);
		// 		result.put("異動後職業", SRCFUNDCHINESE);
		// 	}
		// 	if ("Y".equals(UPD_FLG3)) {
		// 		int oldsalary = Integer.valueOf(Q4A) / 10000;
		// 		result.put("異動前年收入", new String().valueOf(oldsalary)+"萬");
		// 		result.put("異動後年收入", this.newsalary+"萬");
		// 	}

		// 	if ("Y".equals(MARK1)) {
		// 		result.put("變更前是否領有全民健康保險重大傷病證明", this.OMARK1 == "Y" ? "是" : "否");
		// 		result.put("變更後是否領有全民健康保險重大傷病證明", "是");
		// 	}
		// }

	// 	switch (Q1) {
	// 		case "A":
	// 			result.put("您的年齡", "未達40歲");
	// 			break;
	// 		case "B":
	// 			result.put("您的年齡", "40歲以上~未達55歲");
	// 			break;
	// 		case "C":
	// 			result.put("您的年齡", "55歲以上~未達70歲");
	// 			break;
	// 		case "D":
	// 			result.put("您的年齡", "70歲以上");
	// 			break;
	// 	}

	// 	switch (Q2) {
	// 		case "A":
	// 			result.put("您的教育程度", "博士");
	// 			break;
	// 		case "B":
	// 			result.put("您的教育程度", "碩士");
	// 			break;
	// 		case "C":
	// 			result.put("您的教育程度", "大學");
	// 			break;
	// 		case "D":
	// 			result.put("您的教育程度", "專科");
	// 			break;
	// 		case "E":
	// 			result.put("您的教育程度", "高中職");
	// 			break;
	// 		case "F":
	// 			result.put("您的教育程度", "國中以下");
	// 			break;
	// 	}

	// 	switch (Q3) {
	// 		case "A":
	// 			result.put("您的職業", "金融相關行業（銀行、保險、證券等）");
	// 			break;
	// 		case "B":
	// 			result.put("您的職業", "非金融相關行業");
	// 			break;
	// 		case "C":
	// 			result.put("您的職業", "學生／家管／退休／其他");
	// 			break;
	// 	}

	// 	switch (Q4) {
	// 		case "A":
	// 			result.put("您的年收入", "未達50萬");
	// 			break;
	// 		case "B":
	// 			result.put("您的年收入", "50萬以上 - 未達70萬");
	// 			break;
	// 		case "C":
	// 			result.put("您的年收入", "70萬以上 - 未達100萬");
	// 			break;
	// 		case "D":
	// 			result.put("您的年收入", "100萬以上");
	// 			break;
	// 	}

	// 	switch (Q5) {
	// 		case "A":
	// 			result.put("您的資金來源", "薪資、租金、資本利得等");
	// 			break;
	// 		case "B":
	// 			result.put("您的資金來源", "儲蓄所得");
	// 			break;
	// 		case "C":
	// 			result.put("您的資金來源", "退休金");
	// 			break;
	// 	}

	// 	switch (Q6) {
	// 		case "A":
	// 			result.put("您目前擁有之有價證券與存款合計數", "未達50萬");
	// 			break;
	// 		case "B":
	// 			result.put("您目前擁有之有價證券與存款合計數", "50萬以上 - 未達70萬");
	// 			break;
	// 		case "C":
	// 			result.put("您目前擁有之有價證券與存款合計數", "70萬以上 - 未達100萬");
	// 			break;
	// 		case "D":
	// 			result.put("您目前擁有之有價證券與存款合計數", "100萬以上");
	// 			break;
	// 	}

	// 	switch (Q7) {
	// 		case "A":
	// 			result.put("您的投資目的與主要需求", "投資理財");
	// 			break;
	// 		case "B":
	// 			result.put("您的投資目的與主要需求", "退休計劃");
	// 			break;
	// 		case "C":
	// 			result.put("您的投資目的與主要需求", "教育基金");
	// 			break;
	// 		case "D":
	// 			result.put("您的投資目的與主要需求", "清償債務");
	// 			break;
	// 	}

	// 	switch (Q8) {
	// 		case "A":
	// 			result.put("您的計畫提領投資部分金額的時間", "未達1年");
	// 			break;
	// 		case "B":
	// 			result.put("您的計畫提領投資部分金額的時間", "1年以上～未達2年");
	// 			break;
	// 		case "C":
	// 			result.put("您的計畫提領投資部分金額的時間", "2年以上～未達5年");
	// 			break;
	// 		case "D":
	// 			result.put("您的計畫提領投資部分金額的時間", "5年以上");
	// 			break;
	// 	}

	// 	switch (Q9) {
	// 		case "A":
	// 			result.put("您期望的年平均報酬率", "－15％～30％");
	// 			break;
	// 		case "B":
	// 			result.put("您期望的年平均報酬率", "－8％～15％");
	// 			break;
	// 		case "C":
	// 			result.put("您期望的年平均報酬率", "0％～3％");
	// 			break;
	// 	}

	// 	switch (Q10) {
	// 		case "A":
	// 			result.put("您投資金融商品預計投資期限", "未達1年");
	// 			break;
	// 		case "B":
	// 			result.put("您投資金融商品預計投資期限", "1年以上～未達2年");
	// 			break;
	// 		case "C":
	// 			result.put("您投資金融商品預計投資期限", "2年以上");
	// 			break;
	// 	}
	// 	//Q11 START
	// 	StringBuilder Q11 = new StringBuilder();
	// 	if (Q111!=null) {
	// 		Q11.append("曾經投資期貨、連動債、投資型保單、衍生性金融商品");
	// 	}

	// 	if (Q112!=null) {
	// 		if (Q11.length() > 0) {
	// 			Q11.append(",");
	// 		}
	// 		Q11.append("曾經投資國內外共同基金、債券、股票");
	// 	}

	// 	if (Q113!=null) {
	// 		if (Q11.length() > 0) {
	// 			Q11.append(",");
	// 		}
	// 		Q11.append("對於投資有心得，偏好自行決定投資策略");
	// 	}

	// 	if (Q114!=null) {
	// 		if (Q11.length() > 0) {
	// 			Q11.append(",");
	// 		}
	// 		Q11.append("沒經驗");
	// 	}

	// 	result.put("您過去的投資經驗", Q11.toString());
	// 	//Q11 END
	// 	switch (Q12) {
	// 		case "A":
	// 			result.put("您從事投資理財的時間", "未達1年");
	// 			break;
	// 		case "B":
	// 			result.put("您從事投資理財的時間", "1年以上～未達2年");
	// 			break;
	// 		case "C":
	// 			result.put("您從事投資理財的時間", "2年以上～未達5年");
	// 			break;
	// 		case "D":
	// 			result.put("您從事投資理財的時間", "5年以上");
	// 			break;
	// 	}
	// 	//Q13 START
	// 	StringBuilder Q13 = new StringBuilder();
	// 	if (Q131!=null) {
	// 		Q13.append("對金融商品（如連動債等衍生性商品）了解");
	// 	}

	// 	if (Q132!=null) {
	// 		if (Q13.length() > 0) {
	// 			Q13.append(",");
	// 		}
	// 		Q13.append("對金融商品（如股票）了解");
	// 	}

	// 	if (Q133!=null) {
	// 		if (Q13.length() > 0) {
	// 			Q13.append(",");
	// 		}
	// 		Q13.append("對金融商品（如國內外共同基金）了解");
	// 	}

	// 	if (Q134!=null) {
	// 		if (Q13.length() > 0) {
	// 			Q13.append(",");
	// 		}
	// 		Q13.append("對金融商品不了解");
	// 	}

	// 	result.put("您對金融商品的認識", Q13.toString());
	// 	//Q13 END
	// 	switch (Q14) {
	// 		case "A":
	// 			result.put("您最接近的投資行為", "無法忍受投資虧損");
	// 			break;
	// 		case "B":
	// 			result.put("您最接近的投資行為", "我偏好資本損失率低，可以獲取穩定收益");
	// 			break;
	// 		case "C":
	// 			result.put("您最接近的投資行為", "我願意承擔少量風險，以追求潛力稍高的報酬");
	// 			break;
	// 		case "D":
	// 			result.put("您最接近的投資行為", "我喜歡高風險高報酬，不在乎短期的波動");
	// 			break;
	// 	}

	// 	switch (Q15) {
	// 		case "A":
	// 			result.put("您認為風險最高的投資", "期貨或衍生性金融產品");
	// 			break;
	// 		case "B":
	// 			result.put("您認為風險最高的投資", "國內外基金、股票或投資型保單");
	// 			break;
	// 		case "C":
	// 			result.put("您認為風險最高的投資", "存款、定存");
	// 			break;
	// 	}

	// 	result.put("您是否領有全民健康保險重大傷病證明", MARK1=="Y"?"是":"否");

	// 	return result;
	// }

	// /**
	//  * 轉換職業中文
	//  */
	// public String careerSwitch(String careerID){
	// 	String careerChinese = "";
		
	// 	switch(careerID){
	// 		case "061100":
	// 			careerChinese = "國防事業";
	// 			break;
	// 		case "061200":
	// 			careerChinese = "警察單位";
	// 			break;
	// 		case "061300":
	// 			careerChinese = "其他公共行政類";
	// 			break;
	// 		case "061400":
	// 			careerChinese = "教育業";
	// 			break;
	// 		case "061410":
	// 			careerChinese = "學生";
	// 			break;
	// 		case "061500":
	// 			careerChinese = "工、商及服務業";
	// 			break;
	// 		case "0615A0":
	// 			careerChinese = "農林漁牧業";
	// 			break;
	// 		case "0615B0":
	// 			careerChinese = "礦石及土石採取業";
	// 			break;
	// 		case "0615C0":
	// 			careerChinese = "製造業";
	// 			break;
	// 		case "0615D0":
	// 			careerChinese = "水電燃氣業";
	// 			break;
	// 		case "0615E0":
	// 			careerChinese = "營造業";
	// 			break;
	// 		case "0615F0":
	// 			careerChinese = "批發及零售業";
	// 			break;
	// 		case "0615G0":
	// 			careerChinese = "住宿及餐飲業";
	// 			break;
	// 		case "0615H0":
	// 			careerChinese = "運輸、倉儲及通信業";
	// 			break;
	// 		case "0615I0":
	// 			careerChinese = "金融及保險業";
	// 			break;
	// 		case "0615J0":
	// 			careerChinese = "不動產及租賃業";
	// 			break;
	// 		case "061610":
	// 			careerChinese = "其他專業服務業";
	// 			break;
	// 		case "061620":
	// 			careerChinese = "技術服務業";
	// 			break;
	// 		case "061630":
	// 			careerChinese = "特定專業服務業(律師、會計師、地政士、公證人、記帳士、記帳及報稅代理人)";
	// 			break;
	// 		case "061640":
	// 			careerChinese = "特定專業服務業(受聘於專業服務業之行政事務職員)";
	// 			break;
	// 		case "061650":
	// 			careerChinese = "銀樓業(含珠寶、鐘錶及貴金屬之製造、批發及零售)";
	// 			break;
	// 		case "061660":
	// 			careerChinese = "虛擬貨幣交易服務業";
	// 			break;
	// 		case "061670":
	// 			careerChinese = "博弈業";
	// 			break;
	// 		case "061680":
	// 			careerChinese = "國防武器或戰爭設備相關行業(軍火)";
	// 			break;
	// 		case "061690":
	// 			careerChinese = "家管";
	// 			break;
	// 		case "061691":
	// 			careerChinese = "自由業";
	// 			break;
	// 		case "061692":
	// 			careerChinese = "無業";
	// 			break;
	// 		case "061700":
	// 			careerChinese = "其他(無業家管退休)";
	// 			break;
	// 		case "069999":
	// 			careerChinese = "非法人組織授信戶負責人";
	// 			break;
	// 	}
		
		// return careerChinese;
	}

	public String getUPD_FLG() {
		return UPD_FLG;
	}

	public void setUPD_FLG(String uPD_FLG) {
		UPD_FLG = uPD_FLG;
	}

	public String getQ2A() {
		return Q2A;
	}

	public void setQ2A(String q2a) {
		Q2A = q2a;
	}

	public String getQ3A() {
		return Q3A;
	}

	public void setQ3A(String q3a) {
		Q3A = q3a;
	}

	public String getSRCFUND() {
		return SRCFUND;
	}

	public void setSRCFUND(String sRCFUND) {
		SRCFUND = sRCFUND;
	}

	public String getQ4A() {
		return Q4A;
	}

	public void setQ4A(String q4a) {
		Q4A = q4a;
	}

	public String getNewsalary() {
		return newsalary;
	}

	public void setNewsalary(String newsalary) {
		this.newsalary = newsalary;
	}

	public String getOMARK1() {
		return OMARK1;
	}

	public void setOMARK1(String oMARK1) {
		OMARK1 = oMARK1;
	}

	public String getQ1() {
		return Q1;
	}

	public void setQ1(String q1) {
		Q1 = q1;
	}

	public String getQ2() {
		return Q2;
	}

	public void setQ2(String q2) {
		Q2 = q2;
	}

	public String getQ3() {
		return Q3;
	}

	public void setQ3(String q3) {
		Q3 = q3;
	}

	public String getQ4() {
		return Q4;
	}

	public void setQ4(String q4) {
		Q4 = q4;
	}

	public String getQ5() {
		return Q5;
	}

	public void setQ5(String q5) {
		Q5 = q5;
	}

	public String getQ6() {
		return Q6;
	}

	public void setQ6(String q6) {
		Q6 = q6;
	}

	public String getQ7() {
		return Q7;
	}

	public void setQ7(String q7) {
		Q7 = q7;
	}

	public String getQ8() {
		return Q8;
	}

	public void setQ8(String q8) {
		Q8 = q8;
	}

	public String getQ9() {
		return Q9;
	}

	public void setQ9(String q9) {
		Q9 = q9;
	}

	public String getQ10() {
		return Q10;
	}

	public void setQ10(String q10) {
		Q10 = q10;
	}

	public String getQ111() {
		return Q111;
	}

	public void setQ111(String q111) {
		Q111 = q111;
	}

	public String getQ112() {
		return Q112;
	}

	public void setQ112(String q112) {
		Q112 = q112;
	}

	public String getQ113() {
		return Q113;
	}

	public void setQ113(String q113) {
		Q113 = q113;
	}

	public String getQ114() {
		return Q114;
	}

	public void setQ114(String q114) {
		Q114 = q114;
	}

	public String getQ12() {
		return Q12;
	}

	public void setQ12(String q12) {
		Q12 = q12;
	}

	public String getQ131() {
		return Q131;
	}

	public void setQ131(String q131) {
		Q131 = q131;
	}

	public String getQ132() {
		return Q132;
	}

	public void setQ132(String q132) {
		Q132 = q132;
	}

	public String getQ133() {
		return Q133;
	}

	public void setQ133(String q133) {
		Q133 = q133;
	}

	public String getQ134() {
		return Q134;
	}

	public void setQ134(String q134) {
		Q134 = q134;
	}

	public String getQ14() {
		return Q14;
	}

	public void setQ14(String q14) {
		Q14 = q14;
	}

	public String getQ15() {
		return Q15;
	}

	public void setQ15(String q15) {
		Q15 = q15;
	}

	public String getMARK1() {
		return MARK1;
	}

	public void setMARK1(String mARK1) {
		MARK1 = mARK1;
	}
}
