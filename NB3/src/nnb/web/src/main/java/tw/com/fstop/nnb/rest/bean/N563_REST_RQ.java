package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N563_REST_RQ extends BaseRestBean_FX implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7728355628362254793L;
	
	private String	CUSIDN	; //統一編號
	private String	CMSDATE	; //查詢起期
	private String	CMEDATE	; //查詢迄期
	private String  USERDATA;
	private String  OKOVNEXT; //下載用
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getCMSDATE() {
		return CMSDATE;
	}
	public void setCMSDATE(String cMSDATE) {
		CMSDATE = cMSDATE;
	}
	public String getCMEDATE() {
		return CMEDATE;
	}
	public void setCMEDATE(String cMEDATE) {
		CMEDATE = cMEDATE;
	}
	public String getUSERDATA() {
		return USERDATA;
	}
	public void setUSERDATA(String uSERDATA) {
		USERDATA = uSERDATA;
	}
	public String getOKOVNEXT() {
		return OKOVNEXT;
	}
	public void setOKOVNEXT(String oKOVNEXT) {
		OKOVNEXT = oKOVNEXT;
	}
	
}
