package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.Map;

public class DoIDGateCommand_REST_RQ extends BaseRestBean_COM_IDGATE implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8105756062267887930L;
	
	
	public DoIDGateCommand_REST_RQ(String ms_Channel) {
		setMs_Channel(ms_Channel);
	}
	
	private Map<String,String> IN_DATAS; //驗證資料


	public Map<String, String> getIN_DATAS() {
		return IN_DATAS;
	}

	public void setIN_DATAS(Map<String, String> iN_DATAS) {
		IN_DATAS = iN_DATAS;
	}
}
