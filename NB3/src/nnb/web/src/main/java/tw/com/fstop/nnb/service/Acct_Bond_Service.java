package tw.com.fstop.nnb.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.NumericUtil;

@Service
public class Acct_Bond_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private I18n i18n;

	
	/**
	 * 外匯匯入匯款查詢結果
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult bond_balance_result(String cusidn,Map<String, String> reqParam)	{
		log.trace("bond_balance_result_service");
		BaseResult bs = null ;
		try {
			bs = new BaseResult();
			bs = N870_REST(cusidn, reqParam);
			if(bs!=null && bs.getResult()) {
				Map<String , Object> callRow = (Map) bs.getData();
				ArrayList<Map<String , Object>> callTable = (ArrayList<Map<String , Object>>) callRow.get("REC");
				
				for (Map<String, Object> table : callTable) {
					ArrayList<Map<String , String>> callTableList = (ArrayList<Map<String , String>>) table.get("Table");

					for (Map<String, String> data : callTableList) {
					//欄位格式化
						data.put("DPIBAL",NumericUtil.fmtAmount(data.get("DPIBAL"),0));
						data.put("LMTSFI",NumericUtil.fmtAmount(data.get("LMTSFI"),0));
						data.put("RPBAL",NumericUtil.fmtAmount(data.get("RPBAL"),0));
						data.put("LMTSFO",NumericUtil.fmtAmount(data.get("LMTSFO"),0));
						data.put("TSFBAL_N870",NumericUtil.fmtAmount(data.get("TSFBAL_N870"),0));
					}
				}

				bs.addData("ACN1", reqParam.get("ACN1"));
			}
			bs.setResult(true);
		}catch(Exception e) {
//			e.printStackTrace();
			log.error("time_deposit",e);
		}	
		return  bs;
	}
	
	/**
	 * 中央登錄債券明細結果頁
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult bond_detail_result(String cusidn,Map<String, String> reqParam)	{
		log.trace("bond_detail_result_service");
		BaseResult bs = null ;
		try {
			bs = new BaseResult();
//			bs = N871_REST(cusidn, reqParam);
			
			String acn = "".equals(reqParam.get("ACN")) ? "99999999999" : reqParam.get("ACN");
//			LinkedHashMap<String, Object> listmap = new LinkedHashMap<String, Object>();
			String msgCode = "ENRD";
			String msgName = i18n.getMsg("LB.Check_no_data");
			// TODO查詢全部
			LinkedList<Object> labelList = new LinkedList<>();
			if ("99999999999".equals(acn)){
				log.trace("全部查詢");
				bs = N871_REST(cusidn, reqParam);
				if(bs!=null && bs.getResult()) {
					Map<String, Object> dataMap = (Map) bs.getData();
					ArrayList<Map<String, String>> rowListMap = (ArrayList<Map<String, String>>) dataMap.get("REC");
					log.trace("rowListMap>>>=={}", rowListMap);
					ArrayList<Map<String , String>> callRec = new ArrayList<Map<String , String>>();
					LinkedHashMap<String, Object> listmap = new LinkedHashMap<String, Object>();
					
					for (Map<String, String> map : rowListMap) {
						//欄位格式化
						map.put("DPIBAL",NumericUtil.fmtAmount(map.get("DPIBAL"),0));
						map.put("LMTSFI",NumericUtil.fmtAmount(map.get("LMTSFI"),0));
						map.put("RPBAL",NumericUtil.fmtAmount(map.get("RPBAL"),0));
						map.put("LMTSFO",NumericUtil.fmtAmount(map.get("LMTSFO"),0));
						//日期轉換
						map.put("PAYDATE", DateUtil.convertDate(2, map.get("PAYDATE"), "yyyMMdd", "yyy/MM/dd"));
						callRec.add(map);
					}
					rowListMap = callRec;
					//總筆數
					int COUNT = 0;
					
					// 取得全部的ACN並放入List
					Set<String> acnAllList = new HashSet<String>();
					for (Map<String, String> map : rowListMap) {
						map.get("ACN");
						acnAllList.add(map.get("ACN"));
					}
					log.trace("acnAllList>>{}", acnAllList);
					//移除空資料
					for (int i = 0;i < rowListMap.size();i++) {
//						log.debug("BONCOD >> {}",rowListMap.get(i).get("BONCOD"));
						if(rowListMap.get(i).get("BONCOD").equals("")) {
							rowListMap.remove(i);
							i--;
						}
					}
					
					for (String strAcn : acnAllList){
						LinkedHashMap<String, Object> labelMap = new LinkedHashMap<String, Object>();
						LinkedList<Object> listAcnMap = new LinkedList<>();
						for (Map map : rowListMap) {
							if (strAcn.equals(map.get("ACN"))) {
								listAcnMap.add(map);
							}
						}
						COUNT = COUNT + listAcnMap.size();
						if(listAcnMap.size() == 0) {
							//若查無資料
							labelMap.put("TOPMSG", msgCode);
							labelMap.put("ADMSGOUT", msgName);
							
							listmap.put("PAYDATE", msgCode);
							listmap.put("BONCOD", msgName);
							listmap.put("TRNTYP", " ");
							listmap.put("OUTAMT", " ");
							listmap.put("INPAMT", " ");
							listmap.put("DPIBAL", " ");
							listmap.put("LMTSFO", " ");
							listmap.put("LMTSFI", " ");
							listmap.put("RPBAL", " ");
							listAcnMap.add(listmap);
						}
						labelMap.put("ACN", strAcn);
						labelMap.put("rowListMap", listAcnMap);
						labelList.add(labelMap);
					}
					//塞入總筆數
					bs.addData("CMRECNUM", String.valueOf(COUNT));
					//遇到繼續查詢
					String ACN = reqParam.get("ACN");
					String CMSDATE = reqParam.get("CMSDATE");
					String CMEDATE = reqParam.get("CMEDATE");
					String USERDATA_X50 = reqParam.get("USERDATA_X50");
					bs.addData("ACN",ACN);
					bs.addData("CMSDATE",CMSDATE);
					bs.addData("CMEDATE",CMEDATE);
					bs.addData("USERDATA_X50",USERDATA_X50);
					
					bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));					
					bs.addData("labelList", labelList);
				}
			}
			//一般選擇
			else {log.trace("單筆查詢");
				LinkedHashMap<String, Object> labelMap = new LinkedHashMap<String, Object>();
				bs = N871_REST(cusidn, reqParam);
				if(bs!=null && bs.getResult()) {
					Map<String, Object> dataMap = (Map) bs.getData();
					ArrayList<Map<String, String>> rowListMap = (ArrayList<Map<String, String>>) dataMap.get("REC");
					log.trace("rowListMap>>>==1{}", rowListMap);
					ArrayList<Map<String , String>> callRec = new ArrayList<Map<String , String>>();
					Map<String, String> listmap = new HashMap<String, String>();

					//移除空資料
					for (int i = 0;i < rowListMap.size();i++) {
//						log.debug("BONCOD >> {}",rowListMap.get(i).get("BONCOD"));
						if(rowListMap.get(i).get("BONCOD").equals("")) {
							rowListMap.remove(i);
							i--;
						}
					}
					for (Map<String, String> map : rowListMap) {
						//欄位格式化
						map.put("DPIBAL",NumericUtil.fmtAmount(map.get("DPIBAL"),0));
						map.put("LMTSFI",NumericUtil.fmtAmount(map.get("LMTSFI"),0));
						map.put("RPBAL",NumericUtil.fmtAmount(map.get("RPBAL"),0));
						map.put("LMTSFO",NumericUtil.fmtAmount(map.get("LMTSFO"),0));
						//日期轉換
						map.put("PAYDATE", DateUtil.convertDate(2, map.get("PAYDATE"), "yyyMMdd", "yyy/MM/dd"));
						callRec.add(map);
					}
					rowListMap = callRec;
					
					// 總筆數
					int COUNT = rowListMap.size();
					bs.addData("CMRECNUM", String.valueOf(COUNT));
					if(rowListMap.size() == 0) {
						//若查無資料
						labelMap.put("TOPMSG", msgCode);
						labelMap.put("ADMSGOUT", msgName);
						listmap.put("PAYDATE", msgCode);
						listmap.put("BONCOD", msgName);
						listmap.put("TRNTYP", " ");
						listmap.put("OUTAMT", " ");
						listmap.put("INPAMT", " ");
						listmap.put("DPIBAL", " ");
						listmap.put("LMTSFO", " ");
						listmap.put("LMTSFI", " ");
						listmap.put("RPBAL", " ");
						rowListMap.add(listmap);
					}	
					labelMap.put("ACN",acn);
					labelMap.put("rowListMap",rowListMap);
					labelList.add(labelMap);
					bs.addData("labelList", labelList);
					
					//遇到繼續查詢
					String ACN = reqParam.get("ACN");
					String CMSDATE = reqParam.get("CMSDATE");
					String CMEDATE = reqParam.get("CMEDATE");
					String USERDATA_X50 = reqParam.get("USERDATA_X50");
					bs.addData("ACN",ACN);
					bs.addData("CMSDATE",CMSDATE);
					bs.addData("CMEDATE",CMEDATE);
					bs.addData("USERDATA_X50",USERDATA_X50);
				}
			}
			
		}catch(Exception e) {
//			e.printStackTrace();
			log.error("time_deposit",e);
		}	
		return  bs;
	}
	
	/**
	 * 中央登錄債券明細直接下載
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public BaseResult bondDetailDirectDownload(String cusidn,Map<String,String> reqParam){
		BaseResult bs = null;
		try{
			bs = bond_detail_result(cusidn,reqParam);

			if(bs != null && bs.getResult()){
				Map<String,Object> dataMap = (Map<String,Object>)bs.getData();
				log.trace("dataMap={}",dataMap);
				
				Map<String, Object> parameterMap = new HashMap<String, Object>();
				
				List<Map<String,Object>> labelListMap = (List<Map<String,Object>>)dataMap.get("labelList");
				log.trace("labelListMap={}",labelListMap);
			
				parameterMap.put("downloadFileName", reqParam.get("downloadFileName"));
				parameterMap.put("downloadType", reqParam.get("downloadType"));
				parameterMap.put("templatePath", reqParam.get("templatePath"));
				parameterMap.put("hasMultiRowData", reqParam.get("hasMultiRowData"));
				
				String downloadType = reqParam.get("downloadType");
				log.trace(ESAPIUtil.vaildLog("downloadType >> " + downloadType)); 

				// EXCEL和OLDEXCEL下載
				if ("EXCEL".equals(downloadType) || "OLDEXCEL".equals(downloadType))
				{
					parameterMap.put("headerRightEnd",reqParam.get("headerRightEnd"));
					parameterMap.put("headerBottomEnd",reqParam.get("headerBottomEnd"));
					parameterMap.put("multiRowStartIndex",reqParam.get("multiRowStartIndex"));
					parameterMap.put("multiRowEndIndex",reqParam.get("multiRowEndIndex"));
					parameterMap.put("multiRowCopyStartIndex",reqParam.get("multiRowCopyStartIndex"));
					parameterMap.put("multiRowCopyEndIndex",reqParam.get("multiRowCopyEndIndex"));
					parameterMap.put("multiRowDataListMapKey",reqParam.get("multiRowDataListMapKey"));
					parameterMap.put("rowRightEnd", reqParam.get("rowRightEnd"));
				}
				// TXT下載
				else
				{
					parameterMap.put("txtHeaderBottomEnd",reqParam.get("txtHeaderBottomEnd"));
					parameterMap.put("txtMultiRowStartIndex",reqParam.get("txtMultiRowStartIndex"));
					parameterMap.put("txtMultiRowEndIndex",reqParam.get("txtMultiRowEndIndex"));
					parameterMap.put("txtMultiRowCopyStartIndex",reqParam.get("txtMultiRowCopyStartIndex"));
					parameterMap.put("txtMultiRowCopyEndIndex",reqParam.get("txtMultiRowCopyEndIndex"));
					parameterMap.put("txtMultiRowDataListMapKey",reqParam.get("txtMultiRowDataListMapKey"));
					parameterMap.put("txtHasFooter",reqParam.get("txtHasFooter"));
				}
				bs.addData("parameterMap", parameterMap);
			}
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("bondDetailDirectDownload error >> {}", e);
		}
		return bs;
	}
}
