package tw.com.fstop.nnb.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import fstop.orm.po.ADMCURRENCY;
import fstop.orm.po.SYSPARAMDATA;
import fstop.orm.po.TXNBONDDATA;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.AdmCurrencyDao;
import tw.com.fstop.tbb.nnb.dao.AdmHolidayDao;
import tw.com.fstop.tbb.nnb.dao.SysParamDataDao;
import tw.com.fstop.tbb.nnb.dao.TxnBondDataDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;
import tw.com.fstop.tbb.nnb.util.StrUtils;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.WebUtil;

/**
 * 海外債申購 Service
 */
@Service
public class Bond_Purchase_Service extends Base_Service{
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	
	@Autowired
	private AdmHolidayDao admHolidayDao;
	
	@Autowired
	private SysParamDataDao sysParamDataDao;
	
	@Autowired
	private AdmCurrencyDao admCurrencyDao;
	
	@Autowired
	private TxnBondDataDao txnBondDataDao;
	
	@Autowired
	private I18n i18n;
	
	/**
	 * 檢核2. 檢核有無超過交易時間
	 * @return
	 */
	public boolean inTradingHour() {
		
		Date date = new Date();
		String nowTime = new SimpleDateFormat("HHmmss").format(date);
		
		
		SYSPARAMDATA sysParamData = sysParamDataDao.getByPK("NBSYS");
		String bondStratTime = sysParamData.getADBDSHH() + sysParamData.getADBDSMM() + sysParamData.getADBDSSS();
		String bondEndTime = sysParamData.getADBDEHH() + sysParamData.getADBDEMM() + sysParamData.getADBDESS();
		
		//資料庫Alter前暫時寫死 9:00 ~ 14:30 
//		String bondStratTime = "090000";
//		String bondEndTime = "183000";
		
		boolean isHoliday = admHolidayDao.isAdmholiday();
		
		//   ( 假日 || 不在營業時間內 )
		if( isHoliday || (nowTime.compareTo(bondStratTime) <= 0 || nowTime.compareTo(bondEndTime) >= 0)) {
			return false;
		}
		
		return true;
	}
	
	public BaseResult input_page_data1(BaseResult bs) {
		Map<String,Object> bsData = (Map<String,Object>)bs.getData();
		
		//頁面所需值  客戶姓名( 遮罩 ) , 投資屬性 ( 多語系 ), 是否為專業投資人 ( 多語系 )
		String name = StrUtils.isNotEmpty((String) bsData.get("CUSNAME"))?(String)bsData.get("CUSNAME"):(String)bsData.get("NAME");
		bs.addData("hiddenNAME", WebUtil.hideusername1Convert(name));
		
		String FDINVTYPE_SHOW = "";
		if("1".equals(bsData.get("FDINVTYPE"))){
			//積極型
			FDINVTYPE_SHOW += i18n.getMsg("LB.D0945");
		}
		else if("2".equals(bsData.get("FDINVTYPE"))){
			//穩健型
			FDINVTYPE_SHOW += i18n.getMsg("LB.X1766");
		}
		else{
			//保守型
			FDINVTYPE_SHOW += i18n.getMsg("LB.X1767");
		}
		
		if(StrUtil.isNotEmpty((String) bsData.get("RISK7"))) {
			FDINVTYPE_SHOW += "　"+"（專業投資人）" ; 
		}
		bs.addData("FDINVTYPE_SHOW",FDINVTYPE_SHOW);
		
		return bs;
	}
	
	
	public BaseResult input_page_data2(Map<String,String> reqParam) {
		
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			
			String selected_BONDCODE= (String) CodeUtil.fromJson(reqParam.get("BONDDATA"), Map.class).get("BONDCODE");
			reqParam.put("PRDNO", selected_BONDCODE);
			
			log.debug("B018_REST reqParam >> {}",reqParam);
			
			bs = B018_REST(reqParam);
			
		} catch (Exception e) {
			log.error("Bond_Purchase_Service input_page_data2 error");
		}
		return bs;
	}
	
	
	public BaseResult confirm_page_data(BaseResult bs) {
		Map<String,Object> bsData = (Map<String,Object>)bs.getData();
		
		String locale = LocaleContextHolder.getLocale().toString();
		ADMCURRENCY admCurrency = admCurrencyDao.getByPK((String) bsData.get("BONDCRY"));
		if(admCurrency != null){
			String ADCCYNAME = "";
			if(locale.equals("zh_CN")) {
				ADCCYNAME = admCurrency.getADCCYCHSNAME();
			}
			else if(locale.equals("en")) {
				ADCCYNAME = admCurrency.getADCCYENGNAME();
			}
			else {
				ADCCYNAME = admCurrency.getADCCYNAME();
			}
			bs.addData("BONDCRY_SHOW",ADCCYNAME);
		}
		
		
		
		bs.addData("AMOUNT_FMT",NumericUtil.formatNumberString((String) bsData.get("AMOUNT"), 0));
		bs.addData("O09_FMT",NumericUtil.formatNumberString((String) bsData.get("O09"), 2));
		bs.addData("O10_FMT",NumericUtil.formatNumberString((String) bsData.get("O10"), 5));
		bs.addData("O11_FMT",NumericUtil.formatNumberString((String) bsData.get("O11"), 2));
		bs.addData("O12_FMT",NumericUtil.formatNumberString((String) bsData.get("O12"), 2));
		
		return bs;
	}
	
	public List<TXNBONDDATA> getBondData(String fdinvtype,String risk7){
		List<TXNBONDDATA> bondDataList = null;
		
		try{
			
			if(StrUtils.isNotEmpty(risk7)) {
			//專業投資人
				bondDataList = txnBondDataDao.getAllBondData();
			}else {
			//非專業投資人 ( 須帶入投資屬性 )
				bondDataList = txnBondDataDao.getBondDataByFDINVTYPE(fdinvtype);
			}
			
			//處理資料 : 將BONDCRY 變成多語系給頁面顯示
			//1.組合投資屬性及是否為專業投資人
			//2.將幣別依語系轉置 , 給頁面顯示
			String locale = LocaleContextHolder.getLocale().toString();
			for(TXNBONDDATA eachBond:bondDataList) {
				setCRY_SHOW(eachBond,locale);
				String MINBPRICE = eachBond.getMINBPRICE();
				eachBond.setMINBPRICE_FMT(NumericUtil.formatNumberString(MINBPRICE, 0));
				String PROGRESSIVEBPRICE = eachBond.getPROGRESSIVEBPRICE();
				eachBond.setPROGRESSIVEBPRICE_FMT(NumericUtil.formatNumberString(PROGRESSIVEBPRICE, 0));
			}
			
		}catch (Exception e) {
			log.error("Service getBondData error >> {}",e);
		}
		log.debug(ESAPIUtil.vaildLog("bondDataList=>>"+ bondDataList ));
		
		return bondDataList;
	}
	
	/**
	 * 取得幣別的資料
	 */
	public void setCRY_SHOW(TXNBONDDATA bondData,String locale){
		
		ADMCURRENCY admCurrency = admCurrencyDao.getByPK(bondData.getBONDCRY());
		if(admCurrency != null){
			String ADCCYNAME = "";
			if(locale.equals("zh_CN")) {
				ADCCYNAME = admCurrency.getADCCYCHSNAME();
			}
			else if(locale.equals("en")) {
				ADCCYNAME = admCurrency.getADCCYENGNAME();
			}
			else {
				ADCCYNAME = admCurrency.getADCCYNAME();
			}
			bondData.setBONDCRY_SHOW(ADCCYNAME);
		}
		
	}
	
	
	public static void main(String[] args) {
		String bondStratTime = "090000";
		String bondEndTime = "143000";
		String nowTime = "143000";
		System.out.println(nowTime.compareTo(bondStratTime));
		System.out.println(nowTime.compareTo(bondEndTime));
	}
}
