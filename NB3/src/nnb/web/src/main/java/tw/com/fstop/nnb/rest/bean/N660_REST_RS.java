package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N660_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1010788813572286359L;


	private String USERDATA;
	private String CUSIDN;
	private String REC_NO;
	private String DATA;
	public String getUSERDATA() {
		return USERDATA;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public String getREC_NO() {
		return REC_NO;
	}
	public String getDATA() {
		return DATA;
	}
	public void setUSERDATA(String uSERDATA) {
		USERDATA = uSERDATA;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public void setREC_NO(String rEC_NO) {
		REC_NO = rEC_NO;
	}
	public void setDATA(String dATA) {
		DATA = dATA;
	}
	
	

}
