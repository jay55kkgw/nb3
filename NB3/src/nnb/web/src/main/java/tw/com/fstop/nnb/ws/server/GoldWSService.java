package tw.com.fstop.nnb.ws.server;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.common.ResultCode;
import tw.com.fstop.nnb.rest.bean.GoldService_REST_RQ;
import tw.com.fstop.nnb.rest.bean.GoldService_REST_RS;
import tw.com.fstop.nnb.service.Base_Service;
import tw.com.fstop.nnb.ws.server.bean.InformGoldValue;
import tw.com.fstop.nnb.ws.server.bean.Gold;
import tw.com.fstop.nnb.ws.server.bean.GoldSell;
import tw.com.fstop.nnb.ws.server.bean.GoldSellProduct;
import tw.com.fstop.tbb.nnb.dao.AdmMsgCodeDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.web.util.ConfingManager;
import tw.com.fstop.web.util.RESTfulClientUtil;

@Service
public class GoldWSService {

	
	Logger log = LoggerFactory.getLogger(this.getClass());
	
//	目前在這部分Autowired AdmMsgCodeDao 會有錯誤訊息
//	@Autowired(required=true)
//    private AdmMsgCodeDao admMsgCodeDao;
	
	public GoldSell send (Gold gold) {
		log.info("send...");
		Map<String,String> reqParam = null;
		BaseResult bs = null;
		GoldSell  goldsell = null;
		List<GoldSellProduct> sellList = null;
		String ms_code = "";
		try {
			reqParam = CodeUtil.objectCovert(Map.class, gold);
			log.info("reqParam>>{}",reqParam);
			bs = GoldService_REST(reqParam);
			goldsell = new GoldSell();
			if(bs !=null) {
				Map dataMap = (Map) bs.getData();
//				因為webService 回覆 是用0000
				ms_code = bs.getMsgCode().equals("0")?"0000":bs.getMsgCode();
				goldsell.setMsg_cod(ms_code);
//				goldsell.setMsg_cod(bs.getMsgCode());
				goldsell.setMsg_desc(bs.getMessage());
				goldsell.setBank_no((String)dataMap.get("bank_no"));
				goldsell.setDate((String)dataMap.get("date"));
				goldsell.setTime((String)dataMap.get("time"));
				if(bs.getResult()) {
					goldsell.setMsg_desc("");
					log.debug("goldSellProduct>>{}",dataMap.get("goldSellProduct"));
					List<Map<String,String>> datalist = (List<Map<String,String>>) dataMap.get("goldSellProduct");
					sellList = new ArrayList<GoldSellProduct>();
					if(datalist == null || datalist.isEmpty()){
						log.warn("datalist is null or Empty..");
					}
					for(Map m:datalist) {
						GoldSellProduct gp= CodeUtil.objectCovert(GoldSellProduct.class, m);
						sellList.add(gp);
					}
					goldsell.setGoldSellProduct(sellList);
				}
			}
		} catch (Exception e) {
			log.error("GoldWSService.ERROR>>{}",e.toString());
		}
		
		return goldsell;
		
	}
	
	public BaseResult GoldService_REST(Map<String, String> reqParam) 
	{
		log.info("GoldService_REST");
		BaseResult bs = null ;
		GoldService_REST_RQ rq = null;
		GoldService_REST_RS rs = null;
		String baseuri ="";
		try {
			bs =new  BaseResult();
			//		 call REST API
			rq = new GoldService_REST_RQ();
			rq = CodeUtil.objectCovert(GoldService_REST_RQ.class,reqParam);
			
			if(ConfingManager.MS_GOLD.endsWith("/") ==false) {
				baseuri = ConfingManager.MS_GOLD+"/ws";
			}else {
				baseuri = ConfingManager.MS_GOLD+"ws";
			}
			log.debug("baseuri>>{}",baseuri);
			rs = RESTfulClientUtil.send(rq, GoldService_REST_RS.class, rq.getAPI_Name(rq.getClass() ,baseuri ) );
			log.info("GoldService_REST_RS>>{}",rs);
			if(rs != null ) {
				CodeUtil.convert2BaseResult(bs, rs);
			}else {
				log.error("GoldService_REST_RS is null");
				bs.setSYSMessage(ResultCode.SYS_ERROR_TEL);
//				this.getMessageByMsgCode(bs);
			}
		} catch (Exception e) {
			log.error("",e);
			bs.setSYSMessage(ResultCode.SYS_ERROR);
//			this.getMessageByMsgCode(bs);
		}
		return  bs;
	}
	
}
