package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N079_REST_RS extends BaseRestBean implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4363629448511668771L;

	private String FDPACN;//存單帳號
	
	private String FDPNUM;//存單號碼

	private String FDPTYPE;//存單種類

	private String AMT;//交易金額

	private String ITR;//利率

	private String INTMTH;//計息方式

	private String DPISDT;//起存日

	private String DUEDAT;//到期日

	private String INTPAY;//利息

	private String INTPAYS;//利息正負號

	private String TAX;//所得稅

	private String INTRCV;//透支息

	private String PAIAFTX;//稅後本息

	private String NHITAX;//健保費
	
	private String CMQTIME;

	public String getFDPACN()
	{
		return FDPACN;
	}

	public void setFDPACN(String fDPACN)
	{
		FDPACN = fDPACN;
	}

	public String getFDPNUM()
	{
		return FDPNUM;
	}

	public void setFDPNUM(String fDPNUM)
	{
		FDPNUM = fDPNUM;
	}

	public String getFDPTYPE()
	{
		return FDPTYPE;
	}

	public void setFDPTYPE(String fDPTYPE)
	{
		FDPTYPE = fDPTYPE;
	}

	public String getAMT()
	{
		return AMT;
	}

	public void setAMT(String aMT)
	{
		AMT = aMT;
	}

	public String getITR()
	{
		return ITR;
	}

	public void setITR(String iTR)
	{
		ITR = iTR;
	}

	public String getINTMTH()
	{
		return INTMTH;
	}

	public void setINTMTH(String iNTMTH)
	{
		INTMTH = iNTMTH;
	}

	public String getDPISDT()
	{
		return DPISDT;
	}

	public void setDPISDT(String dPISDT)
	{
		DPISDT = dPISDT;
	}

	public String getDUEDAT()
	{
		return DUEDAT;
	}

	public void setDUEDAT(String dUEDAT)
	{
		DUEDAT = dUEDAT;
	}

	public String getINTPAY()
	{
		return INTPAY;
	}

	public void setINTPAY(String iNTPAY)
	{
		INTPAY = iNTPAY;
	}

	public String getINTPAYS()
	{
		return INTPAYS;
	}

	public void setINTPAYS(String iNTPAYS)
	{
		INTPAYS = iNTPAYS;
	}

	public String getTAX()
	{
		return TAX;
	}

	public void setTAX(String tAX)
	{
		TAX = tAX;
	}

	public String getINTRCV()
	{
		return INTRCV;
	}

	public void setINTRCV(String iNTRCV)
	{
		INTRCV = iNTRCV;
	}

	public String getPAIAFTX()
	{
		return PAIAFTX;
	}

	public void setPAIAFTX(String pAIAFTX)
	{
		PAIAFTX = pAIAFTX;
	}

	public String getNHITAX()
	{
		return NHITAX;
	}

	public void setNHITAX(String nHITAX)
	{
		NHITAX = nHITAX;
	}

	public String getCMQTIME()
	{
		return CMQTIME;
	}

	public void setCMQTIME(String cMQTIME)
	{
		CMQTIME = cMQTIME;
	}
	
	

	
}
