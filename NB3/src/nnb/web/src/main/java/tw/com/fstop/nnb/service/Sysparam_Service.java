package tw.com.fstop.nnb.service;

import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.google.gson.Gson;

import fstop.orm.po.TXNCSSSLOG;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.tbb.nnb.dao.SysParamDao;
import tw.com.fstop.tbb.nnb.dao.TxnCsssLogDao;
import tw.com.fstop.util.SessionUtil;

@Service
public class Sysparam_Service {
	private Logger log = LoggerFactory.getLogger(this.getClass().getName());
	
	@Autowired
	SysParamDao sysParamDao;

//	
//	public BaseResult add(Map<String,String> reqParam,Model model) {
//		BaseResult bs = new BaseResult();
//		Gson gson = new Gson();
//		try {
//			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
//			String cusidn = new String(Base64.getDecoder().decode(reqParam.get("CUSIDN")));
//			String json = gson.toJson(reqParam, Map.class);
//			TXNCSSSLOG saveobj =  gson.fromJson(json, TXNCSSSLOG.class);			
//			saveobj.setCUSIDN(cusidn);
//			saveobj.setLASTDATE(getDate());
//			saveobj.setLASTTIME(getTime());
//			
//			txnCsssLogDao.save(saveobj);
//			
//			log.debug("saveobj>>>{}",saveobj);
//			
//			bs.setResult(Boolean.TRUE);
//			bs.setMessage("0", "新增成功");
//		}catch(Exception e) {
//			log.error("{}", e);
//			bs.setResult(Boolean.FALSE);
//			bs.setMessage("1", "新增發生意料外的異常");
//		}
//		return bs;
//	}
//	
//	
//	private String getDate() {
//		Date today =new Date();
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
//		return sdf.format(today);
//	}
//	private String getTime() {
//		Date today =new Date();
//		SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");
//		return sdf.format(today);
//	}
//	
//	
//	/** 
//	*	是否在今年填寫過客戶滿意度問卷調查 或是 選擇三次的下次再填 
//	*	@return Boolean 不需要再填false  需要填寫true
//	*/  
//	public Boolean isFill(String cusidn) {
//		String year = getYear();
//		Boolean result = true;
//		if(txnCsssLogDao.isFill(cusidn,year,"N")||txnCsssLogDao.isFill(cusidn,year,"Y")) {
//			result = false;
//		}
//		return result;
//	}
//
//
//	private String getYear() {
//		Date today = new Date();
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
//		String  year = sdf.format(today);
//		return year;
//	}
}
