package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N1014_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5260294410547687310L;	

	private	String CUSIDN;
	private String FORM;//中/英文證明
	private String CRY;//幣別
	private String DATE;//日期
	private String HEADER;
	private String DATA;//申請成功訊息
	private String OFFSET;
	private String INQ_TYPE;//證明書種類
	private String OUTACN;//扣帳帳號
	private String ACN;//帳號
	private String AMT;//金額
	private String PURPOSE;//申請用途
	private String COPY;//申請份數
	private String MAILADDR;//郵寄地址
	private	String DATE1;//日期
	private	String TAKE_TYPE;//領取方式
	private	String FEAMT;//手續費

	
	
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getFORM() {
		return FORM;
	}
	public void setFORM(String fORM) {
		FORM = fORM;
	}
	public String getCRY() {
		return CRY;
	}
	public void setCRY(String cRY) {
		CRY = cRY;
	}
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getHEADER() {
		return HEADER;
	}
	public void setHEADER(String hEADER) {
		HEADER = hEADER;
	}
	public String getDATA() {
		return DATA;
	}
	public void setDATA(String dATA) {
		DATA = dATA;
	}
	public String getOFFSET() {
		return OFFSET;
	}
	public void setOFFSET(String oFFSET) {
		OFFSET = oFFSET;
	}
	public String getINQ_TYPE() {
		return INQ_TYPE;
	}
	public void setINQ_TYPE(String iNQ_TYPE) {
		INQ_TYPE = iNQ_TYPE;
	}
	public String getOUTACN() {
		return OUTACN;
	}
	public void setOUTACN(String oUTACN) {
		OUTACN = oUTACN;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getAMT() {
		return AMT;
	}
	public void setAMT(String aMT) {
		AMT = aMT;
	}
	public String getPURPOSE() {
		return PURPOSE;
	}
	public void setPURPOSE(String pURPOSE) {
		PURPOSE = pURPOSE;
	}
	public String getCOPY() {
		return COPY;
	}
	public void setCOPY(String cOPY) {
		COPY = cOPY;
	}
	public String getMAILADDR() {
		return MAILADDR;
	}
	public void setMAILADDR(String mAILADDR) {
		MAILADDR = mAILADDR;
	}
	public String getDATE1() {
		return DATE1;
	}
	public void setDATE1(String dATE1) {
		DATE1 = dATE1;
	}
	public String getTAKE_TYPE() {
		return TAKE_TYPE;
	}
	public void setTAKE_TYPE(String tAKE_TYPE) {
		TAKE_TYPE = tAKE_TYPE;
	}
	public String getFEAMT() {
		return FEAMT;
	}
	public void setFEAMT(String fEAMT) {
		FEAMT = fEAMT;
	}


	
	
	


}
