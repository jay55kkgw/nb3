package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;

public class N092_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3522922812919542033L;
	
	@SerializedName(value = "TRANTITLE")
	private String TRANTITLE;

	@SerializedName(value = "TRANNAME")
	private String TRANNAME;
	
	@SerializedName(value = "SVACN")
	private String SVACN;
	
	@SerializedName(value = "ACN")
	private String ACN;

	@SerializedName(value = "TRNGD_str")
	private String TRNGD_str;

	@SerializedName(value = "APPTYPE")
	private String APPTYPE;
	@Override
	public Map<String, String> coverMap(){
		Map<String, String> result = new LinkedHashMap<String, String>();
		result.put("交易名稱", this.TRANTITLE);
		result.put("交易類型", this.TRANNAME);
		if(this.APPTYPE.equals("01")) {
			result.put("交易種類", "預約黃金申購");
			result.put("臺幣轉出帳號", this.SVACN);
			result.put("黃金轉入帳號", this.ACN);
			result.put("買進公克數", this.TRNGD_str + "公克");
		}
		if(this.APPTYPE.equals("02")) {
			result.put("交易種類", "預約黃金回售");
			result.put("黃金轉出帳號", this.ACN);
			result.put("臺幣轉入帳號", this.SVACN);
			result.put("賣出公克數", this.TRNGD_str + "公克");
		}
		return result;
	}
	
	public String getTRANTITLE() {
		return TRANTITLE;
	}

	public void setTRANTITLE(String tRANTITLE) {
		TRANTITLE = tRANTITLE;
	}

	public String getTRANNAME() {
		return TRANNAME;
	}

	public void setTRANNAME(String tRANNAME) {
		TRANNAME = tRANNAME;
	}

	public String getSVACN() {
		return SVACN;
	}
	public void setSVACN(String sVACN) {
		SVACN = sVACN;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getTRNGD_str() {
		return TRNGD_str;
	}
	public void setTRNGD_str(String tRNGD_str) {
		TRNGD_str = tRNGD_str;
	}
	public String getAPPTYPE() {
		return APPTYPE;
	}
	public void setAPPTYPE(String aPPTYPE) {
		APPTYPE = aPPTYPE;
	}

}
