package tw.com.fstop.nnb.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.po.ADMBHCONTACT;
import fstop.orm.po.ADMCURRENCY;
import fstop.orm.po.ADMREMITMENU;
import fstop.orm.po.TXNFXRECORD;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.TxnFxRecordDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.StrUtil;

/**
 * 
 * 功能說明 :外匯共用
 *
 */
@Service
public class Fcy_Common_Service extends Base_Service
{

	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private DaoService daoService;

	@Autowired
	private TxnFxRecordDao txnfxrecorddao;

	@Autowired
	private I18n i18n;

	/**
	 * 判斷是否為營業時間
	 * 
	 * @return
	 */
	public BaseResult isFxBizTime()
	{
		BaseResult bs = new BaseResult();
		try
		{
			// 判斷日期
			if (!daoService.isFxBizTime())
			{
				//外匯交易超過營業時間(銀行營業日9:30-15:30)，請於營業時間內執行。
				bs.setMessage("Z401", i18n.getMsg("LB.X1830"));
			}
			else
			{
				bs.setResult(true);
			}
		}
		catch (Exception e)
		{
			log.error("isFxBizTime  >>> {}", e);
		}
		log.debug("isFxBizTime : {}", bs.getData());
		return bs;
	}


	/**
	 * 取得幣別
	 * @return
	 */
	public BaseResult getCRYList()
	{
		BaseResult bs = new BaseResult();
		List<ADMCURRENCY> admCurrencyList = new LinkedList<>();
		try
		{
			admCurrencyList = daoService.getCRYList();
			if (admCurrencyList.isEmpty())
			{
				bs.setMessage("Z099", i18n.getMsg("LB.Check_no_data"));//查無資料
			}
			else
			{
				bs.addData("REC", admCurrencyList);
				bs.setResult(true);
			}
		}
		catch (Exception e)
		{
			log.error("Fcy_Common_Service getCRYList >>>{}", e);
		}

		log.debug("Fcy_Common_Service getCRYList : {}", bs.getData());
		return bs;
	}

	/**
	 * 一般網銀分行聯絡方式資料檔擷取
	 * 
	 * @param cusidn
	 * @return
	 */
	public BaseResult getBhContactResult(String bhid)
	{
		BaseResult bs = new BaseResult();
		List<ADMBHCONTACT> admbhcontactList = new ArrayList<>();
		try
		{
			admbhcontactList = daoService.getAdmBhContact(bhid);
			if (admbhcontactList.isEmpty())
			{
				bs.setMessage("Z099", i18n.getMsg("LB.Check_no_data"));//查無資料
			}
			else
			{

				bs.addData("REC", admbhcontactList);
				bs.setResult(true);
			}
		}
		catch (Exception e)
		{
			log.error("fcy_Common_Service getBhContactResult >>>{}", e);
		}

		log.debug("fcy_Common_Service getBhContactResult : {}", bs.getData());
		return bs;
	}

	/**
	 * 修改常用選單
	 * 
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult fxRemitQueryAj(String cusidn, Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{
			log.debug(ESAPIUtil.vaildLog("@@@ reqParam(TYPE) == {}"+ reqParam.get("TYPE")));
			log.debug(ESAPIUtil.vaildLog("@@@ fxUtils request.getParameter(ADRMTID) == {}"+ reqParam.get("ADRMTID")));
			log.debug(ESAPIUtil.vaildLog("@@@ fxUtils request.getParameter(ADRMTTYPE) == {}"+ reqParam.get("ADRMTTYPE")));
			daoService.editMyRemitMenu(cusidn, reqParam.get("TYPE"), reqParam.get("ADRMTID"), reqParam.get("ADRMTTYPE"));
			Map m = new HashMap<>();
			m.put("TYPE", reqParam.get("TYPE"));
			bs.setData(m);
			bs.setResult(true);
		}
		catch (Exception e)
		{
			log.error("Fcy_Common_Service fxRemitQueryAj >>>{}", e);
		}

		log.debug(ESAPIUtil.vaildLog("Fcy_Common_Service fxRemitQueryAj : {}" + CodeUtil.toJson(bs.getData())));
		return bs;

	}

	/**
	 * 取得常用選單
	 * 
	 * @param cusidn
	 * @return
	 */
	public BaseResult MyRemitMenu(String cusidn)
	{
		BaseResult bs = new BaseResult();
		List<Map<String, String>> admCurrencyList = new LinkedList<Map<String, String>>();
		try
		{
			admCurrencyList = daoService.getMyRemitMenu(cusidn);
			for (Map<String, String> map : admCurrencyList)
			{
				map.put("display_ADRMTTYPE", ADRMTTYPEConvert(map.get("ADRMTTYPE")));
			}
			bs.addData("REC", admCurrencyList);
			bs.setResult(true);
		}
		catch (Exception e)
		{
			log.error("Fcy_Common_Service MyRemitMenu >>>{}", e);
		}

		log.debug("Fcy_Common_Service MyRemitMenu : {}", bs.getData());
		return bs;
	}

	/**
	 * 匯款分類編號查詢 (大項分類)
	 * 
	 * @param reqParam
	 * @return
	 */
	public BaseResult FxRemitQuery(Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		List<ADMREMITMENU> admCurrencyList = new ArrayList<>();
		try
		{
			log.debug(ESAPIUtil.vaildLog("FxRemitQuery reqParam >>>{}" + CodeUtil.toJson(reqParam)));
			String str_ADRMTTYPE = reqParam.get("ADRMTTYPE");
			String str_CUTTYPE = reqParam.get("CUTTYPE");
			String NULFLAG = reqParam.get("NULFLAG");
			String CNTY = reqParam.get("CNTY");
			String REMTYPE = reqParam.get("REMTYPE");

			admCurrencyList = daoService.getRemitMenu(str_ADRMTTYPE, "", "00", "", str_CUTTYPE);
			log.debug(ESAPIUtil.vaildLog("FxRemitQuery admCurrencyList >>>>>>>>>> {}" + CodeUtil.toJson(admCurrencyList)));
			bs.addData("REC", admCurrencyList);
			bs.addData("str_ADRMTTYPE", str_ADRMTTYPE);
			bs.addData("str_CUTTYPE", str_CUTTYPE);
			bs.addData("NULFLAG", NULFLAG);
			bs.addData("CNTY", StrUtil.isEmpty(CNTY) ? "" : CNTY);
			bs.addData("REMTYPE", StrUtil.isEmpty(REMTYPE) ? "" : REMTYPE);
			bs.setResult(true);
		}
		catch (Exception e)
		{
			log.error("Fcy_Common_Service FxRemitQuery >>>{}", e);
		}

		log.debug("Fcy_Common_Service FxRemitQuery : {}", bs.getData());
		return bs;
	}

	/**
	 * 
	 * 匯款分類編號查詢 (中項分類)
	 * 
	 * @param reqParam
	 * @return
	 */
	public BaseResult FxRemitQuery_1(Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		List<ADMREMITMENU> admCurrencyList = new ArrayList<>();
		try
		{

			log.debug(ESAPIUtil.vaildLog("FxRemitQuery FxRemitQuery_1 >>>{}" + CodeUtil.toJson(reqParam)));
			String str_CUTTYPE = reqParam.get("CUTTYPE");
			String str_ADLKINDID = reqParam.get("ADLKINDID");
			String str_ADRMTTYPE = reqParam.get("ADRMTTYPE");
			String CNTY = reqParam.get("CNTY");
			String REMTYPE = reqParam.get("REMTYPE");
			admCurrencyList = daoService.getRemitMenu(str_ADRMTTYPE, str_ADLKINDID, "", "", str_CUTTYPE);

			log.debug(ESAPIUtil.vaildLog("FxRemitQuery_1 admCurrencyList >>>>>>>>>> {}"+ CodeUtil.toJson(admCurrencyList)));
			bs.addData("REC", admCurrencyList);
			bs.addData("i_Count", admCurrencyList.size());
			bs.addData("str_ADLKINDID", str_ADLKINDID);
			bs.addData("str_ADRMTTYPE", str_ADRMTTYPE);
			bs.addData("str_CUTTYPE", StrUtil.isEmpty(str_CUTTYPE) ? "" : str_CUTTYPE);
			bs.addData("CNTY", StrUtil.isEmpty(CNTY) ? "" : CNTY);
			bs.addData("REMTYPE", StrUtil.isEmpty(REMTYPE) ? "" : REMTYPE);
			bs.setResult(true);
		}
		catch (Exception e)
		{
			log.error("Fcy_Common_Service FxRemitQuery_1 >>>{}", e);
		}

		log.debug("Fcy_Common_Service FxRemitQuery_1 : {}", bs.getData());
		return bs;
	}

	/**
	 * 匯款分類編號查詢 (小項分類)
	 * 
	 * @param reqParam
	 * @return
	 */
	public BaseResult FxRemitQuery_2(Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		List<ADMREMITMENU> admCurrencyList = new ArrayList<>();
		try
		{
			log.debug(ESAPIUtil.vaildLog("FxRemitQuery_2 reqParam >>>{}"+ CodeUtil.toJson(reqParam)));
			String str_CUTTYPE = reqParam.get("CUTTYPE");
			String str_ADLKINDID = reqParam.get("ADLKINDID");
			String str_ADMKINDID = StrUtil.isEmpty(reqParam.get("ADMKINDID")) ? "" : reqParam.get("ADMKINDID").trim();
			String str_ADRMTTYPE = reqParam.get("ADRMTTYPE");
			String CNTY = reqParam.get("CNTY");
			String REMTYPE = reqParam.get("REMTYPE");
			String NULFLAG = reqParam.get("NULFLAG");
			admCurrencyList = daoService.getRemitMenu(str_ADRMTTYPE, str_ADLKINDID, str_ADMKINDID, "*", str_CUTTYPE);

			log.debug(ESAPIUtil.vaildLog("FxRemitQuery_2 admCurrencyList in >>>> {}"+ CodeUtil.toJson(admCurrencyList)));
			// 依條件移除資料
			Iterator<ADMREMITMENU> iterator = admCurrencyList.iterator();
			int count = 0;
			String showOff = "0";
			while (iterator.hasNext())
			{
				ADMREMITMENU admremitmenu = iterator.next(); // must be called before you can call i.remove()
				String adrmtid = admremitmenu.getADRMTID().trim();
				String adrmtdesc = admremitmenu.getADRMTDESC().trim();
				
				if (count == 0) {
					if ( "Y".equals(NULFLAG) && "6".equals(adrmtid.substring(0, 1)) && (str_ADRMTTYPE.equals("1")) ) {
						if(!str_ADLKINDID.equals("09")) {
							showOff = "1";
						}
					}
					if ( "Y".equals(NULFLAG) && "6".equals(adrmtid.substring(0, 1)) && (str_ADRMTTYPE.equals("2")) ) {
						if(!str_ADLKINDID.equals("09")) {
							showOff = "2";
						}
					}
				}
				count++;
				
				if ("6".equals(adrmtid.substring(0, 1)) && "02".equals(str_ADMKINDID) 
						&& !("693".equals(adrmtid) && "TW".equals(CNTY))
				) {
					iterator.remove();
				}
				// 匯出匯款&匯入解款為693，不顯示692
				if ("692".equals(adrmtdesc.substring(0, 3)) && "TW".equals(CNTY)
				) {
					iterator.remove();
				}
			}
			log.debug(ESAPIUtil.vaildLog("FxRemitQuery_2 admCurrencyList out >>>> {}"+ CodeUtil.toJson(admCurrencyList)));
			bs.addData("REC", admCurrencyList);
			bs.addData("str_CUTTYPE", StrUtil.isEmpty(str_CUTTYPE) ? "" : str_CUTTYPE);
			bs.addData("str_ADLKINDID", str_ADLKINDID);
			bs.addData("str_ADMKINDID", str_ADMKINDID);
			bs.addData("str_ADRMTTYPE", str_ADRMTTYPE);
			bs.addData("CNTY", StrUtil.isEmpty(CNTY) ? "" : CNTY);
			bs.addData("REMTYPE", StrUtil.isEmpty(REMTYPE) ? "" : REMTYPE);
			bs.addData("SHOWOFF", showOff);
			bs.setResult(true);
		}
		catch (Exception e)
		{
			log.error("Fcy_Common_Service FxRemitQuery_2 >>>{}", e);
		}

		log.debug("Fcy_Common_Service FxRemitQuery_2 : {}", bs.getData());
		return bs;
	}

	/**
	 * //取得收款行(人)相關資訊
	 * 
	 * @param cusidn
	 * @return
	 */
	public BaseResult getRcvInfo(String cusidn, String BENACC)
	{
		log.debug(ESAPIUtil.vaildLog("getRcvInfo >>{}" +  cusidn));
		log.debug(ESAPIUtil.vaildLog("getRcvInfo BENACC>>{}" + BENACC));
		BaseResult bs = new BaseResult();
		try
		{
			// 一般網銀外幣匯出匯款受款人約定檔擷取
			bs = F031_REST(cusidn);
			if (bs != null && bs.getResult())
			{
				Map<String, Object> callDataF031 = (Map) bs.getData();
				ArrayList<Map<String, String>> callF031REC = (ArrayList<Map<String, String>>) callDataF031.get("REC");
				for (Map<String, String> map : callF031REC)
				{
					// 檢查是否為轉入帳號的資料
					if (BENACC.equals(map.get("BENACC")))
					{
						bs.setData(map);
					}
					else
					{

					}
				}
			}
			else
			{
				log.info("call F031_REST bs erro >>>", bs.getData());
			}
		}
		catch (Exception e)
		{
			log.error("Fcy_Common_Service getRcvInfo >>>{}", e);
		}

		log.debug("Fcy_Common_Service getRcvInfo : {}", bs.getData());
		return bs;
	}

	/**
	 * 取得交易單據--以json格式存在資料庫
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	public BaseResult getFxcertJson(String adtxno, String cusidn)
	{
		log.trace("getFxcert...");

		BaseResult bs = new BaseResult();
		try
		{
			Map<String, String> resultMap = new HashMap<String, String>();

			// FXCERT
			TXNFXRECORD po = txnfxrecorddao.findByADTXNO(adtxno);
			log.trace(ESAPIUtil.vaildLog(("getFxcert.TXNFXRECORD: {}"+ CodeUtil.toJson(po))));

			// 交易單據資料內容
			String fxcert = po.getFXCERT();
			
			// 2.5--HTML
			if (fxcert != null && fxcert.contains("<table")) {
				bs.setMsgCode("1");
				bs.setData(parseHtmlTable(fxcert));
				
			} // 3--JSON
			else {
				resultMap = CodeUtil.fromJson(fxcert, Map.class);
			}
			
			// ****摘要內容:
			String str_MailMemo = resultMap.get("CMMAILMEMO");
			if(str_MailMemo != null) {
				StringBuffer buffer = new StringBuffer();
				if (str_MailMemo.length() <= 35)
				{
					buffer.append(str_MailMemo);
				}
				else if (str_MailMemo.length() > 35 && str_MailMemo.length() <= 70)
				{
					buffer.append(str_MailMemo.substring(0, 35));
					buffer.append("<br>");
					buffer.append(str_MailMemo.substring(35));
				}
				else if (str_MailMemo.length() > 70 && str_MailMemo.length() <= 105)
				{
					buffer.append(str_MailMemo.substring(0, 35));
					buffer.append("<br>");
					buffer.append(str_MailMemo.substring(35, 70));
					buffer.append("<br>");
					buffer.append(str_MailMemo.substring(70));
				}
				// 摘要內容<br>
				resultMap.put("str_MailMemo", buffer.toString());
			}
			
			// ****匯款附言 :
			StringBuffer buf = new StringBuffer();
				
			if (resultMap.get("MEMO1") != null)
				buf.append((String)resultMap.get("MEMO1"));
			if (resultMap.get("MEMO2") != null)
				buf.append("<br>"+(String)resultMap.get("MEMO2"));
			if (resultMap.get("MEMO3") != null)
				buf.append("<br>"+(String)resultMap.get("MEMO3"));			
			if (resultMap.get("MEMO4") != null)
				buf.append("<br>"+(String)resultMap.get("MEMO4"));

			// 匯款附言 有<br>:
			resultMap.put("str_Memo1", buf.toString());
			bs.addAllData(resultMap);

			// 功能代號決定target
			String adopid = po.getADOPID();
			bs.setNext(adopid);

		}
		catch (Exception e)
		{
			log.error("getCurrency.error: {}", e);
		}

		log.debug(ESAPIUtil.vaildLog("resultMap: {}"+ CodeUtil.toJson(bs)));

		return bs;
	}

	/**
	 * 取得交易單據
	 * 
	 * @param adtxno
	 * @param cusidn
	 * @return
	 */
	public BaseResult getFxcert(String adtxno, String cusidn)
	{
		log.trace("getFxcert...");

		BaseResult bs = new BaseResult();
		try
		{
			// FXCERT
			TXNFXRECORD po = txnfxrecorddao.findByADTXNO(adtxno);
			log.trace(ESAPIUtil.vaildLog("getFxcert.TXNFXRECORD: {}"+CodeUtil.toJson(po)));
			String fxcert = po.getFXCERT();
			// 回傳結果
			bs.addData("fxcert", fxcert);
		}
		catch (Exception e)
		{
			log.error("getFxcert.error: {}", e);
		}

		log.debug("getFxcert: {}", bs.getData());

		return bs;
	}

	// 將DB的數字轉換為顯示
	// ADRMTTYPE.1=匯出
	// ADRMTTYPE.2=匯入
	public String ADRMTTYPEConvert(String ADRMTTYPE)
	{
		switch (ADRMTTYPE)
		{
			case "1":
				// ADRMTTYPE = "匯出";
				ADRMTTYPE = i18n.getMsg("LB.X1831");
				break;
			case "2":
				// ADRMTTYPE = "匯入";
				ADRMTTYPE = i18n.getMsg("LB.X1832");
				break;
			default:
				break;
		}
		return ADRMTTYPE;
	}

	
	/**
	 * 取得交易單據
	 * 
	 * @param adtxno
	 * @param cusidn
	 * @return
	 */
	public String parseHtmlTable(String fxcert) {
		log.trace("parseHtmlTable...");
		String result = "";

		try {
			// FXCERT
			Document doc = Jsoup.parse(fxcert, "UTF-8");
			Elements tableElements = doc.select("table");
			result = tableElements != null ? tableElements.toString() : "";
	
		} catch (Exception e) {
			log.error("getFxcert.error: {}", e);
		}

		return result;
	}
	
}
