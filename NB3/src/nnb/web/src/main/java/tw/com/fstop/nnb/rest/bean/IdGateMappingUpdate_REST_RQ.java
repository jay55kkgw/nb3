package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class IdGateMappingUpdate_REST_RQ extends BaseRestBean_IDGATE implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4554065311884526127L;
	
	private String CUSIDN;
	
	private String IDGATEID;
	
	private String DEVICEID;
	
	private String TYPE;
	
	private String DEVICENAME;
	
	
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getTYPE() {
		return TYPE;
	}
	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}
	public String getIDGATEID() {
		return IDGATEID;
	}
	public void setIDGATEID(String iDGATEID) {
		IDGATEID = iDGATEID;
	}
	public String getDEVICEID() {
		return DEVICEID;
	}
	public void setDEVICEID(String dEVICEID) {
		DEVICEID = dEVICEID;
	}
	public String getDEVICENAME() {
		return DEVICENAME;
	}
	public void setDEVICENAME(String dEVICENAME) {
		DEVICENAME = dEVICENAME;
	}
	
	
	
	

}
