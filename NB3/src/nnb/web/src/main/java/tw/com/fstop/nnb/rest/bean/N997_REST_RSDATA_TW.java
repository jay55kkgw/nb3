package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N997_REST_RSDATA_TW extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7445173263967145749L;

	String ACN;
	String SEQ;
	String AMTORLN;
	String BAL;
	String ITR;
	String DATFSLN;
	String DDT;
	String AWT;
	String DATITPY;
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getSEQ() {
		return SEQ;
	}
	public void setSEQ(String sEQ) {
		SEQ = sEQ;
	}
	public String getAMTORLN() {
		return AMTORLN;
	}
	public void setAMTORLN(String aMTORLN) {
		AMTORLN = aMTORLN;
	}
	public String getBAL() {
		return BAL;
	}
	public void setBAL(String bAL) {
		BAL = bAL;
	}
	public String getITR() {
		return ITR;
	}
	public void setITR(String iTR) {
		ITR = iTR;
	}
	public String getDATFSLN() {
		return DATFSLN;
	}
	public void setDATFSLN(String dATFSLN) {
		DATFSLN = dATFSLN;
	}
	public String getDDT() {
		return DDT;
	}
	public void setDDT(String dDT) {
		DDT = dDT;
	}
	public String getAWT() {
		return AWT;
	}
	public void setAWT(String aWT) {
		AWT = aWT;
	}
	public String getDATITPY() {
		return DATITPY;
	}
	public void setDATITPY(String dATITPY) {
		DATITPY = dATITPY;
	}
}
