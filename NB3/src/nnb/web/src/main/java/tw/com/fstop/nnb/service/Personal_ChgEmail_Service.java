package tw.com.fstop.nnb.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.util.DateUtil;

@Service
public class Personal_ChgEmail_Service extends Base_Service {

	@Autowired DaoService daoservice ;
	
	public BaseResult chg_mail(Map<String, String> reqParam) {
		log.trace("chg_mail");
		BaseResult bs = null;

		if (!(reqParam.get("UID").equals(reqParam.get("DPUSERID")))) {
			bs.setResult(false);
			bs.setMsgCode("Z627");
			getMessageByMsgCode(bs);
			return bs;
		}

		try {
			bs = new BaseResult();
			bs = N930_TX_REST(reqParam);
			bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));

			if(bs.getResult()) {
				Map<String,String> data = (Map<String, String>) bs.getData();
				String mail = data.get("DPMYEMAIL").toLowerCase();
				try {
					daoservice.updateMail(reqParam.get("DPUSERID"), mail);
				} catch (Throwable e) {
					log.error("updateMail.error>>{}",e);
				}
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("chg_mail error >> {}",e);
		}

		return bs;
	}

	public BaseResult get_addbkInfo(String sessionId, String pw) {
		// log.trace("get_addbk");
		BaseResult bs = null;

		try {
			bs = new BaseResult();
			// bs = call_N930ea_query(sessionId, pw);
			bs.addData("_CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("get_addbkInfo error >> {}",e);
		}

		return bs;
	}
}
