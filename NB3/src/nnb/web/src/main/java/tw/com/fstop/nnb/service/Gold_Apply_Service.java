package tw.com.fstop.nnb.service;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTimeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.po.ADMBRANCH;
import fstop.orm.po.SYSPARAMDATA;
import fstop.orm.po.TXNUSER;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.AdmBranchDao;
import tw.com.fstop.tbb.nnb.dao.SysParamDataDao;
import tw.com.fstop.tbb.nnb.dao.TxnUserDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.web.util.ConfingManager;

@Service
public class Gold_Apply_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired 
	TxnUserDao txnUserDao;
	@Autowired 
	AdmBranchDao admBranchDao;
	@Autowired
	private SysParamDataDao sysParamDataDao;
	@Autowired
	I18n i18n;
	
	public String amlChecked(String cusidn,Map<String, String> reqParam) {
		String result = "";
		String BRHNAME = "";
		String BRHTEL = "";
		String branchId = "";
		Boolean amlmsg = false;
		try {
			if(reqParam.get("SVACN").length()>=3)
				branchId=reqParam.get("SVACN").substring(0,3);  		
			TXNUSER user = null;
			user = (TXNUSER)txnUserDao.findByUserId(cusidn).get(0);		
			if(user==null)
			{
				log.warn("User ID not found = " + cusidn);
			}
			else
			{
				if(branchId.length()==0)
				{
					branchId = user.getADBRANCHID() == null ? "" : user.getADBRANCHID();
				}
			}
			ADMBRANCH bhContact = admBranchDao.findByBhID(branchId).get(0);
			BRHNAME = bhContact.getADBRANCHNAME();		
			BRHTEL = bhContact.getTELNUM();
			
			reqParam.put("CUSIDN", cusidn);
			BaseResult n960 = N960_REST_UPDATE_CUSNAME(reqParam);
			Map<String,Object> callData_n960 = (Map<String,Object>)n960.getData();
			if(callData_n960.get("NAME") != null) {
				Hashtable amlparams = new Hashtable();	
				amlparams.put("name", callData_n960.get("NAME"));
				String bithdate = DateUtil.convertDateNP0(1, reqParam.get("CMDATE1"), "yyyy/MM/dd", "yyyMMdd"); 
				amlparams.put("year", bithdate.substring(0, 3));
				amlparams.put("month", bithdate.substring(3, 5));
				amlparams.put("day", bithdate.substring(5, 7));
//				amlparams.put("businessUnit", "H57");
				//修正黃金AML參數
				amlparams.put("BUSINESSUNIT", "H07");
				amlparams.put("branchId", branchId);
				amlparams.put("UID", cusidn);
				
				amlparams.put("NAME", callData_n960.get("NAME"));
				amlparams.put("BIRTHDAY", bithdate);
				amlparams.put("BRHCOD", branchId);
				amlparams.put("ADOPID", "NA50");

				BaseResult aml=AML_REST(amlparams,ConfingManager.MS_GOLD);
				log.debug("AML DATA >>{}",CodeUtil.toJson(aml));
				String amlstr = ((Map<String,Object>)aml.getData()).get("RESULT") != null ? (String)((Map<String,Object>)aml.getData()).get("RESULT") : "true";
				log.debug("AML msg >> {}", amlmsg);
				
				amlmsg = amlstr.equals("F") ? true: false;
				//result=amlmsg==true ? "交易失敗!如您有任何疑問，請洽您所往來的"+BRHNAME+"(聯絡電話："+BRHTEL+")詢問，謝謝。" : ""; 
				result=amlmsg==true ? i18n.getMsg("LB.X1881")+BRHNAME+"("+i18n.getMsg("LB.D0539")+"："+BRHTEL+")"+i18n.getMsg("LB.X1882") : ""; 
			}
		}catch(Exception e) {
//			e.printStackTrace();
			log.error("time_deposit",e);
		}
		return result;
	}
	
	public BaseResult gold_account_apply_p3(String cusidn,Map<String,String> reqParam, String kyc) {
		log.trace("gold_account_apply_p3");
		BaseResult bs = null ;
		try {
			bs = new BaseResult();
			bs = N927_REST(cusidn);
			Map<String,Object> callTabel = (Map<String,Object>)bs.getData();
			String str_RISK = (String)callTabel.get("RISK");   //投資屬性
			String GETLTD = (String)callTabel.get("GETLTD");  //KYC問卷填寫日期
			GETLTD = GETLTD == null ? "": GETLTD;
			str_RISK = str_RISK == null ? "": str_RISK;
			String time1="";
			String time2="";
			int day = 0;
			if(!GETLTD.equals("")) {
				time1 = DateUtil.getDate("/");
				time2 = DateUtil.convertDate(1, GETLTD, "yyyMMdd", "yyyy/MM/dd");
				day = DateUtil.getDiffDate(time1,time2,"yyyy/MM/dd");
			}
			//取得問卷版本日期
			SYSPARAMDATA po = null; 
		    String KYCDATE = "";
			try{
				po = sysParamDataDao.getByPK("NBSYS");
				KYCDATE = po.getKYCDATE().trim();
			}
			catch(Exception e){
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("gold_account_apply_p3 error >> {}",e);
			}
			log.debug("@@@ UID/RISK="+cusidn+"/"+str_RISK+"/GETLTD="+GETLTD+"/time1="+time1+"/time2="+time2+"/DAY="+day+"/KYCDATE="+KYCDATE);
		    bs.reset();
			bs.addData("Gd_InvAttr", "0");
			log.debug("KYC >> {}", kyc);
			if(kyc.equals("")) {   //因KYC提醒後，判斷是否繼續交易，有PASS就不會進來這
				log.debug("str_RISK ft >> {}", str_RISK.equals(""));
				log.debug("GETLTD ft >> {}", !GETLTD.equals("") && day>=365);
				log.debug("KYCDATE ft >> {}", !KYCDATE.equals("") && (KYCDATE.equals("")? 0 : Integer.parseInt(KYCDATE)) > (GETLTD.equals("") ? 1 : Integer.parseInt(GETLTD)));
				if(str_RISK.equals("") || (!GETLTD.equals("") && day>=365) || (!KYCDATE.equals("") && (KYCDATE.equals("")? 0 : Integer.parseInt(KYCDATE)) > (GETLTD.equals("") ? 1 : Integer.parseInt(GETLTD)))){
					bs.addData("TXID", "NA50");
					if(day>=365) {   //滿一年
						bs.addData("ALERTTYPE","2");
					}else if(str_RISK.equals("")) {   //無投資屬性
						bs.addData("ALERTTYPE","3");
					}
					bs.addData("Gd_InvAttr", "1");
//					bs.setSYSMessage("OKLR");
				}
			}
			bs.setResult(Boolean.TRUE);
		}catch(Exception e) {
//			e.printStackTrace();
			log.error("time_deposit",e);
		}	
		return  bs;
		
	}
	
	public BaseResult gold_account_apply_result(String cusidn,Map<String, String> reqParam) {
		log.trace("gold_account_apply_result");
		BaseResult bs = null ;
		try {
			if(reqParam.get("FGTXWAY").equals("2"))
				reqParam.put("accNo",reqParam.get("accNo").substring(reqParam.get("accNo").length() - 11));
			bs = new BaseResult();
			bs = NA50_REST(cusidn, reqParam);
			bs.addData("CMTIME", DateUtil.getDate("/") + " " + DateUtil.getTheTime(":"));
			if(bs!=null && bs.getResult()) {
				
			}
		}catch(Exception e) {
//			e.printStackTrace();
			log.error("time_deposit",e);
		}	
		return  bs;
	}
	
	public BaseResult gold_trading_apply(String cusidn,Map<String, String> reqParam) {
		log.trace("gold_trading_apply start");
		BaseResult bs = new BaseResult();
		BaseResult bsN920 = new BaseResult();
		try {
			bsN920 = N920_REST(cusidn, GOLD_RESERVATION_QOERY_N920);
			
			if(bsN920!=null && bsN920.getResult() ) {
				Map<String , Object> callDataN920 = (Map) bsN920.getData();
				ArrayList<Map<String , String>> callTableN920 = (ArrayList<Map<String , String>>) callDataN920.get("REC");
				
				if(callTableN920.size() > 0) {
					bsN920.addData("ToGoFlag", "1");
				}else {
					bsN920.addData("MsgDesc", i18n.getMsg("LB.X1772"));//您尚未申請黃金存摺帳戶，請洽往來分行辦理或線上立即申請。
					bsN920.addData("TxType", "NA50");
					bsN920.addData("Trancode", "NA50");
					bsN920.addData("ToGoFlag", "0");
					bsN920.setSYSMessage("OKLR");
					bsN920.setResult(Boolean.TRUE);
				}
			}
		}catch(Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_trading_apply error >> {}",e);
		}	
		return  bsN920;
	}
	
	public BaseResult gold_trading_apply_p3(String cusidn,Map<String, String> reqParam) {
		log.trace("gold_trading_apply_p3 start");
		BaseResult svacnos = new BaseResult();
		try {
			svacnos = N920_REST(cusidn, OUT_ACNO);
			
			if(svacnos!=null && svacnos.getResult() ) {
				Map<String , Object> callDataSvacnos = (Map) svacnos.getData();
				ArrayList<Map<String , String>> callTableSvacnos = (ArrayList<Map<String , String>>) callDataSvacnos.get("REC");
				
				if(callTableSvacnos.size() <= 0) {
					svacnos.setSYSMessage("Z999");
				}
			}
		}catch(Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_trading_apply_p3 error >> {}",e);
		}	
		return  svacnos;
	}
	
	public BaseResult gold_trading_apply_p3_acnos(String cusidn,Map<String, String> reqParam) {
		log.trace("gold_trading_apply_p3_acnos start");
		BaseResult acnos = new BaseResult();
		try {
			acnos = N920_REST(cusidn, GOLD_RESERVATION_QOERY_N920);
			
			if(acnos!=null && acnos.getResult() ) {
				Map<String , Object> callDataAcnos = (Map) acnos.getData();
				ArrayList<Map<String , String>> callTableAcnos = (ArrayList<Map<String , String>>) callDataAcnos.get("REC");
				String all = "";
				if(callTableAcnos.size() > 1) {
					for(Map<String , String> callRow : callTableAcnos) {
						all += callRow.get("ACN") + ",";
					}
				}
				acnos.addData("ALL", all);
			}
		}catch(Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_trading_apply_p3_acnos error >> {}",e);
		}	
		return  acnos;
	}
	
	public BaseResult gold_trading_apply_result(String cusidn,Map<String, String> reqParam) {
		log.trace("gold_trading_apply_result");
		BaseResult bs = null ;
		try {
			if(reqParam.get("FGTXWAY").equals("2"))
				reqParam.put("accNo",reqParam.get("accNo").substring(reqParam.get("accNo").length() - 11));
			bs = new BaseResult();
			bs = NA60_REST(cusidn, reqParam);
			bs.setResult(Boolean.TRUE);
		}catch(Exception e) {
//			e.printStackTrace();
			log.error("time_deposit",e);
		}	
		return  bs;
	}
}
