package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class Na30_1_REST_RQ extends BaseRestBean_OLS implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4725457230543149167L;
	private String CCBIRTHDATEYY;
	private String CCBIRTHDATEMM;
	private String CCBIRTHDATEDD;
	private String _CUSIDN;
	private String CUSIDN;
	private String icSeq;//晶片卡交易序號
	private String cflg;
	private String CARDNUM;//金融卡卡號
	private String MOBILE;
	private String MAIL;
	private String NOTIFY_ACTIVE;
	private String HLOGINPIN;//簽入密碼
	private String HTRANSPIN;//交易密碼
	private String BRHNAME;//銀行名稱
	private String BRHCOD;//銀行代碼
	private String ATMTRAN;//金融卡線上申請轉帳功能
	private String PHONE_H;
	private String USERIP;
	private String chk;
	private String USERNAME;
	private String _braCode;
	public String getCCBIRTHDATEYY() {
		return CCBIRTHDATEYY;
	}
	public void setCCBIRTHDATEYY(String cCBIRTHDATEYY) {
		CCBIRTHDATEYY = cCBIRTHDATEYY;
	}
	public String getCCBIRTHDATEMM() {
		return CCBIRTHDATEMM;
	}
	public void setCCBIRTHDATEMM(String cCBIRTHDATEMM) {
		CCBIRTHDATEMM = cCBIRTHDATEMM;
	}
	public String getCCBIRTHDATEDD() {
		return CCBIRTHDATEDD;
	}
	public void setCCBIRTHDATEDD(String cCBIRTHDATEDD) {
		CCBIRTHDATEDD = cCBIRTHDATEDD;
	}
	public String get_CUSIDN() {
		return _CUSIDN;
	}
	public void set_CUSIDN(String _CUSIDN) {
		this._CUSIDN = _CUSIDN;
	}
	public String getIcSeq() {
		return icSeq;
	}
	public void setIcSeq(String icSeq) {
		this.icSeq = icSeq;
	}
	public String getCflg() {
		return cflg;
	}
	public void setCflg(String cflg) {
		this.cflg = cflg;
	}
	public String getCARDNUM() {
		return CARDNUM;
	}
	public void setCARDNUM(String cARDNUM) {
		CARDNUM = cARDNUM;
	}
	public String getMOBILE() {
		return MOBILE;
	}
	public void setMOBILE(String mOBILE) {
		MOBILE = mOBILE;
	}
	public String getMAIL() {
		return MAIL;
	}
	public void setMAIL(String mAIL) {
		MAIL = mAIL;
	}
	public String getNOTIFY_ACTIVE() {
		return NOTIFY_ACTIVE;
	}
	public void setNOTIFY_ACTIVE(String nOTIFY_ACTIVE) {
		NOTIFY_ACTIVE = nOTIFY_ACTIVE;
	}
	public String getHLOGINPIN() {
		return HLOGINPIN;
	}
	public void setHLOGINPIN(String hLOGINPIN) {
		HLOGINPIN = hLOGINPIN;
	}
	public String getHTRANSPIN() {
		return HTRANSPIN;
	}
	public void setHTRANSPIN(String hTRANSPIN) {
		HTRANSPIN = hTRANSPIN;
	}
	public String getBRHNAME() {
		return BRHNAME;
	}
	public void setBRHNAME(String bRHNAME) {
		BRHNAME = bRHNAME;
	}
	public String getBRHCOD() {
		return BRHCOD;
	}
	public void setBRHCOD(String bRHCOD) {
		BRHCOD = bRHCOD;
	}
	public String getATMTRAN() {
		return ATMTRAN;
	}
	public void setATMTRAN(String aTMTRAN) {
		ATMTRAN = aTMTRAN;
	}
	public String getPHONE_H() {
		return PHONE_H;
	}
	public void setPHONE_H(String pHONE_H) {
		PHONE_H = pHONE_H;
	}
	public String getUSERIP() {
		return USERIP;
	}
	public void setUSERIP(String uSERIP) {
		USERIP = uSERIP;
	}
	public String getChk() {
		return chk;
	}
	public void setChk(String chk) {
		this.chk = chk;
	}
	public String get_braCode() {
		return _braCode;
	}
	public void set_braCode(String _braCode) {
		this._braCode = _braCode;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getUSERNAME() {
		return USERNAME;
	}
	public void setUSERNAME(String uSERNAME) {
		USERNAME = uSERNAME;
	}
	
}
