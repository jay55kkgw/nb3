package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.ConfingManager;

public class BaseRestBean_AUTH extends BaseRestBean implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3722063169778569951L;

	// private static String ms_Channel = "ms_auth/com/";
	private static String ms_Channel = ConfingManager.MS_AUTH;


	public static String getMs_Channel()
	{
		return ms_Channel;
	}

	public static void setMs_Channel(String ms_Channel)
	{
		BaseRestBean_AUTH.ms_Channel = ms_Channel;
	}

	public static String getAPI_Name(Class<?> retClass) {
		  if(ms_Channel.endsWith("/") == false) {
		    	ms_Channel = ms_Channel + "/";
		    }
		return ms_Channel + retClass.getSimpleName().replace("_REST_RQ", "");
	}
	
	public static String getAPI_Name(Class<?> retClass, String _ms_Channel) {

		if (StrUtil.isNotEmpty(_ms_Channel)) {
			if (_ms_Channel.endsWith("/") == false) {
				_ms_Channel = _ms_Channel + "/";
			}
			return _ms_Channel + retClass.getSimpleName().replace("_REST_RQ", "");
		}
		return getAPI_Name(retClass);
	}
}
