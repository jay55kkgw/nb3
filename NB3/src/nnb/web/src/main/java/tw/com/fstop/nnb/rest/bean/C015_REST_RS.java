package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tw.com.fstop.web.util.WebUtil;

public class C015_REST_RS extends BaseRestBean implements Serializable
{
	Logger log = LoggerFactory.getLogger(this.getClass());

	private static final long serialVersionUID = 8600126206338254151L;
	
	private String CUSIDN;			// 身份證號
	private String SSUP ;
	private String SNID;
	private String ACF;
	private String MSGTYPE;
	private String PROCCODE;
	private String SYSTRACE;
	private String TXNDIID;
	private String TXNSIID;
	private String TXNIDT;
	private String RESPCOD;
	private String SYNCCHK;
	private String BITMAPCFG;
	private String DATE;
	private String TIME;
	private String CUSNAME;
	private String ZIPCODE;
	private String ADDRESS;
	private String HTELPHONE;
	private String OTELPHONE;
	private String MTELPHONE;
	private String BILLSENDMODE;
	private String EMAIL;
	private String SHWD;//視窗註記 2020/07/02新增
	public Logger getLog() {
		return log;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public String getSSUP() {
		return SSUP;
	}
	public String getSNID() {
		return SNID;
	}
	public String getACF() {
		return ACF;
	}
	public String getMSGTYPE() {
		return MSGTYPE;
	}
	public String getPROCCODE() {
		return PROCCODE;
	}
	public String getSYSTRACE() {
		return SYSTRACE;
	}
	public String getTXNDIID() {
		return TXNDIID;
	}
	public String getTXNSIID() {
		return TXNSIID;
	}
	public String getTXNIDT() {
		return TXNIDT;
	}
	public String getRESPCOD() {
		return RESPCOD;
	}
	public String getSYNCCHK() {
		return SYNCCHK;
	}
	public String getBITMAPCFG() {
		return BITMAPCFG;
	}
	public String getDATE() {
		return DATE;
	}
	public String getTIME() {
		return TIME;
	}
	public String getCUSNAME() {
		return CUSNAME;
	}
	public String getZIPCODE() {
		return ZIPCODE;
	}
	public String getADDRESS() {
		return ADDRESS;
	}
	public String getHTELPHONE() {
		return HTELPHONE;
	}
	public String getOTELPHONE() {
		return OTELPHONE;
	}
	public String getMTELPHONE() {
		return MTELPHONE;
	}
	public String getBILLSENDMODE() {
		return BILLSENDMODE;
	}
	public String getEMAIL() {
		return EMAIL;
	}
	public void setLog(Logger log) {
		this.log = log;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public void setSSUP(String sSUP) {
		SSUP = sSUP;
	}
	public void setSNID(String sNID) {
		SNID = sNID;
	}
	public void setACF(String aCF) {
		ACF = aCF;
	}
	public void setMSGTYPE(String mSGTYPE) {
		MSGTYPE = mSGTYPE;
	}
	public void setPROCCODE(String pROCCODE) {
		PROCCODE = pROCCODE;
	}
	public void setSYSTRACE(String sYSTRACE) {
		SYSTRACE = sYSTRACE;
	}
	public void setTXNDIID(String tXNDIID) {
		TXNDIID = tXNDIID;
	}
	public void setTXNSIID(String tXNSIID) {
		TXNSIID = tXNSIID;
	}
	public void setTXNIDT(String tXNIDT) {
		TXNIDT = tXNIDT;
	}
	public void setRESPCOD(String rESPCOD) {
		RESPCOD = rESPCOD;
	}
	public void setSYNCCHK(String sYNCCHK) {
		SYNCCHK = sYNCCHK;
	}
	public void setBITMAPCFG(String bITMAPCFG) {
		BITMAPCFG = bITMAPCFG;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public void setCUSNAME(String cUSNAME) {
		CUSNAME = cUSNAME;
	}
	public void setZIPCODE(String zIPCODE) {
		ZIPCODE = zIPCODE;
	}
	public void setADDRESS(String aDDRESS) {
		ADDRESS = aDDRESS;
	}
	public void setHTELPHONE(String hTELPHONE) {
		HTELPHONE = hTELPHONE;
	}
	public void setOTELPHONE(String oTELPHONE) {
		OTELPHONE = oTELPHONE;
	}
	public void setMTELPHONE(String mTELPHONE) {
		MTELPHONE = mTELPHONE;
	}
	public void setBILLSENDMODE(String bILLSENDMODE) {
		BILLSENDMODE = bILLSENDMODE;
	}
	public void setEMAIL(String eMAIL) {
		EMAIL = eMAIL;
	}
	public String getSHWD() {
		return SHWD;
	}
	public void setSHWD(String sHWD) {
		SHWD = sHWD;
	}
	
}