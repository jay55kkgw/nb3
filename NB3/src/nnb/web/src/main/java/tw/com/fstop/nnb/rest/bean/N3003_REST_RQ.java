package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N3003_REST_RQ extends BaseRestBean_LOAN implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5845732193079178448L;
	private String FUNCID;//FUNCID
	private String CURDATE;//交易日期YYYMMDD
	private String DDATE;//資料日期YYYMMDD
	private String TXNSEQ;//TXNSEQ
	private String ACN_OUT;//轉出帳號
	private String ACN;//貸款帳號
	private String SEQ;//貸款分號
	private String PALPAY;//轉帳金額
	private String PAY_MARK;//還款註記
	private String FLD3002;//FLD3002
	private String showLoanAMT;
	private String CMMAILMEMO;
	private String CMTRMAIL;
	private String CMTRMEMO;
	private String PAYDATE;
	private String DATADATE;
	private String CHKPAGE;
	private String FGTXWAY;
	private String PINNEW;

	private String CUSIDN;//統一編號
	private String DPMYEMAIL;
	private String UID;//統一編號
	private String __TRANS_STATUS;
	private String ADOPID;
	private String FGTXDATE;
	
	public String getFGTXDATE() {
		return FGTXDATE;
	}
	public void setFGTXDATE(String fGTXDATE) {
		FGTXDATE = fGTXDATE;
	}
	public String getFUNCID() {
		return FUNCID;
	}
	public void setFUNCID(String fUNCID) {
		FUNCID = fUNCID;
	}
	public String getCURDATE() {
		return CURDATE;
	}
	public void setCURDATE(String cURDATE) {
		CURDATE = cURDATE;
	}
	public String getDDATE() {
		return DDATE;
	}
	public void setDDATE(String dDATE) {
		DDATE = dDATE;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getTXNSEQ() {
		return TXNSEQ;
	}
	public void setTXNSEQ(String tXNSEQ) {
		TXNSEQ = tXNSEQ;
	}
	public String getACN_OUT() {
		return ACN_OUT;
	}
	public void setACN_OUT(String aCN_OUT) {
		ACN_OUT = aCN_OUT;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getSEQ() {
		return SEQ;
	}
	public void setSEQ(String sEQ) {
		SEQ = sEQ;
	}
	public String getPALPAY() {
		return PALPAY;
	}
	public void setPALPAY(String pALPAY) {
		PALPAY = pALPAY;
	}
	public String getPAY_MARK() {
		return PAY_MARK;
	}
	public void setPAY_MARK(String pAY_MARK) {
		PAY_MARK = pAY_MARK;
	}
	public String getFLD3002() {
		return FLD3002;
	}
	public void setFLD3002(String fLD3002) {
		FLD3002 = fLD3002;
	}
	public String getShowLoanAMT() {
		return showLoanAMT;
	}
	public void setShowLoanAMT(String showLoanAMT) {
		this.showLoanAMT = showLoanAMT;
	}
	public String getCMMAILMEMO() {
		return CMMAILMEMO;
	}
	public void setCMMAILMEMO(String cMMAILMEMO) {
		CMMAILMEMO = cMMAILMEMO;
	}
	public String getCMTRMAIL() {
		return CMTRMAIL;
	}
	public void setCMTRMAIL(String cMTRMAIL) {
		CMTRMAIL = cMTRMAIL;
	}
	public String getCMTRMEMO() {
		return CMTRMEMO;
	}
	public void setCMTRMEMO(String cMTRMEMO) {
		CMTRMEMO = cMTRMEMO;
	}
	public String getPAYDATE() {
		return PAYDATE;
	}
	public void setPAYDATE(String pAYDATE) {
		PAYDATE = pAYDATE;
	}
	public String getDATADATE() {
		return DATADATE;
	}
	public void setDATADATE(String dATADATE) {
		DATADATE = dATADATE;
	}
	public String getCHKPAGE() {
		return CHKPAGE;
	}
	public void setCHKPAGE(String cHKPAGE) {
		CHKPAGE = cHKPAGE;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getPINNEW() {
		return PINNEW;
	}
	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}
	public String getDPMYEMAIL() {
		return DPMYEMAIL;
	}
	public void setDPMYEMAIL(String dPMYEMAIL) {
		DPMYEMAIL = dPMYEMAIL;
	}
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	public String get__TRANS_STATUS() {
		return __TRANS_STATUS;
	}
	public void set__TRANS_STATUS(String __TRANS_STATUS) {
		this.__TRANS_STATUS = __TRANS_STATUS;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
}
