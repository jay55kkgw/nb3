package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N014_REST_RQ extends BaseRestBean_LOAN implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1124472617158015428L;
	
	private String CUSIDN ;
	private String OKOVNEXT="TRUE";
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getOKOVNEXT() {
		return OKOVNEXT;
	}
	public void setOKOVNEXT(String oKOVNEXT) {
		OKOVNEXT = oKOVNEXT;
	}
	
}
