package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class GD011_REST_RSDATA  extends BaseRestBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6981640903546151642L;

	String PRICE1;
	String PRICE2;
	String PRICE3;
	String PRICE4;
	String QTIME;
	String BPRICE;
	String QDATE;
	String LASTDATE;
	String LASTTIME;
	String GOLDPRICEID;
	String SPRICE;
	
	public String getPRICE1() {
		return PRICE1;
	}
	public void setPRICE1(String pRICE1) {
		PRICE1 = pRICE1;
	}
	public String getPRICE2() {
		return PRICE2;
	}
	public void setPRICE2(String pRICE2) {
		PRICE2 = pRICE2;
	}
	public String getPRICE3() {
		return PRICE3;
	}
	public void setPRICE3(String pRICE3) {
		PRICE3 = pRICE3;
	}
	public String getPRICE4() {
		return PRICE4;
	}
	public void setPRICE4(String pRICE4) {
		PRICE4 = pRICE4;
	}
	public String getQTIME() {
		return QTIME;
	}
	public void setQTIME(String qTIME) {
		QTIME = qTIME;
	}
	public String getBPRICE() {
		return BPRICE;
	}
	public void setBPRICE(String bPRICE) {
		BPRICE = bPRICE;
	}
	public String getQDATE() {
		return QDATE;
	}
	public void setQDATE(String qDATE) {
		QDATE = qDATE;
	}
	public String getLASTDATE() {
		return LASTDATE;
	}
	public void setLASTDATE(String lASTDATE) {
		LASTDATE = lASTDATE;
	}
	public String getLASTTIME() {
		return LASTTIME;
	}
	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}
	public String getGOLDPRICEID() {
		return GOLDPRICEID;
	}
	public void setGOLDPRICEID(String gOLDPRICEID) {
		GOLDPRICEID = gOLDPRICEID;
	}
	public String getSPRICE() {
		return SPRICE;
	}
	public void setSPRICE(String sPRICE) {
		SPRICE = sPRICE;
	}
}
