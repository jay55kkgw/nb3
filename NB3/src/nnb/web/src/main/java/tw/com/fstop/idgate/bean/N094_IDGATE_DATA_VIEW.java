package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;

public class N094_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7636034575390818226L;

	@SerializedName(value = "ACN") //黃金存摺帳號
	private String ACN;

	@SerializedName(value = "SVACN") //台幣存款帳號
	private String SVACN;
	
	@SerializedName(value = "TRNAMT") //應繳款總金額
	private String TRNAMT;

	public String getACN() {
		return ACN;
	}

	public void setACN(String aCN) {
		ACN = aCN;
	}

	public String getSVACN() {
		return SVACN;
	}

	public void setSVACN(String sVACN) {
		SVACN = sVACN;
	}

	public String getTRNAMT() {
		return TRNAMT;
	}

	public void setTRNAMT(String tRNAMT) {
		TRNAMT = tRNAMT;
	}
	@Override
	public Map<String, String> coverMap(){
		
		LinkedHashMap<String, String> result = new LinkedHashMap<String, String>();
		result.put("交易名稱", "繳納定期扣款失敗手續費");
		result.put("交易類型", "即時");
		result.put("黃金存摺帳號", this.ACN);
		result.put("台幣存款帳號", this.SVACN);
		result.put("手續費合計", this.TRNAMT);
		return result;
	}

	
}
