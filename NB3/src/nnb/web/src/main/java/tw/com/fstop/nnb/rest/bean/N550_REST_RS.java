package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N550_REST_RS extends BaseRestBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5800031202782191683L;
	private String LENGTH;
	private String CMRECNUM;
	private String ABEND;
	private String REC_NO;
	private String FXSUBAMT_1;
	private String FXSUBAMT_2;
	private String FXSUBAMT_3;
	private String FXSUBAMT_4;
	private String FXSUBAMT_5;
	private String FXSUBAMT_6;
	private String FXSUBAMT_7;
	private String CMPERIOD;
	private String AMTRLCCCY_1;
	private String CMQTIME;
	private String __OCCURS;
	private String USERDATA_X50;

    //信用狀餘額
	//幣別
	private String BALRLCCCY_7;
	private String BALRLCCCY_6;
	private String BALRLCCCY_5;
	private String BALRLCCCY_4;
	private String BALRLCCCY_3;
	private String BALRLCCCY_2;
	private String BALRLCCCY_1;
    //該幣別加總
	private  String FXSUBBAL_7;
	private  String FXSUBBAL_6;
	private String FXSUBBAL_5;
	private String FXSUBBAL_4;
	private String FXSUBBAL_3;
	private String FXSUBBAL_2;
	private String FXSUBBAL_1;
	private LinkedList<N550_REST_RSDATA> REC;
	public String getBALRLCCCY_1() {
		return BALRLCCCY_1;
	}
	public void setBALRLCCCY_1(String bALRLCCCY_1) {
		BALRLCCCY_1 = bALRLCCCY_1;
	}
	public String getLENGTH() {
		return LENGTH;
	}
	public void setLENGTH(String lENGTH) {
		LENGTH = lENGTH;
	}
	public String getCMRECNUM() {
		return CMRECNUM;
	}
	public void setCMRECNUM(String cMRECNUM) {
		CMRECNUM = cMRECNUM;
	}
	public String getABEND() {
		return ABEND;
	}
	public void setABEND(String aBEND) {
		ABEND = aBEND;
	}
	public String getREC_NO() {
		return REC_NO;
	}
	public void setREC_NO(String rEC_NO) {
		REC_NO = rEC_NO;
	}
	public String getFXSUBAMT_1() {
		return FXSUBAMT_1;
	}
	public void setFXSUBAMT_1(String fXSUBAMT_1) {
		FXSUBAMT_1 = fXSUBAMT_1;
	}
	public String getCMPERIOD() {
		return CMPERIOD;
	}
	public void setCMPERIOD(String cMPERIOD) {
		CMPERIOD = cMPERIOD;
	}
	public String getAMTRLCCCY_1() {
		return AMTRLCCCY_1;
	}
	public void setAMTRLCCCY_1(String aMTRLCCCY_1) {
		AMTRLCCCY_1 = aMTRLCCCY_1;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String get__OCCURS() {
		return __OCCURS;
	}
	public void set__OCCURS(String __OCCURS) {
		this.__OCCURS = __OCCURS;
	}
	public String getUSERDATA_X50() {
		return USERDATA_X50;
	}
	public void setUSERDATA_X50(String uSERDATA_X50) {
		USERDATA_X50 = uSERDATA_X50;
	}
	public String getFXSUBBAL_1() {
		return FXSUBBAL_1;
	}
	public void setFXSUBBAL_1(String fXSUBBAL_1) {
		FXSUBBAL_1 = fXSUBBAL_1;
	}
	public LinkedList<N550_REST_RSDATA> getREC() {
		return REC;
	}
	public void setREC(LinkedList<N550_REST_RSDATA> rEC) {
		REC = rEC;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getBALRLCCCY_7() {
		return BALRLCCCY_7;
	}
	public void setBALRLCCCY_7(String bALRLCCCY_7) {
		BALRLCCCY_7 = bALRLCCCY_7;
	}
	public String getBALRLCCCY_6() {
		return BALRLCCCY_6;
	}
	public void setBALRLCCCY_6(String bALRLCCCY_6) {
		BALRLCCCY_6 = bALRLCCCY_6;
	}
	public String getBALRLCCCY_5() {
		return BALRLCCCY_5;
	}
	public void setBALRLCCCY_5(String bALRLCCCY_5) {
		BALRLCCCY_5 = bALRLCCCY_5;
	}
	public String getBALRLCCCY_4() {
		return BALRLCCCY_4;
	}
	public void setBALRLCCCY_4(String bALRLCCCY_4) {
		BALRLCCCY_4 = bALRLCCCY_4;
	}
	public String getBALRLCCCY_3() {
		return BALRLCCCY_3;
	}
	public void setBALRLCCCY_3(String bALRLCCCY_3) {
		BALRLCCCY_3 = bALRLCCCY_3;
	}
	public String getBALRLCCCY_2() {
		return BALRLCCCY_2;
	}
	public void setBALRLCCCY_2(String bALRLCCCY_2) {
		BALRLCCCY_2 = bALRLCCCY_2;
	}
	public String getFXSUBBAL_7() {
		return FXSUBBAL_7;
	}
	public void setFXSUBBAL_7(String fXSUBBAL_7) {
		FXSUBBAL_7 = fXSUBBAL_7;
	}
	public String getFXSUBBAL_6() {
		return FXSUBBAL_6;
	}
	public void setFXSUBBAL_6(String fXSUBBAL_6) {
		FXSUBBAL_6 = fXSUBBAL_6;
	}
	public String getFXSUBBAL_5() {
		return FXSUBBAL_5;
	}
	public void setFXSUBBAL_5(String fXSUBBAL_5) {
		FXSUBBAL_5 = fXSUBBAL_5;
	}
	public String getFXSUBBAL_4() {
		return FXSUBBAL_4;
	}
	public void setFXSUBBAL_4(String fXSUBBAL_4) {
		FXSUBBAL_4 = fXSUBBAL_4;
	}
	public String getFXSUBBAL_3() {
		return FXSUBBAL_3;
	}
	public void setFXSUBBAL_3(String fXSUBBAL_3) {
		FXSUBBAL_3 = fXSUBBAL_3;
	}
	public String getFXSUBBAL_2() {
		return FXSUBBAL_2;
	}
	public void setFXSUBBAL_2(String fXSUBBAL_2) {
		FXSUBBAL_2 = fXSUBBAL_2;
	}
	public String getFXSUBAMT_2() {
		return FXSUBAMT_2;
	}
	public void setFXSUBAMT_2(String fXSUBAMT_2) {
		FXSUBAMT_2 = fXSUBAMT_2;
	}
	public String getFXSUBAMT_3() {
		return FXSUBAMT_3;
	}
	public void setFXSUBAMT_3(String fXSUBAMT_3) {
		FXSUBAMT_3 = fXSUBAMT_3;
	}
	public String getFXSUBAMT_4() {
		return FXSUBAMT_4;
	}
	public void setFXSUBAMT_4(String fXSUBAMT_4) {
		FXSUBAMT_4 = fXSUBAMT_4;
	}
	public String getFXSUBAMT_5() {
		return FXSUBAMT_5;
	}
	public void setFXSUBAMT_5(String fXSUBAMT_5) {
		FXSUBAMT_5 = fXSUBAMT_5;
	}
	public String getFXSUBAMT_6() {
		return FXSUBAMT_6;
	}
	public void setFXSUBAMT_6(String fXSUBAMT_6) {
		FXSUBAMT_6 = fXSUBAMT_6;
	}
	public String getFXSUBAMT_7() {
		return FXSUBAMT_7;
	}
	public void setFXSUBAMT_7(String fXSUBAMT_7) {
		FXSUBAMT_7 = fXSUBAMT_7;
	}

	
	
	

}
