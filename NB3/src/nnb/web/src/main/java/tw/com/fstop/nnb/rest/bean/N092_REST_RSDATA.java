package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N092_REST_RSDATA extends BaseRestBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4741582877807942855L;

	private String APPDATE;
	private String STATUS;
	private String ACN2;
	private String APPTIME;
	private String TRNGD;
	private String APPTYPE;
	
	public String getAPPDATE() {
		return APPDATE;
	}
	public void setAPPDATE(String aPPDATE) {
		APPDATE = aPPDATE;
	}
	public String getSTATUS() {
		return STATUS;
	}
	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}
	public String getACN2() {
		return ACN2;
	}
	public void setACN2(String aCN2) {
		ACN2 = aCN2;
	}
	public String getAPPTIME() {
		return APPTIME;
	}
	public void setAPPTIME(String aPPTIME) {
		APPTIME = aPPTIME;
	}
	public String getTRNGD() {
		return TRNGD;
	}
	public void setTRNGD(String tRNGD) {
		TRNGD = tRNGD;
	}
	public String getAPPTYPE() {
		return APPTYPE;
	}
	public void setAPPTYPE(String aPPTYPE) {
		APPTYPE = aPPTYPE;
	}

	
}
