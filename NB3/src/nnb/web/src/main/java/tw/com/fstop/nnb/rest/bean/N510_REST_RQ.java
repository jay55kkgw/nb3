package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N510_REST_RQ extends BaseRestBean_FX implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -559387283010166442L;
	
	String	ACN	;
	String	CUSIDN	;
	String	TRNSRC = "NB";

	// 是否跳過TXNLOG
	private String ADOPID;
	
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getTRNSRC() {
		return TRNSRC;
	}
	public void setTRNSRC(String tRNSRC) {
		TRNSRC = tRNSRC;
	}
	
}
