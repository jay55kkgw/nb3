package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N022_REST_RS extends BaseRestBean implements Serializable{

	/**
	 * warning
	 * 這支不是真的n022
	 * 是monitor的偽裝
	 * 因為一開始是打n022後來換成nd08
	 */
	private static final long serialVersionUID = -9156994034304457422L;
	String TIME;
	String COUNT;
	String SEQ;
	String DATADT;
	String DBconnect;
	String DB_LOG;
	String TELconnect;
	String TEL_LOG;
//	private LinkedList<N022_REST_RSDATA> REC;
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getCOUNT() {
		return COUNT;
	}
	public void setCOUNT(String cOUNT) {
		COUNT = cOUNT;
	}
	public String getSEQ() {
		return SEQ;
	}
	public void setSEQ(String sEQ) {
		SEQ = sEQ;
	}
	public String getDATADT() {
		return DATADT;
	}
	public void setDATADT(String dATADT) {
		DATADT = dATADT;
	}
//	public LinkedList<N022_REST_RSDATA> getREC() {
//		return REC;
//	}
//	public void setREC(LinkedList<N022_REST_RSDATA> rEC) {
//		REC = rEC;
//	}
	public String getDBconnect() {
		return DBconnect;
	}
	public void setDBconnect(String dBconnect) {
		DBconnect = dBconnect;
	}
	public String getTELconnect() {
		return TELconnect;
	}
	public void setTELconnect(String tELconnect) {
		TELconnect = tELconnect;
	}
	public String getDB_LOG() {
		return DB_LOG;
	}
	public void setDB_LOG(String dB_LOG) {
		DB_LOG = dB_LOG;
	}
	public String getTEL_LOG() {
		return TEL_LOG;
	}
	public void setTEL_LOG(String tEL_LOG) {
		TEL_LOG = tEL_LOG;
	}
}
