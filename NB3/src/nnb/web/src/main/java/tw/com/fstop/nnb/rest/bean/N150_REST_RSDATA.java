package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

/**
 * 輕鬆理財存摺明細查詢結果
 * 
 * @author Jim
 *
 */

public class N150_REST_RSDATA extends BaseRestBean  implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 125756402889237904L;

	private String ACN;//

	private String ABEND;// 結束代碼
	
	private LinkedList<N150_REST_RSDATA2> TABLE;

	public String getACN()
	{
		return ACN;
	}

	public void setACN(String aCN)
	{
		ACN = aCN;
	}

	public String getABEND()
	{
		return ABEND;
	}

	public void setABEND(String aBEND)
	{
		ABEND = aBEND;
	}

	public LinkedList<N150_REST_RSDATA2> getTABLE()
	{
		return TABLE;
	}

	public void setTABLE(LinkedList<N150_REST_RSDATA2> tABLE)
	{
		TABLE = tABLE;
	}


}
