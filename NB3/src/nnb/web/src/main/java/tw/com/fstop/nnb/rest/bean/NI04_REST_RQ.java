package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class NI04_REST_RQ extends BaseRestBean_CC implements Serializable {

	private static final long serialVersionUID = -6018579665065621630L;
	
	
	//電文
	private String SYNC;		// Sync.Check Item
	private String PPSYNC;		// P.P.Key Sync.Check Item
	private String PINKEY;		// 網路銀行密碼
	private String CERTACN;		// 憑證帳號
	private String FLAG;		// 交易狀態 0
	private String BNKRA ;		// 行庫別 050
	private String XMLCA;		// CA 識別碼
	private String XMLCN;		// 憑證ＣＮ
	private String PPSYNCN;		// P.P.Key Sync.Check Item 16
	private String PINNEW;		// 網路銀行密碼（新）
	private String CUSIDN;      // 統一編號
	private String TSFACN;      // 扣帳帳號
	private String PYMT_FLAG;   // 扣繳方式
	
	
	//NI04.java 
	private String PAYFLAG;
	private String APPLYFLAG;
	

	// ms_tw交易機制需要的欄位
	private String FGTXWAY; // 交易機制
	private String pkcs7Sign; // IKEY
	private String jsondc; // IKEY
	
	//IDGATE
	private String sessionID;
	private String idgateID;
	private String txnID;
	
	
	public String getSYNC() {
		return SYNC;
	}
	public String getPPSYNC() {
		return PPSYNC;
	}
	public String getPINKEY() {
		return PINKEY;
	}
	public String getCERTACN() {
		return CERTACN;
	}
	public String getFLAG() {
		return FLAG;
	}
	public String getBNKRA() {
		return BNKRA;
	}
	public String getXMLCA() {
		return XMLCA;
	}
	public String getXMLCN() {
		return XMLCN;
	}
	public String getPPSYNCN() {
		return PPSYNCN;
	}
	public String getPINNEW() {
		return PINNEW;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public String getTSFACN() {
		return TSFACN;
	}
	public String getPYMT_FLAG() {
		return PYMT_FLAG;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public String getPkcs7Sign() {
		return pkcs7Sign;
	}
	public String getJsondc() {
		return jsondc;
	}
	public void setSYNC(String sYNC) {
		SYNC = sYNC;
	}
	public void setPPSYNC(String pPSYNC) {
		PPSYNC = pPSYNC;
	}
	public void setPINKEY(String pINKEY) {
		PINKEY = pINKEY;
	}
	public void setCERTACN(String cERTACN) {
		CERTACN = cERTACN;
	}
	public void setFLAG(String fLAG) {
		FLAG = fLAG;
	}
	public void setBNKRA(String bNKRA) {
		BNKRA = bNKRA;
	}
	public void setXMLCA(String xMLCA) {
		XMLCA = xMLCA;
	}
	public void setXMLCN(String xMLCN) {
		XMLCN = xMLCN;
	}
	public void setPPSYNCN(String pPSYNCN) {
		PPSYNCN = pPSYNCN;
	}
	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public void setTSFACN(String tSFACN) {
		TSFACN = tSFACN;
	}
	public void setPYMT_FLAG(String pYMT_FLAG) {
		PYMT_FLAG = pYMT_FLAG;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public void setPkcs7Sign(String pkcs7Sign) {
		this.pkcs7Sign = pkcs7Sign;
	}
	public void setJsondc(String jsondc) {
		this.jsondc = jsondc;
	}
	public String getPAYFLAG() {
		return PAYFLAG;
	}
	public String getAPPLYFLAG() {
		return APPLYFLAG;
	}
	public void setPAYFLAG(String pAYFLAG) {
		PAYFLAG = pAYFLAG;
	}
	public void setAPPLYFLAG(String aPPLYFLAG) {
		APPLYFLAG = aPPLYFLAG;
	}
	public String getSessionID() {
		return sessionID;
	}
	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}
	public String getIdgateID() {
		return idgateID;
	}
	public void setIdgateID(String idgateID) {
		this.idgateID = idgateID;
	}
	public String getTxnID() {
		return txnID;
	}
	public void setTxnID(String txnID) {
		this.txnID = txnID;
	}
}
