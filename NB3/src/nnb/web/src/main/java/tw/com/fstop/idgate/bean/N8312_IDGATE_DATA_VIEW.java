package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;

public class N8312_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5556555905527940420L;

	@SerializedName(value = "CARDNUM")
	private String CARDNUM;

	@SerializedName(value = "CUSNUM")
	private String CUSNUM;
	
	@Override
	public Map<String, String> coverMap(){
		Map<String, String> result = new HashMap<String, String>();
		result.put("信用卡號", this.CARDNUM);
		result.put("用戶編號（保險證字號）", this.CUSNUM);
		return result;
	}

	public String getCARDNUM() {
		return CARDNUM;
	}

	public void setCARDNUM(String cARDNUM) {
		CARDNUM = cARDNUM;
	}

	public String getCUSNUM() {
		return CUSNUM;
	}

	public void setCUSNUM(String cUSNUM) {
		CUSNUM = cUSNUM;
	}
	
}
