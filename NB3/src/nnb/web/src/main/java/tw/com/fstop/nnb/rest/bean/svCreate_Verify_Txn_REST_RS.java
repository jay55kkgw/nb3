package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 通知手機啟動驗證
 * 
 * @author Leo
 *
 */
public class svCreate_Verify_Txn_REST_RS extends BaseRestBean_IDGATE implements Serializable {
	


	/**
	 * 
	 */
	private static final long serialVersionUID = 4150662103103645182L;
	@JsonProperty
	private svCreate_Verify_Txn_REST_RSDATA data;

	public svCreate_Verify_Txn_REST_RSDATA getData() {
		return data;
	}

	public void setData(svCreate_Verify_Txn_REST_RSDATA data) {
		this.data = data;
	}
	
	

}
