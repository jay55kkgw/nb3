package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.annotations.SerializedName;

import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.util.StrUtils;


public class N283_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6714982693399691806L;

	@SerializedName(value = "DPSCHNO")
	private String DPSCHNO;

	@SerializedName(value = "DPPERMTDATE")
	private String DPPERMTDATE;
	
	@SerializedName(value = "DPTDATE")
	private String DPTDATE;
	
	@SerializedName(value = "DPFDATE")
	private String DPFDATE;
	
	@SerializedName(value = "DPNEXTDATE")
	private String DPNEXTDATE;
	
	@SerializedName(value = "DPWDAC")
	private String DPWDAC;
	
	@SerializedName(value = "DPSVBH")
	private String DPSVBH;
	
	@SerializedName(value = "DPSVAC")
	private String DPSVAC;
	
	@SerializedName(value = "DPTXAMT")
	private String DPTXAMT;
	
	@SerializedName(value = "TXTYPE")
	private String TXTYPE;
	
	@SerializedName(value = "DPTXMEMO")
	private String DPTXMEMO;
	
	@Override
	public Map<String, String> coverMap(){
		LinkedHashMap<String, String> result = new LinkedHashMap<String, String>();
		//交易名稱
		result.put("交易名稱", "臺幣預約交易查詢/取消");
		//交易類型
		result.put("交易類型", "預約取消");
		//預約編號
		result.put("預約編號", this.DPSCHNO);
//				週期
		result.put("週期", this.DPPERMTDATE);
//				生效日 - 截止日
		if(StrUtils.isNotEmpty(this.DPTDATE)) {
			result.put("生效日 - 截止日", this.DPFDATE + "-" + this.DPTDATE);
		}
		else {
			result.put("生效日 - 截止日", this.DPFDATE);
		}
//				下次轉帳日
		result.put("下次轉帳日", this.DPNEXTDATE);
//				轉出帳號
		result.put("轉出帳號", this.DPWDAC);
//				轉入帳號 / 繳費稅代號
		result.put("轉入帳號 / 繳費稅代號", this.DPSVBH + this.DPSVAC);
//				轉帳金額
		result.put("轉帳金額", this.DPTXAMT);
//				交易類別
		result.put("交易類別", this.TXTYPE);
//				備註
		result.put("備註", this.DPTXMEMO);
		return result;
	}

	public String getDPSCHNO() {
		return DPSCHNO;
	}

	public void setDPSCHNO(String dPSCHNO) {
		DPSCHNO = dPSCHNO;
	}

	public String getDPPERMTDATE() {
		return DPPERMTDATE;
	}

	public void setDPPERMTDATE(String dPPERMTDATE) {
		DPPERMTDATE = dPPERMTDATE;
	}

	public String getDPTDATE() {
		return DPTDATE;
	}

	public void setDPTDATE(String dPTDATE) {
		DPTDATE = dPTDATE;
	}

	public String getDPFDATE() {
		return DPFDATE;
	}

	public void setDPFDATE(String dPFDATE) {
		DPFDATE = dPFDATE;
	}

	public String getDPNEXTDATE() {
		return DPNEXTDATE;
	}

	public void setDPNEXTDATE(String dPNEXTDATE) {
		DPNEXTDATE = dPNEXTDATE;
	}

	public String getDPWDAC() {
		return DPWDAC;
	}

	public void setDPWDAC(String dPWDAC) {
		DPWDAC = dPWDAC;
	}

	public String getDPSVBH() {
		return DPSVBH;
	}

	public void setDPSVBH(String dPSVBH) {
		DPSVBH = dPSVBH;
	}

	public String getDPSVAC() {
		return DPSVAC;
	}

	public void setDPSVAC(String dPSVAC) {
		DPSVAC = dPSVAC;
	}

	public String getDPTXAMT() {
		return DPTXAMT;
	}

	public void setDPTXAMT(String dPTXAMT) {
		DPTXAMT = dPTXAMT;
	}

	public String getTXTYPE() {
		return TXTYPE;
	}

	public void setTXTYPE(String tXTYPE) {
		TXTYPE = tXTYPE;
	}

	public String getDPTXMEMO() {
		return DPTXMEMO;
	}

	public void setDPTXMEMO(String dPTXMEMO) {
		DPTXMEMO = dPTXMEMO;
	}

}
