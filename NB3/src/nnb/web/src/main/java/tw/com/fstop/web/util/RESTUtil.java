package tw.com.fstop.web.util;

import java.io.File;
import java.io.FileInputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.servlet.http.HttpServletRequest;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.joda.time.DateTime;
//import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
//import org.apache.http.impl.client.CloseableHttpClient;
//import org.apache.http.impl.client.HttpClients;
//import org.apache.http.ssl.TrustStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import com.google.common.collect.Lists;

import fstop.orm.po.ADMMSGCODE;
import tw.com.fstop.nnb.rest.bean.BaseRestBean;
import tw.com.fstop.tbb.nnb.dao.AdmMsgCodeDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.StrUtil;

@Component
public class RESTUtil {
	 Logger log = LoggerFactory.getLogger(this.getClass());
	@Autowired
	String apache_ap1 ;
	@Autowired
	String apache_ap2 ;
	@Autowired
	String isOpenLoadBlance ;
	@Autowired
	Integer socketTimeout ;
	@Autowired
	private HttpServletRequest request; 
	SecureRandom secureRandom ;
	@Autowired
	AdmMsgCodeDao admMsgCodeDao ;
	
	@PostConstruct
	public void getRandom() {
		try {
			secureRandom = new SecureRandom().getInstanceStrong();
			log.info("init getRandom .. ");
		} catch (NoSuchAlgorithmException e) {
			log.error(e.toString());
		}
	}
	
//	TODO 暫時使用 理論上要從MS_TW 回覆
	public  void getMessageByMsgCode(Object result) {
		log.trace("result>>{}", result);
		String msg = "";
		if (result != null) {
//			BaseResult bs = new BaseResult();
//			CodeUtil.convert2BaseResult(bs, result);
			msg = ((BaseRestBean) result).getMsgName();
			log.trace("msg0>>{}",msg);
			log.trace("MsgCode>>{}",((BaseRestBean) result).getMsgCode());
			if("0".equals(((BaseRestBean) result).getMsgCode())) {
				log.trace("MsgCode is 0  return..");
				return;
			}
			if (StrUtil.isEmpty(msg)) {

				msg = getMessageByMsgCode(((BaseRestBean) result).getMsgCode());
				((BaseRestBean) result).setMsgName(msg);
				log.trace("msg>>{}", ((BaseRestBean) result).getMsgName());

			}
		}

	}
	
	/**
	 * 根據訊息代碼取得訊息
	 * @param msgCode
	 * @return
	 */
	public  String getMessageByMsgCode(String msgCode ) {
		String message= "";
		ADMMSGCODE po =null;
		try {
//			AdmMsgCodeDao admMsgCodeDao= SpringBeanFactory.getBean("admMsgCodeDao");
			log.trace("admMsgCodeDao>>{}",admMsgCodeDao);
			if(admMsgCodeDao.hasMsg(msgCode, null)) {
				po = admMsgCodeDao.get(ADMMSGCODE.class, msgCode);
				message = po.getADMSGIN();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			log.error("",e);
		}
		log.trace("message>>{}",message);
		return message;
	}
	
	
	
	
	/**
	 * 電文傳送 RESTful
	 * @param sendClass
	 * @param retClass
	 * @param api
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public  <T> T send(Object sendClass , Class<?> retClass ,String api ) {
		String url = "";
		String sendTime = "";
		Object result = null;
		Object retobj = null;
		RestTemplate restTemplate = null;
		HashMap<String, String> sendMap = null;
		DateTime d1 = null;
		//GET 使用getForObject
//	    result = restTemplate.getForObject(REST_TW_URL, objclass, params);
		try {
			url =api;
			
			log.trace(ESAPIUtil.vaildLog("url >> " + url)); 
			sendMap = CodeUtil.objectCovert(HashMap.class, sendClass);
//			log.trace(ESAPIUtil.vaildLog("sendMap >> " + sendMap)); 
			if(sendMap ==null) {
				throw new Exception("電文轉換異常，此電文無法送出");
			}
			log.trace(ESAPIUtil.vaildLog("sendJSON >> " + CodeUtil.toJsonCustom(sendMap))); 
			restTemplate = setHttpConfig();
			d1 = new DateTime();
//			result = restTemplate.postForObject(url,  sendMap ,HashMap.class );
			
//			HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
			log.debug("request>>{}",request);
			String trace_cusidn = (String) request.getSession().getAttribute(SessionUtil.CUSIDN);
			log.debug("trace_cusidn>>{}",trace_cusidn);
			String trace_userid = (String) request.getSession().getAttribute(SessionUtil.USERID);
			log.debug("trace_userid>>{}",trace_userid);
			String trace_adguid = (String) request.getAttribute(SessionUtil.ADGUID);
//			String trace_adguid = (String) request.getSession().getAttribute(SessionUtil.ADGUID);
			log.info("trace_adguid>>{}",trace_adguid);
//			壓力測試用
			if(StrUtils.isNotEmpty(request.getParameter("PUUID"))) {
				trace_adguid = request.getParameter("PUUID");
			}
			
			
			trace_adguid = StrUtil.isEmpty(trace_adguid)?UUID.randomUUID().toString():trace_adguid;
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.set("trace_cusidn", trace_cusidn);
			headers.set("trace_userid", trace_userid);
			headers.set("trace_adguid", trace_adguid);
			
			HttpEntity<Map<String, String>> entity = new HttpEntity<>(sendMap, headers);
			
			url = "Y".equals(isOpenLoadBlance)? getLoadBlance(url) :url;
			log.info("before postForEntity diffTime>>{}", DateUtil.getDiffTimeMills(d1, new DateTime()));
			
			d1 = new DateTime();
			ResponseEntity<HashMap> re  = restTemplate.postForEntity(url, entity, HashMap.class );
			
			result = re.getBody();
			log.info(ESAPIUtil.vaildLog("rest >> " + url + "，收送花費時間  >> "+DateUtil.getDiffTimeMills(d1, new DateTime()))); 
			d1 = new DateTime();
		
			log.trace(ESAPIUtil.vaildLog("result >> " + result)); 

			log.trace(ESAPIUtil.vaildLog("result.toJson >> " + CodeUtil.toJson(result))); 
			log.info("trace diffTime>>{}", DateUtil.getDiffTimeMills(d1, new DateTime()));

			if(result == null) {
				throw new Exception("result == null ,電文接收異常");
			}
			
			d1 = new DateTime();
			retobj = CodeUtil.objectCovert(retClass   ,result);
			log.info("objectCovert diffTime>>{}", DateUtil.getDiffTimeMills(d1, new DateTime()));
			log.trace(ESAPIUtil.vaildLog("retobj >> " + retobj)); 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			log.error("",e);
		}
		
		
		return (T) retobj;
		
	}
	/**
	 * 使用@PostConstruct 時，用此方法
	 * @param sendClass
	 * @param retClass
	 * @param api
	 * @return
	 */
	public  <T> T sendForPostConstruct(Object sendClass , Class<?> retClass ,String api ) {
		String url = "";
		Object result = null;
		Object retobj = null;
		RestTemplate restTemplate = null;
		HashMap<String, String> sendMap = null;
		DateTime d1 = null;
		//GET 使用getForObject
//	    result = restTemplate.getForObject(REST_TW_URL, objclass, params);
		try {
			url =api;
			
			log.trace(ESAPIUtil.vaildLog("url >> " + url)); 
			sendMap = CodeUtil.objectCovert(HashMap.class, sendClass);
			log.trace(ESAPIUtil.vaildLog("sendMap >> " + sendMap)); 
			if(sendMap ==null) {
				throw new Exception("電文轉換異常，此電文無法送出");
			}
			log.trace(ESAPIUtil.vaildLog("sendJSON >> " + CodeUtil.toJson(sendMap))); 
			restTemplate = setHttpConfig();
			d1 = new DateTime();
			
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
//			headers.set("trace_cusidn", trace_cusidn);
//			headers.set("trace_userid", trace_userid);
//			headers.set("trace_adguid", trace_adguid);
			
			HttpEntity<Map<String, String>> entity = new HttpEntity<>(sendMap, headers);
			
			url = "Y".equals(isOpenLoadBlance)? getLoadBlance(url) :url;
			log.info("before postForEntity diffTime>>{}", DateUtil.getDiffTimeMills(d1, new DateTime()));
			
			d1 = new DateTime();
			ResponseEntity<HashMap> re  = restTemplate.postForEntity(url, entity, HashMap.class );
			
			result = re.getBody();
			log.info(ESAPIUtil.vaildLog("rest >> " + url + "，收送花費時間  >> "+DateUtil.getDiffTimeMills(d1, new DateTime()))); 
			d1 = new DateTime();
			
			log.trace(ESAPIUtil.vaildLog("result >> " + result)); 
			
			log.trace(ESAPIUtil.vaildLog("result.toJson >> " + CodeUtil.toJson(result))); 
			log.info("trace diffTime>>{}", DateUtil.getDiffTimeMills(d1, new DateTime()));
			
			if(result == null) {
				throw new Exception("result == null ,電文接收異常");
			}
			
			d1 = new DateTime();
			retobj = CodeUtil.objectCovert(retClass   ,result);
			log.info("objectCovert diffTime>>{}", DateUtil.getDiffTimeMills(d1, new DateTime()));
			log.trace(ESAPIUtil.vaildLog("retobj >> " + retobj)); 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			log.error("",e);
		}
		
		
		return (T) retobj;
		
	}
	
	
	
	public String getLoadBlance(String url) {
		String aurl = "";
		String ap = "";
		URI uri;
		log.trace(ESAPIUtil.vaildLog("url>>"+url));
		try {
			uri = new URI(url);
			String domain = uri.getHost();
			log.trace(ESAPIUtil.vaildLog("domain>>"+domain));
			
			ap = getAPIP();
			
			aurl = url.replace(domain, ap);
			log.trace(ESAPIUtil.vaildLog("aurl>>"+aurl));
			
			
		} catch (URISyntaxException e) {
			log.error(e.toString());
			aurl = url;
		}
		
		return aurl;
		
	}
	
	public String getAPIP() {
		Integer i =0;
		String ap = apache_ap1; //預設
		try {
			i =secureRandom.nextInt();
			log.debug("IS_AP1_LIVE>>{} , IS_AP2_LIVE>>{}",CodeUtil.IS_AP1_LIVE,CodeUtil.IS_AP2_LIVE);
			if(i%2==0 ) {
				log.trace("偶數=AP2>>{}"+i);
				ap = apache_ap2;
				if(!CodeUtil.IS_AP2_LIVE ) {
					ap = apache_ap1;
				}
			}else {
				log.trace("奇數=AP1>>{}"+i);
				ap = apache_ap1;
				if(!CodeUtil.IS_AP1_LIVE ) {
					ap = apache_ap2;
				}
			}
		} catch (Exception e) {
			log.error(e.toString());
		}
		return ap;
		
	}
	
	
	
	/**
	 * 設定http連線參數
	 * @return
	 */
	
	public RestTemplate setHttpConfig() {
		Integer socketTimeoutTomstw = socketTimeout;
//		Integer socketTimeoutTomstw = SpringBeanFactory.getBean("socketTimeout");
		Integer connectTimeout = 5*1000;
		Integer socketTimeout = socketTimeoutTomstw*1000;
		HttpComponentsClientHttpRequestFactory rf = new HttpComponentsClientHttpRequestFactory();
		rf.setConnectTimeout(connectTimeout);
		rf.setReadTimeout(socketTimeout);
		return new RestTemplate(rf);
		
	}
	
	
	/**
	 * 測試用
	 */
	public void setHttpConfigI() {
		Integer connectTimeout = 5*1000;
		Integer socketTimeout = 6*1000;
		
		RequestConfig requestConfig;
		CloseableHttpClient httpClient;
		
		
		requestConfig = RequestConfig.custom()
				.setConnectTimeout(connectTimeout)
				.setSocketTimeout(socketTimeout)
				.build();
		httpClient = HttpClientBuilder.create()
				.setDefaultRequestConfig(requestConfig)
				.build();
		
		
	}
	
	
	public Map<String, String> query(final Map<String, String> params, String url, String cert, String soci, boolean auth) {
		
		String retJSON = null;
		try {
			retJSON = sendHttps(params, url, 15, cert, soci, auth);
			Map<String, String> result = CodeUtil.fromJson(retJSON, Map.class);
            return result;
            
		} catch (Throwable e) {
			log.error("", e);
			throw e;
		}
	}
	
	public String sendHttps(Map<String, String> params, String url, Integer timeout, String cert_path, String soci, boolean auth) {
		String result = null;
		RestTemplate restTemplate = null;
		try {
			log.trace(ESAPIUtil.vaildLog("sendHttps.url: {}" + url));
			log.trace(ESAPIUtil.vaildLog("sendHttps.params: {}" + CodeUtil.toJson(params)));
			
			if(!params.isEmpty()) {
				log.trace(ESAPIUtil.vaildLog("RESTfulClient.params.toJson: {}" + CodeUtil.toJson(params)));
			}
			
			restTemplate = setHttpConfigIIII(timeout, cert_path, soci, auth);
			
			result = restTemplate.postForObject(url, params, String.class);
			
			log.trace(ESAPIUtil.vaildLog("sendHttps.result: {}" + result));
			
			if(result == null) {
				throw new Exception("result == null ,電文接收異常");
			}
			
		} catch (ResourceAccessException e) {
			log.error("",e);
			throw e;
			
		} catch (Exception e) {
			log.error("",e);
		}
		
		return result;
	}
	
	/**
	 * 設定https連線參數
	 */
	public RestTemplate setHttpConfigIIII(Integer timeout, String cert_path, String soci, boolean auth) {
		// 新建RestTemplate对象
        RestTemplate restTemplate = null;
        		
        Integer readTimeout = timeout*1000;
		Integer connectTimeout = 5*1000;
		Integer connectionRequestTimeout = 5*1000;
		
		try {
			// 在握手期間，如果URL的主機名和服務器的標識主機名不匹配，則驗證機制可以通過此接口的實現，程序來確定是否應該允許此連接
			HostnameVerifier hv = new HostnameVerifier() {
				@Override
				public boolean verify(String urlHostName, SSLSession session) {
					return true;
				}
			};
			HttpsURLConnection.setDefaultHostnameVerifier(hv);
			
            SSLConnectionSocketFactory ssLSocketFactory = buildSSLSocketFactory("JKS", cert_path, soci, Lists.newArrayList("TLSv1.2"), auth);
            
            // Spring提供HttpComponentsClientHttpRequestFactory指定使用HttpClient作為底層實現HTTP請求
            HttpComponentsClientHttpRequestFactory httpRequestFactory = new HttpComponentsClientHttpRequestFactory(
                    HttpClients.custom().setSSLSocketFactory(ssLSocketFactory).build()
            );
            
            // 數據傳遞Timeout
            httpRequestFactory.setReadTimeout(readTimeout);
            // 連線Timeout
            httpRequestFactory.setConnectTimeout(connectTimeout);
            // request連線Timeout
            httpRequestFactory.setConnectionRequestTimeout(connectionRequestTimeout);
            
            
            restTemplate = new RestTemplate(httpRequestFactory);

		} catch (Exception e) {
			log.error(e.toString());
		}
		return restTemplate;
	}
	
	/**
     * 建構SSLSocketFactory
     *
     * @param keyStoreType
     * @param keyFilePath
     * @param keyPassword
     * @param sslProtocols
     * @param auth 是否驗證憑證
     * @return
     * @throws Exception
     */
    private SSLConnectionSocketFactory buildSSLSocketFactory(String keyStoreType, String certPath, 
				String soci, List<String> sslProtocols, boolean auth) throws Exception {
    	
        // 證書管理器，指定證書及證書類型
        KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        // KeyStore用於存放證書，創建對象時指定交換數字證書的加密標準
        KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
		keyStore.load(
			new FileInputStream(new File(certPath)),
			soci.toCharArray()
		);
        keyManagerFactory.init(keyStore, soci.toCharArray());
        
        SSLContext sslContext = SSLContext.getInstance("SSL");
        if (!auth) {
            // 設置信任證書（繞過TrustStore驗證）
            TrustManager[] trustAllCerts = new TrustManager[1];
            TrustManager trustManager = new AuthX509TrustManager();
            trustAllCerts[0] = trustManager;
            sslContext.init(keyManagerFactory.getKeyManagers(), trustAllCerts, null);
            HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
            
        } else {
        	log.debug(ESAPIUtil.vaildLog("buildSSLSocketFactory.certPath: {}"+certPath));
            // 加載證書材料，重建ssl
            sslContext = SSLContexts.custom().loadTrustMaterial(new File(certPath), soci.toCharArray(), new TrustSelfSignedStrategy()).build();
        }

        SSLConnectionSocketFactory sslConnectionSocketFactory =
                new SSLConnectionSocketFactory(sslContext, sslProtocols.toArray(new String[sslProtocols.size()]),
                        null,
                        new HostnameVerifier() {
                            // 這裡不校驗主機名
                            @Override
                            public boolean verify(String urlHostName, SSLSession session) {
                                return true;
                            }
                        });

        return sslConnectionSocketFactory;
    }
    
}
