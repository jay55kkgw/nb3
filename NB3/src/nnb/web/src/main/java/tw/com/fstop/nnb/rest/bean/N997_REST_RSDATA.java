package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N997_REST_RSDATA extends BaseRestBean implements Serializable{
	private static final long serialVersionUID = -7445173263967145749L;

	private String DPACCSETID;
	private String DPUSERID;
	private String DPGONAME;
	private String DPTRACNO;
	private String DPTRIBANK;
	private String DPTRDACNO;
	private String LASTDATE;
	private String LASTTIME;
	private String DPPHOTOID;
	
	public String getDPACCSETID(){
		return DPACCSETID;
	}
	public void setDPACCSETID(String dPACCSETID){
		DPACCSETID = dPACCSETID;
	}
	public String getDPUSERID(){
		return DPUSERID;
	}
	public void setDPUSERID(String dPUSERID){
		DPUSERID = dPUSERID;
	}
	public String getDPGONAME(){
		return DPGONAME;
	}
	public void setDPGONAME(String dPGONAME){
		DPGONAME = dPGONAME;
	}
	public String getDPTRACNO(){
		return DPTRACNO;
	}
	public void setDPTRACNO(String dPTRACNO){
		DPTRACNO = dPTRACNO;
	}
	public String getDPTRIBANK(){
		return DPTRIBANK;
	}
	public void setDPTRIBANK(String dPTRIBANK){
		DPTRIBANK = dPTRIBANK;
	}
	public String getDPTRDACNO(){
		return DPTRDACNO;
	}
	public void setDPTRDACNO(String dPTRDACNO){
		DPTRDACNO = dPTRDACNO;
	}
	public String getLASTDATE(){
		return LASTDATE;
	}
	public void setLASTDATE(String lASTDATE){
		LASTDATE = lASTDATE;
	}
	public String getLASTTIME(){
		return LASTTIME;
	}
	public void setLASTTIME(String lASTTIME){
		LASTTIME = lASTTIME;
	}
	public String getDPPHOTOID(){
		return DPPHOTOID;
	}
	public void setDPPHOTOID(String dPPHOTOID){
		DPPHOTOID = dPPHOTOID;
	}
}