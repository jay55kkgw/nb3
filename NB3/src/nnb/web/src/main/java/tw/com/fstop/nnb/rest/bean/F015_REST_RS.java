package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * 
 * 功能說明 :線上申請外匯存款帳戶結清銷戶結清資料
 *
 */
public class F015_REST_RS extends BaseRestBean implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7022109449774108056L;

	private String CMQTIME;// 交易時間

	private String TX_CODE;// 交易代號

	private String FILLER;// RESERVE

	private String BRH_COD;// 分行別

	private String TC_NO;// TC號碼

	private String WK_ID;// 工作站代號

	private String TX_ID;// 程式代號

	private String STATUS;// 狀態

	private String PF_KEY;// PF KEY

	private String MSG_CODE;// 訊息代碼

	private String INDICTOR;// 燈號控制碼

	private String TL_ID;// 櫃員代號

	private String TX_SEQ;// 交易序號

	private String ROE;// 使用匯率

	private String AMTCLS;// 結清金額

	private String INT;// 利息

	private String FYTAX;// 稅款

	private String PAIAFTX;// 稅後本息

	private String NTINT;// 臺幣利息

	private String NTTAX;// 臺幣稅款

	private String FYNHITAX;// 外幣補充保費

	private String NTNHITAX;// 臺幣補充保費

	private String NHICOD;// 免扣取對象

	private String MSGCOD;// 訊息

	public String getCMQTIME()
	{
		return CMQTIME;
	}

	public void setCMQTIME(String cMQTIME)
	{
		CMQTIME = cMQTIME;
	}

	public String getTX_CODE()
	{
		return TX_CODE;
	}

	public void setTX_CODE(String tX_CODE)
	{
		TX_CODE = tX_CODE;
	}

	public String getFILLER()
	{
		return FILLER;
	}

	public void setFILLER(String fILLER)
	{
		FILLER = fILLER;
	}

	public String getBRH_COD()
	{
		return BRH_COD;
	}

	public void setBRH_COD(String bRH_COD)
	{
		BRH_COD = bRH_COD;
	}

	public String getTC_NO()
	{
		return TC_NO;
	}

	public void setTC_NO(String tC_NO)
	{
		TC_NO = tC_NO;
	}

	public String getWK_ID()
	{
		return WK_ID;
	}

	public void setWK_ID(String wK_ID)
	{
		WK_ID = wK_ID;
	}

	public String getTX_ID()
	{
		return TX_ID;
	}

	public void setTX_ID(String tX_ID)
	{
		TX_ID = tX_ID;
	}

	public String getSTATUS()
	{
		return STATUS;
	}

	public void setSTATUS(String sTATUS)
	{
		STATUS = sTATUS;
	}

	public String getPF_KEY()
	{
		return PF_KEY;
	}

	public void setPF_KEY(String pF_KEY)
	{
		PF_KEY = pF_KEY;
	}

	public String getMSG_CODE()
	{
		return MSG_CODE;
	}

	public void setMSG_CODE(String mSG_CODE)
	{
		MSG_CODE = mSG_CODE;
	}

	public String getINDICTOR()
	{
		return INDICTOR;
	}

	public void setINDICTOR(String iNDICTOR)
	{
		INDICTOR = iNDICTOR;
	}

	public String getTL_ID()
	{
		return TL_ID;
	}

	public void setTL_ID(String tL_ID)
	{
		TL_ID = tL_ID;
	}

	public String getTX_SEQ()
	{
		return TX_SEQ;
	}

	public void setTX_SEQ(String tX_SEQ)
	{
		TX_SEQ = tX_SEQ;
	}

	public String getROE()
	{
		return ROE;
	}

	public void setROE(String rOE)
	{
		ROE = rOE;
	}

	public String getAMTCLS()
	{
		return AMTCLS;
	}

	public void setAMTCLS(String aMTCLS)
	{
		AMTCLS = aMTCLS;
	}

	public String getINT()
	{
		return INT;
	}

	public void setINT(String iNT)
	{
		INT = iNT;
	}

	public String getFYTAX()
	{
		return FYTAX;
	}

	public void setFYTAX(String fYTAX)
	{
		FYTAX = fYTAX;
	}

	public String getPAIAFTX()
	{
		return PAIAFTX;
	}

	public void setPAIAFTX(String pAIAFTX)
	{
		PAIAFTX = pAIAFTX;
	}

	public String getNTINT()
	{
		return NTINT;
	}

	public void setNTINT(String nTINT)
	{
		NTINT = nTINT;
	}

	public String getNTTAX()
	{
		return NTTAX;
	}

	public void setNTTAX(String nTTAX)
	{
		NTTAX = nTTAX;
	}

	public String getFYNHITAX()
	{
		return FYNHITAX;
	}

	public void setFYNHITAX(String fYNHITAX)
	{
		FYNHITAX = fYNHITAX;
	}

	public String getNTNHITAX()
	{
		return NTNHITAX;
	}

	public void setNTNHITAX(String nTNHITAX)
	{
		NTNHITAX = nTNHITAX;
	}

	public String getNHICOD()
	{
		return NHICOD;
	}

	public void setNHICOD(String nHICOD)
	{
		NHICOD = nHICOD;
	}

	public String getMSGCOD()
	{
		return MSGCOD;
	}

	public void setMSGCOD(String mSGCOD)
	{
		MSGCOD = mSGCOD;
	}

}
