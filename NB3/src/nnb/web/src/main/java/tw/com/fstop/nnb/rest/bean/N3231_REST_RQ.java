package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N3231_REST_RQ extends BaseRestBean_FUND implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6882302173209929244L;
	String UID;

	public String getUID() {
		return UID;
	}

	public void setUID(String uID) {
		UID = uID;
	}
}
