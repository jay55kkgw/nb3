package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.Map;

/**
 * 轉帳交易前代理行查詢收款戶資訊
 * 
 * @author Vincenthuang
 *
 */
public class MtpPreTransferInfoQry_REST_RQ extends BaseRestBean_QR implements Serializable {

	//=========其餘參數===========
	private String ADOPID = "MtpInfoQry";//紀錄TXNLOG用
	
	//=========API參數===========
	private String bankcode;					//銀行代碼
	private String mobilephone;			//手機門號
	
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getBankCode() {
		return bankcode;
	}
	public void setBankCode(String bankCode) {
		this.bankcode = bankCode;
	}
	public String getMobilephone() {
		return mobilephone;
	}
	public void setMobilephone(String mobilephone) {
		this.mobilephone = mobilephone;
	}
}
