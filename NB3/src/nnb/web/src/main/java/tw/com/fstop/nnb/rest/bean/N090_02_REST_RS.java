package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N090_02_REST_RS extends BaseRestBean implements Serializable{
	/**
	 * 黃金回售RS
	 */
	private static final long serialVersionUID = 8724524347206873879L;
	
	private String CMQTIME;
	private String TRNDATE;
	private String TRNTIME;
	private String TRNSRC;
	private String TRNTYP;
	private String TRNCOD;
	private String TRNBDT;
	private String ACN;
	private String CUSIDN;
	private String SVACN;
	private String TRNGD;
	private String PRICE;
	private String TRNAMT;
	private String FEEAMT1;
	private String FEEAMT2;
	private String TRNAMT2;
	
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getTRNDATE() {
		return TRNDATE;
	}
	public void setTRNDATE(String tRNDATE) {
		TRNDATE = tRNDATE;
	}
	public String getTRNTIME() {
		return TRNTIME;
	}
	public void setTRNTIME(String tRNTIME) {
		TRNTIME = tRNTIME;
	}
	public String getTRNSRC() {
		return TRNSRC;
	}
	public void setTRNSRC(String tRNSRC) {
		TRNSRC = tRNSRC;
	}
	public String getTRNTYP() {
		return TRNTYP;
	}
	public void setTRNTYP(String tRNTYP) {
		TRNTYP = tRNTYP;
	}
	public String getTRNCOD() {
		return TRNCOD;
	}
	public void setTRNCOD(String tRNCOD) {
		TRNCOD = tRNCOD;
	}
	public String getTRNBDT() {
		return TRNBDT;
	}
	public void setTRNBDT(String tRNBDT) {
		TRNBDT = tRNBDT;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getSVACN() {
		return SVACN;
	}
	public void setSVACN(String sVACN) {
		SVACN = sVACN;
	}
	public String getTRNGD() {
		return TRNGD;
	}
	public void setTRNGD(String tRNGD) {
		TRNGD = tRNGD;
	}
	public String getPRICE() {
		return PRICE;
	}
	public void setPRICE(String pRICE) {
		PRICE = pRICE;
	}
	public String getTRNAMT() {
		return TRNAMT;
	}
	public void setTRNAMT(String tRNAMT) {
		TRNAMT = tRNAMT;
	}
	public String getFEEAMT1() {
		return FEEAMT1;
	}
	public void setFEEAMT1(String fEEAMT1) {
		FEEAMT1 = fEEAMT1;
	}
	public String getFEEAMT2() {
		return FEEAMT2;
	}
	public void setFEEAMT2(String fEEAMT2) {
		FEEAMT2 = fEEAMT2;
	}
	public String getTRNAMT2() {
		return TRNAMT2;
	}
	public void setTRNAMT2(String tRNAMT2) {
		TRNAMT2 = tRNAMT2;
	}
}