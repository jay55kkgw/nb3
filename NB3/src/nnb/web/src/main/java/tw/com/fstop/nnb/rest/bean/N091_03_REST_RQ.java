package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N091_03_REST_RQ extends BaseRestBean_GOLD implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5863321392629300639L;
	
	private String UID;
	private String ADOPID;
	private String PINNEW;			//網路銀行密碼
	private String TRNSRC = "NB";	//交易來源
	private String ACN;				//黃金存摺帳號
	private String SVACN;			//台幣存款帳號
	private String APPTYPE;			//預約交易種類
	private String TRNGD;			//黃金買進公克數
	private String APPDATE;			//預約日期
	private String APPTIME;			//預約時間
	private String MAC;				//MAC
	private String FGTXWAY;			//交易形態
	private String DPMYEMAIL;		//EMail DB寫入資料
	
	private String SYNC;			//Sync.Check Item
	private String PPSYNC;			//P.P.Key Sync.Check Item
	private String PINKEY;			//網路銀行密碼
	private String CERTACN;			//憑證帳號
	private String FLAG;			//0：非約定，1：約定
	private String BNKRA;			//行庫別
	private String XMLCA;			//CA識別碼
	private String XMLCN;			//憑證CN
	private String PPSYNCN;			//P.P.Key Sync.Check Item 16
	private String TRNTYP;			//交易種類
	private String TRNCOD;			//交易註記碼
	private String TRNBDT;			//交易日期
	private String pkcs7Sign;	// IKEY
	private String jsondc;		// IKEY

	//IDGATE
	private String sessionID;
	private String idgateID;
	private String txnID;
	
	
	public String getSessionID() {
		return sessionID;
	}
	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}
	public String getIdgateID() {
		return idgateID;
	}
	public void setIdgateID(String idgateID) {
		this.idgateID = idgateID;
	}
	public String getTxnID() {
		return txnID;
	}
	public void setTxnID(String txnID) {
		this.txnID = txnID;
	}
	public String getPkcs7Sign() {
		return pkcs7Sign;
	}
	public void setPkcs7Sign(String pkcs7Sign) {
		this.pkcs7Sign = pkcs7Sign;
	}
	public String getJsondc() {
		return jsondc;
	}
	public void setJsondc(String jsondc) {
		this.jsondc = jsondc;
	}
	public String getDPMYEMAIL() {
		return DPMYEMAIL;
	}
	public void setDPMYEMAIL(String dPMYEMAIL) {
		DPMYEMAIL = dPMYEMAIL;
	}
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getPINNEW() {
		return PINNEW;
	}
	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}
	public String getTRNSRC() {
		return TRNSRC;
	}
	public void setTRNSRC(String tRNSRC) {
		TRNSRC = tRNSRC;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getSVACN() {
		return SVACN;
	}
	public void setSVACN(String sVACN) {
		SVACN = sVACN;
	}
	public String getAPPTYPE() {
		return APPTYPE;
	}
	public void setAPPTYPE(String aPPTYPE) {
		APPTYPE = aPPTYPE;
	}
	public String getTRNGD() {
		return TRNGD;
	}
	public void setTRNGD(String tRNGD) {
		TRNGD = tRNGD;
	}
	public String getAPPDATE() {
		return APPDATE;
	}
	public void setAPPDATE(String aPPDATE) {
		APPDATE = aPPDATE;
	}
	public String getAPPTIME() {
		return APPTIME;
	}
	public void setAPPTIME(String aPPTIME) {
		APPTIME = aPPTIME;
	}
	public String getMAC() {
		return MAC;
	}
	public void setMAC(String mAC) {
		MAC = mAC;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getSYNC() {
		return SYNC;
	}
	public void setSYNC(String sYNC) {
		SYNC = sYNC;
	}
	public String getPPSYNC() {
		return PPSYNC;
	}
	public void setPPSYNC(String pPSYNC) {
		PPSYNC = pPSYNC;
	}
	public String getPINKEY() {
		return PINKEY;
	}
	public void setPINKEY(String pINKEY) {
		PINKEY = pINKEY;
	}
	public String getCERTACN() {
		return CERTACN;
	}
	public void setCERTACN(String cERTACN) {
		CERTACN = cERTACN;
	}
	public String getFLAG() {
		return FLAG;
	}
	public void setFLAG(String fLAG) {
		FLAG = fLAG;
	}
	public String getBNKRA() {
		return BNKRA;
	}
	public void setBNKRA(String bNKRA) {
		BNKRA = bNKRA;
	}
	public String getXMLCA() {
		return XMLCA;
	}
	public void setXMLCA(String xMLCA) {
		XMLCA = xMLCA;
	}
	public String getXMLCN() {
		return XMLCN;
	}
	public void setXMLCN(String xMLCN) {
		XMLCN = xMLCN;
	}
	public String getPPSYNCN() {
		return PPSYNCN;
	}
	public void setPPSYNCN(String pPSYNCN) {
		PPSYNCN = pPSYNCN;
	}
	public String getTRNTYP() {
		return TRNTYP;
	}
	public void setTRNTYP(String tRNTYP) {
		TRNTYP = tRNTYP;
	}
	public String getTRNCOD() {
		return TRNCOD;
	}
	public void setTRNCOD(String tRNCOD) {
		TRNCOD = tRNCOD;
	}
	public String getTRNBDT() {
		return TRNBDT;
	}
	public void setTRNBDT(String tRNBDT) {
		TRNBDT = tRNBDT;
	}
}
