package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N2042_REST_RS extends BaseRestBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5908761681156445685L;
	private String ERRCODE;
	private String FGTXWAY;
	private String CUSIDN;
	private String UID;
	private String NAME;
	private String BRHCOD;
	private String BRHNAME;
	
	public String getERRCODE() {
		return ERRCODE;
	}
	public void setERRCODE(String eRRCODE) {
		ERRCODE = eRRCODE;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	public String getBRHCOD() {
		return BRHCOD;
	}
	public void setBRHCOD(String bRHCOD) {
		BRHCOD = bRHCOD;
	}
	public String getBRHNAME() {
		return BRHNAME;
	}
	public void setBRHNAME(String bRHNAME) {
		BRHNAME = bRHNAME;
	}
}
