package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N935_REST_RQ extends BaseRestBean_PS implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5220248968014335229L;

	private String CUSIDN;//統一編號

	private String ADOPID;
	
	public String getCUSIDN()
	{
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN)
	{
		CUSIDN = cUSIDN;
	}

	public String getADOPID() {
		return ADOPID;
	}

	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	
	

}
