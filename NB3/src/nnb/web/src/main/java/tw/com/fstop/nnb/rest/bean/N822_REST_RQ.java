package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N822_REST_RQ extends BaseRestBean_CC implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1217684292267657702L;
	
	private String CUSIDN;//統一編號
	private String CARDNUM;//信用卡卡號
	private String BIRTH;//民國出生年月日
	
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getCARDNUM() {
		return CARDNUM;
	}
	public void setCARDNUM(String cARDNUM) {
		CARDNUM = cARDNUM;
	}
	public String getBIRTH() {
		return BIRTH;
	}
	public void setBIRTH(String bIRTH) {
		BIRTH = bIRTH;
	}
}
