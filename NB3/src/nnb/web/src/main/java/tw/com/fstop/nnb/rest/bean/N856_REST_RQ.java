package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N856_REST_RQ extends BaseRestBean_TW implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2008282879778474821L;

	private String	SrcID ;
	private String	KeyID ;
	private String	DivData ;
	private String	ICV ;
	private String	MAC ;
	private String	STAN ;
	private String	TxnDatetime ;
	private String	OrigTxnDatetime ;
	private String	OrigSTAN ;
	public String getSrcID() {
		return SrcID;
	}
	public void setSrcID(String srcID) {
		SrcID = srcID;
	}
	public String getKeyID() {
		return KeyID;
	}
	public void setKeyID(String keyID) {
		KeyID = keyID;
	}
	public String getDivData() {
		return DivData;
	}
	public void setDivData(String divData) {
		DivData = divData;
	}
	public String getICV() {
		return ICV;
	}
	public void setICV(String iCV) {
		ICV = iCV;
	}
	public String getMAC() {
		return MAC;
	}
	public void setMAC(String mAC) {
		MAC = mAC;
	}
	public String getSTAN() {
		return STAN;
	}
	public void setSTAN(String sTAN) {
		STAN = sTAN;
	}
	public String getTxnDatetime() {
		return TxnDatetime;
	}
	public void setTxnDatetime(String txnDatetime) {
		TxnDatetime = txnDatetime;
	}
	public String getOrigTxnDatetime() {
		return OrigTxnDatetime;
	}
	public void setOrigTxnDatetime(String origTxnDatetime) {
		OrigTxnDatetime = origTxnDatetime;
	}
	public String getOrigSTAN() {
		return OrigSTAN;
	}
	public void setOrigSTAN(String origSTAN) {
		OrigSTAN = origSTAN;
	}
	
}
