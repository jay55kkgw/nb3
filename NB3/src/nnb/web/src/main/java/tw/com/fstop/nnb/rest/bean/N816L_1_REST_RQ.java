package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N816L_1_REST_RQ extends BaseRestBean_CC implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3533299751725538183L;
	
	private String CUSIDN ;	 //
	private String FGTXWAY;  //交易機制 0 SSL
	private String PINNEW;	 //
	private String ArrayParam; //交易資訊
	//TXNLOG
	private String ADGUID;
	private String ADUSERIP;
	
	public String getArrayParam() {
		return ArrayParam;
	}
	public void setArrayParam(String arrayParam) {
		ArrayParam = arrayParam;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public String getPINNEW() {
		return PINNEW;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
}
