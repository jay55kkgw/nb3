package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * C012電文RQ
 */
public class C012_REST_RQ extends BaseRestBean_FUND implements Serializable{
	private static final long serialVersionUID = 6868861910004819383L;
	
	private String NEXT;
	private String RECNO;
	private String CUSIDN;
	private String MAC;

	// 是否跳過TXNLOG
	private String ADOPID;
	
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getNEXT(){
		return NEXT;
	}
	public void setNEXT(String nEXT){
		NEXT = nEXT;
	}
	public String getRECNO(){
		return RECNO;
	}
	public void setRECNO(String rECNO){
		RECNO = rECNO;
	}
	public String getCUSIDN(){
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN){
		CUSIDN = cUSIDN;
	}
	public String getMAC(){
		return MAC;
	}
	public void setMAC(String mAC){
		MAC = mAC;
	}
}