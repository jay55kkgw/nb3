package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class N074_IDGATE_DATA implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -762951082705400880L;

	@SerializedName(value = "CMDATE")
	private String CMDATE;
	@SerializedName(value = "ACN")
	private String ACN;
	@SerializedName(value = "DPAGACNO")
	private String DPAGACNO;
	@SerializedName(value = "DPACNO")
	private String DPACNO;
	@SerializedName(value = "AMOUNT")
	private String AMOUNT;
	
	@SerializedName(value = "FDPACC")
	private String FDPACC;
	@SerializedName(value = "TERM")
	private String TERM;
	@SerializedName(value = "CMDATE1")
	private String CMDATE1;
	@SerializedName(value = "INTMTH")
	private String INTMTH;
	@SerializedName(value = "CODE")
	private String CODE;
	
	@SerializedName(value = "FGSVTYPE")
	private String FGSVTYPE;

	public String getCMDATE() {
		return CMDATE;
	}

	public void setCMDATE(String cMDATE) {
		CMDATE = cMDATE;
	}

	public String getACN() {
		return ACN;
	}

	public void setACN(String aCN) {
		ACN = aCN;
	}

	public String getDPAGACNO() {
		return DPAGACNO;
	}

	public void setDPAGACNO(String dPAGACNO) {
		DPAGACNO = dPAGACNO;
	}

	public String getDPACNO() {
		return DPACNO;
	}

	public void setDPACNO(String dPACNO) {
		DPACNO = dPACNO;
	}

	public String getAMOUNT() {
		return AMOUNT;
	}

	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}

	public String getFDPACC() {
		return FDPACC;
	}

	public void setFDPACC(String fDPACC) {
		FDPACC = fDPACC;
	}

	public String getTERM() {
		return TERM;
	}

	public void setTERM(String tERM) {
		TERM = tERM;
	}

	public String getCMDATE1() {
		return CMDATE1;
	}

	public void setCMDATE1(String cMDATE1) {
		CMDATE1 = cMDATE1;
	}

	public String getINTMTH() {
		return INTMTH;
	}

	public void setINTMTH(String iNTMTH) {
		INTMTH = iNTMTH;
	}

	public String getCODE() {
		return CODE;
	}

	public void setCODE(String cODE) {
		CODE = cODE;
	}

	public String getFGSVTYPE() {
		return FGSVTYPE;
	}

	public void setFGSVTYPE(String fGSVTYPE) {
		FGSVTYPE = fGSVTYPE;
	}
	
	 
	
	
	
	
	
	
	
	
	
	
	
}
