package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N206_REST_RQ extends BaseRestBean_OLS implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -6070822040275633750L;
	private String DATE;
	private String TIME;
	private String CUSIDN;
	private String ACN;

	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}

	public String getACN() {
		return ACN;
	}

	public void setACN(String aCN) {
		ACN = aCN;
	}

}
