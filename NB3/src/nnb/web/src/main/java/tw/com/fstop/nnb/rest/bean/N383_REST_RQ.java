package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N383_REST_RQ extends BaseRestBean_FUND implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6169120777423773477L;
	
	private String	ADOPID ;
	private String	DATE;
	private String	TIME;
	private String	CUSIDN;
	private String	ACN1;
	private String	ACN2;
	
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getACN1() {
		return ACN1;
	}
	public void setACN1(String aCN1) {
		ACN1 = aCN1;
	}
	public String getACN2() {
		return ACN2;
	}
	public void setACN2(String aCN2) {
		ACN2 = aCN2;
	}
}
