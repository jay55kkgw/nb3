package tw.com.fstop.nnb.spring.controller;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.google.gson.Gson;

import fstop.orm.po.TXNFUNDCOMPANY;
import fstop.orm.po.TXNFUNDDATA;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.aop.Dialog;
import tw.com.fstop.nnb.service.Fund_Purchase_Service;
import tw.com.fstop.nnb.service.Fund_Transfer_Service;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.TxnFundDataDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.StrUtils;
import tw.com.fstop.web.util.WebUtil;

/**
 * 基金單筆申購的Controller
 */
@SessionAttributes({SessionUtil.CUSIDN,SessionUtil.KYCDATE,SessionUtil.WEAK,SessionUtil.DPMYEMAIL,SessionUtil.CONFIRM_LOCALE_DATA,SessionUtil.TRANSFER_DATA,SessionUtil.RESULT_LOCALE_DATA})
@Controller
@RequestMapping(value = "/FUND/PURCHASE")
public class Fund_Purchase_Controller{
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private Fund_Purchase_Service fund_purchase_service;
	@Autowired
	private Fund_Transfer_Service fund_transfer_service;
	@Autowired
	private TxnFundDataDao txnFundDataDao;
	@Autowired
	I18n i18n;
	
	/**
	 * 前往基金申購選擇頁
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/fund_purchase_select")
	public String fund_purchase_select(HttpServletRequest request,@RequestParam Map<String,String> requestParam,Model model){
		String target = "/error";
		BaseResult bs = null;
		log.debug("fund_purchase_select");
		
		try{
			bs = new BaseResult();
			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
			String cusidn = new String(Base64.getDecoder().decode((String)SessionUtil.getAttribute(model,SessionUtil.CUSIDN,null)),"UTF-8");
			log.debug("cusidn={}",cusidn);
			String UID = cusidn;
			model.addAttribute("UID",UID);
			
			bs = fund_transfer_service.getN922Data(cusidn);
			
			if(bs == null || !bs.getResult()){
				model.addAttribute(BaseResult.ERROR,bs);
				return target;
			}
			Map<String,Object> bsData = (Map<String,Object>)bs.getData();
			log.debug("bsData={}",bsData);
			
			String KYCNO = (String)bsData.get("CTDNUM"); //預設為投資屬性評估人員編號
			log.debug("KYCNO={}", KYCNO);
			model.addAttribute("KYCNO", KYCNO);
			
			String FDINVTYPE = (String)bsData.get("FDINVTYPE");
			log.debug("FDINVTYPE={}",FDINVTYPE);
			model.addAttribute("FDINVTYPE",FDINVTYPE);
			model.addAttribute("RISK",FDINVTYPE);
			
			String FDINVTYPEChinese;
			if("1".equals(FDINVTYPE)){
//				FDINVTYPEChinese = "積極型";
				FDINVTYPEChinese = i18n.getMsg("LB.D0945");
			}
			else if("2".equals(FDINVTYPE)){
//				FDINVTYPEChinese = "穩健型";
				FDINVTYPEChinese = i18n.getMsg("LB.X1766");
			}
			else{
//				FDINVTYPEChinese = "保守型";
				FDINVTYPEChinese = i18n.getMsg("LB.X1767");
			}
			log.debug("FDINVTYPEChinese={}",FDINVTYPEChinese);
			model.addAttribute("FDINVTYPEChinese",FDINVTYPEChinese);
			
			String EMPNO = (String)bsData.get("EMPNO");
			log.debug("EMPNO={}",EMPNO);
			model.addAttribute("EMPNO",EMPNO);
			
//			String NAME = (String)bsData.get("NAME");
			
			//20210617修正  若有CUSNAME取CUSNAME 無則為NAME , 不然會是空的 //N922原住民姓名修改由NAME->CUSNAME  
			String NAME = "";
			if(StrUtils.isNotEmpty((String)bsData.get("CUSNAME"))) {
				NAME = (String)bsData.get("CUSNAME");
			}else {
				NAME = (String)bsData.get("NAME");
			}
			log.debug("NAME={}",NAME);
			model.addAttribute("NAME",NAME);
			String hiddenNAME = WebUtil.hideusername1Convert(NAME);
			model.addAttribute("hiddenNAME",hiddenNAME);
			
			//新台幣扣帳帳號
			String ACN1 = (String)bsData.get("ACN1");
			log.debug("ACN1={}",ACN1);
			model.addAttribute("ACN1",ACN1);
			
			//外幣扣帳帳號
			String ACN2 = (String)bsData.get("ACN2");
			log.debug("ACN2={}",ACN2);
			model.addAttribute("ACN2",ACN2);
			
			//簽約行
			String APLBRH = (String)bsData.get("APLBRH");
			log.debug("APLBRH={}",APLBRH);
			model.addAttribute("BRHCOD",APLBRH);
			
			String CUTTYPE = (String)bsData.get("CUTTYPE");
			log.debug("CUTTYPE={}",CUTTYPE);
			model.addAttribute("CUTTYPE",CUTTYPE);
			
			String CUTTYPEString;
			if("01".equals(CUTTYPE)){
				CUTTYPEString = "1";
			}
			else{
				CUTTYPEString = "2";
			}
			log.debug("CUTTYPEString={}",CUTTYPEString);
			model.addAttribute("CUTTYPEString",CUTTYPEString);
			
			//判斷KYC來源為行外且無錄音檔時，轉入錯誤訊息頁面 
			//是否為行外
			String EXTRCE = bsData.get("EXTRCE") == null ? "" : ((String)bsData.get("EXTRCE")).trim();
			log.debug("EXTRCE={}",EXTRCE);
			
			//錄音檔交易序號
			String RECNUM = bsData.get("RECNUM") == null ? "" : ((String)bsData.get("RECNUM")).trim();
			log.debug("RECNUM={}",RECNUM);
			
			if("Y".equals(EXTRCE) && RECNUM.length() == 0){
				bs.setMessage("Z622",i18n.getMsg("LB.X1768"));
				model.addAttribute(BaseResult.ERROR,bs);
				return target;
			}
			
			String isFundTradeTime = fund_transfer_service.isFundTradeTimeType2();
			log.debug("isFundTradeTime={}",isFundTradeTime);
			
			//C不能交易 20210915 > 應該不會有這個情況出現   , 平日交易時間外是預約 , 假日也是預約
			if("C".equals(isFundTradeTime)){
				bs.setMessage("",i18n.getMsg("LB.X1769"));
				model.addAttribute(BaseResult.ERROR,bs);
				return target;
			}
			//B預約申購交易
			else if("B".equals(isFundTradeTime)){
			//假的
//			else if(true){
				target = "forward:/FUND/RESERVE/PURCHASE/fund_reserve_purchase_select";
				return target;
			}
			
//			//20220318 移除此導頁功能 , 改由頁面提示
//			//檢核Email
//			String WANAEMAIL = okMap.get("WANAEMAIL");
//			log.debug(ESAPIUtil.vaildLog("WANAEMAIL={}"+WANAEMAIL));
//			
//			if(!"NONEED".equals(WANAEMAIL)){
//				String DPMYEMAIL = (String)SessionUtil.getAttribute(model,SessionUtil.DPMYEMAIL,null);
//				log.debug("DPMYEMAIL={}",DPMYEMAIL);
//				
//				if(DPMYEMAIL == null || "".equals(DPMYEMAIL)){
//					//設定EMAIL
//					target = "forward:/FUND/TRANSFER/fundemail?ADOPID=FundEmail&TXID=C016";
//					return target;
//				}
//			}
			
			//問卷填寫日期
			String GETLTD = (String)bsData.get("GETLTD");
			log.debug("GETLTD={}",GETLTD);
			
			String KYCDAT = "";
			if(GETLTD.length() == 7 && GETLTD.equals("9999999")){
				KYCDAT = "9" + GETLTD;
			}
			else if(GETLTD.length() == 7 && !GETLTD.equals("9999999")){
				KYCDAT = "0" + GETLTD;
			}
			else{
				KYCDAT = GETLTD;
			}
			log.debug("KYCDAT={}",KYCDAT);
			model.addAttribute("KYCDAT",KYCDAT);
			SessionUtil.addAttribute(model,SessionUtil.KYCDATE,KYCDAT);
			
			//專業投資人屬性
			String RISK7 = bsData.get("RISK7") == null ? "" : ((String)bsData.get("RISK7")).trim();
			log.debug("RISK7={}",RISK7);
			model.addAttribute("RISK7",RISK7);
			//生日
			String BRTHDY = bsData.get("BRTHDY") == null ? "" : (String)bsData.get("BRTHDY");
			log.debug("BRTHDY={}",BRTHDY);
			//學歷
			String DEGREE = bsData.get("DEGREE") == null ? "" : (String)bsData.get("DEGREE");
			log.debug("DEGREE={}",DEGREE);
			//重大傷病
			String MARK1 = bsData.get("MARK1") == null ? "" : (String)bsData.get("MARK1");
			log.debug("MARK1={}",MARK1);
			//一年內信託交易>=5 筆註記
			String MARK3 = bsData.get("MARK3") == null ? "" : (String)bsData.get("MARK3");
			log.debug("MARK3={}",MARK3);
			
			int age = fund_transfer_service.getAge(BRTHDY);
		    
		    String WEAK = "";
		    if(age >= 70){
		    	WEAK = "Y";
		    }
		    else{
		    	WEAK = "N";
		    }
		    if(DEGREE.equals("6")){
		    	WEAK += "Y";
		    }
		    else{
		    	WEAK += "N";
		    }
		    if(MARK1.equals("Y")){
		    	WEAK += "Y";
		    }
		    else{
		    	WEAK += "N";
		    }
		    if(MARK3.equals("Y")){
		    	WEAK += "Y";
		    }
		    else{
		    	WEAK += "N";
		    }
		    log.debug("WEAK={}",WEAK);
		    model.addAttribute("WEAK",WEAK);
		    SessionUtil.addAttribute(model,SessionUtil.WEAK,WEAK);
			
		    String time1 = "";
			String time2 = "";
			long day = 0;
			if(!GETLTD.equals("")){
				time1 = new SimpleDateFormat("yyyy/MM/dd").format(new Date());
				log.debug("time1={}",time1);
				String GETLTDString = Integer.valueOf(GETLTD.substring(0,3)) + 1911 + "";
				GETLTDString = GETLTDString + GETLTD.substring(3);
				log.debug("GETLTDString={}",GETLTDString);
				time2 = GETLTDString.substring(0,4) + "/" + GETLTDString.substring(4,6) + "/" + GETLTDString.substring(6,8);
				log.debug("time2={}",time2);
				
				//算出距離上次問卷填寫天數
				day = fund_transfer_service.getRangeDay(time1,time2);
			}
			
			//問卷版本日期
			String KYCDate = fund_transfer_service.getKYCDate();
			
			if(KYCDate == null){
				KYCDate = "";
			}
			//自然人版， 需有投資屬性及問卷填寫日期已滿一年或問卷版本日期大於問卷填寫日期；專業投資人跳過不判斷KYC
			if(((!FDINVTYPE.equals("") && !GETLTD.equals("") && day >= 365) || ((!GETLTD.equals("") && !KYCDate.equals("")) && Integer.parseInt(KYCDate) > Integer.parseInt(GETLTD))) && UID.length() == 10 && RISK7.length() == 0){
			//假的
//			if(false){
				if(day >= 365){
				//假的
//				if(true){
//				if(false){
				    model.addAttribute("TXID","C016");
				    model.addAttribute("ALERTTYPE","2");
				    
				    fund_transfer_service.prepareFundRiskAlertData(okMap,GETLTD,RISK7,model);
				    
				    target = "/fund/fund_risk_alert";
				}
				else{
					target = "forward:/FUND/TRANSFER/fund_invest_attr1?TXID=C016";
				}
				return target;
		    }
			//法人版，需有投資屬性及問卷填寫日期已滿一年或問卷版本日期大於問卷填寫日期；專業投資人跳過不判斷KYC
			else if(((!FDINVTYPE.equals("") && !GETLTD.equals("") && day >= 365) || ((!GETLTD.equals("") && !KYCDate.equals("")) && Integer.parseInt(KYCDate) > Integer.parseInt(GETLTD))) && UID.length() < 10 && RISK7.length() == 0){  	
//			else if(true){
				if(day>=365){
//				if(false){
					model.addAttribute("TXID","C016");
				    model.addAttribute("ALERTTYPE","2");
				    
				    fund_transfer_service.prepareFundRiskAlertData(okMap,GETLTD,RISK7,model);
				    
				    target = "/fund/fund_risk_alert";
				}
				else{		
					target = "forward:/FUND/TRANSFER/fund_invest_attr3?TXID=C016";
				}
				return target;
		    }
		    else if(FDINVTYPE.equals("") && RISK7.length() == 0){
		     	if(UID.length() == 10){
					target = "forward:/FUND/TRANSFER/fund_invest_attr1?TXID=C016";
		     	}
		    	else{
					target = "forward:/FUND/TRANSFER/fund_invest_attr3?TXID=C016";
		    	}
		     	return target;
		    }
			
			fund_purchase_service.prepareFundPurchaseData(okMap,model);
			
			target = "/fund/fund_purchase_select";
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("fund_purchase_select error >> {}",e);
		}
		return target;
	}
	/**
	 * 基金公開說明書同意，前往基金申購手續費頁
	 */
	@RequestMapping(value = "/fund_purchase_fee_expose")
	public String fund_purchase_fee_expose(HttpServletRequest request,@RequestParam Map<String,String> requestParam,Model model){
		String target = "/error";
		
		log.debug("fund_purchase_fee_expose");
		log.debug(ESAPIUtil.vaildLog("fund_purchase_fee_expose requestParam= " + CodeUtil.toJson(requestParam)));
		BaseResult bs = new BaseResult();
		try{
			String cusidn = new String(Base64.getDecoder().decode((String)SessionUtil.getAttribute(model,SessionUtil.CUSIDN,null)),"UTF-8");
			requestParam.put("CUSIDN",cusidn);
			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
		
			if(hasLocale){
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				log.trace(">>>>>>{}",bs.getResult());
				if(!bs.getResult()) {
					model.addAttribute(BaseResult.ERROR,bs);
					bs.setNext("/FUND/PURCHASE/fund_purchase_select");
					return target;
				}
				Map<String,String> dataMap=(Map<String,String>)SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
				for(String key:dataMap.keySet()) {
					model.addAttribute(key,dataMap.get(key));
				}
				target = "fund/fund_fee_expose";
			}
			else {
				bs = fund_purchase_service.prepareFundPurchaseFeeExposeData(requestParam,model);
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
				if(bs == null || !bs.getResult()){
					model.addAttribute(BaseResult.ERROR,bs);
					bs.setNext("/FUND/PURCHASE/fund_purchase_select");
					return target;
				}
				target = "fund/fund_fee_expose";
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, model);
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("fund_purchase_fee_expose error >> {}",e);
		}
		//判斷視窗註記
		model.addAttribute("C016");
		return target;
	}
	/**
	 * 基金申購說明暨風險預告書
	 */
	@RequestMapping(value = "/fund_purchase_instructions_risk_announcement")
	public String fund_purchase_instructions_risk_announcement(HttpServletRequest request,@RequestParam Map<String,String> requestParam,Model model){
		String target = "/error";
		log.debug("fund_purchase_instructions_risk_announcement");
		log.debug(ESAPIUtil.vaildLog("fund_purchase_instructions_risk_announcement requestParam = " + CodeUtil.toJson(requestParam)));
		
		BaseResult bs = new BaseResult();
		try{
			if (requestParam == null) {
				model.addAttribute(BaseResult.ERROR,bs);
				bs.setNext("/FUND/PURCHASE/fund_purchase_select");
				return target;
			}
			Map<String,String> dataMap = requestParam;
			for(String key:dataMap.keySet()) {
				model.addAttribute(key,dataMap.get(key));
			}
			target = "fund/fund_instructions_risk_announcement";
		}
		catch(Exception e){
			log.error("fund_purchase_instructions_risk_announcement error >> {}",e);
		}
		return target;
	}
	/**
	 * 基金申購高齡聲明書
	 */
	@RequestMapping(value = "/fund_purchase_oflag_announcement")
	public String fund_purchase_oflag_announcement(HttpServletRequest request,@RequestParam Map<String,String> requestParam,Model model){
		String target = "/error";
		log.debug("fund_purchase_oflag_announcement");
		log.debug(ESAPIUtil.vaildLog("fund_purchase_oflag_announcement requestParam = " + CodeUtil.toJson(requestParam)));
		
		BaseResult bs = new BaseResult();
		try{
			if (requestParam == null) {
				model.addAttribute(BaseResult.ERROR,bs);
				bs.setNext("/FUND/PURCHASE/fund_purchase_select");
				return target;
			}
			Map<String,String> dataMap = requestParam;
			for(String key:dataMap.keySet()) {
				model.addAttribute(key,dataMap.get(key));
			}
			target = "fund/fund_oflag_announcement";
		}
		catch(Exception e){
			log.error("fund_purchase_oflag_announcement error >> {}",e);
		}
		return target;
	}
	/**
	 * 前往基金申購確認頁
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/fund_purchase_confirm")
	public String fund_purchase_confirm(HttpServletRequest request,@RequestParam Map<String,String> requestParam,Model model){
		String target = "/error";
		
		log.debug("fund_purchase_confirm");
		log.debug("fund_purchase_confirm requestParam = {}", requestParam);
		try{
			String cusidn = new String(Base64.getDecoder().decode((String)SessionUtil.getAttribute(model,SessionUtil.CUSIDN,null)),"UTF-8");
			log.debug("cusidn={}",cusidn);
			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				BaseResult bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				log.trace(">>>>>>{}",bs.getResult());
				if(!bs.getResult()) {
					model.addAttribute(BaseResult.ERROR,bs);
					return target;
				}
				Map<String,String> dataMap=(Map<String,String>)SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
				for(String key:dataMap.keySet()) {
					model.addAttribute(key,dataMap.get(key));
				}
				target = "fund/fund_purchase_confirm";
			}
			else {
				String hiddenCUSIDN = WebUtil.hideID(cusidn);
				model.addAttribute("hiddenCUSIDN",hiddenCUSIDN);
				
				BaseResult bs = fund_transfer_service.getN922Data(cusidn);
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
				log.trace("test>>>>>>{}",bs.getResult());
				log.trace("test>>>>>>{}",bs.toString());
				if(bs == null || !bs.getResult()){
					model.addAttribute(BaseResult.ERROR,bs);
					bs.setNext("/FUND/PURCHASE/fund_purchase_select");
					return target;
				}
				Map<String,Object> bsData = (Map<String,Object>)bs.getData();
				
//				String NAME = (String)bsData.get("NAME");
				
				//20210617修正  若有CUSNAME取CUSNAME 無則為NAME , 不然會是空的 //N922原住民姓名修改由NAME->CUSNAME  
				String NAME = "";
				if(StrUtils.isNotEmpty((String)bsData.get("CUSNAME"))) {
					NAME = (String)bsData.get("CUSNAME");
				}else {
					NAME = (String)bsData.get("NAME");
				}
				
				log.debug("NAME={}",NAME);
				model.addAttribute("NAME",NAME);
				String hiddenNAME = WebUtil.hideusername1Convert(NAME);
				model.addAttribute("hiddenNAME",hiddenNAME);
				
				fund_purchase_service.prepareFundPurchaseConfirmData(requestParam,model);
				
				target = "fund/fund_purchase_confirm";
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, model);
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("fund_purchase_confirm error >> {}",e);
		}
		return target;
	}
	
	
	/**
	 * 前往基金申購結果頁
	 */
	@RequestMapping(value = "/fund_purchase_result")
	@Dialog(ADOPID = "C016")
	public String fund_purchase_result(HttpServletRequest request,@RequestParam Map<String,String> requestParam,Model model){
		String target = "/error";
		
		log.debug("fund_purchase_result");
		try{
			String cusidn = new String(Base64.getDecoder().decode((String)SessionUtil.getAttribute(model,SessionUtil.CUSIDN,null)),"UTF-8");
			log.debug("cusidn={}",cusidn);
			
			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				BaseResult bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				log.trace(">>>>>>{}",bs.getResult());
				if(!bs.getResult()) {
					model.addAttribute(BaseResult.ERROR,bs);
					return target;
				}
				Map<String,String> dataMap=(Map<String,String>)SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
				for(String key:dataMap.keySet()) {
					model.addAttribute(key,dataMap.get(key));
				}
				target = "fund/fund_purchase_result";
			}
			else {
				requestParam.put("CUSIDN",cusidn);
				String hiddenCUSIDN = WebUtil.hideID(cusidn);
				model.addAttribute("hiddenCUSIDN",hiddenCUSIDN);
				
				String KYCDATE = (String)SessionUtil.getAttribute(model,SessionUtil.KYCDATE,null);
				log.debug("KYCDATE={}",KYCDATE);
				requestParam.put("KYCDATE",KYCDATE);
				
				String WEAK = (String)SessionUtil.getAttribute(model,SessionUtil.WEAK,null);
				log.debug("WEAK={}",WEAK);
				requestParam.put("WEAK",WEAK);
				
				//寄件者信箱
				String DPMYEMAIL = (String)SessionUtil.getAttribute(model,SessionUtil.DPMYEMAIL,null);
				log.debug("DPMYEMAIL={}",DPMYEMAIL);
				requestParam.put("DPMYEMAIL",DPMYEMAIL);
				
				//收件者信箱
				requestParam.put("CMTRMAIL",DPMYEMAIL);
				
				String IP = WebUtil.getIpAddr(request);
				log.debug(ESAPIUtil.vaildLog("IP >> " + IP));
				requestParam.put("IP",IP);
				
				BaseResult bs = fund_purchase_service.processFundPurchase(requestParam,model);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				if(bs == null || !bs.getResult()){
					model.addAttribute(BaseResult.ERROR,bs);
					bs.setNext("/FUND/PURCHASE/fund_purchase_select");
					return target;
				}
				
				target = "fund/fund_purchase_result";
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, model);
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("fund_purchase_result error >> {}",e);
		}
		return target;
	}
	/**
	 * 依照COUNTRYTYPE取得基金的資料的AJAX
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/getFundCompanyDataAjax",produces={"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult getFundCompanyDataAjax(@RequestParam Map<String,String> requestParam){
		log.debug("IN getFundCompanyDataAjax");
		
		BaseResult baseResult = new BaseResult();
		baseResult.setResult(Boolean.FALSE);
		
		try{
			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam); 
			
			String COUNTRYTYPE = okMap.get("COUNTRYTYPE");
			log.debug(ESAPIUtil.vaildLog("COUNTRYTYPE= "+COUNTRYTYPE));
			
			String FEETYPE = okMap.get("FEETYPE");
			log.debug(ESAPIUtil.vaildLog("COUNTRYTYPE= "+COUNTRYTYPE));
			
			
			List<Map<String,String>> finalFundCompanyList = new ArrayList<Map<String,String>>();
			List<TXNFUNDCOMPANY> fundCompanyList = fund_purchase_service.getFundCompany(FEETYPE , COUNTRYTYPE);
			
			if(fundCompanyList != null){
				Map<String,String> map;
				
				for(int x=0;x<fundCompanyList.size();x++){
					map = new HashMap<String,String>();
					
					map = CodeUtil.objectCovert(map.getClass(),fundCompanyList.get(x));
					
					finalFundCompanyList.add(map);
				}
				
				baseResult.setResult(Boolean.TRUE);
				String dataString = new Gson().toJson(finalFundCompanyList,finalFundCompanyList.getClass());
				log.debug(ESAPIUtil.vaildLog("dataString="+dataString));
				
				baseResult.setData(dataString);
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getFundCompanyDataAjax error >> {}",e);
		}
		return baseResult;
	}
	/**
	 * 依照COMPANYCODE取得該公司的所有基金資料的AJAX
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/getFundDataByCompanyAjax",produces={"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult getFundDataByCompanyAjax(@RequestParam Map<String,String> requestParam){
		log.debug("IN getFundDataByCompanyAjax");
		log.debug("requestParam = {}", requestParam);
		
		BaseResult baseResult = new BaseResult();
		baseResult.setResult(Boolean.FALSE);
		
		try{
			
			// 解決Trust Boundary Violation 
			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
			
			String COMPANYCODE = okMap.get("COMPANYCODE");
			log.debug(ESAPIUtil.vaildLog("COMPANYCODE=>>" + COMPANYCODE));
			
			String FEETYPE = okMap.get("FEETYPE");
			log.debug(ESAPIUtil.vaildLog("FEETYPE=>>" + FEETYPE));
			
			String MIP = okMap.get("MIP");
			log.debug(ESAPIUtil.vaildLog("MIP=>>"+MIP));
			
			Boolean isRegularPurchase = (FEETYPE == null);
			
			List<Map<String,String>> finalFundDataList = new ArrayList<Map<String,String>>();
			List<TXNFUNDDATA> fundDataList = txnFundDataDao.findByCompany(FEETYPE,COMPANYCODE, isRegularPurchase);
			
			if(fundDataList != null){
				Map<String,String> map;
				
				for(int x=0;x<fundDataList.size();x++){
					//定期不定額基金名單查詢
					if("Y".equals(MIP)){
						String PERIODVAR = fundDataList.get(x).getPERIODVAR();
						log.debug(ESAPIUtil.vaildLog("PERIODVAR= >>"+PERIODVAR));
						
						if("Y".equals(PERIODVAR)){
							map = new HashMap<String,String>();
							map = CodeUtil.objectCovert(map.getClass(),fundDataList.get(x));
							finalFundDataList.add(map);
						}
					}
					//定期定額基金名單查詢
					else{
						map = new HashMap<String,String>();
						map = CodeUtil.objectCovert(map.getClass(),fundDataList.get(x));
						finalFundDataList.add(map);
					}
				}
				
				baseResult.setResult(Boolean.TRUE);
				String dataString = new Gson().toJson(finalFundDataList,finalFundDataList.getClass());
				log.debug(ESAPIUtil.vaildLog("dataString="+dataString));
				
				baseResult.setData(dataString);
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getFundDataByCompanyAjax error >> {}",e);
		}
		return baseResult;
	}
	
	@RequestMapping(value="/fundIkeyCheck",produces={"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult fundIkeyCheck(@RequestParam Map<String,String> requestParam,Model model){
		BaseResult bs = new BaseResult();
		try{
		String cusidn = new String(Base64.getDecoder().decode((String)SessionUtil.getAttribute(model,SessionUtil.CUSIDN,null)),"UTF-8");
		log.debug("cusidn={}",cusidn);
		String UID = cusidn;
		requestParam.put("UID",UID);
		bs = fund_purchase_service.fundIkeyCheck(requestParam);
		}catch(Exception e){
			log.error("fundIkeyCheck error >> {}",e);
		}
		return bs;
	}
}