package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N1010_CP_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4152363686746387737L;
	
    private String OFFSET;

    private String HEADER;

    private String BHCPResult;

	public String getOFFSET() {
		return OFFSET;
	}

	public String getHEADER() {
		return HEADER;
	}

	public void setOFFSET(String oFFSET) {
		OFFSET = oFFSET;
	}

	public void setHEADER(String hEADER) {
		HEADER = hEADER;
	}

	public String getBHCPResult() {
		return BHCPResult;
	}

	public void setBHCPResult(String bHCPResult) {
		BHCPResult = bHCPResult;
	}

	
    
	

}
