package tw.com.fstop.nnb.service;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.rest.bean.N920_REST_RSDATA;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.OriginalUtil;

@Service
public class Overview_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private I18n i18n;
	
	@Autowired
	private Acct_Bond_Service acct_bond_service;
	@Autowired
	private DaoService daoservice;
	
	/**
	 * 使用者設定帳戶總覽應查詢的功能
	 * 
	 * String cusidn
	 * @return
	 */
	public String getTxnuserDPOverview(String cusidn){
		String resultDP = "1,2,3,4,5,6,7";
		
		try {
			// 用cusidn查詢資料庫 TXNUSER.DPOVERVIEW (e.x: 0,1,2,3,4,5,6,7,8)
			String dpoverview = daoservice.get_dpoverview(cusidn);
			resultDP = (dpoverview == null || dpoverview.length() == 0) ? resultDP : dpoverview;
			log.debug("Overview_Service.dpoverview: " + dpoverview);
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getTxnuserDPOverview error >> {}",e);
		}
		
		return resultDP;
	}
	
	/**
	 * 帳戶總覽_信用卡總覽
	 * 
	 * @param sessionId
	 * @return
	 */
	public BaseResult call_N810(Map<String, String> reqParam) {
		log.trace("Overview_Service.call_N810...");
		// 處理結果
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// 電文N810
			bs = N810_REST(reqParam);
			// 資料處理後重新封裝bs
			if (bs != null) {
				// 把Client端需要的資料重新封裝
				if (bs.getResult()) {
					processN810(bs);
				}
				log.debug("N810_REST_RESULT: " + bs.getData());
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("call_N810 error >> {}",e);
		}
		return bs;
	}
	/**
	 * 帳戶總覽_信用卡總覽_資料處理
	 * 
	 * @param sessionId
	 * @return
	 */
	public BaseResult processN810(BaseResult bs) {
		log.trace("Overview_Service.processN810...");
		// 回傳的資料
		LinkedList<Object> n810List = new LinkedList<Object>();
		Map<String, Object> processN810Map = new HashMap<String, Object>();

		try {
			// 電文的資料
			Map<String, Object> mapN810 = (Map<String, Object>) bs.getData();
			log.trace("Overview_Service.processN810.mapN810: " + mapN810);
			
			// 資料處理
			String crlimit = mapN810.get("CRLIMIT") == null ? "" : NumericUtil.fmtAmount( (String) mapN810.get("CRLIMIT"), 2);
			String cshlimit = mapN810.get("CSHLIMIT")  == null ? "" : NumericUtil.fmtAmount( (String) mapN810.get("CSHLIMIT"), 2);
			String accbal = mapN810.get("ACCBAL")  == null ? "" : NumericUtil.fmtAmount( (String) mapN810.get("ACCBAL"), 2);
			String cshbal = mapN810.get("CSHBAL")  == null ? "" : NumericUtil.fmtAmount( (String) mapN810.get("CSHBAL"), 2);
			String int_rate = mapN810.get("CSHBAL")  == null ? "" : NumericUtil.fmtAmount( (String) mapN810.get("INT_RATE"), 3) + " %";
			processN810Map.put("CRLIMITFMT", crlimit); // 信用額度
			processN810Map.put("CSHLIMITFMT", cshlimit); // 預借現金額度
			processN810Map.put("ACCBALFMT", accbal); // 目前已使用額度
			processN810Map.put("CSHBALFMT", cshbal); // 已動用預借現金
			processN810Map.put("INT_RATEFMT", int_rate); // 本期循環信用利率
			// 扣繳方式
			if (null != mapN810.get("PAYMT_ACCT") && !"".equals(mapN810.get("PAYMT_ACCT"))) {
				String acctNum = (String) mapN810.get("PAYMT_ACCT");
				String coverNum = acctNum.substring(0, 6) + "***" + acctNum.substring(9);
				processN810Map.put("PAYMT_ACCT_COVER", coverNum);
			}
			// 自動扣繳帳號--01:全繳 ,02:最低
			if ("01".equals(mapN810.get("DBCODE"))) {
				processN810Map.put("DBCODE", i18n.getMsg("LB.Full_payment"));
			} else if ("02".equals(mapN810.get("DBCODE"))) {
				processN810Map.put("DBCODE", i18n.getMsg("LB.Mini_payment"));
			}
			
			log.trace("Overview_Service.processN810.processN810Map: " + processN810Map);
			
			// 處理好封裝成List回傳給前端FooTable
			n810List.add(processN810Map);
			// // 日期format
			mapN810.put("STOP_DAY", DateUtil.convertDate(2, (String) mapN810.get("STOP_DAY"), "yyyMMdd", "yyy/MM/dd"));
			mapN810.put("STMT_DAY", DateUtil.convertDate(2, (String) mapN810.get("STMT_DAY"), "yyyMMdd", "yyy/MM/dd"));
			mapN810.put("CURR_BALFMT", NumericUtil.fmtAmount((String) mapN810.get("CURR_BAL"), 2));
			mapN810.put("TOTL_DUEFMT", NumericUtil.fmtAmount((String) mapN810.get("TOTL_DUE"), 2));

			mapN810.put("REC", n810List);
			bs.setData(mapN810);
		
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("processN810 error >> {}",e);
		}
		// 回傳處理結果
		return bs;
	}
	
	
	/**
	 * 帳戶總覽_中央登錄債券餘額
	 * 
	 * @param sessionId
	 * @return
	 */
	public BaseResult call_N870(Map<String, String> reqParam) {
		log.trace("Overview_Service.call_N870...");
		// 處理結果
		BaseResult bs = null;
		try {
			log.trace(ESAPIUtil.vaildLog("CUSIDN: " +CodeUtil.toJson(reqParam.get("CUSIDN"))));
			bs = new BaseResult();
			bs = acct_bond_service.bond_balance_result(reqParam.get("CUSIDN"), reqParam);
			// 資料處理後重新封裝bs
			if (bs != null) {
				// 把Client端需要的資料重新封裝
//				if (bs.getResult()) {
//					processN870(bs);
//				}
				log.debug("N870_REST_RESULT: " + bs.getData());
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("call_N870 error >> {}",e);
		}
		return bs;
	}
	/**
	 * 帳戶總覽_中央登錄債券餘額_資料處理
	 * 
	 * @param sessionId
	 * @return
	 */
	public BaseResult processN870(BaseResult bs) {
		log.trace("processN870...");
		// 回傳的資料
		LinkedList<N920_REST_RSDATA> n920List = new LinkedList<N920_REST_RSDATA>();
		LinkedList<Map<String, String>> resultList = new LinkedList<Map<String, String>>();
		Map<String, String> resultMap = new HashMap<String, String>();

		try {
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("processN870 error >> {}",e);
		}
		// 回傳處理結果
		return bs;
	}
	
}
