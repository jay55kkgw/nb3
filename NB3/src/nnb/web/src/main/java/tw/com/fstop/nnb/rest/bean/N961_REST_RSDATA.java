package tw.com.fstop.nnb.rest.bean;

public class N961_REST_RSDATA {
	
    private String DIGACN;
    
    private String DIGTYPE;

	public String getDIGACN() {
		return DIGACN;
	}

	public void setDIGACN(String dIGACN) {
		DIGACN = dIGACN;
	}

	public String getDIGTYPE() {
		return DIGTYPE;
	}

	public void setDIGTYPE(String dIGTYPE) {
		DIGTYPE = dIGTYPE;
	}    
	
	
}
