package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N900_REST_RS extends BaseRestBean implements Serializable {

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8916012281225452321L;

	private String REC_NO;//筆數
	//C900
	private String CITYADDR2;
	private String ADDRESLEN;
	private String DATELE;
	private String ERRMSG;
	private String MOBILEPHONE;
	private String CUSTPOSIT2;
	private String CUSTID;
	private String CUSTPOSIT1;
	private String ENDNM;
	private String EMAIL;
	private String HOMEPHONE;
	private String Primarybitmap;
	private String TERMID;
	private String IDNUM;
	private String ADDR2;
	private String COBIRTHDAY;
	private String ADDR1;
	private String HOUPHONE;
	private String ACTCODE;
	private String DATEBRI;
	private String DATE4;
	private String SERVCODE;
	private String TRACODE;
	private String REASON;
	private String BIRTHDAY;
	private String PERID;
	private String REQTYPE;
	private String ACNDATALEN;
	private String OUTLEN;
	private String MOTMAINAME;
	private String FUNCODE;
	private String STAN;
	private String CITYADDR1;
	private String MSGTYPE;
	private String TIME;
	private String CITYADDR;
	private String OFFICEADDR1;
	private String PRIACN;
	private String OFFICEADDR2;
	private String NAME;
	private String DATE;
	private String NUMTRA;
	private String OFFICPHONE;
	private String OFFPHONE;
	private String INPREQLEN;
	private String CONM;
	private String NEWPIN;
	private String CUSTNM;
	private String CORPNM;
	private String SEQ;
	private String CONTACTNAME1;
	private String CONTACTREL1;
	private String CONTACTPHONE1;
	private String CONTACTNAME2;
	private String CONTACTREL2;
	private String CONTACTPHONE2;
	private String CARDHOLDER;
	private String CARDMAILTYPE;
	private String CYCLEDATE;
	private String OFFICERCODE;
	private String BLOCKCODE;
	private String CARDEXPIREDATE;
	private String MAKECARDDATE;
	private String CDLIMIT;
	private String CENSUSTRACT;
	private String ORIGINLIMIT;
	private String ORIGINLIMITDT;
	private String CURBAL;
	private String CURBALCHDT;
	private String PAYTYPE;
	private String CUSTTYPE;
	private String AUTOPAYACC;
	private String AUTOPAYLIMIT;
	private String BRANCH;
	private String BASERATE;
	private String BASEADJ;
	private String COLLACC;
	private String COLLFLAG;
	private String _0106PROFILE;
	private String _0712PROFILE;
	private String POTCODE;
	private String POTCODEDT;
	private String LASTMAINTDT;
	private String MEMO1;
	private String MEMO2;
	private String MEMO3;
	private String MEMO4;
	private String MEMO5;
	private String OUTDATA2;
	private String CURRDUE;
	private String PASTDUE;
	private String _30DAYSDELQ;
	private String _60DAYSDELQ;
	private String _90DAYSDELQ;
	private String _120DAYSDELQ;
	private String _150DAYSDELQ;
	private String _180DAYSDELQ;
	private String _210DAYSDELQ;
	private String WAIVEANNULFEE;
	private String COHOMEPHONE;
	private String DTEOPENED;
	private String EUCUSTOMERCLASS;
	private String EUBANKACCTIND;
	private String CSHADVLIM;
	
	private String OFFSET;//OFFSET
	private String HEADER;//HEADER
	private String MSGCOD;//交易訊息
	private String SMSA;//帳單遞送地址
	private String POSTCOD;//郵遞區號
	private String ZIPCODE;//郵遞區號
	private String ADDRESS;//地址
	private String HOME_TEL;//住家電話
	private String OFFICE_TEL;//公司電話
	private String MOBILE_TEL;//手機號碼
	
	
	
	public String getREC_NO() {
		return REC_NO;
	}
	public void setREC_NO(String rEC_NO) {
		REC_NO = rEC_NO;
	}
	public String getMOBILEPHONE() {
		return MOBILEPHONE;
	}
	public void setMOBILEPHONE(String mOBILEPHONE) {
		MOBILEPHONE = mOBILEPHONE;
	}
	public String getCUSTPOSIT2() {
		return CUSTPOSIT2;
	}
	public void setCUSTPOSIT2(String cUSTPOSIT2) {
		CUSTPOSIT2 = cUSTPOSIT2;
	}
	public String getCUSTID() {
		return CUSTID;
	}
	public void setCUSTID(String cUSTID) {
		CUSTID = cUSTID;
	}
	public String getCUSTPOSIT1() {
		return CUSTPOSIT1;
	}
	public void setCUSTPOSIT1(String cUSTPOSIT1) {
		CUSTPOSIT1 = cUSTPOSIT1;
	}
	public String getENDNM() {
		return ENDNM;
	}
	public void setENDNM(String eNDNM) {
		ENDNM = eNDNM;
	}
	public String getEMAIL() {
		return EMAIL;
	}
	public void setEMAIL(String eMAIL) {
		EMAIL = eMAIL;
	}
	public String getHOMEPHONE() {
		return HOMEPHONE;
	}
	public void setHOMEPHONE(String hOMEPHONE) {
		HOMEPHONE = hOMEPHONE;
	}
	public String getPrimarybitmap() {
		return Primarybitmap;
	}
	public void setPrimarybitmap(String primarybitmap) {
		Primarybitmap = primarybitmap;
	}
	public String getTERMID() {
		return TERMID;
	}
	public void setTERMID(String tERMID) {
		TERMID = tERMID;
	}
	public String getIDNUM() {
		return IDNUM;
	}
	public void setIDNUM(String iDNUM) {
		IDNUM = iDNUM;
	}
	public String getADDR2() {
		return ADDR2;
	}
	public void setADDR2(String aDDR2) {
		ADDR2 = aDDR2;
	}
	public String getCOBIRTHDAY() {
		return COBIRTHDAY;
	}
	public void setCOBIRTHDAY(String cOBIRTHDAY) {
		COBIRTHDAY = cOBIRTHDAY;
	}
	public String getADDR1() {
		return ADDR1;
	}
	public void setADDR1(String aDDR1) {
		ADDR1 = aDDR1;
	}
	public String getHOUPHONE() {
		return HOUPHONE;
	}
	public void setHOUPHONE(String hOUPHONE) {
		HOUPHONE = hOUPHONE;
	}
	public String getACTCODE() {
		return ACTCODE;
	}
	public void setACTCODE(String aCTCODE) {
		ACTCODE = aCTCODE;
	}
	public String getDATEBRI() {
		return DATEBRI;
	}
	public void setDATEBRI(String dATEBRI) {
		DATEBRI = dATEBRI;
	}
	public String getDATE4() {
		return DATE4;
	}
	public void setDATE4(String dATE4) {
		DATE4 = dATE4;
	}
	public String getSERVCODE() {
		return SERVCODE;
	}
	public void setSERVCODE(String sERVCODE) {
		SERVCODE = sERVCODE;
	}
	public String getTRACODE() {
		return TRACODE;
	}
	public void setTRACODE(String tRACODE) {
		TRACODE = tRACODE;
	}
	public String getREASON() {
		return REASON;
	}
	public void setREASON(String rEASON) {
		REASON = rEASON;
	}
	public String getBIRTHDAY() {
		return BIRTHDAY;
	}
	public void setBIRTHDAY(String bIRTHDAY) {
		BIRTHDAY = bIRTHDAY;
	}
	public String getPERID() {
		return PERID;
	}
	public void setPERID(String pERID) {
		PERID = pERID;
	}
	public String getREQTYPE() {
		return REQTYPE;
	}
	public void setREQTYPE(String rEQTYPE) {
		REQTYPE = rEQTYPE;
	}
	public String getACNDATALEN() {
		return ACNDATALEN;
	}
	public void setACNDATALEN(String aCNDATALEN) {
		ACNDATALEN = aCNDATALEN;
	}
	public String getOUTLEN() {
		return OUTLEN;
	}
	public void setOUTLEN(String oUTLEN) {
		OUTLEN = oUTLEN;
	}
	public String getMOTMAINAME() {
		return MOTMAINAME;
	}
	public void setMOTMAINAME(String mOTMAINAME) {
		MOTMAINAME = mOTMAINAME;
	}
	public String getFUNCODE() {
		return FUNCODE;
	}
	public void setFUNCODE(String fUNCODE) {
		FUNCODE = fUNCODE;
	}
	public String getSTAN() {
		return STAN;
	}
	public void setSTAN(String sTAN) {
		STAN = sTAN;
	}
	public String getCITYADDR1() {
		return CITYADDR1;
	}
	public void setCITYADDR1(String cITYADDR1) {
		CITYADDR1 = cITYADDR1;
	}
	public String getMSGTYPE() {
		return MSGTYPE;
	}
	public void setMSGTYPE(String mSGTYPE) {
		MSGTYPE = mSGTYPE;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getCITYADDR() {
		return CITYADDR;
	}
	public void setCITYADDR(String cITYADDR) {
		CITYADDR = cITYADDR;
	}
	public String getOFFICEADDR1() {
		return OFFICEADDR1;
	}
	public void setOFFICEADDR1(String oFFICEADDR1) {
		OFFICEADDR1 = oFFICEADDR1;
	}
	public String getPRIACN() {
		return PRIACN;
	}
	public void setPRIACN(String pRIACN) {
		PRIACN = pRIACN;
	}
	public String getOFFICEADDR2() {
		return OFFICEADDR2;
	}
	public void setOFFICEADDR2(String oFFICEADDR2) {
		OFFICEADDR2 = oFFICEADDR2;
	}
	public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getNUMTRA() {
		return NUMTRA;
	}
	public void setNUMTRA(String nUMTRA) {
		NUMTRA = nUMTRA;
	}
	public String getOFFICPHONE() {
		return OFFICPHONE;
	}
	public void setOFFICPHONE(String oFFICPHONE) {
		OFFICPHONE = oFFICPHONE;
	}
	public String getOFFPHONE() {
		return OFFPHONE;
	}
	public void setOFFPHONE(String oFFPHONE) {
		OFFPHONE = oFFPHONE;
	}
	public String getINPREQLEN() {
		return INPREQLEN;
	}
	public void setINPREQLEN(String iNPREQLEN) {
		INPREQLEN = iNPREQLEN;
	}
	public String getCONM() {
		return CONM;
	}
	public void setCONM(String cONM) {
		CONM = cONM;
	}
	public String getNEWPIN() {
		return NEWPIN;
	}
	public void setNEWPIN(String nEWPIN) {
		NEWPIN = nEWPIN;
	}
	public String getCUSTNM() {
		return CUSTNM;
	}
	public void setCUSTNM(String cUSTNM) {
		CUSTNM = cUSTNM;
	}
	public String getCORPNM() {
		return CORPNM;
	}
	public void setCORPNM(String cORPNM) {
		CORPNM = cORPNM;
	}
	public String getSEQ() {
		return SEQ;
	}
	public void setSEQ(String sEQ) {
		SEQ = sEQ;
	}
	public String getOFFSET() {
		return OFFSET;
	}
	public void setOFFSET(String oFFSET) {
		OFFSET = oFFSET;
	}
	public String getHEADER() {
		return HEADER;
	}
	public void setHEADER(String hEADER) {
		HEADER = hEADER;
	}
	public String getMSGCOD() {
		return MSGCOD;
	}
	public void setMSGCOD(String mSGCOD) {
		MSGCOD = mSGCOD;
	}
	public String getSMSA() {
		return SMSA;
	}
	public void setSMSA(String sMSA) {
		SMSA = sMSA;
	}
	public String getPOSTCOD() {
		return POSTCOD;
	}
	public void setPOSTCOD(String pOSTCOD) {
		POSTCOD = pOSTCOD;
	}
	public String getADDRESS() {
		return ADDRESS;
	}
	public void setADDRESS(String aDDRESS) {
		ADDRESS = aDDRESS;
	}
	public String getHOME_TEL() {
		return HOME_TEL;
	}
	public void setHOME_TEL(String hOME_TEL) {
		HOME_TEL = hOME_TEL;
	}
	public String getOFFICE_TEL() {
		return OFFICE_TEL;
	}
	public void setOFFICE_TEL(String oFFICE_TEL) {
		OFFICE_TEL = oFFICE_TEL;
	}
	public String getMOBILE_TEL() {
		return MOBILE_TEL;
	}
	public void setMOBILE_TEL(String mOBILE_TEL) {
		MOBILE_TEL = mOBILE_TEL;
	}
	public String getCITYADDR2() {
		return CITYADDR2;
	}
	public void setCITYADDR2(String cITYADDR2) {
		CITYADDR2 = cITYADDR2;
	}
	public String getADDRESLEN() {
		return ADDRESLEN;
	}
	public void setADDRESLEN(String aDDRESLEN) {
		ADDRESLEN = aDDRESLEN;
	}
	public String getDATELE() {
		return DATELE;
	}
	public void setDATELE(String dATELE) {
		DATELE = dATELE;
	}
	public String getERRMSG() {
		return ERRMSG;
	}
	public void setERRMSG(String eRRMSG) {
		ERRMSG = eRRMSG;
	}
	public String getCONTACTNAME1() {
		return CONTACTNAME1;
	}
	public void setCONTACTNAME1(String cONTACTNAME1) {
		CONTACTNAME1 = cONTACTNAME1;
	}
	public String getCONTACTREL1() {
		return CONTACTREL1;
	}
	public void setCONTACTREL1(String cONTACTREL1) {
		CONTACTREL1 = cONTACTREL1;
	}
	public String getCONTACTPHONE1() {
		return CONTACTPHONE1;
	}
	public void setCONTACTPHONE1(String cONTACTPHONE1) {
		CONTACTPHONE1 = cONTACTPHONE1;
	}
	public String getCONTACTNAME2() {
		return CONTACTNAME2;
	}
	public void setCONTACTNAME2(String cONTACTNAME2) {
		CONTACTNAME2 = cONTACTNAME2;
	}
	public String getCONTACTREL2() {
		return CONTACTREL2;
	}
	public void setCONTACTREL2(String cONTACTREL2) {
		CONTACTREL2 = cONTACTREL2;
	}
	public String getCONTACTPHONE2() {
		return CONTACTPHONE2;
	}
	public void setCONTACTPHONE2(String cONTACTPHONE2) {
		CONTACTPHONE2 = cONTACTPHONE2;
	}
	public String getCARDHOLDER() {
		return CARDHOLDER;
	}
	public void setCARDHOLDER(String cARDHOLDER) {
		CARDHOLDER = cARDHOLDER;
	}
	public String getCARDMAILTYPE() {
		return CARDMAILTYPE;
	}
	public void setCARDMAILTYPE(String cARDMAILTYPE) {
		CARDMAILTYPE = cARDMAILTYPE;
	}
	public String getCYCLEDATE() {
		return CYCLEDATE;
	}
	public void setCYCLEDATE(String cYCLEDATE) {
		CYCLEDATE = cYCLEDATE;
	}
	public String getOFFICERCODE() {
		return OFFICERCODE;
	}
	public void setOFFICERCODE(String oFFICERCODE) {
		OFFICERCODE = oFFICERCODE;
	}
	public String getBLOCKCODE() {
		return BLOCKCODE;
	}
	public void setBLOCKCODE(String bLOCKCODE) {
		BLOCKCODE = bLOCKCODE;
	}
	public String getCARDEXPIREDATE() {
		return CARDEXPIREDATE;
	}
	public void setCARDEXPIREDATE(String cARDEXPIREDATE) {
		CARDEXPIREDATE = cARDEXPIREDATE;
	}
	public String getMAKECARDDATE() {
		return MAKECARDDATE;
	}
	public void setMAKECARDDATE(String mAKECARDDATE) {
		MAKECARDDATE = mAKECARDDATE;
	}
	public String getCDLIMIT() {
		return CDLIMIT;
	}
	public void setCDLIMIT(String CDLIMIT) {
		CDLIMIT = CDLIMIT;
	}
	public String getCENSUSTRACT() {
		return CENSUSTRACT;
	}
	public void setCENSUSTRACT(String cENSUSTRACT) {
		CENSUSTRACT = cENSUSTRACT;
	}
	public String getORIGINLIMIT() {
		return ORIGINLIMIT;
	}
	public void setORIGINLIMIT(String oRIGINLIMIT) {
		ORIGINLIMIT = oRIGINLIMIT;
	}
	public String getORIGINLIMITDT() {
		return ORIGINLIMITDT;
	}
	public void setORIGINLIMITDT(String oRIGINLIMITDT) {
		ORIGINLIMITDT = oRIGINLIMITDT;
	}
	public String getCURBAL() {
		return CURBAL;
	}
	public void setCURBAL(String cURBAL) {
		CURBAL = cURBAL;
	}
	public String getCURBALCHDT() {
		return CURBALCHDT;
	}
	public void setCURBALCHDT(String cURBALCHDT) {
		CURBALCHDT = cURBALCHDT;
	}
	public String getPAYTYPE() {
		return PAYTYPE;
	}
	public void setPAYTYPE(String pAYTYPE) {
		PAYTYPE = pAYTYPE;
	}
	public String getCUSTTYPE() {
		return CUSTTYPE;
	}
	public void setCUSTTYPE(String cUSTTYPE) {
		CUSTTYPE = cUSTTYPE;
	}
	public String getAUTOPAYACC() {
		return AUTOPAYACC;
	}
	public void setAUTOPAYACC(String aUTOPAYACC) {
		AUTOPAYACC = aUTOPAYACC;
	}
	public String getAUTOPAYLIMIT() {
		return AUTOPAYLIMIT;
	}
	public void setAUTOPAYLIMIT(String aUTOPAYLIMIT) {
		AUTOPAYLIMIT = aUTOPAYLIMIT;
	}
	public String getBRANCH() {
		return BRANCH;
	}
	public void setBRANCH(String bRANCH) {
		BRANCH = bRANCH;
	}
	public String getBASERATE() {
		return BASERATE;
	}
	public void setBASERATE(String bASERATE) {
		BASERATE = bASERATE;
	}
	public String getBASEADJ() {
		return BASEADJ;
	}
	public void setBASEADJ(String bASEADJ) {
		BASEADJ = bASEADJ;
	}
	public String getCOLLACC() {
		return COLLACC;
	}
	public void setCOLLACC(String cOLLACC) {
		COLLACC = cOLLACC;
	}
	public String getCOLLFLAG() {
		return COLLFLAG;
	}
	public void setCOLLFLAG(String cOLLFLAG) {
		COLLFLAG = cOLLFLAG;
	}
	public String get_0106PROFILE() {
		return _0106PROFILE;
	}
	public void set_0106PROFILE(String _0106profile) {
		_0106PROFILE = _0106profile;
	}
	public String get_0712PROFILE() {
		return _0712PROFILE;
	}
	public void set_0712PROFILE(String _0712profile) {
		_0712PROFILE = _0712profile;
	}
	public String getPOTCODE() {
		return POTCODE;
	}
	public void setPOTCODE(String pOTCODE) {
		POTCODE = pOTCODE;
	}
	public String getPOTCODEDT() {
		return POTCODEDT;
	}
	public void setPOTCODEDT(String pOTCODEDT) {
		POTCODEDT = pOTCODEDT;
	}
	public String getLASTMAINTDT() {
		return LASTMAINTDT;
	}
	public void setLASTMAINTDT(String lASTMAINTDT) {
		LASTMAINTDT = lASTMAINTDT;
	}
	public String getMEMO1() {
		return MEMO1;
	}
	public void setMEMO1(String mEMO1) {
		MEMO1 = mEMO1;
	}
	public String getMEMO2() {
		return MEMO2;
	}
	public void setMEMO2(String mEMO2) {
		MEMO2 = mEMO2;
	}
	public String getMEMO3() {
		return MEMO3;
	}
	public void setMEMO3(String mEMO3) {
		MEMO3 = mEMO3;
	}
	public String getMEMO4() {
		return MEMO4;
	}
	public void setMEMO4(String mEMO4) {
		MEMO4 = mEMO4;
	}
	public String getMEMO5() {
		return MEMO5;
	}
	public void setMEMO5(String mEMO5) {
		MEMO5 = mEMO5;
	}
	public String getOUTDATA2() {
		return OUTDATA2;
	}
	public void setOUTDATA2(String oUTDATA2) {
		OUTDATA2 = oUTDATA2;
	}
	public String getCURRDUE() {
		return CURRDUE;
	}
	public void setCURRDUE(String cURRDUE) {
		CURRDUE = cURRDUE;
	}
	public String getPASTDUE() {
		return PASTDUE;
	}
	public void setPASTDUE(String pASTDUE) {
		PASTDUE = pASTDUE;
	}
	public String get_30DAYSDELQ() {
		return _30DAYSDELQ;
	}
	public void set_30DAYSDELQ(String _30daysdelq) {
		_30DAYSDELQ = _30daysdelq;
	}
	public String get_60DAYSDELQ() {
		return _60DAYSDELQ;
	}
	public void set_60DAYSDELQ(String _60daysdelq) {
		_60DAYSDELQ = _60daysdelq;
	}
	public String get_90DAYSDELQ() {
		return _90DAYSDELQ;
	}
	public void set_90DAYSDELQ(String _90daysdelq) {
		_90DAYSDELQ = _90daysdelq;
	}
	public String get_120DAYSDELQ() {
		return _120DAYSDELQ;
	}
	public void set_120DAYSDELQ(String _120daysdelq) {
		_120DAYSDELQ = _120daysdelq;
	}
	public String get_150DAYSDELQ() {
		return _150DAYSDELQ;
	}
	public void set_150DAYSDELQ(String _150daysdelq) {
		_150DAYSDELQ = _150daysdelq;
	}
	public String get_180DAYSDELQ() {
		return _180DAYSDELQ;
	}
	public void set_180DAYSDELQ(String _180daysdelq) {
		_180DAYSDELQ = _180daysdelq;
	}
	public String get_210DAYSDELQ() {
		return _210DAYSDELQ;
	}
	public void set_210DAYSDELQ(String _210daysdelq) {
		_210DAYSDELQ = _210daysdelq;
	}
	public String getWAIVEANNULFEE() {
		return WAIVEANNULFEE;
	}
	public void setWAIVEANNULFEE(String wAIVEANNULFEE) {
		WAIVEANNULFEE = wAIVEANNULFEE;
	}
	public String getCOHOMEPHONE() {
		return COHOMEPHONE;
	}
	public void setCOHOMEPHONE(String cOHOMEPHONE) {
		COHOMEPHONE = cOHOMEPHONE;
	}
	public String getDTEOPENED() {
		return DTEOPENED;
	}
	public void setDTEOPENED(String dTEOPENED) {
		DTEOPENED = dTEOPENED;
	}
	public String getEUCUSTOMERCLASS() {
		return EUCUSTOMERCLASS;
	}
	public void setEUCUSTOMERCLASS(String eUCUSTOMERCLASS) {
		EUCUSTOMERCLASS = eUCUSTOMERCLASS;
	}
	public String getEUBANKACCTIND() {
		return EUBANKACCTIND;
	}
	public void setEUBANKACCTIND(String eUBANKACCTIND) {
		EUBANKACCTIND = eUBANKACCTIND;
	}
	public String getCSHADVLIM() {
		return CSHADVLIM;
	}
	public void setCSHADVLIM(String cSHADVLIM) {
		CSHADVLIM = cSHADVLIM;
	}
	public String getZIPCODE() {
		return ZIPCODE;
	}
	public void setZIPCODE(String zIPCODE) {
		ZIPCODE = zIPCODE;
	}
}
