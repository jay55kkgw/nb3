package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N930EA_REST_RQ extends BaseRestBean_PS implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1276075313461768152L;
	private String EXECUTEFUNCTION;
	private String DPUSERID;
	private String DPADDBKID;
	private String DPABMAIL;
	private String DPGONAME;
	
	public String getEXECUTEFUNCTION() {
		return EXECUTEFUNCTION;
	}
	public void setEXECUTEFUNCTION(String eXECUTEFUNCTION) {
		EXECUTEFUNCTION = eXECUTEFUNCTION;
	}
	public String getDPUSERID() {
		return DPUSERID;
	}
	public void setDPUSERID(String dPUSERID) {
		DPUSERID = dPUSERID;
	}
	public String getDPADDBKID() {
		return DPADDBKID;
	}
	public void setDPADDBKID(String dPADDBKID) {
		DPADDBKID = dPADDBKID;
	}
	public String getDPABMAIL() {
		return DPABMAIL;
	}
	public void setDPABMAIL(String dPABMAIL) {
		DPABMAIL = dPABMAIL;
	}
	public String getDPGONAME() {
		return DPGONAME;
	}
	public void setDPGONAME(String dPGONAME) {
		DPGONAME = dPGONAME;
	}
}
