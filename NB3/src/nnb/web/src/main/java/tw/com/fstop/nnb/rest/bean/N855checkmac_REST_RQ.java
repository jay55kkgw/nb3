package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N855checkmac_REST_RQ  extends BaseRestBean_TW implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2768592258122963365L;
	private String	DivData ;
	private String	ICV ;
	private String	MAC ;
	private String	IBPD_Param ;
	public String getDivData() {
		return DivData;
	}
	public void setDivData(String divData) {
		this.DivData = divData;
	}
	public String getICV() {
		return ICV;
	}
	public void setICV(String iCV) {
		ICV = iCV;
	}
	public String getMAC() {
		return MAC;
	}
	public void setMAC(String mAC) {
		MAC = mAC;
	}
	public String getIBPD_Param() {
		return IBPD_Param;
	}
	public void setIBPD_Param(String iBPD_Param) {
		IBPD_Param = iBPD_Param;
	}

}
