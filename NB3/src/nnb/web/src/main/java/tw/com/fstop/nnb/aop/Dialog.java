package tw.com.fstop.nnb.aop;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ ElementType.TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Dialog {
	/**
	 * 功能對應代碼
	 * =========================
	 * 台幣帳戶明細查詢：N130
	 * 外幣帳戶明細查詢：N520
	 * 台幣轉帳：N070
	 * 台幣轉入綜存定存：N074
	 * 提前償還本金：N3003
	 * 買賣外幣：F001
	 * 匯出匯款：F002
	 * 轉入外幣綜存定存：N174
	 * 基金餘額損益查詢：C012
	 * 海外債券餘額損益查詢：B012
	 * 基金單筆申購：C016
	 * 基金定期定額申購：C017
	 * 基金轉換：C021
	 * 基金贖回：C024
	 */
	String ADOPID() default "";
}
