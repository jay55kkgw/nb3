
/*
 * Copyright (c) 2017, FSTOP, Inc. All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tw.com.fstop.web.servlet;


import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tw.com.fstop.web.BaseServletProcessor;
import tw.com.fstop.web.ProcessorException;
import tw.com.fstop.web.ServletProcessor;
import tw.com.fstop.web.ctx.WebAppRequestContext;

/**
 * Base Servlet class.
 * Subclass should implements 
 *   void process(HttpServletRequest request, HttpServletResponse response);
 * 
 * <pre>  
 * Sevlet 重導向 URL 時，應呼叫 response.encodeRedirectURL("xxx");
 * Sevlet 處理 URL，應呼叫 response.encodeURL("xxx");
 * </pre>
 *
 * @since 1.0.1
 */
public abstract class BaseServlet extends HttpServlet
{
    //Log4j
    //private final static Logger log = Logger.getLogger(BaseServlet.class.getName());
    //Slf4j
    private final static Logger log = LoggerFactory.getLogger(BaseServlet.class);
    
    
    private static final long serialVersionUID = 1507444885828756246L;
    
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        doPost(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {       
        WebAppRequestContext.setValue(WebAppRequestContext.Key.CONTEXT_PATH, request.getContextPath());
        
        
        ServletProcessor processor = new BaseServletProcessor();
        try
        {
            processor.process(request, response);
            
            WebAppRequestContext.setValue(WebAppRequestContext.Key.USER_INPUT, processor.getInputParam());
                        
            WebAppRequestContext.setValue(WebAppRequestContext.Key.DECODED_USER_INPUT, processor.getDecodedInputParam());

            process(request, response);
        }
        catch (ProcessorException e)
        {
            log.error("BaseServlet error=", e);
        }
        finally
        {
            WebAppRequestContext.remove();
        }
        
    }
    
    /**
     * 基於 Web 安全考量，建議在登入成功後改變 Session ID，想改變 Session ID，
     * 可以透過 Servlet 3.1 於 HttpServletRequest 上新增的 changeSessionId() 來達到。
     * 至於 Servlet 3.0 之前的版本，必須自行取出 HttpSession 中的屬性，令目前的 HttpSession 失效，
     * 然後取得 HttpSession 並設定屬性
     * 
     * @param request http servlet request
     */
    protected HttpSession changeSessionId(HttpServletRequest request)
    {
        HttpSession oldSession = request.getSession(false);
        if (oldSession == null)
        {
            throw new IllegalStateException("No current HttpSession associated with this request");
        }

        Map<String, Object> attrs = new HashMap<String, Object>();
        for (String name : Collections.list(oldSession.getAttributeNames()))
        {
            attrs.put(name, oldSession.getAttribute(name));
            oldSession.removeAttribute(name);
        }
        oldSession.invalidate();

        HttpSession newSession = request.getSession(true);
        for (String name : attrs.keySet())
        {
            newSession.setAttribute(name, attrs.get(name));
        }
        return newSession;
    }      
    
    protected HttpSession getSession(HttpServletRequest request, boolean forceCreate)
    {
        return request.getSession(forceCreate);
    }
    
    protected HttpSession getSession()
    {
        return (HttpSession) WebAppRequestContext.getValue(WebAppRequestContext.Key.HTTP_SESSION);
    }
    
    protected void setSession(HttpSession session)
    {
        WebAppRequestContext.setValue(WebAppRequestContext.Key.HTTP_SESSION, session);
    }
        
    protected void setSessionData(String key, Object value)
    {
        //httpSession.setAttribute(key, value);
        getSession().setAttribute(key, value);
    }
    
    protected Object getSessionData(String key)
    {
        //return httpSession.getAttribute(key);
        return getSession().getAttribute(key);
    }
    
    protected String getContextPath()
    {
        return (String) WebAppRequestContext.getValue(WebAppRequestContext.Key.CONTEXT_PATH);
    }

    @SuppressWarnings("unchecked")
    protected Map<String, String> getUserInput()
    {
        return (Map<String, String>) WebAppRequestContext.getValue(WebAppRequestContext.Key.USER_INPUT);
    }

    protected void setUserInput(Map<String, String> userInput)
    {
        WebAppRequestContext.setValue(WebAppRequestContext.Key.USER_INPUT, userInput);
    }

    @SuppressWarnings("unchecked")
    protected Map<String, String> getDecodedUserInput()
    {
        return (Map<String, String>) WebAppRequestContext.getValue(WebAppRequestContext.Key.DECODED_USER_INPUT);
    }

    protected void setDecodedUserInput(Map<String, String> decodedUserInput)
    {
        WebAppRequestContext.setValue(WebAppRequestContext.Key.DECODED_USER_INPUT, decodedUserInput);
    }
    
    public abstract void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;
    
    
}
