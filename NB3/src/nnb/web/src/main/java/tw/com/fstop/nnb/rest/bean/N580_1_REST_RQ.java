package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;


public class N580_1_REST_RQ extends BaseRestBean_OLA implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5717391694595030203L;

	private String ADOPID;
	private String FILL_X1;//FILL_X1
	private String TRANNAME;//TRANNAME
	private String HEADER;//HEADER
	private String DATE;//日期YYYMMDD
	private String TIME;//時間HHMMSS
	private String CUSIDN;//統一編號
	private String TYPE;//類別
	private String VERNUM;//約定書版號
	private String TRANSEQ;//交易序號
	private String ISSUER;//晶片卡發卡行庫
	private String ACNNO;//晶片卡帳號
	private String ICDTTM;//晶片卡日期時間
	private String ICSEQ;//晶片卡序號
	private String ICMEMO;//晶片卡備註欄
	private String TAC_Length;//TAC DATA Length
	private String TAC;//TAC DATA
	private String TAC_120space;//TAC DATA Space
	private String TRMID;//端末設備查核碼
	private String ACN;
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getFILL_X1() {
		return FILL_X1;
	}
	public void setFILL_X1(String fILL_X1) {
		FILL_X1 = fILL_X1;
	}
	public String getTRANNAME() {
		return TRANNAME;
	}
	public void setTRANNAME(String tRANNAME) {
		TRANNAME = tRANNAME;
	}
	public String getHEADER() {
		return HEADER;
	}
	public void setHEADER(String hEADER) {
		HEADER = hEADER;
	}
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getTYPE() {
		return TYPE;
	}
	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}
	public String getVERNUM() {
		return VERNUM;
	}
	public void setVERNUM(String vERNUM) {
		VERNUM = vERNUM;
	}
	public String getTRANSEQ() {
		return TRANSEQ;
	}
	public void setTRANSEQ(String tRANSEQ) {
		TRANSEQ = tRANSEQ;
	}
	public String getISSUER() {
		return ISSUER;
	}
	public void setISSUER(String iSSUER) {
		ISSUER = iSSUER;
	}
	public String getACNNO() {
		return ACNNO;
	}
	public void setACNNO(String aCNNO) {
		ACNNO = aCNNO;
	}
	public String getICDTTM() {
		return ICDTTM;
	}
	public void setICDTTM(String iCDTTM) {
		ICDTTM = iCDTTM;
	}
	public String getICSEQ() {
		return ICSEQ;
	}
	public void setICSEQ(String iCSEQ) {
		ICSEQ = iCSEQ;
	}
	public String getICMEMO() {
		return ICMEMO;
	}
	public void setICMEMO(String iCMEMO) {
		ICMEMO = iCMEMO;
	}
	public String getTAC_Length() {
		return TAC_Length;
	}
	public void setTAC_Length(String tAC_Length) {
		TAC_Length = tAC_Length;
	}
	public String getTAC() {
		return TAC;
	}
	public void setTAC(String tAC) {
		TAC = tAC;
	}
	public String getTAC_120space() {
		return TAC_120space;
	}
	public void setTAC_120space(String tAC_120space) {
		TAC_120space = tAC_120space;
	}
	public String getTRMID() {
		return TRMID;
	}
	public void setTRMID(String tRMID) {
		TRMID = tRMID;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
