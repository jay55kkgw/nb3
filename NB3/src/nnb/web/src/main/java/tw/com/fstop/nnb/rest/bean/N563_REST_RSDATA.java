package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N563_REST_RSDATA extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2496154579991130006L;

	private String RACCEPDT; //國外承兌日
	private String RBILLAMT; //到單金額
	private String RMATDAT1; //國外預定到期日
	private String RREFNO; //託收編號
	private String RTENOR1; //天期
	private String RPAYDATE; //本行入帳日
	private String RMARK; //備註
	private String RDRAWEE; //進口商
	private String RBILLCCY; //幣別
	private String RVALDATE; //結案日期
	
	public String getRACCEPDT() {
		return RACCEPDT;
	}
	public void setRACCEPDT(String rACCEPDT) {
		RACCEPDT = rACCEPDT;
	}
	public String getRBILLAMT() {
		return RBILLAMT;
	}
	public void setRBILLAMT(String rBILLAMT) {
		RBILLAMT = rBILLAMT;
	}
	public String getRMATDAT1() {
		return RMATDAT1;
	}
	public void setRMATDAT1(String rMATDAT1) {
		RMATDAT1 = rMATDAT1;
	}
	public String getRREFNO() {
		return RREFNO;
	}
	public void setRREFNO(String rREFNO) {
		RREFNO = rREFNO;
	}
	public String getRTENOR1() {
		return RTENOR1;
	}
	public void setRTENOR1(String rTENOR1) {
		RTENOR1 = rTENOR1;
	}
	public String getRPAYDATE() {
		return RPAYDATE;
	}
	public void setRPAYDATE(String rPAYDATE) {
		RPAYDATE = rPAYDATE;
	}
	public String getRMARK() {
		return RMARK;
	}
	public void setRMARK(String rMARK) {
		RMARK = rMARK;
	}
	public String getRDRAWEE() {
		return RDRAWEE;
	}
	public void setRDRAWEE(String rDRAWEE) {
		RDRAWEE = rDRAWEE;
	}
	public String getRBILLCCY() {
		return RBILLCCY;
	}
	public void setRBILLCCY(String rBILLCCY) {
		RBILLCCY = rBILLCCY;
	}
	public String getRVALDATE() {
		return RVALDATE;
	}
	public void setRVALDATE(String rVALDATE) {
		RVALDATE = rVALDATE;
	}
	
}
