package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class N900_IDGATE_DATA implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -762951082705400880L;

	@SerializedName(value = "POSTCOD")
	private String POSTCOD;
	@SerializedName(value = "ADDRESS")
	private String ADDRESS;
	@SerializedName(value = "HOME_TEL")
	private String HOME_TEL;	
	@SerializedName(value = "OFFICE_TEL")
	private String OFFICE_TEL;	
	@SerializedName(value = "MOBILE_TEL")
	private String MOBILE_TEL;
	public String getPOSTCOD() {
		return POSTCOD;
	}
	public String getADDRESS() {
		return ADDRESS;
	}
	public String getHOME_TEL() {
		return HOME_TEL;
	}
	public String getOFFICE_TEL() {
		return OFFICE_TEL;
	}
	public String getMOBILE_TEL() {
		return MOBILE_TEL;
	}
	public void setPOSTCOD(String pOSTCOD) {
		POSTCOD = pOSTCOD;
	}
	public void setADDRESS(String aDDRESS) {
		ADDRESS = aDDRESS;
	}
	public void setHOME_TEL(String hOME_TEL) {
		HOME_TEL = hOME_TEL;
	}
	public void setOFFICE_TEL(String oFFICE_TEL) {
		OFFICE_TEL = oFFICE_TEL;
	}
	public void setMOBILE_TEL(String mOBILE_TEL) {
		MOBILE_TEL = mOBILE_TEL;
	}
	
}
