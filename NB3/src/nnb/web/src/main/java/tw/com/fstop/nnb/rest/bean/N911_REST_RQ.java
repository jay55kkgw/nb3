package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N911_REST_RQ extends BaseRestBean_TW implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7622148160300384644L;

	private String HEADER;	// HEADER
	private String DATE;	// 日期YYYMMDD
	private String TIME;	// 時間HHMMSS
	private String PPSYNCN;	// P.P.Key Sync.Check Item
	private String PINNEW;	// 網路銀行密碼
	private String CUSIDN;	// 統一編號
	private String MMACOD;	// MMA註記
	private String USERID;	// 使用者名稱
	
	public String getHEADER() {
		return HEADER;
	}
	public void setHEADER(String hEADER) {
		HEADER = hEADER;
	}
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getPPSYNCN() {
		return PPSYNCN;
	}
	public void setPPSYNCN(String pPSYNCN) {
		PPSYNCN = pPSYNCN;
	}
	public String getPINNEW() {
		return PINNEW;
	}
	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getMMACOD() {
		return MMACOD;
	}
	public void setMMACOD(String mMACOD) {
		MMACOD = mMACOD;
	}
	public String getUSERID() {
		return USERID;
	}
	public void setUSERID(String uSERID) {
		USERID = uSERID;
	}

	
}
