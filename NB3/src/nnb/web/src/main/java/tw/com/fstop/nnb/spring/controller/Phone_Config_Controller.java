package tw.com.fstop.nnb.spring.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.google.gson.Gson;

import fstop.orm.po.ADMLOGIN;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.common.ResultCode;
import tw.com.fstop.idgate.bean.MTPCHANGEACCOUNT_IDGATE_DATA;
import tw.com.fstop.idgate.bean.MTPCHANGEACCOUNT_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.MTPUNBIND_IDGATE_DATA;
import tw.com.fstop.idgate.bean.MTPUNBIND_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.MTPUSER_IDGATE_DATA;
import tw.com.fstop.idgate.bean.MTPUSER_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N8301_1_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N8301_2_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N8301_2_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N8301_IDGATE_DATA;
import tw.com.fstop.nnb.service.IdGateService;
import tw.com.fstop.nnb.service.Phone_Config_Service;
import tw.com.fstop.nnb.service.Reset_Service;
import tw.com.fstop.tbb.nnb.util.StrUtils;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.DownloadUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.WebUtil;

@SessionAttributes({ 	SessionUtil.PD,SessionUtil.DOWNLOAD_DATALISTMAP_DATA,SessionUtil.PRINT_DATALISTMAP_DATA,
						SessionUtil.CONFIRM_LOCALE_DATA, SessionUtil.RESULT_LOCALE_DATA, SessionUtil.IDGATE_TRANSDATA, SessionUtil.CUSIDN,
						SessionUtil.TRANSFER_CONFIRM_TOKEN,SessionUtil.TRANSFER_RESULT_TOKEN,SessionUtil.TRANSFER_RESULT_FINSH_TOKEN,
						SessionUtil.TRANSFER_DATA,SessionUtil.DPMYEMAIL})
@Controller
@RequestMapping(value = "/PNONE/CONFIG")
public class Phone_Config_Controller {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private Phone_Config_Service phone_config_service;
	
	@Autowired
	IdGateService idgateservice;
	
	
	/**
	 * 進入點 ， 確認是否綁定本行決定到申請還是查詢頁
	 * */
	@RequestMapping(value = "/checkbind_bind")
	public String checkbind_bind(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("setting ...");
		String cusidn;
		Map<String,String> data = new HashMap<String, String>();
		try {
			cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			bs = phone_config_service.checkbind(cusidn);
			data = (Map<String, String>) bs.getData();
		}catch (Exception e) {
			log.error("phone_config_setting: {}", e);
		}finally {
			if(bs!=null && bs.getResult()) {
					target = "redirect:/PNONE/CONFIG/update_select";
			}else {
				if("MP4401".equals(data.get("RCode"))) 
					target = "redirect:/PNONE/CONFIG/setting";
				else
					model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * 手機號碼轉帳設定 輸入頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/setting")
	public String setting(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("setting ...");
		try {
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
		
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			bs = new BaseResult();
			
			
			// 是否是回上一頁
			if("Y".equals(okMap.get("back"))) {
				model.addAttribute("isBack","Y");
				Map<String, String> previousData = (Map) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				model.addAttribute("previousdata", CodeUtil.toJson(previousData));
				log.trace("previousdata: {}", CodeUtil.toJson(previousData));
			}
			
			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = phone_config_service.getTxToken();
			log.trace("phone_config_service.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			if(bs.getResult()) 
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			
			//IDGATE身分
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			String idgateUserFlag = "N";
			Map<String, Object> IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model,SessionUtil.IDGATE_TRANSDATA, null);
			try {
				if (IdgateData == null) {
					IdgateData = new HashMap<String, Object>();
				}
				BaseResult tmp = idgateservice.checkIdentity(cusidn);
				if (tmp.getResult()) {
					idgateUserFlag = "Y";
				}
				tmp.addData("idgateUserFlag", idgateUserFlag);
				IdgateData.putAll((Map<String, String>) tmp.getData());
				SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);
			} catch (Exception e) {
				log.debug("idgateUserFlag error {}", e);
			}
			model.addAttribute("idgateUserFlag", idgateUserFlag);
			
			//塞手機號碼以及身分證
			bs = phone_config_service.setting(cusidn);
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
			
		}catch (Exception e) {
			log.error("phone_config_setting: {}", e);
		}finally {
			if(bs!=null && bs.getResult()) 
				target = "/phone_config/phone_setting";
			else {
				//申請的進入點錯誤導回首頁
				bs.setNext("/INDEX/index");				
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * 手機號碼轉帳設定 確認頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/setting_confirm")
	public String setting_confirm(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs =  new BaseResult();
		String jsondc = "";
		log.trace("phone_config_setting_confirm...");
		try {
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");			
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			
			
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("f_transfer_r_confirm.jsondc: " + jsondc));
			okMap.put("jsondc", jsondc);
			okMap.put("LOGINTYPE", "NB"); // 系統別
			log.trace(ESAPIUtil.vaildLog("f_transfer_r_confirm.okMap: {}"+ CodeUtil.toJson(okMap)));
			
			
			// 回上一頁重新賦值
			if( okMap.containsKey("previous") ){
				Map<String, String> backMap = (Map<String, String>) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				okMap = backMap;
			}
			okMap = ESAPIUtil.validStrMap(okMap); 
			
			
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				if( !bs.getResult())
				{
					log.trace("bs.getResult(): {}", bs.getResult());
					throw new Exception();
				}else{
					// session 資料放入 map 讓後續 model 和回上一頁取值
					okMap.putAll((Map<String, String>) bs.getData());
				}
			}else {
				// 驗證TOKEN 沒TOKEN會到錯誤頁，錯誤頁確認後返回輸入頁
				String token = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null);
				log.trace("f_transfer_t_confirm.token: {}", token);
				if(StrUtil.isNotEmpty(okMap.get("TOKEN"))  &&  okMap.get("TOKEN").equals(token)) {
					bs.setResult(Boolean.TRUE);
				}
				log.trace("bs.getResult(): {}",bs.getResult());
				if(!bs.getResult()) {
					log.trace("throw new Exception()");
					throw new Exception();
				}
				// TOKEN驗證成功，資料處理後進入確認頁
				bs.reset();
				bs = phone_config_service.setting_confirm(okMap, cusidn);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
			}
			
			// IDGATE transdata            		 
            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
    		String adopid = "MTPUSER";
    		String title = "您有一筆 手機號碼轉帳帳號綁定 待確認";
        	if(IdgateData != null) {
	            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
	            if(IdgateData.get("idgateUserFlag").equals("Y")) {
	            	Map<String,String> result = new HashMap();
	            	result.putAll((Map< String,String>) bs.getData());
	            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
	            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
	            	result.put("cusidn", cusidn);
	            	result.put("systemflag", "NB3");
	            	IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(MTPUSER_IDGATE_DATA.class, 
							MTPUSER_IDGATE_DATA_VIEW.class, result));
	                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	            }
        	}
            model.addAttribute("idgateAdopid", adopid);
            model.addAttribute("idgateTitle", title);
            model.addAttribute("previous", "/PNONE/CONFIG/setting");
		}catch (Exception e) {
			log.error("phone_setting_confirm: {}", e);
		}finally {
			if(bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, ((Map<String,Object>)bs.getData()).get("TXTOKEN"));
				
				target = "/phone_config/phone_setting_confirm";
			}else {
				// 到錯誤頁，錯誤頁的確認按鈕導頁到輸入頁
				bs.setNext("/PNONE/CONFIG/setting");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * 手機號碼轉帳設定 結果頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/setting_result")
	public String setting_result(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("user_reset_result...");
		log.trace(ESAPIUtil.vaildLog("reqParam {}" +CodeUtil.toJson(reqParam)));
		
		String target = "/error";
		BaseResult bs =  new BaseResult();
		try {
			// 取得CUSIDN明碼
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		    
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			log.trace("setting_result.locale: " + hasLocale);
			if (hasLocale) {
				log.trace("setting_result.hasLocale...");
				// 判斷從session取出的資料物件是否為BaseResult物件
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( !bs.getResult() ){
					log.trace("setting_result.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				log.trace("setting_result.validate TXTOKEN...");
				log.trace("setting_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("setting_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("setting_result.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("setting_result TXTOKEN 不正確，TXTOKEN>>{}"+ okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						!okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("setting_result is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("setting_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				
				// 取得從輸入頁輸入存在session中的輸入資料
				bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
				Map<String,String> transfer_data = (Map<String, String>) bs.getData();
				// 取得SESSION中的輸入資料，加入到確認頁的交易機制資料
				okMap.putAll(transfer_data);
				// 重置bs，進行交易
				bs.reset();
				String usermail= (String) SessionUtil.getAttribute(model, SessionUtil.DPMYEMAIL, null);
				Map<String,Object> IDGATEDATA = new HashMap<String, Object>(); 
				//IDgate驗證		 
				if(okMap.get("FGTXWAY").equals("7")) {
	                try {		 
	        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
	        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("MTPUSER_IDGATE");
	        				okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
	                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
	                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));
	                        okMap.put("cusidn", cusidn);
	        				IDGATEDATA.putAll(okMap);
	        				IDGATEDATA.put("IDGATEDATA", okMap);
	        				IDGATEDATA.put("usermail", usermail);
	    				    bs = phone_config_service.phone_config_result(IDGATEDATA);
	                }catch(Exception e) {		 
	                    log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
	                }  
				}else {
					IDGATEDATA.putAll(okMap);
					IDGATEDATA.put("usermail", usermail);
					bs = phone_config_service.phone_config_result(IDGATEDATA);
				}
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
			Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
			model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
			
		}catch (Exception e) {
			log.error("Reset_service.keepPW: {}", e);
		}finally {
			if(bs != null && bs.getResult()) {
				target = "/phone_config/phone_setting_result";
				model.addAttribute("result_data", bs);
			}else {
				bs.setNext("/PNONE/CONFIG/checkbind_bind");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}


	/**
	 * 手機號碼轉帳設定 選擇頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/update_select")
	public String update_select(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("phone_update_select...");
		try {
			
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
		
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			
			bs = new BaseResult();
			
			// 是否是回上一頁
			if("Y".equals(okMap.get("back"))) {
				model.addAttribute("isBack","Y");
				Map<String, String> previousData = (Map) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				model.addAttribute("previousdata", CodeUtil.toJson(previousData));
				log.trace("previousdata: {}", CodeUtil.toJson(previousData));
			}
			
			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = phone_config_service.getTxToken();
			log.trace("phone_config_service.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			if(bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			}
			
			//IDGATE身分
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			String idgateUserFlag = "N";
			Map<String, Object> IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model,SessionUtil.IDGATE_TRANSDATA, null);
			try {
				if (IdgateData == null) {
					IdgateData = new HashMap<String, Object>();
				}
				BaseResult tmp = idgateservice.checkIdentity(cusidn);
				if (tmp.getResult()) {
					idgateUserFlag = "Y";
				}
				tmp.addData("idgateUserFlag", idgateUserFlag);
				IdgateData.putAll((Map<String, String>) tmp.getData());
				SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);
			} catch (Exception e) {
				log.debug("idgateUserFlag error {}", e);
			}
			model.addAttribute("idgateUserFlag", idgateUserFlag);
			
			bs = phone_config_service.update_select(cusidn, okMap);
			
		}catch (Exception e) {
			log.error("phone_config_setting: {}", e);
		}finally {
			if(bs!=null && bs.getResult()) {
				model.addAttribute("result_data", bs);
				target = "/phone_config/phone_update_select";
			}else {
				bs.setNext("/INDEX/index");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * 手機號碼轉帳設定修改 輸入頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/update_change")
	public String update_change(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("update_change...");
		try {
			
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
		
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			
			bs = new BaseResult();
			
			// 是否是回上一頁
			if("Y".equals(okMap.get("back"))) {
				model.addAttribute("isBack","Y");
				Map<String, String> previousData = (Map) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				model.addAttribute("previousdata", CodeUtil.toJson(previousData));
				log.trace("previousdata: {}", CodeUtil.toJson(previousData));
			}
			
			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = phone_config_service.getTxToken();
			log.trace("phone_config_service.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			if(bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			}
			bs.addAllData(okMap);
			model.addAttribute("result_data", bs);
			model.addAttribute("previous", "/PNONE/CONFIG/update_select");
		}catch (Exception e) {
			log.error("phone_config_setting: {}", e);
		}finally {
			if(bs!=null && bs.getResult()) {
				target = "/phone_config/phone_update_change";
			}else {
				bs.setNext("/INDEX/index");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	
	/**
	 * 手機號碼轉帳設定更改 確認頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/update_change_confirm")
	public String update_change_confirm(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs =  new BaseResult();
		String jsondc = "";
		log.trace("phone_update_change_confirm...");
		try {
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");			
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("f_transfer_r_confirm.jsondc: " + jsondc));
			okMap.put("jsondc", jsondc);
			okMap.put("LOGINTYPE", "NB"); // 系統別
			log.trace(ESAPIUtil.vaildLog("f_transfer_r_confirm.okMap: {}"+ CodeUtil.toJson(okMap)));
			
			
			// 回上一頁重新賦值
			if( okMap.containsKey("previous") ){
				Map<String, String> backMap = (Map<String, String>) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				okMap = backMap;
			}
			okMap = ESAPIUtil.validStrMap(okMap); 
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				if( !bs.getResult())
				{
					log.trace("bs.getResult(): {}", bs.getResult());
					throw new Exception();
				}else{
					// session 資料放入 map 讓後續 model 和回上一頁取值
					okMap.putAll((Map<String, String>) bs.getData());
				}
			}else {
				// 驗證TOKEN 沒TOKEN會到錯誤頁，錯誤頁確認後返回輸入頁
				String token = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null);
				log.trace("f_transfer_t_confirm.token: {}", token);
				if(StrUtil.isNotEmpty(okMap.get("TOKEN"))  &&  okMap.get("TOKEN").equals(token)) {
					bs.setResult(Boolean.TRUE);
				}
				log.trace("bs.getResult(): {}",bs.getResult());
				if(!bs.getResult()) {
					log.trace("throw new Exception()");
					throw new Exception();
				}
				
				// TOKEN驗證成功，資料處理後進入確認頁
				bs.reset();
				bs = phone_config_service.phone_update_change_confirm(okMap);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
			}
			
			// IDGATE transdata            		 
            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
    		String adopid = "MTPCHANGEACCOUNT";
    		String title = "您有一筆 手機號碼變更轉入帳號 待確認";
        	if(IdgateData != null) {
	            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
	            if(IdgateData.get("idgateUserFlag").equals("Y")) {
	            	Map<String,String> result = new HashMap();
	            	result.putAll((Map< String,String>) bs.getData());
    				result.put("cusidn", cusidn);
	            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
	            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
	            	result.put("txacn1", result.get("oldtxacn"));
	            	result.put("txacn2", result.get("txacn"));
	            	result.put("systemflag", "NB3");
					IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(MTPCHANGEACCOUNT_IDGATE_DATA.class, MTPCHANGEACCOUNT_IDGATE_DATA_VIEW.class, result));
	                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	            }
        	}
            model.addAttribute("idgateAdopid", adopid);
            model.addAttribute("idgateTitle", title);
            model.addAttribute("previous", "/PNONE/CONFIG/update_select");
		}catch (Exception e) {
			log.error("phone_setting_confirm: {}", e);
		}finally {
			if(bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, ((Map<String,Object>)bs.getData()).get("TXTOKEN"));
				
				target = "/phone_config/phone_update_change_confirm";
			}else {
				// 到錯誤頁，錯誤頁的確認按鈕導頁到輸入頁
				bs.setNext("/PNONE/CONFIG/update_select");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * 手機號碼轉帳設定 結果頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/update_change_result")
	public String update_select_change_result(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("user_reset_result...");
		log.trace(ESAPIUtil.vaildLog("reqParam {}" +CodeUtil.toJson(reqParam)));
		
		String target = "/error";
		BaseResult bs =  new BaseResult();
		try {
			// 取得CUSIDN明碼
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			log.trace("update_select_change_result.locale: " + hasLocale);
			if (hasLocale) {
				log.trace("update_select_change_result.hasLocale...");
				// 判斷從session取出的資料物件是否為BaseResult物件
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( !bs.getResult() ){
					log.trace("update_select_change_result.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				log.trace("update_select_change_result.validate TXTOKEN...");
				log.trace("update_select_change_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("update_select_change_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("setting_result.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("setting_result TXTOKEN 不正確，TXTOKEN>>{}"+ okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						!okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("setting_result is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("setting_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				
				// 取得從輸入頁輸入存在session中的輸入資料
				bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
				Map<String,String> transfer_data = (Map<String, String>) bs.getData();
				// 取得SESSION中的輸入資料，加入到確認頁的交易機制資料
				okMap.putAll(transfer_data);
				// 重置bs，進行交易
				bs.reset();
				
				Map<String,Object> IDGATEDATA = new HashMap<String, Object>();
				String usermail= (String) SessionUtil.getAttribute(model, SessionUtil.DPMYEMAIL, null);
				if(okMap.get("FGTXWAY").equals("7")) {
	                try {		 
	        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
	        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("MTPCHANGEACCOUNT_IDGATE");
	        				okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
	                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
	                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));
	                        okMap.put("cusidn", cusidn);
	                        //補驗證
	                        okMap.put("systemflag", "NB3");
	                        okMap.put("txacn1", okMap.get("oldtxacn"));
	                        okMap.put("txacn2", okMap.get("txacn"));
	        				IDGATEDATA.putAll(okMap);
	        				IDGATEDATA.put("IDGATEDATA", okMap);
	        				IDGATEDATA.put("usermail", usermail);
	    					bs = phone_config_service.update_select_change_result(IDGATEDATA);
	                }catch(Exception e) {		 
	                    log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
	                }  
				}else {
					IDGATEDATA.putAll(okMap);
					IDGATEDATA.put("usermail", usermail);
				    bs = phone_config_service.update_select_change_result(IDGATEDATA);
				}
            
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
			model.addAttribute("transfer_result_data", bs);
			Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
			model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
			
		}catch (Exception e) {
			log.error("Reset_service.keepPW: {}", e);
		}finally {
			if(bs != null && bs.getResult()) {
				target = "/phone_config/phone_update_change_result";
				model.addAttribute("result_data", bs);
			}else {
				bs.setNext("/PNONE/CONFIG/update_select");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	
	
	/**
	 * 手機號碼轉帳註銷 確認頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/update_cannel_confirm")
	public String update_cannel_confirm(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs =  new BaseResult();
		String jsondc = "";
		log.trace("phone_update_cannel_confirm...");
		try {
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");			
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("f_transfer_r_confirm.jsondc: " + jsondc));
			okMap.put("jsondc", jsondc);
			okMap.put("LOGINTYPE", "NB"); // 系統別
			log.trace(ESAPIUtil.vaildLog("f_transfer_r_confirm.okMap: {}"+ CodeUtil.toJson(okMap)));
			
			
			// 回上一頁重新賦值
			if( okMap.containsKey("previous") ){
				Map<String, String> backMap = (Map<String, String>) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				okMap = backMap;
			}
			okMap = ESAPIUtil.validStrMap(okMap); 
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				if( !bs.getResult())
				{
					log.trace("bs.getResult(): {}", bs.getResult());
					throw new Exception();
				}else{
					// session 資料放入 map 讓後續 model 和回上一頁取值
					okMap.putAll((Map<String, String>) bs.getData());
				}
			}else {
				// 驗證TOKEN 沒TOKEN會到錯誤頁，錯誤頁確認後返回輸入頁
				String token = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null);
				log.trace("f_transfer_t_confirm.token: {}", token);
				if(StrUtil.isNotEmpty(okMap.get("TOKEN"))  &&  okMap.get("TOKEN").equals(token)) {
					bs.setResult(Boolean.TRUE);
				}
				log.trace("bs.getResult(): {}",bs.getResult());
				if(!bs.getResult()) {
					log.trace("throw new Exception()");
					throw new Exception();
				}
				
				// TOKEN驗證成功，資料處理後進入確認頁
				bs.reset();
				bs = phone_config_service.phone_update_cannel_confirm(okMap);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
			}
			
			Map<String,String> result = new HashMap();
			result.putAll((Map< String,String>) bs.getData());
			// IDGATE transdata            		 
            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
    		String adopid = "MTPUNBIND";
    		String title = "您有一筆 手機號碼註銷轉入帳號 待確認";
        	if(IdgateData != null) {
	            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
	            if(IdgateData.get("idgateUserFlag").equals("Y")) {
	            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
	            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
	            	result.put("cusidn", cusidn);
	            	result.put("systemflag", "NB3");
					IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(MTPUNBIND_IDGATE_DATA.class, MTPUNBIND_IDGATE_DATA_VIEW.class, result));
	                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	            }
        	}
            model.addAttribute("idgateAdopid", adopid);
            model.addAttribute("idgateTitle", title);
            model.addAttribute("previous", "/PNONE/CONFIG/update_select");
		}catch (Exception e) {
			log.error("phone_setting_confirm: {}", e);
		}finally {
			if(bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, ((Map<String,Object>)bs.getData()).get("TXTOKEN"));
				
				target = "/phone_config/phone_update_cannel_confirm";
			}else {
				// 到錯誤頁，錯誤頁的確認按鈕導頁到輸入頁
				bs.setNext("/PNONE/CONFIG/update_select");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	
	/**
	 * 手機號碼轉帳註銷 結果頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/update_cannel_result")
	public String update_cannel_result(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("update_select_cannel_result...");
		log.trace(ESAPIUtil.vaildLog("reqParam {}" +CodeUtil.toJson(reqParam)));
		
		String target = "/error";
		BaseResult bs =  new BaseResult();
		try {
			// 取得CUSIDN明碼
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			log.trace("update_select_cannel_result.locale: " + hasLocale);
			if (hasLocale) {
				log.trace("update_select_change_result.hasLocale...");
				// 判斷從session取出的資料物件是否為BaseResult物件
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( !bs.getResult() ){
					log.trace("update_select_change_result.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				log.trace("update_select_cannel_result.validate TXTOKEN...");
				log.trace("update_select_cannel_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("update_select_cannel_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("setting_result.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("setting_result TXTOKEN 不正確，TXTOKEN>>{}"+ okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						!okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("update_select_cannel_result is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("setting_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				
				// 取得從輸入頁輸入存在session中的輸入資料
				bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
				Map<String,String> transfer_data = (Map<String, String>) bs.getData();
				// 取得SESSION中的輸入資料，加入到確認頁的交易機制資料
				okMap.putAll(transfer_data);
				// 重置bs，進行交易
				bs.reset();
				
				Map<String,Object> IDGATEDATA = new HashMap<String, Object>(); 
			    IDGATEDATA.putAll(okMap);
			    String usermail= (String) SessionUtil.getAttribute(model, SessionUtil.DPMYEMAIL, null);
			    IDGATEDATA.put("usermail",usermail);
				bs = phone_config_service.update_select_cannel_result(IDGATEDATA);
               
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
			model.addAttribute("transfer_result_data", bs);
			Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
			model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
			
		}catch (Exception e) {
			log.error("Reset_service.keepPW: {}", e);
		}finally {
			if(bs != null && bs.getResult()) {
				target = "/phone_config/phone_update_cannel_result";
				model.addAttribute("result_data", bs);
			}else {
				bs.setNext("/PNONE/CONFIG/update_select");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	
	/**
	 * 轉帳交易前查詢收款戶資訊
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/getACCOUNTNAME", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult getACCOUNTNAME(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		BaseResult bs = new BaseResult();
		try {
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			bs = phone_config_service.getACCOUNTNAME(okMap);
		} catch (Exception e) {
			log.error("getACCOUNTNAME error >> {}", e);
			bs.setMessage(ResultCode.SYS_ERROR);
		} 
		return bs;
	}
	
	/**
	 * 查詢有參加的銀行
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/getBankList", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult  getBankList(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		BaseResult bs = new BaseResult();
		try {
			bs = phone_config_service.getBankList(reqParam);
		} catch (Exception e) {
			log.error("getBankList error >> {}", e);
			bs.setMessage(ResultCode.SYS_ERROR);
		} 
		return bs;
	}
	
	/**
	 * 取得銀行代號名稱
	 * 
	 * @param request
	 * @param response
	 * @param reqParama
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/check_phone", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String check_phone(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String result = "false";
		log.trace(ESAPIUtil.vaildLog("reqParam >> " + CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		try {
			bs = phone_config_service.check_phone(reqParam);
			if(bs.getResult()) 
				result = "true";
		} catch (Exception e) {
			log.error("check_phone error >> {}", e);
		}
		return result;
	}
}
