package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * NA01RQ
 */
public class Na01_REST_RQ extends BaseRestBean_LOAN implements Serializable{
	private static final long serialVersionUID = 6868861910004819383L;
	private String CUSIDN;
	private String UID;
	private String FGTXWAY;
	private String PINNEW;
	private String RCVNO;
	private String APPKIND;
	private String APPAMT;
	private String PURPOSE;
	private String PERIODS;
	private String BANKID;
	private String BANKNA;
	private String SEX;
	private String APPNAME;
	private String APPIDNO;
	private String BRTHDT;
	private String EDUS;
	private String MARITAL;
	private String CHILDNO;
	private String CPHONE;
	private String PHONE_01;
	private String PHONE_02;
	private String PHONE_03;
	private String PHONE_11;
	private String PHONE_12;
	private String HOUSE;
	private String MONEY11;
	private String MONEY12;
	private String ZIP1;
	private String CITY1;
	private String ZONE1;
	private String ADDR1;
	private String ZIP2;
	private String CITY2;
	private String ZONE2;
	private String ADDR2;
	private String ZIP3;
	private String CITY3;
	private String ZONE3;
	private String ADDR3;
	private String EMAIL;
	private String CAREERNAME;
	private String CAREERIDNO;
	private String ZIP4;
	private String CITY4;
	private String ZONE4;
	private String ADDR4;
	private String PHONE_41;
	private String PHONE_42;
	private String PHONE_43;
	private String OFFICEY;
	private String OFFICEM;
	private String PROFNO;
	private String INCOME;
	private String PRECARNAME;
	private String PRETKY;
	private String PRETKM;
	private String CKS1;
	private String RELTYPE1;
	private String RELNAME1;
	private String RELID1;
	private String CKS2;
	private String RELTYPE2;
	private String RELNAME2;
	private String RELID2;
	private String CKS3;
	private String RELTYPE3;
	private String RELNAME3;
	private String RELID3;
	private String CKS4;
	private String RELTYPE4;
	private String RELNAME4;
	private String RELID4;
	private String CKP1;
	private String OTHCONM1;
	private String OTHCOID1;
	private String CKM1;
	private String MOTHCONM1;
	private String MOTHCOID1;
	private String PAYSOURCE;
	private String ECERT;
	private String FILE1;
	private String FILE2;
	private String FILE3;
	private String LASTDATE;
	private String LASTTIME;
	private String STATUS;
	private String IP;
	private String FILE11;
	private String LOGINTYPE;
	
	private String iSeqNo;
	private String ISSUER;
	private String ACNNO;
	private String TRMID;
	private String ICSEQ;
	private String TAC;
	private String pkcs7Sign;
	private String jsondc;
	
	public String getCUSIDN(){
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN){
		CUSIDN = cUSIDN;
	}
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getPINNEW() {
		return PINNEW;
	}
	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}
	public String getRCVNO() {
		return RCVNO;
	}
	public void setRCVNO(String rCVNO) {
		RCVNO = rCVNO;
	}
	public String getAPPKIND() {
		return APPKIND;
	}
	public void setAPPKIND(String aPPKIND) {
		APPKIND = aPPKIND;
	}
	public String getAPPAMT() {
		return APPAMT;
	}
	public void setAPPAMT(String aPPAMT) {
		APPAMT = aPPAMT;
	}
	public String getPURPOSE() {
		return PURPOSE;
	}
	public void setPURPOSE(String pURPOSE) {
		PURPOSE = pURPOSE;
	}
	public String getPERIODS() {
		return PERIODS;
	}
	public void setPERIODS(String pERIODS) {
		PERIODS = pERIODS;
	}
	public String getBANKID() {
		return BANKID;
	}
	public void setBANKID(String bANKID) {
		BANKID = bANKID;
	}
	public String getBANKNA() {
		return BANKNA;
	}
	public void setBANKNA(String bANKNA) {
		BANKNA = bANKNA;
	}
	public String getSEX() {
		return SEX;
	}
	public void setSEX(String sEX) {
		SEX = sEX;
	}
	public String getAPPNAME() {
		return APPNAME;
	}
	public void setAPPNAME(String aPPNAME) {
		APPNAME = aPPNAME;
	}
	public String getAPPIDNO() {
		return APPIDNO;
	}
	public void setAPPIDNO(String aPPIDNO) {
		APPIDNO = aPPIDNO;
	}
	public String getBRTHDT() {
		return BRTHDT;
	}
	public void setBRTHDT(String bRTHDT) {
		BRTHDT = bRTHDT;
	}
	public String getEDUS() {
		return EDUS;
	}
	public void setEDUS(String eDUS) {
		EDUS = eDUS;
	}
	public String getMARITAL() {
		return MARITAL;
	}
	public void setMARITAL(String mARITAL) {
		MARITAL = mARITAL;
	}
	public String getCHILDNO() {
		return CHILDNO;
	}
	public void setCHILDNO(String cHILDNO) {
		CHILDNO = cHILDNO;
	}
	public String getCPHONE() {
		return CPHONE;
	}
	public void setCPHONE(String cPHONE) {
		CPHONE = cPHONE;
	}
	public String getPHONE_01() {
		return PHONE_01;
	}
	public void setPHONE_01(String pHONE_01) {
		PHONE_01 = pHONE_01;
	}
	public String getPHONE_02() {
		return PHONE_02;
	}
	public void setPHONE_02(String pHONE_02) {
		PHONE_02 = pHONE_02;
	}
	public String getPHONE_03() {
		return PHONE_03;
	}
	public void setPHONE_03(String pHONE_03) {
		PHONE_03 = pHONE_03;
	}
	public String getPHONE_11() {
		return PHONE_11;
	}
	public void setPHONE_11(String pHONE_11) {
		PHONE_11 = pHONE_11;
	}
	public String getPHONE_12() {
		return PHONE_12;
	}
	public void setPHONE_12(String pHONE_12) {
		PHONE_12 = pHONE_12;
	}
	public String getHOUSE() {
		return HOUSE;
	}
	public void setHOUSE(String hOUSE) {
		HOUSE = hOUSE;
	}
	public String getMONEY11() {
		return MONEY11;
	}
	public void setMONEY11(String mONEY11) {
		MONEY11 = mONEY11;
	}
	public String getMONEY12() {
		return MONEY12;
	}
	public void setMONEY12(String mONEY12) {
		MONEY12 = mONEY12;
	}
	public String getZIP1() {
		return ZIP1;
	}
	public void setZIP1(String zIP1) {
		ZIP1 = zIP1;
	}
	public String getCITY1() {
		return CITY1;
	}
	public void setCITY1(String cITY1) {
		CITY1 = cITY1;
	}
	public String getZONE1() {
		return ZONE1;
	}
	public void setZONE1(String zONE1) {
		ZONE1 = zONE1;
	}
	public String getADDR1() {
		return ADDR1;
	}
	public void setADDR1(String aDDR1) {
		ADDR1 = aDDR1;
	}
	public String getZIP2() {
		return ZIP2;
	}
	public void setZIP2(String zIP2) {
		ZIP2 = zIP2;
	}
	public String getCITY2() {
		return CITY2;
	}
	public void setCITY2(String cITY2) {
		CITY2 = cITY2;
	}
	public String getZONE2() {
		return ZONE2;
	}
	public void setZONE2(String zONE2) {
		ZONE2 = zONE2;
	}
	public String getADDR2() {
		return ADDR2;
	}
	public void setADDR2(String aDDR2) {
		ADDR2 = aDDR2;
	}
	public String getZIP3() {
		return ZIP3;
	}
	public void setZIP3(String zIP3) {
		ZIP3 = zIP3;
	}
	public String getCITY3() {
		return CITY3;
	}
	public void setCITY3(String cITY3) {
		CITY3 = cITY3;
	}
	public String getZONE3() {
		return ZONE3;
	}
	public void setZONE3(String zONE3) {
		ZONE3 = zONE3;
	}
	public String getADDR3() {
		return ADDR3;
	}
	public void setADDR3(String aDDR3) {
		ADDR3 = aDDR3;
	}
	public String getEMAIL() {
		return EMAIL;
	}
	public void setEMAIL(String eMAIL) {
		EMAIL = eMAIL;
	}
	public String getCAREERNAME() {
		return CAREERNAME;
	}
	public void setCAREERNAME(String cAREERNAME) {
		CAREERNAME = cAREERNAME;
	}
	public String getCAREERIDNO() {
		return CAREERIDNO;
	}
	public void setCAREERIDNO(String cAREERIDNO) {
		CAREERIDNO = cAREERIDNO;
	}
	public String getZIP4() {
		return ZIP4;
	}
	public void setZIP4(String zIP4) {
		ZIP4 = zIP4;
	}
	public String getCITY4() {
		return CITY4;
	}
	public void setCITY4(String cITY4) {
		CITY4 = cITY4;
	}
	public String getZONE4() {
		return ZONE4;
	}
	public void setZONE4(String zONE4) {
		ZONE4 = zONE4;
	}
	public String getADDR4() {
		return ADDR4;
	}
	public void setADDR4(String aDDR4) {
		ADDR4 = aDDR4;
	}
	public String getPHONE_41() {
		return PHONE_41;
	}
	public void setPHONE_41(String pHONE_41) {
		PHONE_41 = pHONE_41;
	}
	public String getPHONE_42() {
		return PHONE_42;
	}
	public void setPHONE_42(String pHONE_42) {
		PHONE_42 = pHONE_42;
	}
	public String getPHONE_43() {
		return PHONE_43;
	}
	public void setPHONE_43(String pHONE_43) {
		PHONE_43 = pHONE_43;
	}
	public String getOFFICEY() {
		return OFFICEY;
	}
	public void setOFFICEY(String oFFICEY) {
		OFFICEY = oFFICEY;
	}
	public String getOFFICEM() {
		return OFFICEM;
	}
	public void setOFFICEM(String oFFICEM) {
		OFFICEM = oFFICEM;
	}
	public String getPROFNO() {
		return PROFNO;
	}
	public void setPROFNO(String pROFNO) {
		PROFNO = pROFNO;
	}
	public String getINCOME() {
		return INCOME;
	}
	public void setINCOME(String iNCOME) {
		INCOME = iNCOME;
	}
	public String getPRECARNAME() {
		return PRECARNAME;
	}
	public void setPRECARNAME(String pRECARNAME) {
		PRECARNAME = pRECARNAME;
	}
	public String getPRETKY() {
		return PRETKY;
	}
	public void setPRETKY(String pRETKY) {
		PRETKY = pRETKY;
	}
	public String getPRETKM() {
		return PRETKM;
	}
	public void setPRETKM(String pRETKM) {
		PRETKM = pRETKM;
	}
	public String getCKS1() {
		return CKS1;
	}
	public void setCKS1(String cKS1) {
		CKS1 = cKS1;
	}
	public String getRELTYPE1() {
		return RELTYPE1;
	}
	public void setRELTYPE1(String rELTYPE1) {
		RELTYPE1 = rELTYPE1;
	}
	public String getRELNAME1() {
		return RELNAME1;
	}
	public void setRELNAME1(String rELNAME1) {
		RELNAME1 = rELNAME1;
	}
	public String getRELID1() {
		return RELID1;
	}
	public void setRELID1(String rELID1) {
		RELID1 = rELID1;
	}
	public String getCKS2() {
		return CKS2;
	}
	public void setCKS2(String cKS2) {
		CKS2 = cKS2;
	}
	public String getRELTYPE2() {
		return RELTYPE2;
	}
	public void setRELTYPE2(String rELTYPE2) {
		RELTYPE2 = rELTYPE2;
	}
	public String getRELNAME2() {
		return RELNAME2;
	}
	public void setRELNAME2(String rELNAME2) {
		RELNAME2 = rELNAME2;
	}
	public String getRELID2() {
		return RELID2;
	}
	public void setRELID2(String rELID2) {
		RELID2 = rELID2;
	}
	public String getCKS3() {
		return CKS3;
	}
	public void setCKS3(String cKS3) {
		CKS3 = cKS3;
	}
	public String getRELTYPE3() {
		return RELTYPE3;
	}
	public void setRELTYPE3(String rELTYPE3) {
		RELTYPE3 = rELTYPE3;
	}
	public String getRELNAME3() {
		return RELNAME3;
	}
	public void setRELNAME3(String rELNAME3) {
		RELNAME3 = rELNAME3;
	}
	public String getRELID3() {
		return RELID3;
	}
	public void setRELID3(String rELID3) {
		RELID3 = rELID3;
	}
	public String getCKS4() {
		return CKS4;
	}
	public void setCKS4(String cKS4) {
		CKS4 = cKS4;
	}
	public String getRELTYPE4() {
		return RELTYPE4;
	}
	public void setRELTYPE4(String rELTYPE4) {
		RELTYPE4 = rELTYPE4;
	}
	public String getRELNAME4() {
		return RELNAME4;
	}
	public void setRELNAME4(String rELNAME4) {
		RELNAME4 = rELNAME4;
	}
	public String getRELID4() {
		return RELID4;
	}
	public void setRELID4(String rELID4) {
		RELID4 = rELID4;
	}
	public String getCKP1() {
		return CKP1;
	}
	public void setCKP1(String cKP1) {
		CKP1 = cKP1;
	}
	public String getOTHCONM1() {
		return OTHCONM1;
	}
	public void setOTHCONM1(String oTHCONM1) {
		OTHCONM1 = oTHCONM1;
	}
	public String getOTHCOID1() {
		return OTHCOID1;
	}
	public void setOTHCOID1(String oTHCOID1) {
		OTHCOID1 = oTHCOID1;
	}
	public String getCKM1() {
		return CKM1;
	}
	public void setCKM1(String cKM1) {
		CKM1 = cKM1;
	}
	public String getMOTHCONM1() {
		return MOTHCONM1;
	}
	public void setMOTHCONM1(String mOTHCONM1) {
		MOTHCONM1 = mOTHCONM1;
	}
	public String getMOTHCOID1() {
		return MOTHCOID1;
	}
	public void setMOTHCOID1(String mOTHCOID1) {
		MOTHCOID1 = mOTHCOID1;
	}
	public String getPAYSOURCE() {
		return PAYSOURCE;
	}
	public void setPAYSOURCE(String pAYSOURCE) {
		PAYSOURCE = pAYSOURCE;
	}
	public String getECERT() {
		return ECERT;
	}
	public void setECERT(String eCERT) {
		ECERT = eCERT;
	}
	public String getFILE1() {
		return FILE1;
	}
	public void setFILE1(String fILE1) {
		FILE1 = fILE1;
	}
	public String getFILE2() {
		return FILE2;
	}
	public void setFILE2(String fILE2) {
		FILE2 = fILE2;
	}
	public String getFILE3() {
		return FILE3;
	}
	public void setFILE3(String fILE3) {
		FILE3 = fILE3;
	}
	public String getLASTDATE() {
		return LASTDATE;
	}
	public void setLASTDATE(String lASTDATE) {
		LASTDATE = lASTDATE;
	}
	public String getLASTTIME() {
		return LASTTIME;
	}
	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}
	public String getSTATUS() {
		return STATUS;
	}
	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}
	public String getIP() {
		return IP;
	}
	public void setIP(String iP) {
		IP = iP;
	}
	public String getFILE11() {
		return FILE11;
	}
	public void setFILE11(String fILE11) {
		FILE11 = fILE11;
	}
	public String getLOGINTYPE() {
		return LOGINTYPE;
	}
	public void setLOGINTYPE(String lOGINTYPE) {
		LOGINTYPE = lOGINTYPE;
	}
	public String getACNNO() {
		return ACNNO;
	}
	public void setACNNO(String aCNNO) {
		ACNNO = aCNNO;
	}
	public String getICSEQ() {
		return ICSEQ;
	}
	public void setICSEQ(String iCSEQ) {
		ICSEQ = iCSEQ;
	}
	public String getiSeqNo(){
		return iSeqNo;
	}
	public void setiSeqNo(String iSeqNo){
		this.iSeqNo = iSeqNo;
	}
	public String getISSUER(){
		return ISSUER;
	}
	public void setISSUER(String iSSUER){
		ISSUER = iSSUER;
	}
	public String getTRMID(){
		return TRMID;
	}
	public void setTRMID(String tRMID){
		TRMID = tRMID;
	}
	public String getTAC(){
		return TAC;
	}
	public void setTAC(String tAC){
		TAC = tAC;
	}
	public String getPkcs7Sign(){
		return pkcs7Sign;
	}
	public void setPkcs7Sign(String pkcs7Sign){
		this.pkcs7Sign = pkcs7Sign;
	}
	public String getJsondc(){
		return jsondc;
	}
	public void setJsondc(String jsondc){
		this.jsondc = jsondc;
	}
}