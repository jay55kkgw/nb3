package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N570_REST_RQ extends BaseRestBean_OLS implements Serializable {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 2668918667471826861L;
	
	private String CUSIDN;//使用者帳號
	private String ACCNO;//帳號
	
	private String BCHID;//分行別
	private String CUSAD1C;//中文地址1
	private String CUSAD2C;//中文地址2	
	private String CUSADD1;//英文地址1
	private String CUSADD2;//英文地址2
	private String CUSADD3;//英文地址3
	private String CUSTEL1;//電話號碼
	private String CUSTEL2;//傳真號碼
	private String RMARK;//備註
	private String ADOPID;

	//晶片金融卡
	private	String	ACNNO;
	private	String	FGTXWAY;
	private	String	iSeqNo;
	private	String	TAC;
	private	String	ISSUER;
	private	String	TRMID;
	private	String	pkcs7Sign;
	
	//IDGATE
	private String sessionID;
	private String idgateID;
	private String txnID;
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getACCNO() {
		return ACCNO;
	}
	public void setACCNO(String aCCNO) {
		ACCNO = aCCNO;
	}
	public String getBCHID() {
		return BCHID;
	}
	public void setBCHID(String bCHID) {
		BCHID = bCHID;
	}
	public String getCUSAD1C() {
		return CUSAD1C;
	}
	public void setCUSAD1C(String cUSAD1C) {
		CUSAD1C = cUSAD1C;
	}
	public String getCUSAD2C() {
		return CUSAD2C;
	}
	public void setCUSAD2C(String cUSAD2C) {
		CUSAD2C = cUSAD2C;
	}
	public String getCUSADD1() {
		return CUSADD1;
	}
	public void setCUSADD1(String cUSADD1) {
		CUSADD1 = cUSADD1;
	}
	public String getCUSADD2() {
		return CUSADD2;
	}
	public void setCUSADD2(String cUSADD2) {
		CUSADD2 = cUSADD2;
	}
	public String getCUSADD3() {
		return CUSADD3;
	}
	public void setCUSADD3(String cUSADD3) {
		CUSADD3 = cUSADD3;
	}
	public String getCUSTEL1() {
		return CUSTEL1;
	}
	public void setCUSTEL1(String cUSTEL1) {
		CUSTEL1 = cUSTEL1;
	}
	public String getCUSTEL2() {
		return CUSTEL2;
	}
	public void setCUSTEL2(String cUSTEL2) {
		CUSTEL2 = cUSTEL2;
	}
	public String getRMARK() {
		return RMARK;
	}
	public void setRMARK(String rMARK) {
		RMARK = rMARK;
	}
	public String getACNNO() {
		return ACNNO;
	}
	public void setACNNO(String aCNNO) {
		ACNNO = aCNNO;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getiSeqNo() {
		return iSeqNo;
	}
	public void setiSeqNo(String iSeqNo) {
		this.iSeqNo = iSeqNo;
	}
	public String getTAC() {
		return TAC;
	}
	public void setTAC(String tAC) {
		TAC = tAC;
	}
	public String getISSUER() {
		return ISSUER;
	}
	public void setISSUER(String iSSUER) {
		ISSUER = iSSUER;
	}
	public String getTRMID() {
		return TRMID;
	}
	public void setTRMID(String tRMID) {
		TRMID = tRMID;
	}
	public String getPkcs7Sign() {
		return pkcs7Sign;
	}
	public void setPkcs7Sign(String pkcs7Sign) {
		this.pkcs7Sign = pkcs7Sign;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getSessionID() {
		return sessionID;
	}
	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}
	public String getIdgateID() {
		return idgateID;
	}
	public void setIdgateID(String idgateID) {
		this.idgateID = idgateID;
	}
	public String getTxnID() {
		return txnID;
	}
	public void setTxnID(String txnID) {
		this.txnID = txnID;
	}
	


	

	
}
