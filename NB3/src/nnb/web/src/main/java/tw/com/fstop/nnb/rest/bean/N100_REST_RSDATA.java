package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N100_REST_RSDATA extends BaseRestBean implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1337081657445455831L;

	// n200
	private String ACN;

	private String BRHCOD;// 行庫代號

	private String POSTCOD2;// 通訊地郵遞區號

	private String CTTADR;// 通訊地址

	private String ARACOD;// 連絡電話區域碼 1

	private String TELNUM;// 連絡電話號碼 1

	private String ARACOD2;// 連絡電話區域碼 2"

	private String TELNUM2;// 連絡電話號碼 2

	private String MOBTEL;// 行動電話

	public String getACN()
	{
		return ACN;
	}

	public void setACN(String aCN)
	{
		ACN = aCN;
	}

	public String getBRHCOD()
	{
		return BRHCOD;
	}

	public void setBRHCOD(String bRHCOD)
	{
		BRHCOD = bRHCOD;
	}

	public String getPOSTCOD2()
	{
		return POSTCOD2;
	}

	public void setPOSTCOD2(String pOSTCOD2)
	{
		POSTCOD2 = pOSTCOD2;
	}

	public String getCTTADR()
	{
		return CTTADR;
	}

	public void setCTTADR(String cTTADR)
	{
		CTTADR = cTTADR;
	}

	public String getARACOD()
	{
		return ARACOD;
	}

	public void setARACOD(String aRACOD)
	{
		ARACOD = aRACOD;
	}

	public String getTELNUM()
	{
		return TELNUM;
	}

	public void setTELNUM(String tELNUM)
	{
		TELNUM = tELNUM;
	}

	public String getARACOD2()
	{
		return ARACOD2;
	}

	public void setARACOD2(String aRACOD2)
	{
		ARACOD2 = aRACOD2;
	}

	public String getTELNUM2()
	{
		return TELNUM2;
	}

	public void setTELNUM2(String tELNUM2)
	{
		TELNUM2 = tELNUM2;
	}

	public String getMOBTEL()
	{
		return MOBTEL;
	}

	public void setMOBTEL(String mOBTEL)
	{
		MOBTEL = mOBTEL;
	}

}
