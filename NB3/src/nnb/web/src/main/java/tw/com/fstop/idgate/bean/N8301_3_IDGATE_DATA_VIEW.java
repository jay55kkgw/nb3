package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;

//第一類至第四類投保單位
public class N8301_3_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{
	private static final long serialVersionUID = -5370773529503299761L;
	/**
	 * 
	 */
	
	@SerializedName(value = "CARDNUM")
	private String CARDNUM;
	
	@SerializedName(value = "UNTNUM1")
	private String UNTNUM1;
	
	@SerializedName(value = "CUSIDN1")
	private String CUSIDN1;
	
	@Override
	public Map<String, String> coverMap(){
		LinkedHashMap<String, String> result = new LinkedHashMap<String, String>();
        result.put("交易名稱", "健保費代扣繳申請");
        result.put("交易類型", "申請");
		result.put("信用卡號", this.CARDNUM);
		result.put("投保單位", this.UNTNUM1);
		result.put("統一編號", this.CUSIDN1);
		return result;
	}

	public String getCARDNUM() {
		return CARDNUM;
	}

	public void setCARDNUM(String cARDNUM) {
		CARDNUM = cARDNUM;
	}

	public String getUNTNUM1() {
		return UNTNUM1;
	}

	public void setUNTNUM1(String uNTNUM1) {
		UNTNUM1 = uNTNUM1;
	}

	public String getCUSIDN1() {
		return CUSIDN1;
	}

	public void setCUSIDN1(String cUSIDN1) {
		CUSIDN1 = cUSIDN1;
	}

	
}
