
/*
 * Copyright (c) 2017, FSTOP, Inc. All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tw.com.fstop.web;

import java.util.List;

/**
 * Constants definition for web processor.
 * Here we are defining system wide constants, not for specific business use!
 * 此處定義系統層級的常數，不可定義應用系統使用的常數.
 *
 * @since 1.0.1
 */
public class ProcessorConstants
{
    public enum Key
    {
        PROCESS_RESULT("PROCESS_RESULT"),
        
        INDEX_PAGE("INDEX_PAGE"),
        HOME_PAGE("HOME_PAGE"),
        LOGIN_PAGE("LOGIN_PAGE"),
        LOGOUT_PAGE("LOGOUT_PAGE"),     
        
        REQ_PARAM("REQ_PARAM"),     
        REQ_PARAM_DECODED("REQ_PARAM_DECODED"),     
        
        ERROR_PAGE("ERROR_PAGE"),     
        ERROR_PAGE_401("ERROR_PAGE_401"),     
        ERROR_PAGE_404("ERROR_PAGE_404"),     
        ERROR_PAGE_500("ERROR_PAGE_500"),     
        
        TARGET_URL("TARGET_URL"),
        REDIRECT_URL("REDIRECT_URL"),
        
        RESP_MESSAGE("RESP_MESSAGE"),
        RESP_MESSAGES("RESP_MESSAGES"),

        ERROR_MESSAGE("ERROR_MESSAGE"),
        ERROR_MESSAGES("ERROR_MESSAGES"),
        INVALID_VALUE("")
        ;
        
        private final String text;
        
        private Key(String text)
        {
            this.text = text;
        }
        
        public String getText()
        {
            return this.text;
        }
        
        public static Key fromIndex(int i) 
        {
            try 
            {
                return values()[i];
            } 
            catch (Exception e) 
            {
                return INVALID_VALUE;
            }
        }
        
    }  //Key

    
}
