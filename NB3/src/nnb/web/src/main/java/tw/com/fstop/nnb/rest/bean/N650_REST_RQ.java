package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N650_REST_RQ extends BaseRestBean_TW implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2421807388201055546L;

	String CUSIDN;			//統一編號
	String ACN;				//帳號
	String USERDATA_X50;	//暫存空間區
	
	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}

	public String getACN() {
		return ACN;
	}

	public void setACN(String aCN) {
		ACN = aCN;
	}

	public String getUSERDATA_X50() {
		return USERDATA_X50;
	}

	public void setUSERDATA_X50(String uSERDATA_X50) {
		USERDATA_X50 = uSERDATA_X50;
	}
}
