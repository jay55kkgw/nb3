package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N831_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1153673691729831387L;
	
	private String MSGCOD;//回應代碼
	private String DATE;//日期YYYMMDD
	private String TIME;//時間HHMMSS
	private String CMQTIME;//時間HHMMSS
	
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getMSGCOD() {
		return MSGCOD;
	}
	public void setMSGCOD(String mSGCOD) {
		MSGCOD = mSGCOD;
	}
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
}
