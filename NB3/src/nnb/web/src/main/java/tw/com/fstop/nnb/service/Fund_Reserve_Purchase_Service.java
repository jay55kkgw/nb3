package tw.com.fstop.nnb.service;

import java.net.URLEncoder;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import fstop.orm.po.ADMCURRENCY;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.web.util.WebUtil;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.StrUtils;

/**
 * 基金單筆預約申購的Service
 */
@Service
public class Fund_Reserve_Purchase_Service extends Base_Service{
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private Fund_Transfer_Service fund_transfer_service;
	@Autowired
	I18n i18n;
	
	/**
	 * 準備前往基金預約申購手續費頁的資料
	 */
	@SuppressWarnings("unchecked")
	public BaseResult prepareFundReservePurchaseFeeExposeData(Map<String,String> requestParam,Model model){
		BaseResult bs = null;
		try{
			String FDINVTYPE = requestParam.get("FDINVTYPE");
			log.debug(ESAPIUtil.vaildLog("FDINVTYPE={}"+FDINVTYPE));
			model.addAttribute("FDINVTYPE",FDINVTYPE);
			model.addAttribute("RSKATT",FDINVTYPE);
			
			String FUNDLNAME = requestParam.get("FUNDLNAME");
			log.debug(ESAPIUtil.vaildLog("FUNDLNAME={}"+FUNDLNAME));
			model.addAttribute("FUNDLNAME",FUNDLNAME);
			
			String RISK = requestParam.get("RISK");
			log.debug(ESAPIUtil.vaildLog("RISK={}"+RISK));
			model.addAttribute("RISK",RISK);
			
			String STOP = requestParam.get("STOP");
			log.debug(ESAPIUtil.vaildLog("STOP={}"+STOP));
			while(STOP.length() < 3){
				STOP = "0" + STOP;
			}
			log.debug(ESAPIUtil.vaildLog("STOP={}"+STOP));
			requestParam.put("STOP",STOP);
			
			String YIELD = requestParam.get("YIELD");
			log.debug(ESAPIUtil.vaildLog("YIELD={}"+YIELD));
			while(YIELD.length() < 3){
				YIELD = "0" + YIELD;
			}
			log.debug(ESAPIUtil.vaildLog("YIELD={}"+YIELD));
			requestParam.put("YIELD",YIELD);
			
			String GETLTD = requestParam.get("GETLTD");
			log.debug(ESAPIUtil.vaildLog("GETLTD={}"+GETLTD));
			model.addAttribute("GETLTD",GETLTD);
			
			String GETLTD7 = requestParam.get("GETLTD7");
			log.debug(ESAPIUtil.vaildLog("GETLTD7={}"+GETLTD7));
			model.addAttribute("GETLTD7",GETLTD7);
			
			String RISK7 = requestParam.get("RISK7");
			log.debug(ESAPIUtil.vaildLog("RISK7={}"+RISK7));
			model.addAttribute("RISK7",RISK7);
			
			String FDAGREEFLAG = requestParam.get("FDAGREEFLAG");
			log.debug(ESAPIUtil.vaildLog("FDAGREEFLAG={}"+FDAGREEFLAG));
			model.addAttribute("FDAGREEFLAG",FDAGREEFLAG);
			
			String FDNOTICETYPE = requestParam.get("FDNOTICETYPE");
			log.debug(ESAPIUtil.vaildLog("FDNOTICETYPE={}"+FDNOTICETYPE));
			model.addAttribute("FDNOTICETYPE",FDNOTICETYPE);
			
			String FDPUBLICTYPE = requestParam.get("FDPUBLICTYPE");
			log.debug(ESAPIUtil.vaildLog("FDPUBLICTYPE={}"+FDPUBLICTYPE));
			model.addAttribute("FDPUBLICTYPE",FDPUBLICTYPE);
			
			String FEE_TYPE = requestParam.get("FEE_TYPE");
			log.debug(ESAPIUtil.vaildLog("FEE_TYPE={}"+FEE_TYPE));
			model.addAttribute("FEE_TYPE",FEE_TYPE);
			
			String SLSNO = requestParam.get("SLSNO");
			log.debug(ESAPIUtil.vaildLog("SLSNO={}"+SLSNO));
			model.addAttribute("SLSNO",SLSNO);
			
			String FUNDT = requestParam.get("FUNDT");
			log.debug(ESAPIUtil.vaildLog("FUNDT={}"+FUNDT));
			model.addAttribute("FUNDT",FUNDT);
			
			bs = new BaseResult();
			bs = C031Public_REST(requestParam);
			if(bs != null && bs.getResult()){
				Map<String,Object> bsData = (Map<String,Object>)bs.getData();
				
				String SHWD = (String)bsData.get("SHWD");
				model.addAttribute("SHWD",SHWD);
				
				String XFLAG = (String)bsData.get("XFLAG");
				model.addAttribute("XFLAG",XFLAG);
				
				String PEMAIL = (String)bsData.get("PEMAIL");
				model.addAttribute("PEMAIL",PEMAIL);
				
				String REPID = (String)bsData.get("REPID");
				model.addAttribute("REPID",REPID);
				
				String CUSNAME = (String)bsData.get("CUSNAME");
				log.debug("CUSNAME={}",CUSNAME);
				model.addAttribute("CUSNAME",CUSNAME);
				
				String TRADEDATE = (String)bsData.get("TRADEDATE");
				log.debug("TRADEDATE={}",TRADEDATE);
				model.addAttribute("TRADEDATE",TRADEDATE);
				
				String FCA2 = (String)bsData.get("FCA2");
				log.debug("FCA2={}",FCA2);
				model.addAttribute("FCA2",FCA2);
				
				String AMT3 = (String)bsData.get("AMT3");
				log.debug("AMT3={}",AMT3);
				model.addAttribute("AMT3",AMT3);
				
				String AMT5 = (String)bsData.get("AMT5");
				log.debug("AMT5={}",AMT5);
				model.addAttribute("AMT5",AMT5);
				
				String SSLTXNO = (String)bsData.get("SSLTXNO");
				log.debug("SSLTXNO={}",SSLTXNO);
				model.addAttribute("SSLTXNO",SSLTXNO);
				
				String FUDCUR = (String)bsData.get("FUDCUR");
				log.debug("FUDCUR={}",FUDCUR);
				model.addAttribute("FUNCUR",FUDCUR);
				model.addAttribute("CRY1",FUDCUR);
				
				String ACN1 = requestParam.get("ACN1");
				log.debug(ESAPIUtil.vaildLog("ACN1={}"+ACN1));
				String ACN2 = requestParam.get("ACN2");
				log.debug(ESAPIUtil.vaildLog("ACN2={}"+ACN2));
				
				String OUTACN = "";
				String TYPE = "";
				if("NTD".equals(FUDCUR)){
					OUTACN = ACN1;
					TYPE = "01";
				}
				else{
					OUTACN = ACN2;
					TYPE = "02";
				}
				log.debug(ESAPIUtil.vaildLog("OUTACN={}"+OUTACN));
				model.addAttribute("OUTACN",OUTACN);
				log.debug("TYPE={}",TYPE);
				model.addAttribute("TYPE",TYPE);
				
				String TRANSCODE = (String)bsData.get("TRANSCODE");
				log.debug("TRANSCODE={}",TRANSCODE);
				model.addAttribute("TRANSCODE",TRANSCODE);
				
				String COUNTRYTYPE = (String)bsData.get("COUNTRYTYPE");
				log.debug("COUNTRYTYPE={}",COUNTRYTYPE);
				model.addAttribute("COUNTRYTYPE",COUNTRYTYPE);
				
				String HTELPHONE = (String)bsData.get("HTELPHONE");
				log.debug("HTELPHONE={}",HTELPHONE);
				model.addAttribute("HTELPHONE",HTELPHONE);
				
				String OTELPHONE = (String)bsData.get("OTELPHONE");
				log.debug("OTELPHONE={}",OTELPHONE);
				model.addAttribute("OTELPHONE",OTELPHONE);
				model.addAttribute("INTSACN",OTELPHONE);
				
				String BILLSENDMODE = (String)bsData.get("BILLSENDMODE");
				log.debug("BILLSENDMODE={}",BILLSENDMODE);
				model.addAttribute("BILLSENDMODE",BILLSENDMODE);
				
				String FCAFEE = (String)bsData.get("FCAFEE");
				log.debug("FCAFEE={}",FCAFEE);
				model.addAttribute("FCAFEE",FCAFEE);
				
				String PAYDAY1 = (String)bsData.get("PAYDAY1");
				log.debug("PAYDAY1={}",PAYDAY1);
				model.addAttribute("PAYDAY1",PAYDAY1);
				
				String PAYDAY2 = (String)bsData.get("PAYDAY2");
				log.debug("PAYDAY2={}",PAYDAY2);
				model.addAttribute("PAYDAY2",PAYDAY2);
				
				String PAYDAY3 = (String)bsData.get("PAYDAY3");
				log.debug("PAYDAY3={}",PAYDAY3);
				model.addAttribute("PAYDAY3",PAYDAY3);
				
				String PAYDAY4 = (String)bsData.get("PAYDAY4");
				log.debug("PAYDAY4={}",PAYDAY4);
				model.addAttribute("PAYDAY4",PAYDAY4);
				
				String PAYDAY5 = (String)bsData.get("PAYDAY5");
				log.debug("PAYDAY5={}",PAYDAY5);
				model.addAttribute("PAYDAY5",PAYDAY5);
				
				String PAYDAY6 = (String)bsData.get("PAYDAY6");
				log.debug("PAYDAY6={}",PAYDAY6);
				model.addAttribute("PAYDAY6",PAYDAY6);
				
				String PAYDAY7 = (String)bsData.get("PAYDAY7");
				log.debug("PAYDAY7={}",PAYDAY7);
				model.addAttribute("PAYDAY7",PAYDAY7);
				
				String PAYDAY8 = (String)bsData.get("PAYDAY8");
				log.debug("PAYDAY8={}",PAYDAY8);
				model.addAttribute("PAYDAY8",PAYDAY8);
				
				String PAYDAY9 = (String)bsData.get("PAYDAY9");
				log.debug("PAYDAY9={}",PAYDAY9);
				model.addAttribute("PAYDAY9",PAYDAY9);
				
				String MIP = (String)bsData.get("MIP");
				log.debug("MIP={}",MIP);
				model.addAttribute("MIP",MIP);
				
				String BRHCOD = (String)bsData.get("BRHCOD");
				log.debug("BRHCOD={}",BRHCOD);
				model.addAttribute("BRHCOD",BRHCOD);
				
				String CUTTYPE = (String)bsData.get("CUTTYPE");
				log.debug("CUTTYPE={}",CUTTYPE);
				model.addAttribute("CUTTYPE",CUTTYPE);
				
				String DBDATE = (String)bsData.get("DBDATE");
				log.debug("DBDATE={}",DBDATE);
				model.addAttribute("DBDATE",DBDATE);
				
				STOP = (String)bsData.get("STOP");
				log.debug("STOP={}",STOP);
				model.addAttribute("STOP",STOP);
				
				YIELD = (String)bsData.get("YIELD");
				log.debug("YIELD={}",YIELD);
				model.addAttribute("YIELD",YIELD);
				
				String OFLAG = (String)bsData.get("OFLAG");
				log.debug("OFLAG={}", OFLAG);
				model.addAttribute("OFLAG", OFLAG);
				
				Map<String,String> fundFeeExposeTextMap = fund_transfer_service.getFundFeeExposeInfo(bsData,"C031");
				model.addAttribute("fundFeeExposeTextMap",fundFeeExposeTextMap);
				
				String TXID = requestParam.get("TXID");
				log.debug(ESAPIUtil.vaildLog("TXID={}"+TXID));
				model.addAttribute("TXID",TXID);
				
				//IKEY要使用的JSON:DC
			 	String jsondc = URLEncoder.encode(CodeUtil.toJson(requestParam),"UTF-8");
			 	log.debug(ESAPIUtil.vaildLog("jsondc={}"+jsondc));
			 	model.addAttribute("jsondc",jsondc);
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("prepareFundReservePurchaseFeeExposeData error >> {}",e);
		}
		return bs;
	}
	/**
	 * 準備前往基金預約申購確認頁的資料
	 */
	public void prepareFundReservePurchaseConfirmData(Map<String,String> requestParam,Model model){
		String FDINVTYPE = requestParam.get("FDINVTYPE");
		log.debug(ESAPIUtil.vaildLog("FDINVTYPE={}"+FDINVTYPE));
		model.addAttribute("FDINVTYPE",FDINVTYPE);
		
		String CUSNAME = requestParam.get("CUSNAME");
		log.debug(ESAPIUtil.vaildLog("CUSNAME={}"+CUSNAME));
		model.addAttribute("CUSNAME",CUSNAME);
		
		String FDINVTYPEChinese;
		if("1".equals(FDINVTYPE)){
//			FDINVTYPEChinese = "積極型";
			FDINVTYPEChinese = i18n.getMsg("LB.D0945");
		}
		else if("2".equals(FDINVTYPE)){
//			FDINVTYPEChinese = "穩健型";
			FDINVTYPEChinese = i18n.getMsg("LB.X1766");
		}
		else{
//			FDINVTYPEChinese = "保守型";
			FDINVTYPEChinese = i18n.getMsg("LB.X1767");
		}
		log.debug("FDINVTYPEChinese={}",FDINVTYPEChinese);
		model.addAttribute("FDINVTYPEChinese",FDINVTYPEChinese);
		
		String TRANSCODE = requestParam.get("TRANSCODE");
		log.debug(ESAPIUtil.vaildLog("TRANSCODE={}"+TRANSCODE));
		model.addAttribute("TRANSCODE",TRANSCODE);
		
		String COUNTRYTYPE = requestParam.get("COUNTRYTYPE");
		log.debug(ESAPIUtil.vaildLog("COUNTRYTYPE={}"+COUNTRYTYPE));
		model.addAttribute("COUNTRYTYPE",COUNTRYTYPE);
		
		String INTSACN = requestParam.get("INTSACN");
		log.debug(ESAPIUtil.vaildLog("INTSACN={}"+INTSACN));
		model.addAttribute("INTSACN",INTSACN);
		
		String BILLSENDMODE = requestParam.get("BILLSENDMODE");
		log.debug(ESAPIUtil.vaildLog("BILLSENDMODE={}"+BILLSENDMODE));
		model.addAttribute("BILLSENDMODE",BILLSENDMODE);
		
		String SSLTXNO = requestParam.get("SSLTXNO");
		log.debug(ESAPIUtil.vaildLog("SSLTXNO={}"+SSLTXNO));
		model.addAttribute("SSLTXNO",SSLTXNO);
		
		String PAYDAY1 = requestParam.get("PAYDAY1");
		log.debug(ESAPIUtil.vaildLog("PAYDAY1={}"+PAYDAY1));
		model.addAttribute("PAYDAY1",PAYDAY1);
		
		String PAYDAY2 = requestParam.get("PAYDAY2");
		log.debug(ESAPIUtil.vaildLog("PAYDAY2={}"+PAYDAY2));
		model.addAttribute("PAYDAY2",PAYDAY2);
		
		String PAYDAY3 = requestParam.get("PAYDAY3");
		log.debug(ESAPIUtil.vaildLog("PAYDAY3={}"+PAYDAY3));
		model.addAttribute("PAYDAY3",PAYDAY3);
		
		String BRHCOD = requestParam.get("BRHCOD");
		log.debug(ESAPIUtil.vaildLog("BRHCOD={}"+BRHCOD));
		model.addAttribute("BRHCOD",BRHCOD);
		
		String CUTTYPE = requestParam.get("CUTTYPE");
		log.debug(ESAPIUtil.vaildLog("CUTTYPE={}"+CUTTYPE));
		model.addAttribute("CUTTYPE",CUTTYPE);
		
		String CRY1 = requestParam.get("CRY1");
		log.debug(ESAPIUtil.vaildLog("CRY1={}"+CRY1));
		model.addAttribute("CRY1",CRY1);
		
		String DBDATE = requestParam.get("DBDATE");
		log.debug(ESAPIUtil.vaildLog("DBDATE={}"+DBDATE));
		model.addAttribute("DBDATE",DBDATE);
		
		String PAYDAY4 = requestParam.get("PAYDAY4");
		log.debug(ESAPIUtil.vaildLog("PAYDAY4={}"+PAYDAY4));
		model.addAttribute("PAYDAY4",PAYDAY4);
		
		String PAYDAY5 = requestParam.get("PAYDAY5");
		log.debug(ESAPIUtil.vaildLog("PAYDAY5={}"+PAYDAY5));
		model.addAttribute("PAYDAY5",PAYDAY5);
		
		String PAYDAY6 = requestParam.get("PAYDAY6");
		log.debug(ESAPIUtil.vaildLog("PAYDAY6={}"+PAYDAY6));
		model.addAttribute("PAYDAY6",PAYDAY6);
		
		String MIP = requestParam.get("MIP");
		log.debug(ESAPIUtil.vaildLog("MIP={}"+MIP));
		model.addAttribute("MIP",MIP);
		
		String PRO = requestParam.get("PRO");
		log.debug(ESAPIUtil.vaildLog("PRO={}"+PRO));
		model.addAttribute("PRO",PRO);
		
		String RSKATT = requestParam.get("RSKATT");
		log.debug(ESAPIUtil.vaildLog("RSKATT={}"+RSKATT));
		model.addAttribute("RSKATT",RSKATT);
		
		String PAYDAY7 = requestParam.get("PAYDAY7");
		log.debug(ESAPIUtil.vaildLog("PAYDAY7={}"+PAYDAY7));
		model.addAttribute("PAYDAY7",PAYDAY7);
		
		String PAYDAY8 = requestParam.get("PAYDAY8");
		log.debug(ESAPIUtil.vaildLog("PAYDAY8={}"+PAYDAY8));
		model.addAttribute("PAYDAY8",PAYDAY8);
		
		String PAYDAY9 = requestParam.get("PAYDAY9");
		log.debug(ESAPIUtil.vaildLog("PAYDAY9={}"+PAYDAY9));
		model.addAttribute("PAYDAY9",PAYDAY9);
		
		String FUNDLNAME = requestParam.get("FUNDLNAME");
		log.debug(ESAPIUtil.vaildLog("FUNDLNAME={}"+FUNDLNAME));
		model.addAttribute("FUNDLNAME",FUNDLNAME);
		
		String RISK = requestParam.get("RISK");
		log.debug(ESAPIUtil.vaildLog("RISK={}"+RISK));
		model.addAttribute("RISK",RISK);
		model.addAttribute("RRSK",RISK);
		
		String FUNCUR = requestParam.get("FUNCUR");
		log.debug(ESAPIUtil.vaildLog("FUNCUR={}"+FUNCUR));
		model.addAttribute("FUNCUR",FUNCUR);
		
		String OUTACN = requestParam.get("OUTACN");
		log.debug(ESAPIUtil.vaildLog("OUTACN={}"+OUTACN));
		model.addAttribute("OUTACN",OUTACN);
		
		String TYPE;
		if("NTD".equals(FUNCUR)){
			TYPE = "01";
		}
		else{
			TYPE = "02";
		}
		log.debug("TYPE={}",TYPE);
		model.addAttribute("TYPE",TYPE);
		
		String TYPEChinese;
		if("01".equals(TYPE)){
//			TYPEChinese = "台幣";
			TYPEChinese = i18n.getMsg("LB.W1089");
		}
		else{
//			TYPEChinese = "外幣";
			TYPEChinese = i18n.getMsg("LB.W1090");
		}
		log.debug("TYPEChinese={}",TYPEChinese);
		model.addAttribute("TYPEChinese",TYPEChinese);
		
		ADMCURRENCY admCurrency = fund_transfer_service.getCRYData(FUNCUR);
		String locale = LocaleContextHolder.getLocale().toString();
		if(admCurrency != null){
			String ADCCYNAME = "";
			if(locale.equals("zh_CN")) {
				ADCCYNAME = admCurrency.getADCCYCHSNAME();
			}
			else if(locale.equals("en")) {
				ADCCYNAME = admCurrency.getADCCYENGNAME();
			}
			else {
				ADCCYNAME = admCurrency.getADCCYNAME();
			}
			log.debug("ADCCYNAME={}",ADCCYNAME);
			model.addAttribute("ADCCYNAME",ADCCYNAME);
		}
		
		String AMT3 = requestParam.get("AMT3");
		log.debug(ESAPIUtil.vaildLog("AMT3={}"+AMT3));
		model.addAttribute("AMT3",AMT3);
		Integer AMT3Integer = Integer.valueOf(AMT3);
		log.debug("AMT3Integer={}",AMT3Integer);
		model.addAttribute("AMT3Integer",AMT3Integer);
		String AMT3Format = fund_transfer_service.formatNumberString(String.valueOf(AMT3Integer),0);
		log.debug("AMT3Format={}",AMT3Format);
		model.addAttribute("AMT3Format",AMT3Format);

		String FCAFEE = requestParam.get("FCAFEE");
		log.debug(ESAPIUtil.vaildLog("FCAFEE={}"+FCAFEE));
		model.addAttribute("FCAFEE",FCAFEE);
		String FCAFEEFormat = fund_transfer_service.formatNumberString(FCAFEE,3);
		log.debug(ESAPIUtil.vaildLog("FCAFEEFormat={}"+FCAFEEFormat));
		model.addAttribute("FCAFEEFormat",FCAFEEFormat);
		
		String FCA2 = requestParam.get("FCA2");
		log.debug(ESAPIUtil.vaildLog("FCA2={}"+FCA2));
		model.addAttribute("FCA2",FCA2);
		Integer FCA2Integer = Integer.valueOf(FCA2);
		log.debug("FCA2Integer={}",FCA2Integer);
		model.addAttribute("FCA2Integer",FCA2Integer);
		String FCA2Format = fund_transfer_service.formatNumberString(String.valueOf(FCA2Integer),2);
		log.debug("FCA2Format={}",FCA2Format);
		model.addAttribute("FCA2Format",FCA2Format);
		
		String AMT5 = requestParam.get("AMT5");
		log.debug(ESAPIUtil.vaildLog("AMT5={}"+AMT5));
		model.addAttribute("AMT5",AMT5);
		String AMT5NoDot = AMT5.replaceAll("\\.","");
		log.debug("AMT5NoDot={}",AMT5NoDot);
		Integer AMT5Integer = Integer.valueOf(AMT5NoDot);
		log.debug("AMT5Integer={}",AMT5Integer);
		model.addAttribute("AMT5Integer",AMT5Integer);
		String AMT5Format = fund_transfer_service.formatNumberString(String.valueOf(AMT5Integer),2);
		log.debug("AMT5Format={}",AMT5Format);
		model.addAttribute("AMT5Format",AMT5Format);

		// 如果是後收 , 手續費率 0.000,手續費為0.00 ,扣款金額為申購金額
		if ("A".equals(requestParam.get("FEE_TYPE"))) {
			model.addAttribute("FCAFEEFormat", "0.000");
			model.addAttribute("FCA2Format", "0.00");
			model.addAttribute("AMT5Format", AMT3Format);
		}
		
		String HTELPHONE = requestParam.get("HTELPHONE");
		log.debug(ESAPIUtil.vaildLog("HTELPHONE={}"+HTELPHONE));
		model.addAttribute("HTELPHONE",HTELPHONE);
		
		String TRADEDATE = requestParam.get("TRADEDATE");
		log.debug(ESAPIUtil.vaildLog("TRADEDATE={}"+TRADEDATE));
		model.addAttribute("TRADEDATE",TRADEDATE);
		
		String TRADEDATEFormat = TRADEDATE.substring(1,4) + "/" + TRADEDATE.substring(4,6) + "/" + TRADEDATE.substring(6);
		log.debug(ESAPIUtil.vaildLog("TRADEDATEFormat={}"+TRADEDATEFormat));
		model.addAttribute("TRADEDATEFormat",TRADEDATEFormat);
		
		String YIELD = requestParam.get("YIELD");
		log.debug(ESAPIUtil.vaildLog("YIELD={}"+YIELD));
		model.addAttribute("YIELD",YIELD);
		Integer YIELDInteger = Integer.valueOf(YIELD);
		log.debug("YIELDInteger={}",YIELDInteger);
		model.addAttribute("YIELDInteger",YIELDInteger);
		
		String STOP = requestParam.get("STOP");
		log.debug(ESAPIUtil.vaildLog("STOP={}"+STOP));
		model.addAttribute("STOP",STOP);
		Integer STOPInteger = Integer.valueOf(STOP);
		log.debug("STOPInteger={}",STOPInteger);
		model.addAttribute("STOPInteger",STOPInteger);
		
		String GETLTD = requestParam.get("GETLTD");
		log.debug(ESAPIUtil.vaildLog("GETLTD={}"+GETLTD));
		model.addAttribute("GETLTD",GETLTD);
		
		String GETLTD7 = requestParam.get("GETLTD7");
		log.debug(ESAPIUtil.vaildLog("GETLTD7={}"+GETLTD7));
		model.addAttribute("GETLTD7",GETLTD7);
		
		String RISK7 = requestParam.get("RISK7");
		log.debug(ESAPIUtil.vaildLog("RISK7={}"+RISK7));
		model.addAttribute("RISK7",RISK7);
		
		String FDAGREEFLAG = requestParam.get("FDAGREEFLAG");
		log.debug(ESAPIUtil.vaildLog("FDAGREEFLAG={}"+FDAGREEFLAG));
		model.addAttribute("FDAGREEFLAG",FDAGREEFLAG);
		
		String FDNOTICETYPE = requestParam.get("FDNOTICETYPE");
		log.debug(ESAPIUtil.vaildLog("FDNOTICETYPE={}"+FDNOTICETYPE));
		model.addAttribute("FDNOTICETYPE",FDNOTICETYPE);
		
		String FDPUBLICTYPE = requestParam.get("FDPUBLICTYPE");
		log.debug(ESAPIUtil.vaildLog("FDPUBLICTYPE={}"+FDPUBLICTYPE));
		model.addAttribute("FDPUBLICTYPE",FDPUBLICTYPE);
		
		String SHWD = requestParam.get("SHWD");
		log.debug(ESAPIUtil.vaildLog("SHWD= >>" + SHWD));
		model.addAttribute("SHWD",SHWD);
		
		String XFLAG = requestParam.get("XFLAG");
		log.debug(ESAPIUtil.vaildLog("XFLAG= >>" + XFLAG));
		model.addAttribute("XFLAG",XFLAG);

		String NUM = requestParam.get("NUM");
		log.debug(ESAPIUtil.vaildLog("NUM= >>" + NUM));
		model.addAttribute("NUM",NUM);
		
		String PEMAIL = requestParam.get("PEMAIL");
		log.debug(ESAPIUtil.vaildLog("PEMAIL= >>" + PEMAIL));
		model.addAttribute("PEMAIL",PEMAIL);
		
		String REPID = requestParam.get("REPID");
		log.debug(ESAPIUtil.vaildLog("REPID= >>" + REPID));
		model.addAttribute("REPID",REPID);
		
		String FEE_TYPE = requestParam.get("FEE_TYPE");
		log.debug(ESAPIUtil.vaildLog("FEE_TYPE= >>" + FEE_TYPE));
		model.addAttribute("FEE_TYPE",FEE_TYPE);
		
		String SLSNO = requestParam.get("SLSNO");
		log.debug(ESAPIUtil.vaildLog("SLSNO= >>" + SLSNO));
		model.addAttribute("SLSNO", SLSNO);
		
		String FUNDT = requestParam.get("FUNDT");
		log.debug(ESAPIUtil.vaildLog("FUNDT= >>" + FUNDT));
		model.addAttribute("FUNDT", FUNDT);
		
		String OFLAG = requestParam.get("OFLAG");
		log.debug(ESAPIUtil.vaildLog("OFLAG= >>" + OFLAG));
		model.addAttribute("OFLAG", OFLAG);
	}
	/**
	 * 基金預約申購交易
	 */
	@SuppressWarnings("unchecked")
	public BaseResult processFundReservePurchase(Map<String,String> requestParam,Model model){
		BaseResult bs = null;
		try{
			bs = new BaseResult();
			bs = N372_REST(requestParam);
			if(bs != null && bs.getResult()){
				Map<String,Object> bsData = (Map<String,Object>)bs.getData();
				log.debug("bsData={}",bsData);
				model.addAttribute("bsData",bsData);
				
				String FDINVTYPEChinese = requestParam.get("FDINVTYPEChinese");
				log.debug(ESAPIUtil.vaildLog("FDINVTYPEChinese={}"+FDINVTYPEChinese));
				model.addAttribute("FDINVTYPEChinese",FDINVTYPEChinese);
				
				String TYPEChinese = requestParam.get("TYPEChinese");
				log.debug(ESAPIUtil.vaildLog("TYPEChinese={}"+TYPEChinese));
				model.addAttribute("TYPEChinese",TYPEChinese);
				
				BaseResult n922BS = new BaseResult();
				n922BS = fund_transfer_service.getN922Data(requestParam.get("CUSIDN"));
				
				if(n922BS != null && n922BS.getResult()){
					Map<String,Object> n922BSData = (Map<String,Object>)n922BS.getData();
					log.debug("n922BSData={}",n922BSData);
					
//					String NAME = (String)n922BSData.get("NAME");
//					log.debug("NAME={}",NAME);
					
					//20210617修正  若有CUSNAME取CUSNAME 無則為NAME , 不然會是空的 //N922原住民姓名修改由NAME->CUSNAME  
					String NAME = "";
					if(StrUtils.isNotEmpty((String)n922BSData.get("CUSNAME"))) {
						NAME = (String)n922BSData.get("CUSNAME");
					}else {
						NAME = (String)n922BSData.get("NAME");
					}
					
					String hiddenNAME = WebUtil.hideusername1Convert(NAME);
					log.debug("hiddenNAME={}",hiddenNAME);
					model.addAttribute("hiddenNAME",hiddenNAME);
				}
				
				String FUNDLNAME = requestParam.get("FUNDLNAME");
				log.debug(ESAPIUtil.vaildLog("FUNDLNAME={}"+FUNDLNAME));
				model.addAttribute("FUNDLNAME",FUNDLNAME);
				
				String RISK = requestParam.get("RISK");
				log.debug(ESAPIUtil.vaildLog("RISK={}"+RISK));
				model.addAttribute("RISK",RISK);
				
				String CRY1 = (String)bsData.get("CRY1");
				log.debug("CRY1={}",CRY1);
				
				ADMCURRENCY admCurrency = fund_transfer_service.getCRYData(CRY1);
				String locale = LocaleContextHolder.getLocale().toString();
				if(admCurrency != null){
					String ADCCYNAME = "";
					if(locale.equals("zh_CN")) {
						ADCCYNAME = admCurrency.getADCCYCHSNAME();
					}
					else if(locale.equals("en")) {
						ADCCYNAME = admCurrency.getADCCYENGNAME();
					}
					else {
						ADCCYNAME = admCurrency.getADCCYNAME();
					}
					log.debug("ADCCYNAME={}",ADCCYNAME);
					model.addAttribute("ADCCYNAME",ADCCYNAME);
				}
				
				String AMT3 = (String)bsData.get("AMT3");
				log.debug("AMT3={}",AMT3);
				Integer AMT3Integer = Integer.valueOf(AMT3);
				log.debug("AMT3Integer={}",AMT3Integer);
				String AMT3Format = fund_transfer_service.formatNumberString(String.valueOf(AMT3Integer),0);
				log.debug("AMT3Format={}",AMT3Format);
				model.addAttribute("AMT3Format",AMT3Format);
				
				String FCAFEE = (String)bsData.get("FCAFEE");
				log.debug("FCAFEE={}",FCAFEE);
				String FCAFEEFormat = fund_transfer_service.formatNumberString(FCAFEE,3);
				log.debug("FCAFEEFormat={}",FCAFEEFormat);
				model.addAttribute("FCAFEEFormat",FCAFEEFormat);
				
				String FCA2 = (String)bsData.get("FCA2");
				log.debug("FCA2={}",FCA2);
				Integer FCA2Integer = Integer.valueOf(FCA2);
				log.debug("FCA2Integer={}",FCA2Integer);
				String FCA2Format = fund_transfer_service.formatNumberString(String.valueOf(FCA2Integer),2);
				log.debug("FCA2Format={}",FCA2Format);
				model.addAttribute("FCA2Format",FCA2Format);
				
				String AMT5 = (String)bsData.get("AMT5");
				log.debug("AMT5={}",AMT5);
				String AMT5NoDot = AMT5.replaceAll("\\.","");
				log.debug("AMT5NoDot={}",AMT5NoDot);
				String AMT5Format = fund_transfer_service.formatNumberString(AMT5NoDot,2);
				log.debug("AMT5Format={}",AMT5Format);
				model.addAttribute("AMT5Format",AMT5Format);
				
				//如果是後收  , 手續費率 0.000,手續費為0.00 ,扣款金額為申購金額
				if("A".equals(requestParam.get("FEE_TYPE"))) {
					model.addAttribute("FCAFEEFormat","0.000");
					model.addAttribute("FCA2Format","0.00");
					model.addAttribute("AMT5Format",AMT3Format);
				}
				
				String TRADEDATE = (String)bsData.get("TRADEDATE");
				log.debug("TRADEDATE={}",TRADEDATE);
				String TRADEDATEFormat = TRADEDATE.substring(1,4) + "/" + TRADEDATE.substring(4,6) + "/" + TRADEDATE.substring(6);
				log.debug("TRADEDATEFormat={}",TRADEDATEFormat);
				model.addAttribute("TRADEDATEFormat",TRADEDATEFormat);
				
				String YIELD = requestParam.get("YIELD");
				log.debug(ESAPIUtil.vaildLog("YIELD={}"+YIELD));
				Integer YIELDInteger = Integer.valueOf(YIELD);
				log.debug("YIELDInteger={}",YIELDInteger);
				model.addAttribute("YIELDInteger",YIELDInteger);
				
				String STOP = requestParam.get("STOP");
				log.debug(ESAPIUtil.vaildLog("STOP={}"+STOP));
				Integer STOPInteger = Integer.valueOf(STOP);
				log.debug("STOPInteger={}",STOPInteger);
				model.addAttribute("STOPInteger",STOPInteger);
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("processFundReservePurchase error >> {}",e);
		}
		return bs;
	}
}