package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class B023_REST_RS extends BaseRestBean implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5187837892220105535L;
	
	private String TXNDIID;		//TXN Destination Institute ID
	private String SNID;		//System Network Identifier
	private String RECNO;		//讀取筆數
	private String NEXT;		//資料起始位置
	private String TXNSIID;		//TXN Source Institute ID
	private String TXNIDT;		//TXN Initiate Date and Time
	private String SSUP;		//System Supervisory
	private String FILLER_X3;
	private String SYNCCHK;		//Sync. Check Item
	private String ACF;			//Address Control Field
	private String CMRECNUM;	//查詢比數
	private String MSGTYPE;		//Message Type
	private String TIME;		//時間HHMMSS
	private String SYSTRACE;	//System Trace Audit
	private String LOOPINFO;
	private String CUSNAME;		//姓名
	private String DATE;		//日期YYYMMDD
	private String BITMAPCFG;	//Bit Map Configuration
	private String TOTRECNO;	//總筆數
	private String CMQTIME;		//查詢時間
	private String CUSIDN;		//統一編號
	private String RESPCOD;		//Response Code
	private String __OCCURS;
	private String BRHCOD;
	private LinkedList<B023_REST_RSDATA> REC;
	
	public LinkedList<B023_REST_RSDATA> getREC() {
		return REC;
	}
	public void setREC(LinkedList<B023_REST_RSDATA> rEC) {
		REC = rEC;
	}
	public String getTXNDIID() {
		return TXNDIID;
	}
	public void setTXNDIID(String tXNDIID) {
		TXNDIID = tXNDIID;
	}
	public String getSNID() {
		return SNID;
	}
	public void setSNID(String sNID) {
		SNID = sNID;
	}
	public String getRECNO() {
		return RECNO;
	}
	public void setRECNO(String rECNO) {
		RECNO = rECNO;
	}
	public String getNEXT() {
		return NEXT;
	}
	public void setNEXT(String nEXT) {
		NEXT = nEXT;
	}
	public String getTXNSIID() {
		return TXNSIID;
	}
	public void setTXNSIID(String tXNSIID) {
		TXNSIID = tXNSIID;
	}
	public String getTXNIDT() {
		return TXNIDT;
	}
	public void setTXNIDT(String tXNIDT) {
		TXNIDT = tXNIDT;
	}
	public String getSSUP() {
		return SSUP;
	}
	public void setSSUP(String sSUP) {
		SSUP = sSUP;
	}
	public String getFILLER_X3() {
		return FILLER_X3;
	}
	public void setFILLER_X3(String fILLER_X3) {
		FILLER_X3 = fILLER_X3;
	}
	public String getSYNCCHK() {
		return SYNCCHK;
	}
	public void setSYNCCHK(String sYNCCHK) {
		SYNCCHK = sYNCCHK;
	}
	public String getACF() {
		return ACF;
	}
	public void setACF(String aCF) {
		ACF = aCF;
	}
	public String getCMRECNUM() {
		return CMRECNUM;
	}
	public void setCMRECNUM(String cMRECNUM) {
		CMRECNUM = cMRECNUM;
	}
	public String getMSGTYPE() {
		return MSGTYPE;
	}
	public void setMSGTYPE(String mSGTYPE) {
		MSGTYPE = mSGTYPE;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getSYSTRACE() {
		return SYSTRACE;
	}
	public void setSYSTRACE(String sYSTRACE) {
		SYSTRACE = sYSTRACE;
	}
	public String getLOOPINFO() {
		return LOOPINFO;
	}
	public void setLOOPINFO(String lOOPINFO) {
		LOOPINFO = lOOPINFO;
	}
	public String getCUSNAME() {
		return CUSNAME;
	}
	public void setCUSNAME(String cUSNAME) {
		CUSNAME = cUSNAME;
	}
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getBITMAPCFG() {
		return BITMAPCFG;
	}
	public void setBITMAPCFG(String bITMAPCFG) {
		BITMAPCFG = bITMAPCFG;
	}
	public String getTOTRECNO() {
		return TOTRECNO;
	}
	public void setTOTRECNO(String tOTRECNO) {
		TOTRECNO = tOTRECNO;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getRESPCOD() {
		return RESPCOD;
	}
	public void setRESPCOD(String rESPCOD) {
		RESPCOD = rESPCOD;
	}
	public String get__OCCURS() {
		return __OCCURS;
	}
	public void set__OCCURS(String __OCCURS) {
		this.__OCCURS = __OCCURS;
	}
	public String getBRHCOD() {
		return BRHCOD;
	}
	public void setBRHCOD(String bRHCOD) {
		BRHCOD = bRHCOD;
	}
	
}
