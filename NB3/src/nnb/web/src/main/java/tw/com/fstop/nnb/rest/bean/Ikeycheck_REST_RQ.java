package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class Ikeycheck_REST_RQ extends BaseRestBean_FUND implements Serializable{
	private static final long serialVersionUID = 6868861910004819383L;
	private String UID;
	private String jsondc;
	private String pkcs7Sign;

	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	public String getJsondc() {
		return jsondc;
	}
	public void setJsondc(String jsondc) {
		this.jsondc = jsondc;
	}
	public String getPkcs7Sign() {
		return pkcs7Sign;
	}
	public void setPkcs7Sign(String pkcs7Sign) {
		this.pkcs7Sign = pkcs7Sign;
	}
}