package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class NA40_REST_RQ extends BaseRestBean_OLA implements Serializable {

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4266870322362558382L;
	
	private String FILL_X1;//FILL_X1
	private String DATE;//日期YYYMMDD
	private String TIME;//時間HHMMSS
	private String CUSIDN;//統一編號
	private String TYPE;//申請作業類別
	private String CARDNUM;//信用卡卡號
	private String EXPDTA;//到期日西元月年
	private String CHECKNO;//檢查碼CVV
	private String BIRTHDAY;//民國出生年月日
	private String MOBILE;//行動電話
	private String PHONE_H;//住家電話
	private String MAIL;//MAIL
	private String USERNAME;//使用者名稱
	private String HLOGINPIN;//簽入密碼
	private String HTRANSPIN;//交易密碼
	
	private String CCBIRTHDATEYY;
	private String CCBIRTHDATEMM;
	private String CCBIRTHDATEDD;
	private String USERIP;
	private String _CUSIDN;//統一編號
	
	
	public String getFILL_X1() {
		return FILL_X1;
	}
	public void setFILL_X1(String fILL_X1) {
		FILL_X1 = fILL_X1;
	}
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getTYPE() {
		return TYPE;
	}
	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}
	public String getCARDNUM() {
		return CARDNUM;
	}
	public void setCARDNUM(String cARDNUM) {
		CARDNUM = cARDNUM;
	}
	public String getEXPDTA() {
		return EXPDTA;
	}
	public void setEXPDTA(String eXPDTA) {
		EXPDTA = eXPDTA;
	}
	public String getCHECKNO() {
		return CHECKNO;
	}
	public void setCHECKNO(String cHECKNO) {
		CHECKNO = cHECKNO;
	}
	public String getBIRTHDAY() {
		return BIRTHDAY;
	}
	public void setBIRTHDAY(String bIRTHDAY) {
		BIRTHDAY = bIRTHDAY;
	}
	public String getMOBILE() {
		return MOBILE;
	}
	public void setMOBILE(String mOBILE) {
		MOBILE = mOBILE;
	}
	public String getPHONE_H() {
		return PHONE_H;
	}
	public void setPHONE_H(String pHONE_H) {
		PHONE_H = pHONE_H;
	}
	public String getMAIL() {
		return MAIL;
	}
	public void setMAIL(String mAIL) {
		MAIL = mAIL;
	}
	public String getUSERNAME() {
		return USERNAME;
	}
	public void setUSERNAME(String uSERNAME) {
		USERNAME = uSERNAME;
	}
	public String getHLOGINPIN() {
		return HLOGINPIN;
	}
	public void setHLOGINPIN(String hLOGINPIN) {
		HLOGINPIN = hLOGINPIN;
	}
	public String getHTRANSPIN() {
		return HTRANSPIN;
	}
	public void setHTRANSPIN(String hTRANSPIN) {
		HTRANSPIN = hTRANSPIN;
	}
	public String getCCBIRTHDATEYY() {
		return CCBIRTHDATEYY;
	}
	public void setCCBIRTHDATEYY(String cCBIRTHDATEYY) {
		CCBIRTHDATEYY = cCBIRTHDATEYY;
	}
	public String getCCBIRTHDATEMM() {
		return CCBIRTHDATEMM;
	}
	public void setCCBIRTHDATEMM(String cCBIRTHDATEMM) {
		CCBIRTHDATEMM = cCBIRTHDATEMM;
	}
	public String getCCBIRTHDATEDD() {
		return CCBIRTHDATEDD;
	}
	public void setCCBIRTHDATEDD(String cCBIRTHDATEDD) {
		CCBIRTHDATEDD = cCBIRTHDATEDD;
	}
}
