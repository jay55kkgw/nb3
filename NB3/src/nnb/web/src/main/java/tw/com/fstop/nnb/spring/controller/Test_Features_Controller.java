package tw.com.fstop.nnb.spring.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.util.ESAPIUtil;

@Controller
@RequestMapping("/test")
public class Test_Features_Controller {
	
	@RequestMapping("/test")
	public String testIndex(HttpServletRequest request, HttpServletResponse response) {
		String targate = "test_fetures/test_index";
		return targate;
	}
	
	@RequestMapping("/test_calculator")
	public String testCalculator(HttpServletRequest request, HttpServletResponse response) {
		String targate = "test_fetures/test_calculator";
		return targate;
	}
	
	@RequestMapping("/test_back")
	public String back(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		String targate = "test_fetures/test_back";
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		BaseResult bs = null;
		bs = new BaseResult();
		bs.addData("email", okMap.get("email"));
		bs.addData("password", okMap.get("password"));
		model.addAttribute("bs",bs);
		return targate;
	}
	
	@RequestMapping("/test_back_ajax")
	public @ResponseBody BaseResult backAjax(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		BaseResult bs = null;
		bs = new BaseResult();
		bs.addData("email", okMap.get("email"));
		bs.addData("password", okMap.get("password"));
		model.addAttribute("bs",bs);
		return bs;
	}
	

	@RequestMapping("/test_calculat")
	public @ResponseBody BaseResult add(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {

		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		BaseResult bs = null;
		bs = new BaseResult();
		int result = 0;
		switch(okMap.get("type")) {
		case "add":
			result = Integer.valueOf(okMap.get("number1")) + Integer.valueOf(okMap.get("number2"));
			break;
		case "cut":
			result = Integer.valueOf(okMap.get("number1")) - Integer.valueOf(okMap.get("number2"));
			break;
		}
		bs.addData("result", result);
		model.addAttribute("bs",bs);
		return bs;
	}
}
