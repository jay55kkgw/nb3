package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N215D_1_REST_RS extends BaseRestBean implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1550897467949688346L;
	
	private String SEQ;//序號
	private String CMRECNUM;//bean給的筆數 
	private String COUNT; //電文給的

	LinkedList<N215D_1_REST_RSDATA> REC;

	
	public String getSEQ() {
		return SEQ;
	}

	public void setSEQ(String sEQ) {
		SEQ = sEQ;
	}

	public String getCMRECNUM() {
		return CMRECNUM;
	}

	public void setCMRECNUM(String cMRECNUM) {
		CMRECNUM = cMRECNUM;
	}

	public String getCOUNT() {
		return COUNT;
	}

	public void setCOUNT(String cOUNT) {
		COUNT = cOUNT;
	}

	public LinkedList<N215D_1_REST_RSDATA> getREC() {
		return REC;
	}

	public void setREC(LinkedList<N215D_1_REST_RSDATA> rEC) {
		REC = rEC;
	}


	
	
	
	
}
