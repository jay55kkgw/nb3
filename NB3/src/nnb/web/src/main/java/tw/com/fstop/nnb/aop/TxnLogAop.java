package tw.com.fstop.nnb.aop;

import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.joda.time.DateTime;
import org.joda.time.Seconds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fstop.orm.po.TXNLOG;
import tw.com.fstop.common.ResultCode;
import tw.com.fstop.nnb.custom.annotation.ISTXNLOG;
import tw.com.fstop.tbb.nnb.dao.SysLogDao;
import tw.com.fstop.tbb.nnb.dao.TxnLogDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.WebUtil;

@Aspect
@Component
public class TxnLogAop {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private HttpServletRequest req;
	@Autowired
	ArrayList<String> txnLogList ;
	
	@Autowired TxnLogDao txnlogdao;
	@Autowired SysLogDao syslogdao;
	
	
	@Around("within(tw.com.fstop.web.util.RESTUtil) && execution(* send(..))")
	public Object doAround(ProceedingJoinPoint pjp) {
		log.info("Before doAround..");
		Object result = null;
		Boolean isTxnlLog = Boolean.FALSE;
		String api = "",pstimediff="";
		Map data = null;
		DateTime d1 = null;
		DateTime d2 = null;
		DateTime d3 = null;
		DateTime d4 = null;
		try {
			data = new HashMap<>();
			
			
			log.trace("txnLogList>>{}",txnLogList);
			log.trace("pjp.getArgs()>>{}",pjp.getArgs());
			api = (String) pjp.getArgs()[2];
			
			log.trace("api>>{}",api);
			log.trace("api>>{}",api.lastIndexOf("/"));
			api = api.substring(api.lastIndexOf("/") , api.length()).replace("/", "");
			log.trace("api2>>{}",api);
			
			Object obj = pjp.getArgs()[0];
			data.putAll(CodeUtil.objectCovert(Map.class, obj));
			
//			ADOPID 有帶值就以此為主
			if(data.get("ADOPID") !=null && StrUtil.isNotEmpty(data.get("ADOPID").toString()) ) {
				api = data.get("ADOPID").toString();
			}
//			特例 ，日後應該會拿掉 以ADOPID為主
			api = apiNameRetouch(api ,data);
			isTxnlLog = txnLogList.contains(api);
			
			if(data.get("isTxnlLog") !=null && StrUtil.isNotEmpty(data.get("isTxnlLog").toString()) ) {
				if(data.get("isTxnlLog").equals("N")) {
					isTxnlLog = false;
				}
			}
			
			log.debug("data>>{}",  CodeUtil.toJsonCustom(data));
			log.debug("isTxnlLog>>{}",isTxnlLog);
//			TODO 在這之前抓USERIP、CUSIDN USER  及輸入參數
			
		} catch (Throwable e) {
			log.error(e.toString());
		}
//		要切2個try catch 原因:使前面的錯誤 不會影響pjp.proceed
		try {
			
			d1 = new DateTime();
			log.trace("d1>>{}" ,d1.toString("yyyy-MM-dd HH:mm:ss:SSS") );
			result = pjp.proceed();
			log.info("After doAround..");
			d2 = new DateTime();
			pstimediff = DateUtil.getDiffTimeMills(d1, d2);
			log.warn("API>>{} , pstimediff>>{}",api,pstimediff);
			log.trace("d2>>{}" ,d2.toString("yyyy-MM-dd HH:mm:ss:SSS") );
//			TODO 在這之後抓回傳參數，或是根據特殊電文抓特定參數 ex:N070
			Map<String,String> resultMap = null;
			log.debug("result>>{}",result);
//				null 表示 有 Exection 直接設定為FE0002
			if(result !=null) {
				resultMap = CodeUtil.objectCovert(Map.class, result);
			}else {
				resultMap = new HashMap<>();
				resultMap.put("msgCode", ResultCode.SYS_ERROR_TEL);
			}
			
			log.info("API>>{} , msgCode>>{}",api,resultMap.get("msgCode"));
			if (isTxnlLog) {
				d3 = new DateTime();
				writeTxnLog(data, api, resultMap.get("msgCode") , pstimediff);
				d4 = new DateTime();
				log.info("to Save diff>>{}",DateUtil.getDiffTimeMills(d3, d4));
			}
			
			
			
		} catch (Throwable e) {
			log.error(e.toString());
		}
		log.debug("result>>{}",CodeUtil.toJson(result));
		log.info("doAround End...");
		
		return result;
	}
	
	
	/**
	 * 
	 * @param data :上行資料
	 * @param adopid :api 名稱
	 * @param adexcode :回應代碼
	 * @param pstimediff:回應時間
	 */
	public void writeTxnLog(Map<String,String> data ,String adopid ,String adexcode ,String pstimediff  ) {
		String aduserid = "";
		String aduserip = "";
		String adguid = "";
		TXNLOG po =null;
		try {
			po = new TXNLOG();
			aduserid = (String) req.getSession().getAttribute(SessionUtil.CUSIDN);
			aduserid = StrUtil.isNotEmpty(aduserid)?aduserid:"";
			if(StrUtil.isNotEmpty(aduserid)) {
				aduserid = new String( Base64.getDecoder().decode(aduserid) ,"utf-8");
			}
			adguid = (String) req.getSession().getAttribute(SessionUtil.ADGUID) ;
			adguid = StrUtil.isNotEmpty(adguid)?adguid:"";
//			因後台查詢成功條件是"";
			adexcode = "0".equals(adexcode) ?"":adexcode;
			
//			TODO 測試用
//			aduserid="A123456814";
			
			aduserip = WebUtil.getIpAddr(req);
			log.trace(ESAPIUtil.vaildLog("aduserid >> " + aduserid )); 
			log.trace(ESAPIUtil.vaildLog("adguid >> " + adguid )); 
//			po = CodeUtil.objectCovert(TXNLOG.class, data);
			po = txnlogFileMaping(data, adopid, po);
			
			po.setADTXNO(UUID.randomUUID().toString());
			if(StrUtil.isEmpty(po.getADUSERID())) {
				po.setADUSERID(aduserid);
			}
			po.setADOPID(adopid);
			po.setADUSERIP(aduserip);
			po.setADGUID(adguid);
			po.setADEXCODE(adexcode);
//			po.setADCONTENT(CodeUtil.toJson(data));
			po.setADCONTENT(CodeUtil.toJsonCustom(data));
			po.setLASTDATE(new DateTime().toString("yyyyMMdd"));
			po.setLASTTIME(new DateTime().toString("HHmmss"));
//			TODO 等欄位確定開之後，在打開
			po.setPSTIMEDIFF(pstimediff);
//			if(!adopid.startsWith("F00")) {
//				po = loadTransferParam(data, adopid, po);
//			}
			po = loadSPParam(data, adopid, po);
			log.debug("TXNLOG>>{}",po.toString());
			txnlogdao.save(po);
		} catch (Exception e) {
			log.error(e.toString());
		}
		
		
	}
	
	/**
	 * 取得部分交易電文參數
	 * @param data
	 * @param adopid
	 * @param po
	 * @return
	 */
	public TXNLOG loadTransferParam(Map<String,String> data ,String adopid ,TXNLOG po) {
		String dpagacnoStr = "" ,adcurrency = "" ,adtxacno ,logintype;
		String adsvbh = "" , adagreef ="" ,adreqtype ="" ,adtxamt ="" ,fgtxdate ,fgtxway;
		try {
			
			dpagacnoStr = data.get("DPAGACNO");
			
			if(StrUtil.isNotEmpty(dpagacnoStr)) {
				Map<String,String> tmp =  CodeUtil.fromJson(dpagacnoStr, Map.class);
//				data.putAll(tmp);
				adagreef = StrUtil.isNotEmpty(tmp.get("AGREE")) ?tmp.get("AGREE") :"" ;
				adsvbh = StrUtil.isNotEmpty(tmp.get("BNKCOD")) ?tmp.get("BNKCOD") :"ADSVBH" ;
				
				po.setADAGREEF(adagreef);
				po.setADSVBH(adsvbh);
				
			}
			fgtxdate = StrUtil.isNotEmpty(data.get("FGTXDATE"))?data.get("FGTXDATE"):"";
			if("2".equals(fgtxdate) || "3".equals(fgtxdate) ) {
				adreqtype = "S";
			}
			
			//TODO 要求每個轉帳交易都要加  (ADCURRENCY)
			adcurrency = StrUtil.isNotEmpty(data.get("ADCURRENCY"))?data.get("ADCURRENCY"):"";
			adtxamt = StrUtil.isNotEmpty(data.get("AMOUNT"))?data.get("AMOUNT"):"";
			fgtxway = StrUtil.isNotEmpty(data.get("FGTXWAY"))?data.get("FGTXWAY"):"";
			adtxacno = StrUtil.isNotEmpty(data.get("ACN"))?data.get("ACN"):"";
			logintype = StrUtil.isNotEmpty(data.get("LOGINTYPE"))?data.get("LOGINTYPE"):"";
			
			po.setADCURRENCY(adcurrency);
			po.setADREQTYPE(adreqtype);
			po.setADTXAMT(adtxamt);
			po.setFGTXWAY(fgtxway);
			po.setADTXACNO(adtxacno);
			po.setLOGINTYPE(logintype);
		} catch (Exception e) {
			log.error(e.toString());
		}
		return po;
	}
	
	/**
	 * 針對轉入銀行代碼 、約定非約定
	 * @param data
	 * @param adopid
	 * @param po
	 * @return
	 */
	public TXNLOG loadSPParam(Map<String,String> data ,String adopid ,TXNLOG po) {
		String dpagacnoStr = "" ,adcurrency = "" ,adtxacno ,logintype;
		String adsvbh = "" , adagreef ="" ,adreqtype ="" ,adtxamt ="" ,fgtxdate ,fgtxway;
		try {
			
			dpagacnoStr = data.get("DPAGACNO");
			
			if(StrUtil.isNotEmpty(dpagacnoStr)) {
				Map<String,String> tmp =  CodeUtil.fromJson(dpagacnoStr, Map.class);
				adagreef = StrUtil.isNotEmpty(tmp.get("AGREE")) ?tmp.get("AGREE") :"" ;
				adsvbh = StrUtil.isNotEmpty(tmp.get("BNKCOD")) ?tmp.get("BNKCOD") :"ADSVBH" ;
				po.setADAGREEF(adagreef);
				po.setADSVBH(adsvbh);
			}
			fgtxdate = StrUtil.isNotEmpty(data.get("FGTXDATE"))?data.get("FGTXDATE"):"";
			if("2".equals(fgtxdate) || "3".equals(fgtxdate) ) {
//				外幣預約是B
				if(!adopid.startsWith("F00")) {
					adreqtype = "S";
					po.setADREQTYPE(adreqtype);
				}
			}
			
		} catch (Exception e) {
			log.error(e.toString());
		}
		return po;
	}
	
	
	
	/**
	 * 把送出的資料
	 * @param data
	 * @param adopid
	 * @param po
	 * @return
	 */
	public TXNLOG txnlogFileMaping(Map<String,String> data ,String adopid ,TXNLOG po) {
		Map<String ,String> map = null;
		try {
			try {
				map = SpringBeanFactory.getBean(adopid.toLowerCase()+"-tx");
			} catch (Exception e) {
				log.error("cant not find Bean user d4...{}",adopid.toLowerCase()+"-tx");
				map = SpringBeanFactory.getBean("d4-tx");
			}
			Map<String ,String> map2 = new HashMap<String,String>();
			for(String key :map.keySet()) {
				map2.put(key, data.get(map.get(key)));
			}
			log.debug("map2>>{}",map2);
			data.putAll(map2);
			log.debug("data>>{}", CodeUtil.toJsonCustom(data));
			po = CodeUtil.objectCovert(TXNLOG.class, data);
			log.debug("po>>{}",po.toString());
		} catch (Exception e) {
			log.error(e.toString());
		}
		return po;
	}
	
	
	
	/**
	 * 把API名稱調成跟NNB一致以利後台查詢
	 * 
	 * @param api
	 * @return
	 */
	public String apiNameRetouch(String api , Map<String,Object> data) {
		
		if(StrUtil.isEmpty(api)) {
			return api;
		}
		
		
		try {
			switch (api) {
			case "C016_Public":
				api = "C016";
//			data.put("ADOPID", "C016");
				break;
//			case "N070":
//				if(null != data.get("DPAGACNO") ) {
//					
//					Map<String,String> map = CodeUtil.fromJson(data.get("DPAGACNO").toString(), Map.class);
////					表示跨行 api 
//					if(!"050".equals(map.get("BNKCOD").toString())) {
//						api = "N071";
//					}
//				}
//				break;

			default:
				break;
			}
			//F001T >> F001
//			if(api.startsWith("F00") && api.length()>4) {
//				api = api.substring(0,4);
//			}
		} catch (Exception e) {
			log.error("{}",e);
		}
		
		
		return api;
		
	}
}
