package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

/**
 * 
 * 功能說明 :一般網銀外幣匯出匯款受款人約定檔擷取 回應
 *
 */
public class F031_REST_RS extends BaseRestBean implements Serializable 
{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4371001595190973911L;

	
	private LinkedList<F031_REST_RSDATA> REC;


	public LinkedList<F031_REST_RSDATA> getREC()
	{
		return REC;
	}


	public void setREC(LinkedList<F031_REST_RSDATA> rEC)
	{
		REC = rEC;
	}

}
