package tw.com.fstop.nnb.spring.controller;

import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import tw.com.fstop.nnb.service.Excel_Download_Service;
import tw.com.fstop.nnb.service.OldExcel_Download_Service;
import tw.com.fstop.nnb.service.OldTxt_Download_Service;
import tw.com.fstop.nnb.service.Txt_Download_Service;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DownloadUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;

/*
 * 下載檔案的控制器
 */
@SessionAttributes({SessionUtil.DOWNLOAD_DATALISTMAP_DATA})
@Controller
public class DownloadController{
	private Logger log = LoggerFactory.getLogger(this.getClass().getName());
	@Autowired
	private OldExcel_Download_Service oldexcel_download_service;
	@Autowired
	private Excel_Download_Service excel_download_service;
	@Autowired
	private Txt_Download_Service txt_download_service;
	@Autowired
	private OldTxt_Download_Service oldtxt_download_service;
	
	/*
	 * 交易結果頁下載檔案
	 */
	@RequestMapping("/download")
	@SuppressWarnings("unchecked")
	public ModelAndView download(@RequestParam(required=false) Map<String,Object> parameterMap,Model model){
		log.debug("IN download");
		log.debug(ESAPIUtil.vaildLog("parameterMap={}" + parameterMap));
		
		//先註解 因換行符號會被ESAPI擋掉
		//Avoid Potential O Reflected XSS All Clients
		Map<String,Object> okParameterMap = ESAPIUtil.validMap(parameterMap);
		//Map<String,Object> okParameterMap = parameterMap;
		log.debug(ESAPIUtil.vaildLog("okParameterMap={}" + okParameterMap));
		
		String downloadType = (String)okParameterMap.get("downloadType");
		log.debug(ESAPIUtil.vaildLog("downloadType={}"  + downloadType));
		
		List<Map<String,Object>> dataListMap = (List<Map<String,Object>>)SessionUtil.getAttribute(model,SessionUtil.DOWNLOAD_DATALISTMAP_DATA,null);
		log.debug("dataListMap={}",dataListMap);
		okParameterMap.put("dataListMap",dataListMap);
		
		//舊版本EXCEL下載
		if("OLDEXCEL".equals(downloadType)){
			return new ModelAndView(oldexcel_download_service,okParameterMap);
		}
		//EXCEL下載
		else if("EXCEL".equals(downloadType)){
			return new ModelAndView(excel_download_service,okParameterMap);
		}
		//TXT下載
		else if("TXT".equals(downloadType)){
			return new ModelAndView(txt_download_service,okParameterMap);
		}
		//舊TXT下載
		else{
			return new ModelAndView(oldtxt_download_service,okParameterMap);
		}
	}
	/*
	 * 交易結果頁AJAX下載檔案
	 */
	@RequestMapping("/ajaxDownload")
	@SuppressWarnings("unchecked")
	public ModelAndView ajaxDownload(@RequestBody(required=false) String serializeString,Model model){
		log.debug("IN ajaxDownload");
		log.debug(ESAPIUtil.vaildLog("serializeString={}" + serializeString));
		try{
			Map<String,String> formMap = DownloadUtil.serializeStringToMap(serializeString);
			log.debug(ESAPIUtil.vaildLog("formMap={}" + CodeUtil.toJson(formMap)));
			
			for(Entry<String,String> entry : formMap.entrySet()){
				// Potential O Reflected XSS All Clients
				String formKey = entry.getKey();
				String formValue = URLDecoder.decode(entry.getValue(),"UTF-8");
				formValue = ESAPIUtil.validInput(formValue,"GeneralString",true);
				
				formMap.put(formKey,formValue);
			}
			
			Map<String,Object> parameterMap = new HashMap<String,Object>();
			parameterMap.putAll(formMap);
			log.debug(ESAPIUtil.vaildLog("parameterMap={}" + parameterMap));
			
			String downloadType = (String)parameterMap.get("downloadType");
			log.debug(ESAPIUtil.vaildLog("downloadType={}" + downloadType));
			
			List<Map<String,Object>> dataListMap = (List<Map<String,Object>>)SessionUtil.getAttribute(model,SessionUtil.DOWNLOAD_DATALISTMAP_DATA,null);
			log.debug("dataListMap={}",dataListMap);
			parameterMap.put("dataListMap",dataListMap);
			
			// Potential O Reflected XSS All Clients
			parameterMap = ESAPIUtil.validMap(parameterMap);
			
			//舊版本EXCEL下載
			if("OLDEXCEL".equals(downloadType)){
				return new ModelAndView(oldexcel_download_service,parameterMap);
			}
			//EXCEL下載
			else if("EXCEL".equals(downloadType)){
				return new ModelAndView(excel_download_service,parameterMap);
			}
			//TXT下載
			else if("TXT".equals(downloadType)){
				return new ModelAndView(txt_download_service,parameterMap);
			}
			//舊TXT下載
			else{
				return new ModelAndView(oldtxt_download_service,parameterMap);
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("ajaxDownload error >> {}",e);
			return null;
		}
	}
	/*
	 * 不經過交易結果頁，直接下載檔案
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/directDownload")
	public ModelAndView directDownload(HttpServletRequest request,Model model){
		log.debug("IN directDownload");
		
		Map<String,Object> parameterMap = (Map<String,Object>)request.getAttribute("parameterMap");
		log.debug(ESAPIUtil.vaildLog("parameterMap={}" + CodeUtil.toJson(parameterMap)));
		
		//先註解 因換行符號會被ESAPI擋掉
		Map<String,Object> okParameterMap = ESAPIUtil.validMap(parameterMap);
		//Map<String,Object> okParameterMap = parameterMap;
		log.debug(ESAPIUtil.vaildLog("okParameterMap={}" + CodeUtil.toJson(okParameterMap)));
		
		String downloadType = (String)okParameterMap.get("downloadType");
		log.debug(ESAPIUtil.vaildLog("downloadType={}"+ downloadType));
		
		Map<String,Object> modelMap = model.asMap();
		log.debug("modelMap={}",modelMap);
		
		List<Map<String,String>> dataListMap = (List<Map<String,String>>)modelMap.get(SessionUtil.DOWNLOAD_DATALISTMAP_DATA);
		log.debug("dataListMap={}",dataListMap);
		okParameterMap.put("dataListMap",dataListMap);
		
		//舊版本EXCEL下載
		if("OLDEXCEL".equals(downloadType)){
			return new ModelAndView(oldexcel_download_service,okParameterMap);
		}
		//EXCEL下載
		else if("EXCEL".equals(downloadType)){
			return new ModelAndView(excel_download_service,okParameterMap);
		}
		//TXT下載
		else if("TXT".equals(downloadType)){
			return new ModelAndView(txt_download_service,okParameterMap);
		}
		//舊TXT下載
		else{
			return new ModelAndView(oldtxt_download_service,okParameterMap);
		}
	}
	/*
	 * 不經過交易結果頁，直接AJAX下載檔案
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/ajaxDirectDownload")
	public ModelAndView ajaxDirectDownload(HttpServletRequest request,Model model){
		log.debug("IN ajaxDirectDownload");
		
		Map<String,Object> parameterMap = (Map<String,Object>)request.getAttribute("parameterMap");
		log.debug(ESAPIUtil.vaildLog("parameterMap={}" + CodeUtil.toJson(parameterMap)));
		
		Map<String,Object> okParameterMap = ESAPIUtil.validMap(parameterMap);
		log.debug(ESAPIUtil.vaildLog("okParameterMap={}" + CodeUtil.toJson(okParameterMap)));
		
		String downloadType = (String)okParameterMap.get("downloadType");
		log.debug(ESAPIUtil.vaildLog("downloadType={}" + downloadType));
		
		Map<String,Object> modelMap = model.asMap();
		log.debug("modelMap={}",modelMap);
		
		List<Map<String,String>> dataListMap = (List<Map<String,String>>)modelMap.get(SessionUtil.DOWNLOAD_DATALISTMAP_DATA);
		log.debug("dataListMap={}",dataListMap);
		okParameterMap.put("dataListMap",dataListMap);
		
		//舊版本EXCEL下載
		if("OLDEXCEL".equals(downloadType)){
			return new ModelAndView(oldexcel_download_service,okParameterMap);
		}
		//EXCEL下載
		else if("EXCEL".equals(downloadType)){
			return new ModelAndView(excel_download_service,okParameterMap);
		}
		//TXT下載
		else if("TXT".equals(downloadType)){
			return new ModelAndView(txt_download_service,okParameterMap);
		}
		//舊TXT下載
		else{
			return new ModelAndView(oldtxt_download_service,okParameterMap);
		}
	}
}