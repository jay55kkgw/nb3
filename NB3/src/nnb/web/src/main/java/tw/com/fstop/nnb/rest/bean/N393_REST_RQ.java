package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N393_REST_RQ extends BaseRestBean_FUND implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -9099157386045323756L;
	
	private String DATE;
	private String TIME;
	private String CUSIDN;
	private String PINNEW;
	private String TRANSCODE;
	private String CDNO;
	private String TRADEDATE;
	private String INTRANSCODE;
	private String UNIT;
	private String FCA1;
	private String AMT3;
	private String FCA2;
	private String AMT5;
	private String OUTACN;
	private String BILLSENDMODE;
	private String SSLTXNO;
	private String FUNDAMT;
	private String CRY;
	
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getPINNEW() {
		return PINNEW;
	}
	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}
	public String getTRANSCODE() {
		return TRANSCODE;
	}
	public void setTRANSCODE(String tRANSCODE) {
		TRANSCODE = tRANSCODE;
	}
	public String getTRADEDATE() {
		return TRADEDATE;
	}
	public void setTRADEDATE(String tRADEDATE) {
		TRADEDATE = tRADEDATE;
	}
	public String getINTRANSCODE() {
		return INTRANSCODE;
	}
	public void setINTRANSCODE(String iNTRANSCODE) {
		INTRANSCODE = iNTRANSCODE;
	}
	public String getUNIT() {
		return UNIT;
	}
	public void setUNIT(String uNIT) {
		UNIT = uNIT;
	}
	public String getFCA1() {
		return FCA1;
	}
	public void setFCA1(String fCA1) {
		FCA1 = fCA1;
	}
	public String getAMT3() {
		return AMT3;
	}
	public void setAMT3(String aMT3) {
		AMT3 = aMT3;
	}
	public String getFCA2() {
		return FCA2;
	}
	public void setFCA2(String fCA2) {
		FCA2 = fCA2;
	}
	public String getAMT5() {
		return AMT5;
	}
	public void setAMT5(String aMT5) {
		AMT5 = aMT5;
	}
	public String getOUTACN() {
		return OUTACN;
	}
	public void setOUTACN(String oUTACN) {
		OUTACN = oUTACN;
	}
	public String getBILLSENDMODE() {
		return BILLSENDMODE;
	}
	public void setBILLSENDMODE(String bILLSENDMODE) {
		BILLSENDMODE = bILLSENDMODE;
	}
	public String getSSLTXNO() {
		return SSLTXNO;
	}
	public void setSSLTXNO(String sSLTXNO) {
		SSLTXNO = sSLTXNO;
	}
	public String getFUNDAMT() {
		return FUNDAMT;
	}
	public void setFUNDAMT(String fUNDAMT) {
		FUNDAMT = fUNDAMT;
	}
	public String getCRY() {
		return CRY;
	}
	public void setCRY(String cRY) {
		CRY = cRY;
	}
	public String getCDNO() {
		return CDNO;
	}
	public void setCDNO(String cDNO) {
		CDNO = cDNO;
	}
}
