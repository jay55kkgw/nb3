package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class C116_REST_RSDATA implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7700854256044240974L;

	private String CDNO;// 信託號碼

	private String PAYTAG;// 扣款標的

	private String FUNDAMT;// 信託金額

	private String FUNDCUR;// 贖回信託金額幣別

	private String PAYAMT;// 扣款金額

	private String PAYCUR;// 扣款幣別

	private String STOPLOSS;// 停損

	private String STOPPROF;// 停利

	private String AC202;// 信託業務別

	private String MIP;// 定期不定額註記

	public String getPAYTAG()
	{
		return PAYTAG;
	}

	public void setPAYTAG(String pAYTAG)
	{
		PAYTAG = pAYTAG;
	}

	public String getFUNDAMT()
	{
		return FUNDAMT;
	}

	public void setFUNDAMT(String fUNDAMT)
	{
		FUNDAMT = fUNDAMT;
	}

	public String getFUNDCUR()
	{
		return FUNDCUR;
	}

	public void setFUNDCUR(String fUNDCUR)
	{
		FUNDCUR = fUNDCUR;
	}

	public String getPAYAMT()
	{
		return PAYAMT;
	}

	public void setPAYAMT(String pAYAMT)
	{
		PAYAMT = pAYAMT;
	}

	public String getPAYCUR()
	{
		return PAYCUR;
	}

	public void setPAYCUR(String pAYCUR)
	{
		PAYCUR = pAYCUR;
	}

	public String getSTOPLOSS()
	{
		return STOPLOSS;
	}

	public void setSTOPLOSS(String sTOPLOSS)
	{
		STOPLOSS = sTOPLOSS;
	}

	public String getSTOPPROF()
	{
		return STOPPROF;
	}

	public void setSTOPPROF(String sTOPPROF)
	{
		STOPPROF = sTOPPROF;
	}

	public String getAC202()
	{
		return AC202;
	}

	public void setAC202(String aC202)
	{
		AC202 = aC202;
	}

	public String getMIP()
	{
		return MIP;
	}

	public void setMIP(String mIP)
	{
		MIP = mIP;
	}

	public String getCDNO() {
		return CDNO;
	}

	public void setCDNO(String cDNO) {
		CDNO = cDNO;
	}

}
