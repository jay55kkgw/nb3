package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class N075_IDGATE_DATA implements Serializable{
	
		/**
	 * 
	 */
	private static final long serialVersionUID = 3872305638453120311L;

		//0：非約定，1：約定
		@SerializedName(value = "FLAG")
		private String FLAG;
		
		//轉出帳號
		@SerializedName(value = "OUTACN")
		private String OUTACN;
		
		//代收截止日
		@SerializedName(value = "LIMDAT")
		private String LIMDAT;
		
		//電號
		@SerializedName(value = "ELENUM")
		private String ELENUM;
		
		//檢查號
		@SerializedName(value = "CHKNUM")
		private String CHKNUM;
		
		//繳款金額
		@SerializedName(value = "AMOUNT")
		private String AMOUNT;
		
		//轉出帳號帳上餘額 
		@SerializedName(value = "O_TOTBAL")
		private String O_TOTBAL;
		
		//轉出帳號可用餘額
		@SerializedName(value = "O_AVLBAL")
		private String O_AVLBAL;
		
		
		public String getOUTACN() {
			return OUTACN;
		}
		public void setOUTACN(String oUTACN) {
			OUTACN = oUTACN;
		}
		public String getLIMDAT() {
			return LIMDAT;
		}
		public void setLIMDAT(String lIMDAT) {
			LIMDAT = lIMDAT;
		}
		public String getELENUM() {
			return ELENUM;
		}
		public void setELENUM(String eLENUM) {
			ELENUM = eLENUM;
		}
		public String getCHKNUM() {
			return CHKNUM;
		}
		public void setCHKNUM(String cHKNUM) {
			CHKNUM = cHKNUM;
		}
		public String getAMOUNT() {
			return AMOUNT;
		}
		public void setAMOUNT(String aMOUNT) {
			AMOUNT = aMOUNT;
		}
		public String getO_TOTBAL() {
			return O_TOTBAL;
		}
		public void setO_TOTBAL(String o_TOTBAL) {
			O_TOTBAL = o_TOTBAL;
		}
		public String getO_AVLBAL() {
			return O_AVLBAL;
		}
		public void setO_AVLBAL(String o_AVLBAL) {
			O_AVLBAL = o_AVLBAL;
		}

		
}
