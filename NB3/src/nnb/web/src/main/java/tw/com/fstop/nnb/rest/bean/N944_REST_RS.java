package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N944_REST_RS extends BaseRestBean implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 5848443342851952663L;
	
	private String MSGCOD;
    
	public String getMSGCOD() {
		return MSGCOD;
	}

	public void setMSGCOD(String mSGCOD) {
		MSGCOD = mSGCOD;
	}

}