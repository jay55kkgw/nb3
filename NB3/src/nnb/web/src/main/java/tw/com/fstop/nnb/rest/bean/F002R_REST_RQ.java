package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * 
 * @author Ian
 *
 */
public class F002R_REST_RQ extends BaseRestBean_FX implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8782668314950097519L;

	private String CUSIDN;// 客戶統編

	private String CUSTACC;//// 轉出帳號

	private String CURAMT;// PAYREMIT =1 轉出金額 (議價金額) ;//轉入金額

	private String FGINACNO;// 1 約定帳號, 2 非約定帳號

	private String FGTRDATE; // 0 即時

	private String PAYCCY;// 轉出幣別

	private String PAYREMIT;// 金額類別

	private String REMITCY;// 轉入幣別

	public String getFGINACNO()
	{
		return FGINACNO;
	}

	public void setFGINACNO(String fGINACNO)
	{
		FGINACNO = fGINACNO;
	}

	public String getFGTRDATE()
	{
		return FGTRDATE;
	}

	public void setFGTRDATE(String fGTRDATE)
	{
		FGTRDATE = fGTRDATE;
	}

	public String getCUSTACC()
	{
		return CUSTACC;
	}

	public void setCUSTACC(String cUSTACC)
	{
		CUSTACC = cUSTACC;
	}

	public String getPAYCCY()
	{
		return PAYCCY;
	}

	public void setPAYCCY(String pAYCCY)
	{
		PAYCCY = pAYCCY;
	}

	public String getREMITCY()
	{
		return REMITCY;
	}

	public void setREMITCY(String rEMITCY)
	{
		REMITCY = rEMITCY;
	}

	public String getPAYREMIT()
	{
		return PAYREMIT;
	}

	public void setPAYREMIT(String pAYREMIT)
	{
		PAYREMIT = pAYREMIT;
	}

	public String getCURAMT()
	{
		return CURAMT;
	}

	public void setCURAMT(String cURAMT)
	{
		CURAMT = cURAMT;
	}

	public String getCUSIDN()
	{
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN)
	{
		CUSIDN = cUSIDN;
	}

}
