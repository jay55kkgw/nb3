package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.SpringBeanFactory;

public class N366_1_REST_RS extends BaseRestBean implements Serializable {
	
	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	private static final long serialVersionUID = -2137326113925896396L;

	private String MSGCOD;			// 訊息代號
	private String CUSIDN;			// 身份證號
	private String ACN;				// 欲結清帳號
	private String INACN;			// 結清款轉入本行帳號
	private String O_TOTBAL;		// 結清帳號帳戶餘額
	private String O_TOTBALS;		// 結清帳號帳戶餘額正負號
	private String O_AVLBAL;		// 結清帳號可用餘額
	private String O_AVLBALS;		// 結清帳號可用餘額正負號
	private String I_TOTBAL;		// 轉入帳號帳戶餘額
	private String I_TOTBALS;		// 轉入帳號帳戶餘額正負號
	private String I_AVLBAL;		// 轉入帳號可用餘額
	private String I_AVLBALS;		// 轉入帳號可用餘額正負號
	
	
	/**
	 * 過濾正號
	 * 
	 * @param symbol
	 * @return
	 */
	private String filterPlus(String symbol)
	{
		String result = "-".equals(symbol) ?  "-" : "";
		return result;
	}
	
	/**
	 * 結合負號和餘額
	 * 
	 * @param sign
	 * @param balance
	 * @return
	 */
	private String combineSignBalance(String sign, String balance)
	{
		String result = filterPlus(sign) + balance;
		return result;
	}
	
	public String getMSGCOD() {
		return MSGCOD;
	}
	
	public void setMSGCOD(String mSGCOD) {
		MSGCOD = mSGCOD;
	}

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}

	public String getACN() {
		return ACN;
	}

	public void setACN(String aCN) {
		ACN = aCN;
	}
	
	public String getINACN() {
		return INACN;
	}
	
	/**
	 * 若結清帳號餘額為零時，N366下行電文INACN為空，頁面顯示無
	 * @return
	 */
	public String getINACN_F() 
	{
		String result = INACN;
		try {
			I18n i18n = SpringBeanFactory.getBean("i18n");
			if(INACN == null || "".equals(INACN))
			{
//				無
				result = i18n.getMsg("LB.D1070_2");
			}
		}
		catch(Exception e) {
			log.error("getINACN_F error >> {}",e);
		}
		return result;
	}

	public void setINACN(String iNACN) {
		INACN = iNACN;
	}

	public String getO_TOTBAL() {
		return O_TOTBAL;
	}
	
	/**
	 * 電文回應值有小數點 ex:0.00
	 * @return
	 */
	public String getO_TOTBAL_F() 
	{
		String result = O_TOTBAL;
		try
		{
			// get scale
			BigDecimal s = new BigDecimal(O_TOTBAL);
			int scale = s.scale();
			// 加千分位及小數點
			String bal = NumericUtil.fmtAmount(s.toPlainString(), scale);
			// 過濾正號或加負號
			result = combineSignBalance(O_TOTBALS, bal);
		}
		catch (Exception e)
		{
			log.error("getO_TOTBAL_F error. param >> {}", O_TOTBAL, e);
		}
		return result;
	}
	
	public void setO_TOTBAL(String o_TOTBAL) {
		O_TOTBAL = o_TOTBAL;
	}

	public String getO_TOTBALS() {
		return O_TOTBALS;
	}

	public void setO_TOTBALS(String o_TOTBALS) {
		O_TOTBALS = o_TOTBALS;
	}

	public String getO_AVLBAL() {
		return O_AVLBAL;
	}
	
	/**
	 * 電文回應值有小數點 ex:0.00
	 * @return
	 */
	public String getO_AVLBAL_F() 
	{
		String result = O_AVLBAL;
		try
		{
			// get scale
			BigDecimal s = new BigDecimal(O_AVLBAL);
			int scale = s.scale();
			// 加千分位及小數點
			String bal = NumericUtil.fmtAmount(s.toPlainString(), scale);
			// 過濾正號或加負號
			result = combineSignBalance(O_AVLBALS, bal);
		}
		catch (Exception e)
		{
			log.error("getO_AVLBAL_F error. param >> {}", O_AVLBAL, e);
		}
		return result;
	}

	public void setO_AVLBAL(String o_AVLBAL) {
		O_AVLBAL = o_AVLBAL;
	}

	public String getO_AVLBALS() {
		return O_AVLBALS;
	}

	public void setO_AVLBALS(String o_AVLBALS) {
		O_AVLBALS = o_AVLBALS;
	}

	public String getI_TOTBAL() {
		return I_TOTBAL;
	}
	
	// 0030819555100 to 308,195,551.00
	public String getI_TOTBAL_F() 
	{
		String result = I_TOTBAL;
		try
		{
			BigDecimal b = new BigDecimal(new BigInteger(I_TOTBAL), 2);
			String bal = NumericUtil.fmtAmount(b.toPlainString(), 2);
			result = combineSignBalance(I_TOTBALS, bal);
		}
		catch (Exception e)
		{
			log.error("getI_TOTBAL_F error. param >> {}", I_TOTBAL, e);
		}
		return result;
	}

	public void setI_TOTBAL(String i_TOTBAL) {
		I_TOTBAL = i_TOTBAL;
	}

	public String getI_TOTBALS() {
		return I_TOTBALS;
	}

	public void setI_TOTBALS(String i_TOTBALS) {
		I_TOTBALS = i_TOTBALS;
	}

	public String getI_AVLBAL() {
		return I_AVLBAL;
	}
	
	/**
	 * 0030818555100 to 308,185,551.00
	 * @return
	 */
	public String getI_AVLBAL_F() 
	{
		String result = I_AVLBAL;
		try
		{
			BigDecimal b = new BigDecimal(new BigInteger(I_AVLBAL), 2);
			String bal = NumericUtil.fmtAmount(b.toPlainString(), 2);
			result = combineSignBalance(I_AVLBALS, bal);
		}
		catch (Exception e)
		{
			log.error("getI_AVLBAL_F error. param >> {}", I_AVLBAL, e);
		}
		return result;
	}

	public void setI_AVLBAL(String i_AVLBAL) {
		I_AVLBAL = i_AVLBAL;
	}

	public String getI_AVLBALS() {
		return I_AVLBALS;
	}

	public void setI_AVLBALS(String i_AVLBALS) {
		I_AVLBALS = i_AVLBALS;
	}
	
}