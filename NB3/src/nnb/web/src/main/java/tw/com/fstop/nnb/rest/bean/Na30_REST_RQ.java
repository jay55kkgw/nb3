package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class Na30_REST_RQ extends BaseRestBean_OLA implements Serializable{
/**
	 * 
	 */
	private static final long serialVersionUID = -6456195198001505816L;
		private String CCBIRTHDATEYY;
		private String CCBIRTHDATEMM;
		private String CCBIRTHDATEDD;
		private String CUSIDN;
		private String TYPE;
		private String CARDNUM;
		private String MOBILE;
		private String MAIL;
		private String icSeq;
		public String getCCBIRTHDATEYY() {
			return CCBIRTHDATEYY;
		}
		public void setCCBIRTHDATEYY(String cCBIRTHDATEYY) {
			CCBIRTHDATEYY = cCBIRTHDATEYY;
		}
		public String getCCBIRTHDATEMM() {
			return CCBIRTHDATEMM;
		}
		public void setCCBIRTHDATEMM(String cCBIRTHDATEMM) {
			CCBIRTHDATEMM = cCBIRTHDATEMM;
		}
		public String getCCBIRTHDATEDD() {
			return CCBIRTHDATEDD;
		}
		public void setCCBIRTHDATEDD(String cCBIRTHDATEDD) {
			CCBIRTHDATEDD = cCBIRTHDATEDD;
		}
		public String getCUSIDN() {
			return CUSIDN;
		}
		public void setCUSIDN(String cUSIDN) {
			CUSIDN = cUSIDN;
		}
		public String getTYPE() {
			return TYPE;
		}
		public void setTYPE(String tYPE) {
			TYPE = tYPE;
		}
		public String getCARDNUM() {
			return CARDNUM;
		}
		public void setCARDNUM(String cARDNUM) {
			CARDNUM = cARDNUM;
		}
		public String getMOBILE() {
			return MOBILE;
		}
		public void setMOBILE(String mOBILE) {
			MOBILE = mOBILE;
		}
		public String getMAIL() {
			return MAIL;
		}
		public void setMAIL(String mAIL) {
			MAIL = mAIL;
		}
		public String getIcSeq() {
			return icSeq;
		}
		public void setIcSeq(String icSeq) {
			this.icSeq = icSeq;
		}
		
		
		
		
}
