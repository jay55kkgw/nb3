package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N8330_REST_RQ extends BaseRestBean_PAY implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -925802414319513030L;

	private String TSFACN;
	private String CUSIDN;
	private String TYPE;
	private String TRANSEQ;
	
	public String getTSFACN() {
		return TSFACN;
	}
	public void setTSFACN(String tSFACN) {
		TSFACN = tSFACN;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getTYPE() {
		return TYPE;
	}
	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}
	public String getTRANSEQ() {
		return TRANSEQ;
	}
	public void setTRANSEQ(String tRANSEQ) {
		TRANSEQ = tRANSEQ;
	}
	
	
	
}
