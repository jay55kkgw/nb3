package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 查詢參加單位
 * 
 * @author Vincenthuang
 *
 */
public class MtpBankList_REST_RS extends BaseRestBean_QR_RS implements Serializable {

	private List<AccountInfo> BankInfo;	//明細最外層
	private String TotalCounts;			//參與金融機構總計數量
	public String getTotalCounts() {
		return TotalCounts;
	}
	public void setTotalCounts(String totalCounts) {
		TotalCounts = totalCounts;
	}

	public List<AccountInfo> getBankInfo() {
		return BankInfo;
	}
	public void setBankInfo(List<AccountInfo> bankInfo) {
		BankInfo = bankInfo;
	}

	public class AccountInfo{
		private String Name;	//金融機構名稱
		private String Code;	//金融機構代碼
		public String getName() {
			return Name;
		}
		public void setName(String name) {
			Name = name;
		}
		public String getCode() {
			return Code;
		}
		public void setCode(String code) {
			Code = code;
		}
		
	}	
}
