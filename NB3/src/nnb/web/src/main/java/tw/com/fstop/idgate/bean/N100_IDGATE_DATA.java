package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class N100_IDGATE_DATA implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -762951082705400880L;

	@SerializedName(value = "POSTCOD2")
	private String POSTCOD2;
	@SerializedName(value = "ARACOD")
	private String ARACOD;
	@SerializedName(value = "MOBTEL")
	private String MOBTEL;
	@SerializedName(value = "CTTADR")
	private String CTTADR;
	@SerializedName(value = "TELNUM2")
	private String TELNUM2;
	
	@SerializedName(value = "ARACOD2")
	private String ARACOD2;
	@SerializedName(value = "TELNUM")
	private String TELNUM;
	public String getPOSTCOD2() {
		return POSTCOD2;
	}
	public String getARACOD() {
		return ARACOD;
	}
	public String getMOBTEL() {
		return MOBTEL;
	}
	public String getCTTADR() {
		return CTTADR;
	}
	public String getTELNUM2() {
		return TELNUM2;
	}
	public String getARACOD2() {
		return ARACOD2;
	}
	public String getTELNUM() {
		return TELNUM;
	}
	public void setPOSTCOD2(String pOSTCOD2) {
		POSTCOD2 = pOSTCOD2;
	}
	public void setARACOD(String aRACOD) {
		ARACOD = aRACOD;
	}
	public void setMOBTEL(String mOBTEL) {
		MOBTEL = mOBTEL;
	}
	public void setCTTADR(String cTTADR) {
		CTTADR = cTTADR;
	}
	public void setTELNUM2(String tELNUM2) {
		TELNUM2 = tELNUM2;
	}
	public void setARACOD2(String aRACOD2) {
		ARACOD2 = aRACOD2;
	}
	public void setTELNUM(String tELNUM) {
		TELNUM = tELNUM;
	}

	
}
