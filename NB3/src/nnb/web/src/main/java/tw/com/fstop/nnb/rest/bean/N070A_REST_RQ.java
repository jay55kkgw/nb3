package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N070A_REST_RQ extends BaseRestBean_PAY implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5536422775071501179L;
	
	private String FILL_X1;		// FILL_X1
	private String HEADER;		// HEADER
	private String DATE;		// 日期YYYMMDD
	private String TIME;		// 時間HHMMSS
	private String SYNC;		// Sync.Check Item
	private String PPSYNC;		// P.P.Key Sync.Check Item
	private String PINKEY;		// 網路銀行密碼
	private String CERTACN;		// 憑證帳號
	private String FLAG;		// 0：非約定，1：約定
	private String BNKRA;		// 行庫別
	private String XMLCA;		// CA識別碼
	private String XMLCN;		// 憑證CN
	private String PPSYNCN;		// P.P.Key Sync.Check Item 16
	private String PINNEW;		// 網路銀行密碼（新）
	private String CUSIDN;		// 統一編號
	private String ACN;			// 帳號
	private String TSFACN;		// 轉入帳號
	private String AMOUNT;		// 轉帳金額
	private String TYPE;		// 繳款類別
	private String INPCST;		// 附言
	private String MAC;			// MAC
	private String TRANSEQ;		// 交易序號
	private String ISSUER;		// 晶片卡發卡行庫
	private String ACNNO;		// 晶片卡帳號
	private String ICDTTM;		// 晶片卡日期時間
	private String ICSEQ;		// 晶片卡序號
	private String ICMEMO;		// 晶片卡備註欄
	private String TAC_Length;	// TAC DATA Length
	private String TAC;			// TAC DATA
	private String TAC_120space;// TAC DATA Space
	private String TRMID;		// 端末設備查核碼
	
	// 電文不需要但ms_tw需要的欄位
	private String FGTXWAY;		// 交易機制
	private String CMDATE;		// 預約日期
	private String iSeqNo;		// iSeqNo
	private String FGTXDATE;	// 即時或預約
	private String TRANSPASSUPDATE;// 即時或預約
	private String pkcs7Sign;	// IKEY
	private String jsondc;		// IKEY

	
	// 舊BEAN.writeLog需要的欄位
	private String UID;			// 身分證字號
	private String ADOPID;		// 電文代號
	private String CMTRMEMO;	// 交易備註
	private String DPMYEMAIL;	// 通訊錄
	private String CMTRMAIL;	// 通訊錄
	private String CMMAILMEMO;	// 摘要內容
	
	//IDGATE
	private String sessionID;
	private String idgateID;
	private String txnID;
	
	public String getFILL_X1() {
		return FILL_X1;
	}
	public void setFILL_X1(String fILL_X1) {
		FILL_X1 = fILL_X1;
	}
	public String getHEADER() {
		return HEADER;
	}
	public void setHEADER(String hEADER) {
		HEADER = hEADER;
	}
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getSYNC() {
		return SYNC;
	}
	public void setSYNC(String sYNC) {
		SYNC = sYNC;
	}
	public String getPPSYNC() {
		return PPSYNC;
	}
	public void setPPSYNC(String pPSYNC) {
		PPSYNC = pPSYNC;
	}
	public String getPINKEY() {
		return PINKEY;
	}
	public void setPINKEY(String pINKEY) {
		PINKEY = pINKEY;
	}
	public String getCERTACN() {
		return CERTACN;
	}
	public void setCERTACN(String cERTACN) {
		CERTACN = cERTACN;
	}
	public String getFLAG() {
		return FLAG;
	}
	public void setFLAG(String fLAG) {
		FLAG = fLAG;
	}
	public String getBNKRA() {
		return BNKRA;
	}
	public void setBNKRA(String bNKRA) {
		BNKRA = bNKRA;
	}
	public String getXMLCA() {
		return XMLCA;
	}
	public void setXMLCA(String xMLCA) {
		XMLCA = xMLCA;
	}
	public String getXMLCN() {
		return XMLCN;
	}
	public void setXMLCN(String xMLCN) {
		XMLCN = xMLCN;
	}
	public String getPPSYNCN() {
		return PPSYNCN;
	}
	public void setPPSYNCN(String pPSYNCN) {
		PPSYNCN = pPSYNCN;
	}
	public String getPINNEW() {
		return PINNEW;
	}
	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getTSFACN() {
		return TSFACN;
	}
	public void setTSFACN(String tSFACN) {
		TSFACN = tSFACN;
	}
	public String getAMOUNT() {
		return AMOUNT;
	}
	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}
	public String getTYPE() {
		return TYPE;
	}
	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}
	public String getINPCST() {
		return INPCST;
	}
	public void setINPCST(String iNPCST) {
		INPCST = iNPCST;
	}
	public String getMAC() {
		return MAC;
	}
	public void setMAC(String mAC) {
		MAC = mAC;
	}
	public String getTRANSEQ() {
		return TRANSEQ;
	}
	public void setTRANSEQ(String tRANSEQ) {
		TRANSEQ = tRANSEQ;
	}
	public String getISSUER() {
		return ISSUER;
	}
	public void setISSUER(String iSSUER) {
		ISSUER = iSSUER;
	}
	public String getACNNO() {
		return ACNNO;
	}
	public void setACNNO(String aCNNO) {
		ACNNO = aCNNO;
	}
	public String getICDTTM() {
		return ICDTTM;
	}
	public void setICDTTM(String iCDTTM) {
		ICDTTM = iCDTTM;
	}
	public String getICSEQ() {
		return ICSEQ;
	}
	public void setICSEQ(String iCSEQ) {
		ICSEQ = iCSEQ;
	}
	public String getICMEMO() {
		return ICMEMO;
	}
	public void setICMEMO(String iCMEMO) {
		ICMEMO = iCMEMO;
	}
	public String getTAC_Length() {
		return TAC_Length;
	}
	public void setTAC_Length(String tAC_Length) {
		TAC_Length = tAC_Length;
	}
	public String getTAC() {
		return TAC;
	}
	public void setTAC(String tAC) {
		TAC = tAC;
	}
	public String getTAC_120space() {
		return TAC_120space;
	}
	public void setTAC_120space(String tAC_120space) {
		TAC_120space = tAC_120space;
	}
	public String getTRMID() {
		return TRMID;
	}
	public void setTRMID(String tRMID) {
		TRMID = tRMID;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getCMDATE() {
		return CMDATE;
	}
	public void setCMDATE(String cMDATE) {
		CMDATE = cMDATE;
	}
	public String getiSeqNo() {
		return iSeqNo;
	}
	public void setiSeqNo(String iSeqNo) {
		this.iSeqNo = iSeqNo;
	}
	public String getFGTXDATE() {
		return FGTXDATE;
	}
	public void setFGTXDATE(String fGTXDATE) {
		FGTXDATE = fGTXDATE;
	}
	public String getTRANSPASSUPDATE() {
		return TRANSPASSUPDATE;
	}
	public void setTRANSPASSUPDATE(String tRANSPASSUPDATE) {
		TRANSPASSUPDATE = tRANSPASSUPDATE;
	}
	public String getPkcs7Sign() {
		return pkcs7Sign;
	}
	public void setPkcs7Sign(String pkcs7Sign) {
		this.pkcs7Sign = pkcs7Sign;
	}
	public String getJsondc() {
		return jsondc;
	}
	public void setJsondc(String jsondc) {
		this.jsondc = jsondc;
	}
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getCMTRMEMO() {
		return CMTRMEMO;
	}
	public void setCMTRMEMO(String cMTRMEMO) {
		CMTRMEMO = cMTRMEMO;
	}
	public String getDPMYEMAIL() {
		return DPMYEMAIL;
	}
	public void setDPMYEMAIL(String dPMYEMAIL) {
		DPMYEMAIL = dPMYEMAIL;
	}
	public String getCMTRMAIL() {
		return CMTRMAIL;
	}
	public void setCMTRMAIL(String cMTRMAIL) {
		CMTRMAIL = cMTRMAIL;
	}
	public String getCMMAILMEMO() {
		return CMMAILMEMO;
	}
	public void setCMMAILMEMO(String cMMAILMEMO) {
		CMMAILMEMO = cMMAILMEMO;
	}
	public String getSessionID() {
		return sessionID;
	}
	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}
	public String getIdgateID() {
		return idgateID;
	}
	public void setIdgateID(String idgateID) {
		this.idgateID = idgateID;
	}
	public String getTxnID() {
		return txnID;
	}
	public void setTxnID(String txnID) {
		this.txnID = txnID;
	}
	
	
}
