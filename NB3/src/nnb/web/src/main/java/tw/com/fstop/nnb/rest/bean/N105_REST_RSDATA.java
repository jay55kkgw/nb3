package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N105_REST_RSDATA implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9203884248088650864L;

	private String ACN; // 帳號

	private String SEQ; // 分號

	private String OWNERNM; // 房屋所有人姓名

	private String DASH;

	private String SINQ; // 繳息所屬年月(起日)

	private String DINQ; // 繳息所屬年月(迄日)

	private String DATFSLN; // 貸款起日

	private String DDT; // 貸款迄日

	private String BAL_E; // 餘額(_E表特例)

	private String ADDR; // 房屋座落

	private String OWNDATE; // 房屋所有權取得日

	private String AMTORLN; // 初貸金額

	private String TOTAMT_E;// 繳息金額(_E表特例，不跟連少的FORMAT裡沖突)

	public String getACN()
	{
		return ACN;
	}

	public void setACN(String aCN)
	{
		ACN = aCN;
	}

	public String getSEQ()
	{
		return SEQ;
	}

	public void setSEQ(String sEQ)
	{
		SEQ = sEQ;
	}

	public String getOWNERNM()
	{
		return OWNERNM;
	}

	public void setOWNERNM(String oWNERNM)
	{
		OWNERNM = oWNERNM;
	}

	public String getDASH()
	{
		return DASH;
	}

	public void setDASH(String dASH)
	{
		DASH = dASH;
	}

	public String getSINQ()
	{
		return SINQ;
	}

	public void setSINQ(String sINQ)
	{
		SINQ = sINQ;
	}

	public String getDINQ()
	{
		return DINQ;
	}

	public void setDINQ(String dINQ)
	{
		DINQ = dINQ;
	}

	public String getDATFSLN()
	{
		return DATFSLN;
	}

	public void setDATFSLN(String dATFSLN)
	{
		DATFSLN = dATFSLN;
	}

	public String getDDT()
	{
		return DDT;
	}

	public void setDDT(String dDT)
	{
		DDT = dDT;
	}

	public String getBAL_E()
	{
		return BAL_E;
	}

	public void setBAL_E(String bAL_E)
	{
		BAL_E = bAL_E;
	}

	public String getADDR()
	{
		return ADDR;
	}

	public void setADDR(String aDDR)
	{
		ADDR = aDDR;
	}

	public String getOWNDATE()
	{
		return OWNDATE;
	}

	public void setOWNDATE(String oWNDATE)
	{
		OWNDATE = oWNDATE;
	}

	public String getAMTORLN()
	{
		return AMTORLN;
	}

	public void setAMTORLN(String aMTORLN)
	{
		AMTORLN = aMTORLN;
	}

	public String getTOTAMT_E()
	{
		return TOTAMT_E;
	}

	public void setTOTAMT_E(String tOTAMT_E)
	{
		TOTAMT_E = tOTAMT_E;
	}

}
