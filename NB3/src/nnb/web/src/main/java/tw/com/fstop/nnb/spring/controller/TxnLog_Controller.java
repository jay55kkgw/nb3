package tw.com.fstop.nnb.spring.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.service.TxnLog_Service;
import tw.com.fstop.nnb.service.TxnTwRecord_Service;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;

@RestController
@RequestMapping(value = "/MB/TXNLOG")
public class  TxnLog_Controller{
	Logger log = LoggerFactory.getLogger(this.getClass());
	@Autowired
	TxnLog_Service txnLog_Service;
	@Autowired
	I18n i18n;
	/**
	 * 查詢客戶交易紀錄
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/query" , method = {RequestMethod.POST} , produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody BaseResult query(HttpServletRequest request, HttpServletResponse response, @RequestBody Map<String,String> reqParam, Model model) {
		log.info(ESAPIUtil.vaildLog("reqParam>>{}"+CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		try {
			//解決 Reflected XSS All Clients
			Map<String,String> okMap = ESAPIUtil.validStrMap(reqParam);
			bs = txnLog_Service.query(okMap);
		}catch(Throwable e) {
			log.error("Throwable>>{}",e.toString());
		}
		return bs;
	}
	/**
	 * 新增客戶交易紀錄
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/add" , method = {RequestMethod.POST} , produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody BaseResult add(HttpServletRequest request, HttpServletResponse response, @RequestBody Map<String,String> reqParam, Model model) {
		log.info(ESAPIUtil.vaildLog("reqParam>>{}"+CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		try {
			//解決 Reflected XSS All Clients
			Map<String,String> okMap = ESAPIUtil.validStrMap(reqParam);
			bs = txnLog_Service.add(okMap);
		}catch(Throwable e) {
			log.error("Throwable>>{}",e.toString());
		}
		return bs;
	}

}
