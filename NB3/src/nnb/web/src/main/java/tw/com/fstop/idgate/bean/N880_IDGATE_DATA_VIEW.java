package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;


public class N880_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2100182387501509714L;

	@SerializedName(value = "ACN_SHOW")
	private String ACN_SHOW;
	
	@SerializedName(value = "COUNT")
	private String COUNT;
	
	@Override
	public Map<String, String> coverMap(){
		LinkedHashMap<String, String> result = new LinkedHashMap<String, String>();
		result.put("交易名稱", "約定轉入帳號取消");
		result.put("交易類型", "-");
		result.put("取消轉入帳號", this.ACN_SHOW);
		if(Integer.parseInt(this.COUNT)>0) {
			result.put("已預約資料", this.COUNT +" 筆");
			result.put("提醒您", "若取消轉入帳號將併同取消此轉入帳號之預約交易");
		}
		return result;
	}

	public String getACN_SHOW() {
		return ACN_SHOW;
	}


	public void setACN_SHOW(String aCN_SHOW) {
		ACN_SHOW = aCN_SHOW;
	}


	public String getCOUNT() {
		return COUNT;
	}

	public void setCOUNT(String cOUNT) {
		COUNT = cOUNT;
	}
}
