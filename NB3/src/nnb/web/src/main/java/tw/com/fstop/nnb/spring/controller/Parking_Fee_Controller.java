package tw.com.fstop.nnb.spring.controller;

import java.net.URLEncoder;
import java.util.Base64;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.service.Parking_Fee_Service;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.WebUtil;

@SessionAttributes({ SessionUtil.CUSIDN, SessionUtil.PD,SessionUtil.DOWNLOAD_DATALISTMAP_DATA,
	SessionUtil.PRINT_DATALISTMAP_DATA,SessionUtil.RESULT_LOCALE_DATA, SessionUtil.CONFIRM_LOCALE_DATA, 
	SessionUtil.RESULT_LOCALE_DATA,SessionUtil.CUSIDN, SessionUtil.TRANSFER_CONFIRM_TOKEN, SessionUtil.TRANSFER_RESULT_TOKEN, 
	SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, SessionUtil.TRANSFER_DATA, SessionUtil.STEP1_LOCALE_DATA, SessionUtil.DPMYEMAIL })

@Controller
@RequestMapping(value = "/PARKING/FEE")
public class Parking_Fee_Controller {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	Parking_Fee_Service parking_fee_service;
	/**
	 * 停車費(index)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/parking_withholding")
	public String new_retire_terms(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/parking_fee/parking_withholding";
		BaseResult bsP018 = null;
//		清除切換語系時暫存的資料
		SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
		SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");

		try {
			String getAcn = reqParam.get("Acn");
			log.trace(ESAPIUtil.vaildLog("GET ACN >>>{}"+getAcn));
 			String address[] = {"台北市","新北市","高雄市","桃園市","台中市","台南市","新竹市"};
			bsP018 = parking_fee_service.P018_REST();
			List<Map<String,String>> sortList = new LinkedList<Map<String,String>>();
			for(int i = 0;i < address.length;i++) {
				for(Map<String,String> callRow : (List<Map<String,String>>)((Map<String,Object>)bsP018.getData()).get("REC")) {
					if(callRow.get("ZoneName").equals(address[i])) {
						sortList.add(callRow);
					}
				}
			}
			bsP018.addData("REC_sort", sortList);
			bsP018.addData("Acn", getAcn);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("new_retire_terms error >> {}",e);
		} finally {
			if(bsP018!=null && bsP018.getResult()) {
				target = "parking_fee/parking_withholding";
				model.addAttribute("result_data", bsP018);
			}else {
				bsP018.setPrevious("/INDEX/index");
				model.addAttribute(BaseResult.ERROR, bsP018);
			}
		}
		return target;
	}
	
	
	/**
	 * 申請代繳停車費
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/parking_withholding_apply")
	public String parking_withholding_apply(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{		
		String target = "/error";
		log.trace("parking_withholding_apply start");
		BaseResult bsAcnos = null;
		BaseResult bsP018 = null;
		try {
			bsAcnos = new BaseResult();
			
			String getAcn = reqParam.get("getAcn");
			log.trace(ESAPIUtil.vaildLog("GET ACN >>>{}"+getAcn));

			// 登入時的身分證字號
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			String email = (String) SessionUtil.getAttribute(model, SessionUtil.DPMYEMAIL, null);
			bsAcnos = parking_fee_service.N920_REST(cusidn, parking_fee_service.OUT_ACNO);
			bsP018 = parking_fee_service.P018_REST();
 			String address[] = {"台北市","新北市","高雄市","桃園市","台中市","台南市","新竹市"};
			List<Map<String,String>> sortList = new LinkedList<Map<String,String>>();
			for(int i = 0;i < address.length;i++) {
				for(Map<String,String> callRow : (List<Map<String,String>>)((Map<String,Object>)bsP018.getData()).get("REC")) {
					if(callRow.get("ZoneName").equals(address[i])) {
						sortList.add(callRow);
					}
				}
			}
			bsP018.addData("REC_sort", sortList);
			bsAcnos.addData("EMail", email);
			bsAcnos.addData("Acn", getAcn);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("parking_withholding_apply error >> {}",e);
		} finally {
			if(bsAcnos!=null && bsAcnos.getResult()) {
				target = "parking_fee/parking_withholding_apply";
				model.addAttribute("result_data", bsAcnos);
				model.addAttribute("result_data_p018", bsP018);
			}else {
				bsAcnos.setPrevious("/PARKING/FEE/parking_withholding");
				model.addAttribute(BaseResult.ERROR, bsAcnos);
			}
		}
		return target;
	}
	
	/**
	 * 申請代繳停車費
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/parking_withholding_apply_detile")
	public String parking_withholding_apply_detile(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target ="";
		// 解決Trust Boundary Violation 
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam); 
		// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
		boolean hasLocale = okMap.containsKey("locale");
		if (hasLocale) {
			log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
			Map LocalMap = CodeUtil.objectCovert(Map.class,SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
			target = "/parking_fee/parking_withholding_apply_detile_" + LocalMap.get("ZoneCode");
		} else {
			target = "/parking_fee/parking_withholding_apply_detile_" + okMap.get("ZoneCode");
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, okMap);
			model.addAttribute("result_data", okMap);
			
		}
		return target;
	}
	
	/**
	 * 申請代繳停車費
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/parking_withholding_apply_confirm")
	public String parking_withholding_apply_confirm(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		log.trace("parking_withholding_apply_confirm start");
		BaseResult bs = null;
		String jsondc = "";
		Map<String, String> result = null;
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {
			
			
			bs = new BaseResult();

			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = parking_fee_service.getTxToken();
			log.trace("parking_withholding_apply_confirm.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			if(bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			}
			
			// 登入時的身分證字號
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			result = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null);
			
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(result),"UTF-8");
			log.trace("parking_withholding_apply_confirm.jsondc: " + jsondc);
			
			log.debug("session result >> {}", result);
			result.put("TOKEN", ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			result.put("jsondc", jsondc); // IKEY要用不經過ESAPIUtil，會出錯
			log.trace("parking_withholding_apply_confirm.result: {}", result);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("parking_withholding_apply_confirm error >> {}",e);
		} finally {
			if(result!=null) {
				target = "parking_fee/parking_withholding_apply_confirm";
				model.addAttribute("result_data", result);
			}else {
				bs.setPrevious("/PARKING/FEE/parking_withholding_apply");
				model.addAttribute(BaseResult.ERROR, result);
			}
		}
		return target;
	}
	
	/**
	 * 申請代繳停車費
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/parking_withholding_apply_result")
	public String parking_withholding_apply_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		
		BaseResult bs = null;
		log.trace("parking_withholding_apply_result>>");
		Map<String, String> okMap = null;
		try {
			
			bs = new BaseResult();
            okMap = ESAPIUtil.validStrMap(reqParam);
			//okMap = reqParam;
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				okMap = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null);
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				log.trace("parking_withholding_apply_result.validate TXTOKEN...");
				log.trace("parking_withholding_apply_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("parking_withholding_apply_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
						okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("parking_withholding_apply_result.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("parking_withholding_apply_result TXTOKEN 不正確，TXTOKEN>>{}"+ okMap.get("TOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
						!okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("parking_withholding_apply_result.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("parking_withholding_apply_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				log.debug(ESAPIUtil.vaildLog("reqParam >> {}"+CodeUtil.toJson(reqParam)));
				bs = parking_fee_service.parking_withholding_apply_result(cusidn, okMap);
				bs.addData("CUSIDN", cusidn);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, okMap);
				
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("parking_withholding_apply_result error >> {}",e);
		} finally {
			log.debug(ESAPIUtil.vaildLog("okMap>>{}"+okMap));
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TOKEN"));
			if (bs != null && bs.getResult())
			{
				target = "parking_fee/parking_withholding_apply_result";
				List<Map<String, Object>> dataListMap = ((List<Map<String, Object>>) ((Map<String, Object>) bs.getData()).get("REC"));
				log.debug("dataListMap={}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
				model.addAttribute("result_data", bs);
				model.addAttribute("input_data", okMap);
			}
			else
			{
				//新增回上一頁的路徑
				bs.setPrevious("/PARKING/FEE/parking_withholding_apply");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	
	/**
	 * 繳費查詢
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/parking_withholding_query")
	public String parking_withholding_query(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{		
		String target = "/error";
		log.trace("parking_withholding_query start");
		BaseResult bsAcnos = null;
		BaseResult bsP018 = null;
		try {
			String getAcn = reqParam.get("getAcn");
			log.trace(ESAPIUtil.vaildLog("GET ACN >>>{}"+getAcn));
			bsAcnos = new BaseResult();
			// 登入時的身分證字號
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			bsAcnos = parking_fee_service.N920_REST(cusidn, parking_fee_service.OUT_ACNO);
 			String address[] = {"台北市","新北市","高雄市","桃園市","台中市","台南市","新竹市"};
			bsP018 = parking_fee_service.P018_REST();
			List<Map<String,String>> sortList = new LinkedList<Map<String,String>>();
			for(int i = 0;i < address.length;i++) {
				for(Map<String,String> callRow : (List<Map<String,String>>)((Map<String,Object>)bsP018.getData()).get("REC")) {
					if(callRow.get("ZoneName").equals(address[i])) {
						sortList.add(callRow);
					}
				}
			}
			bsP018.addData("REC_sort", sortList);
			bsAcnos.addData("getAcn", getAcn);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("parking_withholding_query error >> {}",e);
		} finally {
			if(bsAcnos!=null && bsAcnos.getResult()) {
				
				target = "parking_fee/parking_withholding_query_" + reqParam.get("QueryTerms_Str");
				model.addAttribute("result_data", bsAcnos);
				model.addAttribute("result_data_p018", bsP018);
			}else {
				bsAcnos.setPrevious("/PARKING/FEE/parking_withholding");
				model.addAttribute(BaseResult.ERROR, bsAcnos);
			}
		}
		return target;
	}
	

	
	/**
	 * 繳費查詢
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/parking_withholding_query_result")
	public String parking_withholding_query_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{		
		String target = "/error";
		log.trace("parking_withholding_query_result start");
		BaseResult bs = null;
		Map<String, String> okMap = null;
		try {
			//okMap = reqParam;
			okMap = ESAPIUtil.validStrMap(reqParam);
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				okMap = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null);
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
				log.debug(ESAPIUtil.vaildLog("reqParam >> {}"+CodeUtil.toJson(reqParam)));
				bs = parking_fee_service.parking_withholding_query_result(cusidn, okMap);
				bs.addData("CUSIDN", cusidn);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, okMap);
				
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("parking_withholding_query_result error >> {}",e);
		} finally {
			if(bs!=null && bs.getResult()) {
				
				target = "parking_fee/parking_withholding_query_result";
				List<Map<String, Object>> dataListMap = ((List<Map<String, Object>>) ((Map<String, Object>) bs.getData()).get("REC"));
				log.debug("dataListMap={}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
				model.addAttribute("result_data", bs);
				model.addAttribute("input_data", okMap);
			}else {
				bs.setPrevious("/PARKING/FEE/parking_withholding");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * 資料維護
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/parking_withholding_modify")
	public String parking_withholding_modify(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{		
		String target = "/error";
		log.trace("parking_withholding_modify start");
		BaseResult bs = null;
		try {
			String getAcn = reqParam.get("getAcn");
			log.trace(ESAPIUtil.vaildLog("GET ACN >>>{}"+getAcn));
			bs = new BaseResult();
			// 登入時的身分證字號
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			bs = parking_fee_service.parking_withholding_modify(cusidn,reqParam);
			bs.addData("getAcn", getAcn);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("parking_withholding_modify error >> {}",e);
		} finally {
			if(bs!=null && bs.getResult()) {
				target = "parking_fee/parking_withholding_modify";
				model.addAttribute("result_data", bs);
			}else {
				bs.setPrevious("/PARKING/FEE/parking_withholding");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * 資料維護
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/parking_withholding_modify_update")
	public String parking_withholding_modify_update(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{		
		String target = "/error";
		log.trace("parking_withholding_modify_update start");
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		BaseResult bs = new BaseResult();
		BaseResult bsAcnos = null;
		try {

			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = parking_fee_service.getTxToken();
			log.trace("parking_withholding_apply_confirm.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			if(bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			}
			// 登入時的身分證字號
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			bsAcnos = parking_fee_service.N920_REST(cusidn, parking_fee_service.OUT_ACNO);
			okMap.put("TOKEN", ((Map<String,String>) bs.getData()).get("TXTOKEN"));
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("parking_withholding_modify_update error >> {}",e);
		} finally {
			if(bsAcnos!=null && bsAcnos.getResult()) {
				target = "parking_fee/parking_withholding_modify_update";
				model.addAttribute("result_data", okMap);
				model.addAttribute("result_data_n920", bsAcnos);
			}else {
				bs.setPrevious("/PARKING/FEE/parking_withholding");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * 資料維護
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/parking_withholding_modify_update_result")
	public String parking_withholding_modify_update_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{		
		String target = "/error";
		log.trace("parking_withholding_modify_update_result start");
		BaseResult bs = null;
		Map<String, String> okMap = null;
		try {
			//okMap = reqParam;
			okMap = ESAPIUtil.validStrMap(reqParam);
			boolean hasLocale = okMap.containsKey("locale");
			bs = new BaseResult();
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				okMap = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null);
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				// 登入時的身分證字號
				log.trace("parking_withholding_apply_result.validate TXTOKEN...");
				log.trace("parking_withholding_apply_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("parking_withholding_apply_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
						okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("parking_withholding_apply_result.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("parking_withholding_apply_result TXTOKEN 不正確，TXTOKEN>>{}"+ okMap.get("TOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
						!okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("parking_withholding_apply_result.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("parking_withholding_apply_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				log.debug(ESAPIUtil.vaildLog("reqParam >> {}"+CodeUtil.toJson(reqParam)));
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
				bs = parking_fee_service.parking_withholding_modify_update_result(cusidn,reqParam);
				bs.addData("CUSIDN", cusidn);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, okMap);
				
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("parking_withholding_modify_update_result error >> {}",e);
		} finally {
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TOKEN"));
			if(bs!=null && bs.getResult()) {
				target = "parking_fee/parking_withholding_modify_update_result";
				List<Map<String, Object>> dataListMap = ((List<Map<String, Object>>) ((Map<String, Object>) bs.getData()).get("REC"));
				log.debug("dataListMap={}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
				model.addAttribute("result_data", bs);
				model.addAttribute("input_data", okMap);
			}else {
				bs.setPrevious("/PARKING/FEE/parking_withholding");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	
	/**
	 * 資料維護
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/parking_withholding_modify_delete")
	public String parking_withholding_modify_delete(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{		
		String target = "/error";
		log.trace("parking_withholding_modify_update start");
		BaseResult bs = new BaseResult();
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {
			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = parking_fee_service.getTxToken();
			log.trace("parking_withholding_apply_confirm.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			if(bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			}
			// 登入時的身分證字號
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			okMap.put("TOKEN", ((Map<String,String>) bs.getData()).get("TXTOKEN"));
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("parking_withholding_modify_delete error >> {}",e);
		} finally {
			target = "parking_fee/parking_withholding_modify_delete";
			model.addAttribute("result_data", okMap);
		}
		return target;
	}
	
	/**
	 * 資料維護
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/parking_withholding_modify_delete_result")
	public String parking_withholding_modify_delete_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{		
		
		
		
		String target = "/error";
		log.trace("parking_withholding_modify_delete_result start");
		BaseResult bs = null;
		Map<String, String> okMap = null;
		try {
			//okMap = reqParam;
			okMap = ESAPIUtil.validStrMap(reqParam);
			boolean hasLocale = okMap.containsKey("locale");
			bs = new BaseResult();
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				okMap = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null);
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				// 登入時的身分證字號
				log.trace("parking_withholding_modify_delete_result.validate TXTOKEN...");
				log.trace("parking_withholding_modify_delete_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("parking_withholding_modify_delete_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
						okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("parking_withholding_modify_delete_result.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("parking_withholding_modify_delete_result TXTOKEN 不正確，TXTOKEN>>{}"+ okMap.get("TOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
						!okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("parking_withholding_modify_delete_result.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("parking_withholding_modify_delete_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				log.debug(ESAPIUtil.vaildLog("reqParam >> {}"+CodeUtil.toJson(reqParam)));
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
				bs = parking_fee_service.parking_withholding_modify_delete_result(cusidn,reqParam);
				bs.addData("CUSIDN", cusidn);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, okMap);
				
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("parking_withholding_modify_delete_result error >> {}",e);
		} finally {
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TOKEN"));
			if(bs!=null && bs.getResult()) {
				target = "parking_fee/parking_withholding_modify_delete_result";
				List<Map<String, Object>> dataListMap = ((List<Map<String, Object>>) ((Map<String, Object>) bs.getData()).get("REC"));
				log.debug("dataListMap={}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
				model.addAttribute("result_data", bs);
				model.addAttribute("input_data", okMap);
			}else {
				bs.setPrevious("/PARKING/FEE/parking_withholding");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
		
	}
}
