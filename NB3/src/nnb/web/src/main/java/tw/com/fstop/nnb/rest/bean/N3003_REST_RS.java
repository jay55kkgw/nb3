package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N3003_REST_RS extends BaseRestBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4613495575558353513L;
	private String TOA_TRXCOD;//TLN7MBBT
	private String TOA_FILL01;//空白
	private String TOA_TXID;//交易代號
	private String TOA_FUNCID;//功能代碼
	private String TOA_ABEND;//回應代碼
	private String TOA_MSG;//錯誤訊息
	private String CURDATE;//交易日期YYYMMDD
	private String DDATE;//資料日期YYYMMDD
	private String CUSIDN;//統一編號
	private String TXNSEQ;//TXNSEQ
	private String ACN_OUT;//轉出帳號
	private String ACN;//貸款帳號
	private String SEQ;//貸款分號
	private String PALPAY;//轉帳金額
	private String PAY_MARK;//還款註記
	private String CMQTIME;
	
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getTOA_TRXCOD() {
		return TOA_TRXCOD;
	}
	public void setTOA_TRXCOD(String tOA_TRXCOD) {
		TOA_TRXCOD = tOA_TRXCOD;
	}
	public String getTOA_FILL01() {
		return TOA_FILL01;
	}
	public void setTOA_FILL01(String tOA_FILL01) {
		TOA_FILL01 = tOA_FILL01;
	}
	public String getTOA_TXID() {
		return TOA_TXID;
	}
	public void setTOA_TXID(String tOA_TXID) {
		TOA_TXID = tOA_TXID;
	}
	public String getTOA_FUNCID() {
		return TOA_FUNCID;
	}
	public void setTOA_FUNCID(String tOA_FUNCID) {
		TOA_FUNCID = tOA_FUNCID;
	}
	public String getTOA_ABEND() {
		return TOA_ABEND;
	}
	public void setTOA_ABEND(String tOA_ABEND) {
		TOA_ABEND = tOA_ABEND;
	}
	public String getTOA_MSG() {
		return TOA_MSG;
	}
	public void setTOA_MSG(String tOA_MSG) {
		TOA_MSG = tOA_MSG;
	}
	public String getCURDATE() {
		return CURDATE;
	}
	public void setCURDATE(String cURDATE) {
		CURDATE = cURDATE;
	}
	public String getDDATE() {
		return DDATE;
	}
	public void setDDATE(String dDATE) {
		DDATE = dDATE;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getTXNSEQ() {
		return TXNSEQ;
	}
	public void setTXNSEQ(String tXNSEQ) {
		TXNSEQ = tXNSEQ;
	}
	public String getACN_OUT() {
		return ACN_OUT;
	}
	public void setACN_OUT(String aCN_OUT) {
		ACN_OUT = aCN_OUT;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getSEQ() {
		return SEQ;
	}
	public void setSEQ(String sEQ) {
		SEQ = sEQ;
	}
	public String getPALPAY() {
		return PALPAY;
	}
	public void setPALPAY(String pALPAY) {
		PALPAY = pALPAY;
	}
	public String getPAY_MARK() {
		return PAY_MARK;
	}
	public void setPAY_MARK(String pAY_MARK) {
		PAY_MARK = pAY_MARK;
	}
}
