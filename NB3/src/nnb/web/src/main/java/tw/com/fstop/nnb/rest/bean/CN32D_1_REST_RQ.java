package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class CN32D_1_REST_RQ extends BaseRestBean_OLA implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1426000242932324292L;
private String KEYLEN;//無卡提款密碼長度
private String CMPW;//無卡提款密碼
private String CUSIDN;//統一編號
private String ISSUER;//無卡提款行庫別
private String ACN;//無卡提款帳號
private String TRFLAG;//交易項目
public String getKEYLEN() {
	return KEYLEN;
}
public void setKEYLEN(String kEYLEN) {
	KEYLEN = kEYLEN;
}
public String getCMPW() {
	return CMPW;
}
public void setCMPW(String cMPW) {
	CMPW = cMPW;
}
public String getCUSIDN() {
	return CUSIDN;
}
public void setCUSIDN(String cUSIDN) {
	CUSIDN = cUSIDN;
}
public String getACN() {
	return ACN;
}
public void setACN(String aCN) {
	ACN = aCN;
}
public String getTRFLAG() {
	return TRFLAG;
}
public void setTRFLAG(String tRFLAG) {
	TRFLAG = tRFLAG;
}
public String getISSUER() {
	return ISSUER;
}
public void setISSUER(String iSSUER) {
	ISSUER = iSSUER;
}
}
