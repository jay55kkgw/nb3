package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 查詢客戶綁定
 * 
 * @author Vincenthuang
 *
 */
public class MtpQuery_REST_RS extends BaseRestBean_QR_RS implements Serializable {

	private String Txacn;			//綁定帳號
	private String BankCode;		//綁定預設收款帳戶之金融機構代碼
	
	public String getTxacn() {
		return Txacn;
	}
	public void setTxacn(String txacn) {
		Txacn = txacn;
	}
	public String getBankCode() {
		return BankCode;
	}
	public void setBankCode(String bankCode) {
		BankCode = bankCode;
	}
}
