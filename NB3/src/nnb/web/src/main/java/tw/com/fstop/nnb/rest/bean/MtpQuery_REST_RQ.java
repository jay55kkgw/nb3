package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.Map;

/**
 * 查詢客戶綁定
 * 
 * @author Vincenthuang
 *
 */
public class MtpQuery_REST_RQ extends BaseRestBean_QR implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 39717691064748449L;
	
	//=========其餘參數===========
	private String ADOPID = "MtpQuery";	//紀錄TXNLOG用
	
	
	//=========API參數===========
	private String mobilephone;			//手機門號
	
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getMobilephone() {
		return mobilephone;
	}
	public void setMobilephone(String mobilephone) {
		this.mobilephone = mobilephone;
	}

	
	
}
