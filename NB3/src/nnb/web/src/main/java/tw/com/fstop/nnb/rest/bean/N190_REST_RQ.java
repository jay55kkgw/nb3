package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N190_REST_RQ  extends BaseRestBean_GOLD implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6359238222166709148L;
	private String USERDATA_X100;
	private String CUSIDN;
	private String ACN;
	public String getUSERDATA_X100() {
		return USERDATA_X100;
	}
	public void setUSERDATA_X100(String uSERDATA_X100) {
		USERDATA_X100 = uSERDATA_X100;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}

	
}
