package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class F001T_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -839343886979165716L;

	private String TOTBAL; // 轉出帳號帳戶餘額
	private String AVAAMT; // 轉出帳號可用餘額
	private String ADTXNO; // TXNFXRECORD_PKEY
	
	
	public String getTOTBAL() {
		return TOTBAL;
	}
	public void setTOTBAL(String tOTBAL) {
		TOTBAL = tOTBAL;
	}
	public String getAVAAMT() {
		return AVAAMT;
	}
	public void setAVAAMT(String aVAAMT) {
		AVAAMT = aVAAMT;
	}
	public String getADTXNO() {
		return ADTXNO;
	}
	public void setADTXNO(String aDTXNO) {
		ADTXNO = aDTXNO;
	}
	
	
}
