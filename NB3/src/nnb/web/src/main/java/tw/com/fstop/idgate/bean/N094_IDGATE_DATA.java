package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class N094_IDGATE_DATA implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1818113229377292595L;

	@SerializedName(value = "ACN") //黃金存摺帳號
	private String ACN;

	@SerializedName(value = "SVACN") //台幣存款帳號
	private String SVACN;
	
	@SerializedName(value = "TRNAMT") //應繳款總金額
	private String TRNAMT;

	@SerializedName(value = "CUSIDN") //身分驗證
	private String CUSIDN;
	
	public String getACN() {
		return ACN;
	}

	public void setACN(String aCN) {
		ACN = aCN;
	}

	public String getSVACN() {
		return SVACN;
	}

	public void setSVACN(String sVACN) {
		SVACN = sVACN;
	}

	public String getTRNAMT() {
		return TRNAMT;
	}

	public void setTRNAMT(String tRNAMT) {
		TRNAMT = tRNAMT;
	}

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
}
