package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N531_REST_RS extends BaseRestBean implements Serializable {

	private static final long serialVersionUID = 691285126660008588L;
	

	private String ABEND;
	private String USERDATA;
	private String COUNT;
	private String CMQTIME;
	
	LinkedList<N531_REST_RSDATA> REC;

	public String getUSERDATA() {
		return USERDATA;
	}
	public void setUSERDATA(String uSERDATA) {
		USERDATA = uSERDATA;
	}
	public String getABEND() {
		return ABEND;
	}
	public void setABEND(String aBEND) {
		ABEND = aBEND;
	}
	public String getCOUNT() {
		return COUNT;
	}
	public void setCOUNT(String cOUNT) {
		COUNT = cOUNT;
	}
	public LinkedList<N531_REST_RSDATA> getREC() {
		return REC;
	}
	public void setREC(LinkedList<N531_REST_RSDATA> rEC) {
		REC = rEC;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
}
