package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N871_REST_RSDATA extends BaseRestBean implements Serializable {


	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2254497815217535684L;
	

	private String ABEND;
	private String REC_NO;
	private String __OCCURS;
	private String ACN;
	private String USERDATA_X50;
	private String LSTIME;
	private String STADATE;
	private String ENDDATE;
	
	private String DPIBAL;
	private String TRNTYP;
	private String PAYDATE;
	private String LMTSFI;
	private String RPBAL;
	private String OUTAMT;
	private String BONCOD;
	private String LMTSFO;
	private String INPAMT;
	
	
	public String getABEND() {
		return ABEND;
	}
	public void setABEND(String aBEND) {
		ABEND = aBEND;
	}
	public String getREC_NO() {
		return REC_NO;
	}
	public void setREC_NO(String rEC_NO) {
		REC_NO = rEC_NO;
	}
	public String get__OCCURS() {
		return __OCCURS;
	}
	public void set__OCCURS(String __OCCURS) {
		this.__OCCURS = __OCCURS;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getUSERDATA_X50() {
		return USERDATA_X50;
	}
	public void setUSERDATA_X50(String uSERDATA_X50) {
		USERDATA_X50 = uSERDATA_X50;
	}
	public String getLSTIME() {
		return LSTIME;
	}
	public void setLSTIME(String lSTIME) {
		LSTIME = lSTIME;
	}
	public String getSTADATE() {
		return STADATE;
	}
	public void setSTADATE(String sTADATE) {
		STADATE = sTADATE;
	}
	public String getENDDATE() {
		return ENDDATE;
	}
	public void setENDDATE(String eNDDATE) {
		ENDDATE = eNDDATE;
	}
	public String getDPIBAL() {
		return DPIBAL;
	}
	public void setDPIBAL(String dPIBAL) {
		DPIBAL = dPIBAL;
	}
	public String getTRNTYP() {
		return TRNTYP;
	}
	public void setTRNTYP(String tRNTYP) {
		TRNTYP = tRNTYP;
	}
	public String getPAYDATE() {
		return PAYDATE;
	}
	public void setPAYDATE(String pAYDATE) {
		PAYDATE = pAYDATE;
	}
	public String getLMTSFI() {
		return LMTSFI;
	}
	public void setLMTSFI(String lMTSFI) {
		LMTSFI = lMTSFI;
	}
	public String getRPBAL() {
		return RPBAL;
	}
	public void setRPBAL(String rPBAL) {
		RPBAL = rPBAL;
	}
	public String getOUTAMT() {
		return OUTAMT;
	}
	public void setOUTAMT(String oUTAMT) {
		OUTAMT = oUTAMT;
	}
	public String getBONCOD() {
		return BONCOD;
	}
	public void setBONCOD(String bONCOD) {
		BONCOD = bONCOD;
	}
	public String getLMTSFO() {
		return LMTSFO;
	}
	public void setLMTSFO(String lMTSFO) {
		LMTSFO = lMTSFO;
	}
	public String getINPAMT() {
		return INPAMT;
	}
	public void setINPAMT(String iNPAMT) {
		INPAMT = iNPAMT;
	}
	
}
