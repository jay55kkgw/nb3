package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N844_REST_RSDATA implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5211380625971229199L;
	
	private String STATUS;
	private String AMTICHD;
	private String CLR;
	private String LOSACN;
	private String CARDFUN;
	private String BALANCE;
	
	
	public String getSTATUS() {
		return STATUS;
	}
	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}
	public String getAMTICHD() {
		return AMTICHD;
	}
	public void setAMTICHD(String aMTICHD) {
		AMTICHD = aMTICHD;
	}
	public String getCLR() {
		return CLR;
	}
	public void setCLR(String cLR) {
		CLR = cLR;
	}
	public String getLOSACN() {
		return LOSACN;
	}
	public void setLOSACN(String lOSACN) {
		LOSACN = lOSACN;
	}
	public String getCARDFUN() {
		return CARDFUN;
	}
	public void setCARDFUN(String cARDFUN) {
		CARDFUN = cARDFUN;
	}
	public String getBALANCE() {
		return BALANCE;
	}
	public void setBALANCE(String bALANCE) {
		BALANCE = bALANCE;
	}
}
