package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class AML_REST_RS extends BaseRestBean_GOLD implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String RESULT;
	private String LISTTYPE;
	public String getRESULT() {
		return RESULT;
	}
	public void setRESULT(String rESULT) {
		RESULT = rESULT;
	}
	public String getLISTTYPE() {
		return LISTTYPE;
	}
	public void setLISTTYPE(String lISTTYPE) {
		LISTTYPE = lISTTYPE;
	}
	

}
