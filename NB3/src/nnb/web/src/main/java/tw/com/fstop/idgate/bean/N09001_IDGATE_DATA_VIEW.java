package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;

public class N09001_IDGATE_DATA_VIEW  extends Base_IDGATE_DATA_VIEW implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6563994877431268998L;

	@SerializedName(value = "CMQTIME")
	private String CMQTIME;

	@SerializedName(value = "SVACN")
	private String SVACN;
	
	@SerializedName(value = "TRANTITLE")
	private String TRANTITLE;

	@SerializedName(value = "TRANNAME")
	private String TRANNAME;
	
	@SerializedName(value = "ACN")
	private String ACN;
	
	@SerializedName(value = "TRNGDFormat")
	private String TRNGDFormat;
	
	@SerializedName(value = "PRICEFormat")
	private String PRICEFormat;
	
	@SerializedName(value = "DISPRICEFormat")
	private String DISPRICEFormat;
	
	@SerializedName(value = "PERDISFormat")
	private String PERDISFormat;
	
	@SerializedName(value = "TRNFEEFormat")
	private String TRNFEEFormat;
	
	@SerializedName(value = "TRNAMTFormat")
	private String TRNAMTFormat;

	@Override
	public Map<String, String> coverMap(){
		Map<String, String> result = new LinkedHashMap<String, String>();
		result.put("交易名稱", this.TRANTITLE);
		result.put("交易類型", this.TRANNAME);
		result.put("交易日期", this.CMQTIME);
		result.put("臺幣轉出帳號", this.SVACN);
		result.put("黃金轉入帳號", this.ACN);
		result.put("買進公克數", this.TRNGDFormat + "公克");
		result.put("牌告單價", "新臺幣" + this.PRICEFormat + "元／公克");
		result.put("折讓後單價", "新臺幣" + this.DISPRICEFormat + "元／公克");
		result.put("折讓率", this.PERDISFormat);
		result.put("手續費",  "新臺幣" + this.TRNFEEFormat + "元");
		result.put("總扣款金額", "新臺幣" + this.TRNAMTFormat + "元");
		return result;
	}

	public String getTRANTITLE() {
		return TRANTITLE;
	}


	public void setTRANTITLE(String tRANTITLE) {
		TRANTITLE = tRANTITLE;
	}

	public String getTRANNAME() {
		return TRANNAME;
	}

	public void setTRANNAME(String tRANNAME) {
		TRANNAME = tRANNAME;
	}

	public String getCMQTIME() {
		return CMQTIME;
	}

	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}

	public String getSVACN() {
		return SVACN;
	}

	public void setSVACN(String sVACN) {
		SVACN = sVACN;
	}

	public String getACN() {
		return ACN;
	}

	public void setACN(String aCN) {
		ACN = aCN;
	}

	public String getTRNGDFormat() {
		return TRNGDFormat;
	}

	public void setTRNGDFormat(String tRNGDFormat) {
		TRNGDFormat = tRNGDFormat;
	}

	public String getPRICEFormat() {
		return PRICEFormat;
	}

	public void setPRICEFormat(String pRICEFormat) {
		PRICEFormat = pRICEFormat;
	}

	public String getDISPRICEFormat() {
		return DISPRICEFormat;
	}

	public void setDISPRICEFormat(String dISPRICEFormat) {
		DISPRICEFormat = dISPRICEFormat;
	}

	public String getPERDISFormat() {
		return PERDISFormat;
	}

	public void setPERDISFormat(String pERDISFormat) {
		PERDISFormat = pERDISFormat;
	}

	public String getTRNFEEFormat() {
		return TRNFEEFormat;
	}

	public void setTRNFEEFormat(String tRNFEEFormat) {
		TRNFEEFormat = tRNFEEFormat;
	}

	public String getTRNAMTFormat() {
		return TRNAMTFormat;
	}

	public void setTRNAMTFormat(String tRNAMTFormat) {
		TRNAMTFormat = tRNAMTFormat;
	}
}
