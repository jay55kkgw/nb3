package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N935_REST_RS extends BaseRestBean implements Serializable {

	
    /**
	 * 
	 */
	private static final long serialVersionUID = 6658999788110042748L;

	private String CMQTIME;

    private String CMRECNUM;

    private String OFFSET;

    private String REC_NO;//筆數

    private String __OCCURS;

    private String HEADER;

    private String MSGCOD;
    
	private LinkedList<N935_REST_RSDATA> REC;

	public String getCMQTIME()
	{
		return CMQTIME;
	}

	public void setCMQTIME(String cMQTIME)
	{
		CMQTIME = cMQTIME;
	}

	public String getCMRECNUM()
	{
		return CMRECNUM;
	}

	public void setCMRECNUM(String cMRECNUM)
	{
		CMRECNUM = cMRECNUM;
	}

	public String getOFFSET()
	{
		return OFFSET;
	}

	public void setOFFSET(String oFFSET)
	{
		OFFSET = oFFSET;
	}

	public String getREC_NO()
	{
		return REC_NO;
	}

	public void setREC_NO(String rEC_NO)
	{
		REC_NO = rEC_NO;
	}

	public String get__OCCURS()
	{
		return __OCCURS;
	}

	public void set__OCCURS(String __OCCURS)
	{
		this.__OCCURS = __OCCURS;
	}

	public String getHEADER()
	{
		return HEADER;
	}

	public void setHEADER(String hEADER)
	{
		HEADER = hEADER;
	}

	public String getMSGCOD()
	{
		return MSGCOD;
	}

	public void setMSGCOD(String mSGCOD)
	{
		MSGCOD = mSGCOD;
	}

	public LinkedList<N935_REST_RSDATA> getREC()
	{
		return REC;
	}

	public void setREC(LinkedList<N935_REST_RSDATA> rEC)
	{
		REC = rEC;
	}
	
	
}
