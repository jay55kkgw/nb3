package tw.com.fstop.nnb.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.util.StrUtils;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;

@Service
public class Change_Data_Service extends Base_Service
{

	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private I18n i18n;

	@Autowired
	private DaoService daoService;

	/**
	 * N100變更通訊地址/電話_c
	 * 
	 * @param params
	 * @return
	 */
	public BaseResult communication_data_confirm(String cusidn, Map<String, String> reqParam)
	{
		log.trace(ESAPIUtil.vaildLog("communication_data_confirm >> " + CodeUtil.toJson(reqParam)));
		BaseResult bs = null;
		try
		{
			bs = new BaseResult();
			reqParam.put("TXID", "N100_1");
			reqParam.put("ADOPID", "N100_1");
			bs = N100_REST(cusidn, reqParam);
			Map<String, Object> bsData = (Map) bs.getData();
			// Table內容修改
			ArrayList<Map<String, String>> rows = (ArrayList<Map<String, String>>) bsData.get("REC");
			for (Map<String, String> row : rows)
			{
				// 行庫名稱
				String brhcod = row.get("BRHCOD");
				// 比對bank名稱
				log.debug("BRHCOD>>{}", brhcod);
				String bank = daoService.getBankName(brhcod);
				if ("".equals(bank))
				{
					bank = i18n.getMsg("LB.D0378");
				}
				row.put("BRHNAME", bank);
			}
			// 取出第一筆
			Map<String, String> dataSet = rows.get(0);
			log.trace("dataSet>>{}", dataSet);
			bs.addData("dataSet", dataSet);
			// 複製一個REC做資料處理
			ArrayList<Map<String, String>> cloneRec = new ArrayList<Map<String, String>>();
			cloneRec.addAll(rows);
			cloneRec.remove(0);
			log.trace("cloneRec>>{}", cloneRec);
			bs.addData("cloneRec", cloneRec);
			// 頁面上rowspan用
			Integer rowcount = rows.size() + 1;
			bs.addData("ROWCOUNT", rowcount);
			bs.addData("COUNT", rows.size());
			log.trace("rows>>{}", rows);
		}
		catch (Exception e)
		{
			//e.printStackTrace();
			//Avoid Information Exposure Through an Error Message
			log.error("communication_data_confirm error >> {}",e); 
		}
		return bs;
	}

	/**
	 * N100變更通訊地址/電話_全部_確認
	 * 
	 * @param params
	 * @return
	 */
	public BaseResult communication_data_plural_c(String cusidn, Map<String, String> reqParam)
	{
		log.trace(ESAPIUtil.vaildLog("communication_data_plural_c >> " + CodeUtil.toJson(reqParam)));
		BaseResult bs = null;
		try
		{
			bs = new BaseResult();

			bs.addData("dataSet", reqParam);
			if (reqParam != null)
			{
				bs.setResult(true);
			}
		}
		catch (Exception e)
		{
			//e.printStackTrace();
			//Avoid Information Exposure Through an Error Message
			log.error("communication_data_plural_c error >> {}",e); 
			
		}
		return bs;
	}

	/**
	 * N100變更通訊地址/電話_全部_結果
	 * 
	 * @param params
	 * @return
	 */
	public BaseResult communication_data_plural_r(String cusidn, Map<String, String> reqParam)
	{
		log.trace(ESAPIUtil.vaildLog("communication_data_plural_r >> " + CodeUtil.toJson(reqParam)));
		BaseResult bs = null;
		try
		{
			bs = new BaseResult();
			reqParam.remove("jsondc");

			String value = reqParam.get("ROWDATA");
			reqParam.remove("ROWDATA");
			value = value.replace("{", "").replace("}", "").replace("[", "").replace("]", ""); // remove curly brackets
			log.trace("ROWDATA.value: " + value);
			String[] keyValuePairs = value.split(","); // split the string to creat key-value pairs
			log.trace("ROWDATA.keyValuePairs: " + keyValuePairs);

			Map<String, String> map = new HashMap<>();
			String SelectedRecord = "";
			for (String pair : keyValuePairs) // iterate over the pairs
			{
				String[] entry = pair.split("="); // split the pairs to get key and value

				String mapKey = "";
				String MapValue = "";
				// log.trace("ROWDATA.entry.length: " + entry.length);
				if (entry.length == 2)
				{
					mapKey = "".equals(entry[0].trim()) ? "" : entry[0].trim();
					MapValue = "".equals(entry[1].trim()) ? "" : entry[1].trim();
				}
				else
				{
					mapKey = entry[0].trim();
				}
				// log.trace("ROWDATA.mapKey:{} ", mapKey);
				// log.trace("ROWDATA.MapValue:{} ", MapValue);
				map.put(mapKey, MapValue); // add them to the hashmap and trim whitespaces
				if ("BRHNAME".equals(mapKey))
				{
					SelectedRecord += map.get("BRHCOD") + "_" + map.get("BRHNAME") + ",";
					SelectedRecord += map.get("POSTCOD2") + ",";
					SelectedRecord += map.get("CTTADR") + ",";
					SelectedRecord += map.get("ARACOD") + ",";
					SelectedRecord += map.get("TELNUM") + ",";
					SelectedRecord += map.get("ARACOD2") + ",";
					SelectedRecord += map.get("TELNUM2") + ",";
					SelectedRecord += map.get("MOBTEL");
					SelectedRecord = SelectedRecord + "|";
				}
			}
			log.trace("SelectedRecord:{} ", SelectedRecord);

			String value1 = reqParam.get("dataRow");
			reqParam.remove("dataRow");
			value1 = value1.replace("{", "").replace("}", "").replace("[", "").replace("]", ""); // remove curly
																									// brackets
			log.trace("dataRow.value: " + value1);
			String[] keyValuePairs1 = value1.split(","); // split the string to creat key-value pairs
			log.trace("dataRow.keyValuePairs: " + keyValuePairs1);

			Map<String, String> map1 = new HashMap<>();
			String ModifyData = "";
			for (String pair1 : keyValuePairs1) // iterate over the pairs
			{
				String[] entry = pair1.split("="); // split the pairs to get key and value

				String mapKey = "";
				String MapValue = "";
				// log.trace("dataRow.entry.length: " + entry.length);
				if (entry.length == 2)
				{
					mapKey = "".equals(entry[0].trim()) ? "" : entry[0].trim();
					MapValue = "".equals(entry[1].trim()) ? "" : entry[1].trim();
				}
				else
				{
					mapKey = entry[0].trim();
				}
				// log.trace("dataRow.mapKey:{} ", mapKey);
				// log.trace("dataRow.MapValue:{} ", MapValue);
				map1.put(mapKey, MapValue); // add them to the hashmap and trim whitespaces
				if ("TELNUM".equals(mapKey))
				{
					ModifyData += map1.get("POSTCOD2") + ",";
					ModifyData += map1.get("CTTADR") + ",";
					ModifyData += map1.get("ARACOD") + ",";
					ModifyData += map1.get("TELNUM") + ",";
					ModifyData += map1.get("ARACOD2") + ",";
					if (map1.get("TELEXT") != null && !"".equals(map1.get("TELEXT")))
					{
						ModifyData += map1.get("TELNUM2") + "#" + map1.get("TELEXT") + ",";
					}
					else
					{
						ModifyData += map1.get("TELNUM2") + ",";
					}
					ModifyData += map1.get("MOBTEL");
				}
			}
			log.trace("ModifyData:{} ", ModifyData);
			String ACNNO = reqParam.get("ACNNO");
			if (ACNNO != null && !"".equals(ACNNO))
			{
				ACNNO = ACNNO.substring(5);
				log.trace(ESAPIUtil.vaildLog("ACNNO >> " + ACNNO));
				reqParam.put("CHIP_ACN", ACNNO);
			}
			
			String CPPOSTCOD2 = map1.get("CPPOSTCOD2");
			String CPCTTADR = map1.get("CPCTTADR");
			if(StrUtils.isNotEmpty(CPPOSTCOD2)) {
				reqParam.put("CPPOSTCOD2", CPPOSTCOD2);
			}
			if(StrUtils.isNotEmpty(CPCTTADR)) {
				reqParam.put("CPCTTADR", CPCTTADR);
			}
			
			reqParam.put("SelectedRecord", SelectedRecord);
			reqParam.put("ModifyData", ModifyData);
			reqParam.put("ADOPID", "N100");
			log.trace(ESAPIUtil.vaildLog("reqParam >> " + CodeUtil.toJson(reqParam)));
			bs = N100_REST(cusidn, reqParam);

			bs.addData("POSTCOD2", map1.get("POSTCOD2"));
			bs.addData("CTTADR", map1.get("CTTADR"));
			bs.addData("ARACOD", map1.get("ARACOD"));
			bs.addData("TELNUM", map1.get("TELNUM"));
			bs.addData("ARACOD2", map1.get("ARACOD2"));
			bs.addData("TELNUM2", map1.get("TELNUM2"));
			bs.addData("MOBTEL", map1.get("MOBTEL"));
			bs.addData("TELEXT", map1.get("TELEXT"));
			// bs.setResult(true);

		}
		catch (Exception e)
		{
			//e.printStackTrace();
			//Avoid Information Exposure Through an Error Message
			log.error("communication_data_plural_r error >> {}",e); 
		}
		return bs;
	}

	/**
	 * N570
	 * 
	 * @param params
	 * @return
	 */
	public BaseResult fcy_data_query(String cusidn, Map<String, String> reqParam)
	{

		log.trace(ESAPIUtil.vaildLog("fcy_data_query >> " + CodeUtil.toJson(reqParam)));
		BaseResult bs = null;
		try
		{
			bs = new BaseResult();
			reqParam.put("ADOPID", "N570_1");
			bs = N570_REST(cusidn, reqParam);
			Map<String, Object> bsData = (Map) bs.getData();

			// 中文地址移除空白
			String cusad1c = (String) bsData.get("CUSAD1C");
			String cusad2c = (String) bsData.get("CUSAD2C");
			if (StrUtils.isNotEmpty(cusad1c))
			{
				cusad1c = cusad1c.replace("　", "");
				cusad1c = cusad1c.replace(" ", "");
			}
			if (StrUtils.isNotEmpty(cusad2c))
			{
				cusad2c = cusad2c.replace("　", "");
				cusad2c = cusad2c.replace(" ", "");
			}
			// 行庫名稱
			String bchid = (String) bsData.get("BCHID");
			// 比對bank名稱
			String bank = daoService.getBankName(bchid);
			if ("".equals(bank))
			{
				bank = i18n.getMsg("LB.D0378");//未知分行
			}
			bs.addData("BCHNAME", bank);
			bs.addData("CUSAD1C", cusad1c);
			bs.addData("CUSAD2C", cusad2c);

		}
		catch (Exception e)
		{
			//e.printStackTrace();
			//Avoid Information Exposure Through an Error Message
			log.error("fcy_data_query error >> {}",e); 
		}
		return bs;
	}

	/**
	 * N570
	 * 
	 * @param params
	 * @return
	 */
	public BaseResult fcy_data_confirm(String cusidn, Map<String, String> reqParam)
	{
		log.trace(ESAPIUtil.vaildLog("fcy_data_confirm >> " + CodeUtil.toJson(reqParam)));
		BaseResult bs = null;
		try
		{
			bs = new BaseResult();

			bs.addData("dataSet", reqParam);
			if (reqParam != null)
			{
				bs.setResult(true);
			}

		}
		catch (Exception e)
		{
			//e.printStackTrace();
			//Avoid Information Exposure Through an Error Message
			log.error("fcy_data_confirm error >> {}",e); 
		}
		return bs;
	}

	/**
	 * N570
	 * 
	 * @param params
	 * @return
	 */
	public BaseResult fcy_data_result(String cusidn, Map<String, String> reqParam)
	{
		log.trace(ESAPIUtil.vaildLog("fcy_data_result >> " + CodeUtil.toJson(reqParam)));
		BaseResult bs = null;
		try
		{
			bs = new BaseResult();

			String value = reqParam.get("ROWDATA");
			// 拿掉不必要的資料
			reqParam.remove("jsondc");
			reqParam.remove("ROWDATA");
			value = value.substring(1, value.length() - 1); // remove curly brackets
			log.trace(ESAPIUtil.vaildLog("ROWDATA.value >> " + value));
			String[] keyValuePairs = value.split(","); // split the string to creat key-value pairs
			log.trace(ESAPIUtil.vaildLog("ROWDATA.keyValuePairs >> " + keyValuePairs));

			Map<String, String> map = new HashMap<>();
			for (String pair : keyValuePairs) // iterate over the pairs
			{
				String[] entry = pair.split("="); // split the pairs to get key and value

				String mapKey = "";
				String MapValue = "";
				log.trace(ESAPIUtil.vaildLog("ROWDATA.entry.length:  >> " + CodeUtil.toJson(entry.length)));
				if (entry.length == 2)
				{
					mapKey = "".equals(entry[0].trim()) ? "" : entry[0].trim();
					MapValue = "".equals(entry[1].trim()) ? "" : entry[1].trim();
				}
				else
				{
					mapKey = entry[0].trim();
				}
				log.trace(ESAPIUtil.vaildLog("ROWDATA.mapKey >> " + mapKey));
				log.trace(ESAPIUtil.vaildLog("ROWDATA.MapValue >> " + MapValue));
				map.put(mapKey, MapValue); // add them to the hashmap and trim whitespaces
				reqParam.put(mapKey, MapValue);
			}
			reqParam.put("ADOPID", "N570");
			log.trace(ESAPIUtil.vaildLog("map >> " + map));
			log.trace(ESAPIUtil.vaildLog("reqParam >> " + reqParam));
			bs = N570_REST(cusidn, reqParam);

			bs.addData("dataSet", map);

			// if(reqParam != null) {
			// bs.setResult(true);
			// }

		}
		catch (Exception e)
		{
			//e.printStackTrace();
			//Avoid Information Exposure Through an Error Message
			log.error("fcy_data_result error >> {}",e); 
		}
		return bs;
	}

	/**
	 * N900變更信用卡帳單地址／電話(資料頁)
	 * 
	 * @param params
	 * @return
	 */
	public BaseResult credit_data_detail(String cusidn, Map<String, String> reqParam)
	{
		log.trace(ESAPIUtil.vaildLog("credit_data_detail >> " + CodeUtil.toJson(reqParam)));
		BaseResult bs = null;
		try
		{
			bs = new BaseResult();
			reqParam.put("TXID", "N900_1");
			reqParam.put("MSGNAME", "C900");
			reqParam.put("STAN", new DateTime().toString("HHmmss"));
			String TWDATE = DateUtil.getTWDate("").substring(1, 7);
			reqParam.put("DATE", TWDATE);
			reqParam.put("TIME", new DateTime().toString("HHmmss"));
			reqParam.put("DATE4", new DateTime().toString("MMdd"));
			reqParam.put("TERMID", String.format("%-8s", ""));
			reqParam.put("NUMTRA", "00");
			reqParam.put("NAME", String.format("%-30s", ""));
			reqParam.put("DATEBRI", "00000000");
			reqParam.put("IDNUM", String.format("%-15s", ""));
			reqParam.put("OFFPHONE", String.format("%-18s", ""));
			reqParam.put("HOUPHONE", String.format("%-18s", ""));
			reqParam.put("MOTMAINAME", String.format("%-30s", ""));
			reqParam.put("REASON", " ");
			reqParam.put("NEWPIN", String.format("%-16s", ""));
			reqParam.put("PERID", String.format("%-16s", cusidn));
			reqParam.put("ADOPID", "N900_1");
			bs = N900_REST(cusidn, reqParam);
			log.trace("GET BSDATA>>>>>{}", bs.getData());
			
			if(bs != null && bs.getResult()) {
				Map<String, String> row = (Map) bs.getData();
				
				row.put("HOMEPHONE", row.get("HOMEPHONE").replaceAll(" ", ""));
				row.put("OFFICPHONE", row.get("OFFICPHONE").replaceAll(" ", ""));
				row.put("MOBILEPHONE", row.get("MOBILEPHONE").replaceAll(" ", ""));
				row.put("ADDR1", row.get("ADDR1").replaceAll("　", ""));
				row.put("ADDR2", row.get("ADDR2").replaceAll("　", ""));
				row.put("CITYADDR1", row.get("CITYADDR1").replaceAll("　", ""));
				row.put("OFFICEADDR1", row.get("OFFICEADDR1").replaceAll("　", ""));
				row.put("OFFICEADDR2", row.get("OFFICEADDR2").replaceAll("　", ""));
				String ADDR = (row.get("ADDR1") + row.get("ADDR2")).replaceAll("　", "");
				row.put("ADDR", ADDR);
//			row.put("ZIPCODE", row.get("ZIPCODE").replaceAll(" ", ""));
				
				if (row.get("OFFICPHONE").indexOf("#") > -1)
				{
					String[] arrOFFICPHONE = row.get("OFFICPHONE").split("#");
					row.put("strOfficeTel", arrOFFICPHONE[0]);
					if (arrOFFICPHONE.length > 1 && arrOFFICPHONE[1].length() > 0)
					{
						row.put("strOfficeExt", arrOFFICPHONE[1]);
					}
				}
				else
				{
					row.put("strOfficeTel", row.get("OFFICPHONE"));
				}
//			if (StrUtils.isNotEmpty(row.get("ZIPCODE")))
//			{
//				if (row.get("ZIPCODE").length() > 0)
//				{
//					if (row.get("ZIPCODE").length() >= 9)
//					{
//						row.put("strZIP", row.get("ZIPCODE").substring(6, 9));
//					}
//					else if (row.get("ZIPCODE").length() >= 6)
//					{
//						row.put("strZIP", row.get("ZIPCODE").substring(3, 6));
//					}
//					else if (row.get("ZIPCODE").length() >= 3)
//					{
//						row.put("strZIP", row.get("ZIPCODE").substring(0, 3));
//					}
//				}
//			}
				if (StrUtils.isNotEmpty(row.get("SMSA")))
				{
					if (row.get("SMSA").equals("0001"))
					{
						// 戶籍地址
						row.put("strBILLTITLE", "LB.D0143");
						// row.put("strBILLADDR1", row.get("ADDR1").replaceAll(" ", ""));
						// row.put("strBILLADDR2", row.get("ADDR2").replaceAll(" ", ""));
						row.put("strBILLADDR1", (row.get("ADDR1") + row.get("ADDR2")).replaceAll(" ", ""));
						row.put("strZIP", row.get("ZIPCODE").substring(0, 3));
					}
					else if (row.get("SMSA").equals("0002"))
					{
						// 居住地址
						row.put("strBILLTITLE", "LB.D0063");
						row.put("strBILLADDR1", row.get("CITYADDR1").replaceAll(" ", ""));
						row.put("strZIP", row.get("ZIPCODE").substring(3, 6));
					}
					else
					{
						// 公司地址
						row.put("strBILLTITLE", "LB.D0090");
						// row.put("strBILLADDR1", row.get("OFFICEADDR1").replaceAll(" ", ""));
						// row.put("strBILLADDR2", row.get("OFFICEADDR2").replaceAll(" ", ""));
						row.put("strBILLADDR1", (row.get("OFFICEADDR1") + row.get("OFFICEADDR2")).replaceAll(" ", ""));
						row.put("strZIP", row.get("ZIPCODE").substring(6, 9));
					}
				}
			}
		}
		catch (Exception e)
		{
			//e.printStackTrace();
			//Avoid Information Exposure Through an Error Message
			log.error("credit_data_detail error >> {}",e); 
		}
		return bs;
	}

	/**
	 * N900變更信用卡帳單地址／電話(確認頁)
	 * 
	 * @param params
	 * @return
	 */
	public BaseResult credit_data_confirm(String cusidn, Map<String, String> reqParam)
	{

		log.trace(ESAPIUtil.vaildLog("credit_data_confirm~~~{}"+ CodeUtil.toJson(reqParam)));
		BaseResult bs = null;
		try
		{
			bs = new BaseResult();
			bs.addAllData(reqParam);
			if (reqParam.get("SMSA").equals("0001"))
			{
				// 戶籍地址
				bs.addData("strBILLTITLE", "LB.D0143");
			}
			else if (reqParam.get("SMSA").equals("0002"))
			{
				// 居住地址
				bs.addData("strBILLTITLE", "LB.D0063");
			}
			else
			{
				// 公司地址
				bs.addData("strBILLTITLE", "LB.D0090");
			}
			if (reqParam.get("newOFFICEEXT").length() > 0)
			{
				String OFFICETEL = reqParam.get("newOFFICEPHONE") + "#" + reqParam.get("newOFFICEEXT");
				log.trace(ESAPIUtil.vaildLog("OFFICETEL >> " + OFFICETEL));
				bs.addData("OFFICETEL", OFFICETEL);
			}
			else
			{
				bs.addData("OFFICETEL", reqParam.get("newOFFICEPHONE"));
			}

			bs.setResult(true);
			log.trace("GET BSDATA>>>>>{}", bs.getData());
		}
		catch (Exception e)
		{
			//e.printStackTrace();
			//Avoid Information Exposure Through an Error Message 
			log.error(ESAPIUtil.vaildLog("credit_data_confirm error >> " + e));
		}
		return bs;
	}

	/**
	 * N900變更信用卡帳單地址／電話(結果頁)
	 * 
	 * @param params
	 * @return
	 */
	public BaseResult credit_data_result(String cusidn, Map<String, String> reqParam)
	{
		log.trace(ESAPIUtil.vaildLog("credit_data_result >> " + CodeUtil.toJson(reqParam)));
		BaseResult bs = null;
		try
		{
			bs = new BaseResult();
			reqParam.put("TXID", "N900_2");
			String newACNNO = reqParam.get("ACNNO");
			if (newACNNO.length() > 11)
			{
				newACNNO = newACNNO.substring(newACNNO.length() - 11);
				log.trace(ESAPIUtil.vaildLog("newACNNO >> " + newACNNO));
				reqParam.put("ACNNO", newACNNO);
			}
			reqParam.put("ADOPID", "N900");
			bs = N900_REST(cusidn, reqParam);
			log.trace("GET BSDATA>>>>>{}", bs.getData());
			if (bs != null && bs.getResult())
			{
				reqParam.remove("SMSA");
				reqParam.remove("pkcs7Sign");
				reqParam.remove("jsondc");
				log.trace(ESAPIUtil.vaildLog("reqParam Data >> " + CodeUtil.toJson(reqParam)));
				bs.addAllData(reqParam);
				log.trace("BS ALL DATA>>>>>{}", bs.getData());
				Map<String, String> row = (Map) bs.getData();
				if (row.get("newOFFICEPHONE").indexOf("#") > -1)
				{
					String[] arrOFFICPHONE = row.get("newOFFICEPHONE").split("#");
					row.put("strOfficeTel", arrOFFICPHONE[0]);
					if (arrOFFICPHONE.length > 1 && arrOFFICPHONE[1].length() > 0)
					{
						row.put("strOfficeExt", arrOFFICPHONE[1]);
					}
				}
				else
				{
					row.put("strOfficeTel", row.get("newOFFICEPHONE"));
				}
				// 地址種類判斷
				if (row.get("SMSA").equals("0001"))
				{
					// 戶籍地址
					row.put("strBILLTITLE", "LB.D0143");
				}
				else if (row.get("SMSA").equals("0002"))
				{
					// 居住地址
					row.put("strBILLTITLE", "LB.D0063");
				}
				else
				{
					// 公司地址
					row.put("strBILLTITLE", "LB.D0090");
				}

			}

			// Table內容修改
			// ArrayList<Map<String, String>> rows = (ArrayList<Map<String, String>>) bsData.get("REC");
			// for (Map<String, String> row : rows)
			// {
			// if(row.get("newOFFICEPHONE").indexOf("#") > -1) {
			// String[] arrOFFICPHONE = row.get("newOFFICEPHONE").split("#");
			// row.put("strNewOfficeTel", arrOFFICPHONE[0]);
			// if(arrOFFICPHONE.length > 1 && arrOFFICPHONE[1].length() > 0) {
			// row.put("strNewOfficeExt", arrOFFICPHONE[1]);
			// }
			// }
			// else {
			// row.put("strNewOfficeTel", row.get("newOFFICEPHONE"));
			// }
			// if(row.get("SMSA").equals("0001")) {
			// row.put("strBILLTITLE", "戶籍地址");
			// }
			// else if(row.get("SMSA").equals("0002")) {
			// row.put("strBILLTITLE", "居住地址");
			// }
			// else {
			// row.put("strBILLTITLE", "公司地址");
			// }
			// }
			// //取出第一筆其他刪除
			// Map<String, String> dataSet = rows.get(0);
			// log.trace("dataSet>>{}",dataSet);
			// rows.remove(0);
			// log.trace("rows>>{}",rows);
			// //頁面上rowspan用
			// Integer rowcount = rows.size() + 1;
			// bs.addData("ROWCOUNT", rowcount);
			// log.trace("rowcount",rowcount);
			// bs.addData("dataSet", dataSet);
			// bs.addData("COUNT", rows.size());
		}
		catch (Exception e)
		{
			//e.printStackTrace();
			//Avoid Information Exposure Through an Error Message
			log.error("credit_data_result error >> {}",e); 
		}
		return bs;
	}
	
	/**
	 * 變更往來通訊地址，若信用卡帳單地址為公司地址，需打N900
	 * 公司地址必填，住家電話、公司電話、分機、行動電話皆以9999999 最長限制欄位填滿
	 * 
	 * @param params
	 * @return
	 */
	public BaseResult communication_data_plural_r_900(String cusidn, Map<String, String> reqParam)
	{
		log.trace(ESAPIUtil.vaildLog("communication_data_plural_r_900 >> " + CodeUtil.toJson(reqParam)));
		BaseResult bs = null;
		try
		{
			Map<String, String> map900 = new HashMap<>();
			map900.putAll(reqParam);
			map900.put("newADDR", reqParam.get("CPCTTADR"));
			map900.put("newZIP", reqParam.get("CPPOSTCOD2"));
			map900.put("newHOMEPHONE", "99999999999999999");
			map900.put("newOFFICEPHONE", "999999999999999999");
			map900.put("newMOBILEPHONE", "99999999999999999999999999");
			map900.put("SMSA", "0003");
			
			//因打第二次電文卡片序號需+1 避免序號錯誤
			int icseq = Integer.parseInt(reqParam.get("ICSEQ"));
			icseq = icseq + 1;
			String seq = String.format("%1$08d", new Object[]{icseq});
			
			map900.put("ICSEQ", seq);
			map900.put("iSeqNo", seq);
			
			log.info(ESAPIUtil.vaildLog("map900 >> " + CodeUtil.toJson(map900)));
			bs = credit_data_result(cusidn, map900);
		}
		catch (Exception e)
		{
			//e.printStackTrace();
			//Avoid Information Exposure Through an Error Message
			log.error("credit_data_result error >> {}",e); 
		}
		return bs;
	}

}
