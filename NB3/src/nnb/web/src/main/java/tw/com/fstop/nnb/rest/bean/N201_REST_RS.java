package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N201_REST_RS  extends BaseRestBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2230998693636085456L;
	private String DUEDT;
	private String occurMsg;
	private String ACN;
	private String MSGCOD;
	
	public String getDUEDT() {
		return DUEDT;
	}
	public void setDUEDT(String dUEDT) {
		DUEDT = dUEDT;
	}
	public String getOccurMsg() {
		return occurMsg;
	}
	public void setOccurMsg(String occurMsg) {
		this.occurMsg = occurMsg;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getMSGCOD() {
		return MSGCOD;
	}
	public void setMSGCOD(String mSGCOD) {
		MSGCOD = mSGCOD;
	}
}
