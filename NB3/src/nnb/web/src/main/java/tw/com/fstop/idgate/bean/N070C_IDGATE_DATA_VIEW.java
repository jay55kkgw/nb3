package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;

public class N070C_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5556555905527940420L;

	//轉帳日期
	@SerializedName(value = "transferdate")
	private String transferdate;
	//轉出帳號
	@SerializedName(value = "OUTACN_NPD")
	private String OUTACN_NPD;
	//轉出帳號
	@SerializedName(value = "ACNO")
	private String ACNO;
	//轉帳類型(約定、非約定)
	@SerializedName(value = "TransferType")
	private String TransferType;
	//手機號碼
	@SerializedName(value = "DPACNO")
	private String DPACNO;
	//戶名
	@SerializedName(value = "ACCOUNTNAME")
	private String ACCOUNTNAME;
	//轉帳金額
	@SerializedName(value = "AMOUNT")
	private String AMOUNT;
		
	@Override
	public Map<String, String> coverMap(){
		LinkedHashMap<String, String> result = new LinkedHashMap<String, String>();
		result.put("交易名稱", "手機號碼轉帳交易");
		result.put("交易類型","即時");
		result.put("轉帳日期",this.transferdate);
		if(this.TransferType.equals("NPD")) 
			result.put("轉出帳號",this.OUTACN_NPD);
		else 
			result.put("轉出帳號",this.ACNO);
		result.put("手機號碼",this.DPACNO);
		result.put("戶名",this.ACCOUNTNAME);
		result.put("轉帳金額",this.AMOUNT);
		return result;
	}
}
