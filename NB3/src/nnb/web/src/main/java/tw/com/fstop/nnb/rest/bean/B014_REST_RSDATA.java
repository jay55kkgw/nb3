package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class B014_REST_RSDATA extends BaseRestBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4134467394583575331L;
	/**
	 * 
	 */
	private String O01;//分配日期
	private String O02;//配息基準日
	private String O03;//委託單號
	private String O04;//商品代號
	private String O05;//商品名稱
	
	private String O06;//信託業務別
	private String O07;//基準日單位數
	private String O08;//基準日面額
	private String O09;//對帳單利率
	private String O10;//配息區間起
	
	private String O11;//配息區間迄
	private String O12;//配息撥款註記
	private String O13;//配息撥款日期
	private String O14;//配息金額
	private String O15;//手續費
	
	private String O16;//交易稅
	private String O17;//領息幣別
	private String O18;//銀行帳號
	
	public String getO01() {
		return O01;
	}
	public void setO01(String o01) {
		O01 = o01;
	}
	public String getO02() {
		return O02;
	}
	public void setO02(String o02) {
		O02 = o02;
	}
	public String getO03() {
		return O03;
	}
	public void setO03(String o03) {
		O03 = o03;
	}
	public String getO04() {
		return O04;
	}
	public void setO04(String o04) {
		O04 = o04;
	}
	public String getO05() {
		return O05;
	}
	public void setO05(String o05) {
		O05 = o05;
	}
	public String getO06() {
		return O06;
	}
	public void setO06(String o06) {
		O06 = o06;
	}
	public String getO07() {
		return O07;
	}
	public void setO07(String o07) {
		O07 = o07;
	}
	public String getO08() {
		return O08;
	}
	public void setO08(String o08) {
		O08 = o08;
	}
	public String getO09() {
		return O09;
	}
	public void setO09(String o09) {
		O09 = o09;
	}
	public String getO10() {
		return O10;
	}
	public void setO10(String o10) {
		O10 = o10;
	}
	public String getO11() {
		return O11;
	}
	public void setO11(String o11) {
		O11 = o11;
	}
	public String getO12() {
		return O12;
	}
	public void setO12(String o12) {
		O12 = o12;
	}
	public String getO13() {
		return O13;
	}
	public void setO13(String o13) {
		O13 = o13;
	}
	public String getO14() {
		return O14;
	}
	public void setO14(String o14) {
		O14 = o14;
	}
	public String getO15() {
		return O15;
	}
	public void setO15(String o15) {
		O15 = o15;
	}
	public String getO16() {
		return O16;
	}
	public void setO16(String o16) {
		O16 = o16;
	}
	public String getO17() {
		return O17;
	}
	public void setO17(String o17) {
		O17 = o17;
	}
	public String getO18() {
		return O18;
	}
	public void setO18(String o18) {
		O18 = o18;
	}

	
}
