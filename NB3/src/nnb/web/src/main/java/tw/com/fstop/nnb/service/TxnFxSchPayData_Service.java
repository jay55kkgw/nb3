package tw.com.fstop.nnb.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.TemporalAccessor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import fstop.orm.po.TXNFXSCHPAYDATA;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.tbb.nnb.dao.TxnFxSchPayDataDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;
import tw.com.fstop.util.JsonUtil;
import tw.com.fstop.web.util.StrUtils;

@Service
public class TxnFxSchPayData_Service extends Base_Service {
	private Logger log = LoggerFactory.getLogger(this.getClass().getName());

	@Autowired
	TxnFxSchPayDataDao txnFxSchPayDataDao;

	/**
	 * 外幣預約結果查詢
	 * 
	 * @param reqParam
	 *            must include cusidn,sdate,edate
	 * @return BaseResult
	 */
	public BaseResult query(Map<String, String> reqParam) {
		BaseResult bs = new BaseResult();
		Boolean flag = Boolean.FALSE;
		String cusidn = reqParam.get("cusidn");
		String sdate = reqParam.get("sdate");
		String edate = reqParam.get("edate");
		String dptxstatus  = reqParam.get("dptxstatus");
		try {

			if (StrUtils.isNull(cusidn)) {
				log.error(ESAPIUtil.vaildLog("cusidn>>>{}"+cusidn));
				bs.setMessage("cusidn不得為空");
				throw new Exception();
			}

			if (StrUtils.isNull(sdate) || StrUtils.isNull(edate)) {
				LocalDateTime now = LocalDateTime.now();
				DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE;
				formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
				reqParam.put("edate", now.format(formatter));
				reqParam.put("sdate", now.plusMonths(-6).format(formatter));
			}
//			else {
			// 檢查日期是否正常
//			DateTimeFormatter strToDateFormatter = DateTimeFormatter.ofPattern("yyyyMMdd");
//			TemporalAccessor sdate_c = LocalDate.parse(reqParam.get("sdate"), strToDateFormatter);
//			TemporalAccessor edate_c = LocalDate.parse(reqParam.get("edate"), strToDateFormatter);
//			}
			if (Integer.parseInt(reqParam.get("sdate")) > Integer.parseInt(reqParam.get("edate")) || cusidn.isEmpty()) {
				bs.setMessage("期間啟日不得大於期間迄日");
				throw new Exception();
			}
			// flag為true表示查全部
			if (StrUtils.isNull(dptxstatus)) {
				flag = Boolean.TRUE;
			}
			if(flag==false) {
				if(!("0".equals(dptxstatus)||"1".equals(dptxstatus)||"2".equals(dptxstatus))) {
					bs.setMessage("dptxstatus輸入錯誤"+dptxstatus);
					throw new Exception();
				}
			}
			List<TXNFXSCHPAYDATA> list = txnFxSchPayDataDao.getByUidOrAll(reqParam, flag);
			
			List<LinkedHashMap> nameList = new ArrayList<LinkedHashMap>();
			  for(TXNFXSCHPAYDATA po:list) {
			   LinkedHashMap<String,TXNFXSCHPAYDATA> map = new LinkedHashMap<String,TXNFXSCHPAYDATA>();
			   ObjectMapper p= new ObjectMapper();
			   Map map2 = p.convertValue(po, LinkedHashMap.class);

			   map2.remove("pks");
			   map2.put("fxschno", po.getPks().getFXSCHNO());
			   map2.put("fxschtxdate",po.getPks().getFXSCHTXDATE());		
			   map2.put("fxuserid",po.getPks().getUSERID());			
			   nameList.add((LinkedHashMap) map2);
			  }
			  
			bs.setData(nameList);
			bs.setResult(Boolean.TRUE);
			bs.setMessage("0", "查詢成功");
		} catch (DateTimeParseException e) {
			log.error(ESAPIUtil.vaildLog("sdate="+sdate+",edate="+edate));
			bs.setResult(Boolean.FALSE);
			bs.setMsgCode("2");
			bs.setMessage("日期輸入錯誤");
		} catch (Exception e) {
			log.error("error>>>{}"+e.getMessage());
			bs.setResult(Boolean.FALSE);
			bs.setMsgCode("1");
			if (bs.getMessage() == null) {
				bs.setMessage("查詢失敗");
			}
		}
		return bs;
	}
}
