package tw.com.fstop.nnb.spring.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import fstop.orm.po.TXNADDRESSBOOK;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.service.DaoService;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.TxnAddressBookDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.StrUtil;

@RestController
@RequestMapping(value = "/MB/TXNADDRESSBOOK")
public class TxnAddressBook_Controller {

	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	DaoService daoService;
	@Autowired
	I18n i18n;
	@Autowired
	private TxnAddressBookDao txnAddressBookDao;
	
	@RequestMapping(value = "/query" , method = {RequestMethod.POST} , produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody BaseResult query(HttpServletRequest request, HttpServletResponse response, @RequestBody Map<String,String> reqParam, Model model) {
		log.info(ESAPIUtil.vaildLog("reqParam>>{}"+CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		try {
			String cusidn = reqParam.get("cusidn");
			List<TXNADDRESSBOOK> list = daoService.findByDPUserID(cusidn);
			log.trace(ESAPIUtil.vaildLog("addressBook >>{}"+ list));
			
			bs.reset();			
			bs.setData(list);
			bs.setResult(Boolean.TRUE);
			bs.setMessage("0", i18n.getMsg("LB.X1805"));//查詢成功
		} catch (Exception e) {
			log.error(e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", i18n.getMsg("LB.X2132"));//查詢異常
		}
		return bs;
		
	}
	
	@RequestMapping(value = "/add" , method = {RequestMethod.POST} , produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody BaseResult add(HttpServletRequest request, HttpServletResponse response, @RequestBody Map<String,String> reqParam, Model model) {
		log.info(ESAPIUtil.vaildLog("reqParam>>{}"+CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		
		try {
			TXNADDRESSBOOK newAddrBook = new TXNADDRESSBOOK();
			DateTime d = new DateTime();
			
			newAddrBook.setDPUSERID(reqParam.get("dpuserid"));
			newAddrBook.setDPGONAME(reqParam.get("dpgoname"));
			newAddrBook.setDPABMAIL(reqParam.get("dpabmail"));
			//newAddrBook = CodeUtil.objectCovert(TXNADDRESSBOOK.class,reqParam);
			newAddrBook.setLASTDATE(d.toString("yyyyMMdd"));
			newAddrBook.setLASTTIME(d.toString("HHmmss"));
			txnAddressBookDao.save(newAddrBook);
			log.trace(ESAPIUtil.vaildLog("newAddrBook >>{}"+ newAddrBook));
			bs.reset();
			bs.setResult(Boolean.TRUE);
			bs.setMessage("0", i18n.getMsg("LB.X0435"));//新增成功
		} catch (Exception e) {
			log.error(e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", i18n.getMsg("LB.X2133"));
		}		
		return bs;
		
	}
	
	
	@RequestMapping(value = "/update" , method = {RequestMethod.POST} , produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody BaseResult update(HttpServletRequest request, HttpServletResponse response, @RequestBody Map<String,String> reqParam, Model model) {
		log.info(ESAPIUtil.vaildLog("reqParam>>{}"+CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		try {
			String dpaddbkid = reqParam.get("dpaddbkid").toString();
			TXNADDRESSBOOK addrBook = txnAddressBookDao.get(TXNADDRESSBOOK.class, new Integer(dpaddbkid));
			addrBook.setDPUSERID(reqParam.get("dpuserid"));
			addrBook.setDPABMAIL(reqParam.get("dpabmail"));
			addrBook.setDPGONAME(reqParam.get("dpgoname"));
			txnAddressBookDao.saveOrUpdate(addrBook);
			bs.reset();
			bs.setResult(Boolean.TRUE);
			bs.setMessage("0", i18n.getMsg("LB.X0293"));
		} catch (Exception e) {
			log.error(e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", i18n.getMsg("LB.X2134"));
		}		
		return bs;
		
	}
	
	
	/**
	 * 舊網銀專用
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/updateNNB" , method = {RequestMethod.POST} , produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody BaseResult updateNNB(HttpServletRequest request, HttpServletResponse response, @RequestBody Map<String,Object> reqParam, Model model) {
//		log.trace(ESAPIUtil.vaildLog("reqParam>>{}"+CodeUtil.toJson(reqParam)));
//		log.info("reqParam>>{}",reqParam);
		log.info(ESAPIUtil.vaildLog("reqParam>>{}"+CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		Map<String,String> old_map = null;
		Map<String,String> new_map = null;
		TXNADDRESSBOOK addrBook = null;
		Map<String, String> tmpMap = null;
		try {
			
			old_map = (Map<String, String>) reqParam.get("old_data") ;
			new_map =(Map<String, String>) reqParam.get("new_data") ;
			addrBook = txnAddressBookDao.findByInput(old_map.get("dpuserid"),old_map.get("dpgoname"), old_map.get("dpabmail")  );
//			轉大寫
			new_map.remove("dpaddbkid");
			toUpperCaseKey(new_map);
			toUpperCaseKey(old_map);
			log.debug("new_map>>{}",ESAPIUtil.vaildLog(CodeUtil.toJson(new_map)));
			if(addrBook != null) {
				old_map.clear();
				old_map = CodeUtil.objectCovert(Map.class,addrBook);
				old_map.putAll(new_map);
				log.debug("update.old_map>>{}",ESAPIUtil.vaildLog(CodeUtil.toJson(old_map)));
				addrBook = CodeUtil.objectCovert(TXNADDRESSBOOK.class,old_map);
				addrBook.setLASTDATE(DateTime.now().toString("yyyyMMdd"));
				addrBook.setLASTTIME(DateTime.now().toString("HHmmss"));
				log.debug("addrBook>>{}",ESAPIUtil.vaildLog(CodeUtil.toJson(addrBook)) );
				txnAddressBookDao.update(addrBook);
			}else {
				old_map.putAll(new_map);
				log.debug("save.old_map>>{}",ESAPIUtil.vaildLog(CodeUtil.toJson(old_map)));
				addrBook=CodeUtil.objectCovert(TXNADDRESSBOOK.class,old_map);
				addrBook.setDPADDBKID(null);
				addrBook.setLASTDATE(DateTime.now().toString("yyyyMMdd"));
				addrBook.setLASTTIME(DateTime.now().toString("HHmmss"));
				log.debug("save.addrBook>>{}",ESAPIUtil.vaildLog(CodeUtil.toJson(addrBook)) );
				txnAddressBookDao.save(addrBook);
			}
			bs.reset();
			bs.setResult(Boolean.TRUE);
			bs.setMessage("0", i18n.getMsg("LB.X0293"));
		} catch (Exception e) {
			log.error("{}",e);
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", i18n.getMsg("LB.X2134"));
		}		
		return bs;
		
	}
	
	@RequestMapping(value = "/delete" , method = {RequestMethod.POST} , produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody BaseResult delete(HttpServletRequest request, HttpServletResponse response, @RequestBody Map<String,String> reqParam, Model model) {
		log.info(ESAPIUtil.vaildLog("reqParam>>{}"+CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		try {
			String dpaddbkid = reqParam.get("dpaddbkid");
			TXNADDRESSBOOK delAddrBook = new TXNADDRESSBOOK();
			delAddrBook.setDPADDBKID(new Integer(dpaddbkid));
			txnAddressBookDao.remove(delAddrBook);
			bs.reset();
			bs.setResult(Boolean.TRUE);
			bs.setMessage("0", i18n.getMsg("LB.X0436"));
		} catch (Exception e) {
			log.error(e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", i18n.getMsg("LB.X2135"));
		}	
		return bs;
		
	}
	
	@RequestMapping(value = "/deleteNNB" , method = {RequestMethod.POST} , produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody BaseResult deleteNNB(HttpServletRequest request, HttpServletResponse response, @RequestBody Map<String,String> reqParam, Model model) {
		log.info(ESAPIUtil.vaildLog("reqParam>>{}"+CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		TXNADDRESSBOOK delAddrBook = null;
		String dpgoname = null;
		String dpabmail = null;
		try {
			if(StrUtil.isEmpty(reqParam.get("dpuserid"))){
				bs.setResult(Boolean.FALSE);
				bs.setMessage("1", "dpuserid 未必填。");
				return bs;
			}
			
			dpgoname = StrUtil.isEmpty(reqParam.get("dpgoname"))?"":reqParam.get("dpgoname");
			dpabmail = StrUtil.isEmpty(reqParam.get("dpabmail"))?"":reqParam.get("dpabmail");
			delAddrBook = txnAddressBookDao.findByInput(reqParam.get("dpuserid"), dpgoname, dpabmail);
			if(delAddrBook == null) {
				bs.setResult(Boolean.FALSE);
				bs.setMessage("1", "查無資料，未執行刪除。");
				return bs;
			}
			txnAddressBookDao.remove(delAddrBook);
			bs.reset();
			bs.setResult(Boolean.TRUE);
			bs.setMessage("0", i18n.getMsg("LB.X0436"));
		} catch (Exception e) {
			log.error(e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", i18n.getMsg("LB.X2135"));
		}	
		return bs;
		
	}
	
	/**
	 * map key 轉大寫
	 * @param map
	 */
	public void toUpperCaseKey(Map<String,String> map) {
		Map<String,String> tmpMap = new HashMap<String, String>();
		try {
			for(String key:map.keySet()) {
				tmpMap.put(key.toUpperCase(), map.get(key));
			}
			map.clear();
			map.putAll(tmpMap);
		} catch (Exception e) {
			log.error("{}",e);
		}
		
	}
}
