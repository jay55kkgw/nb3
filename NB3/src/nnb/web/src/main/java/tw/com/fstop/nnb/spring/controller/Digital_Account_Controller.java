
package tw.com.fstop.nnb.spring.controller;

import java.io.File;
import java.net.URLEncoder;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import fstop.orm.po.TXNACNAPPLY;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.custom.annotation.IPVERIFY;
import tw.com.fstop.nnb.service.Digital_Account_Service;
import tw.com.fstop.nnb.service.Update_File_Service;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.TxnAcnApplyDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.WebUtil;

@SessionAttributes({ 	SessionUtil.CUSIDN,SessionUtil.BACK_DATA,SessionUtil.CONFIRM_LOCALE_DATA, SessionUtil.UPDATE_IMAGE_DATA, SessionUtil.OTP_SECCURE_TOKEN,
						SessionUtil.RESULT_LOCALE_DATA,SessionUtil.STEP1_LOCALE_DATA,SessionUtil.STEP2_LOCALE_DATA,SessionUtil.OUTSIDE_SOURCE,
						SessionUtil.STEP3_LOCALE_DATA, SessionUtil.ONLINE_APPLY_DATA,SessionUtil.TRANSFER_RESULT_TOKEN,SessionUtil.TRANSFER_RESULT_FINSH_TOKEN })
@Controller
@RequestMapping(value = "/DIGITAL/ACCOUNT")
public class Digital_Account_Controller {

	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private Digital_Account_Service digital_account_service;
	
	@Autowired
	private I18n i18n;
	
	@Autowired
	ServletContext context; 
	
	@Autowired
	Update_File_Service update_file_service;
	
	@Autowired
	TxnAcnApplyDao txnAcnApplyDao;
	/* 
	 * 數位存款帳戶晶片金融卡確認領用申請及變更密碼_進入頁
	 */
	@RequestMapping(value = "/financial_card_confirm")
	public String financial_card_confirm(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		String target = "/digital_account/ErrorWithoutMenu";
		try {
			//清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			
//			SessionUtil.removeAttribute(model, "CUSIDN", "");
//			SessionUtil.removeAttribute(model, "UID", "");
//			SessionUtil.removeAttribute(model, "Request", "");
//			SessionUtil.removeAttribute(model, "Request1", "");
			
		} catch (Exception e) {
			log.error("", e);
		} finally {
			target = "/digital_account/financial_card_confirm";
		}
		return target;		
	}
	/* 
	 * 數位存款帳戶晶片金融卡確認領用申請及變更密碼_step1
	 */
	@IPVERIFY(cusidnKey="CUSIDN")
	@RequestMapping(value = "/financial_card_confirm_step1")
	public String financial_card_confirm_step1(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		String target = "/digital_account/ErrorWithoutMenu";
		BaseResult bs = null;
		log.trace("financial_card_confirm_step1...");
		try {
			//清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			log.trace(ESAPIUtil.vaildLog("financial_card_confirm_step1.reqParam: {}" + CodeUtil.toJson(reqParam)));
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			
			String jsondc = URLEncoder.encode(CodeUtil.toJson(okMap), "UTF-8");
			okMap.put("jsondc", jsondc);
			
			bs = new BaseResult();
			bs = digital_account_service.financial_card_confirm_step1(okMap);
			log.debug("financial_card_confirm_step1.bs: {}", CodeUtil.toJson(bs));
			
			// 使用者統編加密後存取
			SessionUtil.addAttribute(model, SessionUtil.CUSIDN,
											Base64.getEncoder().encodeToString(reqParam.get("CUSIDN").toUpperCase().getBytes()) );
			
			
		} catch (Exception e) {
			log.error("", e);
		} finally {
			log.trace("bs.Result: {}", bs.getResult());
			if( bs != null && bs.getResult()) {
				target = "/digital_account/financial_card_confirm_step1";
				model.addAttribute("financial_card_confirm_step1", bs);
			} else {
				// 到錯誤頁，錯誤頁的確認按鈕導頁到輸入頁
				bs.setNext("/DIGITAL/ACCOUNT/financial_card_confirm");
				model.addAttribute("error", bs);
			}
		}
		return target;		
	}
	/* 
	 * 數位存款帳戶晶片金融卡確認領用申請及變更密碼_step2
	 */
	@RequestMapping(value = "/financial_card_confirm_step2")
	public String financial_card_confirm_step2(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {

		String target = "/digital_account/ErrorWithoutMenu";
		BaseResult bs = null;
		log.trace("financial_card_confirm_step2...");
		Map<String, String> okMap = null;
		try {
			okMap = ESAPIUtil.validStrMap(reqParam);
			log.trace(ESAPIUtil.vaildLog("financial_card_confirm_step2.reqParam: {}"+ CodeUtil.toJson(okMap)));
			
			//清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			
			bs = new BaseResult();

			if(reqParam.get("FGTXWAY") != null && "6".equals(reqParam.get("FGTXWAY"))) {
				String uuid = (String)SessionUtil.getAttribute(model, SessionUtil.OTP_SECCURE_TOKEN, null);
				log.trace("uuid >> {}", uuid);
				if(uuid != null && uuid.equals(reqParam.get("OTPTOKEN"))) {
					bs.setResult(true);
				}
				if(!bs.getResult()) {
					throw new Exception(bs.getMsgCode());
				}
			}
			bs = digital_account_service.financial_card_confirm_step2(okMap);
			log.debug("financial_card_confirm_step2.bs {}", CodeUtil.toJson(bs));
			
		} catch (Exception e) {
			log.error("", e);
		} finally {
			if( bs != null && bs.getResult()) {
				try {
					String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
					reqParam.put(SessionUtil.CUSIDN, cusidn);
					String jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam), "UTF-8");
					log.trace(ESAPIUtil.vaildLog("financial_card_confirm_step2.jsondc: " + jsondc));
					okMap.put("jsondc", jsondc);
					
					// 回上一頁重新賦值
					if( okMap.containsKey("previous") ){
						Map<String, String> backMap = (Map<String, String>) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
						okMap = backMap;
					}
					Map<String,Object> tempbs = (Map<String, Object>) bs.getData();
					tempbs.put("FGTXWAY", okMap.get("FGTXWAY"));
					log.trace("financial_card_confirm_step2.bs: {}", CodeUtil.toJson(bs));
				} catch (Exception e) {
					log.error("", e);
				}
				model.addAttribute("financial_card_confirm_step2", bs);
				target = "/digital_account/financial_card_confirm_step2";
			}
		}
		return target;		
	}
	/* 
	 * 數位存款帳戶晶片金融卡確認領用申請及變更密碼_step2_啟用電文_Ajax
	 */
	@RequestMapping(value = "/financial_card_confirm_step2_activate_aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult financial_card_confirm_step2_activate(HttpServletRequest request, HttpServletResponse response, 
			@RequestParam Map<String, String> reqParam, Model model) {
		BaseResult bs = null;
		log.trace("financial_card_confirm_step2_activate_aj...");
		try {
			//清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			log.trace(ESAPIUtil.vaildLog("financial_card_confirm_step2_activate_aj.reqParam: {}"+ CodeUtil.toJson(reqParam)));
			
			bs = new BaseResult();
			bs = digital_account_service.financial_card_confirm_cardEnable(reqParam);
			log.debug("financial_card_confirm_step2_activate.bs {}", CodeUtil.toJson(bs));
		} catch (Exception e) {
			log.error("", e);
		} finally {
			if(bs!=null && bs.getResult()) {
				Map<String,Object> datamap = (Map<String, Object>) bs.getData();
				bs.setMsgCode(datamap.get("TOPMSG").toString());
			}
		}
		return bs;
			
	}
	/* 
	 * 數位存款帳戶晶片金融卡確認領用申請及變更密碼_step3
	 */
	@RequestMapping(value = "/financial_card_confirm_step3")
	public String financial_card_confirm_step3(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/digital_account/ErrorWithoutMenu";
		BaseResult bs = null;
		log.trace("financial_card_confirm_step3...");
		try {
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam); 
			//清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			
			log.trace(ESAPIUtil.vaildLog("financial_card_confirm_step3.reqParam {}" + CodeUtil.toJson(okMap)));
			bs = new BaseResult();
			bs.setData(okMap);
			
		} catch (Exception e) {
			log.error("", e);
		}finally {
			model.addAttribute("ts", (new Date()).getTime());
			model.addAttribute("financial_card_confirm_step3",bs);
			target = "/digital_account/financial_card_confirm_step3";
		}
		return target;		
	}
	/* 
	 * 數位存款帳戶晶片金融卡確認領用申請及變更密碼_step4
	 */
	@RequestMapping(value = "/financial_card_confirm_step4")
	public String financial_card_confirm_step4(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/digital_account/ErrorWithoutMenu";
		BaseResult bs = null;
		log.trace("financial_card_confirm_step4...");
		try {
			//清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			log.trace(ESAPIUtil.vaildLog("financial_card_confirm_step4.reqParam: {}"+ CodeUtil.toJson(reqParam)));
			
			bs = new BaseResult();
			bs = digital_account_service.financial_card_confirm_cardEnable(reqParam);
			log.debug("financial_card_confirm_step4.bs {}", CodeUtil.toJson(bs));
		} catch (Exception e) {
			log.error("", e);
		} finally {
			if(bs!=null && bs.getResult()) {
				// 錯誤訊息沒有MSGCOD欄位故改抓TOPMSG，若ms沒回應FE0002則bs.getData()為null，改抓bs.getMsgCode
				String msgcod = "";
				Map<String,Object> dataMap = (Map<String,Object>) bs.getData();
				if(dataMap!=null) {
					msgcod = dataMap.get("TOPMSG")!=null
							? String.valueOf(dataMap.get("TOPMSG")) : String.valueOf(dataMap.get("msgCode"));
				} else {
					msgcod = bs.getMsgCode();
				}
				log.trace("isProdLogin.msgcod: {}", msgcod);
				bs.setMsgCode(msgcod);
				
				model.addAttribute("financial_card_confirm_step4",bs);
				target = "/digital_account/financial_card_confirm_step4";
			}
		}
		return target;		
	}
	
	
	/* 數位存款帳戶補申請晶片金融卡
	 * 進入頁
	 */
	@RequestMapping(value = "/financial_card_renew")
	public String financial_card_renew(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, 
			Model model) {
		String target = "/digital_account/ErrorWithoutMenu";
		try {
		} catch (Exception e) {
			log.error("", e);
		}finally {
			target = "/digital_account/financial_card_renew";
		}
		return target;		
	}
	//數位存款帳戶補申請晶片金融卡_step1
	@IPVERIFY(cusidnKey="CUSIDN")
	@RequestMapping(value = "/financial_card_renew_step1")
	public String financial_card_renew_step1(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		String target = "/digital_account/ErrorWithoutMenu";
		BaseResult bs = null;
		log.trace("financial_card_renew_step1 ~ Start");
		try {
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			boolean hasLocale = okMap.containsKey("locale");
			String jsondc = null;
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			//語系切換 & 上一頁
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null));
			}
			else 
			{
				log.trace(ESAPIUtil.vaildLog("ReqParam >> {}"+CodeUtil.toJson(reqParam)));
				bs=new BaseResult();
				bs = digital_account_service.financial_card_renew_step1(okMap);
				bs.addData("jsondc",jsondc);
				log.debug("Bsdata >> {}", bs.getData());
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			log.error("", e);
		}finally {
			log.debug("BsResult >> {}", bs.getResult());
			if(bs!=null && bs.getResult()) {
				if((((Map<String, Object>) bs.getData()).get("MSGCOD"))!=null) {
					if((((Map<String, Object>) bs.getData()).get("MSGCOD")).equals("0000")) {
						model.addAttribute("financial_card_renew_step1",bs);
						target = "/digital_account/financial_card_renew_step1";
					}
				}else {
					bs.setPrevious("/DIGITAL/ACCOUNT/financial_card_renew");
					model.addAttribute(BaseResult.ERROR, bs);
				}
			}else {
				bs.setPrevious("/DIGITAL/ACCOUNT/financial_card_renew");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;		
	}
	//數位存款帳戶補申請晶片金融卡_step2
	@RequestMapping(value = "/financial_card_renew_step2")
	public String financial_card_renew_step2(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		String target = "/digital_account/ErrorWithoutMenu";
		BaseResult bs = null;
		log.trace("financial_card_renew_step2 ~ Start");
		try {
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			boolean hasLocale = okMap.containsKey("locale");
			//語系切換 & 上一頁
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP2_LOCALE_DATA, null));
			}
			else 
			{				
				log.trace(ESAPIUtil.vaildLog("ReqParam >> {}" + CodeUtil.toJson(reqParam)));
				bs=new BaseResult();
				if(reqParam.get("FGTXWAY") != null && "6".equals(reqParam.get("FGTXWAY"))) {
					String uuid = (String)SessionUtil.getAttribute(model, SessionUtil.OTP_SECCURE_TOKEN, null);
					log.trace("uuid >> {}", uuid);
					if(uuid != null && uuid.equals(reqParam.get("OTPTOKEN"))) {
						bs.setResult(true);
					}
					if(!bs.getResult()) {
						throw new Exception(bs.getMsgCode());
					}
				}
				bs = digital_account_service.financial_card_renew_step2(okMap);
				log.debug("Bsdata >> {}", bs.getData());
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.STEP2_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			log.error("", e);
		} finally {
			log.debug("BsResult >> {}", bs.getResult());
			if(bs!=null && bs.getResult()) {
				model.addAttribute("financial_card_renew_step2",bs);
				target = "/digital_account/financial_card_renew_step2";
			}else {
				bs.setNext("/DIGITAL/ACCOUNT/financial_card_renew");
				model.addAttribute(BaseResult.ERROR,bs);
			}
		}
		return target;		
	}
	//數位存款帳戶補申請晶片金融卡_step3
	@RequestMapping(value = "/financial_card_renew_step3")
	public String financial_card_renew_step3(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		String target = "/digital_account/ErrorWithoutMenu";
		BaseResult bs = null;
		log.trace("financial_card_renew_step3 ~ Start");
		try {
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			boolean hasLocale = okMap.containsKey("locale");
			//語系切換 & 上一頁
			if (hasLocale || reqParam.get("isBack").equals("Y"))
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP3_LOCALE_DATA, null));
			}
			else 
			{
				log.trace(ESAPIUtil.vaildLog("ReqParam >> {}" +CodeUtil.toJson(reqParam)));
				bs=new BaseResult();
				bs = digital_account_service.financial_card_renew_step3(okMap);
				log.debug("Bsdata >> {}", bs.getData());
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.STEP3_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			log.error("", e);
		}finally {
			log.debug("BsResult >> {}", bs.getResult());
			if(bs!=null && bs.getResult()) {
				model.addAttribute("financial_card_renew_step3",bs);
				target = "/digital_account/financial_card_renew_step3";
			}else {
				bs.setNext("/DIGITAL/ACCOUNT/financial_card_renew");
				model.addAttribute(BaseResult.ERROR,bs);
			}
		}
		return target;		
	}
	//數位存款帳戶補申請晶片金融卡_step4
	@RequestMapping(value = "/financial_card_renew_step4")
	public String financial_card_renew_step4(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		String target = "/digital_account/ErrorWithoutMenu";
		BaseResult bs = null;
		log.trace("financial_card_renew_step4 ~ Start");
		try {
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			boolean hasLocale = okMap.containsKey("locale");
			//語系切換 & 上一頁
			if (hasLocale || reqParam.get("isBack").equals('Y'))
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP3_LOCALE_DATA, null));
			}
			else 
			{
				log.trace(ESAPIUtil.vaildLog("ReqParam >> {}" + CodeUtil.toJson(reqParam)));
				bs=new BaseResult();
				bs = digital_account_service.financial_card_renew_step4(okMap);
				log.debug("Bsdata >> {}", bs.getData());
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.STEP3_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			log.error("", e);
		}finally {
			log.debug("BsResult >> {}", bs.getResult());
			if(bs!=null && bs.getResult()) {
				model.addAttribute("financial_card_renew_step4",bs);
				target = "/digital_account/financial_card_renew_step4";
			}else {
				bs.setNext("/DIGITAL/ACCOUNT/financial_card_renew");
				model.addAttribute(BaseResult.ERROR,bs);
			}
		}
		return target;		
	}
	//數位存款帳戶補申請晶片金融卡_step5
	@RequestMapping(value = "/financial_card_renew_step5")
	public String financial_card_renew_step5(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		String target = "/digital_account/ErrorWithoutMenu";
		BaseResult bs = null;
		log.trace("financial_card_renew_step5 ~ Start");
		try {
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			boolean hasLocale = okMap.containsKey("locale");
			//語系切換 & 上一頁
			if (hasLocale || reqParam.get("isBack").equals('Y'))
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
			}
			else 
			{
				log.trace(ESAPIUtil.vaildLog("ReqParam >> {}"+CodeUtil.toJson(reqParam)));
				bs=new BaseResult();
				bs = digital_account_service.financial_card_renew_step5(okMap);
				log.debug("Bsdata >> {}", bs.getData());
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			log.error("", e);
		}finally {
			log.trace("BsResult >> {}", bs.getResult());
			if(bs!=null && bs.getResult()) {
				if(((Map<String, String>) bs.getData()).get("MSGCOD").equals("0000")||((Map<String, String>) bs.getData()).get("MSGCOD").equals("    "))
					target = "/digital_account/financial_card_renew_step5";
				else
					target = "/digital_account/financial_card_renew_step5_err";
				model.addAttribute("financial_card_renew_step5",bs);
			}else {
				bs.setNext("/DIGITAL/ACCOUNT/financial_card_renew");
				model.addAttribute(BaseResult.ERROR,bs);
			}
		}
		return target;		
	}

	//線上開立數位存款帳戶(N203)
	@RequestMapping(value = "/apply_digital_account")
	public String apply_digital_account(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model, HttpSession session) {
		Locale currentLocale = LocaleContextHolder.getLocale();
		String locale = currentLocale.toString();
		String path = request.getSession().getServletContext().getRealPath("");
		BaseResult bs = new BaseResult();
		log.debug(ESAPIUtil.vaildLog("request Path >> {}"+path));
		String outside_source = "";
		try {
			log.trace("reqParam >> {}", reqParam);
			if(reqParam.containsKey("outside_source")) {
				outside_source = reqParam.get("outside_source");
				if(outside_source.equals("FET")) {
					outside_source = "1";
				}else {
					bs.setMessage("錯誤的來源");
					model.addAttribute(BaseResult.ERROR, bs);
					return "/online_apply/ErrorWithoutMenu";
				}
			}
		}catch(Exception e) {
			log.error("apply_digital_account error: " + e.toString());
		}
        String outside_source_session = (String)SessionUtil.getAttribute(model, SessionUtil.OUTSIDE_SOURCE, null);
        if(outside_source_session != null) {
        	outside_source = outside_source_session;
        }
		model.addAttribute("outside_source", outside_source);
		SessionUtil.addAttribute(model, SessionUtil.OUTSIDE_SOURCE, outside_source);
		if(locale.equals("zh_TW")) {
			return "/digital_account/apply_digital_account";
		}else {
			model.addAttribute("rev_url", "/DIGITAL/ACCOUNT/apply_digital_account");
			return "/online_apply/turn_to_zhTW";
		}
	}
	
	//線上開立數位存款帳戶(N203)
	@RequestMapping(value = "/apply_digital_account_p1")
	public String apply_digital_account_p1(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model, HttpSession session) {
		String path = request.getSession().getServletContext().getRealPath("");
		log.debug(ESAPIUtil.vaildLog("request Path >> {}"+path));
		String outside_source = "";
		if(reqParam.containsKey("outside_source")) {
			outside_source = reqParam.get("outside_source");
		}
		model.addAttribute("outside_source", outside_source);
		return "/digital_account/apply_digital_account_p1";
	}
	
	//線上開立數位存款帳戶(N203)
	@RequestMapping(value = "/apply_digital_account_p2")
	public String apply_digital_account_p2(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/online_apply/ErrorWithoutMenu";
		BaseResult bs = null;
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {
			String jsondc = null;
			jsondc = URLEncoder.encode(CodeUtil.toJson(okMap),"UTF-8");
			okMap.put("jsondc",jsondc);
			String outside_source = (String)SessionUtil.getAttribute(model, SessionUtil.OUTSIDE_SOURCE, null);
			model.addAttribute("outside_source", outside_source);
		}catch(Exception e) {
			log.error("apply_digital_account_p2 error >> {}", e.toString());
			
		}finally {
			model.addAttribute("result_data",bs);
			model.addAttribute("input_data",okMap);
			target = "/digital_account/apply_digital_account_p2";
		}
		return target;
	}
	
	//線上開立數位存款帳戶(N203)
	@IPVERIFY(cusidnKey="CUSIDN")
	@RequestMapping(value = "/apply_digital_account_p3")
	public String apply_digital_account_p3(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/online_apply/ErrorWithoutMenu";
		BaseResult bs = null;
		BaseResult bsTmp = new BaseResult();
		BaseResult bsN207 = null;
		String checknb = "";
		try {
			bs = new BaseResult();
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			String back = reqParam.get("back") == null ? "" : reqParam.get("back");
			log.debug(ESAPIUtil.vaildLog("back >> {}" + back));
			if (hasLocale || back.equals("Y"))
			{
				//檢測SessionUtil.ONLINE_APPLY_DATA是否已被清空
				Boolean has_session = getSessionHas(SessionUtil.ONLINE_APPLY_DATA,"CUSIDN",model,new BaseResult());
				if(has_session) {
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.ONLINE_APPLY_DATA, null));
				}else {
					bs = new BaseResult();
					bs.setErrorMessage("", i18n.getMsg("LB.X1779"));
				}
			}else {
				if(reqParam.get("FGTXWAY") != null && "6".equals(reqParam.get("FGTXWAY"))) {
					String uuid = (String)SessionUtil.getAttribute(model, SessionUtil.OTP_SECCURE_TOKEN, null);
					log.trace("uuid >> {}", uuid);
					if(uuid != null && uuid.equals(reqParam.get("OTPTOKEN"))) {
						bs.setResult(true);
					}
					if(!bs.getResult()) {
						throw new Exception(bs.getMsgCode());
					}
				}
				
				bs = digital_account_service.N203_VA_REST(reqParam);
				bs.addAllData(reqParam);
				SessionUtil.addAttribute(model, SessionUtil.ONLINE_APPLY_DATA, bs);
				if(!bs.getResult()) {
					throw new Exception(bs.getMsgCode());
				}
				
				//20211007 新增 打電文N207,成功(0000)繼續 , 失敗直接擋掉
				bsN207 = digital_account_service.N207_REST(reqParam);
				
				if(bsN207 == null || !bsN207.getResult()) {
					log.error("N207 Check Failed >> {}" ,bsN207.getData());
					bs = bsN207 ;
					return target;
				}
				
				/****************************************************************/
				bsTmp = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.ONLINE_APPLY_DATA, null));
				Map<String, Object> callData = (Map<String,Object>)bsTmp.getData();
				log.debug("callData >> {}",callData);
				bs = digital_account_service.apply_digital_account_p3((String)callData.get("FGTXWAY"), (String)callData.get("CUSIDN"));
				bs.addAllData(callData);
				bs.addData("BHID", digital_account_service.getUserBHID((String)callData.get("CUSIDN")));

				BaseResult bs2 = digital_account_service.N960_REST_UPDATE_CUSNAME(reqParam);
				if(bs2.getMsgCode().equals("E002")) {
					checknb = "N";
				}else {
					checknb = "Y";
				}
				bs.addData("checknb", checknb);
				bs.addData("LOACN", reqParam.get("LOATTIAC"));
				SessionUtil.addAttribute(model, SessionUtil.ONLINE_APPLY_DATA, bs);
			}
			model.addAttribute("today", DateUtil.getTWDate(""));
			model.addAttribute("bh_data", digital_account_service.getAllBh());
			model.addAttribute("city_data", digital_account_service.getCity());
		}catch(Exception e) {
			log.error("apply_digital_account_p3 error >> {}", e.toString());
			
		}finally {
			if(bs!=null && bs.getResult()) {
				model.addAttribute("result_data",bs);
				target = "/digital_account/apply_digital_account_p3";
			}else if(bs.getMsgCode().equals("E290") && checknb.equals("N")) {
				return apply_digital_account_nb_p1(request, response, reqParam, model);
			}else {
				bs.setNext("/DIGITAL/ACCOUNT/apply_digital_account");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	//線上開立數位存款帳戶(N203)
	@RequestMapping(value = "/apply_digital_account_p3_2")
	public String apply_digital_account_p3_2(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/online_apply/ErrorWithoutMenu";
		BaseResult bs = null;
		try {

			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			String back = reqParam.get("back") == null ? "" : reqParam.get("back");
			log.debug(ESAPIUtil.vaildLog("back >> {}" + back));
			if (hasLocale || back.equals("Y"))
			{
				Boolean has_session = getSessionHas(SessionUtil.ONLINE_APPLY_DATA,"CUSIDN",model,new BaseResult());
				if(has_session) {
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.ONLINE_APPLY_DATA, null));
				}else {
					bs = new BaseResult();
					bs.setErrorMessage("", i18n.getMsg("LB.X1779"));
				}
			}else {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.ONLINE_APPLY_DATA, null));
				bs.addAllData(reqParam);
				SessionUtil.addAttribute(model, SessionUtil.ONLINE_APPLY_DATA, bs);
			}

			model.addAttribute("today", DateUtil.getTWDate(""));
			model.addAttribute("bh_data", digital_account_service.getAllBh());
			model.addAttribute("city_data", digital_account_service.getCity());
		}catch(Exception e) {
			log.error("apply_digital_account_p3 error >> {}", e.toString());
			
		}finally {
			if(bs!=null && bs.getResult()) {
				model.addAttribute("result_data",bs);
				target = "/digital_account/apply_digital_account_p3_2";
			}else {
				bs.setNext("/DIGITAL/ACCOUNT/apply_digital_account");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	//線上開立數位存款帳戶(N203)

	//線上開立數位存款帳戶(N203)
	@RequestMapping(value = "/apply_digital_account_p4")
	public String apply_digital_account_p4(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		BaseResult bs = null;
		String target = "/online_apply/ErrorWithoutMenu";
		try {

			Boolean has_session = getSessionHas(SessionUtil.ONLINE_APPLY_DATA,"CUSIDN",model,new BaseResult());
			if(has_session) {
				// 解決Trust Boundary Violation
				Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
				// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
				boolean hasLocale = okMap.containsKey("locale");
				String back = reqParam.get("back") == null ? "" : reqParam.get("back");
				log.debug(ESAPIUtil.vaildLog("back >> {}" + back));
				if (hasLocale || back.equals("Y"))
				{
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.ONLINE_APPLY_DATA, null));
				}else {
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.ONLINE_APPLY_DATA, null));
					bs.addAllData(reqParam);
					Map<String, Object> callData = (Map<String,Object>)bs.getData();
					log.debug((String)callData.get("CUSIDN"));
					String branchId = (String)callData.get("BHID");
					log.debug("branchId>>{}",branchId);
					log.debug(ESAPIUtil.vaildLog("AdmBh>>"+digital_account_service.getAdmBhContact(branchId)));
					bs.addData("AdmBh", digital_account_service.getAdmBhContact(branchId));
					bs.addData("HASRISK", "N");
					SessionUtil.addAttribute(model, SessionUtil.ONLINE_APPLY_DATA, bs);
				}
			}else {
				bs = new BaseResult();
				bs.setErrorMessage("", i18n.getMsg("LB.X1779"));
			}
		}catch(Exception e){
			log.error(e.toString());
		}finally {
			if(bs!=null && bs.getResult()) {
				model.addAttribute("input_data", bs);
				target = "/digital_account/apply_digital_account_p4";
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
				bs.setNext("/DIGITAL/ACCOUNT/apply_digital_account");
			}
		}
		return target;
	}
	
	//線上開立數位存款帳戶(N203)
	@RequestMapping(value = "/apply_digital_account_p5")
	public String apply_digital_account_p5(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		BaseResult bs = null;
		String target = "/online_apply/ErrorWithoutMenu";
		try {
			Boolean has_session = getSessionHas(SessionUtil.ONLINE_APPLY_DATA,"CUSIDN",model,new BaseResult());
			if(has_session) {
				// 解決Trust Boundary Violation
				Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
				// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
				boolean hasLocale = okMap.containsKey("locale");
				String back = reqParam.get("back") == null ? "" : reqParam.get("back");
				log.debug(ESAPIUtil.vaildLog("back >> {}" + back));
				if (hasLocale || back.equals("Y"))
				{
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.ONLINE_APPLY_DATA, null));
				}else {
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.ONLINE_APPLY_DATA, null));
					bs.addAllData(reqParam);
					log.debug(ESAPIUtil.vaildLog("re >> {}" + CodeUtil.toJson(reqParam)));
					log.debug(ESAPIUtil.vaildLog("bs >> {}" + CodeUtil.toJson(bs.getData())));
					SessionUtil.addAttribute(model, SessionUtil.ONLINE_APPLY_DATA, bs);
				}
			}else {
				bs = new BaseResult();
				bs.setErrorMessage("", i18n.getMsg("LB.X1779"));
			}
		}catch(Exception e){
			log.error(e.toString());
		}finally {
			if(bs!=null && bs.getResult()) {
				model.addAttribute("input_data", bs);
				target = "/digital_account/apply_digital_account_p5";
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
				bs.setNext("/DIGITAL/ACCOUNT/apply_digital_account");
			}
		}
		return target;
	}
	
	//線上開立數位存款帳戶(N203)
	@RequestMapping(value = "/apply_digital_account_p6")
	public String apply_digital_account_p6(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		BaseResult bs = null;
		String target = "/online_apply/ErrorWithoutMenu";
		try {
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			bs = new BaseResult();
			log.debug(ESAPIUtil.vaildLog(CodeUtil.toJson(WebUtil.getIpAddr(request))));
			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = digital_account_service.getTxToken();
			log.trace("electricity_fee.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			if(bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			}
			reqParam.put("TOKEN", ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			String jsondc = null;
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			reqParam.put("jsondc",jsondc);
			Boolean has_session = getSessionHas(SessionUtil.ONLINE_APPLY_DATA,"CUSIDN",model,new BaseResult());
			if(has_session) {
				// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
				boolean hasLocale = okMap.containsKey("locale");
				String back = reqParam.get("back") == null ? "" : reqParam.get("back");
				log.debug(ESAPIUtil.vaildLog("back >> {}" + back));
				if (hasLocale || back.equals("Y"))
				{
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.ONLINE_APPLY_DATA, null));
				}else {
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.ONLINE_APPLY_DATA, null));
					bs.addAllData(reqParam);
					bs.addData("IP", WebUtil.getIpAddr(request));
					SessionUtil.addAttribute(model, SessionUtil.ONLINE_APPLY_DATA, bs);
				}

				String outside_source = (String)SessionUtil.getAttribute(model, SessionUtil.OUTSIDE_SOURCE, null);
				model.addAttribute("outside_source", outside_source);
			}else {
				bs = new BaseResult();
				bs.setErrorMessage("", i18n.getMsg("LB.X1779"));
			}
		}catch(Exception e){
			log.error(e.toString());
		}finally {
			if(bs!=null && bs.getResult()) {
				model.addAttribute("input_data", bs);
				target = "/digital_account/apply_digital_account_p6";
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
				bs.setNext("/DIGITAL/ACCOUNT/apply_digital_account");
			}
		}
		return target;
	}
	
	//線上開立數位存款帳戶(N203)
	@RequestMapping(value = "/apply_digital_account_result")
	public String apply_digital_account_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		BaseResult bs = null;
		String target = "/online_apply/ErrorWithoutMenu";
		Map<String, String> okMap = null;
		try {

			bs = new BaseResult();
            okMap = ESAPIUtil.validStrMap(reqParam);
			//okMap = reqParam;
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				okMap = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.N201_CONFIRM_DATA, null);
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				log.trace("apply_deposit_account_result.validate TXTOKEN...");
				log.trace("apply_deposit_account_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("other_telecom_fee_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
						okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("other_telecom_fee_result.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("other_telecom_fee_result TXTOKEN 不正確，TXTOKEN>>{}"+ okMap.get("TOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
						!okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("other_telecom_fee_result.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("other_telecom_fee_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.ONLINE_APPLY_DATA, null));
				reqParam.put("HLOGINPIN", (String)((Map<String,Object>)bs.getData()).get("HLOGINPIN"));
				reqParam.put("HTRANSPIN", (String)((Map<String,Object>)bs.getData()).get("HTRANSPIN"));
				bs.reset();
				
				log.debug(ESAPIUtil.vaildLog("reqParam >> {}" + CodeUtil.toJson(reqParam)));
				String separ = File.separator;
				String uploadPath_N201 = context.getRealPath("") + separ +"com" + separ + "updateBytes" + separ;
				bs = digital_account_service.apply_deposit_account_result(reqParam, uploadPath_N201);
				if(bs.getResult()) {

					Map<String, String> sesstiondata = (Map<String,String>)SessionUtil.getAttribute(model, SessionUtil.UPDATE_IMAGE_DATA, null);
					if(sesstiondata == null) {
						sesstiondata = new HashMap<String,String>();
					}
					if(!reqParam.get("FILE1").equals("") && sesstiondata.containsKey(reqParam.get("FILE1"))) {
						String randomName = sesstiondata.get(reqParam.get("FILE1"));
						String result = update_file_service.readBinryFile(uploadPath_N201, randomName);
						if(!result.equals("fail")) {
							String filetype = reqParam.get("FILE1").substring(reqParam.get("FILE1").lastIndexOf(".") ,reqParam.get("FILE1").length());
							update_file_service.uploadImage(result, reqParam.get("FILE1"));
							update_file_service.movefile(result, SpringBeanFactory.getBean("imgMovePath") + separ, reqParam.get("UID") + "_01_" + DateUtil.getDate("") + filetype);
							update_file_service.removefile(uploadPath_N201, randomName);
						}
					}
					if(!reqParam.get("FILE2").equals("") && sesstiondata.containsKey(reqParam.get("FILE2"))) {
						String randomName = sesstiondata.get(reqParam.get("FILE2"));
						String result = update_file_service.readBinryFile(uploadPath_N201, randomName);
						if(!result.equals("fail")) {
							String filetype = reqParam.get("FILE2").substring(reqParam.get("FILE2").lastIndexOf(".") ,reqParam.get("FILE2").length());
							update_file_service.uploadImage(result, reqParam.get("FILE2"));
							update_file_service.movefile(result, SpringBeanFactory.getBean("imgMovePath") + separ, reqParam.get("UID") + "_02_" + DateUtil.getDate("") + filetype);
							update_file_service.removefile(uploadPath_N201, randomName);
						}
					}
					if(!reqParam.get("FILE3").equals("") && sesstiondata.containsKey(reqParam.get("FILE3"))) {
						String randomName = sesstiondata.get(reqParam.get("FILE3"));
						String result = update_file_service.readBinryFile(uploadPath_N201, randomName);
						if(!result.equals("fail")) {
							String filetype = reqParam.get("FILE1").substring(reqParam.get("FILE3").lastIndexOf(".") ,reqParam.get("FILE3").length());
							update_file_service.uploadImage(result, reqParam.get("FILE3"));
							update_file_service.movefile(result, SpringBeanFactory.getBean("imgMovePath") + separ, reqParam.get("UID") + "_03_" + DateUtil.getDate("") + filetype);
							update_file_service.removefile(uploadPath_N201, randomName);
						}
					}
					if(!reqParam.get("FILE4").equals("") && sesstiondata.containsKey(reqParam.get("FILE4"))) {
						String randomName = sesstiondata.get(reqParam.get("FILE4"));
						String result = update_file_service.readBinryFile(uploadPath_N201, randomName);
						if(!result.equals("fail")) {
							String filetype = reqParam.get("FILE4").substring(reqParam.get("FILE4").lastIndexOf(".") ,reqParam.get("FILE4").length());
							update_file_service.uploadImage(result, reqParam.get("FILE4"));
							update_file_service.movefile(result, SpringBeanFactory.getBean("imgMovePath") + separ, reqParam.get("UID") + "_04_" + DateUtil.getDate("") + filetype);
							update_file_service.removefile(uploadPath_N201, randomName);
						}
					}
					if((reqParam.get("FILE1").equals("") || !sesstiondata.containsKey(reqParam.get("FILE1"))) || (reqParam.get("FILE2").equals("") || !sesstiondata.containsKey(reqParam.get("FILE2"))) || 
							(reqParam.get("FILE3").equals("") || !sesstiondata.containsKey(reqParam.get("FILE3"))) || (reqParam.get("FILE4").equals("") || !sesstiondata.containsKey(reqParam.get("FILE4")))) {
						bs.addData("chkfile", "N");
					}
				}

				
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.N201_CONFIRM_DATA, okMap);
			}
		}catch(Exception e){
			log.error(e.toString());
		}finally{
			if(bs != null && bs.getResult()) {
				target = "/digital_account/apply_digital_account_result";
				model.addAttribute("result_data", bs);
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
				bs.setNext("/DIGITAL/ACCOUNT/apply_digital_account");
			}
		}
		return target;
	}
	

	
	//線上開立數位存款帳戶(N203)
	@IPVERIFY(cusidnKey="CUSIDN")
	@RequestMapping(value = "/apply_digital_account_nb_p1")
	public String apply_digital_account_nb_p1(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/online_apply/ErrorWithoutMenu";
		BaseResult bs = null;
		String checknb = "";
		try {
			bs = new BaseResult();
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			String back = reqParam.get("back") == null ? "" : reqParam.get("back");
			log.debug(ESAPIUtil.vaildLog("back >> {}" + back));
			if (hasLocale || back.equals("Y"))
			{
				//檢測SessionUtil.ONLINE_APPLY_DATA是否已被清空
				Boolean has_session = getSessionHas(SessionUtil.ONLINE_APPLY_DATA,"CUSIDN",model,new BaseResult());
				if(has_session) {
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.ONLINE_APPLY_DATA, null));
				}else {
					bs = new BaseResult();
					bs.setErrorMessage("", i18n.getMsg("LB.X1779"));
				}
			}else {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.ONLINE_APPLY_DATA, null));
				String cusidn = (String)((Map<String, Object>)bs.getData()).get("CUSIDN");
				TXNACNAPPLY po = txnAcnApplyDao.findByCUSIDNOrderByDateAndTimeDESC(cusidn).get(0);
				bs.addData("BIRTHDAY", po.getBIRTHDAY());
				bs.addData("MAILADDR", po.getMAILADDR());
				SessionUtil.addAttribute(model, SessionUtil.ONLINE_APPLY_DATA, bs);
				
				bs.setResult(true);
			} 
		}catch(Exception e) {
			log.error("apply_digital_account_nb_p1 error >> {}", e.toString());
			
		}finally {
			if(bs!=null && bs.getResult()) {
				model.addAttribute("result_data",bs);
				target = "/digital_account/apply_digital_account_nb_p1";
			}else {
				bs.setNext("/DIGITAL/ACCOUNT/apply_digital_account");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	//線上開立數位存款帳戶(N203)
	@RequestMapping(value = "/apply_digital_account_nb_p2")
	public String apply_digital_account_nb_p2(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {

		BaseResult bs = null;
		String target = "/online_apply/ErrorWithoutMenu";
		try {
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			bs = new BaseResult();
			log.debug(ESAPIUtil.vaildLog(CodeUtil.toJson(WebUtil.getIpAddr(request))));
			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = digital_account_service.getTxToken();
			log.trace("electricity_fee.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			if(bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			}
			reqParam.put("TOKEN", ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			String jsondc = null;
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			reqParam.put("jsondc",jsondc);
			Boolean has_session = getSessionHas(SessionUtil.ONLINE_APPLY_DATA,"CUSIDN",model,new BaseResult());
			if(has_session) {
				// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
				boolean hasLocale = okMap.containsKey("locale");
				String back = reqParam.get("back") == null ? "" : reqParam.get("back");
				log.debug(ESAPIUtil.vaildLog("back >> {}" + back));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.ONLINE_APPLY_DATA, null));
				bs.addAllData(reqParam);
				bs.setMsgCode("0");
				bs.setResult(true);
				SessionUtil.addAttribute(model, SessionUtil.ONLINE_APPLY_DATA, bs);
			}else {
				bs = new BaseResult();
				bs.setErrorMessage("", i18n.getMsg("LB.X1779"));
			}
		}catch(Exception e){
			log.error(e.toString());
		}finally {
			if(bs!=null && bs.getResult()) {
				model.addAttribute("input_data", bs);
				target = "/digital_account/apply_digital_account_nb_p2";
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
				bs.setNext("/DIGITAL/ACCOUNT/apply_digital_account");
			}
		}
		return target;
	
	}

	//線上開立數位存款帳戶(N203)
	@RequestMapping(value = "/apply_digital_account_nb_result")
	public String apply_digital_account_nb_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		BaseResult bs = null;
		String target = "/online_apply/ErrorWithoutMenu";
		Map<String, String> okMap = null;
		try {

			bs = new BaseResult();
            log.trace("apply_digital_account_nb_result reqParam >> {}", CodeUtil.toJson(reqParam));
            okMap = ESAPIUtil.validStrMap(reqParam);
            log.trace("apply_digital_account_nb_result okMap >> {}", CodeUtil.toJson(okMap));
			log.trace("apply_digital_account_nb_result.validate TXTOKEN...");
			log.trace("apply_digital_account_nb_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
			log.trace("apply_digital_account_nb_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
			
			// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
			if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
					okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
				// TXTOKEN第一階段驗證成功
				bs.setResult(Boolean.TRUE);
				log.trace("apply_digital_account_nb_result.bs.step1 is successful...");
			}
			if(!bs.getResult()) {
				log.error(ESAPIUtil.vaildLog("apply_digital_account_nb_result TXTOKEN 不正確，TXTOKEN>>{}"+ okMap.get("TOKEN")));
				throw new Exception();
			}
			// 重置bs，繼續往下驗證
			bs.reset();
			
			// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
			if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
					!okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
				// TXTOKEN第二階段驗證成功
				bs.setResult(Boolean.TRUE);
				log.trace("apply_digital_account_nb_result.bs.step2 is successful...");
			}
			if(!bs.getResult()) {
				log.error(ESAPIUtil.vaildLog("apply_digital_account_nb_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TOKEN")));
				throw new Exception();
			}
			// 重置bs，準備進行交易
			bs.reset();
			bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.ONLINE_APPLY_DATA, null));
			okMap.put("HLOGINPIN", (String)((Map<String,Object>)bs.getData()).get("HLOGINPIN"));
			okMap.put("HTRANSPIN", (String)((Map<String,Object>)bs.getData()).get("HTRANSPIN"));
			bs.reset();
			okMap.put("IP", WebUtil.getIpAddr(request));
			bs = digital_account_service.apply_digital_account_nb_result(okMap);
		}catch(Exception e){
			log.error(e.toString());
		}finally{
			if(bs != null && bs.getResult()) {
				target = "/digital_account/apply_digital_account_nb_result";
				model.addAttribute("result_data", bs);
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
				bs.setNext("/DIGITAL/ACCOUNT/apply_digital_account");
			}
		}
		return target;
	}
	
	//線上數位存款帳戶補上傳身份證件(N204)
	@RequestMapping(value = "/upload_digital_identity")
	public String upload_digital_identity(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model, HttpSession session) {
		return "/digital_account/upload_digital_identity";
	}
	
	//線上數位存款帳戶補上傳身份證件(N204)
	@IPVERIFY(cusidnKey="CUSIDN")
	@RequestMapping(value = "/upload_digital_identity_p1")
	public String upload_digital_identity_p1(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model, HttpSession session) {
		String target = "/online_apply/ErrorWithoutMenu";
		String path = request.getSession().getServletContext().getRealPath("");
		log.debug(ESAPIUtil.vaildLog("request Path >> {}"+path));
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// 解決Trust Boundary Violation  
            Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);  
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			String back = reqParam.get("back") == null ? "" : reqParam.get("back");
			log.debug(ESAPIUtil.vaildLog("back >> {}" + back));
			if (hasLocale || back.equals("Y")){
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}else {
	            // 使用者統編加密後存取 
//	            SessionUtil.addAttribute(model, SessionUtil.CUSIDN,Base64.getEncoder().encodeToString(okMap.get("CUSIDN").toUpperCase().getBytes())); 
	
				// IKEY要使用的JSON:DC
				String jsondc = null;
				jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
				bs = digital_account_service.upload_digital_identity_2041(reqParam);
				bs.addData("jsondc", jsondc);
				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, bs);
			}
		}catch(Exception e) {
			log.error("upload_digital_identity_p1 error >> {}", e.toString());
		}finally {
			if(bs != null && bs.getResult()) {
				target = "/digital_account/upload_digital_identity_p1";
				model.addAttribute("result_data", bs);
			}else {
				bs.setPrevious("/DIGITAL/ACCOUNT/upload_digital_identity");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	//線上數位存款帳戶補上傳身份證件(N204)
	@RequestMapping(value = "/upload_digital_identity_p2")
	public String upload_digital_identity_p2(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model, HttpSession session) {
		String target = "/online_apply/ErrorWithoutMenu";
		String path = request.getSession().getServletContext().getRealPath("");
		log.debug(ESAPIUtil.vaildLog("request Path >> {}"+path));
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// 解決Trust Boundary Violation  
            Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);  
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			String back = reqParam.get("back") == null ? "" : reqParam.get("back");
			log.debug(ESAPIUtil.vaildLog("back >> {}" + back));
			if (hasLocale || back.equals("Y")){
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.ONLINE_APPLY_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}else {
				if(reqParam.get("FGTXWAY") != null && "6".equals(reqParam.get("FGTXWAY"))) {
					String uuid = (String)SessionUtil.getAttribute(model, SessionUtil.OTP_SECCURE_TOKEN, null);
					log.trace("uuid >> {}", uuid);
					if(uuid != null && uuid.equals(reqParam.get("OTPTOKEN"))) {
						bs.setResult(true);
					}
					if(!bs.getResult()) {
						throw new Exception(bs.getMsgCode());
					}
				}
				
				bs = digital_account_service.upload_digital_identity_2042(reqParam);
				SessionUtil.addAttribute(model, SessionUtil.ONLINE_APPLY_DATA, bs);
			}
		}catch(Exception e) {
			log.error("upload_digital_identity_p1 error >> {}", e.toString());
		}finally {
			if(bs != null && bs.getResult()) {
				target = "/digital_account/upload_digital_identity_p2";
				model.addAttribute("result_data", bs);
			}else {
				bs.setNext("/DIGITAL/ACCOUNT/upload_digital_identity");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	//線上數位存款帳戶補上傳身份證件(N204)
	@RequestMapping(value = "/upload_digital_identity_p3")
	public String upload_digital_identity_p3(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		BaseResult bs = null;
		String target = "/online_apply/ErrorWithoutMenu";
		try {
			Boolean has_session = getSessionHas(SessionUtil.ONLINE_APPLY_DATA,"CUSIDN",model,new BaseResult());
			if(has_session) {
				bs = new BaseResult();
				// 解決Trust Boundary Violation  
	            Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);  
				// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
				boolean hasLocale = okMap.containsKey("locale");
				String back = reqParam.get("back") == null ? "" : reqParam.get("back");
				log.debug(ESAPIUtil.vaildLog("back >> {}" + back));
				if (hasLocale || back.equals("Y")){
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.ONLINE_APPLY_DATA, null));
					if (!bs.getResult()) {
						log.trace("bs.getResult() >>{}", bs.getResult());
						throw new Exception();
					}
				}else {
					// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
					bs = digital_account_service.getTxToken();
					log.trace("electricity_fee.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
					if(bs.getResult()) {
						SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
					}
					reqParam.put("TOKEN", ((Map<String,String>) bs.getData()).get("TXTOKEN"));
					String jsondc = null;
					jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
					reqParam.put("jsondc",jsondc);
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.ONLINE_APPLY_DATA, null));
					bs.addAllData(reqParam);
//					log.debug(WebUtil.getIpAddr(request));
					bs.addData("IP", WebUtil.getIpAddr(request));
					SessionUtil.addAttribute(model, SessionUtil.ONLINE_APPLY_DATA, bs);
				}
			}else {
				bs = new BaseResult();
				bs.setErrorMessage("", i18n.getMsg("LB.X1779"));
			}
		}catch(Exception e){
			log.error(e.toString());
			model.addAttribute(BaseResult.ERROR, bs);
		}finally {
			if(bs != null && bs.getResult()) {
				target = "/digital_account/upload_digital_identity_p3";
				model.addAttribute("input_data", bs);
			}else {
				bs.setNext("/DIGITAL/ACCOUNT/upload_digital_identity");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	//線上數位存款帳戶補上傳身份證件(N204)
	@RequestMapping(value = "/upload_digital_identity_result")
	public String upload_digital_identity_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		BaseResult bs = null;
		String target = "/online_apply/ErrorWithoutMenu";
		Map<String, String> okMap = null;
		try {

			bs = new BaseResult();
    		okMap = ESAPIUtil.validStrMap(reqParam);
			//okMap = reqParam;
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				okMap = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.N201_CONFIRM_DATA, null);
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				log.trace("upload_digital_identity_result.validate TXTOKEN...");
				log.trace("upload_digital_identity_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("upload_digital_identity_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
						okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("upload_digital_identity_result.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("upload_digital_identity_result TXTOKEN 不正確，TXTOKEN>>{}" + okMap.get("TOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
						!okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("upload_digital_identity_result.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("upload_digital_identity_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}" + okMap.get("TOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				log.debug(ESAPIUtil.vaildLog("reqParam >> {}"+CodeUtil.toJson(reqParam)));
				String separ = File.separator;
				String uploadPath_N201 = context.getRealPath("") + separ +"com" + separ + "updateBytes" + separ;
				bs = digital_account_service.upload_digital_identity_result(reqParam, uploadPath_N201);
				if(bs.getResult()) {

					Map<String, String> sesstiondata = (Map<String,String>)SessionUtil.getAttribute(model, SessionUtil.UPDATE_IMAGE_DATA, null);
					if(sesstiondata == null) {
						sesstiondata = new HashMap<String,String>();
					}
					if(!reqParam.get("FILE1").equals("") && sesstiondata.containsKey(reqParam.get("FILE1"))) {
						String randomName = sesstiondata.get(reqParam.get("FILE1"));
						String result = update_file_service.readBinryFile(uploadPath_N201, randomName);
						if(!result.equals("fail")) {
							String filetype = reqParam.get("FILE1").substring(reqParam.get("FILE1").lastIndexOf(".") ,reqParam.get("FILE1").length());
							update_file_service.uploadImage(result, reqParam.get("FILE1"));
							update_file_service.movefile(result, SpringBeanFactory.getBean("imgMovePath") + separ, reqParam.get("CUSIDN") + "_01_" + DateUtil.getDate("") + filetype);
							update_file_service.removefile(uploadPath_N201, randomName);
						}
					}
					if(!reqParam.get("FILE2").equals("") && sesstiondata.containsKey(reqParam.get("FILE2"))) {
						String randomName = sesstiondata.get(reqParam.get("FILE2"));
						String result = update_file_service.readBinryFile(uploadPath_N201, randomName);
						if(!result.equals("fail")) {
							String filetype = reqParam.get("FILE2").substring(reqParam.get("FILE2").lastIndexOf(".") ,reqParam.get("FILE2").length());
							update_file_service.uploadImage(result, reqParam.get("FILE2"));
							update_file_service.movefile(result, SpringBeanFactory.getBean("imgMovePath") + separ, reqParam.get("CUSIDN") + "_02_" + DateUtil.getDate("") + filetype);
							update_file_service.removefile(uploadPath_N201, randomName);
						}
					}
					if(!reqParam.get("FILE3").equals("") && sesstiondata.containsKey(reqParam.get("FILE3"))) {
						String randomName = sesstiondata.get(reqParam.get("FILE3"));
						String result = update_file_service.readBinryFile(uploadPath_N201, randomName);
						if(!result.equals("fail")) {
							String filetype = reqParam.get("FILE1").substring(reqParam.get("FILE3").lastIndexOf(".") ,reqParam.get("FILE3").length());
							update_file_service.uploadImage(result, reqParam.get("FILE3"));
							update_file_service.movefile(result, SpringBeanFactory.getBean("imgMovePath") + separ, reqParam.get("CUSIDN") + "_03_" + DateUtil.getDate("") + filetype);
							update_file_service.removefile(uploadPath_N201, randomName);
						}
					}
					if(!reqParam.get("FILE4").equals("") && sesstiondata.containsKey(reqParam.get("FILE4"))) {
						String randomName = sesstiondata.get(reqParam.get("FILE4"));
						String result = update_file_service.readBinryFile(uploadPath_N201, randomName);
						if(!result.equals("fail")) {
							String filetype = reqParam.get("FILE4").substring(reqParam.get("FILE4").lastIndexOf(".") ,reqParam.get("FILE4").length());
							update_file_service.uploadImage(result, reqParam.get("FILE4"));
							update_file_service.movefile(result, SpringBeanFactory.getBean("imgMovePath") + separ, reqParam.get("CUSIDN") + "_04_" + DateUtil.getDate("") + filetype);
							update_file_service.removefile(uploadPath_N201, randomName);
						}
					}
				}
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.N201_CONFIRM_DATA, okMap);
			}
		}catch(Exception e){
			log.error(e.toString());
		}finally{
			if(bs != null && bs.getResult()) {
				target = "/digital_account/upload_digital_identity_result";
				model.addAttribute("result_data", bs);
			}else {
				bs.setNext("/DIGITAL/ACCOUNT/upload_digital_identity");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	//修改數位存款帳戶資料(N104)
	@RequestMapping(value = "/modify_digital_data")
	public String modify_digital_data(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model, HttpSession session) {
		return "/digital_account/modify_digital_data";
	}
	
	//修改數位存款帳戶資料(N104)
	@IPVERIFY(cusidnKey="CUSIDN")
	@RequestMapping(value = "/modify_digital_data_p1")
	public String modify_digital_data_p1(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model, HttpSession session) {
		String target = "/online_apply/ErrorWithoutMenu";
		String path = request.getSession().getServletContext().getRealPath("");
		log.debug(ESAPIUtil.vaildLog("request Path >> {}"+path));
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// 解決Trust Boundary Violation 
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam); 
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			String back = reqParam.get("back") == null ? "" : reqParam.get("back");
			log.debug(ESAPIUtil.vaildLog("back >> {}"+back));
			if (hasLocale || back.equals("Y")){
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}else {
				// 使用者統編加密後存取
//				SessionUtil.addAttribute(model, SessionUtil.CUSIDN,
//						Base64.getEncoder().encodeToString(okMap.get("CUSIDN").toUpperCase().getBytes()));
				// IKEY要使用的JSON:DC
				String jsondc = null;
				jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
				bs = digital_account_service.modify_digital_data_p1(reqParam);
				bs.addData("jsondc", jsondc);
				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, bs);
			}
		}catch(Exception e) {
			log.error("modify_digital_data_p1 error >> {}", e.toString());
		}finally {
			if(bs != null && bs.getResult()) {
				target = "/digital_account/modify_digital_data_p1";
				model.addAttribute("result_data", bs);
			}else {
				bs.setPrevious("/DIGITAL/ACCOUNT/modify_digital_data");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	//修改數位存款帳戶資料(N104)
	@RequestMapping(value = "/modify_digital_data_p2")
	public String modify_digital_data_p2(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model, HttpSession session) {
		String target = "/online_apply/ErrorWithoutMenu";
		String path = request.getSession().getServletContext().getRealPath("");
		log.debug(ESAPIUtil.vaildLog("request Path >> {}"+path));
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// 解決Trust Boundary Violation 
//			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam); 
			Map<String, String> okMap = reqParam; 
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			String back = reqParam.get("back") == null ? "" : reqParam.get("back");
			log.debug(ESAPIUtil.vaildLog("back >> {}" + back));
			if (hasLocale || back.equals("Y")){
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP2_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}else {
				if(reqParam.get("FGTXWAY") != null && "6".equals(reqParam.get("FGTXWAY"))) {
					String uuid = (String)SessionUtil.getAttribute(model, SessionUtil.OTP_SECCURE_TOKEN, null);
					log.trace("uuid >> {}", uuid);
					if(uuid != null && uuid.equals(reqParam.get("OTPTOKEN"))) {
						bs.setResult(true);
					}
					if(!bs.getResult()) {
						throw new Exception(bs.getMsgCode());
					}
				}
				
				bs = digital_account_service.modify_digital_data_p2(reqParam);
				bs.addData("BHID", digital_account_service.getUserBHID(reqParam.get("CUSIDN")));
				SessionUtil.addAttribute(model, SessionUtil.STEP2_LOCALE_DATA, bs);
			}
			model.addAttribute("bh_data", digital_account_service.getAllBh());
			model.addAttribute("city_data", digital_account_service.getCity());
			model.addAttribute("today", DateUtil.getTWDate(""));
		}catch(Exception e) {
			log.error("modify_digital_data_p2 error >> {}", e.toString());
		}finally {
			if(bs != null && bs.getResult()) {
				target = "/digital_account/modify_digital_data_p2";
				model.addAttribute("result_data", bs);
			}else {
				bs.setNext("/DIGITAL/ACCOUNT/modify_digital_data");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	@RequestMapping(value = "/modify_digital_data_p2_2")
	public String modify_digital_data_p2_2(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/online_apply/ErrorWithoutMenu";
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// 解決Trust Boundary Violation
//			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			Map<String, String> okMap = reqParam; 
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			String back = reqParam.get("back") == null ? "" : reqParam.get("back");
			log.debug(ESAPIUtil.vaildLog("back >> {}" + back));
			if (hasLocale || back.equals("Y"))
			{
				Boolean has_session = getSessionHas(SessionUtil.STEP2_LOCALE_DATA,"CUSIDN",model,new BaseResult());
				if(has_session) {
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP2_LOCALE_DATA, null));
				}else {
					bs = new BaseResult();
					bs.setErrorMessage("", i18n.getMsg("LB.X1779"));
				}
			}else {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP2_LOCALE_DATA, null));
				bs.addAllData(reqParam);
				SessionUtil.addAttribute(model, SessionUtil.STEP2_LOCALE_DATA, bs);
			}

			model.addAttribute("today", DateUtil.getTWDate(""));
			model.addAttribute("bh_data", digital_account_service.getAllBh());
			model.addAttribute("city_data", digital_account_service.getCity());
		}catch(Exception e) {
			log.error("modify_digital_data_p2 error >> {}", e.toString());
			
		}finally {
			if(bs!=null && bs.getResult()) {
				model.addAttribute("result_data",bs);
				target = "/digital_account/modify_digital_data_p2_2";
			}else {
				bs.setNext("/DIGITAL/ACCOUNT/modify_digital_data");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	//修改數位存款帳戶資料(N104)
	@RequestMapping(value = "/modify_digital_data_p3")
	public String modify_digital_data_p3(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		BaseResult bs = null;
		String target = "/online_apply/ErrorWithoutMenu";
		try {
			bs = new BaseResult();
			// 解決Trust Boundary Violation 
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam); 
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			String back = reqParam.get("back") == null ? "" : reqParam.get("back");
			log.debug(ESAPIUtil.vaildLog("back >> {}"+back));
			if (hasLocale || back.equals("Y")){
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP2_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}else {
				log.debug(ESAPIUtil.vaildLog(CodeUtil.toJson(WebUtil.getIpAddr(request))));
				// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP2_LOCALE_DATA, null));
				bs.addAllData((Map<String,String>)digital_account_service.getTxToken().getData());
				
				log.trace("modify_digital_data_p3 bs>>"+CodeUtil.toJson(bs) );
				log.trace("electricity_fee.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
				if(bs.getResult()) {
					SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
				}
				reqParam.put("TOKEN", ((Map<String,String>) bs.getData()).get("TXTOKEN"));
				String jsondc = null;
				jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
				reqParam.put("jsondc",jsondc);
				bs.addAllData(reqParam);
				bs.addData("IP",  WebUtil.getIpAddr(request));
				//SessionUtil.addAttribute(model, SessionUtil.STEP2_LOCALE_DATA, bs);
				
			}
		}catch(Exception e){
			log.error(e.toString());
//			model.addAttribute(BaseResult.ERROR, bs);
		}finally {
			if(bs != null && bs.getResult()) {
				target = "/digital_account/modify_digital_data_p3";
				model.addAttribute("input_data", bs);
			}else {
				bs.setNext("/DIGITAL/ACCOUNT/modify_digital_data");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	//修改數位存款帳戶資料(N104)
	@RequestMapping(value = "/modify_digital_data_result")
	public String modify_digital_data_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		BaseResult bs = null;
		String target = "/online_apply/ErrorWithoutMenu";
		Map<String, String> okMap = null;
		try {

			bs = new BaseResult();
            okMap = ESAPIUtil.validStrMap(reqParam);
			//okMap = reqParam;
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				okMap = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.N201_CONFIRM_DATA, null);
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				log.trace("apply_deposit_account_result.validate TXTOKEN...");
				log.trace("apply_deposit_account_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("other_telecom_fee_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
						okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("other_telecom_fee_result.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("other_telecom_fee_result TXTOKEN 不正確，TXTOKEN>>{}" + okMap.get("TOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
						!okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("other_telecom_fee_result.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("other_telecom_fee_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				log.debug(ESAPIUtil.vaildLog("reqParam >> {}"+CodeUtil.toJson(reqParam)));
				bs = digital_account_service.modify_digital_data_result(reqParam);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.N201_CONFIRM_DATA, okMap);
			}
		}catch(Exception e){
			log.error(e.toString());
		}finally{
			if(bs != null && bs.getResult()) {
				target = "/digital_account/modify_digital_data_result";
				model.addAttribute("result_data", bs);
			}else {
				bs.setNext("/DIGITAL/ACCOUNT/modify_digital_data");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	
	/**
	 * 檢測session是否還存在
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	public Boolean getSessionHas(String sessionName, String checkColName, Model model) {
        Boolean flag = Boolean.FALSE;
		try {
			Map<String,String> inputData = (Map<String,String>)SessionUtil.getAttribute(model, sessionName, null);
	        if(inputData != null) {
	        	if(inputData.get(checkColName) != null) {
		        	if(!inputData.get(checkColName).equals("")) {
		        		flag = Boolean.TRUE;
		        	}
	        	}
	        }
		}catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getSessionHas error >> {}",e);
		}
		return flag;
	}
	
	/**
	 * 檢測session是否還存在
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	public Boolean getSessionHas(String sessionName, String checkColName, Model model, BaseResult inputData) {
        Boolean flag = Boolean.FALSE;
		try {
	        inputData = WebUtil.toBs(SessionUtil.getAttribute(model, sessionName, null));
	        if(inputData != null) {
	        	Map<String,Object> callTable = (Map<String,Object>)inputData.getData();
	        	if(callTable.get(checkColName) != null) {
		        	if(!((String)callTable.get(checkColName)).equals("")) {
		        		flag = Boolean.TRUE;
		        	}
	        	}
	        }
		}catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getSessionHas error >> {}",e);
		}
		return flag;
	}
	
	@PostMapping("/apply_deposit_branch")
	public @ResponseBody BaseResult apply_deposit_branch(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		String bid = okMap.get("zip");
		BaseResult bs = new BaseResult();
		bs.addData("AdmBh", digital_account_service.getAdmBhContact(bid));
		model.addAttribute("bs",bs);
		return bs;
	}
	@PostMapping("/verify_lo_account_aj")
	public @ResponseBody BaseResult verify_lo_account_aj(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		BaseResult bs = new BaseResult();
		try {
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			bs = digital_account_service.verify_lo_account_aj(okMap);
			if(!bs.getResult()) {
				bs.setData("");
			}
		}catch(Exception e) {
			log.error("verify_lo_account_aj error >> {}", e);
		}
		return bs;
	}
	@PostMapping("/otpverify_lo_account_aj")
	public @ResponseBody BaseResult otpverify_lo_account_aj(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		BaseResult bs = new BaseResult();
		try {
			//本行帳戶驗證
			bs = digital_account_service.validate_sms_otp(reqParam);
			if(bs.getResult()) {
				String uuid = CodeUtil.generateUniqueId();
				SessionUtil.addAttribute(model, SessionUtil.OTP_SECCURE_TOKEN, uuid);
				bs.addData("otptoken", uuid);
			}
		}catch(Exception e) {
			log.error("verify_lo_account_aj error >> {}", e);
		}
		return bs;
	}
	
}
