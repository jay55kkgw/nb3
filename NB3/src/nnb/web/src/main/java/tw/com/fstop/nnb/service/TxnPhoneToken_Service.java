package tw.com.fstop.nnb.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.tbb.nnb.dao.TxnPhoneTokenDao;
import tw.com.fstop.tbb.nnb.dao.TxnTwSchPayDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.util.StrUtil;
import fstop.orm.po.TXNPHONETOKEN;
import fstop.orm.po.TXNTWRECORD;
import fstop.orm.po.TXNTWSCHPAY;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class TxnPhoneToken_Service {
	private Logger log = LoggerFactory.getLogger(this.getClass().getName());

	@Autowired
	private TxnPhoneTokenDao txnPhoneTokenDao;
	
	/*
	 * 以顧客統編(dpuserid)當作條件去查詢DB，回傳結果
	 */
	public BaseResult query(Map map) {
		BaseResult bs = new BaseResult();
		List<TXNPHONETOKEN> list = null;
		
		try {
//			Field[] fs = TXNPHONETOKEN.class.getDeclaredFields();
// 			for(Field f : fs) {
// 				if(!("serialVersionUID".equals(f.getName()))) {
// 					if(reqParam.get(f.getName()) != null) {
// 						reqParam.put(f.getName(), "");
// 					}
// 					listName.add(f.getName());
// 				}
// 			}
//			Map map = new HashMap();
//	        Enumeration enu = (Enumeration)req.getParameterNames();
//	        while (enu.hasMoreElements()) {
//	            String var = (String)enu.nextElement();
//	            map.put(var.toUpperCase(), req.getParameter(var));
//	        }
//			
			
			Object dpuseridObj = map.get("dpuserid");
			if(dpuseridObj == null) {
				bs.setResult(Boolean.FALSE);
				bs.setMessage("1","身分證未輸入");
				return bs;
			}
			
			String dpuserid = (String)dpuseridObj;
			if(dpuserid.trim().length() == 0) {
				bs.setResult(Boolean.FALSE);
				bs.setMessage("1","身分證未輸入");
				return bs;
			}
			
			
			list = txnPhoneTokenDao.getByEqual(map);
			
			bs.setData(list);
			bs.setResult(Boolean.TRUE);
			bs.setMessage("0", "查詢成功");
			log.info("TwSchPay.list.size >> {}", list.size());
		} catch (Exception e) {
			log.error("{}", e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", "查詢異常");
		}
		return bs;
	}

	/*
	 * 新增推播設定進去DB，回傳結果
	 */
	public BaseResult add(Map<String, String> reqParam) {
		BaseResult bs = new BaseResult();
		TXNPHONETOKEN txnPhoneToken = new TXNPHONETOKEN();
		//簡易日期格式
		SimpleDateFormat sdfD = new SimpleDateFormat("yyyyMMdd");
		//簡易時間格式
		SimpleDateFormat sdfT = new SimpleDateFormat("hhmmss");
		String lastdate = sdfD.format(new Date());
		String lasttime = sdfT.format(new Date());
		try {
			String dpuserid = reqParam.get("dpuserid");
			String phonetoken = reqParam.get("phonetoken");
			String phonetype = reqParam.get("phonetype");
			//檢查PHONETYPE是否為空值
			if(phonetoken == null ||phonetoken.trim().length() == 0) {
				bs.setResult(Boolean.FALSE);
				bs.setMessage("1","phonetoken未輸入");
				return bs;
			}
			//檢查(PHONETYPE)是否輸入A(Android)或I(Iphone)
			if(!("A".equals(reqParam.get("phonetype"))||"I".equals(reqParam.get("phonetype")))) {
				bs.setResult(Boolean.FALSE);
				bs.setMessage("1","phonetype手機OS輸入錯誤");
				return bs;
			}
			//檢查統編(dpuserid)是否為空值
			if((dpuserid == null ||dpuserid.trim().length() == 0)&&(phonetoken == null ||phonetoken.trim().length() == 0)) {
				bs.setResult(Boolean.FALSE);
				bs.setMessage("1","統編或電話說明不可都未輸入");
				return bs;
			}
			//檢查phonetoken是否有人持有，若有則更新phonetoken持有人
			if(txnPhoneTokenDao.getByPhonetoken(phonetoken).size()>0) {
				TXNPHONETOKEN old = txnPhoneTokenDao.getByPhonetoken(phonetoken).get(0);
				if(!old.getDPUSERID().equals(dpuserid)) {
					old.setPHONETYPE("");
					old.setPHONETOKEN("");
					old.setLASTDATE(lastdate);
					old.setLASTTIME(lasttime);
					txnPhoneTokenDao.update(old);
				}
			}else {
				if(txnPhoneTokenDao.getByDpuserid(dpuserid).size()>0) {
					TXNPHONETOKEN old = txnPhoneTokenDao.getByDpuserid(dpuserid).get(0);
					if(old.getDPUSERID().equals(dpuserid)) {
						old.setPHONETOKEN(phonetoken);
						old.setPHONETYPE(phonetype);					
						old.setLASTDATE(lastdate);
						old.setLASTTIME(lasttime);
						txnPhoneTokenDao.update(old);
						bs.setResult(Boolean.TRUE);
						bs.setMessage("0", "更新成功");
						return bs;
					}
				}
			}
				
			//檢查NOTIFYAD是否為空值
			if(reqParam.get("notifyad") == null ||reqParam.get("notifyad").trim().length() == 0) {

				reqParam.put("notifyad", "");
			}

			//檢查NOTIFYTRANS是否為空值
			if(reqParam.get("notifytrans") == null ||reqParam.get("notifytrans").trim().length() == 0) {

				reqParam.put("notifytrans", "");
			}

			//填入時間
			reqParam.put("lastdate",lastdate);
			reqParam.put("lasttime",lasttime);
			//將req所有的key轉成大寫
			Set keySet = reqParam.keySet();
			List<String> keyList = new ArrayList();
			Iterator it = keySet.iterator();
				
			while (it.hasNext()) {
				String str = (String) it.next();
				keyList.add(str);
			}

			for (String key : keyList) {
					reqParam.put(key.toUpperCase(), reqParam.get(key));
					reqParam.remove(key);
			}
	 			
			log.trace(ESAPIUtil.vaildLog("test>>>>>>>>>{}"+ CodeUtil.toJson(reqParam)));
				
			//將req轉成po
			txnPhoneToken = CodeUtil.objectCovert(TXNPHONETOKEN.class, reqParam);
			log.debug(ESAPIUtil.vaildLog("txnPhoneToken PO" + CodeUtil.toJson(txnPhoneToken)));
			if(txnPhoneTokenDao.getByDpuserid(dpuserid).size()>0) {
				txnPhoneTokenDao.update(txnPhoneToken);
				bs.setMessage("0", "更新成功");
			}else {
				txnPhoneTokenDao.save(txnPhoneToken);
				bs.setMessage("0", "新增成功");
			}
			bs.setResult(Boolean.TRUE);
			
		
		} catch (Exception e) {
			log.error("{}", e.toString());
			log.error("{}", e.getMessage());
			log.error("{}", e.getStackTrace());
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", "新增異常");
		}
		return bs;
	}
	
	/*
	 * 更新推播設定
	 */
//	public BaseResult update(Map<String, String> reqParam) {
//		BaseResult bs = new BaseResult();
//		TXNPHONETOKEN txnPhoneToken = new TXNPHONETOKEN();
//		
//		try {
//			//檢查統編(dpuserid)是否為空值
//			if(reqParam.get("dpuserid") == null ||reqParam.get("dpuserid").trim().length() == 0) {
//				bs.setResult(Boolean.FALSE);
//				bs.setMessage("1","統編未輸入");
//				return bs;
//			}
//			//檢查該統編(dpuserid)資料是否已存在
//			if(txnPhoneTokenDao.getByDpuserid(reqParam.get("dpuserid")).size()==0) {
//				bs.setResult(Boolean.FALSE);
//				bs.setMessage("1","該統編資料不存在");
//				return bs;
//			}
//			
//			//檢查(PHONETOKEN)是否為空值
//			if(reqParam.get("phonetoken") == null ||reqParam.get("phonetoken").trim().length() == 0) {
//				bs.setResult(Boolean.FALSE);
//				bs.setMessage("1","phonetoken未輸入");
//				return bs;
//			}
//			
//			//檢查PHONETYPE是否為空值
//			if(reqParam.get("phonetype") == null ||reqParam.get("phonetype").trim().length() == 0) {
//				bs.setResult(Boolean.FALSE);
//				bs.setMessage("1","phonetype未輸入");
//				return bs;
//			}
//			//檢查(PHONETYPE)是否輸入A(Android)或I(Iphone)
//			if(!("A".equals(reqParam.get("phonetype"))||"I".equals(reqParam.get("phonetype")))) {
//				bs.setResult(Boolean.FALSE);
//				bs.setMessage("1","phonetype手機OS輸入錯誤");
//				return bs;
//			}
//
//			//檢查NOTIFYAD是否為空值
//			if(reqParam.get("notifyad") == null ||reqParam.get("notifyad").trim().length() == 0) {
//				bs.setResult(Boolean.FALSE);
//				bs.setMessage("1","notifyad未輸入");
//				return bs;
//			}
//			//檢查(NOTIFYAD)是否輸入Y或N
//			if(!("Y".equals(reqParam.get("notifyad"))||"N".equals(reqParam.get("notifyad")))) {
//				bs.setResult(Boolean.FALSE);
//				bs.setMessage("1","notifyad輸入錯誤");
//				return bs;
//			}
//			//檢查NOTIFYTRANS是否為空值
//			if(reqParam.get("notifytrans") == null ||reqParam.get("notifytrans").trim().length() == 0) {
//				bs.setResult(Boolean.FALSE);
//				bs.setMessage("1","notifytrans未輸入");
//				return bs;
//			}
//			//檢查(NOTIFYTRANS)是否輸入Y或N
//			if(!("Y".equals(reqParam.get("notifytrans"))||"N".equals(reqParam.get("notifytrans")))) {
//				bs.setResult(Boolean.FALSE);
//				bs.setMessage("1","notifytrans輸入錯誤");
//				return bs;
//			}
//
//			//填入時間
//			//簡易日期格式
//			SimpleDateFormat sdfD = new SimpleDateFormat("yyyyMMdd");
//			//簡易時間格式
//			SimpleDateFormat sdfT = new SimpleDateFormat("hhmmss");
//			String lastdate = sdfD.format(new Date());
//			String lasttime = sdfT.format(new Date());
//			reqParam.put("lastdate",lastdate);
//			reqParam.put("lasttime",lasttime);
////			//將req所有的key轉成大寫
//			Set keySet = reqParam.keySet();
//			List<String> keyList = new ArrayList();
//			Iterator it = keySet.iterator();
//				
//			while (it.hasNext()) {
//				String str = (String) it.next();
//				keyList.add(str);
//			}
//
//			for (String key : keyList) {
//					reqParam.put(key.toUpperCase(), reqParam.get(key));
//					reqParam.remove(key);
//			}
//	 			
//			log.trace(ESAPIUtil.vaildLog("test>>>>>>>>>{}"+ CodeUtil.toJson(reqParam)));
//				
//			//將req轉成po
//			txnPhoneToken = CodeUtil.objectCovert(TXNPHONETOKEN.class, reqParam);
//			log.debug("txnPhoneToken PO" + CodeUtil.toJson(txnPhoneToken));
//			txnPhoneTokenDao.update(txnPhoneToken);
//			bs.setResult(Boolean.TRUE);
//			bs.setMessage("0", "修改成功");
//		
//		} catch (Exception e) {
//			log.error("{}", e.toString());
//			log.error("{}", e.getMessage());
//			log.error("{}", e.getStackTrace());
//			bs.setResult(Boolean.FALSE);
//			bs.setMessage("1", "修改異常");
//		}
//		return bs;
//	}
	
}
