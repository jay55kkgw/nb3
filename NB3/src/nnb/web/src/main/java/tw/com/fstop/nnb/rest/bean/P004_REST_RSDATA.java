package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class P004_REST_RSDATA extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7762921987170621373L;

	private String ZoneCode;//縣市別
	private String CarId;//牌照號碼
	private String CarKind;//車種
	private String SlipNo;//停車單號
	private String OpenDate;//開單日期
	private String LastDate;//繳費期限
	private String OpenAmt;//開單金額
	private String PayAccount;//扣款帳號
	private String CardType;//扣款方式
	private String PayDate;//扣款日期
	private String PaySource;//繳費來源別
	private String SessionCode;//繳費進度
	private String SlipRespCode;//此筆停車單處理的回應代碼
	private String SlipRespString;//此筆停車單處理的回應代碼敘述
	
	public String getZoneCode() {
		return ZoneCode;
	}
	public void setZoneCode(String zoneCode) {
		ZoneCode = zoneCode;
	}
	public String getCarId() {
		return CarId;
	}
	public void setCarId(String carId) {
		CarId = carId;
	}
	public String getCarKind() {
		return CarKind;
	}
	public void setCarKind(String carKind) {
		CarKind = carKind;
	}
	public String getSlipNo() {
		return SlipNo;
	}
	public void setSlipNo(String slipNo) {
		SlipNo = slipNo;
	}
	public String getOpenDate() {
		return OpenDate;
	}
	public void setOpenDate(String openDate) {
		OpenDate = openDate;
	}
	public String getLastDate() {
		return LastDate;
	}
	public void setLastDate(String lastDate) {
		LastDate = lastDate;
	}
	public String getOpenAmt() {
		return OpenAmt;
	}
	public void setOpenAmt(String openAmt) {
		OpenAmt = openAmt;
	}
	public String getPayAccount() {
		return PayAccount;
	}
	public void setPayAccount(String payAccount) {
		PayAccount = payAccount;
	}
	public String getCardType() {
		return CardType;
	}
	public void setCardType(String cardType) {
		CardType = cardType;
	}
	public String getPayDate() {
		return PayDate;
	}
	public void setPayDate(String payDate) {
		PayDate = payDate;
	}
	public String getPaySource() {
		return PaySource;
	}
	public void setPaySource(String paySource) {
		PaySource = paySource;
	}
	public String getSessionCode() {
		return SessionCode;
	}
	public void setSessionCode(String sessionCode) {
		SessionCode = sessionCode;
	}
	public String getSlipRespCode() {
		return SlipRespCode;
	}
	public void setSlipRespCode(String slipRespCode) {
		SlipRespCode = slipRespCode;
	}
	public String getSlipRespString() {
		return SlipRespString;
	}
	public void setSlipRespString(String slipRespString) {
		SlipRespString = slipRespString;
	}
}
