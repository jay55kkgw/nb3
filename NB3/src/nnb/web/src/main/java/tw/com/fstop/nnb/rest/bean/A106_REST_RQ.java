package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class A106_REST_RQ extends BaseRestBean_TW implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1124472617158015428L;
	
	private String ABEND;		// 結束代碼
	private String CUSIDN;		// 統一編號
	private String APLBRH;		// 網銀申請行
	private String FUN_TYPE;	// 類別
	private String CAREER1;		// 職業
	private String CAREER2;		// 職稱
	private String CONPNAM;		// 任職機構名稱
	private String CDDCOD;		// 客戶洗錢風險等級
	
	// ms_tw需要的欄位
	private String COMPANYNAME;	// 任職機構名稱
	private String PARAM1;		// 身分證
	private String PARAM2;		// 系統別
	private String PARAM3;		// 任職機構名稱
	private String PARAM4;		// 職業
	private String PARAM5;		// 職稱
	private String PINNEW;		// 交易密碼
	
	
	public String getABEND() {
		return ABEND;
	}
	public void setABEND(String aBEND) {
		ABEND = aBEND;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getAPLBRH() {
		return APLBRH;
	}
	public void setAPLBRH(String aPLBRH) {
		APLBRH = aPLBRH;
	}
	public String getFUN_TYPE() {
		return FUN_TYPE;
	}
	public void setFUN_TYPE(String fUN_TYPE) {
		FUN_TYPE = fUN_TYPE;
	}
	public String getCAREER1() {
		return CAREER1;
	}
	public void setCAREER1(String cAREER1) {
		CAREER1 = cAREER1;
	}
	public String getCAREER2() {
		return CAREER2;
	}
	public void setCAREER2(String cAREER2) {
		CAREER2 = cAREER2;
	}
	public String getCONPNAM() {
		return CONPNAM;
	}
	public void setCONPNAM(String cONPNAM) {
		CONPNAM = cONPNAM;
	}
	public String getCDDCOD() {
		return CDDCOD;
	}
	public void setCDDCOD(String cDDCOD) {
		CDDCOD = cDDCOD;
	}
	public String getCOMPANYNAME() {
		return COMPANYNAME;
	}
	public void setCOMPANYNAME(String cOMPANYNAME) {
		COMPANYNAME = cOMPANYNAME;
	}
	public String getPARAM1() {
		return PARAM1;
	}
	public void setPARAM1(String pARAM1) {
		PARAM1 = pARAM1;
	}
	public String getPARAM2() {
		return PARAM2;
	}
	public void setPARAM2(String pARAM2) {
		PARAM2 = pARAM2;
	}
	public String getPARAM3() {
		return PARAM3;
	}
	public void setPARAM3(String pARAM3) {
		PARAM3 = pARAM3;
	}
	public String getPARAM4() {
		return PARAM4;
	}
	public void setPARAM4(String pARAM4) {
		PARAM4 = pARAM4;
	}
	public String getPARAM5() {
		return PARAM5;
	}
	public void setPARAM5(String pARAM5) {
		PARAM5 = pARAM5;
	}
	public String getPINNEW() {
		return PINNEW;
	}
	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}
	

}
