package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N953_REST_RS extends BaseRestBean implements Serializable {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 9212618584365333820L;

	private String CUSIDN; // 客戶身分證統一編號
	
	private String ACN; // 轉出帳號
	
	private String ICCARD; // 是否有晶片金融卡
	
	private String STSCOD; // 晶片金融卡狀態

	private String REJCOD; // 晶片金融卡狀態
	
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getICCARD() {
		return ICCARD;
	}
	public void setICCARD(String iCCARD) {
		ICCARD = iCCARD;
	}
	public String getSTSCOD() {
		return STSCOD;
	}
	public void setSTSCOD(String sTSCOD) {
		STSCOD = sTSCOD;
	}
	public String getREJCOD() {
		return REJCOD;
	}
	public void setREJCOD(String rEJCOD) {
		REJCOD = rEJCOD;
	}

}
