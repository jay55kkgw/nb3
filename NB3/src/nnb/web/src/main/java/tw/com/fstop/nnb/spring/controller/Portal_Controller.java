package tw.com.fstop.nnb.spring.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.service.Portal_Service;
import tw.com.fstop.util.ESAPIUtil;

@Controller
@RequestMapping(value = "/PORTAL")
public class Portal_Controller
{
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private Portal_Service portal_service;
	
	/**
	 * 確認使用者portal連線狀態
	 */
	@RequestMapping(value = "/getportalstatus", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult getportalstatus(HttpServletRequest request, HttpServletResponse response, 
				@RequestParam Map<String, String> reqParam, Model model) {

		BaseResult bs = null;
		try {
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			
			// 解密使用者統編
			String cusidn = okMap.get("cusidn").toUpperCase();
			log.debug(ESAPIUtil.vaildLog("getportalstatus.cusidn: {}"+ cusidn));
			
			// 解密使用者統編
			String account = okMap.get("account");
			log.debug(ESAPIUtil.vaildLog("getportalstatus.account: {}"+ account));
			
			bs = new BaseResult();
			boolean result = portal_service.getPortalStatus(cusidn, account, "NB3");
			if(result) {
				bs.setMsgCode("0");
				bs.setMessage("portal status is online");
				bs.setResult(true);
				
			} else {
				bs.setMsgCode("0");
				bs.setMessage("portal status is not online");
				bs.setResult(false);
			}
			
		} catch (Exception e) {
			log.error("",e);
		}
		return bs;
	}
	
	/**
	 * 確認使用者portal連線狀態後踢掉使用者portal連線
	 */
	@RequestMapping(value = "/kickportal", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult kickportal(HttpServletRequest request, HttpServletResponse response, 
				@RequestParam Map<String, String> reqParam, Model model) {
		
		BaseResult bs = null;
		try {
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			
			// 解密使用者統編
			String cusidn = okMap.get("cusidn").toUpperCase();
			log.debug(ESAPIUtil.vaildLog("getportalstatus.cusidn: {}"+ cusidn));
			
			// 解密使用者統編
			String account = okMap.get("account");
			log.debug(ESAPIUtil.vaildLog("getportalstatus.account: {}"+ account));
			
			bs = new BaseResult();
			portal_service.kickPortal(cusidn, account, "NB3");
			
			// 不論有無回傳值都視為成功
			bs.setMsgCode("0");
			bs.setMessage("kick portal session success");
			bs.setResult(true);
			
		} catch (Exception e) {
			log.error("",e);
		}
		return bs;
	}

}
