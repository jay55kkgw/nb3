package tw.com.fstop.nnb.service;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.ftp.FTPSClient;
import org.joda.time.DateTime;
import org.owasp.esapi.SafeFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import fstop.orm.po.ADMBRANCH;
import fstop.orm.po.TXNCRLINEAPP;
import fstop.orm.po.TXNLOANAPPLY;
import fstop.orm.po.TXNUSER;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.rest.bean.N360_REST_RS;
import tw.com.fstop.nnb.rest.bean.N361_REST_RQ;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.AdmBranchDao;
import tw.com.fstop.tbb.nnb.dao.AdmEmpInfoDao;
import tw.com.fstop.tbb.nnb.dao.AdmMsgCodeDao;
import tw.com.fstop.tbb.nnb.dao.AdmZipDataDao;
import tw.com.fstop.tbb.nnb.dao.TxnCrLineAppDao;
import tw.com.fstop.tbb.nnb.dao.TxnUserDao;
import tw.com.fstop.tbb.nnb.util.StrUtils;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.web.util.ConfingManager;
import tw.com.fstop.web.util.FTPUtil;

@Service
public class Online_Apply_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	TxnUserDao txnUserDao;
	
	@Autowired
	AdmBranchDao admBranchDao;
	
	@Autowired
	AdmMsgCodeDao admMsgCodeDao;

	@Autowired
	AdmZipDataDao admZipDataDao;
	
	@Autowired
	AdmEmpInfoDao admEmpInfoDao;
	
	@Autowired
	TxnCrLineAppDao txnCrLineAppDao;
	
	@Autowired
	private I18n i18n;
	
	@Autowired
	ServletContext context;
	
	@Autowired
	private Creditcard_Apply_Service creditcard_apply_service;

	@Autowired
	@Qualifier("KYC_QUESTION")
	public LinkedHashMap<String, LinkedHashMap> questions;
	
	

	/**
	 * 取得 KYC 選項 i18n 描述
	 * 
	 * @param kycParams  KYC確認資料(使用者填寫的問卷)
	 * @return
	 */
	public Map<String, String> addKycOptionDescription(Map<String, String> kycParams) 
	{
		// KYC 問卷題目代號 FDQ1~15
		String qKey = "";
		// KYC 問卷選擇題選項 A~D不等
		String option = "";
		// 選擇題選項描述 A. 未達40歲
		String desc = "";
		
		// 取得所有問卷題目代號，定義在 spring-arg
		Iterator<String> keys = questions.keySet().iterator();
		log.debug("題目代號 >> {}", keys);
		
		while(keys.hasNext())
		{
			qKey = keys.next();
			if(kycParams.containsKey(qKey))
			{
				// 取得題目代號
				LinkedHashMap<String, String> descMap = questions.get(qKey);
				// 取得使用者勾選的項目
				option = kycParams.get(qKey);
				log.debug("使用者勾選的項目 >> {}", option);
				
				// 取得該選項的 i18n 描述
				if(option.length() > 1)
				{
					// 複選
					desc = mutipleProcess(option, descMap);
				}
				else
				{
					// 單選
					desc = descMap.get(option);
				}
				
				// 加回 KYC 問卷結果集合
				kycParams.put(qKey + "_Desc", desc);
				log.debug(qKey + "_Desc ： " + desc);
			}
		}
		return kycParams;
	}
	
	
	/**
	 * 複選題處理
	 * 複選字串為連號英文，將選項逐一跑迴圈取得單個 i18n 描述，最後全部合成單一字串回傳
	 * 
	 * @param options
	 * @param descMap
	 * @return
	 */
	public String mutipleProcess(String options, Map<String, String> descMap)
	{
		StringBuffer result = new StringBuffer();
		log.debug("使用者複選的選項 >> {}", options);
		int length = options.length();
		String tmp = options;
		String first = "";
		for(int i = 0; i < length; i++)
		{
			first = tmp.substring(0, 1);
			String desc = descMap.get(first);
			result.append(desc);
			log.debug(desc);
			
			if(tmp.length() > 1)
			{
				result.append(" ");
			}
			
			// remove first string
			tmp = tmp.substring(1);
		}
		log.debug(result.toString());
		return result.toString();
	}
	
	public BaseResult shield_apply_result(Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			log.trace("start shield_apply_result_service");
			log.trace(ESAPIUtil.vaildLog("shield_apply_result.reqParam: {}"+ reqParam));
			bs = new  BaseResult();
			bs = CN19_REST(reqParam);
			if("0000".equals(((Map<String,String>)bs.getData()).get("TOPMSG"))) {
				Map<String, String> cn19Map = (Map<String,String>)bs.getData();
				cn19Map.put("DATAPL",DateUtil.addSlash2(cn19Map.get("DATAPL")));
				cn19Map.put("DATTIM",DateUtil.formatTime(cn19Map.get("DATTIM")));
				bs.setData(cn19Map);
			}
			bs.setResult(true);
			log.trace("CN19_result_service_bs >>{}", bs.getData());
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("shield_apply_result error >> {}",e);
		}
		return bs;
	}
	
	
//	/**
//	 * 發N360電文驗使用者身份(FATCA)、驗AML洗錢防制、N360後處理
//	 * 
//	 * @param cusidn
//	 * @return
//	 */
//	public BaseResult n360(String cusidn) 
//	{
//		log.debug("n360 start");
//		log.debug(ESAPIUtil.vaildLog("reqParam >> {}"+ cusidn));
//		BaseResult bs = new BaseResult();
//		try 
//		{
//			// N360_REST
//			bs = N360_REST(cusidn);
//			log.debug("n360 >> {}", bs.getData());
//			if(bs.getResult() == false) 
//			{
//				throw new Exception();
//			}
//			
//			// AML START
//			String branchId = getBranchId(cusidn);
//			log.debug("branchId >> {}", branchId);
//			
//			Map<String, String> reqParam = new HashMap<>();
//			reqParam.put("CUSIDN", cusidn);
//			reqParam.put("NCHECKNB", "Y");
//			reqParam.put("ADOPID", "N360");
//			// AML取得中心基本資料
//			BaseResult bsN960 = n960Service(reqParam, branchId);
//			
//			// 存入AML result
//			Map<String, String> bsN960Data = (Map) bsN960.getData();
//			boolean amlmsg = Boolean.valueOf(bsN960Data.get("amlmsg"));
//			bs.addData("amlmsg", amlmsg);
//			bs.addData("N960MSG", bsN960.getMessage());
//			
//			if(bsN960.getResult() == false) 
//			{
//				bs = bsN960;
//				throw new Exception();
//			}
//			
//			if(bs != null && bs.getResult())
//			{
//				bs = n360PostProcess(bs);
//				log.debug("n360PostProcess >> {}", CodeUtil.toJson(bs));
//			}
//			
//		} 
//		catch (Exception e) 
//		{
//			log.error("n360 error", e);
//		}
//		return bs;
//	}

	/**
	 * 發N360電文驗使用者身份(FATCA)、驗AML洗錢防制、N360後處理
	 * 
	 * @param cusidn
	 * @return
	 */
	public BaseResult n360(String cusidn) {
		log.debug("n360 start");
		log.debug(ESAPIUtil.vaildLog("reqParam >> {}" + cusidn));
		BaseResult bs = new BaseResult();
		try {
			// N360_REST
			bs = N360_REST(cusidn);
			log.debug("n360 >> {}", bs.getData());

			// 先判斷n360回傳
			if (bs != null && bs.getResult()) {
				// n360處理
				bs = n360PostProcess(bs);
				log.debug("n360PostProcess >> {}", CodeUtil.toJson(bs));
				// n360處理後 判斷結果 正常才打aml 不正常不打 直接到錯誤頁
				// 不正常 ( n360PostProcess 有錯誤狀況 )
				if (!bs.getResult()) {
//					String fakeMsg = "Z611";
//					bs.addData("TOPMSG", fakeMsg);
//					String message = getMessage(fakeMsg);
//					bs.setMessage(fakeMsg , message);
					return bs;
					// 正常 ( 往下打 n960 + aml )
				} else {

					String branchId = getBranchId(cusidn);
					log.debug("branchId >> {}", branchId);

					Map<String, String> reqParam = new HashMap<>();
					reqParam.put("CUSIDN", cusidn);
					reqParam.put("NCHECKNB", "Y");
					reqParam.put("ADOPID", "N360");
					// AML取得中心基本資料
					BaseResult bsN960 = n960Service(reqParam, branchId);

					// 存入AML result
					Map<String, String> bsN960Data = (Map) bsN960.getData();
					boolean amlmsg = Boolean.valueOf(bsN960Data.get("amlmsg"));
					String amlcode = bsN960Data.get("amlcode");
					bs.addData("amlmsg", amlmsg);
					bs.addData("amlcode", amlcode);
					bs.addData("N960MSG", bsN960.getMessage());

					if (bsN960.getResult() == false) {
						bs = bsN960;
					} // n960 end
				} // aml end
			} // n360 end
		} catch (Exception e) {
			log.error("n360 error", e);
		}
		return bs;
	}
	
//	/**
//	 * 基金線上預約開戶作業N360後處理
//	 * 
//	 * @param bs
//	 * @return
//	 */
//	public BaseResult n360PostProcess(BaseResult bs)
//	{
//		Map<String, Object> map = (Map<String, Object>) bs.getData();
//		N360_REST_RS rs = (N360_REST_RS) map.get("RS");
//		
//		String occurMsg = rs.getOccurMsg();
//		if(occurMsg.equals("0000") || occurMsg.equals("    "))
//		{
//			if(rs.getCUSTYPE().equals("N"))
//			{	 
//				rs.setSVYON("");
//				rs.setFATCA("");
//				rs.setORDYON("");
//				rs.setNBYON("");
//				rs.setFNDYON("");
//				rs.setNBFLAG("");
//				occurMsg = "Z614";
//				rs.setOccurMsg(occurMsg);
//			}
//			if(rs.getSVYON().equals("N"))
//			{	 
//				rs.setFATCA("");
//				rs.setORDYON("");
//				rs.setNBYON("");
//				rs.setFNDYON("");
//				rs.setNBFLAG("");
//				// 須導到線上預約開戶
//				occurMsg = "Z613";
//				rs.setOccurMsg(occurMsg);
//			}
//			if(rs.getFATCA().equals("N"))
//			{	 
//				rs.setORDYON("");
//				rs.setNBYON("");
//				rs.setFNDYON("");
//				rs.setNBFLAG("");
//				occurMsg = "Z608";
//				rs.setOccurMsg(occurMsg);
//			}				
//			if(rs.getFNDYON().equals("Y"))
//			{	 
//				rs.setORDYON("");
//				rs.setNBYON("");
//				rs.setNBFLAG("");
//				occurMsg = "Z609";
//				rs.setOccurMsg(occurMsg);
//			}
//			if(rs.getORDYON().equals("Y"))
//			{	 
//				rs.setNBYON("");
//				rs.setNBFLAG("");
//				occurMsg = "Z609";
//				rs.setOccurMsg(occurMsg);
//			}
//			if(rs.getNBYON().equals("N") || (rs.getNBYON().equals("Y") && (rs.getNBFLAG().equals("2") || rs.getNBFLAG().equals("3"))))
//			{	 
//				if(rs.getNBFLAG().equals("2"))
//				{
//					// to 金融卡線上申請/取消轉帳功能
//					occurMsg = "Z611";
//					rs.setOccurMsg(occurMsg);
//				}
//				if(rs.getNBYON().equals("N") || rs.getNBFLAG().equals("3"))
//				{
//					// 須導到晶片金融卡線上申請網銀
//					occurMsg = "Z612";
//					rs.setOccurMsg(occurMsg);
//				}
//			}
//			if(map.get("amlmsg").equals("F")) {
//				occurMsg = "Z616";
//				rs.setOccurMsg((String)map.get("N960MSG"));
//			}
//		}
//		
//		String message = getMessage(occurMsg);
//		bs.setMessage(occurMsg, message);
//		return bs;
//	}
	
	/**
	 * 基金線上預約開戶作業N360後處理
	 * 
	 * @param bs
	 * @return
	 */
	public BaseResult n360PostProcess(BaseResult bs) {
		Map<String, Object> map = (Map<String, Object>) bs.getData();

		String errMsg = "";
		
		if (map.get("TOPMSG").equals("0000") || map.get("TOPMSG").equals("    ")) {
			if (map.get("CUSTYPE").equals("N")) {
				errMsg = "Z614";
				bs.addData("TOPMSG", errMsg);
				bs.setResult(false);
			}
			if (map.get("SVYON").equals("N")) {
				// 須導到線上預約開戶
				errMsg = "Z613";
				bs.addData("TOPMSG", errMsg);
				bs.setResult(false);
			}
			if (map.get("FATCA").equals("N")) {
				errMsg = "R002";
				bs.addData("TOPMSG", errMsg);
				bs.setResult(true);
			}
			if (map.get("FNDYON").equals("Y")) {
				errMsg = "Z609";
				bs.addData("TOPMSG", errMsg);
				bs.setResult(false);
			}
			if (map.get("ORDYON").equals("Y")) {
				errMsg = "Z609";
				bs.addData("TOPMSG", errMsg);
				bs.setResult(false);
			}
			if (map.get("NBYON").equals("N") || (map.get("NBYON").equals("Y")
					&& (map.get("NBFLAG").equals("2") || map.get("NBFLAG").equals("3")))) {
				if (map.get("NBFLAG").equals("2")) {
					// to 金融卡線上申請/取消轉帳功能
					errMsg = "Z611";
					bs.addData("TOPMSG", errMsg);
					bs.setResult(false);
				}
				if (map.get("NBYON").equals("N") || map.get("NBFLAG").equals("3")) {
					// 須導到晶片金融卡線上申請網銀
					errMsg = "Z612";
					bs.addData("TOPMSG", errMsg);
					bs.setResult(false);
				}
			}
		}
		if (!"".equals(errMsg)) {
			String message = getMessage(errMsg);
			bs.setMessage(errMsg, message);
		}

		return bs;
	}
	
	
	/**
	 * 線上預約開立基金戶 交易電文
	 * 
	 * @param reqParam
	 * @return
	 */
	public BaseResult n361(Map<String, String> reqParam) 
	{
		log.debug("n361 start");
		log.debug(ESAPIUtil.vaildLog("reqParam >> {}"+ reqParam));
		BaseResult bs = new BaseResult();
		try 
		{
			//mail 轉小寫
			if(reqParam.get("DPMYEMAIL") != null)
				reqParam.put("DPMYEMAIL", reqParam.get("DPMYEMAIL").toLowerCase());
			String json = CodeUtil.toJson(reqParam);
			log.debug(ESAPIUtil.vaildLog("n361 service reqParam > {}"+ json));
			N361_REST_RQ rq = CodeUtil.fromJson(json, N361_REST_RQ.class);
			
			bs = N361_REST(rq);
			log.debug("N361_REST >> {}", bs.getData());
			
			if(bs != null && bs.getResult())
			{
				bs.addData("RQ", rq);
				n361PostProcess(bs, rq.getCUSIDN());
			}
		} 
		catch (Exception e) 
		{
			log.error("n361 error", e);
		}
		return bs;
	}
	
	
	/**
	 * 基金線上預約開戶作業N361後處理
	 * 
	 * @param bs 下行電文結果
	 * @param rq 上行電文參數
	 * @return
	 */
	public BaseResult n361PostProcess(BaseResult bs, String cusidn)
	{
		TXNUSER user = txnUserDao.get(TXNUSER.class, cusidn);
		if(user == null)
		{
			log.warn(ESAPIUtil.vaildLog("User ID not found >> {}"+ cusidn));
		}
		else
		{
			String dpusername = user.getDPUSERNAME().replace("?", " ");;
			log.trace("n361PostProcess.dpusername: {}", dpusername);
			bs.addData("DPUSERNAME", dpusername);	// 戶名
			
			Map<String, String> bsData = (Map) bs.getData();
			List<ADMBRANCH> list = admBranchDao.findByBhID(bsData.get("BRHCOD"));
			ADMBRANCH po = list.get(0);
			
			bs.addData("BRHNAME", po.getADBRANCHNAME());	// 服務分行名稱
			bs.addData("BRHTEL",  po.getTELNUM());			// 服務分行電話
			bs.addData("BRHADDR", po.getADDRESS());			// 服務分行地址
		}
		return bs;
	}
	
	
	/**
	 * N960查詢使用者資訊(戶名、生日)、call AML洗錢防制API
	 * 
	 * @param reqParam
	 * @param branchId
	 * @return
	 */
	public BaseResult n960Service(Map<String, String> reqParam, String branchId)
	{
		BaseResult bs = new BaseResult();
		Map<String, String> amlParams=new HashMap<>();
		try
		{
			bs = N960_REST_UPDATE_CUSNAME(reqParam);
			if(bs.getResult())
			{
				// 某些帳號，電文回應MSGCOD=0000，但是卻沒有使用者資訊，會導致後續程式出錯
				// 若發生此狀況，一樣讓流程做N360後處理
				Map<String, String> bsData = (Map) bs.getData();
				// NAME 戶名
				if(StrUtils.isEmpty(bsData.get("NAME")))
				{
					log.error("N960查無使用者資訊 >> {}", bsData);
				}
				else
				{
					// 從N960電文取AML參數
					
					amlParams = getAMLparams(reqParam.get("CUSIDN"), branchId, bs);
					amlParams.put("ADOPID", reqParam.get("ADOPID"));
					log.debug(ESAPIUtil.vaildLog("amlParams >> {}"+ amlParams));
				}
				
				String amlmsg = "false";
				// call AML洗錢防制API，檢查使用者是否在黑名單
				BaseResult aml = AML_REST(amlParams,ConfingManager.MS_GOLD);
				log.debug("AML DATA >>{}",CodeUtil.toJson(aml));
				//Ziv. 2021/08/03 R1100002804,不阻擋AML結果，改抓AML CODE 回傳給中心(N361) 
				//if (!"0".equals(aml.getMsgCode())) {
				//	return aml;
				//}
				log.debug("Ziv TEST....");
				amlmsg = ((Map<String,Object>)aml.getData()).get("RESULT") != null ? (String)((Map<String,Object>)aml.getData()).get("RESULT") : "true";
				log.debug("AML msg >> {}", amlmsg);
				//Cancelled by  Ziv. 2021/08/03 R1100002804,不阻擋AML結果，改抓AML CODE 回傳給中心(N361)
				// 該使用者在黑名單中 
				/*if("F".equals(amlmsg))
				{
					String failMsg = getAMLfailMsg(branchId);
					log.debug(ESAPIUtil.vaildLog("AML failMsg >> " + failMsg));
					bs.setErrorMessage("Z616", failMsg);
					bs.setResult(false);
					// AML END
				}*/
				//Ziv. 2021/08/03 R1100002804,不阻擋AML結果，改抓AML CODE 回傳給中心(N361) 
				//S : 制裁名單，恐怖份子，高國家風險(ECR) ; P : 政治人物 ; R : 政治人物之家庭成員及密切關係人 ; O : 其他 ; N : 無相關類別資訊
				String type = ((Map<String,Object>)aml.getData()).get("LISTTYPE").toString();				
				String amlcode = "";
				if("F".equals(amlmsg)) {
					switch (type) {
					case "S":
						amlcode="01"; // 制裁名單
						break;
					case "P":
					case "R":
					case "O":
					case "N":
						amlcode="02"; // 他類名單
						break;
					default:
						amlcode="02";
						break;
					}
				} else {
					amlcode="00"; // 未掃中
				}
				
				bs.addData("amlcode", amlcode);
				// 存入AML result
				bs.addData("amlmsg", amlmsg);
			}
		}
		catch (Exception e) 
		{
			log.error("n960Service error.");
			String message = getMessage("ZX99");
			bs.setErrorMessage("ZX99", message);
			bs.setResult(false);
		}
		return bs;
	}
	
	
	/**
	 * 取得新臺幣活期性存款帳戶、外幣活期性存款帳戶
	 * 
	 * @param cusidn
	 * @param type
	 * @return
	 * @throws Exception
	 */
	public List<String> getAccounts(String cusidn, String type) throws Exception
	{
		log.debug("getAccounts start");
		log.debug(ESAPIUtil.vaildLog("CUSIDN >> "+cusidn));
		List<String> result = new ArrayList<>();
		BaseResult bs = new BaseResult();
		
		bs = N920_REST(cusidn, type);
		
		if(bs.getResult())
		{
			Map<String, Object> bsData = (Map) bs.getData();
			List<Map> rec = (List) bsData.get("REC");
			if(rec.size() > 0)
			{
				for(Map<String, String> row : rec)
				{
					result.add(row.get("ACN"));
				}
			}
		}
		else
		{
			log.error("getAccounts error");
			throw new Exception();
		}
		log.debug("帳號筆數為 >> {}", result.size());
		return result;
	}
	
	
	/**
	 * 取得使用者Email
	 * 
	 * @param cusidn
	 * @return
	 */
	public String getDPMYEMAIL(String cusidn) 
	{
		log.debug("getDPMYEMAIL start");
		log.debug(ESAPIUtil.vaildLog("CUSIDN >> "+cusidn));
		String myEmail = "";
		try 
		{
			List<TXNUSER> list = txnUserDao.findByUserId(cusidn);
			if(list != null && !list.isEmpty())
			{
				myEmail = list.get(0).getDPMYEMAIL().trim();
			}
		} 
		catch (Exception e) 
		{
			log.error("getDPMYEMAIL error", e);
		}
		return myEmail;
	}
	
	
	/**
	 * 取得AML洗錢防制驗證失敗後，要顯示的錯誤訊息
	 * 
	 * @param branchId
	 * @return
	 */
	public String getAMLfailMsg(String branchId)
	{
		String result = "";
		String BRHNAME = "";
		String BRHTEL = "";
		if(StrUtils.isEmpty(branchId))
		{
			String msg = "branchId is Empty";
			log.error(msg);
			throw new NullPointerException("branchId is null");
		}
		
		ADMBRANCH bhContact = new ADMBRANCH();
		List<ADMBRANCH> list = admBranchDao.findByBhID(branchId);
		
		if(list.isEmpty())
		{
			String msg = "can not find ADMBRANCH, branchId >> " + branchId;
			log.error(msg);
			throw new NullPointerException(msg);
		}
		else
		{
			bhContact = list.get(0);
			BRHNAME = bhContact.getADBRANCHNAME();
			BRHTEL = bhContact.getTELNUM();
		}
		//result = "交易失敗!如您有任何疑問，請洽您所往來的" + BRHNAME + "(聯絡電話：" + BRHTEL + ")詢問，謝謝。";
		result = i18n.getMsg("LB.X1881") + BRHNAME + "("+i18n.getMsg("LB.D0539")+"：" + BRHTEL + ")"+i18n.getMsg("LB.X1882")+"。";
		
		return result;
	}
	
	
	/**
	 * 取得AML洗錢防制參數
	 * 
	 * @param cusidn
	 * @param branchId
	 * @return
	 */
	public Map<String, String> getAMLparams(String cusidn, String branchId, BaseResult bs) throws Exception
	{
		Map<String, String> result = new HashMap<>();
		String name = "";
		String BRTHDY = "";
		String businessUnit = "030";
		String year = "";
		String month = "";
		String day = "";
		
		Map<String, String> n960 = (Map<String, String>) bs.getData();
		if(n960.containsKey("NAME"))
		{
			name = n960.get("NAME") == null ? "" : n960.get("NAME");
			//name = "General Taganda";
			BRTHDY = n960.get("BRTHDY") == null ? "" : n960.get("BRTHDY");
			int yearnum = Integer.parseInt(BRTHDY.substring(0, 3)) + 1911;
			year = String.valueOf(yearnum);
			month = BRTHDY.substring(3, 5);
			day = BRTHDY.substring(5, 7);
		}
		
		Map<String, String> amlparams = new HashMap<>();
		amlparams.put("name", name);
		amlparams.put("year", year);
		amlparams.put("month", month);
		amlparams.put("day", day);
		amlparams.put("BUSINESSUNIT", businessUnit);
		amlparams.put("branchId", branchId);
		amlparams.put("UID", cusidn);
		
		amlparams.put("NAME", name);
		amlparams.put("BIRTHDAY", n960.get("BRTHDY"));
		amlparams.put("BRHCOD", branchId);
		
		result = amlparams;
		return result;
	}

	
	/**
	 * 取得銀行代碼
	 * 
	 * @param cusidn
	 * @return
	 */
	public String getBranchId(String cusidn)
	{
		String branchId = "";
		TXNUSER user = txnUserDao.get(TXNUSER.class, cusidn);
		if(user == null)
		{
			log.warn(ESAPIUtil.vaildLog("User ID not found = {}"+ cusidn));
		}
		else
		{
			branchId = StrUtils.trim(user.getADBRANCHID());
			if(branchId.length() == 0)
			{
				BaseResult bsN927 = N927_REST(cusidn);
				Map<String, String> n927 = (Map<String, String>) bsN927.getData();
				branchId = n927.get("APLBRH");
			}
		}
		return branchId;
	}
	
	
	
	/**
	 * 取得錯誤碼中文
	 * 
	 * @param msgCode
	 * @return
	 */
	public String getMessage(String msgCode) 
	{
		String result = "";
		try 
		{
			result = admMsgCodeDao.errorMsg(msgCode);
		}
		catch(Exception ex)
		{
			log.error("getMessage error");
		}
		if(StrUtils.trim(result).length() == 0) 
		{
			if(msgCode.startsWith("Z")) 
			{
				result = i18n.getMsg("LB.X1887");//發送電文時發生錯誤
			}
		}
		return result;
	}

	
	/**
	 * 線上註銷無卡提款
	 * 
	 * @param msgCode
	 * @return
	 */
	public BaseResult logout_without_card_AcnCheck(Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			log.trace("start logout_without_card_AcnCheck Service");
			log.trace(ESAPIUtil.vaildLog("logout_without_card_AcnCheck.reqParam: {}"+ reqParam));
			bs = new  BaseResult();
			bs=N953_REST(reqParam.get("ACN"));
			log.trace("CN32D_result_service_bs >>{}", bs.getData());
			if(bs!=null && bs.getResult())
			{
				bs.setMsgCode(((Map<String,String>) bs.getData()).get("CUSIDN"));
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("logout_without_card_AcnCheck error >> {}",e);
		}
		return bs;
	}
	/**
	 * 線上註銷無卡提款
	 * 
	 * @param msgCode
	 * @return
	 */
	public BaseResult logout_without_card_step1(Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			log.trace("start logout_without_card_step1 Service");
			log.trace(ESAPIUtil.vaildLog("logout_without_card_step1.reqParam: {}"+ reqParam));
			bs = new  BaseResult();
			reqParam.put("CHIP_ACN", reqParam.get("ACNNO"));
			reqParam.put("OUTACN", reqParam.get("ACNNO"));
			bs = CN32D_REST(reqParam);
			Map<String,String>datamap=(Map<String, String>) bs.getData();
			datamap.put("ISSUER", reqParam.get("ISSUER"));
			if((datamap.get("occurMsg").equals("0000")||datamap.get("occurMsg").equals("")) && datamap.get("ENABLE").equals("Y")) {
				bs.setResult(true);}
			else {
				bs.setResult(false);
				if(datamap.get("ENABLE").equals("N")) {
					bs.setMsgCode("NONE");
					bs.setMessage(i18n.getMsg("LB.X1888"));
				}
				else {
					bs.setMsgCode(datamap.get("occurMsg"));
					bs.setMessage(datamap.get("messageName"));
				}
			}
			log.trace("CN32D_result_service_bs >>{}", bs.getData());
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("logout_without_card_step1 error >> {}",e);
		}
		return bs;
	}
	/**
	 * 線上註銷無卡提款
	 * 
	 * @param msgCode
	 * @return
	 */
	public BaseResult logout_without_card_step2(Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			log.trace("start logout_without_card_step2 Service");
			log.trace(ESAPIUtil.vaildLog("logout_without_card_step2.reqParam: {}"+ reqParam));
			bs = new  BaseResult();
			bs = CN32D_1_REST(reqParam);
			Map<String,String>datamap=(Map<String, String>) bs.getData();
			datamap.put("DATETIME",DateUtil.convertDate(0,datamap.get("DATE"),"yyymmdd","yyy/mm/dd" )+" "+DateUtil.formatTime(datamap.get("TIME")));
			log.trace("DATETIME >> "+datamap.get("DATETIME"));
			datamap.put("ACN",reqParam.get("ACN"));
			if(datamap.get("occurMsg").equals("0000")||datamap.get("occurMsg").equals("")) {
				bs.setResult(true);}
			else {
				bs.setResult(false);
				bs.setMsgCode(datamap.get("occurMsg"));
				bs.setMessage(datamap.get("messageName"));

			}
			log.trace("CN32D_1_result_service_bs >>{}", bs.getData());
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("logout_without_card_step2 error >> {}",e);
		}
		return bs;
	}
	/**
	 * 線上註銷臺灣Pay掃碼提款功能
	 * 
	 * @param msgCode
	 * @return
	 */
	public BaseResult logout_taiwan_pay_step1(Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			log.trace("N580_step1 Service");
			log.trace(ESAPIUtil.vaildLog("N580_step1.reqParam: {}"+ reqParam));
			bs = new  BaseResult();
			bs = N580_REST(reqParam);
			Map<String,String>datamap=(Map<String, String>) bs.getData();
			// 切割ACN
			String ACN = datamap.get("ACN").toString();
			int i_count = ACN.length()/11;
			List<String> ACNTEMP = new ArrayList<>();
			for (int i=0;i<i_count;i++)
			{
				int j=i*11;
				ACNTEMP.add(ACN.substring(j,j+11));
			}
			bs.addData("ACNTEMP",ACNTEMP);
			//
			if((datamap.get("occurMsg").equals("0000")||datamap.get("occurMsg").equals("")) && datamap.get("ENABLE").equals("Y")) {
				bs.setResult(true);
				}
			else {
				bs.setResult(false);
				if(datamap.get("ENABLE").equals("N")) {
					bs.setMsgCode("NONE");
					bs.setMessage(i18n.getMsg("LB.X2127"));
				}
				else {
					bs.setMsgCode(datamap.get("occurMsg"));
					bs.setMessage(datamap.get("messageName"));
				
			   }
			log.trace("N580_STEP1_result_service_bs >>{}", bs.getData());
			   }
			}
			catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("logout_taiwan_pay_step1 error >> {}",e);
		}
		return bs;
	}
	
	/**
	 * 線上註銷臺灣Pay掃碼提款功能
	 * 
	 * @param msgCode
	 * @return
	 */
	public BaseResult logout_taiwan_pay_step2(Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			log.trace("N580_step2 Service");
			log.trace(ESAPIUtil.vaildLog("N580_step2.reqParam: {}"+ reqParam));
			bs = new  BaseResult();
			bs = N580_1_REST(reqParam);
			Map<String,String>datamap=(Map<String, String>) bs.getData();
			// 切割ACN
			String CUSIDN = datamap.get("UID");
			String ACN = datamap.get("ACN");
			if(CUSIDN==null)
				CUSIDN = "";
			if(ACN==null)
				ACN = "";
			int i_count = ACN.length()/11;
			List<String> ACNTEMP = new ArrayList<>();
			for (int i=0;i<i_count;i++)
			{
				int j=i*11;
				ACNTEMP.add(ACN.substring(j,j+11));
			}
			bs.addData("ACNTEMP",ACNTEMP);
		    bs.addData("CUSIDN", CUSIDN);
		    bs.addData("ACN",ACN);
		    String DATE1 = datamap.get("DATE");
		    if(DATE1==null)
		    	DATE1="";
		    DATE1=String.valueOf(Integer.parseInt(DATE1.substring(0,3))+1911)+"/"+DATE1.substring(3,5)+"/"+DATE1.substring(5);
		    bs.addData("DATE1",DATE1);
		    String TIME1 = datamap.get("TIME");
		    if(TIME1==null)
		    	TIME1="";
		    TIME1 = TIME1.substring(0,2)+":"+TIME1.substring(2,4);
		    String DATETIME = DATE1+" "+TIME1;
		    bs.addData("DATETIME",DATETIME);
		    log.trace("N580_step2 Service result>>{}",bs.getData());
		    if((datamap.get("occurMsg").equals("0000")) ||(datamap.get("occurMsg").trim().equals(""))) {
				bs.setResult(true);
				}
			else {
				bs.setResult(false);
				bs.setMsgCode(datamap.get("occurMsg"));
				bs.setMessage(datamap.get("messageName"));
				
			   }
			}
			catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("logout_taiwan_pay_step2 error >> {}",e);
		}
		return bs;
	}
	
	
	/**
	 * 查詢分行回傳全部資料
	 * @param 
	 * @return
	 */
	public List getAllBh() {
		List all = null;
		try {
			all = admBranchDao.getAll();
		} catch (Exception e) {
			log.warn("無法取得分行資料.", e);
		}
		return all;
	}
	
	/**
	 * 取得縣市別
	 * @param 
	 * @return
	 */
	public List getCity() {
		List result = admZipDataDao.findCity();
		return result;
	}
	
	
	/**
	 * 取得區域
	 * @param 
	 * @return
	 */
	public BaseResult getArea(final String city) {
		BaseResult bs = new BaseResult();
		List result = admZipDataDao.findArea(city);
		bs.addData("Area", result);
		return bs;
	}	
	
	/**
	 * 取得郵遞區號依據縣市及區域
	 * @param 
	 * @return
	 */
	public BaseResult getZipCodeByCityArea(final String city, final String area) {
		BaseResult bs = new BaseResult();
		String result = admZipDataDao.findZipBycityarea(city, area);
		bs.addData("Zip", result);
		return bs;
	}		
	
	/**
	 * 查詢分行(郵遞區號內)
	 * @param 
	 * @return
	 */
	public BaseResult getZipBh(String ZIPCODE) {
		BaseResult bs = new BaseResult();
		log.debug(ESAPIUtil.vaildLog("zip >>{}"+ ZIPCODE));
		List result = admBranchDao.findZipBh(ZIPCODE); 
		if(result.size() <= 0) {
			result = getAllBh();
		}
		bs.addData("bh", result);
		return bs;
	}
	
	/**
	 * 查詢分行
	 * @param 
	 * @return
	 */
	public BaseResult getCityBh(String CITY) {
		BaseResult bs = new BaseResult();
		log.debug(ESAPIUtil.vaildLog("zip >>{}"+ CITY));
		if(CITY.indexOf("臺") != -1) {
			CITY = CITY.replace("臺", "台");
		}
		List result = admBranchDao.findCityBh(CITY); 
		if(result.size() <= 0) {
			result = getAllBh();
		}
		bs.addData("bh", result);
		return bs;
	}
	
	public void uploadImage(String filePath,String fileName,String updateFileName) {
		TXNLOANAPPLY attrib = new TXNLOANAPPLY();
		FTPUtil ftputil = new FTPUtil();
		Base64 base64 = new Base64();
		try {
			log.debug(ESAPIUtil.vaildLog("updateFileName >> "+updateFileName));
			//修改 Absolute Path Traversal
		    String validFileName = ESAPIUtil.GetValidPathPart(filePath,fileName);
			
			try {
				byte[] bytes = Files.readAllBytes(Paths.get(validFileName));
				
				String isSessionID = SpringBeanFactory.getBean("isSessionID");
				//LOCAL
				if("N".equals(isSessionID)){
					FTPClient ftpclient = ftputil.connect("localhost",21,"root","123456",30000);
					boolean result = ftputil.uploadFile(ftpclient,updateFileName,bytes);
					ftputil.finish(ftpclient);
					
				}else{
					Map<String, String> ftp = SpringBeanFactory.getBean("ftp");
					String use = ftp.get("use");
					String ip = ftp.get("ip");
					int port = Integer.parseInt(ftp.get("port"));
					String name = ftp.get("name");
					String pw = ftp.get("password");
					String path = ftp.get("path");
					String decodeString = new String(base64.decode(pw.getBytes()));
					int timeout = Integer.parseInt(ftp.get("timeout"));
					if("Y".equals(use)){
			        	FTPClient ftpclient = ftputil.connect(ip,port,name,decodeString,timeout);
						ftputil.ChangeToDirectory(ftpclient,path);
						String FILE1 = updateFileName;
						log.debug(ESAPIUtil.vaildLog("FILE1={}"+FILE1));
						
						boolean result = ftputil.uploadFile(ftpclient,FILE1,bytes);
						log.info(ESAPIUtil.vaildLog("「"+FILE1+"」上傳"+ result));
						ftputil.finish(ftpclient);
		
			        }
				}
			}catch(Exception e){
				log.error("uploadImage error >> {}", e.toString());
				log.warn(ESAPIUtil.vaildLog("圖檔上傳失敗"+updateFileName));
			}
		}catch(Exception e) {
			log.error("uploadImage error >> {}", e.toString());
			log.warn(ESAPIUtil.vaildLog("圖檔上傳失敗"+updateFileName));
		}
		
	}
	
	/**
	 * 圖片檢核
	 * @param file
	 * @param fileName
	 * @param fileContentType
	 * @param allowImageType
	 * @param allowImageWidth
	 * @param allowImageHeight
	 * @param allowImageSize
	 * @return
	 */
	public Map<String,String> checkImage(MultipartFile file,String originalFileName,String fileContentType,String[] allowImageType,
											int allowImageWidth,int allowImageHeight,int allowImageSize, String rootPath, String fileName){
		Map<String,String> returnMap = new HashMap<String,String>();
		returnMap.put("result","FALSE");
		returnMap.put("summary","");
		try{
			//判斷格式
			log.trace("fileContentType >> {}", fileContentType);
			for(int x=0;x<allowImageType.length;x++){
				if(allowImageType[x].equals(fileContentType)){
					break;
				}
				if(x == allowImageType.length - 1){
					returnMap.put("summary",i18n.getMsg("LB.X1810"));//圖示格式不符
				}
			}
			//判斷大小
			if(file.getSize() > allowImageSize){
				returnMap.put("summary",i18n.getMsg("LB.X1811") + allowImageSize / 1024 + "KB");//圖示檔案過大，不能超過
			}
			//判斷檔名是否過長
			if(originalFileName.length() > 300){
				returnMap.put("summary",i18n.getMsg("LB.X1812"));//圖示檔名過長
			}
			//判斷副檔名是否過長
			if(FilenameUtils.getExtension(originalFileName).length() > 10){
				returnMap.put("summary",i18n.getMsg("LB.X1813"));//圖示副檔名過長
			}

			if("".equals(returnMap.get("summary"))){
				
				log.debug("allowImageWidth={}",allowImageWidth);
				log.debug("allowImageHeight={}",allowImageHeight);
				if(file.getSize() <= allowImageSize){
					BufferedImage bufferedImage = ImageIO.read(file.getInputStream());
					//檢查檔案尺寸，不合格式就幫使用者改變尺寸
					BufferedImage bufferedResizedImage = getScaledInstance(bufferedImage,640,480,RenderingHints.VALUE_INTERPOLATION_BICUBIC,true);
			
					int width = bufferedResizedImage.getWidth();
					int height = bufferedResizedImage.getHeight();
					
					log.debug("imageWidth={}",width);
					log.debug("imageHeight={}",height);
					String validatePath = ESAPIUtil.vaildPathTraversal2(rootPath,fileName);
					log.trace(ESAPIUtil.vaildLog("validatePath >> {}"+validatePath));
					String fileTyle = validatePath.substring(validatePath.lastIndexOf(".")+1,validatePath.length()); 
					log.debug(ESAPIUtil.vaildLog("fileTyle >> {}"+fileTyle));
					// 修改 Relative Path Traversal
					File outputfile = new SafeFile(validatePath);
					outputfile.setWritable(true, true);
					outputfile.setReadable(true, true);
					outputfile.setExecutable(true, true);
					ImageIO.write(bufferedImage,fileTyle,outputfile);
//					OutputStream os = Files.newOutputStream(Paths.get(filePath), new OpenOption[] { StandardOpenOption.WRITE, StandardOpenOption.CREATE });
//					ImageIO.write(bufferedImage, fileTyle, os);
//					os.flush();
//					os.close();
					
					//檢核通過
					returnMap.put("result","TRUE");
				}
			}
		}
		catch(Exception e){
			if(returnMap.get("summary").equals("")) {
				returnMap.put("summary",i18n.getMsg("LB.X2131"));
			}
//			returnMap.put("message",String.valueOf(e.getMessage()));
		}
		return returnMap;
	}

	/**
	 * 圖片檢核
	 * @param file
	 * @param fileName
	 * @param fileContentType
	 * @param allowImageType
	 * @param allowImageWidth
	 * @param allowImageHeight
	 * @param allowImageSize
	 * @return
	 */
	public Map<String,String> checkImageOnlyImg(MultipartFile file,String originalFileName,String fileContentType,String[] allowImageType,
											int allowImageWidth,int allowImageHeight,int allowImageSize, String rootPath, String fileName){
		Map<String,String> returnMap = new HashMap<String,String>();
		returnMap.put("result","FALSE");
		returnMap.put("summary","");
		try{
			//判斷格式
			//log.trace("fileContentType >> {}", fileContentType);
			for(int x=0;x<allowImageType.length;x++){
				if(allowImageType[x].equals(fileContentType)){
					break;
				}
				if(x == allowImageType.length - 1){
					returnMap.put("summary",i18n.getMsg("LB.X1810"));//圖示格式不符
				}
			}
			if(!fileName.substring(fileName.lastIndexOf(".")+1,fileName.length()).equals("jpg")) {
				returnMap.put("summary",i18n.getMsg("LB.X1810"));//圖示格式不符
			}
			//判斷大小
			if(file.getSize() > allowImageSize){
				returnMap.put("summary",i18n.getMsg("LB.X1811") + allowImageSize / 1024 + "KB");//圖示檔案過大，不能超過
			}
			//判斷檔名是否過長
			if(originalFileName.length() > 300){
				returnMap.put("summary",i18n.getMsg("LB.X1812"));//圖示檔名過長
			}
			//判斷副檔名是否過長
			if(FilenameUtils.getExtension(originalFileName).length() > 10){
				returnMap.put("summary",i18n.getMsg("LB.X1813"));//圖示副檔名過長
			}

			if("".equals(returnMap.get("summary"))){
				
				log.debug("allowImageWidth={}",allowImageWidth);
				log.debug("allowImageHeight={}",allowImageHeight);
				if(file.getSize() <= allowImageSize){
					BufferedImage bufferedImage = ImageIO.read(file.getInputStream());
					//檢查檔案尺寸，不合格式就幫使用者改變尺寸
					BufferedImage bufferedResizedImage = getScaledInstance(bufferedImage,640,480,RenderingHints.VALUE_INTERPOLATION_BICUBIC,true);
			
					int width = bufferedResizedImage.getWidth();
					int height = bufferedResizedImage.getHeight();
					
					log.debug("imageWidth={}",width);
					log.debug("imageHeight={}",height);
					String validatePath = ESAPIUtil.vaildPathTraversal2(rootPath,fileName);
					log.trace(ESAPIUtil.vaildLog("validatePath >> {}"+validatePath));
					String fileTyle = validatePath.substring(validatePath.lastIndexOf(".")+1,validatePath.length()); 
					log.debug(ESAPIUtil.vaildLog("fileTyle >> {}"+fileTyle));
					// 修改 Relative Path Traversal
					File outputfile = new SafeFile(validatePath);
					outputfile.setWritable(true, true);
					outputfile.setReadable(true, true);
					outputfile.setExecutable(true, true);
					ImageIO.write(bufferedImage,fileTyle,outputfile);
//					OutputStream os = Files.newOutputStream(Paths.get(filePath), new OpenOption[] { StandardOpenOption.WRITE, StandardOpenOption.CREATE });
//					ImageIO.write(bufferedImage, fileTyle, os);
//					os.flush();
//					os.close();
					
					//檢核通過
					returnMap.put("result","TRUE");
					log.info(ESAPIUtil.vaildLog("圖檔落地成功:"+validatePath));
				}
			}
		}
		catch(Exception e){
			if(returnMap.get("summary").equals("")) {
				returnMap.put("summary",i18n.getMsg("LB.X2131"));
			}
//			returnMap.put("message",String.valueOf(e.getMessage()));
		}
		return returnMap;
	}
	/**
	 * 調整上傳的圖片尺寸
	 * @param image
	 * @param limitedWidth
	 * @param limitedHeight
	 * @param hint
	 * @param higherQuality
	 * @return
	 */
	public BufferedImage getScaledInstance(BufferedImage image,Integer limitedWidth,Integer limitedHeight,Object hint,boolean higherQuality){
		int initialwidth;
		int initialheight;
		
		int targetWidth;
		int targetHeight;
		
		int finalWidth;
		int finalHeight;
		
		float rate;
		
		int type;
		BufferedImage returnImage = null;
		
		try{
			initialwidth = image.getWidth();
			initialheight = image.getHeight();
			type = (image.getTransparency() == Transparency.OPAQUE) ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
			returnImage = (BufferedImage)image;
			
			//檢查使用者上傳的圖檔寬、高符不符合格式
			
			//寬高都符合規定
			if(initialheight <= limitedHeight && initialwidth <= limitedWidth){
				targetHeight = initialheight;
				targetWidth = initialwidth;
			}
			//寬符合規定，高不符合
			else if(initialheight > limitedHeight && initialwidth <= limitedWidth){
				rate = (float)initialheight / limitedHeight;
				targetHeight = limitedHeight;
				targetWidth = (int)((float)initialwidth / rate);
			}
			//高符合規定，寬不符合
			else if(initialheight <= limitedHeight && initialwidth > limitedWidth){
				rate = (float)initialwidth / limitedWidth;
				targetHeight = (int)((float)initialheight / rate);
				targetWidth = limitedWidth;
			}
			//寬高都不符合規定
			else{
				//原圖高寬比=限制高寬比
				if((float)initialheight / initialwidth == (float)limitedHeight / limitedWidth){
					targetHeight = limitedHeight;
					targetWidth = limitedWidth;
				}
				//原圖高寬比>限制高寬比
				else if((float)initialheight / initialwidth > (float)limitedHeight / limitedWidth){
					rate = (float)initialheight / limitedHeight;
					targetHeight = limitedHeight;
					targetWidth = (int)((float)initialwidth / rate);
				}
				//原圖高寬比<限制高寬比
				else{
					rate = (float)initialwidth / limitedWidth;
					targetHeight = (int)((float)initialheight / rate);
					targetWidth = limitedWidth;
				}
			}
			
			//判斷是否需要高品質的縮圖
			if(higherQuality) {
				finalWidth = image.getWidth();
				finalHeight = image.getHeight();
			}
			else{
				finalWidth = targetWidth;
				finalHeight = targetHeight;
			}
			do{
				if(higherQuality && finalWidth > targetWidth){
					finalWidth /= 2;
					if(finalWidth < targetWidth){
						finalWidth = targetWidth;
					}
				}
				if(higherQuality && finalHeight > targetHeight){
					finalHeight /= 2;
					if(finalHeight < targetHeight){
						finalHeight = targetHeight;
					}
				}
				
				BufferedImage tmp = new BufferedImage(finalWidth,finalHeight,type);
				Graphics2D g2 = tmp.createGraphics();
				g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION,hint);
				g2.drawImage(returnImage,0,0,finalWidth,finalHeight,null);
				g2.dispose();
				returnImage = tmp;
			}
			while(finalWidth != targetWidth || finalHeight != targetHeight);
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getScaledInstance error >> {}",e);
		}
		return returnImage;
	}

	/**
	 * 刪除圖片
	 * @param localPath
	 * @return
	 */
	public boolean removefile(String rootPath,String fileName, String movePath, String moveFileName) {
		String validatePath = "";
		String validateMovePath = "";
		try
		{
			//修改 Absolute Path Traversal
			validatePath = ESAPIUtil.GetValidPathPart(rootPath,fileName);
			//log.trace("movePath >> "+movePath+", moveFileName >> "+moveFileName);
			if(!movePath.equals("") && !moveFileName.equals("")) {
				validateMovePath = ESAPIUtil.vaildPathTraversal2(movePath,moveFileName);
			}
			//log.trace("validateMovePath >> {}",validateMovePath);
			
			try {
				//修改 Incorrect Permission Assignment For Critical Resources
				if(!validateMovePath.equals("")) {
					// CheckMax版本提升後又被掃出Incorrect Permission Assignment For Critical Resources
//					Files.copy(Paths.get(validatePath), Paths.get(validateMovePath));
					
					// 修正Incorrect Permission Assignment For Critical Resources
					// 創建母目錄如果目錄不存在
					File file = new File(movePath);
					file.setWritable(true,true);
					file.setReadable(true,true);
					file.setExecutable(true,true);
					file.mkdirs();
					
					// 讀取source檔案
					byte[] srcFile = Files.readAllBytes(Paths.get(validatePath));
					
					// 產生目標檔案
					BufferedOutputStream fw = new BufferedOutputStream(new FileOutputStream(validateMovePath));
			        fw.write(srcFile);
			        fw.flush();
			        fw.close();
					
					log.trace(ESAPIUtil.vaildLog("複製圖檔成功:"+validatePath));
				}
				Files.delete(Paths.get(validatePath));
				log.warn(ESAPIUtil.vaildLog("刪除圖檔成功:"+validatePath));
				return(true);
			}
			catch(Exception RemoveFileEx)
			{
				log.error(RemoveFileEx.getMessage());
				log.warn("刪除圖檔失敗"+validatePath);
				return(false);
			}		
		}
		catch(Exception RemoveFileEx)
		{
			log.error(RemoveFileEx.getMessage());
			log.warn("刪除圖檔失敗"+validatePath);
			return(false);
		}		
	}
	
	public BaseResult apply_deposit_account_result(Map<String, String> reqParam, String uploadPath) {
		String uploadPath_N201 = uploadPath;
		String separ = File.separator;
		BaseResult bs = null;
		try {
			bs = new BaseResult();
//			reqParam.put("CHGE_DT", DateUtil.convertDate(1,reqParam.get("CHGE_DT"),"yyyyMMdd","yyyMMdd"));
			if(reqParam.get("MAILADDR") != null)
				reqParam.put("MAILADDR", reqParam.get("MAILADDR").toLowerCase());
			bs = N201_REST(reqParam);
			if(bs != null && bs.getResult()) {
				Map<String,Object> callData = (Map<String,Object>)bs.getData();
				callData.put("DUEDT", DateUtil.addSlash2((String)callData.get("DUEDT")));
				callData.put("WEEK", DateUtil.date2DayofWeekII(DateUtil.convertDate(1,(String)callData.get("DUEDT"),"yyy/MM/dd","yyyy-MM-dd"), "yyyy-MM-dd"));
				List bh = admBranchDao.findByBhID(reqParam.get("BRHCOD"));
				log.debug(ESAPIUtil.vaildLog("bh >> " + bh.get(0)));
				bs.addData("BRHNAME", ((ADMBRANCH)bh.get(0)).getADBRANCHNAME());
				bs.addData("BRHTEL", ((ADMBRANCH)bh.get(0)).getTELNUM());
//				if(!reqParam.get("FILE1").equals("")) {
//					String filetype = reqParam.get("FILE1").substring(reqParam.get("FILE1").lastIndexOf(".") ,reqParam.get("FILE1").length());
//					uploadImage(uploadPath_N201, reqParam.get("FILE1"), reqParam.get("UID") + "_01" + filetype);
//					removefile(uploadPath_N201, reqParam.get("FILE1"), SpringBeanFactory.getBean("imgMovePath") + separ, reqParam.get("UID") + "_01_" + DateUtil.getDate("") + filetype);
//				}
//				if(!reqParam.get("FILE2").equals("")) {
//					String filetype = reqParam.get("FILE2").substring(reqParam.get("FILE2").lastIndexOf(".") ,reqParam.get("FILE2").length());
//					uploadImage(uploadPath_N201, reqParam.get("FILE2"), reqParam.get("UID") + "_02" + filetype);
//					removefile(uploadPath_N201, reqParam.get("FILE2"), SpringBeanFactory.getBean("imgMovePath") + separ, reqParam.get("UID") + "_02_" + DateUtil.getDate("") + filetype);
//				}
//				if(!reqParam.get("FILE3").equals("")) {
//					String filetype = reqParam.get("FILE3").substring(reqParam.get("FILE3").lastIndexOf(".") ,reqParam.get("FILE3").length());
//					uploadImage(uploadPath_N201, reqParam.get("FILE3"), reqParam.get("UID") + "_03" + filetype);
//					removefile(uploadPath_N201, reqParam.get("FILE3"), SpringBeanFactory.getBean("imgMovePath") + separ, reqParam.get("UID") + "_03_" + DateUtil.getDate("") + filetype);
//				}
//				if(!reqParam.get("FILE4").equals("")) {
//					String filetype = reqParam.get("FILE4").substring(reqParam.get("FILE4").lastIndexOf(".") ,reqParam.get("FILE4").length());
//					uploadImage(uploadPath_N201, reqParam.get("FILE4"), reqParam.get("UID") + "_04" + filetype);
//					removefile(uploadPath_N201, reqParam.get("FILE4"), SpringBeanFactory.getBean("imgMovePath") + separ, reqParam.get("UID") + "_04_" + DateUtil.getDate("") + filetype);
//				}
				
			}
		}catch(Exception e) {
			log.error(e.toString());
		}
		return bs;
	}
	
	/**
	 * 晶片金融卡申請網路銀行
	 * 
	 * @param msgCode
	 * @return
	 */
	public BaseResult use_component_step1(Map<String, String> reqParam) {
		BaseResult bs = null;
		String result="";
		try {
			log.trace("start use_component_step1_idcheck Service");
			log.trace(ESAPIUtil.vaildLog("use_component_step1_idcheck REQPARAM >>{}"+reqParam));
			bs = new  BaseResult();
			List<ADMBRANCH> bhContact;
			List<Map<String,String>> admbranchdata=new ArrayList<>();
			bhContact=admBranchDao.getAll();
			Map<String,String> tempMap=new HashMap();
			CodeUtil.convert2BaseResult(bs,tempMap);
			bs.setResult(true);
			bs.addAllData(reqParam);
			if(!bhContact.isEmpty()) {
				for(ADMBRANCH row :bhContact) {
					Map<String,String>temp=CodeUtil.objectCovert(Map.class,row);
					Map<String,String>BNBC=new HashMap();
					BNBC.put("ADBRANCHID", temp.get("ADBRANCHID"));
					BNBC.put("ADBRANCHNAME", temp.get("ADBRANCHNAME"));
					BNBC.put("ADBRANENGNAME", temp.get("ADBRANENGNAME"));	
					BNBC.put("ADBRANCHSNAME", temp.get("ADBRANCHSNAME"));	
					admbranchdata.add(BNBC);
				}
				bs.addData("REC",admbranchdata);
			}
			log.trace("BS DATA >> "+ CodeUtil.toJson(bs));
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("use_component_step1 error >> {}",e);
		}
		return bs;
	}
	/**
	 * 晶片金融卡申請網路銀行
	 * 
	 * @param msgCode
	 * @return
	 */
	public BaseResult use_component_step1_idcheck(Map<String, String> reqParam) {
		BaseResult bs = null;
		String result="";
		try {
			log.trace("start use_component_step1_idcheck Service");
			log.trace("use_component_step1_idcheck.reqParam: {}", reqParam);
			bs = new  BaseResult();
			List<ADMBRANCH> bhContact;
			List datalist = new ArrayList();
			
			bs=N960_REST_UPDATE_CUSNAME(reqParam);
			Map<String,Object>datamap=(Map<String, Object>) bs.getData();
			String ALLBRHDATA= (String)datamap.get("BRHCOD");
			log.trace("ALLBRHDATA >> "+ALLBRHDATA);
			if(ALLBRHDATA!=null && ALLBRHDATA.length()>=3 ) {
				int i_Count = (ALLBRHDATA.trim()).length()/3;		
				log.trace("i_Count >> "+i_Count);
				String BRHCOD="";
				int indexstart=0;
				int indexend=0;	
				for (int i=0; i<i_Count; i++)
				{	
					indexstart=indexend;
					indexend+=3;	
					BRHCOD=(ALLBRHDATA.trim()).substring(indexstart,indexend);
					log.trace("BRHCOD >> "+BRHCOD);
					bhContact=admBranchDao.findByBhID(BRHCOD);
					if(bhContact.size()>0) {
						log.trace(ESAPIUtil.vaildLog("bhContact >> "+bhContact));
						ADMBRANCH temp=bhContact.get(0);
						log.trace(ESAPIUtil.vaildLog("temp >> "+temp));
						String BRHNAME = (String) CodeUtil.objectCovert(Map.class,temp).get("ADBRANCHNAME");
						Map m = new HashMap();
						if(BRHNAME==null || (BRHNAME.trim()).length()==0){
							continue;
						}
						else {
							m.put("ADBRANCHID", BRHCOD);	
							m.put("ADBRANCHNAME", BRHNAME);	
							m.put("ADBRANENGNAME", (String) CodeUtil.objectCovert(Map.class,temp).get("ADBRANENGNAME"));	
							m.put("ADBRANCHSNAME", (String) CodeUtil.objectCovert(Map.class,temp).get("ADBRANCHSNAME"));	
							datalist.add(m);
						}
					}
				}
			}else {
				bhContact=admBranchDao.getAll();
				for(ADMBRANCH row :bhContact) {
					Map<String,String>temp=CodeUtil.objectCovert(Map.class,row);
					Map<String,String>BNBC=new HashMap();
					BNBC.put("ADBRANCHID", temp.get("ADBRANCHID"));
					BNBC.put("ADBRANCHNAME", temp.get("ADBRANCHNAME"));
					BNBC.put("ADBRANENGNAME", temp.get("ADBRANENGNAME"));	
					BNBC.put("ADBRANCHSNAME", temp.get("ADBRANCHSNAME"));	
					datalist.add(BNBC);
				}
			}
			Map<String,Object> tempresult=new HashMap<>();
			tempresult.put("Bank",datalist);
			datamap.putAll(tempresult);
			bs.setResult(true);
			log.trace("use_component_step1_idcheck >>{}", CodeUtil.toJson(bs));
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("use_component_step1_idcheck error >> {}",e);
		}
		return bs;
	}
	/**
	 * 晶片金融卡申請網路銀行
	 * 
	 * @param msgCode
	 * @return
	 */
	public String use_component_step1_AUTH(Map<String, String> reqParam) {
		BaseResult bs = null;
		String result="";
		try {
			log.trace("start use_component_step1_AUTH Service");
			log.trace("use_component_step1_AUTH.reqParam: {}", reqParam);
			bs = new  BaseResult();
			List<TXNUSER> user=txnUserDao.findByUserId(reqParam.get("CUSIDN"));
			if(!user.isEmpty()){
				TXNUSER userdata=user.get(0);
				result=userdata.getCCARDFLAG();
				log.trace(ESAPIUtil.vaildLog("use_component_step1_AUTH >>{}"+result));
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("use_component_step1_AUTH error >> {}",e);
		}
		return result;
	}
	/**
	 * 晶片金融卡申請網路銀行
	 * 
	 * @param msgCode
	 * @return
	 */
	public BaseResult use_component_confirm(Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			log.trace("start use_component_confirm Service");
			log.trace(ESAPIUtil.vaildLog("use_component_confirm.reqParam: {}"+ reqParam));
			bs = new  BaseResult();
			String dateyymmdd = reqParam.get("CMTRDATE");
			String[] dataarray=dateyymmdd.split("/");
			log.trace(ESAPIUtil.vaildLog(("dataarray >> "+dataarray)));
			String dateyy=dataarray[0];
			dataarray=dateyymmdd.split("/");
			String datemm=dataarray[1];
			dataarray=dateyymmdd.split("/");
			String datedd=dataarray[2];
			reqParam.put("CCBIRTHDATEYY",dateyy);
			reqParam.put("CCBIRTHDATEMM",datemm);
			reqParam.put("CCBIRTHDATEDD",datedd);
			reqParam.put("_CUSIDN",reqParam.get("CUSIDN"));
			reqParam.put("MOBILE",reqParam.get("PHONE_H"));
			reqParam.put("CChargeApply",reqParam.get("ATMTRAN1") );
			reqParam.put("icSeq",reqParam.get("iSeqNo") );
			log.trace(ESAPIUtil.vaildLog("NA30RQ >>"+reqParam));
			bs = NA30_REST(reqParam);
			log.trace("NA30RQ bsdata >>"+CodeUtil.toJson(bs));
			Map<String,String>datamap=(Map<String, String>) bs.getData();
			datamap.putAll(reqParam);
			if((datamap.get("occurMsg").equals("0000")||datamap.get("occurMsg").equals(""))&&datamap.get("msgCode").equals("0")) {
				bs.setResult(true);}
			else {
				bs.setResult(false);
				bs.setMsgCode(datamap.get("msgCode"));
				bs.setMessage(datamap.get("msgName"));
				bs.setPrevious("/ONLINE/APPLY/use_component");

			}
			log.trace("NA30_result_service_bs >>{}", bs.getData());
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("use_component_confirm error >> {}",e);
		}
		return bs;
	}
	public BaseResult use_component_result(Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			log.trace("start use_component_step1 Service");
			log.trace(ESAPIUtil.vaildLog("use_component_step1.reqParam: {}"+ reqParam));
			bs = new  BaseResult();
			bs = NA30_1_REST(reqParam);
			Map<String,String>datamap=(Map<String, String>) bs.getData();
			if((datamap.get("occurMsg").equals("0000")||datamap.get("occurMsg").equals(""))&& datamap.get("msgCode").equals("0")) {
				bs.setResult(true);
				bs.addAllData(reqParam);
			}
			else {
				bs.setResult(false);
				bs.setMsgCode(datamap.get("msgCode"));
				bs.setMessage(datamap.get("msgName"));
				bs.setPrevious("/ONLINE/APPLY/use_component");

			}
			log.trace("NA30_result_service_bs >>{}", bs.getData());
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("use_component_result error >> {}",e);
		}
		return bs;
	}
	
	/**
	 * NA40使用臺灣企銀信用卡(step2)
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult use_creditcard_step2(String cusidn, Map<String, String> reqParam) {
		log.trace("use_creditcard_step2  service");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = NA40_REST(cusidn, reqParam);
			log.trace("get bs rs>>>>>{}",bs.getData());
			
			Map<String,String>datamap=(Map<String, String>) bs.getData();
			datamap.putAll(reqParam);
		} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * NA40使用臺灣企銀信用卡(結果)
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult use_creditcard_result(String cusidn, Map<String, String> reqParam) {
		log.trace("use_creditcard_result  service");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = NA40_1_REST(cusidn, reqParam);
			log.trace("get bs rs>>>>>{}",bs.getData());
			
			Map<String,String>datamap=(Map<String, String>) bs.getData();
			datamap.putAll(reqParam);
			
			String notify = "";
			if ("Y".equals(datamap.get("NOTIFY_CCARDPAY"))) {
				//信用卡繳款通知
				notify = notify + I18n.getMessage("LB.X1402");

			}

			if ("Y".equals(datamap.get("NOTIFY_ACTIVE"))) {
				if (!"".equals(notify)) {
					//網路銀行相關服務及優惠訊息
					notify = notify + "、" + I18n.getMessage("LB.X1403");
				} else {
					notify = notify + I18n.getMessage("LB.X1403");
				}
			}

			if ("Y".equals(datamap.get("NOTIFY_CCARDBILL"))) {
				if (!"".equals(notify)) {
					//申請信用卡電子帳單
					notify = notify + "、" + I18n.getMessage("LB.D1038");
				} else {
					notify = notify + I18n.getMessage("LB.D1038");
				}
			}

			if (!"".equals(notify)) {
				notify = notify + "。";
			}
			datamap.put("notify", notify);
		} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * 預約開立基金戶MAIL驗證 2021/01/19
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult checkEmpMail(String cusidn, String email) {
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = C011_REST(cusidn, email);
			Map<String,String> c011map =(Map<String, String>) bs.getData();
			
			bs.reset();
			bs.setResult(true);
			bs.setMsgCode("0");
			
			String fundemailflag=c011map.get("FLAG")==null ? "" : c011map.get("FLAG");
			//TRUE : DB有其他人使用這個EMAIL , FALSE : 可使用
			boolean checkEmailDup = txnUserDao.checkEmailDup(cusidn,email);
			//TRUE : 是行員 , FALSE : 不是行員
			boolean checkEmp = admEmpInfoDao.isEmpCUSID(cusidn);
			boolean checkEmpMail=false;
			//如果MAIL是行員信箱 , 則判斷是否有其他相同EMAIL 
			if(email.indexOf("mail.tbb.com.tw")>-1) {
				//TRUE : 是本人  , FLASE : 非本人
				checkEmpMail=admEmpInfoDao.isEmployeeAndEmpMail(cusidn,email);
			}
			if(checkEmailDup||"Y".equals(fundemailflag)) {
				bs.setMessage("EMAIL與他人相同，請改用其他EMAIL信箱");
				bs.setResult(false);
			}
			if((checkEmpMail==false||"1".equals(fundemailflag)) && email.indexOf("mail.tbb.com.tw")>-1) {
				bs.setMessage("行員信箱非本人所有，請使用其他EMAIL信箱");
				bs.setResult(false);
			}
		} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
	
	public Boolean checkApply(Map<String, String> reqParam) {
		Boolean result = false;
		String cusidn = reqParam.get("CUSIDN");
		String cardnum = reqParam.get("CARDNUM");
		List<TXNCRLINEAPP> checkApply = txnCrLineAppDao.findByAll(cusidn, cardnum);
		if (checkApply.size() > 0) {
			try {
				String today = DateUtil.getCurentDateTime("yyyyMMdd");
				String getdate = checkApply.get(0).getCPRIMDATE();
				SimpleDateFormat sdFormat = new SimpleDateFormat("yyyyMMdd");
				Date checkdate = sdFormat.parse(getdate);
				String checkdates = new DateTime(checkdate).plusDays(30).toString("yyyyMMdd");
				if(DateUtil.time2compare(checkdates, today, "yyyyMMdd")) {
					result = true;
				}
			}
			catch (Exception e) {
				// TODO: handle exception
				log.debug("error>>>"+e);
			}
		}
		else {
			result = true;
		}
		return result;
	}
	
	/**
	 * N822
	 * @param reqParam
	 * @return
	 */
	public BaseResult creditcard_limit_increase_otp(Map<String, String> reqParam) {
		log.debug("creditcard_limit_increase_otp Service Start");
		BaseResult bs = null ;
		try {
			bs = new BaseResult();
			Boolean result = checkApply(reqParam);
			if(result) {
				bs = N822_REST(reqParam);
				//test
//				bs.setResult(true);
				if(bs != null && bs.getResult()) {
					//test
//					bs.addData("PHONE", "0912345678");
//					bs.addData("NAME", "XXX");
//					bs.addData("LIMIT", "00100000");
					
					Map<String,String> datamap = (Map<String, String>) bs.getData();
					String phone = datamap.get("PHONE");
					if(StrUtils.isNotEmpty(phone)) {
						phone = phone.substring(0,4) + "-XXX-X" + phone.substring(8);
						bs.addData("PHONEshow", phone);
					}
					else {
						bs.addData("PHONEshow", "");
						bs.addData("errType", "3");
						bs.setResult(false);
					}
					
					String name = datamap.get("NAME");
					if(StrUtils.isNotEmpty(name)) {
						name = name.substring(0,1) + "Ｏ" + name.substring(2);
						bs.addData("NAMEshow", name);
					}
					else {
						bs.addData("NAMEshow", "");
					}
					
					String limit = NumericUtil.fmtAmount(datamap.get("LIMIT"), 0);
					if(StrUtils.isNotEmpty(limit)) {
						bs.addData("LIMIT", NumericUtil.unfmtAmount(limit));
						bs.addData("LIMITshow", limit);
					}
					else {
						bs.addData("LIMITshow", "");
					}
					
					String cusidn = reqParam.get("CUSIDN");
					if(StrUtils.isNotEmpty(cusidn)) {
						cusidn = cusidn.substring(0,4) + "*****" + cusidn.substring(9);
						bs.addData("CUSIDNshow", cusidn);
					}
					else {
						bs.addData("CUSIDNshow", "");
					}
					
					String version1 = reqParam.get("CheckBox1");
					if(version1.equals("on")) {
						bs.addData("VERSION1", "109.12");
					}
					
					String version2 = reqParam.get("CheckBox2");
					if(version2.equals("on")) {
						bs.addData("VERSION2", "109.11");
					}
				}
				else {
					if(!bs.getMsgCode().startsWith("FE")) {
						bs.addData("errType", "1");
					}
				}
				log.debug("bs data >>>"+bs.getData());
			}
			else {
				bs.addData("errType", "4");
				bs.setResult(false);
			}
		} catch (Exception e) {
			log.error("creditcard_limit_increase_otp>> {}", e);
		}
		return  bs;
	}
	
	/**
	 * N822
	 * @param reqParam
	 * @return
	 */
	public BaseResult creditcard_limit_increase_p3(Map<String, String> reqParam) {
		log.debug("creditcard_limit_increase_p3 Service Start");
		BaseResult bs = null ;
		try {
			bs = new BaseResult();
			//驗證簡訊
			reqParam.put("FuncType", "1");
			bs = SmsOtp_REST(reqParam);
			
			if(bs.getResult()) {
				log.info("簡訊驗證通過...");
				bs.addData("CHKID", " ");
			}else {
				log.error("簡訊驗證失敗...");
			}
			
		} catch (Exception e) {
			log.error("creditcard_limit_increase_p3>> {}", e);
		}
		return  bs;
	}
	
	/**
	 * N822
	 * @param reqParam
	 * @return
	 */
	public BaseResult creditcard_limit_increase_p3_1(Map<String, String> reqParam) {
		log.debug("creditcard_limit_increase_p3_1 Service Start");
		BaseResult bs = null ;
		try {
			//打eloan判斷是否要上傳財力
			bs = new BaseResult();
			bs = EloanApi_REST(reqParam);
			//test
//			bs.setResult(true);
			if(bs != null && bs.getResult()) {
				String REASON_VIEW = "";
				switch (reqParam.get("REASON"))
				{
				case "1":
					REASON_VIEW = i18n.getMsg("LB.W1740");
					break;
				case "2":
					REASON_VIEW = i18n.getMsg("LB.W1741");
					break;
				case "3":
					REASON_VIEW = i18n.getMsg("LB.W1742");
					break;
				case "4":
					REASON_VIEW = i18n.getMsg("LB.W0367");
					break;
				case "5":
					REASON_VIEW = i18n.getMsg("LB.W1743");
					break;
				case "7":
					REASON_VIEW = i18n.getMsg("LB.W1744");
					break;
				case "8":
					REASON_VIEW = i18n.getMsg("LB.W1745");
					break;
				case "9":
					REASON_VIEW = i18n.getMsg("LB.D0572");
					break;
				}
				
				bs.addData("REASON_VIEW", REASON_VIEW);
				
				String FGTXSTATUS_VIEW = "";
				if("1".equals(reqParam.get("FGTXSTATUS"))) {
					FGTXSTATUS_VIEW = reqParam.get("CMSDATE")+"~"+reqParam.get("CMEDATE")+"(" + i18n.getMsg("LB.D1608_1") + ")";
				}else {
					FGTXSTATUS_VIEW = i18n.getMsg("LB.W1748");
				}
				bs.addData("FGTXSTATUS_VIEW", FGTXSTATUS_VIEW);
				
				String QUOTA_VIEW = NumericUtil.fmtAmount(reqParam.get("QUOTA"), 0);
				bs.addData("QUOTA_VIEW", QUOTA_VIEW);
				
			}
			
		} catch (Exception e) {
			log.error("creditcard_limit_increase_p3>> {}", e);
		}
		return  bs;
	}
	
	/**
	 * N822
	 * @param reqParam
	 * @return
	 */
	public BaseResult creditcard_limit_increase_result(Map<String, String> reqParam) {
		log.debug("creditcard_limit_increase_result Service Start");
		BaseResult bs = null ;
		try {
			log.debug("result get data>>>"+reqParam);
			bs = new BaseResult();
			
			String cusidn = reqParam.get("CUSIDN");
			String cardnum = reqParam.get("CARDNUM");
			String name = reqParam.get("NAME");
			String crlinetpe = "";
			if(reqParam.get("FGTXSTATUS").equals("1")) {
				//臨時調升
				crlinetpe = "T";
			}
			else if(reqParam.get("FGTXSTATUS").equals("2")) {
				//永久調升
				crlinetpe = "P";
			}
			
			String date = DateUtil.getCurentDateTime("yyyyMMdd");
			String time = DateUtil.getCurentDateTime("HHmmss");
			log.debug("get date>>"+date +" get time>>"+time);
			int filecount = 0;
			for(int i = 3; i < 6; i++) {
				if(StrUtils.isNotEmpty(reqParam.get("FILE"+i))) {
					filecount++;
				}
			}
			
			TXNCRLINEAPP attrib = new TXNCRLINEAPP();
			try {
				if(StrUtils.isEmpty(cusidn) || StrUtils.isEmpty(cardnum) || StrUtils.isEmpty(name)) {
					throw new Exception();
				}
				else {
					attrib = CodeUtil.objectCovert(attrib.getClass(),reqParam);
					
					attrib.setCPRIMDATE(date);
					attrib.setCPRIMTIME(time);
					attrib.setCPRIMID(cusidn);
					attrib.setCPRIMCHNAME(name);
					attrib.setCARDNUM(cardnum);
					attrib.setOCRLINE(reqParam.get("LIMITshow"));
					attrib.setNCRLINE(reqParam.get("QUOTA_VIEW"));
					attrib.setCRLINETPE(crlinetpe);
					if(crlinetpe.equals("T")) {
						String CMSD = reqParam.get("CMSDATE").replaceAll("/", "");
						String CMED = reqParam.get("CMEDATE").replaceAll("/", "");
						attrib.setCRLINEBEGDATE(CMSD);
						attrib.setCRLINEENDDATE(CMED);
					}
					
					attrib.setUPFILES(String.valueOf(filecount));
					attrib.setCPRIMCELLULANO(reqParam.get("PHONE"));
					attrib.setIP(reqParam.get("IP"));
					attrib.setCHANTPE("NB");
					attrib.setVERSION1(reqParam.get("VERSION1"));
					attrib.setVERSION2(reqParam.get("VERSION2"));
					attrib.setCHKID(reqParam.get("CHKID"));
					attrib.setLASTDATE(date);
					attrib.setLASTTIME(time);
					
					txnCrLineAppDao.save(attrib);
				}
			}
			catch (Exception e) {
				// TODO: handle exception
				log.error("DAO SAVE ERROR",e);
				return bs;
			}
			
			Map<String,String> newParamMap = new HashMap<String,String>();
			try {
				newParamMap.put("CPRIMDATE", date);
				newParamMap.put("CPRIMTIME", time);
				newParamMap.put("CPRIMID", cusidn);
				newParamMap.put("CPRIMCHNAME", name);
				newParamMap.put("CARDNUM", cardnum);
				newParamMap.put("OCRLINE", reqParam.get("LIMITshow"));
				newParamMap.put("NCRLINE", reqParam.get("QUOTA_VIEW"));
				newParamMap.put("CRLINETPE", reqParam.get("FGTXSTATUS"));
				if(crlinetpe.equals("T")) {
					newParamMap.put("CRLINEBEGDATE", reqParam.get("CMSDATE").replaceAll("/", ""));
					newParamMap.put("CRLINEENDDATE", reqParam.get("CMEDATE").replaceAll("/", ""));
				}
				else {
					newParamMap.put("CRLINEBEGDATE", "");
					newParamMap.put("CRLINEENDDATE", "");
				}
				newParamMap.put("UPFILES", String.valueOf(filecount));
				newParamMap.put("CPRIMCELLULANO", reqParam.get("PHONE"));
				newParamMap.put("IP", reqParam.get("IP"));
				newParamMap.put("CHANTPE", "NB");
				newParamMap.put("VERSION1", reqParam.get("VERSION1"));
				newParamMap.put("VERSION2", reqParam.get("VERSION2"));
				newParamMap.put("CHKID", reqParam.get("CHKID") == null ? "" : reqParam.get("CHKID"));
				newParamMap.put("RESULT", reqParam.get("RESULT"));
				if(reqParam.get("REASON").equals("9")) {
					newParamMap.put("REASON", reqParam.get("REASON") + reqParam.get("REASON_VIEW") + " " + reqParam.get("REASONIN"));
				}
				else {
					newParamMap.put("REASON", reqParam.get("REASON") + reqParam.get("REASON_VIEW"));
				}
			}
			catch (Exception e) {
				// TODO: handle exception
				log.error("newParamMap ERROR",e);
				return bs;
			}
			
			
			String templatePath = "/uploadTemplate/CreditCardLine.txt";
			log.debug("templatePath={}",templatePath);
			
			Resource resource = new ClassPathResource(templatePath);
			String filename = "";
			
			//申請TXT
			//修改 Incorrect Permission Assignment For Critical Resources
			if(resource.exists()){
				InputStream inputStream = new BufferedInputStream(resource.getInputStream());
			
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"UTF-8"));
				StringBuilder stringBuilder = new StringBuilder();
				String line;
				line = bufferedReader.readLine();
				while(line != null){
					log.debug("line={}",line);
					ESAPIUtil.validInput(line,"GeneralString",true);
					
					String tagName = line.replace("<","").replace(">","");
					log.debug("tagName={}",tagName);
					String value = newParamMap.get(tagName);
					log.debug("value={}",value);
					
					if(value != null && !"".equals(value)){
						line = line + value;
					}
					
					stringBuilder.append(line).append("\n");
					line = bufferedReader.readLine();
				}
				log.debug("stringBuilder.toString()={}",stringBuilder.toString());
				
				try {
					String result = reqParam.get("RESULT");
					if(result.equals("N")) {
						filename = cusidn + date;
					}
					else {
						filename = cusidn + date + "PRE";
					}
					log.debug("檔案落地>" + filename + ".txt");
					String separ = File.separator;
					String skinPath = context.getRealPath("") + separ + "com" + separ + "updateTmp" + separ + filename + ".txt";
					byte[] bytes = (stringBuilder.toString()).getBytes("UTF-8");
					
					// 修正Incorrect Permission Assignment For Critical Resources
					BufferedOutputStream downloadFile = new BufferedOutputStream(new FileOutputStream(skinPath));
					downloadFile.write(bytes);
					downloadFile.close();
					try {
						String movePath = SpringBeanFactory.getBean("imgMovePath") + separ + filename + ".txt";
						// 修正Incorrect Permission Assignment For Critical Resources
						BufferedOutputStream imgmove = new BufferedOutputStream(new FileOutputStream(movePath));
//						FileOutputStream imgmove = new FileOutputStream(movePath);
						imgmove.write(bytes);
						imgmove.close();
						log.debug("檔案落地>" + filename + ".txt EDN");
					}
					catch (Exception e) {
						log.error("imgmove error>>>"+e);
						return bs;
					}
				}
				catch (Exception e) {
					log.debug("檔案落地失敗>>>"+e);
					return bs;
				}
			}
			
			//資料壓縮
			String separ = File.separator;
			String skinPath = context.getRealPath("") + separ + "com" + separ + "updateTmp" + separ + filename + ".txt";
			String file3Path = "";
			String file4Path = "";
			String file5Path = "";
			if(StrUtils.isNotEmpty(reqParam.get("FILE3"))) {
				file3Path = context.getRealPath("") + separ + "com" + separ + "updateTmp" + separ + reqParam.get("FILE3");
			}
			if(StrUtils.isNotEmpty(reqParam.get("FILE4"))) {
				file4Path = context.getRealPath("") + separ + "com" + separ + "updateTmp" + separ + reqParam.get("FILE4");
			}
			if(StrUtils.isNotEmpty(reqParam.get("FILE5"))) {
				file5Path = context.getRealPath("") + separ + "com" + separ + "updateTmp" + separ + reqParam.get("FILE5");
			}
			String zipPath = context.getRealPath("") + separ + "com" + separ + "updateTmp" + separ + filename + ".zip";
			File srcFile= new File(skinPath);
			try {
				log.debug("Do zip start");
				//開起壓縮後輸出的檔案
				ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(zipPath)));
				//開啟欲壓縮的檔案
				File[] file1 = new File[4];
				file1[0] = new File(skinPath);
				int j = 1;
				for(int i = 3; i < 6; i++) {
					if(StrUtils.isNotEmpty(reqParam.get("FILE"+i))) {
						file1[j] = new File(context.getRealPath("") + separ + "com" + separ + "updateTmp" + separ + reqParam.get("FILE"+i));
						j++;
					}
				}
				byte[] buffer = new byte[1024];

				for(int i=0;i<file1.length;i++) {
					if(file1[i] != null) {
						FileInputStream fi = new FileInputStream(file1[i]);
						out.putNextEntry(new ZipEntry(file1[i].getName()));
						int len;
						//讀入需要下載的檔案的內容,打包到zip檔案
						while((len = fi.read(buffer))>0) {
							out.write(buffer,0,len);
						}
						out.closeEntry();
						fi.close();
					}
				}
				out.close();
				log.debug("Do zip end");
				creditcard_apply_service.moveFile(filename + ".zip");
			}
			catch (Exception e) {
				// TODO: handle exception
				log.error("Do zip error >>>" + e);
				return bs;
			}
			
			try {
				//FTP
				String isSessionID = SpringBeanFactory.getBean("isSessionID");
				String ftpfilename = Paths.get(filename + ".zip").getFileName().toString();
				String uploadPath = context.getRealPath("") + separ +"com" + separ + "updateTmp" + separ;
				String validFileName = ESAPIUtil.GetValidPathPart(uploadPath,ftpfilename);
				byte[] bytes = Files.readAllBytes(Paths.get(validFileName));
				
				FTPUtil ftputil = new FTPUtil();
				Base64 base64 = new Base64();
				
				//LOCAL
				if("N".equals(isSessionID)){
					FTPClient ftpclient = ftputil.connect("192.168.50.191",21,"ftpuser","ftpuser",30000);
					boolean result = ftputil.uploadFile(ftpclient,filename + ".zip",bytes);
					ftputil.finish(ftpclient);
				}
				else{
					Map<String, String> ftps = SpringBeanFactory.getBean("eloanFTPS");
					String use = ftps.get("use");
					String ip = ftps.get("ip");
					int port = Integer.parseInt(ftps.get("port"));
					String ftpsname = ftps.get("name");
					String pw = ftps.get("password");
					String path = ftps.get("path");
					String decodeString = new String(base64.decode(pw.getBytes()));
					int timeout = Integer.parseInt(ftps.get("timeout"));
					if("Y".equals(use)) {
						// FTPS連線
						FTPSClient ftpsClient = ftputil.ftpsConnect(ip, port, "TLS", ftpsname, decodeString);
						if(ftpsClient != null) {
							ftputil.ChangeToDirectory(ftpsClient,path);
							// FTPS上傳檔案
							boolean result = ftputil.uploadFile(ftpsClient, filename + ".zip", bytes);
							// 上傳結果
							if(result){
								log.debug("上傳成功");
							}else{
								log.debug("上傳失敗");
							}
							// FTPS離線
							ftputil.finish(ftpsClient);
							log.debug("FTPS中斷連線...");
						}
						else {
							log.error("FTPS Client Null!!!");
						}
					}
				}
			}
			catch (Exception e) {
				// TODO: handle exception
				log.error("ftps error>>>" + e);
				bs.setResult(true);
				return  bs;
			}
		} catch (Exception e) {
			log.error("creditcard_limit_increase_result>> {}", e);
			return bs;
		}
		bs.setResult(true);
		return  bs;
	}
	
	/**
	 * 文件檢核
	 * @param file
	 * @param fileName
	 * @param fileContentType
	 * @param allowImageType
	 * @param allowImageWidth
	 * @param allowImageHeight
	 * @param allowImageSize
	 * @return
	 */
	public Map<String,String> checkFile(MultipartFile file,String originalFileName,String fileContentType,String[] allowImageType,
											int allowImageWidth,int allowImageHeight,int allowImageSize, String rootPath, String fileName){
		Map<String,String> returnMap = new HashMap<String,String>();
		returnMap.put("result","FALSE");
		returnMap.put("summary","");
		try{
			//判斷格式
			//log.trace("fileContentType >> {}", fileContentType);
			if(!"application/pdf".equals(fileContentType)) {
				returnMap.put("summary","格式不符");
			}
			//判斷大小
			if(file.getSize() > allowImageSize){
				returnMap.put("summary","檔案過大，不能超過" + allowImageSize / 1024 + "KB");//圖示檔案過大，不能超過
			}
			//判斷檔名是否過長
			if(originalFileName.length() > 300){
				returnMap.put("summary","檔名過長");//圖示檔名過長
			}
			//判斷副檔名是否過長
			if(FilenameUtils.getExtension(originalFileName).length() > 10){
				returnMap.put("summary","副檔名過長");//圖示副檔名過長
			}

			if("".equals(returnMap.get("summary"))){
				
				log.debug("allowImageWidth={}",allowImageWidth);
				log.debug("allowImageHeight={}",allowImageHeight);
				if(file.getSize() <= allowImageSize){
					
			        InputStream inputStream = file.getInputStream();
			        String separ = File.separator;
					String validatePath = ESAPIUtil.vaildPathTraversal2(rootPath,fileName);
					int index;
					byte[] bytes = new byte[1024];
					BufferedOutputStream fw = new BufferedOutputStream(new FileOutputStream(validatePath));
					while ((index = inputStream.read(bytes)) != -1) {
						fw.write(bytes, 0, index);
				        fw.flush();
					}
					fw.close();
					inputStream.close();
					
					//檢核通過
					returnMap.put("result","TRUE");
					log.info(ESAPIUtil.vaildLog("圖檔落地成功:"+validatePath));
				}
			}
		}
		catch(Exception e){
			if(returnMap.get("summary").equals("")) {
				returnMap.put("summary",i18n.getMsg("LB.X2131"));
			}
		}
		return returnMap;
	}
}

