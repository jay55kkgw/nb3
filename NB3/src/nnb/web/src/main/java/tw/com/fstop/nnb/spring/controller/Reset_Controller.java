package tw.com.fstop.nnb.spring.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.service.Reset_Service;
import tw.com.fstop.tbb.nnb.util.StrUtils;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.DownloadUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.WebUtil;

import javax.servlet.http.HttpSession;

@SessionAttributes({ 	SessionUtil.PD,SessionUtil.DOWNLOAD_DATALISTMAP_DATA,SessionUtil.PRINT_DATALISTMAP_DATA,
						SessionUtil.CONFIRM_LOCALE_DATA, SessionUtil.RESULT_LOCALE_DATA,SessionUtil.CUSIDN,
						SessionUtil.TRANSFER_CONFIRM_TOKEN,SessionUtil.TRANSFER_RESULT_TOKEN,SessionUtil.TRANSFER_RESULT_FINSH_TOKEN,SessionUtil.MOBILE,
						SessionUtil.TRANSFER_DATA})
@Controller
@RequestMapping(value = "/RESET")
public class Reset_Controller {
	Logger log = LoggerFactory.getLogger(this.getClass());

	// TODO 測試用
	// int cnt = 0;

	@Autowired
	private Reset_Service reset_service;

	/**
	 * 變更使用名稱/簽入密碼  輸入頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/user_reset")
	public String user_reset(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/online_apply/ErrorWithoutMenu";
		BaseResult bs = null;
		log.trace("user_reset");
		try {
			bs = new BaseResult();
			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = reset_service.getTxToken();
			log.trace("electricity_fee.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			if(bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			}
			log.debug(ESAPIUtil.vaildLog(CodeUtil.toJson(request.getRemoteAddr())));
			bs.addData("IP", request.getRemoteAddr());
			model.addAttribute("result_data", bs);
			target = "/reset/user_reset";
		}catch (Exception e) {
			log.error("Reset_service.keepPW: {}", e);
		}
		return target;
	}
	
	/**
	 * 變更使用名稱/簽入密碼  結果頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/user_reset_result")
	public String user_reset_result(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/online_apply/ErrorWithoutMenu";
		BaseResult bs = null;
		Map<String, String> okMap = null;
		log.trace(ESAPIUtil.vaildLog("user_reset_result>>>{}"+CodeUtil.toJson(reqParam)));
		try {

			bs = new BaseResult();
//			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			okMap = reqParam;
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				log.trace("apply_deposit_account_result.validate TXTOKEN...");
				log.trace("apply_deposit_account_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("other_telecom_fee_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("other_telecom_fee_result.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("other_telecom_fee_result TXTOKEN 不正確，TXTOKEN>>{}"+ okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						!okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("other_telecom_fee_result.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("other_telecom_fee_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				log.debug(ESAPIUtil.vaildLog("reqParam >> {}"+CodeUtil.toJson(reqParam)));
				bs = reset_service.user_reset_result(reqParam);
				log.trace("bs data >>>>{}",bs.getData());
				log.trace("bs data >>>>{}",bs.getResult());
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
			
		}catch (Exception e) {
			log.error("Reset_service.keepPW: {}", e);
		}finally {
			if(bs != null && bs.getResult()) {
				target = "/reset/user_reset_result";
				model.addAttribute("result_data", bs);
			}else {
				bs.setPrevious("/RESET/user_reset");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * 取得簡訊驗證碼Ajax
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/smsotp_ajax", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody List<Map<String, String>> smsotp_ajax(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("smsotp_ajax...");
		SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, "");
		List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();
		try {
			String cusidn = reqParam.get("CUSIDN");
			String adopid = reqParam.get("ADOPID");
			log.trace(ESAPIUtil.vaildLog("CUSIDN>>>"+ cusidn +" ADOPID>>>"+ adopid));
			String msgType = StrUtils.isEmpty((String)reqParam.get("MSGTYPE")) ? "" : reqParam.get("MSGTYPE").toString();
//			String acn = reqParam.get("ACN");
//			// 帳號資料
			resultList = reset_service.smsotp_ajax(adopid, cusidn, msgType);
//			log.trace("get resultList>>>>>{}",resultList.toString());
//			BaseResult bs = new BaseResult();
//			for(Map<String, String> row : resultList) {
//				if(StrUtils.isNotEmpty(row.get("MSGCOD"))) {
//					bs.setMsgCode(row.get("MSGCOD"));
//					log.trace("add error msg>>>{}",bs.getMsgCode());
//					bs.setMessage(row.get("msgName"));
//					log.trace("add error msg>>>{}",bs.getMessage());
//					SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
//					model.addAttribute(BaseResult.ERROR, bs);
//				}
//				else if(StrUtils.isEmpty(row.get("SVACN"))) {
//					bs.setMessage("您尚未約定扣款帳號，請執行黃金定期定額申購。");
//					log.trace("add error msg>>>{}",bs.getMessage());
//					bs.setMsgCode("");
//					bs.setNext("/GOLD/AVERAGING/averaging_purchase");
//					bs.setPrevious("/INDEX/index");
//					SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
//				}
//			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("smsotp_ajax error >> {}",e);
		}
		return resultList;
	}
    
    @RequestMapping(value = "/forgotPWD")
	public String forgot_pwd(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/reset/forgotPWD";
		return target;
	}

	@RequestMapping(value = "/ssl_reset")
	public String ssl_reset(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.debug("ssl_reset start");
		String target = "/online_apply/ErrorWithoutMenu";
		BaseResult bs = null;
		try {
			String CUSIDN = "";
			try {
				CUSIDN = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			}
			catch (Exception e) {
				log.error("String error{}",e);
			}
			bs = new BaseResult();
			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = reset_service.getTxToken();
			log.trace("electricity_fee.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			if(bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			}
			log.debug(ESAPIUtil.vaildLog(CodeUtil.toJson(request.getRemoteAddr())));
			bs.addData("IP", request.getRemoteAddr());
			bs.addData("cusidn", CUSIDN);
			model.addAttribute("result_data", bs);
			target = "/reset/ssl_reset";
		}catch (Exception e) {
			log.error("Reset_service.keepPW: {}", e);
		}
		return target;
	}
	
	@RequestMapping(value = "/ssl_reset_result")
	public String ssl_reset_result(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.debug("ssl_reset_result start");
		String target = "/online_apply/ErrorWithoutMenu";
		Map<String, String> okMap = null;
		BaseResult bs = null;
		try {

			bs = new BaseResult();
//			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			okMap = reqParam;
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				log.trace("apply_deposit_account_result.validate TXTOKEN...");
				log.trace("apply_deposit_account_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("other_telecom_fee_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("other_telecom_fee_result.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("other_telecom_fee_result TXTOKEN 不正確，TXTOKEN>>{}"+ okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						!okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("other_telecom_fee_result.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("other_telecom_fee_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				log.debug(ESAPIUtil.vaildLog("reqParam >> {}"+CodeUtil.toJson(reqParam)));
				bs = reset_service.ssl_reset_result(reqParam);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
			
		}catch (Exception e) {
			log.error("Reset_service.keepPW: {}", e);
		}finally {
			if(bs != null && bs.getResult()) {
				target = "/reset/ssl_reset_result";
				model.addAttribute("result_data", bs);
			}else {
				bs.setPrevious("/RESET/ssl_reset");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * 取得簡訊驗證碼Ajax
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/smsotpwithphone_ajax", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody List<Map<String, String>> smsotpwithphone_ajax(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model,  HttpSession session) {
		log.trace("smsotpwithphone_ajax...");
		SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, "");
		List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();
		try {
			String cusidn = reqParam.get("CUSIDN");
			
			//String phone = reqParam.get("PHONE");
			String phone = (String) SessionUtil.getAttribute(model, SessionUtil.MOBILE, null);
			log.debug("session mobile==>"+phone);
			
			String adopid = reqParam.get("ADOPID");
			log.trace(ESAPIUtil.vaildLog("CUSIDN>>>"+ cusidn +" ADOPID>>>"+ adopid +" PHONE>>>"+ phone));
			String msgType = StrUtils.isEmpty((String)reqParam.get("MSGTYPE")) ? "" : reqParam.get("MSGTYPE").toString();
//			String acn = reqParam.get("ACN");
//			// 帳號資料
			resultList = reset_service.smsotpwithphone_ajax(adopid, cusidn, phone, msgType);
//			log.trace("get resultList>>>>>{}",resultList.toString());
//			BaseResult bs = new BaseResult();
//			for(Map<String, String> row : resultList) {
//				if(StrUtils.isNotEmpty(row.get("MSGCOD"))) {
//					bs.setMsgCode(row.get("MSGCOD"));
//					log.trace("add error msg>>>{}",bs.getMsgCode());
//					bs.setMessage(row.get("msgName"));
//					log.trace("add error msg>>>{}",bs.getMessage());
//					SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
//					model.addAttribute(BaseResult.ERROR, bs);
//				}
//				else if(StrUtils.isEmpty(row.get("SVACN"))) {
//					bs.setMessage("您尚未約定扣款帳號，請執行黃金定期定額申購。");
//					log.trace("add error msg>>>{}",bs.getMessage());
//					bs.setMsgCode("");
//					bs.setNext("/GOLD/AVERAGING/averaging_purchase");
//					bs.setPrevious("/INDEX/index");
//					SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
//				}
//			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("smsotp_ajax error >> {}",e);
		}
		return resultList;
	}		
}
