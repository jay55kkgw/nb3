package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * 
 * 功能說明 :SMART FUND自動贖回設定 上行
 *
 */
public class C119_REST_RQ extends BaseRestBean_FUND implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -440030806586475881L;


	private String CUSIDN;// 統一編號

	private String CDNO;// 信託號碼

	private String PAYTAG;// 扣款標的

	private String FUNDAMT;// 信託金額

	private String FUNDCUR;// 贖回信託金額幣別

	private String STOPLOSS;// 停損

	private String STOPPROF;// 停利

	private String NSTOPLOSS;// 新停損設定

	private String NSTOPPROF;// 新停利設定

	private String NOOPT;// 不設定註記

	private String AC202;// 信託業務別

	private String MIP;// 定期不定額註記

	private String MAC;// MAC
	
	private String PINNEW;
	
	private String FGTXWAY;



	public String getCUSIDN()
	{
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN)
	{
		CUSIDN = cUSIDN;
	}

	public String getPAYTAG()
	{
		return PAYTAG;
	}

	public void setPAYTAG(String pAYTAG)
	{
		PAYTAG = pAYTAG;
	}

	public String getFUNDAMT()
	{
		return FUNDAMT;
	}

	public void setFUNDAMT(String fUNDAMT)
	{
		FUNDAMT = fUNDAMT;
	}

	public String getFUNDCUR()
	{
		return FUNDCUR;
	}

	public void setFUNDCUR(String fUNDCUR)
	{
		FUNDCUR = fUNDCUR;
	}

	public String getSTOPLOSS()
	{
		return STOPLOSS;
	}

	public void setSTOPLOSS(String sTOPLOSS)
	{
		STOPLOSS = sTOPLOSS;
	}

	public String getSTOPPROF()
	{
		return STOPPROF;
	}

	public void setSTOPPROF(String sTOPPROF)
	{
		STOPPROF = sTOPPROF;
	}

	public String getNSTOPLOSS()
	{
		return NSTOPLOSS;
	}

	public void setNSTOPLOSS(String nSTOPLOSS)
	{
		NSTOPLOSS = nSTOPLOSS;
	}

	public String getNSTOPPROF()
	{
		return NSTOPPROF;
	}

	public void setNSTOPPROF(String nSTOPPROF)
	{
		NSTOPPROF = nSTOPPROF;
	}

	public String getNOOPT()
	{
		return NOOPT;
	}

	public void setNOOPT(String nOOPT)
	{
		NOOPT = nOOPT;
	}

	public String getAC202()
	{
		return AC202;
	}

	public void setAC202(String aC202)
	{
		AC202 = aC202;
	}

	public String getMIP()
	{
		return MIP;
	}

	public void setMIP(String mIP)
	{
		MIP = mIP;
	}

	public String getMAC()
	{
		return MAC;
	}

	public void setMAC(String mAC)
	{
		MAC = mAC;
	}

	public String getPINNEW()
	{
		return PINNEW;
	}

	public void setPINNEW(String pINNEW)
	{
		PINNEW = pINNEW;
	}

	public String getCDNO() {
		return CDNO;
	}

	public void setCDNO(String cDNO) {
		CDNO = cDNO;
	}

	public String getFGTXWAY() {
		return FGTXWAY;
	}

	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}

	
	
	
	

}
