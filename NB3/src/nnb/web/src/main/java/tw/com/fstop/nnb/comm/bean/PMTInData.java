package tw.com.fstop.nnb.comm.bean;

/**
 * 系統代號：
 * 系統名稱：
 * 專案代號：
 * 專案名稱：
 * 設計人　：
 * 設計日期：
 * 程式目的：
 * 邏輯說明：
 * 異動歷史紀錄：
 */
public class PMTInData {
	//還款方式(1:本息平均攤還 / 2:本金平均攤還 / 3:到期一次還本)
	private int repaymentType = 1;
	//貸款本金
	private double loan;
	//本金寬限期(月)
	private int principalGrace = 0;
	
	//年利率(第1段)
	private double yRate1 = 0.0;
	//年利率(第2段)
	private double yRate2 = 0.0;
	//年利率(第3段)
	private double yRate3 = 0.0;
	//貸款期間(第1段)
	private int period1 = 0;
	//貸款期間(第2段)
	private int period2 = 0;
	//貸款期間(第3段)
	private int period3 = 0;
	//其它費用合計
	private double otherFee = 0.0;
	//總利息
	private double totalInterest = 0.0;
	
	//月份(期數)
	private double[] periods;
	//貸款結餘
	private double[] loanBalance;
	//償還本金
	private double[] principal;
	//利息
	private double[] interest;
	//貸款利率
	private double[] rate; 
	//每月繳款(現金流量)
	private double[] cashFlow;

	//建構子
	public PMTInData(){
		System.out.println("[PMTInData] create Instance");
	}

	/**
	 * @return the repaymentType
	 */
	public int getRepaymentType() {
		return repaymentType;
	}

	/**
	 * @param repaymentType the repaymentType to set
	 */
	public void setRepaymentType(int repaymentType) {
		this.repaymentType = repaymentType;
	}

	/**
	 * @return the loan
	 */
	public double getLoan() {
		return loan;
	}

	/**
	 * @param loan the loan to set
	 */
	public void setLoan(double loan) {
		this.loan = loan;
	}

	/**
	 * @return the principalGrace
	 */
	public int getPrincipalGrace() {
		return principalGrace;
	}

	/**
	 * @param principalGrace the principalGrace to set
	 */
	public void setPrincipalGrace(int principalGrace) {
		this.principalGrace = principalGrace;
	}

	/**
	 * @return the yRate1
	 */
	public double getYRate1() {
		return yRate1;
	}

	/**
	 * @param rate1 the yRate1 to set
	 */
	public void setYRate1(double rate1) {
		yRate1 = rate1;
	}

	/**
	 * @return the yRate2
	 */
	public double getYRate2() {
		return yRate2;
	}

	/**
	 * @param rate2 the yRate2 to set
	 */
	public void setYRate2(double rate2) {
		yRate2 = rate2;
	}

	/**
	 * @return the yRate3
	 */
	public double getYRate3() {
		return yRate3;
	}

	/**
	 * @param rate3 the yRate3 to set
	 */
	public void setYRate3(double rate3) {
		yRate3 = rate3;
	}

	/**
	 * @return the period1
	 */
	public int getPeriod1() {
		return period1;
	}

	/**
	 * @param period1 the period1 to set
	 */
	public void setPeriod1(int period1) {
		this.period1 = period1;
	}

	/**
	 * @return the period2
	 */
	public int getPeriod2() {
		return period2;
	}

	/**
	 * @param period2 the period2 to set
	 */
	public void setPeriod2(int period2) {
		this.period2 = period2;
	}

	/**
	 * @return the period3
	 */
	public int getPeriod3() {
		return period3;
	}

	/**
	 * @param period3 the period3 to set
	 */
	public void setPeriod3(int period3) {
		this.period3 = period3;
	}

	/**
	 * @return the otherFee
	 */
	public double getOtherFee() {
		return otherFee;
	}

	/**
	 * @param otherFee the otherFee to set
	 */
	public void setOtherFee(double otherFee) {
		this.otherFee = otherFee;
	}

	/**
	 * @return the totalInterest
	 */
	public double getTotalInterest() {
		return totalInterest;
	}

	/**
	 * @param totalInterest the totalInterest to set
	 */
	public void setTotalInterest(double totalInterest) {
		this.totalInterest = totalInterest;
	}

	/**
	 * @return the periods
	 */
	public double[] getPeriods() {
		return periods;
	}

	/**
	 * @param periods the periods to set
	 */
	public void setPeriods(double[] periods) {
		this.periods = periods;
	}

	/**
	 * @return the loanBalance
	 */
	public double[] getLoanBalance() {
		return loanBalance;
	}

	/**
	 * @param loanBalance the loanBalance to set
	 */
	public void setLoanBalance(double[] loanBalance) {
		this.loanBalance = loanBalance;
	}

	/**
	 * @return the principal
	 */
	public double[] getPrincipal() {
		return principal;
	}

	/**
	 * @param principal the principal to set
	 */
	public void setPrincipal(double[] principal) {
		this.principal = principal;
	}

	/**
	 * @return the interest
	 */
	public double[] getInterest() {
		return interest;
	}

	/**
	 * @param interest the interest to set
	 */
	public void setInterest(double[] interest) {
		this.interest = interest;
	}

	/**
	 * @return the rate
	 */
	public double[] getRate() {
		return rate;
	}

	/**
	 * @param rate the rate to set
	 */
	public void setRate(double[] rate) {
		this.rate = rate;
	}

	/**
	 * @return the cashFlow
	 */
	public double[] getCashFlow() {
		return cashFlow;
	}

	/**
	 * @param cashFlow the cashFlow to set
	 */
	public void setCashFlow(double[] cashFlow) {
		this.cashFlow = cashFlow;
	}
}
