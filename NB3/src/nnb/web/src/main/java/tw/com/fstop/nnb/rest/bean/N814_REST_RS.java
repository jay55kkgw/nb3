package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N814_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6163163035862657792L;
	private String CMQTIME;
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	
}
