package tw.com.fstop.nnb.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.WebUtil;

@Service
public class House_Guarantee_Service extends Base_Service
{
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	I18n i18n;
	// 判斷是否數字
	public static boolean isNumeric(String str)
	{
		String bigStr;
		try
		{
			bigStr = new BigDecimal(str).toString();
		}
		catch (Exception e)
		{
			return false;
		}
		return true;
	}

	/**
	 * 房屋擔保借款繳息清單確認頁
	 * 
	 * @param cusidn
	 * @return
	 */
	public BaseResult interest_list_confirm(String cusidn, Map<String, String> reqParam)
	{

		log.trace(ESAPIUtil.vaildLog("interest_list_confirm>>{}"+ CodeUtil.toJson(reqParam)));
		BaseResult bs = null;
		try
		{
			bs = new BaseResult();
			// call ws API
			String type = reqParam.get("TYPE");
			bs = N105_REST(cusidn, type, reqParam);
			if (bs != null && bs.getResult())
			{
				Map<String, Object> dataMap = (Map) bs.getData();
				ArrayList<Map<String, String>> rows = (ArrayList<Map<String, String>>) dataMap.get("REC");
				ArrayList<Map<String, String>> callRec = new ArrayList<Map<String, String>>();
				for (Map<String, String> map : rows)
				{
					String AMTORLN = (String) map.get("AMTORLN").trim();
					if (!"".equals(map.get("ACN")) && map.get("ACN") != null)
					{
						map.put("AMTORLN", NumericUtil.fmtAmount(NumericUtil.addDot(AMTORLN, 0), 0));
						callRec.add(map);
					}
				}
				dataMap.put("REC", callRec);
				dataMap.put("TXTOKEN", reqParam.get("TOKEN"));
				// 判斷無帳號就ENRD
				String count = String.valueOf(rows.size());
				log.trace("count>>>{}", count);
				if (count == null || "0".equals(count))
				{
					String msgCode = "ENRD";
					String msgName = i18n.getMsg("LB.Check_no_data");//查無資料
					bs.setMessage(msgCode, msgName);
					bs.setResult(Boolean.FALSE);
				}
			}
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}

	/**
	 * 房屋擔保借款繳息清單結果頁
	 * 
	 * @param cusidn
	 * @return
	 */
	public BaseResult interest_list_result(String cusidn, Map<String, String> reqParam)
	{

		log.trace("interest_list_result>>{}", cusidn);
		BaseResult bs = null;
		try
		{
			bs = new BaseResult();
			// call ws API
			log.debug(ESAPIUtil.vaildLog("ROWDATA: >>{}"+ reqParam.get("ROWDATA")));
			String type = reqParam.get("TYPE");
			String value = reqParam.get("ROWDATA");
			value = value.substring(1, value.length() - 1); // remove curly brackets
			log.trace(ESAPIUtil.vaildLog("ROWDATA.value: " + value));
			String[] keyValuePairs = value.split(","); // split the string to creat key-value pairs
			log.trace(ESAPIUtil.vaildLog("ROWDATA.keyValuePairs: " + keyValuePairs));

			Map<String, String> map = new HashMap<>();
			for (String pair : keyValuePairs) // iterate over the pairs
			{
				String[] entry = pair.split("="); // split the pairs to get key and value

				String mapKey = "";
				String MapValue = "";
				log.trace("ROWDATA.entry.length: " + entry.length);
				if (entry.length == 2)
				{
					mapKey = "".equals(entry[0].trim()) ? "" : entry[0].trim();
					MapValue = "".equals(entry[1].trim()) ? "" : entry[1].trim();
				}
				else
				{
					mapKey = entry[0].trim();
				}

				log.trace(ESAPIUtil.vaildLog("ROWDATA.mapKey: " + mapKey));
				log.trace(ESAPIUtil.vaildLog("ROWDATA.MapValue: " + MapValue));
				map.put(mapKey, MapValue); // add them to the hashmap and trim whitespaces
			}
			bs = N105_REST(cusidn, type, map);
			if (bs != null && bs.getResult())
			{
				Map<String, Object> dataMap = (Map) bs.getData();
				String NAME = String.valueOf(dataMap.get("NAME")); // 借戶姓名
				String CUSNAME = String.valueOf(dataMap.get("CUSNAME")); // 原住民姓名

				ArrayList<Map<String, String>> rows = (ArrayList<Map<String, String>>) dataMap.get("REC");
				ArrayList<Map<String, String>> callRec = new ArrayList<Map<String, String>>();
				for (Map<String, String> mapRec : rows)
				{
					// 篩選空資料
					if (!"".equals(mapRec.get("ACN")) && mapRec.get("ACN") != null && isNumeric(mapRec.get("ACN")))
					{
						// 數字格式處理
						String AMTORLN = mapRec.get("AMTORLN").trim();
						String BAL_E = mapRec.get("BAL_E").trim();
						String TOTAMT_E = mapRec.get("TOTAMT_E").trim();
						String ADDR = StringUtils.deleteWhitespace(mapRec.get("ADDR"));
						// 利用if暫時處理電文亂碼用
						if (isNumeric(AMTORLN))
						{
							mapRec.put("AMTORLN", NumericUtil.fmtAmount(NumericUtil.addDot(AMTORLN, 0), 0));
						}
						if (isNumeric(BAL_E))
						{
							mapRec.put("BAL_E", NumericUtil.fmtAmount(NumericUtil.addDot(BAL_E, 0), 0));
						}
						if (isNumeric(TOTAMT_E))
						{
							mapRec.put("TOTAMT_E", NumericUtil.fmtAmount(NumericUtil.addDot(TOTAMT_E, 0), 0));
						}

						mapRec.put("ADDR", ADDR);
						callRec.add(mapRec);
					}
				}
				log.trace("callRec>>>>{}", callRec);
				dataMap.put("REC", callRec);
				dataMap.put("TXTOKEN", reqParam.get("TXTOKEN"));
				bs.addData("CUSIDN", cusidn);
				// 隱藏姓名 call 字霸
				String hideid_name = StrUtil.isNotEmpty(CUSNAME) ? WebUtil.hideusername1Convert(CUSNAME)
						: WebUtil.hideusername1Convert(NAME);
				bs.addData("NAME", hideid_name);
			}
		}
		catch (Exception e)
		{
			// e.printStackTrace();
		}
		return bs;
	}

}
