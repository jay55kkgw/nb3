package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class Ratequery_N030_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5387966715134948191L;

	private String UPDATEDATETIME;
	// 資料陣列
	private LinkedList<Ratequery_N030_RSDATA> REC;

	public String getUPDATEDATETIME() {
		return UPDATEDATETIME;
	}

	public LinkedList<Ratequery_N030_RSDATA> getREC() {
		return REC;
	}

	public void setUPDATEDATETIME(String uPDATEDATETIME) {
		UPDATEDATETIME = uPDATEDATETIME;
	}

	public void setREC(LinkedList<Ratequery_N030_RSDATA> rEC) {
		REC = rEC;
	}

}
