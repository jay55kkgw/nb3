package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N948_REST_RS extends BaseRestBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1759941712189202258L;
	
	private String MSGCOD;
	private String CUSIDN;
	
	public String getMSGCOD() {
		return MSGCOD;
	}
	public void setMSGCOD(String mSGCOD) {
		MSGCOD = mSGCOD;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
    

}