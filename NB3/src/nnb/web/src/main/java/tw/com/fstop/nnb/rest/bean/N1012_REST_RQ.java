package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N1012_REST_RQ extends BaseRestBean_OLS implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4955169564894014555L;

	private String TRANNAME;// TRANNAME

	private String PINNEW;// 網路銀行密碼（新）

	private String CUSIDN;// 統一編號

	private String TYPE;// 查詢類別

	private String ACN;// 帳號

	private String CHKTYPE;// 票據種類

	private String CHKFORM;// 票據格式

	private String CHKCNT;// 作廢票據總張數
	
	private String APPLYNUM;//申請張數

	private String TEL;// 聯絡電話

	private String FGTXWAY;// 交易機制

	public String getTRANNAME()
	{
		return TRANNAME;
	}

	public void setTRANNAME(String tRANNAME)
	{
		TRANNAME = tRANNAME;
	}

	public String getPINNEW()
	{
		return PINNEW;
	}

	public void setPINNEW(String pINNEW)
	{
		PINNEW = pINNEW;
	}

	public String getCUSIDN()
	{
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN)
	{
		CUSIDN = cUSIDN;
	}

	public String getTYPE()
	{
		return TYPE;
	}

	public void setTYPE(String tYPE)
	{
		TYPE = tYPE;
	}

	public String getACN()
	{
		return ACN;
	}

	public void setACN(String aCN)
	{
		ACN = aCN;
	}

	public String getCHKTYPE()
	{
		return CHKTYPE;
	}

	public void setCHKTYPE(String cHKTYPE)
	{
		CHKTYPE = cHKTYPE;
	}

	public String getCHKFORM()
	{
		return CHKFORM;
	}

	public void setCHKFORM(String cHKFORM)
	{
		CHKFORM = cHKFORM;
	}

	public String getCHKCNT()
	{
		return CHKCNT;
	}

	public void setCHKCNT(String cHKCNT)
	{
		CHKCNT = cHKCNT;
	}

	public String getAPPLYNUM()
	{
		return APPLYNUM;
	}

	public void setAPPLYNUM(String aPPLYNUM)
	{
		APPLYNUM = aPPLYNUM;
	}

	public String getTEL()
	{
		return TEL;
	}

	public void setTEL(String tEL)
	{
		TEL = tEL;
	}

	public String getFGTXWAY()
	{
		return FGTXWAY;
	}

	public void setFGTXWAY(String fGTXWAY)
	{
		FGTXWAY = fGTXWAY;
	}

	

}
