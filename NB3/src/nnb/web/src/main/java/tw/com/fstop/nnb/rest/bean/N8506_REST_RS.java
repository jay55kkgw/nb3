package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N8506_REST_RS extends BaseRestBean implements Serializable {

	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4489133007428078613L;
	
	private String CMQTIME;
	private LinkedList<N8506_REST_RSDATA> REC;
	
	
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public LinkedList<N8506_REST_RSDATA> getREC() {
		return REC;
	}
	public void setREC(LinkedList<N8506_REST_RSDATA> rEC) {
		REC = rEC;
	}
	
	
	
	
	

}
