package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class F002_IDGATE_DATA implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -762951082705400880L;

	// 付款人轉出幣別
	@SerializedName(value = "ACCCUR")
	private String ACCCUR;
	
	// 付款帳號
	@SerializedName(value = "ACCTID")
	private String ACCTID;
	
	// 轉出金額
	@SerializedName(value = "ACCAMT")
	private String ACCAMT;
	
	// 付款人轉入幣別
	@SerializedName(value = "CURCODE")
	private String CURCODE;
	
	// 付款日期
	@SerializedName(value = "PRCDT")
	private String PRCDT;

}
