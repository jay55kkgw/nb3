package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
public class EBPay1_REST_RQ extends BaseRestBean_PAY implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8420677227213182445L;
	
	private String CMDATE1;
	private String CMDATE2;
	private String WAT_NO;
	private String CHKCOD;
	private String AMOUNT;
	private String FGTXWAY;
	
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getCMDATE1() {
		return CMDATE1;
	}
	public void setCMDATE1(String cMDATE1) {
		CMDATE1 = cMDATE1;
	}
	public String getCMDATE2() {
		return CMDATE2;
	}
	public void setCMDATE2(String cMDATE2) {
		CMDATE2 = cMDATE2;
	}
	public String getWAT_NO() {
		return WAT_NO;
	}
	public void setWAT_NO(String wAT_NO) {
		WAT_NO = wAT_NO;
	}
	public String getCHKCOD() {
		return CHKCOD;
	}
	public void setCHKCOD(String cHKCOD) {
		CHKCOD = cHKCOD;
	}
	public String getAMOUNT() {
		return AMOUNT;
	}
	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}
}
