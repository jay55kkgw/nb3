package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class IdGateMappingDelete_REST_RQ extends BaseRestBean_IDGATE implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3269283528954234145L;
	private String CUSIDN	;
	private String IDGATEID ;
	private String DEVICEID ;
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getIDGATEID() {
		return IDGATEID;
	}
	public void setIDGATEID(String iDGATEID) {
		IDGATEID = iDGATEID;
	}
	public String getDEVICEID() {
		return DEVICEID;
	}
	public void setDEVICEID(String dEVICEID) {
		DEVICEID = dEVICEID;
	}
	
	
	
	
	

}
