package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;

public class N076_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1289013734027025204L;

	@SerializedName(value = "ACNO")
	private String ACNO;
	@SerializedName(value = "FDPACN_TEXT")
	private String FDPACN_TEXT;
	@SerializedName(value = "FDPNUM")
	private String FDPNUM;
	@SerializedName(value = "AMOUNT")
	private String AMOUNT;

	@Override
	public Map<String, String> coverMap() {
		LinkedHashMap<String, String> result = new LinkedHashMap<String, String>();
		result.put("交易名稱", "臺幣零存整付按月繳存");
		result.put("交易類型", "-");
		result.put("轉出帳號", this.ACNO);
		result.put("存單帳號", this.FDPACN_TEXT);
		result.put("存單號碼", this.FDPNUM);
		result.put("轉帳金額 ", this.AMOUNT);
		return result;
	}

	public String getACNO() {
		return ACNO;
	}

	public void setACNO(String aCNO) {
		ACNO = aCNO;
	}

	public String getFDPACN_TEXT() {
		return FDPACN_TEXT;
	}

	public void setFDPACN_TEXT(String fDPACN_TEXT) {
		FDPACN_TEXT = fDPACN_TEXT;
	}

	public String getFDPNUM() {
		return FDPNUM;
	}

	public void setFDPNUM(String fDPNUM) {
		FDPNUM = fDPNUM;
	}

	public String getAMOUNT() {
		return AMOUNT;
	}

	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}



}
