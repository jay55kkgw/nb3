package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N640_REST_RS extends BaseRestBean implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2173969541744567344L;

	private String CMQTIME;//交易時間

	private String CMRECNUM;

	private String CMPERIOD;

	private String ABEND;//結束代碼

	private String REC_NO;//筆數

	private String ENDDATE;//結束日期

	private String USERDATA_X100;//暫存空間區
	
	private String QUERYNEXT;//繼續查詢資料
	
	private String __OCCURS;

	private String LSTIME;//最後異動時間

	private String STADATE;//開始日期

	private LinkedList<N640_REST_RSDATA> REC;

	public String getCMQTIME()
	{
		return CMQTIME;
	}

	public void setCMQTIME(String cMQTIME)
	{
		CMQTIME = cMQTIME;
	}

	public String getCMRECNUM()
	{
		return CMRECNUM;
	}

	public void setCMRECNUM(String cMRECNUM)
	{
		CMRECNUM = cMRECNUM;
	}

	public String getCMPERIOD()
	{
		return CMPERIOD;
	}

	public void setCMPERIOD(String cMPERIOD)
	{
		CMPERIOD = cMPERIOD;
	}

	public String getABEND()
	{
		return ABEND;
	}

	public void setABEND(String aBEND)
	{
		ABEND = aBEND;
	}

	public String getREC_NO()
	{
		return REC_NO;
	}

	public void setREC_NO(String rEC_NO)
	{
		REC_NO = rEC_NO;
	}

	public String getENDDATE()
	{
		return ENDDATE;
	}

	public void setENDDATE(String eNDDATE)
	{
		ENDDATE = eNDDATE;
	}

	public String getUSERDATA_X100()
	{
		return USERDATA_X100;
	}

	public void setUSERDATA_X100(String uSERDATA_X100)
	{
		USERDATA_X100 = uSERDATA_X100;
	}

	public String getQUERYNEXT()
	{
		return QUERYNEXT;
	}

	public void setQUERYNEXT(String qUERYNEXT)
	{
		QUERYNEXT = qUERYNEXT;
	}

	public String get__OCCURS()
	{
		return __OCCURS;
	}

	public void set__OCCURS(String __OCCURS)
	{
		this.__OCCURS = __OCCURS;
	}

	public String getLSTIME()
	{
		return LSTIME;
	}

	public void setLSTIME(String lSTIME)
	{
		LSTIME = lSTIME;
	}

	public String getSTADATE()
	{
		return STADATE;
	}

	public void setSTADATE(String sTADATE)
	{
		STADATE = sTADATE;
	}

	public LinkedList<N640_REST_RSDATA> getREC()
	{
		return REC;
	}

	public void setREC(LinkedList<N640_REST_RSDATA> rEC)
	{
		REC = rEC;
	}

	

}
