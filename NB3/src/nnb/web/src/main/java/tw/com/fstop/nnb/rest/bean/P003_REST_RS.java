package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class P003_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7565203980560444434L;
	
	private String TxnCode;//交易代碼
	private String TxnDateTime;//交易時間
	private String SerialNo;//流水號
	private String RespCode1;//回應代碼
	private String RespString1;//回應代碼敘述
	private String CustomerId;//客戶ID
	private String Email;//Email
	private String Phone;//手機號碼
	private String Name;//客戶姓名
	private String Count;//申請筆數
	private String CMQTIME;
	private LinkedList<P003_REST_RSDATA> REC;
	
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getTxnCode() {
		return TxnCode;
	}
	public void setTxnCode(String txnCode) {
		TxnCode = txnCode;
	}
	public String getTxnDateTime() {
		return TxnDateTime;
	}
	public void setTxnDateTime(String txnDateTime) {
		TxnDateTime = txnDateTime;
	}
	public String getSerialNo() {
		return SerialNo;
	}
	public void setSerialNo(String serialNo) {
		SerialNo = serialNo;
	}
	public String getRespCode1() {
		return RespCode1;
	}
	public void setRespCode1(String respCode1) {
		RespCode1 = respCode1;
	}
	public String getRespString1() {
		return RespString1;
	}
	public void setRespString1(String respString1) {
		RespString1 = respString1;
	}
	public String getCustomerId() {
		return CustomerId;
	}
	public void setCustomerId(String customerId) {
		CustomerId = customerId;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public String getPhone() {
		return Phone;
	}
	public void setPhone(String phone) {
		Phone = phone;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getCount() {
		return Count;
	}
	public void setCount(String count) {
		Count = count;
	}
	public LinkedList<P003_REST_RSDATA> getREC() {
		return REC;
	}
	public void setREC(LinkedList<P003_REST_RSDATA> rEC) {
		REC = rEC;
	}
}
