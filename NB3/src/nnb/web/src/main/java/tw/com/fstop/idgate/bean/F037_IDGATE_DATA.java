package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class F037_IDGATE_DATA implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3232069342035347716L;
	
	@SerializedName(value = "CUSIDN")
	private String CUSIDN;

	@SerializedName(value = "BENACC")
	private String BENACC;

//	@SerializedName(value = "COUNT")
//	private String COUNT;
	
	@SerializedName(value = "CCY")
	private String CCY;
	
	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}

	
//	public String getCOUNT() {
//		return COUNT;
//	}
//
//	public void setCOUNT(String cOUNT) {
//		COUNT = cOUNT;
//	}

	public String getCCY() {
		return CCY;
	}

	public void setCCY(String cCY) {
		CCY = cCY;
	}
	
	public String getBENACC() {
		return BENACC;
	}

	public void setBENACC(String bENACC) {
		BENACC = bENACC;
	}

}
