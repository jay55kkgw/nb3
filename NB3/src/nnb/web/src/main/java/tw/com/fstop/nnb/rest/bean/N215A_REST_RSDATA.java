package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N215A_REST_RSDATA extends BaseRestBean implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 5803117521181434777L;
	
	private String ACN;//帳號
	private String BNKCOD;//行庫代號
	private String RAFLAG;//約定註記
	private String TRAFLAG;

	public String getTRAFLAG() {
		return TRAFLAG;
	}

	public void setTRAFLAG(String tRAFLAG) {
		TRAFLAG = tRAFLAG;
	}

	public String getACN() {
		return ACN;
	}

	public void setACN(String aCN) {
		ACN = aCN;
	}

	public String getBNKCOD() {
		return BNKCOD;
	}

	public void setBNKCOD(String bNKCOD) {
		BNKCOD = bNKCOD;
	}

	public String getRAFLAG() {
		return RAFLAG;
	}

	public void setRAFLAG(String rAFLAG) {
		RAFLAG = rAFLAG;
	}
	
	
	
	
}
