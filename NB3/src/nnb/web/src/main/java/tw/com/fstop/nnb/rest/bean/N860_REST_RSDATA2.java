package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N860_REST_RSDATA2  extends BaseRestBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6941106375749059889L;
	
	private String REMDATE;		//託收日
	private String PAYACN;		//付款人帳號
	private String DATA;		//補充資料內容
	private String AMOUNT;		//票據金額
	private String FAILRSN;		//未入帳原因
	private String BNKNUM;		//付款行代號
	private String ACN;
	private String DUEDATE;		//到期日
	private String CHKNUM;		//支票號碼
	private String COLLBRH;		//代收行
	private String VALDATE;		//入帳日
	private String VALCNT;		//已入帳總張數
	private String VALAMT;		//已入帳總金額		
	private String TBBACC;		//託收人帳號
	private String TBBSEQ;		//託收人分號
	private String SISID;		//入帳識別碼
	
	public String getVALCNT() {
		return VALCNT;
	}
	public void setVALCNT(String vALCNT) {
		VALCNT = vALCNT;
	}
	public String getVALAMT() {
		return VALAMT;
	}
	public void setVALAMT(String vALAMT) {
		VALAMT = vALAMT;
	}
	public String getTBBACC() {
		return TBBACC;
	}
	public void setTBBACC(String tBBACC) {
		TBBACC = tBBACC;
	}
	public String getTBBSEQ() {
		return TBBSEQ;
	}
	public void setTBBSEQ(String tBBSEQ) {
		TBBSEQ = tBBSEQ;
	}
	public String getSISID() {
		return SISID;
	}
	public void setSISID(String sISID) {
		SISID = sISID;
	}
	public String getREMDATE() {
		return REMDATE;
	}
	public void setREMDATE(String rEMDATE) {
		REMDATE = rEMDATE;
	}
	public String getPAYACN() {
		return PAYACN;
	}
	public void setPAYACN(String pAYACN) {
		PAYACN = pAYACN;
	}
	public String getDATA() {
		return DATA;
	}
	public void setDATA(String dATA) {
		DATA = dATA;
	}
	public String getAMOUNT() {
		return AMOUNT;
	}
	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}
	public String getFAILRSN() {
		return FAILRSN;
	}
	public void setFAILRSN(String fAILRSN) {
		FAILRSN = fAILRSN;
	}
	public String getBNKNUM() {
		return BNKNUM;
	}
	public void setBNKNUM(String bNKNUM) {
		BNKNUM = bNKNUM;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getDUEDATE() {
		return DUEDATE;
	}
	public void setDUEDATE(String dUEDATE) {
		DUEDATE = dUEDATE;
	}
	public String getCHKNUM() {
		return CHKNUM;
	}
	public void setCHKNUM(String cHKNUM) {
		CHKNUM = cHKNUM;
	}
	public String getCOLLBRH() {
		return COLLBRH;
	}
	public void setCOLLBRH(String cOLLBRH) {
		COLLBRH = cOLLBRH;
	}
	public String getVALDATE() {
		return VALDATE;
	}
	public void setVALDATE(String vALDATE) {
		VALDATE = vALDATE;
	}
}
