package tw.com.fstop.nnb.spring.controller;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.service.TxnCusInvAttrService;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;

@RestController
@RequestMapping(value = "/MB/TXNCUSINVATTRIB")
public class TxnCusInvAttrHist_Controller {

	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	TxnCusInvAttrService txncusinvattrservice;
	
	
	@RequestMapping(value = "/queryhis" , method = {RequestMethod.POST} , produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody Map queryhis(HttpServletRequest request, HttpServletResponse response, @RequestBody Map<String,String> reqParam, Model model) {
		log.info(ESAPIUtil.vaildLog("queryhis>>{}"+ CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		try {
			//解決 Reflected XSS All Clients
			Map<String,String> okMap = ESAPIUtil.validStrMap(reqParam);
			bs = txncusinvattrservice.queryhis(okMap);
		} catch (Throwable e) {
			log.error("Throwable>>{}",e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", "查詢異常");
		}
		Map<String,Object> map = new LinkedHashMap<String,Object>();
		map.put("msgCode", bs.getMsgCode());
		map.put("message", bs.getMessage());
		map.put("result", bs.getResult());
		map.put("data", bs.getData());
		return map;
		
	}
	@RequestMapping(value = "/query" , method = {RequestMethod.POST} , produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody BaseResult query(HttpServletRequest request, HttpServletResponse response, @RequestBody Map<String,String> reqParam, Model model) {
		log.info(ESAPIUtil.vaildLog("findByDateRange>>{}"+ CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		try {
			//解決 Reflected XSS All Clients
			Map<String,String> okMap = ESAPIUtil.validStrMap(reqParam);
			bs = txncusinvattrservice.query(okMap);
		} catch (Throwable e) {
			log.error("Throwable>>{}",e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", "查詢異常");
		}
		return bs;
		
	}
	
	@RequestMapping(value = "/update" , method = {RequestMethod.POST} , produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody BaseResult update(HttpServletRequest request, HttpServletResponse response, @RequestBody Map<String,String> reqParam, Model model) {
		log.info(ESAPIUtil.vaildLog("reqParam>>{}"+ CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		try {
			String ADUSERIP = reqParam.get("aduserip");
			String AGREE = reqParam.get("agree");
			if(ADUSERIP.trim() == null || ADUSERIP.length()>20){
				bs.setResult(Boolean.FALSE);
				bs.setMessage("1", "使用者IP異常");
				return bs;
			}
			if(AGREE.trim() == null){
				bs.setResult(Boolean.FALSE);
				bs.setMessage("1", "未輸入使用者是否同意投資屬性結果");
				return bs;
			}
			
			bs = txncusinvattrservice.saveData2InvAttrAndInvAttrHist(reqParam);
		
		} catch (Throwable e) {
			log.error("Throwable>>{}",e.getMessage(), e);
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", "修改異常");
		}
		return bs;
		
	}
}
