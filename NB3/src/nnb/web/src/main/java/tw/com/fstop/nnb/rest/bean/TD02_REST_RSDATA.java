package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class TD02_REST_RSDATA extends BaseRestBean implements Serializable {


	private static final long serialVersionUID = 3588725544057128396L;
	/**
	 * 
	 */
	
	String PAYMTH;
	String TRNDATE;
	String TRNTIME;
	String TRNAMT;
	
	public String getPAYMTH() {
		return PAYMTH;
	}
	public void setPAYMTH(String pAYMTH) {
		PAYMTH = pAYMTH;
	}
	public String getTRNDATE() {
		return TRNDATE;
	}
	public void setTRNDATE(String tRNDATE) {
		TRNDATE = tRNDATE;
	}
	public String getTRNTIME() {
		return TRNTIME;
	}
	public void setTRNTIME(String tRNTIME) {
		TRNTIME = tRNTIME;
	}
	public String getTRNAMT() {
		return TRNAMT;
	}
	public void setTRNAMT(String tRNAMT) {
		TRNAMT = tRNAMT;
	}
	
}
