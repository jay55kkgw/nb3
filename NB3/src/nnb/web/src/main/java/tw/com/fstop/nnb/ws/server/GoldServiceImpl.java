package tw.com.fstop.nnb.ws.server;

import com.google.gson.Gson;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import org.springframework.ws.soap.security.wss4j2.Wss4jSecurityInterceptor;

import tw.com.fstop.nnb.ws.server.bean.InformGoldValue;
import tw.com.fstop.nnb.ws.server.bean.Gold;
import tw.com.fstop.nnb.ws.server.bean.GoldSell;
import tw.com.fstop.nnb.ws.server.bean.InformGoldValueResponse;
import tw.com.fstop.util.CodeUtil;

//key word >>spring-ws soaphandler oasis
//https://stackoverflow.com/questions/4368308/how-to-resolve-failure-of-jax-ws-web-service-invocation-mustunderstand-headers/40626826 
//https://codeday.me/bug/20180813/216825.html
//https://docs.spring.io/spring-ws/docs/2.4.0.RELEASE/reference/htmlsingle/
//https://docs.spring.io/spring-ws/docs/2.4.2.RELEASE/reference/#server-atEndpoint-methods
//https://blog.lqdev.cn/2018/11/09/springboot/chapter-thirty-three/

//https://segmentfault.com/a/1190000009623498
//https://segmentfault.com/a/1190000009845212

@Endpoint
public class GoldServiceImpl implements GoldService{
	static Logger log = LoggerFactory.getLogger(GoldServiceImpl.class);
	
	@Autowired
	GoldWSService goldwsservice;
	
	@PayloadRoot(namespace = "http://ws.fstop", localPart = "informGoldValue")
	public @ResponsePayload InformGoldValueResponse informGoldValue(@RequestPayload InformGoldValue informgoldvalue) {
		log.info("informGoldValue...");
		InformGoldValueResponse rs =null;
		GoldSell sell = null;
		try {
			sell = new GoldSell();
			log.info("gold>>{}",new Gson().toJson(informgoldvalue));
			Gold gold = informgoldvalue.getIn0();
			rs = new InformGoldValueResponse();
//		TODO 測試用
			gold.setTime(new DateTime().plusMinutes(5).toString("HHmmss"));
			
			sell = goldwsservice.send(gold);
			log.debug("sell.map>>{}",CodeUtil.toJson(sell));
			
			rs.setGoldSell(sell);
		} catch (Exception e) {
			log.error("informGoldValue.error>>{}",e);
			sell.setMsg_cod("ZX99");
			sell.setMsg_desc("系統異常");
			rs.setGoldSell(sell);
		}
		return rs;
	}
	
	
//	protected HttpServletRequest getHttpServletRequest() {
//	    TransportContext ctx = TransportContextHolder.getTransportContext();
//	    return ( null != ctx ) ? ((HttpServletConnection ) ctx.getConnection()).getHttpServletRequest() : null;
//	}
	
	/***
	 * 取得台銀報價通知信中報價類型的中文說明
	 * @param kind
	 * @return
	 */
	private String getKindDesc(String kind)
	{
		String result = "";
		
		if (kind.equals("3"))
			result = "修改";
		else if (kind.equals("4"))
			result = "取消";
		
		return result;
	}

	
}
