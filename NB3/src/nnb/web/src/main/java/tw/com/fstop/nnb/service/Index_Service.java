package tw.com.fstop.nnb.service;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

import org.apache.commons.beanutils.BeanUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import com.google.gson.internal.LinkedTreeMap;

import fstop.orm.po.ADMANN;
import fstop.orm.po.ADMCURRENCY;
import fstop.orm.po.ADMLOGINACL;
import fstop.orm.po.SYSPARAMDATA;
import fstop.orm.po.TXNUSER;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.comm.bean.Certificate_Bean;
import tw.com.fstop.nnb.comm.bean.Certificate_Bean.Ext;
import tw.com.fstop.nnb.comm.bean.Certificate_Bean.ExtPrincipal;
import tw.com.fstop.nnb.comm.bean.Certificate_Bean.Unit;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.AdmAnnDao;
import tw.com.fstop.tbb.nnb.dao.AdmHotopDao;
import tw.com.fstop.tbb.nnb.dao.AdmLoginAclDao;
import tw.com.fstop.tbb.nnb.dao.Nb3SysOpDao;
import tw.com.fstop.tbb.nnb.dao.SysParamDataDao;
import tw.com.fstop.tbb.nnb.dao.TxnFxSchPayDao;
import tw.com.fstop.tbb.nnb.dao.TxnLogDao;
import tw.com.fstop.tbb.nnb.dao.TxnTwSchPayDao;
import tw.com.fstop.tbb.nnb.util.DateUtil;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.I18nHelper;
import tw.com.fstop.web.util.WebUtil;

@Service
public class Index_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private AdmAnnDao admannDao;
	@Autowired
	private AdmHotopDao admhotopdao;
	@Autowired
	private TxnTwSchPayDao txntwschpayDao;
	@Autowired
	private TxnFxSchPayDao txnfxschpayDao;
	@Autowired
	private SysParamDataDao sysparamdatadao;
	@Autowired
	private AdmLoginAclDao admloginaclDao;
	@Autowired
	private TxnLogDao txnlogdao;
	@Autowired
	private Nb3SysOpDao nb3sysopdao;
	@Autowired
	private DaoService daoService;
	
	@Autowired
	private Acct_Service acct_service;
	@Autowired
	private Fcy_Acct_Service fcy_acct_service;
	@Autowired
	private Creditcard_Inquiry_Service creditcard_inquiry_service;
	@Autowired
	private Fund_Query_Service fund_query_service;
	@Autowired
	private Loan_Query_Service loan_query_service;
	@Autowired
	private Fund_Transfer_Service fund_transfer_service;
	
	@Autowired
	String certificate_key ;
	@Autowired
	String certificate_url ;

	@Autowired
	private I18n i18n;
	
	/* 更新TXNUSER並取得用戶名稱 */
	public BaseResult getTxnUser(String cusidn) {
		log.trace("getTxnUser...");
		BaseResult bs = null;

		try {
			Map<String,Object> resultMap = new HashMap<String,Object>();
			
			bs = new BaseResult();
			// TXNUSER-改在首頁發Ajax打N920更新資料庫TXNUSER
			BaseResult bsN920 = N920_REST(cusidn, Base_Service.LOGIN);
			if(!"0".equals(bsN920.getMsgCode())) {
				log.error("N920.error: {}", BeanUtils.describe(bsN920));
				bs.setMsgCode(bsN920.getMsgCode());
				bs.setMessage(bsN920.getMessage());
				bs.setResult(false);
				bs.setNext("error"); // 用來區別電文或程式錯誤訊息
				return bs;
			}
			
			// 有原住民姓名(CUSNAME)以原住民姓名為優先
//			String dpname = ((Map<String, Object>) bsN920.getData()).get("NAME").toString();
//			if(StrUtil.isEmpty(dpname)) {
//				dpname = ((Map<String, Object>) bsN920.getData()).get("CUSNAME").toString();
//			}
			String dpname = ((Map<String, Object>) bsN920.getData()).get("CUSNAME").toString();
			if(StrUtil.isEmpty(dpname)) {
				dpname = ((Map<String, Object>) bsN920.getData()).get("NAME").toString();
			}
			
			// TXNUSER
			TXNUSER txnUser = daoService.chkRecord(cusidn, dpname);
			if(txnUser!=null){
				String dpUserName = txnUser.getDPUSERNAME() !=null ? txnUser.getDPUSERNAME().replaceAll("/\\?| 　/"," ") : "";
				// 字霸--隱藏姓名
				if(StrUtil.isNotEmpty(dpUserName)) {
					dpUserName = WebUtil.hideusername1Convert(dpUserName);
				}
				
				resultMap.put("DPUSERNAME", dpUserName);
				resultMap.put("DPMYEMAIL",  txnUser.getDPMYEMAIL() !=null ? txnUser.getDPMYEMAIL() : "");
				log.trace("loginInit.resultMap: {}", resultMap);
				
				bs.setMsgCode("0");
				bs.setMessage("success");
				bs.setResult(true);
				bs.setData(resultMap);
			}
			
		} catch (Exception e) {
			log.error("", e);
		}
		return bs;
	}
	
	/* 使用者基本資料 */
	public BaseResult getA106(String cusidn, Map<String, String> reqParam) {
		log.trace("getA106...");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = A106_REST(cusidn, reqParam);
			
		} catch (Exception e) {
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * 公告訊息
	 * 
	 * @return
	 */
	public List<ADMANN> getByUser(String cusidn) {
		List<ADMANN> list_result = new ArrayList<ADMANN>();
		try {
			List<ADMANN> list_admad = admannDao.getByUser(cusidn);
			log.trace(ESAPIUtil.vaildLog("getByUser.list_admad: " + CodeUtil.toJson(list_admad)));
			for ( ADMANN po : list_admad ) {
				ADMANN newPo = new ADMANN();
				newPo.setID(po.getID());
				newPo.setTYPE(po.getTYPE());
				newPo.setTITLE(po.getTITLE());
				newPo.setCONTENTTYPE(po.getCONTENTTYPE());
				newPo.setURL(po.getURL());
				newPo.setCONTENT(po.getCONTENT());
				newPo.setSRCCONTENT(po.getSRCCONTENT());
				list_result.add(newPo);
			}
			log.trace(ESAPIUtil.vaildLog("getByUser.list_result: " + CodeUtil.toJson(list_result)));
		} catch (Exception e) {
			log.error("{}", e);
		}
		return list_result;
	}
	
	/**
	 * 行事曆--台幣
	 * 
	 * @return
	 */
	public List<String> getReservTwDate(String cusidn) {
		List<String> reservTwDateList = txntwschpayDao.getByUser(cusidn);
		log.trace(ESAPIUtil.vaildLog("getByUser.reservTwDateList: "+ CodeUtil.toJson(reservTwDateList)));
		return reservTwDateList;
	}
	
	/**
	 * 行事曆--外幣
	 * 
	 * @return
	 */
	public List<String> getReservFxDate(String cusidn) {
		List<String> reservFxDateList = txnfxschpayDao.getByUser(cusidn);
		log.trace(ESAPIUtil.vaildLog("getByUser.reservFxDateList: " + CodeUtil.toJson(reservFxDateList)));
		return reservFxDateList;
	}
	
	/**
	 * 取得Timeout時間(秒)
	 * 
	 * @return
	 */
	public String getTimeout() {
		SYSPARAMDATA SYSPARAMDATA = sysparamdatadao.getByPK("NBSYS");
		String adsessionto = SYSPARAMDATA.getADSESSIONTO();
		return adsessionto;
	}

	/**
	 * 判斷使用者當天是否看過第二階段提示訊息
	 * 
	 * @return
	 */
	public boolean getAclNotedate(String cusidn) {
		boolean result = true;
		try {
			String today = DateUtil.getDate("");
			log.debug("getAclNotedate.today: {}", today);
			
			// 白名單應該要有資料才能進入
			List<ADMLOGINACL> admloginaclList = admloginaclDao.findByUserId(cusidn);
			if (!admloginaclList.isEmpty()) {
				ADMLOGINACL po = admloginaclList.get(0);
				// 比對日期，決定是否顯示提示訊息
				String notedate = po.getNOTEDATE();
				if ( StrUtil.isNotEmpty(notedate) ) {
					// 20201119--電金襄理說要每天顯示一次
//					log.debug("getAclNotedate.notedate: {}", notedate); // Stored Log Forging
					result = today.equals(notedate) ? false : true;
//					result=false; // 改成看過了就不顯示
					log.debug("getAclNotedate.result: {}", result);
					
				} else {
					result = true;
				}
				
				// 如果是true，就要顯示給使用者看，同時更新ADMLOGINACL.NOTEDATE
				if (result) {
					log.debug("getAclNotedate.cusidn.update: {}", cusidn);
					po.setNOTEDATE(today);
					admloginaclDao.update(po);
				}
				
			} // 不在白名單
			else {
				String isProd = SpringBeanFactory.getBean("isProd");
				log.info("index.isProd: {}", isProd);
				
				// 如果是第3階段，不在白名單則新增一筆資料，顯示公告訊息用
				if ("3".equals(isProd)) {
					ADMLOGINACL po = new ADMLOGINACL();
					po.setADUSERID(cusidn);
					po.setLASTDATE(today);
					po.setLASTTIME("000000");
					po.setLASTUSER("NB3AP");
					po.setNOTEDATE(today);
					admloginaclDao.save(po);
					result = true;
					
				} else {
					result = false; // 不顯示公告訊息
				}
			}
			
		} catch (Exception e) {
			log.error("",e);
		}
		return result;
		
	}
	
	
	
	/**
	 * 取得台幣資料
	 * 
	 * @param cusidn 使用者統編
	 * @return
	 */
    public BaseResult getTW_summary(String cusidn) {
    	BaseResult bs = null;
        try {
        	bs = new BaseResult();
			// 臺幣餘額查詢
        	CompletableFuture<String> twBalanceResult = getTW_balance(cusidn);
			// 臺幣餘額查詢
        	CompletableFuture<String> twDdepositResult = getTW_deposit(cusidn);
			
			CompletableFuture.allOf(twBalanceResult, twDdepositResult).join();
			
			log.trace("twBalanceResult.get(): " + twBalanceResult.get());
			log.trace("twDdepositResult.get(): " + twDdepositResult.get());

			bs.setResult(true);
			bs.addData("summary_TW_balance", twBalanceResult.get());
			bs.addData("summary_TW_deposit", twDdepositResult.get());
            
        } catch (Exception e) {
            log.error("getTW_balance error: {}", e);
            bs.setResult(false);
        }
        return bs;
    }
    
	/**
	 * 取得台幣活存資料
	 * 
	 * @param cusidn 使用者統編
	 * @return
	 */
    @Async(value="myexecutor")
    public CompletableFuture<String> getTW_balance(String cusidn) {
        try {
        	BaseResult bs = acct_service.balance_query(cusidn);
        	return CompletableFuture.completedFuture(CodeUtil.toJson(bs.getData()));
//            return new AsyncResult<String>(CodeUtil.toJson(bs.getData()));
            
        } catch (Exception e) {
            log.error("getTW_balance error: {}", e);
            return CompletableFuture.completedFuture(CodeUtil.toJson("error"));
//            return new AsyncResult<String>("error");
        }
    }
    /**
     * 取得台幣定存資料
     * 
     * @param cusidn 使用者統編
     * @return
     */

	public BaseResult time_deposit_details(String cusidn) {

		log.trace("time_deposit_details");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = N420_NoTxn_REST(cusidn);
			if(bs!=null && bs.getResult()) {
				Map<String, Object> bsData = (Map) bs.getData();
				// 總計金額格式化
				bsData.put("TOTAMT", NumericUtil.fmtAmount((String) bsData.get("DPTAMT"), 2));
				// Table內容修改
				ArrayList<Map<String, String>> rows = (ArrayList<Map<String, String>>) bsData.get("REC");
				for (Map<String, String> row : rows) 
				{
					// 存款種類
					row.put("TYPENAME", acct_service.getTYPE(row.get("TYPE")));
					// 轉存方式
					row.put("TYPE1", I18nHelper.getTYPE1(row.get("TYPE1")));
					// 起存日
					row.put("DPISDT", DateUtil.convertDate(2, row.get("DPISDT"), "yyyMMdd", "yyy/MM/dd"));
					// 到期日
					String DUEDAT = row.get("DUEDAT");
					row.put("DUEDAT", DateUtil.convertDate(2, DUEDAT, "yyyMMdd", "yyy/MM/dd"));
					// 存款金額
					row.put("AMTFDPFMT", NumericUtil.fmtAmount(row.get("AMTFDP"), 2));
					// 利率格式化
					row.put("ITR", row.get("ITR"));
					// 計息方式
					row.put("INTMTH", I18nHelper.getINTMTH(row.get("INTMTH")));

					// 民國年轉西元年
					DUEDAT = DateUtil.convertDate(1, DUEDAT, "yyyMMdd", "yyyyMMdd");
					// 備註欄依今天是否假日及定存單到期日，決定要否提示適逢例假日說明連結
					row.put("REMIND", acct_service.getRemind(Integer.valueOf(DUEDAT)).toString());
					
					// 自動轉期未轉次數
					row.put("AUTXFTM", acct_service.getAUTXFTM(row.get("AUTXFTM")));
					
					//新增判斷快速選單顯示欄位用 ACN之第4跟5位 = [10,12,60,62,64] 可做解約
					ArrayList<String> conditionList = new ArrayList<String>();
					List list = Arrays.asList("10","12","60","62","64");
					conditionList.addAll(list);
					if (!"5".equals(row.get("TYPE"))) {
						if(conditionList.contains(row.get("ACN").substring(3,5))) {
							//可做解約
							row.put("OPTIONTYPE", "0");
						}else {
							//不可做解約
							row.put("OPTIONTYPE", "1");
						}
					}else {
						//零存整付
						row.put("OPTIONTYPE", "2");
					}
					
					//for 快速選單用
					String jsonString = CodeUtil.toJson(row);
					jsonString.replace("\"", "&acute;");
					row.put("MapValue",jsonString.replace("\"", "&quot;"));
				}
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("time_deposit_details error >> {}", e);
		}
		return bs;
	}
    
    @Async(value="myexecutor")
    public CompletableFuture<String> getTW_deposit(String cusidn) {
    	try {
    		BaseResult bs = time_deposit_details(cusidn);
    		return CompletableFuture.completedFuture(CodeUtil.toJson(bs.getData()));
//    		return new AsyncResult<String>(CodeUtil.toJson(bs.getData()));
    		
    	} catch (Exception e) {
    		log.error("getTW_deposit error: {}", e);
    		return CompletableFuture.completedFuture(CodeUtil.toJson("error"));
//    		return new AsyncResult<String>("error");
    	}
    }

	/**
	 * 外匯活存餘額
	 * 
	 * @param sessionId
	 * @return
	 */
	public BaseResult balance_query(String cusidn, String acn) {

		log.trace("f_balance_query_service");
		BaseResult bs = null;
		BaseResult ck = null;
		try {
			bs = new BaseResult();
			// call ws API
			bs = N510_NoTxn_REST(cusidn, acn);
			ck = N920_REST(cusidn, FX_OUT_DEP_ACNO);
			if (bs != null && bs.getResult()) {
				Map<String, Object> callRow = (Map) bs.getData();
				log.trace("callRow>>{}", callRow);
				// Table1內容修改
				ArrayList<Map<String, String>> callTable1 = (ArrayList<Map<String, String>>) callRow.get("ACNInfo");
				
				Map<String, Object> ckData = (Map) ck.getData();
				ArrayList<Map<String, String>> ckrows = (ArrayList<Map<String, String>>) ckData.get("REC");
				
				String isoutACN = null;
				
				for (Map<String, String> map : callTable1) {
					// 可用餘額
					map.put("AVAILABLE", NumericUtil.fmtAmount(map.get("AVAILABLE"), 2));
					// 帳戶餘額
					map.put("BALANCE", NumericUtil.fmtAmount(map.get("BALANCE"), 2));
					String KIND = fcy_acct_service.getBeanMapValue("fcyQuery", map.get("ACN").substring(3, 5));
					if(KIND!=null && !KIND.equals("")){
					KIND = i18n.getMsg(KIND);
					}
					map.put("KIND", KIND);	
					for (Map<String, String> ckrow : ckrows) {
//						log.trace("ACN={}",ckrow.get("ACN"));
						if(ckrow.get("ACN") != null)
							isoutACN = "Y";
						else isoutACN = "N";
					}
//					log.debug("isoutACN={}",isoutACN);
					map.put("OUTACN", isoutACN);
				}
				// Table2內容修改
				ArrayList<Map<String, String>> callTable2 = (ArrayList<Map<String, String>>) callRow.get("AVASum");
				for (Map<String, String> mapTwo : callTable2) {
					// 可用餘額小計
					mapTwo.put("AVAILABLE",
							NumericUtil.fmtAmount(new BigDecimal(mapTwo.get("AVAILABLE")).toPlainString(), 2));
				}
				// Table3內容修改
				ArrayList<Map<String, String>> callTable3 = (ArrayList<Map<String, String>>) callRow.get("BALSum");
				for (Map<String, String> mapThree : callTable3) {
					// 存款金額小計
					mapThree.put("BALANCE",
							NumericUtil.fmtAmount(new BigDecimal(mapThree.get("BALANCE")).toPlainString(), 2));
				}
			}
		} catch (Exception e) {
			log.error("Fcy_Acct_Service-balance_query ", e);
		}

		return bs;
	}

	/**
	 * 取得外幣活存資料
	 * 
	 * @param cusidn 使用者統編
	 * @return
	 */
    @Async
    public Future<String> getFX_balance(String cusidn) {
        try {
        	BaseResult bs = balance_query(cusidn, null);
			List<ADMCURRENCY> admCurrencyList = daoService.getCRYList();
			List<Map<String,Object>> avasum = (List<Map<String,Object>>)((Map<String,Object>)bs.getData()).get("AVASum");
			for(Map<String,Object> ava : avasum) {
				ADMCURRENCY po = daoService.getCRY((String)ava.get("CUID"));
				if(po != null) {
					ava.put("CUID_NAME", po.getADCCYNAME());
				}
			}
            return new AsyncResult<String>(CodeUtil.toJson(bs.getData()));
            
        } catch (Exception e) {
            log.error("getFX_balance error: {}", e);
            return new AsyncResult<String>("error");
        }
    }

	/**
	 * 外匯定存明細
	 * 
	 * @param sessionId
	 * @return
	 */
	public BaseResult f_time_deposit_details(String cusidn, String acn) {

		log.trace("time_deposit_details_service");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// call ws API
			bs = N530_NoTxn_REST(cusidn, acn);
			if (bs != null && bs.getResult()) {
				Map<String, Object> callRow = (Map) bs.getData();
				// 總計金額處理
				ArrayList<String> fcytotmat = new ArrayList<>();
				String fcytotal = null;
				for (int i = 1; i <= 10; i++) {
					// 幣別電文處理
					String amtcuid = (String) callRow.get("AMTCUID_" + i);
					log.trace("amtcuid>>{}", amtcuid);
					// 金額電文處理
					String fxtotamt = (String) callRow.get("FXTOTAMT_" + i);
					if (amtcuid != null && fxtotamt != null) {
						String fcytotamt = new BigDecimal(fxtotamt).toPlainString();
						fcytotal = amtcuid + " " + NumericUtil.fmtAmount(fcytotamt, 2);
						log.trace("fcytotal>>{}", fcytotal);
						fcytotmat.add(fcytotal);
					}
				}
				// 總計金額
				log.trace("fcytotmat>>{}", fcytotmat);
				bs.addData("FCYTOTMAT", fcytotmat);
				
				// REC"電文處理
				ArrayList<Map<String, String>> callTable = (ArrayList<Map<String, String>>) callRow.get("REC");
				ArrayList<Map<String, String>> callRec = new ArrayList<Map<String, String>>();
				Double itr = null;
				log.trace("callTableM>>{}", callTable);
				for (Map<String, String> map : callTable) {
					if ((!"".equals(map.get("ACN"))) && map.get("ACN") != null) {
						// 計息方式
						map.put("INTMTH", fcy_acct_service.getINTMTH(map.get("INTMTH")));
						// 日期
						map.put("DPISDT", DateUtil.convertDate(2, map.get("DPISDT"), "yyyMMdd", "yyy/MM/dd"));
						map.put("DUEDAT", DateUtil.convertDate(2, map.get("DUEDAT"), "yyyMMdd", "yyy/MM/dd"));
						// 金額顯示處理
						map.put("BALANCE", NumericUtil.fmtAmount(map.get("BALANCE"), 2));
						// 利率處理
						map.put("ITR", NumericUtil.fmtAmount(map.get("ITR"), 3));
						// 次數
						if ("999".equals(map.get("AUTXFTM"))) {
							map.put("AUTXFTM", i18n.getMsg("LB.Unlimited"));
						}
						
						//for選單
						if("52".equals(map.get("ACN").substring(3, 5))||"60".equals(map.get("ACN").substring(3, 5))) {
							map.put("OPTIONTYPE", "1");
						}
						//for 快速選單用
						String jsonString = CodeUtil.toJson(map);
						jsonString.replace("\"", "&acute;");
						map.put("MapValue",jsonString.replace("\"", "&quot;"));
						log.trace("test MapValue>>>>>{}",map.get("MapValue"));
						callRec.add(map);
					}
				}
				callRow.put("REC", callRec);
			}
		} catch (Exception e) {
			// e.printStackTrace();
			log.error("time_deposit", e);
		}
		return bs;
	}
    
    /**
     * 取得外幣定存資料
     * 
     * @param cusidn 使用者統編
     * @return
     */
    @Async
    public Future<String> getFX_deposit(String cusidn) {
    	try {
    		BaseResult bs = f_time_deposit_details(cusidn, null);
			List<String> fcyTotmat = (List<String>)((Map<String,Object>)bs.getData()).get("FCYTOTMAT");
			List<String> fcyTotmatChange = new ArrayList<String>();
			log.trace("{}",fcyTotmat);
			for(String fcy : fcyTotmat) {
				ADMCURRENCY po = daoService.getCRY(fcy.substring(0,3));
				if(po != null) {
					fcy = fcy.substring(0,3) + "/" +po.getADCCYNAME() + "/" + fcy.substring(3,fcy.length());
				}else {
					fcy = fcy.substring(0,3) + "//" + fcy.substring(3,fcy.length());
				}
				fcyTotmatChange.add(fcy);
			}
			bs.addData("FCYTOTMAT", fcyTotmatChange);
    		return new AsyncResult<String>(CodeUtil.toJson(bs.getData()));
    		
    	} catch (Exception e) {
    		log.error("getFX_deposit error: {}", e);
    		return new AsyncResult<String>("error");
    	}
    }
	/**
	 * N810走Rest
	 * 
	 * @param custid
	 * @param
	 * @return
	 */
	public BaseResult overview_rest(Map<String, String> reqParam) {
		log.trace("creditcard_service_overview...");
		log.trace(ESAPIUtil.vaildLog("custid: " + reqParam.get("CUSIDN")));
		// 處理結果
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// 電文N810
			bs = N810_NoTxn_REST(reqParam);

			// 判斷TOPMSG來決定導向正確頁or錯誤訊息頁

			if ("".equals(((Map<String, String>) bs.getData()).get("TOPMSG"))) {
				// 正確頁
				// 把Client端需要的資料重新封裝
				creditcard_inquiry_service.overview_processing_rest(bs);
				log.debug("N810_REST_RESULT: " + bs.getData());

			} else {
				// 錯誤訊息頁
				bs.setResult(false);
				bs.setErrorMessage(((Map<String, String>) bs.getData()).get("TOPMSG"),
						((Map<String, String>) bs.getData()).get("msgName"));
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("overview_rest error >> {}",e);
		}
		// 回傳處理結果
		return bs;
	}
    /**
     * 取得信用卡資料
     * 
     * @param cusidn 使用者統編
     * @return
     */
    @Async
    public Future<String> getCRD_summary(String cusidn) {
    	try {
    		// N810
			Map<String, String> reqParam = new HashMap<String,String>();
			reqParam.put("CUSIDN", cusidn);
			BaseResult bs = overview_rest(reqParam);
			return new AsyncResult<String>(CodeUtil.toJson(bs.getData()));
			
    	} catch (Exception e) {
    		log.error("getCRD_summary error: {}", e);
    		return new AsyncResult<String>("error");
    	}
    }

    @Async
    public Future<String> getBLA_summary(String cusidn) {
    	try {
    		// N810
    		Map<String, String> reqParam = new HashMap<String, String>();
			BaseResult bs = fund_query_service.oversea_balance_result(cusidn, reqParam);
			return new AsyncResult<String>(CodeUtil.toJson(bs.getData()));
			
    	} catch (Exception e) {
    		log.error("getBLA_summary error: {}", e);
    		return new AsyncResult<String>("error");
    	}
    }
    
    /**
     * 取得基金資料
     * 
     * @param cusidn 使用者統編
     * @return
     */
    @Async
    public Future<String> getFUND_summary(String cusidn, String dpusername) {
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = C012_NoTxn_REST(cusidn);
			
			DecimalFormat df = new DecimalFormat("#0.00");
			Map<String, Object> bsData = (Map<String, Object>) bs.getData();
			
			String TOTTWDAMT = (String) bsData.get("TOTTWDAMT");
			log.debug("TOTTWDAMT={}", TOTTWDAMT);
			if (TOTTWDAMT != null) {
				TOTTWDAMT = TOTTWDAMT.replaceAll("\\.", "");
			} else {
				TOTTWDAMT = "";
			}
			log.debug("TOTTWDAMT={}", TOTTWDAMT);
			String TOTTWDAMTFormat = fund_transfer_service.formatNumberString(TOTTWDAMT, 2);
			log.debug("TOTTWDAMTFormat={}", TOTTWDAMTFormat);
			bsData.put("TOTTWDAMTFormat", TOTTWDAMTFormat);
			
			String TOTREFVALUE = (String) bsData.get("TOTREFVALUE");
			log.debug("TOTREFVALUE={}", TOTREFVALUE);
			if (TOTREFVALUE != null) {
				TOTREFVALUE = TOTREFVALUE.replaceAll("\\.", "");
			} else {
				TOTREFVALUE = "";
			}
			log.debug("TOTREFVALUE={}", TOTREFVALUE);
			String TOTREFVALUEFormat = fund_transfer_service.formatNumberString(TOTREFVALUE, 2);
			log.debug("TOTREFVALUEFormat={}", TOTREFVALUEFormat);
			bsData.put("TOTREFVALUEFormat", TOTREFVALUEFormat);
			

			List<Map<String, Object>> secondRows = (List<Map<String, Object>>) bsData.get("REC2");
			log.debug("secondRows={}", secondRows);
			String SUBTOTAMT = "";
			String SUBREFVALUE = "";
			String SUBTOTAMTFormat = "";
			String SUBREFVALUEFormat = "";
			ADMCURRENCY admCurrency;
			String ADCCYNAME;
			List<Map<String, Object>> secondRowsFormat = new ArrayList<Map<String, Object>>();
			for (Map<String , Object> row:secondRows) {
				String SUBCRY = (String) row.get("SUBCRY");
				log.debug("SUBCRY={}", SUBCRY);
				if (SUBCRY != null) {
					admCurrency = fund_transfer_service.getCRYData(SUBCRY);

					if (admCurrency != null) {
						//ADCCYNAME = admCurrency.getADCCYNAME();
						Locale currentLocale = LocaleContextHolder.getLocale();
			    		String locale = currentLocale.toString();
			    		
			    		switch (locale) {
						case "en":
							ADCCYNAME = admCurrency.getADCCYENGNAME();
							break;
						case "zh_CN":
							ADCCYNAME = admCurrency.getADCCYCHSNAME();
							break;
						default:
							ADCCYNAME = admCurrency.getADCCYNAME();
							break;
		                }
						log.debug("ADCCYNAME={}", ADCCYNAME);

						row.put("ADCCYNAME", ADCCYNAME);
					}
				}
				if (SUBCRY.equals("NTD")) {
					SUBTOTAMT = (String) row.get("SUBTOTAMT");
					log.debug("NTDSUBTOTAMT={}", SUBTOTAMT);
					if (SUBTOTAMT != null) {
						SUBTOTAMT = SUBTOTAMT.replaceAll("\\.", "");
					} else {
						SUBTOTAMT = "";
					}
					log.debug("NTDSUBTOTAMT={}", SUBTOTAMT);
					SUBTOTAMTFormat = fund_transfer_service.formatNumberString(SUBTOTAMT, 2);
					log.debug("NTDSUBTOTAMTFormat={}", SUBTOTAMTFormat);
					bsData.put("NTDSUBTOTAMTFormat", SUBTOTAMTFormat);

					SUBREFVALUE = (String) row.get("SUBREFVALUE");
					log.debug("NTDSUBREFVALUE={}", SUBREFVALUE);
					if (SUBREFVALUE != null) {
						SUBREFVALUE = SUBREFVALUE.replaceAll("\\.", "");
					} else {
						SUBREFVALUE = "";
					}
					log.debug("NTDSUBREFVALUE={}", SUBREFVALUE);
					SUBREFVALUEFormat = fund_transfer_service.formatNumberString(SUBREFVALUE, 2);
					log.debug("NTDSUBREFVALUEFormat={}", SUBREFVALUEFormat);
					bsData.put("NTDSUBREFVALUEFormat", SUBREFVALUEFormat);
				}else {
					SUBTOTAMT = (String) row.get("SUBTOTAMT");
					log.debug("SUBTOTAMT={}", SUBTOTAMT);
					if (SUBTOTAMT != null) {
						SUBTOTAMT = SUBTOTAMT.replaceAll("\\.", "");
					} else {
						SUBTOTAMT = "";
					}
					log.debug("SUBTOTAMT={}", SUBTOTAMT);
					SUBTOTAMTFormat = fund_transfer_service.formatNumberString(SUBTOTAMT, 2);
					log.debug("SUBTOTAMTFormat={}", SUBTOTAMTFormat);
					row.put("SUBTOTAMTFormat", SUBTOTAMTFormat);

					SUBREFVALUE = (String) row.get("SUBREFVALUE");
					log.debug("SUBREFVALUE={}", SUBREFVALUE);
					if (SUBREFVALUE != null) {
						SUBREFVALUE = SUBREFVALUE.replaceAll("\\.", "");
					} else {
						SUBREFVALUE = "";
					}
					log.debug("SUBREFVALUE={}", SUBREFVALUE);
					SUBREFVALUEFormat = fund_transfer_service.formatNumberString(SUBREFVALUE, 2);
					log.debug("SUBREFVALUEFormat={}", SUBREFVALUEFormat);
					row.put("SUBREFVALUEFormat", SUBREFVALUEFormat);
					
					secondRowsFormat.add(row);
				}
			}
			
			BigDecimal TOTTWDAMTFormatD = new BigDecimal(NumericUtil.unfmtAmount(TOTTWDAMT));
			BigDecimal TOTREFVALUEFormatD = new BigDecimal(NumericUtil.unfmtAmount(TOTREFVALUE));
			BigDecimal SUBTOTAMTFormatD = new BigDecimal(NumericUtil.unfmtAmount(SUBTOTAMT));
			BigDecimal SUBREFVALUEFormatD = new BigDecimal(NumericUtil.unfmtAmount(SUBREFVALUE));

			String FCYSUBTOTAMT = TOTTWDAMTFormatD.subtract(SUBTOTAMTFormatD).toString();
			log.debug("FCYSUBREFVALUE={}", FCYSUBTOTAMT);
			String FCYSUBTOTAMTEFormat = fund_transfer_service.formatNumberString(FCYSUBTOTAMT, 2);
			log.debug("FCYSUBTOTAMTEFormat={}", FCYSUBTOTAMTEFormat);
			bsData.put("FCYSUBTOTAMTEFormat", FCYSUBTOTAMTEFormat);
			
			String FCYSUBREFVALUE = TOTREFVALUEFormatD.subtract(SUBREFVALUEFormatD).toString();
			log.debug("FCYSUBREFVALUE={}", FCYSUBREFVALUE);
			String FCYSUBREFVALUEFormat = fund_transfer_service.formatNumberString(FCYSUBREFVALUE, 2);
			log.debug("FCYSUBREFVALUEFormat={}", FCYSUBREFVALUEFormat);
			bsData.put("FCYSUBREFVALUEFormat", FCYSUBREFVALUEFormat);
			bsData.put("secondRows", secondRowsFormat);
			
    		return new AsyncResult<String>(CodeUtil.toJson(bs.getData()));
    		
    	} catch (Exception e) {
    		log.error("getFUND_summary error: {}", e);
    		return new AsyncResult<String>("error");
    	}
    }
    
    
    /**
     * 取得借款資料
     * 
     * @param cusidn 使用者統編
     * @return
     */
    @Async
    public Future<String> getLOAN_summary(Map<String, String> reqParam) {
    	try {
    		
    		if(null==reqParam.get("N320GO")||"".equals(reqParam.get("N320GO"))){
    			reqParam.put("N320GO", "GO");
    		}
    		if(null==reqParam.get("N552GO")||"".equals(reqParam.get("N552GO"))) {
    			reqParam.put("N552GO", "GO");
    		}
    		BaseResult bs = loan_query_service.loan_detail(reqParam);
    		return new AsyncResult<String>(CodeUtil.toJson(bs.getData()));
    		
    	} catch (Exception e) {
    		log.error("getLOAN_summary error: {}", e);
    		return new AsyncResult<String>("error");
    	}
    }
	/**
	 * 借款明細查詢
	 * 
	 * @param sessionId
	 * @param pw
	 * @return
	 */
	public BaseResult loan_detail(Map<String, String> reqParam) {
		log.trace("loan_query_service");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// call ws API
			bs = N320_NoTxn_REST(reqParam);
			// 資料處理後重新封裝bs
			if (bs != null) {
				// 把Client端需要的資料重新封裝
				if (bs.getResult()) {
					loan_query_service.loan_detail_processing(bs);
				}
				log.debug("N320_REST: " + bs.getData());
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}

	
    /**
     * 取得借款資料總額--臺幣
     */
    @Async
    public Future<String> getLOAN_N320(Map<String, String> reqParam) {
    	BaseResult bs = null;
    	try {
    		Map<String, Object> resultMap = new HashMap<String, Object>(); 
    		
    		String topmsg_320 = "";
    		String twnum = "";
    		String acn = "";
    		String totalbal_320 = "";
			
    		int count = 0;
			do {
				reqParam.put("N320GO", "GO");
				bs = new BaseResult();
				bs = loan_detail(reqParam);
				Map<String, Object> tmpMap = new HashMap<String, Object>(); 
				tmpMap = (Map<String, Object>) bs.getData();
				
				// 是否繼續迴圈
				topmsg_320 = String.valueOf(tmpMap.get("TOPMSG_320"));
				
				// 只打一次
				if (count == 0) {
					// 帳戶
					ArrayList<LinkedTreeMap<String, String>> twList = (ArrayList<LinkedTreeMap<String, String>>) tmpMap.get("TW");
					if (twList!=null && twList.size()!=0 && twList.get(0).containsKey("ACN")) {
						acn = twList.get(0).get("ACN");
					}
					// 筆數
					twnum = String.valueOf(tmpMap.get("TWNUM"));
					// 金額
					totalbal_320 = String.valueOf(tmpMap.get("TOTALBAL_320"));
					
				} else {
					// 筆數
					twnum = String.valueOf( Integer.valueOf(twnum) + Integer.valueOf(tmpMap.get("TWNUM").toString()) );
					// 金額加總
					String tmp320 = String.valueOf(tmpMap.get("TOTALBAL_320"));
					BigDecimal b1 = new BigDecimal(totalbal_320);
					BigDecimal b2 = new BigDecimal(tmp320);
					totalbal_320 = NumericUtil.formatNumberString(b1.add(b2).toString(), 2);
				}
				
				count++;
				
			} while ( "OKOV".equals(topmsg_320) );
			
			resultMap.put("ACN", acn); // 帳戶
			resultMap.put("TWNUM", twnum); // 筆數
			resultMap.put("TOTALBAL_320", totalbal_320); // 總金額
			
    		return new AsyncResult<String>(CodeUtil.toJson(resultMap));
    		
    		
    	} catch (Exception e) {
    		log.error("getLOAN_N320 error: {}", e);
    		return new AsyncResult<String>("error");
    	}
    }
    
    /**
     * 取得借款資料總額--外幣
     */
    @Async
    public Future<String> getLOAN_N552(Map<String, String> reqParam) {
    	BaseResult bs = null;
    	try {
    		Map<String, Object> resultMap = new HashMap<String, Object>(); 
    		
    		
    		ArrayList<LinkedTreeMap<String, String>> cryList = new ArrayList<LinkedTreeMap<String, String>>();
    		
    		Map<String, String> cryMap = null;
    		
			reqParam.put("N552GO", "GO");
			reqParam.put("OKOVNEXT", "TRUE");
    			
			bs = new BaseResult();
			bs = loan_detail(reqParam);
			
  			Map<String, Object> tmpMap = new HashMap<String, Object>(); 
  			tmpMap = (Map<String, Object>) bs.getData();
			// 帳戶
  			ArrayList<LinkedTreeMap<String, String>> twList = (ArrayList<LinkedTreeMap<String, String>>) tmpMap.get("FX");
			String acn = twList.get(0).get("LNACCNO");
    		
    		resultMap.put("ACN", acn); // 帳戶
    		resultMap.put("FXNUM", tmpMap.get("FXNUM")); // 筆數
			for(Map<String,Object> ava : (List<Map<String,Object>>)tmpMap.get("CRY")) {
				ADMCURRENCY po = daoService.getCRY((String)ava.get("AMTLNCCY"));
				if(po != null) {
					ava.put("CUID_NAME", po.getADCCYNAME());
				}
			}
    		resultMap.put("CRYMAP", tmpMap.get("CRY")); // 幣別-金額
    		
    		return new AsyncResult<String>(CodeUtil.toJson(resultMap));
    		
    	} catch (Exception e) {
    		log.error("getLOAN_N552 error: {}", e);
    		return new AsyncResult<String>("error");
    	}
    }
    

	
	public BaseResult getUsuallyList(String cusidn) {
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			List<Map> list = admhotopdao.findLast5LimitData(cusidn);
			bs.addData("usuallyList", list);
			bs.setResult(true);
		} catch (Exception e) {
			log.error("lock_reset_result: {}", e);
			bs.setResult(false);
		}
		return bs;
	}
	
	public BaseResult getFavoriteList(String cusidn) {
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			List<Map> list = nb3sysopdao.findMyOpData(cusidn);
			bs.addData("favoriteList", list);
			bs.setResult(true);
		} catch (Exception e) {
			log.error("lock_reset_result: {}", e);
			bs.setResult(false);
		}
		return bs;
	}
	
	
	
	
	
	
	

	
	
	
	
	
public  BaseResult getCertificate_url(String cusidn ,String account ,String ip ,String name ) {
		
		log.trace("getCertificate_url");
		BaseResult bs = null;
		
		String authCode = "" ,url="";
		String ts = "" ,soureText="";
		
		Map<String,String> data= null;
		Certificate_Bean bean = null;
		ExtPrincipal extPrincipal = null;
		Ext ext = null;
		Unit unit = null;
		
		try {
			bs = new BaseResult(); 
			bean = new Certificate_Bean();
			extPrincipal =  bean.new ExtPrincipal();
			ext = bean.new Ext();
			unit = bean.new Unit();
			
			ext.setIp(ip);
			unit.setName(cusidn);
			extPrincipal.setAccount(account);
			extPrincipal.setName(name);
			extPrincipal.setExt(ext);
			extPrincipal.setUnit(unit);
			bean.setExtPrincipal(extPrincipal);
			ts = String.valueOf(new DateTime().plusSeconds(5).getMillis()) ;
//			bean.setTs("1594285754186");
			bean.setTs(ts);
			log.trace("bean>>{}",ESAPIUtil.toJson(CodeUtil.toJson(bean)));
//			soureText = URLEncoder.encode(CodeUtil.toJson(bean), CodeUtil.ENCODING);
			soureText = Base64.getUrlEncoder().encodeToString(CodeUtil.toJson(bean).getBytes(CodeUtil.ENCODING)).replaceAll("=", "");
//			            Base64.getUrlEncoder().encodeToString(CodeUtil.toJson(bean).getBytes(CodeUtil.ENCODING));
			log.trace("soureText>>{}",soureText);
			
			authCode = getAuthCode(soureText);
//			url = url+ URLEncoder.encode(authCode, CodeUtil.ENCODING) ;
			url = certificate_url+ authCode;
			log.debug("url>>{}",url);
			bs.setResult(Boolean.TRUE);
			bs.setData(url);
			
		} catch (Exception e) {
			log.error("{}", e);
			bs.setMsgCode("-1");
			bs.setMessage("getCertificate_url error");
			bs.setData(url);
		}
		return bs;
	}
	
	public String getAuthCode(String  soureText) throws Exception {

		String authCode = "";
		try {
//			log.debug("hex>>{}",Hex.encodeHexString(CodeUtil.hmacSHA1Encrypt("hello world", "mykey".getBytes(CodeUtil.ENCODING)) ) );
			authCode = soureText +"."+ new String(Base64.getUrlEncoder().encode(CodeUtil.hmacSHA1Encrypt(soureText, Base64.getDecoder().decode (certificate_key.getBytes()) ) ),CodeUtil.ENCODING).replaceAll("=", ""); ;
//			                           new String(Base64.getUrlEncoder().encode(CodeUtil.hmacSHA1Encrypt(soureText, Base64.getDecoder().decode (certificate_key.getBytes()) ) ),CodeUtil.ENCODING).replaceAll("=", "") ;
			//			authCode = base64url +"."+ new String(Base64.getUrlEncoder().encode(CodeUtil.hmacSHA1Encrypt("hello world", "mykey".getBytes(CodeUtil.ENCODING)) ),CodeUtil.ENCODING) ;
//			authCode = base64url +"."+ Base64.getUrlEncoder().encodeToString(CodeUtil.hmacSHA1Encrypt(base64url, certificate_key.getBytes(CodeUtil.ENCODING)) ) ;
//			authCode = base64url +"."+ Base64.getUrlEncoder().encodeToString(CodeUtil.hmacSHA1Encrypt(base64url, Base64.getDecoder().decode( certificate_key)) ) ;
//			authCode = base64url +"."+ Base64.getUrlEncoder().encodeToString(CodeUtil.hmacSHA1Encrypt(base64url, Base64.getDecoder().decode( certificate_key.getBytes(CodeUtil.ENCODING))) ) ;
//			authCode = base64url +"."+ Base64.getUrlEncoder().encodeToString(CodeUtil.hmacSHA1Encrypt(base64url, key.getBytes(CodeUtil.ENCODING)) ) ;
//			authCode = new String (Base64.getEncoder().encode(CodeUtil.hmacSHA1Encrypt(json, Base64.getDecoder().decode(key.getBytes()) )) , CodeUtil.ENCODING);
		} catch (UnsupportedEncodingException e) {
			log.error("UnsupportedEncodingException>>{}",e);
			throw e;
		} catch (Exception e) {
			log.error("Exception>>{}",e);
			throw e;
		}
		
		return authCode;
		
	}
	
	
	
	
}
