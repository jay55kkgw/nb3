package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class NB31_REST_RQ extends BaseRestBean_OLA implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2755638231100211231L;
	private String CUSIDN;
	private String ACN;
	private String TYPE;
	private String UID;
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getTYPE() {
		return TYPE;
	}
	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	
}
