package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * FundEmail，N930電文RS
 */
public class FundEmail_REST_RS extends BaseRestBean implements Serializable{
	private static final long serialVersionUID = 6791228050721094802L;
	
	private String CUSIDN;
	private String E_MAIL_ADR;
	private String E_BILL;
	private String __MAILERR;
	private String DPMYEMAIL;
	
	public String getCUSIDN(){
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN){
		CUSIDN = cUSIDN;
	}
	public String getE_MAIL_ADR(){
		return E_MAIL_ADR;
	}
	public void setE_MAIL_ADR(String e_MAIL_ADR){
		E_MAIL_ADR = e_MAIL_ADR;
	}
	public String getE_BILL(){
		return E_BILL;
	}
	public void setE_BILL(String e_BILL){
		E_BILL = e_BILL;
	}
	public String get__MAILERR(){
		return __MAILERR;
	}
	public void set__MAILERR(String __MAILERR){
		this.__MAILERR = __MAILERR;
	}
	public String getDPMYEMAIL(){
		return DPMYEMAIL;
	}
	public void setDPMYEMAIL(String dPMYEMAIL){
		DPMYEMAIL = dPMYEMAIL;
	}
}