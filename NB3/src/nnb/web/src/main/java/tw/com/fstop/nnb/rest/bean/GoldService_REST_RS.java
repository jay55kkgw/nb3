package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.List;

/**
 * GoldService電文RQ
 */
public class GoldService_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4149590365191933890L;

//	public String msg_cod;
//
//	public String msg_desc;
//	public String msgCode;
	
	public String message;

	public String date;

	public String time;

	public String bank_no;

	public List<GoldSellProduct> goldSellProduct;
	
	
	

//	@Override
//	public String toString() {
//		return "GoldService_REST_RS [msg_cod=" + msg_cod + ", msg_desc=" + msg_desc + ", date=" + date + ", time="
//				+ time + ", bank_no=" + bank_no + ", goldSellProduct=" + goldSellProduct + "]";
//	}

//	public String getMsg_cod() {
//		return msg_cod;
//	}
//
//	public void setMsg_cod(String msg_cod) {
//		this.msg_cod = msg_cod;
//	}
//
//	public String getMsg_desc() {
//		return msg_desc;
//	}
//
//	public void setMsg_desc(String msg_desc) {
//		this.msg_desc = msg_desc;
//	}

	
	
	@Override
	public String toString() {
		return "GoldService_REST_RS [msgCode=" + super.getMsgCode() + ", message=" + message + ", date=" + date + ", time=" + time
				+ ", bank_no=" + bank_no + ", goldSellProduct=" + goldSellProduct + "]";
	}

	public String getDate() {
		return date;
	}

//	public String getMsgCode() {
//		return msgCode;
//	}
//
//	public void setMsgCode(String msgCode) {
//		this.msgCode = msgCode;
//	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getBank_no() {
		return bank_no;
	}

	public void setBank_no(String bank_no) {
		this.bank_no = bank_no;
	}

	public List<GoldSellProduct> getGoldSellProduct() {
		return goldSellProduct;
	}

	public void setGoldSellProduct(List<GoldSellProduct> goldSellProduct) {
		this.goldSellProduct = goldSellProduct;
	}

}