package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N8302_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7300440938139837802L;
	
	private String TSFACN;//轉帳帳號
	private String TYPE;//查詢類別
	private String UNTNUM;//投保單位代號
	private String CUSIDNUN;//統一編號
	private String UNTTEL;//投保單位電話號碼
	private String ITMNUM;//項目代號1:申請2:註銷3:變更
	private String DATE;//日期YYYMMDD
	private String TIME;//時間HHMMSS
	private String CMQTIME;
	
	public String getTSFACN() {
		return TSFACN;
	}
	public void setTSFACN(String tSFACN) {
		TSFACN = tSFACN;
	}
	public String getTYPE() {
		return TYPE;
	}
	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}
	public String getUNTNUM() {
		return UNTNUM;
	}
	public void setUNTNUM(String uNTNUM) {
		UNTNUM = uNTNUM;
	}
	public String getCUSIDNUN() {
		return CUSIDNUN;
	}
	public void setCUSIDNUN(String cUSIDNUN) {
		CUSIDNUN = cUSIDNUN;
	}
	public String getUNTTEL() {
		return UNTTEL;
	}
	public void setUNTTEL(String uNTTEL) {
		UNTTEL = uNTTEL;
	}
	public String getITMNUM() {
		return ITMNUM;
	}
	public void setITMNUM(String iTMNUM) {
		ITMNUM = iTMNUM;
	}
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
}
