
/*
 * Copyright (c) 2017, FSTOP, Inc. All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tw.com.fstop.spring.helper;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * Helper for get spring application context.
 * 
 *
 * @since 1.0.1
 */
public class ApplicationContextProvider implements ApplicationContextAware
{

    private static ApplicationContext context;

    /**
     * Get spring application context.
     * 
     * @return spring application context
     */
    public static ApplicationContext getApplicationContext()
    {
        return context;
    }

    @Override
    public void setApplicationContext(ApplicationContext ac) throws BeansException
    {
        context = ac;
    }
}
