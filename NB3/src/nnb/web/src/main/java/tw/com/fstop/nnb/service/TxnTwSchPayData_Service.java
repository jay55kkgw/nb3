package tw.com.fstop.nnb.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.tbb.nnb.dao.TxnTwSchPayDataDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;
import fstop.orm.po.TXNTWSCHPAYDATA;
import fstop.orm.po.TXNTWSCHPAYDATA_PK;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class TxnTwSchPayData_Service extends Base_Service {
	private Logger log = LoggerFactory.getLogger(this.getClass().getName());
	
	@Autowired
	private TxnTwSchPayDataDao txnTwSchPayDataDao;
	
	/*
	 * 以顧客統編、查詢起始日期、結束日期、預約結果當作條件查詢預約結果
	 * TxnTwSchPayDataDao內的getByUid方法需要的參數KEY與request不同，故另外處理
	 *
	 */
	
	public BaseResult query(Map<String,String> reqParam) {
		BaseResult bs = new BaseResult();
		//簡易日期格式檢查
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		sdf.setLenient(false);
		try {
			String cusidn = reqParam.get("cusidn");
			String sdate = reqParam.get("sdate");
			String edate = reqParam.get("edate");
			log.trace(ESAPIUtil.vaildLog("統編cusidn = " + cusidn));
			log.trace(ESAPIUtil.vaildLog("起始日sdate = " + sdate));
			log.trace(ESAPIUtil.vaildLog("終止日edate = " + edate));
			DateTime dt;
			Date d;
			
			//判斷時間空值的狀況 0:全部有值;1:無起始日期;10:無結束日期;11:完全無日期;自動帶入前後六個月
			int dtCon = 0;
			if(sdate==null ||sdate.trim().length() == 0) {
				dtCon +=1;
			}
			if(edate==null ||edate.trim().length() == 0) {
				dtCon +=10;
			}
			switch(dtCon) {
			case 1:
				dt = new DateTime(sdf.parse(reqParam.get("edate").replace("/", "").trim()));
				d = new Date(dt.minusMonths(6).getMillis());
				
				reqParam.put("CMSDATE",sdf.format(d));
				reqParam.put("CMEDATE",sdf.format(dt.getMillis()));
//				log.info("sdate="+reqParam.get("sdate"));
				break;
			case 10:
				dt = new DateTime(sdf.parse(reqParam.get("sdate").replace("/", "").trim()));
				d = new Date(dt.plusMonths(6).getMillis());
				reqParam.put("CMEDATE",sdf.format(d));
				reqParam.put("CMSDATE",sdf.format(dt.getMillis()));
//				log.info("edate="+reqParam.get("edate"));
				break;
			case 11:
				dt = new DateTime();
				Date sd = new Date(dt.minusMonths(6).getMillis());
				Date ed = new Date(dt.getMillis());
				reqParam.put("CMSDATE",sdf.format(sd));
				reqParam.put("CMEDATE",sdf.format(ed));
//				log.info("sdate="+reqParam.get("sdate"));
//				log.info("edate="+reqParam.get("edate"));
				break;
			default:
				d = sdf.parse(sdate.replace("/", ""));
				d = sdf.parse(edate.replace("/", ""));
				reqParam.put("CMSDATE",sdate.trim());
				reqParam.put("CMEDATE",edate.trim());
				break;
		}
		//檢查統編時間是否為空值、時間邏輯是否正確
		if(sdf.parse(reqParam.get("CMSDATE")).getTime()>sdf.parse(reqParam.get("CMEDATE")).getTime()) {
			
			bs.setMessage("1", "日期錯誤，起訖日大於終止日");
			bs.setResult(Boolean.FALSE);
			return bs;
		}
		if(cusidn == null || cusidn.trim().length() == 0) {
			bs.setMessage("1", "統編未輸入");
			bs.setResult(Boolean.FALSE);
			return bs;
		}
		
		List<TXNTWSCHPAYDATA> list = new ArrayList<TXNTWSCHPAYDATA>();
		//檢查狀態是否為空值
		if(reqParam.get("dptxstatus").trim() == null ||reqParam.get("dptxstatus").trim().length() == 0) {
			list = txnTwSchPayDataDao.getByUid(reqParam,cusidn);
		}else {
			reqParam.put("FGTXSTATUS", reqParam.get("dptxstatus").trim());
			list = txnTwSchPayDataDao.getByUid(cusidn, reqParam);
		}
		//將複合主鍵拆成一般欄位
		List<LinkedHashMap> nameList = new ArrayList<LinkedHashMap>();
		for(TXNTWSCHPAYDATA po:list) {
			LinkedHashMap<String,TXNTWSCHPAYDATA> map = new LinkedHashMap<String,TXNTWSCHPAYDATA>();
			ObjectMapper p= new ObjectMapper();
			Map map2 = p.convertValue(po, LinkedHashMap.class);
			map2.remove("pks");
			map2.put("dpschtxdate", po.getPks().getDPSCHTXDATE());
			map2.put("dpschno",po.getPks().getDPSCHNO());
			nameList.add((LinkedHashMap) map2);
		}
		 
		 
		log.info("list.size >> {}", list.size());
		bs.setData(nameList);
		bs.setResult(Boolean.TRUE);
		bs.setMessage("0","查詢成功");	
			
		}catch(Exception e) {
			log.error("{}",e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", "查詢異常");
		}
		return bs;	
	}
	
}
