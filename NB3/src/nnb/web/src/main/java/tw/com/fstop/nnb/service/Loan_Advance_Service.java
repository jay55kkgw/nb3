package tw.com.fstop.nnb.service;

import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tw.com.fstop.tbb.nnb.dao.TxnUserDao;
import fstop.orm.po.ADMHOLIDAY;
import fstop.orm.po.TXNUSER;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.tbb.nnb.dao.AdmHolidayDao;
import tw.com.fstop.tbb.nnb.util.DateUtil;
import tw.com.fstop.util.NumericUtil;

@Service
public class Loan_Advance_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private AdmHolidayDao admholidayDao;

	@Autowired
	private TxnUserDao txnUserDao;
	
	public String getNextHoliday() {
		boolean nextHolidayFlag = true;
		String date = DateUtil.getDate("");
		int nDay = 1;
		while (nextHolidayFlag){
			date = DateUtil.getNext_N_Date("yyyyMMdd", nDay);
			nextHolidayFlag = isHOLIDAY(date);
			log.debug("date >> {}",date);
			log.debug("nDay >> {}",nDay);
			log.debug("nextHolidayFlag >> {}",nextHolidayFlag);
			nDay++;
		}
		return DateUtil.convertDate(2, date, "yyyyMMdd", "yyyy/MM/dd") ;
	}
	public boolean isHOLIDAY(String date) {
		try {
			ADMHOLIDAY holiday = null;
			try {
				holiday = (ADMHOLIDAY)admholidayDao.getByPK(date);
			}
			catch (Exception e) {
				holiday = null;					
			}
						
            //若為假日  
			if (holiday.getADHOLIDAYID() != null) {
				return true;				
			}
			//若非假日			
			else {
				return false;				
			}
		}	
		catch(Exception e) {	
			log.error("B105.isHOLIDAY() error : "+ e.getMessage());
		}
		
		return false;
	}
	
	public boolean isLoanBizTime() {

		Date d = new Date();
		String str_CurrentDate = DateUtil.getDate("");		
		String str_CurrentTime = DateUtil.getTheTime("");
	
		try {			
			
			String str_BizStart ="090000";
			String str_BizEnd = "153000";			
			
			ADMHOLIDAY holiday = null;
			try {
				holiday = (ADMHOLIDAY) admholidayDao.getByPK(str_CurrentDate);
			}
			catch (Exception e) {
				holiday = null;					
			}
			
			
			//若非假日 && 系統時間 in 營業時間內
			if ((holiday.getADHOLIDAYID() == null) && (str_CurrentTime.compareTo(str_BizStart) > 0) && (str_CurrentTime.compareTo(str_BizEnd) < 0)) {				
				return true;
			}	
			//若為假日 or 系統時間 not in 營業時間內			
			else {
				return false;				
			}
		}	
		catch(Exception e) {	
			log.error("B105.isLoanBizTime() error : "+ e.getMessage());
		}
		
		return false;
	}			
	
	public boolean chkContains(String chkNo,String DBStr)
	{
		int count = 0;
		int chNo  = 0;
		String str = "";
		while(true)
		{
			chNo = DBStr.indexOf(',',count);
			if(chNo == -1)
			{
				str = DBStr.substring(count,DBStr.length());
				if(str.equals(chkNo)) return true;
				else
         			 break;
			}
			str = DBStr.substring(count,chNo);
			if(str.equals(chkNo)) return true;
			count = chNo+1;
		}
		return false;
	}
	

	public String getHelper(String uid)
	{
		Map<String, String> queryData = new HashMap<String, String>();
		TXNUSER txnUser = txnUserDao.get( TXNUSER.class, uid );
		return txnUser.getDPNOTIFY();
	}
	
	public BaseResult AddressBook(String uid) {
		BaseResult bs = null;
		try {
			Map<String, String> param = new HashMap<String, String>();
			param.put("TABLE", "TxnAddressBook");
			param.put("DPUSERID", uid);
			param.put("EXECUTEFUNCTION", "Query");
			bs = N930EA_REST(param);
		}catch(Exception e) {
			log.error(e.toString());
		}
		return bs;
	}
	public BaseResult repay_advance_result(Map<String, String> request,String cusidn, String adopid) {
		BaseResult bs = null;
		try {
			bs = N3003_REST(request, cusidn, adopid);
			if(bs != null && bs.getResult()) {
				Map<String,Object> callrow = (Map<String,Object>)bs.getData();
				bs.addData("PALPAY", NumericUtil.fmtAmount((String)callrow.get("PALPAY"), 0));
			}
		}catch(Exception e) {
			log.error(e.toString());
		}
		return bs;
		
	}
}
