package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class B012_REST_RSDATA extends BaseRestBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8982042483204067453L;

	String O01;	//委託單號
	String O02;	//商品代號
	String O03;	//商品名稱
	String O04;	//庫存單位數
	String O05;	//庫存面額
	String O06;	//買入價格
	String O07;	//信託本金
	String O08;	//計價幣別
	String O09;	//參考價格
	String O10;	//報價日期
	String O11;	//累計配息金額
	String O12;	//市值
	String O13;	//(不含息)報酬率
	String O14;	//(含息)報酬率
	String O15;	//申購日期
	String O16;	//前手息
	String O17;	//(含息)參考損益
	String O18;	//債券幣別
	String O19;	//委賣處理狀態
	String O20;	//(不含息)參考損益
	
	public String getO16() {
		return O16;
	}
	public void setO16(String o16) {
		O16 = o16;
	}
	public String getO17() {
		return O17;
	}
	public void setO17(String o17) {
		O17 = o17;
	}
	public String getO18() {
		return O18;
	}
	public void setO18(String o18) {
		O18 = o18;
	}
	public String getO01() {
		return O01;
	}
	public void setO01(String o01) {
		O01 = o01;
	}
	public String getO02() {
		return O02;
	}
	public void setO02(String o02) {
		O02 = o02;
	}
	public String getO03() {
		return O03;
	}
	public void setO03(String o03) {
		O03 = o03;
	}
	public String getO04() {
		return O04;
	}
	public void setO04(String o04) {
		O04 = o04;
	}
	public String getO05() {
		return O05;
	}
	public void setO05(String o05) {
		O05 = o05;
	}
	public String getO06() {
		return O06;
	}
	public void setO06(String o06) {
		O06 = o06;
	}
	public String getO07() {
		return O07;
	}
	public void setO07(String o07) {
		O07 = o07;
	}
	public String getO08() {
		return O08;
	}
	public void setO08(String o08) {
		O08 = o08;
	}
	public String getO09() {
		return O09;
	}
	public void setO09(String o09) {
		O09 = o09;
	}
	public String getO10() {
		return O10;
	}
	public void setO10(String o10) {
		O10 = o10;
	}
	public String getO11() {
		return O11;
	}
	public void setO11(String o11) {
		O11 = o11;
	}
	public String getO12() {
		return O12;
	}
	public void setO12(String o12) {
		O12 = o12;
	}
	public String getO13() {
		return O13;
	}
	public void setO13(String o13) {
		O13 = o13;
	}
	public String getO14() {
		return O14;
	}
	public void setO14(String o14) {
		O14 = o14;
	}
	public String getO15() {
		return O15;
	}
	public void setO15(String o15) {
		O15 = o15;
	}
	public String getO19() {
		return O19;
	}
	public void setO19(String o19) {
		O19 = o19;
	}
	public String getO20() {
		return O20;
	}
	public void setO20(String o20) {
		O20 = o20;
	}
}
