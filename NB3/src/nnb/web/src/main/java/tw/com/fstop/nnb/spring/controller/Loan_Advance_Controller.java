package tw.com.fstop.nnb.spring.controller;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.aop.Dialog;
import tw.com.fstop.nnb.service.Loan_Advance_Service;
import tw.com.fstop.tbb.nnb.util.DateUtil;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.WebUtil;

@SessionAttributes({ 	SessionUtil.CUSIDN,SessionUtil.BACK_DATA,SessionUtil.CONFIRM_LOCALE_DATA, 
	SessionUtil.RESULT_LOCALE_DATA,SessionUtil.STEP1_LOCALE_DATA,SessionUtil.STEP2_LOCALE_DATA,
	SessionUtil.STEP3_LOCALE_DATA ,SessionUtil.TRANSFER_RESULT_TOKEN,SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, SessionUtil.DPMYEMAIL })
@Controller
@RequestMapping(value = "/LOAN/ADVANCE")
public class Loan_Advance_Controller {
	
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private Loan_Advance_Service loan_advance_service;
	
	//貸款提前償還本金(3003)
	@RequestMapping(value = "/repay_advance_p1")
	public String repay_advance_p1(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model, HttpSession session) {
		try {
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			String back = reqParam.get("back") == null ? "" : reqParam.get("back");
			log.debug(ESAPIUtil.vaildLog("back >> {}"+ back));
			if (hasLocale || back.equals("Y"))
			{
				reqParam = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null);
			}
			else
			{
				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, reqParam);
			}
		}catch(Exception e) {
			log.debug(e.toString());
		}finally {
			model.addAttribute("inputData", reqParam);
		}
		return "/loan_advance/repay_advance_p1";
	}
	
	//貸款提前償還本金(3003)
	@RequestMapping(value = "/repay_advance_p2")
	public String repay_advance_p2(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model, HttpSession session) {
		BaseResult bsN920 = null;
		String target = "/error";
		try {
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),"utf-8");
			bsN920 = new BaseResult();
			bsN920 = loan_advance_service.N920_REST(cusidn, loan_advance_service.OUT_ACNO);
			
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			String back = reqParam.get("back") == null ? "" : reqParam.get("back");
			log.debug(ESAPIUtil.vaildLog("back >> {}"+ back));
			if (hasLocale || back.equals("Y"))
			{
				reqParam = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.STEP2_LOCALE_DATA, null);
			}
			else
			{
				reqParam.put("nextDay", loan_advance_service.getNextHoliday());
				reqParam.put("disabledFlag", loan_advance_service.isLoanBizTime() ? "Y" : "N");
				reqParam.put("nowDay", DateUtil.getDate("/"));
				reqParam.put("DPMYEMAIL", (String) SessionUtil.getAttribute(model, SessionUtil.DPMYEMAIL, null));
				reqParam.put("sendMe", loan_advance_service.chkContains("1", loan_advance_service.getHelper(cusidn)) ? "Y" : "N");
				SessionUtil.addAttribute(model, SessionUtil.STEP2_LOCALE_DATA, reqParam);
			}
		}catch(Exception e) {
			log.debug(e.toString());
		}finally {
			if(bsN920!=null && bsN920.getResult()) {
				//20200903新增 帳號過濾4、5碼為 01 時，不要顯示
				Map<String, Object> bsN920Data = (Map) bsN920.getData();
				ArrayList<Map<String, String>> rows = (ArrayList<Map<String, String>>) bsN920Data.get("REC");
				for (int i = 0;i < rows.size();i++) {
					if(rows.get(i).get("ACN").equals("") || rows.get(i).get("ACN").substring(3, 5).equals("01")) {
						rows.remove(i);
						i--;
					}
				}
				
				model.addAttribute("inputData", reqParam);
				model.addAttribute("n920_data", bsN920);
				target = "/loan_advance/repay_advance_p2";
			}else {
				bsN920.setPrevious("/LOAN/QUERY/loan_detail");
				model.addAttribute(BaseResult.ERROR, bsN920);
			}
		}
		return target;
	}
	
	//貸款提前償還本金(3003)
	@RequestMapping(value = "/repay_advance_p3")
	public String repay_advance_p3(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model, HttpSession session) {
		BaseResult bs = null;
		String target = "/error";
		try {
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			String back = reqParam.get("back") == null ? "" : reqParam.get("back");
			log.debug(ESAPIUtil.vaildLog("back >> {}"+ back));
			if (hasLocale || back.equals("Y"))
			{
				reqParam = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null);
			}
			else
			{
				bs = new BaseResult();
				// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
				bs = loan_advance_service.getTxToken();
				log.trace("electricity_fee.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
				if(bs.getResult()) {
					SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
				}
				reqParam.put("TOKEN", ((Map<String,String>) bs.getData()).get("TXTOKEN"));
				reqParam.put("PALPAY_str",  NumericUtil.fmtAmount(reqParam.get("PALPAY"),0));
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, reqParam);
			}
		}catch(Exception e) {
			log.debug(e.toString());
		}finally {
			model.addAttribute("inputData", reqParam);
			target = "/loan_advance/repay_advance_p3";
		}
		return target;
	}
	
	//貸款提前償還本金(3003)
	@Dialog(ADOPID = "N3003")
	@RequestMapping(value = "/repay_advance_result")
	public String repay_advance_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model, HttpSession session) {
		BaseResult bs = null;
		String target = "/error";
		Map<String, String> okMap = null;
		try {
			bs = new BaseResult();

            okMap = ESAPIUtil.validStrMap(reqParam);
			//okMap = reqParam;
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				okMap = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null);
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				log.trace("apply_deposit_account_result.validate TXTOKEN...");
				log.trace("apply_deposit_account_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("other_telecom_fee_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
						okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("other_telecom_fee_result.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("other_telecom_fee_result TXTOKEN 不正確，TXTOKEN>>{}"+ okMap.get("TOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
						!okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("other_telecom_fee_result.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("other_telecom_fee_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TOKEN")));
					throw new Exception();
				}
				
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),"utf-8");
				bs = loan_advance_service.repay_advance_result(reqParam, cusidn, "N3003");
				okMap.put("PALPAY", NumericUtil.fmtAmount(okMap.get("PALPAY"), 0));
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, okMap);
			}
		}catch(Exception e) {
			log.debug(e.toString());
		}finally {
			if(bs != null && bs.getResult()) {
				target = "/loan_advance/repay_advance_result";
				model.addAttribute("result_data", bs);
				model.addAttribute("inputData", okMap);
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
				bs.setPrevious("/LOAN/QUERY/loan_detail");
			}
		}
		return target;
	}
	
	//貸款提前償還本金(3003)
	@RequestMapping(value = "/AddressBook")
	public String address_book(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model, HttpSession session) {
		BaseResult bs = null;
		try {
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),"utf-8");
			bs = loan_advance_service.AddressBook(cusidn);
			model.addAttribute("result_data", bs);
		}catch(Exception e) {
			log.debug(e.toString());
		}
		return "/loan_advance/AddressBook";
	}
	
	
	//貸款提前結清(30031)
	@RequestMapping(value = "/settlement_advance_p1")
	public String settlement_advance_p1(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model, HttpSession session) {
		try {
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			String back = reqParam.get("back") == null ? "" : reqParam.get("back");
			log.debug(ESAPIUtil.vaildLog("back >> {}"+ back));
			if (hasLocale || back.equals("Y"))
			{
				reqParam = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null);
			}
			else
			{
				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, reqParam);
			}
		}catch(Exception e) {
			log.debug(e.toString());
		}finally {
			model.addAttribute("inputData", reqParam);
		}
		return "/loan_advance/settlement_advance_p1";
	}
	
	//貸款提前結清(30031)
	@RequestMapping(value = "/settlement_advance_p2")
	public String settlement_advance_p2(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model, HttpSession session) {
		BaseResult bsN920 = null;
		String target = "/error";
		try {
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),"utf-8");
			bsN920 = new BaseResult();
			bsN920 = loan_advance_service.N920_REST(cusidn, loan_advance_service.OUT_ACNO);
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			String back = reqParam.get("back") == null ? "" : reqParam.get("back");
			log.debug(ESAPIUtil.vaildLog("back >> {}"+ back));
			if (hasLocale || back.equals("Y"))
			{
				reqParam = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.STEP2_LOCALE_DATA, null);
			}
			else
			{
				reqParam.put("nextDay", loan_advance_service.getNextHoliday());
				reqParam.put("disabledFlag", loan_advance_service.isLoanBizTime() ? "Y" : "N");
				reqParam.put("nowDay", DateUtil.getDate("/"));
				reqParam.put("DPMYEMAIL", (String) SessionUtil.getAttribute(model, SessionUtil.DPMYEMAIL, null));
				reqParam.put("sendMe", loan_advance_service.chkContains("1", loan_advance_service.getHelper(cusidn)) ? "Y" : "N");
				SessionUtil.addAttribute(model, SessionUtil.STEP2_LOCALE_DATA, reqParam);
			}
		}catch(Exception e) {
			log.debug(e.toString());
		}finally {
			if(bsN920!=null && bsN920.getResult()) {
				model.addAttribute("inputData", reqParam);
				model.addAttribute("n920_data", bsN920);
				target = "/loan_advance/settlement_advance_p2";
			}else {
				bsN920.setPrevious("/LOAN/QUERY/loan_detail");
				model.addAttribute(BaseResult.ERROR, bsN920);
			}
		}
		return target;
	}
	
	//貸款提前結清(30031)
	@RequestMapping(value = "/settlement_advance_p3")
	public String settlement_advance_p3(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model, HttpSession session) {
		BaseResult bs = null;
		String target = "/error";
		try {
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			String back = reqParam.get("back") == null ? "" : reqParam.get("back");
			log.debug(ESAPIUtil.vaildLog("back >> {}"+ back));
			if (hasLocale || back.equals("Y"))
			{
				reqParam = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null);
			}
			else
			{
				bs = new BaseResult();
				// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
				bs = loan_advance_service.getTxToken();
				log.trace("electricity_fee.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
				if(bs.getResult()) {
					SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
				}
				reqParam.put("TOKEN", ((Map<String,String>) bs.getData()).get("TXTOKEN"));
				reqParam.put("PALPAY_str",  NumericUtil.fmtAmount(reqParam.get("PALPAY"),0));
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, reqParam);
			}
		}catch(Exception e) {
			log.debug(e.toString());
		}finally {
			model.addAttribute("inputData", reqParam);
			target = "/loan_advance/settlement_advance_p3";
		}
		return target;
	}
	
	//貸款提前結清(30031)
	@RequestMapping(value = "/settlement_advance_result")
	public String settlement_advance_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model, HttpSession session) {
		BaseResult bs = null;
		String target = "/error";
		Map<String, String> okMap = null;
		try {
			bs = new BaseResult();

            okMap = ESAPIUtil.validStrMap(reqParam);
			//okMap = reqParam;
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				okMap = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null);
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				log.trace("apply_deposit_account_result.validate TXTOKEN...");
				log.trace("apply_deposit_account_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("other_telecom_fee_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
						okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("other_telecom_fee_result.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("other_telecom_fee_result TXTOKEN 不正確，TXTOKEN>>{}"+ okMap.get("TOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
						!okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("other_telecom_fee_result.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("other_telecom_fee_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TOKEN")));
					throw new Exception();
				}
				
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),"utf-8");
				bs = loan_advance_service.repay_advance_result(reqParam, cusidn, "N30031");
				okMap.put("PALPAY", NumericUtil.fmtAmount(okMap.get("PALPAY"), 0));
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, okMap);
			}
		}catch(Exception e) {
			log.debug(e.toString());
		}finally {
			if(bs != null && bs.getResult()) {
				target = "/loan_advance/settlement_advance_result";
				model.addAttribute("result_data", bs);
				model.addAttribute("inputData", okMap);
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
				bs.setPrevious("/LOAN/ADVANCE/settlement_advance_p1");
			}
		}
		return target;
	}
}
