package tw.com.fstop.nnb.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import fstop.orm.po.ADMBANK;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.AdmBankDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.StrUtil;

/**
 * 
 * 功能說明 : 繳納信用卡款
 *
 */
@Service
public class Creditcard_Pay_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	AdmBankDao admBankDao;
	
	@Autowired
	private DaoService daoservice;

	@Autowired
	private I18n i18n;


	/**
	 * 取得信用卡之轉出帳號
	 * 
	 * @param cusidn
	 * @param type
	 * @return
	 */
	public BaseResult getOutAcnoList(String cusidn, String type) {
		log.trace("acno>>{} ", cusidn);
		log.trace("cusidn>>{} ", cusidn);
		BaseResult bs = null;
		Map<String, Object> tmpData = null;
		List<String> acnoList = new ArrayList<String>();
		List<Map<String, String>> acnoList2 = null;
		try {
			bs = new BaseResult();
			bs = N920_REST(cusidn, type);

			if (bs != null && bs.getResult()) {
				tmpData = (Map) bs.getData();
				acnoList2 = (List<Map<String, String>>) tmpData.get("REC");

				for (Map<String, String> acMap : acnoList2) {
					String ACN = acMap.get("ACN");
					acnoList.add(ACN);
				}

				bs.setData(acnoList);

			}

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getOutAcnoList error >> {}",e);
		}
		return bs;

	}

	/**
	 * 取得信用卡之轉入帳號
	 * 
	 * @param cusidn
	 * @param type
	 * @return
	 */
	public BaseResult getInAcnoList(String cusidn, String type) {
		log.trace("acno>>{} ", cusidn);
		BaseResult bs = null;
		LinkedHashMap<String, String> labelMap = null;
		LinkedHashMap<String, String> notAgreeLabelMap = null;
		ObjectMapper objectMapper = new ObjectMapper();
		String tmpStr = "";
		String agree = "";
		try {
			// CALL WS api
			bs = new BaseResult();
			bs = daoservice.getAgreeAcnoListSep(cusidn, type,false);
			// 後製把table 裡面的資料轉成下拉選單所要的格式
			if (bs.getResult()) {
				labelMap = new LinkedHashMap<String, String>();
				notAgreeLabelMap = new LinkedHashMap<String, String>();

				// 12/06目前使用者只有SSL驗證，只能開放約定帳號，等有晶片卡、iKEY驗證後，再開放 常用非約定帳號
				// 下拉選單-請選擇約定/常用非約定帳號，暫時改為請選擇帳號
				// labelMap.put("#0", "-----" +
				// i18n.getMsg("LB.Select_designated_or_common_non-designated_account") +
				// "-----");
				labelMap.put("#0", "--" +  i18n.getMsg("LB.X1824") + "--");
				// 下拉選單-約定帳號
				labelMap.put("#1", "--" + i18n.getMsg("LB.Designated_account") + "--");

				// 下拉選單-常用非約定帳號，暫時改為不顯示
				 notAgreeLabelMap.put("#0", "--" + i18n.getMsg("LB.X1825") + "--");
				 notAgreeLabelMap.put("#1", "--" + i18n.getMsg("LB.Common_non-designated_account") + "--");

				Map<String, Object> data = (Map<String, Object>) bs.getData();
				List<Map<String, String>> agree_list = (List<Map<String, String>>) data.get("REC");
				log.debug("AGREE LIST >> {}" , agree_list);
				
				// 取出轉入帳號
				if(null!=agree_list) {
					for (Map<String, String> tmp : agree_list) {
						log.trace("tmp>> {}", tmp);
						log.trace("VALUE>> {}", tmp.get("VALUE"));
						if (!"#".equals(tmp.get("VALUE"))) {
							JsonNode jsonNode = objectMapper.readTree(tmp.get("VALUE"));
							agree = jsonNode.get("AGREE").asText();
						}
	
						tmpStr = tmp.get("VALUE").toString();
	
						if ("1".equals(agree)) {
							labelMap.put(tmpStr, tmp.get("TEXT"));
						}
					}
				}else {
					
				}
				List<Map<String, String>> notAgree_list = (List<Map<String, String>>) data.get("REC2");
				log.debug("NOT AGREE LIST >> {}" , notAgree_list);
				
				if(null!=notAgree_list) {
					for (Map<String, String> tmp : notAgree_list) {
						log.trace("tmp>> {}", tmp);
						log.trace("VALUE>> {}", tmp.get("VALUE"));
						if (!"#".equals(tmp.get("VALUE"))) {
							JsonNode jsonNode = objectMapper.readTree(tmp.get("VALUE"));
							agree = jsonNode.get("AGREE").asText();
							tmpStr = tmp.get("VALUE").toString();
							
							if ("0".equals(agree)&&"050".equals(jsonNode.get("BNKCOD").asText())) {
								notAgreeLabelMap.put(tmpStr, tmp.get("TEXT"));
							}
							
						}
	
					}
				}else {
					
				}
				// 12/06目前使用者只有SSL驗證，只能開放約定帳號，等有晶片卡、iKEY驗證後，再開放 常用非約定帳號
				// labelMap.putAll(notAgreeLabelMap);
				
				data.put("REC", labelMap);
				data.put("REC2", notAgreeLabelMap);
				log.trace("data >>{}", data);
				bs.setData(data);

			}
			// String json =
			// "{\"_MsgCode\":\"0\",\"_MsgName\":\"\",\"REC\":[{\"_ACN\":\"00160501049\",\"_BNKCOD\":\"050\",\"_AGREE\":\"1\",\"_DPGONAME\":\"\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"AGREE\\\":\\\"1\\\",\\\"ACN\\\":\\\"00160501049\\\",\\\"BNKCOD\\\":\\\"050\\\"}\",\"_TEXT\":\"050-臺灣企銀
			// 00160501049\"},{\"_ACN\":\"00162565656\",\"_BNKCOD\":\"050\",\"_AGREE\":\"1\",\"_DPGONAME\":\"\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"AGREE\\\":\\\"1\\\",\\\"ACN\\\":\\\"00162565656\\\",\\\"BNKCOD\\\":\\\"050\\\"}\",\"_TEXT\":\"050-臺灣企銀
			// 00162565656\"},{\"_ACN\":\"00162778821\",\"_BNKCOD\":\"050\",\"_AGREE\":\"1\",\"_DPGONAME\":\"\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"AGREE\\\":\\\"1\\\",\\\"ACN\\\":\\\"00162778821\\\",\\\"BNKCOD\\\":\\\"050\\\"}\",\"_TEXT\":\"050-臺灣企銀
			// 00162778821\"},{\"_ACN\":\"00162889915\",\"_BNKCOD\":\"050\",\"_AGREE\":\"1\",\"_DPGONAME\":\"\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"AGREE\\\":\\\"1\\\",\\\"ACN\\\":\\\"00162889915\\\",\\\"BNKCOD\\\":\\\"050\\\"}\",\"_TEXT\":\"050-臺灣企銀
			// 00162889915\"},{\"_ACN\":\"01013101163\",\"_BNKCOD\":\"050\",\"_AGREE\":\"1\",\"_DPGONAME\":\"\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"AGREE\\\":\\\"1\\\",\\\"ACN\\\":\\\"01013101163\\\",\\\"BNKCOD\\\":\\\"050\\\"}\",\"_TEXT\":\"050-臺灣企銀
			// 01013101163\"},{\"_ACN\":\"01062010505\",\"_BNKCOD\":\"050\",\"_AGREE\":\"1\",\"_DPGONAME\":\"\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"AGREE\\\":\\\"1\\\",\\\"ACN\\\":\\\"01062010505\\\",\\\"BNKCOD\\\":\\\"050\\\"}\",\"_TEXT\":\"050-臺灣企銀
			// 01062010505\"},{\"_ACN\":\"01062010602\",\"_BNKCOD\":\"050\",\"_AGREE\":\"1\",\"_DPGONAME\":\"\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"AGREE\\\":\\\"1\\\",\\\"ACN\\\":\\\"01062010602\\\",\\\"BNKCOD\\\":\\\"050\\\"}\",\"_TEXT\":\"050-臺灣企銀
			// 01062010602\"},{\"_ACN\":\"01062010807\",\"_BNKCOD\":\"050\",\"_AGREE\":\"1\",\"_DPGONAME\":\"\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"AGREE\\\":\\\"1\\\",\\\"ACN\\\":\\\"01062010807\\\",\\\"BNKCOD\\\":\\\"050\\\"}\",\"_TEXT\":\"050-臺灣企銀
			// 01062010807\"},{\"_ACN\":\"01062577807\",\"_BNKCOD\":\"050\",\"_AGREE\":\"1\",\"_DPGONAME\":\"\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"AGREE\\\":\\\"1\\\",\\\"ACN\\\":\\\"01062577807\\\",\\\"BNKCOD\\\":\\\"050\\\"}\",\"_TEXT\":\"050-臺灣企銀
			// 01062577807\"},{\"_ACN\":\"\",\"_BNKCOD\":\"050\",\"_AGREE\":\"1\",\"_DPGONAME\":\"\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"AGREE\\\":\\\"1\\\",\\\"ACN\\\":\\\"\\\",\\\"BNKCOD\\\":\\\"050\\\"}\",\"_TEXT\":\"050-臺灣企銀\"},{\"_ACN\":\"05064600127\",\"_BNKCOD\":\"050\",\"_AGREE\":\"1\",\"_DPGONAME\":\"\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"AGREE\\\":\\\"1\\\",\\\"ACN\\\":\\\"05064600127\\\",\\\"BNKCOD\\\":\\\"050\\\"}\",\"_TEXT\":\"050-臺灣企銀
			// 05064600127\"},{\"_ACN\":\"76062567895\",\"_BNKCOD\":\"050\",\"_AGREE\":\"1\",\"_DPGONAME\":\"\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"AGREE\\\":\\\"1\\\",\\\"ACN\\\":\\\"76062567895\\\",\\\"BNKCOD\\\":\\\"050\\\"}\",\"_TEXT\":\"050-臺灣企銀
			// 76062567895\"},{\"_ACN\":\"84201234567890\",\"_BNKCOD\":\"005\",\"_AGREE\":\"1\",\"_DPGONAME\":\"\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"AGREE\\\":\\\"1\\\",\\\"ACN\\\":\\\"84201234567890\\\",\\\"BNKCOD\\\":\\\"005\\\"}\",\"_TEXT\":\"005-土地銀行
			// 84201234567890\"},{\"_ACN\":\"99000000000000\",\"_BNKCOD\":\"050\",\"_AGREE\":\"0\",\"_DPGONAME\":\"050-臺灣企銀\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"BNKCOD\\\":\\\"050\\\",\\\"ACN\\\":\\\"99000000000000\\\",\\\"AGREE\\\":\\\"0\\\"}\",\"_TEXT\":\"050-臺灣企銀
			// 99000000000000
			// 050-臺灣企銀\"},{\"_ACN\":\"05052324156\",\"_BNKCOD\":\"050\",\"_AGREE\":\"0\",\"_DPGONAME\":\"test-fx\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"BNKCOD\\\":\\\"050\\\",\\\"ACN\\\":\\\"05052324156\\\",\\\"AGREE\\\":\\\"0\\\"}\",\"_TEXT\":\"050-臺灣企銀
			// 05052324156
			// test-fx\"},{\"_ACN\":\"99123123123111\",\"_BNKCOD\":\"050\",\"_AGREE\":\"0\",\"_DPGONAME\":\"050-臺灣企銀\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"BNKCOD\\\":\\\"050\\\",\\\"ACN\\\":\\\"99123123123111\\\",\\\"AGREE\\\":\\\"0\\\"}\",\"_TEXT\":\"050-臺灣企銀
			// 99123123123111 050-臺灣企銀\"}]}";
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getInAcnoList error >> {}",e);
		}
		return bs;
	}

	/**
	 * 取得轉出帳號
	 * 
	 * @param reqParam
	 * @return
	 */
	public BaseResult payment_page(Map<String, String> reqParam) {
		log.trace(ESAPIUtil.vaildLog("cid>>{} "+ reqParam.get("CUSIDN")));
		BaseResult bs = null;
		Map<String, Object> tmpData = null;
		List<String> acnoList = new ArrayList<String>();
		List<Map<String, String>> acnoList2 = null;
		try {
			// CALL WS api
			bs = new BaseResult();
			//Avoid TXNLOG
			reqParam.put("ADOPID", "__N810");
			bs = N810_REST(reqParam);
			String total_due = NumericUtil.fmtAmount(((Map<String, String>) bs.getData()).get("TOTL_DUE"), 2);
			String curr_bal = NumericUtil.fmtAmount(((Map<String, String>) bs.getData()).get("CURR_BAL"), 2);
			String total_due_send = NumericUtil.fmtAmount(((Map<String, String>) bs.getData()).get("TOTL_DUE"), 0)
					.replaceAll(",", "");
			String curr_bal_send = NumericUtil.fmtAmount(((Map<String, String>) bs.getData()).get("CURR_BAL"), 0)
					.replaceAll(",", "");
			bs.reset();
			bs.setResult(true);
			bs.addData("TOTL_DUE", total_due);
			bs.addData("CURR_BAL", curr_bal);
			bs.addData("TOTL_DUE_SEND", total_due_send);
			bs.addData("CURR_BAL_SEND", curr_bal_send);

			// 設定頁面預約日期為明天，如果當前時間太晚則預約後天
			bs.addData("tmrDate", DateUtil.getTomorrowOrAfterTomorrowDate());
			
			// 設定頁面預約日期最大值
			Calendar cal = Calendar.getInstance();  
			cal.set(Calendar.DATE, cal.get(Calendar.DATE) + 1 );
			cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) + 1 );
			Date nextYearDay = cal.getTime();
						
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
			bs.addData("maxDate", sdf.format(nextYearDay));

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("payment_page error >> {}",e);
		}
		// return retMap;
		return bs;
	}

	/**
	 * 
	 * @param reqParam
	 * @param
	 * @return
	 */
	public BaseResult payment_confirm(Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			// CALL WS api
			bs = new BaseResult();

			if (StrUtil.isNotEmpty(reqParam.get("FGTXDATE")) && (reqParam.get("FGTXDATE").equals("1"))) {
				reqParam.put("transfer_date", DateUtil.getDate("/"));
			}
			// 預約
			if (StrUtil.isNotEmpty(reqParam.get("FGTXDATE")) && (reqParam.get("FGTXDATE").equals("2"))) {
				reqParam.put("transfer_date", reqParam.get("CMTRDATE"));
			}
			bs = getTxToken();
			bs.setResult(true);
			if (bs.getResult()) {
				reqParam.put("FGTXDATE", reqParam.get("FGTXDATE"));
				reqParam.put("TXTOKEN", ((Map<String, String>) bs.getData()).get("TXTOKEN"));
				reqParam.put("AMOUNT_SHOW", NumericUtil.fmtAmount(reqParam.get("AMOUNT"), 2));
				reqParam.put("AMOUNT", reqParam.get("AMOUNT"));
				bs.setData(reqParam);
				bs.setResult(Boolean.TRUE);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("payment_confirm error >> {}",e);
		}
		return bs;
	}

	/**
	 * 
	 * @param reqParam
	 * @param sessionId
	 * @return
	 */
	public BaseResult payment_result(Map<String, String> reqParam) {
		BaseResult bs = null;
		Map<String, String> newMap = null;
		try {
			log.trace(ESAPIUtil.vaildLog("reqParam {}" + CodeUtil.toJson(reqParam)));
			bs = new BaseResult();
			newMap = new HashMap<String, String>();
			payment_Result_Data_Pre_Processing(reqParam);

			// bs = call_N072Two(newMap);
			bs = N072_REST(reqParam);

			if ("1".equals(reqParam.get("FGTXDATE"))) {
				((Map<String, String>) bs.getData()).put("O_AVLBAL",
						NumericUtil.fmtAmount(((Map<String, String>) bs.getData()).get("O_AVLBAL"), 2));
				((Map<String, String>) bs.getData()).put("O_TOTBAL",
						NumericUtil.fmtAmount(((Map<String, String>) bs.getData()).get("O_TOTBAL"), 2));

				((Map<String, String>) bs.getData()).put("AMOUNT_SHOW",
						NumericUtil.fmtAmount(((Map<String, String>) bs.getData()).get("AMOUNT"), 2));
				((Map<String, String>) bs.getData()).put("ACN", ((Map<String, String>) bs.getData()).get("OUTACN"));
				bs.addData("CMTRMEMO", reqParam.get("CMTRMEMO"));
				bs.addData("FGTXDATE", reqParam.get("FGTXDATE"));
			} else if ("2".equals(reqParam.get("FGTXDATE"))) {
				// 移除PINNEW;
				reqParam.remove("PINNEW");
				bs.setData(reqParam);
			}

			bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("payment_result error >> {}",e);
		}
		return bs;
	}

	/**
	 * 轉帳電文前置作業
	 * 
	 * @param reqParam
	 * @param newMap
	 * @param sessionId
	 */
	public void payment_Result_Data_Pre_Processing(Map<String, String> reqParam) {
		Map<String, String> tmp = null;
		try {

			// if (StrUtil.isNotEmpty(reqParam.get("CMTRMEMO"))) {
			// reqParam.put("CMTRMEMO",
			// Base64.getEncoder().encodeToString(reqParam.get("CMTRMEMO").getBytes("UTF-8")));
			// }
			// if (StrUtil.isNotEmpty(reqParam.get("CMMAILMEMO"))) {
			// reqParam.put("CMMAILMEMO",
			// Base64.getEncoder().encodeToString(reqParam.get("CMMAILMEMO").getBytes("UTF-8")));
			// }

//			if ("0".equals(reqParam.get("FLAG"))) {
//				// tmp = CodeUtil.fromJson(reqParam.get("CARDNUM1"), Map.class);
//				reqParam.put("CARDNUM", reqParam.get("CARDNUM2"));
//			}
//			//新增繳納本行信用卡款  20190710
//			if ("2".equals(reqParam.get("FLAG"))) {
//				reqParam.put("CARDNUM", reqParam.get("CARDNUM2"));
//			}
//			if ("1".equals(reqParam.get("FLAG"))) {
//				tmp = CodeUtil.fromJson(reqParam.get("CARDNUM1"), Map.class);
//				log.trace("CARDNUM >>{}", tmp.get("ACN"));
//				reqParam.put("CARDNUM", tmp.get("ACN"));
//			}
			//for TXNLOG
			reqParam.put("ADSVBH", "050");
			reqParam.put("ADCURRENCY", "TWD");
			//for TXNLOG end 
			switch (reqParam.get("FLAG")) {
			case "0": //非約定信用卡帳號  兩種情況 有CARDNUM2 or CARDNUM3 2是普通text,3是下拉式選單值
				if(null!=reqParam.get("CARDNUM2")&&!"".equals(reqParam.get("CARDNUM2"))) {
					reqParam.put("CARDNUM", reqParam.get("CARDNUM2").toUpperCase());
				}else if (null!=reqParam.get("CARDNUM3")&&!"".equals(reqParam.get("CARDNUM3"))) {
					tmp = CodeUtil.fromJson(reqParam.get("CARDNUM3"), Map.class);
					log.trace(ESAPIUtil.vaildLog("CARDNUM >>{}"+ tmp.get("ACN")));
					reqParam.put("CARDNUM", tmp.get("ACN").toUpperCase());
				}
				reqParam.put("ADAGREEF", "0");
				break;
			case "1": //已約定信用卡帳號 下拉式選單值
				tmp = CodeUtil.fromJson(reqParam.get("CARDNUM1"), Map.class);
				log.trace(ESAPIUtil.vaildLog("CARDNUM >>{}"+ tmp.get("ACN")));
				reqParam.put("CARDNUM", tmp.get("ACN").toUpperCase());
				reqParam.put("ADAGREEF", "1");
				break;
			case "2": //新增繳納本行信用卡款 頁面上將值塞到CARDNUM2  20190710 
				reqParam.put("CARDNUM", reqParam.get("CARDNUM2").toUpperCase());
				reqParam.put("ADAGREEF", "0");
				break;
			}
			
			log.trace(ESAPIUtil.vaildLog("OUTACN_PD >>{}"+ reqParam.get("OUTACN")));
			log.trace(ESAPIUtil.vaildLog("OUTACN_NPD >>{}"+ reqParam.get("OUTACN_NPD")));
			if (null != reqParam.get("OUTACN") && !"".equals(reqParam.get("OUTACN"))) {
				reqParam.put("ACN", reqParam.get("OUTACN"));
			}
			if (null != reqParam.get("OUTACN_NPD") && !"".equals(reqParam.get("OUTACN_NPD"))) {
				reqParam.put("ACN", reqParam.get("OUTACN_NPD"));
			}

			// tmp = CodeUtil.fromJson(reqParam.get("DPAGACNO"), Map.class);
			// 中台都會檢核此欄位，如果是null 就塞假資料
			// if (StrUtil.isEmpty(reqParam.get("_OTPKEY"))) {
			// newMap.put("_OTPKEY", "1234");
			// }

			reqParam.put("PINNEW", reqParam.get("PINNEW"));
			reqParam.put("CUSIDN", reqParam.get("CUSIDN"));
			log.trace(ESAPIUtil.vaildLog("reqParam>>{}"+ reqParam));
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("payment_Result_Data_Pre_Processing error >> {}",e);
		}
	}

	/**
	 * 轉帳後電文資料後製
	 * 
	 * @param bs
	 * @return
	 */
	public BaseResult payment_Result_Data_Retouch(BaseResult bs) {
		Map<String, Object> data = null;
		Map<String, String> intsacn = null;
		String bnkcod = "";
		try {
			data = (Map<String, Object>) bs.getData();
			log.trace("intsacn >>{} ", intsacn);
			bnkcod = intsacn.get("BNKCOD");
			log.trace("bnkcod {}", bnkcod);

			ADMBANK po = admBankDao.get(ADMBANK.class, bnkcod);

			if (po == null) {
				return bs;
			}
			log.trace("getADBANKNAME {}", po.getADBANKNAME());
			data.put("_INTSACN", bnkcod + "-" + po.getADBANKNAME() + " " + intsacn.get("ACN"));
		} catch (DataAccessException e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("payment_Result_Data_Retouch error >> {}",e);
		}
		return bs;
	}


}
