package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N815_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8265779729432300040L;
	private LinkedList<N815_REST_RSDATA> REC;
	private String CMQTIME;
	private String CUSIDN;
	private String SEQ;
	private String RECNUM;
	
	public LinkedList<N815_REST_RSDATA> getREC() {
		return REC;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public String getSEQ() {
		return SEQ;
	}
	public String getRECNUM() {
		return RECNUM;
	}
	public void setREC(LinkedList<N815_REST_RSDATA> rEC) {
		REC = rEC;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public void setSEQ(String sEQ) {
		SEQ = sEQ;
	}
	public void setRECNUM(String rECNUM) {
		RECNUM = rECNUM;
	}
}
