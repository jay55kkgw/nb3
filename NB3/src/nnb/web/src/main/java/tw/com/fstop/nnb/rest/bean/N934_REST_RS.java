package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N934_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4152363686746387737L;
	
    private String[] REC;

    private String CMQTIME;

    private String OFFSET;

    private String __OCCURS;

    private String HEADER;

    private String MSGCOD;

	public String[] getREC()
	{
		return REC;
	}

	public void setREC(String[] rEC)
	{
		REC = rEC;
	}

	public String getCMQTIME()
	{
		return CMQTIME;
	}

	public void setCMQTIME(String cMQTIME)
	{
		CMQTIME = cMQTIME;
	}

	public String getOFFSET()
	{
		return OFFSET;
	}

	public void setOFFSET(String oFFSET)
	{
		OFFSET = oFFSET;
	}

	public String get__OCCURS()
	{
		return __OCCURS;
	}

	public void set__OCCURS(String __OCCURS)
	{
		this.__OCCURS = __OCCURS;
	}

	public String getHEADER()
	{
		return HEADER;
	}

	public void setHEADER(String hEADER)
	{
		HEADER = hEADER;
	}

	public String getMSGCOD()
	{
		return MSGCOD;
	}

	public void setMSGCOD(String mSGCOD)
	{
		MSGCOD = mSGCOD;
	}
    
    

}
