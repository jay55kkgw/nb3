package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N560_REST_RSDATA_CRY extends BaseRestBean implements Serializable {

	private static final long serialVersionUID = 1370007803269219195L;

	/**
	 * 
	 */
	private String FXTOTAMTRECNUM;
	private String AMTRBILLCCY;
	private String FXTOTAMT;
	
	public String getFXTOTAMTRECNUM() {
		return FXTOTAMTRECNUM;
	}
	public void setFXTOTAMTRECNUM(String fXTOTAMTRECNUM) {
		FXTOTAMTRECNUM = fXTOTAMTRECNUM;
	}
	public String getAMTRBILLCCY() {
		return AMTRBILLCCY;
	}
	public void setAMTRBILLCCY(String aMTRBILLCCY) {
		AMTRBILLCCY = aMTRBILLCCY;
	}
	public String getFXTOTAMT() {
		return FXTOTAMT;
	}
	public void setFXTOTAMT(String fXTOTAMT) {
		FXTOTAMT = fXTOTAMT;
	}
	
	
	
}
