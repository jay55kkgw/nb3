package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N1011_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8372330692954145813L;	

	private	String BRHTEL;//分行聯絡電話
	private	String BRHADDR;//分行中文住址
	private	String BRHNAME;//分行中文名稱
	private	String RSPCODE;//回應代碼
	private	String HEADER;
	private	String OFFSET;
	private	String CUSIDN;
	private	String ACN;//帳號
	private	String NAME; //申請人
	private	String ADDRESS;//地址
	private	String BRHDAY;//生日
	private	String BUSINESS;//營業種類或職業
	private	String BUSIDN;//營利事業統一編號
	private	String COUNTRY;//國別
	private	String BIRTHPLA;//出生地
	private	String TEL_O;//電話(公)
	private	String TEL_H;//電話(私)
	private	String EDUSRID;//使用者代號
	private	String CUSNAME;//原住民姓名
	
	public String getBRHTEL() {
		return BRHTEL;
	}
	public void setBRHTEL(String bRHTEL) {
		BRHTEL = bRHTEL;
	}
	public String getBRHADDR() {
		return BRHADDR;
	}
	public void setBRHADDR(String bRHADDR) {
		BRHADDR = bRHADDR;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getRSPCODE() {
		return RSPCODE;
	}
	public void setRSPCODE(String rSPCODE) {
		RSPCODE = rSPCODE;
	}
	public String getBRHNAME() {
		return BRHNAME;
	}
	public void setBRHNAME(String bRHNAME) {
		BRHNAME = bRHNAME;
	}
	public String getHEADER() {
		return HEADER;
	}
	public void setHEADER(String hEADER) {
		HEADER = hEADER;
	}
	public String getOFFSET() {
		return OFFSET;
	}
	public void setOFFSET(String oFFSET) {
		OFFSET = oFFSET;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	public String getADDRESS() {
		return ADDRESS;
	}
	public void setADDRESS(String aDDRESS) {
		ADDRESS = aDDRESS;
	}
	public String getBRHDAY() {
		return BRHDAY;
	}
	public void setBRHDAY(String bRHDAY) {
		BRHDAY = bRHDAY;
	}
	public String getBUSINESS() {
		return BUSINESS;
	}
	public void setBUSINESS(String bUSINESS) {
		BUSINESS = bUSINESS;
	}
	public String getBUSIDN() {
		return BUSIDN;
	}
	public void setBUSIDN(String bUSIDN) {
		BUSIDN = bUSIDN;
	}
	public String getCOUNTRY() {
		return COUNTRY;
	}
	public void setCOUNTRY(String cOUNTRY) {
		COUNTRY = cOUNTRY;
	}
	public String getBIRTHPLA() {
		return BIRTHPLA;
	}
	public void setBIRTHPLA(String bIRTHPLA) {
		BIRTHPLA = bIRTHPLA;
	}
	public String getTEL_O() {
		return TEL_O;
	}
	public void setTEL_O(String tEL_O) {
		TEL_O = tEL_O;
	}
	public String getTEL_H() {
		return TEL_H;
	}
	public void setTEL_H(String tEL_H) {
		TEL_H = tEL_H;
	}
	public String getEDUSRID() {
		return EDUSRID;
	}
	public void setEDUSRID(String eDUSRID) {
		EDUSRID = eDUSRID;
	}
	public String getCUSNAME() {
		return CUSNAME;
	}
	public void setCUSNAME(String cUSNAME) {
		CUSNAME = cUSNAME;
	}
}
