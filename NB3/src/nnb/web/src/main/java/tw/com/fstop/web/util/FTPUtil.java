package tw.com.fstop.web.util;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import org.apache.commons.net.ProtocolCommandEvent;
import org.apache.commons.net.ProtocolCommandListener;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.ftp.FTPSClient;
import org.apache.commons.net.util.TrustManagerUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *	上傳FTP(FTPS)
 */
public class FTPUtil{
	private Logger log = LoggerFactory.getLogger(FTPUtil.class);
    
    //連線(FTP)
    public FTPClient connect(String connectIP, int connectPort, String username, String password, int timeout){
    	FTPClient ftp = null;
    	try{
    		ftp = new FTPClient();
    		ftp.setConnectTimeout(timeout);
	        ftp.addProtocolCommandListener(new ProtocolCommandListener(){
	        	public void protocolCommandSent(ProtocolCommandEvent event){
	        		log.debug("requestMessage = " + event.getMessage());
	        		
	        		if("pass".equalsIgnoreCase(event.getCommand())){
		            	//Hidden Password
	        			log.debug("requestMessage = " + event.getMessage().substring(0,event.getMessage().indexOf(" "))+ " ********");
	        		}
	        	}
	    		public void protocolReplyReceived(ProtocolCommandEvent event){
	    			log.debug("responseMessage = " + event.getMessage());
	    		}
	        });
	        
	        if(connectPort == 0){
	        	ftp.connect(connectIP);
	        }
	        else{
	        	ftp.connect(connectIP,connectPort);
	        }
	        
	        int reply = ftp.getReplyCode();
	        if(!FTPReply.isPositiveCompletion(reply)){
	        	finish(ftp);
	        	return null;
	        }
	        if(!ftp.login(username,password)){
	        	finish(ftp);
	        	return null;
	        }
	        ftp.enterLocalPassiveMode();
	        ftp.setBufferSize(1024 * 1024);
	        log.debug(ftp.printWorkingDirectory());
    	}
    	catch(Exception e){
    		log.error("",e);
            finish(ftp);
            return null;
        }
        return ftp;
    }
    //連線(FTPS)
    public FTPSClient connect(String connectIP, int connectPort, String protocal, String username, String password, int timeout){    		
    	FTPSClient ftps = null;
    	try{
    		ftps = new FTPSClient(protocal,false);
    		ftps.setConnectTimeout(timeout);
	        ftps.setTrustManager(TrustManagerUtils.getAcceptAllTrustManager()); 
	        ftps.addProtocolCommandListener(new ProtocolCommandListener(){
	        	public void protocolCommandSent(ProtocolCommandEvent event){
	        		log.debug("requestMessage = " + event.getMessage());
	        		
	        		if("pass".equalsIgnoreCase(event.getCommand())){
	        			//Hidden Password
	        			log.debug("requestMessage = " + event.getMessage().substring(0,event.getMessage().indexOf(" "))+ " ********");
			        }
			    }
			    public void protocolReplyReceived(ProtocolCommandEvent event){
			    	log.debug("responseMessage = " + event.getMessage());
			    }
	    	});
	        
	        if(connectPort == 0){
	        	ftps.connect(connectIP);
	        }
	        else{
	        	ftps.connect(connectIP,connectPort);
	        }
	        
	        int reply = ftps.getReplyCode();
	        if(!FTPReply.isPositiveCompletion(reply)){
	        	finish(ftps);
	        	return null;
	        }
	        if(!ftps.login(username,password)){
	        	finish(ftps);
	        	return null;
	        }
	        if(ftps.getDefaultPort() == 21){
	        	ftps.execPBSZ(0);
	        	ftps.execPROT("P");
	        	ftps.enterLocalPassiveMode();
            }
	        log.debug(ftps.printWorkingDirectory());
    	}
    	catch(Exception e){
    		log.error("",e);
            finish(ftps);
            return null;
        }
        return ftps;
    }
    
    // 連線(FTPS)
   	public FTPSClient ftpsConnect(String connectIP, int connectPort, String protocol, String username, String password) {
   		FTPSClient ftpsClient = new FTPSClient(true);
   		boolean login = false;
   		try{
   			log.debug("connect...");
   			ftpsClient.connect(connectIP,connectPort);
   			
   			log.debug("login...");
   			login = ftpsClient.login(username, password);
   			
   			log.debug("login: "+login);
   			int reply = ftpsClient.getReplyCode();
   			log.debug("reply: "+reply);
  	         
   			if(login) {
   				ftpsClient.execPBSZ(0);
   				ftpsClient.execPROT("P");
   				ftpsClient.enterLocalPassiveMode();
   				
   				ftpsClient.setBufferSize(1024 * 1024);
   			}
   			else {
   				return null;
   			}
               
   		} catch (Exception e) { 
   			e.printStackTrace();
   			System.out.println("error"+e);
   		} 
   		return ftpsClient;
   	}
    
	//將FTP目錄往上一層
    public boolean changeToParentDirectory(FTPClient ftp){
    	boolean result = false;
    	
	    try{
	    	log.debug("變更路徑前:"+ftp.printWorkingDirectory());
		    if(!ftp.changeToParentDirectory()){
		    	return result;
		    }
		    log.debug("變更路徑後:"+ftp.printWorkingDirectory());
		    ftp.printWorkingDirectory();
		    result = true;
		}
	    catch(Exception e){
	    	log.error("",e);
	    }
	    return result;
    }
	//將FTPS目錄往上一層
    public boolean changeToParentDirectory(FTPSClient ftps){
    	boolean result = false;
    	
	    try{
	    	log.debug("變更路徑前:"+ftps.printWorkingDirectory());
		    if(!ftps.changeToParentDirectory()){
		    	return result;
		    }
		    log.debug("變更路徑後:"+ftps.printWorkingDirectory());
		    ftps.printWorkingDirectory();
		    result = true;
		}
	    catch(Exception e){
	    	log.error("",e);
	    }
	    return result;
    }
    //FTP變更上傳路徑
    public boolean ChangeToDirectory(FTPClient ftp,String path){
    	boolean result = false;
    	
	    try{
	    	log.debug("變更路徑前:"+ftp.printWorkingDirectory());
	    	if(!ftp.changeWorkingDirectory(path)){
	    		return result;
	    	}
	    	log.debug("變更路徑後:"+ftp.printWorkingDirectory());
	    	ftp.printWorkingDirectory();
	    	result = true;
	    }
	    catch(Exception e){
	    	log.error("",e);
    	}
	    return result;
    }
	//FTPS變更上傳路徑
    public boolean ChangeToDirectory(FTPSClient ftps,String path){
    	boolean result = false;
    	
	    try{
	    	log.debug("變更路徑前:"+ftps.printWorkingDirectory());
	    	if(!ftps.changeWorkingDirectory(path)){
	    		return result;
	    	}
	    	log.debug("變更路徑後:"+ftps.printWorkingDirectory());
	    	ftps.printWorkingDirectory();
	    	result = true;
	    }
	    catch(Exception e){
	    	log.error("",e);
    	}
	    return result;
    }
    //FTP上傳
    public boolean uploadFile(FTPClient ftp, String fileName, byte[] byteFile){
    	boolean result = false;
    	InputStream is = null;
    	
    	try{
    		is = new ByteArrayInputStream(byteFile);
    	
    		ftp.setFileType(FTP.BINARY_FILE_TYPE);
    		ftp.setControlEncoding("UTF-8");
    		ftp.enterLocalPassiveMode();
    		result = ftp.storeFile(fileName,is);
    	}
	    catch(Exception e){
	    	log.error("",e);
    	}
    	try{
    		if(is != null){
    			is.close();
    		}
    	}
	    catch(Exception e){
	    	log.error("",e);
		}
	    return result;
    }
    
	//FTPS上傳
    public boolean uploadFile(FTPSClient ftps, String fileName, byte[] byteFile){
    	boolean result = false;
    	InputStream is = null;
    	
    	try{
    		is = new ByteArrayInputStream(byteFile);
    	
    		ftps.setFileType(FTP.BINARY_FILE_TYPE);
    		ftps.setControlEncoding("UTF-8");
    		
    		result = ftps.storeFile(fileName,is);
    	}
	    catch(Exception e){
	    	log.error("",e);
    	}
    	try{
    		if(is != null){
    			is.close();
    		}
    	}
	    catch(Exception e){
	    	log.error("",e);
		}
	    return result;
    }
    //FTP結束連線
    public void finish(FTPClient ftp){
    	try{
    		if(ftp != null && ftp.isConnected()){
    			ftp.logout();
    			ftp.disconnect();
    		} 
    	}
        catch(Exception e){
        	log.error("",e);
		}
    }
	//FTPS結束連線
    public void finish(FTPSClient ftps){
    	try{
	        if(ftps != null && ftps.isConnected()){
	        	ftps.logout();
	        	ftps.disconnect();
	        }
    	}
        catch(Exception e){
        	log.error("",e);
		}
    }
}