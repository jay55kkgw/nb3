package tw.com.fstop.nnb.service;

import java.util.ArrayList;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.AdmMsgCodeDao;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.SpringBeanFactory;

/**
 * 
 * 功能說明 :貸款查詢類功能
 *
 */
@Service
public class Loan_Query_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	AdmMsgCodeDao admMsgCodeDao;
	
	@Autowired
	private I18n i18n;

	/**
	 * 借款明細查詢
	 * 
	 * @param sessionId
	 * @param pw
	 * @return
	 */
	public BaseResult loan_detail(Map<String, String> reqParam) {
		log.trace("loan_query_service");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// call ws API
			bs = N320_REST(reqParam);
			// 資料處理後重新封裝bs
			if (bs != null) {
				// 把Client端需要的資料重新封裝
				if (bs.getResult()) {
					loan_detail_processing(bs);
				}
				log.debug("N320_REST: " + bs.getData());
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}

	/**
	 * 借款明細查詢_BaseResult封裝Client端所需資料
	 * 
	 * @param bs
	 * @return
	 */
	public void loan_detail_processing(BaseResult bs) {
		Map<String, Object> mapN320 = null;
		ArrayList<Map<String, String>> twTable = new ArrayList();
		ArrayList<Map<String, String>> fxTable = new ArrayList();
		ArrayList<Map<String, String>> cryTable = new ArrayList();
		//降息備註開關
		String noteFlag="N";
		try {
			
			mapN320 = (Map) bs.getData();
			mapN320.put("TOTALBAL_320", NumericUtil.fmtAmount(
					(String)mapN320.get("TOTALBAL_320"),2));
			
			String msgCode320 = (String)mapN320.get("TOPMSG_320");
			log.trace("msgCode320>>{}",msgCode320);
			if(admMsgCodeDao.hasMsg(msgCode320,null)) {
				mapN320.put("errorMsg320",admMsgCodeDao.errorMsg(msgCode320));
			}

			String msgCode552 = (String)mapN320.get("TOPMSG_552");
			log.trace("msgCode552>>{}",msgCode552);
			if(admMsgCodeDao.hasMsg(msgCode552,null)) {
				mapN320.put("errorMsg552",admMsgCodeDao.errorMsg(msgCode552));
			}
			//N320 data
			if("OKOV".equals(mapN320.get("TOPMSG_320"))) {
				//補上前4個@ 不能補空格 會被trim掉 ,後面10個空格TMRA會補
				String userdata = "@@@@"+(String)mapN320.get("USERDATA_X50_320");
				log.trace("USERDATA_X50_320 >>{}" ,userdata );
				mapN320.put("USERDATA_X50_320",userdata);
			}
			
			
			if("OKOV".equals(mapN320.get("TOPMSG_552"))) {
				//補上前4個@ 不能補空格 會被trim掉 ,後面10個空格TMRA會補
				String userdata = "@@@@"+(String)mapN320.get("USERDATA_X50_552");
				log.trace("USERDATA_X50_552 >>{}" ,userdata );
				mapN320.put("USERDATA_X50_552",userdata);
			}
			
			if (null != mapN320.get("TW") && "00".equals(mapN320.get("MSGFLG_320"))) {
				twTable = (ArrayList<Map<String, String>>) mapN320.get("TW");
				for (Map<String, String> map : twTable) {
					//判斷降息備註,只要paytype有值就顯示
					if(!"N".equals(map.get("PAYTYPE"))&&null!=map.get("PAYTYPE")&&!"".equals(map.get("PAYTYPE"))) {
						noteFlag="Y";
					}
					String acn = map.get("ACN");
					String coverAcn = acn.substring(0, 6) + "***" + acn.substring(9);
					map.put("ACNCOVER", coverAcn);
					map.put("BAL", NumericUtil.fmtAmount(map.get("BAL"),2));
					map.put("AMTORLN", NumericUtil.fmtAmount(NumericUtil.addDot(map.get("AMTORLN"), 2),2));
				}
				mapN320.put("TWNUM", twTable.size() );
				log.trace("TWNUM--> {} ", twTable.size());
				
				if("OKOV".equals(msgCode320)) {
					mapN320.put("N320GO", "GO");
				}else if("OKLR".equals(msgCode320)) {
					mapN320.put("N320GO", "STOP");
				}
				mapN320.put("noteFlag", noteFlag);
				log.trace("N320GO>>{}",mapN320.get("N320GO"));
			}
			
			
			if(null != mapN320.get("FX")) {
				fxTable = (ArrayList<Map<String, String>>) mapN320.get("FX");
				for (Map<String, String> map : fxTable) {
					if(!"".equals(map.get("LNACCNO"))){
					String acn = map.get("LNACCNO");
					String coverAcn = acn.substring(0, 6) + "***" + acn.substring(9);
					map.put("ACNCOVER", coverAcn);
					}
					map.put("NXTINTD",map.get("NXTINTD").replace("-", "/") );
					map.put("LNINTST",map.get("LNINTST").replace("-", "/") );
					map.put("LNUPDATE",map.get("LNUPDATE").replace("-", "/") );
					map.put("LNDUEDT",map.get("LNDUEDT").replace("-", "/") );
					map.put("LNCOS", NumericUtil.fmtAmount(map.get("LNCOS"),2));
					map.put("LNFIXR",map.get("LNFIXR"));
					if("".equals(map.get("TYPEA"))||null==map.get("TYPEA")) {
						map.put("TYPEA",i18n.getMsg(this.getBeanMapValue("n552", "")));
					}else if("01".equals(map.get("TYPEA"))||"04".equals(map.get("TYPEA"))){
						map.put("TYPEA",i18n.getMsg(this.getBeanMapValue("n552", map.get("TYPEA"))));
						map.put("lcMemo", "　LC No.　" + map.get("RELREF"));
					}else if("05".equals(map.get("TYPEA"))&&!"".equals(map.get("TYPEA"))) {
						map.put("TYPEA",i18n.getMsg(this.getBeanMapValue("n552", map.get("TYPEA"))));
						map.put("lcMemo","　"+i18n.getMsg("LB.X1743")+"　"+ map.get("RELREF"));
					}else {
						map.put("TYPEA",i18n.getMsg(this.getBeanMapValue("n552", map.get("TYPEA"))));
					}
				}
				
				if(null != mapN320.get("CRY")) {
					cryTable = (ArrayList<Map<String, String>>) mapN320.get("CRY");
					for (Map<String, String> map : cryTable) {
						map.put("FXTOTAMT",NumericUtil.fmtAmount(map.get("FXTOTAMT"),2));
					}
					
				}
				mapN320.put("FXNUM", fxTable.size() );
				log.trace("FXNUM-->{}", fxTable.size());
				
				if("OKOV".equals(msgCode552)) {
					mapN320.put("N552GO","GO");
				}else if("OKLR".equals(msgCode552)) {
					mapN320.put("N552GO", "STOP");
				}
				log.trace("N552GO>>{}",mapN320.get("N552GO"));
			}
			
			mapN320.put("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
			
			if ("00".equals(mapN320.get("MSGFLG_320"))){
				ArrayList<Map<String, String>> callTable = (ArrayList<Map<String, String>>) mapN320.get("TW");
				StringBuffer buffer = new StringBuffer();
				for (Map<String, String> map : callTable) {
					if (
						(null != map.get("ITRLTD") && !"".equals(map.get("ITRLTD"))
						)&&
						(null != map.get("ITRUS") && !"".equals(map.get("ITRUS")))
						) {
						if(null==map.get("SEQ2")) { 
							map.put("SEQ2"," ");
						}
						buffer.append(
								i18n.getMsg("LB.X1725")+ " <U>"
								+map.get("SEQ2") + "</U> " + map.get("ITRLTD") + " "
								+i18n.getMsg(this.getBeanMapValue("nextLoan", "ITRUSSN." + map.get("ITRUS"))) 
								+" "+ i18n.getMsg("LB.X1726")
								+ map.get("RAT") + "%。 "); 
					}
				}
				mapN320.put("note", buffer.toString());
			}
						
			bs.setData(mapN320);	
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("loan_detail_processing error >> {}",e);
		}

	}
	/**
	 * 
	 * @param sessionId
	 * @return
	 */
	public BaseResult next_loan_query(Map<String, String> reqParam) {

		log.trace("next_loan_query_service_rs");

		// 處理結果
		BaseResult bs = null;
		try {
			
			bs = new BaseResult();
			
			// REST_N014
			bs = call_N014_REST(reqParam);
			bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
			next_loan_processing(bs);
			
		} catch (Exception e) {
			// e.printStackTrace();
			log.error("", e);
		}
		// 回傳處理結果
		return bs;

	}
	
	/**
	 * 下期借款查詢_BaseResult封裝Client端所需資料
	 * 
	 * @param bs
	 * @return
	 */
	public void next_loan_processing(BaseResult bs) {
		Map<String, Object> mapN014 = null;
		mapN014 = (Map<String, Object>) bs.getData();
		if ("00".equals(mapN014.get("MSGFLG_014"))){
			ArrayList<Map<String, String>> callTable = (ArrayList<Map<String, String>>) mapN014.get("TW");
			StringBuffer buffer = new StringBuffer();
			for (Map<String, String> map : callTable) {
				if (
					(null != map.get("ITRLTD") && !"".equals(map.get("ITRLTD"))
					)&&
					(null != map.get("ITRUS") && !"".equals(map.get("ITRUS")))
					) {
					buffer.append(
							i18n.getMsg("LB.X1725")+ " <U>"
							+map.get("SEQ2") + "</U> " + map.get("ITRLTD") + " "
							+i18n.getMsg(this.getBeanMapValue("nextLoan", "ITRUSSN." + map.get("ITRUS"))) 
							+" "+ i18n.getMsg("LB.X1726")
							+ map.get("RAT") + "%。 ");
				}
			}
			mapN014.put("note", buffer.toString());
		}
		
		//TODO 先把錯誤訊息寫在NB3 因為ms_tw 資料庫還沒好
		String msgCode014 =(String) ((Map<String, Object>) bs.getData()).get("TOPMSG_014");
		String msgCode553 = (String)((Map<String, Object>) bs.getData()).get("TOPMSG_553");
		if(!msgCode014.startsWith("OK")) {
			mapN014.put("errorMsg014",admMsgCodeDao.errorMsg(msgCode014));
		}
		if(!msgCode553.startsWith("OK")) {
			mapN014.put("errorMsg553",admMsgCodeDao.errorMsg(msgCode553));
		}
		bs.setData(mapN014);
	}
	
	/**
	 * 
	 * @param cusidn
	 * @param period
	 * @return
	 */
	public BaseResult paid_query(String cusidn, String period) {
		log.trace("paid_query_result_service");
		// 處理結果
		BaseResult bs = null;
		Map<String, Object> mapN016 = null;
		try {
			bs = new BaseResult();
			
			// 電文N016
			bs = N016_REST(cusidn, period);
			String msgCode016 =(String) ((Map<String, Object>) bs.getData()).get("TOPMSG_016");
			String msgCode554 = (String)((Map<String, Object>) bs.getData()).get("TOPMSG_554");
			
			//錯誤訊息
			if(admMsgCodeDao.hasMsg(msgCode016,null)) {
				bs.addData("errorMsg016",admMsgCodeDao.errorMsg(msgCode016));
			}
			if(admMsgCodeDao.hasMsg(msgCode554,null)) {
				bs.addData("errorMsg554",admMsgCodeDao.errorMsg(msgCode554));
			}
		} catch (Exception e) {
			// e.printStackTrace();
			log.error("", e);
		}
		// 回傳處理結果
		return bs;

	}
	
	/**
	 * 輸入beanId && beanMapKey Get beanMapValue
	 * 
	 * @param beanId
	 * @param beanMapKey
	 * @see spring-arg.xml
	 *  @return get spring-arg.xml beanMapValue By beanId && beanMapKey, if any (or
	 *         empty String otherwise)
	 */
	public String getBeanMapValue(String beanId, String beanMapKey)
	{
		// 取得Bean中的Map
		Map<String, String> getTypeBean = SpringBeanFactory.getBean(beanId);
		// 取得Map中的Value
		String getTypeBeanValue = getTypeBean.get(beanMapKey);
		return getTypeBeanValue;
	}



}