package tw.com.fstop.nnb.rest.bean;



public class Monitor_Status {
	
	private String SERVER_STATUS;
	private String DB_STATUS;
	private String TEL_STATUS;
	private String LOGDetail;
	private String BOOT_TIME;
	public String getSERVER_STATUS() {
		return SERVER_STATUS;
	}
	public void setSERVER_STATUS(String sERVER_STATUS) {
		SERVER_STATUS = sERVER_STATUS;
	}
	public String getBOOT_TIME() {
		return BOOT_TIME;
	}
	public void setBOOT_TIME(String bOOT_TIME) {
		BOOT_TIME = bOOT_TIME;
	}
	public String getDB_STATUS() {
		return DB_STATUS;
	}
	public void setDB_STATUS(String dB_STATUS) {
		DB_STATUS = dB_STATUS;
	}
	public String getTEL_STATUS() {
		return TEL_STATUS;
	}
	public void setTEL_STATUS(String tEL_STATUS) {
		TEL_STATUS = tEL_STATUS;
	}
	public String getLOGDetail() {
		return LOGDetail;
	}
	public void setLOGDetail(String lOGDetail) {
		LOGDetail = lOGDetail;
	}
	public void initialStatus( ) {
		this.setDB_STATUS("not connected");
		this.setTEL_STATUS("not connected");
		this.setSERVER_STATUS("dead");
		this.setLOGDetail("");
		this.setBOOT_TIME("");
	}
	public void addLog(String logstring) {
		if(this.getLOGDetail().equals(""))this.setLOGDetail(this.getLOGDetail()+logstring);
		else this.setLOGDetail(this.getLOGDetail()+logstring+"\n");	
	}
}
