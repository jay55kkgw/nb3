package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N559_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6026625164684292891L;

	private String CMRECNUM ;
	private String CMPERIOD;
	private String CMQTIME ;
	private String USERDATA;
	
	private LinkedList<N559_REST_RSDATA> REC;
	private LinkedList<N559_REST_RSDATA_CRY> CRY;
	
	public String getCMRECNUM() {
		return CMRECNUM;
	}
	public void setCMRECNUM(String cMRECNUM) {
		CMRECNUM = cMRECNUM;
	}
	public String getCMPERIOD() {
		return CMPERIOD;
	}
	public void setCMPERIOD(String cMPERIOD) {
		CMPERIOD = cMPERIOD;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getUSERDATA() {
		return USERDATA;
	}
	public void setUSERDATA(String uSERDATA) {
		USERDATA = uSERDATA;
	}
	public LinkedList<N559_REST_RSDATA> getREC() {
		return REC;
	}
	public void setREC(LinkedList<N559_REST_RSDATA> rEC) {
		REC = rEC;
	}
	public LinkedList<N559_REST_RSDATA_CRY> getCRY() {
		return CRY;
	}
	public void setCRY(LinkedList<N559_REST_RSDATA_CRY> cRY) {
		CRY = cRY;
	}
	
}
