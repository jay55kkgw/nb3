package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * 自然人憑證驗證RS
 */
public class Vacheck_REST_RS extends BaseRestBean implements Serializable{
	private static final long serialVersionUID = 6791228050721094802L;
	
	private String RESPONSE;

	public String getRESPONSE(){
		return RESPONSE;
	}
	public void setRESPONSE(String rESPONSE){
		RESPONSE = rESPONSE;
	}
}