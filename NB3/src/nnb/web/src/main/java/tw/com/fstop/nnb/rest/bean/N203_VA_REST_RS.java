package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N203_VA_REST_RS  extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3073286504146866834L;
	private String UID;
	private String CUSIDN;
	private String FGTXWAY;
	
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
}
