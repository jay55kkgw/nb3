package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N1014_REST_RQ extends BaseRestBean_OLS implements Serializable {
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6262312293389012762L;
	
	private	String CUSIDN;	
	private	String ACN;
	private	String TYPE;//查詢類別
	private	String COPY;//申請份數
	private	String PURPOSE;//申請用途
	private	String DATE1;//日期
	private	String INQ_TYPE;//證明書種類
	private	String CRY;//幣別
	private	String AMT;//交易金額
	private	String FORM;//中/英文證明
	private	String TAKE_TYPE;//領取方式
	private	String FEAMT;//手續費
	private	String OUTACN;//扣帳帳號
	private	String PINNEW;
	private	String MAILADDR;//郵寄地址
	private	String FGTXWAY;
	private	String ENGNAME;//英文帳戶
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getTYPE() {
		return TYPE;
	}
	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}
	public String getCOPY() {
		return COPY;
	}
	public void setCOPY(String cOPY) {
		COPY = cOPY;
	}
	public String getPURPOSE() {
		return PURPOSE;
	}
	public void setPURPOSE(String pURPOSE) {
		PURPOSE = pURPOSE;
	}
	public String getDATE1() {
		return DATE1;
	}
	public void setDATE1(String dATE1) {
		DATE1 = dATE1;
	}
	public String getINQ_TYPE() {
		return INQ_TYPE;
	}
	public void setINQ_TYPE(String iNQ_TYPE) {
		INQ_TYPE = iNQ_TYPE;
	}
	public String getCRY() {
		return CRY;
	}
	public void setCRY(String cRY) {
		CRY = cRY;
	}
	public String getAMT() {
		return AMT;
	}
	public void setAMT(String aMT) {
		AMT = aMT;
	}
	public String getFORM() {
		return FORM;
	}
	public void setFORM(String fORM) {
		FORM = fORM;
	}
	public String getTAKE_TYPE() {
		return TAKE_TYPE;
	}
	public void setTAKE_TYPE(String tAKE_TYPE) {
		TAKE_TYPE = tAKE_TYPE;
	}
	public String getFEAMT() {
		return FEAMT;
	}
	public void setFEAMT(String fEAMT) {
		FEAMT = fEAMT;
	}
	public String getOUTACN() {
		return OUTACN;
	}
	public void setOUTACN(String oUTACN) {
		OUTACN = oUTACN;
	}
	public String getPINNEW() {
		return PINNEW;
	}
	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}
	public String getMAILADDR() {
		return MAILADDR;
	}
	public void setMAILADDR(String mAILADDR) {
		MAILADDR = mAILADDR;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getENGNAME() {
		return ENGNAME;
	}
	public void setENGNAME(String eNGNAME) {
		ENGNAME = eNGNAME;
	}

	
	

}
