package tw.com.fstop.nnb.spring.controller;

import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import org.hsqldb.lib.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;


import tw.com.fstop.common.BaseResult;
import tw.com.fstop.idgate.bean.N076_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N076_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N078_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N078_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N079_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N079_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N074_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N074_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N830_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N830_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N831_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N831_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N077_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N077_IDGATE_DATA_VIEW;
import tw.com.fstop.nnb.aop.Dialog;
import tw.com.fstop.nnb.rest.bean.N076_REST_RQ;
import tw.com.fstop.nnb.service.Acct_Tdeposit_Service;
import tw.com.fstop.nnb.service.DaoService;
import tw.com.fstop.nnb.service.IdGateService;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.WebUtil;

/**
 * 
 * 功能說明 :臺幣定存頁面流程控制器
 *
 */
@SessionAttributes({ SessionUtil.CUSIDN, SessionUtil.DPMYEMAIL, SessionUtil.STEP1_LOCALE_DATA,
		SessionUtil.CONFIRM_LOCALE_DATA, SessionUtil.RESULT_LOCALE_DATA, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN,
		SessionUtil.IDGATE_TRANSDATA})
@Controller
@RequestMapping(value = "/NT/ACCT/TDEPOSIT")
public class Acct_Tdeposit_Controller
{

	// jsp資料夾路徑 acct
	private final String JSPFOLDERPATH = "acct";

	// 錯誤頁路徑
	private final String ERRORJSP = "/error";

	Logger log = LoggerFactory.getLogger(this.getClass());

	// 台幣定存相關邏輯
	@Autowired
	private Acct_Tdeposit_Service acct_Tdeposit_Service;
	
	// DAO相關邏輯
	@Autowired
	private DaoService daoService;
	
	@Autowired
	IdGateService idgateservice;

	/**
	 * 
	 * @param request<br>
	 *            DPGONAME 好記名稱 EX:DPGONAME : 050-臺灣企銀 <br>
	 *            DPPHOTOID 好記圖片 EX: DPPHOTOID : 050-臺灣企銀 <br>
	 *            DPTRACNO 約定否 :1：約定 2：非約定 <br>
	 *            DPTRIBANK 銀行代碼 <br>
	 *            DPTRDACNO 銀行帳號
	 * @return
	 */
	@RequestMapping(value = "/addAcn_aj", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody BaseResult getACNO_Data(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace(" addAcn_aj Start ~~~~~~~~~~~~~~~");
		BaseResult bs = new BaseResult();
		try
		{
			// Get session value by session Key
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			reqParam.put("DPUSERID", cusidn);
			log.trace(ESAPIUtil.vaildLog("addAcn_aj reqParam >> " + CodeUtil.toJson(reqParam)));
			bs = daoService.add_common_acount(reqParam);
			log.debug("addAcn_aj >>>>>>> {}", bs.getData());
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getACNO_Data error >> {}",e);
		}

		return bs;

	}

	/**
	 * N074轉入臺幣綜存定存進入頁!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/deposit_transfer")
	public String deposit_transfer(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("deposit_transfer Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		try
		{
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			
			String getAcn = reqParam.get("Acn");
			log.trace(ESAPIUtil.vaildLog("GET ACN >>> " + getAcn));
			
			// Get session value by session Key
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			bs = acct_Tdeposit_Service.getOutAcn(cusidn);
			bs.addData("Acn", getAcn);
			//公司戶不得轉"存本取息"及"整存整付"
			bs.addData("cidlength", cusidn.length());
			if(StrUtil.isNotEmpty(reqParam.get("back"))) {
				if(reqParam.get("back").equals("Y")) {
					bs.addAllData(reqParam);
				}
			}
	        //IDGATE身分
	        String idgateUserFlag="N";		 
	        Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
	        try {		 
	       	   if(IdgateData==null) {
	       		   IdgateData = new HashMap<String, Object>();
	       	   }
	            BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
	            if(tmp.getResult()) {		 
	                idgateUserFlag="Y";                  		 
	            }		 
	            tmp.addData("idgateUserFlag",idgateUserFlag);		 
	            IdgateData.putAll((Map<String, String>) tmp.getData());
	            SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	        }catch(Exception e) {		 
	            log.debug("idgateUserFlag error {}",e);		 
	        }		 
	        model.addAttribute("idgateUserFlag",idgateUserFlag);

		}
		catch (Exception e)
		{
			log.error("deposit_transfer Error ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				model.addAttribute("deposit_transfer", bs);
				target = JSPFOLDERPATH + "/deposit_transfer";
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * ********************N074轉入臺幣綜存定存確認頁*******************
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/deposit_transfer_confirm")
	public String deposit_transfer_confirm(@RequestParam Map<String, String> reqParam, Model model)
	{

		log.trace("deposit_transfer_confirm Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		String jsondc = "";
		try
		{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam), "UTF-8");
			log.trace(ESAPIUtil.vaildLog("renewal_apply_confirm.jsondc >> " + jsondc));
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
			}
			else
			{
				bs = acct_Tdeposit_Service.deposit_transfer_confirm(okMap);
				// 防止重送代碼TxToken
				bs.addData("TXTOKEN", ((Map<String, String>) acct_Tdeposit_Service.getTxToken().getData()).get("TXTOKEN"));
				// IKEY要使用的JSON:DC
				bs.addData("jsondc", jsondc);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
				// IDGATE transdata
				Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
				Map<String,String> dataMap=new HashMap();
				Map<String,String> result=(Map<String,String>)bs.getData();
				dataMap.putAll((Map<String,String>)bs.getData());
				dataMap.put("ACN", dataMap.get("OUTACN"));
				
	    		String adopid = "N074";
	    		String title = "您有一筆-臺幣轉入綜存定存交易待確認,金額"+dataMap.get("AMOUNT");								
	    		if(IdgateData != null) {
	    			model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
	    			if(IdgateData.get("idgateUserFlag").equals("Y")) {
	    				dataMap.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
	    				dataMap.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
//	    				IdgateData=acct_Tdeposit_Service.createDepositIdgateData(bs,IdgateData);
//	    				Map<String,Object>tmp=new HashMap();
//	    				tmp.put("N074_IDGATE", IdgateData);
//	    				SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, tmp);
	    				
	    				//2021/10/22更新 , 若選擇指定到期日 , MS層會把TERM轉成99 , 造成比對失敗
	    				//若選擇指定到期日 ,  FGTDPERIOD = 2 
	    				if("2".equals(result.get("FGTDPERIOD"))) {
	    					dataMap.put("TERM","99");
	    				}
	    				IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N074_IDGATE_DATA.class, N074_IDGATE_DATA_VIEW.class, dataMap));
	    				SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	    			}
	    		}
	            model.addAttribute("idgateAdopid", adopid);
	            model.addAttribute("idgateTitle", title);

			}
		}
		catch (Exception e)
		{
			log.error("deposit_transfer Error ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				model.addAttribute("deposit_transfer_confirm", bs);
				target = JSPFOLDERPATH + "/deposit_transfer_confirm";
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * ********************N074轉入臺幣綜存定存結果頁*******************
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/deposit_transfer_result")
	@Dialog(ADOPID = "N074")
	public String deposit_transfer_result(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("deposit_transfer_result Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		String pageToken = "";
		try
		{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			//Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI

			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{	
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
			}
			else
			{
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
				String dpMyEmail = (String) SessionUtil.getAttribute(model, SessionUtil.DPMYEMAIL, null);
				// Check Session Token
				String SessionFinshToken = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null);
				pageToken = reqParam.get("TXTOKEN");
				BaseResult checkToken = acct_Tdeposit_Service.validateToken(pageToken, SessionFinshToken);
				if (!checkToken.getResult())
				{
					bs = checkToken;
				}
				else
				{
					//IDgate驗證
					try {
						if(okMap.get("FGTXWAY").equals("7")) {
	        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
	        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N074_IDGATE");	
							okMap.put("sessionID",(String) IdgateData.get("sessionid"));
							okMap.put("txnID",(String) IdgateData.get("txnID"));
							okMap.put("idgateID",(String) IdgateData.get("IDGATEID"));
							log.debug("idgate transdata >>"+CodeUtil.toJson(okMap));
						}
					}catch(Exception e) {
						log.error("IDGATE ValidateFail>>{}",bs.getResult());
					}	
					
					Map<String,Object>IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
					model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
					
					bs = acct_Tdeposit_Service.deposit_transfer_result(cusidn, dpMyEmail, okMap);
					// 將查過的資料放入session，待切換 locale 時讀取
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				}
			}
		}
		catch (Exception e)
		{
			log.error("deposit_transfer_result Error ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, pageToken);
				model.addAttribute("deposit_transfer_result", bs);
				target = JSPFOLDERPATH + "/deposit_transfer_result";
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * N077臺幣定存自動轉期申請/變更進入頁!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/renewal_apply")
	public String renewal_apply(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("renewal_apply Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		try
		{
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			// Get session value by session Key
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			bs = acct_Tdeposit_Service.renewal_apply(cusidn);
			log.trace("bs.getResult>> {}", bs.getResult());
		}

		catch (Exception e)
		{
			log.error("renewal_apply Error ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				model.addAttribute("renewal_apply", bs);
				target = JSPFOLDERPATH + "/renewal_apply";
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * 
	 * ********************N077臺幣定存自動轉期申請/變更輸入頁********************
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/renewal_apply_step1")
	public String renewal_apply_step1(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("order_renewal_step1 Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		try
		{
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			boolean back = "Y".equals(okMap.get("back"));
			if (hasLocale || back)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null));
			}
			else
			{
				
				bs = acct_Tdeposit_Service.renewal_apply_step1(cusidn, okMap);

				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, bs);
				log.trace("bs.getResult>> {}", bs.getResult());				
			}
	        //IDGATE身分
	        String idgateUserFlag="N";		 
	        Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
	        try {		 
	        	if(IdgateData==null) {
	        		IdgateData = new HashMap<String, Object>();
	        	}
	            BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
	            if(tmp.getResult()) {		 
	                idgateUserFlag="Y";                  		 
	            }		 
	            tmp.addData("idgateUserFlag",idgateUserFlag);		 
	            IdgateData.putAll((Map<String, String>) tmp.getData());
	            SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	        }catch(Exception e) {		 
	            log.debug("idgateUserFlag error {}",e);		 
	        }		 
	        model.addAttribute("idgateUserFlag",idgateUserFlag);
		}
		catch (Exception e)
		{
			log.error("renewal_apply_step1 Error", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = JSPFOLDERPATH + "/renewal_apply_step1";
				model.addAttribute("renewal_apply_step1", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * ********************N077臺幣定存自動轉期申請/變更確認頁********************
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/renewal_apply_confirm")
	public String renewal_apply_confirm(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("renewal_apply_confirm Start ~~~~~~~~~~~~~~~~");
		String jsondc = "";
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		try
		{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam), "UTF-8");
			log.trace(ESAPIUtil.vaildLog("renewal_apply_confirm.jsondc >> " + jsondc));
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
			}
			else
			{
				// Get Session Value
				bs = acct_Tdeposit_Service.renewal_apply_confirm(okMap);
				// 防止重送代碼TxToken
				bs.addData("TXTOKEN", ((Map<String, String>) acct_Tdeposit_Service.getTxToken().getData()).get("TXTOKEN"));
				// IKEY要使用的JSON:DC
				bs.addData("jsondc", jsondc);
				log.trace("renewal_apply_confirm bs.getData {}", bs.getData());
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
				
				// IDGATE transdata
				Map<String, String> result = null;
				result=(Map<String, String>) bs.getData();
	            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
	    		String adopid = "N077";
	    		String title = "您有一筆-臺幣定存自動轉期申請/變更交易待確認,金額"+result.get("AMTFDP");
	        	if(IdgateData != null) {
		            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
		            if(IdgateData.get("idgateUserFlag").equals("Y")) {
		            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
		            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
						IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N077_IDGATE_DATA.class, N077_IDGATE_DATA_VIEW.class, result));
		                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
		            }
	        	}
	            model.addAttribute("idgateAdopid", adopid);
	            model.addAttribute("idgateTitle", title);
			}
		}
		catch (Exception e)
		{
			log.error("renewal_apply_confirm Error", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = JSPFOLDERPATH + "/renewal_apply_confirm";
				model.addAttribute("renewal_apply_confirm", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * ********************N077臺幣定存自動轉期申請/變更結果頁********************
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/renewal_apply_result")
	public String renewal_apply_result(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("renewal_apply_result Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		String pageToken = "";
		try
		{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			//Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI

			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
			}
			else
			{
				// Get Session Value
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
				String SessionFinshToken = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null);
				// Check Session Token
				pageToken = okMap.get("TXTOKEN");
				BaseResult checkToken = acct_Tdeposit_Service.validateToken(pageToken, SessionFinshToken);
				if (!checkToken.getResult())
				{
					bs = checkToken;
				}
				else
				{
	                //IDgate驗證		 
	                try {		 
	                    if(okMap.get("FGTXWAY").equals("7")) {		 
	        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
	        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N077_IDGATE");
	                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
	                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
	                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));	 
	                    }
	                }catch(Exception e) {		 
	                    log.error("IDGATE TRANS error>>{}",e);		 
	                }  
					Map<String,Object>IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
					model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
					
					bs = acct_Tdeposit_Service.renewal_apply_result(cusidn, okMap);
					// 將查過的資料放入session，待切換 locale 時讀取
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				}
				log.trace("bs.getResult>> {}", bs.getResult());
			}
		}
		catch (Exception e)
		{
			log.error("renewal_apply_result Error", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, pageToken);
				target = JSPFOLDERPATH + "/renewal_apply_result";
				model.addAttribute("renewal_apply_result", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * N078臺幣定存單到期續存進入頁!!!!!!!!!!!!!!!!!!
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/order_renewal")
	public String order_renewal(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("order_renewal Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		try
		{
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			// Get session value by session Key
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			// 得到定存到期的清單
			bs = acct_Tdeposit_Service.order_renewal(cusidn);
			log.trace("order_renewal bs.getResult>> {}", bs.getResult());
		}
		catch (Exception e)
		{
			log.error("order_renewal Error", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = JSPFOLDERPATH + "/order_renewal";
				model.addAttribute("order_renewal", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * ********************N078臺幣定存單到期續存輸入頁********************
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/order_renewal_step1")
	public String order_renewal_step1(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("order_renewal_step1 Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();

		try
		{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			boolean back = "Y".equals(okMap.get("back"));
			if (hasLocale || back)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null));
			}
			else
			{
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
				bs = acct_Tdeposit_Service.order_renewal_step1(cusidn, okMap);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, bs);
			}
			 //IDGATE身分
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
	        String idgateUserFlag="N";		 
	        Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
	        try {		 
	     	    if(IdgateData==null) {
	       		    IdgateData = new HashMap<String, Object>();
	       	    }
	            BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
	            if(tmp.getResult()) {		 
	                idgateUserFlag="Y";                  		 
	            }		 
	            tmp.addData("idgateUserFlag",idgateUserFlag);		 
	            IdgateData.putAll((Map<String, String>) tmp.getData());
	            SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	         }catch(Exception e) {		 
	             log.debug("idgateUserFlag error {}",e);		 
	         }		 
	         model.addAttribute("idgateUserFlag",idgateUserFlag);
			log.info("order_renewal_step1 getbsData>> {}", bs.getData());

		}
		catch (Exception e)
		{
			log.error("order_renewal_step1 Error", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = JSPFOLDERPATH + "/order_renewal_step1";
				model.addAttribute("order_renewal_step1", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * ********************N078臺幣定存單到期續存確認頁********************
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/order_renewal_confirm")
	public String order_renewal_confirm(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("order_renewal_confirm Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		String jsondc = "";
		try
		{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam), "UTF-8");
			log.trace(ESAPIUtil.vaildLog("renewal_apply_confirm.jsondc >> " + jsondc));
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
			}
			else
			{
				// 取得前一頁的資料
				bs = acct_Tdeposit_Service.order_renewal_confirm(okMap);
				// 防止重送代碼TxToken
				bs.addData("TXTOKEN", ((Map<String, String>) acct_Tdeposit_Service.getTxToken().getData()).get("TXTOKEN"));
				// IKEY要使用的JSON:DC
				bs.addData("jsondc", jsondc);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
				log.debug("N078 confirmdata >>"+CodeUtil.toJson(bs.getData()));
				// IDGATE transdata            		 
	            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
	    		Map<String,String>datamap=(Map<String, String>) bs.getData();
	            String adopid = "N078";
	    		String title = "您有一筆-臺幣定存單到期續存交易待確認,金額"+datamap.get("AMTFDP");
	        	if(IdgateData != null) {
		            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
		            if(IdgateData.get("idgateUserFlag").equals("Y")) {
		            	datamap.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
		            	datamap.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
						Map<String,Object>tmp=idgateservice.coverTxnData(N078_IDGATE_DATA.class, N078_IDGATE_DATA_VIEW.class, datamap);
						Map<String, String> txnData=(Map<String, String>) tmp.get("txnData");
						if(!StringUtil.isEmpty(txnData.get("AMTFDP"))) {
							// 存單金額格式化
							String aMTFDP=txnData.get("AMTFDP");
							DecimalFormat df = new DecimalFormat("#,##0.00");
							Double doubleAmtfdp = Double.valueOf(aMTFDP);
							log.debug("idgate AMTFDP >>"+df.format(doubleAmtfdp));
							txnData.put("AMTFDP",df.format(doubleAmtfdp));
							tmp.put("txnData",txnData);
						}
						log.debug("idgate transdata >>"+CodeUtil.toJson(tmp));
						IdgateData.put(adopid + "_IDGATE",tmp);
						SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
		            }
	        	}
	            model.addAttribute("idgateAdopid", adopid);
	            model.addAttribute("idgateTitle", title);
			}
		}
		catch (Exception e)
		{
			log.error("order_renewal_confirm Error", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = JSPFOLDERPATH + "/order_renewal_confirm";
				model.addAttribute("order_renewal_confirm", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * ********************N078臺幣定存單到期續存結果頁 ********************
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/order_renewal_result")
	public String order_renewal_result(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("order_renewal_result Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		String pageToken = "";
		try
		{

			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			//Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI

			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
			}
			else
			{
				// Get Session Value
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
				String SessionFinshToken = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null);
				// Get page Value
				pageToken = reqParam.get("TXTOKEN");
				BaseResult checkToken = acct_Tdeposit_Service.validateToken(pageToken, SessionFinshToken);
				if (!checkToken.getResult())
				{
					bs = checkToken;
				}
				else
				{				
					//IDgate驗證
					try {
						if(okMap.get("FGTXWAY").equals("7")) {
	        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
	        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N078_IDGATE");
	                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
	                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
	                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));	
							log.debug("IDGATE resultdata >>"+CodeUtil.toJson(okMap));
						}
					}catch(Exception e) {
						log.error("IDGATE ValidateFail>>{}",bs.getResult());
					}
					Map<String,Object>IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
					model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
					
					// Call order_renewal_result
					bs = acct_Tdeposit_Service.order_renewal_result(cusidn, okMap);
					// 將查過的資料放入session，待切換 locale 時讀取
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				}
				log.trace("bs.getResult>> {}", bs.getResult());
			}
		}
		catch (Exception e)
		{
			log.error("order_renewal_result Error", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, pageToken);
				model.addAttribute("order_renewal_result", bs);
				target = JSPFOLDERPATH + "/order_renewal_result";
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * N079臺幣綜存定存解約進入頁!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/deposit_cancel")
	public String deposit_cancel(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("deposit_cancel Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		try
		{
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			// Get session value by session Key
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			// 取得解約清單
			bs = acct_Tdeposit_Service.deposit_cancel(cusidn, okMap);
			
	        //IDGATE身分
	        String idgateUserFlag="N";		 
	        Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
	        try {		 
	       	   if(IdgateData==null) {
	       		   IdgateData = new HashMap<String, Object>();
	       	   }
	            BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
	            if(tmp.getResult()) {		 
	                idgateUserFlag="Y";                  		 
	            }		 
	            tmp.addData("idgateUserFlag",idgateUserFlag);		 
	            IdgateData.putAll((Map<String, String>) tmp.getData());
	            SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	        }catch(Exception e) {		 
	            log.debug("idgateUserFlag error {}",e);		 
	        }		 
	        model.addAttribute("idgateUserFlag",idgateUserFlag);
		}
		catch (Exception e)
		{
			log.error("renewal_apply Error ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				model.addAttribute("deposit_cancel", bs);
				target = JSPFOLDERPATH + "/deposit_cancel";
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * ********************N079臺幣綜存定存解約確認頁********************
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/deposit_cancel_confirm")
	public String deposit_cancel_confirm(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("deposit_cancel_confirm Start ~~~~~~~~~~~~~~~~");
		String jsondc = "";
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		try
		{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam), "UTF-8");
			log.trace(ESAPIUtil.vaildLog("deposit_cancel_confirm.jsondc >> " + jsondc));
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
			}
			else
			{
				// Get session value by session Key
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
				// 取得解約資料
				bs = acct_Tdeposit_Service.deposit_cancel_confirm(cusidn, okMap);
				// 防止重送代碼TxToken
				bs.addData("TXTOKEN", ((Map<String, String>) acct_Tdeposit_Service.getTxToken().getData()).get("TXTOKEN"));
				// IKEY要使用的JSON:DC
				bs.addData("jsondc", jsondc);
				log.trace("deposit_cancel_confirm bs.getData {}", bs.getData());
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
			}
			// IDGATE transdata            		 
            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
            Map<String, String> result =new HashMap<>();
            result.putAll((Map<String, String>) bs.getData());
            String adopid = "N079";
    		String title = "您有一筆-臺幣綜存定存解約交易待確認,金額"+result.get("AMT")+"元";
        	if(IdgateData != null) {
	            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
	            if(IdgateData.get("idgateUserFlag").equals("Y")) {
	            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
	            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
	            	result.put("AMT",StrUtil.trim(result.get("AMT").toString()).replaceAll(",", ""));
	            	result.put("DPISDT",StrUtil.trim(result.get("DPISDT").toString()).replaceAll(",", ""));
	            	result.put("DUEDAT",StrUtil.trim(result.get("DUEDAT").toString()).replaceAll(",", ""));
					IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N079_IDGATE_DATA.class, N079_IDGATE_DATA_VIEW.class, result));
	                log.trace("bsdata >>"+CodeUtil.toJson(bs.getData()));
	                log.trace("idgatedata >>"+CodeUtil.toJson(result));
					SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	            }
        	}
            model.addAttribute("idgateAdopid", adopid);
            model.addAttribute("idgateTitle", title);
		}
		catch (Exception e)
		{
			log.error("deposit_cancel_confirm Error ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				model.addAttribute("deposit_cancel_confirm", bs);
				target = JSPFOLDERPATH + "/deposit_cancel_confirm";
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * ********************N079臺幣綜存定存解約結果頁********************
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/deposit_cancel_result")
	public String deposit_cancel_result(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("deposit_cancel_result Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		String pagToken = "";
		try
		{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			//Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
			}
			else
			{
				// Get Session Value
				String dpMyEmail = (String) SessionUtil.getAttribute(model, SessionUtil.DPMYEMAIL, null);
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
				// Check Session Token
				String SessionFinshToken = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null);
				pagToken = okMap.get("TXTOKEN");
				BaseResult checkToken = acct_Tdeposit_Service.validateToken(pagToken, SessionFinshToken);
				if (!checkToken.getResult())
				{
					bs = checkToken;
				}
				else
				{
	                //IDgate驗證		 
	                try {		 
	                    if(okMap.get("FGTXWAY").equals("7")) {		 

	        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
	        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N079_IDGATE");
	                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
	                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
	                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
	                    }		 
	                }catch(Exception e) {		 
	                    log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
	                }  
					Map<String,Object>IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
					model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
					
					// Call Cancel Result
					bs = acct_Tdeposit_Service.deposit_cancel_result(cusidn, dpMyEmail, okMap);
					// 將查過的資料放入session，待切換 locale 時讀取
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				}
			}
		}
		catch (Exception e)
		{
			log.error("deposit_cancel_result Error ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, pagToken);
				model.addAttribute("deposit_cancel_result", bs);
				target = JSPFOLDERPATH + "/deposit_cancel_result";
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/***
	 * N076臺幣零存整付按月繳存進入頁!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/installment_saving")
	public String installment_saving(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("installment_saving Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		try
		{
			log.debug(ESAPIUtil.vaildLog("installment_saving req >> " + CodeUtil.toJson(reqParam)));
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			bs = acct_Tdeposit_Service.installment_saving(cusidn);
			log.trace("bs.getResult>> {}", bs.getResult());

			if (null != okMap.get("ACN"))
			{
				bs.addAllData(okMap);
			}
	        //IDGATE身分
	        String idgateUserFlag="N";		 
	        Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
	        try {		 
	      	    if(IdgateData==null) {
	       		    IdgateData = new HashMap<String, Object>();
	       	    }
	            BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
	            if(tmp.getResult()) {		 
	                  idgateUserFlag="Y";                  		 
	            }		 
	            tmp.addData("idgateUserFlag",idgateUserFlag);		 
	            IdgateData.putAll((Map<String, String>) tmp.getData());
	            SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	        }catch(Exception e) {		 
	            log.debug("idgateUserFlag error {}",e);		 
	        }		 
	        model.addAttribute("idgateUserFlag",idgateUserFlag);
		}
		catch (Exception e)
		{
			log.error("deposit_transfer Error ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				model.addAttribute("installment_saving", bs);
				target = JSPFOLDERPATH + "/installment_saving";
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/***
	 * ********************N076臺幣零存整付按月繳存確認頁*********************
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/installment_saving_confirm")
	public String installment_saving_confirm(@RequestParam Map<String, String> reqParam, Model model)
	{

		log.trace("installment_saving_confirm Start ~~~~~~~~~~~~~~~~");
		String jsondc = "";
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		try
		{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam), "UTF-8");
			log.trace(ESAPIUtil.vaildLog("deposit_cancel_confirm.jsondc >> " + jsondc));
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
			}
			else
			{
				// 解決Trust Boundary Violation
				// 把輸入頁的值轉換到bs
				bs = acct_Tdeposit_Service.installment_saving_confirm(okMap);
				// 防止重送代碼TxToken
				bs.addData("TXTOKEN", ((Map<String, String>) acct_Tdeposit_Service.getTxToken().getData()).get("TXTOKEN"));
				// IKEY要使用的JSON:DC
				bs.addData("jsondc", jsondc);
				log.trace("installment_saving_confirm bs.getData {}", bs.getData());
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
				// IDGATE transdata            		 
				Map<String, String> result=(Map<String, String>) bs.getData();
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
				result.put("OUTACN", result.get("ACNO"));
				result.put("CUSIDN", cusidn);
				Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
	    		String adopid = "N076";
	    		String title = "您有一筆,臺幣零存整付按月繳存交易待確認,金額"+result.get("AMOUNT");
	        	if(IdgateData != null) {
		            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
		            if(IdgateData.get("idgateUserFlag").equals("Y")) {
		            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
		            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
						IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N076_IDGATE_DATA.class, N076_IDGATE_DATA_VIEW.class, result));
		                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
		            }
	        	}
	            model.addAttribute("idgateAdopid", adopid);
	            model.addAttribute("idgateTitle", title);
			}
		}
		catch (Exception e)
		{
			log.error("deposit_transfer Error ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				model.addAttribute("installment_saving_confirm", bs);
				target = JSPFOLDERPATH + "/installment_saving_confirm";
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/***
	 * ********************N076臺幣零存整付按月繳存結果頁*********************
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/installment_saving_result")
	public String installment_saving_result(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("installment_saving_result Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		String pageToken = "";
		try
		{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			//Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
			}
			else
			{
				// Get Session Value
				String dpMyEmail = (String) SessionUtil.getAttribute(model, SessionUtil.DPMYEMAIL, null);
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
				// Check TXToken
				String SessionFinshToken = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null);
				pageToken = okMap.get("TXTOKEN");
				BaseResult checkToken = acct_Tdeposit_Service.validateToken(pageToken, SessionFinshToken);
				if (!checkToken.getResult())
				{
					bs = checkToken;
				}
				else
				{
	                //IDgate驗證		 
	                try {		 
	                    if(okMap.get("FGTXWAY").equals("7")) {
	        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
	        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N076_IDGATE");
	        				log.trace("IDGATE TRANSDATA >>"+CodeUtil.toJson(IdgateData));
	        				okMap.put("sessionID", (String)IdgateData.get("sessionid"));	 
	                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
	                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
	                    }		 
	                }catch(Exception e) {		 
	                    log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
	                } 
	                
					Map<String,Object>IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
					model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
	                
					// Call installment_saving Result
					bs = acct_Tdeposit_Service.installment_saving_result(cusidn, dpMyEmail, okMap);
					// 將查過的資料放入session，待切換 locale 時讀取
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				}
			}
		}
		catch (Exception e)
		{
			log.error("deposit_transfer Error ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				// session add TxToken
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, pageToken);
				model.addAttribute("installment_saving_result", bs);
				target = JSPFOLDERPATH + "/installment_saving_result";
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
}
