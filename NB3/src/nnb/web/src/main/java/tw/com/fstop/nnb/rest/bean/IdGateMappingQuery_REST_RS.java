package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class IdGateMappingQuery_REST_RS extends BaseRestBean implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 5378967423391302704L;
	private String MSGCOD;
	@JsonProperty
	IdGateMappingQuery_REST_RSDATA data;


	public String getMSGCOD() {
		return MSGCOD;
	}

	public void setMSGCOD(String mSGCOD) {
		MSGCOD = mSGCOD;
	}

	public IdGateMappingQuery_REST_RSDATA getData() {
		return data;
	}

	public void setData(IdGateMappingQuery_REST_RSDATA data) {
		this.data = data;
	}


}
