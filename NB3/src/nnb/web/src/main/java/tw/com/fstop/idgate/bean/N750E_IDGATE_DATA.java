package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class N750E_IDGATE_DATA implements Serializable{

	//0：非約定，1：約定
	@SerializedName(value = "FLAG")
	private String FLAG;
	//轉出帳號
	@SerializedName(value = "OUTACN")
	private String OUTACN;
	//條碼一
	@SerializedName(value = "BARCODE1")
	private String BARCODE1;
	//條碼二
	@SerializedName(value = "BARCODE2")
	private String BARCODE2;
	//條碼三
	@SerializedName(value = "BARCODE3")
	private String BARCODE3;
	//繳款金額
	@SerializedName(value = "AMOUNT")
	private String AMOUNT;
	public String getFLAG() {
		return FLAG;
	}
	public void setFLAG(String fLAG) {
		FLAG = fLAG;
	}
	public String getOUTACN() {
		return OUTACN;
	}
	public void setOUTACN(String oUTACN) {
		OUTACN = oUTACN;
	}
	public String getBARCODE1() {
		return BARCODE1;
	}
	public void setBARCODE1(String bARCODE1) {
		BARCODE1 = bARCODE1;
	}
	public String getBARCODE2() {
		return BARCODE2;
	}
	public void setBARCODE2(String bARCODE2) {
		BARCODE2 = bARCODE2;
	}
	public String getBARCODE3() {
		return BARCODE3;
	}
	public void setBARCODE3(String bARCODE3) {
		BARCODE3 = bARCODE3;
	}
	public String getAMOUNT() {
		return AMOUNT;
	}
	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}
	
	
}
