package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * * 外匯線上解款-取得議價部位 
 *
 */
public class F003R_REST_RQ extends BaseRestBean_FX implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8782668314950097519L;

	private String BENACC;// 轉入(解款)帳號

	private String CUSIDN;// 客戶統編

	private String CUSTACC;//// 轉出帳號

	private String RETCCY;//// 轉入(解款)幣別

	private String TXAMT; // 轉出(解款)金額

	private String TXCCY;// 轉出幣別

	//IDGATE
	private String sessionID;
	private String idgateID;
	private String txnID;
	
	public String getCUSTACC()
	{
		return CUSTACC;
	}

	public void setCUSTACC(String cUSTACC)
	{
		CUSTACC = cUSTACC;
	}

	public String getBENACC()
	{
		return BENACC;
	}

	public void setBENACC(String bENACC)
	{
		BENACC = bENACC;
	}

	public String getTXCCY()
	{
		return TXCCY;
	}

	public void setTXCCY(String tXCCY)
	{
		TXCCY = tXCCY;
	}

	public String getRETCCY()
	{
		return RETCCY;
	}

	public void setRETCCY(String rETCCY)
	{
		RETCCY = rETCCY;
	}

	public String getTXAMT()
	{
		return TXAMT;
	}

	public void setTXAMT(String tXAMT)
	{
		TXAMT = tXAMT;
	}

	public String getCUSIDN()
	{
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN)
	{
		CUSIDN = cUSIDN;
	}

	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public String getIdgateID() {
		return idgateID;
	}

	public void setIdgateID(String idgateID) {
		this.idgateID = idgateID;
	}

	public String getTxnID() {
		return txnID;
	}

	public void setTxnID(String txnID) {
		this.txnID = txnID;
	}

}
