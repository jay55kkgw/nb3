package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.hsqldb.lib.StringUtil;

import com.google.gson.annotations.SerializedName;


public class N175_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5370773529503299761L;
	
	@SerializedName(value = "SHOW_TRDATE")
	private String SHOW_TRDATE;
	@SerializedName(value = "ACN")
	private String ACN;
	@SerializedName(value = "FDPNUM")
	private String FDPNUM;
	@SerializedName(value = "CUID")
	private String CUID;
	@SerializedName(value = "SHOW_AMT")
	private String SHOW_AMT;
	
	@SerializedName(value = "SHOW_INTMTH")
	private String SHOW_INTMTH;
	@SerializedName(value = "SHOW_DPISDT")
	private String SHOW_DPISDT;
	@SerializedName(value = "SHOW_DUEDAT")
	private String SHOW_DUEDAT;
	@SerializedName(value = "SHOW_INT")
	private String SHOW_INT;
	@SerializedName(value = "SHOW_TAX")
	private String SHOW_TAX;
	
	@SerializedName(value = "SHOW_PAIAFTX")
	private String SHOW_PAIAFTX;
	@SerializedName(value = "SHOW_NHITAX")
	private String SHOW_NHITAX;
	@SerializedName(value = "CMTRMEMO")
	private String CMTRMEMO;
	@SerializedName(value = "CMTRMAIL")
	private String CMTRMAIL;
	@SerializedName(value = "CMMAILMEMO")
	private String CMMAILMEMO;
	
	@Override
	public Map<String, String> coverMap(){
		LinkedHashMap<String, String> result = new LinkedHashMap<String, String>();
		result.put("交易名稱", "外幣綜存定存解約");
		result.put("轉帳日期", this.SHOW_TRDATE);
		result.put("帳號", this.ACN);
		result.put("存單號碼", this.FDPNUM);
		result.put("幣別", this.CUID);
		result.put("存單金額", this.SHOW_AMT);
		
		result.put("計息方式", this.SHOW_INTMTH);
		result.put("起存日", this.SHOW_DPISDT);
		result.put("到期日", this.SHOW_DUEDAT);
		result.put("利息", this.SHOW_INT);
		result.put("所得稅", this.SHOW_TAX);
		
		result.put("稅後本息", this.SHOW_PAIAFTX);
		result.put("健保費", this.SHOW_NHITAX);
		if(!StringUtil.isEmpty(this.CMTRMEMO))result.put("交易備註", this.CMTRMEMO);
		if(!StringUtil.isEmpty(this.CMTRMAIL))result.put("電子信箱", this.CMTRMAIL);
		if(!StringUtil.isEmpty(this.CMMAILMEMO))result.put("摘要內容", this.CMMAILMEMO);
		return result;
	}

}
