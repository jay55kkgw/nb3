package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N079_REST_RQ extends BaseRestBean_TW implements Serializable
{

	private static final long serialVersionUID = -3822264867723382287L;
	
	
	private String ISSUER; // 晶片卡發卡行庫

	private String NeedSHA1;

	private String FLAG;// 0：非約定，1：約定

	private String TRANSEQ;// 交易序號

	private String ACNNO;// 晶片卡帳號

	private String ICDTTM;// 晶片卡日期時間

	private String ICSEQ;// 晶片卡序號

	private String ICMEMO;// 晶片卡備註欄

	private String TAC_Length;// TAC DATA Length

	private String TAC;// TAC DATA

	private String TAC_120space;// C DATA Space

	private String TRMID;// 端末設備查核碼

	private String iSeqNo; // iSeqNo

	private String pkcs7Sign;// IKEY

	private String jsondc;// IKEY

	private String PINNEW;// 網路銀行密碼（新）SSL 用

	private String FGTXWAY; // 交易機制 0:SSL ,1:ikey, 2:晶片金融卡 
	
	private String ADOPID;//交易代號
	
	private String CMTRMEMO;//交易備註

	private String CMTRMAIL;//通訊錄

	private String CMMAILMEMO;//摘要內容

	private String CUSIDN;// 統一編號

	private String FDPACN;// 存單帳號

	private String FDPNUM;// 存單號碼

	private String FDPTYPE;// 存單種類

	private String AMT;// 交易金額

	private String ITR;// 利率

	private String INTMTH;// 計息方式

	private String DPISDT;// 起存日

	private String DUEDAT;// 到期日

	private String INTPAY;// 利息

	private String INTPAYS;// 利息正負號

	private String TAX;// 所得稅

	private String INTRCV;// 透支息

	private String PAIAFTX;// 稅後本息

	private String NHITAX;// 健保費


	//IDGATE
	private String sessionID;
	private String idgateID;
	private String txnID;
	
	public String getISSUER()
	{
		return ISSUER;
	}

	public void setISSUER(String iSSUER)
	{
		ISSUER = iSSUER;
	}

	public String getNeedSHA1()
	{
		return NeedSHA1;
	}

	public void setNeedSHA1(String needSHA1)
	{
		NeedSHA1 = needSHA1;
	}

	public String getFLAG()
	{
		return FLAG;
	}

	public void setFLAG(String fLAG)
	{
		FLAG = fLAG;
	}

	public String getTRANSEQ()
	{
		return TRANSEQ;
	}

	public void setTRANSEQ(String tRANSEQ)
	{
		TRANSEQ = tRANSEQ;
	}

	public String getACNNO()
	{
		return ACNNO;
	}

	public void setACNNO(String aCNNO)
	{
		ACNNO = aCNNO;
	}

	public String getICDTTM()
	{
		return ICDTTM;
	}

	public void setICDTTM(String iCDTTM)
	{
		ICDTTM = iCDTTM;
	}

	public String getICSEQ()
	{
		return ICSEQ;
	}

	public void setICSEQ(String iCSEQ)
	{
		ICSEQ = iCSEQ;
	}

	public String getICMEMO()
	{
		return ICMEMO;
	}

	public void setICMEMO(String iCMEMO)
	{
		ICMEMO = iCMEMO;
	}

	public String getTAC_Length()
	{
		return TAC_Length;
	}

	public void setTAC_Length(String tAC_Length)
	{
		TAC_Length = tAC_Length;
	}

	public String getTAC()
	{
		return TAC;
	}

	public void setTAC(String tAC)
	{
		TAC = tAC;
	}

	public String getTAC_120space()
	{
		return TAC_120space;
	}

	public void setTAC_120space(String tAC_120space)
	{
		TAC_120space = tAC_120space;
	}

	public String getTRMID()
	{
		return TRMID;
	}

	public void setTRMID(String tRMID)
	{
		TRMID = tRMID;
	}

	public String getiSeqNo()
	{
		return iSeqNo;
	}

	public void setiSeqNo(String iSeqNo)
	{
		this.iSeqNo = iSeqNo;
	}

	public String getPkcs7Sign()
	{
		return pkcs7Sign;
	}

	public void setPkcs7Sign(String pkcs7Sign)
	{
		this.pkcs7Sign = pkcs7Sign;
	}

	public String getJsondc()
	{
		return jsondc;
	}

	public void setJsondc(String jsondc)
	{
		this.jsondc = jsondc;
	}

	public String getPINNEW()
	{
		return PINNEW;
	}

	public void setPINNEW(String pINNEW)
	{
		PINNEW = pINNEW;
	}

	public String getFGTXWAY()
	{
		return FGTXWAY;
	}

	public void setFGTXWAY(String fGTXWAY)
	{
		FGTXWAY = fGTXWAY;
	}

	public String getCUSIDN()
	{
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN)
	{
		CUSIDN = cUSIDN;
	}

	public String getFDPACN()
	{
		return FDPACN;
	}

	public void setFDPACN(String fDPACN)
	{
		FDPACN = fDPACN;
	}

	public String getFDPNUM()
	{
		return FDPNUM;
	}

	public void setFDPNUM(String fDPNUM)
	{
		FDPNUM = fDPNUM;
	}

	public String getFDPTYPE()
	{
		return FDPTYPE;
	}

	public void setFDPTYPE(String fDPTYPE)
	{
		FDPTYPE = fDPTYPE;
	}

	public String getAMT()
	{
		return AMT;
	}

	public void setAMT(String aMT)
	{
		AMT = aMT;
	}

	public String getITR()
	{
		return ITR;
	}

	public void setITR(String iTR)
	{
		ITR = iTR;
	}

	public String getINTMTH()
	{
		return INTMTH;
	}

	public void setINTMTH(String iNTMTH)
	{
		INTMTH = iNTMTH;
	}

	public String getDPISDT()
	{
		return DPISDT;
	}

	public void setDPISDT(String dPISDT)
	{
		DPISDT = dPISDT;
	}

	public String getDUEDAT()
	{
		return DUEDAT;
	}

	public void setDUEDAT(String dUEDAT)
	{
		DUEDAT = dUEDAT;
	}

	public String getINTPAY()
	{
		return INTPAY;
	}

	public void setINTPAY(String iNTPAY)
	{
		INTPAY = iNTPAY;
	}

	public String getINTPAYS()
	{
		return INTPAYS;
	}

	public void setINTPAYS(String iNTPAYS)
	{
		INTPAYS = iNTPAYS;
	}

	public String getTAX()
	{
		return TAX;
	}

	public void setTAX(String tAX)
	{
		TAX = tAX;
	}

	public String getINTRCV()
	{
		return INTRCV;
	}

	public void setINTRCV(String iNTRCV)
	{
		INTRCV = iNTRCV;
	}

	public String getPAIAFTX()
	{
		return PAIAFTX;
	}

	public void setPAIAFTX(String pAIAFTX)
	{
		PAIAFTX = pAIAFTX;
	}

	public String getNHITAX()
	{
		return NHITAX;
	}

	public void setNHITAX(String nHITAX)
	{
		NHITAX = nHITAX;
	}

	public String getADOPID()
	{
		return ADOPID;
	}

	public void setADOPID(String aDOPID)
	{
		ADOPID = aDOPID;
	}

	public String getCMTRMEMO()
	{
		return CMTRMEMO;
	}

	public void setCMTRMEMO(String cMTRMEMO)
	{
		CMTRMEMO = cMTRMEMO;
	}

	public String getCMTRMAIL()
	{
		return CMTRMAIL;
	}

	public void setCMTRMAIL(String cMTRMAIL)
	{
		CMTRMAIL = cMTRMAIL;
	}

	public String getCMMAILMEMO()
	{
		return CMMAILMEMO;
	}

	public void setCMMAILMEMO(String cMMAILMEMO)
	{
		CMMAILMEMO = cMMAILMEMO;
	}

	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public String getIdgateID() {
		return idgateID;
	}

	public void setIdgateID(String idgateID) {
		this.idgateID = idgateID;
	}

	public String getTxnID() {
		return txnID;
	}

	public void setTxnID(String txnID) {
		this.txnID = txnID;
	}

}
