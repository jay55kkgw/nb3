package tw.com.fstop.nnb.aop;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.validation.support.BindingAwareModelMap;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.service.TxnCsssLogService;
import tw.com.fstop.tbb.nnb.dao.SysParamDao;
import tw.com.fstop.tbb.nnb.dao.TxnCsssLogDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.web.util.StrUtils;

@Aspect
@Component
public class DialogAop {

	static Logger log = LoggerFactory.getLogger(DialogAop.class);

	@Autowired
	private TxnCsssLogService txnCsssLogService;

	@Autowired
	private SysParamDao sysparamdao;

	@Autowired
	ArrayList<String> txnLogList;

	private String sdatekey = "TxnCsssLogMailStartDate";
	private String edatekey = "TxnCsssLogMailEndDate";
	private String isOpenkey = "isOpenCsssLog";

	@Value("${SKIP.Dialog:true}")
	private boolean skipDialog;

	private String sdate;
	private String edate;
	private String isOpen;
	
	@Pointcut("@annotation(tw.com.fstop.nnb.aop.Dialog)")
	public void Dialog() {

	}

	/*
	 * 1.判斷 是否啟用 
	 * 2.判斷 時間區間
	 * 3.取session (model) 來決定flag 前端是否include
	 * */
	@After("Dialog()")
	public void doAroundController(JoinPoint joinPoint) {
		 sdate = sysparamdao.getSysParam(this.sdatekey);
		 edate = sysparamdao.getSysParam(this.edatekey);
		 isOpen = sysparamdao.getSysParam(this.isOpenkey);
		 // Stored Log Forging
//		 log.info("sdate = " + sdate);
//		 log.info("edate = " + edate);
//		 log.info("isOpen = " + isOpen);
		Date today = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MMdd");
		String todayMD = sdf.format(today);
		
		Boolean flag = false;
		if("Y".equals(isOpen)) {
			int isdate = Integer.valueOf(sdate);
			int iedate = Integer.valueOf(edate);
			int itodayMD = Integer.valueOf(todayMD);
			if(isdate > iedate) {
				if(0101<=itodayMD && itodayMD<=iedate && isdate<=itodayMD && itodayMD<=1231) {
					flag = true;
				}
			}
			else {
				if(itodayMD<=iedate && isdate<=itodayMD ) {
					flag = true;
				}
			}
		}
		 log.info("flag = " + flag);
		if(flag == true) {
			try {
				BindingAwareModelMap model = new BindingAwareModelMap();
				 log.info("getArgs = " + joinPoint.getArgs().toString());
				for (Object o : joinPoint.getArgs()) {
					if (BindingAwareModelMap.class.equals((o.getClass()))) {
						 log.info("o = " + o.toString());
						model = (BindingAwareModelMap) o;
						break;
					}
				}
				String cusidn = String.valueOf(SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null));
				log.info("cusidn = " + cusidn);
				if(StrUtils.isEmpty(cusidn)) {
					throw new Throwable("cusidn為空");
				}
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				
				if (txnCsssLogService.isFill(cusidn)) {
					MethodSignature signature = (MethodSignature) joinPoint.getSignature();
					Method method = signature.getMethod();
					Dialog dialog = method.getAnnotation(Dialog.class);
					String ADOPID = dialog.ADOPID();
					log.debug("ADOPID>>>{}", ADOPID);
					model.addAttribute("AopAdopid", ADOPID);
					model.addAttribute("showDialog", "true");
				}
			}catch (Throwable e) {
				log.error("error>>>>{}", e);
			}
		}
		
		
		
		//if (skipDialog) {
//			try {
//				BindingAwareModelMap model = new BindingAwareModelMap();
//				for(Object o :joinPoint.getArgs()) {
//					if(BindingAwareModelMap.class.equals((o.getClass()))) {
//						model = (BindingAwareModelMap) o;
//						break;
//					}
//				}
				
//				if (!model.containsKey("error")) {
//					String cusidn = String.valueOf(SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null));
//					cusidn = new String(Base64.getDecoder().decode(cusidn));
//					BaseResult bs = new BaseResult();

//					if (txnCsssLogService.isFill(cusidn)) {
//						MethodSignature signature = (MethodSignature) joinPoint.getSignature();
//						Method method = signature.getMethod();
//						Dialog dialog = method.getAnnotation(Dialog.class);
//
//						String ADOPID = dialog.ADOPID();
//						log.debug("ADOPID>>>{}", ADOPID);
//						model.addAttribute("AopAdopid", ADOPID);
//						Map<String, String> map = new HashMap<String, String>();
//						String key = "";
//						Boolean isDialog = false;
//					}
						//bs = (BaseResult) model.get(key);
						//if ("0".equals(bs.getMsgCode())) isDialog = true;
						
						//改為在頁面上塞
//						switch (ADOPID) {
//						case "N130":
//							key = "demand_deposit_detail_result";
//							bs = (BaseResult) model.get(key);
//							if ("0".equals(bs.getMsgCode())) isDialog = true;
//							break;
//						case "N520":
//							key = "demand_deposit_result";
//							bs = (BaseResult) model.get(key);
//							if ("0".equals(bs.getMsgCode())) isDialog = true;
//							break;
//						case "N070":
//							/*
//							 * 參照/acct/transfer_result頁面邏輯
//							 * <!--                         預約才有轉帳日期 -->
//							 *<c:if test="${transfer_result_data.data.FGTXDATE != '1'}">  
//							*/
//							key = "transfer_result_data";
//							bs = (BaseResult) model.get(key);
//							map = (Map<String, String>) bs.getData();
//							if ("0".equals(map.get("FGTXDATE"))) isDialog = true;
//							break;
//						case "N074":
//							/*
//							 * 參照/acct/deposit_transfer_result頁面邏輯
//							 * <!-- 即時交易結果 -->
//							 *<c:if test="${transfer_result_data.data.FGTXDATE != '1'}">  
//							*/
//							key = "deposit_transfer_result";
//							bs = (BaseResult) model.get(key);
//							map = (Map<String, String>) bs.getData();
//							if ("0".equals(map.get("FGTXDATE"))) isDialog = true;
//							break;
//						case "N3003":
//							/*
//							 * 參照/loan_advance/repay_advance_result頁面邏輯
//							 *<c:if test="${ inputData.FGTXDATE.equals('1') }">  
//							*/
//							key = "inputData";
//							map = (Map<String, String>) model.get(key);
//							if ("0".equals(map.get("FGTXDATE"))) isDialog = true;
//							break;
//						case "F001":
//							key = "transfer_result_data";
//							map = (Map<String, String>) model.get(key);
//							bs = (BaseResult) model.get(key);
//							if ("0".equals(bs.getMsgCode())) isDialog = true;
//							break;
//							/*
//							 * 		<!-- 即時交易結果 -->
//							 *	<c:if test="${BaseResultData.FGTRDATE == '0'}">	
//							 * */
//						case "F002":
//							key = "f_outward_remittances_result";
//							bs = (BaseResult) model.get(key);
//							map = (Map<String, String>) bs.getData();
//							if ("0".equals(bs.getMsgCode())) isDialog = true;
//							break;
//							
//						case "N174":
//							key = "f_deposit_transfer_result";
//							break;
//						case "C012":
//							key = "bs";
//							break;
//						case "B012":
//							key = "oversea_balance_result";
//							break;
//						case "C016":
//							key = "result_locale_data";
//							break;
//						case "C017":
//							key = "null";
//							break;
//						case "C021":
//							key = "result_data";
//							break;
//						case "C024":
//							key = "bs";
//							break;
//						}
						
//						if(isDialog) {
//							model.addAttribute("showDialog", "true");
//						}
						
//						if (!"null".equals(key)) {
//							bs = (BaseResult) model.get(key);
//						}
						// 取MsgCode 如果為0(成功) 帶上跳Dialog的參數
//						log.debug("MsgCode>>>{}", bs.getMsgCode());
//						if ("0".equals(bs.getMsgCode()) || ADOPID == "C017") {
//							model.addAttribute("showDialog", "true");
//						}
//					}
//				}

//			} catch (Throwable e) {
//				log.error("error>>>>{}", e);
//			}
//		}
	}
}
