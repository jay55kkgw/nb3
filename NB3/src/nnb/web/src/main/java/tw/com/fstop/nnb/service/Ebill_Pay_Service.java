package tw.com.fstop.nnb.service;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.tbb.nnb.util.DateUtil;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;

@Service
public class Ebill_Pay_Service extends Base_Service
{

	Logger log = LoggerFactory.getLogger(this.getClass());

	/**
	 * 繳款單掃QRCODE- 全國性繳費 進入頁
	 * 
	 * 
	 * @param reqParam
	 * @return
	 */
	public BaseResult cclifepayment(Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{

			// CARDNUM=263377200059782&PAYAMT=1000&LPAYAMT=200

			String cardnum = reqParam.get("CARDNUM");// 信用卡卡號（銷帳編號)

			String payamt = reqParam.get("PAYAMT");// 本期應繳金額

			String lpayamt = reqParam.get("LPAYAMT");// 最低應繳金額

			bs.addData("CARDNUM", cardnum);
			bs.addData("PAYAMT", payamt);
			bs.addData("LPAYAMT", lpayamt);
			bs.setResult(true);

			if (bs != null && bs.getResult())
			{

			}
			log.trace("cclifepayment bs.getData()>> {}", bs.getData());
		}
		catch (Exception e)
		{
			log.error("cclifepayment  error : {}", e);
		}
		return bs;
	}

	/**
	 * 繳款單掃QRCODE- 全國性繳費 輸入
	 * 
	 * 
	 * @param {"PAYAMT":"1000","CARDNUM":263377200059782","LPAYAMT":"200"}
	 * @return
	 */
	public BaseResult cclifepayment_step1(Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{
			log.debug(ESAPIUtil.vaildLog("cclifepayment_step1 >>>> {}"+CodeUtil.toJson(reqParam)));
			bs.addAllData(reqParam);
			bs.setResult(true);
			if (bs != null && bs.getResult())
			{

			}
			log.trace("cclifepayment_step1 bs.getData()>> {}", bs.getData());
		}
		catch (Exception e)
		{
			log.error("cclifepayment  error : {}", e);
		}
		return bs;
	}

	/**
	 * 繳款單掃QRCODE- 全國性繳費 確認
	 * @param reqParam {"bankid":"","trin_acn":"(轉出帳號)", "PAYAMT":"(應繳金額)" , "ipayamt"="(繳款金額)", "BANKNAME":"050-臺灣企銀", "LPAYAMT":"(最低應繳)", "CARDNUM:"信用卡號", "CustEmail":"test@gmail.com", "CUSIDN":}
	 * @return
	 */
	public BaseResult cclifepayment_confirm(Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{
			log.debug(ESAPIUtil.vaildLog("cclifepayment_confirm >>>> {}" + CodeUtil.toJson(reqParam)));
			reqParam.put("AMOUNT", reqParam.get("PAYAMT"));
			bs = QR10_REST(reqParam);
			if (bs != null && bs.getResult())
			{
				bs.addAllData(reqParam);
			}
			log.debug("cclifepayment bs.getData()>> {}", bs.getData());
		}
		catch (Exception e)
		{
			log.error("cclifepayment  error : {}", e);
		}
		return bs;
	}

	/**
	 * 繳款單掃QRCODE- 全國性繳費 結果
	 * 
	 * @param reqParam
	 * @return
	 */
	public BaseResult cclifepayment_result(Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{
			bs = Q072_REST(reqParam);

			if (bs != null && bs.getResult())
			{
				// 交易時間
				String datetime = DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss");
				reqParam.put("DATETIME", datetime);
				
				bs.addAllData(reqParam);
			}
			log.trace("cclifepayment bs.getData()>> {}", bs.getData());
		}
		catch (Exception e)
		{
			log.error("cclifepayment  error : {}", e);
		}
		return bs;
	}

	public BaseResult taiwaterpayment_comfirm(Map<String, String> reqParam)
	{
		BaseResult bs = null;
		try
		{
			bs = new BaseResult();
			bs = EBPay1_REST(reqParam);
			if (bs != null)
			{
				Map<String, Object> callData = (Map<String, Object>) bs.getData();

				if ( reqParam.get("WAT_NO").length() > 0 && ((String) callData.get("VrfyMacRslt")).equals("Y")
						&& ((String) callData.get("TrnsCode")).equals("0000")
						&& ((String) callData.get("Rcode")).equals("0001"))
				{
					bs.setResult(true);
				}
				else if (!((String) callData.get("VrfyMacRslt")).equals("Y")
						|| !((String) callData.get("TrnsCode")).equals("0000")
						|| !((String) callData.get("Rcode")).equals("0001"))
				{
					if (!((String) callData.get("VrfyMacRslt")).equals("Y"))
					{
						bs.setMessage("0302", "押碼錯誤");
					}
					if (!((String) callData.get("TrnsCode")).equals("0000")
							&& ((String) callData.get("Rcode")).length() == 0)
					{
						bs.setMessage("ZX99", "交易異常錯誤，請重新操作");
					}
					if (((String) callData.get("VrfyMacRslt")).equals("Y")
							&& !((String) callData.get("Rcode")).equals("0001"))
					{
						bs.setMessage((String) callData.get("Rcode"), (String) callData.get("RDESC"));
					}
					bs.setResult(false);
				}
				else
				{
					bs.setMessage("", "您未按照正常流程進入此網頁<br>請重新操作");
					bs.setResult(false);
				}
			}
		}
		catch (Exception e)
		{
			log.error(e.toString());
		}
		return bs;
	}
	public BaseResult taiwaterpayment_result(Map<String, String> reqParam)
	{

		BaseResult bs = null;
		try
		{
			bs = new BaseResult();
			bs = EBPay1_1_REST(reqParam);
			if (bs != null)
			{
				Map<String, Object> callData = (Map<String, Object>) bs.getData();
				
				if(!callData.containsKey("occurMSG")) {
					callData.put("occurMSG", "");
				}
				if(!callData.containsKey("occurMSG1")) {
					callData.put("occurMSG1", "");
				}
				if (((String) callData.get("occurMSG")).length() > 0 && ((String) callData.get("occurMSG")).equals("0000"))
				{
					bs.setResult(true);
				}
				else if ( ( ((String) callData.get("occurMSG")).length() == 0 && ((String) callData.get("occurMSG1")).length() > 0 ) || 
						  ( ((String) callData.get("occurMSG1")).length() == 0 && ((String) callData.get("occurMSG")).length() > 0 ))
				{
					if(((String) callData.get("occurMSG")).length() == 0 && ((String) callData.get("occurMSG1")).length() > 0) {
						bs.setMessage((String)callData.get("occurMSG1"), (String)callData.get("occurMSG1DESC"));
					}
					if(((String) callData.get("occurMSG1")).length() == 0 && ((String) callData.get("occurMSG")).length() > 0) {
						bs.setMessage((String) callData.get("occurMSG"), getMessageByMsgCode((String) callData.get("occurMSG")));
					}
					bs.setResult(false);
				}
				else
				{
					bs.setMessage("", "您未按照正常流程進入此網頁<br>請重新操作");
					bs.setResult(false);
				}
			}
		}
		catch (Exception e)
		{
			log.error(e.toString());
		}
		return bs;
	}
}
