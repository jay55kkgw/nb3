package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N203_REST_RQ extends BaseRestBean_OLA implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5742492262962140958L;
	private String DATE;//日期YYYMMDD
	private String TIME;//時間HHMMSS
	private String UID;//統一編號
	private String CUSIDN;//統一編號
	private String ACN;//預備開戶帳號
	private String NAME;//戶名
	private String BIRTHDAY;//出生日期
	private String POSTCOD1;//戶籍地郵遞區號
	private String PMTADR;//戶籍地址
	private String POSTCOD2;//通訊地郵遞區號
	private String CTTADR;//通訊地址
	private String ARACOD1;//連絡電話區域碼
	private String TELNUM1;//連絡電話號碼(住)
	private String ARACOD2;//連絡電話區域碼
	private String TELNUM2;//連絡電話號碼(公)
	private String CELPHONE;//手機號碼
	private String ARACOD3;//傳真區域碼
	private String FAX;//傳真號碼
	private String MAILADDR;//電子郵件地址
	private String MARRY;//婚姻狀況
	private String CHILD;//小孩數
	private String DEGREE;//教育程度
	private String CAREER1;//職業(聯徵)
	private String CAREER2;//職稱
	private String SALARY;//年收入
	private String HAS_PIC;//身分證是否有照片
	private String ID_CHGE;//身分證異動型態
	private String CHGE_DT;//身分證異動日
	private String CHGE_CY;//身分證補換縣市
	private String BRHCOD;
	
	private String CMTRANPAGE;
	private String ISSUER;
	private String ACNNO;
	private String pkcs7Sign;
	private String jsondc;
	private String TRMID;
	private String iSeqNo;
	private String ICSEQ;
	private String TAC;
	private String RENAME;
	private String PURPOSE;
	private String PREASON;
	private String BDEALING;
	private String BREASON;
	private String MAINFUND;
	private String MREASON;
	private String S_MONEY;
	private String FILE1;
	private String FILE2;
	private String FILE3;
	private String FILE4;
	private String USERNAME;
	private String LOGINPIN;
	private String TRANSPIN;
	private String TRFLAG;
	private String CARDAPPLY;
	private String SNDFLG;
	private String CRDTYP;
	private String NAATAPPLY;
	private String CChargeApply;
	private String LMT;
	private String CROSSAPPLY;
	private String FGTXWAY;
	private String IP;
	private String HASRISK;
	private String HOMEZIP;
	private String HOMETEL;
	private String BUSZIP;
	private String BUSTEL;
	private String COUNTRY;
	private String EMPLOYER;
	private String CHIP_ACN;
	private String ACNTYPE;		//數三 帶入 A3
	private String EBILLFLAG;
	private String CUSNAME;//原住民姓名
	private String BANKCODE;
	private String CHECKNB;
	
	private String HLOGINPIN;
	private String HTRANSPIN;
	

	private String CITYCHA;
	private String CMDATE2;
	private String DIGVERSION;
	private String ACNVERSION;
	private String outside_source;
	private String EMPNO;
	private String HIRISK;
	
	public String getHIRISK() {
		return HIRISK;
	}
	public void setHIRISK(String hIRISK) {
		HIRISK = hIRISK;
	}
	public String getEMPNO() {
		return EMPNO;
	}
	public void setEMPNO(String eMPNO) {
		EMPNO = eMPNO;
	}
	public String getOutside_source() {
		return outside_source;
	}
	public void setOutside_source(String outside_source) {
		this.outside_source = outside_source;
	}
	public String getCITYCHA() {
		return CITYCHA;
	}
	public void setCITYCHA(String cITYCHA) {
		CITYCHA = cITYCHA;
	}
	public String getCMDATE2() {
		return CMDATE2;
	}
	public void setCMDATE2(String cMDATE2) {
		CMDATE2 = cMDATE2;
	}
	public String getDIGVERSION() {
		return DIGVERSION;
	}
	public void setDIGVERSION(String dIGVERSION) {
		DIGVERSION = dIGVERSION;
	}
	public String getACNVERSION() {
		return ACNVERSION;
	}
	public void setACNVERSION(String aCNVERSION) {
		ACNVERSION = aCNVERSION;
	}
	public String getHLOGINPIN() {
		return HLOGINPIN;
	}
	public void setHLOGINPIN(String hLOGINPIN) {
		HLOGINPIN = hLOGINPIN;
	}
	public String getHTRANSPIN() {
		return HTRANSPIN;
	}
	public void setHTRANSPIN(String hTRANSPIN) {
		HTRANSPIN = hTRANSPIN;
	}
	public String getCHECKNB() {
		return CHECKNB;
	}
	public void setCHECKNB(String cHECKNB) {
		CHECKNB = cHECKNB;
	}
	public String getBANKCODE() {
		return BANKCODE;
	}
	public void setBANKCODE(String bANKCODE) {
		BANKCODE = bANKCODE;
	}
	public String getCUSNAME() {
		return CUSNAME;
	}
	public void setCUSNAME(String cUSNAME) {
		CUSNAME = cUSNAME;
	}
	public String getEBILLFLAG() {
		return EBILLFLAG;
	}
	public void setEBILLFLAG(String eBILLFLAG) {
		EBILLFLAG = eBILLFLAG;
	}
	public String getCHIP_ACN() {
		return CHIP_ACN;
	}
	public void setCHIP_ACN(String cHIP_ACN) {
		CHIP_ACN = cHIP_ACN;
	}
	public String getACNTYPE() {
		return ACNTYPE;
	}
	public void setACNTYPE(String aCNTYPE) {
		ACNTYPE = aCNTYPE;
	}
	public String getCOUNTRY() {
		return COUNTRY;
	}
	public void setCOUNTRY(String cOUNTRY) {
		COUNTRY = cOUNTRY;
	}
	public String getEMPLOYER() {
		return EMPLOYER;
	}
	public void setEMPLOYER(String eMPLOYER) {
		EMPLOYER = eMPLOYER;
	}
	public String getMAINFUND() {
		return MAINFUND;
	}
	public void setMAINFUND(String mAINFUND) {
		MAINFUND = mAINFUND;
	}
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	public String getBIRTHDAY() {
		return BIRTHDAY;
	}
	public void setBIRTHDAY(String bIRTHDAY) {
		BIRTHDAY = bIRTHDAY;
	}
	public String getPOSTCOD1() {
		return POSTCOD1;
	}
	public void setPOSTCOD1(String pOSTCOD1) {
		POSTCOD1 = pOSTCOD1;
	}
	public String getPMTADR() {
		return PMTADR;
	}
	public void setPMTADR(String pMTADR) {
		PMTADR = pMTADR;
	}
	public String getPOSTCOD2() {
		return POSTCOD2;
	}
	public void setPOSTCOD2(String pOSTCOD2) {
		POSTCOD2 = pOSTCOD2;
	}
	public String getCTTADR() {
		return CTTADR;
	}
	public void setCTTADR(String cTTADR) {
		CTTADR = cTTADR;
	}
	public String getARACOD1() {
		return ARACOD1;
	}
	public void setARACOD1(String aRACOD1) {
		ARACOD1 = aRACOD1;
	}
	public String getTELNUM1() {
		return TELNUM1;
	}
	public void setTELNUM1(String tELNUM1) {
		TELNUM1 = tELNUM1;
	}
	public String getARACOD2() {
		return ARACOD2;
	}
	public void setARACOD2(String aRACOD2) {
		ARACOD2 = aRACOD2;
	}
	public String getTELNUM2() {
		return TELNUM2;
	}
	public void setTELNUM2(String tELNUM2) {
		TELNUM2 = tELNUM2;
	}
	public String getCELPHONE() {
		return CELPHONE;
	}
	public void setCELPHONE(String cELPHONE) {
		CELPHONE = cELPHONE;
	}
	public String getARACOD3() {
		return ARACOD3;
	}
	public void setARACOD3(String aRACOD3) {
		ARACOD3 = aRACOD3;
	}
	public String getFAX() {
		return FAX;
	}
	public void setFAX(String fAX) {
		FAX = fAX;
	}
	public String getMAILADDR() {
		return MAILADDR;
	}
	public void setMAILADDR(String mAILADDR) {
		MAILADDR = mAILADDR;
	}
	public String getMARRY() {
		return MARRY;
	}
	public void setMARRY(String mARRY) {
		MARRY = mARRY;
	}
	public String getCHILD() {
		return CHILD;
	}
	public void setCHILD(String cHILD) {
		CHILD = cHILD;
	}
	public String getDEGREE() {
		return DEGREE;
	}
	public void setDEGREE(String dEGREE) {
		DEGREE = dEGREE;
	}
	public String getCAREER1() {
		return CAREER1;
	}
	public void setCAREER1(String cAREER1) {
		CAREER1 = cAREER1;
	}
	public String getCAREER2() {
		return CAREER2;
	}
	public void setCAREER2(String cAREER2) {
		CAREER2 = cAREER2;
	}
	public String getSALARY() {
		return SALARY;
	}
	public void setSALARY(String sALARY) {
		SALARY = sALARY;
	}
	public String getHAS_PIC() {
		return HAS_PIC;
	}
	public void setHAS_PIC(String hAS_PIC) {
		HAS_PIC = hAS_PIC;
	}
	public String getID_CHGE() {
		return ID_CHGE;
	}
	public void setID_CHGE(String iD_CHGE) {
		ID_CHGE = iD_CHGE;
	}
	public String getCHGE_DT() {
		return CHGE_DT;
	}
	public void setCHGE_DT(String cHGE_DT) {
		CHGE_DT = cHGE_DT;
	}
	public String getCHGE_CY() {
		return CHGE_CY;
	}
	public void setCHGE_CY(String cHGE_CY) {
		CHGE_CY = cHGE_CY;
	}
	public String getBRHCOD() {
		return BRHCOD;
	}
	public void setBRHCOD(String bRHCOD) {
		BRHCOD = bRHCOD;
	}
	public String getCMTRANPAGE() {
		return CMTRANPAGE;
	}
	public void setCMTRANPAGE(String cMTRANPAGE) {
		CMTRANPAGE = cMTRANPAGE;
	}
	public String getISSUER() {
		return ISSUER;
	}
	public void setISSUER(String iSSUER) {
		ISSUER = iSSUER;
	}
	public String getACNNO() {
		return ACNNO;
	}
	public void setACNNO(String aCNNO) {
		ACNNO = aCNNO;
	}
	public String getPkcs7Sign() {
		return pkcs7Sign;
	}
	public void setPkcs7Sign(String pkcs7Sign) {
		this.pkcs7Sign = pkcs7Sign;
	}
	public String getJsondc() {
		return jsondc;
	}
	public void setJsondc(String jsondc) {
		this.jsondc = jsondc;
	}
	public String getTRMID() {
		return TRMID;
	}
	public void setTRMID(String tRMID) {
		TRMID = tRMID;
	}
	public String getiSeqNo() {
		return iSeqNo;
	}
	public void setiSeqNo(String iSeqNo) {
		this.iSeqNo = iSeqNo;
	}
	public String getICSEQ() {
		return ICSEQ;
	}
	public void setICSEQ(String iCSEQ) {
		ICSEQ = iCSEQ;
	}
	public String getTAC() {
		return TAC;
	}
	public void setTAC(String tAC) {
		TAC = tAC;
	}
	public String getRENAME() {
		return RENAME;
	}
	public void setRENAME(String rENAME) {
		RENAME = rENAME;
	}
	public String getPURPOSE() {
		return PURPOSE;
	}
	public void setPURPOSE(String pURPOSE) {
		PURPOSE = pURPOSE;
	}
	public String getPREASON() {
		return PREASON;
	}
	public void setPREASON(String pREASON) {
		PREASON = pREASON;
	}
	public String getBDEALING() {
		return BDEALING;
	}
	public void setBDEALING(String bDEALING) {
		BDEALING = bDEALING;
	}
	public String getBREASON() {
		return BREASON;
	}
	public void setBREASON(String bREASON) {
		BREASON = bREASON;
	}
	public String getMREASON() {
		return MREASON;
	}
	public void setMREASON(String mREASON) {
		MREASON = mREASON;
	}
	public String getS_MONEY() {
		return S_MONEY;
	}
	public void setS_MONEY(String s_MONEY) {
		S_MONEY = s_MONEY;
	}
	public String getFILE1() {
		return FILE1;
	}
	public void setFILE1(String fILE1) {
		FILE1 = fILE1;
	}
	public String getFILE2() {
		return FILE2;
	}
	public void setFILE2(String fILE2) {
		FILE2 = fILE2;
	}
	public String getFILE3() {
		return FILE3;
	}
	public void setFILE3(String fILE3) {
		FILE3 = fILE3;
	}
	public String getFILE4() {
		return FILE4;
	}
	public void setFILE4(String fILE4) {
		FILE4 = fILE4;
	}
	public String getUSERNAME() {
		return USERNAME;
	}
	public void setUSERNAME(String uSERNAME) {
		USERNAME = uSERNAME;
	}
	public String getLOGINPIN() {
		return LOGINPIN;
	}
	public void setLOGINPIN(String lOGINPIN) {
		LOGINPIN = lOGINPIN;
	}
	public String getTRANSPIN() {
		return TRANSPIN;
	}
	public void setTRANSPIN(String tRANSPIN) {
		TRANSPIN = tRANSPIN;
	}
	public String getTRFLAG() {
		return TRFLAG;
	}
	public void setTRFLAG(String tRFLAG) {
		TRFLAG = tRFLAG;
	}
	public String getCARDAPPLY() {
		return CARDAPPLY;
	}
	public void setCARDAPPLY(String cARDAPPLY) {
		CARDAPPLY = cARDAPPLY;
	}
	public String getSNDFLG() {
		return SNDFLG;
	}
	public void setSNDFLG(String sNDFLG) {
		SNDFLG = sNDFLG;
	}
	public String getCRDTYP() {
		return CRDTYP;
	}
	public void setCRDTYP(String cRDTYP) {
		CRDTYP = cRDTYP;
	}
	public String getNAATAPPLY() {
		return NAATAPPLY;
	}
	public void setNAATAPPLY(String nAATAPPLY) {
		NAATAPPLY = nAATAPPLY;
	}
	public String getCChargeApply() {
		return CChargeApply;
	}
	public void setCChargeApply(String cChargeApply) {
		CChargeApply = cChargeApply;
	}
	public String getLMT() {
		return LMT;
	}
	public void setLMT(String lMT) {
		LMT = lMT;
	}
	public String getCROSSAPPLY() {
		return CROSSAPPLY;
	}
	public void setCROSSAPPLY(String cROSSAPPLY) {
		CROSSAPPLY = cROSSAPPLY;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getIP() {
		return IP;
	}
	public void setIP(String iP) {
		IP = iP;
	}
	public String getHASRISK() {
		return HASRISK;
	}
	public void setHASRISK(String hASRISK) {
		HASRISK = hASRISK;
	}
	public String getHOMEZIP() {
		return HOMEZIP;
	}
	public void setHOMEZIP(String hOMEZIP) {
		HOMEZIP = hOMEZIP;
	}
	public String getHOMETEL() {
		return HOMETEL;
	}
	public void setHOMETEL(String hOMETEL) {
		HOMETEL = hOMETEL;
	}
	public String getBUSZIP() {
		return BUSZIP;
	}
	public void setBUSZIP(String bUSZIP) {
		BUSZIP = bUSZIP;
	}
	public String getBUSTEL() {
		return BUSTEL;
	}
	public void setBUSTEL(String bUSTEL) {
		BUSTEL = bUSTEL;
	}
}
