package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;

//第一類至第四類投保單位
public class N8301_1_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5370773529503299761L;
	
	@SerializedName(value = "TSFACN")
	private String TSFACN;
	
	@SerializedName(value = "UNTNUM1")
	private String UNTNUM1;
	
	@SerializedName(value = "CUSIDN1")
	private String CUSIDN1;
	
	@Override
	public Map<String, String> coverMap(){
        LinkedHashMap<String, String> result = new LinkedHashMap<String, String>();
        result.put("交易名稱", "健保費代扣繳申請");
        result.put("交易類型", "申請");
		result.put("扣帳帳號", this.TSFACN);
		result.put("投保單位", this.UNTNUM1);
		result.put("統一編號", this.CUSIDN1);
		return result;
	}

	public String getTSFACN() {
		return TSFACN;
	}

	public void setTSFACN(String tSFACN) {
		TSFACN = tSFACN;
	}
}
