package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 手機門號_綁定查詢
 * 
 * @author Vincenthuang
 *
 */
public class MtpBindQry_REST_RS extends BaseRestBean_QR_RS implements Serializable {

	private String AccountName;		//收款戶名(無遮罩)
	private String Txacn;			//綁定帳號
	
	public String getTxacn() {
		return Txacn;
	}
	public void setTxacn(String txacn) {
		Txacn = txacn;
	}
	public String getAccountName() {
		return AccountName;
	}
	public void setAccountName(String accountName) {
		AccountName = accountName;
	}
	
}
