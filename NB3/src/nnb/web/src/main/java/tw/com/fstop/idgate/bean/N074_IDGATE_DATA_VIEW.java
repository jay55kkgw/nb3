package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;


public class N074_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5370773529503299761L;
	
	@SerializedName(value = "FGTXDATE")
	private String FGTXDATE;
	@SerializedName(value = "transfer_date")//轉帳日期
	private String transfer_date;
	@SerializedName(value = "CMDATE")//轉帳日期
	private String CMDATE;
	@SerializedName(value = "OUTACN")//轉出帳號
	private String OUTACN;
	@SerializedName(value = "DPAGACNO_TEXT")//轉入帳號
	private String DPAGACNO_TEXT;
	
	@SerializedName(value = "AMOUNT")//轉帳金額
	private String AMOUNT;
	@SerializedName(value = "FDPACCNAME")//存款種類
	private String FDPACCNAME;
	@SerializedName(value = "TERM_TEXT")//期別/到期日
	private String TERM_TEXT;
	@SerializedName(value = "INTMTHNAME")//計息方式
	private String INTMTHNAME;
	@SerializedName(value = "CODENAME")//到期轉期
	private String CODENAME;
	
	@SerializedName(value = "DPSVTYPENAME")//轉存方式
	private String DPSVTYPENAME;
		
		
	@Override
	public Map<String, String> coverMap(){
		LinkedHashMap<String, String> result = new LinkedHashMap<String, String>();
		result.put("交易名稱", "臺幣轉入綜存定存");
		if(this.FGTXDATE.equals("1")) {
			result.put("交易類型", "即時");
			result.put("轉帳日期", this.transfer_date);
		}else if(this.FGTXDATE.equals("2")) {
			result.put("交易類型", "預約");
			result.put("轉帳日期", this.CMDATE);
		}
		result.put("轉出帳號",this.OUTACN);
		result.put("轉入帳號",this.DPAGACNO_TEXT);
		result.put("轉帳金額",this.AMOUNT);
		result.put("存款種類",this.FDPACCNAME);
		result.put("期別/到期日",this.TERM_TEXT);
		result.put("計息方式",this.INTMTHNAME);
		result.put("到期轉期",this.CODENAME);
		result.put("轉存方式",this.DPSVTYPENAME);
		return result;
	}


	public String getFGTXDATE() {
		return FGTXDATE;
	}


	public void setFGTXDATE(String fGTXDATE) {
		FGTXDATE = fGTXDATE;
	}


	public String getTransfer_date() {
		return transfer_date;
	}


	public void setTransfer_date(String transfer_date) {
		this.transfer_date = transfer_date;
	}


	public String getCMDATE() {
		return CMDATE;
	}


	public void setCMDATE(String cMDATE) {
		CMDATE = cMDATE;
	}


	public String getOUTACN() {
		return OUTACN;
	}


	public void setOUTACN(String oUTACN) {
		OUTACN = oUTACN;
	}


	public String getDPAGACNO_TEXT() {
		return DPAGACNO_TEXT;
	}


	public void setDPAGACNO_TEXT(String dPAGACNO_TEXT) {
		DPAGACNO_TEXT = dPAGACNO_TEXT;
	}


	public String getAMOUNT() {
		return AMOUNT;
	}


	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}


	public String getFDPACCNAME() {
		return FDPACCNAME;
	}


	public void setFDPACCNAME(String fDPACCNAME) {
		FDPACCNAME = fDPACCNAME;
	}


	public String getTERM_TEXT() {
		return TERM_TEXT;
	}


	public void setTERM_TEXT(String tERM_TEXT) {
		TERM_TEXT = tERM_TEXT;
	}


	public String getINTMTHNAME() {
		return INTMTHNAME;
	}


	public void setINTMTHNAME(String iNTMTHNAME) {
		INTMTHNAME = iNTMTHNAME;
	}


	public String getCODENAME() {
		return CODENAME;
	}


	public void setCODENAME(String cODENAME) {
		CODENAME = cODENAME;
	}


	public String getDPSVTYPENAME() {
		return DPSVTYPENAME;
	}


	public void setDPSVTYPENAME(String dPSVTYPENAME) {
		DPSVTYPENAME = dPSVTYPENAME;
	}


}
