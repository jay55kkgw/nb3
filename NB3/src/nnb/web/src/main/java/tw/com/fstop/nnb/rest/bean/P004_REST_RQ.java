package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class P004_REST_RQ extends BaseRestBean_OLA implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1726781082532249394L;
	
	private String TRANNAME;//TRANNAME
	private String TxnCode;//交易代碼
	private String TxnDateTime;//交易時間
	private String SerialNo;//流水號
	private String CustomerId;//客戶ID
	private String ZoneCode;//縣市別
	private String QueryTerms;//查詢條件
	private String CarId;//牌照號碼
	private String Account;//扣款帳號
	private String STARTSERACHDATE;//搜尋起始日期
	private String ENDSERACHDATE;//搜尋截止日期
	private String StartRecordNo;//起始筆數
	private String MaxRecordsPerTime;//本次查詢結果最多可回應的資料筆數
	private String StartSearchDate;
	private String EndSearchDate;
	private String ADOPID;
	
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getStartSearchDate() {
		return StartSearchDate;
	}
	public void setStartSearchDate(String startSearchDate) {
		StartSearchDate = startSearchDate;
	}
	public String getEndSearchDate() {
		return EndSearchDate;
	}
	public void setEndSearchDate(String endSearchDate) {
		EndSearchDate = endSearchDate;
	}
	public String getTRANNAME() {
		return TRANNAME;
	}
	public void setTRANNAME(String tRANNAME) {
		TRANNAME = tRANNAME;
	}
	public String getTxnCode() {
		return TxnCode;
	}
	public void setTxnCode(String txnCode) {
		TxnCode = txnCode;
	}
	public String getTxnDateTime() {
		return TxnDateTime;
	}
	public void setTxnDateTime(String txnDateTime) {
		TxnDateTime = txnDateTime;
	}
	public String getSerialNo() {
		return SerialNo;
	}
	public void setSerialNo(String serialNo) {
		SerialNo = serialNo;
	}
	public String getCustomerId() {
		return CustomerId;
	}
	public void setCustomerId(String customerId) {
		CustomerId = customerId;
	}
	public String getZoneCode() {
		return ZoneCode;
	}
	public void setZoneCode(String zoneCode) {
		ZoneCode = zoneCode;
	}
	public String getQueryTerms() {
		return QueryTerms;
	}
	public void setQueryTerms(String queryTerms) {
		QueryTerms = queryTerms;
	}
	public String getCarId() {
		return CarId;
	}
	public void setCarId(String carId) {
		CarId = carId;
	}
	public String getAccount() {
		return Account;
	}
	public void setAccount(String account) {
		Account = account;
	}
	public String getSTARTSERACHDATE() {
		return STARTSERACHDATE;
	}
	public void setSTARTSERACHDATE(String sTARTSERACHDATE) {
		STARTSERACHDATE = sTARTSERACHDATE;
	}
	public String getENDSERACHDATE() {
		return ENDSERACHDATE;
	}
	public void setENDSERACHDATE(String eNDSERACHDATE) {
		ENDSERACHDATE = eNDSERACHDATE;
	}
	public String getStartRecordNo() {
		return StartRecordNo;
	}
	public void setStartRecordNo(String startRecordNo) {
		StartRecordNo = startRecordNo;
	}
	public String getMaxRecordsPerTime() {
		return MaxRecordsPerTime;
	}
	public void setMaxRecordsPerTime(String maxRecordsPerTime) {
		MaxRecordsPerTime = maxRecordsPerTime;
	}
}
