package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * 活期性存款 交易明細查詢
 * @author Ian
 *
 */
public class N130_REST_RQ extends BaseRestBean_TW implements Serializable{
	
	private static final long serialVersionUID = -3567572475674697569L;
	
	private String ACN;//帳號
	private String CUSIDN;
	private String CMSDATE;//開始日期
	private String CMEDATE;//結束日期
	private String FGPERIOD;// 查詢區間
	private String	TRNSRC = "NB" ;
	public String getACN()
	{
		return ACN;
	}
	public void setACN(String aCN)
	{
		ACN = aCN;
	}
	public String getCUSIDN()
	{
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN)
	{
		CUSIDN = cUSIDN;
	}
	public String getCMSDATE()
	{
		return CMSDATE;
	}
	public void setCMSDATE(String cMSDATE)
	{
		CMSDATE = cMSDATE;
	}
	public String getCMEDATE()
	{
		return CMEDATE;
	}
	public void setCMEDATE(String cMEDATE)
	{
		CMEDATE = cMEDATE;
	}
	public String getFGPERIOD()
	{
		return FGPERIOD;
	}
	public void setFGPERIOD(String fGPERIOD)
	{
		FGPERIOD = fGPERIOD;
	}
	public String getTRNSRC()
	{
		return TRNSRC;
	}
	public void setTRNSRC(String tRNSRC)
	{
		TRNSRC = tRNSRC;
	}
	
	
}
