package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N070B_REST_RS extends BaseRestBean implements Serializable {	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1287615214953858734L;
	
	private String OFFSET;		// 空白
	private String HEADER;		// HEADER
	private String SYNC;		// Sync.Check Item
	private String OUTACN;		// 轉出帳號
	private String O_TOTBAL;	// 轉出帳號帳上餘額
	private String O_AVLBAL;	// 轉出帳號可用餘額
	private String INTSACN;		// 期貨帳號
	private String AMOUNT;		// 轉帳金額
	private String DATE;		// 日期YYYMMDD
	private String TIME;		// 時間HHMMSS
	private String MAC;			// MAC
	private String CMTXTIME;	// 交易時間
	
	
	public String getOFFSET() {
		return OFFSET;
	}
	public void setOFFSET(String oFFSET) {
		OFFSET = oFFSET;
	}
	public String getHEADER() {
		return HEADER;
	}
	public void setHEADER(String hEADER) {
		HEADER = hEADER;
	}
	public String getSYNC() {
		return SYNC;
	}
	public void setSYNC(String sYNC) {
		SYNC = sYNC;
	}
	public String getOUTACN() {
		return OUTACN;
	}
	public void setOUTACN(String oUTACN) {
		OUTACN = oUTACN;
	}
	public String getO_TOTBAL() {
		return O_TOTBAL;
	}
	public void setO_TOTBAL(String o_TOTBAL) {
		O_TOTBAL = o_TOTBAL;
	}
	public String getO_AVLBAL() {
		return O_AVLBAL;
	}
	public void setO_AVLBAL(String o_AVLBAL) {
		O_AVLBAL = o_AVLBAL;
	}
	public String getINTSACN() {
		return INTSACN;
	}
	public void setINTSACN(String iNTSACN) {
		INTSACN = iNTSACN;
	}
	public String getAMOUNT() {
		return AMOUNT;
	}
	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getMAC() {
		return MAC;
	}
	public void setMAC(String mAC) {
		MAC = mAC;
	}
	public String getCMTXTIME() {
		return CMTXTIME;
	}
	public void setCMTXTIME(String cMTXTIME) {
		CMTXTIME = cMTXTIME;
	}
	
	

}
