package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * 
 * 功能說明 :一般網銀外幣匯出匯款受款人約定檔擷取
 *
 */
public class F031_REST_RSDATA implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 368834876898736534L;

	private String LENGTH;// LENGTH

	private String PGMID;// 程式名稱

	private String STATUS;// STATUS

	private String RCVCNT;// 接收筆數

	private String CCY;// 匯款幣別

	private String BENACC;// 受款人帳號

	private String BENNAME;// 受款人戶名

	private String BENAD1;// 受款人住址1

	private String BENAD2;// 受款人住址2

	private String BENAD3;// 受款人住址3

	private String ACWBIC;// 受款銀行代號

	private String ACWNAME;// 受款銀行名稱

	private String ACWAD1;// 受款銀行住址1

	private String ACWAD2;// 受款銀行住址2

	private String ACWAD3;// 受款銀行住址3

	private String CNTY;// 國別

	private String REMTYPE;// 受款人身份別

	private String CUSTID;// 客戶統編
	
    private String TEXT;//BENACC+CCY


	public String getLENGTH()
	{
		return LENGTH;
	}

	public void setLENGTH(String lENGTH)
	{
		LENGTH = lENGTH;
	}

	public String getPGMID()
	{
		return PGMID;
	}

	public void setPGMID(String pGMID)
	{
		PGMID = pGMID;
	}

	public String getSTATUS()
	{
		return STATUS;
	}

	public void setSTATUS(String sTATUS)
	{
		STATUS = sTATUS;
	}

	public String getRCVCNT()
	{
		return RCVCNT;
	}

	public void setRCVCNT(String rCVCNT)
	{
		RCVCNT = rCVCNT;
	}

	public String getCCY()
	{
		return CCY;
	}

	public void setCCY(String cCY)
	{
		CCY = cCY;
	}

	public String getBENACC()
	{
		return BENACC;
	}

	public void setBENACC(String bENACC)
	{
		BENACC = bENACC;
	}

	public String getBENNAME()
	{
		return BENNAME;
	}

	public void setBENNAME(String bENNAME)
	{
		BENNAME = bENNAME;
	}

	public String getBENAD1()
	{
		return BENAD1;
	}

	public void setBENAD1(String bENAD1)
	{
		BENAD1 = bENAD1;
	}

	public String getBENAD2()
	{
		return BENAD2;
	}

	public void setBENAD2(String bENAD2)
	{
		BENAD2 = bENAD2;
	}

	public String getBENAD3()
	{
		return BENAD3;
	}

	public void setBENAD3(String bENAD3)
	{
		BENAD3 = bENAD3;
	}

	public String getACWBIC()
	{
		return ACWBIC;
	}

	public void setACWBIC(String aCWBIC)
	{
		ACWBIC = aCWBIC;
	}

	public String getACWNAME()
	{
		return ACWNAME;
	}

	public void setACWNAME(String aCWNAME)
	{
		ACWNAME = aCWNAME;
	}

	public String getACWAD1()
	{
		return ACWAD1;
	}

	public void setACWAD1(String aCWAD1)
	{
		ACWAD1 = aCWAD1;
	}

	public String getACWAD2()
	{
		return ACWAD2;
	}

	public void setACWAD2(String aCWAD2)
	{
		ACWAD2 = aCWAD2;
	}

	public String getACWAD3()
	{
		return ACWAD3;
	}

	public void setACWAD3(String aCWAD3)
	{
		ACWAD3 = aCWAD3;
	}

	public String getCNTY()
	{
		return CNTY;
	}

	public void setCNTY(String cNTY)
	{
		CNTY = cNTY;
	}

	public String getREMTYPE()
	{
		return REMTYPE;
	}

	public void setREMTYPE(String rEMTYPE)
	{
		REMTYPE = rEMTYPE;
	}

	public String getCUSTID()
	{
		return CUSTID;
	}

	public void setCUSTID(String cUSTID)
	{
		CUSTID = cUSTID;
	}

	public String getTEXT()
	{
		return TEXT;
	}

	public void setTEXT(String tEXT)
	{
		TEXT = tEXT;
	}
	
	

}
