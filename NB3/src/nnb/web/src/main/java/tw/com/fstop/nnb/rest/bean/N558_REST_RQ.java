package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
/**
 * 
 * @author fstop
 *
 */
public class N558_REST_RQ extends BaseRestBean_FX implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -404441604996163727L;
	private String ACN;    //帳號
	private String CUSIDN; // 統一編號
	private String FDATE;  //查詢起期
	private String TDATE;  //查詢迄期
	private String LCNO;   //信用狀號碼
	private String TRNSRC; //交易來源
	private String USERDATA;
	
	public String getUSERDATA() {
		return USERDATA;
	}
	public void setUSERDATA(String uSERDATA) {
		USERDATA = uSERDATA;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getFDATE() {
		return FDATE;
	}
	public void setFDATE(String fDATE) {
		FDATE = fDATE;
	}
	public String getTDATE() {
		return TDATE;
	}
	public void setTDATE(String tDATE) {
		TDATE = tDATE;
	}
	public String getLCNO() {
		return LCNO;
	}
	public void setLCNO(String lCNO) {
		LCNO = lCNO;
	}
	public String getTRNSRC() {
		return TRNSRC;
	}
	public void setTRNSRC(String tRNSRC) {
		TRNSRC = tRNSRC;
	}

	


}
