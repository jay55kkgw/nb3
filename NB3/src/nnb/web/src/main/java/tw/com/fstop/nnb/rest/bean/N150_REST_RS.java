package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

/**
 * 輕鬆理財存摺明細查詢
 * 
 * @author Jim
 *
 */

public class N150_REST_RS extends BaseRestBean implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 821589556129505054L;

	private String CMRECNUM; // 資料總數

	private String ABEND; // 結束代碼

	private String REC_NO; // 筆數

	private String ENDDATE; // 結束日期

	private String ACN; // 帳號

	private String CMQTIME;// 查詢時間

	private String CMPERIOD;// 查詢區間

	private String DONEACNOS;
	
	private String QUERYNEXT;//繼續查詢資料

	private LinkedList<N150_REST_RSDATA> REC;

	public String getCMRECNUM()
	{
		return CMRECNUM;
	}

	public void setCMRECNUM(String cMRECNUM)
	{
		CMRECNUM = cMRECNUM;
	}

	public String getABEND()
	{
		return ABEND;
	}

	public void setABEND(String aBEND)
	{
		ABEND = aBEND;
	}

	public String getREC_NO()
	{
		return REC_NO;
	}

	public void setREC_NO(String rEC_NO)
	{
		REC_NO = rEC_NO;
	}

	public String getENDDATE()
	{
		return ENDDATE;
	}

	public void setENDDATE(String eNDDATE)
	{
		ENDDATE = eNDDATE;
	}

	public String getDONEACNOS()
	{
		return DONEACNOS;
	}

	public void setDONEACNOS(String dONEACNOS)
	{
		DONEACNOS = dONEACNOS;
	}

	public String getACN()
	{
		return ACN;
	}

	public void setACN(String aCN)
	{
		ACN = aCN;
	}

	public String getCMQTIME()
	{
		return CMQTIME;
	}

	public void setCMQTIME(String cMQTIME)
	{
		CMQTIME = cMQTIME;
	}

	public String getCMPERIOD()
	{
		return CMPERIOD;
	}

	public void setCMPERIOD(String cMPERIOD)
	{
		CMPERIOD = cMPERIOD;
	}

	public String getQUERYNEXT()
	{
		return QUERYNEXT;
	}

	public void setQUERYNEXT(String qUERYNEXT)
	{
		QUERYNEXT = qUERYNEXT;
	}

	public LinkedList<N150_REST_RSDATA> getREC()
	{
		return REC;
	}

	public void setREC(LinkedList<N150_REST_RSDATA> rEC)
	{
		REC = rEC;
	}

}
