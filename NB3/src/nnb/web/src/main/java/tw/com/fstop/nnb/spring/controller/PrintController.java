package tw.com.fstop.nnb.spring.controller;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;

/*
 * 列印結果頁的控制器
 */
@SessionAttributes({SessionUtil.PRINT_DATALISTMAP_DATA})
@Controller
public class PrintController{
	private Logger log = LoggerFactory.getLogger(this.getClass().getName());

	/*
	 * 結果頁列印
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/print")
	public String print(@RequestParam(required=false) Map<String,Object> parameterMap,Model model){
		log.debug("IN print");
		
		Set<String> keySet = parameterMap.keySet();
		for(String key : keySet){
			log.debug(ESAPIUtil.vaildLog("key={}"+key));
			String value = (String)parameterMap.get(key);
			log.debug(ESAPIUtil.vaildLog("value={}"+value));
		}
		
		String jspTemplateName = (String)parameterMap.get("jspTemplateName");
		log.debug(ESAPIUtil.vaildLog("jspTemplateName={}"+jspTemplateName));
		
		
		try{
			List<Map<String,Object>> dataListMap = (List<Map<String,Object>>)SessionUtil.getAttribute(model,SessionUtil.PRINT_DATALISTMAP_DATA,null);
			log.debug("dataListMap={}",dataListMap);
			parameterMap.put("dataListMap",dataListMap);
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("print error >> {}",e);
		}
		model.addAllAttributes(parameterMap);
		
		return "/printTemplate/" + jspTemplateName;
	}
}