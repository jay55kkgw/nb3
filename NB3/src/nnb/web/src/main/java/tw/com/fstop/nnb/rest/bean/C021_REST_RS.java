package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

/**
 * C021電文RS
 */
public class C021_REST_RS extends BaseRestBean implements Serializable{
	private static final long serialVersionUID = 6791228050721094802L;
	
	private String DATE;
	private String TIME;
	private String NEXT;
	private String RECNO;
	private String CUSIDN;
	private String CUSNAME;
	private String LOOPINFO;
	private String FILLER_X3;
	private String TOTRECNO;
	private LinkedList<C021_REST_RSDATA> REC;
	//下面是N922電文一起回來的值
	private String FDINVTYPE;
	private String CUTTYPE;
	private String RISK7;
	private String GETLTD7;
	private String APLBRH;
	private String NAME;
	private String CAREER;
	private String SALARY;
	private String BRTHDY;
	private String ACN4;
	private String ACN2;
	private String EMPNO;
	private String ACN3;
	private String ICCOD;
	private String DEGREE;
	private String ACN1;
	private String GETLTD;
	private String MARK3;
	private String MARK1;
	
	public String getDATE(){
		return DATE;
	}
	public void setDATE(String dATE){
		DATE = dATE;
	}
	public String getTIME(){
		return TIME;
	}
	public void setTIME(String tIME){
		TIME = tIME;
	}
	public String getNEXT(){
		return NEXT;
	}
	public void setNEXT(String nEXT){
		NEXT = nEXT;
	}
	public String getRECNO(){
		return RECNO;
	}
	public void setRECNO(String rECNO){
		RECNO = rECNO;
	}
	public String getCUSIDN(){
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN){
		CUSIDN = cUSIDN;
	}
	public String getCUSNAME(){
		return CUSNAME;
	}
	public void setCUSNAME(String cUSNAME){
		CUSNAME = cUSNAME;
	}
	public String getLOOPINFO(){
		return LOOPINFO;
	}
	public void setLOOPINFO(String lOOPINFO){
		LOOPINFO = lOOPINFO;
	}
	public String getFILLER_X3(){
		return FILLER_X3;
	}
	public void setFILLER_X3(String fILLER_X3){
		FILLER_X3 = fILLER_X3;
	}
	public String getTOTRECNO(){
		return TOTRECNO;
	}
	public void setTOTRECNO(String tOTRECNO){
		TOTRECNO = tOTRECNO;
	}
	public LinkedList<C021_REST_RSDATA> getREC(){
		return REC;
	}
	public void setREC(LinkedList<C021_REST_RSDATA> rEC){
		REC = rEC;
	}
	public String getFDINVTYPE() {
		return FDINVTYPE;
	}
	public void setFDINVTYPE(String fDINVTYPE) {
		FDINVTYPE = fDINVTYPE;
	}
	public String getCUTTYPE() {
		return CUTTYPE;
	}
	public void setCUTTYPE(String cUTTYPE) {
		CUTTYPE = cUTTYPE;
	}
	public String getRISK7() {
		return RISK7;
	}
	public void setRISK7(String rISK7) {
		RISK7 = rISK7;
	}
	public String getGETLTD7() {
		return GETLTD7;
	}
	public void setGETLTD7(String gETLTD7) {
		GETLTD7 = gETLTD7;
	}
	public String getAPLBRH() {
		return APLBRH;
	}
	public void setAPLBRH(String aPLBRH) {
		APLBRH = aPLBRH;
	}
	public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	public String getCAREER() {
		return CAREER;
	}
	public void setCAREER(String cAREER) {
		CAREER = cAREER;
	}
	public String getSALARY() {
		return SALARY;
	}
	public void setSALARY(String sALARY) {
		SALARY = sALARY;
	}
	public String getBRTHDY() {
		return BRTHDY;
	}
	public void setBRTHDY(String bRTHDY) {
		BRTHDY = bRTHDY;
	}
	public String getACN4() {
		return ACN4;
	}
	public void setACN4(String aCN4) {
		ACN4 = aCN4;
	}
	public String getACN2() {
		return ACN2;
	}
	public void setACN2(String aCN2) {
		ACN2 = aCN2;
	}
	public String getEMPNO() {
		return EMPNO;
	}
	public void setEMPNO(String eMPNO) {
		EMPNO = eMPNO;
	}
	public String getACN3() {
		return ACN3;
	}
	public void setACN3(String aCN3) {
		ACN3 = aCN3;
	}
	public String getICCOD() {
		return ICCOD;
	}
	public void setICCOD(String iCCOD) {
		ICCOD = iCCOD;
	}
	public String getDEGREE() {
		return DEGREE;
	}
	public void setDEGREE(String dEGREE) {
		DEGREE = dEGREE;
	}
	public String getACN1() {
		return ACN1;
	}
	public void setACN1(String aCN1) {
		ACN1 = aCN1;
	}
	public String getGETLTD() {
		return GETLTD;
	}
	public void setGETLTD(String gETLTD) {
		GETLTD = gETLTD;
	}
	public String getMARK3() {
		return MARK3;
	}
	public void setMARK3(String mARK3) {
		MARK3 = mARK3;
	}
	public String getMARK1() {
		return MARK1;
	}
	public void setMARK1(String mARK1) {
		MARK1 = mARK1;
	}
}