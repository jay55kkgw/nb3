
/*
 * Copyright (c) 2017, FSTOP, Inc. All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tw.com.fstop.web.util;

import javax.servlet.http.HttpServletRequest;

import org.owasp.esapi.ESAPI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ESAPI web utility functions.
 * 
 *
 * @since 1.0.1
 */
public class WebESAPIUtil
{
    private final static Logger logger = LoggerFactory.getLogger(WebESAPIUtil.class);
    
    /**
     * Get request parameter by ESAPI.
     * 
     * @param request - http servlet request
     * @param key - parameter key
     * @return parameter value
     */
    public static String getParameter(HttpServletRequest request, String key)
    {
        String value = null;

        if (request.getParameter(key) == null)
        {
            value = null;
        }
        else if ("".equals(request.getParameter(key)))
        {
            value = "";
        }
        else
        {
            try
            {
                value = ESAPI.httpUtilities().getParameter(request, key);
            }
            catch (Exception e)
            {
                logger.error("WebESAPIUtil error=", e);
            }
        }
        return value;
    }

    /**
     * Get request attribute.
     * 
     * @param request - http servlet request
     * @param key - request attribute key
     * @return request attribute value
     */
    public static Object getRequestAttribute(HttpServletRequest request, String key)
    {
        return ESAPI.httpUtilities().getRequestAttribute(request, key);
    }

}
