package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

/**
 * 
 * 功能說明 :一般網銀外幣匯出匯款受款人約定檔擷取 回應
 *
 */
public class F013_REST_RS extends BaseRestBean implements Serializable 
{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4371001595190973911L;

	private String CMQTIME;//交易時間

	private String LENGTH;// LENGTH

	private String TC_NO;// TC號碼

	private String WS_ID;// 工作站代碼 R6000001

	private String TERM_DATE;// 日期YYYYMMDD

	private String TERM_TIME;// 時間HHMMSS

    private String CMRECNUM;

	private String ERR_CODE;// 錯誤訊息
    
	private String LRGOUT_NUM;// 空白

	private String HIGH_IND;// HIGH-IND

	private String IND_TABLE;// IND-TABLE

	private String DATA_LENGTH;// 資料長度

	private String RCVCNT;// 接收筆數

	private LinkedList<F013_REST_RSDATA> REC;

	public String getCMQTIME()
	{
		return CMQTIME;
	}

	public void setCMQTIME(String cMQTIME)
	{
		CMQTIME = cMQTIME;
	}

	public String getLENGTH()
	{
		return LENGTH;
	}

	public void setLENGTH(String lENGTH)
	{
		LENGTH = lENGTH;
	}

	public String getTC_NO()
	{
		return TC_NO;
	}

	public void setTC_NO(String tC_NO)
	{
		TC_NO = tC_NO;
	}

	public String getWS_ID()
	{
		return WS_ID;
	}

	public void setWS_ID(String wS_ID)
	{
		WS_ID = wS_ID;
	}

	public String getTERM_DATE()
	{
		return TERM_DATE;
	}

	public void setTERM_DATE(String tERM_DATE)
	{
		TERM_DATE = tERM_DATE;
	}

	public String getTERM_TIME()
	{
		return TERM_TIME;
	}

	public void setTERM_TIME(String tERM_TIME)
	{
		TERM_TIME = tERM_TIME;
	}

	public String getCMRECNUM()
	{
		return CMRECNUM;
	}

	public void setCMRECNUM(String cMRECNUM)
	{
		CMRECNUM = cMRECNUM;
	}

	public String getERR_CODE()
	{
		return ERR_CODE;
	}

	public void setERR_CODE(String eRR_CODE)
	{
		ERR_CODE = eRR_CODE;
	}

	public String getLRGOUT_NUM()
	{
		return LRGOUT_NUM;
	}

	public void setLRGOUT_NUM(String lRGOUT_NUM)
	{
		LRGOUT_NUM = lRGOUT_NUM;
	}

	public String getHIGH_IND()
	{
		return HIGH_IND;
	}

	public void setHIGH_IND(String hIGH_IND)
	{
		HIGH_IND = hIGH_IND;
	}

	public String getIND_TABLE()
	{
		return IND_TABLE;
	}

	public void setIND_TABLE(String iND_TABLE)
	{
		IND_TABLE = iND_TABLE;
	}

	public String getDATA_LENGTH()
	{
		return DATA_LENGTH;
	}

	public void setDATA_LENGTH(String dATA_LENGTH)
	{
		DATA_LENGTH = dATA_LENGTH;
	}

	public String getRCVCNT()
	{
		return RCVCNT;
	}

	public void setRCVCNT(String rCVCNT)
	{
		RCVCNT = rCVCNT;
	}

	public LinkedList<F013_REST_RSDATA> getREC()
	{
		return REC;
	}

	public void setREC(LinkedList<F013_REST_RSDATA> rEC)
	{
		REC = rEC;
	}

	


}
