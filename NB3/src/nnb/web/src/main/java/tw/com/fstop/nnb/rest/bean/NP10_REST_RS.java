package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class NP10_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6196361413173461127L;

	private String MSGCOD;	// 回應代碼

	public String getMSGCOD() {
		return MSGCOD;
	}

	public void setMSGCOD(String mSGCOD) {
		MSGCOD = mSGCOD;
	}
	
	
}