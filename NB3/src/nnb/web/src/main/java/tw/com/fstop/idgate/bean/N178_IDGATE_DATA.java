package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class N178_IDGATE_DATA implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2441024288361858475L;

	@SerializedName(value = "FYACN")
	private String FYACN;
	@SerializedName(value = "FDPNUM")
	private String FDPNUM;
	@SerializedName(value = "TERM")
	private String TERM;
	@SerializedName(value = "TYPCOD")
	private String TYPCOD;
	@SerializedName(value = "NHITAX")
	private String NHITAX;
	
	@SerializedName(value = "TYPE1")
	private String TYPE1;
	@SerializedName(value = "FYTSFAN")
	private String FYTSFAN;
	@SerializedName(value = "INTMTH")
	private String INTMTH;
	@SerializedName(value = "AMTFDP")
	private String AMTFDP;


	@SerializedName(value = "INT")
	private String INT;
	
	@SerializedName(value = "FYTAX")
	private String FYTAX;

	public String getFYACN() {
		return FYACN;
	}

	public void setFYACN(String fYACN) {
		FYACN = fYACN;
	}

	public String getFDPNUM() {
		return FDPNUM;
	}

	public void setFDPNUM(String fDPNUM) {
		FDPNUM = fDPNUM;
	}

	public String getTERM() {
		return TERM;
	}

	public void setTERM(String tERM) {
		TERM = tERM;
	}

	public String getTYPCOD() {
		return TYPCOD;
	}

	public void setTYPCOD(String tYPCOD) {
		TYPCOD = tYPCOD;
	}

	public String getNHITAX() {
		return NHITAX;
	}

	public void setNHITAX(String nHITAX) {
		NHITAX = nHITAX;
	}

	public String getTYPE1() {
		return TYPE1;
	}

	public void setTYPE1(String tYPE1) {
		TYPE1 = tYPE1;
	}

	public String getFYTSFAN() {
		return FYTSFAN;
	}

	public void setFYTSFAN(String fYTSFAN) {
		FYTSFAN = fYTSFAN;
	}

	public String getINTMTH() {
		return INTMTH;
	}

	public void setINTMTH(String iNTMTH) {
		INTMTH = iNTMTH;
	}

	public String getAMTFDP() {
		return AMTFDP;
	}

	public void setAMTFDP(String aMTFDP) {
		AMTFDP = aMTFDP;
	}

	public String getINT() {
		return INT;
	}

	public void setINT(String iNT) {
		INT = iNT;
	}

	public String getFYTAX() {
		return FYTAX;
	}

	public void setFYTAX(String fYTAX) {
		FYTAX = fYTAX;
	}
	
}
