package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class Aioedm_REST_RQ extends BaseRestBean_CC implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1364580708347299982L;

	private String Type;
	private String cUkey;
	private String cDate;
	
	public String getType() {
		return Type;
	}
	public void setType(String type) {
		Type = type;
	}
	public String getcUkey() {
		return cUkey;
	}
	public void setcUkey(String cUkey) {
		this.cUkey = cUkey;
	}
	public String getcDate() {
		return cDate;
	}
	public void setcDate(String cDate) {
		this.cDate = cDate;
	}
	
}
