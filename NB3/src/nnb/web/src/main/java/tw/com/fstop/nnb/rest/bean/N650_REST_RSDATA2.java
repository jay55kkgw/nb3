package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N650_REST_RSDATA2 extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3088506115720952831L;
	
	String BASEDAY;		//基準日
	String FUNDID;		//基金代號
	String ITR;			//利率
	String FUNDAMT;		//信託金額
	String FUNDPNT;		//單位數
	String BYPRICE;		//買入價格
	String FUNDNUM;		//憑證號碼
	String FDNAME;		//基金名稱
	String ACN;			//帳號
	String BAL;			//損益
	String REFAVL;		//台幣現值

	public String getBASEDAY() {
		return BASEDAY;
	}

	public void setBASEDAY(String bASEDAY) {
		BASEDAY = bASEDAY;
	}

	public String getFUNDID() {
		return FUNDID;
	}

	public void setFUNDID(String fUNDID) {
		FUNDID = fUNDID;
	}

	public String getITR() {
		return ITR;
	}

	public void setITR(String iTR) {
		ITR = iTR;
	}

	public String getFUNDAMT() {
		return FUNDAMT;
	}

	public void setFUNDAMT(String fUNDAMT) {
		FUNDAMT = fUNDAMT;
	}

	public String getFUNDPNT() {
		return FUNDPNT;
	}

	public void setFUNDPNT(String fUNDPNT) {
		FUNDPNT = fUNDPNT;
	}

	public String getBYPRICE() {
		return BYPRICE;
	}

	public void setBYPRICE(String bYPRICE) {
		BYPRICE = bYPRICE;
	}

	public String getFUNDNUM() {
		return FUNDNUM;
	}

	public void setFUNDNUM(String fUNDNUM) {
		FUNDNUM = fUNDNUM;
	}

	public String getFDNAME() {
		return FDNAME;
	}

	public void setFDNAME(String fDNAME) {
		FDNAME = fDNAME;
	}

	public String getACN() {
		return ACN;
	}

	public void setACN(String aCN) {
		ACN = aCN;
	}

	public String getBAL() {
		return BAL;
	}

	public void setBAL(String bAL) {
		BAL = bAL;
	}

	public String getREFAVL() {
		return REFAVL;
	}

	public void setREFAVL(String rEFAVL) {
		REFAVL = rEFAVL;
	}
}
