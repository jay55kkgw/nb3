package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * 
 * 功能說明 : 停損停利設定作業
 *
 */
public class C116_REST_RQ extends BaseRestBean_FUND implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6618798027466799466L;


	private String NEXT;// 資料起始位置

	private String RECNO;// 讀取筆數

	private String CUSIDN;// 統一編號




	public String getNEXT()
	{
		return NEXT;
	}

	public void setNEXT(String nEXT)
	{
		NEXT = nEXT;
	}

	public String getRECNO()
	{
		return RECNO;
	}

	public void setRECNO(String rECNO)
	{
		RECNO = rECNO;
	}

	public String getCUSIDN()
	{
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN)
	{
		CUSIDN = cUSIDN;
	}



}
