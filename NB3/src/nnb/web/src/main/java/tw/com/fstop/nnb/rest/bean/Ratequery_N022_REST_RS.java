package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class Ratequery_N022_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9053105082481299009L;

	private String UPDATEDATE;
	// 資料陣列
	private LinkedList<Ratequery_N022_RSDATA> REC;

	public String getUPDATEDATE() {
		return UPDATEDATE;
	}

	public LinkedList<Ratequery_N022_RSDATA> getREC() {
		return REC;
	}

	public void setUPDATEDATE(String uPDATEDATE) {
		UPDATEDATE = uPDATEDATE;
	}

	public void setREC(LinkedList<Ratequery_N022_RSDATA> rEC) {
		REC = rEC;
	}

}
