package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N108_REST_RQ extends BaseRestBean_TW implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -559387283010166442L;
	
	private String FILL_X1;	// FILL_X1
	private String TRANNAME;// TRANNAME
	private String HEADER;	// HEADER
	private String DATE;	// 日期YYYMMDD
	private String TIME;	// 時間HHMMSS
	private String SYNC;	// Sync.Check Item
	private String PPSYNC;	// P.P.Key Sync.Check Item
	private String PINKEY;	// 網路銀行密碼
	private String CERTACN;	// 憑證帳號
	private String FLAG;	// 0：非約定，1：約定
	private String BNKRA = "050";	// 行庫別
	private String XMLCA;	// CA識別碼
	private String XMLCN;	// 憑證CN
	private String PPSYNCN;	// P.P.Key Sync.Check Item 16
	private String PINNEW;	// 網路銀行密碼（新）
	private String CUSIDN;	// 統一編號
	private String FTYPE;	// 存單交易代號
	private String FDPACN; //存單帳號
	private String FDPDATA;	// 存單資料
	private String MAC;		// MAC
	
	// 電文不需要但ms_tw需要的欄位
	private String FGTXWAY;	// 交易機制
	private String iSeqNo;	// iSeqNo
	private String pkcs7Sign;// IKEY
	private String jsondc;// IKEY

	//IDGATE
	private String sessionID;
	private String idgateID;
	private String txnID;
	
	public String getFILL_X1() {
		return FILL_X1;
	}
	public void setFILL_X1(String fILL_X1) {
		FILL_X1 = fILL_X1;
	}
	public String getTRANNAME() {
		return TRANNAME;
	}
	public void setTRANNAME(String tRANNAME) {
		TRANNAME = tRANNAME;
	}
	public String getHEADER() {
		return HEADER;
	}
	public void setHEADER(String hEADER) {
		HEADER = hEADER;
	}
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getSYNC() {
		return SYNC;
	}
	public void setSYNC(String sYNC) {
		SYNC = sYNC;
	}
	public String getPPSYNC() {
		return PPSYNC;
	}
	public void setPPSYNC(String pPSYNC) {
		PPSYNC = pPSYNC;
	}
	public String getPINKEY() {
		return PINKEY;
	}
	public void setPINKEY(String pINKEY) {
		PINKEY = pINKEY;
	}
	public String getCERTACN() {
		return CERTACN;
	}
	public void setCERTACN(String cERTACN) {
		CERTACN = cERTACN;
	}
	public String getFLAG() {
		return FLAG;
	}
	public void setFLAG(String fLAG) {
		FLAG = fLAG;
	}
	public String getBNKRA() {
		return BNKRA;
	}
	public void setBNKRA(String bNKRA) {
		BNKRA = bNKRA;
	}
	public String getXMLCA() {
		return XMLCA;
	}
	public void setXMLCA(String xMLCA) {
		XMLCA = xMLCA;
	}
	public String getXMLCN() {
		return XMLCN;
	}
	public void setXMLCN(String xMLCN) {
		XMLCN = xMLCN;
	}
	public String getPPSYNCN() {
		return PPSYNCN;
	}
	public void setPPSYNCN(String pPSYNCN) {
		PPSYNCN = pPSYNCN;
	}
	public String getPINNEW() {
		return PINNEW;
	}
	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getFTYPE() {
		return FTYPE;
	}
	public void setFTYPE(String fTYPE) {
		FTYPE = fTYPE;
	}
	public String getFDPACN() {
		return FDPACN;
	}
	public void setFDPACN(String fDPACN) {
		FDPACN = fDPACN;
	}
	public String getFDPDATA() {
		return FDPDATA;
	}
	public void setFDPDATA(String fDPDATA) {
		FDPDATA = fDPDATA;
	}
	public String getMAC() {
		return MAC;
	}
	public void setMAC(String mAC) {
		MAC = mAC;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getiSeqNo() {
		return iSeqNo;
	}
	public void setiSeqNo(String iSeqNo) {
		this.iSeqNo = iSeqNo;
	}
	public String getPkcs7Sign() {
		return pkcs7Sign;
	}
	public void setPkcs7Sign(String pkcs7Sign) {
		this.pkcs7Sign = pkcs7Sign;
	}
	public String getJsondc() {
		return jsondc;
	}
	public void setJsondc(String jsondc) {
		this.jsondc = jsondc;
	}

	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public String getIdgateID() {
		return idgateID;
	}

	public void setIdgateID(String idgateID) {
		this.idgateID = idgateID;
	}

	public String getTxnID() {
		return txnID;
	}

	public void setTxnID(String txnID) {
		this.txnID = txnID;
	}
	
}
