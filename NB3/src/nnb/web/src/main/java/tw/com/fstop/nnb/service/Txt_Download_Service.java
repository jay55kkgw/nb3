package tw.com.fstop.nnb.service;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.view.AbstractView;

import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.util.DownloadUtil;
import tw.com.fstop.util.DownloadUtil.Variable;
import tw.com.fstop.util.ESAPIUtil;

@Service
public class Txt_Download_Service extends AbstractView{
	private Logger log = LoggerFactory.getLogger(this.getClass().getName());
	
	@Autowired
	private I18n i18n;
	
	@SuppressWarnings({"unchecked"})
	public void renderMergedOutputModel(Map<String,Object> parameterMap,HttpServletRequest request,HttpServletResponse response){
		log.info("IN renderMergedOutputModel");
		log.info("parameterMap={}",parameterMap);
		
		InputStream inputStream = null;
		InputStreamReader inputStreamReader = null;
		BufferedReader bufferedReader = null;
		
		try{
			String templatePath = (String)parameterMap.get("templatePath");
			log.info("templatePath={}",templatePath);
			
			Resource resource = new ClassPathResource(templatePath);
//			File file = resource.getFile();

			if(resource.exists()){
				inputStream = new BufferedInputStream(resource.getInputStream());
				inputStreamReader = new InputStreamReader(inputStream,"UTF-8");
				bufferedReader = new BufferedReader(inputStreamReader);

				Boolean hasMultiRowDataBoolean = null;
				String hasMultiRowData = String.valueOf(parameterMap.get("hasMultiRowData"));
				log.trace("hasMultiRowData={}",hasMultiRowData);
				if(!"null".equals(hasMultiRowData)){
					hasMultiRowDataBoolean = Boolean.valueOf(hasMultiRowData);
				}
				log.trace("hasMultiRowDataBoolean={}",hasMultiRowDataBoolean);
				
				Integer txtHeaderBottomEndInteger = null;
				String txtHeaderBottomEnd = String.valueOf(parameterMap.get("txtHeaderBottomEnd"));
				log.trace("txtHeaderBottomEnd={}",txtHeaderBottomEnd);
				if(!"null".equals(txtHeaderBottomEnd)){
					txtHeaderBottomEndInteger = Integer.valueOf(txtHeaderBottomEnd);
				}
				log.trace("txtHeaderBottomEndInteger={}",txtHeaderBottomEndInteger);
				
				Boolean txtHasRowDataBoolean = null;
				String txtHasRowData = String.valueOf(parameterMap.get("txtHasRowData"));
				log.trace("txtHasRowData={}",txtHasRowData);
				if(!"null".equals(txtHasRowData)){
					txtHasRowDataBoolean = Boolean.valueOf(txtHasRowData);
				}
				log.trace("txtHasRowDataBoolean={}",txtHasRowDataBoolean);
				
				Integer txtMultiRowStartIndexInteger = null;
				String txtMultiRowStartIndex = String.valueOf(parameterMap.get("txtMultiRowStartIndex"));
				log.trace("txtMultiRowStartIndex={}",txtMultiRowStartIndex);
				if(!"null".equals(txtMultiRowStartIndex)){
					txtMultiRowStartIndexInteger = Integer.valueOf(txtMultiRowStartIndex);
				}
				log.trace("txtMultiRowStartIndexInteger={}",txtMultiRowStartIndexInteger);

				Integer txtMultiRowEndIndexInteger = null;
				String txtMultiRowEndIndex = String.valueOf(parameterMap.get("txtMultiRowEndIndex"));
				log.trace("txtMultiRowEndIndex={}",txtMultiRowEndIndex);
				if(!"null".equals(txtMultiRowEndIndex)){
					txtMultiRowEndIndexInteger = Integer.valueOf(txtMultiRowEndIndex);
				}
				log.trace("txtMultiRowEndIndexInteger={}",txtMultiRowEndIndexInteger);

				Integer txtMultiRowCopyStartIndexInteger = null;
				String txtMultiRowCopyStartIndex = String.valueOf(parameterMap.get("txtMultiRowCopyStartIndex"));
				log.trace("txtMultiRowCopyStartIndex={}",txtMultiRowCopyStartIndex);
				if(!"null".equals(txtMultiRowCopyStartIndex)){
					txtMultiRowCopyStartIndexInteger = Integer.valueOf(txtMultiRowCopyStartIndex);
				}
				log.trace("txtMultiRowCopyStartIndexInteger={}",txtMultiRowCopyStartIndexInteger);

				Integer txtMultiRowCopyEndIndexInteger = null;
				String txtMultiRowCopyEndIndex = String.valueOf(parameterMap.get("txtMultiRowCopyEndIndex"));
				log.trace("txtMultiRowCopyEndIndex={}",txtMultiRowCopyEndIndex);
				if(!"null".equals(txtMultiRowCopyEndIndex)){
					txtMultiRowCopyEndIndexInteger = Integer.valueOf(txtMultiRowCopyEndIndex);
				}
				log.trace("txtMultiRowCopyEndIndexInteger={}",txtMultiRowCopyEndIndexInteger);

				String txtMultiRowDataListMapKeyString = null;
				String txtMultiRowDataListMapKey = String.valueOf(parameterMap.get("txtMultiRowDataListMapKey"));
				log.trace("txtMultiRowDataListMapKey={}",txtMultiRowDataListMapKey);
				if(!"null".equals(txtMultiRowDataListMapKey)){
					txtMultiRowDataListMapKeyString = txtMultiRowDataListMapKey;
				}
				log.trace("txtMultiRowDataListMapKeyString={}",txtMultiRowDataListMapKeyString);
				
				Boolean txtHasFooterBoolean = null;
				String txtHasFooter = String.valueOf(parameterMap.get("txtHasFooter"));
				log.trace("txtHasFooter={}",txtHasFooter);
				if(!"null".equals(txtHasFooter)){
					txtHasFooterBoolean = Boolean.valueOf(txtHasFooter);
				}
				log.trace("txtHasFooterBoolean={}",txtHasFooterBoolean);
				
				Locale locale = LocaleContextHolder.getLocale();
				String languageTag = locale.toLanguageTag();
				log.info("languageTag={}",languageTag);
				
				StringBuilder stringBuilder = new StringBuilder();
				//列數計算
				int readLineCount = 0;
				
				//先把HEADER的變數換掉
				if(txtHeaderBottomEndInteger != null){
					for(int x=0;x<=txtHeaderBottomEndInteger;x++){
						log.trace("x={}",x);
						
						String inputLineNoValid = bufferedReader.readLine();
						log.trace(ESAPIUtil.vaildLog("inputLine >> " + inputLineNoValid)); 
						
						String inputLine = ESAPIUtil.validInput(inputLineNoValid,"GeneralString",true);
						log.trace(ESAPIUtil.vaildLog("inputLine >> " + inputLine)); 
						
						if(inputLine != null && !"".equals(inputLine)){
							inputLine = replaceLineValue(inputLine,parameterMap);
							
							stringBuilder.append(inputLine + "\r\n");
						}
						else{
							stringBuilder.append("\r\n");

						}
					}
					readLineCount = txtHeaderBottomEndInteger + 1;
					log.trace("readLineCount={}",readLineCount);
				}
				//這裡要分成多個詳細資料和單個的情況
				//多個詳細資料
				if(hasMultiRowDataBoolean != null && hasMultiRowDataBoolean == true){
					//把詳細資料的變數換掉
					if(txtMultiRowStartIndexInteger != null && txtMultiRowEndIndexInteger != null
						&& txtMultiRowCopyStartIndexInteger != null && txtMultiRowCopyEndIndexInteger != null
						&& txtMultiRowDataListMapKeyString != null){
						
						//GROUP開始與ROW開始的差額
						int rangeA = txtMultiRowStartIndexInteger - txtMultiRowCopyStartIndexInteger;
						log.trace("rangeA={}",rangeA);
						
						//要複製的GROUP的LIST(不包含要複製的ROW)
						List<String> groupCopyList = new ArrayList<String>();
						//要複製的ROW的LIST
						List<String> rowCopyList = new ArrayList<String>();
						
						while(true){
							String inputLineNoValid = bufferedReader.readLine();
							log.trace(ESAPIUtil.vaildLog("inputLine >> " + inputLineNoValid)); 
							
							String inputLine = ESAPIUtil.validInput(inputLineNoValid,"GeneralString",true);
							log.trace(ESAPIUtil.vaildLog("inputLine >> " + inputLine)); 
							
							if(inputLine != null){
								//在GROUP的範圍內
								if(readLineCount >= txtMultiRowCopyStartIndexInteger && readLineCount <= txtMultiRowCopyEndIndexInteger){
									//在ROW的範圍內
									if(readLineCount >= txtMultiRowStartIndexInteger && readLineCount <= txtMultiRowEndIndexInteger){
										rowCopyList.add(inputLine);
									}
									else{
										groupCopyList.add(inputLine);
									}
								}
								log.trace(ESAPIUtil.vaildLog("groupCopyList >> " + groupCopyList.toString())); 
								log.trace(ESAPIUtil.vaildLog("rowCopyList >> " + rowCopyList.toString())); 
								if(readLineCount == txtMultiRowCopyEndIndexInteger){
									break;
								}
							}
							else{
								break;
							}
							readLineCount += 1;
							log.trace("readLineCount={}",readLineCount);
						}
						
						List<Map<String,Object>> dataListMap = (List<Map<String,Object>>)parameterMap.get("dataListMap");
						
						//組成GROUP
						for(int x=0;x<dataListMap.size();x++){
							Map<String,Object> firstMap = dataListMap.get(x);
							log.trace("firstMap={}",firstMap);
							
							int rangeCount = 0;
							boolean rowFinish = false;
							
							for(int y=0;y<=groupCopyList.size();y+=0){
								//在ROW的上面
								if(rangeCount < rangeA){
									String inputLine = replaceLineValue(groupCopyList.get(y),firstMap);
									
									stringBuilder.append(inputLine + "\r\n");
									y++;
								}
								//到ROW了，且沒有處理過
								else if(rangeCount == rangeA && rowFinish == false){
									List<Map<String,String>> secondListMap = (List<Map<String,String>>)firstMap.get(txtMultiRowDataListMapKeyString);
									log.trace("secondListMap={}",secondListMap);
									
									for(Map<String,String> secondMap : secondListMap){
										log.trace("secondMap={}",secondMap);
										
										for(int z=0;z<rowCopyList.size();z++){
											List<Variable> variableList = DownloadUtil.getVariables(rowCopyList.get(z),languageTag);
											
											String resultLine = DownloadUtil.getResultLine(variableList,secondMap);
											log.trace("resultLine={}",resultLine);
											stringBuilder.append(resultLine + "\r\n");
										}
									}
									//多帳號做兩行空格，最後一筆資料後不空行，若有超過兩行需確認前端頁面值無誤-可參考外幣活存明細查詢內容
									if(x < dataListMap.size()-1) {
										stringBuilder.append("\r\n");
										stringBuilder.append("\r\n");
									}
									rowFinish = true;
								}
								else if(y < groupCopyList.size()){
									String inputLine = replaceLineValue(groupCopyList.get(y),firstMap);
									stringBuilder.append(inputLine + "\r\n");
									y++;
								}
								else{
									break;
								}
								rangeCount += 1;
								log.trace("rangeCount={}",rangeCount);
							}
						}
					}
				}
				//一個詳細資料
				else{				
					//把詳細資料的變數換掉
					if(txtHasRowDataBoolean != null && txtHasRowDataBoolean == true){
						List<Map<String,String>> dataListMap = (List<Map<String,String>>)parameterMap.get("dataListMap");
						
						while(true){
							String inputLineNoValid = bufferedReader.readLine();
							log.trace(ESAPIUtil.vaildLog("inputLine >> " + inputLineNoValid)); 
							
							String inputLine = ESAPIUtil.validInput(inputLineNoValid,"GeneralString",true);
							log.trace(ESAPIUtil.vaildLog("inputLine >> " + inputLine)); 
							
							if(inputLine != null && !"".equals(inputLine)){
								List<Variable> variableList = DownloadUtil.getVariables(inputLine,languageTag);
								
								//有變數要取代
								if(variableList.size() > 0){
									for(Map<String,String> dataMap : dataListMap){
										log.trace("dataMap={}",dataMap);
										
										inputLine = DownloadUtil.getResultLine(variableList,dataMap);
										log.trace("inputLine={}",inputLine);
										
										stringBuilder.append(inputLine + "\r\n");
									}
								}
								//沒有
								else{
									stringBuilder.append(inputLine + "\r\n");
								}
							}else if("".equals(inputLine)){
								stringBuilder.append("\r\n");
								break;
							}
							else{
								break;
							}
						}
					}
				}
				//把FOOTER的變數換掉
				if(txtHasFooterBoolean != null && txtHasFooterBoolean == true){
					String inputLineNoValid = bufferedReader.readLine();
					log.trace(ESAPIUtil.vaildLog("inputLine >> " + inputLineNoValid)); 
					
					String inputLine = ESAPIUtil.validInput(inputLineNoValid,"GeneralString",true);
					log.trace(ESAPIUtil.vaildLog("inputLine >> " + inputLine)); 
					
					while(inputLine != null){
						if(!"".equals(inputLine)){
							inputLine = replaceLineValue(inputLine,parameterMap);
							
							stringBuilder.append(inputLine + "\r\n");
						}
						else{
							stringBuilder.append("\r\n");
						}
						inputLineNoValid = bufferedReader.readLine();
						log.trace(ESAPIUtil.vaildLog("inputLine >> " + inputLineNoValid)); 
						
						inputLine = ESAPIUtil.validInput(inputLineNoValid,"GeneralString",true);
						log.trace(ESAPIUtil.vaildLog("inputLine >> " + inputLine)); 
					}
				}
				response.setContentType("text/plain;charset=UTF-8");
				
				String downloadFileName = (String)parameterMap.get("downloadFileName");
				log.info("downloadFileName={}",downloadFileName);
				
				String i18nFileName = i18n.getMsg(downloadFileName);
				log.info("i18nFileName={}",i18nFileName);
				
				if(i18nFileName != null && !"".equals(i18nFileName)){
					downloadFileName = i18nFileName.replaceAll(" ","") + ".txt";
				}
				else{
					downloadFileName = downloadFileName + ".txt";
				}
				log.info("downloadFileName={}",downloadFileName);
				
				String header = request.getHeader("User-Agent").toUpperCase();
				 if (header.contains("FIREFOX")) {
					 response.setHeader("Content-Disposition","attachment;filename="+ URLEncoder.encode(downloadFileName,"UTF-8")+";filename*=utf-8'" + URLEncoder.encode(downloadFileName,"UTF-8"));
				 }else {
					 response.setHeader("Content-Disposition","attachment;filename=" + URLEncoder.encode(downloadFileName,"UTF-8"));
				 }
				//解決Exposure of System Data
				BufferedOutputStream buff = null;
				ServletOutputStream outSTr = null;
				try {
					outSTr = response.getOutputStream();// 建立     
					buff = new BufferedOutputStream(outSTr);
					buff.write(ESAPIUtil.validLongInput(stringBuilder.toString(), "GeneralString", true).getBytes("UTF-8"));
					buff.flush();
					buff.close();
					outSTr.close();
				}catch(Exception e) {
					
				}
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("renderMergedOutputModel error >> {}",e);
		}
		try{
			if(bufferedReader != null){
				bufferedReader.close();
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("renderMergedOutputModel error >> {}",e);
		}
		try{
			if(inputStreamReader != null){
				inputStreamReader.close();
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("renderMergedOutputModel error >> {}",e);
		}
		try{
			if(inputStream != null){
				inputStream.close();
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("renderMergedOutputModel error >> {}",e);
		}
	}
	//將變數KEY換成值或是將I18N的KEY換成值
	private String replaceLineValue(String inputLine,Map<String,?> dataMap){
		log.trace(ESAPIUtil.vaildLog("inputLine >> " + inputLine)); 
		String resultLine = inputLine;
		
		List<String> variableKeysList = new ArrayList<String>();
		String leftSignal = "${";
		String rightSignal = "}";
		
		DownloadUtil.getVariableKeys(variableKeysList,leftSignal,rightSignal,resultLine);
		log.trace("variableKeysList={}",variableKeysList);
		
		//有變數KEY要取代
		for(String key : variableKeysList){
			String dataMapValue = (String)dataMap.get(key.trim());
			log.trace("dataMapValue={}",dataMapValue);
			
			String replaceValue = leftSignal + key + rightSignal;
			log.trace("replaceValue={}",replaceValue);
			
			//有對應的值
			if(dataMapValue != null){
				resultLine = resultLine.replace(replaceValue,dataMapValue);
			}
			//沒有對應的值，代空值
			else{
				resultLine = resultLine.replace(replaceValue,"");
			}
		}
		log.trace(ESAPIUtil.vaildLog("resultLine >> " + resultLine)); 
		
		variableKeysList = new ArrayList<String>();
		leftSignal = "i18n{";
		rightSignal = "}";
		
		DownloadUtil.getVariableKeys(variableKeysList,leftSignal,rightSignal,resultLine);
		log.trace("variableKeysList={}",variableKeysList);
		
		//有I18NKEY要取代
		for(String key : variableKeysList){
			String i18nValue = i18n.getMsg(key.trim());
			log.trace("i18nValue={}",i18nValue);
			
			String replaceValue = leftSignal + key + rightSignal;
			log.trace("replaceValue={}",replaceValue);
			
			//有對應的值
			if(i18nValue != null){
				resultLine = resultLine.replace(replaceValue,i18nValue);
			}
			//沒有對應的值，代空值
			else{
				resultLine = resultLine.replace(replaceValue,"");
			}
		}
		log.trace(ESAPIUtil.vaildLog("resultLine >> " + resultLine)); 
		
		return resultLine;
	}
	
	public I18n getI18n(){
		return i18n;
	}
	public void setI18n(I18n i18n){
		this.i18n = i18n;
	}
}