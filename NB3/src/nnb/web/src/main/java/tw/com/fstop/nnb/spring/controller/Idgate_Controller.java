package tw.com.fstop.nnb.spring.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.service.IdGateService;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.SpringBeanFactory;

@Controller
@RequestMapping(value = "/IDGATE/CHECK")
@SessionAttributes({ SessionUtil.IDGATE_TRANSDATA })
public class Idgate_Controller {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	IdGateService idgateservice;
	
	//---------- AJAX AREA START----------
		/**
		 * 進行手機驗證ajax
		 * 
		 * @param reqParam
		 * @param model
		 * @return
		 */
		@RequestMapping(value = "/phone_validate_ajax", produces = {"application/json;charset=UTF-8"})
		public @ResponseBody BaseResult phone_validate_ajax(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> reqParam, Model model) {
			log.debug("<<<<< Start phone_validate_ajax >>>>>");
			BaseResult bs = null;
			try {
				log.debug("AJAX REQ >>>>> "+CodeUtil.toJson(reqParam));

				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
				
				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get(reqParam.get("adopid") + "_IDGATE");
				bs = idgateservice.goValidate(reqParam, IdgateData);
				Map<String, Object> bsdata= (Map<String, Object>) bs.getData();
				IdgateData.put("sessionid", ((Map<String, Object>) bsdata.get("data")).get("sessionid"));
				IdgateData.put("txnID", ((Map<String, Object>) bsdata.get("data")).get("txnID"));
				IdgateDataSession.put(reqParam.get("adopid") + "_IDGATE", IdgateData);
				SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateDataSession);
				log.debug("AJAX BSDATA >>>>> " + CodeUtil.toJson(bs));
			} catch (Exception e) {
				log.error("", e);
			}
			return bs;
		}
		/**
		 * 取得手機驗證結果ajax
		 * 
		 * @param reqParam
		 * @param model
		 * @return
		 */
		@RequestMapping(value = "/phone_validate_result_ajax", produces = {"application/json;charset=UTF-8"})
		public @ResponseBody BaseResult phone_validate_result_ajax(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
			log.debug("<<<<< Start phone_validate_result_ajax >>>>>");
			BaseResult bs = null;
			try {
				bs=new BaseResult();
				Map<String, Object> okMap = new HashMap<String, Object>();
				okMap.putAll(ESAPIUtil.validStrMap(reqParam));
				log.debug("AJAX REQ >>>>> "+CodeUtil.toJson(okMap));
				Map<String,Object> deviceDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
				//TODO
				log.debug("deviceDataSession >>>>> "+CodeUtil.toJson(deviceDataSession));		
				Map<String,Object> deviceData = (Map<String, Object>) deviceDataSession.get(reqParam.get("ADOPID") + "_IDGATE");
				okMap.putAll(deviceData);
				okMap.put("SESSIONID",deviceData.get("sessionid"));
				okMap.put("TXNID",deviceData.get("txnID"));
				log.debug("DeviceData >>>>> "+CodeUtil.toJson(okMap));
				bs.addData("status", idgateservice.getValidateResult(okMap));
				bs.setResult(true);
				log.debug("AJAX BSDATA >>>>> "+CodeUtil.toJson(bs));
			} catch (Exception e) {
				log.error("", e);
			}
			return bs;
		}	
		/**
		 * 取消手機驗證ajax
		 * 
		 * @param reqParam
		 * @param model
		 * @return
		 */
		@RequestMapping(value = "/cancle_phone_ajax", produces = {"application/json;charset=UTF-8"})
		public @ResponseBody BaseResult cancle_phone_ajax(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
			log.debug("<<<<< Start IDGATE_ajax >>>>>");
			BaseResult bs = null;
			try {
				Map<String, Object> okMap = new HashMap<String, Object>();
				log.debug("AJAX REQ >>>>> "+CodeUtil.toJson(reqParam));
				Map<String,Object> deviceDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
				Map<String,Object> deviceData = (Map<String, Object>) deviceDataSession.get(reqParam.get("ADOPID") + "_IDGATE");
				okMap.putAll(deviceData);
				okMap.put("idgateID",deviceData.get("IDGATEID"));
				bs=idgateservice.cancelValidate(okMap);
				log.debug("AJAX BSDATA >>>>> "+CodeUtil.toJson(bs));
			} catch (Exception e) {
				log.error("", e);
			}
			return bs;
		}
		//---------- AJAX AREA END----------
		
		
		/**
		 * 判斷是否有IDGATE_ALERT_SKIP COOKIE
		 * 
		 * @param reqParam
		 * @param model
		 * @return
		 */
		@RequestMapping(value = "/has_pass_cookie_ajax", produces = {"application/json;charset=UTF-8"})
		public @ResponseBody BaseResult has_pass_Cookie_ajax(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam) {
			log.debug("<<<<< Start has_pass_cookie_ajax >>>>>");
			BaseResult bs = new BaseResult();
			bs.addData("hasPass", "F");
			try {
				Cookie[] cookies = request.getCookies();
				for (Cookie c:cookies) {
					if("IDGATE_ALERT_SKIP".equals(c.getName())) {
						bs.addData("hasPass", "T");
					}
				}
			} catch (Exception e) {
				log.error("", e);
			}
			return bs  ;
		}
		
		
		/**
		 * 設定 IDGATE_ALERT_SKIP cookie
		 * 
		 * @param reqParam
		 * @param model
		 * @return
		 */
		@RequestMapping(value = "/set_pass_cookie_ajax", produces = {"application/json;charset=UTF-8"})
		public @ResponseBody BaseResult set_pass_Cookie_ajax(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam) {
			log.debug("<<<<< Start set_pass_cookie_ajax >>>>>");
			BaseResult bs = new BaseResult();
			try {
				Cookie cookie = new Cookie("IDGATE_ALERT_SKIP", "T");
				
				String env = SpringBeanFactory.getBean("MAIL_SUBJECT_ENV");
				int Age = SpringBeanFactory.getBean("IdgateAlertCookieAge");
				
				//開發測試用 以分鐘計
				if(!"P".equals(env)) {
					 cookie.setMaxAge(Age * 60);
				}else {
					//正式用 以天計
					 cookie.setMaxAge(Age * 24 * 60 * 60); 
				}
				response.addCookie(cookie);
				bs.addData("setCookie", "T");
			} catch (Exception e) {
				log.error("", e);
				bs.addData("setCookie", "F");
			}
			return bs;
		}
		
}
