package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tw.com.fstop.web.util.WebUtil;

/**
 * C013電文RS
 */
public class C013_REST_RS extends BaseRestBean implements Serializable
{
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8458947184749077614L;
	private String CUSIDN;			// 身份證號
	private String BEGDATE;			// 查詢起日
	private String ENDDATE;			// 查詢迄日
	private String DATE;			// 查詢日期YYYMMDD
	private String TIME;			// 查詢時間HHMMSS
	private String CUSNAME;			// 姓名
	private LinkedList<C013_REST_RSDATA> REC;
	
	// 電文沒回但ms_tw回應的資料
	private String CMQTIME;			// 查詢時間
	private String CMRECNUM;		// 資料總數
	private String CMPERIOD;		// 查詢期間
	private String __OCCURS;		// __OCCURS

	// 電文回應但目前用不到的資料
	private String NEXT;			// 資料起始位置
	private String RECNO;			// 讀取筆數
	private String TOTRECNO;		// 總筆數
	private String SNID;			// System Network Identifier
	private String TXNDIID;			// TXN Destination Institute ID
	private String TXNSIID;			// TXN Source Institute ID
	private String TXNIDT;			// TXN Initiate Date and Time
	private String SSUP;			// System Supervisory
	private String FILLER_X3;		// FILLER_X3
	private String SYNCCHK;			// Sync. Check Item
	private String ACF;				// Address Control Field
	private String MSGTYPE;			// Message Type
	private String SYSTRACE;		// System Trace Audit
	private String LOOPINFO;		// LOOPINFO
	private String BITMAPCFG;		// Bit Map Configuration
	private String PROCCODE;		// Processing Code
	private String RESPCOD;			// Response Code
	
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	
	// W100089655 to W100089***
	public String getCUSIDN_F() 
	{
		String result = this.CUSIDN;
		try
		{
			result = WebUtil.hideID(result);
		}
		catch (Exception e)
		{
			log.error("getCUSIDN_F error. CUSIDN >> {}", CUSIDN);
		}
		return result;
	}
	
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getBEGDATE() {
		return BEGDATE;
	}
	public void setBEGDATE(String bEGDATE) {
		BEGDATE = bEGDATE;
	}
	public String getENDDATE() {
		return ENDDATE;
	}
	public void setENDDATE(String eNDDATE) {
		ENDDATE = eNDDATE;
	}
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public LinkedList<C013_REST_RSDATA> getREC() {
		return REC;
	}
	public void setREC(LinkedList<C013_REST_RSDATA> rEC) {
		REC = rEC;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getCMRECNUM() {
		return CMRECNUM;
	}
	public void setCMRECNUM(String cMRECNUM) {
		CMRECNUM = cMRECNUM;
	}
	public String getCMPERIOD() {
		return CMPERIOD;
	}
	public void setCMPERIOD(String cMPERIOD) {
		CMPERIOD = cMPERIOD;
	}
	public String get__OCCURS() {
		return __OCCURS;
	}
	public void set__OCCURS(String __OCCURS) {
		this.__OCCURS = __OCCURS;
	}
	public String getNEXT() {
		return NEXT;
	}
	public void setNEXT(String nEXT) {
		NEXT = nEXT;
	}
	public String getRECNO() {
		return RECNO;
	}
	public void setRECNO(String rECNO) {
		RECNO = rECNO;
	}
	public String getCUSNAME() {
		return CUSNAME;
	}
	
	// hideUsername
	public String getCUSNAME_F() 
	{
		String result = this.CUSNAME;
		try
		{
			result = WebUtil.hideusername1Convert(CUSNAME);
		}
		catch (Exception e)
		{
			log.error("getCUSNAME_F error. CUSNAME >> {}", CUSNAME);
		}
		return result;
	}
	
	public void setCUSNAME(String cUSNAME) {
		CUSNAME = cUSNAME;
	}
	public String getTOTRECNO() {
		return TOTRECNO;
	}
	public void setTOTRECNO(String tOTRECNO) {
		TOTRECNO = tOTRECNO;
	}
	public String getSNID() {
		return SNID;
	}
	public void setSNID(String sNID) {
		SNID = sNID;
	}
	public String getTXNDIID() {
		return TXNDIID;
	}
	public void setTXNDIID(String tXNDIID) {
		TXNDIID = tXNDIID;
	}
	public String getTXNSIID() {
		return TXNSIID;
	}
	public void setTXNSIID(String tXNSIID) {
		TXNSIID = tXNSIID;
	}
	public String getTXNIDT() {
		return TXNIDT;
	}
	public void setTXNIDT(String tXNIDT) {
		TXNIDT = tXNIDT;
	}
	public String getSSUP() {
		return SSUP;
	}
	public void setSSUP(String sSUP) {
		SSUP = sSUP;
	}
	public String getFILLER_X3() {
		return FILLER_X3;
	}
	public void setFILLER_X3(String fILLER_X3) {
		FILLER_X3 = fILLER_X3;
	}
	public String getSYNCCHK() {
		return SYNCCHK;
	}
	public void setSYNCCHK(String sYNCCHK) {
		SYNCCHK = sYNCCHK;
	}
	public String getACF() {
		return ACF;
	}
	public void setACF(String aCF) {
		ACF = aCF;
	}
	public String getMSGTYPE() {
		return MSGTYPE;
	}
	public void setMSGTYPE(String mSGTYPE) {
		MSGTYPE = mSGTYPE;
	}
	public String getSYSTRACE() {
		return SYSTRACE;
	}
	public void setSYSTRACE(String sYSTRACE) {
		SYSTRACE = sYSTRACE;
	}
	public String getLOOPINFO() {
		return LOOPINFO;
	}
	public void setLOOPINFO(String lOOPINFO) {
		LOOPINFO = lOOPINFO;
	}
	public String getBITMAPCFG() {
		return BITMAPCFG;
	}
	public void setBITMAPCFG(String bITMAPCFG) {
		BITMAPCFG = bITMAPCFG;
	}
	public String getPROCCODE() {
		return PROCCODE;
	}
	public void setPROCCODE(String pROCCODE) {
		PROCCODE = pROCCODE;
	}
	public String getRESPCOD() {
		return RESPCOD;
	}
	public void setRESPCOD(String rESPCOD) {
		RESPCOD = rESPCOD;
	}
	
}