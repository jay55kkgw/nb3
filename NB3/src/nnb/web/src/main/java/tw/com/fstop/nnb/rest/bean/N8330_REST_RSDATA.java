package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N8330_REST_RSDATA extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5547058703277021468L;
	
	public String ACN;
	public String MEMO;
	public String UNTNUM;
	public String TYPNUM;
	public String HLHBRH;
	public String UNTTEL;
	public String AMOUNT;
	public String BRHBDT;
	public String TIME;
	public String CUSNUM;
	
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getUNTNUM() {
		return UNTNUM;
	}
	public void setUNTNUM(String uNTNUM) {
		UNTNUM = uNTNUM;
	}
	public String getMEMO() {
		return MEMO;
	}
	public void setMEMO(String mEMO) {
		MEMO = mEMO;
	}
	public String getTYPNUM() {
		return TYPNUM;
	}
	public void setTYPNUM(String tYPNUM) {
		TYPNUM = tYPNUM;
	}
	public String getHLHBRH() {
		return HLHBRH;
	}
	public void setHLHBRH(String hLHBRH) {
		HLHBRH = hLHBRH;
	}
	public String getUNTTEL() {
		return UNTTEL;
	}
	public void setUNTTEL(String uNTTEL) {
		UNTTEL = uNTTEL;
	}
	public String getAMOUNT() {
		return AMOUNT;
	}
	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}
	public String getBRHBDT() {
		return BRHBDT;
	}
	public void setBRHBDT(String bRHBDT) {
		BRHBDT = bRHBDT;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getCUSNUM() {
		return CUSNUM;
	}
	public void setCUSNUM(String cUSNUM) {
		CUSNUM = cUSNUM;
	}
	
	
	
}
