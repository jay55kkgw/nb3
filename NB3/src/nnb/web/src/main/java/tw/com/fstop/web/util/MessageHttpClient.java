package tw.com.fstop.web.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;

public class MessageHttpClient {
   private static final Charset UTF_8 = Charset.forName("UTF-8");


   private String connectTimeout = "5000";
   private String socketTimeout = "6000";


   private RequestConfig requestConfig;
   private CloseableHttpClient httpClient;

   private ObjectMapper objectMapper;

   public MessageHttpClient() {
      this.requestConfig = RequestConfig.custom()
              .setConnectTimeout(Integer.valueOf(connectTimeout))
              .setSocketTimeout(Integer.valueOf(socketTimeout))
              .build();
      this.httpClient = HttpClientBuilder.create()
              .setDefaultRequestConfig(this.requestConfig)
              .build();
      this.objectMapper = new ObjectMapper();
      this.objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
      this.objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
   }

   public String send(String url, HashMap request) throws IOException {

      String jsonData = this.serialize(request);
      HttpPost httpPost = new HttpPost(url);
      httpPost.setConfig(this.requestConfig);
      httpPost.setEntity(new StringEntity(jsonData, UTF_8));
      httpPost.setHeader("Content-type", "application/json; charset=utf-8");
      CloseableHttpResponse response = null;

      try {
         response = this.httpClient.execute(httpPost);
         return getResponseString(response);
      } finally {
         if (response != null) {
            EntityUtils.consumeQuietly(response.getEntity());
            response.close();
         }

      }


   }

   private String serialize(Object request) throws IllegalArgumentException {
      try {
         return this.objectMapper.writeValueAsString(request);
      } catch (JsonProcessingException var4) {
         StringBuilder sb = new StringBuilder();
         sb.append("Bad input data to JSON serialization");
         if (var4.getLocation() != null) {
            sb.append("at line:").append(var4.getLocation().getLineNr())
                    .append(" column").append(var4.getLocation().getColumnNr());
         } else {
            sb.append(".").append(var4.getMessage());
         }

         throw new IllegalArgumentException(sb.toString(), var4);
      }
   }

   private <T> T deserialize(String s, Class<T> clazz) throws IOException {
      return this.objectMapper.readValue(s, clazz);
   }

   private String getResponseString(CloseableHttpResponse response) throws IOException {
      String responseString = null;
      if (response.getEntity() != null) {
         responseString = EntityUtils.toString(response.getEntity(), UTF_8);
      }

      return responseString;
   }
}
