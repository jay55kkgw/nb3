package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N556_REST_RQ extends BaseRestBean_FX implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2946662680714274751L;

	String CUSIDN;			//統一編號
	String LCNO;			//信用狀號碼
	String TRNSRC = "NB";	//交易來源

	String ADOPID;
	
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getLCNO() {
		return LCNO;
	}
	public void setLCNO(String lCNO) {
		LCNO = lCNO;
	}
	public String getTRNSRC() {
		return TRNSRC;
	}
	public void setTRNSRC(String tRNSRC) {
		TRNSRC = tRNSRC;
	}
}
