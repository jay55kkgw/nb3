package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class NA60_REST_RQ extends BaseRestBean_GOLD implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2272024503630307719L;
	
	private String DATE;//日期YYYMMDD
	private String TIME;//時間HHMMSS
	private String PPSYNCN;//P.P.Key Sync.Check Item
	private String BNKRA;//行庫別
	private String XMLCA;//CA識別碼
	private String XMLCN;//憑證ＣＮ
	private String KIND;//交易機制 1:晶片2: 簽章
	private String CUSIDN;//統一編號
	private String TYPE;//申請作業類別
	private String ICSEQ;//晶片卡交易序號
	private String TRNSRC;//交易來源
	private String GDACN;//黃金存摺帳戶
	private String SVACN;//約定入扣款帳號
	private String CHIP_ACN;//晶片卡主帳號

	private String icSeq;
	private String accNo;
	private String UID;
	private String FGTXWAY;
	private String ACN;
	private String pkcs7Sign;	// IKEY
	private String jsondc;		// IKEY
	private String ISSUER;
	private String TRMID;
	private String TAC;
	private String CMTRANPAGE;
	private String ADOPID;
	private String braCode;
	private String chk;
	
	public String getISSUER() {
		return ISSUER;
	}
	public void setISSUER(String iSSUER) {
		ISSUER = iSSUER;
	}
	public String getTRMID() {
		return TRMID;
	}
	public void setTRMID(String tRMID) {
		TRMID = tRMID;
	}
	public String getTAC() {
		return TAC;
	}
	public void setTAC(String tAC) {
		TAC = tAC;
	}
	public String getCMTRANPAGE() {
		return CMTRANPAGE;
	}
	public void setCMTRANPAGE(String cMTRANPAGE) {
		CMTRANPAGE = cMTRANPAGE;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getBraCode() {
		return braCode;
	}
	public void setBraCode(String braCode) {
		this.braCode = braCode;
	}
	public String getChk() {
		return chk;
	}
	public void setChk(String chk) {
		this.chk = chk;
	}
	public String getPkcs7Sign() {
		return pkcs7Sign;
	}
	public void setPkcs7Sign(String pkcs7Sign) {
		this.pkcs7Sign = pkcs7Sign;
	}
	public String getJsondc() {
		return jsondc;
	}
	public void setJsondc(String jsondc) {
		this.jsondc = jsondc;
	}
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getPPSYNCN() {
		return PPSYNCN;
	}
	public void setPPSYNCN(String pPSYNCN) {
		PPSYNCN = pPSYNCN;
	}
	public String getBNKRA() {
		return BNKRA;
	}
	public void setBNKRA(String bNKRA) {
		BNKRA = bNKRA;
	}
	public String getXMLCA() {
		return XMLCA;
	}
	public void setXMLCA(String xMLCA) {
		XMLCA = xMLCA;
	}
	public String getXMLCN() {
		return XMLCN;
	}
	public void setXMLCN(String xMLCN) {
		XMLCN = xMLCN;
	}
	public String getKIND() {
		return KIND;
	}
	public void setKIND(String kIND) {
		KIND = kIND;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getTYPE() {
		return TYPE;
	}
	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}
	public String getICSEQ() {
		return ICSEQ;
	}
	public void setICSEQ(String iCSEQ) {
		ICSEQ = iCSEQ;
	}
	public String getTRNSRC() {
		return TRNSRC;
	}
	public void setTRNSRC(String tRNSRC) {
		TRNSRC = tRNSRC;
	}
	public String getGDACN() {
		return GDACN;
	}
	public void setGDACN(String gDACN) {
		GDACN = gDACN;
	}
	public String getSVACN() {
		return SVACN;
	}
	public void setSVACN(String sVACN) {
		SVACN = sVACN;
	}
	public String getCHIP_ACN() {
		return CHIP_ACN;
	}
	public void setCHIP_ACN(String cHIP_ACN) {
		CHIP_ACN = cHIP_ACN;
	}
	public String getIcSeq() {
		return icSeq;
	}
	public void setIcSeq(String icSeq) {
		this.icSeq = icSeq;
	}
	public String getAccNo() {
		return accNo;
	}
	public void setAccNo(String accNo) {
		this.accNo = accNo;
	}
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
}
