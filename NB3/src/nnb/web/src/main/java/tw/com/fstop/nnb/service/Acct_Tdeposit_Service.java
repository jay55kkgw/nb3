package tw.com.fstop.nnb.service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.JsonSyntaxException;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.util.StrUtil;

/**
 * 
 * 功能說明 :台幣定存相關邏輯
 *
 */
@Service
public class Acct_Tdeposit_Service extends Base_Service
{
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private I18n i18n;

	/**
	 * 取得台幣定存轉出帳號
	 * 
	 * @param cusidn
	 * @return
	 */
	public BaseResult getOutAcn(String cusidn)
	{
		BaseResult bs = new BaseResult();
		try
		{
			// OutAcno
			List<String> outAcnoList = null;
			// CALL 電文 取得定存轉出帳號
			bs = N920_REST(cusidn, OUT_ACNO);
			// 後製把REC 裡面的資料轉成下拉選單所要的格式
			if (bs != null && bs.getResult())
			{
				bs.addData("outAcnoList", outAcnoList);
				// 設定頁面預約日期為明天，如果當前時間太晚則預約後天
				bs.addData("tmr", DateUtil.getTomorrowOrAfterTomorrowDate());
				// 可預約次日起一年內之交易
				String nextYearDay = new DateTime().plusYears(1).toString("yyyy/MM/dd");
				bs.addData("nextYearDay", nextYearDay);
				// 可預約次日起一年內之交易
				String nextMonth = new DateTime().plusMonths(1).toString("yyyy/MM/dd");
				bs.addData("nextMonth", nextMonth);
			}
			log.trace("getOutAcnoAndAgreeAcno bs.getData()>> {}", bs.getData());
		}
		catch (Exception e)
		{
			log.error("Acct_Tdeposit_Service getOutAcnoAndAgreeAcno error : {}", e);
		}
		return bs;
	}

	/**
	 * callN420後加工
	 * 
	 * @param cusidn
	 * @param ntype N077,N078
	 * @return
	 */
	public BaseResult getCallN420Data(String cusidn, String ntype)
	{
		BaseResult bs = new BaseResult();
		try
		{
			// Call 中台電文
			bs = N420_REST(cusidn);
			// 處理回傳的BS
			if (bs != null && bs.getResult())
			{
				// 後製
				List<Map<String, String>> dataListMap = ((List<Map<String, String>>) ((Map<String, Object>) bs.getData()).get("REC"));
				// 後製
				log.debug("getCallN420Data dataListMap IN>>>{}", dataListMap);
				// 依條件移除資料
				Iterator<Map<String, String>> iterator = dataListMap.iterator();
				Date date = new Date();
				log.debug("N078_ACNO >>>{}", date);

				switch (ntype)
				{

					case N077_ACNO:
						while (iterator.hasNext())
						{
							Map<String, String> map = iterator.next(); // must be called before you can call
																		// i.remove()
							String type = map.get("TYPE");
							// 如果type=4(整存零付) ||5(零存整付)不顯示
							if ("4".equals(type) || "5".equals(type))
							{
								iterator.remove();
							}
						}
					case N078_ACNO:

						while (iterator.hasNext())
						{
							Map<String, String> map = iterator.next(); // must be called before you can call
																		// i.remove()
							String acn = map.get("ACN");
							String type = map.get("TYPE");
							String subject = acn.substring(3, 4);
							Date dueDate = DateUtil.twconvertDate(map.get("DUEDAT"));
							// 如果是綜存定存不顯示
							if ("1".equals(subject) || "6".equals(subject))
							{
								iterator.remove();
							}
							// 如果到期日大於今日不顯示
							else if (dueDate.after(date))
							{
								iterator.remove();
							}
							// 如果type=4(整存零付) ||5(零存整付)不顯示
							else if ("4".equals(type) || "5".equals(type))
							{
								iterator.remove();
							}
						}
					default:
						log.warn("getCallN420Data type is {}", ntype);
						break;
				}
				log.debug("getCallN420Data dataListMap Out>>>{}", dataListMap);
				// 用整理後的筆數 覆蓋CMRECNUM
				bs.addData("CMRECNUM", dataListMap.size());
				// 後製如果有資料才後製
				if (dataListMap != null && dataListMap.size() > 0)
				{

					// 後製 資料內容
					for (Map<String, String> map : dataListMap)
					{
						// Get Bean Map Value
						String getTypeValue = this.getBeanMapValue("n420", "TYPE" + map.get("TYPE"));
						// use getTypeValue To get i18n value
						String getTypeName = i18n.getMsg(getTypeValue);
						String dpinttype = intmthConvert(map.get("INTMTH"));// 用i18n取代原本的計息中文
						String showDpisdt = this.dateAddSlash(map.get("DPISDT"));
						String showDuedat = this.dateAddSlash(map.get("DUEDAT"));
						// 加入新的值到原本的Map
						// map.put("SHOWAUTXFTM", "999".equals(map.get("AUTXFTM")) ? "無限" : map.get("AUTXFTM"));
						map.put("SHOWAUTXFTM", "999".equals(map.get("AUTXFTM")) ? i18n.getMsg("LB.Unlimited_1")
								: map.get("AUTXFTM"));
						map.put("TYPENAME", getTypeName);
						map.put("DPINTTYPE", dpinttype);
						map.put("SHOWDPISDT", showDpisdt);
						map.put("SHOWDUEDAT", showDuedat);
						map.put("JSON", CodeUtil.toJson(map));
					}

				}
				else
				{
					String msgCode = "ENRD";
					String msgName = i18n.getMsg("LB.Check_no_data");
					bs.setMessage(msgCode, msgName);
					bs.setResult(Boolean.FALSE);
				}
			}

			log.debug("getCallN420Data BaseResult getData :{}", bs.getData());
		}
		catch (

		Exception e)
		{
			log.error("~~~ Acct_Tdeposit_Service getCallN420Data~~Error : ", e);
		}
		return bs;
	}

	/**
	 * N074轉入臺幣綜存定存
	 * 
	 * @param reqParam
	 * @return
	 */
	public BaseResult deposit_transfer_confirm(Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{
			String dpsvTypeName = "";// 續存方式 1:本金轉期，利息轉入綜存活存；2:本金及利息一併轉期
			String codeName = "";// 到期是否轉期
			String term_text = "";// 存款期別顯示
			String transferInAccount = "";// BHO 用
			String fdpaccName = fdpaccConvert(reqParam.get("FDPACC"));// 存款種類 1:定期存款, 2:定期儲蓄存款存本取息,3:定期儲蓄存款整存整付
			String intmthName = intmthConvert(reqParam.get("INTMTH"));// 計息方式 ，0機動，1固定 ;
			// 預約日期註記 1:即時 2:預約
			if (StrUtil.isNotEmpty(reqParam.get("FGTXDATE")) && (reqParam.get("FGTXDATE").equals("1")))
			{
				reqParam.put("transfer_date", DateUtil.getDate("/"));
			}
			if (StrUtil.isNotEmpty(reqParam.get("FGTXDATE")) && (reqParam.get("FGTXDATE").equals("2")))
			{
				reqParam.put("transfer_date", reqParam.get("CMDATE"));
			}
			// 續存方式 1:本金轉期，利息轉入綜存活存；2:本金及利息一併轉期
			//
			switch (reqParam.get("FGSVTYPE"))
			{
				case "1":
					dpsvTypeName = this.getI18nValeByAcctTdposit("DPSVTYPE1NAMETR");
					break;
				case "2":
					dpsvTypeName = this.getI18nValeByAcctTdposit("DPSVTYPE2NAME");
					break;
				default:
					break;
			}
			// 到期是否轉期
			switch (reqParam.get("CODE"))
			{
				case "0":
					codeName = this.getI18nValeByAcctTdposit("CODENAME");// "不轉期";
					break;
				case "1":
					String txtimesText = reqParam.get("TXTIMES_TEXT");
					codeName = this.getI18nValeByAcctTdposit("CODE1NAME") + "&nbsp;" + txtimesText;// "轉期"+次數;
				default:
					break;
			}
			// 存款期別
			switch (reqParam.get("FGTDPERIOD"))
			{
				case "1":
					term_text = reqParam.get("TERM_TEXT");
					break;
				case "2":
					term_text = reqParam.get("CMDATE1");
					break;
			}
			// 約定否 1：約定 2：非約定
			String FGSVACNO = reqParam.get("FGSVACNO");
			switch (FGSVACNO)
			{
				case "1":
					Map<String, String> DPAGACNOMap = jsonConvertMap(reqParam.get("DPAGACNO"));
					transferInAccount = DPAGACNOMap.get("ACN");
					reqParam.put("ADDACN", "false");
					break;
				case "2":
					transferInAccount = reqParam.get("DPACNO");
					reqParam.put("INTSACN", reqParam.get("DPACNO"));
					reqParam.put("DPACNO", reqParam.get("DPACNO"));
					// 判斷是否點選加入常用帳號
					if ("on".equals(reqParam.get("ADDACN")))
					{
						reqParam.put("ADDACN", "true");
					}
					else
					{
						reqParam.put("ADDACN", "false");
					}
					break;
			}
			// 塞資料
			reqParam.put("transferInAccount", transferInAccount);
			reqParam.put("TERM_TEXT", term_text);
			reqParam.put("CODENAME", codeName);
			reqParam.put("INTMTHNAME", intmthName);
			reqParam.put("FDPACCNAME", fdpaccName);
			reqParam.put("DPSVTYPENAME", dpsvTypeName);

			bs.setData(reqParam);
			bs.setResult(true);
			log.trace(ESAPIUtil.vaildLog("bs getData >> " + CodeUtil.toJson(bs.getData())));
		}
		catch (Exception e)
		{
			log.error("deposit_transfer_confirm Ex : >>>>{}", e);
		}
		return bs;
	}

	/**
	 * N074轉入臺幣綜存定存
	 * 
	 * @param cusidn
	 * @param dpMyEmail
	 * @param reqParam
	 * @return
	 */
	public BaseResult deposit_transfer_result(String cusidn, String dpMyEmail, Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{
			// 預約即時/註記
			String fgtxdate = reqParam.get("FGTXDATE");
			reqParam.put("CMTRMAIL", dpMyEmail);// session 信箱
			reqParam.put("CUSIDN", cusidn);
			//TXNLOG
			String FGSVACNO = reqParam.get("FGSVACNO");
			if(FGSVACNO.equals("1")) {
				Map<String, String> DPAGACNOMap = jsonConvertMap(reqParam.get("DPAGACNO"));
				reqParam.put("INTSACN", DPAGACNOMap.get("ACN"));
			}
			else if(FGSVACNO.equals("2")) {
				reqParam.put("INTSACN", reqParam.get("DPACNO"));
			}
			// Call REST電文
			bs = N074_REST(reqParam);
			if (bs != null && bs.getResult())
			{
				// 後製
				Map<String, String> getDataMap = (Map<String, String>) bs.getData();
				//
				getDataMap.put("SDT", this.dateAddSlash(getDataMap.get("SDT")));
				getDataMap.put("DUEDATE", this.dateAddSlash(getDataMap.get("DUEDATE")));
				getDataMap.put("CMDATE", DateUtil.convertDate(2, getDataMap.get("CMDATE"), "yyyyMMdd", "yyyy/MM/dd"));
				getDataMap.put("FDPACC", fdpaccConvert(getDataMap.get("FDPACC")));
				getDataMap.put("INTMTH", intmthConvert(getDataMap.get("INTMTH")));
				// 前端頁面傳入的值
				getDataMap.put("FGTXDATE", fgtxdate);
				getDataMap.put("CMTRMEMO", reqParam.get("CMTRMEMO"));// 交易備註
				getDataMap.put("DPAGACNO_TEXT", reqParam.get("DPAGACNO_TEXT"));// 轉入帳號
				getDataMap.put("transfer_date", reqParam.get("transfer_date"));// 轉帳日期
				getDataMap.put("TERM_TEXT", reqParam.get("TERM_TEXT"));// 存款期別顯示
				getDataMap.put("CODENAME", reqParam.get("CODENAME"));// 到期是否轉期
				getDataMap.put("DPSVTYPENAME", reqParam.get("DPSVTYPENAME"));// 續存方式
			}

			log.trace("call_N074 {} ", bs.getData());
		}
		catch (Exception e)
		{
			log.error("deposit_transfer_result Exception >>>{}", e);
		}
		return bs;
	}

	/**
	 * N077臺幣定存自動轉期申請/變更進入頁 callN420後加工*************************************
	 * 
	 * @param cusidn
	 * @return
	 */
	public BaseResult renewal_apply(String cusidn)
	{
		BaseResult bs = new BaseResult();
		try
		{
			// callN420DataRetouchForN077
			bs = getCallN420Data(cusidn, N077_ACNO);
			log.debug("callN420DataRetouchForN077 BaseResult getData :{}", bs.getData());
		}
		catch (Exception e)
		{
			log.error("~~~callN420DataRetouchForN077~~Error : ", e);
		}
		return bs;
	}

	/**
	 * N077取得 臺幣定存自動轉期申請/變更進入頁的值
	 * 
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult renewal_apply_step1(String cusidn, Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{
			log.debug(ESAPIUtil.vaildLog("renewal_apply_step1 In >> " + CodeUtil.toJson(cusidn)));
			// get radio button value
			String fgselctJson = reqParam.get("FGSELECT");
			// JsonString TO Map
			Map<String, String> fgselectMap = this.jsonConvertMap(fgselctJson);
			// Get BaseResult
			bs = N920_REST(cusidn, ACNO);
			if (bs != null && bs.getResult())
			{
				bs.addData("fgselctJson", fgselctJson);
				bs.addData("fgselectMap", fgselectMap);
			}
			log.debug("~~~renewal_apply_step1~~Out : {}", bs.getData());
		}
		catch (Exception e)
		{
			log.error("~~~renewal_apply_step1~~Error : ", e);
		}
		return bs;
	}

	/**
	 * N077 取得 臺幣定存自動轉期申請/變更輸入頁的值
	 * 
	 * @param reqParam
	 * @return
	 */
	public BaseResult renewal_apply_confirm(Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{
			log.trace(ESAPIUtil.vaildLog("renewal_apply_confirm reqParamInput >> " + reqParam));
			// 續存方式 :"1":本金轉期，利息轉入帳號;"2":本金及利息一併轉期
			String fgsvType = reqParam.get("FGSVTYPE");// 續存方式
			String fgsvName = fgsvTypeConvert(fgsvType); // 頁面顯示的續存方式
			// 選擇2本金及利息一併轉期 轉入帳號 dpsvAcno: ""字串
			String dpsvAcno = "1".equals(fgsvType) ? reqParam.get("DPSVACNO") : "";// 本金轉期，利息轉入帳號
			// 轉期次數 :"1":無限次數 ; "2":有限次數 ; "3":取消自動轉期
			// "3":取消自動轉期，請確認取消自動轉期資料
			String fgrencnt = reqParam.get("FGRENCNT");// 是否自動轉期
			String dprencnt = reqParam.get("DPRENCNT");// 有限次數的次數
			String fgrenName = "";// 頁面顯示轉期方式
			switch (fgrencnt)
			{
				case "1":
					fgrenName = this.getI18nValeByAcctTdposit("DPRENCNT1NAME");
					dprencnt = "";// 無限次數
					break;
				case "2":
					fgrenName = this.getI18nValeByAcctTdposit("DPRENCNT2NAME");
					break;
				case "3":
					fgrenName = this.getI18nValeByAcctTdposit("DPRENCNT3NAME");
					dprencnt = "";// 取消自動轉期
					break;
				default:
					break;
			}
			fgrenName = fgrenName + dprencnt;// 頁面顯示轉期方式+次數
			// jsonString To Map
			Map<String, String> resParam = this.jsonConvertMap(reqParam.get("REQPARAMJSON"));
			// 電文回應的不可以I18n判斷
			resParam.put("DPRENCNT", dprencnt);// 有限次數的次數
			resParam.put("FGSVTYPE", fgsvType);// 續存方式:1；2
			resParam.put("DPSVACNO", dpsvAcno);// 本金轉期，利息轉入帳號
			resParam.put("FGRENCNT", fgrencnt);// 是否自動轉期
			resParam.put("FGSVNAME", fgsvName);// 頁面顯示的續存方式
			resParam.put("FGRENNAME", fgrenName);// 頁面顯示的 轉期次數 :"1":無限次數 ;"3":取消自動轉期
			bs.setData(resParam);
			bs.addData("resParamJson", CodeUtil.toJson(resParam));
			bs.setResult(true);
			log.debug("renewal_apply_confirmDataOut : {}", bs.getData());
		}
		catch (Exception e)
		{
			log.error("~~~renewal_apply_confirmError : ", e);
		}
		return bs;
	}

	/**
	 * N077取得 臺幣定存自動轉期申請/變更回應資料
	 * 
	 * @param sessionId
	 * @param cUSIDN
	 * @param reqParam
	 * @return
	 */
	public BaseResult renewal_apply_result(String cusidn, Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{
			// 存單帳號
			String fDPACN = reqParam.get("ACN");
			// 存款種類
			String fdpType = reqParam.get("TYPE");
			// 轉帳帳號
			String tsfAcn = reqParam.get("DPSVACNO");
			// 整理上行參數
			reqParam.put("CUSIDN", cusidn);/// 統一編號
			reqParam.put("FDPACN", fDPACN);// 存單帳號
			reqParam.put("FDPTYPE", fdpType);// 存款種類
			reqParam.put("TSFACN", tsfAcn); // 利息轉入帳號
			// 將輸入塞入回應的值
			String fgRencat = reqParam.get("FGRENCNT");// 轉期次數 :"1":無限次數 ; "2":有限次數 ; "3":取消自動轉期
			String fgsvName = reqParam.get("FGSVNAME");// 頁面顯示的續存方式
			String fgrenName = reqParam.get("FGRENNAME"); // 頁面顯示的 轉期次數
			reqParam.put("ADSVBH","050");
			reqParam.put("ADAGREEF", "0");
			// call N077_REST
			bs = N077_REST(reqParam);

			// 後製只有一筆不用 forEach
			Map<String, String> getDataMap = (Map<String, String>) bs.getData();
			// 計息方式 ，0機動，1固定
			getDataMap.put("INTMTH", intmthConvert(getDataMap.get("INTMTH")));
			// 存單種類
			getDataMap.put("FDPACC", fdpaccConvert(getDataMap.get("FDPACC")));
			// 將輸入塞入回應
			getDataMap.put("FGRENCNT", fgRencat);// 轉期次數 :"1":無限次數 ; "2":有限次數 ; "3":取消自動轉期
			getDataMap.put("FGSVNAME", fgsvName);// 頁面顯示的續存方式
			getDataMap.put("FGRENNAME", fgrenName); // 頁面顯示的 轉期次數
		}
		catch (Exception e)
		{
			log.error("getOrderRenewalResultError", e);
		}
		return bs;
	}

	/**
	 * N078callN420後加工
	 * 
	 * @param cusidn
	 * @return
	 */
	public BaseResult order_renewal(String cusidn)
	{
		BaseResult bs = new BaseResult();
		try
		{
			// Call 中台電文
			bs = getCallN420Data(cusidn, N078_ACNO);
			log.debug("callN420DataRetouchForN078 BaseResult getData :{}", bs.getData());
		}
		catch (Exception e)
		{
			log.error("~~~callN420DataRetouchForN078~~Error : ", e);
		}
		return bs;

	}

	/**
	 * N078取得 臺幣定存單到期續存進入頁的值*******************************************************
	 * 
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult order_renewal_step1(String cusidn, Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{
			log.trace(ESAPIUtil.vaildLog("getOrderRenewalData In >> " + CodeUtil.toJson(reqParam)));
			// get radio button value
			String fgselctJson = reqParam.get("FGSELECT");
			// Get Page JsonString Value creation resParam
			Map<String, String> fgselectMap = jsonConvertMap(fgselctJson);
			// 取得臺幣存款帳號
			bs = N920_REST(cusidn, ACNO);
			bs.addData("fgselctJson", fgselctJson);
			bs.addData("fgselectMap", fgselectMap);
			log.debug("~~~getOrderRenewalData~~Out : {} ", bs.getData());
		}
		catch (Exception e)
		{
			log.error("~~~getOrderRenewalData~~Error : ", e);
		}
		return bs;
	}

	/**
	 * 取得 臺幣定存單到期續存輸入頁的值
	 * 
	 * @param reqParam
	 * @return
	 */
	public BaseResult order_renewal_confirm(Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{
			String intMth = reqParam.get("INTMTH");// 計息方式 ，0機動，1固定
			String fgsvType = reqParam.get("FGSVTYPE");// 續存方式:1；2
			String dpsvAcno = "1".equals(fgsvType) ? reqParam.get("DPSVACNO") : "";// 本金轉期，利息轉入帳號 選擇2本金及利息一併轉期 轉入帳號為""字串
			String fgsvName = fgsvTypeConvert(fgsvType);// 頁面顯示的續存方式
			//
			Map<String, String> resParam = jsonConvertMap(reqParam.get("REQPARAMJSON"));// JsonString Value TO map
			resParam.put("INTMTH", intMth);// 計息方式: 0；1
			resParam.put("DPINTTYPE", intmthConvert(intMth));// 頁面顯示計息方式 :，0機動，1固定
			resParam.put("FGSVTYPE", fgsvType);// 續存方式:1；2
			resParam.put("DPSVACNO", dpsvAcno);// 本金轉期，利息轉入帳號
			resParam.put("FGSVNAME", fgsvName);// 頁面顯示的續存方式
			//
			bs.setData(resParam);
			bs.setResult(true);
			log.debug(ESAPIUtil.vaildLog("getOrderRenewalStep1Data  BaseResult getData >> "
					+ CodeUtil.toJson(bs.getData())));
		}
		catch (Exception e)
		{
			log.error("getOrderRenewalStep1DataErro : ", e);
		}
		return bs;
	}

	/**
	 * N078取得 臺幣定存單到期續存 回應資料*********************************
	 * 
	 * @param cUSIDN
	 * @param reqParam
	 * @return
	 */
	public BaseResult order_renewal_result(String cUSIDN, Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{
			// 續存方式
			String fgsvName = reqParam.get("FGSVNAME");
			// 轉入帳號
			String dpsvAcno = reqParam.get("DPSVACNO");
			// 存單帳號
			String fDPACN = reqParam.get("ACN");
			// 存單金額格式化
			DecimalFormat df = new DecimalFormat("#,##0.00");
			Double doubleAmtfdp = Double.valueOf(reqParam.get("AMTFDP"));
			String aMTFDP = df.format(doubleAmtfdp);
			// 建立電文的Map
			reqParam.put("CUSIDN", cUSIDN);// 使用者身分證
			reqParam.put("FDPACN", fDPACN); // 存單帳號
			reqParam.put("AMTFDP", aMTFDP);// 存單金額
			reqParam.put("TXID", "N078_1");//
			// Call中台電文
			reqParam.put("ADSVBH","050");
			reqParam.put("ADAGREEF", "0");
			bs = N078_REST(reqParam);
			if (bs != null && bs.getResult())
			{
				// 後置回應電文
				Map<String, String> map = (Map<String, String>) bs.getData();
				String showSdt = this.dateAddSlash(map.get("SDT"));
				String showDuedat = this.dateAddSlash(map.get("DUEDAT"));
				map.put("SDT", showSdt);
				map.put("DUEDAT", showDuedat);
				map.put("FGSVNAME", fgsvName);
				map.put("FDPACC", fdpaccConvert(map.get("FDPACC")));
				map.put("INTMTH", intmthConvert(map.get("INTMTH")));
				map.put("DPSVACNO", dpsvAcno);
			}

			log.debug("order_renewal_result >>>>{}", bs.getData());
		}
		catch (Exception e)
		{
			log.error("~~~order_renewal_result >>>>Error", e);
		}
		return bs;
	}

	/**
	 * N079台幣綜存定存解約取得第一頁所需的資料
	 * 
	 * @param sessionId
	 * @param pwd
	 * @return
	 */
	public BaseResult deposit_cancel(String cusidn, Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{
			// 得到解約清單
			bs = N421_REST(cusidn);
			if (bs != null && bs.getResult())
			{
				// return data 資料加工
				Map<String, ArrayList<Map<String, String>>> getDataMap = (Map<String, ArrayList<Map<String, String>>>) bs.getData();
				ArrayList<Map<String, String>> mapTableValue = (ArrayList<Map<String, String>>) getDataMap.get("REC");
				ArrayList<Map<String, String>> returnMapList = new ArrayList<>();
				for (Map<String, String> map : mapTableValue)
				{
					// 找出單一筆
					if (null != reqParam.get("FDPNUM"))
					{
						if (map.get("FDPNUM").equals(reqParam.get("FDPNUM"))
								&& map.get("ACN").equals(reqParam.get("ACN")))
						{
							returnMapList.add(deposite_List_processing(map));
							bs.addData("REC", returnMapList);
							bs.addData("defaultCheck", "TRUE");
							bs.addData("COUNT", String.valueOf(returnMapList.size()));
						}
						else
						{

						}
						// 全部列表
					}
					else
					{
						deposite_List_processing(map);
						bs.addData("COUNT", String.valueOf(mapTableValue.size()));
					}
				}
			}

		}
		catch (Exception e)
		{
			log.error("~~~deposit_cancel~~Error : ", e);
		}
		return bs;
	}

	public Map deposite_List_processing(Map<String, String> map)
	{
		// 轉換電文回應日期 EX:107810--->>107/08/10
		String showDpisdt = this.dateAddSlash(map.get("DPISDT"));
		String showDuedat = this.dateAddSlash(map.get("DUEDAT"));
		// 取得BeanMap中的Value
		String getTypeValue = this.getBeanMapValue("n420", "TYPE" + map.get("TYPE"));
		// 用BeanMap 取得i18n檔的值
		String getTypeName = i18n.getMsg(getTypeValue);
		// 計息方式 ，0機動，1固定 頁面顯示用
		String intmthName = intmthConvert(map.get("INTMTH"));
		String unlimited_1 = i18n.getMsg("LB.Unlimited_1");
		// 覆蓋原本的Map
		map.put("SHOWAUTXFTM", "999".equals(map.get("AUTXFTM")) ? unlimited_1 : map.get("AUTXFTM"));
		map.put("SHOWDPISDT", showDpisdt);
		map.put("SHOWDUEDAT", showDuedat);
		map.put("TYPENAME", getTypeName);
		map.put("INTMTHNAME", intmthName);
		map.put("JSON", CodeUtil.toJson(map));
		return map;
	}

	/**
	 * N079台幣綜存定存解約取得確認頁所需的資料
	 * 
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult deposit_cancel_confirm(String cusidn, Map<String, String> reqParam)
	{
		log.debug("deposit_cancel_confirm Service statr !!!~~~");
		BaseResult bs = new BaseResult();
		try
		{
			// Get Page JsonString Value creation newReqMap
			Map<String, String> newReqMap = this.jsonConvertMap(reqParam.get("FGSELECT"));
			// 將頁面的map 放到 newReqMap
			newReqMap.putAll(reqParam);
			String fdpAcn = newReqMap.get("ACN");
			String fdpType = newReqMap.get("TYPE");
			String amtfdp = newReqMap.get("AMTFDP");
			// put pageJson Value To newReqMap
			newReqMap.put("FDPACN", fdpAcn); // 帳號
			newReqMap.put("FDPTYPE", fdpType); // 存款種類
			newReqMap.put("AMT", amtfdp); // 存單金額
			// Put Session Value To New map
			newReqMap.put("CUSIDN", cusidn); // 統一編號
			// call_N079 電文 >>>得到台幣綜存定存解約 Data
			bs = N079_REST(WSN079GETCANCELDATA, newReqMap,false);
			if (bs != null && bs.getResult())
			{
				// 後製只有一筆不用 forEach
				Map<String, String> getDataMap = (Map<String, String>) bs.getData();
				String showDpisdt = this.dateAddSlash(getDataMap.get("DPISDT"));
				String showDuedat = this.dateAddSlash(getDataMap.get("DUEDAT"));
				// 取得Map中的Value
				String getTypeValue = this.getBeanMapValue("n420", "TYPE" + getDataMap.get("FDPTYPE"));
				// 用Map Value 取得i18n檔的值
				String getTypeName = i18n.getMsg(getTypeValue);
				// 計息方式網頁顯示 計息方式 ，0機動，1固定
				String intmthName = intmthConvert(getDataMap.get("INTMTH"));
				// 覆蓋原本的Map
				getDataMap.put("CMTRMEMO", newReqMap.get("CMTRMEMO"));
				getDataMap.put("CMMAILMEMO", newReqMap.get("CMMAILMEMO"));
				getDataMap.put("DPISDTSHOW", showDpisdt);// 日期
				getDataMap.put("DUEDATSHOW", showDuedat);// 日期
				getDataMap.put("FDPTYPENAME", getTypeName);// 存款種類
				getDataMap.put("INTMTHNAME", intmthName);// 計息方式
				getDataMap.put("JSON", CodeUtil.toJson(getDataMap));

			}

		}
		catch (Exception e)
		{
			log.error("~~~deposit_cancel_confirm~~Error : ", e);
		}
		return bs;
	}

	/**
	 * N079台幣綜存定存解約
	 * 
	 * @param cusidn
	 * @param dpMyEmail
	 * @param pageReqParam
	 * @return
	 */
	public BaseResult deposit_cancel_result(String cusidn, String dpMyEmail, Map<String, String> pageReqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{
			Map<String, String> jsonMap = this.jsonConvertMap(pageReqParam.get("FGSELECT"));
			pageReqParam.putAll(jsonMap);
			// Session
			pageReqParam.put("CMTRMAIL", dpMyEmail); // Email信箱
			pageReqParam.put("CUSIDN", cusidn);
			// 回應電文
			pageReqParam.put("ADSVBH","050");
			pageReqParam.put("ADAGREEF", "0");
			bs = N079_REST(WSN079ACTION, pageReqParam,true);
			if (bs != null && bs.getResult())
			{
				// 後製只有一筆不用 forEach
				Map<String, String> getDataMap = (Map<String, String>) bs.getData();
				// 覆蓋原本的Map
				getDataMap.put("CMTRMEMO", jsonMap.get("CMTRMEMO"));
				getDataMap.put("SHOWDPISDT", this.dateAddSlash(getDataMap.get("DPISDT")));
				getDataMap.put("SHOWDUEDAT", this.dateAddSlash(getDataMap.get("DUEDAT")));
				getDataMap.put("FDPTYPE", this.fdpaccConvert(getDataMap.get("FDPTYPE")));
				getDataMap.put("INTMTH", this.intmthConvert(getDataMap.get("INTMTH")));
			}
			log.trace("deposit_cancel_result bs.getResult() : {} ", bs.getResult());
		}
		catch (Exception e)
		{
			log.error("~~~deposit_cancel_result~~Error : ", e);
		}
		return bs;
	}

	/**
	 * N076取得臺幣零存整付按月繳存資料
	 * 
	 * @param cusidn
	 * @return
	 */
	public BaseResult installment_saving(String cusidn)
	{
		BaseResult bs = new BaseResult();
		try
		{
			Map<String, String> rqMap = new HashMap<>();
			rqMap.put("CUSIDN", cusidn);
			rqMap.put("ADOPID", "N076_1");
			bs = N076_REST(N076QUERYLLIST, rqMap);
			if (bs != null && bs.getResult())
			{
				// 後製
				Set<Map> fdpAcnSet = new LinkedHashSet<Map>();
				String TableJson = "";
				Map<String, Object> dataMap = (Map) bs.getData();
				List<Map<String, String>> dataListMap = (List<Map<String, String>>) dataMap.get("REC");
				// 後製如果有資料才後製
				if (dataListMap != null && dataListMap.size() > 0)
				{
					TableJson = CodeUtil.toJson(dataListMap);
					for (Map<String, String> map : dataListMap)
					{
						Map<String, Object> fdpMap = new HashMap<>();
						fdpMap.put("ACN", map.get("ACN"));
						fdpMap.put("TEXT", map.get("TEXT"));
						fdpAcnSet.add(fdpMap);
					}
				}
				else
				{
					CodeUtil.toJson(dataListMap);
				}
				bs.addData("fdpAcnSet", fdpAcnSet);
				bs.addData("TableJson", TableJson);
			}

			log.trace(" installment_saving bs.getData {}", bs.getData());
		}
		catch (Exception e)
		{
			log.error("installment_saving Error", e);
		}
		return bs;
	}

	/**
	 * N076取得臺幣零存整付按月繳存 確認頁資料
	 * 
	 * @param reqParam
	 * @return
	 */
	public BaseResult installment_saving_confirm(Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{
			// reqParam = {CMTRMAIL=, FDPNUM=0000001, FDPACN_TEXT= 050-臺灣企銀 01075909752, AMOUNT=11111111, CMMAILMEMO=,
			// FDPACN=01075909752, TransferType=PD, ACNO=01062790594, ACNNO_SHOW=, CMTRMEMO=}
			bs.setData(reqParam);// 存入輸入頁的資料
			bs.setResult(true);// 設定 Result >>> true
			log.trace(ESAPIUtil.vaildLog("installment_saving_confirm >> " + CodeUtil.toJson(bs.getData())));
		}
		catch (Exception e)
		{
			log.error("installment_saving_confirm Error", e);
		}
		return bs;
	}

	/**
	 * N076執行臺幣零存整付按月繳存
	 * 
	 * @param cusidn
	 * @param dpMyEmail
	 * @param reqParam
	 * @return
	 */
	public BaseResult installment_saving_result(String cusidn, String dpMyEmail, Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{
			reqParam.put("CUSIDN", cusidn);
			reqParam.put("CMTRMAIL", dpMyEmail);
			reqParam.put("ADOPID", "N076");
			reqParam.put("TXID", "N076_1");
			// 前置作業-END
			// call電文
			bs = N076_REST(WSN076ACTION, reqParam);
			if (bs != null && bs.getResult())
			{
				Map<String, String> getDataMap = (Map<String, String>) bs.getData();
				// 電文回應的 已存入總金數會有小數點,理論上要在 ms_tw AbstractTelcommConfig 裡處理
				String totamt = getDataMap.get("TOTAMT").replaceAll("\\p{P}", "");//// 完全清除標點
				// 覆蓋原本的Map
				getDataMap.put("TOTAMT", NumericUtil.formatNumberString(totamt, 0));
				getDataMap.put("AMOUNT", NumericUtil.formatNumberString(getDataMap.get("AMOUNT"), 0));
				getDataMap.put("TOTBAL", NumericUtil.formatNumberString(getDataMap.get("TOTBAL"), 2));
				getDataMap.put("AVLBAL", NumericUtil.formatNumberString(getDataMap.get("AVLBAL"), 2));
				getDataMap.put("CMTRMEMO", reqParam.get("CMTRMEMO"));
			}

			log.trace("installment_saving_result bs.getData() : {} ", bs.getData());
		}
		catch (Exception e)
		{
			log.error("getInstallmentSavingResult Error", e);
		}
		return bs;
	}

	public Map<String,Object> createDepositIdgateData(BaseResult bs,Map<String,Object>IdgateSessionData){
		/*
		 * 轉帳日期 transfer_date
		 * 轉出帳號 OUTACN
		 * 轉入帳號 DPAGACNO_TEXT
		 * 轉帳金額 AMOUNT
		 * 存款種類 FDPACCNAME
		 * 期別/到期日 TERM_TEXT
		 * 計息方式 INTMTHNAME
		 * 到期轉奇CODENAME
		 * 轉存方式 DPSVTYPENAME
		 */	
		Map<String,String> IdgatetransData=new HashMap<>();
		LinkedHashMap txndataview = new LinkedHashMap();
		Map<String,String> dataMap=(Map<String, String>) bs.getData();
		//轉帳日期
		if(dataMap.get("FGTXDATE").equals("1")) {
			txndataview.put("轉帳日期",dataMap.get("transfer_date"));
		}else if(dataMap.get("FGTXDATE").equals("2")){
			IdgatetransData.put("CMDATE", dataMap.get("CMDATE"));
			txndataview.put("轉帳日期",dataMap.get("CMDATE"));
		}
		//轉出帳號
		IdgatetransData.put("ACN", dataMap.get("OUTACN"));
		txndataview.put("轉出帳號",dataMap.get("OUTACN"));
		//轉入帳號
		if(dataMap.get("FGSVACNO").equals("1")) {
			IdgatetransData.put("DPAGACNO", dataMap.get("DPAGACNO"));			
		}else if(dataMap.get("FGSVACNO").equals("2")){
			IdgatetransData.put("DPACNO", dataMap.get("DPACNO"));
		}
		txndataview.put("轉入帳號",dataMap.get("DPAGACNO_TEXT"));
		//轉帳金額
		IdgatetransData.put("AMOUNT", dataMap.get("AMOUNT"));
		txndataview.put("轉帳金額",dataMap.get("AMOUNT"));
		//存款種累
		IdgatetransData.put("FDPACC", dataMap.get("FDPACC"));
		txndataview.put("存款種類",dataMap.get("FDPACCNAME"));
		//期別/到期日
		if(dataMap.get("FGTDPERIOD").equals("1")) {			
			IdgatetransData.put("TERM", dataMap.get("TERM"));
		}else if(dataMap.get("FGTDPERIOD").equals("2")){
			IdgatetransData.put("CMDATE1", dataMap.get("CMDATE1"));
		}
		txndataview.put("期別/到期日",dataMap.get("TERM_TEXT"));
		//計息方式
		IdgatetransData.put("INTMTH", dataMap.get("INTMTH"));
		txndataview.put("計息方式",dataMap.get("INTMTHNAME"));
		//到期轉期
		IdgatetransData.put("CODE", dataMap.get("CODE"));
		txndataview.put("到期轉期",dataMap.get("CODENAME"));
		//轉存方式
		IdgatetransData.put("FGSVTYPE", dataMap.get("FGSVTYPE"));
		txndataview.put("轉存方式",dataMap.get("DPSVTYPENAME"));
		
		Map<String,Object>tmptxndataview=new HashMap<>();
		tmptxndataview.put("txndataview", txndataview);
		IdgateSessionData.put("txnData", IdgatetransData);
		IdgateSessionData.put("txnDataView", CodeUtil.toJson(tmptxndataview));
		log.debug("IDGATE N074 TRANSdata >> "+CodeUtil.toJson(IdgateSessionData));
		return IdgateSessionData;
	}
	
	// 根據使用者選擇顯示續存方式
	// 續存方式，1本金轉期，利息轉入帳號；2本金及利息一併轉期
	// 續存方式，1本金轉期(續存)，利息轉入帳號 :有轉入帳號；
	// 2:本金及利息一併轉期 轉入帳號為 ""字串
	public String fgsvTypeConvert(String fgsvType)
	{
		switch (fgsvType)
		{
			case "1":
				fgsvType = this.getI18nValeByAcctTdposit("DPSVTYPE1NAME");
				break;
			case "2":
				fgsvType = this.getI18nValeByAcctTdposit("DPSVTYPE2NAME");// 選擇2本金及利息一併轉期 轉入帳號為""字串
				break;
			default:
				break;
		}
		return fgsvType;
	}

	// 將電文回應的數字轉換為顯示存款種類
	public String fdpaccConvert(String FDPACC)
	{
		// 存款種類 1:定期存款, 2:定期儲蓄存款存本取息,3:定期儲蓄存款整存整付
		switch (FDPACC)
		{
			case "1":
				FDPACC = this.getI18nValeByAcctTdposit("FDPACC1NAME");// "定期存款";
				break;
			case "2":
				FDPACC = this.getI18nValeByAcctTdposit("FDPACC2NAME");// "定期儲蓄存款存本取息";
				break;
			case "3":
				FDPACC = this.getI18nValeByAcctTdposit("FDPACC3NAME");// "定期儲蓄存款整存整付";
				break;
			default:
				break;
		}

		return FDPACC;

	}

	// 將計息方式的數字轉換為中文
	// 或中文轉換數字
	// 計息方式 ，0機動，1固定
	public String intmthConvert(String INTMTH)
	{
		// 計息方式 ，0機動，1固定
		switch (INTMTH)
		{
			case "0":
				INTMTH = this.getI18nValeByAcctTdposit("INTMTHZERONAME");
				break;
			case "1":
				INTMTH = this.getI18nValeByAcctTdposit("INTMTHONENAME");
				break;
			default:
				break;
		}
		return INTMTH;
	}

	/**
	 * 民國年日期格式的日期格式加斜線 yyyMMdd -->> yyy/MM/dd 1080504 -->>> 108/05/04
	 * 
	 * @param reqDate
	 * @return
	 */
	public String dateAddSlash(String reqDate)
	{
		return DateUtil.convertDate(2, reqDate, "yyyMMdd", "yyy/MM/dd");
	}

	/**
	 * 輸入 beanMapKey 得到 acctTdeposit 中的 beanMapValue
	 * 
	 * @param beanMapKey
	 * @return acctTdeposit is beanMapValue
	 */
	public String getI18nValeByAcctTdposit(String beanMapKey)
	{
		return i18n.getMsg(this.getBeanMapValue("acctTdeposit", beanMapKey));
	}

	/**
	 * 輸入beanId && beanMapKey Get beanMapValue
	 * 
	 * @param beanId
	 * @param beanMapKey
	 * @see spring-arg.xml
	 * @return get spring-arg.xml beanMapValue By beanId && beanMapKey, if any (or empty String otherwise)
	 */
	public String getBeanMapValue(String beanId, String beanMapKey)
	{
		// 取得Bean中的Map
		Map<String, String> getTypeBean = SpringBeanFactory.getBean(beanId);
		// 取得Map中的Value
		String getTypeBeanValue = getTypeBean.get(beanMapKey);
		return getTypeBeanValue;
	}

	/**
	 * Json Convert Map<String,String>
	 * 
	 * @param strJson EX : {"ACN":"00162288288","TYPE":"1","FDPNUM":"0000068","AMTFDP":"15000.00"}
	 * @return Map EX :: {ACN=00162288288, TYPE=1, FDPNUM=0000068, AMTFDP=15000.00}
	 */
	public Map<String, String> jsonConvertMap(String strJson)
	{
		Map<String, String> map = new HashMap<String, String>();
		try
		{
			map = CodeUtil.fromJson(strJson, Map.class);
		}
		catch (JsonSyntaxException jsonE)
		{
			log.error("StrJsonConvertMap Exception ", jsonE);
		}
		return map;
	}

	/**
	 * 檢查TxToken
	 * 
	 * @param pageTxToken 頁面TxToken
	 * @param SessionFinshToken
	 * @return false 表示有問題
	 */
	public BaseResult validateToken(String pageTxToken, String SessionFinshToken)
	{
		BaseResult bs = new BaseResult();
		try
		{
			if (pageTxToken == null)
			{
				log.debug(" reqTxToken==null");
				bs.setMessage("FE002", "reqTxToken空白");
				log.debug("validateToken getResult:{}", bs.getMsgCode());
				log.debug("validateToken getResult:{}", bs.getMessage());
			}
			else if (StrUtil.isNotEmpty(SessionFinshToken) && SessionFinshToken.equals(pageTxToken))
			{
				log.debug("validateToken2 getResult:{}", bs.getMsgCode());
				log.debug("validateToken2 getResult:{}", bs.getMessage());
				log.debug(" pagToken SessionFinshToken &&  一樣 重複交易");
				bs.setMessage("FE002", "重複交易");
			}
			else
			{
				bs.setResult(true);
			}
		}
		catch (Exception e)
		{
			log.error("getTransferToken Error", e);
		}
		return bs;
	}
}
