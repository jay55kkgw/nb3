package tw.com.fstop.nnb.spring.controller;

import java.util.Base64;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.service.Personalize_Service;
import tw.com.fstop.util.SessionUtil;

/*
 * 個人化
 */
@SessionAttributes({ SessionUtil.CUSIDN, SessionUtil.ADOPID })
@Controller
@RequestMapping(value="/PERSONALIZE")
public class Personalize_Controller{
	
	private Logger log = LoggerFactory.getLogger(this.getClass().getName());
	
	@Autowired
	private Personalize_Service personalize_service;

	
	@RequestMapping(value = "/add", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult addFavorite(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("addFavorite...");
		BaseResult bs = null;
		String cusidn = "";
		String adopid = "";
		try {
			bs = new BaseResult();
			cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				log.trace("addFavorite.cusidn: " + cusidn);
			}
			adopid = (String) SessionUtil.getAttribute(model, SessionUtil.ADOPID, null);
			bs = personalize_service.addMyOp(cusidn, adopid);
				
		} catch (Exception e) {
			log.error("",e);
		}
		return bs;
	}
	
	@RequestMapping(value = "/delete", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult deleteFavorite(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("deleteFavorite...");
		BaseResult bs = null;
		String cusidn = "";
		String adopid = "";
		try {
			cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				log.trace("deleteFavorite.cusidn: " + cusidn);
			}
			adopid = (String) SessionUtil.getAttribute(model, SessionUtil.ADOPID, null);
			personalize_service.deleteMyOp(cusidn, adopid);
			
		} catch (Exception e) {
			log.error("",e);
		}
		return bs;
	}
	
	@RequestMapping(value = "/deleteById", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult deleteFavoriteById(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("deleteFavoriteById...");
		BaseResult bs = null;
		String cusidn = "";
		String adopid = "";
		try {
			cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				log.trace("deleteFavorite.cusidn: " + cusidn);
			}
			adopid = reqParam.get("ADOPID");
			personalize_service.deleteMyOp(cusidn, adopid);
			
		} catch (Exception e) {
			log.error("",e);
		}
		return bs;
	}
	
	
}