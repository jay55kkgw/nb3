package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * FundEmail，N930電文RQ
 */
public class FundEmail_REST_RQ extends BaseRestBean_FUND implements Serializable{
	private static final long serialVersionUID = 6868861910004819383L;
	private String CUSIDN;
	private String NEW_EMAIL;
	private String DPUSERID;
	
	public String getCUSIDN(){
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN){
		CUSIDN = cUSIDN;
	}
	public String getNEW_EMAIL(){
		return NEW_EMAIL;
	}
	public void setNEW_EMAIL(String nEW_EMAIL){
		NEW_EMAIL = nEW_EMAIL;
	}
	public String getDPUSERID(){
		return DPUSERID;
	}
	public void setDPUSERID(String dPUSERID){
		DPUSERID = dPUSERID;
	}
}