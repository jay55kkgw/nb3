package tw.com.fstop.nnb.service;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.nio.charset.StandardCharsets;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import fstop.orm.po.ADMADS;
import fstop.orm.po.ADMANN;
import fstop.orm.po.ADMLOGIN;
import fstop.orm.po.ADMLOGINACL;
import fstop.orm.po.ADMLOGINACLIP;
import fstop.orm.po.ADMLOGINOUT;
import fstop.orm.po.ADMNBSTATUS;
import fstop.orm.po.NB3SYSOP;
import fstop.orm.po.OLD_TXNUSER;
import fstop.orm.po.PASSIP;
import fstop.orm.po.TXNUSER;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.rest.bean.BaseResultSerialization;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.AdmAdsDao;
import tw.com.fstop.tbb.nnb.dao.AdmAnnDao;
import tw.com.fstop.tbb.nnb.dao.AdmLoginAclDao;
import tw.com.fstop.tbb.nnb.dao.AdmLoginAclIpDao;
import tw.com.fstop.tbb.nnb.dao.AdmLoginDao;
import tw.com.fstop.tbb.nnb.dao.AdmLoginOutDao;
import tw.com.fstop.tbb.nnb.dao.AdmMsgCodeDao;
import tw.com.fstop.tbb.nnb.dao.AdmNbStatusDao;
import tw.com.fstop.tbb.nnb.dao.Nb3SysOpDao;
import tw.com.fstop.tbb.nnb.dao.PassIpDao;
import tw.com.fstop.tbb.nnb.dao.TxnUserDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.StrUtils;

@Service
public class Login_out_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private ServletContext context;
	@Autowired
	private TxnUserDao txnUserDao;
	@Autowired
	private AdmMsgCodeDao admmsgcodedao;
	@Autowired
	private AdmAdsDao admadsdao;
	@Autowired
	private AdmLoginDao admlogindao;
	@Autowired
	private AdmLoginOutDao admloginoutdao;
	@Autowired
	private AdmLoginAclDao admloginacldao;
	@Autowired
	private PassIpDao passipdao;
	@Autowired
	private Nb3SysOpDao nb3SysOpDao;
	@Autowired
	I18n i18n;
	@Autowired
	private AdmAnnDao admannDao;
	@Autowired
	private AdmNbStatusDao admNbStatusDao;
	@Autowired
	private AdmLoginAclIpDao admLoginAclIpDao;
	@Autowired
	private DaoService daoservice;
	@Autowired
	private Send_Mail_Service send_mail_service;
	

	/**
	 * 將資料庫廣告落地
	 * 
	 * 1. 是否更新: 比對時間戳，判斷最後異動時間，決定廣告是否重新落地 ( 對全部資料，不限定是在生效範圍內的資料 )
	 * 2. 重新落地: 先清除資料夾內資料，將生效範圍內的資料都落地，再更新時間戳
	 */
	public boolean downAds() {
		boolean result = false;
		try {
			// 廣告更新結果
			boolean resultB = false, resultC = false;
			
			// 步驟一: 是否更新--banner
//			if ( updateAdsNeed("B") ) {
				// 步驟二: 重新落地--banner
				List<ADMADS> admadsListB = admadsdao.getByType("B"); // Banner
				resultB = downAdsImage(admadsListB, "banner");
				log.debug("downAds.resultB: " + resultB);
				
//			} else {
//				log.debug("downAds.resultB.skip!!!");
//				resultB = true;
//			}
			
			// 步驟一: 是否更新--news
//			if ( updateAdsNeed("C") ) {
				// 步驟二: 重新落地--news
				List<ADMADS> admadsListC = admadsdao.getByType("C"); // News
				resultC = downAdsImage(admadsListC, "news");
				log.debug("downAds.resultC: " + resultC);
				
//			} else {
//				log.debug("downAds.resultC.skip!!!");
//				resultC = true;
//			}
			
			log.debug("downAds.resultB: " + resultB);
			log.debug("downAds.resultC: " + resultC);
			
			result = resultB && resultC;
			log.debug("downAds.result: " + result);
			
		} catch (Exception e) {
			log.error("{}", e);
		}
		return result;
	}
	
	/**
	 * 廣告圖檔落地
	 */
	public boolean downAdsImage(List<ADMADS> admadsList, String type) {
		boolean result = false;
		log.debug(ESAPIUtil.vaildLog("downAdsImage.admadsList: " + admadsList));
		log.debug("downAdsImage.type: {}", type);
		try {
			String separ = File.separator;
			log.debug("countAds.separ: {}", separ);
			String path = downImgPath(type);
			log.debug("countAds.path: {}", path);
			
			// 創建目錄如果目錄不存在
			File file = new File(path);
			file.setWritable(true,true);
			file.setReadable(true,true);
			file.setExecutable(true,true);
			file.mkdirs();
			
			// 先清空目錄
			FileUtils.cleanDirectory(file);
			
			// 圖檔落地 ( 圖檔需求異動改為只有大版 )
			for(int i=1; i<=admadsList.size(); i++) {
				ADMADS admads = admadsList.get(i-1);
				log.trace(ESAPIUtil.vaildLog("downAds.admads: "+ admads));
				
				// 資料庫圖檔Blob轉byteArray-L
				byte[] adsL = admads.getFILECONTENTL();
				String title = admads.getTITLE();
				log.trace("downAds.adsL: {}", adsL!=null);
				// 儲存成圖檔-L
				if(adsL != null) {
					BufferedImage bufferedImageL = ImageIO.read( new BufferedInputStream( new ByteArrayInputStream(adsL)));
					CodeUtil.bufferedimageConvertImage(bufferedImageL, "png", path + separ + type + "-lg-" + i + ".png");
					try {
						//廣告標題存成txt
						String skinPath = path + separ + type + "-lg-" + i + ".txt";
						byte[] bytes = title.getBytes("UTF-8");
						
						// 產生目標檔案
//						FileOutputStream txtFile = new FileOutputStream(skinPath);
//						txtFile.write(bytes);
//						txtFile.close();
						// 修正Incorrect Permission Assignment For Critical Resources
						BufferedOutputStream fw = new BufferedOutputStream(new FileOutputStream(skinPath));
				        fw.write(bytes);
				        fw.flush();
				        fw.close();
					}
					catch (Exception e) {
						log.error("" + e);
					}
				}
				
				log.trace("downAds.OK...");
			}
			
			// banner記錄時間戳
			if("banner".equals(type)) {
				// 更新廣告目錄 ( 更新s1、s2較舊的目錄 )
				String target = path.substring(path.lastIndexOf(separ)+1);
				log.debug("downAds.target: {}", target);
				String pathS = path.substring(0, path.lastIndexOf(separ));
				log.debug("downAds.pathS: {}", pathS);
				
				// 先清空目錄
				File fileS = new File(pathS + separ + "lt");
				fileS.setWritable(true, true);
				fileS.setReadable(true, true);
				fileS.setExecutable(true, true);
				fileS.mkdirs(); // 創建目錄如果目錄不存在
				FileUtils.cleanDirectory(fileS);
				
				// 取得資料庫廣告資料最後編輯時間，紀錄時間戳
				String rsTime = getLastEditTime("B");
				File ts = new File(pathS + separ + "lt" + separ + rsTime);
				ts.setWritable(true, true);
				ts.setReadable(true, true);
				ts.setExecutable(true, true);
				ts.createNewFile();
			}
			
			// news需要記錄時間戳及落地圖檔對應的資料
			if("news".equals(type)) {
				// 更新廣告目錄 ( 更新s1、s2較舊的目錄 )
				String target = path.substring(path.lastIndexOf(separ)+1);
				log.debug("downAds.target: {}", target);
				String pathS = path.substring(0, path.lastIndexOf(separ));
				log.debug("downAds.pathS: {}", pathS);
				
				// 記錄時間戳
				if("s1".equals(target)) {
					// 先清空目錄
					File fileS = new File(pathS + separ + "ts1");
					fileS.setWritable(true, true);
					fileS.setReadable(true, true);
					fileS.setExecutable(true, true);
					fileS.mkdirs(); // 創建目錄如果目錄不存在
					FileUtils.cleanDirectory(fileS);
					
					// 紀錄時間戳
					File ts = new File(pathS + separ + "ts1" + separ + DateUtil.getCurentDateTime("yyyyMMdd HHmmss"));
					ts.setWritable(true, true);
					ts.setReadable(true, true);
					ts.setExecutable(true, true);
					ts.createNewFile();
					
				} else if("s2".equals(target)) {
					// 先清空目錄
					File fileS = new File(pathS + separ + "ts2");
					fileS.setWritable(true, true);
					fileS.setReadable(true, true);
					fileS.setExecutable(true, true);
					fileS.mkdirs(); // 創建目錄如果目錄不存在
					FileUtils.cleanDirectory(fileS);
					
					// 紀錄時間戳
					File ts = new File(pathS + separ + "ts2" + separ + DateUtil.getCurentDateTime("yyyyMMdd HHmmss"));
					ts.setWritable(true, true);
					ts.setReadable(true, true);
					ts.setExecutable(true, true);
					ts.createNewFile();
				}
				
				// 先清空目錄
				File fileLT = new File(pathS + separ + "lt");
				fileLT.setWritable(true, true);
				fileLT.setReadable(true, true);
				fileLT.setExecutable(true, true);
				fileLT.mkdirs(); // 創建目錄如果目錄不存在
				FileUtils.cleanDirectory(fileLT);
				
				// 取得資料庫廣告資料最後編輯時間
				String rsTime = getLastEditTime("C");
				File lt = new File(pathS + separ + "lt" + separ + rsTime);
				lt.setWritable(true, true);
				lt.setReadable(true, true);
				lt.setExecutable(true, true);
				lt.createNewFile();
				
				
				// News廣告資料落地
				List<Map<String, String>> newsTitleList = loadNewsTitle();
				// 記錄時間戳
				if("s1".equals(target)) {
					// 先清空目錄
					File fileS = new File(pathS + separ + "d1");
					fileS.setWritable(true, true);
					fileS.setReadable(true, true);
					fileS.setExecutable(true, true);
					fileS.mkdirs(); // 創建目錄如果目錄不存在
					FileUtils.cleanDirectory(fileS);
					
					// 資料落地
					String dataTarget = pathS + separ + "d1" + separ + "newsData.txt";
					File ts = new File(dataTarget);
					ts.setWritable(true, true);
					ts.setReadable(true, true);
					ts.setExecutable(true, true);
					ts.createNewFile();
					
					BufferedOutputStream fw = new BufferedOutputStream(new FileOutputStream(ts));
//			        FileWriter fw = new FileWriter(dataTarget); // 會用系統預設編碼
					byte[] strToBytes = CodeUtil.toJson(newsTitleList).getBytes("UTF-8");
			        fw.write(strToBytes);
			        fw.flush();
			        fw.close();
					
				} else if("s2".equals(target)) {
					// 先清空目錄
					File fileS = new File(pathS + separ + "d2");
					fileS.setWritable(true, true);
					fileS.setReadable(true, true);
					fileS.setExecutable(true, true);
					fileS.mkdirs(); // 創建目錄如果目錄不存在
					FileUtils.cleanDirectory(fileS);
					
					// 資料落地
					String dataTarget = pathS + separ + "d2" + separ + "newsData.txt";
					File ts = new File(dataTarget);
					ts.setWritable(true, true);
					ts.setReadable(true, true);
					ts.setExecutable(true, true);
					ts.createNewFile();
					
					//Writer fw = new OutputStreamWriter(new FileOutputStream(dataTarget), StandardCharsets.UTF_8);
//			        FileWriter fw = new FileWriter(dataTarget); // 會用系統預設編碼
					BufferedOutputStream fw = new BufferedOutputStream(new FileOutputStream(ts));
					byte[] strToBytes = CodeUtil.toJson(newsTitleList).getBytes("UTF-8");
			        fw.write(strToBytes);
			        fw.flush();
			        fw.close();
				}
				
			}
			
			result = true;
			log.debug("downAds.result: " + result);
			
		} catch (Exception e) {
			log.error("{}", e);
		}
		return result;
	}
	
	/**
	 * 是否需要更新廣告
	 */
	public boolean updateAdsNeed(String type) {
		log.debug("updateAdsNeed.type: " + type);
		boolean result = false;
		
		try {
			String separ = File.separator;
			String lt = ""; // 時間戳
			String path = ""; // 廣告類別路徑
			
			// banner
			if ("B".equals(type)) {
				path = context.getRealPath("") + separ + "com" + separ + "login" + separ + "banner";
			}
			
			// news
			if ("C".equals(type)) {
				path = context.getRealPath("") + separ + "com" + separ + "login" + separ + "news";
			}
			
			// 時間戳路徑
			File file = new File(path + separ + "lt");
			file.setWritable(true, true);
			file.setReadable(true, true);
			file.setExecutable(true, true);
			file.mkdirs(); // 創建目錄如果目錄不存在
			
			// 比對時間戳
			if (file.isDirectory()) {
				String[] filenames = file.list();
				lt = Arrays.asList(filenames).isEmpty() ? "0000000 000000" : filenames[0];
			}
			
			// 取得資料庫廣告資料最後編輯時間
			String rsTime = getLastEditTime(type);
			
			// 時間戳比對資料庫最後異動時間
			result = !lt.equals(rsTime);
			
		} catch (Exception e) {
			log.error("{}", e);
		}
		
		log.debug("updateAdsNeed.result: " + result);
		return result;
	}
	
	/**
	 * 取得資料庫廣告資料最後編輯時間
	 */
	public String getLastEditTime(String type) {
		log.debug("getLastEditTime...");
		String rsTime = "0000000 000000";
		
		try {
			List<String> admadsListTmp = admadsdao.getLastDateTime(type);
			log.trace(ESAPIUtil.vaildLog("Login_out_Service.getLastDateTime:" + admadsListTmp));
			if (admadsListTmp != null) {
				for (Object o : admadsListTmp) {
					Object[] or = (Object[]) o;

					String lastdate = StrUtil.isNotEmpty(or[0].toString()) ? or[0].toString() : "0000000";
					String lasttime = StrUtil.isNotEmpty(or[1].toString()) ? or[1].toString() : "000000";
					rsTime = lastdate + " " + lasttime;
				}
				log.trace(ESAPIUtil.vaildLog("rsTime: " + rsTime));
			}
			
		} catch (Exception e) {
			log.error("{}", e);
		}
		
//		log.debug("getLastEditTime.rsTime: " + rsTime);
		return rsTime;
	}
	
	/**
	 * 廣告圖檔落地目錄
	 */
	public String downImgPath(String type) {
		log.debug("downImgPath...");
		String path = "";
		try {
			String separ = File.separator;
			
			if("banner".equals(type)) {
				path = context.getRealPath("") + separ + "com" + separ + "login" + separ + "banner" + separ + "bs";
				
			} else if("news".equals(type)) {
				path = context.getRealPath("") + separ + "com" + separ + "login" + separ + "news" + separ + compareNewsTS(false);
				
			}
			log.debug("downImgPath.path: {}", path);
			
		} catch (Exception e) {
			log.error("{}", e);
		}
		
		log.debug("downImgPath.path: " + path);
		return path;
	}
	
	/**
	 * news比對時間戳
	 * @param desc 是否反向
	 */
	public String compareNewsTS(boolean desc) {
		log.debug("compareNewsTS...");
		String imgSrc = "s1";
		try {
			String separ = File.separator;
			
			// news需要比對時間戳記取得較新的目錄
			String path = context.getRealPath("") + separ + "com" + separ + "login" + separ + "news";
			File file1 = new File(path + separ + "ts1");
			file1.setWritable(true, true);
			file1.setReadable(true, true);
			file1.setExecutable(true, true);
			file1.mkdirs(); // 創建目錄如果目錄不存在

			File file2 = new File(path + separ + "ts2");
			file2.setWritable(true, true);
			file2.setReadable(true, true);
			file2.setExecutable(true, true);
			file2.mkdirs(); // 創建目錄如果目錄不存在
	
			// 比對時間戳
			String ts1 = "";
			String ts2 = "";
			if (file1.isDirectory()) {
				String[] filenames1 = file1.list();
				ts1 = Arrays.asList(filenames1).isEmpty() ? "0000000 000000" : filenames1[0];
				String[] filenames2 = file2.list();
				ts2 = Arrays.asList(filenames2).isEmpty() ? "0000000 000001" : filenames2[0];
				
				// 取較舊的目錄，desc是否反向
				if(DateUtil.time2compare(ts1, ts2, "yyyyMMdd HHmmss")) {
					imgSrc = desc ? "s2" : "s1";
					
				} else {
					imgSrc = desc ? "s1" : "s2";
				}
				
			} else {
				log.trace("[" + file1 + "]不是目錄");
			}
		} catch (Exception e) {
			log.error("{}", e);
		}
		
		log.debug("compareNewsTS.imgSrc: " + imgSrc);
		return imgSrc;
	}
	
	/**
	 * 載入News廣告標題
	 */
	public List<Map<String, String>> loadNewsTitle() {
		List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();
		try {
			byte[] imageContent = null;
			String strContent = "";
			// 取得資料庫廣告資料
			List<String> admadsListTmp = admadsdao.getTitleByType("C"); // Banner
			log.debug(ESAPIUtil.vaildLog("Login_out_Service.loadNewsTitle:" + admadsListTmp));
			if (admadsListTmp != null) {
				for (Object o : admadsListTmp) {
					Object[] or = (Object[]) o;
					Map<String, String> rowdata = new HashMap<String, String>();
					rowdata.put("TITLE", StrUtil.isNotEmpty(or[0].toString()) ? or[0].toString() : " ");
					rowdata.put("URL", StrUtil.isNotEmpty(or[1].toString()) ? or[1].toString() : " ");
					rowdata.put("STARTDATE", StrUtil.isNotEmpty(or[2].toString()) ? or[2].toString() : " ");
					rowdata.put("ENDDATE", StrUtil.isNotEmpty(or[3].toString()) ? or[3].toString() : " ");
					rowdata.put("TARGETTYPE", StrUtil.isNotEmpty(or[4].toString()) ? or[4].toString() : " ");
					rowdata.put("newsId", StrUtil.isNotEmpty(or[5].toString()) ? or[5].toString() : " ");
					rowdata.put("CONTENT", StrUtil.isNotEmpty(or[6].toString()) ? or[6].toString() : " ");
					
					log.debug("TYPE>>>"+ESAPIUtil.vaildLog(or[4].toString()));
					if(or[4].toString().equals("3")) {
						ADMADS po = admadsdao.getById(or[5].toString());
						imageContent = po.getTARGETCONTENT();
						strContent = new String(imageContent,"UTF-8");
						strContent = strContent.replaceAll("\n\r","<br/>");
						strContent = strContent.replaceAll("\r\n","<br/>");
						strContent = strContent.replaceAll("\n","<br/>");
						strContent = strContent.replaceAll("\r","<br/>");
						rowdata.put("TARGETCONTENT", strContent);
					}
					resultList.add(rowdata);
				}
				log.debug(ESAPIUtil.vaildLog("resultList>>>"+resultList.toString()));
			}
			
		} catch (Exception e) {
			log.error("{}", e);
		}
		return resultList;
	}
	
	
	/**
	 * 載入News廣告資料
	 */
	public List<Map<String, String>> loadNewsData(String imgSrc) {
		List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();
		try {
			String separ = File.separator;
			String target = "s1".equals(imgSrc) ? "d1" : "d2";
			target = context.getRealPath("") + separ + "com" + separ + "login" + separ + "news" + separ + target + separ + "newsData.txt";
			log.debug("loadNewsData: {}", target);
			
			// 取得落地廣告資料
			StringBuffer sb = new StringBuffer();
			
			InputStreamReader fr = new InputStreamReader(new FileInputStream(target), StandardCharsets.UTF_8);
//	        FileReader fr = new FileReader(target); // 會用系統預設編碼
	        BufferedReader br = new BufferedReader(fr);
	        while (br.ready()) {
	        	sb.append(br.readLine());
	        }
	        fr.close();
	        
//	        log.debug(ESAPIUtil.vaildLog("loadNewsData.sb: {}"+ sb.toString()));
	        resultList = CodeUtil.fromJson(sb.toString(), resultList.getClass());
//	        log.debug(ESAPIUtil.vaildLog("loadNewsData.resultList: {}"+ resultList));
			
		} catch (Exception e) {
			log.error("{}", e);
		}
		return resultList;
	}
	
	
	/**
	 * 計算登入頁廣告數量
	 */
	public Integer countAds(String folder, int size) {
		log.debug("countAds.folder: {}", folder);
		log.debug("countAds.size: {}", size);
		
		int imgs = 0;
		String[] filenames;
		try {
			String separ = File.separator;
			log.debug("countAds.separ: {}", separ);
			String path = context.getRealPath("") + separ + "com" + separ + "login" + separ + folder + separ + "bs";
			log.debug("countAds.path: {}", path);
			
			// 創建目錄如果目錄不存在
			File file = new File(path);
			file.setWritable(true,true);
			file.setReadable(true,true);
			file.setExecutable(true,true);
			file.mkdirs();
			
			// 計算目錄下檔案數量
			if (file.isDirectory()) {
				filenames = file.list();
				int adcount = 0;
				for(String name : filenames) {
					if(name.indexOf(".txt")!=-1) {
						adcount++;
					}
				}
				imgs = (filenames.length - adcount)/size; // 廣告有大中小版，計算廣告數量要除以版型
				log.debug("loadAds.imgs: " + imgs);
			} else {
				log.trace("[" + file + "]不是目錄");
			}
			
		} catch (Exception e) {
			log.error("{}", e);
		}
		return imgs;
	}
	
	/**
	 * 網路通行碼檢核
	 * @param cusidn 身分證字號
	 * @param userid 使用者帳號
	 * @param pinnew 簽入密碼
	 * @return
	 */
	public BaseResult login(String cusidn, String userid, String pinnew) {
		log.trace("login_out_service.login...");
		log.trace(ESAPIUtil.vaildLog("login_cusidn: " + cusidn));
		log.trace(ESAPIUtil.vaildLog("login_userid: " + userid));
		
		// 處理結果
		BaseResult bs = null ;
		try {
			// 網路通行碼檢核
			bs = N911_REST(cusidn, userid, pinnew);
			log.debug("login.N911_REST: " + BeanUtils.describe(bs) );
			
		} catch (Exception e) {
			log.error("{}", e);
		}
		return bs;
	}
	
	/**
	 *成功登入需發N912取得登入需要的資訊
	 * @param cusidn 身分證字號
	 * @param userid 使用者帳號
	 * @param pinnew 簽入密碼
	 * @return
	 */
	public void loginN912(String cusidn, BaseResult bs) {
		log.trace("login_out_service.login...");
		log.trace(ESAPIUtil.vaildLog("login_cusidn: " + cusidn));
		
		// 處理結果
		BaseResult bsN912 = null ;
		try {
			// 成功登入需發N912取得登入需要的資訊
			bsN912 = N912_REST(cusidn);
			log.debug("login.N912_REST: " + BeanUtils.describe(bsN912) );
			
			// N912業務權限(TRNATR)非NB不能進入NB
			Map<String,Object> n912Map = null;
			n912Map = (Map<String, Object>) bsN912.getData();
			String trnatr = String.valueOf(n912Map.get("TRNATR"));

			if(!"0".equals(bsN912.getMsgCode())) {
				log.error("bsN912.error: {}", BeanUtils.describe(bsN912));
				bs.setMsgCode(bsN912.getMsgCode());
				bs.setMessage(bsN912.getMessage());
				bs.setResult(false);
				bs.setNext("error"); // 用來區別電文或程式錯誤訊息
				return;
				
			} else if ( !"NB".equals(trnatr) ) {
				log.error("bsN912.check.fail: {}", BeanUtils.describe(bsN912));
				bs.setMsgCode("99338");
				bs.setMessage("請確認所登入帳號的系統是否正確。");
				bs.setResult(false);
				bs.setNext("error"); // 用來區別電文或程式錯誤訊息
				return;
			}
			
			
			// 將N911下行結合N912下行
			loginInit(cusidn ,bs, bsN912);
			log.debug("login.bs.final: " + BeanUtils.describe(bs) );
			
		} catch (Exception e) {
			log.error("{}", e);
			bs.setMsgCode("Z035");
			bs.setMessage(i18n.getMsg("LB.X1886"));
			bs.setResult(false);
			bs.setNext("error"); // 用來區別電文或程式錯誤訊息
		}
	}
	
	/**
	 * 登入時所需資料
	 * @param cusidn 使用者統編
	 * @param bs N911電文結果
	 * @return
	 */
	public void loginInit(String cusidn, BaseResult bs, BaseResult bsN912) {
		log.trace("login_out_service.loginInit...");
		log.trace(ESAPIUtil.vaildLog("login_out_service.cusidn: {}"+ cusidn));
		
		String dpMyEmail = ""; // 登入時要取得N911下行mail更新TXNUSER
//		String dpUserName = ""; // 移到首頁發Ajax更新並存進Session
		
		try {
			Map<String,Object> resultMap = null;
			resultMap = (Map<String, Object>) bs.getData();
			
			Map<String,Object> n912Map = null;
			n912Map = (Map<String, Object>) bsN912.getData();
			
			
			// 電文代碼錯誤訊息
			String errorCode = String.valueOf(resultMap.get("TOPMSG"));
			log.trace("loginInit.errorCode: {}", errorCode);
			String errorMsg = StrUtil.isNotEmpty(errorCode) ? admmsgcodedao.errorMsg(errorCode) : "";
			
			
			// TXNUSER-改在首頁發Ajax打N920更新資料庫TXNUSER
//			BaseResult bsN920 = N920_REST(cusidn, Base_Service.LOGIN);
//			if(!"0".equals(bsN920.getMsgCode())) {
//				log.error("N920.error: {}", BeanUtils.describe(bsN920));
//				bs.setMsgCode(bsN920.getMsgCode());
//				bs.setMessage(bsN920.getMessage());
//				bs.setResult(false);
//				bs.setNext("error"); // 用來區別電文或程式錯誤訊息
//				return;
//			}
//			String dpname = ((Map<String, Object>) bsN920.getData()).get("NAME").toString();
//			if(StrUtil.isEmpty(dpname)) {
//				dpname = ((Map<String, Object>) bsN920.getData()).get("CUSNAME").toString();
//			}
			// TXNUSER
//			TXNUSER txnUser = txnUserDao.chkRecord(cusidn, dpname);
			dpMyEmail = StrUtil.isNotEmpty(resultMap.get("MAILADDR").toString()) ? resultMap.get("MAILADDR").toString() : "";
			TXNUSER txnUser = daoservice.loginChk(cusidn, dpMyEmail);
			if(txnUser!=null){
				dpMyEmail = txnUser.getDPMYEMAIL();
//				dpUserName = txnUser.getDPUSERNAME(); // 移到首頁發Ajax更新並存進Session
			}
			
			// N912業務權限及約定功能
			String authority = "ALL"; // N912.APATR若為空字串則為ALL(一般網銀臨櫃申請使用者)
			String apatrs = String.valueOf(n912Map.get("APATR"));
			authority = loginInitAuthority(apatrs);
			log.debug("loginInit.authority: {}", authority);
			
			
			// 登入需要紀錄的資料
			resultMap.put("ERRORMSG", errorMsg);
			resultMap.put("DPMYEMAIL", dpMyEmail);
//			resultMap.put("DPUSERNAME", dpUserName); // 移到首頁發Ajax更新並存進Session
			resultMap.put("AUTHORITY", authority);
			
			
			// 完成登入所需資料處理重新包裝
//			log.trace("loginInit.resultMap: {}", resultMap);
			bs.setData(resultMap);
			
		} catch (Exception e) {
			log.error("{}", e);
			bs.setMsgCode("Z035");
			bs.setMessage(i18n.getMsg("LB.X1886"));
			bs.setResult(false);
			bs.setNext("error"); // 用來區別電文或程式錯誤訊息
		}
	}
	
	
	/**
	 *成功登入需發N912取得登入需要的資訊
	 * @param cusidn 身分證字號
	 * @param userid 使用者帳號
	 * @param pinnew 簽入密碼
	 * @return
	 */
	public void expiredpwN912(String cusidn, BaseResult bs) {
		log.trace("login_out_service.login...");
		log.trace(ESAPIUtil.vaildLog("login_cusidn: " + cusidn));
		
		// 處理結果
		BaseResult bsN912 = null ;
		try {
			// 成功登入需發N912取得登入需要的資訊
			bsN912 = N912_REST(cusidn);
			log.debug("login.N912_REST: " + BeanUtils.describe(bsN912) );

			// N912業務權限(TRNATR)非NB不能進入NB
			Map<String,Object> n912Map = null;
			n912Map = (Map<String, Object>) bsN912.getData();
			String trnatr = String.valueOf(n912Map.get("TRNATR"));
			
			if(!"0".equals(bsN912.getMsgCode())) {
				log.error("bsN912.error: {}", BeanUtils.describe(bsN912));
				bs.setMsgCode(bsN912.getMsgCode());
				bs.setMessage(bsN912.getMessage());
				bs.setResult(false);
				bs.setNext("error"); // 用來區別電文或程式錯誤訊息
				return;
				
			} else if ( !"NB".equals(trnatr) ) {
				log.error("bsN912.check.fail: {}", BeanUtils.describe(bsN912));
				bs.setMsgCode("99338");
				bs.setMessage("請確認所登入帳號的系統是否正確。");
				bs.setResult(false);
				bs.setNext("error"); // 用來區別電文或程式錯誤訊息
				return;
			}
			
			// 將N911下行結合N912下行
			expiredpwInit(cusidn ,bs, bsN912);
			log.debug("login.bs.final: " + BeanUtils.describe(bs) );
			
		} catch (Exception e) {
			log.error("{}", e);
			bs.setMsgCode("Z035");
			bs.setMessage(i18n.getMsg("LB.X1886"));
			bs.setResult(false);
			bs.setNext("error"); // 用來區別電文或程式錯誤訊息
		}
	}
	
	/**
	 * 登入時所需資料
	 * @param cusidn 使用者統編
	 * @param bs N911電文結果
	 * @return
	 */
	public void expiredpwInit(String cusidn, BaseResult bs, BaseResult bsN912) {
		log.trace("login_out_service.loginInit...");
		log.trace(ESAPIUtil.vaildLog("login_out_service.cusidn: {}"+ cusidn));
		
		String dpMyEmail = ""; // 登入時要取得N911下行mail更新TXNUSER
		
		try {
			Map<String,Object> resultMap = new HashMap<String, Object>();
			
			Map<String,Object> n912Map = null;
			n912Map = (Map<String, Object>) bsN912.getData();
			
			// TXNUSER
			List<TXNUSER> txnUserList = txnUserDao.findByUserId(cusidn);
			if(txnUserList!=null && !txnUserList.isEmpty()){
				dpMyEmail = txnUserList.get(0).getDPMYEMAIL();
			}
			
			// N912業務權限及約定功能
			String authority = "ALL"; // N912.APATR若為空字串則為ALL(一般網銀臨櫃申請使用者)
			String apatrs = String.valueOf(n912Map.get("APATR"));
			authority = loginInitAuthority(apatrs);
			log.debug("loginInit.authority: {}", authority);
			
			// 登入需要紀錄的資料
			resultMap.put("DPMYEMAIL", dpMyEmail);
			resultMap.put("AUTHORITY", authority);
			
			// 完成登入所需資料處理重新包裝
			log.trace(ESAPIUtil.vaildLog("loginInit.resultMap: {}"+ resultMap));
			bs.setMsgCode("0");
			bs.setMessage("success");
			bs.setResult(true);
			bs.setData(resultMap);
			
		} catch (Exception e) {
			log.error("{}", e);
			bs.setMsgCode("Z035");
			bs.setMessage(i18n.getMsg("LB.X1886"));
			bs.setResult(false);
			bs.setNext("error"); // 用來區別電文或程式錯誤訊息
		}
	}
	
	
	/**
	 * 取得使用者在網銀擁有的最大業務權限
	 * @param apatrs 使用者業務權限
	 * @return
	 */
	public String loginInitAuthority(String apatrs) {
		log.trace("login_out_service.loginInitAuthority...");
		log.debug("loginInitAuthority.apatrs: {}", apatrs);
		
		String authority = "ALL"; // N912.APATR若為空字串則為ALL(一般網銀臨櫃申請使用者)
		
		try {
			// 取得使用者在網銀擁有的最大業務權限
			for(String apatr: apatrs.split(" ")) {
				// ATM(金融卡申請網銀使用者)
				if("ATM".equalsIgnoreCase(apatr.toUpperCase()) && !"ATMT".equals(authority)){
					authority = "ATM";
				}
				// CRD(信用卡申請網銀使用者)
				if("CRD".equalsIgnoreCase(apatr.toUpperCase()) && !"ATM".equals(authority)){
					authority = "CRD";
				}
				// ATMT
				if("ATMT".equalsIgnoreCase(apatr.toUpperCase())){
					authority = "ATMT";
				}
			}
			
		} catch (Exception e) {
			log.error("{}", e);
		}
		
		log.trace("loginInitAuthority.authority: {}", authority);
		return authority;
	}
	
	/**
	 * 網路通行碼檢核
	 * @param cusidn 身分證字號
	 * @param userid 使用者帳號
	 * @param pinnew 簽入密碼
	 * @return
	 */
	public boolean isIKeyUser(String xmlcod) {
		log.trace(ESAPIUtil.vaildLog("isIKeyUser.xmlcod: {}"+ xmlcod));
		
		// 處理結果
		boolean isIKeyUser = false ;
		
		try {
			// 是IKEY使用者
			if( "2".equals(xmlcod) || "4".equals(xmlcod) || "5".equals(xmlcod) || "6".equals(xmlcod)) {
				isIKeyUser = true;
			}
		} catch (Exception e) {
			log.error("{}", e);
		}
		return isIKeyUser;
	}
	
	/**
	 * 沿用舊密碼登入時--取得使用者 登入/登出 狀態檔 資訊
	 */
	public ADMLOGIN getAdmLogin(String cusidn) {
		ADMLOGIN po = null;
		try {
			log.trace("Login_out_Service.setAdmLogin...");
			
			// 若資料庫有資料則取資料庫的資料
			List<ADMLOGIN> admloginList = admlogindao.findByUserId(cusidn);
			if(admloginList != null && !admloginList.isEmpty()) {
				po = admlogindao.findByUserId(cusidn).get(0);
			} else {
				po = new ADMLOGIN();
			}
			
		} catch (Exception e) {
			log.error("{}", e);
		}
		return po;
	}

	/**
	 * 登入時--紀錄使用者 登入/登出 狀態檔 資訊
	 * @param reqParam 查詢參數
	 * @param tokenId 使用者識別
	 * @param logintime 本次登入日期時間
	 * @param userip 登入 IP-Address
	 * @param bs BaseResult
	 * @return
	 */
	public boolean setAdmLogin(String cusidn, String tokenId, Date logintime, String userip, Map<String, Object> dataMap) {
		boolean result = false;
		try {
			log.trace("Login_out_Service.setAdmLogin...");
			
			// 若資料庫有資料則取資料庫的資料
			ADMLOGIN po = null;
			List<ADMLOGIN> admloginList = admlogindao.findByUserId(cusidn);
			if(admloginList != null && !admloginList.isEmpty()) {
				po = admlogindao.findByUserId(cusidn).get(0);
			} else {
				po = new ADMLOGIN();
			}
			
			po.setADUSERID(cusidn);			// 使用者統編ID
			po.setTOKENID(tokenId);			// 使用者識別
			po.setADUSERTYPE((StrUtils.trim(cusidn).length() == 10 ? "0" : "1")); // 操作者類別	0:個人戶 (預設) 1:企業戶 (獨資統編)
			
			// 登出時使用的欄位
//			po.setLOGOUTTIME("");			// 本次正常登出日期時間
//			po.setFORCELOGOUTTIME("");		// 本次強迫登出日期時間
			
			po.setLASTLOGINTIME(po.getLOGINTIME());	// 上次登入日期時間
			po.setLOGINTIME(new DateTime(logintime).toString("yyyyMMddHHmmss")); // 本次登入日期時間
			
			po.setLASTADUSERIP(po.getADUSERIP());	// 上次登入IP
			po.setADUSERIP(userip);					// 登入 IP-Address
			
			po.setLOGINOUT("0");					// 登入狀態	0:登入 1:登出
			po.setLOGINTYPE("NB");					// 登入來源	NB (預設) MB=行動裝置登入
			
			// 使用者姓名
			String dpusername = dataMap.get("DPUSERNAME")!=null ? dataMap.get("DPUSERNAME").toString().replaceAll("　","") : "";
			po.setDPUSERNAME(dpusername); // 使用者姓名
			
			// 如果dataMap無資料就保持原本資料
			po.setMMACOD(dataMap.get("MMACOD")!=null ? dataMap.get("MMACOD").toString() : po.getMMACOD()); // MMA輕鬆理財註記
			po.setXMLCOD(dataMap.get("XMLCOD")!=null ? dataMap.get("XMLCOD").toString() : po.getXMLCOD()); // 憑證代碼
			po.setBRTHDY(dataMap.get("BRTHDY")!=null ? dataMap.get("BRTHDY").toString() : po.getBRTHDY()); // 出生年月日
			po.setTELNUM(dataMap.get("TELNUM")!=null ? dataMap.get("TELNUM").toString() : po.getTELNUM()); // 電話號碼
			po.setSTAFF(dataMap.get("STAFF")!=null ? dataMap.get("STAFF").toString() : po.getSTAFF()); // 是否為行員
			po.setMBSTAT(dataMap.get("MBSTAT")!=null ? dataMap.get("MBSTAT").toString() : po.getMBSTAT()); // 行動銀行目前狀態
			po.setMBOPNDT(dataMap.get("MBOPNDT")!=null ? dataMap.get("MBOPNDT").toString() : po.getMBOPNDT()); // 行動銀行啟用/關閉日期
			po.setMBOPNTM(dataMap.get("MBOPNTM")!=null ? dataMap.get("MBOPNTM").toString() : po.getMBOPNTM()); // 行動銀行啟用/關閉時間
			po.setIDNCOD("1"); // 1 個人模式  2 企業模式
			po.setAPATR(dataMap.get("AUTHORITY")!=null ? dataMap.get("AUTHORITY").toString() : po.getAPATR()); // 申請網銀約定業務權限功能
			po.setMOBTEL(dataMap.get("MOBTEL")!=null ? dataMap.get("MOBTEL").toString() : po.getMOBTEL()); // 行動電話
			
			log.trace("Login_out_Service.setAdmLogin: {}", CodeUtil.toJson(po));
			admlogindao.saveOrUpdate(po);
			
			result = true;
			
		} catch (Exception e) {
			log.error("{}", e);
		}
		
		return result;
	}
	
	/**
	 * 取得 使用者 登入/登出 狀態檔 資訊
	 */
	public ADMLOGIN getAdmLoginout(String cusidn) {
		ADMLOGIN po = null;
		try {
			log.trace("Login_out_Service.getAdmLoginout...");

			// 取得登入時資訊
			List<ADMLOGIN> admloginList = admlogindao.findByUserId(cusidn);
			if(admloginList != null && !admloginList.isEmpty()) {
				po = admlogindao.findByUserId(cusidn).get(0);
			} else {
				po = new ADMLOGIN();
			}
			
		} catch (Exception e) {
			log.error("{}", e);
		}
		
		return po;
	}
		
	/**
	 * 登出時--紀錄使用者 登入/登出 狀態檔 資訊
	 */
	public void setAdmLogout(String cusidn) {
		List<ADMLOGIN> admloginList = null;
		try {
			log.trace("Login_out_Service.setAdmLogin...");

			// 取得登入時資訊
			admloginList = admlogindao.findByUserId(cusidn);
			if(admloginList != null && !admloginList.isEmpty()) {
				for(ADMLOGIN po : admloginList) {
					po.setADUSERID(cusidn); // 使用者統編ID
					po.setLOGINOUT("1"); // 登入狀態	0:登入 1:登出
					po.setLOGOUTTIME(new DateTime(new Date()).toString("yyyyMMddHHmmss")); // 本次正常登出日期時間
					log.trace(ESAPIUtil.vaildLog("Login_out_Service.setAdmLogout: {}"+CodeUtil.toJson(po)));
					admlogindao.update(po);
				}
			}
			
		} catch (Exception e) {
			log.error("{}", e);
		}
	}
	
	/**
	 * 驗證使用者 登入/登出 狀態
	 * @param cusidn 使用者統編
	 * @return
	 */
	public Boolean isLoginStatus(String cusidn) {
		boolean result = false;
		List<ADMLOGIN> admloginList = null;
		
		try {
			log.trace(ESAPIUtil.vaildLog("Login_out_Service.isLoginStatus.cusidn: " + cusidn));
			
			String loginout = "";
			
			// 取得登入時資訊
			admloginList = admlogindao.findByUserId(cusidn);
			if(admloginList != null && !admloginList.isEmpty()) {
				for(ADMLOGIN po : admloginList) {
					loginout = po.getLOGINOUT();
				}
			}
			
			// 登入狀態	0:登入 1:登出
			result = "0".equals(loginout) ? true : false;
			
		} catch (Exception e) {
			log.error("{}", e);
		}
		
		return result;
	}
	
	
	/**
	 * 驗證使用者Session的TokenId是否跟登入時一致
	 * @param cusidn 使用者統編
	 * @param tokenid 登入時戳記
	 * @return
	 */
	public boolean isLoginToken(String cusidn, String tokenid) {
		boolean result = false;
		List<ADMLOGIN> admloginList = null;
		
		try {
			log.trace(ESAPIUtil.vaildLog("Login_out_Service.isLoginToken.cusidn: " + cusidn));
			
			String loginToken = "";
			
			// 取得登入時資訊
			admloginList = admlogindao.findByUserId(cusidn);
			if(admloginList != null && !admloginList.isEmpty()) {
				for(ADMLOGIN po : admloginList) {
					loginToken = po.getTOKENID();
				}
			}
			
			//  驗證TOKENID
			if ( StrUtil.isNotEmpty(tokenid) && tokenid.equals(loginToken) ) {
				// 通過
				result = true;
			} else {
				// 驗證錯誤
				log.warn(ESAPIUtil.vaildLog("isLoginToken.fail.tokenid: " + tokenid));
				log.warn(ESAPIUtil.vaildLog("isLoginToken.fail.loginToken: " + loginToken));
			}
			
		} catch (Exception e) {
			log.error("{}", e);
		}

		return result;
	}
	
	
	
	/**
	 * 驗證IP 登入/登出 狀態
	 * @param cusidn 使用者統編
	 * @return
	 */
	public Boolean isLoginIp(String cusidn, String ip) {
		boolean result = false;
		List<ADMLOGIN> admloginList = null;
		
		try {
			log.trace(ESAPIUtil.vaildLog("Login_out_Service.isLoginIp.ip: " + ip));
			
			// 取得登入時資訊
			admloginList = admlogindao.findByIp(cusidn, ip);
			if(admloginList != null && !admloginList.isEmpty()) {
				result = true;
			}
			
		} catch (Exception e) {
			log.error("{}", e);
		}
		
		return result;
	}
	
	/**
	 * 驗證使用者 登入/登出 IP
	 * @param cusidn 使用者統編
	 * @return
	 */
	public String loginIP(String cusidn) {
		String aduserip = "";
		List<ADMLOGIN> admloginList = null;
		
		try {
			log.trace("Login_out_Service.isLoginStatus...");
			
			// 取得登入時資訊
			admloginList = admlogindao.findByUserId(cusidn);
			if(admloginList != null && !admloginList.isEmpty()) {
				for(ADMLOGIN po : admloginList) {
					aduserip = po.getADUSERIP();
				}
			}
			
		} catch (Exception e) {
			log.error("{}", e);
		}
		
		return aduserip;
	}
	
	/**
	 * 強迫登出時--紀錄使用者 登入/登出 狀態檔 資訊
	 * @param cusidn 使用者統編
	 * @return
	 */
	public void setAdmForceLogout(String cusidn) {
		List<ADMLOGIN> admloginList = null;
		try {
			log.trace(ESAPIUtil.vaildLog("Login_out_Service.setAdmForceLogout.cusidn: " + cusidn));
			
			// 取得登入時資訊
			admloginList = admlogindao.findByUserId(cusidn);
			if(admloginList != null && !admloginList.isEmpty()) {
				for(ADMLOGIN po : admloginList) {
					po.setADUSERID(cusidn); // 使用者統編ID
					po.setLOGINOUT("1"); // 登入狀態	0:登入 1:登出
					po.setFORCELOGOUTTIME(new DateTime(new Date()).toString("yyyyMMddHHmmss")); // 本次強迫登出日期時間
					po.setLASTLOGINTIME(po.getLOGINTIME());	// 上次登入日期時間
					po.setLASTADUSERIP(po.getLASTADUSERIP()); // 上次登入IP
					log.trace("Login_out_Service.setAdmForceLogout: {}", CodeUtil.toJson(po));
					admlogindao.update(po);
				}
			}
			
		} catch (Exception e) {
			log.error("{}", e);
		}
	}

	/**
	 * 檢查使用者登入狀態
	 * @param cusidn 使用者統編
	 * @return
	 */
	public Boolean checkLogin(Map<String, String> okMap, String ip) {
		log.trace("checkLogin...");
		boolean result = true;
		try {
			String cusidn = okMap.get("cusidn").toUpperCase();
			log.trace(ESAPIUtil.vaildLog("checkLogin.cusidn: " + cusidn));
			boolean isLoginStatus = isLoginStatus(cusidn);
			log.trace("checkLogin.isLoginStatus: " + isLoginStatus);
			String reqIP = loginIP(cusidn);
			log.trace(ESAPIUtil.vaildLog("checkLogin.reqIP: " + reqIP));
			
			// 新世代網銀是登入狀態且登入ip不同，需要後踢前才可登入
			if( isLoginStatus && !reqIP.equals(ip)) {
				result = false;
			}
			
		} catch (Exception e) {
			log.error("{}", e);
		}
		return result;
	}

	/**
	 * 取得menu list
	 */
	@Cacheable(value="ADMMBCache",key="'admmb'",unless="#result == null")
	public String getAdmNbStatu() {
		ADMNBSTATUS result = admNbStatusDao.findByID("TBBNB");
		return result.getADNBSTATUS();
	}
	@CacheEvict(value="ADMMBCache",key="'admmb'")
	public void deleteAdmNbStatu() {
	}
	/**
	 * 取得aclip list
	 */
	public List<String> getADMLOGINACLIP() {
		List<ADMLOGINACLIP> ipList = admLoginAclIpDao.getAll();
		List<String> result = new ArrayList<String>();
		
		log.trace("ADMLOGINACLIP list.size >> {}", ipList.size());
		
		for(ADMLOGINACLIP po : ipList) {
			result.add(po.getIP());
		}
		
		return result;
	}
	
	/**
	 * 取得menu list
	 */
	@Cacheable(value="menuCache",key="'auth_' + #type",unless="#result.data == null")
	public BaseResultSerialization getMenuList(String type) {
		BaseResultSerialization bs = null;
		BaseResultSerialization bs2 = null;
		BaseResultSerialization bs3 = null;
		try {
			bs = new BaseResultSerialization();
			bs2 = new BaseResultSerialization();
			List<NB3SYSOP> menu = nb3SysOpDao.getAll();
			Map<String,Object> menuObject = new HashMap<String,Object>();
			menuObject.put("menu", menu);
			CodeUtil.convert2BaseResult(bs, menuObject,true);
			List<NB3SYSOP> menu2 = nb3SysOpDao.getAll();
			Map<String,Object> menuObject2 = new HashMap<String,Object>();
			menuObject2.put("menu", menu2);
			CodeUtil.convert2BaseResult(bs2, menuObject2,true);
			bs3 = new BaseResultSerialization();
			if(!type.equals("null"))
				bs.addData("sort_menu", sortMap((Map<String,Object>)bs.getData(),"",type,bs3));
			bs.addData("sort_menu_only_name", sortMap((Map<String,Object>)bs2.getData(),""));
			log.trace("urlKeyMap >> {}", bs3.getData());
			bs.addData("urlKeyMap", bs3.getData());
//			log.debug("menu>>{}",(Map<String,Object>)bs.getData());
		}catch(Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getMenuList error >> {}",e);
		}
		return bs;
	}
	
	@CacheEvict(value="menuCache",key="'auth_' + #type")
	public void deleteMenuList(String type) {
	}
	
	public Boolean ckickIsIn(String adopauthtype, String type) {
		Boolean flag = false;
		String[] authtypes = adopauthtype.split(",");
		for(String authtype: authtypes) {
			if(authtype.equals(type)) {
				flag = true;
			}
		}
		return flag;
	}
	
	public List<Map<String,Object>> sortMap(Map<String,Object> list,String name,String type, BaseResultSerialization bs){
		List<Map<String,Object>> layer = new ArrayList<Map<String,Object>>();
		try {
			List<Map<String,Object>> menu = (List<Map<String,Object>>)list.get("menu");
			for(Map<String,Object>op : menu) {
				if(((String)op.get("ADOPGROUPID")).equals(name) &&  ckickIsIn((String)op.get("ADOPAUTHTYPE"),type)) {
					//塞入可供快速選單查詢用列表
					if( ((String)op.get("ADOPALIVE")).equals("1") && ((String)op.get("ISANONYMOUS")).equals("0") ) {
						bs.addData((String)op.get("URL") , (String)op.get("ADOPID"));
					}
					List<Map<String,Object>> seed_layer = sortMap(list,(String)op.get("ADOPID"),type, bs);
					op.put("SEED_LAYER", seed_layer);
					layer.add(op);
				}
			}
			Collections.sort( layer, new Comparator<Map<String,Object>>(){
			    public int compare( Map<String,Object> op1, Map<String,Object> op2 )
			    {
			    	if(op1 == null && op2 == null) {  
			    	    return 0;  
			    	}  
			    	if(op1 == null) {  
			    	    return -1;  
			    	}  
			    	if(op2 == null) {
			    	    return 1;  
			    	}
			    	String op1_seq = (String)op1.get("ADSEQ");
			    	String op2_seq = (String)op2.get("ADSEQ");
//			    	log.debug("{} > {} => {}", op1_seq, op2_seq, Integer.parseInt(op1_seq) < Integer.parseInt(op2_seq));
			        // 回傳值: -1 前者比後者小, 0 前者與後者相同, 1 前者比後者大
			    	if(op1_seq == null && op2_seq == null) {  
			    	    return 0;  
			    	}  
			    	if(op1_seq == null) {  
			    	    return -1;  
			    	}  
			    	if(op2_seq == null) {
			    	    return 1;  
			    	}
			    	if(Integer.parseInt(op1_seq) > Integer.parseInt(op2_seq)) {  
			    	    return 1;  
			    	}  
			    	if(Integer.parseInt(op2_seq) > Integer.parseInt(op1_seq)) {  
			    	    return -1;  
			    	}  
			    	return 0;  
			    	
//			        return Integer.parseInt(op1_seq) < Integer.parseInt(op2_seq)? -1: 1;
			    }
			});
		}catch(Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("sortMap error >> {}",e);
		}
		return layer;
	}
	
	public List<Map<String,Object>> sortMap(Map<String,Object> list,String name){
		List<Map<String,Object>> layer = new ArrayList<Map<String,Object>>();
		try {
			List<Map<String,Object>> menu = (List<Map<String,Object>>)list.get("menu");
			for(Map<String,Object>op : menu) {
				if(((String)op.get("ADOPGROUPID")).equals(name)) {
//					op.put("URL", "");
					List<Map<String,Object>> seed_layer = sortMap(list,(String)op.get("ADOPID"));
					op.put("SEED_LAYER", seed_layer);
					layer.add(op);
				}
			}
			Collections.sort( layer, new Comparator<Map<String,Object>>(){
			    public int compare( Map<String,Object> op1, Map<String,Object> op2 )
			    {
			    	if(op1 == null && op2 == null) {  
			    	    return 0;  
			    	}  
			    	if(op1 == null) {  
			    	    return -1;  
			    	}  
			    	if(op2 == null) {
			    	    return 1;  
			    	}
			    	String op1_seq = (String)op1.get("ADSEQ");
			    	String op2_seq = (String)op2.get("ADSEQ");
//			    	log.debug("{} > {} => {}", op1_seq, op2_seq, Integer.parseInt(op1_seq) < Integer.parseInt(op2_seq));
			        // 回傳值: -1 前者比後者小, 0 前者與後者相同, 1 前者比後者大
			    	if(op1_seq == null && op2_seq == null) {  
			    	    return 0;  
			    	}  
			    	if(op1_seq == null) {  
			    	    return -1;  
			    	}  
			    	if(op2_seq == null) {
			    	    return 1;  
			    	}
			    	if(Integer.parseInt(op1_seq) > Integer.parseInt(op2_seq)) {  
			    	    return 1;  
			    	}  
			    	if(Integer.parseInt(op2_seq) > Integer.parseInt(op1_seq)) {  
			    	    return -1;  
			    	}  
			    	return 0;  
			    	
//			        return Integer.parseInt(op1_seq) < Integer.parseInt(op2_seq)? -1: 1;
			    }
			});
		}catch(Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("sortMap error >> {}",e);
		}
		return layer;
	}
	
	
	/**
	 * 驗證是否為行內IP且使用行內電腦
	 * @param loginip Client端IP
	 * @param staff 是否為行員
	 * @return
	 */
	public BaseResult ipVerifyNP10(String cusidn, String loginip){
		log.debug("ipVerify...");
		
		BaseResult bsNP10 = new BaseResult();
		try {
			// NP10--檢查行員才能使用行內IP
			bsNP10 = NP10_REST(cusidn, loginip);
			
		} catch (Exception e) {
			log.error("{}", e);
		}
		return bsNP10; // 驗證通過
	}
	
	
	/**
	 * 驗證是否為行內IP且使用行內電腦
	 * @param loginip Client端IP
	 * @param staff 是否為行員
	 * @return
	 */
	public boolean ipVerify(String cusidn, String loginip, String staff){
		log.debug("ipVerify...");
		try {

			// NP10--檢查行員才能使用行內IP
//			BaseResult bsNP10 = NP10_REST(cusidn, loginip);
//			if( bsNP10.getData() != null ) {
//				String np10result = ((Map<String, Object>) bsNP10.getData()).get("MSGCOD") != null
//						? String.valueOf(((Map<String, Object>) bsNP10.getData()).get("MSGCOD")) : "";
//						
//				if (!"0000".equals(np10result)) {
//					return false; // 驗證失敗
//				}
//			}
			
			// 屬於網銀專區白名單不須驗證IP
			if(isWhiteListUser(cusidn)) {
				log.debug(ESAPIUtil.vaildLog("ipVerify.whitelist.cusidn: {}"+ cusidn));
				return true; 
			}
			
			// 是否為行員
//			String staff = "";
//			if( bs.getData() != null ) {
//				staff = ((Map<String, Object>) bs.getData()).get("STAFF") != null
//						? String.valueOf(((Map<String, Object>) bs.getData()).get("STAFF")) : "";
//			}
			
			// 截取ip最後小數點後的數字，使用來判斷網銀專區ip
	        String nbarea = loginip.substring(loginip.lastIndexOf(".")+1);
	        log.debug(ESAPIUtil.vaildLog("LOGINIP >> " + loginip + "STAFF >>" + staff +"nbarea >>"+nbarea)); 
	      
	        
	        // 非行員於行內IP使用網銀電腦
	        if ( !staff.equals("Y") && isInHouseIP(loginip) && !nbarea.equals("151") ) {
	        	log.warn(ESAPIUtil.vaildLog("ipVerify.warning: cusidn>>" + cusidn + ", LOGINIP>>" + loginip + ", STAFF>>" + staff + ", nbarea>>" + nbarea));
	        	log.warn("ipVerify.result: " + "非行員於行內IP使用非網銀專區的電腦");
	            return false; // 驗證失敗
	        }
	        
		} catch (Exception e) {
			log.error("{}", e);
		}
		return true; // 驗證通過
	}
	
	/* 是否為行內IP */
	public boolean isInHouseIP(String sIP) {
		try {
			// IP合法性
		    if (sIP == null || sIP.length() < 8 || sIP.indexOf(".") == -1) {
		        return false;
		    }
		    // 非行內IP
		    String sIPHead = sIP.substring(0, sIP.indexOf("."));
		    if (!sIPHead.equals("10")) {
		        return false;
		    }
		    
		} catch (Exception e) {
			log.error("{}", e);
		}
	    return true;
	}
	
	/**
	 * 公告訊息
	 * 
	 * @return
	 */
	public List<ADMANN> getByAll() {
		List<ADMANN> list_result = new ArrayList<ADMANN>();
		try {
			List<ADMANN> list_admad = admannDao.getByAll();
			log.trace(ESAPIUtil.vaildLog("getByUser.list_admad: " + CodeUtil.toJson(list_admad)));
			for ( ADMANN po : list_admad ) {
				ADMANN newPo = new ADMANN();
				newPo.setID(po.getID());
				newPo.setTYPE(po.getTYPE());
				newPo.setTITLE(po.getTITLE());
				newPo.setCONTENTTYPE(po.getCONTENTTYPE());
				newPo.setURL(po.getURL());
				newPo.setCONTENT(po.getCONTENT());
				newPo.setSRCCONTENT(po.getSRCCONTENT());
				list_result.add(newPo);
			}
			log.trace(ESAPIUtil.vaildLog("getByUser.list_result: " + CodeUtil.toJson(list_result)));
		} catch (Exception e) {
			log.error("{}", e);
		}
		return list_result;
	}
	
	/**
	 * 公告訊息 by id
	 * 
	 * @return
	 */
	public ADMANN getById(String id) {
		ADMANN list_admad = admannDao.getById(id);
		log.trace(ESAPIUtil.vaildLog("getByUser.list_admad: " + CodeUtil.toJson(list_admad)));
		return list_admad;
	}
	
	/**
	 * 廣告訊息 by id
	 * 
	 * @return
	 */
	public ADMADS getAdsById(String id) {
		ADMADS list_admad = admadsdao.getById(id);
		log.trace(ESAPIUtil.vaildLog("getByUser.list_admad: " + CodeUtil.toJson(list_admad)));
		return list_admad;
	}
	
	/**
	 * 大廣告訊息
	 * 
	 * @return
	 */
	public ADMADS getAds(String num) {
		log.debug(ESAPIUtil.vaildLog("get num>>>"+num));
		ADMADS list_admad = admadsdao.getOrder(num);
		log.trace(ESAPIUtil.vaildLog("getByUser.list_admad: " + CodeUtil.toJson(list_admad)));
		return list_admad;
	}
	

	/**
	 * 使用者是否於網銀專區白名單中
	 * @param cusidn 使用者統編
	 * @return
	 */
	public Boolean isWhiteListUser(String cusidn) {
		boolean result = true;
		List<PASSIP> whiteListIPList = null;
		
		try {
			log.trace(ESAPIUtil.vaildLog("isWhiteListUser.cusidn: {}"+ cusidn));
			
			// 取得登入時資訊
			whiteListIPList = passipdao.findByUserId(cusidn);
			log.trace(ESAPIUtil.vaildLog("isWhiteListUser.passipList: "+ CodeUtil.toJson(whiteListIPList)));
			
			// 有資料代表是白名單成員
			result = (whiteListIPList != null && !whiteListIPList.isEmpty()) ? true : false;
			log.trace("isWhiteListUser.result: {}", result);
			
		} catch (Exception e) {
			log.error("{}", e);
		}
		
		return result;
	}
	
	/**
	 * 使用者是否於正式上線第一階段白名單中
	 * @param cusidn 使用者統編
	 * @return
	 */
	public Boolean isProdAcl(String cusidn) {
		boolean result = true;
		List<ADMLOGINACL> admloginaclList = null;
		
		try {
			log.trace(ESAPIUtil.vaildLog("isProdAcl.cusidn: {}"+ cusidn));
			
			// 取得登入時資訊
			admloginaclList = admloginacldao.findByUserId(cusidn);
			log.trace(ESAPIUtil.vaildLog("isProdAcl.admloginaclList: {}"+ CodeUtil.toJson(admloginaclList)));
			
			// 有資料代表是白名單成員
			result = (admloginaclList != null && !admloginaclList.isEmpty()) ? true : false;
			log.trace("isProdAcl.result: {}", result);
			
		} catch (Exception e) {
			log.error("{}", e);
		}
		
		return result;
	}
	
	
	/**
	 * NA70 網路銀行簽入密碼／使用者代號線上解鎖
	 * 
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult lock_reset_result(String cusidn, Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = NA70_REST(cusidn, reqParam);
			
		} catch (Exception e) {
			log.error("lock_reset_result: {}", e);
		}
		return bs;
	}

	
	/**
	 * ADMLOGINOUT記錄登入失敗錯誤代碼
	 * 
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public void setLoginExCode(BaseResult bs, String cusidn, String userip, Date logintime, String tokenId) {
		try {
			String excode = bs.getMsgCode();
			if (!"0".equals(excode)) {
				ADMLOGINOUT po = new ADMLOGINOUT();
				po.setADUSERTYPE((StrUtils.trim(cusidn).length() == 10 ? "0" : "1")); // 操作者類別	0:個人戶 (預設) 1:企業戶 (獨資統編)
				po.setADUSERID(cusidn); // 使用者統編ID
				po.setADUSERIP(userip); // 登入 IP-Address
				po.setCMYYYYMM(new DateTime(logintime).toString("yyyyMM"));
				po.setCMDD(new DateTime(logintime).toString("dd"));
				po.setCMTIME(new DateTime(logintime).toString("HHmmss"));
				po.setLOGINOUT("0"); // 登入
				po.setLOGINTYPE("NB"); // 登入系統
				po.setTOKENID(tokenId); // 登入取得的UUID
				po.setADEXCODE(excode); // 錯誤代碼
				
				admloginoutdao.save(po);
			}
			
		} catch (Exception e) {
			log.error("",e);
		}
	}

	/**
	 * ADMADS廣告落地更新時間
	 * 
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public String getAdsUpdateDT(String adsType) {
		log.debug("getAdsUpdateDT.type: " + adsType);
		String adsudt = ""; // 廣告更新時間戳
		
		try {
			String separ = File.separator;
			String path = ""; // 廣告類別路徑
			
			// 大廣告
			if ("B".equals(adsType)) {
				// 時間戳路徑
				path = context.getRealPath("") + separ + "com" + separ + "login" + separ + "banner";
				File file = new File(path + separ + "lt");
				file.setWritable(true, true);
				file.setReadable(true, true);
				file.setExecutable(true, true);
				file.mkdirs(); // 創建目錄如果目錄不存在
				
				// 比對時間戳
				if (file.isDirectory()) {
					String[] filenames = file.list();
					adsudt = Arrays.asList(filenames).isEmpty() ? "0" : filenames[0];
					
					if (StrUtils.isNotEmpty(adsudt)) {
						adsudt = adsudt.replaceAll(" ", "");
					}
				}
				
			}
			
			// 小廣告
			if ("C".equals(adsType)) {
				// 時間戳路徑
				path = context.getRealPath("") + separ + "com" + separ + "login" + separ + "news";
				File file = new File(path + separ + "lt");
				file.setWritable(true, true);
				file.setReadable(true, true);
				file.setExecutable(true, true);
				file.mkdirs(); // 創建目錄如果目錄不存在
				
				// 比對時間戳
				if (file.isDirectory()) {
					String[] filenames = file.list();
					adsudt = Arrays.asList(filenames).isEmpty() ? "0" : filenames[0];
					
					if (StrUtils.isNotEmpty(adsudt)) {
						adsudt = adsudt.replaceAll(" ", "");
					}
				}
				
			}

		} catch (Exception e) {
			log.error("", e);
		}

		return adsudt;
	}

	
	/**
	 * 寄信通知登入結果
	 * 
	 * @param cusidn 使用者統編
	 * @param loginResut 登入成功與否
	 * @return
	 */
	public void sendMail(String cusidn, boolean loginResut) {
		String useremail = "";
		try {
			List<TXNUSER> txnusers = txnUserDao.findByUserId(cusidn);
			if (txnusers != null && !txnusers.isEmpty()) {
				TXNUSER user = txnusers.get(0);
				useremail = user.getDPMYEMAIL();
				if (StrUtils.isNotEmpty(useremail)) {
					List<String> Receivers = new ArrayList<String>();
					Receivers.add(useremail);
					send_mail_service.SendMail(cusidn, "login", loginResut, Receivers);
				}
			}
			
		} catch (Exception e) {
			log.error("", e);
		}
	}
	
}
