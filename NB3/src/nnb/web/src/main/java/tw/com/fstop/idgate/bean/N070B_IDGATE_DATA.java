package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class N070B_IDGATE_DATA implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 777167011845422448L;
	
	@SerializedName(value = "CUSIDN")
	private String CUSIDN;// 統一編號
	@SerializedName(value = "ACN")
	private String ACN;// 帳號
	@SerializedName(value = "FGSVACNO")
	private String FGSVACNO; // 約定帳號(1) or 非約定帳號(2)
	@SerializedName(value = "DPAGACNO")
	private String DPAGACNO; // 轉入帳號
	@SerializedName(value = "DPACNO")
	private String DPACNO; // 轉入帳號2
	@SerializedName(value = "AMOUNT")
	private String AMOUNT;
	
	
}
