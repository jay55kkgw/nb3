package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N860_REST_RSDATA extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2177576544172816296L;
	
	private String ACN;
	private LinkedList<N860_REST_RSDATA2> TABLE;
	private String MSGCOD;
	
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public LinkedList<N860_REST_RSDATA2> getTable() {
		return TABLE;
	}
	public void setTable(LinkedList<N860_REST_RSDATA2> tABLE) {
		TABLE = tABLE;
	}
	public String getMSGCOD() {
		return MSGCOD;
	}
	public void setMSGCOD(String mSGCOD) {
		MSGCOD = mSGCOD;
	}

}
