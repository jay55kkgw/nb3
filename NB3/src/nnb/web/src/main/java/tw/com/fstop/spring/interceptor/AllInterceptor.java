
/*
 * Copyright (c) 2017, FSTOP, Inc. All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tw.com.fstop.spring.interceptor;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.WebRequestInterceptor;

import tw.com.fstop.spring.helper.I18n;

public class AllInterceptor implements WebRequestInterceptor 
{

    @Autowired
    private I18n i18n;

    
    @Override
    public void afterCompletion(WebRequest req, Exception e) throws Exception
    {
    }

    @Override
    public void postHandle(WebRequest req, ModelMap map) throws Exception
    {
        if (map != null)
        {            
            map.put("i18n", i18n);
        }


    }

    @Override
    public void preHandle(WebRequest req) throws Exception
    {
        
    }

}
