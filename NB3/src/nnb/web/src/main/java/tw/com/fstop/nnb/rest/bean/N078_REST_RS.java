package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N078_REST_RS extends BaseRestBean implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5781376602039886902L;

	private String OFFSET;// 空白

	private String HEADER;// HEADER

	private String SYNC;// Sync.Check Item

	private String MSGCOD;// 回應代碼

	private String FDPACN;// 存單帳號

	private String FDPNUM;// 存單號碼

	private String FDPACC;// 存單種類

	private String AMOUNT;// 存單金額

	private String TERM;// 存單期別

	private String SDT;// 起存日

	private String DUEDAT;// 到期日

	private String INTMTH;// 計息方式

	private String ITR;// 利率

	private String TSFACN;// 轉帳帳號

	private String FDPAMT;// 原存單金額

	private String PYDINT;// 原存單利息

	private String GRSTAX;// 代扣利息所得稅

	private String NHITAX;// 健保費

    private String CMQTIME;

    private String __OCCURS;

    private String CMTXTIME;

	public String getOFFSET()
	{
		return OFFSET;
	}

	public void setOFFSET(String oFFSET)
	{
		OFFSET = oFFSET;
	}

	public String getHEADER()
	{
		return HEADER;
	}

	public void setHEADER(String hEADER)
	{
		HEADER = hEADER;
	}

	public String getSYNC()
	{
		return SYNC;
	}

	public void setSYNC(String sYNC)
	{
		SYNC = sYNC;
	}

	public String getMSGCOD()
	{
		return MSGCOD;
	}

	public void setMSGCOD(String mSGCOD)
	{
		MSGCOD = mSGCOD;
	}

	public String getFDPACN()
	{
		return FDPACN;
	}

	public void setFDPACN(String fDPACN)
	{
		FDPACN = fDPACN;
	}

	public String getFDPNUM()
	{
		return FDPNUM;
	}

	public void setFDPNUM(String fDPNUM)
	{
		FDPNUM = fDPNUM;
	}

	public String getFDPACC()
	{
		return FDPACC;
	}

	public void setFDPACC(String fDPACC)
	{
		FDPACC = fDPACC;
	}

	public String getAMOUNT()
	{
		return AMOUNT;
	}

	public void setAMOUNT(String aMOUNT)
	{
		AMOUNT = aMOUNT;
	}

	public String getTERM()
	{
		return TERM;
	}

	public void setTERM(String tERM)
	{
		TERM = tERM;
	}

	public String getSDT()
	{
		return SDT;
	}

	public void setSDT(String sDT)
	{
		SDT = sDT;
	}

	public String getDUEDAT()
	{
		return DUEDAT;
	}

	public void setDUEDAT(String dUEDAT)
	{
		DUEDAT = dUEDAT;
	}

	public String getINTMTH()
	{
		return INTMTH;
	}

	public void setINTMTH(String iNTMTH)
	{
		INTMTH = iNTMTH;
	}

	public String getITR()
	{
		return ITR;
	}

	public void setITR(String iTR)
	{
		ITR = iTR;
	}

	public String getTSFACN()
	{
		return TSFACN;
	}

	public void setTSFACN(String tSFACN)
	{
		TSFACN = tSFACN;
	}

	public String getFDPAMT()
	{
		return FDPAMT;
	}

	public void setFDPAMT(String fDPAMT)
	{
		FDPAMT = fDPAMT;
	}

	public String getPYDINT()
	{
		return PYDINT;
	}

	public void setPYDINT(String pYDINT)
	{
		PYDINT = pYDINT;
	}

	public String getGRSTAX()
	{
		return GRSTAX;
	}

	public void setGRSTAX(String gRSTAX)
	{
		GRSTAX = gRSTAX;
	}

	public String getNHITAX()
	{
		return NHITAX;
	}

	public void setNHITAX(String nHITAX)
	{
		NHITAX = nHITAX;
	}

	public String getCMQTIME()
	{
		return CMQTIME;
	}

	public void setCMQTIME(String cMQTIME)
	{
		CMQTIME = cMQTIME;
	}

	public String get__OCCURS()
	{
		return __OCCURS;
	}

	public void set__OCCURS(String __OCCURS)
	{
		this.__OCCURS = __OCCURS;
	}

	public String getCMTXTIME()
	{
		return CMTXTIME;
	}

	public void setCMTXTIME(String cMTXTIME)
	{
		CMTXTIME = cMTXTIME;
	}

}
