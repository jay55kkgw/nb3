package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class Cusidn_REST_RQ extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3618973959340371339L;
	
	
	private String CUSIDN;
	
	private String TYPE;


	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}

	public String getTYPE() {
		return TYPE;
	}

	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}


}
