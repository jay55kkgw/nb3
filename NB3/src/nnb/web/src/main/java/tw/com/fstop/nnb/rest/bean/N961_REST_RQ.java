package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N961_REST_RQ extends BaseRestBean_OLS implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8412192250208527166L;
	private String CUSIDN;
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
}
