package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N615_REST_RQ extends BaseRestBean_TW implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1463058301290929722L;
	String	ACN	;
	String	CUSIDN	;
	String	STADATE ;
	String	ENDDATE ;
	String  CMSDATE ;
	String  CMEDATE ;
	
	
	public String getCMSDATE() {
		return CMSDATE;
	}
	public void setCMSDATE(String cMSDATE) {
		CMSDATE = cMSDATE;
	}
	public String getCMEDATE() {
		return CMEDATE;
	}
	public void setCMEDATE(String cMEDATE) {
		CMEDATE = cMEDATE;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getSTADATE() {
		return STADATE;
	}
	public void setSTADATE(String sTADATE) {
		STADATE = sTADATE;
	}
	public String getENDDATE() {
		return ENDDATE;
	}
	public void setENDDATE(String eNDDATE) {
		ENDDATE = eNDDATE;
	}
	
	
	
	

}
