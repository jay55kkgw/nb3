package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;


public class N750C_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{

	//交易類別
	@SerializedName(value = "FGTXDATE")
	private String FGTXDATE;
	//交易時間
	@SerializedName(value = "transfer_date")
	private String transfer_date;
	//轉出帳號
	@SerializedName(value = "OUTACN")
	private String OUTACN;
	//條碼一
	@SerializedName(value = "BARCODE1")
	private String BARCODE1;	
	//條碼二
	@SerializedName(value = "BARCODE2")
	private String BARCODE2;
	//條碼三
	@SerializedName(value = "BARCODE3")
	private String BARCODE3;
	//繳款金額
	@SerializedName(value = "IDGATE_AMOUNT")
	private String IDGATE_AMOUNT;
	
	@Override
	public Map<String, String> coverMap(){
		Map<String, String> result = new LinkedHashMap<String, String>();
		String fgtxdate = "";
		if("1".equals(this.FGTXDATE))
			fgtxdate = "即時繳費";
		else if("2".equals(this.FGTXDATE))
			fgtxdate = "預約繳費";
		result.put("交易名稱", "繳費-勞保費");
		result.put("交易類別", fgtxdate);
		result.put("轉帳日期", this.transfer_date);
		result.put("轉出帳號", this.OUTACN);
		result.put("條碼一", this.BARCODE1);
		result.put("條碼二", this.BARCODE2);
		result.put("條碼三", this.BARCODE3);
		result.put("繳款金額", "新台幣 "+ this.IDGATE_AMOUNT+" 元");
		return result;
	}

	public String getFGTXDATE() {
		return FGTXDATE;
	}

	public void setFGTXDATE(String fGTXDATE) {
		FGTXDATE = fGTXDATE;
	}


	public String getTransfer_date() {
		return transfer_date;
	}

	public void setTransfer_date(String transfer_date) {
		this.transfer_date = transfer_date;
	}

	public String getOUTACN() {
		return OUTACN;
	}

	public void setOUTACN(String oUTACN) {
		OUTACN = oUTACN;
	}

	public String getBARCODE1() {
		return BARCODE1;
	}

	public void setBARCODE1(String bARCODE1) {
		BARCODE1 = bARCODE1;
	}

	public String getBARCODE2() {
		return BARCODE2;
	}

	public void setBARCODE2(String bARCODE2) {
		BARCODE2 = bARCODE2;
	}

	public String getBARCODE3() {
		return BARCODE3;
	}

	public void setBARCODE3(String bARCODE3) {
		BARCODE3 = bARCODE3;
	}

	public String getIDGATE_AMOUNT() {
		return IDGATE_AMOUNT;
	}

	public void setIDGATE_AMOUNT(String iDGATE_AMOUNT) {
		IDGATE_AMOUNT = iDGATE_AMOUNT;
	}



	
	
}
