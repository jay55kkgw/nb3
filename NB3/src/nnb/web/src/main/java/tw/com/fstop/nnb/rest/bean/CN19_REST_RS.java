package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class CN19_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6689507774335171979L;

	private String OFFSET;	// 空白
	private String HEADER;	// HEADER
	private String CN;	// 憑證識別資料
	private String DATAPL;	//密碼有效日期
	private String DATTIM;// 密碼有效時間
	
	
	public String getOFFSET() {
		return OFFSET;
	}
	public void setOFFSET(String oFFSET) {
		OFFSET = oFFSET;
	}
	public String getHEADER() {
		return HEADER;
	}
	public void setHEADER(String hEADER) {
		HEADER = hEADER;
	}
	public String getCN() {
		return CN;
	}
	public void setCN(String cN) {
		CN = cN;
	}
	public String getDATAPL() {
		return DATAPL;
	}
	public void setDATAPL(String dATAPL) {
		DATAPL = dATAPL;
	}
	public String getDATTIM() {
		return DATTIM;
	}
	public void setDATTIM(String dATTIM) {
		DATTIM = dATTIM;
	}
	
}
