package tw.com.fstop.nnb.spring.controller;

import java.util.Base64;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.nnb.service.Online_Apply_Fatca_Service;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;

@SessionAttributes({
	SessionUtil.CUSIDN,
	SessionUtil.CUSIDN_N361
})

@Controller
@RequestMapping(value = "/ONLINE/APPLY/FATCA")
public class Online_Apply_Fatca_Controller {
	
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private Online_Apply_Fatca_Service online_apply_fatca_service;

	//FATCA及CRS個人客戶自我聲明書暨個人資料同意書頁面
	@RequestMapping(value = "/fatca_and_crs_consent")
	public String apply_menu(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/online_apply/fatca_and_crs_consent";
		log.debug("/online_apply/fatca_and_crs_consent start");
		
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		
		online_apply_fatca_service.prepareFatcaConsentData(okMap, model);

		String next = "";
		String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
		
		if (cusidn != null) {
			next = "/ONLINE/APPLY/apply_trust_account_KYC";
		} else {
			next = "/ONLINE/APPLY/apply_fund_account_KYC";
			cusidn = String.valueOf(SessionUtil.getAttribute(model, SessionUtil.CUSIDN_N361, null));
		}
		cusidn = new String(Base64.getDecoder().decode(cusidn));

		model.addAttribute("next", next);
		model.addAttribute("CUSIDN", cusidn);
		
		return target;
	}
}
