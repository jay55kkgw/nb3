package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N951_REST_RQ extends BaseRestBean_TW implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1065505469195494796L;
	
	private String CUSIDN;
	private String UID;
	private String PINNEW;
	//修改 Privacy Violation
	private String CMPW;
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	
	public String getUID() {
		return UID;
	}
	
	public void setUID(String uID) {
		UID = uID;
	}

	public String getPINNEW() {
		return PINNEW;
	}

	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}

	public String getCMPW() {
		return CMPW;
	}

	public void setCMPW(String cMPW) {
		CMPW = cMPW;
	}
	
}
