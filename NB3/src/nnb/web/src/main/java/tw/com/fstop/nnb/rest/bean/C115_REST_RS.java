package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

/**
 * C115電文RS
 */
public class C115_REST_RS extends BaseRestBean implements Serializable{
	

	private static final long serialVersionUID = 7604437896032300437L;
	
	private String TXNDIID ;
	private String SNID ;
	private String RECNO ;
	private String NEXT ;
	private String TXNSIID ;
	private String TXNIDT ;
	private String SSUP ;
	private String ALTERTYPE ;
	private String FILLER_X3 ;
	private String SYNCCHK ;
	private String ACF ;
	private String CMRECNUM ;
	private String MSGTYPE ;
	private String TIME ;
	private String SYSTRACE ;
	private String LOOPINFO ;
	private String CUSNAME ;
	private String DATE ;
	private String BITMAPCFG ;
	private String TOTRECNO ;
	private String CMQTIME ;
	private String PROCCODE ;
	private String CUSIDN ;
	private String RESPCOD ;
	private String SHWD;//視窗註記 2020/07/02新增
	
	private LinkedList<C115_REST_RSDATA> REC;
	
	public String getTXNDIID() {
		return TXNDIID;
	}
	public void setTXNDIID(String tXNDIID) {
		TXNDIID = tXNDIID;
	}
	public String getSNID() {
		return SNID;
	}
	public void setSNID(String sNID) {
		SNID = sNID;
	}
	public String getRECNO() {
		return RECNO;
	}
	public void setRECNO(String rECNO) {
		RECNO = rECNO;
	}
	public String getNEXT() {
		return NEXT;
	}
	public void setNEXT(String nEXT) {
		NEXT = nEXT;
	}
	public String getTXNSIID() {
		return TXNSIID;
	}
	public void setTXNSIID(String tXNSIID) {
		TXNSIID = tXNSIID;
	}
	public String getTXNIDT() {
		return TXNIDT;
	}
	public void setTXNIDT(String tXNIDT) {
		TXNIDT = tXNIDT;
	}
	public String getSSUP() {
		return SSUP;
	}
	public void setSSUP(String sSUP) {
		SSUP = sSUP;
	}
	public String getALTERTYPE() {
		return ALTERTYPE;
	}
	public void setALTERTYPE(String aLTERTYPE) {
		ALTERTYPE = aLTERTYPE;
	}
	public String getFILLER_X3() {
		return FILLER_X3;
	}
	public void setFILLER_X3(String fILLER_X3) {
		FILLER_X3 = fILLER_X3;
	}
	public String getSYNCCHK() {
		return SYNCCHK;
	}
	public void setSYNCCHK(String sYNCCHK) {
		SYNCCHK = sYNCCHK;
	}
	public String getACF() {
		return ACF;
	}
	public void setACF(String aCF) {
		ACF = aCF;
	}
	public String getCMRECNUM() {
		return CMRECNUM;
	}
	public void setCMRECNUM(String cMRECNUM) {
		CMRECNUM = cMRECNUM;
	}
	public String getMSGTYPE() {
		return MSGTYPE;
	}
	public void setMSGTYPE(String mSGTYPE) {
		MSGTYPE = mSGTYPE;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getSYSTRACE() {
		return SYSTRACE;
	}
	public void setSYSTRACE(String sYSTRACE) {
		SYSTRACE = sYSTRACE;
	}
	public String getLOOPINFO() {
		return LOOPINFO;
	}
	public void setLOOPINFO(String lOOPINFO) {
		LOOPINFO = lOOPINFO;
	}
	public String getCUSNAME() {
		return CUSNAME;
	}
	public void setCUSNAME(String cUSNAME) {
		CUSNAME = cUSNAME;
	}
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getBITMAPCFG() {
		return BITMAPCFG;
	}
	public void setBITMAPCFG(String bITMAPCFG) {
		BITMAPCFG = bITMAPCFG;
	}
	public String getTOTRECNO() {
		return TOTRECNO;
	}
	public void setTOTRECNO(String tOTRECNO) {
		TOTRECNO = tOTRECNO;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getPROCCODE() {
		return PROCCODE;
	}
	public void setPROCCODE(String pROCCODE) {
		PROCCODE = pROCCODE;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getRESPCOD() {
		return RESPCOD;
	}
	public void setRESPCOD(String rESPCOD) {
		RESPCOD = rESPCOD;
	}
	public LinkedList<C115_REST_RSDATA> getREC() {
		return REC;
	}
	public void setREC(LinkedList<C115_REST_RSDATA> rEC) {
		REC = rEC;
	}
	public String getSHWD() {
		return SHWD;
	}
	public void setSHWD(String sHWD) {
		SHWD = sHWD;
	}
	
}