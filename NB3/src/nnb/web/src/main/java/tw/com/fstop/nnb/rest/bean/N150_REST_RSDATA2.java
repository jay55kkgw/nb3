package tw.com.fstop.nnb.rest.bean;

public class N150_REST_RSDATA2
{

	private String ACN;// 帳號

	private String LSTLTD; // 異動日

	private String MEMO; // 摘要

	private String CODDB; // 支出

	private String CODCR; // 收入

	private String BAL; // 餘額

	private String TRNBRH; // 收付行

	private String DATA;// 特殊排版格式

	private String FORMAT;// 排版格式

	private String DTA16;//補充資料

	private String ABEND;//結束代碼

	public String getACN()
	{
		return ACN;
	}

	public void setACN(String aCN)
	{
		ACN = aCN;
	}

	public String getLSTLTD()
	{
		return LSTLTD;
	}

	public void setLSTLTD(String lSTLTD)
	{
		LSTLTD = lSTLTD;
	}

	public String getMEMO()
	{
		return MEMO;
	}

	public void setMEMO(String mEMO)
	{
		MEMO = mEMO;
	}

	public String getCODDB()
	{
		return CODDB;
	}

	public void setCODDB(String cODDB)
	{
		CODDB = cODDB;
	}

	public String getCODCR()
	{
		return CODCR;
	}

	public void setCODCR(String cODCR)
	{
		CODCR = cODCR;
	}

	public String getBAL()
	{
		return BAL;
	}

	public void setBAL(String bAL)
	{
		BAL = bAL;
	}

	public String getTRNBRH()
	{
		return TRNBRH;
	}

	public void setTRNBRH(String tRNBRH)
	{
		TRNBRH = tRNBRH;
	}

	public String getDATA()
	{
		return DATA;
	}

	public void setDATA(String dATA)
	{
		DATA = dATA;
	}

	public String getFORMAT()
	{
		return FORMAT;
	}

	public void setFORMAT(String fORMAT)
	{
		FORMAT = fORMAT;
	}

	public String getDTA16()
	{
		return DTA16;
	}

	public void setDTA16(String dTA16)
	{
		DTA16 = dTA16;
	}

	public String getABEND()
	{
		return ABEND;
	}

	public void setABEND(String aBEND)
	{
		ABEND = aBEND;
	}
	
}
