package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N961_REST_RS extends BaseRestBean implements Serializable  {
	

    /**
	 * 
	 */
	private static final long serialVersionUID = 4696926569277536539L;

	private String MSGCODE;//回應代碼
    
    private String CNT;//筆數
    
  	private LinkedList<N961_REST_RSDATA> REC; //電文內陣列

	public String getMSGCODE() {
		return MSGCODE;
	}

	public void setMSGCODE(String mSGCODE) {
		MSGCODE = mSGCODE;
	}

	public String getCNT() {
		return CNT;
	}

	public void setCNT(String cNT) {
		CNT = cNT;
	}

	public LinkedList<N961_REST_RSDATA> getREC() {
		return REC;
	}

	public void setREC(LinkedList<N961_REST_RSDATA> rEC) {
		REC = rEC;
	}

}
