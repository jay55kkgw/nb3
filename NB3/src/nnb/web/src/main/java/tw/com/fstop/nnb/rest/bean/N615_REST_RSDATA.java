package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N615_REST_RSDATA implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -798621962586225950L;
	
	
	String LSTLTD;
	String AMTTRN;
	String TRNBRH;
	String DATA25;
	String ECMARK;
	String CODDBCR;
	String TRNSRC;
	String ACN;
	String LSTIME;
	String MEMO;
	String MEMO_C;
	
	public String getLSTLTD() {
		return LSTLTD;
	}
	public void setLSTLTD(String lSTLTD) {
		LSTLTD = lSTLTD;
	}
	public String getAMTTRN() {
		return AMTTRN;
	}
	public void setAMTTRN(String aMTTRN) {
		AMTTRN = aMTTRN;
	}
	public String getTRNBRH() {
		return TRNBRH;
	}
	public void setTRNBRH(String tRNBRH) {
		TRNBRH = tRNBRH;
	}
	public String getDATA25() {
		return DATA25;
	}
	public void setDATA25(String dATA25) {
		DATA25 = dATA25;
	}
	public String getECMARK() {
		return ECMARK;
	}
	public void setECMARK(String eCMARK) {
		ECMARK = eCMARK;
	}
	public String getCODDBCR() {
		return CODDBCR;
	}
	public void setCODDBCR(String cODDBCR) {
		CODDBCR = cODDBCR;
	}
	public String getTRNSRC() {
		return TRNSRC;
	}
	public void setTRNSRC(String tRNSRC) {
		TRNSRC = tRNSRC;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getLSTIME() {
		return LSTIME;
	}
	public void setLSTIME(String lSTIME) {
		LSTIME = lSTIME;
	}
	public String getMEMO() {
		return MEMO;
	}
	public void setMEMO(String mEMO) {
		MEMO = mEMO;
	}
	public String getMEMO_C() {
		return MEMO_C;
	}
	public void setMEMO_C(String mEMO_C) {
		MEMO_C = mEMO_C;
	}
	
	


}
