package tw.com.fstop.nnb.spring.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.service.Send_Mail_Service;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;

@SessionAttributes({ SessionUtil.CUSIDN, SessionUtil.DPMYEMAIL})
@Controller
@RequestMapping(value="/SEND/MAIL")
public class Send_Mail_Controller {
	
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private Send_Mail_Service send_mail_service;

	
	// 取得TXNUSER用戶的mail
	@RequestMapping(value = "/myemail_aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult myemail_aj(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("myemail_aj...");
		BaseResult bs = null;
		String cusidn = "";
		
		try {
			bs = new BaseResult();
			
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			
			cusidn = okMap.get("cusidn").toUpperCase();
			log.trace(ESAPIUtil.vaildLog("myemail_aj.cusidn: " + cusidn));
				
			// TXNUSER
			bs = send_mail_service.getTxnUser(cusidn);
			log.trace(ESAPIUtil.vaildLog("myemail_aj.bs: " + CodeUtil.toJson(bs)));
				
		} catch (Exception e) {
			log.error("",e);
		}
		return bs;
	}
	
	// 取得TXNUSER用戶的mail--因資安風險改為在後端登入成功時即刻發送mail通知
//	@RequestMapping(value = "/sendmail_aj", produces = {"application/json;charset=UTF-8"})
//	public @ResponseBody BaseResult sendmail_aj(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
//		log.trace("sendmail_aj...");
//		BaseResult bs = null;
//		BaseResult mail_bs = null;
//		
//		
//		try {
//			bs = new BaseResult();
//			
//			// 解決Trust Boundary Violation
//			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//			
//			// 使用者統編
//			String cusidn = okMap.get("cusidn").toUpperCase();
//			if("INDEX".equals(cusidn)) {
//				cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
//				// session有無CUSIDN
//				if( !"".equals(cusidn) && cusidn != null ) {
//					cusidn = new String(Base64.getDecoder().decode(cusidn));
//				}
//			}
//			log.debug(ESAPIUtil.vaildLog("myemail_aj.cusidn: " + cusidn));
//			
//			// 信件類型
//			String type = okMap.get("type");
//			log.trace(ESAPIUtil.vaildLog("myemail_aj.type: " + type));
//			
//			// 信件結果
//			String result = okMap.get("result");
//			log.trace(ESAPIUtil.vaildLog("myemail_aj.result: " + result));
//			
//			
//			// TXNUSER
//			bs = send_mail_service.getTxnUser(cusidn);
//			log.trace(ESAPIUtil.vaildLog("myemail_aj.bs: " + CodeUtil.toJson(bs)));
//
//			
//			// 寄信
//			String dpmyemail = String.valueOf(((Map<String,Object>) bs.getData()).get("DPMYEMAIL"));
//			if( dpmyemail!=null && type!=null && result!=null) {
//				List<String> Receivers = new ArrayList<String>();
//				Receivers.add(dpmyemail);
//				mail_bs = send_mail_service.SendMail(cusidn, type, Boolean.valueOf(result), Receivers);
//			}
//			
//		} catch (Exception e) {
//			log.error("",e);
//		}
//		return mail_bs;
//	}
	
	
}
