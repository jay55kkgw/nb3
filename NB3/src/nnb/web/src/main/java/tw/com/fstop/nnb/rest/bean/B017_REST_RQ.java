package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class B017_REST_RQ extends BaseRestBean_FUND implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4312904457439697734L;
	
	private String CUSIDN ;
	private String BRHCOD ;
	private String IP ;
	
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getBRHCOD() {
		return BRHCOD;
	}
	public void setBRHCOD(String bRHCOD) {
		BRHCOD = bRHCOD;
	}
	public String getIP() {
		return IP;
	}
	public void setIP(String iP) {
		IP = iP;
	}
}
