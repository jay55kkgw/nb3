package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.Map;

/**
 * 取消客戶綁定
 * 
 * @author Vincenthuang
 *
 */
public class MtpUnbind_REST_RQ extends BaseRestBean_QR implements Serializable {

	

	//=========其餘參數===========
	private String ADOPID = "MtpUnbind";//紀錄TXNLOG用

	
	//=========API參數===========
	private String mobilephone;			//手機門號
	private String txacn;				//綁定轉入帳號
	private String bankcode;			//銀行代碼
	private String txselect;			//取消範圍  A:取消自行/跨行綁定  B:僅取消跨行綁定(跨行即為預設綁定)
	private String usermail;


	
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	
	public String getTxselect() {
		return txselect;
	}
	public void setTxselect(String txselect) {
		this.txselect = txselect;
	}
	public String getMobilephone() {
		return mobilephone;
	}
	public void setMobilephone(String mobilephone) {
		this.mobilephone = mobilephone;
	}
	public String getTxacn() {
		return txacn;
	}
	public void setTxacn(String txacn) {
		this.txacn = txacn;
	}
	public String getBankcode() {
		return bankcode;
	}
	public void setBankcode(String bankcode) {
		this.bankcode = bankcode;
	}
	public String getUsermail() {
		return usermail;
	}
	public void setUsermail(String usermail) {
		this.usermail = usermail;
	}
}
