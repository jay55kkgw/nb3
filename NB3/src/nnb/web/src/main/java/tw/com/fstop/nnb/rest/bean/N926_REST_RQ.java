package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N926_REST_RQ  extends BaseRestBean_GOLD implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2584522917016141197L;

	private String CUSIDN;
	private String TYPE;
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getTYPE() {
		return TYPE;
	}
	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}
}
