package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.google.gson.annotations.SerializedName;

public class F001R_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6217395999626186523L;
	
	
	@SerializedName(value = "PAYDATE")
	private String PAYDATE; // 轉帳日期
	
	@SerializedName(value = "CUSTACC")
	private String CUSTACC; // 轉出帳號
	
	@SerializedName(value = "str_Curr")
	private String str_Curr; // 幣別
	
	@SerializedName(value = "str_OutAmt")
	private String str_OutAmt; // 轉出金額
	
	@SerializedName(value = "BENACC")
	private String BENACC; // 轉入帳號
	
	@SerializedName(value = "str_InAmt")
	private String str_InAmt; // 轉入金額
	
	@SerializedName(value = "RATE")
	private String RATE; // 匯率
	
	
	@Override
	public Map<String, String> coverMap(){
		Map<String, String> result = new LinkedHashMap<String, String>();
		
		result.put("交易名稱", "買賣外幣/約定轉帳");
		result.put("交易類型", "即時");
		
		if (StringUtils.isNotBlank(this.PAYDATE) && !"NaN".equals(this.PAYDATE)) result.put("轉帳日期", this.PAYDATE);
		if (StringUtils.isNotBlank(this.CUSTACC) && !"NaN".equals(this.CUSTACC)) result.put("轉出帳號", this.CUSTACC);
		if (StringUtils.isNotBlank(this.str_OutAmt) && !"NaN".equals(this.str_OutAmt)) result.put("轉出金額", this.str_Curr + this.str_OutAmt + "元");
		if (StringUtils.isNotBlank(this.BENACC) && !"NaN".equals(this.BENACC)) result.put("轉入帳號", this.BENACC);
		if (StringUtils.isNotBlank(this.str_InAmt) && !"NaN".equals(this.str_InAmt)) result.put("轉入金額", this.str_Curr + this.str_InAmt + "元");
		if (StringUtils.isNotBlank(this.RATE) && !"NaN".equals(this.RATE)) result.put("匯率", this.RATE);
		return result;

	}


	public String getPAYDATE() {
		return PAYDATE;
	}


	public void setPAYDATE(String pAYDATE) {
		PAYDATE = pAYDATE;
	}


	public String getCUSTACC() {
		return CUSTACC;
	}


	public void setCUSTACC(String cUSTACC) {
		CUSTACC = cUSTACC;
	}


	public String getStr_Curr() {
		return str_Curr;
	}


	public void setStr_Curr(String str_Curr) {
		this.str_Curr = str_Curr;
	}


	public String getStr_OutAmt() {
		return str_OutAmt;
	}


	public void setStr_OutAmt(String str_OutAmt) {
		this.str_OutAmt = str_OutAmt;
	}


	public String getBENACC() {
		return BENACC;
	}


	public void setBENACC(String bENACC) {
		BENACC = bENACC;
	}


	public String getStr_InAmt() {
		return str_InAmt;
	}


	public void setStr_InAmt(String str_InAmt) {
		this.str_InAmt = str_InAmt;
	}

	
}
