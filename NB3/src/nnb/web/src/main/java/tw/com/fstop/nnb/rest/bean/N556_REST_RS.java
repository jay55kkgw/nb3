package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N556_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7546780563480250808L;
	
	String RLCOS; 		//信用狀餘額
	String RBENNAME;	//受益人
	String RTXTENOR;	//買方負擔利息天數
	String RSHPDATE;	//最後裝船日
	String RMARGOS;		//保證金餘額
	String RADVNM;		//通知銀行名稱
	String RAMDNO;		//修改次數
	String RTENOR;		//賣方負擔利息天數
	String CMRECNUM;	//查詢筆數
	String REC_NO;		//筆數
	String RADVBK;		//通知銀行代號
	String RLCCCY;		//幣別
	String RLCNO;		//信用狀號碼
	String RALLOW;		//金額上下寬容範圍±
	String RORIGAMT;	//開狀金額
	String RMAXDRAW;	//可到單金額
	String CMQTIME;		//查詢時間
	String RMARK;		//備註
	String REXPDATE;	//信用狀到期日
	String ROPENDAT;	//開狀日期
	String __OCCURS;
	
	public String getRLCOS() {
		return RLCOS;
	}
	public void setRLCOS(String rLCOS) {
		RLCOS = rLCOS;
	}
	public String getRBENNAME() {
		return RBENNAME;
	}
	public void setRBENNAME(String rBENNAME) {
		RBENNAME = rBENNAME;
	}
	public String getRSHPDATE() {
		return RSHPDATE;
	}
	public void setRSHPDATE(String rSHPDATE) {
		RSHPDATE = rSHPDATE;
	}
	public String getRMARGOS() {
		return RMARGOS;
	}
	public void setRMARGOS(String rMARGOS) {
		RMARGOS = rMARGOS;
	}
	public String getRADVNM() {
		return RADVNM;
	}
	public void setRADVNM(String rADVNM) {
		RADVNM = rADVNM;
	}
	public String getRAMDNO() {
		return RAMDNO;
	}
	public void setRAMDNO(String rAMDNO) {
		RAMDNO = rAMDNO;
	}
	public String getRTENOR() {
		return RTENOR;
	}
	public void setRTENOR(String rTENOR) {
		RTENOR = rTENOR;
	}
	public String getCMRECNUM() {
		return CMRECNUM;
	}
	public void setCMRECNUM(String cMRECNUM) {
		CMRECNUM = cMRECNUM;
	}
	public String getREC_NO() {
		return REC_NO;
	}
	public void setREC_NO(String rEC_NO) {
		REC_NO = rEC_NO;
	}
	public String getRADVBK() {
		return RADVBK;
	}
	public void setRADVBK(String rADVBK) {
		RADVBK = rADVBK;
	}
	public String getRLCCCY() {
		return RLCCCY;
	}
	public void setRLCCCY(String rLCCCY) {
		RLCCCY = rLCCCY;
	}
	public String getRLCNO() {
		return RLCNO;
	}
	public void setRLCNO(String rLCNO) {
		RLCNO = rLCNO;
	}
	public String getRALLOW() {
		return RALLOW;
	}
	public void setRALLOW(String rALLOW) {
		RALLOW = rALLOW;
	}
	public String getRORIGAMT() {
		return RORIGAMT;
	}
	public void setRORIGAMT(String rORIGAMT) {
		RORIGAMT = rORIGAMT;
	}
	public String getRMAXDRAW() {
		return RMAXDRAW;
	}
	public void setRMAXDRAW(String rMAXDRAW) {
		RMAXDRAW = rMAXDRAW;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getRMARK() {
		return RMARK;
	}
	public void setRMARK(String rMARK) {
		RMARK = rMARK;
	}
	public String getREXPDATE() {
		return REXPDATE;
	}
	public void setREXPDATE(String rEXPDATE) {
		REXPDATE = rEXPDATE;
	}
	public String getROPENDAT() {
		return ROPENDAT;
	}
	public void setROPENDAT(String rOPENDAT) {
		ROPENDAT = rOPENDAT;
	}
	public String get__OCCURS() {
		return __OCCURS;
	}
	public void set__OCCURS(String __OCCURS) {
		this.__OCCURS = __OCCURS;
	}
	
	
}
