package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N106_REST_RSDATA implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6017632509557376285L;

	private String BRHCOD;
	private String BRHNAME;
	private String BUSIDN;
	private String SEQCLM;
	private String FORM;
	private String TAMPAY;
	private String AMTTAX;
	private String NETPAY;
	
	
	public String getBRHCOD() {
		return BRHCOD;
	}
	public String getBRHNAME() {
		return BRHNAME;
	}
	public String getBUSIDN() {
		return BUSIDN;
	}
	public String getSEQCLM() {
		return SEQCLM;
	}
	public String getFORM() {
		return FORM;
	}
	public String getTAMPAY() {
		return TAMPAY;
	}
	public String getAMTTAX() {
		return AMTTAX;
	}
	public String getNETPAY() {
		return NETPAY;
	}
	public void setBRHCOD(String bRHCOD) {
		BRHCOD = bRHCOD;
	}
	public void setBRHNAME(String bRHNAME) {
		BRHNAME = bRHNAME;
	}
	public void setBUSIDN(String bUSIDN) {
		BUSIDN = bUSIDN;
	}
	public void setSEQCLM(String sEQCLM) {
		SEQCLM = sEQCLM;
	}
	public void setFORM(String fORM) {
		FORM = fORM;
	}
	public void setTAMPAY(String tAMPAY) {
		TAMPAY = tAMPAY;
	}
	public void setAMTTAX(String aMTTAX) {
		AMTTAX = aMTTAX;
	}
	public void setNETPAY(String nETPAY) {
		NETPAY = nETPAY;
	}
	
}
