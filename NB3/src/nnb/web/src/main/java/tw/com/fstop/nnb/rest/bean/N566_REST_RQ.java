package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N566_REST_RQ extends BaseRestBean_FX implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6131337349079586169L;
	String	CMSDATE	;
	String	CUSIDN	;
	String	CMEDATE ;
	String	FDATE ;
	String	TDATE ;
	public String getCMSDATE() {
		return CMSDATE;
	}
	public void setCMSDATE(String cMSDATE) {
		CMSDATE = cMSDATE;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getCMEDATE() {
		return CMEDATE;
	}
	public void setCMEDATE(String cMEDATE) {
		CMEDATE = cMEDATE;
	}
	public String getFDATE() {
		return FDATE;
	}
	public void setFDATE(String fDATE) {
		FDATE = fDATE;
	}
	public String getTDATE() {
		return TDATE;
	}
	public void setTDATE(String tDATE) {
		TDATE = tDATE;
	}
	
	
	
}
