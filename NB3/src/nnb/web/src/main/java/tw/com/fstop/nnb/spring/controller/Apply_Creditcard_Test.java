package tw.com.fstop.nnb.spring.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.util.SessionUtil;

@SessionAttributes({ SessionUtil.CUSIDN, SessionUtil.USERID, SessionUtil.CONFIRM_LOCALE_DATA,SessionUtil.TRANSFER_DATA,SessionUtil.TRANSFER_CONFIRM_TOKEN,
	SessionUtil.TRANSFER_RESULT_FINSH_TOKEN,SessionUtil.RESULT_LOCALE_DATA,SessionUtil.DPMYEMAIL,SessionUtil.TRANSFER_RESULT_TOKEN,SessionUtil.PRINT_DATALISTMAP_DATA })
@Controller
@RequestMapping(value = "/test/apply_creditcard")
public class Apply_Creditcard_Test {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@RequestMapping(value = "/APPLY_CREDITCARD_TEST")
	public String input_txn(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		
		return "/apply_creditcard_p4_test";
	}
	
}