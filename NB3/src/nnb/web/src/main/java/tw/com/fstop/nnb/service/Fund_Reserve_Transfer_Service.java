package tw.com.fstop.nnb.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import fstop.orm.po.ADMCURRENCY;
import fstop.orm.po.TXNFUNDDATA;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.StrUtils;
import tw.com.fstop.web.util.WebUtil;

/**
 * 基金預約轉換交易的Service
 */
@Service
public class Fund_Reserve_Transfer_Service extends Base_Service{
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private I18n i18n;
	@Autowired
	private Fund_Transfer_Service fund_transfer_service;
	
	/**
	 * 準備前往基金預約轉換選擇頁的資料
	 */
	public BaseResult prepareFundReserveTransferSelectData(Map<String,String> requestParam){
		BaseResult bs = new BaseResult();
		try {
			String CUSIDN = requestParam.get("CUSIDN");
			String hiddenCUSIDN = WebUtil.hideID(CUSIDN);
			bs.addData("hiddenCUSIDN",hiddenCUSIDN);
			String NAME = requestParam.get("NAME");
			String hiddenNAME = WebUtil.hideusername1Convert(NAME);
			bs.addData("hiddenNAME",hiddenNAME);
			String CDNO = requestParam.get("CDNO");
			bs.addData("CDNO",CDNO);
			String hiddenCDNO = WebUtil.hideAccount(CDNO);
			bs.addData("hiddenCDNO",hiddenCDNO);
			
			String BRHCOD = requestParam.get("BRHCOD");
			log.debug(ESAPIUtil.vaildLog("BRHCOD={}"+BRHCOD));
			bs.addData("BRHCOD",BRHCOD);
			
			String FDINVTYPE = requestParam.get("FDINVTYPE");
			log.debug(ESAPIUtil.vaildLog("FDINVTYPE={}"+FDINVTYPE));
			bs.addData("FDINVTYPE",FDINVTYPE);
			
			String FDINVTYPEChinese;
			if("1".equals(FDINVTYPE)){
				//積極型
				FDINVTYPEChinese = i18n.getMsg("LB.D0945");
			}
			else if("2".equals(FDINVTYPE)){
				//穩健型
				FDINVTYPEChinese = i18n.getMsg("LB.X1766");
			}
			else{
				//保守型
				FDINVTYPEChinese = i18n.getMsg("LB.X1767");
			}
			log.debug("FDINVTYPEChinese={}",FDINVTYPEChinese);
			bs.addData("FDINVTYPEChinese",FDINVTYPEChinese);
			
			String FUNDLNAME = requestParam.get("FUNDLNAME");
			log.debug(ESAPIUtil.vaildLog("FUNDLNAME={}"+FUNDLNAME));
			bs.addData("FUNDLNAME",FUNDLNAME);
			
			String CRY = requestParam.get("CRY");
			log.debug(ESAPIUtil.vaildLog("CRY={}"+CRY));
			bs.addData("CRY",CRY);
			ADMCURRENCY admCurrency = fund_transfer_service.getCRYData(CRY);
			
			if(admCurrency != null){
				String ADCCYNAME = admCurrency.getADCCYNAME();
				log.debug(ESAPIUtil.vaildLog("ADCCYNAME={}"+ADCCYNAME));
				bs.addData("ADCCYNAME",ADCCYNAME);
				bs.addData("ADCCYENGNAME", admCurrency.getADCCYENGNAME());
				bs.addData("ADCCYCHSNAME", admCurrency.getADCCYCHSNAME());
			}
			
			String UNIT = requestParam.get("UNIT");
			log.debug(ESAPIUtil.vaildLog("UNIT={}"+UNIT));
			UNIT = fund_transfer_service.formatNumberString(UNIT,4);
			log.debug(ESAPIUtil.vaildLog("UNIT={}"+UNIT));
			bs.addData("UNIT",UNIT);
			
			String RISK7 = requestParam.get("RISK7");
			log.debug(ESAPIUtil.vaildLog("RISK7={}"+RISK7));
			bs.addData("RISK7",RISK7);
			
			String GETLTD7 = requestParam.get("GETLTD7");
			log.debug(ESAPIUtil.vaildLog("GETLTD7={}"+GETLTD7));
			bs.addData("GETLTD7",GETLTD7);
			
			String SHORTTRADE = requestParam.get("SHORTTRADE");
			log.debug(ESAPIUtil.vaildLog("SHORTTRADE={}"+SHORTTRADE));
			
			int shortDays = 0;
			
			if(!"00000000".equals(SHORTTRADE)){
				String systemDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
				log.debug("systemDate={}",systemDate);
				
				int year = Integer.parseInt(SHORTTRADE.substring(0,4)) + 1911;
				String shortMonth = SHORTTRADE.substring(4,6);
				String shortDay = SHORTTRADE.substring(6);
				SHORTTRADE = String.valueOf(year) + shortMonth + shortDay;
				
				shortDays = fund_transfer_service.caculateDaysBetweenTwoDate(systemDate,SHORTTRADE);
			}
			log.debug(ESAPIUtil.vaildLog("SHORTTRADE={}"+SHORTTRADE));
			bs.addData("SHORTTRADE",SHORTTRADE);
			
			log.debug("shortDays={}",shortDays);
			bs.addData("shortDays",shortDays);
			
			String SHORTTUNIT = requestParam.get("SHORTTUNIT");
			log.debug(ESAPIUtil.vaildLog("SHORTTUNIT={}"+SHORTTUNIT));
			bs.addData("SHORTTUNIT",SHORTTUNIT);
			
			String BILLSENDMODE = requestParam.get("BILLSENDMODE");
			log.debug(ESAPIUtil.vaildLog("BILLSENDMODE={}"+BILLSENDMODE));
			bs.addData("BILLSENDMODE",BILLSENDMODE);
			
			String OFUNDAMT = requestParam.get("OFUNDAMT");
			log.debug(ESAPIUtil.vaildLog("OFUNDAMT={}"+OFUNDAMT));
			
			String OFUNDAMTDotTwo = OFUNDAMT.replaceAll("\\.","");
			log.debug("OFUNDAMTDotTwo={}",OFUNDAMTDotTwo);
			
			OFUNDAMTDotTwo = fund_transfer_service.formatNumberString(OFUNDAMTDotTwo,2);
			log.debug("OFUNDAMTDotTwo={}",OFUNDAMTDotTwo);
			bs.addData("OFUNDAMTDotTwo",OFUNDAMTDotTwo);
			
			String BILLSENDMODEChinese;
			//全部
			if("1".equals(BILLSENDMODE)){
				BILLSENDMODEChinese = i18n.getMsg("LB.All");
				bs.addData("BILLSENDMODEChinese",BILLSENDMODEChinese);
				
				if (OFUNDAMT.indexOf(".") != -1){
					OFUNDAMT = OFUNDAMT.substring(0,OFUNDAMT.indexOf("."));		
				}
				log.debug(ESAPIUtil.vaildLog("OFUNDAMT={}"+OFUNDAMT));
				OFUNDAMT = fund_transfer_service.formatNumberString(OFUNDAMT,0);
				log.debug(ESAPIUtil.vaildLog("OFUNDAMT={}"+OFUNDAMT));
				
				bs.addData("FUNDAMT",OFUNDAMT);
			}
			//部分
			else{
				BILLSENDMODEChinese = i18n.getMsg("LB.X0383");
				bs.addData("BILLSENDMODEChinese",BILLSENDMODEChinese);
			}
			
			String FEE_TYPE = requestParam.get("FEE_TYPE");
			log.debug(ESAPIUtil.vaildLog("FEE_TYPE={}"+FEE_TYPE));
			bs.addData("FEE_TYPE",FEE_TYPE);
			
			bs.setResult(Boolean.TRUE);
		}catch(Exception e) {
			log.debug(e.getMessage());
		}
		return bs;
	}
	/**
	 * 準備前往基金預約轉換手續費頁的資料
	 */
	@SuppressWarnings("unchecked")
	public BaseResult prepareFundReserveTransferFeeExposeData(Map<String,String> requestParam){
		BaseResult bs = null;
		try{
			String FUNDAMT = requestParam.get("FUNDAMT");
			log.debug(ESAPIUtil.vaildLog("FUNDAMT={}"+FUNDAMT));
			
			//將轉換金額的逗號拿掉
			String FUNDAMTNoComma = FUNDAMT.replaceAll(",","");
			log.debug("FUNDAMTNoComma={}",FUNDAMTNoComma);
			requestParam.put("FUNDAMTNoComma",FUNDAMTNoComma);
			
			String UNIT = requestParam.get("UNIT");
			log.debug(ESAPIUtil.vaildLog("UNIT={}"+UNIT));
			//將單位數的逗號拿掉
			String UNITNoComma = UNIT.replaceAll(",","");
			log.debug("UNITNoComma={}",UNITNoComma);
			//將持有基金單位數還原成主機下來的值
			String UNITNoDot = UNITNoComma.replaceAll("\\.","");
			log.debug("UNITNoDot={}",UNITNoDot);
			//補滿12位的長度
			while(UNITNoDot.length() < 12){
				UNITNoDot = "0" + UNITNoDot;
			}
			requestParam.put("UNITNoDot",UNITNoDot);

			bs = new BaseResult();
			bs = C032Public_REST(requestParam);
			
			String TRANSCODE = requestParam.get("TRANSCODE");
			log.debug(ESAPIUtil.vaildLog("TRANSCODE={}"+TRANSCODE));
			bs.addData("TRANSCODE",TRANSCODE);
			
			String CDNO = requestParam.get("CDNO");
			log.debug(ESAPIUtil.vaildLog("CDNO={}"+CDNO));
			bs.addData("CDNO",CDNO);
			
			String INTRANSCODE = requestParam.get("INTRANSCODE");
			log.debug(ESAPIUtil.vaildLog("INTRANSCODE={}"+INTRANSCODE));
			bs.addData("INTRANSCODE",INTRANSCODE);
			
			String BILLSENDMODE = requestParam.get("BILLSENDMODE");
			log.debug(ESAPIUtil.vaildLog("BILLSENDMODE={}"+BILLSENDMODE));
			bs.addData("BILLSENDMODE",BILLSENDMODE);
			
			String RRSK = requestParam.get("RRSK");
			log.debug(ESAPIUtil.vaildLog("RRSK={}"+RRSK));
			bs.addData("RRSK",RRSK);
			
			String RISK7 = requestParam.get("RISK7");
			log.debug(ESAPIUtil.vaildLog("RISK7={}"+RISK7));
			bs.addData("RISK7",RISK7);
			
			String FDINVTYPE = requestParam.get("FDINVTYPE");
			log.debug(ESAPIUtil.vaildLog("FDINVTYPE={}"+FDINVTYPE));
			bs.addData("FDINVTYPE",FDINVTYPE);
			
			String SHORTTRADE = requestParam.get("SHORTTRADE");
			log.debug(ESAPIUtil.vaildLog("SHORTTRADE={}"+SHORTTRADE));
			bs.addData("SHORTTRADE",SHORTTRADE);
			
			String SHORTTUNIT = requestParam.get("SHORTTUNIT");
			log.debug(ESAPIUtil.vaildLog("SHORTTUNIT={}"+SHORTTUNIT));
			bs.addData("SHORTTUNIT",SHORTTUNIT);
			
			String FDAGREEFLAG = requestParam.get("FDAGREEFLAG");
			log.debug(ESAPIUtil.vaildLog("FDAGREEFLAG={}"+FDAGREEFLAG));
			bs.addData("FDAGREEFLAG",FDAGREEFLAG);
			
			String FDNOTICETYPE = requestParam.get("FDNOTICETYPE");
			log.debug(ESAPIUtil.vaildLog("FDNOTICETYPE={}"+FDNOTICETYPE));
			bs.addData("FDNOTICETYPE",FDNOTICETYPE);
			
			String FDPUBLICTYPE = requestParam.get("FDPUBLICTYPE");
			log.debug(ESAPIUtil.vaildLog("FDPUBLICTYPE={}"+FDPUBLICTYPE));
			bs.addData("FDPUBLICTYPE",FDPUBLICTYPE);
			
			String FEE_TYPE = requestParam.get("FEE_TYPE");
			log.debug(ESAPIUtil.vaildLog("FEE_TYPE={}"+FEE_TYPE));
			bs.addData("FEE_TYPE",FEE_TYPE);
			
			if(bs != null && bs.getResult()){
				Map<String,Object> bsData = (Map<String,Object>)bs.getData();
				
				String TRADEDATE = (String)bsData.get("TRADEDATE");
				log.debug("TRADEDATE={}",TRADEDATE);
				bs.addData("TRADEDATE",TRADEDATE);
				
				String FCA1 = (String)bsData.get("FCA1");
				log.debug("FCA1={}",FCA1);
				bs.addData("FCA1",FCA1);
				
				String FCA2 = (String)bsData.get("FCA2");
				log.debug("FCA2={}",FCA2);
				bs.addData("FCA2",FCA2);
				
				String AMT3 = (String)bsData.get("AMT3");
				log.debug("AMT3={}",AMT3);
				bs.addData("AMT3",AMT3);
				
				String AMT5 = (String)bsData.get("AMT5");
				log.debug("AMT5={}",AMT5);
				bs.addData("AMT5",AMT5);
				
				String SSLTXNO = (String)bsData.get("SSLTXNO");
				log.debug("SSLTXNO={}",SSLTXNO);
				bs.addData("SSLTXNO",SSLTXNO);
				
				String FUNCUR = (String)bsData.get("FUNCUR");
				log.debug("FUNCUR={}",FUNCUR);
				bs.addData("FUNCUR",FUNCUR);
				
				FUNDAMT = (String)bsData.get("FUNDAMT");
				log.debug("FUNDAMT={}",FUNDAMT);
				bs.addData("FUNDAMT",FUNDAMT);
				
				String FUNDAMTFormat = fund_transfer_service.formatNumberString(FUNDAMT.replaceAll("\\.",""),2);
				log.debug("FUNDAMTFormat={}",FUNDAMTFormat);
				bs.addData("FUNDAMTFormat",FUNDAMTFormat);
				
				//將轉換金額的逗號拿掉，保留小數點
				FUNDAMTNoComma = FUNDAMTFormat.replaceAll(",","");
				log.debug("FUNDAMTNoComma={}",FUNDAMTNoComma);
				bs.addData("FUNDAMTNoComma",FUNDAMTNoComma);
				
				UNIT = (String)bsData.get("UNIT");
				log.debug("UNIT={}",UNIT);
				bs.addData("UNIT",UNIT);
				
				String UNITDot = fund_transfer_service.formatNumberString(UNIT,4);
				log.debug("UNITDot={}",UNITDot);
				bs.addData("UNITDot",UNITDot);
				
				Integer UNITInteger = Integer.valueOf(UNIT);
				log.debug("UNITInteger={}",UNITInteger);
				bs.addData("UNITInteger",UNITInteger);
				
				Map<String,String> fundFeeExposeTextMap = fund_transfer_service.getFundFeeExposeInfo(bsData,"C032");
				bs.addData("fundFeeExposeTextMap",fundFeeExposeTextMap);
				
				String TXID = requestParam.get("TXID");
				log.debug(ESAPIUtil.vaildLog("TXID={}"+TXID));
				bs.addData("TXID",TXID);
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("prepareFundReserveTransferFeeExposeData error >> {}",e);
		}
		return bs;
	}
	/**
	 * 準備前往基金預約轉換確認頁的資料
	 */
	public BaseResult prepareFundReserveTransferConfirmData(Map<String,String> requestParam, String cusidn){
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = fund_transfer_service.getN922Data(cusidn);
			if (bs != null && bs.getResult()) {
				
				Map<String,Object> bsData = (Map<String,Object>)bs.getData();
				//新台幣扣帳帳號
				String ACN1 = (String)bsData.get("ACN1");
				log.debug("ACN1={}",ACN1);
				bs.addData("ACN1",ACN1);
				bs.addData("OUTACN",ACN1);
				//外幣扣帳帳號
				String ACN2 = (String)bsData.get("ACN2");
				log.debug("ACN2={}",ACN2);
				bs.addData("ACN2",ACN2);
				//新台幣入帳帳號
				String ACN3 = (String)bsData.get("ACN3");
				log.debug("ACN3={}",ACN3);
				bs.addData("ACN3",ACN3);
				//外幣入帳帳號
				String ACN4 = (String)bsData.get("ACN4");
				log.debug("ACN4={}",ACN4);
				bs.addData("ACN4",ACN4);
				
//				String NAME = (String)bsData.get("NAME");
				//20210617修正  若有CUSNAME取CUSNAME 無則為NAME , 不然會是空的 //N922原住民姓名修改由NAME->CUSNAME  
				String NAME = "";
				if(StrUtils.isNotEmpty((String)bsData.get("CUSNAME"))) {
					NAME = (String)bsData.get("CUSNAME");
				}else {
					NAME = (String)bsData.get("NAME");
				}
				
				log.debug("NAME={}",NAME);
				bs.addData("NAME",NAME);
				String hiddenNAME = WebUtil.hideusername1Convert(NAME);
				bs.addData("hiddenNAME",hiddenNAME);
				
				String hiddenCUSIDN = WebUtil.hideID(cusidn);
				bs.addData("hiddenCUSIDN",hiddenCUSIDN);
			
				String TRANSCODE = requestParam.get("TRANSCODE");
				log.debug(ESAPIUtil.vaildLog("TRANSCODE={}"+TRANSCODE));
				bs.addData("TRANSCODE",TRANSCODE);
				
				String CDNO = requestParam.get("CDNO");
				log.debug(ESAPIUtil.vaildLog("CDNO={}"+CDNO));
				bs.addData("CDNO",CDNO);
				
				String hiddenCDNO = WebUtil.hideAccount(CDNO);
//				log.debug("hiddenCREDITNO={}",hiddenCREDITNO);
				bs.addData("hiddenCDNO",hiddenCDNO);
				
				String INTRANSCODE = requestParam.get("INTRANSCODE");
				log.debug(ESAPIUtil.vaildLog("INTRANSCODE={}"+INTRANSCODE));
				bs.addData("INTRANSCODE",INTRANSCODE);
				
				String UNIT = requestParam.get("UNIT");
				log.debug(ESAPIUtil.vaildLog("UNIT={}"+UNIT));
				bs.addData("UNIT",UNIT);
				
				String UNITInteger = requestParam.get("UNITInteger");
				log.debug(ESAPIUtil.vaildLog("UNITInteger={}"+UNITInteger));
				bs.addData("UNITInteger",UNITInteger);
				
				String UNITDot = requestParam.get("UNITDot");
				log.debug(ESAPIUtil.vaildLog("UNITDot={}"+UNITDot));
				bs.addData("UNITDot",UNITDot);
				
				String BILLSENDMODE = requestParam.get("BILLSENDMODE");
				log.debug(ESAPIUtil.vaildLog("BILLSENDMODE={}"+BILLSENDMODE));
				bs.addData("BILLSENDMODE",BILLSENDMODE);
				
				String BILLSENDMODEChinese;
				//全部
				if("1".equals(BILLSENDMODE)){
					BILLSENDMODEChinese = i18n.getMsg("LB.All");
					bs.addData("BILLSENDMODEChinese",BILLSENDMODEChinese);
				}
				//部分
				else{
					BILLSENDMODEChinese = i18n.getMsg("LB.X0383");
					bs.addData("BILLSENDMODEChinese",BILLSENDMODEChinese);
				}
				
				String FUNDAMT = requestParam.get("FUNDAMT");
				log.debug(ESAPIUtil.vaildLog("FUNDAMT={}"+FUNDAMT));
				bs.addData("FUNDAMT",FUNDAMT);
				
				String FUNDAMTFormat = requestParam.get("FUNDAMTFormat");
				log.debug(ESAPIUtil.vaildLog("FUNDAMTFormat={}"+FUNDAMTFormat));
				bs.addData("FUNDAMTFormat",FUNDAMTFormat);
				
				String FUNDAMTNoComma = requestParam.get("FUNDAMTNoComma");
				log.debug(ESAPIUtil.vaildLog("FUNDAMTNoComma={}"+FUNDAMTNoComma));
				bs.addData("FUNDAMTNoComma",FUNDAMTNoComma);
				
				String RRSK = requestParam.get("RRSK");
				log.debug(ESAPIUtil.vaildLog("RRSK={}"+RRSK));
				bs.addData("RRSK",RRSK);
				
				String TRADEDATE = requestParam.get("TRADEDATE");
				log.debug(ESAPIUtil.vaildLog("TRADEDATE={}"+TRADEDATE));
				bs.addData("TRADEDATE",TRADEDATE);
				
				String TRADEDATEFormat = TRADEDATE.substring(1,4) + "/" + TRADEDATE.substring(4,6) + "/" + TRADEDATE.substring(6);
				log.debug(ESAPIUtil.vaildLog("TRADEDATEFormat={}"+TRADEDATEFormat));
				bs.addData("TRADEDATEFormat",TRADEDATEFormat);
				
				String FCA1 = requestParam.get("FCA1");
				log.debug(ESAPIUtil.vaildLog("FCA1={}"+FCA1));
				bs.addData("FCA1",FCA1);
				
				String FCA1Format = fund_transfer_service.formatNumberString(FCA1,2);
				log.debug(ESAPIUtil.vaildLog("FCA1Format={}"+FCA1Format));
				bs.addData("FCA1Format",FCA1Format);
				
				String AMT3 = requestParam.get("AMT3");
				log.debug(ESAPIUtil.vaildLog("AMT3={}"+AMT3));
				bs.addData("AMT3",AMT3);
				
				Integer AMT3Integer = Integer.valueOf(AMT3);
				log.debug("AMT3Integer={}",AMT3Integer);
				bs.addData("AMT3Integer",AMT3Integer);
				
				String FCA2 = requestParam.get("FCA2");
				log.debug(ESAPIUtil.vaildLog("FCA2={}"+FCA2));
				bs.addData("FCA2",FCA2);
				
				String FCA2Format = fund_transfer_service.formatNumberString(FCA2,2);
				log.debug(ESAPIUtil.vaildLog("FCA2Format={}"+FCA2Format));
				bs.addData("FCA2Format",FCA2Format);
				
				String AMT5 = requestParam.get("AMT5");
				log.debug(ESAPIUtil.vaildLog("AMT5={}"+AMT5));
				bs.addData("AMT5",AMT5);
		
			    String SSLTXNO = requestParam.get("SSLTXNO");
				log.debug(ESAPIUtil.vaildLog("SSLTXNO={}"+SSLTXNO));
				bs.addData("SSLTXNO",SSLTXNO);
				
				String FUNCUR = requestParam.get("FUNCUR");
				log.debug(ESAPIUtil.vaildLog("FUNCUR={}"+FUNCUR));
				bs.addData("CRY",FUNCUR);
				
				String RISK7 = requestParam.get("RISK7");
				log.debug(ESAPIUtil.vaildLog("RISK7={}"+RISK7));
				bs.addData("RISK7",RISK7);
				bs.addData("PRO",RISK7);
				
				String FDINVTYPE = requestParam.get("FDINVTYPE");
				log.debug(ESAPIUtil.vaildLog("FDINVTYPE={}"+FDINVTYPE));
				bs.addData("FDINVTYPE",FDINVTYPE);
				bs.addData("RSKATT",FDINVTYPE);
				
				String SHORTTRADE = requestParam.get("SHORTTRADE");
				log.debug(ESAPIUtil.vaildLog("SHORTTRADE={}"+SHORTTRADE));
				bs.addData("SHORTTRADE",SHORTTRADE);
				
				String SHORTTUNIT = requestParam.get("SHORTTUNIT");
				log.debug(ESAPIUtil.vaildLog("SHORTTUNIT={}"+SHORTTUNIT));
				bs.addData("SHORTTUNIT",SHORTTUNIT);
				
				String FDAGREEFLAG = requestParam.get("FDAGREEFLAG");
				log.debug(ESAPIUtil.vaildLog("FDAGREEFLAG={}"+FDAGREEFLAG));
				bs.addData("FDAGREEFLAG",FDAGREEFLAG);
				
				String FDNOTICETYPE = requestParam.get("FDNOTICETYPE");
				log.debug(ESAPIUtil.vaildLog("FDNOTICETYPE={}"+FDNOTICETYPE));
				bs.addData("FDNOTICETYPE",FDNOTICETYPE);
				
				String FDPUBLICTYPE = requestParam.get("FDPUBLICTYPE");
				log.debug(ESAPIUtil.vaildLog("FDPUBLICTYPE={}"+FDPUBLICTYPE));
				bs.addData("FDPUBLICTYPE",FDPUBLICTYPE);
				
				String SHWD = requestParam.get("SHWD");
				log.debug(ESAPIUtil.vaildLog("SHWD={}"+SHWD));
				bs.addData("SHWD",SHWD);
				
				String XFLAG = requestParam.get("XFLAG");
				log.debug(ESAPIUtil.vaildLog("XFLAG={}"+XFLAG));
				bs.addData("XFLAG",XFLAG);
				
				String FEE_TYPE = requestParam.get("FEE_TYPE");
				log.debug(ESAPIUtil.vaildLog("FEE_TYPE= >>" + XFLAG));
				bs.addData("FEE_TYPE",FEE_TYPE);
				
				TXNFUNDDATA txnFundData = fund_transfer_service.getFundData(TRANSCODE);
				String fundName = "";
				
				if(txnFundData != null){
					fundName = txnFundData.getFUNDLNAME();
					
					ADMCURRENCY admCurrency = fund_transfer_service.getCRYData(FUNCUR);
					
					if(admCurrency != null){
						String ADCCYNAME = admCurrency.getADCCYNAME();
						log.debug("ADCCYNAME={}",ADCCYNAME);
						bs.addData("ADCCYNAME",ADCCYNAME);
						bs.addData("ADCCYENGNAME", admCurrency.getADCCYENGNAME());
						bs.addData("ADCCYCHSNAME", admCurrency.getADCCYCHSNAME());
					}
				}
				log.debug(ESAPIUtil.vaildLog("fundName={}"+fundName));
				bs.addData("fundName",fundName);
				
				txnFundData = fund_transfer_service.getFundData(INTRANSCODE);
				String inFundName = "";
				
				if(txnFundData != null){
					inFundName = txnFundData.getFUNDLNAME();
				}
				log.debug(ESAPIUtil.vaildLog("inFundName={}"+inFundName));
				bs.addData("inFundName",inFundName);
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("prepareFundReserveTransferConfirmData error >> {}",e);
		}
		return bs;
	}
	/**
	 * 基金預約轉換交易
	 */
	@SuppressWarnings("unchecked")
	public BaseResult processFundReserveTransfer(Map<String,String> requestParam){
		BaseResult bs = null;
		try{
			bs = new BaseResult();
			bs = N373_REST(requestParam);
			if(bs != null && bs.getResult()){
				Map<String,Object> bsData = (Map<String,Object>)bs.getData();
				log.debug("bsData={}",bsData);
				
				String OUTACN = requestParam.get("OUTACN");
				log.debug(ESAPIUtil.vaildLog("OUTACN={}"+OUTACN));
				bs.addData("OUTACN",OUTACN);
				
				String CDNO = (String)bsData.get("CDNO");
				log.debug("CREDITNO={}",CDNO);
				String hiddenCDNO = WebUtil.hideAccount(CDNO);
				log.debug("hiddenCREDITNO={}",hiddenCDNO);
				bs.addData("hiddenCDNO",hiddenCDNO);
				
				String TRANSCODE = (String)bsData.get("TRANSCODE");
				log.debug("TRANSCODE={}",TRANSCODE);
				
				String FUNCUR = (String)bsData.get("FUNCUR");
				log.debug("FUNCUR={}",FUNCUR);
				
				TXNFUNDDATA txnFundData = fund_transfer_service.getFundData(TRANSCODE);
				String fundName = "";
				
				if(txnFundData != null){
					fundName = txnFundData.getFUNDLNAME();
					
					ADMCURRENCY admCurrency = fund_transfer_service.getCRYData(FUNCUR);
					
					if(admCurrency != null){
						String ADCCYNAME = admCurrency.getADCCYNAME();
						log.debug("ADCCYNAME={}",ADCCYNAME);
						bs.addData("ADCCYNAME",ADCCYNAME);
						bs.addData("ADCCYENGNAME", admCurrency.getADCCYENGNAME());
						bs.addData("ADCCYCHSNAME", admCurrency.getADCCYCHSNAME());
					}
				}
				log.debug("fundName={}",fundName);
				bs.addData("fundName",fundName);
				
				String INTRANSCODE = (String)bsData.get("INTRANSCODE");
				log.debug("INTRANSCODE={}",INTRANSCODE);
				
				txnFundData = fund_transfer_service.getFundData(INTRANSCODE);
				String inFundName = "";
				
				if(txnFundData != null){
					inFundName = txnFundData.getFUNDLNAME();
				}
				log.debug("inFundName={}",inFundName);
				bs.addData("inFundName",inFundName);
				
				String BILLSENDMODE = (String)bsData.get("BILLSENDMODE");
				log.debug("BILLSENDMODE={}",BILLSENDMODE);
				
				String BILLSENDMODEChinese;
				//全部
				if("1".equals(BILLSENDMODE)){
					BILLSENDMODEChinese =  i18n.getMsg("LB.All");
					bs.addData("BILLSENDMODEChinese",BILLSENDMODEChinese);
				}
				//部分
				else{
					BILLSENDMODEChinese = i18n.getMsg("LB.X0383");
					bs.addData("BILLSENDMODEChinese",BILLSENDMODEChinese);
				}
				
				String UNIT = (String)bsData.get("UNIT");
				log.debug("UNIT={}",UNIT);
				String UNITFormat = fund_transfer_service.formatNumberString(UNIT,4);
				log.debug("UNITFormat={}",UNITFormat);
				bs.addData("UNITFormat",UNITFormat);
				
				String FUNDAMT = (String)bsData.get("FUNDAMT");
				log.debug("FUNDAMT={}",FUNDAMT);
				String FUNDAMTNoDot = FUNDAMT.replaceAll("\\.","");
				log.debug("FUNDAMTNoDot={}",FUNDAMTNoDot);
				
				String FUNDAMTFormat = fund_transfer_service.formatNumberString(FUNDAMTNoDot,2);
				log.debug("FUNDAMTFormat={}",FUNDAMTFormat);
				bs.addData("FUNDAMTFormat",FUNDAMTFormat);
				
				String AMT3 = (String)bsData.get("AMT3");
				log.debug("AMT3={}",AMT3);
				Integer AMT3Integer = Integer.valueOf(AMT3);
				log.debug("AMT3Integer={}",AMT3Integer);
				bs.addData("AMT3Integer",AMT3Integer);
				
				String FCA2 = (String)bsData.get("FCA2");
				log.debug("FCA2={}",FCA2);
				String FCA2Format = fund_transfer_service.formatNumberString(FCA2,2);
				log.debug("FCA2Format={}",FCA2Format);
				bs.addData("FCA2Format",FCA2Format);
				
				String FCA1 = (String)bsData.get("FCA1");
				log.debug("FCA1={}",FCA1);
				String FCA1Format = fund_transfer_service.formatNumberString(FCA1,2);
				log.debug("FCA1Format={}",FCA1Format);
				bs.addData("FCA1Format",FCA1Format);
				
				String AMT5 = (String)bsData.get("AMT5");
				log.debug("AMT5={}",AMT5);
				String AMT5NoDot = AMT5.replaceAll("\\.","");
				log.debug("AMT5NoDot={}",AMT5NoDot);
				
				String AMT5Format = fund_transfer_service.formatNumberString(AMT5NoDot,2);
				log.debug("AMT5Format={}",AMT5Format);
				bs.addData("AMT5Format",AMT5Format);
				
				String TRADEDATE = requestParam.get("TRADEDATE");
				log.debug(ESAPIUtil.vaildLog("TRADEDATE={}"+TRADEDATE));
				
				String TRADEDATEFormat = TRADEDATE.substring(1,4) + "/" + TRADEDATE.substring(4,6) + "/" + TRADEDATE.substring(6);
				log.debug(ESAPIUtil.vaildLog("TRADEDATEFormat={}"+TRADEDATEFormat));
				bs.addData("TRADEDATEFormat",TRADEDATEFormat);
				
				BaseResult n922BS = new BaseResult();
				n922BS = fund_transfer_service.getN922Data(requestParam.get("CUSIDN"));
				
				if(n922BS != null && n922BS.getResult()){
					Map<String,Object> n922BSData = (Map<String,Object>)n922BS.getData();
					log.debug("n922BSData={}",n922BSData);
					
//					String NAME = (String)n922BSData.get("NAME");
//					log.debug("NAME={}",NAME);
					
					//20210617修正  若有CUSNAME取CUSNAME 無則為NAME , 不然會是空的 //N922原住民姓名修改由NAME->CUSNAME  
					String NAME = "";
					if(StrUtils.isNotEmpty((String)n922BSData.get("CUSNAME"))) {
						NAME = (String)n922BSData.get("CUSNAME");
					}else {
						NAME = (String)n922BSData.get("NAME");
					}
					String hiddenNAME = WebUtil.hideusername1Convert(NAME);
					log.debug("hiddenNAME={}",hiddenNAME);
					bs.addData("hiddenNAME",hiddenNAME);
				}
				
//				model.addAttribute("bsData",bsData);
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("processFundReserveTransfer error >> {}",e);
		}
		return bs;
	}
}