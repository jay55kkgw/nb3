
/*
 * Copyright (c) 2017, FSTOP, Inc. All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tw.com.fstop.spring.helper;

import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

@Component
public class I18n
{

    @Autowired
    private MessageSource messageSource;
    
    private static MessageSource msg;
    
    @PostConstruct
    public void init() 
    {
        msg = messageSource;
    }

    public MessageSource getMessageSource()
    {
        return messageSource;
    }

    public void setMessageSource(MessageSource messageSource)
    {
        this.messageSource = messageSource;
    }

    /**
     * Get i18n translation with message parameters, locale and default value.
     * 
     * @param key - i18n key
     * @param params - message parameters
     * @param locale - locale
     * @param def - default value
     * @return i18n translation
     */
    public String getMsg(String key, Object[] params, Locale locale, String def)
    {
        String ret = getMessage(key, params, locale);
        if (ret == null)
        {
            ret = def;
        }
        return ret;
    }
    
    /**
     * Get i18n translation with message parameters, locale.
     * 
     * @param key - i18n key
     * @param params - message parameters
     * @param locale - locale
     * @return i18n translation
     */
    public String getMsg(String key, Object[] params, Locale locale)
    {
        return getMessage(key, params, locale);
    }
    
    /**
     * Get i18n translation with message parameters and default value.
     * 
     * @param key - i18n key
     * @param params - message parameters
     * @param def - default value
     * @return i18n translation
     */
    public String getMsg(String key, Object[] params, String def)
    {
        return getMessageWithDefaultValue(key, params, def);
    }

    /**
     * Get i18n translation with message parameters.
     * 
     * @param key - i18n key
     * @param params - message parameters
     * @return i18n translation
     */
    public String getMsg(String key, Object[] params)
    {
        return getMessage(key, params, null);
    }
    
    /**
     * Get i18n translation with default value.
     * 
     * @param key - i18n key
     * @param def - default value
     * @return i18n translation
     */
    public String getMsg(String key, String def)
    {
        String ret = getMessage(key, null, null);
        if (ret == null)
        {
            ret = def;
        }
        return ret;
    }
    
    /**
     * Get i18n translation.
     * 
     * @param key - i18n key
     * @return i18n translation
     */
    public String getMsg(String key)
    {
        return getMessage(key, null, null);
    }

    //--
    public static String getMessageWithDefaultValue(String key, Object[] params, String def)
    {
        String ret = getMessage(key, params, null);
        if (ret == null)
        {
            ret = def;
        }
        return ret;
    }    

    public static String getMessage(String key, Object[] params, Locale locale)
    {
        String ret = null;
        if (locale == null)
        {
            // obtain locale from LocaleContextHolder
            Locale currentLocale = LocaleContextHolder.getLocale();
            locale = currentLocale;
        }
        
        try
        {
            ret = msg.getMessage(key, params, locale);            
        }
        catch(NoSuchMessageException e)
        {
            //just return null
        }
        
        return ret;
    }

    public static String getMessage(String key, Object[] params)
    {
        return getMessage(key, params, null);
    }    

    /**
     * Get i18n translation with default value.
     * 
     * @param key - i18n key
     * @param def - default value
     * @return i18n translation
     */
    public static String getMessage(String key, String def)
    {
        String ret = getMessage(key, null, null);
        if (ret == null)
        {
            ret = def;
        }
        return ret;
    }    

    public static String getMessage(String key)
    {
        return getMessage(key, null, null);
    }    

    
}
