package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class NI02_REST_RQ extends BaseRestBean_CC implements Serializable {

	private static final long serialVersionUID = -6018579665065621630L;
	
	
	//電文
	private String SYNC;		// Sync.Check Item
	private String PPSYNC;		// P.P.Key Sync.Check Item
	private String PINKEY;		// 網路銀行密碼
	private String CERTACN;		// 憑證帳號
	private String FLAG;		// 交易狀態 0
	private String BNKRA ;		// 行庫別 050
	private String XMLCA;		// CA 識別碼
	private String XMLCN;		// 憑證ＣＮ
	private String PPSYNCN;		// P.P.Key Sync.Check Item 16
	private String CUSIDN;      // 統一編號
	private String CARDNUM;    
	private String MAC;  
	
	// ms_tw交易機制需要的欄位
	private String FGTXWAY; // 交易機制
	private String pkcs7Sign; // IKEY
	private String jsondc; // IKEY
	public String getSYNC() {
		return SYNC;
	}
	public String getPPSYNC() {
		return PPSYNC;
	}
	public String getPINKEY() {
		return PINKEY;
	}
	public String getCERTACN() {
		return CERTACN;
	}
	public String getFLAG() {
		return FLAG;
	}
	public String getBNKRA() {
		return BNKRA;
	}
	public String getXMLCA() {
		return XMLCA;
	}
	public String getXMLCN() {
		return XMLCN;
	}
	public String getPPSYNCN() {
		return PPSYNCN;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public String getCARDNUM() {
		return CARDNUM;
	}
	public String getMAC() {
		return MAC;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public String getPkcs7Sign() {
		return pkcs7Sign;
	}
	public String getJsondc() {
		return jsondc;
	}
	public void setSYNC(String sYNC) {
		SYNC = sYNC;
	}
	public void setPPSYNC(String pPSYNC) {
		PPSYNC = pPSYNC;
	}
	public void setPINKEY(String pINKEY) {
		PINKEY = pINKEY;
	}
	public void setCERTACN(String cERTACN) {
		CERTACN = cERTACN;
	}
	public void setFLAG(String fLAG) {
		FLAG = fLAG;
	}
	public void setBNKRA(String bNKRA) {
		BNKRA = bNKRA;
	}
	public void setXMLCA(String xMLCA) {
		XMLCA = xMLCA;
	}
	public void setXMLCN(String xMLCN) {
		XMLCN = xMLCN;
	}
	public void setPPSYNCN(String pPSYNCN) {
		PPSYNCN = pPSYNCN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public void setCARDNUM(String cARDNUM) {
		CARDNUM = cARDNUM;
	}
	public void setMAC(String mAC) {
		MAC = mAC;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public void setPkcs7Sign(String pkcs7Sign) {
		this.pkcs7Sign = pkcs7Sign;
	}
	public void setJsondc(String jsondc) {
		this.jsondc = jsondc;
	}
	
}
