package tw.com.fstop.web.util;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.SSLContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections4.map.MultiValueMap;
import org.apache.commons.logging.Log;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.joda.time.DateTime;
//import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
//import org.apache.http.impl.client.CloseableHttpClient;
//import org.apache.http.impl.client.HttpClients;
//import org.apache.http.ssl.TrustStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.AStar.TBConvert.WStrURLConvert;

import fstop.orm.po.ADMMSGCODE;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.rest.bean.BaseRestBean;
import tw.com.fstop.tbb.nnb.dao.AdmMsgCodeDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.I18nUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.util.StrUtil;


public class RESTfulClientUtil {
	static Logger log = LoggerFactory.getLogger(RESTfulClientUtil.class);
//	static final String REST_TW_URL = SpringBeanFactory.getBean("REST_TW_URL");
	
//	static private Integer connectTimeout = 5*1000;
//	static private Integer socketTimeout = 6*1000;
//
//	static private RequestConfig requestConfig;
//	static private CloseableHttpClient httpClient;
	
	
	
	public static <T> T send(Class<?> sendClass , Class<?> retClass ,String api ) {
		
		Object  result = null;
	    RestTemplate restTemplate = new RestTemplate();
	    String url = "";
	    //GET 使用getForObject
//	    result = restTemplate.getForObject(REST_TW_URL, objclass, params);
	    try {
//			result = restTemplate.postForObject(REST_TW_URL,  sendClass ,retClass );
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("send error >> {}",e);
		}
		
		
		return (T) result;
		
	}
	
	
//	TODO 暫時使用 理論上要從MS_TW 回覆
	public static void getMessageByMsgCode(Object result) {
		log.trace("result>>{}", result);
		String msg = "";
		if (result != null) {
//			BaseResult bs = new BaseResult();
//			CodeUtil.convert2BaseResult(bs, result);
			msg = ((BaseRestBean) result).getMsgName();
			log.trace("msg0>>{}",msg);
			log.trace("MsgCode>>{}",((BaseRestBean) result).getMsgCode());
			if("0".equals(((BaseRestBean) result).getMsgCode())) {
				log.trace("MsgCode is 0  return..");
				return;
			}
			if (StrUtil.isEmpty(msg)) {

				msg = getMessageByMsgCode(((BaseRestBean) result).getMsgCode());
				((BaseRestBean) result).setMsgName(msg);
				log.trace("msg>>{}", ((BaseRestBean) result).getMsgName());

			}
		}

	}
	
	/**
	 * 根據訊息代碼取得訊息
	 * @param msgCode
	 * @return
	 */
	public static String getMessageByMsgCode(String msgCode ) {
		String message= "";
		ADMMSGCODE po =null;
		try {
			AdmMsgCodeDao admMsgCodeDao= SpringBeanFactory.getBean("admMsgCodeDao");
			log.trace("admMsgCodeDao>>{}",admMsgCodeDao);
			if(admMsgCodeDao.hasMsg(msgCode, null)) {
				po = admMsgCodeDao.get(ADMMSGCODE.class, msgCode);
				message = po.getADMSGIN();
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getMessageByMsgCode error >> {}",e);
		}
		log.trace("message>>{}",message);
		return message;
	}
	
	
	
	
	/**
	 * 電文傳送 RESTful
	 * @param sendClass
	 * @param retClass
	 * @param api
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T> T send(Object sendClass , Class<?> retClass ,String api ) {
		String url = "";
		String sendTime = "";
		Object result = null;
		Object retobj = null;
		RestTemplate restTemplate = null;
		HashMap<String, String> sendMap = null;
		//GET 使用getForObject
//	    result = restTemplate.getForObject(REST_TW_URL, objclass, params);
		try {
			url =api;
			log.trace("url>>{}",url);
			sendMap = CodeUtil.objectCovert(HashMap.class, sendClass);
			log.trace("sendMap >>{}",sendMap);
			if(sendMap ==null) {
				throw new Exception("電文轉換異常，此電文無法送出");
			}
			log.trace("sendJSON >>{}",CodeUtil.toJson(sendMap));
			restTemplate = setHttpConfig();
			sendTime = new DateTime().toString("yyyy-MM-dd HH:mm:ss");
//			result = restTemplate.postForObject(url,  sendMap ,HashMap.class );
			
			HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
			log.debug("request>>{}",request);
			String trace_cusidn = (String) request.getSession().getAttribute(SessionUtil.CUSIDN);
			log.debug("trace_cusidn>>{}",trace_cusidn);
			String trace_userid = (String) request.getSession().getAttribute(SessionUtil.USERID);
			log.debug("trace_userid>>{}",trace_userid);
			
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.set("trace_cusidn", trace_cusidn);
			headers.set("trace_userid", trace_userid);
			
			HttpEntity<Map<String, String>> entity = new HttpEntity<>(sendMap, headers);
			ResponseEntity<HashMap> re  = restTemplate.postForEntity(url, entity, HashMap.class );
			
			result = re.getBody();
			
			
			log.trace("result>>{}",result);
			
			log.trace("result.toJson>>{}", CodeUtil.toJson(result));
			
			log.trace("rest>>{}，收送花費時間>>{}",url,DateUtil.getDiffTimeStamp(sendTime, new DateTime().toString("yyyy-MM-dd HH:mm:ss")));
			if(result == null) {
				throw new Exception("result == null ,電文接收異常");
			}
			
			
			retobj = CodeUtil.objectCovert(retClass   ,result);
			log.trace("retobj >>{}",retobj);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("send error >> {}",e);
		}
		
		
		return (T) retobj;
		
	}
	
	/**
	 * 設定http連線參數
	 * @return
	 */
	
	public static RestTemplate setHttpConfig() {
		Integer socketTimeoutTomstw = SpringBeanFactory.getBean("socketTimeout");
		Integer connectTimeout = 5*1000;
		Integer socketTimeout = socketTimeoutTomstw*1000;
		HttpComponentsClientHttpRequestFactory rf = new HttpComponentsClientHttpRequestFactory();
		rf.setConnectTimeout(connectTimeout);
		rf.setReadTimeout(socketTimeout);
		return new RestTemplate(rf);
		
	}
	
	
	/**
	 * 測試用
	 */
	public static void setHttpConfigI() {
		Integer connectTimeout = 5*1000;
		Integer socketTimeout = 6*1000;
		
		RequestConfig requestConfig;
		CloseableHttpClient httpClient;
		
		
		requestConfig = RequestConfig.custom()
				.setConnectTimeout(connectTimeout)
				.setSocketTimeout(socketTimeout)
				.build();
		httpClient = HttpClientBuilder.create()
				.setDefaultRequestConfig(requestConfig)
				.build();
		
		
	}
	
	
	
//	
//	public static void setHttpConfig() {
//		
//		SSLContext sslContext = null;
//		
//		TrustStrategy acceptingTrustStrategy = new TrustStrategy() {
//			
//			@Override
//			public boolean isTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
//				// TODO Auto-generated method stub
////				return false;
//				log.trace("X509Certificate >>{} , String {}",arg0 , arg1);
//				return true;
//			}
//		};
//		
//		 try {
//			sslContext = org.apache.http.ssl.SSLContexts.custom()
//			            .loadTrustMaterial(null, acceptingTrustStrategy)
//			            .build();
//			
//			
//			
//			SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);
////			SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext , new String[]{"TLSv1.2"},null,
////					SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext , new String[]{"TLSv1","TLSv1.1", "TLSv1.2"},null,
////					SSLConnectionSocketFactory.STRICT_HOSTNAME_VERIFIER);
//			
//	        CloseableHttpClient httpClient = HttpClients.custom()
//	                .setSSLSocketFactory(csf)
//	                .build();
//	        HttpComponentsClientHttpRequestFactory requestFactory =
//	                new HttpComponentsClientHttpRequestFactory();
//
//	        requestFactory.setHttpClient(httpClient);
//			
//			
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			log.error("",e);
//		}
//	}
//	
	
	public static RestTemplate setHttpConfig2() {
		
		SSLContext sslContext = null;
		RestTemplate restTemplate = null;
		TrustStrategy acceptingTrustStrategy = new TrustStrategy() {
			
			@Override
			public boolean isTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
				// TODO Auto-generated method stub
//				return false;
				log.trace("X509Certificate >>{} , String {}",arg0 , arg1);
				return true;
			}
		};
		
		try {
			sslContext = org.apache.http.ssl.SSLContexts.custom()
					.loadTrustMaterial(null, acceptingTrustStrategy)
					.build();
			
			
			
			SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);
//			SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext , new String[]{"TLSv1.2"},null,
//					SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext , new String[]{"TLSv1","TLSv1.1", "TLSv1.2"},null,
//					SSLConnectionSocketFactory.STRICT_HOSTNAME_VERIFIER);
			
			CloseableHttpClient httpClient = HttpClients.custom()
					.setSSLSocketFactory(csf)
					.build();
			HttpComponentsClientHttpRequestFactory requestFactory =
					new HttpComponentsClientHttpRequestFactory();
			
			requestFactory.setHttpClient(httpClient);
			
			restTemplate = new RestTemplate(requestFactory);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("isTrusted error >> {}",e);
		}
		return restTemplate;
	}
	
	
	public static void sendTest() {
		String url = "https://nnb.tbb.com.tw/TBBNBAppsWeb_MB/services/";
//		String url = "https://nnb.tbb.com.tw";
		String result = null;
		String request = "";
		RestTemplate restTemplate = null;
		//GET 使用getForObject
//	    result = restTemplate.getForObject(REST_TW_URL, objclass, params);
		try {
			log.trace("url>>{}",url);
//			setHttpConfig();
//			restTemplate = new RestTemplate();
			result = restTemplate.getForObject( url,String.class);
			restTemplate = setHttpConfig2();
			result = restTemplate.postForObject(url, request, String.class);
			log.trace("result>>{}",result);
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("sendTest error >> {}",e);
		}
		
	}
	public static String sendTest2() {
		MessageHttpClient client = new MessageHttpClient();
		String s = null;


        HashMap<String, String> request  = new HashMap();
        request.put("name", "AB123");
        request.put("version", "3.1.1");
//      s = "http://localhost:8080/rest/N110";
//      s = "https://nnb.tbb.com.tw";
        s = "https://nnb.tbb.com.tw/TBBNBAppsWeb_MB/services/";
		try {
			s = client.send(s, request);
		} catch (IOException e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("sendTest2 error >> {}",e);
		}
		// 因Heuristic CGI Stored XSS註解
//        System.out.println(s);
        return s;
	}
	
	
	
	
	
	public static void main(String args[]) throws Exception {
		sendTest();
	}
	
	
}
