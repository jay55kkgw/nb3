package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N845_REST_RS extends BaseRestBean implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8360251093123480087L;
	
	private String CMQTIME;
//	private String TOPMSG;
	private String ABEND;
	private String REC_NO;
	private String __OCCURS;
	private String USERDATA;
	private LinkedList<N845_REST_RSDATA> REC;
//	private String TXID;
//	private String msgCode;
	
	
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
//	public String getTOPMSG() {
//		return TOPMSG;
//	}
//	public void setTOPMSG(String tOPMSG) {
//		TOPMSG = tOPMSG;
//	}
	public String getABEND() {
		return ABEND;
	}
	public void setABEND(String aBEND) {
		ABEND = aBEND;
	}
	public String getREC_NO() {
		return REC_NO;
	}
	public void setREC_NO(String rEC_NO) {
		REC_NO = rEC_NO;
	}
	public String get__OCCURS() {
		return __OCCURS;
	}
	public void set__OCCURS(String __OCCURS) {
		this.__OCCURS = __OCCURS;
	}
	public String getUSERDATA() {
		return USERDATA;
	}
	public void setUSERDATA(String uSERDATA) {
		USERDATA = uSERDATA;
	}
	public LinkedList<N845_REST_RSDATA> getREC() {
		return REC;
	}
	public void setREC(LinkedList<N845_REST_RSDATA> rEC) {
		REC = rEC;
	}
//	public String getTXID() {
//		return TXID;
//	}
//	public void setTXID(String tXID) {
//		TXID = tXID;
//	}
//	public String getMsgCode() {
//		return msgCode;
//	}
//	public void setMsgCode(String msgCode) {
//		this.msgCode = msgCode;
//	}
	
	
	
}
