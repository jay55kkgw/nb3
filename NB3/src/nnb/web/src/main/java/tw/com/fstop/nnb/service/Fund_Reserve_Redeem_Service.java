package tw.com.fstop.nnb.service;

import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import fstop.orm.po.ADMCURRENCY;
import fstop.orm.po.TXNFUNDDATA;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.StrUtils;
import tw.com.fstop.web.util.WebUtil;

/**
 * 基金預約贖回交易的Service
 */
@Service
public class Fund_Reserve_Redeem_Service extends Base_Service{
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private Fund_Transfer_Service fund_transfer_service;
	
	@Autowired
	private I18n i18n;
	
	/**
	 * 準備前往基金預約贖回選擇頁的資料
	 */
	public BaseResult prepareFundReserveRedeemSelectData(Map<String,String> requestParam){
		BaseResult bs =new BaseResult();
		
		bs.addAllData(requestParam);
		
		String hiddenCUSIDN = WebUtil.hideID(requestParam.get("CUSIDN"));
		bs.addData("hiddenCUSIDN",hiddenCUSIDN);
		
		String hiddenNAME = WebUtil.hideusername1Convert(requestParam.get("CUSNAME"));
		bs.addData("hiddenNAME",hiddenNAME);
		
		String hiddenCDNO = WebUtil.hideAccount(requestParam.get("CDNO"));
		bs.addData("hiddenCDNO",hiddenCDNO);
		
		String CRY = requestParam.get("CRY");
		log.debug(ESAPIUtil.vaildLog("CRY={}"+CRY));
		bs.addData("CRY",CRY);

		String TYPE10 = requestParam.get("TYPE10");
		log.debug(ESAPIUtil.vaildLog("TYPE10={}"+TYPE10));
		bs.addData("TYPE10", TYPE10);
		
		String FUNDT = requestParam.get("FUNDT");
		log.debug(ESAPIUtil.vaildLog("FUNDT={}"+FUNDT));
		bs.addData("FUNDT", FUNDT);
		
		String RAMT = "";
		String AMT = "";
		if(CRY.equals("NTD")) {
			if (TYPE10.equals("1")) {
				RAMT = requestParam.get("C30RAMT");
				AMT = requestParam.get("C30AMT");
			} else {
				RAMT = requestParam.get("C302RAMT");
				AMT = requestParam.get("C302AMT");
			}
		} else {
			if (TYPE10.equals("1")) {
				RAMT = requestParam.get("Y30RAMT");
				AMT = requestParam.get("Y30AMT");
			} else {
				RAMT = requestParam.get("Y302RAMT");
				AMT = requestParam.get("Y302AMT");
			}
		}
		bs.addData("RAMT", RAMT);
		bs.addData("AMT", AMT);
		log.debug("CRY = " + CRY + ", TYPE10 = " + TYPE10 + ", RAMT = " + RAMT + ", AMT = " + AMT);
		
		String FORMAT_RAMT = fund_transfer_service.formatNumberString(RAMT,0);
		bs.addData("FORMAT_RAMT", FORMAT_RAMT);
		String FORMAT_AMT = fund_transfer_service.formatNumberString(AMT,0);
		bs.addData("FORMAT_AMT", FORMAT_AMT);
		
		ADMCURRENCY admCurrency = fund_transfer_service.getCRYData(CRY);	
		if(admCurrency != null){
			String ADCCYNAME = "";
			Locale currentLocale = LocaleContextHolder.getLocale();
    		String locale = currentLocale.toString();
    		
    		switch (locale) {
			case "en":
				ADCCYNAME = admCurrency.getADCCYENGNAME();
				break;
			case "zh_CN":
				ADCCYNAME = admCurrency.getADCCYCHSNAME();
				break;
			default:
				ADCCYNAME = admCurrency.getADCCYNAME();
				break;
            }
    		bs.addData("ADCCYNAME",ADCCYNAME);
		}
		
		String OFUNDAMT = requestParam.get("OFUNDAMT");	
		String OFUNDAMTDotTwo = OFUNDAMT.replaceAll("\\.","");
		log.debug("OFUNDAMTDotTwo={}",OFUNDAMTDotTwo);
		
		OFUNDAMTDotTwo = fund_transfer_service.formatNumberString(OFUNDAMTDotTwo,2);
		bs.addData("OFUNDAMTDotTwo",OFUNDAMTDotTwo);
		
		String UNIT = requestParam.get("UNIT");
		UNIT = fund_transfer_service.formatNumberString(UNIT,4);
		bs.addData("UNIT",UNIT);
		
		String BILLSENDMODE = requestParam.get("BILLSENDMODE");			
		String TRADEDATE = requestParam.get("TRADEDATE");		
		String TRADEDATEFormat = TRADEDATE.substring(1,4) + "/" + TRADEDATE.substring(4,6) + "/" + TRADEDATE.substring(6);
		bs.addData("TRADEDATEFormat",TRADEDATEFormat);
		String BILLSENDMODEChinese;
		//全部
		if("1".equals(BILLSENDMODE)){
			BILLSENDMODEChinese = i18n.getMsg("LB.All");//全部
			bs.addData("BILLSENDMODEChinese",BILLSENDMODEChinese);
			if(OFUNDAMT.indexOf(".") != -1){
				OFUNDAMT = OFUNDAMT.substring(0,OFUNDAMT.indexOf("."));		
			}
			OFUNDAMT = fund_transfer_service.formatNumberString(OFUNDAMT,0);
			bs.addData("FUNDAMT",OFUNDAMT);
		}
		//部分
		else{
			BILLSENDMODEChinese = i18n.getMsg("LB.X0383");
			bs.addData("BILLSENDMODEChinese",BILLSENDMODEChinese);
		}
		
		String FEE_TYPE = requestParam.get("FEE_TYPE");
		log.debug(ESAPIUtil.vaildLog("FEE_TYPE={}"+FEE_TYPE));
		bs.addData("FEE_TYPE",FEE_TYPE);
		
		return bs;
	}
	/**
	 * 準備前往基金預約贖回確認頁的資料
	 */
	public void prepareFundReserveRedeemConfirmData(Map<String,String> requestParam,BaseResult bs){
		Map<String,Object> bsData = (Map<String,Object>)bs.getData();
		String NAME = (String)bsData.get("NAME");
		String hiddenNAME = WebUtil.hideusername1Convert(NAME);
		bs.addData("hiddenNAME",hiddenNAME);
		bs.addData("BANKID",bsData.get("APLBRH"));
		
		bs.addAllData(requestParam);

		String hiddenCDNO = WebUtil.hideAccount(requestParam.get("CDNO"));
		bs.addData("hiddenCDNO",hiddenCDNO);
		
		
		String CRY = requestParam.get("CRY");
		String TYPE = "";
		if("NTD".equals(CRY)){
			TYPE = "03";
		}else{
			TYPE = "04";
		}
		bs.addData("TYPE",TYPE);
		
		ADMCURRENCY admCurrency = fund_transfer_service.getCRYData(CRY);
		if(admCurrency != null){
			String ADCCYNAME = "";
			Locale currentLocale = LocaleContextHolder.getLocale();
    		String locale = currentLocale.toString();
    		
    		switch (locale) {
			case "en":
				ADCCYNAME = admCurrency.getADCCYENGNAME();
				break;
			case "zh_CN":
				ADCCYNAME = admCurrency.getADCCYCHSNAME();
				break;
			default:
				ADCCYNAME = admCurrency.getADCCYNAME();
				break;
            }
			bs.addData("ADCCYNAME",ADCCYNAME);
		}
		
		String OFUNDAMT = requestParam.get("OFUNDAMT");	
		String OFUNDAMTDotTwo = OFUNDAMT.replaceAll("\\.","");		
		OFUNDAMTDotTwo = fund_transfer_service.formatNumberString(OFUNDAMTDotTwo,2);
		bs.addData("OFUNDAMTDotTwo",OFUNDAMTDotTwo);
		
		String TRADEDATE = requestParam.get("TRADEDATE");
		bs.addData("RESTOREDAY",TRADEDATE);
		String TRADEDATEFormat = TRADEDATE.substring(1,4) + "/" + TRADEDATE.substring(4,6) + "/" + TRADEDATE.substring(6);
		bs.addData("TRADEDATEFormat",TRADEDATEFormat);
		
		String UNIT = requestParam.get("UNIT");
		String UNITNoComma = UNIT.replaceAll(",","");
		bs.addData("UNITNoComma",UNITNoComma);
		
		String BILLSENDMODE = requestParam.get("BILLSENDMODE");
		String BILLSENDMODEChinese;
		//全部
		if("1".equals(BILLSENDMODE)){
			BILLSENDMODEChinese = i18n.getMsg("LB.All");//全部
			bs.addData("BILLSENDMODEChinese",BILLSENDMODEChinese);
		}//部分
		else{
			BILLSENDMODEChinese = i18n.getMsg("LB.X0383");
			bs.addData("BILLSENDMODEChinese",BILLSENDMODEChinese);
		}
		
		String FUNDAMT = requestParam.get("FUNDAMT");
		//全部
		if("1".equals(BILLSENDMODE)){
			bs.addData("FUNDAMTFormat",FUNDAMT);
		}
		//部份
		else{
			String FUNDAMTFormat = fund_transfer_service.formatNumberString(FUNDAMT,0);
			bs.addData("FUNDAMTFormat",FUNDAMTFormat);
		}
		String FUNDAMTNoComma = FUNDAMT.replaceAll(",","");
		bs.addData("FUNDAMTNoComma",FUNDAMTNoComma);
		
		String FEE_TPYE = requestParam.get("FEE_TPYE");
		bs.addData("FEE_TPYE",FEE_TPYE);
		
	}
	/**
	 * 基金預約贖回交易
	 */
	@SuppressWarnings("unchecked")
	public BaseResult processFundReserveRedeem(Map<String,String> requestParam){
		BaseResult bs = null;
		try{
			bs = new BaseResult();
			
			String UNITOriginal = requestParam.get("UNITNoComma").replaceAll("\\.","");
			
			while(UNITOriginal.length() < 12){
				UNITOriginal = "0" + UNITOriginal;
			}
			requestParam.put("UNITOriginal",UNITOriginal);
			
			bs = N374_REST(requestParam);
			log.debug("N374 BS DATA >>{}",CodeUtil.toJson(bs));
			if(bs != null && bs.getResult()){
				//bs.addAllData(requestParam);
				
				Map<String,Object> bsData = (Map<String,Object>)bs.getData();
				
				String CDNO = (String)bsData.get("CDNO");
				String hiddenCDNO = WebUtil.hideAccount(CDNO);
				//model.addAttribute("hiddenCREDITNO",hiddenCREDITNO);
				bs.addData("hiddenCDNO",hiddenCDNO);
				
				String TRANSCODE = (String)bsData.get("TRANSCODE");			
				String FUNDCUR = (String)bsData.get("FUNDCUR");				
				TXNFUNDDATA txnFundData = fund_transfer_service.getFundData(TRANSCODE);
				String FUNDLNAME = "";				
				if(txnFundData != null){
					FUNDLNAME = txnFundData.getFUNDLNAME();	
					ADMCURRENCY admCurrency = fund_transfer_service.getCRYData(FUNDCUR);
					if(admCurrency != null){
						String ADCCYNAME = "";
						Locale currentLocale = LocaleContextHolder.getLocale();
			    		String locale = currentLocale.toString();
			    		
			    		switch (locale) {
						case "en":
							ADCCYNAME = admCurrency.getADCCYENGNAME();
							break;
						case "zh_CN":
							ADCCYNAME = admCurrency.getADCCYCHSNAME();
							break;
						default:
							ADCCYNAME = admCurrency.getADCCYNAME();
							break;
		                }
						log.debug("ADCCYNAME={}",ADCCYNAME);
						//model.addAttribute("ADCCYNAME",ADCCYNAME);
						bs.addData("ADCCYNAME",ADCCYNAME);
					}
				}
				//model.addAttribute("FUNDLNAME",FUNDLNAME);
				bs.addData("FUNDLNAME",FUNDLNAME);

				String BILLSENDMODE = (String)bsData.get("BILLSENDMODE");		
				String BILLSENDMODEChinese;
				//全部
				if("1".equals(BILLSENDMODE)){
					BILLSENDMODEChinese = i18n.getMsg("LB.All");//全部
					//model.addAttribute("BILLSENDMODEChinese",BILLSENDMODEChinese);
					bs.addData("BILLSENDMODEChinese",BILLSENDMODEChinese);
				}
				//部分
				else{
					BILLSENDMODEChinese = i18n.getMsg("LB.X0383");
					//model.addAttribute("BILLSENDMODEChinese",BILLSENDMODEChinese);
					bs.addData("BILLSENDMODEChinese",BILLSENDMODEChinese);
				}
				
				String UNIT = (String)bsData.get("UNIT");
				String UNITFormat = fund_transfer_service.formatNumberString(UNIT,4);
				//model.addAttribute("UNITFormat",UNITFormat);
				bs.addData("UNITFormat",UNITFormat);
				
				String FUNDAMT = (String)bsData.get("FUNDAMT");
				String FUNDAMTNoDot = FUNDAMT.replaceAll("\\.","");
				String FUNDAMTFormat = fund_transfer_service.formatNumberString(FUNDAMTNoDot,2);
				//model.addAttribute("FUNDAMTFormat",FUNDAMTFormat);
				bs.addData("FUNDAMTFormat",FUNDAMTFormat);
				
				String TRADEDATE = (String)bsData.get("RESTOREDAY");				
				String TRADEDATEFormat = TRADEDATE.substring(1,4) + "/" + TRADEDATE.substring(4,6) + "/" + TRADEDATE.substring(6);
				//model.addAttribute("TRADEDATEFormat",TRADEDATEFormat);
				bs.addData("TRADEDATEFormat",TRADEDATEFormat);
				
				BaseResult n922BS = new BaseResult();
				n922BS = fund_transfer_service.getN922Data(requestParam.get("CUSIDN"));
				
				if(n922BS != null && n922BS.getResult()){
					Map<String,Object> n922BSData = (Map<String,Object>)n922BS.getData();

//					String NAME = (String)n922BSData.get("NAME");
					//20210617修正  若有CUSNAME取CUSNAME 無則為NAME , 不然會是空的 //N922原住民姓名修改由NAME->CUSNAME  
					String NAME = "";
					if(StrUtils.isNotEmpty((String)n922BSData.get("CUSNAME"))) {
						NAME = (String)n922BSData.get("CUSNAME");
					}else {
						NAME = (String)n922BSData.get("NAME");
					}
					
					String hiddenNAME = WebUtil.hideusername1Convert(NAME);
					//model.addAttribute("hiddenNAME",hiddenNAME);
					bs.addData("hiddenNAME",hiddenNAME);
				}
				//model.addAttribute("bsData",bsData);
			}
			//log.debug("MODEL DATA >>{}",CodeUtil.toJson(model));
			log.debug(ESAPIUtil.vaildLog("BS DATA >>{}"+CodeUtil.toJson(bs)));
			log.debug(ESAPIUtil.vaildLog("REQPARAM DATA >>{}"+CodeUtil.toJson(requestParam)));
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("processFundReserveRedeem error >> {}",e);
		}
		return bs;
	}
}