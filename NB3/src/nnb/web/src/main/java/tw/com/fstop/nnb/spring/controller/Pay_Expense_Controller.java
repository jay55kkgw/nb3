package tw.com.fstop.nnb.spring.controller;

import java.net.URLEncoder;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.idgate.bean.N070B_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N070B_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N171_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N171_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N750A_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N750A_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N070_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N070_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N070A_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N070A_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N073_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N073_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N075_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N075_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N171_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N171_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N750A_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N750A_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N750B_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N750B_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N750C_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N750C_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N750E_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N750E_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N750F_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N750F_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N750G_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N750G_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N750H_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N750H_IDGATE_DATA_VIEW;
import tw.com.fstop.nnb.service.Acct_Transfer_Service;
import tw.com.fstop.nnb.service.IdGateService;
import tw.com.fstop.nnb.service.Pay_Expense_Service;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.util.StrUtils;
import tw.com.fstop.web.util.WebUtil;

@SessionAttributes({SessionUtil.CUSIDN, SessionUtil.PD,SessionUtil.DOWNLOAD_DATALISTMAP_DATA,
					SessionUtil.PRINT_DATALISTMAP_DATA,SessionUtil.RESULT_LOCALE_DATA, SessionUtil.CONFIRM_LOCALE_DATA,
					SessionUtil.RESULT_LOCALE_DATA,SessionUtil.CUSIDN, SessionUtil.TRANSFER_CONFIRM_TOKEN, SessionUtil.TRANSFER_RESULT_TOKEN,
					SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, SessionUtil.TRANSFER_DATA, SessionUtil.BACK_DATA, SessionUtil.IDGATE_TRANSDATA })
@Controller
@RequestMapping(value = "/PAY/EXPENSE")
public class Pay_Expense_Controller {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private Acct_Transfer_Service acct_transfer_service ;
	
	@Autowired
	private Pay_Expense_Service pay_expense_service;
	
	@Autowired		 
	IdGateService idgateservice;

	/**
	 * 取得臺幣存款轉出帳號
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/getOutAcn_aj" ,produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult getOutAcn(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("electricity_fee_result.getOutACNO...");
		BaseResult bs = new BaseResult();
		
		try {
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			bs = acct_transfer_service.getOutACNO_List(cusidn);
		} catch (Exception e) {
			log.error("", e);
		}
		
		return bs;
		
	}
	
	/**
	 * 開新視窗顯示N171-繳稅_稽徵單位代號列表
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/unit_code_list")
	public String index(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.debug("unit_code_list...");
		String target = "/pay_expense/unit_code_list";
		log.trace("target: " + target);
		return target;
	}
	

	@RequestMapping(value = "/school")
	public String school(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		return "/pay_expense/school";
	}
	/**
	 * 進入N070A-學雜費_輸入頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/tuition_fee")
	public String tuition_fee(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		log.trace("tuition_fee...");
		BaseResult bs = null;
		
		try {
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
		
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);

			// 是否是回上一頁
			if("Y".equals(okMap.get("back"))) {
				model.addAttribute("isBack","Y");
				Map<String, String> previousData = (Map) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				model.addAttribute("previousdata", CodeUtil.toJson(previousData));
				log.trace("previousdata: {}", CodeUtil.toJson(previousData));
			}
			
			// 設定頁面預約日期為明天，如果當前時間太晚則預約後天
			model.addAttribute("tmrDate", DateUtil.getTomorrowOrAfterTomorrowDate());
			
			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = acct_transfer_service.getTxToken();
			log.trace("tuition_fee.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			if(bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			}
			
			// 登入時的身分證字號
			String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				// 解密成明碼
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				log.trace("tuition_fee.cusidn: " + cusidn);
				
				// 轉出成功是否Email通知
				boolean chk_result = pay_expense_service.chkContains(cusidn);
				model.addAttribute("sendMe", chk_result);
				
	           //IDGATE身分
	           String idgateUserFlag="N";		 
	           Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
	           try {		 
	        	   if(IdgateData==null) {
	        		   IdgateData = new HashMap<String, Object>();
	        	   }
	               BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
	               if(tmp.getResult()) {		 
	                   idgateUserFlag="Y";                  		 
	               }		 
	               tmp.addData("idgateUserFlag",idgateUserFlag);		 
	               IdgateData.putAll((Map<String, String>) tmp.getData());
	               SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	           }catch(Exception e) {		 
	               log.debug("idgateUserFlag error {}",e);		 
	           }		 
	           model.addAttribute("idgateUserFlag",idgateUserFlag);
				
			}else {
				log.error("session no cusidn!!!");
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("tuition_fee error >> {}",e);
		} finally {
			if(bs!=null && bs.getResult()) {
				target = "/pay_expense/tuition_fee";
				model.addAttribute("tuition_fee", bs);
				
				// 表單驗證用
				model.addAttribute("str_SystemDate", DateUtil.getNextDay());
				model.addAttribute("sNextYearDay", DateUtil.getNextYearDay());
				
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
				
			}
		}
		return target;
	}
	
	/**
	 * 進入N070A-學雜費_確認頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/tuition_fee_confirm")
	public String tuition_fee_confirm(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("tuition_fee_confirm...");
		
		String target = "/error";
		String jsondc = "";
		BaseResult bs = new BaseResult();
		
		log.trace(ESAPIUtil.vaildLog("tuition_fee_confirm.reqParam: {}"+CodeUtil.toJson(reqParam)));
		
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		
		try {
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("tuition_fee_confirm.jsondc: " + jsondc));
			
			okMap.put("jsondc", jsondc); // IKEY要用不經過ESAPIUtil，會出錯
			okMap.put("LOGINTYPE", "NB"); // 系統別
			log.trace(ESAPIUtil.vaildLog("tuition_fee_confirm.okMap: {}"+ okMap));
			
			
			
			// 回上一頁重新賦值
			if( okMap.containsKey("previous") ){
				Map<String, String> backMap = (Map<String, String>) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				okMap = backMap;
			}
			
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale: {}"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				if( !bs.getResult())
				{
//					TODO 交易結束後，CONFIRM_LOCALE_DATA 會被清空，造成在URL輸入確認頁會導向錯誤頁，原因待查
					log.trace("bs.getResult(): {}", bs.getResult());
					throw new Exception();
				}else{
					// session 資料放入 map 讓後續 model 和回上一頁取值
					okMap.putAll((Map<String, String>) bs.getData());
				}
			}else {
				// 驗證TOKEN 沒TOKEN會到錯誤頁，錯誤頁確認後返回輸入頁
				String token = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null);
				log.trace("tuition_fee_confirm.token: {}", token);
				if(StrUtil.isNotEmpty(okMap.get("TOKEN"))  &&  okMap.get("TOKEN").equals(token)) {
					bs.setResult(Boolean.TRUE);
				}
				log.trace("bs.getResult(): {}",bs.getResult());
				if(!bs.getResult()) {
					log.trace("throw new Exception()");
					throw new Exception();
				}
				
				// TOKEN驗證成功，資料處理後進入確認頁
				bs.reset();
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
				bs = pay_expense_service.tuition_fee_confirm(okMap, cusidn);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
				
				
				// IDGATE transdata        
				Map<String,String> result = new HashMap();
				result.putAll((Map< String,String>) bs.getData());
				String AMOUNT = result.get("AMOUNT");
				String transfer_date = result.get("transfer_date");
				pay_expense_service.preProcessing(result);
				result.put("IDGATE_AMOUNT", AMOUNT);
				result.put("transfer_date", transfer_date);
	            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
	    		String adopid = "N070A";
	    		String title = "您有一筆 繳費-學雜費 待確認，金額"+result.get("IDGATE_AMOUNT");
	        	if(IdgateData != null) {
		            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
		            if(IdgateData.get("idgateUserFlag").equals("Y")) {
		            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
		            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
		            	result.put("ACN", result.get("OUTACN"));
						IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N070A_IDGATE_DATA.class, N070A_IDGATE_DATA_VIEW.class, result));
		                }
		                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
		            }
	            model.addAttribute("idgateAdopid", adopid);
	            model.addAttribute("idgateTitle", title);
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("tuition_fee_confirm error >> {}",e);
		}finally {
			if(bs.getResult()) {
				target = "/pay_expense/tuition_fee_confirm";
//			TODO 這邊還要調整 不能存物件，要存成JSNO字串
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, ((Map<String,Object>)bs.getData()).get("TXTOKEN"));
				
				// 設定回上一頁的url,配合confirm頁面的js
				model.addAttribute("previous","/PAY/EXPENSE/tuition_fee");
				// 回填使用DATA
				SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, okMap);
			}else {
				// 到錯誤頁，錯誤頁的確認按鈕導頁到輸入頁
				bs.setNext("/PAY/EXPENSE/tuition_fee");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * 進入N070A-學雜費_結果頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/tuition_fee_result")
	public String tuition_fee_result(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("tuition_fee_result...");
		log.trace(ESAPIUtil.vaildLog("reqParam {}" +CodeUtil.toJson(reqParam)));

		String target = "/error";
		String next = "/PAY/EXPENSE/tuition_fee";
		String previous = "/PAY/EXPENSE/tuition_fee_confirm";

		BaseResult bs = null;
		try {
			// 初始化BaseResult
			bs = new BaseResult();
			
			// 取得CUSIDN明碼
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//			Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			
			// 判斷LOCAL KEY是否有帶值，有帶表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			log.trace("tuition_fee_result.locale: " + hasLocale);
			if(hasLocale){
				log.trace("tuition_fee_result.hasLocale...");
				// 判斷從session取出的資料物件是否為BaseResult物件
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( !bs.getResult() ){
					log.trace("tuition_fee_result.bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			// 判斷LOCAL KEY沒有帶值則驗證TXTOKEN，驗證通過才可做交易
			if(!hasLocale){
				log.trace("tuition_fee_result.validate TXTOKEN...");
				log.trace("tuition_fee_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("tuition_fee_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("tuition_fee_result.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("tuition_fee_result TXTOKEN 不正確，TXTOKEN>>{}"+ okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						!okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("tuition_fee_result.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("tuition_fee_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				
				
				// 取得從輸入頁輸入存在session中的輸入資料
				bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
				Map<String,String> transfer_data = (Map<String, String>) bs.getData();
				
				// 取得SESSION中的輸入資料，加入到確認頁的交易機制資料
				okMap.putAll(transfer_data);
				
				// 重置bs，進行交易
				bs.reset();
				
                //IDgate驗證		 
                try {		 
                    if(okMap.get("FGTXWAY").equals("7")) {		 
        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N070A_IDGATE");
                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));	
                    }
                }catch(Exception e) {		 
                    log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
                }  
                bs = pay_expense_service.tuition_fee_result(cusidn, okMap);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
			model.addAttribute("transfer_result_data", bs);
			Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
			model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("tuition_fee_result error >> {}",e);
		} finally {
//			TODO 要清除必要的SESSION
			
			// 解決Trust Boundary Violation
//			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			
			// 交易成功要存之前的token 在Session 以利下次比對是否重複交易
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TXTOKEN"));
			if(bs.getResult()) {
				target = "/pay_expense/tuition_fee_result";
			}else {
				bs.setNext(next);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	
	/**
	 * 進入N171-繳稅_輸入頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/pay_taxes")
	public String pay_taxes(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		log.trace("pay_taxes...");
		BaseResult bs = null;
		
		try {
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
		
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);

			// 是否是回上一頁
			if("Y".equals(okMap.get("back"))) {
				model.addAttribute("isBack","Y");
				Map<String, String> previousData = (Map) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				model.addAttribute("previousdata", CodeUtil.toJson(previousData));
				log.trace("previousdata: {}", CodeUtil.toJson(previousData));
			}
			
			// 設定頁面預約日期為明天，如果當前時間太晚則預約後天
			model.addAttribute("tmrDate", DateUtil.getTomorrowOrAfterTomorrowDate());
			
			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = acct_transfer_service.getTxToken();
			log.trace("pay_taxes.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			if(bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			}
			
			// 登入時的身分證字號
			String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				// 解密成明碼
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				log.trace("pay_taxes.cusidn: " + cusidn);
				
				// 轉出成功是否Email通知
				boolean chk_result = pay_expense_service.chkContains(cusidn);
				model.addAttribute("sendMe", chk_result);
				
			}else {
				log.error("session no cusidn!!!");
			}
			
			
			// IDGATE身分
			String idgateUserFlag = "N";
			Map<String, Object> IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
			try {
				if (IdgateData == null) {
					IdgateData = new HashMap<String, Object>();
				}
				BaseResult tmp = idgateservice.checkIdentity(cusidn);
				if (tmp.getResult()) {
					idgateUserFlag = "Y";
				}
				tmp.addData("idgateUserFlag", idgateUserFlag);
				IdgateData.putAll((Map<String, String>) tmp.getData());
				SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);
			} catch (Exception e) {
				log.debug("idgateUserFlag error {}", e);
			}
			model.addAttribute("idgateUserFlag", idgateUserFlag);

			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("pay_taxes error >> {}",e);
		} finally {
			if(bs!=null && bs.getResult()) {
				target = "/pay_expense/pay_taxes";
				model.addAttribute("pay_taxes", bs);
				
				// 表單驗證用
				model.addAttribute("str_SystemDate", DateUtil.getNextDay());
				model.addAttribute("sNextYearDay", DateUtil.getNextYearDay());
				model.addAttribute("str_Today", DateUtil.getDate("/"));
				
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
				
			}
		}
		return target;
	}
	
	/**
	 * 進入N171-繳稅_確認頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/pay_taxes_confirm")
	public String pay_taxes_confirm(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("pay_taxes_confirm...");
		
		String target = "/error";
		String jsondc = "";
		String cusidn = "";
		
		BaseResult bs = new BaseResult();
		
		log.trace(ESAPIUtil.vaildLog("pay_taxes_confirm.reqParam: {}"+CodeUtil.toJson(reqParam)));
		
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		
		try {
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("pay_taxes_confirm.jsondc: " + jsondc));
			
			okMap.put("jsondc", jsondc); // IKEY要用不經過ESAPIUtil，會出錯
			okMap.put("LOGINTYPE", "NB"); // 系統別
			log.trace(ESAPIUtil.vaildLog("pay_taxes_confirm.okMap: {}"+ okMap));
			
			
			// 回上一頁重新賦值
			if( okMap.containsKey("previous") ){
				Map<String, String> backMap = (Map<String, String>) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				okMap = backMap;
			}
			
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale: {}"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				if( !bs.getResult())
				{
//					TODO 交易結束後，CONFIRM_LOCALE_DATA 會被清空，造成在URL輸入確認頁會導向錯誤頁，原因待查
					log.trace("bs.getResult(): {}", bs.getResult());
					throw new Exception();
				}else{
					// session 資料放入 map 讓後續 model 和回上一頁取值
					okMap.putAll((Map<String, String>) bs.getData());
				}
			}else {
				// 驗證TOKEN 沒TOKEN會到錯誤頁，錯誤頁確認後返回輸入頁
				String token = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null);
				log.trace("pay_taxes_confirm.token: {}", token);
				if(StrUtil.isNotEmpty(okMap.get("TOKEN"))  &&  okMap.get("TOKEN").equals(token)) {
					bs.setResult(Boolean.TRUE);
				}
				log.trace("bs.getResult(): {}",bs.getResult());
				if(!bs.getResult()) {
					log.trace("throw new Exception()");
					throw new Exception();
				}
				
				// TOKEN驗證成功，資料處理後進入確認頁
				bs.reset();
				cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
				bs = pay_expense_service.pay_taxes_confirm(okMap, cusidn);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
			}
			
			// IDGATE transdata
			Map<String, Object> IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
			String adopid = "N171";
    		String title = "您有一筆繳稅交易待確認，金額"+((Map<String, String>) bs.getData()).get("AMOUNT");
			if (IdgateData != null) {
            	model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
	            if(IdgateData.get("idgateUserFlag").equals("Y")) {		 
	            	
	            	Map<String, String> idgateMap = new HashMap<String, String>();
	            	idgateMap.putAll(okMap);
	            	idgateMap.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
	            	idgateMap.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
	            	idgateMap.put("CUSIDN", cusidn);
	            	idgateMap.put("AMOUNT_VIEW", okMap.get("AMOUNT"));
	            	//20211026 修正  如果在 TAXTYPE1 為11的情況下 在N171.java內會將 PAYDUED 轉換 , 造成FE0011 錯誤
	            	//故照N171方式修改 , 並將資料切開 須同步修改N171_IDGATE_DATA 及 N171_IDGATE_DATA_VIEW
	            	idgateMap.put("PAYDUED_VIEW", okMap.get("PAYDUED"));
	            	if("11".equals(okMap.get("TAXTYPE1"))) {
	            		String paydue = okMap.get("PAYDUED");
	        			paydue = StrUtils.trim(paydue).replaceAll("/", "");   // 將 yyy/MM/dd 的 "/" 去掉, 變成 yyyMMdd
	        			idgateMap.put("PAYDUED", StrUtils.right(paydue, 6));
	            	}
	            	idgateMap.put("AMOUNT", NumericUtil.unfmtAmount(okMap.get("AMOUNT")));
	            	IdgateData.put("N171_IDGATE", idgateservice.coverTxnData(N171_IDGATE_DATA.class, N171_IDGATE_DATA_VIEW.class, idgateMap));
	            	
	            	SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	            }
            }
            
            model.addAttribute("idgateAdopid", adopid);
            model.addAttribute("idgateTitle", title);

            
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("pay_taxes_confirm error >> {}",e);
		}finally {
			if(bs.getResult()) {
				target = "/pay_expense/pay_taxes_confirm";
//			TODO 這邊還要調整 不能存物件，要存成JSNO字串
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, ((Map<String,Object>)bs.getData()).get("TXTOKEN"));
				
				// 設定回上一頁的url,配合confirm頁面的js
				model.addAttribute("previous","/PAY/EXPENSE/pay_taxes");
				// 回填使用DATA
				SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, okMap);
			}else {
				// 到錯誤頁，錯誤頁的確認按鈕導頁到輸入頁
				bs.setNext("/PAY/EXPENSE/pay_taxes");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * 進入N171-繳稅_結果頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/pay_taxes_result")
	public String pay_taxes_result(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("pay_taxes_result...");
		log.trace(ESAPIUtil.vaildLog("reqParam {}" +CodeUtil.toJson(reqParam)));

		String target = "/error";
		String next = "/PAY/EXPENSE/pay_taxes";
		String previous = "/PAY/EXPENSE/pay_taxes_confirm";

		BaseResult bs = null;
		try {
			// 初始化BaseResult
			bs = new BaseResult();
			
			// 取得CUSIDN明碼
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//			Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			
			// 判斷LOCAL KEY是否有帶值，有帶表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			log.trace("pay_taxes_result.locale: " + hasLocale);
			if(hasLocale){
				log.trace("pay_taxes_result.hasLocale...");
				// 判斷從session取出的資料物件是否為BaseResult物件
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( !bs.getResult() ){
					log.trace("pay_taxes_result.bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			// 判斷LOCAL KEY沒有帶值則驗證TXTOKEN，驗證通過才可做交易
			if(!hasLocale){
				log.trace("pay_taxes_result.validate TXTOKEN...");
				log.trace("pay_taxes_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("pay_taxes_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("pay_taxes_result.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("pay_taxes_result TXTOKEN 不正確，TXTOKEN>>{}"+ okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						!okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("pay_taxes_result.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("pay_taxes_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				
				
				// 取得從輸入頁輸入存在session中的輸入資料
				bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
				Map<String,String> transfer_data = (Map<String, String>) bs.getData();
				
				// 取得SESSION中的輸入資料，加入到確認頁的交易機制資料
				okMap.putAll(transfer_data);
				
				// 重置bs，進行交易
				bs.reset();
				
				// IDgate驗證
				try {
					if (okMap.get("FGTXWAY").equals("7")) {
						Map<String, Object> IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
						IdgateData = (Map<String, Object>) IdgateData.get("N171_IDGATE");
						okMap.put("sessionID", (String) IdgateData.get("sessionid"));
						okMap.put("txnID", (String) IdgateData.get("txnID"));
						okMap.put("idgateID", (String) IdgateData.get("IDGATEID"));
					}
				} catch (Exception e) {
					log.error("IDGATE ValidateFail>>{}", bs.getResult());
				}
				
				bs = pay_expense_service.pay_taxes_result(cusidn, okMap);
				
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
				model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
			}
			model.addAttribute("transfer_result_data", bs);
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("pay_taxes_result error >> {}",e);
		} finally {
//			TODO 要清除必要的SESSION
			
			// 解決Trust Boundary Violation
//			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			
			// 交易成功要存之前的token 在Session 以利下次比對是否重複交易
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TXTOKEN"));
			if(bs.getResult()) {
				target = "/pay_expense/pay_taxes_result";
			}else {
				bs.setNext(next);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	
	/**
	 * 進入N073-繳納期貨保證金(限本行)_輸入頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/futures_deposit")
	public String futures_deposit(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		log.trace("futures_deposit...");
		BaseResult bs = null;
		
		try {
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
		
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);

			// 是否是回上一頁
			if("Y".equals(okMap.get("back"))) {
				model.addAttribute("isBack","Y");
				Map<String, String> previousData = (Map) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				
				// DPAGACNO 會有雙引號所以JSON.parse會有問題
				model.addAttribute("DPAGACNO_BACK", previousData.get("DPAGACNO"));
				previousData.remove("DPAGACNO");
				model.addAttribute("previousdata", CodeUtil.toJson(previousData));
				log.trace("previousdata: {}", CodeUtil.toJson(previousData));
			}
			
			// 設定頁面預約日期為明天，如果當前時間太晚則預約後天
			model.addAttribute("tmrDate", DateUtil.getTomorrowOrAfterTomorrowDate());
			
			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = acct_transfer_service.getTxToken();
			log.trace("futures_deposit.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			if(bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			}
			
			// 登入時的身分證字號
			String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				// 解密成明碼
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				log.trace("futures_deposit.cusidn: " + cusidn);
				
				// 轉出成功是否Email通知
				boolean chk_result = pay_expense_service.chkContains(cusidn);
				model.addAttribute("sendMe", chk_result);
				
			}else {
				log.error("session no cusidn!!!");
			}
			
		  //IDGATE身分
           String idgateUserFlag="N";		 
           Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
           try {		 
        	   if(IdgateData==null) {
        		   IdgateData = new HashMap<String, Object>();
        	   }
               BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
               if(tmp.getResult()) {		 
                   idgateUserFlag="Y";                  		 
               }		 
               tmp.addData("idgateUserFlag",idgateUserFlag);		 
               IdgateData.putAll((Map<String, String>) tmp.getData());
               SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
           }catch(Exception e) {		 
               log.debug("idgateUserFlag error {}",e);		 
           }		 
           model.addAttribute("idgateUserFlag",idgateUserFlag);
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("futures_deposit error >> {}",e);
		} finally {
			if(bs!=null && bs.getResult()) {
				target = "/pay_expense/futures_deposit";
				model.addAttribute("futures_deposit", bs);
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
				
			}
		}
		return target;
	}
	
	/**
	 * 進入N073-繳納期貨保證金(限本行)_確認頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/futures_deposit_confirm")
	public String futures_deposit_confirm(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("futures_deposit_confirm...");
		
		String target = "/error";
		String jsondc = "";
		BaseResult bs = new BaseResult();
		
		log.trace(ESAPIUtil.vaildLog("futures_deposit_confirm.reqParam: {}"+CodeUtil.toJson(reqParam)));
		
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		
		try {
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("futures_deposit_confirm.jsondc: " + jsondc));
			
			okMap.put("jsondc", jsondc); // IKEY要用不經過ESAPIUtil，會出錯
			okMap.put("LOGINTYPE", "NB"); // 系統別
			log.trace(ESAPIUtil.vaildLog("futures_deposit_confirm.okMap: {}"+ okMap));
			
			// 回上一頁重新賦值
			if( okMap.containsKey("previous") ){
				Map<String, String> backMap = (Map<String, String>) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				okMap = backMap;
			}
			
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale: {}"+okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				if( !bs.getResult())
				{
//					TODO 交易結束後，CONFIRM_LOCALE_DATA 會被清空，造成在URL輸入確認頁會導向錯誤頁，原因待查
					log.trace("bs.getResult(): {}", bs.getResult());
					throw new Exception();
				}else{
					// session 資料放入 map 讓後續 model 和回上一頁取值
					okMap.putAll((Map<String, String>) bs.getData());
				}
			}else {
				// 驗證TOKEN 沒TOKEN會到錯誤頁，錯誤頁確認後返回輸入頁
				String token = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null);
				log.trace("futures_deposit_confirm.token: {}", token);
				if(StrUtil.isNotEmpty(okMap.get("TOKEN"))  &&  okMap.get("TOKEN").equals(token)) {
					bs.setResult(Boolean.TRUE);
				}
				log.trace("bs.getResult(): {}",bs.getResult());
				if(!bs.getResult()) {
					log.trace("throw new Exception()");
					throw new Exception();
				}
				
				// TOKEN驗證成功，資料處理後進入確認頁
				bs.reset();
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
				bs = pay_expense_service.futures_deposit_confirm(okMap, cusidn);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
				
				// IDGATE transdata       
				Map<String,String> result = new HashMap();
				result.putAll((Map< String,String>) bs.getData());
				String AMOUNT = result.get("AMOUNT");
				String transfer_date = result.get("transfer_date");
				pay_expense_service.preProcessing(result);
				result.put("IDGATE_AMOUNT", AMOUNT);
				result.put("transfer_date", transfer_date);
	            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
	    		String adopid = "N073";
	    		String title = "您有一筆 繳費-繳納期貨保證金 待確認，金額 " + result.get("IDGATE_AMOUNT");
	        	if(IdgateData != null) {
		            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
		            if(IdgateData.get("idgateUserFlag").equals("Y")) {
		            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
		            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
		            	// 轉出帳號
		            	result.put("ACN", (String)result.get("OUTACN"));
		            	// 期貨帳號
		            	result.put("INTSACN", (String)result.get("DPACNO"));
		            	
		            	String trin_agree = "";
		            	String fgsvacno = result.get("FGSVACNO");
		        		if("1".equals(fgsvacno)) {
		        			trin_agree = result.get("AGREE");
		        		}
		        		else if("2".equals(fgsvacno)) {  //非約定
		        			trin_agree = "0";   // 0: 非約定, 1: 約定
		        		}
		        		result.put("FLAG", trin_agree);
		            	
		            	
						IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N073_IDGATE_DATA.class, N073_IDGATE_DATA_VIEW.class, result));
		                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
		            }
	        	}
	            model.addAttribute("idgateAdopid", adopid);
	            model.addAttribute("idgateTitle", title);
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("futures_deposit_confirm error >> {}",e);
		}finally {
			if(bs.getResult()) {
				target = "/pay_expense/futures_deposit_confirm";
//			TODO 這邊還要調整 不能存物件，要存成JSNO字串
				log.debug("futures_deposit_confirm.TRANSFER_DATA: {}", CodeUtil.toJson(bs));
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, ((Map<String,Object>)bs.getData()).get("TXTOKEN"));
				
				// 設定回上一頁的url,配合confirm頁面的js
				model.addAttribute("previous","/PAY/EXPENSE/futures_deposit");
				// 回填使用DATA
				SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, okMap);
			}else {
				// 到錯誤頁，錯誤頁的確認按鈕導頁到輸入頁
				bs.setNext("/PAY/EXPENSE/futures_deposit");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * 進入N073-繳納期貨保證金(限本行)_結果頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/futures_deposit_result")
	public String futures_deposit_result(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("futures_deposit_result...");
		log.trace(ESAPIUtil.vaildLog("reqParam {}" +CodeUtil.toJson(reqParam)));

		String target = "/error";
		String next = "/PAY/EXPENSE/futures_deposit";
		String previous = "/PAY/EXPENSE/futures_deposit_confirm";

		BaseResult bs = null;
		try {
			// 初始化BaseResult
			bs = new BaseResult();
			
			// 取得CUSIDN明碼
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//			Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			
			// 判斷LOCAL KEY是否有帶值，有帶表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			log.trace("futures_deposit_result.locale: " + hasLocale);
			if(hasLocale){
				log.trace("futures_deposit_result.hasLocale...");
				// 判斷從session取出的資料物件是否為BaseResult物件
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( !bs.getResult() ){
					log.trace("futures_deposit_result.bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			// 判斷LOCAL KEY沒有帶值則驗證TXTOKEN，驗證通過才可做交易
			if(!hasLocale){
				log.trace("futures_deposit_result.validate TXTOKEN...");
				log.trace("futures_deposit_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("futures_deposit_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("futures_deposit_result.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("futures_deposit_result TXTOKEN 不正確，TXTOKEN>>{}"+ okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						!okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("futures_deposit_result.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("futures_deposit_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				
				
				// 取得從輸入頁輸入存在session中的輸入資料
				bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
				Map<String,String> transfer_data = (Map<String, String>) bs.getData();
				
				// 取得SESSION中的輸入資料，加入到確認頁的交易機制資料
				okMap.putAll(transfer_data);
				
				// 重置bs，進行交易
				bs.reset();
				
				try {		 
                    if(okMap.get("FGTXWAY").equals("7")) {	
        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N073_IDGATE");
                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
                    }
                }catch(Exception e) {		 
                    log.error("IDGATE ValidateFail>>{}",bs.getResult());
                }
				bs = pay_expense_service.futures_deposit_result(cusidn, okMap);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
			model.addAttribute("transfer_result_data", bs);
			Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
			model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("futures_deposit_result error >> {}",e);
		} finally {
//			TODO 要清除必要的SESSION
			
			// 解決Trust Boundary Violation
//			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			
			// 交易成功要存之前的token 在Session 以利下次比對是否重複交易
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TXTOKEN"));
			if(bs.getResult()) {
				target = "/pay_expense/futures_deposit_result";
			}else {
				bs.setNext(next);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	
	/**
	 * 進入N075-電費_輸入頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/electricity_fee")
	public String electricity_fee(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		log.trace("electricity_fee...");
		BaseResult bs = null;
		
		try {
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
		
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);

			// 是否是回上一頁
			if("Y".equals(okMap.get("back"))) {
				model.addAttribute("isBack","Y");
				Map<String, String> previousData = (Map) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				model.addAttribute("previousdata", CodeUtil.toJson(previousData));
				log.trace("previousdata: {}", CodeUtil.toJson(previousData));
			}
			
			// 設定頁面預約日期為明天，如果當前時間太晚則預約後天
			model.addAttribute("tmrDate", DateUtil.getTomorrowOrAfterTomorrowDate());
			
			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = acct_transfer_service.getTxToken();
			log.trace("electricity_fee.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			if(bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			}
			
			// 登入時的身分證字號
			String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				// 解密成明碼
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				log.trace("electricity_fee.cusidn: " + cusidn);
				
				// 轉出成功是否Email通知
				boolean chk_result = pay_expense_service.chkContains(cusidn);
				model.addAttribute("sendMe", chk_result);
				
	           //IDGATE身分
	           String idgateUserFlag="N";		 
	           Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
	           try {		 
	        	   if(IdgateData==null) {
	        		   IdgateData = new HashMap<String, Object>();
	        	   }
	               BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
	               if(tmp.getResult()) {		 
	                   idgateUserFlag="Y";                  		 
	               }		 
	               tmp.addData("idgateUserFlag",idgateUserFlag);		 
	               IdgateData.putAll((Map<String, String>) tmp.getData());
	               SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	           }catch(Exception e) {		 
	               log.debug("idgateUserFlag error {}",e);		 
	           }		 
	           model.addAttribute("idgateUserFlag",idgateUserFlag);
			}else {
				log.error("session no cusidn!!!");
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("electricity_fee error >> {}",e);
		} finally {
			if(bs!=null && bs.getResult()) {
				target = "/pay_expense/electricity_fee";
				model.addAttribute("electricity_fee", bs);
				
				// 表單驗證用
				model.addAttribute("str_SystemDate", DateUtil.getNextDay());
				model.addAttribute("sNextYearDay", DateUtil.getNextYearDay());
				model.addAttribute("str_Today", DateUtil.getDate("/"));
				
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
				
			}
		}
		return target;
	}
	
	/**
	 * 進入N075-電費_確認頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/electricity_fee_confirm")
	public String electricity_fee_confirm(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("electricity_fee_confirm...");
		
		String target = "/error";
		String jsondc = "";
		BaseResult bs = new BaseResult();
		
		log.trace(ESAPIUtil.vaildLog("electricity_fee_confirm.reqParam: {}"+CodeUtil.toJson(reqParam)));
		
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		
		try {
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("electricity_fee_confirm.jsondc: " + jsondc));
			
			okMap.put("jsondc", jsondc); // IKEY要用不經過ESAPIUtil，會出錯
			okMap.put("LOGINTYPE", "NB"); // 系統別
			log.trace(ESAPIUtil.vaildLog("electricity_fee_confirm.okMap: {}"+ okMap));
			
			
			
			// 回上一頁重新賦值
			if( okMap.containsKey("previous") ){
				Map<String, String> backMap = (Map<String, String>) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				okMap = backMap;
			}
			
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale: {}"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				if( !bs.getResult())
				{
//					TODO 交易結束後，CONFIRM_LOCALE_DATA 會被清空，造成在URL輸入確認頁會導向錯誤頁，原因待查
					log.trace("bs.getResult(): {}", bs.getResult());
					throw new Exception();
				}else{
					// session 資料放入 map 讓後續 model 和回上一頁取值
					okMap.putAll((Map<String, String>) bs.getData());
				}
			}else {
				// 驗證TOKEN 沒TOKEN會到錯誤頁，錯誤頁確認後返回輸入頁
				String token = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null);
				log.trace("electricity_fee_confirm.token: {}", token);
				if(StrUtil.isNotEmpty(okMap.get("TOKEN"))  &&  okMap.get("TOKEN").equals(token)) {
					bs.setResult(Boolean.TRUE);
				}
				log.trace("bs.getResult(): {}",bs.getResult());
				if(!bs.getResult()) {
					log.trace("throw new Exception()");
					throw new Exception();
				}
				
				// TOKEN驗證成功，資料處理後進入確認頁
				bs.reset();
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
				bs = pay_expense_service.electricity_fee_confirm(okMap, cusidn);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
				
				// IDGATE transdata        
				Map<String,String> result = new HashMap();
				result.putAll((Map< String,String>) bs.getData());
				String LIMDAT = new String(result.get("LIMDAT"));
				String AMOUNT = result.get("AMOUNT");
				String transfer_date = result.get("transfer_date");
				pay_expense_service.preProcessing(result);
				result.put("IDGATE_AMOUNT", AMOUNT);
				result.put("transfer_date", transfer_date);
				result.put("IDGATE_LIMDAT", LIMDAT);
	            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
	    		String adopid = "N075";
	    		String title = "您有一筆 繳費-臺灣電力公司 待確認，金額"+result.get("IDGATE_AMOUNT");
	        	if(IdgateData != null) {
		            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
		            if(IdgateData.get("idgateUserFlag").equals("Y")) {
		            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
		            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
						IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N075_IDGATE_DATA.class, N075_IDGATE_DATA_VIEW.class, result));
		                }
		                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
		            }
	            model.addAttribute("idgateAdopid", adopid);
	            model.addAttribute("idgateTitle", title);
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("electricity_fee_confirm error >> {}",e);
		}finally {
			if(bs.getResult()) {
				target = "/pay_expense/electricity_fee_confirm";
//			TODO 這邊還要調整 不能存物件，要存成JSNO字串
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, ((Map<String,Object>)bs.getData()).get("TXTOKEN"));
				
				// 設定回上一頁的url,配合confirm頁面的js
				model.addAttribute("previous","/PAY/EXPENSE/electricity_fee");
				// 回填使用DATA
				SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, okMap);
			}else {
				// 到錯誤頁，錯誤頁的確認按鈕導頁到輸入頁
				bs.setNext("/PAY/EXPENSE/electricity_fee");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * 進入N075-電費_結果頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/electricity_fee_result")
	public String electricity_fee_result(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("electricity_fee_result...");
		log.trace(ESAPIUtil.vaildLog("reqParam {}" +CodeUtil.toJson(reqParam)));

		String target = "/error";
		String next = "/PAY/EXPENSE/electricity_fee";
		String previous = "/PAY/EXPENSE/electricity_fee_confirm";

		BaseResult bs = null;
		try {
			// 初始化BaseResult
			bs = new BaseResult();
			
			// 取得CUSIDN明碼
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//			Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			
			// 判斷LOCAL KEY是否有帶值，有帶表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			log.trace("electricity_fee_result.locale: " + hasLocale);
			if(hasLocale){
				log.trace("electricity_fee_result.hasLocale...");
				// 判斷從session取出的資料物件是否為BaseResult物件
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( !bs.getResult() ){
					log.trace("electricity_fee_result.bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			// 判斷LOCAL KEY沒有帶值則驗證TXTOKEN，驗證通過才可做交易
			if(!hasLocale){
				log.trace("electricity_fee_result.validate TXTOKEN...");
				log.trace("electricity_fee_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("electricity_fee_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("electricity_fee_result.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("electricity_fee_result TXTOKEN 不正確，TXTOKEN>>{}"+ okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						!okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("electricity_fee_result.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("electricity_fee_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				
				
				// 取得從輸入頁輸入存在session中的輸入資料
				bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
				Map<String,String> transfer_data = (Map<String, String>) bs.getData();
				
				// 取得SESSION中的輸入資料，加入到確認頁的交易機制資料
				okMap.putAll(transfer_data);
				
				// 重置bs，進行交易
				bs.reset();
				
				//IDgate驗證		 
                try {		 
                    if(okMap.get("FGTXWAY").equals("7")) {		 
        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N075_IDGATE");
                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));	
                    }
                }catch(Exception e) {		 
                    log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
                }  
                bs = pay_expense_service.electricity_fee_result(cusidn, okMap);  					
				
				
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
				model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
			}
			model.addAttribute("transfer_result_data", bs);
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("electricity_fee_result error >> {}",e);
		} finally {
//			TODO 要清除必要的SESSION
			
			// 解決Trust Boundary Violation
//			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			
			// 交易成功要存之前的token 在Session 以利下次比對是否重複交易
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TXTOKEN"));
			if(bs.getResult()) {
				target = "/pay_expense/electricity_fee_result";
			}else {
				bs.setNext(next);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	

	/**
	 * 進入N750A-臺灣省水費_輸入頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/water_tw_fee")
	public String water_tw_fee(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		log.trace("water_tw_fee...");
		BaseResult bs = null;
		
		try {
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
		
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);

			// 是否是回上一頁
			if("Y".equals(okMap.get("back"))) {
				model.addAttribute("isBack","Y");
				Map<String, String> previousData = (Map) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				model.addAttribute("previousdata", CodeUtil.toJson(previousData));
				log.trace("previousdata: {}", CodeUtil.toJson(previousData));
			}
			
			// 設定頁面預約日期為明天，如果當前時間太晚則預約後天
			model.addAttribute("tmrDate", DateUtil.getTomorrowOrAfterTomorrowDate());
			
			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = acct_transfer_service.getTxToken();
			log.trace("water_tw_fee.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			if(bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			}
			
			// 登入時的身分證字號
			String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				// 解密成明碼
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				log.trace("water_tw_fee.cusidn: " + cusidn);
				
				// 轉出成功是否Email通知
				boolean chk_result = pay_expense_service.chkContains(cusidn);
				model.addAttribute("sendMe", chk_result);
				
				
	           //IDGATE身分
	           String idgateUserFlag="N";		 
	           Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
	           try {		 
	        	   if(IdgateData==null) {
	        		   IdgateData = new HashMap<String, Object>();
	        	   }
	               BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
	               if(tmp.getResult()) {		 
	                   idgateUserFlag="Y";                  		 
	               }		 
	               tmp.addData("idgateUserFlag",idgateUserFlag);		 
	               IdgateData.putAll((Map<String, String>) tmp.getData());
	               SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	           }catch(Exception e) {		 
	               log.debug("idgateUserFlag error {}",e);		 
	           }		 
	           model.addAttribute("idgateUserFlag",idgateUserFlag);
		 
				
			}else {
				log.error("session no cusidn!!!");
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("water_tw_fee error >> {}",e);
		} finally {
			if(bs!=null && bs.getResult()) {
				target = "/pay_expense/water_tw_fee";
				model.addAttribute("water_tw_fee", bs);
				
				// 表單驗證用
				model.addAttribute("str_SystemDate", DateUtil.getNextDay());
				model.addAttribute("sNextYearDay", DateUtil.getNextYearDay());
				model.addAttribute("str_Today", DateUtil.getDate("/"));
				
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
				
			}
		}
		return target;
	}
	
	/**
	 * 進入N750A-臺灣省水費_確認頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/water_tw_fee_confirm")
	public String water_tw_fee_confirm(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("water_tw_fee_confirm...");
		
		String target = "/error";
		String jsondc = "";
		BaseResult bs = new BaseResult();
		log.trace(ESAPIUtil.vaildLog("water_tw_fee_confirm.reqParam: {}"+CodeUtil.toJson(reqParam)));
		
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		
		try {
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("water_tw_fee_confirm.jsondc: " + jsondc));
			
			okMap.put("jsondc", jsondc);
			okMap.put("LOGINTYPE", "NB"); // 系統別
			log.trace(ESAPIUtil.vaildLog("water_tw_fee_confirm.okMap: {}"+ okMap));
			
			
			// 回上一頁重新賦值
			if( okMap.containsKey("previous") ){
				Map<String, String> backMap = (Map<String, String>) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				okMap = backMap;
			}
			
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale: {}"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				if( !bs.getResult())
				{
//					TODO 交易結束後，CONFIRM_LOCALE_DATA 會被清空，造成在URL輸入確認頁會導向錯誤頁，原因待查
					log.trace("bs.getResult(): {}", bs.getResult());
					throw new Exception();
				}else{
					// session 資料放入 map 讓後續 model 和回上一頁取值
					okMap.putAll((Map<String, String>) bs.getData());
				}
			}else {
				// 驗證TOKEN 沒TOKEN會到錯誤頁，錯誤頁確認後返回輸入頁
				String token = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null);
				log.trace("water_tw_fee_confirm.token: {}", token);
				if(StrUtil.isNotEmpty(okMap.get("TOKEN"))  &&  okMap.get("TOKEN").equals(token)) {
					bs.setResult(Boolean.TRUE);
				}
				log.trace("bs.getResult(): {}",bs.getResult());
				if(!bs.getResult()) {
					log.trace("throw new Exception()");
					throw new Exception();
				}
				
				// TOKEN驗證成功，資料處理後進入確認頁
				bs.reset();
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
				bs = pay_expense_service.water_tw_fee_confirm(okMap, cusidn);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
				
				// IDGATE transdata    
				Map<String,String> result = new HashMap();
				result.putAll((Map< String,String>) bs.getData());
				String PAYDUE = result.get("PAYDUE");
				String AMOUNT = result.get("AMOUNT");
				String transfer_date = result.get("transfer_date");
				pay_expense_service.preProcessing(result);
				result.put("IDGATE_AMOUNT", AMOUNT);
				result.put("transfer_date", transfer_date);
				result.put("IDGATE_PAYDUE", PAYDUE);
	            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
	    		String adopid = "N750A";
	    		String title = "您有一筆 繳費-臺灣自來水公司 待確認，金額"+result.get("IDGATE_AMOUNT");;
	        	if(IdgateData != null) {
		            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
		            if(IdgateData.get("idgateUserFlag").equals("Y")) {
		            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
		            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
		            	IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N750A_IDGATE_DATA.class, N750A_IDGATE_DATA_VIEW.class, result));
		                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
		            }
	        	}
	            model.addAttribute("idgateAdopid", adopid);
	            model.addAttribute("idgateTitle", title);
	            
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("water_tw_fee_confirm error >> {}",e);
		}finally {
			if(bs.getResult()) {
				target = "/pay_expense/water_tw_fee_confirm";
//			TODO 這邊還要調整 不能存物件，要存成JSNO字串
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, ((Map<String,Object>)bs.getData()).get("TXTOKEN"));
				
				// 設定回上一頁的url,配合confirm頁面的js
				model.addAttribute("previous","/PAY/EXPENSE/water_tw_fee");
				// 回填使用DATA
				SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, okMap);
			}else {
				// 到錯誤頁，錯誤頁的確認按鈕導頁到輸入頁
				bs.setNext("/PAY/EXPENSE/water_tw_fee");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * 進入N750A-臺灣省水費_結果頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/water_tw_fee_result")
	public String water_tw_fee_result(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("water_tw_fee_result...");
		log.trace(ESAPIUtil.vaildLog("reqParam {}"  +CodeUtil.toJson(reqParam)));

		String target = "/error";
		String next = "/PAY/EXPENSE/water_tw_fee";
		String previous = "/PAY/EXPENSE/water_tw_fee_confirm";

		BaseResult bs = null;
		try {
			// 初始化BaseResult
			bs = new BaseResult();
			
			// 取得CUSIDN明碼
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//			Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			
			// 判斷LOCAL KEY是否有帶值，有帶表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			log.trace("water_tw_fee_result.locale: " + hasLocale);
			if(hasLocale){
				log.trace("water_tw_fee_result.hasLocale...");
				// 判斷從session取出的資料物件是否為BaseResult物件
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( !bs.getResult() ){
					log.trace("water_tw_fee_result.bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			// 判斷LOCAL KEY沒有帶值則驗證TXTOKEN，驗證通過才可做交易
			if(!hasLocale){
				log.trace("water_tw_fee_result.validate TXTOKEN...");
				log.trace("water_tw_fee_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("water_tw_fee_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("water_tw_fee_result.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("water_tw_fee_result TXTOKEN 不正確，TXTOKEN>>{}"+ okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						!okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("water_tw_fee_result.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("water_tw_fee_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				
				
				// 取得從輸入頁輸入存在session中的輸入資料
				bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
				Map<String,String> transfer_data = (Map<String, String>) bs.getData();
				
				// 取得SESSION中的輸入資料，加入到確認頁的交易機制資料
				okMap.putAll(transfer_data);
				
				// 重置bs，進行交易
				bs.reset();
				
				 //IDgate驗證		 
				try {		 
                    if(okMap.get("FGTXWAY").equals("7")) {	
        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N750A_IDGATE");
                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));	
                    }
                }catch(Exception e) {		 
                    log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
                } 
				
                bs = pay_expense_service.water_tw_fee_result(cusidn, okMap);
				
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
				model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
			}
			model.addAttribute("transfer_result_data", bs);
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("water_tw_fee_result error >> {}",e);
		} finally {
//			TODO 要清除必要的SESSION
			
			// 解決Trust Boundary Violation
//			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			
			// 交易成功要存之前的token 在Session 以利下次比對是否重複交易
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TXTOKEN"));
			if(bs.getResult()) {
				target = "/pay_expense/water_tw_fee_result";
			}else {
				bs.setNext(next);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	

	/**
	 * 進入N750B-台北市水費_輸入頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/water_tpe_fee")
	public String water_tpe_fee(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		log.trace("water_tpe_fee...");
		BaseResult bs = null;
		
		try {
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
		
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);

			// 是否是回上一頁
			if("Y".equals(okMap.get("back"))) {
				model.addAttribute("isBack","Y");
				Map<String, String> previousData = (Map) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				model.addAttribute("previousdata", CodeUtil.toJson(previousData));
				log.trace("previousdata: {}", CodeUtil.toJson(previousData));
			}
			
			// 設定頁面預約日期為明天，如果當前時間太晚則預約後天
			model.addAttribute("tmrDate", DateUtil.getTomorrowOrAfterTomorrowDate());
			
			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = acct_transfer_service.getTxToken();
			log.trace("water_tpe_fee.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			if(bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			}
			
			// 登入時的身分證字號
			String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				// 解密成明碼
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				log.trace("water_tpe_fee.cusidn: " + cusidn);
				
				// 轉出成功是否Email通知
				boolean chk_result = pay_expense_service.chkContains(cusidn);
				model.addAttribute("sendMe", chk_result);
				
			}else {
				log.error("session no cusidn!!!");
			}
			
			//IDGATE身分
	           String idgateUserFlag="N";		 
	           Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
	           try {		 
	        	   if(IdgateData==null) {
	        		   IdgateData = new HashMap<String, Object>();
	        	   }
	               BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
	               if(tmp.getResult()) {		 
	                   idgateUserFlag="Y";                  		 
	               }		 
	               tmp.addData("idgateUserFlag",idgateUserFlag);		 
	               IdgateData.putAll((Map<String, String>) tmp.getData());
	               SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	           }catch(Exception e) {		 
	               log.debug("idgateUserFlag error {}",e);		 
	           }		 
	           model.addAttribute("idgateUserFlag",idgateUserFlag);
	           
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("water_tpe_fee error >> {}",e);
		} finally {
			if(bs!=null && bs.getResult()) {
				target = "/pay_expense/water_tpe_fee";
				model.addAttribute("water_tpe_fee", bs);
				
				// 表單驗證用
				model.addAttribute("str_SystemDate", DateUtil.getNextDay());
				model.addAttribute("sNextYearDay", DateUtil.getNextYearDay());
				model.addAttribute("str_Today", DateUtil.getDate("/"));
				
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
				
			}
		}
		return target;
	}
	
	/**
	 * 進入N750B-臺灣省水費_確認頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/water_tpe_fee_confirm")
	public String water_tpe_fee_confirm(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("water_tpe_fee_confirm...");
		
		String target = "/error";
		String jsondc = "";
		BaseResult bs = new BaseResult();
		
		log.trace(ESAPIUtil.vaildLog("water_tpe_fee_confirm.reqParam: {}"+CodeUtil.toJson(reqParam)));
		
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		
		try {
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("water_tpe_fee_confirm.jsondc: " + jsondc));
			
			okMap.put("jsondc", jsondc);
			okMap.put("LOGINTYPE", "NB"); // 系統別
			log.trace(ESAPIUtil.vaildLog("water_tpe_fee_confirm.okMap: {}"+ okMap));
			
			
			// 回上一頁重新賦值
			if( okMap.containsKey("previous") ){
				Map<String, String> backMap = (Map<String, String>) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				okMap = backMap;
			}
			
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale: {}"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				if( !bs.getResult())
				{
//					TODO 交易結束後，CONFIRM_LOCALE_DATA 會被清空，造成在URL輸入確認頁會導向錯誤頁，原因待查
					log.trace("bs.getResult(): {}", bs.getResult());
					throw new Exception();
				}else{
					// session 資料放入 map 讓後續 model 和回上一頁取值
					okMap.putAll((Map<String, String>) bs.getData());
				}
			}else {
				// 驗證TOKEN 沒TOKEN會到錯誤頁，錯誤頁確認後返回輸入頁
				String token = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null);
				log.trace("water_tpe_fee_confirm.token: {}", token);
				if(StrUtil.isNotEmpty(okMap.get("TOKEN"))  &&  okMap.get("TOKEN").equals(token)) {
					bs.setResult(Boolean.TRUE);
				}
				log.trace("bs.getResult(): {}",bs.getResult());
				if(!bs.getResult()) {
					log.trace("throw new Exception()");
					throw new Exception();
				}
				
				// TOKEN驗證成功，資料處理後進入確認頁
				bs.reset();
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
				bs = pay_expense_service.water_tpe_fee_confirm(okMap, cusidn);
				
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
				
				// IDGATE transdata    
				Map<String,String> result = new HashMap();
				result.putAll((Map< String,String>) bs.getData());
				String AMOUNT = result.get("AMOUNT");
				String transfer_date = result.get("transfer_date");
				pay_expense_service.preProcessing(result);
				result.put("IDGATE_AMOUNT", AMOUNT);
				result.put("transfer_date", transfer_date);
	            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
	    		String adopid = "N750B";
	    		String title = "您有一筆 繳費-臺北自來水事業處 待確認，金額"+result.get("IDGATE_AMOUNT");
	        	if(IdgateData != null) {
		            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
		            if(IdgateData.get("idgateUserFlag").equals("Y")) {
		            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
		            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
							IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N750B_IDGATE_DATA.class, N750B_IDGATE_DATA_VIEW.class,result));
		                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
		            }
	        	}
	            model.addAttribute("idgateAdopid", adopid);
	            model.addAttribute("idgateTitle", title);
			}

			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("water_tpe_fee_confirm error >> {}",e);
		}finally {
			if(bs.getResult()) {
				target = "/pay_expense/water_tpe_fee_confirm";
//			TODO 這邊還要調整 不能存物件，要存成JSNO字串
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, ((Map<String,Object>)bs.getData()).get("TXTOKEN"));
				
				// 設定回上一頁的url,配合confirm頁面的js
				model.addAttribute("previous","/PAY/EXPENSE/water_tpe_fee");
				// 回填使用DATA
				SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, okMap);
			}else {
				// 到錯誤頁，錯誤頁的確認按鈕導頁到輸入頁
				bs.setNext("/PAY/EXPENSE/water_tpe_fee");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * 進入N750B-臺灣省水費_結果頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/water_tpe_fee_result")
	public String water_tpe_fee_result(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("water_tpe_fee_result...");
		log.trace(ESAPIUtil.vaildLog("reqParam {}" +CodeUtil.toJson(reqParam)));

		String target = "/error";
		String next = "/PAY/EXPENSE/water_tpe_fee";
		String previous = "/PAY/EXPENSE/water_tpe_fee_confirm";

		BaseResult bs = null;
		try {
			// 初始化BaseResult
			bs = new BaseResult();
			
			// 取得CUSIDN明碼
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
     //		Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			
			// 判斷LOCAL KEY是否有帶值，有帶表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			log.trace("water_tpe_fee_result.locale: " + hasLocale);
			if(hasLocale){
				log.trace("water_tpe_fee_result.hasLocale...");
				// 判斷從session取出的資料物件是否為BaseResult物件
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( !bs.getResult() ){
					log.trace("water_tpe_fee_result.bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			// 判斷LOCAL KEY沒有帶值則驗證TXTOKEN，驗證通過才可做交易
			if(!hasLocale){
				log.trace("water_tpe_fee_result.validate TXTOKEN...");
				log.trace("water_tpe_fee_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("water_tpe_fee_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("water_tpe_fee_result.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("water_tpe_fee_result TXTOKEN 不正確，TXTOKEN>>{}"+ okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						!okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("water_tpe_fee_result.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("water_tpe_fee_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				
				
				// 取得從輸入頁輸入存在session中的輸入資料
				bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
				Map<String,String> transfer_data = (Map<String, String>) bs.getData();
				
				// 取得SESSION中的輸入資料，加入到確認頁的交易機制資料
				okMap.putAll(transfer_data);
				
				// 重置bs，進行交易
				bs.reset();
				
				//IDgate驗證		 

				try {		 
                    if(okMap.get("FGTXWAY").equals("7")) {	
        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N750B_IDGATE");
                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
                    }
                }catch(Exception e) {		 
                    log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
                } 
				bs = pay_expense_service.water_tpe_fee_result(cusidn, okMap);
				
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
			model.addAttribute("transfer_result_data", bs);
			Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
			model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("water_tpe_fee_result error >> {}",e);
		} finally {
//			TODO 要清除必要的SESSION
			
			// 解決Trust Boundary Violation
//			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			
			// 交易成功要存之前的token 在Session 以利下次比對是否重複交易
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TXTOKEN"));
			if(bs.getResult()) {
				target = "/pay_expense/water_tpe_fee_result";
			}else {
				bs.setNext(next);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	
	/**
	 * 進入N750C-勞保費_輸入頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/labor_insurance")
	public String labor_insurance(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		log.trace("labor_insurance...");
		BaseResult bs = null;
		
		try {
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
		
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);

			// 是否是回上一頁
			if("Y".equals(okMap.get("back"))) {
				model.addAttribute("isBack","Y");
				Map<String, String> previousData = (Map) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				model.addAttribute("previousdata", CodeUtil.toJson(previousData));
				log.trace("previousdata: {}", CodeUtil.toJson(previousData));
			}
			
			// 設定頁面預約日期為明天，如果當前時間太晚則預約後天
			model.addAttribute("tmrDate", DateUtil.getTomorrowOrAfterTomorrowDate());
			
			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = acct_transfer_service.getTxToken();
			log.trace("labor_insurance.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			if(bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			}
			
			// 登入時的身分證字號
			String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				// 解密成明碼
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				log.trace("labor_insurance.cusidn: " + cusidn);
				
				// 轉出成功是否Email通知
				boolean chk_result = pay_expense_service.chkContains(cusidn);
				model.addAttribute("sendMe", chk_result);
				
				
				
	           //IDGATE身分
	           String idgateUserFlag="N";		 
	           Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
	           try {		 
	        	   if(IdgateData==null) {
	        		   IdgateData = new HashMap<String, Object>();
	        	   }
	               BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
	               if(tmp.getResult()) {		 
	                   idgateUserFlag="Y";                  		 
	               }		 
	               tmp.addData("idgateUserFlag",idgateUserFlag);		 
	               IdgateData.putAll((Map<String, String>) tmp.getData());
	               SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	           }catch(Exception e) {		 
	               log.debug("idgateUserFlag error {}",e);		 
	           }		 
	           model.addAttribute("idgateUserFlag",idgateUserFlag);
	           
			}else {
				log.error("session no cusidn!!!");
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("labor_insurance error >> {}",e);
		} finally {
			if(bs!=null && bs.getResult()) {
				target = "/pay_expense/labor_insurance";
				model.addAttribute("labor_insurance", bs);
				
				// 表單驗證用
				model.addAttribute("str_SystemDate", DateUtil.getNextDay());
				model.addAttribute("sNextYearDay", DateUtil.getNextYearDay());
				
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
				
			}
		}
		return target;
	}
	
	/**
	 * 進入N750C-勞保費_確認頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/labor_insurance_confirm")
	public String labor_insurance_confirm(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("labor_insurance_confirm...");
		
		String target = "/error";
		String jsondc = "";
		BaseResult bs = new BaseResult();
		
		log.trace(ESAPIUtil.vaildLog("labor_insurance_confirm.reqParam: {}"+CodeUtil.toJson(reqParam)));
		
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		
		try {
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("labor_insurance_confirm.jsondc: " + jsondc));
			
			okMap.put("jsondc", jsondc);
			okMap.put("LOGINTYPE", "NB"); // 系統別
			log.trace(ESAPIUtil.vaildLog("labor_insurance_confirm.okMap: {}"+ okMap));
			
			
			// 回上一頁重新賦值
			if( okMap.containsKey("previous") ){
				Map<String, String> backMap = (Map<String, String>) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				okMap = backMap;
			}
			
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale: {}"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				if( !bs.getResult())
				{
//					TODO 交易結束後，CONFIRM_LOCALE_DATA 會被清空，造成在URL輸入確認頁會導向錯誤頁，原因待查
					log.trace("bs.getResult(): {}", bs.getResult());
					throw new Exception();
				}else{
					// session 資料放入 map 讓後續 model 和回上一頁取值
					okMap.putAll((Map<String, String>) bs.getData());
				}
			}else {
				// 驗證TOKEN 沒TOKEN會到錯誤頁，錯誤頁確認後返回輸入頁
				String token = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null);
				log.trace("labor_insurance_confirm.token: {}", token);
				if(StrUtil.isNotEmpty(okMap.get("TOKEN"))  &&  okMap.get("TOKEN").equals(token)) {
					bs.setResult(Boolean.TRUE);
				}
				log.trace("bs.getResult(): {}",bs.getResult());
				if(!bs.getResult()) {
					log.trace("throw new Exception()");
					throw new Exception();
				}
				
				// TOKEN驗證成功，資料處理後進入確認頁
				bs.reset();
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
				bs = pay_expense_service.labor_insurance_confirm(okMap, cusidn);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
				
				// IDGATE transdata     
				Map<String,String> result = new HashMap();
				result.putAll((Map< String,String>) bs.getData());
				
				String AMOUNT = result.get("AMOUNT");
				String transfer_date = result.get("transfer_date");
				pay_expense_service.preProcessing(result);
				result.put("IDGATE_AMOUNT", AMOUNT);
				result.put("transfer_date", transfer_date);
				
	            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
	    		String adopid = "N750C";
	    		String title = "您有一筆 繳費-勞保費 待確認，金額"+result.get("IDGATE_AMOUNT");;
	        	if(IdgateData != null) {
		            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
		            if(IdgateData.get("idgateUserFlag").equals("Y")) {
		            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
		            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
						IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N750C_IDGATE_DATA.class, N750C_IDGATE_DATA_VIEW.class, result));
		                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
		            }
	        	}
	            model.addAttribute("idgateAdopid", adopid);
	            model.addAttribute("idgateTitle", title);
	            
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("labor_insurance_confirm error >> {}",e);
		}finally {
			if(bs.getResult()) {
				target = "/pay_expense/labor_insurance_confirm";
//			TODO 這邊還要調整 不能存物件，要存成JSNO字串
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, ((Map<String,Object>)bs.getData()).get("TXTOKEN"));
				
				// 設定回上一頁的url,配合confirm頁面的js
				model.addAttribute("previous","/PAY/EXPENSE/labor_insurance");
				// 回填使用DATA
				SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, okMap);
			}else {
				// 到錯誤頁，錯誤頁的確認按鈕導頁到輸入頁
				bs.setNext("/PAY/EXPENSE/labor_insurance");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * 進入N750C-勞保費_結果頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/labor_insurance_result")
	public String labor_insurance_result(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("labor_insurance_result...");
		log.trace(ESAPIUtil.vaildLog("reqParam {}" +CodeUtil.toJson(reqParam)));

		String target = "/error";
		String next = "/PAY/EXPENSE/labor_insurance";
		String previous = "/PAY/EXPENSE/labor_insurance_confirm";

		BaseResult bs = null;
		try {
			// 初始化BaseResult
			bs = new BaseResult();
			
			// 取得CUSIDN明碼
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
   //		Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			
			// 判斷LOCAL KEY是否有帶值，有帶表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			log.trace("labor_insurance_result.locale: " + hasLocale);
			if(hasLocale){
				log.trace("labor_insurance_result.hasLocale...");
				// 判斷從session取出的資料物件是否為BaseResult物件
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( !bs.getResult() ){
					log.trace("labor_insurance_result.bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			// 判斷LOCAL KEY沒有帶值則驗證TXTOKEN，驗證通過才可做交易
			if(!hasLocale){
				log.trace("labor_insurance_result.validate TXTOKEN...");
				log.trace("labor_insurance_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("labor_insurance_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("labor_insurance_result.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("labor_insurance_result TXTOKEN 不正確，TXTOKEN>>{}"+ okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						!okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("labor_insurance_result.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("labor_insurance_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				
				
				// 取得從輸入頁輸入存在session中的輸入資料
				bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
				Map<String,String> transfer_data = (Map<String, String>) bs.getData();
				
				// 取得SESSION中的輸入資料，加入到確認頁的交易機制資料
				okMap.putAll(transfer_data);
				
				// 重置bs，進行交易
				bs.reset();
				
                //IDgate驗證		 
				try {		 
                    if(okMap.get("FGTXWAY").equals("7")) {	
        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N750C_IDGATE");
                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
                    }
                }catch(Exception e) {		 
                    log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
                } 
				bs = pay_expense_service.labor_insurance_result(cusidn, okMap);
				
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
			model.addAttribute("transfer_result_data", bs);
			Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
			model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("labor_insurance_result error >> {}",e);
		} finally {
//			TODO 要清除必要的SESSION
			
			// 解決Trust Boundary Violation
//			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			
			// 交易成功要存之前的token 在Session 以利下次比對是否重複交易
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TXTOKEN"));
			if(bs.getResult()) {
				target = "/pay_expense/labor_insurance_result";
			}else {
				bs.setNext(next);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	
	/**
	 * 進入N750E-健保費/補充保險費_輸入頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/health_insurance")
	public String health_insurance(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		log.trace("health_insurance...");
		BaseResult bs = null;
		
		try {
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
		
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);

			// 是否是回上一頁
			if("Y".equals(okMap.get("back"))) {
				model.addAttribute("isBack","Y");
				Map<String, String> previousData = (Map) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				model.addAttribute("previousdata", CodeUtil.toJson(previousData));
				log.trace("previousdata: {}", CodeUtil.toJson(previousData));
			}
			
			// 設定頁面預約日期為明天，如果當前時間太晚則預約後天
			model.addAttribute("tmrDate", DateUtil.getTomorrowOrAfterTomorrowDate());
			
			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = acct_transfer_service.getTxToken();
			log.trace("health_insurance.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			if(bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			}
			
			// 登入時的身分證字號
			String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				// 解密成明碼
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				log.trace("health_insurance.cusidn: " + cusidn);
				
				// 轉出成功是否Email通知
				boolean chk_result = pay_expense_service.chkContains(cusidn);
				model.addAttribute("sendMe", chk_result);
				
	           //IDGATE身分
	           String idgateUserFlag="N";		 
	           Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
	           try {		 
	        	   if(IdgateData==null) {
	        		   IdgateData = new HashMap<String, Object>();
	        	   }
	               BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
	               if(tmp.getResult()) {		 
	                   idgateUserFlag="Y";                  		 
	               }		 
	               tmp.addData("idgateUserFlag",idgateUserFlag);		 
	               IdgateData.putAll((Map<String, String>) tmp.getData());
	               SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	           }catch(Exception e) {		 
	               log.debug("idgateUserFlag error {}",e);		 
	           }		 
	           model.addAttribute("idgateUserFlag",idgateUserFlag);
	           
				
			}else {
				log.error("session no cusidn!!!");
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("health_insurance error >> {}",e);
		} finally {
			if(bs!=null && bs.getResult()) {
				target = "/pay_expense/health_insurance";
				model.addAttribute("health_insurance", bs);
				
				// 表單驗證用
				model.addAttribute("str_SystemDate", DateUtil.getNextDay());
				model.addAttribute("sNextYearDay", DateUtil.getNextYearDay());
				
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
				
			}
		}
		return target;
	}
	
	/**
	 * 進入N750E-健保費/補充保險費_確認頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/health_insurance_confirm")
	public String health_insurance_confirm(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("health_insurance_confirm...");
		
		String target = "/error";
		String jsondc = "";
		BaseResult bs = new BaseResult();
		
		log.trace(ESAPIUtil.vaildLog("health_insurance_confirm.reqParam: {}"+CodeUtil.toJson(reqParam)));
		
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		
		try {
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("health_insurance_confirm.jsondc: " + jsondc));
			
			okMap.put("jsondc", jsondc);
			okMap.put("LOGINTYPE", "NB"); // 系統別
			log.trace(ESAPIUtil.vaildLog("health_insurance_confirm.okMap: {}"+ okMap));
			
			
			// 回上一頁重新賦值
			if( okMap.containsKey("previous") ){
				Map<String, String> backMap = (Map<String, String>) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				okMap = backMap;
			}
			
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale: {}"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				if( !bs.getResult())
				{
//					TODO 交易結束後，CONFIRM_LOCALE_DATA 會被清空，造成在URL輸入確認頁會導向錯誤頁，原因待查
					log.trace("bs.getResult(): {}", bs.getResult());
					throw new Exception();
				}else{
					// session 資料放入 map 讓後續 model 和回上一頁取值
					okMap.putAll((Map<String, String>) bs.getData());
				}
			}else {
				// 驗證TOKEN 沒TOKEN會到錯誤頁，錯誤頁確認後返回輸入頁
				String token = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null);
				log.trace("health_insurance_confirm.token: {}", token);
				if(StrUtil.isNotEmpty(okMap.get("TOKEN"))  &&  okMap.get("TOKEN").equals(token)) {
					bs.setResult(Boolean.TRUE);
				}
				log.trace("bs.getResult(): {}",bs.getResult());
				if(!bs.getResult()) {
					log.trace("throw new Exception()");
					throw new Exception();
				}
				
				// TOKEN驗證成功，資料處理後進入確認頁
				bs.reset();
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
				bs = pay_expense_service.health_insurance_confirm(okMap, cusidn);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
				
				// IDGATE transdata 
				Map<String,String> result = new HashMap();
				result.putAll((Map< String,String>) bs.getData());
				String AMOUNT = result.get("AMOUNT");
				String transfer_date = result.get("transfer_date");
				pay_expense_service.preProcessing(result);
				result.put("IDGATE_AMOUNT", AMOUNT);
				result.put("transfer_date", transfer_date);
	            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
	    		String adopid = "N750E";
	    		String title = "您有一筆 繳費-健保費/補充保險費 待確認，金額" + result.get("IDGATE_AMOUNT");
	        	if(IdgateData != null) {
		            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
		            if(IdgateData.get("idgateUserFlag").equals("Y")) {
		            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
		            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
						IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N750E_IDGATE_DATA.class, N750E_IDGATE_DATA_VIEW.class, result));
		                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
		            }
	        	}
	            model.addAttribute("idgateAdopid", adopid);
	            model.addAttribute("idgateTitle", title);
	           
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("health_insurance_confirm error >> {}",e);
		}finally {
			if(bs.getResult()) {
				target = "/pay_expense/health_insurance_confirm";
//			TODO 這邊還要調整 不能存物件，要存成JSNO字串
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, ((Map<String,Object>)bs.getData()).get("TXTOKEN"));
				
				// 設定回上一頁的url,配合confirm頁面的js
				model.addAttribute("previous","/PAY/EXPENSE/health_insurance");
				// 回填使用DATA
				SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, okMap);
			}else {
				// 到錯誤頁，錯誤頁的確認按鈕導頁到輸入頁
				bs.setNext("/PAY/EXPENSE/health_insurance");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * 進入N750E-健保費/補充保險費_結果頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/health_insurance_result")
	public String health_insurance_result(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("health_insurance_result...");
		log.trace(ESAPIUtil.vaildLog("reqParam {}"+CodeUtil.toJson(reqParam)));

		String target = "/error";
		String next = "/PAY/EXPENSE/health_insurance";
		String previous = "/PAY/EXPENSE/health_insurance_confirm";

		BaseResult bs = null;
		try {
			// 初始化BaseResult
			bs = new BaseResult();
			
			// 取得CUSIDN明碼
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			//Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			
			// 判斷LOCAL KEY是否有帶值，有帶表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			log.trace("health_insurance_result.locale: " + hasLocale);
			if(hasLocale){
				log.trace("health_insurance_result.hasLocale...");
				// 判斷從session取出的資料物件是否為BaseResult物件
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( !bs.getResult() ){
					log.trace("health_insurance_result.bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			// 判斷LOCAL KEY沒有帶值則驗證TXTOKEN，驗證通過才可做交易
			if(!hasLocale){
				log.trace("health_insurance_result.validate TXTOKEN...");
				log.trace("health_insurance_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("health_insurance_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("health_insurance_result.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("health_insurance_result TXTOKEN 不正確，TXTOKEN>>{}"+ okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						!okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("health_insurance_result.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("health_insurance_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				
				
				// 取得從輸入頁輸入存在session中的輸入資料
				bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
				Map<String,String> transfer_data = (Map<String, String>) bs.getData();
				
				// 取得SESSION中的輸入資料，加入到確認頁的交易機制資料
				okMap.putAll(transfer_data);
				
				// 重置bs，進行交易
				bs.reset();
				
                //IDgate驗證		 
				try {		 
                    if(okMap.get("FGTXWAY").equals("7")) {	
        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N750E_IDGATE");
                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
                    }
                }catch(Exception e) {		 
                    log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
                } 
				bs = pay_expense_service.health_insurance_result(cusidn, okMap);
				
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
			model.addAttribute("transfer_result_data", bs);
			Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
			model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("health_insurance_result error >> {}",e);
		} finally {
//			TODO 要清除必要的SESSION
			
			// 解決Trust Boundary Violation
//			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			
			// 交易成功要存之前的token 在Session 以利下次比對是否重複交易
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TXTOKEN"));
			if(bs.getResult()) {
				target = "/pay_expense/health_insurance_result";
			}else {
				bs.setNext(next);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	
	/**
	 * 進入N750F-國民年金保險費_輸入頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/national_pension")
	public String national_pension(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		log.trace("national_pension...");
		BaseResult bs = null;
		
		try {
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
		
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);

			// 是否是回上一頁
			if("Y".equals(okMap.get("back"))) {
				model.addAttribute("isBack","Y");
				Map<String, String> previousData = (Map) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				model.addAttribute("previousdata", CodeUtil.toJson(previousData));
				log.trace("previousdata: {}", CodeUtil.toJson(previousData));
			}
			
			// 設定頁面預約日期為明天，如果當前時間太晚則預約後天
			model.addAttribute("tmrDate", DateUtil.getTomorrowOrAfterTomorrowDate());
			
			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = acct_transfer_service.getTxToken();
			log.trace("national_pension.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			if(bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			}
			
			// 登入時的身分證字號
			String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				// 解密成明碼
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				log.trace("national_pension.cusidn: " + cusidn);
				
				// 轉出成功是否Email通知
				boolean chk_result = pay_expense_service.chkContains(cusidn);
				model.addAttribute("sendMe", chk_result);
				
				  //IDGATE身分
		           String idgateUserFlag="N";		 
		           Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
		           try {		 
		        	   if(IdgateData==null) {
		        		   IdgateData = new HashMap<String, Object>();
		        	   }
		               BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
		               if(tmp.getResult()) {		 
		                   idgateUserFlag="Y";                  		 
		               }		 
		               tmp.addData("idgateUserFlag",idgateUserFlag);		 
		               IdgateData.putAll((Map<String, String>) tmp.getData());
		               SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
		           }catch(Exception e) {		 
		               log.debug("idgateUserFlag error {}",e);		 
		           }		 
		           model.addAttribute("idgateUserFlag",idgateUserFlag);
		           
			}else {
				log.error("session no cusidn!!!");
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("national_pension error >> {}",e);
		} finally {
			if(bs!=null && bs.getResult()) {
				target = "/pay_expense/national_pension";
				model.addAttribute("national_pension", bs);
				
				// 表單驗證用
				model.addAttribute("str_SystemDate", DateUtil.getNextDay());
				model.addAttribute("sNextYearDay", DateUtil.getNextYearDay());
				
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
				
			}
		}
		return target;
	}
	
	/**
	 * 進入N750F-國民年金保險費_確認頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/national_pension_confirm")
	public String national_pension_confirm(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("national_pension_confirm...");
		
		String target = "/error";
		String jsondc = "";
		BaseResult bs = new BaseResult();
		
		log.trace(ESAPIUtil.vaildLog("national_pension_confirm.reqParam: {}"+CodeUtil.toJson(reqParam)));
		
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		
		try {
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("national_pension_confirm.jsondc: " + jsondc));
			
			okMap.put("jsondc", jsondc);
			okMap.put("LOGINTYPE", "NB"); // 系統別
			log.trace(ESAPIUtil.vaildLog("national_pension_confirm.okMap: {}"+ okMap));
			
			
			// 回上一頁重新賦值
			if( okMap.containsKey("previous") ){
				Map<String, String> backMap = (Map<String, String>) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				okMap = backMap;
			}
			
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale: {}"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				if( !bs.getResult())
				{
//					TODO 交易結束後，CONFIRM_LOCALE_DATA 會被清空，造成在URL輸入確認頁會導向錯誤頁，原因待查
					log.trace("bs.getResult(): {}", bs.getResult());
					throw new Exception();
				}else{
					// session 資料放入 map 讓後續 model 和回上一頁取值
					okMap.putAll((Map<String, String>) bs.getData());
				}
			}else {
				// 驗證TOKEN 沒TOKEN會到錯誤頁，錯誤頁確認後返回輸入頁
				String token = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null);
				log.trace("national_pension_confirm.token: {}", token);
				if(StrUtil.isNotEmpty(okMap.get("TOKEN"))  &&  okMap.get("TOKEN").equals(token)) {
					bs.setResult(Boolean.TRUE);
				}
				log.trace("bs.getResult(): {}",bs.getResult());
				if(!bs.getResult()) {
					log.trace("throw new Exception()");
					throw new Exception();
				}
				
				// TOKEN驗證成功，資料處理後進入確認頁
				bs.reset();
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
				bs = pay_expense_service.national_pension_confirm(okMap, cusidn);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
				

				// IDGATE transdata   
				Map<String,String> result = new HashMap();
				result.putAll((Map< String,String>) bs.getData());
				String AMOUNT = result.get("AMOUNT");
				String transfer_date = result.get("transfer_date");
				pay_expense_service.preProcessing(result);
				result.put("IDGATE_AMOUNT", AMOUNT);
				result.put("transfer_date", transfer_date);
	            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
	    		String adopid = "N750F";
	    		String title = "您有一筆 繳費-國民年金保險費 待確認，金額"+result.get("IDGATE_AMOUNT");
	        	if(IdgateData != null) {
		            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
		            if(IdgateData.get("idgateUserFlag").equals("Y")) {
		            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
		            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
						IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N750F_IDGATE_DATA.class, N750F_IDGATE_DATA_VIEW.class, result));
		                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
		            }
	        	}
	            model.addAttribute("idgateAdopid", adopid);
	            model.addAttribute("idgateTitle", title);
	            
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("national_pension_confirm error >> {}",e);
		}finally {
			if(bs.getResult()) {
				target = "/pay_expense/national_pension_confirm";
//			TODO 這邊還要調整 不能存物件，要存成JSNO字串
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, ((Map<String,Object>)bs.getData()).get("TXTOKEN"));
				
				// 設定回上一頁的url,配合confirm頁面的js
				model.addAttribute("previous","/PAY/EXPENSE/national_pension");
				// 回填使用DATA
				SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, okMap);
			}else {
				// 到錯誤頁，錯誤頁的確認按鈕導頁到輸入頁
				bs.setNext("/PAY/EXPENSE/national_pension");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * 進入N750F-國民年金保險費_結果頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/national_pension_result")
	public String national_pension_result(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("national_pension_result...");
		log.trace(ESAPIUtil.vaildLog("reqParam {}"+CodeUtil.toJson(reqParam)));

		String target = "/error";
		String next = "/PAY/EXPENSE/national_pension";
		String previous = "/PAY/EXPENSE/national_pension_confirm";

		BaseResult bs = null;
		try {
			// 初始化BaseResult
			bs = new BaseResult();
			
			// 取得CUSIDN明碼
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//			Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			
			// 判斷LOCAL KEY是否有帶值，有帶表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			log.trace("national_pension_result.locale: " + hasLocale);
			if(hasLocale){
				log.trace("national_pension_result.hasLocale...");
				// 判斷從session取出的資料物件是否為BaseResult物件
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( !bs.getResult() ){
					log.trace("national_pension_result.bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			// 判斷LOCAL KEY沒有帶值則驗證TXTOKEN，驗證通過才可做交易
			if(!hasLocale){
				log.trace("national_pension_result.validate TXTOKEN...");
				log.trace("national_pension_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("national_pension_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("national_pension_result.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("national_pension_result TXTOKEN 不正確，TXTOKEN>>{}"+ okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						!okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("national_pension_result.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("national_pension_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				
				
				// 取得從輸入頁輸入存在session中的輸入資料
				bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
				Map<String,String> transfer_data = (Map<String, String>) bs.getData();
				
				// 取得SESSION中的輸入資料，加入到確認頁的交易機制資料
				okMap.putAll(transfer_data);
				
				// 重置bs，進行交易
				bs.reset();
				
				  //IDgate驗證		 
				try {		 
                    if(okMap.get("FGTXWAY").equals("7")) {	
        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N750F_IDGATE");
                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
                    }
                }catch(Exception e) {		 
                    log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
                } 
				bs = pay_expense_service.national_pension_result(cusidn, okMap);
				
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
			model.addAttribute("transfer_result_data", bs);
			Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
			model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("national_pension_result error >> {}",e);
		} finally {
//			TODO 要清除必要的SESSION
			
			// 解決Trust Boundary Violation
//			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			
			// 交易成功要存之前的token 在Session 以利下次比對是否重複交易
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TXTOKEN"));
			if(bs.getResult()) {
				target = "/pay_expense/national_pension_result";
			}else {
				bs.setNext(next);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	
	/**
	 * 進入N750H-新制勞工退休金_輸入頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/labor_pension")
	public String labor_pension(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		log.trace("labor_pension...");
		BaseResult bs = null;
		
		try {
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
		
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);

			// 是否是回上一頁
			if("Y".equals(okMap.get("back"))) {
				model.addAttribute("isBack","Y");
				Map<String, String> previousData = (Map) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				model.addAttribute("previousdata", CodeUtil.toJson(previousData));
				log.trace("previousdata: {}", CodeUtil.toJson(previousData));
			}
			
			// 設定頁面預約日期為明天，如果當前時間太晚則預約後天
			model.addAttribute("tmrDate", DateUtil.getTomorrowOrAfterTomorrowDate());
			
			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = acct_transfer_service.getTxToken();
			log.trace("labor_pension.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			if(bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			}
			
			// 登入時的身分證字號
			String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				// 解密成明碼
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				log.trace("labor_pension.cusidn: " + cusidn);
				
				// 轉出成功是否Email通知
				boolean chk_result = pay_expense_service.chkContains(cusidn);
				model.addAttribute("sendMe", chk_result);
				
				
	           //IDGATE身分
	           String idgateUserFlag="N";		 
	           Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
	           try {		 
	        	   if(IdgateData==null) {
	        		   IdgateData = new HashMap<String, Object>();
	        	   }
	               BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
	               if(tmp.getResult()) {		 
	                   idgateUserFlag="Y";                  		 
	               }		 
	               tmp.addData("idgateUserFlag",idgateUserFlag);		 
	               IdgateData.putAll((Map<String, String>) tmp.getData());
	               SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	           }catch(Exception e) {		 
	               log.debug("idgateUserFlag error {}",e);		 
	           }		 
	           model.addAttribute("idgateUserFlag",idgateUserFlag);
		           
				
			}else {
				log.error("session no cusidn!!!");
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("labor_pension error >> {}",e);
		} finally {
			if(bs!=null && bs.getResult()) {
				target = "/pay_expense/labor_pension";
				model.addAttribute("labor_pension", bs);
				
				// 表單驗證用
				model.addAttribute("str_SystemDate", DateUtil.getNextDay());
				model.addAttribute("sNextYearDay", DateUtil.getNextYearDay());
				
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
				
			}
		}
		return target;
	}
	
	/**
	 * 進入N750H-新制勞工退休金_確認頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/labor_pension_confirm")
	public String labor_pension_confirm(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("labor_pension_confirm...");
		
		String target = "/error";
		String jsondc = "";
		BaseResult bs = new BaseResult();
		
		log.trace(ESAPIUtil.vaildLog("labor_pension_confirm.reqParam: {}"+CodeUtil.toJson(reqParam)));
		
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		
		try {
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("labor_pension_confirm.jsondc: " + jsondc));
			
			okMap.put("jsondc", jsondc);
			okMap.put("LOGINTYPE", "NB"); // 系統別
			log.trace(ESAPIUtil.vaildLog("labor_pension_confirm.okMap: {}"+ okMap));
			
			
			// 回上一頁重新賦值
			if( okMap.containsKey("previous") ){
				Map<String, String> backMap = (Map<String, String>) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				okMap = backMap;
			}
			
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale: {}"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				if( !bs.getResult())
				{
//					TODO 交易結束後，CONFIRM_LOCALE_DATA 會被清空，造成在URL輸入確認頁會導向錯誤頁，原因待查
					log.trace("bs.getResult(): {}", bs.getResult());
					throw new Exception();
				}else{
					// session 資料放入 map 讓後續 model 和回上一頁取值
					okMap.putAll((Map<String, String>) bs.getData());
				}
			}else {
				// 驗證TOKEN 沒TOKEN會到錯誤頁，錯誤頁確認後返回輸入頁
				String token = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null);
				log.trace("labor_pension_confirm.token: {}", token);
				if(StrUtil.isNotEmpty(okMap.get("TOKEN"))  &&  okMap.get("TOKEN").equals(token)) {
					bs.setResult(Boolean.TRUE);
				}
				log.trace("bs.getResult(): {}",bs.getResult());
				if(!bs.getResult()) {
					log.trace("throw new Exception()");
					throw new Exception();
				}
				
				// TOKEN驗證成功，資料處理後進入確認頁
				bs.reset();
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
				bs = pay_expense_service.labor_pension_confirm(okMap, cusidn);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
				

				// IDGATE transdata            		 
				Map<String,String> result = new HashMap();
				result.putAll((Map< String,String>) bs.getData());
				String AMOUNT = result.get("AMOUNT");
				String transfer_date = result.get("transfer_date");
				pay_expense_service.preProcessing(result);
				result.put("IDGATE_AMOUNT", AMOUNT);
				result.put("transfer_date", transfer_date);
	            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
	    		String adopid = "N750H";
	    		String title = "您有一筆 繳費-新制勞工退休金 待確認，金額"+result.get("IDGATE_AMOUNT");
	        	if(IdgateData != null) {
		            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
		            if(IdgateData.get("idgateUserFlag").equals("Y")) {
		            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
		            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
						IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N750H_IDGATE_DATA.class, N750H_IDGATE_DATA_VIEW.class, result));
	                }
	                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
		        }
	            model.addAttribute("idgateAdopid", adopid);
	            model.addAttribute("idgateTitle", title);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("labor_pension_confirm error >> {}",e);
		}finally {
			if(bs.getResult()) {
				target = "/pay_expense/labor_pension_confirm";
//			TODO 這邊還要調整 不能存物件，要存成JSNO字串
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, ((Map<String,Object>)bs.getData()).get("TXTOKEN"));
				
				// 設定回上一頁的url,配合confirm頁面的js
				model.addAttribute("previous","/PAY/EXPENSE/labor_pension");
				// 回填使用DATA
				SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, okMap);
			}else {
				// 到錯誤頁，錯誤頁的確認按鈕導頁到輸入頁
				bs.setNext("/PAY/EXPENSE/labor_pension");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * 進入N750H-新制勞工退休金_結果頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/labor_pension_result")
	public String labor_pension_result(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("labor_pension_result...");
		log.trace(ESAPIUtil.vaildLog("reqParam {}" +CodeUtil.toJson(reqParam)));

		String target = "/error";
		String next = "/PAY/EXPENSE/labor_pension";
		String previous = "/PAY/EXPENSE/labor_pension_confirm";

		BaseResult bs = null;
		try {
			// 初始化BaseResult
			bs = new BaseResult();
			
			// 取得CUSIDN明碼
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		//	Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			
			// 判斷LOCAL KEY是否有帶值，有帶表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			log.trace("labor_pension_result.locale: " + hasLocale);
			if(hasLocale){
				log.trace("labor_pension_result.hasLocale...");
				// 判斷從session取出的資料物件是否為BaseResult物件
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( !bs.getResult() ){
					log.trace("labor_pension_result.bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			// 判斷LOCAL KEY沒有帶值則驗證TXTOKEN，驗證通過才可做交易
			if(!hasLocale){
				log.trace("labor_pension_result.validate TXTOKEN...");
				log.trace("labor_pension_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("labor_pension_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("labor_pension_result.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("labor_pension_result TXTOKEN 不正確，TXTOKEN>>{}"+ okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						!okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("labor_pension_result.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("labor_pension_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				
				
				// 取得從輸入頁輸入存在session中的輸入資料
				bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
				Map<String,String> transfer_data = (Map<String, String>) bs.getData();
				
				// 取得SESSION中的輸入資料，加入到確認頁的交易機制資料
				okMap.putAll(transfer_data);
				
				// 重置bs，進行交易
				bs.reset();
				
				try {		 
                    if(okMap.get("FGTXWAY").equals("7")) {	
        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N750H_IDGATE");
                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
                    }
                }catch(Exception e) {		 
                    log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
                } 
				bs = pay_expense_service.labor_pension_result(cusidn, okMap);
				
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
			model.addAttribute("transfer_result_data", bs);
			Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
			model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("labor_pension_result error >> {}",e);
		} finally {
//			TODO 要清除必要的SESSION
			
			// 解決Trust Boundary Violation
//			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			
			// 交易成功要存之前的token 在Session 以利下次比對是否重複交易
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TXTOKEN"));
			if(bs.getResult()) {
				target = "/pay_expense/labor_pension_result";
			}else {
				bs.setNext(next);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	
	/**
	 * 進入N750G-欣欣瓦斯費_輸入頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/gas_fee")
	public String gas_fee(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		log.trace("gas_fee...");
		BaseResult bs = null;
		
		try {
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
		
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);

			// 是否是回上一頁
			if("Y".equals(okMap.get("back"))) {
				model.addAttribute("isBack","Y");
				Map<String, String> previousData = (Map) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				model.addAttribute("previousdata", CodeUtil.toJson(previousData));
				log.trace("previousdata: {}", CodeUtil.toJson(previousData));
			}
			
			// 設定頁面預約日期為明天，如果當前時間太晚則預約後天
			model.addAttribute("tmrDate", DateUtil.getTomorrowOrAfterTomorrowDate());
			
			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = acct_transfer_service.getTxToken();
			log.trace("gas_fee.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			if(bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			}
			
			// 登入時的身分證字號
			String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				// 解密成明碼
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				log.trace("gas_fee.cusidn: " + cusidn);
				
				// 轉出成功是否Email通知
				boolean chk_result = pay_expense_service.chkContains(cusidn);
				model.addAttribute("sendMe", chk_result);
				
			 //IDGATE身分
	           String idgateUserFlag="N";		 
	           Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
	           try {		 
	        	   if(IdgateData==null) {
	        		   IdgateData = new HashMap<String, Object>();
	        	   }
	               BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
	               if(tmp.getResult()) {		 
	                   idgateUserFlag="Y";                  		 
	               }		 
	               tmp.addData("idgateUserFlag",idgateUserFlag);		 
	               IdgateData.putAll((Map<String, String>) tmp.getData());
	               SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	           }catch(Exception e) {		 
	               log.debug("idgateUserFlag error {}",e);		 
	           }		 
	           model.addAttribute("idgateUserFlag",idgateUserFlag);
	           
				
			}else {
				log.error("session no cusidn!!!");
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gas_fee error >> {}",e);
		} finally {
			if(bs!=null && bs.getResult()) {
				target = "/pay_expense/gas_fee";
				model.addAttribute("gas_fee", bs);
				
				// 表單驗證用
				model.addAttribute("str_SystemDate", DateUtil.getNextDay());
				model.addAttribute("sNextYearDay", DateUtil.getNextYearDay());
				
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
				
			}
		}
		return target;
	}
	
	/**
	 * 進入N750G-欣欣瓦斯費_確認頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/gas_fee_confirm")
	public String gas_fee_confirm(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("gas_fee_confirm...");
		
		String target = "/error";
		String jsondc = "";
		BaseResult bs = new BaseResult();
		
		log.trace(ESAPIUtil.vaildLog("gas_fee_confirm.reqParam: {}"+CodeUtil.toJson(reqParam)));
		
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		
		try {
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("gas_fee_confirm.jsondc: " + jsondc));
			
			okMap.put("jsondc", jsondc);
			okMap.put("LOGINTYPE", "NB"); // 系統別
			log.trace(ESAPIUtil.vaildLog("gas_fee_confirm.okMap: {}"+ okMap));
			
			
			// 回上一頁重新賦值
			if( okMap.containsKey("previous") ){
				Map<String, String> backMap = (Map<String, String>) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				okMap = backMap;
			}
			
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale: {}"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				if( !bs.getResult())
				{
//					TODO 交易結束後，CONFIRM_LOCALE_DATA 會被清空，造成在URL輸入確認頁會導向錯誤頁，原因待查
					log.trace("bs.getResult(): {}", bs.getResult());
					throw new Exception();
				}else{
					// session 資料放入 map 讓後續 model 和回上一頁取值
					okMap.putAll((Map<String, String>) bs.getData());
				}
			}else {
				// 驗證TOKEN 沒TOKEN會到錯誤頁，錯誤頁確認後返回輸入頁
				String token = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null);
				log.trace("gas_fee_confirm.token: {}", token);
				if(StrUtil.isNotEmpty(okMap.get("TOKEN"))  &&  okMap.get("TOKEN").equals(token)) {
					bs.setResult(Boolean.TRUE);
				}
				log.trace("bs.getResult(): {}",bs.getResult());
				if(!bs.getResult()) {
					log.trace("throw new Exception()");
					throw new Exception();
				}
				
				// TOKEN驗證成功，資料處理後進入確認頁
				bs.reset();
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
				bs = pay_expense_service.gas_fee_confirm(okMap, cusidn);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
				
				// IDGATE transdata  
				Map<String,String> result = new HashMap();
				result.putAll((Map< String,String>) bs.getData());
				String AMOUNT = result.get("AMOUNT");
				String transfer_date = result.get("transfer_date");
				pay_expense_service.preProcessing(result);
				result.put("IDGATE_AMOUNT", AMOUNT);
				result.put("transfer_date", transfer_date);
	            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
	    		String adopid = "N750G";
	    		String title = "您有一筆 繳費-欣欣瓦斯 待確認，金額"+result.get("IDGATE_AMOUNT");
	        	if(IdgateData != null) {
		            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
		            if(IdgateData.get("idgateUserFlag").equals("Y")) {
		            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
		            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
		            	result.put("CUSIDN", cusidn);
						IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N750G_IDGATE_DATA.class, N750G_IDGATE_DATA_VIEW.class, result));
		                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
		            }
	        	}
	            model.addAttribute("idgateAdopid", adopid);
	            model.addAttribute("idgateTitle", title);
	            
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gas_fee_confirm error >> {}",e);
		}finally {
			if(bs.getResult()) {
				target = "/pay_expense/gas_fee_confirm";
//			TODO 這邊還要調整 不能存物件，要存成JSNO字串
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, ((Map<String,Object>)bs.getData()).get("TXTOKEN"));
				
				// 設定回上一頁的url,配合confirm頁面的js
				model.addAttribute("previous","/PAY/EXPENSE/gas_fee");
				// 回填使用DATA
				SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, okMap);
			}else {
				// 到錯誤頁，錯誤頁的確認按鈕導頁到輸入頁
				bs.setNext("/PAY/EXPENSE/gas_fee");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * 進入N750G-欣欣瓦斯費_結果頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/gas_fee_result")
	public String gas_fee_result(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("gas_fee_result...");
		log.trace(ESAPIUtil.vaildLog("reqParam {}"+CodeUtil.toJson(reqParam)));

		String target = "/error";
		String next = "/PAY/EXPENSE/gas_fee";
		String previous = "/PAY/EXPENSE/gas_fee_confirm";

		BaseResult bs = null;
		try {
			// 初始化BaseResult
			bs = new BaseResult();
			
			// 取得CUSIDN明碼
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//			Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			
			// 判斷LOCAL KEY是否有帶值，有帶表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			log.trace("gas_fee_result.locale: " + hasLocale);
			if(hasLocale){
				log.trace("gas_fee_result.hasLocale...");
				// 判斷從session取出的資料物件是否為BaseResult物件
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( !bs.getResult() ){
					log.trace("gas_fee_result.bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			// 判斷LOCAL KEY沒有帶值則驗證TXTOKEN，驗證通過才可做交易
			if(!hasLocale){
				log.trace("gas_fee_result.validate TXTOKEN...");
				log.trace("gas_fee_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("gas_fee_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("gas_fee_result.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("gas_fee_result TXTOKEN 不正確，TXTOKEN>>{}"+ okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						!okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("gas_fee_result.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("gas_fee_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				
				
				// 取得從輸入頁輸入存在session中的輸入資料
				bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
				Map<String,String> transfer_data = (Map<String, String>) bs.getData();
				
				// 取得SESSION中的輸入資料，加入到確認頁的交易機制資料
				okMap.putAll(transfer_data);
				
				// 重置bs，進行交易
				bs.reset();
				
                //IDgate驗證		 
                try {		 
                    if(okMap.get("FGTXWAY").equals("7")) {	
        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N750G_IDGATE");
                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
                    }
                }catch(Exception e) {		 
                    log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
                } 
                bs = pay_expense_service.gas_fee_result(cusidn, okMap);
				
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
			model.addAttribute("transfer_result_data", bs);
			Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
			model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gas_fee_result error >> {}",e);
		} finally {
//			TODO 要清除必要的SESSION
			
			// 解決Trust Boundary Violation
//			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			
			// 交易成功要存之前的token 在Session 以利下次比對是否重複交易
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TXTOKEN"));
			if(bs.getResult()) {
				target = "/pay_expense/gas_fee_result";
			}else {
				bs.setNext(next);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * N070B 其他費用(輸入頁)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/other_fee")
	public String other_fee(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		log.trace("other_fee...");
		BaseResult bs = null;
		
		try {
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
		
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);

			// 是否是回上一頁
			if("Y".equals(okMap.get("back"))) {
				model.addAttribute("isBack","Y");
				Map<String, String> previousData = (Map) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				
				// DPAGACNO 會有雙引號所以JSON.parse會有問題
				model.addAttribute("DPAGACNO_BACK", previousData.get("DPAGACNO"));
				previousData.remove("DPAGACNO");
				model.addAttribute("previousdata", CodeUtil.toJson(previousData));
				log.trace("previousdata: {}", CodeUtil.toJson(previousData));
			}
			
			// 設定頁面預約日期為明天，如果當前時間太晚則預約後天
			model.addAttribute("tmrDate", DateUtil.getTomorrowOrAfterTomorrowDate());
			
			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = acct_transfer_service.getTxToken();
			log.trace("other_fee.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			if(bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			}
			
			// 登入時的身分證字號
			String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				// 解密成明碼
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				log.trace("other_fee.cusidn: " + cusidn);
				
				// 轉出成功是否Email通知
				boolean chk_result = pay_expense_service.chkContains(cusidn);
				model.addAttribute("sendMe", chk_result);
				
			}else {
				log.error("session no cusidn!!!");
			}
			
			//IDGATE身分
	           String idgateUserFlag="N";		 
	           Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
	           try {		 
	        	   if(IdgateData==null) {
	        		   IdgateData = new HashMap<String, Object>();
	        	   }
	               BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
	               if(tmp.getResult()) {		 
	                   idgateUserFlag="Y";                  		 
	               }		 
	               tmp.addData("idgateUserFlag",idgateUserFlag);		 
	               IdgateData.putAll((Map<String, String>) tmp.getData());
	               SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	           }catch(Exception e) {		 
	               log.debug("idgateUserFlag error {}",e);		 
	           }		 
	           model.addAttribute("idgateUserFlag",idgateUserFlag);
	           
	       } catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("other_fee error >> {}",e);
		} finally {
			if(bs!=null && bs.getResult()) {
				target = "/pay_expense/other_fee";
				model.addAttribute("other_fee", bs);
				
				// 表單驗證用
				model.addAttribute("str_SystemDate", DateUtil.getNextDay());
				model.addAttribute("sNextYearDay", DateUtil.getNextYearDay());
				
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
				
			}
		}
		return target;
	}
	
	/**
	 * N070B 其他費用(確認頁)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/other_fee_confirm")
	public String other_fee_confirm(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("other_fee_confirm...");
		
		String target = "/error";
		String jsondc = "";
		BaseResult bs = new BaseResult();
		
		log.trace(ESAPIUtil.vaildLog("other_fee_confirm.reqParam: {}"+CodeUtil.toJson(reqParam)));
		
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		
		try {
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("other_fee_confirm.jsondc: " + jsondc));
			
			okMap.put("jsondc", jsondc); // IKEY要用不經過ESAPIUtil，會出錯
			okMap.put("LOGINTYPE", "NB"); // 系統別
			log.trace(ESAPIUtil.vaildLog("other_fee_confirm.okMap: {}"+ okMap));
			
			
			// 回上一頁重新賦值
			if( okMap.containsKey("previous") ){
				Map<String, String> backMap = (Map<String, String>) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				okMap = backMap;
			}
			
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale: {}"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				if( !bs.getResult())
				{
//					TODO 交易結束後，CONFIRM_LOCALE_DATA 會被清空，造成在URL輸入確認頁會導向錯誤頁，原因待查
					log.trace("bs.getResult(): {}", bs.getResult());
					throw new Exception();
				}else{
					// session 資料放入 map 讓後續 model 和回上一頁取值
					okMap.putAll((Map<String, String>) bs.getData());
				}
			}else {
				// 驗證TOKEN 沒TOKEN會到錯誤頁，錯誤頁確認後返回輸入頁
				String token = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null);
				log.trace("other_fee_confirm.token: {}", token);
				if(StrUtil.isNotEmpty(okMap.get("TOKEN"))  &&  okMap.get("TOKEN").equals(token)) {
					bs.setResult(Boolean.TRUE);
				}
				log.trace("bs.getResult(): {}",bs.getResult());
				if(!bs.getResult()) {
					log.trace("throw new Exception()");
					throw new Exception();
				}
				
				// TOKEN驗證成功，資料處理後進入確認頁
				bs.reset();
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
				bs = pay_expense_service.other_fee_confirm(okMap, cusidn);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
				
				// IDGATE transdata
				Map<String, String> result = (Map<String, String>) bs.getData();
	            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
	    		String adopid = "N070B";
	    		String title = "";
	    		if("1".equals(okMap.get("FGTXDATE"))){
	    			title = "您有一筆即時繳費交易待確認，金額 "+result.get("AMOUNT");
	    		}else {
	    			title = "您有一筆預約繳費交易待確認，金額 "+result.get("AMOUNT");
	    		}
	    		
	        	if(IdgateData != null) {
		            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
		            if(IdgateData.get("idgateUserFlag").equals("Y")) {
		            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
		            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
		            	//轉換AMOUNT的格式 for IDGATE驗證 , 新增AMOUNT_FMT給VIEW
		            	String amount =  result.get("AMOUNT");
		            	result.put("AMOUNT", NumericUtil.unfmtAmount(amount));
		            	result.put("AMOUNT_FMT", amount );
		            	result.put("CUSIDN", cusidn );
		            	log.debug("IDGATE ADOPID >> {}" , adopid);
		            	log.debug("IDGATE TITLE >> {}" , title);
		            	log.debug("IDGATE VALUE >> {}" , result);
		            	
						IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N070B_IDGATE_DATA.class, N070B_IDGATE_DATA_VIEW.class, result));
		                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
		            }
	        	}
	        	
	            model.addAttribute("idgateAdopid", adopid);
	            model.addAttribute("idgateTitle", title);
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("other_fee_confirm error >> {}",e);
		}finally {
			if(bs.getResult()) {
				target = "/pay_expense/other_fee_confirm";
//			TODO 這邊還要調整 不能存物件，要存成JSNO字串
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, ((Map<String,Object>)bs.getData()).get("TXTOKEN"));
				
				// 設定回上一頁的url,配合confirm頁面的js
				model.addAttribute("previous","/PAY/EXPENSE/other_fee");
				// 回填使用DATA
				SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, okMap);
			}else {
				// 到錯誤頁，錯誤頁的確認按鈕導頁到輸入頁
				bs.setNext("/PAY/EXPENSE/other_fee");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * N070B 其他費用(結果頁)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/other_fee_result")
	public String other_fee_result(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("other_fee_result...");
		log.trace(ESAPIUtil.vaildLog("reqParam {}" +CodeUtil.toJson(reqParam)));

		String target = "/error";
		String next = "/PAY/EXPENSE/other_fee";
		String previous = "/PAY/EXPENSE/other_fee_confirm";

		BaseResult bs = null;
		try {
			// 初始化BaseResult
			bs = new BaseResult();
			
			// 取得CUSIDN明碼
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
	//		Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			
			// 判斷LOCAL KEY是否有帶值，有帶表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			log.trace("other_fee_result.locale: " + hasLocale);
			if(hasLocale){
				log.trace("other_fee_result.hasLocale...");
				// 判斷從session取出的資料物件是否為BaseResult物件
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( !bs.getResult() ){
					log.trace("other_fee_result.bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			// 判斷LOCAL KEY沒有帶值則驗證TXTOKEN，驗證通過才可做交易
			if(!hasLocale){
				log.trace("other_fee_result.validate TXTOKEN...");
				log.trace("other_fee_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("other_fee_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("other_fee_result.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("other_fee_result TXTOKEN 不正確，TXTOKEN>>{}"+ okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						!okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("other_fee_result.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("other_fee_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				
				
				// 取得從輸入頁輸入存在session中的輸入資料
				bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
				Map<String,String> transfer_data = (Map<String, String>) bs.getData();
				
				// 取得SESSION中的輸入資料，加入到確認頁的交易機制資料
				okMap.putAll(transfer_data);
				
				// 重置bs，進行交易
				bs.reset();
				
				
				//IDgate驗證		 
                try {		 
                    if(okMap.get("FGTXWAY").equals("7")) {		 
        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N070B_IDGATE");
                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));	 
                    }
                }catch(Exception e) {		 
                    log.error("IDGATE TRANS error>>{}",e);		 
                }  
				
				bs = pay_expense_service.other_fee_result(cusidn, okMap);
				
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
			model.addAttribute("transfer_result_data", bs);
			Map<String,Object> IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
			model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("other_fee_result error >> {}",e);
		} finally {
//			TODO 要清除必要的SESSION
			
			// 解決Trust Boundary Violation
//			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			
			// 交易成功要存之前的token 在Session 以利下次比對是否重複交易
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TXTOKEN"));
			if(bs.getResult()) {
				target = "/pay_expense/other_fee_result";
			}else {
				bs.setNext(next);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
}
