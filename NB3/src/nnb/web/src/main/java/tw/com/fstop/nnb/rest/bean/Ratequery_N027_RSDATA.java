package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class Ratequery_N027_RSDATA extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3052203423117435416L;

	private String RECNO;
	private String HEADER;
	private String SEQ;
	private String BRHBDT;
	private String COUNT;
	private String TERM;
	private String ITR1;
	private String TITLE1;
	private String ITR2;
	private String TITLE2;
	private String ITR3;
	private String TITLE3;
	private String FILL;

	public String getRECNO() {
		return RECNO;
	}

	public String getHEADER() {
		return HEADER;
	}

	public String getSEQ() {
		return SEQ;
	}

	public String getBRHBDT() {
		return BRHBDT;
	}

	public String getCOUNT() {
		return COUNT;
	}

	public String getTERM() {
		return TERM;
	}

	public String getITR1() {
		return ITR1;
	}

	public String getTITLE1() {
		return TITLE1;
	}

	public String getITR2() {
		return ITR2;
	}

	public String getTITLE2() {
		return TITLE2;
	}

	public String getITR3() {
		return ITR3;
	}

	public String getTITLE3() {
		return TITLE3;
	}

	public String getFILL() {
		return FILL;
	}

	public void setRECNO(String rECNO) {
		RECNO = rECNO;
	}

	public void setHEADER(String hEADER) {
		HEADER = hEADER;
	}

	public void setSEQ(String sEQ) {
		SEQ = sEQ;
	}

	public void setBRHBDT(String bRHBDT) {
		BRHBDT = bRHBDT;
	}

	public void setCOUNT(String cOUNT) {
		COUNT = cOUNT;
	}

	public void setTERM(String tERM) {
		TERM = tERM;
	}

	public void setITR1(String iTR1) {
		ITR1 = iTR1;
	}

	public void setTITLE1(String tITLE1) {
		TITLE1 = tITLE1;
	}

	public void setITR2(String iTR2) {
		ITR2 = iTR2;
	}

	public void setTITLE2(String tITLE2) {
		TITLE2 = tITLE2;
	}

	public void setITR3(String iTR3) {
		ITR3 = iTR3;
	}

	public void setTITLE3(String tITLE3) {
		TITLE3 = tITLE3;
	}

	public void setFILL(String fILL) {
		FILL = fILL;
	}

}
