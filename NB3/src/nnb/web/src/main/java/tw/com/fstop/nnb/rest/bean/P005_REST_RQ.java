package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class P005_REST_RQ extends BaseRestBean_OLA implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4563036692770779938L;
	
	private String TRANNAME;//TRANNAME
	private String TxnCode;//交易代碼
	private String TxnDateTime;//交易時間
	private String SerialNo;//流水號
	private String CustomerId;//客戶ID
	private String Email;//Email
	private String Phone;//手機號碼
	private String Name;//客戶姓名
	private String Source;//申請來源別
	private String Count;//申請筆數
	private String ZoneCode;//縣市別
	private String CarId;//牌照號碼
	private String CarKind;//車種
	private String Account;//扣款帳號
	private String CardType;//扣款方式
	private String NeedConfirm;//需客戶確認扣款
	private String MailFlag;//可發通知email
	private String SMSFlag;//可發簡訊通知
	private String CardValidDate;//信用卡有效日期
	private String CardCVC2;//信用卡CVC2
	private String FGTXWAY;
	private String PINNEW;
	private String CUSIDN;
	private String ADOPID;
	
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getTRANNAME() {
		return TRANNAME;
	}
	public void setTRANNAME(String tRANNAME) {
		TRANNAME = tRANNAME;
	}
	public String getTxnCode() {
		return TxnCode;
	}
	public void setTxnCode(String txnCode) {
		TxnCode = txnCode;
	}
	public String getTxnDateTime() {
		return TxnDateTime;
	}
	public void setTxnDateTime(String txnDateTime) {
		TxnDateTime = txnDateTime;
	}
	public String getSerialNo() {
		return SerialNo;
	}
	public void setSerialNo(String serialNo) {
		SerialNo = serialNo;
	}
	public String getCustomerId() {
		return CustomerId;
	}
	public void setCustomerId(String customerId) {
		CustomerId = customerId;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public String getPhone() {
		return Phone;
	}
	public void setPhone(String phone) {
		Phone = phone;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getSource() {
		return Source;
	}
	public void setSource(String source) {
		Source = source;
	}
	public String getCount() {
		return Count;
	}
	public void setCount(String count) {
		Count = count;
	}
	public String getZoneCode() {
		return ZoneCode;
	}
	public void setZoneCode(String zoneCode) {
		ZoneCode = zoneCode;
	}
	public String getCarId() {
		return CarId;
	}
	public void setCarId(String carId) {
		CarId = carId;
	}
	public String getCarKind() {
		return CarKind;
	}
	public void setCarKind(String carKind) {
		CarKind = carKind;
	}
	public String getAccount() {
		return Account;
	}
	public void setAccount(String account) {
		Account = account;
	}
	public String getCardType() {
		return CardType;
	}
	public void setCardType(String cardType) {
		CardType = cardType;
	}
	public String getNeedConfirm() {
		return NeedConfirm;
	}
	public void setNeedConfirm(String needConfirm) {
		NeedConfirm = needConfirm;
	}
	public String getMailFlag() {
		return MailFlag;
	}
	public void setMailFlag(String mailFlag) {
		MailFlag = mailFlag;
	}
	public String getSMSFlag() {
		return SMSFlag;
	}
	public void setSMSFlag(String sMSFlag) {
		SMSFlag = sMSFlag;
	}
	public String getCardValidDate() {
		return CardValidDate;
	}
	public void setCardValidDate(String cardValidDate) {
		CardValidDate = cardValidDate;
	}
	public String getCardCVC2() {
		return CardCVC2;
	}
	public void setCardCVC2(String cardCVC2) {
		CardCVC2 = cardCVC2;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getPINNEW() {
		return PINNEW;
	}
	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}
}
