package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N625_REST_RS extends BaseRestBean implements Serializable {


	
	/**
	 * 
	 */
	private static final long serialVersionUID = -323784728769213537L;

	String USERDATA_X13; //日期
	String USERDATA_X42; //使用者名字
	String REC_NO; //筆數
	String USERDATA_X95; //???
	String USERDATA_X50; //???
	
//	OCCURS_電文陣列1
	LinkedList<N625_REST_RSDATA> REC;



	public String getUSERDATA_X13() {
		return USERDATA_X13;
	}


	public void setUSERDATA_X13(String uSERDATA_X13) {
		USERDATA_X13 = uSERDATA_X13;
	}


	public String getUSERDATA_X42() {
		return USERDATA_X42;
	}


	public void setUSERDATA_X42(String uSERDATA_X42) {
		USERDATA_X42 = uSERDATA_X42;
	}


	public String getREC_NO() {
		return REC_NO;
	}


	public void setREC_NO(String rEC_NO) {
		REC_NO = rEC_NO;
	}


	public String getUSERDATA_X95() {
		return USERDATA_X95;
	}


	public void setUSERDATA_X95(String uSERDATA_X95) {
		USERDATA_X95 = uSERDATA_X95;
	}
	

	public String getUSERDATA_X50() {
		return USERDATA_X50;
	}


	public void setUSERDATA_X50(String uSERDATA_X50) {
		USERDATA_X50 = uSERDATA_X50;
	}


	public LinkedList<N625_REST_RSDATA> getREC() {
		return REC;
	}


	public void setREC(LinkedList<N625_REST_RSDATA> rEC) {
		REC = rEC;
	}



	
	

	

}
