package tw.com.fstop.nnb.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.NumericUtil;

@Service
public class Acct_Management_Service extends Base_Service
{

	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	I18n i18n;
	/**
	 * 輕鬆理財存摺明細
	 * 
	 * @param cusidn
	 * @return
	 */
	public BaseResult passbook_details(String cusidn)
	{	
		log.trace("passbook_details_service");
		BaseResult bs = null;
		try
		{
			bs = new BaseResult();
			// call rest API
			bs = N920_REST(cusidn, MANAGEMENT);

			if (bs != null && bs.getResult())
			{
				Map<String, Object> dataMap = (Map) bs.getData();
				log.trace("dataMap ={}", dataMap);
				List<Map<String, String>> dataListMap = (List<Map<String, String>>) dataMap.get("REC");
				log.trace("dataListMap ={}", dataListMap);

				if (dataListMap != null && dataListMap.size() > 0)
				{
					// 如果需要後製可以在此做
				}
				else
				{
					String msgCode = "Z999";
					String msgName = i18n.getMsg("LB.X2140");
					bs.setMessage(msgCode, msgName);
					bs.setResult(Boolean.FALSE);
				}
			}
		}
		catch (Exception e)
		{
			log.error("demand_deposit_detail erro {}", e);
		}
		return bs;
	}

	/**
	 * 輕鬆理財存摺明細結果
	 * 
	 * @param cusidn 統一編號
	 * @param reqParam
	 * @return
	 */

	public BaseResult passbook_details_result(String cusidn, Map<String, String> reqParam)
	{
		log.trace("passbook_details_result");
		BaseResult bs = new BaseResult();
		try
		{
			log.debug(ESAPIUtil.vaildLog("passbook_details_result >> " + CodeUtil.toJson(reqParam)));
			reqParam.put("CUSIDN", cusidn);
			// call rest API
			bs = N150_REST(reqParam);
			// 後製
			if (bs != null && bs.getResult())
			{
				// 總筆數
				int CMRECNUM = 0;
				// 取得回傳bs
				Map<String, Object> dataMap = (Map) bs.getData();
				// 取得 REC
				List<Map<String, Object>> recListMap = (List<Map<String, Object>>) dataMap.get("REC");
				log.debug("recListMap >>>>> {}", recListMap);
				// 處理REC多個TABLE
				for (Map<String, Object> mapRec : recListMap)
				{
					// 如果msgCode =="0" 表示TABLE正常
					if ("0".equals(mapRec.get("msgCode")))
					{
						// 取得TABLE
						List<Map<String, String>> tableListMap = (List<Map<String, String>>) mapRec.get("TABLE");
						for (Map<String, String> maptable : tableListMap)
						{
							// 處理TABLE 欄位
							maptable.put("LSTLTD", DateUtil.convertDate(2, maptable.get("LSTLTD"), "yyyMMdd", "yyy/MM/dd"));// 最後異動日format
							maptable.put("BAL", NumericUtil.fmtAmount(maptable.get("BAL"), 2));// 結存
							// 當FORMAT=0時，支出放CODDB，收入放CODCR，否則支出和收入欄合併放DATA
							if ("0".equals(maptable.get("FORMAT")))
							{

							}
							else
							{
								maptable.put("CODDB", "");
								maptable.put("CODCR", maptable.get("DATA"));
							} // end if
						} // for end
							// 總筆數計算;正確的才算
						CMRECNUM = CMRECNUM + tableListMap.size();
					}
					// 如果msgCode不等於0則把msgCode放到第一個欄位 msgName 放到第二個欄位
					else
					{
						Map<String, String> tableMap = new LinkedHashMap<>();
						List<Map<String, String>> tableListMap = new LinkedList<Map<String, String>>();
						tableMap.put("LSTLTD", String.valueOf(mapRec.get("msgCode")));
						tableMap.put("MEMO", String.valueOf(mapRec.get("msgName")));
						tableListMap.add(tableMap);
						mapRec.put("TABLE", tableListMap);
					}
				} // for end
				dataMap.put("CMRECNUM", String.valueOf(CMRECNUM));
				// 加入查詢時間
				String cmsdate = reqParam.get("CMSDATE");
				String cmedate = reqParam.get("CMEDATE");
				dataMap.put("CMSDATE", cmsdate);
				dataMap.put("CMEDATE", cmedate);
			}
		}
		catch (Exception e)
		{
			log.error("passbook_details_result", e);
		}
		return bs;
	}

	/**
	 * 輕鬆理財證券交割明細
	 * 
	 * @param cusidn
	 * @return
	 */
	public BaseResult passbook_securities_details(String cusidn)
	{
		log.trace("passbook_details_service");
		BaseResult bs = null;
		try
		{
			bs = new BaseResult();
			// call REST API
			bs = N920_REST(cusidn, MANAGEMENT);
			{
				Map<String, Object> dataMap = (Map) bs.getData();
				log.trace("dataMap ={}", dataMap);
				List<Map<String, String>> dataListMap = (List<Map<String, String>>) dataMap.get("REC");
				log.trace("dataListMap ={}", dataListMap);

				if (dataListMap != null && dataListMap.size() > 0)
				{
					// 如果需要後製可以在此做
				}
				else
				{
					String msgCode = "Z999";
					String msgName = i18n.getMsg("LB.X2140");
					bs.setMessage(msgCode, msgName);
					bs.setResult(Boolean.FALSE);
				}
			}
		}
		catch (Exception e)
		{
			log.error("passbook_securities_details {} ", e);
		}
		return bs;
	}

	// 輕鬆理財證券交割明細查詢結果
	public BaseResult passbook_securities_details_result(String cusidn, Map<String, String> reqParam)
	{
		log.trace("passbook_securities_details_result ");
		BaseResult bs = null;
		try
		{
			log.trace(ESAPIUtil.vaildLog("reqParam >> " + CodeUtil.toJson(reqParam)));
			bs = new BaseResult();
			// call Rest API
			bs = N640_REST(cusidn, reqParam);
			log.trace("passbook_securities_details_result >>{}", bs.getData());
			if (bs != null && bs.getResult())
			{
				// 總筆數
				int CMRECNUM = 0;
				// 取得回傳bs
				Map<String, Object> dataMap = (Map) bs.getData();
				// 取得 REC
				List<Map<String, Object>> recListMap = (List<Map<String, Object>>) dataMap.get("REC");
				log.debug("passbook_securities_details_result recListMap >>>>> {}", recListMap);
				// 處理REC多個TABLE
				for (Map<String, Object> mapRec : recListMap)
				{
					// 如果msgCode =="0" 表示TABLE正常
					if ("0".equals(mapRec.get("msgCode")))
					{
						// 取得TABLE
						List<Map<String, String>> tableListMap = (List<Map<String, String>>) mapRec.get("TABLE");
						for (Map<String, String> maptable : tableListMap)
						{
							// 處理TABLE 欄位
							maptable.put("LSTLTD", DateUtil.convertDate(2, maptable.get("LSTLTD"), "yyyMMdd", "yyy/MM/dd"));// 最後異動日format
							maptable.put("BAL", NumericUtil.fmtAmount(maptable.get("BAL"), 2));//
						} // for end
							// 總筆數計算;正確的才算
						CMRECNUM = CMRECNUM + tableListMap.size();
					}
					// 如果msgCode不等於0則把msgCode放到第一個欄位 msgName 放到第二個欄位
					else
					{
						// 回傳表格
						Map<String, String> tableMap = new LinkedHashMap<>();
						// 回傳陣列
						List<Map<String, String>> tableListMap = new LinkedList<Map<String, String>>();
						// 塞入表格資料
						tableMap.put("LSTLTD", String.valueOf(mapRec.get("msgCode")));
//						tableMap.put("MEMO", String.valueOf(mapRec.get("msgName")));
						tableMap.put("MEMO", getMessageByMsgCode(String.valueOf(mapRec.get("msgCode"))));
						tableListMap.add(tableMap);
						mapRec.put("TABLE", tableListMap);
					}
				} // for end
				dataMap.put("CMRECNUM", String.valueOf(CMRECNUM));
			}
			// 訊息表格處理
			else
			{
				// 取得回傳bs
				Map<String, Object> dataMap = (Map) bs.getData();
				// 取得 REC
				List<Map<String, Object>> recListMap = (List<Map<String, Object>>) dataMap.get("REC");
				log.debug("passbook_securities_details_result--2 recListMap >>>>> {}", recListMap);
				// 處理REC多個TABLE
				for (Map<String, Object> mapRec : recListMap)
				{
					String msgCode = (String) mapRec.get("msgCode");
					// 如果 大於6碼 或是前2位 =="FE" 不做訊息表格處理
					if (msgCode.length() >= 6 || "FE".equals(msgCode.substring(0, 2)))
					{
						log.error("passbook_securities_details_result--2 msgCode >>>> {} ", msgCode);
					}
					else
					{
						// 如果msgCode不等於0則把msgCode放到第一個欄位 msgName 放到第二個欄位
						// 回傳表格
						Map<String, String> tableMap = new LinkedHashMap<>();
						// 回傳陣列
						List<Map<String, String>> tableListMap = new LinkedList<Map<String, String>>();
						// 塞入表格資料
						tableMap.put("LSTLTD", msgCode);
//						tableMap.put("MEMO", String.valueOf(dataMap.get("msgName")));
						tableMap.put("MEMO", getMessageByMsgCode(msgCode));
						//
						tableListMap.add(tableMap);
						mapRec.put("TABLE", tableListMap);
						// 表格外面
						dataMap.put("CMRECNUM", String.valueOf(0));// 總筆數
						bs.setResult(true);
					}
				} // for end

			}
		}
		catch (Exception e)
		{
			log.error("passbook_securities_details_result", e);
		}
		return bs;
	}

	// 輕鬆理財證券交割明細直接下載
	public BaseResult passbook_securities_details_DirectDownload(String cusidn, Map<String, String> reqParam)
	{
		BaseResult bs = null;
		try
		{
			// 日期處理
			bs = passbook_securities_details_result(cusidn, reqParam);
			if (bs != null)
			{
				Map<String, Object> dataMap = (Map<String, Object>) bs.getData();
				log.trace("dataMap={}", dataMap);
				Map<String, Object> parameterMap = new HashMap<String, Object>();
				parameterMap.put("CMQTIME", dataMap.get("CMQTIME"));
				parameterMap.put("CMPERIOD", dataMap.get("CMPERIOD"));
				parameterMap.put("COUNT", (dataMap.get("CMRECNUM")).toString());
				parameterMap.put("downloadFileName", reqParam.get("downloadFileName"));
				parameterMap.put("downloadType", reqParam.get("downloadType"));
				parameterMap.put("templatePath", reqParam.get("templatePath"));
				parameterMap.put("hasMultiRowData", reqParam.get("hasMultiRowData"));
				String downloadType = reqParam.get("downloadType");
				log.trace(ESAPIUtil.vaildLog("downloadType >> " + downloadType));
				log.trace("CURRANCY_PRINT={}", dataMap.get("CURRANCY_PRINT"));
				// EXCEL和OLDEXCEL下載
				if ("EXCEL".equals(downloadType) || "OLDEXCEL".equals(downloadType))
				{
					parameterMap.put("headerRightEnd", reqParam.get("headerRightEnd"));
					parameterMap.put("headerBottomEnd", reqParam.get("headerBottomEnd"));
					parameterMap.put("rowRightEnd", reqParam.get("rowRightEnd"));
					parameterMap.put("multiRowStartIndex", reqParam.get("multiRowStartIndex"));
					parameterMap.put("multiRowEndIndex", reqParam.get("multiRowEndIndex"));
					parameterMap.put("multiRowCopyStartIndex", reqParam.get("multiRowCopyStartIndex"));
					parameterMap.put("multiRowCopyEndIndex", reqParam.get("multiRowCopyEndIndex"));
					parameterMap.put("multiRowDataListMapKey", reqParam.get("multiRowDataListMapKey"));
				}
				// TXT下載
				else
				{
					parameterMap.put("txtHeaderBottomEnd", reqParam.get("txtHeaderBottomEnd"));
					parameterMap.put("txtMultiRowStartIndex", reqParam.get("txtMultiRowStartIndex"));
					parameterMap.put("txtMultiRowEndIndex", reqParam.get("txtMultiRowEndIndex"));
					parameterMap.put("txtMultiRowDataListMapKey", reqParam.get("txtMultiRowDataListMapKey"));
					parameterMap.put("txtMultiRowCopyStartIndex", reqParam.get("txtMultiRowCopyStartIndex"));
					parameterMap.put("txtMultiRowCopyEndIndex", reqParam.get("txtMultiRowCopyEndIndex"));
				}
				bs.addData("parameterMap", parameterMap);
				log.trace(ESAPIUtil.vaildLog("parameterMap >> " + CodeUtil.toJson(parameterMap)));
			}
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("outward_remittance_ajaxDirectDownload error >> {}", e);
		}
		return bs;
	}

	/**
	 * 輕鬆理財自動申購基金明細結果
	 * 
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult purchase_fund_details_result(String cusidn, Map<String, String> reqParam)
	{
		log.trace("bond_balance_result_service");
		BaseResult bs = null;
		try
		{
			bs = new BaseResult();
			bs = N650_REST(cusidn, reqParam);
			if (bs != null && bs.getResult())
			{
				Map<String, Object> callRow = (Map) bs.getData();
				ArrayList<Map<String, Object>> callTable = (ArrayList<Map<String, Object>>) callRow.get("REC");

				for (Map<String, Object> table : callTable)
				{
					ArrayList<Map<String, String>> callTableList = (ArrayList<Map<String, String>>) table.get("Table");

					for (Map<String, String> data : callTableList)
					{
						// 欄位格式化
						data.put("FUNDAMT", NumericUtil.fmtAmount(data.get("FUNDAMT"), 0));
						data.put("FUNDPNT", NumericUtil.fmtAmount(data.get("FUNDPNT"), 4));
						data.put("BYPRICE", NumericUtil.fmtAmount(data.get("BYPRICE"), 4));
						data.put("ITR", NumericUtil.fmtAmount(data.get("ITR"), 4));
						data.put("REFAVL", NumericUtil.fmtAmount(data.get("REFAVL"), 4));
						data.put("BAL", NumericUtil.fmtAmount(data.get("BAL"), 3));
					}
				}

				bs.addData("ACN1", reqParam.get("ACN1"));
			}
			bs.setResult(true);
		}
		catch (Exception e)
		{
			// e.printStackTrace();
			log.error("time_deposit", e);
		}
		return bs;
	}

	// 輕鬆理財存摺明細直接下載
	public BaseResult PassbookDetailsdirectDownload(String cusidn, Map<String, String> reqParam)
	{
		BaseResult bs = null;
		try
		{
			bs = passbook_details_result(cusidn, reqParam);
			if (bs != null && bs.getResult())
			{
				Map<String, Object> dataMap = (Map) bs.getData();
				log.trace("dataMap={}", dataMap);
				List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("REC");
				log.debug("rowListMap={}", rowListMap);
				Map<String, Object> parameterMap = new HashMap<String, Object>();

				parameterMap.put("CMQTIME", dataMap.get("CMQTIME"));
				parameterMap.put("CMPERIOD", dataMap.get("CMPERIOD"));
				parameterMap.put("CMRECNUM", dataMap.get("CMRECNUM"));
				parameterMap.put("ACN", dataMap.get("ACN"));

				parameterMap.put("downloadFileName", reqParam.get("downloadFileName"));
				parameterMap.put("downloadType", reqParam.get("downloadType"));
				parameterMap.put("templatePath", reqParam.get("templatePath"));
				parameterMap.put("hasMultiRowData", reqParam.get("hasMultiRowData"));

				String downloadType = reqParam.get("downloadType");
				log.trace(ESAPIUtil.vaildLog("downloadType >> " + downloadType));
				// EXCEL和OLDEXCEL下載
				if ("EXCEL".equals(downloadType) || "OLDEXCEL".equals(downloadType))
				{
					parameterMap.put("headerRightEnd", reqParam.get("headerRightEnd"));
					parameterMap.put("headerBottomEnd", reqParam.get("headerBottomEnd"));
					parameterMap.put("rowRightEnd", reqParam.get("rowRightEnd"));
					parameterMap.put("multiRowStartIndex", reqParam.get("multiRowStartIndex"));
					parameterMap.put("multiRowEndIndex", reqParam.get("multiRowEndIndex"));
					parameterMap.put("multiRowCopyStartIndex", reqParam.get("multiRowCopyStartIndex"));
					parameterMap.put("multiRowCopyEndIndex", reqParam.get("multiRowCopyEndIndex"));
					parameterMap.put("multiRowDataListMapKey", reqParam.get("multiRowDataListMapKey"));

				}
				// TXT下載
				else
				{
					parameterMap.put("txtHeaderBottomEnd", reqParam.get("txtHeaderBottomEnd"));
					parameterMap.put("txtMultiRowStartIndex", reqParam.get("txtMultiRowStartIndex"));
					parameterMap.put("txtMultiRowEndIndex", reqParam.get("txtMultiRowEndIndex"));
					parameterMap.put("txtMultiRowCopyStartIndex", reqParam.get("txtMultiRowCopyStartIndex"));
					parameterMap.put("txtMultiRowCopyEndIndex", reqParam.get("txtMultiRowCopyEndIndex"));
					parameterMap.put("txtMultiRowDataListMapKey", reqParam.get("txtMultiRowDataListMapKey"));
				}
				bs.addData("parameterMap", parameterMap);
			}

		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("PassbookDetailsdirectDownload error >> {}", e);
		}
		return bs;
	}

}
