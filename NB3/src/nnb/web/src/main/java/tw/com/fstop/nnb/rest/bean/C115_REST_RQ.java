package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * C115電文RQ
 */
public class C115_REST_RQ extends BaseRestBean_FUND implements Serializable{
	
	private static final long serialVersionUID = 7272105946161521442L;
	
	private String SSUP ;
	private String SNID ;
	private String ACF ;
	private String MSGTYPE ;
	private String PROCCODE ;
	private String SYSTRACE ;
	private String TXNDIID ;
	private String TXNSIID ;
	private String TXNIDT ;
	private String RESPCOD ;
	private String SYNCCHK ;
	private String BITMAPCFG ;
	private String NEXT ;
	private String RECNO ;
	private String CUSIDN ;
	private String ALTERTYPE ;
	private String MAC ;
	
	public String getSSUP() {
		return SSUP;
	}
	public void setSSUP(String sSUP) {
		SSUP = sSUP;
	}
	public String getSNID() {
		return SNID;
	}
	public void setSNID(String sNID) {
		SNID = sNID;
	}
	public String getACF() {
		return ACF;
	}
	public void setACF(String aCF) {
		ACF = aCF;
	}
	public String getMSGTYPE() {
		return MSGTYPE;
	}
	public void setMSGTYPE(String mSGTYPE) {
		MSGTYPE = mSGTYPE;
	}
	public String getPROCCODE() {
		return PROCCODE;
	}
	public void setPROCCODE(String pROCCODE) {
		PROCCODE = pROCCODE;
	}
	public String getSYSTRACE() {
		return SYSTRACE;
	}
	public void setSYSTRACE(String sYSTRACE) {
		SYSTRACE = sYSTRACE;
	}
	public String getTXNDIID() {
		return TXNDIID;
	}
	public void setTXNDIID(String tXNDIID) {
		TXNDIID = tXNDIID;
	}
	public String getTXNSIID() {
		return TXNSIID;
	}
	public void setTXNSIID(String tXNSIID) {
		TXNSIID = tXNSIID;
	}
	public String getTXNIDT() {
		return TXNIDT;
	}
	public void setTXNIDT(String tXNIDT) {
		TXNIDT = tXNIDT;
	}
	public String getRESPCOD() {
		return RESPCOD;
	}
	public void setRESPCOD(String rESPCOD) {
		RESPCOD = rESPCOD;
	}
	public String getSYNCCHK() {
		return SYNCCHK;
	}
	public void setSYNCCHK(String sYNCCHK) {
		SYNCCHK = sYNCCHK;
	}
	public String getBITMAPCFG() {
		return BITMAPCFG;
	}
	public void setBITMAPCFG(String bITMAPCFG) {
		BITMAPCFG = bITMAPCFG;
	}
	public String getNEXT() {
		return NEXT;
	}
	public void setNEXT(String nEXT) {
		NEXT = nEXT;
	}
	public String getRECNO() {
		return RECNO;
	}
	public void setRECNO(String rECNO) {
		RECNO = rECNO;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getALTERTYPE() {
		return ALTERTYPE;
	}
	public void setALTERTYPE(String aLTERTYPE) {
		ALTERTYPE = aLTERTYPE;
	}
	public String getMAC() {
		return MAC;
	}
	public void setMAC(String mAC) {
		MAC = mAC;
	}
	
}