package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class NB31_1_REST_RQ extends BaseRestBean_OLA implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4163661534334002266L;

	private String  CUSIDN;//統一編號
	private String  ACN;//數位帳號
	private String  NAATAPPLY;//是否申請非約定轉帳
	private String  CChargeApply;//是否申請消費扣款
	private String CROSSAPPLY;//是否申請跨國交易
	private String  SEND;//卡片領取方式
	private String  CRDTYP;//卡片種類
	private String  LMT;//消費扣款日限額
	private String  TRNTYP;//交易類別
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getCRDTYP() {
		return CRDTYP;
	}
	public void setCRDTYP(String cRDTYP) {
		CRDTYP = cRDTYP;
	}
	public String getLMT() {
		return LMT;
	}
	public void setLMT(String lMT) {
		LMT = lMT;
	}
	public String getTRNTYP() {
		return TRNTYP;
	}
	public void setTRNTYP(String tRNTYP) {
		TRNTYP = tRNTYP;
	}
	public String getNAATAPPLY() {
		return NAATAPPLY;
	}
	public void setNAATAPPLY(String nAATAPPLY) {
		NAATAPPLY = nAATAPPLY;
	}
	public String getCChargeApply() {
		return CChargeApply;
	}
	public void setCChargeApply(String cChargeApply) {
		CChargeApply = cChargeApply;
	}
	public String getCROSSAPPLY() {
		return CROSSAPPLY;
	}
	public void setCROSSAPPLY(String cROSSAPPLY) {
		CROSSAPPLY = cROSSAPPLY;
	}
	public String getSEND() {
		return SEND;
	}
	public void setSEND(String sEND) {
		SEND = sEND;
	}
}
