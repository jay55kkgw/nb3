package tw.com.fstop.nnb.spring.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.google.gson.Gson;

import fstop.orm.po.TXNBONDDATA;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.service.Bond_Purchase_Service;
import tw.com.fstop.nnb.service.Kyc_Service;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.util.DateUtil;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.WebUtil;

/**
 * 海外債申購 Controller
 */
@SessionAttributes({ SessionUtil.CUSIDN, SessionUtil.KYCDATE, SessionUtil.WEAK, SessionUtil.XMLCOD,
		SessionUtil.DPMYEMAIL, SessionUtil.KYC, SessionUtil.STEP1_LOCALE_DATA, SessionUtil.STEP2_LOCALE_DATA,
		SessionUtil.TRANSFER_CONFIRM_TOKEN, SessionUtil.TRANSFER_RESULT_TOKEN, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN,
		SessionUtil.STEP3_LOCALE_DATA, SessionUtil.CONFIRM_LOCALE_DATA, SessionUtil.IDGATE_TRANSDATA,
		SessionUtil.RESULT_LOCALE_DATA, SessionUtil.USERIP })
@Controller
@RequestMapping(value = "/BOND/PURCHASE")
public class Bond_Purchase_Controller {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	I18n i18n;

	@Autowired
	Bond_Purchase_Service bond_purchase_service;

	@Autowired
	private Kyc_Service kyc_Service;

	/**
	 * 海外債輸入頁先前檢核
	 * 
	 * 1. 是否有約定海外債券「海外債券特別約定事項暨同意書」 ( B017 ) 2. 檢核有無超過交易時間 ( AM9:00~PM2:30 ) 3.
	 * KYC是否超過一年 ( N922 ) 4. 是否有約定網銀外幣入扣帳帳號 ( N922 )
	 * 
	 * 頁面特殊提示視窗 1. 對帳單退件或e-mail與他人相同之客戶，其使用網銀申購海外債券交易比照基金網銀交易時出示提醒視窗 ( B017
	 * EMAILMSG欄位嗎 ? )
	 * 
	 * 未約定(3)網銀外幣入扣帳號，出訊息 ( 請至分行臨櫃簽署約定網銀外幣帳號 )。
	 * 
	 * 
	 * 未簽署(4)特別約定事項，出訊息: 您尚未簽屬「海外債券特別約定事項暨同意書」，簽屬後才可進行申購，是否需要線上簽屬「海外債券特別約定事項暨同意書」。
	 * 
	 * 
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/bond_purchase_input")
	public String bond_purchase_input(HttpServletRequest request, @RequestParam Map<String, String> requestParam,
			Model model) {

		String target = "/error";
		log.info("<<< bond_purchase_input >>>>");
		// 最後用來set 頁面值
		BaseResult bs = new BaseResult();
		// N922 電文回傳
		BaseResult bsN922 = new BaseResult();
		// B017 電文回傳
		BaseResult bsB017 = new BaseResult();

		// 檢核過不過 flag
		Boolean checkPoint = true;

		try {

			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);

			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = bond_purchase_service.getTxToken();
			log.trace("bond_purchase_input.TXTOKEN: " + ((Map<String, String>) bs.getData()).get("TXTOKEN"));
			if (bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN,
						((Map<String, String>) bs.getData()).get("TXTOKEN"));
			}

			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"UTF-8");
			log.debug("cusidn={}", cusidn);
			String ip = WebUtil.getIpAddr(request);

			okMap.put("CUSIDN", cusidn);
			okMap.put("IP", ip);

			// 先打 N922 取得檢核1 ( B017 )需要的 BRHCOD >> N922的 APLBRH
			bsN922 = bond_purchase_service.N922_REST(cusidn);

			// N922 失敗情況 : return
			if (bsN922 == null || !bsN922.getResult()) {
				checkPoint = false;
				bs = bsN922 ;
				bs.setPrevious("/INDEX/index");
				return target;
			}

			Map<String, Object> dataN922 = (Map<String, Object>) bsN922.getData();

			bs.addAllData(dataN922);

			okMap.put("BRHCOD", (String) dataN922.get("APLBRH"));

			bs.addAllData(okMap);

			// 檢核1 是否有約定海外債券「海外債券特別約定事項暨同意書」 ( B017 )
			bsB017 = bond_purchase_service.B017_REST(okMap);

			if (bsB017 != null && bsB017.getResult()) {
				Map<String, Object> dataB017 = (Map<String, Object>) bsB017.getData();
				// 測試用 EMAILMSG有值
//				dataB017.put("EMAILMSG", "11");
				
				bs.addAllData(dataB017);
				// 測試用 未簽署同意書
//				 dataB017.put("AGREE", "N");

				// 沒簽署同意書
				if ("N".equals(dataB017.get("AGREE"))) {
					checkPoint = false;
					target = "/bond/bond_deal_sign_alert";
					return target;
				}

			} else {
				checkPoint = false;
				// B017 失敗情況 : return
				bs = bsB017 ;
				bs.setPrevious("/INDEX/index");
				return target;
			}

			// 檢核1 結束

			// 檢核2 檢核有無超過交易時間 ( AM9:00~PM2:30 )
			Boolean inTradingHour = bond_purchase_service.inTradingHour();

			if (!inTradingHour) {
				checkPoint = false;
				bs.setErrorMessage("B162", i18n.getMsg("LB.X2585"));
				bs.setResult(false);
				bs.setPrevious("/INDEX/index");
				model.addAttribute(BaseResult.ERROR, bs);
				return target;
			}
			// 檢核2 結束

			// 檢核3 KYC是否超過一年 ( N922 )
			// 問卷版本日期 FROM 資料庫
			String KYCDate = kyc_Service.getKYCDate();
			KYCDate = (null != KYCDate) ? KYCDate : "";
			// 投資屬性
			String FDINVTYPE = dataN922.get("FDINVTYPE") == null ? "" : (String) dataN922.get("FDINVTYPE");
			// 問卷填寫日期 (民國年 10001010 , 0990101)
			String GETLTD = dataN922.get("GETLTD") == null ? "" : (String) dataN922.get("GETLTD");
			// 專業投資人屬性
			String RISK7 = dataN922.get("RISK7") == null ? "" : ((String) dataN922.get("RISK7")).trim();

			String date1 = "";
			String date2 = "";
			long day = 0;
			if (!"".equals(GETLTD)) {
				date1 = new SimpleDateFormat("yyyy/MM/dd").format(DateUtil.twconvertDate(GETLTD));
				date2 = new SimpleDateFormat("yyyy/MM/dd").format(new Date());
			}
			day = kyc_Service.getRangeDay(date2, date1);

			// 自然人版
			// 需有投資屬性及問卷填寫日期已滿一年
			// 或 問卷版本日期大於問卷填寫日期
			// 專業投資人跳過不判斷KYC
			if (((!FDINVTYPE.equals("") && !GETLTD.equals("") && day >= 365)
					|| ((!GETLTD.equals("") && !KYCDate.equals(""))
							&& Integer.parseInt(KYCDate) > Integer.parseInt(GETLTD)))
					&& cusidn.length() == 10 && RISK7.length() == 0) {
				if (day >= 365) {
					// //TODO:待改
					 model.addAttribute("UID",cusidn);
					 model.addAttribute("TXID","B019");
					 model.addAttribute("ALERTTYPE","2");
					 target = "/fund/fund_risk_alert";
				} else {
					// //TODO:待改
					 target = "forward:/FUND/TRANSFER/fund_invest_attr1?TXID=B019";
				}
				return target;
			}
			// 法人版，需有投資屬性及問卷填寫日期已滿一年或問卷版本日期大於問卷填寫日期；專業投資人跳過不判斷KYC
			else if (((!FDINVTYPE.equals("") && !GETLTD.equals("") && day >= 365)
					|| ((!GETLTD.equals("") && !KYCDate.equals(""))
							&& Integer.parseInt(KYCDate) > Integer.parseInt(GETLTD)))
					&& cusidn.length() < 10 && RISK7.length() == 0) {
				if (day >= 365) {
					// //TODO:待改
					 model.addAttribute("UID",cusidn);
					 model.addAttribute("TXID","B019");
					 model.addAttribute("ALERTTYPE","2");
					 target = "/fund/fund_risk_alert";
				} else {
					// //TODO:待改
					 target = "forward:/FUND/TRANSFER/fund_invest_attr3?TXID=B019";
				}
				return target;
			}
			else if(FDINVTYPE.equals("") && RISK7.length() == 0){
		     	if(cusidn.length() == 10){
					target = "forward:/FUND/TRANSFER/fund_invest_attr1?TXID=B019";
		     	}
		    	else{
					target = "forward:/FUND/TRANSFER/fund_invest_attr3?TXID=B019";
		    	}
		     	return target;
		    }

			// 檢核3 結束

			// 檢核4 是否有約定網銀外幣入扣帳帳號 ( N922 )
			String inacn = (String) dataN922.get("ACN2");
			String outacn = (String) dataN922.get("ACN4");
			// 測試用 未約定網銀外幣帳號
//			 inacn="";
			// outacn="";
			// 任一沒有就錯誤頁
			if (StrUtil.isEmpty(inacn) || StrUtil.isEmpty(outacn)) {
				checkPoint = false;
				bs.setErrorMessage("ZX99", i18n.getMsg("LB.X2586"));
				bs.setResult(false);
				bs.setPrevious("/INDEX/index");
				model.addAttribute(BaseResult.ERROR, bs);
				return target;
			}
			// 檢核4 結束

			bs = bond_purchase_service.input_page_data1(bs);
			SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, bs);
		} catch (Exception e) {
			log.error("bond_purchase_input error >> {}", e);
		} finally {
			if (checkPoint) {
				model.addAttribute("bond_purchase_input", bs);
				target = "/bond/bond_purchase_input";
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * 海外債券申購 輸入頁2
	 * 
	 * @param request
	 * @param requestParam
	 * @param model
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/bond_purchase_input2")
	public String bond_purchase_input2(HttpServletRequest request, @RequestParam Map<String, String> requestParam,
			Model model) {
		String target = "/error";
		Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
		BaseResult bs = new BaseResult();
		BaseResult bsB018 = new BaseResult();
		log.info("<<< bond_purchase_input2 >>>>");

		try {

			bs = new BaseResult();
			if (StrUtil.isNotEmpty(okMap.get("TOKEN")) && okMap.get("TOKEN")
					.equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null))) {
				bs.setResult(Boolean.TRUE);
			}
			log.trace("TRANSFER_CONFIRM_TOKEN CHECK >>{}", bs.getResult());
			if (!bs.getResult()) {
				log.trace("throw new Exception()");
				throw new Exception();
			}

			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"UTF-8");

			log.debug("cusidn={}", cusidn);
			okMap.put("CUSIDN", cusidn);

			String ip = WebUtil.getIpAddr(request);
			okMap.put("IP", ip);

			// 取得申購價格
			bsB018 = bond_purchase_service.input_page_data2(okMap);

			if (bsB018 != null && bsB018.getResult()) {

				bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null);

				Map<String, String> dataB018 = (Map<String, String>) bsB018.getData();

				bs.addAllData(dataB018);

				// B018電文的BUYPRICE
				bs.addData("BUYPRICE_FMT", NumericUtil.formatNumberString(dataB018.get("BUYPRICE"), 4));

				// okMap所選債券的 BONDNAME 及 BONDCRY
				bs.addData("BONDNAME", (String) CodeUtil.fromJson(okMap.get("BONDDATA"), Map.class).get("BONDNAME"));
				bs.addData("BONDCRY", (String) CodeUtil.fromJson(okMap.get("BONDDATA"), Map.class).get("BONDCRY"));
				bs.addData("BONDCODE", (String) CodeUtil.fromJson(okMap.get("BONDDATA"), Map.class).get("BONDCODE"));
				
				bs.addAllData(okMap);
				SessionUtil.addAttribute(model, SessionUtil.STEP2_LOCALE_DATA, bs);

			} else {

				model.addAttribute(BaseResult.ERROR, bsB018);
				return target;

			}
		} catch (Exception e) {
			log.error("bond_purchase_input2 error >> {}", e);
		} finally {
			if (bs != null && bs.getResult()) {

				// 前面頁面帶的TOKEN繼續往下一頁帶 ( 因為還是輸入頁 )
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, okMap.get("TOKEN"));
				model.addAttribute("bond_purchase_input2", bs);

				target = "/bond/bond_purchase_input2";
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}

		return target;
	}

	/**
	 * 海外債券申購 取得購買債券明細 (B019)
	 * 
	 * @param request
	 * @param requestParam
	 * @param model
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/bond_purchase_detail_check")
	public String bond_purchase_detail_check(HttpServletRequest request, @RequestParam Map<String, String> requestParam,
			Model model) {
		String target = "/error";
		BaseResult bs = new BaseResult();
		BaseResult bsB019 = new BaseResult();
		try {
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"UTF-8");
			log.debug("cusidn={}", cusidn);
			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);

			bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.STEP2_LOCALE_DATA, null);

			bs.addAllData(okMap);

			Map<String, Object> bsData = (Map) bs.getData();

			Map<String, String> b019_1_in = new HashMap<String, String>();

			processB019_1_params(b019_1_in, bsData);

			log.debug("B019_01_REST reqParam >> {}", b019_1_in);
			bsB019 = bond_purchase_service.B019_REST(b019_1_in);

			if (bsB019 != null && bsB019.getResult()) {

				bs.addAllData((Map<String, String>) bsB019.getData());
				SessionUtil.addAttribute(model, SessionUtil.STEP3_LOCALE_DATA, bs);

			} else {
				model.addAttribute(BaseResult.ERROR, bsB019);
				return target;
			}

		} catch (Exception e) {
			log.error("bond_purchase_detail_check error >> {}", e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "forward:/BOND/PURCHASE/bond_information1";
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * 商品說明暨風險預告書 , 商品說明書(英文版) 導頁
	 * 
	 * @param request
	 * @param requestParam
	 * @param model
	 * @return
	 */
	// 聲明書P1 > 商品說明暨風險預告書 , 商品說明書(英文版) ,
	// 本人已自行判斷本投資並承擔風險，投資實際損益之計算方式以商品說明暨風險預告書/所載條款為準
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/bond_information1")
	public String bond_information_1(HttpServletRequest request, @RequestParam Map<String, String> requestParam,
			Model model) {
		String target = "/error";
		Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
		log.info("<<< bond_information1 >>>>");

		BaseResult bs = new BaseResult();
		try {
			if (StrUtil.isNotEmpty(okMap.get("TOKEN")) && okMap.get("TOKEN")
					.equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null))) {
				bs.setResult(Boolean.TRUE);
			}

			if (!bs.getResult()) {
				log.error(ESAPIUtil.vaildLog("bond_information1 TOKEN 不正確  >> " + okMap.get("TOKEN")));
				throw new Exception();
			}

			bs.addData("PRDNO", ((Map<String, String>) ((BaseResult) SessionUtil.getAttribute(model,
					SessionUtil.STEP3_LOCALE_DATA, null)).getData()).get("PRDNO"));

		} catch (Exception e) {
			log.error("bond_information1 error >> {}", e);
		} finally {
			if (bs != null && bs.getResult()) {
				log.debug("test get bs.data>>>" + bs.getData());
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, okMap.get("TOKEN"));
				model.addAttribute("bond_information1", bs);
				target = "/bond/bond_information1";
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}

		return target;
	}

	/**
	 * 主動購買聲明書 導頁
	 * 
	 * @param request
	 * @param requestParam
	 * @param model
	 * @return
	 */
	// 聲明書P2 > 主動購買聲明書
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/bond_information2")
	public String bond_information_2(HttpServletRequest request, @RequestParam Map<String, String> requestParam,
			Model model) {

		String target = "/error";
		Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
		log.info("<<< bond_information2 >>>>");

		BaseResult bs = new BaseResult();
		try {
			if (StrUtil.isNotEmpty(okMap.get("TOKEN")) && okMap.get("TOKEN")
					.equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null))) {
				bs.setResult(Boolean.TRUE);
			}

			if (!bs.getResult()) {
				log.error(ESAPIUtil.vaildLog("bond_information2 TOKEN 不正確  >> " + okMap.get("TOKEN")));
				throw new Exception();
			}

			log.trace("bs.getResult() >>{}", bs.getResult());
		} catch (Exception e) {
			log.error("bond_information2 error >> {}", e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/bond/bond_information2";
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}

		return target;
	}

	/**
	 * 高齡聲明書 導頁
	 * 
	 * @param request
	 * @param requestParam
	 * @param model
	 * @return
	 */
	// 聲明書P3 > 高齡聲明書
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/bond_information3")
	public String bond_information_3(HttpServletRequest request, @RequestParam Map<String, String> requestParam,
			Model model) {

		String target = "/error";
		Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
		log.info("<<< bond_information3 >>>>");

		BaseResult bs = new BaseResult();
		try {
			if (StrUtil.isNotEmpty(okMap.get("TOKEN")) && okMap.get("TOKEN")
					.equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null))) {
				bs.setResult(Boolean.TRUE);
			}
			
			bs = (BaseResult) SessionUtil.getAttribute(model,SessionUtil.STEP3_LOCALE_DATA, null);
			
			if (!bs.getResult()) {
				log.error(ESAPIUtil.vaildLog("bond_information3 TOKEN 不正確  >> " + okMap.get("TOKEN")));
				throw new Exception();
			}

		} catch (Exception e) {
			log.error("bond_information3 error >> {}", e);
		} finally {
			if (bs != null && bs.getResult()) {
				// 測試用
//				bs.addData("O15", "Y");

				Map<String, Object> bsData = (Map) bs.getData();
				// 是否需要填寫高齡聲明書
				if (bsData.get("O15").equals("Y")) {
					target = "/bond/bond_information3";
				} else {
					target = "forward:/BOND/PURCHASE/bond_purchase_confirm";
				}
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}

		return target;
	}

	/**
	 * 海外債券申購確認頁
	 * 
	 * @param request
	 * @param requestParam
	 * @param model
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/bond_purchase_confirm")
	public String bond_purchase_confirm(HttpServletRequest request, @RequestParam Map<String, String> requestParam,
			Model model) {
		String target = "/error";
		Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
		log.info("<<< bond_purchase_confirm >>>>");

		BaseResult bs = new BaseResult();
		try {
			if (StrUtil.isNotEmpty(okMap.get("TOKEN")) && okMap.get("TOKEN")
					.equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null))) {
				bs.setResult(Boolean.TRUE);
			}

			if (!bs.getResult()) {
				log.error(ESAPIUtil.vaildLog("bond_purchase_confirm TOKEN 不正確  >> " + okMap.get("TOKEN")));
				throw new Exception();
			}

			bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.STEP3_LOCALE_DATA, null);

			bs = bond_purchase_service.confirm_page_data(bs);

		} catch (Exception e) {
			log.error("bond_purchase_confirm error >> {}", e);
		} finally {
			if (bs != null && bs.getResult()) {

				target = "/bond/bond_purchase_confirm";
				model.addAttribute("bond_purchase_confirm", bs);

			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}

		return target;
	}

	/**
	 * 海外債券申購結果頁
	 * 
	 * @param request
	 * @param requestParam
	 * @param model
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/bond_purchase_result")
	public String bond_purchase_result(HttpServletRequest request, @RequestParam Map<String, String> requestParam,
			Model model) {
		String target = "/error";
		Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
		log.info("<<< bond_purchase_result >>>>");
		log.info("<<< bond_purchase_result >>>>" + okMap);
		
		BaseResult bs = new BaseResult();
		BaseResult bsN951 = new BaseResult();
		BaseResult bsB019 = new BaseResult();
		try {
			if (StrUtil.isNotEmpty(okMap.get("TOKEN")) && okMap.get("TOKEN")
					.equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null))) {
				bs.setResult(Boolean.TRUE);
			}
			log.trace("bs.getResult() >>{}", bs.getResult());
			
			if (!bs.getResult()) {
				log.trace("throw new Exception()");
				throw new Exception();
			}
			
			bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.STEP3_LOCALE_DATA, null);

			Map<String, Object> bsData = (Map) bs.getData();
			
			Map<String, String> b019_2_in = new HashMap<String, String>();
			
			//串上行
			processB019_2_params(b019_2_in, bsData);
			
			bsN951 = bond_purchase_service.N951_REST(b019_2_in.get("CUSIDN"),okMap);										
			
			if(bsN951.getMsgCode().equals("0")) {
				
				log.debug("B019_02_REST reqParam >> {}", b019_2_in);
				bsB019 = bond_purchase_service.B019_REST(b019_2_in);
				
				
				if (bsB019 != null && bsB019.getResult()) {

					bs.addAllData((Map)bsB019.getData());

				} else {
					model.addAttribute(BaseResult.ERROR, bsB019);
					return target;
				}
			
			}else {
				bs.setErrorMessage(bsN951.getMsgCode(), bsN951.getMessage());
				bs.setResult(false);
				log.trace(bs.getMsgCode());
			}
			
		} catch (Exception e) {
			log.error("bond_purchase_result error >> {}", e);
		} finally {
			if (bs != null && bs.getResult()) {
				log.debug("test result data>>>" + bs.getData());
				target = "/bond/bond_purchase_result";
				model.addAttribute("bond_purchase_result", bs);
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}

		return target;
	}

	/**
	 * 依照TRANSCODE取得基金的資料的AJAX
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/getBondDataAjax", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody BaseResult getBondDataAjax(@RequestParam Map<String, String> requestParam) {
		log.info("getBondDataAjax");

		BaseResult baseResult = new BaseResult();
		baseResult.setResult(Boolean.FALSE);

		try {
			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);

			log.debug("getBondDataAjax requestParam >> {}", okMap);

			String FDINVTYPE = okMap.get("FDINVTYPE");

			String RISK7 = (null != okMap.get("RISK7")) ? okMap.get("RISK7") : "";

			List<Map<String, String>> dataList = new ArrayList<Map<String, String>>();
			// 只顯示符合客戶投資屬性之商品
			List<TXNBONDDATA> bondDataList = bond_purchase_service.getBondData(FDINVTYPE, RISK7);

			if (bondDataList != null) {

				for (TXNBONDDATA eachData : bondDataList) {
					Map<String, String> map = new HashMap<String, String>();

					map = CodeUtil.objectCovert(map.getClass(), eachData);

					dataList.add(map);
				}

				baseResult.setResult(Boolean.TRUE);
				String dataString = new Gson().toJson(dataList, dataList.getClass());
				log.debug(ESAPIUtil.vaildLog("dataString=" + dataString));

				baseResult.setData(dataString);
			}
		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("getBondDataAjax error >> {}", e);
		}
		return baseResult;
	}

	public void processB019_1_params(Map<String, String> b019_1_in, Map<String, Object> bsData) {

		// "CUSIDN":"A123456814",
		// "BRHCOD":"010",
		// "I01": "1",
		// "I02":"AA01",
		// "I03":"1197200",
		// "I04":"300000",
		// "I05":"",
		// "I06":"",
		// "I07":"",
		// "I08":"",
		// "I09":"",
		// "I10":"00150975220",
		// "I11":"",
		// "I12":"",
		// "I13":"USD",
		// "I14":" ",
		// "I15":"2",
		// "I16":" ",
		// "I17":" ",
		// "I18":" ",
		// "I19":" ",
		// "IP": "10.10.220.69"

		b019_1_in.put("CUSIDN", (String) bsData.get("CUSIDN"));
		b019_1_in.put("BRHCOD", (String) bsData.get("APLBRH"));
		// 類別(1:檢核 2:確認)
		b019_1_in.put("I01", "1");
		// 商品代號
		b019_1_in.put("I02", (String) bsData.get("PRDNO"));
		// 申購價格
		b019_1_in.put("I03", (String) bsData.get("BUYPRICE"));
		// 委買面額
		b019_1_in.put("I04", (String) bsData.get("AMOUNT"));
		// 扣款帳號
		b019_1_in.put("I10", (String) bsData.get("ACN2"));
		// 幣別
		b019_1_in.put("I13", (String) bsData.get("BONDCRY"));
		// 專業投資人
		b019_1_in.put("I14", (String) bsData.get("RISK7"));
		// 投資人風險等級
		b019_1_in.put("I15", (String) bsData.get("FDINVTYPE"));
		// 投資人風險等級
		b019_1_in.put("IP", (String) bsData.get("IP"));
		
		log.info("b019_1_in >> {}",b019_1_in.toString());
	}

	public void processB019_2_params(Map<String, String> b019_2_in, Map<String, Object> bsData) {

		// "CUSIDN":"A123456814",
		// "BRHCOD":"010",
		// "I01": "2",
		// "I02":"AA02",
		// "I03":"0001098800",
		// "I04":"300000",
		// "I05":"+",
		// "I06":"000000001",
		// "I07":"00150000",
		// "I08":"001077480",
		// "I09":"00036993481",
		// "I10":"00150975220",
		// "I11":"",
		// "I12":"",
		// "I13":"USD",
		// "I14":"",
		// "I15":"2",
		// "I16":"Y",
		// "I17":"Y",
		// "I18":"Y",
		// "I19":"",
		// "IP": "10.10.220.69"

		b019_2_in.put("CUSIDN", (String) bsData.get("CUSIDN"));
		b019_2_in.put("BRHCOD", (String) bsData.get("APLBRH"));
		// 類別(1:檢核 2:確認)
		b019_2_in.put("I01", "2");
		// 商品代號
		b019_2_in.put("I02", (String) bsData.get("PRDNO"));
		// 申購價格
		b019_2_in.put("I03", (String) bsData.get("BUYPRICE"));
		// 委買面額
		b019_2_in.put("I04", (String) bsData.get("AMOUNT"));
		// 前手息(+/-)
		b019_2_in.put("I05", (String) bsData.get("O08"));
		// 前手息
		b019_2_in.put("I06", (String) bsData.get("O09"));
		// 手續費率
		b019_2_in.put("I07", (String) bsData.get("O10"));
		// 預估手續費
		b019_2_in.put("I08", (String) bsData.get("O11"));
		// 預估圈存金額
		b019_2_in.put("I09", (String) bsData.get("O12"));
		// 扣款帳號
		b019_2_in.put("I10", (String) bsData.get("ACN2"));
		// 轉介行員(中心)
		b019_2_in.put("I11", (String) bsData.get("EMPNO"));
		// 推薦行員(客戶)
		b019_2_in.put("I12", (String) bsData.get("I12"));
		// 幣別
		b019_2_in.put("I13", (String) bsData.get("BONDCRY"));
		// 專業投資人
		b019_2_in.put("I14", (String) bsData.get("RISK7"));
		// 投資人風險等級
		b019_2_in.put("I15", (String) bsData.get("FDINVTYPE"));
		// 風險預告書(Y/N)
		b019_2_in.put("I16", "Y");
		// 自主投資聲明書(Y/N)
		b019_2_in.put("I17", "Y");
		// 商品說明書(Y/N)
		b019_2_in.put("I18", "Y");
		// 高齡聲明書(Y/N)
		if("Y".equals((String) bsData.get("O15"))) {
			b019_2_in.put("I19", "Y");
		}
		if("Y".equals((String) bsData.get("MAILSIGN"))) {
			b019_2_in.put("I20", "Y");
		}
		// IP
		b019_2_in.put("IP", (String) bsData.get("IP"));
		
		b019_2_in.put("FGTXWAY", "0");
		
		log.info("b019_2_in >> {}",b019_2_in.toString());
	}
}
