package tw.com.fstop.nnb.spring.controller;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Base64;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.service.Component_Service;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.StrUtil;

@Controller  
@RequestMapping("/COMPONENT")
@SessionAttributes({SessionUtil.CUSIDN,SessionUtil.XMLCOD})
public class Component_Controller {

	static Logger log = LoggerFactory.getLogger(Component_Controller.class);
	
	@Autowired
   	ServletContext servletContext;
	
	@Autowired
	private I18n i18n;
	
	@Autowired
	private Component_Service component_service;
	
	@Autowired
	private Login_out_Controller login_out_controller;
	
	/**
	 * 元件_進入點
	 * 
	 * @return
	 */
	@RequestMapping(value = "/startup")
	public String index(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.debug("component_startup...");
		
		model.addAttribute("jsondc", "123");
		
		String target = "/component/component_demo";
		log.trace("target: " + target);
		return target;
	}
	
	/**
	 * 元件_判斷使用者是否IKEY使用者
	 * 
	 * @return
	 */
	@RequestMapping(value = "/isikeyuser_aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult isIkeyUser(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("isIkeyUser...");
		BaseResult bs = null;
		String xmlcod = "false";
		try {
			bs = new BaseResult();
			
			xmlcod = (String) SessionUtil.getAttribute(model, SessionUtil.XMLCOD, null);
			
			// session有無XMLCOD
			if( !"".equals(xmlcod) && xmlcod != null ) {
				log.trace("xmlcod: " + xmlcod);
				// 判斷使用者是否IKEY使用者
				bs = component_service.isIkeyUser(xmlcod);
			}else {
				bs.setMsgCode("0");
				bs.setMessage(i18n.getMsg("LB.X1762"));
				log.error("session no XMLCOD!!!");
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("isIkeyUser error >> {}",e);
		}
		return bs;
	}
	
	/**
	 * 元件_取得各元件的最新版本號
	 * 
	 * @return
	 */
	@RequestMapping(value = "/component_version_aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult componentVersion(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("componentVersion...");
		BaseResult bs = null;
		try {
			// 取得各元件的最新版本號
			bs = component_service.componentVersion();
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("componentVersion error >> {}",e);
		}
		return bs;
	}
	
	/**
	 * 元件_下載
	 * 
	 * @return
	 */
	@RequestMapping(value = "/component_download", produces = { "text/plain;charset=UTF-8" })
	public void componentDownload(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model, SessionStatus status) {
		log.trace("componentDownload...");
		try {
			// 多瀏覽器元件下載
			String relativePath = request.getParameter("componentPath");
			//修改 HTTP Response Splitting
			if(relativePath.equals("win")) {
				relativePath = "component/windows/combo/Install_TbbComboNativeAgentHost.exe";
			}else if(relativePath.equals("mac")) {
				relativePath = "component/mac/combo/Setup-TbbComboMacNativeAgentHost.pkg";
			}else {
				relativePath = "";
			}
			log.debug("relativePath=" + relativePath);
			
			org.springframework.core.io.Resource resource = new ClassPathResource(relativePath);
//			File downloadFile = resource.getFile();
//			log.trace("resource len >> {}, file len >> {}", resource.contentLength(), downloadFile.length());
			if(resource.exists()){
				InputStream fileInputStream = resource.getInputStream();

				String mimeType = servletContext.getMimeType(relativePath);
				if (mimeType == null) {
					mimeType = "application/octet-stream";
				}
				log.debug("mimeType=" + mimeType);

				response.setContentType(mimeType);
				response.setContentLength((int) resource.contentLength());

				String headerKey = "Content-Disposition";
				String headerValue = String.format("attachment; filename=\"%s\"", resource.getFilename());
				response.setHeader(headerKey, headerValue);
				OutputStream outStream = response.getOutputStream();

				byte[] buffer = new byte[4096];
				int bytesRead = -1;

				while ((bytesRead = fileInputStream.read(buffer)) != -1) {
					outStream.write(buffer, 0, bytesRead);
				}
				fileInputStream.close();
				outStream.close();
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("componentDownload error >> {}",e);
		}
		
		// 下載完畢自動登出--20201201先不登出，因為IE前端會意外送出元件下載的請求
//		try {
//			login_out_controller.logout_aj(request, response, reqParam, model, status);
//			
//		} catch (Exception e) {
//			log.error("componentDownload logout error >> {}",e);
//		}
		
		return;
	}
	
	/**
	 * 元件_取得驗證是否本人
	 * 
	 * @return
	 */
	@RequestMapping(value = "/component_acct_aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult componentAcct(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("componentVersion...");
		BaseResult bs = null;
		try {
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			String acn = reqParam.get("ACN");
			// N953驗證卡片是否為本人
			bs = component_service.checkIdentity(cusidn, acn);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("componentAcct error >> {}",e);
		}
		return bs;
	}
	
	
	/**
	 * 元件_取得驗證是否為本人及卡片為啟用狀態
	 * 
	 * @return
	 */
	@RequestMapping(value = "/component_enable_aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult componentEnable(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("componentVersion...");
		BaseResult bs = null;
		try {
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			String acn = reqParam.get("ACN");
			// N953驗證卡片是否為本人
			bs = component_service.checkIdentityEnable(cusidn, acn);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("componentAcct error >> {}",e);
		}
		return bs;
	}
	
	/**
	 * 元件_取得驗證是否本人--未登入
	 * 
	 * @return
	 */
	@RequestMapping(value = "/component_checkid_aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult component_checkid_aj(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("component_checkid_aj...");
		BaseResult bs = null;
		try {
			String cusidn = new String(Base64.getDecoder().decode(reqParam.get("CUSIDN")), "utf-8");
			String acn = reqParam.get("ACN");
			// N953驗證卡片是否為本人
			bs = component_service.checkIdentity(cusidn, acn);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("component_checkid_aj error >> {}",e);
		}
		return bs;
	}
	
	/**
	 * 元件_取得驗證是否本人
	 * 
	 * @return
	 */
	@RequestMapping(value = "/component_without_id_aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult component_without_id_aj(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("componentVersion...");
		BaseResult bs = null;
		try {
			String cusidn = reqParam.get("UID");
			String acn = reqParam.get("ACN");
			// N953驗證卡片是否為本人
			bs = component_service.checkIdentity(cusidn, acn);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("component_without_id_aj error >> {}",e);
		}
		return bs;
	}
	
	/**
	 * 自然人憑證驗證的AJAX
	 */
	@RequestMapping(value="/vaCheckAjax",produces={"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult vaCheckAjax(@RequestParam Map<String,String> requestParam){
		log.debug("Component_Controller.vaCheckAjax...");
		
		BaseResult baseResult = new BaseResult();
		baseResult.setResult(Boolean.FALSE);
		
		try{
			String AUTHCODE = requestParam.get("AUTHCODE");
			log.debug(ESAPIUtil.vaildLog("AUTHCODE >> " + AUTHCODE));
			BaseResult vaBS = new BaseResult();
			vaBS = component_service.VACheck_REST(AUTHCODE);
			
			if(vaBS != null && vaBS.getResult()){
				baseResult.setResult(Boolean.TRUE);
				baseResult.setMsgCode("0");;
				baseResult.setMessage(i18n.getMsg("LB.X1759"));;
				
				Map<String,Object> vaBSData = (Map<String,Object>)vaBS.getData();
				String RESPONSE = (String)vaBSData.get("RESPONSE");
				log.debug("RESPONSE={}",RESPONSE);
				
				baseResult.setData(RESPONSE);
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("vaCheckAjax error >> {}",e);
		}
		return baseResult;
	}
	
	
	/**
	 * 元件_IKEY不登入
	 * 
	 * @return
	 */
	@RequestMapping(value = "/ikey_without_login_aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult component_ikey_without_login_aj(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		BaseResult baseResult = new BaseResult();
		baseResult.setResult(Boolean.FALSE);
		try{
			if(StrUtil.isNotEmpty(reqParam.get("bs64")) && "Y".equals(reqParam.get("bs64"))) {
				reqParam.put("UID", new String(Base64.getDecoder().decode(reqParam.get("UID")), "utf-8"));
			}
			baseResult = component_service.ikey_without_login(reqParam);
		}catch (Exception e) {
			log.error("ikey_without_login_aj error >> {}",e);
		}
		return baseResult;
	}
}
