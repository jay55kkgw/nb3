package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N191_REST_RSDATA2  extends BaseRestBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7395289349805538621L;
	
	private String PRICE; //單價(元)
    private String LSTLTD; //異動日期
    private String TRNBRH; //收付款行別
    private String TRNAMT; //交易金額(元)
    private String GDBAL; //結餘額
    private String TIME; //異動時間
    private String CODDBCR; //借貸別（D.支出，C.收入）
    private String TRNGD; //交易公克數
    private String ACN; //帳號
    private String DPWDAMT; //支出
    private String DPSVAMT; //存入
    private String MEMO; //摘要
    
	public String getPRICE() {
		return PRICE;
	}
	public void setPRICE(String pRICE) {
		PRICE = pRICE;
	}
	public String getDPSVAMT() {
		return DPSVAMT;
	}
	public void setDPSVAMT(String dPSVAMT) {
		DPSVAMT = dPSVAMT;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getCODDBCR() {
		return CODDBCR;
	}
	public void setCODDBCR(String cODDBCR) {
		CODDBCR = cODDBCR;
	}
	public String getTRNGD() {
		return TRNGD;
	}
	public void setTRNGD(String tRNGD) {
		TRNGD = tRNGD;
	}
	public String getMEMO() {
		return MEMO;
	}
	public void setMEMO(String mEMO) {
		MEMO = mEMO;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getGDBAL() {
		return GDBAL;
	}
	public void setGDBAL(String gDBAL) {
		GDBAL = gDBAL;
	}
	public String getTRNAMT() {
		return TRNAMT;
	}
	public void setTRNAMT(String tRNAMT) {
		TRNAMT = tRNAMT;
	}
	public String getTRNBRH() {
		return TRNBRH;
	}
	public void setTRNBRH(String tRNBRH) {
		TRNBRH = tRNBRH;
	}
	public String getLSTLTD() {
		return LSTLTD;
	}
	public void setLSTLTD(String lSTLTD) {
		LSTLTD = lSTLTD;
	}
	public String getDPWDAMT() {
		return DPWDAMT;
	}
	public void setDPWDAMT(String dPWDAMT) {
		DPWDAMT = dPWDAMT;
	}
	
    
}
