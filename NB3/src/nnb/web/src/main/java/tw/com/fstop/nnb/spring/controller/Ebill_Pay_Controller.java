package tw.com.fstop.nnb.spring.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.common.ResultCode;
import tw.com.fstop.nnb.service.DaoService;
import tw.com.fstop.nnb.service.Ebill_Pay_Service;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.WebUtil;

@Controller
@RequestMapping(value = "/EBILL/PAY")
@SessionAttributes({  SessionUtil.TRANSFER_DATA,
		SessionUtil.TRANSFER_CONFIRM_TOKEN, SessionUtil.TRANSFER_RESULT_TOKEN, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN,
		SessionUtil.BACK_DATA, SessionUtil.PRINT_DATALISTMAP_DATA, SessionUtil.CONFIRM_LOCALE_DATA,
		SessionUtil.RESULT_LOCALE_DATA })
public class Ebill_Pay_Controller
{
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private Ebill_Pay_Service ebill_Pay_Service;
	
	@Autowired
	private DaoService daoservice;


	/**
	 * 取得銀行代號名稱
	 * 
	 * @param request
	 * @param response
	 * @param reqParama
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/getDpBHNO_aj", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody BaseResult getDpBHNO(@RequestParam Map<String, String> reqParam, Model model)
	{
		String str = "getInAcno_aj";
		log.trace("hi {}", str);
		log.trace(ESAPIUtil.vaildLog("reqParam>>{}" +  CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		try
		{
			bs = daoservice.getDpBHNO_List("noLoing");
		}
		catch (Exception e)
		{
			log.debug("");
		}
		return bs;
	}

	/**
	 * 繳款單掃QRCODE- 全國性繳費
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/cclifepayment")
	public String cclifepayment(@RequestParam Map<String, String> reqParam, Model model)
	{
		String target = "/online_apply/ErrorWithoutMenu";
		log.trace("cclifepayment !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		BaseResult bs = new BaseResult();
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try
		{
			bs = ebill_Pay_Service.cclifepayment(okMap);
		}
		catch (Exception e)
		{
			log.error("cclifepayment error{}", e);
			bs.setSYSMessage(ResultCode.SYS_ERROR);
		}
		finally
		{
			if (bs.getResult())
			{
				target = "ebill/cclifepayment";
				model.addAttribute("cclifepayment", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	/**
	 * 繳款單掃QRCODE- 全國性繳費 輸入
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/cclifepayment_step1")
	public String cclifepayment_step1(@RequestParam Map<String, String> reqParam, Model model)
	{
		String target = "/online_apply/ErrorWithoutMenu";
		log.trace("cclifepayment_step1 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		BaseResult bs = new BaseResult();
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try
		{
			bs = ebill_Pay_Service.cclifepayment_step1(okMap);

			
		}
		catch (Exception e)
		{
			log.error("cclifepayment error{}", e);
			bs.setSYSMessage(ResultCode.SYS_ERROR);
		}
		finally
		{
			if (bs.getResult())
			{
				target = "ebill/cclifepayment_step1";
				model.addAttribute("cclifepayment_step1", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}

		return target;
	}

	/**
	 * 繳款單掃QRCODE- 全國性繳費 -確認
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "cclifepayment_confirm")
	public String cclifepayment_confirm(@RequestParam Map<String, String> reqParam, Model model)
	{
		String target = "/online_apply/ErrorWithoutMenu";
		log.trace("cclifepayment_confirm !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		BaseResult bs = new BaseResult();

		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try
		{
			bs = ebill_Pay_Service.getTxToken();
			log.trace("cclifepayment_confirm.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			if(bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			}
			String TXTOKEN = ((Map<String,String>) bs.getData()).get("TXTOKEN");
			bs.reset();
			bs = ebill_Pay_Service.cclifepayment_confirm(okMap);
			bs.addData("TXTOKEN", TXTOKEN);
		}
		catch (Exception e)
		{
			log.error("cclifepayment error{}", e);
			bs.setSYSMessage(ResultCode.SYS_ERROR);
		}
		finally
		{
			if (bs.getResult())
			{
				target = "ebill/cclifepayment_confirm";
				model.addAttribute("cclifepayment_confirm", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}

		return target;
	}

	/**
	 * 繳款單掃QRCODE- 全國性繳費 --結果
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "cclifepayment_result")
	public String cclifepayment_result(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model)
	{
		String target = "/online_apply/ErrorWithoutMenu";
		log.trace("cclifepayment_result!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ");
		BaseResult bs = new BaseResult();

		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try
		{
			log.trace("cclifepayment_result.validate TXTOKEN...");
			log.trace("cclifepayment_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
			log.trace("cclifepayment_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
			
			// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
			if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
					okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
				// TXTOKEN第一階段驗證成功
				bs.setResult(Boolean.TRUE);
				log.trace("cclifepayment_result.bs.step1 is successful...");
			}
			if(!bs.getResult()) {
				log.error(ESAPIUtil.vaildLog("cclifepayment_result TXTOKEN 不正確，TXTOKEN>>{}" + okMap.get("TXTOKEN")));
				throw new Exception();
			}
			// 重置bs，繼續往下驗證
			bs.reset();
			
			// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
			if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
					!okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
				// TXTOKEN第二階段驗證成功
				bs.setResult(Boolean.TRUE);
				log.trace("cclifepayment_result.bs.step2 is successful...");
			}
			if(!bs.getResult()) {
				log.error(ESAPIUtil.vaildLog("cclifepayment_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TXTOKEN")));
				throw new Exception();
			}
			// 重置bs，準備進行交易
			bs.reset();
			// 交易成功要存之前的token 在Session 以利下次比對是否重複交易
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TXTOKEN"));
			
			//使用者的IP
			String IP = WebUtil.getIpAddr(request);
			okMap.put("IP", IP);
			bs = ebill_Pay_Service.cclifepayment_result(okMap);
		}
		catch (Exception e)
		{
			log.error("cclifepayment error{}", e);
			bs.setSYSMessage(ResultCode.SYS_ERROR);
		}
		finally
		{
			if (bs.getResult())
			{
				target = "ebill/cclifepayment_result";
				model.addAttribute("cclifepayment_result", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}

		return target;
	}

	
	@RequestMapping(value = "/ccpay")
	public String ccpay(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		return "/ebill_pay/ccpay";
	}
	
	@RequestMapping(value = "/taiwaterpayment")
	public String taiwaterpayment(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		String target = "/ebill_pay/taiwaterpayment";
		return target;
	}
	
	@RequestMapping(value = "/taiwaterpayment_comfirm")
	public String taiwaterpayment_comfirm(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		String target = "/digital_account/ErrorWithoutMenu";
		BaseResult bs = null;
		Map<String, String> okMap = null;
		try {
			bs = new BaseResult();
			// 解決Trust Boundary Violation
			okMap = ESAPIUtil.validStrMap(reqParam);
			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = ebill_Pay_Service.getTxToken();
			log.trace("taiwaterpayment_comfirm.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			if(bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			}
			String TXTOKEN = ((Map<String,String>) bs.getData()).get("TXTOKEN");
			bs = ebill_Pay_Service.taiwaterpayment_comfirm(okMap);
			bs.addData("IP", request.getRemoteAddr());
			bs.addData("TXTOKEN", TXTOKEN);
		}catch(Exception e) {
			log.error(e.toString());
		}finally {
			if(bs!=null && bs.getResult()) {
				model.addAttribute("input_data", okMap);
				model.addAttribute("result_data", bs);
				target = "/ebill_pay/taiwaterpayment_comfirm";
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
				bs.setNext("/EBILL/PAY/ccpay");
			}
			
		}
		return target;
	}
	
	@RequestMapping(value = "/taiwaterpayment_result")
	public String taiwaterpayment_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		String target = "/digital_account/ErrorWithoutMenu";
		BaseResult bs = null;
		Map<String, String> okMap = null;
		try {
			bs = new BaseResult();
			// 解決Trust Boundary Violation
			okMap = ESAPIUtil.validStrMap(reqParam);

			log.trace("taiwaterpayment_result.validate TXTOKEN...");
			log.trace("taiwaterpayment_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
			log.trace("taiwaterpayment_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
			
			// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
			if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
					okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
				// TXTOKEN第一階段驗證成功
				bs.setResult(Boolean.TRUE);
				log.trace("taiwaterpayment_result.bs.step1 is successful...");
			}
			if(!bs.getResult()) {
				log.error(ESAPIUtil.vaildLog("taiwaterpayment_result TXTOKEN 不正確，TXTOKEN>>{}" + okMap.get("TXTOKEN")));
				throw new Exception();
			}
			// 重置bs，繼續往下驗證
			bs.reset();
			
			// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
			if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
					!okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
				// TXTOKEN第二階段驗證成功
				bs.setResult(Boolean.TRUE);
				log.trace("taiwaterpayment_result.bs.step2 is successful...");
			}
			if(!bs.getResult()) {
				log.error(ESAPIUtil.vaildLog("taiwaterpayment_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TXTOKEN")));
				throw new Exception();
			}
			// 重置bs，準備進行交易
			bs.reset();
			// 交易成功要存之前的token 在Session 以利下次比對是否重複交易
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TXTOKEN"));
			
			bs = ebill_Pay_Service.taiwaterpayment_result(okMap);
		}catch(Exception e) {
			log.error(e.toString());
		}finally {
			if(bs!=null && bs.getResult()) {
				model.addAttribute("result_data", bs);
				model.addAttribute("input_data", okMap);
				target = "/ebill_pay/taiwaterpayment_result";
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
				bs.setNext("/EBILL/PAY/ccpay");
			}
			
		}
		return target;
	}
}
