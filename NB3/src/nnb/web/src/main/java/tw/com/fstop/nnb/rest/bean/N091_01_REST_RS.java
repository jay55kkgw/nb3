package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N091_01_REST_RS extends BaseRestBean implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8724524347206873879L;
	
	private String CMQTIME;
	private String TRNDATE;
	private String TRNTIME;
	private String TRNSRC;
	private String TRNTYP;
	private String TRNCOD;
	private String TRNBDT;
	private String ACN;
	private String CUSIDN;
	private String SVACN;
	private String APPTYPE; 
	private String TRNGD;
	private String APPDATE;
	private String APPTIME;
	
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getTRNDATE() {
		return TRNDATE;
	}
	public void setTRNDATE(String tRNDATE) {
		TRNDATE = tRNDATE;
	}
	public String getTRNTIME() {
		return TRNTIME;
	}
	public void setTRNTIME(String tRNTIME) {
		TRNTIME = tRNTIME;
	}
	public String getTRNSRC() {
		return TRNSRC;
	}
	public void setTRNSRC(String tRNSRC) {
		TRNSRC = tRNSRC;
	}
	public String getTRNTYP() {
		return TRNTYP;
	}
	public void setTRNTYP(String tRNTYP) {
		TRNTYP = tRNTYP;
	}
	public String getTRNCOD() {
		return TRNCOD;
	}
	public void setTRNCOD(String tRNCOD) {
		TRNCOD = tRNCOD;
	}
	public String getTRNBDT() {
		return TRNBDT;
	}
	public String getACN() {
		return ACN;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public String getSVACN() {
		return SVACN;
	}
	public String getAPPTYPE() {
		return APPTYPE;
	}
	public String getTRNGD() {
		return TRNGD;
	}
	public String getAPPDATE() {
		return APPDATE;
	}
	public String getAPPTIME() {
		return APPTIME;
	}
	public void setTRNBDT(String tRNBDT) {
		TRNBDT = tRNBDT;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public void setSVACN(String sVACN) {
		SVACN = sVACN;
	}
	public void setAPPTYPE(String aPPTYPE) {
		APPTYPE = aPPTYPE;
	}
	public void setTRNGD(String tRNGD) {
		TRNGD = tRNGD;
	}
	public void setAPPDATE(String aPPDATE) {
		APPDATE = aPPDATE;
	}
	public void setAPPTIME(String aPPTIME) {
		APPTIME = aPPTIME;
	}
}
