package tw.com.fstop.nnb.service;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import fstop.orm.po.TXNTWRECORD;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.tbb.nnb.dao.TxnTwRecordDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.web.util.StrUtils;

@Service
public class TxnTwRecord_Service {
	private Logger log = LoggerFactory.getLogger(this.getClass().getName());
	
	@Autowired
	TxnTwRecordDao txnTwRecordDao;
	
	/*
	 * 以顧客統編(cusidn)、電文、一段時間當作條件去查詢DB，回傳結果
	 */
	public BaseResult query(Map<String,String> reqParam) {
		BaseResult bs = new BaseResult();
		List<TXNTWRECORD> list = null;
		//簡易時間格式檢查
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		sdf.setLenient(false);
		try {
			
			DateTime dt;
			Date d;
			//檢查統編、電文是否為空值
			if(reqParam.get("cusidn").trim() == null || reqParam.get("cusidn").trim().length() == 0) {
				bs.setMessage("1", "統編未輸入");
				bs.setResult(Boolean.FALSE);
				return bs;
			}
			
			int dtCon = 0;//判斷時間空值的狀況 0:全部有值;1:無起始日期;10:無結束日期;11:完全無日期;自動帶入前後六個月
			
			if(reqParam.get("sdate")== null || reqParam.get("sdate").trim().length() == 0) {
				dtCon += 1;
			}
			

			if(reqParam.get("edate")== null || reqParam.get("edate").trim().length() == 0) {
				dtCon += 10;
			}
			
			switch(dtCon) {
				case 1:
					dt = new DateTime(sdf.parse(reqParam.get("edate").replace("/", "").trim()));
					d = new Date(dt.minusMonths(6).getMillis());
					reqParam.put("sdate",sdf.format(d));
//					log.info("sdate="+reqParam.get("sdate"));
					break;
				case 10:
					dt = new DateTime(sdf.parse(reqParam.get("sdate").replace("/", "").trim()));
					d = new Date(dt.plusMonths(6).getMillis());
					reqParam.put("edate",sdf.format(d));
//					log.info("edate="+reqParam.get("edate"));
					break;
				case 11:
					dt = new DateTime();
					Date sd = new Date(dt.minusMonths(6).getMillis());
					Date ed = new Date(dt.getMillis());
					reqParam.put("sdate",sdf.format(sd));
					reqParam.put("edate",sdf.format(ed));
//					log.info("sdate="+reqParam.get("sdate"));
//					log.info("edate="+reqParam.get("edate"));
					break;
				default:
					d = sdf.parse(reqParam.get("sdate").trim());
					d = sdf.parse(reqParam.get("edate").trim());
					break;
			}
			//檢查時間是否正確
			if(sdf.parse(reqParam.get("sdate")).getTime()>sdf.parse(reqParam.get("edate")).getTime()) {
				bs.setMessage("1", "日期錯誤，起訖日大於終止日");
				bs.setResult(Boolean.FALSE);
				return bs;
			}
			
			list = txnTwRecordDao.findByDateRange(reqParam);
			log.info("list.size= >> {}", list.size());
			bs.setData(list);
			bs.setResult(Boolean.TRUE);
			bs.setMessage("0", "查詢成功");
			
		}catch(Exception e) {
			log.error("{}", e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", "查詢異常");
		}
		
		return bs;
	}
	/*
	 * 新增台幣轉帳結果進去DB
	 */
	public BaseResult add(Map<String,String> reqParam) {
		if(StrUtils.isNull(reqParam.get("logintype"))) {
			reqParam.put("logintype", "MB");
		}
		
		BaseResult bs = new BaseResult();
		TXNTWRECORD po = null;
		List<String> exlist = new ArrayList<String>();
		exlist.add("DPTXMEMO");
		exlist.add("DPTXMAILS");
		exlist.add("DPTXMAILMEMO");
		exlist.add("DPSCHID");
		exlist.add("DPEFEE");
		exlist.add("PCSEQ");
		exlist.add("DPTXNO");
		exlist.add("DPTOTAINFO");
		exlist.add("DPREMAIL");
		exlist.add("DPEXCODE");
		exlist.add("DPSVBH");
		try {
			//取得PO的所有變數名稱，序列化代碼不用檢查
			Field[] fs = TXNTWRECORD.class.getDeclaredFields();
 			List<String> listName = new ArrayList<String>();
 			for(Field f : fs) {
 				if(!("serialVersionUID".equals(f.getName()))) {
 					listName.add(f.getName());
 				}
 			}
 			List<String> list = new ArrayList<String>();
			for(String str:listName) {
				if(reqParam.get(str.toLowerCase()) == null) {
					bs.setResult(Boolean.FALSE);
					bs.setMessage("1", str +"為空值");
					return bs;
				}
				if(reqParam.get(str.toLowerCase()).trim().length() == 0 && !exlist.contains(str)) {
					bs.setResult(Boolean.FALSE);
					bs.setMessage("1", str +"為空值");
					return bs;
				}
				if(reqParam.get(str.toLowerCase()).trim().length() == 0 && exlist.contains(str)) {
					
					reqParam.put(str.toLowerCase(), "");
				}
				if(reqParam.get(str.toLowerCase()).trim().length() == 0 && str.equals("DPSCHID")) {
					reqParam.put(str.toLowerCase(), "0");
				}
				list.add(str.toLowerCase());
			}
			//檢查錯誤代碼是否為空
			if(reqParam.get("dpexcode")==null||reqParam.get("dpexcode").length() == 0) {
				reqParam.put("dpexcode","");
			}
			//將req的key轉成大寫
			for(String str:list) {
				reqParam.put(str.toUpperCase(),reqParam.get(str));
				reqParam.remove(str);
			}
			
			po = CodeUtil.objectCovert(TXNTWRECORD.class, reqParam);
			txnTwRecordDao.save(po);
			bs.setResult(Boolean.TRUE);
			bs.setMessage("0", "新增成功");
		}catch(Exception e) {
			log.error("{}", e);
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", "新增發生意料外的異常");
//			bs.setMessage("1", e +"為空值");
		}
		
		return bs;
	}
	
}
