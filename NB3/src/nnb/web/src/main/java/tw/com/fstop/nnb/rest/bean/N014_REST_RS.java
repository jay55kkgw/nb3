package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.Map;

public class N014_REST_RS extends BaseRestBean implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 775129110323679813L;
	
	private String CMQTIME ;
	private String CMRECNUM ;
	private String MSGFLG_014;
	private String TOPMSG_014;
	private String TOPMSG_553;
	private String ADMSGOUT_014;
	private String ADMSGOUT_553;
	
	private LinkedList<N014_REST_RSDATA> TW;
	private LinkedList<N014_REST_RSDATA2> FX;
	
	
	public String getMSGFLG_014() {
		return MSGFLG_014;
	}
	public void setMSGFLG_014(String mSGFLG_014) {
		MSGFLG_014 = mSGFLG_014;
	}
	public String getTOPMSG_014() {
		return TOPMSG_014;
	}
	public void setTOPMSG_014(String tOPMSG_014) {
		TOPMSG_014 = tOPMSG_014;
	}
	public String getTOPMSG_553() {
		return TOPMSG_553;
	}
	public void setTOPMSG_553(String tOPMSG_553) {
		TOPMSG_553 = tOPMSG_553;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getCMRECNUM() {
		return CMRECNUM;
	}
	public void setCMRECNUM(String cMRECNUM) {
		CMRECNUM = cMRECNUM;
	}
	
	public LinkedList<N014_REST_RSDATA> getTW() {
		return TW;
	}
	public void setTW(LinkedList<N014_REST_RSDATA> tW) {
		TW = tW;
	}
	public LinkedList<N014_REST_RSDATA2> getFX() {
		return FX;
	}
	public void setFX(LinkedList<N014_REST_RSDATA2> fX) {
		FX = fX;
	}
	public String getADMSGOUT_014() {
		return ADMSGOUT_014;
	}
	public void setADMSGOUT_014(String aDMSGOUT_014) {
		ADMSGOUT_014 = aDMSGOUT_014;
	}
	public String getADMSGOUT_553() {
		return ADMSGOUT_553;
	}
	public void setADMSGOUT_553(String aDMSGOUT_553) {
		ADMSGOUT_553 = aDMSGOUT_553;
	}
	
	
}
