package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N625_REST_RQ extends BaseRestBean_TW implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6809941354084071405L;
	
	String	ACN;
	String	CUSIDN;
	String	FGPERIOD;	//日期選擇
	String	CMSDATE;	//起日
	String	CMEDATE;	//迄日
	String	USERDATA_X50;//繼續查詢
	
	
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}	
	public String getFGPERIOD() {
		return FGPERIOD;
	}
	public void setFGPERIOD(String fGPERIOD) {
		FGPERIOD = fGPERIOD;
	}
	public String getCMSDATE() {
		return CMSDATE;
	}
	public void setCMSDATE(String cMSDATE) {
		CMSDATE = cMSDATE;
	}
	public String getCMEDATE() {
		return CMEDATE;
	}
	public void setCMEDATE(String cMEDATE) {
		CMEDATE = cMEDATE;
	}
	public String getUSERDATA_X50() {
		return USERDATA_X50;
	}
	public void setUSERDATA_X50(String uSERDATA_X50) {
		USERDATA_X50 = uSERDATA_X50;
	}


	
	
	
	

}
