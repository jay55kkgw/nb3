package tw.com.fstop.nnb.spring.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.service.Mobile_Service;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;

@Controller
@RequestMapping(value = "/MB3")
public class Mobile_Controller {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private Mobile_Service mobile_service;
	
	
	/**
	 * 確認使用者MB3連線狀態後踢掉使用者MB3連線
	 */
	@RequestMapping(value = "/kickmobile", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult kickmb3(HttpServletRequest request, HttpServletResponse response, 
				@RequestParam Map<String, String> reqParam, Model model) {
		log.info(ESAPIUtil.vaildLog("reqParam>>{}"+CodeUtil.toJson(reqParam)));
		BaseResult bs = null;
		try {
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			
			String cusidn = okMap.get("cusidn").toUpperCase();
			
			bs = new BaseResult();
			boolean result = mobile_service.kickMB3DB(cusidn);
			
			if (result) {
				log.debug("kick MB3 success");
				bs.setMsgCode("0");
				bs.setMessage("kick MB3 success");
				bs.setResult(true);
				
			} else {
				log.info("kick MB3 fail");
				bs.setMsgCode("1");
				bs.setMessage("kick MB3 fail");
				bs.setResult(false);
			}
			
		} catch (Exception e) {
			log.error("",e);
		}
		return bs;
	}

}
