package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N953_REST_RQ extends BaseRestBean_TW implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1065505469195494796L;
	
	private String DATE; // 日期YYYMMDD
	private String TIME; // 時間HHMMSS
	private String ACN;  // 晶片卡主帳號
	
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	
}
