package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N930EA_REST_RS extends BaseRestBean implements Serializable   {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8180250000299126766L;

	private String DPUSERID;
	private String DPADDBKID;
	private String DPGONAME;
	private String DPABMAIL;

	LinkedList<N930EA_REST_RSDATA> REC;
	
	
	public String getDPUSERID() {
		return DPUSERID;
	}
	public void setDPUSERID(String dPUSERID) {
		DPUSERID = dPUSERID;
	}
	public String getDPADDBKID() {
		return DPADDBKID;
	}
	public void setDPADDBKID(String dPADDBKID) {
		DPADDBKID = dPADDBKID;
	}
	public String getDPGONAME() {
		return DPGONAME;
	}
	public void setDPGONAME(String dPGONAME) {
		DPGONAME = dPGONAME;
	}
	public String getDPABMAIL() {
		return DPABMAIL;
	}
	public void setDPABMAIL(String dPABMAIL) {
		DPABMAIL = dPABMAIL;
	}
	public LinkedList<N930EA_REST_RSDATA> getREC() {
		return REC;
	}
	public void setREC(LinkedList<N930EA_REST_RSDATA> rEC) {
		REC = rEC;
	}
}
