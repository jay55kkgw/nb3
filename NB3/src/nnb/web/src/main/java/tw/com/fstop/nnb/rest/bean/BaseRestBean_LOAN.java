package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.ConfingManager;

public class BaseRestBean_LOAN extends BaseRestBean implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 9052248421135682166L;

	
//	AOP 攔截用屬性 台幣預設用 TWD
	public String ADCURRENCY = "TWD";
	private String ADTXACNO;		// 轉出帳號 
	private String ADSVBH;			// 轉入服務銀行 
	private String ADREQTYPE;		// 預約>>台幣:S，外幣:B ，其他就給空字串 
	private String ADTXAMT;			// 轉出金額
	private String ADAGREEF;		// 轉入帳號約定或非約定註記>>0:非約定，1:約定
//	private String FGTXWAY;			// 交易機制 ex:0=SSL ，1=IKEY 
	private static String ms_Channel = ConfingManager.MS_LOAN;


	public static String getMs_Channel()
	{
		return ms_Channel;
	}

	public static void setMs_Channel(String ms_Channel)
	{
		BaseRestBean_LOAN.ms_Channel = ms_Channel;
	}

	public static String getAPI_Name(Class<?> retClass) {
		  if(ms_Channel.endsWith("/") == false) {
		    	ms_Channel = ms_Channel + "/";
		    }
		return ms_Channel + retClass.getSimpleName().replace("_REST_RQ", "");
	}
	
	public static String getAPI_Name(Class<?> retClass, String _ms_Channel) {

		if (StrUtil.isNotEmpty(_ms_Channel)) {
			if (_ms_Channel.endsWith("/") == false) {
				_ms_Channel = _ms_Channel + "/";
			}
			return _ms_Channel + retClass.getSimpleName().replace("_REST_RQ", "");
		}
		return getAPI_Name(retClass);
	}

	public String getADCURRENCY() {
		return ADCURRENCY;
	}

	public void setADCURRENCY(String aDCURRENCY) {
		ADCURRENCY = aDCURRENCY;
	}

	public String getADTXACNO() {
		return ADTXACNO;
	}

	public void setADTXACNO(String aDTXACNO) {
		ADTXACNO = aDTXACNO;
	}

	public String getADSVBH() {
		return ADSVBH;
	}

	public void setADSVBH(String aDSVBH) {
		ADSVBH = aDSVBH;
	}

	public String getADREQTYPE() {
		return ADREQTYPE;
	}

	public void setADREQTYPE(String aDREQTYPE) {
		ADREQTYPE = aDREQTYPE;
	}

	public String getADTXAMT() {
		return ADTXAMT;
	}

	public void setADTXAMT(String aDTXAMT) {
		ADTXAMT = aDTXAMT;
	}

	public String getADAGREEF() {
		return ADAGREEF;
	}

	public void setADAGREEF(String aDAGREEF) {
		ADAGREEF = aDAGREEF;
	}
	
	
}
