package tw.com.fstop.nnb.service;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hpsf.Array;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import fstop.orm.po.ADMLOGIN;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.AdmLoginDao;
import tw.com.fstop.tbb.nnb.dao.AdmMsgCodeDao;
import tw.com.fstop.tbb.nnb.dao.TxnSmsLogDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.EncUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.StrUtil;

@Service
public class Phone_Config_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private Personal_Serving_Service personal_serving_service;

	@Autowired
	AdmMsgCodeDao admMsgCodeDao;

	@Autowired
	AdmLoginDao admLoginDao;

	@Autowired
	I18n i18n;

	@Autowired
	private TxnSmsLogDao txnSmsLogDao;

	private final String checknum = "5442424f70656e4150494d6f62696c655472616e73666572";

	public ADMLOGIN getAdmLogin(String cusidn) {
		ADMLOGIN po = null;
		try {
			log.trace("Login_out_Service.setAdmLogin...");

			// 若資料庫有資料則取資料庫的資料
			List<ADMLOGIN> admloginList = admLoginDao.findByUserId(cusidn);
			if (admloginList != null && !admloginList.isEmpty()) {
				po = admLoginDao.findByUserId(cusidn).get(0);
			} else {
				po = new ADMLOGIN();
			}

		} catch (Exception e) {
			log.error("{}", e);
		}
		return po;
	}

	/**
	 *	判斷是否有在本行(050)綁定手機門號 
	 * @param cusidn 身分證字號
	 * 
	 * */
	public BaseResult checkbind(String cusidn) {
		BaseResult bs = null;
		Map<String, String> reqParam = new HashMap<String, String>();
		try {
			bs = new BaseResult();
			ADMLOGIN po = this.getAdmLogin(cusidn);
			String mobilephone = po.getMOBTEL();
			//從電文存DB讀出來的，一般都有
			if(StrUtil.isEmpty(mobilephone)) 
				log.error("DB查無此統編對應的手機");
			
			reqParam.put("mobilephone", mobilephone);
			reqParam.put("userid", cusidn);
			bs = MtpBindQry_REST(reqParam);
			
		} catch (Exception e) {
			log.error("phone_config_setting_confirm error >> {}", e);
		}
		return bs;
	}
	
	/**
	 * 手機轉帳綁定 確認頁打MS_QR
	 * 
	 * @param adopid
	 * @param cusidn
	 * @return
	 */
	public BaseResult setting(String cusidn) {
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			ADMLOGIN po = this.getAdmLogin(cusidn);
			String mobilephone = po.getMOBTEL();
			if("".equals(mobilephone)) 
				log.error("Session CUSIDN lost...");
			bs.addData("mobilephone",mobilephone);
			//OTP驗證用
			bs.addData("cusidn",cusidn);
			bs.setResult(true);
		} catch (Exception e) {
			log.error("phone_config_setting_confirm error >> {}", e);
		}
		return bs;
	}
	
	/**
	 * 手機轉帳綁定 確認頁打MS_QR
	 * 
	 * @param adopid
	 * @param cusidn
	 * @return
	 */
	public BaseResult setting_confirm(Map<String, String> reqParam, String cusidn) {
		BaseResult bs = null;

		try {
			log.trace(ESAPIUtil.vaildLog("setting_confirm.reqParam: {}" + CodeUtil.toJson(reqParam)));
			bs = new BaseResult();

			reqParam.put("ADOPID","MtpUser"); 
			reqParam.put("FuncType","1"); // 0:發送 1:驗證
			reqParam.put("CUSIDN",cusidn); 
			reqParam.put("UID",cusidn); 
			
			//驗證OTP
			bs = SmsOtp_REST(reqParam);
			
			if(bs.getResult()) 
				log.info("OTP Pass...");
			else {
				log.error("OTP Fail...");
				return bs;
			}
			
//			Gson gson = new Gson();
//			Map<String, String> DPAGACNO = gson.fromJson(reqParam.get("DPAGACNO"), Map.class);
//			String txacn = DPAGACNO.get("ACN");
			reqParam.put("cusidn", cusidn);
			reqParam.put("txacn", reqParam.get("DPAGACNO"));
			reqParam.put("systemflag", "NB3");
			// 放入TXTOKEN
			bs = getTxToken();
			if (bs.getResult()) {
				reqParam.put("TXTOKEN", ((Map<String, String>) bs.getData()).get("TXTOKEN"));
				bs.setData(reqParam);
			}
		} catch (Exception e) {
			log.error("phone_config_setting_confirm error >> {}", e);
		}
		return bs;
	}

	/**
	 * 手機轉帳綁定 結果頁打MS_QR
	 * 
	 * @param adopid
	 * @param cusidn
	 * @return
	 */
	public BaseResult phone_config_result(Map<String, Object> reqParam) {
		BaseResult bs = new BaseResult();
		try {
			
			// 打ms_qr
			bs = MtpUser_REST(reqParam);
			
			//TODO ms_qr上版本後移除
//			Map<String,Object> result =(Map<String,Object>) bs.getData();
//			if("4001".equals(result.get("RCode")) || "O001".equals(result.get("RCode")))
//				bs.setResult(Boolean.TRUE);
			
			if (bs.getResult()) { 
				reqParam.put("TXTOKEN", ((Map<String, String>) bs.getData()).get("TXTOKEN"));
				bs.addAllData(reqParam);
			}
		} catch (Exception e) {
			log.error("phone_config_result.error: {}", e);
		}
		return bs;
	}

	/**
	 * 手機轉帳綁定 查詢頁打MS_QR
	 * 
	 * @param adopid
	 * @param cusidn
	 * @return
	 */
	public BaseResult update_select(String cusidn, Map<String, String> reqParam) {
		BaseResult bs = new BaseResult();
		BaseResult bs2 = new BaseResult();
		try {
			ADMLOGIN po = this.getAdmLogin(cusidn);
			String mobilephone = po.getMOBTEL();
			
			if(StrUtil.isEmpty(mobilephone)) 
				return bs;
			
			//優先查本行綁定
			reqParam.put("mobilephone", mobilephone);
			reqParam.put("userid", cusidn);
			bs = MtpBindQry_REST(reqParam);
			Map<String,String> result = (Map<String, String>) bs.getData();
			if(bs.getResult()) {
				//查詢是否為預設
				bs2 = MtpQuery_REST(reqParam);
				bs.addAllData(reqParam);
				bs.addData("oldtxacn", result.get("Txacn"));
				bs.addData("binddefault", "N");
				if (bs2.getResult()) 
					bs.addData("binddefault", "Y");
			}
			
		} catch (Exception e) {
			log.error("phone_config_result.error: {}", e);
		}
		return bs;
	}
	
	/**
	 * 手機轉帳修改 參數加工
	 * 
	 * @param adopid
	 * @param cusidn
	 * @return
	 */
	public BaseResult phone_update_change_confirm(Map<String, String> reqParam) {
		BaseResult bs = new BaseResult();
		try {
			
//			Gson gson = new Gson();
//			Map<String, String> DPAGACNO = gson.fromJson(reqParam.get("DPAGACNO"), Map.class);
//			String txacn = DPAGACNO.get("ACN");
			reqParam.put("txacn", reqParam.get("DPAGACNO"));
			
			// 放入TXTOKEN
			bs = getTxToken();
			if (bs.getResult()) {
				reqParam.put("TXTOKEN", ((Map<String, String>) bs.getData()).get("TXTOKEN"));
				bs.setData(reqParam);
			}	
		
		} catch (Exception e) {
			log.error("phone_config_result.error: {}", e);
		}
		return bs;
	}
	
	/**
	 * 手機轉帳修改 結果頁打MS_QR
	 * 
	 * @param adopid
	 * @param cusidn
	 * @return
	 */
	public BaseResult update_select_change_result(Map<String, Object> reqParam) {
		BaseResult bs = new BaseResult();
		try {
			String today = DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss");
			String mobilephone = reqParam.get("mobilephone").toString();
			String oldtxacn = reqParam.get("oldtxacn").toString();
			String txacn = reqParam.get("txacn").toString();
			String oldbind = reqParam.get("oldbind").toString();
			String binddefault = reqParam.get("binddefault").toString();
			
			reqParam.put("bankcode", "050");
			reqParam.put("mobilephone", mobilephone);
			reqParam.put("txacn1", oldtxacn);
			reqParam.put("txacn2", txacn);
			reqParam.put("datetime", today);
			//同帳號
			if(oldtxacn.equals(txacn)) {
				//同是否預設 直接回成功
				if(oldbind.equals(binddefault)) {
					bs.setResult(Boolean.TRUE);
					bs.setData(reqParam);
					return bs;
				//預設改非預設 用unbind範圍B讓中心去判斷
				}else if("Y".equals(oldbind)&& "N".equals(binddefault)) {
					reqParam.put("txacn", txacn);
					reqParam.put("txselect", "B");
					bs = MtpUnbind_REST(reqParam);
				//非預設改預設 改打綁定為預設
				}else if("N".equals(oldbind)&& "Y".equals(binddefault)) {
					reqParam.put("txacn", txacn);
					bs = MtpUser_REST(reqParam);
				}
			}else {
				// 打ms_qr
				bs = MtpChangeAccount_REST(reqParam);
			}
			

			//TODO ms_qr上版本後移除
//			Map<String,Object> result =(Map<String,Object>) bs.getData();
//			if("4001".equals(result.get("RCode")) || "O001".equals(result.get("RCode")))
//				bs.setResult(Boolean.TRUE);
			
			if (bs.getResult()) { 
				reqParam.put("TXTOKEN", ((Map<String, String>) bs.getData()).get("TXTOKEN"));
				bs.setData(reqParam);
			}
			
		} catch (Exception e) {
			log.error("phone_config_result.error: {}", e);
		}
		return bs;
	}

	/**
	 * 手機轉帳註銷 確認頁加工參數
	 * 
	 * @param adopid
	 * @param cusidn
	 * @return
	 */
	public BaseResult phone_update_cannel_confirm(Map<String, String> reqParam) {
		BaseResult bs = new BaseResult();
		try {
			reqParam.put("txacn", reqParam.get("oldtxacn"));
			reqParam.put("bankcode", "050");
			// 放入TXTOKEN
			bs = getTxToken();
			if (bs.getResult()) {
				reqParam.put("TXTOKEN", ((Map<String, String>) bs.getData()).get("TXTOKEN"));
				bs.setData(reqParam);
			}	
		
		} catch (Exception e) {
			log.error("phone_config_result.error: {}", e);
		}
		return bs;
	}
	
	/**
	 * 手機轉帳註銷 結果頁打MS_QR
	 * 
	 * @param adopid
	 * @param cusidn
	 * @return
	 */
	public BaseResult update_select_cannel_result(Map<String, Object> reqParam) {
		BaseResult bs = new BaseResult();
		try {
			String today = DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss");
			reqParam.put("datetime", today);
			reqParam.put("bankcode", "050");
			reqParam.put("txselect", "A");
			// 打ms_qr
			bs = MtpUnbind_REST(reqParam);
			
			//TODO ms_qr上版本後移除
//			Map<String,Object> result =(Map<String,Object>) bs.getData();
//			if("4001".equals(result.get("RCode")) || "O001".equals(result.get("RCode")))
//				bs.setResult(Boolean.TRUE);
			
			if (bs.getResult()) { 
				reqParam.put("TXTOKEN", ((Map<String, String>) bs.getData()).get("TXTOKEN"));
				bs.setData(reqParam);
			}
		} catch (Exception e) {
			log.error("phone_config_result.error: {}", e);
		}
		return bs;
	}
	
	public BaseResult getACCOUNTNAME(Map<String, String> reqParam) {
		BaseResult bs = new BaseResult();
		try {
			String dpbhno = reqParam.get("DPBHNO");
			reqParam.put("bankcode", dpbhno == null ? "" : dpbhno.split("-")[0]);
			reqParam.put("mobilephone", reqParam.get("DPACNO"));			
			bs = MtpPreTransferInfoQry_REST(reqParam);

			Map<String,Object> result =(Map<String,Object>) bs.getData();			
			if(bs.getResult()) {
				Map<String,String> AccountInfo = (Map<String, String>) result.get("AccountInfo");
				bs.addData("BankCode", AccountInfo.get("BankCode"));
				bs.addData("AccountName", AccountInfo.get("AccountName"));
				bs.addData("AccountType", AccountInfo.get("AccountType"));		
			}
		} catch (Exception e) {
			log.error("phone_config_result.error: {}", e);
		}
		return bs;
	}
	
	public BaseResult getBankList(Map<String, String> reqParam) {
		BaseResult bs = new BaseResult();
		List<String> banks = new ArrayList<String>();
		try {
			bs = MtpBankList_REST(reqParam);
			Map<String,Object> data = (Map<String, Object>) bs.getData();
			List<Map<String,String>> bankses = (List<Map<String, String>>) data.get("BankInfo");
			
			if(bs.getResult()) {
				String TotalCounts = (String) data.get("TotalCounts");
				if(TotalCounts != null && Float.valueOf(TotalCounts)>0) {
					for(Map map:bankses) {
						banks.add(map.get("Code")+"-"+map.get("Name"));
					}
				}
			}
			
//			String[] array = {"004-臺灣銀行",
//					"005-土地銀行","006-合庫商銀","007-第一銀行","008-華南銀行","009-彰化銀行","011-上海銀行",
//					"012-台北富邦","013-國泰世華","016-高雄銀行","017-兆豐商銀","018-農業金庫","021-花旗(台灣)銀行",
//					"022-美國銀行","025-首都銀行","039-澳盛(台灣)銀行","040-中華開發","048-王道商業銀行","050-臺灣企銀",
//					"052-渣打商銀","053-台中銀行","054-京城商銀","057-台東企銀","072-德意志銀行","075-東亞銀行",
//					"081-匯豐(台灣)銀行","101-瑞興銀行","102-華泰銀行","103-臺灣新光商銀","104-台北五信","106-台北九信",
//					"108-陽信銀行","114-基隆一信","115-基隆二信","118-板信銀行","119-淡水一信","120-淡水信合社",
//					"124-宜蘭信合社","127-桃園信合社","130-新竹一信","132-新竹三信","139-竹南信合社","146-台中二信",
//					"147-三信銀行","158-彰化一信","161-彰化五信","162-彰化六信","163-彰化十信","165-鹿港信合社","178-嘉義三信",
//					"179-嘉義四信","188-台南三信","204-高雄三信","215-花蓮一信","216-花蓮二信","222-澎湖一信","223-澎湖二信",
//					"224-金門信合社","512-雲林漁會","515-嘉義漁會","517-南市區漁會","518-南縣漁會","520-高雄市小港區漁會",
//					"521-高雄縣漁會","523-枋寮區漁會","524-新港漁會","525-澎湖區漁會","605-高雄市高雄地區農會","612-神岡鄉農會",
//					"613-名間農會","614-彰化縣農會","616-雲林縣農會","617-嘉義縣農會","618-台南縣農會","619-高雄縣農會",
//					"620-屏東縣農會","621-花蓮縣農會","622-台東縣農會","624-澎湖農會","625-台中市農會","627-連江縣農會",
//					"700-中華郵政","803-聯邦銀行","805-遠東銀行","806-元大銀行","807-永豐銀行","808-玉山銀行","809-凱基銀行",
//					"810-星展(台灣)銀行","812-台新銀行","814-大眾銀行","815-日盛銀行","816-安泰銀行","822-中國信託",
//					"901-大里市農會","903-汐止農會","904-新莊農會","910-桃農中心","912-冬山農會","916-草屯農會",
//					"922-臺南市臺南地區農會","928-板橋農會","951-北農中心","954-中農中心"
//			}; 
			
//			List<String> list = new ArrayList<String>();
//			list.addAll(Arrays.asList(array));
			Collections.sort(banks);
			bs.setData(banks);
		} catch (Exception e) {
			log.error("phone_config_result.error: {}", e);
		}
		return bs;
	} 
	
	public BaseResult check_phone(Map<String, String> reqParam) {
		BaseResult bs = new BaseResult();
		try {
			String phonenumber = reqParam.get("PNUMBER");
			String bankcode = reqParam.get("BKCODE");
			reqParam.put("mobilephone", phonenumber);			
			reqParam.put("bankcode", bankcode == null ? "" :  bankcode.split("-")[0]);
			
			//調整為只打infoqry，另外判斷如果RCode為4401代表查詢成功但是查無帳號
			bs = MtpPreTransferInfoQry_REST(reqParam);
			Map<String,Object> result = (Map<String, Object>) bs.getData();
			if("MP4401".equals(result.get("RCode")))  
				bs.setResult(false);				
		} catch (Exception e) {
			log.error("phone_config_result.error: {}", e);
		}
		return bs;
	}
	
}
