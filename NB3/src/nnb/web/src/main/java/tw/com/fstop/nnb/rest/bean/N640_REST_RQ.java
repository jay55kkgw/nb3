package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N640_REST_RQ extends BaseRestBean_TW implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2566053222559454496L;
	
	private String USERDATA_X100;//繼續查詢用暫存空間區
	private String ACN;//帳號
	private String CUSIDN;//統一編號
	private String CMSDATE;//開始日期
	private String CMEDATE;//結束日期
	private String LSTIME;//最後異動時間
	private String FILLER_X39;//FILLER_X39
	
	public String getCMSDATE() {
		return CMSDATE;
	}
	public void setCMSDATE(String cMSDATE) {
		CMSDATE = cMSDATE;
	}
	public String getCMEDATE() {
		return CMEDATE;
	}
	public void setCMEDATE(String cMEDATE) {
		CMEDATE = cMEDATE;
	}

	
	public String getUSERDATA_X100() {
		return USERDATA_X100;
	}
	public void setUSERDATA_X100(String uSERDATA_X100) {
		USERDATA_X100 = uSERDATA_X100;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getLSTIME() {
		return LSTIME;
	}
	public void setLSTIME(String lSTIME) {
		LSTIME = lSTIME;
	}
	
	public String getFILLER_X39() {
		return FILLER_X39;
	}
	public void setFILLER_X39(String fILLER_X39) {
		FILLER_X39 = fILLER_X39;
	}

	
}
