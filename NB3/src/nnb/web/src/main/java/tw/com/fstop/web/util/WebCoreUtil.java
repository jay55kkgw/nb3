
/*
 * Copyright (c) 2017, FSTOP, Inc. All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tw.com.fstop.web.util;

import java.text.SimpleDateFormat;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import tw.com.fstop.util.SpringBeanFactory;



/**
 * Web utility functions for web application.
 * 
 *
 * @since 1.0.1
 */
public class WebCoreUtil
{
	private static Logger log = LoggerFactory.getLogger(WebCoreUtil.class);
	private static ObjectMapper jsonMapper;
	
	static
	{
		jsonMapper = new ObjectMapper();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        jsonMapper.setDateFormat(df);		
	}
	  	
  	//帳號之科目(帳號第4~5碼)
  	public static String getACNgroup(String acn) {
  		String result = acn.substring(3, 5);
  		String group = null;
  		  		
  		Map<String,String> map =SpringBeanFactory.getBean("acnGroup");
  		for(String key : map.keySet()) {
  			String value = map.get(key);
  			//log.debug("value={}",value);
  			if(value.indexOf(result) != -1) {
  				group = key;
  				break;
  			} 	
  		}
  		
  		return group;
  	}
}