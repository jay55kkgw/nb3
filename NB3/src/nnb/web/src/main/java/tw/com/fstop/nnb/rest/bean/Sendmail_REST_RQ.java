package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.List;

public class Sendmail_REST_RQ extends BaseRestBean_PS implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -342224001134061643L;
	
	private String ADMAILCONTENT;
	private String ADMAILACNO;
	private String ADMSUBJECT;
	
	public String getADMAILCONTENT() {
		return ADMAILCONTENT;
	}
	public void setADMAILCONTENT(String aDMAILCONTENT) {
		ADMAILCONTENT = aDMAILCONTENT;
	}
	public String getADMAILACNO() {
		return ADMAILACNO;
	}
	public void setADMAILACNO(String aDMAILACNO) {
		ADMAILACNO = aDMAILACNO;
	}
	public String getADMSUBJECT() {
		return ADMSUBJECT;
	}
	public void setADMSUBJECT(String aDMSUBJECT) {
		ADMSUBJECT = aDMSUBJECT;
	}
}
