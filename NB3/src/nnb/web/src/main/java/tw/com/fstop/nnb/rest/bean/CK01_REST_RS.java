package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

public class CK01_REST_RS extends BaseRestBean implements Serializable {

	private static final long serialVersionUID = 9018187800581094918L;
	/**
	 * 
	 */
	private String CUSTID;
	private String CUSNAME ;
	private String HPHONE ;
	private String OPHONE ;
	private String MPFONE ;
	private String BLOCKFG ;
	private String CYUSDFG ;
	private String DELAYPAYFG ;
	private String CURRBAL ;
	private String CRLIMIT ;
	private String POT ;
	private String CYCLE ;
	private String CARDNUMS;
	private String RATE;
	
	
	public String getCUSTID() {
		return CUSTID;
	}
	public String getCUSNAME() {
		return CUSNAME;
	}
	public String getHPHONE() {
		return HPHONE;
	}
	public String getOPHONE() {
		return OPHONE;
	}
	public String getMPFONE() {
		return MPFONE;
	}
	public String getBLOCKFG() {
		return BLOCKFG;
	}
	public String getCYUSDFG() {
		return CYUSDFG;
	}
	public String getDELAYPAYFG() {
		return DELAYPAYFG;
	}
	public String getCURRBAL() {
		return CURRBAL;
	}
	public String getCRLIMIT() {
		return CRLIMIT;
	}
	public String getPOT() {
		return POT;
	}
	public String getCYCLE() {
		return CYCLE;
	}
	public String getCARDNUMS() {
		return CARDNUMS;
	}
	public String getRATE() {
		return RATE;
	}
	public String getRATE1() {
		return RATE1;
	}
	public void setCUSTID(String cUSTID) {
		CUSTID = cUSTID;
	}
	public void setCUSNAME(String cUSNAME) {
		CUSNAME = cUSNAME;
	}
	public void setHPHONE(String hPHONE) {
		HPHONE = hPHONE;
	}
	public void setOPHONE(String oPHONE) {
		OPHONE = oPHONE;
	}
	public void setMPFONE(String mPFONE) {
		MPFONE = mPFONE;
	}
	public void setBLOCKFG(String bLOCKFG) {
		BLOCKFG = bLOCKFG;
	}
	public void setCYUSDFG(String cYUSDFG) {
		CYUSDFG = cYUSDFG;
	}
	public void setDELAYPAYFG(String dELAYPAYFG) {
		DELAYPAYFG = dELAYPAYFG;
	}
	public void setCURRBAL(String cURRBAL) {
		CURRBAL = cURRBAL;
	}
	public void setCRLIMIT(String cRLIMIT) {
		CRLIMIT = cRLIMIT;
	}
	public void setPOT(String pOT) {
		POT = pOT;
	}
	public void setCYCLE(String cYCLE) {
		CYCLE = cYCLE;
	}
	public void setCARDNUMS(String cARDNUMS) {
		CARDNUMS = cARDNUMS;
	}
	public void setRATE(String rATE) {
		RATE = rATE;
	}
	public void setRATE1(String rATE1) {
		RATE1 = rATE1;
	}
	private String RATE1;
	
	
	
	
}

