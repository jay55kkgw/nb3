package tw.com.fstop.nnb.spring.controller;

import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import fstop.orm.po.ADMANN;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.service.Acct_Service;
import tw.com.fstop.nnb.service.Index_Service;
import tw.com.fstop.nnb.service.Verify_Mail_Service;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.util.StrUtils;
import tw.com.fstop.web.util.WebUtil;

@SessionAttributes({ SessionUtil.CUSIDN, SessionUtil.PD, SessionUtil.AUTHORITY, SessionUtil.USERDATA, 
						SessionUtil.DPMYEMAIL, SessionUtil.DPUSERNAME, SessionUtil.SUSERNAME, SessionUtil.TIMEOUT, SessionUtil.SENDMAIL,SessionUtil.USERID,
						SessionUtil.ISCERTIFICATIONREGISTERY, SessionUtil.REMAILID , SessionUtil.EMAILALERT})
@Controller
@RequestMapping(value="/INDEX")
public class Index_Controller {
	
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private Index_Service index_service;
	@Autowired
	private Acct_Service acct_service;
	
	@Autowired
	private Verify_Mail_Service verify_mail_service;
	
	@Autowired
	I18n i18n;

	@Autowired
	String certificate_url ;
//	String certificate_url = "https://172.22.11.41/raweb/logon.htm?authCode=";
	@Autowired
	String isCertificationRegistery ;
	
	// 進首頁--檢查使用者基本資料
	@RequestMapping(value = "/index")
	public String index(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.debug("enter_index...");
		String target = "/index/index";
		String cusidn = "";
		String authority = "";
		try {
			
			if(StrUtils.isEmpty(isCertificationRegistery)) {
				isCertificationRegistery = "Y";
			}
			SessionUtil.addAttribute(model, SessionUtil.ISCERTIFICATIONREGISTERY, isCertificationRegistery);
			
			// 一次登入只會顯示一次
			if(SessionUtil.getAttribute(model, SessionUtil.USERDATA, null) != null) {
				log.trace("index.skip.a106...");
				SessionUtil.addAttribute(model, SessionUtil.USERDATA, false);
				return target;
			}
			
			cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				cusidn = new String(Base64.getDecoder().decode(cusidn));
			}
			log.trace("index.cusidn: " + cusidn);
			
			authority = (String) SessionUtil.getAttribute(model, SessionUtil.AUTHORITY, null);
			log.trace("index.authority: " + authority);
			
			
			// A106使用者基本資料
			if(cusidn.length() == 10 && !authority.equals("CRD")){
				Map<String, String> a106Param = new HashMap<String, String>();
				a106Param.put("FUN_TYPE","R");//R:查詢
				BaseResult a106_bs = index_service.getA106(cusidn, a106Param);
				Map<String,Object> a106_dataMap = (Map<String,Object>) a106_bs.getData();
				log.debug("index.a106_dataMap: " + CodeUtil.toJson(a106_dataMap));
				
				String career1 = a106_dataMap.get("CAREER1").toString();
				String career2 = a106_dataMap.get("CAREER2").toString();
				String conpnam = a106_dataMap.get("CONPNAM").toString();
				
				// 沒基本資料導頁到填寫頁
				if( career1.equals("") || career2.equals("") || conpnam.equals("") ){
					log.trace("A106");
					// 一次登入只會顯示一次
					SessionUtil.addAttribute(model, SessionUtil.USERDATA, true);
					
					model.addAttribute("CUSIDN", cusidn); 	// 使用者統編
					model.addAttribute("CAREER1", career1); // 職業
					model.addAttribute("CAREER2", career2); // 職稱
					model.addAttribute("CONPNAM", conpnam); // 任職機構名稱
					
				} else {
					SessionUtil.addAttribute(model, SessionUtil.USERDATA, false);
				}
				
			} else {
				SessionUtil.addAttribute(model, SessionUtil.USERDATA, false);
			}
			
			
			// 第二、三階段提示訊息
			String isProd = SpringBeanFactory.getBean("isProd");
			log.info("index.isProd: {}", isProd);
			
			// 第二階段: 每天只需顯示一次，當天之後的登入不再出現
			if ("2".equals(isProd) || "3".equals(isProd)) {
				boolean firstaday = index_service.getAclNotedate(cusidn);
				model.addAttribute("firstaday", firstaday);
			}
			
			
			// 電子對帳單退件處理
			String remailid = (String) SessionUtil.getAttribute(model, SessionUtil.REMAILID, null);
			if(StrUtils.isNotEmpty(remailid)) {
				log.debug("INDEX.remailid: " + remailid);
				boolean showRemailId = false;
				String remailIdText = "";
				
//				boolean showR1 = "1".equals(remailid.substring(0, 1));
				boolean showR2 = "1".equals(remailid.substring(1, 2));
				boolean showR3 = "1".equals(remailid.substring(2, 3));
				boolean showR4 = "1".equals(remailid.substring(3, 4));
				
				if (showR2 || showR3 || showR4) {
					showRemailId = true;
					
					if (showR2) {
						remailIdText += i18n.getMsg("LB.X2459");
					}
					if (showR3) {
						if (StrUtils.isNotEmpty(remailIdText)) {
							remailIdText += "</br>";
						}
						remailIdText += i18n.getMsg("LB.X2460");
					}
					if (showR4) {
						if (StrUtils.isNotEmpty(remailIdText)) {
							remailIdText += "</br>";
						}
						remailIdText += i18n.getMsg("LB.X2461");
					}
				}
				
				model.addAttribute("showRemailId", showRemailId);
				model.addAttribute("remailIdText", remailIdText);
			}
			
		} catch (Exception e) {
			log.error("",e);
		}
		
		log.trace("target: " + target);
		return target;
	}
	
	
	// 修改使用者基本資料
	@RequestMapping(value = "/updateA106_aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult updateA106_aj(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("updateA106_aj...");
		BaseResult bs = null;
		String cusidn = "";
		try {
			cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				log.trace("updateA106_aj.cusidn: " + cusidn);
				
				// 修改A106使用者基本資料
				bs = index_service.getA106(cusidn, reqParam);
			}
				
		} catch (Exception e) {
			log.error("",e);
		}
		return bs;
	}
	
	// 取得TXNUSER用戶名稱
	@RequestMapping(value = "/username_aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult username_aj(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("username_aj...");
		BaseResult bs = null;
		String cusidn = "";
		try {
			cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				log.trace("cusidn: " + cusidn);
				// TXNUSER
				bs = new BaseResult();
				bs = index_service.getTxnUser(cusidn);
				
				// 更新Session
				if(bs.getResult()) {
					Map<String,Object> resultMap = null;
					resultMap = (Map<String, Object>) bs.getData();
					log.trace("username_aj.resultMap: {}", resultMap);
					
					// TXNUSER-我的Email
					SessionUtil.addAttribute(model, SessionUtil.DPMYEMAIL, String.valueOf(resultMap.get("DPMYEMAIL")));
					// TXNUSER-使用者姓名
					SessionUtil.addAttribute(model, SessionUtil.DPUSERNAME, String.valueOf(resultMap.get("DPUSERNAME")));
					// 畫面顯示--使用者姓名(跟TXNUSER來源一樣，只是存在不同SESSION)
					SessionUtil.addAttribute(model, SessionUtil.SUSERNAME, String.valueOf(resultMap.get("DPUSERNAME")));
					
				}
				
				log.trace("username_aj.bs: {}", CodeUtil.toJson(bs));
				
			} else {
				log.error("session no cusidn!!!");
			}
		} catch (Exception e) {
			log.error("",e);
		}
		return bs;
	}
	
	// 取得資產負債圖資料
	@RequestMapping(value = "/assets_aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult assets_aj(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("assets_aj...");
		BaseResult bs = null;
		String cusidn = "";
		try {
			cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				log.trace("cusidn: " + cusidn);
				// A1000
				bs = acct_service.assets_query(cusidn);
			} else {
				log.error("session no cusidn!!!");
			}
		} catch (Exception e) {
			log.error("",e);
		}
		return bs;
	}
	
	
	// 取得公告訊息
	@RequestMapping(value = "/bulletin_aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody List<ADMANN> getBulletin(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("getBulletin...");
		// 公告訊息
		List<ADMANN> bulletin_List = null;
		String cusidn = "";
		try {
			cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				log.trace("cusidn: " + cusidn);
				// 取得公告訊息
	    		bulletin_List = index_service.getByUser(cusidn);
			}else {
				log.error("session no cusidn!!!");
			}
			
		} catch (Exception e) {
			log.error("",e);
		}
		return bulletin_List;
	}
	
//	// 確認是否是登入後第一次進入首頁
//	@RequestMapping(value = "/checkMail_aj", produces = {"application/json;charset=UTF-8"})
//	public @ResponseBody BaseResult checkMail_aj(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
//		log.debug("checkMail_aj...");
//		boolean result = true;
//		try {
//			// 點擊我的首頁不寄信
//			if(SessionUtil.getAttribute(model, SessionUtil.SENDMAIL, null) != null) {
//				result = false;
//				
//			} // 登入成功寄信並註記
//			else {
//				SessionUtil.addAttribute(model, SessionUtil.SENDMAIL, false);
//			}
//			
//		} catch (Exception e) {
//			log.error("",e);
//		}
//		
//		BaseResult bs = new BaseResult();
//		bs.setResult(result);
//		return bs;
//	}

	
	// 取得臺幣預約轉帳查詢取消資料
	@RequestMapping(value = "/index_TW" , produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult getReservTW(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("getReservTW...");
		// 處理結果
		List<String> reservTwDateList = null;
		String cusidn = "";
		try {
			cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				log.trace("cusidn: " + cusidn);
				// 臺幣預約轉帳查詢
				reservTwDateList = index_service.getReservTwDate(cusidn);
				log.trace(ESAPIUtil.vaildLog("reservTwDateList:" + reservTwDateList));
			}else {
				log.error("session no cusidn!!!");
			}
			
		} catch (Exception e) {
			log.error("",e);
		}
		BaseResult bs = new BaseResult();
		bs.setResult(true);
		bs.addData("data", reservTwDateList);
		
		return bs;
	}
	
	// 取得外幣預約轉帳查詢取消資料
	@RequestMapping(value = "/index_FX" , produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult getReservFX(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("getReservFX...");
		// 處理結果
		List<String> reservFxDateList = null;
		String cusidn = "";
		try {
			cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				log.trace("cusidn: " + cusidn);
				// 臺幣預約轉帳查詢
				reservFxDateList = index_service.getReservFxDate(cusidn);
				log.trace(ESAPIUtil.vaildLog("reservFxDateList:" + reservFxDateList ));
			}else {
				log.error("session no cusidn!!!");
			}
			
		} catch (Exception e) {
			log.error("",e);
		}
		BaseResult bs = new BaseResult();
		bs.setResult(true);
		bs.addData("data", reservFxDateList);
		
		return bs;

	}
	
	
	@RequestMapping(value="/errorcode")
	public String toErrorCode() {
		log.trace("errorcode...");
		return "/index/errorcode";
	}
	
	@RequestMapping(value="/safe")
	public String toSafe() {
		log.trace("safe...");
		return "/index/safe";
	}
	
	
	// 取得臺幣資料
	@RequestMapping(value = "/summary_TW" , produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult getSummaryTW(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("getSummaryTW...");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			
			String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if (!"".equals(cusidn) && cusidn != null) {
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				log.trace("cusidn: " + cusidn);
				
				bs = index_service.getTW_summary(cusidn);
				
			} else {
				log.error("session no cusidn!!!");
			}

		} catch (Exception e) {
			log.error("", e);
		}
		
		return bs;
	}
	
	// 取得外幣資料
	@RequestMapping(value = "/summary_FX" , produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult getSummaryFX(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("getSummaryFX...");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			
			String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if (!"".equals(cusidn) && cusidn != null) {
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				log.trace("cusidn: " + cusidn);
				
				// 外幣餘額查詢
				Future<String> fxBalanceResult = index_service.getFX_balance(cusidn);
				// 外幣定期查詢
				Future<String> fxDdepositResult = index_service.getFX_deposit(cusidn);
				
				while (!fxBalanceResult.isDone() || !fxDdepositResult.isDone()) {
			        log.trace("Continue doing something else.");
			        Thread.sleep(1000);
			    }
				log.trace("fxBalanceResult.get(): " + fxBalanceResult.get());
				log.trace("fxDdepositResult.get(): " + fxDdepositResult.get());

				bs.setResult(true);
				bs.addData("summary_FX_balance", fxBalanceResult.get());
				bs.addData("summary_FX_deposit", fxDdepositResult.get());
				
			} else {
				log.error("session no cusidn!!!");
			}

		} catch (Exception e) {
			log.error("", e);
		}
		
		return bs;
	}
	
	
	// 取得信用卡資料
	@RequestMapping(value = "/summary_CRD" , produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult getSummaryCRD(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("getSummaryCRD...");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			
			String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if (!"".equals(cusidn) && cusidn != null) {
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				log.trace("cusidn: " + cusidn);
				
				// 信用卡總覽
				Future<String> crdSummaryResult = index_service.getCRD_summary(cusidn);
				
				while (!crdSummaryResult.isDone()) {
					log.trace("Continue doing something else.");
					Thread.sleep(1000);
				}
				log.trace("crdSummaryResult.get(): " + crdSummaryResult.get());
				
				bs.setResult(true);
				bs.addData("summary_CRD_result", crdSummaryResult.get());
				
			} else {
				log.error("session no cusidn!!!");
			}
			
		} catch (Exception e) {
			log.error("", e);
		}
		
		return bs;
	}
	
	// 取得基金資料
	@RequestMapping(value = "/summary_FUND" , produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult getSummaryFUND(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("getSummaryFUND...");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			
			String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if (!"".equals(cusidn) && cusidn != null) {
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				log.trace("cusidn: " + cusidn);
				String dpusername =(String)SessionUtil.getAttribute(model,SessionUtil.DPUSERNAME,null);
				log.trace("dpusername: " + dpusername);
				
				// 信用卡總覽
				Future<String> fundSummaryResult = index_service.getFUND_summary(cusidn, dpusername);
				Future<String> blaSummaryResult = index_service.getBLA_summary(cusidn);
				
				while (!fundSummaryResult.isDone() || !blaSummaryResult.isDone()) {
					log.trace("Continue doing something else.");
					Thread.sleep(1000);
				}
				log.trace("fundSummaryResult.get(): " + fundSummaryResult.get());
				log.trace("blaSummaryResult.get(): " + blaSummaryResult.get());
				
				bs.setResult(true);
				bs.addData("summary_FUND_result", fundSummaryResult.get());
				bs.addData("summary_BLA_result", blaSummaryResult.get());
				
			} else {
				log.error("session no cusidn!!!");
			}
			
		} catch (Exception e) {
			log.error("", e);
		}
		
		return bs;
	}
	
	// 取得借款資料
	@RequestMapping(value = "/summary_LOAN" , produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult getSummaryLOAN(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("getSummaryLOAN...");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			
			String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if (!"".equals(cusidn) && cusidn != null) {
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				log.trace("cusidn: " + cusidn);
				
				// 借款明細
				reqParam.put("CUSIDN", cusidn);
				Future<String> loanTwSummaryResult = index_service.getLOAN_N320(reqParam);
				Future<String> loanFxSummaryResult = index_service.getLOAN_N552(reqParam);
				
				while (!loanTwSummaryResult.isDone() || !loanFxSummaryResult.isDone()) {
					log.trace("Continue doing something else.");
					Thread.sleep(1000);
				}
				log.trace("loanTwSummaryResult.get(): " + loanTwSummaryResult.get());
				log.trace("loanFxSummaryResult.get(): " + loanFxSummaryResult.get());

				bs.setResult(true);
				bs.addData("loan_tw_result", loanTwSummaryResult.get());
				bs.addData("loan_fx_result", loanFxSummaryResult.get());
				
			} else {
				log.error("session no cusidn!!!");
			}
			
		} catch (Exception e) {
			log.error("", e);
		}
		
		return bs;
	}

	@RequestMapping(value = "/getUsuallyList" , produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult getUsuallyList(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("getUsuallyList...");
		BaseResult bs = null;
		try {
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			bs = index_service.getUsuallyList(cusidn);
		} catch (Exception e) {
			log.error("", e);
		}
		
		return bs;
	}
	
	@RequestMapping(value = "/getFavoriteList" , produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult getFavoriteList(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("getFavoriteList...");
		BaseResult bs = null;
		try {
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			bs = index_service.getFavoriteList(cusidn);
		} catch (Exception e) {
			log.error("", e);
		}
		
		return bs;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	@RequestMapping(value = "/certificate_url_aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult certificate_url_aj(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("certificate_url_aj...");
		BaseResult bs = null;
		String cusidn = "" , name ="" , account="" , ip = "";
		try {
			cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			name = (String) SessionUtil.getAttribute(model, SessionUtil.SUSERNAME, null);
			account = (String) SessionUtil.getAttribute(model, SessionUtil.USERID, null);
			ip = WebUtil.getIpAddr(request);
//			本地端測試用
//			ip = "10.10.215.92";
			
			if(StrUtils.isEmpty(cusidn) || StrUtils.isEmpty(name) ||  StrUtils.isEmpty(account) || StrUtils.isEmpty(ip) ) {
				bs = new BaseResult();
				bs.setMsgCode("-1");
				bs.setMessage("cusidn or name or account(userid) or ip isEmpty...");
				bs.setData(certificate_url);
				return bs;
			}
			
			cusidn = new String(Base64.getDecoder().decode(cusidn));
			account = new String(Base64.getDecoder().decode(account));
			log.trace("name: {}" , name);
			log.trace("account: {}" , account);
			bs = index_service.getCertificate_url(cusidn, account, ip, name);
		} catch (Exception e) {
			log.error("{}",e);
			bs = new BaseResult();
			bs.setMsgCode("-1");
			bs.setMessage("certificate_url_aj error...");
			bs.setData(certificate_url);
		}
		return bs;
	}
	
	@RequestMapping(value = "/fixing")
	public String fixing(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.debug("fixing...");
		String target = null;
		target = "/monitor/fixing";
		BaseResult bs = new BaseResult();
		return target;
	}
	
	@RequestMapping(value = "/emailalert_aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult emailalert_aj(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.debug("emailalert_aj...");
		BaseResult bs = null;
		String cusidn = "";
		String passAlert = "";
		try {
			cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			cusidn = new String(Base64.getDecoder().decode(cusidn));
			passAlert = (String) SessionUtil.getAttribute(model, SessionUtil.EMAILALERT, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				
				if(StrUtils.isEmpty(passAlert)) {
					log.trace("emailalert_aj.cusidn: " + cusidn);
					bs = verify_mail_service.emailAlertShow(cusidn);
					SessionUtil.addAttribute(model, SessionUtil.EMAILALERT, "PASS");
				}
				
			}
				
		} catch (Exception e) {
			log.error("",e);
		}
		return bs;
	}
	
	
	@RequestMapping(value = "/resendEmail_aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult resendEmail_aj(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.debug("resendEmail_aj...");
		BaseResult bs = null;
		String cusidn = "";
		try {
			cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			cusidn = new String(Base64.getDecoder().decode(cusidn));
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				log.trace("resendEmail_aj.cusidn: " + cusidn);
				
				reqParam.put("CUSIDN", cusidn);
				reqParam.put("EXECUTEFUNCTION", "RESEND");
				
				String IP = WebUtil.getIpAddr(request);
				log.debug(ESAPIUtil.vaildLog("IP >> " + IP));
				reqParam.put("IP",IP);
				
				
				bs = verify_mail_service.N930_TX_REST(reqParam);
			}
				
		} catch (Exception e) {
			log.error("",e);
		}
		return bs;
	}
	
}
