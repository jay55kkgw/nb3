package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.Map;

/**
 * 通知手機啟動驗證
 * 
 * @author Leo
 *
 */
public class svCreate_Verify_Txn_REST_RQ extends BaseRestBean_IDGATE implements Serializable {
	
	private static final long serialVersionUID = -8105756062267887930L;

	private String idgateID; //IDGATE ID
	private String deviceID; //裝置ID
	private String authType; //
	private String title; //
	private Map<String,String> txnData; //
	private Map<String,Object> txnDataView; //
	private String push; //是否推播
	private String adopid; //功能代號
//	private String LOGINTYPE; //使用系統
	private Boolean resultCode; 
	public String getIdgateID() {
		return idgateID;
	}
	public void setIdgateID(String idgateID) {
		this.idgateID = idgateID;
	}
	public String getDeviceID() {
		return deviceID;
	}
	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}
	public String getAuthType() {
		return authType;
	}
	public void setAuthType(String authType) {
		this.authType = authType;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	public String getPush() {
		return push;
	}
	public void setPush(String push) {
		this.push = push;
	}
	public String getAdopid() {
		return adopid;
	}
	public void setAdopid(String adopid) {
		this.adopid = adopid;
	}
//	public String getLOGINTYPE() {
//		return LOGINTYPE;
//	}
//	public void setLOGINTYPE(String lOGINTYPE) {
//		LOGINTYPE = lOGINTYPE;
//	}
	public Boolean getResultCode() {
		return resultCode;
	}
	public void setResultCode(Boolean resultCode) {
		this.resultCode = resultCode;
	}
	public Map<String, String> getTxnData() {
		return txnData;
	}
	public void setTxnData(Map<String, String> txnData) {
		this.txnData = txnData;
	}
	public Map<String, Object> getTxnDataView() {
		return txnDataView;
	}
	public void setTxnDataView(Map<String, Object> txnDataView) {
		this.txnDataView = txnDataView;
	}
	
	
	
	
	

}
