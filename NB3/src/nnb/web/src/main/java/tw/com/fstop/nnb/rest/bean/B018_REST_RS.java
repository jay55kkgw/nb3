package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class B018_REST_RS  extends BaseRestBean implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 979968128784905294L;
	
	private String PRDNO;
	private String BUYPRICE;
	private String PRICEDATE;
	
	public String getPRDNO() {
		return PRDNO;
	}
	public void setPRDNO(String pRDNO) {
		PRDNO = pRDNO;
	}
	public String getBUYPRICE() {
		return BUYPRICE;
	}
	public void setBUYPRICE(String bUYPRICE) {
		BUYPRICE = bUYPRICE;
	}
	public String getPRICEDATE() {
		return PRICEDATE;
	}
	public void setPRICEDATE(String pRICEDATE) {
		PRICEDATE = pRICEDATE;
	}
	
}
