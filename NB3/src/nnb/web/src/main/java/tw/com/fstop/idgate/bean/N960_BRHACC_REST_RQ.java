package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import tw.com.fstop.nnb.rest.bean.BaseRestBean_GOLD;
import tw.com.fstop.nnb.rest.bean.BaseRestBean_OLA;

public class N960_BRHACC_REST_RQ extends BaseRestBean_OLA implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6438141964161067478L;
	String CUSIDN;
	String NCHECKNB;	// 不檢查網銀身份
	String BRHACC;	// 帳號所屬分行別

	public String getBRHACC() {
		return BRHACC;
	}

	public void setBRHACC(String bRHACC) {
		BRHACC = bRHACC;
	}

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}

	public String getNCHECKNB() {
		return NCHECKNB;
	}

	public void setNCHECKNB(String nCHECKNB) {
		NCHECKNB = nCHECKNB;
	}
	
}
