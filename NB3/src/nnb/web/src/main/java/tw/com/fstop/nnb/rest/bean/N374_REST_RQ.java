package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * N374電文RQ
 */
public class N374_REST_RQ extends BaseRestBean_FUND implements Serializable{
	private static final long serialVersionUID = 6868861910004819383L;
	private String CUSIDN;
	private String UID;
	private String FGTXWAY;
	private String PINNEW;
	private String TYPE;
	private String TRANSCODE;
	private String CDNO;
	private String TRADEDATE;
	private String RESTOREDAY;
	private String BILLSENDMODE;
	private String UNIT;
	private String FUNDACN;
	private String BANKID;
	private String FUNDAMT;
	private String SSLTXNO;
	private String CRY;
	private String FDAGREEFLAG;
	private String FDNOTICETYPE;
	private String FDPUBLICTYPE;
	private String DPMYEMAIL;
	private String CMTRMAIL;
	private String IP;

	//for txnlog
	private String ADOPID = "C033";
	
	private String FUNDLNAME;
	

	public String getCUSIDN(){
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN){
		CUSIDN = cUSIDN;
	}
	public String getUID(){
		return UID;
	}
	public void setUID(String uID){
		UID = uID;
	}
	public String getFGTXWAY(){
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY){
		FGTXWAY = fGTXWAY;
	}
	public String getPINNEW(){
		return PINNEW;
	}
	public void setPINNEW(String pINNEW){
		PINNEW = pINNEW;
	}
	public String getTYPE(){
		return TYPE;
	}
	public void setTYPE(String tYPE){
		TYPE = tYPE;
	}
	public String getTRANSCODE(){
		return TRANSCODE;
	}
	public void setTRANSCODE(String tRANSCODE){
		TRANSCODE = tRANSCODE;
	}
	public String getTRADEDATE(){
		return TRADEDATE;
	}
	public void setTRADEDATE(String tRADEDATE){
		TRADEDATE = tRADEDATE;
	}
	public String getRESTOREDAY(){
		return RESTOREDAY;
	}
	public void setRESTOREDAY(String rESTOREDAY){
		RESTOREDAY = rESTOREDAY;
	}
	public String getUNIT(){
		return UNIT;
	}
	public void setUNIT(String uNIT){
		UNIT = uNIT;
	}
	public String getFUNDACN(){
		return FUNDACN;
	}
	public void setFUNDACN(String fUNDACN){
		FUNDACN = fUNDACN;
	}
	public String getBANKID(){
		return BANKID;
	}
	public void setBANKID(String bANKID){
		BANKID = bANKID;
	}
	public String getBILLSENDMODE(){
		return BILLSENDMODE;
	}
	public void setBILLSENDMODE(String bILLSENDMODE){
		BILLSENDMODE = bILLSENDMODE;
	}
	public String getFUNDAMT(){
		return FUNDAMT;
	}
	public void setFUNDAMT(String fUNDAMT){
		FUNDAMT = fUNDAMT;
	}
	public String getSSLTXNO(){
		return SSLTXNO;
	}
	public void setSSLTXNO(String sSLTXNO){
		SSLTXNO = sSLTXNO;
	}
	public String getCRY(){
		return CRY;
	}
	public void setCRY(String cRY){
		CRY = cRY;
	}
	public String getFDAGREEFLAG(){
		return FDAGREEFLAG;
	}
	public void setFDAGREEFLAG(String fDAGREEFLAG){
		FDAGREEFLAG = fDAGREEFLAG;
	}
	public String getFDNOTICETYPE(){
		return FDNOTICETYPE;
	}
	public void setFDNOTICETYPE(String fDNOTICETYPE){
		FDNOTICETYPE = fDNOTICETYPE;
	}
	public String getFDPUBLICTYPE(){
		return FDPUBLICTYPE;
	}
	public void setFDPUBLICTYPE(String fDPUBLICTYPE){
		FDPUBLICTYPE = fDPUBLICTYPE;
	}
	public String getDPMYEMAIL(){
		return DPMYEMAIL;
	}
	public void setDPMYEMAIL(String dPMYEMAIL){
		DPMYEMAIL = dPMYEMAIL;
	}
	public String getCMTRMAIL(){
		return CMTRMAIL;
	}
	public void setCMTRMAIL(String cMTRMAIL){
		CMTRMAIL = cMTRMAIL;
	}
	public String getCDNO() {
		return CDNO;
	}
	public void setCDNO(String cDNO) {
		CDNO = cDNO;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getFUNDLNAME() {
		return FUNDLNAME;
	}
	public void setFUNDLNAME(String fUNDLNAME) {
		FUNDLNAME = fUNDLNAME;
	}
	public String getIP() {
		return IP;
	}
	public void setIP(String iP) {
		IP = iP;
	}
}