package tw.com.fstop.web.filter;

import java.io.IOException;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tw.com.fstop.nnb.service.Login_out_Service;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.web.util.WebUtil;

public class AdmnbFilter implements Filter {

	static Logger log = LoggerFactory.getLogger(AdmnbFilter.class);
	private Login_out_Service login_out_service;
	
    @Override
    public void init(FilterConfig filterConfig) throws ServletException{
    	
    }
    
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException{
    	log.trace("AdmmbFilter...");
    	
    	HttpServletRequest req  = (HttpServletRequest) request ;
    	HttpServletResponse resp = (HttpServletResponse)response;
    	HttpSession session = req.getSession();
    	
		// request.uri, e.x: /nnb/login
		String funcUri = req.getRequestURI();
		// contextPath, e.x: /nnb
		String contextPath = req.getContextPath();
		// servletPath, e.x: /login
//		String pathInfo = req.getPathInfo(); // getPathInfo在較新的Server上會為null，故改用getRequestURI去掉getContextPath
		String pathInfo = funcUri.indexOf(contextPath)==0 ? funcUri.replaceFirst(contextPath, "") : funcUri;
		

		log.trace(ESAPIUtil.vaildLog("AdmmbFilter.funcUri: " + funcUri)); 
		log.trace("AdmmbFilter.contextPath: " + contextPath);
		log.trace(ESAPIUtil.vaildLog("AdmmbFilter.pathInfo: " + pathInfo)); 
    	
		try {
			login_out_service = login_out_service==null ? SpringBeanFactory.getBean("login_out_Service") : login_out_service;
			if(
				!pathInfo.startsWith("/css/") && !pathInfo.startsWith("/js/") 
				&& !pathInfo.startsWith("/img/") && !pathInfo.startsWith("/fonts/")  
				&& !pathInfo.startsWith("/component/") && !pathInfo.startsWith("/clearMenuList")
				&& !pathInfo.startsWith("/errorCloss") && !pathInfo.startsWith("/clearAdmNbStatu")
				&& !pathInfo.startsWith("/ebillApply/")
			) {
				String AdmNbStatus = login_out_service.getAdmNbStatu();
		
				if(AdmNbStatus.equals("N")) {
					String redirectUrl = req.getContextPath() + "/errorCloss";
					resp.sendRedirect(redirectUrl);
					return;
				}
				if (AdmNbStatus.equals("T")) {
					String userip = WebUtil.getIpAddr(req);
					//Log Forging
					log.trace("userIP >> {}",ESAPIUtil.vaildLog(userip));
					boolean flag = false;
					List<String> ipList = login_out_service.getADMLOGINACLIP();
//					log.trace("ipList >> {}",ipList);
					for(String ip: ipList) {
//						log.trace("ip >> {}",ip);
//						log.trace("ip >> {}",ip.indexOf("-"));
						if(ip.indexOf("-") > -1) {
							if(ipIsValid(ip,userip)) {
								flag = true;
								break;
							}
						}else {
							if(ip.equals(userip)) {
								flag = true;
								break;
							}
						}
					}
					log.trace("flag >> {}",flag);
					if(!flag) {
						String redirectUrl = req.getContextPath() + "/errorCloss";
						resp.sendRedirect(redirectUrl);
						return;
					}
				}
			}
			// 通過此Filter
			chain.doFilter(request, response);
		} catch (Exception e) {
			log.error("",e);
		}
    }

	public void destroy() {
	}
	
	public static boolean ipIsValid(String ipSection, String ip) {
        if (ipSection == null)
            throw new NullPointerException("IP段不能為空!");
        if (ip == null)
            throw new NullPointerException("IP不能為空!");
        ipSection = ipSection.trim();
        ip = ip.trim();
//        log.trace("ipSection >>{}, ip >>{}",ipSection,ip);
        final String REGX_IP = "((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9]).){3}(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])";
        final String REGX_IPB = REGX_IP + "-" + REGX_IP;
        if (!ipSection.matches(REGX_IPB) || !ip.matches(REGX_IP))
            return false;
        int idx = ipSection.indexOf('-');
//        log.trace("idx >> {}", idx);
//        log.trace("ipSection >>{}, ip >>{}",ipSection,ip);
        String[] sips = ipSection.substring(0, idx).split("\\.");
        String[] sipe = ipSection.substring(idx + 1).split("\\.");
        String[] sipt = ip.split("\\.");
//        log.trace("sips >>{}, sipe >>{}, sipt>>{}",sips,sipe,sipt);
        long ips = 0L, ipe = 0L, ipt = 0L;
        for (int i = 0; i < 4; ++i) {
            ips = ips << 8 | Integer.parseInt(sips[i]);
            ipe = ipe << 8 | Integer.parseInt(sipe[i]);
            ipt = ipt << 8 | Integer.parseInt(sipt[i]);
        }
//        log.trace("ips >>{}, ipe >>{}, ipt>>{}",ips,ipe,ipt);
        if (ips > ipe) {
            long t = ips;
            ips = ipe;
            ipe = t;
        }
        return ips <= ipt && ipt <= ipe;
    }
}
