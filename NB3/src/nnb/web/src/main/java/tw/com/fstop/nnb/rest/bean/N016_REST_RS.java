package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N016_REST_RS extends BaseRestBean implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 775129110323679813L;
	
	
	private String ABEND ;
	private String USERDATA ;
	private String CMQTIME ;
	private String CMRECNUM ;
//	String TXID; // 父類別 已有 不需重複宣告 否則Gson 會出錯誤
	private String  CMPERIOD ;
	private String TOPMSG_016;
	private String TOPMSG_554;
	private LinkedList<N016_REST_RSDATA> TW;
	private LinkedList<N016_REST_RSDATA2> FX;
	
	
	public String getABEND() {
		return ABEND;
	}
	public void setABEND(String aBEND) {
		ABEND = aBEND;
	}
	public String getUSERDATA() {
		return USERDATA;
	}
	public void setUSERDATA(String uSERDATA) {
		USERDATA = uSERDATA;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getCMRECNUM() {
		return CMRECNUM;
	}
	public void setCMRECNUM(String cMRECNUM) {
		CMRECNUM = cMRECNUM;
	}
	public String getCMPERIOD() {
		return CMPERIOD;
	}
	public void setCMPERIOD(String cMPERIOD) {
		CMPERIOD = cMPERIOD;
	}
	public LinkedList<N016_REST_RSDATA> getTW() {
		return TW;
	}
	public void setTW(LinkedList<N016_REST_RSDATA> tW) {
		TW = tW;
	}
	public LinkedList<N016_REST_RSDATA2> getFX() {
		return FX;
	}
	public void setFX(LinkedList<N016_REST_RSDATA2> fX) {
		FX = fX;
	}
	public String getTOPMSG_016() {
		return TOPMSG_016;
	}
	public void setTOPMSG_016(String tOPMSG_016) {
		TOPMSG_016 = tOPMSG_016;
	}
	public String getTOPMSG_554() {
		return TOPMSG_554;
	}
	public void setTOPMSG_554(String tOPMSG_554) {
		TOPMSG_554 = tOPMSG_554;
	}
	
}