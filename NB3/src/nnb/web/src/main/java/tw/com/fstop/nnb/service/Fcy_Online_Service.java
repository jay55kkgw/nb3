package tw.com.fstop.nnb.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.po.ADMBHCONTACT;
import fstop.orm.po.TXNUSER;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.AdmBhContactDao;
import tw.com.fstop.tbb.nnb.dao.TxnUserDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.StrUtil;

@Service
public class Fcy_Online_Service extends Base_Service
{

	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private TxnUserDao txnUserDao;

	@Autowired
	private DaoService daoService;

	@Autowired
	private TxnUserDao userDao;

	@Autowired
	private AdmBhContactDao admBhContactDao;

	@Autowired
	I18n i18n;
	/**
	 * F004 外匯匯入匯款線上解款申請/註銷
	 * 
	 * @param cusidn
	 * @return
	 */
	public BaseResult fcy_Online_Service(String cusidn)
	{
		BaseResult bs = new BaseResult();
		try
		{
			TXNUSER user = daoService.chkRecord(cusidn);
			if (null != user)
			{
				String str_Flag = user.getFXREMITFLAG();
				bs.addData("str_Flag", str_Flag);
				bs.setResult(true);
			}
			else
			{
				log.debug(ESAPIUtil.vaildLog("TXNUSER is null >>>>> {}"+ user));
				bs.setMessage("FE002", i18n.getMsg("LB.Check_no_data"));//查無資料
			}
		}
		catch (Exception e)
		{
			log.error("Fcy_Remittances_Service{}", e);
		}
		return bs;
	}

	/**
	 * F004 外匯匯入匯款線上解款申請/註銷 確認頁
	 * 
	 * @param reqParamMap
	 * @return
	 */
	public BaseResult f_remittances_apply_confirm(Map<String, String> reqParamMap)
	{
		BaseResult bs = new BaseResult();
		try
		{
			bs.addData("TXTYPE", reqParamMap.get("TXTYPE"));
			bs.setResult(true);
		}
		catch (Exception e)
		{
			log.error("f_remittances_apply_confirm{}", e);
		}
		return bs;
	}

	/**
	 * F004 外匯匯入匯款線上解款申請/註銷 結果頁
	 * 
	 * @param cusidn
	 * @return
	 */
	public BaseResult f_remittances_apply_result(String cusidn, Map<String, String> reqParamMap)
	{
		BaseResult bs = new BaseResult();
		try
		{
			reqParamMap.put("CUSIDN", cusidn);
			// Call REST電文
			bs = F035_REST(reqParamMap);
			if (bs != null && bs.getResult())
			{
				bs.addData("TXTYPE", reqParamMap.get("TXTYPE"));
			}
		}
		catch (Exception e)
		{
			log.error("f_remittances_apply_confirm{}", e);
		}
		return bs;
	}

	/**
	 * F003 外匯匯入匯款線上解款清單
	 * 
	 * @param cusidn
	 * @return
	 */
	public BaseResult f_remittances_payments_step1(String cusidn)
	{
		BaseResult bs = new BaseResult();
		try
		{
			// 判斷日期
			if (!daoService.isFxBizTime())
			{
				//外匯交易超過營業時間(銀行營業日9:30-15:30)，請於營業時間內執行。
				bs.setMessage("Z401", i18n.getMsg("LB.X1830"));
				return bs;
			}
			else
			{
				bs = F013_REST(cusidn);
				// return data 資料加工
				if (bs != null && bs.getResult())
				{
					// log
					log.debug("f_remittances_payments_step1 F013_REST in >>>>> {}", bs.getData());
					Map<String, ArrayList<Map<String, String>>> getDataMap = (Map<String, ArrayList<Map<String, String>>>) bs.getData();
					ArrayList<Map<String, String>> dataListMap = (ArrayList<Map<String, String>>) getDataMap.get("REC");
					// 依條件移除資料
					Iterator<Map<String, String>> iterator = dataListMap.iterator();
					// 檢查 F013 下行電文中,若有非本人之明細則不帶出
					while (iterator.hasNext())
					{

						Map<String, String> map = iterator.next(); // must be called before you can call i.remove()
						// //檢查 F013 下行電文中,若有非本人之明細則不帶出
						String custid = map.get("CUSTID");
						if (!cusidn.equals(custid))
						{
							iterator.remove();
						}
						else
						{
							map.put("showTXAMT", NumericUtil.formatNumberString(map.get("TXAMT"), 2));
							map.put("helperjson", CodeUtil.toJson(map));
						}
					}
					// 用整理後的筆數COUNT
					bs.addData("COUNT", String.valueOf(dataListMap.size()));
					// log
					log.debug("f_remittances_payments_step1 F013_REST out>>>>> {}", bs.getData());
					TXNUSER user = daoService.chkRecord(cusidn);
					String str_Flag = user.getFXREMITFLAG();
					bs.addData("FXREMITFLAG", str_Flag);
				}
				else
				{
					if (!daoService.isErrorCode(bs.getMsgCode()))
					{
						TXNUSER user = daoService.chkRecord(cusidn);
						String str_Flag = user.getFXREMITFLAG();
						// 已申請
						if (str_Flag.equals("Y"))
						{
							// TopMessageException tex = TopMessageException.create("ENRD", "F003"); //
							// 無任何匯入匯款線上解款資料時,產生
							// ENRD
							// 訊息
							//無任何匯入匯款線上解款資料
							bs.setMessage("ENRD", i18n.getMsg("LB.X1834"));
							return bs;
						}
						// 已取消 or 尚未申請
						else
						{
							//已取消 or 尚未申請
							bs.setMessage("ENRD", i18n.getMsg("LB.X1833")+"or"+i18n.getMsg("LB.D0276"));
							return bs;
						}
					}

				}
			}
		}

		catch (Exception e)
		{
			log.error("f_remittances_payments_step1 {}", e);
		}
		return bs;
	}

	/**
	 * 
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult f_remittances_payments_step2(String cusidn, Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{
			// 科目 50~59、 12、60、61、62、64
			// CALL N920電文 取得轉出帳號
			bs = N920_REST(cusidn, Fcy_Remittances_Service.FX_OUT_ACNO);

			// 後製把REC 裡面的資料轉成下拉選單所要的格式
			if (bs != null && bs.getResult())
			{
				Map<String, Object> callDataN920 = (Map<String, Object>) bs.getData();

				String str_CUSTIDF = (String) callDataN920.get("CUSTIDF");
				String str_CUSTYPE = "";
				if (str_CUSTIDF.equals("1"))
				{
					str_CUSTYPE = "1";
				}
				else if (str_CUSTIDF.equals("2"))
				{
					str_CUSTYPE = "2";
				}
				else if (str_CUSTIDF.equals("3") || str_CUSTIDF.equals("5"))
				{
					str_CUSTYPE = "3";
				}
				else if (str_CUSTIDF.equals("4") || str_CUSTIDF.equals("6"))
				{
					str_CUSTYPE = "4";
				}

				log.debug(ESAPIUtil.vaildLog("f_remittances_payments_step2 in {}" + CodeUtil.toJson(reqParam)));
				String helperjson = reqParam.get("helperjson");
				reqParam = CodeUtil.fromJson(helperjson, Map.class);
				List<Map> optionList = new LinkedList<>();
				String str_RETCCY = reqParam.get("RETCCY");
				String str_Flag = "";
				if (str_RETCCY.equals("TWD"))
				{
					str_Flag = "T";
				}
				// 建立解款幣別
				String TXCCY = reqParam.get("TXCCY");
				Map<String, String> optionMap = new HashMap<String, String>();
				optionMap.put("TXCCY", TXCCY);
				optionList.add(optionMap);
				// 如果符合條件則新增TWD
				if (!reqParam.get("BENACC").startsWith("893"))
				{
					Map<String, String> optionMap2 = new HashMap<String, String>();
					optionMap2.put("TXCCY", "TWD");
					optionList.add(optionMap2);

				}
				bs.setData(reqParam);
				bs.addData("optionList", optionList);
				bs.addData("str_Flag", str_Flag);
				bs.addData("CUSTYPE", str_CUSTYPE);
				log.debug(ESAPIUtil.vaildLog("f_remittances_payments_step2 out {}"+ CodeUtil.toJson(bs.getData())));
				List<ADMBHCONTACT> bhContactList = admBhContactDao.findByBhID(((String) ((Map<String, Object>) bs.getData()).get("BENACC")).substring(0, 3));
				bs.addData("ADCONTACTTEL", bhContactList.get(0).getADCONTACTTEL());
			}
		}
		catch (Exception e)
		{
			log.error("f_remittances_payments_step2 {}", e);
		}
		return bs;
	}

	/**
	 * 
	 * @param cusidn
	 * @param transfer_data
	 * @param reqParam
	 * @return
	 */
	public BaseResult f_remittances_payments_step3(String cusidn, Map<String, String> transfer_data,
			Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{
			transfer_data.putAll(reqParam);
			transfer_data.put("CUSIDN", cusidn);
			bs = F003R_REST(transfer_data);
			if (bs != null && bs.getResult())
			{
				Map<String, Object> callDataF003R = (Map<String, Object>) bs.getData();
				String CURAMT = String.valueOf(callDataF003R.get("CURAMT"));
				String ATRAMT = String.valueOf(callDataF003R.get("ATRAMT"));
				callDataF003R.putAll(transfer_data);
				callDataF003R.put("showCURAMT", NumericUtil.fmtAmount(CURAMT, 2));// 轉出
				callDataF003R.put("showTXAMT", NumericUtil.fmtAmount(ATRAMT, 2));// 轉入
			}

		}
		catch (Exception e)
		{
			log.error("f_remittances_payments_step3 {}", e);
		}
		log.debug("f_remittances_payments_step3 bs Data {}", bs.getData());
		return bs;
	}

	/**
	 * 
	 * @param cusidn
	 * @param transfer_data
	 * @param reqParam
	 * @return
	 */
	public BaseResult f_remittances_payments_confirm(String cusidn, Map<String, String> transfer_data,
			Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{
			reqParam.putAll(transfer_data);

			bs.setData(reqParam);
			bs.setResult(true);
		}
		catch (Exception e)
		{
			log.error("f_remittances_payments_step2 {}", e);
		}
		return bs;
	}

	/**
	 * show transfer_data 加工
	 * 
	 * @param transfer_data
	 * @return 新增<br>
	 *         :str_ORDMEMO1<br>
	 *         BGROENO
	 */
	public Map<String, String> transfer_data_Retouch(Map<String, String> transfer_data)
	{

		StringBuffer str_ORDMEMO1 = new StringBuffer();
		if (transfer_data.get("ORDMEMO1") != null)
		{
			str_ORDMEMO1.append((String) transfer_data.get("ORDMEMO1"));
		}
		if (transfer_data.get("ORDMEMO2") != null)
		{

			str_ORDMEMO1.append("<br>" + (String) transfer_data.get("ORDMEMO2"));
		}
		if (transfer_data.get("ORDMEMO3") != null)
		{
			str_ORDMEMO1.append("<br>" + (String) transfer_data.get("ORDMEMO3"));
		}
		if (transfer_data.get("ORDMEMO4") != null)
		{
			str_ORDMEMO1.append("<br>" + (String) transfer_data.get("ORDMEMO4"));
		}
		transfer_data.put("str_ORDMEMO1", str_ORDMEMO1.toString());
		transfer_data.put("BGROENO", (transfer_data.get("BGROENO") == null) ? "" : transfer_data.get("BGROENO"));
		// 付款人身分別中文
		String BENTYPE = transfer_data.get("BENTYPE");
		String display_BENTYPE = "";
		switch (BENTYPE)
		{
			case "1":
				display_BENTYPE = i18n.getMsg("LB.Government");//政府
				break;
			case "2":
				display_BENTYPE = i18n.getMsg("LB.Public_enterprise");//公營事業
				break;
			case "3":
				display_BENTYPE = i18n.getMsg("LB.Private_enterprise");//民間
				break;
			case "4":
				display_BENTYPE = i18n.getMsg("LB.Other_Account");//他人帳戶
				break;
			case "5":
				display_BENTYPE = i18n.getMsg("LB.My_Account");//本人帳戶
				break;
			default:
				break;
		}
		transfer_data.put("display_BENTYPE", display_BENTYPE);

		return transfer_data;

	}

	/**
	 * F003 外匯匯入匯款線上解款
	 * @param cusidn
	 * @param reqParam
	 * @param reqParam2
	 * @return
	 */
	public BaseResult f_remittances_payments_result(String cusidn, Map<String, String> transfer_data,
			Map<String, String> reqParam)
	{

		BaseResult bs = new BaseResult();
		try
		{
			transfer_data.put("CUSIDN", cusidn);
			transfer_data.put("nowDate", new DateTime().toString("yyyy/MM/dd HH:mm:ss"));
			transfer_data_Retouch(transfer_data);
			reqParam.putAll(transfer_data);
			//txnLog 所需的欄位
			reqParam.put("ADOPID", "F003");
			reqParam.put("ADTXACNO", reqParam.get("FYTSFAN"));// 轉入帳號 
//			reqParam.put("ADSVBH", "050");// 轉入服務銀行 
			reqParam.put("ADTXAMT", reqParam.get("TXAMT"));// 轉出金額
			reqParam.put("ADCURRENCY", reqParam.get("TXCCY"));// 轉出幣別 
			reqParam.put("ADAGREEF", "1");// 轉入帳號約定或非約定註記>>0:非約定，1:約定//固定為 1：約定
			
			
			log.debug(ESAPIUtil.vaildLog("transfer_data >>>> {}"+ CodeUtil.toJson(transfer_data)));
			log.debug(ESAPIUtil.vaildLog("reqParam >>>> {}" + CodeUtil.toJson(reqParam)));
			bs = F003T_REST(reqParam);
			// 後製把 裡面的資料轉成所要的格式
			if (bs != null && bs.getResult())
			{
				// call N920取得姓名
				BaseResult bsN920 = N920_REST(cusidn, FX_OUT_ACNO);
				Map<String, Object> callDataN920 = (Map<String, Object>) bsN920.getData();
				String name = String.valueOf(callDataN920.get("NAME"));

				Map<String, Object> callDataF003T = (Map<String, Object>) bs.getData();
				// 手續費
				// 費用合計
				String str_CommAmt = String.valueOf(callDataF003T.get("COMMAMT2"));
				String str_CommCcy = String.valueOf(callDataF003T.get("COMMCCY2"));
				if (str_CommCcy.equals("TWD") || str_CommCcy.equals("JPY"))
				{
					str_CommAmt = str_CommAmt.substring(0, str_CommAmt.length() - 2);
				}
				// 匯款金額
				String str_OrgAmt = String.valueOf(callDataF003T.get("ORGAMT"));
				String str_OrgCcy = String.valueOf(callDataF003T.get("ORGCCY"));
				if (str_OrgCcy.equals("TWD") || str_OrgCcy.equals("JPY"))
				{
					str_OrgAmt = str_OrgAmt.substring(0, str_OrgAmt.length() - 2);
				}
				// 解款金額
				String str_PmtAmt = String.valueOf(callDataF003T.get("PMTAMT"));
				String str_PmtCcy = String.valueOf(callDataF003T.get("PMTCCY"));
				if (str_PmtCcy.equals("TWD") || str_PmtCcy.equals("JPY"))
				{
					str_PmtAmt = str_PmtAmt.substring(0, str_PmtAmt.length() - 2);
				}
				// 匯率
				String str_Rate = String.valueOf(callDataF003T.get("EXRATE"));
				if (callDataF003T.get("EXRATE") == null)
				{
					str_Rate = "";
				}
				else if (str_Rate != null && str_Rate.replace("0", "").equals(""))
				{
					str_Rate = "";
				}
				else
				{
					str_Rate = NumericUtil.formatNumberString(str_Rate, 8);

				}
				bs.addAllData(transfer_data);
				bs.addData("NAME", name);// 姓名
				bs.addData("str_Rate", str_Rate);// 匯率
				bs.addData("str_CommAmt", NumericUtil.formatNumberStringByCcy(str_CommAmt, 2, str_CommCcy));//// 手續費
				bs.addData("str_OrgAmt", NumericUtil.formatNumberStringByCcy(str_OrgAmt, 2, str_OrgCcy));//// 匯款金額
				bs.addData("str_PmtAmt", NumericUtil.formatNumberStringByCcy(str_PmtAmt, 2, str_PmtCcy));// // 解款金額
				bs.addData("jspTitle", i18n.getMsg("LB.W0317"));//外匯匯入匯款線上解款
				// 取得塞DB的HTML
//				String str_ResultHtml = CodeUtil.HtmlToString("/htmlTemplate/f_remittances_payments_result_print.html", callDataF003T);
//				String adtxno = String.valueOf(callDataF003T.get("ADTXNO"));
//				daoService.updateFxCert(adtxno, str_ResultHtml);
			}

			log.debug("f_outward_remittances_result bs data {}", bs.getData());
		}
		catch (Exception e)
		{
			log.error("f_remittances_step1 {}", e);
		}
		return bs;
	}

	/**
	 * 檢查TxToken
	 * 
	 * @param pageTxToken 頁面TxToken
	 * @param SessionFinshToken
	 * @return false 表示有問題
	 */
	public BaseResult validateToken(String pageTxToken, String SessionFinshToken)
	{
		BaseResult bs = new BaseResult();
		try
		{
			if (pageTxToken == null)
			{
				log.debug(" reqTxToken==null");
				bs.setMessage("FE002", "reqTxToken空白");
				log.debug("validateToken getResult:{}", bs.getMsgCode());
				log.debug("validateToken getResult:{}", bs.getMessage());
			}
			else if (StrUtil.isNotEmpty(SessionFinshToken) && SessionFinshToken.equals(pageTxToken))
			{
				log.debug("validateToken2 getResult:{}", bs.getMsgCode());
				log.debug("validateToken2 getResult:{}", bs.getMessage());
				log.debug(" pagToken SessionFinshToken &&  一樣 重複交易");
				bs.setMessage("FE002", i18n.getMsg("LB.X1804"));//重複交易
			}
			else
			{
				bs.setResult(true);
			}
		}
		catch (Exception e)
		{
			log.error("getTransferToken Error", e);
		}
		return bs;
	}

}
