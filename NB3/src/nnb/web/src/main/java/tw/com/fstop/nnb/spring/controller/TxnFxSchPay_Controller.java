package tw.com.fstop.nnb.spring.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import tw.com.fstop.common.BaseResult;

import tw.com.fstop.nnb.service.TxnFxSchPay_Service;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;

@RestController
@RequestMapping(value = "/MB/TXNFXSCHPAY")
public class TxnFxSchPay_Controller {

	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	TxnFxSchPay_Service txnfxschpay_Service;

	/*
	 * 外幣預約明細查詢
	 */
	@PostMapping(value = "/query", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody BaseResult query(HttpServletRequest request, HttpServletResponse response,
			@RequestBody Map<String, String> reqParam, Model model) {		
		log.info(ESAPIUtil.vaildLog("reqParam>>{}"+CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		try {
			bs = txnfxschpay_Service.query(reqParam);
		} catch (Throwable e) {
			log.error(e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", "查詢失敗");
		}
		return bs;
	}

	/*
	 * 外幣預約明細新增
	 */
	@PostMapping(value = "/add", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody BaseResult add(HttpServletRequest request, HttpServletResponse response,
			@RequestBody Map<String, String> reqParam, Model model) {		
		log.info(ESAPIUtil.vaildLog("reqParam>>{}"+CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		try {
			bs = txnfxschpay_Service.insert(reqParam);
		} catch (Exception e) {
			log.error(e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", "新增失敗");
		}
		return bs;
	}
	/*
	 * 外幣預約明細取消
	 */
	@PostMapping(value = "/cancel", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody BaseResult cancel(HttpServletRequest request, HttpServletResponse response,
			@RequestBody Map<String, String> reqParam, Model model) {		
		log.info(ESAPIUtil.vaildLog("reqParam>>{}"+CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		try {
			bs = txnfxschpay_Service.cancel(reqParam);
		} catch (Exception e) {
			log.error(e.toString());
			bs.setResult(Boolean.FALSE);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
			bs.setMessage("1", "取消失敗");
		}
		return bs;
	}
}
