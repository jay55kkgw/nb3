package tw.com.fstop.nnb.aop;

import java.util.Base64;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.validation.support.BindingAwareModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import brave.Tracer;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.StrUtil;


//https://blog.csdn.net/qq_27093465/article/details/78800100
//https://matthung0807.blogspot.com/2018/02/spring-aop.html
@Order(0)
@Aspect
@Component
public class LoginAop {

	Logger log = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private HttpServletRequest req; 
	@Autowired Tracer tracer;
//	@Autowired
//	private MyTrancer mytrancer;
//	@Autowired
//	HttpTracing httptracing;
//	@Autowired
//	AccessTokenFilter accesstokenfilter;
//	@Before("within(tw.com.fstop.nnb.spring.controller.Login_out_Controller)")
	@Before("within(tw.com.fstop.nnb.spring.controller..*)")
	public Object doBeforeLogin(JoinPoint joinPoint) {
		
		log.trace("doBeforeLogin...");
		String adguid = "";
		String cusidn = "";
		String userid = "";
		Object[] args = null;
		Map<String,String> data = null;
		try {
			
			try {
				args = joinPoint.getArgs();
				if(args !=null) {
					for(int i = 0 ; i < args.length ; i++) {
						if( args[i]  instanceof List) {
							List l = (List) args[i];
						}
						if( args[i]  instanceof Map) {
//							data = (Map) args[i];
						}
						if( args[i]  instanceof LinkedHashMap && !(args[i] instanceof BindingAwareModelMap)) {
							data = (Map) args[i];
						}
						if( args[i]  instanceof BindingAwareModelMap) {
//							data = (Map) args[i];
						}
					}
				}
			} catch (Exception e) {
				log.error("{}",e);
			}
			String methodName = joinPoint.getSignature().getName();
//			登入是 login_aj
			log.info("methodName>>{}",methodName);
			MethodSignature signature = (MethodSignature) joinPoint.getSignature();
			RequestMapping rqm = signature.getMethod().getAnnotation(RequestMapping.class);
			log.trace("rqm>>{}",rqm);
			
			adguid = UUID.randomUUID().toString();
			req.setAttribute(SessionUtil.ADGUID, adguid);
//			req.getSession().setAttribute(SessionUtil.ADGUID, adguid);
			
//			壓力測試用
			if(StrUtil.isNotEmpty(req.getParameter("PUUID"))) {
				adguid = req.getParameter("PUUID");
			}
			
//			if("login_aj".equals(methodName)) {
//				adguid = UUID.randomUUID().toString();
//			}else {
//				adguid = (String) req.getSession().getAttribute(SessionUtil.ADGUID);
//				if(StrUtil.isEmpty(adguid)) {
//					adguid = UUID.randomUUID().toString();
//					req.getSession().setAttribute(SessionUtil.ADGUID, adguid);
//				}
//			}
			
			data = data ==null ? new HashMap<String, String>():data;
			
			cusidn =  data.get("cusidn");
			userid =  data.get("userName");
			log.trace(ESAPIUtil.vaildLog("cusidn >> " + cusidn));
			if(StrUtil.isEmpty(cusidn)) {
				cusidn = (String) req.getSession().getAttribute(SessionUtil.CUSIDN);
				cusidn = StrUtil.isNotEmpty(cusidn) ?new String( Base64.getDecoder().decode(cusidn) ,"utf-8"):cusidn;
			}
			if(StrUtil.isEmpty(userid)) {
				userid = (String) req.getSession().getAttribute(SessionUtil.USERID);
				userid = StrUtil.isNotEmpty(userid) ? new String( Base64.getDecoder().decode(userid) ,"utf-8") :userid;
			}
			log.info(ESAPIUtil.vaildLog("adguid>>{}" + adguid));
			log.trace(ESAPIUtil.vaildLog("cusidn2 >> " + cusidn)); 
//			accesstokenfilter.tracer.currentSpan();
			if(StrUtil.isNotEmpty(cusidn)) {
				MDC.put("loginid",cusidn );
			}
			if(StrUtil.isNotEmpty(userid)) {
				MDC.put("userid",userid );
			}
			if(StrUtil.isNotEmpty(userid)) {
				MDC.put("adguid",adguid );
			}
			log.trace("traceId>>{}",tracer.currentSpan());
			log.trace("tracer>>{}",tracer);
		} catch (Exception e) {
			log.error(e.toString());
		}
		return Boolean.TRUE;
		
	}

	@After("within(tw.com.fstop.nnb.spring.controller..*)")
	public Object doAfterLogin(JoinPoint joinPoint) {
		MDC.put("loginid","" );
		MDC.put("userid","" );
		MDC.put("adguid","" );
		return Boolean.TRUE;
	}
	
	
}
