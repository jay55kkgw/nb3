package tw.com.fstop.nnb.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.hsqldb.lib.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import fstop.orm.po.SYSPARAMDATA;
import fstop.orm.po.TXNADDRESSBOOK;
import fstop.orm.po.TXNTRACCSET;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.AdmBankDao;
import tw.com.fstop.tbb.nnb.dao.SysParamDataDao;
import tw.com.fstop.tbb.nnb.dao.TxnTrAccSetDao;
import tw.com.fstop.tbb.nnb.dao.TxnTwRecordDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.I18nUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.util.StrUtil;

@Service
public class Acct_Transfer_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private AdmBankDao admbankdao;
	
	@Autowired
	private DaoService daoservice; 
	
	@Autowired
	private TxnTrAccSetDao txntraccsetdao;

	@Autowired
	private TxnTwRecordDao txnTwRecordDao;

	@Autowired
	private SysParamDataDao sysParamDataDao;
	
	@Autowired
	private I18n i18n;

	@Autowired
	IdGateService idgateservice;
	
	
	
	/**
	 * 取得轉帳之轉入帳號
	 * @param cusidn
	 * @param type
	 * @return
	 */
	public BaseResult getAcnoList(String cusidn,String type) {
		log.trace("acno>>{} ",cusidn);
		BaseResult bs = null;
		LinkedHashMap<String,String> labelMap = null;
		LinkedHashMap<String,String> notAgreeLabelMap = null;
		ObjectMapper objectMapper = new ObjectMapper();
		String tmpStr = "";
		String agree = "";
		try {
			//  CALL WS api
			bs =new  BaseResult();
//			bs = daoservice.getAgreeAcnoList(cusidn,type,Boolean.TRUE);
			bs = daoservice.getAgreeAcnoList(cusidn,type,Boolean.FALSE);
//			後製把table 裡面的資料轉成下拉選單所要的格式
			if(bs.getResult()) {
				labelMap = new LinkedHashMap<String,String>();
				notAgreeLabelMap = new LinkedHashMap<String,String>();
				
				// 12/06目前使用者只有SSL驗證，只能開放約定帳號，等有晶片卡、iKEY驗證後，再開放 常用非約定帳號
				// 下拉選單-請選擇約定/常用非約定帳號，暫時改為請選擇帳號
				// labelMap.put("#0", "-----" + i18n.getMsg("LB.Select_designated_or_common_non-designated_account") + "-----");
				labelMap.put("#0", i18n.getMsg("LB.Select_account"));
				// 下拉選單-約定帳號
				labelMap.put("#1","--"+i18n.getMsg("LB.Designated_account")+"--");
				
				// 下拉選單-常用非約定帳號，暫時改為不顯示
				 notAgreeLabelMap.put("#2","--"+i18n.getMsg("LB.Common_non-designated_account")+"--");
				
				Map<String,Object> data = (Map<String,Object>) bs.getData();
				List<Map<String,String>> table_list = (List<Map<String, String>>) data.get("REC");
				//取出轉入帳號
				for(Map<String,String> tmp :table_list) {
					log.trace("tmp>> {}",tmp);
					log.trace("VALUE>> {}",tmp.get("VALUE"));
					if(!"#".equals(tmp.get("VALUE"))) {
						JsonNode jsonNode = objectMapper.readTree(tmp.get("VALUE"));
						agree = jsonNode.get("AGREE").asText();
					}

					tmpStr = tmp.get("VALUE").toString();
					
//					濾掉原生API 就有放#的部分
					if(tmpStr.indexOf("#") !=-1) {
						continue;
					}
					
					if("1".equals(agree)) {	
						labelMap.put(tmpStr, tmp.get("TEXT"));
					}
					else 
					{
						notAgreeLabelMap.put(tmpStr, tmp.get("TEXT"));
					}
				}
				labelMap.putAll(notAgreeLabelMap);
				//為了查轉入帳號問題
				log.info("INACNO List >>{}",labelMap);
				data.put("REC", labelMap);
				log.trace("data >>{}",data);
				bs.setData(data);
				
			}
//			String json = "{\"_MsgCode\":\"0\",\"_MsgName\":\"\",\"REC\":[{\"_ACN\":\"00160501049\",\"_BNKCOD\":\"050\",\"_AGREE\":\"1\",\"_DPGONAME\":\"\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"AGREE\\\":\\\"1\\\",\\\"ACN\\\":\\\"00160501049\\\",\\\"BNKCOD\\\":\\\"050\\\"}\",\"_TEXT\":\"050-臺灣企銀 00160501049\"},{\"_ACN\":\"00162565656\",\"_BNKCOD\":\"050\",\"_AGREE\":\"1\",\"_DPGONAME\":\"\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"AGREE\\\":\\\"1\\\",\\\"ACN\\\":\\\"00162565656\\\",\\\"BNKCOD\\\":\\\"050\\\"}\",\"_TEXT\":\"050-臺灣企銀 00162565656\"},{\"_ACN\":\"00162778821\",\"_BNKCOD\":\"050\",\"_AGREE\":\"1\",\"_DPGONAME\":\"\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"AGREE\\\":\\\"1\\\",\\\"ACN\\\":\\\"00162778821\\\",\\\"BNKCOD\\\":\\\"050\\\"}\",\"_TEXT\":\"050-臺灣企銀 00162778821\"},{\"_ACN\":\"00162889915\",\"_BNKCOD\":\"050\",\"_AGREE\":\"1\",\"_DPGONAME\":\"\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"AGREE\\\":\\\"1\\\",\\\"ACN\\\":\\\"00162889915\\\",\\\"BNKCOD\\\":\\\"050\\\"}\",\"_TEXT\":\"050-臺灣企銀 00162889915\"},{\"_ACN\":\"01013101163\",\"_BNKCOD\":\"050\",\"_AGREE\":\"1\",\"_DPGONAME\":\"\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"AGREE\\\":\\\"1\\\",\\\"ACN\\\":\\\"01013101163\\\",\\\"BNKCOD\\\":\\\"050\\\"}\",\"_TEXT\":\"050-臺灣企銀 01013101163\"},{\"_ACN\":\"01062010505\",\"_BNKCOD\":\"050\",\"_AGREE\":\"1\",\"_DPGONAME\":\"\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"AGREE\\\":\\\"1\\\",\\\"ACN\\\":\\\"01062010505\\\",\\\"BNKCOD\\\":\\\"050\\\"}\",\"_TEXT\":\"050-臺灣企銀 01062010505\"},{\"_ACN\":\"01062010602\",\"_BNKCOD\":\"050\",\"_AGREE\":\"1\",\"_DPGONAME\":\"\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"AGREE\\\":\\\"1\\\",\\\"ACN\\\":\\\"01062010602\\\",\\\"BNKCOD\\\":\\\"050\\\"}\",\"_TEXT\":\"050-臺灣企銀 01062010602\"},{\"_ACN\":\"01062010807\",\"_BNKCOD\":\"050\",\"_AGREE\":\"1\",\"_DPGONAME\":\"\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"AGREE\\\":\\\"1\\\",\\\"ACN\\\":\\\"01062010807\\\",\\\"BNKCOD\\\":\\\"050\\\"}\",\"_TEXT\":\"050-臺灣企銀 01062010807\"},{\"_ACN\":\"01062577807\",\"_BNKCOD\":\"050\",\"_AGREE\":\"1\",\"_DPGONAME\":\"\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"AGREE\\\":\\\"1\\\",\\\"ACN\\\":\\\"01062577807\\\",\\\"BNKCOD\\\":\\\"050\\\"}\",\"_TEXT\":\"050-臺灣企銀 01062577807\"},{\"_ACN\":\"\",\"_BNKCOD\":\"050\",\"_AGREE\":\"1\",\"_DPGONAME\":\"\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"AGREE\\\":\\\"1\\\",\\\"ACN\\\":\\\"\\\",\\\"BNKCOD\\\":\\\"050\\\"}\",\"_TEXT\":\"050-臺灣企銀\"},{\"_ACN\":\"05064600127\",\"_BNKCOD\":\"050\",\"_AGREE\":\"1\",\"_DPGONAME\":\"\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"AGREE\\\":\\\"1\\\",\\\"ACN\\\":\\\"05064600127\\\",\\\"BNKCOD\\\":\\\"050\\\"}\",\"_TEXT\":\"050-臺灣企銀 05064600127\"},{\"_ACN\":\"76062567895\",\"_BNKCOD\":\"050\",\"_AGREE\":\"1\",\"_DPGONAME\":\"\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"AGREE\\\":\\\"1\\\",\\\"ACN\\\":\\\"76062567895\\\",\\\"BNKCOD\\\":\\\"050\\\"}\",\"_TEXT\":\"050-臺灣企銀 76062567895\"},{\"_ACN\":\"84201234567890\",\"_BNKCOD\":\"005\",\"_AGREE\":\"1\",\"_DPGONAME\":\"\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"AGREE\\\":\\\"1\\\",\\\"ACN\\\":\\\"84201234567890\\\",\\\"BNKCOD\\\":\\\"005\\\"}\",\"_TEXT\":\"005-土地銀行 84201234567890\"},{\"_ACN\":\"99000000000000\",\"_BNKCOD\":\"050\",\"_AGREE\":\"0\",\"_DPGONAME\":\"050-臺灣企銀\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"BNKCOD\\\":\\\"050\\\",\\\"ACN\\\":\\\"99000000000000\\\",\\\"AGREE\\\":\\\"0\\\"}\",\"_TEXT\":\"050-臺灣企銀 99000000000000 050-臺灣企銀\"},{\"_ACN\":\"05052324156\",\"_BNKCOD\":\"050\",\"_AGREE\":\"0\",\"_DPGONAME\":\"test-fx\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"BNKCOD\\\":\\\"050\\\",\\\"ACN\\\":\\\"05052324156\\\",\\\"AGREE\\\":\\\"0\\\"}\",\"_TEXT\":\"050-臺灣企銀 05052324156 test-fx\"},{\"_ACN\":\"99123123123111\",\"_BNKCOD\":\"050\",\"_AGREE\":\"0\",\"_DPGONAME\":\"050-臺灣企銀\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"BNKCOD\\\":\\\"050\\\",\\\"ACN\\\":\\\"99123123123111\\\",\\\"AGREE\\\":\\\"0\\\"}\",\"_TEXT\":\"050-臺灣企銀 99123123123111 050-臺灣企銀\"}]}";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getAcnoList error >> {}",e);
		}
		return bs;
	}
	
	
	/**
	 * 取得轉出帳號
	 * @param cusidn
	 * @param type
	 * @return
	 */
	public BaseResult getOutACNO_List(String cusidn) {
		log.trace("cusidn>>{} ",cusidn);
		BaseResult bs = null;
		Map<String,Object> tmpData = null;
		List<String> acnoList = new ArrayList<String>();
		List<Map<String,String>> acnoList2 = null;
		LinkedHashMap<String,String> labelMap = null;
		try {
			//  CALL WS api
			bs =new  BaseResult();
			
//			N920的台幣轉帳的轉出帳號清單
			bs = N920_REST(cusidn,OUT_ACNO);
			//把REC轉出帳號轉成ajax需要的格式
			if(bs!=null && bs.getResult()) {
				tmpData = (Map) bs.getData();
				acnoList2 = (List<Map<String,String>>) tmpData.get("REC");
				labelMap = new LinkedHashMap<String,String>();

				for(Map<String,String> acMap :acnoList2) {
					String ACN = acMap.get("ACN");
					String attr = acMap.get("ATTRBUE");
					attr = chackOutacn(ACN, attr);
					labelMap.put(ACN, attr);
				}
				log.trace("labelMap data >>{}",labelMap);
				bs.setData(labelMap);

			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getOutACNO_List error >> {}",e);
		}
//		return retMap;
		return bs;
	}
	
	/**
	 * 取得銀行代號
	 * @param cusidn
	 * @param type
	 * @return
	 */
//	public BaseResult getDpBHNO_List(String cusidn) {
//		log.trace("cusidn>>{} ",cusidn);
//		BaseResult bs = null;
//		List<Map<String,Object>> data = new ArrayList<>();
////		Map<String,Object> tmpData = null;
//		List<String> bhnoList = new ArrayList<String>();
////		List<Map<String,String>> bhnoList2 = null;
//		try {
//			//  CALL WS api
//			bs =new  BaseResult();
//		//TODO 還未測試是否能用	
////			取出行庫代碼
//			List<ADMBANK> ADMBANK = daoservice.getBankList();
//			log.trace("ADMBANK>>>{}",ADMBANK);
////			bs.addData("REC", ADMBANK);
//			//把REC轉出帳號轉成ajax需要的格式
////			if(bs!=null && bs.getResult()) {
//			if(ADMBANK != null) {
//				for(ADMBANK each:ADMBANK) {
//					//將po轉成map
//					Map<String, Object> eachMap = ObjectConvertUtil.object2Map(each);
//					log.trace("eachMap>>{}",eachMap);
//					String BANKID = each.getADBANKID();
//					String BANKNAME = each.getADBANKNAME();
//					String DPBHNO = BANKID+"-"+BANKNAME;
//					bhnoList.add(DPBHNO);
////					data.add((Map<String, Object>) tmpData);
//				}
////				bs.addData("REC", bhnoList);
//				bs.setData(bhnoList);
//				bs.setMessage("0", "查詢成功");
//				bs.setResult(true);
////				bs.setData(data);
////				tmpData = (Map) bs.getData();
////				log.trace("tmpData>>{}",tmpData);
////				bhnoList2 = (List<Map<String,String>>) tmpData.get("REC");				
////				log.trace("bhnoList2>>{}",bhnoList2);
//				
////				for(Map<String,String> acMap :bhnoList2) {
////					String ACN = acMap.get("ACN");
////					bhnoList.add(ACN);
////				}
//				
////				bs.setData(bhnoList);
//
//			}
//
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			log.error("{}",e);
//		}
////		return retMap;
//		return bs;
//	}
	
	/**
	 * 
	 * @param 
	 * @return
	 */
	public BaseResult transfer_confirm( Map<String, String> reqParam ,String cusidn ) {
		log.trace(ESAPIUtil.vaildLog("transfer_confirm >> " + CodeUtil.toJson(reqParam)));
		BaseResult bs = null;
		Map<String, Object> callRow = null;
		String fgsvacno = "";
		String tmp_dpbhno ="";
		String dpbhno ="";
		try {
			//  CALL WS api
			bs =new  BaseResult();
			bs = getTxToken();
			if(bs.getResult()) {
				bs.addAllData(reqParam);
				reqParam.put("TXTOKEN",((Map<String,String>) bs.getData()).get("TXTOKEN"));
//				bs.setData(reqParam);
				//金額顯示修改
//				callRow = (Map) bs.getData();
//				callRow.put("AMOUNT",NumericUtil.fmtAmount(reqParam.get("AMOUNT"),2));
				//2020090去掉小數點兩位
				reqParam.put("AMOUNT",NumericUtil.fmtAmount(reqParam.get("AMOUNT"),0));
				
				fgsvacno = reqParam.get("FGSVACNO");
				log.trace(ESAPIUtil.vaildLog("fgsvacno >> " + fgsvacno));
				String transferInAccount = "";
				String agreeflag="";
				if("1".equals(fgsvacno)) {
					//約定轉入帳號
					String DPAGACNO = reqParam.get("DPAGACNO");
					Map<String,String> DPAGACNOMap = CodeUtil.fromJson(DPAGACNO,Map.class);
					log.trace(ESAPIUtil.vaildLog("DPAGACNO >> " + DPAGACNO));
					transferInAccount = DPAGACNOMap.get("ACN");
					agreeflag=DPAGACNOMap.get("AGREE");
				}else {
					tmp_dpbhno = reqParam.get("DPBHNO");
					dpbhno= reqParam.get("DPBHNO").split("-")[0];
					reqParam.put("DPBHNO", dpbhno);
					log.trace(ESAPIUtil.vaildLog("dpbhno >> " + dpbhno));
					//非約定轉入帳號
					String DPACNO = reqParam.get("DPACNO");
					log.trace(ESAPIUtil.vaildLog("DPACNO >> " + DPACNO));
					transferInAccount = reqParam.get("DPACNO");
					reqParam.put("DPAGACNO_TEXT" , tmp_dpbhno+" "+DPACNO) ;
//					非約定轉入帳號加入常用帳號
					if("1".equals(reqParam.get("ADDACN"))){
						bs = daoservice.add_common_acount(cusidn,tmp_dpbhno,dpbhno,fgsvacno,transferInAccount);
					}
					
				}
				String ACN = reqParam.get("TransferType").equals("NPD") ? reqParam.get("OUTACN_NPD") : reqParam.get("ACNO");
				String HASUSETIME = txnTwRecordDao.isTodayHasUse(cusidn, "N070", ACN, transferInAccount, NumericUtil.unfmtAmount(reqParam.get("AMOUNT")), DateUtil.getDate(""));
				reqParam.put("isHasUse", HASUSETIME.equals("") ? "N" : "Y");
				reqParam.put("HasUseTime", DateUtil.formatTime(HASUSETIME));
				log.trace(ESAPIUtil.vaildLog("transferInAccount >> " + transferInAccount));
				log.trace("errorMsg={}",((Map<String,String>)bs.getData()).get("errorMsg"));
				//將轉入帳號存起來
				reqParam.put("transferInAccount", transferInAccount);
				//約定帳號內有常用非約定帳號
				reqParam.put("agreeflag", agreeflag);
				bs.addAllData(reqParam);
			}
			bs.setResult(Boolean.TRUE);
			
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			bs.setResult(Boolean.FALSE);
			log.error("transfer_confirm error >> {}",e);
		}
		return bs;
	}
	
	
	
	/**
	 * 
	 * @param reqParam
	 * @param pin
	 * @param cusidn
	 * @param fgtxway
	 * @return
	 */
//	@Transactional(value="nnb_transactionManager")
	public BaseResult transfer_result( Map<String, String> reqParam ,String cusidn) {
		BaseResult bs = null;
		Map<String,String> newMap = null; 
		try {
			//遮罩環境變數
//			String TransferMask = SpringBeanFactory.getBean("TransferMask")!=null
//					? SpringBeanFactory.getBean("TransferMask").toString() : "N";
			log.trace(ESAPIUtil.vaildLog("reqParam >> " + CodeUtil.toJson(reqParam)));
			bs = new  BaseResult();
			newMap = new HashMap<String,String>();
			transfer_Result_Data_Pre_Processing(reqParam, newMap, cusidn);
			log.trace(ESAPIUtil.vaildLog("newMap >> " + CodeUtil.toJson(newMap)));
			
			bs = N070_REST(newMap);	
			log.trace("bs.getResult>>{}",bs.getResult());
			log.trace("bs.getData>>{}",bs.getData());
			if(bs!=null && bs.getResult()) {
				
				boolean diffOutAcn = false; // 轉出帳號上行下是否不相同
				boolean diffAmount = false; // 轉帳金額上行下是否不相同
				
				// 轉出帳號上下行比對
				String rqoutacn = newMap.get("ACN");
				String rsoutacn = ((Map<String, String>) bs.getData()).get("OUTACN");
				// 上下行轉出帳號都有值，並且不相同
				if (StrUtil.isNotEmpty(rqoutacn) && StrUtil.isNotEmpty(rsoutacn) && !rqoutacn.equals(rsoutacn) ) {
					diffOutAcn = true;
				}
				
				// 轉帳金額上下行比對
				String rqAmount = newMap.get("AMOUNT");
				String rsAmount = ((Map<String, String>) bs.getData()).get("AMOUNT");
				// 上下行轉帳金額都有值，並且不相同
				if (StrUtil.isNotEmpty(rqAmount) && StrUtil.isNotEmpty(rsAmount) && !rqAmount.equals(rsAmount) ) {
					diffAmount = true;
				}
				
				// 若是比對出有值且不相同則視為問題交易，回ZX98
				if (diffOutAcn || diffAmount) {
					bs.setResult(false);
					bs.setMsgCode("ZX98");
					bs.setMessage(daoservice.getMessageByMsgCode("ZX98"));
					bs.setNext("/NT/ACCT/TRANSFER/transfer");
					sendMail(cusidn,newMap,bs);
					return bs;
				}
				
				Map<String, Object> callRow = (Map) bs.getData();
				String fgtxdate =  newMap.get("FGTXDATE");
				log.trace("fgtxdate>>>{}",fgtxdate);
				bs.addData("FGTXDATE", fgtxdate);				
				String fgsvacno = reqParam.get("FGSVACNO");
				String transfertype = reqParam.get("TransferType");
				if("1".equals(fgtxdate)) {
//					log.info("TransferMask >>"+TransferMask);
//					if(TransferMask.equals("Y")) {
//						int acnlength=0;
//						String starstring="";
//						String tmp="";
//						//遮轉入
//						if(callRow.get("INTSACN")!=null) {
//							acnlength=callRow.get("INTSACN").toString().length();						
//							for(int i=0;i<acnlength-6;i++) {
//								starstring=starstring+"*";
//							}
//							tmp=callRow.get("INTSACN").toString().substring(0,3)+starstring+callRow.get("INTSACN").toString().substring(acnlength-3,acnlength);
//							callRow.put("INTSACN",tmp);
//						}
//						//遮轉出
//						if(callRow.get("OUTACN")!=null) {
//							acnlength=callRow.get("OUTACN").toString().length();					
//							starstring="";
//							for(int i=0;i<acnlength-6;i++) {
//								starstring=starstring+"*";
//							}
//							tmp=callRow.get("OUTACN").toString().substring(0,3)+starstring+callRow.get("OUTACN").toString().substring(acnlength-3,acnlength);
//							callRow.put("OUTACN",tmp);
//						}
//					}
					callRow.put("TOPMSG",i18n.getMsg("LB.Transfer_successful"));					
					//金額顯示處理			
					callRow.put("AMOUNT",NumericUtil.fmtAmount((String) callRow.get("AMOUNT"),2));				 	
					callRow.put("O_TOTBAL",NumericUtil.fmtAmount((String) callRow.get("O_TOTBAL"),2));				 	
					callRow.put("O_AVLBAL",NumericUtil.fmtAmount((String) callRow.get("O_AVLBAL"),2));
					if(callRow.get("I_TOTBAL") !=null && callRow.get("I_TOTBAL").toString().length()>0) {
						String I_TOTBAL = callRow.get("I_TOTBAL").toString().replace("+","");
						callRow.put("I_TOTBAL",NumericUtil.fmtAmount(I_TOTBAL,2));				 	
					}
					if(callRow.get("FEE") !=null && callRow.get("FEE").toString().length()>0) {
						String FEE = callRow.get("FEE").toString();
						callRow.put("FEE",NumericUtil.fmtAmount(FEE,2));				 	
					}
					if(callRow.get("I_AVLBAL") !=null && callRow.get("I_AVLBAL").toString().length()>0) {
						String I_AVLBAL = callRow.get("I_AVLBAL").toString().replace("+","");				
						callRow.put("I_AVLBAL",NumericUtil.fmtAmount(I_AVLBAL,2));
					}
					//備註
						if(StrUtil.isNotEmpty(reqParam.get("INPCST"))) {
							//附言
							callRow.put("INPCST",reqParam.get("INPCST"));
						}
						if(StrUtil.isNotEmpty(reqParam.get("CMTRMEMO"))) {
							//交易備註
							callRow.put("CMTRMEMO",reqParam.get("CMTRMEMO")) ;
						}
						if(StrUtil.isNotEmpty(reqParam.get("CMMAILMEMO"))) {
							callRow.put("CMMAILMEMO",reqParam.get("CMMAILMEMO")) ;
						}		
				}else {
					bs.addData("TOPMSG",i18n.getMsg("LB.W0284"));//預約成功
					//轉帳金額
					bs.addData("AMOUNT", NumericUtil.fmtAmount(reqParam.get("AMOUNT"),2));
					//轉出帳號
					
					if("PD".equals(transfertype)) {
						bs.addData("OUTACN", reqParam.get("ACNO"));
					}else {
						bs.addData("OUTACN", reqParam.get("OUTACN_NPD"));
					}
					//轉帳日期/日期區間
					String cmdate = reqParam.get("CMDATE");
					String cmsdate = reqParam.get("CMSDATE");
					String cmedate = reqParam.get("CMEDATE");
					String cmddStr = "";
					if("2".equals(fgtxdate)) {
						bs.addData("SCHDATE", cmdate);
					}else if("3".equals(fgtxdate)) {
						if (cmsdate.equals(cmedate)) {
							bs.addData("SCHDATE", cmsdate);
						} else {
							
//							cmddStr = I18nUtil.getMsg("LB.The",null,null)+" "+reqParam.get("CMDD")+I18nUtil.getMsg("LB.Day",null,null)+"， "+I18nUtil.getMsg("LB.Period_start_date",null,null)+cmsdate+I18nUtil.getMsg("LB.Period_end_date",null,null)+cmedate;
							cmddStr = I18nUtil.getMsg("LB.CMDD",null,new Object[]{reqParam.get("CMDD"),cmsdate,cmedate});
							bs.addData("SCHDATE", cmddStr);
							
//							bs.addData("SCHDATE", cmsdate + " ～ " + cmedate);
						}						
					}
					//附言
					bs.addData("INPCST",reqParam.get("INPCST"));
					//交易備註
					bs.addData("CMTRMEMO",reqParam.get("CMTRMEMO")) ;
					bs.addData("CMMAILMEMO",reqParam.get("CMMAILMEMO")) ;
					//轉入帳號
					String intsacn = "";
					if("1".equals(fgsvacno)) {
						intsacn = ((Map<String,String>) CodeUtil.fromJson(reqParam.get("DPAGACNO"), Map.class)).get("ACN"); 
					}else {
						intsacn =reqParam.get("DPACNO");
					}
//					bs.addData("INTSACN", reqParam.get("DPAGACNO"));
					bs.addData("INTSACN", intsacn);
				}
				/* 20190522 移到確認頁面
				//常用帳號寫入DB
				String ADDACN = reqParam.get("ADDACN");
				if("1".equals(ADDACN)){
					daoservice.add_common_acount(newMap);
				}
				*/

				//時間顯示
				bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("transfer_result error >> {}",e);
		}
		return bs;
	}
	
	
	/**
	 * 轉帳電文前置作業
	 * @param reqParam
	 * @param newMap
	 * @param cusidn
	 */
	public void transfer_Result_Data_Pre_Processing(Map<String,String> reqParam ,Map<String,String> newMap ,String cusidn){
		Map<String,String> tmp = null; 
		
		try {

			newMap.putAll(CodeUtil.objectCovert(Map.class, reqParam));
			//轉出帳號約定/非約定
			String transfertype = reqParam.get("TransferType");
			log.trace(ESAPIUtil.vaildLog("transfertype >> " + transfertype));
			if(transfertype.equals("PD")) {
				newMap.put("ACN", reqParam.get("ACNO"));				
			}else if(transfertype.equals("NPD")){
//				newMap.put("ACN", reqParam.get("ACNNO_SHOW"));				
				newMap.put("ACN", reqParam.get("OUTACN_NPD"));				
			}
			
			//非約定轉入
			String DPBHNO = reqParam.get("DPBHNO");
			log.trace(ESAPIUtil.vaildLog("dpbhno >> " + DPBHNO));
			if(DPBHNO != null){
				DPBHNO = DPBHNO.split("-")[0];
			}
			
			//約定非約定
			String fgsvacno = reqParam.get("FGSVACNO");
			newMap.put("FGSVACNO", fgsvacno);
			log.trace(ESAPIUtil.vaildLog("fgsvacno >> " + fgsvacno));
			if(! "1".equals(fgsvacno)) {
				//非約定轉入帳號
				newMap.put("DPBHNO", DPBHNO);
			}
			
			newMap.put("CUSIDN", cusidn);
			//交易方式02
			newMap.put("TYPE", "02");
			//金額
//			newMap.put("AMOUNT", NumericUtil.fmtAmount(reqParam.get("AMOUNT"),0));
			newMap.put("AMOUNT", NumericUtil.unfmtAmount(reqParam.get("AMOUNT")));
			//操作功能ID
			newMap.put("ADOPID", "N070");
			
			//加入常用帳號
			newMap.put("DPUSERID", cusidn);			
			newMap.put("DPGONAME", "");		
			newMap.put("DPTRIBANK", DPBHNO);			
			newMap.put("DPTRACNO", reqParam.get("DPACNO"));			
			
			//交易備註轉全形
			newMap.put("CMTRMEMO", halfToFull(newMap.get("CMTRMEMO")));	
			
			log.trace(ESAPIUtil.vaildLog("newMap >> " + CodeUtil.toJson(newMap)));

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			// TODO Auto-generated catch block
			//e.printStackTrace();
			log.error("transfer_Result_Data_Pre_Processing error >> {}",e);
		}
	}
	
	
	
	public void transfer_Result_Data_Pre_Processing_bk(Map<String,String> reqParam ,Map<String,String> newMap ,String cusidn){
		Map<String,String> tmp = null; 
		
		try {
			if(StrUtil.isNotEmpty(reqParam.get("INPCST"))) {
				reqParam.put("INPCST",reqParam.get("INPCST"));
			}
			if(StrUtil.isNotEmpty(reqParam.get("CMTRMEMO"))) {
				reqParam.put("CMTRMEMO",reqParam.get("CMTRMEMO")) ;
			}
			if(StrUtil.isNotEmpty(reqParam.get("CMMAILMEMO"))) {
				reqParam.put("CMMAILMEMO",reqParam.get("CMMAILMEMO")) ;
			}

			//單次/循環轉帳
			String fgtxdate = reqParam.get("FGTXDATE");
			if("1".equals(fgtxdate)) {
				newMap.put("FGTXDATE", reqParam.get("FGTXDATE"));							
			}else if("2".equals(fgtxdate)) {
				newMap.put("FGTXDATE", reqParam.get("FGTXDATE"));
				newMap.put("CMDATE", reqParam.get("CMDATE"));
			}else if("3".equals(fgtxdate)) {
				newMap.put("FGTXDATE", reqParam.get("FGTXDATE"));
				newMap.put("CMEDATE", reqParam.get("CMEDATE"));
				newMap.put("CMSDATE", reqParam.get("CMSDATE"));
			}
			
			//轉出帳號約定/非約定
			String transfertype = reqParam.get("TransferType");
			log.trace("transfertype>>{}",transfertype);
			if(transfertype.equals("PD")) {
				newMap.put("ACN", reqParam.get("ACNO"));				
			}else if(transfertype.equals("NPD")){
				newMap.put("ACN", reqParam.get("ACNNO_SHOW"));				
			}
			
			//非約定轉入
			String DPBHNO = reqParam.get("DPBHNO");
			if(DPBHNO != null){
				DPBHNO = DPBHNO.split("-")[0];
				log.trace("dpbhno>>{}",DPBHNO);				
			}
			
			//約定非約定
			String fgsvacno = reqParam.get("FGSVACNO");
			newMap.put("FGSVACNO", fgsvacno);
			log.trace("fgsvacno>>{}",fgsvacno);
			if("1".equals(fgsvacno)) {
				//約定轉入帳號
				newMap.put("DPAGACNO", reqParam.get("DPAGACNO"));
			}else {
				//非約定轉入帳號
				newMap.put("DPBHNO", DPBHNO);
				newMap.put("DPACNO", reqParam.get("DPACNO"));	
			}
			
			newMap.put("CUSIDN", cusidn);
			newMap.put("UID", cusidn);
			newMap.put("PINNEW", reqParam.get("PINNEW"));
			//交易方式02
			newMap.put("TYPE", "02");
			//交易機制
			newMap.put("FGTXWAY", reqParam.get("FGTXWAY"));
			//金額
			newMap.put("AMOUNT", NumericUtil.fmtAmount(reqParam.get("AMOUNT"),0));
		//晶片金融卡
			newMap.put("ISSUER", reqParam.get("ISSUER"));
			newMap.put("ACNNO", reqParam.get("ACNNO"));
			newMap.put("TRMID", reqParam.get("TRMID"));
			newMap.put("iSeqNo", reqParam.get("iSeqNo"));
			newMap.put("ICSEQ", reqParam.get("ICSEQ"));
			newMap.put("TAC", reqParam.get("TAC"));
		//預約
			//操作功能ID
			newMap.put("ADOPID", "N070");
			//發送Mail清單
			newMap.put("CMTRMAIL", reqParam.get("CMTRMAIL"));			
			//寫NB or MB 預設NB
//			newMap.put("LOGINTYPE", "");	
			
			
			
		//加入常用帳號
			newMap.put("DPUSERID", cusidn);			
			newMap.put("DPGONAME", "");		
			newMap.put("DPTRIBANK", DPBHNO);			
			newMap.put("DPTRACNO", reqParam.get("DPACNO"));			
			
			log.trace("newMap>>{}",newMap);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("transfer_Result_Data_Pre_Processing_bk error >> {}",e);
		}
	}
	
	
	
	
	/**
	 * 轉帳後電文資料後製
	 * @param bs
	 * @return
	 */
	public BaseResult transfer_Result_Data_Retouch(BaseResult bs) {
		Map<String ,Object> data = null;
		Map<String ,String> intsacn = null;
		Map<String ,String> inpcst = null;
		String bnkcod = "";
		String pscod = "";
		String dataintsacn = "";
		String datainpcst = "";
		try {
			if(bs != null) {
				data = (Map<String, Object>) bs.getData();
					dataintsacn = data.get("INTSACN").toString();
					intsacn = CodeUtil.fromJson(dataintsacn, Map.class) ;				
					log.trace("intsacn >>{} ",intsacn );
				
					bnkcod = intsacn.get("BNKCOD");
					log.trace("bnkcod {}",bnkcod);
				//12/18附言後製測試
					datainpcst = data.get("INPCST").toString();
//				if(datainpcst != null) {
					inpcst = CodeUtil.fromJson(datainpcst, Map.class) ;				
					pscod = inpcst.get("BNKCOD");
				
			}
			//TODO 先關閉DB相關
//			ADMBANK po = admbank_Dao.get(ADMBANK.class ,bnkcod);
//			ADMBANK potwo = admbank_Dao.get(ADMBANK.class ,pscod);
			
//			if(po==null) {
//				return bs;
//			}
//			else if(potwo==null) {
//				return bs;				
////			}
//			log.trace("getADBANKNAME {}",po.getADBANKNAME());
//			data.put("INTSACN", bnkcod+"-"+po.getADBANKNAME()+" "+intsacn.get("ACN"));
//			data.put("INPCST", pscod+"-"+potwo.getADBANKNAME()+" "+inpcst.get("ACN"));
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			//Avoid Information Exposure Through an Error Message
			log.error("transfer_Result_Data_Retouch error >> {}",e);
		}
				
		return bs;
		
	}

	public Map<String,Object> createIdgateData(BaseResult bs,Map<String,Object>IdgateSessionData){
		/* key            bs_key
		 * FGTXDATE 預約即時 FGTXDATE
		 * ACN      轉出帳號 
		 * TXNDATE  轉帳日期 
		 * TXTOKEN
		 *          轉入帳號  transferInAccount
		 *          轉帳金額 AMOUNT
		 * */
		Map<String,String> IdgatetransData=new HashMap<>();
		LinkedHashMap<String,String> txndataview = new LinkedHashMap<String,String>();
		Map<String,String> dataMap=(Map<String, String>) bs.getData();
		
		//交易名稱
		txndataview.put("交易名稱","臺幣轉帳");
		//交易類型+日期
		IdgatetransData.put("FGTXDATE", dataMap.get("FGTXDATE"));
		if(dataMap.get("FGTXDATE").equals("1")) {		
			//IdgatetransData.put("transferdate", dataMap.get("transferdate"));
			txndataview.put("交易類型","即時");
			txndataview.put("轉帳日期",dataMap.get("transferdate"));
		}else if(dataMap.get("FGTXDATE").equals("2")){
			IdgatetransData.put("CMDATE", dataMap.get("CMDATE"));
			txndataview.put("交易類型","預約");
			txndataview.put("轉帳日期",dataMap.get("CMDATE"));
		}else if(dataMap.get("FGTXDATE").equals("3")) {
			IdgatetransData.put("CMDD", dataMap.get("CMDD"));
			IdgatetransData.put("CMSDATE", dataMap.get("CMSDATE"));
			IdgatetransData.put("CMEDATE", dataMap.get("CMEDATE"));
			txndataview.put("交易類型","預約");
			txndataview.put("轉帳日期","每月"+dataMap.get("CMDD")+"日。起日:"+
					dataMap.get("CMSDATE")+"迄日"+dataMap.get("CMEDATE"));
		}
		//轉出帳號
		if(dataMap.get("TransferType").equals("NPD")) {
			IdgatetransData.put("ACN", dataMap.get("OUTACN_NPD"));
			txndataview.put("轉出帳號",dataMap.get("OUTACN_NPD"));
		}else {
			IdgatetransData.put("ACN", dataMap.get("ACNO"));
			txndataview.put("轉出帳號",dataMap.get("ACNO"));
		}
		//TXTOKEN
		IdgatetransData.put("TXTOKEN", dataMap.get("TXTOKEN"));
		//轉入帳號
		String fgsvacno = dataMap.get("FGSVACNO");
		log.trace("fgsvacno>>{}",fgsvacno);
		if("1".equals(fgsvacno)) {
			//約定轉入帳號
			IdgatetransData.put("DPAGACNO", dataMap.get("DPAGACNO"));
		}else {
			//非約定轉入帳號
			IdgatetransData.put("DPBHNO", dataMap.get("DPBHNO"));
			IdgatetransData.put("DPACNO", dataMap.get("DPACNO"));	
		}
		//IdgatetransData.put("DPAGACNO_TEXT", dataMap.get("DPAGACNO_TEXT"));
		txndataview.put("轉入帳號",dataMap.get("DPAGACNO_TEXT"));
		//轉帳金額
		IdgatetransData.put("AMOUNT", dataMap.get("AMOUNT"));
		txndataview.put("轉帳金額",dataMap.get("AMOUNT"));
		
		Map<String,Object>tmptxndataview=new HashMap<>();
		List tmp=new ArrayList();
		for (Map.Entry<String,String> viewdata : txndataview.entrySet()){
			Map<String,Object>tmpviewdata=new HashMap<>();
			tmpviewdata.put(viewdata.getKey(), viewdata.getValue());
			tmp.add(tmpviewdata);
		}
//		tmp.add(txndataview);
		tmptxndataview.put("txndataview", tmp);
		IdgateSessionData.put("txnData", IdgatetransData);
//		IdgateSessionData.put("txnDataView", CodeUtil.toJson(tmptxndataview));
		IdgateSessionData.put("txnDataView", tmptxndataview);
		log.debug("IDGATE TRANSdata >> "+CodeUtil.toJson(IdgateSessionData));
		return IdgateSessionData;
	}
	public Map<String,Object> customIdgateData(Map<String,Object>idgatedata,String adopid,Map<String, String> bsdata) {
		Map<String,Object> n070data=(Map<String, Object>) idgatedata.get("N070_IDGATE");
		Map<String, String> txnData=(Map<String, String>) n070data.get("txnData");
		log.trace("txnDatamap before: "+CodeUtil.toJson(txnData));
		//金額欄位調整
		String amount=NumericUtil.unfmtAmount(txnData.get("AMOUNT"));
		txnData.put("AMOUNT",amount);
		log.trace("IDGate transfer amount"+amount);
		//帳號
		if(bsdata.get("TransferType").equals("NPD")) {
			txnData.put("ACN", bsdata.get("OUTACN_NPD"));
		}else {
			txnData.put("ACN", bsdata.get("ACNO"));
		}
		log.trace("IDGate transfer txnDatamap: "+CodeUtil.toJson(txnData));
		n070data.put("txnData",txnData);
		idgatedata.put("N070_IDGATE", n070data);
		log.trace("N070_IDGATE :"+CodeUtil.toJson(idgatedata));
		
		return idgatedata;
	}
	/**
	 * 取得通訊錄email
	 * 
	 * @param cusidn
	 * @param pw
	 * @return
	 */
	public BaseResult getAddressbook(String cusidn) {
		log.trace("service.getAddressbook");
		BaseResult bs = null ;
		try 
		{
			bs = new  BaseResult();
			List<TXNADDRESSBOOK> AdBk = daoservice.findByDPUserID(cusidn);
			bs.setResult(true);
//			bs.addData("AdBk", AdBk);
			bs.addData("REC", AdBk);

		}
		catch (Exception e) 
		{
			log.error("service.getAddressbook error", e);
		}
		return  bs;
	}
	
	/**
	 *	查詢約定轉入帳號
	 * 
	 * @param cusidn
	 * @param pw
	 * @return
	 */
	public BaseResult getSearchDagacno(String cusidn) {
		log.trace("service.getSearchDagacno");
		BaseResult bs = null ;
		bs = new BaseResult();
		try 
		{
			bs = N215Q_REST(cusidn);
			if(bs != null) {
				int COUNT = 0;
				Map<String, Object> bsData = (Map) bs.getData();
				List<Map<String,String>> countdata = new ArrayList<>();
				Map<String,String> mapdata = new HashMap<String, String>();
				if(bsData != null) {
					ArrayList<Map<String, String>> row = (ArrayList<Map<String, String>>) bsData.get("REC");
					for (Map<String, String> map : row) {
						try {
							String acn = map.get("ACN");
							String bnkcod = map.get("BNKCOD");
							String bank = "";
							Locale currentLocale = LocaleContextHolder.getLocale();
				    		log.debug("admbanl.locale >> {}" , currentLocale);
				    		String locale = currentLocale.toString();
	
			                switch (locale) {
								case "en":
									bank = admbankdao.getBankEnName(bnkcod);
									break;
								case "zh_CN":
									bank = admbankdao.getBankChName(bnkcod);
									break;
								default:
									bank = admbankdao.getBankName(bnkcod);
									break;
								
			                }
							map.put("BNKCOD", bank);
							List<TXNTRACCSET> txntraccset =  txntraccsetdao.findByDPGONAME(cusidn,bnkcod,acn);
							if(txntraccset != null) {
								for(TXNTRACCSET each: txntraccset) {
	//								Map<String, Object> eachMap = ObjectConvertUtil.object2Map(each);
	//								row.add((Map<String, String>) eachMap.get("DPGONAME"));
									map.put("DPGONAME", each.getDPGONAME());
								}
							}
						}catch(Exception e){
							log.error(e.getMessage());
						}
					}
					COUNT = row.size();
					log.trace("bsData>>{}",bsData);
					log.trace(ESAPIUtil.vaildLog("row>>{}"+row));				
				}
				
				//總筆數				
				bs.addData("COUNT",COUNT);
				//處理空值顯示
				if(bs == null || COUNT == 0) {
					mapdata.put("DPSCHNO"," ");
					mapdata.put("DPSCHTXDATE"," ");
					mapdata.put("DPWDAC"," ");
					mapdata.put("DPSVBH"," ");
					mapdata.put("DPSVAC"," ");
					mapdata.put("DPTXAMT"," ");
					mapdata.put("DPEFEE"," ");
					mapdata.put("DPSTANNO"," ");
					mapdata.put("DPSEQ"," ");
					mapdata.put("DPTXMEMO"," ");
					mapdata.put("DPTXSTATUS"," ");
					countdata.add(mapdata);							
					bs.addData("REC", countdata);
				}
				log.trace("bsData_count>>{}",bs.getData());	
				bs.setResult(true);
			}
			
		}
		catch (Exception e) 
		{
			log.error("service.getSearchDagacno error", e);
		}
		return  bs;
	}
	
	public String chackOutacn(String acn, String attr) { 
		String acnClass = acn.substring(3, 5);
		if(attr.equals("01")) {
			if(acnClass.equals("01") || acnClass.equals("05")) {
				//支票存款
				acn = acn + " 支票存款";
			}
			else {
				acn = acn;
			}
		}
		else if(attr.equals("02")) {
			if(acnClass.equals("12")) {
				//活期存款
				acn = acn + " 活期存款";
			}
			else if(acnClass.equals("60") || acnClass.equals("62") || acnClass.equals("64")) {
				//活期儲蓄存款
				acn = acn + " 活期儲蓄存款";
			}
			else if(acnClass.equals("61")) {
				//活期存款(儲蓄部)
				acn = acn + " 活期存款(儲蓄部)";
			}
			else {
				acn = acn;
			}
		}
		else if(attr.equals("03")) {
			if(acnClass.equals("06") || acnClass.equals("07") || acnClass.equals("08")) {
				//特殊活儲
				acn = acn + " 特殊活儲";
			}
			else if(acnClass.equals("10")) {
				//同業存款（活存）
				acn = acn + " 同業存款（活存）";
			}
			else if(acnClass.equals("11") || acnClass.equals("14") || acnClass.equals("15") || acnClass.equals("16") || acnClass.equals("36") || acnClass.equals("37") || acnClass.equals("38") || acnClass.equals("39")) {
				//活期存款（證券戶）
				acn = acn + " 活期存款（證券戶）";
			}
			else if(acnClass.equals("13")) {
				//行員存款
				acn = acn + " 行員存款";
			}
			else if(acnClass.equals("17") || acnClass.equals("18") || acnClass.equals("19") || acnClass.equals("20") || acnClass.equals("46") || acnClass.equals("47") || acnClass.equals("48") || acnClass.equals("49")) {
				//活期儲蓄存款（證券戶）
				acn = acn + " 活期儲蓄存款（證券戶）";
			}
			else if(acnClass.equals("63")) {
				//行員存款(儲蓄部)
				acn = acn + " 行員存款(儲蓄部)";
			}
			else if(acnClass.equals("65") || acnClass.equals("66") || acnClass.equals("67") || acnClass.equals("68") || acnClass.equals("69")) {
				//台電職工存款
				acn = acn + " 台電職工存款";
			}
			else {
				acn = acn;
			}
		}
		else if(attr.equals("04")) {
			if(acnClass.equals("50")) {
				//外匯活期存款
				acn = acn + " 外匯活期存款";
			}
			else if(acnClass.equals("52")) {
				//外匯綜合存款
				acn = acn + " 外匯綜合存款";
			}
			else if(acnClass.equals("54")) {
				//外匯備償專戶
				acn = acn + " 外匯備償專戶";
			}
			else {
				acn = acn;
			}
		}
		else if(attr.equals("05")) {
			if(acnClass.equals("51")) {
				//外匯定期存款
				acn = acn + " 外匯定期存款";
			}
			else {
				acn = acn;
			}
		}
		else if(attr.equals("06")) {
			if(acnClass.equals("21") || acnClass.equals("22") || acnClass.equals("23") || acnClass.equals("24") || acnClass.equals("25") || acnClass.equals("26") || acnClass.equals("27") || acnClass.equals("28")
					|| acnClass.equals("29") || acnClass.equals("71") || acnClass.equals("72") || acnClass.equals("73") || acnClass.equals("74") || acnClass.equals("75") || acnClass.equals("76") 
					|| acnClass.equals("77") || acnClass.equals("78") || acnClass.equals("79") || acnClass.equals("80") || acnClass.equals("81")) {
				//定期存款
				acn = acn + " 定期存款";
			}
			else {
				acn = acn;
			}
		}
		else if(attr.equals("07")) {
			if(acnClass.equals("12") || acnClass.equals("60") || acnClass.equals("61") || acnClass.equals("62") || acnClass.equals("64")) {
				//整合型智慧理財帳戶
				acn = acn + " 整合型智慧理財帳戶";
			}
			else {
				acn = acn;
			}
		}
		else if(attr.equals("08")) {
			if(acnClass.equals("97")) {
				//中央登錄債券帳號
				acn = acn + " 中央登錄債券帳號";
			}
			else {
				acn = acn;
			}
		}
		else if(attr.equals("09")) {
			if(acnClass.equals("98")) {
				//黃金存摺帳號
				acn = acn + " 黃金存摺帳號";
			}
			else {
				acn = acn;
			}
		}
		else {
			acn = acn;
		}
		
		return acn;
	}
	
	public void sendMail(String cusidn,Map<String,String>reqParam,BaseResult bs) {
		SYSPARAMDATA po = null; 
		po = sysParamDataDao.getByPK("NBSYS");
		String adapmail = po.getADAPMAIL();
		String mail_subject_env = SpringBeanFactory.getBean("MAIL_SUBJECT_ENV");
		String subject = "";
		String content = "";
		List<String> Receivers = Arrays.asList(adapmail.split(",|;"));
		StringBuffer sb = new StringBuffer();
		sb.append("身分證字號:"+cusidn+"<br>");
		sb.append("時間:"+DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss")+"<br>");
		sb.append("上行資料: "+CodeUtil.toJson(reqParam)+"<br>");
		sb.append("<br>");
		sb.append("========================================================================================");
		sb.append("<br>");
		sb.append("下行資料: "+CodeUtil.toJson(bs.getData())+"<br>");
		sb.append("<br>");
		switch (mail_subject_env) {
			case "D":
			subject = "[開發環境] 臺灣企銀 新世代網路銀行 N071結果異常";
				break;
			case "T":
				subject = "[測試環境] 臺灣企銀 新世代網路銀行 N071結果異常";
				break;
			case "P":
				subject = "臺灣企銀 新世代網路銀行 N071結果異常";
				break;
		}
		content = sb.toString();
		BaseResult bs2 = SendAdMail_REST(subject, content, Receivers,null);
		log.info(ESAPIUtil.vaildLog("mail result : "+ bs2.getData()));
		
	}
	//半形英數字轉全形
	public String halfToFull(String str) {
		String result=str;
		for(char c:result.toCharArray()){
			if((int)c == 32)result = result.replace(c, (char)12288);//空白需特別判斷
			else if((int)c >= 33 && (int)c <= 126) {
				result = result.replace(c, (char)(((int)c)+65248));
			}
		}
		return result;
	}
	//全形英數字轉半形
	public String fullToHalf(String str) {
		String result=str;
		for(char c:result.toCharArray()){
			if((int)c == 12288)result = result.replace(c, (char)32);//空白需特別判斷
			else if((int)c >= 65281 && (int)c <= 65374){
				result = result.replace(c, (char)(((int)c)-65248));
			}
		}	
		return result;
	}	

}