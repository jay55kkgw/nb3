package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N093_01_REST_RS extends BaseRestBean implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 558669080128600478L;
	
	private String CMQTIME;
	private String SYNC;//Sync.Check Item 
	private String TRNDATE;//日期YYYMMDD 
	private String TRNTIME;//時間HHMMSS 
	private String TRNSRC;//交易來源 
	private String TRNTYP;//交易種類 
	private String TRNCOD;//交易註記碼 
	
	private String TRNBDT;//交易日期 
	private String ACN;//黃金存摺帳號 
	private String CUSIDN;//客戶統一編號 
	private String SVACN;//台幣存款帳號 
	private String DATE_06;//06日是否申購或變更 
	private String AMT06_SIGN;//06日投資金額SIGN 
	private String AMT06;//06日投資金額 
	private String FLAG_06;//06日狀態變更 
	private String CHA_06;//06日約定可否變更 
	private String DATE_16;//16日是否申購或變更 
	private String AMT16_SIGN;//16日投資金額SIGN 
	private String AMT16;//16日投資金額 
	private String FLAG_16;//16日狀態變更 
	private String CHA_16;//16日約定可否變更 
	private String DATE_26;//26日是否申購或變更 
	private String AMT26_SIGN;//26日投資金額SIGN 
	private String AMT26;//26日投資金額 
	private String FLAG_26;//26日狀態變更 
	private String CHA_26;//26日約定可否變更 
	
	
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getSYNC() {
		return SYNC;
	}
	public void setSYNC(String sYNC) {
		SYNC = sYNC;
	}
	public String getTRNDATE() {
		return TRNDATE;
	}
	public void setTRNDATE(String tRNDATE) {
		TRNDATE = tRNDATE;
	}
	public String getTRNTIME() {
		return TRNTIME;
	}
	public void setTRNTIME(String tRNTIME) {
		TRNTIME = tRNTIME;
	}
	public String getTRNSRC() {
		return TRNSRC;
	}
	public void setTRNSRC(String tRNSRC) {
		TRNSRC = tRNSRC;
	}
	public String getTRNTYP() {
		return TRNTYP;
	}
	public void setTRNTYP(String tRNTYP) {
		TRNTYP = tRNTYP;
	}
	public String getTRNCOD() {
		return TRNCOD;
	}
	public void setTRNCOD(String tRNCOD) {
		TRNCOD = tRNCOD;
	}
	public String getTRNBDT() {
		return TRNBDT;
	}
	public void setTRNBDT(String tRNBDT) {
		TRNBDT = tRNBDT;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getSVACN() {
		return SVACN;
	}
	public void setSVACN(String sVACN) {
		SVACN = sVACN;
	}
	public String getDATE_06() {
		return DATE_06;
	}
	public void setDATE_06(String dATE_06) {
		DATE_06 = dATE_06;
	}
	public String getAMT06_SIGN() {
		return AMT06_SIGN;
	}
	public void setAMT06_SIGN(String aMT06_SIGN) {
		AMT06_SIGN = aMT06_SIGN;
	}
	public String getAMT06() {
		return AMT06;
	}
	public void setAMT06(String aMT06) {
		AMT06 = aMT06;
	}
	public String getFLAG_06() {
		return FLAG_06;
	}
	public void setFLAG_06(String fLAG_06) {
		FLAG_06 = fLAG_06;
	}
	public String getCHA_06() {
		return CHA_06;
	}
	public void setCHA_06(String cHA_06) {
		CHA_06 = cHA_06;
	}
	public String getDATE_16() {
		return DATE_16;
	}
	public void setDATE_16(String dATE_16) {
		DATE_16 = dATE_16;
	}
	public String getAMT16_SIGN() {
		return AMT16_SIGN;
	}
	public void setAMT16_SIGN(String aMT16_SIGN) {
		AMT16_SIGN = aMT16_SIGN;
	}
	public String getAMT16() {
		return AMT16;
	}
	public void setAMT16(String aMT16) {
		AMT16 = aMT16;
	}
	public String getFLAG_16() {
		return FLAG_16;
	}
	public void setFLAG_16(String fLAG_16) {
		FLAG_16 = fLAG_16;
	}
	public String getCHA_16() {
		return CHA_16;
	}
	public void setCHA_16(String cHA_16) {
		CHA_16 = cHA_16;
	}
	public String getDATE_26() {
		return DATE_26;
	}
	public void setDATE_26(String dATE_26) {
		DATE_26 = dATE_26;
	}
	public String getAMT26_SIGN() {
		return AMT26_SIGN;
	}
	public void setAMT26_SIGN(String aMT26_SIGN) {
		AMT26_SIGN = aMT26_SIGN;
	}
	public String getAMT26() {
		return AMT26;
	}
	public void setAMT26(String aMT26) {
		AMT26 = aMT26;
	}
	public String getFLAG_26() {
		return FLAG_26;
	}
	public void setFLAG_26(String fLAG_26) {
		FLAG_26 = fLAG_26;
	}
	public String getCHA_26() {
		return CHA_26;
	}
	public void setCHA_26(String cHA_26) {
		CHA_26 = cHA_26;
	}
	
	
}
