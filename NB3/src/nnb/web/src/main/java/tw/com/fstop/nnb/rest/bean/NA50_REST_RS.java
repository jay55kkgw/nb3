package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class NA50_REST_RS  extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1168565978102926988L;
	
	private String BRHNUM;//領取黃金存摺之開戶行
	private String GDACN;//黃金存摺帳戶
	private String AMT_FEE;//開戶手續費
	private String LOTCNT;//累積抽獎點數
	
	public String getBRHNUM() {
		return BRHNUM;
	}
	public void setBRHNUM(String bRHNUM) {
		BRHNUM = bRHNUM;
	}
	public String getGDACN() {
		return GDACN;
	}
	public void setGDACN(String gDACN) {
		GDACN = gDACN;
	}
	public String getAMT_FEE() {
		return AMT_FEE;
	}
	public void setAMT_FEE(String aMT_FEE) {
		AMT_FEE = aMT_FEE;
	}
	public String getLOTCNT() {
		return LOTCNT;
	}
	public void setLOTCNT(String lOTCNT) {
		LOTCNT = lOTCNT;
	}

	
}
