package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N420_REST_RQ extends BaseRestBean_TW implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2689928567967565581L;
	String	ACN;
	String	CUSIDN;
	String	OKOVNEXT;
	

	// 是否跳過TXNLOG
	private String ADOPID;
	
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getOKOVNEXT() {
		return OKOVNEXT;
	}
	public void setOKOVNEXT(String oKOVNEXT) {
		OKOVNEXT = oKOVNEXT;
	}
	
}
