package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class P002_REST_RQ extends BaseRestBean_OLA implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3953085518651087946L;

	private String TRANNAME;//TRANNAME
	private String TxnCode;//交易代碼
	private String TxnDateTime;//交易時間
	private String SerialNo;//流水號
	private String CustomerId;//客戶ID
	private String Source;//申請來源別
	private String Count;//申請筆數
	private String ZoneCode;//縣市別
	private String CarId;//牌照號碼
	private String StartDate;//啟用日期
	private String FGTXWAY;
	private String PINNEW;
	private String CUSIDN;
	private String ADOPID;
	private String Account;
	
	public String getAccount() {
		return Account;
	}
	public void setAccount(String account) {
		Account = account;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getTRANNAME() {
		return TRANNAME;
	}
	public void setTRANNAME(String tRANNAME) {
		TRANNAME = tRANNAME;
	}
	public String getTxnCode() {
		return TxnCode;
	}
	public void setTxnCode(String txnCode) {
		TxnCode = txnCode;
	}
	public String getTxnDateTime() {
		return TxnDateTime;
	}
	public void setTxnDateTime(String txnDateTime) {
		TxnDateTime = txnDateTime;
	}
	public String getSerialNo() {
		return SerialNo;
	}
	public void setSerialNo(String serialNo) {
		SerialNo = serialNo;
	}
	public String getCustomerId() {
		return CustomerId;
	}
	public void setCustomerId(String customerId) {
		CustomerId = customerId;
	}
	public String getSource() {
		return Source;
	}
	public void setSource(String source) {
		Source = source;
	}
	public String getCount() {
		return Count;
	}
	public void setCount(String count) {
		Count = count;
	}
	public String getZoneCode() {
		return ZoneCode;
	}
	public void setZoneCode(String zoneCode) {
		ZoneCode = zoneCode;
	}
	public String getCarId() {
		return CarId;
	}
	public void setCarId(String carId) {
		CarId = carId;
	}
	public String getStartDate() {
		return StartDate;
	}
	public void setStartDate(String startDate) {
		StartDate = startDate;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getPINNEW() {
		return PINNEW;
	}
	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}
}
