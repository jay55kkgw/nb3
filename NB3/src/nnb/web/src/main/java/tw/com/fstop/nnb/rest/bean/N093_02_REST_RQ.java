package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N093_02_REST_RQ extends BaseRestBean_GOLD implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5241033511079173004L;
	
	private String DATE;
	private String TIME;
	private String CERTACN;
	private String XMLCA;
	private String XMLCN;
	private String PINNEW;
	private String CUSIDN;
	private String TRNCOD;
	private String TRNBDT;
	private String ACN;
	private String CUSIDN1;
	private String SVACN;
	private String DATE_06;
	private String AMT_06_SIGN;
	private String AMT_06;
	private String FLAG_06;
	private String CHA_06;
	private String DATE_16;
	private String AMT_16_SIGN;
	private String AMT_16;
	private String FLAG_16;
	private String CHA_16;
	private String DATE_26;
	private String AMT_26_SIGN;
	private String AMT_26;
	private String FLAG_26;
	private String CHA_26;
	private String UID;
	private String FGTXWAY;
	private String ADOPID = "N09302";
	private String DPMYEMAIL;
	
	private String AMT_06_N;
	private String AMT_16_N;
	private String AMT_26_N;
	private String PAYSTATUS_06;
	private String PAYSTATUS_16;
	private String PAYSTATUS_26;
	
	private	String	ACNNO;
	private	String	iSeqNo;
	private	String	TAC;
	private	String	ISSUER;
	private	String	TRMID;
	private	String	pkcs7Sign;
	
	//IDGATE
	private String sessionID;
	private String idgateID;
	private String txnID;
	
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getCERTACN() {
		return CERTACN;
	}
	public void setCERTACN(String cERTACN) {
		CERTACN = cERTACN;
	}
	public String getXMLCA() {
		return XMLCA;
	}
	public void setXMLCA(String xMLCA) {
		XMLCA = xMLCA;
	}
	public String getXMLCN() {
		return XMLCN;
	}
	public void setXMLCN(String xMLCN) {
		XMLCN = xMLCN;
	}
	public String getPINNEW() {
		return PINNEW;
	}
	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getTRNCOD() {
		return TRNCOD;
	}
	public void setTRNCOD(String tRNCOD) {
		TRNCOD = tRNCOD;
	}
	public String getTRNBDT() {
		return TRNBDT;
	}
	public void setTRNBDT(String tRNBDT) {
		TRNBDT = tRNBDT;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getCUSIDN1() {
		return CUSIDN1;
	}
	public void setCUSIDN1(String cUSIDN1) {
		CUSIDN1 = cUSIDN1;
	}
	public String getSVACN() {
		return SVACN;
	}
	public void setSVACN(String sVACN) {
		SVACN = sVACN;
	}
	public String getDATE_06() {
		return DATE_06;
	}
	public void setDATE_06(String dATE_06) {
		DATE_06 = dATE_06;
	}
	public String getAMT_06_SIGN() {
		return AMT_06_SIGN;
	}
	public void setAMT_06_SIGN(String aMT_06_SIGN) {
		AMT_06_SIGN = aMT_06_SIGN;
	}
	public String getAMT_06() {
		return AMT_06;
	}
	public void setAMT_06(String aMT_06) {
		AMT_06 = aMT_06;
	}
	public String getFLAG_06() {
		return FLAG_06;
	}
	public void setFLAG_06(String fLAG_06) {
		FLAG_06 = fLAG_06;
	}
	public String getCHA_06() {
		return CHA_06;
	}
	public void setCHA_06(String cHA_06) {
		CHA_06 = cHA_06;
	}
	public String getDATE_16() {
		return DATE_16;
	}
	public void setDATE_16(String dATE_16) {
		DATE_16 = dATE_16;
	}
	public String getAMT_16_SIGN() {
		return AMT_16_SIGN;
	}
	public void setAMT_16_SIGN(String aMT_16_SIGN) {
		AMT_16_SIGN = aMT_16_SIGN;
	}
	public String getAMT_16() {
		return AMT_16;
	}
	public void setAMT_16(String aMT_16) {
		AMT_16 = aMT_16;
	}
	public String getFLAG_16() {
		return FLAG_16;
	}
	public void setFLAG_16(String fLAG_16) {
		FLAG_16 = fLAG_16;
	}
	public String getCHA_16() {
		return CHA_16;
	}
	public void setCHA_16(String cHA_16) {
		CHA_16 = cHA_16;
	}
	public String getDATE_26() {
		return DATE_26;
	}
	public void setDATE_26(String dATE_26) {
		DATE_26 = dATE_26;
	}
	public String getAMT_26_SIGN() {
		return AMT_26_SIGN;
	}
	public void setAMT_26_SIGN(String aMT_26_SIGN) {
		AMT_26_SIGN = aMT_26_SIGN;
	}
	public String getAMT_26() {
		return AMT_26;
	}
	public void setAMT_26(String aMT_26) {
		AMT_26 = aMT_26;
	}
	public String getFLAG_26() {
		return FLAG_26;
	}
	public void setFLAG_26(String fLAG_26) {
		FLAG_26 = fLAG_26;
	}
	public String getCHA_26() {
		return CHA_26;
	}
	public void setCHA_26(String cHA_26) {
		CHA_26 = cHA_26;
	}
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getDPMYEMAIL() {
		return DPMYEMAIL;
	}
	public void setDPMYEMAIL(String dPMYEMAIL) {
		DPMYEMAIL = dPMYEMAIL;
	}
	public String getAMT_06_N() {
		return AMT_06_N;
	}
	public void setAMT_06_N(String aMT_06_N) {
		AMT_06_N = aMT_06_N;
	}
	public String getAMT_16_N() {
		return AMT_16_N;
	}
	public void setAMT_16_N(String aMT_16_N) {
		AMT_16_N = aMT_16_N;
	}
	public String getAMT_26_N() {
		return AMT_26_N;
	}
	public void setAMT_26_N(String aMT_26_N) {
		AMT_26_N = aMT_26_N;
	}
	public String getPAYSTATUS_06() {
		return PAYSTATUS_06;
	}
	public void setPAYSTATUS_06(String pAYSTATUS_06) {
		PAYSTATUS_06 = pAYSTATUS_06;
	}
	public String getPAYSTATUS_16() {
		return PAYSTATUS_16;
	}
	public void setPAYSTATUS_16(String pAYSTATUS_16) {
		PAYSTATUS_16 = pAYSTATUS_16;
	}
	public String getPAYSTATUS_26() {
		return PAYSTATUS_26;
	}
	public void setPAYSTATUS_26(String pAYSTATUS_26) {
		PAYSTATUS_26 = pAYSTATUS_26;
	}
	public String getACNNO() {
		return ACNNO;
	}
	public void setACNNO(String aCNNO) {
		ACNNO = aCNNO;
	}
	public String getiSeqNo() {
		return iSeqNo;
	}
	public void setiSeqNo(String iSeqNo) {
		this.iSeqNo = iSeqNo;
	}
	public String getTAC() {
		return TAC;
	}
	public void setTAC(String tAC) {
		TAC = tAC;
	}
	public String getISSUER() {
		return ISSUER;
	}
	public void setISSUER(String iSSUER) {
		ISSUER = iSSUER;
	}
	public String getTRMID() {
		return TRMID;
	}
	public void setTRMID(String tRMID) {
		TRMID = tRMID;
	}
	public String getPkcs7Sign() {
		return pkcs7Sign;
	}
	public void setPkcs7Sign(String pkcs7Sign) {
		this.pkcs7Sign = pkcs7Sign;
	}
	public String getSessionID() {
		return sessionID;
	}
	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}
	public String getIdgateID() {
		return idgateID;
	}
	public void setIdgateID(String idgateID) {
		this.idgateID = idgateID;
	}
	public String getTxnID() {
		return txnID;
	}
	public void setTxnID(String txnID) {
		this.txnID = txnID;
	}
}
