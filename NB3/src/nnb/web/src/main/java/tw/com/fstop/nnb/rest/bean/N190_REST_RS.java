package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N190_REST_RS  extends BaseRestBean implements Serializable{

	private static final long serialVersionUID = -3629961584233878352L;
	/**
	 * 
	 */
	private String TOTAL_ODFBAL;//
	private String TOTAL_TSFBAL;//可用餘額(公克)
	private String TOTAL_GDBAL;//帳戶餘額(公克)
	private String CMQTIME;
	private String CMRECNUM;
	LinkedList<N190_REST_RSDATA> REC;
	private String TOTAL_MKBAL;//參考市值(元)
	public String getTOTAL_MKBAL() {
		return TOTAL_MKBAL;
	}
	public void setTOTAL_MKBAL(String tOTAL_MKBAL) {
		TOTAL_MKBAL = tOTAL_MKBAL;
	}
	public String getTOTAL_ODFBAL() {
		return TOTAL_ODFBAL;
	}
	public void setTOTAL_ODFBAL(String tOTAL_ODFBAL) {
		TOTAL_ODFBAL = tOTAL_ODFBAL;
	}
	public String getTOTAL_TSFBAL() {
		return TOTAL_TSFBAL;
	}
	public void setTOTAL_TSFBAL(String tOTAL_TSFBAL) {
		TOTAL_TSFBAL = tOTAL_TSFBAL;
	}
	public String getTOTAL_GDBAL() {
		return TOTAL_GDBAL;
	}
	public void setTOTAL_GDBAL(String tOTAL_GDBAL) {
		TOTAL_GDBAL = tOTAL_GDBAL;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getCMRECNUM() {
		return CMRECNUM;
	}
	public void setCMRECNUM(String cMRECNUM) {
		CMRECNUM = cMRECNUM;
	}
	public LinkedList<N190_REST_RSDATA> getREC() {
		return REC;
	}
	public void setREC(LinkedList<N190_REST_RSDATA> rEC) {
		REC = rEC;
	}

}
