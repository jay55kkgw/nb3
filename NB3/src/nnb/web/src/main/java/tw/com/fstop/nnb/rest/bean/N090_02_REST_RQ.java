package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N090_02_REST_RQ extends BaseRestBean_GOLD implements Serializable {
	/**
	 * 黃金回售RQ
	 */
	private static final long serialVersionUID = 1392497182026606436L;
	private String ADOPID = "N09002";
	private String PINNEW;
	private String CUSIDN;
	private String TRNSRC;
	private String TRNTYP;
	private String TRNCOD;
	private String TRNBDT;
	private String ACN;
	private String SVACN;
	private String TRNGD;
	private String TSFBAL_H;
	private String PRICE;
	private String SELLFLAG;
	private String TRNAMT_SIGN;
	private String TRNAMT;
	private String FEEAMT1_SIGN;
	private String FEEAMT1;
	private String FEEAMT2_SIGN;
	private String FEEAMT2;
	private String TRNAMT2_SIGN;
	private String TRNAMT2;
	private String UID;
	private String FGTXWAY;
	private String DPMYEMAIL;
	private String pkcs7Sign;
	private String jsondc;
	//IDGATE
	private String sessionID;
	private String idgateID;
	private String txnID;
	
	public String getSessionID() {
		return sessionID;
	}
	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}
	public String getIdgateID() {
		return idgateID;
	}
	public void setIdgateID(String idgateID) {
		this.idgateID = idgateID;
	}
	public String getTxnID() {
		return txnID;
	}
	public void setTxnID(String txnID) {
		this.txnID = txnID;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getPINNEW() {
		return PINNEW;
	}
	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getTRNSRC() {
		return TRNSRC;
	}
	public void setTRNSRC(String tRNSRC) {
		TRNSRC = tRNSRC;
	}
	public String getTRNTYP() {
		return TRNTYP;
	}
	public void setTRNTYP(String tRNTYP) {
		TRNTYP = tRNTYP;
	}
	public String getTRNCOD() {
		return TRNCOD;
	}
	public void setTRNCOD(String tRNCOD) {
		TRNCOD = tRNCOD;
	}
	public String getTRNBDT() {
		return TRNBDT;
	}
	public void setTRNBDT(String tRNBDT) {
		TRNBDT = tRNBDT;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getSVACN() {
		return SVACN;
	}
	public void setSVACN(String sVACN) {
		SVACN = sVACN;
	}
	public String getTRNGD() {
		return TRNGD;
	}
	public void setTRNGD(String tRNGD) {
		TRNGD = tRNGD;
	}
	public String getTSFBAL_H() {
		return TSFBAL_H;
	}
	public void setTSFBAL_H(String tSFBAL_H) {
		TSFBAL_H = tSFBAL_H;
	}
	public String getPRICE() {
		return PRICE;
	}
	public void setPRICE(String pRICE) {
		PRICE = pRICE;
	}
	public String getSELLFLAG() {
		return SELLFLAG;
	}
	public void setSELLFLAG(String sELLFLAG) {
		SELLFLAG = sELLFLAG;
	}
	public String getTRNAMT_SIGN() {
		return TRNAMT_SIGN;
	}
	public void setTRNAMT_SIGN(String tRNAMT_SIGN) {
		TRNAMT_SIGN = tRNAMT_SIGN;
	}
	public String getTRNAMT() {
		return TRNAMT;
	}
	public void setTRNAMT(String tRNAMT) {
		TRNAMT = tRNAMT;
	}
	public String getFEEAMT1_SIGN() {
		return FEEAMT1_SIGN;
	}
	public void setFEEAMT1_SIGN(String fEEAMT1_SIGN) {
		FEEAMT1_SIGN = fEEAMT1_SIGN;
	}
	public String getFEEAMT1() {
		return FEEAMT1;
	}
	public void setFEEAMT1(String fEEAMT1) {
		FEEAMT1 = fEEAMT1;
	}
	public String getFEEAMT2_SIGN() {
		return FEEAMT2_SIGN;
	}
	public void setFEEAMT2_SIGN(String fEEAMT2_SIGN) {
		FEEAMT2_SIGN = fEEAMT2_SIGN;
	}
	public String getFEEAMT2() {
		return FEEAMT2;
	}
	public void setFEEAMT2(String fEEAMT2) {
		FEEAMT2 = fEEAMT2;
	}
	public String getTRNAMT2_SIGN() {
		return TRNAMT2_SIGN;
	}
	public void setTRNAMT2_SIGN(String tRNAMT2_SIGN) {
		TRNAMT2_SIGN = tRNAMT2_SIGN;
	}
	public String getTRNAMT2() {
		return TRNAMT2;
	}
	public void setTRNAMT2(String tRNAMT2) {
		TRNAMT2 = tRNAMT2;
	}
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getDPMYEMAIL() {
		return DPMYEMAIL;
	}
	public void setDPMYEMAIL(String dPMYEMAIL) {
		DPMYEMAIL = dPMYEMAIL;
	}
	public String getPkcs7Sign() {
		return pkcs7Sign;
	}
	public void setPkcs7Sign(String pkcs7Sign) {
		this.pkcs7Sign = pkcs7Sign;
	}
	public String getJsondc() {
		return jsondc;
	}
	public void setJsondc(String jsondc) {
		this.jsondc = jsondc;
	}
}