package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class N8301_2_IDGATE_DATA implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -762951082705400880L;

	@SerializedName(value = "TSFACN")
	private String TSFACN;
	
	@SerializedName(value = "CUSIDN6")
	private String CUSIDN6;

	@SerializedName(value = "ADOPID")
	private String ADOPID;
	
	@SerializedName(value = "TYPE")
	private String TYPE;
	
	@SerializedName(value = "ITMNUM")
	private String ITMNUM;
	
	@SerializedName(value = "HLHBRH")
	private String HLHBRH;
	
	public String getADOPID() {
		return ADOPID;
	}

	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}

	public String getTYPE() {
		return TYPE;
	}

	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}

	public String getITMNUM() {
		return ITMNUM;
	}

	public void setITMNUM(String iTMNUM) {
		ITMNUM = iTMNUM;
	}

	public String getTSFACN() {
		return TSFACN;
	}

	public void setTSFACN(String tSFACN) {
		TSFACN = tSFACN;
	}

	public String getCUSIDN6() {
		return CUSIDN6;
	}

	public void setCUSIDN6(String cUSIDN6) {
		CUSIDN6 = cUSIDN6;
	}

	public String getHLHBRH() {
		return HLHBRH;
	}

	public void setHLHBRH(String hLHBRH) {
		HLHBRH = hLHBRH;
	}

}
