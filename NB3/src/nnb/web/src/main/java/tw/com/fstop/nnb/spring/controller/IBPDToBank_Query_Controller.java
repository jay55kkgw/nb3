package tw.com.fstop.nnb.spring.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import fstop.orm.po.ONLINE_APPOINTMENT_TXNLOG;
import fstop.orm.po.ONLINE_APPOINTMENT_TXNLOG_PK;
import tw.com.fstop.nnb.service.IBPDToBank_Query_Service;
import tw.com.fstop.tbb.nnb.dao.OnlineAppointmentTxnlogDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;

@Controller
@RequestMapping(value = "/IBPDToBank/API")
public class IBPDToBank_Query_Controller {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	IBPDToBank_Query_Service iBPDToBank_Query_Service;
	
	@Autowired
	OnlineAppointmentTxnlogDao onlineAppointmentTxnlogDao;
	
	@RequestMapping(value = "/query" , method = {RequestMethod.POST} , produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,headers = "content-type=application/json")
	public @ResponseBody Map<String, String> query(HttpServletRequest request, HttpServletResponse response, @RequestBody Map<String,Object> reqParam, Model model) {
		Map<String, String> result = new HashMap<String, String>();
		try {
			log.debug(ESAPIUtil.vaildLog("send reqParam >> "+ reqParam));
			if(iBPDToBank_Query_Service.checkMac(reqParam)) {
				result = iBPDToBank_Query_Service.query(reqParam);
			}else {
				result.put("RCode", "0302");
			}
			try {
				ONLINE_APPOINTMENT_TXNLOG po = new ONLINE_APPOINTMENT_TXNLOG();
				ONLINE_APPOINTMENT_TXNLOG_PK pks = new ONLINE_APPOINTMENT_TXNLOG_PK();
				pks.setTXNDATE(((String)reqParam.get("TxnDatetime")).length() >= 8 ? ((String)reqParam.get("TxnDatetime")).substring(0, 8) : DateUtil.getDate(""));
				pks.setSTAN((String)reqParam.get("STAN"));
				pks.setSRCID((String)reqParam.get("SrcID"));
				pks.setKEYID((String)reqParam.get("KeyID"));
				po.setPks(pks);
				po.setTXNDATETIME((String)reqParam.get("TxnDatetime"));
				po.setCUSTBILLERACNT(result.get("CustBillerAcnt"));
				po.setCUSTBANKACNT(result.get("CustBankAcnt"));
				po.setDIVDATA((String)reqParam.get("DivData"));
				po.setICV((String)reqParam.get("ICV"));
				po.setMAC((String)reqParam.get("MAC"));
				po.setRCODE(result.get("RCode"));
				po.setORIGTXNDATETIME((String)reqParam.get("OrigTxnDatetime"));
				po.setORIGSTAN((String)reqParam.get("OrigSTAN"));
				po.setLASTDATE(DateUtil.getDate(""));
				po.setLASTTIME(DateUtil.getTheTime(""));
				po.setRESULTDATA(CodeUtil.toJson(result));
			}catch(Exception e) {
				log.error(e.toString());
			}
		}catch(Exception e) {
			result.put("RCode", "9999");
		}
		return result;
	}
}
