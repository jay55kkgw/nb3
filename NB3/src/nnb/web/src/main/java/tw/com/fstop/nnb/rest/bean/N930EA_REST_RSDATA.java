package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N930EA_REST_RSDATA extends BaseRestBean implements Serializable   {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5461140141220018586L;

	private String DPADDBKID;

	private String DPUSERID;

	private String DPGONAME;

	private String DPABMAIL;

	private String LASTDATE;

	private String LASTTIME;

	public String getDPADDBKID() {
		return DPADDBKID;
	}

	public void setDPADDBKID(String dPADDBKID) {
		DPADDBKID = dPADDBKID;
	}

	public String getDPUSERID() {
		return DPUSERID;
	}

	public void setDPUSERID(String dPUSERID) {
		DPUSERID = dPUSERID;
	}

	public String getDPGONAME() {
		return DPGONAME;
	}

	public void setDPGONAME(String dPGONAME) {
		DPGONAME = dPGONAME;
	}

	public String getDPABMAIL() {
		return DPABMAIL;
	}

	public void setDPABMAIL(String dPABMAIL) {
		DPABMAIL = dPABMAIL;
	}

	public String getLASTDATE() {
		return LASTDATE;
	}

	public void setLASTDATE(String lASTDATE) {
		LASTDATE = lASTDATE;
	}

	public String getLASTTIME() {
		return LASTTIME;
	}

	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}
	
	
}
