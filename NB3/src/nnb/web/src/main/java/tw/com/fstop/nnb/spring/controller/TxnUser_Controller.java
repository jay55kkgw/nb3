package tw.com.fstop.nnb.spring.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import fstop.orm.po.OLD_TXNUSER;
import fstop.orm.po.TXNUSER;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.custom.annotation.ISTXNLOG;
import tw.com.fstop.nnb.service.Acct_Service;
import tw.com.fstop.nnb.service.DaoService;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.Old_TxnUserDao;
import tw.com.fstop.tbb.nnb.dao.TxnUserDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;

@RestController
@RequestMapping(value = "/MB/TXNUSER")
public class TxnUser_Controller {

	Logger log = LoggerFactory.getLogger(this.getClass());

	//@Autowired
	//Logger log;
	@Autowired
	DaoService daoservice;
	@Autowired
	Acct_Service acct_service;

	@Autowired
	private TxnUserDao txnUserDao;
	@Autowired
	private Old_TxnUserDao oldtxnUserDao;
	
	@Autowired
	I18n i18n;
	

	
	@RequestMapping(value = "/query", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody BaseResult query(HttpServletRequest request, HttpServletResponse response,
			@RequestBody Map<String, String> reqParam, Model model) {
		log.info(ESAPIUtil.vaildLog("reqParam>>{}"+ CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		try {
			Map<String,String> okMap = ESAPIUtil.validStrMap(reqParam);
//			Reflected XSS All Clients
			String cusidn = okMap.get("cusidn");
//			String cusidn = reqParam.get("cusidn");
//			List<TXNUSER> list = txnUserDao.findByUserId(cusidn);
			TXNUSER po= txnUserDao.get(TXNUSER.class, cusidn);
			log.trace(ESAPIUtil.vaildLog("User >>{}"+ po));
			bs.reset();
			bs.setData(po);
			bs.setResult(Boolean.TRUE);
			// CodeUtil.convert2BaseResult(bs, obj);
			bs.setMessage("0", i18n.getMsg("LB.X1805"));
		} catch (Exception e) {
			log.error(e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", i18n.getMsg("LB.X2132"));
		}
		return bs;

	}
	@RequestMapping(value = "/N110", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody BaseResult N110(HttpServletRequest request, HttpServletResponse response,
			@RequestBody Map<String, String> reqParam, Model model) {
		log.info(ESAPIUtil.vaildLog("reqParam>>{}"+ CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		try {
			String cusidn = reqParam.get("cusidn");
			bs = acct_service.balance_query(cusidn);
			bs.setMessage("0", i18n.getMsg("LB.X1805"));
		} catch (Exception e) {
			log.error(e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", i18n.getMsg("LB.X2132"));
		}
		return bs;
		
	}
	@RequestMapping(value = "/queryo", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody BaseResult queryo(HttpServletRequest request, HttpServletResponse response,
			@RequestBody Map<String, String> reqParam, Model model) {
		log.info(ESAPIUtil.vaildLog("reqParam>>{}"+ CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		try {
			String cusidn = reqParam.get("cusidn");
			List<OLD_TXNUSER> list = oldtxnUserDao.findByUserId(cusidn);
			log.trace(ESAPIUtil.vaildLog("OLD User >>{}"+ list));
			bs.reset();
			bs.setData(list);
			bs.setResult(Boolean.TRUE);
			// CodeUtil.convert2BaseResult(bs, obj);
			bs.setMessage("0", i18n.getMsg("LB.X1805"));
		} catch (Exception e) {
			log.error(e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", i18n.getMsg("LB.X2132"));
		}
		return bs;
		
	}
	@RequestMapping(value = "/queryt", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody BaseResult queryt(HttpServletRequest request, HttpServletResponse response,
			@RequestBody Map<String, String> reqParam, Model model) {
		log.info(ESAPIUtil.vaildLog("reqParam>>{}"+ CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		try {
			String cusidn = reqParam.get("cusidn");
			List<TXNUSER> list = txnUserDao.findByUserId(cusidn);
			log.trace(ESAPIUtil.vaildLog("User >>{}"+ list));
			bs.reset();
			bs.setData(list);
			bs.setResult(Boolean.TRUE);
			// CodeUtil.convert2BaseResult(bs, obj);
			bs.setMessage("0", i18n.getMsg("LB.X1805"));
		} catch (Exception e) {
			log.error(e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", i18n.getMsg("LB.X2132"));
		}
		return bs;
		
	}

	@RequestMapping(value = "/add", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody BaseResult add(HttpServletRequest request, HttpServletResponse response,
			@RequestBody Map<String, String> reqParam, Model model) {
		log.info(ESAPIUtil.vaildLog("reqParam>>{}"+ CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		try {
			
			TXNUSER newUser = new TXNUSER();
			Map<String, String> reqParamNew = new HashMap<String, String>();
			Set<String> keys = reqParam.keySet();
			for(String key:keys) {
				//檢測屬性為String 且非 ASKERRTIMES、SCHCOUNT、DPSUERID的null
				List<String> regList = new ArrayList<String>();
				regList.add("ASKERRTIMES");
				regList.add("SCHCOUNT");
				regList.add("DPSUERID");
				if(!(regList.indexOf(key) == -1 && reqParam.get(key) == null)){
					reqParamNew.put(key.toUpperCase(), reqParam.get(key));
				}
			}
			log.debug(ESAPIUtil.vaildLog("reqParamNew="+reqParamNew));
			if(reqParamNew.containsKey("DPMYEMAIL") && !"".equals(reqParamNew.get("DPMYEMAIL"))){
				String DPMYEMAIL = reqParamNew.get("DPMYEMAIL");
				String regex = "^\\w+((-\\w+)|(\\.\\w+))*\\@[A-Za-z0-9]+((\\.|-)[A-Za-z0-9]+)*\\.[A-Za-z]+$";
				if(!DPMYEMAIL.matches(regex)){
					bs.setResult(Boolean.FALSE);
					bs.setMessage("1", "email格式不符");
					return bs;
				}
			}
			newUser = CodeUtil.objectCovert(TXNUSER.class, reqParamNew);
			newUser.setLASTDATE(new DateTime().toString("yyyyMMdd"));
			newUser.setLASTTIME(new DateTime().toString("HHmmss"));
			
			

			if (newUser.getDPSUERID() == "") {
				bs.setResult(Boolean.FALSE);
				bs.setMessage("1", i18n.getMsg("LB.X2137"));
			// } else if (newUser.getADBRANCHID() == "") {
			// 	bs.setResult(Boolean.FALSE);
			// 	bs.setMessage("1", i18n.getMsg("LB.X2138"));
			// } else if (newUser.getASKERRTIMES() == null) {
			// 	bs.setResult(Boolean.FALSE);
			// 	bs.setMessage("1", i18n.getMsg("LB.X2139"));
			} else {
				txnUserDao.save(newUser);
				log.trace(ESAPIUtil.vaildLog("User >>{}"+ CodeUtil.toJson(newUser)));
				bs.reset();
				// bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
				// bs.addData("Data", newUser);
				bs.setResult(Boolean.TRUE);
				bs.setMessage("0", i18n.getMsg("LB.X0435"));
			}
		} catch (Exception e) {
			log.error(e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", i18n.getMsg("LB.X2133"));
		}
		return bs;

	}

	@RequestMapping(value = "/update", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody BaseResult update(HttpServletRequest request, HttpServletResponse response,
			@RequestBody Map<String, String> reqParam, Model model) {
		log.info(ESAPIUtil.vaildLog("reqParam>>{}"+ CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		try {
			TXNUSER oldUser = txnUserDao.getNotifyById(reqParam.get("dpsuerid"));
			TXNUSER updateUser = new TXNUSER();
			Map<String, String> reqParamNew = new HashMap<String, String>();
			Set<String> keys = reqParam.keySet();
			for(String key:keys) {
				//檢測屬性為String 且非 ASKERRTIMES、SCHCOUNT、DPSUERID的null
				List<String> regList = new ArrayList<String>();
				regList.add("ASKERRTIMES");
				regList.add("SCHCOUNT");
				regList.add("DPSUERID");
				if(!(regList.indexOf(key) == -1 && reqParam.get(key) == null)){
					reqParamNew.put(key.toUpperCase(), reqParam.get(key));
				}
			}
			log.debug(ESAPIUtil.vaildLog("reqParamNew="+reqParamNew));
			if(reqParamNew.containsKey("DPMYEMAIL") && !"".equals(reqParamNew.get("DPMYEMAIL"))){
				String DPMYEMAIL = reqParamNew.get("DPMYEMAIL");
				String regex = "^\\w+((-\\w+)|(\\.\\w+))*\\@[A-Za-z0-9]+((\\.|-)[A-Za-z0-9]+)*\\.[A-Za-z]+$";
				if(!DPMYEMAIL.matches(regex)){
					bs.setResult(Boolean.FALSE);
					bs.setMessage("1", "email格式不符");
					return bs;
				}
			}
			updateUser = CodeUtil.objectCovert(TXNUSER.class, reqParamNew);
			updateUser.setLASTDATE(new DateTime().toString("yyyyMMdd"));
			updateUser.setLASTTIME(new DateTime().toString("HHmmss"));
			if (updateUser.getDPSUERID() == "") {
				bs.setResult(Boolean.FALSE);
				bs.setMessage("1", i18n.getMsg("LB.X2137"));
			// } else if (updateUser.getADBRANCHID() == "") {
			// 	bs.setResult(Boolean.FALSE);
			// 	bs.setMessage("1", i18n.getMsg("LB.X2138"));
			// } else if (updateUser.getASKERRTIMES() == null) {
			// 	bs.setResult(Boolean.FALSE);
			// 	bs.setMessage("1", i18n.getMsg("LB.X2139"));
			} else {
				txnUserDao.saveOrUpdate(updateUser);
				log.trace("User >>{}", updateUser);
				bs.reset();
				bs.setResult(Boolean.TRUE);
				bs.setMessage("0", i18n.getMsg("LB.D0399"));
			}
		} catch (Exception e) {
			log.error(e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", i18n.getMsg("LB.X2136"));
		}
		return bs;

	}

	@RequestMapping(value = "/delete", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody BaseResult delete(HttpServletRequest request, HttpServletResponse response,
			@RequestBody Map<String, String> reqParam, Model model) {
		log.info(ESAPIUtil.vaildLog("reqParam>>{}"+ CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		try {
			String dpsuerid = reqParam.get("dpsuerid");
			TXNUSER deleteUser = new TXNUSER();
			deleteUser.setDPSUERID(new String(dpsuerid));
			txnUserDao.remove(deleteUser);
			log.trace(ESAPIUtil.vaildLog("User >>{}"+ CodeUtil.toJson(deleteUser)));
			bs.reset();
			bs.setResult(Boolean.TRUE);
			bs.setMessage("0", i18n.getMsg("LB.X0436"));
		} catch (Exception e) {
			log.error(e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", i18n.getMsg("LB.X2135"));
		}
		return bs;

	}
	
	@RequestMapping(value="/Create", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody BaseResult create(@ModelAttribute @Valid TXNUSER admCountry, BindingResult result){

		BaseResult response = new BaseResult();
        try {
            if ( result.hasErrors() ) {
                Map<String, String> errors = result.getFieldErrors().stream()
	                .collect(
	                    Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage)
	                );
                response.setResult(false);
                response.addAllData(errors);
            } else {
                response.setResult(true);
	        }
	
	    }catch (Exception e) {
	    	log.error(e.toString());
	    }
        return response;

    }

	
	/**
	 * 給舊行動中台偵測服務用
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/echo", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody BaseResult test(HttpServletRequest request, HttpServletResponse response,
			@RequestBody Map<String, String> reqParam, Model model) {
		log.info(ESAPIUtil.vaildLog("reqParam>>{}"+ CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		try {
			bs.setResult(Boolean.TRUE);
			bs.setMessage("0", i18n.getMsg("LB.X1805"));
		} catch (Exception e) {
			log.error(e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", i18n.getMsg("LB.X2132"));
		}
		return bs;
		
	}
}
