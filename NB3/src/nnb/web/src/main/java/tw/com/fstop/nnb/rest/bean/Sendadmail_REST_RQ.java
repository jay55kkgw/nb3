package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.List;

public class Sendadmail_REST_RQ extends BaseRestBean_PS implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6609503751986828158L;
	private String Subject;//主旨
	private String Content;//mail內文
	private String ContentType;//mail內文格式 1:表示clien 端信件內容及格式都自行組
	private List<String> Receivers;//收件者
	public String getSubject() {
		return Subject;
	}
	public void setSubject(String subject) {
		Subject = subject;
	}
	public String getContent() {
		return Content;
	}
	public void setContent(String content) {
		Content = content;
	}
	public List<String> getReceivers() {
		return Receivers;
	}
	public void setReceivers(List<String> receivers) {
		Receivers = receivers;
	}
	public String getContentType() {
		return ContentType;
	}
	public void setContentType(String contentType) {
		ContentType = contentType;
	}
	
	
	
}
