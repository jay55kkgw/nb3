package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N177_REST_RS extends BaseRestBean implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 175776978484878341L;
	String FDPNUM;		// 存單號碼
	String AMTFDP;		// 存單金額
	String HEADER;
	String CRY;			// 幣別
	String MAC;
	String AUTXFTM;		// 轉期期數
	String CODE;		// 轉存方式
	String INTMTH;		// 計息方式
	String TRNTIME;		// 交易時間 HHMMSS
	String CMQTIME; 	// 交易日期時間 YYYY/MM/DD HH:MM:SS
	String OFFSET;
	String __OCCURS;
	String FYACN;		// 存單帳號
	String FYTSFAN;		// 轉入帳號
	String SYNC;
	String TRNDATE;		// 交易日期 YYYMMDD
	String CMTXTIME;
	
	public String getFDPNUM() {
		return FDPNUM;
	}
	public void setFDPNUM(String fDPNUM) {
		FDPNUM = fDPNUM;
	}
	public String getAMTFDP() {
		return AMTFDP;
	}
	public void setAMTFDP(String aMTFDP) {
		AMTFDP = aMTFDP;
	}
	public String getHEADER() {
		return HEADER;
	}
	public void setHEADER(String hEADER) {
		HEADER = hEADER;
	}
	public String getCRY() {
		return CRY;
	}
	public void setCRY(String cRY) {
		CRY = cRY;
	}
	public String getMAC() {
		return MAC;
	}
	public void setMAC(String mAC) {
		MAC = mAC;
	}
	public String getAUTXFTM() {
		return AUTXFTM;
	}
	public void setAUTXFTM(String aUTXFTM) {
		AUTXFTM = aUTXFTM;
	}
	public String getCODE() {
		return CODE;
	}
	public void setCODE(String cODE) {
		CODE = cODE;
	}
	public String getINTMTH() {
		return INTMTH;
	}
	public void setINTMTH(String iNTMTH) {
		INTMTH = iNTMTH;
	}
	public String getTRNTIME() {
		return TRNTIME;
	}
	public void setTRNTIME(String tRNTIME) {
		TRNTIME = tRNTIME;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getOFFSET() {
		return OFFSET;
	}
	public void setOFFSET(String oFFSET) {
		OFFSET = oFFSET;
	}
	public String get__OCCURS() {
		return __OCCURS;
	}
	public void set__OCCURS(String __OCCURS) {
		this.__OCCURS = __OCCURS;
	}
	public String getFYACN() {
		return FYACN;
	}
	public void setFYACN(String fYACN) {
		FYACN = fYACN;
	}
	public String getFYTSFAN() {
		return FYTSFAN;
	}
	public void setFYTSFAN(String fYTSFAN) {
		FYTSFAN = fYTSFAN;
	}
	public String getSYNC() {
		return SYNC;
	}
	public void setSYNC(String sYNC) {
		SYNC = sYNC;
	}
	public String getTRNDATE() {
		return TRNDATE;
	}
	public void setTRNDATE(String tRNDATE) {
		TRNDATE = tRNDATE;
	}
	public String getCMTXTIME() {
		return CMTXTIME;
	}
	public void setCMTXTIME(String cMTXTIME) {
		CMTXTIME = cMTXTIME;
	}
	
}
