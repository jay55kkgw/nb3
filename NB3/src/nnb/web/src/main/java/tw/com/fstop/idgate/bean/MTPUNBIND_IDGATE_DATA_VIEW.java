package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;


public class MTPUNBIND_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5370773529503299761L;

	@SerializedName(value = "mobilephone")
	private String mobilephone;

	@SerializedName(value = "txacn")
	private String txacn;
	
	@SerializedName(value = "binddefault")
	private String binddefault;
	public String getBinddefault() {
		return binddefault;
	}
	public void setBinddefault(String binddefault) {
		this.binddefault = binddefault;
	}
	
	@Override
	public Map<String, String> coverMap(){
		Map<String, String> result = new LinkedHashMap<String, String>();
		String binddefault_str = "";
		if("Y".equals(this.binddefault)) 
			binddefault_str = "是";
		else
			binddefault_str = "否";
		
		result.put("交易名稱", "手機號碼註銷轉入帳號");
		result.put("手機號碼", this.mobilephone);
		result.put("綁定帳號", this.txacn);
		result.put("同意作為預設收款帳戶", binddefault_str);
		return result;
	}

	public String getMobilephone() {
		return mobilephone;
	}

	public void setMobilephone(String mobilephone) {
		this.mobilephone = mobilephone;
	}

	public String getTxacn() {
		return txacn;
	}

	public void setTxacn(String txacn) {
		this.txacn = txacn;
	}



	
}
