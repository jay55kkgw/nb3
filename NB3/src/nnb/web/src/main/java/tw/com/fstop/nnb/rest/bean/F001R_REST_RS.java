package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class F001R_REST_RS extends BaseRestBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5742105962348348714L;

	private String MSGCODE;
	private String F007CURAMT;
	private String F007ATRAMT;
	private String RATE;
	private String BGROENO;
	
	
	public String getMSGCODE() {
		return MSGCODE;
	}
	public void setMSGCODE(String mSGCODE) {
		MSGCODE = mSGCODE;
	}
	public String getF007CURAMT() {
		return F007CURAMT;
	}
	public void setF007CURAMT(String f007curamt) {
		F007CURAMT = f007curamt;
	}
	public String getF007ATRAMT() {
		return F007ATRAMT;
	}
	public void setF007ATRAMT(String f007atramt) {
		F007ATRAMT = f007atramt;
	}
	public String getRATE() {
		return RATE;
	}
	public void setRATE(String rATE) {
		RATE = rATE;
	}
	public String getBGROENO() {
		return BGROENO;
	}
	public void setBGROENO(String bGROENO) {
		BGROENO = bGROENO;
	}
	
	
	
}
