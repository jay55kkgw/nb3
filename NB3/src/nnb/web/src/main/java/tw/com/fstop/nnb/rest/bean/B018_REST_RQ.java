package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class B018_REST_RQ extends BaseRestBean_FUND implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4535152293557575005L;
	
	private String PRDNO ;
	private String IP ;
	public String getPRDNO() {
		return PRDNO;
	}
	public void setPRDNO(String pRDNO) {
		PRDNO = pRDNO;
	}
	public String getIP() {
		return IP;
	}
	public void setIP(String iP) {
		IP = iP;
	}
	
	
}
