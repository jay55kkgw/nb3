package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N366_REST_RQ extends BaseRestBean_OLS implements Serializable {
	
	private static final long serialVersionUID = 4784098371040643285L;
	private String DATE;			// 日期YYYMMDD
	private String TIME;			// 時間HHMMSS
	private String CUSIDN;			// 身份證號
	private String ACN;				// 欲結清帳號
	private String ADOPID;
	
	
	public String getDATE() {
		return DATE;
	}
	
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	
	public String getTIME() {
		return TIME;
	}
	
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	
	public String getACN() {
		return ACN;
	}
	
	public void setACN(String aCN) {
		ACN = aCN;
	}

	public String getADOPID() {
		return ADOPID;
	}

	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	
}