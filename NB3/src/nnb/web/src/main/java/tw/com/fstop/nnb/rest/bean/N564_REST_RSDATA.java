package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N564_REST_RSDATA implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8546109277562505814L;
	private String RBILLAMT;
	private String RNEGDATE;
	private String RBILLTYP;
	private String RREFNO;
	private String RCMDATE1;
	private String RCOUNTRY;
	private String RMARK;
	private String RUPSTAT;
	private String RLCNO;
	private String RBILLCCY;
    private String RVALDATE;

	public String getRREFNO() {
		return RREFNO;
	}
	public void setRREFNO(String rREFNO) {
		RREFNO = rREFNO;
	}
	public String getRMARK() {
		return RMARK;
	}
	public void setRMARK(String rMARK) {
		RMARK = rMARK;
	}
	public String getRBILLAMT() {
		return RBILLAMT;
	}
	public void setRBILLAMT(String rBILLAMT) {
		RBILLAMT = rBILLAMT;
	}
	public String getRNEGDATE() {
		return RNEGDATE;
	}
	public void setRNEGDATE(String rNEGDATE) {
		RNEGDATE = rNEGDATE;
	}
	public String getRBILLTYP() {
		return RBILLTYP;
	}
	public void setRBILLTYP(String rBILLTYP) {
		RBILLTYP = rBILLTYP;
	}
	public String getRCMDATE1() {
		return RCMDATE1;
	}
	public void setRCMDATE1(String rCMDATE1) {
		RCMDATE1 = rCMDATE1;
	}
	public String getRLCNO() {
		return RLCNO;
	}
	public void setRLCNO(String rLCNO) {
		RLCNO = rLCNO;
	}
	public String getRCOUNTRY() {
		return RCOUNTRY;
	}
	public void setRCOUNTRY(String rCOUNTRY) {
		RCOUNTRY = rCOUNTRY;
	}
	public String getRUPSTAT() {
		return RUPSTAT;
	}
	public void setRUPSTAT(String rUPSTAT) {
		RUPSTAT = rUPSTAT;
	}
	public String getRBILLCCY() {
		return RBILLCCY;
	}
	public void setRBILLCCY(String rBILLCCY) {
		RBILLCCY = rBILLCCY;
	}
	public String getRVALDATE() {
		return RVALDATE;
	}
	public void setRVALDATE(String rVALDATE) {
		RVALDATE = rVALDATE;
	}
}
