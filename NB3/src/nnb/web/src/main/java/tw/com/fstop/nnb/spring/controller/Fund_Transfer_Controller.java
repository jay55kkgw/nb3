package tw.com.fstop.nnb.spring.controller;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.google.gson.Gson;

import fstop.orm.po.ADMCURRENCY;
import fstop.orm.po.TXNFUNDDATA;
import fstop.orm.po.TXNUSER;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.idgate.bean.N322_2_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N322_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N322_IDGATE_DATA_VIEW;
import tw.com.fstop.nnb.aop.Dialog;
import tw.com.fstop.nnb.service.Fund_Transfer_Service;
import tw.com.fstop.nnb.service.IdGateService;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.util.StrUtils;
import tw.com.fstop.web.util.WebUtil;

/**
 * 基金轉換交易的Controller
 */
@SessionAttributes({ SessionUtil.CUSIDN, SessionUtil.KYCDATE, SessionUtil.WEAK, SessionUtil.XMLCOD,
		SessionUtil.DPMYEMAIL, SessionUtil.KYC, SessionUtil.STEP1_LOCALE_DATA, SessionUtil.STEP2_LOCALE_DATA,
		SessionUtil.STEP3_LOCALE_DATA, SessionUtil.CONFIRM_LOCALE_DATA, SessionUtil.IDGATE_TRANSDATA, SessionUtil.RESULT_LOCALE_DATA,
		SessionUtil.USERIP, SessionUtil.KYC_INSERT_HIST_PASS , SessionUtil.KYC_INSERT_HIST_PASS_FINSH })
@Controller
@RequestMapping(value = "/FUND/TRANSFER")
public class Fund_Transfer_Controller {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private Fund_Transfer_Service fund_transfer_service;

	@Autowired
	I18n i18n;

	@Autowired		 
	IdGateService idgateservice;

	/**
	 * 前往基金轉換查詢頁
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/query_fund_transfer_data")
	public String query_fund_transfer_data(HttpServletRequest request, @RequestParam Map<String, String> requestParam,
			Model model) {
		String target = "/error";
		Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
		BaseResult bs = null;
		BaseResult n922BS = null;
		log.debug("query_fund_transfer_data");

		try {
			bs = new BaseResult();
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"UTF-8");
			log.debug("cusidn={}", cusidn);

			bs = fund_transfer_service.query_fund_transfer_data(cusidn);
			log.debug("bs.getData={}", bs.getData());

			if (bs == null || !bs.getResult()) {
				model.addAttribute(BaseResult.ERROR, bs);
				return target;
			}

			Map<String, Object> bsData = (Map<String, Object>) bs.getData();
			log.debug("bsData={}", bsData);

			List<Map<String, String>> rows = (List<Map<String, String>>) bsData.get("REC");
			log.debug("rows={}", rows);
			
			//排序
			rows.sort(Comparator.comparing(
                    m -> m.get("TRANSCODE"), 
                    Comparator.nullsLast(Comparator.naturalOrder()))
            );

			n922BS = new BaseResult();
			n922BS = fund_transfer_service.getN922Data(cusidn);

			if (n922BS == null || !n922BS.getResult()) {
				model.addAttribute(BaseResult.ERROR, n922BS);
				return target;
			}
			Map<String, Object> n922BSData = (Map<String, Object>) n922BS.getData();
			log.debug("n922BSData={}", n922BSData);

			// 判斷KYC來源為行外且無錄音檔時，轉入錯誤訊息頁面
			// 是否為行外
			String EXTRCE = n922BSData.get("EXTRCE") == null ? "" : ((String) n922BSData.get("EXTRCE")).trim();
			log.debug("EXTRCE={}", EXTRCE);

			// 錄音檔交易序號
			String RECNUM = n922BSData.get("RECNUM") == null ? "" : ((String) n922BSData.get("RECNUM")).trim();
			log.debug("RECNUM={}", RECNUM);

			if ("Y".equals(EXTRCE) && RECNUM.length() == 0) {
				n922BS.setMessage("Z622", i18n.getMsg("LB.X1768"));
				model.addAttribute(BaseResult.ERROR, n922BSData);
				return target;
			}
			
//			//20220318 移除此導頁功能 , 改由頁面提示
//			// 檢核Email
//			String WANAEMAIL = okMap.get("WANAEMAIL");
//			log.debug(ESAPIUtil.vaildLog("WANAEMAIL={}" + WANAEMAIL));
//
//			if (!"NONEED".equals(WANAEMAIL)) {
//				String DPMYEMAIL = (String) SessionUtil.getAttribute(model, SessionUtil.DPMYEMAIL, null);
//				log.debug("DPMYEMAIL={}", DPMYEMAIL);
//
//				if (DPMYEMAIL == null || "".equals(DPMYEMAIL)) {
//					// 設定EMAIL
//					target = "forward:/FUND/TRANSFER/fundemail?ADOPID=FundEmail&TXID=C021";
//					return target;
//				}
//			}

			String UID = cusidn;
			model.addAttribute("CUSIDN", cusidn);
			model.addAttribute("UID", UID);
			String hiddenCUSIDN = WebUtil.hideID(cusidn);
			model.addAttribute("hiddenCUSIDN", hiddenCUSIDN);

			// 信託號碼
			String CDNO = request.getParameter("CDNO");
			log.debug(ESAPIUtil.vaildLog("CDNO >> " + CDNO));
			// 基金代碼
			String TRANSCODE = request.getParameter("TRANSCODE");
			log.debug(ESAPIUtil.vaildLog("TRANSCODE >> " + TRANSCODE));
			// 姓名
//			String NAME = bsData.get("NAME") == null ? "" : (String) bsData.get("NAME");
			
			//20210617修正  若有CUSNAME取CUSNAME 無則為NAME , 不然會是空的 //N922原住民姓名修改由NAME->CUSNAME  
			String NAME = "";
			if(StrUtils.isNotEmpty((String)n922BSData.get("CUSNAME"))) {
				NAME = (String)bsData.get("CUSNAME");
			}else {
				NAME = (String)bsData.get("NAME");
			}
			
			log.debug("NAME={}", NAME);
			model.addAttribute("NAME", NAME);
			String hiddenNAME = WebUtil.hideusername1Convert(NAME);
			model.addAttribute("hiddenNAME", hiddenNAME);
			// 投資屬性
			String FDINVTYPE = bsData.get("FDINVTYPE") == null ? "" : (String) bsData.get("FDINVTYPE");
			log.debug("FDINVTYPE={}", FDINVTYPE);
			model.addAttribute("FDINVTYPE", FDINVTYPE);
			// 問卷填寫日期
			String GETLTD = bsData.get("GETLTD") == null ? "" : (String) bsData.get("GETLTD");
			log.debug("GETLTD={}", GETLTD);
			// 專業屬性資料取得日期
			String GETLTD7 = bsData.get("GETLTD7") == null ? "" : (String) bsData.get("GETLTD7");
			log.debug("GETLTD7={}", GETLTD7);
			model.addAttribute("GETLTD7", GETLTD7);

			String KYCDAT = "";
			if (GETLTD.length() == 7 && GETLTD.equals("9999999")) {
				KYCDAT = "9" + GETLTD;
			} else if (GETLTD.length() == 7 && !GETLTD.equals("9999999")) {
				KYCDAT = "0" + GETLTD;
			} else {
				KYCDAT = GETLTD;
			}
			log.debug("KYCDAT={}", KYCDAT);
			model.addAttribute("KYCDAT", KYCDAT);
			SessionUtil.addAttribute(model, SessionUtil.KYCDATE, KYCDAT);

			// 專業投資人屬性
			String RISK7 = bsData.get("RISK7") == null ? "" : ((String) bsData.get("RISK7")).trim();
			log.debug("RISK7={}", RISK7);
			model.addAttribute("RISK7", RISK7);
			// 生日
			String BRTHDY = bsData.get("BRTHDY") == null ? "" : (String) bsData.get("BRTHDY");
			log.debug("BRTHDY={}", BRTHDY);
			// 學歷
			String DEGREE = bsData.get("DEGREE") == null ? "" : (String) bsData.get("DEGREE");
			log.debug("DEGREE={}", DEGREE);
			// 重大傷病
			String MARK1 = bsData.get("MARK1") == null ? "" : (String) bsData.get("MARK1");
			log.debug("MARK1={}", MARK1);
			// 一年內信託交易>=5 筆註記
			String MARK3 = bsData.get("MARK3") == null ? "" : (String) bsData.get("MARK3");
			log.debug("MARK3={}", MARK3);

			int age = fund_transfer_service.getAge(BRTHDY);

			String WEAK = "";
			if (age >= 70) {
				WEAK = "Y";
			} else {
				WEAK = "N";
			}
			if (DEGREE.equals("6")) {
				WEAK += "Y";
			} else {
				WEAK += "N";
			}
			if (MARK1.equals("Y")) {
				WEAK += "Y";
			} else {
				WEAK += "N";
			}
			if (MARK3.equals("Y")) {
				WEAK += "Y";
			} else {
				WEAK += "N";
			}
			log.debug("WEAK={}", WEAK);
			model.addAttribute("WEAK", WEAK);
			SessionUtil.addAttribute(model, SessionUtil.WEAK, WEAK);

			String time1 = "";
			String time2 = "";
			long day = 0;
			if (!GETLTD.equals("")) {
				time1 = new SimpleDateFormat("yyyy/MM/dd").format(new Date());
				log.debug("time1={}", time1);
				String GETLTDString = Integer.valueOf(GETLTD.substring(0, 3)) + 1911 + "";
				GETLTDString = GETLTDString + GETLTD.substring(3);
				log.debug("GETLTDString={}", GETLTDString);
				time2 = GETLTDString.substring(0, 4) + "/" + GETLTDString.substring(4, 6) + "/"
						+ GETLTDString.substring(6, 8);
				log.debug("time2={}", time2);

				// 算出距離上次問卷填寫天數
				day = fund_transfer_service.getRangeDay(time1, time2);
			}

			// 問卷版本日期
			String KYCDate = fund_transfer_service.getKYCDate();

			if (KYCDate == null) {
				KYCDate = "";
			}
			// 自然人版， 需有投資屬性及問卷填寫日期已滿一年或問卷版本日期大於問卷填寫日期；專業投資人跳過不判斷KYC
			if (((!FDINVTYPE.equals("") && !GETLTD.equals("") && day >= 365)
					|| ((!GETLTD.equals("") && !KYCDate.equals(""))
							&& Integer.parseInt(KYCDate) > Integer.parseInt(GETLTD)))
					&& UID.length() == 10 && RISK7.length() == 0) {
				// 假的
				// if(true){
				// if(false){
				if (day >= 365) {
					// 假的
					// if(true){
					// if(false){
					model.addAttribute("TXID", "C021");
					model.addAttribute("ALERTTYPE", "2");

					fund_transfer_service.prepareFundRiskAlertData(okMap, GETLTD, RISK7, model);

					target = "/fund/fund_risk_alert";
				} else {
					target = "forward:/FUND/TRANSFER/fund_invest_attr1?TXID=C021";
				}
				return target;
			}
			// 法人版，需有投資屬性及問卷填寫日期已滿一年或問卷版本日期大於問卷填寫日期；專業投資人跳過不判斷KYC
			else if (((!FDINVTYPE.equals("") && !GETLTD.equals("") && day >= 365)
					|| ((!GETLTD.equals("") && !KYCDate.equals(""))
							&& Integer.parseInt(KYCDate) > Integer.parseInt(GETLTD)))
					&& UID.length() < 10 && RISK7.length() == 0) {
				// else if(true){
				if (day >= 365) {
					// 假的
					// if(false){
					model.addAttribute("TXID", "C021");
					model.addAttribute("ALERTTYPE", "2");

					fund_transfer_service.prepareFundRiskAlertData(okMap, GETLTD, RISK7, model);

					target = "/fund/fund_risk_alert";
				} else {
					target = "forward:/FUND/TRANSFER/fund_invest_attr3?TXID=C021";
				}
				return target;
			} else if (FDINVTYPE.equals("") && RISK7.length() == 0) {
				if (UID.length() == 10) {
					target = "forward:/FUND/TRANSFER/fund_invest_attr1?TXID=C021";
				} else {
					target = "forward:/FUND/TRANSFER/fund_invest_attr3?TXID=C021";
				}
				return target;
			}
			// 未確認標記
			String ConfirmFlag = okMap.get("ConfirmFlag");
			if (ConfirmFlag != null && "TRUE".equals(ConfirmFlag)) {
				model.addAttribute("ConfirmFlag", true);
			}

			// 簽約行
			String BRHCOD = bsData.get("APLBRH") == null ? "" : (String) bsData.get("APLBRH");
			log.debug("BRHCOD={}", BRHCOD);
			model.addAttribute("BRHCOD", BRHCOD);

			boolean breakFlag = false;

			List<Map<String, String>> fundRows = new ArrayList<Map<String, String>>();

			for (int x = 0; x < rows.size(); x++) {
				Map<String, String> rowMap = rows.get(x);
				log.debug("rowMap={}", rowMap);

				if ((CDNO != null && !CDNO.equals(rowMap.get("CDNO")))
						|| (TRANSCODE != null && !TRANSCODE.equals(rowMap.get("TRANSCODE")))) {
					continue;
				}
				if ((CDNO != null && CDNO.equals(rowMap.get("CDNO")))
						&& (TRANSCODE != null && TRANSCODE.equals(rowMap.get("TRANSCODE")))) {
					breakFlag = true;
				}

				TXNFUNDDATA txnFundData = fund_transfer_service.getFundData(rowMap.get("TRANSCODE"));

				if (txnFundData != null) {
					rowMap.put("FUNDLNAME", txnFundData.getFUNDLNAME());
					rowMap.put("COUNTRYTYPE", txnFundData.getCOUNTRYTYPE());
					rowMap.put("FUNDT", txnFundData.getFUNDT());
				}
				
				
				if("000".equals(rowMap.get("FUSMON"))) {
					//前收 : 全部/部分轉換
					rowMap.put("FEE_TYPE_CHK", "1");
				}else {
					if(fund_transfer_service.getFundDataByGroupMark("A",rowMap.get("TRANSCODE"),rowMap.get("FUSMON"),rowMap.get("COUNTRYTYPE"),rowMap.get("CRY")).size()>0) {
						//後收 : 有可以全部轉換的基金
						rowMap.put("FEE_TYPE_CHK", "2");
					}else {
						//後收: 無可以轉換的基金
						rowMap.put("FEE_TYPE_CHK", "3");
						if(StrUtil.isEmpty(rowMap.get("COUNTRYTYPE").trim())) {
							rowMap.put("noticeNsg", i18n.getMsg("LB.X2498")+"<br>"+i18n.getMsg("LB.X2499"));
						}else {
							rowMap.put("noticeNsg", i18n.getMsg("LB.X2498")+"<br>"+i18n.getMsg("LB.X2500"));
						}
						
					}
				}
				
				
				fundRows.add(rowMap);
				
				if (breakFlag) {
					break;
				}
			}
			model.addAttribute("fundRows", fundRows);
			model.addAttribute("dataCount", fundRows.size());
			model.addAttribute("bsData", bsData);

			target = "/fund/query_fund_transfer_data";
		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("query_fund_transfer_data error >> {}", e);
		}
		return target;
	}

	/**
	 * 前往基金風險警告頁
	 */
	@RequestMapping(value = "/fund_risk_alert")
	public String fund_risk_alert(HttpServletRequest request, @RequestParam Map<String, String> requestParam,
			Model model) {
		Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
		String target = "/error";

		log.debug("fund_risk_alert");
		try {
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"UTF-8");
			log.debug("cusidn={}", cusidn);
			model.addAttribute("UID", cusidn);

			String TXID = okMap.get("TXID");
			log.debug(ESAPIUtil.vaildLog("TXID={}" + TXID));
			model.addAttribute("TXID", TXID);

			String ALERTTYPE = okMap.get("ALERTTYPE");
			log.debug(ESAPIUtil.vaildLog("ALERTTYPE={}" + ALERTTYPE));
			model.addAttribute("ALERTTYPE", ALERTTYPE);

			target = "/fund/fund_risk_alert";
		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("fund_risk_alert error >> {}", e);
		}
		return target;
	}

	/**
	 * S 線上投資屬性問卷調查表 導向 前往個人KYC頁
	 */
	@RequestMapping(value = "/fund_invest_attr")
	public String fund_invest_attr(@RequestParam Map<String, String> requestParam, Model model) {
		log.debug("fund_invest_attr start!!");
		BaseResult bs = null;
		String target = "/error";
		String cusidn = "";
		try {
			cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, "UTF-8")));
			bs = new BaseResult();
			bs = fund_transfer_service.getN927Data(cusidn);
			bs.addData("CUSIDN", cusidn);
			
		} catch (Exception e) {
			log.error("fund_invest_attr erro", e);
		} finally {
			if (bs != null && bs.getResult()) {
				Map<String, Object> bsData = (Map) bs.getData();
				String str_RISK = String.valueOf(bsData.get("RISK")); // 投資屬性
				if (cusidn.length() == 10 && !str_RISK.equals("")) {
					target = "forward:/FUND/TRANSFER/fund_invest_attr1?TXID=S";
					// response.sendRedirect(RootPath+"/simpleform?trancode=InvestAttr1&TXID=S");
				} else if (cusidn.length() != 10 && !str_RISK.equals("")) {
					target = "forward:/FUND/TRANSFER/fund_invest_attr3?TXID=S";
					// response.sendRedirect(RootPath+"/simpleform?trancode=InvestAttr3&TXID=S");
				} else {
					model.addAttribute("fund_invest_attr", bs);
					target = "/fund/fund_invest_attr";
				}
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		log.info("fund_invest_attr target {}", target);
		return target;
	}

	/**
	 * 前往個人KYC頁
	 */
	@RequestMapping(value = "/fund_invest_attr1")
	public String fund_invest_attr1(HttpServletRequest request, @RequestParam Map<String, String> requestParam,
			Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.debug("fund_invest_attr1");
		Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
		try {
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"UTF-8");
			String UID = cusidn;

			// 判斷KYC每日僅能填寫三次條件
			boolean todayUserKYCCountBoolean = fund_transfer_service.todayUserKYCCount(UID);
			model.addAttribute("todayUserKYCCountBoolean", todayUserKYCCountBoolean);

			String TXID = okMap.get("TXID");
			log.debug(ESAPIUtil.vaildLog("TXID={}" + TXID));
			model.addAttribute("TXID", TXID);

			// 判斷是不是從線上申請進來，如果是的話，KYC頁不顯示選單及麵包屑
			String ONLINEAPPLY = okMap.get("ONLINEAPPLY");
			log.debug(ESAPIUtil.vaildLog("ONLINEAPPLY={}" + ONLINEAPPLY));
			model.addAttribute("ONLINEAPPLY", ONLINEAPPLY);

			if (TXID != null && !"".equals(TXID) && TXID.substring(0, 1).equals("C")) {
				bs = fund_transfer_service.getN922Data(UID);
			} else {
				bs = fund_transfer_service.getN927Data(UID);
			}
			fund_transfer_service.prepareInvestAttr1Data(bs, TXID, model);
			 //IDGATE身分
			 String idgateUserFlag="N";		 
			 Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
			 try {		 
				 if(IdgateData==null) {
					 IdgateData = new HashMap<String, Object>();
				 }
				 BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
				 if(tmp.getResult()) {		 
					 idgateUserFlag="Y";                  		 
				 }		 
				 tmp.addData("idgateUserFlag",idgateUserFlag);		 
				 IdgateData.putAll((Map<String, String>) tmp.getData());
				 SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
			 }catch(Exception e) {		 
				 log.debug("idgateUserFlag error {}",e);		 
			 }		 
			 model.addAttribute("idgateUserFlag",idgateUserFlag);
  
			target = "/fund/fund_invest_attr1";
		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("fund_invest_attr1 error >> {}", e);
		}
		return target;
	}

	/**
	 * 前往個人KYC確認頁
	 */
	@RequestMapping(value = "/fund_invest_attr2")
	public String fund_invest_attr2(HttpServletRequest request, @RequestParam Map<String, String> requestParam,
			Model model) {
		log.debug("fund_invest_attr2 Start~~~~~");
		String target = "/error";
		String next = "fund/fund_invest_attr";
		BaseResult bs = new BaseResult();
		Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
		try {
			String XMLCOD = (String) SessionUtil.getAttribute(model, SessionUtil.XMLCOD, null);
			// Get session value by session Key
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			okMap.put("CUSIDN", cusidn);
			
			model.addAttribute("XMLCOD", XMLCOD);
			bs = fund_transfer_service.prepareInvestAttr2Data(okMap, model);
			Map<String, String> result = new HashMap<>();
			// 通用處理
			for(String key : requestParam.keySet()) {
				String value = requestParam.get(key);
				result.put(key, value);
			}
			log.debug("session result >> {}", result);            		 
			// IDGATE transdata
            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
    		String adopid = "N322";
    		String title = "您有一筆投資屬性評估調查表－（自然人版）問卷結果更新待確認";
        	if(IdgateData != null) {
	            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
	            if(IdgateData.get("idgateUserFlag").equals("Y")) {
	            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
	            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
					IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N322_IDGATE_DATA.class, N322_IDGATE_DATA_VIEW.class, result));
	                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	            }
        	}
            model.addAttribute("idgateAdopid", adopid);
            model.addAttribute("idgateTitle", title);

		} catch (Exception e) {
			log.error("fund_invest_attr2 error >> {}", e);
		} finally {
			if (bs.getResult()) {
				target = "/fund/fund_invest_attr2";
			} else {
				bs.setNext(next);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * 執行個人KYC變更
	 */
	@RequestMapping(value = "/fund_invest_attr_s")
	public String fund_invest_attr_s(HttpServletRequest request, @RequestParam Map<String, String> requestParam,
			Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.debug("fund_invest_attr_s");
		try {

			bs = new BaseResult();
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"UTF-8");
			log.debug("cusidn={}", cusidn);
			requestParam.put("CUSIDN", cusidn);
			
			//20200226註解 ( 如要上KYC功能修正再打開 )
			String aduserip = (String) SessionUtil.getAttribute(model, SessionUtil.USERIP, null);
			requestParam.put("ADUSERIP", aduserip);
			requestParam.put("PCMAC", "");
			String devName = requestParam.get("DEVNAME");
			if (devName.length() > 30) {
				devName = devName.substring(0, 30);
			}
			log.info("devName.length() : " + devName.length());
			log.info(ESAPIUtil.vaildLog("devName : " + devName));
			requestParam.put("DEVNAME", devName);
			log.info(ESAPIUtil.vaildLog("requestParam : " + requestParam));
			if(requestParam.get("FGTXWAY").equals("7")) {		 
				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N322_IDGATE");
				requestParam.put("sessionID", (String)IdgateData.get("sessionid"));		 
				requestParam.put("txnID", (String)IdgateData.get("txnID"));		 
				requestParam.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
			}	

			String kyc_insert_hist_pass = (String) SessionUtil.getAttribute(model, SessionUtil.KYC_INSERT_HIST_PASS, null);		
			if(!requestParam.containsKey("kyc_insert_uuid") || kyc_insert_hist_pass == null || !requestParam.get("kyc_insert_uuid").equals(kyc_insert_hist_pass)) {
				bs.setResult(false);
				throw new Exception("kyc_insert_hist_pass check error");
			}
			String kyc_insert_hist_pass_finsh = (String) SessionUtil.getAttribute(model, SessionUtil.KYC_INSERT_HIST_PASS_FINSH, null);		
			if(!requestParam.containsKey("kyc_insert_uuid") || requestParam.get("kyc_insert_uuid").equals(kyc_insert_hist_pass_finsh)) {
				bs.setResult(false);
				throw new Exception("kyc_insert_hist_pass check error");
			}
			SessionUtil.addAttribute(model, SessionUtil.KYC_INSERT_HIST_PASS_FINSH, requestParam.get("kyc_insert_uuid"));
			
			bs = fund_transfer_service.fund_invest_attr_s(requestParam);

			//20200226註解 ( 如要上KYC功能修正再打開 )
			if ("N".equals(requestParam.get("AGREE"))) {
				String TXID = requestParam.get("TXID");
				log.debug(ESAPIUtil.vaildLog("TXID={}" + TXID));

				if ("C021".equals(TXID) || "C032".equals(TXID)) {
					target = "forward:/FUND/TRANSFER/query_fund_transfer_data";
				} else if ("C016".equals(TXID)) {
					target = "forward:/FUND/PURCHASE/fund_purchase_select";
				} else if ("C017".equals(TXID)) {
					target = "forward:/FUND/REGULAR/fund_regular_select";
				} else if ("C031".equals(TXID)) {
					target = "forward:/FUND/RESERVE/PURCHASE/fund_reserve_purchase_select";
				} else if ("N09001".equals(TXID) || "N09101".equals(TXID)) {
					target = "forward:/GOLD/TRANSACTION/gold_buy";
				} else if ("N09002".equals(TXID) || "N09102".equals(TXID)) {
					target = "forward:/GOLD/TRANSACTION/gold_resale";
				} else if ("NA50".equals(TXID)) {
					target = "forward:/GOLD/APPLY/gold_account_apply";
				} else if ("S".equals(TXID) || "G".equals(TXID)) {
					target = "forward:/FUND/TRANSFER/fund_invest_attr";
				} else if ("B019".equals(TXID)) {
					target = "forward:/BOND/PURCHASE/bond_purchase_input";
				}
				return target;
			}
			if (bs != null && bs.getResult()) {
				String TXID = requestParam.get("TXID");
				log.debug(ESAPIUtil.vaildLog("TXID={}" + TXID));

				if ("C021".equals(TXID) || "C032".equals(TXID)) {
					target = "forward:/FUND/TRANSFER/query_fund_transfer_data";
				} else if ("C016".equals(TXID)) {
					target = "forward:/FUND/PURCHASE/fund_purchase_select";
				} else if ("C017".equals(TXID)) {
					target = "forward:/FUND/REGULAR/fund_regular_select";
				} else if ("C031".equals(TXID)) {
					target = "forward:/FUND/RESERVE/PURCHASE/fund_reserve_purchase_select";
				} else if ("N09001".equals(TXID) || "N09101".equals(TXID)) {
					target = "forward:/GOLD/TRANSACTION/gold_buy";
				} else if ("N09002".equals(TXID) || "N09102".equals(TXID)) {
					target = "forward:/GOLD/TRANSACTION/gold_resale";
				} else if ("NA50".equals(TXID)) {
					target = "forward:/GOLD/APPLY/gold_account_apply";
				} else if ("B019".equals(TXID)) {
					target = "forward:/BOND/PURCHASE/bond_purchase_input";
				}else if ("S".equals(TXID) || "G".equals(TXID)) {

					String FDINVTYPE = requestParam.get("FDINVTYPE");
					log.debug(ESAPIUtil.vaildLog("FDINVTYPE={}" + FDINVTYPE));

					String FDINVTYPEChinese;
					if ("1".equals(FDINVTYPE)) {
						FDINVTYPEChinese = i18n.getMsg("LB.D0945");// 積極型
					} else if ("2".equals(FDINVTYPE)) {
						FDINVTYPEChinese = i18n.getMsg("LB.X1766");// 穩健型
					} else {
						FDINVTYPEChinese = i18n.getMsg("LB.X1767");// 保守型
					}
					log.debug("FDINVTYPEChinese={}", FDINVTYPEChinese);
					model.addAttribute("FDINVTYPEChinese", FDINVTYPEChinese);

					String CUSIDN = requestParam.get("CUSIDN");
					log.debug(ESAPIUtil.vaildLog("CUSIDN={}" + CUSIDN));

					String type = "";
					if (CUSIDN.length() == 10) {
						type = "（" + i18n.getMsg("LB.D0856") + "）";// 自然人版
					} else {
						type = "（" + i18n.getMsg("LB.X1770") + "）";// 法人版
					}
					log.debug("type={}", type);
					model.addAttribute("type", type);

					target = "/fund/fund_invest_attr5";
				}
			}
		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("fund_invest_attr_s error >> {}", e);
		} finally {
			if (bs != null && bs.getResult()) {

			} else {
				bs.setPrevious("/FUND/TRANSFER/fund_invest_attr");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * 前往法人KYC頁
	 */
	@RequestMapping(value = "/fund_invest_attr3")
	public String fund_invest_attr3(HttpServletRequest request, @RequestParam Map<String, String> requestParam,
			Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.debug("fund_invest_attr3");
		Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
		try {
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"UTF-8");
			String UID = cusidn;

			// 判斷KYC每日僅能填寫三次條件
			boolean todayUserKYCCountBoolean = fund_transfer_service.todayUserKYCCount(UID);
			model.addAttribute("todayUserKYCCountBoolean", todayUserKYCCountBoolean);

			String TXID = okMap.get("TXID");
			log.debug(ESAPIUtil.vaildLog("TXID={}" + TXID));
			model.addAttribute("TXID", TXID);

			if (TXID != null && !"".equals(TXID) && TXID.substring(0, 1).equals("C")) {
				bs = fund_transfer_service.getN922Data(UID);
			} else {
				bs = fund_transfer_service.getN927Data(UID);
			}
			fund_transfer_service.prepareInvestAttr3Data(bs, TXID, model);
			 //IDGATE身分
			 String idgateUserFlag="N";		 
			 Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
			 try {		 
				 if(IdgateData==null) {
					 IdgateData = new HashMap<String, Object>();
				 }
				 BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
				 if(tmp.getResult()) {		 
					 idgateUserFlag="Y";                  		 
				 }		 
				 tmp.addData("idgateUserFlag",idgateUserFlag);		 
				 IdgateData.putAll((Map<String, String>) tmp.getData());
				 SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
			 }catch(Exception e) {		 
				 log.debug("idgateUserFlag error {}",e);		 
			 }		 
			 model.addAttribute("idgateUserFlag",idgateUserFlag);

			target = "/fund/fund_invest_attr3";
		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("fund_invest_attr3 error >> {}", e);
		}
		return target;
	}

	/**
	 * 前往法人KYC確認頁
	 */
	@RequestMapping(value = "/fund_invest_attr4")
	public String fund_invest_attr4(HttpServletRequest request, @RequestParam Map<String, String> requestParam,
			Model model) {
		String target = "/error";
		log.debug("fund_invest_attr4");
		Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
		try {
			String XMLCOD = (String) SessionUtil.getAttribute(model, SessionUtil.XMLCOD, null);
			model.addAttribute("XMLCOD", XMLCOD);

			fund_transfer_service.prepareInvestAttr4Data(okMap, model);

			target = "/fund/fund_invest_attr4";
			Map<String, String> result = new HashMap<>();
			result.putAll(okMap);
			log.debug("session result >> {}", result);
			// IDGATE transdata
            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
    		String adopid = "N322";
    		String title = "您有一筆投資屬性評估調查表－（法人版）問卷結果更新待確認";
        	if(IdgateData != null) {
	            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
	            if(IdgateData.get("idgateUserFlag").equals("Y")) {
	            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
	            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
					IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N322_IDGATE_DATA.class, N322_2_IDGATE_DATA_VIEW.class, result));
	                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	            }
        	}
            model.addAttribute("idgateAdopid", adopid);
            model.addAttribute("idgateTitle", title);
		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("fund_invest_attr4 error >> {}", e);
		}
		return target;
	}

	/**
	 * 前往基金轉換選擇頁
	 */
	@RequestMapping(value = "/fund_transfer_select")
	public String fund_transfer_select(HttpServletRequest request, @RequestParam Map<String, String> requestParam,
			Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.debug("fund_transfer_select");
		try {
			bs = new BaseResult();
			// 用來判斷即時或預約交易
			boolean isFundTradeTime = fund_transfer_service.isFundTradeTime();
			log.debug("isFundTradeTime={}", isFundTradeTime);

			// 預約交易
			if (isFundTradeTime == false) {
				// 假的
				// if(true){
				target = "forward:/FUND/RESERVE/TRANSFER/fund_reserve_transfer_select";
				return target;
			}

			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {

				String TRANSCODE = requestParam.get("TRANSCODE");
				log.debug(ESAPIUtil.vaildLog("TRANSCODE={}" + TRANSCODE));
				model.addAttribute("TRANSCODE", TRANSCODE);
				
				String FEETYPE = requestParam.get("FEE_TYPE");
				log.debug(ESAPIUtil.vaildLog("FEE_TYPE={}" + FEETYPE));
				model.addAttribute("FEE_TYPE", FEETYPE);
				
				String FUSMON = requestParam.get("FUSMON");
				log.debug(ESAPIUtil.vaildLog("FUSMON={}" + FUSMON));
				model.addAttribute("FUSMON", FUSMON);
				
				String TRANSCRY = requestParam.get("CRY");
				log.debug(ESAPIUtil.vaildLog("TRANSCRY={}" + TRANSCRY));
				model.addAttribute("TRANSCRY", TRANSCRY);
				
				String COUNTRYTYPE = requestParam.get("COUNTRYTYPE");
				log.debug(ESAPIUtil.vaildLog("COUNTRYTYPE={}" + COUNTRYTYPE));
				model.addAttribute("COUNTRYTYPE", COUNTRYTYPE);
				
				
				List<TXNFUNDDATA> finalFundDataList = fund_transfer_service
						.getFundDataByGroupMark(FEETYPE,TRANSCODE,FUSMON,COUNTRYTYPE,TRANSCRY);
				if (finalFundDataList.size() == 0) {
					model.addAttribute(BaseResult.ERROR, bs);
					return target;
				}
				model.addAttribute("finalFundDataList", finalFundDataList);

				bs = fund_transfer_service.prepareFundTransferSelectData(requestParam);
				bs.addData("TRANSCODE", TRANSCODE);
				bs.addData("finalFundDataList", finalFundDataList);
				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, bs);
			}

		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("fund_transfer_select error >> {}", e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/fund/fund_transfer_select";
				model.addAttribute("result_data", bs);
			} else {
				bs.setPrevious("/FUND/TRANSFER/query_fund_transfer_data");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * 前往基金風險確認頁
	 */
	@RequestMapping(value = "/fund_risk_confirm")
	public String fund_risk_confirm(HttpServletRequest request, @RequestParam Map<String, String> requestParam,
			Model model) {
		String target = "/error";
		BaseResult bs = null;

		log.debug("fund_risk_confirm");
		
		//基金交易若無報錯，TOPMSG塞0000，msgCode塞0
		if (requestParam.get("TOPMSG") == null || requestParam.get("TOPMSG").isEmpty()) {
			requestParam.put("TOPMSG", "0000");
			requestParam.put("msgCode", "0");
		}
		
		//基金交易錯誤代號處理
		if (!requestParam.get("TOPMSG").equals("0000") && !requestParam.get("msgCode").equals("0")) {
			String TXID = requestParam.get("TXID");
			log.debug(ESAPIUtil.vaildLog("TXID={}" + TXID));
			String link = "";
			if ("C021".equals(TXID) || "C032".equals(TXID)) {
				link = "/FUND/TRANSFER/query_fund_transfer_data";
			} else if ("C016".equals(TXID)) {
				link = "/FUND/PURCHASE/fund_purchase_select";
			} else if ("C017".equals(TXID)) {
				link = "/FUND/REGULAR/fund_regular_select";
			} else if ("C031".equals(TXID)) {
				link = "/FUND/RESERVE/PURCHASE/fund_reserve_purchase_select";
			}
			
			Map<String, Object> errorData = new HashMap<String, Object>();
			errorData.put("msgCode", requestParam.get("msgCode"));
			errorData.put("message", requestParam.get("message"));
			errorData.put("previous", link);
			model.addAttribute("error", errorData);
			return target;
		}
		
		try {

			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP2_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
					bs = fund_transfer_service.prepareFundRiskConfirmData(requestParam);
					SessionUtil.addAttribute(model, SessionUtil.STEP2_LOCALE_DATA, bs);
			}

		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("fund_risk_confirm error >> {}", e);
		} finally {
			if (bs != null && bs.getResult()) {
				if(StrUtil.isNotEmpty(requestParam.get("Step")) && "A".equals(requestParam.get("FEE_TYPE"))) {
					//後收特別約定事項切開
					target = "/fund/fund_risk_confirm2";
				}else {
					//普通的
					target = "/fund/fund_risk_confirm";
				}
				model.addAttribute("result_data", bs);
				log.debug("fund_risk_confirm: result_data = {}", bs.getData());
			} else {
				bs.setPrevious("/FUND/TRANSFER/query_fund_transfer_data");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	//20210802 依需求 , 將此頁跟報酬率資訊頁面整合
//	/**
//	 * 前往基金近五年費用率及報酬率資訊
//	 */
//	@RequestMapping(value = "/fund_information_confirm")
//	public String fund_information_confirm(HttpServletRequest request,@RequestParam Map<String,String> requestParam,Model model){
//		String target = "/error";
//		BaseResult bs = null;
//		
//		log.debug("fund_information_confirm");
//		try{
//			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
////			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
//			bs = fund_transfer_service.prepareFundPublicData(okMap);
//		}
//		catch(Exception e){
//			//Avoid Information Exposure Through an Error Message
//			//e.printStackTrace();
//			log.error("fund_information_confirm error >> {}",e);
//		}finally {
//			if(bs != null && bs.getResult()) {
//				target = "/fund/fund_information_confirm";
//				model.addAttribute("result_data", bs);
//			}else {
//				bs.setPrevious("/FUND/TRANSFER/query_fund_transfer_data");
//				model.addAttribute(BaseResult.ERROR, bs);
//			}
//		}
//		return target;
//	}
	/**
	 * 基金風險確認頁同意
	 */
	@RequestMapping(value = "/fund_risk_confirmY")
	public String fund_risk_confirmY(HttpServletRequest request, @RequestParam Map<String, String> requestParam,
			Model model) {
		String target = "/error";

		log.debug("fund_risk_confirmY");
		try {
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"UTF-8");
			requestParam.put("UID", cusidn);

			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (!hasLocale) {
				fund_transfer_service.insertFundTraceLog(requestParam);
			}
			target = "forward:/FUND/TRANSFER/fund_public";
		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("fund_risk_confirmY error >> {}", e);
		}
		return target;
	}

	/**
	 * 基金風險確認頁取消
	 */
	@RequestMapping(value = "/fund_risk_confirmN")
	public String fund_risk_confirmN(HttpServletRequest request, @RequestParam Map<String, String> requestParam,
			Model model) {
		String target = "/error";

		log.debug("fund_risk_confirmN");
		try {
			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				requestParam = (Map<String, String>) SessionUtil.getAttribute(model, SessionUtil.STEP3_LOCALE_DATA,
						null);
			}
			if (!hasLocale) {
				String cusidn = new String(
						Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
						"UTF-8");
				requestParam.put("UID", cusidn);

				fund_transfer_service.insertFundTraceLog(requestParam);

				SessionUtil.addAttribute(model, SessionUtil.STEP3_LOCALE_DATA, requestParam);
			}

			String TXID = requestParam.get("TXID");
			log.debug(ESAPIUtil.vaildLog("TXID={}" + TXID));
			if ("C021".equals(TXID) || "C032".equals(TXID)) {
				target = "forward:/FUND/TRANSFER/query_fund_transfer_data?ConfirmFlag=TRUE";
			} else if ("C016".equals(TXID)) {
				target = "forward:/FUND/PURCHASE/fund_purchase_select?ConfirmFlag=TRUE";
			} else if ("C017".equals(TXID)) {
				target = "forward:/FUND/REGULAR/fund_regular_select?ConfirmFlag=TRUE";
			} else if ("C031".equals(TXID)) {
				target = "forward:/FUND/RESERVE/PURCHASE/fund_reserve_purchase_select?ConfirmFlag=TRUE";
			}
		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("fund_risk_confirmN error >> {}", e);
		}
		return target;
	}

	/**
	 * 前往基金公開說明書頁
	 */
	@RequestMapping(value = "/fund_public")
	public String fund_public(HttpServletRequest request, @RequestParam Map<String, String> requestParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.debug(ESAPIUtil.vaildLog("fund_public"+CodeUtil.toJson(requestParam)));
		try {
			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP3_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				
				//原本在 /fund_information_confirm > fund_information_confirm.jsp打confirmY做 , 現在跳過fund_information_confirm了所以移至此
				fund_transfer_service.insertFundTraceLog(requestParam);
				
				bs = fund_transfer_service.prepareFundPublicData(requestParam);
				SessionUtil.addAttribute(model, SessionUtil.STEP3_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("fund_public error >> {}", e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "fund/fund_public";
				log.debug("/fund_public bs.getData = " + bs.getData());
				model.addAttribute("result_data", bs);
			} else {
				bs.setPrevious("/FUND/TRANSFER/query_fund_transfer_data");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * 基金公開說明書同意，前往基金轉換手續費頁
	 */
	@RequestMapping(value = "/fund_fee_expose")
	public String fund_fee_expose(HttpServletRequest request, @RequestParam Map<String, String> requestParam,
			Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.debug("fund_fee_expose");
		try {
			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP3_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				String cusidn = new String(
						Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
						"UTF-8");
				requestParam.put("CUSIDN", cusidn);
				bs = fund_transfer_service.prepareFundFeeExposeData(requestParam);
				SessionUtil.addAttribute(model, SessionUtil.STEP3_LOCALE_DATA, bs);
			}

			// IKEY要使用的JSON:DC
			String jsondc = URLEncoder.encode(CodeUtil.toJson(requestParam), "UTF-8");
			log.debug(ESAPIUtil.vaildLog("jsondc={}" + jsondc));
			model.addAttribute("jsondc", jsondc);
		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("fund_fee_expose error >> {}", e);
		} finally {

			if (bs != null && bs.getResult()) {
				target = "fund/fund_fee_expose_bs";
				model.addAttribute("result_data", bs);
				log.debug("/fund_reserve_purchase_fee_expose bs.getData = " + bs.getData());
			} else {
				bs.setPrevious("/FUND/TRANSFER/query_fund_transfer_data");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * 前往基金轉換確認頁
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/fund_transfer_confirm")
	public String fund_transfer_confirm(HttpServletRequest request, @RequestParam Map<String, String> requestParam,
			Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.debug("fund_transfer_confirm");
		try {
			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				String cusidn = new String(
						Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
						"UTF-8");
				log.debug("cusidn={}", cusidn);
				bs = fund_transfer_service.prepareFundTransferConfirmData(requestParam, cusidn);
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("fund_transfer_confirm error >> {}", e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "fund/fund_transfer_confirm";
				model.addAttribute("result_data", bs);
				log.debug("fund/fund_transfer_confirm bs.getData = " + bs.getData());
			} else {
				bs.setPrevious("/FUND/TRANSFER/query_fund_transfer_data");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * 前往基金轉換結果頁
	 */
	@RequestMapping(value = "/fund_transfer_result")
	@Dialog(ADOPID = "C021")
	public String fund_transfer_result(HttpServletRequest request, @RequestParam Map<String, String> requestParam,
			Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.debug("fund_transfer_result");
		try {
			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				String cusidn = new String(
						Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
						"UTF-8");
				log.debug("cusidn={}", cusidn);
				requestParam.put("CUSIDN", cusidn);

				String KYCDATE = (String) SessionUtil.getAttribute(model, SessionUtil.KYCDATE, null);
				log.debug("KYCDATE={}", KYCDATE);
				requestParam.put("KYCDATE", KYCDATE);

				String WEAK = (String) SessionUtil.getAttribute(model, SessionUtil.WEAK, null);
				log.debug("WEAK={}", WEAK);
				requestParam.put("WEAK", WEAK);

				// 寄件者信箱
				String DPMYEMAIL = (String) SessionUtil.getAttribute(model, SessionUtil.DPMYEMAIL, null);
				log.debug("DPMYEMAIL={}", DPMYEMAIL);
				requestParam.put("DPMYEMAIL", DPMYEMAIL);

				// 收件者信箱
				requestParam.put("CMTRMAIL", DPMYEMAIL);

				String IP = WebUtil.getIpAddr(request);
				log.debug(ESAPIUtil.vaildLog("IP >> " + IP));
				requestParam.put("IP", IP);

				bs = fund_transfer_service.processFundTransfer(requestParam);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("fund_transfer_result error >> {}", e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "fund/fund_transfer_result";
				model.addAttribute("result_data", bs);
				log.debug("fund/fund_transfer_result bs.getData = " + bs.getData());
			} else {
				bs.setPrevious("/FUND/TRANSFER/query_fund_transfer_data");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * 前往基金EMAIL設定頁
	 */
	@RequestMapping(value = "/fundemail")
	public String fundemail(HttpServletRequest request, @RequestParam Map<String, String> requestParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.debug("fundemail");
		try {
			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				bs = new BaseResult();
				String cusidn = new String(
						Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
						"UTF-8");
				log.debug("cusidn={}", cusidn);
				String UID = cusidn;
				bs.addData("UID", UID);

				String TXID = requestParam.get("TXID");
				log.debug(ESAPIUtil.vaildLog("TXID={}" + TXID));
				bs.addData("TXID", TXID);
				bs.setResult(true);
				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("fundemail error >> {}", e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "fund/fundemail";
				model.addAttribute("result_data", bs);
			} else {
				bs.setPrevious("/INDEX/index");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * 更改基金EMAIL
	 */
	@RequestMapping(value = "/change_fundemail")
	public String change_fundemail(HttpServletRequest request, @RequestParam Map<String, String> requestParam,
			Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.debug("change_fundemail");
		try {
			bs = new BaseResult();

			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"UTF-8");
			log.debug("cusidn={}", cusidn);
			requestParam.put("CUSIDN", cusidn);

			String newEmail = requestParam.get("newEmail");
			log.debug(ESAPIUtil.vaildLog("newEmail={}" + newEmail));
			requestParam.put("NEW_EMAIL", newEmail);

			bs = fund_transfer_service.change_fundemail(requestParam, model);
			log.debug("bs.getData={}", bs.getData());

			if (bs == null || !bs.getResult()) {
				model.addAttribute(BaseResult.ERROR, bs);
				return target;
			}

			List<TXNUSER> txnUserList = fund_transfer_service.getTxnUserList(cusidn);

			if (txnUserList == null) {
				model.addAttribute(BaseResult.ERROR, bs);
				return target;
			}

			String DPMYEMAIL = txnUserList.get(0).getDPMYEMAIL();
			log.debug(ESAPIUtil.vaildLog("DPMYEMAIL={}" + DPMYEMAIL));
			SessionUtil.addAttribute(model, SessionUtil.DPMYEMAIL, DPMYEMAIL);

			String TXID = requestParam.get("TXID");
			log.debug(ESAPIUtil.vaildLog("TXID={}" + TXID));

			if ("C021".equals(TXID) || "C032".equals(TXID)) {
				target = "forward:/FUND/TRANSFER/query_fund_transfer_data";
			} else if ("C016".equals(TXID)) {
				target = "forward:/FUND/PURCHASE/fund_purchase_select";
			} else if ("C017".equals(TXID)) {
				target = "forward:/FUND/REGULAR/fund_regular_select";
			} else if ("C024".equals(TXID) || "C033".equals(TXID)) {
				target = "forward:/FUND/REDEEM/fund_redeem_data";
			} else if ("C031".equals(TXID)) {
				target = "forward:/FUND/RESERVE/PURCHASE/fund_reserve_purchase_select";
			} else if ("C111".equals(TXID)) {

			}
		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("change_fundemail error >> {}", e);
		}
		return target;
	}

	/**
	 * 依照TRANSCODE取得基金的資料的AJAX
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/getFundDataAjax", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody BaseResult getFundDataAjax(@RequestParam Map<String, String> requestParam) {
		log.debug("IN getFundDataAjax");

		BaseResult baseResult = new BaseResult();
		baseResult.setResult(Boolean.FALSE);

		try {
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);

			String INTRANSCODE = okMap.get("INTRANSCODE");
			log.debug(ESAPIUtil.vaildLog("INTRANSCODE={}" + INTRANSCODE));

			TXNFUNDDATA txnFundData = fund_transfer_service.getFundData(INTRANSCODE);
			
			
			
			if (txnFundData != null) {
				Map<String, String> resultMap = new HashMap<String, String>();
				resultMap = BeanUtils.describe(txnFundData);

				String CRY = txnFundData.getTRANSCRY();
				log.debug(ESAPIUtil.vaildLog("CRY={}" + CRY));

				ADMCURRENCY admCurrency = fund_transfer_service.getCRYData(CRY);
				String locale = LocaleContextHolder.getLocale().toString();
				if (admCurrency != null) {
					String ADCCYNAME = "";
					if(locale.equals("zh_CN")) {
						ADCCYNAME = admCurrency.getADCCYCHSNAME();
					}
					else if(locale.equals("en")) {
						ADCCYNAME = admCurrency.getADCCYENGNAME();
					}
					else {
						ADCCYNAME = admCurrency.getADCCYNAME();
					}
					log.debug("ADCCYNAME={}", ADCCYNAME);
					
					resultMap.put("ADCCYNAME", ADCCYNAME);
				}
				
				resultMap.put("CBAMT_FMT", NumericUtil.formatNumberString(resultMap.get("CBAMT"), 0));
				resultMap.put("YBAMT_FMT", NumericUtil.formatNumberString(resultMap.get("YBAMT"), 0));
				resultMap.put("CBTAMT_FMT", NumericUtil.formatNumberString(resultMap.get("CBTAMT"), 0));
				resultMap.put("YBTAMT_FMT", NumericUtil.formatNumberString(resultMap.get("YBTAMT"), 0));
				
				baseResult.setResult(Boolean.TRUE);
				String dataString = new Gson().toJson(resultMap, resultMap.getClass());
				log.debug(ESAPIUtil.vaildLog("dataString={}" + dataString));

				baseResult.setData(dataString);
			}
		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("getFundDataAjax error >> {}", e);
		}
		return baseResult;
	}
//	20200226註解 ( 如要上KYC功能修正再打開 )
	/**
	 * N951_aj
	 * 
	 * @return
	 */
	@RequestMapping(value = "/n951_aj", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody BaseResult n951_aj(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.info("n951_aj...");
		BaseResult bs = null;
		try {
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");
			bs = fund_transfer_service.N951_REST(cusidn, reqParam);
		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("n951_aj error >> {}", e);
		}
		return bs;
	}
	
	/**
	 * INSERT_TXNCUSINVATTRHIST_aj
	 * 
	 * @return
	 */
	@RequestMapping(value = "/insert_hist", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody BaseResult insert_hist(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.info("insert_hist_aj...");
		BaseResult bs = null;
		try {
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			bs = new BaseResult();
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");
			String aduserip = (String) SessionUtil.getAttribute(model, SessionUtil.USERIP, null);
			// 解決Trust Boundary Violation
			okMap.put("CUSIDN", cusidn);
			okMap.put("ADUSERIP", aduserip);
			log.info(ESAPIUtil.vaildLog("okMap" + okMap));
			//獲取 中心計算之KYC分數及投資屬性
			bs = fund_transfer_service.getKycScore(okMap);
			String SCORE =StrUtil.isNotEmpty((String) ((Map<String,Object>)bs.getData()).get("SCORE"))?(String) ((Map<String,Object>)bs.getData()).get("SCORE"):"";
			String RISK =StrUtil.isNotEmpty((String) ((Map<String,Object>)bs.getData()).get("RISK"))?(String) ((Map<String,Object>)bs.getData()).get("RISK"):"";
			okMap.put("FDINVTYPE", RISK);
			okMap.put("FDSCORE", SCORE);
			bs = fund_transfer_service.aj_insert_txncusinvattrhist(okMap);
			bs.addData("FDINVTYPE", RISK);
			bs.addData("iScore", SCORE);
			if(bs.getResult()) {
				String uuid = UUID.randomUUID().toString();
				SessionUtil.addAttribute(model, SessionUtil.KYC_INSERT_HIST_PASS, uuid);
				bs.addData("kyc_insert_uuid", uuid);
			}
		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("insert_hist error >> {}", e);
		}
		return bs;
	}
	
	@RequestMapping(value = "/check_KYC_times", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody BaseResult check_KYC_times(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.info("check_KYC_times...");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");

			// 判斷KYC每日僅能填寫三次條件
			boolean todayUserKYCCountBoolean = fund_transfer_service.todayUserKYCCount(cusidn);
			bs.setResult(todayUserKYCCountBoolean);
			
		} catch (Exception e) {
			log.error("check_KYC_times error >> {}", e);
		}
		return bs;
	}
}