package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class IdGateMappingQuery_REST_RSDATA2 implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -2373605397642836469L;
	/**
	 * 
	 */
	private String DEVICEID;
    private String DEVICENAME;
    private String DEVICETYPE;
    private String DEVICESTATUS;
    private String VERIFYTYPE;
    private String ERRORCNT;
    private String ISDEFAULT;
    private String LASTDATE;
    private String LASTTIME;
    private String QUICKID;
    private String CUSIDN;
    private String IDGATEID;
	private String QCKTRAN;
    
	public String getQCKTRAN() {
		return QCKTRAN;
	}
	public void setQCKTRAN(String qCKTRAN) {
		QCKTRAN = qCKTRAN;
	}
	public String getDEVICEID() {
		return DEVICEID;
	}
	public void setDEVICEID(String dEVICEID) {
		DEVICEID = dEVICEID;
	}
	public String getDEVICENAME() {
		return DEVICENAME;
	}
	public void setDEVICENAME(String dEVICENAME) {
		DEVICENAME = dEVICENAME;
	}
	public String getDEVICETYPE() {
		return DEVICETYPE;
	}
	public void setDEVICETYPE(String dEVICETYPE) {
		DEVICETYPE = dEVICETYPE;
	}
	public String getDEVICESTATUS() {
		return DEVICESTATUS;
	}
	public void setDEVICESTATUS(String dEVICESTATUS) {
		DEVICESTATUS = dEVICESTATUS;
	}
	public String getVERIFYTYPE() {
		return VERIFYTYPE;
	}
	public void setVERIFYTYPE(String vERIFYTYPE) {
		VERIFYTYPE = vERIFYTYPE;
	}
	public String getERRORCNT() {
		return ERRORCNT;
	}
	public void setERRORCNT(String eRRORCNT) {
		ERRORCNT = eRRORCNT;
	}
	public String getISDEFAULT() {
		return ISDEFAULT;
	}
	public void setISDEFAULT(String iSDEFAULT) {
		ISDEFAULT = iSDEFAULT;
	}
	public String getLASTDATE() {
		return LASTDATE;
	}
	public void setLASTDATE(String lASTDATE) {
		LASTDATE = lASTDATE;
	}
	public String getLASTTIME() {
		return LASTTIME;
	}
	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}
	
	public String getQUICKID() {
		return QUICKID;
	}
	public void setQUICKID(String qUICKID) {
		QUICKID = qUICKID;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getIDGATEID() {
		return IDGATEID;
	}
	public void setIDGATEID(String iDGATEID) {
		IDGATEID = iDGATEID;
	}

	

}
