package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class Ratequery_N023_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1253165411003606404L;
	
	// 資料陣列
	LinkedList<Ratequery_N023_RSDATA> REC;

	public LinkedList<Ratequery_N023_RSDATA> getREC() {
		return REC;
	}

	public void setREC(LinkedList<Ratequery_N023_RSDATA> rEC) {
		REC = rEC;
	}
	
	
}
