package tw.com.fstop.nnb.rest.bean;

public class N935_REST_RSDATA
{
	private String AMOUNT;//匯款設定金額

    private String ACN;//匯入匯款通知帳號

    private String FLAG;//是否設定匯款通知

	public String getAMOUNT()
	{
		return AMOUNT;
	}

	public void setAMOUNT(String aMOUNT)
	{
		AMOUNT = aMOUNT;
	}

	public String getACN()
	{
		return ACN;
	}

	public void setACN(String aCN)
	{
		ACN = aCN;
	}

	public String getFLAG()
	{
		return FLAG;
	}

	public void setFLAG(String fLAG)
	{
		FLAG = fLAG;
	}
    
    

}
