package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N215A_REST_RQ extends BaseRestBean_OLS implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3758405647099239634L;
	
	private String UID;//使用者帳號
	private String ADOPID;
	
	public String getADOPID() {
		return ADOPID;
	}

	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}

	public String getUID() {
		return UID;
	}

	public void setUID(String uID) {
		UID = uID;
	}



	

	
}
