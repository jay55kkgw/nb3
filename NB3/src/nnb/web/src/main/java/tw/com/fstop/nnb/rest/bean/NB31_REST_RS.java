package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class NB31_REST_RS extends BaseRestBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6883969117552658405L;
	private String MSGCOD;
	private String CUSIDN;
	private String ACN;
	private String UID;
	private String POSTCOD;
	private String ACNTYPE;
	private String MILADR;
	public String getMSGCOD() {
		return MSGCOD;
	}
	public void setMSGCOD(String mSGCOD) {
		MSGCOD = mSGCOD;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	public String getPOSTCOD() {
		return POSTCOD;
	}
	public void setPOSTCOD(String pOSTCOD) {
		POSTCOD = pOSTCOD;
	}
	public String getACNTYPE() {
		return ACNTYPE;
	}
	public void setACNTYPE(String aCNTYPE) {
		ACNTYPE = aCNTYPE;
	}
	public String getMILADR() {
		return MILADR;
	}
	public void setMILADR(String mILADR) {
		MILADR = mILADR;
	}

}
