package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N555_REST_RQ extends BaseRestBean_FX implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5300378813490277990L;

	String CUSIDN;			//統一編號
	String LCNO;			//信用狀號碼
	String TRNSRC = "NB";	//交易來源
	String USERDATA_X50;	//本次未完資料KEY值

	String ADOPID;
	
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getUSERDATA_X50() {
		return USERDATA_X50;
	}
	public void setUSERDATA_X50(String uSERDATA_X50) {
		USERDATA_X50 = uSERDATA_X50;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getLCNO() {
		return LCNO;
	}
	public void setLCNO(String lCNO) {
		LCNO = lCNO;
	}
	public String getTRNSRC() {
		return TRNSRC;
	}
	public void setTRNSRC(String tRNSRC) {
		TRNSRC = tRNSRC;
	}
}
