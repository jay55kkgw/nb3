package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N191_REST_RSDATA  extends BaseRestBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4182932702069176573L;
	
	private String ACN; //帳號
	LinkedList<N191_REST_RSDATA2> TABLE;
	
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public LinkedList<N191_REST_RSDATA2> getTable() {
		return TABLE;
	}
	public void setTable(LinkedList<N191_REST_RSDATA2> tABLE) {
		TABLE = tABLE;
	}
}
