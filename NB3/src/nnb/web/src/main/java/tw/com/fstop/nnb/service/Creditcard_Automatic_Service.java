package tw.com.fstop.nnb.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.google.gson.Gson;

import fstop.orm.po.ADMCARDDATA;
import fstop.orm.po.TXNCARDAPPLY;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.AdmCardDataDao;
import tw.com.fstop.tbb.nnb.dao.CreditUtils;
import tw.com.fstop.tbb.nnb.dao.TxnCardApplyDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;
import tw.com.fstop.tbb.nnb.util.StrUtils;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.web.util.I18nHelper;

@Service
public class Creditcard_Automatic_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	I18n i18n;

	/**
	 * 取得信用卡之轉出帳號
	 * 
	 * @param cusidn
	 * @param type
	 * @return
	 */
	public BaseResult getOutAcnoList(String cusidn, String type) {
		log.trace("acno>>{} ", cusidn);
		log.trace("cusidn>>{} ", cusidn);
		BaseResult bs = null;
		Map<String, Object> tmpData = null;
		List<String> acnoList = new ArrayList<String>();
		List<Map<String, String>> acnoList2 = null;
		try {
			bs = new BaseResult();
			bs = N920_REST(cusidn, type);

			if (bs != null && bs.getResult()) {
				tmpData = (Map) bs.getData();
				acnoList2 = (List<Map<String, String>>) tmpData.get("REC");

				for (Map<String, String> acMap : acnoList2) {
					String ACN = acMap.get("ACN");
					acnoList.add(ACN);
				}

				bs.setData(acnoList);

			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getOutAcnoList error >> {}",e);
		}
		return bs;

	}

	/**
	 * 信用卡款自動扣繳申請/取消 輸入頁所需資料
	 * 
	 * @param reqParam
	 * @return
	 */
	public BaseResult debit_apply(Map<String, String> reqParam) {
		log.trace("debit_apply_service");
		log.trace(ESAPIUtil.vaildLog("cid>> " + reqParam.get("CUSIDN")));
		BaseResult bs = null;
		try {
			// 頁面所需資訊from N810
			bs = new BaseResult();
			//Avoid TXNLOG
			reqParam.put("ADOPID", "__N810");
			bs = N810_REST(reqParam);
			if("0".equals(((Map<String, String>) bs.getData()).get("msgCode"))) {
				String paymt_acct = ((Map<String, String>) bs.getData()).get("PAYMT_ACCT");
				log.trace("PAYMT_ACCT >>{}" , paymt_acct);
				String dbcode = ((Map<String, String>) bs.getData()).get("DBCODE");
				log.trace("DBCODE >>{}" , dbcode);
				//清除不必要的資訊
				bs.reset();
				bs.setResult(true);
				bs.addData("DBCODE", dbcode);
				bs.addData("PAYMT_ACCT", paymt_acct);
				bs.addData("CMQTIME",  DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
			}else {
				// 錯誤訊息頁
				bs.setResult(false);
				bs.setErrorMessage(((Map<String, String>) bs.getData()).get("msgCode"),
						((Map<String, String>) bs.getData()).get("msgName"));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("debit_apply error >> {}",e);
		}
		// return retMap;
		return bs;
	}
	
	/**
	 * 信用卡款自動扣繳申請/取消 確認頁所需資料
	 * 
	 * @param reqParam
	 * @return
	 */
	public BaseResult debit_apply_confirm(Map<String, String> reqParam) {
		log.trace("debit_apply_confirm_service");
		BaseResult bs = null;
		try {
			// 放入TXTOKEN
			bs = getTxToken();
			if(bs.getResult()) {
				reqParam.put("TXTOKEN",((Map<String,String>) bs.getData()).get("TXTOKEN"));
				bs.setData(reqParam);
				bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
				bs.setResult(Boolean.TRUE);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("debit_apply_confirm error >> {}",e);
		}
		// return retMap;
		return bs;
	}
	
	/**
	 * 信用卡款自動扣繳申請/取消 結果頁所需資料
	 * @param reqParam
	 * @param sessionId
	 * @return
	 */
	public BaseResult debit_apply_result(Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			log.trace(ESAPIUtil.vaildLog("debit_apply_result_service_reqParam = " + CodeUtil.toJson(reqParam)));
			bs = new BaseResult();

			// bs = call_N072Two(newMap);
			//For TXNLOG
			reqParam.put("ADTXACNO", reqParam.get("TSFACN"));
			bs = NI04_REST(reqParam);
			bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
			bs.addData("APPLYFLAG", reqParam.get("APPLYFLAG"));
			bs.addData("display_PAYFLAG", reqParam.get("display_PAYFLAG"));

		} catch (Exception e) {
			// TODO Auto-generated catch block
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("debit_apply_result error >> {}",e);
		}
		return bs;
	}
}